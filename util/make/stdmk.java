#
#   Copyright �� SQISoft Inc.
#   All Rights Reserved.
#
#   This source code is confidential and proprietary and may not be used
#   or distributed without the written permission of WINCC Inc.
#
#   Created by :  skpark
#   Modified by :
#   Last update : 2001/3/20
#   Comment :
#
#
#   stdmk.java
#
ifndef  _stdmk_java_
        _stdmk_java_ = ""
#
#

####
#### Your Local Makefile should define belows
####
#PACKAGE = 
#SRCS = *.java
#
#IDL_FILES =
#IDL_GENDIR =  ./gen
#IDL_GENSRCS = $(IDL_GENDIR)/**/*.java $(IDL_GENDIR)/**/**/*.java
#
#SRCPATH = .
####
####
####

ifndef MAKE_RFILE
    MAKE_RFILE = $(PROJECT_HOME)/log/make/make.result
endif

TARGETPATH = $(PROJECT_HOME)/classes

IDL = $(JACORB_ROOT)/bin/idl
IDL_INCLUDES =  -I$(PROJECT_HOME)/src/idl -I$(JACORB_ROOT)/idl/omg -I$(JACORB_ROOT)/idl/jacorb
IDL_FLAGS = -d $(IDL_GENDIR) $(IDL_INCLUDES) -ir  -all
JAVAC = $(JAVA_HOME)/bin/javac

CLASSPATH1 = $(PROJECT_HOME)/classes
CLASSPATH2 = $(JACORB_ROOT)/classes
CLASSPATH3 = ${TRDPARTYROOT}/java/ACE_java/classes
CLASSPATH4 = ${TRDPARTYROOT}/java/jdbc/classes12.zip
CLASSPATH5 = ${TRDPARTYROOT}/java/jdbc/nls_charset12.zip
CLASSPATH6 = ${TRDPARTYROOT}/java/EasyCharts/classes
CLASSPATH7 = ${J2EE_HOME}/lib/j2ee.jar:${J2EE_HOME}/lib/locale

CLASSPATH = $(CLASSPATH1):$(CLASSPATH2):$(CLASSPATH3):$(CLASSPATH4):$(CLASSPATH5):$(CLASSPATH6):$(CLASSPATH7)

ifndef IDL_FILES
	GEN_ENTRY =
else
	GEN_ENTRY =  gen
endif

all: maketry main makesucceed

main: 
	$(JAVAC) -classpath $(CLASSPATH) -sourcepath $(SRCPATH) -d $(TARGETPATH) $(SRCS) 

idl:
	$(IDL) $(IDL_FLAGS) $(IDL_FILES)	

$(GEN_ENTRY): idl
	$(JAVAC) -classpath $(CLASSPATH) -sourcepath $(IDL_GENDIR) -d $(TARGETPATH) $(IDL_GENSRCS) 

clean:
	rm -Rf $(TARGETPATH)/$(PACKAGE)/*.class

realclean:
	rm -Rf $(TARGETPATH)/$(PACKAGE)/*.class

maketry:
	touch $(MAKE_RFILE)
	echo " " >> $(MAKE_RFILE)
	echo `date ` >> $(MAKE_RFILE)
	echo '$(PACKAGE) make try { ' >> $(MAKE_RFILE)

makesucceed:
	echo '$(PACKAGE) make succeed } ' >> $(MAKE_RFILE)

#
#   end stdmk.java
#
endif
