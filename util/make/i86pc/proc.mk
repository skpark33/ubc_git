#
#   Copyright �� SQISoft Inc.
#   All Rights Reserved.
#
#   This source code is confidential and proprietary and may not be used
#   or distributed without the written permission of WINCC Inc.
#
#   Created by :  skpark
#   Modified by :
#   Last update : 2001/3/20
#   Comment :
#
#
#   proc.mk
#
ifndef  _proc_mk_
        _proc_mk_ = ""
#
#
#include $(ORACLE_HOME)/precomp/lib/env_precomp.mk
#include $(MAKEROOT)/stdmk

RDBMSLIB=$(ORACLE_HOME)/rdbms/lib/
LIBHOME=$(ORACLE_HOME)/lib/
PRODLIBHOME=$(ORACLE_HOME)/precomp/lib
#RDBMSLIB=$(ORACLE_HOME)/rdbms/lib64/
#LIBHOME=$(ORACLE_HOME)/lib64/
#PRODLIBHOME=$(ORACLE_HOME)/precomp/lib64
#CFLAGS="+DA2.0W +DS2.0 $(CFLAGS)"
#LFLAGS="+DA2.0W"

#
# The target 'build' puts together an executable $(EXE) from the .o files
# in $(OBJS) and the libraries in $(PROLDLIBS).  It is used to build the
# c sample programs.  The rules to make .o files from .c and .pc files are
# later in this file.
# ($(PROLDLIBS) includes the client shared library, and $(STATICPROLDLIBS) does
# not.)
#

#build: $(OBJS)
#	$(CC) $(LFLAGS) -o $(EXE) $(OBJS) -L$(LIBHOME) $(PROLDLIBS)
#
#build_static: $(OBJS)
#	$(CC) $(LFLAGS) -o $(EXE) $(OBJS) -L$(LIBHOME) $(STATICPROLDLIBS) $(STATICPROLDLIBS)

# The c++ samples are built using the 'cppbuild' target.  It precompiles to
# get a .c file, compiles to get a .o file and then builds the executable.
#

#cppcomp: 
#	$(PROC) $(PROCPPFLAGS) iname=$(SRCS) oname=$(SRCS).C
#	$(CC) -c -I$(INCLUDEDIR) $(INCLUDE) $(SRCS).C
#cppcomp_static: 
#	$(PROC) $(PROCPPFLAGS) iname=$(SRCS) oname=$(SRCS).C
#	$(CC) -c $(INCLUDE) $(SRCS).C
#
#cppbuild: 
#	$(PROC) $(PROCPPFLAGS) iname=$(EXE) oname=$(EXE).C
#	$(CC) $(LFLAGS) -c $(INCLUDE) $(EXE).C
#	$(CC) $(LFLAGS) -o $(EXE) $(OBJS) -L$(LIBHOME) $(CPPLDLIBS)
#
#cppbuild_static: 
#	$(PROC) $(PROCPPFLAGS) iname=$(EXE) oname=$(EXE).C
#	$(CC) $(LFLAGS) -c $(INCLUDE) $(EXE).C
#	$(CC) $(LFLAGS) -o $(EXE) $(OBJS) -L$(LIBHOME) $(STATICCPPLDLIBS) $(NAETTOBJS) $(LDFLAGSLIST)

#
# Some of the samples require that .sql scripts be run before precompilation.
# If you set RUNSQL=run in your call to make, then make will use sqlplus or
# svrmgrl, as appropriate, to run the .sql scripts.
# If you leave RUNSQL unset, then make will print out a reminder to run the
# scripts.
# If you have already run the scripts, then RUNSQL=done will omit the reminder.
#
#sqlplus_run:
#	($(CD) ../sql; $(BINHOME)sqlplus $(USER) @$(SCRIPT) < /dev/null)
#svrmgrl_run:
#	($(CD) ../sql; $(BINHOME)svrmgrl < $(SCRIPT).sql)
#sqlplus_ svrmgrl_:
#	@$(ECHO) "# You must run the .sql script, " $(SCRIPT),
#	@$(ECHO) "# before precomping this sample."
#sqlplus_done svrmgrl_done:
#
# Here are some rules for converting .pc -> .c -> .o and for .typ -> .h.
#
# If proc needs to find .h files, it should find the same .h files that the 
# c compiler finds.  We use a macro named INCLUDE to hadle that.  The general 
# format of the INCLUDE macro is 
# INCLUDE= $(I_SYM)dir1 $(I_SYM)dir2 ...
#
# Normally, I_SYM=-I, for the c compiler.  However, we have a special target,
# pc1, which calls $(PROC) with various arguments, include $(INCLUDE).  It
# is used like this:
#	$(MAKE) -f $(MAKEFILE) <more args to make> I_SYM=include= pc1
# This is used for some of $(SAMPLES) and for $(OBJECT_SAMPLE).
#.SUFFIXES: .pc .c .C .o .typ .h
#
#pc1:
#	$(PROC) $(PROCFLAGS) iname=$(PCCSRC) $(INCLUDE)
#
#.pc.c:
#	$(PROC) $(PROCFLAGS) iname=$*
#
#.pc.o:
#	$(PROC) $(PROCFLAGS) iname=$*
#	$(CC) $(CFLAGS64) -c $*.c
#
#.c.o:
#	$(CC) $(CFLAGS64) -c $*.c
#
#.typ.h:
#	$(OTT) intype=$*.typ hfile=$*.h outtype=$*o.typ $(OTTFLAGS) code=c user=scott/tiger
#
#
# The macro definition fill in some details or override some defaults from 
# other files.
#
OTTFLAGS=$(PCCFLAGS)
CLIBS= $(TTLIBS_QA) $(LDLIBS)
PRODUCT_LIBHOME=
MAKEFILE= $(MAKEROOT)/proc.mk
PROCPLSFLAGS= sqlcheck=full userid=$(USERID) 
PROCPPFLAGS= code=cpp sqlcheck=semantics userid=cmuser/cmuser000 $(CCPSYSINCLUDE)
USERID=$(DBUSER)/$(DBPASSWD)
NETWORKHOME=$(ORACLE_HOME)/network/
PLSQLHOME=$(ORACLE_HOME)/plsql/
INCLUDE=$(I_SYM). $(I_SYM)$(VBROKERDIR)/include $(I_SYM)$(VBROKERDIR)/include/stubs $(I_SYM)$(PRECOMPHOME)public $(I_SYM)$(RDBMSHOME)public $(I_SYM)$(RDBMSHOME)demo $(I_SYM)$(PLSQLHOME)public $(I_SYM)$(NETWORKHOME)public
I_SYM=-I
STATICPROLDLIBS=$(SCOREPT) $(SSCOREED) $(DEF_ON) $(LLIBCLIENT) $(LLIBSQL) $(TTLIBS)
PROLDLIBS=$(LLIBCLNTSH) $(STATICPROLDLIBS)
STATICCPPLDLIBS=$(SCOREPT) $(SSCOREED) $(DEF_ON) $(LLIBCLIENT) $(LLIBSQL) $(DEVTTLIBS)
CPPLDLIBS += -L$(LIBHOME) $(LLIBCLNTSH) $(STATICCPPLDLIBS) 
endif
