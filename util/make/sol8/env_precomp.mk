#
#   Copyright �� SQISoft Inc.
#   All Rights Reserved.
#
#   This source code is confidential and proprietary and may not be used
#   or distributed without the written permission of WINCC Inc.
#
#   Created by :  skpark
#   Modified by :
#   Last update : 2001/3/20
#   Comment :
#
#
#   env_precomp.mk
#
ifndef  _env_precomp_mk_
        _env_precomp_mk_ = ""
#
#
# Entering precomp.mk
# Entering /private/anataraj/srchome/805/buildtools/mkfile/prefix.mk

# Entering /private/anataraj/srchome/805/buildtools/mkfile/defs.mk

SHELL=/bin/sh

AS_EXT=s
LIB_EXT=a
OBJ_EXT=o
PLB_EXT=plb
SO_EXT=so
LOCK_EXT=lk
SQL_EXT=sql


AR=ar
AS=as
AWK=awk
CAT=cat
CC=cc
CD=cd
CHMOD=chmod
CP=cp
CPP=cpp
DATE=date
ECHO=echo
ECHON=echo -n
EXEC=exec
FIND=find
FOLLOW=-follow
NOLEAF=-noleaf
GREP=grep
KILL=kill
SLEEP=sleep
LD=ld
LMAKE=make
LN=ln
LNS=ln -s
MKDIR=mkdir
MV=mv
NM=nm
PERL=perl
RM=rm
RMF=rm -f
RMRF=rm -rf
SED=sed
SORT=sort
TOUCH=touch
XARGS=xargs
LS=ls

BINHOME=$(ORACLE_HOME)/bin/
LIBHOME=$(ORACLE_HOME)/lib/

ECHODO=$(BINHOME)echodo
GENSYSLIB=$(BINHOME)gensyslib
GENCLNTSH=$(BINHOME)genclntsh
GENNAFLG=$(BINHOME)gennaflg
GENAUTAB=$(BINHOME)genautab

ARCHIVE_OBJ=$(AR) r $@ $*.$(OBJ_EXT)
ARCREATE=$(AR) cr$(ARLOCAL)
ARDELETE=$(AR) d$(ARLOCAL)
AREXTRACT=$(AR) x 
ARPRINT=$(AR) t
ARREPLACE=$(AR) r 
DOAR=$(ARCREATE) $@ $? $(RANLIB)
MAKE=$(LMAKE)
QUIET=>/dev/null 2>&1
QUIETE=2>/dev/null
SILENT=@
CTSMAKEFLAGS=$(SILENT:@=--no-print-directory -s)

CFLAGS=$(GFLAG) $(OPTIMIZE) $(CDEBUG) $(CCFLAGS) $(QACCFLAGS) $(PFLAGS)\
	$(SHARED_CFLAG) $(USRFLAGS)
PFLAGS=$(INCLUDE) $(SPFLAGS) $(LPFLAGS)

LDCCOM=$(PURECMDS) $(CC) $(GFLAG) $(CCFLAGS) $(EXOSFLAGS) $(LDFLAGS)
LDFLAGS=-o $@ -L$(PRODLIBHOME) -L$(LIBHOME)
LDLIBS=$(EXPDLIBS) $(EXOSLIBS) $(SYSLIBS) $(EXSYSLIBS) $(MATHLIB) $(USRLIBS)
MATHLIB=-lm
PURECMDS=$(PURELINK) $(PURIFY) $(PURECOV) $(QUANTIFY)

SYSLIBLIST=$(LIBHOME)sysliblist
SYSLIBS=`$(CAT) $(SYSLIBLIST)`

NACCFLAGS=$(LIBHOME)naccflgs
#NALDFLAGS=$(LIBHOME)naldflgs
NACCFLAGSLIST=`$(CAT) $(NACCFLAGS)`
#NALDFLAGSLIST=`$(CAT) $(NALDFLAGS)`

TTLIBD=$(NETLIBD) $(LIBRDBMS_CLT) $(LIBMM) $(CORELIBD) $(LIBEPC) $(LIBCLNTSH)
#STATICTTLIBS=$(NAUTAB) $(NAETAB) $(NAEDHS) $(NALDFLAGSLIST) \
STATICTTLIBS=$(NAUTAB) $(NAETAB) $(NAEDHS) \
       $(NETLIBS) $(LLIBRDBMS_CLT) $(LLIBMM) $(CORELIBS)\
       $(NETLIBS) $(LLIBRDBMS_CLT) $(LIBPLS_CLT)\
       $(LLIBEPC) $(CORELIBS) $(LLIBRDBMS_CLT) $(CORELIBS) $(LDLIBS)
TTLIBS=$(LLIBCLNTSH) $(STATICTTLIBS)
DEVTTLIBS=$(TTLIBS)

STLIBS=$(SGL_ON) $(CONFIG) $(LLIBSERVER) $(LLIBPLSF) $(LLIBSLAX) \
       $(LIBGENERIC) $(LLIBCLIENT) $(LLIBMM) $(LLIBKNLOPT) $(LLIBPLSB)\
       $(LLIBSERVER) $(LLIBEXTP) $(NAUTAB) $(NAETAB) $(NAEDHS) \
       #$(NALDFLAGSLIST) $(NETLIBS) $(LLIBCLIENT) \
       $(NETLIBS) $(LLIBCLIENT) \
       $(LLIBPLSF) $(LLIBICX) $(LLIBOWSUTL) \
       $(NETLIBS) $(LLIBEPC) $(SDOLIBS) $(LLIBCORE) $(LLIBVSN) $(LLIBCOMMON)\
       $(LIBGENERIC) $(LLIBKNLOPT) $(NMLIBS) $(CARTLIBS) $(CORELIBS) $(LDLIBS)

# Exiting /private/anataraj/srchome/805/buildtools/mkfile/defs.mk
# Entering /private/anataraj/srchome/805/buildtools/mkfile/platform.mk

#by wegf
#PLATFORM=solaris

MOTIFHOME=/usr/dt/
OPENWINHOME=/usr/openwin/
GUILIBHOME=$(OPENWINHOME)lib/

CLUSTERDIR=/opt/SUNWcluster


GMAKE=make -r
LMAKE=/usr/ccs/bin/make
FIND=/usr/local/bin/find # gnu find!
PS=ps -feda

ARCHIVE=@`if echo ${LIB} | grep lib > /dev/null 2>&1;then $(ARCREATE) $(LIB) $? $(RANLIB); else echo ""; fi`

GROUP=/etc/group

LINK=ld -dy $(COMPOBJS)

LLIBTHREAD=

MOTIFLIBS=-L$(MOTIFHOME)lib -lXm -lgen -L$(GUILIBHOME) -R$(GUILIBHOME)\
	-lXt -lX11 -lw -lm
XLIBS=-L$(GUILIBHOME) -R$(GUILIBHOME) $(GUILIBHHOME)libXaw.so.4 -lXt -lXmu\
	-lXext -lX11 -lm

EXOSFLAGS=-L$(CLUSTERDIR)/lib -R$(CLUSTERDIR)/lib

CCVER=SC4.2
COMPOBJ=$(ORACLE_HOME)/lib/$(CCVER)
COMPOBJS=$(COMPOBJ)/crti.o $(COMPOBJ)/crt1.o $(COMPOBJ)/crtn.o

EXSYSLIBS= -R $(CLUSTERDIR)/lib:$(ORACLE_HOME)/lib \
	-Y P,$(LD_LIBRARY_PATH):$(CLUSTERDIR)/lib:/usr/ccs/lib:/usr/lib \
	-Qy -lc -laio
#DEVTTLIBS=$(NAUTAB) $(NAETAB) $(NAEDHS) $(NALDFLAGSLIST) \
DEVTTLIBS=$(NAUTAB) $(NAETAB) $(NAEDHS) \
       $(NETLIBS) $(LLIBRDBMS_CLT) $(LLIBMM) $(CORELIBS)\
       $(NETLIBS) $(LLIBRDBMS_CLT) $(LIBPLS_CLT)\
       $(LLIBEPC) $(CORELIBS) $(LLIBRDBMS_CLT) $(CORELIBS)\
       $(EXPDLIBS) $(EXOSLIBS) $(SYSLIBS) -lc -laio $(MATHLIB) $(USRLIBS)

OPTIMIZE2=-xO2
OPTIMIZE4=-xO4
OPTIMIZE=$(OPTIMIZE2)

ROFLAGS=-c -xMerge
SPFLAGS=-DSLMXMX_ENABLE -DSLTS_ENABLE -D_SVID_GETTOD

AR=/usr/ccs/bin/ar
AS=/usr/ccs/bin/as
CPP=$(CC) -E $(ANSI)
CHMOD=/bin/chmod
CHGRP=/bin/chgrp
CHOWN=/bin/chown

ASFLAGS=-P -K PIC
ASPFLAGS=-P $(PFLAGS)
NOKPIC_ASFLAGS=-P
ASRO=$(AS) $(ASROFLAGS) $(MAKERO) $<
ASROFLAGS=$(ASFLAGS)
CXC=-Xc
CXA=-Xa
ANSI=$(CXC)



NOKPIC_CCFLAGS= -Xa $(PROFILE) -xstrconst -xF $(XS) -mr \
        -xarch=v8 -xcache=16/32/1:1024/64/1 -xchip=ultra -D_REENTRANT
CCFLAGS= $(NOKPIC_CCFLAGS) -K PIC
FASTCCFLAGS= $(NOKPIC_CCFLAGS)

SHARED_LDFLAG=-G -L$(ORACLE_HOME)/lib -R$(ORACLE_HOME)/lib -o
SHARED_CFLAG=

# Exiting /private/anataraj/srchome/805/buildtools/mkfile/platform.mk
# Entering /private/anataraj/srchome/805/buildtools/mkfile/rules.mk
 
# Exiting /private/anataraj/srchome/805/buildtools/mkfile/rules.mk
# Exiting /private/anataraj/srchome/805/buildtools/mkfile/prefix.mk
# Entering /private/anataraj/srchome/805/network/exports.mk

LIBNETV2     = $(LIBHOME)libnetv2.$(LIB_EXT)
LLIBNETV2    = -lnetv2

LIBNTTCP     = $(LIBHOME)libnttcp.$(LIB_EXT)
LLIBNTTCP    = -lnttcp

LIBNTOD      = $(LIBHOME)libntod.$(LIB_EXT)
LLIBNTOD     = -lntod

LIBNTLU62    = $(LIBHOME)libntlu62.$(LIB_EXT)
LLIBNTLU62   = -lntlu62

LIBNTS       = $(LIBHOME)libnts.$(LIB_EXT)
LLIBNTS      = -lnts

LIBNETWORK   = $(LIBHOME)libnetwork.$(LIB_EXT)  	
LLIBNETWORK  = -lnetwork

LIBSQLNET    = $(LIBNETV2)   $(LIBNTTCP)  $(LIBNETWORK)
LLIBSQLNET   = $(LLIBNETV2) $(LLIBNTTCP) $(LLIBNETWORK)

LIBRPC       = $(LIBHOME)libncr.$(LIB_EXT)
LLIBRPC      = -lncr 

LIBTNSAPI    = $(NETWORKLIB)libtnsapi.$(LIB_EXT)
LLIBTNSAPI   = -ltnsapi

TNSLSNR	     = $(BINHOME)tnslsnr
LSNRCTL	     = $(BINHOME)lsnrctl
NAMES	     = $(BINHOME)names
NAMESCTL     = $(BINHOME)namesctl

OKINIT       = $(BINHOME)okinit
OKLIST       = $(BINHOME)oklist
OKDSTRY      = $(BINHOME)okdstry

NIGTAB       = $(LIBHOME)nigtab.$(OBJ_EXT)
NIGCON       = $(LIBHOME)nigcon.$(OBJ_EXT) 
NTCONTAB     = $(LIBHOME)ntcontab.$(OBJ_EXT)
MTS_NCR	     = $(LIBHOME)ncrstab.$(OBJ_EXT)

OSNTABST     = $(NETWORKLIB)osntabst.$(OBJ_EXT)
NNFGT        = $(NETWORKLIB)nnfgt.$(OBJ_EXT)

NAETAB       = $(LIBHOME)naeet.$(OBJ_EXT) $(LIBHOME)naect.$(OBJ_EXT)
NAEDHS	     = $(LIBHOME)naedhs.$(OBJ_EXT)
NAUTAB	     = $(LIBHOME)nautab.$(OBJ_EXT)

NETLIBS      = $(LLIBSQLNET) $(LLIBRPC) $(LLIBSQLNET)
NETLIBD      = $(LIBSQLNET) $(LIBRPC)

# Entering /private/anataraj/srchome/805/network/s_exports.mk

NATIVE =


# Exiting /private/anataraj/srchome/805/network/s_exports.mk
# Exiting /private/anataraj/srchome/805/network/exports.mk
# Entering /private/anataraj/srchome/805/oracore/exports.mk

COREHOME= $(ORACLE_HOME)/oracore/
ORACOREHOME= $(COREHOME)

LIBCORE= $(LIBHOME)libcore4.$(LIB_EXT)
LLIBCORE= -lcore4

S0MAIN= $(LIBHOME)s0main.o
SCOREPT= $(LIBHOME)scorept.o
SSCOREED= $(LIBHOME)sscoreed.o

COREPUBLIC=$(COREHOME)include/ $(COREHOME)public/
ORACOREPUBH=$(I_SYM)$(COREHOME)include $(I_SYM)$(COREHOME)public

CORELIBD=$(LIBNLSRTL) $(LIBCV6) $(LIBCORE) $(LIBNLSRTL) $(LIBCORE) $(LIBNLSRTL)
CORELIBS=$(LLIBNLSRTL) $(LLIBCV6) $(LLIBCORE) $(LLIBNLSRTL) $(LLIBCORE) $(LLIBNLSRTL)

# Entering /private/anataraj/srchome/805/oracore/s_exports.mk
 
# Exiting /private/anataraj/srchome/805/oracore/s_exports.mk

# Exiting /private/anataraj/srchome/805/oracore/exports.mk
# Entering /private/anataraj/srchome/805/nlsrtl/exports.mk

NLSRTLHOME= $(ORACLE_HOME)/nlsrtl/

ORA_NLS = $(ORACLE_HOME)/ocommon/nls/admin/data/
ORA_NLS33 = $(ORACLE_HOME)/ocommon/nls/admin/data/

LIBNLSRTL= $(LIBHOME)libnlsrtl3.$(LIB_EXT)
LLIBNLSRTL= -lnlsrtl3

NLSRTLPUBH = $(I_SYM)$(NLSRTLHOME)include
# Exiting /private/anataraj/srchome/805/nlsrtl/exports.mk
# Entering /private/anataraj/srchome/805/rdbms/exports.mk

LIBSERVER=$(LIBHOME)libserver.$(LIB_EXT)
LLIBSERVER=-lserver

LIBCLIENT=$(LIBHOME)libclient9.$(LIB_EXT)
LLIBCLIENT=-lclient9

LIBGENERIC=$(LIBHOME)libgeneric.$(LIB_EXT)
LLIBGENERIC=-lgeneric

LIBCOMMON=$(LIBHOME)libcommon.$(LIB_EXT)
LLIBCOMMON=-lcommon

LIBVSN=$(LIBHOME)libvsn.$(LIB_EXT)
LLIBVSN=-lvsn

LIBAGENT=$(LIBHOME)libagent.$(LIB_EXT)
LLIBAGENT=-lagent

DLMHOME=$(ORACLE_HOME)/odlm/

SKGXNS=$(RDBMSLIB)skgxns.$(OBJ_EXT)
SKGXND=$(RDBMSLIB)skgxnd.$(OBJ_EXT)
SKGXN=$(RDBMSLIB)skgxn.$(OBJ_EXT)

LIBDLM=$(SKGXN) $(LIBHOME)libudlm.$(LIB_EXT)
LLIBDLM=$(SKGXN) -ludlm

LIBNM=$(LIBDLM)
LLIBNM=$(LLIBDLM)
NMLIBLIST=$(RDBMSLIB)nmliblist
NMLIBS=`$(CAT) $(NMLIBLIST)`

LIBMM=$(LIBHOME)libmm.$(LIB_EXT)
LLIBMM=-lmm


LIBRDBMS_CLT=$(LIBCLIENT) $(LIBVSN) $(LIBCOMMON) $(LIBGENERIC)
LLIBRDBMS_CLT=$(LLIBCLIENT) $(LLIBVSN) $(LLIBCOMMON) $(LLIBGENERIC)

LIBCLNTSH=$(LIBHOME)libclntsh.$(SO_EXT)
LLIBCLNTSH=-lclntsh

CONFIG = $(RDBMSLIB)config.$(OBJ_EXT)

SDOLIBS = `if ${AR} tv ${ORACLE_HOME}/rdbms/lib/libknlopt.a | grep "kxmnsd.o" > /dev/null 2>&1 ; then echo " " ; else echo "-lmdknl -lmdhh"; fi`

DEF_ON= $(RDBMSLIB)kpudfo.$(OBJ_EXT)
DEF_OFF= $(RDBMSLIB)kpundf.$(OBJ_EXT)
DEF_OPT= $(RDBMSLIB)defopt.$(OBJ_EXT)

LIBSLAX=$(LIBHOME)libslax.a
LLIBSLAX=-lslax

LIBSQL=$(LIBHOME)libsql9.$(LIB_EXT)
LLIBSQL=-lsql9

LIBSVRM= $(LIBHOME)smalmain.$(OBJ_EXT) $(LIBHOME)libsvrmgrl.$(LIB_EXT) $(LIBHOME)libslpm.$(LIB_EXT)
LLIBSVRM= $(LIBHOME)smalmain.$(OBJ_EXT) -lsvrmgrl -lslpm

RDBMSHOME=$(ORACLE_HOME)/rdbms/
RDBMSLIB=$(RDBMSHOME)lib/
RDBMSADMIN=$(RDBMSHOME)admin/

# Entering /private/anataraj/srchome/805/rdbms/s_exports.mk

# Exiting /private/anataraj/srchome/805/rdbms/s_exports.mk
# Exiting /private/anataraj/srchome/805/rdbms/exports.mk
# Entering /private/anataraj/srchome/805/otrace/exports.mk

EPCLIBS=$(LLIBEPC)

LIBEPC=$(LIBHOME)libepc.$(LIB_EXT)
LLIBEPC=-lepc

LIBEPCPT=$(LIBHOME)libepcpt.$(LIB_EXT)
LLIBEPCPT=-lepcpt

LIBEPCFE=$(LIBHOME)libepcfe.$(LIB_EXT)
LLIBEPCFE=-lepcfe

# Entering /private/anataraj/srchome/805/otrace/s_exports.mk

# Exiting /private/anataraj/srchome/805/otrace/s_exports.mk
# Exiting /private/anataraj/srchome/805/otrace/exports.mk
# Entering /private/anataraj/srchome/805/plsql/exports.mk

LIBEXTP=$(LIBHOME)libextp.$(LIB_EXT)
LLIBEXTP=-lextp

WRAP=$(BINHOME)wrap

LIBPLSB=$(LIBHOME)libplsb.$(LIB_EXT)
LLIBPLSB=-lplsb
LIBPLSF=$(LIBHOME)libplsf.$(LIB_EXT)
LLIBPLSF=-lplsf
PLSQLLIBS=$(LIBPLSF) $(LIBPLSB)
LPLSQLLIBS=$(LLIBPLSF) $(LLIBPLSB)

LLIBPLSQL= $(LPLSQLLIBS) $(LLIBEXTP)

LIBICD=$(LIBHOME)libicd.$(LIB_EXT)
LLIBICD=-licd
LIBPSD=$(LIBHOME)libpsd.$(LIB_EXT)
LLIBPSD=-lpsd
LIBPSA=$(LIBHOME)libpsa.$(LIB_EXT)
LLIBPSA=-lpsa

# Entering /private/anataraj/srchome/805/plsql/s_exports.mk
 
# Exiting /private/anataraj/srchome/805/plsql/s_exports.mk
# Exiting /private/anataraj/srchome/805/plsql/exports.mk
 
# Entering /private/anataraj/srchome/805/precomp/exports.mk
TOP = $(ORACLE_HOME)/precomp
LIBSQL= $(LIBHOME)libsql9.$(LIB_EXT)
LLIBSQL= -lsql9
 
PRODUCT=precomp
PRECOMPHOME= $(ORACLE_HOME)/precomp/
PRODHOME=$(PRECOMPHOME)
PRECOMPADMIN=$(PRECOMPHOME)admin/
PRECOMPLIB=$(PRECOMPHOME)lib/
 
PRECOMPPUBH=$(I_SYM)$(PRECOMPHOME)public $(I_SYM)$(PRECOMPHOME)include
 
# Entering /private/anataraj/srchome/805/precomp/s_exports.mk
 
# Exiting /private/anataraj/srchome/805/precomp/s_exports.mk

# Exiting /private/anataraj/srchome/805/precomp/exports.mk

ADE_DEL_FILE=echo ""

PRECOMPLIB=$(PRECOMPHOME)lib/
PRECOMPMESG=$(PRECOMPHOME)mesg/
PRECOMPBIN=$(PRECOMPHOME)bin/
PRECOMPADMIN=$(PRECOMPHOME)admin/
PRECOMPINSTALL=$(PRECOMPHOME)install/
PRECOMPDEMO=$(PRECOMPHOME)demo/
PRECOMPADALIB=$(PRECOMPHOME)pubsrc/adalib

PRODLIBHOME=$(PRECOMPLIB)

 
 
LIBPAD=$(PRECOMPLIB)libpad.$(LIB_EXT)
LIBPC=$(PRECOMPLIB)libpc.$(LIB_EXT)
LIBPCC=$(PRECOMPLIB)libpcc.$(LIB_EXT)
LIBPCD=$(PRECOMPLIB)libpcd.$(LIB_EXT)
LIBPCO=$(PRECOMPLIB)libpco.$(LIB_EXT)
LIBPFO=$(PRECOMPLIB)libpfo.$(LIB_EXT)
LIBPGP=$(PRECOMPLIB)libpgp.$(LIB_EXT)
LIBPPL=$(PRECOMPLIB)libppl.$(LIB_EXT)
LIBPPA=$(PRECOMPLIB)libppa.$(LIB_EXT)
LIBPROC2=$(PRECOMPLIB)libproc2.$(LIB_EXT)
LIBMOD=$(PRECOMPLIB)libmod.$(LIB_EXT)
LIBSQL=$(LIBHOME)libsql9.$(LIB_EXT)
LIBPDC=$(PRECOMPLIB)libpdc.$(LIB_EXT)
LIBOTT=$(PRECOMPLIB)libots2c.$(LIB_EXT)
LIBPROCOB2=$(PRECOMPLIB)libprocob2.$(LIB_EXT)
LIBFIDL=$(PRECOMPLIB)libfidl.$(LIB_EXT)

PRECOMP_LIBS=$(LIBPAD) $(LIBPC) $(LIBPCC) $(LIBPCO) $(LIBPFO) \
             $(LIBPGP) $(LIBPPL) $(LIBPPA) $(LIBPCD) $(LIBPROC2) \
             $(LIBMOD) $(LIBSQL) $(LIBPDC) $(LIBOTT) $(LIBPROCOB2) $(LIBFIDL)
 
PC5DRV=pc5drv.$(OBJ_EXT)
PC0DRV=pc0drv.$(OBJ_EXT)
PC2DRV=pc2drv.$(OBJ_EXT)
PC1DRV=pc1drv.$(OBJ_EXT)
PC4DRV=pc4drv.$(OBJ_EXT)
PC3DRV=pc3drv.$(OBJ_EXT)
PMSCPR=pmscpr.$(OBJ_EXT)
PMSAPR=pmsapr.$(OBJ_EXT)
PROC2OBJS=main.$(OBJ_EXT) sspced.$(OBJ_EXT) spcpt.$(OBJ_EXT) pcprnt.$(OBJ_EXT)
MODOBJS=
OTTOBJS=o2m.$(OBJ_EXT) so2ed.$(OBJ_EXT) so2pt.$(OBJ_EXT)
PROCOB2OBJS=pcbd.$(OBJ_EXT) pcbrnt.$(OBJ_EXT)
FIDLOBJS=fdmain.$(OBJ_EXT)

ADAMAIN=$(PRECOMPLIB)$(PC5DRV)
COBMAIN=$(PRECOMPLIB)$(PC0DRV)
CMAIN=$(PRECOMPLIB)$(PC2DRV)
FORMAIN=$(PRECOMPLIB)$(PC1DRV)
PASMAIN=$(PRECOMPLIB)$(PC4DRV)
PLIMAIN=$(PRECOMPLIB)$(PC3DRV)
MODCMAIN=$(PRECOMPLIB)$(PMSCPR)
MODADAMAIN=$(PRECOMPLIB)$(PMSAPR)
MAINOBJS=$(PRECOMPLIB)main.$(OBJ_EXT) $(PRECOMPLIB)sspced.$(OBJ_EXT) \
	$(PRECOMPLIB)spcpt.$(OBJ_EXT) $(PRECOMPLIB)pcprnt.$(OBJ_EXT)
MODOBJS=
O2MAIN=$(PRECOMPLIB)o2m.$(OBJ_EXT) $(PRECOMPLIB)so2ed.$(OBJ_EXT) \
		$(PRECOMPLIB)so2pt.$(OBJ_EXT)
COB2MAIN=$(PRECOMPLIB)pcbd.$(OBJ_EXT) $(PRECOMPLIB)pcbrnt.$(OBJ_EXT)
FIDLMAIN=$(PRECOMPLIB)$(FIDLOBJS)
 
PDAOBJ=pda.$(OBJ_EXT)
PDCOBJ=pdc.$(OBJ_EXT)
PDBOBJ=pdb.$(OBJ_EXT)
PDFOBJ=pdf.$(OBJ_EXT)
PDPOBJ=pdp.$(OBJ_EXT)
PD1OBJ=pd1.$(OBJ_EXT)
PDSOBJ=pds.$(OBJ_EXT)
CODEGENOBJS=$(PDAOBJ) $(PDCOBJ) $(PDBOBJ) $(PDFOBJ) \
		$(PDPOBJ) $(PD1OBJ) $(PDSOBJ)

ADAPDC=$(PRECOMPLIB)$(PDAOBJ) $(PRECOMPLIB)$(PDSOBJ)
CPDC=$(PRECOMPLIB)$(PDCOBJ) $(PRECOMPLIB)$(PDSOBJ)
COBPDC=$(PRECOMPLIB)$(PDBOBJ) $(PRECOMPLIB)$(PDSOBJ)
FORPDC=$(PRECOMPLIB)$(PDFOBJ) $(PRECOMPLIB)$(PDSOBJ)
PASPDC=$(PRECOMPLIB)$(PDPOBJ) $(PRECOMPLIB)$(PDSOBJ)
PLIPDC=$(PRECOMPLIB)$(PD1OBJ) $(PRECOMPLIB)$(PDSOBJ)

PRECOMP_OBJFILES= $(ADAPDC) $(CPDC) $(COBPDC) $(FORPDC) $(PASPDC) \
		  $(PLIPDC) $(PRECOMPLIB)$(PDSOBJ) \
		  $(ADAMAIN) $(COBMAIN) $(CMAIN) $(FORMAIN) $(PASMAIN) \
		  $(PLIMAIN) $(MODCMAIN) $(MODADAMAIN) $(O2MAIN) \
		  $(MAINOBJS) $(COB2MAIN) $(FIDLMAIN)

STATICPROLDLIBS=$(LLIBCLIENT) $(LIBSQL) $(SCOREPT) $(SSCOREED) $(DEF_ON) $(DEVTTLIBS) $(LLIBTHREAD)
PROLDLIBS=$(LLIBCLNTSH) $(STATICPROLDLIBS)

PROADA=proada
PROCOB18=procob18
PROC16=proc16
PROFOR=profor
PROPAS=propas
PROPLI=propli
PROC=proc
OTT=ott
MODC=modc
MODADA=modada
PROCOB=procob
FIDL=fidl
RTSORA=rtsora

# Entering /private/anataraj/srchome/805/precomp/s_precomp.mk
 
ASFLAGS=-K PIC

LLAIO=-laio
LLIBTHREAD=-lthread

CCPSYSINCLUDE=sys_include='(/usr/local/include/g++-3,/usr/local/sparc-sun-solaris2.6/include,/usr/include)'
#CCPSYSINCLUDE=sys_include='(/opt/SUNWspro/SC5.0/include/CC4,/usr/include)'
CCP=CC

ADAPORT=verdix
ADACC=ada -e
ADALD=a.ld
ADACLEAN= a.rmlib -f

COB = cob
COBFLAGS=-C IBMCOMP -C NESTCALL -x
COBGNTFLAGS=-C IBMCOMP -C NESTCALL -u
GNT=.gnt
COBSQLINTF=$(PRECOMPLIB)cobsqlintf.o

FC=f77-4.0
FCFLAGS=-mt

# Exiting /private/anataraj/srchome/805/precomp/s_precomp.mk
# Exiting precomp.mk
# Entering /private/anataraj/srchome/805/buildtools/mkfile/libclntsh.mk
#$(LIBCLNTSH): \
#            ${ORACLE_HOME}/lib/libclient9.a ${ORACLE_HOME}/lib/libsql9.a \
#            ${ORACLE_HOME}/lib/libnetv2.a ${ORACLE_HOME}/lib/libnttcp.a \
#            ${ORACLE_HOME}/lib/libnetwork.a ${ORACLE_HOME}/lib/libncr.a \
#            ${ORACLE_HOME}/lib/libcommon.a ${ORACLE_HOME}/lib/libgeneric.a \
#            ${ORACLE_HOME}/lib/libmm.a ${ORACLE_HOME}/rdbms/lib/xaondy.o \
#            ${ORACLE_HOME}/lib/libnlsrtl3.a ${ORACLE_HOME}/lib/libcore4.a \
#            ${ORACLE_HOME}/lib/libepc.a
#	$(SILENT)$(ECHO) "Building client shared library libclntsh.so ..."
#	$(SILENT)$(ECHO) "Call script $$ORACLE_HOME/bin/genclntsh ..."
#	$(GENCLNTSH)
#	$(SILENT)$(ECHO) "Built $$ORACLE_HOME/lib/libclntsh.so ... DONE"
#
# Exiting /private/anataraj/srchome/805/buildtools/mkfile/libclntsh.mk

OCI_HDRS = -I$(ORACLE_HOME)/rdbms/include -I$(ORACLE_HOME)/rdbms/public -I$(ORACLE_HOME)/rdbms/demo -I$(ORACLE_HOME)/network/public -I$(ORACLE_HOME)/oracode/include -I$(ORACLE_HOME)/oracode/public
endif
