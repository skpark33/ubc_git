@echo off
echo ##################################################
echo ##################################################
echo mgrRun for UTV %1
echo ##################################################
echo ##################################################
call %COP_HOME%\util\win32\basicEnv.bat

set logroot=%PROJECT_HOME%\log
set UNXUTILS_BIN_DIR=%TRDPARTYROOT%\win32\UnxUtils\usr\local\wbin

REM ##################################################
REM ##############  set properties  ##################
REM ##################################################

if %CORBA_VENDOR% == vbroker goto L_prop_vbroker
if %CORBA_VENDOR% == tao goto L_prop_tao
goto L_prop_fini

:L_prop_vbroker
set properties=-DORBpropStorage=%CONFIGROOT%\vbroker\fs.properties
goto L_prop_fini

:L_prop_tao
set properties=-ORBInitRef NameService=corbaloc:iiop:%NAMESERVER_HOST%:%NAMESERVER_PORT%/NameService
set properties=%properties% -ORBInitRef InterfaceRepository=corbaloc:iiop:%IRSERVER_HOST%:%IRSERVER_PORT%/InterfaceRepository
set properties=%properties% -ORBSvcConf %CONFIGROOT%\tao\svc.conf
set app_properties=%properties% -ORBEndPoint iiop://%SERVER_HOST%:14020/portspan=20
REM set properties=%properties% -ORBEndpoint iiop://211.232.57.251:52100/portspan=100
goto L_prop_fini

:L_prop_fini

cd %LOGROOT%

REM ##################################################
REM ##################################################
REM #########                               ##########
REM #########       Process Registry        ##########
REM #########                               ##########
REM ##################################################
REM ##################################################

REM -=-=-=-=-[ application ]-=-=-=-=-

if %1 == all       	goto  L_all
if %1 == kill       	goto  L_kill

if %1 == smServer       goto  L_smServer
if %1 == amServer       goto  L_amServer
if %1 == fmServer       goto  L_fmServer
if %1 == rmServer       goto  L_rmServer
if %1 == bmServer       goto  L_bmServer
if %1 == cmServer       goto  L_cmServer
if %1 == shcClient1     goto  L_shcClient1
if %1 == shcClient2     goto  L_shcClient2

echo Invalid command : %1

REM ##################################################
REM ###################  help  #######################
REM ##################################################

echo mgrRun --------------------------
echo	    all
echo	    kill
echo        smServer
echo        amServer
echo        fmServer
echo        rmServer
echo        bmServer
echo        cmServer
echo        shcClient1, shcClient2

goto L_fini

REM ##################################################
REM ###################  all  ########################
REM ##################################################

:L_all

echo "smServer"
start "smServer" /min mgrRun smServer
sleep 1

echo "amServer"
start "amServer" /min mgrRun amServer
sleep 1

echo "fmServer"
start "fmServer" /min mgrRun fmServer
sleep 1

echo "rmServer"
start "rmServer" /min mgrRun rmServer
sleep 1

echo "bmServer"
start "bmServer" /min mgrRun bmServer
sleep 1

echo "cmServer"
start "cmServer" /min mgrRun cmServer
sleep 1


goto L_fini

REM ##################################################
REM #############          KILL            ###########
REM ##################################################

:L_kill

kill shcClient1
kill shcClient2
kill cmServer
kill bmServer
kill rmServer
kill fmServer
kill amServer
kill smServer

goto L_fini

REM ##################################################
REM ################### amServer #####################
REM ##################################################

:L_amServer

set binary=UTV_amServer.exe
set logname=amServer
set option= %app_properties% +kind AM +id 0 +domain MGR +noES +w
set debug=+Debug all
goto L_run

:L_fmServer

set binary=UTV_fmServer.exe
set logname=fmServer
set option=%app_properties% +kind FM +id 0 +domain MGR +w +noES
set debug=+Debug heartbeatMon hostInfoEventHandler killDownEventHandler startUpEventHandler 
goto L_run

:L_smServer

set binary=UTV_smServer.exe
set logname=smServer
set option=%app_properties% +kind SM +id 0 +domain MGR +w +noES +idleHour 0
set debug=+Debug all
goto L_run

:L_bmServer

set binary=UTV_bmServer.exe
set logname=bmServer
set option=%app_properties% +kind BM +id 0 +domain MGR +w +noES 
set debug=+Debug all
goto L_run

:L_rmServer

set binary=UTV_rmServer.exe
set logname=rmServer
set option=%app_properties% +kind RM +id 0 +domain MGR +w +noES 
set debug=
goto L_run

:L_cmServer

set binary=UTV_cmServer.exe
set logname=cmServer
set option=%app_properties% +kind CM +id 0 +domain MGR +w +noES 
set debug=+Debug all
goto L_run

:L_shcClient1

set binary=UTV_shcClient.exe
set logname=shcClient1
set option=%properties% +w
set debug=+Debug all
goto L_run

:L_shcClient2

set binary=UTV_shcClient.exe
set logname=shcClient2
set option=%properties% +id UBC2 +w
set debug=+Debug all
goto L_run


REM ##################################################
REM ##################################################
REM #########                               ##########
REM #########    RUN & FINI Area            ##########
REM #########                               ##########
REM ##################################################
REM ##################################################


REM ##################################################
REM #############          RUN             ###########
REM ##################################################

:L_run

cd %logroot%
set logfile=%logroot%\mgrRun_%logname%.log
if exist %logfile% del %logfile%
echo "%binary% %option% %debug% +noxlog"
%binary% %option% %debug% +noxlog | tee %logfile%

goto L_fini

:L_run_without_log

cd %logroot%
set logfile=%logroot%\mgrRun_%logname%.log
if exist %logfile% del %logfile%

echo "%binary% %option% %debug% +noxlog"
%binary% %option% %debug% +noxlog
goto L_fini

REM ##################################################
REM #############         FINI             ###########
REM ##################################################

:L_fini

if %1 == base    goto L_post
goto L_finifini  

:L_post
post_server_restart_event
goto L_finifini

:L_finifini
echo scrit end


