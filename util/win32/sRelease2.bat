@echo off

set build_cop=%COP_HOME%
set build_utv=%PROJECT_HOME%
REMset install_cop=D:\Project\COP2005\cop
set install_cop=Y:\COP2005\cop
REM set install_utv=D:\Project\ubc
set install_utv=Y:\ubc

set logroot=%PROJECT_HOME%\log
set logfile=%logroot%\sReleaseNote2.txt
echo %logfile%

IF NOT EXIST %logfile% (
    echo "" > %logfile%
)

echo ################################################## 
echo ################################################## 
echo ##############    Release for UTV   ############## 
echo ################################################## 
echo ################################################## 

DATE /T 
TIME /T 

echo ################################################## 
echo ##############     copy cop     ################## 
echo ################################################## 

echo XCOPY /D /Y %build_cop%\bin8\*.dll %install_cop%\bin8\ 
XCOPY /D /Y %build_cop%\bin8\*.dll %install_cop%\bin8\ 

echo XCOPY /D /Y %build_cop%\bin8\*.exe %install_cop%\bin8\ 
XCOPY /D /Y %build_cop%\bin8\*.exe %install_cop%\bin8\ 


echo XCOPY /D /Y /E %build_cop%\config\idl %install_cop%\config\idl 
XCOPY /D /Y /E %build_cop%\config\idl %install_cop%\config\idl 

echo XCOPY /D /Y /E %build_cop%\config\model %install_cop%\config\model 
XCOPY /D /Y /E %build_cop%\config\model %install_cop%\config\model 

echo ################################################## 
echo ##############     copy utv     ################## 
echo ################################################## 

echo XCOPY /D /Y %build_utv%\bin8\FileServiceWrap.dll %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\FileServiceWrap*.dll %install_utv%\bin8\ 

echo XCOPY /D /Y %build_utv%\bin8\UBC_hsrServer.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBC_hsrServer.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UBC_odServer.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBC_odServer.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UBC_mmServer.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBC_mmServer.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UBC_fmServer.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBC_fmServer.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UBC_lmServer.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBC_lmServer.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UBC_pmServer.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBC_pmServer.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UBC_dmServer.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBC_dmServer.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UTV_WOL.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UTV_WOL.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\XYNTService.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\XYNTService.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UTV_sample.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UTV_sample.exe %install_utv%\bin8\ 
REM echo XCOPY /D /Y %build_utv%\bin8\UBCVNC_Manager.exe %install_utv%\bin8\ 
REM XCOPY /D /Y %build_utv%\bin8\UBCVNC_Manager.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\repeater.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\repeater.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\FTPForwardingServer.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\FTPForwardingServer.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UBCAGTServer.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBCAGTServer.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\ServerInstall.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\ServerInstall.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\MenuRegister.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\MenuRegister.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\SocketCallGateway.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\SocketCallGateway.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\EventGateway.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\EventGateway.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\ContentsDistributor.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\ContentsDistributor.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UBCServerAgent.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBCServerAgent.exe %install_utv%\bin8\ 
echo XCOPY /D /Y %build_utv%\bin8\UBCServerAgentSvc.exe %install_utv%\bin8\ 
XCOPY /D /Y %build_utv%\bin8\UBCServerAgentSvc.exe %install_utv%\bin8\ 


echo XCOPY /D /Y %build_utv%\config\*.properties %install_utv%\config\ 
XCOPY /D /Y %build_utv%\config\*.properties %install_utv%\config\ 

echo XCOPY /D /Y /E %build_utv%\config\idl\SVR\*.idl %install_utv%\config\idl\SVR\ 
XCOPY /D /Y /E %build_utv%\config\idl\SVR\*.idl %install_utv%\config\idl\SVR\ 

echo XCOPY /D /Y /E %build_utv%\config\model\SVR\model.rule %install_utv%\config\model\SVR\ 
XCOPY /D /Y /E %build_utv%\config\model\SVR\model.rule %install_utv%\config\model\SVR\ 

REM echo XCOPY /D /Y /E %build_utv%\config\cli %install_utv%\config\cli 
REM XCOPY /D /Y /E %build_utv%\config\cli %install_utv%\config\cli 

echo XCOPY /D /Y /E %build_utv%\config\tao\svf.conf %install_utv%\config\tao\ 
XCOPY /D /Y /E %build_utv%\config\tao\svf.conf %install_utv%\config\tao\ 


echo XCOPY /D /Y %build_utv%\util\win32\*.bat %install_utv%\util\win32\ 
XCOPY /D /Y %build_utv%\util\win32\*.bat %install_utv%\util\win32\ 
move  /Y %install_utv%\util\win32\copRun2.bat %install_utv%\util\win32\copRun.bat 

echo XCOPY /D /Y %build_utv%\web\UBC_HTTP\UBC_HTTP\bin\UBC_HTTP.dll %install_utv%\web\UBC_HTTP\bin\ 
XCOPY /D /Y %build_utv%\web\UBC_HTTP\UBC_HTTP\bin\UBC_HTTP.dll %install_utv%\web\UBC_HTTP\bin\ 

echo XCOPY /D /Y /E %build_utv%\config\sql\alterTable.sql %install_utv%\config\sql\.
XCOPY /D /Y /E %build_utv%\config\sql\alterTable.sql %install_utv%\config\sql\. 

echo ################################################## 
echo #############         FINI             ########### 
echo ################################################## 

echo script end 
