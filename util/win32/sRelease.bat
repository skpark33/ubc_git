@echo off

set build_cop=%COP_HOME%
set build_utv=%PROJECT_HOME%
set install_cop=D:\Project\COP2005\cop
set install_utv=D:\Project\utv1

set logroot=%PROJECT_HOME%\log
set logfile=%logroot%\sReleaseNote.txt
echo %logfile%

IF NOT EXIST %logfile% (
    echo "" > %logfile%
)

echo ################################################## >> %logfile%
echo ################################################## >> %logfile%
echo ##############    Release for UTV   ############## >> %logfile%
echo ################################################## >> %logfile%
echo ################################################## >> %logfile%

DATE /T >> %logfile%
TIME /T >> %logfile%

echo ################################################## >> %logfile%
echo ##############     copy cop     ################## >> %logfile%
echo ################################################## >> %logfile%

echo XCOPY /D /Y %build_cop%\bin8\*.dll %install_cop%\bin8\ >> %logfile%
XCOPY /D /Y %build_cop%\bin8\*.dll %install_cop%\bin8\ >> %logfile%

echo XCOPY /D /Y %build_cop%\bin8\*.exe %install_cop%\bin8\ >> %logfile%
XCOPY /D /Y %build_cop%\bin8\*.exe %install_cop%\bin8\ >> %logfile%


echo XCOPY /D /Y /E %build_cop%\config\idl %install_cop%\config\idl >> %logfile%
XCOPY /D /Y /E %build_cop%\config\idl %install_cop%\config\idl >> %logfile%

echo XCOPY /D /Y /E %build_cop%\config\model %install_cop%\config\model >> %logfile%
XCOPY /D /Y /E %build_cop%\config\model %install_cop%\config\model >> %logfile%

echo ################################################## >> %logfile%
echo ##############     copy utv     ################## >> %logfile%
echo ################################################## >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\*.dll %install_utv%\bin8\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\*.dll %install_utv%\bin8\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\*.exe %install_utv%\bin8\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\*.exe %install_utv%\bin8\ >> %logfile%


echo XCOPY /D /Y %build_utv%\config\*.properties %install_utv%\config\ >> %logfile%
XCOPY /D /Y %build_utv%\config\*.properties %install_utv%\config\ >> %logfile%

echo XCOPY /D /Y /E %build_utv%\config\idl %install_utv%\config\idl >> %logfile%
XCOPY /D /Y /E %build_utv%\config\idl %install_utv%\config\idl >> %logfile%

echo XCOPY /D /Y /E %build_utv%\config\model %install_utv%\config\model >> %logfile%
XCOPY /D /Y /E %build_utv%\config\model %install_utv%\config\model >> %logfile%

echo XCOPY /D /Y /E %build_utv%\config\cli %install_utv%\config\cli >> %logfile%
XCOPY /D /Y /E %build_utv%\config\cli %install_utv%\config\cli >> %logfile%

echo XCOPY /D /Y /E %build_utv%\config\tao %install_utv%\config\tao >> %logfile%
XCOPY /D /Y /E %build_utv%\config\tao %install_utv%\config\tao >> %logfile%


echo XCOPY /D /Y %build_utv%\util\win32\*.bat %install_utv%\util\win32\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\*.bat %install_utv%\util\win32\ >> %logfile%

echo ################################################## >> %logfile%
echo #############         FINI             ########### >> %logfile%
echo ################################################## >> %logfile%

echo script end >> %logfile%
