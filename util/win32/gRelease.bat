@echo off

set build_cop=%COP_HOME%
set build_utv=%PROJECT_HOME%
set version=UTV1.0
set install_root=D:\SQIsoft
set install_exe=D:\SQIsoft\%version%\execute
set install_cfg=D:\SQIsoft\%version%\execute\config

set logroot=%PROJECT_HOME%\log
set logfile=%logroot%\gReleaseNote.txt
echo %logfile%

IF NOT EXIST %logfile% (
    echo "" > %logfile%
)

set versionfile=%logroot%\gVersionNote.txt
echo %versionfile%

IF EXIST %versionfile% (
    move /Y %versionfile% %versionfile%.old
)

DATE /T > %versionfile%
TIME /T >> %versionfile%

XCOPY /L /D /Y %build_cop%\bin8\*.dll %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_cop%\bin8\clInvoker.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_cop%\bin8\COP_timeSyncer.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_cop%\bin8\COP_scpClient.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_cop%\util\win32\kill.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\*.dll %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\SelfAutoUpdate.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UTV_agtServer.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UBCFlashProxy.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UBCStudio.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UTV_WOL.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UBCImporter.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UBCNetExporter.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UBCFirmwareView.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UTV_brwClient2.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UBCDeviceWizard.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\VNC_Proxy.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\MenuRegister.exe %install_exe%\ >> %versionfile%
REM XCOPY /L /D /Y %build_utv%\bin8\UTV_starter.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UTV_shcClient.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\Manager.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\bin8\UTVLogin.exe %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\config\*.properties %install_cfg%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\ubckill.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\softDrive.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\UBCStudioUpdate.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\UBCStudioUpdate.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\rebootSystem.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\shutdownSystem.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\afterInstall.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\makeLink.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\ServiceRun.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y %build_utv%\util\win32\ServiceKill.bat %install_exe%\ >> %versionfile%
XCOPY /L /D /Y "%build_utv%\util\win32\Reboot System.lnk" %install_exe%\ >> %versionfile%
XCOPY /L /D /Y "%build_utv%\util\win32\Shutdown System.lnk" %install_exe%\ >> %versionfile%
XCOPY /L /D /Y "%build_utv%\util\win32\UBC Tools.lnk" %install_exe%\ >> %versionfile%
XCOPY /L /D /Y "%build_utv%\util\win32\UBC Start.lnk" %install_exe%\ >> %versionfile%
XCOPY /L /D /Y "%build_utv%\util\win32\VNC Proxy.lnk" %install_exe%\ >> %versionfile%
XCOPY /L /D /Y "%build_utv%\util\win32\UBCReady.lnk" %install_exe%\ >> %versionfile%
XCOPY /L /D /Y /E %build_utv%\config\tao %install_cfg%\tao\ >> %versionfile%

REM if exist %logroot%\%versionfile%.temp del %versionfile%.temp
REM sed -e s/COP\.NAME_GROUP\.AGT.*$//g %install_cfg%\utv1.properties > %versionfile%.temp	
REM copy /Y %versionfile%.temp %versionfile%


echo ################################################## >> %logfile%
echo ################################################## >> %logfile%
echo ##############    Release for UTV   ############## >> %logfile%
echo ################################################## >> %logfile%
echo ################################################## >> %logfile%

DATE /T >> %logfile%
TIME /T >> %logfile%

echo ################################################## >> %logfile%
echo ##############     copy cop     ################## >> %logfile%
echo ################################################## >> %logfile%

echo XCOPY /D /Y %build_cop%\bin8\*.dll %install_exe%\ >> %logfile%
XCOPY /D /Y %build_cop%\bin8\*.dll %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_cop%\bin8\clInvoker.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_cop%\bin8\clInvoker.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_cop%\bin8\COP_timeSyncer.exe %install_exe%\ >> %logfile%
XCOPY /Y %build_cop%\bin8\COP_timeSyncer.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_cop%\bin8\COP_scpClient.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_cop%\bin8\COP_scpClient.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_cop%\util\win32\kill.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_cop%\util\win32\kill.exe %install_exe%\ >> %logfile%

echo ################################################## >> %logfile%
echo ##############     copy utv     ################## >> %logfile%
echo ################################################## >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\*.dll %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\*.dll %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\SelfAutoUpdate.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\SelfAutoUpdate.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UTV_agtServer.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UTV_agtServer.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UBCFlashProxy.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UBCFlashProxy.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UBCImporter.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UBCImporter.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UBCStudio.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UBCStudio.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UTV_WOL.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UTV_WOL.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UBCNetExporter.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UBCNetExporter.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UBCFirmwareView.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UBCFirmwareView.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UTV_brwClient2.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UTV_brwClient2.exe %install_exe%\ >> %logfile%

copy /y %install_exe%\UTV_brwClient2.exe %install_exe%\UTV_brwClient2_UBC1.exe
copy /y %install_exe%\UTV_brwClient2.exe %install_exe%\UTV_brwClient2_UBC2.exe
copy /y %install_exe%\UTV_brwClient2.exe %install_exe%\UTV_brwClient2_UBC3.exe

echo XCOPY /D /Y %build_utv%\bin8\UBCDeviceWizard.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UBCDeviceWizard.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\VNC_Proxy.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\VNC_Proxy.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\MenuRegister.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\MenuRegister.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UTV_starter.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UTV_starter.exe %install_exe%\ >> %logfile%
MOVE /Y %install_exe%\UTV_starter.exe  %install_exe%\UTV_starter.ex_>> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UTV_shcClient.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UTV_shcClient.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\Manager.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\Manager.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\bin8\UTVLogin.exe %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\bin8\UTVLogin.exe %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\config\*.properties %install_cfg%\ >> %logfile%
XCOPY /D /Y %build_utv%\config\*.properties %install_cfg%\ >> %logfile%

echo " Delete NAME_GRUP LINE from utv1.properties file"
if exist %install_cfg%\temp.properties del %install_cfg%\temp.properties
sed -e s/COP\.NAME_GROUP\.AGT.*$//g %install_cfg%\utv1.properties > %install_cfg%\temp.properties	
copy /Y %install_cfg%\temp.properties %install_cfg%\utv1.properties

echo XCOPY /D /Y %build_utv%\util\win32\ubckill.bat %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\ubckill.bat %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\util\win32\softDrive.bat %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\softDrive.bat %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\util\win32\uBCStudioUpdate.bat %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\uBCStudioUpdate.bat %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\util\win32\rebootSystem.bat %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\rebootSystem.bat %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\util\win32\shutdownSystem.bat %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\shutdownSystem.bat %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\util\win32\afterIntall.bat %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\afterIntall.bat %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\util\win32\makeLink.bat %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\makeLink.bat %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\util\win32\ServiceRun.bat %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\ServiceRun.bat %install_exe%\ >> %logfile%

echo XCOPY /D /Y %build_utv%\util\win32\ServiceKill.bat %install_exe%\ >> %logfile%
XCOPY /D /Y %build_utv%\util\win32\ServiceKill.bat %install_exe%\ >> %logfile%

echo XCOPY /D /Y "%build_utv%\util\win32\Reboot System.lnk" %install_exe%\ >> %logfile%
XCOPY /D /Y "%build_utv%\util\win32\Reboot System.lnk" %install_exe%\ >> %logfile%

echo XCOPY /D /Y "%build_utv%\util\win32\Shutdown System.lnk" %install_exe%\ >> %logfile%
XCOPY /D /Y "%build_utv%\util\win32\Shutdown System.lnk" %install_exe%\ >> %logfile%

echo XCOPY /D /Y "%build_utv%\util\win32\UBC Tools.lnk" %install_exe%\ >> %logfile%
XCOPY /D /Y "%build_utv%\util\win32\UBC Tools.lnk" %install_exe%\ >> %logfile%

echo XCOPY /D /Y "%build_utv%\util\win32\UBC Start.lnk" %install_exe%\ >> %logfile%
XCOPY /D /Y "%build_utv%\util\win32\UBC Start.lnk" %install_exe%\ >> %logfile%

echo XCOPY /D /Y "%build_utv%\util\win32\VNC Proxy.lnk" %install_exe%\ >> %logfile%
XCOPY /D /Y "%build_utv%\util\win32\VNC Proxy.lnk" %install_exe%\ >> %logfile%

echo XCOPY /D /Y "%build_utv%\util\win32\UBCReady.lnk" %install_exe%\ >> %logfile%
XCOPY /D /Y "%build_utv%\util\win32\UBCReady.lnk" %install_exe%\ >> %logfile%

echo XCOPY /D /Y /E %build_utv%\config\tao %install_cfg%\tao\ >> %logfile%
XCOPY /D /Y /E %build_utv%\config\tao %install_cfg%\tao\ >> %logfile%

echo ################################################## >> %logfile%
echo #############         FINI             ########### >> %logfile%
echo ################################################## >> %logfile%

echo script end >> %logfile%
