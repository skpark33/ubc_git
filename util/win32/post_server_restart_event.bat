@echo off

if not "%1"=="" (
set version_no=%1
goto next
)

echo  Naming 재기동시는 client 도 재기동되어야 하므로 재기동번호를 입력합니다
echo  재기동 번호는 날짜와 일런번호 두자리로 이루어집니다.
echo  예를 들어 2007년 1월 1일 두번쨰 재기동이라면
echo  07010102 를 입력하세요
echo  만약 client 를 재기동하지 않으려면 그냥 Enter를 치십시요.
set /p version_no=재기동번호를 입력하세요.[ex: 07010101] :  

:next

if "%version_no%" == "" goto END_1

cd %PROJECT_HOME%\util\win32

set vfile=%AUTO_UPDATE_VERSION_FILE%

if exist new_version.txt del new_version.txt

sed -e s/evt_restart,evt_restart,......../evt_restart,evt_restart,%version_no%/g  %vfile% > new_version.txt

copy /Y new_version.txt %vfile%

if exist new_version.txt del new_version.txt

echo 재기동 번호 : %version_no%  가 post 되었습니다.

:END_1

echo Bye...



