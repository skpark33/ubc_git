@echo off
echo ##################################################
echo ##################################################
echo copRun for UTV %1
echo ##################################################
echo ##################################################
REM if %1 == new  goto L_basic2
REM call %COP_HOME%\util\win32\basicEnv.bat
REM goto L_continue
REM :L_basic2
call %COP_HOME%\util\win32\basicEnv2.bat
REM :L_continue


set logroot=%PROJECT_HOME%\log
set UNXUTILS_BIN_DIR=%TRDPARTYROOT%\win32\UnxUtils\usr\local\wbin

REM ##################################################
REM ##############  set properties  ##################
REM ##################################################

if %CORBA_VENDOR% == vbroker goto L_prop_vbroker
if %CORBA_VENDOR% == tao goto L_prop_tao
goto L_prop_fini

:L_prop_vbroker
set properties=-DORBpropStorage=%CONFIGROOT%\vbroker\fs.properties
goto L_prop_fini

:L_prop_tao
set properties=-ORBInitRef NameService=corbaloc:iiop:%NAMESERVER_HOST%:%NAMESERVER_PORT%/NameService
set properties=%properties% -ORBInitRef InterfaceRepository=corbaloc:iiop:%IRSERVER_HOST%:%IRSERVER_PORT%/InterfaceRepository
set properties=%properties% -ORBSvcConf %CONFIGROOT%\tao\svc.conf
set app_properties=%properties% -ORBEndPoint iiop://%SERVER_HOST%:14120/portspan=29
REM set properties=%properties% -ORBEndpoint iiop://211.232.57.251:52100/portspan=100
goto L_prop_fini

:L_prop_fini

cd %LOGROOT%

REM ##################################################
REM ##################################################
REM #########                               ##########
REM #########       Process Registry        ##########
REM #########                               ##########
REM ##################################################
REM ##################################################

REM -=-=-=-=-[ group ]-=-=-=-=-

if %1 == base      goto  L_base
if %1 == new      goto  L_new
if %1 == mgr       goto  L_mgr
if %1 == notiTest      goto  L_notiTest


REM -=-=-=-=-[ initializer ]-=-=-=-=-

if %1 == init           goto  L_init
if %1 == kill           goto  L_kill
if %1 == killagt        goto  L_killagt

REM -=-=-=-=-[ base ]-=-=-=-=-

if %1 == osagent    goto  L_osagent
if %1 == nameserv   goto  L_nameserv
if %1 == irep           goto  L_irep
if %1 == ior2Naming goto  L_ior2Naming
if %1 == idl2ir         goto  L_idl2ir
if %1 == nbgen      goto  L_nbgen
if %1 == notiInfoGen    goto  L_notiInfoGen
if %1 == dummySm        goto  L_dummySm
if %1 == xMirServer     goto  L_xMirServer
if %1 == notifymanager1 goto  L_notifymanager1
if %1 == notifymanager2 goto  L_notifymanager2
if %1 == notifyserver1 goto  L_notifyserver1
if %1 == notiDbProxy goto  L_notiDbProxy
if %1 == tsServer goto  L_tsServer
if %1 == pfServer goto  L_pfServer

if %1 == IPServer goto  L_IPServer
if %1 == IPClient goto  L_IPClient
if %1 == LDPSimulator goto  L_LDPSimulator
if %1 == spServer goto  L_spServer
if %1 == spServer1 goto  L_spServer1
if %1 == spServer2 goto  L_spServer2
if %1 == spServer3 goto  L_spServer3
if %1 == SPManager goto  L_SPManager
if %1 == scpServer goto  L_scpServer
if %1 == scpServer1 goto  L_scpServer1
if %1 == scpServer2 goto  L_scpServer2
if %1 == scpServer3 goto  L_scpServer3
if %1 == scpClient1 goto  L_scpClient1
if %1 == scpClient2 goto  L_scpClient2
if %1 == shcServer goto  L_shcServer
if %1 == shcClient1 goto  L_shcClient1
if %1 == shcClient2 goto  L_shcClient2


REM -=-=-=-=-[ application ]-=-=-=-=-

if %1 == mgrServer       goto  L_mgrServer
if %1 == cmServer       goto  L_cmServer
if %1 == faultServer       goto  L_faultServer
if %1 == mgwServer       goto  L_mgwServer
if %1 == agtServer       goto  L_agtServer

REM -=-=-=-=-[ utility ]-=-=-=-=-

if %1 == cli            goto  L_cli
if %1 == cli_sp            goto  L_cli_sp
if %1 == enumGen    goto  L_enumGen
if %1 == xOidgen    goto  L_xOidgen
if %1 == xMirTree       goto  L_xMirTree
if %1 == dummyServer1   goto  L_dummyServer1
if %1 == dummyServer2   goto  L_dummyServer2
if %1 == timeSyncer goto  L_timeSyncer
if %1 == timeServer goto  L_timeServer
if %1 == scheduleSaver goto L_scheduleSaver

REM -=-=-=-=-[ new server ]-=-=-=-=-

if %1 == odServer goto L_odServer
if %1 == pmServer1 goto L_pmServer1
if %1 == pmServer2 goto L_pmServer2
if %1 == lmServer1 goto L_lmServer1
if %1 == lmServer2 goto L_lmServer2
if %1 == fmServer goto L_fmServer
if %1 == dmServer goto L_dmServer
if %1 == agtServer1 goto L_agtServer1
if %1 == agtServer2 goto L_agtServer2
if %1 == sample goto L_sample
if %1 == vncManager goto L_vncManager
if %1 == repeater goto L_repeater
if %1 == FTPForwardingServer goto  L_FTPForwardingServer
if %1 == SocketCallGateway goto  L_SocketCallGateway
if %1 == EventGateway goto  L_EventGateway
if %1 == mmServer goto  L_mmServer

echo Invalid command : %1

REM ##################################################
REM ###################  help  #######################
REM ##################################################

echo copRun --------------------------
echo    init
echo    start (+alone)
echo    kill
echo    base :
echo        osagent, nameserv
echo        irep, ior2Naming, idl2ir, nbgen, notiInfoGen
echo        dummySm, xMirServer, notifymanager1, notifymanager2
echo        IPServer, IPServer, spServer, SPManager, tsServer, scpServer
echo		shcServer
echo    mgr :
echo        mgrServer
echo        cmServer
echo        faultServer
echo        mgwServer
echo    agt :
echo        agtServer
echo    ==other==
echo        cli, xOidgen, xMirTree, dummyServer1, dummyServer2, timeServer, scheduleSaver

goto L_fini

REM ##################################################
REM ###################  base  #######################
REM ##################################################

:L_base

echo "osagent"
start "osagent" /min copRun osagent
sleep 5

echo "nameserv"
start "nameserv" /min copRun nameserv
sleep 5

echo "irep"
start "irep" /min copRun irep
sleep 10

REM echo "ior2Naming"
REM start "ior2Naming" /min copRun ior2Naming
REM sleep 10

echo "idl2ir"
start "idl2ir" /min copRun idl2ir
if %CORBA_VENDOR% == vbroker goto L_sleep_vbroker
if %CORBA_VENDOR% == tao goto L_sleep_tao
goto L_sleep_fini
:L_sleep_vbroker
sleep 10
goto L_sleep_fini
:L_sleep_tao
sleep 20
goto L_sleep_fini
:L_sleep_fini

echo "nbgen"
start "nbgen" /min copRun nbgen
sleep 10

echo "notiInfoGen"
start "notiInfoGen" /min copRun notiInfoGen
sleep 10

echo "dummySm"
start "dummySm" /min copRun dummySm
sleep 10

echo "xMirServer"
start "xMirServer" /min copRun xMirServer
sleep 10

echo "notifymanager1"
start "notifymanager1" /min copRun notifymanager1
sleep 10

REM sleep 10
REM echo "notifymanager2"
REM "notifymanager2" /min copRun notifymanager2

echo "notiDbProxy"
start "notiDbProxy" /min copRun notiDbProxy
sleep 10

REM echo "IPServer"
REM start "IPServer" /min copRun IPServer
REM sleep 10

REM echo "IPClient"
REM start "IPClient" /min copRun IPClient
REM sleep 5

echo "timeServer"
start "timeServer" /min copRun timeServer
sleep 5

REM echo "pfServer"
REM start "pfServer" /min copRun pfServer
REM sleep 5

REM echo "tsServer"
REM start "tsServer" /min copRun tsServer
REM sleep 5

REM #echo "LDPSimulator"
REM #start "LDPSimulator" /min copRun LDPSimulator
REM #sleep 10

echo "SPManager"
start "SPManager" /min copRun SPManager
sleep 5

echo "spServer1"
start "spServer1" /min copRun spServer1

echo "spServer2"
start "spServer2" /min copRun spServer2

echo "spServer3"
start "spServer3" /min copRun spServer3

echo "scpServer1"
start "scpServer1" /min copRun scpServer1

echo "scpServer2"
start "scpServer2" /min copRun scpServer2

echo "scpServer3"
start "scpServer3" /min copRun scpServer3

REM echo "shcServer"
REM start "shcServer" /min copRun shcServer
REM sleep 5 

REM echo "scpServer"
REM start "scpServer" /min copRun scpServer
REM sleep 10

echo "mgwServer"
start "mgwServer" /min copRun mgwServer
sleep 1

REM start "mgrRun all" /min mgrRun all
call mgrRun all

REM echo "mgrServer"
REM start "mgrServer" /min copRun mgrServer
REM sleep 1
REM 
REM echo "cmServer"
REM start "cmServer" /min copRun cmServer
REM sleep 1
REM 
REM echo "faultServer"
REM start "faultServer" /min copRun faultServer
REM sleep 1
REM 


goto L_fini

REM ##################################################
REM ###################  new  #######################
REM ##################################################

:L_new

echo "nameserv"
start "nameserv" /min copRun nameserv
sleep 5

echo "irep"
start "irep" /min copRun irep
sleep 10

echo "idl2ir"
start "idl2ir" /min copRun idl2ir
sleep 20

echo "nbgen"
start "nbgen" /min copRun nbgen
sleep 10

echo "notiInfoGen"
start "notiInfoGen" /min copRun notiInfoGen
sleep 10

echo "dummySm"
start "dummySm" /min copRun dummySm
sleep 10

echo "xMirServer"
start "xMirServer" /min copRun xMirServer
sleep 10

echo "notifymanager1"
start "notifymanager1" /min copRun notifymanager1
sleep 10

REM sleep 10
REM echo "notifymanager2"
REM "notifymanager2" /min copRun notifymanager2

echo "SPManager"
start "SPManager" /min copRun SPManager
sleep 5

echo "spServer1"
start "spServer1" /min copRun spServer1

echo "spServer2"
start "spServer2" /min copRun spServer2

echo "spServer3"
start "spServer3" /min copRun spServer3

REM echo "scpServer1"
REM start "scpServer1" /min copRun scpServer1

REM echo "scpServer2"
REM start "scpServer2" /min copRun scpServer2

REM echo "scpServer3"
REM start "scpServer3" /min copRun scpServer3

echo "timeServer"
start "timeServer" /min copRun timeServer
sleep 1

echo "pfServer"
start "pfServer" /min copRun pfServer
sleep 5

echo "odServer"
start "odServer" /min copRun odServer
sleep 1

echo "pmServer1"
start "pmServer1" /min copRun pmServer1
sleep 1

echo "pmServer2"
start "pmServer2" /min copRun pmServer2
sleep 1

echo "lmServer1"
start "lmServer1" /min copRun lmServer1
sleep 1

echo "lmServer2"
start "lmServer2" /min copRun lmServer2
sleep 1

echo "fmServer"
start "fmServer" /min copRun fmServer
sleep 1


echo "dmServer"
start "dmServer" /min copRun dmServer
sleep 1


echo "agtServer1"
start "agtServer1" /min copRun agtServer1
sleep 1

echo "agtServer2"
start "agtServer2" /min copRun agtServer2
sleep 1

REM echo "vncManager"
REM start "vncManager" /min copRun vncManager
REM sleep 1

echo "repeater"
start "repeater" /min copRun repeater
sleep 1

echo "FTPForwardingServer"
start "FTPForwardingServer" /min copRun FTPForwardingServer
sleep 1

echo "SocketCallGateway"
start "SocketCallGateway" /min copRun SocketCallGateway
sleep 1

echo "EventGateway"
start "EventGateway" /min copRun EventGateway
sleep 1

goto L_fini

REM ##################################################
REM #####################  mgr  #######################
REM ##################################################

:L_mgr
echo "mgrServer"
start "mgrServer" /min copRun mgrServer
sleep 1

echo "cmServer"
start "cmServer" /min copRun cmServer
sleep 1

echo "faultServer"
start "faultServer" /min copRun faultServer
sleep 1

:L_mgw
echo "mgwServer"
start "mgwServer" /min copRun mgwServer
sleep 1

goto L_fini

REM ##################################################
REM ###################  notiTest #######################
REM ##################################################

:L_notiTest

echo "osagent"
start "osagent" /min copRun osagent
sleep 5
echo "nameserv"
start "nameserv" /min copRun nameserv
sleep 5
echo "dummySm"
start "dummySm" /min copRun dummySm
sleep 10
REM echo "notifymanager1"
REM start "notifymanager1" /min copRun notifymanager1
REM sleep 10

goto L_fini

REM ##################################################
REM #############          KILL            ###########
REM ##################################################

:L_kill

call mgrRun kill

kill UBC_mmServer
kill SocketCallGateway
kill EventGateway
kill odServer
kill pmServer
kill lmServer
kill fmServer
kill dmServer
kill UBCAGTServer
kill mgwServer
kill faultServer
kill cmServer
kill mgrServer
kill tsServer
kill pfServer
kill dummyServer1
kill dummyServer2
kill clInvoker
kill notifymanager1
kill notifymanager2
kill notify
kill xMirServer
kill dummySm
kill notiInfoGen
kill nbgen
kill idl2ir
kill ior2Naming
kill notiDbProxy
kill COP_ipServer
kill COP_ipClient
kill LDPSimulator
kill timeServer
kill scpClient
kill scpServer
kill spManager
kill spManager
kill spServer

kill UTV_starter
kill UTV_agtServer
kill COP_timeSyncer
kill shcServer

kill osagent
kill irep
kill nameserv
kill IPServer
kill IPClient
kill SPManager
kill mgrRun

kill repeater
kill FTPForwardingServer

kill VNC_Manager
kill UBCVNC_Manager

if %CORBA_VENDOR% == vbroker goto L_kill_vbroker
if %CORBA_VENDOR% == tao goto L_kill_tao
goto L_fini

:L_killagt
kill UTV_starter
kill UTV_agtServer
kill COP_timeSyncer
goto L_fini

:L_kill_vbroker
kill irep
kill nameserv
kill osagent
goto L_fini

:L_kill_tao
kill IFR_Service
kill Naming_Service
goto L_fini

REM ##################################################
REM #################### init ########################
REM ##################################################

:L_init
echo "initSystem"
initSystem

goto L_fini

REM ##################################################
REM #################### osagent #####################
REM ##################################################

:L_osagent

if %CORBA_VENDOR% == vbroker goto L_vb_osagent
echo "osagent is only supported vbroker"
goto L_fini

:L_vb_osagent
osagent -a %NAMESERVER_HOST%
goto L_fini

REM ##################################################
REM ################## nameserv ######################
REM ##################################################

:L_nameserv

if %CORBA_VENDOR% == vbroker goto L_ns_vbroker
if %CORBA_VENDOR% == tao goto L_ns_tao
goto L_fini

:L_ns_vbroker
echo "nameserv"
nameserv.exe -VBJprop vbroker.se.iiop_tp.host=%NAMESERVER_HOST% -VBJprop vbroker.se.iiop_tp.scm.iiop_tp.listener.port=%NAMESERVER_PORT% -VBJprop vbroker.se.default.local.listener.shm=false
goto L_fini

:L_ns_tao
echo "Naming_Service"
Naming_Service -m 1 -o ns.ior -ORBEndPoint iiop://%NAMESERVER_HOST%:%NAMESERVER_PORT%
goto L_fini

REM ##################################################
REM #################### irep     ####################
REM ##################################################

:L_irep
cd  %HOMEDRIVE%%HOMEPATH%
del %IR_FILE%*
cd %LOGROOT%

if %CORBA_VENDOR% == vbroker goto L_ir_vbroker
if %CORBA_VENDOR% == tao goto L_ir_tao
goto L_fini

:L_ir_vbroker
echo "irep"
irep %IR_FILE% -VBJprop vbroker.se.iiop_tp.host=%NAMESERVER_HOST%
goto L_fini

:L_ir_tao
echo "IFR_Service"
IFR_Service -o ir.ior -ORBEndPoint iiop://%IRSERVER_HOST%:%IRSERVER_PORT%
goto L_fini

REM ##################################################
REM #################### ior2Naming ##################
REM ##################################################

:L_ior2Naming
echo "ior2Naming"
ior2Naming.exe +name IR=Repository +ir %properties%
goto L_fini

REM ##################################################
REM #################### idl2ir  #####################
REM ##################################################

:L_idl2ir
echo "idl2ir"
set all_idl=%PROJECT_HOME%\log\all.idl
if exist %all_idl% del %all_idl%

set idl_list=%PROJECT_HOME%\log\idl.list
if exist %idl_list% del %idl_list%

set sed_script=%COP_HOME%\util\win32\idl_script.sed%

cd %COP_HOME%\config\idl\caf
%UNXUTILS_BIN_DIR%\find . -name "*.idl" > %idl_list%
%UNXUTILS_BIN_DIR%\sed -f %sed_script% %idl_list%> %all_idl%
del %idl_list%

cd %COP_HOME%\config\idl\cas
%UNXUTILS_BIN_DIR%\find . -name "*.idl" > %idl_list%
%UNXUTILS_BIN_DIR%\sed -f %sed_script% %idl_list%>> %all_idl%
del %idl_list%

cd %COP_HOME%\config\idl\cig
%UNXUTILS_BIN_DIR%\find . -name "*.idl" > %idl_list%
%UNXUTILS_BIN_DIR%\sed -f %sed_script% %idl_list%>> %all_idl%
del %idl_list%

REM cd %COP_HOME%\config\idl\mo
REM %UNXUTILS_BIN_DIR%\find . -name "*.idl" > %idl_list%
REM %UNXUTILS_BIN_DIR%\sed -f %sed_script% %idl_list%>> %all_idl%
REM del %idl_list%

cd %PROJECT_HOME%\config\idl
%UNXUTILS_BIN_DIR%\find . -name "*.idl" > %idl_list%
%UNXUTILS_BIN_DIR%\sed -f %sed_script% %idl_list%>> %all_idl%
del %idl_list%

cd %LOGROOT%
type all.idl

if %CORBA_VENDOR% == vbroker goto L_idl2ir_vbroker
if %CORBA_VENDOR% == tao goto L_idl2ir_tao
goto L_fini

:L_idl2ir_vbroker
idl2ir -irep %IR_FILE% -I. -I%PROJECT_HOME%\config\idl -I%COP_HOME%\config\idl\caf -I%COP_HOME%\config\idl\cas -I%COP_HOME%\config\idl\cig -I%COP_HOME%\config\idl\mo -I%VBROKER_HOME%\idl %PROJECT_HOME%\log\all.idl
goto L_fini

:L_idl2ir_tao
tao_ifr %properties% -D_TAO -I. -I%PROJECT_HOME%\config\idl -I%COP_HOME%\config\idl\caf -I%COP_HOME%\config\idl\cas -I%COP_HOME%\config\idl\cig -I%COP_HOME%\config\idl\mo -I%TAO_ROOT% %PROJECT_HOME%\log\all.idl
goto L_fini

REM ##################################################
REM #################### nbgen  ######################
REM ##################################################

:L_nbgen

set binary=nbgen.exe
set logname=nbgen
set option=%properties% +noidl +id NBGEN
set debug=+Debug all
goto L_run

REM ##################################################
REM #################### notiInfoGen  ################
REM ##################################################

:L_notiInfoGen

set binary=notiInfoGen.exe
set logname=notiInfoGen
set option=%properties% +id NOTIIFNOGEN
set debug=+Debug all
goto L_run

REM ##################################################
REM #################### dummySm  ####################
REM ##################################################

:L_dummySm

set binary=dummySm.exe
set logname=dummySm
set option= %app_properties% +kind SM +id dummySm +domain SM +w
set debug=+Debug all
goto L_run

REM ##################################################
REM ################## xMirServer  ###################
REM ##################################################

:L_xMirServer

set binary=xMirServer.exe
set logname=xMirServer
set option= %app_properties% +kind MIR +id MIR +domain GW +recache +module ALL +mirId ManagedElement +w
set debug=+Debug all
goto L_run

REM ##################################################
REM ###################  notifymanager1  #############
REM ##################################################

:L_notifymanager1

set binary=notifymanager.exe
set logname=notifymanager1
set option= +kind NotiManager +id MGR01 +domain FW +name FW=FW/Manager=M01 +Mgr M01 +prefilter %app_properties% +logPolicy NOTIFY_DEBUG_POLICY +noDS +w

if %CORBA_VENDOR% == vbroker goto L_nm1_vbroker
if %CORBA_VENDOR% == tao goto L_nm1_tao
goto L_nm1_fini

:L_nm1_vbroker
set option= %option% +prop %CONFIGROOT%\vbroker\fs.properties -Dvbroker.se.iiop_tp.scm.iiop_tp.listener.port=%NOTIMGR1_PORT%
goto L_nm1_fini

:L_nm1_tao
set option= %option% -ORBEndpoint iiop://%SERVER_HOST%:%NOTIMGR1_PORT%
goto L_nm1_fini

:L_nm1_fini
set debug=+Debug all
goto L_run

REM ##################################################
REM ################### notifymanager2  ##############
REM ##################################################

:L_notifymanager2

set binary=notifymanager.exe
set logname=notifymanager2
set option= +kind NotiManager +id MGR02 +domain FW +name FW=FW/Manager=M02 +Mgr M02 +prefilter %app_properties% +logPolicy NOTIFY_DEBUG_POLICY +noDS +w

if %CORBA_VENDOR% == vbroker goto L_nm2_vbroker
if %CORBA_VENDOR% == tao goto L_nm2_tao
goto L_nm2_fini

:L_nm2_vbroker
set option= %option% +prop %CONFIGROOT%\vbroker\fs.properties -Dvbroker.se.iiop_tp.scm.iiop_tp.listener.port=%NOTIMGR2_PORT%
goto L_nm2_fini

:L_nm2_tao
set option= %option% -ORBEndpoint iiop://%SERVER_HOST%:%NOTIMGR2_PORT%
goto L_nm2_fini

:L_nm2_fini
set debug=
goto L_run

REM ##################################################
REM ################### notifyserver1  ##############
REM ##################################################

:L_notifyserver1

set binary=notifyserver.exe
set logname=DEFAULT_1
set option= +id DEFAULT_1 +name FW=FW/Notification=DEFAULT_1 +Mgr M01 +prefilter %app_properties% +logPolicy NOTIFY_DEBUG_POLICY +noDS +w

if %CORBA_VENDOR% == vbroker goto L_noti1_vbroker
if %CORBA_VENDOR% == tao goto L_noti1_tao
goto L_noti1_fini

:L_noti1_vbroker
set option= %option% -Dvbroker.se.iiop_tp.scm.iiop_tp.listener.port=%NOTI_DF_PRI_PORT%
goto L_noti1_fini

:L_noti1_tao
set option= %option% -ORBEndpoint iiop://%SERVER_HOST%:%NOTI_DF_PRI_PORT%
set option= %option% -ORBDebug -ORBDebugLevel 10
goto L_noti1_fini

:L_noti1_fini
set debug=
goto L_run

REM ##################################################
REM ################### notiDbProxy ##############
REM ##################################################

:L_notiDbProxy

set binary=notiDbProxy.exe
set logname=NotiDbProxy
set option= %app_properties% +kind NotiDbProxy +id PROXY_SVR +db CM_DB +w
set debug=+Debug all
goto L_run

REM ##################################################
REM ################### IPServer ##############
REM ##################################################

:L_IPServer

set binary=COP_ipServer.exe
set logname=IPServer
set option=50000
set debug=
goto L_run_without_log

REM ##################################################
REM ################### IPClient ##############
REM ##################################################

:L_IPClient

set binary=COP_ipClient.exe
set logname=IPClient
set option=
set debug=+w +addr 211.232.57.184 +port 50000
goto L_run

REM ##################################################
REM ################### LDPSimulator ##############
REM ##################################################

:L_LDPSimulator

set binary=LDPSimulator.exe
set logname=LDPSimulator
set option=5352
set debug=+Debug all
goto L_run

REM ##################################################
REM ################### spServer ##############
REM ##################################################

:L_spServer

set binary=COP_spServer.exe
set logname=spServer
set option= %app_properties% +w 
set debug=+Debug all +logPolicy NOTIFY_DEBUG_POLICY
goto L_run

REM ##################################################
REM ################### spServer1 ##############
REM ##################################################

:L_spServer1

set binary=COP_spServer.exe
set logname=spServer1
set option= +kind SP +id SP01 +spip %NAMESERVER_HOST% +spport 14151 +noDS +noES
set option= %option% %app_properties% +w 
set debug=+Debug off
goto L_run

REM ##################################################
REM ################### spServer2 ##############
REM ##################################################

:L_spServer2

set binary=COP_spServer.exe
set logname=spServer2
set option= +kind SP +id SP02 +spip %NAMESERVER_HOST% +spport 14152 +noDS +noES
set option= %option% %app_properties% +w 
set debug=+Debug off
goto L_run


REM ##################################################
REM ################### spServer3 ##############
REM ##################################################

:L_spServer3

set binary=COP_spServer.exe
set logname=spServer3
set option= +kind SP +id SP03 +spip %NAMESERVER_HOST% +spport 14153 +noDS +noES
set option= %option% %app_properties% +w 
set debug=+Debug off
goto L_run

REM ##################################################
REM ##################  scpServer1 ###############
REM ##################################################

:L_scpServer1

set binary=COP_scpServer.exe
set logname=scpServer1 
set option= +kind SCP +id SCP01 +domain SCP +readPort 14160 +writePort 14161 +noDS +noES
set option= %option% %app_properties% +w 
set debug=+Debug off
goto L_run

REM ##################################################
REM ##################  scpServer2 ###############
REM ##################################################

:L_scpServer2

set binary=COP_scpServer.exe
set logname=scpServer2 
set option= +kind SCP +id SCP02 +domain SCP +readPort 14162 +writePort 14163 +noDS +noES
set option= %option% %app_properties% +w 
set debug=+Debug off
goto L_run

REM ##################################################
REM ##################  scpServer3 ###############
REM ##################################################

:L_scpServer3

set binary=COP_scpServer.exe
set logname=scpServer3 
set option= +kind SCP +id SCP03 +domain SCP +readPort 14164 +writePort 14165 +noDS +noES
set option= %option% %app_properties% +w 
set debug=+Debug off
goto L_run

REM ##################################################
REM ################### SPManager ##############
REM ##################################################

:L_SPManager

set binary=COP_spManager.exe
set logname=SPManager
set option= %app_properties% +w +id M01 +kind SPM +pretend +noDS
set debug=+Debug all
goto L_run

REM ##################################################
REM ################### TimeSyncer ##############
REM ##################################################

:L_timeSyncer

set binary=COP_timeSyncer.exe
set logname=TimeSyncer
set option=
set debug=+log
goto L_run

REM ##################################################
REM ################### TimeServer ##############
REM ##################################################

:L_timeServer

set binary=COP_timeServer.exe
set logname=TimeServer
set option=14106
set debug=
goto L_run

REM ##################################################
REM #####################  cli #######################
REM ##################################################

:L_cli

set binary=clInvoker.exe
set logname=clInvoker
set option= %properties% +post +w +noES +project ubc
set debug=Debug all
goto L_run_without_log

REM ##################################################
REM #####################  cli_sp #######################
REM ##################################################

:L_cli_sp

set binary=clInvoker.exe
set logname=clInvoker
set option= %properties% +post +w +noES +socketproxy
set debug=Debug all
goto L_run_without_log

REM ##################################################
REM #################### enumGen  ####################
REM ##################################################

:L_enumGen

set binary=enumGen.exe
set logname=enumGen
set option=+psType ORA9 +psName ST_DB
set debug=+Debug all
goto L_run

REM ##################################################
REM #################### xOidgen  ####################
REM ##################################################

:L_xOidgen

set binary=xOidgen.exe
set logname=xOidgen
set option=+oidRoot %PROJECT_HOME%\config\snmp +module IMGC IMPS IMSR MMC MRC TIIS MSS PNA TCS
set debug=+Debug all
goto L_run


REM ##################################################
REM #################### scheduleSaver ####################
REM ##################################################

:L_scheduleSaver

set binary=UTV_scheduleSaver.exe
set logname=scheduleSaver
set option=
set debug=+Debug all
goto L_run

REM ##################################################
REM ###################  xMirTree ####################
REM ##################################################

:L_xMirTree

set binary=xMirTree.exe
set logname=xMirTree
set option= %properties%
set debug=
goto L_run

REM ##################################################
REM ##################  dummyServer1 #################
REM ##################################################

:L_dummyServer1

set binary=dummyServer.exe
set logname=dummyServer1
set option= %properties% +id Dummy1 +kind Dummy +domain SM +w
set debug=+Debug all
goto L_run

REM ##################################################
REM ##################  dummyServer2 #################
REM ##################################################

:L_dummyServer2

set binary=dummyServer.exe
set logname=dummyServer2pserver:copdep@211.232.57.251:80:/nms/users/copcvs/VOB
set option= %properties% +id Dummy2 +kind Dummy +domain SM +w
set debug=+Debug all
goto L_run


REM ##################################################
REM ##################  tsServer #####################
REM ##################################################

:L_tsServer

set binary=tsServer.exe
set logname=tsServer
set option=%app_properties% +kind TS +id TS +domain FW +psType MYSQL +psName CM_DB +w
set debug=
goto L_run


REM ##################################################
REM ##################  pfServer #####################
REM ##################################################

:L_pfServer

set binary=pfServer.exe
set logname=pfServer
set option=%app_properties% +kind PF +id PF +domain FW +w
set debug=
goto L_run


REM ##################################################
REM ##################  socket servers ###############
REM ##################################################

:L_scpServer

set binary=COP_scpServer.exe
set logname=scpServer
set option=%app_properties% +kind SCP +id SCP1 +domain SCP +w +noES
set debug=+Debug all
goto L_run

:L_scpClient1

set binary=COP_scpClient.exe
set logname=scpClient1
set option=%app_properties% +entities AGT=UTV-SRV3 +domain SCP +w +noES +project ubc
set debug=+Debug all
goto L_run

:L_scpClient2

set binary=COP_scpClient.exe
set logname=scpClient2
set option=%app_properties% +entities AGT=AGT1 +domain SCP +w +noES +project ubc
set debug=+Debug all
goto L_run


:L_shcServer

set binary=UTV_shcServer.exe
set logname=shcServer
set option=%app_properties% +kind SHC +id SHC1 +domain SHC +w +noES
set debug=+Debug all
goto L_run

:L_shcClient1

set binary=UTV_shcClient.exe
set logname=shcClient1
set option=+w
set debug=+Debug all
goto L_run

:L_shcClient2

set binary=UTV_shcClient.exe
set logname=shcClient2
set option=+id UBC2 +w
set debug=+Debug all
goto L_run

REM ##################################################
REM ##################  mgrServer  ###################
REM ##################################################

:L_mgrServer

set binary=UTV_mgrServer.exe
set logname=mgrServer
set option=%app_properties% +kind MGR +id MGR1 +domain MGR +w +noES
REM set debug=+Debug all
set debug=+Debug scheduleManager
goto L_run

:L_cmServer

set binary=UTV_cmServer.exe
set logname=cmServer
set option=%app_properties% +kind PING +id MGR1 +domain MGR +w +noES +idleHour 14
set debug=+Debug powerCon utvUtil mgetConfig
goto L_run

:L_faultServer

set binary=UTV_faultServer.exe
set logname=faultServer
set option=%app_properties% +kind FLT +id MGR1 +domain MGR +w +noES
set debug=+Debug heartbeatEventHandler hostInfoEventHandler killDownEventHandler startUpEventHandler 
goto L_run

:L_mgwServer

set binary=UTV_mgwServer.exe
set logname=mgwServer
REM set option=%app_properties% +kind MGW +id 1 +domain MGR +w +noES +period 10
set option=%app_properties% +kind MGW +id isProgressed +domain MGR +w +noES +period 10
set debug=+Debug mgwMon
goto L_run

REM ##################################################
REM ##################  agtServer  ###################
REM ##################################################

:L_agtServer

set binary=UTV_agtServer.exe
set logname=agtServer
set option=%app_properties% +kind AGT +id AGT1 +domain AGENT +noES +w +appId AGENT
set debug=+Debug all
goto L_run


REM ##################################################
REM ##################  new Server  ###################
REM ##################################################

:L_odServer

set binary=UBC_odServer.exe
set logname=odServer
set option=%app_properties% +kind OD +id OD +domain OD +noES +w +appId OD
set debug=+Debug all
goto L_run

:L_pmServer1

set binary=UBC_pmServer.exe
set logname=pmServer1
set option=%app_properties% +kind PM +id 1 +domain PM +noES +w +appId PM
set debug=+Debug all
goto L_run

:L_pmServer2

set binary=UBC_pmServer.exe
set logname=pmServer2
set option=%app_properties% +kind PM +id 2 +domain PM +noES +w +appId PM
set debug=+Debug all
goto L_run

:L_fmServer

set binary=UBC_fmServer.exe
set logname=fmServer
set option=%app_properties% +kind FM +id 1 +domain FM +noES +w +appId FM
set debug=+Debug all
goto L_run

:L_lmServer1

set binary=UBC_lmServer.exe
set logname=lmServer1
set option=%app_properties% +kind LM +id 1 +domain LM +noES +w +appId LM
set debug=+Debug all
goto L_run

:L_lmServer2

set binary=UBC_lmServer.exe
set logname=lmServer2
set option=%app_properties% +kind LM +id 2 +domain LM +noES +w +appId LM
set debug=+Debug all
goto L_run

:L_dmServer

set binary=UBC_dmServer.exe
set logname=dmServer
set option=%app_properties% +screenShot 5 +kind DM +id 1 +domain DM +noES +w +appId DM
set debug=+Debug all
goto L_run


:L_agtServer1

set binary=UBCAGTServer.exe
set logname=agtServer1
set option=%app_properties% +kind AGTServer +id 1 +domain AGENT +noES +w +appId AGENTSERVER
set debug=+Debug all
goto L_run

:L_agtServer2

set binary=UBCAGTServer.exe
set logname=agtServer2
set option=%app_properties% +kind AGTServer +id 2 +domain AGENT +noES +w +appId AGENTSERVER
set debug=+Debug all
goto L_run

:L_sample

set binary=UTV_sample.exe
set logname=UTV_sample
set option=%properties% +w +noxlog cli +ubc +server
set debug=+Debug all
goto L_run

:L_vncManager

cd D:\Project\ubc\bin8
echo "D:\Project\ubc\bin8\UBCVNC_Manager.exe"
start D:\Project\ubc\bin8\UBCVNC_Manager.exe
cd D:\Project\ubc\log
goto L_fini

:L_repeater

cd D:\Project\ubc\bin8
echo "D:\Project\ubc\bin8\repeater.exe"
start D:\Project\ubc\bin8\repeater.exe
cd D:\Project\ubc\log
goto L_fini

:L_FTPForwardingServer

cd D:\Project\ubc\bin8
echo "D:\Project\ubc\bin8\FTPForwardingServer.exe"
start D:\Project\ubc\bin8\FTPForwardingServer.exe
cd D:\Project\ubc\log
goto L_fini

:L_SocketCallGateway

cd D:\Project\ubc\bin8
echo "D:\Project\ubc\bin8\SocketCallGateway.exe" %app_properties%
start D:\Project\ubc\bin8\SocketCallGateway.exe %app_properties%
cd D:\Project\ubc\log
goto L_fini

:L_EventGateway

cd D:\Project\ubc\bin8
echo "D:\Project\ubc\bin8\EventGateway.exe" %app_properties%
start D:\Project\ubc\bin8\EventGateway.exe %app_properties%
cd D:\Project\ubc\log
goto L_fini

:L_mmServer

cd D:\Project\ubc\bin8
echo "D:\Project\ubc\bin8\UBC_mmServer.exe" +w +Debug all %app_properties% 
start D:\Project\ubc\bin8\UBC_mmServer.exe +w +Debug all %app_properties%   +id MM +kind MM
cd D:\Project\ubc\log
goto L_fini

REM ##################################################
REM ##################################################
REM #########                               ##########
REM #########    RUN & FINI Area            ##########
REM #########                               ##########
REM ##################################################
REM ##################################################


REM ##################################################
REM #############          RUN             ###########
REM ##################################################

:L_run

cd %logroot%
set logfile=%logroot%\copRun_%logname%.log
if exist %logfile% del %logfile%
echo "%binary% %option% %debug% +noxlog"
%binary% %option% %debug% +noxlog | tee %logfile%

goto L_fini

:L_run_without_log

cd %logroot%
set logfile=%logroot%\copRun_%logname%.log
if exist %logfile% del %logfile%

echo "%binary% %option% %debug% +noxlog"
%binary% %option% %debug% +noxlog
goto L_fini

REM ##################################################
REM #############         FINI             ###########
REM ##################################################

:L_fini

if %1 == base    goto L_post
goto L_finifini  

:L_post
post_server_restart_event
goto L_finifini

:L_finifini
echo scrit end
REM exit
