#!/bin/csh -f

if ($#argv < 2) then 
	echo 'sed.csh <convert_expression> <filter_expression...>'
	echo 'ex) % sed.csh aaa/bbb "*.[Ch]"'
	exit 1
endif

set cnv_exp = "$argv[1]"
set filter_exp = "$argv[2-$#argv]"

echo "---------------------------------"
echo "${cnv_exp}, ${filter_exp}"
echo "---------------------------------"

set now = `date +'%Y%m%d%H%M%S'`
set sedscript = "/tmp/cnv.sed.$now"
if (-f $sedscript ) then
	\rm -f $sedscript
endif
echo s/"$cnv_exp"/g > $sedscript

set all = `ls -1`

foreach node ( $all )
	switch($node)
		case "${filter_exp}" :
			echo $node
			sed -f $sedscript $node > $node.tmp
			if (${status} == 0) then
					mv $node.tmp $node
			else
					rm $node.tmp
			endif
		breaksw
	endsw
end

\rm -f $sedscript
