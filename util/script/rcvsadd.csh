#!/usr/bin/csh -f

set prog_name = rcvsadd.csh

set root_dir = 

if ($#argv == 0) then
    echo "Usage: $prog_name <directory_name>"
    exit
endif

if ($#argv == 1) then
    set root_dir = $argv[1]
endif

if ($#argv == 2 && $argv[1] == internal) then
    set root_dir = $argv[2]
endif

if $#root_dir == 0 then
    echo "Usage: $prog_name <directory_name>"
    exit
endif

unalias ls
set dirs = `ls -a $root_dir`

cd $root_dir
echo `pwd`
foreach file_name ($dirs)
    if ($file_name == . || $file_name == .. || $file_name == CVS ) then
        continue
    endif
    if ( -d $file_name) then
        echo "cvs add $file_name"
        cvs add $file_name
        $prog_name internal $file_name
    else if ( -f $file_name) then
        echo "cvs add $file_name"
        cvs add $file_name
    endif
end

