#!/bin/csh -f

if ($#argv < 3) then 
	echo 'chstr.csh <dir> <convert_expression> <filter_expression...>'
	echo 'ex) % chstr.csh . aaa/bbb "*.[Ch]"'
	exit 1
endif

set dir = $argv[1]
set cnv_exp = "$argv[2]"
set filter_exp = "$argv[3-$#argv]"

#echo "---------------------------------"
#echo "${dir}, ${cnv_exp}, ${filter_exp}"
#echo "---------------------------------"

cd $dir

set now = `date +'%Y%m%d%H%M%S'`
set sedscript = "/tmp/cnv.chstr.$now"
if (-f $sedscript ) then
	\rm -f $sedscript
endif
echo "s/$cnv_exp/g" > $sedscript

chstrf.csh $dir $sedscript "$filter_exp"

\rm -f $sedscript
