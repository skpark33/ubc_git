#!/bin/csh -f

set now = `date +'%Y_%m%d_%H%M'`

set dir = $PROJECTROOT/log/top
set outfile = $dir/$now
set mergefile = $dir/merges

mkdir -p $dir
touch $outfile

top -f $outfile

echo "" >> $mergefile
cat $outfile >> $mergefile
