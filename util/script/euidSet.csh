#/bin/csh -f

echo ""
echo "+------------------------------------------------------------+"
echo ""                            
echo "                           WARNNING                          "
echo "               This job requires ROOT permission             "
echo ""                          
echo "+------------------------------------------------------------+"
su root << EOF
	do_euidSet.csh $*
EOF
