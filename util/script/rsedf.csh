#!/bin/csh -f

if ($#argv < 3) then 
	echo 'rsedf.csh <dir> <sed_filter_path> <filter_expression...>'
	echo 'ex) % rsedf.csh . /tmp/mysed.script "*.[Ch]"'
	exit 1
endif

set dir = $argv[1]
set sedscript = $argv[2]
set filter_exp = "$argv[3-$#argv]"

cd $dir
echo "[$dir]----------"

set all = `ls -1`

foreach node ( $all )
	if -d $node then
		rsedf.csh $node $sedscript "$filter_exp"
	else
		switch($node)
			case "${filter_exp}" :
				echo $node
				sed -f $sedscript $node > $node.tmp
				if (${status} == 0) then
						mv $node.tmp $node
				else 
						rm $node.tmp
				endif
			breaksw
		endsw
	endif
end

