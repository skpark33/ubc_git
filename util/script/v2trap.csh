#!/bin/csh -f

if($#argv < 1) then
    echo " Usage : v2trap.csh <port_no> "
    exit(1);
endif

set port = $1
set hostname = 150.23.15.123
set v2opt = "-v 2c -c public ${hostname}:${port}"
set mibdir = "-M ${PROJECT_HOME}/config/mib"
set command = "${TRDPARTYROOT}/gnu/bin/snmptrap"

set now = `date '+%Y/%m/%d/%H:%M:%S'`

#########################################
#              i  INTEGER
#              u  UNSIGNED
#              c  COUNTER32
#              s  STRING
#              x  HEX STRING
#              d  DECIMAL STRING
#              n  NULLOBJ
#              o  OBJID
#              t  TIMETICKS
#              a  IPADDRESS
#              b  BITS
#########################################


#echo "--------------------------------------------------------------"
#set notioid = "IPV6-MIB::ipv6IfStateChange"
#
#set vname1 = "IPV6-MIB::ipv6IfDescr s"
#set vname2 = "IPV6-MIB::ipv6IfOperStatus i"
#
#set val1 = "This_is_IPV6_TRAP_TEST"
#set val2 = 5;
#
#set values = "$vname1 $val1 $vname2 $val2"
#
#echo "$command $mibdir $v2opt '' $notioid $values"
#$command $mibdir $v2opt '' $notioid $values
#echo "--------------------------------------------------------------"


#echo "--------------------------------------------------------------"
#set notioid = "AAA-ALARM-MIB::aaaCpuLoadHighOn"

#set vname1 = "AAA-ALARM-MIB::aaaAlarmType i"
#set vname2 = "AAA-ALARM-MIB::aaaAlarmThreshold i"

#set val1 = 1
#set val2 = 99;

#set values = "$vname1 $val1 $vname2 $val2"

#echo "$command $mibdir $v2opt '' $notioid $values"
#$command $mibdir $v2opt '' $notioid $values
#echo "--------------------------------------------------------------"

#set notioid = "IAGW-KTF-MIB_20030714::iagwSystemDown"
#set notioid = "1.3.6.1.4.1.1618.7.2.1"

#set vname1 = "iagwServerAlarmType i"
#set vname1 = "1.3.6.1.4.1.1618.7.1.1 i"
#set vname2 = "iagwServerAlarmSeverity i"
#set vname2 = "1.3.6.1.4.1.1618.7.1.2 i"
##set vname3 = "iagwServerAlarmIpAddress a"
#set vname3 = "1.3.6.1.4.1.1618.7.1.3 a"

#set val1 = 1
#set val2 = 4;
#set val3 = "99999999";

echo "--------------------------------------------------------------"
set notioid = "1.3.6.1.6.3.1.1.5.3"

set vname1 = "1.3.6.1.2.1.2.2.1.1 i"
set vname2 = "1.3.6.1.2.1.2.2.1.4 i"
set vname3 = "1.3.6.1.2.1.2.2.1.2 a"

set val1 = 1
set val2 = 1;
set val3 = "99999999";


set values = "$vname1 $val1 $vname2 $val2 $vname3 $val3"

echo "$command $mibdir $v2opt '' $notioid $values"
$command $mibdir $v2opt '' $notioid $values
echo "--------------------------------------------------------------"



