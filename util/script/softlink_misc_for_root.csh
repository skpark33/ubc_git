#!/bin/csh -f

set ace_ver = "5.6.2"
set tao_ver = "1.6.2"
set snmppp_ver = "3.2.22"

echo ""
echo "+------------------------------------------------------------+"
echo ""
echo "                           WARNNING                          "
echo "               This job requires ROOT permission             "
echo ""
echo "+------------------------------------------------------------+"
su root << EOF
	cd /usr/lib/sparcv9;

	#
	# 기존 Link 파일 삭제
	#
	rm -f libsnmp++.so;
	rm -f libACE.so.${ace_ver};
	rm -f libTAO.so.${tao_ver};
	rm -f libTAO_AnyTypeCode.so.${tao_ver};
	rm -f libTAO_PI.so.${tao_ver};
	rm -f libTAO_TypeCodeFactory.so.${tao_ver};
	rm -f libTAO_Svc_Utils.so.${tao_ver};
	rm -f libTAO_IORTable.so.${tao_ver};
	rm -f libTAO_CodecFactory.so.${tao_ver};
	rm -f libTAO_DynamicAny.so.${tao_ver};
	rm -f libTAO_IFRService.so.${tao_ver};
	rm -f libTAO_IFR_Client.so.${tao_ver};
	rm -f libTAO_Messaging.so.${tao_ver};
	rm -f libTAO_PortableServer.so.${tao_ver};
	rm -f libTAO_Valuetype.so.${tao_ver};

	#
	# soft Link
	#
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/snmp++${snmppp_ver}/lib/libsnmp++.so .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/ace/libACE.so.${ace_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_AnyTypeCode.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_PI.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_TypeCodeFactory.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_Svc_Utils.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_Svc_Utils.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_IORTable.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_CodecFactory.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_DynamicAny.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_IFRService.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_IFR_Client.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_Messaging.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_PortableServer.so.${tao_ver} .;
	ln -s /nms/users/nmsprj/project/stn/3rdparty/misc/ACE_wrappers${ace_ver}/lib/libTAO_Valuetype.so.${tao_ver} .;

	#
	# confirm list
	#
	ls -al *snmp*;
	ls -al *ACE*;
	ls -al *TAO*;
EOF
