#!/bin/csh -f

if ($#argv < 1 ) then
	echo "USAGE : v1trap.csh iteration_number"
	exit 0
endif

set max = $1

set host1 = 211.232.57.149
set host = `hostname`
#switch (`uname -m`)
#    case alpha:
#	set host = 172.21.90.59
#    breaksw
#    case sun4u:
#	set host = 172.21.90.64
#    breaksw
#endsw
set command = "snmptrap -v 1"

set up = '3'
set down = '2'

set counter = 0


while(1)

	@ counter = $counter + 1

	if ( $max <  $counter ) then
		break
	endif

	echo "$command -c public ${host}:8003  '' $host1 $down 0 ''"
	echo "$command -c public ${host}:8003  '' $host1 $up 0 ''"

	$command -c public ${host}:8003 '' ${host1} $down 0 ''  
	$command -c public ${host}:8003 '' ${host1} $up 0 ''  

end
