#!/bin/csh -f

set sleep_cnt = 60
set limit_cnt = 60

if ($#argv > 0) then
	set sleep_cnt         = "$argv[1]"
endif
if ($#argv > 1) then
	set limit_cnt         = "$argv[2]"
endif

@ counter = 1

set log = /home/ubc/project/ubc/util/script/perf/connection_mon.log

rm -r -f $log
set now = `date +%y%m%d%H%M%S`
#echo "connection_mon $sleep_count $limit_cnt start $now" > $log

while 1
	set est = `netstat -anlp | grep 8080 | grep EST | wc -l`
	echo "$est" >> $log
	sleep $sleep_cnt
	@ counter = $counter + 1
	if($limit_cnt > 0) then
		if($counter > $limit_cnt) then 
			break
		endif
	endif
end

@ val = 0
foreach line (`cat $log`)
    #echo "($line)"
	set val = `echo $val+$line | bc`
	echo $val
end
set result = `echo $val/$limit_cnt | bc`

echo "average = $result"
exit(0)
