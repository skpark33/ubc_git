#!/bin/csh -f

set sleep_sec = 60
set limit_cnt = 60
set proc_name = "httpd"

if ($#argv > 0) then
	set proc_name         = "$argv[1]"
endif
if ($#argv > 1) then
	set sleep_sec         = "$argv[2]"
endif
if ($#argv > 2) then
	set limit_cnt         = "$argv[3]"
endif

set log = /home/ubc/project/ubc/util/script/perf/mem_mon.log

rm -r -f $log
set now = `date +%y%m%d%H%M%S`
#echo "memcpu_mon $proc_name $sleep_sec $limit_cnt start $now" > $log

@ counter = 0
while 1
#	ps -eo user,pid,ppid,pmem,pcpu,time,comm --sort -rss | grep $proc_name >> $log
	ps -eo pmem,comm --sort -rss | grep $proc_name | cut -d' ' -f2 >> $log
	sleep $sleep_sec
	@ counter = $counter + 1
	if($limit_cnt > 0) then
		if($counter > $limit_cnt) then 
			break
		endif
	endif
end

@ val = 0
foreach line (`cat $log`)
    #echo "($line)"
	set val = `echo $val+$line | bc`
	echo $val
end
set result = `echo $val/$limit_cnt | bc`

echo "average = $result %"
exit(0)
