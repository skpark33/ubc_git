#!/bin/csh -f

set limit_est = 50
set limit_cnt = 0

if ($#argv > 0) then
	set limit_est         = "$argv[1]"
endif
if ($#argv > 1) then
	set limit_cnt         = "$argv[2]"
endif

@ counter = 1

set log = /home/ubc/project/ubc/log/netmon.log
set cli = /home/ubc/project/ubc/config/cli/netmon.cli
set ubcRun = /home/ubc/project/ubc/util/script/ubcRun
set run_apache = /home/ubc/project/ubc/util/script/run_apache.sh

rm -r -f $log
set now = `date +%y%m%d%H%M%S`
echo "netmon $limit_est $limit_cnt start $now" > $log

while 1
	set est = `netstat -anlp | grep 8080 | grep EST | wc -l`
	echo "($counter) connection=$est" 
	if( $est  > $limit_est )  then
		set now = `date +%y%m%d%H%M%S`
		echo "$now Alarm : Too much ESTABLISHED connection : $est"
		echo "$now Alarm : Too much ESTABLISHED connection : $est" >> $log
		netstat -anlp | grep 8080 | grep EST >> $log
		$run_apache
		rm -r -f $cli
		echo "post PM=1 heartbeat reserved1=1, reserved2=${est}" > $cli
		$ubcRun cli +run $cli	
		sleep 60
	endif
	sleep 15
	@ counter = $counter + 1
	if($limit_cnt > 0) then
		if($counter > $limit_cnt) then 
			break
		endif
	endif
end

exit(0)
