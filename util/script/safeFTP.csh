#!/bin/csh -f
set retryCount = 1
set waitTime = 60
if ($#argv < 4) then
    echo "USAGE : "
    echo "safeSend.csh <targetHost> <userid> <passwd> <filename> [targetDirectory] [-p] "
    echo "option -p should be set, if you want to create targetDiretory on targetHost"
    exit(1)
endif
set now = `date +'%Y_%m_%d_%H_%M_%S'`
@ counter = 0
set result = 0
while($counter <= $retryCount )
#   echo "$PROJECT_HOME/util/bin/send.csh $* | grep STOR | wc -l"
    set retval = `$PROJECT_HOME/util/bin/send.csh $* | tail -2 | grep STOR | wc -l`
    if ( $retval != 0 ) then
        set result = 1
        break;
    endif
    echo "send failed, I'll re-try after $waitTime seconds"
    sleep($waitTime);
    @ counter = $counter + 1
end
echo "----------------------------------------------------------------------"
echo -n "ftp at $now to $1 for file $4 is "
if ($result == 1) then
    echo " succeed"
    echo ""
    exit(0)
end
echo " faild"
echo ""
exit(1)

