#!/bin/csh -f
if ($#argv < 4) then
    echo "USAGE : "
    echo "send.csh <targetHost> <userid> <passwd> <filename> [targetDirectory] [-p] "
    echo "option -p should be set, if you want to create targetDiretory on targetHost"
    exit(1)
endif
set targetHost = $argv[1]
set userid     = $argv[2]
set passwd     = $argv[3]
set filename   = $argv[4]
if ($#argv > 4) then
    set targetDirectory = $argv[5]
else
    set targetDirectory = "."
endif
if ($#argv > 5) then
    set mkOption = "mkdir $targetDirectory"
else
    set mkOption = "bin"
endif
ftp -din $targetHost << EOF
    user $userid $passwd
    $mkOption
    cd $targetDirectory
    put ${filename}
    bye
EOF
