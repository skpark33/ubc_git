#!/usr/bin/csh

if ( $#argv < 2 )  then
        echo "Usage : nohup mem_monitor.csh <process_name> <period> &"
        exit;
endif

set process_name=$argv[1]
set period=$argv[2]

cd ${PROJECT_HOME}/log
mkdir -p MEMORY
cd ${PROJECT_HOME}/log/MEMORY

set logfile=${process_name}.log

touch $logfile 
while(1)
	checkp ${process_name} >> ${logfile}
	sleep $period
end
