#!/usr/bin/csh -f

set newRoot = ":pserver:${USER}@omega:2401/nms/users/copcvs/VOB"

unalias rm
if -e ~/Root then
   rm -f ~/Root
endif

echo $newRoot > ~/Root

#cd ~/copEnv

#find . -name Root -exec cp ~/Root {} \;
#find . -name Root -exec cat {} \;

cd ~/project/cop

find . -name Root -exec cp ~/Root {} \;
find . -name Root -exec cat {} \;

#cd ~/copEnv

#if -e cop.env.temp then
#    rm -f cop.env.temp
#endif

#echo "setenv CVSROOT $newRoot" > cop.env.temp
#sed -e '1,$ s/setenv CVSROOT.*$/#&/g' cop.env >> cop.env.temp

#if($status == 0) then
#    if -e cop.env.old then
#        rm -f cop.env.old
#    endif
#    mv cop.env cop.env.old
#    mv cop.env.temp cop.env
#endif
