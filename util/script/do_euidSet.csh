#!/bin/csh -f

foreach filename ( $* )
	chown root ${filename}
	chmod 4755  ${filename}
	echo "File ${filename} 's effiective userid is changed as follows\!"
	echo ""
	ls -al ${filename}
	echo ""
end

