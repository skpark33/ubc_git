#!/bin/csh -f

set PROJECT_HOME=/home/ubc/project/ubc

cd ${PROJECT_HOME}

set release_home = ${PROJECT_HOME}/web/UBC_DEFAULT/UBC_Server/UPDATE

set new_release_file = ${release_home}/ubc/release.txt.new
set old_release_file = ${PROJECT_HOME}/release.txt

set version = "0"
if (-e $new_release_file) then
	set version = `head -1 $new_release_file`
endif
if ($version == "0") then
	echo "no new version"
	exit(0)
endif

if (! -e $old_release_file) then
	cp  $new_release_file $old_release_file
endif

set lastVer = "0"
if (-e $old_release_file) then
	set lastVer = `head -1 $old_release_file`
endif
echo "last version = $lastVer"
echo "new version = $version" 

set new_name_array = ()
set new_time_array = ()

@ idx = 0
@ word_counter = 0

foreach word (`cat $new_release_file`)
	if ( $word_counter == 0 ) then
		@ word_counter = $word_counter + 1
		continue
	endif
	
	@ var = $word_counter % 2
	#echo "$word_counter, $var,  $word"

	if($var == 1) then
		set new_name_array = ($new_name_array $word)
		@ idx = $idx + 1
		#echo "new_name_array[$idx]=$new_name_array[$idx]"
	else 
		set new_time_array = ($new_time_array $word)
		#echo "new_time_array[$idx]=$new_time_array[$idx]"
	endif	
	@ word_counter = $word_counter + 1
end

set cur_name_array = ()
set cur_time_array = ()

@ idx = 0
@ word_counter = 0
foreach word (`cat $old_release_file`)
	if ( $word_counter == 0 ) then
		@ word_counter = $word_counter + 1
		continue
	endif
	
	@ var = $word_counter % 2
	#echo "$word_counter, $var,  $word"

	if($var == 1) then
		set cur_name_array = ($cur_name_array $word)
		@ idx = $idx + 1
		#echo "cur_name_array[$idx]=$cur_name_array[$idx]"
	else 
		set cur_time_array = ($cur_time_array $word)
		#echo "cur_time_array[$idx]=$cur_time_array[$idx]"
	endif	
	@ word_counter = $word_counter + 1
end

set target_name_array = ()
set target_time_array = ()
@ new_idx = 1
foreach new_name ( $new_name_array )
	#echo $new_name
	@ cur_idx = 1	
	@ matched = 0
	foreach cur_name ( $cur_name_array )
		if( $new_name == $cur_name ) then
			@ matched = 1
			break
		endif
		@ cur_idx = $cur_idx + 1	
	end

	if( $matched == 1 ) then
		#echo "$new_name founded"
		@ new_time = $new_time_array[$new_idx]
		@ cur_time = $cur_time_array[$cur_idx]
		#echo "$new_time"
		#echo "$cur_time"
		if ($new_time > $cur_time ) then
			set target_name_array = ($target_name_array $new_name)		
			set target_time_array = ($target_time_array $new_time)		
		endif
	endif
	@ new_idx = $new_idx + 1
end

@ target_count = 0
foreach new_name ( $target_name_array )
	@ target_count = $target_count + 1
	echo "$new_name is new"
end
if ($target_count > 0) then
	echo "$target_count  files changed"
else
	echo "nothing is changed"
	exit(0)
endif

set now = `date +%y%m%d%H%M%S`

@ target_count = 0
@ success_count = 0
foreach new_name ( $target_name_array )
	@ target_count = $target_count + 1

	if($new_name == "ubc/bin/x86_64/libCopService.so") then
		set target_file = "/opt/axis2c/services/CopService/libCopService.so"
	else if($new_name == "ubc/bin/x86_64/libFileService.so") then
		set target_file = "/opt/axis2c/services/FileService/libFileService.so"
	else
		set target_file = "${PROJECT_HOME}/../$new_name"
	endif
	echo "mv ${target_file} ${target_file}.$now"
	mv ${target_file} ${target_file}.$now
	echo "cp -f ${release_home}/$new_name ${target_file}"
	cp -f ${release_home}/$new_name ${target_file}
	if($status == 0) then
		@ success_count = $success_count + 1
	else
		mv ${target_dir}/$new_name.$now ${target_file}
	endif
end

if ($target_count == $success_count) then
	echo "cp  -f $new_release_file  $old_release_file"
	cp  -f $new_release_file  $old_release_file
	if($status == 0) then
		echo "new version $version is released"
	else
		echo "new version $version release failed"
		exit(1)
	endif
else
	echo "new version $version release patically failed"
	exit(2)
endif

exit(0)
