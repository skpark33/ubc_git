#!/usr/bin/csh

if ( $#argv < 1 )  then
        echo "Usage : mem_monitor1.csh <process_name>"
        exit;
endif

set process_name=$argv[1]

cd ${PROJECT_HOME}/log
mkdir -p MEMORY
cd ${PROJECT_HOME}/log/MEMORY

set logfile=${process_name}.log

touch $logfile 
checkp ${process_name} >> ${logfile}
