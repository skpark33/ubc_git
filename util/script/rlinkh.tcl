#!/bin/sh
# \
exec tclsh "$0" ${1+"$@"}

# global variables
set NoOfFile 0

proc DateStamp { var } {
    upvar $var stamp
    if [ catch { exec date } result ] {
        puts "exec date error: $result"
    } else {
        set stamp $result
    }
}

proc DoProcess { fullpath_a destDir } {
    #puts "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    if [ catch { exec ln -s $fullpath_a  $destDir } result ] {
        puts "FAIL : ln -s $fullpath_a $destDir "
		puts "$result"
    } else {
        puts "ln -s $fullpath_a $destDir "
		puts "$result"
    }
}

proc Dir { startDir destDir } {
    global NoOfFile

    cd $startDir
    foreach filename [glob -nocomplain * ] {
		#puts "filename = $filename"
		set subdestdir [file join $destDir $filename ]
		#puts "subdestdir = $subdestdir"
        if [file isdirectory $filename] {
    		if [ catch { exec mkdir $subdestdir } result ] {
        		puts "FAIL : mkdir $subdestdir"
				puts "$result"
			} else {
        		puts "mkdir $subdestdir"
				puts "$result"
			}
            Dir  [file join $startDir $filename] $subdestdir
            cd $startDir
        } else {
            if [regexp {[^.].h$} $filename] {
                #puts "filename  = $filename"
                incr NoOfFile
                DoProcess [file join $startDir $filename] $subdestdir 
            }
        }
    }
}

### main ####

if { $argc == 2 } {
    set srcPath [lindex $argv 0]
    set destPath [lindex $argv 1]
    #puts $srcPath
} else {
    puts "USAGE : rlinkh.tcl srcPath destPath"
    exit
}

DateStamp startTime
Dir $srcPath $destPath
DateStamp endTime

puts "SQIsoft rlinkh version 1.0 written by skpark"
puts "NoOfFile = $NoOfFile"
puts "startTime : $startTime"
puts "endTime   : $endTime"

