#include "common/libHttpRequest/HttpRequest.h"
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <util/libEncoding/Encoding.h>
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchUtil.h"
#include "pptCheckTimer.h"


#define		OFS_FLASH_EXE_PATH				"C:\\SQISoft\\Contents\\ENC\\"


ciSET_DEBUG(9, "pptCheckTimer");


pptCheckTimer::pptCheckTimer()
{
	ciDEBUG2(5, ("pptCheckTimer()") );
}

pptCheckTimer::~pptCheckTimer()
{
	ciDEBUG2(5, ("~pptCheckTimer()"));
}

void pptCheckTimer::processExpired(ciString name, int counter, int interval)
{
	//ciDEBUG2(5, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	scratchUtil* aUtil = scratchUtil::getInstance();
	HWND hwnd_ppt = aUtil->getWHandle("pptview.exe");

	if( hwnd_ppt )
	{
		::SetWindowPos(hwnd_ppt, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
		::ShowWindow(hwnd_ppt, SW_SHOW);
		::SetForegroundWindow(hwnd_ppt);
	}

	return;
}
