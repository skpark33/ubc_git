#include <util/libEncoding/Encoding.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciDebug.h>
#include <cci/libValue/ccistringlist.h>

//#include <Dbghelp.h>
#include <shlobj.h>
#include "contents.h"

#include "libPPT2Image/libPPT2Image.h"


#define		SQI_ENC_DIRECTORY		L"C:\\SQISoft\\Contents\\ENC\\"
#define		SQI_TMP_DIRECTORY		L"C:\\SQISoft\\Contents\\Temp\\"


ciSET_DEBUG(9, "contentsInfo");

ciString _getItem(const char* xml, const char* starttag, const char* endtag)
{
	char* start = (char*)strstr(xml, starttag);
	if( start == NULL ) return "";

	start += strlen(starttag);

	char* end = strstr(start, endtag);
	if( end == NULL ) return "";

	int size = end-start;
	if( size < 0 )
	{
		ciERROR2( ("NEGATIVE SIZE !!!") );
		return "";
	}
	//else if( size >= 1024 )
	//{
	//	ciWARN2( ("BUFFER OVER SIZE !!!") );
	//	size = 1023;
	//}

	//char buf[1024] = {0};
	char* buf = new char[size+1];
	AUTO_DELETE(buf);
	::ZeroMemory(buf, size+1);
	strncpy(buf, start, size);

	return buf;
}

//ciString contentsInfo::_getItem(const char* xml, const char* starttag, const char* endtag)
ciWString _getItem(const wchar_t* xml, const wchar_t* starttag, const wchar_t* endtag)
{
	wchar_t* start = (wchar_t*)wcsstr(xml, starttag);
	if( start == NULL ) return L"";

	start += wcslen(starttag);

	wchar_t* end = wcsstr(start, endtag);
	if( end == NULL ) return L"";

	int size = end-start;
	if( size < 0 )
	{
		ciERROR2( ("NEGATIVE SIZE !!!") );
		return L"";
	}
	//else if( size >= 1024 )
	//{
	//	ciWARN2( ("BUFFER OVER SIZE !!!") );
	//	size = 1023;
	//}

	//char buf[1024] = {0};
	wchar_t* buf = new wchar_t[size+1];
	AUTO_DELETE(buf);
	::ZeroMemory(buf, sizeof(wchar_t)*(size+1));
	wcsncpy(buf, start, size);

	return buf;
}

//ciBoolean contentsInfo::_findTag(const char* xml, const char* tag)
ciBoolean _findTag(const wchar_t* xml, const wchar_t* tag)
{
	wchar_t* find = (wchar_t*)wcsstr(xml, tag);
	if( find == NULL ) return ciFalse;
	return ciTrue;
}

void stringReplace(ciWString& target, const wchar_t* from, const wchar_t* to) {
    int end_pos = wcslen(from);
	int begin_pos = 0;

	int tmpLen = wcslen(to);
	begin_pos -= tmpLen;

    while (1) {
        begin_pos = int(target.find(from, begin_pos+tmpLen));
        if (begin_pos < 0) {
            break;
        }
        target.replace(begin_pos, end_pos, to);
    }
}

ciBoolean contentsInfo::getContentsInfoList(contentsInfoList& infoList, ciWString& strXML)
{
	ciDEBUG2(5, ("getContentsInfoList()") );

	// ppt-to-image convert test
	//static bool ppt_test = false;
	//if( ppt_test==false )
	//{
	//	ppt_test = true;

	//	//
	////	CPPT2Image ppt2img;
	////	ppt2img.InitThumbnail(-1, 90);
	////	ppt2img.SaveToImage("R:\\UBC Manager_수정사항.test.테스트.pptx");

	//	//
	//	ciString localPath = "R:\\UBC Manager_수정사항.test.테스트.pptx";

	//	char drv[MAX_PATH]={0},dir[MAX_PATH]={0},fn[MAX_PATH]={0},ext[MAX_PATH]={0};
	//	_splitpath(localPath.c_str(), drv, dir, fn, ext);
	//	if( stricmp(ext,".ppt")==0 || stricmp(ext,".pptx")==0 )
	//	{
	//		ciDEBUG2(5, ("this is ppt-file (%s)", localPath.c_str()) );

	//		// delete thumbnail-directory
	//		sprintf(ext, "%s%s%s\\thumbs", drv, dir, fn);
	//		ciDEBUG2(5, ("clear thumbnail-directory (%s)", ext) );
	//		removeDirectory(ext);

	//		// delete ppt-directory
	//		sprintf(ext, "%s%s%s", drv, dir, fn);
	//		ciDEBUG2(5, ("clear ppt-directory (%s)", ext) );
	//		removeDirectory(ext);

	//		ciDEBUG2(5, ("convert ppt to png (%s)", localPath.c_str()) );
	//		if( ::SetFileAttributes(localPath.c_str(), FILE_ATTRIBUTE_READONLY)==FALSE ) //읽기전용 설정(사이즈 변경 방지)
	//		{
	//			ciWARN( ("FAIL SetFileAttributes !!! (%s) (ErrCode:%u)", localPath.c_str(), ::GetLastError()) );
	//		}
	//		CPPT2Image ppt2img;
	//		ppt2img.InitThumbnail(-1, 90);
	//		if( ppt2img.SaveToImage(localPath.c_str()) == FALSE )
	//		{
	//			ciWARN(("PPT2IMAGE FAIL !!! (%s) (%s)", ppt2img.GetErrorMessage(), localPath.c_str()));
	//		}
	//	}
	//}

	const wchar_t* xml_start = strXML.c_str();

	// clear list
	clearContentsInfoList(infoList);

	//find start-tag
	wchar_t* tag_start = (wchar_t*)wcsstr(strXML.c_str(), CONTENTS_STARTTAG);
	while( tag_start != NULL )
	{
		ciDEBUG2(5, ("find brochure-tag at [%d]", tag_start-xml_start) );

		// find end-tag
		wchar_t* tag_end = wcsstr(tag_start, CONTENTS_ENDTAG);
		if( tag_end == NULL )
		{
			ciWARN2( ("MISSING CONTENTS_ENDTAG !!!") );
			break;
		}
		tag_end += wcslen(CONTENTS_ENDTAG);

		// get item-string
		int size = tag_end - tag_start;
		if( size < 0 )
		{
			ciERROR2( ("NEGATIVE SIZE !!!") );
			break;
		}
		wchar_t* buf_xml = new wchar_t[size+1];
		AUTO_DELETE(buf_xml);
		::ZeroMemory(buf_xml, sizeof(wchar_t)*(size+1));
		wcsncpy(buf_xml, tag_start, size);

		// create new-object
		contentsInfo* info = getContentsInfoObject(buf_xml);
		if( info != NULL )
		{
			infoList.push_back(info);
			ciDEBUG2(5, ("add new-contents(size:%d) %s", infoList.size(), GetUTF8fromU(info->toString(ciFalse).c_str())) );
		}
		else
		{
			ciWARN2( ("INVALID CONTENTS-INFO-XML !!!\r\n%s", GetUTF8fromU(buf_xml)) );
		}

		//find start-tag
		tag_start = wcsstr(tag_end, CONTENTS_STARTTAG);
	}

	//
	if( infoList.size() == 0 )
	{
		ciWARN2( ("NO CONTENTS-INFO !!!") );
		return ciFalse;
	}

	ciDEBUG2(5, ("total contents-info-count (%d)", infoList.size()) );

	return ciTrue;
}

void contentsInfo::clearContentsInfoList(contentsInfoList& infoList)
{
	ciDEBUG2(5, ("clearContentsInfoList(%d)", infoList.size()) );

	contentsInfoListIter itr = infoList.begin();
	contentsInfoListIter end = infoList.end();
	for( ; itr!=end; itr++ )
	{
		contentsInfo* info = (*itr);
		if(info) delete info;
	}
	infoList.clear();

	ciDEBUG2(5, ("clearContentsInfoList() end") );
}

contentsInfo* contentsInfo::getContentsInfoObject(const wchar_t* xml, OFS_CONTENTS_TYPE type)
{
	ciDEBUG2(5, ("getContentsInfoObject(%d)", type) );

	// no-type
	if( type == OFS_UNION_CONTENTS_TYPE )
	{
		// find contents-type
		//if( _findTag(xml, BROCHURECONTENTS_STARTTAG) && _findTag(xml, BROCHURECONTENTS_ENDTAG) )
		//	type = OFS_BROCHURE_CONTENTS_TYPE;
		//else if( _findTag(xml, GOODSCONTENTS_STARTTAG) && _findTag(xml, GOODSCONTENTS_ENDTAG) )
		//	type = OFS_GOODS_CONTENTS_TYPE;

		ciWString str_type = _getItem(xml, CONTENTSTYPE_STARTTAG, CONTENTSTYPE_ENDTAG);
		if( str_type == BROCHURE_CONTENTS_TYPE )
			type = OFS_BROCHURE_CONTENTS_TYPE;
		else if( str_type == GOODS_CONTENTS_TYPE )
			type = OFS_GOODS_CONTENTS_TYPE;
		else if( str_type == DELETE_CONTENTS_TYPE )
			type = OFS_DELETE_CONTENTS_TYPE;

		ciDEBUG2(5, ("new contents type (%d)", type) );
	}

	// create new-object
	contentsInfo* info = NULL;
	//switch( type )
	//{
	//default:
	//case OFS_UNION_CONTENTS_TYPE:
		info = new contentsInfo;
	//	break;

	//case OFS_BROCHURE_CONTENTS_TYPE:
	//	info = new brochureContentsInfo;
	//	break;

	//case OFS_GOODS_CONTENTS_TYPE:
	//	info = new goodsContentsInfo;
	//	break;
	//}

	// extract info
	if( info && info->fromString(xml) )
	{
		info->contentsType = type;
		ciDEBUG2(5, ("fromString succeed (%s)", GetUTF8fromU(info->toString(ciFalse).c_str())) );
		return info;
	}

	// no-type or no-info
	ciWARN2( ("fromString FAIL !!! (%s)", GetUTF8fromU(xml)) );
	delete info;

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

contentsInfo::contentsInfo()
{
	init();
}

void contentsInfo::init()
{
	contentsType	= OFS_UNION_CONTENTS_TYPE;

	mediaType		= L"";
	filename		= L"";
	location		= L"";
	locationOri		= L"";
	volume			= 0;

	time_t tm = 1;
	lastUpdateTime.set(tm);

	filenameOri		= L"";
	timeFlag		= L"";

	existLocalPath	= ciFalse;
	existTempPath	= ciFalse;
};

ciBoolean contentsInfo::fromString(const wchar_t* xml)
{
	ciDEBUG2(5, ("fromString()") );

	init();

	// sample :
	// <downFile>
	//	 	<filename>SQJAW15512GYD_A1.jpg</filename>
	//		<location>contents/uploadData/goods/SQJAW15512GYD/SQJAW15512GYD_A1.jpg</location>
	//	 	<volume>0</volume>
	//		<lastUpdateTime>1445504178</lastUpdateTime>
	// </downFile>

	// mediaType
	mediaType	= _getItem(xml, MEDIATYPE_STARTTAG, MEDIATYPE_ENDTAG);

	// filename
	filename	= _getItem(xml, FILENAME_STARTTAG, FILENAME_ENDTAG);

	// location
	locationOri = _getItem(xml, LOCATION_STARTTAG, LOCATION_ENDTAG);
	location	= L"/" + locationOri;
	stringReplace(location, L"//", L"/");//ciStringUtil::stringReplace(location, "//", "/");

	// volume
	volume		= _wtoi64(_getItem(xml, VOLUME_STARTTAG, VOLUME_ENDTAG).c_str());

	// lastUpdateTime
	time_t tm	= _wtoi64(_getItem(xml, LASTUPDATETIME_STARTTAG, LASTUPDATETIME_ENDTAG).c_str());
	if( tm > 0 ) lastUpdateTime.set(tm);

	//
	filenameOri	= _getItem(xml, FILENAME_ORI_STARTTAG, FILENAME_ORI_ENDTAG);

	//
	filenameAlt = _getItem(xml, FILENAME_ALT_STARTTAG, FILENAME_ALT_ENDTAG);

	//
	timeFlag	= _getItem(xml, TIMEFLAG_STARTTAG, TIMEFLAG_ENDTAG);

	stringReplace(filename, L"&amp;", L"&");//ciStringUtil::stringReplace(filename, "&amp;", "&");
	stringReplace(location, L"&amp;", L"&");//ciStringUtil::stringReplace(location, "&amp;", "&");
	stringReplace(locationOri, L"&amp;", L"&");//ciStringUtil::stringReplace(locationOri, "&amp;", "&");
	stringReplace(filenameOri, L"&amp;", L"&");//ciStringUtil::stringReplace(filenameOri, "&amp;", "&");
	stringReplace(filenameAlt, L"&amp;", L"&");//ciStringUtil::stringReplace(filenameAlt, "&amp;", "&");

	// use only in KHQS
	if( ciArgParser::getInstance()->isSet("+KHQS") )
	{
		if( _getLocalPath(L"contents/KHQS/", L"KHQS\\", location.c_str(), lastUpdateTime.getTime()) == ciFalse )
		{
			ciWARN2( ("INVALID PATH for KHQS !!!") );
			return ciFalse;
		}
	}
	//
	else if( ciArgParser::getInstance()->isSet("+HMCSFS") )
	{
		if( _getLocalPath(L"files/pmis/", L"PDIS\\", location.c_str(), lastUpdateTime.getTime()) == ciFalse )
		{
			ciWARN2( ("INVALID PATH for HMCSFS !!!") );
			return ciFalse;
		}
	}
	//
	else if( ciArgParser::getInstance()->isSet("+ROYAL") )
	{
		if( _getLocalPath(L"contents/JIBOKJAE/", L"ROYAL\\", location.c_str(), lastUpdateTime.getTime()) == ciFalse )
		{
			ciWARN2( ("INVALID PATH for ROYAL !!!") );
			return ciFalse;
		}
	}
	//
	else if( ciArgParser::getInstance()->isSet("+SPC_SALE") )
	{
		if( _getLocalPath(L"contents/spc_sale/", L"SPC_SALE\\", location.c_str(), lastUpdateTime.getTime()) == ciFalse )
		{
			ciWARN2( ("INVALID PATH for SPC_SALE !!!") );
			return ciFalse;
		}
	}
	//
	else if( ciArgParser::getInstance()->isSet("+SSG_SHOP") )
	{
		if( _getLocalPath(L"contents/ssg_shop/", L"SSG_SHOP\\", location.c_str(), lastUpdateTime.getTime()) == ciFalse )
		{
			ciWARN2( ("INVALID PATH for SPC_SALE !!!") );
			return ciFalse;
		}
	}
	// use only in OFS
	else if( _getLocalPath(L"contents/uploadData/", L"OFS\\", location.c_str(), lastUpdateTime.getTime()) == ciFalse )
	{
		ciWARN2( ("INVALID PATH !!!") );
		return ciFalse;
	}

	if( filename.length() == 0 /*|| volume == 0*/ )
	{
		ciWARN2( ("NO FILENAME OR SIZE !!!") );
		return ciFalse;
	}

	ciDEBUG2(5, ("fromString(%s)", GetUTF8fromU(toString(ciFalse).c_str())) );

	return ciTrue;
}

ciWString contentsInfo::toString(ciBoolean xml_type)
{
	wchar_t buf[4096] = {0};

	if( xml_type )
	{
		swprintf(buf,
			L"	%s\r\n"
			L"		<contentType>%d</contentType>\r\n"
			L"		<filename>%s</filename>\r\n"
			L"		<filenameOri>%s</filenameOri>\r\n"
			L"		<filenameAlt>%s</filenameAlt>\r\n"
			L"		<location>%s</location>\r\n"
			L"		<timeFlag>%s</timeFlag>\r\n", 
			L"		<volume>%I64u</volume>\r\n", 
			L"		<lastUpdateTime>%I64d</lastUpdateTime>\r\n"
			L"	%s\r\n",
			CONTENTS_STARTTAG,//UNIONCONTENTS_STARTTAG,
				contentsType,
				filename.c_str(),
				filenameOri.c_str(),
				filenameAlt.c_str(),
				location.c_str(),
				timeFlag.c_str(),
				volume,
				lastUpdateTime.getTime(),
			CONTENTS_ENDTAG//UNIONCONTENTS_ENDTAG
			);
	}
	else
	{
		swprintf(buf, 
			L"{"
				L"{contentsType,%d},"
				L"{filename,%s},"
				L"{filenameOri,%s},"
				L"{filenameAlt,%s},"
				L"{location,%s},"
				L"{timeFlag,%s},"
				L"{volume,%I64u},"
				L"{lastUpdateTime,%I64d}"
			L"}",
				contentsType,
				filename.c_str(),
				filenameOri.c_str(),
				filenameAlt.c_str(),
				location.c_str(),
				timeFlag.c_str(),
				volume,
				lastUpdateTime.getTime()
			);
	}

	return buf;
}

contentsInfo* contentsInfo::duplicate()
{
	contentsInfo* dup = new contentsInfo;
	*dup = *this;

	return dup;
}

ciBoolean contentsInfo::isEqual(contentsInfo* info)
{
	if( contentsType	!= info->contentsType	) return ciFalse;

	if( mediaType		!= info->mediaType		) return ciFalse;
	if( filename		!= info->filename		) return ciFalse;
	if( location		!= info->location		) return ciFalse;
	if( volume			!= info->volume			) return ciFalse;
	if( lastUpdateTime.getTime() != info->lastUpdateTime.getTime() ) return ciFalse;

	if( filenameOri		!= info->filenameOri	) return ciFalse;
	if( timeFlag		!= info->timeFlag		) return ciFalse;

	return ciTrue;
}

ciBoolean contentsInfo::_getLocalPath(const wchar_t* findStr, const wchar_t* replaceStr, const wchar_t* path, time_t tm)
{
	//
	localPath = L"";
	tempPath = L"";
	bakPath = L"";
	altPath = L"";

	// sample :
	// contents/uploadData/goods/SQWAW15626GYX/SQWAW15621GYX_A6.jpg
	// contents/uploadData/brochure/test/TEST_001.mp4
	// ^------------------^ cut
	wchar_t* start = (wchar_t*)wcsstr(path, findStr);//"contents/uploadData/");
	if( start == NULL )
	{
		ciWARN2( ("INVALID PATH !!! (%s)", GetUTF8fromU(path)) );
		//return ciFalse;
		start = (wchar_t*)path;
	}
	else
		start += wcslen(findStr);//"contents/uploadData/");

	// local_path
	localPath = SQI_ENC_DIRECTORY;
	localPath += replaceStr;
	localPath += start;

	if( filenameOri.length() > 0 ) // for HMCSFS
	{
		//ciStringUtil::stringReplace(localPath, filename.c_str(), filenameOri.c_str()); // rename filename to filenameOri
		stringReplace(localPath, filename.c_str(), filenameOri.c_str()); // rename filename to filenameOri
		if( wcsstr(localPath.c_str(), filenameOri.c_str()) == NULL )
		{
			ciWARN2( ("INVALID PATH (NOT EXIST FILENAME IN LOCATION) !!! (filename:%s, localPath:%s)", GetUTF8fromU(filename.c_str()), GetUTF8fromU(localPath.c_str())) );
			//return ciFalse;
		}
	}

	// temp_path
	tempPath = SQI_TMP_DIRECTORY;
	tempPath += replaceStr;
	tempPath += start;
	wchar_t tmp[32] = {0};
	swprintf(tmp, L".%I64d", tm);
	tempPath += tmp;

	// bak_path
	bakPath = localPath;
	bakPath += L".bak";

	// alt_path
	if( filenameAlt.length() > 0 ) // for HMCSFS
	{
		altPath = SQI_ENC_DIRECTORY;
		altPath += replaceStr;
		altPath += start;

		//ciStringUtil::stringReplace(altPath, filename.c_str(), filenameAlt.c_str()); // rename filename to filenameAlt
		stringReplace(altPath, filename.c_str(), filenameAlt.c_str()); // rename filename to filenameAlt
		if( wcsstr(altPath.c_str(), filenameAlt.c_str()) == NULL )
		{
			ciWARN2( ("INVALID PATH (NOT EXIST FILENAME_ALT IN LOCATION) !!! (filename:%s, altPath:%s)", GetUTF8fromU(filename.c_str()), GetUTF8fromU(altPath.c_str())) );
			//return ciFalse;
		}
	}

	stringReplace(localPath, L"/", L"\\");//ciStringUtil::stringReplace(localPath, "/", "\\");
	stringReplace(localPath, L"\\\\", L"\\");//ciStringUtil::stringReplace(localPath, "\\\\", "\\");

	stringReplace(tempPath, L"/", L"\\");//ciStringUtil::stringReplace(tempPath, "/", "\\");
	stringReplace(tempPath, L"\\\\", L"\\");//ciStringUtil::stringReplace(tempPath, "\\\\", "\\");

	stringReplace(bakPath, L"/", L"\\");//ciStringUtil::stringReplace(bakPath, "/", "\\");
	stringReplace(bakPath, L"\\\\", L"\\");//ciStringUtil::stringReplace(bakPath, "\\\\", "\\");

	stringReplace(altPath, L"/", L"\\");//ciStringUtil::stringReplace(altPath, "/", "\\");
	stringReplace(altPath, L"\\\\", L"\\");//ciStringUtil::stringReplace(altPath, "\\\\", "\\");

	ciDEBUG2(5, ("local_path (%s)", GetUTF8fromU(localPath.c_str())) );
	ciDEBUG2(5, ("temp_path (%s)", GetUTF8fromU(tempPath.c_str())) );
	ciDEBUG2(5, ("bak_path (%s)", GetUTF8fromU(bakPath.c_str())) );
	ciDEBUG2(5, ("alt_path (%s)", GetUTF8fromU(altPath.c_str())) );

	return ciTrue;
}

int contentsInfo::isExistFile(ULONGLONG* pFileSize)
{
	struct _stati64 fs;
	if( _wstati64(localPath.c_str(), &fs) == 0 )
	{
		if( pFileSize != NULL )
			*pFileSize = fs.st_size;

		if( volume == fs.st_size )
		{
			// same size => already exist => no download
			ciDEBUG2(5, ("local_path already exist (%s)", GetUTF8fromU(localPath.c_str())) );
			existLocalPath = ciTrue;
			return 1;
		}

		ciDEBUG2(5, ("local_path different size (%s) (org_size:%I64u) (cur_size:%I64u)", GetUTF8fromU(localPath.c_str()), volume, fs.st_size) );
		return -1;
	}

	if( pFileSize != NULL ) *pFileSize = 0;
	return 0;
}

int contentsInfo::isExistTempFile(ULONGLONG* pFileSize)
{
	struct _stati64 fs;
	if( altPath.length()>0 && _wstati64(altPath.c_str(), &fs)==0 )
	{
		if( pFileSize != NULL )
			*pFileSize = fs.st_size;

		if( volume == fs.st_size )
		{
			// same size => already exist => no download
			ciDEBUG2(5, ("alt_path already exist (%s)", GetUTF8fromU(altPath.c_str())) );
			tempPath = altPath;
			existTempPath = ciTrue;
			return 1;
		}
	}

	if( _wstati64(tempPath.c_str(), &fs) == 0 )
	{
		if( pFileSize != NULL )
			*pFileSize = fs.st_size;

		if( volume == fs.st_size )
		{
			// same size => already exist => no download
			ciDEBUG2(5, ("temp_path already exist (%s)", GetUTF8fromU(tempPath.c_str())) );
			existTempPath = ciTrue;
			return 1;
		}

		ciDEBUG2(5, ("temp_path different size (%s) (org_size:%I64u) (cur_size:%I64u)", GetUTF8fromU(tempPath.c_str()), volume, fs.st_size) );
		return -1;
	}

	if( pFileSize != NULL ) *pFileSize = 0;
	return 0;
}

ciBoolean contentsInfo::replaceFile()
{
	if( existLocalPath == ciFalse && existTempPath == ciFalse )
	{
		ciWARN( ("not exist local-file & temp-file !!! (%s)(%s)", GetUTF8fromU(localPath.c_str()), GetUTF8fromU(tempPath.c_str())) );
		return ciFalse;
	}

	if( existLocalPath )
	{
		ciDEBUG2(5, ("already exist in local_path(%s) ==> don't replace_file", GetUTF8fromU(localPath.c_str())) );
		return ciTrue;
	}

	// delete bak
	ciDEBUG2(5, ("delete bak (%s)", GetUTF8fromU(bakPath.c_str())) );
	::SetFileAttributesW(bakPath.c_str(), FILE_ATTRIBUTE_NORMAL); //읽기전용 해제
	::DeleteFileW(bakPath.c_str());

	// move org to bak
	ciDEBUG2(5, ("move org to bak (%s) -> (%s)", GetUTF8fromU(localPath.c_str()), GetUTF8fromU(bakPath.c_str())) );
	BOOL move_org_to_bak = ::MoveFileW(localPath.c_str(), bakPath.c_str());

	// move tmp to org
	ciDEBUG2(5, ("move tmp to org (%s) -> (%s)", GetUTF8fromU(tempPath.c_str()), GetUTF8fromU(localPath.c_str())) );
	createFullDirectory(localPath.c_str());
	if( ::MoveFileW(tempPath.c_str(), localPath.c_str()) == FALSE )
	{
		ciWARN( ("move tmp to org FAIL !!! (%s) -> (%s)", GetUTF8fromU(tempPath.c_str()), GetUTF8fromU(localPath.c_str())) );
		if( move_org_to_bak )
		{
			ciDEBUG2(5, ("move bak to org (%s) -> (%s)", GetUTF8fromU(bakPath.c_str()), GetUTF8fromU(localPath.c_str())) );
			::MoveFileW(bakPath.c_str(), localPath.c_str());
		}
		return ciFalse;
	}

	// all success => delete bak
	ciDEBUG2(5, ("all success, delete bak (%s)", GetUTF8fromU(bakPath.c_str())) );
	::SetFileAttributesW(bakPath.c_str(), FILE_ATTRIBUTE_NORMAL); //읽기전용 해제
	::DeleteFileW(bakPath.c_str());

	if( ciArgParser::getInstance()->isSet("+HMCSFS") )
	{
		// ppt ==> png (for HMCSFS)
		wchar_t drv[MAX_PATH]={0},dir[MAX_PATH]={0},fn[MAX_PATH]={0},ext[MAX_PATH]={0};
		_wsplitpath(localPath.c_str(), drv, dir, fn, ext);
		if( wcsicmp(ext,L".ppt")==0 || wcsicmp(ext,L".pptx")==0 )
		{
			ciDEBUG2(5, ("this is ppt-file (%s)", GetUTF8fromU(localPath.c_str())) );

			// delete thumbnail-directory
			swprintf(ext, L"%s%s%s\\thumbs", drv, dir, fn);
			ciDEBUG2(5, ("clear thumbnail-directory (%s)", GetUTF8fromU(ext)) );
			removeDirectory(ext);

			// delete ppt-directory
			swprintf(ext, L"%s%s%s", drv, dir, fn);
			ciDEBUG2(5, ("clear ppt-directory (%s)", GetUTF8fromU(ext)) );
			removeDirectory(ext);

/*
			WIN32_FIND_DATA fd;
			HANDLE handle = FindFirstFile(tmp, &fd);
			if( handle != INVALID_HANDLE_VALUE )
			{
				BOOL ret = TRUE;
				while( ret )
				{
					char fullpath[MAX_PATH];
					sprintf(fullpath, "%s%s%s\\%s", drv, dir, fn, fd.cFileName);

					if( fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
					{
						// directory
					}
					else
					{
						// file => delete
						::SetFileAttributes(fullpath, FILE_ATTRIBUTE_NORMAL); //읽기전용 해제
						::DeleteFile(fullpath);
					}

					ret = FindNextFile(handle, &fd);
				}
				FindClose(handle);

				sprintf(tmp, "%s%s%s", drv, dir, fn);
				::RemoveDirectory(tmp);
			}
*/
			ciDEBUG2(5, ("convert ppt to png (%s)", GetUTF8fromU(localPath.c_str())) );
			if( ::SetFileAttributesW(localPath.c_str(), FILE_ATTRIBUTE_READONLY)==FALSE ) //읽기전용 설정(사이즈 변경 방지)
			{
				ciWARN( ("FAIL SetFileAttributes !!! (%s) (ErrCode:%u)", GetUTF8fromU(localPath.c_str()), ::GetLastError()) );
			}
			CPPT2Image ppt2img;
			ppt2img.InitThumbnail(-1, 90);
			if( ppt2img.SaveToImage(localPath.c_str()) == FALSE )
			{
				ciWARN(("PPT2IMAGE FAIL !!! (%s) (%s)", GetUTF8fromA(ppt2img.GetErrorMessage()), GetUTF8fromU(localPath.c_str())));
			}
		}
	}

	return ciTrue;
}

ciBoolean contentsInfo::removeDirectory(const wchar_t* path)
{
	wchar_t find_path[MAX_PATH];
	swprintf(find_path, L"%s\\*.*", path);

	WIN32_FIND_DATAW fd;
	HANDLE handle = FindFirstFileW(find_path, &fd);
	if( handle == INVALID_HANDLE_VALUE ) return ciFalse;

	BOOL ret = TRUE;
	while( ret )
	{
		wchar_t fullpath[MAX_PATH];
		swprintf(fullpath, L"%s\\%s", path, fd.cFileName);

		if( fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			// directory ==> skip
		}
		else
		{
			// file => delete
			::SetFileAttributesW(fullpath, FILE_ATTRIBUTE_NORMAL); //읽기전용 해제
			::DeleteFileW(fullpath);
		}

		ret = FindNextFileW(handle, &fd);
	}
	FindClose(handle);

	::RemoveDirectoryW(path);

	return ciTrue;
}

////////////////////////////////////////////////////////////////////////////////

brochureContentsInfo::brochureContentsInfo()
{
	init();
}

void brochureContentsInfo::init()
{
	contentsInfo::init();

	contentsType	= OFS_BROCHURE_CONTENTS_TYPE;
	mgrId			= L"";
	siteId			= L"";
	brochureId		= L"";
	pageId			= L"";
	frameId			= L"";
	contentsId		= L"";
	brochureType	= L"";
	zorder			= 0;
	runningTime		= 0;
	creatorId		= L"";
	description		= L"";
};

ciBoolean brochureContentsInfo::fromString(const wchar_t* xml)
{
	if( !contentsInfo::fromString(xml) ) return ciFalse;

	mgrId			= _getItem(xml, MGRID_STARTTAG, MGRID_ENDTAG);
	siteId			= _getItem(xml, SITEID_STARTTAG, SITEID_ENDTAG);
	brochureId		= _getItem(xml, BROCHUREID_STARTTAG, BROCHUREID_ENDTAG);
	pageId			= _getItem(xml, PAGEID_STARTTAG, PAGEID_ENDTAG);
	frameId			= _getItem(xml, FRAMEID_STARTTAG, FRAMEID_ENDTAG);
	contentsId		= _getItem(xml, CONTENTSID_STARTTAG, CONTENTSID_ENDTAG);
	brochureType	= _getItem(xml, BROCHURETYPE_STARTTAG, BROCHURETYPE_ENDTAG);
	zorder			= _wtoi(_getItem(xml, ZORDER_STARTTAG, ZORDER_ENDTAG).c_str());
	runningTime		= _wtoi(_getItem(xml, RUNNINGTIME_STARTTAG, RUNNINGTIME_ENDTAG).c_str());
	creatorId		= _getItem(xml, CREATORID_STARTTAG, CREATORID_ENDTAG);
	description		= _getItem(xml, DESCRIPTION_STARTTAG, DESCRIPTION_ENDTAG);

	return ciTrue;
}

ciWString brochureContentsInfo::toString(ciBoolean xml_type)
{
	wchar_t buf[4096] = {0};

	if( xml_type )
	{
		swprintf(buf,
			L"	%s\r\n"
			L"		<mgrId>%s</mgrId>\r\n"
			L"		<siteId>%s</siteId>\r\n"
			L"		<brochureId>%s</brochureId>\r\n"
			L"		<pageId>%s</pageId>\r\n"
			L"		<frameId>%s</frameId>\r\n"
			L"		<contentsId>%s</contentsId>\r\n"
			L"		<brochureType>%s</brochureType>\r\n"
			L"		<mediaType>%s</mediaType>\r\n"
			L"		<filename>%s</filename>\r\n"
			L"		<location>%s</location>\r\n"
			L"		<volume>%I64u</volume>\r\n"
			L"		<zorder>%d</zorder>\r\n"
			L"		<runningTime>%d</runningTime>\r\n"
			L"		<creatorId>%s</creatorId>\r\n"
			L"		<lastUpdateTime>%I64d</lastUpdateTime>\r\n"
			L"		<description>%s</description>\r\n"
			L"	%s\r\n",
				CONTENTS_STARTTAG,//BROCHURECONTENTS_STARTTAG,
					mgrId.c_str(),
					siteId.c_str(),
					brochureId.c_str(),
					pageId.c_str(),
					frameId.c_str(),
					contentsId.c_str(),
					brochureType.c_str(),
					mediaType.c_str(),
					filename.c_str(),
					location.c_str(),
					volume,
					zorder,
					runningTime,
					creatorId.c_str(),
					lastUpdateTime.getTime(),
					description.c_str(),
				CONTENTS_ENDTAG//BROCHURECONTENTS_ENDTAG
			);
	}
	else
	{
		swprintf(buf,
			L"{"
				L"{contentsType,brochureContents},"
				L"{mgrId,%s},"
				L"{siteId,%s},"
				L"{brochureId,%s},"
				L"{pageId,%s},"
				L"{frameId,%s},"
				L"{contentsId,%s},"
				L"{brochureType,%s},"
				L"{mediaType,%s},"
				L"{filename,%s},"
				L"{location,%s},"
				L"{volume,%I64u},"
				L"{zorder,%d},"
				L"{runningTime,%d},"
				L"{creatorId,%s},"
				L"{lastUpdateTime,%I64d},"
				L"{description,%s},"
			L"}",
				mgrId.c_str(),
				siteId.c_str(),
				brochureId.c_str(),
				pageId.c_str(),
				frameId.c_str(),
				contentsId.c_str(),
				brochureType.c_str(),
				mediaType.c_str(),
				filename.c_str(),
				location.c_str(),
				volume,
				zorder,
				runningTime,
				creatorId.c_str(),
				lastUpdateTime.getTime(),
				description.c_str()
			);
	}

	return buf;
}

contentsInfo* brochureContentsInfo::duplicate()
{
	brochureContentsInfo* dup = new brochureContentsInfo;
	*dup = *this;

	return dup;
}

ciBoolean brochureContentsInfo::isEqual(contentsInfo* info)
{
	//if( contentsType != info->contentsType ) return ciFalse;
	if( !contentsInfo::isEqual(info) ) return ciFalse;

	brochureContentsInfo* tmp = (brochureContentsInfo*)info;
	if( mgrId			!= tmp->mgrId			) return ciFalse;
	if( siteId			!= tmp->siteId			) return ciFalse;
	if( brochureId		!= tmp->brochureId		) return ciFalse;
	if( pageId			!= tmp->pageId			) return ciFalse;
	if( frameId			!= tmp->frameId			) return ciFalse;
	if( contentsId		!= tmp->contentsId		) return ciFalse;
	if( brochureType	!= tmp->brochureType	) return ciFalse;
	if( zorder			!= tmp->zorder			) return ciFalse;
	if( runningTime		!= tmp->runningTime		) return ciFalse;
	if( creatorId		!= tmp->creatorId		) return ciFalse;
	if( description		!= tmp->description		) return ciFalse;

	return ciTrue;
}

////////////////////////////////////////////////////////////////////////////////

goodsContentsInfo::goodsContentsInfo()
{
	init();
}

void goodsContentsInfo::init()
{
	contentsInfo::init();

	contentsType	= OFS_GOODS_CONTENTS_TYPE;
	mgrId			= L"";
	siteId			= L"";
	goodsId			= L"";
	contentsId		= L"";
	parentId		= L"";
	usageType		= L"";
	creatorId		= L"";
	description		= L"";
};

ciBoolean goodsContentsInfo::fromString(const wchar_t* xml)
{
	if( !contentsInfo::fromString(xml) ) return ciFalse;

	mgrId			= _getItem(xml, MGRID_STARTTAG, MGRID_ENDTAG);
	siteId			= _getItem(xml, SITEID_STARTTAG, SITEID_ENDTAG);
	goodsId			= _getItem(xml, GOODSID_STARTTAG, GOODSID_ENDTAG);
	contentsId		= _getItem(xml, CONTENTSID_STARTTAG, CONTENTSID_ENDTAG);
	parentId		= _getItem(xml, PARENTID_STARTTAG, PARENTID_ENDTAG);
	usageType		= _getItem(xml, USAGETYPE_STARTTAG, USAGETYPE_ENDTAG);
	creatorId		= _getItem(xml, CREATORID_STARTTAG, CREATORID_ENDTAG);
	description		= _getItem(xml, DESCRIPTION_STARTTAG, DESCRIPTION_ENDTAG);

	return ciTrue;
}

ciWString goodsContentsInfo::toString(ciBoolean xml_type)
{
	wchar_t buf[4096] = {0};
	if( xml_type )
	{
		swprintf(buf,
			L"	%s\r\n"
			L"		<mgrId>%s</mgrId>\r\n"
			L"		<siteId>%s</siteId>\r\n"
			L"		<goodsId>%s</brochureId>\r\n"
			L"		<contentsId>%s</contentsId>\r\n"
			L"		<mediaType>%s</mediaType>\r\n"
			L"		<filename>%s</filename>\r\n"
			L"		<location>%s</location>\r\n"
			L"		<volume>%I64u</volume>\r\n"
			L"		<parentId>%s</parentId>\r\n"
			L"		<usageType>%s</usageType>\r\n"
			L"		<creatorId>%s</creatorId>\r\n"
			L"		<lastUpdateTime>%I64d</lastUpdateTime>\r\n"
			L"		<description>%s</description>\r\n"
			L"	%s\r\n",
				CONTENTS_STARTTAG,//GOODSCONTENTS_STARTTAG,
					mgrId.c_str(),
					siteId.c_str(),
					goodsId.c_str(),
					contentsId.c_str(),
					mediaType.c_str(),
					filename.c_str(),
					location.c_str(),
					volume,
					parentId.c_str(),
					usageType.c_str(),
					creatorId.c_str(),
					lastUpdateTime.getTime(),
					description.c_str(),
				CONTENTS_ENDTAG//GOODSCONTENTS_ENDTAG
			);
	}
	else
	{
		swprintf(buf,
			L"{"
				L"{mgrId,%s},"
				L"{siteId,%s},"
				L"{goodsId,%s},"
				L"{contentsId,%s},"
				L"{mediaType,%s},"
				L"{filename,%s},"
				L"{location,%s},"
				L"{volume,%I64u},"
				L"{parentId,%s},"
				L"{usageType,%s},"
				L"{creatorId,%s},"
				L"{lastUpdateTime,%I64d},"
				L"{description,%s},"
			L"}",
				mgrId.c_str(),
				siteId.c_str(),
				goodsId.c_str(),
				contentsId.c_str(),
				mediaType.c_str(),
				filename.c_str(),
				location.c_str(),
				volume,
				parentId.c_str(),
				usageType.c_str(),
				creatorId.c_str(),
				lastUpdateTime.getTime(),
				description.c_str()
			);
	}
	return buf;
}

contentsInfo* goodsContentsInfo::duplicate()
{
	goodsContentsInfo* dup = new goodsContentsInfo;
	*dup = *this;

	return dup;
}

ciBoolean goodsContentsInfo::isEqual(contentsInfo* info)
{
	//if( contentsType != info->contentsType ) return ciFalse;
	if( !contentsInfo::isEqual(info) ) return ciFalse;

	goodsContentsInfo* tmp = (goodsContentsInfo*)info;
	if( mgrId			!= tmp->mgrId		) return ciFalse;
	if( siteId			!= tmp->siteId		) return ciFalse;
	if( goodsId			!= tmp->goodsId		) return ciFalse;
	if( contentsId		!= tmp->contentsId	) return ciFalse;
	if( parentId		!= tmp->parentId	) return ciFalse;
	if( usageType		!= tmp->usageType	) return ciFalse;
	if( creatorId		!= tmp->creatorId	) return ciFalse;
	if( description		!= tmp->description	) return ciFalse;

	return ciTrue;
}

ciBoolean createFullDirectory(const wchar_t* path, ciBoolean isFilenameIncluded)
{
	BOOL ret = TRUE;
	wchar_t drv[MAX_PATH], dir[MAX_PATH], fname[MAX_PATH], ext[MAX_PATH];
	if( isFilenameIncluded )
	{
		_wsplitpath(path, drv, dir, fname, ext);

		swprintf(fname, L"%s%s", drv, dir);
	}
	else
	{
		swprintf(fname, L"%s", path);
	}

	if( _waccess(fname,00) == 0 ) return TRUE;

	ciWARN( ("createFullDirectory (%s)", GetUTF8fromU(fname)) );
	//ret = ::MakeSureDirectoryPathExists(fname);
	ret = ::SHCreateDirectoryExW(NULL, fname, NULL);
	if( ret ) return TRUE;
	ciWARN( ("FAIL TO MakeSureDirectoryPathExists() !!! (%s) (ErrCode:%u)", GetUTF8fromU(fname), ::GetLastError()) );
	return FALSE;
}
