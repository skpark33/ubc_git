#ifndef _contentsInfo_h_
#define _contentsInfo_h_
/*
#define		UNIONCONTENTS_STARTTAG		_T("<downFile>")
#define		UNIONCONTENTS_ENDTAG		_T("</downFile>")

#define		BROCHURECONTENTS_STARTTAG	_T("<brochureContentsVo>")
#define		BROCHURECONTENTS_ENDTAG		_T("</brochureContentsVo>")

#define		GOODSCONTENTS_STARTTAG		_T("<goodsContentsVo>")
#define		GOODSCONTENTS_ENDTAG		_T("</goodsContentsVo>")
*/
#define		CONTENTS_STARTTAG			L"<downFile>"
#define		CONTENTS_ENDTAG				L"</downFile>"

#define		CONTENTSTYPE_STARTTAG		L"<contentType>"
#define		CONTENTSTYPE_ENDTAG			L"</contentType>"

#define		BROCHURE_CONTENTS_TYPE		L"brochure"
#define		GOODS_CONTENTS_TYPE			L"goods"
#define		DELETE_CONTENTS_TYPE		L"delete"

#define		MGRID_STARTTAG				L"<mgrId>"
#define		MGRID_ENDTAG				L"</mgrId>"
#define		SITEID_STARTTAG				L"<siteId>"
#define		SITEID_ENDTAG				L"</siteId>"
#define		BROCHUREID_STARTTAG			L"<brochureId>"
#define		BROCHUREID_ENDTAG			L"</brochureId>"
#define		GOODSID_STARTTAG			L"<goodsId>"
#define		GOODSID_ENDTAG				L"</goodsId>"
#define		PAGEID_STARTTAG				L"<pageId>"
#define		PAGEID_ENDTAG				L"</pageId>"
#define		FRAMEID_STARTTAG			L"<frameId>"
#define		FRAMEID_ENDTAG				L"</frameId>"
#define		CONTENTSID_STARTTAG			L"<contentsId>"
#define		CONTENTSID_ENDTAG			L"</contentsId>"
#define		BROCHURETYPE_STARTTAG		L"<brochureType>"
#define		BROCHURETYPE_ENDTAG			L"</brochureType>"
#define		MEDIATYPE_STARTTAG			L"<mediaType>"
#define		MEDIATYPE_ENDTAG			L"</mediaType>"
#define		FILENAME_STARTTAG			L"<filename>"
#define		FILENAME_ENDTAG				L"</filename>"
#define		LOCATION_STARTTAG			L"<location>"
#define		LOCATION_ENDTAG				L"</location>"
#define		VOLUME_STARTTAG				L"<volume>"
#define		VOLUME_ENDTAG				L"</volume>"
#define		ZORDER_STARTTAG				L"<zorder>"
#define		ZORDER_ENDTAG				L"</zorder>"
#define		RUNNINGTIME_STARTTAG		L"<runningTime>"
#define		RUNNINGTIME_ENDTAG			L"</runningTime>"
#define		PARENTID_STARTTAG			L"<parentId>"
#define		PARENTID_ENDTAG				L"</parentId>"
#define		USAGETYPE_STARTTAG			L"<usageType>"
#define		USAGETYPE_ENDTAG			L"</usageType>"
#define		CREATORID_STARTTAG			L"<creatorId>"
#define		CREATORID_ENDTAG			L"</creatorId>"
#define		LASTUPDATETIME_STARTTAG		L"<lastUpdateTime>"
#define		LASTUPDATETIME_ENDTAG		L"</lastUpdateTime>"
#define		DESCRIPTION_STARTTAG		L"<description>"
#define		DESCRIPTION_ENDTAG			L"</description>"
#define		FILENAME_ORI_STARTTAG		L"<filenameOri>"
#define		FILENAME_ORI_ENDTAG			L"</filenameOri>"
#define		TIMEFLAG_STARTTAG			L"<timeFlag>"
#define		TIMEFLAG_ENDTAG				L"</timeFlag>"
#define		ULASTUPDATETIME_STARTTAG	L"<uLastUpdateTime>"
#define		ULASTUPDATETIME_ENDTAG		L"</uLastUpdateTime>"
#define		FILENAME_ALT_STARTTAG		L"<filenameAlt>"
#define		FILENAME_ALT_ENDTAG			L"</filenameAlt>"


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
enum OFS_CONTENTS_TYPE {
	OFS_UNION_CONTENTS_TYPE = 0,
	OFS_BROCHURE_CONTENTS_TYPE,
	OFS_GOODS_CONTENTS_TYPE,
	OFS_DELETE_CONTENTS_TYPE,
};
/*
enum OFS_BROCHURE_TYPE {
	OFS_BROCHURE_UNKNOWN = 0,
	OFS_BROCHURE_SMARTWINDOW,
	OFS_BROCHURE_SMARTMIRROR,
	OFS_BROCHURE_SMARTHANGER,
	OFS_BROCHURE_SMARTSHOWCASE,
	OFS_BROCHURE_SMARTSHELF,
	OFS_BROCHURE_ETC,
};

enum OFS_MEDIA_TYPE {
	OFS_MEDIA_UNKNOWN = -1,
	OFS_MEDIA_VIDEO = 0,		// 비디오 (0)
	OFS_MEDIA_IMAGE,			// 이미지 (1)
	OFS_MEDIA_URL,				// 웹주소 (2)
	OFS_MEDIA_FLASH,			// 플래시 (3)
	OFS_MEDIA_ETC = 99,			// 기타 (99)
};

enum OFS_USAGE_TYPE {
	OFS_USAGE_UNKNOWN = 0,
	OFS_USAGE_CF_VIDEO,			// 연관CF동영상 (1)
	OFS_USAGE_REPRESENTATIVE,	// 상품대표이미지 (2)
	OFS_USAGE_FRONT,			// 정면이미지 (3)
	OFS_USAGE_SIDE,				// 측면이미지 (4)
	OFS_USAGE_RIGHTSIDE,		// 우 측면 이미지 (5)
	OFS_USAGE_LEFTSIDE,			// 좌 측면 이미지 (6)
	OFS_USAGE_BACK,				// 후면이미지 (7)
	OFS_USAGE_WEAR,				// 착용샷 (8)
	OFS_USAGE_THUMBNAIL,		// 썸네일 (9)
	OFS_USAGE_COLORSAMPLE,		// 색상 샘플 (10)
	OFS_USAGE_BLUEPRINT,		// 사이즈도면 (11)
	OFS_USAGE_ETC = 99,			// 기타 (99)
};
*/

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class contentsInfo;
typedef		list<contentsInfo*>			contentsInfoList;
typedef		contentsInfoList::iterator	contentsInfoListIter;


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
ciString	_getItem(const char* xml, const char* starttag, const char* endtag);
ciWString	_getItem(const wchar_t* xml, const wchar_t* starttag, const wchar_t* endtag);
ciBoolean	_findTag(const wchar_t* xml, const wchar_t* tag);


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class contentsInfo
{
public:
	static	ciBoolean		getContentsInfoList(contentsInfoList& infoList, ciWString& strXML);
	static	void			clearContentsInfoList(contentsInfoList& infoList);
	static	contentsInfo*	getContentsInfoObject(const wchar_t* xml, OFS_CONTENTS_TYPE type=OFS_UNION_CONTENTS_TYPE);

protected:
//	static ciString		_getItem(const char* xml, const char* starttag, const char* endtag);
//	static ciBoolean	_findTag(const char* xml, const char* tag);


/////////////////////////////////////////////////////////////////
public:
	contentsInfo();
	virtual ~contentsInfo() {};

	// class attributes
	OFS_CONTENTS_TYPE	contentsType;

	// contents attributes
	ciWString		mediaType;		//
	ciWString		filename;		//콘텐츠파일명/Area 명
	ciWString		location;		//서버파일 경로
	ciWString		locationOri;	//원본 서버파일 경로
	ULONGLONG		volume;			//file size
	ciTime			lastUpdateTime;

	ciWString		filenameOri;	//원본 콘텐츠파일명
	ciWString		filenameAlt;	//old 원본 콘텐츠파일명
	ciWString		timeFlag;		//배포시간 제한

	ciWString		localPath;		//로컬 전체경로 (C:\SQISoft\ENC\...filename)
	ciWString		tempPath;		//로컬임시다운로드 전체경로 (C:\SQISoft\Temp\...filename)
	ciWString		bakPath;		//로컬백업 전체경로 (C:\SQISoft\ENC\...filename.bak)
	ciWString		altPath;		//old 로컬 전체경로

	ciBoolean		existLocalPath;
	ciBoolean		existTempPath;

	//
	virtual void			init();

	virtual ciBoolean		fromString(const wchar_t* xml);
	virtual ciWString		toString(ciBoolean xml_type=ciTrue);

	virtual contentsInfo*	duplicate();
	virtual ciBoolean		isEqual(contentsInfo* info);

	virtual	int				isExistFile(ULONGLONG* pFileSize=NULL); // -1:diff-size, 0:not-exist, 1:exist, pFileSize = return real-file-size
	virtual	int				isExistTempFile(ULONGLONG* pFileSize=NULL); // -1:diff-size, 0:not-exist, 1:exist, pFileSize = return real-file-size

	virtual	ciBoolean		replaceFile();

	static ciBoolean		removeDirectory(const wchar_t* path);

private:
	ciBoolean	_getLocalPath(const wchar_t* findStr, const wchar_t* replaceStr, const wchar_t* path, time_t tm);
};

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class brochureContentsInfo : public contentsInfo
{
public:
	brochureContentsInfo();
	virtual ~brochureContentsInfo() {};

	// brochure-contents attributes
	ciWString		mgrId;			//서버아이디
	ciWString		siteId;			//브랜드 ID
	ciWString		brochureId;		//브로셔 ID
	ciWString		pageId;			//page ID
	ciWString		frameId;		//frame ID
	ciWString		contentsId;		//Contents Id
	ciWString		brochureType;	//
	int				zorder;			//화면이 나올 순서
	int				runningTime;	//플레이시간(초)
	ciWString		creatorId;		//등록자Id
	ciWString		description;	//설명

	//
	virtual void	init();

	virtual ciBoolean		fromString(const wchar_t* xml);
	virtual ciWString		toString(ciBoolean xml_type=ciTrue);

	virtual contentsInfo*	duplicate();
	virtual ciBoolean		isEqual(contentsInfo* info);

};


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class goodsContentsInfo : public contentsInfo
{
public:
	goodsContentsInfo();
	virtual ~goodsContentsInfo() {};

	// goods-contents attributes
	ciWString		mgrId;			//서버아이디
	ciWString		siteId;			//브랜드 ID
	ciWString		goodsId;
	ciWString		contentsId;		//Contents Id
	ciWString		filename;		//콘텐츠파일명/Area 명
	ciWString		location;		//파일 경로
	ULONGLONG		volume;			//file size
	ciWString		parentId;
	ciWString		usageType;
	ciWString		creatorId;		//등록자Id
	ciWString		description;	//설명

	//
	virtual void	init();

	virtual ciBoolean		fromString(const wchar_t* xml);
	virtual ciWString		toString(ciBoolean xml_type=ciTrue);

	virtual contentsInfo*	duplicate();
	virtual ciBoolean		isEqual(contentsInfo* info);

};

ciBoolean createFullDirectory(const wchar_t* path, ciBoolean isFilenameIncluded=ciTrue);

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class autoDeleter
{
public:
	autoDeleter(char* p) { _init(); _psz = p; };
	autoDeleter(wchar_t* wp) { _init(); _wpsz = wp; };
	autoDeleter(contentsInfo* info) { _init(); _info = info; };
	~autoDeleter() { _clear(); };

protected:
	char* _psz;
	wchar_t* _wpsz;
	contentsInfo* _info;

	void _init()
	{
		_psz = NULL;
		_wpsz = NULL;
		_info = NULL;
	};

	void _clear()
	{
		if( _psz != NULL ) delete _psz;
		if( _wpsz != NULL ) delete _wpsz;
		if( _info != NULL ) delete _info;
	};
};

#define	AUTO_DELETE(var)		autoDeleter __deleter_##var (var);

void stringReplace(ciWString& target, const wchar_t* from, const wchar_t* to);

#endif // _contentsInfo_h_