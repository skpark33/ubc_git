#ifndef _contentsCheckTimer_H_
#define _contentsCheckTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>

//class contentsInfoList;
class contentsManagerTimer;
#include "contents.h"


class contentsCheckTimer	: public ciWinPollable
{
public:
	contentsCheckTimer(	const char* site, 
						const char* host, 
						const char* ip, 
						ciLong port, 
						const char* id, 
						const char* pwd );
	virtual ~contentsCheckTimer();

	virtual void processExpired(ciString name, int counter, int interval);

	ciBoolean	isInitialized() { return _initialize; };
	ciBoolean	isDownloadingContents(const wchar_t* location);

	static ciString	flashPath;
	static ciString	flashSubPath;
	static ciString	flashFilename;
	static ciString	flashArgument;
protected:
	ciBoolean	_initialize;

	ciString	_site;
	ciString	_host;

	ciString	_svrIp;
	ciLong		_svrPort;
	ciString	_svrId;
	ciString	_svrPwd;

	//ciString	_cmsServerIp;
	//ciLong		_cmsServerPort;
	//ciString	_contentsDownloadListURL;
	//ciString	_brochureIdURL;
	//ciString	_brochureXMLURL;

	ciTime		_lastSyncTime;
	ciTime		_lastestContentsTime;

	ciString	_prevBrochureId;
	ciTime		_prevBrochureTime;
	ciString	_runningBrochureId;
	ciTime		_runningBrochureTime;

	ciWString	_brochureXmlPath;

	contentsManagerTimer*	_brochConMngTimer;
	contentsManagerTimer*	_goodsConMngTimer;
	contentsManagerTimer*	_unionConMngTimer;
	contentsManagerTimer*	_deleteConMngTimer;

	void		_initTimer();
	ciBoolean	_checkContentsFile(contentsInfoList* infoList, int& goodsCount, int& brochureCount);
	void		_clearContentsInfoList(contentsInfoList* infoList);
	ciBoolean	_setLastUpdateTime(ciTime tm);
	ciBoolean	_updateLastSyncTime();

	ciString	_getAnsiXML(const char* xml);
	ciWString	_getUnicodeXML(const char* xml);
	ciLong		_getDownloadRemainCount();
	ciLong		_getAllRemainCount();
	ciBoolean	_httpRequestPost(const char* url, const char* param, ciString& result);

	ciBoolean	_getContentsDownloadListXML(ciWString& strUniXml);
	ciBoolean	_getBrochureId(ciString& brochureId, ciTime& brochureTime);
	ciBoolean	_getBrochureXml(const char* brochureId, ciWString& strUniXML);
	ciLong		_getBrochureContentsInfoList(const wchar_t* xml, contentsInfoList* infoList);
	ciBoolean	_writeXML(const wchar_t* xml, ciBoolean overwrite=ciTrue);

	ciWString	_getXMLforKHQS(contentsInfoList* infoList);
	ciWString	_getXMLforHMCSFS(contentsInfoList* infoList);

	ciBoolean	_replaceContentsFiles(contentsInfoList* infoList);

	const wchar_t*	_getContentsType(ciWString location);

public:
	static ciBoolean	_killFlashProcess(const char* process_name=NULL);
	static ciULong		_createProcess(const char *exe, const char* dir, const char* argument, BOOL minFlag=FALSE);
	static ciULong		_startFlashProcess();
	static ciULong		_getFlashPID();

};

#endif //_contentsCheckTimer_H_
