/*! \file ciWStringTokenizer.h
 *
 *  Copyright ⓒ 2003 WINCC Inc.
 *  All Rights Reserved.
 *
 *  \brief string 을 delimiters 로 Tokenning 한다.
 *  (Environment: OSF1 5.1A)
 *
 *  \author 	jhchoi
 *  \version
 *  \date 		2003년 4월 22일 18:00
 */


#ifndef _ciWStringTokenizer_h_
#define _ciWStringTokenizer_h_

#include <ci/libBase/ciListType.h>

//ciWStringTokenizer Class
/**
 * The string tokenizer class allows an application to break a 
 * string into tokens. The tokenization method is simple.
 *
 * @param	str 			: a string to be parsed.
 * @param	delim 		: the char type delimiters.
 * @param	deliList		: the string type delimiters.
 * @param	returnDelims	: flag indicating whether to return the delimiters as tokens.
 * @param	nullToken		: flag indicating whether to return the null string as tokens
 */
class ciWStringTokenizer {
public:

	//Default Delimiters \n,\t,\r,\f,space로 문자열을 Tokenning 한다.
	ciWStringTokenizer(const ciWString& str);
	ciWStringTokenizer(const wchar_t* str);

 	ciWStringTokenizer(const ciWString& str, const ciWString& delim, ciBoolean nullToken = ciFalse,
				ciBoolean returnDelims = ciFalse);
	ciWStringTokenizer(const wchar_t* str, const wchar_t* delim, ciBoolean nullToken = ciFalse,
				ciBoolean returnDelims = ciFalse);
	//string delimiters
	ciWStringTokenizer(const ciWString& str, const ciWStringList& deliList, ciBoolean nullToken = ciFalse,
				ciBoolean returnDelims  = ciFalse);
	ciWStringTokenizer(const wchar_t* str, const ciWStringList& deliList, ciBoolean nullToken = ciFalse,
				ciBoolean returnDelims = ciFalse);

	~ciWStringTokenizer();

	ciBoolean hasMoreTokens();
	ciInt countTokens();	
	
	ciWString nextToken();
	ciWString nextToken(const ciWString& delim);
	ciWString nextToken(const wchar_t* delim);
	ciWString nextToken(const ciWStringList& deliList); //string delimiters
	ciWString nextToken(ciInt n);
	ciWString restTokens();
	
	void nextToken_r(ciWString& out);
	void nextToken_r(const ciWString& delim, ciWString& out);
	void nextToken_r(const wchar_t* delim, ciWString& out);
	void nextToken_r(const ciWStringList& deliList, ciWString& out); //string delimiters
	void nextToken_r(ciInt n, ciWString& out);
	void restTokens_r(ciWString& out);

private:
	void _ciWStringTokenizer(const wchar_t* str, const wchar_t* delim = L" \n\t\r\f", 
					ciBoolean nullToken = ciFalse, ciBoolean retDelims = ciFalse);
	//string delimiters
	void _ciWStringTokenizer(const wchar_t* str, const ciWStringList& deliList, 
					ciBoolean nullToken = ciFalse, ciBoolean retDelims = ciFalse);
	void _setMaxDelimChar();
	void _setMaxDelimString(); //string delimiters
	ciInt _skipDelimiters(ciInt startPos);
	ciInt _scanToken(ciInt startPos);
	
	ciWString _str;
	ciWString _delimiters;
	ciWStringList _delimStrList; //string delimiters
	/**
	* maxDelimChar stores the value of the delimiter character with the
	* highest value. It is used to optimize the detection of delimiter
	* characters.
	*/	
	wchar_t _maxDelimChar;
	ciWString _maxDelimString; //string delimiters
	ciBoolean _retDelims;
	ciBoolean _delimsChanged;
	ciBoolean _nullToken;
	ciBoolean _strDelims;
	ciInt _currentPosition;
	ciInt _newPosition;
	ciInt _maxPosition;
};

#endif //_ciWStringTokenizer_h_

