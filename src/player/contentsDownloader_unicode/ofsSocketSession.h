#ifndef _ofsSocketSession_h_
#define _ofsSocketSession_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libBase/ciTime.h>
//#include "reservedEventHandler.h"


////////////////////
class ofsSocketSession;
class reservedEvtHandler;

class ofsSocketHandler : public ciThread {
public:
	static ofsSocketHandler*	getInstance();
	static void	clearInstance();

	virtual ~ofsSocketHandler() {}

	void setSession(ofsSocketSession* session) { _session=session;}
	void fini();
	void run();
	void destroy();
	void push(const char* recvMsg);

protected:
	ofsSocketHandler() : _session(0), _isAlive(ciTrue) {}
	static ofsSocketHandler*		_instance;
	static ciMutex			_instanceLock;

	ciMutex				_listLock;
	ciStringList		_list;
	ciBoolean			_isAlive;
	ofsSocketSession*	_session;
};

class ofsSocketSession : public ciThread {
public:
	ofsSocketSession(int port, reservedEvtHandler* handler);
	virtual ~ofsSocketSession();

	ciBoolean init();
	void fini();

	void run();
	void destroy();

	ciBoolean _processCmd(const char* recvMsg);

protected:
	reservedEvtHandler* _handler;
	ciBoolean _isAlive;
	int _serverFd;
	int _serverPort;
};

#endif // _ofsSocketSession_h_
