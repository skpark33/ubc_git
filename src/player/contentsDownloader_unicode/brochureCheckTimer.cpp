#include "common/libHttpRequest/HttpRequest.h"
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <util/libEncoding/Encoding.h>
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchUtil.h"
#include "brochureCheckTimer.h"
#include "contentsCheckTimer.h"
#include "contentsDownloadTimer.h"
//#include "downloadTimer.h"
#include "contents.h"
#include "contentsDownloaderWorld.h"


#if defined(_WIN32)
#   include <winsock2.h>
//  WinSock DLL을 사용할 버전
#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0
#   include <time.h>
#else
#   include <sys/socket.h>       /* for socket(), connect(), send(), and recv() */
#   include <arpa/inet.h>        /* for sockaddr_in and inet_addr() */
#endif


#define		OFS_FLASH_EXE_PATH				"C:\\SQISoft\\Contents\\ENC\\"
#define		OFS_FLASH_XML_FULLPATH			"C:\\SQISoft\\Contents\\ENC\\OFS\\brochure\\xml\\default_play.xml"
#define		KHQS_FLASH_XML_FULLPATH			"C:\\SQISoft\\Contents\\ENC\\KHQS\\xml\\default_play.xml"
#define		HMCSFS_FLASH_XML_FULLPATH		"C:\\SQISoft\\Contents\\ENC\\PDIS\\xml\\default_play.xml"

#define		CONTENTS_IN_BROCHURE_STARTTAG	"<contents"
#define		CONTENTS_IN_BROCHURE_ENDTAG		"</contents>"


ciSET_DEBUG(9, "brochureCheckTimer");


ciString	brochureCheckTimer::flashPath = "";
ciString	brochureCheckTimer::flashSubPath = "";
ciString	brochureCheckTimer::flashFilename = "";


brochureCheckTimer::brochureCheckTimer(const char* site, const char* host, contentsCheckTimer* timer) 
{
	ciDEBUG(5, ("brochureCheckTimer(%s,%s)", site, host) );

	_site = site;
	_host = host;
	_contentsCheckTimer = timer;

	_prevFlashPid = 0;
	_prevBrochureId = "";
	_prevBrochureTime.set((time_t)1);

	//
	_getCMSInfo();

	//
//	_getClientInfo();

	//
	_getFlashEnv();

	ciDEBUG(5, ("brochureCheckTimer()"));
}

brochureCheckTimer::~brochureCheckTimer()
{
	ciDEBUG(5, ("~brochureCheckTimer()"));
}

ciString getAnsiXML(const char* xml)
{
	ciString str_decoding = "";
	char* tmp = (char*)strstr(xml, "encoding=\"");
	if( tmp != NULL && _strnicmp(tmp+10,"utf-8",5) == 0 )
	{
		UTF8toA(xml, str_decoding);
	}
	else
	{
		str_decoding = xml;
	}

	return str_decoding;
}

ciString getAnsiXML(ciString& xml)
{
	return getAnsiXML(xml.c_str());
}

void brochureCheckTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(5, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	//
	//char url[1024] = "api/brochureInfoView/Api008_01.do";
	char sendmsg[1024];
	sprintf(sendmsg, "siteId=%s&hostId=%s&returnType=XML", _site.c_str(), _host.c_str() );
	//sprintf(sendmsg, "hostId=HOST_001&returnType=XML" );

	ciDEBUG(5, ("_cmsServerIP(%s), _cmsServerPort(%d), sendMsg(%s)", _cmsServerIP.c_str(), _cmsServerPort, sendmsg) );

	while( true )
	{
		CHttpRequest http_request;
		http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _cmsServerIP.c_str(), _cmsServerPort);
		if( ciArgParser::getInstance()->isSet("+HMCSFS") )
			http_request.AddAdditionalHeader(_getAdditionHttpHeader().c_str());

		//
		CString str_buf = "";
		if( !http_request.RequestPost(_cmsIdUrl.c_str(), sendmsg, str_buf) )
		{
			ciWARN( ("RequestPost FAIL !!! (%s?%s)", _cmsIdUrl.c_str(), sendmsg) );
			break;
		}
		ciString str_xml = (LPCSTR)str_buf;
		ciString str_xml_ansi = getAnsiXML(str_xml);
		ciDEBUG(5, ("RequestPost(%s)", str_xml_ansi.c_str()) );

		// get brochure-id
		ciString str_bid, str_err;
		ciTime tm_lastupdate;
		if( _getBrochureInfo(str_xml_ansi.c_str(), str_bid, tm_lastupdate, str_err) == ciFalse )
		{
			// ERROR !!!
			ciERROR( ("_getBrochureInfo FAIL !!!") );
			break;
		}

		//
		if( _checkBrochure(str_bid.c_str(), tm_lastupdate) == ciFalse )
		{
			ciWARN( ("_checkBrochure FAIL !!! (%s, %I64u)", str_bid.c_str(), tm_lastupdate.getTime()) );
			break;
		}

		// all success
		ciDEBUG(5, ("processExpired() success") );
		return;
	}

	//
	_prevFlashPid = _getFlashPID();
	if( _prevFlashPid == 0 )
		_prevFlashPid = _startFlashProcess();

	ciDEBUG(5, ("processExpired() end") );

	return;
}

ciBoolean brochureCheckTimer::_getBrochureInfo(const char* xml, ciString& bId, ciTime& tm, ciString& err)
{
	//
	ciString str_result = _getItem(xml, "<resultFlag>", "</resultFlag>");
	ciStringUtil::trim(str_result);

	err = _getItem(xml, "<resultMsg>", "</resultMsg>");
	ciStringUtil::trim(err);

	bId = _getItem(xml, "<brochureId>", "</brochureId>");
	ciStringUtil::trim(bId);

	ciString str_tm = _getItem(xml, "<lastUpdateTime>", "</lastUpdateTime>");
	tm = _atoi64(str_tm.c_str());

	ciDEBUG(5, ("resultFlag : %s", str_result.c_str()) );
	ciDEBUG(5, ("resultMsg : %s", err.c_str()) );
	ciDEBUG(5, ("brochureId : %s", bId.c_str()) );
	ciDEBUG(5, ("lastUpdateTime : %I64d", tm) );

	if( stricmp(str_result.c_str(),"true") == 0 )
	{
		return ciTrue;
	}

	return ciFalse;
}

ciBoolean brochureCheckTimer::_checkBrochure(const char* bId, ciTime tm)
{
	int cur_flash_pid = _getFlashPID();
	if( _prevFlashPid != 0 &&
		_prevFlashPid == cur_flash_pid &&
		_prevBrochureId == bId &&
		_prevBrochureTime == tm )
	{
		// nothing to push
		return ciTrue;
	}

	//
	// something is changed or different  ==>  change to new-brochure
	//

	ciDEBUG(5, ("pid : %d, %d", _prevFlashPid, cur_flash_pid) );
	ciDEBUG(5, ("bid : %s, %s", _prevBrochureId.c_str(), bId) );
	ciDEBUG(5, ("tm  : %I64d, %I64d", _prevBrochureTime.getTime(), tm.getTime()) );

/*
	ciString retval = "";
	if( !_pushBrochureId(bId, retval) )
	{
		// push ERROR
		ciERROR( ("_pushBrochureId(%s) ERROR !!! (%s)", bId, retval.c_str()) );
		return ciFalse;
	}
	if( retval.length() < 2 )
	{
		// invalid data
		ciWARN( ("invalid RETURN-DATA !!! (%s)", retval.c_str()) );
		return ciFalse;
	}
	if( stricmp(retval.c_str()+strlen(retval.c_str())-2, "OK") != 0 )
	{
		// response ERROR
		ciWARN( ("response ERROR !!! (%s)", retval.c_str()) );
		return ciFalse;
	}
*/

	// get brochure-xml
	ciString str_xml;
	if( !_getBrochureXML(bId, str_xml) )
	{
		// get xml ERROR !!!
		ciERROR( ("_getBrochureXML(%s) ERROR !!! (%s)", bId, str_xml.c_str()) );
		return ciFalse;
	}

	// utf-8 ==> ansi decoding 
	ciString str_xml_ansi = getAnsiXML(str_xml);
	// check contents
	if( !_contentsCheck(str_xml_ansi.c_str()) )
	{
		// get xml ERROR !!!
		ciWARN( ("_contentsCheck(%s) ERROR !!!", bId) );
		return ciFalse;
	}

	// "<location>contents/uploadData"  ==>  "<location>"
	ciStringUtil::stringReplace(str_xml, "<location>/contents/uploadData", "<location>");
	ciStringUtil::stringReplace(str_xml, "<location>contents/uploadData", "<location>");
	// "<location>contents/KHQS"  ==>  "<location>"
	ciStringUtil::stringReplace(str_xml, "<location>/contents/KHQS", "<location>");
	ciStringUtil::stringReplace(str_xml, "<location>contents/KHQS", "<location>");
	// "<location>contents/KHQS"  ==>  "<location>"
	ciStringUtil::stringReplace(str_xml, "<location>/files/pmis", "<location>");
	ciStringUtil::stringReplace(str_xml, "<location>files/pmis", "<location>");

	// write brochure-xml
	if( ciArgParser::getInstance()->isSet("+KHQS") )
	{
		// only use KHQS
		str_xml = _getXMLforKHQS(str_xml.c_str());
		ciDEBUG(5, ("xml is converted for KHQS (%s)", getAnsiXML(str_xml).c_str()) );
	}
	else if( ciArgParser::getInstance()->isSet("+HMCSFS") )
	{
		// only use KHQS
		str_xml = _getXMLforHMCSFS(str_xml.c_str());
		ciDEBUG(5, ("xml is converted for HMCSFS (%s)", getAnsiXML(str_xml).c_str()) );
	}

	if( !_writeXML(str_xml.c_str()) )
	{
		// get xml ERROR !!!
		ciERROR( ("_writeXML(%s) ERROR !!!", bId) );
		return ciFalse;
	} 

	// kill flash process
	if( flashFilename.length() > 0 && !_killFlashProcess() )
	{
		// kill ERROR !!!
		ciERROR( ("_killFlashProcess(%s) ERROR !!!", bId) );
		return ciFalse;
	}

	// startup flash process
	if( flashFilename.length() > 0 )
	{
		cur_flash_pid = _startFlashProcess();
		if( cur_flash_pid == 0 )
		{
			// start ERROR !!!
			ciERROR( ("_startFlashProcess(%s) ERROR !!!", bId) );
			return ciFalse;
		}
	}

	_prevFlashPid = cur_flash_pid;
	_prevBrochureId = bId;
	_prevBrochureTime = tm;

	ubcConfig aIni("UBCVariables");
	aIni.set("OFS_CMS_INFO","LastBrochureId", bId);
	char buf[32] = {0};
	sprintf(buf, "%I64d", tm.getTime());
	aIni.set("OFS_CMS_INFO","LastBrochureTime", buf);

	return ciTrue;
}
/*
#define		CLEANUP_RETURN(y)				{ WSACleanup(); return y; }
//#define		CLOSE_CLEANUP_RETURN(x,y)	{ closesocket(x); WSACleanup(); return y; }
#define		CLOSE_CLEANUP_RETURN(x,y)		{ closesocket(x); CLEANUP_RETURN(y); }

ciBoolean brochureCheckTimer::_pushBrochureId(const char* bId, ciString& retval)
{
	ciDEBUG(5, ("_pushBrochureId(%s:%d,%s)", _clientIP.c_str(), _clientPort, bId) );

	// Load WinSock DLL
	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA WsaData;
	if( WSAStartup(VersionRequested, &WsaData) != 0 )
	{
		ciERROR( ("WSAStartup ERROR !!!") );
		CLEANUP_RETURN(ciFalse);
	}

	// create socket
	SOCKET socket_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if( socket_fd < 0 )
	{
		ciERROR( ("SOCKET OPEN ERROR !!!") );
		CLEANUP_RETURN(ciFalse);
	}

	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(_clientIP.c_str());
	server_addr.sin_port = htons(_clientPort);

	// connect socket
	if( connect(socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0 )
	{
		ciERROR( ("[%s:%d] CONNECT ERROR !!!", _clientIP.c_str(), _clientPort) );
		CLOSE_CLEANUP_RETURN(socket_fd,ciFalse);
	}

	// create data
	ciString value = _createPacket(bId); // utf-8
	//char buf[1024];
	//sprintf(buf, "%010d", str_xml_utf8.length());
	//ciString value = "~AGNTTEXT0";
	//value += buf;
	//value += str_xml_utf8;

	ciDEBUG(5, ("packet(%s) send to [%s:%d]", value.c_str(), _clientIP.c_str(), _clientPort) );

	// send data
	int total_len = value.length();
	int total_send = 0;
	int retry = 0;
	while( total_len > total_send && retry < 10 ) // retry while 1-sec
	{
		if( retry > 0 ) ::Sleep(100);

		int msg_size = send(socket_fd, value.c_str()+total_send, total_len-total_send, 0);
		if( msg_size == SOCKET_ERROR )
		{
			ciERROR( ("send ERROR !!! (errcode:%d)", ::WSAGetLastError()) );
			break;
		}

		ciDEBUG(5, ("data(%d byte) send to [%s:%d]", msg_size, _clientIP.c_str(), _clientPort) );
		total_send += msg_size;
		retry++;
	}

	// occur error
	if( total_len > total_send )
	{
		ciERROR( ("ERROR or TIMEOUT for SENDING !!!") );
		CLOSE_CLEANUP_RETURN(socket_fd,ciFalse);
	}

	// receive data
	timeval ReceiveTimeout;
	ReceiveTimeout.tv_sec  = 3; // 3-sec
	ReceiveTimeout.tv_usec = 0;

	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(socket_fd, &fds);

	ciDEBUG(5, ("waiting for receiving [%s:%d]...", _clientIP.c_str(), _clientPort) );
	int iRC = select(2, &fds, NULL, NULL, &ReceiveTimeout);
	// Timeout
	if( iRC == 0 )
	{
		ciERROR( ("TIMEOUT for RECEIVING !!!") );
		CLOSE_CLEANUP_RETURN(socket_fd,ciFalse);
	}
	// Error
	if( iRC < 0 )
	{
		ciERROR( ("select ERROR !!!") );
		CLOSE_CLEANUP_RETURN(socket_fd,ciFalse);
	}
	if( !FD_ISSET(socket_fd, &fds) )
	{
		ciERROR( ("something ERROR !!!") );
		CLOSE_CLEANUP_RETURN(socket_fd,ciFalse);
	}

	// Receive OK
	retval = "";
	char buf[1024];
	while( true )
	{
		memset(buf, 0x00, sizeof(buf));
		int msg_size = recv(socket_fd, buf, 2, 0);
		if( msg_size == 0 )
		{
			ciDEBUG(5, ("connection gracefully closed.") );
			break;
		}
		if( msg_size < 0 )
		{
			ciERROR( ("TIMEOUT for RECEIVING !!! (errcode:%d)", ::WSAGetLastError()) );
			break;
		}
		ciDEBUG(5, ("data(%d byte) received from [%s:%d]", msg_size, _clientIP.c_str(), _clientPort) );
		retval += buf;
	}

	ciDEBUG(5, ("packet(%s) received from [%s:%d]", retval.c_str(), _clientIP.c_str(), _clientPort) );

	CLOSE_CLEANUP_RETURN(socket_fd,ciTrue);
}
*/
ciBoolean brochureCheckTimer::_getCMSInfo()
{
	// get cms-server-ip
	ciString buf = "";
	ubcConfig aIni("UBCVariables");
	aIni.get("OFS_CMS_INFO","ServerIP", buf, "");
	ciDEBUG(5, ("ServerIP(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_cmsServerIP = buf;
	if( _cmsServerIP.length() == 0 )
	{
		_cmsServerIP = "127.0.0.1";
		aIni.set("OFS_CMS_INFO","ServerIP", _cmsServerIP.c_str());
		ciDEBUG(5, ("new ServerIP(%s)", _cmsServerIP.c_str()) );
	}

	// get cms-server-port
	buf = "";
	aIni.get("OFS_CMS_INFO","ServerPort", buf, "");
	ciDEBUG(5, ("ServerPort(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_cmsServerPort = atoi(buf.c_str());
	if( _cmsServerPort == 0 )
	{
		_cmsServerPort = 8000;
		aIni.set("OFS_CMS_INFO","ServerPort", _cmsServerPort);
		ciDEBUG(5, ("new ServerPort(%d)", _cmsServerPort) );
	}

	// get cms-brochure-id-url
	buf = "";
	aIni.get("OFS_CMS_INFO","BrochureIdUrl", buf, "");
	ciDEBUG(5, ("BrochureIdUrl(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_cmsIdUrl = buf;
	if( _cmsIdUrl.length() == 0 )
	{
		_cmsIdUrl = "api/brochureInfoView/Api008_01.do";
		aIni.set("OFS_CMS_INFO", "BrochureIdUrl", _cmsIdUrl.c_str());
		ciDEBUG(5, ("new BrochureIdUrl(%s)", _cmsIdUrl.c_str()) );
	}

	// get cms-brochure-xml-url
	buf = "";
	aIni.get("OFS_CMS_INFO","BrochureXmlUrl", buf, "");
	ciDEBUG(5, ("BrochureXmlUrl(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_cmsXmlUrl = buf;
	if( _cmsXmlUrl.length() == 0 )
	{
		_cmsXmlUrl = "api/brochureInfoView/Api008.do";
		aIni.set("OFS_CMS_INFO", "BrochureXmlUrl", _cmsXmlUrl.c_str());
		ciDEBUG(5, ("new BrochureXmlUrl(%s)", _cmsXmlUrl.c_str()) );
	}

	// get last-brochure-id & time
	buf = "";
	aIni.get("OFS_CMS_INFO","LastBrochureId", buf, "");
	ciDEBUG(5, ("LastBrochureId(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_prevBrochureId = buf;

	aIni.get("OFS_CMS_INFO","LastBrochureTime", buf, "1");
	ciDEBUG(5, ("LastBrochureTime(%s)", buf.c_str()) );
	time_t last_bro_time = _atoi64(buf.c_str());
	_prevBrochureTime.set(last_bro_time);

	return ciTrue;
}
/*
ciBoolean brochureCheckTimer::_getClientInfo()
{
	//
	ciString buf = "";
	ubcConfig aIni("UBCVariables");
	aIni.get("OFS_CLIENT_INFO","ClientIP", buf, "");
	ciDEBUG(5, ("ClientIP(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_clientIP = buf;
	if( _clientIP.length() == 0 )
	{
		_clientIP = "127.0.0.1";
		aIni.set("OFS_CLIENT_INFO","ClientIP", _clientIP.c_str());
		ciDEBUG(5, ("new ClientIP(%s)", _clientIP.c_str()) );
	}

	//
	buf = "";
	aIni.get("OFS_CLIENT_INFO","ClientPort", buf, "");
	ciDEBUG(5, ("ClientPort(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_clientPort = atoi(buf.c_str());
	if( _clientPort == 0 )
	{
		_clientPort = 9000;
		aIni.set("OFS_CLIENT_INFO","ClientPort", _clientPort);
		ciDEBUG(5, ("new ClientPort(%d)", _clientPort) );
	}

	return ciTrue;
}

ciString brochureCheckTimer::_createPacket(const char* bId)
{
	ciDEBUG(5, ("_createPacket(%s)", bId) );

	char buf[1024];
	sprintf(buf, "~AGNTTEXT0%010dchangeBrochure|%s", strlen(bId)+15, bId);

	ciDEBUG(5, ("packet=%s)", buf) );

	ciString str_utf8;
	AtoUTF8(buf, str_utf8);

	return str_utf8;

	//ciString str_xml = "";

	//// create xml
	//str_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
	//str_xml += "<ELIGA>\r\n";
	//str_xml += "	<changeBrochure>\r\n";
	//str_xml += "		<brochureId>";
	//str_xml +=			bId;
	//str_xml += "		</brochureId>\r\n";
	//str_xml += "	</changeBrochure>\r\n";
	//str_xml += "</ELIGA>";

	//ciDEBUG(5, ("_createXML(%s)", str_xml.c_str()) );

	//ciString str_utf8;
	//AtoUTF8(str_xml.c_str(), str_utf8);

	////ciDEBUG(5, ("_createXML(%s)", str_xml.c_str()) );
	//return str_utf8;

}
*/

ciBoolean brochureCheckTimer::_getFlashEnv()
{
	// brochure xml path
	if     ( ciArgParser::getInstance()->isSet("+KHQS") )
		_brochureXmlPath = KHQS_FLASH_XML_FULLPATH;
	else if( ciArgParser::getInstance()->isSet("+HMCSFS") )
		_brochureXmlPath = HMCSFS_FLASH_XML_FULLPATH;
	else
		_brochureXmlPath = OFS_FLASH_XML_FULLPATH;

	// path
	if( flashPath.length() == 0 )
		flashPath = OFS_FLASH_EXE_PATH;
	if( flashPath[flashPath.length()-1] != '\\' )
		flashPath += "\\";
	ciDEBUG(5, ("Flash Path (%s)", flashPath.c_str()) );

	// already exist exe (maybe get from arguments)
	return ciTrue;
	//if( flashFilename.length() != 0 )
	//{
	//	ciDEBUG(5, ("Flash Filename (%s) from arguments", flashFilename.c_str()) );
	//	return ciTrue;
	//}

	//// empty => find exe
	//ciString str_find = flashPath;
	//if( str_find[str_find.length()-1] != '\\' )
	//	str_find += "\\";
	//str_find += "OFS_*.exe";

	//WIN32_FIND_DATA fd;
	//HANDLE handle = FindFirstFile(str_find.c_str(), &fd);
	//if( handle != INVALID_HANDLE_VALUE )
	//{
	//	FILETIME last_tm = {0};
	//	do
	//	{
	//		if( CompareFileTime(&fd.ftLastWriteTime, &last_tm) > 0 )
	//		{
	//			flashFilename = fd.cFileName;
	//			last_tm = fd.ftLastWriteTime;
	//		}
	//	}
	//	while( ::FindNextFile(handle, &fd) != 0 );
	//	FindClose(handle);

	//	ciDEBUG(5, ("Find Flash Filename (%s)", flashFilename.c_str()) );
	//	return ciTrue;
	//}

	//return ciFalse;
}

ciBoolean brochureCheckTimer::_getBrochureXML(const char* bId, ciString& xml)
{
	//
	//char url[1024] = "api/brochureInfoView/Api008.do";
	char sendmsg[1024];
	sprintf(sendmsg, "brochureId=%s&returnType=XML", bId );

	ciDEBUG(5, ("_cmsServerIP(%s), _cmsServerPort(%d), sendMsg(%s)", _cmsServerIP.c_str(), _cmsServerPort, sendmsg) );

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _cmsServerIP.c_str(), _cmsServerPort);
	if( ciArgParser::getInstance()->isSet("+HMCSFS") )
		http_request.AddAdditionalHeader(_getAdditionHttpHeader().c_str());

	//
	CString str_buf = "";
	if( !http_request.RequestPost(_cmsXmlUrl.c_str(), sendmsg, str_buf) )
	{
		ciWARN( ("RequestPost FAIL !!! (%s?%s)", _cmsXmlUrl.c_str(), sendmsg) );
		return ciFalse; // error, false
	}

	char header[] = {0xEF, 0xBB, 0xBF, 0x3C, 0x3F, 0x78, 0x6D, 0x6C}; // BOM<?xml
	if( memcmp(header, (LPCSTR)str_buf, 8) != 0 &&
		memcmp(header+3, (LPCSTR)str_buf, 5) != 0  )
	{
		ciWARN( ("INVALID RESPONSE !!! (%s)", str_buf) );
		return ciFalse; // error, false
	}

	ciDEBUG(5, ("RequestPost(%s)", getAnsiXML((LPCSTR)str_buf).c_str()) );
	xml = (LPCSTR)str_buf;

	return ciTrue;
}

ciBoolean brochureCheckTimer::_contentsCheck(const char* xml)
{
	if( _contentsCheckTimer == NULL || xml == NULL ) return ciTrue;

	//
	char* tag_start = (char*)strstr(xml, CONTENTS_IN_BROCHURE_STARTTAG);
	while( tag_start != NULL )
	{
		ciDEBUG(5, ("find brochure-contents-tag at [%d]", tag_start-xml) );

		// find end-tag
		char* tag_end = strstr(tag_start, CONTENTS_IN_BROCHURE_ENDTAG);
		if( tag_end == NULL ) break;
		tag_end += strlen(CONTENTS_IN_BROCHURE_ENDTAG);

		// get item-string
		int size = tag_end - tag_start;
		if( size < 0 )
		{
			ciERROR( ("NEGATIVE SIZE !!!") );
			break;
		}
		char* buf_xml = new char[size+1];
		AUTO_DELETE(buf_xml);
		::ZeroMemory(buf_xml, size+1);
		strncpy(buf_xml, tag_start, size);

		// get location in buf_xml
		ciString str_filename = _getItem(buf_xml, FILENAME_STARTTAG, FILENAME_ENDTAG);
		ciString str_filename_ori = _getItem(buf_xml, FILENAME_ORI_STARTTAG, FILENAME_ORI_ENDTAG);
		ciString str_location = _getItem(buf_xml, LOCATION_STARTTAG, LOCATION_ENDTAG);
		ciString str_volume = _getItem(buf_xml, VOLUME_STARTTAG, VOLUME_ENDTAG);
		if( _contentsCheckTimer->isDownloadingContents(str_location.c_str()) )
		{
			ciDEBUG(5, ("this contents is in downloading [%s]", str_location.c_str()) );
			return ciFalse;
		}

		// make dummy-xml
		ciString dummy_xml = "";
		dummy_xml += "<downFile><filename>";
		dummy_xml += str_filename;
		dummy_xml += "</filename><filenameOri>";
		dummy_xml += str_filename_ori;
		dummy_xml += "</filenameOri><location>";
		dummy_xml += str_location;
		dummy_xml += "</location><volume>";
		dummy_xml += str_volume;
		dummy_xml += "</volume><lastUpdateTime>1</lastUpdateTime></downFile>";

		// get contents-info from dummy-xml
		contentsInfo* info = contentsInfo::getContentsInfoObject(dummy_xml.c_str());
		AUTO_DELETE(info);
		if( info != NULL )
		{
			ciDEBUG(5, ("dummy contents-info( %s )", info->toString(false).c_str()) );

			// get file-status
			struct _stati64 fs;
			if( _stati64(info->localPath.c_str(), &fs) != 0 )
			{
				ciWARN( ("not exist (%s)", info->localPath.c_str()) );
				return ciFalse;
			}

			// open success
			if( info->volume != fs.st_size )
			{
				// different size => must download
				ciWARN( ("different size (%s)", info->localPath.c_str()) );
				return ciFalse;
			}
		}
		else
		{
			ciWARN( ("FAIL TO CREATE DUMMY-INFO !!! ( %s )", dummy_xml.c_str()) );
		}

		//
		tag_start = strstr(tag_end, CONTENTS_IN_BROCHURE_STARTTAG);
	}

	return ciTrue;
}

ciBoolean brochureCheckTimer::_writeXML(const char* xml)
{
	ciDEBUG(5, ("_writeXML(%s)", _brochureXmlPath.c_str()) );

	//
	createFullDirectory(_brochureXmlPath.c_str());

	//
	FILE* fp = fopen(_brochureXmlPath.c_str(), "wb");
	if( fp == NULL )
	{
		//
		ciERROR( ("fopen(%s) ERROR !!!", _brochureXmlPath.c_str()) );
		return ciFalse;
	}

	int xml_size = strlen(xml);
	int write_size = fwrite(xml, 1, xml_size, fp);
	fclose(fp);

	if( write_size != xml_size )
	{
		//
		ciERROR( ("fwrite(%s) ERROR !!!", _brochureXmlPath.c_str()) );
		return ciFalse;
	}

	return ciTrue;
}

ciBoolean brochureCheckTimer::_killFlashProcess()
{
	ciDEBUG(5, ("_killFlashProcess(%s)", flashFilename.c_str()) );
	if( flashFilename.length() == 0 ) return ciFalse;

	for(int count=0; count<100; count++ ) // do until 10-sec
	{
		int flash_pid = scratchUtil::getInstance()->getPid(flashFilename.c_str());
		if( flash_pid == 0 )
		{
			ciDEBUG(5, ("success to killProcess(%s)", flashFilename.c_str()) );
			return ciTrue;
		}
		scratchUtil::getInstance()->killProcess(flash_pid);
		::Sleep(100); // wait 0.1sec
	}

	ciWARN( ("FAIL killProcess !!! (%s)", flashFilename.c_str()) );
	return ciFalse;
}

ciULong createProcess(const char *exename, const char* arg,const char* dir, BOOL minFlag)
{
//	myDEBUG("_createProcess : exename(%s %s)\n", exename, arg);

	STARTUPINFO            si;
	PROCESS_INFORMATION     pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(STARTUPINFO);
	//if (minFlag) {
		si.dwFlags = STARTF_USESHOWWINDOW;
	//	si.wShowWindow = SW_MINIMIZE;
	//}
	//si.lpDesktop = "Winsta0\\Default";

	//if(!dir || !strlen(dir)){
	//	dir = UBC_HOME;
	//}

	CString c_exename = exename;
	CString c_dir = dir;

	BOOL bRun = CreateProcess(
		NULL, 
		(LPSTR)arg, 
		NULL, 
		NULL, 
		FALSE, 
		NORMAL_PRIORITY_CLASS, 
		NULL, 
		(LPSTR)(LPCSTR)c_dir, 
		&si, 
		&pi
		);

	CloseHandle( pi.hThread );
	CloseHandle( pi.hProcess );

	if (bRun)
	{
		//myDEBUG("createProcess : pid(%d)\n", pi.dwProcessId);
		return pi.dwProcessId;
	}
	//myDEBUG("createProcess : fail to CreateProcess\n");
	return 0;
}

ciULong brochureCheckTimer::_startFlashProcess()
{
	ciDEBUG(5, ("_startFlashProcess(%s)", flashFilename.c_str()) );
	if( flashFilename.length() == 0 ) return 0;

	// execute C:/SQISoft/Contents/ENC/OFS_xxx/OFS_xxx.exe
	ciString fullpath = flashPath;
	fullpath += flashSubPath;
	fullpath += "\\";
	fullpath += flashFilename;

	ciDEBUG(5, ("createProcess(%s)", fullpath.c_str()) );
	ciULong pid = createProcess("", fullpath.c_str(), flashPath.c_str(), FALSE);
	if( pid > 0 )
	{
		ciDEBUG(5, ("success to createProcess(%d)", pid) );
		return pid;
	}

	// execute C:/SQISoft/Contents/ENC/OFS_xxx.exe
	fullpath = flashPath;
	fullpath += flashFilename;

	ciDEBUG(5, ("createProcess(%s)", fullpath.c_str()) );
	pid = createProcess("", fullpath.c_str(), flashPath.c_str(), FALSE);
	if( pid > 0 )
	{
		ciDEBUG(5, ("success to createProcess(%d)", pid) );
		return pid;
	}

	ciWARN( ("FAIL createProcess !!! (%s)(errcode:%d)", fullpath.c_str(), GetLastError()) );
	return 0;
}

ciULong brochureCheckTimer::_getFlashPID()
{
	return scratchUtil::getInstance()->getPid(flashFilename.c_str());
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
ciString brochureCheckTimer::_getXMLforKHQS(const char* xml)
{
	if( xml == NULL ) return "";

	//
	CStringArray file_list;
	char* tag_start = (char*)strstr(xml, CONTENTS_IN_BROCHURE_STARTTAG);
	while( tag_start != NULL )
	{
		ciDEBUG(5, ("find brochure-contents-tag at [%d]", tag_start-xml) );

		// find end-tag
		char* tag_end = strstr(tag_start, CONTENTS_IN_BROCHURE_ENDTAG);
		if( tag_end == NULL ) break;
		tag_end += strlen(CONTENTS_IN_BROCHURE_ENDTAG);

		// get item-string
		int size = tag_end - tag_start;
		if( size < 0 )
		{
			ciERROR( ("NEGATIVE SIZE !!!") );
			break;
		}
		char* buf_xml = new char[size+1];
		AUTO_DELETE(buf_xml);
		::ZeroMemory(buf_xml, size+1);
		strncpy(buf_xml, tag_start, size);

		// get location in buf_xml
		ciString str_location = _getItem(buf_xml, LOCATION_STARTTAG, LOCATION_ENDTAG);
		ciString str_volume = _getItem(buf_xml, VOLUME_STARTTAG, VOLUME_ENDTAG);

		ciStringUtil::stringReplace(str_location, "/", "\\");
		ciStringUtil::stringReplace(str_location, "\\\\", "\\");

		ciStringTokenizer aTokens(str_location.c_str(), "\\");
		if( aTokens.countTokens() >= 3 )
		{
			str_location += "|";
			str_location += str_volume;
			file_list.Add(str_location.c_str());
		}
		else
		{
			ciWARN( ("INVALID LOCATION !!! (%d, %s)", aTokens.countTokens(), str_location.c_str()) );
		}

		//
		tag_start = strstr(tag_end, CONTENTS_IN_BROCHURE_STARTTAG);
	}
//	if( file_list.GetCount() == 0 ) return "";

	//// sort
	int cnt = file_list.GetCount();
	//for(int i=0; i<cnt-1; i++)
	//{
	//	for(int j=i+1; j<cnt; j++)
	//	{
	//		ciString str1 = (LPCSTR)file_list.GetAt(i);
	//		ciString str2 = (LPCSTR)file_list.GetAt(j);

	//		ciString lower1 = str1;
	//		ciString lower2 = str2;
	//		UTF8toA(str1.c_str(), lower1);
	//		UTF8toA(str2.c_str(), lower2);
	//		ciStringUtil::toLower(lower1);
	//		ciStringUtil::toLower(lower2);

	//		if( lower1 > lower2 )
	//		{
	//			file_list.SetAt(i, str2.c_str());
	//			file_list.SetAt(j, str1.c_str());
	//		}
	//	}
	//}

	//
	ciString str_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n\t<contents>\r\n";
	ciString str_folder = "";
	ciString str_subfolder = "";
	for(int i=0; i<cnt; i++)
	{
		ciStringTokenizer bTokens((LPCSTR)file_list.GetAt(i), "|");

		ciString str_location = bTokens.nextToken();
		ciString str_volume = bTokens.nextToken();

		ciStringTokenizer aTokens(str_location.c_str(), "\\");
		ciString tmp_folder = aTokens.nextToken();
		ciString tmp_subfolder = aTokens.nextToken();
		ciString tmp_filename = (aTokens.restTokens().c_str() + 1);

		if( stricmp(str_folder.c_str(), tmp_folder.c_str()) != 0 )
		{
			if( !str_subfolder.empty() )
				str_xml += "\t\t</subfolder>\r\n";

			if( !str_folder.empty() )
				str_xml += "\t</folder>\r\n";

			str_folder = tmp_folder;
			str_subfolder = "";

			str_xml += "\t<folder name=\"";
			str_xml += str_folder;
			str_xml += "\">\r\n";
		}

		if( stricmp(str_subfolder.c_str(), tmp_subfolder.c_str()) != 0 )
		{
			if( !str_subfolder.empty() )
				str_xml += "\t\t</subfolder>\r\n";

			str_subfolder = tmp_subfolder;

			str_xml += "\t\t<subfolder name=\"";
			str_xml += str_subfolder;
			str_xml += "\">\r\n";
		}

		str_xml += "\t\t\t<file>\r\n\t\t\t\t<filename>";
		str_xml += tmp_filename;
		str_xml += "</filename>\r\n\t\t\t\t<location>";
		str_xml += str_location;
		str_xml += "</location>\r\n\t\t\t\t<volume>";
		str_xml += str_volume;
		str_xml += "</volume>\r\n\t\t\t</file>\r\n";
	}

	if( !str_subfolder.empty() )
		str_xml += "\t\t</subfolder>\r\n";

	if( !str_folder.empty() )
		str_xml += "\t</folder>\r\n";

	str_xml += "</contents>";

	return str_xml;
}


#include "libPPT2Image/libPPT2Image.h"

ciString brochureCheckTimer::_getXMLforHMCSFS(const char* xml)
{
	if( xml == NULL ) return "";

	//CPPT2Image ppt2image;
	//ppt2image.convert();

	//
	CStringArray file_list;
	char* tag_start = (char*)strstr(xml, CONTENTS_IN_BROCHURE_STARTTAG);
	while( tag_start != NULL )
	{
		ciDEBUG(5, ("find brochure-contents-tag at [%d]", tag_start-xml) );

		// find end-tag
		char* tag_end = strstr(tag_start, CONTENTS_IN_BROCHURE_ENDTAG);
		if( tag_end == NULL ) break;
		tag_end += strlen(CONTENTS_IN_BROCHURE_ENDTAG);

		// get item-string
		int size = tag_end - tag_start;
		if( size < 0 )
		{
			ciERROR( ("NEGATIVE SIZE !!!") );
			break;
		}
		char* buf_xml = new char[size+1];
		AUTO_DELETE(buf_xml);
		::ZeroMemory(buf_xml, size+1);
		strncpy(buf_xml, tag_start, size);

		// get location in buf_xml
		ciString str_location = _getItem(buf_xml, LOCATION_STARTTAG, LOCATION_ENDTAG);
		ciString str_volume = _getItem(buf_xml, VOLUME_STARTTAG, VOLUME_ENDTAG);

		ciStringUtil::stringReplace(str_location, "/", "\\");
		ciStringUtil::stringReplace(str_location, "\\\\", "\\");

		ciStringTokenizer aTokens(str_location.c_str(), "\\");
//		if( aTokens.countTokens() >= 3 )
		{
			str_location += "|";
			str_location += str_volume;
			file_list.Add(str_location.c_str());
		}
//		else
//		{
//			ciWARN( ("INVALID LOCATION !!! (%d, %s)", aTokens.countTokens(), str_location.c_str()) );
//		}

		//
		tag_start = strstr(tag_end, CONTENTS_IN_BROCHURE_STARTTAG);
	}
//	if( file_list.GetCount() == 0 ) return "";

	//// sort
	int cnt = file_list.GetCount();
	//for(int i=0; i<cnt-1; i++)
	//{
	//	for(int j=i+1; j<cnt; j++)
	//	{
	//		ciString str1 = (LPCSTR)file_list.GetAt(i);
	//		ciString str2 = (LPCSTR)file_list.GetAt(j);

	//		ciString lower1 = str1;
	//		ciString lower2 = str2;
	//		UTF8toA(str1.c_str(), lower1);
	//		UTF8toA(str2.c_str(), lower2);
	//		ciStringUtil::toLower(lower1);
	//		ciStringUtil::toLower(lower2);

	//		if( lower1 > lower2 )
	//		{
	//			file_list.SetAt(i, str2.c_str());
	//			file_list.SetAt(j, str1.c_str());
	//		}
	//	}
	//}

	//
	ciString str_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n\t<contents>\r\n";
	ciString str_folder = "";
	ciString str_subfolder = "";
	ciString str_subsubfolder = "";
	ciString str_subsubsubfolder = "";
	for(int i=0; i<cnt; i++)
	{
		ciStringTokenizer bTokens((LPCSTR)file_list.GetAt(i), "|");

		ciString str_location = bTokens.nextToken();
		ciString str_volume = bTokens.nextToken();

		ciStringTokenizer aTokens(str_location.c_str(), "\\");
		ciString tmp_folder = "";//aTokens.nextToken();
		ciString tmp_subfolder = "";//aTokens.nextToken();
		ciString tmp_subsubfolder = "";//aTokens.nextToken();
		ciString tmp_subsubsubfolder = "";//aTokens.nextToken();
		ciString tmp_filename = "";//(aTokens.restTokens().c_str() + 1);
		switch( aTokens.countTokens() )
		{
		case 1:
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		case 2:
			tmp_folder = aTokens.nextToken();
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		case 3:
			tmp_folder = aTokens.nextToken();
			tmp_subfolder = aTokens.nextToken();
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		case 4:
			tmp_folder = aTokens.nextToken();
			tmp_subfolder = aTokens.nextToken();
			tmp_subsubfolder = aTokens.nextToken();
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		case 5:
		default:
			tmp_folder = aTokens.nextToken();
			tmp_subfolder = aTokens.nextToken();
			tmp_subsubfolder = aTokens.nextToken();
			tmp_subsubsubfolder = aTokens.nextToken();
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		}

		if( stricmp(str_folder.c_str(), tmp_folder.c_str()) != 0 )
		{
			if( !str_subsubsubfolder.empty() ) str_xml += "\t\t\t\t</subsubsubfolder>\r\n";
			if( !str_subsubfolder.empty() ) str_xml += "\t\t\t</subsubfolder>\r\n";
			if( !str_subfolder.empty() ) str_xml += "\t\t</subfolder>\r\n";
			if( !str_folder.empty() ) str_xml += "\t</folder>\r\n";

			str_folder = tmp_folder;
			str_subfolder = "";
			str_subsubfolder = "";
			str_subsubsubfolder = "";

			str_xml += "\t<folder name=\"";
			str_xml += str_folder;
			str_xml += "\">\r\n";
		}

		if( stricmp(str_subfolder.c_str(), tmp_subfolder.c_str()) != 0 )
		{
			if( !str_subsubsubfolder.empty() ) str_xml += "\t\t\t\t</subsubsubfolder>\r\n";
			if( !str_subsubfolder.empty() ) str_xml += "\t\t\t</subsubfolder>\r\n";
			if( !str_subfolder.empty() ) str_xml += "\t\t</subfolder>\r\n";

			str_subfolder = tmp_subfolder;
			str_subsubfolder = "";
			str_subsubsubfolder = "";

			str_xml += "\t\t<subfolder name=\"";
			str_xml += str_subfolder;
			str_xml += "\">\r\n";
		}

		if( stricmp(str_subsubfolder.c_str(), tmp_subsubfolder.c_str()) != 0 )
		{
			if( !str_subsubsubfolder.empty() ) str_xml += "\t\t\t\t</subsubsubfolder>\r\n";
			if( !str_subsubfolder.empty() ) str_xml += "\t\t\t</subsubfolder>\r\n";

			str_subsubfolder = tmp_subsubfolder;
			str_subsubsubfolder = "";

			str_xml += "\t\t\t<subsubfolder name=\"";
			str_xml += str_subsubfolder;
			str_xml += "\">\r\n";
		}

		if( stricmp(str_subsubsubfolder.c_str(), tmp_subsubsubfolder.c_str()) != 0 )
		{
			if( !str_subsubsubfolder.empty() ) str_xml += "\t\t\t\t</subsubsubfolder>\r\n";

			str_subsubsubfolder = tmp_subsubsubfolder;

			str_xml += "\t\t\t\t<subsubsubfolder name=\"";
			str_xml += str_subsubsubfolder;
			str_xml += "\">\r\n";
		}

		str_xml += "\t\t\t\t\t<file>\r\n\t\t\t\t\t\t<filename>";
		str_xml += tmp_filename;
		str_xml += "</filename>\r\n\t\t\t\t\t\t<location>";
		str_xml += str_location;
		str_xml += "</location>\r\n\t\t\t\t\t\t<volume>";
		str_xml += str_volume;
		str_xml += "</volume>\r\n\t\t\t\t\t</file>\r\n";
	}

	if( !str_subsubsubfolder.empty() )
		str_xml += "\t\t\t\t</subsubsubfolder>\r\n";

	if( !str_subsubfolder.empty() )
		str_xml += "\t\t\t</subsubfolder>\r\n";

	if( !str_subfolder.empty() )
		str_xml += "\t\t</subfolder>\r\n";

	if( !str_folder.empty() )
		str_xml += "\t</folder>\r\n";

	str_xml += "</contents>";

	return str_xml;
}
