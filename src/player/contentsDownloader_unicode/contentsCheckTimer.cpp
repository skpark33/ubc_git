#include "common/libHttpRequest/HttpRequest.h"
#include <ci/libBase/ciTime.h>
//#include <ci/libBase/ciStringTokenizer.h>
#include "ciWStringTokenizer.h"
#include <ci/libConfig/ciEnv.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <util/libEncoding/Encoding.h>
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchutil.h"
#include "contentsCheckTimer.h"
#include "contentsManagerTimer.h"
#include "contentsDownloadTimer.h"
#include "contentsDownloaderWorld.h"


#define		OFS_FLASH_EXE_PATH				L"C:\\SQISoft\\Contents\\ENC\\"
#define		OFS_FLASH_XML_FULLPATH			L"C:\\SQISoft\\Contents\\ENC\\OFS\\brochure\\xml\\default_play.xml"
#define		KHQS_FLASH_XML_FULLPATH			L"C:\\SQISoft\\Contents\\ENC\\KHQS\\xml\\default_play.xml"
#define		HMCSFS_FLASH_XML_FULLPATH		L"C:\\SQISoft\\Contents\\ENC\\PDIS\\xml\\default_play.xml"
#define		ROYAL_FLASH_XML_FULLPATH		L"C:\\SQISoft\\Contents\\ENC\\ROYAL\\xml\\default_play.xml"
#define		SPC_SALE_FLASH_XML_FULLPATH		L"C:\\SQISoft\\Contents\\ENC\\SPC_SALE\\xml\\default_play.xml"
#define		SSG_SALE_FLASH_XML_FULLPATH		L"C:\\SQISoft\\Contents\\ENC\\SSG_SHOP\\xml\\default_play.xml"

#define		CONTENTS_IN_BROCHURE_STARTTAG	L"<contents"
#define		CONTENTS_IN_BROCHURE_ENDTAG		L"</contents>"


ciSET_DEBUG(9, "contentsCheckTimer");


ciString	contentsCheckTimer::flashPath = "";
ciString	contentsCheckTimer::flashSubPath = "";
ciString	contentsCheckTimer::flashFilename = "";
ciString	contentsCheckTimer::flashArgument = "";


contentsCheckTimer::contentsCheckTimer(	const char* site, 
										const char* host, 
										const char* ip, 
										ciLong port, 
										const char* id, 
										const char* pwd )
{
	ciDEBUG2(5, ("contentsCheckTimer(%s,%s,%s,%d,%s,%s)", site, host, ip, port, id, pwd) );

	_brochConMngTimer = NULL;
	_goodsConMngTimer = NULL;
	_unionConMngTimer = NULL;
	_deleteConMngTimer = NULL;

	_initialize	= ciFalse;

	_site		= site;
	_host		= host;
	_svrIp		= ip;
	_svrPort	= port;
	_svrId		= id;
	_svrPwd		= pwd;

	ubcConfig aIni("UBCVariables");
	ciString buf;

	// get last-sync-time
	buf = "";
	aIni.get("OFS_CMS_INFO", "LastSyncTime", buf, "1");
	ciStringUtil::trim(buf);
	ciDEBUG2(5, ("LastSyncTime=%s", buf.c_str()) );
	time_t last_sync_time = _atoi64(buf.c_str());
	_lastSyncTime.set(last_sync_time);
	_lastestContentsTime.set(last_sync_time);

	//// get cms-server-ip
	//aIni.get("OFS_CMS_INFO", "ServerIP", _cmsServerIp, "");
	//ciStringUtil::trim(_cmsServerIp);
	//ciDEBUG2(5, ("ServerIP=%s", _cmsServerIp.c_str()) );
	//if( _cmsServerIp.length() == 0 )
	//{
	//	_cmsServerIp = DEFAULT_CMS_SERVER_IP;
	//	aIni.set("OFS_CMS_INFO", "ServerIP", _cmsServerIp.c_str());
	//	ciDEBUG2(5, ("set to default_cms_server_ip(%s)", _cmsServerIp.c_str()) );
	//}

	//// get cms-server-port
	//buf = "";
	//aIni.get("OFS_CMS_INFO", "ServerPort", buf, "");
	//ciStringUtil::trim(buf);
	//ciDEBUG2(5, ("ServerPort=%s", buf.c_str()) );
	//_cmsServerPort = atoi(buf.c_str());
	//if( _cmsServerPort == 0 )
	//{
	//	_cmsServerPort = DEFAULT_CMS_SERVER_PORT;
	//	aIni.set("OFS_CMS_INFO", "ServerPort", _cmsServerPort);
	//	ciDEBUG2(5, ("set to default_cms_server_port(%d)", _cmsServerPort) );
	//}

	//// get cms-contents-download-list-url
	//aIni.get("OFS_CMS_INFO", "ContentsUrl", _contentsDownloadListURL, "");
	//ciStringUtil::trim(_contentsDownloadListURL);
	//ciDEBUG2(5, ("ContentsUrl=%s", _contentsDownloadListURL.c_str()) );
	//if( _contentsDownloadListURL.length() == 0 )
	//{
	//	_contentsDownloadListURL = DEFAULT_CONTENTS_DOWNLOAD_LIST_URL;
	//	aIni.set("OFS_CMS_INFO", "ContentsUrl", _contentsDownloadListURL.c_str());
	//	ciDEBUG2(5, ("set to default_contents_download_list_url(%s)", _contentsDownloadListURL.c_str()) );
	//}

	//// get cms-brochure-id-url
	//aIni.get("OFS_CMS_INFO", "BrochureIdUrl", _brochureIdURL, "");
	//ciStringUtil::trim(_brochureIdURL);
	//ciDEBUG2(5, ("BrochureIdUrl=%s", _brochureIdURL.c_str()) );
	//if( _brochureIdURL.length() == 0 )
	//{
	//	_brochureIdURL = DEFAULT_BROCHURE_ID_URL;
	//	aIni.set("OFS_CMS_INFO", "BrochureIdUrl", _brochureIdURL.c_str());
	//	ciDEBUG2(5, ("set to default_brochure_id_url(%s)", _brochureIdURL.c_str()) );
	//}

	//// get cms-brochure-xml-url
	//buf = "";
	//aIni.get("OFS_CMS_INFO", "BrochureXmlUrl", _brochureXMLURL, "");
	//ciStringUtil::trim(_brochureXMLURL);
	//ciDEBUG2(5, ("BrochureXmlUrl=%s", _brochureXMLURL.c_str()) );
	//if( _brochureXMLURL.length() == 0 )
	//{
	//	_brochureXMLURL = DEFAULT_BROCHURE_XML_URL;
	//	aIni.set("OFS_CMS_INFO", "BrochureXmlUrl", _brochureXMLURL.c_str());
	//	ciDEBUG2(5, ("set to default_brochure_xml_url(%s)", _brochureXMLURL.c_str()) );
	//}

	// get last-brochure-id & time
	aIni.get("OFS_CMS_INFO", "LastBrochureId", _prevBrochureId, "");
	ciStringUtil::trim(_prevBrochureId);
	ciDEBUG2(5, ("LastBrochureId=%s", _prevBrochureId.c_str()) );
	_runningBrochureId = _prevBrochureId;

	aIni.get("OFS_CMS_INFO", "LastBrochureTime", buf, "1");
	ciDEBUG2(5, ("LastBrochureTime=%s", buf.c_str()) );
	time_t last_bro_time = _atoi64(buf.c_str());
	_prevBrochureTime.set(last_bro_time);
	_runningBrochureTime = _prevBrochureTime;

	// brochure xml path
	if     ( ciArgParser::getInstance()->isSet("+KHQS") )
		_brochureXmlPath = KHQS_FLASH_XML_FULLPATH;
	else if( ciArgParser::getInstance()->isSet("+HMCSFS") )
		_brochureXmlPath = HMCSFS_FLASH_XML_FULLPATH;
	else if ( ciArgParser::getInstance()->isSet("+ROYAL") )
		_brochureXmlPath = ROYAL_FLASH_XML_FULLPATH;
	else if ( ciArgParser::getInstance()->isSet("+SPC_SALE") )
		_brochureXmlPath = SPC_SALE_FLASH_XML_FULLPATH;
	else if ( ciArgParser::getInstance()->isSet("+SSG_SHOP") )
		_brochureXmlPath = SSG_SALE_FLASH_XML_FULLPATH;
	else
		_brochureXmlPath = OFS_FLASH_XML_FULLPATH;
	ciDEBUG2(5, ("brochureXmlPath=%s", GetUTF8fromU(_brochureXmlPath.c_str())) );

	// path
	if( flashPath.length() == 0 )
		flashPath = GetAfromU(OFS_FLASH_EXE_PATH);
	if( flashPath[flashPath.length()-1] != '\\' )
		flashPath += "\\";
	ciDEBUG2(5, ("flashPath=%s", flashPath.c_str()) );
	ciDEBUG2(5, ("flashSubPath=%s", flashSubPath.c_str()) );
	ciDEBUG2(5, ("flashFilename=%s", flashFilename.c_str()) );

	// init timer(s)
	_initTimer();

	ciDEBUG2(5, ("contentsCheckTimer()"));
}

contentsCheckTimer::~contentsCheckTimer()
{
	ciDEBUG2(5, ("~contentsCheckTimer()"));
}

ciString contentsCheckTimer::_getAnsiXML(const char* xml)
{
	ciString str_decoding = xml;
	ciStringUtil::toLower(str_decoding);
	char* tmp = (char*)strstr(str_decoding.c_str(), "encoding=\"");
	if( tmp != NULL && _strnicmp(tmp+10,"utf-8",5) == 0 )
	{
		ciDEBUG2(5, ("utf-8 xml"));
		//UTF8toA(xml, str_decoding);
		CEncoding::getInstance()->UTF8ToAnsi(xml, str_decoding, 949);
	}
	else
	{
		ciDEBUG2(5, ("ansi xml"));
		str_decoding = xml;
	}

	return str_decoding;
}

ciWString contentsCheckTimer::_getUnicodeXML(const char* xml)
{
	ciWString str_uni = L"";
	ciString str_lower = xml;
	ciStringUtil::toLower(str_lower);
	char* tmp = (char*)strstr(str_lower.c_str(), "encoding=\"");
	if( tmp != NULL && _strnicmp(tmp+10,"utf-8",5) == 0 )
	{
		ciDEBUG2(5, ("utf-8 xml"));
		//CEncoding::getInstance()->UTF8ToUnicode(xml, str_uni);
		UTF8toU(xml, str_uni);
	}
	else
	{
		ciDEBUG2(5, ("ansi xml"));
		//AtoU(xml, str_uni);
		CEncoding::getInstance()->AnsiToUnicode(xml, str_uni, 949);
	}

	return str_uni;
}

void contentsCheckTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG2(5, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	// check init
	if( _initialize==ciFalse && counter>=6 ) // not init & over 1-min
	{
		// ==> init forcibly & set 1-min-interval
		ciWARN2(("not initialize over 1-min"));
		_initialize = ciTrue;
		setInterval(60);
	}

	if( counter>0 && _getFlashPID()==0 )
	{
		ciDEBUG2(5, ("no flash process ==> start flash process") );
		_startFlashProcess();
	}


	////////////////////////////////////////////////////////////////////////
	// download contents-files
	////////////////////////////////////////////////////////////////////////

	// get contents_download_list
	ciWString str_uni_xml;
	if( _getContentsDownloadListXML(str_uni_xml) == ciFalse )
	{
		// httpRequestPost-fail
		ciWARN( ("FAIL TO _getContentsDownloadListXML() !!!") );
		return;
	}

	// get contents-list
	contentsInfoList contents_list;
	if( !contentsInfo::getContentsInfoList(contents_list, str_uni_xml) )
	{
		// NO-CONTENTS-INFO
		ciWARN( ("NO CONTENTS from XML !!!") );
		//return;
	}

	// stop delete-contents-manager-timer
	if( ciArgParser::getInstance()->isSet("+HMCSFS") && _deleteConMngTimer!=NULL )
	{
		// HMCSFS ==> pause timer
		_deleteConMngTimer->stopTimer();
	}

	// check contents-list and push to download-list
	int goods_missing_count=0, before_brochure_missing_count=0;
	_checkContentsFile(&contents_list, goods_missing_count, before_brochure_missing_count);
	ciDEBUG2(5, ("goods_missing_count(before download)=%d", goods_missing_count) );
	ciDEBUG2(5, ("brochure_missing_count(before download)=%d", before_brochure_missing_count) );

	if( goods_missing_count==0 && before_brochure_missing_count==0 )
	{
		//ciDEBUG2(5, ("no download contents") );
		// no downloading contents ==> update last-sync-time
		if( _getAllRemainCount() == 0 )
		{
			ciDEBUG2(5, ("no missing contents ==> update last_sync_time") );
			_updateLastSyncTime();
		}
	}
	else
	{
		// exist downloading contents
		int check = 0;
		int cnt = 0;
		// wait until all-download (regardless of complete/fail)
		while( (cnt=_getDownloadRemainCount()) > 0 )
		{
			::Sleep(1000);
			if( ++check % 60 != 0 ) continue;

			ciDEBUG2(5, ("holding until download-complete (remain:%d)", cnt) );
			if( _getFlashPID() == 0 )
			{
				ciDEBUG2(5, ("no flash process ==> start flash process") );
				_startFlashProcess();
			}
		}
	}

	// clear list
	_clearContentsInfoList(&contents_list);


	////////////////////////////////////////////////////////////////////////
	// brochure-contents-file check
	////////////////////////////////////////////////////////////////////////

	// get brochure-id & time
	ciString new_brochure_id = "";
	ciTime new_brochure_time;
	if( _getBrochureId(new_brochure_id, new_brochure_time) == ciFalse )
	{
		// httpRequestPost-fail or not-true
		ciWARN( ("FAIL TO _getBrochureId() !!!") );
		// restart delete-contents-manager-timer
		if( ciArgParser::getInstance()->isSet("+HMCSFS") && _deleteConMngTimer!=NULL )
		{
			// HMCSFS ==> resume timer
			_deleteConMngTimer->startTimer();
		}
		return;
	}
	ciDEBUG2(5, ("new_brochure_id=%s", new_brochure_id.c_str()) );
	ciDEBUG2(5, ("new_brochure_time=%I64d(%s)", new_brochure_time.getTime(), new_brochure_time.getTimeString().c_str()) );

	// get brochure-xml
	//ciString str_xml;
	str_uni_xml = L"";
	if( _getBrochureXml(new_brochure_id.c_str(), str_uni_xml) == ciFalse )
	{
		// httpRequestPost-fail
		ciWARN( ("FAIL TO _getBrochureXml() !!!") );
		// restart delete-contents-manager-timer
		if( ciArgParser::getInstance()->isSet("+HMCSFS") && _deleteConMngTimer!=NULL )
		{
			// HMCSFS ==> resume timer
			_deleteConMngTimer->startTimer();
		}
		return;
	}

	// get brochure-contents-list from xml
	// check brochure-contents
	// get count of missing-brochure-contents-files
	//str_ansi_xml = _getAnsiXML(str_xml.c_str());
	int after_brochure_missing_count = _getBrochureContentsInfoList(str_uni_xml.c_str()/*_getAnsiXML(str_xml.c_str()).c_str()*/, &contents_list);
	ciDEBUG2(5, ("brochure_missing_count(after download)=%d", after_brochure_missing_count) );

	// modify brochure-xml ( missing ==> 0-volume )
	// write brochure-xml
	if( ciArgParser::getInstance()->isSet("+KHQS") || ciArgParser::getInstance()->isSet("+ROYAL") )
	{
		// only use KHQS
		str_uni_xml = _getXMLforKHQS(&contents_list);
		ciDEBUG2(5, ("xml is converted for KHQS\r\n%s", GetUTF8fromU(str_uni_xml.c_str())) );
	}
	else if( ciArgParser::getInstance()->isSet("+HMCSFS") )
	{
		// only use HMCSFS
		str_uni_xml = _getXMLforHMCSFS(&contents_list);
		ciDEBUG2(5, ("xml is converted for HMCSFS\r\n%s", GetUTF8fromU(str_uni_xml.c_str())) );
	}
	else
	{
		// "<location>contents/uploadData"  ==>  "<location>"
		/*ciStringUtil*/::stringReplace(str_uni_xml, L"<location>/contents/uploadData", L"<location>");
		/*ciStringUtil*/::stringReplace(str_uni_xml, L"<location>contents/uploadData", L"<location>");
	}

	//
	if( !_writeXML(str_uni_xml.c_str()) )
	{
		// get xml ERROR !!!
		ciWARN2( ("FAIL TO _writeXML !!! (%s)", GetUTF8fromU(_brochureXmlPath.c_str())) );
	}

	//
	ciDEBUG2(5, ("_prevBrochureId=%s", _prevBrochureId.c_str()) );
	ciDEBUG2(5, ("_prevBrochureTime=%I64d(%s)", _prevBrochureTime.getTime(), _prevBrochureTime.getTimeString().c_str()) );
	if( _prevBrochureId != new_brochure_id || 
		_prevBrochureTime.getTime() != new_brochure_time.getTime() )
	{
		ciDEBUG2(5, ("different brochure_id or brochure_time") );
		if( after_brochure_missing_count == 0 )
		{
			ciDEBUG2(5, ("all-brochure-contents downloaded") );

			ubcConfig aIni("UBCVariables");
			aIni.set("OFS_CMS_INFO", "LastBrochureId", new_brochure_id.c_str() );
			char buf[32] = {0};
			sprintf(buf, "%I64d", new_brochure_time.getTime());
			aIni.set("OFS_CMS_INFO", "LastBrochureTime", buf);

			_prevBrochureId = new_brochure_id;
			_prevBrochureTime = new_brochure_time;

			_runningBrochureId = "";
			_runningBrochureTime.set((time_t)1);
		}
	}

	ciDEBUG2(5, ("_runningBrochureId=%s", _runningBrochureId.c_str()) );
	ciDEBUG2(5, ("_runningBrochureTime=%I64d(%s)", _runningBrochureTime.getTime(), _runningBrochureTime.getTimeString().c_str()) );
	if( _runningBrochureId != new_brochure_id || // different running_brochure_id
		_runningBrochureTime.getTime() != new_brochure_time.getTime() || // different running_brochure_time
		before_brochure_missing_count != after_brochure_missing_count ) // downloaded something-brochure-contents-files
	{
		ciDEBUG2(5, ("restart flash process") );

		_runningBrochureId = new_brochure_id;
		_runningBrochureTime = new_brochure_time;

		// restart flash
		_killFlashProcess(flashFilename.c_str());
		if ( ciArgParser::getInstance()->isSet("+SPC_SALE") )
			_killFlashProcess("chrome.exe");

		_replaceContentsFiles(&contents_list);

		//if( !_writeXML(str_xml.c_str()) )
		//{
		//	// get xml ERROR !!!
		//	ciWARN2( ("_writeXML(BrochureId=%s) ERROR !!!", new_brochure_id.c_str()) );
		//}

		_startFlashProcess();
	}

	// restart delete-contents-manager-timer
	if( ciArgParser::getInstance()->isSet("+HMCSFS") && _deleteConMngTimer!=NULL )
	{
		// HMCSFS ==> resume timer
		_deleteConMngTimer->startTimer();
	}

	// clear list
	_clearContentsInfoList(&contents_list);

	// check init
	if( _initialize==ciFalse )
	{
		_initialize = ciTrue;
		setInterval(60);
	}

	ciDEBUG2(5, ("processExpired(%s) end", name.c_str()) );

	return;
}

void contentsCheckTimer::_initTimer()
{
	srand( (unsigned)time(NULL) );
	int interval = 0;

	// brochure contents manager timer
	interval = 3 + (rand() % 4); // min 3-sec, max 6-sec
	_brochConMngTimer = new contentsManagerTimer(_site.c_str());
	_brochConMngTimer->initTimer("BroConMngTimer", interval); // every n-sec
	_brochConMngTimer->initServerInfo(_svrIp.c_str(), _svrPort, _svrId.c_str(), _svrPwd.c_str());
	_brochConMngTimer->startTimer();

	// goods contents manager timer
	interval = 3 + (rand() % 4); // min 3-sec, max 6-sec
	_goodsConMngTimer = new contentsManagerTimer(_site.c_str());
	_goodsConMngTimer->initTimer("GooConMngTimer", interval); // every n-sec
	_goodsConMngTimer->initServerInfo(_svrIp.c_str(), _svrPort, _svrId.c_str(), _svrPwd.c_str());
	_goodsConMngTimer->startTimer();

	// goods contents manager timer
	_unionConMngTimer = NULL;

	// delete contents manager timer (use only in KHQS or HMCSFS)
	if( ciArgParser::getInstance()->isSet("+KHQS") ||
		ciArgParser::getInstance()->isSet("+HMCSFS") ||
		ciArgParser::getInstance()->isSet("+ROYAL") ||
		ciArgParser::getInstance()->isSet("+SPC_SALE") ||
		ciArgParser::getInstance()->isSet("+SSG_SHOP")
	  )
	{
		_deleteConMngTimer = new contentsManagerTimer(_site.c_str());
		_deleteConMngTimer->initTimer("DelConMngTimer", 1); // every 1-sec
		contentsInfo info;
		_deleteConMngTimer->pushbackToDeleteList(&info); // insert dummy
		_deleteConMngTimer->startTimer();
	}
}

ciBoolean contentsCheckTimer::_checkContentsFile(contentsInfoList* infoList, int& goodsCount, int& brochureCount)
{
	//
	contentsInfoListIter itr = infoList->begin();
	contentsInfoListIter end = infoList->end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* info = (*itr);

		// update _lastestContentsTime
		if( info->lastUpdateTime > _lastestContentsTime )
		{
			_lastestContentsTime = info->lastUpdateTime;
		}

		contentsManagerTimer* timer = NULL;
		switch( info->contentsType )
		{
		default:
		case OFS_UNION_CONTENTS_TYPE:
			timer = _unionConMngTimer;
			break;

		case OFS_BROCHURE_CONTENTS_TYPE:
			timer = _brochConMngTimer;
			break;

		case OFS_GOODS_CONTENTS_TYPE:
			timer = _goodsConMngTimer;
			break;

		case OFS_DELETE_CONTENTS_TYPE:
			if( _deleteConMngTimer )
			{
				_deleteConMngTimer->pushbackToDeleteList(info);
			}
			break;
		}

		if( timer == NULL ) continue;

		// get file-status
		if( info->isExistFile() > 0 )
		{
			// exist local_path
			continue;
		}
		else if( info->isExistTempFile() > 0 )
		{
			// exist temp_path
			ciDEBUG2(5, ("exist temp-file (%s)", GetUTF8fromU(info->localPath.c_str())) );
		}
		else
		{
			// not exist => must download
			ciDEBUG2(5, ("not exist (%s)", GetUTF8fromU(info->localPath.c_str())) );

			// must download  =>  add to contentsManagerTimer
			timer->pushbackToDownloadList(info);
		}

		switch( info->contentsType )
		{
		case OFS_BROCHURE_CONTENTS_TYPE: brochureCount++; break;
		case OFS_GOODS_CONTENTS_TYPE: goodsCount++; break;
		default: break;
		}
	}

	return ciTrue;
}

void contentsCheckTimer::_clearContentsInfoList(contentsInfoList* infoList)
{
	contentsInfo::clearContentsInfoList(*infoList);

	if( _brochConMngTimer ) _brochConMngTimer->clearCompleteInfoList();
	if( _goodsConMngTimer ) _goodsConMngTimer->clearCompleteInfoList();
	if( _unionConMngTimer ) _unionConMngTimer->clearCompleteInfoList();
}

ciBoolean contentsCheckTimer::_setLastUpdateTime(ciTime tm)
{
	//
	char buf[64];
	sprintf(buf, "%I64d", tm.getTime());

	ubcConfig aIni("UBCVariables");
	return aIni.set("OFS_CMS_INFO", "LastSyncTime", buf);
}

ciBoolean contentsCheckTimer::_updateLastSyncTime()
{
	if( _lastSyncTime == _lastestContentsTime )
		return ciTrue;

	if( !_setLastUpdateTime(_lastestContentsTime) )
	{
		ciWARN2(("FAIL TO SET LastSyncTime !!! (time:%I64d)", _lastestContentsTime.getTime()));
		return ciFalse;
	}

	_lastSyncTime = _lastestContentsTime;
	ciDEBUG2(5, ("LastSyncTime is updated (time:%I64d)", _lastSyncTime.getTime()));
	return ciTrue;
}

ciBoolean contentsCheckTimer::isDownloadingContents(const wchar_t* location)
{
	if( _brochConMngTimer == NULL ) return ciFalse;

	return _brochConMngTimer->isDownloadingContents(location);
}

ciLong contentsCheckTimer::_getDownloadRemainCount()
{
	int count = 0;

	if( _brochConMngTimer != NULL ) count += _brochConMngTimer->getRemainingCount();
	if( _goodsConMngTimer != NULL ) count += _goodsConMngTimer->getRemainingCount();
	if( _unionConMngTimer != NULL ) count += _unionConMngTimer->getRemainingCount();

	return count;
}

ciLong contentsCheckTimer::_getAllRemainCount()
{
	int count = _getDownloadRemainCount();

	if( _deleteConMngTimer != NULL ) count += _deleteConMngTimer->getRemainingCount();

	return count;
}

ciBoolean contentsCheckTimer::_httpRequestPost(const char* url, const char* param, ciString& result)
{
	result = "";

	CHttpRequest http_request;
	//http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _cmsServerIp.c_str(), _cmsServerPort);
	http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, contentsDownloaderWorld::getCMSServerIP(), contentsDownloaderWorld::getCMSServerPort());
	if( ciArgParser::getInstance()->isSet("+HMCSFS") )
	{
		// for HMCSFS (add additionalHeader)
		http_request.AddAdditionalHeader(_getAdditionHttpHeader().c_str());
	}

	//
	CString str_buf = "";
	if( !http_request.RequestPost(url, param, str_buf) )
	{
		ciWARN2( ("RequestPost FAIL !!! (URL:%s, ErrMsg:%s)", http_request.GetRequestURL(), http_request.GetErrorMsg()) );
		return ciFalse; // error, false
	}

	result = (LPCSTR)str_buf;
	return ciTrue;
}

ciBoolean contentsCheckTimer::_getContentsDownloadListXML(ciWString& strUniXml)
{
	// get contents_download_list from api

	//char url[] = "api/contentDownloadList/Api013.do";
	char param[1024] = {0};
	sprintf(param, "siteId=%s&hostId=%s&lastUpdateTime=%I64d&returnType=XML", _site.c_str(), _host.c_str(), _lastSyncTime.getTime() );

	char full_url[1024] = {0};
	//sprintf(full_url, "%s:%d/%s?%s", _cmsServerIp.c_str(), _cmsServerPort, _contentsDownloadListURL.c_str(), param);
	sprintf(full_url, "%s:%d/%s?%s", contentsDownloaderWorld::getCMSServerIP(), 
										contentsDownloaderWorld::getCMSServerPort(), 
										contentsDownloaderWorld::getContentsDownloadListURL(), 
										param);
	ciDEBUG2(5, ("_getContentsDownloadListXML(%s)", full_url) );

	ciString str_xml;
	//if( _httpRequestPost(_contentsDownloadListURL.c_str(), param, str_xml) == ciFalse )
	if( _httpRequestPost(contentsDownloaderWorld::getContentsDownloadListURL(), param, str_xml) == ciFalse )
		return ciFalse;

	//
	strUniXml = _getUnicodeXML(str_xml.c_str());
	ciDEBUG2(5, ("RequestPost(%s)\r\n%s", full_url, GetUTF8fromU(strUniXml.c_str())) );

	return ciTrue;
}

ciBoolean contentsCheckTimer::_getBrochureId(ciString& brochureId, ciTime& brochureTime)
{
	// get brochure_id & brochure_time

	//char url[1024] = "api/brochureInfoView/Api008_01.do";
	char param[1024];
	sprintf(param, "siteId=%s&hostId=%s&returnType=XML", _site.c_str(), _host.c_str() );

	char full_url[1024] = {0};
	//sprintf(full_url, "%s:%d/%s?%s", _cmsServerIp.c_str(), _cmsServerPort, _brochureIdURL.c_str(), param);
	sprintf(full_url, "%s:%d/%s?%s", contentsDownloaderWorld::getCMSServerIP(), 
										contentsDownloaderWorld::getCMSServerPort(), 
										contentsDownloaderWorld::getBrochureIdURL(),
										param);
	ciDEBUG2(5, ("_getBrochureId(%s)", full_url) );

	ciString str_xml;
	//if( _httpRequestPost(_brochureIdURL.c_str(), param, str_xml) == ciFalse )
	if( _httpRequestPost(contentsDownloaderWorld::getBrochureIdURL(), param, str_xml) == ciFalse )
		return ciFalse;

	ciString str_xml_ansi = _getAnsiXML(str_xml.c_str());
	ciDEBUG2(5, ("RequestPost(%s)\r\n%s", full_url, str_xml_ansi.c_str()) );

	//
	ciString str_result_flag = _getItem(str_xml_ansi.c_str(), "<resultFlag>", "</resultFlag>");
	ciStringUtil::trim(str_result_flag);

	ciString str_err_msg = _getItem(str_xml_ansi.c_str(), "<resultMsg>", "</resultMsg>");
	ciStringUtil::trim(str_err_msg);

	brochureId = _getItem(str_xml_ansi.c_str(), "<brochureId>", "</brochureId>");
	ciStringUtil::trim(brochureId);

	ciString str_tm = _getItem(str_xml_ansi.c_str(), "<lastUpdateTime>", "</lastUpdateTime>");
	brochureTime.set(_atoi64(str_tm.c_str()));

	ciDEBUG2(5, ("resultFlag : %s", GetUTF8fromA(str_result_flag.c_str())) );
	ciDEBUG2(5, ("resultMsg : %s", GetUTF8fromA(str_err_msg.c_str())) );
	ciDEBUG2(5, ("brochureId : %s", GetUTF8fromA(brochureId.c_str())) );
	ciDEBUG2(5, ("lastUpdateTime : %I64d", brochureTime.getTime()) );

	if( stricmp(str_result_flag.c_str(),"true") != 0 )
	{
		ciWARN2( ("resultFlag is FALSE !!!") );
		return ciFalse;
	}

	return ciTrue;
}

ciBoolean contentsCheckTimer::_getBrochureXml(const char* brochureId, ciWString& strUniXML)
{
	// get brochure-xml

	//char url[1024] = "api/brochureInfoView/Api008.do";
	char param[1024];
	sprintf(param, "brochureId=%s&returnType=XML", brochureId );

	char full_url[1024] = {0};
	//sprintf(full_url, "%s:%d/%s?%s", _cmsServerIp.c_str(), _cmsServerPort, _brochureXMLURL.c_str(), param);
	sprintf(full_url, "%s:%d/%s?%s", contentsDownloaderWorld::getCMSServerIP(), 
										contentsDownloaderWorld::getCMSServerPort(), 
										contentsDownloaderWorld::getBrochureXmlURL(),
										param);
	ciDEBUG2(5, ("_getBrochureId(%s)", full_url) );

	ciString str_xml = "";
	//if( _httpRequestPost(_brochureXMLURL.c_str(), param, strXML) == ciFalse )
	if( _httpRequestPost(contentsDownloaderWorld::getBrochureXmlURL(), param, str_xml) == ciFalse )
		return ciFalse;

	strUniXML = _getUnicodeXML(str_xml.c_str());

	ciDEBUG2(5, ("RequestPost(%s)\r\n%s", full_url, GetUTF8fromU(strUniXML.c_str())) );

	return ciTrue;
}

ciLong contentsCheckTimer::_getBrochureContentsInfoList(const wchar_t* xml, contentsInfoList* infoList)
{
	ciLong missing_count = 0;

	//
	wchar_t* tag_start = (wchar_t*)wcsstr(xml, CONTENTS_IN_BROCHURE_STARTTAG);
	while( tag_start != NULL )
	{
		ciDEBUG2(5, ("find brochure-contents-tag at [%d]", tag_start-xml) );

		// find end-tag
		wchar_t* tag_end = wcsstr(tag_start, CONTENTS_IN_BROCHURE_ENDTAG);
		if( tag_end == NULL ) break;
		tag_end += wcslen(CONTENTS_IN_BROCHURE_ENDTAG);

		// get item-string
		int size = tag_end - tag_start;
		if( size < 0 )
		{
			ciERROR2( ("NEGATIVE SIZE !!!") );
			break;
		}
		wchar_t* buf_xml = new wchar_t[size+1];
		AUTO_DELETE(buf_xml);
		::ZeroMemory(buf_xml, sizeof(wchar_t)*(size+1));
		wcsncpy(buf_xml, tag_start, size);

		// get location in buf_xml
		ciWString str_filename			= _getItem(buf_xml, FILENAME_STARTTAG, FILENAME_ENDTAG);
		ciWString str_filename_ori		= _getItem(buf_xml, FILENAME_ORI_STARTTAG, FILENAME_ORI_ENDTAG);
		ciWString str_filename_alt		= _getItem(buf_xml, FILENAME_ALT_STARTTAG, FILENAME_ALT_ENDTAG);
		ciWString str_location			= _getItem(buf_xml, LOCATION_STARTTAG, LOCATION_ENDTAG);
		ciWString str_volume			= _getItem(buf_xml, VOLUME_STARTTAG, VOLUME_ENDTAG);
		ciWString str_lastupdatetime	= _getItem(buf_xml, ULASTUPDATETIME_STARTTAG, ULASTUPDATETIME_ENDTAG);
		if( str_lastupdatetime.length() == 0 )
			str_lastupdatetime	= _getItem(buf_xml, LASTUPDATETIME_STARTTAG, LASTUPDATETIME_ENDTAG);

		/*ciStringUtil*/::stringReplace(str_filename, L"&amp;", L"&");
		/*ciStringUtil*/::stringReplace(str_filename_ori, L"&amp;", L"&");
		/*ciStringUtil*/::stringReplace(str_filename_alt, L"&amp;", L"&");
		/*ciStringUtil*/::stringReplace(str_location, L"&amp;", L"&");

		// make dummy-xml
		ciWString temp_xml = L"";
		temp_xml += L"<downFile><filename>";
		temp_xml += str_filename;
		temp_xml += L"</filename><filenameOri>";
		temp_xml += str_filename_ori;
		temp_xml += L"</filenameOri><filenameAlt>";
		temp_xml += str_filename_alt;
		temp_xml += L"</filenameAlt><location>";
		temp_xml += str_location;
		temp_xml += L"</location><volume>";
		temp_xml += str_volume;
		temp_xml += L"</volume><lastUpdateTime>";
		temp_xml += str_lastupdatetime;
		temp_xml += L"</lastUpdateTime></downFile>";

		// get contents-info from dummy-xml
		contentsInfo* info = contentsInfo::getContentsInfoObject(temp_xml.c_str());
		if( info != NULL )
		{
			infoList->push_back(info);

			// "<location>contents/KHQS"  ==>  "<location>"
			/*ciStringUtil*/::stringReplace(info->location, L"/contents/KHQS", L"");
			/*ciStringUtil*/::stringReplace(info->location, L"contents/KHQS", L"");
			// "<location>files/pmis"  ==>  "<location>"
			/*ciStringUtil*/::stringReplace(info->location, L"/files/pmis", L"");
			/*ciStringUtil*/::stringReplace(info->location, L"files/pmis", L"");
			// "<location>contents/ROYAL"  ==>  "<location>"
			/*ciStringUtil*/::stringReplace(info->location, L"/contents/JIBOKJAE", L"");
			/*ciStringUtil*/::stringReplace(info->location, L"contents/JIBOKJAE", L"");
			// "<location>contents/spc_sale"  ==>  "<location>"
			/*ciStringUtil*/::stringReplace(info->location, L"/contents/spc_sale", L"");
			/*ciStringUtil*/::stringReplace(info->location, L"contents/spc_sale", L"");

			ciDEBUG2(5, ("brochure-contents-info( %s )", GetUTF8fromU(info->toString(false).c_str())) );
			ciDEBUG2(5, ("local_path=%s", GetUTF8fromU(info->localPath.c_str())) );
			ciDEBUG2(5, ("temp_path=%s", GetUTF8fromU(info->tempPath.c_str())) );

			// check file-exist
			info->isExistFile();
			info->isExistTempFile();

			if( info->existLocalPath == ciFalse && info->existTempPath == ciFalse )
			{
				missing_count++;
				info->volume = 0;
			}
		}
		else
		{
			ciWARN2( ("FAIL TO CREATE BROCHURE-CONTENTS-INFO !!! ( %s )", GetUTF8fromU(temp_xml.c_str())) );
		}

		//
		tag_start = wcsstr(tag_end, CONTENTS_IN_BROCHURE_STARTTAG);
	}

	return missing_count;
}

ciWString contentsCheckTimer::_getXMLforKHQS(contentsInfoList* infoList)
{
	//
	ciWString str_xml = L"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n\t<contents>\r\n";
	ciWString str_folder = L"";
	ciWString str_subfolder = L"";

	contentsInfoListIter itr = infoList->begin();
	contentsInfoListIter end = infoList->end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* info = (*itr);

		ciWString str_location = info->location;
		ciWString str_volume = L"";

		wchar_t buf[64] = {0};
		swprintf(buf, L"%I64u", info->volume);
		str_volume = buf;

		ciWStringTokenizer aTokens(str_location.c_str(), L"/");
		ciWString tmp_folder = aTokens.nextToken();
		ciWString tmp_subfolder = aTokens.nextToken();
		ciWString tmp_filename = (aTokens.restTokens().c_str() + 1);

		if( wcsicmp(str_folder.c_str(), tmp_folder.c_str()) != 0 )
		{
			if( !str_subfolder.empty() )
				str_xml += L"\t\t</subfolder>\r\n";

			if( !str_folder.empty() )
				str_xml += L"\t</folder>\r\n";

			str_folder = tmp_folder;
			str_subfolder = L"";

			str_xml += L"\t<folder name=\"";
			str_xml += str_folder;
			str_xml += L"\">\r\n";
		}

		if( wcsicmp(str_subfolder.c_str(), tmp_subfolder.c_str()) != 0 )
		{
			if( !str_subfolder.empty() )
				str_xml += L"\t\t</subfolder>\r\n";

			str_subfolder = tmp_subfolder;

			str_xml += L"\t\t<subfolder name=\"";
			str_xml += str_subfolder;
			str_xml += L"\">\r\n";
		}

		str_xml += L"\t\t\t<file>\r\n\t\t\t\t<filename>";
		str_xml += tmp_filename;
		str_xml += L"</filename>\r\n\t\t\t\t<location>";
		str_xml += str_location;
		str_xml += L"</location>\r\n\t\t\t\t<volume>";
		str_xml += str_volume;
		str_xml += L"</volume>\r\n\t\t\t\t<contentsType>";
		str_xml += _getContentsType(str_location);
		str_xml += L"</contentsType>\r\n\t\t\t</file>\r\n";
	}

	if( !str_subfolder.empty() )
		str_xml += L"\t\t</subfolder>\r\n";

	if( !str_folder.empty() )
		str_xml += L"\t</folder>\r\n";

	str_xml += L"</contents>";

	return str_xml;
}

ciWString contentsCheckTimer::_getXMLforHMCSFS(contentsInfoList* infoList)
{
	//
	ciWString str_xml = L"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n\t<contents>\r\n";
	ciWString str_folder = L"";
	ciWString str_subfolder = L"";
	ciWString str_subsubfolder = L"";
	ciWString str_subsubsubfolder = L"";
	ciWString str_subsubsubsubfolder = L"";

	contentsInfoListIter itr = infoList->begin();
	contentsInfoListIter end = infoList->end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* info = (*itr);

		ciWString str_location = info->location;
		ciWString str_volume;

		wchar_t buf[64] = {0};
		swprintf(buf, L"%I64u", info->volume);
		str_volume = buf;

		/*ciStringUtil*/::stringReplace(str_location, info->filename.c_str(), info->filenameOri.c_str());

		ciWStringTokenizer aTokens(str_location.c_str(), L"/");
		ciWString tmp_folder = L"";
		ciWString tmp_subfolder = L"";
		ciWString tmp_subsubfolder = L"";
		ciWString tmp_subsubsubfolder = L"";
		ciWString tmp_subsubsubsubfolder = L"";
		ciWString tmp_filename = L"";
		switch( aTokens.countTokens() )
		{
		case 1:
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		case 2:
			tmp_folder = aTokens.nextToken();
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		case 3:
			tmp_folder = aTokens.nextToken();
			tmp_subfolder = aTokens.nextToken();
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		case 4:
			tmp_folder = aTokens.nextToken();
			tmp_subfolder = aTokens.nextToken();
			tmp_subsubfolder = aTokens.nextToken();
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		case 5:
			tmp_folder = aTokens.nextToken();
			tmp_subfolder = aTokens.nextToken();
			tmp_subsubfolder = aTokens.nextToken();
			tmp_subsubsubfolder = aTokens.nextToken();
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		case 6:
		default:
			tmp_folder = aTokens.nextToken();
			tmp_subfolder = aTokens.nextToken();
			tmp_subsubfolder = aTokens.nextToken();
			tmp_subsubsubfolder = aTokens.nextToken();
			tmp_subsubsubsubfolder = aTokens.nextToken();
			tmp_filename = (aTokens.restTokens().c_str() + 1);
			break;
		}

		if( wcsicmp(str_folder.c_str(), tmp_folder.c_str()) != 0 )
		{
			if( !str_subsubsubsubfolder.empty() ) str_xml += L"\t\t\t\t\t</subsubsubsubfolder>\r\n";
			if( !str_subsubsubfolder.empty() ) str_xml += L"\t\t\t\t</subsubsubfolder>\r\n";
			if( !str_subsubfolder.empty() ) str_xml += L"\t\t\t</subsubfolder>\r\n";
			if( !str_subfolder.empty() ) str_xml += L"\t\t</subfolder>\r\n";
			if( !str_folder.empty() ) str_xml += L"\t</folder>\r\n";

			str_folder = tmp_folder;
			str_subfolder = L"";
			str_subsubfolder = L"";
			str_subsubsubfolder = L"";
			str_subsubsubsubfolder = L"";

			str_xml += L"\t<folder name=\"";
			str_xml += str_folder;
			str_xml += L"\">\r\n";
		}

		if( wcsicmp(str_subfolder.c_str(), tmp_subfolder.c_str()) != 0 )
		{
			if( !str_subsubsubsubfolder.empty() ) str_xml += L"\t\t\t\t\t</subsubsubsubfolder>\r\n";
			if( !str_subsubsubfolder.empty() ) str_xml += L"\t\t\t\t</subsubsubfolder>\r\n";
			if( !str_subsubfolder.empty() ) str_xml += L"\t\t\t</subsubfolder>\r\n";
			if( !str_subfolder.empty() ) str_xml += L"\t\t</subfolder>\r\n";

			str_subfolder = tmp_subfolder;
			str_subsubfolder = L"";
			str_subsubsubfolder = L"";
			str_subsubsubsubfolder = L"";

			str_xml += L"\t\t<subfolder name=\"";
			str_xml += str_subfolder;
			str_xml += L"\">\r\n";
		}

		if( wcsicmp(str_subsubfolder.c_str(), tmp_subsubfolder.c_str()) != 0 )
		{
			if( !str_subsubsubsubfolder.empty() ) str_xml += L"\t\t\t\t\t</subsubsubsubfolder>\r\n";
			if( !str_subsubsubfolder.empty() ) str_xml += L"\t\t\t\t</subsubsubfolder>\r\n";
			if( !str_subsubfolder.empty() ) str_xml += L"\t\t\t</subsubfolder>\r\n";

			str_subsubfolder = tmp_subsubfolder;
			str_subsubsubfolder = L"";
			str_subsubsubsubfolder = L"";

			str_xml += L"\t\t\t<subsubfolder name=\"";
			str_xml += str_subsubfolder;
			str_xml += L"\">\r\n";
		}

		if( wcsicmp(str_subsubsubfolder.c_str(), tmp_subsubsubfolder.c_str()) != 0 )
		{
			if( !str_subsubsubsubfolder.empty() ) str_xml += L"\t\t\t\t\t</subsubsubsubfolder>\r\n";
			if( !str_subsubsubfolder.empty() ) str_xml += L"\t\t\t\t</subsubsubfolder>\r\n";

			str_subsubsubfolder = tmp_subsubsubfolder;
			str_subsubsubsubfolder = L"";

			str_xml += L"\t\t\t\t<subsubsubfolder name=\"";
			str_xml += str_subsubsubfolder;
			str_xml += L"\">\r\n";
		}

		if( wcsicmp(str_subsubsubsubfolder.c_str(), tmp_subsubsubsubfolder.c_str()) != 0 )
		{
			if( !str_subsubsubsubfolder.empty() ) str_xml += L"\t\t\t\t\t</subsubsubsubfolder>\r\n";

			str_subsubsubsubfolder = tmp_subsubsubsubfolder;

			str_xml += L"\t\t\t\t\t<subsubsubsubfolder name=\"";
			str_xml += str_subsubsubsubfolder;
			str_xml += L"\">\r\n";
		}

		str_xml += L"\t\t\t\t\t\t<file>\r\n\t\t\t\t\t\t\t<filename>";
		str_xml += tmp_filename;
		str_xml += L"</filename>\r\n\t\t\t\t\t\t\t<location>";
		str_xml += str_location;
		str_xml += L"</location>\r\n\t\t\t\t\t\t\t<volume>";
		str_xml += str_volume;
		str_xml += L"</volume>\r\n\t\t\t\t\t\t</file>\r\n";
	}

	if( !str_subsubsubsubfolder.empty() )
		str_xml += L"\t\t\t\t\t</subsubsubsubfolder>\r\n";

	if( !str_subsubsubfolder.empty() )
		str_xml += L"\t\t\t\t</subsubsubfolder>\r\n";

	if( !str_subsubfolder.empty() )
		str_xml += L"\t\t\t</subsubfolder>\r\n";

	if( !str_subfolder.empty() )
		str_xml += L"\t\t</subfolder>\r\n";

	if( !str_folder.empty() )
		str_xml += L"\t</folder>\r\n";

	str_xml += L"</contents>";

	return str_xml;
}

ciBoolean contentsCheckTimer::_writeXML(const wchar_t* xml, ciBoolean overwrite)
{
	ciString str_utf8_xml = "";
	UtoUTF8(xml, str_utf8_xml);

	ciDEBUG2(5, ("_writeXML(%s)", GetUTF8fromU(_brochureXmlPath.c_str())) );

	if( overwrite == ciFalse )
	{
		FILE* fp = _wfopen(_brochureXmlPath.c_str(), L"rb");
		if( fp != NULL )
		{
			fclose(fp);
			return ciTrue;
		}
	}

	//
	createFullDirectory(_brochureXmlPath.c_str());

	//
	FILE* fp = _wfopen(_brochureXmlPath.c_str(), L"wb");
	if( fp == NULL )
	{
		//
		ciERROR2( ("FAIL TO fopen() !!! (%s)", GetUTF8fromU(_brochureXmlPath.c_str())) );
		return ciFalse;
	}

	int xml_size = str_utf8_xml.length();
	int write_size = fwrite(str_utf8_xml.c_str(), 1, xml_size, fp); // no BOM (of utf-8)
	fclose(fp);

	if( write_size != xml_size )
	{
		//
		ciERROR2( ("FAIL TO fwrite() !!! (%s)", GetUTF8fromU(_brochureXmlPath.c_str())) );
		return ciFalse;
	}

	return ciTrue;
}

ciBoolean contentsCheckTimer::_killFlashProcess(const char* process_name/*=NULL*/)
{
	ciDEBUG2(5, ("_killFlashProcess(%s)", process_name) );
	if( process_name == NULL ) return ciFalse;

	for(int count=0; count<100; count++ ) // do until 10-sec
	{
		int flash_pid = scratchUtil::getInstance()->getPid(process_name);
		if( flash_pid == 0 )
		{
			ciDEBUG2(5, ("success to killProcess(%s)", process_name) );
			return ciTrue;
		}
		scratchUtil::getInstance()->killProcess(flash_pid);
		::Sleep(100); // wait 0.1sec
	}

	ciWARN2( ("FAIL killProcess !!! (%s)", process_name) );
	return ciFalse;
}

ciULong contentsCheckTimer::_createProcess(const char *exe, const char* dir, const char* argument, BOOL minFlag)
{
	ciDEBUG2(5, ("_createProcess(%s, %s)", exe, dir) );

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	::ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESHOWWINDOW;
	if (minFlag)	si.wShowWindow = SW_MINIMIZE;
	else			si.wShowWindow = SW_NORMAL;
	//si.lpDesktop = "Winsta0\\Default";

	//if(!dir || !strlen(dir)){
	//	dir = UBC_HOME;
	//}

	BOOL bRun = CreateProcess(
		NULL, 
		(LPSTR)exe, 
		NULL, 
		NULL, 
		FALSE, 
		NORMAL_PRIORITY_CLASS, 
		NULL, 
		dir, 
		&si, 
		&pi
		);

	CloseHandle( pi.hThread );
	CloseHandle( pi.hProcess );

	if (bRun)
	{
		//myDEBUG("createProcess : pid(%d)\n", pi.dwProcessId);
		return pi.dwProcessId;
	}
	//myDEBUG("createProcess : fail to CreateProcess\n");
	return 0;
}

ciULong contentsCheckTimer::_startFlashProcess()
{
	ciDEBUG2(5, ("_startFlashProcess(%s)", flashFilename.c_str()) );

	if ( ciArgParser::getInstance()->isSet("+SPC_SALE") )
		_killFlashProcess("chrome.exe");

	if( flashFilename.length() == 0 ) return 0;

	// execute C:/SQISoft/Contents/ENC/OFS_xxx/OFS_xxx.exe
	ciString fulldir = flashPath;
	if( fulldir.length() == 0 || fulldir[fulldir.length()-1]!='\\') fulldir+="\\";
	fulldir += flashSubPath;

	ciString fullpath = fulldir;
	if( fullpath.length() == 0 || fullpath[fullpath.length()-1]!='\\') fullpath+="\\";
	fullpath += flashFilename;

	if( flashArgument.length() > 0)
	{
		fullpath += " ";
		fullpath += flashArgument;
	}

	ciDEBUG2(5, ("_createProcess(%s)", fullpath.c_str()) );
	ciULong pid = _createProcess(fullpath.c_str(), fulldir.c_str(), flashArgument.c_str(), FALSE);
	if( pid > 0 )
	{
		ciDEBUG2(5, ("success to createProcess(%d)", pid) );
		return pid;
	}

	// execute C:/SQISoft/Contents/ENC/OFS_xxx.exe
	fullpath = flashPath;
	fullpath += flashFilename;

	if( flashArgument.length() > 0)
	{
		fullpath += " ";
		fullpath += flashArgument;
	}

	ciDEBUG2(5, ("_createProcess(%s)", fullpath.c_str()) );
	pid = _createProcess(fullpath.c_str(), flashPath.c_str(), flashArgument.c_str(), FALSE);
	if( pid > 0 )
	{
		ciDEBUG2(5, ("success to createProcess(%d)", pid) );
		return pid;
	}

	ciWARN2( ("FAIL createProcess !!! (%s)(errcode:%d)", fullpath.c_str(), GetLastError()) );
	return 0;
}

ciULong contentsCheckTimer::_getFlashPID()
{
	if( flashFilename.length() == 0 ) return 0;

	return scratchUtil::getInstance()->getPid(flashFilename.c_str());
}

ciBoolean contentsCheckTimer::_replaceContentsFiles(contentsInfoList* infoList)
{
	ciDEBUG2(5, ("_replaceContentsFiles()") );

	contentsInfoListIter itr = infoList->begin();
	contentsInfoListIter end = infoList->end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* info = (*itr);

		ciDEBUG2(5, ("replaceFile(%s)", GetUTF8fromU(info->toString(ciFalse).c_str())) );
		if( !info->replaceFile() )
		{
			ciWARN( ("FAIL to replaceFile !!! (%s)", GetUTF8fromU(info->toString(ciFalse).c_str())) );
		}
	}

	return ciTrue;
}

const wchar_t* contentsCheckTimer::_getContentsType(ciWString location)
{
	ciString str_location = "";
	UtoA(location.c_str(), str_location);
	ciStringUtil::toLower(str_location);

	if (strstr(str_location.c_str(), ".avi")!=NULL ||
		strstr(str_location.c_str(), ".flv")!=NULL ||
		strstr(str_location.c_str(), ".mp4")!=NULL ||
		strstr(str_location.c_str(), ".mkv")!=NULL ||
		strstr(str_location.c_str(), ".wmv")!=NULL)
	{
		return L"0";
	}

	if (strstr(str_location.c_str(), ".bmp")!=NULL ||
		strstr(str_location.c_str(), ".jpg")!=NULL ||
		strstr(str_location.c_str(), ".png")!=NULL)
	{
		return L"1";
	}

	return L"-1";
}
