#ifndef _contentsDownloadTimer_H_
#define _contentsDownloadTimer_H_

//#include "afxwin.h"
//#include "afxcmn.h"
#include "common/libFileServiceWrap/FileServiceWrapDll.h"
#include "contents.h"

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>


class contentsDownloadTimer : public ciWinPollable
					, public CFileServiceWrap::IProgressHandler
{
public:
	contentsDownloadTimer(const char* site);
	virtual	~contentsDownloadTimer();

	enum DOWNLOAD_STATUS
	{
		DS_INIT = 0,
		DS_LOGINING,
		DS_READY,
		DS_DOWNLOADING,
		DS_COMPLETED,
		DS_ERROR
	};

	virtual	void	processExpired(ciString name, int counter, int interval);

	void			initServerInfo(const char* ip, ciLong port, const char* id, const char* pwd);

	virtual	void	ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath);

	void			setContentsInfo(contentsInfo* info);
	DOWNLOAD_STATUS	getStatus() { return _status; };
	ULONGLONG		getDownloadedSize() { return _downloadedSize; };
	ciWString		getLocalPath() { return _info.localPath; };

protected:
	//ciString	_cmsServerIp;
	//ciLong		_cmsServerPort;
	ciString	_downloadStartTime;
	ciString	_downloadEndTime;
	ciBoolean	_isNowDownloadTime();

	DOWNLOAD_STATUS		_status;
	ciMutex				_lock;
	ciBoolean			_isInfoExist;
	contentsInfo		_info;
	ULONGLONG			_downloadedSize;

	CFileServiceWrap	m_objFileSvcClient;

	ciString		_name;
	// server info
	ciString		_site;
	ciString		_ip;
	ciLong			_port;
	ciString		_id;
	ciString		_pwd;

};

#endif //_contentsDownloadTimer_H_
