 /*! \file netExporterWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _netExporterWorld_h_
#define _netExporterWorld_h_

#include <ci/libWorld/ciWorld.h> 

class netExporterWorld : public ciWorld {
public:
	netExporterWorld();
	~netExporterWorld();
 	
	//[
	//	From ciWorld
	//
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);
	//
	//]

	ciBoolean udpServer(int port);
	ciBoolean getTimeStr(string& timeStr);
	string curTimeStr();
/*
	ciBoolean parse(ofstream& out , const char* command);
	ciBoolean import(ofstream& out, string& scheduleFile);
	ciBoolean shutdownTime(ofstream& out, string& timeStr);
	
*/
protected:	


};
		
#endif //_netExporterWorld_h_
