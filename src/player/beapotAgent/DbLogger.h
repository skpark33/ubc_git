/*! \class DbLogger
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/10/06 11:01:00
 */

#ifndef PLAYER_BEAPOTAGENT_DBLOGGER_H
#define PLAYER_BEAPOTAGENT_DBLOGGER_H

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include <ci/libMySQL/ciMySQL.h>

#include "BeapotWorker.h"
#include "ChannelDefine.h"

class DbLogger : public ciThread
{
public:
	static DbLogger* getInstance();
	static void clearInstance();

    virtual ~DbLogger(void);
    
    void    loadIni();

    void    run();

    void    StartDB();
    void    StopDB();

    void    Enqueue(BEACON_SCAN_MSG& data);
    void    EnqueuePick(GOODS_PICK_UPDOWN& data);

protected:
    bool    ExecuteDml_(const char* sz_sql);

    // iot_beacon_log, iot_beacon_freeze_log
    bool    insertCheckMap_(BEACON_SCAN_MSG& data);
    bool    updateCheckMap_(BEACON_SCAN_MSG& data);

    bool    InsertIotBeaconLog_(BEACON_SCAN_MSG& data);
    bool    UpdateIotBeacon_(BEACON_SCAN_MSG& data);
    bool    InsertIotBeaconFreezeLog_(BEACON_SCAN_MSG& data);
    void    updateIotBeaconOperstate_();

    // iot_pickup_log
    bool    InsertIotPickupLog_(GOODS_PICK_UPDOWN& data);

protected:
    DbLogger(void);

    list<BEACON_SCAN_MSG>    queue_;
    ciMutex		    queue_lock_;

    list<GOODS_PICK_UPDOWN>    pick_queue_;
    ciMutex		    pick_queue_lock_;

    bool            started_;

    ciMySQL*        p_mysql_db_;
    string          db_type_;
    string          db_name_;
    string          db_user_;
    string          db_pass_;
    string          db_host_;
    ciLong          db_port_;    
    string          site_id_;
    ciShort         beacon_gap_min_;
    ciMutex         db_lock_;

    // operstate timer check
    time_t          operstate_time_t_;

    // key : beaconId, value : last ctime 
    map<string, time_t>  insert_check_map_; 

    // key : beaconId, value : last ctime  
    map<string, time_t>  update_check_map_;

private:
	static ciMutex _sLock;
	static DbLogger* _instance;
};

#endif //PLAYER_BEAPOTAGENT_DBLOGGER_H