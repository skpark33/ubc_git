/** \class elMysqlEndpoint
 *  Copyright ⓒ 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief elMysqlEndpoint는 기본적으로 타이머와 쓰레드를 담고 있다.
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elMysqlEndpoint_h_
#define _elMysqlEndpoint_h_

#include "elEndpoint.h"
#include "elMySQLDriver.h"
#include "elMysqlSession.h"

class elMysqlEndpoint : public elEndpoint
{
public:
    elMysqlEndpoint(void);
    ~elMysqlEndpoint(void);

    virtual bool Submit(string el_command_type, string el_command_id, string str_command, string& entity, elRecord& field_name_arr, elRecordList& records);

    bool    Init(string my_id);
    bool    Start(string option = ADMIN_STATE_START_NO);
    bool    Stop();

    bool    Select(string str_sql);
    bool    GetRecordList(elRecordList& selected_records);
    bool    Next(elRecord& record);

    bool    Execute(string str_sql);
    bool    ExecuteSequential(string str_sql, elRecordList& values);
    bool    ExecuteDeleteInsert(string str_sql, elRecordList& values);
    bool    ExecuteInsert(string str_sql, elRecordList& values);
    bool    ExecuteBulkInsert(string table_name, elRecordList& values);

protected:
    void    StartDB_();
    void    StopDB_();

protected:
    ciMutex		    queue_lock_;
    elMysqlSession* p_mysql_session_;

    string          db_type_;
    string          db_name_;
    string          db_user_;
    string          db_pass_;
    string          db_host_;
    ciLong          db_port_;    
    string          url_;
    string          protocol_;
    string          return_type_;

    string          entity_;
    elRecord        field_name_arr_;

};

#endif //_elMysqlEndpoint_h_