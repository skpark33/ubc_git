#include "elComponent.h"
#include "elTimer.h"

elComponent::elComponent() {
    timer_ = NULL;
}

elComponent::~elComponent() {
    StopTimer();
    if (timer_) delete timer_;
}

void elComponent::InitTimer(const char* pname, int interval_sec)
{ 
	if(timer_) {
		StopTimer();
		delete timer_; 
        timer_ = NULL;
	}
	timer_ = new elTimer(pname, this); 
    timer_->SetInterval(interval_sec);
}

void elComponent::StartTimer()
{ 
    if(timer_) timer_->StartTimer(); 
}

void elComponent::StopTimer()
{ 
    if(timer_) timer_->StopTimer();
}

void elComponent::SetInterval(int interval_msec)
{ 
    if(timer_) timer_->SetInterval(interval_msec);
}

// Thread method
void elComponent::StartThread()
{
    started_ = true;
    start();

}

void elComponent::StopThread()
{
	if(started_) {
		started_ = false;
		stop(); 
	}
}

void elComponent::run()
{
}

