#ifndef PLAYER_BEAPOTAGENT_CHANNELDEFINE_H
#define PLAYER_BEAPOTAGENT_CHANNELDEFINE_H

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>

#define ACCEPT_TCP_PERMANENT_CHANNEL            "ACCEPT_TCP_PERMANENT_CHANNEL"
#define ACCEPT_TCP_ONETRANSACTION_CHANNEL       "ACCEPT_TCP_ONETRANSACTION_CHANNEL"
#define ACCEPT_TCP_DUMMY_CHANNEL                "ACCEPT_TCP_DUMMY_CHANNEL"

#define CONNECT_TCP_PERMANENT_CHANNEL           "CONNECT_TCP_PERMANENT_CHANNEL"
#define CONNECT_TCP_ONETRANSACTION_CHANNEL      "CONNECT_TCP_ONETRANSACTION_CHANNEL"

class TcpStreamServer;
class TcpStreamChannel;

class ServiceAccessWorker
{
public:
    // 향후 recv 파라메터를 BufferBase로 수정
    virtual void RecvFromChannel(string& str_msg) = 0;
    virtual void SendToService(string& str_msg) = 0;
    virtual void RegisterTcpChannel(const char* channel_id, TcpStreamChannel* p_channel) = 0;
};

typedef struct _BEACON_ACCEL_DATA {
    string  beacon_id;
    string  scanner_id;
    int     battery;
    int     rssi;
    double  accel_x;
    double  accel_y;
    double  accel_z;
} BEACON_ACCEL_DATA;

typedef struct _GOODS_PICK_UPDOWN {
    string  beacon_id;
    string  goods_id;
    time_t  pickup_time;
    time_t  pickdown_time;
    time_t  exist_time;
    int     pickup_status;
} GOODS_PICK_UPDOWN;

#endif //PLAYER_BEAPOTAGENT_CHANNELDEFINE_H