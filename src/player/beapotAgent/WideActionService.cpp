#include "WideActionService.h"

#include <ci/libDebug/ciDebug.h>
#include "ci/libBase/ciStringTokenizer.h"
#include "common/libScratch/scratchUtil.h"
#include "common/libCommon/ubcIni.h"

ciSET_DEBUG(5, "WideActionService");

// Circular Queue Size는 가장 최근 4개까지 처리하기 위해 size를 5로 등록
WideActionService::WideActionService() : started_(false)
{
    ciDEBUG(5, ("WideActionService() "));

    // Default 값 셋팅
    site_id_ = "SE";
    api_adapter_ip_ = "127.0.0.1";
    api_adapter_port_ = 8000;

    loadIni();

    goodsInfoManager* p_goods_info_mgr = goodsInfoManager::getInstance();
    p_goods_info_mgr->setConnection(site_id_.c_str(), api_adapter_ip_.c_str(), api_adapter_port_);

}

WideActionService::~WideActionService(void)
{
    StopService();
}

void WideActionService::loadIni()
{
    //C:\\SQISoft\\UTV1.0\\execute\\data\\UBCVariables.ini
	ubcConfig aIni("UBCVariables");

    aIni.get("ROOT", "SITEID", site_id_);
    // XML에서 읽어야 한다.
    aIni.get("OFS", "API_ADAPTER_IP", api_adapter_ip_);
    aIni.get("OFS", "API_ADAPTER_PORT", api_adapter_port_);

    // Default 값 셋팅
    if (site_id_ == "") site_id_ = "SE";

    if (api_adapter_port_ == 0) {
        api_adapter_ip_ = "127.0.0.1";
        api_adapter_port_ = 8000;
    }
}

void WideActionService::StartService()
{
    ciDEBUG(5, ("StartService() "));

    ciDEBUG(3, ("before WideActionService thread start()"));    
    started_ = true;
    start();
    ciDEBUG(3, ("after WideActionService thread start()"));

}

void WideActionService::StopService()
{
    ciDEBUG(5, ("StopService... "));
	if(started_) {
		started_ = false;
		stop(); 
	}
}

void WideActionService::run()
{
    ciDEBUG(5, ("run... "));

    while(started_) 
    {
        if (beacon_action_queue_.size() > 0)
        {
            BEACON_ACTION_INFO beacon_action_info;
            {
            ciGuard aGuard(beacon_action_queue_lock_);
            beacon_action_info = beacon_action_queue_.front();
            beacon_action_queue_.pop_front();
            }
            ciDEBUG2(1,("run(beacon_action_queue_.size<%d>)", beacon_action_queue_.size()));

            //1. goodsInfoManager에서 상품정보 매핑
            goodsInfoList goods_info_list;
			if(GetGoodsInfo_(beacon_action_info, goods_info_list)){

				//PickCount set
				SetPickupCount_(beacon_action_info.action_type, goods_info_list);

                // 
                EnqueuePickupLogDb();

                // goods_info_list goodsInfo* delete 추가, memory leak 제거
                ReleaseGoodsInfo(goods_info_list);
			}
        } else {
            Sleep(100); // 100ms
        }
        
    }
}


void WideActionService::processEvent(ciString& actionType, ciStringList& beaconIdList, int additionalState)
{
    ciDEBUG2(1,("processEvent(%s, beaconListCnt[%d], %d) ", actionType.c_str(), beaconIdList.size(), additionalState));
    
    BEACON_ACTION_INFO action_info(actionType, beaconIdList); 
    {
    ciGuard aGuard(beacon_action_queue_lock_);        
    beacon_action_queue_.push_back(action_info);
    }
    ciDEBUG2(5, ("processEvent(beacon_action_queue_.size<%d>)", beacon_action_queue_.size()));
}


bool WideActionService::GetGoodsInfo_(BEACON_ACTION_INFO& beacon_action_info, goodsInfoList& out_goods_list)
{
    ciDEBUG(5, ("GetGoodsInfo_(beacon_action_info.beacon_id_list:%d) ", beacon_action_info.beacon_id_list.size() ));
    
    goodsInfoManager* p_goods_info_mgr = goodsInfoManager::getInstance();
    InitPickDown_(); // 2015-11-13

    int real_beacon_no = 0;
    int failCount = 0;
    goodsInfo goods_out_info;
    list<string>::iterator beacon_itr;
    for (beacon_itr = beacon_action_info.beacon_id_list.begin(); beacon_itr != beacon_action_info.beacon_id_list.end(); beacon_itr++) {
        
        ciString str_beacon_id = *beacon_itr;
        goodsInfo* p_goods_info = new goodsInfo;
        p_goods_info->goodsId = "NULL";
 
        if(str_beacon_id != "NULL") {
            ciDEBUG2(1,("GetGoodsInfo_(str_beacon_id:%s) ", str_beacon_id.c_str()));
            if (!p_goods_info_mgr->get(str_beacon_id.c_str(), *p_goods_info)) {
                ciDEBUG(5, ("p_goods_info_mgr->get. Not exists str_beacon_id[%s] ", str_beacon_id.c_str()));
                //continue; // 상품 못 가져오면 다음 상품조회
				failCount++;
			}else{
	            real_beacon_no++;
			}
        }
        SetPickUp_(str_beacon_id, p_goods_info->goodsId);
        out_goods_list.push_back(p_goods_info);
    }
	if(failCount == 3){
		return false;
	}

    ciDEBUG(5, ("GetGoodsInfo_(real_beacon_no:%d) ", real_beacon_no ));
    return true;
}

bool WideActionService::SetPickupCount_(ciString& action_type, goodsInfoList& in_goods_info_list)
{
    ciDEBUG(5, ("SetPickupCount_(%s, %d) ", action_type.c_str(), in_goods_info_list.size() ));
    
    goodsInfoManager* p_goods_info_mgr = goodsInfoManager::getInstance();
    string str_send_msg("");

    int i = 0;
    list<goodsInfo*>::iterator itr;
    for (itr = in_goods_info_list.begin(); itr != in_goods_info_list.end(); itr++) {
        goodsInfoList out_best123_list;
        goodsInfo* p_goods_info = *itr;
        if (p_goods_info->goodsId != "NULL") {
            if (!p_goods_info_mgr->setPickupCount(p_goods_info->goodsId.c_str(), action_type.c_str(), out_best123_list)) {
                ciDEBUG(5, ("p_goods_info_mgr->setPickupCount. Failed! goodsId_%d[%s] ", i, p_goods_info->goodsId.c_str() ));
            } 
            ciDEBUG(5, ("p_goods_info_mgr->setPickupCount. Okay! action_type[%s] goodsId_%d[%s]", action_type.c_str(), i, p_goods_info->goodsId.c_str()));
        }

    }

    return true;
}

void WideActionService::InitPickDown_()
{
    ciDEBUG(5, ("InitPickDown_"));

    map<string, GOODS_PICK_UPDOWN>::iterator itr;

    for (itr = pickup_goods_map_.begin(); itr != pickup_goods_map_.end(); itr++) {
        itr->second.pickup_status = PICK_DOWN;
    }
    null_beacon_cnt_ = 0;
}


void WideActionService::SetPickUp_(ciString& beacon_id, ciString& goods_id)
{
    ciDEBUG(5, ("SetPickUp_(%s, %s) ", beacon_id.c_str(), goods_id.c_str()));
    
    if (beacon_id != "NULL" && goods_id != "NULL") {
        // beacon_id가 "NULL"이 아니면 map에 없으면 push
		map<string, GOODS_PICK_UPDOWN>::iterator itr = pickup_goods_map_.find(beacon_id);

		if(itr == pickup_goods_map_.end()) {
            GOODS_PICK_UPDOWN pick;
            pick.beacon_id      = beacon_id;
            pick.goods_id       = goods_id;
            pick.pickup_time    = time(NULL);
            pick.pickdown_time  = 0;
            pick.exist_time     = 0;
            pick.pickup_status  = PICK_INIT;

            pickup_goods_map_[beacon_id] = pick;
            ciDEBUG(5, ("SetPickUp_ pickup_goods_map_(%s, %s) PICK_INIT ", pickup_goods_map_[beacon_id].beacon_id.c_str(), pickup_goods_map_[beacon_id].goods_id.c_str()));
		}else{
            // map에 있으면 존재했던 시간 계산 입력, 필요없나?
            pickup_goods_map_[beacon_id].exist_time = time(NULL);
            // pickup 상태 
            pickup_goods_map_[beacon_id].pickup_status = PICK_UP;
            ciDEBUG(5, ("SetPickUp_ pickup_goods_map_(%s, %s) PICK_UP ", pickup_goods_map_[beacon_id].beacon_id.c_str(), pickup_goods_map_[beacon_id].goods_id.c_str()));
		}
        
    } else {    
        // beacon_id가 "NULL" 이면 갯수 만큼 exist_time이 오래된 순서대로 map에서 PICK_DOWN 할까 아님 다 PICK_DOWN으로 판단할까? PICK_INIT, PICK_UP이 아닌 비콘은 전체 PICK_DOWN으로 판단.        
        // beacon_id가 "NULL"이 아니나 goods_id는 "NULL"인 경우는 PICK_DOWN 상태가 된다. API가 오류나도 DbLogger에서 iot_goods_map에서 goods_id가 있다면 iot_pickup_log는 정상적으로 insert된다.
        null_beacon_cnt_++; // null_beacon_cnt_는 현재 쓰이지는 않지만 계산은 한다.
    }
}

void WideActionService::EnqueuePickupLogDb()
{
    ciDEBUG(5, ("EnqueuePickupLogDb() "));
    
    p_db_logger_ = DbLogger::getInstance();

    // 2015-11-13 단순하게 null_beacon_cnt를 확인하지 않고 무조건 PICK_DOWN인건 전부 DB로 insert
    //            PICK_DOWN만 erase
    map<string, GOODS_PICK_UPDOWN>::iterator itr;
    for (itr = pickup_goods_map_.begin(); itr != pickup_goods_map_.end(); ) {
        if (itr->second.pickup_status == PICK_DOWN) {
            itr->second.pickdown_time = time(NULL);
            p_db_logger_->EnqueuePick(itr->second);
            ciDEBUG(5, ("EnqueuePick(%s, %s) PICK_DOWN ", itr->second.beacon_id.c_str(), itr->second.goods_id.c_str()));
            pickup_goods_map_.erase(itr++);
        } else {
            ++itr;
        }
    }

}

void WideActionService::ReleaseGoodsInfo(goodsInfoList& goods_info_list)
{
    ciDEBUG(5, ("ReleaseGoodsInfo() "));

    goodsInfoList::iterator goods_itr;    
    
    for (goods_itr = goods_info_list.begin(); goods_itr != goods_info_list.end(); goods_itr++) {
        goodsInfo* p_goods_info = *goods_itr;
        delete p_goods_info;
        p_goods_info = NULL;
    }
}