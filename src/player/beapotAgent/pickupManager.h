#ifndef _pickupManager_h_
#define _pickupManager_h_

#include "actionManager.h"

class pickupManager : public actionManager
{
public:
	pickupManager(actionHandler* handler, int threshold = 5, int maxOrder=3, int expiredSec=2)
		:actionManager("PICKUP", handler,maxOrder,expiredSec)
	{ _no_move_threshold_count = threshold; _pickup_threshold = 500; loadIni();}
	virtual ~pickupManager() {};

	virtual int 	received(const char* beaconId, const char* scannerId,
						ciLong battery, ciLong rssi,
						ciLong xiro_x, ciLong xiro_y, ciLong xiro_z);

	virtual	void	timeExpired();
	virtual void	loadIni();

	enum 
	{
		NO_MOVE = 1,
		MOVE,
		PICK_UP
	};


protected:
	int _actionAnalisys(const char* beaconId, ciLong x,ciLong y,ciLong z, ciShort axis);

	ciLong _no_move_threshold_count;
	ciLong _pickup_threshold;

	DeltaMap	_deltaMap;
};

#endif // _pickupManager_h_
