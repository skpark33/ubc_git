/** \class elComponent
 *  Copyright ⓒ 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief elComponent는 기본적으로 타이머와 쓰레드를 담고 있다.
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elComponent_h_
#define _elComponent_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciThread.h>
#include "elTimer.h"

#include "elComponentRule.h"
#include "elGatewayRule.h"
#include "elRouteRule.h"
#include "elJobRule.h"
#include "elCommandRule.h"
#include "elRuleRepository.h"

#define ADMIN_STATE_STOP          "0"
#define ADMIN_STATE_START_ALL     "1"
#define ADMIN_STATE_START_TIMER   "2"
#define ADMIN_STATE_START_THREAD  "3"
#define ADMIN_STATE_START_CHILD   "4"
#define ADMIN_STATE_START_NO      "5"

class elComponent : public ciThread
{
public:
    elComponent();
    virtual ~elComponent();

    // ABS
    virtual void ProcessExpired(string name, int counter, int interval_sec, int interval_msec) = 0;
    virtual void Update() = 0;
    virtual bool Init(string my_id) = 0;
    virtual bool Start(string option) = 0;
    virtual bool Stop() = 0;

    // Timer method
    virtual void InitTimer(const char* pname, int interval_sec = 60);
    virtual void StartTimer();
    virtual void StopTimer();
    virtual void SetInterval(int interval_msec);

    // Thread method
    virtual void StartThread();
    virtual void StopThread();
    virtual void run();

    bool    IsStarted() { return started_; }
    string  GetId() { return id_; }
    string  GetType() { return type_; }
    string  GetName() { return name_; }
    string  GetOperState() { return oper_state_; }
    string  GetAdminState() { return admin_state_; }


protected:
    elTimer*    timer_;
    bool        started_;

    string      id_;
    string      type_;
    string      name_;
    string      oper_state_;
    string      admin_state_;
};

#endif // _elComponent_h_