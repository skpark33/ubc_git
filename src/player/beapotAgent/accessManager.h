#ifndef _accessManager_h_
#define _accessManager_h_

#include "actionManager.h"

class accessManager : public actionManager
{
public:
	accessManager(actionHandler* handler, int maxOrder=3, int expiredSec=2)
		:actionManager("MIRROR_ACCESS", handler,maxOrder,expiredSec) {};
	virtual ~accessManager() {};

	virtual int 	received(const char* beaconId, const char* scannerId,
						ciLong battery, ciLong rssi,
						ciLong xiro_x, ciLong xiro_y, ciLong xiro_z);

	virtual	void	timeExpired();

protected:

};

#endif // _accessManager_h_
