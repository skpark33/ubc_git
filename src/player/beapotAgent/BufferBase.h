/*! \class BufferBase
 *  Copyright ⓒ 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/10/06 11:01:00
 */

#ifndef PLAYER_BEAPOTAGENT_BUFFERBASE_H
#define	PLAYER_BEAPOTAGENT_BUFFERBASE_H

#include <ci/libBase/ciBaseType.h>

/// 버퍼 최소 사이즈
#define DEFAULT_BUF_SIZE            4096
/// 버퍼 최대 사이즈 16777216 byte
#define MAX_BUF_SIZE                4096*4096

/// 바이트 오더 플레그
#define HOST_BYTE_ORDER             0
#define NETWORK_BYTE_ORDER          1

class BufferBase
{
    public:
        BufferBase();
        BufferBase(int size);        
        virtual ~BufferBase();

        void                Init(bool alloc_init);
        char*               Get();
        char*               Get(unsigned int index_pos);
        int                 Length();
        int                 Size();
        
        bool                AppendDataLen(int length);
        int                 Resize(int size);
        
        void                SetByteOrder(int byte_order);
        int                 GetByteOrder();
        
        bool                Set(const char* data, int start_set_pos, int length);
        bool                Append(const char* data, int length);
        //int                 Copy(char* copy_packet);
        
    protected:                
        bool                Alloc_(int size);
        bool                ReAlloc_(int new_size);
        void                DeAlloc_();
        
        int                 Align4_(int len);
                
        unsigned int        buffer_len_;
        int                 alloc_size_;
        char*               buffer_;
        int                 byte_order_;
};

#endif	// PLAYER_BEAPOTAGENT_BUFFERBASE_H

