/** \class elGatewayRule
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elGatewayRule_h_
#define _elGatewayRule_h_

#include "elRule.h"

class elGatewayRule : public elRule
{
public:
    elGatewayRule(string strRuleKeyword = EL_KEYWORD_GATEWAY_RULE);
    virtual ~elGatewayRule(void);

    bool        Set(JsonObject* pGatewayObj, elRule* pParentRule, elRule* pComposedRule);
    elRuleIdMap GetChildRuleMap(string keyword = EL_KEYWORD_ROUTE_RULE);

public:
    string      gw_id;
    string      gw_type;
    string      gw_name;
    string      local_ip;

protected:
    bool        SetRouteRule_(JsonArray* pRouteRuleArr);

protected:
    JsonObject* p_gateway_rule_obj_;
    elRule*     p_top_rule_;
    elRule*     p_parent_rule_;

    JsonArray*  p_route_rule_arr_;
    elRuleIdMap route_rule_map_;

};

#endif // _elGatewayRule_h_