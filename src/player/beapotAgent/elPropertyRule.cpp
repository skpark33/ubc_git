#include "elPropertyRule.h"
#include "elEndPointRule.h"
#include "elRuleRepository.h"

ciSET_DEBUG(5, "elPropertyRule");

elPropertyRule::elPropertyRule(string strRuleKeyword)
    : p_property_obj_(NULL), p_binder_array_(NULL), p_parent_rule_(NULL), p_top_rule_(NULL), is_ep_db(false)
{
    rule_keyword = strRuleKeyword;
}

elPropertyRule::~elPropertyRule(void)
{
}

bool elPropertyRule::Set(JsonObject* pPropertyObj, elRule* pParentRule, elRule* pComposedRule)
{
    ciDEBUG(5, ("Set()"));

    JsonValue*  temp_value = NULL;

    if (!pPropertyObj) {
        ciERROR((""));
        return false;
    }
    if (!pPropertyObj->isObject()) {
        ciERROR((""));
        return false;
    }
    p_property_obj_ = pPropertyObj;
    p_parent_rule_ = pParentRule;
    p_top_rule_ = pComposedRule;

    id = property_id = p_property_obj_->getString("PropertyID");
    if (property_id == "") {
        ciERROR((""));
        return false;
    }
    type = property_type = p_property_obj_->getString("PropertyType");
    if (property_type == "") {
        ciERROR((""));
        return false;
    }
    name = property_name = p_property_obj_->getString("PropertyName");
    parent_id = (pParentRule == NULL) ? "" : pParentRule->id;

    ciDEBUG(5, ("Set PropertyRule(%s, %s, %s)", property_id.c_str(), property_type.c_str(), property_name.c_str()));
    //oper_state = p_property_obj_->getString("OperState");
    //if (oper_state == "") {
    //    oper_state = "0";
    //}
    //admin_state = p_property_obj_->getString("AdminState");
    //if (admin_state == "") {
    //    admin_state = "0";
    //}

    data_source = p_property_obj_->getString("DataSource");
    if (data_source == "") {
        ciERROR((""));
        return false;
    }

    temp_value = p_property_obj_->get("Binder");
    if (!temp_value) {
        ciERROR(("Failed! JsonValue is NULL "));
        return false;
    } else if (!temp_value->isArray()) {
        ciERROR(("Failed! Set:p_property_obj_ is not JsonArray "));
        return false;
    }

    p_binder_array_ = (JsonArray*) temp_value;

    Stop_();
    Start_();

    // Add to Top Rule!
    p_top_rule_->Add(property_id, this);

    return true;
}

string elPropertyRule::BindProperty(string query)
{
    ciDEBUG(5, ("BindProperty Start(%s)", query.c_str()));

    if (query == "") {
        return query;
    }

    // 매번 실시간 값을 바인딩
    // 2015-12-16   ELiga.cfg 파일을 다시 읽어야 하기 때문에 현재는 안된다.
    //              elRuleRepository가 Timer로 매번 읽는 방식으로 향후 업그레이드한다.
    if (!SetPropertyBinder_(p_binder_array_)) {
        return query;
    }

    size_t pos;
    string findString("");
    string replaceString("");

    map<string, elPropertyBinder>::iterator itr;
    for (itr = prop_bind_map.begin(); itr != prop_bind_map.end(); itr++) {
        findString = itr->second.bind;
        replaceString = itr->second.value;
        // #{value} 형식 확인
        if (findString[0] == '#' && findString[1] == '{' && findString[findString.size()-1] == '}') 
        {
            while ((pos = query.find(findString)) != string::npos) {
                // Property value와 같은 건은 다 바꾼다. 
                // replaceString이 길어 query문장을 덮어쓰지 않도록 erase, insert
                query.erase(pos, findString.length());
                query.insert(pos,replaceString);
            }
        }
    }   
    ciDEBUG(5, ("Success! BindProperty : %s", query.c_str() ));
    return query;
}

string elPropertyRule::GlobalBindProperty(string query)
{
    ciDEBUG(5, ("GlobalBindProperty Start(%s)", query.c_str()));

    if (query == "") {
        return query;
    }

    // 매번 실시간 값을 바인딩
    // 2015-12-16   ELiga.cfg 파일을 다시 읽어야 하기 때문에 현재는 안된다.
    //              elRuleRepository가 Timer로 매번 읽는 방식으로 향후 업그레이드한다.
    if (!SetGlobalPropertyBinder_(p_binder_array_)) {
        //return string("");
        return query;
    }

    size_t pos;
    string findString("");
    string replaceString("");

    map<string, elPropertyBinder>::iterator itr;
    for (itr = prop_bind_map.begin(); itr != prop_bind_map.end(); itr++) {
        findString = itr->second.bind;
        replaceString = itr->second.value;
        // #{value} 형식 확인
        if (findString[0] == '#' && findString[1] == '{' && findString[findString.size()-1] == '}') 
        {
            while ((pos = query.find(findString)) != string::npos) {
                // Property value와 같은 건은 다 바꾼다. 
                // replaceString이 길어 query문장을 덮어쓰지 않도록 erase, insert
                query.erase(pos, findString.length());
                query.insert(pos,replaceString);
            }
        }
    }

    ciDEBUG(5, ("Success! GlobalBindProperty : %s", query.c_str() ));
    return query;
}


bool elPropertyRule::Start_()
{
    ciDEBUG(5, ("Start_()"));

    // 향후 구현사항 : DB가 아니더라도 모두 EndPointRule을 참고해서 얻을수 있도록 코딩
    elRuleRepository* p_rule_repository_ = elRuleRepository::GetInstance();
    elEndPointRule* p_endpoint_rule_ = (elEndPointRule*) p_rule_repository_->GetEligaRule(data_source);
    if (!p_endpoint_rule_) {
        ciWARN(("Get EndPointRule(%s)", data_source.c_str()));
        return false;
    }

    //#include <common/libCommon/utvMacro.h>
    //utvUtil::getInstance()->getHostId()
    //실행시,  +host <hostId> 파라메터가 있으면 그걸 가져오고,
    //없으면 computer name 을 리턴합니다.
    ep_type_ = p_endpoint_rule_->ep_type; // MYSQL
    host_ = p_endpoint_rule_->host;
    port_ = p_endpoint_rule_->port;
    name_ = p_endpoint_rule_->group;
    user_ = p_endpoint_rule_->user;
    pass_ = p_endpoint_rule_->pass;

    if (ep_type_ == "MYSQL") {
        is_ep_db = true;
        db_session_.StartDB(host_, port_, name_, user_, pass_);
        is_ep_db = db_session_.IsConnected();
        return true;
    } else {
        is_ep_db = false;
    }
    return false;
}

void elPropertyRule::Stop_()
{
    ciDEBUG(5, ("Stop_()"));
    db_session_.StopDB();
}

bool  elPropertyRule::SetPropertyBinder_(JsonArray* pArray)
{
    ciDEBUG(5, ("SetPropertyBinder_()"));
    //ciGuard guard(lock_);

    elRuleRepository* p_rule_repository_ = elRuleRepository::GetInstance();
    if (!pArray) {
        ciERROR(("Binder is NULL! SetPropertyBinder_"));
        return false;
    }

    JsonObject* pObj = NULL;
    string str_cnt = "0";

    for (int i = 0; i < pArray->getCount(); i++) {
        pObj = (JsonObject*) pArray->get(i);
        if (pObj == NULL) {
            ciERROR(("Failed! pArray[%d] is NULL ", i));
            return false;
        }

        elPropertyBinder binder;
        binder.name     = pObj->getString("Name");
        binder.bind    = pObj->getString("Bind");
        binder.select   = pObj->getString("Select");
        binder.value   = pObj->getString("Value");

        if (property_type == "GLOBAL_PROPERTY") {
            binder.value   = p_rule_repository_->GetGlobalValue(binder.name);
        } else {
            //binder.value
            if (is_ep_db) {
                string bindedSelect = p_rule_repository_->SetGlobalPropertyBind(binder.select);
                ciDEBUG(5, ("SetGlobalPropertyBind() End!!!"));

                // 2016-01-20 add bindedSelect != "" : select 항목이 없는 경우는 쿼리하지 않는다.
                if (bindedSelect != "" && db_session_.Select(bindedSelect)) {
                    elRecordList record_list;
                    if (db_session_.GetRecordList(record_list)) {
						ciDEBUG(5, ("SetGlobalPropertyBind() record_list:%d", record_list.size()));
                        if (property_type == "DB_SELECT_ONE_VALUE") {
							ciDEBUG(5, ("SetGlobalPropertyBind() DB_SELECT_ONE_VALUE 1 !!!"));
                            if (record_list[0].size() == 2) {
                                binder.value = record_list[0][0].value;
                                str_cnt = record_list[0][1].value;
                                if(str_cnt == "0") {
                                    binder.value   = "1970-01-01 00:00:00";
                                }
								ciDEBUG(5, ("SetGlobalPropertyBind() DB_SELECT_ONE_VALUE 2!!!"));
                            }
							ciDEBUG(5, ("SetGlobalPropertyBind() DB_SELECT_ONE_VALUE 3!!!"));
                        } else {
                            //not exist
                        }
					} else {
						ciERROR(("GetRecordList failed"));
					}
                } else {
                    ciWARN(("SetPropertyBinder_ bindedSelect do not run !!! (%s)  ", bindedSelect.c_str()));
                    return false;
                }
            }
        }
        //prop_bind_map.insert(map<string, elPropertyBinder>::value_type(binder.name, binder));
        ciDEBUG(5, ("SetPropertyBinder_() name(%s) bind(%s) value(%s) select(%s)", binder.name.c_str(), binder.bind.c_str(), binder.value.c_str(), binder.select.c_str()));
        prop_bind_map[binder.name] = binder;
    }
    return true;
}


bool  elPropertyRule::SetGlobalPropertyBinder_(JsonArray* pArray)
{
    ciDEBUG(5, ("SetGlobalPropertyBinder_()"));

    elRuleRepository* p_rule_repository_ = elRuleRepository::GetInstance();

    if (!pArray) {
        ciERROR(("Binder is NULL! SetGlobalPropertyBinder_"));
        return false;
    }

    JsonObject* pObj = NULL;
    string str_cnt = "0";

    for (int i = 0; i < pArray->getCount(); i++) {
        pObj = (JsonObject*) pArray->get(i);
        if (pObj == NULL) {
            ciERROR(("Failed! pArray[%d] is NULL ", i));
            return false;
        }

        elPropertyBinder binder;
        binder.name     = pObj->getString("Name");
        binder.bind    = pObj->getString("Bind");
        binder.select   = pObj->getString("Select");
        binder.value   = pObj->getString("Value");

        if (property_type == "GLOBAL_PROPERTY") {
            binder.value   = p_rule_repository_->GetGlobalValue(binder.name);
        } else {
            ciERROR(("SetGlobalPropertyBinder_ can using only GLOBAL"));
            return false;
        }

        // 2016-01-20 add binder.name != ""
        if (binder.name != "") {
            prop_bind_map.insert(map<string, elPropertyBinder>::value_type(binder.name, binder));
        }
        ciDEBUG(5, ("SetGlobalPropertyBinder_() name(%s) bind(%s) value(%s) select(%s)", binder.name.c_str(), binder.bind.c_str(), binder.value.c_str(), binder.select.c_str()));
    }

    return true;
}
