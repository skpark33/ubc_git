#include "BeaconSimulator.h"
#include "ChannelDefine.h"
#include "JsonBase.h"

#include <ci/libDebug/ciDebug.h>
#include <ci/libBase/ciStringTokenizer.h>

#include <ctime>

ciSET_DEBUG(5, "BeaconSimulator");

//////////////////////////////////////////////////////////////////////
/// ciMsecTimer
void ciMsecTimer::run() {
    while(_started) {
        ::Sleep(_interval_msec);
        _counter++;
        if(p_object_) {
            p_object_->processExpired(_name, _counter, _interval_msec);
        }
    }
}

//////////////////////////////////////////////////////////////////////
/// BeaconSimulator

BeaconSimulator::BeaconSimulator()
{
    ciDEBUG(5, ("BeaconSimulator() "));

    started_ = false;
    sample_send_on_ = false;
    action_type_ = "";
    p_action_manager_ = NULL;
        
    initTimer("BeaconSimulatorTimer", 500);
        
}

BeaconSimulator::~BeaconSimulator(void)
{
    ciDEBUG(5, ("~BeaconSimulator() "));    
}

void BeaconSimulator::processExpired(ciString name, int counter, int interval_msec)
{
    ciDEBUG(5, ("processExpired(%s, counter<%d>, interval<%d>) ", name.c_str(), counter, interval_msec));
    sample_send_on_ = true;
}

void BeaconSimulator::Start()
{
    ciDEBUG(5, ("Start() "));

    SampleRead_();

    ciDEBUG(1, ("before BeaconSimulator thread start()"));    
    started_ = true;
    start();
    ciDEBUG(1, ("after BeaconSimulator thread start()"));

    setInterval(500); // 500ms
    ciDEBUG(1, ("before BeaconSimulator timer start()"));    
    startTimer();
    ciDEBUG(1, ("after BeaconSimulator timer start()"));
}

void BeaconSimulator::Stop()
{
    ciDEBUG(1, ("Stop() "));

    stopTimer();
	if(started_) {
		started_ = false;
		stop(); 
	}
}

void BeaconSimulator::run()
{
    ciDEBUG(5, ("run... "));
    
    string str_msg("");
    int n_data_idx = 0;    

    // sample beacon data
    beaconInfo beacon_scan_msg1;
    beaconInfo beacon_scan_msg2;
    beaconInfo beacon_scan_msg3;

    beacon_scan_msg1.beaconId  = g_pickup_D03972CDDB0A_data[0].beacon_id;
    beacon_scan_msg1.scannerId = g_pickup_D03972CDDB0A_data[0].scanner_id;
    beacon_scan_msg1.battery   = g_pickup_D03972CDDB0A_data[0].battery;
    beacon_scan_msg1.rssi      = g_pickup_D03972CDDB0A_data[0].rssi;
    beacon_scan_msg1.xiro_x = 0; beacon_scan_msg1.xiro_y = 0; beacon_scan_msg1.xiro_z = 0;

    beacon_scan_msg2.beaconId  = g_pickup_D03972CDDA78_data[0].beacon_id;
    beacon_scan_msg2.scannerId = g_pickup_D03972CDDA78_data[0].scanner_id;
    beacon_scan_msg2.battery   = g_pickup_D03972CDDA78_data[0].battery;
    beacon_scan_msg2.rssi      = g_pickup_D03972CDDA78_data[0].rssi;
    beacon_scan_msg2.xiro_x = 0; beacon_scan_msg2.xiro_y = 0; beacon_scan_msg2.xiro_z = 0;

    beacon_scan_msg3.beaconId  = g_pickup_D03972CDD962_data[0].beacon_id;
    beacon_scan_msg3.scannerId = g_pickup_D03972CDD962_data[0].scanner_id;
    beacon_scan_msg3.battery   = g_pickup_D03972CDD962_data[0].battery;
    beacon_scan_msg3.rssi      = g_pickup_D03972CDD962_data[0].rssi;
    beacon_scan_msg3.xiro_x = 0; beacon_scan_msg3.xiro_y = 0; beacon_scan_msg3.xiro_z = 0;

	ciULong access_var = 0L;

    while(started_) {
        if (sample_send_on_)
        {
			if (action_type_ == "PICKUP") {
	            SendToActionManager(beacon_scan_msg1);            
	            SendToActionManager(beacon_scan_msg2);            
	            SendToActionManager(beacon_scan_msg3);            
			}
            sample_send_on_ = false;
        } else {
            if (n_data_idx > 5999) {
                n_data_idx = 0;
            } else {
                if (action_type_ == "PICKUP") {
                    // D0:39:72:CD:DB:0A - 500ms 사이의 최대 Peak 값 계산
                    ciLong temp_x = (ciLong) (g_pickup_D03972CDDB0A_data[n_data_idx].accel_x * 1000.00);
                    ciLong temp_y = (ciLong) (g_pickup_D03972CDDB0A_data[n_data_idx].accel_y * 1000.00);
                    ciLong temp_z = (ciLong) (g_pickup_D03972CDDB0A_data[n_data_idx].accel_z * 1000.00);
                    if (beacon_scan_msg1.xiro_x < temp_x ) {                        
                        beacon_scan_msg1.xiro_x = temp_x;
                        ciDEBUG(1, ("xiro_x[%ld]", beacon_scan_msg1.xiro_x));
                    }
                    if (beacon_scan_msg1.xiro_y < temp_y ) {
                        beacon_scan_msg1.xiro_y = temp_y;
                        ciDEBUG(1, ("xiro_y[%ld]", beacon_scan_msg1.xiro_y));
                    }
                    if (beacon_scan_msg1.xiro_z < temp_z ) {
                        beacon_scan_msg1.xiro_z = temp_z;
                        ciDEBUG(1, ("xiro_z[%ld]", beacon_scan_msg1.xiro_z));
                    }

                    

                    // D0:39:72:CD:DA:78 - 500ms 사이의 Peak 값 계산
                    temp_x = (ciLong) (g_pickup_D03972CDDA78_data[n_data_idx].accel_x * 1000.00);
                    temp_y = (ciLong) (g_pickup_D03972CDDA78_data[n_data_idx].accel_y * 1000.00);
                    temp_z = (ciLong) (g_pickup_D03972CDDA78_data[n_data_idx].accel_z * 1000.00);
                    if (beacon_scan_msg2.xiro_x < temp_x ) beacon_scan_msg2.xiro_x = temp_x;
                    if (beacon_scan_msg2.xiro_y < temp_y ) beacon_scan_msg2.xiro_y = temp_y;
                    if (beacon_scan_msg2.xiro_z < temp_z ) beacon_scan_msg2.xiro_z = temp_z;

                    // D0:39:72:CD:D9:62 - 500ms 사이의 Peak 값 계산
                    temp_x = (ciLong) (g_pickup_D03972CDD962_data[n_data_idx].accel_x * 1000.00);
                    temp_y = (ciLong) (g_pickup_D03972CDD962_data[n_data_idx].accel_y * 1000.00);
                    temp_z = (ciLong) (g_pickup_D03972CDD962_data[n_data_idx].accel_z * 1000.00);
                    if (beacon_scan_msg3.xiro_x < temp_x ) beacon_scan_msg3.xiro_x = temp_x;
                    if (beacon_scan_msg3.xiro_y < temp_y ) beacon_scan_msg3.xiro_y = temp_y;
                    if (beacon_scan_msg3.xiro_z < temp_z ) beacon_scan_msg3.xiro_z = temp_z;

                } else if (action_type_ == "MIRROR_ACCESS") {
					if(access_var % 3 == 0) {
			            SendToActionManager(beacon_scan_msg1);            
			            SendToActionManager(beacon_scan_msg2);            
					}else if(access_var %3 == 1) {
			            SendToActionManager(beacon_scan_msg2);            
					}else if(access_var % 3 == 2) {
			            SendToActionManager(beacon_scan_msg2);            
			            SendToActionManager(beacon_scan_msg3);            					
					}
					if(access_var % 4 == 0) {
			            SendToActionManager(beacon_scan_msg3);            
			            SendToActionManager(beacon_scan_msg1);            
			            SendToActionManager(beacon_scan_msg2); 
                        ciDEBUG(1,(" ZZZ ACCESS beacon"));	
					}else if(access_var % 4 == 1) {
			            SendToActionManager(beacon_scan_msg1);            
			            SendToActionManager(beacon_scan_msg2);            
					}else if(access_var % 4 == 2) {
			            SendToActionManager(beacon_scan_msg2);            
					}

				} else {
                }
                n_data_idx++;
            }
			if (action_type_ == "MIRROR_ACCESS") {
				time_t now = time(NULL);//
				srand((int) now);
				int random_number = (rand() % 10);
				ciDEBUG(1,(" random_number=%d sec", random_number));			
				::Sleep((random_number+1)*1000);
				access_var = random_number;
			} else if (action_type_ == "PICKUP") {
				::Sleep(5); // 1 msec loof
			}
        }
    }
    ciDEBUG(5, ("run return #######################... "));
}

void BeaconSimulator::RegisterActionManager(const char* action_type, actionManager* p_action_manager)
{
    ciDEBUG(5, ("RegisterActionManager(%s) ", action_type));
    action_type_ = action_type;
    p_action_manager_ = p_action_manager;
}

void BeaconSimulator::SendToActionManager(beaconInfo& beacon_scan_msg)
{
    ciDEBUG2(1, ("SendToActionManager(BEACON_ID<%s>, SCANNER_ID<%s>, BATTERY<%d>, RSSI<%d>, XIRO<%ld, %ld, %ld)> ",  
                beacon_scan_msg.beaconId.c_str(), beacon_scan_msg.scannerId.c_str(),
		        beacon_scan_msg.battery, beacon_scan_msg.rssi,
		        beacon_scan_msg.xiro_x, beacon_scan_msg.xiro_y, beacon_scan_msg.xiro_z
            ));

    int ret = 0;

    if(p_action_manager_) {        
        ret = p_action_manager_->received(beacon_scan_msg.beaconId.c_str(), beacon_scan_msg.scannerId.c_str(),
		                        beacon_scan_msg.battery, beacon_scan_msg.rssi,
		                        beacon_scan_msg.xiro_x, beacon_scan_msg.xiro_y, beacon_scan_msg.xiro_z);
    }

    beacon_scan_msg.xiro_x = 0;
    beacon_scan_msg.xiro_y = 0;
    beacon_scan_msg.xiro_z = 0;
}

bool BeaconSimulator::FileRead(std::string strFileFullName, ciStringList& outList )
{
    bool ret_value = true;

    char szBuffer[2048];
    unsigned int uReaded;
    FILE *fin;
    
    if ((fin = fopen(strFileFullName.c_str(), "rb")) == NULL) return false;

	while(fgets(szBuffer,2048,fin)){
        outList.push_back(szBuffer);
        memset(szBuffer, 0x00, sizeof (szBuffer));
    }

    fclose(fin);

    return true;
}


/*
bool BeaconSimulator::FileRead(std::string strFileFullName, std::string& strBuffer, bool is_strcat)
{
    bool ret_value = true;

    char szBuffer[2048];
    unsigned int uReaded;
    FILE *fin;
    
    if (is_strcat == false) strBuffer = "";

    if ((fin = fopen(strFileFullName.c_str(), "rb")) == NULL) return false;

    do {
        //memset(szBuffer, 0x00, sizeof (szBuffer));
        uReaded = (unsigned int) fread(szBuffer, 1, sizeof (szBuffer) - 1, fin);
        if (uReaded > 0) {
            szBuffer[uReaded] = 0;
            strBuffer += szBuffer;
        }
        else break;
    } while (uReaded == (sizeof (szBuffer) - 1));

    fclose(fin);
}
*/
void BeaconSimulator::SampleRead_()
{
    ciDEBUG(5, ("SampleRead_(C:\\Project\\utv1\\bin8\\SampleBeaconData.dat) start"));

    ciStringList sampleList;
    //FileRead("C:\\Project\\utv1\\bin8\\SampleBeaconData1.dat", sampleList);
    FileRead("SampleBeaconData1.dat", sampleList);
	_push_back(sampleList, g_pickup_D03972CDDB0A_data);
	sampleList.clear();

    //FileRead("C:\\Project\\utv1\\bin8\\SampleBeaconData2.dat", sampleList);
    FileRead("SampleBeaconData2.dat", sampleList);
	_push_back(sampleList, g_pickup_D03972CDDA78_data);
	sampleList.clear();

    //FileRead("C:\\Project\\utv1\\bin8\\SampleBeaconData3.dat", sampleList);
    FileRead("SampleBeaconData3.dat", sampleList);
	_push_back(sampleList, g_pickup_D03972CDD962_data);
	sampleList.clear();
}

void BeaconSimulator::_push_back(ciStringList& inList, vector<BEACON_ACCEL_DATA>& outVector)
{
	ciStringList::iterator itr;
	for(itr=inList.begin();itr!=inList.end();itr++){
		ciString str_record = (*itr);
		ciStringUtil::safeRemoveSpace(str_record);
        ciStringTokenizer field_token(str_record, ",");
        int j = 0;
        BEACON_ACCEL_DATA beacon_data;
        while (field_token.hasMoreTokens()) {
            ciString str_field = field_token.nextToken();
			if(j%7==0){
				beacon_data.beacon_id  = str_field;
			}else if(j%7==1){
				beacon_data.scanner_id = str_field;
			}else if(j%7==2){
				beacon_data.battery    = atoi(str_field.c_str());
			}else if(j%7==3){
				beacon_data.rssi       = atoi(str_field.c_str());
			}else if(j%7==4){
				beacon_data.accel_x    = atof(str_field.c_str());
			}else if(j%7==5){
				beacon_data.accel_y    = atof(str_field.c_str());
			}else if(j%7==6){
				beacon_data.accel_z    = atof(str_field.c_str());
                ciDEBUG(5, ("DATA : %f,%f,%f",beacon_data.accel_x,beacon_data.accel_y,beacon_data.accel_z)); 
                outVector.push_back(beacon_data);
			}
            j++;
        }
	}			
}
/*
void BeaconSimulator::SampleRead_()	
{
    string str_beacon_buf("");
    FileRead("C:\\Project\\utv1\\bin8\\SampleBeaconData.dat", str_beacon_buf, false);

    ciStringTokenizer record_token(str_beacon_buf, "\r\n");    
    string str_record("");
    string str_field("");
    
    
    string str_beacon_mac("D0:39:72:CD:DB:0A");
    int i = 0;
    int rec_cnt = record_token.countTokens();
    while(record_token.hasMoreTokens()) {
        
        str_record = record_token.nextToken();
        ciDEBUG(8, ("RECORD[%d][%s] ", i, str_record.c_str() ));

        ciStringUtil::safeRemoveSpace(str_record);
        ciStringTokenizer field_token(str_record, ",");
        ciStringVector str_vec_field;
        int j = 0;
        while (field_token.hasMoreTokens()) {
            str_field = field_token.nextToken();
            str_vec_field.push_back(str_field);
            ciDEBUG(8, ("FIELD[%d][%s] ", j, str_field.c_str() ));

            j++;
        }

        if (str_vec_field.size() < 7) {
            ciDEBUG(8, ("NULL Parse Record[%d][%s] vec_size[%d]", i, str_record.c_str(), str_vec_field.size() ));
            continue;
        }

        BEACON_ACCEL_DATA beacon_data;
        beacon_data.beacon_id  = str_vec_field[0];
        beacon_data.scanner_id = str_vec_field[1];
        beacon_data.battery    = atoi(str_vec_field[2].c_str());
        beacon_data.rssi       = atoi(str_vec_field[3].c_str());
        beacon_data.accel_x    = atof(str_vec_field[4].c_str());
        beacon_data.accel_y    = atof(str_vec_field[5].c_str());
        beacon_data.accel_z    = atof(str_vec_field[6].c_str());

        if        ( beacon_data.beacon_id == "D0:39:72:CD:DB:0A" ) {
            g_pickup_D03972CDDB0A_data.push_back(beacon_data);
        } else if ( beacon_data.beacon_id == "D0:39:72:CD:DA:78" ) {
            g_pickup_D03972CDDA78_data.push_back(beacon_data);
        } else if ( beacon_data.beacon_id == "D0:39:72:CD:D9:62" ) {
            g_pickup_D03972CDD962_data.push_back(beacon_data);
        } else {
            ciDEBUG(8, ("ERROR Parse Record[%d][%s] ", i, str_record.c_str() ));
        }

        ciDEBUG(8, ("Parse Record[%d] [%s, %s, %d, %d, %lf, %lf, %lf] ", i
                    , beacon_data.beacon_id.c_str(), beacon_data.scanner_id.c_str(), beacon_data.battery, beacon_data.rssi
                    , beacon_data.accel_x, beacon_data.accel_y, beacon_data.accel_z));
        i++;
    }

    ciDEBUG(5, ("SampleRead_() end  BEACON Data record[%d][%d]", rec_cnt, i));
}
	*/

/**
    //{
    //    "XIRO_BEACON": {
    //        "ACTION_TYPE": "PING",
    //        "XIRO": [
    //            "54300",
    //            "32674",
    //            "58568"
    //        ],
    //        "SCANNERID": "scanner105",
    //        "BEACONID": "00:18:9A:25:E1:A8",
    //        "RSSI": "X12X5741X",
    //        "BATTERY": "35",
    //        "CTIME": "2015-09-25 15:03:55"
    //    }
    //}
    // 
**/

string BeaconSimulator::MakeBeaconPacket(string scanner_id, string beacon_id, int rssi, int battery, int xiro_x, int xiro_y, int xiro_z )
{
    // sample msg : "~SIMLJSON00000000004DATA"
    string str_msg("~SIMLJSON0");
    string str_json("");
    char sz_no_buf[33];
    
    string str_rssi    = itoa(rssi, sz_no_buf, 10);
    string str_battery = itoa(battery, sz_no_buf, 10);
    string str_xiro_x  = itoa(xiro_x, sz_no_buf, 10);
    string str_xiro_y  = itoa(xiro_y, sz_no_buf, 10);
    string str_xiro_z  = itoa(xiro_z, sz_no_buf, 10);
    string str_ctime("");

    time_t	m_time;
    struct tm	m_tm;
	char	buf[20];

    time(&m_time);
	tm* local_time = localtime(&m_time);
	if(local_time == 0){
		m_time = MAXLONG;
		local_time = localtime(&m_time);
	}
	m_tm = *local_time;

    sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d",
			m_tm.tm_year + 1900, m_tm.tm_mon + 1, m_tm.tm_mday,
			m_tm.tm_hour, m_tm.tm_min, m_tm.tm_sec);
	str_ctime = buf;


    char sz_buf[11];
    memset(sz_buf, 0x0, 11);

    // JSON CREATE
    JsonObject* root        = new JsonObject();
    JsonObject* xiro_beacon = new JsonObject();
    JsonArray*  xiro_xyz_array = new JsonArray();

    root->add("XIRO_BEACON", xiro_beacon);
    xiro_beacon->add("ACTION_TYPE", "PING");

    xiro_xyz_array->add(str_xiro_x.c_str());
    xiro_xyz_array->add(str_xiro_y.c_str());
    xiro_xyz_array->add(str_xiro_z.c_str());
    xiro_beacon->add("XIRO", xiro_xyz_array);

    xiro_beacon->add("SCANNERID", scanner_id.c_str());
    xiro_beacon->add("BEACONID", beacon_id.c_str());
    xiro_beacon->add("RSSI", str_rssi.c_str());
    xiro_beacon->add("BATTERY", str_battery.c_str());
    xiro_beacon->add("CTIME", str_ctime.c_str());

    str_json = root->toString();
    //

    // Length write
    sprintf(sz_buf, "%010d", (int) str_json.size());

    str_msg += ciString(sz_buf);
    str_msg += str_json;

    ciDEBUG(1,("MakeBeaconPacket PACKET[%s]", str_msg.c_str() ));
    return str_msg;

}
