#ifndef _wideManager_h_
#define _wideManager_h_

#include "pickupManager.h"

class wideManager : public pickupManager
{
public:
	wideManager(actionHandler* handler, int threshold = 5, int maxOrder=3, int expiredSec=2);
    virtual ~wideManager();

protected:

};

#endif // _wideManager_h_