#include "elEndPointRule.h"

ciSET_DEBUG(5, "elEndPointRule");

elEndPointRule::elEndPointRule(string strRuleKeyword) 
    : p_endpoint_obj_(NULL), p_top_rule_(NULL), p_parent_rule_(NULL)
{
    ciDEBUG(5, ("elEndPointRule()"));
    rule_keyword = strRuleKeyword;
}

elEndPointRule::~elEndPointRule(void)
{
    ciDEBUG(5, ("~elEndPointRule()"));
}


bool elEndPointRule::Set(JsonObject* pEndPointObj, elRule* pParentRule, elRule* pComposedRule)
{
    ciDEBUG(5, ("Set()"));

    if (!pEndPointObj) {
        return false;
    }
    if (!pEndPointObj->isObject()) {
        p_endpoint_obj_ = NULL;
        return false;
    }
    p_endpoint_obj_ = pEndPointObj;
    p_parent_rule_ = pParentRule;
    p_top_rule_ = pComposedRule;    

    id   = ep_id   = pEndPointObj->getString("EndPointID");
//    parent_id      = parentRuleId;
    type = ep_type = pEndPointObj->getString("EndPointType");
    name = ep_name = pEndPointObj->getString("EndPointName");
    parent_id = (pParentRule == NULL) ? "" : pParentRule->id;

    oper_state     = pEndPointObj->getString("OperState");
    admin_state    = pEndPointObj->getString("AdminState");

    group          = pEndPointObj->getString("Group");
    user           = pEndPointObj->getString("User");
    pass           = pEndPointObj->getString("Pass");
    host           = pEndPointObj->getString("Host");
    port           = ::atol(pEndPointObj->getString("Port"));
    url            = pEndPointObj->getString("URL");
    protocol       = pEndPointObj->getString("Protocol");
    return_type    = pEndPointObj->getString("ReturnType");

    // set ParentID
    if (!pParentRule) {
        ciERROR(("Failed! Not exist parent rule "));
        return true;
    }
    parent_id = pComposedRule->id;

    // Add to ComposedRule
    if (!pComposedRule) {
        ciERROR(("Failed! Not exist composed rule "));
        return false;
    }
    pComposedRule->Add(ep_id, this);

    ciDEBUG(5, ("Set() Complete"));
    return true;
}