/** \class elTimer
 *  Copyright ⓒ 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief 추후 여러 타이머 종류를 등록할 수 있도록 수정한다.
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elTimer_h_
#define _elTimer_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciThread.h>

class elComponent;

class elTimer : public ciThread
{
public:
    elTimer(const char* pTimerName, elComponent* polling_component);
    virtual ~elTimer(void);

    void StartTimer();
    void StopTimer();

    void SetInterval(int interval_sec, int interval_msec=0);
    virtual void run();

protected:

	int 			counter_;
    int 			interval_sec_;
    int 			interval_msec_;
	bool    		started_;
	string		    name_;

    elComponent*    polling_component_;
};

#endif //_elTimer_h_