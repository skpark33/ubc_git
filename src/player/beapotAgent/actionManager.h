#ifndef _actionManager_h_
#define _actionManager_h_

#include <ci/libThread/ciSyncUtil.h>
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libTimer/ciWinTimer.h>

class actionManager;

class actionHandler {
public:
	actionHandler() : _actionManager(0) {}
	virtual ~actionHandler() {}

	virtual void processEvent(ciString& actionType, ciStringList& beaconIdList,
								int additionalState) = 0;

	void	setActionManager(actionManager* p) { _actionManager = p; }
protected:
	actionManager*			 _actionManager;

};

class beaconInfo {
public:
	beaconInfo() :rssi(0),
		xiro_x(0), xiro_y(0), xiro_z(0), battery(0), 
		order(0), state(0), no_move_count(0) {};
	beaconInfo(const char* id, ciLong x,ciLong y,ciLong z) :rssi(0),
		xiro_x(x), xiro_y(y), xiro_z(z), battery(0), 
		order(0), state(0), no_move_count(0) { beaconId = id; };
	virtual ~beaconInfo() {};

	ciString beaconId;
	ciString scannerId;
	ciLong	 battery;
	ciLong	 rssi;
	ciLong	 xiro_x;
	ciLong	 xiro_y;
	ciLong	 xiro_z;
	ciTime	lastUpdateTime;
	time_t	order;
	int		state;
	int		no_move_count;
};

typedef multimap<ciString, ciShort>	dummyMap;
typedef map<ciString, beaconInfo*>	beaconInfoMap;
typedef multimap<time_t, beaconInfo*>	beaconInfoSortedMap;

class axisEvalData {
public:
	axisEvalData() :x_count(0), y_count(0), z_count(0) {};
	virtual ~axisEvalData() {};

	int		 x_count;
	int		 y_count;
	int		 z_count;
};
typedef map<ciString, axisEvalData*>	axisEvalDataMap;

#define EVAL_MAX_COUNT 2
#define MAX_TIME_GAP 5

class DeltaEle {
public:
	DeltaEle() :overCount(0), prev_value(0), updateTime(0) {}
	virtual ~DeltaEle() {}

	long		prev_value;
	int			overCount;
	time_t		updateTime;
};
typedef map<ciString, DeltaEle*>	DeltaMap;

enum 
{
	AXIS_X = 1,
	AXIS_Y,
	AXIS_Z
};



class actionTimer;

class actionManager {
public:	
	actionManager(const char* actionType, actionHandler* handler, int maxOrder=3, int expiredSec=2);
	virtual ~actionManager();

	void	start(int interval=1);
	ciBoolean clear();

	virtual int 	received(const char* beaconId, const char* scannerId,
						ciLong battery, ciLong rssi,
						ciLong xiro_x, ciLong xiro_y, ciLong xiro_z) = 0;

	virtual	void	timeExpired() = 0;

	virtual void	loadIni();

	void			setPrevious(const char* p);
	ciBoolean 		isChanged(ciString& actionString);
protected:

	long _getDelta(const char* beaconId,ciLong x,ciLong y,ciLong z,int threshold);
	
	
	virtual int _calc123(beaconInfoSortedMap& sortedMap, int state);

	void		_axisEval_in(const char* beaconId,ciLong x,ciLong y,ciLong z);
	int			_axisEval_out(const char* beaconId);
	ciBoolean	_isMoving(ciLong x,ciLong y,ciLong z);
	ciBoolean	_isMoving_no_nomalize(const char*  beaconId, ciLong x,ciLong y,ciLong z);

	actionHandler*			_handler;
	ciMutex					_handlerLock;

	beaconInfoMap			_map;
	ciMutex					_mapLock;
	ciString				_actionType;
	ciShort					_maxOrder;
	ciShort					_expiredSec;
	actionTimer*			_timer;

	axisEvalDataMap			_axisEvalMap;

	ciString				_preActionString;
	ciMutex					_preActionStringLock;

	DeltaMap				_deltaMap;
	ciLong					_stdDev;

	beaconInfoMap			_normalizeFailMap;
};

class actionTimer : public ciWinPollable
{
public:
	actionTimer(actionManager* manager) : _manager(manager) {};
	virtual ~actionTimer() {};

	virtual void processExpired(ciString name, int counter, int interval)
	{
		if(counter == 1) return;
		if(_manager) {
			_manager->timeExpired();
		}
	}

protected:
	actionManager*	_manager;
};


#endif // _actionManager_h_
