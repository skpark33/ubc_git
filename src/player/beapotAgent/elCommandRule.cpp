#include "elCommandRule.h"

ciSET_DEBUG(5, "elCommandRule");

elCommandRule::elCommandRule(int nCommandSeq, string strRuleKeyword)
    : p_command_obj_(NULL), p_parent_rule_(NULL), p_top_rule_(NULL)
{
    rule_keyword = strRuleKeyword;
}

elCommandRule::~elCommandRule(void)
{
}

bool elCommandRule::Set(JsonObject* pCommandObj, elRule* pParentRule, elRule* pComposedRule)
{
    ciDEBUG(5, ("Set()"));

    JsonValue*  temp_value = NULL;

    if (!pCommandObj) {
        return false;
    }
    if (!pCommandObj->isObject()) {
        return false;
    }
    p_command_obj_ = pCommandObj;
    p_parent_rule_ = pParentRule;
    p_top_rule_ = pComposedRule;

    id = command_id = p_command_obj_->getString("CommandID");
    if (command_id == "") {
        return false;
    }
    type = command_type = p_command_obj_->getString("CommandType");
    if (command_type == "") {
        return false;
    }
    name = command_name = p_command_obj_->getString("CommandName");
    parent_id = (pParentRule == NULL) ? "" : pParentRule->id;

    oper_state = p_command_obj_->getString("OperState");
    if (oper_state == "") {
        oper_state = "0";
    }
    admin_state = p_command_obj_->getString("AdminState");
    if (admin_state == "") {
        admin_state = "0";
    }

    partner_command_id = p_command_obj_->getString("ParterCommandID");

    end_point = p_command_obj_->getString("EndPointID");
    if (end_point == "") {
        return false;
    }

    entity = p_command_obj_->getString("Entity");
    if (entity == "") {
        return false;
    }

    // FieldArr
    temp_value = p_command_obj_->get("FieldArr");
    if (!temp_value) {
        ciERROR(("Failed! FieldArr JsonValue is NULL "));
        return false;
    } else if (!temp_value->isArray()) {
        ciERROR(("Failed! Set:p_command_obj_ FieldArr is not JsonArray "));
        return false;
    }

    SetStringVector((JsonArray*) temp_value, field_arr_vec);
    temp_value = NULL;

    //FieldTypes field_type_vec
    temp_value = p_command_obj_->get("FieldTypes");
    if (!temp_value) {
        ciERROR(("Failed! FieldTypes JsonValue is NULL "));
        return false;
    } else if (!temp_value->isArray()) {
        ciERROR(("Failed!  Set:p_command_obj_ FieldTypes is not JsonArray "));
        return false;
    }

    SetStringVector((JsonArray*) temp_value, field_type_vec);
    temp_value = NULL;

    // Command
    command = p_command_obj_->getString("Command");
    if (command == "") {
        return false;
    }

    // PropertyID
    property_id = p_command_obj_->getString("PropertyID");

    // Add to Top Rule!
    p_top_rule_->Add(command_id, this);

    return true;
}

elRuleIdMap elCommandRule::GetChildRuleMap(string keyword)
{
    if (keyword == EL_KEYWORD_DEFAULT_RULE) {
        return child_rule_map_;
    }

    // default
    return child_rule_map_;
}
