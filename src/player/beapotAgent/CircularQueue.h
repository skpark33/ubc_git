#ifndef PLAYER_BEAPOTAGENT_CIRCULARQUEUE_H
#define	PLAYER_BEAPOTAGENT_CIRCULARQUEUE_H

#include <ci/libThread/ciSyncUtil.h>

#define QUEUE_NO_ERR            0
#define QUEUE_OVERFLOW_ERR      -1
#define QUEUE_UNDERFLOW_ERR     -2
#define QUEUE_EMPTY             -3


template <class T>
class CircularQueue 
{
    public:
        // initialize integer data members
        CircularQueue();
        CircularQueue(bool is_auto_del);
        CircularQueue(int max_qsize, bool is_auto_del);        
        ~CircularQueue();

        bool    push_back(const T& item);   // Full�̸� Push�ȵ�
        bool    front(T& item);             // ���� ��ġ�� item ����
        T       front();                    // ���� ��ġ�� item ����
        bool    pop_front();                // ���� ��ġ�� ���� ��ġ�� �ű�, ������ ����
        void    clear(void);

        int     size(void) const;
        bool    empty(void) const;
        bool    full(void) const;

        int     GetError(void) const;
        
    private:
        int             max_qsize_;
       
        unsigned int    front_no_, rear_no_, count_no_;
        //T               queue_list_[max_qsize_];
        T*              queue_list_; //max_qsize_

        bool            is_auto_del_;
        int             error_no_;
        
        ciMutex         queue_lock_;
};

// initialize queue front, rear, count
template <class T>
CircularQueue<T>::CircularQueue() : front_no_(0), rear_no_(0), count_no_(0) 
{
    is_auto_del_ = false;
    max_qsize_ = 4096;
    queue_list_ = new T[max_qsize_];
    error_no_ = QUEUE_NO_ERR;
}

template <class T>
CircularQueue<T>::CircularQueue(bool is_auto_del) : front_no_(0), rear_no_(0), count_no_(0) 
{
    is_auto_del_ = is_auto_del;
    max_qsize_ = 4096;
    queue_list_ = new T[max_qsize_];
    error_no_ = QUEUE_NO_ERR;
}

template <class T>
CircularQueue<T>::CircularQueue(int max_qsize, bool is_auto_del) : front_no_(0), rear_no_(0), count_no_(0) 
{
    is_auto_del_ = is_auto_del;
    max_qsize_ = max_qsize;
    queue_list_ = new T[max_qsize_];
    error_no_ = QUEUE_NO_ERR;
}

template <class T>
CircularQueue<T>::~CircularQueue() {
    delete [] queue_list_;
}

// insert item into the queue
template <class T>
bool CircularQueue<T>::push_back(const T& item) 
{
    // Todo : Lock
    //ciGuard guard(queue_lock_);
    
    // if queue is full, return false, wait...
    if (front_no_ == (rear_no_ + 1) % max_qsize_) { // if (count_no_ == max_qsize_)
        //cerr << "Queue overflow! count_no_ = " << count_no_ << endl;
        error_no_ = QUEUE_OVERFLOW_ERR;
        if (is_auto_del_) {
            // auto delete front_no_ element 
            pop_front();
        } else {
            // if queue is full, return false
            return false;
        }
    }
        
    // increment count_no_, assign item to qlist and update rear_no_
    count_no_++;
    queue_list_[rear_no_] = item;
    rear_no_ = (rear_no_ + 1) % max_qsize_;
    error_no_ = QUEUE_NO_ERR;
    
    return true;
}

/// element from front of queue and return its value
template <class T>
bool CircularQueue<T>::front(T& item)
{
    // Todo : Lock
    //ciGuard guard(queue_lock_);
    
    T temp;

    // if qlist is empty, terminate the program
    if (front_no_ == rear_no_) { 
        if (count_no_ == 0) error_no_ = QUEUE_EMPTY;
        else error_no_ = QUEUE_UNDERFLOW_ERR;
        //cerr << "Queue underflow! count_no_ = " << count_no_ << endl;
        return false;
    } else {
        // record value at the front of the queue
        item = queue_list_[front_no_];

    }
    return true;
}

template <class T>
T CircularQueue<T>::front()
{   
    T out;
    front(out);
    return out;
}

// pop element from front of queue 
template <class T>
bool CircularQueue<T>::pop_front() 
{
    // Todo : Lock
    //ciGuard guard(queue_lock_);
    
    T temp;

    // if qlist is empty, terminate the program
    if (front_no_ == rear_no_) { 
        if (count_no_ == 0) error_no_ = QUEUE_EMPTY;
        else error_no_ = QUEUE_UNDERFLOW_ERR;
        //cerr << "Queue underflow! count_no_ = " << count_no_ << endl;
        return false;
    } else {
        // decrement count_no_, advance front and return former front
        if (count_no_ > 0) count_no_--;
        front_no_ = (front_no_ + 1) % max_qsize_;    
        error_no_ = QUEUE_NO_ERR;
    }
    return true;
}

/// clear the queue by resetting count_no_, front and rear_no_ to 0
template <class T>
void CircularQueue<T>::clear(void) {
    count_no_ = 0;
    front_no_ = 0;
    rear_no_ = 0;
}

/// return number of queue elements
template <class T>
int CircularQueue<T>::size(void) const 
{
    return count_no_;
}

/// test for an empty queue
template <class T>
bool CircularQueue<T>::empty(void) const 
{
    return front_no_ == rear_no_; //count_no_ == 0;
}

// test for a full queue

template <class T>
bool CircularQueue<T>::full(void) const 
{
    //return count_no_ == max_qsize_;
    return (front_no_ == (rear_no_ + 1) % max_qsize_);
}

template <class T>
int CircularQueue<T>::GetError(void) const
{
    return error_no_;
}

#endif	// PLAYER_BEAPOTAGENT_CIRCULARQUEUE_H

