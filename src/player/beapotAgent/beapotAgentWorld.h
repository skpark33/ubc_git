 /*! \file beapotAgentWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _beapotAgentWorld_h_
#define _beapotAgentWorld_h_


#include <ci/libWorld/ciWorld.h> 
#include "TcpStreamServer.h"
#include "BeapotWorker.h"
#include "pickupManager.h"
#include "accessManager.h"
#include "wideManager.h"
#include "BeaconPushService.h"
#include "WideActionService.h"

class beapotAgentWorld : public ciWorld {
public:
	beapotAgentWorld();
	~beapotAgentWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

    void    BeaconAgentStart();
    void    BeaconSimulatorStart();

	static int send(char* p_param);

protected:
    static TcpStreamServer*   p_beapot_server;
    static BeapotWorker*      p_beapot_worker;
    static actionManager*     p_action;
    static wideManager*       p_wide_action;
    static BeaconPushService* p_push_service;
    static WideActionService* p_wide_service;
};
		
#endif //_beapotAgentWorld_h_
