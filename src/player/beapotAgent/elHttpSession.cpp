#ifdef _WIN32
#include "elWinHttpRequest.h"
#else
#endif

#include "elHttpSession.h"


ciSET_DEBUG(5, "elHttpSession");

elHttpSession::elHttpSession(void)
{
    ciDEBUG(5, ("elHttpSession() "));

    host_ = "127.0.0.1";
    url_ = "";
    port_ = 8000;
}

elHttpSession::elHttpSession(string host, string url, int port)
{
    ciDEBUG(5, ("elHttpSession(%s, %s, %d)", host.c_str(), url.c_str(), port));

    host_ = host;
    url_ = url;
    port_ = port;
}

elHttpSession::~elHttpSession(void)
{
    ciDEBUG(5, ("~elHttpSession() "));
}

void elHttpSession::InitSession(string host, string url, int port)
{
    ciDEBUG(5, ("Init(%s, %s, %d)", host.c_str(), url.c_str(), port));

    host_ = host;
    url_ = url;
    port_ = port;
}

bool elHttpSession::SendPostResult(string str_request, string& out_str_result, string strHttpHeader, bool b_base64_encoding)
{
    ciDEBUG(5, ("SendPostResult(http:%s:%ld/%s?%s, BASE64:%d)", host_.c_str(), port_, url_.c_str(), str_request.c_str(), b_base64_encoding));

#ifdef _WIN32
	CString out_result_msg;
	elWinHttpRequest http_request;

	ClearError_();

	http_request.Init(elWinHttpRequest::CONNECTION_USER_DEFINED, host_.c_str(), port_);
    //Init(elWinHttpRequest::CONNECTION_USER_DEFINED, host_.c_str(), port_);

	bool ret_val = false;
    if (b_base64_encoding) {
        ret_val = http_request.RequestPost(url_.c_str(), str_request.c_str(), out_result_msg, strHttpHeader);        
        //ret_val = RequestPost(url_.c_str(), str_request.c_str(), out_result_msg);        
    } else {
        // 일단 현재는 UBCVariable.ini 에 ROOT::USE_HTTP_BASE64 관련 속성이 1로 설정된 경우만 encrypt 처리하므로 같은 걸 호출한다.
        ret_val = http_request.RequestPost(url_.c_str(), str_request.c_str(), out_result_msg, strHttpHeader);
        //ret_val = http_request.Request(false, url_.c_str(), str_request.c_str(), out_result_msg); // protected 함수라 안됨
        //ret_val = RequestPost(url_.c_str(), str_request.c_str(), out_result_msg);
    }

	if(!ret_val) {
		ciERROR((http_request.GetErrorMsg()));
		SetError_(http_request.GetErrorMsg());
		//ciERROR((GetErrorMsg()));
		//SetError_(GetErrorMsg());
		return false;
	}
    out_str_result = (const char* ) out_result_msg.GetBuffer();
#else
#endif

    return true;
}

bool elHttpSession::SendGetResult(string str_request, string& out_str_result, string strHttpHeader, bool b_base64_encoding)
{
    ciDEBUG(5, ("SendGetResult(http:%s:%ld/%s?%s, BASE64:%d)", host_.c_str(), port_, url_.c_str(), str_request.c_str(), b_base64_encoding));

    string str_url_request;

#ifdef _WIN32

	CString out_result_msg;
	elWinHttpRequest http_request;

	ClearError_();

	http_request.Init(elWinHttpRequest::CONNECTION_USER_DEFINED, host_.c_str(), port_);
    //Init(elWinHttpRequest::CONNECTION_USER_DEFINED, host_.c_str(), port_);
	bool ret_val = false;

    str_url_request = url_ + "?" + str_request;
    if (b_base64_encoding) {
        ret_val = http_request.RequestGet(str_url_request.c_str(), out_result_msg, strHttpHeader);        
        //ret_val = RequestGet(str_url_request.c_str(), out_result_msg);        
    } else {
        // 일단 현재는 UBCVariable.ini 에 ROOT::USE_HTTP_BASE64 관련 속성이 1로 설정된 경우만 encrypt 처리하므로 같은 걸 호출한다.
        ret_val = http_request.RequestGet(str_url_request.c_str(), out_result_msg, strHttpHeader);
        //ret_val = http_request.Request(true, url_.c_str(), str_request.c_str(), out_result_msg); // protected 함수라 안됨
        //ret_val = RequestGet(str_url_request.c_str(), out_result_msg);
    }

	if(!ret_val) {
		ciERROR((http_request.GetErrorMsg()));
		SetError_(http_request.GetErrorMsg());
		//ciERROR((GetErrorMsg()));
		//SetError_(GetErrorMsg());
		return false;
	}
    out_str_result = (const char* ) out_result_msg.GetBuffer();
#else
#endif

    return true;
}

///////////////////////////
//
// HttpRequest
