/** \class elRoute
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elRoute_h_
#define _elRoute_h_

#include "elComponent.h"
#include "elJob.h"

class elRoute : public elComponent
{
public:
    elRoute(void);
    virtual ~elRoute(void);

    void    ProcessExpired(string name, int counter, int interval_sec, int interval_msec=0);
    void    Update();
    bool    Init(string my_id);
    bool    Start(string option = ADMIN_STATE_START_ALL);
    bool    Stop();

protected:
    void    SetChildComponent_();

protected:
    elRuleRepository*   p_rule_repository_;
    elRouteRule*        p_my_rule_;
    elRuleIdMap         job_rule_map_;
    vector<elJob*>      job_vec_;

};

#endif // _elRoute_h_