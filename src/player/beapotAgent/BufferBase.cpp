#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>

#include "BufferBase.h"

ciSET_DEBUG(1, "BufferBase");

BufferBase::BufferBase()
{
    ciDEBUG(5, ("Start... "));
    
    buffer_ = NULL;
    buffer_len_ = 0;
    alloc_size_ = 0;
    
    Alloc_(DEFAULT_BUF_SIZE);
}


BufferBase::BufferBase(int size)
{
    ciDEBUG(5, ("Start... "));
    
    buffer_ = NULL;
    buffer_len_ = 0;
    alloc_size_ = 0;
    byte_order_ = -1;
    
    Alloc_(size);
}

BufferBase::~BufferBase()
{
    DeAlloc_();
}

void BufferBase::Init(bool alloc_init) {
    ciDEBUG(5, ("Init()... "));

    if (alloc_init) {
        DeAlloc_();
    } else {
        alloc_size_ = 0;
        buffer_len_ = 0;
        byte_order_ = HOST_BYTE_ORDER;                
    }
}

char* BufferBase::Get() {
    return buffer_;
}

char* BufferBase::Get(unsigned int index_pos) {
    return buffer_ + index_pos;
}

int BufferBase::Length() {
    return buffer_len_;
}

int BufferBase::Size() {
    return alloc_size_;
}

bool BufferBase::AppendDataLen(int length)
{
    ciDEBUG(5, ("AppendDataLen... "));
    
    if ((int) buffer_len_ + length > alloc_size_) {
        ciERROR(("length exceeds the alloc_size_."));
        return false;
    }
    buffer_len_ += length;
    ciDEBUG(5, ("buffer_len_ : %d ", buffer_len_));
    return true;
}

int BufferBase::Resize(int size)
{
    ciDEBUG(5, ("Resize... "));
    
    if (size > alloc_size_) {
        if (ReAlloc_(size) == false) {
            return -1;
        }
    }
    ciDEBUG(5, ("size[%d], alloc_size_[%d]\n", size, alloc_size_));
    return alloc_size_;
}

void BufferBase::SetByteOrder(int byte_order)
{
    byte_order_ = byte_order;
}

int BufferBase::GetByteOrder()
{
    return byte_order_;
}


///
/// Character Buffer를 Set
/// @param data [in] : Set Buffer
/// @param start_set_pos [in] : 시작 위치
/// @param length [in] : 버퍼에 들어갈 길이
/// @return 성공이냐 실패냐
///    | Return code | Description |
///    | :---------: | :---------- |
///    | true        | 성공        |
///    | false       | 실패        |
/// @see  
///
bool BufferBase::Set(const char* data, int start_set_pos, int length)
{
    ciDEBUG(5, ("Start... "));
    
    if (data == NULL) return false;

    if (length + start_set_pos > alloc_size_) {
        if (ReAlloc_(length) == false) {
            return false;
        }
    }
    
    memcpy(buffer_ + start_set_pos, data, length);
    
    // 패킷 중간에 Header처럼 끼워넣는 경우 buffer_len_ 보다 더 크면 buffer_len_을 다시 설정
    if(buffer_len_ < (unsigned int) length + start_set_pos) {
        buffer_len_ = (unsigned int) length + start_set_pos;
    }
    
    return true;
}

bool BufferBase::Append(const char* data, int length)
{
    ciDEBUG(5, ("Start... "));
    
    if (data == NULL) return false;
    
    if ((int) buffer_len_ + length > alloc_size_) {
        if (ReAlloc_(buffer_len_ + length) == false) {
            return false;
        }
    }    
    
    memcpy(buffer_ + buffer_len_, data, length);
    buffer_len_ += length;
    
    return true;    
}

//int BufferBase::Copy(char* copy_packet)
//{
//    frDEBUG(("<%s> " "Start... ", __func__));
//    
//    unsigned int n = 0;
//    
//    if (buffer_len_ > sizeof(copy_packet)) {
//        n = sizeof(copy_packet);
//    } else {
//        n = buffer_len_;
//    }
//    
//    memcpy(copy_packet, buffer_, n);
//    
//    return n;
//}
        
bool BufferBase::Alloc_(int size)
{
    ciDEBUG(5, ("Start... size[%d] ", size));
    
    int n = 0;
    
    if (size == 0) return false;    

    n = DEFAULT_BUF_SIZE;
    while (size > n) {
        n += DEFAULT_BUF_SIZE;
    }    
    
    // 16M 넘으면 false
    if (n > MAX_BUF_SIZE) {
        ciERROR(("Exceed MAX_BUF_SIZE[%d]", MAX_BUF_SIZE));
        return false;
    }
    
    buffer_ = (char*) malloc(n);
    if (buffer_ == NULL) {
        ciERROR(("There is a lack of heap memory"));
        return false;
    }
    alloc_size_ = n;
    ciDEBUG(5, ("Alloc_(alloc_size_[%d])", alloc_size_));
    
    return true;
}

bool BufferBase::ReAlloc_(int new_size)
{
    ciDEBUG(5, ("Start... new_size[%d] ", new_size));
    
    int n = 0;
    char* temp = NULL;
    char* buffer_backup = NULL;
    
    if (new_size == 0) return false;
    
    n = alloc_size_;
    while (new_size > n) {
        n += DEFAULT_BUF_SIZE;
    }

    // 16M 넘으면 false
    if (n > MAX_BUF_SIZE) {
        ciERROR(("Exceed MAX_BUF_SIZE[%d]", MAX_BUF_SIZE));
        return false;
    }
    
    // 기존 버퍼 백업
    buffer_backup = new char[buffer_len_];
    memcpy(buffer_backup, buffer_, buffer_len_);
    
    temp = (char*) realloc((void*) buffer_, n);
    if (temp == NULL) {        
        ciERROR(("There is a lack of heap memory"));
        return false;
    }
    buffer_ = temp;
    alloc_size_ = n;
    
    // 기존 버퍼 복사
    memcpy(buffer_, buffer_backup, buffer_len_);
    
    return true;    
}

void BufferBase::DeAlloc_()
{
    ciDEBUG(5, ("Start... "));

    alloc_size_ = 0;
    buffer_len_ = 0;
    byte_order_ = HOST_BYTE_ORDER;
        
    if (buffer_ == NULL) {
        if (alloc_size_ != 0) {
            alloc_size_ = 0;
            buffer_len_ = 0;
            ciDEBUG(5, ("alloc_size_ is inaccurate."));
        }
        return;
    }
    
    free(buffer_);
    buffer_ = NULL;
}

int BufferBase::Align4_(int len)
{
    ciDEBUG(5, ("Start... "));
    
    int n = 0;
    
    if (len % 4 == 1)      n = len + 3;
    else if (len % 4 == 2) n = len + 2;
    else if (len % 4 == 3) n = len + 1;
    else n = len;
    
    return n;
}
