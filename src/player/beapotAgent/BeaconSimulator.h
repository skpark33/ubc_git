/*! \class BeaconSimulator
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/10/06 11:01:00
 */

#ifndef PLAYER_BEAPOTAGENT_BEACONSIMULATOR_H
#define PLAYER_BEAPOTAGENT_BEACONSIMULATOR_H

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>

#include "ChannelDefine.h"
#include "actionManager.h"

class ciObject;

class ciMsecTimer : public ciThread
{
public:
    ciMsecTimer(const char* pTimerName, ciObject* p_object) : _name(pTimerName), 
        p_object_(p_object), _counter(0), _interval_msec(100),_started(ciFalse) {} 

    virtual ~ciMsecTimer(void) { stopTimer(); }

    void startTimer() {
        stopTimer();
        _started = ciTrue;
	    start();
    }
    void stopTimer() {
	    if(_started) {
		    _started = ciFalse;
		    stop(); 
	    }
    }
    void setInterval(int interval_msec) {
        _interval_msec = interval_msec; 
    }

    void run() ;

protected:
    ciObject* p_object_;

private:
	int 			_counter ;
    int 			_interval_msec;
	ciBoolean		_started;
	ciString		_name;

};


class ciObject {
public:
    ciObject() {
        _timer = NULL;
    }
    virtual ~ciObject() {
        stopTimer();
        if (_timer) delete _timer;
    }

    virtual void processExpired(ciString name, int counter, int interval_msec) = 0;
    virtual void initTimer(const char* pname, int interval_msec = 100)
	{ 
		if(_timer) {
			stopTimer();
			delete _timer; 
            _timer = NULL;
		}
		_timer = new ciMsecTimer(pname, this); 
	    _timer->setInterval(interval_msec);
	}
    virtual void startTimer()                   { if(_timer) _timer->startTimer(); }
    virtual void stopTimer()                    { if(_timer) _timer->stopTimer(); }
    virtual void setInterval(int interval_msec)     { if(_timer) _timer->setInterval(interval_msec); }

private:
    ciMsecTimer* _timer;
};

class BeaconSimulator : public ciThread, public ciObject
{
public:
    BeaconSimulator();
    virtual ~BeaconSimulator(void);

    virtual void processExpired(ciString name, int counter, int interval_msec);

    void Start();
    void Stop();
    void run();

    void RegisterActionManager(const char* action_type, actionManager* p_action_manager);
    void SendToActionManager(beaconInfo& beacon_scan_msg);
    
    //bool FileRead(std::string strFileFullName, std::string& strBuffer, bool is_strcat);
    bool FileRead(std::string strFileFullName, ciStringList& outList);

    static string MakeBeaconPacket(string scanner_id, string beacon_id, int rssi, int battery, int xiro_x, int xiro_y, int xiro_z );
protected:
    void SampleRead_();
	void _push_back(ciStringList& inList, vector<BEACON_ACCEL_DATA>& outVector);

protected:
    string              action_type_;
    actionManager*      p_action_manager_;
    bool                started_;
    bool                sample_send_on_;
    ciMutex		        lock_;

    vector<BEACON_ACCEL_DATA> g_pickup_D03972CDDB0A_data;
    vector<BEACON_ACCEL_DATA> g_pickup_D03972CDDA78_data;
    vector<BEACON_ACCEL_DATA> g_pickup_D03972CDD962_data;

};


#endif //PLAYER_BEAPOTAGENT_BEACONSIMULATOR_H