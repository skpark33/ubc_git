#include "elRuleRepository.h"

#include "elEndPointRule.h"
#include "elGatewayRule.h"
#include "elRouteRule.h"
#include "elJobRule.h"
#include "elPropertyRule.h"

#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciXProperties.h>

ciSET_DEBUG(5, "elRuleRepository");

ciMutex elRuleRepository::_single_lock;
elRuleRepository* elRuleRepository::_instance = 0;

elRuleRepository* elRuleRepository::GetInstance()
{
	if (!_instance) {
		CI_GUARD(_single_lock, "getInstance()");
		if (!_instance) {
			_instance = new elRuleRepository;
            _instance->Init("C:\\SQISoft\\UTV1.0\\execute\\data\\ELiga.cfg", EL_COMPONENT_RULE);
		}
	}
	return _instance;
}

void elRuleRepository::ClearInstance()
{
	if (_instance) {
		CI_GUARD(_single_lock, "getInstance()");
		if (_instance) {
			delete _instance;
			_instance = 0;
		}
	}
}


elRuleRepository::elRuleRepository(void) : p_eliga_component_rule_(NULL), eliga_root_obj_(NULL), eliga_component_obj_(NULL)
{
    ciDEBUG(5, ("elRuleRepository()"));
}

elRuleRepository::~elRuleRepository(void)
{
    ciDEBUG(5, ("~elRuleRepository()"));
}

elRule* elRuleRepository::GetEligaRule(string keyword, string id)
{
    if (!p_eliga_component_rule_) {
        return NULL;
    }

    return p_eliga_component_rule_->GetRule(keyword, id);
}

elRule* elRuleRepository::GetEligaRule(string id)
{
    if (!p_eliga_component_rule_) {
        return NULL;
    }

    return p_eliga_component_rule_->GetRule(id);
}


bool elRuleRepository::Init(string pathFileName, string eligaRuleName)
{
    ciDEBUG(5, ("Init(%s)", pathFileName.c_str()));

    ciGuard init_guard(repo_lock_);

    GetHostName();

    if (!ReadELigaCfg_(pathFileName)) {
        return false;
    }

    path_file_name_ = pathFileName;
    eliga_component_rule_key_ = eligaRuleName;

    if (!SetRepository_(str_eliga_cfg_, eliga_component_rule_key_)) {
        return false;
    }

    ciDEBUG(5, ("Succeed Init", pathFileName.c_str()));
    return true;
}


string elRuleRepository::GetHostName()
{
    string hostName("");

	if(ciArgParser::getInstance()->getArgValue("+host",hostName)) {
		return hostName;
	}
#ifdef _WINDOWS	
	char    szBuffer[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD   dwNameSize = MAX_COMPUTERNAME_LENGTH + 1;
	GetComputerNameA(szBuffer, &dwNameSize);
#else
	char    szBuffer[255 + 1];
	memset(szBuffer,0x00,256);
	gethostname(szBuffer,256);
#endif
	hostName = szBuffer; 
	
    // Global.HostName은 예약어
    SetGlobalValue("Global.HostName", hostName);

	return hostName;
}

void elRuleRepository::SetGlobalValue(string globalName, string globalValue)
{
    ciDEBUG(5, ("SetGlobalValue(%s, %s)", globalName.c_str(), globalValue.c_str()));

    // 2016-01-20 
    if (globalName == "") {
        return;
    }
    global_map_[globalName] = globalValue;
}

// "Global.Hostname" : "KOLON-46009"
// "Global.ShopID" : "LC4855"
// "Global.BrandID" : "LC%"
string elRuleRepository::GetGlobalValue(string globalName)
{
	map<string, string>::iterator itr = global_map_.find(globalName);
	if(itr == global_map_.end()) {
		return string("");
	}
    return itr->second;
}

string elRuleRepository::SetGlobalPropertyBind(string source)
{
    ciDEBUG(5, ("SetGlobalPropertyBind(%s)", source.c_str()));

    // 2016-01-20 select가 없는 경우 "" 처리
    if (source == "") {
        return source;
    }

    elPropertyRule* pPropertyRule = (elPropertyRule*) GetEligaRule("GLOBAL_PROPERTY"); // GLOBAL_PROPERTY 는 오직 하나

    if (!pPropertyRule) {
        ciERROR(("Failed! Get GLOBAL_PROPERTY pPropertyRule " ));
        return source;
    }

    ciDEBUG(5, ("Property Rule (%s, %s)", pPropertyRule->property_id.c_str(), pPropertyRule->property_type.c_str()));
    // source 바인딩 #{value} 바인딩Value가 없으면 source가 바뀌는 것 없음.
    return pPropertyRule->GlobalBindProperty(source);
}

bool elRuleRepository::ReadELigaCfg_(string& pathFileName)
{
    ciDEBUG(5, ("ReadELigaCfg_(%s)", pathFileName.c_str()));

    bool ret_value = true;

    char sz_buffer[2048];
    unsigned int readed;
    FILE *fin;
    
    memset(sz_buffer, 0x00, sizeof (sz_buffer));
    str_eliga_cfg_ = "";

    if ((fin = fopen(pathFileName.c_str(), "rb")) == NULL) {
        ciERROR(("Failed ReadELigaCfg_(%s)", pathFileName.c_str()));
        return false;
    }

    do {
        readed = (unsigned int) fread(sz_buffer, 1, sizeof (sz_buffer) - 1, fin);
        if (readed > 0) {
            sz_buffer[readed] = 0;
            str_eliga_cfg_ += sz_buffer;
        }
        else break;
    } while (readed == (sizeof (sz_buffer) - 1));

    fclose(fin);

    return true;
}

bool elRuleRepository::SetRepository_(string& strEligaCfg, string& eligaRuleKeyword) 
{
    ciDEBUG(5, ("SetRepository_(%s)", eligaRuleKeyword.c_str()));

    if (eligaRuleKeyword == EL_KEYWORD_COMPONENT_RULE) {        
        if (!SetELigaComponentRule_(strEligaCfg)) {
            ciERROR(("Failed! SetELigaComponentRule_. Rule Keyword : %s ", eligaRuleKeyword.c_str()));
            return false;
        }
    } else {
        ciERROR(("Failed! ELiga rule is not exist "));
        return false;
    }
        
    ciDEBUG(5, ("Success! SetRepository_(%s)", eligaRuleKeyword.c_str()));
    return true;
}

bool elRuleRepository::SetELigaComponentRule_(string& strELigaCfg)
{
    ciDEBUG(5, ("SetELigaComponentRule_()"));
    ciDEBUG(5, ("\nELiga.cfg : \n%s\n", strELigaCfg.c_str() ));

    string str_temp("");
    string str_id  ("");
    JsonObject* temp_obj = NULL;
    JsonValue* temp_value = NULL;

    JsonValue* json_value = JsonValue::create(strELigaCfg.c_str(), 0, (int) strELigaCfg.length());

    if (json_value == NULL) {
        ciERROR(("Failed! strELigaCfg, JsonValue::create "));
        return false;
    }

    if (json_value->isObject() == false) {
        ciERROR(("Failed! ELigaRoot is not JsonObject "));
        delete json_value;
        return false;
    }
    eliga_root_obj_ = (JsonObject*) json_value;

    // ELigaComponentRule
    if ((temp_value = eliga_root_obj_->get(eliga_component_rule_key_.c_str())) == NULL) {
        ciERROR(("Failed! can not found ELigaComponentRule : %s ", eliga_component_rule_key_.c_str()));
        delete eliga_root_obj_;
        return false;
    }
    if (temp_value->isObject() == false) {
        ciERROR(("Failed! ELigaComponentRule is not JsonObject "));
        delete json_value;
        return false;
    }    
    eliga_component_obj_ = (JsonObject*) temp_value;
    
    p_eliga_component_rule_ = new elComponentRule(EL_KEYWORD_COMPONENT_RULE);
    if (!p_eliga_component_rule_->Set(eliga_component_obj_, NULL, NULL)) {
        ciERROR(("Failed! ELigaComponentRule setting"));
        delete eliga_root_obj_;
        return false;
    }

    p_eliga_component_rule_->Add(p_eliga_component_rule_->component_id, p_eliga_component_rule_);

    return true;
}

