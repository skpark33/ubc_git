#include "elComponentRule.h"
#include "elEndPointRule.h"
#include "elPropertyRule.h"
#include "elGatewayRule.h"
#include "elRouteRule.h"
#include "elJobRule.h"


ciSET_DEBUG(5, "elComponentRule");

elComponentRule::elComponentRule(string strRuleKeyword)
    : p_component_obj_(NULL), p_top_rule_(NULL), p_parent_rule_(NULL)
{
    ciDEBUG(5, ("elComponentRule()"));
    rule_keyword = strRuleKeyword;
}


elComponentRule::~elComponentRule(void)
{
    ciDEBUG(5, ("~elComponentRule()"));
}


void elComponentRule::ClearDefineConstVMap_(elDefineConstVMap& defConstVMap)
{
    defConstVMap.clear();
}

void elComponentRule::ClearRuleKeyMMap_(elRuleKeyMMap& comRuleKeyMMap)
{
	elRuleKeyMMap::iterator itr;
    elRuleIdMap::iterator jtr;
	for(itr=comRuleKeyMMap.begin(); itr!=comRuleKeyMMap.end(); itr++) {
        // Clear per elRuleIdMap
        for (jtr = itr->second.begin(); jtr != itr->second.end(); ) {
            elRule* p_ele = jtr->second;
            if (p_ele) {
                delete p_ele;
                itr->second.erase(jtr++);
            } else {
                ++jtr;
            }            
        }
        itr->second.clear();
	}
	comRuleKeyMMap.clear();
}

bool elComponentRule::SetDefineRuleConstant_(JsonObject* pDefRuleType)
{
    ciDEBUG(5, ("SetDefineRuleConstant_(DefineRuleType)"));

    JsonObject* json_obj   = NULL;
    JsonArray*  json_arr   = NULL;
    JsonValue*  temp_value = NULL;
    string      str_temp("");

    // DefineRuleType
    json_arr = GetChildArray(EL_COM_DFC_COMP_TYPE, pDefRuleType);
    if (!json_arr) {
        ciERROR(("Failed! %s can not found ", EL_COM_DFC_COMP_TYPE));
        return false;
    }
    elRuleConstVec component_const_vec;    
    SetStringVector(json_arr, component_const_vec);
    define_const_vmap_.insert(elDefineConstVMap::value_type(EL_COM_DFC_COMP_TYPE, component_const_vec));

    // EndPointType
    json_arr = GetChildArray(EL_COM_DFC_EP_TYPE, pDefRuleType);
    if (!json_arr) {
        ciERROR(("Failed! %s can not found ", EL_COM_DFC_EP_TYPE));
        return false;
    }
    elRuleConstVec ep_type_vec;    
    SetStringVector(json_arr, ep_type_vec);
    define_const_vmap_.insert(elDefineConstVMap::value_type(EL_COM_DFC_EP_TYPE, ep_type_vec));

    // GatewayType
    json_arr = GetChildArray(EL_COM_DFC_COMP_TYPE, pDefRuleType);
    if (!json_arr) {
        ciERROR(("Failed! %s can not found ", EL_COM_DFC_COMP_TYPE));
        return false;
    }
    elRuleConstVec gw_type_vec;    
    SetStringVector(json_arr, gw_type_vec);
    define_const_vmap_.insert(elDefineConstVMap::value_type(EL_COM_DFC_COMP_TYPE, gw_type_vec));

    // RouteType
    json_arr = GetChildArray(EL_COM_DFC_ROUTE_TYPE, pDefRuleType);
    if (!json_arr) {
        ciERROR(("Failed! %s can not found ", EL_COM_DFC_ROUTE_TYPE));
        return false;
    }
    elRuleConstVec route_type_vec;    
    SetStringVector(json_arr, route_type_vec);
    define_const_vmap_.insert(elDefineConstVMap::value_type(EL_COM_DFC_ROUTE_TYPE, route_type_vec));

    // JobType
    json_arr = GetChildArray(EL_COM_DFC_JOB_TYPE, pDefRuleType);
    if (!json_arr) {
        ciERROR(("Failed! %s can not found ", EL_COM_DFC_JOB_TYPE));
        return false;
    }
    elRuleConstVec job_type_vec;    
    SetStringVector(json_arr, job_type_vec);
    define_const_vmap_.insert(elDefineConstVMap::value_type(EL_COM_DFC_JOB_TYPE, job_type_vec));

    ciDEBUG(5, ("SetDefineRuleConstant_ Complete! define_const_vmap_ size : %d", define_const_vmap_.size()));

    return true;
}

bool elComponentRule::SetEndPointRule_(JsonArray* pEndPointArr)
{
    ciDEBUG(5, ("SetEndPointRule_(ELigaEndPoint)"));

    if (!pEndPointArr) {
        return false;
    }

    JsonObject*     endpoint_obj = NULL;
    elEndPointRule* p_endpoint_rule = NULL;

    for (int i = 0; i < pEndPointArr->getCount(); i++) {
        endpoint_obj = (JsonObject*) pEndPointArr->get(i);
        if (endpoint_obj == NULL) {
            ciERROR(("Failed! pEndPointArr[%d] is NULL ", i));
            return false;
        }
        // new elEndPointRule
        p_endpoint_rule = new elEndPointRule(EL_KEYWORD_ENDPOINT_RULE);
        p_endpoint_rule->Set(endpoint_obj, this, this);
    }

    ciDEBUG(5, ("SetEndPointRule_ complete"));
    return true;
}

bool elComponentRule::SetPropertyRule_(JsonArray* pPropertyArr)
{
    ciDEBUG(5, ("SetPropertyRule_(ELigaProperty)"));
    if (!pPropertyArr) {
        return false;
    }

    JsonObject*     property_obj = NULL;
    elPropertyRule*  p_property_rule = NULL;

    for (int i = 0; i < pPropertyArr->getCount(); i++) {
        property_obj = (JsonObject*) pPropertyArr->get(i);
        if (property_obj == NULL) {
            ciERROR(("Failed! pPropertyArr[%d] is NULL ", i));
            return false;
        }
        // new elPropertyRule
        p_property_rule = new elPropertyRule(EL_KEYWORD_PROPERTY_RULE);
        p_property_rule->Set(property_obj, this, this);

    }

    ciDEBUG(5, ("SetPropertyRule_ complete"));

    return true;
}

bool elComponentRule::SetGatewayRule_(JsonArray* pGatewayArr)
{
    ciDEBUG(5, ("SetGatewayRule_(ELigaGateway)"));
    if (!pGatewayArr) {
        return false;
    }

    JsonObject*     gateway_obj = NULL;
    elGatewayRule*  p_gateway_rule = NULL;

    for (int i = 0; i < pGatewayArr->getCount(); i++) {
        gateway_obj = (JsonObject*) pGatewayArr->get(i);
        if (gateway_obj == NULL) {
            ciERROR(("Failed! pGatewayArr[%d] is NULL ", i));
            return false;
        }
        // new elEndPointRule
        p_gateway_rule = new elGatewayRule(EL_KEYWORD_GATEWAY_RULE);
        p_gateway_rule->Set(gateway_obj, this, this);

    }

    //if (gateway_id_idx.size() == 0) {
    //    ciERROR(("Failed! gateway_id_idx size is 0. not exist ELigaGateway rule "));
    //    return false;
    //}
    ciDEBUG(5, ("SetGatewayRule_ complete"));

    return true;
}


bool elComponentRule::Set(JsonObject* pComponentRule, elRule* pParentRule, elRule* pComposedRule)
{
    ciDEBUG(5, ("Set(ELigaComponentRule)"));

    if (pComponentRule == NULL) {
        ciERROR(("Failed! ELigaComponentRule is NULL"));
        return false;
    }

    // 자료 초기화
    ClearDefineConstVMap_(define_const_vmap_);
    ClearRuleKeyMMap_(component_rule_mmap_);

    p_component_obj_ = pComponentRule;
    p_parent_rule_ = pParentRule;
    p_top_rule_ = pComposedRule;
    parent_id = (pParentRule == NULL) ? "" : pParentRule->id;

    JsonObject* child_obj  = NULL;
    JsonValue*  temp_value = NULL;
    JsonArray*  child_arr  = NULL;

    // DefineRuleType
    child_obj = GetChildObject(EL_KEYWORD_DEFINE_RULE_CONSTANT, pComponentRule);
    if (!child_obj) {
        ciERROR(("Failed! %s can not found ", EL_KEYWORD_DEFINE_RULE_CONSTANT));
        return false;
    }
    if (!SetDefineRuleConstant_(child_obj)) {
        ciERROR(("Failed! SetDefineRuleConstant_ "));
        return false;
    }

    // ELigaEndPoint
    child_arr = GetChildArray(EL_KEYWORD_ENDPOINT_RULE, pComponentRule);
    if (!child_arr) {
        ciERROR(("Failed! %s can not found ", EL_KEYWORD_ENDPOINT_RULE));
        return false;
    }
    if (!SetEndPointRule_(child_arr)) {
        ciERROR(("Failed! SetEndPointRule_ "));
        return false;
    }

    // ELigaProperty
    child_arr = GetChildArray(EL_KEYWORD_PROPERTY_RULE, pComponentRule);
    if (!child_arr) {
        ciERROR(("Failed! %s can not found ", EL_KEYWORD_PROPERTY_RULE));
        return false;
    }
    if (!SetPropertyRule_(child_arr)) {
        ciERROR(("Failed! SetPropertyRule_ "));
        return false;
    }

    // ELigaGatewayRule
    child_arr = GetChildArray(EL_KEYWORD_GATEWAY_RULE, pComponentRule);
    if (!child_arr) {
        ciERROR(("Failed! %s can not found ", EL_KEYWORD_GATEWAY_RULE));
        return false;
    }
    if (!SetGatewayRule_(child_arr)) {
        ciERROR(("Failed! SetGatewayRule_ "));
        return false;
    }

    return true;
}

// 각 elRule에서 Set시 Add
void elComponentRule::Add(string id, elRule* pRule)
{
    ciDEBUG(5, ("Add(%s)", id.c_str()));

    elRuleIdMap rule_id_map;

    // Composited
    if (rule_id_map.insert(elRuleIdMap::value_type(id, pRule)).second == false) {
        ciERROR(("Failed! Inserting Rule ID Map. id = %s ", id.c_str()));
        return;
    }    
    //if (component_rule_mmap_.insert(elRuleKeyMMap::value_type(pRule->rule_keyword, rule_id_map)).second == false) {
    //    ciERROR(("Failed! Inserting Rule Key MMap. rule_keyword = %s ", pRule->rule_keyword.c_str()));
    //    return;
    //}

    if (component_rule_id_map_.insert(elRuleIdMap::value_type(id, pRule)).second == false) {
        ciERROR(("Failed! Inserting component_rule_id_map_. id = %s ", id.c_str()));
        return;
    }    

}

void elComponentRule::Remove(string keyword, string id)
{
    ciDEBUG(5, ("Not yet!!!"));
}

elRule* elComponentRule::GetRule(string keyword, string id)
{
    ciDEBUG(5, ("GetRule(%s, %s)", keyword.c_str(), id.c_str()));

    string str_temp_key("");
    string str_temp_id("");
    elRuleConstVec def_comp_key_vec = define_const_vmap_[EL_COM_DFC_COMP_KEY];
    elRuleConstVec def_comp_id_vec  = define_const_vmap_[EL_COM_DFC_COMP_ID];

    // DefineRuleconstant 에 key와 id가 정의되어 있지 않으면 return NULL 이다.
    // "ComponentKeyword": [ "ELigaEndPoint", "ELigaGateway", "RouteArr", "JobArr", "FromCommand", "ToCommand" ]
    for (unsigned int i = 0; i < def_comp_key_vec.size(); i++) {
        if (def_comp_key_vec[i] == keyword) {
            str_temp_key = def_comp_key_vec[i];
        }
    }
    // "ComponentID": [ "OFS_DB_GW_1" ]
    for (unsigned int i = 0; i < def_comp_id_vec.size(); i++) {
        if (def_comp_id_vec[i] == id) {
            str_temp_id = def_comp_id_vec[i];
        }
    }
    
    if (str_temp_key == keyword) {
        if (str_temp_id == id) {
            elRuleIdMap rule_id_map = component_rule_mmap_[keyword];
            return rule_id_map[id]; // return elRule*
        }
        ciERROR(("Failed! Not exist ComponentID : %s ", id.c_str()));
    }
    ciERROR(("Failed! Not exist ComponentKeyword : %s ", keyword.c_str()));

    return NULL;
}

elRule* elComponentRule::GetRule(string id)
{
    ciDEBUG(5, ("GetRule(%s)", id.c_str()));

    string str_temp_id("");
    elRuleConstVec def_comp_id_vec  = define_const_vmap_[EL_COM_DFC_COMP_ID];

    // "ComponentID": [ "GW_OFS_DBGW_1" ]
    for (unsigned int i = 0; i < def_comp_id_vec.size(); i++) {
        if (def_comp_id_vec[i] == id) {
            str_temp_id = def_comp_id_vec[i];
        }
    }
    
    //if (str_temp_id == id) {
        return component_rule_id_map_[id]; // return elRule*
    //}
    //ciERROR(("Failed! Not exist DefineRuleConstant::ComponentID : %s ", id.c_str()));

    //return NULL;
}

