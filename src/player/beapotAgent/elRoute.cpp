#include "elRoute.h"

ciSET_DEBUG(5, "elRoute");

elRoute::elRoute(void)
{
    ciDEBUG(5, ("elRoute()"));
}

elRoute::~elRoute(void)
{
    ciDEBUG(5, ("~elRoute()"));
}

void elRoute::ProcessExpired(string name, int counter, int interval_sec, int interval_msec)
{
    ciDEBUG(5, ("ProcessExpired(%s, %d, %d, %d)", name.c_str(), counter, interval_sec, interval_msec));

}

void elRoute::Update()
{
    ciDEBUG(5, ("Update()"));
}

bool elRoute::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elRouteRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;

    job_rule_map_ = p_my_rule_->GetChildRuleMap(EL_KEYWORD_JOB_RULE);
    return true;
}

bool elRoute::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    if (option == ADMIN_STATE_START_NO) {
        // Do noting
        return true;
    } else if (option == ADMIN_STATE_START_ALL) {
        InitTimer("elRouteTimer", 60);
        StartTimer();
        StartThread();
        SetChildComponent_();
    } else if (option == ADMIN_STATE_START_TIMER) {
    } else if (option == ADMIN_STATE_START_THREAD) {
    } else if (option == ADMIN_STATE_START_CHILD) {
        SetChildComponent_();
    } else {
        ciERROR(("Failed! Start option(%s)", option.c_str()));
        return false;
    }

    return true;
}

bool elRoute::Stop()
{
    ciDEBUG(5, ("Stop()"));

    StopTimer();
    StopThread();

    // Stop child
    for (unsigned int i = 0; i < job_vec_.size(); i++) {
        job_vec_[i]->Stop();
    }

    return true;
}

void elRoute::SetChildComponent_()
{
    ciDEBUG(5, ("SetChildComponent_()"));

	elRuleIdMap::iterator itr;
	for(itr = job_rule_map_.begin(); itr != job_rule_map_.end(); itr++) {
		elRule* ele = itr->second;
        elJob* job = new elJob();
        job->Init(ele->id);
        job->Start(ele->admin_state);
        job_vec_.push_back(job);
	}
}

