/*! \class BeaconPushService
 *  Copyright ⓒ 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/10/06 11:01:00
 */

#ifndef _WideActionService_h_
#define _WideActionService_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include "actionManager.h"
#include "goodsInfoManager.h"
#include "TcpStreamChannel.h"
#include "CircularQueue.h"
#include "BeaconPushService.h"
#include "DbLogger.h"

enum 
{
	PICK_INIT = 1,
	PICK_UP,
	PICK_DOWN
};


class WideActionService : public actionHandler, public ciThread
{
public:
    WideActionService();
    ~WideActionService(void);

	virtual void    processEvent(ciString& actionType, ciStringList& beaconIdList, int additionalState);

    void    loadIni();
    void    StartService();
    void    StopService();
    void    run();

protected:
    bool    GetGoodsInfo_(BEACON_ACTION_INFO& beacon_action_info, goodsInfoList& out_goods_list);
    bool    SetPickupCount_(ciString& action_type, goodsInfoList& in_goods_info_list);
    void    InitPickDown_();
    void    SetPickUp_(ciString& beacon_id, ciString& goods_id);
    void    EnqueuePickupLogDb();
    void    ReleaseGoodsInfo(goodsInfoList& goods_info_list);

protected:
    list<BEACON_ACTION_INFO> beacon_action_queue_; // DB는 딜레이 되도 되나 빼먹으면 안되기 때문에 리스트 큐 사용
    //CircularQueue<BEACON_ACTION_INFO> beacon_action_queue_;
    ciMutex		             beacon_action_queue_lock_; //ciGuard aGuard(beacon_action_queue_lock_);
    bool                     started_;
   
    ciString                 site_id_;
    ciString                 api_adapter_ip_;
    ciLong                   api_adapter_port_;

    DbLogger*                p_db_logger_;

    map<string, GOODS_PICK_UPDOWN> pickup_goods_map_;
    int                      null_beacon_cnt_;
};


#endif //_WideActionService_h_