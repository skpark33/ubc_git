#include "wideManager.h"

ciSET_DEBUG(5, "wideManager");

wideManager::wideManager(actionHandler* handler, int threshold, int maxOrder, int expiredSec)
		: pickupManager(handler, threshold, maxOrder, expiredSec)
{
    ciDEBUG(1,("wideManager"));

    _actionType = "WIDE";
    _no_move_threshold_count = threshold;
    _pickup_threshold = 500;
    loadIni();
}

wideManager::~wideManager()
{
}
