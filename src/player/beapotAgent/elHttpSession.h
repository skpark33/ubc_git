/** \class elHttpSession
 *  Copyright ⓒ 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief elHttpSession 향후 elSession 추상클래스로 부터 상속
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2016/01/22 11:01:00
 */

#ifndef _elHttpSession_h_
#define _elHttpSession_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>

class elHttpSession
{
public:

    elHttpSession(void);
    elHttpSession(string host, string url, int port);
    ~elHttpSession(void);

    void    InitSession(string host, string url, int port);
    bool    SendPostResult(string str_request, string& out_str_result, string strHttpHeader="", bool b_base64_encoding=true);
    bool    SendGetResult(string str_request, string& out_str_result, string strHttpHeader="", bool b_base64_encoding=true);    

    const char* GetError()               { return error_msg_.c_str(); }

protected:
  	void    SetError_(const char* err)   { error_msg_ = err; }
	void    ClearError_()                { error_msg_ = "";}

protected:    
    string  host_;
    string  url_;
    ciLong  port_;
    
    string	error_msg_;

};

#endif //_elHttpSession_h_