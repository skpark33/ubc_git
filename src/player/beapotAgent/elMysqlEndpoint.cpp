#include "elMysqlEndpoint.h"

ciSET_DEBUG(5, "elMysqlEndpoint");

elMysqlEndpoint::elMysqlEndpoint(void) : p_mysql_session_(NULL)
{
}

elMysqlEndpoint::~elMysqlEndpoint(void)
{
}



bool elMysqlEndpoint::Submit(string el_command_type, string el_command_id, string str_command, string& entity, elRecord& field_name_arr, elRecordList& records)
{
    ciDEBUG(5, ("Submit(%s, %s, %s)", el_command_type.c_str(), el_command_id.c_str(), str_command.c_str()));

    entity_ = entity;
    field_name_arr_ = field_name_arr;

    if (el_command_type == "SELECT") {
        if (Select(str_command)) {
            if (!GetRecordList(records)) {
                ciDEBUG(5, ("Execute %s! CommandID(%s), GetRecordList(%d) exec Command(%s)", el_command_type.c_str(), el_command_id.c_str(), records.size(), str_command.c_str()));
                return false;
            }
        } else {
            return false;
        }
    } else if (el_command_type == "SEQUENTIAL") {
        if (!ExecuteSequential(str_command, records)) {
            ciERROR(("ExecuteSequential Failed %s! CommandID(%s), GetRecordList(%d) exec Command(%s)", el_command_type.c_str(), el_command_id.c_str(), records.size(), str_command.c_str()));
            return false;
        }
    } else if (el_command_type == "DELETE_INSERT") {
        if (!ExecuteDeleteInsert(str_command, records)) {
            ciERROR(("Execute Failed %s! CommandID(%s), GetRecordList(%d) exec Command(%s)", el_command_type.c_str(), el_command_id.c_str(), records.size(), str_command.c_str()));
            return false;
        }
    } else if (el_command_type == "INSERT") {
        if (!ExecuteInsert(str_command, records)) {
            ciERROR(("Execute Failed %s! CommandID(%s), GetRecordList(%d) exec Command(%s)", el_command_type.c_str(), el_command_id.c_str(), records.size(), str_command.c_str()));
            return false;
        } 
    //} else if (el_command_type == "BULK_INSERT") {
        //if (!p_endpoint->ExecuteBulkInsert(entity, records)) {
        //    ciERROR(("Execute Failed %s! CommandID(%s), GetRecordList(%d) exec Command(%s)", el_command_type.c_str(), el_command_id.c_str(), records.size(), str_command.c_str()));
        //    return false;
        //} 
    } else {
        ciERROR(("Unknown command type : %s, %s, %s", el_command_type.c_str(), el_command_id.c_str(), str_command.c_str()));
        return false;
    }

    return true;
}

bool elMysqlEndpoint::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elEndPointRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;

    db_type_ = "MYSQL";
    db_host_ = p_my_rule_->host;
    db_port_ = p_my_rule_->port;
    db_name_ = p_my_rule_->group;
    db_user_ = p_my_rule_->user;
    db_pass_ = p_my_rule_->pass;
    url_     = p_my_rule_->url;
    protocol_= p_my_rule_->protocol;
    return_type_ = p_my_rule_->return_type;

    
    return true;
}

bool elMysqlEndpoint::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    if (option == ADMIN_STATE_START_NO) {
        StartDB_();
        return true;
    } else if (option == ADMIN_STATE_START_ALL) {
        StartDB_();
        InitTimer("elEndpoint Timer", 60);
        StartTimer();
        StartThread();
    } else if (option == ADMIN_STATE_START_TIMER) {
        StartTimer();
    } else if (option == ADMIN_STATE_START_THREAD) {
        StartThread();
    } else if (option == ADMIN_STATE_START_CHILD) {
    } else {
        ciERROR(("Failed! Start option(%s)", option.c_str()));
        return false;
    }

    return true;
}

bool elMysqlEndpoint::Stop()
{
    ciDEBUG(5, ("Stop()"));

    StopTimer();
    StopThread();

    // Stop child
    for (unsigned int i = 0; i < child_vec_.size(); i++) {
        child_vec_[i]->Stop();
    }
    StopDB_();
    return true;
}


bool elMysqlEndpoint::Select(string str_sql)
{
    ciDEBUG(5, ("Select(%s)", str_sql.c_str()));

    if (!p_mysql_session_) {
        ciERROR(("p_mysql_session_ is NULL!!!"));
        return false;
    }

    if (p_mysql_session_->Select(str_sql) == false) {
        ciERROR(("p_mysql_session_ Select Failed!!!"));
        return false;
    }
    return true;
}

bool elMysqlEndpoint::GetRecordList(elRecordList& selected_records)
{
    ciDEBUG(5, ("GetRecordList()"));

    if (!p_mysql_session_) {
        ciERROR(("p_mysql_session_ is NULL!!!"));
        return false;
    }
    if (!p_mysql_session_->GetRecordList(selected_records)) {
        ciERROR(("p_mysql_session_ GetRecordList Failed!!!"));
        return false;
    }
    return true;
}

bool elMysqlEndpoint::Next(elRecord& record)
{
    ciDEBUG(5, ("Next()"));
    if (!p_mysql_session_) {
        ciERROR(("p_mysql_session_ is NULL!!!"));
        return false;
    }
    if (!p_mysql_session_->Next(record)) {
        return false;
    }
    return true;
}


bool elMysqlEndpoint::Execute(string str_sql)
{
    ciDEBUG(5, ("Execute(%s)", str_sql.c_str()));

    if (!p_mysql_session_) {
        ciERROR(("p_mysql_session_ is NULL!!!"));
        return false;
    }
    if (!p_mysql_session_->Execute(str_sql)) {
        ciERROR(("p_mysql_session_ Execute Failed!!!"));
        return false;
    }
    return true;
}

bool elMysqlEndpoint::ExecuteSequential(string str_sql, elRecordList& values)
{
    ciDEBUG(5, ("ExecuteSequential(%s, %d)", str_sql.c_str(), values.size() ));

    int error_num = -1;

    if (!p_mysql_session_) {
        ciERROR(("elSession is NULL!!!"));
        return false;
    }

    error_num = p_mysql_session_->ExecuteSequential(str_sql, values);
    if (error_num < 0) {
        ciERROR(("elSession Failed ExecuteSequential!!! error=%d", error_num));
        return false;
    }
    return true;
}

// P1. INSERT 시 한글필드가 깨진다
// P2. INSERT 시 DATETIME 필드가 NULL이면 에러난다.
// P3. SET AUTOCOMMIT=0 를 사용못한다.
// P4. DELETE; INSERT; 쿼리를 한번에 못한다.
bool elMysqlEndpoint::ExecuteDeleteInsert(string str_sql, elRecordList& values)
{
    ciDEBUG(5, ("ExecuteDeleteInsert(%s)", str_sql.c_str()));

    if (!p_mysql_session_) {
        ciERROR(("p_mysql_session_ is NULL!!!"));
        return false;
    }
    if (!p_mysql_session_->ExecuteDeleteInsert(str_sql, values)) {
        ciWARN(("p_mysql_session_ ExecuteDeleteInsert Failed!!!"));
        return false;
    }
    return true;
}

bool elMysqlEndpoint::ExecuteInsert(string str_sql, elRecordList& values)
{
    ciDEBUG(5, ("ExecuteInsert(%s)", str_sql.c_str()));

    if (!p_mysql_session_) {
        ciERROR(("p_mysql_session_ is NULL!!!"));
        return false;
    }
    if (!p_mysql_session_->ExecuteInsert(str_sql, values)) {
        return false;
    }
    return true;
}

bool elMysqlEndpoint::ExecuteBulkInsert(string table_name, elRecordList& values)
{
    ciDEBUG(5, ("ExecuteBulkInsert(%s)", table_name.c_str()));

    if (!p_mysql_session_) {
        ciERROR(("p_mysql_session_ is NULL!!!"));
        return false;
    }
    if (!p_mysql_session_->ExecuteBulkInsert(table_name, values)) {
        return false;
    }
    return true;
}

void elMysqlEndpoint::StartDB_()
{
    ciDEBUG(5, ("StartDB_()"));

    if (p_mysql_session_ == NULL) {
        p_mysql_session_ = new elMysqlSession; 
        p_mysql_session_->StartDB(db_host_.c_str(), db_port_, db_name_.c_str(), db_user_.c_str(), db_pass_.c_str());
    }
}

void elMysqlEndpoint::StopDB_()
{
    ciDEBUG(5, ("StopDB_()"));

    if (p_mysql_session_) {
        p_mysql_session_->StopDB();
        delete p_mysql_session_;
        p_mysql_session_ = NULL;
    }
}


/// START TRANSACTION ~ COMMIT|ROLLBACK TEST
#if 0 
bool elMysqlEndpoint::ExecuteDeleteInsert(string str_sql, elRecordList& values)
{
    ciDEBUG(5, ("ExecuteDeleteInsert(%s)", str_sql.c_str()));

    if (str_sql == "") {
        ciERROR(("ExecuteDeleteInsert() SQL is NULL!!!"));
        return false;
    }
    if (!VerifyDB_()) {
        return false;
    }

    //[ Test
    if (!Execute(" START TRANSACTION ")) {
        ciERROR(("Failed! START TRANSACTION"));
        return false;
    }
    ciDEBUG(5, ("Success! START TRANSACTION 1"));

    if (!Execute(" DELETE FROM test.IOT_BEACON_TEST WHERE BEACONID='00:18:9A:25:E1:33' ")) {
        ciERROR(("Failed! DELETE FROM test.IOT_BEACON_TEST"));
        Execute("ROLLBACK");
        return false;
    }
    ciDEBUG(5, ("Success! DELETE FROM test.IOT_BEACON_TEST"));

    if (!Execute(" COMMIT ")) {
        ciERROR(("Failed! COMMIT"));
        return false;
    }
    ciDEBUG(5, ("Success! COMMIT test.IOT_BEACON_TEST"));
    ////////////////

    if (!Execute(" START TRANSACTION ")) {
        ciERROR(("Failed! START TRANSACTION"));
        return false;
    }
    ciDEBUG(5, ("Success! START TRANSACTION 2"));

    if (!Execute(" DELETE FROM test.IOT_BEACON_TEST WHERE BEACONID='00:18:9A:25:E1:22' ")) {
        ciERROR(("Failed! DELETE FROM test.IOT_BEACON_TEST"));
        Execute("ROLLBACK");
        return false;
    }
    ciDEBUG(5, ("Success! DELETE FROM test.IOT_BEACON_TEST WHERE BEACONID='00:18:9A:25:E1:22' "));

    if (!Execute(" ROLLBACK ")) {
        ciERROR(("Failed! ROLLBACK"));
        return false;
    }
    ciDEBUG(5, ("Success! ROLLBACK test.IOT_BEACON_TEST"));
    //]

    return true;
}

// EXAM)
//START TRANSACTION;
//INSERT INTO test.IOT_BEACON_TEST VALUES  ('0','SE6255','00:18:9A:25:E1:22','2','1','1','50','','','','','','2015-12-11 16:53:05','','2015-12-11 16:53:05','???','0.00','0.00','0.00','2015-12-11 16:53:05','a7c3e051-0dab-4ed0-ba31-d583b35ccd61','6255','7');
//INSERT INTO test.IOT_BEACON_TEST VALUES  ('0','SE6255','00:18:9A:25:E1:33','2','1','1','50','','','','','','2015-12-11 16:53:05','','2015-12-11 16:53:05','???','0.00','0.00','0.00','2015-12-11 16:53:05','a7c3e051-0dab-4ed0-ba31-d583b35ccd61','6255','7');
//INSERT INTO test.IOT_BEACON_TEST VALUES  ('0','SE6255','00:18:9A:25:E1:44','2','1','1','50','','','','','','2015-12-11 16:53:05','','2015-12-11 16:53:05','???','0.00','0.00','0.00','2015-12-11 16:53:05','a7c3e051-0dab-4ed0-ba31-d583b35ccd61','6255','7');
//INSERT INTO test.IOT_BEACON_TEST VALUES  ('0','SE6255','00:18:9A:25:E1:55','2','1','1','50','','','','','','2015-12-11 16:53:05','','2015-12-11 16:53:05','???','0.00','0.00','0.00','2015-12-11 16:53:05','a7c3e051-0dab-4ed0-ba31-d583b35ccd61','6255','7');
//COMMIT; #ROLLBACK;

#endif