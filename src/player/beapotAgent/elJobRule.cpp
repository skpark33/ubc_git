#include "elJobRule.h"
#include "elCommandRule.h"

ciSET_DEBUG(5, "elJobRule");

elJobRule::elJobRule(string strRuleKeyword)
    : p_job_obj_(NULL), p_parent_rule_(NULL), p_top_rule_(NULL)
{
    rule_keyword = strRuleKeyword;
}

elJobRule::~elJobRule(void)
{
}

bool elJobRule::Set(JsonObject* pJobObj, elRule* pParentRule, elRule* pComposedRule)
{
    ciDEBUG(5, ("Set()"));

    JsonValue*  temp_value = NULL;

    if (!pJobObj) {
        return false;
    }
    if (!pJobObj->isObject()) {
        return false;
    }
    p_job_obj_ = pJobObj;
    p_parent_rule_ = pParentRule;
    p_top_rule_ = pComposedRule;

    id = job_id = p_job_obj_->getString("JobID");
    if (job_id == "") {
        return false;
    }
    type = job_type = p_job_obj_->getString("JobType");
    if (job_type == "") {
        return false;
    }
    name = job_name = p_job_obj_->getString("JobName");
    parent_id = (pParentRule == NULL) ? "" : pParentRule->id;

    oper_state = p_job_obj_->getString("OperState");
    if (oper_state == "") {
        oper_state = "0";
    }
    admin_state = p_job_obj_->getString("AdminState");
    if (admin_state == "") {
        admin_state = "0";
    }
    schedule = p_job_obj_->getString("Schedule");

    // Add to Top Rule!
    p_top_rule_->Add(job_id, this);

    // FromCommandRule
    temp_value = p_job_obj_->get(EL_KEYWORD_FROM_COMMAND_RULE);
    if (!temp_value->isArray()) {
        return false;
    }
    p_from_rule_arr_ = (JsonArray*) temp_value;

    if (!SetCommandRule_(EL_KEYWORD_FROM_COMMAND_RULE, p_from_rule_arr_)) {
        return false;
    }

    // ToCommandRule
    temp_value = p_job_obj_->get(EL_KEYWORD_TO_COMMAND_RULE);
    if (!temp_value->isArray()) {
        return false;
    }
    p_to_rule_arr_ = (JsonArray*) temp_value;

    if (!SetCommandRule_(EL_KEYWORD_TO_COMMAND_RULE, p_to_rule_arr_)) {
        return false;
    }

    return true;
}

elRuleIdMap elJobRule::GetChildRuleMap(string keyword)
{
    if (keyword == EL_KEYWORD_FROM_COMMAND_RULE) {
        return from_rule_map_;
    } else if (keyword == EL_KEYWORD_TO_COMMAND_RULE) {
        return to_rule_map_;
    }

    // default
    return to_rule_map_;
}

bool elJobRule::SetCommandRule_(string strCommandKeyword, JsonArray* pCommandRuleArr)
{
    if (!pCommandRuleArr) {
        ciERROR(("Failed! pCommandRuleArr is NULL "));
        return false;
    }

    ciDEBUG(5, ("SetFromCommandRule_(%s) pCommandRuleArr count : %d ", strCommandKeyword.c_str(), pCommandRuleArr->getCount() ));

    JsonObject*   command_rule_obj = NULL;
    elCommandRule*  p_command_rule = NULL;

    //elRuleIdMap job_rule_map;
    for (int i = 0; i < pCommandRuleArr->getCount(); i++) {
        command_rule_obj = (JsonObject*) pCommandRuleArr->get(i);
        if (command_rule_obj == NULL) {
            ciERROR(("Failed! pCommandRuleArr[%d] is NULL ", i));
            return false;
        }
        // new elCommandRule
        p_command_rule = new elCommandRule(i, rule_keyword);
        if (!p_command_rule->Set(command_rule_obj, this, p_top_rule_)) {
            ciERROR(("Failed! pCommandRuleArr[%d] Command Rule Set", i));
            return false;
        }

        // Make up elCommandRule Map
        if (strCommandKeyword == EL_KEYWORD_FROM_COMMAND_RULE) {
            from_rule_map_.insert(elRuleIdMap::value_type(p_command_rule->command_id, p_command_rule));
        } else if (strCommandKeyword == EL_KEYWORD_TO_COMMAND_RULE) {
            to_rule_map_.insert(elRuleIdMap::value_type(p_command_rule->command_id, p_command_rule));
        }
    }
    
    return true;
}
