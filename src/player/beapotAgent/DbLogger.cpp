#include "DbLogger.h"

#include <ci/libDebug/ciDebug.h>
#include <ci/libBase/ciTime.h>
#include "common/libCommon/ubcIni.h"

ciSET_DEBUG(5, "DbLogger");

ciMutex DbLogger::_sLock;
DbLogger* DbLogger::_instance = 0;

DbLogger* DbLogger::getInstance()
{
	if (!_instance) {
		CI_GUARD(_sLock, "getInstance()");
		if (!_instance) {
			_instance = new DbLogger;
		}
	}
	return _instance;
}

void DbLogger::clearInstance()
{
	if (_instance) {
		CI_GUARD(_sLock, "getInstance()");
		if (_instance) {
			delete _instance;
			_instance = 0;
		}
	}
}

DbLogger::DbLogger(void) : p_mysql_db_(NULL), started_(false)
{
    ciDEBUG(5, ("DbLogger()"));
    loadIni();
    StartDB();

    ciTime ctime;
    operstate_time_t_ = ctime.getTime();
}

DbLogger::~DbLogger(void)
{
    ciDEBUG(5, ("~DbLogger()"));
    StopDB();
}

void DbLogger::loadIni()
{
    //C:\\SQISoft\\UTV1.0\\execute\\data\\UBCVariables.ini
	ubcConfig aIni("UBCVariables");

	aIni.get("OFS", "DBTYPE", db_type_);
	aIni.get("OFS", "DBNAME", db_name_);
    aIni.get("OFS", "DBUSER", db_user_);
    aIni.get("OFS", "DBPASS", db_pass_);
    aIni.get("OFS", "DBHOST", db_host_);
    aIni.get("OFS", "DBPORT", db_port_);
    aIni.get("ROOT","SITEID", site_id_);

    ciDEBUG(5, ("loadIni(%s, %s, %s, %s) ", db_type_.c_str(), db_name_.c_str(), db_user_.c_str(), db_pass_.c_str() ));

    if (db_type_ == "") db_type_ = "MYSQL";
    if (db_name_ == "") db_name_ = "iot";
    if (db_user_ == "") db_user_ = "ubc";
    if (db_pass_ == "") db_pass_ = "sqicop";
    if (db_host_ == "") db_host_ = "127.0.0.1";
    if (db_port_ == 0 ) db_port_ = 3306;
    if (site_id_ == "") site_id_ = "SE";

	ciShort gapMin=10;
	aIni.get("OFS","BEACON_GAP_MIN", gapMin);
	if(gapMin == 0)	beacon_gap_min_ = 10;
    else beacon_gap_min_ = gapMin;
}

void DbLogger::StartDB()
{
    ciDEBUG(5, ("StartDB()"));

    if (p_mysql_db_ == NULL) {
        p_mysql_db_ = new ciMySQL(db_name_.c_str(), db_user_.c_str(), db_pass_.c_str(), db_host_.c_str(), db_port_);
    }

    {
    ciGuard aGuard(db_lock_);
    if (!p_mysql_db_->connect()) {
        ciERROR(("DbLogger Start Failed!!!"));
        return;
    }
    }//ciGuard

    if (!started_) {
        ciWARN2(("before DbLogger thread start()"));    
        started_ = true;
        start();
        ciWARN2(("after DbLogger thread start()"));
    }
}

void DbLogger::StopDB()
{
    ciWARN2(("Stop()"));

    if(started_) {
		started_ = false;
		stop(); 
	}

    if (p_mysql_db_ == NULL) {
        {
        ciGuard aGuard(db_lock_);

        p_mysql_db_->disconnect();
        delete p_mysql_db_;
        p_mysql_db_ = NULL;
        }//ciGuard
    }
}

void DbLogger::run()
{
    ciWARN2(("run()"));

    BEACON_SCAN_MSG beapot_data;
    GOODS_PICK_UPDOWN goods_pick_data;

    bool b_insert_check = false;
    bool b_update_check = false;

    while(started_) {
        // BeapotWorker에서 enqueue
        if (queue_.size() > 0)
        {
            {
            ciGuard aGuard(queue_lock_);
            beapot_data = queue_.front();
            queue_.pop_front();
            }
            ciDEBUG2(5, ("run1(queue_.size<%d>)", queue_.size()));

            // Business
            b_insert_check = insertCheckMap_(beapot_data);
            b_update_check = updateCheckMap_(beapot_data);

            if (b_update_check) {
                UpdateIotBeacon_(beapot_data);
                InsertIotBeaconFreezeLog_(beapot_data);
            }
            if (b_insert_check) {
                InsertIotBeaconLog_(beapot_data);
            }
        }  
        if(pick_queue_.size() > 0){ // WideActionService에서 enqueue
            ciDEBUG2(5, ("run2-1(pick_queue_.size<%d>)", pick_queue_.size()));
            {
            ciGuard aGuard(pick_queue_lock_);
            goods_pick_data = pick_queue_.front();
            pick_queue_.pop_front();
            }
            ciDEBUG2(5, ("run2-2(pick_queue_.size<%d>)", pick_queue_.size()));
            
            // iot_pickup_log Insert
            InsertIotPickupLog_(goods_pick_data);

        } else {
            updateIotBeaconOperstate_();
            Sleep(100); // 100ms
        }
    }
    ciDEBUG(5, ("run return #######################... "));

}

void DbLogger::Enqueue(BEACON_SCAN_MSG& data)
{
    ciDEBUG(5, ("Enqueue()"));

    {
    ciGuard aGuard(queue_lock_);
    queue_.push_back(data);
    }
    ciDEBUG(5, ("Enqueue() queue_ size:%d", queue_.size()));
}

void DbLogger::EnqueuePick(GOODS_PICK_UPDOWN& data)
{
    ciDEBUG(5, ("EnqueuePick()"));

    {
    ciGuard aGuard(pick_queue_lock_);
    pick_queue_.push_back(data);
    }
    ciDEBUG(5, ("EnqueuePick() pick_queue size:%d", pick_queue_.size()));
}

bool DbLogger::ExecuteDml_(const char* sz_sql)
{
    ciDEBUG(5, ("ExecuteDml_()"));

    if (!sz_sql) {
        ciERROR(("ExecuteDml_() SQL is NULL!!!"));
        return false;
    }

    if (!p_mysql_db_) {
        ciERROR(("ExecuteDml_() DB Object is NULL!!!"));
        return false;
    }

    {
    ciGuard aGuard(db_lock_);

    if (!p_mysql_db_->execute(sz_sql)) {
        p_mysql_db_->disconnect();
        delete p_mysql_db_;
        p_mysql_db_ = NULL;
        p_mysql_db_ = new ciMySQL(db_name_.c_str(), db_user_.c_str(), db_pass_.c_str(), db_host_.c_str(), db_port_);
        p_mysql_db_->connect();
        ciERROR(("ExecuteDml_() FAILED! Reconnected!!!"));
        return false;
    }

    } // ciGuard

    return true;
}

/// - CTIME, currentTime - lastUpdateTime이 beacon_gap_min_(10)*60초 이하인 경우만 map과 IOT_BEACON_LOG 에 삽입한다. 
/// - 10분에 한번 발송되는 10min period event는 빼기 위해서 lastUpdateTime이 beacon_gap_min_(10)*60초 초과인 경우는 map만 업데이트하고 IOT_BEACON_LOG에는 삽입하지 않는다. 
///  map을 업데이트하는 이유는 0.5sec action event가 딜레이 현상때문에 수신안되는 경우가 있을수도 있어 이런 경우 다시 lastUpdateTime을 갱신을 해야 insert 작업이 가능하다.
/// - 10분에 한번 발송되는 10min period event는 빼기 위해서
/// - map size가 0이면 첫 신호이다. insert
bool DbLogger::insertCheckMap_(BEACON_SCAN_MSG& data)
{
    ciDEBUG(5, ("insertCheckMap_()"));

    //map<string, time_t>  insert_check_map_; 
    ciTime cur_ctime(data.ctime);

    // find한 다음 없는 beaconId가 없는 경우 명확하게 map에 insert
    if (insert_check_map_.find(data.beacon_id) == insert_check_map_.end()) {
        insert_check_map_[data.beacon_id] = cur_ctime.getTime();
		return true ;
    }
     
    // beacon_gap_min_ 초과인 경우 lastUpdateTime만 갱신한다.
    if (insert_check_map_[data.beacon_id] + (beacon_gap_min_*60) < cur_ctime.getTime()) {
        // lastUpdateTime 갱신만 하고 db처리 안하도록 false리턴
        insert_check_map_[data.beacon_id] = cur_ctime.getTime();
        return false;
    }

    // cur와 lastUpdateTime 차이가 10초 이하인 경우 갱신 후 true 리턴
    insert_check_map_[data.beacon_id] = cur_ctime.getTime();

    return true;
}


/// - CTIME, currentTime - lastUpdateTime이 beacon_gap_min_ 초과인 경우만 map과 IOT_BEACON 에 업데이트한다. 
/// - 10분에 한번 발송되는 period event만 처리함.
/// - time_t가 0이면 첫 신호이다.  update
bool DbLogger::updateCheckMap_(BEACON_SCAN_MSG& data)
{
    ciDEBUG(5, ("updateCheckMap_()"));

    //map<string, time_t>  update_check_map_;    
    ciTime cur_ctime(data.ctime);

    // find한 다음 없는 beaconId가 없는 경우 명확하게 map에 insert
    if (update_check_map_.find(data.beacon_id) == update_check_map_.end()) {
        update_check_map_[data.beacon_id] = cur_ctime.getTime();
		return true;
    }
     
    // beacon_gap_min_(10분) 초과인 경우 lastUpdateTime만 갱신한다.
    if (update_check_map_[data.beacon_id] + (beacon_gap_min_*60) < cur_ctime.getTime()) {
        // lastUpdateTime 갱신만 하고 db처리 안하도록 false리턴
        update_check_map_[data.beacon_id] = cur_ctime.getTime();
        return true;
    }

    return false;
}


bool DbLogger::InsertIotBeaconLog_(BEACON_SCAN_MSG& data)
{
    ciDEBUG(5, ("InsertIotBeaconLog()"));

    char sz_sql[4096];
    memset(sz_sql, 0x00, 4096);

    string mgr_id("0");

    sprintf(sz_sql, " INSERT INTO IOT.IOT_BEACON_LOG(mgrId, siteId, scannerId, beaconId, rssi, xiro_x,xiro_y,xiro_z, battery, ctime) " 
                    " VALUES('%s', '%s', '%s', '%s', '%ld', '%ld','%ld','%ld', '%ld', STR_TO_DATE('%s', '%%Y-%%m-%%d %%H:%%i:%%S') ) "
                    , mgr_id.c_str(), site_id_.c_str(), data.scanner_id.c_str(), data.beacon_id.c_str(), data.rssi
                    , data.xiro_x, data.xiro_y, data.xiro_z, data.battery 
                    , data.ctime.c_str()
                    );

    if (!ExecuteDml_(sz_sql)) {
        ciERROR(("InsertIotBeaconLog FAILED!!! SQL:%s", sz_sql));
        return false;
    }
    ciDEBUG(5, ("InsertIotBeaconLog SUCCESS!!! SQL:%s", sz_sql));

    return true;
}

bool DbLogger::UpdateIotBeacon_(BEACON_SCAN_MSG& data)
{
    ciDEBUG(5, ("UpdateIotBeacon_()"));

    char sz_sql[4096];
    memset(sz_sql, 0x00, 4096);

    string mgr_id("0");
    string payload("");

    sprintf(sz_sql, " UPDATE IOT.IOT_BEACON "
                    " SET    battery = %d, lastConnectTime = STR_TO_DATE('%s', '%%Y-%%m-%%d %%H:%%i:%%S') "
                    " WHERE  beaconId = '%s' "
                    , data.battery
                    , data.ctime.c_str()
                    , data.beacon_id.c_str()
                    );

    if (!ExecuteDml_(sz_sql)) {
        ciERROR(("UpdateIotBeacon_ FAILED!!! SQL:%s", sz_sql));
        return false;
    }
    ciDEBUG(5, ("UpdateIotBeacon_ SUCCESS!!! SQL:%s", sz_sql));

    return true;
}

void DbLogger::updateIotBeaconOperstate_()
{
    ciDEBUG(5, ("updateIotBeaconOperstate_()"));

    time_t cur_time_t;
    ciTime ctime;
    cur_time_t = ctime.getTime();
    // 1시간에 한번 체크, 이전시간이 3600초전이면 return
    // 2016-05-04 30분에 한번 체크로 수정
    if ((cur_time_t - operstate_time_t_) < 1800) {
        return;
    }
    operstate_time_t_ = cur_time_t; // 1시간 되면 현재시간으로 재등록

    char sz_sql[4096];
    memset(sz_sql, 0x00, 4096);

    string mgr_id("0");
    string payload("");

    sprintf(sz_sql, " UPDATE IOT.IOT_BEACON "
                    " SET OPERSTATE = case when LASTCONNECTTIME > (now() - interval 1 hour) then 1 else 0 end "
                    );

    if (!ExecuteDml_(sz_sql)) {
        ciERROR(("updateIotBeaconOperstate_ FAILED!!! SQL:%s", sz_sql));
        return;
    }
    ciWARN2(("updateIotBeaconOperstate_ SUCCESS!!! SQL:%s", sz_sql));
}

bool DbLogger::InsertIotBeaconFreezeLog_(BEACON_SCAN_MSG& data)
{
    ciDEBUG(5, ("InsertIotBeaconFreezeLog_()"));

    char sz_sql[4096];
    memset(sz_sql, 0x00, 4096);

    string mgr_id("0");

    sprintf(sz_sql, " INSERT INTO IOT.IOT_BEACON_FREEZE_LOG(mgrId, beaconId, ctime, xiro_x, xiro_y, xiro_z) " 
                    " VALUES('%s', '%s', STR_TO_DATE('%s', '%%Y-%%m-%%d %%H:%%i:%%S'), '%ld', '%ld','%ld' ) "
                    , mgr_id.c_str(), data.beacon_id.c_str(), data.ctime.c_str()
                    , data.xiro_x, data.xiro_y, data.xiro_z                    
                    );

    if (!ExecuteDml_(sz_sql)) {
        ciERROR(("InsertIotBeaconFreezeLog_ FAILED!!! SQL:%s", sz_sql));
        return false;
    }
    ciDEBUG(5, ("InsertIotBeaconFreezeLog_ SUCCESS!!! SQL:%s", sz_sql));

    return true;
}

bool DbLogger::InsertIotPickupLog_(GOODS_PICK_UPDOWN& data)
{
    char sz_sql[4096];
    memset(sz_sql, 0x00, 4096);
    

    ciTime pickup_ci_time(data.pickup_time); //#define UNIX_DATE_FORMAT_C	"%04d/%02d/%02d %02d:%02d:%02d"
    ciTime pickdown_ci_time(data.pickdown_time);
    int pickup_sec = (int) data.pickdown_time - (int) data.pickup_time;

    ciDEBUG(5, ("InsertIotPickupLog_(%s, %s, %s, %s, %d)", data.beacon_id.c_str(), data.goods_id.c_str(), pickup_ci_time.getTimeString().c_str(), pickdown_ci_time.getTimeString().c_str(), data.pickdown_time - data.pickup_time));

    sprintf(sz_sql, 
            " INSERT INTO IOT.IOT_PICKUP_LOG(mgrId, siteId, beaconId, goodsId, zoneId, pickupTime, pickdownTime, pickupSec) " 
            " SELECT mgrId, siteId, beaconId, goodsId, zoneId "
            "      , STR_TO_DATE('%s', '%%Y/%%m/%%d %%H:%%i:%%S') AS pickupTime " 
            "      , STR_TO_DATE('%s', '%%Y/%%m/%%d %%H:%%i:%%S') AS pickdownTime " 
            "      , %d AS pickupSec "
            " FROM  iot_goods_map "
            " WHERE beaconId = '%s' "
            , pickup_ci_time.getTimeString().c_str()
            , pickdown_ci_time.getTimeString().c_str()
            , pickup_sec
            , data.beacon_id.c_str()
            );

    if (!ExecuteDml_(sz_sql)) {
        ciERROR(("InsertIotPickupLog_ FAILED!!! SQL:%s", sz_sql));
        return false;
    }
    ciDEBUG(5, ("InsertIotPickupLog_ beaconId[%s], goodsId[%s] SUCCESS!!! SQL:%s", data.beacon_id.c_str(), data.goods_id.c_str(), sz_sql));

    return true;
}