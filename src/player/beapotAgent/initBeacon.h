#ifndef _initBeacon_h_
#define _initBeacon_h_

#include "actionManager.h"

class AccelData {
public:
	AccelData () : x(0),y(0),z(0)/*,ctime(0),worth(ciFalse)*/{}
	AccelData (ciLong px,ciLong py,ciLong pz) : x(px),y(py),z(pz)/*,ctime(0),worth(ciFalse)*/{}
	void set(ciLong px,ciLong py,ciLong pz) { x=px;y=py;z=pz; }
	AccelData& operator= (const AccelData& rValue)
	{
		x = rValue.x;
		y = rValue.y;
		z = rValue.z;
		return *this;
	}

	ciLong x;
	ciLong y;
	ciLong z;
	//time_t ctime;
	//ciBoolean	worth;  // worth to write
};
//typedef multimap<time_t, AccelData>	AccelMap;  // key is abs(x)+abs(y)+abs(z) __int64 type
//typedef list<AccelData*>	AccelList;  


class InitEvalData {
public:
	InitEvalData() : mid_x(-1),mid_y(-1),mid_z(-1), axis_z(0), updateTime(0), dirtyFlag(ciFalse), accelCount(0) {}
	virtual ~InitEvalData() {}

	//AccelMap		accelMap;
	//AccelList		accelList;
	AccelData		first;
	AccelData		last;
	int				accelCount;

	ciLong			mid_x;
	ciLong			mid_y;
	ciLong			mid_z;

	ciBoolean		dirtyFlag;
	int				axis_z;
	time_t			updateTime;

	//ciBoolean	evaluate(const char* beaconId, int sampleCount);
	ciBoolean	evaluate(const char* beaconId, int sampleCount, int stddev , ciLong x, ciLong y, ciLong z);
	ciBoolean	evaluateAxis();
	void		setFreezValue(const char*beaconId,ciLong x, ciLong y, ciLong z);

	//ciBoolean 	loadFreezeDB(const char* beaconId, ciBoolean alreadyDone, int sampleCount);
	ciBoolean 	updateBeaconDB(const char* beaconId);
	ciBoolean	createBeaconDB(const char* beaconId);

protected:

};
typedef map<ciString, InitEvalData*>	InitEvalMap;

class initBeacon : public ciWinPollable
{
public:
	static initBeacon*	getInstance();
	static void	clearInstance();

	virtual ~initBeacon() {}

	virtual void processExpired(ciString name, int counter, int interval);

	int		 	loadBeaconDB();
	ciBoolean	normalize(const char* beaconId, ciLong& x, ciLong& y, ciLong& z, ciShort& axis);

	ciBoolean isNormalizeMode() { return _normalize; }

	const char*	getAppPath();

	const char* getSiteId() { return _siteId.c_str(); }

protected:
	initBeacon(); 

	void 		_clear();
	int			_writeFreezeDB();

	static initBeacon*	_instance;
	static ciMutex		_instanceLock;

	ciBoolean			_normalize;
	ciShort				_sampleCount;
	ciLong				_stdDev;
	ciString			_mainDirectory;

	ciString		_siteId;


	InitEvalMap		_map;
	ciMutex			_mapLock;


};

#endif // _initBeacon_h_
