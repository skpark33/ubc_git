/** \class elCommandRule
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elCommandRule_h_
#define _elCommandRule_h_

#include "elRule.h"

class elCommandRule : public elRule
{
public:
    elCommandRule(int nCommandSeq, string strRuleKeyword = EL_KEYWORD_DEFAULT_RULE);
    virtual ~elCommandRule(void);

    virtual bool    Set(JsonObject* pCommandObj, elRule* pParentRule, elRule* pComposedRule);
    elRuleIdMap     GetChildRuleMap(string keyword = EL_KEYWORD_FROM_COMMAND_RULE);

public:
    string          command_id;
    string          command_type;
    string          command_name;

    string          partner_command_id;
    string          end_point;
    string          entity;
    elRuleConstVec  field_arr_vec;
    elRuleConstVec  field_type_vec;
    string          command;
    string          property_id;
    elRuleConstVec  argv_vec;

protected:
    JsonObject*     p_command_obj_;
    elRule*         p_top_rule_;
    elRule*         p_parent_rule_;
    elRuleIdMap     child_rule_map_;

};

#endif // _elCommandRule_h_