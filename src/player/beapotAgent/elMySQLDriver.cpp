#include "elMySQLDriver.h"
#include <ci/libDebug/ciDebug.h>

ciSET_DEBUG(5, "elMySQLDriver");

elMySQLDriver::elMySQLDriver(const char* pDbName, const char* pDbUser, const char* pDbPass, const char* pHost, int pPort) 
{
	ciDEBUG(3, ("elMySQLDriver()"));

	_res = 0;
	_fields = 0;
	_num_fields = 0;
	_row = 0;

    _dbName = pDbName;
    _dbUser = pDbUser;
    _dbPasswd = pDbPass;
	_dbHost = pHost;
	_dbPort = pPort;

	ciDEBUG(3, ("elMySQLDriver(%s,%s,%s,%s,%d)",
					_dbName.c_str(),_dbUser.c_str(),_dbPasswd.c_str(),_dbHost.c_str(), _dbPort));

	_mysql = new MYSQL;
	ciGuard aGuard(_lock);
    if(!mysql_init(_mysql)) {
		ciERROR(("session init failed"));
		_mysql = 0;
	}
	_connected = ciFalse;

}

elMySQLDriver::~elMySQLDriver() 
{
    ciDEBUG(3, ("~elMySQLDriver()"));
	if (_connected) disconnect();
}

void
elMySQLDriver::_closeSession()
{
    ciDEBUG(3, ("_closeSession()"));
	if(_mysql) {
		_clearResult();
		mysql_close(_mysql);
		delete _mysql;  // 20100628 skpark memory leak
		_mysql = 0;
	}
	_connected = ciFalse;
}

void
elMySQLDriver::_clearResult()
{
    ciDEBUG(3, ("_clearResult()"));
	
	//skpark 2013.10.1
	while(mysql_more_results(_mysql))
	{
		if(mysql_next_result(_mysql))
		{
			MYSQL_RES* sql_result = mysql_use_result(_mysql);
            if(sql_result) 	{
                mysql_free_result(sql_result);
            }
		}
	}
	//
	
	if(_res) { 
		mysql_free_result( _res ) ; 
		_res = 0; 
		_fields = 0;
		_num_fields = 0;
		_row = 0;
	} 

    _field_infos.clear();
}

ciBoolean
elMySQLDriver::connect() 
{
	ciDEBUG(3, ("connect(%s,%s,%s)", (char *) _dbName.c_str(), (char *) _dbUser.c_str(), (char *) _dbPasswd.c_str()));

	ciGuard aGuard(_lock);
	if(_connected) {
		ciWARN(("my sql session already connected"));
		return ciTrue;
	}

    if(!mysql_real_connect(_mysql, 
							_dbHost.c_str(),
							_dbUser.c_str(),
							_dbPasswd.c_str(), 
							_dbName.c_str(), 
							_dbPort, (char *)NULL, CLIENT_MULTI_RESULTS)) {

        ciERROR(("connect error : %d,%s\n",mysql_errno(_mysql), mysql_error(_mysql)));
		_closeSession();
		return ciFalse;
    }

	_connected = ciTrue;

    // SET NAMES EUCKR;
    // UTF8 String 읽기
    _execute(" SET NAMES UTF8 ");

	ciDEBUG(3, ("connected"));
	return ciTrue;
}

ciBoolean
elMySQLDriver::disconnect() 
{
	ciDEBUG(3, ("disconect()"));
	ciGuard aGuard(_lock);
	if(!_connected) {
		ciWARN(("my sql session already disconnected"));
		return ciTrue;
	}
	_closeSession();
	return ciTrue;
}

ciBoolean
elMySQLDriver::execute(const char* pSql) 
{
	if(_execute(pSql)) {
		unsigned long long num_rows = mysql_affected_rows( _mysql ) ;
		ciDEBUG(3,("Done : %d rows affected",num_rows));
		return ciTrue;
	}
	return ciFalse;
}

ciBoolean
elMySQLDriver::_execute(const char* pSql) 
{
	ciGuard aGuard(_lock);
	_clearResult();
    //if(mysql_query(_mysql, pSql)) 
	if(mysql_real_query(_mysql, pSql, (unsigned long) strlen(pSql)))
	{
        ciERROR(("execute error : %d,%s - SQL[%s]",mysql_errno(_mysql), mysql_error(_mysql), pSql));
        return ciFalse ;
    }
    ciDEBUG(5,("%s query executed", pSql));
	return ciTrue;
}


//
// Error발생 -> execute error : 2014,Commands out of sync; you can't run this command now - SQL[SELECT DATE_FORMAT(MAX(lastUpdateTime), '%Y-%m-%d %H:%i:%S') AS lastUpdateTime, COUNT(*) AS CNT FROM iot.IOT_BEACON WHERE BINARY siteId LIKE 'LC4855%' ]
//
//You need to clear out your result set on any queries that will return results. If you're not using the results then just call:
//
//  MYSQL_RES *results;
//  results = mysql_store_result(localConn);
//  mysql_free_result(results);
//
//To use your results just call mysql_store_result (or mysql_use_result) after queries 
//that return results and be sure to use mysql_free_result on them at some point later. 
//This should clear up any problems with CR_COMMANDS_OUT_OF_SYNC errors.
//
//
//From the documentation for mysql_store_result (emphasis added):
//  After invoking mysql_query() or mysql_real_query(), you must call mysql_store_result() or mysql_use_result() 
//  for every statement that successfully produces a result set (SELECT, SHOW, DESCRIBE, EXPLAIN, CHECK TABLE, and so forth). 
//  You must also call mysql_free_result() after you are done with the result set.
ciBoolean
elMySQLDriver::next()
{
	ciDEBUG(5,("next"));
	if(!_res) {
		_res = mysql_store_result( _mysql ) ;
		if(_res) {
			_num_fields = mysql_num_fields(_res);
			_fields =  mysql_fetch_fields(_res);

			elDbFieldInfo db_field_info;
			for(int i = 0; i < _num_fields; i++) {                
				// 2015-12-30 name, db, def, catalog 등은 길이를 확인하고 버퍼에 복사하여 string에 대입해야 한다.
				//            아니면 단말에 따라 죽을 때가 있다. 그런데 길이, 값이 이상하다....
				//            현재는 필요없어 주석처리하니 해결됨.

				//ciDEBUG(5,("[%d] db_field_info name : %d", i, _fields[i].name_length));
                //db_field_info.name          = (_fields[i].name_length ? _fields[i].name : "");
				
				//ciDEBUG(5,("[%d] db_field_info org_name : %d", i));                
				//db_field_info.org_name      = (_fields[i].org_name ? _fields[i].org_name : "");
				
				//ciDEBUG(5,("[%d] db_field_info table : %d", i));                
				//db_field_info.table         = (_fields[i].table ? _fields[i].table : "");

				//ciDEBUG(5,("[%d] db_field_info org_table : %d", i));
                //db_field_info.org_table     = (_fields[i].org_table ? _fields[i].org_table : "");

				//ciDEBUG(5,("[%d] db_field_info db : %d", i));
                //db_field_info.db            = (_fields[i].db ? _fields[i].db : "");

				//ciDEBUG(5,("[%d] db_field_info catalog : %d", i));
                //db_field_info.catalog       = (_fields[i].catalog ? _fields[i].catalog : "");

				//ciDEBUG(5,("[%d] db_field_info def : %d", i));
                //db_field_info.def           = (_fields[i].def ? _fields[i].def : "");

				ciDEBUG(5,("[%d] db_field_info length : %d", i, _fields[i].length ));
                db_field_info.length        = _fields[i].length;

				ciDEBUG(5,("[%d] db_field_info max_length : %d", i, _fields[i].max_length));
                db_field_info.max_length    = _fields[i].max_length;

				ciDEBUG(5,("[%d] db_field_info name_length : %d", i, _fields[i].name_length));
                db_field_info.name_length   = _fields[i].name_length;

				ciDEBUG(5,("[%d] db_field_info org_name_length : %d", i, _fields[i].org_name_length));
                db_field_info.org_name_length = _fields[i].org_name_length;

				ciDEBUG(5,("[%d] db_field_info table_length : %d", i, _fields[i].table_length));
                db_field_info.table_length  = _fields[i].table_length;

				ciDEBUG(5,("[%d] db_field_info db_length : %d", i, _fields[i].db_length));
                db_field_info.db_length     = _fields[i].db_length;

				ciDEBUG(5,("[%d] db_field_info catalog_length : %d", i, _fields[i].catalog_length));
                db_field_info.catalog_length= _fields[i].catalog_length;

				ciDEBUG(5,("[%d] db_field_info def_length : %d", i, _fields[i].def_length));
                db_field_info.def_length    = _fields[i].def_length;

				ciDEBUG(5,("[%d] db_field_info flags : %d", i, _fields[i].flags));
                db_field_info.flags         = _fields[i].flags;

				ciDEBUG(5,("[%d] db_field_info decimals : %d", i, _fields[i].decimals));
                db_field_info.decimals      = _fields[i].decimals;

				ciDEBUG(5,("[%d] db_field_info charsetnr : %d", i, _fields[i].charsetnr));
                db_field_info.charsetnr     = _fields[i].charsetnr;

				db_field_info.type          = _getFieldType(_fields[i].type);
				ciDEBUG(5,("[%d] db_field_info type : %d, %s", i, _fields[i].type, db_field_info.type.c_str()));
				
                _field_infos.push_back(db_field_info);
			}
		}
	}
	if(_res) {
		_row = mysql_fetch_row(_res);
		ciDEBUG(3,("rows ciTrue"));

		if(_row) {
			return ciTrue;
		}
		ciDEBUG(3,("end of rows"));
		return ciFalse;
	}

	if( mysql_field_count( _mysql ) != 0) {
		ciERROR(("fetch failed : %s",mysql_error( _mysql)));
	}else{
		ciDEBUG(1,("no result"));
	}
	return ciFalse;
}


const char*
elMySQLDriver::getValueString(int idx)
{
	ciDEBUG(6,("getValue(%d/%d)",idx,_num_fields));
	if(_row && idx < _num_fields) {
		return (const char*) _row[idx];
	}
	ciERROR(("invalid index(%d/%d) or fetch failed", idx, _num_fields));
	return 0;
}

elField elMySQLDriver::getField(int idx)
{
	ciDEBUG(5,("getField(%d/%d)",idx,_num_fields));

    elField db_field;
    db_field.name = "";
    db_field.value = "";
    db_field.type = "";
    db_field.is_exist = false;

    if(_row == NULL) {
        return db_field;
    }

	if(idx < _num_fields) {
        db_field.name   = _field_infos[idx].name;
        db_field.value  = (_row[idx] == NULL ? "" : _row[idx]);
        db_field.type   = _field_infos[idx].type;
        db_field.is_exist = true;

        //default converting
        if (db_field.value == "") {
            if (db_field.type == EL_FIELD_TYPE_DATETIME) {
                db_field.value = "1970-01-01 00:00:00";
            } else if (db_field.type != EL_FIELD_TYPE_VAR_STRING) { 
                if (db_field.type != EL_FIELD_TYPE_STRING) {
                    db_field.value = "NULL";
                }
            }
        } else {
            // 따옴표 이스케이프 처리
            string quote("'");
            string escapeQuote("''");
            ciStringUtil::stringReplace(db_field.value, quote.c_str(), escapeQuote.c_str());
        }        

        ciDEBUG(5, ("getField[%d] %s:%s:%s", idx, db_field.name.c_str(), db_field.value.c_str(), db_field.type.c_str()) );

		return db_field;
	}

    // 2016-01-22 add. you must test! - mysql_free_result(_res);
    if(_res) { 
	    mysql_free_result( _res ) ; 
	    _res = 0; 
	    _fields = 0;
	    _num_fields = 0;
	    _row = 0;
    } 
    _field_infos.clear();

    ciERROR(("invalid index(%d/%d) or fetch failed", idx, _num_fields));
    return db_field;
}


string elMySQLDriver::_getFieldType(int type)
{
    string str_type("");

	if (type   		 == CLIENT_MULTI_STATEMENTS) {  
		str_type = "CLIENT_MULTI_STATEMENTS";
	} else if (type == FIELD_TYPE_DECIMAL     ) {
		str_type = "DECIMAL";
	} else if (type == FIELD_TYPE_NEWDECIMAL  ) {
		str_type = "NUMERIC";
	} else if (type == FIELD_TYPE_TINY        ) {
		str_type = "TINYINT";
	} else if (type == FIELD_TYPE_SHORT       ) {
		str_type = "SMALLINT";
	} else if (type == FIELD_TYPE_LONG        ) {
		str_type = "INTEGER";
	} else if (type == FIELD_TYPE_FLOAT       ) {
		str_type = "FLOAT";
	} else if (type == FIELD_TYPE_DOUBLE      ) {
		str_type = "DOUBLE";
	} else if (type == FIELD_TYPE_NULL        ) {
		str_type = "NULL";
	} else if (type == FIELD_TYPE_TIMESTAMP   ) {
		str_type = "TIMESTAMP";
	} else if (type == FIELD_TYPE_LONGLONG    ) {
		str_type = "BIGINT";
	} else if (type == FIELD_TYPE_INT24       ) {
		str_type = "MEDIUMINT";
	} else if (type == FIELD_TYPE_DATE        ) {
		str_type = "DATE";
	} else if (type == FIELD_TYPE_TIME        ) {
		str_type = "TIME";
	} else if (type == FIELD_TYPE_DATETIME    ) {
		str_type = "DATETIME";
	} else if (type == FIELD_TYPE_YEAR        ) {
		str_type = "YEAR";
	} else if (type == FIELD_TYPE_NEWDATE     ) {
		str_type = "NEWDATE";
	} else if (type == FIELD_TYPE_ENUM        ) {
		str_type = "ENUM";
	} else if (type == FIELD_TYPE_SET         ) {
		str_type = "SET";
	} else if (type == FIELD_TYPE_TINY_BLOB   ) {
		str_type = "TINYBLOB";
	} else if (type == FIELD_TYPE_MEDIUM_BLOB ) {
		str_type = "MEDIUMBLOB";
	} else if (type == FIELD_TYPE_LONG_BLOB   ) {
		str_type = "LONGBLOB";
	} else if (type == FIELD_TYPE_BLOB        ) {
		str_type = "BLOB";
	} else if (type == FIELD_TYPE_VAR_STRING  ) {
		str_type = "VARCHAR";
	} else if (type == FIELD_TYPE_STRING      ) {
		str_type = "STRING";
	} else if (type == FIELD_TYPE_TINY        ) {
		str_type = "TINYINT";
	} else if (type == FIELD_TYPE_INTERVAL ) {
		str_type = "INTERVAL";
	} else if (type == FIELD_TYPE_GEOMETRY    ) {
		str_type = "GEOMETRY";
	} else if (type == FIELD_TYPE_BIT         ) {    
		str_type = "BIT";
	} else {
		str_type = "";
	}
    return str_type;
}