/*! \class BeaconPushService
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/10/06 11:01:00
 */

#ifndef PLAYER_BEAPOTAGENT_BEACONPUSHSERVICE_H
#define PLAYER_BEAPOTAGENT_BEACONPUSHSERVICE_H

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include "actionManager.h"
#include "goodsInfoManager.h"
#include "TcpStreamChannel.h"
#include "CircularQueue.h"

class BEACON_ACTION_INFO {
public:
    BEACON_ACTION_INFO() {}
    BEACON_ACTION_INFO(ciString& actionType, ciStringList& beaconIdList) {
        action_type = actionType;
        beacon_id_list = beaconIdList;
    }
    ~BEACON_ACTION_INFO() {}

    int         GetCount() { return (int) beacon_id_list.size(); }
    ciString    Get(int index) {
        ciStringList::iterator itr;    
        ciString str_ret("");
        int i = 0;
        for (itr = beacon_id_list.begin(); itr != beacon_id_list.end(); itr++) {
            if (i == index) {
                str_ret = *itr;
                return str_ret;
            }
            i++;
        }
        return str_ret;
    }

public:
    ciString        action_type;
    ciStringList    beacon_id_list;
};

typedef struct _SEND_FLASH_MSG {
    string          access_pickup_msg;
    string          best123_msg;
} SEND_FLASH_MSG;

class BeaconPushService : public actionHandler, public ciThread
{
public:
    BeaconPushService();
    virtual ~BeaconPushService(void);

	virtual void    processEvent(ciString& actionType, ciStringList& beaconIdList, int additionalState);

    void    loadIni();
    void    Start();
    void    Stop();
    void    run();

    void    SendMessageFlash(string& str_send_msg);

public:
    //Simul ����
    CircularQueue<SEND_FLASH_MSG> flash_msg_queue;    
	ciBoolean				        isSimul;

protected:
    bool    GetGoodsInfo_(BEACON_ACTION_INFO& beacon_action_info, goodsInfoList& out_goods_list);
    string  SetMirrorAccessCount_(ciString& action_type, goodsInfoList& in_goods_info_list);

    string  CreateGoodsInfoMsg_(ciString& action_type, goodsInfoList& goods_info_list);
    void    ReleaseGoodsInfo(goodsInfoList& goods_info_list);
    bool    SortGoodsInfo_(int case_num, vector<goodsInfo*>& in_good_vec, goodsInfoList& out_goods_list);
    
protected:
    //list<BEACON_ACTION_INFO> beacon_action_queue_;
    CircularQueue<BEACON_ACTION_INFO> beacon_action_queue_;
    ciMutex		             beacon_action_queue_lock_; //ciGuard aGuard(beacon_action_queue_lock_);
    bool                     started_;
   
    ciString                 flash_ip_;
    ciLong                   flash_port_;
    ciString                 site_id_;
    ciString                 api_adapter_ip_;
    ciLong                   api_adapter_port_;

    TcpStreamChannel         flash_channel_;


};

#endif //PLAYER_BEAPOTAGENT_BEACONPUSHSERVICE_H
