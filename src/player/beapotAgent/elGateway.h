/** \class elGateway
 *  Copyright ⓒ 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief 명령 서버, 채널등을 관리
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elGateway_h_
#define _elGateway_h_

#include "elComponent.h"
#include "elRoute.h"

class elGateway : public elComponent
{
public:
    elGateway();
    virtual ~elGateway();

    void    ProcessExpired(string name, int counter, int interval_sec, int interval_msec=0);
    void    Update();
    bool    Init(string my_id);
    bool    Start(string option = ADMIN_STATE_START_ALL);
    bool    Stop();

protected:
    void    SetChildComponent_();

protected:
    elRuleRepository*   p_rule_repository_;
    elGatewayRule*      p_my_rule_;
    elRuleIdMap         route_rule_map_;
    vector<elRoute*>    route_vec_;

};

#endif // _elGateway_h_