/** \class elCommand
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elCommand_h_
#define _elCommand_h_

#include "elComponent.h"
#include "elEndpoint.h"
#include "elMysqlDriver.h"

class elCommand : public elComponent
{
public:
    elCommand(void);
    virtual ~elCommand(void);

    void    ProcessExpired(string name, int counter, int interval_sec, int interval_msec=0);
    void    Update();
    bool    Init(string my_id);
    bool    Start(string option = ADMIN_STATE_START_ALL);
    bool    Stop();

    bool    Execute(elRecordList& record_list);
    int     SetExecCommand(string str_raw_command, elRecordList& records, string& str_exe_command_out);

public:
    string          command_id;
    string          partner_command_id;
    string          end_point;
    string          entity;
    //vector<string>  fleld_arr_vec;
    elRecord        field_name_arr_;
    string          command;
    string          property_id;
    vector<string>  argv_vec;

protected:
    bool            IncreaseCommandRecords_(string& str_nobind_block, elRecordList& records, ciStringVector& repeat_block_vec_out);
    bool            ExtractRepeatBlock_(string& str_raw_command, string& command_front_out, string& str_repeat_block, string& command_end_out);
    int             BindCommandValue_(elRecord& record, string& str_bind_command_inout);
    int             BindCommandValueAtRecords_(elRecordList& record, string& str_bind_command_inout);
    bool            IsDigit_(string str_digit);


protected:
    elRuleRepository*       p_rule_repository_;
    elCommandRule*          p_my_rule_;
    elEndPointRule*         p_endpoint_rule_;
    elEndpoint*             p_endpoint;

    ciMutex		            execute_lock_;

};

#endif // _elCommand_h_