#include "elGateway.h"

ciSET_DEBUG(5, "elGateway");

elGateway::elGateway(void) : p_rule_repository_(NULL), p_my_rule_(NULL)
{
    ciDEBUG(5, ("elGateway()"));
}

elGateway::~elGateway(void)
{
    ciDEBUG(5, ("~elGateway()"));
}

void elGateway::ProcessExpired(string name, int counter, int interval_sec, int interval_msec) 
{
    ciDEBUG(5, ("ProcessExpired(%s, %d, %d, %d)", name.c_str(), counter, interval_sec, interval_msec));
}

void elGateway::Update() 
{
    ciDEBUG(5, ("Update()"));
}

bool elGateway::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elGatewayRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;

    route_rule_map_ = p_my_rule_->GetChildRuleMap(EL_KEYWORD_ROUTE_RULE);
    return true;
}

bool elGateway::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    if (option == ADMIN_STATE_START_NO) {
        // Do noting
        return true;
    } else if (option == ADMIN_STATE_START_ALL) {
        SetChildComponent_();
    } else if (option == ADMIN_STATE_START_TIMER) {
    } else if (option == ADMIN_STATE_START_THREAD) {
    } else if (option == ADMIN_STATE_START_CHILD) {
        SetChildComponent_();
    } else {
        ciERROR(("Failed! Start option(%s)", option.c_str()));
        return false;
    }

    return true;
}

bool elGateway::Stop()
{
    ciDEBUG(5, ("Stop()"));

    // Stop Timer

    // Stop Thread

    // Stop child
    for (unsigned int i = 0; i < route_vec_.size(); i++) {
        route_vec_[i]->Stop();
    }

    return true;
}


void elGateway::SetChildComponent_()
{
    ciDEBUG(5, ("SetChildComponent_()"));

	elRuleIdMap::iterator itr;
	for(itr=route_rule_map_.begin(); itr!=route_rule_map_.end(); itr++) {
		elRule* ele = itr->second;
        elRoute* route = new elRoute();
        route->Init(ele->id);
        route->Start(ele->admin_state);
        route_vec_.push_back(route);
	}
}