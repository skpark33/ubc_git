use mysql;
FLUSH PRIVILEGES ;

CREATE USER ubc@'localhost' identified by 'sqicop';
GRANT ALL PRIVILEGES ON *.* TO ubc@'localhost' IDENTIFIED BY 'sqicop'  WITH GRANT OPTION ;
CREATE USER ubc@'%' identified by 'sqicop';
GRANT ALL PRIVILEGES ON *.* TO ubc@'%' IDENTIFIED BY 'sqicop'  WITH GRANT OPTION ;


FLUSH PRIVILEGES ;

use iot;

ALTER TABLE `iot`.`iot_beacon` 
ADD COLUMN `freeze_x` DECIMAL(9,2) NULL DEFAULT 0 AFTER `description`,
ADD COLUMN `freeze_y` DECIMAL(9,2) NULL DEFAULT 0 AFTER `freeze_x`,
ADD COLUMN `freeze_z` DECIMAL(9,2) NULL DEFAULT 0 AFTER `freeze_y`,
ADD COLUMN `freeze_time` DATETIME NULL AFTER `freeze_z`;

CREATE TABLE `iot.iot_beacon_freeze_log` (
  `mgrId` varchar(64) NOT NULL DEFAULT '0',
  `beaconId` varchar(64) NOT NULL DEFAULT '',
  `ctime` datetime DEFAULT NULL,
  `xiro_x` decimal(9,2) DEFAULT NULL,
  `xiro_y` decimal(9,2) DEFAULT NULL,
  `xiro_z` decimal(9,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `iot`.`iot_beacon_log` 
DROP COLUMN `beaconLogId`,
CHANGE COLUMN `lastUpdateTime` `ctime` DATETIME NULL DEFAULT NULL ,
ADD COLUMN `xiro_x` DECIMAL(9,2) NULL AFTER `ctime`,
ADD COLUMN `xiro_y` DECIMAL(9,2) NULL AFTER `xiro_x`,
ADD COLUMN `xiro_z` DECIMAL(9,2) NULL AFTER `xiro_y`,
DROP PRIMARY KEY;

ALTER TABLE `iot`.`iot_promotion` 
CHANGE COLUMN `bannerImg` `bannerImg` VARCHAR(255) NULL COMMENT '' ,
CHANGE COLUMN `pushImg` `pushImg` VARCHAR(255) NULL COMMENT '' ,
CHANGE COLUMN `contextImg` `contextImg` VARCHAR(255) NULL COMMENT '' ;

ALTER TABLE `iot`.`iot_promotion` 
ADD COLUMN `deviceType` TINYINT NULL COMMENT '' AFTER `description`;




