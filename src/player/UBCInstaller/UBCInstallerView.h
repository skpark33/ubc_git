// UBCInstallerView.h : CUBCInstallerView 클래스의 인터페이스
//


#pragma once


class CUBCInstallerView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CUBCInstallerView();
	DECLARE_DYNCREATE(CUBCInstallerView)

public:
	enum{ IDD = IDD_UBCINSTALLER_FORM };

// 특성입니다.
public:
	CUBCInstallerDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CUBCInstallerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

	COLORREF	m_rgbBackGround;

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
};

#ifndef _DEBUG  // UBCInstallerView.cpp의 디버그 버전
inline CUBCInstallerDoc* CUBCInstallerView::GetDocument() const
   { return reinterpret_cast<CUBCInstallerDoc*>(m_pDocument); }
#endif

