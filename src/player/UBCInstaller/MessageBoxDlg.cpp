// MessageBoxDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCInstaller.h"
#include "MessageBoxDlg.h"


// CMessageBoxDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMessageBoxDlg, CDialog)

CMessageBoxDlg::CMessageBoxDlg(LPCSTR lpszTitle, LPCSTR lpszMessage, UINT nType, CWnd* pParent /*=NULL*/)
	: CDialog(CMessageBoxDlg::IDD, pParent)
	, m_strTitle ( lpszTitle )
	, m_strMessage ( lpszMessage )
	, m_nType ( nType )
	, m_hIcon ( NULL )
{
	m_font.CreatePointFont(14*10, ::GetDefaultFontName());
}

CMessageBoxDlg::~CMessageBoxDlg()
{
}

void CMessageBoxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_ICON, m_stcIcon);
	DDX_Control(pDX, IDC_STATIC_MSG, m_stcMsg);
	DDX_Control(pDX, IDYES, m_btnYes);
	DDX_Control(pDX, IDNO, m_btnNo);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CMessageBoxDlg, CDialog)
	ON_BN_CLICKED(IDYES, &CMessageBoxDlg::OnBnClickedYes)
	ON_BN_CLICKED(IDNO, &CMessageBoxDlg::OnBnClickedNo)
END_MESSAGE_MAP()


// CMessageBoxDlg 메시지 처리기입니다.

BOOL CMessageBoxDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	m_btnYes.SetWindowText(::LoadString(IDS_YES));
	m_btnNo.SetWindowText(::LoadString(IDS_NO));
	m_btnOK.SetWindowText(::LoadString(IDS_OK));
	m_btnCancel.SetWindowText(::LoadString(IDS_CANCEL));

	m_stcMsg.SetFont(&m_font);
	m_btnYes.SetFont(&m_font);
	m_btnNo.SetFont(&m_font);
	m_btnOK.SetFont(&m_font);
	m_btnCancel.SetFont(&m_font);

	//
	switch( m_nType & MB_ICONMASK )
	{
	case MB_ICONEXCLAMATION: // Load the icon with the exclamation mark.
		m_hIcon = AfxGetApp()->LoadStandardIcon(MAKEINTRESOURCE(IDI_EXCLAMATION));
		break;

	case MB_ICONHAND: // Load the icon with the error symbol.
		m_hIcon = AfxGetApp()->LoadStandardIcon(MAKEINTRESOURCE(IDI_HAND));
		break;

	case MB_ICONQUESTION: // Load the icon with the question mark.
		m_hIcon = AfxGetApp()->LoadStandardIcon(MAKEINTRESOURCE(IDI_QUESTION));
		break;

	default:
	case MB_ICONASTERISK: // Load the icon with the information symbol.
		m_hIcon = AfxGetApp()->LoadStandardIcon(MAKEINTRESOURCE(IDI_ASTERISK));
		break;
	}
	if( m_hIcon ) m_stcIcon.SetIcon(m_hIcon);

	//
	m_btnYes.ShowWindow(SW_HIDE);
	m_btnNo.ShowWindow(SW_HIDE);
	m_btnOK.ShowWindow(SW_HIDE);
	m_btnCancel.ShowWindow(SW_HIDE);
	int limit_width = 100;
	switch( m_nType & MB_TYPEMASK )
	{
	default:
	case MB_ABORTRETRYIGNORE: // not support yet...
	case MB_RETRYCANCEL: // not support yet...
	case MB_OK:
		m_listButtons.Add(&m_btnOK);
		limit_width = 25;
		break;
	case MB_OKCANCEL:
		m_listButtons.Add(&m_btnOK);
		m_listButtons.Add(&m_btnCancel);
		limit_width = 125;
		break;
	case MB_YESNOCANCEL:
		m_listButtons.Add(&m_btnYes);
		m_listButtons.Add(&m_btnNo);
		m_listButtons.Add(&m_btnCancel);
		limit_width = 225;
		break;
	case MB_YESNO:
		m_listButtons.Add(&m_btnYes);
		m_listButtons.Add(&m_btnNo);
		limit_width = 125;
		break;
	}

	//
	SetWindowText(m_strTitle);
	m_stcMsg.SetWindowText(m_strMessage);

	//
	CDC* pDC = GetDC();
	CFont* old_font = pDC->SelectObject(&m_font);

	CRect rect_msg(0,0,0,0);
	pDC->DrawTextEx(m_strMessage, &rect_msg, DT_CALCRECT, NULL);
	if( rect_msg.Width() < limit_width )
		rect_msg.right = rect_msg.left + limit_width;

	pDC->SelectObject(old_font);
	ReleaseDC(pDC);

	//
	CRect rect;
	GetWindowRect(rect);
	rect.right += rect_msg.Width();
	rect.bottom += rect_msg.Height();
	MoveWindow(rect);

	m_stcMsg.GetWindowRect(rect);
	ScreenToClient(rect);
	rect.right += rect_msg.Width();
	rect.bottom += rect_msg.Height();
	m_stcMsg.MoveWindow(rect);

	if( m_listButtons.GetCount() > 0 )
	{
		//
		CButton* btn = m_listButtons.GetAt(m_listButtons.GetCount()-1);

		btn->GetWindowRect(rect);
		ScreenToClient(rect);
		rect.OffsetRect(rect_msg.Width(), rect_msg.Height());
		btn->MoveWindow(rect);
		btn->ShowWindow(SW_SHOW);

		//
		for(int i=m_listButtons.GetCount()-2; i>=0; i--)
		{
			btn = m_listButtons.GetAt(i);
			rect.OffsetRect(-(rect.Width() * 120 / 100), 0);
			btn->MoveWindow(rect);
			btn->ShowWindow(SW_SHOW);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMessageBoxDlg::OnBnClickedYes()
{
	EndDialog(IDYES);
}

void CMessageBoxDlg::OnBnClickedNo()
{
	EndDialog(IDNO);
}

/////////////////////////////////////////////////////////////////
int MessageBox(LPCSTR lpszTitle, LPCSTR lpszMessage, UINT nType)
{
	CMessageBoxDlg dlg(lpszTitle, lpszMessage, nType);
	return dlg.DoModal();
}

int MessageBox(LPCSTR lpszTitle, UINT nMsgID, UINT nType)
{
	CString str_msg;
	str_msg.LoadString(nMsgID);

	CMessageBoxDlg dlg(lpszTitle, str_msg, nType);
	return dlg.DoModal();
}

int MessageBox(LPCSTR lpszMessage, UINT nType)
{
	CString str_title;
	str_title = ::AfxGetAppName();

	CMessageBoxDlg dlg(str_title, lpszMessage, nType);
	return dlg.DoModal();
}

int MessageBox(UINT nMsgID, UINT nType)
{
	CString str_title;
	str_title = ::AfxGetAppName();

	CString str_msg;
	str_msg.LoadString(nMsgID);

	CMessageBoxDlg dlg(str_title, str_msg, nType);
	return dlg.DoModal();
}
