// InstallDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCInstaller.h"
#include "InstallDlg.h"

#include "SelectOSVerDlg.h"
#include "MessageBoxDlg.h"

#include "Installer.h"


#define		TIMER_ID_CHECK_WIN_VER		1025
#define		TIMER_TIME_CHECK_WIN_VER	100

#define		TIMER_ID_PROGRESSING		1026
#define		TIMER_TIME_PROGRESSING		300

#define		WM_ITEM_COMPLETE			(WM_USER + 2049)
#define		WM_ALL_COMPLETE				(WM_USER + 2050)


// CInstallDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CInstallDlg, CDialog)

CInstallDlg::CInstallDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInstallDlg::IDD, pParent)
	, m_dlgDetectOS( this )
{
	m_font.CreatePointFont(14*10, ::GetDefaultFontName());
	m_font2.CreatePointFont(12*10, ::GetDefaultFontName());

	m_bChecking = false;
	m_bInstalling = false;

	m_nInstallCount = 0;
	m_nProgressingCount = 0;
}

CInstallDlg::~CInstallDlg()
{
	ClearDataList();
}

void CInstallDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_EDITION, m_stcEdition);
	DDX_Control(pDX, IDC_LIST_ITEM, m_lcItem);
	DDX_Control(pDX, IDC_BUTTON_INSTALL, m_btnInstall);
	DDX_Control(pDX, IDC_BUTTON_LICENSE, m_btnLicense);
	DDX_Control(pDX, IDC_BUTTON_SELECT_OS, m_btnSelectOS);
	DDX_Control(pDX, IDCANCEL, m_btnExit);
}


BEGIN_MESSAGE_MAP(CInstallDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_INSTALL, &CInstallDlg::OnBnClickedButtonInstall)
	ON_BN_CLICKED(IDC_BUTTON_LICENSE, &CInstallDlg::OnBnClickedButtonLicense)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_OS, &CInstallDlg::OnBnClickedButtonSelectOs)
	ON_NOTIFY(NM_CLICK, IDC_LIST_ITEM, &CInstallDlg::OnNMClickListItem)
	ON_MESSAGE(WM_ITEM_COMPLETE, OnItemComplete)
	ON_MESSAGE(WM_ALL_COMPLETE, OnAllComplete)
	ON_MESSAGE(WM_SET_OS_VERSION, OnSetOSVersion)
END_MESSAGE_MAP()


// CInstallDlg 메시지 처리기입니다.

BOOL CInstallDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// delete (pre-regist)auto-startup
	CInstaller installer;
	installer.DeleteProcessFromRun();

	//
	switch( ::GetUBCEdition() )
	{
	case UBC_EDITION_STANDARD_PLUS:
		m_bmpEdition.LoadBitmap(IDB_EDITION_STANDARD_PLUS);
		break;
	case UBC_EDITION_ENTERPRISE:
		m_bmpEdition.LoadBitmap(IDB_EDITION_ENTERPRISE);
		break;
	default:
		m_bmpEdition.LoadBitmap(IDB_EDITION_STANDARD);
		break;
	}

	//
	m_btnInstall.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_INSTALLING)));
	m_btnLicense.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_LICENSE)));
	m_btnSelectOS.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_OS)));
	m_btnExit.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_EXIT)));

	m_btnInstall.SetFont(&m_font);
	m_btnLicense.SetFont(&m_font);
	if( stricmp(::GetDefaultFontName(), "Microsoft Sans Serif") != 0 )
		m_btnSelectOS.SetFont(&m_font2);
	else
		m_btnSelectOS.SetFont(&m_font);
	m_btnExit.SetFont(&m_font);

	m_btnInstall.SetWindowText(::LoadString(IDS_BUTTON_INSTALL));
	m_btnLicense.SetWindowText(::LoadString(IDS_BUTTON_LICENSE));
	m_btnSelectOS.SetWindowText(::LoadString(IDS_BUTTON_SELECT_OS));
	m_btnExit.SetWindowText(::LoadString(IDS_BUTTON_EXIT));

	//
	CBitmap bmp;
	bmp.LoadBitmap(IDB_LISTCTRL);
	m_imglist.Create(26, 26, ILC_COLORDDB | ILC_MASK, 6, 1);
	m_imglist.Add(&bmp, RGB(255, 0, 255));

	// init list-ctrl
	CRect rect;
	m_lcItem.GetClientRect(rect);
	m_lcItem.SetExtendedStyle(m_lcItem.GetExtendedStyle() /*| LVS_EX_CHECKBOXES*/ | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_SUBITEMIMAGES );
	m_lcItem.SetImageList(&m_imglist, LVSIL_SMALL);
	m_lcItem.SetFont(&m_font);

	m_lcItem.InsertColumn(0, ::LoadString(IDS_COLUMN_INSTALL_ITEMS), 0, 370);
	m_lcItem.InsertColumn(1, ::LoadString(IDS_COLUMN_STATUS), 0, rect.Width()-370);

	//
	m_dlgDetectOS.Create(IDD_DETECT_OS, this);
	m_dlgDetectOS.CenterWindow();
	m_dlgDetectOS.ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CInstallDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	CRect rect;
	m_stcEdition.GetClientRect(rect);

	CDC memdc;
	memdc.CreateCompatibleDC(&dc);
	memdc.SelectObject(&m_bmpEdition);

	BITMAP bmp_info;
	m_bmpEdition.GetBitmap(&bmp_info);

	dc.BitBlt(rect.left, rect.top, bmp_info.bmWidth, bmp_info.bmHeight, &memdc, 0, 0, SRCCOPY);
}

void CInstallDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case TIMER_ID_PROGRESSING:
		{
			int cnt = m_lcItem.GetItemCount();
			for(int i=0; i<cnt; i++)
			{
				INSTALL_DATA* data = (INSTALL_DATA*)m_lcItem.GetItemData(i);
				if( data == NULL ) continue;

				if( data->eInstallStatus == INSTALL_STATUS_CHECKING||
					data->eInstallStatus == INSTALL_STATUS_INSTALLING )
				{
					CString str = ::GetInstallStatusString(data->eInstallStatus);
					for(int j=0; j<(m_nProgressingCount%7); j++) str+=".";
					m_lcItem.SetItemText(i, 1, str);
					m_nProgressingCount++;
					break;
				}
			}
		}
		break;
	}

	CDialog::OnTimer(nIDEvent);
}

void CInstallDlg::OnCancel()
{
	if( m_bChecking || m_bInstalling )
	{
		// work in progress
		int ret = ::MessageBox(IDS_MSG_WORK_IN_PROGRESS_EXIT, MB_ICONWARNING | MB_YESNO);
		if( ret == IDNO ) return;
	}

	CInstaller installer;
	if( !installer.IsExistLicenseInfo() )
	{
		// not exist license-info
		int ret = ::MessageBox(IDS_MSG_NOT_EXIST_LICENSE_INFO, MB_ICONWARNING | MB_YESNO);
		if( ret == IDNO ) return;
	}

	if( m_nInstallCount > 0 )
	{
		// need to reboot
		int ret = ::MessageBox(IDS_MSG_CHANGED_NEED_TO_REBOOTING, MB_ICONWARNING | MB_YESNO);
		if( ret == IDYES )
		{
			CInstaller installer;
			if( !installer.Reboot() )
			{	// fail to reboot
				::MessageBox(IDS_MSG_FAIL_REBOOT_REBOOT_MANUALLY
								/*"Can't reboot automatically.\r\n\r\n"
								"You must reboot manually."*/, MB_ICONWARNING);
			}
		}
	}

	CDialog::OnCancel();
}

void CInstallDlg::OnBnClickedButtonInstall()
{
	INSTALL_DATA_LIST* data_list = new INSTALL_DATA_LIST;

	//
	int cnt = m_lcItem.GetItemCount();
	for(int i=0; i<cnt; i++)
	{
		if( m_lcItem.GetCheck(i) )
		{
			data_list->Add((INSTALL_DATA*)m_lcItem.GetItemData(i));
		}
	}

	//
	if( data_list->GetCount() == 0 )
	{	// nothing selected-install-items
		::MessageBox(IDS_MSG_NOTHING_TO_SELECTED_INSTALL_ITEMS, MB_ICONSTOP);
		delete data_list;
		return;
	}

	//
	m_bInstalling = true;
	m_btnInstall.EnableWindow(FALSE);
	m_btnLicense.EnableWindow(FALSE);
	m_btnSelectOS.EnableWindow(FALSE);

	THREAD_PARAM* param = new THREAD_PARAM;
	param->hParentWnd = GetSafeHwnd();
	param->pDataList = data_list;

	CWinThread* thread = ::AfxBeginThread(InstallItemsThread, (LPVOID)param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	thread->m_bAutoDelete = true;
	thread->ResumeThread();
}

void CInstallDlg::OnBnClickedButtonLicense()
{
	CInstaller installer;

	installer.PopupLicenseCenter();
}

void CInstallDlg::OnBnClickedButtonSelectOs()
{
	CSelectOSVerDlg dlg;
	if( dlg.DoModal() == IDCANCEL ) return;

	Init( dlg.GetSelectOSVersion() );
}

void CInstallDlg::OnNMClickListItem(NMHDR *pNMHDR, LRESULT *pResult)
{
	if( m_bChecking || m_bInstalling ) return;

	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<NMITEMACTIVATE*>(pNMHDR);

	int idx = pNMItemActivate->iItem;
	if( idx < 0 ) return;

	LVITEM item;
	item.mask = LVIF_IMAGE;
	item.iItem = idx;
	item.iSubItem = 0;

	if( m_lcItem.GetCheck(idx) )
	{	// check ==> uncheck
		item.iImage = 0;
		m_lcItem.SetCheck(idx, 0);
	}
	else
	{	// uncheck ==> check
		item.iImage = 1;
		m_lcItem.SetCheck(idx, 1);
	}

	m_lcItem.SetItem(&item);

	*pResult = 0;
}

void CInstallDlg::Init(OS_VERSION osVer)
{
	ClearDataList();
	m_lcItem.DeleteAllItems();

	// not support os  ==>  select os manually
	if( osVer == OS_NOT_SUPPORT )
	{
		if( IDYES == ::MessageBox(IDS_MSG_NOT_SUOOPRT_OS_VERSION, MB_ICONSTOP | MB_YESNO) )
		{
			CSelectOSVerDlg dlg;
			if( dlg.DoModal() == IDOK )
			{
				osVer = dlg.GetSelectOSVersion();
			}
		}
	}

	switch( osVer )
	{
	default:
	case OS_NOT_SUPPORT:
		break;

	case OS_WIN_XP:
		SetWindowText(::LoadString(IDS_MSG_INSTALL_ITEMS_FOR_WIN_XP));
		InitializeWinXP();
		break;

	case OS_WIN_VISTA:
		SetWindowText(::LoadString(IDS_MSG_INSTALL_ITEMS_FOR_WIN_VISTA));
		InitializeWinVista();
		break;

	case OS_WIN_7:
		SetWindowText(::LoadString(IDS_MSG_INSTALL_ITEMS_FOR_WIN_7));
		InitializeWin7();
		break;

	case OS_WIN_8:
		SetWindowText(::LoadString(IDS_MSG_INSTALL_ITEMS_FOR_WIN_8));
		InitializeWin8();
		break;

	case OS_WIN_9:
		SetWindowText(::LoadString(IDS_MSG_INSTALL_ITEMS_FOR_WIN_9));
		InitializeWin9();
		break;
	}

	int cnt = m_InstallDataList.GetCount();
	if( cnt == 0 )
	{	// not support os  ==>  exit
		::AfxGetMainWnd()->PostMessage(WM_COMMAND, ID_APP_EXIT);
		CDialog::OnCancel();
		return;
	}

	// add install-items to list
	for(int i=0; i<cnt; i++)
	{
		INSTALL_DATA* data = m_InstallDataList.GetAt(i);

		m_lcItem.InsertItem(i, szInstallItemName[data->eItem], 0);
		m_lcItem.SetItemText(i, 1, ::GetInstallStatusString(data->eInstallStatus));

		LVITEM item;
		item.mask = LVIF_IMAGE;
		item.iItem = i;
		item.iSubItem = 1;
		item.iImage = data->eInstallStatus;
		m_lcItem.SetItem(&item);

		m_lcItem.SetItemData(i, (DWORD)data);
		m_lcItem.SetCheck(i, 0);
	}

	// run checking-thread
	m_bChecking = true;
	m_btnInstall.EnableWindow(FALSE);
	m_btnLicense.EnableWindow(FALSE);
	m_btnSelectOS.EnableWindow(FALSE);

	THREAD_PARAM* param = new THREAD_PARAM;
	param->hParentWnd = GetSafeHwnd();
	param->pDataList = &m_InstallDataList;

	CWinThread* thread = ::AfxBeginThread(CheckItemsThread, (LPVOID)param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	thread->m_bAutoDelete = true;
	thread->ResumeThread();
}

void CInstallDlg::InitializeWinXP()
{
	// all-windows
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_VNC));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_KLITE_CODEC));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FLASHPLAYER));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SHORTCUT));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FIREWALL_EXCEPTION));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SECURITY));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SOFT_DRIVE));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_TASKBAR));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_AUTO_LOGIN));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FLASHPLAYER_UPDATE));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_WINDOWS_UPDATE));

	// only xp
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_CLEANUP_WIZARD));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SCREEN_SAVER_XP));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SECURITY_CENTER_ALARM));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_WINDOWS_ERROR_REPORTING_XP));
}

void CInstallDlg::InitializeWinVista()
{
	CInstaller installer;
	if( installer.Check(INSTALL_ITEM_UAC_VISTA) == INSTALL_STATUS_NOT_INSTALL )
	{
		if( installer.Install(INSTALL_ITEM_UAC_VISTA) == INSTALL_STATUS_NOT_INSTALL )
		{
			// uac on  ==>  fail to set UAC-off  ==> error message & quit
			::MessageBox(IDS_MSG_UAC_NOT_RELEASE_MUST_RELEASE_MANUALLY
							/*"User-Account-Control isn't released\r\n"
							"and can't release User-Account-Control.\r\n\r\n"
							"You must release User-Account-Control manually\r\n"
							"in user account settings of control panel."*/, MB_ICONWARNING);
			return;
		}
		else
		{
			// uac on  ==>  success to set UAC-off  ==>  need to reboot
			if( !installer.AddProcessToRun() )
			{
				// success to add shortcut in startup-group
				::MessageBox(IDS_MSG_UAC_OFF_REBOOT_AUTOMATICALLY_EXECUTE_MANUALLY
								/*"User-Account-Control is released now.\r\n"
								"You need to reboot.\r\n\r\n"
								"If you press OK button, you will reboot.\r\n"
								"After rebooting, execute UBC Install on the Desktop."*/, MB_ICONWARNING);
			}
			else
			{
				// fail to add shortcut in startup-group
				::MessageBox(IDS_MSG_UAC_OFF_REBOOT_AUTOMATICALLY_CONTINUE_INSTALL
								/*"User-Account-Control is released now.\r\n"
								"You need to reboot.\r\n\r\n"
								"If you press OK button, you will reboot.\r\n"
								"After rebooting, you can continue to install."*/, MB_ICONWARNING);
			}

			if( !installer.Reboot() )
			{	// fail to reboot
				::MessageBox(IDS_MSG_FAIL_REBOOT_REBOOT_MANUALLY
								/*"Can't reboot automatically.\r\n\r\n"
								"You must reboot manually."*/, MB_ICONWARNING);
			}
			return;
		}
	}
	else
	{
		if( installer.Install(INSTALL_ITEM_UAC_VISTA) == INSTALL_STATUS_NOT_INSTALL )
		{
			// UAC isn't released perfectly !!!
			::MessageBox(IDS_MSG_UAC_OFF_BUT_NOT_PERFECTLY
							/*"User-Account-Control is released.\r\n"
							"But you need to reboot for releasing User-Account-Control perfectly.\r\n\r\n"
							"If you press OK button, you will reboot.\r\n"
							"After rebooting, you can continue to install."*/, MB_ICONWARNING);

			if( !installer.Reboot() )
			{	// fail to reboot
				::MessageBox(IDS_MSG_FAIL_REBOOT_REBOOT_MANUALLY
								/*"Can't reboot automatically.\r\n\r\n"
								"You must reboot manually."*/, MB_ICONWARNING);
			}
			return;
		}
	}

	// all-windows
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_VNC));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_KLITE_CODEC));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FLASHPLAYER));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SHORTCUT));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FIREWALL_EXCEPTION));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SECURITY));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SOFT_DRIVE));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_TASKBAR));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_AUTO_LOGIN));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FLASHPLAYER_UPDATE));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_WINDOWS_UPDATE));

	// only vista
	//m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_UAC_VISTA));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SCREEN_SAVER_VISTA));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SIDE_BAR));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_VISTA));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_AERO_THEME_VISTA));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_CUSTOMER_EXPERIENCE));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_WELCOME_CENTER));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_WINDOWS_ERROR_REPORTING_VISTA));
}

void CInstallDlg::InitializeWin7()
{
	CInstaller installer;
	if( installer.Check(INSTALL_ITEM_UAC_7) == INSTALL_STATUS_NOT_INSTALL )
	{
		if( installer.Install(INSTALL_ITEM_UAC_7) == INSTALL_STATUS_NOT_INSTALL )
		{
			// uac on  ==>  fail to set UAC-off  ==> error message & quit
			::MessageBox(IDS_MSG_UAC_NOT_RELEASE_MUST_RELEASE_MANUALLY_POPUP_DIALOG
							/*"User-Account-Control isn't released\r\n"
							"and can't release User-Account-Control.\r\n\r\n"
							"You must release User-Account-Control manually.\r\n\r\n"
							"If you press OK button,\r\n"
							"dialog box of User-Account-Control will be displayed."*/, MB_ICONWARNING);

			if( !installer.PopupUACfor7() )
			{	// fail to popup uac-settings
				::MessageBox(IDS_MSG_FAIL_POPUP_UAC_MANUALLY_RELEASE
								/*"Can't popup dialog box of User-Account-Control.\r\n\r\n"
								"You must release User-Account-Control manually\r\n"
								"in User-Accounts settings of Control-Panel."*/, MB_ICONWARNING);
			}
			return;
		}
		else
		{
			// uac on  ==>  success to set UAC-off  ==>  need to reboot
			if( !installer.AddProcessToRun() )
			{
				// success to add shortcut in startup-group
				::MessageBox(IDS_MSG_UAC_OFF_REBOOT_AUTOMATICALLY_EXECUTE_MANUALLY
								/*"User-Account-Control is released now.\r\n"
								"You need to reboot.\r\n\r\n"
								"If you press OK button, you will reboot.\r\n"
								"After rebooting, execute UBC Install on the Desktop."*/, MB_ICONWARNING);
			}
			else
			{
				// fail to add shortcut in startup-group
				::MessageBox(IDS_MSG_UAC_OFF_REBOOT_AUTOMATICALLY_CONTINUE_INSTALL
								/*"User-Account-Control is released now.\r\n"
								"You need to reboot.\r\n\r\n"
								"If you press OK button, you will reboot.\r\n"
								"After rebooting, you can continue to install."*/, MB_ICONWARNING);
			}

			if( !installer.Reboot() )
			{	// fail to reboot
				::MessageBox(IDS_MSG_FAIL_REBOOT_REBOOT_MANUALLY
								/*"Can't reboot automatically.\r\n\r\n"
								"You must reboot manually."*/, MB_ICONWARNING);
			}
			return;
		}
	}
	else
	{
		if( installer.Install(INSTALL_ITEM_UAC_7) == INSTALL_STATUS_NOT_INSTALL )
		{
			// UAC isn't released perfectly !!!
			::MessageBox(IDS_MSG_UAC_OFF_BUT_NOT_PERFECTLY
							/*"User-Account-Control is released.\r\n"
							"But you need to reboot for releasing User-Account-Control perfectly.\r\n\r\n"
							"If you press OK button, you will reboot.\r\n"
							"After rebooting, you can continue to install."*/, MB_ICONWARNING);

			if( !installer.Reboot() )
			{	// fail to reboot
				::MessageBox(IDS_MSG_FAIL_REBOOT_REBOOT_MANUALLY
								/*"Can't reboot automatically.\r\n\r\n"
								"You must reboot manually."*/, MB_ICONWARNING);
			}
			return;
		}
	}

	// all-windows
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_VNC));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_KLITE_CODEC));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FLASHPLAYER));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SHORTCUT));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FIREWALL_EXCEPTION));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SECURITY));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SOFT_DRIVE));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_TASKBAR));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_AUTO_LOGIN));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FLASHPLAYER_UPDATE));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_WINDOWS_UPDATE));

	// only 7
	//m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_UAC_7));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SCREEN_SAVER_7));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_7));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_AERO_THEME_7));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_WINDOWS_ERROR_REPORTING_7));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_CODEC_CONFIG));
}

void CInstallDlg::InitializeWin8()
{
	CInstaller installer;
	if( installer.Check(INSTALL_ITEM_UAC_7) == INSTALL_STATUS_NOT_INSTALL )
	{
		if( installer.Install(INSTALL_ITEM_UAC_7) == INSTALL_STATUS_NOT_INSTALL )
		{
			// uac on  ==>  fail to set UAC-off  ==> error message & quit
			::MessageBox(IDS_MSG_UAC_NOT_RELEASE_MUST_RELEASE_MANUALLY_POPUP_DIALOG
							/*"User-Account-Control isn't released\r\n"
							"and can't release User-Account-Control.\r\n\r\n"
							"You must release User-Account-Control manually.\r\n\r\n"
							"If you press OK button,\r\n"
							"dialog box of User-Account-Control will be displayed."*/, MB_ICONWARNING);

			if( !installer.PopupUACfor7() )
			{	// fail to popup uac-settings
				::MessageBox(IDS_MSG_FAIL_POPUP_UAC_MANUALLY_RELEASE
								/*"Can't popup dialog box of User-Account-Control.\r\n\r\n"
								"You must release User-Account-Control manually\r\n"
								"in User-Accounts settings of Control-Panel."*/, MB_ICONWARNING);
			}
			return;
		}
		else
		{
			// uac on  ==>  success to set UAC-off  ==>  need to reboot
			if( !installer.AddProcessToRun() )
			{
				// success to add shortcut in startup-group
				::MessageBox(IDS_MSG_UAC_OFF_REBOOT_AUTOMATICALLY_EXECUTE_MANUALLY
								/*"User-Account-Control is released now.\r\n"
								"You need to reboot.\r\n\r\n"
								"If you press OK button, you will reboot.\r\n"
								"After rebooting, execute UBC Install on the Desktop."*/, MB_ICONWARNING);
			}
			else
			{
				// fail to add shortcut in startup-group
				::MessageBox(IDS_MSG_UAC_OFF_REBOOT_AUTOMATICALLY_CONTINUE_INSTALL
								/*"User-Account-Control is released now.\r\n"
								"You need to reboot.\r\n\r\n"
								"If you press OK button, you will reboot.\r\n"
								"After rebooting, you can continue to install."*/, MB_ICONWARNING);
			}

			if( !installer.Reboot() )
			{	// fail to reboot
				::MessageBox(IDS_MSG_FAIL_REBOOT_REBOOT_MANUALLY
								/*"Can't reboot automatically.\r\n\r\n"
								"You must reboot manually."*/, MB_ICONWARNING);
			}
			return;
		}
	}
	else
	{
		if( installer.Install(INSTALL_ITEM_UAC_7) == INSTALL_STATUS_NOT_INSTALL )
		{
			// UAC isn't released perfectly !!!
			::MessageBox(IDS_MSG_UAC_OFF_BUT_NOT_PERFECTLY
							/*"User-Account-Control is released.\r\n"
							"But you need to reboot for releasing User-Account-Control perfectly.\r\n\r\n"
							"If you press OK button, you will reboot.\r\n"
							"After rebooting, you can continue to install."*/, MB_ICONWARNING);

			if( !installer.Reboot() )
			{	// fail to reboot
				::MessageBox(IDS_MSG_FAIL_REBOOT_REBOOT_MANUALLY
								/*"Can't reboot automatically.\r\n\r\n"
								"You must reboot manually."*/, MB_ICONWARNING);
			}
			return;
		}
	}

	// all-windows
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_VNC));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_KLITE_CODEC));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FLASHPLAYER));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SHORTCUT));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FIREWALL_EXCEPTION));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SECURITY));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SOFT_DRIVE));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_TASKBAR));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_AUTO_LOGIN));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_FLASHPLAYER_UPDATE));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_WINDOWS_UPDATE));

	// only 8
	//m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_UAC_8));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_SCREEN_SAVER_8));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_8));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_WINDOWS_ERROR_REPORTING_8));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_CODEC_CONFIG_8));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_HOT_CORNERS_8));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_BOOT_DESKTOP_8));
	m_InstallDataList.Add(new INSTALL_DATA(INSTALL_ITEM_LOCK_SCREEN_8));
}

void CInstallDlg::InitializeWin9()
{
	//
	// win9 is not released yet...
	//
}

void CInstallDlg::ClearDataList()
{
	int cnt = m_InstallDataList.GetCount();
	for(int i=0; i<cnt; i++)
	{
		INSTALL_DATA* data = m_InstallDataList.GetAt(i);
		if( data ) delete data;
	}
	m_InstallDataList.RemoveAll();
}

LRESULT CInstallDlg::OnItemComplete(WPARAM wParam, LPARAM lParam)
{
	// complete checking/installing item
	INSTALL_DATA* complete_data = (INSTALL_DATA*)wParam;

	int cnt = m_lcItem.GetItemCount();
	for(int i=0; i<cnt; i++)
	{
		INSTALL_DATA* data = (INSTALL_DATA*)m_lcItem.GetItemData(i);

		if( data == complete_data )
		{
			m_nProgressingCount = 0;

			// update status-text
			m_lcItem.SetItemText(i, 1, ::GetInstallStatusString(data->eInstallStatus));

			// update icon
			LVITEM item;
			item.mask = LVIF_IMAGE;
			item.iItem = i;
			item.iSubItem = 1;
			item.iImage = data->eInstallStatus;
			m_lcItem.SetItem(&item);

			if( data->eInstallStatus == INSTALL_STATUS_NOT_INSTALL )
			{	// not install  ==>  check
				m_lcItem.SetCheck(i);

				item.iSubItem = 0;
				item.iImage = 1;
				m_lcItem.SetItem(&item);
			}
			else if( data->eInstallStatus == INSTALL_STATUS_INSTALLED )
			{	// installed  ==>  unckeck
				m_lcItem.SetCheck(i, 0);

				item.iSubItem = 0;
				item.iImage = 0;
				m_lcItem.SetItem(&item);

				if( m_bInstalling ) m_nInstallCount++;
			}

			break;
		}
	}

	return 0;
}

LRESULT CInstallDlg::OnAllComplete(WPARAM wParam, LPARAM lParam)
{
	// checking/installing all complete
	m_bChecking = false;
	m_bInstalling = false;

	m_btnInstall.EnableWindow();
	m_btnLicense.EnableWindow();
	m_btnSelectOS.EnableWindow();

	return 0;
}

LRESULT CInstallDlg::OnSetOSVersion(WPARAM wParam, LPARAM lParam)
{
	// initialize by os-ver
	Init( (OS_VERSION)wParam );
	return 0;
}

UINT CheckItemsThread(LPVOID pParam)
{
	THREAD_PARAM* param = (THREAD_PARAM*)pParam;

	HWND hParentWnd = param->hParentWnd;
	INSTALL_DATA_LIST* pDataList = param->pDataList;

	delete param;

	// start display-processing-timer
	::SetTimer(hParentWnd, TIMER_ID_PROGRESSING, TIMER_TIME_PROGRESSING, NULL);

	// checking items
	int cnt = pDataList->GetCount();
	for(int i=0; i<cnt; i++)
	{
		INSTALL_DATA* data = pDataList->GetAt(i);

		CInstaller installer;
		data->eInstallStatus = installer.Check(data->eItem);
		::PostMessage(hParentWnd, WM_ITEM_COMPLETE, (WPARAM)data, NULL);
		::Sleep(100);
	}

	// stop display-processing-timer
	::KillTimer(hParentWnd, TIMER_ID_PROGRESSING);

	// send all complete message
	::PostMessage(hParentWnd, WM_ALL_COMPLETE, NULL, NULL);

	return 0;
}

UINT InstallItemsThread(LPVOID pParam)
{
	THREAD_PARAM* param = (THREAD_PARAM*)pParam;

	HWND hParentWnd = param->hParentWnd;
	INSTALL_DATA_LIST* pDataList = param->pDataList;

	delete param;

	// start display-processing-timer
	::SetTimer(hParentWnd, TIMER_ID_PROGRESSING, TIMER_TIME_PROGRESSING, NULL);

	// installing items
	int cnt = pDataList->GetCount();
	for(int i=0; i<cnt; i++)
	{
		INSTALL_DATA* data = pDataList->GetAt(i);

		data->eInstallStatus = INSTALL_STATUS_INSTALLING;
		::PostMessage(hParentWnd, WM_ITEM_COMPLETE, (WPARAM)data, NULL);

		//::Sleep(4000);

		CInstaller installer;
		data->eInstallStatus = installer.Install(data->eItem);
		::PostMessage(hParentWnd, WM_ITEM_COMPLETE, (WPARAM)data, NULL);
	}

	// stop display-processing-timer
	::KillTimer(hParentWnd, TIMER_ID_PROGRESSING);

	// send all complete message
	::PostMessage(hParentWnd, WM_ALL_COMPLETE, NULL, NULL);

	delete pDataList;

	return 0;
}
