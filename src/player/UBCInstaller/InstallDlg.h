#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#include "DetectOSDlg.h"


// CInstallDlg 대화 상자입니다.

class CInstallDlg : public CDialog
{
	DECLARE_DYNAMIC(CInstallDlg)

public:
	CInstallDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CInstallDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_INSTALL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	bool		m_bChecking;
	bool		m_bInstalling;

	int			m_nInstallCount;
	int			m_nProgressingCount;

	CBitmap		m_bmpEdition;

	CFont		m_font;
	CFont		m_font2;
	CImageList	m_imglist;

	INSTALL_DATA_LIST	m_InstallDataList;

	CDetectOSDlg	m_dlgDetectOS;

	void	Init(OS_VERSION osVer);

	void	InitializeWinXP();
	void	InitializeWinVista();
	void	InitializeWin7();
	void	InitializeWin8();
	void	InitializeWin9();

	void	ClearDataList();

public:
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonInstall();
	afx_msg void OnBnClickedButtonLicense();
	afx_msg void OnBnClickedButtonSelectOs();
	afx_msg void OnNMClickListItem(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnItemComplete(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAllComplete(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetOSVersion(WPARAM wParam, LPARAM lParam);

	CStatic		m_stcEdition;
	CListCtrl	m_lcItem;
	CButton		m_btnInstall;
	CButton		m_btnLicense;
	CButton		m_btnSelectOS;
	CButton		m_btnExit;
};

static UINT CheckItemsThread(LPVOID pParam);
static UINT InstallItemsThread(LPVOID pParam);

typedef struct {
	HWND	hParentWnd;
	INSTALL_DATA_LIST*	pDataList;
} THREAD_PARAM;
