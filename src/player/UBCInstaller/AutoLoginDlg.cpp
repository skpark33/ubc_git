// AutoLoginDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCInstaller.h"
#include "AutoLoginDlg.h"


extern BOOL SetThreadLocaleEx(LCID dwLocale=0);


// CAutoLoginDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAutoLoginDlg, CDialog)

CAutoLoginDlg::CAutoLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAutoLoginDlg::IDD, pParent)
{
	SetThreadLocaleEx();

	m_font.CreatePointFont(14*10, ::GetDefaultFontName());
}

CAutoLoginDlg::~CAutoLoginDlg()
{
}

void CAutoLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_MSG, m_stcMsg);
	DDX_Control(pDX, IDC_STATIC_ID, m_stcID);
	DDX_Control(pDX, IDC_STATIC_PASSWORD, m_stcPassword);
	DDX_Control(pDX, IDC_EDIT_ID, m_editID);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_editPassword);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CAutoLoginDlg, CDialog)
END_MESSAGE_MAP()


// CAutoLoginDlg 메시지 처리기입니다.

BOOL CAutoLoginDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	SetWindowText(::LoadString(IDS_CAPTION_WINDOWS_AUTO_LOGIN_SETTINGS));

	//
	m_stcMsg.SetFont(&m_font);
	m_stcID.SetFont(&m_font);
	m_stcPassword.SetFont(&m_font);
	m_editID.SetFont(&m_font);
	m_editPassword.SetFont(&m_font);
	m_btnOK.SetFont(&m_font);
	m_btnCancel.SetFont(&m_font);

	//
	m_stcMsg.SetWindowText(::LoadString(IDS_MSG_PLEASE_INPUT_WINDOWS_AUTO_LOGIN_ID_PASSWORD));
	m_stcID.SetWindowText(::LoadString(IDS_LOGIN_ID));
	m_stcPassword.SetWindowText(::LoadString(IDS_PASSWORD));
	m_btnOK.SetWindowText(::LoadString(IDS_BUTTON_OK));
	m_btnCancel.SetWindowText(::LoadString(IDS_BUTTON_CANCEL));

	//
	m_btnOK.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_CLOSE)));
	m_btnCancel.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_EXIT)));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAutoLoginDlg::OnOK()
{
	m_editID.GetWindowText(m_strLoginID);
	m_editPassword.GetWindowText(m_strPassword);

	CDialog::OnOK();
}
