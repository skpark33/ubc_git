// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// UBCInstaller.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

#include "resource.h"

LPCSTR GetOSVersionString(OS_VERSION eOSVersion)
{
	static CString str_os_version[OS_COUNT];

	if( str_os_version[eOSVersion].GetLength() == 0 )
	{
		switch( eOSVersion )
		{
		default:
			str_os_version[eOSVersion].LoadString(IDS_OS_NOT_SUPPORT);
			break;
		case OS_WIN_XP:
			str_os_version[eOSVersion].LoadString(IDS_OS_WIN_XP);
			break;
		case OS_WIN_VISTA:
			str_os_version[eOSVersion].LoadString(IDS_OS_WIN_VISTA);
			break;
		case OS_WIN_7:
			str_os_version[eOSVersion].LoadString(IDS_OS_WIN_7);
			break;
		case OS_WIN_8:
			str_os_version[eOSVersion].LoadString(IDS_OS_WIN_8);
			break;
		case OS_WIN_9:
			str_os_version[eOSVersion].LoadString(IDS_OS_WIN_9);
			break;
		}

		if( str_os_version[eOSVersion].GetLength() == 0 )
			str_os_version[eOSVersion] = szOSVersion[eOSVersion];
	}

	return str_os_version[eOSVersion];
}

LPCSTR GetInstallStatusString(INSTALL_STATUS eInstallStatus)
{
	static CString str_install_status[INSTALL_STATUS_COUNT];

	if( str_install_status[eInstallStatus].GetLength() == 0 )
	{
		switch( eInstallStatus )
		{
		default:
			eInstallStatus = INSTALL_STATUS_UNKNOWN;
			break;
		case INSTALL_STATUS_CHECKING:
			str_install_status[eInstallStatus].LoadString(IDS_INSTALL_STATUS_CHECKING);
			break;
		case INSTALL_STATUS_NOT_INSTALL:
			str_install_status[eInstallStatus].LoadString(IDS_INSTALL_STATUS_NOT_INSTALL);
			break;
		case INSTALL_STATUS_INSTALLING:
			str_install_status[eInstallStatus].LoadString(IDS_INSTALL_STATUS_INSTALLING);
			break;
		case INSTALL_STATUS_INSTALLED:
			str_install_status[eInstallStatus].LoadString(IDS_INSTALL_STATUS_INSTALLED);
			break;
		}

		if( str_install_status[eInstallStatus].GetLength() == 0 )
			str_install_status[eInstallStatus] = szInstallStatus[eInstallStatus];
	}

	return str_install_status[eInstallStatus];
}

UBC_EDITION GetUBCEdition()
{
	static UBC_EDITION edition = UBC_EDITION_UNKNOWN;
	if( edition == UBC_EDITION_UNKNOWN )
	{
		for(int i=0; i<__argc; i++)
		{
			if( stricmp(__argv[i], "+PLS") == 0 )
			{
				edition = UBC_EDITION_STANDARD_PLUS;
				break;
			}
			if( stricmp(__argv[i], "+ENT") == 0 )
			{
				edition = UBC_EDITION_ENTERPRISE;
				break;
			}
		}
		if( edition == UBC_EDITION_UNKNOWN )
			edition = UBC_EDITION_STANDARD;
	}

	return edition;
}

LANGID GetLangIDfromParameter()
{
	static LANGID land_id = MAXWORD;
	if( land_id == MAXWORD )
	{
		for(int i=0; i<__argc; i++)
		{
			if( stricmp(__argv[i], "+eng") == 0 )
			{
				land_id = LANG_ENGLISH;
				break;
			}
			if( stricmp(__argv[i], "+jpn") == 0 )
			{
				land_id = LANG_JAPANESE;
				break;
			}
			if( stricmp(__argv[i], "+kor") == 0 )
			{
				land_id = LANG_KOREAN;
				break;
			}
		}
		if( land_id == MAXWORD ) land_id = 0;
	}

	return land_id;
}

LPCSTR GetDefaultFontName()
{
	static CString str_font_name = "";

	if( str_font_name.GetLength() == 0 )
	{
		str_font_name.LoadString(IDS_DISPLAY_FONT_NAME);

		if( str_font_name.GetLength() == 0 )
			str_font_name = "Microsoft Sans Serif";
	}

	return str_font_name;
}

LPCSTR LoadString(UINT nID)
{
	static CString str;

	str = "";
	str.LoadString(nID);

	return str;
}
