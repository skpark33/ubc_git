// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "Installer.h"
#include "UBCInstaller.h"

#include <shlwapi.h>
#include <Dbghelp.h>
#include <io.h>

#include "ShockwaveFlash.h"
#include "MessageBoxDlg.h"
#include "AutoLoginDlg.h"


#include "common/libScratch/scratchUtil.h"


CInstaller::CInstaller()
{
}

CInstaller::~CInstaller()
{
}

INSTALL_STATUS CInstaller::Check(INSTALL_ITEM eItem)
{
	__DEBUG__(("Check(%d)", (int)eItem));

	switch( eItem )
	{
	case INSTALL_ITEM_VNC:							return CheckVNC();
	case INSTALL_ITEM_KLITE_CODEC:					return CheckKLiteCodec();
	case INSTALL_ITEM_FLASHPLAYER:					return CheckFlashplayer();
	case INSTALL_ITEM_SHORTCUT:						return CheckShortCut();
	case INSTALL_ITEM_FIREWALL_EXCEPTION:			return CheckFirewallException();
	case INSTALL_ITEM_SECURITY:						return CheckSecurity();
	case INSTALL_ITEM_SOFT_DRIVE:					return CheckSoftDrive();
	case INSTALL_ITEM_AUTO_LOGIN:					return CheckAutoLogin();
	case INSTALL_ITEM_TASKBAR:						return CheckTaskbar();
	case INSTALL_ITEM_FLASHPLAYER_UPDATE:			return CheckFlashUpdate();
	case INSTALL_ITEM_WINDOWS_UPDATE:				return CheckWindowsUpdate();

	case INSTALL_ITEM_CLEANUP_WIZARD:				return CheckCleanWizard();
	case INSTALL_ITEM_SCREEN_SAVER_XP:				return CheckScreenSaverForXP();
	case INSTALL_ITEM_SECURITY_CENTER_ALARM:		return CheckSecurityCenterAlarm();
	case INSTALL_ITEM_WINDOWS_ERROR_REPORTING_XP:	return CheckErrorReportingForXP();

	case INSTALL_ITEM_UAC_VISTA:					return CheckUACForVista();
	case INSTALL_ITEM_SCREEN_SAVER_VISTA:			return CheckScreenSaverForVista();
	case INSTALL_ITEM_SIDE_BAR:						return CheckSideBar();
	case INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_VISTA:return CheckTrayIconNotificationsForVista();
	case INSTALL_ITEM_AERO_THEME_VISTA:				return CheckAeroThemeForVista();
	case INSTALL_ITEM_CUSTOMER_EXPERIENCE:			return CheckCustomerExperience();
	case INSTALL_ITEM_WELCOME_CENTER:				return CheckWelcomeCenter();
	case INSTALL_ITEM_WINDOWS_ERROR_REPORTING_VISTA:return CheckErrorReportingForVista();

	case INSTALL_ITEM_UAC_7:						return CheckUACFor7();
	case INSTALL_ITEM_SCREEN_SAVER_7:				return CheckScreenSaverFor7();
	case INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_7:	return CheckTrayIconNotificationsFor7();
	case INSTALL_ITEM_AERO_THEME_7:					return CheckAeroThemeFor7();
	case INSTALL_ITEM_WINDOWS_ERROR_REPORTING_7:	return CheckErrorReportingFor7();
	case INSTALL_ITEM_CODEC_CONFIG:					return CheckCodecConfig();

	case INSTALL_ITEM_UAC_8:						return CheckUACFor8();
	case INSTALL_ITEM_SCREEN_SAVER_8:				return CheckScreenSaverFor8();
	case INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_8:	return CheckTrayIconNotificationsFor8();
	case INSTALL_ITEM_WINDOWS_ERROR_REPORTING_8:	return CheckErrorReportingFor8();
	case INSTALL_ITEM_CODEC_CONFIG_8:				return CheckCodecConfigFor8();
	case INSTALL_ITEM_HOT_CORNERS_8:				return CheckHotCornersFor8();
	case INSTALL_ITEM_BOOT_DESKTOP_8:				return CheckBootDesktopFor8();
	case INSTALL_ITEM_LOCK_SCREEN_8:				return CheckLockScreenFor8();
	}

	__ERROR__(("Unknown Check Item !!!"));
	return INSTALL_STATUS_UNKNOWN;
}

INSTALL_STATUS CInstaller::Install(INSTALL_ITEM eItem)
{
	__DEBUG__(("Install(%d)", (int)eItem));

	switch( eItem )
	{
	case INSTALL_ITEM_VNC:							return InstallVNC();
	case INSTALL_ITEM_KLITE_CODEC:					return InstallKLiteCodec();
	case INSTALL_ITEM_FLASHPLAYER:					return InstallFlashplayer();
	case INSTALL_ITEM_SHORTCUT:						return InstallShortCut();
	case INSTALL_ITEM_FIREWALL_EXCEPTION:			return InstallFirewallException();
	case INSTALL_ITEM_SECURITY:						return InstallSecurity();
	case INSTALL_ITEM_SOFT_DRIVE:					return InstallSoftDrive();
	case INSTALL_ITEM_AUTO_LOGIN:					return InstallAutoLogin();
	case INSTALL_ITEM_TASKBAR:						return InstallTaskbar();
	case INSTALL_ITEM_FLASHPLAYER_UPDATE:			return InstallFlashUpdate();
	case INSTALL_ITEM_WINDOWS_UPDATE:				return InstallWindowsUpdate();

	case INSTALL_ITEM_CLEANUP_WIZARD:				return InstallCleanWizard();
	case INSTALL_ITEM_SCREEN_SAVER_XP:				return InstallScreenSaverForXP();
	case INSTALL_ITEM_SECURITY_CENTER_ALARM:		return InstallSecurityCenterAlarm();
	case INSTALL_ITEM_WINDOWS_ERROR_REPORTING_XP:	return InstallErrorReportingForXP();

	case INSTALL_ITEM_UAC_VISTA:					return InstallUACForVista();
	case INSTALL_ITEM_SCREEN_SAVER_VISTA:			return InstallScreenSaverForVista();
	case INSTALL_ITEM_SIDE_BAR:						return InstallSideBar();
	case INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_VISTA:return InstallTrayIconNotificationsForVista();
	case INSTALL_ITEM_AERO_THEME_VISTA:				return InstallAeroThemeForVista();
	case INSTALL_ITEM_CUSTOMER_EXPERIENCE:			return InstallCustomerExperience();
	case INSTALL_ITEM_WELCOME_CENTER:				return InstallWelcomeCenter();
	case INSTALL_ITEM_WINDOWS_ERROR_REPORTING_VISTA:return InstallErrorReportingForVista();

	case INSTALL_ITEM_UAC_7:						return InstallUACFor7();
	case INSTALL_ITEM_SCREEN_SAVER_7:				return InstallScreenSaverFor7();
	case INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_7:	return InstallTrayIconNotificationsFor7();
	case INSTALL_ITEM_AERO_THEME_7:					return InstallAeroThemeFor7();
	case INSTALL_ITEM_WINDOWS_ERROR_REPORTING_7:	return InstallErrorReportingFor7();
	case INSTALL_ITEM_CODEC_CONFIG:					return InstallCodecConfig();

	case INSTALL_ITEM_UAC_8:						return InstallUACFor8();
	case INSTALL_ITEM_SCREEN_SAVER_8:				return InstallScreenSaverFor8();
	case INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_8:	return InstallTrayIconNotificationsFor8();
	case INSTALL_ITEM_WINDOWS_ERROR_REPORTING_8:	return InstallErrorReportingFor8();
	case INSTALL_ITEM_CODEC_CONFIG_8:				return InstallCodecConfigFor8();
	case INSTALL_ITEM_HOT_CORNERS_8:				return InstallHotCornersFor8();
	case INSTALL_ITEM_BOOT_DESKTOP_8:				return InstallBootDesktopFor8();
	case INSTALL_ITEM_LOCK_SCREEN_8:				return InstallLockScreenFor8();
	}

	__ERROR__(("Unknown Install Item !!!"));
	return INSTALL_STATUS_UNKNOWN;
}

BOOL CInstaller::AddProcessToRun()
{
	__FUNC_BEGIN__

	CString strPath = "";

	if( __argc > 0 )
	{
		strPath += "\"";
		strPath += __argv[0];
		strPath += "\"";
	}

	for(int i=1; i<__argc; i++)
	{
		strPath += " ";
		strPath += __argv[i];
	}

	__DEBUG__(("AutoRun Full-Path=%s", strPath));

	BOOL ret_val = SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", "UBCInstaller", strPath);
	if( !ret_val )
	{
		__ERROR__(("Fail to regist auto-run in startup group !!!"));
	}

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::DeleteProcessFromRun()
{
	__FUNC_BEGIN__

	BOOL ret_val = DeleteRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", "UBCInstaller");
	if( !ret_val )
	{
		__WARN__(("Fail to delete auto-run in startup group !!!"));
	}

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::Reboot()
{
	__FUNC_BEGIN__

	CString str_output;
	BOOL ret_val = CreateProcessRedirection(GetSystemPath(), "shutdown.exe /r /f /t 3", str_output);

	int pos = 0;
	CString str_line = str_output.Tokenize(" \t\r\n", pos);
	if( !ret_val || str_line != "" )
	{
		__ERROR__(("Fail to shutdown !!! (%s)", str_output));
	}

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::PopupUACfor7()
{
	__FUNC_BEGIN__

	BOOL ret_val = CreateProcess(GetSystemPath(), "UserAccountControlSettings.exe", 0, SW_SHOW);
	if( !ret_val )
	{
		__ERROR__(("Fail to popup UAC-setting-dialog !!!"));
	}

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::StopPcaSvc()
{
	__FUNC_BEGIN__

	CString str_output;
	BOOL ret_val = CreateProcessRedirection(GetSystemPath(), "net.exe stop PcaSvc", str_output);

	if( !ret_val || // fail to run
		(str_output.Find("NET HELPMSG") > 0 && str_output.Find("3521") < 0) ) // not started
	{
		__ERROR__(("Fail to stop Program-Compatibility-Assistant-Service !!! (%s)", str_output));
	}

	__FUNC_END__
	return ret_val;
}

OS_VERSION CInstaller::GetWindowsVersion()
{
	__FUNC_BEGIN__

	static OS_VERSION os_ver = OS_NOT_SUPPORT;
	if( os_ver != OS_NOT_SUPPORT )
	{
		__DEBUG__(("WinVer=%s", szOSVersion[os_ver]));
		__FUNC_END__
		return os_ver;
	}

	// get os ver. from wmic
	CString str_path = GetSystemPath();
	str_path += "Wbem";

	CString str_output = "";
	CreateProcessRedirection(str_path, "wmic.exe os get caption", str_output);

	// output example...

	//Caption
	//Microsoft Windows 7 Home Premium

	//Caption
	//Microsoft Windows XP Professional

	str_output.MakeLower();
	if(      str_output.Find("windows xp") >= 0 )		os_ver = OS_WIN_XP;
	else if( str_output.Find("windows vista") >= 0 )	os_ver = OS_WIN_VISTA;
	else if( str_output.Find("windows 7") >= 0 )		os_ver = OS_WIN_7;
	else if( str_output.Find("windows 8") >= 0 )		os_ver = OS_WIN_8;
	else if( str_output.Find("windows 9") >= 0 )		os_ver = OS_WIN_9;
	else if( str_output.Find("windows server") >= 0 )
	{
		__WARN__(("Windows Server series isn't support !!!"));
		__FUNC_END__
		return os_ver;
	}

	if( os_ver != OS_NOT_SUPPORT )
	{
		__DEBUG__(("WinVer=%s", szOSVersion[os_ver]));
		__FUNC_END__
		return os_ver;
	}

	// get os ver. from API
	OSVERSIONINFOEX osvi = {0};
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

	if( !GetVersionEx((OSVERSIONINFO*)&osvi) )
	{
		osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		if ( !GetVersionEx((OSVERSIONINFO*)&osvi) )
		{
			__ERROR__(("Fail to GetVersionEx !!!"));
			__FUNC_END__
			return OS_NOT_SUPPORT;
		}
	}

	// 6.3 : Windows 8.1
	// 6.2 : Windows 8
	// 6.1 : Windows 7		|| Windows Server 2008 R2
	// 6.0 : Windows Vista	|| Windows Server 2008
	// 5.1 : Windows XP
	if( osvi.dwMajorVersion == 6 && (osvi.dwMinorVersion == 3 || osvi.dwMinorVersion == 2) )
		os_ver = OS_WIN_8;
	else if( osvi.dwMajorVersion == 6 && osvi.dwMinorVersion == 1 )
		os_ver = OS_WIN_7;
	else if( osvi.dwMajorVersion == 6 && osvi.dwMinorVersion == 0 )
		os_ver = OS_WIN_VISTA;
	else if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
		os_ver = OS_WIN_XP;

	__DEBUG__(("WinVer=%s", szOSVersion[os_ver]));
	__FUNC_END__
	return os_ver;
}

typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle(_T("kernel32")),_T("IsWow64Process"));

BOOL CInstaller::IsWow64()
{
	__FUNC_BEGIN__

	if( GetWindowsVersion() <= OS_WIN_XP )
	{
		__WARN__(("Win_XP or NOT_SUPPORT_OS can't check x64 !!!"));
		__FUNC_END__
		return FALSE;
	}

	BOOL bIsWow64 = FALSE; 
	if( NULL != fnIsWow64Process )
	{
		if( !fnIsWow64Process(GetCurrentProcess(), &bIsWow64) )
		{
			// handle error
			__ERROR__(("Fail to execute IsWow64Process() function !!!"));
		}
	}
	else
	{
		__ERROR__(("Fail to get IsWow64Process() function !!!"));
	}

	__FUNC_END__
	return bIsWow64;
}

BOOL CInstaller::PopupLicenseCenter()
{
	__FUNC_BEGIN__

	CString str_cmd = "UBCHostAuthorizer.exe ";

	switch( GetUBCEdition() )
	{
	default:
	case UBC_EDITION_STANDARD:
		break;
	case UBC_EDITION_STANDARD_PLUS:
		str_cmd += "+PLS ";
		break;
	case UBC_EDITION_ENTERPRISE:
		str_cmd += "+ENT ";
		break;
	}

	BOOL ret_val = CreateProcess(GetAppPath(), str_cmd, 0, SW_SHOW);
	if( !ret_val )
	{
		__ERROR__(("Fail to execute License-Center (%s) !!!", str_cmd));
	}

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::IsExistLicenseInfo()
{
	__FUNC_BEGIN__

	CString str_fn = GetAppPath();
	str_fn += "project.dat";

	CFileStatus fs;
	if( !CFile::GetStatus(str_fn, fs) )
	{
		__WARN__(("Fail to get License-Info (%s) !!!", str_fn));
		__FUNC_END__
		return FALSE;
	}

	__FUNC_END__
	return TRUE;
}

////////////////////////////////////////////////////////////////
BOOL CInstaller::CreateProcessRedirection(LPCSTR lpszDir, LPCSTR lpszCmdLine, CString& strOutputString)
{
	__FUNC_BEGIN__

	CString str_cmd = lpszDir;
	if( str_cmd.GetLength() > 0 )
	{
		if( str_cmd.Right(1) != "\\" )
			str_cmd += "\\";
	}
	str_cmd += lpszCmdLine;

	__DEBUG__(("CommandLine=%s", str_cmd));

	//
	HANDLE hReadPipe, hWritePipe;

	SECURITY_ATTRIBUTES securityAttr;
	ZeroMemory(&securityAttr,sizeof(securityAttr));
	securityAttr.nLength = sizeof(securityAttr);
	securityAttr.bInheritHandle = TRUE;

	CreatePipe(&hReadPipe, &hWritePipe, &securityAttr, 0);

	STARTUPINFO sStartupInfo = {0};
	PROCESS_INFORMATION process_Info = {0};

	sStartupInfo.cb = sizeof(STARTUPINFO);
	sStartupInfo.dwFlags = STARTF_USESTDHANDLES;
	sStartupInfo.hStdInput = NULL;
	sStartupInfo.hStdOutput = hWritePipe;
	sStartupInfo.hStdError = hWritePipe;

	BOOL bSuccessProcess = ::CreateProcess(0, (LPSTR)(LPCSTR)str_cmd, 
										0, 0, 
										TRUE, NORMAL_PRIORITY_CLASS|CREATE_NO_WINDOW, 
										0, lpszDir, 
										&sStartupInfo, &process_Info);
	CloseHandle(hWritePipe);

	if(FALSE == bSuccessProcess)
	{
		CloseHandle(hReadPipe);

		__ERROR__(("Fail to execute (%s) !!!", str_cmd));
		__FUNC_END__
		return FALSE;
	}

	strOutputString.Empty();
	BOOL bResult = TRUE;
	do
	{
		char szTemp[1024] = {0};
		DWORD dwReadByte = 0;
		bResult = ::ReadFile(hReadPipe, szTemp, sizeof(szTemp)-1, &dwReadByte, 0);

		if(TRUE == bResult)
		{
			strOutputString += szTemp;
		}
	} while(TRUE == bResult);

	CloseHandle(hReadPipe);

	__FUNC_END__
	return TRUE;
}

BOOL CInstaller::CreateProcess(LPCSTR lpszDir, LPCSTR lpszCmdLine, DWORD dwTimeout, WORD wShowWindow)
{
	__FUNC_BEGIN__

	PROCESS_INFORMATION pi = {0};
	STARTUPINFO si = {0};
	si.cb = sizeof(STARTUPINFO);
	si.wShowWindow = wShowWindow;

	CString str_cmd = lpszDir;
	if( str_cmd.GetLength() > 0 )
	{
		if( str_cmd.Right(1) != "\\" )
			str_cmd += "\\";
	}
	str_cmd += lpszCmdLine;

	__DEBUG__(("CommandLine=%s", str_cmd));

	if( !::CreateProcess(NULL, (LPSTR)(LPCSTR)str_cmd, NULL, NULL, FALSE, 0/*CREATE_NO_WINDOW*/, NULL, lpszDir, &si, &pi) )
	{
		__ERROR__(("Fail to execute (%s) !!!", str_cmd));
		__FUNC_END__
		return FALSE;
	}

	if( dwTimeout > 0 )
	{
		if( ::WaitForSingleObject(pi.hProcess, dwTimeout) == WAIT_TIMEOUT )
		{
			::CloseHandle(pi.hThread);
			::CloseHandle(pi.hProcess);

			__WARN__(("Timeout (%s) !!!", str_cmd));
			__FUNC_END__
			return FALSE;
		}
	}

	::CloseHandle(pi.hThread);
	::CloseHandle(pi.hProcess);

	__FUNC_END__
	return TRUE;
}

LPCTSTR CInstaller::GetSetACLFilename()
{
	static CString strSetACLFilename = "";

	if( strSetACLFilename.GetLength() == 0 )
		strSetACLFilename = ( IsWow64() ? "SetACL_x64.exe" : "SetACL_x86.exe" );

	return strSetACLFilename;
}

BOOL CInstaller::ExecuteSetACL(HKEY hMainKey, LPCSTR lpszSubKey)
{
	__FUNC_BEGIN__

	if( lpszSubKey == NULL )
	{
		__WARN__(("SubKey is NULL !!! "));
		__FUNC_END__
		return FALSE;
	}

	CString str_key;
	if     ( hMainKey == HKEY_CLASSES_ROOT   )	str_key = "HKEY_CLASSES_ROOT";
	else if( hMainKey == HKEY_CURRENT_USER   )	str_key = "HKEY_CURRENT_USER";
	else if( hMainKey == HKEY_LOCAL_MACHINE  )	str_key = "HKEY_LOCAL_MACHINE";
	else if( hMainKey == HKEY_USERS          )	str_key = "HKEY_USERS";
	else if( hMainKey == HKEY_CURRENT_CONFIG )	str_key = "HKEY_CURRENT_CONFIG";

	if( str_key.GetLength() == 0 )
	{
		__WARN__(("Invalid Main Key !!! "));
		__FUNC_END__
		return FALSE;
	}
	if( lpszSubKey[0] != '\\') str_key += "\\";
	str_key += lpszSubKey;

	// setacl
	// set owner
	CString str_path = Get3rdpartyPath();
	str_path += "SetACL\\";

	CString str_cmd, str_output;
	str_cmd.Format("%s -on \"%s\" -ot reg -actn setowner -ownr \"n:administrators\"", 
		GetSetACLFilename(),
		str_key );
	if( !CreateProcessRedirection(str_path, str_cmd, str_output) )
	{
		// fail to execute setacl
		__ERROR__(("Fail to execute (%s) !!!", str_cmd));
		__ERROR__(("Output=%s", str_output));
		return FALSE;
	}
	else if( str_output.Find("SetACL finished successfully") < 0 )
	{
		// fail to setacl
		__ERROR__(("Fail to SetACL (%s) !!!", str_cmd));
		__ERROR__(("Output=%s", str_output));
		return FALSE;
	}

	// set access-control
	str_cmd.Format("%s -on \"%s\" -ot reg -actn ace -ace \"n:administrators;p:full\"", 
		GetSetACLFilename(),
		str_key );
	if( !CreateProcessRedirection(str_path, str_cmd, str_output) )
	{
		// fail to execute setacl
		__ERROR__(("Fail to execute (%s) !!!", str_cmd));
		__ERROR__(("Output=%s", str_output));
		return FALSE;
	}
	else if( str_output.Find("SetACL finished successfully") < 0 )
	{
		// fail to setacl
		__ERROR__(("Fail to SetACL (%s) !!!", str_cmd));
		__ERROR__(("Output=%s", str_output));
		return FALSE;
	}

	__FUNC_END__
	return TRUE;
}

LPCTSTR CInstaller::GetProgramFilesPath()
{
	static CString strProgramFilesDir = "";

	if(strProgramFilesDir.GetLength() == 0)
	{
		TCHAR szPath[MAX_PATH] = { 0x00 };
		if(!SHGetSpecialFolderPath(NULL, szPath, CSIDL_PROGRAM_FILES, FALSE))
		{
			strProgramFilesDir = "C:\\Program Files\\";
		}
		else
		{
			strProgramFilesDir = szPath;
			strProgramFilesDir += "\\";
		}
	}

	return strProgramFilesDir;
}

LPCTSTR	CInstaller::GetStartupPath()
{
	static CString strStartupDir = "";

	if(strStartupDir.GetLength() == 0)
	{
		TCHAR szPath[MAX_PATH] = { 0x00 };
		::SHGetSpecialFolderPath (NULL, szPath, CSIDL_STARTUP, FALSE);

		strStartupDir = szPath;
		strStartupDir += "\\";
	}

	return strStartupDir;
}

LPCTSTR CInstaller::GetSystemPath()
{
	static CString strSystemDir = "";

	if(strSystemDir.GetLength() == 0)
	{
		TCHAR szPath[MAX_PATH] = { 0x00 };
		::SHGetSpecialFolderPath (NULL, szPath, CSIDL_SYSTEM, FALSE);

		strSystemDir = szPath;
		strSystemDir += "\\";
	}

	return strSystemDir;
}

LPCTSTR CInstaller::GetAppPath()
{
	static CString strAppPath = "";

	if(strAppPath.GetLength() == 0)
	{
		CString strPath;
		TCHAR szModule[MAX_PATH]={0};
		::ZeroMemory(szModule, MAX_PATH);
		::GetModuleFileName(NULL, szModule, MAX_PATH);

		TCHAR szDrive[MAX_PATH]={0}, szPath[MAX_PATH]={0}, szFilename[MAX_PATH]={0}, szExt[MAX_PATH]={0};
		_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);
		strPath = szDrive;
		strPath += szPath;

		strAppPath = strPath;
		strAppPath += "\\";
	}

	return strAppPath;
}

LPCTSTR CInstaller::Get3rdpartyPath()
{
	static CString str3rdPath = "";

	if(str3rdPath.GetLength() == 0)
	{
		CString str_app_path = GetAppPath();

		if( str_app_path.GetLength() > 3 )
		{
			str3rdPath = str_app_path.Left(3); //	C:/
			str3rdPath += "SQISoft\\3rdparty\\";
		}
	}

	return str3rdPath;
}

LPCTSTR	CInstaller::GetVNCPath()
{
	static CString str_vnc_path = "";

	if( str_vnc_path.GetLength() == 0 )
	{
		//
		CString str_path[2];

		str_path[0] = GetProgramFilesPath();
		str_path[0] += "UltraVNC\\";

		str_path[1] = GetProgramFilesPath();
		str_path[1] += "uvnc bvba\\UltraVNC\\";

		//
		bool find = false;
		for(int i=0; i<2; i++)
		{
			if( ::_access(str_path[i], 0) == 0 )
			{
				str_vnc_path = str_path[i];
				break;
			}
		}
	}

	return str_vnc_path;
}

LPCTSTR CInstaller::GetSIDofUserAccount()
{
	__FUNC_BEGIN__

	static CString str_sid = "";
	if( str_sid.GetLength() > 0 )
	{
		__DEBUG__(("SID=%s", str_sid));
		__FUNC_END__
		return str_sid;
	}

	// get sid from whoami.exe
	CString str_output;
	CreateProcessRedirection(GetSystemPath(), "whoami.exe /user", str_output);

	// output example...

	//사용자 정보
	//----------------
	//
	//사용자 이름                  SID
	//============================ ============================================
	//ubcsvr-225\administrator     S-1-5-21-264050421-3702377799-2534421462-500


	// find '====...' line
	bool find_delimiter = false;
	int pos = 0;
	CString str_line;
	while( (str_line=str_output.Tokenize("\r\n",pos)) != "" )
	{
		if( str_line.Find("==") >= 0 )
		{
			find_delimiter = true;
			break;
		}
	}

	if( find_delimiter )
	{
		str_line = str_output.Tokenize("\r\n",pos);

		pos = 0;
		str_sid = str_line.Tokenize(" ",pos); // find first-token
		if( str_sid != "" )
		{
			str_sid = str_line.Tokenize(" ", pos); // find second-token
			str_sid.Trim();

			__DEBUG__(("SID=%s", str_sid));
			__FUNC_END__
			return str_sid; // second token is SID
		}
		str_sid = "";
	}

	// get sid from wmic.exe
	const char* user_name = ::getenv("USERNAME");
	if( user_name == NULL )
	{
		__ERROR__(("USERNAME-env is NULL !!!"));
		__FUNC_END__
		return "";
	}

	CString str_path = GetSystemPath();
	str_path += "Wbem";

	CString str_cmd = "wmic.exe useraccount where name='";
	str_cmd += user_name;
	str_cmd += "' get sid";

	str_output = "";
	CreateProcessRedirection(str_path, str_cmd, str_output);

	// output example...

	//SID
	//S-1-5-21-264050421-3702377799-2534421462-500

	pos = 0;
	str_line = str_output.Tokenize("\r\n",pos); // find first-token
	str_line.Trim();
	if( str_line == "SID" )
	{
		str_sid = str_output.Tokenize("\r\n",pos); // find second-token
		str_sid.Trim();

		__DEBUG__(("SID=%s", str_sid));
		__FUNC_END__
		return str_sid; // second-token is SID
	}

	__ERROR__(("SID is NULL !!!"));
	__FUNC_END__
	return "";
}

////////////////////////////////////////////////////////////////////////////////////////////
CString CInstaller::GetErrorMessage(LONG nCode)
{
	LPVOID lpMsgBuf = NULL;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
				| FORMAT_MESSAGE_IGNORE_INSERTS
				| FORMAT_MESSAGE_FROM_SYSTEM
				, NULL
				, nCode
				, 0
				, (LPTSTR)&lpMsgBuf
				, 0
				, NULL );

	CString strErrMsg = (LPCSTR)lpMsgBuf;
	LocalFree( lpMsgBuf );

	return strErrMsg;
}

BOOL CInstaller::CreateRegKey(HKEY hMainKey, LPCSTR lpszSubKey, DWORD dwOption)
{
	__FUNC_BEGIN__

	HKEY hKey;
	BOOL ret_val = TRUE;
	LONG lRet = ::RegCreateKeyEx(hMainKey, lpszSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS | dwOption, NULL, &hKey, NULL);
	if( lRet != ERROR_SUCCESS )
	{
		__WARN__(("Fail to create key (%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, GetErrorMessage(lRet)));
		ret_val = FALSE;
	}
	RegCloseKey(hKey);

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::DeleteRegKey(HKEY hMainKey, LPCSTR lpszSubKey, DWORD dwOption)
{
	__FUNC_BEGIN__

	BOOL ret_val = TRUE;
	LONG lRet = ::RegDeleteKeyEx(hMainKey, lpszSubKey, KEY_ALL_ACCESS | dwOption, NULL);
	if( lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to delete key (%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, GetErrorMessage(lRet)));
		ret_val = FALSE;
	}

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::GetRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, DWORD& dwValue, DWORD dwOption)
{
	__FUNC_BEGIN__

	HKEY hKey;
	DWORD dwSize = sizeof(DWORD);
	DWORD dwType = REG_DWORD;

	LONG lRet = ::RegOpenKeyEx(hMainKey, lpszSubKey, 0, KEY_ALL_ACCESS | dwOption, &hKey);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to open key (%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, GetErrorMessage(lRet)));
		__FUNC_END__
		return FALSE;
	}

	BOOL ret_val = TRUE;
	lRet = RegQueryValueEx(hKey, lpszValueName, NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to get value (%s\\%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, lpszValueName, GetErrorMessage(lRet)));
		ret_val = FALSE;
	}
	RegCloseKey(hKey);

	__DEBUG__(("Value=%u", dwValue));
	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::GetRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, CString& strValue, DWORD dwOption)
{
	__FUNC_BEGIN__

	HKEY hKey;
	DWORD dwSize = 1024;
	DWORD dwType = REG_SZ;
	char szValue[1024] = {0};

	LONG lRet = ::RegOpenKeyEx(hMainKey, lpszSubKey, 0, KEY_ALL_ACCESS | dwOption, &hKey);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to open value (%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, GetErrorMessage(lRet)));
		__FUNC_END__
		return FALSE;
	}

	BOOL ret_val = TRUE;
	lRet = RegQueryValueEx(hKey, lpszValueName, NULL, &dwType, (LPBYTE)&szValue, &dwSize);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to get value (%s\\%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, lpszValueName, GetErrorMessage(lRet)));
		ret_val = FALSE;
	}

	RegCloseKey(hKey);

	strValue = szValue;

	__DEBUG__(("Value=%s", szValue));
	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::SetRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, DWORD dwValue, DWORD dwOption)
{
	__FUNC_BEGIN__

	HKEY hKey;
	DWORD dwSize = sizeof(DWORD);
	DWORD dwType = REG_DWORD;

	LONG lRet = ::RegOpenKeyEx(hMainKey, lpszSubKey, 0, KEY_ALL_ACCESS | dwOption, &hKey);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to open value (%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, GetErrorMessage(lRet)));
		__FUNC_END__
		return FALSE;
	}

	BOOL ret_val = TRUE;
	lRet = RegSetValueEx(hKey, lpszValueName, NULL, dwType, (LPBYTE)&dwValue, dwSize);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to set value (%s\\%s\\%s=%u) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, lpszValueName, dwValue, GetErrorMessage(lRet)));
		ret_val = FALSE;
	}
	RegCloseKey(hKey);

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::SetRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, CString strValue, DWORD dwOption)
{
	__FUNC_BEGIN__

	HKEY hKey;
	DWORD dwSize = strValue.GetLength();
	DWORD dwType = REG_SZ;

	LONG lRet = ::RegOpenKeyEx(hMainKey, lpszSubKey, 0, KEY_ALL_ACCESS | dwOption, &hKey);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to open value (%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, GetErrorMessage(lRet)));
		__FUNC_END__
		return FALSE;
	}

	BOOL ret_val = TRUE;
	lRet = RegSetValueEx(hKey, lpszValueName, NULL, dwType, (LPBYTE)(LPCSTR)strValue, dwSize);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to set value (%s\\%s\\%s=%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, lpszValueName, strValue, GetErrorMessage(lRet)));
		ret_val = FALSE;
	}
	RegCloseKey(hKey);

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::SetRegValueIfExist(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, DWORD dwValue, DWORD dwOption)
{
	__FUNC_BEGIN__

	DWORD value = 0;
	if( !GetRegValue(hMainKey, lpszSubKey, lpszValueName, value, dwOption) )
	{
		__WARN__(("Value is not exist (%s\\%s\\%s) !!!", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, lpszValueName));
		__FUNC_END__
		return FALSE;
	}

	if( value == dwValue )
	{
		__DEBUG__(("Value is Equal"));
		__FUNC_END__
		return TRUE;
	}

	BOOL ret_val = SetRegValue(hMainKey, lpszSubKey, lpszValueName, dwValue, dwOption);
	if( !ret_val )
	{
		__WARN__(("Fail to set value (%s\\%s\\%s=%u) !!!", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, lpszValueName, dwValue));
	}

	__FUNC_END__
	return ret_val;
}

BOOL CInstaller::DeleteRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, DWORD dwOption)
{
	__FUNC_BEGIN__

	HKEY hKey;

	LONG lRet = ::RegOpenKeyEx(hMainKey, lpszSubKey, 0, KEY_ALL_ACCESS | dwOption, &hKey);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to open value (%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, GetErrorMessage(lRet)));
		__FUNC_END__
		return FALSE;
	}

	BOOL ret_val = TRUE;
	lRet = RegDeleteValue(hKey, lpszValueName);
	if(lRet != ERROR_SUCCESS)
	{
		__WARN__(("Fail to delete value (%s\\%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, lpszValueName, GetErrorMessage(lRet)));
		ret_val = FALSE;
	}
	RegCloseKey(hKey);

	__FUNC_END__
	return ret_val;
}

////////////////////////////////////////////////////////////////
INSTALL_STATUS CInstaller::CheckVNC()
{
	__FUNC_BEGIN__

	//
	CString str_vnc_path = GetVNCPath();
	if( str_vnc_path.GetLength() == 0 )
	{
		__WARN__(("Fail to find vnc-folder !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	str_vnc_path += "winvnc.exe";
	CFileStatus fs;
	if( !CFile::GetStatus(str_vnc_path, fs) )
	{
		__WARN__(("Fail to find vnc-exe-file !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	DWORD value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\uvnc_service", "Start", value) )
	{
		__WARN__(("Fail to get value !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 2 )
	{
		__WARN__(("VNC service isn't auto-start !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckKLiteCodec()
{
	__FUNC_BEGIN__

	//
	CString str_vnc_path[2];
	str_vnc_path[0] = GetProgramFilesPath();
	str_vnc_path[0] += "K-Lite Codec Pack\\ffdshow\\ffdshow.ax"; // old version (5.x)
	str_vnc_path[1] = GetProgramFilesPath();
	str_vnc_path[1] += "K-Lite Codec Pack\\Filters\\ffdshow\\ffdshow.ax"; // new version (10.x)

	//
	bool find = false;
	for(int i=0; i<2; i++)
	{
		CFileStatus fs;
		if( CFile::GetStatus(str_vnc_path[i], fs) )
		{
			find = true;
			break;
		}
	}

	if( !find )
	{
		__WARN__(("Fail to find codec-file !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	DWORD value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\GNU\\ffdshow", "trayIcon", value) )
	{
		__WARN__(("Fail to find codec-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 0 )
	{
		__WARN__(("Codec registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckFlashplayer()
{
	__FUNC_BEGIN__

	CString str_value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "Software\\Macromedia\\FlashPlayerActiveX", "PlayerPath", str_value, KEY_WOW64_32KEY) )
	{
		__WARN__(("Fail to get flash-player-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	CFileStatus fs;
	if( !CFile::GetStatus(str_value, fs) )
	{
		__WARN__(("Fail to find flash-player-file (%s) !!!", str_value));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	CShockwaveFlash flash;
	int ret_val = flash.Create("FLASH", "CFlash", WS_CHILD, CRect(0,0,0,0), ::AfxGetMainWnd(), 0xfeff);
	//flash.SetOwner(::AfxGetMainWnd());

	if( !ret_val )
	{
		__WARN__(("Fail to create flash-player-object !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	flash.DestroyWindow();

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckShortCut()
{
	__FUNC_BEGIN__

	CString str_ready_path = GetStartupPath();
	str_ready_path += "UBCReady.lnk";

	if( !PathFileExists(str_ready_path) )
	{
		__WARN__(("Fail to find shortcut (%s) !!!", str_ready_path));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckFirewallException()
{
	__FUNC_BEGIN__

	CString str_fw_ex_path = GetAppPath();
	if( IsWow64() )
		str_fw_ex_path += "firewallException_x64.txt";
	else
		str_fw_ex_path += "firewallException.txt";

	TRY
	{
		CFile file_fw_ex;
		if( !file_fw_ex.Open(str_fw_ex_path, CFile::modeRead | CFile::typeBinary) )
		{
			__WARN__(("Fail to open firewall-file (%s) !!!", str_fw_ex_path));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		int file_size = file_fw_ex.GetLength();
		char* buf = new char[file_size+1];
		::ZeroMemory(buf, file_size+1);

		UINT read_size = file_fw_ex.Read(buf, file_size);
		if( read_size != file_size )
		{
			__WARN__(("Fail to read firewall-file !!! (rsize=%u, fsize=%u)", read_size, file_size));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		CString str_buf = buf;
		delete[]buf;

		int pos = 0;
		CString str_line;

		while( (str_line=str_buf.Tokenize("\r\n", pos)) != "" )
		{
			int pos_sub = 0;
			CString str_left = str_line.Tokenize(",", pos_sub);

			if( !scratchUtil::getInstance()->IsAppExceptionAdded((LPCSTR)str_left) )
			{
				__WARN__(("(%s) isn't set in firewall !!!", str_left));
				__FUNC_END__
				return INSTALL_STATUS_NOT_INSTALL;
			}
		}
	}
	CATCH(CFileException, ex)
	{
		__ERROR__(("FileException !!! (%s)", GetErrorMessage(ex->m_cause)));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	END_CATCH

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckSecurity()
{
	__FUNC_BEGIN__

	CString str_app_path = GetAppPath();
	if( str_app_path.GetLength() < 3 ) 
	{
		__WARN__(("Invalid path (%s) !!!", str_app_path));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	CString str_contents_path = str_app_path.Left(3);
	str_contents_path += "SQISoft\\Contents";

	CFileFind ff;
	if( !ff.FindFile(str_contents_path) )
	{
		__WARN__(("Invalid path !!! (%s)", GetErrorMessage(::GetLastError())));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	ff.FindNextFile();
	if( !ff.IsHidden() )
	{
		__WARN__(("Contents path isn't hidded !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckSoftDrive()
{
	__FUNC_BEGIN__

	if( !PathFileExists("X:\\SqiSoft\\Contents\\ENC") )
	{
		__WARN__(("ENC-path in SoftDrive isn't exist !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( !PathFileExists("X:\\SqiSoft\\Contents\\Temp") )
	{
		__WARN__(("Temp-path in SoftDrive isn't exist !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( !PathFileExists("X:\\SQISoft\\UTV1.0\\execute\\config") )
	{
		__WARN__(("config-path in SoftDrive isn't exist !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckAutoLogin()
{
	__FUNC_BEGIN__

	//
	CString str_auto_login_use;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "AutoAdminLogon", str_auto_login_use) )
	{
		__WARN__(("Fail to get auto-login-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( atoi(str_auto_login_use) != 1 )
	{
		__WARN__(("Auto-login-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	CString str_login_id, str_login_pwd;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultUserName", str_login_id) )
	{
		__WARN__(("Fail to find Auto-login-ID !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultPassword", str_login_pwd) )
	{
		__WARN__(("Fail to find Auto-login-password !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( str_login_id.GetLength() == 0 )
	{
		__WARN__(("Auto-login-ID isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckTaskbar()
{
	__FUNC_BEGIN__

	APPBARDATA TAppBarData = {0};
	TAppBarData.cbSize = sizeof(TAppBarData);
	TAppBarData.hWnd = (HWND)::FindWindow("Shell_TrayWnd",NULL);
	UINT state = SHAppBarMessage(ABM_GETSTATE, &TAppBarData);	

	if( !(state & ABS_AUTOHIDE) )
	{
		__WARN__(("Taskbar isn't auto-hide !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckFlashUpdate()
{
	__FUNC_BEGIN__

	CString str_flash_path = GetSystemPath();
	str_flash_path += "Macromed\\Flash\\mms.cfg";

	//
	if( !PathFileExists(str_flash_path) )
	{
		__WARN__(("Fail to find flash-update-file (%s) !!!", str_flash_path));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	TRY
	{
		CFile file_flash_cfg;
		if( !file_flash_cfg.Open(str_flash_path, CFile::modeRead | CFile::typeBinary) )
		{
			__WARN__(("Fail to open flash-update-file (%s) !!!", str_flash_path));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		int file_size = file_flash_cfg.GetLength();
		char* buf = new char[file_size+1];
		::ZeroMemory(buf, file_size+1);

		UINT read_size = file_flash_cfg.Read(buf, file_size);
		if( read_size != file_size )
		{
			__WARN__(("Fail to read flash-update-file !!! (rsize=%u, fsize=%u)", read_size, file_size));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		CString str_buf = buf;
		delete[]buf;

		int pos = 0;
		CString str_line;

		while( (str_line=str_buf.Tokenize("\r\n", pos)) != "" )
		{
			if( strnicmp(str_line, "AutoUpdateDisable", 17) == 0 )
			{
				int auto_update_disable = atoi( (LPCSTR)str_line + 18 );
				if( auto_update_disable == 0 )
				{
					__WARN__(("AutoUpdateDisable isn't set !!!"));
					__FUNC_END__
					return INSTALL_STATUS_NOT_INSTALL;
				}
			}
		}
	}
	CATCH(CFileException, ex)
	{
		__ERROR__(("FileException !!! (%s)", GetErrorMessage(ex->m_cause)));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	END_CATCH

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckWindowsUpdate()
{
	__FUNC_BEGIN__

	DWORD value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsUpdate\\Auto Update", "AUOptions", value) )
	{
		__WARN__(("Fail to get windows-update-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 1 )
	{
		__WARN__(("Windows-update-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckCleanWizard()
{
	__FUNC_BEGIN__

	DWORD value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Desktop\\CleanupWiz", "NoRun", value) )
	{
		__WARN__(("Fail to get clean-wizard-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 1 )
	{
		__WARN__(("Clean-wizard-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckScreenSaverForXP()
{
	__FUNC_BEGIN__

	//
	CString str_screen_save_active;
	if( !GetRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "ScreenSaveActive", str_screen_save_active) )
	{
		__WARN__(("Fail to get screen-saver-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	if( atoi(str_screen_save_active) != 0 )
	{
		__WARN__(("Screen-saver-registry(1) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	CString str_scrnsave = "";
	GetRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "SCRNSAVE.EXE", str_scrnsave);
	if( str_scrnsave != "" )
	{
		__WARN__(("Screen-saver-registry(2) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	// 체크할때 설정을 실행 (알아낼 방법이 아직 없는듯함. 너무 복잡함)
	static char* setting_cmd_list[] = {
		"powercfg.exe /LIST",
		"powercfg.exe /CREATE UBCPowerScheme",
		"powercfg.exe /SETACTIVE UBCPowerScheme",

		"powercfg.exe /CHANGE UBCPowerScheme /monitor-timeout-ac 0",
		"powercfg.exe /CHANGE UBCPowerScheme /monitor-timeout-dc 0",
		"powercfg.exe /CHANGE UBCPowerScheme /disk-timeout-ac 0",
		"powercfg.exe /CHANGE UBCPowerScheme /disk-timeout-dc 0",
		"powercfg.exe /CHANGE UBCPowerScheme /standby-timeout-ac 0",
		"powercfg.exe /CHANGE UBCPowerScheme /standby-timeout-dc 0",
		"powercfg.exe /CHANGE UBCPowerScheme /hibernate-timeout-ac 0",
		"powercfg.exe /CHANGE UBCPowerScheme /hibernate-timeout-dc 0",
		"", // end mark
	};
	// get list
	CString str_output;
	if( !CreateProcessRedirection(GetSystemPath(), setting_cmd_list[0], str_output) )
	{
		__WARN__(("Fail to execute (%s) !!!", setting_cmd_list[0]));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	if( str_output.Find("UBCPowerScheme") < 0 )
	{
		// not exist power-scheme  ==>  create
		if( !CreateProcessRedirection(GetSystemPath(), setting_cmd_list[1], str_output) )
		{
			__WARN__(("Fail to execute (%s) !!!", setting_cmd_list[1]));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}
	}

	// set value
	INSTALL_STATUS ret_val = INSTALL_STATUS_INSTALLED;
	for(int i=3; strlen(setting_cmd_list[i])>0; i++)
	{
		if( !CreateProcessRedirection(GetSystemPath(), setting_cmd_list[i], str_output) )
		{
			__WARN__(("Fail to execute (%s) !!!", setting_cmd_list[i]));
			ret_val = INSTALL_STATUS_NOT_INSTALL;
		}
	}

	// set active
	if( !CreateProcessRedirection(GetSystemPath(), setting_cmd_list[2], str_output) )
	{
		__WARN__(("Fail to execute (%s) !!!", setting_cmd_list[2]));
		ret_val = INSTALL_STATUS_NOT_INSTALL;
	}
	else
	{
		int pos = 0;
		CString str_line = str_output.Tokenize(" \t\r\n", pos);
		if( str_line != "" )
		{
			__ERROR__(("UBCPowerScheme isn't set !!!"));
			ret_val = INSTALL_STATUS_NOT_INSTALL;
		}
	}

	__FUNC_END__
	return ret_val;
}

INSTALL_STATUS CInstaller::CheckSecurityCenterAlarm()
{
	__FUNC_BEGIN__

	DWORD value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\wscsvc", "Start", value) )
	{
		__WARN__(("Fail to get security-center-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 3 )
	{
		__WARN__(("Security-Center-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckErrorReportingForXP()
{
	__FUNC_BEGIN__

	//
	DWORD value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting", "DoReport", value) )
	{
		__WARN__(("Fail to get error-report-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 0 )
	{
		__WARN__(("Error-report-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting", "ShowUI", value) )
	{
		__WARN__(("Fail to get error-show-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 0 )
	{
		__WARN__(("Error-show-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckTrayIconNotifications()
{
	__FUNC_BEGIN__

	//
	DWORD value = 0;
	GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced", "EnableBalloonTips", value);
	if( value != 0 )
	{
		__WARN__(("Enable-notificaion-registry(1) is exist !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	value = 0;
	GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "TaskbarNoNotification", value);
	if( value != 1 )
	{
		__WARN__(("Taskbar-nofication-registry(1) is exist !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	value = 0;
	GetRegValue(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced", "EnableBalloonTips", value);
	if( value != 0 )
	{
		__WARN__(("Enable-notificaion-registry(2) is exist !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	value = 0;
	GetRegValue(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "TaskbarNoNotification", value);
	if( value != 1 )
	{
		__WARN__(("Taskbar-nofication-registry(2) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckUACForVista()
{
	__FUNC_BEGIN__

	CString str_subkey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System";

	//
	if( !ExecuteSetACL(HKEY_LOCAL_MACHINE, str_subkey) )
	{
		__WARN__(("Fail to SetACL !!! (%s\\%s)", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey));
	}

	//
	DWORD value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, str_subkey, "EnableLUA", value) )
	{
		__WARN__(("Fail to get UAC-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 0 )
	{
		__WARN__(("UAC-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckScreenSaverForVista()
{
	__FUNC_BEGIN__

	//
	CString str_screen_save_active;
	if( !GetRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "ScreenSaveActive", str_screen_save_active) )
	{
		__WARN__(("Fail to get screen-saver-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	if( atoi(str_screen_save_active) != 0 )
	{
		__WARN__(("Screen-saver-registry(1) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	CString str_scrnsave = "";
	GetRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "SCRNSAVE.EXE", str_scrnsave);
	if( str_scrnsave != "" )
	{
		__WARN__(("Screen-saver-registry(2) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	// 체크할때 설정을 실행 (알아낼 방법이 아직 없는듯함. 너무 복잡함)
	static char* setting_cmd_list[] = {
		"powercfg.exe /CHANGE /monitor-timeout-ac 0",
		"powercfg.exe /CHANGE /monitor-timeout-dc 0",
		"powercfg.exe /CHANGE /disk-timeout-ac 0",
		"powercfg.exe /CHANGE /disk-timeout-dc 0",
		"powercfg.exe /CHANGE /standby-timeout-ac 0",
		"powercfg.exe /CHANGE /standby-timeout-dc 0",
		"powercfg.exe /CHANGE /hibernate-timeout-ac 0",
		"powercfg.exe /CHANGE /hibernate-timeout-dc 0",
		"", // end mark
	};

	INSTALL_STATUS ret_val = INSTALL_STATUS_INSTALLED;
	for(int i=0; strlen(setting_cmd_list[i])>0; i++)
	{
		CString str_output;
		if( !CreateProcessRedirection(GetSystemPath(), setting_cmd_list[i], str_output) )
		{
			__WARN__(("Fail to execute (%s) !!!", setting_cmd_list[i]));
			ret_val = INSTALL_STATUS_NOT_INSTALL;
		}
		//else
		//{
		//	int pos = 0;
		//	CString str_line = str_output.Tokenize(" \t\r\n", pos);
		//	if( !ret_val || str_line != "" )
		//	{
		//		__ERROR__(("(%s) isn't set !!!", setting_cmd_list[i]));
		//		ret_val = INSTALL_STATUS_NOT_INSTALL;
		//	}
		//}
	}

	__FUNC_END__
	return ret_val;
}

INSTALL_STATUS CInstaller::CheckSideBar()
{
	__FUNC_BEGIN__

	CString str_value;
	GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Run", "Sidebar", str_value);

	str_value.Trim();
	if( str_value.GetLength() != 0 )
	{
		__WARN__(("Sidebar-registry is exist !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckTrayIconNotificationsForVista()
{
	__FUNC_BEGIN__

	//
	CString str_subkey = "SOFTWARE\\Microsoft\\Security Center\\Svc";
	if( !ExecuteSetACL(HKEY_LOCAL_MACHINE, str_subkey) )
	{
		__WARN__(("Fail to SetACL !!! (%s\\%s)", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey));
	}

	//
	str_subkey += "\\";
	str_subkey += GetSIDofUserAccount();
	CreateRegKey(HKEY_LOCAL_MACHINE, str_subkey);

	if( !ExecuteSetACL(HKEY_LOCAL_MACHINE, str_subkey) )
	{
		__WARN__(("Fail to SetACL !!! (%s\\%s)", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey));
	}

	//
	DWORD value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, str_subkey, "EnableNotifications", value) )
	{
		__WARN__(("Fail to get security-center-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 0 )
	{
		__WARN__(("Security-center-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckTrayIconNotifications();
}

INSTALL_STATUS CInstaller::CheckAeroThemeForVista()
{
	__FUNC_BEGIN__

	//
	DWORD value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\DWM", "ColorizationOpaqueBlend", value) )
	{
		__WARN__(("Fail to get aero-theme-registry(1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 1 )
	{
		__WARN__(("Aero-theme-registry(1) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\DWM", "CompositionPolicy", value) )
	{
		__FUNC_END__
		return INSTALL_STATUS_INSTALLED; // fail  ==>  value=0  ==>  installed
	}

	if( value != 0 )
	{
		__WARN__(("Aero-theme-registry(2) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckCustomerExperience()
{
	__FUNC_BEGIN__

	CString str_output;
	if( !CreateProcessRedirection(GetSystemPath(), "schtasks.exe /query /v", str_output) )
	{
		__WARN__(("Fail to execute !!! (%s)", str_output));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	// output example...

	//호스트 이름      작업 이름                                다음 실행 시간         상태            로그온 모드             마지막 실행 시간        마지막 결과 만든 이          실행할 작업                                        시작 위치                                주석                                                                             예약된 작업 상태       유휴 시간                                전원 관리                                        다음 사용자 이름으로 실행                다시 예약되지 않으면 작업 삭제 다음 시간 동안 실행되면 작업 중지        일정                                                                             일정 유형                    시작 시간    시작 날짜  끝 날짜    일                                          월                                          반복: 매                 반복: 시간까지       반복: 기간까지                 반복: 아직 실행 중이면 중지        
	//================ ======================================== ====================== =============== ======================= ====================== ============ ================ ================================================== ======================================== ================================================================================ ====================== ======================================== ================================================ ======================================== ============================== ======================================== ================================================================================ ============================ ============ ========== ========== =========================================== =========================================== ======================== ==================== ============================== ===================================
	//UBCSVR-225       Consolidator                             2014-08-22 오후 12:00: 시작하지 못함   대화형/백그라운드       2014-08-21 오후 5:00:0  -2147479295 Microsoft Corpor %SystemRoot%\System32\wsqmcons.exe                 N/A                                      사용자가 Windows 사용자 환경 개선 프로그램에 참여하기로 동의한 경우 이 작업은 사 사용                   사용 안 함                                                                                SYSTEM                                   사용                           72:00:00                                 이 형식으로 데이터를 예약할 수 없습니다.                                         한 번만, 매시간              오전 12:00:0 2004-01-02 N/A        N/A                                         N/A                                         19시간, 0분              없음                 사용 안 함                     사용 안 함                         

	int pos = 0;
	CString str_line;
	while( (str_line=str_output.Tokenize("\r\n", pos)) != "" )
	{
		int find = str_line.Find("Consolidator");
		if( find < 0 ) continue;

		find += strlen("Consolidator");
		CString str_status = str_line.Tokenize(" ", find);
		if( atoi(str_status) == 0 )
		{	// something words  ==>  disable
			__FUNC_END__
			return INSTALL_STATUS_INSTALLED;
		}
		else
		{	// date-string(2014-08-22)  ==>  enable
			__WARN__(("Consolidator is auto-start !!! (%s)", str_line));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}
	}

	__FUNC_END__
	return INSTALL_STATUS_NOT_INSTALL;
}

INSTALL_STATUS CInstaller::CheckWelcomeCenter()
{
	__FUNC_BEGIN__

	CString str_value;
	GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Run", "WindowsWelcomeCenter", str_value);

	str_value.Trim();
	if( str_value.GetLength() != 0 )
	{
		__WARN__(("Welcome-center-registry isn't set!!! (%s)", str_value));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckErrorReportingForVista()
{
	__FUNC_BEGIN__

	//
	DWORD value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\Windows Error Reporting", "Disabled", value) )
	{
		__WARN__(("Fail to get error-disabled-registry(1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 1 )
	{
		__WARN__(("Error-disabled-registry(1) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\Windows Error Reporting", "DontShowUI", value) )
	{
		__WARN__(("Fail to get error-show-registry(1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 1 )
	{
		__WARN__(("Error-show-registry(1) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\Windows Error Reporting", "Disabled", value) )
	{
		__WARN__(("Fail to get error-disabled-registry(2) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 1 )
	{
		__WARN__(("Error-disabled-registry(2) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\Windows Error Reporting", "DontShowUI", value) )
	{
		__WARN__(("Fail to get error-show-registry(2) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 1 )
	{
		__WARN__(("Error-show-registry(2) isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckUACFor7()
{
	return CheckUACForVista();
}

INSTALL_STATUS CInstaller::CheckScreenSaverFor7()
{
	return CheckScreenSaverForVista();
}

INSTALL_STATUS CInstaller::CheckTrayIconNotificationsFor7()
{
	__FUNC_BEGIN__

	DWORD value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "HideSCAHealth", value) )
	{
		__WARN__(("Fail to get security-alarm-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value == 0 )
	{
		__WARN__(("Security-alarm-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckTrayIconNotifications();
}

INSTALL_STATUS CInstaller::CheckAeroThemeFor7()
{
	__FUNC_BEGIN__

	CString str_value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Themes", "CurrentTheme", str_value) )
	{
		__WARN__(("Fail to get aero-theme-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	str_value.MakeLower();
	if( str_value.Find("basic.theme") < 0 )
	{
		__WARN__(("Aero-theme-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckErrorReportingFor7()
{
	return CheckErrorReportingForVista();
}

static char* szLAVVideoDecoders[] = {
	// LAV Video Decoder = {EE30215D-164F-4A92-A4EB-9D4C13390F9F}
	"{47504A4D-0000-0010-8000-00AA00389B71}", // MJPG
	"{e06d8026-db46-11cf-b4d1-00805f6cbbea}", // MPEG2 VIDEO
	"{e436eb80-524f-11ce-9f53-0020af0ba770}", // MPEGPacket
	"{e436eb81-524f-11ce-9f53-0020af0ba770}", // MPEG1Payload
	"{31435641-0000-0010-8000-00AA00389B71}", // avc1
	"{58564944-0000-0010-8000-00AA00389B71}", // DIVX
	"{78766964-0000-0010-8000-00AA00389B71}", // divx
	"{64737664-0000-0010-8000-00AA00389B71}", // dvsd
	"{34363248-0000-0010-8000-00AA00389B71}", // H264
	"{34363268-0000-0010-8000-00AA00389B71}", // h264
	"{31435648-0000-0010-8000-00AA00389B71}", // HVC1 (h265)
	"{5334504D-0000-0010-8000-00AA00389B71}", // MP4S
	"{7334706D-0000-0010-8000-00AA00389B71}", // mp4s
	"{5634504D-0000-0010-8000-00AA00389B71}", // MP4V
	"{7634706D-0000-0010-8000-00AA00389B71}", // mp4v
	"{31435657-0000-0010-8000-00AA00389B71}", // WVC1
	"{44495658-0000-0010-8000-00AA00389B71}", // XVID
	"{64697678-0000-0010-8000-00AA00389B71}", // xvid
	"", // end mark
};

static char* szLAVAudioDecoders[] = {
	// LAV Audio Decoder = {E8E73B6B-4CB3-44A4-BE99-4F7BCB96E491}
	"{000000FF-0000-0010-8000-00aa00389b71}", // AAC1 LAV Audio Decoder
	"{00001602-0000-0010-8000-00aa00389b71}", // LATM AAC (MPEG_ADTS_AAC)
	"{00000050-0000-0010-8000-00AA00389B71}", // MP1
	"{e06d802b-db46-11cf-b4d1-00805f6cbbea}", // MP2
	"{00000055-0000-0010-8000-00AA00389B71}", // MP3
	"", // end mark
};

static char* szWMVideoDecoder[] = {
	// WMVideo Decoder DMO = {82d353df-90bd-4382-8bc2-3f6192b76e34}
	"{31564D57-0000-0010-8000-00AA00389B71}", // WMV1
	"{32564D57-0000-0010-8000-00AA00389B71}", // WMV2
	"{33564D57-0000-0010-8000-00AA00389B71}", // WMV3
	"{41564D57-0000-0010-8000-00AA00389B71}", // WMVA
	"{50564D57-0000-0010-8000-00AA00389B71}", // WMVP
	"{52564D57-0000-0010-8000-00AA00389B71}", // WMVR
	"{32505657-0000-0010-8000-00AA00389B71}", // WVP2
	"", // end mark
};

static char* szWMAudioDecoder[] = {
	// WMAudio Decoder DMO = {2eeb4adf-4578-4d10-bca7-bb955f56320a}
	"{00000160-0000-0010-8000-00aa00389b71}", // WMA9_00
	"{00000161-0000-0010-8000-00aa00389b71}", // WMA9_01
	"{00000162-0000-0010-8000-00aa00389b71}", // WMA9_02
	"{00000163-0000-0010-8000-00aa00389b71}", // WMA9_03
	"", // end mark
};

// DV Video Decoder = {B1B77C00-C3E4-11cf-AF79-00AA00B67A42}
// {20637664-0000-0010-8000-00AA00389B71} = DVC			
// {64687664-0000-0010-8000-00AA00389B71} = DVHD
// {6c737664-0000-0010-8000-00AA00389B71} = DVSL

// Microsoft DTV-DVD Audio Decoder = {E1F1A0B8-BEEE-490D-BA7C-066C40B5E2B9}
// {00001600-0000-0010-8000-00aa00389b71} = ADTS
// {e06d8032-db46-11cf-b4d1-00805f6cbbea} = MPEG-2 LPCM Audio

// WMV Screen decoder DMO = {7BAFB3B1-D8F4-4279-9253-27DA423108DE}
// {3153534D-0000-0010-8000-00AA00389B71} = MSS1
// {3253534D-0000-0010-8000-00AA00389B71} = MSS2

// WMSpeech Decoder DMO = {874131CB-4ECC-443B-8948-746B89595D20}
// {0000000A-0000-0010-8000-00AA00389B71} = WMSP1
// {0000000B-0000-0010-8000-00AA00389B71} = WMSP2

// Mpeg4 Decoder DMO = {F371728A-6052-4D47-827C-D039335DFE0A}
// {3234504D-0000-0010-8000-00AA00389B71} = MP42
// {3234706D-0000-0010-8000-00AA00389B71} = mp42
// {3447504D-0000-0010-8000-00AA00389B71} = MPG4
// {3467706D-0000-0010-8000-00AA00389B71} = mpg4

// Mpeg4s Decoder DMO = {2A11BAE2-FE6E-4249-864B-9E9ED6E8DBC2}
// {3253344D-0000-0010-8000-00AA00389B71} = M4S2
// {3273346D-0000-0010-8000-00AA00389B71} = m4s2

// Mpeg43 Decoder DMO = {CBA9E78B-49A3-49EA-93D4-6BCBA8C4DE07}
// {3334504D-0000-0010-8000-00AA00389B71} = MP43
// {3334706D-0000-0010-8000-00AA00389B71} = mp43

// ffdshow Audio Decoder = {0F40E1E5-4F79-4988-B1A9-CC98794E6B55}

// ffdshow Video Decoder = {04FE9017-F873-410E-871E-AB91661A4EF7}

// MP3 Decoder DMO = {BBEEA841-0A63-4F52-A7AB-A9B3A84ED38A}

// Microsoft DTV-DVD Video Decoder = {212690FB-83E5-4526-8FD7-74478B7939CD}

// Microsoft DTV-DVD Audio Decoder = {E1F1A0B8-BEEE-490D-BA7C-066C40B5E2B9}

// MJPEG Decompressor = {301056D0-6DFF-11d2-9EEB-006008039E37}

// MPEG Video Decoder = {feb50740-7bef-11ce-9bd9-0000e202599c}

// MPEG Audio Codec = {4a2286e0-7bef-11ce-9bd9-0000e202599c}

// Enhanced Video Renderer = {FA10746C-9B63-4B6C-BC49-FC300EA5F256}


typedef struct
{
	CString strDecoderName;
	CString strDecoderCLSID;
	char** pszCodecList;
} CODEC_ITEM;
typedef CArray<CODEC_ITEM, CODEC_ITEM&>		CODEC_LIST;

INSTALL_STATUS CInstaller::CheckCodecConfig()
{
	__FUNC_BEGIN__

	CString str_subkey = "SOFTWARE\\Microsoft\\DirectShow\\Preferred";
	if( IsWow64() ) str_subkey = "SOFTWARE\\Wow6432Node\\Microsoft\\DirectShow\\Preferred";

	if( !ExecuteSetACL(HKEY_LOCAL_MACHINE, str_subkey) )
	{
		__WARN__(("Fail to SetACL !!! (%s\\%s)", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey));
	}

	CODEC_LIST codec_list;

	CODEC_ITEM add_item;
	add_item.strDecoderName = "LAV Video Decoder";
	add_item.strDecoderCLSID = "{EE30215D-164F-4A92-A4EB-9D4C13390F9F}";
	add_item.pszCodecList = szLAVVideoDecoders;
	codec_list.Add(add_item);

	add_item.strDecoderName = "LAV Audio Decoder";
	add_item.strDecoderCLSID = "{E8E73B6B-4CB3-44A4-BE99-4F7BCB96E491}";
	add_item.pszCodecList = szLAVAudioDecoders;
	codec_list.Add(add_item);

	add_item.strDecoderName = "WMVideo Decoder DMO";
	add_item.strDecoderCLSID = "{82d353df-90bd-4382-8bc2-3f6192b76e34}";
	add_item.pszCodecList = szWMVideoDecoder;
	codec_list.Add(add_item);

	add_item.strDecoderName = "WMAudio Decoder DMO";
	add_item.strDecoderCLSID = "{2eeb4adf-4578-4d10-bca7-bb955f56320a}";
	add_item.pszCodecList = szWMAudioDecoder;
	codec_list.Add(add_item);

	INSTALL_STATUS ret_val = INSTALL_STATUS_INSTALLED;
	for(int i=0; i<codec_list.GetCount(); i++)
	{
		CODEC_ITEM& item = codec_list.GetAt(i);

		__DEBUG__(("Check Codec (%s)", item.strDecoderName));

		for(int i=0; strlen(item.pszCodecList[i])>0; i++)
		{
			CString str_value = 0;
			if( !GetRegValue(HKEY_LOCAL_MACHINE, str_subkey, item.pszCodecList[i], str_value) )
			{
				__WARN__(("Fail to get codec-registry (%s\\%s\\%s) !!!", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey, item.pszCodecList[i]));
				__FUNC_END__
				ret_val = INSTALL_STATUS_NOT_INSTALL;
			}

			if( stricmp(str_value, item.strDecoderCLSID) != 0)
			{
				__WARN__(("Codec-registry (%s\\%s\\%s) isn't set !!!", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey, item.pszCodecList[i]));
				__FUNC_END__
				ret_val = INSTALL_STATUS_NOT_INSTALL;
			}
		}
	}

	__FUNC_END__
	return ret_val;
}

INSTALL_STATUS CInstaller::CheckUACFor8()
{
	return CheckUACForVista();
}

INSTALL_STATUS CInstaller::CheckScreenSaverFor8()
{
	return CheckScreenSaverForVista();
}

INSTALL_STATUS CInstaller::CheckTrayIconNotificationsFor8()
{
	return CheckTrayIconNotificationsFor7();
}

INSTALL_STATUS CInstaller::CheckErrorReportingFor8()
{
	return CheckErrorReportingForVista();
}

INSTALL_STATUS CInstaller::CheckCodecConfigFor8()
{
	return CheckCodecConfig();
}

INSTALL_STATUS CInstaller::CheckHotCornersFor8()
{
	__FUNC_BEGIN__

	DWORD value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\ImmersiveShell\\EdgeUI", "DisableTLCorner", value) )
	{
		__WARN__(("Fail to get disable-tlcorner-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value == 0 )
	{
		__WARN__(("Disable-tlcorner-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\ImmersiveShell\\EdgeUI", "DisableTRCorner", value) )
	{
		__WARN__(("Fail to get disable-trcorner-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value == 0 )
	{
		__WARN__(("Disable-trcorner-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckBootDesktopFor8()
{
	__FUNC_BEGIN__

	DWORD value = 0;
	if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StartPage", "OpenAtLogon", value) )
	{
		__WARN__(("Fail to get open-at-logon-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value != 0 )
	{
		__WARN__(("Open-at-logon-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::CheckLockScreenFor8()
{
	__FUNC_BEGIN__

	DWORD value = 0;
	if( !GetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Policies\\Microsoft\\Windows\\Personalization", "NoLockScreen", value) )
	{
		__WARN__(("Fail to get no-lock-screen-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( value == 0 )
	{
		__WARN__(("No-lock-screen-registry isn't set !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

////////////////////////////////////////////////////////////////
extern int vncEncryptPasswd(char*, char*);

INSTALL_STATUS CInstaller::InstallVNC()
{
	__FUNC_BEGIN__

	//
	CString str_vnc_path = Get3rdpartyPath();
	str_vnc_path += "VNC\\Install\\";

	CString str_cmd = "UltraVNC_X86_Setup.exe /verysilent /loadinf=";
	str_cmd += ( IsWow64() ? "UltraVNC_X64_Setup.inf" : "UltraVNC_X86_Setup.inf" );

	if( !CreateProcess(str_vnc_path, str_cmd, 3*60*1000) ) // wait 3-min
	{
		__WARN__(("Fail to install vnc-file (%s) !!!", str_vnc_path));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	// close auto-popup-window
	CString str_output;
	CreateProcessRedirection(GetSystemPath(), "taskkill.exe /im iexplore.exe", str_output);

	// stop vnc-service
	str_cmd = "net.exe stop uvnc_service";
	if( !CreateProcessRedirection(GetSystemPath(), str_cmd, str_output) )
	{
		__WARN__(("Fail to stop vnc-service (%s) !!!", str_cmd));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	// close winvnc.exe
	CreateProcessRedirection(GetSystemPath(), "taskkill.exe /im winvnc.exe", str_output);

	// set ini
	CString str_ini_path = GetVNCPath();
	if( str_ini_path.GetLength() == 0 )
	{
		__WARN__(("Fail to find vnc-folder !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	str_ini_path += "ultravnc.ini";

	char buf_pwd_enc[256] = {0};
	vncEncryptPasswd("ubc", buf_pwd_enc); // pwd = "ubc"
	if( !WritePrivateProfileStruct("ultravnc", "passwd", buf_pwd_enc, 8, str_ini_path) )
	{
		__WARN__(("Fail to write vnc-password !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	char buf_pwd2_enc[256] = {0};
	vncEncryptPasswd("ubcubcubc", buf_pwd2_enc); // view only pwd = "ubcubcubc"
	if( !WritePrivateProfileStruct("ultravnc", "passwd2", buf_pwd2_enc, 8, str_ini_path) )
	{
		__WARN__(("Fail to write vnc-password (view only) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	if( !WritePrivateProfileString("admin","AllowLoopback","1", str_ini_path) )
	{
		__WARN__(("Fail to set AllowLoopback !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	if( !WritePrivateProfileString("admin","AutoPortSelect","0", str_ini_path) )
	{
		__WARN__(("Fail to set AutoPortSelect !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	if( !WritePrivateProfileString("admin","PortNumber","5900", str_ini_path) )
	{
		__WARN__(("Fail to set PortNumber !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	// start vnc-service
	str_cmd = "net.exe start uvnc_service";
	if( !CreateProcessRedirection(GetSystemPath(), str_cmd, str_output) )
	{
		__WARN__(("Fail to start vnc-service (%s) !!!", str_cmd));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckVNC();
}

INSTALL_STATUS CInstaller::InstallKLiteCodec()
{
	__FUNC_BEGIN__

	// ini 추출
	// K-Lite_Codec_Pack_Full.exe /unattended  ==>  klcp_full_unattended.ini , klcp_full_unattended.bat 파일 생성
	// klcp_full_unattended.ini 를 K-Lite_Codec_Pack_Full.ini 로 변경

	// K-Lite_Codec_Pack_Full.ini 에 audio\raw\ffdshow 추가

	// ini 대로 설치
	CString str_codec_path = Get3rdpartyPath();
	str_codec_path += "Codec\\Install\\";

	if( !CreateProcess(str_codec_path, "K-Lite_Codec_Pack_Full.exe /verysilent /loadinf=K-Lite_Codec_Pack_Full.ini", 3*60*1000) ) // wait 3-min
	{
		__WARN__(("Fail to install codec-file (%s) !!!", str_codec_path));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	// K-Lite_Codec_Pack_Full.ini 에서 다음아이템 제거
	// systray_haali, systray_lavsplitter, systray_lav, systray_ffdshow, systray_madvr
	// 위 아이템들 제거하면 아래 코드는 불필요
/*
	// Show LAV Splitter Icon
	// HKCU/Software/MPC-HC/MPC-HC/Internal Filters/LAVSplitter/TrayIcon=0
	// HKLM/Software/LAV64/Splitter/TrayIcon=0
	// HKLM/Software/Wow6432Node/LAV/Splitter/TrayIcon=0
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\MPC-HC\\MPC-HC\\Internal Filters\\LAVSplitter", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_LOCAL_MACHINE, "Software\\LAV64\\Splitter", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_LOCAL_MACHINE, "Software\\LAV\\Splitter", "TrayIcon", 0, KEY_WOW64_32KEY);

	// Show LAV Decoder Icons
	// HKCU/Software/MPC-HC/MPC-HC/Internal Filters/LAVVideo/TrayIcon=0
	// HKLM/Software/LAV64/Audio/TrayIcon=0
	// HKLM/Software/LAV64/Video/TrayIcon=0
	// HKLM/Software/Wow6432Node/LAV/Audio/TrayIcon=0
	// HKLM/Software/Wow6432Node/LAV/Video/TrayIcon=0
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\MPC-HC\\MPC-HC\\Internal Filters\\LAVVideo", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_LOCAL_MACHINE, "Software\\LAV64\\Audio", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_LOCAL_MACHINE, "Software\\LAV64\\Video", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_LOCAL_MACHINE, "Software\\LAV\\Audio", "TrayIcon", 0, KEY_WOW64_32KEY);
	SetRegValueIfExist(HKEY_LOCAL_MACHINE, "Software\\LAV\\Video", "TrayIcon", 0, KEY_WOW64_32KEY);

	// Show ffdshow Icons
	// HKCU/Software/GNU/ffdshow/trayIcon=0
	// HKCU/Software/GNU/ffdshow_audio/trayIcon=0
	// HKCU/Software/GNU/ffdshow_audio_raw/trayIcon=0
	// HKCU/Software/GNU/ffdshow_raw/trayIcon=0
	// HKCU/Software/GNU/ffdshow64/trayIcon=0
	// HKCU/Software/GNU/ffdshow64_audio/trayIcon=0
	// HKCU/Software/GNU/ffdshow64_audio_raw/trayIcon=0
	// HKCU/Software/GNU/ffdshow64_raw/trayIcon=0
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\GNU\\ffdshow", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\GNU\\ffdshow_raw", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\GNU\\ffdshow_audio", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\GNU\\ffdshow_audio_raw", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\GNU\\ffdshow64", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\GNU\\ffdshow64_raw", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\GNU\\ffdshow64_audio", "TrayIcon", 0);
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\GNU\\ffdshow64_audio_raw", "TrayIcon", 0);

	// Show Haali Media Splitter Icon
	// HKCU/Software/Haali/Matroska Splitter/ui.trayicon=0
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\Haali\\Matroska Splitter", "ui.trayicon", 0);

	// Show madVR Icon
	// HKCU/Software/madshi/madHcCtrl/ShowTrayIcon=0
	SetRegValueIfExist(HKEY_CURRENT_USER, "Software\\madshi\\madHcCtrl", "ShowTrayIcon", 0);
*/
	__FUNC_END__
	return CheckKLiteCodec();
}

INSTALL_STATUS CInstaller::InstallFlashplayer()
{
	__FUNC_BEGIN__

	::MessageBox(IDS_MSG_INSTALL_FLASH_PLAYER, MB_ICONINFORMATION);

	::ShellExecute(NULL, "open", "iexplore.exe", "http://www.adobe.com/software/flash/about/", "", SW_SHOW);

	::MessageBox(IDS_MSG_COMPLETE_FLASH_PLAYER, MB_ICONQUESTION);

	__FUNC_END__
	return CheckFlashplayer();
}

INSTALL_STATUS CInstaller::InstallShortCut()
{
	__FUNC_BEGIN__

	if( !CreateProcess(GetAppPath(), "MenuRegister.exe /install", 10*1000) ) // wait 10-sec
	{
		__WARN__(("Fail to install short-cut !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckShortCut();
}

INSTALL_STATUS CInstaller::InstallFirewallException()
{
	__FUNC_BEGIN__

	CString str_fw_ex_path = GetAppPath();
	if( IsWow64() )
		str_fw_ex_path += "firewallException_x64.txt";
	else
		str_fw_ex_path += "firewallException.txt";

	TRY
	{
		CFile file_fw_ex;
		if( !file_fw_ex.Open(str_fw_ex_path, CFile::modeRead | CFile::typeBinary) )
		{
			__WARN__(("Fail to open firewall-file (%s) !!!", str_fw_ex_path));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		int file_size = file_fw_ex.GetLength();
		char* buf = new char[file_size+1];
		::ZeroMemory(buf, file_size+1);

		UINT read_size = file_fw_ex.Read(buf, file_size);
		if( read_size != file_size )
		{
			__WARN__(("Fail to read firewall-file !!! (rsize=%u, fsize=%u)", read_size, file_size));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		CString str_buf = buf;
		delete[]buf;

		int pos = 0;
		CString str_line;

		while( (str_line=str_buf.Tokenize("\r\n", pos)) != "" )
		{
			int pos_sub = 0;
			CString str_left = str_line.Tokenize(",", pos_sub);

			if( !scratchUtil::getInstance()->AddToExceptionList((LPCSTR)str_left) )
			{
				__WARN__(("Fail to add to firewall (%s) !!!", str_left));
				__FUNC_END__
				return INSTALL_STATUS_NOT_INSTALL;
			}
		}
	}
	CATCH(CFileException, ex)
	{
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	END_CATCH

	__FUNC_END__
	return CheckFirewallException();
}

INSTALL_STATUS CInstaller::InstallSecurity()
{
	__FUNC_BEGIN__

	CString str_app_path = GetAppPath();
	if( str_app_path.GetLength() < 3 ) 
	{
		__WARN__(("Invalid path !!! (%s)", str_app_path));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	CString str_contents_path = str_app_path.Left(3);
	str_contents_path += "SQISoft\\Contents";

	CString str_cmd;
	str_cmd.Format("attrib.exe +h \"%s\"", str_contents_path);

	if( !CreateProcess(GetSystemPath(), str_cmd, 10*1000) ) // wait 10-sec
	{
		__WARN__(("Fail to install security (%s) !!!", str_cmd));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckSecurity();
}

INSTALL_STATUS CInstaller::InstallSoftDrive()
{
	__FUNC_BEGIN__

	CString str_ftproot = "C:\\ftproot";//_UBC_CD("C:\\ftproot");

	//
	CString str_sub_path = str_ftproot;
	str_sub_path += "\\SQISoft\\Contents\\ENC\\";
	if( !::MakeSureDirectoryPathExists(str_sub_path) )
	{
		if( ::GetLastError() != ERROR_ALREADY_EXISTS )
		{
			__WARN__(("Fail to create directory (%s) !!!", str_sub_path));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}
	}

	//
	str_sub_path = str_ftproot;
	str_sub_path += "\\SQISoft\\Contents\\Temp\\";
	if( !::MakeSureDirectoryPathExists(str_sub_path) )
	{
		if( ::GetLastError() != ERROR_ALREADY_EXISTS )
		{
			__WARN__(("Fail to create directory (%s) !!!", str_sub_path));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}
	}

	//
	str_sub_path = str_ftproot;
	str_sub_path += "\\SQISoft\\UTV1.0\\execute\\config\\";
	if( !::MakeSureDirectoryPathExists(str_sub_path) )
	{
		if( ::GetLastError() != ERROR_ALREADY_EXISTS )
		{
			__WARN__(("Fail to create directory (%s) !!!", str_sub_path));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}
	}

	//
	CString str_cmd = "subst.exe X: ";
	str_cmd += str_ftproot;

	if( !CreateProcess(GetSystemPath(), str_cmd, 10*1000) ) // wait 10-sec
	{
		__WARN__(("Fail to execute (%s) !!!", str_cmd));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckSoftDrive();
}

INSTALL_STATUS CInstaller::InstallTaskbar()
{
	__FUNC_BEGIN__

	APPBARDATA abd = {0};
	abd.cbSize = sizeof(APPBARDATA);
	abd.hWnd = (HWND)::FindWindow("Shell_TrayWnd",NULL);
	abd.lParam = ABS_AUTOHIDE; // ABS_ALWAYSONTOP은 해제
	SHAppBarMessage(ABM_SETSTATE, &abd);

	__FUNC_END__
	return CheckTaskbar();
}

INSTALL_STATUS CInstaller::InstallAutoLogin()
{
	__FUNC_BEGIN__

	CAutoLoginDlg dlg;
	if( dlg.DoModal() == IDOK )
	{
		if( !SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "AutoAdminLogon", "1") )
		{
			__WARN__(("Fail to set auto-login !!!"));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		if( !SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultUserName", dlg.GetLoginID()) )
		{
			__WARN__(("Fail to set auto-login-ID (%s) !!!", dlg.GetLoginID()));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		if( !SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultPassword", dlg.GetPassword()) )
		{
			__WARN__(("Fail to set auto-login-password (%s) !!!", dlg.GetPassword()));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}
	}

	__FUNC_END__
	return CheckAutoLogin();
}

INSTALL_STATUS CInstaller::InstallFlashUpdate()
{
	__FUNC_BEGIN__

	CString str_flash_path = GetSystemPath();
	str_flash_path += "Macromed\\Flash\\mms.cfg";

	//
	if( !PathFileExists(str_flash_path) )
	{
		// 2015. 6. 9. mms.cfg가 존재하지 않을 경우 생성. ssb.

		__WARN__(("Fail to find flash-update-file (%s) !!!", str_flash_path));
		//__FUNC_END__
		//return INSTALL_STATUS_NOT_INSTALL;

		TRY
		{
			CFile file_flash_cfg;
			if( !file_flash_cfg.Open(str_flash_path, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
			{
				__WARN__(("Fail to open flash-update-file (%s) !!!", str_flash_path));
				__FUNC_END__
				return INSTALL_STATUS_NOT_INSTALL;
			}


			CString str_result;
			str_result += "AutoUpdateDisable=1\r\n";
			str_result += "SlientAutoUpdateEnable=0\r\n";

			file_flash_cfg.Write((LPVOID)(LPCSTR)str_result, str_result.GetLength());
			file_flash_cfg.Close();
		}
		CATCH(CFileException, ex)
		{
			__ERROR__(("FileException !!! (%s)", GetErrorMessage(ex->m_cause)));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}
		END_CATCH
	}

	TRY
	{
		CFile file_flash_cfg;
		if( !file_flash_cfg.Open(str_flash_path, CFile::modeRead | CFile::typeBinary) )
		{
			__WARN__(("Fail to open flash-update-file (%s) !!!", str_flash_path));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		int file_size = file_flash_cfg.GetLength();
		char* buf = new char[file_size+1];
		::ZeroMemory(buf, file_size+1);

		UINT read_size = file_flash_cfg.Read(buf, file_size);
		file_flash_cfg.Close();

		if( read_size != file_size )
		{
			__WARN__(("Fail to read flash-update-file !!! (rsize=%u, fsize=%u)", read_size, file_size));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		CString str_buf = buf;
		delete[]buf;

		int pos = 0;
		CString str_line;
		CString str_result;

		while( (str_line=str_buf.Tokenize("\r\n", pos)) != "" )
		{
			if( str_result.GetLength() > 0 )
				str_result += "\r\n";

			if( strnicmp(str_line, "AutoUpdateDisable", 17) == 0 )
			{
				str_result += "AutoUpdateDisable=1";
			}
			else
			{
				str_result += str_line;
			}
		}

		//
		if( !file_flash_cfg.Open(str_flash_path, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
		{
			__WARN__(("Fail to open flash-update-file (%s) !!!", str_flash_path));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}

		file_flash_cfg.Write((LPVOID)(LPCSTR)str_result, str_result.GetLength());
		file_flash_cfg.Close();
	}
	CATCH(CFileException, ex)
	{
		__ERROR__(("FileException !!! (%s)", GetErrorMessage(ex->m_cause)));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}
	END_CATCH

	__FUNC_END__
	return CheckFlashUpdate();
}

INSTALL_STATUS CInstaller::InstallWindowsUpdate()
{
	__FUNC_BEGIN__

	if( !SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsUpdate\\Auto Update", "AUOptions", 1) )
	{
		__WARN__(("Fail to set screen-saver-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckWindowsUpdate();
}

INSTALL_STATUS CInstaller::InstallCleanWizard()
{
	__FUNC_BEGIN__

	if( !SetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Desktop\\CleanupWiz", "NoRun", 1) )
	{
		__WARN__(("Fail to set clean-wizard-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckCleanWizard();
}

INSTALL_STATUS CInstaller::InstallScreenSaverForXP()
{
	__FUNC_BEGIN__

	if( !SetRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "ScreenSaveActive", "0") )
	{
		__WARN__(("Fail to set screen-saver-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	CString str_val;
	if( GetRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "SCRNSAVE.EXE", str_val) )
	{
		// if exist, must delete
		if( !DeleteRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "SCRNSAVE.EXE") )
		{
			__WARN__(("Fail to delete screen-saver-registry !!!"));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}
	}

	__FUNC_END__
	return CheckScreenSaverForXP();
}

INSTALL_STATUS CInstaller::InstallSecurityCenterAlarm()
{
	__FUNC_BEGIN__

	CString str_output;
	BOOL ret_val = CreateProcessRedirection(GetSystemPath(), "net.exe stop wscsvc", str_output);

	if( !ret_val || // fail to run
		(str_output.Find("NET HELPMSG") > 0 && str_output.Find("3521") < 0) ) // not started
	{
		__WARN__(("Fail to stop security-center !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( !SetRegValue(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\wscsvc", "Start", 3) )
	{
		__WARN__(("Fail to set security-center-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
		return CheckSecurityCenterAlarm();
}

INSTALL_STATUS CInstaller::InstallErrorReportingForXP()
{
	__FUNC_BEGIN__

	if( !SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting", "DoReport", 0) )
	{
		__WARN__(("Fail to set error-report-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( !SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting", "ShowUI", 0) )
	{
		__WARN__(("Fail to set error-show-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckErrorReportingForXP();
}

INSTALL_STATUS CInstaller::InstallTrayIconNotifications()
{
	__FUNC_BEGIN__

	//
	DeleteRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced", "EnableBalloonTips");

	//
	DeleteRegValue(HKEY_CURRENT_USER,  "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced", "EnableBalloonTips");

	//
	if( !SetRegValue(HKEY_CURRENT_USER,  "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "TaskbarNoNotification", 1) )
	{
		__WARN__(("Fail to set taskbar-nofication-registry(1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	if( !SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "TaskbarNoNotification", 1) )
	{
		__WARN__(("Fail to set taskbar-nofication-registry(2) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::InstallUACForVista()
{
	__FUNC_BEGIN__

	CString str_subkey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System";

	//
	if( !ExecuteSetACL(HKEY_LOCAL_MACHINE, str_subkey) )
	{
		__WARN__(("Fail to SetACL !!! (%s\\%s)", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey));
	}

	//
	if( !SetRegValue(HKEY_LOCAL_MACHINE, str_subkey, "EnableLUA", 0) )
	{
		__WARN__(("Fail to set UAC-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return INSTALL_STATUS_INSTALLED;
}

INSTALL_STATUS CInstaller::InstallScreenSaverForVista()
{
	__FUNC_BEGIN__

	if( !SetRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "ScreenSaveActive", "0") )
	{
		__WARN__(("Fail to set screen-saver-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	CString str_val;
	if( GetRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "SCRNSAVE.EXE", str_val) )
	{
		// if exist, must delete
		if( !DeleteRegValue(HKEY_CURRENT_USER, "Control Panel\\Desktop", "SCRNSAVE.EXE") )
		{
			__WARN__(("Fail to delete screen-saver-registry !!!"));
			__FUNC_END__
			return INSTALL_STATUS_NOT_INSTALL;
		}
	}

	__FUNC_END__
	return CheckScreenSaverForVista();
}

INSTALL_STATUS CInstaller::InstallSideBar()
{
	__FUNC_BEGIN__

	DeleteRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Run", "Sidebar");

	__FUNC_END__
	return CheckSideBar();
}

INSTALL_STATUS CInstaller::InstallTrayIconNotificationsForVista()
{
	__FUNC_BEGIN__

	CString str_subkey = "SOFTWARE\\Microsoft\\Security Center\\Svc\\";
	str_subkey += GetSIDofUserAccount();

	CreateRegKey(HKEY_LOCAL_MACHINE, str_subkey);

	//
	if( !ExecuteSetACL(HKEY_LOCAL_MACHINE, str_subkey) )
	{
		__WARN__(("Fail to SetACL !!! (%s\\%s)", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey));
	}

	//
	if( !SetRegValue(HKEY_LOCAL_MACHINE, str_subkey, "EnableNotifications", 0) )
	{
		__WARN__(("Fail to set security-center-registry !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__

	InstallTrayIconNotifications();

	return CheckTrayIconNotificationsForVista();
}

INSTALL_STATUS CInstaller::InstallAeroThemeForVista()
{
	__FUNC_BEGIN__

	if( !SetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\DWM", "ColorizationOpaqueBlend", 1) )
	{
		__WARN__(("Fail to set aero-theme-registry(1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( !SetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\DWM", "CompositionPolicy", 0) )
	{
		__WARN__(("Fail to set aero-theme-registry(2) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckAeroThemeForVista();
}

INSTALL_STATUS CInstaller::InstallCustomerExperience()
{
	__FUNC_BEGIN__

	CString str_cmd = "schtasks.exe /Change /TN \"\\Microsoft\\Windows\\Customer Experience Improvement Program\\Consolidator\" /DISABLE";

	CString str_output;
	if( !CreateProcessRedirection(GetSystemPath(), str_cmd, str_output) )
	{
		__WARN__(("Fail to execute (%s) !!!", str_cmd));
	}

	__FUNC_END__
	return CheckCustomerExperience();
}

INSTALL_STATUS CInstaller::InstallWelcomeCenter()
{
	__FUNC_BEGIN__

	DeleteRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Run", "WindowsWelcomeCenter");

	__FUNC_END__
	return CheckWelcomeCenter();
}

INSTALL_STATUS CInstaller::InstallErrorReportingForVista()
{
	__FUNC_BEGIN__

	//
	if( !SetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\Windows Error Reporting", "Disabled", 1) )
	{
		__WARN__(("Fail to set error-disabled-registry(1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	if( !SetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\Windows Error Reporting", "DontShowUI", 1) )
	{
		__WARN__(("Fail to set error-show-registry(1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	if( !SetRegValue(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\Windows Error Reporting", "Disabled", 1) )
	{
		__WARN__(("Fail to set error-disabled-registry(2) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	if( !SetRegValue(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\Windows Error Reporting", "DontShowUI", 1) )
	{
		__WARN__(("Fail to set error-show-registry(2) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckErrorReportingForVista();
}

INSTALL_STATUS CInstaller::InstallUACFor7()
{
	return InstallUACForVista();
}

INSTALL_STATUS CInstaller::InstallScreenSaverFor7()
{
	return InstallScreenSaverForVista();
}

INSTALL_STATUS CInstaller::InstallTrayIconNotificationsFor7()
{
	__FUNC_BEGIN__

	//
	CreateRegKey(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer");
	if( !SetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "HideSCAHealth", 1) )
	{
		__WARN__(("Fail to set security-alarm-registry (1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	CreateRegKey(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer");
	if( !SetRegValue(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "HideSCAHealth", 1) )
	{
		__WARN__(("Fail to set security-alarm-registry (2) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__

	InstallTrayIconNotifications();

	return CheckTrayIconNotificationsFor7();
}

INSTALL_STATUS CInstaller::InstallAeroThemeFor7()
{
	__FUNC_BEGIN__

	//
	CString str_cmd = "rundll32.exe Shell32.dll,Control_RunDLL desk.cpl desk,@Themes /Action:OpenTheme /File:\"C:\\Windows\\resources\\Ease of Access Themes\\basic.theme\"";
	if( !CreateProcess(GetSystemPath(), str_cmd, 30*1000) ) // wait 30-sec
	{
		__WARN__(("Fail to execute (%s) !!!", str_cmd));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	//
	for(int i=0; i<6000; i++) // wait 60-sec
	{
		::Sleep(10);

		CString str_value = 0;
		if( !GetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Themes", "CurrentTheme", str_value) )
			continue;

		str_value.MakeLower();
		if( str_value.Find("basic.theme") > 0 )
			break;
	}

	//
	HWND hwnd = NULL;
	for(int i=0; i<6000 && hwnd==NULL; i++) // wait 60-sec
	{
		::Sleep(10);
		hwnd = ::FindWindow("CabinetWClass", NULL);
	}

	if( hwnd == NULL )
	{
		__WARN__(("Fail to find Display control panel !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	CRect rect;
	::GetWindowRect(hwnd, rect);
	rect.MoveToXY(1000, 600);
	::MoveWindow(hwnd, rect.left, rect.top, rect.Width(), rect.Height(), TRUE);

	::SendMessage(hwnd, WM_CLOSE, 0, 0);

	__FUNC_END__
	return CheckAeroThemeFor7();
}

INSTALL_STATUS CInstaller::InstallErrorReportingFor7()
{
	__FUNC_BEGIN__

	InstallErrorReportingForVista();

	__FUNC_END__
	return CheckErrorReportingFor7();
}

INSTALL_STATUS CInstaller::InstallCodecConfig()
{
	__FUNC_BEGIN__

	CString str_subkey = "SOFTWARE\\Microsoft\\DirectShow\\Preferred";
	if( IsWow64() ) str_subkey = "SOFTWARE\\Wow6432Node\\Microsoft\\DirectShow\\Preferred";

	if( !ExecuteSetACL(HKEY_LOCAL_MACHINE, str_subkey) )
	{
		__WARN__(("Fail to SetACL !!! (%s\\%s)", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey));
	}

	CODEC_LIST codec_list;

	CODEC_ITEM add_item;
	add_item.strDecoderName = "LAV Video Decoder";
	add_item.strDecoderCLSID = "{EE30215D-164F-4A92-A4EB-9D4C13390F9F}";
	add_item.pszCodecList = szLAVVideoDecoders;
	codec_list.Add(add_item);

	add_item.strDecoderName = "LAV Audio Decoder";
	add_item.strDecoderCLSID = "{E8E73B6B-4CB3-44A4-BE99-4F7BCB96E491}";
	add_item.pszCodecList = szLAVAudioDecoders;
	codec_list.Add(add_item);

	add_item.strDecoderName = "WMVideo Decoder DMO";
	add_item.strDecoderCLSID = "{82d353df-90bd-4382-8bc2-3f6192b76e34}";
	add_item.pszCodecList = szWMVideoDecoder;
	codec_list.Add(add_item);

	add_item.strDecoderName = "WMAudio Decoder DMO";
	add_item.strDecoderCLSID = "{2eeb4adf-4578-4d10-bca7-bb955f56320a}";
	add_item.pszCodecList = szWMAudioDecoder;
	codec_list.Add(add_item);

	for(int i=0; i<codec_list.GetCount(); i++)
	{
		CODEC_ITEM& item = codec_list.GetAt(i);

		__DEBUG__(("Set Codec (%s)", item.strDecoderName));

		for(int i=0; strlen(item.pszCodecList[i])>0; i++)
		{
			if( !SetRegValue(HKEY_LOCAL_MACHINE, str_subkey, item.pszCodecList[i], item.strDecoderCLSID) )
			{
				__WARN__(("Fail to set codec-registry (%s\\%s\\%s) !!!", szHKEY[(int)HKEY_LOCAL_MACHINE & 0x0F], str_subkey, item.pszCodecList[i]));
				__FUNC_END__
				return INSTALL_STATUS_NOT_INSTALL;
			}
		}
	}

	__FUNC_END__
	return CheckCodecConfig();
}

INSTALL_STATUS CInstaller::InstallUACFor8()
{
	return InstallUACForVista();
}

INSTALL_STATUS CInstaller::InstallScreenSaverFor8()
{
	return InstallScreenSaverForVista();
}

INSTALL_STATUS CInstaller::InstallTrayIconNotificationsFor8()
{
	return InstallTrayIconNotificationsFor7();
}

INSTALL_STATUS CInstaller::InstallErrorReportingFor8()
{
	__FUNC_BEGIN__

	InstallErrorReportingForVista();

	__FUNC_END__
	return CheckErrorReportingFor8();
}

INSTALL_STATUS CInstaller::InstallCodecConfigFor8()
{
	return InstallCodecConfig();
}

INSTALL_STATUS CInstaller::InstallHotCornersFor8()
{
	__FUNC_BEGIN__

	CreateRegKey(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\ImmersiveShell\\EdgeUI");

	if( !SetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\ImmersiveShell\\EdgeUI", "DisableTLCorner", 1) )
	{
		__WARN__(("Fail to set disable-tlcorner-registry (1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	if( !SetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\ImmersiveShell\\EdgeUI", "DisableTRCorner", 1) )
	{
		__WARN__(("Fail to set disable-trcorner-registry (1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckHotCornersFor8();
}

INSTALL_STATUS CInstaller::InstallBootDesktopFor8()
{
	__FUNC_BEGIN__

	if( !SetRegValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StartPage", "OpenAtLogon", 0) )
	{
		__WARN__(("Fail to set open-at-logon-registry (0) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckBootDesktopFor8();
}

INSTALL_STATUS CInstaller::InstallLockScreenFor8()
{
	__FUNC_BEGIN__

	CreateRegKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Policies\\Microsoft\\Windows\\Personalization");

	if( !SetRegValue(HKEY_LOCAL_MACHINE, "SOFTWARE\\Policies\\Microsoft\\Windows\\Personalization", "NoLockScreen", 1) )
	{
		__WARN__(("Fail to set no-lock-screen-registry (1) !!!"));
		__FUNC_END__
		return INSTALL_STATUS_NOT_INSTALL;
	}

	__FUNC_END__
	return CheckLockScreenFor8();
}
