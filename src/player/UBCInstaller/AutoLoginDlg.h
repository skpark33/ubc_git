#pragma once
#include "afxwin.h"


// CAutoLoginDlg 대화 상자입니다.

class CAutoLoginDlg : public CDialog
{
	DECLARE_DYNAMIC(CAutoLoginDlg)

public:
	CAutoLoginDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAutoLoginDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_AUTO_LOGIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CFont	m_font;

	CString	m_strLoginID;
	CString	m_strPassword;
public:
	CStatic m_stcMsg;

	CStatic m_stcID;
	CStatic m_stcPassword;

	CEdit m_editID;
	CEdit m_editPassword;

	CButton m_btnOK;
	CButton m_btnCancel;

	LPCSTR	GetLoginID() { return m_strLoginID; };
	LPCSTR	GetPassword() { return m_strPassword; };
};
