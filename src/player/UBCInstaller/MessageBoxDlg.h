#pragma once
#include "afxwin.h"


// CMessageBoxDlg 대화 상자입니다.

class CMessageBoxDlg : public CDialog
{
	DECLARE_DYNAMIC(CMessageBoxDlg)

public:
	CMessageBoxDlg(LPCSTR lpszTitle, LPCSTR lpszMessage, UINT nType, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMessageBoxDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MESSAGE_BOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CFont		m_font;

	CString		m_strTitle;
	CString		m_strMessage;
	UINT		m_nType;

	HICON		m_hIcon;

	CArray<CButton*, CButton*>	m_listButtons;
public:
	CStatic		m_stcIcon;
	CStatic		m_stcMsg;
	CButton		m_btnYes;
	CButton		m_btnNo;
	CButton		m_btnOK;
	CButton		m_btnCancel;
	afx_msg void OnBnClickedYes();
	afx_msg void OnBnClickedNo();
};

int MessageBox(LPCSTR lpszTitle, LPCSTR lpszMessage, UINT nType=MB_ICONINFORMATION|MB_OK);
int MessageBox(LPCSTR lpszTitle, UINT nMsgID, UINT nType=MB_ICONINFORMATION|MB_OK);

int MessageBox(LPCSTR lpszMessage, UINT nType=MB_ICONINFORMATION|MB_OK);
int MessageBox(UINT nMsgID, UINT nType=MB_ICONINFORMATION|MB_OK);
