#pragma once
#include "afxwin.h"


// CSelectOSVerDlg 대화 상자입니다.

class CSelectOSVerDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectOSVerDlg)

public:
	CSelectOSVerDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSelectOSVerDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SELECT_OS_VERSION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CFont	m_font;
	OS_VERSION	m_OSVersion;

public:
	afx_msg void OnBnClickedButtonWindowsXp();
	afx_msg void OnBnClickedButtonWindowsVista();
	afx_msg void OnBnClickedButtonWindows7();
	afx_msg void OnBnClickedButtonWindows8();

	CButton		m_btnWindowsXP;
	CButton		m_btnWindowsVista;
	CButton		m_btnWindows7;
	CButton		m_btnWindows8;
	CButton		m_btnCancel;

	OS_VERSION	GetSelectOSVersion() { return m_OSVersion; };
};
