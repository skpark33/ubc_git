#pragma once
#include "afxwin.h"


// CDetectOSDlg 대화 상자입니다.

class CDetectOSDlg : public CDialog
{
	DECLARE_DYNAMIC(CDetectOSDlg)

public:
	CDetectOSDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDetectOSDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DETECT_OS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CFont	m_font;
	CFont	m_font2;

	OS_VERSION	m_eOSVer;
	bool	m_bFindOSVer;

public:
	CStatic m_stcMessage;

	afx_msg LRESULT OnSetOSVersion(WPARAM wParam, LPARAM lParam);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CStatic m_stcOSVersion;
	CButton m_btnClose;
	afx_msg void OnBnClickedClose();
};

static UINT CheckOSThread(LPVOID pParam);