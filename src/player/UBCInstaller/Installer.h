#pragma once

class CInstaller
{
public:
	CInstaller();
	~CInstaller();

	INSTALL_STATUS	Check(INSTALL_ITEM eItem);
	INSTALL_STATUS	Install(INSTALL_ITEM eItem);

	BOOL	AddProcessToRun();
	BOOL	DeleteProcessFromRun();

	BOOL	Reboot();
	BOOL	PopupUACfor7();
	BOOL	StopPcaSvc(); // Program Compatibility Assistant Service(프로그램 호환성 관리자) off

	OS_VERSION GetWindowsVersion();
	BOOL	IsWow64();

	BOOL	PopupLicenseCenter();
	BOOL	IsExistLicenseInfo();

protected:
	////////////////////////////////////////////////////////////////////////////////////////////
	BOOL	CreateProcessRedirection(LPCSTR lpszDir, LPCSTR lpszCmdLine, CString& strOutputString);
	BOOL	CreateProcess(LPCSTR lpszDir, LPCSTR lpszCmdLine, DWORD dwTimeout=INFINITE, WORD wShowWindow=SW_HIDE);

	LPCTSTR GetSetACLFilename();
	BOOL	ExecuteSetACL(HKEY hMainKey, LPCSTR lpszSubKey);

	LPCTSTR	GetProgramFilesPath();
	LPCTSTR	GetStartupPath();
	LPCTSTR	GetSystemPath();
	LPCTSTR GetAppPath();
	LPCTSTR Get3rdpartyPath();

	LPCTSTR	GetVNCPath();

	LPCTSTR	GetSIDofUserAccount();

	////////////////////////////////////////////////////////////////////////////////////////////
	CString	GetErrorMessage(LONG nCode);

	BOOL	CreateRegKey(HKEY hMainKey, LPCSTR lpszSubKey, DWORD dwOption=KEY_WOW64_64KEY);
	BOOL	DeleteRegKey(HKEY hMainKey, LPCSTR lpszSubKey, DWORD dwOption=KEY_WOW64_64KEY);

	BOOL	GetRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, DWORD& dwValue, DWORD dwOption=KEY_WOW64_64KEY);
	BOOL	GetRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, CString& strValue, DWORD dwOption=KEY_WOW64_64KEY);

	BOOL	SetRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, DWORD dwValue, DWORD dwOption=KEY_WOW64_64KEY);
	BOOL	SetRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, CString strValue, DWORD dwOption=KEY_WOW64_64KEY);
	BOOL	SetRegValueIfExist(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, DWORD dwValue, DWORD dwOption=KEY_WOW64_64KEY);

	BOOL	DeleteRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, DWORD dwOption=KEY_WOW64_64KEY);

	////////////////////////////////////////////////////////////////////////////////////////////
	// all-windows
	INSTALL_STATUS	CheckVNC();						// vnc
	INSTALL_STATUS	CheckKLiteCodec();				// k-lite codec
	INSTALL_STATUS	CheckFlashplayer();				// flash player
	INSTALL_STATUS	CheckShortCut();				// create short-cut
	INSTALL_STATUS	CheckFirewallException();		// firewall exception
	INSTALL_STATUS	CheckSecurity();				// set security
	INSTALL_STATUS	CheckSoftDrive();				// set soft drive
	INSTALL_STATUS	CheckAutoLogin();				// auto login
	INSTALL_STATUS	CheckTaskbar();					// auto-hide on, always-on-top off
	INSTALL_STATUS	CheckFlashUpdate();				// disable flsah update
	INSTALL_STATUS	CheckWindowsUpdate();			// disable windows update

	// xp
	INSTALL_STATUS	CheckCleanWizard();				// disable clean wizard
	INSTALL_STATUS	CheckScreenSaverForXP();		// disable screen saver
	INSTALL_STATUS	CheckSecurityCenterAlarm();		// disable security center alarm
	INSTALL_STATUS	CheckErrorReportingForXP();		// disable error reporting (for XP)

	// vista, 7, 8
	INSTALL_STATUS	CheckTrayIconNotifications();	// disable tray icon notifications

	// vista
	INSTALL_STATUS	CheckUACForVista();				// disable uac (for vista)
	INSTALL_STATUS	CheckScreenSaverForVista();		// disable screen saver (for vista)
	INSTALL_STATUS	CheckSideBar();					// disable side bar
	INSTALL_STATUS	CheckTrayIconNotificationsForVista();// disable tray icon notifications (for vista)
	INSTALL_STATUS	CheckAeroThemeForVista();		// disable aero theme (for vista)
	INSTALL_STATUS	CheckCustomerExperience();		// disable customer experience
	INSTALL_STATUS	CheckWelcomeCenter();			// disable welcome center
	INSTALL_STATUS	CheckErrorReportingForVista();		// disable error reporting (for vista)

	// 7
	INSTALL_STATUS	CheckUACFor7();					// disable uac (for 7)
	INSTALL_STATUS	CheckScreenSaverFor7();			// disable screen saver (for 7)
	INSTALL_STATUS	CheckTrayIconNotificationsFor7();// disable tray icon notifications (for 7)
	INSTALL_STATUS	CheckAeroThemeFor7();			// disable aero theme (for 7)
	INSTALL_STATUS	CheckErrorReportingFor7();		// disable error reporting (for 7)
	INSTALL_STATUS	CheckCodecConfig();				// set codec config

	// 8
	INSTALL_STATUS	CheckUACFor8();					// disable uac (for 8)
	INSTALL_STATUS	CheckScreenSaverFor8();			// disable screen saver (for 8)
	INSTALL_STATUS	CheckTrayIconNotificationsFor8();	// disable security center alarm (for 8)
	INSTALL_STATUS	CheckErrorReportingFor8();		// disable error reporting (for 8)
	INSTALL_STATUS	CheckCodecConfigFor8();			// set codec config (for 8)
	INSTALL_STATUS	CheckHotCornersFor8();			// disable the charms bar and switcher hot corners (for 8)
	INSTALL_STATUS	CheckBootDesktopFor8();			// booting directly to the desktop (for 8)
	INSTALL_STATUS	CheckLockScreenFor8();			// disable the lock screen (for 8)

	////////////////////////////////////////////////////////////////////////////////////////////
	// all-windows
	INSTALL_STATUS	InstallVNC();					// vnc
	INSTALL_STATUS	InstallKLiteCodec();			// k-lite codec
	INSTALL_STATUS	InstallFlashplayer();			// flash player
	INSTALL_STATUS	InstallShortCut();				// create short-cut
	INSTALL_STATUS	InstallFirewallException();		// firewall exception
	INSTALL_STATUS	InstallSecurity();				// set security
	INSTALL_STATUS	InstallSoftDrive();				// set soft drive
	INSTALL_STATUS	InstallAutoLogin();				// auto login
	INSTALL_STATUS	InstallTaskbar();				// auto-hide on, always-on-top off
	INSTALL_STATUS	InstallFlashUpdate();			// disable flsah update
	INSTALL_STATUS	InstallWindowsUpdate();			// disable windows update

	// xp
	INSTALL_STATUS	InstallCleanWizard();			// disable clean wizard
	INSTALL_STATUS	InstallScreenSaverForXP();		// disable screen saver (for xp)
	INSTALL_STATUS	InstallSecurityCenterAlarm();	// disable security center alarm
	INSTALL_STATUS	InstallErrorReportingForXP();	// disable error reporting (for xp)

	// vista, 7, 8
	INSTALL_STATUS	InstallTrayIconNotifications();	// disable tray icon notifications

	// vista
	INSTALL_STATUS	InstallUACForVista();			// disable uac (for vista)
	INSTALL_STATUS	InstallScreenSaverForVista();	// disable screen saver (for vista)
	INSTALL_STATUS	InstallSideBar();				// disable side bar
	INSTALL_STATUS	InstallTrayIconNotificationsForVista();	// disable security center alarm (for vista)
	INSTALL_STATUS	InstallAeroThemeForVista();		// disable aero theme (for vista)
	INSTALL_STATUS	InstallCustomerExperience();	// disable customer experience
	INSTALL_STATUS	InstallWelcomeCenter();			// disable welcome center
	INSTALL_STATUS	InstallErrorReportingForVista();// disable error reporting (for Vista)

	// 7
	INSTALL_STATUS	InstallUACFor7();				// disable uac (for 7)
	INSTALL_STATUS	InstallScreenSaverFor7();		// disable screen saver (for 7)
	INSTALL_STATUS	InstallTrayIconNotificationsFor7();	// disable security center alarm (for 7)
	INSTALL_STATUS	InstallAeroThemeFor7();			// disable aero theme (for 7)
	INSTALL_STATUS	InstallErrorReportingFor7();	// disable error reporting (for 7)
	INSTALL_STATUS	InstallCodecConfig();			// set codec config

	// 8
	INSTALL_STATUS	InstallUACFor8();				// disable uac (for 8)
	INSTALL_STATUS	InstallScreenSaverFor8();		// disable screen saver (for 8)
	INSTALL_STATUS	InstallTrayIconNotificationsFor8();	// disable security center alarm (for 8)
	INSTALL_STATUS	InstallErrorReportingFor8();	// disable error reporting (for 8)
	INSTALL_STATUS	InstallCodecConfigFor8();		// set codec config (for 8)
	INSTALL_STATUS	InstallHotCornersFor8();		// disable the charms bar and switcher hot corners (for 8)
	INSTALL_STATUS	InstallBootDesktopFor8();		// booting directly to the desktop (for 8)
	INSTALL_STATUS	InstallLockScreenFor8();		// disable the lock screen (for 8)

};
