// DetectOSDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCInstaller.h"
#include "DetectOSDlg.h"

#include "Installer.h"


#define		TIMER_ID_CHECKING_OS		2049
#define		TIMER_TIME_CHECKING_OS		200

#define		TIMER_ID_AUTO_CLOSE			2050
#define		TIMER_TIME_AUTO_CLOSE		4000


// CDetectOSDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDetectOSDlg, CDialog)

CDetectOSDlg::CDetectOSDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDetectOSDlg::IDD, pParent)
	, m_eOSVer ( OS_NOT_SUPPORT )
	, m_bFindOSVer ( false )
{
	m_font.CreatePointFont(14*10, ::GetDefaultFontName());

	m_font2.CreateFont( 34,						// nHeight
						0,							// nWidth
						0,							// nEscapement
						0,							// nOrientation
						FW_BOLD,					// nWeight
						FALSE,						// bItalic
						FALSE,						// bUnderline
						0,							// cStrikeOut
						ANSI_CHARSET,				// nCharSet
						OUT_DEFAULT_PRECIS,		// nOutPrecision
						CLIP_DEFAULT_PRECIS,		// nClipPrecision
						DEFAULT_QUALITY,			// nQuality
						DEFAULT_PITCH | FF_SWISS,
						::GetDefaultFontName() );

}

CDetectOSDlg::~CDetectOSDlg()
{
}

void CDetectOSDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_MESSAGE, m_stcMessage);
	DDX_Control(pDX, IDC_STATIC_OS_VERSION, m_stcOSVersion);
	DDX_Control(pDX, IDC_CLOSE, m_btnClose);
}


BEGIN_MESSAGE_MAP(CDetectOSDlg, CDialog)
	ON_MESSAGE(WM_SET_OS_VERSION, OnSetOSVersion)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CLOSE, &CDetectOSDlg::OnBnClickedClose)
END_MESSAGE_MAP()


// CDetectOSDlg 메시지 처리기입니다.

BOOL CDetectOSDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(::LoadString(IDS_CAPTION_DETECT_OS));

	//
	m_stcMessage.SetFont(&m_font);
	m_stcMessage.SetWindowText(::LoadString(IDS_MSG_DETECTING_OS_VER));

	//
	m_stcOSVersion.SetFont(&m_font2);

	//
	m_btnClose.SetFont(&m_font);
	m_btnClose.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_CLOSE)));
	m_btnClose.SetWindowText(::LoadString(IDS_BUTTON_OK));

	// start display-processing-timer
	SetTimer(TIMER_ID_CHECKING_OS, TIMER_TIME_CHECKING_OS, NULL);

	// start checking-os-ver
	CWinThread* thread = ::AfxBeginThread(CheckOSThread, (LPVOID)GetSafeHwnd(), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	thread->m_bAutoDelete = true;
	thread->ResumeThread();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDetectOSDlg::OnOK()
{
	//CDialog::OnOK();
}

void CDetectOSDlg::OnCancel()
{
	//CDialog::OnCancel();
}

LRESULT CDetectOSDlg::OnSetOSVersion(WPARAM wParam, LPARAM lParam)
{
	if( m_pParentWnd && m_pParentWnd->GetSafeHwnd() )
	{
		m_bFindOSVer = true;
		m_eOSVer = (OS_VERSION)wParam;

		::PostMessage(m_pParentWnd->GetSafeHwnd(), WM_SET_OS_VERSION, wParam, lParam);

		// not suooprt os  ==>  close
		if( m_eOSVer == OS_NOT_SUPPORT )
			OnBnClickedClose();
	}

	return 1;
}

UINT CheckOSThread(LPVOID pParam)
{
	HWND parent_wnd = (HWND)pParam;

	DWORD start_tick = ::GetTickCount();

	CInstaller installer;
	installer.StopPcaSvc();

	//
	OS_VERSION os_ver = installer.GetWindowsVersion();
	//if( os_ver == OS_NOT_SUPPORT )
	//	os_ver = OS_WIN_XP;

	DWORD end_tick = ::GetTickCount();

	// prevent fast-closing
	if( end_tick - start_tick < 1000 )
		::Sleep(1000);

	::PostMessage(parent_wnd, WM_SET_OS_VERSION, (WPARAM)os_ver, (LPARAM)NULL);

	return 1;
}

void CDetectOSDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case TIMER_ID_CHECKING_OS:
		if( m_bFindOSVer == false )
		{
			static int cnt = 0;
			CString str;
			for(int i=0; i<(cnt%7); i++) str+=".";
			m_stcOSVersion.SetWindowText(str);
			cnt++;
		}
		else
		{
			KillTimer(TIMER_ID_CHECKING_OS);
			m_stcMessage.SetWindowText(::LoadString(IDS_MSG_DETECTED_OS_VER));
			m_stcOSVersion.SetWindowText(::GetOSVersionString(m_eOSVer));
			m_stcOSVersion.ShowWindow(SW_SHOW);
			m_btnClose.ShowWindow(SW_SHOW);
			m_btnClose.EnableWindow();

			//SetTimer(TIMER_ID_AUTO_CLOSE, TIMER_TIME_AUTO_CLOSE, NULL);
		}
		break;

	case TIMER_ID_AUTO_CLOSE:
		KillTimer(TIMER_ID_AUTO_CLOSE);
		ShowWindow(SW_HIDE);
		break;
	}

	CDialog::OnTimer(nIDEvent);
}

void CDetectOSDlg::OnBnClickedClose()
{
	ShowWindow(SW_HIDE);
}
