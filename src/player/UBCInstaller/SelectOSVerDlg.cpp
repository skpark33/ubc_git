// SelectOSVerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCInstaller.h"
#include "SelectOSVerDlg.h"


// CSelectOSVerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSelectOSVerDlg, CDialog)

CSelectOSVerDlg::CSelectOSVerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectOSVerDlg::IDD, pParent)
{
	m_font.CreatePointFont(14*10, ::GetDefaultFontName());
	m_OSVersion = OS_NOT_SUPPORT;
}

CSelectOSVerDlg::~CSelectOSVerDlg()
{
}

void CSelectOSVerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_WINDOWS_XP, m_btnWindowsXP);
	DDX_Control(pDX, IDC_BUTTON_WINDOWS_VISTA, m_btnWindowsVista);
	DDX_Control(pDX, IDC_BUTTON_WINDOWS_7, m_btnWindows7);
	DDX_Control(pDX, IDC_BUTTON_WINDOWS_8, m_btnWindows8);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CSelectOSVerDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_WINDOWS_XP, &CSelectOSVerDlg::OnBnClickedButtonWindowsXp)
	ON_BN_CLICKED(IDC_BUTTON_WINDOWS_VISTA, &CSelectOSVerDlg::OnBnClickedButtonWindowsVista)
	ON_BN_CLICKED(IDC_BUTTON_WINDOWS_7, &CSelectOSVerDlg::OnBnClickedButtonWindows7)
	ON_BN_CLICKED(IDC_BUTTON_WINDOWS_8, &CSelectOSVerDlg::OnBnClickedButtonWindows8)
END_MESSAGE_MAP()


// CSelectOSVerDlg 메시지 처리기입니다.

BOOL CSelectOSVerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_btnWindowsXP.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_WINDOWS_XP)));
	m_btnWindowsVista.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_WINDOWS_VISTA)));
	m_btnWindows7.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_WINDOWS_7)));
	m_btnWindows8.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_WINDOWS_8)));
	m_btnCancel.SetIcon(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_EXIT)));

	m_btnWindowsXP.SetFont(&m_font);
	m_btnWindowsVista.SetFont(&m_font);
	m_btnWindows7.SetFont(&m_font);
	m_btnWindows8.SetFont(&m_font);
	m_btnCancel.SetFont(&m_font);

	m_btnWindowsXP.SetWindowText(::LoadString(IDS_CAPTION_SELECT_OS_VERSION_WIN_XP));
	m_btnWindowsVista.SetWindowText(::LoadString(IDS_CAPTION_SELECT_OS_VERSION_WIN_VISTA));
	m_btnWindows7.SetWindowText(::LoadString(IDS_CAPTION_SELECT_OS_VERSION_WIN_7));
	m_btnWindows8.SetWindowText(::LoadString(IDS_CAPTION_SELECT_OS_VERSION_WIN_8));
	m_btnCancel.SetWindowText(::LoadString(IDS_SELECT_OS_VERSION_BUTTON_CANCEL));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSelectOSVerDlg::OnBnClickedButtonWindowsXp()
{
	m_OSVersion = OS_WIN_XP;
	CDialog::OnOK();
}

void CSelectOSVerDlg::OnBnClickedButtonWindowsVista()
{
	m_OSVersion = OS_WIN_VISTA;
	CDialog::OnOK();
}

void CSelectOSVerDlg::OnBnClickedButtonWindows7()
{
	m_OSVersion = OS_WIN_7;
	CDialog::OnOK();
}

void CSelectOSVerDlg::OnBnClickedButtonWindows8()
{
	m_OSVersion = OS_WIN_8;
	CDialog::OnOK();
}
