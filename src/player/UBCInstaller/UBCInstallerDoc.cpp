// UBCInstallerDoc.cpp : CUBCInstallerDoc 클래스의 구현
//

#include "stdafx.h"
#include "UBCInstaller.h"

#include "UBCInstallerDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUBCInstallerDoc

IMPLEMENT_DYNCREATE(CUBCInstallerDoc, CDocument)

BEGIN_MESSAGE_MAP(CUBCInstallerDoc, CDocument)
END_MESSAGE_MAP()


// CUBCInstallerDoc 생성/소멸

CUBCInstallerDoc::CUBCInstallerDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CUBCInstallerDoc::~CUBCInstallerDoc()
{
}

BOOL CUBCInstallerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CUBCInstallerDoc serialization

void CUBCInstallerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}


// CUBCInstallerDoc 진단

#ifdef _DEBUG
void CUBCInstallerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CUBCInstallerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CUBCInstallerDoc 명령
