// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "UBCInstaller.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
{
}

CMainFrame::~CMainFrame()
{
}


BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	cs.style = WS_OVERLAPPED | WS_CAPTION /*| FWS_ADDTOTITLE*/ | WS_MAXIMIZE | WS_SYSMENU;

	return TRUE;
}


// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame 메시지 처리기


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetMenu(NULL);

	// add font in resource
	HINSTANCE hResInstance = AfxGetResourceHandle( );
	HRSRC res = FindResource(hResInstance, MAKEINTRESOURCE(IDR_FONT_KOR), "FONTEX");
	if( res )
	{
		HGLOBAL mem = LoadResource(hResInstance, res);
		void *data = LockResource(mem);
		size_t len = SizeofResource(hResInstance, res);

		DWORD nFonts;
		HANDLE m_fonthandle = AddFontMemResourceEx( data,		// font resource
													len,		// number of bytes in font resource 
													NULL,		// Reserved. Must be 0.
													&nFonts		// number of fonts installed
													);

		if( m_fonthandle == NULL )
		{
			::AfxMessageBox("Fail to add a embedding font !!!", MB_ICONSTOP);
		}
	}
	return 0;
}
