// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <afxsock.h>		// MFC 소켓 확장
#include <afxtempl.h>
#include <afxmt.h>


#include "Log.h"


#define		WM_SET_OS_VERSION		(WM_USER + 4097)


static char* szHKEY[] = {
	"HKEY_CLASSES_ROOT", // 0
	"HKEY_CURRENT_USER", // 1
	"HKEY_LOCAL_MACHINE", // 2
	"HKEY_USERS", // 3
	"HKEY_PERFORMANCE_DATA", // 4
	"HKEY_CURRENT_CONFIG", // 5
	"HKEY_DYN_DATA", // 6
	"Unknown HKEY",
	"Unknown HKEY",
	"Unknown HKEY",
	"Unknown HKEY",
	"Unknown HKEY",
	"Unknown HKEY",
	"Unknown HKEY",
	"Unknown HKEY",
	"Unknown HKEY",
};

enum UBC_EDITION {
	UBC_EDITION_UNKNOWN = 0,
	UBC_EDITION_STANDARD,
	UBC_EDITION_STANDARD_PLUS,
	UBC_EDITION_ENTERPRISE,
};

enum OS_VERSION {
	OS_NOT_SUPPORT = 0,
	OS_WIN_XP,
	OS_WIN_VISTA,
	OS_WIN_7,
	OS_WIN_8,
	OS_WIN_9,

	OS_COUNT
};

static char* szOSVersion[] = {
	"Not Support OS",	// OS_NOT_SUPPORT
	"Windows XP",		// OS_WIN_XP
	"Windows Vista",	// OS_WIN_VISTA
	"Windows 7",		// OS_WIN_7
	"Windows 8",		// OS_WIN_8
	"Windows 9",		// OS_WIN_9
};
LPCSTR	GetOSVersionString(OS_VERSION eOSVersion);

static char* szInstallItemName[] = {
	" Unknown Install Item",
	" Install VNC Server",
	" Install K-Lite Codec Pack",
	" Install Flashplayer",
	" Create Shortcut", // copy UBCReady.lnk to startup-group
	" Set Firewall Exception", // register to firewall-exception from firewallException.txt
	" Set Security", // hide contents-path
	" Set Soft Drive", // create ftproot & subst x:
	" Set Taskbar", // auto-hide on, always-on-top off
	" Set Windows Auto Login",
	" Disable Flashplayer Update", // 10
	" Disable Windows Update",

	// xp
	" Disable Desktop Cleanup Wizard", // 12
	" Disable Screen Saver",
	" Disable Security Center Alarm",
	" Disable Windows Error Reporting", // disable windows error reporting for xp

	// vista
	" Disable UAC", // 16
	" Disable Screen Saver",
	" Disable Side Bar",
	" Disable Tray Icon Notifications", // vista
	" Disable Windows Aero Theme", // vista
	" Disable Windows Customer Experience Improvement Message",
	" Disable Welcome Center",
	" Disable Windows Error Reporting", // disable windows error reporting for vista,7

	// 7
	" Disable UAC", // 24
	" Disable Screen Saver",
	" Disable Tray Icon Notifications",
	" Disable Windows Aero Theme",
	" Disable Windows Error Reporting", // disable windows error reporting for vista,7
	" Set Codec Configuration", // win7

	// 8
	" Disable UAC", // 30
	" Disable Screen Saver",
	" Disable Tray Icon Notifications",
	" Disable Windows Error Reporting", // disable windows error reporting for vista,7,8
	" Set Codec Configuration", // win7
	" Disable Hot Corners", // disable the charms bar and switcher hot corners for win8
	" Set Booting Desktop", // booting directly to the desktop for win8
	" Disable Lock Screen", // disable the lock screen for win8

};

enum INSTALL_ITEM {
	INSTALL_ITEM_UNKNOWN = 0,
	INSTALL_ITEM_VNC, // 1
	INSTALL_ITEM_KLITE_CODEC,
	INSTALL_ITEM_FLASHPLAYER,
	INSTALL_ITEM_SHORTCUT,
	INSTALL_ITEM_FIREWALL_EXCEPTION,
	INSTALL_ITEM_SECURITY,
	INSTALL_ITEM_SOFT_DRIVE,
	INSTALL_ITEM_TASKBAR,
	INSTALL_ITEM_AUTO_LOGIN,
	INSTALL_ITEM_FLASHPLAYER_UPDATE, // 10
	INSTALL_ITEM_WINDOWS_UPDATE,

	// xp
	INSTALL_ITEM_CLEANUP_WIZARD, // 12
	INSTALL_ITEM_SCREEN_SAVER_XP,
	INSTALL_ITEM_SECURITY_CENTER_ALARM,
	INSTALL_ITEM_WINDOWS_ERROR_REPORTING_XP,

	// vista
	INSTALL_ITEM_UAC_VISTA, // 16
	INSTALL_ITEM_SCREEN_SAVER_VISTA,
	INSTALL_ITEM_SIDE_BAR,
	INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_VISTA,
	INSTALL_ITEM_AERO_THEME_VISTA,
	INSTALL_ITEM_CUSTOMER_EXPERIENCE,
	INSTALL_ITEM_WELCOME_CENTER,
	INSTALL_ITEM_WINDOWS_ERROR_REPORTING_VISTA,

	// 7
	INSTALL_ITEM_UAC_7, // 24
	INSTALL_ITEM_SCREEN_SAVER_7,
	INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_7,
	INSTALL_ITEM_AERO_THEME_7,
	INSTALL_ITEM_WINDOWS_ERROR_REPORTING_7,
	INSTALL_ITEM_CODEC_CONFIG,

	// 8
	INSTALL_ITEM_UAC_8, // 30
	INSTALL_ITEM_SCREEN_SAVER_8,
	INSTALL_ITEM_TRAY_ICON_NOTIFICATIONS_8,
	INSTALL_ITEM_WINDOWS_ERROR_REPORTING_8,
	INSTALL_ITEM_CODEC_CONFIG_8,
	INSTALL_ITEM_HOT_CORNERS_8,
	INSTALL_ITEM_BOOT_DESKTOP_8,
	INSTALL_ITEM_LOCK_SCREEN_8,

	INSTALL_ITEM_COUNT,
};

enum INSTALL_STATUS {
	INSTALL_STATUS_UNKNOWN = 0,
	INSTALL_STATUS_UNKNOWN2,
	INSTALL_STATUS_CHECKING, // 2
	INSTALL_STATUS_NOT_INSTALL, // 3
	INSTALL_STATUS_INSTALLING, // 4
	INSTALL_STATUS_INSTALLED, // 5

	INSTALL_STATUS_COUNT
};

static char* szInstallStatus[] = {
	"Unknown Status",	// INSTALL_STATUS_UNKNOWN
	"Unknown Status",	// INSTALL_STATUS_UNKNOWN2
	"Checking",			// INSTALL_STATUS_CHECKING
	"Not Installed",	// INSTALL_STATUS_NOT_INSTALL
	"Installing",		// INSTALL_STATUS_INSTALLING
	"Installed",		// INSTALL_STATUS_INSTALLED
};
LPCSTR	GetInstallStatusString(INSTALL_STATUS eInstallStatus);


class INSTALL_DATA {
public:
	INSTALL_DATA(INSTALL_ITEM item=INSTALL_ITEM_UNKNOWN, INSTALL_STATUS status=INSTALL_STATUS_CHECKING)
		{ eItem=item; eInstallStatus=status; };
	~INSTALL_DATA() {};

	INSTALL_ITEM	eItem;
	INSTALL_STATUS	eInstallStatus;
};

typedef		CArray<INSTALL_DATA*, INSTALL_DATA*>		INSTALL_DATA_LIST;


UBC_EDITION	GetUBCEdition();
LANGID		GetLangIDfromParameter();
LPCSTR		GetDefaultFontName();
LPCSTR		LoadString(UINT nID); // not safe in thread. don't use in thread.

using namespace std;

//#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
//#endif


