// UBCInstallerView.cpp : CUBCInstallerView 클래스의 구현
//

#include "stdafx.h"
#include "UBCInstaller.h"

#include "UBCInstallerDoc.h"
#include "UBCInstallerView.h"

#include "InstallDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		GRADIANT_VERT
#define		COLOR_STD		RGB(53, 66, 10)
#define		COLOR_PLS		RGB(138, 4, 15)
#define		COLOR_ENT		RGB(19, 33, 56)


// CUBCInstallerView

IMPLEMENT_DYNCREATE(CUBCInstallerView, CFormView)

BEGIN_MESSAGE_MAP(CUBCInstallerView, CFormView)
	ON_WM_TIMER()
	ON_WM_PAINT()
END_MESSAGE_MAP()

// CUBCInstallerView 생성/소멸

CUBCInstallerView::CUBCInstallerView()
	: CFormView(CUBCInstallerView::IDD)
{
	m_rgbBackGround = COLOR_STD;
}

CUBCInstallerView::~CUBCInstallerView()
{
}

void CUBCInstallerView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CUBCInstallerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CUBCInstallerView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//
	switch( ::GetUBCEdition() )
	{
	case UBC_EDITION_STANDARD_PLUS:
		m_rgbBackGround = COLOR_PLS;
		break;
	case UBC_EDITION_ENTERPRISE:
		m_rgbBackGround = COLOR_ENT;
		break;
	default:
		m_rgbBackGround = COLOR_STD;
		break;
	}

	//
	SetTimer(1025, 10, NULL);
}


// CUBCInstallerView 진단

#ifdef _DEBUG
void CUBCInstallerView::AssertValid() const
{
	CFormView::AssertValid();
}

void CUBCInstallerView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CUBCInstallerDoc* CUBCInstallerView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCInstallerDoc)));
	return (CUBCInstallerDoc*)m_pDocument;
}
#endif //_DEBUG


// CUBCInstallerView 메시지 처리기

void CUBCInstallerView::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case 1025:
		KillTimer(1025);
		{
			CInstallDlg dlg;
			dlg.DoModal();
			::AfxGetMainWnd()->PostMessage(WM_QUIT); // exit this
		}
		break;
	}

	CFormView::OnTimer(nIDEvent);
}

void CUBCInstallerView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CFormView::OnPaint()을(를) 호출하지 마십시오.

	CRect rtRect;
	GetClientRect(&rtRect);

	TRIVERTEX			vert[4];
	GRADIENT_TRIANGLE	gTri[2];

	vert [0] .x       =  rtRect.left;
	vert [0] .y       =  rtRect.top;
	vert [0] .Red     =  0xff00;
	vert [0] .Green   =  0xff00;
	vert [0] .Blue    =  0xff00;
	vert [0] .Alpha   =  0x0000;

	vert [1] .x       =  rtRect.right;
	vert [1] .y       =  rtRect.top;
	vert [1] .Red     =  0xff00;
	vert [1] .Green   =  0xff00;
	vert [1] .Blue    =  0xff00;
	vert [1] .Alpha   =  0x0000;

	vert [2] .x       =  rtRect.right;
	vert [2] .y       =  rtRect.bottom; 
	vert [2] .Red     =  (m_rgbBackGround & 0x000000FF) << 8;
	vert [2] .Green   =  (m_rgbBackGround & 0x0000FF00);
	vert [2] .Blue    =  (m_rgbBackGround & 0x00FF0000) >> 8;
	vert [2] .Alpha   =  0x0000;

	vert [3] .x       =  rtRect.left;
	vert [3] .y       =  rtRect.bottom;
	vert [3] .Red     =  (m_rgbBackGround & 0x000000FF) << 8;
	vert [3] .Green   =  (m_rgbBackGround & 0x0000FF00);
	vert [3] .Blue    =  (m_rgbBackGround & 0x00FF0000) >> 8;
	vert [3] .Alpha   =  0x0000;

	gTri[0].Vertex1   = 0;
	gTri[0].Vertex2   = 1;
	gTri[0].Vertex3   = 2;

	gTri[1].Vertex1   = 0;
	gTri[1].Vertex2   = 2;
	gTri[1].Vertex3   = 3;

	GradientFill(dc.m_hDC, vert, 4,&gTri, 2, GRADIENT_FILL_TRIANGLE);

}
