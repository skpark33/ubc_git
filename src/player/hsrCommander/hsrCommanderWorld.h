 /*! \file hsrCommanderWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _hsrCommanderWorld_h_
#define _hsrCommanderWorld_h_
#pragma once


#include <cci/libWorld/cciORBWorld.h> 
#include <ci/libThread/ciMutex.h> 
#include <ci/libThread/ciThread.h> 


//class hsrCommanderWorld : public cciORBWorld {
class hsrCommanderWorld : public ciWorld {
public:
	hsrCommanderWorld();
	~hsrCommanderWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

	static  int password();


protected:	
	static int _wget(char* arg);
	static int _cli(char* arg);
	static int _run(char* arg);
	static int _login(char* arg);
	static int _logout(char* arg);

	static ciBoolean _runCommand(const char* commandStr, ciString& outVal, ciBoolean bPrint);
	static ciBoolean _runCLI(const char* commandStr);


	static int _port;
	static int _socketPort;
	static ciString _ip;

public:
	
};


#endif //_hsrCommanderWorld_h_
