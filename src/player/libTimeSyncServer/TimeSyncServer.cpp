#include "StdAfx.h"

#include "TimeSyncServer.h"


#define		DEFAULT_SYNC_INTERVAL		(10*60)		// default 10-min
#define		DEFAULT_SYNC_UDP_PORT		(8800)		// UDP 14000 port


CTimeSyncServer* CTimeSyncServer::_instance = 0; 

CTimeSyncServer* CTimeSyncServer::getInstance()
{
	if( _instance==NULL ) _instance = new CTimeSyncServer;
	return _instance;
}

void CTimeSyncServer::clearInstance()
{
	if( _instance )
	{
		delete _instance;
		_instance = NULL;
	}
}

CTimeSyncServer::CTimeSyncServer()
{
	::ZeroMemory(m_szHostname, sizeof(m_szHostname));
	DWORD size = sizeof(m_szHostname);
	GetComputerNameEx(ComputerNamePhysicalDnsHostname, m_szHostname, &size);

	m_nSyncUDPPort = DEFAULT_SYNC_UDP_PORT;
	m_nSyncIntervalSec = DEFAULT_SYNC_INTERVAL;
	m_hMessageReceiveWnd = NULL;

	m_pThread = NULL;
	m_bRunThread = false;
}

CTimeSyncServer::~CTimeSyncServer()
{
	DeleteThread();
}

bool CTimeSyncServer::DeleteThread()
{
	m_bRunThread = false;

	if(m_pThread)
	{
		DWORD dwExitCode = 0;

		try
		{
			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);
		}
		catch(...)
		{
			//DWORD dw = GetLastError();
			//CHAR szBuf[80];
			//sprintf(szBuf, "GetLastError Code : %d", dw);
		}

		try
		{
			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, 3000);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}

			delete m_pThread;
		}
		catch(...)
		{
			//DWORD dw = GetLastError();
			//CHAR szBuf[80];
			//sprintf(szBuf, "GetLastError Code : %d", dw);
		}

		m_pThread = NULL;
	}

	return true;
}

bool CTimeSyncServer::StartSync()
{
	if( m_pThread ) return true;

	m_bRunThread = true;
	m_pThread = ::AfxBeginThread(TimeSyncFunc, (LPVOID)this, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
	m_pThread->m_bAutoDelete = FALSE;
	m_pThread->ResumeThread();

	return true;
}

bool CTimeSyncServer::StopSync()
{
	return DeleteThread();
}

UINT CTimeSyncServer::TimeSyncFunc(LPVOID pParam)
{
	CTimeSyncServer* sync_server = (CTimeSyncServer*)pParam;

	char* hostname = sync_server->m_szHostname;
	UINT sync_udp_port = sync_server->m_nSyncUDPPort;
	int interval_sec = sync_server->m_nSyncIntervalSec;
	HWND msg_recv_wnd = sync_server->m_hMessageReceiveWnd;

	CAsyncSocket udp_sock;
	if( !udp_sock.Create(sync_udp_port, SOCK_DGRAM) )
	{
		// socket fail
		if( msg_recv_wnd ) ::PostMessage(msg_recv_wnd, WM_TIME_SYNC_ERROR, TIME_SYNC_ERROR_SOCKET_CREATE_FAIL, NULL);
		return FALSE;
	}

	char ch_broadcast = 1;
	udp_sock.SetSockOpt(SO_BROADCAST, (char*)&ch_broadcast, sizeof(ch_broadcast));

	int no_delay = 1;
	udp_sock.SetSockOpt(TCP_NODELAY, &no_delay, sizeof(no_delay));

	time_t sync_time = 0;
	while( sync_server->m_bRunThread )
	{
		time_t cur_time = CTime::GetCurrentTime().GetTime();

		if( cur_time > sync_time+(interval_sec) )
		{
			if( msg_recv_wnd)
				::PostMessage(msg_recv_wnd, WM_TIME_SYNC_SEND, NULL, NULL);

			sync_time = cur_time;

			for(int i=0; i<150 && sync_server->m_bRunThread; i++) // 150 times -> 1.5sec
			{
				char buf[128] = {0};
				sprintf(buf, "_TIME_%s|%I64d_SYNC_", hostname, CTime::GetCurrentTime().GetTime());

				int total_size = strlen(buf);
				int send_size = 0;
				while(total_size > send_size && sync_server->m_bRunThread)
				{
					int size = udp_sock.SendTo(buf+send_size, total_size-send_size, sync_udp_port);
					if( size == SOCKET_ERROR )
					{
						if( msg_recv_wnd ) ::PostMessage(msg_recv_wnd, WM_TIME_SYNC_ERROR, TIME_SYNC_ERROR_SEND_FAIL, NULL);
						return FALSE;
					}
					send_size += size;
				}
				::Sleep(10); // wait 10 msec
			}
		}

		::Sleep(100); // wait 100 msec
	}

	return TRUE;
}
