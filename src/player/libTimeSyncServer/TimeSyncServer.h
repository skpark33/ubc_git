#pragma once

#include <afxsock.h>


// Message
#define		WM_TIME_SYNC_ERROR					(WM_USER + 1400)
// WPARAM
#define		TIME_SYNC_ERROR_SOCKET_CREATE_FAIL	(1)
#define		TIME_SYNC_ERROR_SEND_FAIL			(2)

// Message
#define		WM_TIME_SYNC_SEND					(WM_USER + 1401)


class CTimeSyncServer
{
public:
	static CTimeSyncServer*	getInstance();
	static void	clearInstance();

protected:
	static CTimeSyncServer*	_instance;

	CTimeSyncServer(void);
public:
	virtual ~CTimeSyncServer(void);

protected:
	char	m_szHostname[64];
	HWND	m_hMessageReceiveWnd;
	UINT	m_nSyncUDPPort;
	int		m_nSyncIntervalSec;	// sync interval (seconds)

	CWinThread*		m_pThread;
	bool	m_bRunThread;
	bool	DeleteThread();
	static	UINT	TimeSyncFunc(LPVOID pParam);

public:
	void	SetMessageReceiveWnd(HWND hWnd) { m_hMessageReceiveWnd = hWnd; };

	void	SetSyncUDPPort(UINT nPort) { m_nSyncUDPPort = nPort; };
	void	SetSyncInterval(int nSec) { m_nSyncIntervalSec = nSec; };

	bool	StartSync();
	bool	StopSync();

	UINT	GetSyncPort() { return m_nSyncUDPPort; };
	UINT	GetSyncInterval() { return m_nSyncIntervalSec; }; // sec
};
