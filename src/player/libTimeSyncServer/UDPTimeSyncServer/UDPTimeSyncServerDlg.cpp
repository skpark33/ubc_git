// UDPTimeSyncServerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UDPTimeSyncServer.h"
#include "UDPTimeSyncServerDlg.h"

#include "../TimeSyncServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CUDPTimeSyncServerDlg 대화 상자




CUDPTimeSyncServerDlg::CUDPTimeSyncServerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUDPTimeSyncServerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_font.CreatePointFont(72*10, "굴림");
}

void CUDPTimeSyncServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CURRENT_TIME, m_editCurrentTime);
	DDX_Control(pDX, IDC_EDIT_STATUS, m_editStatus);
	DDX_Control(pDX, IDC_EDIT_SYNC_PORT, m_editSyncPort);
	DDX_Control(pDX, IDC_EDIT_SYNC_INTERVAL, m_editSyncInterval);
	DDX_Control(pDX, IDC_BUTTON_START, m_btnStart);
	DDX_Control(pDX, IDC_BUTTON_STOP, m_btnStop);
	DDX_Control(pDX, IDC_BUTTON_SAVE_SETTINGS, m_btnSaveSettings);
}

BEGIN_MESSAGE_MAP(CUDPTimeSyncServerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_START, &CUDPTimeSyncServerDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CUDPTimeSyncServerDlg::OnBnClickedButtonStop)
	ON_MESSAGE(WM_TIME_SYNC_ERROR, OnTimeSyncError)
	ON_MESSAGE(WM_TIME_SYNC_SEND, OnTimeSyncSend)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_SETTINGS, &CUDPTimeSyncServerDlg::OnBnClickedButtonSaveSettings)
END_MESSAGE_MAP()


// CUDPTimeSyncServerDlg 메시지 처리기

BOOL CUDPTimeSyncServerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	m_editCurrentTime.SetFont(&m_font);
	m_editStatus.SetWindowText("중지");

	CTimeSyncServer::getInstance()->SetMessageReceiveWnd(GetSafeHwnd());

	char buf[1024]={0};
	::GetPrivateProfileString("ROOT", "TIME_SYNC_PORT", "", buf, 1023, ::GetUBCVariablesIniPath());
	UINT port = atoi(buf);
	if( port>0 ) CTimeSyncServer::getInstance()->SetSyncUDPPort(port);
	port = CTimeSyncServer::getInstance()->GetSyncPort();
	sprintf(buf, "%d", port);
	m_editSyncPort.SetWindowText(buf);

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("ROOT", "TIME_SYNC_INTERVAL", "", buf, 1023, ::GetUBCVariablesIniPath());
	UINT interval = atoi(buf);
	if( interval>0 ) CTimeSyncServer::getInstance()->SetSyncInterval(interval);
	interval = CTimeSyncServer::getInstance()->GetSyncInterval();
	sprintf(buf, "%d", interval);
	m_editSyncInterval.SetWindowText(buf);

	EnableControls(FALSE);

	SetTimer(1025, 1000, NULL);
	SetTimer(1026, 50, NULL);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUDPTimeSyncServerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUDPTimeSyncServerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUDPTimeSyncServerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CUDPTimeSyncServerDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 1025)
	{
		KillTimer(1025);
		OnBnClickedButtonStart();
	}
	if(nIDEvent == 1026)
	{
		CTime cur_tm = CTime::GetCurrentTime();

		if( cur_tm != m_tmCurrent )
		{
			m_tmCurrent = cur_tm;
			m_editCurrentTime.SetWindowText(m_tmCurrent.Format("%H:%M:%S"));
		}
	}

	CDialog::OnTimer(nIDEvent);
}

void CUDPTimeSyncServerDlg::OnBnClickedButtonStart()
{
	CString str_port;
	m_editSyncPort.GetWindowText(str_port);
	UINT port = atoi(str_port);
	if( port>0 ) CTimeSyncServer::getInstance()->SetSyncUDPPort(port);

	CString str_interval;
	m_editSyncInterval.GetWindowText(str_interval);
	int interval = atoi(str_interval);
	if( interval>0 ) CTimeSyncServer::getInstance()->SetSyncInterval(interval);

	if( CTimeSyncServer::getInstance()->StartSync() )
	{
		m_editStatus.SetWindowText("동기화 시작");
		EnableControls(TRUE);
	}
	else
	{
		m_editStatus.SetWindowText("동기화 실패 !!!");
		EnableControls(FALSE);
	}
}

void CUDPTimeSyncServerDlg::OnBnClickedButtonStop()
{
	CTimeSyncServer::getInstance()->StopSync();
	m_editStatus.SetWindowText("중지");
	EnableControls(FALSE);
	//m_editSyncPort.SetReadOnly(FALSE);
	//m_editSyncInterval.SetReadOnly(FALSE);
}

LRESULT CUDPTimeSyncServerDlg::OnTimeSyncError(WPARAM wParam, LPARAM lParam)
{
	CTimeSyncServer::getInstance()->StopSync();
	m_editStatus.SetWindowText("동기화 실패 !!!");

	return TRUE;
}

LRESULT CUDPTimeSyncServerDlg::OnTimeSyncSend(WPARAM wParam, LPARAM lParam)
{
	CString title;
	title.Format("UDP Time Sync for Server - %s Send", CTime::GetCurrentTime().Format("%H:%M:%S") );

	SetWindowText(title);

	return TRUE;
}

void CUDPTimeSyncServerDlg::OnBnClickedButtonSaveSettings()
{
	//
	CString str_port;
	m_editSyncPort.GetWindowText(str_port);

	int port = atoi(str_port);
	if( port <= 0 )
	{
		::AfxMessageBox("Invalid port number !!!", MB_ICONSTOP);
		return;
	}

	//
	CString str_interval;
	m_editSyncInterval.GetWindowText(str_interval);

	int interval = atoi(str_interval);
	if( interval <= 0 )
	{
		::AfxMessageBox("Invalid interval value !!!", MB_ICONSTOP);
		return;
	}

	::WritePrivateProfileString("ROOT", "TIME_SYNC_PORT", str_port, ::GetUBCVariablesIniPath());
	::WritePrivateProfileString("ROOT", "TIME_SYNC_INTERVAL", str_interval, ::GetUBCVariablesIniPath());
}

void CUDPTimeSyncServerDlg::EnableControls(BOOL bStart)
{
	if( bStart )
	{
		m_editSyncPort.SetReadOnly(TRUE);
		m_editSyncInterval.SetReadOnly(TRUE);
		m_btnStart.EnableWindow(FALSE);
		m_btnStop.EnableWindow(TRUE);
		m_btnSaveSettings.EnableWindow(FALSE);
	}
	else
	{
		m_editSyncPort.SetReadOnly(FALSE);
		m_editSyncInterval.SetReadOnly(FALSE);
		m_btnStart.EnableWindow(TRUE);
		m_btnStop.EnableWindow(FALSE);
		m_btnSaveSettings.EnableWindow(TRUE);
	}
}
