// UDPTimeSyncServerDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"


// CUDPTimeSyncServerDlg 대화 상자
class CUDPTimeSyncServerDlg : public CDialog
{
// 생성입니다.
public:
	CUDPTimeSyncServerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UDPTIMESYNCSERVER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	CWinThread*	m_pThread;

	CTime	m_tmCurrent;
	CFont	m_font;

	void	EnableControls(BOOL bStart);

public:
	CEdit	m_editStatus;
	CEdit	m_editSyncPort;
	CEdit	m_editSyncInterval;
	CEdit	m_editCurrentTime;

	CButton	m_btnStart;
	CButton	m_btnStop;
	CButton	m_btnSaveSettings;

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonStop();
	afx_msg LRESULT OnTimeSyncError(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTimeSyncSend(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonSaveSettings();
};
