// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// UDPTimeSyncServer.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


LPCTSTR GetUBCVariablesIniPath()
{
	static CString	_iniDirectory = _T("");

	if(_iniDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_iniDirectory = str;
		_iniDirectory.Append(_T("data\\UBCVariables.ini"));
	}

	return _iniDirectory;
}
