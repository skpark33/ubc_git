#include "common/libHttpRequest/HttpRequest.h"
#include <ci/libBase/ciTime.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <util/libEncoding/Encoding.h>
#include "common/libCommon/ubcIni.h"
#include "contentsCheckTimer.h"
#include "contentsManagerTimer.h"
#include "ofsDownloaderWorld.h"


ciSET_DEBUG(9, "contentsCheckTimer");


contentsCheckTimer::contentsCheckTimer(	const char* site, 
										const char* host, 
										const char* ip, 
										ciLong port, 
										const char* id, 
										const char* pwd )
{
	ciDEBUG(5, ("contentsCheckTimer(%s,%s,%s,%d,%s,%s)", site, host, ip, port, id, pwd) );

	_brochConMngTimer = NULL;
	_goodsConMngTimer = NULL;
	_unionConMngTimer = NULL;
	_deleteConMngTimer = NULL;

	_initialize	= ciFalse;

	_site		= site;
	_host		= host;
	_svrIp		= ip;
	_svrPort	= port;
	_svrId		= id;
	_svrPwd		= pwd;

	// get last-sync-time
	ciString buf = "";
	ubcConfig aIni("UBCVariables");
	aIni.get("OFS_CMS_INFO","LastSyncTime", buf, "1");
	ciDEBUG(5, ("LastSyncTime(%s)", buf.c_str()) );

	time_t last_sync_time = _atoi64(buf.c_str());
	_lastSyncTime.set(last_sync_time);
	_lastestContentsTime.set(last_sync_time);

	// get cms-server-ip
	buf = "";
	aIni.get("OFS_CMS_INFO", "ServerIP", buf, "");
	ciDEBUG(5, ("ServerIP(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_cmsIp = buf;
	if( _cmsIp.length() == 0 )
	{
		_cmsIp = "127.0.0.1";
		aIni.set("OFS_CMS_INFO", "ServerIP", _cmsIp.c_str());
		ciDEBUG(5, ("new ServerIP(%s)", _cmsIp.c_str()) );
	}

	// get cms-server-port
	buf = "";
	aIni.get("OFS_CMS_INFO","ServerPort", buf, "");
	ciDEBUG(5, ("ServerPort(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_cmsPort = atoi(buf.c_str());
	if( _cmsPort == 0 )
	{
		_cmsPort = 8000;
		aIni.set("OFS_CMS_INFO", "ServerPort", _cmsPort);
		ciDEBUG(5, ("new ServerPort(%d)", _cmsPort) );
	}

	// get cms-contents-url
	buf = "";
	aIni.get("OFS_CMS_INFO", "ContentsUrl", buf, "");
	ciDEBUG(5, ("ContentsUrl(%s)", buf.c_str()) );
	ciStringUtil::trim(buf);
	_cmsUrl = buf;
	if( _cmsUrl.length() == 0 )
	{
		_cmsUrl = "api/contentDownloadList/Api013.do";
		aIni.set("OFS_CMS_INFO", "ContentsUrl", _cmsUrl.c_str());
		ciDEBUG(5, ("new ContentsUrl(%s)", _cmsUrl.c_str()) );
	}

	// init timer(s)
	_initTimer();

	ciDEBUG(5, ("contentsCheckTimer()"));
}

contentsCheckTimer::~contentsCheckTimer()
{
	ciDEBUG(5, ("~contentsCheckTimer()"));
}

extern ciString getAnsiXML(const char* xml);

void contentsCheckTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(5, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	// check init
	if( _initialize==ciFalse && counter>=6 ) // not init & over 1-min
	{
		// ==> init forcibly & set 5-min-interval
		_initialize = ciTrue;
		if( ciArgParser::getInstance()->isSet("+KHQS") )
			setInterval(60);
		else
			setInterval(300);
	}

	// get xml from api
	//char url[] = "api/contentDownloadList/Api013.do";
	char param[1024] = {0};
	sprintf(param, "siteId=%s&hostId=%s&lastUpdateTime=%I64d&returnType=XML", _site.c_str(), _host.c_str(), _lastSyncTime.getTime() );

	ciDEBUG(5, ("_cmsIp(%s), _cmsPort(%d), param(%s)", _cmsIp.c_str(), _cmsPort, param) );

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _cmsIp.c_str(), _cmsPort);
	if( ciArgParser::getInstance()->isSet("+HMCSFS") )
		http_request.AddAdditionalHeader(_getAdditionHttpHeader().c_str());

	//
	CString str_buf = "";
	if( !http_request.RequestPost(_cmsUrl.c_str(), param, str_buf) )
	{
		ciWARN( ("RequestPost FAIL !!! (ErrCode:%s", http_request.GetErrorMsg()) );
		return; // error, false
	}

	//
	ciString str_xml = getAnsiXML((LPCSTR)str_buf);//(LPCSTR)str_buf;
	ciDEBUG(5, ("RequestPost(%s)", str_xml.c_str()) );

	// get contents-list
	contentsInfoList contents_list;
	if( !contentsInfo::getContentsInfoList(contents_list, str_xml) )
	{
		ciWARN( ("getContentsInfoList FAIL !!!") );
		return; // error, false
	}

	// check contents-list and push to download-list
	_checkContentsFile(&contents_list);

	// clear list
	_clearContentsInfoList(&contents_list);

	// update last-sync-time
	_updateLastSyncTime();

	// check init
	if( _initialize == ciFalse)
	{
		_initialize = ciTrue;
		if( ciArgParser::getInstance()->isSet("+KHQS") )
			setInterval(60);
		else
			setInterval(300);
	}

	ciDEBUG(5, ("processExpired(%s) end", name.c_str()) );

	return;
}

void contentsCheckTimer::_initTimer()
{
	// brochure contents manager timer
	_brochConMngTimer = new contentsManagerTimer(_site.c_str());
	_brochConMngTimer->initTimer("brochure contents manager timer", 1); // every 1-sec
	_brochConMngTimer->initServerInfo(_svrIp.c_str(), _svrPort, _svrId.c_str(), _svrPwd.c_str());
	_brochConMngTimer->startTimer();

	// goods contents manager timer
	_goodsConMngTimer = new contentsManagerTimer(_site.c_str());
	_goodsConMngTimer->initTimer("goods contents manager timer", 1); // every 1-sec
	_goodsConMngTimer->initServerInfo(_svrIp.c_str(), _svrPort, _svrId.c_str(), _svrPwd.c_str());
	_goodsConMngTimer->startTimer();

	// goods contents manager timer
	_unionConMngTimer = NULL;

	// delete contents manager timer (use only in KHQS or HMCSFS)
	if( ciArgParser::getInstance()->isSet("+KHQS") ||
		ciArgParser::getInstance()->isSet("+HMCSFS") )
	{
		_deleteConMngTimer = new contentsManagerTimer(_site.c_str());
		_deleteConMngTimer->initTimer("delete contents manager timer", 1); // every 1-sec
		contentsInfo info; _deleteConMngTimer->pushbackToDeleteList(&info); // insert dummy
		_deleteConMngTimer->startTimer();
	}
}

void contentsCheckTimer::_checkContentsFile(contentsInfoList* infoList)
{
	contentsInfoList downloading_list;
	contentsInfoList deleting_list;

	contentsInfoListIter itr = infoList->begin();
	contentsInfoListIter end = infoList->end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* info = (*itr);

		contentsManagerTimer* timer = NULL;
		switch( info->contentsType )
		{
		default:
		case OFS_UNION_CONTENTS_TYPE:
			timer = _unionConMngTimer;
			break;

		case OFS_BROCHURE_CONTENTS_TYPE:
			timer = _brochConMngTimer;
			break;

		case OFS_GOODS_CONTENTS_TYPE:
			timer = _goodsConMngTimer;
			break;

		case OFS_DELETE_CONTENTS_TYPE:
			if( _deleteConMngTimer )
			{
				//if( _deleteConMngTimer->pushbackToDeleteList(info) )
				//{
				//	if( info->lastUpdateTime > _lastestContentsTime )
				//	{
				//		_lastestContentsTime = info->lastUpdateTime;
				//	}
				//}
				deleting_list.push_back( info );
			}
			break;
		}

		if( timer == NULL ) continue;
		downloading_list.push_back( info );

		// update _lastestContentsTime
		if( info->lastUpdateTime > _lastestContentsTime )
		{
			_lastestContentsTime = info->lastUpdateTime;
		}

		// get file-status
		ciBoolean find = ciFalse;
		struct _stati64 fs;
		if( _stati64(info->localPath.c_str(), &fs) == 0 )
			find = ciTrue;
		else
		{
			if( _stati64(info->tempPath.c_str(), &fs) == 0 )
				find = ciTrue;
		}

		if( find )
		{
			// open success
			if( info->volume == fs.st_size )
			{
				// same size => already exist => no download
				ciDEBUG(5, ("already exist (%s)", info->localPath.c_str()) );
				continue;
			}

			// different size => must download
			ciDEBUG(5, ("different size (%s)", info->localPath.c_str()) );
		}
		else
		{
			// open error => not exist => must download
			ciDEBUG(5, ("not exist (%s)", info->localPath.c_str()) );
		}

		// must download  =>  add to contentsManagerTimer
		timer->pushbackToDownloadList(info);
	}

	//
	itr = deleting_list.begin();
	end = deleting_list.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* del_info = (*itr);

		ciBoolean is_dup = ciFalse;
		contentsInfoListIter itr2 = downloading_list.begin();
		contentsInfoListIter end2 = downloading_list.end();
		for( ; itr2!=end2; itr2++)
		{
			contentsInfo* down_info = (*itr2);

			if( stricmp(down_info->location.c_str(), del_info->location.c_str()) == 0 )
			{
				is_dup = true;
				break;
			}
		}

		if( is_dup == ciFalse )
		{
			// not exist in down-list  ==>  add to del-list
			if( _deleteConMngTimer->pushbackToDeleteList(del_info) )
			{
				if( del_info->lastUpdateTime > _lastestContentsTime )
				{
					_lastestContentsTime = del_info->lastUpdateTime;
				}
			}
		}
	}
}

void contentsCheckTimer::_clearContentsInfoList(contentsInfoList* infoList)
{
	contentsInfo::clearContentsInfoList(*infoList);

	if( _brochConMngTimer ) _brochConMngTimer->clearCompleteInfoList();
	if( _goodsConMngTimer ) _goodsConMngTimer->clearCompleteInfoList();
	if( _unionConMngTimer ) _unionConMngTimer->clearCompleteInfoList();
}

ciBoolean contentsCheckTimer::_setLastUpdateTime(ciTime tm)
{
	//
	char buf[64];
	sprintf(buf, "%I64d", tm.getTime());

	ubcConfig aIni("UBCVariables");
	return aIni.set("OFS_CMS_INFO", "LastSyncTime", buf);
}

ciBoolean contentsCheckTimer::_updateLastSyncTime()
{
	if( _brochConMngTimer && _brochConMngTimer->getRemainingCount()>0 )
	{
		ciWARN(("BROCHURE-CONTENTS IS NOT COMPLETED TO DOWNLOAD !!! (cnt:%d)", _brochConMngTimer->getRemainingCount()));
		return ciFalse;
	}

	if( _goodsConMngTimer && _goodsConMngTimer->getRemainingCount()>0 )
	{
		ciWARN(("GOODS-CONTENTS IS NOT COMPLETED TO DOWNLOAD !!! (cnt:%d)", _goodsConMngTimer->getRemainingCount()));
		return ciFalse;
	}

	if( _unionConMngTimer && _unionConMngTimer->getRemainingCount()>0 )
	{
		ciWARN(("UNION-CONTENTS IS NOT COMPLETED TO DOWNLOAD !!! (cnt:%d)", _unionConMngTimer->getRemainingCount()));
		return ciFalse;
	}

	if( _deleteConMngTimer && _deleteConMngTimer->getRemainingCount()>0 )
	{
		ciWARN(("DELETE-CONTENTS IS NOT COMPLETED TO DELETE !!! (cnt:%d)", _deleteConMngTimer->getRemainingCount()));
		return ciFalse;
	}

	if( _setLastUpdateTime(_lastestContentsTime) )
	{
		_lastSyncTime = _lastestContentsTime;
		ciDEBUG(5, ("LastSyncTime is updated (time:%I64u)", _lastSyncTime.getTime()));
		return ciTrue;
	}
	ciWARN(("FAIL TO SET LastSyncTime !!! (time:%I64u)", _lastestContentsTime.getTime()));

	return ciFalse;
}

ciBoolean contentsCheckTimer::isDownloadingContents(const char* location)
{
	if( _brochConMngTimer == NULL ) return ciFalse;

	return _brochConMngTimer->isDownloadingContents(location);
}
