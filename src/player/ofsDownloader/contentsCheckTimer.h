#ifndef _contentsCheckTimer_H_
#define _contentsCheckTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>

//class contentsInfoList;
class contentsManagerTimer;
#include "contents.h"


class contentsCheckTimer	: public ciWinPollable
{
public:
	contentsCheckTimer(	const char* site, 
						const char* host, 
						const char* ip, 
						ciLong port, 
						const char* id, 
						const char* pwd );
	virtual ~contentsCheckTimer();

	virtual void processExpired(ciString name, int counter, int interval);

	ciBoolean	isInitialized() { return _initialize; };
	ciBoolean	isDownloadingContents(const char* location);

protected:
	ciBoolean	_initialize;

	ciString	_site;
	ciString	_host;

	ciString	_svrIp;
	ciLong		_svrPort;
	ciString	_svrId;
	ciString	_svrPwd;

	ciString	_cmsIp;
	ciLong		_cmsPort;
	ciString	_cmsUrl;

	ciTime		_lastSyncTime;
	ciTime		_lastestContentsTime;

	contentsManagerTimer*	_brochConMngTimer;
	contentsManagerTimer*	_goodsConMngTimer;
	contentsManagerTimer*	_unionConMngTimer;
	contentsManagerTimer*	_deleteConMngTimer;

	void		_initTimer();
	void		_checkContentsFile(contentsInfoList* infoList);
	void		_clearContentsInfoList(contentsInfoList* infoList);
	ciBoolean	_setLastUpdateTime(ciTime tm);
	ciBoolean	_updateLastSyncTime();
};

#endif //_contentsCheckTimer_H_
