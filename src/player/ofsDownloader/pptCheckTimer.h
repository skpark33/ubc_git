#ifndef _pptCheckTimer_H_
#define _pptCheckTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>


class pptCheckTimer : public ciWinPollable
{
public:
	pptCheckTimer();
	virtual	~pptCheckTimer();

	virtual	void	processExpired(ciString name, int counter, int interval);

};

#endif //_pptCheckTimer_H_
