#include "common/libHttpRequest/HttpRequest.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciTime.h>
#include "contentsDownloadTimer.h"
#include <Dbghelp.h>

ciSET_DEBUG(9, "contentsDownloadTimer");


contentsDownloadTimer::contentsDownloadTimer(const char* site) 
{
	ciDEBUG(5, ("contentsDownloadTimer(%s)", site));

	_status = DS_INIT;
	_isInfoExist = ciFalse;
	_downloadedSize = 0;
	_site = site;
}

contentsDownloadTimer::~contentsDownloadTimer()
{
	ciDEBUG(5, ("~contentsDownloadTimer()"));
}

void contentsDownloadTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(5, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	if( _status == DS_INIT || _status == DS_LOGINING )
	{
		_status = DS_LOGINING;

		if( !m_objFileSvcClient.Login(_id.c_str(), _pwd.c_str()) )
		{
			// login failed !!!
			ciWARN( ("LOGIN FAIL !!!(%s,%s)", _id.c_str(), _pwd.c_str()) );
			return;
		}

		_status = DS_READY;
		ciDEBUG(5,  ("login succeed") );
	}

	// complete(or error)  ==>  skip
	if( _status == DS_COMPLETED || _status == DS_ERROR )
	{
		ciDEBUG(5, ("do nothing(status:%d)", _status) );
		return;
	}

	// info isn't exist  ==>  skip
	{
		ciGuard aGuard(_lock);
		if( _isInfoExist == ciFalse )
		{
			if( counter % 60 == 0 )
			{
				ciDEBUG(5, ("no download-contents") );
			}
			return;
		}
	}

	// ready  ==>  start download
	ciDEBUG(5, ("start download (%s)", _info.toString(ciFalse).c_str()) );
	_status = DS_DOWNLOADING;
	_downloadedSize = 0;

	BOOL result = FALSE;
	try
	{
		// check file(_info.tempPath) size  ==> same size ==> not download
		//									==> different size ==> download

		// get file-status
		struct _stati64 fs;
		if( _stati64(_info.tempPath.c_str(), &fs) == 0 )
		{
			// open success
			if( _info.volume == fs.st_size )
			{
				// same size => already exist => no download
				ciDEBUG(5, ("already exist, skip download (%s)", _info.tempPath.c_str()) );
				result = TRUE;
			}
			else
			{
				// different size => must download
				ciDEBUG(5, ("different size, download (%s)", _info.tempPath.c_str()) );
			}
		}
		else
		{
			// open error => not exist => must download
			ciDEBUG(5, ("not exist, download (%s)", _info.tempPath.c_str()) );
		}

		if( result == FALSE )
		{
			ciDEBUG(5, ("download (%s) (%s)", _info.location.c_str(), _info.tempPath.c_str()) );
			result = m_objFileSvcClient.GetFile(_info.location.c_str(), _info.tempPath.c_str(), 0, (IProgressHandler*)this);
		}
	}
	catch (CString& e)
	{
		result = FALSE;
		ciWARN( ("EXCEPTION !!! (%s) (%s)", (LPCSTR)e, _info.toString(ciFalse).c_str()) );
	}
	catch(...)
	{
		result = FALSE;
		ciWARN( ("EXCEPTION !!! (%s)", _info.toString(ciFalse).c_str()) );
	}

	while( result )
	{
		// delete bak
		ciDEBUG(5, ("delete bak (%s)", _info.bakPath.c_str()) );
		::DeleteFile(_info.bakPath.c_str());

		// move org to bak
		ciDEBUG(5, ("move org to bak (%s) -> (%s)", _info.localPath.c_str(), _info.bakPath.c_str()) );
		BOOL move_org_to_bak = ::MoveFile(_info.localPath.c_str(), _info.bakPath.c_str());

		// move tmp to org
		ciDEBUG(5, ("move tmp to org (%s) -> (%s)", _info.tempPath.c_str(), _info.localPath.c_str()) );
		createFullDirectory(_info.localPath.c_str());
		result = ::MoveFile(_info.tempPath.c_str(), _info.localPath.c_str());
		if( result == FALSE )
		{
			ciDEBUG(5, ("move tmp to org FAIL !!! (%s) -> (%s)", _info.tempPath.c_str(), _info.localPath.c_str()) );
			if( move_org_to_bak )
			{
				ciDEBUG(5, ("move org to tmp (%s) -> (%s)", _info.bakPath.c_str(), _info.localPath.c_str()) );
				::MoveFile(_info.bakPath.c_str(), _info.localPath.c_str());
			}
			break;
		}

		// all success => delete bak
		ciDEBUG(5, ("all success, delete bak (%s)", _info.bakPath.c_str()) );
		::DeleteFile(_info.bakPath.c_str());

		break;
	}

	_status = ( result==TRUE ? DS_COMPLETED : DS_ERROR );
	ciDEBUG(5, ("new status (%d) (%s)", _status, m_objFileSvcClient.GetErrorMsg()) );

	return;
}

void contentsDownloadTimer::initServerInfo(const char* ip, ciLong port, const char* id, const char* pwd)
{
	ciDEBUG(5, ("initServerInfo(%s,%d,%s,%s)", ip, port, id, pwd) );

	_ip		= ip;
	_port	= port;
	_id		= id;
	_pwd	= pwd;

	m_objFileSvcClient.SetHttpServerInfo(_ip.c_str(), _port);
}

void contentsDownloadTimer::ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath)
{
/*
	enum FS_PROGRESS_FLAG
	{
		FS_START = 0,
		FS_DOING,
		FS_STOP,
		FS_COMPLETE,
		FS_ERROR
	};
*/

	ciDEBUG(5, ("ProgressEvent(%s)(%I64d)", _info.filename.c_str(), uSendBytes) );

	_downloadedSize = uSendBytes;
/*

	//FS_START = 0,
	//FS_DOING,
	//FS_STOP,
	//FS_COMPLETE,
	//FS_ERROR

	if(nFlag == CFileServiceWrap::FS_DOING && m_ulNewFileSize > 0)
	{
		int progress = (uSendBytes * 100) / m_ulNewFileSize;

		m_pc.SetPos(progress);

		CString str_progress;
		str_progress.Format("%d %%", progress);

		m_stcProgress.SetWindowText(str_progress);

		m_editNewFileSize.SetWindowText( ::ToMoneyTypeString(m_ulNewFileSize) );
	}

	TraceLog(("ProgressEvent::OnBytesReceived end"));
*/
}

void contentsDownloadTimer::setContentsInfo(contentsInfo* info)
{
	ciGuard aGuard(_lock);

	_status = DS_READY;

	if( info == NULL )
	{
		ciDEBUG(5, ("setContentsInfo(NULL)") );
		_isInfoExist = ciFalse;
		_info.init();
		return;
	}

	ciDEBUG(5, ("setContentsInfo(%s)", info->toString(ciFalse).c_str()) );
	_info = *info;
	_isInfoExist = ciTrue;
}

ciBoolean createFullDirectory(const char* path, ciBoolean isFilenameIncluded)
{
	BOOL ret = TRUE;
	if( isFilenameIncluded )
	{
		char drv[MAX_PATH], dir[MAX_PATH], fname[MAX_PATH], ext[MAX_PATH];
		_splitpath(path, drv, dir, fname, ext);

		sprintf(fname, "%s%s", drv, dir);
		ret = ::MakeSureDirectoryPathExists(fname);
	}
	else
	{
		ret = ::MakeSureDirectoryPathExists(path);
	}

	if( ret ) return TRUE;
	return FALSE;
}
