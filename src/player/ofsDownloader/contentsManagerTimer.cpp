#include "common/libHttpRequest/HttpRequest.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciTime.h>
#include "contentsCheckTimer.h"
#include "contentsManagerTimer.h"
#include "contentsDownloadTimer.h"


ciSET_DEBUG(9, "contentsManagerTimer");


contentsManagerTimer::contentsManagerTimer(const char* site) 
{
	ciDEBUG(5, ("contentsManagerTimer(%s)", site));

	_site = site;
	_contentsDownloadTimer = NULL;
}

contentsManagerTimer::~contentsManagerTimer()
{
	ciDEBUG(5, ("~contentsManagerTimer()"));
}

void contentsManagerTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(5, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	if( _deleteingList.size() > 0 )
	{
		_DeleteFileInList();
		return;
	}

	// _contentsDownloadTimer is null  ==>  create new one & return
	if( _contentsDownloadTimer == NULL )
	{
		ciDEBUG(5, ("create contentsDownloadTimer"));

		ciString timer_name = name;
		ciStringUtil::stringReplace(timer_name, "manager", "download");

		_contentsDownloadTimer = new contentsDownloadTimer(_site.c_str());
		_contentsDownloadTimer->initTimer(timer_name.c_str(), 1); // every 1-sec
		_contentsDownloadTimer->initServerInfo(_ip.c_str(), _port, _id.c_str(), _pwd.c_str());
		_contentsDownloadTimer->startTimer();
		return;
	}

	contentsDownloadTimer::DOWNLOAD_STATUS status = _contentsDownloadTimer->getStatus();
	// _contentsDownloadTimer is DS_INIT  ==>  do nothing & return
	if( contentsDownloadTimer::DS_INIT == status )
	{
		ciDEBUG(5, ("do nothing (status:%d)", status));
		return;
	}

	// _contentsDownloadTimer is DS_LOGINING  ==>  do nothing & return
	if( contentsDownloadTimer::DS_LOGINING == status )
	{
		ciDEBUG(5, ("do nothing (status:%d)", status));
		return;
	}

	// _contentsDownloadTimer is DS_READY
	//		_downloadList is null  ==>  do nothing & return
	//		_downloadList is exist  ==>  get header from _downloadList  ==>  push to _contentsDownloadTimer
	if( contentsDownloadTimer::DS_READY == status )
	{
		ciGuard aGuard(_lock);

		// get header from _downloadList 
		contentsInfo* info = _getHeader();

		// _downloadList is null  ==>  do nothing & return
		if( info == NULL )
		{
			if( counter % 60 == 0 )
			{
				ciDEBUG(5, ("nothing to download") );
			}
			return;
		}
		ciDEBUG(5, ("setContentsInfo(%s)", info->toString(ciFalse).c_str()) );

		// _downloadList is exist  ==>  push to _contentsDownloadTimer
		_contentsDownloadTimer->setContentsInfo(info);

		return;
	}

	// _contentsDownloadTimer is DS_DOWNLOADING
	//		getDownloadedSize is equal over 1-min  ==>  delete _contentsDownloadTimer & re-queue header
	if( contentsDownloadTimer::DS_DOWNLOADING == status )
	{
		static ULONGLONG prev_downloaded_size = 0;
		static ciString prev_download_path = "";
		static int holding_count = 0;

		ciGuard aGuard(_lock);

		if( prev_download_path != _contentsDownloadTimer->getLocalPath() ||
			prev_downloaded_size != _contentsDownloadTimer->getDownloadedSize() )
		{
			holding_count = 0;
			ciDEBUG(5, ("reset holding-count (%s)", prev_download_path.c_str()) );
		}
		else
		{
			// equal size & path
			holding_count++;
			if( holding_count <= 60 )
			{
				ciDEBUG(5, ("download holding (%d,%I64d,%s)", holding_count, prev_downloaded_size, prev_download_path.c_str()) );
				return;
			}

			ciWARN( ("OVER HOLDING-COUNT (%s)", prev_download_path.c_str()) );

			// equal status over 1-min
			delete _contentsDownloadTimer;
			_contentsDownloadTimer = NULL;

			// re-queue header
			ciDEBUG(5, ("_requeueHeader(%s)", prev_download_path.c_str()) );
			_requeueHeader();
		}

		return;
	}

	// _contentsDownloadTimer is DS_COMPLETED
	//		pop header from _downloadList & set null-info to _contentsDownloadTimer
	if( contentsDownloadTimer::DS_COMPLETED == status )
	{
		ciGuard aGuard(_lock);

		contentsInfo* info = _getHeader();
		if( info == NULL )
		{
			ciERROR( ("INVALID PROCESS STATUS !!!") );
			return;
		}

		// pop header
		ciDEBUG(5, ("complete, pop-header(%s)", info->toString(ciFalse).c_str()) );
		_popHeader();

		// set null-info
		ciDEBUG(5, ("set null-contents") );
		_contentsDownloadTimer->setContentsInfo(NULL);

		return;
	}

	// _contentsDownloadTimer is DS_ERROR
	//		re-queue header & set null-info to _contentsDownloadTimer
	if( contentsDownloadTimer::DS_ERROR == status )
	{
		ciGuard aGuard(_lock);

		contentsInfo* info = _getHeader();
		if( info == NULL )
		{
			ciERROR( ("INVALID PROCESS STATUS !!!") );
			return;
		}

		// re-queue header
		ciWARN( ("DOWNLOAD-ERROR, requeue-header(%s)", info->toString(ciFalse).c_str()) );
		_requeueHeader();

		// set null-info
		ciDEBUG(5, ("set null-contents") );
		_contentsDownloadTimer->setContentsInfo(NULL);

		return;
	}


	//
	// invalid status
	//
	ciWARN( ("INVALID STATUS !!!") );

	return;
}

void contentsManagerTimer::initServerInfo(const char* ip, ciLong port, const char* id, const char* pwd)
{
	ciDEBUG(5, ("initServerInfo(%s,%d,%s,%s)", ip, port, id, pwd) );

	_ip		= ip;
	_port	= port;
	_id		= id;
	_pwd	= pwd;
}

contentsInfo* contentsManagerTimer::_getHeader()
{
	contentsInfoListIter itr = _downloadList.begin();
	contentsInfoListIter end = _downloadList.end();
	if( itr == end ) return NULL;

	contentsInfo* tmp = (*itr);
	return tmp;
}

ciBoolean contentsManagerTimer::_popHeader()
{
	contentsInfoListIter itr = _downloadList.begin();
	contentsInfoListIter end = _downloadList.end();
	if( itr == end ) return ciFalse;

	contentsInfo* tmp = (*itr);

	_downloadList.pop_front();

	ciDEBUG(5, ("addToCompleteList (%s)", tmp->toString(ciFalse).c_str()) );
	_completeList.push_front( tmp );

	return ciTrue;
}

ciBoolean contentsManagerTimer::_requeueHeader()
{
	contentsInfoListIter itr = _downloadList.begin();
	contentsInfoListIter end = _downloadList.end();
	if( itr == end ) return ciFalse;
	contentsInfo* tmp = (*itr);

	_downloadList.pop_front();

	_downloadList.push_back( tmp );

	return ciTrue;
}


ciBoolean contentsManagerTimer::pushbackToDownloadList(contentsInfo* info)
{
	if( info == NULL ) return ciFalse;

	ciGuard aGuard(_lock);

	// check already-download
	contentsInfoListIter itr = _completeList.begin();
	contentsInfoListIter end = _completeList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->isEqual(info) )
		{
			// already exist in complete-list => pass
			ciDEBUG(5, ("already exist in complete-list (%s)", info->toString(ciFalse).c_str()) );
			return ciFalse;
		}
	}

	// check duplicate
	itr = _downloadList.begin();
	end = _downloadList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->isEqual(info) )
		{
			// exist duplicate => pass
			ciDEBUG(5, ("already exist in downloading-list (%s)", info->toString(ciFalse).c_str()) );
			return ciFalse;
		}
	}

	// add clone to download-list
	ciDEBUG(5, ("pushbackToDownloadList (%s)", info->toString(ciFalse).c_str()) );
	_downloadList.push_front( info->duplicate() );

	return ciTrue;
}

ciBoolean contentsManagerTimer::pushbackToDeleteList(contentsInfo* info)
{
	if( info == NULL ) return ciFalse;

	ciGuard aGuard(_lock);

	// check already-delete
	contentsInfoListIter itr = _completeList.begin();
	contentsInfoListIter end = _completeList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->isEqual(info) )
		{
			// already exist in complete-list => pass
			ciDEBUG(5, ("already exist in complete-list (%s)", info->toString(ciFalse).c_str()) );
			return ciFalse;
		}
	}

	// check duplicate
	itr = _deleteingList.begin();
	end = _deleteingList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->isEqual(info) )
		{
			// exist duplicate => pass
			ciDEBUG(5, ("already exist in deleting-list (%s)", info->toString(ciFalse).c_str()) );
			return ciFalse;
		}
	}

	// add clone to download-list
	ciDEBUG(5, ("pushbackToDeleteList (%s)", info->toString(ciFalse).c_str()) );
	_deleteingList.push_front( info->duplicate() );

	return ciTrue;
}

ciBoolean contentsManagerTimer::clearCompleteInfoList()
{
	ciGuard aGuard(_lock);

	// check duplicate
	contentsInfoListIter itr = _completeList.begin();
	contentsInfoListIter end = _completeList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		delete tmp;
	}

	_completeList.clear();

	return ciTrue;
}

ciLong contentsManagerTimer::getRemainingCount()
{
	ciGuard aGuard(_lock);

	//
	int count = _deleteingList.size();
	if( count > 0 ) count--; // remove dummy

	//
	count += _downloadList.size();

	return count;
}

ciBoolean contentsManagerTimer::isDownloadingContents(const char* location)
{
	if( location == NULL ) return ciFalse;

	//
	// only use in brochure-contents-manager
	//

	ciGuard aGuard(_lock);

	ciString str_location = "/";
	str_location += location;
	ciStringUtil::stringReplace(str_location, "//", "/");

	// check downloading-list
	contentsInfoListIter itr = _downloadList.begin();
	contentsInfoListIter end = _downloadList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( stricmp(tmp->location.c_str(), str_location.c_str()) == 0 )
		{
			// exist in downloading-list
			return ciTrue;
		}
	}

	return ciFalse;
}

ciBoolean contentsManagerTimer::isDeletingContents(const char* filename, ULONGLONG filesize)
{
	if( filename == NULL ) return ciFalse;

	ciString str_filename_lower = filename;
	ciStringUtil::toLower(str_filename_lower);

	//
	// only use in delete-contents-manager
	//

	ciGuard aGuard(_lock);

	// check downloading-list
	contentsInfoListIter itr = _deleteingList.begin();
	contentsInfoListIter end = _deleteingList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);

		ciString str_location_lower = tmp->location;
		ciStringUtil::toLower(str_location_lower);

		if( str_location_lower.length() == 0 &&
			strstr(str_location_lower.c_str(), str_filename_lower.c_str()) == NULL )
		{
			// different => continue
			continue;
		}

		if( tmp->volume != filesize )
		{
			// different => continue
			continue;
		}

		// find same-file
		return ciTrue;
	}

	// not find same-file
	return ciFalse;
}

ciBoolean contentsManagerTimer::_DeleteFileInList()
{
	ciGuard aGuard(_lock);

	if( _deleteingList.size() == 0 ) return ciTrue;

	contentsInfoList not_del_list;

	// check downloading-list
	contentsInfoListIter itr = _deleteingList.begin();
	contentsInfoListIter end = _deleteingList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->location.length() == 0 )
		{
			// skip dummy
			not_del_list.push_back(tmp);
			continue;
		}

		if( ::access(tmp->localPath.c_str(), 00) != 0 )
		{
			// deleting-file is not exist
			ciDEBUG(5, ("deleting-file is not exist (%s)", tmp->localPath.c_str()));
			//delete tmp;
			_completeList.push_back(tmp);
			continue;
		}
		if( ::DeleteFile(tmp->localPath.c_str()) )
		{
			// success to delete ==> delete info
			ciDEBUG(5, ("success to delete (%s)", tmp->localPath.c_str()));
			//delete tmp;
			_completeList.push_back(tmp);
			continue;
		}

		// fail to delete
		ciWARN(("FAIL TO DELETE !!! (ErrCode:%u) (%s)", ::GetLastError(), tmp->localPath.c_str()));
		not_del_list.push_back(tmp);
	}
	_deleteingList.clear();

	//
	itr = not_del_list.begin();
	end = not_del_list.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		_deleteingList.push_back(tmp);
	}

	return ciTrue;
}
