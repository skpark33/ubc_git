 /*! \file ofsDownloaderWorld.h
 *  Copyright ⓒ 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _ofsDownloaderWorld_h_
#define _ofsDownloaderWorld_h_

#include <afxwin.h>
#include <afxcmn.h>

#include <cci/libWorld/cciORBWorld.h> 
#include "ofsSocketSession.h"
#include "common/libCommon/ubcHost.h"

class ubcHostView;
class brochureCheckTimer;
class contentsCheckTimer;
class contentsManagerTimer;
class pptCheckTimer;

class ofsDownloaderWorld : public cciORBWorld
{
public:
	ofsDownloaderWorld();
	~ofsDownloaderWorld();

	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

protected:
	ofsSocketSession*			_session;

	brochureCheckTimer*			_brochureCheckTimer;
	contentsCheckTimer*			_contentsCheckTimer;
	contentsManagerTimer*		_contentsManagerTimer;
	pptCheckTimer*				_pptCheckTimer;


	void		_releaseMutexLock();

	ciBoolean	_getSiteId(const char* host, ciString& brand, ciString& site, ubcHostView* pView);

	int			_cleanupDirectory(const char* szDir, int recur); // recur=0 : szDir하위만 삭제(szDir자체는 보존)
																// recur=1 : szDir자체까지 포함하여 하위전체삭제
};

ciString _getAdditionHttpHeader();

#endif //_ofsDownloaderWorld_h_
