#ifndef _contentsManagerTimer_H_
#define _contentsManagerTimer_H_

//#include "afxwin.h"
//#include "afxcmn.h"
#include "common/libFileServiceWrap/FileServiceWrapDll.h"
#include "contents.h"

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>


class contentsDownloadTimer;


class contentsManagerTimer : public ciWinPollable
{
public:
	contentsManagerTimer(const char* site);
	virtual	~contentsManagerTimer();

	virtual	void	processExpired(ciString name, int counter, int interval);

	void		initServerInfo(const char* ip, ciLong port, const char* id, const char* pwd);

	ciBoolean	pushbackToDownloadList(contentsInfo* info);
	ciBoolean	pushbackToDeleteList(contentsInfo* info);
	ciBoolean	clearCompleteInfoList();
	ciLong		getRemainingCount();

	ciBoolean	isDownloadingContents(const char* location);
	ciBoolean	isDeletingContents(const char* filename, ULONGLONG filesize);

protected:

	ciString	_site;

	ciString	_ip;
	ciLong		_port;
	ciString	_id;
	ciString	_pwd;

	contentsInfoList	_downloadList;
	contentsInfoList	_completeList;
	contentsInfoList	_deleteingList;
	ciMutex				_lock;

	contentsDownloadTimer*	_contentsDownloadTimer;

	contentsInfo*	_getHeader();
	ciBoolean		_popHeader();
	ciBoolean		_requeueHeader();

	ciBoolean		_DeleteFileInList();
};

#endif //_contentsManagerTimer_H_
