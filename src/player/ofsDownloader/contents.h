#ifndef _contentsInfo_h_
#define _contentsInfo_h_
/*
#define		UNIONCONTENTS_STARTTAG		"<downFile>"
#define		UNIONCONTENTS_ENDTAG		"</downFile>"

#define		BROCHURECONTENTS_STARTTAG	"<brochureContentsVo>"
#define		BROCHURECONTENTS_ENDTAG		"</brochureContentsVo>"

#define		GOODSCONTENTS_STARTTAG		"<goodsContentsVo>"
#define		GOODSCONTENTS_ENDTAG		"</goodsContentsVo>"
*/
#define		CONTENTS_STARTTAG		"<downFile>"
#define		CONTENTS_ENDTAG			"</downFile>"

#define		CONTENTSTYPE_STARTTAG	"<contentType>"
#define		CONTENTSTYPE_ENDTAG		"</contentType>"

#define		BROCHURE_CONTENTS_TYPE	"brochure"
#define		GOODS_CONTENTS_TYPE		"goods"
#define		DELETE_CONTENTS_TYPE	"delete"

#define		MGRID_STARTTAG			"<mgrId>"
#define		MGRID_ENDTAG			"</mgrId>"
#define		SITEID_STARTTAG			"<siteId>"
#define		SITEID_ENDTAG			"</siteId>"
#define		BROCHUREID_STARTTAG		"<brochureId>"
#define		BROCHUREID_ENDTAG		"</brochureId>"
#define		GOODSID_STARTTAG		"<goodsId>"
#define		GOODSID_ENDTAG			"</goodsId>"
#define		PAGEID_STARTTAG			"<pageId>"
#define		PAGEID_ENDTAG			"</pageId>"
#define		FRAMEID_STARTTAG		"<frameId>"
#define		FRAMEID_ENDTAG			"</frameId>"
#define		CONTENTSID_STARTTAG		"<contentsId>"
#define		CONTENTSID_ENDTAG		"</contentsId>"
#define		BROCHURETYPE_STARTTAG	"<brochureType>"
#define		BROCHURETYPE_ENDTAG		"</brochureType>"
#define		MEDIATYPE_STARTTAG		"<mediaType>"
#define		MEDIATYPE_ENDTAG		"</mediaType>"
#define		FILENAME_STARTTAG		"<filename>"
#define		FILENAME_ENDTAG			"</filename>"
#define		LOCATION_STARTTAG		"<location>"
#define		LOCATION_ENDTAG			"</location>"
#define		VOLUME_STARTTAG			"<volume>"
#define		VOLUME_ENDTAG			"</volume>"
#define		ZORDER_STARTTAG			"<zorder>"
#define		ZORDER_ENDTAG			"</zorder>"
#define		RUNNINGTIME_STARTTAG	"<runningTime>"
#define		RUNNINGTIME_ENDTAG		"</runningTime>"
#define		PARENTID_STARTTAG		"<parentId>"
#define		PARENTID_ENDTAG			"</parentId>"
#define		USAGETYPE_STARTTAG		"<usageType>"
#define		USAGETYPE_ENDTAG		"</usageType>"
#define		CREATORID_STARTTAG		"<creatorId>"
#define		CREATORID_ENDTAG		"</creatorId>"
#define		LASTUPDATETIME_STARTTAG	"<lastUpdateTime>"
#define		LASTUPDATETIME_ENDTAG	"</lastUpdateTime>"
#define		DESCRIPTION_STARTTAG	"<description>"
#define		DESCRIPTION_ENDTAG		"</description>"
#define		FILENAME_ORI_STARTTAG	"<filenameOri>"
#define		FILENAME_ORI_ENDTAG		"</filenameOri>"
#define		TIMEFLAG_STARTTAG		"<timeFlag>"
#define		TIMEFLAG_ENDTAG			"</timeFlag>"
#define		ULASTUPDATETIME_STARTTAG	"<uLastUpdateTime>"
#define		ULASTUPDATETIME_ENDTAG		"</uLastUpdateTime>"


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
enum OFS_CONTENTS_TYPE {
	OFS_UNION_CONTENTS_TYPE = 0,
	OFS_BROCHURE_CONTENTS_TYPE,
	OFS_GOODS_CONTENTS_TYPE,
	OFS_DELETE_CONTENTS_TYPE,
};
/*
enum OFS_BROCHURE_TYPE {
	OFS_BROCHURE_UNKNOWN = 0,
	OFS_BROCHURE_SMARTWINDOW,
	OFS_BROCHURE_SMARTMIRROR,
	OFS_BROCHURE_SMARTHANGER,
	OFS_BROCHURE_SMARTSHOWCASE,
	OFS_BROCHURE_SMARTSHELF,
	OFS_BROCHURE_ETC,
};

enum OFS_MEDIA_TYPE {
	OFS_MEDIA_UNKNOWN = -1,
	OFS_MEDIA_VIDEO = 0,		// 비디오 (0)
	OFS_MEDIA_IMAGE,			// 이미지 (1)
	OFS_MEDIA_URL,				// 웹주소 (2)
	OFS_MEDIA_FLASH,			// 플래시 (3)
	OFS_MEDIA_ETC = 99,			// 기타 (99)
};

enum OFS_USAGE_TYPE {
	OFS_USAGE_UNKNOWN = 0,
	OFS_USAGE_CF_VIDEO,			// 연관CF동영상 (1)
	OFS_USAGE_REPRESENTATIVE,	// 상품대표이미지 (2)
	OFS_USAGE_FRONT,			// 정면이미지 (3)
	OFS_USAGE_SIDE,				// 측면이미지 (4)
	OFS_USAGE_RIGHTSIDE,		// 우 측면 이미지 (5)
	OFS_USAGE_LEFTSIDE,			// 좌 측면 이미지 (6)
	OFS_USAGE_BACK,				// 후면이미지 (7)
	OFS_USAGE_WEAR,				// 착용샷 (8)
	OFS_USAGE_THUMBNAIL,		// 썸네일 (9)
	OFS_USAGE_COLORSAMPLE,		// 색상 샘플 (10)
	OFS_USAGE_BLUEPRINT,		// 사이즈도면 (11)
	OFS_USAGE_ETC = 99,			// 기타 (99)
};
*/

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class contentsInfo;
typedef		list<contentsInfo*>			contentsInfoList;
typedef		contentsInfoList::iterator	contentsInfoListIter;


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
ciString	_getItem(const char* xml, const char* starttag, const char* endtag);
ciBoolean	_findTag(const char* xml, const char* tag);


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class contentsInfo
{
public:
	static	ciBoolean		getContentsInfoList(contentsInfoList& infoList, ciString& strXML);
	static	void			clearContentsInfoList(contentsInfoList& infoList);
	static	contentsInfo*	getContentsInfoObject(const char* xml, OFS_CONTENTS_TYPE type=OFS_UNION_CONTENTS_TYPE);

protected:
//	static ciString		_getItem(const char* xml, const char* starttag, const char* endtag);
//	static ciBoolean	_findTag(const char* xml, const char* tag);


/////////////////////////////////////////////////////////////////
public:
	contentsInfo();
	virtual ~contentsInfo() {};

	// class attributes
	OFS_CONTENTS_TYPE	contentsType;

	// contents attributes
	ciString		mediaType;		//
	ciString		filename;		//콘텐츠파일명/Area 명
	ciString		location;		//서버파일 경로
	ULONGLONG		volume;			//file size
	ciTime			lastUpdateTime;

	ciString		localPath;		//로컬 전체경로 (C:\SQISoft\ENC\...filename)
	ciString		tempPath;		//로컬임시다운로드 전체경로 (C:\SQISoft\Temp\...filename)
	ciString		bakPath;		//로컬백업 전체경로 (C:\SQISoft\ENC\...filename.bak)

	ciString		filenameOri;	//원본 콘텐츠파일명
	ciString		timeFlag;		//배포시간 제한

	//
	virtual void			init();

	virtual ciBoolean		fromString(const char* xml);
	virtual ciString		toString(ciBoolean xml_type=ciTrue);

	virtual contentsInfo*	duplicate();
	virtual ciBoolean		isEqual(contentsInfo* info);

private:
	ciBoolean	_getLocalPath(const char* findStr, const char* replaceStr, const char* path, time_t tm);
};

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class brochureContentsInfo : public contentsInfo
{
public:
	brochureContentsInfo();
	virtual ~brochureContentsInfo() {};

	// brochure-contents attributes
	ciString		mgrId;			//서버아이디
	ciString		siteId;			//브랜드 ID
	ciString		brochureId;		//브로셔 ID
	ciString		pageId;			//page ID
	ciString		frameId;		//frame ID
	ciString		contentsId;		//Contents Id
	ciString		brochureType;	//
	int				zorder;			//화면이 나올 순서
	int				runningTime;	//플레이시간(초)
	ciString		creatorId;		//등록자Id
	ciString		description;	//설명

	//
	virtual void	init();

	virtual ciBoolean		fromString(const char* xml);
	virtual ciString		toString(ciBoolean xml_type=ciTrue);

	virtual contentsInfo*	duplicate();
	virtual ciBoolean		isEqual(contentsInfo* info);

};


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class goodsContentsInfo : public contentsInfo
{
public:
	goodsContentsInfo();
	virtual ~goodsContentsInfo() {};

	// goods-contents attributes
	ciString		mgrId;			//서버아이디
	ciString		siteId;			//브랜드 ID
	ciString		goodsId;
	ciString		contentsId;		//Contents Id
	ciString		filename;		//콘텐츠파일명/Area 명
	ciString		location;		//파일 경로
	ULONGLONG		volume;			//file size
	ciString		parentId;
	ciString		usageType;
	ciString		creatorId;		//등록자Id
	ciString		description;	//설명

	//
	virtual void	init();

	virtual ciBoolean		fromString(const char* xml);
	virtual ciString		toString(ciBoolean xml_type=ciTrue);

	virtual contentsInfo*	duplicate();
	virtual ciBoolean		isEqual(contentsInfo* info);

};

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
class autoDeleter
{
public:
	autoDeleter(char* p) { _init(); _psz = p; };
	autoDeleter(contentsInfo* info) { _init(); _info = info; };
	~autoDeleter() { _clear(); };

protected:
	char* _psz;
	contentsInfo* _info;

	void _init()
	{
		_psz = NULL;
		_info = NULL;
	};

	void _clear()
	{
		if( _psz != NULL ) delete _psz;
		if( _info != NULL ) delete _info;
	};
};

#define	AUTO_DELETE(var)		autoDeleter __deleter_##var (var);

#endif // _contentsInfo_h_