#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <cci/libValue/ccistringlist.h>
#include "contents.h"


#define		SQI_ENC_DIRECTORY		"C:\\SQISoft\\Contents\\ENC\\"
#define		SQI_TMP_DIRECTORY		"C:\\SQISoft\\Contents\\Temp\\"


ciSET_DEBUG(9, "contentsInfo");


//ciString contentsInfo::_getItem(const char* xml, const char* starttag, const char* endtag)
ciString _getItem(const char* xml, const char* starttag, const char* endtag)
{
	char* start = (char*)strstr(xml, starttag);
	if( start == NULL ) return "";

	start += strlen(starttag);

	char* end = strstr(start, endtag);
	if( end == NULL ) return "";

	int size = end-start;
	if( size < 0 )
	{
		ciERROR( ("NEGATIVE SIZE !!!") );
		return "";
	}
	else if( size >= 1024 )
	{
		ciWARN( ("BUFFER OVER SIZE !!!") );
		size = 1023;
	}

	char buf[1024] = {0};
	strncpy(buf, start, size);

	return buf;
}

//ciBoolean contentsInfo::_findTag(const char* xml, const char* tag)
ciBoolean _findTag(const char* xml, const char* tag)
{
	char* find = (char*)strstr(xml, tag);
	if( find == NULL ) return ciFalse;
	return ciTrue;
}

ciBoolean contentsInfo::getContentsInfoList(contentsInfoList& infoList, ciString& strXML)
{
	ciDEBUG(5, ("getContentsInfoList()") );

	const char* xml_start = strXML.c_str();

	// clear list
	clearContentsInfoList(infoList);
/*
	// get brochure-contents
	char* tag_start = (char*)strstr(strXML.c_str(), BROCHURECONTENTS_STARTTAG);
	while( tag_start != NULL )
	{
		ciDEBUG(5, ("find brochure-tag at [%d]", tag_start-xml_start) );

		// find end-tag
		char* tag_end = strstr(tag_start, BROCHURECONTENTS_ENDTAG);
		if( tag_end == NULL )
		{
			ciWARN( ("MISSING BROCHURE-TAG !!!") );
			break;
		}
		tag_end += strlen(BROCHURECONTENTS_ENDTAG);

		// get item-string
		int size = tag_end - tag_start;
		if( size < 0 )
		{
			ciERROR( ("NEGATIVE SIZE !!!") );
			break;
		}
		char* buf_xml = new char[size+1];
		::ZeroMemory(buf_xml, size+1);
		strncpy(buf_xml, tag_start, size);

		// create new-object
		contentsInfo* info = getContentsInfoObject(buf_xml, OFS_BROCHURE_CONTENTS_TYPE);
		if( info != NULL )
		{
			infoList.push_back(info);
			ciDEBUG(5, ("new brochure-contents(size:%d) %s", infoList.size(), info->toString(ciFalse).c_str()) );
		}
		else
		{
			ciWARN( ("INVALID CONTENTS-XML !!!") );
		}
		delete buf_xml;

		//
		tag_start = strstr(tag_end, BROCHURECONTENTS_STARTTAG);
	}

	// get goods-contents
	tag_start = (char*)strstr(strXML.c_str(), GOODSCONTENTS_STARTTAG);
	while( tag_start != NULL )
	{
		ciDEBUG(5, ("find goods-tag at [%d]", tag_start-xml_start) );

		// find end-tag
		char* tag_end = strstr(tag_start, GOODSCONTENTS_ENDTAG);
		if( tag_end == NULL )
		{
			ciWARN( ("MISSING BROCHURE-TAG !!!") );
			break;
		}
		tag_end += strlen(GOODSCONTENTS_ENDTAG);

		// get item-string
		int size = tag_end - tag_start;
		if( size < 0 )
		{
			ciERROR( ("NEGATIVE SIZE !!!") );
			break;
		}
		char* buf_xml = new char[size+1];
		::ZeroMemory(buf_xml, size+1);
		strncpy(buf_xml, tag_start, size);

		// create new-object
		contentsInfo* info = getContentsInfoObject(buf_xml, OFS_GOODS_CONTENTS_TYPE);
		if( info != NULL )
		{
			infoList.push_back(info);
			ciDEBUG(5, ("new goods-contents(size:%d) %s", infoList.size(), info->toString(ciFalse).c_str()) );
		}
		else
		{
			ciWARN( ("INVALID CONTENTS-XML !!!") );
		}
		delete buf_xml;

		//
		tag_start = strstr(tag_end, GOODSCONTENTS_STARTTAG);
	}

	// get union-contents
	tag_start = (char*)strstr(strXML.c_str(), UNIONCONTENTS_STARTTAG);
	while( tag_start != NULL )
	{
		ciDEBUG(5, ("find contents-tag at [%d]", tag_start-xml_start) );

		// find end-tag
		char* tag_end = strstr(tag_start, UNIONCONTENTS_ENDTAG);
		if( tag_end == NULL )
		{
			ciWARN( ("MISSING CONTENTS-TAG !!!") );
			break;
		}
		tag_end += strlen(UNIONCONTENTS_ENDTAG);

		// get item-string
		int size = tag_end - tag_start;
		if( size < 0 )
		{
			ciERROR( ("NEGATIVE SIZE !!!") );
			break;
		}
		char* buf_xml = new char[size+1];
		::ZeroMemory(buf_xml, size+1);
		strncpy(buf_xml, tag_start, size);

		// create new-object
		contentsInfo* info = getContentsInfoObject(buf_xml, OFS_UNION_CONTENTS_TYPE);
		if( info != NULL )
		{
			infoList.push_back(info);
			ciDEBUG(5, ("new contents(size:%d) %s", infoList.size(), info->toString(ciFalse).c_str()) );
		}
		else
		{
			ciWARN( ("INVALID CONTENTS-XML !!!") );
		}

		//
		tag_start = strstr(tag_end, UNIONCONTENTS_STARTTAG);
	}
*/
	char* tag_start = (char*)strstr(strXML.c_str(), CONTENTS_STARTTAG);
	while( tag_start != NULL )
	{
		ciDEBUG(5, ("find brochure-tag at [%d]", tag_start-xml_start) );

		// find end-tag
		char* tag_end = strstr(tag_start, CONTENTS_ENDTAG);
		if( tag_end == NULL )
		{
			ciWARN( ("MISSING BROCHURE-TAG !!!") );
			break;
		}
		tag_end += strlen(CONTENTS_ENDTAG);

		// get item-string
		int size = tag_end - tag_start;
		if( size < 0 )
		{
			ciERROR( ("NEGATIVE SIZE !!!") );
			break;
		}
		char* buf_xml = new char[size+1];
		AUTO_DELETE(buf_xml);
		::ZeroMemory(buf_xml, size+1);
		strncpy(buf_xml, tag_start, size);

		// create new-object
		contentsInfo* info = getContentsInfoObject(buf_xml);
		if( info != NULL )
		{
			infoList.push_back(info);
			ciDEBUG(5, ("new brochure-contents(size:%d) %s", infoList.size(), info->toString(ciFalse).c_str()) );
		}
		else
		{
			ciWARN( ("INVALID CONTENTS-XML !!!") );
		}

		//
		tag_start = strstr(tag_end, CONTENTS_STARTTAG);
	}

	//
	if( infoList.size() == 0 )
	{
		ciWARN( ("NO CONTENTS-INFO !!!") );
		return ciFalse;
	}

	ciDEBUG(5, ("total contents-info count (%d)", infoList.size()) );

	return ciTrue;
}

void contentsInfo::clearContentsInfoList(contentsInfoList& infoList)
{
	ciDEBUG(5, ("clearContentsInfoList(%d)", infoList.size()) );

	contentsInfoListIter itr = infoList.begin();
	contentsInfoListIter end = infoList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* info = (*itr);
		if(info) delete info;
	}
	infoList.clear();

	ciDEBUG(5, ("clearContentsInfoList() end") );
}

contentsInfo* contentsInfo::getContentsInfoObject(const char* xml, OFS_CONTENTS_TYPE type)
{
	ciDEBUG(5, ("getContentsInfoObject(%d)", type) );

	// no-type
	if( type == OFS_UNION_CONTENTS_TYPE )
	{
		// find contents-type
		//if( _findTag(xml, BROCHURECONTENTS_STARTTAG) && _findTag(xml, BROCHURECONTENTS_ENDTAG) )
		//	type = OFS_BROCHURE_CONTENTS_TYPE;
		//else if( _findTag(xml, GOODSCONTENTS_STARTTAG) && _findTag(xml, GOODSCONTENTS_ENDTAG) )
		//	type = OFS_GOODS_CONTENTS_TYPE;

		ciString str_type = _getItem(xml, CONTENTSTYPE_STARTTAG, CONTENTSTYPE_ENDTAG);
		if( str_type == BROCHURE_CONTENTS_TYPE )
			type = OFS_BROCHURE_CONTENTS_TYPE;
		else if( str_type == GOODS_CONTENTS_TYPE )
			type = OFS_GOODS_CONTENTS_TYPE;
		else if( str_type == DELETE_CONTENTS_TYPE )
			type = OFS_DELETE_CONTENTS_TYPE;

		ciDEBUG(5, ("new contents type (%d)", type) );
	}

	// create new-object
	contentsInfo* info = NULL;
	//switch( type )
	//{
	//default:
	//case OFS_UNION_CONTENTS_TYPE:
		info = new contentsInfo;
	//	break;

	//case OFS_BROCHURE_CONTENTS_TYPE:
	//	info = new brochureContentsInfo;
	//	break;

	//case OFS_GOODS_CONTENTS_TYPE:
	//	info = new goodsContentsInfo;
	//	break;
	//}

	// extract info
	if( info && info->fromString(xml) )
	{
		info->contentsType = type;
		ciDEBUG(5, ("fromString succeed (%s)", info->toString(ciFalse).c_str()) );
		return info;
	}
	ciWARN( ("fromString FAIL !!! (%s)", xml) );

	// no-type || no-info
	delete info;

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

contentsInfo::contentsInfo()
{
	init();
}

void contentsInfo::init()
{
	contentsType	= OFS_UNION_CONTENTS_TYPE;
	mediaType		= "";
	filename		= "";
	location		= "";
	timeFlag		= "";
	volume			= 0;

	time_t tm = 1;
	lastUpdateTime.set(tm);
};

ciBoolean contentsInfo::fromString(const char* xml)
{
	ciDEBUG(5, ("fromString()") );

	init();

	// sample :
	// <downFile>
	//	 	<filename>SQJAW15512GYD_A1.jpg</filename>
	//		<location>contents/uploadData/goods/SQJAW15512GYD/SQJAW15512GYD_A1.jpg</location>
	//	 	<volume>0</volume>
	//		<lastUpdateTime>1445504178</lastUpdateTime>
	// </downFile>

	// mediaType
	mediaType = _getItem(xml, MEDIATYPE_STARTTAG, MEDIATYPE_ENDTAG);

	// filename
	filename	= _getItem(xml, FILENAME_STARTTAG, FILENAME_ENDTAG);

	// location
	location	= "/" + _getItem(xml, LOCATION_STARTTAG, LOCATION_ENDTAG);
	ciStringUtil::stringReplace(location, "//", "/");

	// volume
	volume		= _atoi64(_getItem(xml, VOLUME_STARTTAG, VOLUME_ENDTAG).c_str());

	// lastUpdateTime
	time_t tm	= _atoi64(_getItem(xml, LASTUPDATETIME_STARTTAG, LASTUPDATETIME_ENDTAG).c_str());
	if( tm > 0 ) lastUpdateTime.set(tm);

	//
	filenameOri	= _getItem(xml, FILENAME_ORI_STARTTAG, FILENAME_ORI_ENDTAG);

	//
	timeFlag	= _getItem(xml, TIMEFLAG_STARTTAG, TIMEFLAG_ENDTAG);

	// use only in KHQS
	if( ciArgParser::getInstance()->isSet("+KHQS") )
	{
		if( _getLocalPath("contents/KHQS/", "KHQS\\", location.c_str(), lastUpdateTime.getTime()) == ciFalse )
		{
			ciWARN( ("INVALID PATH !!!") );
			return ciFalse;
		}
	}
	//
	else if( ciArgParser::getInstance()->isSet("+HMCSFS") )
	{
		if( _getLocalPath("files/pmis/", "PDIS\\", location.c_str(), lastUpdateTime.getTime()) == ciFalse )
		{
			ciWARN( ("INVALID PATH !!!") );
			return ciFalse;
		}
	}
	// use only in OFS
	else if( _getLocalPath("contents/uploadData/", "OFS\\", location.c_str(), lastUpdateTime.getTime()) == ciFalse )
	{
		ciWARN( ("INVALID PATH !!!") );
		return ciFalse;
	}

	if( filename.length() == 0 /*|| volume == 0*/ )
	{
		ciWARN( ("NO FILENAME OR SIZE !!!") );
		return ciFalse;
	}

	ciDEBUG(5, ("fromString(%s)", toString(ciFalse).c_str()) );

	return ciTrue;
}

ciString contentsInfo::toString(ciBoolean xml_type)
{
	char buf[4096] = {0};

	if( xml_type )
	{
		sprintf(buf,
			"	%s\r\n"
			"		<contentType>%d</contentType>\r\n"
			"		<filename>%s</filename>\r\n"
			"		<filenameOri>%s</filenameOri>\r\n"
			"		<location>%s</location>\r\n"
			"		<timeFlag>%s</timeFlag>\r\n", 
			"		<volume>%I64d</volume>\r\n", 
			"		<lastUpdateTime>%I64d</lastUpdateTime>\r\n"
			"	%s\r\n",
			CONTENTS_STARTTAG,//UNIONCONTENTS_STARTTAG,
				contentsType,
				filename.c_str(),
				filenameOri.c_str(),
				location.c_str(),
				timeFlag.c_str(),
				volume,
				lastUpdateTime.getTime(),
			CONTENTS_ENDTAG//UNIONCONTENTS_ENDTAG
			);
	}
	else
	{
		sprintf(buf, 
			"{"
				"{contentsType,%d},"
				"{filename,%s},"
				"{filenameOri,%s},"
				"{location,%s},"
				"{timeFlag,%s},"
				"{volume,%I64d},"
				"{lastUpdateTime,%I64d}"
			"}",
				contentsType,
				filename.c_str(),
				filenameOri.c_str(),
				location.c_str(),
				timeFlag.c_str(),
				volume,
				lastUpdateTime.getTime()
			);
	}

	return buf;
}

contentsInfo* contentsInfo::duplicate()
{
	contentsInfo* dup = new contentsInfo;
	*dup = *this;

	return dup;
}

ciBoolean contentsInfo::isEqual(contentsInfo* info)
{
	if( contentsType	!= info->contentsType	) return ciFalse;

	if( mediaType		!= info->mediaType		) return ciFalse;
	if( filename		!= info->filename		) return ciFalse;
	if( filenameOri		!= info->filenameOri	) return ciFalse;
	if( location		!= info->location		) return ciFalse;
	if( timeFlag		!= info->timeFlag		) return ciFalse;
	if( volume			!= info->volume			) return ciFalse;
	if( lastUpdateTime.getTime() != info->lastUpdateTime.getTime() ) return ciFalse;

	return ciTrue;
}

ciBoolean contentsInfo::_getLocalPath(const char* findStr, const char* replaceStr, const char* path, time_t tm)
{
	//
	localPath = "";
	tempPath = "";
	bakPath = "";

	// sample :
	// contents/uploadData/goods/SQWAW15626GYX/SQWAW15621GYX_A6.jpg
	// contents/uploadData/brochure/test/TEST_001.mp4
	// ^------------------^ cut
	char* start = (char*)strstr(path, findStr);//"contents/uploadData/");
	if( start == NULL )
	{
		ciWARN( ("INVALID PATH !!! (%s)", path) );
	}
	else
		start += strlen(findStr);//"contents/uploadData/");

	//
	localPath = SQI_ENC_DIRECTORY;
	localPath += replaceStr;
	localPath += start;

	if( filenameOri.length() > 0 ) // for HMCSFS
	{
		ciStringUtil::stringReplace(localPath, filename.c_str(), filenameOri.c_str()); // rename filename to filenameOri
		if( strstr(localPath.c_str(), filenameOri.c_str()) == NULL )
		{
			ciWARN( ("INVALID PATH (NOT EXIST FILENAME IN LOCATION) !!! (filename:%s, localPath:%s)", filename.c_str(), localPath.c_str()) );
		}
	}

	tempPath = SQI_TMP_DIRECTORY;
	tempPath += replaceStr;
	tempPath += start;
	char tmp[32] = {0};
	sprintf(tmp, ".%I64d", tm);
	tempPath += tmp;

	bakPath = localPath;
	bakPath += ".bak";

	ciStringUtil::stringReplace(localPath, "/", "\\");
	ciStringUtil::stringReplace(localPath, "\\\\", "\\");

	ciStringUtil::stringReplace(tempPath, "/", "\\");
	ciStringUtil::stringReplace(tempPath, "\\\\", "\\");

	ciStringUtil::stringReplace(bakPath, "/", "\\");
	ciStringUtil::stringReplace(bakPath, "\\\\", "\\");

	ciDEBUG(5, ("local_path (%s)", localPath.c_str()) );
	ciDEBUG(5, ("temp_path (%s)", tempPath.c_str()) );
	ciDEBUG(5, ("backup_path (%s)", bakPath.c_str()) );

	return ciTrue;
}

////////////////////////////////////////////////////////////////////////////////

brochureContentsInfo::brochureContentsInfo()
{
	init();
}

void brochureContentsInfo::init()
{
	contentsInfo::init();

	contentsType	= OFS_BROCHURE_CONTENTS_TYPE;
	mgrId			= "";
	siteId			= "";
	brochureId		= "";
	pageId			= "";
	frameId			= "";
	contentsId		= "";
	brochureType	= "";
	zorder			= 0;
	runningTime		= 0;
	creatorId		= "";
	description		= "";
};

ciBoolean brochureContentsInfo::fromString(const char* xml)
{
	if( !contentsInfo::fromString(xml) ) return ciFalse;

	mgrId			= _getItem(xml, MGRID_STARTTAG, MGRID_ENDTAG);
	siteId			= _getItem(xml, SITEID_STARTTAG, SITEID_ENDTAG);
	brochureId		= _getItem(xml, BROCHUREID_STARTTAG, BROCHUREID_ENDTAG);
	pageId			= _getItem(xml, PAGEID_STARTTAG, PAGEID_ENDTAG);
	frameId			= _getItem(xml, FRAMEID_STARTTAG, FRAMEID_ENDTAG);
	contentsId		= _getItem(xml, CONTENTSID_STARTTAG, CONTENTSID_ENDTAG);
	brochureType	= _getItem(xml, BROCHURETYPE_STARTTAG, BROCHURETYPE_ENDTAG);
	zorder			= atoi(_getItem(xml, ZORDER_STARTTAG, ZORDER_ENDTAG).c_str());
	runningTime		= atoi(_getItem(xml, RUNNINGTIME_STARTTAG, RUNNINGTIME_ENDTAG).c_str());
	creatorId		= _getItem(xml, CREATORID_STARTTAG, CREATORID_ENDTAG);
	description		= _getItem(xml, DESCRIPTION_STARTTAG, DESCRIPTION_ENDTAG);

	return ciTrue;
}

ciString brochureContentsInfo::toString(ciBoolean xml_type)
{
	char buf[4096] = {0};

	if( xml_type )
	{
		sprintf(buf,
			"	%s\r\n"
			"		<mgrId>%s</mgrId>\r\n"
			"		<siteId>%s</siteId>\r\n"
			"		<brochureId>%s</brochureId>\r\n"
			"		<pageId>%s</pageId>\r\n"
			"		<frameId>%s</frameId>\r\n"
			"		<contentsId>%s</contentsId>\r\n"
			"		<brochureType>%s</brochureType>\r\n"
			"		<mediaType>%s</mediaType>\r\n"
			"		<filename>%s</filename>\r\n"
			"		<location>%s</location>\r\n"
			"		<volume>%I64d</volume>\r\n"
			"		<zorder>%d</zorder>\r\n"
			"		<runningTime>%d</runningTime>\r\n"
			"		<creatorId>%s</creatorId>\r\n"
			"		<lastUpdateTime>%I64d</lastUpdateTime>\r\n"
			"		<description>%s</description>\r\n"
			"	%s\r\n",
				CONTENTS_STARTTAG,//BROCHURECONTENTS_STARTTAG,
					mgrId.c_str(),
					siteId.c_str(),
					brochureId.c_str(),
					pageId.c_str(),
					frameId.c_str(),
					contentsId.c_str(),
					brochureType.c_str(),
					mediaType.c_str(),
					filename.c_str(),
					location.c_str(),
					volume,
					zorder,
					runningTime,
					creatorId.c_str(),
					lastUpdateTime.getTime(),
					description.c_str(),
				CONTENTS_ENDTAG//BROCHURECONTENTS_ENDTAG
			);
	}
	else
	{
		sprintf(buf,
			"{"
				"{contentsType,brochureContents},"
				"{mgrId,%s},"
				"{siteId,%s},"
				"{brochureId,%s},"
				"{pageId,%s},"
				"{frameId,%s},"
				"{contentsId,%s},"
				"{brochureType,%s},"
				"{mediaType,%s},"
				"{filename,%s},"
				"{location,%s},"
				"{volume,%I64d},"
				"{zorder,%d},"
				"{runningTime,%d},"
				"{creatorId,%s},"
				"{lastUpdateTime,%I64d},"
				"{description,%s},"
			"}",
				mgrId.c_str(),
				siteId.c_str(),
				brochureId.c_str(),
				pageId.c_str(),
				frameId.c_str(),
				contentsId.c_str(),
				brochureType.c_str(),
				mediaType.c_str(),
				filename.c_str(),
				location.c_str(),
				volume,
				zorder,
				runningTime,
				creatorId.c_str(),
				lastUpdateTime.getTime(),
				description.c_str()
			);
	}

	return buf;
}

contentsInfo* brochureContentsInfo::duplicate()
{
	brochureContentsInfo* dup = new brochureContentsInfo;
	*dup = *this;

	return dup;
}

ciBoolean brochureContentsInfo::isEqual(contentsInfo* info)
{
	//if( contentsType != info->contentsType ) return ciFalse;
	if( !contentsInfo::isEqual(info) ) return ciFalse;

	brochureContentsInfo* tmp = (brochureContentsInfo*)info;
	if( mgrId			!= tmp->mgrId			) return ciFalse;
	if( siteId			!= tmp->siteId			) return ciFalse;
	if( brochureId		!= tmp->brochureId		) return ciFalse;
	if( pageId			!= tmp->pageId			) return ciFalse;
	if( frameId			!= tmp->frameId			) return ciFalse;
	if( contentsId		!= tmp->contentsId		) return ciFalse;
	if( brochureType	!= tmp->brochureType	) return ciFalse;
	if( zorder			!= tmp->zorder			) return ciFalse;
	if( runningTime		!= tmp->runningTime		) return ciFalse;
	if( creatorId		!= tmp->creatorId		) return ciFalse;
	if( description		!= tmp->description		) return ciFalse;

	return ciTrue;
}

////////////////////////////////////////////////////////////////////////////////

goodsContentsInfo::goodsContentsInfo()
{
	init();
}

void goodsContentsInfo::init()
{
	contentsInfo::init();

	contentsType	= OFS_GOODS_CONTENTS_TYPE;
	mgrId			= "";
	siteId			= "";
	goodsId			= "";
	contentsId		= "";
	parentId		= "";
	usageType		= "";
	creatorId		= "";
	description		= "";
};

ciBoolean goodsContentsInfo::fromString(const char* xml)
{
	if( !contentsInfo::fromString(xml) ) return ciFalse;

	mgrId			= _getItem(xml, MGRID_STARTTAG, MGRID_ENDTAG);
	siteId			= _getItem(xml, SITEID_STARTTAG, SITEID_ENDTAG);
	goodsId			= _getItem(xml, GOODSID_STARTTAG, GOODSID_ENDTAG);
	contentsId		= _getItem(xml, CONTENTSID_STARTTAG, CONTENTSID_ENDTAG);
	parentId		= _getItem(xml, PARENTID_STARTTAG, PARENTID_ENDTAG);
	usageType		= _getItem(xml, USAGETYPE_STARTTAG, USAGETYPE_ENDTAG);
	creatorId		= _getItem(xml, CREATORID_STARTTAG, CREATORID_ENDTAG);
	description		= _getItem(xml, DESCRIPTION_STARTTAG, DESCRIPTION_ENDTAG);

	return ciTrue;
}

ciString goodsContentsInfo::toString(ciBoolean xml_type)
{
	char buf[4096] = {0};
	if( xml_type )
	{
		sprintf(buf,
			"	%s\r\n"
			"		<mgrId>%s</mgrId>\r\n"
			"		<siteId>%s</siteId>\r\n"
			"		<goodsId>%s</brochureId>\r\n"
			"		<contentsId>%s</contentsId>\r\n"
			"		<mediaType>%s</mediaType>\r\n"
			"		<filename>%s</filename>\r\n"
			"		<location>%s</location>\r\n"
			"		<volume>%I64d</volume>\r\n"
			"		<parentId>%s</parentId>\r\n"
			"		<usageType>%s</usageType>\r\n"
			"		<creatorId>%s</creatorId>\r\n"
			"		<lastUpdateTime>%I64d</lastUpdateTime>\r\n"
			"		<description>%s</description>\r\n"
			"	%s\r\n",
				CONTENTS_STARTTAG,//GOODSCONTENTS_STARTTAG,
					mgrId.c_str(),
					siteId.c_str(),
					goodsId.c_str(),
					contentsId.c_str(),
					mediaType.c_str(),
					filename.c_str(),
					location.c_str(),
					volume,
					parentId.c_str(),
					usageType.c_str(),
					creatorId.c_str(),
					lastUpdateTime.getTime(),
					description.c_str(),
				CONTENTS_ENDTAG//GOODSCONTENTS_ENDTAG
			);
	}
	else
	{
		sprintf(buf,
			"{"
				"{mgrId,%s},"
				"{siteId,%s},"
				"{goodsId,%s},"
				"{contentsId,%s},"
				"{mediaType,%s},"
				"{filename,%s},"
				"{location,%s},"
				"{volume,%I64d},"
				"{parentId,%s},"
				"{usageType,%s},"
				"{creatorId,%s},"
				"{lastUpdateTime,%I64d},"
				"{description,%s},"
			"}",
				mgrId.c_str(),
				siteId.c_str(),
				goodsId.c_str(),
				contentsId.c_str(),
				mediaType.c_str(),
				filename.c_str(),
				location.c_str(),
				volume,
				parentId.c_str(),
				usageType.c_str(),
				creatorId.c_str(),
				lastUpdateTime.getTime(),
				description.c_str()
			);
	}
	return buf;
}

contentsInfo* goodsContentsInfo::duplicate()
{
	goodsContentsInfo* dup = new goodsContentsInfo;
	*dup = *this;

	return dup;
}

ciBoolean goodsContentsInfo::isEqual(contentsInfo* info)
{
	//if( contentsType != info->contentsType ) return ciFalse;
	if( !contentsInfo::isEqual(info) ) return ciFalse;

	goodsContentsInfo* tmp = (goodsContentsInfo*)info;
	if( mgrId			!= tmp->mgrId		) return ciFalse;
	if( siteId			!= tmp->siteId		) return ciFalse;
	if( goodsId			!= tmp->goodsId		) return ciFalse;
	if( contentsId		!= tmp->contentsId	) return ciFalse;
	if( parentId		!= tmp->parentId	) return ciFalse;
	if( usageType		!= tmp->usageType	) return ciFalse;
	if( creatorId		!= tmp->creatorId	) return ciFalse;
	if( description		!= tmp->description	) return ciFalse;

	return ciTrue;
}
