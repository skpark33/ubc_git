// MenuRegister.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "MenuRegister.h"
#include "scPath.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 유일한 응용 프로그램 개체입니다.

CWinApp theApp;

using namespace std;


//#define		UBC_CONTENTS_PATH		"C:\\SQISoft\\UTV1.0\\Contents\\"
#define		UBC_EXECUTE_PATH		"C:\\SQISoft\\UTV1.0\\execute\\"
#define		UBC_SERVER_UTIL_PATH	"C:\\Project\\ubc\\util\\win32\\"

typedef struct
{
	char*	filename;
	char*	source_path;
	int 	target_path;
} INSTALL_ITEM;


INSTALL_ITEM client_uninstall_item[] = {
	{"UBCFirmwareView.lnk",		UBC_EXECUTE_PATH, CSIDL_STARTUP},
	{"UBC Start.lnk",		UBC_EXECUTE_PATH, CSIDL_STARTUP},
	{"VNC Proxy.lnk",		UBC_EXECUTE_PATH, CSIDL_STARTUP},
	{"softDrive.lnk",		UBC_EXECUTE_PATH, CSIDL_STARTUP},
	{"UBC Tools.lnk",		UBC_EXECUTE_PATH, CSIDL_DESKTOP},
	{"Shutdown System.lnk",	UBC_EXECUTE_PATH, CSIDL_DESKTOP},
	{"Reboot System.lnk",	UBC_EXECUTE_PATH, CSIDL_DESKTOP},
	{"UBC Enterprise Manager.lnk",	UBC_EXECUTE_PATH, CSIDL_DESKTOP},
	{"UBC Enterprise Studio.lnk",	UBC_EXECUTE_PATH, CSIDL_DESKTOP},
	{"UBCReady.lnk",		UBC_EXECUTE_PATH, CSIDL_STARTUP},
	{"", "", 0}
};

INSTALL_ITEM client_install_item[] = {
	{"UBCReady.lnk",		UBC_EXECUTE_PATH, CSIDL_STARTUP},
	{"", "", 0}
};

INSTALL_ITEM server_uninstall_item[] = {
	{"UBC Start.lnk",		UBC_SERVER_UTIL_PATH, CSIDL_STARTUP},
	{"", "", 0}
};

INSTALL_ITEM server_install_item[] = {
	{"UBC Start.lnk",		UBC_SERVER_UTIL_PATH, CSIDL_STARTUP},
	{"", "", 0}
};

INSTALL_ITEM* install_item=0;
INSTALL_ITEM* uninstall_item=0;


bool Install();
bool Uninstall();


int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	

	// MFC를 초기화합니다. 초기화하지 못한 경우 오류를 인쇄합니다.
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: 오류 코드를 필요에 따라 수정합니다.
		_tprintf(_T("심각한 오류: MFC를 초기화하지 못했습니다.\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: 응용 프로그램의 동작은 여기에서 코딩합니다.
		install_item=client_install_item;
		uninstall_item=client_uninstall_item;
			
		if(argc == 3){
			if(_stricmp(argv[2], "/server") == 0)
			{
				install_item=server_install_item;
				uninstall_item=server_uninstall_item;
			}
		}

		if(argc >= 2)
		{
			if(_stricmp(argv[1], "/install") == 0)
			{
				
				Install();

				return nRetCode;
			}
			else if(_stricmp(argv[1], "/uninstall") == 0)
			{
				Uninstall();

				return nRetCode;
			}
		}

		cout << "\r\nUsage : MenuRegister /install" << "\r\n        MenuRegister /uninstall\r\n";
	}

	return nRetCode;
}

bool Install()
{
	CString source, target;
	char TargetPath[MAX_PATH+1];

	int i=0;
	while(install_item[i].filename[0] != 0)
	{
		source = _UBC_CD(install_item[i].source_path);
		source.Append(install_item[i].filename);

		::ZeroMemory(TargetPath, sizeof(TargetPath));
		::SHGetSpecialFolderPath (NULL, TargetPath, install_item[i].target_path, FALSE);

		target= TargetPath;
		target.Append("\\");
		target.Append(install_item[i].filename);

#ifdef _DEBUG
		TRACE("%s\r\n%s\r\n\r\n", source, target);
#else
		::CopyFile(source, target, FALSE);
#endif

		i++;
	}

	return true;
}

bool Uninstall()
{
	CString source, target;
	char TargetPath[MAX_PATH+1];

	int i=0;
	while(uninstall_item[i].filename[0] != 0)
	{
		::ZeroMemory(TargetPath, sizeof(TargetPath));
		::SHGetSpecialFolderPath (NULL, TargetPath, uninstall_item[i].target_path, FALSE);

		target= TargetPath;
		target.Append("\\");
		target.Append(uninstall_item[i].filename);

#ifdef _DEBUG
		TRACE("%s\r\n\r\n", target);
#else
		::DeleteFile(target);
#endif


		i++;
	}
#ifdef _HYUNDAI_KIA_
	CString command1 = "rd /S /Q D:\\SQISoft D:\\ftproot";
	system(command1.GetBuffer());
#endif
	return true;
}
