//#include "libDownload/ProfileManager.h"
#include "libDownload/Downloader.h"
#include "common/libAGTData/AGTDownloadResult.h"
#include "libAGTClient/AGTDownloadResultClient.h"

#include <cci/libWrapper/cciEvent.h>
#include "common/libInstall/installUtil.h"
#include "common/libScratch/scratchUtil.h"
#include "common/libCommon/ubcIni.h"
#include "common/libPlan/planData.h"
#include "common/libPlan/planManager.h"
#include <libProjectorControl/ProjectorControl.h>


#include "reloadEventHandler.h"

ciSET_DEBUG(10, "reloadEventHandler");

reloadEventHandler::~reloadEventHandler()  
{ 
	delete _libDownloader;
}

ciBoolean
reloadEventHandler::addHand()
{
	ciDEBUG(5,("addHand()"));
	return ciTrue;
}

void
reloadEventHandler::processEvent(cciEvent& aEvent)
{
	ciDEBUG(5,("processEvent()"));
 
	ciString eventName = aEvent.getEventType();

	if(eventName == "download" || eventName == "stopDownload") {
		/*
		HWND downHwnd = scratchUtil::getInstance()->getWHandle(DOWNLOADER_NAME);
		HWND fwvHwnd = scratchUtil::getInstance()->getWHandle("UBCFirmwareView.exe");
		if(!downHwnd){
			ciERROR(("get Window Handler fail"));
			return;
		}
		ciDEBUG(1,("download"));
		*/
		ciString siteId;
		if(!aEvent.getItem("siteId", siteId)) {
			ciERROR(("%s Event does not have siteId", eventName.c_str()));
			return ;
		}

		ciString programId;
		if(!aEvent.getItem("programId", programId)) {
			ciERROR(("%s Event does not have programId", eventName.c_str()));
			return ;
		}

		ciShort side;
		if(!aEvent.getItem("side", side)) {
			ciERROR(("%s Event does not have side", eventName.c_str()));
			return ;
		}

		ciShort downloadType;
		if(!aEvent.getItem("downloadType", downloadType)) {
			ciERROR(("%s Event does not have downloadType", eventName.c_str()));
			return ;
		}

		if(0 == _libDownloader){
			_libDownloader = new CDownloader;
			_libDownloader->SetCheckFileTime(this->_fileTimeCheck);
		}

		int brwId = 0;
		for(int i=1;i<=2;i++){
			if(side & i) {
				brwId = i-1;
				/*
				CmdDownload lpData(programId.c_str(), siteId.c_str(), downloadType, brwId, fwvHwnd);\
				lpData.printIt();

				COPYDATASTRUCT appInfo;
				appInfo.dwData = WM_CMD_DOWNLOAD_START;  // download 시작
				appInfo.lpData = lpData.getData();
				appInfo.cbData = 1;
				//::PostMessage(downHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
				if(0 == ::SendMessage(downHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo)){
					ciERROR(("Message send error"));
				}else{
					ciDEBUG(1,("download SendMessage (brwId 1)"));
				}
				ciDEBUG(1,("download SendMessage (brwId 0)"));
				*/

				CString strPackageFile, strSiteId;
				strPackageFile = programId.c_str();      //경로없이 파일명만
				strSiteId = siteId.c_str();

				if(downloadType == E_TYPE_BPI_FILE){
					ciDEBUG(1,("start download plan bpi(%s)", programId.c_str()));
					if(!_libDownloader->DownloadBPIFile(strPackageFile, strSiteId)) {
						ciWARN(("Fail to download bpi file : %s", strPackageFile));
   						aEvent.addItem("result", ciFalse);
					}else{
						ciDEBUG(1,("Succeed to download bpi file : %s", strPackageFile));
						aEvent.addItem("result", ciTrue);
					}
					continue ;
				}
				if(downloadType == E_TYPE_PACKAGE_FILE){
					int nBRWId = 0;
					ciDEBUG(1,("start download program ini(%s)", programId.c_str()));
					
					if(!_libDownloader->DownloadPackageFile(strPackageFile, strSiteId,nBRWId,false)) {
						ciWARN(("Fail to download package file : %s", strPackageFile));
   						aEvent.addItem("result", ciFalse);
					}else{
						ciDEBUG(1,("Succeed to download ini file : %s", strPackageFile));
						aEvent.addItem("result", ciTrue);
					}
					continue ;
				}
				if(downloadType == E_TYPE_ALL_FILE) {
					int nBRWId = 0;
					ciDEBUG(1,("start download package and contents(%s,%s)",strSiteId,strPackageFile));
					int nRet = _libDownloader->ProcessDownloadPackage(strPackageFile, strSiteId,nBRWId,ciFalse);
					switch(nRet)
					{
						case E_DNRET_SUCCESS:          //성공
						case E_DNRET_ALREADY_EXIST:     //성공
						   {
							   //ciDEBUG(1,("%s", pszRet[nRet])); 
							   ciDEBUG(1, ("Download success %d", nRet)); 
		   						aEvent.addItem("result", ciTrue);
							   break; 
						   }
						case E_DNRET_FAIL_MISSING_FILE:     //해당컨텐츠패키지파일을 찾지 못함
						case E_DNRET_FAIL_FTP_ADDR:        //FTP 주소를 받아오지 못함
						case E_DNRET_FAIL_FTP_CONNECT:     //FTP 연결에 실패함
						case E_DNRET_FAIL_EMPTY_LIST:       //컨텐츠패키지파일에 다운로드해야하는 컨텐츠 파일이 없음
						case E_DNRET_CANCEL:           //다운로드 작업이 중단되었음
						case E_DNRET_ERROR_UNKNOWN:           //다운로드 작업이 중단되었음
						case E_DNRET_FAIL_PACKAGE_DOWNLOAD:           //다운로드 작업이 중단되었음
						default :
							{
								ciERROR(("Anyway, Download failed {%d}", nRet));
							   aEvent.addItem("result", ciFalse);
							   break; 
						   }
					}//switch
					if(nRet == E_DNRET_ALREADY_EXIST){
						ciDEBUG(1,("All files are already  exist"));
						continue;
					}
					int downloadState = AGTDownloadResultClient::getInstance()->getDownloadResult(programId.c_str());
					if(downloadState >= 0){
						if(downloadState == DN_SUCCESS || downloadState == DN_SUCCESS_NO_DN){
							ciDEBUG(1, ("%s download succeed", programId.c_str())); 
							aEvent.addItem("result", ciTrue);
						}else{
							ciDEBUG(1, ("%s download failed(%d)", programId.c_str(), downloadState)); 
							aEvent.addItem("result", ciFalse);
						}
					}else{
						ciERROR(("%s get download result failed", programId.c_str())); 
					}
					continue;
				} // if-else
				if(downloadType == E_TYPE_CONTENTS_FILE) {
					int nBRWId = 0;
					if(eventName == "download"){
						ciDEBUG(1,("start download contents(%s,%s)",strSiteId,strPackageFile));
						// skpark 이미 다운로드 된것이 아닌지 미리 한번 본다. 2012.4.20
						int downloadState = _libDownloader->IsAlreadyDownloaded(strPackageFile);
						if(downloadState == 1){
							ciDEBUG2(1,("%s package already downloaded", strPackageFile));
							continue;
						}
						int nRet = _libDownloader->DownloadPlayContents(strSiteId, strPackageFile, nBRWId,ciFalse);
						switch(nRet)
						{
							case E_DNRET_SUCCESS:          //성공
							   {
								   //ciDEBUG(1,("%s", pszRet[nRet])); 
								   ciDEBUG(1,("Downlaod sucess %d", nRet)); 
			   						aEvent.addItem("result", ciTrue);
								   break; 
							   }
							case E_DNRET_FAIL_MISSING_FILE:     //해당컨텐츠패키지파일을 찾지 못함
							case E_DNRET_FAIL_FTP_ADDR:        //FTP 주소를 받아오지 못함
							case E_DNRET_FAIL_FTP_CONNECT:     //FTP 연결에 실패함
							case E_DNRET_FAIL_EMPTY_LIST:       //컨텐츠패키지파일에 다운로드해야하는 컨텐츠 파일이 없음
							case E_DNRET_CANCEL:           //다운로드 작업이 중단되었음
							default :
								{
									ciERROR(("Anyway, Download failed {%d}", nRet));
			   						aEvent.addItem("result", ciFalse);
								   break; 
							   }
						}//switch
						downloadState = AGTDownloadResultClient::getInstance()->getDownloadResult(programId.c_str());
						if(downloadState >= 0){
							if(downloadState == DN_SUCCESS || downloadState == DN_SUCCESS_NO_DN){
								ciDEBUG(1, ("%s download succeed", programId.c_str())); 
								aEvent.addItem("result", ciTrue);
							}else{
								ciDEBUG(1, ("%s download failed(%d)", programId.c_str(), downloadState)); 
								aEvent.addItem("result", ciFalse);
							}
						}else{
							ciERROR(("%s get download result failed", programId.c_str())); 
						}
						continue;
					}
					if(eventName == "stopDownload"){
						ciDEBUG(1,("stop download contents(%s)", programId.c_str()));
						_libDownloader->CancelDownload();
						continue;
					}
				} // if-else
			} // if brw & i
		} // for
		return;
	} // if(eventName==)

	if(eventName == "reloadSchedule") {
		ciDEBUG(1,("%s event Received", eventName.c_str()));

		ciString siteId;
		if(!aEvent.getItem("siteId", siteId)) {
			ciERROR(("%s Event does not have siteId", eventName.c_str()));
			return ;
		}

		ciString programId;
		if(!aEvent.getItem("programId", programId)) {
			ciERROR(("%s Event does not have programId", eventName.c_str()));
			return ;
		}

		ciShort side;
		if(!aEvent.getItem("side", side)) {
			ciERROR(("%s Event does not have side", eventName.c_str()));
			return ;
		}

		ciTime eventKey;
		if(!aEvent.getItem("eventKey", eventKey)) {
			ciERROR(("%s Event does not have eventKey", eventName.c_str()));
			return ;
		}

		ciTime todayStartTime;
		if(!aEvent.getItem("todayStartTime", todayStartTime)) {
			ciERROR(("%s Event does not have todayStartTime", eventName.c_str()));
			return ;
		}

		ciString by = "AGENT";
		aEvent.getItem("BY", by);
		
		ciDEBUG(1,("%s event Received", eventName.c_str()));
		ciDEBUG(1,("eventKey=%s,%ld ", eventKey.getTimeString().c_str(), eventKey.getTime()));
		ciDEBUG(1,("siteId=%s ", siteId.c_str()));
		ciDEBUG(1,("programId=%s ", programId.c_str()));
		ciDEBUG(1,("side=%d ", side));
		ciDEBUG(1,("todayStartTime=%s,%ld ", todayStartTime.getTimeString().c_str(), todayStartTime.getTime()));
		ciDEBUG(1,("BY=%s ", by.c_str()));

		if(_isAlreadyDone(programId.c_str(), eventKey.getTime(),side, todayStartTime.getTime())){
			return;
		}

		int monitorCount = scratchUtil::getInstance()->getMonitorCount();
		if(monitorCount==1){
			// 만약, 모니터 갯수가 1개라면,  brwClient2_UBC2.exe 는 떠있어서는 안되며
			// Auto 도 해제되어 있어야 한다.
			installUtil::getInstance()->stopBrowser2(2);
		}

		if(programId == "NULL"){
			if(side & 1) {
				if(!installUtil::getInstance()->stopBrowser2(1)){
					ciDEBUG(1,("brwClient2.exe does not exist"));
				}else{
					ciDEBUG(1,("kill brwClient2.exe succeed"));
				}
			}
			if(side & 2) {
				if(!installUtil::getInstance()->stopBrowser2(2)){
					ciDEBUG(1,("brwClient2_UBC1.exe does not exist"));
				}else{
					ciDEBUG(1,("kill brwClient2_UBC1.exe succeed"));
				}
			}
			ubcMutexConfig aIni("UBCBrowser");
			aIni.set("RELOADSCHEDULE_LOG","TIME",(ciULong)eventKey.getTime());
			aIni.set("RELOADSCHEDULE_LOG","NAME",eventName.c_str());
			aIni.set("RELOADSCHEDULE_LOG","PROGRAM",programId.c_str());
			aIni.set("RELOADSCHEDULE_LOG","SIDE",side);
			//skpark add 20100531
			if(side & 1) {
				aIni.set("RELOADSCHEDULE_LOG","PROGRAM1",programId.c_str());
				aIni.set("RELOADSCHEDULE_LOG","TIME1",(ciULong)eventKey.getTime());
				aIni.set("RELOADSCHEDULE_LOG","BY1",by.c_str());
			}
			if(side & 2) {
				aIni.set("RELOADSCHEDULE_LOG","PROGRAM2",programId.c_str());
				aIni.set("RELOADSCHEDULE_LOG","TIME2",(ciULong)eventKey.getTime());
				aIni.set("RELOADSCHEDULE_LOG","BY2",by.c_str());
			}
			return;		
		}

		ciBoolean done1=ciFalse;
		ciBoolean done2=ciFalse;

		
		// 현재 브라우저가 떠 있으며,  그 브라우저가 현재 programId 와 동일한 프로그램 ID
		// 를 돌리고 있다면, return 한다.
		
		if(	side & 1 ) {
			ciString binary="UTV_brwClient2.exe";
			unsigned long pid = scratchUtil::getInstance()->getPid(binary.c_str());
			if(pid) { // 현재 기동중이다.
				HWND brwHwnd = scratchUtil::getInstance()->getWHandle(pid);
				if(brwHwnd){
					ciString currentArgValue;
					if(scratchUtil::getInstance()->extractProcessCommandLine(binary.c_str(), currentArgValue)){
						ciString currentHost;
						installUtil::getInstance()->getHostValue(currentArgValue.c_str(), ciFalse, currentHost);
						if(currentHost == programId){
							ciDEBUG(1,("%s program is running now, maybe scheduleChangedEvent is applied already", programId.c_str()));
							side = side & 2;  // 1번 사이드를 무력화 시킨다.
							done1=ciTrue;
						}
					}else{
						ciWARN(("extractProcessCommandLine(%s) failed", binary.c_str()));
					}
				}
			}
		}
		if(	side & 2 ) {
			ciString binary="UTV_brwClient2_UBC1.exe";
			unsigned long pid = scratchUtil::getInstance()->getPid(binary.c_str());
			if(pid) { // 현재 기동중이다.
				HWND brwHwnd = scratchUtil::getInstance()->getWHandle(pid);
				if(brwHwnd){
					ciString currentArgValue;
					if(scratchUtil::getInstance()->extractProcessCommandLine(binary.c_str(), currentArgValue)){
						ciString currentHost;
						installUtil::getInstance()->getHostValue(currentArgValue.c_str(), ciFalse, currentHost);
						if(currentHost == programId){
							ciDEBUG(1,("%s program is running now, maybe scheduleChangedEvent is applied already",programId.c_str()));
							side = side & 1;  // 2번 사이드를 무력화 시킨다.
							done2=ciTrue;
						}
					}else{
						ciWARN(("extractProcessCommandLine(%s) failed", binary.c_str()));
					}
				}
			}
		}


		char refreshInfo[1024];
		memset(refreshInfo,0x00,sizeof(refreshInfo));
		sprintf(refreshInfo,"\t\t<ad name=\"%s\" desc=\"\" sitename=\"%s\" networkuse=\"1\" />\n"
			, programId.c_str(),siteId.c_str());

		ciBoolean sent_refresh = ciFalse;
		// 앞뒷면이냐에 따라, 해당 브라우져의 옵션을 고치고

		getMonitorOnOff(programId.c_str());
		if(monitorOff){
			int failCounter=0;
			for(int i=0;i<3;i++){
				if(!ProjectorWrapper::getInstance()->projectorOff()) {
					ciDEBUG(1,("projector off failed, try once again"));
					failCounter++;
					::Sleep(1000);
				}else{
					break;
				}
			}
		}
		if(monitorOn){
			int failCounter=0;
			for(int i=0;i<3;i++){
				if(!ProjectorWrapper::getInstance()->projectorOn()) {
					ciDEBUG(1,("projector on failed, try once again"));
					failCounter++;
					::Sleep(1000);
				}else{
					break;
				}
			}
		}

		if(	side & 1 ) {
			// 이부분은 브라우저가 완전히 스스로를 나타낸 다음에 해야하므로 브라우저에 넘겨야함.
			installUtil::getInstance()->setChannelName(0,programId.c_str(),siteId.c_str());
			// +replace 옵션을 줘서 부라우저를 구동한다.
			
			//skpark 2010.4.13 firmwareView 화면을 refresh 시킨다.
			if(!sent_refresh) {
				scratchUtil::getInstance()->socketAgent("127.0.0.1",14007,"refresh",refreshInfo);
				sent_refresh = ciTrue;
			}
			
			if(!installUtil::getInstance()->runBrowser(1,programId.c_str(),siteId.c_str(),"+replace",true)){
				ciERROR(("browser(%d) run failed", 0));
				//installUtil::getInstance()->setChannelName(0,programId.c_str(),siteId.c_str());
			}else{
				done1=ciTrue;
			}
			/*
			for(int i=0;i<20;i++){
				::Sleep(100);
				HWND brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2.exe");
				if(brwHwnd){
					COPYDATASTRUCT appInfo;
					appInfo.dwData = 900;  // ShowWindow
					appInfo.lpData = (char*)"";
					appInfo.cbData = 1;
					//::PostMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
					::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
					ciDEBUG(1,("UTV_BRW ShowWindow SendMessage"));
					scratchUtil::getInstance()->getFocus(brwHwnd);
					break;
				}
			}
			*/
		}
		if(	side & 2 ) {
			// 이부분은 브라우저가 완전히 스스로를 나타낸 다음에 해야하므로 브라우저에 넘겨야함.
			installUtil::getInstance()->setChannelName(1,programId.c_str(),siteId.c_str());
			// +replace 옵션을 줘서 부라우저를 구동한다.
			
			//skpark 2010.4.13  firmwareView 화면을 refresh 시킨다.
			if(!sent_refresh) {
				scratchUtil::getInstance()->socketAgent("127.0.0.1",14007,"refresh",refreshInfo);
				sent_refresh = ciTrue;
			}
			
			if(!installUtil::getInstance()->runBrowser(2,programId.c_str(),siteId.c_str(),"+replace", true)){
				ciERROR(("browser(%d) run failed", 1));
				//installUtil::getInstance()->setChannelName(1,programId.c_str(),siteId.c_str());
			}else{
				done2=ciTrue;
			}
			/*
			for(int i=0;i<20;i++){
				::Sleep(100);
				HWND brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2_UBC1.exe");
				if(brwHwnd){
					COPYDATASTRUCT appInfo;
					appInfo.dwData = 900;  // ShowWindow
					appInfo.lpData = (char*)"";
					appInfo.cbData = 1;
					//::PostMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
					::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
					ciDEBUG(1,("UTV_BRW ShowWindow SendMessage"));
					scratchUtil::getInstance()->getFocus(brwHwnd);
					break;
				}
			}
			*/
		}

		ubcMutexConfig aIni("UBCBrowser");
		aIni.set("RELOADSCHEDULE_LOG","TIME",(ciULong)eventKey.getTime());
		aIni.set("RELOADSCHEDULE_LOG","NAME",eventName.c_str());
		aIni.set("RELOADSCHEDULE_LOG","PROGRAM",programId.c_str());
		aIni.set("RELOADSCHEDULE_LOG","SIDE",side);
		//skpark add 20100531
		if(done1) {
			aIni.set("RELOADSCHEDULE_LOG","PROGRAM1",programId.c_str());
			aIni.set("RELOADSCHEDULE_LOG","TIME1",(ciULong)eventKey.getTime());
			aIni.set("RELOADSCHEDULE_LOG","BY1",by.c_str());
		}
		if(done2) {
			aIni.set("RELOADSCHEDULE_LOG","PROGRAM2",programId.c_str());
			aIni.set("RELOADSCHEDULE_LOG","TIME2",(ciULong)eventKey.getTime());
			aIni.set("RELOADSCHEDULE_LOG","BY2",by.c_str());
		}

		return;
	}
	ciERROR(("%s is invalid event", eventName.c_str()));
	return ;
}

ciBoolean
reloadEventHandler::_isAlreadyDone(const char* requestedProgramId, ciULong requestedTime, int side, 
								   ciULong todayStartTime)
{
	ciDEBUG(1,("_isAlreadyDone(requested programId=%s, time=%ld, todayStartTime=%ld)", 
		requestedProgramId, requestedTime, (ciULong)todayStartTime));

	ubcMutexConfig aIni("UBCBrowser");
	ciString programId;
	ciULong	eventKey=0L;
	if(side & 1) {
		aIni.get("RELOADSCHEDULE_LOG","PROGRAM1",programId);
		aIni.get("RELOADSCHEDULE_LOG","TIME1",(ciULong)eventKey);
	}
	if(side & 2) {
		aIni.get("RELOADSCHEDULE_LOG","PROGRAM2",programId);
		aIni.get("RELOADSCHEDULE_LOG","TIME2",(ciULong)eventKey);
	}

	ciDEBUG(1,("last programId=%s", programId.c_str()));
	ciDEBUG(1,("last Time=%ld", eventKey));

	if(programId == requestedProgramId){
		// 같다!!! 더 이상 reloadEvent 를 진행하지 않는다.
		// skpark 2012.5.14  같다 하더라도, 이 경우는 방송계획이 새로 튄 경우이므로
		// 브라우저가 살아있지 않다면,  새로 구동해야 한다.
		installUtil* aUtil = installUtil::getInstance();
		ciString curSchedule;
		aUtil->getCurrentHost(side, curSchedule);
		if(!curSchedule.empty()){
			ciDEBUG(1,("%s already done", requestedProgramId));
			return ciTrue;
		}
		ciWARN(("NO CURRENT BROWSER RUNNING !!!(requested=%s)", requestedProgramId));
	}
	/*
	// startTime 이후에 다른 방법으로 변경이 일어났다면 그 변경을 존중한다..
	if( eventKey > todayStartTime ){
		ciDEBUG(1,("%s already ignored", requestedProgramId));
		return ciTrue;
	}
	*/
	// 이곳은 언제나 PLANTIMER 의 경우만 온다.
	time_t now = time(0);
	if( now - eventKey > 60 ){
		ciDEBUG(1,("requestedProgramId is newer than plan over 60 sec !!!"));
		ciDEBUG(1,("program changed(%s-->%s)", programId.c_str(), requestedProgramId));
	}else{
		ciDEBUG(1,("requestedProgramId is not newer than plan over 60 sec !!!"));
		ciDEBUG(1,("program not changed(%s-->%s)", programId.c_str(), requestedProgramId));
		return ciTrue;
	}

	ciDEBUG(1,("%s is not done", requestedProgramId));
	return ciFalse;
}


void		
reloadEventHandler::getMonitorOnOff(const char* programId)
{
	ciString strPath = _appPath;
	strPath += "config\\";
	strPath += programId;
	strPath += ".ini";
	this->monitorOn = (bool)GetPrivateProfileInt("Host", "monitorOn", 0, strPath.c_str());
	this->monitorOff = (bool)GetPrivateProfileInt("Host", "monitorOff", 0, strPath.c_str());
}


