//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FWV.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FWV_DIALOG                  102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     130
#define IDI_TRAY                        131
#define IDR_MENU1                       132
#define IDR_TRAY_MENU                   132
#define IDB_BITMAP_CCSERVER             133
#define IDB_BITMAP3                     134
#define IDB_BITMAP_CCCLIENT             134
#define IDC_FLASH_FRAME                 1000
#define IDC_STATIC_LOGO                 1041
#define IDC_STATIC_CACHE                1042
#define IDC_STATIC_CACHE2               1043
#define ID_TRAY_OPEN                    32771
#define ID_TRAY_CLOSE                   32772
#define ID__32801                       32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
