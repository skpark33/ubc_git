#pragma once

#include <afxsock.h>

// CListenSocket 명령 대상입니다.

class CListenSocket : public CAsyncSocket
{
public:
	CListenSocket();
	virtual ~CListenSocket();

	virtual void OnAccept(int nErrorCode);

	void	Init(HWND hParentWnd, UINT nPort=14006);
	BOOL	StartListen();

protected:
	HWND	m_hParentWnd;
	UINT	m_nPort;
};


