#pragma once

#include <afxmt.h>
#include <afxsock.h>
#include <afxtempl.h>


#define		WM_CLOSE_DATA_SOCKET		(WM_USER+12345)



// CDataSocket 명령 대상입니다.

class CDataSocket : public CAsyncSocket
{
protected:
	static	CCriticalSection		_lock;
	static	CArray<CDataSocket*>	_list;

public:
	static	bool	AddInstance(CDataSocket* obj);
	static	bool	RemoveInstance(CDataSocket* obj);
	static	bool	SendToAll(const char* data);



public:
	CDataSocket(HWND hParentWnd);
	virtual ~CDataSocket();
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

protected:
	HWND	m_hParentWnd;

	bool	keyboardSimulator(const char* command);

	static CString	ConvertString(const char* str);
};


