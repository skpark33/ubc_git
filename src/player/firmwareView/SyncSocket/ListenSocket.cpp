// ListenSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ListenSocket.h"
#include "DataSocket.h"

#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciDebug.h>
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchUtil.h"
#include "common/libInstall/installUtil.h"

ciSET_DEBUG(10, "CListenSocket");


// CListenSocket

CListenSocket::CListenSocket()
{
	ciDEBUG(1, ("CListenSocket()") );
}

CListenSocket::~CListenSocket()
{
	ciDEBUG(1, ("~CListenSocket()") );
}


// CListenSocket 멤버 함수

void CListenSocket::OnAccept(int nErrorCode)
{
	ciDEBUG(1, ("OnAccept(%d)", nErrorCode) );

	if( nErrorCode != 0 )
	{
		// error
		ciWARN( ("nErrorCode=%d", nErrorCode) );
		return;
	}

	CDataSocket* tmp = new CDataSocket(m_hParentWnd);

	if( !Accept(*tmp) )
	{
		// error
		DWORD err_code = ::GetLastError();
		ciWARN( ("fail to Accept !!! (%u)", err_code) );
		return;
	}

	// success
	CDataSocket::AddInstance(tmp);

	static ciString license_key = "";
	if( license_key.length()==0 )
	{
		ciString mac;
		scratchUtil::getInstance()->readAuthFile(license_key, mac);
	}

	ciDEBUG(1,("Send LicenseKey(%s)", license_key.c_str()));
	tmp->Send(license_key.c_str(), license_key.length()+1);

	CAsyncSocket::OnAccept(nErrorCode);
}

void CListenSocket::Init(HWND hParentWnd, UINT nPort)
{
	ciDEBUG(1,("Init(%u,%u)", hParentWnd, nPort));
	m_hParentWnd = hParentWnd;
	m_nPort = nPort;
}

BOOL CListenSocket::StartListen()
{
	ciDEBUG(1,("StartListen()"));
	if( !Create(m_nPort))
	{
		// error
		DWORD err_code = ::GetLastError();
		ciWARN(("fail to Create !!! (%u)", err_code));
		return FALSE;
	}

	if( !Listen() )
	{
		// error
		DWORD err_code = ::GetLastError();
		ciWARN(("fail to Listen !!! (%u)", err_code));
		return FALSE;
	}

	return TRUE;
}
