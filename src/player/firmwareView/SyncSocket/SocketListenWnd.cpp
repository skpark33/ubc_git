// SocketListenWnd.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "SocketListenWnd.h"
#include "DataSocket.h"

#include <ci/libDebug/ciDebug.h>

ciSET_DEBUG(10, "CSocketListenWnd");


// CSocketListenWnd

IMPLEMENT_DYNAMIC(CSocketListenWnd, CWnd)

CSocketListenWnd::CSocketListenWnd()
{
	ciDEBUG(1, ("CSocketListenWnd()") );
}

CSocketListenWnd::~CSocketListenWnd()
{
	ciDEBUG(1, ("~CSocketListenWnd()") );
}


BEGIN_MESSAGE_MAP(CSocketListenWnd, CWnd)
	ON_WM_PAINT()
	ON_MESSAGE(WM_CLOSE_DATA_SOCKET, OnCloseDataSocket)
END_MESSAGE_MAP()



// CSocketListenWnd 메시지 처리기입니다.


BOOL CSocketListenWnd::Init(CWnd* pParentWnd)
{
	ciDEBUG(1, ("Init()") );

	if(!Create(NULL, "socket_wnd", WS_CHILD, CRect(0,0,0,0), pParentWnd, 0xfefd))
	{
		ciWARN( ("Fail to create !!!") );
		return FALSE;
	}

	m_listensocket.Init(GetSafeHwnd());
	return m_listensocket.StartListen();
}

void CSocketListenWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	CRect client_rect;
	GetClientRect(client_rect);

	dc.FillSolidRect(client_rect, RGB(0,0,0));
}

LRESULT CSocketListenWnd::OnCloseDataSocket(WPARAM wParam, LPARAM lParam)
{
	ciDEBUG(1, ("OnCloseDataSocket(%d,%d)", wParam, lParam) );

	CDataSocket* data_sock = (CDataSocket*)wParam;
	delete data_sock;

	return 0;
}
