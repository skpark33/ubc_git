// DataSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DataSocket.h"

#include <ci/libDebug/ciDebug.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <common/libInstall/installUtil.h>
#include <common/libScratch/scratchUtil.h>
#include <common/libCommon/ubcUtil.h>
#include <common/libCommon/ubcIni.h>

ciSET_DEBUG(10, "CDataSocket");


// CDataSocket

CDataSocket::CDataSocket(HWND hParentWnd)
{
	ciDEBUG(1, ("CDataSocket()") );
	m_hParentWnd = hParentWnd;
}

CDataSocket::~CDataSocket()
{
	ciDEBUG(1, ("~CDataSocket()") );
	CDataSocket::RemoveInstance(this);
}


// CDataSocket 멤버 함수

void CDataSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	ciDEBUG(1, ("OnClose(%d)", nErrorCode) );

	::PostMessage(m_hParentWnd, WM_CLOSE_DATA_SOCKET, (WPARAM)this, NULL);

	CAsyncSocket::OnClose(nErrorCode);
}

CString	CDataSocket::ConvertString(const char* str)
{
	char buf[1024] = {0};
	for(int i=0; (*str)!=NULL; i++,str++)
	{
		char ch = (*str);
		if( 32<=ch && ch<=126 )
		{
			buf[i] = ch;
		}
		else
		{
			char tmp[16] = {0};
			sprintf(tmp, "%%%X", ch);
			strcat(buf, tmp);
			i += strlen(tmp);
		}
	}

	return buf;
}

void CDataSocket::OnReceive(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	ciDEBUG(1, ("OnReceive(%d)", nErrorCode) );

	char buf[1024] = {0};
	int nRead = Receive(buf, 1024);
	ciDEBUG(1, ("Receive(%d)=(%s)", nRead, ConvertString(buf)) );

	switch (nRead)
	{
	case 0:
	case SOCKET_ERROR:
		// read nothing -> socket is closed
		{
			::PostMessage(m_hParentWnd, WM_CLOSE_DATA_SOCKET, (WPARAM)this, NULL);
		}
		break;

	default:
		ciString command = buf;
		ciStringUtil::stringReplace(command, "\n", "");
		keyboardSimulator(command.c_str());
		break;
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

#define	BUF_LEN	1024

bool CDataSocket::keyboardSimulator(const char* command)
{
	ciDEBUG(1, ("keyboardSimulator(%s)", command) );
	//char buf[BUF_LEN+1] = {0};

	//list<string>  aList,  bList;

	//int j = 0;
	int len = strlen(command);
	//for(int i=0; i<len; i++)
	//{
	//	if( command[i] == '+' )
	//	{
	//		buf[j] = 0;
	//		if( strlen(buf) )
	//		{
	//			aList.push_back(buf);
	//		}
	//		memset(buf, 0x00, BUF_LEN+1);
	//		j=0;
	//		continue;
	//	}
	//	else
	//	{
	//		buf[j] = command[i];
	//		j++;
	//	}
	//}
	//aList.push_back(buf);

	//scratchUtil::getInstance()->getFocus("UTV_BRW");

	//list<string>::iterator itr;
	//for(itr=aList.begin(); itr!=aList.end(); itr++)
	//{
	//	//_kbdEvent((*itr).c_str(),0);				//skpark 2010 3.24 임시로 막음
	//	bList.push_front((*itr));
	//}
	////Sleep(1000);
	//list<string>::iterator jtr;
	//for(jtr=bList.begin(); jtr!=bList.end(); jtr++)
	//{
	//	//_kbdEvent((*jtr).c_str(),KEYEVENTF_KEYUP);  //skpark 2010 3.24 임시로 막음
	//}

	if(stricmp(command,"ctrl+x")==0)
	{
		SendToAll("E");

		//system("ubckill.bat");
		scratchUtil::getInstance()->createProcess("ubckill.bat", "");
		scratchUtil::getInstance()->showTaskbar();
		exit(1);
		return true;
	}

	if(strnicmp(command,"ctrl+",5)==0)
	{
		if(SendToAll(command+5)==false)
		{
			ciDEBUG(1, ("already send-data !!! (%s)", command) );
			return true;
		}
	}

	HWND brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2.exe");
	if( brwHwnd == NULL )
	{
		ciWARN( ("not found UTV_brwClient2.exe") );
		brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2_UBC1.exe");
	}
	if( brwHwnd )
	{
		ciDEBUG(1, ("WM_COPYDATA(%s)", command) );
		COPYDATASTRUCT appInfo;
		appInfo.dwData = UBC_WM_KEYBOARD_EVENT;  // 1000
		appInfo.lpData = (char*)command;
		appInfo.cbData = len+1;
		//::PostMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
		::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
	}

	return true;
}

CCriticalSection		CDataSocket::_lock;
CArray<CDataSocket*>	CDataSocket::_list;

bool CDataSocket::AddInstance(CDataSocket* obj)
{
	_lock.Lock();
	_list.Add(obj);
	_lock.Unlock();
	ciDEBUG(1, ("AddInstance(%d)", _list.GetCount()) );
	return true;
}

bool CDataSocket::RemoveInstance(CDataSocket* obj)
{
	_lock.Lock();
	for(int i=0; i<_list.GetCount(); i++)
	{
		CDataSocket* tmp = (CDataSocket*)_list.GetAt(i);
		if(tmp==obj)
		{
			_list.RemoveAt(i);
			_lock.Unlock();
			ciDEBUG(1, ("RemoveInstance(%d)", _list.GetCount()) );
			return true;
		}
	}
	_lock.Unlock();
	ciWARN( ("RemoveInstance(%d)", _list.GetCount()) );
	return false;
}

bool CDataSocket::SendToAll(const char* data)
{
	ciDEBUG(1, ("SendToAll(%s)", ConvertString(data)) );
	_lock.Lock();

	static CString str_prev_data = "";
	if( str_prev_data == data )
	{
		_lock.Unlock();
		ciDEBUG(1, ("already send-data !!! (%s)", str_prev_data) );
		return false;
	}
	str_prev_data = data;

	for(int i=0; i<_list.GetCount(); i++)
	{
		CDataSocket* tmp = (CDataSocket*)_list.GetAt(i);
		tmp->Send(data, strlen(data)+1);
	}

	_lock.Unlock();
	return true;
}
