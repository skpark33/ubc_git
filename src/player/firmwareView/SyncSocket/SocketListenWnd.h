#pragma once


#include "ListenSocket.h"


// CSocketListenWnd

class CSocketListenWnd : public CWnd
{
	DECLARE_DYNAMIC(CSocketListenWnd)

public:
	CSocketListenWnd();
	virtual ~CSocketListenWnd();

	BOOL	Init(CWnd* pParentWnd);

protected:
	DECLARE_MESSAGE_MAP()

	CListenSocket	m_listensocket;

public:
	afx_msg void OnPaint();
	afx_msg LRESULT OnCloseDataSocket(WPARAM wParam, LPARAM lParam);

};


