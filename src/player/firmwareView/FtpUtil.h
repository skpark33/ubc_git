
#ifndef __TFTP_DEF__
#define __TFTP_DEF__

#include "libFTPClient/FTPClient.h"
using namespace nsFTP;

class FtpUtil
{
public:
	FtpUtil(LPCSTR user, LPCSTR pass, LPCSTR address, unsigned short port = 21);
	virtual ~FtpUtil();

public:
	BOOL Connect();
	void Disconnect();
	BOOL FileDownload(LPCSTR remoteFile, LPCSTR localFile, LPCSTR localDir);
	BOOL FileUpload(LPCSTR localFile, LPCSTR remoteFile);
	void Passive(BOOL passiveFlag = true);

	BOOL ChangeWorkingDirectory(LPCSTR pathname);
	BOOL _ChangeWorkingDirectory(LPCSTR pathname); // mkdir & cd
	BOOL FileFind(LPCSTR srcname);
	long FileSize(LPCSTR srcname);
	void AttachObserver(CFTPClient::CNotification* pObserver);
	void DetachObserver(CFTPClient::CNotification* pObserver);

public:
	CFTPClient*       m_pFTPClient;

	CString m_strAddress;
	CString m_strName;
	CString m_strPassword;
	unsigned short m_port;

	BOOL m_passiveFlag;
};


#endif
