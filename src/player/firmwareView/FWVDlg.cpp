#include "stdafx.h"
#include "windows.h" //skpark USB_Device
#include "dbt.h" //skpark USB_Device
#include "MsgBox.h"
#include "FWV.h"
#include "FWVDlg.h"
#include "tcpSocketSession.h"

using namespace std;
#include <common/libScratch/scratchUtil.h>
#include <common/libInstall/installUtil.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciEnv.h>
#include "common/libCommon/ubcIni.h"
#include "common/libCommon/utvMacro.h"
#include "libAGTClient/AGTDownloadResultClient.h"
#include "common/libPlan/planTimer.h"
#include "reloadEventHandler.h"

#include "libTimeSyncClient/TimeSyncClient.h"
#include "libTimeSyncServer/TimeSyncServer.h"
#include "SyncSocket/DataSocket.h"

#ifdef _FELICA_
#include <libFelicaUtil/felicaUtil.h>
#endif

ciSET_DEBUG(10,"CFWVDlg");

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define EXE_PATH _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\")

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);   

protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


CFWVDlg::CFWVDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFWVDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pFlash = 0;
	disable_area_y = 0;
	isMinimize = FALSE;
	_autoImport = FALSE;
}

void CFWVDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FLASH_FRAME, m_ctrlPictureFrame);
	DDX_Control(pDX, IDC_STATIC_LOGO, logoVar);
	DDX_Control(pDX, IDC_STATIC_CACHE, cacheVar);
	DDX_Control(pDX, IDC_STATIC_CACHE2, cacheVar2);
}

BEGIN_MESSAGE_MAP(CFWVDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
	ON_WM_ERASEBKGND()
	ON_WM_DESTROY()
	//ON_WM_CLOSE()
	ON_WM_LBUTTONUP()
	ON_WM_COPYDATA()
	ON_MESSAGE(UM_TRAYICON,TrayIconMessage)
	ON_COMMAND(ID_TRAY_OPEN, OnTrayOpen)
	ON_COMMAND(ID_TRAY_CLOSE, OnTrayClose)
	// skpark USB_Device
	ON_WM_DEVICECHANGE()
END_MESSAGE_MAP()



BOOL CFWVDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}


	SetIcon(m_hIcon, TRUE);			
	SetIcon(m_hIcon, FALSE);		

	ciDEBUG(1,("SetWindowText"));

	this->SetWindowText("UBC FirmwareView");
	
	if(ciArgParser::getInstance()->isSet("+minimize")) {
		isMinimize = TRUE;
		if(ciArgParser::getInstance()->isSet("+no_icon")){
			//skpark 2014.05.29 +no_icon이 있으면 아이콘트레이에는 넣지않는다.
		}else{
			_initTray();
		}
		//this->ShowWindow(SW_HIDE);
		scratchUtil::getInstance()->showTaskbar(true);

		// skpark 2012.6.18 FirmwareView 를 Hide 하는 경우라면, stater 도 하이드 한다.
		HWND sttHwnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
		if(sttHwnd){
			ciDEBUG(1, ("STARTER NOT HIDE"));
			CWnd* sttCWnd = CWnd::FromHandle(sttHwnd);
			if(sttCWnd)  {
				ciDEBUG(1, ("STARTER HIDE"));
				sttCWnd->ShowWindow(SW_HIDE);
			}
		}else{
			ciDEBUG(1,("STARTER NOT FOUND"));
		}

	}else{
		scratchUtil::getInstance()->showTaskbar(false);
	}
	ShowWindow(SW_MAXIMIZE);

	Invalidate();
	
	ciDEBUG(1,("before play"));

	if(!ciArgParser::getInstance()->isSet("+static_hostname")) {
		HostNameIdentify();
	}

	CBitmap a_bmp;
	VERIFY(a_bmp.LoadBitmap(IDB_BITMAP1));
	BITMAP bm ;
	VERIFY(a_bmp.GetObject(sizeof(bm), (LPVOID)&bm));
	CRect rectParent;
	GetClientRect(&rectParent);
	int height = rectParent.Height();
	int width = rectParent.Width();
	int x = width-bm.bmWidth-5;
	int y = height-bm.bmHeight-5;

	disable_area_y = y;
	//logoVar.SetWindowPos(NULL, x,y, bm.bmWidth ,bm.bmHeight , SWP_NOMOVE | SWP_NOZORDER);
	logoVar.MoveWindow(x,y,bm.bmWidth,bm.bmHeight);

	//skpark show ContentsCacheServer
	_showCacheServerImage();

	ciDEBUG(1,("EDITION=%s", m_edition));

	// skpark USB_Device
	_initDeviceChangeInterface();

	Play();

	ciDEBUG(1,("after play"));

	/*
	if(!getFocus("UTV_BRW")){
		// 브라우저가 떠있지 않다.
		// 이시점에 혹시  Auto 로 설정된 브라우저가 없다면 UBCReady 를 죽여야한다.
		if(!installUtil::getInstance()->autoBrowserExist()){
			scratchUtil::getInstance()->ExitUBCReady(this->m_hWnd);
		}
	}
	*/

	felicaServer();

	//AfxBeginThread(CFWVDlg::WaitForWindowCreated, GetSafeHwnd());
	AfxBeginThread(CFWVDlg::WaitForWindowCreated, this);
	ciDEBUG(1,("init complete"));

	// time sync
	ubcConfig aIni("UBCVariables");
	ciLong time_sync_server = 0;
	aIni.get("ROOT","TIME_SYNC_SERVER", time_sync_server);
	ciLong time_sync_interval = 0;
	aIni.get("ROOT","TIME_SYNC_INTERVAL", time_sync_interval);
	ciLong time_sync_port = 0;
	aIni.get("ROOT","TIME_SYNC_PORT", time_sync_port);

	if( time_sync_server > 0 )
	{
		if( time_sync_interval > 0 ) CTimeSyncServer::getInstance()->SetSyncInterval(time_sync_interval);
		if( time_sync_port > 0 ) CTimeSyncServer::getInstance()->SetSyncUDPPort(time_sync_port);

		CTimeSyncServer::getInstance()->StartSync();
	}
	else if( time_sync_server == 0 )
	{
		if( time_sync_port > 0 ) CTimeSyncClient::getInstance()->SetSyncUDPPort(time_sync_port);

		if( !CTimeSyncClient::getInstance()->StartSync() )
		{
			ciERROR(("Time Sync Error !!!"));
		}
	}
	else if( time_sync_server < 0 )
	{
		// do not work TimeSync-Client/Server
	}

	// <!-- 골프장전용 (2016.08.11)
	if(ciArgParser::getInstance()->isSet("+sync_process"))
	{
		if(!m_socket_wnd.Init((CWnd*)this))
		{
			ciERROR(("Fail to create SOCKET_LISTEN_WND !!!"));
		}
	}
	// -->
	return TRUE; 
}


BOOL CFWVDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	ciDEBUG(1,("OnCopyData processMsg %d", pCopyDataStruct->dwData));

	switch(pCopyDataStruct->dwData)
	{		
	case UBC_WM_DOWNLOAD_EVENT: // Download data changed
		{
			ciDEBUG(1,("OnCopyData processMsg 2000 - download progress msg"));
			/* skpark emergent comment out */
			DownloadProgressMsg* aMsg = (DownloadProgressMsg*)(LPCSTR)pCopyDataStruct->lpData;
			if(AGTDownloadResultClient::getInstance()->processMsg(aMsg)){
				ciDEBUG(1,("processMsg SUCCEED 2000"));
			}else{
				ciERROR(("processMsg FAILED"));
			}
			
			break;
		}
	case UBC_WM_BRW_INFO_EVENT: // BRW_INFO
		{
			ciDEBUG2(1,("OnCopyData processMsg 30000 - brw Info"));
			char buffer[1024];
			memset(buffer,0x00,1024);
			memcpy(buffer,(const char*)(LPCSTR)pCopyDataStruct->lpData, pCopyDataStruct->cbData);
			if(planManager::getInstance()->putBRWInfo(buffer)){
				ciDEBUG(1,("processMsg SUCCEED 3000"));
			}else{
				ciERROR(("processMsg FAILED"));
			}
			break;
		}
		/*
	case WM_CMD_DOWNLOAD_END: // Download data changed
		{
			ciDEBUG(1,("OnCopyData processMsg WM_CMD_DOWNLOAD_END"));
			break;
		}
		*/
	// <!-- 골프장전용 (2016.08.11)
	case UBC_WM_TEMPLATE_SHORTCUT:
		{
			char buf[1024];
			memset(buf,0x00,1024);
			memcpy(buf,(const char*)(LPCSTR)pCopyDataStruct->lpData, pCopyDataStruct->cbData);
			if(strnicmp(buf,"ctrl+",5)==0)
				CDataSocket::SendToAll(buf+5);
			else
				CDataSocket::SendToAll(buf);
		}
		break;
	// -->
	default:
		break;
	}

	ciDEBUG(1,("FWVDlg::OnCopyData() end"));
	BOOL retval = CDialog::OnCopyData(pWnd, pCopyDataStruct);
	ciDEBUG(1,("FWVDlg::OnCopyData() end"));
	return retval;
}

void
CFWVDlg::felicaServer()
{
#ifdef _FELICA_
	felicaUtil::getInstance()->felicaServerStart(this->m_hWnd);
	felicaUtil::getInstance()->setURL("http://www.daum.net/");
#endif
}

UINT 
CFWVDlg::WaitForWindowCreated(LPVOID pParam)
{
	ciDEBUG(1,("WaitForWindowCreated start"));
	//HWND hWnd = (HWND)pParam;
	CFWVDlg* pWnd = (CFWVDlg*)pParam;
	HWND hWnd = pWnd->GetSafeHwnd();

	if(hWnd)// && ::IsWindow(hWnd))
	{
		while(::WaitForSingleObject(hWnd, 0) == WAIT_TIMEOUT)
		{
			ciDEBUG(1,("WaitForWindowCreated timeout"));
		}//if
		ciDEBUG(1,("WaitForWindowCreated passed"));

		for(int i=0;i<2;i++){

			HWND brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2.exe");
			if(brwHwnd==0){
				brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2_UBC1.exe");
			}
			if(brwHwnd){
				ciDEBUG(1,("BROWSER already Started"));
				COPYDATASTRUCT appInfo;
				appInfo.dwData = 900;  // ShowWindow
				appInfo.lpData = (char*)"";
				appInfo.cbData = 1;
				//::PostMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
				ciDEBUG(1,("UTV_BRW ShowWindow SendMessage"));
				scratchUtil::getInstance()->getFocus(brwHwnd);
				//return 1;
			}
			::Sleep(60);
		}

		//CFWVDlg* pWnd = (CFWVDlg*)CWnd::FromHandle(hWnd);
		::Sleep(1000);
		if(pWnd->isMinimize){
			if(ciArgParser::getInstance()->isSet("+no_icon")){
				pWnd->ShowWindow(SW_MINIMIZE);
			}else{
				pWnd->ShowWindow(SW_HIDE);
			}
		}

	}else{
		ciWARN(("window handle is null"));
	}
	return 0;
}

void CFWVDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}


void CFWVDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); 

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		
		CPaintDC dc(this);
		CRect client_rect;
		GetClientRect(client_rect);
		dc.FillSolidRect(client_rect, RGB(0, 0, 0));	
		CDialog::OnPaint();
	}


}


HCURSOR CFWVDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CFWVDlg::Play()
{
	ciDEBUG(1,("Play()"));

	static int id = 0xd000;

	ciDEBUG(1,("new CShockwaveFlash()"));
	m_pFlash = new CShockwaveFlash();

	CRect rectParent;
	GetClientRect(&rectParent);
	int height = rectParent.Height();
	int width = rectParent.Width();

	int x = (width-768)/2;
	int y = (height-600)/2;

	CRect client_rect;
	//GetClientRect(&client_rect);

	m_ctrlPictureFrame.MoveWindow(x, y, 768, 600);
	m_ctrlPictureFrame.GetClientRect(&client_rect);
	
	ciDEBUG(1,("m_pFlash->Create()"));
	BOOL ret = m_pFlash->Create("Flash", WS_CHILD | WS_VISIBLE, client_rect, &m_ctrlPictureFrame, id--);

	if(ret == FALSE)
	{
		MessageBoxA("Fail to create Flash control !!!");
		return false;
	}

	CString path = EXE_PATH;

	ciString customerInfo;
	ciBoolean isWedding=ciFalse;

	ubcConfig aIni("UBCVariables");
	aIni.get("ROOT","USE_GUESTBOOK",isWedding);
	if(isWedding){
		path.Append("flash\\Firmware_wedding.swf");
	}else{
		aIni.get("CUSTOMER_INFO","NAME",customerInfo);
		if(customerInfo=="NARSHA"){
			path.Append("flash\\Firmware_v7_narsha.swf");
		}else{
			ciString lang = scratchUtil::getInstance()->getLang();
			if(!lang.empty()){
				ciString file = "flash\\Firmware_v7_" + lang + ".swf";
				path.Append(file.c_str());
			}else{
				path.Append("flash\\Firmware_v7.swf");
			}
		}
	}
	ciDEBUG(1,("m_pFlash->LoadMovie(%s)", path));
	m_pFlash->LoadMovie(0, path);
	ciDEBUG(1,("m_pFlash->ShowWindow()"));
	m_pFlash->ShowWindow(SW_SHOW);

	ciDEBUG(1,("m_pFlash->ShowWindow()"));
	setEdition();

	ciDEBUG(1,("~Play()"));

	return true;
}

BOOL CFWVDlg::setEdition()
{
	ciString ip = _getIp();
	ip += ".";
	ciString version = _getVersion();
	ciString hostname = _getComputerName();

	ciDEBUG(1,("VERSION=%s", version.c_str()));
	ciDEBUG(1,("IP=%s", ip.c_str()));
	ciDEBUG(1,("HOSTNAME=%s", hostname.c_str()));
	ciDEBUG(1,("EDITION=%s", m_edition));

	try{
		CString setEdition = "<invoke name=\"setEdition\" returntype=\"xml\"><arguments><string>";
		setEdition += m_edition;
		setEdition += "|";
		setEdition += version.c_str();
		setEdition += "|";
		setEdition += hostname.c_str();
		setEdition += "|";
		setEdition += ip.c_str();
		setEdition += "</string></arguments></invoke>";
		m_pFlash->CallFunction(setEdition);
	}catch(COleDispatchException* ex){
		TCHAR   szCause[255];
		CString strFormatted;
		ex->GetErrorMessage(szCause, 255);
		strFormatted = "The program exited because of this error: ";
		strFormatted += szCause;
		ciERROR((strFormatted));
		return false;
	}
	return true;
}

const char*
CFWVDlg::_getIp()
{
	static ciString ip="";
	for(int i=0;i<3;i++){
		scratchUtil::getInstance()->getIpAddress(ip);
		if(ip!="127.0.0.1" && !ip.empty()){
			break;
		}
		::Sleep(1000);
	}
	return ip.c_str();
}

const char*
CFWVDlg::_getComputerName()
{
	static ciString name="";

	if(name.empty()){
		if(ciArgParser::getInstance()->isSet("+static_hostname")) {
			ciString mac,edition,hostId;
			if(scratchUtil::getInstance()->readAuthFile(hostId,mac,edition)){
				name = hostId;
			}
		}
	}

	if(name.empty()){
		TCHAR  szTemp[MAX_PATH] = { 0x00 };
		DWORD  dwBufSize = MAX_PATH;
		GetComputerNameEx(ComputerNamePhysicalDnsHostname, szTemp, &dwBufSize);
		name = szTemp;
	}
	return name.c_str();
}
const char*
CFWVDlg::_getVersion()
{
	static ciString version="";
	if(version.empty()){
		scratchUtil::getInstance()->getVersion(version,false);
	}
	return version.c_str();
}


BOOL CFWVDlg::PreTranslateMessage(MSG* pMsg)
{
#ifdef _FELICA_
	felicaUtil::getInstance()->preTranslateMessage(pMsg);
#endif

	if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		return TRUE;
	}
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_DELETE)
	{
		if(isMinimize){
			this->ShowWindow(SW_HIDE);
		}else{
			this->ShowWindow(SW_MINIMIZE);
		}
		scratchUtil::getInstance()->showTaskbar(true);
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH CFWVDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	return hbr;
}

int 
CFWVDlg::getFocus(const char* appTitle)
{
	HWND hWnd = ::FindWindow(NULL, appTitle); 
	if(hWnd == NULL){
		return 0;
	}

	::SetFocus(hWnd);
	BOOL bRet = ::SetForegroundWindow(hWnd);
	if(!bRet)  {
		return 0;
	}
	//ciDEBUG(1,("set foreground window"));

	int i;
	for(i=0;i<50;i++){
		HWND aWnd = ::GetForegroundWindow();
		if(aWnd == hWnd){
			break;
		}
		Sleep(100);
	}
	if(i==50){
		return 0;
	}
	return 1;
}

BOOL CFWVDlg::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
//	return CDialog::OnEraseBkgnd(pDC);
}

void CFWVDlg::OnDestroy()
{
	ciDEBUG(1,("OnDestroy()"));
	
	scratchUtil::getInstance()->showTaskbar(true);
	
	// skpark USB_Device
	if(_autoImport) {
		if(m_hDevNotify) {
			UnregisterDeviceNotification(m_hDevNotify); 
		}
	}

	
	CDialog::OnDestroy();
	//트래이 리소스 해제 
	NOTIFYICONDATA nid;
	::ZeroMemory(&nid, sizeof(nid));
	nid.cbSize = sizeof(nid);
	nid.hWnd = m_hWnd;
	nid.uID = IDI_TRAY;
	Shell_NotifyIcon(NIM_DELETE,&nid);//삭제플래그를 준다.

	
}
/*
void CFWVDlg::OnClose()
{
	ciDEBUG(1,("OnClose()"));
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	scratchUtil::getInstance()->showTaskbar(true);
	CDialog::OnClose();
}
*/

void CFWVDlg::HostNameIdentify()
{
	// Key 와 HostName 과 NetBiose Name 이 불일치 할경우 일치시킬 것
	// Key 와 UltraVnc.ini 화일의 포트번호가 불일치 할 경우 일치시킬것
	ciString host,mac;
	if(scratchUtil::getInstance()->readAuthFile(host,mac)){
		if(installUtil::getInstance()->hostNameIdentify(host.c_str())){
			// 호스트명이 변경될 경우 리부트한다.
			
			HWND hwind = ::FindWindow(NULL, "UBC FirmwareView");
			FWVMessageBoxH(hwind, "ComputerName has been changed !!!, System will be reboot",5000);
			ciString command = "rebootSystem.bat";
			ciString arg="";
			if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
				ciDEBUG(1,("%s %s succeed", command.c_str(), arg.c_str()));
			}else{
				ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
			}
		}
		if(m_edition!="ENT"){ // port 번호를 바꿔주는 작업은 STD 에서만 한다.
			                // ENT 는 repeater 를 쓰기때문에.
			installUtil::getInstance()->vncPortIdentify(host.c_str());
		}
	}else{
		ciERROR(("READ AUTHENTICATION FAILED"));
	}
}
void CFWVDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	static time_t pre_time = 0;
	static int aCount = 0;

	if( disable_area_y > 0 && disable_area_y - 5 < point.y){
		return;  // 이지역은 눌러봐야 소용없다.
	}

	if(aCount==0){
		pre_time = time(NULL);
		aCount++;

		CDialog::OnLButtonUp(nFlags, point);
		return;
	}

	time_t now = time(NULL);
	if(now - pre_time > 1){
		aCount = 0;

		CDialog::OnLButtonUp(nFlags, point);
		return;
	}

	if( aCount >= 3){
		if(isMinimize){
			this->ShowWindow(SW_HIDE);
		}else{
			this->ShowWindow(SW_MINIMIZE);
		}
		scratchUtil::getInstance()->showTaskbar(true);
		aCount = 0;

		CDialog::OnLButtonUp(nFlags, point);
		return;
	}

	aCount++;
	CDialog::OnLButtonUp(nFlags, point);
}


void CFWVDlg::_initTray()
{
	NOTIFYICONDATA nid; 

	::ZeroMemory(&nid, sizeof(nid));
	nid.cbSize = sizeof(nid);
	nid.hWnd = m_hWnd;    //트레이아이콘과 통신할 윈도우 핸들
	nid.uID = IDI_TRAY;   //트레이아이콘의 리소스ID(Data측면)
	nid.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP;
	nid.uCallbackMessage = UM_TRAYICON; //트레이아이콘의 메시지가 수신시 수신윈도우가 처리할 메시지
	nid.hIcon = AfxGetApp()->LoadIcon(IDI_TRAY);//아이콘리소스(UI측면)

	lstrcpy(nid.szTip, "UBC FirmwareView"); // 툴팁

	// taskBar상태영역에 아이콘 추가,삭제,수정할때 시스템에 메시지 전달
	Shell_NotifyIcon(NIM_ADD,&nid); //구조체내용으로 트레이아이콘을 등록한다.(Data측면)

	SendMessage(WM_SETICON,(WPARAM)TRUE,(LPARAM)nid.hIcon);//트레이화면에 붙이기(UI측면)
}

LONG CFWVDlg::TrayIconMessage(WPARAM wParam,LPARAM lParam)
{
	switch (lParam)
	{
	case WM_RBUTTONDOWN: //트레이아이콘을 한 번클릭시
		{
			//메뉴를 로드한다.
			CMenu menu, *pMenu;
			CPoint pt;

			menu.LoadMenu(IDR_TRAY_MENU);
			pMenu = menu.GetSubMenu(0); //첫번째 주메뉴를 호출한다.
			GetCursorPos(&pt);

			//트레이아이콘의 메뉴를 X,Y위치에 뛰운다.
			pMenu->TrackPopupMenu(TPM_RIGHTALIGN,pt.x,pt.y,this);
			break;
		}
		//트레이 아이콘을 더블클릭했을때 윈도우가 보여지게 한다.
	case WM_LBUTTONDBLCLK:
		{
			ShowWindow(SW_SHOW); //윈도우를 보여준다.
			//ShowWindow(SW_NORMAL);
			ShowWindow(SW_MAXIMIZE);
			SetForegroundWindow();
			SetFocus();
			break;
		}
	}

	return 1;
}


void CFWVDlg::OnTrayOpen()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	ShowWindow(SW_SHOW); //윈도우를 보여준다.
	ShowWindow(SW_MAXIMIZE);
}

void CFWVDlg::OnTrayClose()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	PostQuitMessage(0); // 프로그램 종료
}

// skpark ContentsCacheServer 표시
void CFWVDlg::_showCacheServerImage()
{
	ubcConfig aIni("UBCVariables");

	ciString serverIp = "";
	aIni.get("ROOT","ContentsCacheServer",serverIp);
	if(serverIp.empty()) {
		ciDEBUG(1,("It's nothing to do with ContentsCacheServer"));
		cacheVar2.ShowWindow(SW_HIDE);
		cacheVar.ShowWindow(SW_HIDE);
		return;
	}
	if(serverIp=="localhost") {
		ciDEBUG(1,("It's ContentsCache Server"));

		CBitmap a_bmp;
		VERIFY(a_bmp.LoadBitmap(IDB_BITMAP_CCSERVER));
		BITMAP bm ;
		VERIFY(a_bmp.GetObject(sizeof(bm), (LPVOID)&bm));
		CRect rectParent;
		GetClientRect(&rectParent);
		int height = rectParent.Height();
		int width = rectParent.Width();
		int x = 6;
		int y = height-bm.bmHeight-5;;

		cacheVar.MoveWindow(x,y,bm.bmWidth,bm.bmHeight);
		cacheVar.ShowWindow(SW_SHOW);
		cacheVar2.ShowWindow(SW_HIDE);
		return;
	}

	ciDEBUG(1,("It's ContentsCache Client"));

	CBitmap a_bmp;
	VERIFY(a_bmp.LoadBitmap(IDB_BITMAP_CCCLIENT));
	BITMAP bm ;
	VERIFY(a_bmp.GetObject(sizeof(bm), (LPVOID)&bm));
	CRect rectParent;
	GetClientRect(&rectParent);
	int height = rectParent.Height();
	int width = rectParent.Width();
	int x = 6;
	int y = height-bm.bmHeight-5;;

	cacheVar2.MoveWindow(x,y,bm.bmWidth,bm.bmHeight);
	cacheVar2.ShowWindow(SW_SHOW);
	cacheVar.ShowWindow(SW_HIDE);
	return;

}
// skpark USB_Device
void CFWVDlg::_initDeviceChangeInterface()
{
	ubcConfig aIni("UBCVariables");

	_autoImport = ciFalse;
	aIni.get("ROOT","USB_AUTO_IMPORT",_autoImport);
	if(_autoImport == ciFalse) {
		ciDEBUG(1,("USB_AUTO_IMPORT not allowed"));
		return;
	}


	ciDEBUG(1,("_initDeviceChangeInterface()"));
	// 필터 구조체 선언
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilter; 
	// 필터 구조체 초기화
	ZeroMemory(&NotificationFilter, sizeof(NotificationFilter)); 
	// 구조체 사이즈
	NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE); 
	// 디바이스 타입
	NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE; 
    // 인자에 Window handle과 필터 구조체, 그리고 parameter를 설정.
	m_hDevNotify = RegisterDeviceNotification(m_hWnd, 
							&NotificationFilter,
							DEVICE_NOTIFY_WINDOW_HANDLE );//| DEVICE_NOTIFY_ALL_INTERFACE_CLASSES); 
}

// skpark USB_Device
char 
FirstDriveFromMask (ULONG unitmask)
{
/*----------------------------------------------------------------------
   FirstDriveFromMask (unitmask)

      Finds the first valid drive letter from a mask of drive letters. The
      mask must be in the format bit 0 = A, bit 1 = B, bit 3 = C, etc.

      A valid drive letter is defined when the corresponding bit is set to
      1.

      Returns the drive letter that was first found.
   ----------------------------------------------------------------------*/ 
	char i;
	for (i = 0; i < 26; ++i)
	{
		if (unitmask & 0x1)	break;
		unitmask = unitmask >> 1;
	}
	return (i + 'A');
}

// skpark USB_Device
BOOL CFWVDlg::OnDeviceChange( UINT nEventType, DWORD_PTR dwData )
{
	if(this->_autoImport == ciFalse){
		ciDEBUG(6,("USB_AUTO_IMPORT not allowed"));
		return FALSE;
	}

	BOOL bReturn = CWnd::OnDeviceChange(nEventType, dwData);

	PDEV_BROADCAST_HDR lpdb = (PDEV_BROADCAST_HDR)dwData;
	if(lpdb == NULL) {
		//ciWARN(("Unknown Device_Changed Event 1, Skipped"));
		return bReturn;
	}
	if (lpdb->dbch_devicetype != DBT_DEVTYP_VOLUME)	{
		ciWARN(("Device_Changed, but It's not USB_DRIVE, Skipped"));
		return bReturn;
	}
	PDEV_BROADCAST_VOLUME lpdbv = (PDEV_BROADCAST_VOLUME)lpdb;
	if(lpdbv == NULL) {
		//ciWARN(("Unknown Device_Changed Event 2, Skipped"));
		return bReturn;
	}

	ciWARN(("USB_DRIVE Device_Changed event !!!"));

	if (nEventType == DBT_DEVICEARRIVAL) {// 장치 연결
		//if (lpdbv->dbcv_flags & DBTF_MEDIA) {
		char drive = FirstDriveFromMask(lpdbv->dbcv_unitmask);
		ciWARN(("USB_DRIVE_CONNECTED !!! DRIVE:%c ",drive));
		//}

		char ini_path[256];
		char ann_path[256];
		memset(ini_path,0x00,256);
		sprintf(ini_path,"%c:\\SQISoft\\UTV1.0\\execute\\config", drive);
		memset(ann_path,0x00,256);
		sprintf(ann_path,"%c:\\SQISoft\\UTV1.0\\execute\\data", drive);
		
		ciString targetIni;
		ciString program = "NONE";
		if(ciFalse == _getTargetContentsPackage(ini_path, targetIni)){
			// Message 를 pop_up 해주어야 한다. !!!!
			ciWARN(("No valid Contents Package here"));
			//return bReturn;
			bReturn = false;
		}else{
			int len = targetIni.size();
			program = targetIni.substr(0,len-4); // ".ini" 부분을 떼어내야한다.
		}

		bool announce_exist = false;
		if(access(ann_path, R_OK)==0){
			ciWARN(("Annonuce exist"));
			//return bReturn;
			announce_exist = true;
		}else{
			ciWARN(("Annonuce not exist"));
		}

		if(bReturn==false && announce_exist ==false) return false;

		char driveStr[256];
		memset(driveStr,0x00,256);
		sprintf(driveStr,"%c:\\", drive);

		// Contents 와 ini 를 C 드라이브로 import 한다.
		ciString command = "UBCImporter.exe";
		ciString arg=" +ini ";
		arg += program ;
		arg += " +drive " ;
		arg += driveStr;
		arg += " +display 0 +nomsg +USB_AUTO_IMPORT";
		if(announce_exist) {
			arg += " +announce_exist";
		}
		ciString debugStr = command + arg;

		//AfxMessageBox(debugStr.c_str());
		
		// USB_AUTO_IMPORT 명령은 importer 가 직접
		// starter 옵션을 고치고, 브라우저를 다시 띠우게 된다. firmwareView 도 refresh 한다.
	
		//if(system(command.c_str())==0){
		if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
			ciDEBUG(1,("%s %s succeed", command.c_str(), arg.c_str()));
			return ciTrue;
		}
		// USB Drive 를 자동제거하는 부분도 고려해야한다.
		
		/*
		// starter 옵션을 고치고, 브라우저를 다시 띠우게 된다. firmwareView 도 refresh 한다.
		CString strArg;
		strArg.Format("+ini %s +display 0 +nomsg +reservationStart", program.c_str());
		ciDEBUG(1, ("socketAgent call : %s", strArg));
		if(!scratchUtil::getInstance()->socketAgent("127.0.0.1", 14007, "import", strArg))
		{
			ciDEBUG(1, ("socketAgent call fail : %s", strArg));
		}//if
		*/



	}else if (nEventType == DBT_DEVICEREMOVECOMPLETE) { // 장치 제거
		//if (lpdbv->dbcv_flags & DBTF_MEDIA) {
		char drive = FirstDriveFromMask(lpdbv->dbcv_unitmask);
		ciWARN(("USB_DRIVE_DISCONNECTED !!! DRIVE:%c ", drive));
		//}
	}else { // 기타 Event
		ciWARN(("USB_DRIVE Device_Changed %d", nEventType));	
	}


	return bReturn;
}


ciBoolean
CFWVDlg::_getTargetContentsPackage(const char* targetPath, ciString& targetName)
{
	ciDEBUG(1,("_getTargetContentsPackage(%s)", targetPath));

	ciString	pattern = targetPath;
				pattern += "\\*.ini";

	WIN32_FIND_DATA fileData;
	HANDLE hFind = FindFirstFile(pattern.c_str(),&fileData);
	if(hFind == INVALID_HANDLE_VALUE){
		return ciFalse;
	}

	ciString	lastTime;
	do {
		if(fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
			continue;
		}
		ciDEBUG(9,("%s founded", fileData.cFileName));

		ciString filename = targetPath;
		filename += "\\";
		filename += fileData.cFileName;

		// lastUpdateTime 이 가장 최근인 1놈만을 채택한다.
		char	lastUpdateTime[32];
		memset(lastUpdateTime, '\0', sizeof(lastUpdateTime));
		GetPrivateProfileString("Host","lastUpdateTime","",lastUpdateTime,sizeof(lastUpdateTime),filename.c_str());
		if(strlen(lastUpdateTime) < 19){
			ciDEBUG(1,("Invalid lastUpdateTime information, It may not contents package ini file"));
			continue;
		}
		ciDEBUG(1,("Ini=%s, lastUpdateTime=%s", fileData.cFileName, lastUpdateTime));
		
		if(lastTime.empty() || lastTime <= lastUpdateTime){
			lastTime=lastUpdateTime;
			targetName=fileData.cFileName;
		}
	} while(FindNextFile(hFind, &fileData));
	FindClose(hFind);

	if(targetName.empty()){
		ciWARN(("No Contents Package Here, It's not UBC USB Drive"));
		return ciFalse;
	}
	ciDEBUG(1,("Finally Selected Ini=%s, lastUpdateTime=%s", targetName.c_str(), lastTime.c_str()));
	return ciTrue;
}