#include "stdafx.h"
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchUtil.h"
#include "libAGTClient/AGTClientSession.h"
#include "FtpUtil.h"
#include "agtScreenshotTimer.h"
#include "WindowCapture.h"
#include "FileServiceWrapDll.h"
#include "common/libHttpRequest/HttpRequest.h"
#include <afxtempl.h>

ciSET_DEBUG(9, "agtScreenshotTimer");

#define DEFAULT_FTPPATH "/ScreenShot"
#define SCREENSHOT_LOCAL_PATH _COP_CD("C:\\SQIsoft\\ScreenShot")
#define UPLOADED_PREFIX		"uploaded_"

agtScreenshotTimer::agtScreenshotTimer(const char* hostId, int screenshotPeriod) 
: _interval(0), _hostId(hostId)
{
	ciDEBUG(3, ("agtScreenshotTimer()"));
	_ssFtpIp="";
	_ssFtpUser="";
	_ssFtpPassword="";
	_ssFtpPort = 21;

	if (!::GetEnableShortScreenshotPeriod())
		_screenshotPeriod = int(screenshotPeriod/60);
	else
		_screenshotPeriod = screenshotPeriod;

	ubcConfig  aIni("UBCVariables");
	_dayLimit = 0;
	if (!aIni.get("ROOT","PlayLogDayLimit", _dayLimit)) {
		_dayLimit = 15;  // Default 1 day
	}

	_dayLimit = (_dayLimit/2);
	if(_dayLimit < 1) _dayLimit = 1;
}

agtScreenshotTimer::~agtScreenshotTimer() {
	ciDEBUG(3, ("~agtScreenshotTimer()"));
}

void 
agtScreenshotTimer::initTimer(const char* pname, int interval) {
	ciDEBUG(3, ("initTimer(%s,%d)", pname, interval));
	_interval = interval;
	_timer = new ciTimer(pname, this);
	_timer->setInterval(interval);
}

void
agtScreenshotTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(3, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	if ((counter > 0) && ((counter % _screenshotPeriod) != 1)) {
		return;
	}

	ciShort screenshotPeriod;
	ubcConfig  aIni("UBCVariables");
	aIni.get("ROOT","ScreenShotPeriod", screenshotPeriod);
	if (!::GetEnableShortScreenshotPeriod())
	{
		if(screenshotPeriod >= 60){
			_screenshotPeriod = int(screenshotPeriod/60);
		}
	}
	else
	{
		if(screenshotPeriod > 0)
			_screenshotPeriod = screenshotPeriod;
	}

	//
	// skpark 2010.7.14 처음에 ftp 정보를 못가져오면, 계속 ftp 가 안되는 증세 때문에
	//
	if(_ssFtpUser.empty() ||  _ssFtpIp.empty() || _ssFtpPassword.empty()){
		ciWARN(("Ftp Infomation Unknown"));
		// 어떤 이유로 해서 아직 ftp 정보를 가져오지 못했다.
		// 따라서, 이들 정보를 다시 얻어낸다.
		AGTClientSession* a_agentClient = AGTClientSession::getInstance();
		ciString ip = a_agentClient->getFtpIP();
		ciString user = a_agentClient->getFtpId();
		ciString password = a_agentClient->getFtpPasswd();
		int port = a_agentClient->getFtpPort();

		this->setScreenShotFtp(ip.c_str(), user.c_str(), password.c_str(), (ciUShort)port);
		//return; // 쉬면안된다.
	}

	// ScreenShot을 찍어서 FTP로 업로딩
	
	if (!screenshot())	{
		ciERROR(("ScreenShot error..."));
		
		//return;
	}

	if (!::GetEnableShortScreenshotPeriod())
	{
		if (counter % (6*_screenshotPeriod) == 1) {  // 300*6 = 30분 마다 한번씩 지운다.
			deleteScreenShotFiles();
		}
	}
	else
	{
		static int delete_count = (30 * 60); // 30분
		if( counter > delete_count )
		{
			deleteScreenShotFiles();
			delete_count += (30 * 60); // 30분 추가
		}
	}
}

ciBoolean
agtScreenshotTimer::screenshot(const char* fileName,const char* ftpPath)
{
	ciDEBUG(9, ("screenshot(%s,%s)", fileName, ftpPath));
	
	ciString strFileName;

	if (!strlen(fileName)) {
		ciString  current = "";
		if (!::GetEnableShortScreenshotPeriod())
			current = this->_curTimeStr();
		else
			current = this->_curTimeStrEx();
#ifdef _COP_MSC_
		strFileName = ciEnv::newEnv("COMPUTERNAME");
#else
		strFileName = getenv("HOSTNAME");
#endif
		strFileName += "_";
		strFileName += current;	
	} else {
		strFileName = fileName;
	}

	ciString strFilePath = DEFAULT_FTPPATH;
	if(strlen(ftpPath)){
		strFilePath = ftpPath;
	}
	strFilePath += "/";
	strFilePath += _hostId;

	// ScreenShot Capture
	CWindowCapture winCapture;
	try {
		winCapture.OnCapture(CWindowCapture::CAP_JPG, strFileName.c_str(), SCREENSHOT_LOCAL_PATH);
	} catch (...) {
		ciERROR(("unknown exception occorred. capture failed..."));
		return ciFalse;
	}

	/////////////////////////////////////////////////////////////////////////
	//Modyfied by jwh184 2012-02-29
	strFileName += ".jpg";
	ciString strLocalPath = SCREENSHOT_LOCAL_PATH;
	strLocalPath += "\\";
	strLocalPath += strFileName;
	ciDEBUG(1,("Upload FileName: %s" , strLocalPath.c_str()));

	if(_ssFtpUser.empty() ||  _ssFtpIp.empty() || _ssFtpPassword.empty()){
		ciERROR(("Network Connection ERROR, ScreenShot(%s) can't send", strLocalPath.c_str()));
		return ciFalse;
	}

	ciStringList uploadTargetList;
	getUploadTargetFiles(uploadTargetList);

	if(!IsUseHttp())
	{
		ciDEBUG(1, ("It's not HTTP mode"));
		// FTP Upload
		FtpUtil ftpUtil(_ssFtpUser.c_str(), _ssFtpPassword.c_str(), _ssFtpIp.c_str(), _ssFtpPort);	
		if (!ftpUtil.Connect())
		{
			ciERROR(("ftp server[%s] open error.", _ssFtpIp.c_str()));
			return ciFalse;
		}

		ftpUtil.Passive();
		ftpUtil.ChangeWorkingDirectory(strFilePath.c_str());

		ciStringList::iterator itr;
		for(itr=uploadTargetList.begin();itr!=uploadTargetList.end();itr++){
			//ftpUtil.setBinary();		
			//ftpUtil.FileUpload(strLocalPath.c_str(), strFileName.c_str());
			ciString uploadFile = SCREENSHOT_LOCAL_PATH;
			uploadFile += "\\";
			uploadFile += (*itr);
			
			if(ftpUtil.FileUpload(uploadFile.c_str(), itr->c_str())){
				changeSucceedFileName(itr->c_str());
			}
		}
		ftpUtil.Disconnect();
	}
	else
	{
		ciDEBUG(1, ("It's HTTP mode"));
		CFileServiceWrap httpFile(_ssFtpIp.c_str(), _ssFtpPort);
		ciDEBUG(1, ("Http file service initialized ip=%s, port=%d", _ssFtpIp.c_str(), _ssFtpPort));
		httpFile.Login(_ssFtpUser.c_str(), _ssFtpPassword.c_str());
		ciDEBUG(1, ("Http login success id=%s, password=%s", _ssFtpUser.c_str(), _ssFtpPassword.c_str()));

		ciStringList::iterator itr;
		for(itr=uploadTargetList.begin();itr!=uploadTargetList.end();itr++){
			//strFilePath += "/" + strFileName;
			//ciDEBUG(1, ("Trying Putfile %s ==> %s", strLocalPath.c_str(), strFilePath.c_str()));
			//if(!httpFile.PutFile(strLocalPath.c_str(), strFilePath.c_str(), 0, NULL))
			//{
			//	ciERROR(("Http file service error : ", httpFile.GetErrorMsg()));
			//	return ciFalse;
			//}//if
			//ciDEBUG(1, ("Success Putfile %s ==> %s", strLocalPath.c_str(), strFileName.c_str()));
			

			ciString uploadFile = SCREENSHOT_LOCAL_PATH;
			uploadFile += "\\";
			uploadFile += (*itr);
			
			ciString strRemotePath = strFilePath;
			strRemotePath += "/" + (*itr);
			
			ciDEBUG(1, ("Trying Putfile %s ==> %s", uploadFile.c_str(), strRemotePath.c_str()));
			if(!httpFile.PutFile(uploadFile.c_str(), strRemotePath.c_str(), 0, NULL))
			{
				ciERROR(("Http file service error : %s ", httpFile.GetErrorMsg()));
				//httpFile.Logout();
				continue;
			}//if
			changeSucceedFileName(itr->c_str());
			ciDEBUG(1, ("Success Putfile %s ==> %s", uploadFile.c_str(), strRemotePath.c_str()));
			//httpFile.Logout();
		}
		httpFile.Logout();
	}//if
	/////////////////////////////////////////////////////////////////////////

	// set filename to UBCVariables.ini
	setLastScreenShotFile(strFileName.c_str());

	return ciTrue;
}

//////////////////////////////////////////////////////////////////////////
int CompareAscending(const void *a, const void *b)
{
	CString *pA = (CString*)a;
	CString *pB = (CString*)b;
	return (pA->Compare(*pB));
}

int CompareDescending(const void *a, const void *b)
{
	CString *pA = (CString*)a;
	CString *pB = (CString*)b;
	return (-1 * (pA->Compare(*pB)));
}

void SortStringArray(CStringArray& csa/*, BOOL bDescending*/)
{
	int iArraySize = csa.GetSize();
	if (iArraySize <= 0)
		return;

	int iCSSize = sizeof (CString*);
	void* pArrayStart = (void *)&csa[0];

	//  if (bDescending)
	qsort (pArrayStart, iArraySize, iCSSize, CompareDescending);
	//  else
	//     qsort (pArrayStart, iArraySize, iCSSize, CompareAscending);
}
//////////////////////////////////////////////////////////////////////////

int 
agtScreenshotTimer::getUploadTargetFiles(ciStringList& targetList)
{
	ciDEBUG(9, ("getUploadTargetFiles()"));

	ciString dirPath = SCREENSHOT_LOCAL_PATH;
	dirPath += "\\*.jpg";

	HANDLE hFile = NULL;
	WIN32_FIND_DATA FileData;
	hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		return 0;
	}

	if (!::GetEnableShortScreenshotPeriod())
	{
		// 원래방식
		do {
			if(targetList.size() > 60) break;  // 너무 많은 스크린샷이 한번에 가지 않도록 한다.
			if ((FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) continue;

			ciString targetFile = FileData.cFileName;
			int prefix_len = strlen(UPLOADED_PREFIX);
			if (targetFile.length() <= prefix_len || 
				targetFile.substr(0,prefix_len) == UPLOADED_PREFIX) continue;

			ciDEBUG(2, ("upload target file[%s]", targetFile.c_str()));
			targetList.push_back(targetFile);
		} while (FindNextFile(hFile, &FileData));
	}
	else
	{
		// 골프장 방식
		CTime cur_tm = CTime::GetCurrentTime();
		CTimeSpan span_tm(0, 1, 0, 0); // 1시간
		CTime one_hour_prev_tm = cur_tm - span_tm; // 1시간 전 시간
		CString str_prev_tm = one_hour_prev_tm.Format("%Y%m%d%H%M%S");

		CStringArray temp_list;

		do {
			if ((FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) continue;

			ciString targetFile = FileData.cFileName;
			int prefix_len = strlen(UPLOADED_PREFIX);
			if (targetFile.length() <= prefix_len || 
				targetFile.substr(0,prefix_len) == UPLOADED_PREFIX) continue;

			// ex) SQI-06000_20150102030405.jpg
			ciStringTokenizer tokens(targetFile.c_str(), "_.");
			if(tokens.countTokens() < 3) continue;

			// ex) 20150102030405.jpg
			ciString file_time = tokens.nextToken();
			file_time = tokens.nextToken();

			if( file_time < (LPCSTR)str_prev_tm) continue; // 오직 1시간 이내 파일만

			ciDEBUG(2, ("upload target file[%s]", targetFile.c_str()));
			temp_list.Add(targetFile.c_str());
		} while (FindNextFile(hFile, &FileData));

		SortStringArray(temp_list);
		for(int i=0; i<temp_list.GetCount() && i<60; i++) // 너무 많은 스크린샷이 한번에 가지 않도록 한다.
		{
			targetList.push_back((LPCSTR)temp_list.GetAt(i));
		}
	}

	FindClose(hFile);

	return targetList.size();
}

ciBoolean 
agtScreenshotTimer::changeSucceedFileName(const char* strFileName)
{
	ciString srcFullPath = SCREENSHOT_LOCAL_PATH;
	srcFullPath += "\\";
	srcFullPath += strFileName;

	ciString tarFullPath = SCREENSHOT_LOCAL_PATH;
	tarFullPath += "\\";
	tarFullPath += UPLOADED_PREFIX;
	tarFullPath += strFileName;

	if( !::MoveFile(srcFullPath.c_str(), tarFullPath.c_str()) )
	{
		ciERROR(("%s --> %s file move failed",  srcFullPath.c_str(), tarFullPath.c_str()));
		return ciFalse;
	}
	ciDEBUG(9, ("changeSucceedFileName(%s --> %s) succeed", srcFullPath.c_str(), tarFullPath.c_str()));
	return ciTrue;
}

ciBoolean 
agtScreenshotTimer::deleteScreenShotFiles()
{
	ciDEBUG(1, ("deleteScreenShotFiles()"));

	ciString dirPath = SCREENSHOT_LOCAL_PATH;
	dirPath += "\\*.*";

	HANDLE hFile = NULL;
	WIN32_FIND_DATA FileData;
	hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) return ciFalse;

	do {
		if (!(FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			CTime fileTime(FileData.ftLastWriteTime);
			CTime referTime = CTime::GetCurrentTime();
			CTimeSpan spanTime(_dayLimit, 0, 0, 0);
			referTime = referTime - spanTime; // 15일시간 이전
			if (fileTime < referTime && !strstr(FileData.cFileName, "log"))
			{
				ciDEBUG(2, ("remove file[%s]", FileData.cFileName));
				ciString delFilePath = SCREENSHOT_LOCAL_PATH;
				delFilePath += "\\";
				delFilePath += FileData.cFileName;
				::remove(delFilePath.c_str());
			}
		}
	} while (FindNextFile(hFile, &FileData));
	FindClose(hFile);

	return ciTrue;
}

void 
agtScreenshotTimer::setScreenShotFtp(const char* ip, const char* user, const char* password, ciUShort port)
{
	ciDEBUG(9, ("setScreenShotFtp(%s,%s,%s,%d)", ip, user, password, port));

	_ssFtpIp = ip;
	_ssFtpUser = user;
	_ssFtpPassword = password;
	_ssFtpPort = port;
}

ciBoolean 
agtScreenshotTimer::setLastScreenShotFile(const char* fileName)
{
	ubcConfig aIni("UBCVariables");
	if (!aIni.set("ROOT", "LastScreenShotFile", fileName)) {
		ciERROR(("[ROOT]LastScreenShotFile set[%s] failed.", fileName));
		return ciFalse;
	}

	if (::GetEnableShortScreenshotPeriod())
	{
		CHttpRequest http_request;
		http_request.Init(CHttpRequest::CONNECTION_SERVER);

		CString send_msg, result;
		send_msg.Format("hostId=%s&lastScreenshot=%s", _hostId.c_str(), fileName);
		BOOL ret_val = http_request.RequestPost("/UBC_Player/set_last_screenshot.asp",send_msg,result);
	}

	return ciTrue;
}

string 
agtScreenshotTimer::_curTimeStr() 
{
    time_t current;
    time(&current);

    char* str;
    struct tm aTM;
#ifdef _WIN32
    struct tm *temp = localtime(&current);
    if (temp) {
        aTM = *temp;
    } else {
        memset((void*)&aTM, 0x00, sizeof(aTM));
    }
#else
    localtime_r(&current, &aTM);
#endif

    str = (char*)malloc(sizeof(char)*20);
    sprintf(str,"%4d%02d%02d%02d%02d",
        aTM.tm_year+1900,aTM.tm_mon+1,aTM.tm_mday,
        aTM.tm_hour,aTM.tm_min);
    string strTime = str;
    free(str);
    return strTime;
}

string 
agtScreenshotTimer::_curTimeStrEx() 
{
    time_t current;
    time(&current);

    char* str;
    struct tm aTM;
#ifdef _WIN32
    struct tm *temp = localtime(&current);
    if (temp) {
        aTM = *temp;
    } else {
        memset((void*)&aTM, 0x00, sizeof(aTM));
    }
#else
    localtime_r(&current, &aTM);
#endif

    str = (char*)malloc(sizeof(char)*32);
    sprintf(str,"%4d%02d%02d%02d%02d%02d",
        aTM.tm_year+1900,aTM.tm_mon+1,aTM.tm_mday,
		aTM.tm_hour,aTM.tm_min, aTM.tm_sec);
    string strTime = str;
    free(str);
    return strTime;
}
