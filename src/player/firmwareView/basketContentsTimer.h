#ifndef _basketContentsTimer_H_
#define _basketContentsTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <hi/libHttp/ubcMux.h>


class basketContentsData {
public:
	basketContentsData() : zorder(0), contentsType(0), volume(0), runningTime(0), alreadyDownload(ciFalse), copySucceed(ciFalse), timeExpired(ciFalse) {}
    ciString		basketContentsId;
    ciShort				zorder;
    ciShort				contentsType;
    ciString		siteId;
    ciString		contentsId;
    ciString		location;
    ciString		filename;
    ciULong			volume;
    ciString		description;
    ciLong			runningTime;
    ciString		registerId;
    ciString		registerTime;
    ciString		startTime;
	ciBoolean		alreadyDownload;
	ciBoolean		copySucceed;
	ciBoolean		timeExpired;

	void print();
	ciBoolean write();
	ciBoolean load(int pZorder);
};
typedef map<int, basketContentsData*>	BasketContentsMAP;

class basketContentsTimer : public ciWinPollable {
public:
	basketContentsTimer(ciString& site, ciString& host);
	virtual ~basketContentsTimer();

	virtual void processExpired(ciString name, int counter, int interval);

	void clearMap();
	void printMap();

protected:

	void	_makePath(const char* relpath, CString& outval);

	ciBoolean _getBasketContents(const char* url, CString& sendMsg, CStringArray& line_list);
	int _setBasketContents(CStringArray& line_list);
	int _downloadContents();
	int _safeCopyContents(const char* nowStr);
	ciBoolean _copyContents(const char* nowStr);
	virtual ciBoolean _startTimeExpired();
	int			_deleteOldFile(ciShort durationDay);
	int			_removeContents(const char* nowStr);
	ciBoolean	_isExist(ciString& filename);

	unsigned long _getFileSize(const char* filename);


	int	_loadIni();
	int	_writeIni();

	BasketContentsMAP	_map;

	TCHAR _cDrive[MAX_PATH], _cPath[MAX_PATH];

	ciMutex		_mapLock;
	ciString _site;
	ciString _host;

	ciBoolean	_useReboot;

	ciString _server1;
	ciString _server2;
	ciULong _port;

};

#endif //_basketContentsTimer_H_
