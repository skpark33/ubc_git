 /*! \file tcpSocketServer.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI Server
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _tcpSocketServer_h_
#define _tcpSocketServer_h_

#include "ci/libThread/ciThread.h"


class tcpSocketServer : public virtual ciThread
{
public:
	tcpSocketServer(ciInt port) { _port = port; _xmlBuf = "";_hasLog=ciFalse;}
	virtual ~tcpSocketServer(){}

	void destroy();
	void run();

	ciBoolean	xmlRefresh();

	ciBoolean	tcpServer(int port);
	ciBoolean	parse(const char* command,ciString& res);
	ciBoolean	import(string& scheduleFile);
	ciBoolean	shutdownTime(string& timeStr);
	ciBoolean	firmwareView(const char* directive, const char* data, ciString& res);
	ciBoolean	keyboardSimulator(const char* command, const char* data);
	
	void myLogOpen(const char *filename=0) ;
	void myLogClose() ;
	void myDEBUG(const char *pFormat,...); 
	void myERROR(const char *pFormat,...) ;
	void myWARN(const char *pFormat,...); 

protected:	
	string		_curTimeStr();
	int			_kbdEvent(const char* value, DWORD flag);
	int			_getFocus();

	ciBoolean	_setData(ciString& data);
	const char*	_getData(const char* name);

	ciBoolean	_readXml();
	ciBoolean	_writeXml();
	ciBoolean	_isEOLXml(const char* xmlLine);
	ciBoolean	_channelModify(const char* ch, const char* channelName);
	ciBoolean	_channelAdd(const char* channelName,
							  const char* siteName,
							  const char* desc,
							  const char* networkUse);

	ciBoolean	_getIniInfo(const char* channelName,
							  ciString& out_siteId,ciString& out_desc, ciString& out_networkUse);

	void		_buildXMLLine(ciStringList& iniList,
							  ciStringList& outXMLLineList,
							  ciStringList& outXMLCompareLineList);

	ciStringMap	_dataMap;
	ciString   _xmlBuf;
	ofstream	_log;
	ciBoolean	_hasLog;
	ciInt		_port;
};


#endif //_tcpSocketServer_h_
