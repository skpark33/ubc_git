#include "stdafx.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchUtil.h"
#include "xmlTimer.h"
#include "libHttpRequest/HttpRequest.h"

ciSET_DEBUG(9, "xmlTimer");

#define CONTENTS_PATH _COP_CD("C:\\SQISoft\\Contents\\Enc\\")
#define ANNOUNCE_PATH _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\data\\announce\\")

xmlTimer::xmlTimer(const char* siteId, const char* hostId) 
{
	ciDEBUG(3, ("xmlTimer()"));
	_port = 8000;
	_useSSL = false;
	_serverIP = "";
	_siteId = siteId;
	_hostId = hostId;
}

xmlTimer::~xmlTimer() {
	ciDEBUG(3, ("~xmlTimer()"));
}

void
xmlTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(3, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	ciTime now;

	char nowStr[10];
	memset(nowStr,0x00,10);
	sprintf(nowStr, "%02d:%02d", now.getHour(), now.getMinute());

	ciDEBUG(3, ("processExpired(%s,%s,%d,%d)", nowStr, name.c_str(), counter, interval));

	ciStringMap::iterator itr;
	for(itr=_xmlMap.begin();itr!=_xmlMap.end();itr++){

		ciString url = itr->second;
		ciString filename = itr->first;

		ciBoolean getSucceed = false;

		CHttpRequest http_request;
		http_request.Init(CHttpRequest::CONNECTION_SERVER);
		if(!_serverIP.empty()) {
			http_request.SetServerIP(_serverIP.c_str());
		}
		if(!_useSSL) {
			http_request.DontUseSSL();
		}
		http_request.SetPort(_port);

		CStringArray line_list;
		BOOL ret_val = http_request.RequestGet(url.c_str(),line_list);

		CString text;
		for(int i=0; i<line_list.GetCount(); i++)
		{
			const CString& line = line_list.GetAt(i);
			ciDEBUG(1,("response : Line[%d]%s", i, line));
			if(i==0) continue;
			text += line;
			text += "\n";
		}
		
		if(ret_val)
		{
			if(line_list.GetCount() > 1 )
			{
				CString first_line =  line_list.GetAt(0);
				if(	first_line.GetLength() >= 2 && 
					first_line.Mid(0,2).CompareNoCase("OK") == 0) 
				{
					getSucceed = true;	
					if((_prevText != text)){
						if(_writeXML(filename, text)){
							_writeAnnounce(text);
							_prevText = text;
						}

					}
				}
			}
		}
		if(!getSucceed){
			ciERROR((http_request.GetErrorMsg()));
		}
	}
	return;
}

void xmlTimer::setXMLMap(ciStringMap& p, unsigned long port, ciBoolean useSSL, const char* serverIP) 
{ 
	 ciDEBUG(1,("setXMLMap(%ld,%d,%s)", port, useSSL, serverIP));

	 _port = port; 
	 _useSSL= useSSL; 
	 _serverIP=serverIP;

	 ciStringMap::iterator itr;
	 for(itr=p.begin();itr!=p.end();itr++){
		 _xmlMap.insert(ciStringMap::value_type(itr->first,itr->second));
		 ciDEBUG(1,("setXMLMap(%s,%s)",itr->first.c_str(),itr->second.c_str()));
	 }

}

bool xmlTimer::_writeXML(ciString& pXmlName, CString& text)
{
	ciDEBUG(1, ("_writeXML(%s)", pXmlName.c_str()) );

	CString filename;
	filename = CONTENTS_PATH;
	filename += "announce\\xml\\";
	filename += pXmlName.c_str();
	filename += ".xml";

	CString filename_bak = filename + ".bak";
	CString filename_bak_bak = filename_bak + ".bak";
	CString filename_tmp = filename + ".tmp";

	// file open
	CFile file;
	CFileException ex;
	if( file.Open(filename_tmp, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary, &ex) == FALSE )
	{
		ciERROR(("%s file open error", filename_tmp ));
		return false;
	}

	TRY
	{
		file.Write(text, text.GetLength());
	}
	CATCH(CFileException, ex)
	{
		ciERROR(("%s Write Error (errcode:%d)", filename_tmp, ex->m_cause) );
		return false;
	}
	CATCH(CException, ex)
	{
		ciERROR(("%s Write Error", filename_tmp) );
		return false;
	}
	END_CATCH
	file.Close();

	::DeleteFile(filename_bak_bak);
	::MoveFile(filename_bak, filename_bak_bak);
	::MoveFile(filename, filename_bak);
	if( ::MoveFile(filename_tmp, filename) == FALSE )
	{
		ciWARN(("%s can't move to %s (errcode:%d)", filename_tmp, filename, ::GetLastError()) );
		return false;
	}

	ciDEBUG(10, ("write xml ok") );
	return true;
}

bool xmlTimer::_writeAnnounce(CString& text)
{
	ciDEBUG(1, ("_writeAnnounce(%s)", text));
	
	CString filename;
	filename = ANNOUNCE_PATH;
	filename += "UBCAnnounce_local.ini";

	ciString buf = text;
	_getBraceValue(buf,"<date>","</date>");
	ciString date = buf;

	ciDEBUG(1, ("date=(%s)", date.c_str()));

	if(date.empty()){
		// announce 파일을 지운다.
		//::DeleteFile(filename);
		WritePrivateProfileString("announce", "isstart", "0", filename);
		_sendMessage("local",false);
		return true;
	}

	buf = text;
	_getBraceValue(buf,"<stime>","</stime>");
	ciString stime = date + " " + buf;
	ciDEBUG(1, ("stime=(%s)", stime.c_str()));

	buf = text;
	_getBraceValue(buf,"<ftime>","</ftime>");
	ciString ftime = date + " " + buf;
	ciDEBUG(1, ("ftime=(%s)", ftime.c_str()));

	ciTime startTime(stime.c_str());
	ciTime endTime(ftime.c_str());
	ciTime now;

	CString startStr,endStr, nowStr;
	startStr.Format("starttime=%ld\n", startTime.getTime());
	endStr.Format("endtime=%ld\n", endTime.getTime());
	nowStr.Format("createtime=%ld\n", now.getTime());

	CString hostStr;
	hostStr.Format("hostidlist=[\"%s\"]\n", _hostId.c_str());
	CString siteStr;
	siteStr.Format("siteId=%s\n", _siteId.c_str());

	const char* rest1 ="width=3840\ncomment9=\nfilemd5=\nbgfile=welcome.swf\ntitle=test\nbglocation=\nposx=0\n";
	const char* rest2 ="lrmargin=0\nposy=310\nposition=0\nisstart=1\nannounceid=local\n";
	const char* rest3 ="height=220\nserialno=0\nsoundvolume=100\nfilesize=\nalpha=0\n";
	const char* rest4 ="fontsize=24\ncomment1=welcome.swf\ncomment2=43200\ncreator=WIAmanager\n";
	const char* rest5 ="comment3=\nlock=0\ncomment4=\nplayspeed=1003\nbgcolor=#000000\n";
	const char* rest6 ="comment5=\nmgrid=1\nudmargin=0\ncontentstype=3\ncomment10=\n";
	const char* rest7 = "comment6=\nfont=System\ncomment7=\nheightratio=0.562500\n";
	const char* rest8 ="sourcesize=2\nfgcolor=#FFFFFF\ncomment8=\n";


	CFile file;
	CFileException ex;
	if( file.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary, &ex) == FALSE )
	{
		ciERROR(("%s file open error", filename ));
		return false;
	}

	CString initext = "[announce]\n";
	initext += startStr;
	initext += endStr;
	initext += nowStr;
	initext += hostStr;
	initext += siteStr;
	initext += rest1;
	initext += rest2;
	initext += rest3;
	initext += rest4;
	initext += rest5;
	initext += rest6;
	initext += rest7;
	initext += rest8;

	TRY
	{
		file.Write(initext, initext.GetLength());
	}
	CATCH(CFileException, ex)
	{
		ciERROR(("%s Write Error (errcode:%d)", filename, ex->m_cause) );
		return false;
	}
	CATCH(CException, ex)
	{
		ciERROR(("%s Write Error", filename));
		return false;
	}
	END_CATCH
	file.Close();
	_sendMessage("local",true);
	return true;
}

void 
xmlTimer::_sendMessage(const char* announceId,ciBoolean isStart)
{
	HWND brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2.exe");
	if(brwHwnd){
		_sendMessage(brwHwnd,announceId,isStart);
	}
	HWND brwHwnd1 = scratchUtil::getInstance()->getWHandle("UTV_brwClient2_UBC1.exe");
	if(brwHwnd1){
		_sendMessage(brwHwnd1,announceId,isStart);
	}
}
void 
xmlTimer::_sendMessage(HWND brwHwnd, const char* announceId, ciBoolean isStart)
{
	ciDEBUG(1,("sendMessage()"));

	COPYDATASTRUCT appInfo;
	if(isStart) {
		appInfo.dwData = 902;  // announce msg start
	}else{
		appInfo.dwData = 903;  // announce msg end
	}
	char buf[256];
	memset(buf,0x00,256);	
	sprintf(buf,"%s",announceId);
	appInfo.lpData = buf;
	appInfo.cbData = strlen(buf)+1;
	// 타임아웃을 건다.
	DWORD dwRet = 0;
	if(!::SendMessageTimeout(brwHwnd, WM_COPYDATA, 0, (LPARAM)&appInfo, SMTO_ABORTIFHUNG|SMTO_NORMAL, 5000, &dwRet)){
		ciERROR(("SendMessage Timeout !!!"));
		return;
	}
	ciDEBUG(1,("UTV_BRW announceExpired SendMessage End"));
	return;
}

bool
xmlTimer::_getBraceValue(ciString& target,const char* lb, const char* rb) {
    ciString a_buf = target;
    int start = target.find(lb);
    int end = target.rfind(rb);
    if(start >= 0 && end >=0 && start+strlen(lb) <= end ){
        a_buf = target.substr(start+strlen(lb), end - start - strlen(rb) + 1);
		target = a_buf;
		return true;
    }
    target = "";
	return false;
}
/*
<date>2014-04-04</date>
<stime>12:00:00</stime>
<ftime>21:00:00</ftime>
*/
/*
[announce]\n
createtime=1396590891
starttime=1396590736
endtime=1396677136
hostidlist=["WIA-59009"]
siteid=WIA
*/
