#include "stdafx.h"
/*! \file tcpSocketServer.C
 *  Copyright ⓒ 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI Server
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
*/

#include "tcpSocketServer.h"


#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libConfig/ciXProperties.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <CMN/libInstall/installUtil.h>
#include <CMN/libScratch/scratchUtil.h>

#if defined(_COP_ACC_) && defined(_COP_NOSTDLIB_)
#	include <fstream.h>
#else
#	include <fstream>
using namespace std;
#endif

#if defined(_WIN32)
#   include <winsock2.h>
//  WinSock DLL을 사용할 버전
#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0
#	include <time.h>
#else
#   include <sys/socket.h>       /* for socket(), connect(), send(), and recv() */
#   include <arpa/inet.h>        /* for sockaddr_in and inet_addr() */
#endif

#define BUF_LEN 128
#define CODE_LEN 89
#define EXE_PATH "C:\\SQISoft\\UTV1.0\\execute\\"

#define VK_1 48
#define VK_2 49
#define VK_3 50
#define VK_4 51
#define VK_5 52
#define VK_6 53
#define VK_7 54
#define VK_8 55
#define VK_9 56
#define VK_0 57

#define VK_A 65
#define VK_B 66
#define VK_C 67
#define VK_D 68
#define VK_E 69
#define VK_F 70
#define VK_G 71
#define VK_H 72
#define VK_I 73
#define VK_J 74
#define VK_K 75
#define VK_L 76
#define VK_M 77
#define VK_N 78
#define VK_O 79
#define VK_P 80
#define VK_Q 81
#define VK_R 82
#define VK_S 83
#define VK_T 84
#define VK_U 85
#define VK_V 86 
#define VK_W 87
#define VK_X 88
#define VK_Y 89
#define VK_Z 90


unsigned char virtualCode[CODE_LEN] = 
{ 
	0,VK_ESCAPE,VK_1,VK_2,VK_3,VK_4,VK_5,VK_6,VK_7,VK_8,
	VK_9,VK_0,VK_SUBTRACT,0,VK_BACK,VK_TAB,VK_Q,VK_W,VK_E,VK_R,
	VK_T,VK_Y,VK_U,VK_I,VK_O,VK_P,0,0,VK_RETURN,VK_CONTROL,
	VK_A,VK_S,VK_D,VK_F,VK_G,VK_H,VK_J,VK_K,VK_L,0,
	0,0,VK_LSHIFT,0,VK_Z,VK_X,VK_C,VK_V,VK_B,VK_N,
	VK_M,0,0,0,VK_RSHIFT,VK_PRINT,VK_MENU,VK_SPACE,VK_CAPITAL,VK_F1,
	VK_F2,	VK_F3,	VK_F4,	VK_F5,	VK_F6,	VK_F7,	VK_F8,	VK_F9,	VK_F10,VK_NUMLOCK,
	VK_SCROLL,VK_HOME,VK_UP,VK_PRIOR,VK_SUBTRACT,VK_LEFT,0,VK_RIGHT,VK_ADD,VK_END,
	VK_DOWN,VK_NEXT,VK_INSERT,VK_DELETE,0,0,0,VK_F11,VK_F12
};

const char* scanString[CODE_LEN] = 
{
	"","ESC","1","2","3","4","5","6","7","8",
	"9","0","MINUS","EQUAL","BS","TAB","Q","W","E","R",
	"T","Y","U","I","O","P","SQUARE_OPEN","SQUARE_CLOSE","ENTER","CTRL",
	"A","S","D","F","G","H","J","K","L","SEMICOLON",
	"QUOTATION","QUOTATION2","LSHIFT","WON","Z","X","C","V","B","N",
	"M","COMMA","PERIOD","SLASH","RSHIFT","PRTSC","ALT","SPACE","CAPS","F1",
	"F2","F3","F4","F5","F6","F7","F8","F9","F10","NUM",
	"SCROLL","HOME","UP","PGUP","MINUS","LEFT","CENTER","RIGHT","PLUS","END",
	"DOWN","PGDN","INS","DEL","","","","F11","F12"
};


ciSET_DEBUG(10, "tcpSocketServer");

void
tcpSocketServer::run()
{
	tcpServer(_port);
}

ciBoolean
tcpSocketServer::tcpServer(int port)
{
	myDEBUG("tcpServer(%d)",port);

	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA       WsaData;            // receives data from WSAStartup 
	if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {		
		return ciFalse;
	}
	
	myDEBUG("%s  Server : start...\n", (const char*)_curTimeStr().c_str());  

	struct sockaddr_in server_addr, client_addr;	// socket address
	
	// socket 생성
	SOCKET server_fd = socket( PF_INET, SOCK_STREAM, 0 );
	memset( &server_addr, 0, sizeof(struct sockaddr) );
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(port);

	// bind() 호출
	if( bind(server_fd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) < 0 ){
		myERROR("Server: Can't bind local address");
		return ciFalse;
	}

	// 소켓을 수동 대기모드로 세팅
	listen(server_fd, 5);
	myDEBUG("Server start.\n");

	while(1) {

		// 연결요청을 기다림
		int len = sizeof(client_addr);
		SOCKET client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &len);
		if(client_fd < 0) {
			myERROR("Server: accept failed.");
			return ciFalse;
		}
		myDEBUG("Server : A client connected. %s\n", inet_ntoa(client_addr.sin_addr));

		while(1) {
			string command="";

			while(1) {
				char buf[BUF_LEN+1];
				memset(buf, 0x00, sizeof(buf));
				int msg_size = recv(client_fd, buf, 1, 0);
				if(msg_size<=0){
					myWARN("recv error");
					break;
				}
				if(buf[0]=='\n'){
					if(command!="ping"){
						myDEBUG("command end (%s)", command.c_str());
					}
					break;
				}
				command += buf;
			}

			if(command.empty()){
				myDEBUG("socket broken");
				break;
			}

			if(command!="ping"){
				myDEBUG("(%s) received", command.c_str());
			}
			ciString res = "";
			ciBoolean retval = parse(command.c_str(), res);
		

			int msg_size = send(client_fd,res.c_str(),res.length()+1,0);
			if(msg_size<=0){
				myDEBUG("send error");
				break;
			}
			if(command!="ping"){
				myDEBUG("%s Send", res.substr(0,(res.size()>3000?3000:res.size())).c_str());
			}
		} // receive loop while

		myDEBUG("%s  Client : connection close....\n", (const char*)_curTimeStr().c_str());
		closesocket(client_fd);
		Sleep(100);
	}  // socket connection  loop while

	closesocket(server_fd);
	WSACleanup();		

	return ciTrue;
}

ciBoolean 
tcpSocketServer::parse(const char* command, ciString& res)
{
	string directive;
	string data;
	boolean directive_retreived = false;
	
	int len=strlen(command);
	for(int i=0;i<len;i++){
		if(command[i] == '|') {
			directive_retreived = true;
			continue;
		}
		if(directive_retreived){
			data+=command[i];
		}else{
			directive+=command[i];
		}
	}
	if(directive!="ping"){
		myDEBUG("%s|%s", directive.c_str(), data.c_str());
	}
	
	ciBoolean retval = 0;
	if(strcmp(directive.c_str(),"import")==0){
		retval =  import(data);
	}else if(strcmp(directive.c_str(),"shutdownTime")==0){
		retval =  shutdownTime(data);
	}else if(strncmp(directive.c_str(),"FV_",3)==0){
		_setData(data);
		retval =  firmwareView(directive.c_str(), data.c_str(),res);
	}else if(strncmp(directive.c_str(),"ping",4)==0){
		retval = ciTrue;
	}else{
		retval = keyboardSimulator(directive.c_str(), data.c_str());
	}

	if(res.empty()){
		res = directive;
		res += "|";
		if(retval){
			res += "OK\0";
		}else{
			res += "NO\0";
		}
	}else{
		ciString buf = res;
		res = directive;
		res += "|";
		res += buf;
	}

	return retval;
}


ciBoolean
tcpSocketServer::firmwareView(const char* directive, const char* data, ciString& res)
{
	myDEBUG("firmwareView(%s,%s)", directive, data);

	//
	// HOME
	//
	if(strcmp(directive,"FV_REBOOT")==0){
		ciString command = "rebootSystem.bat";
		ciString arg="";
		//if(system(command.c_str())==0){
		if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
			myDEBUG("%s %s succeed", command.c_str(), arg.c_str());
			return ciTrue;
		}
		myERROR("%s %s fail", command.c_str(), arg.c_str());
		return ciFalse;
	}

	if(strcmp(directive,"FV_SHUTDOWN")==0){
		ciString command = "shutdownSystem.bat";
		ciString arg="";
		//if(system(command.c_str())==0){
		if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
			myDEBUG("%s %s succeed", command.c_str(), arg.c_str());
			return ciTrue;
		}
		myERROR("%s %s fail", command.c_str(), arg.c_str());
		return ciFalse;
	}

	if(strcmp(directive,"FV_UBCSTART")==0){
		return installUtil::getInstance()->startUBC();
	}

	if(strcmp(directive,"FV_UBCSTOP")==0){
		return installUtil::getInstance()->stopUBC();
	}

	if(strcmp(directive,"FV_STUDIO")==0){
		ciString command = "UBCStudioUpdate.bat";
		ciString arg="";
		//if(system(command.c_str())==0){
		if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
			myDEBUG("%s %s succeed", command.c_str(), arg.c_str());
			return ciTrue;
		}
		myERROR("%s %s fail", command.c_str(), arg.c_str());
		return ciFalse;
	}

	if(strcmp(directive,"FV_QUERY_UBC_STATUS")==0){
		ciString OPT="SRC";
		if(!installUtil::getInstance()->getDisplayOption(OPT)){
			myWARN("get Display option failed, default value %s will be used", OPT.c_str());
		}
		ciBoolean show=ciFalse;
		if(!installUtil::getInstance()->getMouseOption(show)){
			myWARN("get Mouse option failed, default value %d will be used", show);
		}
		ciString shutdownTime="";
		if(!installUtil::getInstance()->getShutdownTime(shutdownTime)){
			myWARN("get shutdownTime failed, default value %s will be used", shutdownTime.c_str());
		}

		char buf[128];
		memset(buf,0x00,128);
		sprintf(buf,"OPT=%s,Show=%d,ShutdownTime=%s", OPT.c_str(), show, shutdownTime.c_str());
		res = buf;
		/*
		HANDLE hMutex = ::CreateMutex(NULL,FALSE, "UTV_starter");
		if (hMutex != NULL) {
			if(::GetLastError() == ERROR_ALREADY_EXISTS) {
				res = "START\0";
				::CloseHandle(hMutex);
				myDEBUG("FV_QUERY_UBC_STATUS %s will be return", res.c_str());
				return ciTrue;
			}
		}
		res = "STOP\0";
		*/
		myDEBUG("FV_QUERY_UBC_STATUS %s will be return", res.c_str());
		return ciTrue;
	}

	// 
	// Chanel Service
	//

	if(strcmp(directive,"FV_CH_PLAY")==0){
		const char* host = _getData("ChannelName");
		if(!host || !strlen(host)){
			myERROR("ChannelName is null");
			return ciFalse;
		}
		const char* site = _getData("SiteName");
		if(!site || !strlen(site)){
			myERROR("SiteName is null");
			return ciFalse;
		}
		const char* networkUse = _getData("NetworkUse");
		if(!networkUse || !strlen(networkUse)){
			myERROR("networkUse is null");
			return ciFalse;
		}
		ciBoolean isNetwork = (atoi(networkUse) ? ciTrue : ciFalse);

		if(!installUtil::getInstance()->setChannelName(host,site,isNetwork,ciTrue)){
			myERROR("setChannelProperty Error");
			return ciFalse;
		}

		HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
		if (!sttHwnd) {
			myWARN("STT Window Handle is not found");
			//installUtil::getInstance()->stopUBC();
			return installUtil::getInstance()->startUBC();
		}else{
			ciString processId="BROWSER";
			installUtil::getInstance()->getCurrentBRW(processId);
			return scratchUtil::getInstance()->startProcess((char*)processId.c_str());
		}		
	}

	if(strcmp(directive,"FV_CH_STOP")==0) {
		HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
		if (!sttHwnd) {
			myWARN("STT Window Handle is not found");
			return installUtil::getInstance()->stopUBC();
		}else{
			ciString processId="BROWSER";
			installUtil::getInstance()->getCurrentBRW(processId);
			return scratchUtil::getInstance()->stopProcess((char*)processId.c_str());		
		}
	}

	if(strcmp(directive,"FV_QUERY_CURRENT_CH")==0){
		ciBoolean networkUse=ciTrue;
		return installUtil::getInstance()->getChannelName(res,networkUse);
	}
	
	//
	// Channel Modify
	// 

	if(strcmp(directive,"FV_CH_MODIFY")==0){
		const char* ch = _getData("Ch");
		const char* channelName = _getData("ChannelName");

		if(!ch || !strlen(ch) || !channelName || !strlen(channelName)){
			myERROR("Invalid Ch or ChannelName");
			return ciFalse;
		}

		if(!_readXml()){
			myERROR("read XML failed");
			return ciFalse;
		}

		if(!_channelModify(ch, channelName)){
			myERROR("_channelModify failed");
			return ciFalse;
		}

		res += _xmlBuf;
		myDEBUG("Channel Modify succeed");
		return ciTrue;
	}
	
	if(strcmp(directive,"FV_IMPORT")==0){
		ciString command = "UBCImporter.exe";
		ciString arg=" C:";
		//if(system(command.c_str())==0){
		if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
			myDEBUG("%s %s succeed", command.c_str(), arg.c_str());
			return ciTrue;
		}
		myERROR("%s %s fail", command.c_str(), arg.c_str());
		return ciFalse;
	}

	if(strcmp(directive,"FV_REFRESH")==0){
		// build new xml...
		if(!xmlRefresh()){
			myERROR("refresh failed");
			return ciFalse;
		}

		res += _xmlBuf;
		myDEBUG("FV_REFRESH succeed");
		return ciTrue;
	}

	if(strcmp(directive,"FV_CH_ADD")==0){
		const char* siteName = _getData("SiteName");
		const char* channelName = _getData("ChannelName");
		const char* desc = _getData("Desc");
		const char* networkUse = _getData("NetworkUse");

		if(!channelName || !strlen(channelName)){
			myERROR("Invalid ChannelName");
			return ciFalse;
		}
	
		if(!_readXml()){
			myERROR("read XML failed");
			return ciFalse;
		}

		if(!_channelAdd(channelName,siteName,desc,networkUse)){
			myERROR("channel Add failed");
			return ciTrue;
		}
		res += _xmlBuf;
		myDEBUG("FV_CH_ADD succeed");
		return ciTrue;
	}

	//
	// Setting
	//

	if(strcmp(directive,"FV_DISPLAY_SET")==0){
		const char* displayOpt = _getData("OPT");
		if(!displayOpt){
			myERROR("OPT= not specified");
			return ciFalse;
		}
		ciString arg = "+autoscale +aspectratio";
		if(strncmp(displayOpt,"FULL",4)==0){
			arg = "+autoscale";
		}
		if(installUtil::getInstance()->setDisplayOption(arg,ciTrue)){
			myDEBUG("setBrowserOption Succeed");
			return ciTrue;
		}
		myERROR("setBrowserOption Error");
		return ciFalse;
	}
	
	if(strcmp(directive,"FV_TOOLS")==0){
		ciString command = "UBCDeviceWizard.exe";
		ciString arg="";
		//if(system(command.c_str())==0){
		if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
			myDEBUG("%s %s succeed", command.c_str(), arg.c_str());
			return ciTrue;
		}
		myERROR("%s %s fail", command.c_str(), arg.c_str());
		return ciFalse;	
	}
	
	if(strcmp(directive,"FV_UPDATE")==0){
		
		HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
		if (!sttHwnd) {
			myWARN("STT Window Handle is not found");
			ciString command = "SelfAutoUpdate.exe";
			ciString arg=" +log +ignore_env +update_only";
			//if(system(command.c_str())==0){
			if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
				myDEBUG("%s %s succeed", command.c_str(), arg.c_str());
				return ciTrue;
			}
			myERROR("%s %s fail", command.c_str(), arg.c_str());
			return false;
		}
		//::SendMessage(sttHwnd, WM_SHOWWINDOW, 5, 0);
		//::ShowWindow(sttHwnd, SW_SHOW);

		COPYDATASTRUCT appInfo;
		appInfo.dwData = 600;  // AutoUpdate
		appInfo.lpData = (char*)"";
		appInfo.cbData = 1;
		::SendMessage(sttHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
		myDEBUG("FV_UPDATE succeed");
		return ciTrue;
	}
	
	if(strcmp(directive,"FV_REGISTER")==0){
		ciString corbaProp;
		if(!ciXProperties::getInstance()->get("STARTER.APP_AGENT.PROPERTIES", corbaProp)){
			myERROR("get CORBA Properties failed");
			return ciFalse;
		}
		ciString command = "UBCExtraInstall.exe";
		ciString arg=" +eng +authOnly " + corbaProp;
		//if(system(command.c_str())==0){
		if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
			myDEBUG("%s %s succeed", command.c_str(), arg.c_str());
			return ciTrue;
		}
		myERROR("%s %s fail", command.c_str(), arg.c_str());
		return ciFalse;	
	}
	
	if(strcmp(directive,"FV_SHUTDOWNTIME")==0){
		const char* timeStr = _getData("ShutdownTime");
		if(!timeStr){
			myERROR("ShutdownTime not specified");
			return ciFalse;
		}
		ciString buf = timeStr;
		if(installUtil::getInstance()->shutdownTime(buf,ciTrue)){
			myDEBUG("Set AutoShutdownTime Succeed");
			return ciTrue;
		}
		myERROR("Set AutoShutdownTime failed");
		return ciFalse;
	}

	if(strcmp(directive,"FV_MOUSE")==0){
		const char* showStr = _getData("Show");
		if(!showStr){
			myERROR("Show not specified");
			return ciFalse;
		}
		ciBoolean show = (atoi(showStr) ? ciTrue : ciFalse);
		if(installUtil::getInstance()->setMouseOption(show,ciTrue)){
			myDEBUG("Set setMouseOption Succeed");
			return ciTrue;
		}
		myERROR("Set setMouseOption failed");
		return ciTrue;
	}

	if(strcmp(directive,"FV_STARTER")==0){

		HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
		if (!sttHwnd) {
			myWARN("STT Window Handle is not found");
			ciString command = "SelfAutoUpdate.exe ";
			ciString arg=" +log +ignore_env";
			//if(system(command.c_str())==0){
			if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciTrue)){
				myDEBUG("%s %s succeed", command.c_str(), arg.c_str());
				return ciTrue;
			}
			myERROR("%s %s fail", command.c_str(), arg.c_str());
			return false;
		}
		//::SendMessage(sttHwnd, WM_SHOWWINDOW, 5, 0);
		//::ShowWindow(sttHwnd, SW_SHOW);

		COPYDATASTRUCT appInfo;
		appInfo.dwData = 900;  // ShowWindow
		appInfo.lpData = (char*)"";
		appInfo.cbData = 1;
		::SendMessage(sttHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
		myDEBUG("FV_STARTER succeed");
		return ciTrue;
	}

	myERROR("%s is unknown command", directive);
	return ciFalse;
}

ciBoolean
tcpSocketServer::import(string& scheduleFile)
{
	myDEBUG("import %s", scheduleFile.c_str());

	string strExecute = EXE_PATH;
	strExecute += "UBCImporter.exe X: ";
	strExecute += scheduleFile;

	myDEBUG("WinExec %s", strExecute.c_str());
	int nRet = WinExec(strExecute.c_str(), SW_NORMAL);
	if(nRet < 31) {
		myERROR("UBCImporter run fail");
		return ciFalse;
	}

	myDEBUG("import %s complete", scheduleFile.c_str());
	return ciTrue;
}

ciBoolean
tcpSocketServer::shutdownTime(string& timeStr)
{
	myDEBUG("shutdownTime %s", timeStr.c_str());

	if(timeStr.length() < 5) {
		myERROR("wrong time format %s", timeStr.c_str());
		return ciFalse;
	}

	ciString hour = timeStr.substr(0,2);
	ciString min = timeStr.substr(3,2);

	ciXProperties* aUtil = ciXProperties::getInstance();

	ciString argValue="";

	if(!aUtil->get("STARTER.APP_AGENT.ARGUMENT", argValue)){
		myERROR("getProperties STARTER.APP_AGENT.ARGUMENT failed");
		return ciFalse;
	}

	ciBoolean has_set = ciFalse;
	ciString newValue= "";
	ciStringTokenizer tokens(argValue.c_str());
	while(tokens.hasMoreTokens()){
		ciString token = tokens.nextToken();
		if(token == "+shutdown"){
			continue;
		}
		if(token == "+hour"){
			has_set = ciTrue;
			continue;
		}
		if(token == "+minute"){
			has_set = ciTrue;
			continue;
		}
		if(has_set==ciFalse){
			newValue += token;
			newValue += " ";
		}
		has_set = ciFalse;
	}

	myDEBUG("ARG=%s", newValue.c_str());

	newValue += "+shutdown +hour ";
	newValue += hour;
	newValue += " +minute ";
	newValue += min;
	
	myDEBUG("ARG=%s", newValue.c_str());
	/*
	if(!aUtil->set("STARTER.APP_AGENT.ARGUMENT", newValue.c_str())){
		myERROR("setProperties STARTER.APP_AGENT.ARGUMENT failed");
		return ciFalse;
	}
	
	if(!aUtil->write()){
		myERROR("setProperties STARTER.APP_AGENT.ARGUMENT failed");
		return ciFalse;
	}
	*/
	HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
	if (!sttHwnd) {
		myERROR("STT Window Handle is not found.");
		return ciFalse;
	}
	ciString msgData = "STARTER.APP_AGENT.ARGUMENT";
	msgData += "=";
	msgData += newValue;

	COPYDATASTRUCT appInfo;
	appInfo.dwData = 300;  // Set Property
	appInfo.lpData = (char*)msgData.c_str();
	appInfo.cbData = msgData.length() + 1;

	::SendMessage(sttHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
	
	myDEBUG("sendMessage to STT (ARG=%s)", newValue.c_str());

	string strExecute = EXE_PATH;
	strExecute += "kill.exe UTV_agtServer";
	
	myDEBUG("WinExec %s", strExecute.c_str());
	int nRet = WinExec(strExecute.c_str(), SW_NORMAL);
	if(nRet < 31) {
		myERROR("%s fail", strExecute.c_str());
		return ciFalse;
	}
	myDEBUG("shutdownTime %s complete", timeStr.c_str());
	return ciTrue;
}

ciBoolean
tcpSocketServer::keyboardSimulator(const char* command, const char* data)
{
	myDEBUG("keyboardSimulator(%s,%s)", command, data);
	char buf[BUF_LEN+1];
	
	list<string>  aList,  bList;

	int j=0;
	int len=strlen(command);
	for(int i=0;i<len;i++){
		if(command[i] == '+') {
			buf[j]=0;
			if(strlen(buf)){
				aList.push_back(buf);
			}
			memset(buf,0x00, BUF_LEN+1);
			j=0;
			continue;
		}else{
			buf[j]=command[i];
			j++;
		}
	}
	aList.push_back(buf);

	_getFocus();

	list<string>::iterator itr;
	for(itr=aList.begin();itr!=aList.end();itr++){
		_kbdEvent((*itr).c_str(),0);
		bList.push_front((*itr));
	}
	//Sleep(1000);
	list<string>::iterator jtr;
	for(jtr=bList.begin();jtr!=bList.end();jtr++){
		_kbdEvent((*jtr).c_str(),KEYEVENTF_KEYUP);
	}
	return ciTrue;
}

int 
tcpSocketServer::_kbdEvent(const char* value, DWORD flag)
{
	//mylog(out,"%s", value);

	int i=0;
	for(i=1;i<CODE_LEN;i++){
		const char* str = scanString[i];
		if(strcmp(value,str)==0){
			unsigned char vCode = virtualCode[i];
			if(vCode){
				keybd_event(vCode,0,flag,0);
				if(flag==0){
					myDEBUG("%s key pushed", value);
				}else{
					myDEBUG("%s key released", value);
				}
			}else{
				myDEBUG("%s is invalid key", value);
			}
			break;
		}
	}
	return i;
}

int 
tcpSocketServer::_getFocus()
{
	HWND hWnd = ::FindWindow(NULL, "UTV_BRW"); 
	if(hWnd == NULL){
		//myERROR("find program failed");
		return 0;
	}

	::SetFocus(hWnd);
	BOOL bRet = ::SetForegroundWindow(hWnd);
	if(!bRet)  {
		myERROR("set foreground window failed");
		return 0;
	}
	myDEBUG("set foreground window");

	int i;
	for(i=0;i<50;i++){
		HWND aWnd = GetForegroundWindow();
		if(aWnd == hWnd){
			break;
		}
		Sleep(100);
	}
	if(i==50){
		myERROR("error ; set forground failed");
		return 0;
	}
	return 1;
}

string 
tcpSocketServer::_curTimeStr() 
{
    time_t current;
    time(&current);

    char* str;
    struct tm aTM;
#ifdef _WIN32
    struct tm *temp = localtime(&current);
    if (temp) {
        aTM = *temp;
    } else {
        memset((void*)&aTM, 0x00, sizeof(aTM));
    }
#else
    localtime_r(&current, &aTM);
#endif

    str = (char*)malloc(sizeof(char)*20);
    sprintf(str,"%4d-%02d-%02d %02d:%02d:%02d",
        aTM.tm_year+1900,aTM.tm_mon+1,aTM.tm_mday,
        aTM.tm_hour,aTM.tm_min,aTM.tm_sec);
    string strTime = str;
    free(str);
    return strTime;
}

ciBoolean
tcpSocketServer::_setData(ciString& data)
{
	_dataMap.clear();
	
	if(data.empty()){
		return ciFalse;
	}

	myDEBUG("_setData(%s)", data.c_str());

	ciStringTokenizer aTokens(data.c_str(),",");

	while(aTokens.hasMoreTokens()){
		ciString tok =aTokens.nextToken();
		ciString name,value;
		ciStringUtil::divide(tok,'=', &name, &value);

		_dataMap.insert(ciStringMap::value_type(name,value));
	}
	return ciTrue;

}

const char*
tcpSocketServer::_getData(const char* name)
{
	ciStringMap::iterator itr = _dataMap.find(name);

	if(itr!=_dataMap.end()){
		return itr->second.c_str();
	}
	return "";
}


ciBoolean
tcpSocketServer::_readXml()
{
	ciString file = EXE_PATH;
	file += "\\flash\\xml\\channel.xml";

	ifstream in(file.c_str());
	if(in.fail()){
		myERROR("%s file open error", file.c_str());
		return ciFalse;
	}

	myDEBUG("readXml(%s)", file.c_str());

	ciBoolean newLine=ciTrue;
	_xmlBuf = "";
	while(!in.eof()){
		char buf[1024];
		memset(buf,0x00,sizeof(buf));
		in.getline(buf,sizeof(buf)-1);
		if(!newLine){
			ciStringUtil::leftTrim(buf);
		}
		if(_isEOLXml(buf)){
			_xmlBuf += buf;
			_xmlBuf += "\n";
			newLine=ciTrue;
		}else{
			_xmlBuf += buf;
			_xmlBuf += " ";
			newLine=ciFalse;
		}
	}
	in.close();

	myDEBUG("read xml file %s : \n%s", 
		file.c_str(),_xmlBuf.substr(0,(_xmlBuf.size()>3000?3000:_xmlBuf.size())).c_str());
	return ciTrue;
}
ciBoolean
tcpSocketServer::_writeXml()
{
	ciString file = EXE_PATH;
	file += "\\flash\\xml\\channel.xml";

	ofstream out(file.c_str());
	if(out.fail()){
		myERROR("%s file open error", file.c_str());
		return ciFalse;
	}

	myDEBUG("writeXml(%s)", file.c_str());

	out.write(_xmlBuf.c_str(), _xmlBuf.size());
	if(out.fail()){
		myERROR("%s file write error", file.c_str());
		out.close();
		return ciFalse;
	}
	out.close();
	myDEBUG("write xml file %s : \n%s", file.c_str(), 
		_xmlBuf.substr(0,(_xmlBuf.size()>3000?3000:_xmlBuf.size())).c_str());
	return ciTrue;
}

ciBoolean
tcpSocketServer::_isEOLXml(const char* xmlLine)
{
	ciString buf = xmlLine;
	ciStringUtil::rightTrim(buf);
	int len = buf.size();
	if(len<1) {
		//myDEBUG("Not End of XML Line");
		return ciFalse;
	}
	if(buf[len-1]=='>'){
		//myDEBUG("End of XML Line");
		return ciTrue;
	}
	//myDEBUG("Not End of XML Line");
	return ciFalse;
}

ciBoolean
tcpSocketServer::_channelModify(const char* ch, const char* channelName)
{
	myDEBUG("_channelModify(%s,%s)", ch, channelName);

	ciString keyword = "name=\"";
	keyword += ch;
	keyword += "\"";

	ciString verify = "set_ad=\"";

	ciString target = "set_ad=\"";
	target += channelName;
	target += "\"";

	myDEBUG("key=%s, target=%s", keyword.c_str(), target.c_str());

	ciBoolean matched = ciFalse;
	ciString buf="";
	ciStringTokenizer aTokens(_xmlBuf.c_str(),"<>/ \t\n",false,true);
	while(aTokens.hasMoreTokens()){
		ciString aTok=aTokens.nextToken();
		if(aTok==keyword){
			myDEBUG("%s key matched", keyword.c_str());
			matched =ciTrue;
			buf += aTok;
			continue;
		}
		if(matched && strncmp(aTok.c_str(),verify.c_str(),verify.size())==0){
			buf += target;
			matched = ciFalse;
			continue;
		}
		buf += aTok;	
	}
	
	_xmlBuf = buf;

	//myDEBUG("result=\n%s", _xmlBuf.c_str());
	_writeXml();
	return ciTrue;
}

ciBoolean
tcpSocketServer::_channelAdd(const char* channelName,
							  const char* siteName,
							  const char* desc,
							  const char* networkUse)
{
	myDEBUG("_channelAdd(%s,%s,%s,%s)",channelName,siteName,desc,networkUse);

	//<ad name="ATC" desc="Demo for ATC" sitename="ERAE" networkuse="0" /> 

	char line[1024];
	memset(line,0x00,1024);
	sprintf(line,"\t\t<ad name=\"%s\" desc=\"%s\" sitename=\"%s\" networkuse=\"%d\" />\n",
			channelName,desc,siteName,atoi(networkUse));

	ciBoolean matched = ciFalse;
	ciString buf = "";	
	ciStringTokenizer aTokens(_xmlBuf.c_str(), "\n");
	while(aTokens.hasMoreTokens()){
		ciString aTok = aTokens.nextToken();
		if(!matched && ciStringUtil::wildcmp("*</ad_info>*",aTok.c_str())){
			buf += line;
			matched = ciTrue;
		}
		buf += aTok;
		buf += "\n";
	}

	if(!matched){
		myERROR("</ad_info> not found");
		return ciFalse;
	}
	_xmlBuf = buf;
	_writeXml();

	return ciTrue;
}

ciBoolean
tcpSocketServer::xmlRefresh()
{
	myDEBUG("xmlRefresh()");

	if(!_readXml()){
		myERROR("read XML failed");
		return ciFalse;
	}

	// listup ini file
	
	ciString dir = EXE_PATH;
	dir += "\\config\\*.ini";

	ciStringList iniList;
	installUtil::getInstance()->getFileList(dir.c_str(), iniList);

	ciStringList xmlLineList;
	ciStringList xmlCompareLineList;
	_buildXMLLine(iniList, xmlLineList, xmlCompareLineList);

	ciBoolean start = ciFalse;
	ciBoolean end = ciFalse;
	ciString buf = "";	
	ciStringTokenizer aTokens(_xmlBuf.c_str(), "\n");
	ciBoolean skip = ciFalse;
	while(aTokens.hasMoreTokens()){
		ciString aTok = aTokens.nextToken();
		if(!end && !start && ciStringUtil::wildcmp("*<ad_info*",aTok.c_str())){
			start = ciTrue;
		}
		if(!end && start && ciStringUtil::wildcmp("*</ad_info>*",aTok.c_str())){
			start = ciFalse;
			end = ciTrue;
			ciStringList::iterator itr;
			for(itr=xmlLineList.begin();itr!=xmlLineList.end();itr++){
				buf += itr->c_str();
			}
		}
		if(start){
			if(ciStringUtil::wildcmp("*networkuse=\"0\"*",aTok.c_str())){
				myDEBUG("%s skipped", aTok.c_str());
				continue;
			}else{
				skip=ciFalse;
				ciStringList::iterator itr;
				for(itr=xmlCompareLineList.begin();itr!=xmlCompareLineList.end();itr++){
					if(ciStringUtil::wildcmp(itr->c_str(),aTok.c_str())){
						myDEBUG("%s skipped", aTok.c_str());
						skip = ciTrue;
						break;
					}
				}
			}
		}
		if(skip){
			skip=ciFalse;
			continue;
		}
		buf += aTok;
		buf += "\n";
	}
	_xmlBuf = buf;
	_writeXml();
	return ciTrue;
}

void
tcpSocketServer::_buildXMLLine(ciStringList& iniList,
							  ciStringList& outXMLLineList,
							  ciStringList& outXMLCompareLineList)
{
	ciStringList::iterator itr;
	for(itr=iniList.begin();itr!=iniList.end();itr++){
		ciString filename = itr->c_str();

		if(strncmp(filename.c_str(),"__template_", strlen("__template"))==0){
			continue;
		}
		ciString channelName, ext;
		ciStringUtil::divide(filename,'.',&channelName,&ext);

		ciString siteName,desc,networkUse="0";
		_getIniInfo(channelName.c_str(),siteName,desc,networkUse);

		char line[1024];
		memset(line,0x00,1024);
		sprintf(line,"\t\t<ad name=\"%s\" desc=\"%s\" sitename=\"%s\" networkuse=\"%s\" />\n",
			channelName.c_str(),desc.c_str(),siteName.c_str(),networkUse.c_str());
		
		outXMLLineList.push_back(line);

		memset(line,0x00,1024);
		sprintf(line,"*<ad name=\"%s\"*",channelName.c_str());
		outXMLCompareLineList.push_back(line);
	}
}


ciBoolean
tcpSocketServer::_getIniInfo(const char* channelName,
							  ciString& out_siteId,ciString& out_desc, ciString& out_networkUse)
{
	myDEBUG("_getIniInfo(%s)", channelName);

		// 1. read ini file

	string filename=EXE_PATH;
	filename += "\\config\\";
	filename += channelName;
	filename += ".ini";

	char buf[1024];
	memset(buf,0x00,1024);
	GetPrivateProfileString("Host",
							"description",
							"",
							buf,
							sizeof(buf),
							filename.c_str());

	myDEBUG("Description=%s\n", buf);
	out_desc=buf;

	memset(buf,0x00,1024);
	GetPrivateProfileString("Host",
							"networkUse",
							"0",
							buf,
							sizeof(buf),
							filename.c_str());

	myDEBUG("networkUse=%s\n", buf);
	out_networkUse=buf;


	memset(buf,0x00,1024);
	GetPrivateProfileString("Host",
							"site",
							"",
							buf,
							sizeof(buf),
							filename.c_str());

	myDEBUG("site=%s\n", buf);
	out_siteId=buf;

	return ciTrue;
}

void
tcpSocketServer::myLogOpen(const char *filename) 
{
	ciString logFile = "";
	if(filename && strlen(filename)){
		logFile = filename;
	}else{
		logFile += ciWorld::getProcName();
		logFile += ".log";
	}
	
	// log file open
	_log.open(logFile.c_str(), ios_base::out);
	if( _log.fail() ) {
		printf("%s open failed.\n", logFile.c_str());
		_hasLog=ciFalse;
		return ;
	} 
	printf("%s has opened.\n", logFile.c_str());
	_hasLog=ciTrue;
	return;
}

void
tcpSocketServer::myLogClose() 
{
	if(_hasLog) _log.close();
	printf("log has closed.\n");
	return;

}
void
tcpSocketServer::myDEBUG(const char *pFormat,...) 
{
 	char	aBuf[4096];
	va_list	aVp;
	va_start(aVp, pFormat);
	vsprintf(aBuf, pFormat, aVp);

	printf("%s\n", (const char*)aBuf);
	if(_hasLog){
		_log << aBuf << endl;
	}
}
void
tcpSocketServer::myERROR(const char *pFormat,...) 
{
 	char	aBuf[4096];
	va_list	aVp;
	va_start(aVp, pFormat);
	vsprintf(aBuf, pFormat, aVp);

	printf("ERROR : %s\n", (const char*)aBuf);
	if(_hasLog){
		_log << "ERROR : " << aBuf << endl;
	}
}
void
tcpSocketServer::myWARN(const char *pFormat,...) 
{
 	char	aBuf[4096];
	va_list	aVp;
	va_start(aVp, pFormat);
	vsprintf(aBuf, pFormat, aVp);

	printf("WARN : %s\n", (const char*)aBuf);
	if(_hasLog){
		_log << "WARN : " << aBuf << endl;
	}
}

