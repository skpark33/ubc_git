// FWVDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "WebBrowser/ShockwaveFlash.h"
#include "SyncSocket/SocketListenWnd.h"


// CFWVDlg 대화 상자
class CFWVDlg : public CDialog
{
// 생성입니다.
public:
	CFWVDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FWV_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	BOOL setEdition();
	static UINT WaitForWindowCreated(LPVOID pParam);
	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);

	void _initTray();
	afx_msg LONG TrayIconMessage(WPARAM wParam,LPARAM lParam);
	//afx_msg LONG TrayIconUpdate(WPARAM wParam,LPARAM lParam);
	afx_msg void OnTrayOpen();
	afx_msg void OnTrayClose();
	//afx_msg void OnTrayAbout();	
	//afx_msg void OnTrayUpdate();

// 구현입니다.
protected:
	HICON m_hIcon;
	CString	m_edition;

	int	disable_area_y;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	BOOL OnDeviceChange( UINT nEventType, DWORD_PTR dwData ); // skpark USB_Device


// skpark
	const char*	_getIp();
	const char* _getComputerName();
	const char* _getVersion();


	BOOL Play();
	CShockwaveFlash* m_pFlash;
	int getFocus(const char* appTitle);
	void HostNameIdentify();

	void felicaServer();

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CStatic m_ctrlPictureFrame;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
	//afx_msg void OnClose();
	CStatic logoVar;
	
	void	setEdition(const char* edition) { m_edition = edition; }
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

	BOOL	isMinimize;

	HANDLE	m_hDevNotify ;  // skpark USB_Device
	void	_initDeviceChangeInterface(); // skpark USB_Device
	ciBoolean	_getTargetContentsPackage(const char* path, ciString& targetName); // skpark USB_Drive
	ciBoolean   _autoImport; // skpark USB_Device
	//virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam); // skpark USB_Device_2

	void	_showCacheServerImage();
	CStatic cacheVar;
	CStatic cacheVar2;

protected:
	CSocketListenWnd	m_socket_wnd;
};
