#ifndef _autoUpdateTimer_H_
#define _autoUpdateTimer_H_

#include <ci/libTimer/ciTimer.h>

class autoUpdateTimer : public ciPollable {
protected:
	int _interval;

public:
	autoUpdateTimer();
	virtual ~autoUpdateTimer();

	virtual void initTimer(const char* pname, int interval = 60);
	virtual void processExpired(ciString name, int counter, int interval);
	ciBoolean autoUpdate();

protected:

	ciBoolean	_getContentsDownloadTime();
	
	ciString	_contentsDownloadTime;
	ciString	_lastCheckTime;
};

#endif //_autoUpdateTimer_H_
