/*
 * 
 *	Copyright . 2001 WINCC Inc.
 *	All Rights Reserved.
 *
 *	This source code is confidential and proprietary and may not be used
 *	or distributed without the written permission of WINCC Inc.
 *
 *	Created by :  wegf
 *	Modified by : 
 *	Last update : 2001/04/18 16:23:52
 *	Comment :
 */
#ifndef _tcpSocketHandler_h_
#define _tcpSocketHandler_h_

#include <ci/libBase/ciBaseType.h>
#include <ace/SOCK_Stream.h>
#include <ace/Svc_Handler.h>
#include <ace/Synch.h>

class tcpSocketHandler : public ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_MT_SYNCH>
{
public:
	tcpSocketHandler();
	
	virtual ~tcpSocketHandler();

	virtual int open(void *);
	virtual int close(u_long p_val);
	virtual int svc();

	virtual int handle_close(ACE_HANDLE pHandle = ACE_INVALID_HANDLE,
								ACE_Reactor_Mask pMask = ACE_Event_Handler::ALL_EVENTS_MASK);

	
protected:
	typedef ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_MT_SYNCH> inherited;	

};


#endif //_tcpSocketHandler_h_
