#ifndef _tcpSocketAcceptor_h_
#define _tcpSocketAcceptor_h_

#include <ace/SOCK_Acceptor.h>
#include <ace/Acceptor.h>
#include "firmwareView/tcpSocketSession.h"



class tcpSocketAcceptor : public ACE_Acceptor<tcpSocketSession, ACE_SOCK_ACCEPTOR> {
public:
	tcpSocketAcceptor();
	virtual ~tcpSocketAcceptor();
		

    virtual int make_svc_handler(tcpSocketSession*& pSvcHandler);
    virtual int activate_svc_handler(tcpSocketSession* pSvcHandler);

protected:
	typedef ACE_Acceptor<tcpSocketSession, ACE_SOCK_ACCEPTOR> inherited;
};

#endif
