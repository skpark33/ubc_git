#ifndef _GPQSTimer_H_
#define _GPQSTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <hi/libHttp/ubcMux.h>

class AGTClientSession;

class GPQSInfoData {
public:
	GPQSInfoData() : isDiff(ciFalse), isDownloaded(ciFalse) {} 
    ciString		file_line;
    ciString		jsp_line;
	ciBoolean		isDiff;
	ciBoolean		isDownloaded;
	ciString		tablename;

	void print();
};

class GPQSTimer : public ciWinPollable {
public:

	typedef map<int, GPQSInfoData*>	GPQSMAP;
	typedef map<ciString, ciStringSet*>		LISTMAP;

	GPQSTimer(ciString& site, ciString& host, ciBoolean isTotal, ciBoolean isAll);
	virtual ~GPQSTimer();

	virtual void processExpired(ciString name, int counter, int interval);

	void clearMap();
	void printMap();

	static const char* getLastGetTime() ;
	static const char* getLastErrMsg();
	static void	setLastGetTime(const char* p);
	static void	setLastErrMsg(const char* p, const char* errLevel="2");
	static void	setLastErrMsg1(const char* format, const char* p, const char* errLevel="2"); 
	static void	setLastErrMsg2(const char* format, const char* p1, const char* p2, const char* errLevel="2");
	static void setAGTClientSession(AGTClientSession* session) { _agtSession = session; }

protected:
	ciBoolean	_cleanDataFolder();
	ciBoolean	_reget();

	void		_makePath(const char* relpath, ciString& outval);

	void		_readXmlFile();
	void 		_writeXmlFile();
	ciBoolean 	_writeXmlFile(ciString& filename, CStringArray& line_list);

	ciBoolean _getGPQSInfo(const char* url, CString& sendMsg, CStringArray& line_list, const char* errLevel="2");
	int _compareGPSQInfo(CStringArray& line_list);
	int _downloadData();
	int _removeOldData();
	int _moveData();
	int _makeFileList();
	int _makeFileListTotal();

	ciBoolean _getGPQSData(ciString& tablename);
	ciBoolean _getGPQSData(const char* jsp, const char* filename);
	ciBoolean _getGPQSDataList(const char* jsp, ciStringList& dataList);

	int _getCarList(const char* csrs,const char* corp,int month, ciStringList& carList);

	void _lockData(int val, const char* nowStr);

	void _clearFileListMap();

	ciBoolean	_getItem(const char* tag, ciString& line);
	ciBoolean	_getCorpValue(ciString& filename, ciString& outval);

	ciBoolean	_isCMSError(CStringArray& line_list) ;

	ciBoolean	_isPass(int idx, const char* corp=0);

	void _P01();
	void _P02();
	void _P03();
	void _P04();
	void _P05();
	void _P06();
	void _P07();
	void _P08();
	void _P09();
	void _P10();
	void _P11();
	void _P12();
	void _P13();
	void _P14();

	void _clearDone();
	ciBoolean _page_done[14];

	//int			_deleteOldFile(ciShort durationDay);

	ciMutex		_mapLock;
	GPQSMAP	_map;

	TCHAR _cDrive[MAX_PATH], _cPath[MAX_PATH];

	ciString _site;
	ciString _host;
	ciString _corpFile;
	ciString _carFile;
	ciString _fileListFile;
	ciString _infoFile;
	ciString _lockFile;
	ciString _dataFolder;
	ciString _tempFolder;
	ciStringList _corpList;
	ciString _server;
	ciULong _port;

	ciBoolean _isTotal;
	ciBoolean _isAll;
	ciBoolean _pass[11];
	ciBoolean _regetFlag;

	ciBoolean _isDoing;
	ciMutex _isDoingLock;
	void _startMark();
	void _endMark();
	ciBoolean _isStart();

	static ciString _nowStr;
	static ciMutex	_msgLock;
	static ciString _lastGetTime;
	static ciString _lastErrMsg;
	static AGTClientSession* _agtSession;

	LISTMAP _fileListMap;
};

#endif //_GPQSTimer_H_
