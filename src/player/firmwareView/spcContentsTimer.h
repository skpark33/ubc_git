#ifndef _spcContentsTimer_H_
#define _spcContentsTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <hi/libHttp/ubcMux.h>

class spcContentsData {
public:
	spcContentsData() : zorder(0), volume(0), alreadyDownload(ciFalse) {}
	ciShort			zorder;
	ciString		filename;
	ciULong			volume;
	ciBoolean		alreadyDownload;

	void print();
};
typedef map<int, spcContentsData*> SpcContentsMAP;

class spcContentsTimer : public ciWinPollable
{
public:
	spcContentsTimer(ciString& site, ciString& host);
	virtual ~spcContentsTimer(void);

	virtual void processExpired(ciString name, int counter, int interval);

	void		clearMap();
	void		printMap();

protected:

	void		_makePath(const char* relpath, CString& outval);
	ciBoolean	_makeSometimeContentsApiXml();
	ciBoolean	_makeRealtimeContentsApiXml();
	ciBoolean	_getSpcContents(const char* url, CString& sendMsg, CStringArray& line_list);
	int			_setSpcContents(CStringArray& line_list);
	int			_downloadContents();
	int			_removeContents();
	int			_removeContents(int counter, CString filepath, CString filename);
	ciBoolean	_isContents(ciString& filename);
	ciBoolean	_isExist(ciString& filename);
	unsigned long	_getFileSize(const char* filename);
	void 		_makeHostXml();
	ciBoolean	_writeXmlFile(ciString& filename, CStringArray& line_list);
	ciBoolean	_getSpcContentsApi(const char* url, CStringArray& line_list);
	ciBoolean	_getSpcContentsApi(const char* jsp, const char* filename);

	TCHAR		_cDrive[MAX_PATH];
	TCHAR		_cPath[MAX_PATH];

	ciString	_hostFile;
	ciString	_apiFolder;
	ciBoolean	_apiWriteSuccess;

	ciMutex		_mapLock;
	ciString	_site;
	ciString	_host;

	ciString	_server;
	ciULong		_port;

	SpcContentsMAP	_map;

};

#endif //_spcContentsTimer_H_
