//#include "stdafx.h"
/*! \file tcpSocketWorld.C
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
*/

//#include "stdafx.h"
//#include "MsgBox.h"
#include "tcpSocketWorld.h"

#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciEnv.h>
#include <common/libInstall/installUtil.h>
#include <common/libCommon/ubcIni.h>



ciSET_DEBUG(10, "tcpSocketWorld");

tcpSocketWorld::tcpSocketWorld(const char* edition) {
	//ciDEBUG(1,("tcpSocketWorld()"));
	//_tcpServer=0;

	_edition = edition;
}

tcpSocketWorld::~tcpSocketWorld() {
	//ciDEBUG(1,("~tcpSocketWorld()"));
}

ciBoolean 
tcpSocketWorld::init(int& p_argc, char** &p_argv) 
{
	ciDEBUG(1,("tcpSocketWorld::init(%d)", p_argc));	
	
	if(ciWorld::init(p_argc,p_argv) == ciFalse) {
		ciERROR(("ciWorld::init() is FAILED"));
		return ciFalse;
	}
	const char*  ignoreDebugEnv = getenv("IGNORE_COP_DEBUG");
	if(ignoreDebugEnv==0 || ignoreDebugEnv[0] != '1'){
		ciDebug::setDebugOn();
		ciWorld::ylogOn();
	}
	ciEnv::defaultEnv(true,"utv1");
	if(ciWorld::hasWatcher() == ciTrue) {}

	extraJob();
		
/*
	_watcher = ciWatcher::getInstance(_procName.c_str(), stdout);
	_watcher->init();
	_console = new ciConsoleHandler();
*/	
	// +w is Default Option
	
	tcpSocketSession aSession;
	aSession.xmlRefresh();

	return ciTrue;
}

void
tcpSocketWorld::extraJob()
{

	// SiteID �� UBCVariables.ini �� �Űܳ��´�.
	//installUtil::getInstance()->propertyMigration1();

	// Starter ���� �������� �߿���� �� ���� �ߵ��� �Ұ�
	//installUtil::getInstance()->runBrowserAfterFirmware(_edition);
	
	//ciString dsService;
	//ciXProperties::getInstance()->get("COP.NAME_GROUP.Site",dsService);
	//if(dsService!="PM=*") {
	//	ciDEBUG(1,("DiRECTORY SERVICE ADD"));
	//	installUtil::getInstance()->setProperty("COP.DIRECTORY_SERVICE.PM/Site","UBC_SITE,mgrId,siteId",ciFalse);
	//	installUtil::getInstance()->setProperty("COP.NAME_GROUP.Site","PM=*",ciFalse);
	//}
	

}

ciBoolean
tcpSocketWorld::accept()
{
	int port=14007;
	ciString portStr;
	if(ciArgParser::getInstance()->getArgValue("+port", portStr)){
		port = atoi(portStr.c_str());
	}
	ciBoolean open_flag = ciFalse;
	ACE_INET_Addr rAddr(port);
	int i=0;
	while(1){
		if( _acceptor.open(rAddr) < 0 ) {
			ciERROR(("session's open() for Port[%d] is FAILED (%d)", port,i ));
			::Sleep(1000*180);  // 3�а� ����. TIME_WAIT
			continue;
		}
		open_flag = ciTrue;
		break;
	}
	if(open_flag){
		ciDEBUG(1,("Socket accepted(%ld) for read", port));
		return ciTrue;
	}
	ciERROR(("session's open() for Port[%d] is FAILED", port ));
	return ciFalse;
}

ciBoolean
tcpSocketWorld::fini(long p_finiCode)
{
	ciDEBUG(1,("fini()"));
	//_acceptor.fini();
	_acceptor.close();
	ciDEBUG(1,("acceptor closed()"));

/*	
	_tcpServer->myLogClose();
	_tcpServer->stop();
	delete _tcpServer;
	_tcpServer = 0;
*/	
	//ciWorld::ylogOff();
//	return ciWorld::fini(p_finiCode);
	ciDEBUG(1,("fini end()"));
	return 1;
}



void 
tcpSocketWorldThread::init(const char* edition) 
{ 
	world = new tcpSocketWorld(edition);
	if(!world) {
		 exit(1);
	}
	if(world->init(__argc, __argv) == ciFalse) {
		 world->fini(2);
		 exit(1);
	}
}

void 
tcpSocketWorldThread::run()
{
	if(!world->accept()){
		ciERROR(("FATAL : Socket Open Failed"));
		//exit(1) ;
	}
	ciDEBUG(1,("world->run()"));
	world->run();
	//world->fini();

}

void 
tcpSocketWorldThread::destroy()
{
	ciDEBUG(1,("destroy()"));
	world->fini(0);
}