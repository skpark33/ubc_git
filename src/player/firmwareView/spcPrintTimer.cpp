#include "StdAfx.h"
#include "libDownload/Downloader.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include "ci/libDebug/ciArgParser.h"
#include "spcPrintTimer.h"
#include "common/libHttpRequest/HttpRequest.h"
#include "common/libInstall/installUtil.h"
#include <common/libScratch/scratchUtil.h>
#include <common/libCommon/ubcIni.h>
#include <GdiPlus.h>
#pragma comment(lib, "gdiplus.lib")
#include <WinSpool.h>
#pragma comment(lib, "winspool.lib")

ciSET_DEBUG(9, "spcPrintTimer");

void
spcPrintData::print()
{
	ciDEBUG(1,("%d,%s,%d,%d,%d",
		zorder,
		filename.c_str(),
		volume,
		alreadyDownload,
		printResult
		));
}

spcPrintTimer::spcPrintTimer(ciString& site, ciString& host)
{
	_site = site;
	_host = host;

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, _cDrive, _cPath, cFilename, cExt);

	ciDEBUG(3, ("spcPrintTimer()"));

	ubcConfig aIni("UBCConnect");

	aIni.get("SPC","CMSServer",_server);
	if(_server.empty()){
		_server = "125.141.230.154";
	}

	_port = 0;
	aIni.get("SPC","CMSPort",_port);
	if(_port == 0) _port = 8090;

	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&_gdiplusToken, &gdiplusStartupInput, NULL);
}

spcPrintTimer::~spcPrintTimer(void)
{
	ciDEBUG(3, ("~spcPrintTimer()"));

	Gdiplus::GdiplusShutdown(_gdiplusToken);
}

void
spcPrintTimer::processExpired(ciString name, int counter, int interval)
{
	// 이 타이머는 30초마다 한번씩 호출된다.
	ciTime now;
	char nowStr[20];
	memset(nowStr,0x00,20);
	sprintf(nowStr, "%04d%02d%02d%02d%02d%02d", 
		now.getYear(),now.getMonth(),now.getDay(),now.getHour(), now.getMinute(),now.getSecond());

	ciDEBUG(3, ("processExpired(%s,%s,%d,%d)", nowStr, name.c_str(), counter, interval));

	if (counter <= 1) {
		// 첫회는 건너뛴다.
		return;
	}

	CStringArray line_list;

	CString printReadyUrl;
	printReadyUrl.Format("/spc_cms/contentsProofShotPrintReady.do?id=%s", _host.c_str());
	if(_getSpcPrintPhoto(printReadyUrl, line_list)){
		if(!_setSpcPrintPhoto(line_list)) {
			ciWARN(("No spcPrintPhoto changed"));
		}
	}

	if(!_downloadPrintPhoto()) {
		ciWARN(("No downloadPrintPhoto count"));
	}

	if(!_printPhoto()){
		ciWARN(("No printPhoto count"));
	}

	// 삭제하지 않는다. (spcContentsTimer에서 삭제)
	//if (counter % 5 == 0) {
	//	// 5회 반복마다 실행한다.
	//	if(!_removePrintPhoto()){
	//		ciWARN(("No removePrintPhoto count"));
	//	}
	//}
}

void
spcPrintTimer::clearMap()
{
	ciDEBUG(1,("clearMap()"));
	ciGuard aGuard(_mapLock);

	SpcPrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		delete (itr->second);
	}
	_map.clear();
}

void
spcPrintTimer::printMap()
{
	ciDEBUG(1,("printMap()"));
	ciGuard aGuard(_mapLock);
	SpcPrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		itr->second->print();
	}
}

void
spcPrintTimer::_makePath(const char* relpath, CString& outval)
{
	CString strPath;
	strPath.Format(_T("%s%s..\\..\\%s"), _cDrive, _cPath, relpath);
	CFileStatus status;
	CFile::GetStatus(strPath, status );
	outval = status.m_szFullName;
}

ciBoolean
spcPrintTimer::_getSpcPrintPhoto(const char* url, CStringArray& line_list)
{
	ciDEBUG(3, ("_getSpcPrintPhoto(%s)", url));
	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _server.c_str(), _port);
	BOOL ret_val = http_request.RequestGet(url,line_list);
	if(!ret_val) {
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	}

	BOOL getSucceed = false;
	if(line_list.GetCount() > 1 )
	{
		CString first_line =  line_list.GetAt(0);
		if(first_line.GetLength() >= 2 && first_line.Mid(0,2).CompareNoCase("OK") == 0) 
		{
			getSucceed = true;
		}
	}

	if(!getSucceed){
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	}

	return ciTrue;
}

int
spcPrintTimer::_setSpcPrintPhoto(CStringArray& line_list)
{
	ciDEBUG(3, ("_setSpcPrintPhoto()"));

	//clearMap();

	int retval = 0;
	spcPrintData* aData = NULL;

	int count = line_list.GetCount();
	for(int i=0; i<count; i++)
	{
		ciString line = line_list.GetAt(i);

		ciString key, value;
		ciStringUtil::divide(line,'=',&key,&value);

		if(key == "zorder") {
			ciGuard aGuard(_mapLock);
			SpcPrintMAP::iterator itr =_map.find(atoi(value.c_str()));
			if(itr != _map.end()){
				aData = itr->second;
				ciDEBUG(1,("%d data founded", aData->zorder));
				retval++;
			}else{
				aData = new spcPrintData();
				aData->zorder = atoi(value.c_str());
				_map.insert(SpcPrintMAP::value_type(aData->zorder, aData));
				ciDEBUG(1,("%d data inserted", aData->zorder));
			}
			continue;
		}
		if(aData==0){
			continue;
		}

		if(key == "filename")
		{
			ciDEBUG(1,("filename=%s", value.c_str()));
			aData->filename = value;
		}
		else if(key == "volume")
		{
			unsigned long buf = strtoul(value.c_str(),NULL,10);
			if(aData->volume != buf) {
				ciDEBUG(1,("chanaged"));
				aData->volume = buf;
				aData->alreadyDownload = ciFalse;
				aData->printResult = ciFalse;
				retval++;
			}
		}
	}

	printMap();
	return retval;
}

int
spcPrintTimer::_downloadPrintPhoto()
{
	ciDEBUG(3, ("_downloadContents()"));

	int counter=0;
	CDownloader libDownloader;
	SpcPrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		spcPrintData* aData = itr->second;
		aData->print();
		if(!aData->filename.empty() && aData->alreadyDownload == ciFalse) {
			if(libDownloader.GetFile(aData->filename.c_str(), aData->volume, "", _site.c_str(), "spc")) {
				unsigned long downloadedSize = _getFileSize(aData->filename.c_str());
				if(downloadedSize == aData->volume) {
					ciDEBUG(1,("spc/%s file download succeed", aData->filename.c_str()));
					aData->alreadyDownload = ciTrue;
					counter++;
				}else{
					ciERROR(("spc/%s file download failed (filesize(%ld) != downloadedSize(%ld)"
						, aData->filename.c_str(), aData->volume, downloadedSize));
				}
			}else{
				ciERROR(("spc/%s file download failed", aData->filename.c_str()));
			}
		}
	}
	ciDEBUG(1,("%d data downloaded", counter));
	return counter;
}

int
spcPrintTimer::_removePrintPhoto()
{
	ciDEBUG(3, ("_removePrintPhoto()"));

	int counter=0;

	CString contentsDir, filename;
	_makePath("Contents\\Enc\\spc", contentsDir);

	SpcPrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		spcPrintData* aData = itr->second;
		if(!aData->filename.empty() && _isExist(aData->filename) == ciTrue && aData->alreadyDownload == ciTrue && aData->printResult == ciTrue) {
			ciString filePath = contentsDir;
			filePath += "\\";
			filePath += aData->filename;

			if(::access(filePath.c_str(),0) == 0) {
				counter++;
				::remove(filePath.c_str());
				ciDEBUG(5,("%s file deleted", filePath.c_str()));
			}
		}
	}

	ciDEBUG(1,("%d file removed", counter));
	return counter;
}

int
spcPrintTimer::_printPhoto()
{
	ciDEBUG(3, ("_printPhoto()"));

	int counter=0;

	CString contentsDir, filename;
	_makePath("Contents\\Enc\\spc", contentsDir);

	SpcPrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		spcPrintData* aData = itr->second;
		if(!aData->filename.empty() && _isExist(aData->filename) == ciTrue && aData->alreadyDownload == ciTrue && aData->printResult == ciFalse) {
			ciString filePath = contentsDir;
			filePath += "\\";
			filePath += aData->filename;

			ciString printResult;
			if (_printAction(filePath) == ciTrue) {
				printResult = "DONE";
			} else {
				printResult = "FAIL";
			}

			CString strHttpResult = "";
			CString strResultUrl = "";
			strResultUrl.Format("/spc_cms/contentsProofShotPrintResult.do?id=%s&pid=%d&rst=%s", _host.c_str(), aData->zorder, printResult.c_str());

			CHttpRequest http_request;
			http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _server.c_str(), _port);
			BOOL ret_val = http_request.RequestGet(strResultUrl, strHttpResult);
			if(!ret_val) {
				ciERROR((http_request.GetErrorMsg()));
			} else {
				aData->printResult = ciTrue;
				ciDEBUG(5,("%s result send", strResultUrl));
			}

			counter++;
		}
	}

	return counter;
}

ciBoolean
spcPrintTimer::_printAction(ciString& filename)
{
	ciDEBUG(3, ("_printAction(%s)", filename.c_str()));

	CPrintDialog printDlg(FALSE);
	printDlg.GetDefaults();
	printDlg.m_pd.Flags &= ~PD_RETURNDEFAULT;

	if (_isPrinterOnline(printDlg.GetDeviceName()) == false) {
		ciERROR(("printer(%s) status fail", printDlg.GetDeviceName()));
		return ciFalse;
	}

	BSTR bstrFilePath;
	LPCTSTR_to_BSTR(&bstrFilePath, filename.c_str());

	Gdiplus::Bitmap *pPrintImage = Gdiplus::Bitmap::FromFile(bstrFilePath);

	LPDEVMODE dev = printDlg.GetDevMode();
	GlobalUnlock(dev);
	dev->dmOrientation=DMORIENT_PORTRAIT;
	dev->dmPaperSize=DMPAPER_JAPANESE_POSTCARD;

	CDC dc;
	if (!dc.Attach(printDlg.GetPrinterDC()))
	{
		ciERROR(("dc attach failed"));
		return ciFalse;
	}
	dc.ResetDC(dev);
	dc.m_bPrinting = TRUE;

	ciShort paperHorz = 0;
	ciShort paperVert = 0;
	ubcConfig aIni("UBCVariables");
	aIni.get("SPC", "PaperHorz", paperHorz);
	if (paperHorz == 0) paperHorz = 89;
	aIni.get("SPC", "PaperVert", paperVert);
	if (paperVert == 0) paperVert = 119;

	double f64MaxwInPixels = pPrintImage->GetWidth();
	double f64MaxhInPixels = pPrintImage->GetHeight();
	//double f64MaxwInMillimeters = dc.GetDeviceCaps(HORZSIZE);
	//double f64MaxhInMillimeters = dc.GetDeviceCaps(VERTSIZE);
	double f64MaxwInMillimeters = paperHorz;
	double f64MaxhInMillimeters = paperVert;
	double f64PixelRate = f64MaxhInPixels / f64MaxhInMillimeters;
	double f64DPI = 100;
	double f64OneMM2Pixel_width = f64MaxwInMillimeters / 25.4;
	double f64OneMM2Pixel_height = f64MaxhInMillimeters / 25.4;
	double f64OneMM2Pixel_width_rate = (f64MaxwInPixels / f64PixelRate) / 25.4;
	int left = static_cast<int>(((f64OneMM2Pixel_width - f64OneMM2Pixel_width_rate)/2)*f64DPI);
	int maxw = static_cast<int>(f64OneMM2Pixel_width_rate*f64DPI);
	int maxh = static_cast<int>(f64OneMM2Pixel_height*f64DPI);

	DOCINFO di;
	::ZeroMemory (&di, sizeof (DOCINFO));
	di.cbSize = sizeof(DOCINFO);
	di.lpszDocName = "Photo Print";

	int startResult = dc.StartDoc(&di);

	CPrintInfo Info;
	Info.SetMaxPage(1);
	Info.m_nCurPage = 1;
	Info.m_rectDraw.SetRect(0, 0, static_cast<int>(f64MaxwInPixels), static_cast<int>(f64MaxhInPixels));

	dc.StartPage(); // begin new page

	Gdiplus::Graphics graphics(dc);
	Gdiplus::Rect paperRect(left, 0, maxw, maxh);
	graphics.DrawImage(pPrintImage, paperRect);

	int endResult = dc.EndPage(); // end page

	if (endResult)
		dc.EndDoc(); // end a print job
	else
		dc.AbortDoc();  // abort job.

	dc.Detach();
	delete pPrintImage;

	return ciTrue;
}

ciBoolean
spcPrintTimer::_isPrinterOnline(CString& printerName)
{
	ciDEBUG(3, ("_isPrinterOnline(%s)", printerName));

	bool ret = true;

	if (printerName.Find("XPS") != -1 || printerName.Find("PDF") != -1)
	{
		ciERROR(("File Print not support"));
		return false;
	}

	HANDLE hPrinter;
	if (OpenPrinter((LPSTR)(LPCTSTR)printerName, &hPrinter, NULL) == 0)
	{
		ciERROR(("OpenPrinter failed"));
		return false;
	}

	DWORD dwBufsize = 0;
	PRINTER_INFO_2* pinfo = 0;
	int nRet = 0;
	nRet = GetPrinter(hPrinter, 2,(LPBYTE)pinfo, dwBufsize, &dwBufsize); //Get dwBufsize
	DWORD dwGetPrinter = 0;
	if (nRet == 0)
	{
		dwGetPrinter = GetLastError();
	}

	PRINTER_INFO_2* pinfo2 = (PRINTER_INFO_2*)malloc(dwBufsize); //Allocate with dwBufsize
	nRet = GetPrinter(hPrinter, 2, reinterpret_cast<LPBYTE>(pinfo2), dwBufsize, &dwBufsize);
	if (nRet == 0)
	{
		ciERROR(("GetPrinter failed"));
		dwGetPrinter = GetLastError();
		ret = false;
	}
	else if (pinfo2->Attributes & PRINTER_ATTRIBUTE_WORK_OFFLINE)
	{
		ciERROR(("GetPrinter Attributes(%d) : PRINTER_ATTRIBUTE_WORK_OFFLINE", pinfo2->Attributes));
		ret = false;
	}
	else if (pinfo2->Status & PRINTER_STATUS_OFFLINE)
	{
		ciERROR(("GetPrinter Status(%d) : PRINTER_STATUS_OFFLINE", pinfo2->Status));
		ret = false;
	}
	else if (pinfo2->Status & PRINTER_STATUS_NO_TONER)
	{
		ciERROR(("GetPrinter Status(%d) : PRINTER_STATUS_NO_TONER", pinfo2->Status));
		ret = false;
	}
	else if (pinfo2->Status & PRINTER_STATUS_PAPER_OUT)
	{
		ciERROR(("GetPrinter Status(%d) : PRINTER_STATUS_PAPER_OUT", pinfo2->Status));
		ret = false;
	}
	else if (pinfo2->Status & PRINTER_STATUS_PAPER_PROBLEM)
	{
		ciERROR(("GetPrinter Status(%d) : PRINTER_STATUS_PAPER_PROBLEM", pinfo2->Status));
		ret = false;
	}

	if (ret == false) {
		if (pinfo2->cJobs > 0) {
			ciWARN(("Remove Print Jobs(%d)", pinfo2->cJobs));

			// 대기열 삭제
			SetPrinter(hPrinter, 0, 0, PRINTER_CONTROL_PURGE);
		}
	}

	free(pinfo2);
	ClosePrinter( hPrinter );
	return ret;
}

ciBoolean
spcPrintTimer::_isPrintPhoto(ciString& filename)
{
	SpcPrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		spcPrintData* aData = itr->second;
		if (aData->filename == filename) {
			ciDEBUG(1,("contents %s exist", filename.c_str()));
			return ciTrue;
		}
	}
	ciDEBUG(1,("contents %s not exist", filename.c_str()));
	return ciFalse;
}

ciBoolean
spcPrintTimer::_isExist(ciString& filename)
{
	CString source;
	_makePath("Contents\\Enc\\spc\\", source);
	source += filename.c_str();
	if(::access(source,0) == 0) {
		ciDEBUG(1,("file %s exist", source));
		return ciTrue;
	}
	ciDEBUG(1,("file %s not exist", source));
	return ciFalse;
}

unsigned long
spcPrintTimer::_getFileSize(const char* filename)
{
	unsigned long retval = 0;

	CString fullpath;
	_makePath("Contents\\Enc\\spc\\", fullpath);
	fullpath += filename;

	ciDEBUG(1,("stat(%s)", fullpath));
	int fdes = open(fullpath,O_RDONLY);
	if(fdes<0){
		ciWARN(("%s file open failed", fullpath));
		return retval;
	}
	struct stat statBuf;
	if(fstat(fdes, &statBuf)==0){
		retval = statBuf.st_size;
	}
	ciDEBUG(1,("filesize(%s)=%ld", fullpath, retval));
	close(fdes);
	return retval;
}

HRESULT
spcPrintTimer::LPCTSTR_to_BSTR(BSTR *pbstr, LPCTSTR psz)
{
#ifndef UNICODE
	BSTR bstr;
	int i;
	HRESULT hr;
	// compute the length of the required BSTR
	//
	i =  MultiByteToWideChar(CP_ACP, 0, psz, -1, NULL, 0);
	if (i <= 0) { return E_UNEXPECTED; };
	// allocate the widestr, +1 for terminating null
	//
	bstr = SysAllocStringLen(NULL, i-1);
	// SysAllocStringLen adds 1
	if (bstr != NULL)
	{
		MultiByteToWideChar(CP_ACP, 0, psz, -1, (LPWSTR)bstr, i);
		((LPWSTR)bstr)[i - 1] = 0;
		*pbstr = bstr; hr = S_OK;
	}
	else
	{
		hr = E_OUTOFMEMORY;
	};
	return hr;
#else
	BSTR bstr;
	bstr = SysAllocString(psz);
	if (bstr != NULL)
	{
		*pbstr = bstr;
		return S_OK;
	}
	else
	{
		return E_OUTOFMEMORY;
	};
#endif
	// UNICODE
}
