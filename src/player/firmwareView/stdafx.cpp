// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// FWV.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Http를 사용하는지 여부를 반환한다. \n
/// @return <형: bool> \n
///			<값: 설명> \n
///			<값: 설명> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsUseHttp()
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, cDrive, cPath, cFilename, cExt);

	//Http 사용 여부
	CString strPath;
	strPath.Format(_T("%s%s\\data\\UBCVariables.ini"), cDrive, cPath);
	char cBuffer[256] = { 0x00 };
	::GetPrivateProfileString(_T("ROOT"), _T("HTTP_ONLY"), _T(""), cBuffer, 256, strPath);
	if(_tcslen(cBuffer) == 0)
	{
		return false;
	}//if
	
	return true;
}

int GetEnableShortScreenshotPeriod(void)
{
	static int enable = -1;
	if(enable >= 0) return enable;

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, cDrive, cPath, cFilename, cExt);

	//Http 사용 여부
	CString strPath;
	strPath.Format(_T("%s%s\\data\\UBCVariables.ini"), cDrive, cPath);
	char cBuffer[256] = { 0x00 };
	::GetPrivateProfileString(_T("ROOT"), _T("ENABLE_SHORT_SCREENSHOT_PERIOD"), _T("0"), cBuffer, 256, strPath);
	enable = atoi(cBuffer);

	return (enable>0);
}

LPCTSTR GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}