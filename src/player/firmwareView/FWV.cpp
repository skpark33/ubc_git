// FWV.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "FWV.h"
#include "FWVDlg.h"
#include "tcpSocketWorld.h"
#include "tcpSocketSession.h"
#include "agtScreenshotTimer.h"
#include "autoUpdateTimer.h"
#include "xmlTimer.h"
#include "udpThread.h"
#include "tcpGuestSocketThread.h"
#include "libFileTransfer/FTServer.h"
#include "common/libScratch/scratchUtil.h"
#include "common/libInstall/installUtil.h"
#include "common/libInstallWeb/installWebUtil.h"
#include "common/libCommon/ubcIni.h"
#include <ci/libWorld/ciWorld.h> 
#include <ci/libDebug/ciDebug.h> 
#include <ci/libBase/ciStringTokenizer.h> 
#include "libShutdownTimer/ShutdownTimer.h"
#include "libShutdownTimer/ShutdownThread.h"
#include "libAGTClient/AGTClientSession.h"
#include "libTimeSyncer/TimeSyncTimer.h"
#include "libServerCheck/ServerCheck.h"
#include <common/libCommon/ubcIni.h>
#include <common/libCommon/ubcUtil.h>
#include "common/libPlan/planTimer.h"
#include "reloadEventHandler.h"
#include <winsock2.h> // 기존에 윈속을 include 하고 있으면 빼셔도 됩니다.
#include "libCLITransfer/CTSocket.h"
#include "common/libCommon/SecurityIni.h"
#include <libProjectorControl/ProjectorControl.h>
#include "ci/libDebug/ciArgParser.h"
#include "ci/libConfig/ciIni.h"
#include "basketContentsTimer.h"
#include "GPQSTimer.h"
#include "spcContentsTimer.h"
#include "spcPrintTimer.h"
#include "libSysOnOffTime/SysOnOffTime.h"
#include "common/libFlashRollback/libFlashRollback.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ciSET_DEBUG(10,"CWV");

// serverIpCheckThread

void
serverIpCheckThread::run()
{
	ciDEBUG(1,("run()"));

	ciDEBUG(1,("Server Ip Check Start"));

	ubcConfig aIni("UBCConnect");
	string standby_ip="";
	string main_ip="";
	aIni.get("ORB_NAMESERVICE","STANDBY_IP", standby_ip);
	aIni.get("ORB_NAMESERVICE","IP", main_ip);

	if(standby_ip.empty()) {
		CServerCheck serverCheck;
		int serverIpState = serverCheck.Check();
		if(SERVERCHECK_STATUS_SERVER_IP_CHANGING == serverIpState){
			serverCheck.ChangeServerIP();
		}
		ciDEBUG(1,("Server Ip Check End(%d)", serverIpState));	
		return ;
	}

	if(standby_ip != main_ip) {
		ciDEBUG(1,("STANDBY IP Case(%s)", standby_ip.c_str()));	

		CServerCheck serverCheck;
		if(!serverCheck.ConnectTest(main_ip.c_str())){
			ciDEBUG(1,("MAIN IP DOES NOT WORK(%s)", main_ip.c_str()));	
			if(serverCheck.ConnectTest(standby_ip.c_str())){
				ciDEBUG(1,("STANDBY IP DOES WORK(%s), TRY TO CHANGE SERVER", standby_ip.c_str()));	
				if(serverCheck.ChangeServerIP(standby_ip.c_str())) {
					ciDEBUG(1,("ChangeServerIP Succeed(%s --> %s)", main_ip.c_str(), standby_ip.c_str()));
				}else{
					ciWARN(("ChangeServerIP failed(%s --> %s)", main_ip.c_str(), standby_ip.c_str()));
				}
			}else{
				ciWARN(("STANDBY IP ALSO DOES NOT WORK(%s)", standby_ip.c_str()));	
			}
		}
	}
	return;
}


// CFWVApp

BEGIN_MESSAGE_MAP(CFWVApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CFWVApp 생성

CFWVApp::CFWVApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
	m_worldThread = 0;
	m_udpThread = 0;
	m_guestThread = 0;
	m_ftpServer = 0;
	m_shutdownTimer = 0;
	m_monitorOnOffTimer = 0;
	m_screenshotTimer = 0;
	m_autoUpdateTimer = 0;
	m_xmlTimer = 0;
	m_timeSyncTimer = 0;
	m_agentClient = 0;
	m_serverIpThread = 0;
	m_basketContentsTimer = 0;
	m_gpqsTimer = 0;
}


// 유일한 CFWVApp 개체입니다.

CFWVApp theApp;


// CFWVApp 초기화

BOOL CFWVApp::InitInstance()
{
	HANDLE hMutex = ::CreateMutex(NULL,FALSE, "FirmwareView");
	if (hMutex != NULL) {
		if(::GetLastError() == ERROR_ALREADY_EXISTS) {
			::CloseHandle(hMutex);
			//ciDEBUG(1,("UBC FirmwareView already exist"));
			//::PostMessage(m_pMainWnd, WM_CLOSE, 0, 0);
			 exit(1);
		}
	}

	//////////////////////////////////////////////////////////////
	//Added by jwh184 2002-03-05
	// Initialize OLE libraries
	if(!AfxOleInit())
	{
		AfxMessageBox("OLE initialization failed.  Make sure that the OLE libraries are the correct version.");
		return FALSE;
	}//if
	//////////////////////////////////////////////////////////////
	
	SetErrorMode(SEM_NOGPFAULTERRORBOX);

		
	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("FirmwareView"));

//	scOPEN_DEBUG("FirmwareView.log");

	{
		
		
		// world 로 이전되었음.
		//tcpSocketSession aSession;
		//aSession.modifyBRWProperties();
		//aSession.xmlRefresh();
	}

	//HANDLE              hToken;
	//TOKEN_PRIVILEGES    tokenPriv = {0};
	//if (SetPrivilege(hToken, tokenPriv) == ERROR_SUCCESS) {

	// 기타 배치파일을 실행해 주는 함수.

	// UBCDeviceWizard.exe 에서 DGNET.dll 에러가 나므로 미리 띠워놓는다.
	/*
	ciString command = "UBCDeviceWizard.exe";
	ciString arg="";
	//int nRet = WinExec(command.c_str(), SW_HIDE);
	//if(nRet < 31) {
	//	ciERROR(("UBCDeviceWizard run fail"));
	//}
	HWND hwind = ::FindWindow(NULL, "UBC Device Wizard");
	if(!hwind){
		if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,FALSE)){
			ciDEBUG(1,("%s %s succeed", command.c_str(), arg.c_str()));
			//return ciTrue;
		}
		for(int i=0;i<20;i++){
			hwind = ::FindWindow(NULL, "UBC Device Wizard");
			if(!hwind){
				::Sleep(100);
				continue;
			}
			::ShowWindow(hwind,SW_HIDE);
			break;
		}
	}else{
		::ShowWindow(hwind,SW_HIDE);
	}
	*/
	
	ciDebug::setDebugOn();
	ciDebug::logOpen(".\\log\\preUBCFirmwareView.exe.log");

	ciDEBUG(1,("-----------------Initialize start------------------"));

	scratchUtil::getInstance()->myLogOpen("firmwareView_mylog.log");

	_crateUBCReadyShortCut();
	

	ciBoolean isGlobal = ubcUtil::getInstance()->isGlobalUI();
	scratchUtil::getInstance()->myDEBUG("isGlobal=%d", isGlobal);
	if(!isGlobal)
	{
		{
			// IP로 자동인증
			if(installWebUtil::getInstance()->authByIP() == 2)
			{
				// 인증서가 갱신된 경우임.  리부트 해야함.
				system("rebootSystem.bat");
			}
		}
	}

	ciString mac,edition,hostId;
	if(!scratchUtil::getInstance()->readAuthFile(hostId,mac,edition)){
		scratchUtil::getInstance()->myDEBUG("readAuthFile failed");
		if(isGlobal){
			installUtil::getInstance()->stopUBC();
			ciString arg= " +eng +ENT +global ";
			ciString command = "UBCHostAuthorizer.exe";
			scratchUtil::getInstance()->myDEBUG("command=%s %s", command.c_str(), arg.c_str());
			if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
				ciDEBUG(1,("%s %s succeed", command.c_str(), arg.c_str()));
				Sleep(1000);
				scratchUtil::getInstance()->getFocus("License Center");
				Sleep(1000);
				return false;
				//exit(1);

			}
		}
		edition = STANDARD_EDITION;
	}
	ciDEBUG(1,("EDITION=%s", edition.c_str()));
	scratchUtil::getInstance()->myDEBUG("EDITION=%s", edition.c_str());
	
	extrajob(edition.c_str(),hostId.c_str());
	flashSocketServer(edition.c_str());

	//서버 IP 가 변경되었는지 확인한다.
	if(edition==ENTERPRISE_EDITION){
		serverIpCheck();
		//ciDEBUG(1,("Server Ip Check Start"));	
		//CServerCheck serverCheck;
		//int serverIpState = serverCheck.Check();
		//if(SERVERCHECK_STATUS_SERVER_IP_CHANGING == serverIpState){
		//	serverCheck.ChangeServerIP();
		//}
		//ciDEBUG(1,("Server Ip Check End(%d)", serverIpState));	
	}

	_getBooutupTime();
	holidayShutdown();

	if(!udpSocketServer(edition.c_str())){
		ciString myip,mymac;
		if(scratchUtil::getInstance()->getIpAddress(myip)){
			if(scratchUtil::getInstance()->getMacAddress(mymac)){
				udpThread::changeClientIP(myip.c_str(),mymac.c_str(),hostId.c_str());
			}
		}
	}
	if(edition!=ENTERPRISE_EDITION){
		ftpSocketServer();
	}
	shutdownTimer();
	//week_shutdownTimer();  안해도 된다. 왜냐하면 processExpired 에서 한번 다시 조정하기 때문이다.

	ciString siteId;

	if(edition == ENTERPRISE_EDITION){
		timeSyncTimer();
		agentClient();
		screenshotTimer(hostId.c_str());
		setAutoUpdateTimer();
		installUtil::getInstance()->getSiteId(siteId);
		getXMLTimer(siteId.c_str(), hostId.c_str());
	}

	guest_accept();

#ifdef _HYUNDAI_KIA_  	
	if(edition == ENTERPRISE_EDITION){


		planManager* aManager = planManager::getInstance();
		aManager->setHost(hostId.c_str());
		//aManager->loadIni();
		
		reloadEventHandler* aEventHandler = new reloadEventHandler(siteId.c_str(),hostId.c_str(), GetAppPath());

		//skpark same_size_file_problem 
		ubcConfig aIni("UBCVariables");
		ciBoolean use_time_check = ciFalse;
		aIni.get("ROOT","USE_TIME_CHECK",use_time_check);
		if(ciArgParser::getInstance()->isSet("+use_time_check") || use_time_check == true){ //skpark same_size_file_problem 
			// 이 경우 file time 비교를 하지 않고 다운로드 하므로 좀더 빠르다.
			aEventHandler->setFileTimeCheck(true);
		}
		aManager->setEventHandler(aEventHandler, ciTrue);

		planTimer* pTimer = planTimer::getInstance();
		//pTimer->initTimer("planTimer",60);  // 1분마다 호출된다.
		pTimer->initTimer("planTimer",1);  // 1초마다 호출된다.
		pTimer->startTimer();

		if(ciArgParser::getInstance()->isSet("+basketcontents")) {
			m_basketContentsTimer = new basketContentsTimer(siteId, hostId);
			m_basketContentsTimer->initTimer("basketContentsTimer",60);  // 1분마다 호출된다.
			m_basketContentsTimer->setZero();  // 00초에 시작되도록 한다.
			m_basketContentsTimer->startTimer();
		}
		if(ciArgParser::getInstance()->isSet("+GPQS")) {
			m_gpqsTimer = new GPQSTimer(siteId, hostId, false, false);
			m_gpqsTimer->initTimer("m_gpqsTimer",300);  // 5분마다 호출된다.
			if(this->m_agentClient){
				m_gpqsTimer->setAGTClientSession(this->m_agentClient);
			}
			//m_gpqsTimer->setZero();  // 00초에 시작되도록 한다.
			m_gpqsTimer->startTimer();

		}else
		if(ciArgParser::getInstance()->isSet("+GPQS_total")) {
			if(ciArgParser::getInstance()->isSet("+GPQS_all")) {
				m_gpqsTimer = new GPQSTimer(siteId, hostId, true, true);
				if(this->m_agentClient){
					m_gpqsTimer->setAGTClientSession(this->m_agentClient);
				}
				m_gpqsTimer->initTimer("m_gpqsTimer",300);  // 5분마다 호출된다.
				//m_gpqsTimer->setZero();  // 00초에 시작되도록 한다.
				m_gpqsTimer->startTimer();
			}else{
				m_gpqsTimer = new GPQSTimer(siteId, hostId, true, false);
				if(this->m_agentClient){
					m_gpqsTimer->setAGTClientSession(this->m_agentClient);
				}
				m_gpqsTimer->initTimer("m_gpqsTimer",300);  // 5분마다 호출된다.
				//m_gpqsTimer->setZero();  // 00초에 시작되도록 한다.
				m_gpqsTimer->startTimer();
			
			}
		}

		if(ciArgParser::getInstance()->isSet("+spccontents")) {
			m_spcContentsTimer = new spcContentsTimer(siteId, hostId);
			m_spcContentsTimer->initTimer("spcContentsTimer",60);  // 1분마다 호출된다.
			//m_spcContentsTimer->setZero();  // 00초에 시작되도록 한다.
			m_spcContentsTimer->startTimer();
		}

		if(ciArgParser::getInstance()->isSet("+spcprint")) {
			m_spcPrintTimer = new spcPrintTimer(siteId, hostId);
			m_spcPrintTimer->initTimer("spcPrintTimer",30);  // 30초마다 호출된다.
			//m_spcPrintTimer->setZero();  // 00초에 시작되도록 한다.
			m_spcPrintTimer->startTimer();
		}
	}
#endif

	// 플래시 ocx 롤백
	CFlashRollback fbb;
	fbb.CheckFlashFile();

	ciDEBUG(1,("---------------Initialize fininshed---------------->%s",edition.c_str()));

	CFWVDlg dlg;
	m_pMainWnd = &dlg;
	dlg.setEdition(edition.c_str());
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}

	//	RestorePrivilege(hToken, tokenPriv);
	//}else{
	//	ciERROR(("Debug previledge failed"));
	//}
	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

int CFWVApp::ExitInstance()
{
//	scCLOSE_DEBUG();
	ciDEBUG(1,("ExitInstance()"));

	ProjectorWrapper::clearInstance();

	ciDEBUG(1,("planTimer::clearInstance()"));
	planTimer::clearInstance();
	ciDEBUG(1,("planManager::clearInstance()"));
	planManager::clearInstance();
	ciDEBUG(1,("planManager end()"));

	if(m_basketContentsTimer) {
		ciDEBUG(1,("m_basketContentsTimer->stop()"));
		m_basketContentsTimer->stopTimer();
		delete m_basketContentsTimer;
	}
	if(m_gpqsTimer) {
		ciDEBUG(1,("m_gpqsTimer->stop()"));
		m_gpqsTimer->stopTimer();
		delete m_gpqsTimer;
	}

	if(m_worldThread){
		ciDEBUG(1,("m_worldThread->stop()"));
		m_worldThread->stop();
		m_worldThread->destroy();
		delete m_worldThread;
	}
	stopThread::getInstance()->stopTimer();

	if(m_serverIpThread){
		ciDEBUG(1,("m_serverIpThread->stop()"));
		m_serverIpThread->stop();
		delete m_serverIpThread;
	}

	if(m_udpThread){
		ciDEBUG(1,("m_udpThread->stop()"));
		m_udpThread->stop();
		delete m_udpThread;
	}
	
	if(m_guestThread){
		ciDEBUG(1,("m_guestThread->stop()"));
		m_guestThread->stop();
		delete m_guestThread;
	}
	if(m_ftpServer){
		ciDEBUG(1,("delete m_ftpServer"));
		delete m_ftpServer;
	}
	if(m_shutdownTimer){
		ciDEBUG(1,("delete m_shutdownTimer"));
		m_shutdownTimer->stopTimer();
		delete m_shutdownTimer;
	}
	if(m_monitorOnOffTimer){
		ciDEBUG(1,("delete m_monitorOnOffTimer"));
		m_monitorOnOffTimer->stopTimer();
		delete m_monitorOnOffTimer;
	}
	if(m_screenshotTimer){
		ciDEBUG(1,("delete m_screenshotTimer"));
		m_screenshotTimer->stopTimer();
		delete m_screenshotTimer;
	}
	if(m_autoUpdateTimer){
		ciDEBUG(1,("delete m_autoUpdatetTimer"));
		m_autoUpdateTimer->stopTimer();
		delete m_autoUpdateTimer;
	}
	if(m_xmlTimer){
		ciDEBUG(1,("delete m_xmltTimer"));
		m_xmlTimer->stopTimer();
		delete m_xmlTimer;
	}
	if(m_timeSyncTimer){
		ciDEBUG(1,("delete m_timeSyncTimer"));
		m_timeSyncTimer->stopTimer();
		delete m_timeSyncTimer;
	}
	if (m_agentClient) {
		ciDEBUG(1,("delete m_agentClient"));
		m_agentClient->fini();
		AGTClientSession::clearInstance();
		m_agentClient = 0;
	}

	ciDEBUG(1,("ExitInstance End"));

	//////////////////////////////////////////////////////////////
	//Added by jwh184 2002-03-05
	CoUninitialize();
	//////////////////////////////////////////////////////////////

	return CWinApp::ExitInstance();
}

DWORD CFWVApp::SetPrivilege( HANDLE &hToken, TOKEN_PRIVILEGES &tokenPriv )
{
    DWORD   dwResult = ERROR_SUCCESS;
    LUID    luidDebug;


    if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken) != FALSE)
    {
        if (LookupPrivilegeValue(_T(""), SE_DEBUG_NAME, &luidDebug) != FALSE)
        {
            tokenPriv.PrivilegeCount           = 1;
            tokenPriv.Privileges[0].Luid       = luidDebug;
            tokenPriv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

            if (AdjustTokenPrivileges(hToken, FALSE, &tokenPriv, sizeof(tokenPriv), NULL, NULL) != FALSE)
                dwResult = GetLastError();
        }
        else
            dwResult = GetLastError();
    }
    else
        dwResult = GetLastError();
        
    return (dwResult);
}

//====================================================================

DWORD CFWVApp::RestorePrivilege( HANDLE &hToken, TOKEN_PRIVILEGES &tokenPriv )
{
    DWORD   dwResult = ERROR_SUCCESS;
    LUID    luidDebug;
    
            
    if (LookupPrivilegeValue(_T(""), SE_DEBUG_NAME, &luidDebug) != FALSE)
    {
        tokenPriv.Privileges[0].Luid       = luidDebug;
        tokenPriv.Privileges[0].Attributes = SE_PRIVILEGE_REMOVED;

        if (AdjustTokenPrivileges(hToken, FALSE, &tokenPriv, sizeof(tokenPriv), NULL, NULL) != FALSE)
            dwResult = GetLastError();
    }

    CloseHandle(hToken);

    return (dwResult);
}

void
CFWVApp::flashSocketServer(const char* edition)
{
	
	m_worldThread = new tcpSocketWorldThread();
	m_worldThread->init(edition);
	ciDEBUG(1,("flashSocketServer(%s) before start",edition));
	m_worldThread->start();
	ciDEBUG(1,("flashSocketServer() after start"));

	stopThread::getInstance()->initTimer("stopTimer",5);
	stopThread::getInstance()->startTimer();
}

void
CFWVApp::serverIpCheck()
{
	ciDEBUG(1,("m_serverIpThread() before start"));
	m_serverIpThread = new serverIpCheckThread();
	m_serverIpThread->start();
	ciDEBUG(1,("m_serverIpThread() after start"));

}

ciBoolean
CFWVApp::udpSocketServer(const char* edition)
{
	ciDEBUG(1,("udpSocketServer() before start"));

	ubcConfig aIni("UBCVariables");
	ciBoolean useSerial = ciFalse;
	aIni.get("ROOT","USE_SERIAL",useSerial);

	if(!useSerial) {
		if(strcmp(edition,STANDARD_EDITION)==0) return ciFalse;
		if(strcmp(edition,ENTERPRISE_EDITION)==0) return ciFalse;
	}

	m_udpThread = new udpThread(14008,useSerial);
	m_udpThread->start();
	ciDEBUG(1,("udpSocketServer() after start"));
	return ciTrue;
}

void
CFWVApp::ftpSocketServer()
{
	ciDEBUG(1,("ftpSocketServer() before start"));
	m_ftpServer = new CFTServer();
	ciDEBUG(1,("ftpSocketServer() before start"));
	m_ftpServer->SetDonloadRoot(_COP_CD("C:\\"));
	ciDEBUG(1,("ftpSocketServer() before start"));
	m_ftpServer->SetTemporaryRoot(_COP_CD("C:\\ftproot\\"));
	ciDEBUG(1,("ftpSocketServer() before start"));
	m_ftpServer->Start();
	ciDEBUG(1,("ftpSocketServer() after start"));
}

void
CFWVApp::holidayShutdown()
{
	ciDEBUG(1,("holidayShutdown"));
	ciString timeStr = "";
	installUtil::getInstance()->getShutdownTime(timeStr);
	
	if(timeStr.empty()){
		ciDEBUG(1,("No holiday set"));
		return;
	}
	ciTime buf;
	char today[12];
	sprintf(today, "%04d/%02d/%02d", buf.getYear(), buf.getMonth(), buf.getDay());

	ubcConfig aIni("UBCVariables");
	ciString hasDone = "";
	aIni.get("SHUTDOWNTIME","done",hasDone);
	if(hasDone == today){
		aIni.set("SHUTDOWNTIME","done","0");
		ciDEBUG(1,("Already shutdowned once!!!"));
		return;
	}

	ciString holidayStr="";
	int len = timeStr.size();
	if(timeStr[0]==SHUTDOWNTIME_DELIMETER && len > 1){
		holidayStr = timeStr.substr(1);
	}else{
		if(len < 5) {
			ciERROR(("Invalid TimeStr(%s)", timeStr.c_str()));
			return;
		}
		if(len > 6) {
			holidayStr = timeStr.substr(6);
		}
	}
	if(holidayStr.empty()){
		ciDEBUG(1,("No holiday setting"));
		return;
	}

	cciTime now;
	
	char day[10];
	sprintf(day,"%0d.%02d", now.getMonth(), now.getDay());
	char week[10];
	sprintf(week,"%d", now.getWeekDay());

	ciDEBUG(1,("today=%s,%s",day,week));

	ciBoolean isTodayHoliday = ciFalse;
	ciStringTokenizer aTokens(holidayStr,SHUTDOWNTIME_DELIMETER_D);
	while(aTokens.hasMoreTokens()){
		ciString holiday = aTokens.nextToken();
		ciDEBUG(1,("holiday=%s", holiday.c_str()));
		if(holiday == day || holiday == week ){
			isTodayHoliday = ciTrue;
			break;
		}
	}
	
	if(isTodayHoliday){
		ciDEBUG(1,("Today is holiday"));
		aIni.set("SHUTDOWNTIME","done",today);

		// shutdown 명령은 별도 스레드에서 진행하고 자신은 exit 한다.
		ciString command = "shutdownSystem.bat";
		ShutdownThread* sdThread = new ShutdownThread;
		sdThread->setCommand(command.c_str());
		sdThread->start();
#ifdef _WIN32
			::Sleep(1000*30);
#else
			sleep(5);
#endif;
		//::PostMessage(this->m_pMainWnd,WM_CLOSE,0,0);
		exit(1);
	}else{
		ciDEBUG(1,("Today is working day"));
	}
}

void
CFWVApp::week_shutdownTimer(const char* pTimeStr)
{
	ciDEBUG(1,("week_shutdownTimer() before start"));
	ciString timeStr = "";

	if(pTimeStr==0 || strlen(pTimeStr)<5){
		installUtil::getInstance()->getWeekShutdownTime(timeStr);
	}else{
		timeStr=pTimeStr;
	}

	if(timeStr.empty() || timeStr.size() < 7 || timeStr[0]!='('){
		return;
	}

	if(installUtil::getInstance()->calcShutdownTime(timeStr)){
		shutdownTimer(timeStr.c_str());
	}

}


void
CFWVApp::shutdownTimer(const char* pTimeStr)
{
	ciDEBUG(1,("shutdownTimer() before start"));
	ciString timeStr = "";
	
	if(pTimeStr==0 || strlen(pTimeStr)<5){
		installUtil::getInstance()->getShutdownTime(timeStr);
	}else{
		timeStr=pTimeStr;
	}

	if(m_shutdownTimer){
		// 타이머를 stop 할때, processExpired 가 수행중이라면, 죽는 경우가 발생한다.
		// 이를 막기 위해, getShutdownTime 을 한번 해준다.
		// getShutdownTime 에는 mutex lock 이 들어있어서, processExpired 중간에는
		// 통과하지 못한다.
		int hour=0;
		int min=0;
		int waitTime=0;
		m_shutdownTimer->getShutdownTime(hour,min,waitTime);
		ciDEBUG(1,("PrevValue=%02d:%02d", hour,min));
		m_shutdownTimer->stopTimer();
		delete m_shutdownTimer;
		m_shutdownTimer=0;
	}
	//if(pTimeStr==0){
	//	if(m_monitorOnOffTimer){
	//		m_monitorOnOffTimer->stopTimer();
	//		delete m_monitorOnOffTimer;
	//		m_monitorOnOffTimer=0;
	//	}
	//}

	int	hour = -1;
	int	min = -1;
	if(timeStr.size() >= 5 && timeStr[0]!=SHUTDOWNTIME_DELIMETER && timeStr.substr(0,5) != "NOSET" ){
		hour = atoi(timeStr.substr(0,2).c_str());
		min = atoi(timeStr.substr(3,2).c_str());
	}

	ciLong hostType = 99;
	installUtil::getInstance()->getHostType(hostType);

	ciString buf;
	ciBoolean monitorOff =ciFalse;
	ciStringList monitorOffList;
	this->getMonitorOff(monitorOff);
	this->getMonitorOffList(buf);
	if(buf.size() > 4){
		ciStringUtil::removeAllQuote(buf);
		ciStringUtil::getBraceValues(buf,",",&monitorOffList,"[","]");
	}
	/*
	if(hour < 0 && min < 0 && (monitorOff == ciFalse || monitorOffList.size() == 0) ){
		return;
	}
	*/

	m_shutdownTimer = new ShutdownTimer();
	m_shutdownTimer->initTimer("Shutdown Timer", 30);
	//m_shutdownTimer->setHostType(hostType);
	//m_shutdownTimer->setMonitorOffTime(monitorOffList);
	//m_shutdownTimer->setMonitorOffFlag(monitorOff);
	m_shutdownTimer->setShutdownTime(hour, min, 10);
	//m_shutdownTimer->setZero();
	m_shutdownTimer->startTimer();
	ciDEBUG(1,("shutdownTimer() after start"));

	if(pTimeStr==0 && m_monitorOnOffTimer==0){
		m_monitorOnOffTimer = new MonitorOnOffTimer();
		m_monitorOnOffTimer->initTimer("MonitorOnOff Timer", 60);
		m_monitorOnOffTimer->setHostType(hostType);
		m_monitorOnOffTimer->setMonitorOffTime(monitorOffList);
		m_monitorOnOffTimer->setMonitorOffFlag(monitorOff);
		m_monitorOnOffTimer->setZero();
		m_monitorOnOffTimer->startTimer();
		ciDEBUG(1,("monitorOnOffTimer() after start"));
	}

}

void
CFWVApp::screenshotTimer(const char* hostId)
{
	ciShort screenshotPeriod = 0;

	ubcConfig  aIni("UBCVariables");
	aIni.get("ROOT","ScreenShotPeriod", screenshotPeriod);

	if (screenshotPeriod && m_agentClient) 
	{
		ciDEBUG(1,("screenshotTimer() before start"));
		if (!::GetEnableShortScreenshotPeriod() && screenshotPeriod < 120) screenshotPeriod = 120; // 최저값 120s
		cout << "ScreenShot Timer Start.\n"	<< " Period\t: " << screenshotPeriod << endl;

		ciString ip = m_agentClient->getFtpIP();
		ciString user = m_agentClient->getFtpId();
		ciString password = m_agentClient->getFtpPasswd();
		int port = m_agentClient->getFtpPort();

		m_screenshotTimer = new agtScreenshotTimer(hostId,screenshotPeriod);
		m_screenshotTimer->setScreenShotFtp(ip.c_str(), user.c_str(), password.c_str(), (ciUShort)port);
		if (::GetEnableShortScreenshotPeriod())
			m_screenshotTimer->initTimer("Screenshot Timer", 1);
		else
			m_screenshotTimer->initTimer("Screenshot Timer");
		//COM 초기화
		if(ciIniUtil::isHttpOnly()){
			m_screenshotTimer->setCom_MSC(ciTrue);
		}
		m_screenshotTimer->startTimer();
		ciDEBUG(1,("screenshotTimer() after start"));
	}
}

void
CFWVApp::setAutoUpdateTimer()
{
	ciDEBUG(1,("autoUpdateTimer() before start"));
	m_autoUpdateTimer = new autoUpdateTimer();
	m_autoUpdateTimer->initTimer("autoUpdateTimer Timer");
	m_autoUpdateTimer->startTimer();
	ciDEBUG(1,("autoUpdateTimer() after start"));
}

void
CFWVApp::getXMLTimer(const char* siteId, const char* hostId)
{
	ciStringMap xmlMap;

	ubcConfig  aIni("UBCVariables");
	unsigned long port = 0;
	aIni.get("XML","PORT", port);
	if(port == 0){
		port = 8000;
	}
	ciBoolean useSSL = false;
	aIni.get("XML","USE_SSL", useSSL);
	ciString ip="";
	aIni.get("XML","SERVER_IP", ip);

	{
		ciString buf;
		aIni.get("XML","WIA_WELCOMEBOARD", buf);
		if(!buf.empty()){
			xmlMap.insert(ciStringMap::value_type("WIA_WELCOMEBOARD",buf));
			ciDEBUG(1,("WIA_WELCOMEBOARD=%s", buf.c_str()));
		}
	}		

	if(xmlMap.size()> 0){
		ciDEBUG(1,("xmlTimer() before start"));
		m_xmlTimer = new xmlTimer(siteId,hostId);
		m_xmlTimer->setXMLMap(xmlMap,port,useSSL,ip.c_str());
		m_xmlTimer->initTimer("xmlTimer Timer", 60);
		m_xmlTimer->startTimer();
		ciDEBUG(1,("xmlTimer() after start"));
	}
}


void
CFWVApp::timeSyncTimer()
{
	ciShort timeSyncPeriod = 0;

	ubcConfig  aIni("UBCVariables");
	aIni.get("ROOT","TimeSyncPeriod", timeSyncPeriod);

	if (timeSyncPeriod) {
		ciDEBUG(1,("timeSyncTimer() before start"));
		timeSyncPeriod = 300 + ::_getpid() % 120;  // (최대 120초까지 간격차이를 둠) 설정값은 무시함
		/*
		if (timeSyncPeriod < 60*60) {
			timeSyncPeriod = 60*60; // 최저값 60분
			timeSyncPeriod += ::_getpid() % 100;  // (최대 999초까지 간격차이를 둠)
		}
		*/
		cout << "TimeSync Timer Start.\n"	<< " Period\t: " << timeSyncPeriod << endl;


		m_timeSyncTimer = new TimeSyncTimer();
		m_timeSyncTimer->setHostInfoFromIni(); // from UBCConnect.ini
		m_timeSyncTimer->initTimer("TimeSync Timer", (ciInt)timeSyncPeriod);
		m_timeSyncTimer->startTimer();
		ciDEBUG(1,("timeSyncTimer() after start"));
	}
}

void
CFWVApp::agentClient()
{
	ciDEBUG(1,("agentClient() before start"));
	// Agent 객체 생성
	if (!m_agentClient) {
		m_agentClient = AGTClientSession::getInstance();
		m_agentClient->init(m_shutdownTimer, m_monitorOnOffTimer);
	}

	ciDEBUG(1,("SCREENSHOT FTP IP = %s", m_agentClient->getFtpIP()));
	ciDEBUG(1,("SCREENSHOT FTP ID = %s", m_agentClient->getFtpId()));
	ciDEBUG(1,("SCREENSHOT FTP Passwd = %s", m_agentClient->getFtpPasswd()));
	ciDEBUG(1,("SCREENSHOT FTP Port = %d", m_agentClient->getFtpPort()));

	ciDEBUG(1,("agentClient() after start"));
}


void
CFWVApp::extrajob(const char* edition,const char* hostId)
{
	ciDEBUG(1,("extrajob() before start"));
	scratchUtil::getInstance()->DoAdditionalJob();
	installUtil::getInstance()->setFirewallException();
	ciLong vncPort = this->_getVncPort(hostId);
	if(vncPort > 0 && !scratchUtil::getInstance()->IsTcpPortAdded(vncPort)){
		ciDEBUG(1,("vncPort(%d) FirewallException does not set yet", vncPort));
		if(!scratchUtil::getInstance()->AddTcpPort(vncPort)){
			ciERROR(("vncPort(%d) FirewallException set failed", vncPort));
		}else{
			ciDEBUG(1,("vncPort(%d) FirewallException set succeed", vncPort));
		}
	}
	ciDEBUG(1,("extrajob() after start"));

	// 인터넷 시간 동기화 OFF 기능
	//ciDEBUG(1,("RemoteCLICall"));
	//RemoteCLICall("cmd /c net time /setsntp", "127.0.0.1");
	//ciDEBUG(1,("RemoteCLICall"));

	//패스워드가 변경되었는지 확인한다.
	ciShort isChanged = 0;
	ubcConfig aIni("UBCVariables");
	aIni.get("ROOT","WinX_Changed",isChanged);
	if(isChanged){
		ciString winPassword;
		aIni.get("ROOT","WinX", winPassword);
		if (!winPassword.empty()) {
			CSecurityIni aSec;
			ciString buf = aSec.Base64ToAscii(winPassword.c_str());
			winPassword = buf;
			ciDEBUG(1,("password changed(%s)", winPassword.c_str()));
			ciString command = "cmd /c net user Administrator ";
			command += winPassword;
			ciDEBUG(1,("RemoteCLICall(%s)", command.c_str()));
			int passwordChanged = RemoteCLICall(command.c_str(), "127.0.0.1");
			ciDEBUG(1,("RemoteCLICall(%s)", command.c_str()));
			if(passwordChanged>0){
				aIni.set("ROOT","WinX_Changed",(ciShort)0);
				// skpark 2012.3.22  password 가 바뀌면, autoLogon 을 다시 셋팅한다.
				installUtil::getInstance()->setAutoLogon(winPassword.c_str());
			}else{
				ciDEBUG(1,("password set failed"));
			}
		}
	}

	// SPC 를 위하여,
	{
		if(strncmp(hostId,"SPC-",4)==0){
			if(installUtil::getInstance()->setDisplayOption("+autoscale",ciTrue)){
				ciDEBUG(1,("setBrowserOption for SPC +autoscale Succeed"));
			}else{
				ciERROR(("setBrowserOption for SPC +autoscale Failed"));
			}
		}
	}


	// 현대/기아를 위해 UpdatCenter IP 를 UBCConnect.ini 에 써준다.
	{
		ciString new_update_center_ip ="UBCLICENSE.SQISOFT.COM";  // 202 Server
		if(strncmp(hostId,"KIA-",4)==0){
			new_update_center_ip = "58.87.34.251";  // WEB02 Server
		}else
		if(strncmp(hostId,"HYUNDAI-",8)==0){
			new_update_center_ip = "58.87.34.248";  // WEB02 Server
		}
		ubcConfig aIni("UBCConnect");
		ciString buf;
		aIni.get("UBCCENTER","FTP_IP", buf);
		ciBoolean isStatic = ciFalse;
		aIni.get("UBCCENTER","STATIC",isStatic);
		
		if (isStatic == ciFalse && (buf.empty() || buf != new_update_center_ip)) {
			if (!aIni.set("UBCCENTER","FTP_IP", new_update_center_ip.c_str())) {
				ciERROR(("[UBCCENTER]FTP_IP=%s set failed.", new_update_center_ip.c_str()));
			}else{
				ciDEBUG(1, ("[UBCCENTER]FTP_IP=%s set succeed.", new_update_center_ip.c_str()));
			}
		}else{
			ciDEBUG(1, ("[UBCCENTER]FTP_IP=%s already set.", buf.c_str()));
		}
	}
	
	// UBCCLIAgent 프로그램을 실행한다.
	ciString exe_dir = _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\");
	ciString ubcCliAgent = exe_dir + "UbcCLIAgent.exe";
	ciDEBUG(1,("register firewall %s", ubcCliAgent.c_str()));
	if(scratchUtil::getInstance()->AddToExceptionList(ubcCliAgent)){
		ciDEBUG(1,("register firewall %s succeed", ubcCliAgent.c_str()));
		unsigned long pid = scratchUtil::getInstance()->createProcess("ubcCliAgent.exe"," a",exe_dir.c_str(),ciFalse);
		if(pid>0){
			ciDEBUG(1,("run %s a succeed", ubcCliAgent.c_str()));
		}else{
			ciERROR(("run %s a failed", ubcCliAgent.c_str()));
		}
	}else{
		ciDEBUG(1,("register firewall %s failed, Maybe already registered", ubcCliAgent.c_str()));
	}


	// brw1 에 +multivision 옵션이 있거나, displayCounter 가 1이라면, brw 2 번은 동작하지 않도록 조치한다.
	ciBoolean isAuto = installUtil::getInstance()->autoBrowserExist(2-1);
	if(isAuto){
		int displayCounter = scratchUtil::getInstance()->getMonitorCount();
		ciBoolean isMultiVision = installUtil::getInstance()->isOptionExist("STARTER.APP_BROWSER.ARGUMENT","+multivision");
		if(displayCounter < 2 || isMultiVision){
			installUtil::getInstance()->stopBrowser2(2);
		}
	}
	
	//VIDEO_OPEN_TYPE=2
	ubcConfig bIni("UBCBrowser");
	ciULong openType=0;
	ciULong openType_set=0;
	bIni.get("ROOT","VIDEO_OPEN_TYPE",openType);
	bIni.get("ROOT","VIDEO_OPEN_TYPE_SET",openType_set);
	if(openType!=2 && openType_set!=1){
		openType=2;
		openType_set=1;
		bIni.set("ROOT","VIDEO_OPEN_TYPE",openType);
		bIni.set("ROOT","VIDEO_OPEN_TYPE_SET",openType_set);
	}

	// check original-vnc
	ciShort changeOrgVNC = 0;
	aIni.get("ROOT","UseOriginalVNC",changeOrgVNC);
	if(strcmp(edition,ENTERPRISE_EDITION)==0) {
		/*
		// 롯데의 경우 VIDEO_RENDER를 강제로 1로 놓는다.
		if(strncmp(hostId,"LOTTE",5)==0){
			ubcConfig bIni("UBCBrowser");
			ciULong render=0;
			bIni.get("ROOT","VIDEO_RENDER",render);
			if(render!=1){
				render=1;
				bIni.set("ROOT","VIDEO_RENDER",render);
			}
			// skpark 2010.05.20
			// 코덱셋팅도 롤백한다.
			installUtil::getInstance()->codecSettingRollback();

		}
		*/
		/*
		//skpark 2010.07.01
		if(scratchUtil::getInstance()->IsWindows7()){
			if(!installUtil::getInstance()->checkCodecSettingWin7()){
				installUtil::getInstance()->lastestCodecSettingWin7();
			}
		}
		*/
		ciString value;
		ubcConfig aIni("UBCStarter");

		ciBoolean ret_val = ciFalse;
		if(changeOrgVNC)
		{
			// VNC 원복
			ciDEBUG(1,("restoreVNCBinary"));
			ret_val = installUtil::getInstance()->restoreVNCBinary("");
		}
		else
		{
			// VNC 교체
			ciDEBUG(1,("updateVNCBinary"));
			ret_val = installUtil::getInstance()->updateVNCBinary("");
		}

		if(!ret_val){
			ciERROR(("ERROR : Update VNC exe failed"));
		}else{
			ciDEBUG(1,("Update VNC exe Succeed"));
			if(aIni.get("VNC_Proxy","AUTO_STARTUP",value)){
				if(value!="false"){
					ciDEBUG(1,("Set Auto off of VNCProxy"));
					if(!installUtil::getInstance()->setProperty("STARTER.APP_VNC_Proxy.AUTO_STARTUP","false",ciTrue)) {
						ciERROR(("Set Auto off of VNCProxy failed"));
					}else{
						//아예 VNCProxy 를 확실하게 죽여버린다.
						unsigned long lPid = scratchUtil::getInstance()->getPid("VNC_Proxy.exe");
						if(lPid>0){
							scratchUtil::getInstance()->killProcess(lPid);  // kill VNC_Proxy
						}
					}
				}
			}
		}
		if(aIni.get("CoreTemp","AUTO_STARTUP",value)){
			if(value!="false"){
				ciDEBUG(1,("Set Auto off of CoreTemp"));
				if(!installUtil::getInstance()->setProperty("STARTER.APP_CoreTemp.AUTO_STARTUP","false",ciTrue)) {
					ciERROR(("Set Auto off of CoreTemp failed"));
				}
			}
		}
		// remove +local 2011.12.1 skpark
		ciBoolean isLocal=ciFalse;
		installUtil::getInstance()->getLocalOption(isLocal);
		if(isLocal){
			installUtil::getInstance()->setLocalOption(ciFalse,ciTrue);
		}
	}else{
		// 혹시 Enpterise 용 vncserver 가 깔려있다면, normal 한 vncserver 로 교체해준다.
		installUtil::getInstance()->restoreVNCBinary("");

		// add vnc_proxy ip
		ciString ip;
		ubcConfig cIni("UBCConnect");
		cIni.get("VNCMANAGER","KINGYOO",ip);
		if(ip.empty()){
			//cIni.set("VNCMANAGER","KINGYOO","211.232.57.221");
			cIni.set("VNCMANAGER","KINGYOO","211.232.57.202");
		}
		
			
		// run vnc_proxy
		ciString value;
		ubcConfig aIni("UBCStarter");
		if(aIni.get("VNC_Proxy","AUTO_STARTUP",value)){
			if(value!="true"){
				installUtil::getInstance()->setProperty("STARTER.APP_VNC_Proxy.AUTO_STARTUP","true",ciTrue);
			}
		}
		// set +local
		ciBoolean isLocal=ciFalse;
		installUtil::getInstance()->getLocalOption(isLocal);
		if(!isLocal){
			installUtil::getInstance()->setLocalOption(ciTrue,ciTrue);
		}

	}
}
bool	
CFWVApp::getMonitorOffList(string& outVal)
{
	ubcConfig aIni("UBCVariables");
	if (!aIni.get("ROOT","MonitorOffList", outVal)) 
	{
		ciWARN(("[ROOT]MonitorOffList get failed."));
		// Default Value
		outVal="";
		return false;
	}
	ciDEBUG(9, ("[ROOT]MonitorOffList is %s", outVal.c_str()));
	return true;
}
bool	
CFWVApp::getMonitorOff(bool& outVal)
{
	ubcConfig aIni("UBCVariables");
	if (!aIni.get("ROOT","MonitorOff", outVal)) 
	{
		ciWARN(("[ROOT]MonitorOff get failed."));
		// Default Value
		outVal=false;
		return false;
	}
	ciDEBUG(9, ("[ROOT]MonitorOff is %d", outVal));
	return true;
}

void
CFWVApp::_crateUBCReadyShortCut()
{
	CString szTarget;
	char TargetPath[MAX_PATH+1];
	::SHGetSpecialFolderPath (NULL, TargetPath, CSIDL_STARTUP, FALSE);
	szTarget = TargetPath;
	szTarget += "\\UBCReady.lnk";

	CFileFind ff;
	if(ff.FindFile(szTarget)){
		ff.Close();
		return ;
	}
	ff.Close();

	// create ubcReady short cut


	CString source, target;

	source = _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\");
	source.Append("UBCReady.lnk");

	::CopyFile(source, szTarget, FALSE);

}

void 
CFWVApp::guest_accept()
{
	ubcConfig aIni("UBCVariables");
	ciBoolean useGuestBook=ciFalse;
	aIni.get("ROOT","USE_GUESTBOOK",useGuestBook);
	if(!useGuestBook){
		aIni.get("ROOT","USE_KIA_GUESTBOOK",useGuestBook);
	}

	if(!useGuestBook){
		ciDEBUG(1,("This is not GuestBook version"));
		return ;
	}
	ciDEBUG(1,("This is GuestBook version"));


	ciDEBUG(1,("guest_accept() before start"));
	m_guestThread = new tcpGuestSocketThread();
	m_guestThread->start();
	ciDEBUG(1,("guest_accept() after start"));

}


long
CFWVApp::_getVncPort(const char* pHostId)
{
	ciString hostId = pHostId;
	if(!ciStringUtil::contain(hostId.c_str(),'-')){
		return -1;
	}
	int len = hostId.size();	
	if(len < 7) {
		ciERROR(("hostId should be longer than 6"));
		return -1;
	}

	ciLong no = atol(hostId.substr(len-5,5).c_str());
	ciDEBUG(1,("vncPort = %ld", no));
	return no;
}


void
CFWVApp::_getBooutupTime()
{
	ciTime bootuptime;

	CSysOnOffTime sysonoff;
	time_t tm = sysonoff.GetLastBootUpTime();
	if(tm>0)
	{
		bootuptime.set(tm);
	}
	else
	{
		ciDEBUG(1,("CSysOnOffTime::GetLastBootUpTime() fail !!!"));
	}

	char buf[32] = {0};
	sprintf(buf, "%04d/%02d/%02d %02d:%02d:%02d", 
		bootuptime.getYear(), 
		bootuptime.getMonth(), 
		bootuptime.getDay(), 
		bootuptime.getHour(), 
		bootuptime.getMinute(), 
		bootuptime.getSecond()
	);

	//
	ubcConfig aIni("UBCVariables");
	aIni.set("SHUTDOWNTIME", "LastBootupTime", buf);

	ciDEBUG(1,("LastBootupTime=%s", buf));
}


//AD_INFO* info;
//m_ads.Lookup(str_id, (void*&)info)
//m_ads.SetAt(str_id, info);

//POSITION pos = m_ads.GetStartPosition();
//CString key;
//while(pos)
//{
//	AD_INFO* info = NULL;
//	m_ads.GetNextAssoc( pos, key, (void*&)info );

#define EXE_PATH _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\")

CChannelXml::CChannelXml()
{
	ciDEBUG(10, ("CChannelXml()") );

	m_bAlreadyRead = false;

	for(int i=0; i<8; i++)
		m_channels[i].channel.Format("ch%d", i+1);

	ReadXML();
}

CChannelXml::~CChannelXml()
{
	ciDEBUG(10, ("~CChannelXml()") );
	Clear();
}

void CChannelXml::Clear(bool only_ad)
{
	ciDEBUG(10, ("Clear()") );

	//
	if( only_ad == false )
	{
		for(int i=0; i<8; i++)
		{
			m_channels[i].channelName = "";
		}
	}

	//
	ciDEBUG(10, ("delete ads-objects") );
	POSITION pos = m_ads.GetStartPosition();
	CString key;
	while(pos)
	{
		AD_INFO* info = NULL;
		m_ads.GetNextAssoc( pos, key, (void*&)info );
		if( info ) delete info;
	}
	m_ads.RemoveAll();

	//
	if( only_ad == false )
		m_bAlreadyRead = false;
	ciDEBUG(10, ("~Clear()") );
}

bool CChannelXml::GetValue(CString& line, LPCSTR key, CString& value)
{
	// find key
	int start_pos = line.Find(key);
	if( start_pos < 0 ) return false;

	// find end-double-quotation
	start_pos += strlen(key);
	int end_pos = line.Find("\"", start_pos+1);
	if( end_pos < 0 ) return false;

	// get value between double-quotation
	value = line.Mid(start_pos, end_pos - start_pos);
	value.Trim("\"");

	return true;
}

bool CChannelXml::ExtractChannelInfo(CString& line, CHANNEL_INFO& info)
{
	// check, is channel-info line?
	if( line.Find("<ch") < 0 )
		return false;

	bool ret_val = true;

	// get name value
	if( GetValue(line, "name=", info.channel) == false ) ret_val = false;
	// get set_ad value
	if( GetValue(line, "set_ad=", info.channelName) == false ) ret_val = false;

	return ret_val;
}

bool CChannelXml::ExtractADInfo(CString& line, AD_INFO& info)
{
	// check, is ad-info line?
	if( line.Find("<ad") < 0 )
		return false;

	bool ret_val = true;

	// get name value
	if( GetValue(line, "name=", info.channelName) == false ) ret_val = false;
	// get desc value
	if( GetValue(line, "desc=", info.desc) == false ) ret_val = false;
	// get sitename value
	if( GetValue(line, "sitename=", info.siteName) == false ) ret_val = false;

	// get networkuse value
	CString value;
	if( GetValue(line, "networkuse=", value) == false ) ret_val = false;
	info.networkuse = atoi(value);

	return ret_val;
}

bool CChannelXml::ReadXML()
{
	ciDEBUG(10, ("ReadXML(%d)", m_bAlreadyRead) );
	if( m_bAlreadyRead )
	{
		ciDEBUG(10, ("already read xml") );
		return true;
	}

	CString filename[2];
	filename[0] = EXE_PATH;
	filename[0] += "flash\\xml\\channel.xml";
	filename[1] = EXE_PATH;
	filename[1] += "flash\\xml\\channel.bak";

	for(int i=0; i<2; i++)
	{
		Clear();

		// file open
		CFile file;
		CFileException ex;
		ciDEBUG(10, ("%s file open", filename[i]) );
		if( file.Open(filename[i], CFile::modeRead | CFile::typeBinary, &ex) == FALSE )
		{
			ciERROR(("%s file open error (errcode:%d)", filename[i], ex.m_cause));
			if( i == 0 )
			{
				ciDEBUG(10, ("backup file used") );
				if( ::CopyFile(filename[1], filename[0], FALSE) == FALSE )
				{
					ciERROR(("%s file restore error (errcode:%d)", filename[1], ::GetLastError()));
				}
				continue;
			}
			return ciFalse;
		}

		ciDEBUG(10, ("readXml(%s)", filename[i]) );

		// make buffer
		int file_size = (int)file.GetLength();
		char* buf_utf8 = new char[file_size + 1];
		char* buf_ansi = new char[file_size * 2 + 1];
		::ZeroMemory(buf_utf8, file_size + 1);
		::ZeroMemory(buf_ansi, file_size * 2 + 1);

		// file read
		TRY
		{
			file.Read(buf_utf8, file_size);
		}
		CATCH(CFileException, ex)
		{
			ciERROR(("%s Read Error (errcode:%d)", filename[i], ex->m_cause) );
			continue;
		}
		CATCH(CException, ex)
		{
			ciERROR(("%s Read Error", filename[i]) );
			continue;
		}
		END_CATCH

		file.Close();

		// convert utf8 -> ansi
		int len = scratchUtil::getInstance()->UTF8ToAnsi(buf_utf8, buf_ansi, file_size * 2);
		CString str_lines = buf_ansi;
		delete[]buf_utf8;
		delete[]buf_ansi;

		if( len <= 0 )
		{
			ciWARN(("UTF8ToAnsi error!"));
			continue;
		}

		//
		bool channel_part = false, ad_part = false;
		int channel_part_count = 0, ad_part_count = 0;

		int pos = 0;
		CString str_line = str_lines.Tokenize("\r\n", pos); // divide by line
		while( str_line != "" )
		{
			str_line.Trim("\t ");

			if( str_line.Find("<ch_info") >= 0 )
			{
				channel_part_count++;
				channel_part = true;
			}
			else if( str_line.Find("</ch_info>") >= 0 )
			{
				channel_part = false;
			}
			else if( str_line.Find("<ad_info") >= 0 )
			{
				ad_part_count++;
				ad_part = true;
			}
			else if( str_line.Find("</ad_info>") >= 0 )
			{
				ad_part = false;
			}

			if( channel_part && ad_part )
			{
				// something wrong
				break;
			}

			if( channel_part )
			{
				// add channel from line
				CHANNEL_INFO info;
				if( ExtractChannelInfo(str_line, info) )
				{
					for(int i=0; i<8; i++)
					{
						if( stricmp(m_channels[i].channel, info.channel) == 0 )
						{
							m_channels[i] = info;
							ciDEBUG(10, ("%s=%s", m_channels[i].channel, m_channels[i].channelName) );
							break;
						}
					}
				}
			}
			else if( ad_part )
			{
				// add ad from line
				AD_INFO* info = new AD_INFO;
				if( ExtractADInfo(str_line, *info) )
				{
					CString key = (LPCSTR)info->channelName;
					_strlwr((LPSTR)(LPCSTR)key); // key.MakeLower();

					AD_INFO* tmp_info;
					if( m_ads.Lookup(key, (void*&)tmp_info) )
					{
						// exist same key of ad-info
						ciWARN(("exist same ad-info! (%s)", str_line));
						// delete new ad-info
						delete info;
					}
					else
					{
						ciDEBUG(10, ("%s=%s,%s,%s,%d", key, info->channelName, info->desc, info->siteName, info->networkuse) );
						m_ads.SetAt(key, info);
					}
				}
			}

			str_line = str_lines.Tokenize("\r\n", pos);	// divide by line
		}

		if( channel_part == false && 
			ad_part      == false && 
			channel_part_count == 1 && 
			ad_part_count      == 1 )
		{
			// all ok
			ciDEBUG(10, ("read xml ok") );
			m_bAlreadyRead = true;
			return true;
		}

		// something wrong
		ciWARN(("something wrong"));
	}

	// all fail
	Clear();
	ciWARN(("read xml fail"));
	return false;
}

CString	CChannelXml::GetXML()
{
	// header
	CString xml_ansi = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";

	// channel part
	xml_ansi += "<channel>\r\n";
	xml_ansi += "\t<ch_info name=\"\">\r\n";
	for(int i=0; i<8; i++)
	{
		CString str;
		str.Format("\t\t<ch name=\"%s\" set_ad=\"%s\"/>\r\n", m_channels[i].channel, m_channels[i].channelName);
		xml_ansi += str;
	}
	xml_ansi += "\t</ch_info>\r\n";

	// create ad_info list
	CArray<AD_INFO*, AD_INFO*>	list_adinfo;
	POSITION pos = m_ads.GetStartPosition();
	CString key;
	while(pos)
	{
		AD_INFO* info = NULL;
		m_ads.GetNextAssoc( pos, key, (void*&)info );

		if( info )
		{
			list_adinfo.Add(info);
		}
	}

	// sort ad_info list
	for(int i=0; i<list_adinfo.GetCount()-1; i++)
	{
		AD_INFO* info1 = (AD_INFO*)list_adinfo.GetAt(i);

		for(int j=i+1; j<list_adinfo.GetCount(); j++)
		{
			AD_INFO* info2 = (AD_INFO*)list_adinfo.GetAt(j);

			if( stricmp(info1->channelName, info2->channelName) > 0 )
			{
				list_adinfo.SetAt(i, info2);
				list_adinfo.SetAt(j, info1);

				info1 = (AD_INFO*)list_adinfo.GetAt(i);
				info2 = (AD_INFO*)list_adinfo.GetAt(j);
			}
		}
	}

	// ad_info part
	xml_ansi += "\t<ad_info name=\"\">\r\n";
	for(int i=0; i<list_adinfo.GetCount(); i++)
	{
		AD_INFO* info = (AD_INFO*)list_adinfo.GetAt(i);
		CString str;
		str.Format("\t\t<ad name=\"%s\" desc=\"%s\" sitename=\"%s\" networkuse=\"%d\" />\r\n",
			info->channelName,
			info->desc,
			info->siteName,
			info->networkuse );

		xml_ansi += str;
	}
	xml_ansi += "\t</ad_info>\r\n";
	xml_ansi += "</channel>\r\n";

	return xml_ansi;
}

bool CChannelXml::WriteXML()
{
	ciDEBUG(10, ("WriteXML(%d)", m_bAlreadyRead) );

	if( m_bAlreadyRead == false )
	{
		ciWARN(("channel.xml not reading"));
		return false;
	}

	CString filename;
	filename = EXE_PATH;
	filename += "flash\\xml\\channel.xml";

	CString filename_bak;
	filename_bak = EXE_PATH;
	filename_bak += "flash\\xml\\channel.bak";

	CString filename_bak_bak;
	filename_bak_bak = EXE_PATH;
	filename_bak_bak += "flash\\xml\\channel.bak.bak";

	CString filename_tmp;
	filename_tmp = EXE_PATH;
	filename_tmp += "flash\\xml\\channel.tmp";

	// file open
	CFile file;
	CFileException ex;
	if( file.Open(filename_tmp, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary, &ex) == FALSE )
	{
		ciERROR(("%s file open error", filename_tmp ));
		return false;
	}

	CString xml_ansi = GetXML();
	ciDEBUG(10, ("XML=%s", xml_ansi) );

	// convert ansi -> utf8
	char* buf_utf8 = new char[xml_ansi.GetLength() * 3];
	::ZeroMemory(buf_utf8, xml_ansi.GetLength() * 3);
	int len = scratchUtil::getInstance()->AnsiToUTF8((LPSTR)(LPCSTR)xml_ansi, buf_utf8, xml_ansi.GetLength() * 3);

	if( len <= 0 )
	{
		delete[]buf_utf8;
		ciWARN(("AnsiToUTF8 error!"));
		return false;
	}

	// file write
	TRY
	{
		BYTE header[3] = { 0xEF, 0xBB, 0xBF }; // utf8 BOM
		file.Write(header, 3);
		file.Write(buf_utf8, strlen(buf_utf8));
	}
	CATCH(CFileException, ex)
	{
		delete[]buf_utf8;
		ciERROR(("%s Write Error (errcode:%d)", filename_tmp, ex->m_cause) );
		return false;
	}
	CATCH(CException, ex)
	{
		delete[]buf_utf8;
		ciERROR(("%s Write Error", filename_tmp) );
		return false;
	}
	END_CATCH

	delete[]buf_utf8;
	file.Close();

	::DeleteFile(filename_bak_bak);
	::MoveFile(filename_bak, filename_bak_bak);
	::MoveFile(filename, filename_bak);
	if( ::MoveFile(filename_tmp, filename) == FALSE )
	{
		ciWARN(("%s can't move to %s (errcode:%d)", filename_tmp, filename, ::GetLastError()) );
		return false;
	}

	ciDEBUG(10, ("write xml ok") );
	return true;
}

bool CChannelXml::RefreshXML(const char* additionalLine)
{
	ciDEBUG(10, ("RefreshXML(%s)", additionalLine ? additionalLine : "NULL") );

	Clear(true);

	// getHost info
	ciString host1, host2;
	installUtil::getInstance()->getHost(1, host1);
	installUtil::getInstance()->getHost(2, host2);

	ciDEBUG(10, ("host1=%s", host1.c_str()) );
	ciDEBUG(10, ("host2=%s", host2.c_str()) );

	bool host1_exist = false;
	bool host2_exist = false;

	// check exist host(channelname)
	for(int i=0; i<8; i++)
	{
		if( stricmp(m_channels[i].channelName, host1.c_str()) == 0 )
		{
			ciDEBUG(10, ("%s(host1) exist in ch%d", host1.c_str(), i+1) );
			host1_exist = true;
		}
		if( stricmp(m_channels[i].channelName, host2.c_str()) == 0 )
		{
			ciDEBUG(10, ("%s(host2) exist in ch%d", host2.c_str(), i+1) );
			host2_exist = true;
		}
	}

	// set in empty slot
	for(int i=0; i<8; i++)
	{
		if( host1_exist == false && m_channels[i].channelName == "" )
		{
			ciDEBUG(10, ("%s(host1) set in ch%d", host1.c_str(), i+1) );
			m_channels[i].channelName = host1.c_str();
			host1_exist = true;
		}
		if( host2_exist == false && m_channels[i].channelName == "" )
		{
			ciDEBUG(10, ("%s(host2) set in ch%d", host2.c_str(), i+1) );
			m_channels[i].channelName = host2.c_str();
			host2_exist = true;
		}
	}

	// set in any slot
	for(int i=0; i<8; i++)
	{
		if( i==7 || ( host1_exist == false && stricmp(m_channels[i].channelName, host2.c_str()) != 0 ) )
		{	// last slot, or not-host2-slot
			ciDEBUG(10, ("%s(host1) set in ch%d", host1.c_str(), i+1) );
			m_channels[i].channelName = host1.c_str();
			host1_exist = true;
		}
		if( i==7 || ( host2_exist == false && stricmp(m_channels[i].channelName, host1.c_str()) != 0 ) )
		{	// last slot, or not-host1-slot
			ciDEBUG(10, ("%s(host2) set in ch%d", host2.c_str(), i+1) );
			m_channels[i].channelName = host2.c_str();
			host2_exist = true;
		}
	}

	//
	ciString find_path = EXE_PATH;
	find_path += "\\config\\*.ini";

	ciStringList iniList;
	installUtil::getInstance()->getFileList(find_path.c_str(), iniList);

	ciStringList::iterator itr;
	for( itr=iniList.begin(); itr!=iniList.end(); itr++ )
	{
		ciString filename = itr->c_str();

		if(	strnicmp(filename.c_str(), "__template_", strlen("__template_")) == 0 ||
			strnicmp(filename.c_str(), "__preview_",  strlen("__preview_") ) == 0 ||
			strnicmp(filename.c_str(), "UBCAnnounce", strlen("UBCAnnounce")) == 0 )
		{
			continue;
		}

		ciString channelName = filename;
		ciStringUtil::cutPostfix(channelName, ".");

		// 채널명에 스페이스가 포함되어 있다면, 사용하지 말아야 한다.
		ciStringUtil::charReplace(channelName, ' ', '_');

		ciString siteName, desc, networkUse="0";
		tcpSocketSession::_getIniInfo(channelName.c_str(), siteName, desc, networkUse);

		//
		CString key = channelName.c_str();
		_strlwr((LPSTR)(LPCSTR)key); // key.MakeLower();

		AD_INFO* tmp_info;
		if( m_ads.Lookup(key, (void*&)tmp_info) )
		{
			// exist same key ad-info
			ciWARN(("exist same ad-info! (%s)", filename.c_str()));
		}
		else
		{
			ciDEBUG(10, ("%s=%s,%s,%s,%s", key, channelName.c_str(), desc.c_str(), siteName.c_str(), networkUse.c_str()) );
			tmp_info = new AD_INFO;
			tmp_info->channelName = channelName.c_str();
			tmp_info->desc        = desc.c_str();
			tmp_info->siteName    = siteName.c_str();
			tmp_info->networkuse  = atoi(networkUse.c_str());
			m_ads.SetAt(key, tmp_info);
		}
	}

	if( additionalLine && strlen(additionalLine) )
	{
		ciDEBUG(10, ("additionalLine=%s", additionalLine) );
		CString line = additionalLine;

		AD_INFO* info = new AD_INFO;
		ExtractADInfo(line, *info);

		//
		CString key = (LPCSTR)info->channelName;
		_strlwr((LPSTR)(LPCSTR)key); // key.MakeLower();
		key.Trim(" ");

		if( key.GetLength() > 0 )
		{
			AD_INFO* tmp_info;
			if( m_ads.Lookup(key, (void*&)tmp_info) )
			{
				// exist same key ad-info
				ciWARN(("exist same ad-info! (%s)", additionalLine));
				// overwrite new info
				*tmp_info = *info;
				delete info;
			}
			else
			{
				ciDEBUG(10, ("%s=%s,%s,%s,%d", key, info->channelName, info->desc, info->siteName, info->networkuse) );
				m_ads.SetAt(key, info);
			}
		}
		else
		{
			// exist same key ad-info
			ciWARN(("no key! (%s)", additionalLine));
			delete info;
		}
	}

	m_bAlreadyRead = true;
	WriteXML();

	return true;
}

bool CChannelXml::ChannelAdd(LPCSTR channelName, LPCSTR siteName, LPCSTR desc, LPCSTR networkUse)
{
	ciDEBUG(1, ("ChannelAdd(%s,%s,%s,%s)", channelName, siteName, desc, networkUse) );

	CString key = channelName;
	_strlwr((LPSTR)(LPCSTR)key); // key.MakeLower();

	AD_INFO* tmp_info;
	if( m_ads.Lookup(key, (void*&)tmp_info) == FALSE )
	{
		tmp_info = new AD_INFO;
		m_ads.SetAt(key, tmp_info);
	}

	tmp_info->channelName = channelName;
	tmp_info->desc        = desc;
	tmp_info->siteName    = siteName;
	tmp_info->networkuse  = atoi(networkUse);

	WriteXML();

	return true;
}

bool CChannelXml::ChannelDelete(LPCSTR channelName, LPCSTR siteName, LPCSTR desc, LPCSTR networkUse)
{
	ciDEBUG(1, ("ChannelDelete(%s,%s,%s,%s)", channelName, siteName, desc, networkUse) );

	CString key = channelName;
	_strlwr((LPSTR)(LPCSTR)key); // key.MakeLower();

	AD_INFO* tmp_info;
	if( m_ads.Lookup(key, (void*&)tmp_info) )
	{
		delete tmp_info;
		m_ads.RemoveKey(key);
	}
	else
	{
		ciWARN(("No ad-info(%s,%s,%s,%s)", channelName, siteName, desc, networkUse));
	}

	WriteXML();

	return true;
}

bool CChannelXml::ChannelModify(LPCSTR ch, LPCSTR channelName, LPCSTR networkUse)
{
	ciDEBUG(10, ("ChannelModify(%s, %s, %s)", ch, channelName, networkUse) );

	for(int i=0; i<8; i++)
	{
		if( stricmp(m_channels[i].channel, ch) == 0 )
		{
			ciDEBUG(10, ("%s=%s", m_channels[i].channel, m_channels[i].channelName) );
			m_channels[i].channelName = channelName;
			break;
		}
	}

	NetworkUseModify(channelName, networkUse);

	WriteXML();

	return true;
}

bool CChannelXml::NetworkUseModify(LPCSTR channelName, LPCSTR networkUse)
{
	ciDEBUG(10, ("NetworkUseModify(%s, %s)", channelName, networkUse) );

	CString key = channelName;
	_strlwr((LPSTR)(LPCSTR)key); // key.MakeLower();

	AD_INFO* tmp_info;
	if( m_ads.Lookup(key, (void*&)tmp_info) )
	{
		tmp_info->networkuse = atoi(networkUse);
		return true;
	}

	ciWARN(("No ad-info(%s, %s)", channelName, networkUse));

	return false;
}

bool CChannelXml::ScheduleModify(LPCSTR channelName, LPCSTR site, LPCSTR desc, LPCSTR networkUse)
{
	ciDEBUG(1, ("_scheScheduleModifyduleModify(%s,%s,%s,%s)", channelName, site, desc, networkUse) );

	CString key = channelName;
	_strlwr((LPSTR)(LPCSTR)key); // key.MakeLower();

	AD_INFO* tmp_info;
	if( m_ads.Lookup(key, (void*&)tmp_info) )
	{
		tmp_info->desc = desc;
		tmp_info->siteName = site;
		tmp_info->networkuse = atoi(networkUse);
	}
	else
	{
		ciWARN(("No ad-info(%s,%s,%s,%s)", channelName, site, desc, networkUse));
	}

	WriteXML();

	return true;
}
