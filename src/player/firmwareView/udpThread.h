 /*! \file udpThread.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _udpThread_h_
#define _udpThread_h_

#include <ci/libThread/ciThread.h> 

class udpThread : public ciThread {
public:
	udpThread();
	udpThread(unsigned short port,ciBoolean useSerial);
	virtual ~udpThread();

	virtual void run();
 	
	ciBoolean getTimeStr(string& timeStr);
	string curTimeStr();

	ciBoolean getCurrentSerialServerIP();
	ciBoolean changeSerialServerIP(const char* ip);
	static ciBoolean changeClientIP(const char* ip, const char* mac, const char* hostId);

protected:	

	unsigned short _port;
	ciString _currentSerialServerIp;
	ciBoolean	_useSerial;

};
		
#endif //_udpThread_h_
