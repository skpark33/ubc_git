 /*! \file tcpSocketWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _tcpSocketWorld_h_
#define _tcpSocketWorld_h_

#include <ci/libWorld/ciWorld.h> 
#include "ci/libThread/ciThread.h"
#include <firmwareView/tcpSocketAcceptor.h>
#include <common/libScratch/scratchUtil.h>
//#include <FWV/tcpSocketServer.h>

class tcpSocketWorld : public ciWorld {
public:
	tcpSocketWorld(const char* edition);
	~tcpSocketWorld();
 	
	//[
	//	From ciWorld
	//
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);

	ciBoolean accept();

	void	extraJob();
		//
	//]
protected:
	tcpSocketAcceptor	_acceptor;
	ciString _edition;

	//tcpSocketServer*	_tcpServer;
};
	
class tcpSocketWorldThread : public virtual ciThread {
public:
	void init(const char* edition = STANDARD_EDITION);
	void run();
	void destroy();
	
protected:
	tcpSocketWorld* world;
};
#endif //_tcpSocketWorld_h_
