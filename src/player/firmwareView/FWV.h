// FWV.h : PROJECT_NAME 응용 프로그램에 대한 주 헤더 파일입니다.
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"		// 주 기호입니다.
#include <string>
#include <ci/libThread/ciThread.h> 

using namespace std;

// CFWVApp:
// 이 클래스의 구현에 대해서는 FWV.cpp을 참조하십시오.
//
class tcpSocketWorldThread;
class udpThread;
class CFTServer;
class ShutdownTimer;
class MonitorOnOffTimer;
class agtScreenshotTimer;
class autoUpdateTimer;
class xmlTimer;
class AGTClientSession;
class TimeSyncTimer;
class tcpGuestSocketThread;
class basketContentsTimer;
class GPQSTimer;
class spcContentsTimer;
class spcPrintTimer;

class serverIpCheckThread : public ciThread {
public:
	serverIpCheckThread() {};
	virtual ~serverIpCheckThread(){};
	virtual void run();
protected:	
};

class CFWVApp : public CWinApp
{
public:
	void	shutdownTimer(const char* pTimeStr=0);
	void	week_shutdownTimer(const char* pTimeStr=0);

	CFWVApp();

// 재정의입니다.
	public:
	virtual BOOL InitInstance();

// 구현입니다.

	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();

protected:
	tcpSocketWorldThread* m_worldThread;
	udpThread* m_udpThread;
	CFTServer *m_ftpServer;
	ShutdownTimer*		m_shutdownTimer;
	MonitorOnOffTimer*		m_monitorOnOffTimer;
	agtScreenshotTimer*	m_screenshotTimer;
	autoUpdateTimer*	m_autoUpdateTimer;
	xmlTimer*	m_xmlTimer;
	TimeSyncTimer*		m_timeSyncTimer;
	AGTClientSession* m_agentClient;
	tcpGuestSocketThread* m_guestThread;
	serverIpCheckThread* m_serverIpThread;
	basketContentsTimer*	m_basketContentsTimer;
	GPQSTimer*	m_gpqsTimer;
	spcContentsTimer*	m_spcContentsTimer;
	spcPrintTimer*	m_spcPrintTimer;

	DWORD   SetPrivilege( HANDLE &hToken, TOKEN_PRIVILEGES &tokenPriv );
	DWORD   RestorePrivilege( HANDLE &hToken, TOKEN_PRIVILEGES &tokenPriv );

	void flashSocketServer(const char* edition);
	ciBoolean udpSocketServer(const char* edition);
	void ftpSocketServer();
	void serverIpCheck();
	void screenshotTimer(const char* hostId);
	void setAutoUpdateTimer();
	void getXMLTimer(const char* siteId, const char* hostId);

	void holidayShutdown();
	void timeSyncTimer();
	void agentClient();

	void extrajob(const char* edition,const char* hostId);

	bool	getMonitorOffList(string& outVal);
	bool	getMonitorOff(bool& outVal);

	void	_crateUBCReadyShortCut();

	void	guest_accept();
	long	_getVncPort(const char* pHostId);

	void	_getBooutupTime();
};

extern CFWVApp theApp;


#include <afxtempl.h>

typedef struct {
	CString		channel;
	CString		channelName;
} CHANNEL_INFO;

typedef struct {
	CString		channelName;
	CString		desc;
	CString		siteName;
	int			networkuse;
} AD_INFO;


class CChannelXml
{
public:
	CChannelXml();
	virtual ~CChannelXml();

protected:
	CHANNEL_INFO		m_channels[8];
	CMapStringToPtr		m_ads;

	bool	m_bAlreadyRead;

	void	Clear(bool only_ad=false);

	bool	GetValue(CString& line, LPCSTR key, CString& value);
	bool	ExtractChannelInfo(CString& line, CHANNEL_INFO& info);
	bool	ExtractADInfo(CString& line, AD_INFO& info);

public:
	CString	GetXML();

	bool	ReadXML();
	bool	WriteXML();
	bool	RefreshXML(const char* additionalLine=NULL);

	bool	ChannelAdd(LPCSTR channelName, LPCSTR siteName, LPCSTR desc, LPCSTR networkUse);
	bool	ChannelDelete(LPCSTR channelName, LPCSTR siteName, LPCSTR desc, LPCSTR networkUse);
	bool	ChannelModify(LPCSTR ch, LPCSTR channelName, LPCSTR networkUse);
	bool	NetworkUseModify(LPCSTR channelName, LPCSTR networkUse);
	bool	ScheduleModify(LPCSTR channelName, LPCSTR site, LPCSTR desc,LPCSTR networkUse);
};
