#ifndef _tcpSocketSession_h_
#define _tcpSocketSession_h_

#include <ci/libSocket/ciTaskMutex.h>
#include <ci/libThread/ciMutex.h>
#include <ci/libBase/ciListType.h>
#include <map>
#include <ci/libTimer/ciTimer.h>

#include "tcpSocketHandler.h"

//#include "FWVDlg.h"

class tcpSocketData
{
public:
	static tcpSocketData*	getInstance();
	static void	clearInstance();

	virtual		~tcpSocketData();

	void		enq(const char* data);
	ciBoolean	deq(ciString& res);
	void		clear();

protected:
	tcpSocketData();

	static tcpSocketData*	_instance;
	static ciMutex			_instanceLock;

	ciMutex			_listLock;
	ciStringList	_list;
};


class stopThread : public ciPollable
{
public:
	static stopThread*	getInstance();
	static void	clearInstance();

	virtual		~stopThread();

	void		enq(const char* programId, int display, time_t stopTime);
	void		clear();

	virtual void processExpired(ciString name, int counter, int interval);

protected:
	stopThread();

	static stopThread*	_instance;
	static ciMutex		_instanceLock;

	class _STOPEle
	{
	public:
		ciString	programId;
		int			display;
		time_t		stopTime;
	};
	typedef list<_STOPEle> _STOPList;

	_STOPList	_list;
	ciMutex		_listLock;
};


enum DIRECTIVE_TYPE {
	DIRECTIVE_UNKNOWN = 0,
	DIRECTIVE_ping,
	DIRECTIVE_pe_ping,
	DIRECTIVE_refresh,
	DIRECTIVE_browser_info,
	DIRECTIVE_import,
	DIRECTIVE_displaySoundOn,
	DIRECTIVE_shutdownTime,
	DIRECTIVE_week_shutdownTime,
	DIRECTIVE_FV_COMMAND_SHUTDOWN,
	DIRECTIVE_FV_COMMAND_REFRESH,
	DIRECTIVE_FV_REBOOT,
	DIRECTIVE_FV_SHUTDOWN,
	DIRECTIVE_FV_UBCSTART,
	DIRECTIVE_FV_UBCSTOP,
	DIRECTIVE_FV_STUDIO,
	DIRECTIVE_FV_WEDDING_EXPORT,
	DIRECTIVE_FV_WEDDING_IMPORT,
	DIRECTIVE_FV_QUERY_UBC_STATUS,
	DIRECTIVE_FV_CH_PLAY,
	DIRECTIVE_FV_CH_DOWNLOAD,
	DIRECTIVE_FV_CH_STOP,
	DIRECTIVE_FV_QUERY_CURRENT_CH,
	DIRECTIVE_FV_AUTO_OFF,
	DIRECTIVE_FV_AUTO_ON,
	DIRECTIVE_FV_CH_MODIFY,
	DIRECTIVE_FV_SCHEDULE_MODIFY,
	DIRECTIVE_FV_IMPORT,
	DIRECTIVE_FV_EXPORT,
	DIRECTIVE_FV_REFRESH,
	DIRECTIVE_FV_CH_DELETE,
	DIRECTIVE_FV_DELETE_ALL,
	DIRECTIVE_FV_QUERY_RECOVERY,
	DIRECTIVE_FV_CH_RECOVERY,
	DIRECTIVE_FV_CH_ADD,
	DIRECTIVE_FV_DISPLAY_SET,
	DIRECTIVE_FV_TOOLS,
	DIRECTIVE_FV_UPDATE,
	DIRECTIVE_FV_REGISTER,
	DIRECTIVE_FV_SHUTDOWNTIME,
	DIRECTIVE_FV_WEEK_SHUTDOWNTIME,
	DIRECTIVE_FV_MOUSE,
	DIRECTIVE_FV_STARTER,
	DIRECTIVE_FV_APPLICATION,
};

class CChannelXml;

class tcpSocketSession : public tcpSocketHandler
{
public:
	tcpSocketSession();
	virtual ~tcpSocketSession();

	int	svc();

	ciBoolean	readline(ciString& outVal);
	ciBoolean	writeline(const char* inVal);
	ciBoolean	writeline(ciString& inVal);
	ciBoolean	writePing();
	ciBoolean	xmlRefresh(const char* additionalLine=NULL);

	void		toUTF8(ciString& xmlBuf);
	void		toAnsi(ciString& xmlBuf);

protected:
	ciBoolean	tcpServer(int port);
	ciBoolean	parse(const char* command, ciString& res);
	ciBoolean	import(string& scheduleFile);
	ciBoolean	import2(string& arg);
	ciBoolean	firmwareView(const char* directive, const char* data, ciString& res);
	ciBoolean	keyboardSimulator(const char* command, const char* data);

protected:	
	string		_curTimeStr();
	int			_kbdEvent(const char* value, DWORD flag);

	ciBoolean	_setData(ciString& data);
	const char*	_getData(const char* name);

	ciBoolean	_readXml();
	ciBoolean	_writeXml();
	ciBoolean	_isEOLXml(const char* xmlLine);
	ciBoolean	_channelModify(const char* ch, const char* channelName, const char* networkUse);
	ciBoolean	_networkUseModify(ciString& buf,const char* channelName, const char* networkUse);
	ciBoolean	_scheduleModify(const char* channelName, const char* site, const char* desc,const char* networkUse);

	ciBoolean	_channelAdd(const char* channelName,
							const char* siteName,
							const char* desc,
							const char* networkUse);
	ciBoolean	_channelDelete(const char* channelName,
							   const char* siteName,
							   const char* desc,
							   const char* networkUse);

	void		_buildXMLLine(ciStringList& iniList,
							  ciStringList& outXMLLineList,
							  ciStringList& outXMLCompareLineList,
							  const char* additionalLine);

	ciBoolean	_addStatus(ciString& res);

	ciStringMap	_dataMap;
	ciString	_xmlBuf;
	ciString	_edition;

	ciMutex		_sockLock;
	ciDouble	_installedVersion;

	ciBoolean _isShutdownMenuDisable;
	//
	DIRECTIVE_TYPE _getDirectiveType(ciString& directive)	{ return _getDirectiveType(directive.c_str()); };
	DIRECTIVE_TYPE _getDirectiveType(const char* directive);

	CChannelXml*	m_channelXML;

public:

	static ciBoolean	_getIniInfo(const char* channelName, 
							ciString& out_siteId, 
							ciString& out_desc, 
							ciString& out_networkUse);

};

#endif
