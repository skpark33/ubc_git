#include "stdafx.h"

#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciAceType.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <cci/libValue/cciEntity.h>
#include "libAGTClient/AGTClientSession.h"
#include "libAGTClient/AGTProcessManager.h"
#include <ci/libBase/ciStringTokenizer.h>
#include <common/libInstall/installUtil.h>
#include <common/libScratch/scratchUtil.h>
#include <common/libCommon/ubcUtil.h>
#include <common/libCommon/ubcIni.h>
#include <common/libPlan/planManager.h>
#include "libAnnounce/announceEventHandler.h"

#include "tcpSocketSession.h"
#include "FWV.h"
//#include "iconv.h"

ciSET_DEBUG(10,"tcpSocketSession");

#define BUF_LEN 128
#define CODE_LEN 89
#define EXE_PATH _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\")


#define VK_1 48
#define VK_2 49
#define VK_3 50
#define VK_4 51
#define VK_5 52
#define VK_6 53
#define VK_7 54
#define VK_8 55
#define VK_9 56
#define VK_0 57

#define VK_A 65
#define VK_B 66
#define VK_C 67
#define VK_D 68
#define VK_E 69
#define VK_F 70
#define VK_G 71
#define VK_H 72
#define VK_I 73
#define VK_J 74
#define VK_K 75
#define VK_L 76
#define VK_M 77
#define VK_N 78
#define VK_O 79
#define VK_P 80
#define VK_Q 81
#define VK_R 82
#define VK_S 83
#define VK_T 84
#define VK_U 85
#define VK_V 86 
#define VK_W 87
#define VK_X 88
#define VK_Y 89
#define VK_Z 90


unsigned char virtualCode[CODE_LEN] = 
{ 
	0,VK_ESCAPE,VK_1,VK_2,VK_3,VK_4,VK_5,VK_6,VK_7,VK_8,
	VK_9,VK_0,VK_SUBTRACT,0,VK_BACK,VK_TAB,VK_Q,VK_W,VK_E,VK_R,
	VK_T,VK_Y,VK_U,VK_I,VK_O,VK_P,0,0,VK_RETURN,VK_CONTROL,
	VK_A,VK_S,VK_D,VK_F,VK_G,VK_H,VK_J,VK_K,VK_L,0,
	0,0,VK_LSHIFT,0,VK_Z,VK_X,VK_C,VK_V,VK_B,VK_N,
	VK_M,0,0,0,VK_RSHIFT,VK_PRINT,VK_MENU,VK_SPACE,VK_CAPITAL,VK_F1,
	VK_F2,	VK_F3,	VK_F4,	VK_F5,	VK_F6,	VK_F7,	VK_F8,	VK_F9,	VK_F10,VK_NUMLOCK,
	VK_SCROLL,VK_HOME,VK_UP,VK_PRIOR,VK_SUBTRACT,VK_LEFT,0,VK_RIGHT,VK_ADD,VK_END,
	VK_DOWN,VK_NEXT,VK_INSERT,VK_DELETE,0,0,0,VK_F11,VK_F12
};

const char* scanString[CODE_LEN] = 
{
	"","ESC","1","2","3","4","5","6","7","8",
	"9","0","MINUS","EQUAL","BS","TAB","Q","W","E","R",
	"T","Y","U","I","O","P","SQUARE_OPEN","SQUARE_CLOSE","ENTER","CTRL",
	"A","S","D","F","G","H","J","K","L","SEMICOLON",
	"QUOTATION","QUOTATION2","LSHIFT","WON","Z","X","C","V","B","N",
	"M","COMMA","PERIOD","SLASH","RSHIFT","PRTSC","ALT","SPACE","CAPS","F1",
	"F2","F3","F4","F5","F6","F7","F8","F9","F10","NUM",
	"SCROLL","HOME","UP","PGUP","MINUS","LEFT","CENTER","RIGHT","PLUS","END",
	"DOWN","PGDN","INS","DEL","","","","F11","F12"
};


//
// tcpSocketData Class
//

tcpSocketData*	tcpSocketData::_instance = NULL;
ciMutex			tcpSocketData::_instanceLock;

tcpSocketData* tcpSocketData::getInstance()
{
	if( _instance == NULL )
	{
		ciGuard aGuard(_instanceLock);
		if( _instance == NULL )
		{
			_instance = new tcpSocketData;
		}
	}
	return _instance;
}

void tcpSocketData::clearInstance()
{
	if( _instance )
	{
		ciGuard aGuard(_instanceLock);
		if( _instance )
		{
			delete _instance;
			_instance = NULL;
		}
	}
}

tcpSocketData::tcpSocketData()
{
  	clear();
}

tcpSocketData::~tcpSocketData()
{
	clear();
}

void tcpSocketData::clear()
{
	ciGuard aGuard(_listLock);
	_list.clear();
}

void tcpSocketData::enq(const char* data)
{
	ciGuard aGuard(_listLock);
	ciDEBUG(1, ("enq(%s)", data) );
	_list.push_back(data);
}

ciBoolean tcpSocketData::deq(ciString& res)
{
	ciGuard aGuard(_listLock);
	if( _list.size() > 0 )
	{
		res = _list.front();
		_list.pop_front();
		ciDEBUG(1, ("deq(%s)", res.c_str()) );
		return ciTrue;

	}
	ciDEBUG(1, ("no data in q()") );
	return ciFalse;
}

//
// stopThread Class
//

stopThread*	stopThread::_instance = NULL; 
ciMutex		stopThread::_instanceLock;

stopThread*	stopThread::getInstance()
{
	if( _instance == NULL )
	{
		ciGuard aGuard(_instanceLock);
		if( _instance == NULL )
		{
			_instance = new stopThread;
		}
	}
	return _instance;
}

void stopThread::clearInstance()
{
	if( _instance )
	{
		ciGuard aGuard(_instanceLock);
		if( _instance )
		{
			delete _instance;
			_instance = NULL;
		}
	}
}

stopThread::stopThread()
{
  	clear();
}

stopThread::~stopThread()
{
	clear();
}

void stopThread::clear()
{
	ciGuard aGuard(_listLock);
	_list.clear();
}

void stopThread::enq(const char* programId, int display, time_t stopTime)
{
	ciGuard aGuard(_listLock);
	ciDEBUG(1, ("enq(%s,%d,%ld)", programId, display, stopTime) );

	_STOPEle data;
	data.programId	= programId;
	data.display	= display;
	data.stopTime	= stopTime;

	_list.push_back(data);
}

void stopThread::processExpired(ciString name, int counter, int interval)
{
	//ciDEBUG(3, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));
	ciGuard aGuard(_listLock);

	ciTime now;
	_STOPList::iterator itr = _list.begin();
	while( itr != _list.end() )
	{
		_STOPEle data = *itr;
		if( now >= (data.stopTime) )
		{
			installUtil::getInstance()->stopBrowser(data.display, data.programId.c_str());
			_list.erase(itr++);
			continue;
		}
		++itr;
	}
	return ;
}

tcpSocketSession::tcpSocketSession()
{
	ciDEBUG(1, ("tcpSocketSession") );
	ciString host, mac;
	if( !scratchUtil::getInstance()->readAuthFile(host,mac,_edition) )
	{
		ciWARN(("readAuth failed"));
	}
	_installedVersion = scratchUtil::getInstance()->getInstalledVersion();
	ciDEBUG(1, ("tcpSocketSession installedVersion=%f", _installedVersion) );

	m_channelXML = new CChannelXml;
	ciDEBUG(1, ("create channel.xml object") );

	_isShutdownMenuDisable = ciFalse;
	ubcConfig aIni("UBCVariables");
	aIni.get("ROOT","SHUTDOWN_MENU_DISABLE",_isShutdownMenuDisable);

	ciDEBUG(1,("_isShutdownMenuDisable=%d", _isShutdownMenuDisable));
}

tcpSocketSession::~tcpSocketSession()
{
	ciDEBUG(1, ("~tcpSocketSession") );
	if( m_channelXML ) delete m_channelXML;
	ciDEBUG(1, ("delete channel.xml object") );
}

int tcpSocketSession::svc()
{
	ciDEBUG(1, ("svc()") );

	while(1)
	{
		ciString command = "";
		if( readline(command) == ciFalse )
		{
			ciWARN(("recv error"));
			break;
		}
		if( command.empty() )
		{
			ciDEBUG(1, ("socket broken") );
			break;
		}

		DIRECTIVE_TYPE dir_type = _getDirectiveType(command);
		if( dir_type != DIRECTIVE_ping )
		{
			ciDEBUG(1, ("(%s) received", command.c_str()) );
		}

		ciString res = "";
		ciBoolean retval = parse(command.c_str(), res);

		if( res.empty() )
		{
			ciDEBUG(1, ("don't need to reply") );
			break;
		}

		if( writeline(res.c_str()) == ciFalse )
		{
			ciDEBUG(1, ("send error") );
			break;
		}
		if( dir_type != DIRECTIVE_ping )
		{
			ciDEBUG(1, ("%s Send", res.substr(0, (res.size()>3000 ? 3000 : res.size())).c_str()) );
		}
	} // receive loop while

	ciDEBUG(1, ("%s Client : connection close....\n", (const char*)_curTimeStr().c_str()) );
	return 0;
}

ciBoolean tcpSocketSession::parse(const char* command, ciString& res)
{
	string directive;
	string data;
	boolean directive_retreived = false;

	int len = strlen(command);
	for(int i=0; i<len; i++)
	{
		if( !directive_retreived && command[i] == '|')
		{
			directive_retreived = true;
			continue;
		}
		if(directive_retreived)
			data += command[i];
		else
			directive += command[i];
	}

	DIRECTIVE_TYPE dir_type = _getDirectiveType(directive);

	// as a response of ping from swf
	if( dir_type != DIRECTIVE_ping )
	{
		ciDEBUG(1, ("%s|%s", directive.c_str(), data.c_str()) );
	}

	ciBoolean retval = 0;

	switch(dir_type)
	{
	// as a response of pe studio
	case DIRECTIVE_pe_ping:
		{
			char hostName[BUF_LEN] = {0};
			DWORD namesize = BUF_LEN;
			GetComputerNameEx(ComputerNamePhysicalDnsHostname, hostName, &namesize);

			// get shutdown time
			string timeStr = "";
			ubcConfig aIni("UBCVariables");
			aIni.get("SHUTDOWNTIME", "value", timeStr);

			// get mac address
			string mac = "";
			if(scratchUtil::getInstance()->getMacAddress(mac))
			{
				ciDEBUG(1, ("mac=%s", mac.c_str()) );
				ciStringUtil::stringReplace(mac,"-","");  // "-" 을 없앰.
			}
			else
			{
				ciERROR(("mac address get failed"));
			}

			// get 모니터의 갯수
			int monitorCount = scratchUtil::getInstance()->getMonitorCount();

			char resBuf[BUF_LEN] = {0};
			sprintf(resBuf, "%s|%s|%s|%d", hostName, timeStr.c_str(), mac.c_str(), monitorCount);
			ciDEBUG(1, ("resBuf=%s", resBuf) );
			res = resBuf;
			return ciTrue;
		}
		break;

	// from Agent
	case DIRECTIVE_FV_COMMAND_SHUTDOWN:
		{
			tcpSocketData::getInstance()->enq(directive.c_str());
			return ciTrue;
		}
		break;

	// from Impoter2, predownloader
	case DIRECTIVE_refresh:
		{
			if( xmlRefresh(data.c_str()) )
			{
				tcpSocketData::getInstance()->enq("FV_COMMAND_REFRESH");
				return ciTrue;
			}
			ciERROR(("refresh failed"));
			return ciFalse;
		}
		break;

	case DIRECTIVE_browser_info:
		// from brw
		retval = planManager::getInstance()->putBRWInfo(data.c_str());
		break;

	case DIRECTIVE_import:
		// from studio
		retval =  import2(data);
		break;

	//case DIRECTIVE_displaySoundOn:
	//	// from deviceWizard
	//	retval =  installUtil::getInstance()->displaySoundOn(data, res);
	//	break;

	case DIRECTIVE_shutdownTime:
		// from studio
		retval =  installUtil::getInstance()->setShutdownTime(data, ciTrue);
		if(retval)
			theApp.shutdownTimer(data.c_str());
		break;

	case DIRECTIVE_week_shutdownTime:
		// from studio
		retval =  installUtil::getInstance()->setWeekShutdownTime(data,ciTrue);
		if(retval)
			theApp.week_shutdownTimer(data.c_str());
		break;

	case DIRECTIVE_ping:
		{
			ciString qData;
			if( tcpSocketData::getInstance()->deq(qData) )
			{
				DIRECTIVE_TYPE qdata_type = _getDirectiveType(qData);

				switch(qdata_type)
				{
				case DIRECTIVE_FV_COMMAND_SHUTDOWN:
					scratchUtil::getInstance()->getFocus("UBC FirmwareView");
					directive = qData;
					break;

				case DIRECTIVE_FV_COMMAND_REFRESH:
					directive = qData;
					break;
				}
			}

			_addStatus(res);

			retval = ciTrue;
		}
		break;

	default:
		if( strncmp(directive.c_str(), "FV_", 3) == 0 )
		{
			// from flash
			//AfxGetApp()->BeginWaitCursor();
			toAnsi(data);
			_setData(data);
			retval = firmwareView(directive.c_str(), data.c_str(), res);
			toUTF8(res);
			//AfxGetApp()->EndWaitCursor();
		}
		else
		{
			retval = keyboardSimulator(directive.c_str(), data.c_str());
		}
		break;
	}

	if( res.empty() )
	{
		res = directive;
		res += "|";
		if( retval )
			res += "OK\0";
		else
			res += "NO\0";
	}
	else
	{
		ciString buf = res;
		res = directive;
		res += "|";
		res += buf;
	}

	return retval;
}

ciBoolean tcpSocketSession::_addStatus(ciString& res)
{
	ciDEBUG(9, ("addStatus()") );

	ciString service = "Service=";
	HANDLE hMutex = ::OpenMutex(MUTEX_ALL_ACCESS, FALSE, "UTV_starter");
	if( hMutex )
	{
		service += "START";
		ciDEBUG(1, ("UTV_starter is alive") );
		::CloseHandle(hMutex);
	}
	else
	{
		service += "STOP";
		ciDEBUG(1, ("UTV_starter is dead") );
	}
	res = service;

	if( _edition == ENTERPRISE_EDITION )
	{
		ciBoolean connectionStatus = AGTClientSession::getInstance()->getConnectionStatus();
		ciDEBUG(1, ("CONNECTION STATUS=%d", connectionStatus) );

		res += ",Network=";
		if( connectionStatus )
		{
			ciString ip = "";
			scratchUtil::getInstance()->getIpAddress(ip);
			if( !ip.empty() && ip != "127.0.0.1" )
				res += ip;
			else
				res += "STOP";
			//res += "START";
		}
		else
		{
			res += "STOP";
		}
	}
	else
	{
		res += ",Network=STOP";
	}
	return ciTrue;
}

ciBoolean tcpSocketSession::firmwareView(const char* directive, const char* data, ciString& res)
{
	ciDEBUG(1, ("firmwareView(%s,%s)", directive, data) );

	DIRECTIVE_TYPE dir_type = _getDirectiveType(directive);
	//
	// HOME
	//
	switch( dir_type )
	{
	case DIRECTIVE_FV_REBOOT:
		{
			ciString command = "rebootSystem.bat";
			ciString arg = "";
			//if(system(command.c_str())==0){
			if( scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(), 0, ciFalse) )
			{
				ciDEBUG(1, ("%s %s succeed", command.c_str(), arg.c_str()) );
				return ciTrue;
			}
			ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
			return ciFalse;
		}
		break;

	case DIRECTIVE_FV_SHUTDOWN:
		{
			if(!_isShutdownMenuDisable) {
				ciString command = "shutdownSystem.bat";
				ciString arg = "";
				//if(system(command.c_str())==0){
				if( scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(), 0, ciFalse) )
				{
					ciDEBUG(1, ("%s %s succeed", command.c_str(), arg.c_str()) );
					return ciTrue;
				}
				ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
			}
			return ciFalse;
		}
		break;

	case DIRECTIVE_FV_UBCSTART:
		{
			//HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
			AGTProcessManager::getInstance()->updateState("UTV_starter.exe", 0);
			HWND sttHwnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
			if( sttHwnd == NULL )
			{
				if( ciArgParser::getInstance()->isSet("+minimize") )
					return installUtil::getInstance()->startUBC("+bg");
				else
					return installUtil::getInstance()->startUBC();
			}

			HWND brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2.exe");
			if( brwHwnd == NULL )
				brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2_UBC1.exe");

			if( brwHwnd )
			{
				ciDEBUG(1, ("UBC already Started") );
				COPYDATASTRUCT appInfo;
				appInfo.dwData = 900;  // ShowWindow
				appInfo.lpData = (char*)"";
				appInfo.cbData = 1;
				//::PostMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
				ciDEBUG(1, ("UTV_BRW ShowWindow SendMessage") );
				scratchUtil::getInstance()->getFocus(brwHwnd);
				return ciTrue;
			}
			else
			{
				installUtil::getInstance()->stopUBC();
				return installUtil::getInstance()->startUBC();
			}
		}
		break;

	case DIRECTIVE_FV_UBCSTOP:
		{
			AGTProcessManager::getInstance()->updateState("UTV_starter.exe", -1);
			// icon tray 에서 starter 를 제거한다.
			//scratchUtil::getInstance()->removeIconTray("UTV Starter", 169);
			return installUtil::getInstance()->stopUBC();
		}
		break;

	case DIRECTIVE_FV_STUDIO:
		{
			//HWND hwind = ::FindWindow(NULL, "UBCStudio PE");
			HWND hwind2 = NULL;
			ciString lang = scratchUtil::getInstance()->getLang();
			if( !lang.empty() )
			{
				ciString binary2 = "UBCStudio";
				if( _edition == ENTERPRISE_EDITION )
					binary2 += "EE_";
				else
					binary2 += "PE_";

				binary2 += lang;
				binary2 += ".exe";
				hwind2 = scratchUtil::getInstance()->getWHandle(binary2.c_str());
			}

			//HWND hwind1 = scratchUtil::getInstance()->getWHandle("UBCStudio.exe");
			//if (!hwind1 && !hwind2) {
			if( hwind2 == NULL )
			{
				ciWARN(("STUDIO Window Handle is not found"));
				scratchUtil::getInstance()->removeIconTray("UTV Starter", 169);

				//ciString command = "UBCStudioUpdate.bat";
				
				ciString dir = _COP_CD("C:\\SQIsoft\\UTV1.0\\execute");
				ciString command = _COP_CD("C:\\SQIsoft\\UTV1.0\\execute\\SelfAutoUpdate.exe");

				ciString arg = " +update_only +log +ignore_env ";
				if( _edition == ENTERPRISE_EDITION )
				{
					//command = "UBCStudioEEUpdate.bat";
					arg +=  "+studioEE";
				}
				else
				{
					arg +=  "+studio";
				}

				//if(system(command.c_str())==0){
				if( scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(), dir.c_str(), ciFalse) )
				{
					ciDEBUG(1, ("%s %s succeed", command.c_str(), arg.c_str()) );
					// 다른 UBC 와 POWERPNT를 모두 죽인다.
					scratchUtil::getInstance()->createProcess("ubckill.bat", arg.c_str(), dir.c_str(), ciFalse);
					ciDEBUG(1, ("ubckill.bat executed") );
					return ciTrue;
				}
				ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
				return ciFalse;
			}
			else
			{
				ciWARN(("STUDIO Window already exist"));
				if( hwind2 )
				{
					::ShowWindow(hwind2, SW_SHOWNORMAL);
					scratchUtil::getInstance()->getFocus(hwind2);
				}
				//if(hwind1){
				//	::ShowWindow(hwind1, SW_SHOWNORMAL);
				//	scratchUtil::getInstance()->getFocus(hwind1);
				//}
				return ciTrue;
			}
		}
		break;

	case DIRECTIVE_FV_WEDDING_EXPORT:
		{
			ciString binary2 = "UBCWeddingExporter.exe";
			HWND hwind2 = scratchUtil::getInstance()->getWHandle(binary2.c_str());
			if( hwind2 == NULL )
			{
				ciWARN(("%s Window Handle is not found", binary2.c_str()));
				if( scratchUtil::getInstance()->createProcess(binary2.c_str(), "", 0, ciFalse) )
				{
					ciDEBUG(1, ("%s succeed", binary2.c_str()) );
				}
				ciERROR(("%s fail", binary2.c_str()));
				return ciFalse;
			}
			else
			{
				ciWARN(("%s Window already exist", binary2.c_str()));
				if( hwind2 )
				{
					::ShowWindow(hwind2, SW_SHOWNORMAL);
					scratchUtil::getInstance()->getFocus(hwind2);
				}
				return ciTrue;
			}
		}
		break;

	case DIRECTIVE_FV_WEDDING_IMPORT:
		{
			ciString binary2 = "UBCWeddingImporter.exe";
			HWND hwind2 = scratchUtil::getInstance()->getWHandle(binary2.c_str());
			if( hwind2 == NULL )
			{
				ciWARN(("%s Window Handle is not found", binary2.c_str()));
				if( scratchUtil::getInstance()->createProcess(binary2.c_str(), "", 0, ciFalse) )
				{
					ciDEBUG(1, ("%s succeed", binary2.c_str()) );
					installUtil::getInstance()->stopBrowser3(1);
					installUtil::getInstance()->stopBrowser3(2);
					return ciTrue;
				}
				ciERROR(("%s fail", binary2.c_str()));
				return ciFalse;
			}
			else
			{
				ciWARN(("%s Window already exist", binary2.c_str()));
				if( hwind2 )
				{
					::ShowWindow(hwind2, SW_SHOWNORMAL);
					scratchUtil::getInstance()->getFocus(hwind2);
					installUtil::getInstance()->stopBrowser3(1);
					installUtil::getInstance()->stopBrowser3(2);
				}
				return ciTrue;
			}
		}
		break;

	case DIRECTIVE_FV_QUERY_UBC_STATUS:
		{
			ciString OPT = "SRC";
			if( !installUtil::getInstance()->getDisplayOption(OPT) )
			{
				ciWARN(("get Display option failed, default value %s will be used", OPT.c_str()));
			}
			ciBoolean show = ciFalse;
			if( !installUtil::getInstance()->getMouseOption(show) )
			{
				ciWARN(("get Mouse option failed, default value %d will be used", show));
			}
			ciBoolean autoValue = ciFalse;
			if( !installUtil::getInstance()->getAutoUpdate(autoValue) )
			{
				ciWARN(("get AutoUpdateFlag failed, default value %d will be used", autoValue));
			}
			ciString shutdownTime = "";
			if( !installUtil::getInstance()->getShutdownTime(shutdownTime) )
			{
				ciWARN(("get shutdownTime failed, default value %s will be used", shutdownTime.c_str()));
			}
			ciString week_shutdownTime = "";
			if( !installUtil::getInstance()->getWeekShutdownTime(week_shutdownTime) )
			{
				ciWARN(("get week_shutdownTime failed, default value %s will be used", week_shutdownTime.c_str()));
			}

			char buf[1024] = {0};
			sprintf(buf, "OPT=%s,Show=%d,ShutdownTime=%s,WeekShutdownTime=%s,Auto=%d", 
				OPT.c_str(), show, shutdownTime.c_str(), week_shutdownTime.c_str(), autoValue);

			ciDEBUG(1, ("%s", buf) );

			res = buf;
			/*
			HANDLE hMutex = ::CreateMutex(NULL,FALSE, "UTV_starter");
			if( hMutex != NULL )
			{
				if( ::GetLastError() == ERROR_ALREADY_EXISTS )
				{
					res = "START\0";
					::CloseHandle(hMutex);
					ciDEBUG(1, ("FV_QUERY_UBC_STATUS %s will be return", res.c_str()) );
					return ciTrue;
				}
			}
			res = "STOP\0";
			*/
			ciDEBUG(1, ("FV_QUERY_UBC_STATUS %s will be return", res.c_str()) );
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_CH_PLAY:
		{
			const char* host = _getData("ChannelName");
			if( host == NULL || strlen(host) == 0 )
			{
				ciERROR(("ChannelName is null"));
				return ciFalse;
			}
			const char* site = _getData("SiteName");
			if( site == NULL || strlen(site) == 0 )
			{
				ciERROR(("SiteName is null"));
				site = "USER";
				//return ciFalse;
			}

			ciBoolean isNetwork = ciTrue;
			const char* networkUse = _getData("NetworkUse");
			if( networkUse == NULL || strlen(networkUse) == 0 )
			{
				ciERROR(("networkUse is null"));
				networkUse=ciFalse;
				//return ciFalse;
			}
			else
			{
				if( atoi(networkUse) == 0 )
					isNetwork=ciFalse;
			}

			int display = 1;
			const char* strBuf = _getData("Display");
			if( strBuf == NULL || strlen(strBuf) == 0 )
			{
				ciWARN(("Display is null"));
			}
			else
			{
				display = atoi(strBuf);
			}

			if( !installUtil::getInstance()->runBrowser(display, host, site, isNetwork) )
			{
				ciERROR(("browser(%d) run failed", display));
				return ciFalse;
			}
			ciTime now;
			installUtil::getInstance()->writeBrowser(now, host, display, "FV_CH_PLAY");

			// 
			//HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
			HWND sttHwnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
			if( sttHwnd == NULL )
			{
				ciWARN(("STT Window Handle is not found"));
				//installUtil::getInstance()->stopUBC();
				return installUtil::getInstance()->startUBC();
			}
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_CH_DOWNLOAD:
		{
			const char* host = _getData("ChannelName");
			if( host == NULL || strlen(host) == 0 )
			{
				ciERROR(("ChannelName is null"));
				return ciFalse;
			}
			const char* site = _getData("SiteName");
			if( site == NULL || strlen(site) == 0 )
			{
				ciERROR(("SiteName is null"));
				site = "USER";
				//return ciFalse;
			}

			ciBoolean isNetwork = ciTrue;
			const char* networkUse = _getData("NetworkUse");
			if( networkUse == NULL || strlen(networkUse) == 0 )
			{
				ciERROR(("networkUse is null"));
				networkUse=ciFalse;
				//return ciFalse;
			}
			else
			{
				if( atoi(networkUse) == 0 )
					isNetwork=ciFalse;
			}

			int display = 1;
			const char* strBuf = _getData("Display");
			if(!strBuf || !strlen(strBuf)){
				ciWARN(("Display is null"));
			}else{
				display = atoi(strBuf);
			}
		
			ciString para = " +site " ;
			para += site ;
			para += " +host " ;
			para += host;
			para += " +appId BROWSER +socketproxy +log +ignore_env +download_only +autoscale +center +dbglog ";
			if( scratchUtil::getInstance()->createProcess("UTV_brwClient2.exe", para.c_str(), 0, ciFalse) )
			{
				ciDEBUG(1,("UTV_brwClient2.exe %s succeed", para.c_str()));
				return ciTrue;
			}

			/*
			if( !installUtil::getInstance()->runBrowser(display, host, site, "+download_only", isNetwork) )
			{
				ciERROR(("browser +download_only (%d) run failed", display));
				return ciFalse;
			}
			ciDEBUG(1, ("browser +download_only succeed") );
			*/

			announceEventHandler aHandler(site, host);
			aHandler.announce("*", ciTrue, ciFalse);

			return ciTrue;
		}
		break;

	/*
	// 
	// Chanel Service
	//
	if(strncmp(directive,"FV_CH_PLAY",strlen("FV_CH_PLAY"))==0)
	{
		ciString argName = "STARTER.APP_BROWSER.ARGUMENT";
		ciString autoStartUpName = "STARTER.APP_BROWSER.AUTO_STARTUP";
		ciString processId="BROWSER";
		ciString binary="UTV_brwClient2.exe";
		if(strcmp(directive,"FV_CH_PLAY_B")==0){
			argName="STARTER.APP_BROWSER1.ARGUMENT";
			autoStartUpName="STARTER.APP_BROWSER1.AUTO_STARTUP";
			processId="BROWSER1";
			binary="UTV_brwClient2_UBC1.exe";
		}

		// set 하기 전에 Old 값을 보관한다.
		ciString oldHost;
		ciString oldSite;
		ciBoolean oldIsNetwork;
		installUtil::getInstance()->getChannelName(argName.c_str(),oldHost,oldSite,oldIsNetwork);
		ciDEBUG(1,("oldHost=(%s), oldSite=(%s), oldNetworkUse=(%d))", 
			oldHost.c_str(), oldSite.c_str(), oldIsNetwork);

		const char* host = _getData("ChannelName");
		if(!host || !strlen(host)){
			ciERROR(("ChannelName is null"));
			return ciFalse;
		}
		const char* site = _getData("SiteName");
		if(!site || !strlen(site)){
			ciERROR(("SiteName is null"));
			//return ciFalse;
		}
		const char* networkUse = _getData("NetworkUse");
		if(!networkUse || !strlen(networkUse)){
			ciERROR(("networkUse is null"));
			//return ciFalse;
		}
		ciBoolean isNetwork = (atoi(networkUse) ? ciTrue : ciFalse);
		ciDEBUG(1,("newHost=(%s), newSite=(%s), newNetworkUse=(%s)", host,site,networkUse));

		if(!installUtil::getInstance()->setChannelName(argName.c_str(),autoStartUpName.c_str(),
														"true",
														host,site,isNetwork,ciTrue)) {
			ciERROR(("setChannelProperty Error"));
			return ciFalse;
		}

		//HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
		HWND sttHwnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
		if (!sttHwnd) {
			ciWARN(("STT Window Handle is not found"));
			//installUtil::getInstance()->stopUBC();
			return installUtil::getInstance()->startUBC();
		}else{
			//HWND brwHwnd = ::FindWindow(NULL, "UTV_BRW");
			HWND brwHwnd = scratchUtil::getInstance()->getWHandle(binary.c_str());
			if(brwHwnd){
				ciDEBUG(1,("STT Window Handle  found, "));
				// 브라우저가 현재 플레이 중이라면,  같은 채널인지 검사가 필요하다.
				// 채널이 같으면, 그냥 showWindow 만 하면 되지만,
				// 채널이 다르면, 죽이고 다시 살려야 하기 때문이다.
				//::ShowWindow(brwHwnd, SW_SHOWNORMAL);
				if(oldHost==host){
					ciDEBUG(1,("Same Channel(%s) play", host));
					COPYDATASTRUCT appInfo;
					appInfo.dwData = 900;  // ShowWindow
					appInfo.lpData = (char*)"";
					appInfo.cbData = 1;
					//::PostMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
					::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
					ciDEBUG(1,("UTV_BRW ShowWindow SendMessage"));
					scratchUtil::getInstance()->getFocus(brwHwnd);
					return ciTrue;
				}
			}
			if(brwHwnd){
				ciDEBUG(1,("Channel Different!!!!(%s!=%s) kill current Browser ", oldHost.c_str(), host));
				// 브라우저가 살아있으면서, 채널이 다른경우이므로 죽인다.
				scratchUtil::getInstance()->stopProcess((char*)processId.c_str());
			}
			ciDEBUG(1,("StartProcess(%s)", processId.c_str()));
			return scratchUtil::getInstance()->startProcess((char*)processId.c_str());
		}		
	}
	*/

	case DIRECTIVE_FV_CH_STOP:
		{
			const char* display = _getData("Display");
			scratchUtil::getInstance()->killBrowser(atoi(display), 3000, ciTrue);
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_QUERY_CURRENT_CH:
		{
			// 새로운 방송이 생겼을 수도 있는데, 이를 반영하기 위해서...2009.08.06
			if( !xmlRefresh() )
			{
				ciERROR(("refresh failed"));
				return ciFalse;
			}

			int display = 1;
			const char* strBuf = _getData("Display");
			if( strBuf == NULL || strlen(strBuf) == 0 )
			{
				ciWARN(("Display is null"));
			}
			else
			{
				display = atoi(strBuf);
			}

			return installUtil::getInstance()->getChannelName(res, display);
			// 만약 xml 의 ch 에 등록되어 있지 않는 놈이 Current Ch 이라면, 등록을 해주어야 한다는
			// 문제가 있다.
		}
		break;

	case DIRECTIVE_FV_AUTO_OFF:
		{
			ciString startUp = "false";
			int display = 1;
			const char* strBuf = _getData("Display");
			if( strBuf == NULL || strlen(strBuf) == 0 )
			{
				ciWARN(("Display is null"));
			}
			else
			{
				display = atoi(strBuf);
			}

			ciString autoStartUpName = "STARTER.APP_BROWSER.AUTO_STARTUP";
			if( display == 2 )
			{
				autoStartUpName = "STARTER.APP_BROWSER1.AUTO_STARTUP";
			}
			ciDEBUG(1, ("set Auto off display=(%d)", display) );
			if( !installUtil::getInstance()->setProperty(autoStartUpName.c_str(), startUp.c_str(), ciTrue) )
			{
				ciERROR(("setChannelProperty Error"));
				return ciFalse;
			}
			return ciTrue;		
		}
		break;

	case DIRECTIVE_FV_AUTO_ON:
		{
			ciString startUp = "true";
			int display = 1;
			const char* strBuf = _getData("Display");
			if( strBuf == NULL || strlen(strBuf) == 0 )
			{
				ciWARN(("Display is null"));
			}
			else
			{
				display = atoi(strBuf);
			}

			ciString argName		 = "STARTER.APP_BROWSER.ARGUMENT";
			ciString autoStartUpName = "STARTER.APP_BROWSER.AUTO_STARTUP";
			ciString processId		 = "BROWSER";
			if( display == 2 )
			{
				argName = "STARTER.APP_BROWSER1.ARGUMENT";
				autoStartUpName = "STARTER.APP_BROWSER1.AUTO_STARTUP";
				processId = "BROWSER1";
			}

			const char* host = _getData("ChannelName");
			if( host == NULL || strlen(host) == 0 )
			{
				ciERROR(("ChannelName is null"));
				return ciFalse;
			}
			const char* site = _getData("SiteName");
			if( site == NULL || strlen(site) == 0 )
			{
				ciERROR(("SiteName is null"));
				site="USER";
				//return ciFalse;
			}
			const char* networkUse = _getData("NetworkUse");
			if( networkUse == NULL || strlen(networkUse) == 0 )
			{
				ciERROR(("networkUse is null"));
				//return ciFalse;
			}
			ciBoolean isNetwork = (atoi(networkUse) ? ciTrue : ciFalse);
			ciDEBUG(1, ("set Auto On newHost=(%s), newSite=(%s), newNetworkUse=(%s)", host, site, networkUse) );
			if( !installUtil::getInstance()->setChannelName(argName.c_str(), autoStartUpName.c_str(), 
															host, site, startUp.c_str(), isNetwork, ciTrue) )
			{
				ciERROR(("setChannelProperty Error"));
				return ciFalse;
			}
			// 자동으로 결국 play 도 된다는 점에 주의할 것.
			return ciTrue;
		}
		break;

	//
	// Channel Modify
	// 
	case DIRECTIVE_FV_CH_MODIFY:
		{
			const char* ch			= _getData("Ch");
			const char* channelName	= _getData("ChannelName");
			const char* networkUse	= _getData("NetworkUse");

			if( ch == NULL || strlen(ch) == 0 )
			{
				ciERROR(("Invalid Ch"));
				return ciFalse;
			}

			if( !_readXml() )
			{
				ciERROR(("read XML failed"));
				return ciFalse;
			}

			if( !_channelModify(ch, channelName, networkUse) )
			{
				ciERROR(("_channelModify failed"));
				return ciFalse;
			}

			res += _xmlBuf;
			ciDEBUG(1, ("Channel Modify succeed") );
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_SCHEDULE_MODIFY:
		{
			const char* channelName	= _getData("ChannelName");
			const char* networkUse	= _getData("NetworkUse");
			const char* site		= _getData("SiteName");
			const char* desc		= _getData("Desc");

			if( channelName == NULL || strlen(channelName) == 0 )
			{
				ciERROR(("Invalid Ch"));
				return ciFalse;
			}

			if( !_readXml() )
			{
				ciERROR(("read XML failed"));
				return ciFalse;
			}

			if( !_scheduleModify(channelName, site,desc,networkUse) )
			{
				ciERROR(("_channelModify failed"));
				return ciFalse;
			}

			res += _xmlBuf;
			ciDEBUG(1, ("Channel Modify succeed") );
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_IMPORT:
		{
			HWND hwind = ::FindWindow(NULL, "UBC Importer");
			if( hwind == NULL )
			{
				ciString command = "UBCImporter.exe";
				ciString arg=" +drive all";
				//if(system(command.c_str())==0){
				if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(), 0, ciFalse) )
				{
					ciDEBUG(1,("%s %s succeed", command.c_str(), arg.c_str()));
					return ciTrue;
				}
				ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
				return ciFalse;
			}
			ciWARN(("UBC Importer Window already exist"));
			::ShowWindow(hwind, SW_SHOWNORMAL);
			return scratchUtil::getInstance()->getFocus(hwind);
		}
		break;

	case DIRECTIVE_FV_EXPORT:
		{
			HWND hwind = ::FindWindow(NULL, "UBC Importer");
			if( hwind == NULL )
			{
				ciString command = "UBCImporter.exe";
				ciString arg=" +drive all +export";
				//if(system(command.c_str())==0){
				if( scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(), 0, ciFalse) )
				{
					ciDEBUG(1, ("%s %s succeed", command.c_str(), arg.c_str()) );
					return ciTrue;
				}
				ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
				return ciFalse;
			}
			ciWARN(("UBC Importer Window already exist"));
			::ShowWindow(hwind, SW_SHOWNORMAL);
			return scratchUtil::getInstance()->getFocus(hwind);
		}
		break;

	case DIRECTIVE_FV_REFRESH:
		{
			// build new xml...
			if( !xmlRefresh() )
			{
				ciERROR(("refresh failed"));
				return ciFalse;
			}

			res += m_channelXML->GetXML();
			ciDEBUG(1, ("FV_REFRESH succeed") );
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_CH_DELETE:
		{
			const char* siteName	= _getData("SiteName");
			const char* channelName	= _getData("ChannelName");
			const char* desc		= _getData("Desc");
			const char* networkUse	= _getData("NetworkUse");
			if( channelName == NULL || strlen(channelName) == 0 )
			{
				ciERROR(("Invalid ChannelName"));
				return ciFalse;
			}

			if( !_readXml() )
			{
				ciERROR(("read XML failed"));
				return ciFalse;
			}

			if( !_channelDelete(channelName, siteName, desc, networkUse) )
			{
				ciERROR(("channel Add failed"));
				return ciTrue;
			}
			res += _xmlBuf;

			CString filename = EXE_PATH;
			filename += "config\\";
			filename += channelName;
			filename += ".ini";

			CString backupFile = EXE_PATH;
			backupFile += "config\\tao\\";  //tao directory 를 backup directory 로 쓴다.
			backupFile += channelName;
			backupFile += ".ini";

			if( !::MoveFile(filename, backupFile) )
			{
				ciERROR(("%s --> %s file move failed", filename, backupFile));
			}
			//if( !::DeleteFile(filename) )
			//{
			//	ciERROR(("%s file delete failed", filename));
			//}
			/*
			ciString command = "del .\\config\\";
			command += channelName;
			command += ".ini";

			if( !scratchUtil::getInstance()->createProcess(command.c_str(), "", 0, ciFalse) )
			{
				ciWARN(("%s ini file delete failed", channelName);
			}
			*/
			ciDEBUG(1, ("FV_CH_DELETE (%s) succeed", filename) );
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_DELETE_ALL:
		{
			CString path = EXE_PATH;
			path += "config\\tao\\*.ini";  //tao directory 를 backup directory 로 쓴다.
			ciStringList fileList;
			installUtil::getInstance()->getFileList(path, fileList);
			int count = 0;
			ciStringList::iterator itr;
			for(itr=fileList.begin(); itr!=fileList.end(); itr++)
			{
				ciString filename = EXE_PATH;
				filename += "config\\tao\\";
				filename += itr->c_str();
				if( !::DeleteFile(filename.c_str()) )
				{
					ciERROR(("delete %s failed", filename.c_str()));
				}
				else
				{
					ciDEBUG(1, ("delete %s succeed", filename.c_str()) );
					count++;
				}
			}
			ciDEBUG(1, ("FV_DELETE_ALL (%s,%d) succeed", path, count) );
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_QUERY_RECOVERY:
		{
			CString path = EXE_PATH;
			path += "config\\tao\\*.ini";  //tao directory 를 backup directory 로 쓴다.
			ciStringList fileList;
			installUtil::getInstance()->getFileList(path, fileList);
			res = "INIFiles=";
			ciStringList::iterator itr;
			int i=0;
			for(itr=fileList.begin(); itr!=fileList.end(); itr++,i++)
			{
				if(i) res += ";";
				ciString name = (*itr);
				//ciStringUtil::divide((*itr), '.', &name, &ext);
				ciStringUtil::cutPostfix(name, ".");
				res += name;
			}
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_CH_RECOVERY:
		{
			const char* fileList = _getData("INIFiles");
			if( fileList == 0 )
			{
				ciERROR(("INIFiles arg is null"));
				return ciFalse;
			}
			ciDEBUG(1, ("fileList=%s",fileList) );
			int counter = 0;
			ciStringTokenizer aTokens(fileList, ";");
			while( aTokens.hasMoreTokens() )
			{
				ciString token = aTokens.nextToken();
				ciDEBUG(1, ("token=%s", token.c_str()) );
				CString filename = EXE_PATH;
				filename += "config\\";
				filename += token.c_str();
				filename += ".ini";

				CString backupFile = EXE_PATH;
				backupFile += "config\\tao\\";  //tao directory 를 backup directory 로 쓴다.
				backupFile += token.c_str();
				backupFile += ".ini";

				if( !::MoveFile(backupFile, filename) )
				{
					ciERROR(("%s --> %s file move failed", backupFile, filename));
				}
				else
				{
					ciDEBUG(1, ("%s --> %s file move succeed", backupFile, filename) );
					counter++;
				}
			}
			ciDEBUG(1, ("%d file copied", counter) );
			if( counter )
			{
				if( !xmlRefresh() )
				{
					ciERROR(("refresh failed"));
					return ciFalse;
				}
				res += _xmlBuf;
				ciDEBUG(1, ("FV_REFRESH succeed") );
			}
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_CH_ADD:
		{
			const char* siteName	= _getData("SiteName");
			const char* channelName	= _getData("ChannelName");
			const char* desc		= _getData("Desc");
			const char* networkUse	= _getData("NetworkUse");

			if( channelName == NULL || strlen(channelName) == 0 )
			{
				ciERROR(("Invalid ChannelName"));
				return ciFalse;
			}

			if( !_readXml() )
			{
				ciERROR(("read XML failed"));
				return ciFalse;
			}

			if( !_channelAdd(channelName, siteName, desc, networkUse) )
			{
				ciERROR(("channel Add failed"));
				return ciTrue;
			}
			res += _xmlBuf;
			ciDEBUG(1, ("FV_CH_ADD succeed") );
			return ciTrue;
		}
		break;

	//
	// Setting
	//

	case DIRECTIVE_FV_DISPLAY_SET:
		{
			const char* displayOpt = _getData("OPT");
			if( displayOpt == NULL )
			{
				ciERROR(("OPT= not specified"));
				return ciFalse;
			}
			ciString arg = "+autoscale +aspectratio";
			if( strncmp(displayOpt,"FULL",4) == 0 )
			{
				arg = "+autoscale";
			}
			if( installUtil::getInstance()->setDisplayOption(arg.c_str(), ciTrue) )
			{
				ciDEBUG(1, ("setBrowserOption Succeed") );
				return ciTrue;
			}
			ciERROR(("setBrowserOption Error"));
			return ciFalse;
		}
		break;

	case DIRECTIVE_FV_TOOLS:
		{
			HWND hwind = ::FindWindow(NULL, "UBC Device Wizard");
			if( hwind == NULL )
			{
				ciString command = EXE_PATH;
				command += "UBCDeviceWizard.exe";
				ciString arg = " ";
				//if(system(command.c_str())==0){
				if( scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(), 0, ciFalse) )
				{
					ciDEBUG(1, ("%s %s succeed", command.c_str(), arg.c_str()) );
					return ciTrue;
				}
				ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
				return ciFalse;	
			}
			else
			{
				ciWARN(("UBC Importer Window already exist"));
				::ShowWindow(hwind, SW_SHOWNORMAL);
				return scratchUtil::getInstance()->getFocus(hwind);
			}
		}
		break;

	case DIRECTIVE_FV_UPDATE:
		{
			const char* autoStr = _getData("Auto");
			if( autoStr == NULL || strlen(autoStr) == 0 )
			{
				ciDEBUG(1, ("Auto not specified") );
			}
			else
			{
				ciBoolean autoVal = (atoi(autoStr) ? ciTrue : ciFalse);
				if( installUtil::getInstance()->setAutoUpdate(autoVal) )
				{
					ciDEBUG(1, ("Set setAutoUpdate Succeed") );
					return ciTrue;
				}
				else
				{
					ciERROR(("Set setAutoUpdate failed"));
					return ciFalse;
				}
			}
			//HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
			HWND sttHwnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
			if( sttHwnd == NULL )
			{
				ciWARN(("STT Window Handle is not found"));
				ciString command = "SelfAutoUpdate.exe";
				ciString arg=" +log +ignore_env +update_only +firmwareView";
				//if(system(command.c_str())==0){
				if( scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(), 0, ciFalse) )
				{
					ciDEBUG(1, ("%s %s succeed", command.c_str(), arg.c_str()) );
					return ciTrue;
				}
				ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
				return false;
			}
			//::SendMessage(sttHwnd, WM_SHOWWINDOW, 5, 0);
			//::ShowWindow(sttHwnd, SW_SHOWNORMAL);
			//scratchUtil::getInstance()->getFocus(sttHwnd);

			COPYDATASTRUCT appInfo;
			appInfo.dwData = 600;  // AutoUpdate
			appInfo.lpData = (char*)"";
			appInfo.cbData = 1;
			::SendMessage(sttHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
			ciDEBUG(1, ("FV_UPDATE succeed") );
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_REGISTER:
		{
			HWND hwind = ::FindWindow(NULL, "Additional Install Option");
			if( hwind == NULL )
			{
				/*
				ciString host,mac,edition;
				if(!scratchUtil::getInstance()->readAuthFile(host,mac,edition)){
					ciWARN(("readAuth failed"));
				}
				*/
				ciBoolean isGlobal = ubcUtil::getInstance()->isGlobalUI();

				ciString command = "UBCExtraInstall.exe";
				ciString arg= " +authOnly ";
				if( _installedVersion > 0 )
					command = "UBCHostAuthorizer.exe";
				else
					arg += " +eng ";

				if( !_edition.empty() )
				{
					arg+= "+";
					arg+= _edition;
					arg+= " ";
				}

				if(isGlobal)
					arg+= " +global ";

				//arg+=corbaProp;
				//if(system(command.c_str())==0){
				if( scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(), 0, ciFalse) )
				{
					ciDEBUG(1, ("%s %s succeed", command.c_str(), arg.c_str()) );
					return ciTrue;
				}
				ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
				return ciFalse;	
			}
			else
			{
				ciWARN(("Additional Install Option Window already exist"));
				::ShowWindow(hwind, SW_SHOWNORMAL);
				return scratchUtil::getInstance()->getFocus(hwind);
			}
		}
		break;

	case DIRECTIVE_FV_SHUTDOWNTIME:
		{
			const char* timeStr = _getData("ShutdownTime");
			if( timeStr == NULL )
			{
				ciERROR(("ShutdownTime not specified"));
				return ciFalse;
			}
			ciString buf = timeStr;
			if( installUtil::getInstance()->setShutdownTime(buf, ciTrue) )
			{
				theApp.shutdownTimer(timeStr);
				ciDEBUG(1, ("Set AutoShutdownTime Succeed") );
				return ciTrue;
			}

			ciERROR(("Set AutoShutdownTime failed"));
			return ciFalse;
		}
		break;

	case DIRECTIVE_FV_WEEK_SHUTDOWNTIME:
		{
			const char* timeStr = _getData("WeekShutdownTime");
			if( timeStr == NULL )
			{
				ciERROR(("WeekShutdownTime not specified"));
				return ciFalse;
			}
			ciString buf = timeStr;
			if( installUtil::getInstance()->setWeekShutdownTime(buf, ciTrue) )
			{
				theApp.week_shutdownTimer(timeStr);
				ciDEBUG(1, ("Set WeekShutdownTime Succeed") );
				return ciTrue;
			}

			ciERROR(("Set WeekShutdownTime failed"));
			return ciFalse;
		}
		break;

	case DIRECTIVE_FV_MOUSE:
		{
			const char* showStr = _getData("Show");
			if( showStr == NULL )
			{
				ciERROR(("Show not specified"));
				return ciFalse;
			}
			ciBoolean show = (atoi(showStr) ? ciTrue : ciFalse);
			if( installUtil::getInstance()->setMouseOption(show, ciTrue) )
			{
				ciDEBUG(1, ("Set setMouseOption Succeed") );
				return ciTrue;
			}
			ciERROR(("Set setMouseOption failed"));
			return ciTrue;
		}
		break;

	case DIRECTIVE_FV_STARTER:
		{
			//HWND sttHwnd = ::FindWindow(NULL, "UTV Starter");
			HWND sttHwnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
			if( sttHwnd == NULL )
			{
				ciWARN(("STT Window Handle is not found"));
				ciString command = "SelfAutoUpdate.exe ";
				ciString arg = " +log +ignore_env +shownormal";
				//if(system(command.c_str())==0){
				if( scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(), 0, ciTrue) )
				{
					ciDEBUG(1, ("%s %s succeed", command.c_str(), arg.c_str()) );
					return ciTrue;
				}
				ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
				return false;
			}
			else
			{
				ciWARN(("UTV Starter Window already exist"));
				::ShowWindow(sttHwnd, SW_SHOWNORMAL);
				return scratchUtil::getInstance()->getFocus(sttHwnd);
			}
			//::SendMessage(sttHwnd, WM_SHOWWINDOW, 5, 0);
			//::ShowWindow(sttHwnd, SW_SHOW);
			/*
			COPYDATASTRUCT appInfo;
			appInfo.dwData = 900;  // ShowWindow
			appInfo.lpData = (char*)"";
			appInfo.cbData = 1;
			::SendMessage(sttHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
			ciDEBUG(1,("FV_STARTER succeed"));
			return ciTrue;
			*/
		}
		break;

	// 외부 APPLICATION 으로 부터 받은 명령일때
	case DIRECTIVE_FV_APPLICATION:
		{
			const char* state = _getData("STATE");
			if( state == NULL || strlen(state) == 0 )
			{
				ciWARN(("Invalid FV_APPLICATION DATA"));
				return ciFalse;
			}
			HWND brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2.exe");
			if( brwHwnd == NULL )
			{
				brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2_UBC1.exe");
			}
			if( brwHwnd == NULL )
			{
				ciWARN(("No Brw now"));
				return ciFalse;
			}
			ciString stateStr = state;
			if( stateStr == "APP_ACTIVE" )
			{
				// Application 이 Active 되었으므로, UBC 는 MINIMIZE 된다.
				COPYDATASTRUCT appInfo;
				appInfo.dwData = 800;  // Minimize
				appInfo.lpData = (char*)"";
				appInfo.cbData = 1;
				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
				return ciTrue;
			}
			if( stateStr == "APP_IDLE" )
			{
				// Application 이 IDLE 되었으므로, UBC 는 MAXIMIZE 된다.
				COPYDATASTRUCT appInfo;
				appInfo.dwData = 801;  // Maximize
				appInfo.lpData = (char*)"";
				appInfo.cbData = 1;
				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
				return ciTrue;
			}
		}
		break;
	}

	ciERROR(("%s is unknown command", directive));
	return ciFalse;
}

ciBoolean tcpSocketSession::import(string& scheduleFile)
{
	if( _edition != ENTERPRISE_EDITION )
		scheduleFile += " +local";

	ciDEBUG(1, ("import %s", scheduleFile.c_str()) );

	// starter option 을 수정하고,
	// 브라우저에 re-init 명령을 날린다.

	int monitorCount = scratchUtil::getInstance()->getMonitorCount();
	if( monitorCount == 1 )
	{
		// 만약, 모니터 갯수가 1개라면,  brwClient2_UBC2.exe 는 떠있어서는 안되며
		// Auto 도 해제되어 있어야 한다.
		installUtil::getInstance()->stopBrowser2(2);
	}

	string strExecute = EXE_PATH;
	strExecute += "UBCImporter.exe +drive X: ";
	strExecute += scheduleFile;  //+ini foo +display 0 +network

	ciDEBUG(1, ("WinExec %s", strExecute.c_str()) );
	int nRet = WinExec(strExecute.c_str(), SW_NORMAL);
	if( nRet < 31 )
	{
		ciERROR(("UBCImporter run fail"));
		return ciFalse;
	}

	ciDEBUG(1, ("import %s complete", scheduleFile.c_str()) );
	return ciTrue;
}

ciBoolean tcpSocketSession::import2(string& arg)
{
	// starter option 을 수정하고,
	// 브라우저에 re-init 명령을 날린다.
	// arg  = +ini foo +display 0 +nomsg +reservationStart/+reservationEnd  onTime

	ciDEBUG(1, ("import2(%s)", arg.c_str()) );

	ciBoolean get_ini		= ciFalse;
	ciBoolean get_display	= ciFalse;
	ciBoolean isNetwork		= ciFalse;
	ciBoolean stop			= ciFalse;
	ciString host, display;
	ciString onTimeStr;

	ciStringTokenizer aTokens(arg);
	while( aTokens.hasMoreTokens() )
	{
		ciString aToken = aTokens.nextToken();

		if( isNetwork || stop )
		{
			if( onTimeStr.empty() )
			{
				onTimeStr = aToken;
				continue;
			}
		}

		if( aToken == "+reservationStart" )
		{
			isNetwork = ciTrue;
			continue;
		}
		if( aToken == "+reservationEnd" )
		{
			stop = ciTrue;
			continue;
		}
		if( aToken == "+ini" )
		{
			get_ini = ciTrue;
			continue;
		}
		if( get_ini )
		{
			host = aToken;
			get_ini = ciFalse;
		}
		if( aToken == "+display" )
		{
			get_display = ciTrue;
			continue;
		}
		if( get_display )
		{
			display = aToken;
			get_display = ciFalse;
		}
	}

	if( host.empty() || display.empty() )
	{
		ciERROR(("host or display value is null"));
		return ciFalse;
	}

	const char* disPtr = display.c_str();
	int counter = 0;

	ciTime now;
	time_t onTime = now.getTime();
	if( !onTimeStr.empty() )
	{
		onTime = atol(onTimeStr.c_str());
		// 만약 현재시간과  onTime  이 30초 이내에 있으면 바로 처리한다.
		if( now > onTime-30 )
		{
			onTimeStr = "";
		}
	}

	while( *disPtr )
	{
		int displayNo = 1;
		ciString argName	 = "STARTER.APP_BROWSER.ARGUMENT";
		ciString startUpName = "STARTER.APP_BROWSER.AUTO_STARTUP";
		ciString browser	 = "UTV_brwClient2.exe";
		if( *disPtr == '1' )
		{
			displayNo = 2;
			argName		= "STARTER.APP_BROWSER1.ARGUMENT";
			startUpName	= "STARTER.APP_BROWSER1.AUTO_STARTUP";
			browser		= "UTV_brwClient2_UBC1.exe";
		}
		if( stop )
		{
			// 만약, 지금 돌고 있는 브라우저가 stop 요청이 들어온 브라우저라면
			// AUTO 를 해제하고, 죽인다.
			if( now > onTime-30 )
			{
				installUtil::getInstance()->stopBrowser(displayNo, host.c_str());
			}
			else
			{
				// 그런데, toTime 이 되기 전까지는 죽이면 안된다. (onTimeStr임)
				//  이걸 Q 에 push 하고 thread 가 돌면서
				// 시간이 될때까지 기다려야함.

				stopThread::getInstance()->enq(host.c_str(), displayNo, onTime);
			}
		}
		else
		{
			ciString programSiteId, other;
			ciStringUtil::divide(host, '_', &programSiteId, &other);
			if( programSiteId.empty() )
				programSiteId="user";

			// 이부분은 브라우저가 완전히 스스로를 나타낸 다음에 해야하므로 브라우저에 넘겨야함.
			ciBoolean hasSet = installUtil::getInstance()->setChannelName(
									displayNo-1, host.c_str(), programSiteId.c_str() );

			if( !hasSet )
			{
				ciERROR(("set %s failed", argName.c_str()));
				disPtr++;
				continue;
			}

			//skpark 2010.4.13 firmwareView 화면을 refresh 시킨다.
			char refreshInfo[1024] = {0};
			sprintf(refreshInfo, "\t\t<ad name=\"%s\" desc=\"\" sitename=\"%s\" networkuse=\"%d\" />\n"
				, host.c_str(), programSiteId.c_str(), isNetwork );
			scratchUtil::getInstance()->socketAgent("127.0.0.1", 14007, "refresh", refreshInfo);

			/////////////////////////////////////////////////////////////////////////////

			// 브라우저에 +report 옵션을 추가해서 play시킨다.
			ciString replaceOption = "";
			if( _edition != ENTERPRISE_EDITION )
			{
				replaceOption += " +local";
			}
			replaceOption += " +replace " + onTimeStr;

			// 최소화옵션 설정시 UTV_brwClient2.exe 띄우지 않도록 설정
			if( !ciArgParser::getInstance()->isSet("+minimize") )
			{
				if( !installUtil::getInstance()->runBrowser(displayNo, replaceOption.c_str()) )
				{
					ciERROR(("%s fail", browser.c_str()));
					/*
					ciBoolean hasSet = 	installUtil::getInstance()->setChannelName(
										displayNo-1,host.c_str(),programSiteId.c_str());
					*/
					disPtr++;
					continue;
				}
			}
		}
		ciDEBUG(1, ("%s succeed", browser.c_str()) );
		counter++;
		disPtr++;
	}
	// xml 화일에 해당 스케쥴이 없다면 추가해야 한다.  이를위해 xml 을 refresh 해준다.
	if( counter )
	{
		ciDEBUG(1, ("%d import succeed",counter) );
		xmlRefresh();
		return ciTrue;
	}

	ciERROR(("import failed"));
	return ciFalse;

	/*
	// 브라우저에 re-init 명령을 날린다.

	HWND brwHwnd = scratchUtil::getInstance()->getWHandle(browser.c_str());
	if(brwHwnd){
		COPYDATASTRUCT appInfo;
		appInfo.dwData = 0;  // re-init  번호 다시 설정해야!!!!!
		appInfo.lpData = (char*)""; 
		appInfo.cbData = 1;
		::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
	}
	*/
}

ciBoolean tcpSocketSession::keyboardSimulator(const char* command, const char* data)
{
	ciDEBUG(1, ("keyboardSimulator(%s,%s)", command, data) );
	char buf[BUF_LEN+1] = {0};

	list<string>  aList,  bList;

	int j = 0;
	int len = strlen(command);
	for(int i=0; i<len; i++)
	{
		if( command[i] == '+' )
		{
			buf[j] = 0;
			if( strlen(buf) )
			{
				aList.push_back(buf);
			}
			memset(buf, 0x00, BUF_LEN+1);
			j=0;
			continue;
		}
		else
		{
			buf[j] = command[i];
			j++;
		}
	}
	aList.push_back(buf);

	scratchUtil::getInstance()->getFocus("UTV_BRW");

	list<string>::iterator itr;
	for(itr=aList.begin(); itr!=aList.end(); itr++)
	{
		//_kbdEvent((*itr).c_str(),0);				//skpark 2010 3.24 임시로 막음
		bList.push_front((*itr));
	}
	//Sleep(1000);
	list<string>::iterator jtr;
	for(jtr=bList.begin(); jtr!=bList.end(); jtr++)
	{
		//_kbdEvent((*jtr).c_str(),KEYEVENTF_KEYUP);  //skpark 2010 3.24 임시로 막음
	}

	HWND brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2.exe");
	if( brwHwnd == NULL )
	{
		brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2_UBC1.exe");
	}
	if( brwHwnd )
	{
		COPYDATASTRUCT appInfo;
		appInfo.dwData = UBC_WM_KEYBOARD_EVENT;  // 1000
		appInfo.lpData = (char*)command;
		appInfo.cbData = len+1;
		//::PostMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
		::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
	}

	return ciTrue;
}

int tcpSocketSession::_kbdEvent(const char* value, DWORD flag)
{
	//mylog(out,"%s", value);

	int i = 0;
	for(i=1; i<CODE_LEN; i++)
	{
		const char* str = scanString[i];
		if( strcmp(value,str) == 0 )
		{
			unsigned char vCode = virtualCode[i];
			if( vCode )
			{
				keybd_event(vCode, 0, flag, 0);
				if( flag == 0 )
				{
					ciDEBUG(1, ("%s key pushed", value) );
				}
				else
				{
					ciDEBUG(1, ("%s key released", value) );
				}
			}
			else
			{
				ciDEBUG(1, ("%s is invalid key", value) );
			}
			break;
		}
	}
	return i;
}

string tcpSocketSession::_curTimeStr()
{
    time_t current;
    time(&current);

    char* str;
    struct tm aTM;
#ifdef _WIN32
    struct tm *temp = localtime(&current);
    if( temp )
        aTM = *temp;
    else
        memset((void*)&aTM, 0x00, sizeof(aTM));
#else
    localtime_r(&current, &aTM);
#endif

    str = (char*)malloc(sizeof(char)*20);
    sprintf(str, "%4d-%02d-%02d %02d:%02d:%02d",
					aTM.tm_year+1900,
					aTM.tm_mon+1,
					aTM.tm_mday,
					aTM.tm_hour,
					aTM.tm_min,
					aTM.tm_sec
				);
    string strTime = str;
    free(str);
    return strTime;
}

ciBoolean tcpSocketSession::_setData(ciString& data)
{
	_dataMap.clear();

	if(data.empty())
		return ciFalse;

	ciDEBUG(1, ("_setData(%s)", data.c_str()) );

	ciStringTokenizer aTokens(data.c_str(), ",");

	while( aTokens.hasMoreTokens() )
	{
		ciString tok =aTokens.nextToken();
		ciString name, value;
		ciStringUtil::divide(tok, '=', &name, &value);
		do
		{	// value 가 "," 를 포함한 경우 괄호를 쳐서 들어오므로 이를 처리한다.
			if( value[0] != '(' )
				break;
			if(value[value.size()-1] == ')')
				break;
			if(!aTokens.hasMoreTokens())
				break;

			value += ",";
			value += aTokens.nextToken();
		} while(1);

		_dataMap.insert(ciStringMap::value_type(name, value));
	}
	return ciTrue;
}

const char* tcpSocketSession::_getData(const char* name)
{
	ciStringMap::iterator itr = _dataMap.find(name);

	if( itr != _dataMap.end() )
	{
		return itr->second.c_str();
	}
	return "";
}

ciBoolean tcpSocketSession::_readXml()
{
	if( m_channelXML )
		return m_channelXML->ReadXML();
	return ciFalse;

/*
	//
	ciString file = EXE_PATH;
	file += "flash\\xml\\channel.xml";
	ciString backupFile = EXE_PATH;
	backupFile += "flash\\xml\\channel.bak";

	for(int i=0; i<2; i++)
	{
		ifstream in(file.c_str());
		if( in.fail() )
		{
			ciERROR(("%s file open error", file.c_str()));
			if( i == 0 )
			{
				ciDEBUG(1, ("backup file used") );
				::MoveFile(backupFile.c_str(), file.c_str());
				continue;
			}
			return ciFalse;
		}

		ciDEBUG(1, ("readXml(%s)", file.c_str()) );

		ciBoolean newLine = ciTrue;
		_xmlBuf = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		ciBoolean eof = ciFalse;
		ciBoolean bof = ciFalse;
		int lineCount = 0;
		while( !in.eof() )
		{  
			char buf[1024] = {0};
			in.getline(buf, sizeof(buf)-1);
			lineCount++;
			if( strncmp(buf, "<channel>", strlen("<channel>")) == 0 )
			{
				 bof = ciTrue;
			}
			if( !bof )
			{
				// 아직 <channel> 을 만나자 못했다
				// channel 은 통상 2번째 줄에 있어야 한다.
				// 계속해서 <channel> 을 만나지 못한다면 화일이 깨진것이다
				if( lineCount > 10 )
				{
					ciERROR(("Something Wrong in channel.xml file"));
					break;
				}
				continue;
			}
			if( !newLine )
			{
				ciStringUtil::leftTrim(buf);
			}
			if( _isEOLXml(buf) )
			{
				_xmlBuf += buf;
				_xmlBuf += "\n";
				newLine = ciTrue;
			}
			else
			{
				_xmlBuf += buf;
				_xmlBuf += " ";
				newLine = ciFalse;
			}
			if( strncmp(buf, "</channel>", strlen("</channel>")) == 0 )
			{
				ciDEBUG(1, ("End of File") );
				eof = ciTrue;
				break;
			}
		}
		in.close();
		if( _xmlBuf.size() < 100 || !eof )
		{	//100자 미만이라면 깨진것으로 본다.
			ciERROR(("%s file broken", file.c_str()));
			if( i == 0 )
			{
				ciDEBUG(1, ("backup file used"));
				ciString crashFile = file + "_crash";
				::MoveFile(file.c_str(), crashFile.c_str());
				::CopyFile(backupFile.c_str(), file.c_str(), FALSE);
				_xmlBuf = "";
				continue;
			}
			return ciFalse;
		}
	}

	if( !_xmlBuf.empty() )
	{
		toAnsi(_xmlBuf);
	}
	//ciDEBUG(1,("after convert xml : \n%s", 
	//	_xmlBuf.substr(0,(_xmlBuf.size()>3000?3000:_xmlBuf.size())).c_str()));
		//ciDEBUG(1,("read xml file %s : \n%s", 
	//	file.c_str(),_xmlBuf.substr(0,(_xmlBuf.size()>3000?3000:_xmlBuf.size())).c_str()));
	return ciTrue;
*/
}

ciBoolean tcpSocketSession::_writeXml()
{
	if( m_channelXML )
		return m_channelXML->WriteXML();

	return ciFalse;
/*
	ciString backupFile = EXE_PATH;
	backupFile += "flash\\xml\\channel.bak";

	ciString xmlFile = EXE_PATH;
	xmlFile += "flash\\xml\\channel.xml";

	ciString tmpFile = EXE_PATH;
	tmpFile += "flash\\xml\\channel.tmp";

	ciDEBUG(1, ("writeXml(%s)", tmpFile.c_str()) );
	ciStringUtil::rightTrim(_xmlBuf);

	ofstream out(tmpFile.c_str());
	if( out.fail() )
	{
		ciERROR(("%s tmpFile open error", tmpFile.c_str()));
		return ciFalse;
	}

	if( !_xmlBuf.empty() )
	{
		toUTF8(_xmlBuf);
	}

	out.write(_xmlBuf.c_str(), _xmlBuf.size());
	
	if( out.fail() )
	{
		ciERROR(("%s tmpFile write error", tmpFile.c_str()));
		out.close();
		return ciFalse;
	}
	out.close();
	//ciDEBUG(1,("write xml tmpFile %s : \n%s", tmpFile.c_str(), 
	//	_xmlBuf.substr(0,(_xmlBuf.size()>3000?3000:_xmlBuf.size())).c_str()));

	::DeleteFile(backupFile.c_str());
	::MoveFile(xmlFile.c_str(), backupFile.c_str());
	::MoveFile(tmpFile.c_str(), xmlFile.c_str());

	return ciTrue;
*/
}

void tcpSocketSession::toUTF8(ciString& xmlBuf)
{
	ciDEBUG(1, ("ToUTF8") );
	char* src = (char*)xmlBuf.c_str();
	size_t srcLen = xmlBuf.length();
	size_t desLen = srcLen*2;
	char* des = new char[desLen + 1];

	ciDEBUG(1, ("before AnsiToUTF8! %d", srcLen) );
	int len = scratchUtil::getInstance()->AnsiToUTF8(src, des, desLen);
	if( len )
	{
		ciDEBUG(1, ("AnsiToUTF8 succeed! %d", len) );
		xmlBuf = des;
	}
	else
	{
		ciDEBUG(1, ("AnsiToUTF8 error! %d", len) );
	}
	delete[]des;
}

void tcpSocketSession::toAnsi(ciString& xmlBuf)
{
	ciDEBUG(1, ("ToAnsi") );

	char* src = (char*)xmlBuf.c_str();
	size_t srcLen = xmlBuf.length();
	size_t desLen = srcLen*2;
	char* des = new char[desLen + 1];

	ciDEBUG(1, ("before UTF8ToAnsi! %d", srcLen) );
	int len = scratchUtil::getInstance()->UTF8ToAnsi(src, des, desLen);
	if( len )
	{
		ciDEBUG(1, ("UTF8ToAnsi succeed! %d", len) );
		xmlBuf = des;
	}
	else
	{
		ciDEBUG(1, ("UTF8ToAnsi error! %d", len) );
	}
	delete[]des;
}

ciBoolean tcpSocketSession::_isEOLXml(const char* xmlLine)
{
	ciString buf = xmlLine;
	ciStringUtil::rightTrim(buf);
	int len = buf.size();
	if( len < 1 )
	{
		//ciDEBUG(1,("Not End of XML Line"));
		return ciFalse;
	}
	if( buf[len-1] == '>' )
	{
		//ciDEBUG(1,("End of XML Line"));
		return ciTrue;
	}
	//ciDEBUG(1,("Not End of XML Line"));
	return ciFalse;
}

ciBoolean tcpSocketSession::_channelModify(const char* ch, const char* channelName, const char* networkUse)
{
	if( m_channelXML )
		return m_channelXML->ChannelModify(ch, channelName, networkUse);

	return ciFalse;
/*
	ciDEBUG(1, ("_channelModify(%s,%s,%s)", ch, channelName, networkUse) );

	ciString keyword = "name=\"";
	keyword += ch;
	keyword += "\"";

	ciString verify = "set_ad=\"";

	ciString target = "set_ad=\"";
	target += channelName;
	target += "\"";

	ciDEBUG(1, ("key=%s, target=%s", keyword.c_str(), target.c_str()) );

	ciBoolean matched = ciFalse;
	ciString buf = "";
	ciStringTokenizer aTokens(_xmlBuf.c_str(), "<>/ \t\n", false, true);
	while( aTokens.hasMoreTokens() )
	{
		ciString aTok = aTokens.nextToken();
		if( aTok == keyword )
		{
			ciDEBUG(1, ("%s key matched", keyword.c_str()) );
			matched = ciTrue;
			buf += aTok;
			continue;
		}
		if( matched && strncmp(aTok.c_str(), verify.c_str(), verify.size()) == 0 )
		{
			buf += target;
			matched = ciFalse;
			continue;
		}

		buf += aTok;	
	}

	_networkUseModify(buf, channelName, networkUse);

	_xmlBuf = buf;

	//ciDEBUG(1,("result=\n%s", _xmlBuf.c_str()));
	_writeXml();
	return ciTrue;
*/
}

ciBoolean tcpSocketSession::_scheduleModify(const char* channelName, const char* site, const char* desc, const char* networkUse)
{
	if( m_channelXML )
		return m_channelXML->ScheduleModify(channelName, site, desc, networkUse);
	return ciFalse;

/*
	ciDEBUG(1, ("_scheduleModify(%s,%s,%s,%s)", channelName,site,desc,networkUse) );

	ciString keyword = "*ad name=\"";
	keyword += channelName;
	keyword += "\"*";

	char newVal[512] = {0};
	sprintf(newVal, "desc=\"%s\" sitename=\"%s\" networkuse=\"%s\" />", desc, site, networkUse);

	ciString newBuf;
	ciStringTokenizer aTokens(_xmlBuf.c_str(), "\n");
	while( aTokens.hasMoreTokens() )
	{
		ciString aTok = aTokens.nextToken();
		//ciDEBUG(1,("%s\n", aTok.c_str()));
		if( ciStringUtil::wildcmp(keyword.c_str(), aTok.c_str()) )
		{
			ciDEBUG(1, ("_scheduleModify key=%s matched", keyword.c_str()) );
			int idx = aTok.find("desc=");
			if( idx > 0 )
			{
				aTok = aTok.substr(0, idx);
				aTok += newVal;
			}
		}
		newBuf += aTok;
		newBuf += "\n";
	}

	_xmlBuf = newBuf;
	//ciDEBUG(1,("result=\n%s", _xmlBuf.c_str()));
	_writeXml();
	return ciTrue;
*/
}

ciBoolean tcpSocketSession::_networkUseModify(ciString& buf,
									const char* channelName, const char* networkUse)
{
	ciString keyword = "*ad name=\"";
	keyword += channelName;
	keyword += "\"*";

	char newVal[20] = {0};
	sprintf(newVal, "networkuse=\"%s\" />", networkUse);

	ciDEBUG(1, ("_networkUseModify key=%s, value=%s", keyword.c_str(),newVal) );

	ciString newBuf;
	ciStringTokenizer aTokens(buf.c_str(), "\n");
	while( aTokens.hasMoreTokens() )
	{
		ciString aTok = aTokens.nextToken();
		//ciDEBUG(1, ("%s\n", aTok.c_str()) );
		if( ciStringUtil::wildcmp(keyword.c_str(), aTok.c_str()) )
		{
			ciDEBUG(1, ("_networkUseModify key=%s matched", keyword.c_str()) );
			int idx = aTok.find("networkuse=");
			if( idx > 0 )
			{
				aTok = aTok.substr(0, idx);
				aTok += newVal;
			}
		}
		newBuf += aTok;
		newBuf += "\n";
	}
	buf = newBuf;
	return ciTrue;
}

ciBoolean tcpSocketSession::_channelAdd(const char* channelName,
										const char* siteName,
										const char* desc,
										const char* networkUse)
{
	if( m_channelXML )
		return m_channelXML->ChannelAdd(channelName, siteName, desc, networkUse);

	return  ciFalse;

/*
	ciDEBUG(1, ("_channelAdd(%s,%s,%s,%s)", channelName, siteName, desc, networkUse) );

	//<ad name="ATC" desc="Demo for ATC" sitename="ERAE" networkuse="0" /> 

	char line[1024] = {0};
	sprintf(line, "\t\t<ad name=\"%s\" desc=\"%s\" sitename=\"%s\" networkuse=\"%d\" />\n",
			channelName, desc, siteName, atoi(networkUse) );

	ciBoolean matched = ciFalse;
	ciString buf = "";	
	ciStringTokenizer aTokens(_xmlBuf.c_str(), "\n");
	while( aTokens.hasMoreTokens() )
	{
		ciString aTok = aTokens.nextToken();
		if( !matched && ciStringUtil::wildcmp("*</ad_info>*", aTok.c_str()) )
		{
			buf += line;
			matched = ciTrue;
		}
		buf += aTok;
		buf += "\n";
	}

	if( !matched )
	{
		ciERROR(("</ad_info> not found"));
		return ciFalse;
	}
	_xmlBuf = buf;
	_writeXml();

	return ciTrue;
*/
}

ciBoolean tcpSocketSession::_channelDelete( const char* channelName,
											const char* siteName,
											const char* desc,
											const char* networkUse)
{
	if( m_channelXML )
		return m_channelXML->ChannelDelete(channelName, siteName, desc, networkUse);

	return ciFalse;

/*
	ciDEBUG(1, ("_channelDelete(%s,%s,%s,%s)", channelName, siteName, desc, networkUse) );

	//<ad name="ATC" desc="Demo for ATC" sitename="ERAE" networkuse="0" /> 

	char line[1024] = {0};
	sprintf(line, "*ad name=\"%s\"*", channelName);

	ciString buf;
	ciStringTokenizer aTokens(_xmlBuf.c_str(), "\n");
	while( aTokens.hasMoreTokens() )
	{
		ciString aTok = aTokens.nextToken();
		if( ciStringUtil::wildcmp(line, aTok.c_str()) )
			continue;

		buf += aTok;
		buf += "\n";
	}

	_xmlBuf = buf;
	_writeXml();

	return ciTrue;
*/
}

ciBoolean tcpSocketSession::xmlRefresh(const char* additionalLine)
{
	if( m_channelXML )
		return m_channelXML->RefreshXML(additionalLine);

	return ciFalse;

/*
	ciDEBUG(1, ("xmlRefresh(%s)", (additionalLine ? additionalLine : "NULL")) );

	if( !_readXml() )
	{
		ciERROR(("read XML failed"));
		return ciFalse;
	}

	// listup ini file
	
	ciString dir = EXE_PATH;
	dir += "\\config\\*.ini";

	ciStringList iniList;
	installUtil::getInstance()->getFileList(dir.c_str(), iniList);

	ciStringList xmlLineList;
	ciStringList xmlCompareLineList;
	_buildXMLLine(iniList, xmlLineList, xmlCompareLineList, additionalLine);

	// getHost info
	ciString ahost1, ahost2;
	installUtil::getInstance()->getHost(1, ahost1);
	installUtil::getInstance()->getHost(2, ahost2);

	ciBoolean host1_exist = ciFalse;
	ciBoolean host2_exist = ciFalse;

	ciString key1 = "";
	if( !ahost1.empty() )
	{
		key1 = "*" + ahost1 + "*";
	}
	else
	{
		host1_exist = ciTrue;
	}

	ciString key2 = "";
	if( !ahost2.empty() && ahost1 != ahost2 )
	{
		key2 = "*" + ahost2 + "*";
	}
	else
	{
		host2_exist = ciTrue;
	}

	ciBoolean start		= ciFalse;
	ciBoolean end		= ciFalse;
	ciBoolean ch_start	= ciFalse;
	ciBoolean ch_end	= ciFalse;
	ciString buf = "";	
	ciStringTokenizer aTokens(_xmlBuf.c_str(), "\n");
	ciBoolean skip = ciFalse;
	ciStringList chList;
	ciStringList chList1;
	ciStringList chList2;
	while( aTokens.hasMoreTokens() )
	{
		ciString aTok = aTokens.nextToken();

		// ch_info 에 없는 auto_start host 를 끼워넣기 위한 삽질
		if( !ch_end && !ch_start && ciStringUtil::wildcmp("*<ch_info*", aTok.c_str()) )
		{
			ch_start = ciTrue;
			buf += aTok;
			buf += "\n";
			continue;
		}
		if( !ch_end && ch_start && ciStringUtil::wildcmp("*</ch_info>*", aTok.c_str()) )
		{
			ch_start = ciFalse;
			ch_end = ciTrue;

			ciStringList* listPtr = &chList;

			if( host1_exist == ciFalse )
			{
				listPtr = &chList1;
				//ciBoolean matched=ciFalse;
				if( 1 )
				{	// 1st turn - None 이 있는 경우
					ciStringList::iterator itr;
					int i=1;
					for( itr=chList.begin(); itr!=chList.end(); itr++,i++ )
					{
						if( !host1_exist )
						{
							if( ciStringUtil::wildcmp("*set_ad=\"\"*", itr->c_str()) )
							{	// NONE case
								ciDEBUG(1, ("host1 %d matched - none case",i) );
								char newval[256] = {0};
								sprintf(newval, "\t\t<ch name=\"ch%d\" set_ad=\"%s\"/>", i, ahost1.c_str());
								listPtr->push_back(newval);
								host1_exist = ciTrue;
								continue;
							}
						}
						listPtr->push_back(*itr);
					}
				}// 1st turn

				if( !host1_exist ){  // 2nd tern - None 이 없는 경우 
					listPtr->clear();
					ciStringList::iterator itr;
					int i=1;
					for( itr=chList.begin(); itr!=chList.end(); itr++,i++ )
					{
						if( !host1_exist )
						{
							if( host2_exist == ciFalse || key2.empty() )
							{	// host2 가 없으면 첫번째에 넣는다.
								char newval[256] = {0};
								sprintf(newval, "\t\t<ch name=\"ch%d\" set_ad=\"%s\"/>", i, ahost1.c_str());
								ciDEBUG(1, ("host1 %d matched without host2",i) );
								listPtr->push_back(newval);
								host1_exist = ciTrue;
								continue;
							}
							else
							{	// host2 있으면 host2 값이 아닌 첫번째 또는 제일 마지막에 넣는다.(도배케이스)
								if( i==8 || ciStringUtil::wildcmp(key2.c_str(), itr->c_str()) == 0 )
								{	// NONE case
									ciDEBUG(1, ("host1 %d matched with host2",i) );
									char newval[256] = {0};
									sprintf(newval, "\t\t<ch name=\"ch%d\" set_ad=\"%s\"/>", i, ahost1.c_str());
									listPtr->push_back(newval);
									host1_exist = ciTrue;
									continue;
								}
							}
						}
						listPtr->push_back(*itr);
					}
				}
			}
			////////////////////////
			///////////////////////
			if( host2_exist == ciFalse )
			{
				ciStringList* listPtr1 = listPtr;
				listPtr = &chList2;
				//ciBoolean matched=ciFalse;
				if( 1 )
				{	// 1st turn - None 이 있는 경우
					ciStringList::iterator itr;
					int i=1;
					for(itr=listPtr1->begin(); itr!=listPtr1->end(); itr++,i++)
					{
						if( !host2_exist )
						{
							if( ciStringUtil::wildcmp("*set_ad=\"\"*", itr->c_str()) )
							{	// NONE case
								ciDEBUG(1, ("host2 %d matched - none case",i) );
								char newval[256] = {0};
								sprintf(newval, "\t\t<ch name=\"ch%d\" set_ad=\"%s\"/>", i, ahost2.c_str());
								listPtr->push_back(newval);
								host2_exist = ciTrue;
								continue;
							}
						}
						listPtr->push_back(*itr);
					}
				}// 1st turn

				if( !host2_exist )
				{	// 2nd tern - None 이 없는 경우 
					listPtr->clear();
					ciStringList::iterator itr;
					int i=1;
					for(itr=listPtr1->begin(); itr!=listPtr1->end(); itr++,i++)
					{
						if( !host2_exist )
						{
							if( host1_exist == ciFalse || key1.empty() )
							{	// host1 가 없으면 첫번째에 넣는다.
								ciDEBUG(1, ("host2 %d matched without host1",i) );
								char newval[256] = {0};
								sprintf(newval, "\t\t<ch name=\"ch%d\" set_ad=\"%s\"/>", i, ahost2.c_str());
								listPtr->push_back(newval);
								host2_exist = ciTrue;
								continue;
							}
							else
							{	// host1 있으면 host1 값이 아닌 첫번째 또는 제일 마지막에 넣는다.(도배케이스)
								if( i==8 || ciStringUtil::wildcmp(key1.c_str(), itr->c_str()) == 0 )
								{	// NONE case
									ciDEBUG(1, ("host2 %d matched with host1",i) );
									char newval[256] = {0};
									sprintf(newval, "\t\t<ch name=\"ch%d\" set_ad=\"%s\"/>", i, ahost2.c_str());
									listPtr->push_back(newval);
									host2_exist = ciTrue;
									continue;
								}
							}
						}
						listPtr->push_back(*itr);
					}
				}
			}

			ciStringList::iterator itr;
			for(itr=listPtr->begin(); itr!=listPtr->end(); itr++)
			{
				buf += *itr;
				buf += "\n";
			}
			buf += aTok;
			buf += "\n";
			continue;
		}

		if( !end && !start && ciStringUtil::wildcmp("*<ad_info*", aTok.c_str()) )
		{
			start = ciTrue;
			buf += aTok;
			buf += "\n";
			continue;
		}

		if( !end && start && ciStringUtil::wildcmp("*</ad_info>*",aTok.c_str()) )
		{
			start = ciFalse;
			end = ciTrue;
			ciStringList::iterator itr;
			for(itr=xmlLineList.begin(); itr!=xmlLineList.end(); itr++)
			{
				buf += itr->c_str();
			}
			buf += aTok;
			buf += "\n";
			continue;
		}

		if( ch_start )
		{
			if( host1_exist == ciFalse && key1.empty() == ciFalse && ciStringUtil::wildcmp(key1.c_str(), aTok.c_str()) )
			{
				host1_exist = ciTrue;
			}
			if( host2_exist == ciFalse && key2.empty() == ciFalse && ciStringUtil::wildcmp(key2.c_str(), aTok.c_str()) )
			{
				host2_exist = ciTrue;
			}
			chList.push_back(aTok);
			continue;
		}
		if( start )
		{
			if( ciStringUtil::wildcmp("*networkuse=\"0\"*", aTok.c_str()) )
			{
				ciDEBUG(1, ("%s skipped", aTok.c_str()) );
				continue;
			}
			else
			{
				skip = ciFalse;
				ciStringList::iterator itr;
				for(itr=xmlCompareLineList.begin(); itr!=xmlCompareLineList.end(); itr++)
				{
					if( ciStringUtil::wildcmp(itr->c_str(), aTok.c_str()) )
					{
						//ciDEBUG(1,("%s skipped", aTok.c_str()));
						skip = ciTrue;
						break;
					}
				}
			}
		}
		if( skip )
		{
			skip=ciFalse;
			continue;
		}
		buf += aTok;
		buf += "\n";
	}
	_xmlBuf = buf;
	_writeXml();
	return ciTrue;
*/
}

void tcpSocketSession::_buildXMLLine(ciStringList& iniList,
									 ciStringList& outXMLLineList,
									 ciStringList& outXMLCompareLineList,
									 const char* additionalLine)
{
	ciDEBUG(1, ("_buildXMLLine(%s)", (additionalLine?additionalLine:"NULL")) );

	ciStringList::iterator itr;
	for(itr=iniList.begin(); itr!=iniList.end(); itr++)
	{
		ciString filename = itr->c_str();

		if( strncmp(filename.c_str(), "__template_", strlen("__template_")) == 0 )
		{
			continue;
		}
		if( strncmp(filename.c_str(), "__preview_", strlen("__preview_")) == 0 )
		{
			continue;
		}
		if( strncmp(filename.c_str(), "UBCAnnounce", strlen("UBCAnnounce")) == 0 )
		{
			continue;
		}
		ciString channelName = filename;
		//ciStringUtil::divide(filename,'.',&channelName,&ext);
		ciStringUtil::cutPostfix(channelName, ".");

		// 채널명에 스페이스가 포함되어 있다면, 사용하지 말아야 한다.
		ciStringUtil::charReplace(channelName, ' ', '_');
		
		ciString siteName,desc,networkUse = "0";
		_getIniInfo(channelName.c_str(), siteName, desc, networkUse);

		char line[1024] = {0};
		sprintf(line, "\t\t<ad name=\"%s\" desc=\"%s\" sitename=\"%s\" networkuse=\"%s\" />\n",
			channelName.c_str(), desc.c_str(), siteName.c_str(), networkUse.c_str());

		outXMLLineList.push_back(line);

		memset(line,0x00,1024);
		sprintf(line, "*<ad name=\"%s\"*", channelName.c_str());
		outXMLCompareLineList.push_back(line);
	}

	if( additionalLine && strlen(additionalLine) )
	{
		ciString buf = additionalLine;
		int i = buf.find("desc");
		if( i > 2 )
		{
			buf = buf.substr(2, i-2);
			ciBoolean matched = ciFalse;
			ciStringList::iterator itr;
			for(itr=outXMLCompareLineList.begin(); itr!=outXMLCompareLineList.end(); itr++)
			{
				ciString tok = itr->substr(1,itr->size()-2);
				//ciDEBUG(1,("############ %s == %s ###########", tok.c_str(),buf.c_str()));
				if( buf == tok )
				{
					matched = ciTrue;
					break;
				}
			}
			if( !matched )
			{
				ciString buf3 = additionalLine;
				buf3 += "\n";
				//ciDEBUG(1, ("additionalLine=%s",buf3.c_str()) );

				outXMLLineList.push_back(buf3);
				ciString buf2 = "*" + buf + "*";
				outXMLCompareLineList.push_back(buf2);
			}
		}
	}
}

ciBoolean tcpSocketSession::_getIniInfo(const char* channelName,
									    ciString& out_siteId,
										ciString& out_desc,
										ciString& out_networkUse)
{
	ciDEBUG(1, ("_getIniInfo(%s)", channelName) );

	// 1. read ini file

	string filename = EXE_PATH;
	filename += "\\config\\";
	filename += channelName;
	filename += ".ini";

	char buf[1024] = {0};
	GetPrivateProfileString("Host", "description", "", buf, sizeof(buf), filename.c_str());

	//ciDEBUG(1, ("Description=%s\n", buf) );
	out_desc = buf;

	memset(buf,0x00,1024);
	GetPrivateProfileString("Host", "networkUse", "1", buf, sizeof(buf), filename.c_str());

	//ciDEBUG(1, ("networkUse=%s\n", buf) );
	out_networkUse = buf;

	memset(buf,0x00,1024);
	GetPrivateProfileString("Host", "site", "", buf, sizeof(buf), filename.c_str());

	//ciDEBUG(1, ("site=%s\n", buf) );
	out_siteId = buf;

	return ciTrue;
}

ciBoolean tcpSocketSession::readline(ciString& outVal)
{
	ciDEBUG(1, ("readline()") );

	ciGuard aGuard(_sockLock);

	ciBoolean eof = ciFalse;

	while( 1 )
	{
		char recvBuf[2] = {0};
		int nread = peer().recv_n(recvBuf, 1);
		if( nread < 0 )
		{
			ciWARN(("socket read error"));
			return ciFalse;
		}
		if( nread == 0 )
		{
			ciWARN(("unexpected socket EOF"));
			return ciFalse;
		}
		if( !eof && (recvBuf[0] == '\n') )
		{	// '0'
			eof = ciTrue;
			ciDEBUG(1, ("tcpSocketSession received(%s)",outVal.c_str()) );
			return ciTrue;
		}
		outVal += recvBuf;
	}
	return ciTrue;
}

ciBoolean tcpSocketSession::writeline(const char* inVal)
{
	ciString buf = inVal;
	return writeline(buf);
}

ciBoolean tcpSocketSession::writeline(ciString& inVal)
{
	ciDEBUG(1, ("Write to socket(%s)", inVal.c_str()) );

	ciGuard aGuard(_sockLock);

	inVal += "\0";

	int aSendLen = peer().send_n(inVal.c_str(), (int)inVal.length()+1);
	if( aSendLen <= 0 )
	{
		ciERROR(("socket() write error"));
		return ciFalse;
	}
	ciDEBUG(1, ("tcpSocketSession (%s) sent", inVal.c_str()) );
	return ciTrue;
}

ciBoolean tcpSocketSession::writePing()
{
	ciString inVal = "ping\0";

	ciGuard aGuard(_sockLock);

	int aSendLen = peer().send_n(inVal.c_str(), (int)inVal.length()+1);	
	if( aSendLen <= 0 )
	{
		ciERROR(("socket() write error"));
		return ciFalse;
	}
	return ciTrue;
}

DIRECTIVE_TYPE tcpSocketSession::_getDirectiveType(const char* directive)
{
	ciString dir = directive;

	if( dir == "pe_ping" )				return DIRECTIVE_pe_ping;
	if( dir == "refresh" )				return DIRECTIVE_refresh;
	if( dir == "browser_info" )			return DIRECTIVE_browser_info;
	if( dir == "import" )				return DIRECTIVE_import;
	if( dir == "displaySoundOn" )		return DIRECTIVE_displaySoundOn;
	if( dir == "shutdownTime" )			return DIRECTIVE_shutdownTime;
	if( dir == "week_shutdownTime" )	return DIRECTIVE_week_shutdownTime;
	if( dir == "FV_COMMAND_SHUTDOWN" )	return DIRECTIVE_FV_COMMAND_SHUTDOWN;
	if( dir == "FV_COMMAND_REFRESH" )	return DIRECTIVE_FV_COMMAND_REFRESH;
	if( dir == "FV_REBOOT" )			return DIRECTIVE_FV_REBOOT;
	if( dir == "FV_SHUTDOWN" )			return DIRECTIVE_FV_SHUTDOWN;
	if( dir == "FV_UBCSTART" )			return DIRECTIVE_FV_UBCSTART;
	if( dir == "FV_UBCSTOP" )			return DIRECTIVE_FV_UBCSTOP;
	if( dir == "FV_STUDIO" )			return DIRECTIVE_FV_STUDIO;
	if( dir == "FV_WEDDING_EXPORT" )	return DIRECTIVE_FV_WEDDING_EXPORT;
	if( dir == "FV_WEDDING_IMPORT" )	return DIRECTIVE_FV_WEDDING_IMPORT;
	if( dir == "FV_QUERY_UBC_STATUS" )	return DIRECTIVE_FV_QUERY_UBC_STATUS;
	if( dir == "FV_CH_PLAY" )			return DIRECTIVE_FV_CH_PLAY;
	if( dir == "FV_CH_DOWNLOAD" )		return DIRECTIVE_FV_CH_DOWNLOAD;
	if( dir == "FV_CH_STOP" )			return DIRECTIVE_FV_CH_STOP;
	if( dir == "FV_QUERY_CURRENT_CH" )	return DIRECTIVE_FV_QUERY_CURRENT_CH;
	if( dir == "FV_AUTO_OFF" )			return DIRECTIVE_FV_AUTO_OFF;
	if( dir == "FV_AUTO_ON" )			return DIRECTIVE_FV_AUTO_ON;
	if( dir == "FV_CH_MODIFY" )			return DIRECTIVE_FV_CH_MODIFY;
	if( dir == "FV_SCHEDULE_MODIFY" )	return DIRECTIVE_FV_SCHEDULE_MODIFY;
	if( dir == "FV_IMPORT" )			return DIRECTIVE_FV_IMPORT;
	if( dir == "FV_EXPORT" )			return DIRECTIVE_FV_EXPORT;
	if( dir == "FV_REFRESH" )			return DIRECTIVE_FV_REFRESH;
	if( dir == "FV_CH_DELETE" )			return DIRECTIVE_FV_CH_DELETE;
	if( dir == "FV_DELETE_ALL" )		return DIRECTIVE_FV_DELETE_ALL;
	if( dir == "FV_QUERY_RECOVERY" )	return DIRECTIVE_FV_QUERY_RECOVERY;
	if( dir == "FV_CH_RECOVERY" )		return DIRECTIVE_FV_CH_RECOVERY;
	if( dir == "FV_CH_ADD" )			return DIRECTIVE_FV_CH_ADD;
	if( dir == "FV_DISPLAY_SET" )		return DIRECTIVE_FV_DISPLAY_SET;
	if( dir == "FV_TOOLS" )				return DIRECTIVE_FV_TOOLS;
	if( dir == "FV_UPDATE" )			return DIRECTIVE_FV_UPDATE;
	if( dir == "FV_REGISTER" )			return DIRECTIVE_FV_REGISTER;
	if( dir == "FV_SHUTDOWNTIME" )		return DIRECTIVE_FV_SHUTDOWNTIME;
	if( dir == "FV_WEEK_SHUTDOWNTIME" )	return DIRECTIVE_FV_WEEK_SHUTDOWNTIME;
	if( dir == "FV_MOUSE" )				return DIRECTIVE_FV_MOUSE;
	if( dir == "FV_STARTER" )			return DIRECTIVE_FV_STARTER;
	if( dir == "FV_APPLICATION" )		return DIRECTIVE_FV_APPLICATION;

	if( strncmp(directive, "ping", 4)==0 ) return DIRECTIVE_ping;

	return DIRECTIVE_UNKNOWN;
}
//
//tcpSocketSessionList tcpSocketSession::_list;
//ciMutex tcpSocketSession::_listLock;
//
//ciBoolean tcpSocketSession::addInstance(tcpSocketSession* obj)
//{
//	ciGuard aGuard(_listLock);
//	_list.push_back(obj);
//
//	ciDEBUG(1, ("addInstance(%d)", _list.size()) );
//
//	return ciTrue;
//}
//
//ciBoolean tcpSocketSession::removeInstance(tcpSocketSession* obj)
//{
//	ciGuard aGuard(_listLock);
//
//	tcpSocketSessionList::iterator itr = _list.begin();
//	tcpSocketSessionList::iterator end = _list.end();
//	for( ; itr!=end; itr++)
//	{
//		tcpSocketSession* tcp_sock = (*itr);
//		if(obj == tcp_sock)
//		{
//			_list.erase(itr);
//			ciDEBUG(1, ("success removeInstance(%d)", _list.size()) );
//			return ciTrue;
//		}
//	}
//
//	ciWARN(("fail to removeInstance(%d) !!!", _list.size()) );
//	return ciFalse;
//}
//
//ciBoolean tcpSocketSession::sendToAll(const char* data)
//{
//	ciDEBUG(1, ("sendToAll(%s)", data) );
//
//	ciGuard aGuard(_listLock);
//
//	tcpSocketSessionList::iterator itr = _list.begin();
//	tcpSocketSessionList::iterator end = _list.end();
//	for( ; itr!=end; itr++)
//	{
//		tcpSocketSession* tcp_sock = (*itr);
//
//		if(tcp_sock->_valid)
//			tcp_sock->writeline(data);
//	}
//
//	return ciTrue;
//}
