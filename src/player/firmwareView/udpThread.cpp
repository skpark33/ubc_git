/*! \file udpThread.C
 *  Copyright ⓒ 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
*/

#include "udpThread.h"

#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <common/libScratch/scratchUtil.h>
#include <common/libCommon/ubcIni.h>

#if defined(_COP_ACC_) && defined(_COP_NOSTDLIB_)
#	include <fstream.h>
#else
#	include <fstream>
using namespace std;
#endif

#if defined(_WIN32)
#   include <winsock2.h>
//  WinSock DLL을 사용할 버전
#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0
#	include <time.h>
#else
#   include <sys/socket.h>       /* for socket(), connect(), send(), and recv() */
#   include <arpa/inet.h>        /* for sockaddr_in and inet_addr() */
#endif

#define BUF_LEN 128
#define EXE_PATH _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\")
#define SERIAL_CONFIG_FILE  _COP_CD("C:\\SQISoft\\Contents\\Enc\\xml\\configure.xml")



ciSET_DEBUG(10, "udpThread");

udpThread::udpThread() {
	ciDEBUG(3, ("udpThread()"));
	_port = 14008;
	_currentSerialServerIp="";
	_useSerial = ciFalse;
}

udpThread::udpThread(unsigned short port,ciBoolean useSerial) {
	ciDEBUG(3, ("udpThread(%ld)",port));
	_port = port;
	_currentSerialServerIp="";
	_useSerial = useSerial;
}

udpThread::~udpThread() {
	ciDEBUG(3, ("~udpThread()"));
}



void
udpThread::run()
{
	ciDEBUG(1,("run(%1d)",_port));

	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA       WsaData;            // receives data from WSAStartup 
	if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {		
		return ;
	}
	
	ciDEBUG(3,("%s  Server : start...\n", (const char*)curTimeStr().c_str()));  

	struct sockaddr_in server_addr, client_addr;	// socket address
	
	// socket 생성
	SOCKET server_fd = socket( AF_INET, SOCK_DGRAM, 0 );
	memset( &server_addr, 0, sizeof(struct sockaddr) );
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(_port);

	// bind() 호출
	if( bind(server_fd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) < 0 ){
		ciERROR(("Server: Can't bind local address"));
		return ;
	}

	ciDEBUG(3,("Server start.(%s)\n", inet_ntoa(server_addr.sin_addr)));

	while(1) {
		char buf[BUF_LEN+1];
		memset(buf,0x00,BUF_LEN);
		// 연결요청을 기다림
		int len = sizeof(client_addr);
		int recvLen = recvfrom(server_fd,buf,BUF_LEN,0,(struct sockaddr *)&client_addr, &len);
		if(recvLen < 0) {
			ciDEBUG(3,("Server: receive failed."));
			continue;
		}
		buf[recvLen]=0;
		ciString clientIp = inet_ntoa(client_addr.sin_addr);
		ciDEBUG(3,("Server : received from (%s)\n", clientIp.c_str()));
		ciDEBUG(3,("Server : received data (%s)\n", buf));

		static ciString hostId;
		if(ciArgParser::getInstance()->isSet("+static_hostname")) {
			ciString mac,edition;
			scratchUtil::getInstance()->readAuthFile(hostId,mac,edition);
		}

		if(strcmp(buf,"ping")==0) {
		
			if(hostId.empty()){
				// get host name
				char hostName[BUF_LEN];
				memset(hostName,0x00,BUF_LEN);
					DWORD namesize=BUF_LEN;
					GetComputerNameEx(ComputerNamePhysicalDnsHostname, hostName, &namesize);
					hostId = hostName;
			}
		
			// get shutdown time
			string timeStr="";
			getTimeStr(timeStr);
			// get mac address
			string mac="";
			if(scratchUtil::getInstance()->getMacAddress(mac)){
				ciDEBUG(1,("mac=%s", mac.c_str()));		
				ciStringUtil::stringReplace(mac,"-","");  // "-" 을 없앰.
			}else{
				ciERROR(("mac address get failed"));
			}
			// get 모니터의 갯수
			int monitorCount = scratchUtil::getInstance()->getMonitorCount();
			
			char res[BUF_LEN];
			memset(res,0x00,BUF_LEN);
			//sprintf(res,"%s|%s|%s|%d", hostIName,timeStr.c_str(), mac.c_str(),monitorCount);
			sprintf(res,"%s|%s|%s|%d", hostId.c_str(),timeStr.c_str(), mac.c_str(),monitorCount);
			ciDEBUG(1,("res=%s", res));		
			
			int sendLen = sendto(server_fd,res,BUF_LEN,0,(struct sockaddr *)&client_addr, len);
			if(sendLen<0){
				ciDEBUG(3,("Server: send failed."));
				continue;
			}
			ciDEBUG(3,("Server : send to   (%s)\n", inet_ntoa(client_addr.sin_addr)));
			ciDEBUG(3,("Server : send data (%s)\n", res));

		} else if (strcmp(buf,"serial")==0) {

			if(!_useSerial){
				continue;
			}

			ciDEBUG(1,("serial interface received(%s)", clientIp.c_str()));
			if(_currentSerialServerIp.empty()){
				getCurrentSerialServerIP();
			}

			char res[BUF_LEN];
			memset(res,0x00,BUF_LEN);

			if(_currentSerialServerIp.empty()) {
				sprintf(res,"NO");
			}else{
				if(_currentSerialServerIp != clientIp){
					changeSerialServerIP(clientIp.c_str());
				}
				sprintf(res,"OK");
			}
			ciDEBUG(1,("res=%s", res));		

			int sendLen = sendto(server_fd,res,BUF_LEN,0,(struct sockaddr *)&client_addr, len);
			if(sendLen<0){
				ciDEBUG(3,("Server: send failed."));
				continue;
			}
			ciDEBUG(3,("Server : send to   (%s)\n", inet_ntoa(client_addr.sin_addr)));
			ciDEBUG(3,("Server : send data (%s)\n", res));
		
		}
	}

	closesocket(server_fd);
	WSACleanup();		
	
	return;
}


ciBoolean
udpThread::getCurrentSerialServerIP()
{
	ciDEBUG(1,("getCurrentSerialServerIp()"));
	FILE* fp_r = fopen(SERIAL_CONFIG_FILE, "r");
	if(!fp_r){
		ciERROR(("%s file open failed", SERIAL_CONFIG_FILE));
		return ciFalse;
	}

	char buf[1024];
	while(fgets(buf,sizeof(buf)-1, fp_r)){
		ciString buf2 = buf;
		int i1 = buf2.find("<serverurl>");
		if(i1>0){
			i1 += strlen("<serverurl>");
			int i2 = buf2.find("</serverurl>");
			if(i2>i1){
				_currentSerialServerIp = buf2.substr(i1,i2-i1);
				ciDEBUG(1,("Current Serial ServerIP=%s", _currentSerialServerIp.c_str()));
				break;
			}
		}
	}
	fclose(fp_r);
	return ciTrue;
}

ciBoolean
udpThread::changeSerialServerIP(const char* ip)
{
	ciDEBUG(1,("changeSerialServerIP(%s)",ip));
	
	FILE* fp_r = fopen(SERIAL_CONFIG_FILE, "r");
	if(!fp_r){
		ciERROR(("%s file open failed", SERIAL_CONFIG_FILE));
		return ciFalse;
	}

	_currentSerialServerIp = ip;
	ciStringList aList;
	char buf[1024];
	while(fgets(buf,sizeof(buf)-1, fp_r)){
		ciString buf2 = buf;
		int i1 = buf2.find("<serverurl>");
		if(i1>0){
			ciString line = "\t<serverurl>" + _currentSerialServerIp + "</serverurl>\n";
			aList.push_back(line);
		}else{
			aList.push_back(buf);
		}

	}
	fclose(fp_r);

	FILE* fp_w = fopen(SERIAL_CONFIG_FILE, "w");
	if(!fp_w){
		ciERROR(("%s file open failed", SERIAL_CONFIG_FILE));
		return ciFalse;
	}
	ciDEBUG(1,("%s file opened (line=%d)", SERIAL_CONFIG_FILE,aList.size()));

	ciStringList::iterator itr;
	for(itr=aList.begin();itr!=aList.end();itr++){
		fputs(itr->c_str(),fp_w);
	}
	fclose(fp_w);
	return ciTrue;
}

ciBoolean
udpThread::changeClientIP(const char* ip, const char* mac, const char* hostId)
{
	ciDEBUG(1,("changeClientIP(%s)",ip));
	
	FILE* fp_r = fopen(SERIAL_CONFIG_FILE, "r");
	if(!fp_r){
		ciERROR(("%s file open failed", SERIAL_CONFIG_FILE));
		return ciFalse;
	}

	ciStringList aList;
	char buf[1024];
	while(fgets(buf,sizeof(buf)-1, fp_r)){
		ciString buf2 = buf;
		int i1 = buf2.find("<clienturl>");
		if(i1>0){
			ciString line = "\t<clienturl>" + ciString(ip) + "</clienturl>\n";
			aList.push_back(line);
		}else{
			int i2 = buf2.find("<number>");
			if(i2>0){
				ciString line = "\t<number>" + ciString(mac) + "</number>\n";
				aList.push_back(line);
				line = "\t<hostid>" + ciString(hostId) + "</hostid>\n";
				aList.push_back(line);
			}else{
				int i3 = buf2.find("<hostid>");
				if(i3>0){
					//기존에 있는 hostid 라인은 스킵한다.
				}else{
					aList.push_back(buf);
				}
			}
		}
	}
	fclose(fp_r);

	FILE* fp_w = fopen(SERIAL_CONFIG_FILE, "w");
	if(!fp_w){
		ciERROR(("%s file open failed", SERIAL_CONFIG_FILE));
		return ciFalse;
	}
	ciDEBUG(1,("%s file opened (line=%d)", SERIAL_CONFIG_FILE,aList.size()));

	ciStringList::iterator itr;
	for(itr=aList.begin();itr!=aList.end();itr++){
		fputs(itr->c_str(),fp_w);
	}
	fclose(fp_w);
	return ciTrue;
}

ciBoolean
udpThread::getTimeStr(string& timeStr)
{
	ubcConfig aIni("UBCVariables");
	aIni.get("SHUTDOWNTIME","value", timeStr);
	return true;
/*
	ciString argValue="";
	
	if(!ubcConfig::lvc_getProperty("STARTER.APP_AGENT.ARGUMENT", argValue)){
		ciDEBUG(3,("getProperties STARTER.APP_AGENT.ARGUMENT failed"));
		return false;
	}

	boolean has_set = false;
	boolean has_min = false;
	boolean has_hour = false;
	ciString newValue= "";
	ciStringTokenizer tokens(argValue.c_str());
	
	while(tokens.hasMoreTokens()){
		ciString token = tokens.nextToken();

		if(token == "+shutdown"){
			has_set = ciTrue;
			continue;
		}
		if(!has_set){
			continue;
		}
		if(token == "+hour"){
			has_hour = true;
			continue;
		}
		if(token == "+minute"){
			has_min = true;
			continue;
		}
		if(has_hour){
			newValue += token;
			newValue += ":";
			has_hour = false;
		}
		if(has_min){
			newValue += token;
			has_min = false;
		}
	}

	timeStr = newValue;
	return true;
*/
}


string 
udpThread::curTimeStr() 
{
    time_t current;
    time(&current);

    char* str;
    struct tm aTM;
#ifdef _WIN32
    struct tm *temp = localtime(&current);
    if (temp) {
        aTM = *temp;
    } else {
        memset((void*)&aTM, 0x00, sizeof(aTM));
    }
#else
    localtime_r(&current, &aTM);
#endif

    str = (char*)malloc(sizeof(char)*20);
    sprintf(str,"%4d-%02d-%02d %02d:%02d:%02d",
        aTM.tm_year+1900,aTM.tm_mon+1,aTM.tm_mday,
        aTM.tm_hour,aTM.tm_min,aTM.tm_sec);
    string strTime = str;
    free(str);
    return strTime;
}
