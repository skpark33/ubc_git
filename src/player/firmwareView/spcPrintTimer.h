#ifndef _spcPrintTimer_H_
#define _spcPrintTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <hi/libHttp/ubcMux.h>

class spcPrintData {
public:
	spcPrintData() : zorder(0), volume(0), alreadyDownload(ciFalse), printResult(ciFalse) {}
	ciShort			zorder;
	ciString		filename;
	ciULong			volume;
	ciBoolean		alreadyDownload;
	ciBoolean		printResult;

	void print();
};
typedef map<int, spcPrintData*> SpcPrintMAP;

class spcPrintTimer : public ciWinPollable
{
public:
	spcPrintTimer(ciString& site, ciString& host);
	virtual ~spcPrintTimer(void);

	virtual void processExpired(ciString name, int counter, int interval);

	void		clearMap();
	void		printMap();

protected:

	void		_makePath(const char* relpath, CString& outval);
	ciBoolean	_getSpcPrintPhoto(const char* url, CStringArray& line_list);
	int			_setSpcPrintPhoto(CStringArray& line_list);
	int			_downloadPrintPhoto();
	int			_removePrintPhoto();
	int			_printPhoto();
	ciBoolean	_printAction(ciString& filename);
	ciBoolean	_isPrinterOnline(CString& printerName);
	ciBoolean	_isPrintPhoto(ciString& filename);
	ciBoolean	_isExist(ciString& filename);
	unsigned long	_getFileSize(const char* filename);
	HRESULT		LPCTSTR_to_BSTR(BSTR *pbstr, LPCTSTR psz);

	TCHAR		_cDrive[MAX_PATH];
	TCHAR		_cPath[MAX_PATH];

	ciMutex		_mapLock;
	ciString	_site;
	ciString	_host;

	ciString	_server;
	ciULong		_port;

	SpcPrintMAP	_map;

	ULONG_PTR	_gdiplusToken;
};

#endif //_spcPrintTimer_H_
