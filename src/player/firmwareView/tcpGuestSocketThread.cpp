/*! \file tcpGuestSocketThread.C
 *  Copyright ⓒ 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
*/

#include "tcpGuestSocketThread.h"

#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <common/libScratch/scratchUtil.h>
#include <common/libCommon/ubcIni.h>

#if defined(_COP_ACC_) && defined(_COP_NOSTDLIB_)
#	include <fstream.h>
#else
#	include <fstream>
using namespace std;
#endif

#if defined(_WIN32)
#   include <winsock2.h>
//  WinSock DLL을 사용할 버전
#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0
#	include <time.h>
#else
#   include <sys/socket.h>       /* for socket(), connect(), send(), and recv() */
#   include <arpa/inet.h>        /* for sockaddr_in and inet_addr() */
#endif


ciSET_DEBUG(10, "tcpGuestSocketThread");

tcpGuestSocketThread::tcpGuestSocketThread() {
	ciDEBUG(3, ("tcpGuestSocketThread()"));
}

tcpGuestSocketThread::~tcpGuestSocketThread() {
	ciDEBUG(3, ("~tcpGuestSocketThread()"));
	guest_close();
}

void
tcpGuestSocketThread::run()
{
	ciDEBUG(1,("run()"));
	guest_accept();
	return;
}


ciBoolean 
tcpGuestSocketThread::guest_accept()
{
	ciDEBUG(1,("guest_accept()"));

	int port=14003;
	ciString portStr;
	if(ciArgParser::getInstance()->getArgValue("+guest_port", portStr)){
		port = atoi(portStr.c_str());
	}

	ACE_INET_Addr rAddr(port);
	while(1) {
		if( _guest_acceptor.open(rAddr) < 0 ) {
			ciDEBUG(1,("session's open() for Port[%d] is FAILED", port ));
			//_useGuestBook = ciFalse;
			//return ciFalse;
			::Sleep(30000);
			continue;
		}
		break;
	}
	ciDEBUG(1,("Socket accepted(%ld) for read", port));
	return ciTrue;
}

void 
tcpGuestSocketThread::guest_close()
{
	ciDEBUG(1,("guest_close()"));
	_guest_acceptor.close();
}