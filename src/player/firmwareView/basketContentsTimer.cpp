#include "stdafx.h"
#include "libDownload/Downloader.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include "ci/libDebug/ciArgParser.h"
#include "basketContentsTimer.h"
#include "common/libHttpRequest/HttpRequest.h"
#include "common/libInstall/installUtil.h"
#include <common/libScratch/scratchUtil.h>
#include <common/libCommon/ubcIni.h>

ciSET_DEBUG(9, "basketContentsTimer");


void 
basketContentsData::print() 
{
	ciDEBUG(1,("%s,%d,%d,%s,%s,%s,%s,%lu,%s,%d,%s,%s,%s,%d,%d,%d\n",
		basketContentsId.c_str(),
		zorder,
		contentsType,
		siteId.c_str(),
		contentsId.c_str(),
		location.c_str(),
		filename.c_str(),
		volume,
		description.c_str(),
		runningTime,
		registerId.c_str(),
		registerTime.c_str(),
		startTime.c_str(),
		alreadyDownload,
		copySucceed,
		timeExpired
		));
}

ciBoolean 
basketContentsData::load(int pZorder) 
{
	ciBoolean retval = ciFalse;
	char key[10];
	memset(key,0x00,10);
	sprintf(key,"%d",pZorder);

	ubcConfig aIni("UBCBasketContents");

	retval = aIni.get(key,"basketContentsId",basketContentsId);
	if(retval) {
		aIni.get(key,"zorder",zorder);
		aIni.get(key,"contentsType",contentsType);
		aIni.get(key,"siteId",siteId);
		aIni.get(key,"contentsId",contentsId);
		aIni.get(key,"location",location);
		aIni.get(key,"filename",filename);
		aIni.get(key,"volume",volume);
		aIni.get(key,"description",description);
		aIni.get(key,"runningTime",runningTime);
		aIni.get(key,"registerId",registerId);
		aIni.get(key,"registerTime",registerTime);
		aIni.get(key,"startTime",startTime);
		aIni.get(key,"alreadyDownload",alreadyDownload);
		aIni.get(key,"copySucceed",copySucceed);
		aIni.get(key,"timeExpired",timeExpired);
	}
	return retval;
}


ciBoolean 
basketContentsData::write() 
{
	ciBoolean retval = ciFalse;
	char key[10];
	memset(key,0x00,10);
	sprintf(key,"%d",zorder);

	ubcConfig aIni("UBCBasketContents");

	retval = aIni.set(key,"basketContentsId",basketContentsId.c_str());
	if(retval) {
		aIni.set(key,"zorder",zorder);
		aIni.set(key,"contentsType",contentsType);
		aIni.set(key,"siteId",siteId.c_str());
		aIni.set(key,"contentsId",contentsId.c_str());
		aIni.set(key,"location",location.c_str());
		aIni.set(key,"filename",filename.c_str());
		aIni.set(key,"volume",volume);
		aIni.set(key,"description",description.c_str());
		aIni.set(key,"runningTime",runningTime);
		aIni.set(key,"registerId",registerId.c_str());
		aIni.set(key,"registerTime",registerTime.c_str());
		aIni.set(key,"startTime",startTime.c_str());
		aIni.set(key,"alreadyDownload",alreadyDownload);
		aIni.set(key,"copySucceed",copySucceed);
		aIni.set(key,"timeExpired",timeExpired);
	}
	return retval;
}


basketContentsTimer::basketContentsTimer(ciString& site, ciString& host) 
{
	_site = site;
	_host = host;

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, _cDrive, _cPath, cFilename, cExt);

	if(ciArgParser::getInstance()->isSet("+reboot")) {
		_useReboot = ciTrue;
	}else{
		_useReboot = ciFalse;
	}


	ciDEBUG(3, ("basketContentsTimer()"));

	ubcConfig aIni("UBCConnect");

	aIni.get("GPQS","FTPServer1",_server1);
	if(_server1.empty()){
		_server1 = "10.10.46.128";
	}
	aIni.get("GPQS","FTPServer2",_server2);
	if(_server2.empty()){
		_server2 = "10.10.46.129";
	}
	_port = 0;
	aIni.get("GPQS","Port",_port);
	if(_port == 0) _port = 8080;


	_loadIni();
}

basketContentsTimer::~basketContentsTimer() {
	ciDEBUG(3, ("~basketContentsTimer()"));
}

void
basketContentsTimer::_makePath(const char* relpath, CString& outval)
{
	CString strPath;
	strPath.Format(_T("%s%s..\\..\\%s"), _cDrive, _cPath, relpath);
	CFileStatus status;
	CFile::GetStatus(strPath, status );
	outval = status.m_szFullName;
}

void
basketContentsTimer::processExpired(ciString name, int counter, int interval)
{
	// 이 타이머는 1분마다 한번씩 호출된다.

	ciTime now;
	char nowStr[20];
	memset(nowStr,0x00,20);
	sprintf(nowStr, "%04d%02d%02d%02d%02d%02d", 
		now.getYear(),now.getMonth(),now.getDay(),now.getHour(), now.getMinute(),now.getSecond());

	ciDEBUG(3, ("processExpired(%s,%s,%d,%d)", nowStr, name.c_str(), counter, interval));

	if(counter > 1 ) {
		// 첫회는 건너뛴다. 이후 매분 호출된다.
		if(_startTimeExpired()){	
			if(_safeCopyContents((const char*)nowStr)) {
				_writeIni();
				if(_useReboot) {
					ciDEBUG(1,("system reboot for basketcontents changed"));
					system("rebootSystem.bat");
				}
			}
		}
	}

	if(counter % 3 != 1) {
		// 이하는 처음한번 호출되고, 이후, 3분마다 한번씩 호출되도록 한다.
		return;  
	}

	CStringArray line_list;
	CString sendMsg;
	sendMsg.Format("siteId=%s", _site.c_str());
	if(!_getBasketContents("/UBC_Player/get_basketcontents.php", sendMsg, line_list)){
		return;
	}
	if(!_setBasketContents(line_list)) {
		ciWARN(("No basketContents changed"));
		return;
	}

	_writeIni();
	
	if(_removeContents(nowStr)){
		_writeIni();
		if(_useReboot) {
			ciDEBUG(1,("system reboot for basketcontents remove"));
			system("rebootSystem.bat");
		}
	}

	if(!_downloadContents()) {
		return;
	}

	_writeIni();
	
	
	if(counter % 60 == 1) {
		// 이하는 처음한번 호출되고, 이후, 60분마다 한번씩 호출되도록 한다.
		// 3일이 지난 old 자료는 지운다.
		_deleteOldFile(3); 
	}

}


ciBoolean
basketContentsTimer::_getBasketContents(const char* url, CString& sendMsg, CStringArray& line_list)
{
	ciDEBUG(3, ("_getBasketContents(%s,%s)", url,sendMsg));
	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_SERVER);
	BOOL ret_val = http_request.RequestPost(url,sendMsg,line_list);
	if(!ret_val) {
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	}
	
	BOOL getSucceed = false;
	if(line_list.GetCount() > 1 )
	{
		CString first_line =  line_list.GetAt(0);
		if(	first_line.GetLength() >= 2 && 
			first_line.Mid(0,2).CompareNoCase("OK") == 0) 
		{
			getSucceed = true;	
		}
	}

	if(!getSucceed){
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	}

	return ciTrue;
}

int
basketContentsTimer::_setBasketContents(CStringArray& line_list)
{
	ciDEBUG(3, ("_setBasketContents()"));

	//clearMap();
	
	int retval = 0;
	basketContentsData* aData = NULL;

	int count = line_list.GetCount();
	for(int i=0; i<count; i++)
	{
		ciString line = line_list.GetAt(i);

		ciString key, value;
		ciStringUtil::divide(line,'=',&key,&value);

		if(key == "zorder") {
			ciGuard aGuard(_mapLock);
			BasketContentsMAP::iterator itr =_map.find(atoi(value.c_str()));
			if(itr != _map.end()){
				aData = itr->second;
				ciDEBUG(1,("%d data founded", aData->zorder));
				retval++;
			}else{
				aData = new basketContentsData();
				aData->zorder = atoi(value.c_str());
				_map.insert(BasketContentsMAP::value_type(aData->zorder, aData));
				ciDEBUG(1,("%d data inserted", aData->zorder));
			}
			continue;
		}
		if(aData==0){
			continue;
		}

		if(key == "basketContentsId")				
		{
			if(aData->basketContentsId != value) {
				ciDEBUG(1,("chanaged"));
				aData->basketContentsId = value;
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
		else if(key == "contentsType")	
		{
			int buf = atoi(value.c_str());		
			if(aData->contentsType != buf) {
				ciDEBUG(1,("chanaged"));
				aData->contentsType = buf;
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
		else if(key == "siteId") 
		{		
			if(aData->siteId != value) {
				ciDEBUG(1,("chanaged"));
				aData->siteId = value;
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
		else if(key == "contentsId") 
		{
			if(aData->contentsId != value) {
				ciDEBUG(1,("chanaged"));
				aData->contentsId = value;
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
		else if(key == "location")
		{
			if(aData->location != value) {
				ciDEBUG(1,("chanaged"));
				aData->location = value;
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
		else if(key == "filename")
		{
			//if(aData->description != value ) {
			ciDEBUG(1,("filename=%s", value.c_str()));
				//		
				//if(!aData->filename.empty() && value.empty()){
				//	//aData->alreadyDownload = ciFalse;
				//	//aData->copySucceed = ciFalse;
				//}else{
				//	aData->alreadyDownload = ciFalse;
				//	aData->copySucceed = ciFalse;
				//}
				aData->filename = value;
				//retval++;
			//}
		}
		else if(key == "volume")
		{
			unsigned long buf = strtoul(value.c_str(),NULL,10);
			if(aData->volume != buf) {
				ciDEBUG(1,("chanaged"));
				aData->volume = buf;
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
		//else if(key == "description")
		//{
		//	if(aData->description != value) {
		//		aData->description = value;
		//		aData->alreadyDownload = ciFalse;
		//		aData->copySucceed = ciFalse;
		//		retval++;
		//	}
		//}
		else if(key == "runningTime")
		{
			int buf = atoi(value.c_str());		
			if(aData->runningTime != buf) {
				ciDEBUG(1,("chanaged"));
				aData->runningTime = buf;
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
		else if(key == "registerId")
		{
			if(aData->registerId != value) {
				ciDEBUG(1,("chanaged"));
				aData->registerId = value;
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
		else if(key == "registerTime")
		{
			if(aData->registerTime != value) {
				ciDEBUG(1,("chanaged"));
				aData->registerTime = value.c_str();
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
		else if(key == "startTime")
		{
			if(aData->startTime != value) {
				ciDEBUG(1,("chanaged"));
				aData->startTime = value.c_str();
				aData->alreadyDownload = ciFalse;
				aData->copySucceed = ciFalse;
				retval++;
			}
		}
	}


	BasketContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		basketContentsData* data = itr->second;
		data->description = data->filename;
		if(!data->filename.empty()) {
			ciStringTokenizer aTokens(data->filename,".");
			ciString ext;
			while(aTokens.hasMoreTokens()){
				ext = aTokens.nextToken();
			}
			ciString prefix = data->contentsId;
			ciStringUtil::getBraceValue(prefix,"{","}");
			data->filename = prefix;
			data->filename += ".";
			data->filename += ext;
		}
	}

	printMap();
	return retval;
}



void
basketContentsTimer::clearMap()
{
	ciDEBUG(1,("clearMap()"));
	ciGuard aGuard(_mapLock);

	BasketContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		delete (itr->second);
	}
	_map.clear();
}

void
basketContentsTimer::printMap()
{
	ciDEBUG(1,("printMap()"));
	ciGuard aGuard(_mapLock);
	BasketContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		itr->second->print();
	}
}

int
basketContentsTimer::_downloadContents()
{
	ciDEBUG(3, ("_downloadContents()"));

	int counter=0;
	CDownloader libDownloader;
	BasketContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		basketContentsData* aData = itr->second;
		aData->print();
		if(!aData->filename.empty() && (_isExist(aData->filename) == ciFalse || aData->alreadyDownload==ciFalse)) {
			//if(libDownloader.GetFile(_server1.c_str(), _server2.c_str(), _port,
			//	aData->filename.c_str(),aData->volume, aData->contentsId.c_str(), _site.c_str(), "basket")) {
			if(libDownloader.GetFile(aData->filename.c_str(),aData->volume, aData->contentsId.c_str(), _site.c_str(), "basket")) {
				unsigned long downloadedSize = _getFileSize(aData->filename.c_str());
				if(downloadedSize == aData->volume) {
					ciDEBUG(1,("basket/%s/%s file download succeed", aData->contentsId.c_str(),aData->filename.c_str()));
					aData->alreadyDownload = ciTrue;
					aData->copySucceed = ciFalse;
					counter++;
				}else{
					ciERROR(("basket/%s/%s file download failed (filesize(%ld) != downloadedSize(%ld)"
						, aData->contentsId.c_str(),aData->filename.c_str(),aData->volume, downloadedSize));
				}
			}else{
				ciERROR(("basket/%s/%s file download failed", aData->contentsId.c_str(),aData->filename.c_str()));
			}
		}
	}
	ciDEBUG(1,("%d data downloaded", counter));
	return counter;
}

ciBoolean
basketContentsTimer::_isExist(ciString& filename)
{
	CString source;
	_makePath("Contents\\Enc\\basket\\", source);
	source += filename.c_str();
	if(::access(source,0) == 0) {
		ciDEBUG(1,("file %s exist", source));
		return ciTrue;
	}
	ciDEBUG(1,("file %s not exist", source));
	return ciFalse;
}

ciBoolean
basketContentsTimer::_copyContents(const char* nowStr)
{
	ciDEBUG(3, ("_copyContents(%s)", nowStr));

	ciBoolean retval = ciTrue;
	int counter=0;
	BasketContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		basketContentsData* aData = itr->second;
		if(aData->timeExpired && aData->copySucceed == ciFalse &&  aData->alreadyDownload && !aData->filename.empty()) {
			// file 을 Contents/Enc/basket --> Contents/Enc 로 copy
			CString new_file_name ;
			//new_file_name.Format("%d_%s.%s", aData->zorder, aData->siteId.c_str(), (aData->contentsType==0 ? "avi" : "pptx"));
			new_file_name.Format("%d.%s", aData->zorder, (aData->contentsType==0 ? "avi" : "pptx"));
			
			CString source;
			_makePath("Contents\\Enc\\basket\\", source);
			CString target, moveName;
			_makePath("Contents\\Enc\\", target);
			
			source += aData->filename.c_str();
			moveName = target;
			moveName.Format("%s_old_%s_%s", target, nowStr, new_file_name);
			target += new_file_name;

			// copy할 파일이 이미 있다면 다른이름으로 무브한다.
			if(::access(target,0) == 0) {
				if(::MoveFile(target, moveName)){
					ciDEBUG(1,("%s --> %s move succeed", target, moveName));
				}else{
					ciWARN(("%s --> %s move failed", target, moveName));
				}
			}

			if(::CopyFile(source, target, FALSE)) {
				ciDEBUG(1,("%s --> %s copy file succeed", source, target));
				aData->copySucceed = ciTrue;
				aData->timeExpired = ciFalse;
				counter++;
			}else{
				ciERROR(("%s --> %s copy file failed", source, target));
				retval = ciFalse;
			}
		
		}
	}	
	ciDEBUG(1,("%d file copied", counter));
	return retval;
}

int
basketContentsTimer::_safeCopyContents(const char* nowStr)
{
	ciDEBUG(3, ("_safeCopyContents(%s)", nowStr));

	// brw 를 죽여야 한다.
	if(scratchUtil::getInstance()->killBrowser(3000,ciTrue)){
		ciDEBUG(1, ("Stop brw succeed"));
	}else{
		ciERROR(("Stop brw failed"));
	}

	int counter=0;
	unsigned long pid = scratchUtil::getInstance()->getPid("POWERPNT.EXE");
	while(pid > 0 && counter < 10) 
	{
		if(scratchUtil::getInstance()->killProcess(pid) > 0){
			ciDEBUG(1, ("Stop powerpnt succeed"));
		}
		::Sleep(500);  // 죽는중일수 있기 때문에 조금 쉬었다가 한다.
		counter++;
		pid = scratchUtil::getInstance()->getPid("POWERPNT.EXE");
	}

	counter = 0;
	ciBoolean retval = ciFalse;
	while(!retval && counter < 10) {
		::Sleep(500);
		retval = _copyContents(nowStr);
		counter++;
	}
	
	// starter 로 하여금 다시 살리게 한다.
	ciDEBUG(1,("Start brw"));
	scratchUtil::getInstance()->setBrwAdmin("1");

	return retval;
}


int
basketContentsTimer::_removeContents(const char* nowStr)
{
	ciDEBUG(3, ("_removeContents(%s)", nowStr));

	int counter=0;
	BasketContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		basketContentsData* aData = itr->second;
		if(aData->filename.empty() && aData->alreadyDownload==ciTrue && aData->copySucceed) {
			counter++;	
		}
	}
	if(counter > 0){
		// brw 를 죽여야 한다.  		
		if(installUtil::getInstance()->stopBrowser3(1)){
			ciDEBUG(1, ("Stop brw succeed"));
		}else{
			ciERROR(("Stop brw failed"));
		}
	}	

	ciBoolean isExist[2];
	for(int i=0;i<2;i++) {
		isExist[i] = ciFalse;
	}
	
	for(itr=_map.begin();itr!=_map.end();itr++){
		basketContentsData* aData = itr->second;
		if(aData->zorder < 2) {
			isExist[aData->zorder] = ciTrue;
		}
		if(aData->filename.empty() && aData->alreadyDownload==ciTrue && aData->copySucceed) {
			// file 을 Contents/Enc/basket --> Contents/Enc 로 copy
			CString remove_file_name ;
			//new_file_name.Format("%d_%s.%s", aData->zorder, aData->siteId.c_str(), (aData->contentsType==0 ? "avi" : "pptx"));
			remove_file_name.Format("%d.%s", aData->zorder, (aData->contentsType==0 ? "avi" : "pptx"));
			
			CString target, moveName;
			_makePath("Contents\\Enc\\", target);
			
			moveName = target;
			moveName.Format("%s_old_%s_%s", target, nowStr, remove_file_name);
			target += remove_file_name;

			// 파일이 local에 있다면 다른이름으로 무브한다.
			if(::access(target,0) == 0) {
				if(::MoveFile(target, moveName)){
					ciDEBUG(1,("%s --> %s move succeed", target, moveName));
					aData->alreadyDownload = ciFalse;
					aData->copySucceed = ciFalse;
					counter++;
				}else{
					ciWARN(("%s --> %s move failed", target, moveName));
				}
			}
		
		}
	}	

	// 존재하지 않는 파일은 지운다.
	for(int i=0;i<2;i++) {
		if(isExist[i] == ciFalse) {
			ciDEBUG(1,("%dth file does not required, it should be removed", i));
			CString remove_file_name ;
			remove_file_name.Format("%d.%s", i, (i==0 ? "avi" : "pptx"));

			CString target, moveName;
			_makePath("Contents\\Enc\\", target);
			
			moveName = target;
			moveName.Format("%s_old_%s_%s", target, nowStr, remove_file_name);
			target += remove_file_name;

			// 파일이 local에 있다면 다른이름으로 무브한다.
			if(::access(target,0) == 0) {
				if(::MoveFile(target, moveName)){
					ciDEBUG(1,("%s --> %s move succeed", target, moveName));
					counter++;
				}else{
					ciWARN(("%s --> %s move failed", target, moveName));
				}
			}

		}
	}


	if(counter > 0){
		// starter 로 하여금 다시 살리게 한다.
		ciDEBUG(1,("Start brw"));
		scratchUtil::getInstance()->setBrwAdmin("1");
	}

	ciDEBUG(1,("%d file removed", counter));
	return counter;
}


ciBoolean
basketContentsTimer::_startTimeExpired()
{
	// 아주 조건없이, 현재 이미 시간이 지난 데이터가 있는지만 본다.
	// 단, 이미 처리를 완료한 것은 뺀다. download 가 안된것도 뺀다.
	// 하나라도 시간이 도래한 데이터가 있으면 true 를 리턴한다.
	time_t now_t = time(null);
	now_t = now_t + 10;  // 10초 정도 타임머가 먼저동작하도록 한다.

	ciTime now(now_t);
	BasketContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		basketContentsData* aData = itr->second;
		ciTime aTime(aData->startTime.c_str());
		if(aTime.getTime() <= now_t) {
			ciDEBUG(1,("startTime exipired"));
			aData->timeExpired = ciTrue;		
		}
	}
	for(itr=_map.begin();itr!=_map.end();itr++){
		basketContentsData* aData = itr->second;
		if(aData->timeExpired && aData->copySucceed == ciFalse && aData->alreadyDownload && !aData->filename.empty()) {
			ciDEBUG(1,("startTime exipired and not yet moved"));
			return ciTrue;		
		}
	}
	ciDEBUG(1,("no file startTime is not exipired() or already moved() or not yet downloaed()"));
	return ciFalse;
}

int
basketContentsTimer::_deleteOldFile(ciShort durationDay)
{
	ciDEBUG(1,("_deleteOldFile()"));

	CString subDir;
	_makePath("Contents\\Enc\\", subDir);


	string dirPath = subDir;
	dirPath += "_old_*.*";

	ciDEBUG(5,("old files=%s", dirPath.c_str()));
	HANDLE hFile = NULL;
	WIN32_FIND_DATA FileData;
	hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		ciERROR(("old file not found"));
		return 0;
	}

	CTime referTime = CTime::GetCurrentTime();
	CTimeSpan spanTime(durationDay, 0, 0, 0);
	referTime = referTime - spanTime; // 2일이전

	int deleted_counter = 0;
	do {
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			continue;
		}

		ciString delFilePath = subDir;
		delFilePath += "\\";
		delFilePath += FileData.cFileName;
		ciDEBUG(5,("%s file founded", delFilePath.c_str()));

		CTime fileTime(FileData.ftLastWriteTime);
		if (fileTime < referTime) {
			::remove(delFilePath.c_str());
			deleted_counter++;
			ciDEBUG(5,("%s file deleted", delFilePath.c_str()));
		}
	
	} while (FindNextFile(hFile, &FileData));
	FindClose(hFile);
	return deleted_counter;
}

int
basketContentsTimer::_loadIni()
{
	ciDEBUG(1,("_loadIni()"));

	int counter=0;

	basketContentsData* aData = NULL;
	ciGuard aGuard(_mapLock);
	
	aData = new basketContentsData();
	if(aData->load(0) && !aData->basketContentsId.empty()) {
		_map.insert(BasketContentsMAP::value_type(aData->zorder, aData));
		ciDEBUG(1,("%d data inserted", aData->zorder));
		counter++;
	}
	aData = new basketContentsData();
	if(aData->load(1) && !aData->basketContentsId.empty()) {
		_map.insert(BasketContentsMAP::value_type(aData->zorder, aData));
		ciDEBUG(1,("%d data inserted", aData->zorder));
		counter++;
	}
/*
	aData = new basketContentsData();
	if(aData->load(2) && !aData->basketContentsId.empty()) {
		_map.insert(BasketContentsMAP::value_type(aData->basketContentsId, aData));
		ciDEBUG(1,("%s data inserted", aData->basketContentsId.c_str()));
		counter++;
	}
*/
	return counter;
}

int
basketContentsTimer::_writeIni()
{
	ciDEBUG(1,("_writeIni()"));
	int counter=0;
	BasketContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		basketContentsData* aData = itr->second;
		if(aData->write()) {
			counter++;
		}
	}
	ciDEBUG(1,("%d basketcontents data write", counter));
	return counter;
}


unsigned long
basketContentsTimer::_getFileSize(const char* filename)
{
	unsigned long retval = 0;

	CString fullpath;
	_makePath("Contents\\Enc\\basket\\", fullpath);
	fullpath += filename;

	ciDEBUG(1,("stat(%s)", fullpath));
	int fdes = open(fullpath,O_RDONLY);
	if(fdes<0){
		ciWARN(("%s file open failed", fullpath));
		return retval;
	}
	struct stat statBuf;
	if(fstat(fdes, &statBuf)==0){
		retval = statBuf.st_size;
	}
	ciDEBUG(1,("filesize(%s)=%ld", fullpath, retval));
	return retval;
}