//#include "stdafx.h"
#include <ci/libDebug/ciDebug.h>
#include "tcpSocketAcceptor.h"
#include "common/libScratch/scratchUtil.h"

ciSET_DEBUG(10, "tcpSocketAcceptor");

tcpSocketAcceptor::tcpSocketAcceptor() {
	
}

tcpSocketAcceptor::~tcpSocketAcceptor() {
}

int
tcpSocketAcceptor::make_svc_handler(tcpSocketSession*& pSvcHandler)
{
    ciDEBUG(1,("make_svc_handler(SvcHandler)"));

    return inherited::make_svc_handler(pSvcHandler);
}

int
tcpSocketAcceptor::activate_svc_handler(tcpSocketSession* pSvcHandler)
{
    ciDEBUG(1,("activate_svc_handler(SvcHandler)"));

	return inherited::activate_svc_handler(pSvcHandler);
}