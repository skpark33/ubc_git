#include "StdAfx.h"
#include "libDownload/Downloader.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciTime.h>
#include <ci/libFile/ciDirHandler.h>
#include <ci/libBase/ciStringTokenizer.h>
#include "ci/libDebug/ciArgParser.h"
#include "spcContentsTimer.h"
#include "common/libHttpRequest/HttpRequest.h"
#include "common/libInstall/installUtil.h"
#include <common/libScratch/scratchUtil.h>
#include <common/libCommon/ubcIni.h>

ciSET_DEBUG(9, "spcContentsTimer");

void
spcContentsData::print()
{
	ciDEBUG(1,("%d,%s,%d,%d",
		zorder,
		filename.c_str(),
		volume,
		alreadyDownload
		));
}

spcContentsTimer::spcContentsTimer(ciString& site, ciString& host)
{
	_site = site;
	_host = host;

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, _cDrive, _cPath, cFilename, cExt);

	CString hostFile;
	_makePath("Contents\\Enc\\xml\\host.xml", hostFile);
	_hostFile = hostFile;

	CString apiFolder;
	_makePath("Contents\\Enc\\xml", apiFolder);
	_apiFolder = apiFolder;

	_apiWriteSuccess = ciFalse;

	ciDEBUG(3, ("spcContentsTimer()"));

	ubcConfig aIni("UBCConnect");

	aIni.get("SPC","CMSServer",_server);
	if(_server.empty()){
		_server = "125.141.230.154";
	}

	_port = 0;
	aIni.get("SPC","CMSPort",_port);
	if(_port == 0) _port = 8090;
}

spcContentsTimer::~spcContentsTimer(void)
{
	ciDEBUG(3, ("~spcContentsTimer()"));
}

void
spcContentsTimer::processExpired(ciString name, int counter, int interval)
{
	// 이 타이머는 1분마다 한번씩 호출된다.
	ciTime now;
	char nowStr[20];
	memset(nowStr,0x00,20);
	sprintf(nowStr, "%04d%02d%02d%02d%02d%02d", 
		now.getYear(),now.getMonth(),now.getDay(),now.getHour(), now.getMinute(),now.getSecond());

	ciDEBUG(3, ("processExpired(%s,%s,%d,%d)", nowStr, name.c_str(), counter, interval));

	if (counter <= 1) {
		_makeHostXml();

		// 첫회는 건너뛴다.
		return;
	}

	if (_apiWriteSuccess == ciFalse) {
		_apiWriteSuccess = _makeSometimeContentsApiXml();

		if (_apiWriteSuccess == ciTrue) {
			// brw 를 죽여야 한다.
			if(installUtil::getInstance()->stopBrowser3(1)){
				ciDEBUG(1, ("Stop brw succeed"));

				::Sleep(500);  // 죽는중일수 있기 때문에 조금 쉬었다가 한다.

				// starter 로 하여금 다시 살리게 한다.
				ciDEBUG(1,("Start brw"));
				scratchUtil::getInstance()->setBrwAdmin("1");
			}else{
				ciERROR(("Stop brw failed"));
			}
		}
	}

	_makeRealtimeContentsApiXml();

	CStringArray line_list;

	CString sendMsg;
	sendMsg.Format("id=%s&type=all", _host.c_str());
	if (counter <= 5) {
		sendMsg.Format("id=%s&type=photo", _host.c_str());
	}

	if(!_getSpcContents("/spc_cms/contentsFileIni.do", sendMsg, line_list)){
		return;
	}

	if(!_setSpcContents(line_list)) {
		ciWARN(("No spcContents changed"));
	}

	if(!_downloadContents()) {
		ciWARN(("No downloadContents count"));
	}

	if (counter <= 2) {
		// 2회까지만 실행한다.
		if(!_removeContents()){
			ciWARN(("No removeContents count"));
		}
	}
}

void
spcContentsTimer::clearMap()
{
	ciDEBUG(1,("clearMap()"));
	ciGuard aGuard(_mapLock);

	SpcContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		delete (itr->second);
	}
	_map.clear();
}

void
spcContentsTimer::printMap()
{
	ciDEBUG(1,("printMap()"));
	ciGuard aGuard(_mapLock);
	SpcContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		itr->second->print();
	}
}

void
spcContentsTimer::_makePath(const char* relpath, CString& outval)
{
	CString strPath;
	strPath.Format(_T("%s%s..\\..\\%s"), _cDrive, _cPath, relpath);
	CFileStatus status;
	CFile::GetStatus(strPath, status );
	outval = status.m_szFullName;
}

ciBoolean
spcContentsTimer::_makeSometimeContentsApiXml()
{
	ciDEBUG(3, ("_makeSometimeContentsApiXml()"));

	CString apiUrl;
	apiUrl.Format("/spc_cms/contentsTotemInfo.do?id=%s", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsTotemInfo.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsFamousAllDetail.do?id=%s&lang=KR", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsFamousAllDetail_KR.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsFamousAllDetail.do?id=%s&lang=EN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsFamousAllDetail_EN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsFamousAllDetail.do?id=%s&lang=CN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsFamousAllDetail_CN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsFamousAllDetail.do?id=%s&lang=JP", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsFamousAllDetail_JP.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsDutyfreeAllDetail.do?id=%s&lang=KR", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsDutyfreeAllDetail_KR.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsDutyfreeAllDetail.do?id=%s&lang=EN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsDutyfreeAllDetail_EN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsDutyfreeAllDetail.do?id=%s&lang=CN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsDutyfreeAllDetail_CN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsDutyfreeAllDetail.do?id=%s&lang=JP", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsDutyfreeAllDetail_JP.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsRecentShot.do?id=%s&net=0", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsRecentShot.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsProofShotGallery.do?id=%s&net=0", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsProofShotGallery.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsRecommendAllMenu.do?id=%s&lang=KR", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsRecommendAllMenu_KR.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsRecommendAllMenu.do?id=%s&lang=EN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsRecommendAllMenu_EN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsRecommendAllMenu.do?id=%s&lang=CN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsRecommendAllMenu_CN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsRecommendAllMenu.do?id=%s&lang=JP", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsRecommendAllMenu_JP.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsStoreAllMenu.do?id=%s&lang=KR", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsStoreAllMenu_KR.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsStoreAllMenu.do?id=%s&lang=EN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsStoreAllMenu_EN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsStoreAllMenu.do?id=%s&lang=CN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsStoreAllMenu_CN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsStoreAllMenu.do?id=%s&lang=JP", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsStoreAllMenu_JP.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsStoreAllMenuDetail.do?id=%s&lang=KR", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsStoreAllMenuDetail_KR.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsStoreAllMenuDetail.do?id=%s&lang=EN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsStoreAllMenuDetail_EN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsStoreAllMenuDetail.do?id=%s&lang=CN", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsStoreAllMenuDetail_CN.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsStoreAllMenuDetail.do?id=%s&lang=JP", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsStoreAllMenuDetail_JP.xml") == ciFalse) return ciFalse;
	apiUrl.Format("/spc_cms/contentsStoreMovie.do?id=%s", _host.c_str());
	if (_getSpcContentsApi(apiUrl, "contentsStoreMovie.xml") == ciFalse) return ciFalse;

	return ciTrue;
}

ciBoolean
spcContentsTimer::_makeRealtimeContentsApiXml()
{
	ciDEBUG(3, ("_makeRealtimeContentsApiXml()"));

	CString apiUrl;

	return ciTrue;
}

ciBoolean
spcContentsTimer::_getSpcContents(const char* url, CString& sendMsg, CStringArray& line_list)
{
	ciDEBUG(3, ("_getSpcContents(%s,%s)", url,sendMsg));
	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _server.c_str(), _port);
	BOOL ret_val = http_request.RequestPost(url,sendMsg,line_list);
	if(!ret_val) {
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	}

	BOOL getSucceed = false;
	if(line_list.GetCount() > 1 )
	{
		CString first_line =  line_list.GetAt(0);
		if(first_line.GetLength() >= 2 && first_line.Mid(0,2).CompareNoCase("OK") == 0) 
		{
			getSucceed = true;
		}
	}

	if(!getSucceed){
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	}

	return ciTrue;
}

int
spcContentsTimer::_setSpcContents(CStringArray& line_list)
{
	ciDEBUG(3, ("_setSpcContents()"));

	//clearMap();
	
	int retval = 0;
	spcContentsData* aData = NULL;

	int count = line_list.GetCount();
	for(int i=0; i<count; i++)
	{
		ciString line = line_list.GetAt(i);

		ciString key, value;
		ciStringUtil::divide(line,'=',&key,&value);

		if(key == "zorder") {
			ciGuard aGuard(_mapLock);
			SpcContentsMAP::iterator itr =_map.find(atoi(value.c_str()));
			if(itr != _map.end()){
				aData = itr->second;
				ciDEBUG(1,("%d data founded", aData->zorder));
				retval++;
			}else{
				aData = new spcContentsData();
				aData->zorder = atoi(value.c_str());
				_map.insert(SpcContentsMAP::value_type(aData->zorder, aData));
				ciDEBUG(1,("%d data inserted", aData->zorder));
			}
			continue;
		}
		if(aData==0){
			continue;
		}

		if(key == "filename")
		{
			ciDEBUG(1,("filename=%s", value.c_str()));
			aData->filename = value;
		}
		else if(key == "volume")
		{
			unsigned long buf = strtoul(value.c_str(),NULL,10);
			if(aData->volume != buf) {
				ciDEBUG(1,("chanaged"));
				aData->volume = buf;
				aData->alreadyDownload = ciFalse;
				retval++;
			}
		}
	}

	printMap();
	return retval;
}

int
spcContentsTimer::_downloadContents()
{
	ciDEBUG(3, ("_downloadContents()"));

	int counter=0;
	CDownloader libDownloader;
	SpcContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		spcContentsData* aData = itr->second;
		aData->print();
		if(!aData->filename.empty() && (_isExist(aData->filename) == ciFalse || aData->alreadyDownload == ciFalse)) {
			if(libDownloader.GetFile(aData->filename.c_str(), aData->volume, "", _site.c_str(), "spc")) {
				unsigned long downloadedSize = _getFileSize(aData->filename.c_str());
				if(downloadedSize == aData->volume) {
					ciDEBUG(1,("spc/%s file download succeed", aData->filename.c_str()));
					aData->alreadyDownload = ciTrue;
					counter++;
				}else{
					ciERROR(("spc/%s file download failed (filesize(%ld) != downloadedSize(%ld)"
						, aData->filename.c_str(), aData->volume, downloadedSize));
				}
			}else{
				ciERROR(("spc/%s file download failed", aData->filename.c_str()));
			}
		}
	}
	ciDEBUG(1,("%d data downloaded", counter));
	return counter;
}

int
spcContentsTimer::_removeContents()
{
	ciDEBUG(3, ("_removeContents()"));

	int counter=0;

	CString contentsDir, filename;
	_makePath("Contents\\Enc\\spc", contentsDir);

	counter = _removeContents(counter, contentsDir, filename);

	ciDEBUG(1,("%d file removed", counter));
	return counter;
}

int
spcContentsTimer::_removeContents(int counter, CString filepath, CString filename)
{
	string dirPath = filepath;
	if (filename.IsEmpty() == false) {
		dirPath += "\\";
		dirPath += filename;
	}
	dirPath += "\\";
	dirPath += "*.*";

	WIN32_FIND_DATA FileData;
	HANDLE hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		ciERROR(("contents file not found"));
		return 0;
	}

	do {
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			if (FileData.cFileName[0] != '.') {
				ciString filePath = filename;
				if (filename.IsEmpty() == false) {
					filePath += "\\";
				}
				filePath += FileData.cFileName;

				counter = _removeContents(counter, filepath, filePath.c_str());
			}
			continue;
		}

		ciString filePath = filepath;
		if (filename.IsEmpty() == false) {
			filePath += "\\";
			filePath += filename;
		}
		filePath += "\\";
		filePath += FileData.cFileName;
		ciDEBUG(5,("%s file founded", filePath.c_str()));

		ciString contentsPath = "";
		if (filename.IsEmpty() == false) {
			contentsPath += filename;
			contentsPath += "\\";
		}
		contentsPath += FileData.cFileName;

		if (_isContents(contentsPath) == ciFalse) {
			if(::access(filePath.c_str(),0) == 0) {
				::remove(filePath.c_str());
				ciDEBUG(5,("%s file deleted", filePath.c_str()));
			}

			counter++;
		}

	} while (FindNextFile(hFile, &FileData));
	FindClose(hFile);

	return counter;
}

ciBoolean
spcContentsTimer::_isContents(ciString& filename)
{
	SpcContentsMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		spcContentsData* aData = itr->second;
		if (aData->filename == filename) {
			ciDEBUG(1,("contents %s exist", filename.c_str()));
			return ciTrue;
		}
	}
	ciDEBUG(1,("contents %s not exist", filename.c_str()));
	return ciFalse;
}

ciBoolean
spcContentsTimer::_isExist(ciString& filename)
{
	CString source;
	_makePath("Contents\\Enc\\spc\\", source);
	source += filename.c_str();
	if(::access(source,0) == 0) {
		ciDEBUG(1,("file %s exist", source));
		return ciTrue;
	}
	ciDEBUG(1,("file %s not exist", source));
	return ciFalse;
}

unsigned long
spcContentsTimer::_getFileSize(const char* filename)
{
	unsigned long retval = 0;

	CString fullpath;
	_makePath("Contents\\Enc\\spc\\", fullpath);
	fullpath += filename;

	ciDEBUG(1,("stat(%s)", fullpath));
	int fdes = open(fullpath,O_RDONLY);
	if(fdes<0){
		ciWARN(("%s file open failed", fullpath));
		return retval;
	}
	struct stat statBuf;
	if(fstat(fdes, &statBuf)==0){
		retval = statBuf.st_size;
	}
	ciDEBUG(1,("filesize(%s)=%ld", fullpath, retval));
	close(fdes);
	return retval;
}

void
spcContentsTimer::_makeHostXml()
{
	ciDEBUG(1,("_makeHostXml()"));

	FILE* fp = fopen(_hostFile.c_str(),"w");

	if(!fp) {
		ciWARN(("%s file open failed", _hostFile.c_str()));
		return;
	}

	fprintf(fp,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
	fprintf(fp,"<host>\r\n");
	fprintf(fp,"\t<id>%s</id>\r\n", _host.c_str());
	fprintf(fp,"\t<api>http://%s:%d/spc_cms</api>\r\n", _server.c_str(), _port);
	fprintf(fp,"</host>\r\n");
	fclose(fp);
}

ciBoolean
spcContentsTimer::_writeXmlFile(ciString& filename, CStringArray& line_list)
{
	ciDEBUG(3, ("_writeXmlFile(%s)", filename.c_str()));

	FILE* fp = fopen(filename.c_str(),"w");

	if(!fp) {
		ciWARN(("%s file open failed", filename.c_str()));
		return ciFalse;
	}
	int counter=0;
	int count = line_list.GetCount();
	for(int i=0; i<count; i++)
	{
		ciString line = line_list.GetAt(i);
		fputs(line.c_str(),fp);
		fputs("\n",fp);
	}
	fclose(fp);
	return ciTrue;
}

ciBoolean
spcContentsTimer::_getSpcContentsApi(const char* url, CStringArray& line_list)
{
	ciDEBUG(3, ("_getSpcContentsApi(%s)", url));
	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _server.c_str(), _port);
	BOOL ret_val = http_request.RequestGet(url, line_list);
	if(!ret_val) {
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	}
	
	BOOL getSucceed = false;
	if(line_list.GetCount() > 1 )
	{
		getSucceed = true;
	}

	if(!getSucceed){
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	}

	return ciTrue;
}

ciBoolean
spcContentsTimer::_getSpcContentsApi(const char* jsp, const char* filename)
{
	ciDEBUG(1,("_getSpcContentsApi(%s,%s)", jsp, filename));

	ciDirHandler::create(_apiFolder);

	ciString fullpath = _apiFolder;
	fullpath += "\\";
	fullpath += filename;

	CStringArray line_list;
	if(!_getSpcContentsApi(jsp, line_list)){
		ciERROR(("%s jsp call failed", jsp));
		return ciFalse;
	}
	if(!_writeXmlFile(fullpath, line_list)) {
		ciERROR(("%s file write failed", fullpath.c_str()));
		return ciFalse;
	}

	ciDEBUG(1,("%s file succeed", fullpath.c_str()));
	return ciTrue;
}
