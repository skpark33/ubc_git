#include "stdafx.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchUtil.h"
#include "autoUpdateTimer.h"

ciSET_DEBUG(9, "autoUpdateTimer");


autoUpdateTimer::autoUpdateTimer() 
: _interval(0)
{
	ciDEBUG(3, ("autoUpdateTimer()"));
	ciTime now;

	char nowStr[10];
	memset(nowStr,0x00,10);
	sprintf(nowStr, "%02d:%02d", now.getHour(), now.getMinute());

	_lastCheckTime = nowStr;

}

autoUpdateTimer::~autoUpdateTimer() {
	ciDEBUG(3, ("~autoUpdateTimer()"));
}

void 
autoUpdateTimer::initTimer(const char* pname, int interval) {
	ciDEBUG(3, ("initTimer(%s,%d)", pname, interval));
	_interval = interval;
	_timer = new ciTimer(pname, this);
	_timer->setInterval(interval);
}

void
autoUpdateTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(3, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	ciTime now;

	char nowStr[10];
	memset(nowStr,0x00,10);
	sprintf(nowStr, "%02d:%02d", now.getHour(), now.getMinute());

	ciDEBUG(3, ("processExpired(%s,%s,%d,%d)", nowStr, name.c_str(), counter, interval));

	_getContentsDownloadTime();
	if(this->_contentsDownloadTime.empty()){
		ciDEBUG(1,("ContentsDownloadTime does not set"));
		_lastCheckTime = nowStr;
		return;
	}


	if(	_lastCheckTime < _contentsDownloadTime && _contentsDownloadTime <= nowStr ){
		ciDEBUG(1,("AutoUpdateTime(%s == %s)", _contentsDownloadTime.c_str(), nowStr));
		// 2012.7.18 skpark
		// AutoUpdateFlag 값이 0 이 아닐때만 하도록 수정
		ubcConfig aIni("UBCVariables");
		ciString buf;
		aIni.get("ROOT","AutoUpdateFlag", buf,"1");
		ciShort autoValue = atoi(buf.c_str());
		if(autoValue){
			autoUpdate();
		}
	}else{
		ciDEBUG(1,("Not AutoUpdateTime(%s != %s)", _contentsDownloadTime.c_str(), nowStr));
	}

	_lastCheckTime = nowStr;
	return;
}



ciBoolean
autoUpdateTimer::autoUpdate()
{
	ciDEBUG(1,("autoUpdate"));
	HWND sttHwnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
	if (!sttHwnd) {
		ciWARN(("STT Window Handle is not found"));
		ciString command = "SelfAutoUpdate.exe";
		ciString arg=" +log +ignore_env +update_only +firmwareView";
		//if(system(command.c_str())==0){
		if(scratchUtil::getInstance()->createProcess(command.c_str(), arg.c_str(),0,ciFalse)){
			ciDEBUG(1,("%s %s succeed", command.c_str(), arg.c_str()));
			return ciTrue;
		}
		ciERROR(("%s %s fail", command.c_str(), arg.c_str()));
		return false;
	}
	//::SendMessage(sttHwnd, WM_SHOWWINDOW, 5, 0);
	//::ShowWindow(sttHwnd, SW_SHOWNORMAL);
	//scratchUtil::getInstance()->getFocus(sttHwnd);

	COPYDATASTRUCT appInfo;
	appInfo.dwData = 600;  // AutoUpdate
	appInfo.lpData = (char*)"";
	appInfo.cbData = 1;
	::SendMessage(sttHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
	ciDEBUG(1,("FV_UPDATE succeed"));
	return ciTrue;
}

ciBoolean	
autoUpdateTimer::_getContentsDownloadTime()
{
	ubcConfig aIni("UBCVariables");
	if (!aIni.get("ROOT","ContentsDownloadTime", _contentsDownloadTime)) 
	{
		ciWARN(("[ROOT]ContentsDownloadTime get failed."));
		// Default Value
		_contentsDownloadTime="";
		return ciFalse;
	}
	if (_contentsDownloadTime.empty() || _contentsDownloadTime == "NOSET" ||
		_contentsDownloadTime.size() != 5 || _contentsDownloadTime[2] != ':') {
		ciWARN(("invalid ContentsDownloadTime"));
		_contentsDownloadTime="";
		return ciFalse;
	}
	ciDEBUG(9, ("[ROOT]ContentsDownloadTime is %s",_contentsDownloadTime.c_str()));
	return ciTrue;
}