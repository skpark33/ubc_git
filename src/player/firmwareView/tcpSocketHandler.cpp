/* Copyright . 2001 WINCC Inc.
 *	All Rights Reserved.
 *
 *	This source code is confidential and proprietary and may not be used
 *	or distributed without the written permission of WINCC Inc.
 *
 *	Created by :  wegf
 *	Modified by : 
 *	Last update : 2001/04/18 16:23:48
 *	Comment :
 */
//#include "stdafx.h"
#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciDebug.h>

#include <stdlib.h>
//#include <sys/time.h>
#include "tcpSocketHandler.h"
#include "common/libScratch/scratchUtil.h"

ciSET_DEBUG(10, "tcpSocketHandler");

tcpSocketHandler::tcpSocketHandler (void)
{
	ciDEBUG(1,("construct tcpSocketHandler 1"));
	
}

tcpSocketHandler::~tcpSocketHandler ()
{
	ciDEBUG(1,("tcpSocketHandler's instance is deleting now..."));
}

int
tcpSocketHandler::open (void *)
{
	ciDEBUG(1,("open()"));
	if (this->activate(THR_NEW_LWP|THR_DETACHED) == -1) {
		ciERROR(("open() - this->activate() for handle(%d) is FAILED", this->get_handle()));
		return -1;
	}
	return 0;
}

int
tcpSocketHandler::close (u_long p_longval)
{
	ciDEBUG(1,("close(%ld) - "
			"This Thread is closed for handle(%d) and threadd ",
			p_longval, this->get_handle() ));
	return inherited::close(p_longval);
}

int
tcpSocketHandler::svc()
{
	ciDEBUG(1,("svc()"));
	return 0;
}

int
tcpSocketHandler::handle_close(ACE_HANDLE pHandle, ACE_Reactor_Mask pMask)
{
	ciDEBUG(1,("handle_close(HANDLE:%d)", pHandle));

	return inherited::handle_close(pHandle, pMask);
}

