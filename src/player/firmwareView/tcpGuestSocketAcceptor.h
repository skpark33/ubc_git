#ifndef _tcpGuestSocketAcceptor_h_
#define _tcpGuestSocketAcceptor_h_

#include <ace/SOCK_Acceptor.h>
#include <ace/Acceptor.h>
#include "tcpGuestSocketSession.h"



class tcpGuestSocketAcceptor : public ACE_Acceptor<tcpGuestSocketSession, ACE_SOCK_ACCEPTOR> {
public:
	tcpGuestSocketAcceptor();
	virtual ~tcpGuestSocketAcceptor();
		

    virtual int make_svc_handler(tcpGuestSocketSession*& pSvcHandler);
    virtual int activate_svc_handler(tcpGuestSocketSession* pSvcHandler);

protected:
	typedef ACE_Acceptor<tcpGuestSocketSession, ACE_SOCK_ACCEPTOR> inherited;
};

#endif
