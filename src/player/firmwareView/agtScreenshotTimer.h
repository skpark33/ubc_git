#ifndef _agtScreenshotTimer_H_
#define _agtScreenshotTimer_H_

#include <ci/libTimer/ciTimer.h>

class agtScreenshotTimer : public ciPollable {
protected:
	int _interval;

public:
	agtScreenshotTimer(const char* hostId, int screenshotPeriod);
	virtual ~agtScreenshotTimer();

	virtual void initTimer(const char* pname, int interval = 60);
	virtual void processExpired(ciString name, int counter, int interval);

	void setScreenShotFtp(const char* ip, const char* user, const char* password, ciUShort port);
	ciBoolean screenshot(const char* fileName = "",	const char* ftpPath = "");
	ciBoolean deleteScreenShotFiles();
	ciBoolean setLastScreenShotFile(const char* fileName);
	int getUploadTargetFiles(ciStringList& targetList);
	ciBoolean changeSucceedFileName(const char* strFileName);

protected:

	string		_curTimeStr();
	string		_curTimeStrEx();
	
	ciString _hostId;
	ciString _ssFtpIp;
	ciString _ssFtpUser;
	ciString _ssFtpPassword;
	ciUShort _ssFtpPort;
	int		_screenshotPeriod;

	ciUShort _dayLimit;
};

#endif //_agtScreenshotTimer_H_
