#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciAceType.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libFile/ciFile.h>
#include <cci/libValue/cciEntity.h>
#include "libAGTClient/AGTClientSession.h"
#include <ci/libBase/ciStringTokenizer.h>
#include <common/libInstall/installUtil.h>
#include <common/libScratch/scratchUtil.h>
#include "tcpGuestSocketSession.h"

#if defined (_COP_MSC_)
     #include <windows.h>
     #include <atltime.h>
#endif#


ciSET_DEBUG(10,"tcpGuestSocketSession");

//GUEST_BOOK|NAME=[신랑_신부]|SIZE=[그림파일바이트수] \n\0
#define BUF_LEN 128
#define KEYSTR "GUEST_BOOK|NAME="
#define DELSTR "GUEST_BOOK|DELE="
#define SIZESTR "SIZE="
#define JPGKEY "JPG="
#define GUESTBOOK_PATH  _COP_CD("C:\\SQISoft\\Contents\\Enc\\Plugins\\Wedding\\GuestBook")
#define GUESTBOOK_DATA_PATH  _COP_CD("C:\\SQISoft\\Contents\\Enc\\Plugins\\data")


/*------ Base64 Encoding Table ------*/
static const char MimeBase64[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '+', '/'
};

/*------ Base64 Decoding Table ------*/
static int DecodeMimeBase64[256] = {
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 00-0F */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 10-1F */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,-1,-1,-1,63,  /* 20-2F */
    52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,  /* 30-3F */
    -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,  /* 40-4F */
    15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,  /* 50-5F */
    -1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,  /* 60-6F */
    41,42,43,44,45,46,47,48,49,50,51,-1,-1,-1,-1,-1,  /* 70-7F */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 80-8F */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* 90-9F */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* A0-AF */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* B0-BF */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* C0-CF */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* D0-DF */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,  /* E0-EF */
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1   /* F0-FF */
    };

int base64_decode(char *text, unsigned char *dst, int numBytes )
{
  const char* cp;
  int space_idx = 0, phase;
  int d, prev_d = 0;
  unsigned char c;

    space_idx = 0;
    phase = 0;

    for ( cp = text; *cp != '\0'; ++cp ) {
        d = DecodeMimeBase64[(int) *cp];
        if ( d != -1 ) {
            switch ( phase ) {
                case 0:
                    ++phase;
                    break;
                case 1:
                    c = ( ( prev_d << 2 ) | ( ( d & 0x30 ) >> 4 ) );
                    if ( space_idx < numBytes )
                        dst[space_idx++] = c;
                    ++phase;
                    break;
                case 2:
                    c = ( ( ( prev_d & 0xf ) << 4 ) | ( ( d & 0x3c ) >> 2 ) );
                    if ( space_idx < numBytes )
                        dst[space_idx++] = c;
                    ++phase;
                    break;
                case 3:
                    c = ( ( ( prev_d & 0x03 ) << 6 ) | d );
                    if ( space_idx < numBytes )
                        dst[space_idx++] = c;
                    phase = 0;
                    break;
				}
           prev_d = d;
       }else{
			printf("Unknown packet : %c\n", *cp);
		}

    }

    return space_idx;

}

int base64_encode(char *text, int numBytes, char **encodedText)
{
  unsigned char input[3]  = {0,0,0};
  unsigned char output[4] = {0,0,0,0};
  int   index, i, j, size;
  char *p, *plen;

  plen           = text + numBytes - 1;
  size           = (4 * (numBytes / 3)) + (numBytes % 3? 4 : 0) + 1;
  (*encodedText) = (char*)malloc(size);
  j              = 0;

    for  (i = 0, p = text;p <= plen; i++, p++) {
        index = i % 3;
        input[index] = *p;

        if (index == 2 || p == plen) {
            output[0] = ((input[0] & 0xFC) >> 2);
            output[1] = ((input[0] & 0x3) << 4) | ((input[1] & 0xF0) >> 4);
            output[2] = ((input[1] & 0xF) << 2) | ((input[2] & 0xC0) >> 6);
            output[3] = (input[2] & 0x3F);

            (*encodedText)[j++] = MimeBase64[output[0]];
            (*encodedText)[j++] = MimeBase64[output[1]];
            (*encodedText)[j++] = index == 0? '=' : MimeBase64[output[2]];
            (*encodedText)[j++] = index <  2? '=' : MimeBase64[output[3]];

            input[0] = input[1] = input[2] = 0;
        }
    }

    (*encodedText)[j] = '\0';

    return 0;
}

int decodeToByteArray(const char* data, unsigned char* output)
{
	// Initialise output ByteArray for decoded data
	//var output:ByteArray = new ByteArray();

	// Create data and output buffers
	//var dataBuffer:Array=new Array(4);
	//var outputBuffer:Array=new Array(3);

	string BASE64_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

	unsigned int dataBuffer[4];
	unsigned int outputBuffer[3];

	int data_length = strlen(data);
	int nBytes = 0;

	// While there are data bytes left to be processed
	for (int i = 0; i < data_length; i += 4)
	{
		// Populate data buffer with position of Base64 characters for
		// next 4 bytes from encoded data
		for (int j = 0; j < 4 && i + j < data_length; j++)
		{
			dataBuffer[j]=BASE64_CHARS.find(data[i+j]);
		}

		// Decode data buffer back into bytes
		outputBuffer[0] = (dataBuffer[0] << 2) + ((dataBuffer[1] & 0x30) >> 4);
		outputBuffer[1] = ((dataBuffer[1] & 0x0f) << 4) + ((dataBuffer[2] & 0x3c) >> 2);
		outputBuffer[2] = ((dataBuffer[2] & 0x03) << 6) + dataBuffer[3];

		// Add all non-padded bytes in output buffer to decoded data
		for (int k= 0; k < 3; k++)
		{
			if (dataBuffer[k+1]==64)
			{
				printf("%d_", nBytes);
				break;
			}
			output[nBytes++] = outputBuffer[k];
		}
	}

	// Return decoded data
	return nBytes;
}



tcpGuestSocketSession::tcpGuestSocketSession() {
	ciDEBUG(1,("tcpGuestSocketSession"));
}

tcpGuestSocketSession::~tcpGuestSocketSession() {
	ciDEBUG(1,("~tcpGuestSocketSession"));
}

int
tcpGuestSocketSession::svc() {
	ciDEBUG(1,("svc()"));

	// 20151026 skpark testcode TEST Code for OSF
	HWND socketReceiverWnd = scratchUtil::getInstance()->getWHandle("SocketReceiver.exe");

	ciBoolean send = ciFalse;
	int filesize = 0;
	ciString filename = "";
	int keyLen = strlen(KEYSTR);
	int delLen = strlen(DELSTR);
	int seq = 0;
	while(1) {
		ciString command="";
		if(!readline(command)){
			ciWARN(("recv error"));
			break;
		}
		if(command.empty()){
			ciDEBUG(1,("socket broken"));
			break;
		}
		if(command.substr(0,delLen) == DELSTR){
			filename = "";
			filesize = 0;
			_getFileInfo(command.substr(delLen).c_str(),filename,filesize);
			if(filename.empty() || filesize == 0){
				ciERROR(("Somthing wrong! filename or filesize is Empty"));
				writeline("NO|filename or filesize is Empty");
				break;
			}
			this->deleteFile(filename.c_str(), filesize);
		}else if(command.substr(0,keyLen) == KEYSTR){
			filename = "";
			filesize = 0;
			_getFileInfo(command.substr(keyLen).c_str(),filename,filesize);

			if(filename.empty() || filesize == 0){
				ciERROR(("Somthing wrong! filename or filesize is Empty"));
				writeline("NO|filename or filesize is Empty");
				break;
			}
			int readSize = readAndWrite(filename.c_str(), filesize, ++seq);
			if(filesize != readSize) {
				ciERROR(("read and write Error"));
				writeline("NO|read or write jpg file error");
				break;
			}
			this->deleteOldFile(filename.c_str(),60,105);
		}

		if(send == ciFalse) {
			this->writeXML(filename.c_str());
			if(!writeline("OK")){
				ciDEBUG(1,("send error"));
				break;
			}
		}
	} // receive loop while

	ciDEBUG(1,("Client : connection close....\n"));
	return 0;
}
ciBoolean
tcpGuestSocketSession::_getFileInfo(const char* command, ciString& filename, int& filesize)
{
	//[신랑_신부]|SIZE=[그림파일바이트수] 
	ciDEBUG(1,("getFileInfo(%s)", command));

	string filesize_str;
	boolean filename_retreived = false;
	
	int len=strlen(command);
	for(int i=0;i<len;i++){
		if(command[i] == '|') {
			filename_retreived = true;
			continue;
		}
		if(filename_retreived){
			filesize_str+=command[i];
		}else{
			filename+=command[i];
		}
	}
	ciDEBUG(1,("filename=(%s),filesize_str=(%s)", filename.c_str(),filesize_str.c_str()));

	if(filesize_str.size() < strlen(SIZESTR+1)){
		ciERROR(("data format something wrong"));
		return ciFalse;
	}
	
	filesize = atoi(filesize_str.substr(strlen(SIZESTR)).c_str());
	ciDEBUG(1,("filename=(%s),filesize=(%d)", filename.c_str(),filesize));
	return ciTrue;

}

int
tcpGuestSocketSession::deleteOldFile(const char* filename, int duration, int maximumFileCount)
{
	ciDEBUG(1,("deleteOldFile(%s,%d,%d)", filename, duration,maximumFileCount));

	string subDir = GUESTBOOK_PATH;
	subDir += 	"\\";
	subDir += 	filename;

	string dirPath = subDir;
	dirPath += "\\*.png";

	ciDEBUG(5,("guestbook files=%s", dirPath.c_str()));

	ciStringMap remainFiles;
	CTime referTime = CTime::GetCurrentTime();
	CTimeSpan spanTime(duration, 0, 0, 0);
	referTime = referTime - spanTime; // 90일이전
	int deleted_counter=0;

	HANDLE hFile = NULL;
	WIN32_FIND_DATA FileData;
	hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		ciERROR(("guestbook file not found"));
		return ciFalse;
	}

	do {
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			continue;
		}
		if (strstr(FileData.cFileName, "log") || strstr(FileData.cFileName,"no_shot")) {
			continue;
		}

		ciString delFilePath = subDir;
		delFilePath += "\\";
		delFilePath += FileData.cFileName;

		ciDEBUG(1,("FileData.cFileName = %s",  FileData.cFileName));
		ciDEBUG(1,("delFilePath = %s", delFilePath.c_str()));

		CTime fileTime(FileData.ftLastWriteTime);
		if (fileTime < referTime) {
			if(this->deleteFile(delFilePath.c_str())){
				deleted_counter++;
				continue;
			}
		}
		char key[521];
		memset(key,0x00,512);
		sprintf(key,"%ld\\%s",unsigned long(fileTime.GetTime()), delFilePath.c_str());
		remainFiles.insert(ciStringMap::value_type(key, delFilePath.c_str()));
		
	} while (FindNextFile(hFile, &FileData));
	::CloseHandle(hFile);

	int len = remainFiles.size();
	ciStringMap::iterator itr;
	for(itr=remainFiles.begin();itr!=remainFiles.end();itr++){
		ciDEBUG(1,("remain files %s", itr->first.c_str()));
	}
	ciDEBUG(1,("remain files total = %d", len));


	if(len > maximumFileCount) {
		ciStringMap::iterator itr;
		for(itr=remainFiles.begin();itr!=remainFiles.end();itr++){
			if((len-deleted_counter) > maximumFileCount){
				ciDEBUG(1,("Number of File(%d) is over %d",len-deleted_counter, maximumFileCount));
				if(this->deleteFile(itr->second.c_str())){
					deleted_counter++;
				}
			}

		}
	}

	ciDEBUG(5,("%d file deleted", deleted_counter));
	return deleted_counter;
}

ciBoolean 
tcpGuestSocketSession::deleteFile(const char* delFilePath)
{
	ciString command = "attrib -R ";
	command += delFilePath;
	if(system(command.c_str())==0){
		ciDEBUG(1,("%s file read only attributes removed", command.c_str()));
	}else{
		ciERROR(("%s file read only attributes removed failed", command.c_str()));
	}

	ciBoolean retval = ::DeleteFile(delFilePath);
	if(retval){
		ciDEBUG(5,("%s file deleted succeed", delFilePath));
	}else{
		ciERROR(("%s file delete failed", delFilePath));
	}
	return retval;
}

int
tcpGuestSocketSession::writeXML(const char* filename)
{
	ciDEBUG(1,("writeXML(%s)", filename));
	string xml = GUESTBOOK_DATA_PATH;
	xml += 	"\\";
	xml += 	filename;
	xml += 	"list.xml";

	ofstream out(xml.c_str());
	if(out.fail()){
		ciERROR(("%s  open error", xml.c_str()));
		return 0;
	}
	ciString _xmlBuf = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	_xmlBuf += "\n<list>";

	string subDir = GUESTBOOK_PATH;
	subDir += 	"\\";
	subDir += 	filename;

	string dirPath = subDir;
	dirPath += "\\*.png";

	ciDEBUG(5,("guestbook files=%s", dirPath.c_str()));

	HANDLE hFile = NULL;
	WIN32_FIND_DATA FileData;
	hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		ciERROR(("guestbook file not found"));
		return 0;
	}

	do {
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			continue;
		}
		if (strstr(FileData.cFileName, "log") || strstr(FileData.cFileName,"no_shot")) {
			continue;
		}
		_xmlBuf += "\n\t<filename>";
		_xmlBuf += FileData.cFileName;
		_xmlBuf += "</filename>";
	} while (FindNextFile(hFile, &FileData));
	_xmlBuf += "\n</list>";

	out.write(_xmlBuf.c_str(), _xmlBuf.size());
	if(out.fail()){
		ciERROR(("%s tmpFile write error", xml.c_str()));
		out.close();
		return 0;
	}
	ciDEBUG(1,("writeXML(%s) succeed", filename));
	out.close();
	return 1;
}

int
tcpGuestSocketSession::readAndWrite(const char* filename, int filesize, int seq)
{

	ciDEBUG(1,("readAndWrite(%s,%d,%d)", filename, filesize,seq));

	ciGuard aGuard(_sockLock);

	int len = strlen(JPGKEY);
	char header[10];
	memset(header, 0x00, sizeof(header));
	int nread = peer().recv_n(header,len);
	if(nread != len || strcmp(header,JPGKEY)!=0){
		ciERROR(("JPG Header error"));
		return 0;
	}

	ciString receivedStr="";
	ciBoolean eof = ciFalse;
	for(int i=0;i<filesize;i++) {
		char recvBuf[2];
		memset(recvBuf, 0x00, sizeof(recvBuf));
		int nread = peer().recv_n(recvBuf,1);
		if(nread < 0) {
			ciERROR(("socket read error"));
			break;
		}
		if(nread == 0) {
			ciWARN(("unexpected socket EOF"));
			break;
		}
		receivedStr += recvBuf;
	}

	int srcLen = receivedStr.size();
	if(srcLen==0){
		ciERROR(("read failed"));
		return 0;
	}

	if(srcLen!=filesize){
		ciERROR(("read failed (%d != %d)", srcLen, filesize));
		return 0;
	}

	if(srcLen%4 != 0){
		ciWARN(("Something Wiered srcLen%%4 != 0"));
	}

	char path[256];
	sprintf(path,"%s\\%s", GUESTBOOK_PATH, filename);

	scratchUtil::getInstance()->createDir(path);

	char fullpath[256];
	ciTime now;
	sprintf(fullpath,"%s\\guestbook%04d%02d%02d%02d%02d%02d_%d.png"
		, path
		, now.getYear()
		, now.getMonth()
		, now.getDay()
		, now.getHour()
		, now.getMinute()
		, now.getSecond()
		,seq);

	//setlocale(LC_ALL,"Korean"); // 한글파일명도 열기위해

	int fdes = ::open(fullpath,O_CREAT|O_WRONLY|O_BINARY);
	//	ofstream out(fullpath);
	//if(out.fail()){
	if(fdes<0) {
		ciERROR(("%s file open error", fullpath));
		return 0;
	}
	
	unsigned char* decoded = new unsigned char[srcLen+1];
	memset(decoded,0x00,srcLen);
	//int dstLen = base64_decode((char*)receivedStr.c_str(),decoded,srcLen);
	int dstLen = decodeToByteArray((char*)receivedStr.c_str(),decoded);

	ciDEBUG(1,("srcLen=%ld, dstLen=%ld", srcLen, dstLen));
	if(dstLen==0){
		ciWARN(("decoding failed !!!"));
	}else {
		if(srcLen*0.75 != dstLen){
			ciWARN(("Something wierd srcLen*0.75 != dstLen !!!"));
		}
		//for(int j=0;j<dstLen;j++){
		//	out << decoded[j];
		//}
		// 조금씩 쪼개서 써볼것
		// 
		unsigned char* ptr = decoded;
		for(int k=0;k<dstLen;k++){
			//printf("%02x", *ptr);
			::write(fdes,ptr,1);
			ptr++;
		}
		// 1.  EUC-KR 로 보낼때와, UTF-8 로 보낼때, 원본이 다른지를 비교해볼것
		// 2.  다른 머신에서 한번 해볼것.
		//::write(fdes,receivedStr.c_str(),srcLen);
	}
	delete decoded;
	//out.close();
	::close(fdes);
	return srcLen;
}

int
tcpGuestSocketSession::deleteFile(const char* filename, int filesize)
{
	ciDEBUG(1,("deleteFile(%s,%d)", filename, filesize));

	ciGuard aGuard(_sockLock);

	int len = strlen(JPGKEY);
	char header[10];
	memset(header, 0x00, sizeof(header));
	int nread = peer().recv_n(header,len);
	if(nread != len || strcmp(header,JPGKEY)!=0){
		ciERROR(("JPG Header error"));
		return 0;
	}

	ciString receivedStr="";
	ciBoolean eof = ciFalse;
	for(int i=0;i<filesize;i++) {
		char recvBuf[2];
		memset(recvBuf, 0x00, sizeof(recvBuf));
		int nread = peer().recv_n(recvBuf,1);
		if(nread < 0) {
			ciERROR(("socket read error"));
			break;
		}
		if(nread == 0) {
			ciWARN(("unexpected socket EOF"));
			break;
		}
		receivedStr += recvBuf;
	}

	int srcLen = receivedStr.size();
	if(srcLen==0){
		ciERROR(("read failed"));
		return 0;
	}

	if(srcLen!=filesize){
		ciERROR(("read failed (%d != %d)", srcLen, filesize));
		return 0;
	}

	char path[256];
	sprintf(path,"%s\\%s", GUESTBOOK_PATH, filename);

	char fullpath[256];
	sprintf(fullpath,"%s\\%s", path, receivedStr.c_str());

	//DWORD attrib = ::GetFileAttributes(fullpath);
	//attrib = FILE_ATTRIBUTE_NORMAL;
	//::SetFileAttributes(fullpath,attrib);
	ciString command = "attrib -R ";
	command += fullpath;
	if(system(command.c_str())==0){
		ciDEBUG(1,("%s file read only attributes removed", command.c_str()));
	}else{
		ciERROR(("%s file read only attributes removed failed", command.c_str()));
	}
	//ciString backFile = fullpath;
	//backFile += ".bak";
	//::MoveFile(fullpath, backFile.c_str());
	ciBoolean retval =  ::DeleteFile(fullpath);
	ciDEBUG(1,("%s file removed(%d)", fullpath, retval));
	//ciBoolean retval =  ::DeleteFile(backFile.c_str());
	return srcLen;
}


ciBoolean
tcpGuestSocketSession::readline(ciString& outVal)
{
	ciDEBUG(1,("readline()"));

	ciGuard aGuard(_sockLock);

	ciBoolean eof = ciFalse;

	while(1) {
		char recvBuf[2];
		memset(recvBuf, 0x00, sizeof(recvBuf));
		int nread = peer().recv_n(recvBuf,1);
		if(nread < 0) {
			ciERROR(("socket read error"));
			return ciFalse;
		}
		if(nread == 0) {
			ciWARN(("unexpected socket EOF"));
			return ciFalse;
		}
		if(!eof && (recvBuf[0] == '\n')) {  // '0'
			eof = ciTrue;
			ciDEBUG(1,("tcpGuestSocketSession received(%s)",outVal.c_str()));
			return ciTrue;
		}
		outVal += recvBuf;
	}
	return ciTrue;
}


ciBoolean
tcpGuestSocketSession::writeline(const char* inVal)
{
	ciString buf = inVal;
	return writeline(buf);
}

ciBoolean
tcpGuestSocketSession::writeline(ciString& inVal)
{
	ciDEBUG(1,("Write to socket(%s)", inVal.c_str()));

	ciGuard aGuard(_sockLock);

	inVal += "\0";

	int aSendLen = peer().send_n(inVal.c_str(), (int)inVal.length()+1);	
	if (aSendLen <= 0) {
			ciERROR(("socket() write error"));
			return ciFalse;
	}
	ciDEBUG(1,("tcpGuestSocketSession (%s) sent", inVal.c_str()));
	return ciTrue;
}


