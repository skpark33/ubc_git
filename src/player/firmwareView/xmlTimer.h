#ifndef _xmlTimer_H_
#define _xmlTimer_H_

#include <ci/libTimer/ciTimer.h>

class xmlTimer : public ciPollable {
public:
	xmlTimer(const char* siteId, const char* hostId);
	virtual ~xmlTimer();

	virtual void processExpired(ciString name, int counter, int interval);
	void setXMLMap(ciStringMap& p, unsigned long port, ciBoolean useSSL, const char* serverIP="") ;

protected:

	bool _writeXML(ciString& filename, CString& text);
	bool _writeAnnounce(CString& text);
	void _sendMessage(const char* announceId,ciBoolean isStart);
	void _sendMessage(HWND brwHwnd, const char* announceId, ciBoolean isStart);
	bool _getBraceValue(ciString& target,const char* lb, const char* rb);

	ciStringMap	_xmlMap;
	unsigned long _port;
	ciBoolean _useSSL;
	ciString _serverIP;
	ciString _hostId;
	ciString _siteId;

	CString _prevText;
};

#endif //_xmlTimer_H_
