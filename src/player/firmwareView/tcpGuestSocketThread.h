 /*! \file tcpGuestSocketThread.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _tcpGuestSocketThread_h_
#define _tcpGuestSocketThread_h_

#include "tcpGuestSocketAcceptor.h"
#include <ci/libThread/ciThread.h> 

class tcpGuestSocketThread : public ciThread {
public:
	tcpGuestSocketThread();
	virtual ~tcpGuestSocketThread();

	virtual void run();
 	
	void guest_close();
	ciBoolean guest_acceptor();

protected:	

	tcpGuestSocketAcceptor	_guest_acceptor;
	ciBoolean guest_accept();


};
		
#endif //_tcpGuestSocketThread_h_
