#include <ci/libDebug/ciDebug.h>
#include "tcpGuestSocketAcceptor.h"
#include "common/libScratch/scratchUtil.h"

ciSET_DEBUG(10, "tcpGuestSocketAcceptor");

tcpGuestSocketAcceptor::tcpGuestSocketAcceptor() {
	
}

tcpGuestSocketAcceptor::~tcpGuestSocketAcceptor() {
}

int
tcpGuestSocketAcceptor::make_svc_handler(tcpGuestSocketSession*& pSvcHandler)
{
    ciDEBUG(1,("make_svc_handler(SvcHandler)"));

    return inherited::make_svc_handler(pSvcHandler);
}

int
tcpGuestSocketAcceptor::activate_svc_handler(tcpGuestSocketSession* pSvcHandler)
{
    ciDEBUG(1,("activate_svc_handler(SvcHandler)"));

	return inherited::activate_svc_handler(pSvcHandler);
}