#ifndef _reloadEventHandler_h_
#define _reloadEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

class CDownloader;

class reloadEventHandler : public cciEventHandler {
public:
	reloadEventHandler(const char* siteId, const char* hostId, const char* appPath) 
		{ _siteId =siteId; _hostId = hostId;_appPath = appPath; _libDownloader = 0; monitorOn=0;monitorOff=0;
			_fileTimeCheck=false;} // //skpark same_size_file_problem
	virtual ~reloadEventHandler();
   	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean		addHand();
	void			setSiteId(const char* siteId) { _siteId = siteId; }
	void			setFileTimeCheck(bool b) { _fileTimeCheck = b; } //skpark same_size_file_problem

protected:
	ciBoolean	_isAlreadyDone(const char* requestedProgramId, ciULong requestedTime, int side, 
								ciULong startTime);
	//ciULong _eventKey;
	//ciMutex	_eventKeyLock;

	ciString _siteId;
	ciString _hostId;
	ciString _appPath;

	CDownloader* _libDownloader;

	bool monitorOn;
	bool monitorOff;

	void	getMonitorOnOff(const char* programId);

	bool	_fileTimeCheck;  //skpark same_size_file_problem
};


#endif // _reloadEventHandler_h_
