/* Copyright . 2001 WINCC Inc.
 *	All Rights Reserved.
 *
 *	This source code is confidential and proprietary and may not be used
 *	or distributed without the written permission of WINCC Inc.
 *
 *	Created by :  wegf
 *	Modified by : 
 *	Last update : 2001/04/18 16:23:48
 *	Comment :
 */
#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciDebug.h>

#include <stdlib.h>
//#include <sys/time.h>
#include "tcpGuestSocketHandler.h"
#include "common/libScratch/scratchUtil.h"

ciSET_DEBUG(10, "tcpGuestSocketHandler");

tcpGuestSocketHandler::tcpGuestSocketHandler (void)
{
	ciDEBUG(1,("construct tcpGuestSocketHandler 1"));
	
}

tcpGuestSocketHandler::~tcpGuestSocketHandler ()
{
	ciDEBUG(1,("tcpGuestSocketHandler's instance is deleting now..."));
}

int
tcpGuestSocketHandler::open (void *)
{
	ciDEBUG(1,("open()"));
	if (this->activate(THR_NEW_LWP|THR_DETACHED) == -1) {
		ciERROR(("open() - this->activate() for handle(%d) is FAILED", this->get_handle()));
		return -1;
	}
	return 0;
}

int
tcpGuestSocketHandler::close (u_long p_longval)
{
	ciDEBUG(1,("close(%ld) - "
			"This Thread is closed for handle(%d) and threadd ",
			p_longval, this->get_handle() ));
	return inherited::close(p_longval);
}

int
tcpGuestSocketHandler::svc()
{
	ciDEBUG(1,("svc()"));
	return 0;
}

int
tcpGuestSocketHandler::handle_close(ACE_HANDLE pHandle, ACE_Reactor_Mask pMask)
{
	ciDEBUG(1,("handle_close(HANDLE:%d)", pHandle));

	return inherited::handle_close(pHandle, pMask);
}

