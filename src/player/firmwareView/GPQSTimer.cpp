#include "stdafx.h"
#include "libDownload/Downloader.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciTime.h>
#include <ci/libFile/ciDirHandler.h>
#include "GPQSTimer.h"
#include "common/libHttpRequest/HttpRequest.h"
#include "common/libInstall/installUtil.h"
#include <common/libScratch/scratchUtil.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libDebug/ciArgParser.h>
#include "ci/libFile/ciFileUtil.h"
#include <common/libCommon/ubcIni.h>
#include "libAGTClient/AGTClientSession.h"

ciMutex	GPQSTimer::_msgLock;
ciString GPQSTimer::_lastGetTime;
ciString GPQSTimer::_lastErrMsg;
ciString GPQSTimer::_nowStr;
AGTClientSession*  GPQSTimer::_agtSession = 0;

ciSET_DEBUG(9, "GPQSTimer");

void GetLineList(CString& str, CStringArray& line_list)
{
	int pos = 0;
	CString token = str.Tokenize(_T("\r\n"), pos);
	while(token != _T(""))
	{
		line_list.Add(token);
		token = str.Tokenize(_T("\r\n"), pos);
	}
}

void 
GPQSInfoData::print() 
{
	ciDEBUG(1,("%s,%s,(%d),(%d)\n",file_line.c_str(),jsp_line.c_str(),isDiff, isDownloaded));
}

GPQSTimer::GPQSTimer(ciString& site, ciString& host, ciBoolean isTotal, ciBoolean isAll) 
{
	_site = site;
	_host = host;

	_isTotal = isTotal;
	_isAll = isAll;

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, _cDrive, _cPath, cFilename, cExt);

	ciDEBUG(3, ("GPQSTimer()"));

	_makePath("Contents\\Enc\\GPQS\\GPQS_corpList.xml", _corpFile);
	_makePath("Contents\\Enc\\GPQS\\GPQS_carList.xml", _carFile);
	_makePath("Contents\\Enc\\GPQS\\GPQS_info.xml", _infoFile);

	_makePath("Contents\\Enc\\GPQS\\GPQS_lock.xml", _lockFile);
	if(_isTotal) {
		_makePath("Contents\\Enc\\GPQS\\GPQS_fileList_total.xml", _fileListFile);
	}else{
		_makePath("Contents\\Enc\\GPQS\\GPQS_fileList.xml", _fileListFile);
	}
	_makePath("Contents\\Enc\\GPQS\\data", _dataFolder);
	_makePath("Contents\\Enc\\GPQS\\temp", _tempFolder);

	ciDirHandler::create(_dataFolder);
	ciDirHandler::create(_tempFolder);

	ubcConfig aIni("UBCConnect");

	aIni.get("GPQS","Server",_server);
	if(_server.empty()){
		_server = "10.10.46.127";
	}
	_port = 0;
	aIni.get("GPQS","Port",_port);
	if(_port == 0) _port = 8080;

	_isDoing = ciFalse;

	for(int i=0;i<11;i++){
		_pass[i] = ciFalse;
	}
	
	const char* arg[11] = { "-P01", "-P02","-P03","-P04","-P05","-P06",
							"-P07","-P08","-P09","-P10","-P11" };
	for(int i=0;i<11;i++) {
		if(ciArgParser::getInstance()->isSet(arg[i])) {
			_pass[i] = ciTrue;
		}
	}

	ubcConfig bIni("UBCVariables");
	bIni.get("GPQS","lastGetTime",_lastGetTime,"");

}

GPQSTimer::~GPQSTimer() {
	ciDEBUG(3, ("~GPQSTimer()"));
}

void
GPQSTimer::_makePath(const char* relpath, ciString& outval)
{
	CString strPath;
	strPath.Format(_T("%s%s..\\..\\%s"), _cDrive, _cPath, relpath);
	CFileStatus status;
	CFile::GetStatus(strPath, status );
	outval = status.m_szFullName;
}

void
GPQSTimer::processExpired(ciString name, int counter, int interval)
{
	// 이 타이머는 5분마다 한번씩 호출된다.

	ciTime now;
	char nowStr[24];
	memset(nowStr,0x00,24);
	sprintf(nowStr, "%04d/%02d/%02d %02d:%02d:%02d", 
		now.getYear(),now.getMonth(),now.getDay(),now.getHour(), now.getMinute(),now.getSecond());
	
	_nowStr = nowStr;

	ciDEBUG(3, ("processExpired(%s,%s,%d,%d)", nowStr, name.c_str(), counter, interval));

	if(_isStart()) {
		ciWARN(("previous work does not end"));
		return ;
	}

	_startMark();


	if(_reget() || counter == 1) {
		// 이하는 처음한번만 호출
		_readXmlFile();

		//1. 법인명을 가져온다.
		CStringArray line_list;
		CString sendMsg;
		if(_getGPQSInfo("cms/getcorplist.do", sendMsg, line_list,"1")){
			_corpList.clear();
			int count = line_list.GetCount();
			for(int i=0; i<count; i++)
			{
				ciString line = line_list.GetAt(i);
				if(_getItem("<data>", line)) {
					ciDEBUG(1,("CORP : [%s]", line.c_str()));
					if(_isTotal) {
						if(line == "KMC" || line == "HMC"){
							// KMC, HMC 만 처리한다.
							_corpList.push_back(line);
						}
					}else{
						//if(line != "KMC" && line != "HMC"){
							_corpList.push_back(line);
						//}
					}
				}
			}
			_writeXmlFile(_corpFile, line_list);
		}
		//2. 차종을 가져온다.
		line_list.RemoveAll();
		sendMsg.Empty();
		if(_getGPQSInfo("cms/getcarlist.do", sendMsg, line_list,"1")){
			_writeXmlFile(_carFile, line_list);
		}
	}

	if(_regetFlag) { 
		// reget 작업은 _map의 file_list 를지워놓으면 된다.
		ciGuard aGuard(_mapLock);
		GPQSMAP::iterator itr;
		for(itr=_map.begin();itr!=_map.end();itr++){
			GPQSInfoData* aData = itr->second;
			aData->file_line = "";
		}
	}


	CStringArray line_list;
	CString sendMsg;
	//sendMsg.Format("siteId=%s", _site.c_str());

	//1. GPQS 가 바뀌었는지 보고
	if(!_getGPQSInfo("cms/getlastdt.do", sendMsg, line_list,"1")){ // skpark temp code
		_endMark();
		return;
	}
	//2. GPQS/GPQS_info.xml 파일 비교하고,
	if(!_compareGPSQInfo(line_list)) {
		ciWARN(("No GPQS changed"));
		// 앞의 에러는 의미가 없으므로 지운다. (성공시간이 매니저에서 보이게된다)
		if(!this->_lastGetTime.empty() && this->_lastErrMsg[0] == '1') {
			if(GPQSTimer::_agtSession) {
				_agtSession->setHostMsg2(_lastGetTime.c_str());
				_agtSession->setHostMsg1("");
				_lastErrMsg = "";
			}
		}
		_endMark();
		return;
	}
	// 3. Data 가져와서 GPQS/TEMP 에 쓰고 
	if(!_downloadData()) {
		_endMark();
		return;
	}
	// 4.GPQS/GPQS_lock.xml lock 설정한다음
	_lockData(1, nowStr);

	// 5.새로 수집된 페이지에 한하여 예전 자료는 지운다.
	if(!ciArgParser::getInstance()->isSet("+GPQS_keep_old")){
	 _removeOldData();
	}
	// 6.GPQS/Data 로 데이터를 옮기고 
	_moveData();

	//7. GWQS/GPQS_fileList.xml 을 작성한다.
	if(_isTotal && !_isAll) {
		_makeFileListTotal();
	}else{
		_makeFileList();
	}
	//8.GPQS/GPQS_info.xml 파일 write 한다.
	_writeXmlFile();

	//9. GPQS/GPQS_lock.xml lock 푼다
	_lockData(0, nowStr);

	if(counter % 60 == 1) {
		// 이하는 처음한번 호출되고, 이후, 60분마다 한번씩 호출되도록 한다.
		//_deleteOldFile(3);
	}
	GPQSTimer::setLastGetTime(nowStr);
	_endMark();
}

void
GPQSTimer::_readXmlFile()
{
	ciDEBUG(3, ("_readXmlFile(%s)", _infoFile.c_str()));

	FILE* fp = fopen(_infoFile.c_str(),"r");

	if(!fp) {
		ciWARN(("%s file open failed", _infoFile.c_str()));
		return;
	}

	int idx = 0;
	char line[1024];
	memset(line,0x00,1024);
	while(fgets(line,1024,fp)){
		//if(idx==0) {
		//	idx++; // line <?xml version=\"1.0\"?> 건너뜀
		//	continue;
		//}
		ciGuard aGuard(_mapLock);
		GPQSInfoData* aData = 0;
		GPQSMAP::iterator itr =_map.find(idx);
		if(itr != _map.end()){
			aData = itr->second;
			ciDEBUG(1,("%s data founded", aData->file_line.c_str()));
		}else{
			aData = new GPQSInfoData();
			_map.insert(GPQSMAP::value_type(idx, aData));
		}
		ciStringUtil::rightTrim(line);
		aData->file_line = line;
		ciDEBUG(1,("%s data inserted", aData->file_line.c_str()));
		idx++;
	}
	fclose(fp);
}

void
GPQSTimer::_writeXmlFile()
{
	ciDEBUG(3, ("_writeXmlFile(%s)", _infoFile.c_str()));

	FILE* fp = fopen(_infoFile.c_str(),"w");

	if(!fp) {
		ciWARN(("%s file open failed", _infoFile.c_str()));
		GPQSTimer::setLastErrMsg1("%s file open failed", _infoFile.c_str());
		return;
	}
	int counter=0;
	GPQSMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		GPQSInfoData* aData = itr->second;
		fputs(aData->jsp_line.c_str(),fp);
		fputs("\n",fp);
		aData->isDiff = ciFalse;
		aData->file_line = aData->jsp_line;
	}
	fclose(fp);
}

ciBoolean
GPQSTimer::_writeXmlFile(ciString& filename, CStringArray& line_list)
{
	ciDEBUG(3, ("_writeXmlFile(%s)", filename.c_str()));

	FILE* fp = fopen(filename.c_str(),"w");

	if(!fp) {
		ciWARN(("%s file open failed", filename.c_str()));
		GPQSTimer::setLastErrMsg1("%s file open failed", filename.c_str());
		return ciFalse;
	}
	int counter=0;
	int count = line_list.GetCount();
	for(int i=0; i<count; i++)
	{
		ciString line = line_list.GetAt(i);
		fputs(line.c_str(),fp);
		fputs("\n",fp);
	}
	fclose(fp);
	return ciTrue;
}


ciBoolean
GPQSTimer::_getGPQSInfo(const char* url, CString& sendMsg, CStringArray& line_list, const char* errLevel)
{
	ciDEBUG(3, ("_getGPQSInfo(%s)", url));
	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _server.c_str(), _port);
	BOOL ret_val = http_request.RequestPost(url,sendMsg,line_list);
	//BOOL ret_val = http_request.RequestGet(url,line_list);
	if(!ret_val) {
		ciERROR((http_request.GetErrorMsg()));
		// return ciFalse; //skpark temp code
		ret_val = true; //skpark temp code
		CString buf = http_request.GetErrorMsg();
		GetLineList(buf,line_list);
	}
	ciDEBUG(3, ("line_count(%d)", line_list.GetCount()));
	
	BOOL getSucceed = false;
	if(line_list.GetCount() > 1 )
	{
		CString first_line =  line_list.GetAt(0);
		ciDEBUG(1,("line_list=%s",first_line));
		if(	first_line.GetLength() >= 4 && 
			first_line.Mid(0,4).CompareNoCase("Fail") != 0) 
		{
			getSucceed = true;	
		}
	}

	if(!getSucceed){
		ciERROR((http_request.GetErrorMsg()));
		GPQSTimer::setLastErrMsg(http_request.GetErrorMsg(), errLevel);
		return ciFalse;
	}

	if(url != ciString("cms/error.do")) {
		if(_isCMSError(line_list)) {
			GPQSTimer::setLastErrMsg1("%s : call error", url, errLevel);
		}
	}

	return ciTrue;
}

int
GPQSTimer::_compareGPSQInfo(CStringArray& line_list)
{
	ciDEBUG(3, ("_compareGPSQInfo()"));
/*
<?xml version="1.0"?>
<GPQS_UPDATE>
  <FOLDER id="GPQS_CSRSCLAM" update="" />
  <FOLDER id="GPQS_CSRSCORP" update="2015-06-10 11:52:05" />
  <FOLDER id="GPQS_CSRSSTAT" update="" />
  <FOLDER id="GPQS_CSRSSYST" update="" />
  <FOLDER id="GPQS_CSRSVEHL" update="" />
  <FOLDER id="GPQS_QPPMCORP" update="" />
  <FOLDER id="GPQS_QPPMHARD" update="" />
  <FOLDER id="GPQS_QPPMVEND" update="" />
  <FOLDER id="GPQS_QVOCDATA" update="" />
</GPQS_UPDATE>

*/

	//clearMap();
	
	int retval = 0;
	GPQSInfoData* aData = NULL;

	int count = line_list.GetCount();
	for(int i=0; i<count; i++)
	{
		ciString line = line_list.GetAt(i);

		ciGuard aGuard(_mapLock);
		GPQSMAP::iterator itr =_map.find(i);
		if(itr != _map.end()){
			aData = itr->second;
		}else{
			aData = new GPQSInfoData();
			_map.insert(GPQSMAP::value_type(i, aData));
		}
		aData->jsp_line = line;

		if(!aData->jsp_line.empty() && aData->jsp_line != aData->file_line)		{
			ciDEBUG(1,("jsp(%s) <--> file(%s) is changed", aData->jsp_line.c_str(), aData->file_line.c_str()));
			aData->isDiff = ciTrue;
			aData->isDownloaded = ciFalse;
			retval++;
		}else{
			aData->isDiff = ciFalse;
		}
		ciString buf = aData->jsp_line;
		ciStringUtil::getBraceValue(buf,"<",">");
		ciString key, value;
		ciStringUtil::divide(buf,'=',&key,&value);
		aData->tablename = ciStringUtil::getFirstToken(value,ciString(" "));
		ciStringUtil::removeQuote(aData->tablename);

		ciDEBUG(1,("%s(%s) data inserted", aData->jsp_line.c_str(), aData->tablename.c_str()));
	}
	//printMap();
	ciDEBUG(1,("%d data is changed", retval));
	return retval;
}



void
GPQSTimer::clearMap()
{
	ciDEBUG(1,("clearMap()"));
	ciGuard aGuard(_mapLock);

	GPQSMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		delete (itr->second);
	}
	_map.clear();
}

void
GPQSTimer::printMap()
{
	ciDEBUG(1,("printMap()"));
	ciGuard aGuard(_mapLock);
	GPQSMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		itr->second->print();
	}
}

void
GPQSTimer::_clearFileListMap()
{
	LISTMAP::iterator itr;
	for(itr=_fileListMap.begin();itr!=_fileListMap.end();itr++)
	{
		ciStringSet* ele = itr->second;
		delete ele;
	}
	_fileListMap.clear();
	ciDEBUG(1,("_clearFileListMap"));
}

int
GPQSTimer::_downloadData()
{
	ciDEBUG(3, ("_downloadData()"));

	_clearDone();	

	//move 하기 전에 해당 페이지는 지우고,  move 후에 fileList 를 만들어야 함. 해당 페이지를 모두 갱신하는...
	_clearFileListMap();

	int counter=0;
	GPQSMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		GPQSInfoData* aData = itr->second;
		//aData->print();
		if(aData->isDiff && aData->isDownloaded == ciFalse && !aData->tablename.empty()) {
			if(_getGPQSData(aData->tablename)) {
				aData->isDownloaded = ciTrue;
				ciDEBUG(1,("GPQS/data/%s file download succeed", aData->tablename.c_str()));
				counter++;
			}else{
				ciERROR(("GPQS/data/%s file download failed", aData->tablename.c_str()));
				GPQSTimer::setLastErrMsg1("GPQS/data/%s file download failed", aData->tablename.c_str());
			}
		}
	}
	ciDEBUG(1,("%d data downloaded", counter));
	return counter;
}

void
GPQSTimer::_lockData(int val, const char* nowStr)
{
	FILE* fp = fopen(_lockFile.c_str(),"w");
	if(fp) {
		if(val == 0) {
			ciDEBUG(1,("%s lock released", _lockFile.c_str()));
		}else{
			ciDEBUG(1,("%s lock locked", _lockFile.c_str()));
		}
		CString buf;
		fputs("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", fp);
		fputs("<GPQS_LOCK>\n",fp);
		buf.Format("<lock> %d </lock>\n", val);
		fputs(buf,fp);
		buf.Format("<updateTime> %s </updateTime>\n", nowStr);
		fputs(buf,fp);
		fputs("</GPQS_LOCK>\n",fp);
		fflush(fp);
		fclose(fp);
	}else{
		ciERROR(("%s lock failed", _lockFile.c_str()));
	}
}

int
GPQSTimer::_removeOldData()
{
	ciDEBUG(3, ("_removeOldData()"));

	int counter=0;

	ciString tarFolder = _dataFolder;
	tarFolder += "\\";

	LISTMAP::iterator itr;
	for(itr=_fileListMap.begin();itr!=_fileListMap.end();itr++)
	{
		int remove_counter = 0;

		ciString prefix = itr->first;
		ciDEBUG(1,("prefix=%s", prefix.c_str()));

		ciString dirPath = tarFolder;
		dirPath += (prefix + "*.xml");

		HANDLE hFile = NULL;
		WIN32_FIND_DATA FileData;
		hFile = FindFirstFile(dirPath.c_str(), &FileData);
		if (hFile == INVALID_HANDLE_VALUE) {
			ciWARN(("old file not found"));
			continue;
		}

		do {
			if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				continue;
			}

			ciString tarFilePath = tarFolder;
			tarFilePath += "\\";
			tarFilePath += FileData.cFileName;
			ciDEBUG(5,("%s : %s file founded", prefix.c_str(),	tarFilePath.c_str()));

			if (::DeleteFile(tarFilePath.c_str()))
			{
				remove_counter++;
				ciDEBUG(5,("%s : %s file removed", prefix.c_str(),tarFilePath.c_str()));
			}else{
				ciERROR(("%s : %s file remove failed", prefix.c_str(),tarFilePath.c_str()));
				GPQSTimer::setLastErrMsg2("%s : %s file remove failed", prefix.c_str(),tarFilePath.c_str());
			}
		
		} while (FindNextFile(hFile, &FileData));
		FindClose(hFile);
		ciDEBUG(1,("%d file removed", remove_counter));
		counter += remove_counter;
	}
	return counter;
}

int
GPQSTimer::_moveData()
{
	ciDEBUG(3, ("_moveData()"));

	ciString srcFolder = _tempFolder;
	srcFolder += "\\";

	ciString tarFolder = _dataFolder;
	tarFolder += "\\";

	//Folder 가 없으면 만들어야 한다.
	ciDirHandler::create(tarFolder);

	ciString dirPath = srcFolder;
	dirPath += "*.xml";

	HANDLE hFile = NULL;
	WIN32_FIND_DATA FileData;
	hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		ciERROR(("old file not found"));
		return 0;
	}

	int move_counter = 0;
	do {
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			continue;
		}

		ciString srcFilePath = srcFolder;
		srcFilePath += "\\";
		srcFilePath += FileData.cFileName;
		ciDEBUG(5,("%s file founded", srcFilePath.c_str()));

		ciString tarFilePath = tarFolder;
		tarFilePath += "\\";
		tarFilePath += FileData.cFileName;

		if (::MoveFileEx(srcFilePath.c_str(), tarFilePath.c_str(), MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
		{
			move_counter++;
			ciDEBUG(5,("%s --> %s file moved", srcFilePath.c_str(), tarFilePath.c_str()));
		}else{
			ciERROR(("%s --> %s file move failed", srcFilePath.c_str(), tarFilePath.c_str()));
			GPQSTimer::setLastErrMsg2("%s --> %s file move failed", srcFilePath.c_str(), tarFilePath.c_str());
		}
	
	} while (FindNextFile(hFile, &FileData));
	FindClose(hFile);
	
	ciDEBUG(1,("%d file moved", move_counter));
	return move_counter;
}


int
GPQSTimer::_makeFileList()
{
	ciDEBUG(1,("_makeFileList()"));
	int counter = 0;

	ciString dirPath = _dataFolder;
	dirPath += "\\*.xml";

	HANDLE hFile = NULL;
	WIN32_FIND_DATA FileData;
	hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		ciERROR(("old file not found"));
		return 0;
	}

	this->_clearFileListMap();
	
	int move_counter = 0;
	do {
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			continue;
		}

		ciString filename = FileData.cFileName;
		ciDEBUG(5,("%s file founded", filename.c_str()));
		if(filename.length() < 4) {
			continue;		
		}
		
		ciString corp;
		_getCorpValue(filename,corp);

		ciStringSet* fileList = 0;
		LISTMAP::iterator itr = _fileListMap.find(corp);
		if(itr!=_fileListMap.end()){
			fileList = itr->second;
		}else{
			fileList = new ciStringSet();
			_fileListMap.insert(LISTMAP::value_type(corp,fileList));
		}
		fileList->insert(ciStringSet::value_type((filename)));
	
	} while (FindNextFile(hFile, &FileData));
	FindClose(hFile);

	LISTMAP::iterator itr = _fileListMap.find("COMMON");
	if(itr != _fileListMap.end())
	{
		ciStringSet* afileList = itr->second;
		LISTMAP::iterator jtr;
		for(jtr=_fileListMap.begin();jtr!=_fileListMap.end();jtr++)
		{
			if(jtr->first == "COMMON") {
				continue;
			}
			ciStringSet* bfileList = jtr->second;
			ciStringSet::iterator ktr;
			for(ktr=afileList->begin();ktr!=afileList->end();ktr++) {
				bfileList->insert(ciStringSet::value_type(*ktr));
			}
		}
		_fileListMap.erase(itr);		
	}

	ciStringList corpList;

	for(itr=_fileListMap.begin();itr!=_fileListMap.end();itr++)
	{
		corpList.push_back(itr->first);
	}

	if(corpList.size() == 0) {
		return 0;
	}

	FILE* fp = fopen(_fileListFile.c_str(),"w");

	if(!fp) {
		ciWARN(("%s file open failed", _fileListFile.c_str()));
		return 0;
	}

	fprintf(fp,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	fprintf(fp,"<GPQS_FILELIST>\n");

	ciStringList::iterator jtr;
	for(jtr=corpList.begin();jtr!=corpList.end();jtr++)
	{
		ciString corp = *jtr;
		fprintf(fp,	"<corp id=\"%s\">\n", corp.c_str());

		LISTMAP::iterator ltr = _fileListMap.find(corp);
		if(ltr == _fileListMap.end()) {
			fprintf(fp,	"</corp>\n");
			continue;
		}
		ciStringSet* fileList = ltr->second;

		ciStringSet::iterator ktr;
		for(ktr=fileList->begin();ktr!=fileList->end();ktr++) {
			ciString file = (*ktr);
			if(file.substr(0,3) == "P07" || file.substr(0,3) == "P08") {
				if(strstr(file.c_str(),"_02.") || strstr(file.c_str(),"_03.") ){
					fprintf(fp,"\t<frame> GPQS/data/%s </frame>\n", ktr->c_str());
				}else{
					fprintf(fp,"\t<page> GPQS/data/%s </page>\n", ktr->c_str());
				}
			}else{
				if(strstr(file.c_str(),"_01.") || strstr(file.c_str(),"_02.")){
					fprintf(fp,"\t<frame> GPQS/data/%s </frame>\n", ktr->c_str());
				}else{
					fprintf(fp,"\t<page> GPQS/data/%s </page>\n", ktr->c_str());
				}
			}
			counter++;
		}
		fprintf(fp,	"</corp>\n");
	}
	fprintf(fp,"</GPQS_FILELIST>\n");
	fclose(fp);
	return counter;
}

int
GPQSTimer::_makeFileListTotal()
{
	ciDEBUG(1,("_makeFileListTotal()"));
	int counter = 0;

	ciString dirPath = _dataFolder;
	dirPath += "\\*.xml";

	HANDLE hFile = NULL;
	WIN32_FIND_DATA FileData;
	hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		ciERROR(("old file not found"));
		return 0;
	}

	this->_clearFileListMap();
	
	int move_counter = 0;
	do {
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			continue;
		}

		ciString filename = FileData.cFileName;
		ciDEBUG(5,("%s file founded", filename.c_str()));
		if(filename.length() < 4) {
			continue;		
		}
		
		ciString corp;
		_getCorpValue(filename,corp);

		ciStringSet* fileList = 0;
		LISTMAP::iterator itr = _fileListMap.find(corp);
		if(itr!=_fileListMap.end()){
			fileList = itr->second;
		}else{
			fileList = new ciStringSet();
			_fileListMap.insert(LISTMAP::value_type(corp,fileList));
		}
		fileList->insert(ciStringSet::value_type((filename)));
	
	} while (FindNextFile(hFile, &FileData));
	FindClose(hFile);

	LISTMAP::iterator itr = _fileListMap.find("COMMON");
	if(itr != _fileListMap.end())
	{
		ciStringSet* afileList = itr->second;
		LISTMAP::iterator jtr;
		for(jtr=_fileListMap.begin();jtr!=_fileListMap.end();jtr++)
		{
			if(jtr->first == "COMMON") {
				continue;
			}
			ciStringSet* bfileList = jtr->second;
			ciStringSet::iterator ktr;
			for(ktr=afileList->begin();ktr!=afileList->end();ktr++) {
				bfileList->insert(ciStringSet::value_type(*ktr));
			}
		}
		_fileListMap.erase(itr);		
	}

	ciStringSet totalFileSet;
	for(itr=_fileListMap.begin();itr!=_fileListMap.end();itr++)
	{
		ciStringSet* bfileList = itr->second;
		ciStringSet::iterator ktr;
		for(ktr=bfileList->begin();ktr!=bfileList->end();ktr++) {
			totalFileSet.insert(ciStringSet::value_type(*ktr));
		}
	}


	FILE* fp = fopen(_fileListFile.c_str(),"w");

	if(!fp) {
		ciWARN(("%s file open failed", _fileListFile.c_str()));
		return 0;
	}

	fprintf(fp,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	fprintf(fp,"<GPQS_FILELIST>\n");

	ciStringSet::iterator jtr;
	for(jtr=totalFileSet.begin();jtr!=totalFileSet.end();jtr++)
	{
		ciString file = *jtr;
		if(file.substr(0,3) == "P07" || file.substr(0,3) == "P08") {
			if(strstr(file.c_str(),"_02.") || strstr(file.c_str(),"_03.") ){
				fprintf(fp,"\t<frame> GPQS/data/%s </frame>\n", file.c_str());
			}else{
				fprintf(fp,"\t<page> GPQS/data/%s </page>\n", file.c_str());
			}
		}else{
			if(strstr(file.c_str(),"_01.") || strstr(file.c_str(),"_02.")){
				fprintf(fp,"\t<frame> GPQS/data/%s </frame>\n", file.c_str());
			}else{
				fprintf(fp,"\t<page> GPQS/data/%s </page>\n", file.c_str());
			}
		}
		counter++;
	}
	fprintf(fp,"</GPQS_FILELIST>\n");
	fclose(fp);
	return counter;
}



ciBoolean
GPQSTimer::_getCorpValue(ciString& filename, ciString& outval)
{
	ciStringList::iterator itr;
	for(itr=_corpList.begin();itr!=_corpList.end();itr++) {
		const char* corp = itr->c_str();

	ciString corp1 = corp;
	corp1 += "_";

	ciString corp2 = corp;
	corp2 += ".";


		//if(strstr(filename.c_str(),corp)) {
		if(strstr(filename.c_str(),corp1.c_str()) || strstr(filename.c_str(),corp2.c_str()) ) {
			outval = corp;
			return ciTrue;
		}
	}
	outval = "COMMON";
	return ciFalse;
}

/*
int
GPQSTimer::_deleteOldFile(ciShort durationDay)
{
	ciDEBUG(1,("_deleteOldFile()"));

	CString subDir;
	_makePath("Contents\\Enc\\", subDir);


	string dirPath = subDir;
	dirPath += "_old_*.*";

	ciDEBUG(5,("old files=%s", dirPath.c_str()));
	HANDLE hFile = NULL;
	WIN32_FIND_DATA FileData;
	hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		ciERROR(("old file not found"));
		return 0;
	}

	CTime referTime = CTime::GetCurrentTime();
	CTimeSpan spanTime(durationDay, 0, 0, 0);
	referTime = referTime - spanTime; // 2일이전

	int deleted_counter = 0;
	do {
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			continue;
		}

		ciString delFilePath = subDir;
		delFilePath += "\\";
		delFilePath += FileData.cFileName;
		ciDEBUG(5,("%s file founded", delFilePath.c_str()));

		CTime fileTime(FileData.ftLastWriteTime);
		if (fileTime < referTime) {
			::remove(delFilePath.c_str());
			deleted_counter++;
			ciDEBUG(5,("%s file deleted", delFilePath.c_str()));
		}
	
	} while (FindNextFile(hFile, &FileData));
	FindClose(hFile);
	return deleted_counter;
}
*/

ciBoolean 
GPQSTimer::_getGPQSData(const char* jsp, const char* filename)
{

	ciString page = filename;
	if(filename && strlen(filename) > 3){
		page = page.substr(0,3);
	}else{
		ciERROR(("_getGPQSData(%s) Invalid filename", jsp));
		GPQSTimer::setLastErrMsg1("%s:Invalid filename", jsp);
		return ciFalse;
	}

	ciDEBUG(1,("_getGPQSData(%s,%s)", jsp,filename));

	ciString fullpath = _tempFolder;
	fullpath += "\\";
	fullpath += filename;
	
	CStringArray line_list;
	CString sendMsg;
	if(!_getGPQSInfo(jsp, sendMsg, line_list)){
		ciERROR(("%s jsp call failed", jsp));
		GPQSTimer::setLastErrMsg2("%s(%s) call failed", jsp,filename);
		return ciFalse;
	}
	if(!_writeXmlFile(fullpath, line_list)) {
		ciERROR(("%s file write failed", fullpath.c_str()));
		GPQSTimer::setLastErrMsg1("%s file write failed", filename);
		return ciFalse;
	}
	/*
	if(jsp != ciString("cms/error.do")) {
		if(_isCMSError(line_list)) {
			GPQSTimer::setLastErrMsg("%s : no data", filename);
		}
	}
	*/
	ciStringSet* fileList = 0;
	LISTMAP::iterator itr = _fileListMap.find(page);
	if(itr!=_fileListMap.end()){
		fileList = itr->second;
	}else{
		fileList = new ciStringSet();
		ciDEBUG(1,("%s page inserted", page.c_str()));
		_fileListMap.insert(LISTMAP::value_type(page,fileList));
	}
	fileList->insert(ciStringSet::value_type(filename));


	ciDEBUG(1,("%s file succeed", fullpath.c_str()));
	return ciTrue;
}



ciBoolean 
GPQSTimer::_getGPQSDataList(const char* jsp, ciStringList& dataList)
{
	ciDEBUG(1,("_getGPQSDataList(%s)", jsp));

	CStringArray line_list;
	CString sendMsg;
	if(!_getGPQSInfo(jsp, sendMsg, line_list)){
		ciERROR(("%s jsp call failed", jsp));
		return ciFalse;
	}
	int count = line_list.GetCount();
	for(int i=0; i<count; i++)
	{
		ciString line = line_list.GetAt(i);
		if(_getItem("<data>", line)) {
			ciDEBUG(1,("data : [%s]", line.c_str()));
			dataList.push_back(line);
		}
	}
	ciDEBUG(1,("%d data founded", dataList.size()));
	return ciTrue;
}


ciBoolean 
GPQSTimer::_getItem(const char* tag, ciString& line)
{
	ciStringUtil::trim(line);
	int len = strlen(tag);
	if(line.length() <= len) {
		return ciFalse;
	}
	if(line.substr(0,len) != tag){
		return ciFalse;
	}
	line = line.substr(len);
	ciStringUtil::cutPostfix(line,"<");
	return ciTrue;
}

void
GPQSTimer::_clearDone()
{
	for(int i=0;i<14;i++) {
		_page_done[i] = ciFalse;
	}
}

int
GPQSTimer::_getCarList(const char* csrs,const char* corp,int month, ciStringList& carList)
{
	ciDEBUG(1,("_getCarList(%s,%s,%d)", csrs,corp,month));

	char jsp[256]; memset(jsp,0x00,256);
	if(month > 0){
		sprintf(jsp,"cms/getcarlist.do?csrs=%s&corp=%s&mf=%d", csrs,corp, month);
	}else{
		sprintf(jsp,"cms/getcarlist.do?csrs=%s&corp=%s&mf=@", csrs,corp);
	}
    _getGPQSDataList(jsp, carList);

	return carList.size();
}


ciBoolean
GPQSTimer::_getGPQSData(ciString& tablename)
{
	ciDEBUG(1,("_getGPQSData(%s)", tablename.c_str()));
/*
<?xml version="1.0"?>
<GPQS_UPDATE>
  <FOLDER id="GPQS_CSRSCLAM" update="" />
  <FOLDER id="GPQS_CSRSCORP" update="2015-06-10 11:52:05" />
  <FOLDER id="GPQS_CSRSSTAT" update="" />
  <FOLDER id="GPQS_CSRSSYST" update="" />
  <FOLDER id="GPQS_CSRSVEHL" update="" />
  <FOLDER id="GPQS_QPPMCORP" update="" />
  <FOLDER id="GPQS_QPPMHARD" update="" />
  <FOLDER id="GPQS_QPPMVEND" update="" />
  <FOLDER id="GPQS_QVOCDATA" update="" />
</GPQS_UPDATE>
*/


	if(tablename == "GPQS_CSRSCORP") {
		// 1. CS 10,000 종합현황
		_P01(); 
		// 2. CS 10,000 법인별 상세현황
		_P02();
		
		// 4. RS 10,000 종합현황
		_P04(); 
		// 5. RS 10,000 법인별 상세현황
		_P05();
	}

	if(tablename == "GPQS_CSRSVEHL") {
		// 2. CS 10,000 법인별 상세현황
		_P02();
		// 3. CS 10,000 법인별 차종별 상세현황
		_P03();

		// 5. RS 10,000 법인별 상세현황
		_P05();
		// 6. RS 10,000 법인별 차종별 상세현황
		_P06();

	}

	if(tablename == "GPQS_CSRSCLAM") {
		// 3. CS 10,000 법인별 차종별 상세현황
		_P03();

		// 6. RS 10,000 법인별 차종별 상세현황
		_P06();
	}


	if(tablename == "GPQS_CSRSSTAT") {
		// 3. CS 10,000 법인별 차종별 상세현황
		_P03();

		// 6. RS 10,000 법인별 차종별 상세현황
		_P06();
	}

	if(tablename == "GPQS_CSRSSYST") {
		// 3. CS 10,000 법인별 차종별 상세현황
		_P03();

		// 6. RS 10,000 법인별 차종별 상세현황
		_P06();
	}

	if(tablename == "GPQS_QPPMHARD") {
		// 7. 법인별 입고 불량 종합현황 (종합(ALL), 선별(S), 공정별(G))
		_P07();

		// 8. 법인별 입고 불량 상세현황 (종합(ALL), 선별(S), 공정별(G))
		_P08();
	}

	if(tablename == "GPQS_QPPMVEND") {
		// 7. 법인별 입고 불량 종합현황 (종합(ALL), 선별(S), 공정별(G))
		_P07();

		// 8. 법인별 입고 불량 상세현황 (종합(ALL), 선별(S), 공정별(G))
		_P08();
	}

	if(tablename == "GPQS_QVOCDATA") {
		// 9. 협력사별 Q-VOC 현황_장기미결
		_P09();

		// 10. 협력사별 Q-VOC 현황_대책완료율
		_P10();

		// 11. 협력사별 Q-VOC 현황_대책수립평균소요일
		_P11();
	}


	return ciTrue;
}


void 
GPQSTimer::_P01()
{
	ciDEBUG(1,("_P01()"));
	if(_page_done[0]) return;
	// 1. CS 10,000 종합현황

	if(_isPass(0)) {
		_getGPQSData("cms/error.do", "P01.xml");
	}else{
		_getGPQSData("cms/total.do?csrs=cs", "P01.xml");
	}
	ciDEBUG(1,("_P01() done"));
	_page_done[0] = ciTrue;

}
void 
GPQSTimer::_P02()
{
	ciDEBUG(1,("_P02()"));
	if(_page_done[1]) return;
	// 2. CS 10,000 법인별 상세현황
	ciStringList::iterator itr;
	for(itr=_corpList.begin();itr!=_corpList.end();itr++) {
		const char* corp = itr->c_str();
		for(int month = 1;month <= 3 ; month++) {
			char jsp[256]; memset(jsp,0x00,256);
			sprintf(jsp,"cms/factory.do?csrs=cs&corp=%s&mf=%d", corp, month);
			char xml[256]; memset(xml,0x00,256);
			sprintf(xml,"P02_%s_%02d.xml", corp, month);
			if(_isPass(1,corp)) {
				_getGPQSData("cms/error.do", xml);
			}else{
				_getGPQSData(jsp, xml);
			}
		}
	}
	ciDEBUG(1,("_P02() done"));
	_page_done[1] = ciTrue;
}

void 
GPQSTimer::_P03()
{
	// total 의 경우 하지 않는다.
	//if(_isTotal && !_isAll)  return;

	ciDEBUG(1,("_P03()"));

	if(_page_done[2]) return;
	// 3. CS 10,000 법인별 차종별 상세현황
	ciStringList::iterator itr;
	for(itr=_corpList.begin();itr!=_corpList.end();itr++) {
		const char* corp = itr->c_str();
		if( corp == ciString("HMC") || corp == ciString("KMC") ) {
			continue;
		}
		for(int month = 1;month <= 3 ; month++) {
			ciStringList carList;
			_getCarList("cs", corp,month,carList);
			ciStringList::iterator jtr;
			for(jtr=carList.begin();jtr!=carList.end();jtr++) {
				const char* car = jtr->c_str();
				char jsp[256]; memset(jsp,0x00,256);
				sprintf(jsp,"cms/cars.do?csrs=cs&corp=%s&car=%s&mf=%d", corp, car, month);
				char xml[256]; memset(xml,0x00,256);
				sprintf(xml,"P03_%s_%s_%02d.xml", corp, car, month);
				if(_isPass(2,corp)) {
					_getGPQSData("cms/error.do", xml);
				}else{
					_getGPQSData(jsp, xml);
				}
			}
		}
	}
	ciDEBUG(1,("_P03() done"));
	_page_done[2] = ciTrue;
}
void 
GPQSTimer::_P04()
{
	ciDEBUG(1,("_P04()"));
	if(_page_done[3]) return;
	// 4. RS 10,000 종합현황
	if(_isPass(3)) {
		_getGPQSData("cms/error.do", "P04.xml");
	}else{
		_getGPQSData("cms/total.do?csrs=rs", "P04.xml");
	}
	ciDEBUG(1,("_P04() done"));
	_page_done[3] = ciTrue;
}
void 
GPQSTimer::_P05()
{
	ciDEBUG(1,("_P05()"));
	if(_page_done[4]) return;
	// 5. RS 10,000 법인별 상세현황
	ciStringList::iterator itr;
	for(itr=_corpList.begin();itr!=_corpList.end();itr++) {
		const char* corp = itr->c_str();
		char jsp[256]; memset(jsp,0x00,256);
		sprintf(jsp,"cms/factory.do?csrs=rs&corp=%s&mf=@", corp);
		char xml[256]; memset(xml,0x00,256);
		sprintf(xml,"P05_%s_00.xml", corp);
		if(_isPass(4,corp)) {
			_getGPQSData("cms/error.do", xml);
		}else{
			_getGPQSData(jsp, xml);
		}
	}
	ciDEBUG(1,("_P05() done"));
	_page_done[4] = ciTrue;
}
void 
GPQSTimer::_P06()
{
	// total 의 경우 하지 않는다.
	//if(_isTotal && !_isAll)  return;

	ciDEBUG(1,("_P06()"));
	if(_page_done[5]) return;
	// 6. RS 10,000 법인별 차종별 상세현황
	ciStringList::iterator itr;
	for(itr=_corpList.begin();itr!=_corpList.end();itr++) {
		const char* corp = itr->c_str();
		if( corp == ciString("HMC") || corp == ciString("KMC") ) {
			continue;
		}
		ciStringList carList;
		_getCarList("rs",corp,0,carList);
		ciStringList::iterator jtr;
		for(jtr=carList.begin();jtr!=carList.end();jtr++) {
			const char* car = jtr->c_str();
			char jsp[256]; memset(jsp,0x00,256);
			sprintf(jsp,"cms/cars.do?csrs=rs&corp=%s&car=%s&mf=@", corp, car);
			char xml[256]; memset(xml,0x00,256);
			sprintf(xml,"P06_%s_%s_00.xml", corp, car);
			if(_isPass(5,corp)) {
				_getGPQSData("cms/error.do", xml);
			}else{
				_getGPQSData(jsp, xml);
			}
		}
	}
	ciDEBUG(1,("_P06() done"));
	_page_done[5] = ciTrue;
}
void 
GPQSTimer::_P07()
{
	ciDEBUG(1,("_P07()"));
	if(_page_done[6]) return;
	// 7. 법인별 입고 불량 종합현황 (종합(ALL), 선별(S), 공정별(G))
	const char* kind[4] = { "ALL","S","G","" };
	for(int i=0;i<3;i++) {
		char jsp[256]; memset(jsp,0x00,256);
		sprintf(jsp,"cms/fault.do?bsjs=%s", kind[i]);
		char xml[256]; memset(xml,0x00,256);
		sprintf(xml,"P07_%02d.xml", i+1);
		if(_isPass(6)) {
			_getGPQSData("cms/error.do", xml);
		}else{
			_getGPQSData(jsp, xml);
		}
	}
	ciDEBUG(1,("_P07() done"));
	_page_done[6] = ciTrue;
}
void 
GPQSTimer::_P08()
{
	// total 의 경우 하지 않는다.
	//if(_isTotal && !_isAll)  return;

	ciDEBUG(1,("_P08()"));
	if(_page_done[7]) return;
	// 8. 법인별 입고 불량 상세현황 (종합(ALL), 선별(S), 공정별(G))
	const char* kind[4] = { "ALL","S","G","" };
	ciStringList::iterator itr;
	for(itr=_corpList.begin();itr!=_corpList.end();itr++) {
		const char* corp = itr->c_str();
		if( corp == ciString("HMC") || corp == ciString("KMC") ) {
			continue;
		}
		for(int i=0;i<3;i++) {
			char jsp[256]; memset(jsp,0x00,256);
			sprintf(jsp,"cms/faultCorp.do?bsjs=%s&corp=%s", kind[i], corp);
			char xml[256]; memset(xml,0x00,256);
			sprintf(xml,"P08_%s_%02d.xml", corp,i+1);
			if(_isPass(7,corp)) {
				_getGPQSData("cms/error.do", xml);
			}else{
				_getGPQSData(jsp, xml);
			}
		}
	}
	ciDEBUG(1,("_P08() done"));
	_page_done[7] = ciTrue;
}

void 
GPQSTimer::_P09()
{
	ciDEBUG(1,("_P09()"));
	if(_page_done[8]) return;
	// 9. 협력사별 Q-VOC 현황_장기미결
	ciStringList::iterator itr;
	for(itr=_corpList.begin();itr!=_corpList.end();itr++) {
		const char* corp = itr->c_str();
		char jsp[256]; memset(jsp,0x00,256);
		sprintf(jsp,"cms/undecided.do?corp=%s", corp);
		char xml[256]; memset(xml,0x00,256);
		sprintf(xml,"P09_%s.xml", corp);
		if(_isPass(8,corp)) {
			_getGPQSData("cms/error.do", xml);
		}else{
			_getGPQSData(jsp, xml);
		}
	}
	ciDEBUG(1,("_P09() done"));
	_page_done[8] = ciTrue;
}
void 
GPQSTimer::_P10()
{
	ciDEBUG(1,("_P10()"));
	if(_page_done[9]) return;
	// 10. 협력사별 Q-VOC 현황_대책완료율
	ciStringList::iterator itr;
	for(itr=_corpList.begin();itr!=_corpList.end();itr++) {
		const char* corp = itr->c_str();
		char jsp[256]; memset(jsp,0x00,256);
		sprintf(jsp,"cms/completion.do?corp=%s", corp);
		char xml[256]; memset(xml,0x00,256);
		sprintf(xml,"P10_%s.xml", corp);
		if(_isPass(9,corp)) {
			_getGPQSData("cms/error.do", xml);
		}else{
			_getGPQSData(jsp, xml);
		}
	}
	ciDEBUG(1,("_P10() done"));
	_page_done[9] = ciTrue;
}
void 
GPQSTimer::_P11()
{
	ciDEBUG(1,("_P11()"));
	if(_page_done[10]) return;
	// 11. 협력사별 Q-VOC 현황_대책수립평균소요일
	ciStringList::iterator itr;
	for(itr=_corpList.begin();itr!=_corpList.end();itr++) {
		const char* corp = itr->c_str();
		char jsp[256]; memset(jsp,0x00,256);
		sprintf(jsp,"cms/runday.do?corp=%s", corp);
		char xml[256]; memset(xml,0x00,256);
		sprintf(xml,"P11_%s.xml", corp);
		if(_isPass(10,corp)) {
			_getGPQSData("cms/error.do", xml);
		}else{
			_getGPQSData(jsp, xml);
		}
	}
	ciDEBUG(1,("_P11() done"));
	_page_done[10] = ciTrue;
}
void 
GPQSTimer::_P12()
{
	if(_page_done[11]) return;
	_page_done[11] = ciTrue;
}
void 
GPQSTimer::_P13()
{
	if(_page_done[12]) return;
	_page_done[12] = ciTrue;
}
void 
GPQSTimer::_P14()
{
	if(_page_done[13]) return;
	_page_done[13] = ciTrue;
}

void 
GPQSTimer::_startMark()
{
	ciGuard aGuard(_isDoingLock);
	_isDoing = ciTrue;
}
void 
GPQSTimer::_endMark()
{
	ciGuard aGuard(_isDoingLock);
	_isDoing = ciFalse;

}
ciBoolean 
GPQSTimer::_isStart()
{
	ciGuard aGuard(_isDoingLock);
	return _isDoing;

}

const char* 
GPQSTimer::getLastGetTime() 
{ 
	ciGuard aGuard(_msgLock);
	return _lastGetTime.c_str(); 
}

const char* 
GPQSTimer::getLastErrMsg() 
{ 
	ciGuard aGuard(_msgLock);
	return _lastErrMsg.c_str(); 
}

void	
GPQSTimer::setLastGetTime(const char* p) 
{ 
	ciGuard aGuard(_msgLock);
	_lastGetTime = p; 
	if(GPQSTimer::_agtSession) {
		_agtSession->setHostMsg2(_lastGetTime.c_str());
		_agtSession->setHostMsg1("");
		_lastErrMsg = "";
	}
	ubcConfig aIni("UBCVariables");
	aIni.set("GPQS","lastGetTime",_lastGetTime.c_str());

}

void	
GPQSTimer::setLastErrMsg(const char* p, const char* errLevel) 
{ 
	ciGuard aGuard(_msgLock);
	_lastErrMsg = errLevel; 
	_lastErrMsg += "(";
	_lastErrMsg += _nowStr;
	_lastErrMsg += ")";
	_lastErrMsg += p; 
	if(GPQSTimer::_agtSession) {
		_agtSession->setHostMsg1(_lastErrMsg.c_str());
	}
}

void	
GPQSTimer::setLastErrMsg1(const char* format, const char* p,const char* errLevel) 
{ 
	ciGuard aGuard(_msgLock);
	char buf[2048];	  
	sprintf(buf, format, p);	  
	setLastErrMsg((const char*)buf,errLevel);
}

void	
GPQSTimer::setLastErrMsg2(const char* format, const char* p1, const char* p2,const char* errLevel) 
{ 
	ciGuard aGuard(_msgLock);
	char buf[2048];	  
	sprintf(buf, format, p1,p2);	  
	setLastErrMsg((const char*)buf,errLevel);
}

ciBoolean	
GPQSTimer::_isCMSError(CStringArray& line_list) 
{ 
	int count = line_list.GetCount();
	for(int i=0; i<count; i++)
	{
		ciString line = line_list.GetAt(i);
		if(strstr(line.c_str(), "cmserror") != NULL || strstr(line.c_str(), "Not Found") != NULL){
			return ciTrue;
		}
	}
	return ciFalse;
}

ciBoolean 
GPQSTimer::_isPass(int idx, const char* corp)
{
	ciDEBUG(1,("_isPass(%d,%s)", idx, (corp?corp:"NULL")));

	if(corp == 0 || strlen(corp)==0) {
		return _pass[idx];
	}
	if(_site == corp) {
		return _pass[idx];
	}
	return ciFalse;
}

ciBoolean 
GPQSTimer::_cleanDataFolder()
{
	LPCTSTR lpDirPath = _dataFolder.c_str();

	if(lpDirPath == NULL)// 경로가 없는 경우 되돌아간다.
	{  
		return false;
	}//if

	BOOL bRval = FALSE;
	int nRval = 0;
	//CString strNextDirPath = _T("");
	CString strRoot = _T("");
	CString strPath = lpDirPath;
	strPath.Append(_T("\\*.*"));
	CFileFind find;

	// 폴더가 존재 하는 지 확인 검사 
	bRval = find.FindFile(strPath);
	if(bRval == FALSE)
	{
		return bRval;
	}//if

	while(bRval)
	{
		bRval = find.FindNextFile();
		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		// Directory 일 경우
		if(find.IsDirectory())             
		{
			//strNextDirPath.Format(_T("%s\\*.*") , find.GetFilePath()); 
			// Recursion function 호출            
			//DeleteDirectory(find.GetFilePath());            
			continue;
		}
		else	// file일 경우
		{
			// 파일 삭제
			DeleteFile(find.GetFilePath()); 
		}
	}//while

	strRoot = find.GetRoot();     
	find.Close(); 
	bRval = RemoveDirectory(strRoot);

	if(bRval) {
		ciDEBUG(1,("rmdir(%s) succeed", _dataFolder.c_str()));
		return ciTrue;
	}

	ciERROR(("removeDirectory(%s) failed", _dataFolder.c_str()));
	return ciFalse;
}

ciBoolean 
GPQSTimer::_reget()
{
	_regetFlag = ciFalse;
	if(GPQSTimer::_agtSession == 0) return ciFalse;
	ciString msg = _agtSession->getFwMsg();

	ciDEBUG(1,("_reget(%s)", msg.c_str()));

	if(strstr(msg.c_str(),"HMG::clean") || strstr(msg.c_str(),"HMC::clean")) {
		_cleanDataFolder();
	}
	if(strstr(msg.c_str(),"HMG::reget") || strstr(msg.c_str(),"HMC::reget")) {
		_regetFlag = ciTrue;
	}
	_agtSession->setFwMsg("OK");
	ciDEBUG(1,("_reget(%s)", msg.c_str()));
	return ciTrue;
}

