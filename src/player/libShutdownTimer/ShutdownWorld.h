 /*! \file ShutdownWorld.h
 *  Copyright �� 2003, SQI. All rights reserved.
 *
 *  \brief World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2003/08/06 11:01:00
 */

#ifndef _SHUTDOWNWORLD_H_
#define _SHUTDOWNWORLD_H_

#include <ci/libBase/ciWin32DLL.h>
#include <ci/libWorld/ciWorld.h>

#include "ShutdownTimer.h"

// ShutdownWorld class
class ShutdownWorld : public ciWorld {
public:	
	ShutdownWorld();
	virtual ~ShutdownWorld();
	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode = 0);
	
	static int setShutdownTime(char* p_param);

protected:	
	ShutdownTimer _timer;

};
		
#endif //_SHUTDOWNWORLD_H_
