#ifndef _ShutdownTimer_H_
#define _ShutdownTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>

class ShutdownTimer : public ciWinPollable {
protected:
	int _interval;

	ciMutex _mutex;
	int _shutdownHour; // 24�� ����
	int _shutdownMinute;
	int _shutdownWaitTime;
	bool _nextDayFlag;

	//ciStringList _monitorOffList;
	//ciBoolean	_monitorOff;
	//ciLong		_hostType;

	ciString _batchFile;

public:
	ShutdownTimer();
	virtual ~ShutdownTimer();

	//virtual void initTimer(const char* pname, int interval = 60);
	virtual void processExpired(ciString name, int counter, int interval);

	//void setHostType(ciLong hostType);
	//void setMonitorOffTime(ciStringList& monitorOffList);
	//void setMonitorOffFlag(ciBoolean monitorOff);
	//void clearMonitorOffTime();
	
	void setShutdownTime(int hour, int minute, int waitTime);
	void getShutdownTime(int& hour, int& minute, int& waitTime);
	void setBatchFile(const char* filename);
	const char* getBatchFile();

	void Shutdown(const char* shutdown_time=NULL);

	ciBoolean isProjector();
//	ciBoolean	projectorOn();
//	ciBoolean	projectorOff();

//	ciBoolean	projectorOn(int eType);
//	ciBoolean	projectorOff(int eType);

	//void	monitorOn_Off(int counter);
	//int		_isMonitorOn(int counter);
	
	void	ShutdownSystem();
};

#endif //_ShutdownTimer_H_
