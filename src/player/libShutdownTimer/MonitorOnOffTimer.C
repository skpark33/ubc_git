#include <ci/libDebug/ciDebug.h>
#include <ci/libBase/ciTime.h>
#include "ci/libConfig/ciEnv.h"

#include "MonitorOnOffTimer.h"
#include <common/libScratch/scratchUtil.h>
#include <common/libInstall/installUtil.h>
#include <common/libCommon/utvUtil.h>
#include <libProjectorControl/ProjectorControl.h>

#define HOST_TYPE_UNKNOWN		99
#define HOST_TYPE_PROJECTOR		3
#define HOST_TYPE_PDP			5

ciSET_DEBUG(3, "MonitorOnOffTimer");

MonitorOnOffTimer::MonitorOnOffTimer() 
: _interval(0)
{
	ciDEBUG(3, ("MonitorOnOffTimer()"));
}

MonitorOnOffTimer::~MonitorOnOffTimer() {
	ciDEBUG(3, ("~MonitorOnOffTimer()"));
}
/*
void 
MonitorOnOffTimer::initTimer(const char* pname, int interval) {
	ciDEBUG(3, ("initTimer(%s,%d)", pname, interval));

	_interval = interval;
	_timer = new ciTimer(pname, this);
	_timer->setInterval(interval);
}
*/
void
MonitorOnOffTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG2(3, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	CI_GUARD(_mutex, "processExpired()");

	monitorOn_Off(counter);
	//ciString first = _monitorOffList.front();
	//if(first == "ON-OFF"){
	//	_monitorOffList.pop_front();
	//	monitorOn_Off();
	//}else{
	//	monitorOff_On();
	//}
}


void
MonitorOnOffTimer::monitorOn_Off(int counter)
{
	static int on_failCounter = 0;
	static int off_failCounter = 0;

	ciDEBUG(1,("monitorOn_Off(counter=%d,on_failCounter=%d,off_failCounter%d)", counter, on_failCounter, off_failCounter));

	if(_monitorOff==ciFalse || _monitorOffList.size() == 0){
		// 모니터 on/off 가 설정되어 있지 않은경우
		// 모니터 전원은 건드리지 않고, 절전기능만 off 한다.
		
		if(counter==1){
			on_failCounter = 0;
			ciDEBUG(1,("monitorPowerSaveOff()"));
			scratchUtil::getInstance()->monitorPowerSaveOff();
			//if(!ProjectorWrapper::getInstance()->projectorOn()) {
			//	ciDEBUG(1,("projector on failed, try once again"));
			//	on_failCounter++;
			//}
			return;
		}
		/*
		if(on_failCounter > 0 ) {
			if ( on_failCounter < 3 ) {  // 2회, 3회까지
				if(!ProjectorWrapper::getInstance()->projectorOn()) {
					ciDEBUG(1,("projector on failed, try once again"));
					on_failCounter++;
				}else{
					on_failCounter = 0;
				}
			}else{
				on_failCounter = 0;
			}
		}
		*/
		return;
	}

	ciDEBUG(3,("monitorOn_Off process"));

	int onMonitor = _isMonitorOn(counter);

	if(onMonitor==0) {
		ciDEBUG2(1,("monitorOff()"));
		on_failCounter = 0;
		off_failCounter = 0;
		ciDEBUG(1,("monitorPowerSaveOn"));
		//scratchUtil::getInstance()->monitorPowerSaveOn();
		if(!ProjectorWrapper::getInstance()->projectorOff()){
			off_failCounter++;
		}
		return;
	}

	if(onMonitor==1) {
		ciDEBUG2(1,("monitorOn()"));
		on_failCounter = 0;
		off_failCounter = 0;
		ciDEBUG(1,("monitorPowerSaveOff"));
		scratchUtil::getInstance()->monitorPowerSaveOff();
		if(!ProjectorWrapper::getInstance()->projectorOn()){
			on_failCounter++;
		}
		return;
	}

	if(on_failCounter > 0 ){
		if ( on_failCounter < 3 ) {  // 2회, 3회까지
			ciDEBUG(1,("monitorOn()"));
			ciDEBUG(1,("monitorPowerSaveOff"));
			scratchUtil::getInstance()->monitorPowerSaveOff();
			if(!ProjectorWrapper::getInstance()->projectorOn()) {
				ciDEBUG(1,("projector on failed, try once again"));
				on_failCounter++;
			}else{
				on_failCounter = 0;
			}
		}else{
			on_failCounter = 0;
		}
	}

	if(off_failCounter > 0 ){
		if ( off_failCounter < 3 ) {  // 2회, 3회까지
			ciDEBUG(1,("monitorOff()"));
			ciDEBUG(1,("monitorPowerSaveOn"));
			//scratchUtil::getInstance()->monitorPowerSaveOn();
			if(!ProjectorWrapper::getInstance()->projectorOff()) {
				ciDEBUG(1,("projector off failed, try once again"));
				off_failCounter++;
			}else{
				off_failCounter = 0;
			}
		}else{
			off_failCounter = 0;
		}
	}
	return;
}

int
MonitorOnOffTimer::_isMonitorOn(int counter)
{
	int onMonitor = -1;
	ciStringList::iterator itr;
	for(itr=_monitorOffList.begin();itr!=_monitorOffList.end();itr++){
		ciString buf = (*itr);
		ciString offTime,onTime;
		ciStringUtil::divide(buf,'-',&onTime,&offTime);

		if(onTime.size()!=5 || offTime.size()!=5){
			ciWARN(("Invalid MonitorOffTime format %s,%s", onTime.c_str(), offTime.c_str()));
			continue;
		}

		if(counter==1){
			offTime += ":00";
			onTime += ":00";
			if(utvUtil::getInstance()->isBetween(onTime.c_str(), offTime.c_str())){
				ciDEBUG(1,("now is between %s and %s", onTime.c_str(), offTime.c_str()));
				onMonitor=1;  // File 에 쓰는 것으로 수정한다.
				break;
			}else{
				ciDEBUG(1,("now is not between %s and %s", onTime.c_str(), offTime.c_str()));
				onMonitor=0;
			}
		}else{
			if(utvUtil::getInstance()->isOnTime(onTime.c_str())){
				onMonitor=1;
			}else if(utvUtil::getInstance()->isOnTime(offTime.c_str())){
				onMonitor=0;
			}
			if(onMonitor >= 0){
				break;
			}
		}
	}
	return onMonitor;
}

/*
ciBoolean
MonitorOnOffTimer::projectorOn(int pType)
{
	eProjectorType eType =  (eProjectorType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = projector.ToString(eType);
	ciDEBUG(1,("projectorOn(%s)", eTypeStr.c_str()));

	eProjectorStatus eStatus = eProjectorStatus_UNKNOWN;
	eProjectorReturnValue  ret_val = projector.PowerStatus(eStatus);
	if(ret_val == eProjectorRV_SUCCESS && eStatus == eProjectorStatus_PowerOn){
		ciWARN(("%s BEAM PROJECTOR IS ALREADY POWER ON", eTypeStr.c_str()));
		return ciTrue;
	}
	
	ret_val = projector.PowerOn();
	if(ret_val == eProjectorRV_SUCCESS){
		ciDEBUG(1,("%s BEAM PROJECTOR IS POWER ON SUCCEED", eTypeStr.c_str()));
		return ciTrue;
	}
	ciDEBUG(1,("%s BEAM PROJECTOR IS POWER ON FAILED %d", eTypeStr.c_str(), ret_val));
	return ciFalse;
}

ciBoolean
MonitorOnOffTimer::projectorOff(int pType)
{
	eProjectorType eType = (eProjectorType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = projector.ToString(eType);
	ciDEBUG(1,("projectorOff(%s)", eTypeStr.c_str()));

	eProjectorStatus eStatus = eProjectorStatus_UNKNOWN;
	eProjectorReturnValue  ret_val = projector.PowerStatus(eStatus);
	if(ret_val == eProjectorRV_SUCCESS && eStatus == eProjectorStatus_PowerOff){
		ciWARN(("%s BEAM PROJECTOR IS ALREADY POWER OFF", eTypeStr.c_str()));
		return ciTrue;
	}
	
	ret_val = projector.PowerOff();
	if(ret_val == eProjectorRV_SUCCESS){
		ciDEBUG(1,("%s BEAM PROJECTOR IS POWER OFF SUCCEED", eTypeStr.c_str()));
		return ciFalse;
	}
	ciDEBUG(1,("%s BEAM PROJECTOR IS POWER OFF FAILED %d", ret_val, eTypeStr.c_str()));
	return ciFalse;
}

ciBoolean
MonitorOnOffTimer::projectorOff()
{
	if( projectorOff(eProjectorType_LG) || 
		projectorOff(eProjectorType_SHINDORICO) ||
		projectorOff(eProjectorType_CANNON) ){
		return ciTrue;
	}
	return ciFalse;
}
ciBoolean
MonitorOnOffTimer::projectorOn()
{
	if( projectorOn(eProjectorType_LG) || 
		projectorOn(eProjectorType_SHINDORICO) ||
		projectorOn(eProjectorType_CANNON) ){
		return ciTrue;
	}
	return ciFalse;
}
*/
ciBoolean
MonitorOnOffTimer::isProjector()
{
	return (( _hostType == HOST_TYPE_PROJECTOR || _hostType == HOST_TYPE_PDP  || _hostType == HOST_TYPE_UNKNOWN ) ? ciTrue : ciFalse);
}

void 
MonitorOnOffTimer::setHostType(ciLong hostType) 
{ 
	ciDEBUG(3, ("setHostType(%d)",hostType));
	CI_GUARD(_mutex, "setHostType()");
	_hostType = hostType; 
}
void 
MonitorOnOffTimer::setMonitorOffTime(ciStringList& monitorOffList) 
{ 
	ciDEBUG(3, ("setMonitorOffTime(%d)", monitorOffList.size()));
	CI_GUARD(_mutex, "setMonitorOffTime()");
	_monitorOffList = monitorOffList; 
}
void 
MonitorOnOffTimer::setMonitorOffFlag(ciBoolean monitorOff) 
{ 
	ciDEBUG(3, ("setMonitorOffFlag(%d)",monitorOff));
	CI_GUARD(_mutex, "setMonitorOffFlag()");
	_monitorOff = monitorOff; 
}
void 
MonitorOnOffTimer::clearMonitorOffTime() 
{ 
	ciDEBUG(3, ("clearMonitorOffTime()"));
	CI_GUARD(_mutex, "clearMonitorOffTime()");
	_monitorOffList.clear(); 
}

