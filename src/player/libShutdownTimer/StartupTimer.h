#ifndef _StartupTimer_H_
#define _StartupTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>

class StartupTimer : public ciWinPollable {
protected:
	int _interval;

	ciMutex _mutex;
	int _startupHour; // 24�� ����
	int _startupMinute;
	int _startupWaitTime;
	bool _nextDayFlag;

public:
	StartupTimer();
	virtual ~StartupTimer();

	virtual void processExpired(ciString name, int counter, int interval);

	void setStartupTime(int hour, int minute, int waitTime);
	void getStartupTime(int& hour, int& minute, int& waitTime);

	void Startup();
};

#endif //_StartupTimer_H_
