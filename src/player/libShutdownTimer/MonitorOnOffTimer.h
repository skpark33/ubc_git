#ifndef _MonitorOnOffTimer_H_
#define _MonitorOnOffTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>

class MonitorOnOffTimer : public ciWinPollable {
protected:
	int _interval;

	ciMutex _mutex;

	ciStringList _monitorOffList;
	ciBoolean	_monitorOff;
	ciLong		_hostType;


public:
	MonitorOnOffTimer();
	virtual ~MonitorOnOffTimer();

	//virtual void initTimer(const char* pname, int interval = 60);
	virtual void processExpired(ciString name, int counter, int interval);

	void setHostType(ciLong hostType);
	void setMonitorOffTime(ciStringList& monitorOffList);
	void setMonitorOffFlag(ciBoolean monitorOff);
	void clearMonitorOffTime();
	

	ciBoolean isProjector();
//	ciBoolean	projectorOn();
//	ciBoolean	projectorOff();

//	ciBoolean	projectorOn(int eType);
//	ciBoolean	projectorOff(int eType);

	void	monitorOn_Off(int counter);
	int		_isMonitorOn(int counter);
	
};

#endif //_MonitorOnOffTimer_H_
