#ifndef _ShutdownThread_h_
#define _ShutdownThread_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>

	////////////////////
	// ShutdownThread

class ShutdownThread : public virtual ciThread {
protected:
	ciString _command;

public:
	ShutdownThread();
	virtual ~ShutdownThread();

	virtual void run();

	void setCommand(const char* command);
};

#endif // _ShutdownThread_h_
