// ServiceMgr.h: interface for the CServiceMgr class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVICEMGR_H__D5D636CE_8FB0_44BA_BD03_3E14E22C668A__INCLUDED_)
#define AFX_SERVICEMGR_H__D5D636CE_8FB0_44BA_BD03_3E14E22C668A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ServiceUtil.h"

typedef struct
{
	CHAR	lpServiceName[MAX_PATH];
	CHAR	lpDisplayName[MAX_PATH];
	CHAR	lpDescription[MAX_PATH];
	CHAR	lpBinaryPathName[MAX_PATH];
	CHAR	lpModulePath[MAX_PATH];

	DWORD	dwDesiredAccess;
	/*	#define SERVICE_QUERY_CONFIG			0x0001
		#define SERVICE_CHANGE_CONFIG			0x0002
		#define SERVICE_QUERY_STATUS			0x0004
		#define SERVICE_ENUMERATE_DEPENDENTS	0x0008
		#define SERVICE_START					0x0010
		#define SERVICE_STOP					0x0020
		#define SERVICE_PAUSE_CONTINUE			0x0040
		#define SERVICE_INTERROGATE				0x0080
		#define SERVICE_USER_DEFINED_CONTROL	0x0100
		#define SERVICE_ALL_ACCESS				0x01FF */

	DWORD	dwServiceType;
	/*	#define SERVICE_FILE_SYSTEM_DRIVER		0x0002
		#define SERVICE_KERNEL_DRIVER			0x0001
		#define SERVICE_WIN32_OWN_PROCESS		0x0010
		#define	SERVICE_WIN32_SHARE_PROCESS		0x0020
		#define SERVICE_INTERACTIVE_PROCESS		0x0100 */ 

	DWORD	dwStartType;
	/*	#define SERVICE_BOOT_START				0x0000
		#define SERVICE_SYSTEM_START			0x0001
		#define SERVICE_AUTO_START				0x0002
		#define SERVICE_DEMAND_START			0x0003
		#define SERVICE_DISABLED				0x0004 */

	DWORD	dwErrorControl;
	/*	#define SERVICE_ERROR_IGNORE			0x0000
		#define SERVICE_ERROR_NORMAL			0x0001
		#define SERVICE_ERROR_SEVERE			0x0002
		#define SERVICE_ERROR_CRITICAL			0x0003 */

	CHAR	lpDependencies[MAX_PATH];
	/* Example) "LanmanWorkstation\0LanmanServer\0LmHosts\0\0" */

	CHAR	lpServiceStartName[MAX_PATH];
	/* if lpServiceStartName is Not NULL
		LSA_HANDLE Policy = GetPolicyHandle();
		BYTE buffer[1024] = {0};
		PSID sid = (PSID)buffer;
		if(GetUserSID(sid, "user name"))
			UserPrivileges(sid, Policy, L"SeServiceLogonRight"); */
		
	CHAR	lpPassword[MAX_PATH];
	/* if lpServiceStartName is Not NULL and lpPassword is NULL
		HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\Lsa -> limitblankpassworduse (DWORD)
		0 Disable - Blank Password use
		1 Enable  - No. */

	SERVICE_FAILURE_ACTIONS sfa;
	SC_ACTION sfaAction[3];
	/* if service failed, what do you want? 
		ChangeServiceConfig2 (SERVICE_CONFIG_FAILURE_ACTIONS)
		SC_ACTION::Type 
			SC_ACTION_NONE, -- default, 
			SC_ACTION_RESTART, 
			SC_ACTION_REBOOT, 
			SC_ACTION_RUN_COMMAND
		SC_ACTION::Delay
			if Type != SC_ACTION_NONE
				default delay 2000 ms 
	
		remark) index begin 0 - 1 - 2 */
	CHAR	sfaReBootMessage[MAX_PATH];
	CHAR	sfaRetryCommand[MAX_PATH];
	UINT	sfaFailureCounter;

	SERVICE_STATUS_HANDLE sshHandler;
} SRVARG, *LPSRVARG;


class CServiceManager  
{
	class CSrvHandle
	{
	public:
		CSrvHandle() 
		{ 
			hSrv = hScm = NULL; 
		}
		~CSrvHandle() 
		{ 
			if(hSrv) CloseServiceHandle(hSrv); 
			if(hScm) CloseServiceHandle(hScm); 
		}
		SC_HANDLE hSrv, hScm;
	};

public:
	CServiceManager();
	virtual ~CServiceManager();

	/*----------------------------
	 | Service Control Functions |
	 ----------------------------*/

	// install service with configured param
	DWORD Install();

	// uninstall service, if force is true, and timeover -> kill process
	DWORD Uninstall(BOOL bForce = FALSE, DWORD dwWait = 30 /* sec */);

	// start service with timeout
	DWORD Start(DWORD dwWait = 30 /* sec */);

	// stop service with timeout
	DWORD Stop(BOOL bForce = FALSE, DWORD dwWait = 30 /* sec */);

	// pause service with timeout
	DWORD Pause(DWORD dwWait = 30 /* sec */);

	// continue service with timeout
	DWORD Continue(DWORD dwWait = 30 /* sec */);

	// retrive service run status
	DWORD GetServiceRunStatus(SERVICE_STATUS& ss);
	DWORD GetServiceRunStatus();

	// set service run status 
	DWORD SetServiceRunStatus(DWORD dwStatus);

	// set service event handle store
	VOID  SetServiceHandler(SERVICE_STATUS_HANDLE ssh);
	SERVICE_STATUS_HANDLE GetServiceHandler();


	/*-----------------------------------------
	 | Service Install Param Config Functions |
	 -----------------------------------------*/

	// set service unique name ( must be, default fill)
	VOID  ConfigServiceName(LPCTSTR lpszStr);

	// set service display name ( must be, default fill)
	VOID  ConfigServiceDisp(LPCTSTR lpszStr);

	// set service description ( optional )
	VOID  ConfigServiceDesc(LPCTSTR lpszStr);

	// set service execute file full path ( must be, default fill)
	VOID  ConfigServiceExec(LPCTSTR lpszStr);

	// specfic startup username and password ( optional )
	VOID  ConfigStartUser(LPCTSTR lpszUser, LPCTSTR lpszPassword);

	// dependency other services ( optional )
	VOID  ConfigDependencies(LPCTSTR lpszStr);

	// if service start failed, next step? ( optional, default fill)
	VOID  ConfigSfaAction(UINT uIDX, SC_ACTION_TYPE action, UINT delay);
	VOID  ConfigSfaRebootMessage(LPCTSTR lpszStr);
	VOID  ConfigSfaRetryCommand(LPCTSTR lpszStr);
	VOID  ConfigSfaCounterReset(UINT count);

public:
	static SRVARG m_InstallParam;

protected:
	VOID  ConfigInitialize();
};

#endif // !defined(AFX_SERVICEMGR_H__D5D636CE_8FB0_44BA_BD03_3E14E22C668A__INCLUDED_)
