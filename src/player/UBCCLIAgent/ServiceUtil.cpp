// ServiceUtil.cpp: implementation of the CServiceUtility class.
//
//////////////////////////////////////////////////////////////////////

#define _WIN32_WINNT 0x0501

#include "ServiceUtil.h"

#include <Userenv.h>
#include <stdio.h>
#include <shlwapi.h>


#pragma comment (lib, "Userenv.lib")
#pragma comment (lib, "WtsApi32.lib")
#pragma comment(lib, "shlwapi.lib")

#ifdef _UNICODE
#pragma comment(lib, "comsuppw.lib")
#else
#pragma comment(lib, "comsupp.lib")
#endif

//#if _MSC_VER == 1200
//#pragma comment(lib, "comsupp.lib")
//#elif _MSC_VER == 1500
//#pragma comment(lib, "comsuppw.lib")
//#else
//#error "not support"
//#endif

#pragma comment(lib, "shell32.lib")


LSA_HANDLE GetPolicyHandle();
BOOL InitLsaString(PLSA_UNICODE_STRING pLsaString, LPCWSTR pwszString);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CServiceUtility::CServiceUtility()
{

}

CServiceUtility::~CServiceUtility()
{

}

BOOL CServiceUtility::ServiceUserBlankPassword(BOOL bAllow)
{
	HKEY pkey;
	DWORD Ret = ::RegOpenKey(HKEY_LOCAL_MACHINE, "SYSTEM\\ControlSet001\\Control\\Lsa", &pkey);
	if(Ret != ERROR_SUCCESS)
		return Ret;

	DWORD Value = !bAllow;
	Ret = ::RegSetValueEx(pkey, "limitblankpassworduse", 0, REG_DWORD, (LPBYTE)&Value, sizeof(DWORD));
	if(Ret != ERROR_SUCCESS)
	{
		RegCloseKey(pkey);
		return FALSE;
	}

	RegCloseKey(pkey);
	RefreshPolicyEx(TRUE, RP_FORCE);
	return TRUE;
}

BOOL CServiceUtility::GetUserSID(PSID sid, const char* username)
{
    DWORD SidBufSz;
    char DomainNameBuf[256] = {0};
    DWORD DomainNameBufSz;
    SID_NAME_USE SNU;
    DomainNameBufSz = 256;
	
    if(!LookupAccountName(NULL, username, sid, &SidBufSz, DomainNameBuf, &DomainNameBufSz, &SNU))
        return FALSE;
	return TRUE;
}

BOOL CServiceUtility::UserPrivileges(const char* username, wchar_t* pszPrivilege)
{
	LSA_UNICODE_STRING lucPrivilege;
	NTSTATUS ntsResult;

	BYTE buffer[1024] = {0};
	PSID AccountSID = (PSID)buffer;
	if(GetUserSID(AccountSID, username) == FALSE)
		return FALSE;

	LSA_HANDLE PolicyHandle = GetPolicyHandle();
	if(PolicyHandle == NULL)
		return FALSE;
	
	// Create an LSA_UNICODE_STRING for the privilege names.
	if (!InitLsaString(&lucPrivilege, pszPrivilege))
	{
		LsaClose(PolicyHandle);
		return FALSE;
	}
	
	ntsResult = LsaAddAccountRights(
		PolicyHandle,  // An open policy handle.
		AccountSID,    // The target SID.
		&lucPrivilege, // The privileges.
		1              // Number of privileges.
		);                
	
	LsaClose(PolicyHandle);
	return (BOOL)(ntsResult == ERROR_SUCCESS);
}

BOOL CServiceUtility::ProcessPrivileges(HANDLE pid, char* pszPrivilege, BOOL bEnable)
{
	HANDLE hToken; 
	TOKEN_PRIVILEGES tkp; 
	
	// Get a token for this process. 
	if (!OpenProcessToken(pid, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) 
		return FALSE; 
	
	// Get the LUID for the shutdown privilege. 
	LookupPrivilegeValue(NULL, pszPrivilege, &tkp.Privileges[0].Luid); 
	
	tkp.PrivilegeCount = 1;  // one privilege to set    
	tkp.Privileges[0].Attributes = bEnable ? SE_PRIVILEGE_ENABLED : SE_PRIVILEGE_REMOVED; 
	
	// Get the shutdown privilege for this process. 
	return AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0); 
}

BOOL CServiceUtility::IsServiceMode()
{
	return GetConsoleWindow() ? FALSE : TRUE;
}

BOOL CServiceUtility::CreateProcessToDesktop(char* pszExecute, char* args, STARTUPINFO& si, PROCESS_INFORMATION& pi, UINT SessionID)
{
	DWORD count;
	PWTS_PROCESS_INFO info;
	if(WTSEnumProcesses(info, &count) == FALSE)
		return FALSE;

	CHAR  UserName[256] = {0};
	DWORD UserLen = 256;
	GetUserName(UserName, &UserLen);
	UserPrivileges(UserName, L"SeDebugPrivilege");
	UserPrivileges(UserName, L"SeMachineAccountPrivilege");
	

	UINT ProcessID = 0;
	HANDLE hOpenProcess = NULL;
	for(UINT i=0; i<count; i++)
	{
		if(info[i].SessionId == SessionID)
		{
			hOpenProcess = OpenProcess(PROCESS_ALL_ACCESS, TRUE, info[i].ProcessId);
			if(hOpenProcess == NULL)
				continue;
			ProcessID = info[i].ProcessId;
			break;
		}
	}
	WTSFree(info);

	if(ProcessID == 0)
		return FALSE;

	HANDLE token = NULL;
	if(OpenProcessToken(hOpenProcess, TOKEN_ASSIGN_PRIMARY| TOKEN_DUPLICATE, &token) == FALSE)
	{
		CloseHandle(hOpenProcess);
		return FALSE;
	}

	HANDLE newtoken = NULL;
	if(::DuplicateTokenEx(token, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS, NULL, SecurityImpersonation, TokenPrimary, &newtoken) == FALSE)
	{
		CloseHandle(hOpenProcess);
		CloseHandle(token);
		return FALSE;
	}
	
	CloseHandle(token);

	void* EnvBlock = NULL;
	CreateEnvironmentBlock(&EnvBlock, newtoken, FALSE);

	if(::CreateProcessAsUser(newtoken, 
		pszExecute, 
		args,
		NULL,
		NULL,
		FALSE,
		NORMAL_PRIORITY_CLASS | CREATE_UNICODE_ENVIRONMENT |
		CREATE_NEW_CONSOLE  | CREATE_SEPARATE_WOW_VDM |
		CREATE_NEW_PROCESS_GROUP,
		EnvBlock,
		NULL,
		&si,
		&pi) == TRUE)
	{
		CloseHandle(hOpenProcess);
		CloseHandle(newtoken);
		return TRUE;
	}

	CloseHandle(hOpenProcess);
	CloseHandle(newtoken);
	return FALSE;
}

BOOL CServiceUtility::WTSEnumSessions(PWTS_SESSION_INFO& info, LPDWORD count)
	{ return WTSEnumerateSessions(WTS_CURRENT_SERVER_HANDLE , 0, 1, &info, count); }
BOOL CServiceUtility::WTSEnumProcesses(PWTS_PROCESS_INFO& info, LPDWORD count)
	{ return WTSEnumerateProcesses (WTS_CURRENT_SERVER_HANDLE, 0, 1, &info, count); }
void CServiceUtility::WTSFree(void* pData)
	{ WTSFreeMemory(pData); }

CServiceUtility::CWTSSession::CWTSSession(HANDLE hWTS, DWORD SessionID)
	{ Init(); Query(hWTS, SessionID); }
CServiceUtility::CWTSSession::~CWTSSession()
	{ Free(); }

void CServiceUtility::CWTSSession::Query(HANDLE hWTS, DWORD SessionID)
{
	DWORD len;
	LPTSTR str;
	if(WTSQuerySessionInformation(hWTS, SessionID, WTSClientAddress, &str, &len))
	{
		WTS_CLIENT_ADDRESS* addr = (WTS_CLIENT_ADDRESS*)str;
		BYTE* p = &addr->Address[2];
		if(SessionID == 0 || SessionID == 65536)
			sprintf(IPAddress, "0.0.0.0");
		else
			sprintf(IPAddress, "%d.%d.%d.%d", p[0], p[1], p[2], p[3] );
		WTSFreeMemory(str);
	}
	
	if(WTSQuerySessionInformation(hWTS, SessionID, WTSClientDisplay, &str, &len))
	{
		WTS_CLIENT_DISPLAY * addr = (WTS_CLIENT_DISPLAY *)str;
		HorizontalResolution = (USHORT)addr->HorizontalResolution;
		VerticalResolution = (USHORT)addr->VerticalResolution;
		ColorDepth = (USHORT)addr->ColorDepth;
		WTSFreeMemory(str);
	}

	if(WTSQuerySessionInformation(hWTS, SessionID, WTSClientProtocolType, &str, &len))
	{
		memcpy(&ProtocolType, str, 2);
		WTSFreeMemory(str);
	}
	
	WTSQuerySessionInformation(hWTS, SessionID, WTSClientName, &ClientName, &len);
	WTSQuerySessionInformation(hWTS, SessionID, WTSDomainName, &DomainName, &len);
	WTSQuerySessionInformation(hWTS, SessionID, WTSUserName, &UserName, &len);
	WTSQuerySessionInformation(hWTS, SessionID, WTSWinStationName, &WinStation, &len);
}

void CServiceUtility::CWTSSession::Init()
{
	IPAddress[0] = 0;
	HorizontalResolution = 0;
	VerticalResolution = 0;
	ColorDepth = 0;
	ProtocolType = 0;
	ClientName = NULL;
	DomainName = NULL;
	UserName = NULL;
	WinStation = NULL;
}

void CServiceUtility::CWTSSession::Free()
{
	if(ClientName)
		WTSFreeMemory(ClientName);
	if(DomainName)
		WTSFreeMemory(DomainName);
	if(UserName)
		WTSFreeMemory(UserName);
	if(WinStation)
		WTSFreeMemory(WinStation);

	Init();
}


#define BUFSIZE 256

typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
typedef BOOL (WINAPI *PGPI)(DWORD, DWORD, DWORD, DWORD, PDWORD);

#ifndef StringCchCopy
#define StringCchCopy(a, b, c) strcpy(a, c)
#endif
#ifndef StringCchCat
#define StringCchCat(a, b, c) strcat(a, c)
#endif
#ifndef StringCchPrintf
#define StringCchPrintf _snprintf
#endif

#ifndef PRODUCT_BUSINESS
#define PRODUCT_BUSINESS					0x00000006 // Business
#define PRODUCT_BUSINESS_N					0x00000010 // Business N
#define PRODUCT_CLUSTER_SERVER				0x00000012 // HPC Edition
#define PRODUCT_DATACENTER_SERVER			0x00000008 // Server Datacenter (full installation)
#define PRODUCT_DATACENTER_SERVER_CORE		0x0000000C // Server Datacenter (core installation)
#define PRODUCT_DATACENTER_SERVER_CORE_V	0x00000027 // Server Datacenter without Hyper-V (core installation)
#define	PRODUCT_DATACENTER_SERVER_V			0x00000025 // Server Datacenter without Hyper-V (full installation)
#define PRODUCT_ENTERPRISE					0x00000004 // Enterprise
#define PRODUCT_ENTERPRISE_E				0x00000046 // Enterprise E
#define PRODUCT_ENTERPRISE_N				0x0000001B // Enterprise N
#define PRODUCT_ENTERPRISE_SERVER			0x0000000A // Server Enterprise (full installation)
#define PRODUCT_ENTERPRISE_SERVER_CORE		0x0000000E // Server Enterprise (core installation)
#define PRODUCT_ENTERPRISE_SERVER_CORE_V	0x00000029 // Server Enterprise without Hyper-V (core installation) 
#define PRODUCT_ENTERPRISE_SERVER_IA64		0x0000000F // Server Enterprise for Itanium-based Systems
#define PRODUCT_ENTERPRISE_SERVER_V			0x00000026 // Server Enterprise without Hyper-V (full installation)
#define PRODUCT_HOME_BASIC					0x00000002 // Home Basic 
#define PRODUCT_HOME_BASIC_E				0x00000043 // Home Basic E
#define PRODUCT_HOME_BASIC_N				0x00000005 // Home Basic N
#define PRODUCT_HOME_PREMIUM				0x00000003 // Home Premium
#define PRODUCT_HOME_PREMIUM_E				0x00000044 // Home Premium E
#define PRODUCT_HOME_PREMIUM_N				0x0000001A // Home Premium N
#define PRODUCT_HYPERV						0x0000002A // Microsoft Hyper-V Server
#define PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT	0x0000001E // Windows Essential Business Server Management Server
#define PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING		0x00000020 // Windows Essential Business Server Messaging Server
#define PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY		0x0000001F // Windows Essential Business Server Security Server
#define PRODUCT_PROFESSIONAL				0x00000030 // Professional
#define PRODUCT_PROFESSIONAL_E				0x00000045 // Professional E
#define PRODUCT_PROFESSIONAL_N				0x00000031 // Professional N
#define PRODUCT_SERVER_FOR_SMALLBUSINESS	0x00000018 // Windows Server 2008 for Windows Essential Server Solutions
#define PRODUCT_SERVER_FOR_SMALLBUSINESS_V	0x00000023 // Windows Server 2008 without Hyper-V for Windows Essential Server Solutions
#define PRODUCT_SERVER_FOUNDATION			0x00000021 // Server Foundation
#define PRODUCT_SMALLBUSINESS_SERVER		0x00000009 // Windows Small Business Server
#define PRODUCT_SMALLBUSINESS_SERVER_PREMIUM		0x00000019 // Small Business Server Premium Edition
#define PRODUCT_STANDARD_SERVER				0x00000007 // Server Standard (full installation)
#define PRODUCT_STANDARD_SERVER_CORE		0x0000000D // Server Standard (core installation)
#define PRODUCT_STANDARD_SERVER_CORE_V		0x00000028 // Server Standard without Hyper-V (core installation)
#define PRODUCT_STANDARD_SERVER_V			0x00000024 // Server Standard without Hyper-V (full installation)
#define PRODUCT_STARTER						0x0000000B // Starter
#define PRODUCT_STARTER_E					0x00000042 // Starter E
#define PRODUCT_STARTER_N					0x0000002F // Starter N
#define PRODUCT_STORAGE_ENTERPRISE_SERVER	0x00000017 // Storage Server Enterprise
#define PRODUCT_STORAGE_EXPRESS_SERVER		0x00000014 // Storage Server Express
#define PRODUCT_STORAGE_STANDARD_SERVER		0x00000015 // Storage Server Standard
#define PRODUCT_STORAGE_WORKGROUP_SERVER	0x00000016 // Storage Server Workgroup
#define PRODUCT_UNDEFINED					0x00000000 // An unknown product
#define PRODUCT_ULTIMATE					0x00000001 // Ultimate
#define PRODUCT_ULTIMATE_E					0x00000047 // Ultimate E
#define PRODUCT_ULTIMATE_N					0x0000001C // Ultimate N
#define PRODUCT_WEB_SERVER					0x00000011 // Web Server (full installation)
#define PRODUCT_WEB_SERVER_CORE				0x0000001D // Web Server (core installation)
#define VER_SUITE_WH_SERVER					0x00008000 // Windows Home Server is installed.
 
#endif

BOOL CServiceUtility::GetOSDisplayString( LPTSTR pszOS)
{
	OSVERSIONINFOEX osvi;
	SYSTEM_INFO si;
	PGNSI pGNSI;
	PGPI pGPI;
	BOOL bOsVersionInfoEx;
	DWORD dwType;
	
	ZeroMemory(&si, sizeof(SYSTEM_INFO));
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	
	if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
		return 1;
	
	// Call GetNativeSystemInfo if supported or GetSystemInfo otherwise.
	
	pGNSI = (PGNSI) GetProcAddress(
		GetModuleHandle(TEXT("kernel32.dll")), 
		"GetNativeSystemInfo");
	if(NULL != pGNSI)
		pGNSI(&si);
	else GetSystemInfo(&si);
	
	if ( VER_PLATFORM_WIN32_NT==osvi.dwPlatformId && 
        osvi.dwMajorVersion > 4 )
	{
		StringCchCopy(pszOS, BUFSIZE, TEXT("Microsoft "));
		
		// Test for the specific product.
		
		if ( osvi.dwMajorVersion == 6 )
		{
			if( osvi.dwMinorVersion == 0 )
			{
				if( osvi.wProductType == VER_NT_WORKSTATION )
					StringCchCat(pszOS, BUFSIZE, TEXT("Windows Vista "));
				else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2008 " ));
			}
			
			if ( osvi.dwMinorVersion == 1 )
			{
				if( osvi.wProductType == VER_NT_WORKSTATION )
					StringCchCat(pszOS, BUFSIZE, TEXT("Windows 7 "));
				else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2008 R2 " ));
			}
			
			pGPI = (PGPI) GetProcAddress(
				GetModuleHandle(TEXT("kernel32.dll")), 
				"GetProductInfo");
			
			pGPI( osvi.dwMajorVersion, osvi.dwMinorVersion, 0, 0, &dwType);
			
			switch( dwType )
			{
            case PRODUCT_ULTIMATE:
				StringCchCat(pszOS, BUFSIZE, TEXT("Ultimate Edition" ));
				break;
            case PRODUCT_HOME_PREMIUM:
				StringCchCat(pszOS, BUFSIZE, TEXT("Home Premium Edition" ));
				break;
            case PRODUCT_HOME_BASIC:
				StringCchCat(pszOS, BUFSIZE, TEXT("Home Basic Edition" ));
				break;
            case PRODUCT_ENTERPRISE:
				StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition" ));
				break;
            case PRODUCT_BUSINESS:
				StringCchCat(pszOS, BUFSIZE, TEXT("Business Edition" ));
				break;
            case PRODUCT_STARTER:
				StringCchCat(pszOS, BUFSIZE, TEXT("Starter Edition" ));
				break;
            case PRODUCT_CLUSTER_SERVER:
				StringCchCat(pszOS, BUFSIZE, TEXT("Cluster Server Edition" ));
				break;
            case PRODUCT_DATACENTER_SERVER:
				StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter Edition" ));
				break;
            case PRODUCT_DATACENTER_SERVER_CORE:
				StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter Edition (core installation)" ));
				break;
            case PRODUCT_ENTERPRISE_SERVER:
				StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition" ));
				break;
            case PRODUCT_ENTERPRISE_SERVER_CORE:
				StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition (core installation)" ));
				break;
            case PRODUCT_ENTERPRISE_SERVER_IA64:
				StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition for Itanium-based Systems" ));
				break;
            case PRODUCT_SMALLBUSINESS_SERVER:
				StringCchCat(pszOS, BUFSIZE, TEXT("Small Business Server" ));
				break;
            case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM:
				StringCchCat(pszOS, BUFSIZE, TEXT("Small Business Server Premium Edition" ));
				break;
            case PRODUCT_STANDARD_SERVER:
				StringCchCat(pszOS, BUFSIZE, TEXT("Standard Edition" ));
				break;
            case PRODUCT_STANDARD_SERVER_CORE:
				StringCchCat(pszOS, BUFSIZE, TEXT("Standard Edition (core installation)" ));
				break;
            case PRODUCT_WEB_SERVER:
				StringCchCat(pszOS, BUFSIZE, TEXT("Web Server Edition" ));
				break;
			}
		}
		
		if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
		{
			if( GetSystemMetrics(SM_SERVERR2) )
				StringCchCat(pszOS, BUFSIZE, TEXT( "Windows Server 2003 R2, "));
			else if ( osvi.wSuiteMask==VER_SUITE_STORAGE_SERVER )
				StringCchCat(pszOS, BUFSIZE, TEXT( "Windows Storage Server 2003"));
			else if ( osvi.wSuiteMask==VER_SUITE_WH_SERVER )
				StringCchCat(pszOS, BUFSIZE, TEXT( "Windows Home Server"));
			else if( osvi.wProductType == VER_NT_WORKSTATION &&
				si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64)
			{
				StringCchCat(pszOS, BUFSIZE, TEXT( "Windows XP Professional x64 Edition"));
			}
			else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2003, "));
			
			// Test for the server type.
			if ( osvi.wProductType != VER_NT_WORKSTATION )
			{
				if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_IA64 )
				{
					if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
						StringCchCat(pszOS, BUFSIZE, TEXT( "Datacenter Edition for Itanium-based Systems" ));
					else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
						StringCchCat(pszOS, BUFSIZE, TEXT( "Enterprise Edition for Itanium-based Systems" ));
				}
				
				else if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64 )
				{
					if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
						StringCchCat(pszOS, BUFSIZE, TEXT( "Datacenter x64 Edition" ));
					else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
						StringCchCat(pszOS, BUFSIZE, TEXT( "Enterprise x64 Edition" ));
					else StringCchCat(pszOS, BUFSIZE, TEXT( "Standard x64 Edition" ));
				}
				
				else
				{
					if ( osvi.wSuiteMask & VER_SUITE_COMPUTE_SERVER )
						StringCchCat(pszOS, BUFSIZE, TEXT( "Compute Cluster Edition" ));
					else if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
						StringCchCat(pszOS, BUFSIZE, TEXT( "Datacenter Edition" ));
					else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
						StringCchCat(pszOS, BUFSIZE, TEXT( "Enterprise Edition" ));
					else if ( osvi.wSuiteMask & VER_SUITE_BLADE )
						StringCchCat(pszOS, BUFSIZE, TEXT( "Web Edition" ));
					else StringCchCat(pszOS, BUFSIZE, TEXT( "Standard Edition" ));
				}
			}
		}
		
		if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
		{
			StringCchCat(pszOS, BUFSIZE, TEXT("Windows XP "));
			if( osvi.wSuiteMask & VER_SUITE_PERSONAL )
				StringCchCat(pszOS, BUFSIZE, TEXT( "Home Edition" ));
			else StringCchCat(pszOS, BUFSIZE, TEXT( "Professional" ));
		}
		
		if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
		{
			StringCchCat(pszOS, BUFSIZE, TEXT("Windows 2000 "));
			
			if ( osvi.wProductType == VER_NT_WORKSTATION )
			{
				StringCchCat(pszOS, BUFSIZE, TEXT( "Professional" ));
			}
			else 
			{
				if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
					StringCchCat(pszOS, BUFSIZE, TEXT( "Datacenter Server" ));
				else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
					StringCchCat(pszOS, BUFSIZE, TEXT( "Advanced Server" ));
				else StringCchCat(pszOS, BUFSIZE, TEXT( "Server" ));
			}
		}
		
		// Include service pack (if any) and build number.
		
		if( strlen(osvi.szCSDVersion) > 0 )
		{
			StringCchCat(pszOS, BUFSIZE, TEXT(" ") );
			StringCchCat(pszOS, BUFSIZE, osvi.szCSDVersion);
		}
		
		TCHAR buf[80];
		
		StringCchPrintf( buf, 80, TEXT(" (build %d)"), osvi.dwBuildNumber);
		StringCchCat(pszOS, BUFSIZE, buf);
		
		if ( osvi.dwMajorVersion >= 6 )
		{
			if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64 )
				StringCchCat(pszOS, BUFSIZE, TEXT( ", 64-bit" ));
			else if (si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_INTEL )
				StringCchCat(pszOS, BUFSIZE, TEXT(", 32-bit"));
		}
		
		return TRUE; 
   }
   
   else
   {  
	   printf( "This sample does not support this version of Windows.\n");
	   return FALSE;
   }
}
#define TARGET_SYSTEM_NAME L"."
LSA_HANDLE GetPolicyHandle()
{
	LSA_OBJECT_ATTRIBUTES ObjectAttributes;
	WCHAR SystemName[] = TARGET_SYSTEM_NAME;
	USHORT SystemNameLength;
	LSA_UNICODE_STRING lusSystemName;
	NTSTATUS ntsResult;
	LSA_HANDLE lsahPolicyHandle;
	
	// Object attributes are reserved, so initialize to zeros.
	ZeroMemory(&ObjectAttributes, sizeof(ObjectAttributes));
	
	//Initialize an LSA_UNICODE_STRING to the server name.
	SystemNameLength = wcslen(SystemName);
	lusSystemName.Buffer = SystemName;
	lusSystemName.Length = SystemNameLength * sizeof(WCHAR);
	lusSystemName.MaximumLength = (SystemNameLength+1) * sizeof(WCHAR);
	
	// Get a handle to the Policy object.
	ntsResult = LsaOpenPolicy(
        &lusSystemName,    //Name of the target system.
        &ObjectAttributes, //Object attributes.
        POLICY_ALL_ACCESS, //Desired access permissions.
        &lsahPolicyHandle  //Receives the policy handle.
		);
	
	if (ntsResult != ERROR_SUCCESS)
		return NULL;
	return lsahPolicyHandle;
}

BOOL InitLsaString(PLSA_UNICODE_STRING pLsaString, LPCWSTR pwszString)
{
	DWORD dwLen = 0;
	
	if (NULL == pLsaString)
		return FALSE;
	
	if (NULL != pwszString) 
	{
		dwLen = wcslen(pwszString);
		if (dwLen > 0x7ffe)   // String is too large
			return FALSE;
	}
	
	// Store the string.
	pLsaString->Buffer = (WCHAR *)pwszString;
	pLsaString->Length =  (USHORT)dwLen * sizeof(WCHAR);
	pLsaString->MaximumLength= (USHORT)(dwLen+1) * sizeof(WCHAR);
	
	return TRUE;
}

