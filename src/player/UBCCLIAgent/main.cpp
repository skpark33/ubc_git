#include <windows.h>
#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>
#include <conio.h>

#include <tlhelp32.h>
//#include <iostream>
//#include <string>
#include "psapi.h"
#pragma comment(lib, "Psapi.lib")

#include <Userenv.h>
#pragma comment(lib, "Userenv.lib")

//#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")

#include "libCLITransfer/CTSocket.h"
#ifdef _DEBUG
#pragma comment(lib, "libCLITransfer_d.lib")
#else
#pragma comment(lib, "libCLITransfer.lib")
#endif

#include "ServiceUtil.h"
#include "ServiceMgr.h"

BOOL RunCmd(LPCTSTR szCmd);
DWORD WINAPI service_handler(DWORD fdwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
{
  	CServiceManager manager;

	switch (fdwControl) 
    { 
    case SERVICE_CONTROL_PAUSE:
        manager.SetServiceRunStatus(SERVICE_PAUSE_PENDING);
		// 서비스를 일시 중지 시킨다.
        manager.SetServiceRunStatus(SERVICE_PAUSED);
        break;
        
    case SERVICE_CONTROL_CONTINUE:
        manager.SetServiceRunStatus(SERVICE_CONTINUE_PENDING);
        // 일시 중지 시킨 서비스를 재개한다.
        manager.SetServiceRunStatus(SERVICE_RUNNING);
        break;
        
    case SERVICE_CONTROL_STOP:
        manager.SetServiceRunStatus(SERVICE_STOP_PENDING);
        // 서비스를 멈춘다 (즉, 종료와 같은 의미)
		// 서비스를 종료하면, service_main 는 절대로 리턴하지 않는다.
		// 그러므로 해제하려면 작업이 있으면 모든것을 이곳에서 처리한다.
        manager.SetServiceRunStatus(SERVICE_STOPPED);
        break;

    default:
        break;
    }
    
	return NO_ERROR;
}

int service_main(INT ARGC, LPSTR* ARGV)
{
 	CServiceManager manager;
	CServiceUtility utility;
	if(utility.IsServiceMode() == TRUE)
	{
		manager.SetServiceHandler(RegisterServiceCtrlHandlerEx(CServiceManager::m_InstallParam.lpServiceName, service_handler, NULL));
		if (manager.GetServiceHandler() == NULL) 
			return 0;
	}
	else
	{
		printf("run debug mode\npress any key close...\n");
	}

	manager.SetServiceRunStatus(SERVICE_START_PENDING);

	// bla bla initialize...
	Sleep(1000);

 	// start ok, i'm ready receive event
	manager.SetServiceRunStatus(SERVICE_RUNNING);

	/////////////////////////////////////////////////////////////////////
	// test code
	/////////////////////////////////////////////////////////////////////
	//char szCmd[4096] = {0};
	//sprintf(szCmd, "cmd /c %s", "notepad");
	//if(!RunCmd(szCmd))
	//{
	//	::WinExec(szCmd, SW_SHOW);
	//}

	CliTransStartup();
	printf("CliTransStartup\n");

	CCTSocket server;

	if(server.Open() == INVALID_SOCKET) return -1;
	printf("Open\n");

	if(!server.Listen(CLI_PORT)) return -1;
	printf("Listen\n");

	fd_set readset;
	FD_ZERO(&readset);

	struct sockaddr_in cli_addr;
	int clilen = sizeof(cli_addr);
	SOCKET hSocket = server.GetHandle();

    while(manager.GetServiceRunStatus() != SERVICE_STOPPED)
    {
		printf("while\n");
        if(manager.GetServiceRunStatus() == SERVICE_PAUSED)
        {
			printf("SERVICE_PAUSED\n");
            Sleep(1000);
            continue;
        }

		if(utility.IsServiceMode() == FALSE && _kbhit())
		{
			printf("stop debug mode\n");
			break;
		}

		//::MessageBeep(0xFFFFFFFF);
		//printf("ting...\n");
		//Sleep(1000);

		SOCKET s = accept(hSocket, (struct sockaddr *)&cli_addr, &clilen);
		printf("accept\n");

		if(s == INVALID_SOCKET)
		{
			Sleep(1000);
			continue;
		}

		CCTSocket client(s);
		client.SetAddress(cli_addr);
		FD_SET(s, &readset);

		fd_set rset;
		struct timeval tv;
		char buf[4096] = {0};

		tv.tv_sec = 10;
		tv.tv_usec = 0;
		rset = readset;

		int nSel = 0;
		int nWaitCnt = 30;
		while(nWaitCnt>0)
		{
			nWaitCnt--;
			printf("nWaitCnt [%d]\n", nWaitCnt);

			nSel = server.Select(&rset, NULL, NULL, &tv);
			printf("server.Select [%d]\n", nSel);

			if(nSel > 0) break;
			else if(nSel == SOCKET_ERROR)
			{
				client.Close();
				return -1;
			}

			Sleep(1000);
		}

		printf("nWaitCnt [%d]\n", nWaitCnt);

		if(nWaitCnt < 0)
		{
			client.Close();
			continue;
		}

		printf("nSel [%d]\n", nSel);

		if(FD_ISSET(s, &rset))
		{
			for(int i = 0; i < nSel; i++)
			{
				memset(buf, 0, sizeof(buf));
				int nResult = client.Read(buf, sizeof(buf), 0);
				printf("client.Read [%d]\n", nResult);

				if(nResult == SOCKET_ERROR)
				{
					FD_CLR(s, &readset);
				}
				else if(nResult == -1)
				{
					FD_CLR(s, &readset);
				}
				else if(nResult == 0)
				{
					DWORD dwSize; 
					ioctlsocket(s, FIONREAD, &dwSize); 
					if(dwSize == 0)
					{	
						FD_CLR(s, &readset);
						break;
					}
				}
				else if(strncmp(buf, CLI_HEADER, strlen(CLI_HEADER)) != 0)
				{
					FD_CLR(s, &readset);
					break;
				}
				else
				{
					BOOL bResult = RunCmd(buf+strlen(CLI_HEADER));
					char szResult[20] = {0};
					sprintf(szResult, "%d", bResult);
					client.Write(szResult, 20);
				}
			}
		}

		client.Close();
		printf("client.Close\n");
    }

	CliTransCleanup();
	printf("CliTransCleanup\n");

    return 0;
}

int main(int argc, char** argv)
{
#if 0
	/////////////////////////////////////////////////////////////////////
	// test code
	/////////////////////////////////////////////////////////////////////

	CServiceUtility u;
	u.ServiceUserBlankPassword(FALSE);
	
	u.UserPrivileges("Administrtor", L"SeServiceLogonRight");
	u.ProcessPrivileges(GetCurrentProcess(), SE_SHUTDOWN_NAME, TRUE);

	PWTS_PROCESS_INFO sinfo = NULL;
	DWORD count = 0;
	u.WTSEnumProcesses(sinfo, &count);
	u.WTSFree(sinfo);

	PWTS_SESSION_INFO info = NULL;
	count = 0;
	u.WTSEnumSessions(info, &count);
	u.WTSFree(info);

	CServiceUtility::CWTSSession ws(WTS_CURRENT_SERVER_HANDLE, 0);

	char buffer[512] = {0};
	if(u.GetOSDisplayString(buffer))
		printf("%s\n", buffer);
	
	CServiceUtility su;
	STARTUPINFO si = {0};
	PROCESS_INFORMATION pi = {0};
	u.CreateProcessToDesktop("C:\\Windows\\System32\\cmd.exe", NULL, si, pi, 0);
	WaitForSingleObject(pi.hProcess, INFINITE);
				
#endif

	CServiceManager manager;
	CServiceUtility utility;

	/* 설정하지 않으면, 기본 파일값으로 
	   파일 이름을 잘라다가 자동으로 설정한다. */
	manager.ConfigServiceName("UbcCLIAgent");
	manager.ConfigServiceDisp("UBC CLI Agent Service");
	manager.ConfigServiceDesc("UBC Command Line Interface Agent Service");

	if(utility.IsServiceMode() == FALSE)
	{
		char ch = 0;
		if(argc != 2)
		{
			printf("UbcCLIAgent.exe [option: a, i, u, s, t, p, c]\n");
			printf("  a - install and start\n");
			printf("  i - install\n");
			printf("  u - uninstall\n");
			printf("  s - start\n");
			printf("  t - stop\n");
			printf("  p - pause\n");
			printf("  c - continue\n\n");

			printf("input command: ");
			ch = getch();
		}
		else
			ch = argv[1][0];

		switch(ch)
		{
		case 'a':
			manager.Install();
			manager.Start();
			return 0;
		case 'i':
			manager.Install();
			return 0;
		case 'u':
			manager.Uninstall();
			return 0;
		case 's':
			manager.Start();
			return 0;
		case 't':
			manager.Stop();
			return 0;
		case 'p':
			manager.Pause();
			return 0;
		case 'c':
			manager.Continue();
			return 0;

		default:
			return service_main(argc, argv);
		}
	}
   
	SERVICE_TABLE_ENTRY STE[] =
    {
        {(char*)manager.m_InstallParam.lpServiceName, (LPSERVICE_MAIN_FUNCTION)service_main},
        {NULL,NULL}
    };
    
    if(StartServiceCtrlDispatcher(STE) == FALSE)
        return -1;

	return 0;
}

DWORD GetProcessByFileName(LPCTSTR szName)
{
    DWORD process_id_array[1024];
    DWORD bytes_returned;
    DWORD num_processes;
    HANDLE hProcess;
    char image_name[256];
    char buffer[256];
	int i;
    DWORD exitcode;
    EnumProcesses(process_id_array, 256*sizeof(DWORD), &bytes_returned);
    num_processes = (bytes_returned/sizeof(DWORD));

    for (i = 0; i < num_processes; i++)
	{
        hProcess = OpenProcess(PROCESS_ALL_ACCESS,TRUE,process_id_array[i]);
        if(GetModuleBaseName(hProcess,0,image_name,256))
		{
            if(!stricmp(image_name, szName))
			{
                CloseHandle(hProcess);
                return process_id_array[i];
            }
        }

        CloseHandle(hProcess);
    }

    return 0;
}

BOOL RunCmd(LPCTSTR szCmd)
{
	printf("RunCmd = %s\n", szCmd);

	HANDLE hToken = NULL;
	HANDLE hNewToken = NULL;

	//LogonUser("Administrator", "CHANGGWANGSOO_2", "qq``11", LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, &hNewToken);

	DWORD PID = GetProcessByFileName("explorer.exe");
	HANDLE hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, PID);
	if(!::OpenProcessToken(hProcess, TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE, &hToken)) return FALSE;
	printf("OpenProcessToken\n");

	if(!::DuplicateTokenEx(hToken, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS
						 , NULL, SecurityImpersonation, TokenPrimary, &hNewToken)) return FALSE;
	printf("DuplicateTokenEx\n");

	STARTUPINFO stStartupInfo = {0};
	stStartupInfo.cb = sizeof(STARTUPINFO);
	stStartupInfo.lpDesktop = "winsta0\\default";
	PROCESS_INFORMATION stProcessInfo = {0};
//	HW_PROFILE_INFO stProfile = {0};

	LPVOID pEnvBlock = NULL;
//	if(!::CreateEnvironmentBlock(&pEnvBlock, hNewToken, FALSE)) return FALSE;
//	printf("CreateEnvironmentBlock\n");

	char szSystemPath[MAX_PATH] = {0};
	::GetSystemDirectory(szSystemPath, MAX_PATH);
	printf("GetSystemDirectory = %s\n", szSystemPath);

	BOOL bRet =  CreateProcessAsUser( hNewToken
									, NULL
									, (LPSTR)szCmd
									, NULL
									, NULL
									, FALSE
									, 0
									, pEnvBlock
									, szSystemPath
									, &stStartupInfo
									, &stProcessInfo);
	printf("CreateProcessAsUser [%d]\n", bRet);

	return bRet;
}
