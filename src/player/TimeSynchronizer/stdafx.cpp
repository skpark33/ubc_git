// stdafx.cpp : source file that includes just the standard includes
// TimeSynchronizer.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"



LPCTSTR GetUBCVariablesIniPath()
{
	static CString	_iniDirectory = _T("");

	if(_iniDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_iniDirectory = str;
		_iniDirectory.Append(_T("data\\UBCVariables.ini"));
	}

	return _iniDirectory;
}
