#pragma once

#include <afxmt.h>


class CLockObject
{
public:
	CLockObject(CCriticalSection& lock);
	CLockObject(CCriticalSection* lock);
	CLockObject(CMutex& lock);
	CLockObject(CMutex* lock);

	virtual ~CLockObject();

	void Unlock();

protected:
	CMutex*				m_mutex;
	CCriticalSection*	m_cs;
	bool				m_bLock;
};


#define		LOCK(x)			CLockObject __lock__##x(x);
#define		UNLOCK(x)		__lock__##x.Unlock();


//////////////////////////////////////////////
//////////////////////////////////////////////

BOOL		DeleteThread(bool& bRunThread, CWinThread*& ppThread, DWORD dwWaitMilliSec=3000);

DWORD		GetTickDiff(DWORD dwStart, DWORD dwEnd);
LPCTSTR		GetErrorString(DWORD err_code);
