// TimeSyncObject.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TimeSynchronizer.h"
#include "TimeSyncInterface.h"

#include "BasicFunc.h"


// CTimeSyncObject

IMPLEMENT_DYNAMIC(CTimeSyncObject, CWnd)

CTimeSyncObject::CTimeSyncObject(HWND hParentWnd)
{
	m_bStartSync = false;
	m_hParentWnd = hParentWnd;
	m_strServerIP = "127.0.0.1";
	m_nPort = 8081;
	m_nInterval = 60;
}

CTimeSyncObject::~CTimeSyncObject()
{
}


BEGIN_MESSAGE_MAP(CTimeSyncObject, CWnd)
END_MESSAGE_MAP()



// CTimeSyncObject 메시지 처리기입니다.


BOOL CTimeSyncObject::Create()
{
	return CWnd::Create(NULL, "TIME_SYNC_OBEJCT", WS_CHILD, CRect(0,0,0,0), CWnd::FromHandle(m_hParentWnd), 0xfeff);
}

void CTimeSyncObject::SetInfo(LPCSTR lpszServerIP, LPCSTR lpszTerminalID, UINT nPort, int nInterval)
{
	m_strServerIP = lpszServerIP;
	m_strTerminalID = lpszTerminalID;
	m_nPort = nPort;
	m_nInterval = nInterval;
}

LPCSTR CTimeSyncObject::GetInfoString()
{
	if( m_strInfo.GetLength() == 0 )
		m_strInfo.Format("ServerIP=%s, TerminalID=%s, Port=%u, Interval=%d", m_strServerIP, m_strTerminalID, m_nPort, m_nInterval);

	return m_strInfo;
}

///////////////////////////////////////////////////////////////////////////////

CAsyncSocketEx::CAsyncSocketEx()
{
	m_bInitialized = false;
	m_hParentWnd = NULL;
	m_nTerminalID = 0;
	m_nPeerPort = 0;
}

CAsyncSocketEx::~CAsyncSocketEx()
{
}

bool CAsyncSocketEx::PopDataFromBuffer(char*& pBuf, int& nBufLen, int nPopSize)
{
	int left_size = nBufLen - nPopSize;
	char* tmp_buf = new char[left_size];
	if( tmp_buf == NULL ) return false;

	memcpy(tmp_buf, pBuf+nPopSize, left_size);
	memcpy(pBuf, tmp_buf, left_size);
	delete[]tmp_buf;

	nBufLen = left_size;

	return true;
}

bool CAsyncSocketEx::IncreaseBufferSize(char*& pBuf, int& nMaxBuffSize)
{
	int new_max_size = nMaxBuffSize + BUFFER_SIZE;
	char* new_buffer = new char[new_max_size];
	if( new_buffer == NULL ) return false;

	memcpy(new_buffer, pBuf, nMaxBuffSize);
	delete[]pBuf;

	pBuf = new_buffer;
	nMaxBuffSize = new_max_size;

	return true;
}

CString CAsyncSocketEx::BinaryDataToString(LPBYTE pData, int len)
{
	if(pData == NULL || len == 0) return "No Data";

	CString ret_val = "";
	CString str_tmp;

	for(int i=0; i<len; i++)
	{
		BYTE byte = pData[i];
		if( 32 <= byte || byte < 127 )
			ret_val.AppendChar((char)byte);
		else
			ret_val += " ";

		str_tmp.Format("(%02X) ", byte);
		ret_val += str_tmp;
	}

	return ret_val;
}

LPCTSTR CAsyncSocketEx::GetSocketInfo()
{
	if( m_strInfo.GetLength() == 0 )
		m_strInfo.Format("Addr=0x%08X, Peer=%s:%d, ID=NoID", this, m_strPeerIP, m_nPeerPort);
	return m_strInfo;
}

LPCTSTR CAsyncSocketEx::GetSocketInfo(DWORD dwErrCode)
{
	m_strInfoEx.Format("%s, ErrCode=%s", GetSocketInfo(), ::GetErrorString(dwErrCode));
	return m_strInfoEx;
}
