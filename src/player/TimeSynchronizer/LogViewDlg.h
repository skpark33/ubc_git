#pragma once

#include "afxcmn.h"
#include "TimeSyncInterface.h"

#include "LogListCtrl.h"


// CLogViewDlg 대화 상자입니다.

class CLogViewDlg : public CDialog
{
	DECLARE_DYNAMIC(CLogViewDlg)

protected:
	static CLogViewDlg*	_instance;
public:
	static CLogViewDlg* getInstance(CWnd* pParentWnd=NULL);
	static void clearInstance();

protected:
	CLogViewDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.

public:
	virtual ~CLogViewDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	//enum { IDD = IDD_LOG_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);

	//CListCtrl		m_lcLog;
	CLogListCtrl	m_lcLog;

	void	AddLog(LOG_TYPE eType, LPCSTR lpszLog);
};
