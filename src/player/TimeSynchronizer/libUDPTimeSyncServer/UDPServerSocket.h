#pragma once

class CUDPTimeSyncServer;


// CUDPServerSocket 명령 대상입니다.

class CUDPServerSocket : public CAsyncSocket
{
public:
	CUDPServerSocket(HWND hParentWnd, CUDPTimeSyncServer* pSyncObject, UINT nPort, int nInterval);
	virtual ~CUDPServerSocket();

	virtual void OnClose(int nErrorCode);

	BOOL		StartSync();
	BOOL		StopSync();

	void		Reconnect();

protected:
	HWND		m_hParentWnd;
	CUDPTimeSyncServer*	m_pSyncObject;
	UINT		m_nPort;
	int			m_nInterval;

	bool		m_bStartSync;

	bool		m_bRunThread;
	CWinThread*	m_pSyncThread;
	static UINT	SyncThreadFunc(LPVOID pParam);

};
