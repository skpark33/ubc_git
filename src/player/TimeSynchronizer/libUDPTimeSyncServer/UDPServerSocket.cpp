// UDPServerSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UDPServerSocket.h"

#include "UDPTimeSyncServer.h"


typedef struct {
	bool*				pRunThread;
	UINT				nPort;
	int 				nInterval;
	CUDPServerSocket*	pSocket;
	HWND				hParentWnd;
} UDP_SERVER_SOCKET_THREAD_PARAM;


////////////////////////////////////////////////////////////////////////
// CUDPServerSocket

CUDPServerSocket::CUDPServerSocket(HWND hParentWnd, CUDPTimeSyncServer* pSyncObject, UINT nPort, int nInterval)
:	m_hParentWnd ( hParentWnd )
,	m_pSyncObject ( pSyncObject )
,	m_nPort ( nPort )
,	m_nInterval ( nInterval )

,	m_bStartSync ( false )

,	m_bRunThread ( false )
,	m_pSyncThread ( NULL )
{
	__FUNC_BEGIN__;

	__FUNC_END__;
}

CUDPServerSocket::~CUDPServerSocket()
{
	__FUNC_BEGIN__;

	StopSync();

	__FUNC_END__;
}


// CUDPServerSocket 멤버 함수

void CUDPServerSocket::OnClose(int nErrorCode)
{
	__DEBUG__( ("begin... (%s)", ::GetErrorString(nErrorCode)) );

	if( nErrorCode )
	{
		// OnClose error
		CString msg;
		msg.Format("OnClose ERROR !!! (%s)", ::GetErrorString(nErrorCode));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);
	}
	else
	{
		// OnClose error
		CString msg;
		msg.Format("OnClose");
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);
	}

	Reconnect();

	CAsyncSocket::OnClose(nErrorCode);

	__FUNC_END__;
}

BOOL CUDPServerSocket::StartSync()
{
	__FUNC_BEGIN__;

	if( m_bStartSync )
	{
		__DEBUG__( ("Already Start") );
		return TRUE;
	}

	if( !Create(m_nPort, SOCK_DGRAM) )
	{
		// Create ERROR !!!
		Reconnect();

		CString msg;
		msg.Format("Create ERROR !!! (%s)", ::GetErrorString(::GetLastError()));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		return FALSE;
	}

	static char ch_broadcast = 1;
	if( !SetSockOpt(SO_BROADCAST, (char*)&ch_broadcast, sizeof(ch_broadcast)) )
	{
		// SetSockOpt ERROR !!!
		Reconnect();

		CString msg;
		msg.Format("SetSockOpt(SO_BROADCAST) ERROR !!! (%s)", ::GetErrorString(::GetLastError()));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		return FALSE;
	}

	static int no_delay = 1;
	if( !SetSockOpt(TCP_NODELAY, &no_delay, sizeof(no_delay)) )
	{
		// SetSockOpt ERROR !!!
		Reconnect();

		CString msg;
		msg.Format("SetSockOpt(TCP_NODELAY) ERROR !!! (%s)", ::GetErrorString(::GetLastError()));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		return FALSE;
	}

	// create sending-thread
	UDP_SERVER_SOCKET_THREAD_PARAM* thread_param = new UDP_SERVER_SOCKET_THREAD_PARAM;
	if( thread_param == NULL )
	{
		// create param ERROR --> out of memory? --> reconnect
		Reconnect();

		CString msg;
		msg.Format("Create Thread-Param ERROR !!!");
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);

		return FALSE;
	}

	thread_param->pRunThread = &m_bRunThread;
	thread_param->nPort		 = m_nPort;
	thread_param->nInterval	 = m_nInterval;
	thread_param->pSocket	 = this;
	thread_param->hParentWnd = m_hParentWnd;

	m_bRunThread = true;
	m_pSyncThread = ::AfxBeginThread(SyncThreadFunc, (LPVOID)thread_param, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
	if( m_pSyncThread == NULL )
	{
		// AfxBeginThread ERROR --> out of memory? --> reconnect
		m_bRunThread = false;
		delete thread_param;

		Reconnect();

		CString msg;
		msg.Format("AfxBeginThread ERROR !!!");
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);

		return FALSE;
	}

	m_pSyncThread->m_bAutoDelete = FALSE;
	m_pSyncThread->ResumeThread();

	m_bStartSync = true;

	return TRUE;
}

BOOL CUDPServerSocket::StopSync()
{
	::DeleteThread(m_bRunThread, m_pSyncThread, 1000);

	m_bStartSync = false;

	return TRUE;
}

void CUDPServerSocket::Reconnect()
{
	if( m_pSyncObject ) m_pSyncObject->Reconnect();
}

UINT CUDPServerSocket::SyncThreadFunc(LPVOID pParam)
{
	UDP_SERVER_SOCKET_THREAD_PARAM* thread_param = (UDP_SERVER_SOCKET_THREAD_PARAM*)pParam;

	bool*				run_thread	= thread_param->pRunThread;
	UINT				port		= thread_param->nPort;
	int 				interval	= thread_param->nInterval;
	CUDPServerSocket*	sock		= thread_param->pSocket;
	HWND				parent_wnd	= thread_param->hParentWnd;

	delete thread_param;

	bool is_sync_signal = true;
	time_t last_sync_time = 0;
	time_t last_hb_time = 0;
	while( *run_thread )
	{
		time_t tm_now = CTime::GetCurrentTime().GetTime();

		if( tm_now < (last_sync_time + interval) )
		{
			if( tm_now < (last_hb_time + 10) ) // heart beat interval = 10sec
			{
				::Sleep(1);
				continue;
			}
			else
			{
				is_sync_signal = false;
			}
		}
		else
		{
			is_sync_signal = true;

			last_sync_time = tm_now;
		}

		last_hb_time = tm_now;

		if( is_sync_signal )
		{
			// time sync

			// send header to buffer
			char buf[24+1] = "_SS_                _SE_"; // (S)ync(E)nd
			//                012345678901234567890123 = 24 byte
			//                    45678901             = 8 byte, time_t (now)
			//                            23456789     = 8 byte, time_t (now xor)

			memcpy(buf+4, &last_sync_time, sizeof(last_sync_time)); // set time data

			time_t verify_time = ~last_sync_time;
			memcpy(buf+12, &verify_time, sizeof(verify_time)); // set verify-data(XOR)

			//
			int total_size = 24;
			int send_size = 0;
			while(total_size > send_size && *run_thread)
			{
				int size = sock->SendTo(buf+send_size, total_size-send_size, port);
				if( size == SOCKET_ERROR )
				{
					DWORD err_code = ::GetLastError();

					sock->Close();
					sock->Reconnect();

					CString msg;
					msg.Format("SendTo ERROR !!! (%s)", ::GetErrorString(err_code));
					__WARN__( ((LPCTSTR)msg) );
					POST_WARN(parent_wnd, msg);

					return FALSE;
				}
				send_size += size;
			}

			::PostMessage(parent_wnd, WM_SYNC_SERVER, NULL, NULL);
		}
		else
		{
			// heart beat

			char buf[4+1] = "_HB_"; // (H)eart(B)eat

			int total_size = 4;
			int send_size = 0;
			while(total_size > send_size && *run_thread)
			{
				int size = sock->SendTo(buf+send_size, total_size-send_size, port);
				if( size == SOCKET_ERROR )
				{
					DWORD err_code = ::GetLastError();

					sock->Close();
					sock->Reconnect();

					CString msg;
					msg.Format("SendTo ERROR !!! (%s)", ::GetErrorString(err_code));
					__WARN__( ((LPCTSTR)msg) );
					POST_WARN(parent_wnd, msg);

					return FALSE;
				}
				send_size += size;
			}
		}
	}

	return TRUE;
}
