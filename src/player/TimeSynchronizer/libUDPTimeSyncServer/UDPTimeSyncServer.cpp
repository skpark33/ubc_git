#include "StdAfx.h"

#include "UDPTimeSyncServer.h"

#include "UDPServerSocket.h"


// CUDPTimeSyncServer

IMPLEMENT_DYNAMIC(CUDPTimeSyncServer, CWnd)


CUDPTimeSyncServer::CUDPTimeSyncServer(HWND hParentWnd)
: CTimeSyncObject(hParentWnd)
{
	__FUNC_BEGIN__;

	m_pServerSocket = NULL;

	__FUNC_END__;
}

CUDPTimeSyncServer::~CUDPTimeSyncServer()
{
	__FUNC_BEGIN__;

	StopSync();

	__FUNC_END__;
}


BEGIN_MESSAGE_MAP(CUDPTimeSyncServer, CWnd)
	ON_WM_TIMER()
END_MESSAGE_MAP()


BOOL CUDPTimeSyncServer::StartSync()
{
	__FUNC_BEGIN__;

	if( m_bStartSync && m_pServerSocket )
	{
		__DEBUG__( ("Already Start (%s)", GetInfoString()) );
		return TRUE;
	}

	m_bStartSync = true;
	KillTimer(TIMER_ID_RECONNECT);

	m_pServerSocket = new CUDPServerSocket(m_hParentWnd, this, m_nPort, m_nInterval);
	if( m_pServerSocket == NULL )
	{
		// create CUDPServerSocket ERROR --> out of memory?
		CString msg;
		msg.Format("Create UDPServerSocket ERROR !!! (%s)", GetInfoString());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);

		Reconnect();

		return FALSE;
	}
	else if( !m_pServerSocket->StartSync() )
	{
		// StartSync ERROR !!! --> reconnect
		delete m_pServerSocket;
		m_pServerSocket = NULL;

		Reconnect();

		return FALSE;
	}

	__FUNC_END__;

	return TRUE;
}

BOOL CUDPTimeSyncServer::StopSync()
{
	__FUNC_BEGIN__;

	m_bStartSync = false;

	if( m_pServerSocket != NULL )
	{
		// delete ListenSocket
		__DEBUG__( ("Delete UDPServerSocket (%s)", GetInfoString()) );
		m_pServerSocket->StopSync();
		delete m_pServerSocket;
		m_pServerSocket = NULL;
	}

	__FUNC_END__;

	return TRUE;
}

void CUDPTimeSyncServer::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case TIMER_ID_RECONNECT:
		{
			__DEBUG__( ("OnTimer TIMER_ID_RECONNECT (%d)", m_bStartSync) );

			KillTimer(nIDEvent);
			if( m_bStartSync )
			{
				StopSync();
				StartSync();
			}
		}
		break;
	}

	CTimeSyncObject::OnTimer(nIDEvent);
}

void CUDPTimeSyncServer::Reconnect()
{
	__FUNC_BEGIN__;

	SetTimer(TIMER_ID_RECONNECT, TIMER_TIME_RECONNECT, NULL);

	__FUNC_END__;
}
