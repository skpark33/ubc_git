#pragma once

class CUDPServerSocket;


// CUDPTimeSyncServer 명령 대상입니다.

class CUDPTimeSyncServer : public CTimeSyncObject
{
	DECLARE_DYNAMIC(CUDPTimeSyncServer)

public:
	CUDPTimeSyncServer(HWND hParentWnd);
	virtual ~CUDPTimeSyncServer(void);

protected:
	CUDPServerSocket*	m_pServerSocket;

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	virtual	BOOL	StartSync();
	virtual	BOOL	StopSync();

	void	Reconnect();
};
