#include <ci/libDebug/ciDebug.h>
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libConfig/ciIni.h>
#include <hi/libHttp/hiHttpWrapper.h>

#include "CMN/libCommon/ubcIni.h"
#include "TimeSyncTimer.h"

ciSET_DEBUG(1, "TimeSyncTimer");

TimeSyncTimer::TimeSyncTimer() 
: _interval(0), _timeHost(""), _timePort(0)
{
	ciDEBUG(3, ("TimeSyncTimer()"));
	if(ciIniUtil::isHttpOnly()){
		this->setCom_MSC(ciTrue);
	}

}

TimeSyncTimer::~TimeSyncTimer() {
	ciDEBUG(3, ("~TimeSyncTimer()"));
}

void 
TimeSyncTimer::initTimer(const char* pname, int interval) {
	ciDEBUG(3, ("initTimer(%s,%d)", pname, interval));

	_interval = interval;
	 ciPollable::initTimer(pname, interval);


}

void 
TimeSyncTimer::setHostInfo(const char* timeHost, int timePort)
{
	_timeHost = timeHost;
	_timePort = timePort;
}

ciBoolean 
TimeSyncTimer::setHostInfoFromIni()
{
	ubcConfig  aIni("UBCConnect");
	if (!aIni.get("TIMESYNCER","IP", _timeHost)) {
		ciERROR(("[TIMESYNCER]IP is not found."));
		return ciFalse;
	}
	ciShort timePort = 0;
	if (!aIni.get("TIMESYNCER","PORT", timePort)) {
		ciERROR(("[TIMESYNCER]PORT is not found."));
		return ciFalse;
	}
	_timePort = timePort;
	return ciTrue;
}

void
TimeSyncTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(9,("processExpired(%s,%d,%d)", 
		name.c_str(), counter, interval));  // 무조건 600초 + 99초 마다 들어온다.
	
	static int successPoint = 0;
	if(counter == 1){
		// 단말이 몰리는 것을 막기위해 처음은 건너뛴다.
		return ;
	}
	
	if(successPoint > 0 && counter - successPoint < 60 ){  
		// 처음에는 무조건 동기화
		// 앞전에 실패했으면 무조건 동기화
		// 앞전에 성공했으면 (300+120)*60 초마다 한번씩만 동기화한다.
		return;
	}

	// Time Sync
	if(	timeSync(_timeHost.c_str(), _timePort) == ciFalse){
		ciERROR(("Time sync failed"));
		successPoint = 0;
	}else{
		successPoint = counter;
	}

	ciTime now;
	ubcConfig  aIni("UBCVariables");
	aIni.set("ROOT","TimeSyncTime", now.getTimeString().c_str() );
	aIni.set("ROOT","TimeSyncResult", ciBoolean((successPoint>0 ? ciTrue : ciFalse)));
}

#include <ace/INET_Addr.h>
#include <ace/SOCK_Connector.h>
#include <ace/SOCK_Stream.h>

ciBoolean 
TimeSyncTimer::timeSync()
{
	TimeSyncTimer instance;
	instance.setHostInfoFromIni();
	ciBoolean retval = TimeSyncTimer::timeSync(instance._timeHost.c_str(), instance._timePort);

	ciTime now;
	ubcConfig  aIni("UBCVariables");
	aIni.set("ROOT","TimeSyncTime2", now.getTimeString().c_str() );
	aIni.set("ROOT","TimeSyncResult2", retval);

	return retval;
}

ciBoolean 
TimeSyncTimer::timeSync(const char* timeHost, int timePort)
{

    time_t current = 0;

	if(ciIniUtil::isHttpOnly()){
		hiHttpWrapper aWrapper;  // main 에 접속한다.
		if(!aWrapper.call("TIME",timeHost,timePort,"","")){
			ciERROR(("http time get failed(%s)", aWrapper.getErrorMsg()));
			return ciFalse;
		}
		ciString timeVal;
		if(aWrapper.getResultMsg(timeVal)==ciFalse || timeVal.empty()){
			ciERROR(("http time get failed"));
			return ciFalse;
		}
		current = atol(timeVal.c_str());
	}else{
	
		int result;
		ACE_INET_Addr server_addr;

		if (timePort != 0)
			result = server_addr.set (timePort, timeHost);
		else
			result = server_addr.set ("timesyncer", timeHost);
		if (result == -1) {
			ciERROR(("invalid address. %s:%d", timeHost, timePort));
			return ciFalse;
		}

		ACE_SOCK_Connector connector;
		ACE_SOCK_Stream peer;

		if (connector.connect (peer , server_addr) < 0) {
			ciERROR(("connect() error. %s:%d", timeHost, timePort));
			return ciFalse;
		}
		char buf[256];
		memset(buf, 0x00, sizeof(buf));
		if (peer.recv (buf, sizeof(buf)) == -1) {
			ciERROR(("recv() error. %s:%d", timeHost, timePort));
			peer.close();
			return ciFalse;
		}
		peer.close();
		current = atol(buf);
	}
	
	ciTime aTime(current);			
	ciDEBUG(1,("recv time %s", aTime.getTimeString().c_str()));

	SYSTEMTIME localTime;
	::GetLocalTime(&localTime);

	localTime.wYear = aTime.getYear();
	localTime.wMonth = aTime.getMonth();
	localTime.wDay = aTime.getDay();
	localTime.wHour = aTime.getHour();
	localTime.wMinute = aTime.getMinute();
	localTime.wSecond = aTime.getSecond();

	if (!::SetLocalTime(&localTime)) {
		int nErrCode = ::GetLastError();
		LPTSTR szErr;
		int nRet = ::FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			(DWORD)nErrCode,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&szErr,
			0,
			NULL);

		if ( nRet != 0 ) {
			ciERROR(("::SetSystemTime() : Error [%d]%s", nErrCode, szErr));
		}
	}
	ciDEBUG(9,("timeSync complete"));
	return ciTrue;
}



ciBoolean 
TimeSyncTimer::_connectTest(int timeout)
{
	if(!setHostInfoFromIni()){
		ciERROR(("setHostInfoFromIni() failed"));
		return ciFalse;
	}

    int result;
    ACE_INET_Addr server_addr;

    if (_timePort != 0)
		result = server_addr.set (_timePort, _timeHost.c_str());
    else
		result = server_addr.set ("timesyncer", _timeHost.c_str());

	if (result == -1) {
		ciERROR(("invalid address. %s:%d", _timeHost.c_str(), _timePort));
		return ciFalse;
	}

    ACE_SOCK_Connector connector;
    ACE_SOCK_Stream peer;
	ACE_Time_Value aTimeval;
	aTimeval.set(timeout,0);

    if (connector.connect (peer , server_addr, &aTimeval) < 0) {
		ciERROR(("connect() error. %s:%d", _timeHost.c_str(), _timePort));
		return ciFalse;
	}
	peer.close();
	ciDEBUG(1,("connect succeed : %s:%d", _timeHost.c_str(), _timePort));
	return ciTrue;
}
