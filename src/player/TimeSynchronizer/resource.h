//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TimeSynchronizer.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_TIMESYNCHRONIZER_FORM       101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_TimeSynchronizeTYPE         129
#define IDI_ALERT                       130
#define IDI_NORMAL                      131
#define IDD_LOG_VIEW                    132
#define IDI_LOG                         133
#define IDI_WARN                        134
#define IDI_ICON1                       135
#define IDI_STOP                        135
#define IDC_LIST_CLIENTS                1001
#define IDC_EDIT_PORT                   1002
#define IDC_EDIT_INTERVAL               1003
#define IDC_BUTTON_START                1004
#define IDC_BUTTON_STOP                 1005
#define IDC_COMBO_TYPE                  1007
#define IDC_STATIC_STATUS               1009
#define IDC_STATIC_CURRENT_TIME         1010
#define IDC_BUTTON_SET_TO_DEFAULT       1011
#define IDC_STATIC_STATUS_ICON          1012
#define IDC_STATIC_TERMINAL_ID          1013
#define IDC_LIST_LOG                    1013
#define IDC_STATIC_TERMINAL_IP          1014
#define IDC_STATIC_LAST_SYNC_TIME       1015
#define IDC_STATIC_SERVER_IP            1016
#define ID_SHOW_LOGVIEW                 32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
