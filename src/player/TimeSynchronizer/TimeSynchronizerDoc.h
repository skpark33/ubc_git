// TimeSynchronizerDoc.h : interface of the CTimeSynchronizerDoc class
//


#pragma once


class CTimeSynchronizerDoc : public CDocument
{
protected: // create from serialization only
	CTimeSynchronizerDoc();
	DECLARE_DYNCREATE(CTimeSynchronizerDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CTimeSynchronizerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


