// TimeSynchronizerDoc.cpp : implementation of the CTimeSynchronizerDoc class
//

#include "stdafx.h"
#include "TimeSynchronizer.h"

#include "TimeSynchronizerDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTimeSynchronizerDoc

IMPLEMENT_DYNCREATE(CTimeSynchronizerDoc, CDocument)

BEGIN_MESSAGE_MAP(CTimeSynchronizerDoc, CDocument)
END_MESSAGE_MAP()


// CTimeSynchronizerDoc construction/destruction

CTimeSynchronizerDoc::CTimeSynchronizerDoc()
{
	// TODO: add one-time construction code here

}

CTimeSynchronizerDoc::~CTimeSynchronizerDoc()
{
}

BOOL CTimeSynchronizerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CTimeSynchronizerDoc serialization

void CTimeSynchronizerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CTimeSynchronizerDoc diagnostics

#ifdef _DEBUG
void CTimeSynchronizerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTimeSynchronizerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CTimeSynchronizerDoc commands
