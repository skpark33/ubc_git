// TCPServerSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TCPServerSocket.h"


typedef struct {
	bool*				pRunThread;
	CCriticalSection*	pCS;
	char**				ppBuf;
	int*				buff_len;
	CTCPServerSocket*	pSocket;
	HWND				hParentWnd;
} TCP_SERVER_SOCKET_THREAD_PARAM;


////////////////////////////////////////////////////////////////////////
// CTCPServerSocket

CTCPServerSocket::CTCPServerSocket(HWND hParentWnd)
:	m_bValid ( true )

,	m_bRunThread ( false )
,	m_pSendThread ( NULL )

,	m_nMaxRecvBufferSize ( BUFFER_SIZE )
,	m_pRecvBuffer ( NULL )
,	m_nRecvBufferLen ( 0 )

,	m_nMaxSendBufferSize ( BUFFER_SIZE )
,	m_pSendBuffer ( NULL )
,	m_nSendBufferLen ( 0 )
{
	__FUNC_BEGIN__;

	m_hParentWnd = hParentWnd;

	// create buffer
	m_pRecvBuffer = new char[BUFFER_SIZE];
	m_pSendBuffer = new char[BUFFER_SIZE];
	if( m_pRecvBuffer == NULL || m_pSendBuffer == NULL )
	{
		// create buffer ERROR --> out of memory?
		CString msg;
		msg.Format("Create buffer ERROR !!! (%s)", GetSocketInfo());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);
	}

	// set time to now
	SetSendTimeToNow();
	SetRecvTimeToNow();

	// create sending-thread
	TCP_SERVER_SOCKET_THREAD_PARAM* thread_param = new TCP_SERVER_SOCKET_THREAD_PARAM;
	if( thread_param == NULL )
	{
		// create param ERROR --> out of memory?
		CString msg;
		msg.Format("Create Thread-Param ERROR !!! (%s)", GetSocketInfo());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);
	}
	else
	{
		thread_param->pRunThread = &m_bRunThread;
		thread_param->pCS		 = &m_csSendBuffer;
		thread_param->ppBuf		 = &m_pSendBuffer;
		thread_param->buff_len	 = &m_nSendBufferLen;
		thread_param->pSocket	 = this;
		thread_param->hParentWnd = hParentWnd;

		m_bRunThread = true;
		m_pSendThread = ::AfxBeginThread(SendBufferFunc, (LPVOID)thread_param, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
		if( m_pSendThread == NULL )
		{
			// AfxBeginThread ERROR --> out of memory?
			m_bRunThread = false;
			delete thread_param;

			CString msg;
			msg.Format("AfxBeginThread ERROR !!! (%s)", GetSocketInfo());
			__ERROR__( ((LPCTSTR)msg) );
			POST_ERROR(m_hParentWnd, msg);
		}
		else
		{
			m_pSendThread->m_bAutoDelete = FALSE;
			m_pSendThread->ResumeThread();
		}
	}

	__FUNC_END__;
}

CTCPServerSocket::~CTCPServerSocket()
{
	__FUNC_BEGIN__;

	::DeleteThread(m_bRunThread, m_pSendThread, 1000);

	{
		// delete recv-buffer
		LOCK(m_csRecvBuffer);
		if( m_pRecvBuffer ) delete[]m_pRecvBuffer;
		m_pRecvBuffer = NULL;
		m_nMaxRecvBufferSize = 0;
		m_nRecvBufferLen = 0;
	}

	{
		// delete send-buffer
		LOCK(m_csSendBuffer);
		if( m_pSendBuffer ) delete[]m_pSendBuffer;
		m_pSendBuffer = NULL;
		m_nMaxSendBufferSize = 0;
		m_nSendBufferLen = 0;
	}

	__FUNC_END__;
}


// CTCPServerSocket 멤버 함수

void CTCPServerSocket::OnConnect(int nErrorCode)
{
	CAsyncSocketEx::OnConnect(nErrorCode);
}

void CTCPServerSocket::OnClose(int nErrorCode)
{
	__DEBUG__( ("begin... (%s)", GetSocketInfo(nErrorCode)) );

	if( nErrorCode )
	{
		// OnClose error
		__WARN__( ("OnClose ERROR !!! (%s)", GetSocketInfo(nErrorCode)) );
	}

	SetInvalid();

	CAsyncSocketEx::OnClose(nErrorCode);

	__FUNC_END__;
}

void CTCPServerSocket::OnReceive(int nErrorCode)
{
	__DEBUG__( ("begin... (%s)", GetSocketInfo(nErrorCode)) );

	if( nErrorCode )
	{
		// OnReceive error
		SetInvalid();

		CString msg;
		msg.Format("OnReceive ERROR !!! (%s)", GetSocketInfo(nErrorCode));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);
	}
	else
	{
		// OnReceive OK
		char buf[BUFFER_SIZE];
		int read_size = Receive(buf, BUFFER_SIZE);

		switch( read_size )
		{
		case 0:
			// read nothing (socket is closed)
			{
				SetInvalid();

				CString msg;
				msg.Format("Receive ZERO !!! (%s)", GetSocketInfo());
				__WARN__( ((LPCTSTR)msg) );
				POST_WARN(m_hParentWnd, msg);
			}
			break;

		case SOCKET_ERROR:
			{
				DWORD err_code = GetLastError();
				if( err_code != WSAEWOULDBLOCK )
				{
					// socket error occurred --> close socket
					SetInvalid();

					CString msg;
					msg.Format("Receive ERROR !!! (%s)", GetSocketInfo(err_code) );
					__WARN__( ((LPCTSTR)msg) );
					POST_WARN(m_hParentWnd, msg);
				}
			}
			break;

		default:
			{
				LOCK(m_csRecvBuffer);

				bool buffer_ok = (m_nRecvBufferLen + read_size < m_nMaxRecvBufferSize); // not overflow
				if( !buffer_ok )
				{
					__DEBUG__(("Increase(Recv)Buffer (%s, OldMaxSize=%d)", GetSocketInfo(), m_nMaxRecvBufferSize));
					// overflow ==> increase buffer-size
					buffer_ok = IncreaseBufferSize(m_pRecvBuffer, m_nMaxRecvBufferSize); 
				}

				if( !buffer_ok )
				{
					CString msg;
					msg.Format("Increase(Recv)Buffer ERROR !!! (%s)", GetSocketInfo() );
					__ERROR__( ((LPCTSTR)msg) );
					POST_ERROR(m_hParentWnd, msg);
				}
				else
				{
					// attach recv-data to buffer
					memcpy(m_pRecvBuffer + m_nRecvBufferLen, buf, read_size);
					m_nRecvBufferLen += read_size;
				}
			}

			__DEBUG__( ("PacketAnalysis (init:%d, size:%d)", m_bInitialized, m_nRecvBufferLen) );
			PacketAnalysis();

			break;
		}
	}

	CAsyncSocketEx::OnReceive(nErrorCode);

	__FUNC_END__;
}

void CTCPServerSocket::PacketAnalysis()
{
	LOCK(m_csRecvBuffer);

	if( m_bInitialized == false )
	{
		// not recv init-header;

		// header : "_IS_      Terminal-ID       _IE_"; // (I)nit(S)tart - Terminal-ID(24byte) - (I)nit(E)nd
		//           01234567890123456789012345678901 = 32 byte

		if( m_nRecvBufferLen < 32 ) return; // not enough header size

		if( memcmp(m_pRecvBuffer,    "_IS_", 4) != 0 ||
			memcmp(m_pRecvBuffer+28, "_IE_", 4) != 0 )
		{
			// wrong header
			SetInvalid();

			CString msg;
			msg.Format("Wrong Header !!! (%s, HeaderSize=%d)", GetSocketInfo(), m_nRecvBufferLen);
			__WARN__( ((LPCTSTR)msg) );
			__WARN__( ((LPCTSTR)BinaryDataToString((LPBYTE)m_pRecvBuffer, (m_nRecvBufferLen>50 ? 50:m_nRecvBufferLen))) );
			POST_WARN(m_hParentWnd, msg);

			// auto disconnect by timer
			return;
		}

		//
		m_pRecvBuffer[28] = 0;
		m_strTerminalID = m_pRecvBuffer+4;
		m_strTerminalID.Trim();
		if( m_strTerminalID.Find("-") < 0 )
		{
			// wrong header
			SetInvalid();

			CString msg;
			msg.Format("Wrong Header !!! (%s, HeaderSize=%d)", GetSocketInfo(), m_nRecvBufferLen);
			__WARN__( ((LPCTSTR)msg) );
			__WARN__( ((LPCTSTR)BinaryDataToString((LPBYTE)m_pRecvBuffer, (m_nRecvBufferLen>50 ? 50:m_nRecvBufferLen))) );
			POST_WARN(m_hParentWnd, msg);

			// auto disconnect by timer
			return;
		}

		//
		int pos = 0;
		CString str_id = m_strTerminalID.Tokenize("-", pos);
		str_id = m_strTerminalID.Tokenize("-", pos); // get license-number

		m_nTerminalID = atoi(str_id);
		if( m_nTerminalID == 0 )
		{
			// wrong header
			SetInvalid();

			CString msg;
			msg.Format("Wrong Header !!! (%s, HeaderSize=%d)", GetSocketInfo(), m_nRecvBufferLen);
			__WARN__( ((LPCTSTR)msg) );
			__WARN__( ((LPCTSTR)BinaryDataToString((LPBYTE)m_pRecvBuffer, (m_nRecvBufferLen>50 ? 50:m_nRecvBufferLen))) );
			POST_WARN(m_hParentWnd, msg);

			// auto disconnect by timer
			return;
		}

		m_strInfo.Format("Addr=0x%08X, Peer=%s:%d, ID=%s", this, m_strPeerIP, m_nPeerPort, m_strTerminalID);

		if( !PopDataFromBuffer(m_pRecvBuffer, m_nRecvBufferLen, 32) ) // pop valid-header
		{
			CString msg;
			msg.Format("PopDataFromBuffer ERROR !!! (%s)", GetSocketInfo());
			__ERROR__( ((LPCTSTR)msg) );
			POST_ERROR(m_hParentWnd, msg);
		}

		m_bInitialized = true;
	}
	else
	{
		if( m_nRecvBufferLen < 4 ) return; // not enough header size

		for(int i=m_nRecvBufferLen-4; i>=0; i--)
		{
			if( memcmp(m_pRecvBuffer, "_SO_", 4) == 0 ) // (S)ync(O)K
			{
				__DEBUG__( ("ServerSocket-Response OK (%s)", GetSocketInfo()) );
				SetRecvTimeToNow();
				if( !PopDataFromBuffer(m_pRecvBuffer, m_nRecvBufferLen, i+4) ) // pop lastest valid-header (with previous all data)
				{
					CString msg;
					msg.Format("PopDataFromBuffer ERROR !!! (%s)", GetSocketInfo());
					__ERROR__( ((LPCTSTR)msg) );
					POST_ERROR(m_hParentWnd, msg);
				}
				::PostMessage(m_hParentWnd, WM_SYNC_TERMINAL, m_nTerminalID, NULL);
				break;
			}
		}
	}
}

BOOL CTCPServerSocket::AddToSendBuffer(char* pAddBuf, int len)
{
	LOCK(m_csSendBuffer);

	bool buffer_ok = (m_nSendBufferLen + len < m_nMaxSendBufferSize); // not overflow
	if( !buffer_ok )
	{
		__DEBUG__(("Increase(Send)Buffer (%s, OldMaxSize=%d)", GetSocketInfo(), m_nMaxSendBufferSize));
		// overflow ==> increase buffer-size
		buffer_ok = IncreaseBufferSize(m_pSendBuffer, m_nMaxSendBufferSize); 
	}

	if( !buffer_ok )
	{
		CString msg;
		msg.Format("Increase(Send)Buffer ERROR !!! (%s)", GetSocketInfo() );
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);
	}
	else
	{
		// attach recv-data to buffer
		memcpy(m_pSendBuffer + m_nSendBufferLen, pAddBuf, len);
		m_nSendBufferLen += len;
	}

	return TRUE;
}

UINT CTCPServerSocket::SendBufferFunc(LPVOID pParam)
{
	TCP_SERVER_SOCKET_THREAD_PARAM* thread_param = (TCP_SERVER_SOCKET_THREAD_PARAM*)pParam;

	bool*				run_thread	= thread_param->pRunThread;
	CCriticalSection*	cs_lock		= thread_param->pCS;
	char**				pp_buf		= thread_param->ppBuf;
	int*				buff_len	= thread_param->buff_len;
	CTCPServerSocket*	sock		= thread_param->pSocket;
	HWND				parent_wnd	= thread_param->hParentWnd;

	delete thread_param;

	while(*run_thread)
	{
		if( *buff_len > 0 )
		{
			LOCK(cs_lock);

			char* buf = *pp_buf;
			int send_size = sock->Send(buf, *buff_len);

			if( send_size > 0 )
			{
				// send ok
				sock->SetSendTimeToNow(); // set send-time to now
				CAsyncSocketEx::PopDataFromBuffer(buf, *buff_len, send_size); // pop data from buffer
			}
			else if( send_size == SOCKET_ERROR )
			{
				// send error !!! --> close & thread end
				DWORD err_code = ::GetLastError();

				sock->SetInvalid();

				CString msg;
				msg.Format("Send ERROR !!! (%s)", sock->GetSocketInfo(err_code) );
				__WARN__( ((LPCTSTR)msg) );
				POST_WARN(parent_wnd, msg);

				break;
			}
		}

		::Sleep(1);
	}

	return 0;
}
