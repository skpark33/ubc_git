#pragma once

class CTCPTimeSyncServer;


// CTCPListenSocket 명령 대상입니다.

class CTCPListenSocket : public CAsyncSocket
{
public:
	CTCPListenSocket(HWND hParentWnd, CTCPTimeSyncServer* pSyncObject, UINT nPort);
	virtual ~CTCPListenSocket();

	virtual void OnAccept(int nErrorCode);
	virtual void OnClose(int nErrorCode);

protected:
	CTCPTimeSyncServer*		m_pSyncObject;
	HWND	m_hParentWnd;
	UINT	m_nPort;

	BOOL	m_bStart;

	void	Reconnect();

public:
	BOOL	StartListen();
	void	StopListen();
};
