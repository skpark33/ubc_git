#include "StdAfx.h"

#include "TCPTimeSyncServer.h"

#include "TCPListenSocket.h"
#include "TCPServerSocket.h"


#define		WM_DELETE_SOCKET				(WM_USER + 4097) // wp:socket(CAsyncSocket)		lp:none


typedef struct {
	HWND	hParentWnd;
	HWND	hSyncServer;
	int		nInterval;
	bool*	pRunThread;
	CTCPServerSocketList*	pReadySocketList;
	CTCPServerSocketList*	pSyncSocketList;
	CCriticalSection*		pCS;
} TCP_TIME_SYNC_SERVER_THREAD_PARAM;


////////////////////////////////////////////////////////////////////////
// CTCPTimeSyncServer

IMPLEMENT_DYNAMIC(CTCPTimeSyncServer, CWnd)


CTCPTimeSyncServer::CTCPTimeSyncServer(HWND hParentWnd)
: CTimeSyncObject(hParentWnd)
{
	__FUNC_BEGIN__;

	m_pListenSocket = NULL;
	m_pTimeSyncThread = NULL;

	__FUNC_END__;
}

CTCPTimeSyncServer::~CTCPTimeSyncServer()
{
	__FUNC_BEGIN__;
	
	StopSync();

	__FUNC_END__;
}


BEGIN_MESSAGE_MAP(CTCPTimeSyncServer, CWnd)
	ON_WM_TIMER()
	ON_MESSAGE(WM_DELETE_SOCKET, OnDeleteSocket)
END_MESSAGE_MAP()


BOOL CTCPTimeSyncServer::StartSync()
{
	__FUNC_BEGIN__;

	if( m_bStartSync && m_pListenSocket )
	{
		__DEBUG__( ("Already Start (%s)", GetInfoString()) );
		return TRUE;
	}

	m_bStartSync = true;
	KillTimer(TIMER_ID_RECONNECT);

	m_pListenSocket = new CTCPListenSocket(m_hParentWnd, this, m_nPort);
	if( m_pListenSocket == NULL )
	{
		// create CTCPListenSocket ERROR --> out of memory?
		CString msg;
		msg.Format("Create TCPListenSocket ERROR !!! (%s)", GetInfoString());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);

		Reconnect();

		return FALSE;
	}
	else if( !m_pListenSocket->StartListen() )
	{
		// StartListen ERROR !!! --> reconnect
		delete m_pListenSocket;
		m_pListenSocket = NULL;

		Reconnect();

		return FALSE;
	}

	TCP_TIME_SYNC_SERVER_THREAD_PARAM* thread_param = new TCP_TIME_SYNC_SERVER_THREAD_PARAM;
	if( thread_param == NULL )
	{
		// create param ERROR --> out of memory?
		CString msg;
		msg.Format("Create Thread-Param ERROR !!! (%s)", GetInfoString());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);
	}
	else
	{
		thread_param->hParentWnd		= m_hParentWnd;
		thread_param->hSyncServer		= GetSafeHwnd();
		thread_param->nInterval			= m_nInterval;
		thread_param->pRunThread		= &m_bRunTimeSyncThread;
		thread_param->pReadySocketList	= &m_arrReadySocket;
		thread_param->pSyncSocketList	= &m_arrSyncSocket;
		thread_param->pCS				= &m_csSocketList;

		m_bRunTimeSyncThread = true;
		m_pTimeSyncThread = ::AfxBeginThread(TimeSyncFunc, (LPVOID)thread_param, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
		if( m_pTimeSyncThread == NULL )
		{
			// AfxBeginThread ERROR --> out of memory?
			m_bRunTimeSyncThread = false;
			delete thread_param;

			CString msg;
			msg.Format("AfxBeginThread ERROR !!! (%s)", GetInfoString());
			__ERROR__( ((LPCTSTR)msg) );
			POST_ERROR(m_hParentWnd, msg);
		}
		else
		{
			m_pTimeSyncThread->m_bAutoDelete = FALSE;
			m_pTimeSyncThread->ResumeThread();
		}
	}

	__FUNC_END__;

	return TRUE;
}

BOOL CTCPTimeSyncServer::StopSync()
{
	__FUNC_BEGIN__;

	m_bStartSync = false;

	if( m_pListenSocket != NULL )
	{
		// delete ListenSocket
		__DEBUG__( ("Delete ListenSocket (%s)", GetInfoString()) );
		m_pListenSocket->StopListen();
		delete m_pListenSocket;
		m_pListenSocket = NULL;
	}

	// delete thread
	__DEBUG__( ("DeleteThread (%s)", GetInfoString()) );
	DeleteThread(m_bRunTimeSyncThread, m_pTimeSyncThread, 2000);

	// delete ServerSocket list
	ClearSocketList();

	__FUNC_END__;

	return TRUE;
}

UINT CTCPTimeSyncServer::TimeSyncFunc(LPVOID pParam)
{
	TCP_TIME_SYNC_SERVER_THREAD_PARAM* thread_param = (TCP_TIME_SYNC_SERVER_THREAD_PARAM*)pParam;

	HWND parent_wnd		= thread_param->hParentWnd;
	HWND sync_server	= thread_param->hSyncServer;
	int interval		= thread_param->nInterval;
	bool* run_thread	= thread_param->pRunThread;
	CTCPServerSocketList* ready_socket_list	= thread_param->pReadySocketList;
	CTCPServerSocketList* sync_socket_list	= thread_param->pSyncSocketList;
	CCriticalSection* cs_lock				= thread_param->pCS;

	delete thread_param;

	time_t last_sync_time = CTime::GetCurrentTime().GetTime();

	int count = 0;
	while( *run_thread )
	{
		if( ++count < 10 )
		{
			::Sleep(100);
			continue;
		}
		count = 0;

		// check list
		{
			LOCK(cs_lock);

			time_t tm_now = CTime::GetCurrentTime().GetTime();

			// check ready in ready-list
			int cnt = ready_socket_list->GetCount();
			for(int i=cnt-1; i>=0 && *run_thread; i--)
			{
				CTCPServerSocket* pSock = (CTCPServerSocket*)ready_socket_list->GetAt(i);
				if( pSock->GetTerminalID() > 0 ) // recv id --> move to sync-list
				{
					ready_socket_list->RemoveAt(i);
					sync_socket_list->Add(pSock);

					//CString* str_peer_ip = new CString(pSock->GetPeerIP();
					::PostMessage(parent_wnd, WM_CONNECT_TERMINAL, pSock->GetTerminalID(), (LPARAM)(new CString(pSock->GetPeerIP())) );
				}
				else if( !pSock->IsValid() || tm_now - pSock->GetLastSendTime() > 3 ) // invalid or no id(no recv) over 3sec => delete from ready-list
				{
					ready_socket_list->RemoveAt(i);
					pSock->ShutDown();
					pSock->SetInvalid();

					::PostMessage(sync_server, WM_DELETE_SOCKET, (WPARAM)pSock, NULL);
				}
			}

			// check dead in sync-list
			cnt = sync_socket_list->GetCount();
			for(int i=cnt-1; i>=0 && *run_thread; i--)
			{
				CTCPServerSocket* pSock = (CTCPServerSocket*)sync_socket_list->GetAt(i);

				if( !pSock->IsValid() || tm_now/*pSock->GetLastSendTime()*/ - pSock->GetLastRecvTime() > interval+3 ) // invalid or send, but no recv over 3sec
				{
					sync_socket_list->RemoveAt(i);
					pSock->ShutDown();

					::PostMessage(parent_wnd, WM_DISCONNECT_TERMINAL, pSock->GetTerminalID(), NULL );
					::PostMessage(sync_server, WM_DELETE_SOCKET, (WPARAM)pSock, NULL);
				}
			}
		}


		// check sync-interval
		if( CTime::GetCurrentTime().GetTime() - last_sync_time > interval )
		{
			LOCK(cs_lock);

			// send header to buffer
			char buf[24+1] = "_SS_                _SE_"; // (S)ync(E)nd
			//                012345678901234567890123 = 24 byte
			//                    45678901             = 8 byte, time_t (now)
			//                            23456789     = 8 byte, time_t (now xor)

			int cnt = sync_socket_list->GetCount();
			for(int i=0; i<cnt && *run_thread; i++)
			{
				CTCPServerSocket* pSock = (CTCPServerSocket*)sync_socket_list->GetAt(i);
				pSock->AddToSendBuffer(buf, 4); // first send (S)ync(S)tart
			}

			time_t tm_now = CTime::GetCurrentTime().GetTime();
			last_sync_time = tm_now;

			for(int i=0; i<1100 && *run_thread; i++) // 1ms x 1100 = 1100ms(1.1sec)
			{
				last_sync_time = CTime::GetCurrentTime().GetTime();

				if( tm_now != last_sync_time )
				{
					// it's time, exactly second is changed --> send time info
					memcpy(buf+4, &last_sync_time, sizeof(last_sync_time)); // set time data

					time_t verify_time = ~last_sync_time;
					memcpy(buf+12, &verify_time, sizeof(verify_time)); // set verify-data(XOR)

					for(int i=0; i<cnt && *run_thread; i++)
					{
						CTCPServerSocket* pSock = (CTCPServerSocket*)sync_socket_list->GetAt(i);
						pSock->AddToSendBuffer(buf+4, 20); // send body & (S)ync(E)nd
					}
					break;
				}
				::Sleep(1);
			}

			// send sync flag
			::PostMessage(parent_wnd, WM_SYNC_SERVER, NULL, NULL);
		}
	}

	return TRUE;
}

void CTCPTimeSyncServer::AddSocket(CTCPServerSocket* pSock)
{
	__FUNC_BEGIN__;

	// add socket to ready-list
	LOCK(m_csSocketList);
	m_arrReadySocket.Add(pSock);
	UNLOCK(m_csSocketList);

	char buf[16+1] = "_IS_        _IE_"; // (I)nit(S)tart - Interval(4byte) - Interval XOR(4byte) - (I)nit(E)nd
	//                0123456789012345 = 16 byte
	//                    4567         = 4 byte, int (interval)
	//                        8901     = 4 byte, int (interval xor)

	memcpy(buf+4, &m_nInterval, sizeof(m_nInterval)); // interval data

	int verify = ~m_nInterval;
	memcpy(buf+8, &verify, sizeof(verify)); // verify interval data (XOR)

	pSock->AddToSendBuffer(buf, 16);

	__FUNC_END__;
}

void CTCPTimeSyncServer::DeleteSocket(CTCPServerSocket* pSock)
{
	if( pSock )
	{
		int terminal_id = pSock->GetTerminalID();
		__DEBUG__( ("Delete Socket (%s)", pSock->GetSocketInfo()) );
		if( terminal_id ) ::PostMessage(m_hParentWnd, WM_DISCONNECT_TERMINAL, terminal_id, NULL );
		pSock->ShutDown();
		delete pSock;
	}
}

void CTCPTimeSyncServer::ClearSocketList()
{
	__FUNC_BEGIN__;

	LOCK(m_csSocketList);

	int cnt = m_arrReadySocket.GetCount();
	__DEBUG__( ("Clear Ready-Socket-List (%d)", cnt) );
	for(int i=0; i<cnt; i++)
	{
		CTCPServerSocket* pSock = (CTCPServerSocket*)m_arrReadySocket.GetAt(i);
		if( pSock ) DeleteSocket(pSock);
	}
	m_arrReadySocket.RemoveAll();

	cnt = m_arrSyncSocket.GetCount();
	__DEBUG__( ("Clear Socket-List (%d)", cnt) );
	for(int i=0; i<cnt; i++)
	{
		CTCPServerSocket* pSock = (CTCPServerSocket*)m_arrSyncSocket.GetAt(i);
		if( pSock ) DeleteSocket(pSock);
	}
	m_arrSyncSocket.RemoveAll();

	__FUNC_END__;
}

void CTCPTimeSyncServer::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case TIMER_ID_RECONNECT:
		{
			__DEBUG__( ("OnTimer TIMER_ID_RECONNECT (%d)", m_bStartSync) );

			KillTimer(nIDEvent);
			if( m_bStartSync )
			{
				StopSync();
				StartSync();
			}
		}
		break;
	}

	CTimeSyncObject::OnTimer(nIDEvent);
}

LRESULT CTCPTimeSyncServer::OnDeleteSocket(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__( ("begin... (WPARAM:%d)", wParam) );

	DeleteSocket((CTCPServerSocket*)wParam);

	__FUNC_END__;
	return 0;
}

void CTCPTimeSyncServer::Reconnect()
{
	__FUNC_BEGIN__;

	SetTimer(TIMER_ID_RECONNECT, TIMER_TIME_RECONNECT, NULL);

	__FUNC_END__;
}
