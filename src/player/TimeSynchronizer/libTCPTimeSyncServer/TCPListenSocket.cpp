// TCPServerTCPListenSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TCPTimeSyncServer.h"
#include "TCPListenSocket.h"
#include "TCPServerSocket.h"


// CTCPServerTCPListenSocket

CTCPListenSocket::CTCPListenSocket(HWND hParentWnd, CTCPTimeSyncServer* pSyncObject, UINT nPort)
:	m_pSyncObject ( pSyncObject )
,	m_hParentWnd ( hParentWnd )
,	m_nPort ( nPort )
,	m_bStart ( FALSE )
{
	__FUNC_BEGIN__;

	__DEBUG__( ("Port=%d", nPort) );

	__FUNC_END__;
}

CTCPListenSocket::~CTCPListenSocket()
{
	__FUNC_BEGIN__;
	__FUNC_END__;
}

// CTCPListenSocket 멤버 함수

void CTCPListenSocket::OnAccept(int nErrorCode)
{
	__DEBUG__( ("begin... (ErrCode:%d)", nErrorCode) );

	if( nErrorCode )
	{
		// OnAccept error
		StopListen();
		Reconnect();

		CString msg;
		msg.Format("OnAccept ERROR !!! (%s)", ::GetErrorString(nErrorCode));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);
	}
	else
	{
		// OnAccept OK
		CTCPServerSocket* p_sock = new CTCPServerSocket(m_hParentWnd);
		if( p_sock == NULL )
		{
			CString msg;
			msg.Format("Create TCPServerSocket ERROR !!!");
			__ERROR__( ((LPCTSTR)msg) );
			POST_ERROR(m_hParentWnd, msg);
		}
		else if( Accept(*p_sock) )
		{
			// Accept OK
			__DEBUG__( ("Accept ServerSocket (%s)", p_sock->GetSocketInfo()) );

			p_sock->ExtractPeerInfo();
			m_pSyncObject->AddSocket(p_sock);
		}
		else
		{
			// Accept error --> stop & reconnect
			DWORD err_code = ::GetLastError();

			StopListen();
			Reconnect();

			CString msg;
			msg.Format("Accept ERROR !!! (%s)", ::GetErrorString(err_code));
			__WARN__( ((LPCTSTR)msg) );
			POST_WARN(m_hParentWnd, msg);
		}
	}

	CAsyncSocket::OnAccept(nErrorCode);

	__FUNC_END__;
}

void CTCPListenSocket::OnClose(int nErrorCode)
{
	__DEBUG__( ("begin... (ErrCode:%d)", nErrorCode) );

	if( nErrorCode )
	{
		// OnClose error
		CString msg;
		msg.Format("OnClose ERROR !!! (%s)", ::GetErrorString(nErrorCode));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);
	}

	if( m_bStart )
	{
		// closed by error --> reconnect
		Reconnect();
	}

	CAsyncSocket::OnClose(nErrorCode);

	__FUNC_END__;
}

void CTCPListenSocket::Reconnect()
{
	__FUNC_BEGIN__;

	m_pSyncObject->Reconnect();

	__FUNC_END__;
}

BOOL CTCPListenSocket::StartListen()
{
	__FUNC_BEGIN__;

	if( m_bStart )
	{
		__DEBUG__( ("Already Listen (Port=%d)", m_nPort) );
		return TRUE;
	}

	__DEBUG__( ("Create ListenSocket (Port=%d)", m_nPort) );

	BOOL ret_val = Create(m_nPort);
	if( ret_val == NULL )
	{
		// Create error --> reconnect
		Reconnect();

		CString msg;
		msg.Format("Create ERROR !!! (%s)", ::GetErrorString(::GetLastError()));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		return FALSE;
	}

	// Create OK
	ret_val = Listen();
	if( ret_val == NULL )
	{
		// Listen error --> reconnect
		Reconnect();

		CString msg;
		msg.Format("Listen ERROR !!! (%s)", ::GetErrorString(::GetLastError()));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		return FALSE;
	}

	// Listen OK
	m_bStart = TRUE;

	__FUNC_END__;

	return ret_val;
}

void CTCPListenSocket::StopListen()
{
	__FUNC_BEGIN__;

	m_bStart = FALSE;
	Close();

	__FUNC_END__;
}
