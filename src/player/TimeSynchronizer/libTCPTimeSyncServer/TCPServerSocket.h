#pragma once

#include <afxmt.h>


// CTCPServerSocket 명령 대상입니다.

class CTCPServerSocket : public CAsyncSocketEx
{
public:
	CTCPServerSocket(HWND hParentWnd);
	virtual ~CTCPServerSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

	BOOL		AddToSendBuffer(char* pAddBuf, int len);

	time_t		GetLastSendTime() { return m_tmLastSend.GetTime(); };
	time_t		GetLastRecvTime() { return m_tmLastRecv.GetTime(); };

	void		SetSendTimeToNow() { m_tmLastSend = CTime::GetCurrentTime(); };
	void		SetRecvTimeToNow() { m_tmLastRecv = CTime::GetCurrentTime(); };

	bool		IsValid() { return m_bValid; };
	void		SetInvalid() { Close(); m_bValid = false; };

protected:
	bool		m_bValid;

	bool		m_bRunThread;
	CWinThread*	m_pSendThread;
	static UINT	SendBufferFunc(LPVOID pParam);

	CCriticalSection	m_csRecvBuffer;
	int			m_nMaxRecvBufferSize;
	char*		m_pRecvBuffer;
	int			m_nRecvBufferLen;
	CTime		m_tmLastRecv;

	CCriticalSection	m_csSendBuffer;
	int			m_nMaxSendBufferSize;
	char*		m_pSendBuffer;
	int			m_nSendBufferLen;
	CTime		m_tmLastSend;

	void		PacketAnalysis();

};
