#pragma once

#include <afxtempl.h>
#include <afxmt.h>


class CTCPListenSocket;
class CTCPServerSocket;

typedef CArray<CTCPServerSocket*, CTCPServerSocket*>	CTCPServerSocketList;


// CTCPTimeSyncServer 명령 대상입니다.

class CTCPTimeSyncServer : public CTimeSyncObject
{
	DECLARE_DYNAMIC(CTCPTimeSyncServer)

public:
	CTCPTimeSyncServer(HWND hParentWnd);
	virtual ~CTCPTimeSyncServer(void);

protected:
	CTCPListenSocket*	m_pListenSocket;

	CTCPServerSocketList	m_arrReadySocket;
	CTCPServerSocketList	m_arrSyncSocket;
	CCriticalSection		m_csSocketList;

	bool			m_bRunTimeSyncThread;
	CWinThread*		m_pTimeSyncThread;
	static UINT		TimeSyncFunc(LPVOID pParam);

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnDeleteSocket(WPARAM wParam, LPARAM lParam);

	virtual	BOOL	StartSync();
	virtual	BOOL	StopSync();

	void	AddSocket(CTCPServerSocket* pSock);
	void	DeleteSocket(CTCPServerSocket* pSock);
	void	ClearSocketList();

	void	Reconnect();
};
