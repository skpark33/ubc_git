// CheckListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "SortListCtrl.h"

CSortListCtrl::CSortListCtrl()
:	m_nSortCol(0)
,	m_bAscend(false)
{
	m_rgbDisconTextColor = RGB(0,0,0);
	m_rgbDisconBackColor = RGB(192,192,192);

	m_rgbConnTextColor = RGB(0,0,0);
	m_rgbConnBackColor = RGB(255,201,14);
}

CSortListCtrl::~CSortListCtrl()
{
}

BEGIN_MESSAGE_MAP(CSortListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnNMCustomdraw)
	ON_NOTIFY_REFLECT_EX(LVN_COLUMNCLICK, OnLvnColumnclick)
END_MESSAGE_MAP()

void CSortListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
	int idx = static_cast<int>( pLVCD->nmcd.dwItemSpec );

	switch(pLVCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		break;

	case CDDS_ITEMPREPAINT:
		{
			CString str_conn = GetItemText(idx, 1);

			if( str_conn == "O" )
			{
				pLVCD->clrText   = m_rgbConnTextColor;
				pLVCD->clrTextBk = m_rgbConnBackColor;

				*pResult = CDRF_NOTIFYITEMDRAW;
			}
			if( str_conn == "X" )
			{
				pLVCD->clrText   = m_rgbDisconTextColor;
				pLVCD->clrTextBk = m_rgbDisconBackColor;

				*pResult = CDRF_NOTIFYITEMDRAW;
			}
			else
				*pResult = CDRF_DODEFAULT;
		}
		break;

	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		//if(m_bDisplayLongLength && m_bDisplayLongLengthValues[pLVCD->iSubItem])
		//{
		//	pLVCD->clrText   = m_rgbSelectTextColor;
		//	pLVCD->clrTextBk = m_rgbSelectBackColor;
		//	*pResult = CDRF_NEWFONT;
		//}
		//else
			*pResult = CDRF_NEWFONT;
		break;

	default:
		*pResult = CDRF_DODEFAULT;
		break;
	}
}

BOOL CSortListCtrl::OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int	nSortCol = pNMLV->iSubItem;

	return Sort(nSortCol, (m_nSortCol != nSortCol) ? true : !m_bAscend);
}

int CALLBACK CSortListCtrl::SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam)
{
	SORT_PARAM*	ptrSortParam = (SORT_PARAM *)lSortParam;
	CListCtrl*	pListCtrl = (CListCtrl *)CWnd::FromHandle(ptrSortParam->hWnd);

	LVFINDINFO	lvFind;
	lvFind.flags = LVFI_PARAM;
	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	CString strItem1 = pListCtrl->GetItemText(nIndex1, ptrSortParam->nCol);
	CString	strItem2 = pListCtrl->GetItemText(nIndex2, ptrSortParam->nCol);

	if (ptrSortParam->bAscend == true)
		return strItem1.Compare(strItem2);
	else
		return strItem2.Compare(strItem1);
}

BOOL CSortListCtrl::Sort(int col, bool ascend)
{
	/*
	if(col<0)
	{
	col = m_nSortCol;
	ascend = m_bAscend;
	}
	else
	{
	m_nSortCol = col;
	m_bAscend = ascend;
	}

	//
	SORT_PARAM	sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = col;
	sort_param.bAscend = ascend;

	return SortItems(&SortColumn, (LPARAM)&sort_param);
	*/

	CHeaderCtrl* header = GetHeaderCtrl();

	// 정렬표시 해제
	HD_ITEM hditem;
	hditem.mask = HDI_FORMAT;
	header->GetItem( m_nSortCol, &hditem );
	hditem.fmt &= ~(HDF_SORTDOWN | HDF_SORTUP);
	header->SetItem( m_nSortCol, &hditem );

	// 정렬한다.
	SORT_PARAM	sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = col;
	sort_param.bAscend = ascend;

	// 현재 정렬기준 컬럼과 정렬모드를 저장한다.
	m_nSortCol = col;
	m_bAscend = sort_param.bAscend;

	BOOL ret_val = SortItems(&SortColumn, (LPARAM)&sort_param);

	// 정렬표시 설정
	header->GetItem( m_nSortCol, &hditem );

	hditem.fmt &= ~(HDF_SORTDOWN | HDF_SORTUP);
	hditem.fmt |= (m_bAscend ? HDF_SORTUP : HDF_SORTDOWN );

	header->SetItem( m_nSortCol, &hditem );

	return ret_val;
}
