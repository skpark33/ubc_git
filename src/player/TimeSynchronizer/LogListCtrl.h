#pragma once


class CLogListCtrl : public CListCtrl
{
public:
	CLogListCtrl();
	virtual ~CLogListCtrl();

protected:
	DECLARE_MESSAGE_MAP()

	COLORREF	m_rgbDebugTextColor;
	COLORREF	m_rgbDebugBackColor;

	COLORREF	m_rgbWarnTextColor;
	COLORREF	m_rgbWarnBackColor;

	COLORREF	m_rgbErrorTextColor;
	COLORREF	m_rgbErrorBackColor;

public:
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);

protected:
	virtual void PreSubclassWindow();
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
