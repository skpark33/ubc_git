// TimeSynchronizerView.h : interface of the CTimeSynchronizerView class
//


#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#include "SortListCtrl.h"
#include "ReposControl2.h"

class CTimeSyncObject;
class CLogViewDlg;


//////////////////////////////////////////////////////
class CTimeSynchronizerView : public CFormView
{
protected: // create from serialization only
	CTimeSynchronizerView();
	DECLARE_DYNCREATE(CTimeSynchronizerView)

public:
	enum{ IDD = IDD_TIMESYNCHRONIZER_FORM };
	enum SYNC_TYPE { NO_SYNC=0, SYNC_TCP_SERVER, SYNC_TCP_CLIENT, SYNC_UDP_SERVER, SYNC_UDP_CLIENT, };

// Attributes
public:
	CTimeSynchronizerDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CTimeSynchronizerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	CStatic		m_stcTerminalID;
	CStatic		m_stcTerminalIP;

	CStatic		m_stcStatus;
	CStatic		m_stcStatusIcon;
	CStatic		m_stcServerIP;
	CComboBox	m_cbxType;
	CEdit		m_editPort;
	CEdit		m_editInterval;
	CStatic		m_stcLastSyncTime;
	CStatic		m_stcCurrentTime;

	CButton		m_btnStart;
	CButton		m_btnStop;

	//CListCtrl	m_lcClients;
	CSortListCtrl	m_lcClients;

protected:
	CReposControl2	m_repos;

	CFont		m_fontCurrentTime;

	HICON		m_icoNormal;
	HICON		m_icoWarn;
	HICON		m_icoAlert;
	HICON		m_icoStop;

	CString		m_strServerIP;
	SYNC_TYPE	m_eSyncType;
	bool		m_bSyncStart;
	CTimeSyncObject*	m_pSyncObject;

	CLogViewDlg*		m_pLogViewDlg;

	SYNC_TYPE	GetSyncType() { return (SYNC_TYPE)m_cbxType.GetCurSel(); };
	LPCSTR		GetSyncTypeString(SYNC_TYPE eSyncType);

	bool		CheckValues();

	void		GetDefaultValues();
	void		SetDefaultValues();
	BOOL		GetLocalIPAddress(CString& strAddr);

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnCbnSelchangeComboType();

	afx_msg void OnBnClickedButtonSetToDefault();

	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonStop();

	afx_msg LRESULT OnPostLog(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnConnectTerminal(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDisconnectTerminal(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSyncServer(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSyncTerminal(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetInterval(WPARAM wParam, LPARAM lParam);
//	afx_msg void OnStnDblclickStaticStatusIcon();
	afx_msg void OnShowLogview();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};

#ifndef _DEBUG  // debug version in TimeSynchronizerView.cpp
inline CTimeSynchronizerDoc* CTimeSynchronizerView::GetDocument() const
   { return reinterpret_cast<CTimeSynchronizerDoc*>(m_pDocument); }
#endif

