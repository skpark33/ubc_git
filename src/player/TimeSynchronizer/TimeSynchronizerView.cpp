// TimeSynchronizerView.cpp : implementation of the CTimeSynchronizerView class
//

#include "stdafx.h"
#include "TimeSynchronizer.h"

#include "TimeSynchronizerDoc.h"
#include "TimeSynchronizerView.h"

#include "TimeSyncInterface.h"
#include "libTCPTimeSyncServer/TCPTimeSyncServer.h"
#include "libTCPTimeSyncClient/TCPTimeSyncClient.h"
#include "libUDPTimeSyncServer/UDPTimeSyncServer.h"
#include "libUDPTimeSyncClient/UDPTimeSyncClient.h"

#include "LogViewDlg.h"


#define		TIMER_ID_AUTO_START		(1022)
#define		TIMER_TIME_AUTO_START	(1000) // 1-sec

#define		TIMER_ID_CLOCK_TICK		(1023)


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTimeSynchronizerView

IMPLEMENT_DYNCREATE(CTimeSynchronizerView, CFormView)

BEGIN_MESSAGE_MAP(CTimeSynchronizerView, CFormView)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_SET_TO_DEFAULT, &CTimeSynchronizerView::OnBnClickedButtonSetToDefault)
	ON_BN_CLICKED(IDC_BUTTON_START, &CTimeSynchronizerView::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CTimeSynchronizerView::OnBnClickedButtonStop)
	ON_CBN_SELCHANGE(IDC_COMBO_TYPE, &CTimeSynchronizerView::OnCbnSelchangeComboType)
	ON_MESSAGE(WM_POST_LOG, OnPostLog)
	ON_MESSAGE(WM_CONNECT_TERMINAL, OnConnectTerminal)
	ON_MESSAGE(WM_DISCONNECT_TERMINAL, OnDisconnectTerminal)
	ON_MESSAGE(WM_SYNC_SERVER, OnSyncServer)
	ON_MESSAGE(WM_SYNC_TERMINAL, OnSyncTerminal)
	ON_MESSAGE(WM_SET_INTERVAL, OnSetInterval)
	ON_STN_DBLCLK(IDC_STATIC_STATUS_ICON, &CTimeSynchronizerView::OnShowLogview)
	ON_COMMAND(ID_SHOW_LOGVIEW, &CTimeSynchronizerView::OnShowLogview)
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CTimeSynchronizerView construction/destruction

CTimeSynchronizerView::CTimeSynchronizerView()
	: CFormView(CTimeSynchronizerView::IDD)
{
	m_fontCurrentTime.CreatePointFont(48*10, "Arial");

	m_eSyncType = NO_SYNC;
	m_bSyncStart = false;
	m_pSyncObject = NULL;
}

CTimeSynchronizerView::~CTimeSynchronizerView()
{
	CLogViewDlg::clearInstance();
	m_pLogViewDlg = NULL;

	OnBnClickedButtonStop();
}

void CTimeSynchronizerView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_stcStatus);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_cbxType);
	DDX_Control(pDX, IDC_EDIT_PORT, m_editPort);
	DDX_Control(pDX, IDC_EDIT_INTERVAL, m_editInterval);
	DDX_Control(pDX, IDC_STATIC_CURRENT_TIME, m_stcCurrentTime);
	DDX_Control(pDX, IDC_LIST_CLIENTS, m_lcClients);
	DDX_Control(pDX, IDC_BUTTON_START, m_btnStart);
	DDX_Control(pDX, IDC_BUTTON_STOP, m_btnStop);
	DDX_Control(pDX, IDC_STATIC_TERMINAL_ID, m_stcTerminalID);
	DDX_Control(pDX, IDC_STATIC_TERMINAL_IP, m_stcTerminalIP);
	DDX_Control(pDX, IDC_STATIC_SERVER_IP, m_stcServerIP);
	DDX_Control(pDX, IDC_STATIC_LAST_SYNC_TIME, m_stcLastSyncTime);
	DDX_Control(pDX, IDC_STATIC_STATUS_ICON, m_stcStatusIcon);
}

BOOL CTimeSynchronizerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CTimeSynchronizerView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	m_repos.SetParent(this);
	m_repos.AddControl((CWnd*)&m_lcClients, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_pLogViewDlg = CLogViewDlg::getInstance(this);

	//
	GetDefaultValues();

	//
	char buf_name[MAX_PATH+1] = {0};
	DWORD size = MAX_PATH;
	::GetComputerName(buf_name, &size);
	m_stcTerminalID.SetWindowText(buf_name);
	m_stcTerminalID.SetWindowText("SQI-06142"); //////////////////////////////////////////////////////////////////////////////////////////////////

	//
	CString str_ip;
	GetLocalIPAddress(str_ip);
	m_stcTerminalIP.SetWindowText(str_ip);

	//
	m_stcStatus.SetWindowText(" Stopped");

	m_icoNormal	= ::AfxGetApp()->LoadIcon(IDI_NORMAL);
	m_icoWarn	= ::AfxGetApp()->LoadIcon(IDI_WARN);
	m_icoAlert	= ::AfxGetApp()->LoadIcon(IDI_ALERT);
	m_icoStop	= ::AfxGetApp()->LoadIcon(IDI_STOP);
	m_stcStatusIcon.SetIcon(m_icoStop);

	//
	m_strServerIP = "127.0.0.1";
	m_stcServerIP.SetWindowText(m_strServerIP);

	//
	m_cbxType.InsertString(0, "No Sync");
	m_cbxType.InsertString(1, "TCP Server");
	m_cbxType.InsertString(2, "TCP Client");
	m_cbxType.InsertString(3, "UDP Server");
	m_cbxType.InsertString(4, "UDP Client");
	m_cbxType.SetCurSel(m_eSyncType);
	OnCbnSelchangeComboType();

	//
	m_lcClients.SetExtendedStyle(m_lcClients.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_lcClients.InsertColumn(0, "ID", 0, 125);
	m_lcClients.InsertColumn(1, "Connect", LVCFMT_CENTER, 60);
	m_lcClients.InsertColumn(2, "IP", 0, 110);
	m_lcClients.InsertColumn(3, "Last Sync Time", 0, 135);

	//
	m_stcCurrentTime.SetFont(&m_fontCurrentTime);

	//
	m_btnStop.EnableWindow(FALSE);

	//
	SetTimer(TIMER_ID_AUTO_START, TIMER_TIME_AUTO_START, NULL); // auto start
	SetTimer(TIMER_ID_CLOCK_TICK, 50, NULL); // display current-time

	::AfxGetMainWnd()->PostMessage(WM_USER+2222);
}


// CTimeSynchronizerView diagnostics

#ifdef _DEBUG
void CTimeSynchronizerView::AssertValid() const
{
	CFormView::AssertValid();
}

void CTimeSynchronizerView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CTimeSynchronizerDoc* CTimeSynchronizerView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTimeSynchronizerDoc)));
	return (CTimeSynchronizerDoc*)m_pDocument;
}
#endif //_DEBUG


// CTimeSynchronizerView message handlers

void CTimeSynchronizerView::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case TIMER_ID_AUTO_START:
		KillTimer(TIMER_ID_AUTO_START);
		if( m_eSyncType > NO_SYNC )
		{
			OnBnClickedButtonStart();
		}
		break;

	case TIMER_ID_CLOCK_TICK:
		{
			static CString str_prev_tm = "";
			CString str_cur_tm = CTime::GetCurrentTime().Format("%H:%M:%S");
			if( str_prev_tm != str_cur_tm )
			{
				str_prev_tm = str_cur_tm;
				m_stcCurrentTime.SetWindowText( str_prev_tm );
			}
		}
		break;
	}

	CFormView::OnTimer(nIDEvent);
}

void CTimeSynchronizerView::OnBnClickedButtonSetToDefault()
{
	SetDefaultValues();
}

void CTimeSynchronizerView::OnBnClickedButtonStart()
{
	if( !CheckValues() ) return;

	m_bSyncStart = true;

	//m_eSyncType = GetSyncType();
	switch( m_eSyncType )
	{
	case SYNC_TCP_SERVER:
		m_pSyncObject = new CTCPTimeSyncServer(GetSafeHwnd());
		break;
	case SYNC_TCP_CLIENT:
		m_pSyncObject = new CTCPTimeSyncClient(GetSafeHwnd());
		break;
	case SYNC_UDP_SERVER:
		m_pSyncObject = new CUDPTimeSyncServer(GetSafeHwnd());
		break;
	case SYNC_UDP_CLIENT:
		m_pSyncObject = new CUDPTimeSyncClient(GetSafeHwnd());
		break;
	}

	if( m_pSyncObject == NULL )
	{
		POST_ERROR(GetSafeHwnd(), "Fail to allocate Time-Sync-Object !!!");
		return;
	}

	if( !m_pSyncObject->Create() )
	{
		POST_ERROR(GetSafeHwnd(), "Fail to Create Time-Sync-Object !!!");
		return;
	}

	CString str_terminal_id;
	m_stcTerminalID.GetWindowText(str_terminal_id);

	CString str_port;
	m_editPort.GetWindowText(str_port);
	int port = atoi(str_port);

	CString str_interval;
	m_editInterval.GetWindowText(str_interval);
	int interval = atoi(str_interval);

	CString str_log;
	str_log.Format("StartSync : ServerIP(%s), Type(%s), Port(%u), Interval(%d)", m_strServerIP, GetSyncTypeString(m_eSyncType), port, interval );
	POST_DEBUG(GetSafeHwnd(), str_log);

	m_pSyncObject->SetInfo(m_strServerIP, str_terminal_id, port, interval);
	if( m_pSyncObject->StartSync() )
	{
		m_stcStatusIcon.SetIcon(m_icoNormal);

		POST_DEBUG(GetSafeHwnd(), "Started");
	}
	else
	{
		//if( m_pSyncObject )
		//{
		//	m_pSyncObject->StopSync();
		//	delete m_pSyncObject;
		//	m_pSyncObject = NULL;
		//}

		//SetTimer(TIMER_ID_RECONNECT, TIMER_TIME_RECONNECT, NULL);

		POST_ERROR(GetSafeHwnd(), "Fail to StartSync !!!");
	}

	m_cbxType.EnableWindow(FALSE);
	m_editPort.EnableWindow(FALSE);
	m_editInterval.EnableWindow(FALSE);

	m_btnStart.EnableWindow(FALSE);
	m_btnStop.EnableWindow(TRUE);
}

void CTimeSynchronizerView::OnBnClickedButtonStop()
{
	m_bSyncStart = false;

	if( GetSafeHwnd() ) POST_DEBUG(GetSafeHwnd(), "Stopped");

	if( m_pSyncObject )
	{
		m_pSyncObject->StopSync();
		delete m_pSyncObject;
		m_pSyncObject = NULL;
	}

	if( m_cbxType.GetSafeHwnd() ) m_cbxType.EnableWindow(TRUE);
	OnCbnSelchangeComboType();

	if( m_stcStatusIcon.GetSafeHwnd() ) m_stcStatusIcon.SetIcon(m_icoStop);
}

void CTimeSynchronizerView::OnCbnSelchangeComboType()
{
	if( !m_cbxType.GetSafeHwnd() ) return;

	m_eSyncType = GetSyncType();

	switch( m_eSyncType )
	{
	case 0:
		if( m_editPort.GetSafeHwnd() )		m_editPort.EnableWindow(FALSE);
		if( m_editInterval.GetSafeHwnd() )	m_editInterval.EnableWindow(FALSE);
		if( m_lcClients.GetSafeHwnd() )		m_lcClients.EnableWindow(FALSE);
		if( m_btnStart.GetSafeHwnd() )		m_btnStart.EnableWindow(FALSE);
		if( m_btnStop.GetSafeHwnd() )		m_btnStop.EnableWindow(FALSE);
		break;

	case 1: // TCP Server
		if( m_editPort.GetSafeHwnd() )		m_editPort.EnableWindow(TRUE);
		if( m_editInterval.GetSafeHwnd() )	m_editInterval.EnableWindow(TRUE);
		if( m_lcClients.GetSafeHwnd() )		m_lcClients.EnableWindow(TRUE);
		if( m_btnStart.GetSafeHwnd() )		m_btnStart.EnableWindow(TRUE);
		if( m_btnStop.GetSafeHwnd() )		m_btnStop.EnableWindow(FALSE);
		break;

	case 3: // UDP Server
		if( m_editPort.GetSafeHwnd() )		m_editPort.EnableWindow(TRUE);
		if( m_editInterval.GetSafeHwnd() )	m_editInterval.EnableWindow(TRUE);
		if( m_lcClients.GetSafeHwnd() )		m_lcClients.EnableWindow(FALSE);
		if( m_btnStart.GetSafeHwnd() )		m_btnStart.EnableWindow(TRUE);
		if( m_btnStop.GetSafeHwnd() )		m_btnStop.EnableWindow(FALSE);
		break;

	case 2: // TCP Client
	case 4: // UDP Client
		if( m_editPort.GetSafeHwnd() )		m_editPort.EnableWindow(TRUE);
		if( m_editInterval.GetSafeHwnd() )	m_editInterval.EnableWindow(FALSE);
		if( m_lcClients.GetSafeHwnd() )		m_lcClients.EnableWindow(FALSE);
		if( m_btnStart.GetSafeHwnd() )		m_btnStart.EnableWindow(TRUE);
		if( m_btnStop.GetSafeHwnd() )		m_btnStop.EnableWindow(FALSE);
		break;
	}
}

LRESULT CTimeSynchronizerView::OnPostLog(WPARAM wParam, LPARAM lParam)
{
	LOG_TYPE lgo_type = (LOG_TYPE)wParam;
	CString* str_log = (CString*)lParam;

	switch( lgo_type )
	{
	case eLogDebug:	/*m_stcStatusIcon.SetIcon(m_icoNormal);*/ break;
	case eLogWarn:	m_stcStatusIcon.SetIcon(m_icoWarn); break;
	case eLogError:	m_stcStatusIcon.SetIcon(m_icoAlert); break;
	default:		m_stcStatusIcon.SetIcon(m_icoStop); break;
	}

	if( str_log )
	{
		m_pLogViewDlg->AddLog(lgo_type, *str_log);
		delete str_log;
	}

	return 1;
}

LRESULT CTimeSynchronizerView::OnConnectTerminal(WPARAM wParam, LPARAM lParam)
{
	DWORD id = (DWORD)wParam;
	CString* str_ip = (CString*)lParam;

	if( !m_bSyncStart )
	{
		if( str_ip ) delete str_ip;
		return 0;
	}

	LVFINDINFO info;
	info.flags = LVFI_PARAM;
	info.lParam = id;

	int idx = m_lcClients.FindItem(&info);
	if( idx < 0 )
	{
		idx = m_lcClients.GetItemCount();

		char buf[32] = {0};
		sprintf(buf, "%d", id);

		m_lcClients.InsertItem(idx, buf);
		m_lcClients.SetItemData(idx, id);
	}

	m_lcClients.SetItemText(idx, 1, "O");
	if( str_ip )
	{
		m_lcClients.SetItemText(idx, 2, *str_ip);
		delete str_ip;
	}
	m_lcClients.SetItemText(idx, 3, CTime::GetCurrentTime().Format("%H:%M:%S"));

	m_lcClients.RedrawItems(idx, idx);

	return 1;
}

LRESULT CTimeSynchronizerView::OnDisconnectTerminal(WPARAM wParam, LPARAM lParam)
{
//	if( !m_bSyncStart ) return 0;

	DWORD id = (DWORD)wParam;

	LVFINDINFO info;
	info.flags = LVFI_PARAM;
	info.lParam = id;

	int idx = m_lcClients.FindItem(&info);
	if( idx < 0 ) return 0;

	m_lcClients.SetItemText(idx, 1, "X");

	m_lcClients.RedrawItems(idx, idx);

	return 1;
}

LRESULT CTimeSynchronizerView::OnSyncServer(WPARAM wParam, LPARAM lParam)
{
	if( !m_bSyncStart ) return 0;

	m_stcStatusIcon.SetIcon(m_icoNormal);

	m_stcLastSyncTime.SetWindowText(CTime::GetCurrentTime().Format("%H:%M:%S"));

	return 1;
}

LRESULT CTimeSynchronizerView::OnSyncTerminal(WPARAM wParam, LPARAM lParam)
{
	if( !m_bSyncStart ) return 0;

	DWORD id = (DWORD)wParam;

	LVFINDINFO info;
	info.flags = LVFI_PARAM;
	info.lParam = id;

	int idx = m_lcClients.FindItem(&info);
	if( idx < 0 ) return 0;

	m_lcClients.SetItemText(idx, 3, CTime::GetCurrentTime().Format("%H:%M:%S"));

	m_lcClients.RedrawItems(idx, idx);

	return 1;
}

LRESULT CTimeSynchronizerView::OnSetInterval(WPARAM wParam, LPARAM lParam)
{
	int interval = (int)wParam;

	CString str;
	str.Format("%d", interval);

	m_editInterval.SetWindowText(str);

	return 0;
}

LPCSTR CTimeSynchronizerView::GetSyncTypeString(SYNC_TYPE eSyncType)
{
	switch( eSyncType )
	{
	case SYNC_TCP_SERVER: return "TCP Server";
	case SYNC_TCP_CLIENT: return "TCP Client";
	case SYNC_UDP_SERVER: return "UDP Server";
	case SYNC_UDP_CLIENT: return "UDP Client";
	}
	return "Unknown";
}

bool CTimeSynchronizerView::CheckValues()
{
	int type = m_cbxType.GetCurSel();
	if( type < NO_SYNC || SYNC_UDP_CLIENT < type )
	{
		::AfxMessageBox("Invalid Type !!!", MB_ICONSTOP);
		return false;
	}

	if( m_editPort.IsWindowEnabled() )
	{
		CString str_port;
		m_editPort.GetWindowText(str_port);
		if( atoi(str_port) == 0 )
		{
			::AfxMessageBox("Invalid Port !!!", MB_ICONSTOP);
			return false;
		}
	}

	if( m_editInterval.IsWindowEnabled() )
	{
		CString str_interval;
		m_editInterval.GetWindowText(str_interval);
		if( atoi(str_interval) < 10 )
		{
			::AfxMessageBox("Invalid Interval (Min:10) !!!", MB_ICONSTOP);
			return false;
		}
	}

	return true;
}

//void CTimeSynchronizerView::OnStnDblclickStaticStatusIcon()
//{
//	m_pLogViewDlg->ShowWindow(SW_SHOW);
//}

void CTimeSynchronizerView::OnShowLogview()
{
	if( !m_pLogViewDlg->IsWindowVisible() )
	{
		CRect rect_dlg;
		m_pLogViewDlg->GetWindowRect(rect_dlg);

		CRect rect;
		::AfxGetMainWnd()->GetWindowRect(rect);

		m_pLogViewDlg->MoveWindow(rect.right+1, rect.top, rect_dlg.Width(), rect_dlg.Height());
		m_pLogViewDlg->ShowWindow(SW_SHOW);
	}
}

BOOL CTimeSynchronizerView::GetLocalIPAddress(CString& strAddr)
{
	__FUNC_BEGIN__;

	// get host ip address
    strAddr = _T("");
	char ac[128] = {0};

    if(gethostname(ac, sizeof(ac)) == -1)
	{
		__WARN__( (_T("gethostname fail")) );
		return FALSE;
	}

    struct hostent* phe = gethostbyname(ac);
    if(phe == 0)
	{
		__WARN__( (_T("gethostbyname fail")) );
		return FALSE;
	}

    if(phe->h_addr_list[0] != 0)
	{
        struct in_addr addr;
        memcpy(&addr, phe->h_addr_list[0], sizeof(struct in_addr));
        strAddr = inet_ntoa(addr);
    }

	__FUNC_END__;

    return TRUE;
}

void CTimeSynchronizerView::GetDefaultValues()
{
	char buf[1024]={0};

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("ROOT", "TIME_SYNC_TYPE", "", buf, 1023, ::GetUBCVariablesIniPath());
	int type = atoi(buf);
	if( NO_SYNC <= type && type <= SYNC_UDP_CLIENT ) m_eSyncType = (SYNC_TYPE)type;

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("ROOT", "TIME_SYNC_PORT", "", buf, 1023, ::GetUBCVariablesIniPath());
	UINT port = atoi(buf);
	m_editPort.SetWindowText(buf);

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("ROOT", "TIME_SYNC_INTERVAL", "", buf, 1023, ::GetUBCVariablesIniPath());
	UINT interval = atoi(buf);
	if( interval > 0 ) m_editInterval.SetWindowText(buf);
}

void CTimeSynchronizerView::SetDefaultValues()
{
	char buf[1024]={0};

	//
	sprintf(buf, "%d", (int)m_eSyncType);
	::WritePrivateProfileString("ROOT", "TIME_SYNC_TYPE", buf, ::GetUBCVariablesIniPath());

	//
	::ZeroMemory(buf, sizeof(buf));
	m_editPort.GetWindowText(buf, 1023);
	::WritePrivateProfileString("ROOT", "TIME_SYNC_PORT", buf, ::GetUBCVariablesIniPath());

	//
	::ZeroMemory(buf, sizeof(buf));
	m_editInterval.GetWindowText(buf, 1023);
	::WritePrivateProfileString("ROOT", "TIME_SYNC_INTERVAL", buf, ::GetUBCVariablesIniPath());
}

void CTimeSynchronizerView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if( GetSafeHwnd() && m_lcClients.GetSafeHwnd() )
	{
		m_repos.MoveControl(TRUE);
	}
}
