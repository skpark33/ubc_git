// UDPClientSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UDPClientSocket.h"
#include "UDPTimeSyncClient.h"


// CUDPClientSocket

CUDPClientSocket::CUDPClientSocket(HWND hParentWnd, CUDPTimeSyncClient* pSyncObject, UINT nPort)
:	m_pSyncObject ( pSyncObject )

,	m_nPort ( nPort )

,	m_nMaxRecvBufferSize ( BUFFER_SIZE )
,	m_pRecvBuffer ( NULL )
,	m_nRecvBufferLen ( 0 )
{
	__FUNC_BEGIN__;

	m_hParentWnd = hParentWnd;

	m_pRecvBuffer = new char[BUFFER_SIZE];
	if( m_pRecvBuffer == NULL )
	{
		// create buffer ERROR --> out of memory?
		CString msg;
		msg.Format("Create buffer ERROR !!! (%s)", GetSocketInfo());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);
	}

	__FUNC_END__;
}

CUDPClientSocket::~CUDPClientSocket()
{
	__FUNC_BEGIN__;

	// delete recv-buffer
	if( m_pRecvBuffer ) delete[]m_pRecvBuffer;
	m_pRecvBuffer = NULL;
	m_nMaxRecvBufferSize = 0;
	m_nRecvBufferLen = 0;

	__FUNC_END__;
}


// CUDPClientSocket 멤버 함수

void CUDPClientSocket::OnClose(int nErrorCode)
{
	__DEBUG__( ("begin... (%s)", GetSocketInfo(nErrorCode)) );

	if( nErrorCode )
	{
		// close error
		CString msg;
		msg.Format("OnClose ERROR !!! (%s)", GetSocketInfo(nErrorCode));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);
	}
	else
	{
		CString msg;
		msg.Format("OnClose (%s)", GetSocketInfo());
		__DEBUG__( ((LPCTSTR)msg) );
		POST_DEBUG(m_hParentWnd, msg);
	}

	// close --> reconnect
	SetReconnectTimer();

	CAsyncSocketEx::OnClose(nErrorCode);

	__FUNC_END__;
}

void CUDPClientSocket::OnReceive(int nErrorCode)
{
	__DEBUG__( ("begin... (%s)", GetSocketInfo(nErrorCode)) );

	if( nErrorCode )
	{
		// OnReceive error
		Close();

		CString msg;
		msg.Format("OnReceive ERROR !!! (%s)", GetSocketInfo(nErrorCode));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		// error --> reconnect
		SetReconnectTimer();
	}
	else
	{
		// OnReceive OK
		char buf[BUFFER_SIZE];
		int read_size = Receive(buf, BUFFER_SIZE);

		switch( read_size )
		{
		case 0:
			// read nothing (socket is closed)
			{
				Close();

				CString msg;
				msg.Format("Receive ZERO !!! (%s)", GetSocketInfo());
				__WARN__( ((LPCTSTR)msg) );
				POST_WARN(m_hParentWnd, msg);

				SetReconnectTimer(); // reconnect
			}
			break;

		case SOCKET_ERROR:
			{
				DWORD err_code = GetLastError();
				if( err_code != WSAEWOULDBLOCK )
				{
					// socket error occurred --> close socket
					Close();

					CString msg;
					msg.Format("Receive Error !!! (%s)", GetSocketInfo(err_code) );
					__WARN__( ((LPCTSTR)msg) );
					POST_WARN(m_hParentWnd, msg);

					SetReconnectTimer(); // reconnect
				}
			}
			break;

		default:
			{
				bool buffer_ok = (m_nRecvBufferLen + read_size < m_nMaxRecvBufferSize); // not overflow
				if( !buffer_ok )
				{
					__DEBUG__(("Increase(Recv)Buffer (%s, OldMaxSize=%d)", GetSocketInfo(), m_nMaxRecvBufferSize));
					// overflow ==> increase buffer-size
					buffer_ok = IncreaseBufferSize(m_pRecvBuffer, m_nMaxRecvBufferSize); 
				}

				if( !buffer_ok )
				{
					CString msg;
					msg.Format("Increase(Recv)Buffer ERROR !!! (%s)", GetSocketInfo() );
					__ERROR__( ((LPCTSTR)msg) );
					POST_ERROR(m_hParentWnd, msg);
				}
				else
				{
					// attach recv-data to buffer
					memcpy(m_pRecvBuffer + m_nRecvBufferLen, buf, read_size);
					m_nRecvBufferLen += read_size;
				}
			}

			__DEBUG__( ("PacketAnalysis (init:%d, size:%d)", m_bInitialized, m_nRecvBufferLen) );
			PacketAnalysis();

			break;
		}
	}

	CAsyncSocketEx::OnReceive(nErrorCode);

	__FUNC_END__;
}

void CUDPClientSocket::PacketAnalysis()
{
	if( m_nRecvBufferLen >= 24 )
	{
		// check sync time

		// header : "_SS_                _SE_"; // (S)ync(S)tart - Time(8byte) - Time XOR(8byte) - (S)ync(E)nd
		//           012345678901234567890123 = 24 byte
		//               45678901             = 8 byte, time_t(time)
		//                       23456789     = 8 byte, time_t(time xor)

		for(int i=m_nRecvBufferLen-24; i>=0; i--)
		{
			if( memcmp(m_pRecvBuffer,    "_SS_", 4) == 0 &&
				memcmp(m_pRecvBuffer+20, "_SE_", 4) == 0 )
			{
				time_t tm_now;
				memcpy(&tm_now, m_pRecvBuffer+4, sizeof(tm_now));

				time_t tm_now_xor;
				memcpy(&tm_now_xor, m_pRecvBuffer+12, sizeof(tm_now_xor));

				if( tm_now != ~tm_now_xor )
				{
					// data error
					Close();

					CString msg;
					msg.Format("Wrong Data !!! (%s, DataSize=%d)", GetSocketInfo(), m_nRecvBufferLen);
					__WARN__( ((LPCTSTR)msg) );
					__WARN__( ((LPCTSTR)BinaryDataToString((LPBYTE)m_pRecvBuffer, (m_nRecvBufferLen>50 ? 50:m_nRecvBufferLen))) );
					POST_WARN(m_hParentWnd, msg);

					SetReconnectTimer();
				}
				else
				{
					SetHeartbeatTimer();

					//// set time
					//CTime tm(tm_now);
					//SYSTEMTIME sys_tm;
					//tm.GetAsSystemTime(sys_tm);
					//::SetLocalTime(&sys_tm);

					PopDataFromBuffer(m_pRecvBuffer, m_nRecvBufferLen, i+24);

					// send sync flag
					::PostMessage(m_hParentWnd, WM_SYNC_SERVER, NULL, NULL);
				}
				return;
			}
		}
	}
	if( m_nRecvBufferLen >= 4 )
	{
		for(int i=m_nRecvBufferLen-4; i>=0; i--)
		{
			if( memcmp(m_pRecvBuffer, "_HB_", 4) == 0 )
			{
				SetHeartbeatTimer();

				PopDataFromBuffer(m_pRecvBuffer, m_nRecvBufferLen, i+4);
			}
		}
	}
}

BOOL CUDPClientSocket::Connect()
{
	__FUNC_BEGIN__;

	CString msg;
	msg.Format("Connect (%s)", GetSocketInfo());
	__DEBUG__( ((LPCTSTR)msg) );
	POST_DEBUG(m_hParentWnd, msg);

	if( !Create(m_nPort, SOCK_DGRAM) )
	{
		// Create ERROR !!!
		CString msg;
		msg.Format("Create ERROR !!! (%s)", ::GetErrorString(::GetLastError()));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		// auto reconnect by timer
		return FALSE;
	}

	static char ch_broadcast = 1;
	if( !SetSockOpt(SO_BROADCAST, (char*)&ch_broadcast, sizeof(ch_broadcast)) )
	{
		// SetSockOpt ERROR !!!
		CString msg;
		msg.Format("SetSockOpt(SO_BROADCAST) ERROR !!! (%s)", ::GetErrorString(::GetLastError()));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		// auto reconnect by timer
		return FALSE;
	}

	__FUNC_END__;

	return TRUE;
}

void CUDPClientSocket::SetReconnectTimer()
{
	__FUNC_BEGIN__;

	m_pSyncObject->SetReconnectTimer(); // set reconnect timer

	__FUNC_END__;
}

void CUDPClientSocket::KillReconnectTimer()
{
	__FUNC_BEGIN__;

	m_pSyncObject->KillReconnectTimer(); // kill reconnect timer

	__FUNC_END__;
}

void CUDPClientSocket::SetHeartbeatTimer()
{
	__FUNC_BEGIN__;

	m_pSyncObject->SetHeartbeatTimer(); // set heartbeat timer

	__FUNC_END__;
}

void CUDPClientSocket::KillHeartbeatTimer()
{
	__FUNC_BEGIN__;

	m_pSyncObject->KillHeartbeatTimer(); // kill heartbeat timer

	__FUNC_END__;
}
