#pragma once

#define		TIMER_ID_SYNC				(2049)

class CUDPClientSocket;


class CUDPTimeSyncClient : public CTimeSyncObject
{
	DECLARE_DYNAMIC(CUDPTimeSyncClient)

public:
	CUDPTimeSyncClient(HWND hParentWnd);
	virtual ~CUDPTimeSyncClient(void);

protected:
	CUDPClientSocket*	m_pClientSocket;

	DECLARE_MESSAGE_MAP()

	BOOL	CreateSocket();
	void	DeleteSocket();

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	virtual	BOOL	StartSync();
	virtual	BOOL	StopSync();

	void	SetReconnectTimer();
	void	KillReconnectTimer();

	void	SetHeartbeatTimer();
	void	KillHeartbeatTimer();
};
