#include "StdAfx.h"

#include "UDPTimeSyncClient.h"
#include "UDPClientSocket.h"


// CUDPTimeSyncClient

IMPLEMENT_DYNAMIC(CUDPTimeSyncClient, CWnd)


CUDPTimeSyncClient::CUDPTimeSyncClient(HWND hParentWnd)
: CTimeSyncObject(hParentWnd)
{
	__FUNC_BEGIN__;

	m_pClientSocket = NULL;

	__FUNC_END__;
}

CUDPTimeSyncClient::~CUDPTimeSyncClient()
{
	__FUNC_BEGIN__;

	StopSync();

	__FUNC_END__;
}


BEGIN_MESSAGE_MAP(CUDPTimeSyncClient, CWnd)
	ON_WM_TIMER()
END_MESSAGE_MAP()


BOOL CUDPTimeSyncClient::StartSync()
{
	__FUNC_BEGIN__;

	if( m_bStartSync && m_pClientSocket )
	{
		__DEBUG__( ("Already Start (%s)", GetInfoString()) );
		return TRUE;
	}

	m_bStartSync = true;
	SetReconnectTimer();

	if( !CreateSocket() )
	{
		DeleteSocket();

		return FALSE;
	}

	KillReconnectTimer();
	SetHeartbeatTimer();

	__FUNC_END__;

	return TRUE;
}

BOOL CUDPTimeSyncClient::StopSync()
{
	__FUNC_BEGIN__;

	m_bStartSync = false;
	KillReconnectTimer();
	KillHeartbeatTimer();

	DeleteSocket();

	__FUNC_END__;

	return TRUE;
}

BOOL CUDPTimeSyncClient::CreateSocket()
{
	__FUNC_BEGIN__;

	if( m_pClientSocket )
	{
		__DEBUG__( ("Already Exist Socket (%s)", GetInfoString()) );
		return TRUE;
	}

	m_pClientSocket = new CUDPClientSocket(m_hParentWnd, this, m_nPort);
	if( m_pClientSocket == NULL )
	{
		// create CUDPClientSocket ERROR !!! --> out of memory?
		CString msg;
		msg.Format("Create UDPClientSocket ERROR !!! (%s)", GetInfoString());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);

		// auto reconnect by timer
		return FALSE;
	}
	else if( !m_pClientSocket->Connect() )
	{
		DWORD err_code = ::GetLastError();
		if( err_code != WSAEWOULDBLOCK )
		{
			// create socket ERROR !!!
			CString msg;
			msg.Format("Connect ERROR !!! (%s)", ::GetErrorString(err_code));
			__WARN__( ((LPCTSTR)msg) );
			POST_WARN(m_hParentWnd, msg);

			// auto reconnect by timer
			return FALSE;
		}
	}

	__FUNC_END__;

	return TRUE;
}

void CUDPTimeSyncClient::DeleteSocket()
{
	__FUNC_BEGIN__;

	if( m_pClientSocket )
	{
		delete m_pClientSocket;
		m_pClientSocket = NULL;
	}

	__FUNC_END__;
}

void CUDPTimeSyncClient::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case TIMER_ID_RECONNECT:
		{
			// no recv init-header, timeover --> reconnect
			__DEBUG__( ("OnTimer TIMER_ID_HEART_BEAT (%d)", m_bStartSync) );
			KillTimer(nIDEvent);
			if( m_bStartSync )
			{
				StopSync();
				StartSync();
			}
		}
		break;

	case TIMER_ID_HEART_BEAT:
		{
			// no recv init-header, timeover --> reconnect
			__DEBUG__( ("OnTimer TIMER_ID_HEART_BEAT (%d)", m_bStartSync) );
			KillTimer(nIDEvent);
			if( m_bStartSync )
			{
				StopSync();
				StartSync();
			}
		}
		break;
	}

	CTimeSyncObject::OnTimer(nIDEvent);
}

void CUDPTimeSyncClient::SetReconnectTimer()
{
	if( GetSafeHwnd() ) SetTimer(TIMER_ID_RECONNECT, TIMER_TIME_RECONNECT, NULL);
}

void CUDPTimeSyncClient::KillReconnectTimer()
{
	if( GetSafeHwnd() ) KillTimer(TIMER_ID_RECONNECT);
}

void CUDPTimeSyncClient::SetHeartbeatTimer()
{
	if( GetSafeHwnd() ) SetTimer(TIMER_ID_HEART_BEAT, TIMER_TIME_HEART_BEAT*2, NULL);
}

void CUDPTimeSyncClient::KillHeartbeatTimer()
{
	if( GetSafeHwnd() ) KillTimer(TIMER_ID_HEART_BEAT);
}
