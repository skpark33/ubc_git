#pragma once

#include <afxsock.h>
#include <afxmt.h>

class CUDPTimeSyncClient;


// CUDPServerSocket 명령 대상입니다.

class CUDPClientSocket : public CAsyncSocketEx
{
public:
	CUDPClientSocket(HWND hParentWnd, CUDPTimeSyncClient* pSyncObject, UINT nPort);
	virtual ~CUDPClientSocket();

	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

	BOOL		AddToSendBuffer(char* pAddBuf, int len);

	BOOL		Connect();

protected:
	CUDPTimeSyncClient*	m_pSyncObject;

	UINT		m_nPort;

	int			m_nMaxRecvBufferSize;
	char*		m_pRecvBuffer;
	int			m_nRecvBufferLen;

	void		PacketAnalysis();

	void		SetReconnectTimer();
	void		KillReconnectTimer();

	void		SetHeartbeatTimer();
	void		KillHeartbeatTimer();
};
