// TimeSyncSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TimeSyncSocket.h"

#include "TimeSyncClient.h"


// CTimeSyncSocket

CTimeSyncSocket::CTimeSyncSocket(HWND hMessageReceiveWnd)
{
	m_hMessageReceiveWnd = hMessageReceiveWnd;
	m_tmLastReceived = 0;
	::ZeroMemory(m_szBuffer, sizeof(m_szBuffer));
	m_nBufferSize = 0;
}

CTimeSyncSocket::~CTimeSyncSocket()
{
}


// CTimeSyncSocket 멤버 함수

void CTimeSyncSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//
	// error
	//

	CAsyncSocket::OnClose(nErrorCode);
}

void CTimeSyncSocket::AddToBuffer(char* pAddBuf, int nSize)
{
	time_t cur_time = CTime::GetCurrentTime().GetTime();

	if( m_tmLastReceived+3 < cur_time ) // is new packet (3sec)
	{
		// clear buffer
		::ZeroMemory(m_szBuffer, sizeof(m_szBuffer));
		m_nBufferSize = 0;
		m_tmLastReceived = cur_time;
	}

	if(m_nBufferSize+nSize > TIME_SYNC_BUFFER_SIZE) // buffer overflow
	{
		// clear buffer
		::ZeroMemory(m_szBuffer, sizeof(m_szBuffer));
		m_nBufferSize = 0;
	}

	memcpy(m_szBuffer+m_nBufferSize, pAddBuf, nSize);
	m_nBufferSize += nSize;
}

void CTimeSyncSocket::Parsing()
{
	char* buf = (char*)m_szBuffer;
	char* last_position = NULL;

	while(1)
	{
		// find '_TIME_'
		char* start = strstr(buf, "_TIME_");
		if(start == NULL) break;
		last_position = start;

		// find '_SYNC_'
		char* end = strstr(start, "_SYNC_");
		if(end == NULL) break;
		last_position = end;

		// find '|' between start and end
		char* delimeter = strstr(start, "|");
		if(delimeter == NULL) break;
		if(delimeter > end) break;

		// get computer name
		char computer_name[32] = {0};
		memcpy(computer_name, start+6, delimeter-(start+6));

		// get receive time
		time_t recv_time = _atoi64(delimeter+1);
		TRACE("%I64d\n", recv_time);
		CTime tm(recv_time);

		if( tm != CTime::GetCurrentTime() )
		{
			if( m_hMessageReceiveWnd )
				::PostMessage(m_hMessageReceiveWnd, WM_TIME_SYNC_RECEIVE, NULL, NULL);

			SYSTEMTIME SystemTime;
			tm.GetAsSystemTime(SystemTime);

			SetLocalTime(&SystemTime);
		}

		//
		buf = end;
	}

	if( last_position != NULL )
	{
		char tmp_buf[TIME_SYNC_BUFFER_SIZE] = {0};
		strcpy(tmp_buf, last_position);

		::ZeroMemory(m_szBuffer, sizeof(m_szBuffer));
		strcpy(m_szBuffer, tmp_buf);
		m_nBufferSize = strlen(m_szBuffer);
	}
}

void CTimeSyncSocket::OnReceive(int nErrorCode)
{
	char buf[TIME_SYNC_BUFFER_SIZE] = {0};

	int read_size = Receive(buf, TIME_SYNC_BUFFER_SIZE);

	AddToBuffer(buf, read_size);

	Parsing();

	CAsyncSocket::OnReceive(nErrorCode);
}
