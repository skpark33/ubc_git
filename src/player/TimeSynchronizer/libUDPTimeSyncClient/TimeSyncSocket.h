#pragma once

#include <afxsock.h>

#define		TIME_SYNC_BUFFER_SIZE		(8192)

// CTimeSyncSocket 명령 대상입니다.

class CTimeSyncSocket : public CAsyncSocket
{
public:
	CTimeSyncSocket(HWND hMessageReceiveWnd);
	virtual ~CTimeSyncSocket();

protected:
	HWND	m_hMessageReceiveWnd;

	time_t	m_tmLastReceived;
	char	m_szBuffer[TIME_SYNC_BUFFER_SIZE];
	int		m_nBufferSize;

	void	AddToBuffer(char* pAddBuf, int nSize);
	void	Parsing();

public:
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
};
