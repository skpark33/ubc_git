#pragma once

#define		TIMER_ID_SYNC				(2049)

class CTCPClientSocket;


class CTCPTimeSyncClient : public CTimeSyncObject
{
	DECLARE_DYNAMIC(CTCPTimeSyncClient)

public:
	CTCPTimeSyncClient(HWND hParentWnd);
	virtual ~CTCPTimeSyncClient(void);

protected:
	CTCPClientSocket*	m_pClientSocket;

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	virtual	BOOL	StartSync();
	virtual	BOOL	StopSync();

	BOOL	CreateSocket();
	void	DeleteSocket();

	void	SetReconnectTimer();
	void	KillReconnectTimer();

	void	SetResyncTimer(int nInterval);
	void	KillResyncTimer();
};
