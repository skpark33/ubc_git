// TCPClientSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TCPClientSocket.h"
#include "TCPTimeSyncClient.h"


typedef struct {
	bool*				pRunThread;
	CCriticalSection*	pCS;
	char**				ppBuf;
	int*				buff_len;
	CTCPClientSocket*	pSocket;
	HWND				hParentWnd;
	CTCPTimeSyncClient*	pSyncClient;
} TCP_CLIENT_SOCKET_THREAD_PARAM;


// CTCPClientSocket

CTCPClientSocket::CTCPClientSocket(HWND hParentWnd, CTCPTimeSyncClient* pSyncObject, LPCSTR lpszServerIP, LPCSTR lpszTerminalID, UINT nPort)
:	m_pSyncObject ( pSyncObject )

,	m_strServerIP ( lpszServerIP )
,	m_strTerminalID ( lpszTerminalID )
,	m_nPort ( nPort )

,	m_nInterval ( 0 )

,	m_bRunThread ( false )
,	m_pSendThread ( NULL )

,	m_nMaxRecvBufferSize ( BUFFER_SIZE )
,	m_pRecvBuffer ( NULL )
,	m_nRecvBufferLen ( 0 )

,	m_nMaxSendBufferSize ( BUFFER_SIZE )
,	m_pSendBuffer ( NULL )
,	m_nSendBufferLen ( 0 )
{
	__FUNC_BEGIN__;

	m_hParentWnd = hParentWnd;

	m_pRecvBuffer = new char[BUFFER_SIZE];
	m_pSendBuffer = new char[BUFFER_SIZE];
	if( m_pRecvBuffer == NULL || m_pSendBuffer == NULL )
	{
		// create buffer ERROR --> out of memory?
		CString msg;
		msg.Format("Create buffer ERROR !!! (%s)", GetSocketInfo());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);
	}

	//
	TCP_CLIENT_SOCKET_THREAD_PARAM* thread_param = new TCP_CLIENT_SOCKET_THREAD_PARAM;
	if( thread_param == NULL )
	{
		// create param ERROR --> out of memory?
		CString msg;
		msg.Format("Create Thread-Param ERROR !!! (%s)", GetSocketInfo());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);
	}
	else
	{
		thread_param->pRunThread	= &m_bRunThread;
		thread_param->pCS			= &m_csSendBuffer;
		thread_param->ppBuf			= &m_pSendBuffer;
		thread_param->buff_len		= &m_nSendBufferLen;
		thread_param->pSocket		= this;
		thread_param->hParentWnd	= hParentWnd;
		thread_param->pSyncClient	= pSyncObject;

		m_bRunThread = true;
		m_pSendThread = ::AfxBeginThread(SendBufferFunc, (LPVOID)thread_param, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
		if( m_pSendThread == NULL )
		{
			// AfxBeginThread ERROR --> out of memory?
			m_bRunThread = false;
			delete thread_param;

			CString msg;
			msg.Format("AfxBeginThread ERROR !!! (%s)", GetSocketInfo());
			__ERROR__( ((LPCTSTR)msg) );
			POST_ERROR(m_hParentWnd, msg);
		}
		else
		{
			m_pSendThread->m_bAutoDelete = FALSE;
			m_pSendThread->ResumeThread();
		}
	}

	__FUNC_END__;
}

CTCPClientSocket::~CTCPClientSocket()
{
	__FUNC_BEGIN__;

	::DeleteThread(m_bRunThread, m_pSendThread, 1000);

	{
		// delete recv-buffer
		LOCK(m_csRecvBuffer);
		if( m_pRecvBuffer ) delete[]m_pRecvBuffer;
		m_pRecvBuffer = NULL;
		m_nMaxRecvBufferSize = 0;
		m_nRecvBufferLen = 0;
	}

	{
		// delete send-buffer
		LOCK(m_csSendBuffer);
		if( m_pSendBuffer ) delete[]m_pSendBuffer;
		m_pSendBuffer = NULL;
		m_nMaxSendBufferSize = 0;
		m_nSendBufferLen = 0;
	}

	__FUNC_END__;
}


// CTCPClientSocket 멤버 함수

void CTCPClientSocket::OnConnect(int nErrorCode)
{
	__DEBUG__( ("begin... (%s)", GetSocketInfo(nErrorCode)) );

	if( nErrorCode )
	{
		// connect error
		CString msg;
		msg.Format("OnConnect ERROR !!! (%s)", ::GetErrorString(nErrorCode));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		// auto reconnect by timer
	}
	else
	{
		// success to connect

		// header : "_IS_  Terminal-ID(24byte)   _IE_"; // (I)nit(S)tart - Terminal-ID(24byte) - (I)nit(E)nd
		//           01234567890123456789012345678901 = 32 byte
		//               456789012345678901234567     = 24 byte, Terminal-ID
		char buf[32+1] = "_IS_                        _IE_";
		strcpy(buf+4, m_strTerminalID);

		// send init data
		AddToSendBuffer(buf, 32);
	}

	CAsyncSocketEx::OnConnect(nErrorCode);

	__FUNC_END__;
}

void CTCPClientSocket::OnClose(int nErrorCode)
{
	__DEBUG__( ("begin... (%s)", GetSocketInfo(nErrorCode)) );

	if( nErrorCode )
	{
		// close error
		CString msg;
		msg.Format("OnClose ERROR !!! (%s)", GetSocketInfo(nErrorCode));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);
	}
	else
	{
		CString msg;
		msg.Format("OnClose (%s)", GetSocketInfo());
		__DEBUG__( ((LPCTSTR)msg) );
		POST_DEBUG(m_hParentWnd, msg);
	}

	// close --> reconnect
	m_pSyncObject->SetReconnectTimer();

	CAsyncSocketEx::OnClose(nErrorCode);

	__FUNC_END__;
}

void CTCPClientSocket::OnReceive(int nErrorCode)
{
	__DEBUG__( ("begin... (%s)", GetSocketInfo(nErrorCode)) );

	if( nErrorCode )
	{
		// OnReceive error
		Close();

		CString msg;
		msg.Format("OnReceive ERROR !!! (%s)", GetSocketInfo(nErrorCode));
		__WARN__( ((LPCTSTR)msg) );
		POST_WARN(m_hParentWnd, msg);

		m_pSyncObject->SetReconnectTimer();
	}
	else
	{
		// OnReceive OK
		char buf[BUFFER_SIZE];
		int read_size = Receive(buf, BUFFER_SIZE);

		switch( read_size )
		{
		case 0:
			// read nothing (socket is closed)
			{
				Close();

				CString msg;
				msg.Format("Receive ZERO !!! (%s)", GetSocketInfo());
				__WARN__( ((LPCTSTR)msg) );
				POST_WARN(m_hParentWnd, msg);

				m_pSyncObject->SetReconnectTimer(); // reconnect
			}
			break;

		case SOCKET_ERROR:
			{
				DWORD err_code = GetLastError();
				if( err_code != WSAEWOULDBLOCK )
				{
					// socket error occurred --> close socket
					Close();

					CString msg;
					msg.Format("Receive Error !!! (%s)", GetSocketInfo(err_code) );
					__WARN__( ((LPCTSTR)msg) );
					POST_WARN(m_hParentWnd, msg);

					m_pSyncObject->SetReconnectTimer(); // reconnect
				}
			}
			break;

		default:
			{
				LOCK(m_csRecvBuffer);

				bool buffer_ok = (m_nRecvBufferLen + read_size < m_nMaxRecvBufferSize); // not overflow
				if( !buffer_ok )
				{
					__DEBUG__(("Increase(Recv)Buffer (%s, OldMaxSize=%d)", GetSocketInfo(), m_nMaxRecvBufferSize));
					// overflow ==> increase buffer-size
					buffer_ok = IncreaseBufferSize(m_pRecvBuffer, m_nMaxRecvBufferSize); 
				}

				if( !buffer_ok )
				{
					CString msg;
					msg.Format("Increase(Recv)Buffer ERROR !!! (%s)", GetSocketInfo() );
					__ERROR__( ((LPCTSTR)msg) );
					POST_ERROR(m_hParentWnd, msg);
				}
				else
				{
					// attach recv-data to buffer
					memcpy(m_pRecvBuffer + m_nRecvBufferLen, buf, read_size);
					m_nRecvBufferLen += read_size;
				}
			}

			__DEBUG__( ("PacketAnalysis (init:%d, size:%d)", m_bInitialized, m_nRecvBufferLen) );
			PacketAnalysis();

			break;
		}
	}

	CAsyncSocketEx::OnReceive(nErrorCode);

	__FUNC_END__;
}

void CTCPClientSocket::PacketAnalysis()
{
	LOCK(m_csRecvBuffer);

	if( m_bInitialized == false )
	{
		// not recv init-header;

		// header : "_IS_        _IE_"; // (I)nit(S)tart - Interval(4byte) - Interval XOR(4byte) - (I)nit(E)nd
		//           0123456789012345 = 16 byte
		//               4567         = 4 byte, int (interval)
		//                   8901     = 4 byte, int (interval xor)

		if( m_nRecvBufferLen < 16 ) return; // not enough header size

		if( memcmp(m_pRecvBuffer,    "_IS_", 4) != 0 ||
			memcmp(m_pRecvBuffer+12, "_IE_", 4) != 0 )
		{
			// wrong header
			Close();

			CString msg;
			msg.Format("Wrong Header !!! (%s, HeaderSize=%d)", GetSocketInfo(), m_nRecvBufferLen);
			__WARN__( ((LPCTSTR)msg) );
			__WARN__( ((LPCTSTR)BinaryDataToString((LPBYTE)m_pRecvBuffer, (m_nRecvBufferLen>50 ? 50:m_nRecvBufferLen))) );
			POST_WARN(m_hParentWnd, msg);

			// ==> auto reconnect by timer
			return;
		}

		//
		memcpy(&m_nInterval, m_pRecvBuffer+4, sizeof(m_nInterval));

		int interval_xor;
		memcpy(&interval_xor, m_pRecvBuffer+8, sizeof(interval_xor));

		if( m_nInterval <= 0 || m_nInterval != ~interval_xor )
		{
			// wrong header
			Close();

			CString msg;
			msg.Format("Wrong Header !!! (%s, HeaderSize=%d)", GetSocketInfo(), m_nRecvBufferLen);
			__WARN__( ((LPCTSTR)msg) );
			__WARN__( ((LPCTSTR)BinaryDataToString((LPBYTE)m_pRecvBuffer, (m_nRecvBufferLen>50 ? 50:m_nRecvBufferLen))) );
			POST_WARN(m_hParentWnd, msg);

			// ==> auto reconnect by timer
			return;
		}

		m_pSyncObject->KillReconnectTimer(); // stop reconnect-timer
		SetResyncTimer(); // set resync timer

		PopDataFromBuffer(m_pRecvBuffer, m_nRecvBufferLen, 16);

		::PostMessage(m_hParentWnd, WM_SET_INTERVAL, m_nInterval, NULL);

		m_bInitialized = true;
	}
	else
	{
		// header : "_SS_                _SE_"; // (I)nit(S)tart - Interval(4byte) - Interval XOR(4byte) - (I)nit(E)nd
		//           012345678901234567890123 = 24 byte
		//               45678901             = 8 byte, time_t(time)
		//                       23456789     = 8 byte, time_t(time xor)

		if( m_nRecvBufferLen < 24 ) return; // not enough header size

		for(int i=m_nRecvBufferLen-24; i>=0; i--)
		{
			if( memcmp(m_pRecvBuffer,    "_SS_", 4) == 0 &&
				memcmp(m_pRecvBuffer+20, "_SE_", 4) == 0 )
			{
				time_t tm_now;
				memcpy(&tm_now, m_pRecvBuffer+4, sizeof(tm_now));

				time_t tm_now_xor;
				memcpy(&tm_now_xor, m_pRecvBuffer+12, sizeof(tm_now_xor));

				if( tm_now != ~tm_now_xor )
				{
					// error
					Close();

					CString msg;
					msg.Format("Wrong Data !!! (%s, DataSize=%d)", GetSocketInfo(), m_nRecvBufferLen);
					__WARN__( ((LPCTSTR)msg) );
					__WARN__( ((LPCTSTR)BinaryDataToString((LPBYTE)m_pRecvBuffer, (m_nRecvBufferLen>50 ? 50:m_nRecvBufferLen))) );
					POST_WARN(m_hParentWnd, msg);

					break;
				}

				SetResyncTimer();

				//// set time
				//CTime tm(tm_now);
				//SYSTEMTIME sys_tm;
				//tm.GetAsSystemTime(sys_tm);
				//::SetLocalTime(&sys_tm);

				// send response
				AddToSendBuffer("_SO_", 4);

				PopDataFromBuffer(m_pRecvBuffer, m_nRecvBufferLen, i+24);

				// send sync flag
				::PostMessage(m_hParentWnd, WM_SYNC_SERVER, NULL, NULL);

				break;
			}
		}
	}
}

BOOL CTCPClientSocket::AddToSendBuffer(char* pAddBuf, int len)
{
	LOCK(m_csSendBuffer);

	bool buffer_ok = (m_nSendBufferLen + len < m_nMaxSendBufferSize); // not overflow
	if( !buffer_ok )
	{
		__DEBUG__(("Increase(Send)Buffer (%s, OldMaxSize=%d)", GetSocketInfo(), m_nMaxSendBufferSize));
		// overflow ==> increase buffer-size
		buffer_ok = IncreaseBufferSize(m_pSendBuffer, m_nMaxSendBufferSize); 
	}

	if( !buffer_ok )
	{
		CString msg;
		msg.Format("Increase(Send)Buffer ERROR !!! (%s)", GetSocketInfo() );
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);
	}
	else
	{
		// attach recv-data to buffer
		memcpy(m_pSendBuffer + m_nSendBufferLen, pAddBuf, len);
		m_nSendBufferLen += len;
	}

	return TRUE;
}

UINT CTCPClientSocket::SendBufferFunc(LPVOID pParam)
{
	TCP_CLIENT_SOCKET_THREAD_PARAM* thread_param = (TCP_CLIENT_SOCKET_THREAD_PARAM*)pParam;

	bool*				run_thread	= thread_param->pRunThread;
	CCriticalSection*	cs_lock		= thread_param->pCS;
	char**				pp_buf		= thread_param->ppBuf;
	int*				buff_len	= thread_param->buff_len;
	CTCPClientSocket*	sock		= thread_param->pSocket;
	HWND				parent_wnd	= thread_param->hParentWnd;
	CTCPTimeSyncClient*	sync_client	= thread_param->pSyncClient;

	delete thread_param;

	while(*run_thread)
	{
		if( *buff_len > 0 )
		{
			LOCK(cs_lock);

			char* buf = *pp_buf;
			int send_size = sock->Send(buf, *buff_len);

			if( send_size > 0 )
			{
				// send ok
				PopDataFromBuffer(buf, *buff_len, send_size);
			}
			if( send_size == SOCKET_ERROR )
			{
				// send error !!! --> close & thread end
				DWORD err_code = ::GetLastError();

				sock->Close();
				sync_client->SetReconnectTimer();

				CString msg;
				msg.Format("Send Error !!! (%s)", sock->GetSocketInfo(err_code) );
				__WARN__( ((LPCTSTR)msg) );
				POST_WARN(parent_wnd, msg);

				break;
			}
		}

		::Sleep(1);
	}

	return 0;
}

BOOL CTCPClientSocket::Connect()
{
	__FUNC_BEGIN__;

	CString msg;
	msg.Format("Connect (%s)", GetSocketInfo());
	__DEBUG__( ((LPCTSTR)msg) );
	POST_DEBUG(m_hParentWnd, msg);

	if( !Create() )
	{
		__WARN__( ("Create ERROR !!! (%s)", GetSocketInfo()) );
		return FALSE;
	}

	__FUNC_END__;

	return CAsyncSocketEx::Connect(m_strServerIP, m_nPort);
}

void CTCPClientSocket::SetResyncTimer()
{
	__FUNC_BEGIN__;

	m_pSyncObject->SetResyncTimer(m_nInterval); // set resync timer

	__FUNC_END__;
}
