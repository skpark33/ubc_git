#pragma once

#include <afxsock.h>
#include <afxmt.h>

class CTCPTimeSyncClient;


// CTCPServerSocket 명령 대상입니다.

class CTCPClientSocket : public CAsyncSocketEx
{
public:
	CTCPClientSocket(HWND hParentWnd, CTCPTimeSyncClient* pSyncObject, LPCSTR lpszServerIP, LPCSTR lpszTerminalID, UINT nPort);
	virtual ~CTCPClientSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

	BOOL		AddToSendBuffer(char* pAddBuf, int len);

	BOOL		Connect();

protected:
	CTCPTimeSyncClient*	m_pSyncObject;

	CString		m_strServerIP;
	CString		m_strTerminalID;
	UINT		m_nPort;

	int			m_nInterval;

	bool		m_bRunThread;
	CWinThread*	m_pSendThread;
	static UINT	SendBufferFunc(LPVOID pParam);

	CCriticalSection	m_csRecvBuffer;
	int			m_nMaxRecvBufferSize;
	char*		m_pRecvBuffer;
	int			m_nRecvBufferLen;

	CCriticalSection	m_csSendBuffer;
	int			m_nMaxSendBufferSize;
	char*		m_pSendBuffer;
	int			m_nSendBufferLen;


	void		PacketAnalysis();

	void		SetResyncTimer();
};
