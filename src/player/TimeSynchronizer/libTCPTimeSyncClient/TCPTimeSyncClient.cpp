#include "StdAfx.h"

#include "TCPTimeSyncClient.h"
#include "TCPClientSocket.h"


// CTCPTimeSyncClient

IMPLEMENT_DYNAMIC(CTCPTimeSyncClient, CWnd)


CTCPTimeSyncClient::CTCPTimeSyncClient(HWND hParentWnd)
: CTimeSyncObject(hParentWnd)
{
	__FUNC_BEGIN__;

	m_pClientSocket = NULL;

	__FUNC_END__;
}

CTCPTimeSyncClient::~CTCPTimeSyncClient()
{
	__FUNC_BEGIN__;

	StopSync();

	__FUNC_END__;
}


BEGIN_MESSAGE_MAP(CTCPTimeSyncClient, CWnd)
	ON_WM_TIMER()
END_MESSAGE_MAP()


BOOL CTCPTimeSyncClient::StartSync()
{
	__FUNC_BEGIN__;

	if( m_bStartSync && m_pClientSocket )
	{
		__DEBUG__( ("Already Start (%s)", GetInfoString()) );
		return TRUE;
	}

	m_bStartSync = true;
	SetReconnectTimer();

	if( !CreateSocket() )
	{
		DeleteSocket();
		// auto reconnect by timer
		return FALSE;
	}

	__FUNC_END__;

	return TRUE;
}

BOOL CTCPTimeSyncClient::StopSync()
{
	__FUNC_BEGIN__;

	m_bStartSync = false;
	KillReconnectTimer();

	DeleteSocket();

	__FUNC_END__;

	return TRUE;
}

BOOL CTCPTimeSyncClient::CreateSocket()
{
	__FUNC_BEGIN__;

	if( m_pClientSocket )
	{
		__DEBUG__( ("Already Exist Socket (%s)", GetInfoString()) );
		return TRUE;
	}

	m_pClientSocket = new CTCPClientSocket(m_hParentWnd, this, m_strServerIP, m_strTerminalID, m_nPort);
	if( m_pClientSocket == NULL )
	{
		// create CTCPClientSocket ERROR !!! --> out of memory?
		CString msg;
		msg.Format("Create TCPClientSocket ERROR !!! (%s)", GetInfoString());
		__ERROR__( ((LPCTSTR)msg) );
		POST_ERROR(m_hParentWnd, msg);

		// auto reconnect by timer
		return FALSE;
	}
	else if( !m_pClientSocket->Connect() )
	{
		DWORD err_code = ::GetLastError();
		if( err_code != WSAEWOULDBLOCK )
		{
			// create socket ERROR !!!
			CString msg;
			msg.Format("Connect ERROR !!! (%s)", ::GetErrorString(err_code));
			__WARN__( ((LPCTSTR)msg) );
			POST_WARN(m_hParentWnd, msg);

			// auto reconnect by timer
			return FALSE;
		}
	}

	__FUNC_END__;

	return TRUE;
}

void CTCPTimeSyncClient::DeleteSocket()
{
	__FUNC_BEGIN__;

	if( m_pClientSocket )
	{
		delete m_pClientSocket;
		m_pClientSocket = NULL;
	}

	__FUNC_END__;
}

void CTCPTimeSyncClient::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case TIMER_ID_RECONNECT:
		{
			// no recv init-header, timeover --> reconnect
			__DEBUG__( ("OnTimer TIMER_ID_RECONNECT (%d)", m_bStartSync) );
			KillTimer(nIDEvent);
			if( m_bStartSync )
			{
				StopSync();
				StartSync();
			}
		}
		break;

	case TIMER_ID_SYNC:
		{
			// no recv sync-info, timeover --> reconnect
			__DEBUG__( ("OnTimer TIMER_ID_SYNC (%d)", m_bStartSync) );
			KillTimer(nIDEvent);
			if( m_bStartSync )
			{
				StopSync();
				StartSync();
			}
		}
		break;
	}

	CTimeSyncObject::OnTimer(nIDEvent);
}

void CTCPTimeSyncClient::SetReconnectTimer()
{
	if( GetSafeHwnd() ) SetTimer(TIMER_ID_RECONNECT, TIMER_TIME_RECONNECT, NULL);
}

void CTCPTimeSyncClient::KillReconnectTimer()
{
	if( GetSafeHwnd() ) KillTimer(TIMER_ID_RECONNECT);
}

void CTCPTimeSyncClient::SetResyncTimer(int nInterval)
{
	if( GetSafeHwnd() ) SetTimer(TIMER_ID_SYNC, nInterval*1000 + TIMER_TIME_RECONNECT, NULL);
}

void CTCPTimeSyncClient::KillResyncTimer()
{
	if( GetSafeHwnd() ) KillTimer(TIMER_ID_SYNC);
}
