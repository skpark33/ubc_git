// LogListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "LogListCtrl.h"


#define		LIST_COUNT_LIMIT		10000
#define		LIST_DELETE_COUNT		1000



CLogListCtrl::CLogListCtrl()
{
	m_rgbDebugTextColor = RGB(0, 0, 0);
	m_rgbDebugBackColor = RGB(255, 255, 255);

	m_rgbWarnTextColor = RGB(0, 0, 0);
	m_rgbWarnBackColor = RGB(255, 128, 64);

	m_rgbErrorTextColor = RGB(0, 0, 0);
	m_rgbErrorBackColor = RGB(255, 0, 0);
}

CLogListCtrl::~CLogListCtrl()
{
}

BEGIN_MESSAGE_MAP(CLogListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnNMCustomdraw)
	
	ON_WM_TIMER()
END_MESSAGE_MAP()

void CLogListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
	int idx = static_cast<int>( pLVCD->nmcd.dwItemSpec );

	switch(pLVCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		break;

	case CDDS_ITEMPREPAINT:
		{
			DWORD log_type = GetItemData(idx);

			switch( log_type )
			{
			case 1: // debug
				pLVCD->clrText   = m_rgbDebugTextColor;
				pLVCD->clrTextBk = m_rgbDebugBackColor;

				*pResult = CDRF_NOTIFYITEMDRAW;
				break;

			case 2: // warn
				pLVCD->clrText   = m_rgbWarnTextColor;
				pLVCD->clrTextBk = m_rgbWarnBackColor;

				*pResult = CDRF_NOTIFYITEMDRAW;
				break;

			case 3: // error
				pLVCD->clrText   = m_rgbErrorTextColor;
				pLVCD->clrTextBk = m_rgbErrorBackColor;

				*pResult = CDRF_NOTIFYITEMDRAW;
				break;

			default:
				*pResult = CDRF_DODEFAULT;
				break;
			}
		}
		break;

	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		//if(m_bDisplayLongLength && m_bDisplayLongLengthValues[pLVCD->iSubItem])
		//{
		//	pLVCD->clrText   = m_rgbSelectTextColor;
		//	pLVCD->clrTextBk = m_rgbSelectBackColor;
		//	*pResult = CDRF_NEWFONT;
		//}
		//else
			*pResult = CDRF_NEWFONT;
		break;

	default:
		*pResult = CDRF_DODEFAULT;
		break;
	}
}

void CLogListCtrl::PreSubclassWindow()
{
	SetTimer(1022, 10000, NULL);

	CListCtrl::PreSubclassWindow();
}

void CLogListCtrl::OnTimer(UINT_PTR nIDEvent)
{
	CListCtrl::OnTimer(nIDEvent);

	if( nIDEvent == 1022 )
	{
		KillTimer(1022);
		int idx = GetItemCount();
		if( idx > LIST_COUNT_LIMIT )
		{
			for(int i=idx-1; i>=LIST_COUNT_LIMIT-LIST_DELETE_COUNT; i--)
			{
				DeleteItem(i);
			}
		}
		SetTimer(1022, 10000, NULL);
	}
}
