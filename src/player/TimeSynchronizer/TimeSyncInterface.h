// SyncObjectInterface.h : interface of the CSyncObjectInterface class
//


#pragma once

enum LOG_TYPE { eLogDebug=1, eLogWarn, eLogError };

#define		WM_POST_LOG						(WM_USER + 2048) // wp:type			lp:msg(CString pointer, must delete)
#define		POST_DEBUG(h, m)				{ CString* _msg_ = new CString(m); if(_msg_) ::PostMessage(h, WM_POST_LOG, (WPARAM)eLogDebug, (LPARAM)_msg_); }
#define		POST_WARN(h, m)					{ CString* _msg_ = new CString(m); if(_msg_) ::PostMessage(h, WM_POST_LOG, (WPARAM)eLogWarn,  (LPARAM)_msg_); }
#define		POST_ERROR(h, m)				{ CString* _msg_ = new CString(m); if(_msg_) ::PostMessage(h, WM_POST_LOG, (WPARAM)eLogError, (LPARAM)_msg_); }

#define		WM_CONNECT_TERMINAL				(WM_USER + 2049) // wp:id(dword)		lp:ip(CString pointer, must delete)
#define		WM_DISCONNECT_TERMINAL			(WM_USER + 2050) // wp:id(dword)		lp:none
#define		WM_SYNC_SERVER					(WM_USER + 2051) // wp:none				lp:none
#define		WM_SYNC_TERMINAL				(WM_USER + 2052) // wp:id(dword)		lp:none
#define		WM_SET_INTERVAL					(WM_USER + 2053) // wp:interval(int)	lp:none

#define		TIMER_ID_RECONNECT				(1025)
#define		TIMER_TIME_RECONNECT			(5000) // 5 sec

#define		TIMER_ID_HEART_BEAT				(1023)
#define		TIMER_TIME_HEART_BEAT			(10000) // 10 sec

#define		BUFFER_SIZE						(4*1024)	// 4k


//////////////////////////////////////////
class CTimeSyncObject : public CWnd
{
	DECLARE_DYNAMIC(CTimeSyncObject)

public:
	CTimeSyncObject(HWND hParentWnd);
	virtual ~CTimeSyncObject();

protected:
	bool	m_bStartSync;

	HWND	m_hParentWnd;
	CString	m_strServerIP;
	CString	m_strTerminalID;
	UINT	m_nPort;
	int		m_nInterval;

	DECLARE_MESSAGE_MAP()

	CString	m_strInfo;

public:
	BOOL	Create();

	void	SetInfo(LPCSTR lpszServerIP, LPCSTR lpszTerminalID, UINT nPort, int nInterval); // nInterval(sec) 0 => 60(default)

	virtual BOOL	StartSync() { return FALSE; };
	virtual BOOL	StopSync() { return FALSE; };

	LPCSTR	GetInfoString();
};


//////////////////////////////////////////
class CAsyncSocketEx : public CAsyncSocket
{
public:
	CAsyncSocketEx();
	virtual ~CAsyncSocketEx();

protected:
	bool		m_bInitialized;

	HWND		m_hParentWnd;
	CString		m_strTerminalID;
	int			m_nTerminalID;

	CString		m_strPeerIP;
	UINT		m_nPeerPort;

	CString		m_strInfo;
	CString		m_strInfoEx;

public:
	static bool PopDataFromBuffer(char*& pBuf, int& nBufLen, int nPopSize);
	static bool IncreaseBufferSize(char*& pBuf, int& nMaxBuffSize);
	static CString BinaryDataToString(LPBYTE pData, int len);

	int 		GetTerminalID() { return m_nTerminalID; };
	LPCTSTR 	GetPeerIP() { return m_strPeerIP; };

	void		ExtractPeerInfo() { GetPeerName(m_strPeerIP, m_nPeerPort); };
	void		GetPeerInfo(CString& strIP, UINT& nPort) { strIP=m_strPeerIP; nPort=m_nPeerPort; };

	LPCTSTR		GetSocketInfo();
	LPCTSTR		GetSocketInfo(DWORD dwErrCode);

};
