// LogViewDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TimeSynchronizer.h"
#include "LogViewDlg.h"


// CLogViewDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLogViewDlg, CDialog)

CLogViewDlg* CLogViewDlg::_instance = NULL;

CLogViewDlg* CLogViewDlg::getInstance(CWnd* pParentWnd)
{
	if( _instance == NULL )
	{
		_instance = new CLogViewDlg(pParentWnd);
		_instance->Create(IDD_LOG_VIEW, pParentWnd);
		_instance->ShowWindow(SW_HIDE);
	}
	return _instance;
}

void CLogViewDlg::clearInstance()
{
	if( _instance )
	{
		if( _instance->GetSafeHwnd() ) _instance->DestroyWindow();
		delete _instance;
		_instance = NULL;
	}
}

////////////////////////////////////////////////

CLogViewDlg::CLogViewDlg(CWnd* pParent /*=NULL*/)
//	: CDialog(CLogViewDlg::IDD_LOG_VIEW, pParent)
{

}

CLogViewDlg::~CLogViewDlg()
{
}

void CLogViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_lcLog);
}


BEGIN_MESSAGE_MAP(CLogViewDlg, CDialog)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CLogViewDlg 메시지 처리기입니다.

BOOL CLogViewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_lcLog.SetExtendedStyle(m_lcLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcLog.InsertColumn(0, "Time", 0, 125);
	m_lcLog.InsertColumn(1, "Type", 0, 50);
	m_lcLog.InsertColumn(2, "Log", 0, 380);

	HICON ico = ::AfxGetApp()->LoadIcon(IDI_LOG);

	SetIcon(ico, TRUE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CLogViewDlg::OnOK()
{
	ShowWindow(SW_HIDE);
	//CDialog::OnOK();
}

void CLogViewDlg::OnCancel()
{
	ShowWindow(SW_HIDE);
	//CDialog::OnCancel();
}

void CLogViewDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if( GetSafeHwnd() && m_lcLog.GetSafeHwnd() )
	{
		m_lcLog.MoveWindow(0, 0, cx, cy);
	}
}

void CLogViewDlg::AddLog(LOG_TYPE eType, LPCSTR lpszLog)
{
	m_lcLog.InsertItem(0, CTime::GetCurrentTime().Format("%Y-%m-%d  %H:%M:%S"));
	m_lcLog.SetItemData(0, eType);

	switch( eType )
	{
	case eLogDebug:	m_lcLog.SetItemText(0, 1, "debug"); break;
	case eLogWarn:	m_lcLog.SetItemText(0, 1, "WARN"); break;
	case eLogError:	m_lcLog.SetItemText(0, 1, "ERROR"); break;
	}

	m_lcLog.SetItemText(0, 2, lpszLog);
}
