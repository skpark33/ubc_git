// ExtraInstallDlg.cpp : 구현 파일
//
#include "stdafx.h"
#include "ExtraInstall.h"
#include "ExtraInstallDlg.h"
#include "CMN/libScratch/scratchUtil.h"
#include "CMN/libInstall/installUtil.h"
#include "CMN/libDBUtil/dbUtil.h"
#include "CMN/libCommon/ubcEnterprise.h"
#include "CMN/libCommon/ubcIni.h"
//#include "CMN/libCommon/ubcIniMux.h"
#include <ci/libConfig/ciEnv.h>
#include <ci/libConfig/ciXProperties.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <cci/libWrapper/cciCall.h>
#include "CMN/libScratch/scEnv.h"


//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif

ciSET_DEBUG(10,"ExtraInstallDlg");



// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CExtraInstallDlg 대화 상자




CExtraInstallDlg::CExtraInstallDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExtraInstallDlg::IDD, pParent)
	, m_stredit1(_T(""))
	, _check_SetSiteID(TRUE)
	, _check_InstallVNC(TRUE)
	, _check_SetVNCAdmin(TRUE)
	, _check_ChangeVNCBackground(TRUE)
	, _check_InstallCodec(TRUE)
	, _check_SetCodecProp(TRUE)
	, _check_InstallDivX(TRUE)
	, _check_InstallFLV(TRUE)
	, _check_CreateSoftDrive(TRUE)
	//, _check_InstallFTP(TRUE)
	//, _check_SetFTPAdmin(TRUE)
	, _check_CreateShortCut(TRUE)
	, _check_ChangeComputerName(TRUE)
	, _check_SetSecurity1(TRUE)
	, _check_SetSecurity2(TRUE)
	, _check_ReleaseErrorReport(TRUE)
	, _check_SetFirewallException(TRUE)
	, _check_InstallFlash(TRUE)
	, _check_SetAutoShutdownTime(TRUE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	//skpark
	_orbThread=0;
	_hasAuth=ciFalse;

	m_bDixVCodec = FALSE;
	m_bFLV = FALSE;
	m_bKLiteCodec = FALSE;
	m_bUtrVnc = FALSE;
	m_bFlash = FALSE;
}

void CExtraInstallDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_stredit1);
	DDX_Control(pDX, IDC_EDIT2, Edit2);
	DDX_Control(pDX, IDC_EDIT1, Edit1);
	DDX_Control(pDX, IDC_STATIC_1, Static1);
	DDX_Control(pDX, IDC_STATIC_2, Static2);
	DDX_Check(pDX, IDC_CHECK1, _check_SetSiteID);
	DDX_Check(pDX, IDC_CHECK2, _check_InstallVNC);
	DDX_Check(pDX, IDC_CHECK3, _check_SetVNCAdmin);
	DDX_Check(pDX, IDC_CHECK4, _check_ChangeVNCBackground);
	DDX_Check(pDX, IDC_CHECK5, _check_InstallCodec);
	DDX_Check(pDX, IDC_CHECK6, _check_SetCodecProp);
	DDX_Check(pDX, IDC_CHECK7, _check_InstallDivX);
	DDX_Check(pDX, IDC_CHECK8, _check_CreateSoftDrive);
	DDX_Check(pDX, IDC_CHECK9, _check_InstallFLV);
	//DDX_Check(pDX, IDC_CHECK10, _check_SetFTPAdmin);
	DDX_Check(pDX, IDC_CHECK11, _check_CreateShortCut);
	DDX_Check(pDX, IDC_CHECK12, _check_ChangeComputerName);
	DDX_Check(pDX, IDC_CHECK13, _check_SetSecurity1);
	DDX_Check(pDX, IDC_CHECK14, _check_SetSecurity2);
	DDX_Check(pDX, IDC_CHECK15, _check_ReleaseErrorReport);
	DDX_Check(pDX, IDC_CHECK16, _check_SetFirewallException);
	DDX_Check(pDX, IDC_CHECK17, _check_InstallFlash);
	DDX_Check(pDX, IDC_CHECK18, _check_SetAutoShutdownTime);
	DDX_Control(pDX, IDC_STATIC1, _static_SetSiteID);
	DDX_Control(pDX, IDC_STATIC2, _static_InstallVNC);
	DDX_Control(pDX, IDC_STATIC3, _static_SetVNCAdmin);
	DDX_Control(pDX, IDC_STATIC4, _static_ChangeVNCBackground);
	DDX_Control(pDX, IDC_STATIC5, _static_InstallCodec);
	DDX_Control(pDX, IDC_STATIC6, _static_SetCodecProp);
	DDX_Control(pDX, IDC_STATIC7, _static_InstallDivX);
	DDX_Control(pDX, IDC_STATIC9, _static_InstallFLV);
	DDX_Control(pDX, IDC_STATIC8, _static_CreateSoftDrive);
	DDX_Control(pDX, IDC_STATIC11, _static_CreateShortCut);
	DDX_Control(pDX, IDC_STATIC12, _static_ChangeComputerName);
	DDX_Control(pDX, IDC_STATIC13, _static_SetSecurity1);
	DDX_Control(pDX, IDC_STATIC14, _static_SetSecurity2);
	DDX_Control(pDX, IDC_STATIC15, _static_ReleaseErrorReport);
	DDX_Control(pDX, IDC_STATIC16, _static_SetFirewallException);
	DDX_Control(pDX, IDC_STATIC17, _static_InstallFlash);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, _shutdownTime);
	DDX_Control(pDX, IDOK2, NextBtn);
	DDX_Control(pDX, IDCANCEL, finishBtn);
	DDX_Control(pDX, IDC_STATIC_LOGO, logoVar);
	DDX_Control(pDX, IDC_EDIT_IP, _EDIT_IP);
	DDX_Control(pDX, IDC_STATIC_IP, _STATIC_IP);
	DDX_Control(pDX, IDC_STATIC_IP_GROUP, _GROUP_IP);
	DDX_Control(pDX, IDC_BUTTON_IP_SEARCH, _BUTTON_IP_SEARCH);
	DDX_Control(pDX, IDC_BUTTON_IP_CONFIRM, _BUTTON_IP_CONFIRM);
	DDX_Control(pDX, IDC_CHECK9, FLV_CHECK9);
}

BEGIN_MESSAGE_MAP(CExtraInstallDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CExtraInstallDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CExtraInstallDlg::OnBnClickBN)
	ON_BN_CLICKED(IDOK2, &CExtraInstallDlg::OnBnClickNext)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER1, &CExtraInstallDlg::OnDtnDatetimechangeDatetimepicker1)
	ON_BN_CLICKED(IDC_CHECK1, &CExtraInstallDlg::OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, &CExtraInstallDlg::OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_CHECK3, &CExtraInstallDlg::OnBnClickedCheck3)
	ON_BN_CLICKED(IDC_CHECK4, &CExtraInstallDlg::OnBnClickedCheck4)
	ON_BN_CLICKED(IDC_CHECK5, &CExtraInstallDlg::OnBnClickedCheck5)
	ON_BN_CLICKED(IDC_CHECK6, &CExtraInstallDlg::OnBnClickedCheck6)
	ON_BN_CLICKED(IDC_CHECK7, &CExtraInstallDlg::OnBnClickedCheck7)
	ON_BN_CLICKED(IDC_CHECK8, &CExtraInstallDlg::OnBnClickedCheck8)
	ON_BN_CLICKED(IDC_CHECK9, &CExtraInstallDlg::OnBnClickedCheck9)
	ON_BN_CLICKED(IDC_CHECK11, &CExtraInstallDlg::OnBnClickedCheck11)
	ON_BN_CLICKED(IDC_CHECK12, &CExtraInstallDlg::OnBnClickedCheck12)
	ON_BN_CLICKED(IDC_CHECK13, &CExtraInstallDlg::OnBnClickedCheck13)
	ON_BN_CLICKED(IDC_CHECK14, &CExtraInstallDlg::OnBnClickedCheck14)
	ON_BN_CLICKED(IDC_CHECK15, &CExtraInstallDlg::OnBnClickedCheck15)
	ON_BN_CLICKED(IDC_CHECK16, &CExtraInstallDlg::OnBnClickedCheck16)
	ON_BN_CLICKED(IDC_CHECK17, &CExtraInstallDlg::OnBnClickedCheck17)
	ON_BN_CLICKED(IDC_CHECK18, &CExtraInstallDlg::OnBnClickedCheck18)
	ON_BN_CLICKED(IDCANCEL, &CExtraInstallDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_IP_CONFIRM, &CExtraInstallDlg::OnBnClickedButtonIpConfirm)
	ON_BN_CLICKED(IDC_BUTTON_IP_SEARCH, &CExtraInstallDlg::OnBnClickedButtonIpSearch)
END_MESSAGE_MAP()


// CExtraInstallDlg 메시지 처리기

BOOL CExtraInstallDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// skpark logfile
	/*
	out.open("ExtraInstall.log", ios_base::out);
	if( out.fail() ) {
		printf("ExtraInstall.log open failed.\n");
	} 
	ciDEBUG(1,("Extra Install Start"));
	*/
	ciArgParser::initialize(__argc,__argv);
	ciEnv::defaultEnv(ciFalse, "utv1");
	ciDebug::setDebugOn();
	ciDebug::logOpen("ExtraInstall.log");
/*
	const char* originalPath = getenv("PATH");
	ciEnv::defaultEnv(true,"utv1");

	ciString newPath = ciEnv::getenv("PATH");

	newPath += ";";
	newPath += originalPath;

	ciEnv::setEnv("PATH", newPath.c_str());

	ciDEBUG(1,("PATH=%s" ,newPath.c_str()));
	ciDEBUG(1,("PATH=%s" ,getenv("PATH")));
*/


	// ACE init
	ACE::init();

	// ORB Init
	_orbThread = new ::cciGUIORBThread(__argc,__argv,"UBCCENTER");
	_orbThread->start();

	ciDEBUG(1,("ORB init"));

	ATL::CTime aTime(2008,03,01,22,00,00);
	_shutdownTime.SetTime(&aTime);
	

	//////////////////////////////////////////////////////////////
	/*
	 * 디폴트로 한글모드로 설정 뒤에 +eng 붙여서 실행시 영어모드로 변환
	 */
	CString stk1,stk2,stk3,stk4;
	CString eng = "+eng";
	
	stk1 = "                Key : ";
	stk2 = "Thanks For Using UBC Studio";
	stk3 = "Please Insert Authentication Key";
	stk4 = "";
/*
	CString strArgC, strArgV[20];
    strArgC.Format("%d", __argc );

    for( int iCount = 0; iCount < __argc; iCount++)
    {
         strArgV[iCount] = __argv[iCount];
    }
	if(strArgV[1] == eng){
*/
	/*
	if(ciArgParser::getInstance()->isSet("+wedding")){
		// swf 파일의 링크를 만들어 주어야 한다.
		ciString command = UBC_EXE_PATH;
		command += "\\junction.exe ";
		command += UBC_CONTENT_PATH;
		command += "\\ ";
		command += UBC_EXE_PATH;
		command += "\\flash\\plugins_wedding.swf";

		system(command.c_str());
		ciDEBUG(1,("%s executed", command.c_str()));

		::CreateS

	}
	*/
	if(ciArgParser::getInstance()->isSet("+eng")){
		Static1.SetWindowText(stk1);
   		Static2.SetWindowText(stk2);
		//Static3.SetWindowText(stk3);
		//Static4.SetWindowText(stk4);
	} 
	_edition = STANDARD_EDITION;
	if(ciArgParser::getInstance()->isSet("+PLS")){
		_edition = PLUS_EDITION;
	
	}
	if(ciArgParser::getInstance()->isSet("+ENT")){
		_edition = ENTERPRISE_EDITION;
		_setEnterpriseIp();
	}else{
		_EDIT_IP.EnableWindow(0);
		_STATIC_IP.EnableWindow(0);
		_GROUP_IP.EnableWindow(0);
		_BUTTON_IP_CONFIRM.EnableWindow(0);
		_BUTTON_IP_SEARCH.EnableWindow(0);
		ubcConfig aIni("UBCStarter");
		if(!aIni.set("PREDOWNLOADER","AUTO_STARTUP","false")){
			ciERROR(("UBCStarter.ini file  PREDOWNLOADER::AUTO_STARTUP=false failed"));
		}else{
			ciDEBUG(1,("UBCStarter.ini file PREDOWNLOADER::AUTO_STARTUP=false changed"));
		}
	}

	//////////////////////////////////////////////////////////////
	/* 에디트 박스 내 문자열 길이 제한 */ 
	((CEdit *) GetDlgItem(IDC_EDIT1))->SetLimitText(10);
	((CEdit *) GetDlgItem(IDC_EDIT2))->SetLimitText(5);
	//////////////////////////////////////////////////////////////

	// 현재 키값을 보여준다.
	ciString host, mac, edition;
	if(scratchUtil::getInstance()->readAuthFile(host,mac,edition)){
		ciDEBUG(1,("host=%s, mac=%s,edition=%s", host.c_str(), mac.c_str(),edition.c_str()));
	}else{
		ciDEBUG(1,("ERROR: readAuth fail"));
	}
	if(!host.empty()){
		int idx =host.find('-');
		if(idx>0){
			ciString prefix=host.substr(0,idx);
			ciString port=host.substr(idx+1);
			m_stredit1 = prefix.c_str();
			Edit1.SetWindowText(prefix.c_str());
			Edit2.SetWindowText(port.c_str());
			ciDEBUG(1,("prefix=%s, port=%s", prefix.c_str(), port.c_str()));
			_host = host;
			_site = prefix;
			_port = port;
			
			if(_edition == ENTERPRISE_EDITION){
				//ciString currentSiteId;
				//installUtil::getInstance()->getSiteId(currentSiteId);
				//if(currentSiteId.empty() || currentSiteId == "NULL" || currentSiteId != _site){
					GetSiteId();
				//}
			}
		}
	}else{
		edition = _edition;
		_host = "";
		_site = "";
		_port = "";
	}

	if(edition==ENTERPRISE_EDITION){
		ciString customerInfo;
		ubcConfig aIni("UBCVariables");
		aIni.get("CUSTOMER_INFO","NAME",customerInfo);
		if(customerInfo=="NARSHA"){
			HBITMAP bitmap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP4));
			HBITMAP oldBitMap = logoVar.SetBitmap(bitmap);
			DeleteObject(oldBitMap);
		}else{
			HBITMAP bitmap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP2));
			HBITMAP oldBitMap = logoVar.SetBitmap(bitmap);
			DeleteObject(oldBitMap);
		}
	}else if(edition==PLUS_EDITION){
		HBITMAP bitmap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP3));
		HBITMAP oldBitMap = logoVar.SetBitmap(bitmap);
		DeleteObject(oldBitMap);
	}

	_codecPath = UBC_3RDPARTY_PATH;
	_codecPath += "\\Codec\\";
	_codecPath += CODEC_FILE; 
	_codecVer = "latest";

	ifstream in(_codecPath.c_str(), ios_base::in);
	if( in.fail() ) {
		ciWARN(("%s open failed. %s file will be used\n",_codecPath.c_str(), CODEC_OLD_FILE));
		_codecPath = UBC_3RDPARTY_PATH;
		_codecPath += "\\Codec\\";
		_codecPath += CODEC_OLD_FILE; 
		_codecVer="4.7";
	}else{
		in.close();
	}
	ciDEBUG(1,("DEBUG CODEC ver=%s" ,_codecVer.c_str()));


	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.


	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	UpdateData(TRUE);
	if(ciArgParser::getInstance()->isSet("+authOnly")){
		_check_SetSiteID=FALSE;
		_check_InstallVNC=FALSE;
		_check_SetVNCAdmin=FALSE;
		_check_ChangeVNCBackground=FALSE;
		_check_InstallCodec=FALSE;
		_check_SetCodecProp=FALSE;
		_check_InstallDivX=FALSE;
		_check_InstallFLV=FALSE;
		_check_CreateSoftDrive=FALSE;
		//_check_InstallFTP=FALSE;
		//_check_SetFTPAdmin=FALSE;
		_check_CreateShortCut=FALSE;
		_check_ChangeComputerName=FALSE;
		_check_SetSecurity1=FALSE;
		_check_SetSecurity2=FALSE;
		_check_ReleaseErrorReport=FALSE;
		_check_SetFirewallException=FALSE;
		_check_InstallFlash=FALSE;
		_check_SetAutoShutdownTime=FALSE;
		NextBtn.EnableWindow(FALSE);
		finishBtn.EnableWindow(TRUE);
	}else{
		NextBtn.EnableWindow(FALSE);
		finishBtn.EnableWindow(FALSE);
	}
	UpdateData(FALSE);
	CheckIntalledItem();
	OnBnClickedCheck18();

	debugOff();

	HWND hWnd = ::FindWindow(NULL, "Installation of UBC");
	if(hWnd){
		HWND hPWnd = ::GetParent(hWnd);
		if(hPWnd)
			::ShowWindow(hPWnd, SW_SHOWMINIMIZED);
		else
			::ShowWindow(hWnd, SW_SHOWMINIMIZED);
	}

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CExtraInstallDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CExtraInstallDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CExtraInstallDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CExtraInstallDlg::OnBnClickedOk()
{
	OnOK();
}

void CExtraInstallDlg::OnBnClickedCancel()
{
	HWND hWnd = ::FindWindow(NULL, "Installation of UBC");
	if(hWnd){
		HWND hPWnd = ::GetParent(hWnd);
		if(hPWnd)
			::ShowWindow(hPWnd, SW_SHOWMAXIMIZED);
		else
			::ShowWindow(hWnd, SW_SHOWMAXIMIZED);
	}

	OnCancel();
}

void CExtraInstallDlg::OnBnClickBN()
{
	CString SEdit1,SEdit2;  

	Edit1.GetWindowText(SEdit1);
	Edit2.GetWindowText(SEdit2);

	if(SEdit1 == "" || SEdit2 == ""){
		MessageBox("Please Insert Authentication Key");
		return ;
	}

	ciDEBUG(1,("DEBUG %s,%s" ,SEdit1, SEdit2));

	// 대문자로 전환
	SEdit1.MakeUpper();
	SEdit2.MakeUpper();
	Edit1.SetWindowText(SEdit1);
	Edit2.SetWindowText(SEdit2);
	m_stredit1 = SEdit1;
	
	_site = SEdit1;
	_host = SEdit1;
	_host += "-";
	_host += SEdit2;
	_port = SEdit2;

	ciDEBUG(1,("DEBUG %s,%s" ,_site.c_str(), _host.c_str()));


	ciBoolean reset = ciFalse;
	ciString errString = "";
	ciString mac = "";

	// 인증은 Center 에 행해지며, Center 에 인증시 site ID 는 항상 host ID 와 일치한다는
	// 사실에 주의한다.
	
	if(AUTH_SUCCEED != installUtil::getInstance()->auth(_site.c_str(),_host.c_str(),_edition.c_str(),
														errString,mac,reset)){
		MessageBox(errString.c_str());
		ciDEBUG(1,(errString.c_str()));
		_hasAuth = ciTrue;
		if(_edition == ENTERPRISE_EDITION){
			GetSiteId();
		}
	}else{
		_hasAuth = ciTrue;
		MessageBox(errString.c_str());
		ciDEBUG(1,(errString.c_str()));
		ciDEBUG(1,(mac.c_str()));
		if(_edition == ENTERPRISE_EDITION){
			GetSiteId();
		}
		NextBtn.EnableWindow(TRUE);
		CheckIntalledItem();
	}
	finishBtn.EnableWindow(TRUE);
	/*	
	if((SEdit1 == Str1) && (SEdit2 == Str2)){
		MessageBox("정상적으로 인증 되었습니다.");
	} else {
		MessageBox("인증에 실패하였습니다. 다시 입력해 주세요.");
		Edit1.SetWindowTextA("");
		Edit2.SetWindowTextA("");
		return ;
	}
	*/
}

bool
CExtraInstallDlg::GetSiteId()
{
	ciString command = _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\UTV_sample.exe ");
	command += "+noES +noxlog +util +getSiteId +host ";
	command += _host;
	ciString prefix = _site;

	if(system(command.c_str())==0){
		ciDEBUG(1,("Get SITE ID Succeed"));
	}else{
		ciERROR(("Get Site ID failed"));
	}
	installUtil::getInstance()->getSiteId(_site); // 어쨌거나 UBCVariables.ini 값을 site 값으로 사용함
	if(_site=="NULL"){		// 그러나 UBCVariable.ini 가 NULL 이라면, 초기화 안되었다는 뜻이므로 
		_site=prefix;      // 그런 경우는 할수 없이 hostId 를 믿는편이 옳다는 것.
		installUtil::getInstance()->setSiteId(_site.c_str());
	}
	ciDEBUG(1,("Before Site ID = %s , After Site ID = %s", prefix.c_str(), _site.c_str()));
	return true;
}

void CExtraInstallDlg::OnBnClickNext()
{
	ciDEBUG(1,("OnBnClickNext()"));

	NextBtn.EnableWindow(FALSE);
	UpdateData(TRUE);

	if(!_hasAuth  || _site.empty() || _host.empty() ){
		MessageBox("Please Authentication First");
		return ;
	}
	
	installUtil* aUtil = installUtil::getInstance();

	if(_check_SetSiteID==TRUE){
		if(!aUtil->setSiteId(_site.c_str())){
			MessageBox("ERROR : Set SiteID failed");
		}else{
			MessageBox("Set SiteID Succeed");
		}
	}
	if(_check_InstallVNC==TRUE){
		if(!aUtil->installVNC("", _port.c_str())){
			MessageBox("ERROR : Install VNC failed");
		}else{
			MessageBox("Install VNC Succeed");
			if(_edition=="ENT"){
				if(!aUtil->updateVNCBinary(_port.c_str())){
					MessageBox("ERROR : Update VNC exe failed");
				}else{
					MessageBox("Update VNC exe Succeed");
					if(!installUtil::getInstance()->setProperty("STARTER.APP_VNC_Proxy.AUTO_STARTUP","false",ciTrue)) {
						ciERROR(("Set Auto off of VNCProxy failed"));
					}
				}
			}
		}
	}

	// VNC Admin check 을 다시한번 더 해준다.
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK3);
	if(CheckVNCAdmin()){
		pCheck->SetCheck(FALSE);
	}else{
		pCheck->SetCheck(TRUE);
	}
	_check_SetVNCAdmin = pCheck->GetCheck();
	if(_check_SetVNCAdmin==TRUE){
		if(!aUtil->updateVNCAdmin("", _port.c_str())){
			MessageBox("ERROR : Set VNC Admin failed");
		}else{
			MessageBox("Set VNC Admin Succeed");
		}
	}

	if(_check_ChangeVNCBackground==TRUE){
		if(!aUtil->updateVNCBackground()){
			MessageBox("ERROR : Update VNC Background failed");
		}else{
			MessageBox("Update VNC Background Succeed");
		}
	}
	if(_check_InstallCodec==TRUE){
		if(!aUtil->installCodec(_codecPath.c_str())){
			MessageBox("ERROR : install CODEC failed");
		}else{
			MessageBox("install CODEC Succeed");
		}
	}
	if(_check_SetCodecProp==TRUE){
		if(_codecVer!="latest"){
			if(!aUtil->oldCodecSetting()){
				MessageBox("ERROR : Set CODEC Properties failed");
			}else{
				MessageBox("Set CODEC Properties Succeed");
			}
		}else{
			/*
			if(scratchUtil::getInstance()->IsWindows7()){
				if(!aUtil->lastestCodecSettingWin7()){
					MessageBox("ERROR : Set WIN7 CODEC Properties failed");
				}else{
					MessageBox("Set WIN7 CODEC Properties Succeed");
				}
			}
			*/
			// 문제를 일으키므로 하지 않는다.
			/*
			if(!aUtil->lastestCodecSetting()){
				MessageBox("ERROR : Set CODEC Properties failed");
			}else{
				MessageBox("Set CODEC Properties Succeed");
			}
			*/
		}
	}
	if(_check_InstallFLV==TRUE){
		if(!aUtil->installFLV()){
			MessageBox("ERROR : Install FLV failed");
		}else{
			MessageBox("Install FLV Succeed");		
		}
	}
	if(_check_InstallDivX==TRUE){
		if(!aUtil->installDivX()){
			MessageBox("ERROR : Install DivX failed");
		}else{
			MessageBox("Install DivX Succeed");		
		}
	}

	if(_check_CreateSoftDrive==TRUE){
		if(!aUtil->runSoftDriveBat()){
			MessageBox("ERROR : Create Soft Drive failed");
		}else{
			MessageBox("Create Soft Drive Succeed");		
		}
	}
	/*
	if(_check_InstallFTP==TRUE){
		if(!aUtil->installFTP()){
			MessageBox("ERROR : Install FTP Server failed");
		}else{
			MessageBox("Install FTP Server Succeed");
		}
	}
	if(_check_SetFTPAdmin==TRUE){
		if(!aUtil->updateFTPAdmin()){
			MessageBox("ERROR : Set FTP Properties failed");
		}else{
			MessageBox("Set FTP Properties Succeed");
		}
	}
	*/
	if(_check_CreateShortCut==TRUE){
		ciString command = "MenuRegister.exe /install";
		if(system(command.c_str())!=0){
			MessageBox("ERROR : MenuRegister failed");
		}else{
			MessageBox("MenuRegister Succeed");
		}
	}
	if(_check_ChangeComputerName==TRUE){
		if(!aUtil->setComputerName(_host.c_str())){
			MessageBox("ERROR : Set Computer Name failed");
		}else{

			MessageBox("Set Computer Name Succeed");
		}
	}
	if(_check_SetSecurity1==TRUE){
		if(!aUtil->drm1(0)){
			MessageBox("ERROR : set Security level 1 failed");
		}else{
			MessageBox("Set Security Level 1 Succeed");
		}
	}
	if(_check_SetSecurity2==TRUE){
		if(!aUtil->setRegistry1(0)){
			MessageBox("ERROR : Set Security Level 2 failed");
		}else{
			MessageBox("Set Security Level 2 Succeed");
		}		
	}
	if(_check_ReleaseErrorReport==TRUE){
		if(!aUtil->setErrorReport()){
			//MessageBox("ERROR : Release Error Report failed");
		}else{
			MessageBox("Release Error Report Succeed");
		}
	}
	if(_check_SetFirewallException==TRUE){
		if(!aUtil->setFirewallException()){
			MessageBox("ERROR : Set Firewall Exception failed");
		}else{
			MessageBox("Set Firewall Exception Succeed");
		}
	}
	if(_check_InstallFlash==TRUE){
		if(!aUtil->installFlashPlayer()){
			MessageBox("ERROR : Install Flash failed");
		}else{
			MessageBox("Install Flash Succeed");
		}
	}
	if(_check_SetAutoShutdownTime==TRUE){
		//...
		ATL::CTime aTime;
		_shutdownTime.GetTime(aTime);
		char buf[10];
		memset(buf,0x00,10);
		sprintf(buf, "%02d:%02d", aTime.GetHour(),aTime.GetMinute());
		ciString timeStr=buf;
		if(!aUtil->setShutdownTime(timeStr,ciFalse)){
			MessageBox("ERROR : Set AutoShutdownTime failed");
		}else{
			MessageBox("Set AutoShutdownTime Succeed");
		}
	}

	finishBtn.EnableWindow(TRUE);

	if(!CheckIntalledItem())
		NextBtn.EnableWindow(TRUE);
}
/*
void
CExtraInstallDlg::mylog(const char *pFormat,...) 
{
 	char	aBuf[4096];
	va_list	aVp;
	va_start(aVp, pFormat);
	vsprintf(aBuf, pFormat, aVp);

	printf("%s\n", (const char*)aBuf);
	out << aBuf << endl;
}
*/
void CExtraInstallDlg::OnDestroy()
{
	
	CDialog::OnDestroy();

	//cciEventManager::clearInstance();
	ciDEBUG(1,("ORB End"));
	if(_orbThread) {
		_orbThread->destroy();
		delete _orbThread;
		_orbThread=0;
	}

	ciDEBUG(1,("Extra Install End"));
	out.close();
}

void CExtraInstallDlg::OnDtnDatetimechangeDatetimepicker1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	
	*pResult = 0;
}

void CExtraInstallDlg::OnBnClickedCheck18()
{
	UpdateData();
	if(_check_SetAutoShutdownTime==FALSE) {
		_shutdownTime.ShowWindow(SW_HIDE);
	}else{
		_shutdownTime.ShowWindow(SW_NORMAL);
	}
}

void CExtraInstallDlg::OnBnClickedCheck1()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK1);

	if(!CheckSiteID()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck2()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK2);
	if(!CheckVNC()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck3()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK3);

	if(!CheckVNCAdmin()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck4()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK4);
	if(!CheckVBCBack()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck5()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK5);
	if(!CheckCodec()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck6()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK6);
	if(!CheckCodecProp()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck7()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK7);
	if(!CheckDivX()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck8()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK8);
	if(!CheckSoftDrive()){
		pCheck->SetCheck(TRUE);
	}
}
void CExtraInstallDlg::OnBnClickedCheck9()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK9);
	if(!CheckFLV()){
		pCheck->SetCheck(TRUE);
	}
}
void CExtraInstallDlg::OnBnClickedCheck11()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK11);
	if(!CheckShortCut()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck12()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK12);

	if(!CheckComputerName()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck13()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK13);
	if(!CheckSecurity1()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck14()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK14);
	if(!CheckSecurity2()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck15()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK15);
	if(!CheckErrorReport()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck16()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK16);
	if(!CheckFirewallExc()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::OnBnClickedCheck17()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK17);
	if(!CheckFlash()){
		pCheck->SetCheck(TRUE);
	}
}

void CExtraInstallDlg::GetMyComputerName(ciString& prefix, ciString& port)
{
	CString szTemp;
	char lpBuffer[1024];
	DWORD nSize = sizeof(lpBuffer);

	HKEY hKey;
	LONG lRet;

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,_T("SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ComputerName"),0,KEY_ALL_ACCESS,&hKey);
	if(lRet == ERROR_SUCCESS){
		RegQueryValueEx(hKey,_T("ComputerName"),NULL, NULL, (LPBYTE)lpBuffer, &nSize);
	}
	RegCloseKey(hKey);

//	GetComputerNameEx(ComputerNamePhysicalDnsHostname, lpBuffer, &nSize);
//	ciDEBUG(1,("GetMyComputerName-NameEx %s", lpBuffer));

	ciString host = lpBuffer;
	if(!host.empty()){
		int idx =host.find('-');
		if(idx>0){
			szTemp=host.substr(0,idx).c_str();
			szTemp.MakeUpper();
			prefix = szTemp;

			szTemp=host.substr(idx+1).c_str();
			szTemp.MakeUpper();
			port=szTemp;
			ciDEBUG(1,("GetMyComputerName:prefix=%s, port=%s", prefix.c_str(), port.c_str()));
		}
	}
}

bool CExtraInstallDlg::CheckIntalledItem()
{
	ciDEBUG(1,("CheckIntalledItem()"));

	bool bRet = true;
	CButton* pCheck=0;

	HKEY hKey;
	LONG lRet;
	CString szBuf;
	DWORD dwIndex = 0;
	DWORD dwValue;
	char szValue[1024];

	UpdateData(TRUE);

	// Codec & VNC Check
	RegOpenKeyEx(HKEY_LOCAL_MACHINE,_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"),0,KEY_ALL_ACCESS,&hKey);
	do{
		dwValue = sizeof(szValue);
		ZeroMemory(szValue, dwValue);
		lRet = RegEnumKeyEx(hKey, dwIndex++, szValue, &dwValue, NULL, NULL, NULL, NULL);
		
		szBuf = szValue;
		szBuf.MakeLower();

		if(!m_bDixVCodec && szBuf.Find(_T("divx plus")) != -1){
			m_bDixVCodec  = TRUE;
		}
		else if(!m_bFLV && szBuf.Find(_T("flv splitter")) != -1){
			m_bFLV  = TRUE;
		}
		else if(!m_bKLiteCodec && szBuf.Find(_T("klitecodec")) != -1){
			m_bKLiteCodec = TRUE;
		}
		else if(!m_bUtrVnc && szBuf.Find(_T("ultravnc")) != -1){
			m_bUtrVnc = TRUE;
		}
	}while(lRet == ERROR_SUCCESS);
	RegCloseKey(hKey);

	// Flash Check
	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,_T("SOFTWARE\\Macromedia\\FlashPlayer"),0,KEY_ALL_ACCESS,&hKey);
	if(lRet == ERROR_SUCCESS){
		dwValue = sizeof(szValue);
		RegQueryValueEx(hKey,_T("CurrentVersion"),NULL, NULL, (LPBYTE)szValue, &dwValue);
		RegCloseKey(hKey);

		int iStart = 0;
		szBuf = szValue;
		CString szToken = szBuf.Tokenize(",", iStart);
		if(szToken.IsEmpty()){
			m_bFlash = FALSE;
		}else{
			sprintf(szValue, "%s", szToken);
			if(atoi(szValue) >= 10)
				m_bFlash = TRUE;
			else
				m_bFlash = FALSE;
		}
	}else{
		m_bFlash = FALSE;
	}
	
	pCheck = (CButton*)GetDlgItem(IDC_CHECK1);
	if(CheckSiteID()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_SetSiteID = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK2);
	if(CheckVNC()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_InstallVNC = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK3);
	if(CheckVNCAdmin()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_SetVNCAdmin = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK4);
	if(CheckVBCBack()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_ChangeVNCBackground = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK5);
	if(CheckCodec()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_InstallCodec = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK6);
	if(CheckCodecProp()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_SetCodecProp = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK9);
	if(CheckFLV()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_InstallFLV = pCheck->GetCheck();

	if(_codecVer!="latest"){
		pCheck = (CButton*)GetDlgItem(IDC_CHECK7);
		if(CheckDivX()){
			pCheck->SetCheck(FALSE);
		}else{
			bRet = false;
			pCheck->SetCheck(TRUE);
		}
	}else{
		pCheck->SetCheck(FALSE);
		_static_InstallDivX.SetTextColor(RGB(0x00,0x00,0x00));
		_static_InstallDivX.SetWindowText("(Ignored)");
	}
	_check_InstallDivX = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK8);
	if(CheckSoftDrive()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_CreateSoftDrive = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK11);
	if(CheckShortCut()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_CreateShortCut = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK12);
	if(CheckComputerName()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_ChangeComputerName = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK13);
	if(CheckSecurity1()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_SetSecurity1 = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK14);
	if(CheckSecurity2()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_SetSecurity2 = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK15);
	if(CheckErrorReport()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_ReleaseErrorReport = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK16);
	if(CheckFirewallExc()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_SetFirewallException = pCheck->GetCheck();

	pCheck = (CButton*)GetDlgItem(IDC_CHECK17);
	if(CheckFlash()){
		pCheck->SetCheck(FALSE);
	}else{
		bRet = false;
		pCheck->SetCheck(TRUE);
	}
	_check_InstallFlash = pCheck->GetCheck();

	UpdateData(FALSE);
	return bRet;
}

bool CExtraInstallDlg::CheckSiteID()
{
	ciDEBUG(1,("CheckSiteID()"));

	_static_SetSiteID.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_SetSiteID.SetWindowText("(Not installed)");

	ciString currentSite;
	installUtil::getInstance()->getSiteId(currentSite);
	if(currentSite.length() == 0){
		return false;
	}
/*
	CEdit* pPrefix = (CEdit*)GetDlgItem(IDC_EDIT1);
	char prefix[1024];
	pPrefix->GetWindowText(prefix, sizeof(prefix));
	if(strlen(prefix) == 0){
		return false;
	}
	
	CEdit* pVncPort = (CEdit*)GetDlgItem(IDC_EDIT2);
	char vncPort[1024];
	pVncPort->GetWindowText(vncPort, sizeof(vncPort));
	if(strlen(vncPort) == 0){
		return false;
	}

	ciString hostId = prefix;
	hostId += "-";
	hostId += vncPort;

	ciString siteId;
	installUtil::getInstance()->getSiteId(hostId.c_str(), siteId);
	_site = siteId;
*/
	if(currentSite != _site){
		return false;
	}

	ciString buf = "(";
	buf += _site;
	buf += ")";
	_static_SetSiteID.SetTextColor(RGB(0x00,0x00,0x00));
	_static_SetSiteID.SetWindowText(buf.c_str());
	
	return true;
}

bool CExtraInstallDlg::CheckVNC()
{
	ciDEBUG(1,("CheckVNC()"));

	if(m_bUtrVnc){
		_static_InstallVNC.SetTextColor(RGB(0x00,0x00,0x00));
		_static_InstallVNC.SetWindowText("(Installed)");
	}else{
		_static_InstallVNC.SetTextColor(RGB(0xFF,0x00,0x00));
		_static_InstallVNC.SetWindowText("(Not installed)");
	}

	return m_bUtrVnc;
}

bool CExtraInstallDlg::CheckVNCAdmin()
{
	ciDEBUG(1,("CheckVNCAdmin()"));

	ciString port;
	char szBuf[1024];
	CEdit* pPort = (CEdit*)GetDlgItem(IDC_EDIT2);
	pPort->GetWindowText(szBuf, sizeof(szBuf));
	port = szBuf;

	_static_SetVNCAdmin.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_SetVNCAdmin.SetWindowText("(Not installed)");

	if(port.empty())
		return false;

	if(!installUtil::getInstance()->checkVNCAdmin("", port.c_str()))
		return false;
/*
	if(!installUtil::getInstance()->checkVNCPasswd())    
		return false;
*/
	_static_SetVNCAdmin.SetTextColor(RGB(0x00,0x00,0x00));
	_static_SetVNCAdmin.SetWindowText("(Installed)");

	return true;
}

bool CExtraInstallDlg::CheckVBCBack()
{
	ciDEBUG(1,("CheckVBCBack()"));

	_static_ChangeVNCBackground.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_ChangeVNCBackground.SetWindowText("(Not installed)");

	CFileFind ff;
	if(!ff.FindFile("C:\\Windows\\system32\\background.bmp")){
		ff.Close();
		return false;
	}
	ff.Close();

	_static_ChangeVNCBackground.SetTextColor(RGB(0x00,0x00,0x00));
	_static_ChangeVNCBackground.SetWindowText("(Installed)");

	return true;
}

bool CExtraInstallDlg::CheckCodec()
{
	ciDEBUG(1,("CheckCodec()"));

	if(m_bKLiteCodec){
		_static_InstallCodec.SetTextColor(RGB(0x00,0x00,0x00));
		_static_InstallCodec.SetWindowText("(Installed)");
	}else{
		_static_InstallCodec.SetTextColor(RGB(0xFF,0x00,0x00));
		_static_InstallCodec.SetWindowText("(Not installed)");
	}

	return m_bKLiteCodec;
}

bool CExtraInstallDlg::CheckCodecProp()
{
	ciDEBUG(1,("CheckCodecProp()"));

	_static_SetCodecProp.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_SetCodecProp.SetWindowText("(Not installed)");

	if(_codecVer!="latest"){
		if(!installUtil::getInstance()->checkCodecSetting()){
			return false;
		}
	}else{
		/*
		if(scratchUtil::getInstance()->IsWindows7()){
			if(!installUtil::getInstance()->checkCodecSettingWin7()){
				return false;
			}
		}
		*/
		if(!installUtil::getInstance()->checkCodecSetting()){
			return false;
		}
	}

	_static_SetCodecProp.SetTextColor(RGB(0x00,0x00,0x00));
	_static_SetCodecProp.SetWindowText("(Installed)");

	return true;
}

bool CExtraInstallDlg::CheckDivX()
{
	ciDEBUG(1,("CheckDivX()"));

	if(m_bDixVCodec){
		_static_InstallDivX.SetTextColor(RGB(0x00,0x00,0x00));
		_static_InstallDivX.SetWindowText("(Installed)");
	}else{
		_static_InstallDivX.SetTextColor(RGB(0xFF,0x00,0x00));
		_static_InstallDivX.SetWindowText("(Not installed)");
	}

	return m_bDixVCodec;
}
bool CExtraInstallDlg::CheckFLV()
{
	ciDEBUG(1,("CheckFLV()"));

	if(m_bFLV){
		_static_InstallFLV.SetTextColor(RGB(0x00,0x00,0x00));
		_static_InstallFLV.SetWindowText("(Installed)");
	}else{
		ciString filename = UBC_3RDPARTY_PATH;
		filename += "\\Codec\\flv_splitter.exe";
		if(installUtil::getInstance()->checkVerify(filename.c_str())){
			_static_InstallFLV.SetTextColor(RGB(0xFF,0x00,0x00));
			_static_InstallFLV.SetWindowText("(Not installed)");
		}else{
			_static_InstallFLV.SetTextColor(RGB(0x00,0x00,0x00));
			_static_InstallFLV.SetWindowText("(Ignored)");
			m_bFLV = ciTrue;
		}
	}
	ciDEBUG(1,("CheckFLV end()"));

	return m_bFLV;
}
bool CExtraInstallDlg::CheckSoftDrive()
{
	ciDEBUG(1,("CheckSoftDrive()"));

	_static_CreateSoftDrive.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_CreateSoftDrive.SetWindowText("(Not installed)");


	CFileFind ff;
	if(!ff.FindFile("X:\\SqiSoft\\*.*")){
		ff.Close();
		return false;
	}
	ff.Close();

	_static_CreateSoftDrive.SetTextColor(RGB(0x00,0x00,0x00));
	_static_CreateSoftDrive.SetWindowText("(Installed)");

	return true;
}

bool CExtraInstallDlg::CheckShortCut()
{
	ciDEBUG(1,("CheckShortCut()"));

	_static_CreateShortCut.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_CreateShortCut.SetWindowText("(Not installed)");

	CString szTarget;
	char TargetPath[MAX_PATH+1];
	::SHGetSpecialFolderPath (NULL, TargetPath, CSIDL_STARTUP, FALSE);
	szTarget = TargetPath;
	szTarget += "\\UBCReady.lnk";

	CFileFind ff;
	if(!ff.FindFile(szTarget)){
		ff.Close();
		return false;
	}
	ff.Close();

	_static_CreateShortCut.SetTextColor(RGB(0x00,0x00,0x00));
	_static_CreateShortCut.SetWindowText("(Installed)");

	return true;
}

bool CExtraInstallDlg::CheckComputerName()
{
	ciDEBUG(1,("CheckComputerName()"));

	// Site Check
	ciString prefix, port;
	GetMyComputerName(prefix, port);

	char szPrefix[1024], szPort[1024];
	CEdit* pPrefix = (CEdit*)GetDlgItem(IDC_EDIT1);
	CEdit* pVncPort = (CEdit*)GetDlgItem(IDC_EDIT2);

	_static_ChangeComputerName.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_ChangeComputerName.SetWindowText("(Not installed)");

	if(prefix.length() == 0)
		return false;

	if(port.length() == 0)
		return false;
	
	pPrefix->GetWindowText(szPrefix, sizeof(szPrefix));
	if(strlen(szPrefix) == 0)
		return false;

	pVncPort->GetWindowText(szPort, sizeof(szPort));
	if(strlen(szPort) == 0)
		return false;

	if(prefix != szPrefix)
		return false;

	if(port != szPort)
		return false;

	_static_ChangeComputerName.SetTextColor(RGB(0x00,0x00,0x00));
	_static_ChangeComputerName.SetWindowText("(Installed)");

	return true;
}

bool CExtraInstallDlg::CheckSecurity1()
{
	ciDEBUG(1,("CheckSecurity1()"));

	_static_SetSecurity1.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_SetSecurity1.SetWindowText("(Not installed)");

	const char* szFiles=_COP_CD("C:\\SQISoft\\Contents");
	CFileFind ff;
	if(!ff.FindFile(szFiles)){
		ff.Close();
		return false;
	}
	ff.FindNextFile();
	if(!ff.IsHidden()){
		ff.Close();
		return false;
	}
	ff.Close();

	_static_SetSecurity1.SetTextColor(RGB(0x00,0x00,0x00));
	_static_SetSecurity1.SetWindowText("(Installed)");

	return true;
}

bool CExtraInstallDlg::CheckSecurity2()
{
	ciDEBUG(1,("CheckSecurity2()"));

	_static_SetSecurity2.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_SetSecurity2.SetWindowText("(Not installed)");

	HKEY hKey;
	LONG lRet;
	CString regKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced\\Folder\\Hidden\\SHOWALL";

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,regKey,0,KEY_ALL_ACCESS,&hKey);
	if(lRet != ERROR_SUCCESS){
		RegCloseKey(hKey);
		return false;
	}

	DWORD dwValue;
	DWORD dwSize = sizeof(dwValue);
	RegQueryValueEx(hKey,_T("CheckedValue"),NULL, NULL, (LPBYTE)&dwValue, &dwSize);
	RegCloseKey(hKey);

	if(0 != dwValue)
		return false;

	_static_SetSecurity2.SetTextColor(RGB(0x00,0x00,0x00));
	_static_SetSecurity2.SetWindowText("(Installed)");

	return true;
}

bool CExtraInstallDlg::CheckErrorReport()
{
	ciDEBUG(1,("CheckErrorReport()"));

	_static_ReleaseErrorReport.SetTextColor(RGB(0x00,0x00,0x00));
	_static_ReleaseErrorReport.SetWindowText("(Ignored)");

	HKEY hKey;
	LONG lRet;
	CString regKey = "SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting";

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,regKey,0,KEY_ALL_ACCESS,&hKey);
	if(lRet != ERROR_SUCCESS){
		RegCloseKey(hKey);
		return true;
	}

	DWORD dwValue=0;
	DWORD dwSize = sizeof(dwValue);
	RegQueryValueEx(hKey,_T("ShowUI"),NULL, NULL, (LPBYTE)&dwValue, &dwSize);
	RegCloseKey(hKey);

	if(0 != dwValue)
		return false;

	_static_ReleaseErrorReport.SetTextColor(RGB(0x00,0x00,0x00));
	_static_ReleaseErrorReport.SetWindowText("(Installed)");

	return true;
}

bool CExtraInstallDlg::CheckFirewallExc()
{
	ciDEBUG(1,("CheckFirewallExc()"));

	_static_SetFirewallException.SetTextColor(RGB(0xFF,0x00,0x00));
	_static_SetFirewallException.SetWindowText("(Not installed)");

	if(installUtil::getInstance()->checkFirewallException()){
		_static_SetFirewallException.SetTextColor(RGB(0x00,0x00,0x00));
		_static_SetFirewallException.SetWindowText("(Installed)");
		return true;
	}	
	return false;
}

bool CExtraInstallDlg::CheckFlash()
{
	ciDEBUG(1,("CheckFlash()"));

	if(m_bFlash){
		_static_InstallFlash.SetTextColor(RGB(0x00,0x00,0x00));
		_static_InstallFlash.SetWindowText("(Installed)");
	}else{
			_static_InstallFlash.SetTextColor(RGB(0xFF,0x00,0x00));
		_static_InstallFlash.SetWindowText("(Not installed)");
	}

	return m_bFlash;
}

void CExtraInstallDlg::OnBnClickedButtonIpConfirm()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	char szIP[256];
	memset(szIP,0x00,256);

	_EDIT_IP.GetWindowText(szIP,sizeof(szIP));

	if(strlen(szIP) < 7 ){
		ciERROR(("Invalid IP Address"));
		MessageBox("Invalid IP Address");
		return;
	}

	_BUTTON_IP_CONFIRM.EnableWindow(0);


	ubcConfig aIni("UBCConnect");
	ciString oldIp;
	aIni.get("ORB_NAMESERVICE","IP", oldIp);


	if(!dbUtil::getInstance()->changeServerIP(szIP,ciFalse)){
		ciERROR(("Confirm failed"));
		_BUTTON_IP_CONFIRM.EnableWindow(1);
		MessageBox("Confirm failed");
		return;
	}
	ciDEBUG(1,("Confirm succeed"));
	if(oldIp != szIP){
		//int counter = ubcIniMux::getInstance()->changeIp(oldIp.c_str(), (const char*)szIP);
		_BUTTON_IP_CONFIRM.EnableWindow(1);
		char buf[1024];
		sprintf(buf,"%s(%s-->%s)", "Server IP is Changed. Please restart system", oldIp.c_str(), szIP);
		MessageBox(buf);
		return ;
	}
	_BUTTON_IP_CONFIRM.EnableWindow(1);
	MessageBox("Confirm succeed");
	return;

}

void CExtraInstallDlg::OnBnClickedButtonIpSearch()
{
	/*
	ciString host, mac, edition,site;
	if(scratchUtil::getInstance()->readAuthFile(host,mac,edition)){
		ciDEBUG(1,("host=%s, mac=%s,edition=%s", host.c_str(), mac.c_str(),edition.c_str()));
	}else{
		ciDEBUG(1,("ERROR: readAuth fail"));
		MessageBox("Plaease Input Key first");
		return;
	}
	*/

	if(_host.empty()){
		MessageBox("Plaease Input Authentication Key first");
		return;
	}

	ciString key,ip;
	ubcEnterprise ent;
	if(ent.getEnterpriseKey("*",_host.c_str(), key)){
		if(ent.getEnterpriseIp(key.c_str(), ip)){
			_EDIT_IP.SetWindowText(ip.c_str());
			//_BUTTON_IP_CONFIRM.EnableWindow(1);
			return;
		}
	}
	
	MessageBox("Enterprise Server IP Search failed, Input IP manually");
	//_BUTTON_IP_CONFIRM.EnableWindow(1);
	return;
}

void
CExtraInstallDlg::_setEnterpriseIp()
{
	ciDEBUG(1,("_setEnterpriseIp()"));

	ubcConfig aIni("UBCConnect");
	ciString ip;
	aIni.get("ORB_NAMESERVICE","IP", ip);
	if(ip.empty()){
		//_BUTTON_IP_CONFIRM.EnableWindow(0);
		return;
	}
	_EDIT_IP.SetWindowText(ip.c_str());
	return ;

}

void
CExtraInstallDlg::debugOff()
{
	ciDEBUG(1,("debugOff"));

	const char*  ignoreDebugEnv = ciEnv::newEnv("IGNORE_COP_DEBUG");

	if(ignoreDebugEnv){
		ciDEBUG(1,("IGNORE_COP_DEBUG={%s}",ignoreDebugEnv));
		return;
	}

	// IGNORE_COP_DEBUG 가 셋팅되어 있지 않다면, 이는 처음 인스톨 순간이라고 본다.
	// 이 함수는 처음 인스톨 했을때만 호출되어야 한다.

	ciDEBUG(1,("IGNORE_COP_DEBUG does not set"));

	scEnv::getInstance()->setSysEnv("IGNORE_COP_DEBUG","1");
	ciEnv::setEnv("IGNORE_COP_DEBUG","1");
	ciDEBUG(1,("IGNORE_COP_DEBUG=1"));

}