//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ExtraInstall.rc
//
#define IDOK2                           3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_EXTRAINSTALL_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     130
#define IDB_BITMAP2                     131
#define IDB_BITMAP3                     132
#define IDB_BITMAP4                     134
#define IDC_BUTTON1                     1000
#define IDC_EDIT1                       1001
#define IDC_EDIT2                       1002
#define IDC_STATIC_1                    1003
#define IDC_STATIC_2                    1004
#define IDC_STATIC_3                    1005
#define IDC_STATIC_4                    1006
#define IDC_CHECK1                      1007
#define IDC_CHECK2                      1008
#define IDC_CHECK3                      1009
#define IDC_CHECK4                      1010
#define IDC_CHECK5                      1011
#define IDC_CHECK6                      1012
#define IDC_CHECK7                      1013
#define IDC_CHECK8                      1014
#define IDC_EDIT_IP                     1015
#define IDC_CHECK10                     1016
#define IDC_STATIC_IP                   1016
#define IDC_CHECK11                     1017
#define IDC_CHECK12                     1018
#define IDC_CHECK13                     1019
#define IDC_CHECK14                     1020
#define IDC_CHECK15                     1021
#define IDC_CHECK16                     1022
#define IDC_CHECK17                     1023
#define IDC_DATETIMEPICKER1             1024
#define IDC_CHECK18                     1025
#define IDC_STATIC1                     1026
#define IDC_STATIC2                     1027
#define IDC_STATIC3                     1028
#define IDC_STATIC4                     1029
#define IDC_STATIC5                     1030
#define IDC_STATIC6                     1031
#define IDC_STATIC7                     1032
#define IDC_STATIC8                     1033
#define IDC_STATIC11                    1034
#define IDC_STATIC10                    1035
#define IDC_STATIC12                    1035
#define IDC_STATIC13                    1036
#define IDC_STATIC14                    1037
#define IDC_STATIC15                    1038
#define IDC_STATIC16                    1039
#define IDC_STATIC17                    1040
#define IDC_STATIC_LOGO                 1041
#define IDC_BUTTON_IP_SEARCH            1042
#define IDC_BUTTON3                     1043
#define IDC_BUTTON_IP_CONFIRM           1043
#define IDC_STATIC_MISC                 1044
#define IDC_STATIC_IP_GROUP             1045
#define IDC_CHECK9                      1046
#define IDC_STATIC9                     1047

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1046
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
