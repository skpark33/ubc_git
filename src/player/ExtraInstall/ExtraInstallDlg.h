// ExtraInstallDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include <stdlib.h>
#include <stdio.h>
#include <list>
#include <fstream>
#include "cci/libWorld/cciGUIORBThread.h"

using namespace std;
#include "afxdtctl.h"
#include "colorstaticst.h"


// CExtraInstallDlg 대화 상자
class CExtraInstallDlg : public CDialog
{
// 생성입니다.
public:
	CExtraInstallDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_EXTRAINSTALL_DIALOG };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;
	
	//skpark logfile
	ofstream out;
	::cciGUIORBThread* _orbThread;
	ciString	_host;
	ciString	_site;
	ciString	_port;
	ciBoolean	_hasAuth;
	ciString	_edition;

	CString m_stredit1;
	CEdit Edit2;
	CEdit Edit1;

	CStatic Static1;
	CStatic Static2;
	CDateTimeCtrl _shutdownTime;
	CButton NextBtn;
	CButton finishBtn;

	BOOL m_bDixVCodec;
	BOOL m_bFLV;
	BOOL m_bKLiteCodec;
	BOOL m_bUtrVnc;
	BOOL m_bFlash;

	BOOL _check_SetSiteID;
	BOOL _check_InstallVNC;
	BOOL _check_SetVNCAdmin;
	BOOL _check_ChangeVNCBackground;
	BOOL _check_InstallCodec;
	BOOL _check_SetCodecProp;
	BOOL _check_InstallDivX;
	BOOL _check_InstallFLV;
	BOOL _check_CreateSoftDrive;
	//BOOL _check_InstallFTP;
	//BOOL _check_SetFTPAdmin;
	BOOL _check_CreateShortCut;
	BOOL _check_ChangeComputerName;
	BOOL _check_SetSecurity1;
	BOOL _check_SetSecurity2;
	BOOL _check_ReleaseErrorReport;
	BOOL _check_SetFirewallException;
	BOOL _check_InstallFlash;
	BOOL _check_SetAutoShutdownTime;

	CColorStaticST _static_SetSiteID;
	CColorStaticST _static_InstallVNC;
	CColorStaticST _static_SetVNCAdmin;
	CColorStaticST _static_ChangeVNCBackground;
	CColorStaticST _static_InstallCodec;
	CColorStaticST _static_SetCodecProp;
	CColorStaticST _static_InstallFLV;
	CColorStaticST _static_InstallDivX;
	CColorStaticST _static_CreateSoftDrive;
	CColorStaticST _static_CreateShortCut;
	CColorStaticST _static_ChangeComputerName;
	CColorStaticST _static_SetSecurity1;
	CColorStaticST _static_SetSecurity2;
	CColorStaticST _static_ReleaseErrorReport;
	CColorStaticST _static_SetFirewallException;
	CColorStaticST _static_InstallFlash;

	void GetMyComputerName(ciString&, ciString&);
	bool CheckIntalledItem();
	bool CheckSiteID();
	bool CheckVNC();
	bool CheckVNCAdmin();
	bool CheckVBCBack();
	bool CheckCodec();
	bool CheckCodecProp();
	bool CheckDivX();
	bool CheckFLV();
	bool CheckSoftDrive();
	bool CheckShortCut();
	bool CheckComputerName();
	bool CheckSecurity1();
	bool CheckSecurity2();
	bool CheckErrorReport();
	bool CheckFirewallExc();
	bool CheckFlash();

	bool GetSiteId();

	void	debugOff();

	// 생성된 메시지 맵 함수
	DECLARE_MESSAGE_MAP()
public:
	// skpark logfile
	//void mylog(const char *pFormat,...);
//	BOOL StrCheck(CString Str1,CString Str2);

	void	_setEnterpriseIp();

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickBN();
	afx_msg void OnBnClickNext();
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDtnDatetimechangeDatetimepicker1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnBnClickedCheck3();
	afx_msg void OnBnClickedCheck4();
	afx_msg void OnBnClickedCheck5();
	afx_msg void OnBnClickedCheck6();
	afx_msg void OnBnClickedCheck7();
	afx_msg void OnBnClickedCheck8();
	afx_msg void OnBnClickedCheck9();
	afx_msg void OnBnClickedCheck11();
	afx_msg void OnBnClickedCheck12();
	afx_msg void OnBnClickedCheck13();
	afx_msg void OnBnClickedCheck14();
	afx_msg void OnBnClickedCheck15();
	afx_msg void OnBnClickedCheck16();
	afx_msg void OnBnClickedCheck17();
	afx_msg void OnBnClickedCheck18();
	afx_msg void OnBnClickedCancel();
	CStatic logoVar;
	afx_msg void OnBnClickedButtonIpConfirm();
	afx_msg void OnBnClickedButtonIpSearch();
	CEdit _EDIT_IP;
	CStatic _STATIC_IP;
	CStatic _GROUP_IP;
	CButton _BUTTON_IP_SEARCH;
	CButton _BUTTON_IP_CONFIRM;

	ciString _codecPath;
	ciString _codecVer;
	CButton FLV_CHECK9;
};
