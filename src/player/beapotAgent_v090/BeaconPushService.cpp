#include "BeaconPushService.h"

#include <ci/libDebug/ciDebug.h>
#include "ci/libBase/ciStringTokenizer.h"
#include "common/libScratch/scratchUtil.h"
#include "common/libCommon/ubcIni.h"

ciSET_DEBUG(5, "BeaconPushService");

BeaconPushService::BeaconPushService(void) : flash_msg_queue(true)
{
    ciDEBUG(5, ("BeaconPushService() "));

    // Default 값 셋팅
    flash_ip_ = "127.0.0.1";
    flash_port_ = 9000;
    site_id_ = "SE";
    api_adapter_ip_ = "127.0.0.1";
    api_adapter_port_ = 8000;

    loadIni();

    goodsInfoManager* p_goods_info_mgr = goodsInfoManager::getInstance();
    p_goods_info_mgr->setConnection(site_id_.c_str(), api_adapter_ip_.c_str(), api_adapter_port_);

	isSimul = ciFalse;
}

BeaconPushService::~BeaconPushService(void)
{
}

void BeaconPushService::loadIni()
{
    //C:\\SQISoft\\UTV1.0\\execute\\data\\UBCVariables.ini
	ubcConfig aIni("UBCVariables");

	aIni.get("OFS", "FLASH_IP", flash_ip_);
	aIni.get("OFS", "FLASH_TCP_PORT", flash_port_);
    aIni.get("ROOT", "SITEID", site_id_);
    // XML에서 읽어야 한다.
    aIni.get("OFS", "API_ADAPTER_IP", api_adapter_ip_);
    aIni.get("OFS", "API_ADAPTER_PORT", api_adapter_port_);

    // Default 값 셋팅
    if (site_id_ == "") site_id_ = "SE";

    if (flash_port_ == 0) {
        flash_ip_ = "127.0.0.1";
        flash_port_ = 9000;
    }

    if (api_adapter_port_ == 0) {
        api_adapter_ip_ = "127.0.0.1";
        api_adapter_port_ = 8000;
    }
}

void BeaconPushService::Start()
{
    ciDEBUG(5, ("Start() "));

    ciDEBUG(3, ("before BeapotWorker thread start()"));    
    started_ = true;
    start();
    ciDEBUG(3, ("after BeapotWorker thread start()"));

}

void BeaconPushService::Stop()
{
    ciDEBUG(5, ("Stop... "));
	if(started_) {
		started_ = false;
		stop(); 
	}
}

void BeaconPushService::run()
{
    ciDEBUG(5, ("run... "));

    while(started_) {

        // 2015-10-21 beacon_action_queue_.empty()가 오동작하는 것
        if (beacon_action_queue_.size() > 0)
        {
            BEACON_ACTION_INFO beacon_action_info;
            {
            ciGuard aGuard(beacon_action_queue_lock_);
            beacon_action_info = beacon_action_queue_.front();
            beacon_action_queue_.pop_front();
            }
            ciDEBUG2(1,("run(beacon_action_queue_.size<%d>)", beacon_action_queue_.size()));

            //1. goodsInfoManager에서 상품정보 매핑
            goodsInfoList goods_info_list;
            GetGoodsInfo_(beacon_action_info, goods_info_list);

            //2. XML Message 생성 -> Flash msg 생성            
            string send_flash_msg = CreateGoodsInfoMsg_(beacon_action_info.action_type, goods_info_list);

            //3. TcpStreamChannel로 전송
            if (send_flash_msg != "" && isSimul == ciFalse) {
                SendMessageFlash(send_flash_msg);
            }

            //4. PickCount, mirrorCount DB insert & get best123 list
            //   It sends BEST123 message every goods_id
            string send_flash_best123_msg = SetPickupAndMirrorCount_SendFlash_(beacon_action_info.action_type, goods_info_list);
            if (send_flash_best123_msg != "" && isSimul == ciFalse) {
                SendMessageFlash(send_flash_best123_msg);
            }

            SEND_FLASH_MSG st_flash_msg;
            if (send_flash_msg != "" && isSimul == ciTrue) {
                st_flash_msg.access_pickup_msg  = send_flash_msg;
                st_flash_msg.best123_msg        = send_flash_best123_msg;
                flash_msg_queue.push_back(st_flash_msg);
                ciDEBUG(5, ("flash_msg_queue.size:%d [%s]", flash_msg_queue.size(), send_flash_msg.c_str() ));
            }

        } else {
            Sleep(100); // 100ms
        }
        
    }
}


void BeaconPushService::processEvent(ciString& actionType, ciStringList& beaconIdList, int additionalState)
{
    ciDEBUG2(1,("processEvent(%s, beaconListCnt[%d], %d) ", actionType.c_str(), beaconIdList.size(), additionalState));
    
    BEACON_ACTION_INFO action_info(actionType, beaconIdList); 
    {
    ciGuard aGuard(beacon_action_queue_lock_);        
    beacon_action_queue_.push_back(action_info);
    }
    ciDEBUG2(5, ("processEvent(beacon_action_queue_.size<%d>)", beacon_action_queue_.size()));
}


bool BeaconPushService::GetGoodsInfo_(BEACON_ACTION_INFO& beacon_action_info, goodsInfoList& out_goods_list)
{
    ciDEBUG(5, ("GetGoodsInfo_(beacon_action_info.beacon_id_list:%d) ", beacon_action_info.beacon_id_list.size() ));
    
    goodsInfoManager* p_goods_info_mgr = goodsInfoManager::getInstance();

    int real_beacon_no = 0;
    goodsInfo goods_out_info;
    list<string>::iterator beacon_itr;
    for (beacon_itr = beacon_action_info.beacon_id_list.begin(); beacon_itr != beacon_action_info.beacon_id_list.end(); beacon_itr++) {
        
        ciString str_beacon_id = *beacon_itr;
        goodsInfo* p_goods_info = new goodsInfo;
        p_goods_info->goodsId = "NULL";
 
        if(str_beacon_id != "NULL") {
            ciDEBUG2(1,("GetGoodsInfo_(str_beacon_id:%s) ", str_beacon_id.c_str()));
            if (!p_goods_info_mgr->get(str_beacon_id.c_str(), *p_goods_info)) {
                ciDEBUG(5, ("p_goods_info_mgr->get. Not exists str_beacon_id[%s] ", str_beacon_id.c_str()));
                //continue; // 상품 못 가져오면 다음 상품조회
            } 
            real_beacon_no++;
        }
        out_goods_list.push_back(p_goods_info);
    }

    ciDEBUG(5, ("GetGoodsInfo_(real_beacon_no:%d) ", real_beacon_no ));
    return true;
}

string BeaconPushService::SetPickupAndMirrorCount_SendFlash_(ciString& action_type, goodsInfoList& in_goods_info_list)
{
    ciDEBUG(5, ("SetPickupAndMirrorCount_(%s, %d) ", action_type.c_str(), in_goods_info_list.size() ));
    
    goodsInfoManager* p_goods_info_mgr = goodsInfoManager::getInstance();
    string str_send_msg("");

    int i = 0;
    list<goodsInfo*>::iterator itr;
    for (itr = in_goods_info_list.begin(); itr != in_goods_info_list.end(); itr++) {
        goodsInfoList out_best123_list;
        goodsInfo* p_goods_info = *itr;
        if (p_goods_info->goodsId != "NULL") {
            if (!p_goods_info_mgr->setPickupCount(p_goods_info->goodsId.c_str(), action_type.c_str(), out_best123_list)) {
                ciDEBUG(5, ("p_goods_info_mgr->setPickupCount. Failed! goodsId_%d[%s] ", i, p_goods_info->goodsId.c_str() ));
                continue; // 상품 못 가져오면 다음 상품조회
            } 
            ciDEBUG(5, ("p_goods_info_mgr->setPickupCount. Okay! action_type[%s] goodsId_%d[%s]", action_type.c_str(), i, p_goods_info->goodsId.c_str()));
            // if the PICKUP, create and send BEST123 XML Message
            if (action_type == "PICKUP") {
                str_send_msg = CreateGoodsInfoMsg_(ciString("BEST123"), out_best123_list);
                if (str_send_msg != "") {
                    //SendMessageFlash(str_send_msg);
                    return str_send_msg;
                }
            }
        }

    }

    return "";
}

string BeaconPushService::CreateGoodsInfoMsg_(ciString& action_type, goodsInfoList& goods_info_list)
{
    ciDEBUG(5, ("CreateGoodsInfoMsg_(%s, %d) ", action_type.c_str(), goods_info_list.size()));

    if (goods_info_list.size() == 0) {
        ciDEBUG(5, ("CreateGoodsInfoMsg_ Not exist goods_info_list[")); 
        
        //return "";
    }

    // sample msg : "~AGNTXML_00000000004DATA"
    //string str_msg("~AGNTXML_0");
    string str_msg("");
    string str_xml("");
    string str_no_xml("");
    char sz_buf[11];
    memset(sz_buf, 0x0, 11);

    //[ XML create
	str_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	str_xml += "<ELIGA>\n";
	str_xml += "<BEACON>\n";
    str_xml += "<"; // MIRROR_ACCESS, PICKUP
	str_xml += action_type;
	str_xml += ">\n";
    
    char sz_no_buf[33];
    int goods_no = 0;
    int i = 0;    
    goodsInfoList::iterator itr;
    for (itr = goods_info_list.begin(); itr != goods_info_list.end(); itr++) {
        if (i >= 3) break; // 3개만 flash에 보낸다.

        str_no_xml = ciString("<NO") + itoa(i+1, sz_no_buf, 10) + ciString(">\n");
        //sprintf(sz_buf, "%s%d", str_no_xml.c_str(), i);
        str_xml += str_no_xml;

        goodsInfo* goods_info = *itr;
    
        str_xml += "<GOODSID>" + goods_info->goodsId   + "</GOODSID>\n";
        if(goods_info->goodsId != "NULL") {
            str_xml += "<NAME>"    + goods_info->goodsName + "</NAME>\n";
            str_xml += "<PRICE>"   + goods_info->price     + "</PRICE>\n";
            str_xml += "<SIZE>"    + goods_info->size_std  + "</SIZE>\n";
            str_xml += "<COLOR>"   + goods_info->color     + "</COLOR>\n";
            goods_no++;
        }
        if (action_type == "BEST123") {
            str_xml += ciString("<COUNT>") + itoa(goods_info->pickCount, sz_no_buf, 10)  + ciString("</COUNT>\n");
        }

        str_no_xml = ciString("</NO") + itoa(i+1, sz_no_buf, 10) + ciString(">\n");
        str_xml += str_no_xml;
        i++;
    }

    str_xml += "</"; // MIRROR_ACCESS, PICKUP
	str_xml += action_type;
	str_xml += ">\n";
    str_xml += "</BEACON>\n";
    str_xml += "</ELIGA>";
    //]

    // Length write
    //sprintf(sz_buf, "%010d", (int) str_xml.size());

    //str_msg += ciString(sz_buf);
    str_msg += str_xml;

    ciDEBUG(1,("CreateGoodsInfoMsg_(%s, %d, RealGoodsNo:%d) XML_MSG[%s]", action_type.c_str(), goods_info_list.size(), goods_no, str_msg.c_str() ));
    return str_msg;
}

string  BeaconPushService::CreateBest123Msg_(goodsInfoList& goods_info_list)
{
    ciDEBUG(5, ("CreateBest123Msg_() "));

    string str_msg("");

    return str_msg;

}


void BeaconPushService::SendMessageFlash(string& str_send_msg)
{
    ciDEBUG(1,("SendMessageFlash(\n%s) ", str_send_msg.c_str() ));

    flash_channel_.StartConnect(flash_ip_, flash_port_, CONNECT_TCP_ONETRANSACTION_CHANNEL);
    int send_len = flash_channel_.SendMessage(str_send_msg.c_str(), (int) str_send_msg.size());
    if (send_len <= 0) {
        ciDEBUG2(1, ("flash_channel_.SendMessage failed. send_len[%d] msg[%d][\n%s\n] ", send_len, str_send_msg.size(), str_send_msg.c_str() ));
        if (isSimul) {
            ciWARN( ("flash_channel_.SendMessage failed. send_len[%d] msg[%d][\n%s\n] ", send_len, str_send_msg.size(), str_send_msg.c_str() ));
        }
    } else {
        ciDEBUG2(1, ("flash_channel_.SendMessage success.len[%d] msg[\n%s\n]", send_len, str_send_msg.size(), str_send_msg.c_str() ));
        if (isSimul) {
            ciWARN( ("flash_channel_.SendMessage failed. send_len[%d] msg[%d][\n%s\n] ", send_len, str_send_msg.size(), str_send_msg.c_str() ));
        }
    }
    flash_channel_.Close();

}