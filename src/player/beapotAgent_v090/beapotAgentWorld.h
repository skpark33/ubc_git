 /*! \file beapotAgentWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _beapotAgentWorld_h_
#define _beapotAgentWorld_h_


#include <ci/libWorld/ciWorld.h> 
#include "BeaconPushService.h"

class beapotAgentWorld : public ciWorld {
public:
	beapotAgentWorld();
	~beapotAgentWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

    void    BeaconAgentStart();
    void    BeaconSimulatorStart();

	static int send(char* p_param);

protected:
    static BeaconPushService* p_push_service;
};
		
#endif //_beapotAgentWorld_h_
