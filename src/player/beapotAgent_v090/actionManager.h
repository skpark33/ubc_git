#ifndef _actionManager_h_
#define _actionManager_h_

#include <ci/libThread/ciSyncUtil.h>
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libTimer/ciWinTimer.h>

class actionHandler {
public:
	virtual void processEvent(ciString& actionType, ciStringList& beaconIdList,
								int additionalState) = 0;
};

class beaconInfo {
public:
	beaconInfo() :rssi(0), xiro_x(0), xiro_y(0), xiro_z(0), battery(0), order(0), state(0), no_move_count(0) {};
	virtual ~beaconInfo() {};

	ciString beaconId;
	ciString scannerId;
	ciLong	 battery;
	ciLong	 rssi;
	ciLong	 xiro_x;
	ciLong	 xiro_y;
	ciLong	 xiro_z;
	ciTime	lastUpdateTime;
	time_t	order;
	int		state;
	int		no_move_count;
};

typedef map<ciString, beaconInfo*>	beaconInfoMap;
typedef multimap<time_t, beaconInfo*>	beaconInfoSortedMap;

class axisEvalData {
public:
	axisEvalData() :x_count(0), y_count(0), z_count(0) {};
	virtual ~axisEvalData() {};

	int		 x_count;
	int		 y_count;
	int		 z_count;
};
typedef map<ciString, axisEvalData*>	axisEvalDataMap;


class actionTimer;

class actionManager {
public:	
	actionManager(const char* actionType, actionHandler* handler, int maxOrder=3, int expiredSec=2);
	virtual ~actionManager();

	void	start(int interval=1);
	ciBoolean clear();

	virtual int 	received(const char* beaconId, const char* scannerId,
						ciLong battery, ciLong rssi,
						ciLong xiro_x, ciLong xiro_y, ciLong xiro_z) = 0;

	virtual	void	timeExpired() = 0;

	virtual void	loadIni();

protected:
	enum 
	{
		AXIS_X = 1,
		AXIS_Y,
		AXIS_Z
	};

	void		_axisEval_in(const char* beaconId,ciLong x,ciLong y,ciLong z);
	int			_axisEval_out(const char* beaconId);

	actionHandler*			_handler;
	beaconInfoMap			_map;
	ciMutex					_mapLock;
	ciString				_actionType;
	ciShort					_maxOrder;
	ciShort					_expiredSec;
	actionTimer*			_timer;

	axisEvalDataMap			_axisEvalMap;
};

class actionTimer : public ciWinPollable
{
public:
	actionTimer(actionManager* manager) : _manager(manager) {};
	virtual ~actionTimer() {};

	virtual void processExpired(ciString name, int counter, int interval)
	{
		if(counter == 1) return;
		if(_manager) {
			_manager->timeExpired();
		}
	}

protected:
	actionManager*	_manager;
};


class accessActionManager : public actionManager
{
public:
	accessActionManager(actionHandler* handler, int maxOrder=3, int expiredSec=2)
		:actionManager("MIRROR_ACCESS", handler,maxOrder,expiredSec) {};
	virtual ~accessActionManager() {};

	virtual int 	received(const char* beaconId, const char* scannerId,
						ciLong battery, ciLong rssi,
						ciLong xiro_x, ciLong xiro_y, ciLong xiro_z);

	virtual	void	timeExpired();
};

class pickupActionManager : public actionManager
{
public:
	pickupActionManager(actionHandler* handler, int threshold = 5, int maxOrder=3, int expiredSec=2)
		:actionManager("PICKUP", handler,maxOrder,expiredSec)
	{ _no_move_threshold_count = threshold; _pickup_threshold = 500; };
	virtual ~pickupActionManager() {};

	virtual int 	received(const char* beaconId, const char* scannerId,
						ciLong battery, ciLong rssi,
						ciLong xiro_x, ciLong xiro_y, ciLong xiro_z);

	virtual	void	timeExpired();
	virtual void	loadIni();

	enum 
	{
		NO_MOVE = 1,
		MOVE,
		PICK_UP
	};


protected:
	int _actionAnalisys(const char* beaconId, ciLong x,ciLong y,ciLong z);

	ciLong _no_move_threshold_count;
	ciLong _pickup_threshold;
};



#endif // _actionManager_h_
