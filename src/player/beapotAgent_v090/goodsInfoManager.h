#ifndef _goodsInfoManager_h_
#define _goodsInfoManager_h_

#include <ci/libThread/ciSyncUtil.h>
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libTimer/ciWinTimer.h>

class CStringArray;

class goodsInfo {
public:
	goodsInfo() :mirrorCount(0), pickCount(0) {};
	virtual ~goodsInfo() {};

	void set(goodsInfo* p) {
		goodsId		= p->goodsId;
		price		= p->price;
		material	= p->material;
		color		= p->color;
		size_std	= p->size_std;
		goodsName	= p->goodsName;
		mirrorCount		= p->mirrorCount;
		pickCount		= p->pickCount;
	}

	ciString goodsId;
	ciString price;
	ciString size_std;
	ciString goodsName;
	ciString material;
	ciString color;
	int		 mirrorCount;
	int		 pickCount;

	ciTime	lastUpdateTime;
};

typedef map<ciString, goodsInfo*>	goodsInfoMap;
typedef list<goodsInfo*>	goodsInfoList;


class goodsInfoManager {
public:	
	static goodsInfoManager* getInstance();
	static void clearInstance();

	virtual ~goodsInfoManager();
	
	void setConnection(const char* siteId, const char* serverIp="127.0.0.1", ciULong port=8000) 
	{ _serverIp=serverIp; _port=port; _siteId = siteId;}
	
	ciBoolean get(const char* beaconId, goodsInfo& outval);
	ciBoolean setPickupCount(const char* goodsId, const char* actionType, goodsInfoList& outBest123);

	void setErr(const char* err) { _errMsg = err; };
	void clearErr() { _errMsg = "";}

	void	clear();
protected:
	goodsInfoManager();

	ciBoolean _resultCheck(CStringArray& line_list);
	goodsInfo* _parseGoodsXML(CStringArray& line_list, int startIdx, int endIdx);
	int _parseGoodsListXML(CStringArray& line_list, goodsInfoList& outBest123);

	goodsInfoMap			_map;
	ciMutex					_mapLock;

	ciString			_serverIp;
	ciULong				_port;
	ciString			_siteId;

	ciString			_errMsg;

	static ciMutex _sLock;
	static goodsInfoManager* _instance;
};

#endif
