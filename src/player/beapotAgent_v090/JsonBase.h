/*! \class JsonObject
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author 
 *  \version 1.0
 *  \purpose
 *  \date 2015/10/06 11:01:00
 */

#ifndef PLAYER_BEAPOTAGENT_JSONOBJECT_H
#define PLAYER_BEAPOTAGENT_JSONOBJECT_H

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciSyncUtil.h>

class JsonValue {
public:
    JsonValue();
    virtual ~JsonValue();

    virtual int         parse(const char *szSrc, int nStart, int nLen) = 0;
    virtual int         getType()    { return _nType; }
    virtual bool        isNull()   	 { return (_nType == TYPE_NULL); }
    virtual bool        isNumber()   { return (_nType == TYPE_NUMBER); }
    virtual bool        isString()   { return (_nType == TYPE_STRING); }
    virtual bool        isArray()    { return (_nType == TYPE_ARRAY); }
    virtual bool        isObject()   { return (_nType == TYPE_OBJECT); }
    virtual void*       getValue() = 0;
    virtual JsonValue*  get(int nIndex)               { return NULL; }
    virtual JsonValue*  get(const char* szName)       { return NULL; }
	virtual int			getInt()					  { return -1; }
	virtual unsigned int	getUInt()				  { return -1; }
	virtual float		getFloat()					  { return -1; }
	virtual long int	getLongInt()				  { return -1; }
    virtual const char* getString()                   { return NULL; }
    virtual const char* getString(int nIndex)         { return NULL; }
    virtual const char* getString(const char* szName) { return NULL; }
    virtual string      toString() = 0;
	virtual int			toString(string& str) = 0;
    virtual void        debugPrint(int nIndent, const char *szPrefix = "", const char *szSuffix = "") = 0;
    virtual void        debugPrint2(FILE* fp, int nIndent, const char *szPrefix = "", const char *szSuffix = "") = 0;
    virtual int         getCount() = 0;
    virtual int         getCount(int nIndex)          { return 0; }
    virtual int         getCount(const char* szName)  { return 0; }
	//virtual bool		getStringMap(frStrStrMap& ValueMap) { return false;}

    enum {
		TYPE_NULL = 0,
        TYPE_STRING = 1,
        TYPE_NUMBER,
        TYPE_OBJECT,
        TYPE_ARRAY
    };

    static JsonValue *create(const char *szSrc, int nStart, int nLen, int *pNextPos = NULL);

    static int findChar(const char *szSrc, int nStart, int nLen, char nFindChar, char nEscapeChar);
    static int findIncluding(const char *szSrc, int nStart, int nLen, const char *szFindCharSet);
    static int findExcluding(const char *szSrc, int nStart, int nLen, const char *szFindCharSet);

protected:
    int _nType;

};

class JsonNull : public JsonValue {
public:
    JsonNull();
	virtual ~JsonNull();

    int         parse(const char *szSrc, int nStart, int nLen);
    void*       getValue();
    const char* getString();
    string      toString();
	int			toString(string& str);
    void        debugPrint(int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    void        debugPrint2(FILE* fp, int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    int         getCount()   { return 1; }

protected:
};

class JsonString : public JsonValue {
public:
    JsonString();
    JsonString(const char *value);
    JsonString(const char *value, int nLen);
	virtual ~JsonString();

    int         parse(const char *szSrc, int nStart, int nLen);
    void*       getValue()   { return &_value; }
    const char* getString()  { return _value.c_str(); }
	string		toString();
	int			toString(string& str);
    void        debugPrint(int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    void        debugPrint2(FILE* fp, int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    int         getCount()   { return 1; }

protected:
    string _value   ;
    string _strValue;

public:
    static string setEscapeChar(const char *szSrc, int nStart, int nLen);
    static string removeEscapeChar(const char *szSrc, int nStart, int nLen);
};

class JsonNumber : public JsonValue {
public:
    JsonNumber();
    JsonNumber(int nValue);
    JsonNumber(unsigned int nValue);
    JsonNumber(const char* nValue);
	virtual ~JsonNumber();

    int         parse(const char *szSrc, int nStart, int nLen);
    void*       getValue()   { return &_value; }
	int			getInt();
	unsigned int getUInt();
	float		getFloat();
	long int	getLongInt();
    const char* getString()  { return _value.c_str(); }
    string      toString();
	int			toString(string& str);
    void        debugPrint(int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    void        debugPrint2(FILE* fp, int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    int         getCount()   { return 1; }

protected:
    string _value;
};

class JsonArray : public JsonValue {
public:
    JsonArray();
    virtual ~JsonArray();

    int         parse(const char *szSrc, int nStart, int nLen);
    void*       getValue()              { return &_value; }
    JsonValue*  get(unsigned int nIndex)
    {
        if (nIndex < _value.size())
            return _value[nIndex];
        return NULL;
    }
    const char* getString(int nIndex)
    {
        JsonValue *p = get(nIndex);
        if (p)
            return p->getString();
        return NULL;
    }
    string      toString();
	int			toString(string& str);
    void        debugPrint(int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    void        debugPrint2(FILE* fp, int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    int         getCount()              { return (int) _value.size(); }
    int         getCount(int nIndex)
    {
        JsonValue *p = get(nIndex);
        if (p)
            return p->getCount();
        return 0;
    }

protected:
    vector<JsonValue*> _value;

public:
    int add(JsonValue  *pValue );
    int add(const char *szValue);
    int add(int         nValue );
    int add(unsigned int nValue );
};

typedef struct {
    JsonString *name ;
    JsonValue  *value;
} JSON_T_OBJECT_ELEMENT;

class JsonObject : public JsonValue {
public:
    JsonObject();
    virtual ~JsonObject();

    int         parse(const char *szSrc, int nStart, int nLen);
    void*       getValue()   { return &_value; }
    JsonValue*  get(const char *szName);
    JsonValue*  pop(const char *szName); //get and remove only pointer
    const char* getString(const char *szName)
    {
        JsonValue *p = get(szName);
        if (p)
            return p->getString();
        return NULL;
    }
    string      toString();
	int			toString(string& str);
    void        debugPrint(int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    void        debugPrint2(FILE* fp, int nIndent, const char *szPrefix = "", const char *szSuffix = "");
    int         getCount()  { return (int) _value.size(); }
    int         getCount(const char *szName)
    {
        JsonValue *p = get(szName);
        if (p)
            return p->getCount();
        return 0;
    }

protected:
    list<JSON_T_OBJECT_ELEMENT*> _value;

public:
    int add(const char *szName, JsonValue  *pValue );
    int add(const char *szName, const char *szValue);
    int add(const char *szName, int         nValue );
    int add(const char *szName, unsigned int nValue );
};

#define JSON_N_NULL_HEAD          'n'
#define JSON_N_ARRAY_HEAD         '['
#define JSON_N_ARRAY_TAIL         ']'
#define JSON_N_OBJECT_HEAD        '{'
#define JSON_N_OBJECT_TAIL        '}'
#define JSON_N_STRING_TIE         '"'
#define JSON_N_NAMEVALUE_TOKENER  ':'
#define JSON_N_TOKENER            ','
#define JSON_N_ESCAPE_CHAR        '\\'

#define JSON_S_NULL		          "null"
#define JSON_S_NULL_HEAD          "n"
#define JSON_S_ARRAY_HEAD         "["
#define JSON_S_ARRAY_TAIL         "]"
#define JSON_S_OBJECT_HEAD        "{"
#define JSON_S_OBJECT_TAIL        "}"
#define JSON_S_STRING_TIE         "\""
#define JSON_S_NAMEVALUE_TOKENER  ":"
#define JSON_S_TOKENER            ","
#define JSON_S_ESCAPE_CHAR        "\\"

#define JSON_S_IGNORE_CHARSET     "\n\r\t "
#define JSON_S_TOKEN_STOPPER      "\n\r\t []{}:,"

typedef vector<JsonValue*>              JSON_T_ARRAY_VALUE ;
typedef string                          JSON_T_STRING_VALUE;
typedef string                          JSON_T_NUMBER_VALUE;
typedef list<JSON_T_OBJECT_ELEMENT*>    JSON_T_OBJECT_VALUE;

#if 0
class JsonValuePtrVector : public vector<JsonValue*>
{
	public :
		~JsonValuePtrVector() {
			Clear();
		}

		void Clear() {
			DELETE_ITERATOR(JsonValuePtrVector);
		}

};
#endif

class JsonValueIntMap : public map<int, JsonValue*>
{
public:
	JsonValueIntMap();
	virtual ~JsonValueIntMap();

	void Release(bool deepRelease = true);
};

class JsonValueStringMap : public map<string, JsonValue*>
{
public:
	JsonValueStringMap();
	virtual ~JsonValueStringMap();

	void Release(bool deepRelease = true);
};

#endif // PLAYER_BEAPOTAGENT_JSONOBJECT_H
