/*! \class BeapotWorker
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/10/06 11:01:00
 */

#ifndef PLAYER_BEAPOTAGENT_BEACONWORKER_H
#define PLAYER_BEAPOTAGENT_BEACONWORKER_H

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include "actionManager.h"
#include "BufferBase.h"
#include "TcpStreamChannel.h"

typedef struct _BEACON_SCAN_MSG {
    string        action_type;
    string        beacon_id;
    string        scanner_id;
    ciLong        battery;
    ciLong        rssi;
    ciLong        xiro_x;
    ciLong        xiro_y;
    ciLong        xiro_z;
    string        ctime;
} BEACON_SCAN_MSG;

class BeapotWorker : public ServiceAccessWorker, public ciThread
{
public:
    BeapotWorker(void);
    virtual ~BeapotWorker(void);

    void Start();
    void Stop();
    void run();

    void RegisterTcpChannel(const char* channel_id, TcpStreamChannel* p_channel);
    void RegisterActionManager(const char* action_type, actionManager* p_action_manager);

    void RecvFromChannel(string& str_msg);
    void SendToService(string& str_msg);
    void SendToActionManager(BEACON_SCAN_MSG& beacon_scan_msg);

protected:
    bool ParseMessage_(string& str_msg, BEACON_SCAN_MSG& beacon_scan_msg);

protected:
    map<string, TcpStreamChannel*>  channel_registry_; // key : channelType
    map<string, actionManager*>     action_manager_registry_; // key : actionType

    list<string>                    channel_msg_queue_;
    ciMutex		                    channel_queue_lock_;
    bool                            started_;
};

#endif //PLAYER_BEAPOTAGENT_BEACONWORKER_H