#include "BeapotWorker.h"
#include "JsonBase.h"
#include "TcpStreamChannel.h"

#include <ci/libDebug/ciDebug.h>

ciSET_DEBUG(5, "BeapotWorker");

BeapotWorker::BeapotWorker(void)
{
    ciDEBUG(5, ("BeapotWorker... "));

    started_ = false;
}

BeapotWorker::~BeapotWorker(void)
{
    ciDEBUG(5, ("~BeapotWorker... "));
}

void BeapotWorker::Start()
{
    ciDEBUG(5, ("Start... "));

    ciDEBUG(3, ("before BeapotWorker thread start()"));    
    started_ = true;
    start();
    ciDEBUG(3, ("after BeapotWorker thread start()"));

}

void BeapotWorker::Stop()
{
    ciDEBUG(5, ("Stop... "));
	if(started_) {
		started_ = false;
		stop(); 
	}
}

void BeapotWorker::run()
{
    ciDEBUG(5, ("run... "));
    string str_msg("");

    while(started_) {
        if (channel_msg_queue_.size() > 1)
        {
            {
            ciGuard aGuard(channel_queue_lock_);
            str_msg = channel_msg_queue_.front();
            channel_msg_queue_.pop_front();
            }
            ciDEBUG2(5, ("run(channel_msg_queue_.size<%d>)", channel_msg_queue_.size()));

            //str_msg �Ľ�
            BEACON_SCAN_MSG beacon_scan_msg;
            bool b_ret = ParseMessage_(str_msg, beacon_scan_msg);
            if (b_ret) {
                SendToActionManager(beacon_scan_msg);
            }
        } else {
            Sleep(100); // 100ms
        }
    }
    ciDEBUG(5, ("run return #######################... "));
}

void BeapotWorker::RegisterTcpChannel(const char* channel_id, TcpStreamChannel* p_channel)
{
    ciDEBUG(5, ("RegisterTcpChannel(%s) ", channel_id));
    channel_registry_[channel_id] = p_channel;
    
    // Bridge of receiving message 
    p_channel->SetServiceAccessPoint(this); 
}

void BeapotWorker::RegisterActionManager(const char* action_type, actionManager* p_action_manager)
{
    ciDEBUG(5, ("RegisterActionManager(%s) ", action_type));
    action_manager_registry_[action_type] = p_action_manager;
}

void BeapotWorker::RecvFromChannel(string& str_msg)
{
    ciDEBUG2(1, ("RecvFromChannel(%s) ", str_msg.c_str() ));
    {
    ciGuard aGuard(channel_queue_lock_);
    channel_msg_queue_.push_back(str_msg);
    }
    ciDEBUG2(5, ("RecvFromChannel(channel_msg_queue_.size<%d>)", channel_msg_queue_.size()));
}

void BeapotWorker::SendToService(string& str_msg)
{
    ciDEBUG(5, ("SendToService(%s) ", str_msg.c_str() ));

}

void BeapotWorker::SendToActionManager(BEACON_SCAN_MSG& beacon_scan_msg)
{
    ciDEBUG(5, ("SendToActionManager() " ));
	map<string, actionManager*>::iterator itr;
    int ret = 0;
    for(itr=action_manager_registry_.begin(); itr!=action_manager_registry_.end(); itr++) {
        actionManager* p_action = itr->second;
        if(p_action) {        
            ret = p_action->received(beacon_scan_msg.beacon_id.c_str(), beacon_scan_msg.scanner_id.c_str(),
			                        beacon_scan_msg.battery, beacon_scan_msg.rssi,
			                        beacon_scan_msg.xiro_x, beacon_scan_msg.xiro_y, beacon_scan_msg.xiro_z);
        }
    }

}

bool BeapotWorker::ParseMessage_(string& str_msg, BEACON_SCAN_MSG& beacon_scan_msg)
{
    ciDEBUG(5, ("ParseMessage_(%s) ", str_msg.c_str()));
    //{
    //    "XIRO_BEACON": {
    //        "ACTION_TYPE": "PING",
    //        "XIRO": [
    //            "54300",
    //            "32674",
    //            "58568"
    //        ],
    //        "SCANNERID": "scanner105",
    //        "BEACONID": "00:18:9A:25:E1:A8",
    //        "RSSI": "X12X5741X",
    //        "BATTERY": "35",
    //        "CTIME": "2015-09-25 15:03:55"
    //    }
    //}
    // 

    //{
    //    "XIRO_BEACON": {
    //        "ACTION_TYPE": "PING",
    //        "XIRO": [
    //            "0",
    //            "0",
    //            "0"
    //        ],
    //        "SCANNERID": "sbsystems",
    //        "BEACONID": "D0:39:72:CD:D9:62",
    //        "RSSI": "-44",
    //        "BATTERY": "0",
    //        "CTIME": "2015-10-16 12:29:37"
    //    }
    //}

    string str_temp1("");
    string str_temp2("");
    string str_temp3("");


    JsonValue* json_value = JsonValue::create(str_msg.c_str(), 0, (int) str_msg.length());
    if (json_value == NULL) {
        ciDEBUG(5, ("JsonValue::create error... json_msg[%d] : %s <false>", str_msg.length(), str_msg.c_str() ));
        return false;
    }

    if (json_value->isObject() == false) {
        ciDEBUG(5, ("json_value->isObject() <false> "));
        delete json_value;
        return false;
    }

    JsonObject* msg_obj = (JsonObject*) json_value;
    JsonValue* temp_value = NULL;

    if ((temp_value = msg_obj->get("XIRO_BEACON")) == NULL) {
        ciDEBUG(5, ("Message[%s] is <false>", msg_obj->toString().c_str()));
        delete json_value;
        return false;
    }

    JsonObject* xiro_beacon = (JsonObject*) temp_value;
    if ((temp_value = xiro_beacon->get("ACTION_TYPE")) == NULL) {
        ciDEBUG(3, ("Message[%s] is <false>", xiro_beacon->toString().c_str()));
        delete json_value;
        return false;
    }
    str_temp1 = temp_value->getString();
    beacon_scan_msg.action_type = str_temp1;

    JsonArray* json_array = NULL;
    if ((json_array = (JsonArray*) xiro_beacon->get("XIRO")) == NULL) {
        ciDEBUG(3, ("Message[%s] is <false>", xiro_beacon->toString().c_str()));
        delete json_value;
        return false;
    }
    if (json_array != NULL) {
        if (json_array->getCount() == 3) {
            str_temp1 = json_array->getString(0);
            str_temp2 = json_array->getString(1);
            str_temp3 = json_array->getString(2);

            beacon_scan_msg.xiro_x = atol(str_temp1.c_str());
            beacon_scan_msg.xiro_y = atol(str_temp2.c_str());
            beacon_scan_msg.xiro_z = atol(str_temp3.c_str());
        }
    } 
    
    if ((temp_value = xiro_beacon->get("SCANNERID")) == NULL) {
        ciDEBUG(3, ("Message[%s] is <false>", xiro_beacon->toString().c_str()));
        delete json_value;
        return false;
    }
    beacon_scan_msg.scanner_id = temp_value->getString();

    if ((temp_value = xiro_beacon->get("BEACONID")) == NULL) {
        ciDEBUG(3, ("Message[%s] is <false>", xiro_beacon->toString().c_str()));
        delete json_value;
        return false;
    }
    beacon_scan_msg.beacon_id = temp_value->getString();

    if ((temp_value = xiro_beacon->get("RSSI")) == NULL) {
        ciDEBUG(3, ("Message[%s] is <false>", xiro_beacon->toString().c_str()));
        delete json_value;
        return false;
    }
    str_temp1 = temp_value->getString();
    beacon_scan_msg.rssi = atol(str_temp1.c_str());

    if ((temp_value = xiro_beacon->get("BATTERY")) == NULL) {
        ciDEBUG(3, ("Message[%s] is <false>", xiro_beacon->toString().c_str()));
        delete json_value;
        return false;
    }
    str_temp1 = temp_value->getString();
    beacon_scan_msg.battery = atol(str_temp1.c_str());

    if ((temp_value = xiro_beacon->get("CTIME")) == NULL) {
        ciDEBUG(3, ("Message[%s] is <false>", xiro_beacon->toString().c_str()));
        delete json_value;
        return false;
    }
    beacon_scan_msg.ctime = temp_value->getString();

    return true;
}