/*! \class TcpStreamChannel
 *  Copyright ⓒ 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/10/06 11:01:00
 */

#ifndef PLAYER_BEAPOTAGENT_TCPSTREAMCHANNEL_H
#define PLAYER_BEAPOTAGENT_TCPSTREAMCHANNEL_H

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include "SocketBase.h"
#include "ChannelDefine.h"

//class TcpStreamServer;
//class ServiceAccessWorker;

class TcpStreamChannel  : public ciThread
{
public:
    TcpStreamChannel();
    TcpStreamChannel(TcpStreamServer* p_listener);
    virtual ~TcpStreamChannel(void);

    void        SetServiceAccessPoint(ServiceAccessWorker* p_worker);
    void        StartServer(int sock_fd, const char* sz_channel_id, bool b_nonblockmode, string channel_type);
    void        Restart(int sock_fd);
    int         StartConnect(string str_ip, int port, string channel_type);
    void        Stop();
    void        Close();
    int         SendMessage(const char* p_send_msg, int length);
       
    void        run();	

    const char* GetChannelType();
    const char* GetChannelId();

protected:
    void        SendConnectAck_();
    void        SendSyncResponse_();
    int         RecvStream_();

protected:
    //ciTimer           timer_;      
    bool                started_;    
    ciMutex		        lock_; //ciGuard aGuard(_instanceLock);

    int                 sock_fd_;
    SocketBase          socket_;
    TcpStreamServer*    p_listener_;
    ServiceAccessWorker* p_sap_worker_;

    string              channel_id_;
    string              channel_type_;
    bool                b_nonblockmode_;
    bool                b_sync_comm_;


};

/***
class ServiceAccessWorker : public ciThread
{
public:
    ServiceAccessWorker(void) {};
    virtual ~ServiceAccessWorker(void) {};

    // 향후 recv 파라메터를 BufferBase로 수정
    virtual void RecvFromChannel(string& str_msg) = 0;
    virtual void SendToService(string& str_msg) = 0;
    virtual void RegisterTcpChannel(const char* channel_id, TcpStreamChannel* p_channel) = 0;
};
**/

#endif //PLAYER_BEAPOTAGENT_TCPSTREAMCHANNEL_H