// VNCProxyView.h : CVNCProxyView 클래스의 인터페이스
//


#pragma once


class CVNCProxyView : public CEditView
{
protected: // serialization에서만 만들어집니다.
	CVNCProxyView();
	DECLARE_DYNCREATE(CVNCProxyView)

// 특성입니다.
public:
	CVNCProxyDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CVNCProxyView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

	afx_msg LRESULT OnAppendText(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnClearModifyFlag(WPARAM wParam, LPARAM lParam);
};

#ifndef _DEBUG  // VNCProxyView.cpp의 디버그 버전
inline CVNCProxyDoc* CVNCProxyView::GetDocument() const
   { return reinterpret_cast<CVNCProxyDoc*>(m_pDocument); }
#endif

