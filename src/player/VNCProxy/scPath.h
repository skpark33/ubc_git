#pragma once
#ifndef _scPath_h_
#define _scPath_h_

#include <cstdio>
#include <string>
#include <map>

#include <afxmt.h>

using namespace std;

#	define		_UBC_CD(xxx)		scPath::getInstance()->changeDrive(xxx)


class scGuard {
public:
	scGuard(CCriticalSection* p) : _section(p) { _section->Lock(); }
	~scGuard() { _section->Unlock(); }
protected:
	CCriticalSection* _section;
};


class scPath {
public:

	static scPath*	getInstance();
	static void	clearInstance();

	virtual ~scPath() ;
	const char* changeDrive(const char* orgPath);
	const char* getDrive();

protected:
	scPath();

	static scPath*	_instance;
	static  CCriticalSection	_instanceLock;

	string _drive;
	CCriticalSection	_driveLock;

	map<string,string*>	_pathMap;
	CCriticalSection	_pathMapLock;
};




#endif // _scPath_h_
