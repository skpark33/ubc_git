// BypassSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VNCProxy.h"
#include "BypassSocket.h"

#include "MainFrm.h"


#define		BUFF_SIZE			4096
#define		SLEEP_TIME			(25)
#define		HEALTH_SLEEP_TIME	(1000)	// 1 sec

// CBypassSocket

CBypassSocket::CBypassSocket(int nIndex, SOCKET_TYPE socket_type, int nPort)
:	m_nIndex (nIndex)
,	m_SocketType (socket_type)
,	m_nPort (nPort)
,	m_pLinkSocket (NULL)
,	m_bReceiveFirstPacket (false)
,	m_bConnect (false)
,	m_bProcessThread (true)
,	m_pThread (NULL)
{
	switch(socket_type)
	{
	case SOCKET_TYPE_LOCAL:
		m_pThread = ::AfxBeginThread(SendFunc, (LPVOID)this, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = FALSE;
		m_pThread->ResumeThread();
		break;

	case SOCKET_TYPE_REMOTE:
		m_pThread = ::AfxBeginThread(HealthCheckFunc, (LPVOID)this, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = FALSE;
		m_pThread->ResumeThread();
		break;

	case SOCKET_TYPE_HEALTH_CHECK:
		break;
	}
}

CBypassSocket::~CBypassSocket()
{
	//
	m_mutex.Lock();
	m_bProcessThread = false;
	m_mutex.Unlock();

	DeleteThread();

	//
	m_mutex.Lock();
	int count = m_PacketDataList.GetCount();
	for(int i=0; i<count; i++)
	{
		PACKET_DATA& data = m_PacketDataList.GetAt(i);
		delete[]data.buf;
	}
	m_PacketDataList.RemoveAll();
	m_mutex.Unlock();
}

// CBypassSocket 멤버 함수

void CBypassSocket::OnConnect(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CAsyncSocket::OnConnect(nErrorCode);

	if(nErrorCode == 0)
	{
		m_bConnect = true;

		CString msg;

		CMainFrame* main_frame = (CMainFrame*)::AfxGetMainWnd();
		switch(m_SocketType)
		{
		case SOCKET_TYPE_LOCAL:
			msg.Format("Local Socket Connected = %d:%d", m_nIndex, m_nPort);
			break;
		case SOCKET_TYPE_REMOTE:
			main_frame->PostMessage(WM_CONNECT_SOCKET, m_nIndex);
			msg.Format("Remote Socket Connected = %d:%d", m_nIndex, m_nPort);
			break;
		case SOCKET_TYPE_HEALTH_CHECK:
			msg.Format("Health Socket Connected = %d:%d", m_nIndex, m_nPort);
			break;
		}

		main_frame->AppendLog(msg);

		main_frame->SetTimer(TIMER_CLOSE_CONNECT_SOCKET, TIMER_CLOSE_CONNECT_SOCKET_TIME, NULL);
	}
}

void CBypassSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	m_mutex.Lock();
	m_bProcessThread = false;
	m_mutex.Unlock();

	DeleteThread();

	CString msg;

	CMainFrame* main_frame = (CMainFrame*)::AfxGetMainWnd();
	switch(m_SocketType)
	{
	case SOCKET_TYPE_LOCAL:
		msg.Format("Local Socket Closed(%d) = %d:%d", nErrorCode, m_nIndex, m_nPort);
		break;
	case SOCKET_TYPE_REMOTE:
		msg.Format("Remote Socket Closed(%d) = %d:%d", nErrorCode, m_nIndex, m_nPort);
		break;
	case SOCKET_TYPE_HEALTH_CHECK:
		msg.Format("Health Socket Closed(%d) = %d:%d", nErrorCode, m_nIndex, m_nPort);
		break;
	}

	main_frame->AppendLog(msg);
	main_frame->PostMessage(WM_CLOSE_SOCKET, m_nIndex);

	CAsyncSocket::OnClose(nErrorCode);
}

void CBypassSocket::OnReceive(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CMainFrame* main_frame = (CMainFrame*)::AfxGetMainWnd();

	//
	if( m_SocketType == SOCKET_TYPE_REMOTE && m_pLinkSocket->m_bConnect == false )
	{
		//
		m_mutex.Lock();
		m_bProcessThread = false;
		m_mutex.Unlock();

		DeleteThread();

		m_mutex.Lock();
		int count = m_PacketDataList.GetCount();
		for(int i=0; i<count; i++)
		{
			PACKET_DATA& data = m_PacketDataList.GetAt(i);
			delete[]data.buf;
		}
		m_PacketDataList.RemoveAll();
		m_mutex.Unlock();

		//
		m_bProcessThread = true;

		m_pThread = ::AfxBeginThread(SendFunc, (LPVOID)this, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = FALSE;
		m_pThread->ResumeThread();

		//
		main_frame->AppendLog("Local Socket Connecting", m_pLinkSocket->GetIndex());
		BOOL ret;
		do
		{
			ret = m_pLinkSocket->Connect("127.0.0.1", m_pLinkSocket->GetPort());
			if(ret == FALSE)
			{
				int err = m_pLinkSocket->GetLastError();
				if(WSAEISCONN == err) break;
				main_frame->AppendLog("Local Socket Error", GetSocketErrorCodeString(err));
			}
		}
		while(ret == FALSE);
	}

	//
	char buff[BUFF_SIZE];
	int nRead = Receive(buff, BUFF_SIZE);

	if(m_SocketType == SOCKET_TYPE_HEALTH_CHECK )
	{
		main_frame->SetTimer(TIMER_CLOSE_CONNECT_SOCKET, TIMER_CLOSE_CONNECT_SOCKET_TIME, NULL);
	}
	else if(m_bReceiveFirstPacket == false && m_SocketType == SOCKET_TYPE_REMOTE)
	{
		m_bReceiveFirstPacket = true;

		nRead--;

		if(nRead != 0 && nRead != SOCKET_ERROR)
		{
			char* buf = new char[nRead];
			memcpy(buf, buff+1, nRead);
			SendEx(buf, nRead);
		}
	}
	else
	{
		if(nRead != 0 && nRead != SOCKET_ERROR)
		{
			char* buf = new char[nRead];
			memcpy(buf, buff, nRead);
			SendEx(buf, nRead);
		}
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

void CBypassSocket::OnSend(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CAsyncSocket::OnSend(nErrorCode);
}

bool CBypassSocket::DeleteThread()
{
	if(m_pThread)
	{
		DWORD dwExitCode = 0;

		try
		{
			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);
		}
		catch(...)
		{
			//DWORD dw = GetLastError();
			//CHAR szBuf[80];
			//sprintf(szBuf, "GetLastError Code : %d", dw);
		}

		try
		{
			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, 3000);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}

			delete m_pThread;
		}
		catch(...)
		{
			//DWORD dw = GetLastError();
			//CHAR szBuf[80];
			//sprintf(szBuf, "GetLastError Code : %d", dw);
		}

		m_pThread = NULL;
	}

	return true;
}

void CBypassSocket::SendEx(void* lpBuf, int nBufLen)
{
	m_mutex.Lock();

	if(m_PacketDataList.GetCount() < 16384)
	{
		PACKET_DATA data;
		data.length	= nBufLen;
		data.buf	= lpBuf;

		m_PacketDataList.Add(data);
	}

	m_mutex.Unlock();
}

UINT CBypassSocket::SendFunc(LPVOID pParam)
{
	CBypassSocket* skt = (CBypassSocket*)pParam;

	skt->m_mutex.Lock();

	while(skt->m_bProcessThread)
	{
		int count = skt->m_PacketDataList.GetCount();
		if(count != 0)
		{
			int size = 0;
			for(int i=0; i<count; i++)
			{
				PACKET_DATA& data = skt->m_PacketDataList.GetAt(i);
				size += data.length;
			}

			char* packet = new char[size];
			int pos = 0;
			for(int i=0; i<count; i++)
			{
				PACKET_DATA& data = skt->m_PacketDataList.GetAt(i);
				memcpy(packet + pos, data.buf, data.length);
				pos += data.length;
			}

			if(skt->m_pLinkSocket && skt->m_pLinkSocket->m_hSocket)
			{
				if(skt->m_pLinkSocket->Send(packet, size) != SOCKET_ERROR)
				{
					for(int i=0; i<count; i++)
					{
						PACKET_DATA& data = skt->m_PacketDataList.GetAt(i);
						delete[]data.buf;
					}
					skt->m_PacketDataList.RemoveAll();
				}
			}

			delete[]packet;
		}

		skt->m_mutex.Unlock();

		::Sleep(SLEEP_TIME);

		skt->m_mutex.Lock();
	}

	skt->m_mutex.Unlock();

	return 0;
}

UINT CBypassSocket::HealthCheckFunc(LPVOID pParam)
{
	CBypassSocket* skt = (CBypassSocket*)pParam;

	skt->m_mutex.Lock();

	while(skt->m_bProcessThread)
	{
		skt->Send("PingPingPing", 12);

		skt->m_mutex.Unlock();

		::Sleep(HEALTH_SLEEP_TIME);

		skt->m_mutex.Lock();
	}

	skt->m_mutex.Unlock();

	return 0;
}
