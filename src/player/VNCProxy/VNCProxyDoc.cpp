// VNCProxyDoc.cpp : CVNCProxyDoc 클래스의 구현
//

#include "stdafx.h"
#include "VNCProxy.h"

#include "VNCProxyDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CVNCProxyDoc

IMPLEMENT_DYNCREATE(CVNCProxyDoc, CDocument)

BEGIN_MESSAGE_MAP(CVNCProxyDoc, CDocument)
END_MESSAGE_MAP()


// CVNCProxyDoc 생성/소멸

CVNCProxyDoc::CVNCProxyDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CVNCProxyDoc::~CVNCProxyDoc()
{
}

BOOL CVNCProxyDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	reinterpret_cast<CEditView*>(m_viewList.GetHead())->SetWindowText(NULL);

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CVNCProxyDoc serialization

void CVNCProxyDoc::Serialize(CArchive& ar)
{
	// CEditView에는 모든 serialization을 처리하는 edit 컨트롤이 들어 있습니다.
	reinterpret_cast<CEditView*>(m_viewList.GetHead())->SerializeRaw(ar);
}


// CVNCProxyDoc 진단

#ifdef _DEBUG
void CVNCProxyDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CVNCProxyDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CVNCProxyDoc 명령
