//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VNCProxy.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_VNCProxyTYPE                129
#define IDR_POPUP_MENU                  130
#define IDB_TRAYICON_DISCON             131
#define IDB_TRAYICON_CON                132
#define ID_SHOW_WINDOWS                 32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
