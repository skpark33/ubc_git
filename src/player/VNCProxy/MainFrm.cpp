// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "VNCProxy.h"

#include "MainFrm.h"
#include "BypassSocket.h"
#include "scPath.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



CStringArray CMainFrame::m_listParameter;


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_COMMAND(ID_SHOW_WINDOWS, OnShowWindows)
	ON_MESSAGE(WM_ICON_NOTIFY, OnTrayNotification)
	ON_MESSAGE(WM_CONNECT_SOCKET, OnConnectSocket)
	ON_MESSAGE(WM_CLOSE_SOCKET, OnCloseSocket)
	ON_WM_TIMER()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
{
	// TODO: 여기에 멤버 초기화 코드를 추가합니다.

	m_strServerAddress = _T("");
}

CMainFrame::~CMainFrame()
{
	//
	if(m_hTrayIconDiscon)
		::DestroyIcon(m_hTrayIconDiscon);

	//
	m_mutex.Lock();

	for(int i=0; i<m_SocketLinkList.GetCount(); i++)
	{
		SOCKET_LINK* socket_link = m_SocketLinkList.GetAt(i);
		if(socket_link)
		{
			socket_link->pHealthSocket->Close();
			socket_link->pLocalVNCSocket->Close();
			socket_link->pRemoetVNCSocket->Close();
			socket_link->pLocalSubVNCSocket->Close();
			socket_link->pRemoetSubVNCSocket->Close();

			delete socket_link->pHealthSocket;
			delete socket_link->pLocalVNCSocket;
			delete socket_link->pRemoetVNCSocket;
			delete socket_link->pLocalSubVNCSocket;
			delete socket_link->pRemoetSubVNCSocket;

			delete socket_link;
		}
	}

	m_mutex.Unlock();
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	//if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
	//	| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
	//	!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	//{
	//	TRACE0("도구 모음을 만들지 못했습니다.\n");
	//	return -1;      // 만들지 못했습니다.
	//}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("상태 표시줄을 만들지 못했습니다.\n");
		return -1;      // 만들지 못했습니다.
	}

	// TODO: 도구 모음을 도킹할 수 없게 하려면 이 세 줄을 삭제하십시오.
	//m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	//EnableDocking(CBRS_ALIGN_ANY);
	//DockControlBar(&m_wndToolBar);

	//
	GetVNCParameter();

	//
	CString title;
	title.LoadString(AFX_IDS_APP_TITLE);
	title.Append("\r\nConnect to ");
	title.Append(m_strServerAddress);
	title.Append(":");
	title.Append(::ToString(m_nServerPort));
	CreateTrayIcon(this, title, IDR_POPUP_MENU, IDB_TRAYICON_DISCON);

	title.Replace("\r\n", " - ");
	SetWindowText(title);

	//
	SetTimer(TIMER_CHECK_CONNECT_SOCKET, TIMER_CHECK_CONNECT_SOCKET_TIME, NULL);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	//cs.x = 0;
	//cs.y = 0;

	//cs.cx = 480;
	//cs.cy = 640;

	cs.x = -100;
	cs.y = -100;
	cs.cx = 0;
	cs.cy = 0;

	cs.style &= (~FWS_ADDTOTITLE);

	return TRUE;
}


BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(pMsg->message == WM_COMMAND && pMsg->wParam == ID_APP_EXIT)
	{
		GetActiveView()->SendMessage(WM_CLEAR_MODIFY_FLAG);
	}

	return CFrameWnd::PreTranslateMessage(pMsg);
}

void CMainFrame::OnShowWindows()
{
	CRect rect;
	MoveWindow(0, 0, 640, 480);
	ShowWindow(SW_SHOWNORMAL);
	::SetForegroundWindow(m_hWnd);
}

void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(nID == SC_CLOSE)
	{
		ShowWindow(SW_HIDE);
		return;
	}

	CFrameWnd::OnSysCommand(nID, lParam);
}

void CMainFrame::OnDestroy()
{
	CFrameWnd::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame 메시지 처리기


void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(nType == SIZE_MINIMIZED)
	{
		ShowWindow(SW_HIDE);
		return;
	}
}

BOOL CMainFrame::CreateTrayIcon(CWnd* pWnd, LPCTSTR lpszTitle, UINT nMenuID, UINT nBmpIcon)
{
	if(CWnd::FindWindow("Shell_TrayWnd", "") == NULL)
	{
		return FALSE;
	}//if

	// Make Tray Icon
	// 투명배경색은 무조건 White(255,255,255)
	if(m_BMP24bitDiscon.LoadBitmap(nBmpIcon))
	{
		ICONINFO icInfo;
		icInfo.fIcon = TRUE;
		icInfo.hbmMask = (HBITMAP) m_BMP24bitDiscon;
		icInfo.xHotspot = 0;
		icInfo.yHotspot = 0;
		icInfo.hbmColor = (HBITMAP) m_BMP24bitDiscon;
		m_hTrayIconDiscon = CreateIconIndirect(&icInfo);
	}

	if(m_BMP24bitCon.LoadBitmap(nBmpIcon+1))
	{
		ICONINFO icInfo;
		icInfo.fIcon = TRUE;
		icInfo.hbmMask = (HBITMAP) m_BMP24bitCon;
		icInfo.xHotspot = 0;
		icInfo.yHotspot = 0;
		icInfo.hbmColor = (HBITMAP) m_BMP24bitCon;
		m_hTrayIconCon = CreateIconIndirect(&icInfo);
	}

	return m_TrayIcon.Create(pWnd, WM_ICON_NOTIFY, lpszTitle, m_hTrayIconDiscon, nMenuID);
}

LRESULT CMainFrame::OnTrayNotification(WPARAM wParam, LPARAM lParam)
{
	if(m_TrayIcon.Enabled())
        return m_TrayIcon.OnTrayNotification(wParam, lParam);

	return 0;
}

LRESULT CMainFrame::OnConnectSocket(WPARAM wParam, LPARAM lParam)
{
	m_mutex.Lock();

	int index = wParam;
	if(index == m_SocketLinkList.GetCount()-1)
		m_TrayIcon.SetIcon(m_hTrayIconCon);

	m_mutex.Unlock();

	return 0;
}

LRESULT CMainFrame::OnCloseSocket(WPARAM wParam, LPARAM lParam)
{
	m_mutex.Lock();

	//
	int index = wParam;
	if(m_SocketLinkList.GetCount() > 0)
	{
		SOCKET_LINK* socket_link = m_SocketLinkList.GetAt(index);
		if(socket_link)
		{
			AppendLog("Delete Socket", index);

			socket_link->pHealthSocket->Close();
			socket_link->pLocalVNCSocket->Close();
			socket_link->pRemoetVNCSocket->Close();
			socket_link->pLocalSubVNCSocket->Close();
			socket_link->pRemoetSubVNCSocket->Close();

			delete socket_link->pHealthSocket;
			delete socket_link->pLocalVNCSocket;
			delete socket_link->pRemoetVNCSocket;
			delete socket_link->pLocalSubVNCSocket;
			delete socket_link->pRemoetSubVNCSocket;

			delete socket_link;

			m_SocketLinkList.SetAt(index, NULL);
		}
	}

	//
	if(index == m_SocketLinkList.GetCount()-1)
		m_TrayIcon.SetIcon(m_hTrayIconDiscon);

	m_mutex.Unlock();

	return 0;
}

void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CFrameWnd::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case TIMER_CHECK_CONNECT_SOCKET:
		m_mutex.Lock();
		{
			int index = m_SocketLinkList.GetCount();

			SOCKET_LINK* socket_link = NULL;

			if(m_SocketLinkList.GetCount() > 0)
			{
				socket_link = m_SocketLinkList.GetAt(m_SocketLinkList.GetCount()-1);
			}

			if(socket_link == NULL)
			{
				AppendLog("Create New Socket", index);

				socket_link = new SOCKET_LINK;

				socket_link->pHealthSocket    = new CBypassSocket(index, SOCKET_TYPE_HEALTH_CHECK, m_nServerPort + LOCAL_HEALTH_PORT_GAP);
				socket_link->pLocalVNCSocket  = new CBypassSocket(index, SOCKET_TYPE_LOCAL,        m_nServerPort /*VNC_PORT*/);
				socket_link->pRemoetVNCSocket = new CBypassSocket(index, SOCKET_TYPE_REMOTE,       m_nServerPort + LOCAL_REMOTE_VNC_PORT_GAP);
				socket_link->pLocalSubVNCSocket  = new CBypassSocket(index, SOCKET_TYPE_LOCAL,        SUB_VNC_PORT/*m_nServerPort + LOCAL_LOCAL_SUB_VNC_PORT_GAP*/);
				socket_link->pRemoetSubVNCSocket = new CBypassSocket(index, SOCKET_TYPE_REMOTE,       m_nServerPort + LOCAL_REMOTE_SUB_VNC_PORT_GAP);

				socket_link->pHealthSocket->Create();
				socket_link->pLocalVNCSocket->Create();
				socket_link->pRemoetVNCSocket->Create();
				socket_link->pLocalSubVNCSocket->Create();
				socket_link->pRemoetSubVNCSocket->Create();

				socket_link->pLocalVNCSocket->m_pLinkSocket = socket_link->pRemoetVNCSocket;
				socket_link->pRemoetVNCSocket->m_pLinkSocket = socket_link->pLocalVNCSocket;

				socket_link->pLocalSubVNCSocket->m_pLinkSocket = socket_link->pRemoetSubVNCSocket;
				socket_link->pRemoetSubVNCSocket->m_pLinkSocket = socket_link->pLocalSubVNCSocket;

				m_SocketLinkList.Add(socket_link);
			}

			if(socket_link->pHealthSocket->m_bConnect == false)
			{
				AppendLog("Health Socket Connecting", socket_link->pHealthSocket->GetIndex());
				socket_link->pHealthSocket->Connect(m_strServerAddress, socket_link->pHealthSocket->GetPort());
				int err = socket_link->pHealthSocket->GetLastError();
				if(WSAEISCONN != err)
					AppendLog("Health Socket Error", GetSocketErrorCodeString(err));
				else
					SetTimer(TIMER_CLOSE_CONNECT_SOCKET, TIMER_CLOSE_CONNECT_SOCKET_TIME, NULL);
			}
			if(socket_link->pRemoetVNCSocket->m_bConnect == false)
			{
				AppendLog("Remote Socket Connecting", socket_link->pRemoetVNCSocket->GetIndex());
				socket_link->pRemoetVNCSocket->Connect(m_strServerAddress, socket_link->pRemoetVNCSocket->GetPort());
				int err = socket_link->pRemoetVNCSocket->GetLastError();
				if(WSAEISCONN != err)
					AppendLog("Remote Socket Error", GetSocketErrorCodeString(err));
				else
					SetTimer(TIMER_CLOSE_CONNECT_SOCKET, TIMER_CLOSE_CONNECT_SOCKET_TIME, NULL);
			}
			if(socket_link->pRemoetSubVNCSocket->m_bConnect == false)
			{
				AppendLog("Remote Socket Connecting", socket_link->pRemoetSubVNCSocket->GetIndex());
				socket_link->pRemoetSubVNCSocket->Connect(m_strServerAddress, socket_link->pRemoetSubVNCSocket->GetPort());
				int err = socket_link->pRemoetSubVNCSocket->GetLastError();
				if(WSAEISCONN != err)
					AppendLog("Remote Socket Error", GetSocketErrorCodeString(err));
				else
					SetTimer(TIMER_CLOSE_CONNECT_SOCKET, TIMER_CLOSE_CONNECT_SOCKET_TIME, NULL);
			}
		}
		m_mutex.Unlock();
		break;

	case TIMER_CLOSE_CONNECT_SOCKET:
		{
			KillTimer(TIMER_CLOSE_CONNECT_SOCKET);
			PostMessage(WM_CLOSE_SOCKET, m_SocketLinkList.GetCount()-1);
		}
		break;

	case TIMER_RECONNECT_SOCKET:
		//KillTimer(TIMER_RECONNECT_SOCKET);
		//PostMessage(WM_CLOSE_SOCKET, m_SocketLinkList[list_index].GetCount()-1);
		break;
	}
}

void CMainFrame::AppendLog(LPCSTR lpszLog)
{
	CView* view = GetActiveView();

	TCHAR timebuf[64], datebuf[64];
	_tstrtime( timebuf );
	_tstrdate( datebuf );

	CString msg;
	msg.Format("[%s %s] %s\r\n", datebuf, timebuf, lpszLog);

	view->SendMessage(WM_APPEND_TEXT, (WPARAM)(LPCSTR)msg);
}

void CMainFrame::AppendLog(LPCSTR lpszLog, LPCSTR lpszValue)
{
	CView* view = GetActiveView();

	TCHAR timebuf[64], datebuf[64];
	_tstrtime( timebuf );
	_tstrdate( datebuf );

	CString msg;
	msg.Format("[%s %s] %s = %s\r\n", datebuf, timebuf, lpszLog, lpszValue);

	view->SendMessage(WM_APPEND_TEXT, (WPARAM)(LPCSTR)msg);
}

void CMainFrame::AppendLog(LPCSTR lpszLog, int nValue)
{
	CView* view = GetActiveView();

	TCHAR timebuf[64], datebuf[64];
	_tstrtime( timebuf );
	_tstrdate( datebuf );

	CString msg;
	msg.Format("[%s %s] %s = %d\r\n", datebuf, timebuf, lpszLog, nValue);

	view->SendMessage(WM_APPEND_TEXT, (WPARAM)(LPCSTR)msg);
}

bool CMainFrame::GetSiteId()
{
	const char* filePath = _UBC_CD("C:\\SQISoft\\UTV1.0\\execute\\data\\UBCVariables.ini");
	char	szValue[16];
	memset(szValue, 0x00, sizeof(szValue));
	GetPrivateProfileStringA("ROOT", "SiteId", "", szValue, sizeof(szValue), filePath);

	m_siteId = szValue;
	if(strlen(szValue)==0) {
		return false;
	}
	return true;
}

void CMainFrame::GetVNCParameter()
{
	//
	bool find = false;
	for(int i=0; i<m_listParameter.GetCount(); i++)
	{
		if(m_listParameter.GetAt(i) == "+ip")
		{
			find = true;
			m_strMainServerAddress = m_listParameter.GetAt(i+1);
			break;
		}
	}
	if(find == false)
	{
		m_strMainServerAddress = "";
		const char* filePath = _UBC_CD("C:\\SQISoft\\UTV1.0\\execute\\data\\UBCConnect.ini");
		char	szValue[32];
		if(GetSiteId()){
			memset(szValue, 0x00, sizeof(szValue));
			GetPrivateProfileString("VNCMANAGER", m_siteId, "", szValue, sizeof(szValue), filePath);
			m_strMainServerAddress = szValue;
		}

		if(m_strMainServerAddress.IsEmpty()){
			memset(szValue, 0x00, sizeof(szValue));
			GetPrivateProfileString("VNCMANAGER", "IP", "211.232.57.202", szValue, sizeof(szValue), filePath);
			m_strMainServerAddress = szValue;
		}
	}

	// <-- 포워딩된 ip주소 검색 (2010.08.30)
	CString url;
	url.Format(_T("http://%s:8080/ubc_get_vnc_addr.asp?hostId=%s"), m_strMainServerAddress, GetHostName());

	CInternetSession is;
	is.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, 10000);	// timeout : 10sec
	is.SetOption(INTERNET_OPTION_CONTROL_RECEIVE_TIMEOUT, 10000 );
	is.SetOption(INTERNET_OPTION_CONTROL_SEND_TIMEOUT, 10000 );
	is.SetOption(INTERNET_OPTION_DATA_SEND_TIMEOUT, 10000 );
	is.SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT, 10000 );

	try 
	{
		CHttpFile* file = (CHttpFile*)is.OpenURL(url, 1, INTERNET_FLAG_TRANSFER_BINARY | INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE);
		if(file)
		{
			char ipaddr[1024] = {0};

			int idx = 0;
			char buf[32] = {0};
			int read_size = file->Read(buf, 32);
			while(read_size != 0 && idx+read_size < 1023)
			{
				memcpy(ipaddr+idx, buf, read_size);
				idx += read_size;
				::ZeroMemory(buf, sizeof(buf));
				read_size = file->Read(buf, 32);
			}

			CString strIpaddr = ipaddr;
			strIpaddr.TrimLeft(_T(" "));
			strIpaddr.TrimRight(_T(" "));
			bool is_valid = true;
			int dot_count = 0;
			for(int i=0; i<strIpaddr.GetLength(); i++)
			{
				TCHAR tch = strIpaddr.GetAt(i);
				if( tch == _T('.'))
				{
					dot_count++;
					continue;
				}
				if( tch < _T('0') || tch > _T('9') )
				{
					is_valid = false;
					break;
				}
			}
			if(is_valid && dot_count == 3 && strIpaddr.GetLength() < 16)
			{
				m_strForwardServerAddress = strIpaddr;
				m_strServerAddress = m_strForwardServerAddress;
			}
		}
	}
	catch(CInternetException* ex)
	{
		ex->Delete();
	}

	if(m_strServerAddress.GetLength() == 0)
		m_strServerAddress = m_strMainServerAddress;
	//-->

	//
	find = false;
	for(int i=0; i<m_listParameter.GetCount(); i++)
	{
		if(m_listParameter.GetAt(i) == "+port")
		{
			find = true;
			m_nServerPort = atoi(m_listParameter.GetAt(i+1));
			break;
		}
	}
	if(find == false)
	{
		int port = 5899;

		CString host_name = GetHostName();

		if(host_name.GetLength() >= 5)
		{
			CString port_str = host_name.Right(5);
			port = atoi(port_str);
		}

		m_nServerPort = port;
	}
}

LPCTSTR	CMainFrame::GetHostName()
{
	static CString strHostName = "";

	if(strHostName.GetLength() == 0)
	{
		bool find = false;
		for(int i=0; i<m_listParameter.GetCount(); i++)
		{
			if(m_listParameter.GetAt(i) == "+host")
			{
				find = true;
				strHostName = m_listParameter.GetAt(i+1);
				break;
			}
		}
		if(find == false)
		{
			char    szBuffer[256];
			DWORD   dwNameSize = 256 + 1;
			GetComputerNameA(szBuffer, &dwNameSize);
			strHostName = szBuffer; 
		}
	}

	return strHostName;
}
