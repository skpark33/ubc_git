// MainFrm.h : CMainFrame 클래스의 인터페이스
//


#pragma once

#include "TrayIcon.h"
#include "BypassSocket.h"

class CMainFrame : public CFrameWnd
{
	
protected: // serialization에서만 만들어집니다.
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 특성입니다.
public:

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

// 구현입니다.
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 컨트롤 모음이 포함된 멤버입니다.
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;

// 생성된 메시지 맵 함수
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindows();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg LRESULT OnTrayNotification(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnConnectSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseSocket(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

	CTrayIcon	m_TrayIcon;		// 트레이
	HICON		m_hTrayIconDiscon;	// 트레이 아이콘 (ICON)
	CBitmap		m_BMP24bitDiscon;		// 트레이 아이콘 (BMP)
	HICON		m_hTrayIconCon;	// 트레이 아이콘 (ICON)
	CBitmap		m_BMP24bitCon;		// 트레이 아이콘 (BMP)

	BOOL	CreateTrayIcon(CWnd* pWnd, LPCTSTR lpszTitle, UINT nMenuID, UINT nBmpIcon);
	void	GetVNCParameter();
	LPCTSTR	GetHostName();
	bool	GetSiteId();

	CMutex		m_mutex;
	SOCKET_LINK_LIST	m_SocketLinkList;

public:

	static CStringArray	m_listParameter;

	CString		m_strMainServerAddress;
	CString		m_strForwardServerAddress;

	CString		m_strServerAddress;
	int			m_nServerPort;
	CString		m_siteId;

	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void	AppendLog(LPCSTR lpszLog);
	void	AppendLog(LPCSTR lpszLog, LPCSTR lpszValue);
	void	AppendLog(LPCSTR lpszLog, int nValue);
};


