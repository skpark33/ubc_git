// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// VNCProxy.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

CString	ToString(int nValue)
{
	CString str;
	str.Format("%d", nValue);

	return str;
}

CString GetSocketErrorCodeString(int nErrorCode)
{
	CString msg;

	switch( nErrorCode )
	{
	case WSAEINVAL:
		msg.Format("WSAEINVAL (%d)", nErrorCode);
		break;
	case WSAEWOULDBLOCK:
		msg.Format("WSAEWOULDBLOCK (%d)", nErrorCode);
		break;
	default:
		msg.Format("Unknown (%d)", nErrorCode);
		break;
	}

	return msg;
}