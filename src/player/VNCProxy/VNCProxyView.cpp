// VNCProxyView.cpp : CVNCProxyView 클래스의 구현
//

#include "stdafx.h"
#include "VNCProxy.h"

#include "VNCProxyDoc.h"
#include "VNCProxyView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CVNCProxyView

IMPLEMENT_DYNCREATE(CVNCProxyView, CEditView)

BEGIN_MESSAGE_MAP(CVNCProxyView, CEditView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CEditView::OnFilePrintPreview)

	ON_MESSAGE(WM_APPEND_TEXT, OnAppendText)
	ON_MESSAGE(WM_CLEAR_MODIFY_FLAG, OnClearModifyFlag)
END_MESSAGE_MAP()

// CVNCProxyView 생성/소멸

CVNCProxyView::CVNCProxyView()
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CVNCProxyView::~CVNCProxyView()
{
}

BOOL CVNCProxyView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	BOOL bPreCreated = CEditView::PreCreateWindow(cs);
	cs.style &= ~(ES_AUTOHSCROLL|WS_HSCROLL);	// 자동 줄바꿈을 사용합니다.

	return bPreCreated;
}


// CVNCProxyView 인쇄

BOOL CVNCProxyView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 CEditView 준비
	return CEditView::OnPreparePrinting(pInfo);
}

void CVNCProxyView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// 기본 CEditView 시작 인쇄
	CEditView::OnBeginPrinting(pDC, pInfo);
}

void CVNCProxyView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// 기본 CEditView 종료 인쇄
	CEditView::OnEndPrinting(pDC, pInfo);
}


// CVNCProxyView 진단

#ifdef _DEBUG
void CVNCProxyView::AssertValid() const
{
	CEditView::AssertValid();
}

void CVNCProxyView::Dump(CDumpContext& dc) const
{
	CEditView::Dump(dc);
}

CVNCProxyDoc* CVNCProxyView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CVNCProxyDoc)));
	return (CVNCProxyDoc*)m_pDocument;
}
#endif //_DEBUG


// CVNCProxyView 메시지 처리기

LRESULT CVNCProxyView::OnAppendText(WPARAM wParam, LPARAM lParam)
{
	LPCSTR text = (LPCSTR)wParam;

	SetRedraw(FALSE);

	CEdit& edit = GetEditCtrl();

	if(edit.GetSafeHwnd())
	{
//		int start, end;
//		edit.GetSel(start, end);
		edit.SetSel(-1, -1);

		edit.ReplaceSel(text);
		//edit.ReplaceSel(_T("\r\n"));

//		edit.SetSel(start, end);

		GetDocument()->SetModifiedFlag(FALSE);
	}

	SetRedraw(TRUE);

	return 0;
}

LRESULT CVNCProxyView::OnClearModifyFlag(WPARAM wParam, LPARAM lParam)
{
	GetDocument()->SetModifiedFlag(FALSE);
	return 0;
}
