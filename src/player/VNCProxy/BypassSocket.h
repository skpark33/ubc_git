#pragma once


// CBypassSocket 명령 대상입니다.

//
typedef struct
{
	int 	length;
	void*	buf;
} PACKET_DATA;

typedef CArray<PACKET_DATA, PACKET_DATA&> PACKET_DATA_LIST;

//
enum SOCKET_TYPE {
	SOCKET_TYPE_LOCAL = 0,
	SOCKET_TYPE_REMOTE,
	SOCKET_TYPE_HEALTH_CHECK,
};


//////////////////////////////////////


class CBypassSocket : public CAsyncSocket
{
public:
	CBypassSocket(int nIndex, SOCKET_TYPE socket_type, int nPort);
	virtual ~CBypassSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnSend(int nErrorCode);

	int		GetIndex() { return m_nIndex; };
	int		GetPort() { return m_nPort; };
	void	SendEx(void* lpBuf, int nBufLen);

	bool			m_bConnect;
	CBypassSocket*	m_pLinkSocket;

protected:
	bool			m_bReceiveFirstPacket;

	int				m_nListIndex;
	int				m_nIndex;
	SOCKET_TYPE		m_SocketType;

	int				m_nPort;

	// for Thread-Sending
	bool		m_bProcessThread;
	CWinThread*	m_pThread;
	bool		DeleteThread();

	CMutex		m_mutex;
	PACKET_DATA_LIST	m_PacketDataList;

	static UINT	SendFunc(LPVOID pParam);
	static UINT HealthCheckFunc(LPVOID pParam);
};


typedef struct {
	CBypassSocket*	pHealthSocket;

	CBypassSocket*	pLocalVNCSocket;
	CBypassSocket*	pRemoetVNCSocket;

	CBypassSocket*	pLocalSubVNCSocket;
	CBypassSocket*	pRemoetSubVNCSocket;
} SOCKET_LINK;

typedef		CArray<SOCKET_LINK*, SOCKET_LINK*>		SOCKET_LINK_LIST;

