// HookDll.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "stdlib.h"

//---------------------------------------------------
// Global Variables
// 공용 메모리
//---------------------------------------------------
#pragma data_seg(".HKT")
HINSTANCE	g_Module = NULL;		// DLL Handle 
HHOOK		g_Hook = NULL;			// Hook Handle
HWND		g_HwndServer = NULL;	// Hook Server Window Handle
#pragma data_seg()

POINT		g_ptOld;				// Old cursor point

BOOL	RemoveHook();				// Stop cursor hook
BOOL	SetHook(HWND hWnd);			// Start cursor hook


//------------------------------------------------------------------
// DllMain : Entry point
//------------------------------------------------------------------
BOOL APIENTRY DllMain( 
						HANDLE hModule, 
						DWORD  ul_reason_for_call, 
						LPVOID lpReserved
					)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		g_Module = (HINSTANCE)hModule; // Save Dll Handle
		break;
		
	case DLL_PROCESS_DETACH:
		RemoveHook();
		break;
    }
	
    return TRUE;
}

//--------------------------------------------------------------
// Hook Procedure - Keyboard
//--------------------------------------------------------------
LRESULT CALLBACK KeyboardProcedure(int nCode, WPARAM wParam, LPARAM lParam)
{
	if(nCode >= 0)
	{
		//Mouse move 메시지만...
		if(wParam == WM_MOUSEMOVE)
		{
			MOUSEHOOKSTRUCT* pstMouse  = (MOUSEHOOKSTRUCT*)lParam;
			if(g_ptOld.x == 0 && g_ptOld.y == 0)	//최초
			{
				g_ptOld.x = pstMouse->pt.x;
				g_ptOld.y = pstMouse->pt.y;
				return ::CallNextHookEx(g_Hook , nCode , wParam , lParam);
			}//if

			//X, Y 각 방향으로 30 포인트 이상을 움직였을때만...
			if(abs(g_ptOld.x - pstMouse->pt.x) > 30 || abs(g_ptOld.y - pstMouse->pt.y) > 30)
			{
				g_ptOld.x = pstMouse->pt.x;
				g_ptOld.y = pstMouse->pt.y;

				COPYDATASTRUCT  CDS;
				// Set CDS
				CDS.dwData = 9005;	//Hook
				CDS.cbData = sizeof(MOUSEHOOKSTRUCT);
				CDS.lpData = (PVOID)lParam;
				
				::SendMessage(g_HwndServer , WM_COPYDATA , 0 , (LPARAM)&CDS);
				return ::CallNextHookEx(g_Hook , nCode , wParam , lParam);
			}//if	
		}//if
	}//if

	// We must pass the all messages on to CallNextHookEx.
	return ::CallNextHookEx(g_Hook , nCode , wParam , lParam);
}

//------------------------------------------------------------------
// Set Hook
//------------------------------------------------------------------
BOOL SetHook(HWND hWnd) 
{
	g_HwndServer = hWnd ;		// Set Hook Server
	g_ptOld.x = 0;
	g_ptOld.y = 0;

	g_Hook = SetWindowsHookEx(WH_MOUSE , KeyboardProcedure , (HINSTANCE)g_Module , 0);
	if(g_Hook)
	{
		return true;
	}//if
	
	return false ;
}

//------------------------------------------------------------------
// Remove Hook
//------------------------------------------------------------------
BOOL RemoveHook() 
{
	if(UnhookWindowsHookEx(g_Hook))
	{
		return true ;
	}//if

	return false;
}
