#ifndef _AGTIOTInteractiveLogThread_h_
#define _AGTIOTInteractiveLogThread_h_

#include "AGTProcessInteractiveLogThread.h"

////////////////////
// AGTIOTInteractiveLogThread

class AGTIOTInteractiveLogThread : public virtual AGTProcessInteractiveLogThread {
public:
	AGTIOTInteractiveLogThread(int port=8000);
	virtual ~AGTIOTInteractiveLogThread();

	ciBoolean ExecuteAllInteractiveLog();
	ciBoolean ExecuteInteractiveLog(const char* fullpath, const char* filename);

	void run();
protected:
	int _port;
	ciShort _dayLimit;
};

#endif // _AGTIOTInteractiveLogThread_h_
