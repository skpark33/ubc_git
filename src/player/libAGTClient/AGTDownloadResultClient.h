#ifndef _AGTDownloadStateHandler_h_
#define _AGTDownloadStateHandler_h_

#include "ci/libBase/ciBaseType.h"
#include "ci/libBase/ciListType.h"
#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include <ci/libThread/ciCondition.h>
#include "common/libAGTData/AGTData.h"
#include "common/libAGTData/AGTDownloadResult.h"



////////////////////
// AGTDownloadResultClient

typedef map<ciString, int> AGTStateMap;		// 프로그래명, download 상태 맵

class AGTDownloadResultClient  {
public:
	static AGTDownloadResultClient*	getInstance();
	static void	clearInstance();

	virtual ~AGTDownloadResultClient();
	
	void				clear();
	ciBoolean			processMsg(DownloadProgressMsg* pMsg);
	ciBoolean			readIni(DownloadProgressMsg* pMsg, AGTDownloadResult* pResult);
	void				push(AGTDownloadResult* ele);
	ciBoolean			hasMore();
	AGTDownloadResult*	pop();
	
	void			updateDownloadResult(const char* programId, int state);
	int				getDownloadResult(const char* programId);

protected:
	AGTDownloadResultClient();

	static AGTDownloadResultClient*	_instance;
	static ciMutex _instanceLock;

	ciMutex					_msgLock;
	ciMutex					_listLock;
	AGTDownloadResultList	_list;

	ciMutex					_mapLock;
	AGTStateMap				_stateMap;  // 상태만 보관하기 위한 map 

};

#endif // _AGTDownloadStateHandler_h_
