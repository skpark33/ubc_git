#ifndef _AGTProcessPlayLog_H_
#define _AGTProcessPlayLog_H_

#include "ci/libBase/ciListType.h"

class RawPlayLog {
protected:
	RawPlayLog() : tryTime(0),startTime(0),closeTime(0),endTime(0) {}

	ciString scheduleId;	// ������ ���̵�
	ciString proID;			// ���α׷� ���̵�
	ciString contID;		// ������ ���̵�
	ciString contName;		// ������ �̸�
	ciString contFileName;	// ������ ���ϸ�
	ciUInt planTime;		// �� ��� ���� �ð�
	time_t	tryTime;		// TRY
	time_t  startTime;		// START
	time_t  closeTime;		// SUCCED, FAIL
	time_t  endTime;		// END
};
typedef map<ciString,RawPlayLog>  RawPlayLogList;


typedef struct _PlayLog {
	ciString scheduleId;	// ������ ���̵�
	ciString proID;			// ���α׷� ���̵�
	ciString contID;		// ������ ���̵�
	ciString contName;		// ������ �̸�
	ciString contFileName;	// ������ ���ϸ�
	ciUInt playCount;		// ��� Ƚ��(����)
	ciUInt playingTime;		// �� ��� �ð�
	ciUInt planTime;		// �� ��� ���� �ð�
	ciUInt failCount;		// ��� ���� Ƚ��
	ciUInt failTime;		// �� ��� ���� �ð�(��� ���� Ƚ�� X ��� �ð�)
	ciString date;			// �����
	ciString action;		// ��� TRY/START/SUCCEED|FAIL/END
	ciString note;			// ���
	ciULong lastStartTime;	// �ֱ� ���� �ð� 
} PlayLog;

typedef map<ciString, PlayLog*> PlayLogMap;

class AGTProcessPlayLog {
public:
	AGTProcessPlayLog(const char* siteId, const char* hostId);
	virtual ~AGTProcessPlayLog();

	ciBoolean Execute(const char* iFileName, ciString& oFileName);
	ciBoolean _ExecutePlayLog(const char* strBuffer, const char* strDate);
	void ANSI2UTF8(const char *in, char *out, int nOut);
	void toUTF8(ciString& xmlBuf);

protected:
	ciBoolean _Execute(const char* iFileName, ciString& oFileName);
	ciBoolean _Execute_MultiLang(const char* iFileName, ciString& oFileName);

	PlayLogMap _playLogTable;
	ciString _siteId;
	ciString _hostId;
};

#endif //_AGTProcessPlayLog_H_
