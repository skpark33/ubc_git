/*! \class ProjectorStateThread
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _ProjectorStateThread_h_
#define _ProjectorStateThread_h_

#include <ci/libThread/ciThread.h> 

class ProjectorStateThread :  public  ciThread {
public:

	static ProjectorStateThread*	getInstance();
	static void	clearInstance();
	virtual ~ProjectorStateThread() ;
	virtual void run();

	ciBoolean isRun();

protected:
	ProjectorStateThread();

	static ProjectorStateThread*	_instance;
	static ciMutex		_instanceLock;

	ciMutex _lock;
	ciBoolean _isRun;

};	

#endif //_ProjectorStateThread_h_
