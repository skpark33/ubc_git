#ifndef _AGTHost_h_
#define _AGTHost_h_

#include <ci/libThread/ciSyncUtil.h>
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>

#if defined (_COP_MSC_)
    #include <tlhelp32.h> // Process Control
    #include <windows.h>
#endif

#include "libAudioControl/UBCAudioControl.h"

class AGTHost {
public:	
	virtual ~AGTHost();

	static AGTHost* getInstance();
	static void clearInstance();

	ciBoolean init();
	ciBoolean fini();

	ciBoolean shutdown( 
		ciShort waitingTime = 0,			// 종료때까지의 남은 시간
		const char* arg	= ""				// 추가 파라메터
		);		

	ciBoolean reboot( 
		ciShort waitingTime = 0,			// 재부팅까지의 남은 시간
		const char* arg = ""				// 추가 파라메터
		);

	ciBoolean processkilldown(
		const char* binaryName,				// 종료시킬 프로세스
		const char* arg						// 추가 파라메터
		);

	ciBoolean stopService();
	ciBoolean startService();

	// 프로세스 관련 함수	
	static ciUInt createProcess(const char* command, ciBoolean minFlag = ciFalse);
	static ciBoolean killProcess(const char* binaryName);
	static ciUInt checkProcess(const char* binaryName);
	static BOOL SafeTerminateProcess(HANDLE hProcess, UINT uExitCode);
	
	ciULong volume();
	ciBoolean mute();
	ciBoolean volume(ciULong volume);	// Volume 변경
	ciBoolean mute(ciBoolean mute); // Mute 세팅

protected:
	static ciBoolean _killProcess(const char* binaryName, ciUInt pid = 0);

	//UBCAudioControl _audioControl;

	AGTHost();
	static AGTHost* _host;
	static ciMutex _sLock;
};

#endif // _AGTHost_h_
