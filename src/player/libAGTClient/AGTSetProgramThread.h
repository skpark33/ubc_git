#ifndef _AGTSetProgramThread_h_
#define _AGTSetProgramThread_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>

////////////////////
// AGTSetProgramThread

class AGTSetProgramThread : public virtual ciThread {
public:
	AGTSetProgramThread(const char* lastSchedule, ciULong lastScheduleTime,
						ciBoolean reload, const char* display, int pperiod);
	virtual ~AGTSetProgramThread();

	void run();
	void destroy();
	ciBoolean	isAlive() { return _isAlive; }

protected:
	ciBoolean	_importProgram(time_t now);
	bool		_stop3rdPartyPlayer();

	ciString	_lastSchedule;
	ciULong	_lastScheduleTime;
	ciString	_display;
	ciBoolean	_reload;
	ciBoolean	_isAlive;
	int			_period;
};
typedef list<AGTSetProgramThread*>  AGTSetProgramThreadList;

class AGTSetProgramThreadManager {
public:
	static void		clearAll();
	static void		clearAllForce();
	static void		clear();
	static void		push(AGTSetProgramThread* pThread);
protected:
	static AGTSetProgramThreadList	_list;
	static ciMutex					_listLock;

};


#endif // _AGTSetProgramThread_h_
