#ifndef _HOST_INFO_H_
#define _HOST_INFO_H_

#include <ci/libBase/ciBaseType.h>
#include <list>

//----------------------------------------------------------------------------
//  struct       hostMonitor
//----------------------------------------------------------------------------

typedef struct
{
    char    hostName[20];
    int     hostStatus;     // 0:"DirectorFail", 1:RESOURCE_NORMAL, 2:"Warning"
    int     cpuThrChk;
    int     memThrChk;
    float   userCpu;
    float   niceCpu;
    float   sysCpu;
    float   idleCpu;
    long    t_rmem;
    long    rmem;
    long    t_vmem;
    long    vmem;
    long    t_numApp;
    long    runApp;
    long    waitApp;
    long    sleepApp;
    long    zombieApp;
    long    idleApp;
    long    swap; 		// 보류
} host_info;

typedef struct
{
    ciString name;
    ciBoolean monitored;
    ciString remarks;
} fileSystemData;

typedef list<fileSystemData> fileSystemList;    // 파일시스템 저장 데이터

bool get_host_info(host_info*);
int getCPUCount();
void getOSInfo(ciString &pInfo);

#endif 
