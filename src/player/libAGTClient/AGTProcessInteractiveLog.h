#ifndef _AGTProcessInteractiveLog_H_
#define _AGTProcessInteractiveLog_H_

#include "ci/libBase/ciListType.h"

typedef struct _InteractiveLog {
	_InteractiveLog() { eventTime=0;counter=0; watchSec=0; }
	ciULong eventTime;		// 간 
	ciString programId;		// 프로그램 아이디
	ciString contentsId;	// 컨텐츠 아이디
	ciString keyword;		// 이벤트내용
	ciString keyword2;		// 이벤트내용
	ciULong	counter;
	ciULong watchSec;
} InteractiveLog;

typedef map<ciString, InteractiveLog*> InteractiveLogMap;

class AGTProcessInteractiveLog {
public:
	AGTProcessInteractiveLog(const char* siteId, const char* hostId);
	virtual ~AGTProcessInteractiveLog();

	ciBoolean Execute(const char* iFileName, ciString& oFileName);
	ciBoolean _ExecuteInteractiveLog(const char* strBuffere);
	void ANSI2UTF8(const char *in, char *out, int nOut);
	void toUTF8(ciString& xmlBuf);

protected:
	ciBoolean _Execute(const char* iFileName, ciString& oFileName);
	ciBoolean _Execute_MultiLang(const char* iFileName, ciString& oFileName);
	ciULong _getWatchSec(ciString& strArg);

	InteractiveLogMap _interactiveLogTable;
	ciString _siteId;
	ciString _hostId;
};

#endif //_AGTProcessInteractiveLog_H_
