#ifndef _AGTSocketHandler_h_
#define _AGTSocketHandler_h_

#include "ci/libBase/ciBaseType.h"
#include "common/libAGTData/AGTCommand.h"
#include "libShutdownTimer/ShutdownTimer.h"
#include "libShutdownTimer/MonitorOnOffTimer.h"
#include "libShutdownTimer/ShutdownThread.h"
#include "AGTClientTimer.h"


class AGTSetProgramThread;
class AGTDownloadResult;

class AGTSocketHandler {
public:
	AGTSocketHandler(ShutdownTimer* pApp, MonitorOnOffTimer* pApp2, int pPeriod);
	virtual ~AGTSocketHandler();

	void init(const char* siteId, const char* hostId, 
			const char* macAddr, const char* edition,
			const char* serverIP, const char* org_serverIP, int serverPort, int period, int screenshotPeriod);
	void fini();

	ciBoolean sendInfo(ciBoolean bootUp, int counter);
	ciBoolean sendDownloadState();
	int		getPeriod() { return _period; }

	void setHostMsg1(const char* msg) { _fwHostMsg1 = msg; }
	void setHostMsg2(const char* msg) { _fwHostMsg2 = msg; }

	ciString  getFwMsg() ;
	void  setFwMsg(const char* msg) ;

protected:
	ciBoolean _bypassInfo(const char* sendMsg, int serverPort, const char* serverIp = "127.0.0.1");
	ciBoolean _sendInfo(const char* sendMsg, SOCKET& server_fd);
	ciBoolean _recvCmd(SOCKET& server_fd);
	void _processCmd(AGTCommand& command);

	ciBoolean	_sendInfoHttp(const char* sendMsg);
	ciBoolean   _sendDownloadStateHttp(const char* sendMsg);


	void getPowerOnOffTime(unsigned long& onTime, unsigned long& offTime);
	ciBoolean _getPowerOnOffTime(const char* logName, 
		 unsigned long bootDownNo,  unsigned long bootUpNo,							
		unsigned long& onTime, unsigned long& offTime);


	void _scheduleMonitor(
		ciString& curSchedule1, 
		ciString& curSchedule2, 
		ciString& autoSchedule1, 
		ciString& autoSchedule2,
		ciString& hostMsg1, 
		ciString& hostMsg2 
		);

	void _systemMonitor(
		ciString& hostName, 
		ciString& os,
		ciString& hostIP,
		ciUInt& realMemoryUsed,
		ciUInt& realMemoryTotal,
		ciUInt& virtualMemoryUsed,
		ciUInt& virtualMemoryTotal,
		ciUInt& swapInUsed,
		ciFloat& total_cpu_used
		);
	void _filesystemMonitor(ciString& diskUsed);
	void _filesystemMonitor(ciString& diskUsed, const char* targetDrive);
	ciBoolean _soundMonitor(ciULong& volume, ciBoolean& mute, ciInt& soundDisplay);
	ciBoolean _shutdownTime(ciString& shutdownTime, ciString& holiday);			// GET
	void _shutdownTime(const char* strValue);			// SET
	void _startupTime(const char* strValue);			// 사용되지 않음
	ciBoolean _getLastScreenShotFile(ciString& fileName);
	ciBoolean _getAuthDate(unsigned long& authDate);
	ciBoolean _getAutoUpdateFlag();	
	ciLong _getRenderMode();	
	ciBoolean _getVersion(ciString& version);
	ciLong _getMonitorUseTime();
	ciLong _getVncPort();
	ciBoolean _getVNCState();
	ciBoolean _getProtocolType(ciString& protocolType);

	ciBoolean _getProcessState(const char* binaryName);
	ciShort _getMonitorState();
	ciBoolean _getStarterState();
	ciBoolean _getBrowserState();
	ciBoolean _getBrowser1State();
	ciBoolean _getFirmwareViewState();
	ciBoolean _getPreDownloaderState();

	ciUShort _getPlayLogDayLimit();
	ciBoolean _getPlayLogDayCriteria(ciString& outval);
	ciBoolean _getMonitorOffList(ciString& outval);
	ciBoolean _getMonitorOff(ciBoolean& outval);
	ciBoolean _getCategory(ciString& category);
	ciBoolean _getMonitorType(ciShort& monitorType);
	ciBoolean _getContentsDownloadTime(ciString& outval);
	ciBoolean _getWinPassword(ciString& outval);
	ciBoolean _getWeekShutdownTime(ciString& outval);
	ciUShort _getHddThreshold();

	ciBoolean _setAutoUpdateFlag(ciBoolean autoUpdateFlag);
	ciBoolean _setRenderMode(ciLong renderMode);
	ciBoolean _setPlayLogDayLimit(ciUShort dayLimit);
	ciBoolean _setPlayLogDayCriteria(const char* dayCriteria);
	ciBoolean _setMonitorOffList(const char* monitorOffList);
	ciBoolean _setMonitorOff(ciBoolean monitorOff);
	ciBoolean _setCategory(const char* category);
	ciBoolean _setMonitorType(ciShort monitorType);
	ciBoolean _setContentsDownloadTime(const char* dayCriteria);
	ciBoolean _setWinPassword(const char* winPassword);
	ciBoolean _setWeekShutdownTime(const char* weekShutdownTime);
	ciBoolean _setHddThreshold(ciUShort threshold);
	ciBoolean _cleanContents(ciString& programList);

	ciBoolean _cleanAllContents();
	ciBoolean _cleanContents_excludeProgramList(ciStringList& excludeProgramList);
	ciBoolean _cleanContents_deleteProgramList(ciStringList& deleteProgramList);
	ciBoolean _cleanContents_deleteFileList(ciStringList& deleteFileList);

	ciBoolean _monitorPower(ciBoolean onOff);
	ciBoolean _monitorPower(ciBoolean onOff, ciString& ipAddress);

	void		_setProgram(AGTCommand& command, const char* display);
	void		_autoUpdate(AGTCommand& command);
	void		_announceExpired(AGTCommand& command);
	void		_startup(AGTCommand& command);
	void		_importProgram(ciString& program, int display, time_t now);

	ciBoolean	_writeBootTime();
	ciBoolean	_readBootTime();
	ciBoolean	_ignoreCmd(ciString& strCmd, unsigned long timestamp);
	
	int			_setProperties(ciString& properties);
	bool		_setPropertyName(ciString& properties);
	int			_getProperty(ciString& value);

	ciBoolean	_setIni(ciString& directive,ciString& filename,ciString& entryname,
						ciString& varname, ciString& value);
	ciBoolean	_getIni(ciString& filename,ciString& entryname,
						ciString& varname, ciString& value);

protected:
	ciString _siteId;
	ciString _hostId;
	ciString _customer;
	ciString _serverIP;
	ciString _org_serverIP;
	ciString _macAddress;
	ciString _edition;
	int _serverPort;
	int _bypassPort;
	int _period;
	int _screenshotPeriod;
	AGTClientTimer* _timer;
	int _displayCounter;

	ciString _propertyName;  //항상 보고할 ini 값
	ciString _fwHostMsg1;  //브라우저가 아닌, firmwareView 가 보고하는 host메시지
	ciString _fwHostMsg2;  //브라우저가 아닌, firmwareView 가 보고하는 host메시지
	ciString _fwMsg; // socket 이 firmwareView 에게 전달하는 메시지
	ciMutex _fwMsgLock; // socket 이 firmwareView 에게 전달하는 메시지

	list<AGTSetProgramThread*>	_setProgramThreadList;
	ShutdownTimer* m_shutdownTimer;
	MonitorOnOffTimer* m_monitorOnOffTimer;
	
	unsigned long 	_bootUpTime;
	unsigned long 	_bootDownTime;

	bool _noMonitor;

};

#endif //_AGTSocketHandler_h_