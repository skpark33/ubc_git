/*! \file AGTClientSession.C
 *  Copyright ⓒ 2002, SQIsoft. All rights reserved.
 *
 *  \brief UBC Agent Client World
 *  (Environment: TAO 1.6.2, Windows 2003 SP2)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2009/07/15 17:01:00
 */

#include "AGTClientSession.h"
#include "AGTMuxCheckThread.h"

#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libThread/ciThread.h>
#include <ci/libConfig/ciIni.h>
#include <hi/libHttp/hiHttpMux.h>
#include "common/libSockUtil/sockUtil.h"
#include "common/libScratch/scratchUtil.h"
#include "common/libCommon/ubcIni.h"
#include "common/libCommon/ubcMuxRegacy.h"


ciSET_DEBUG(10, "AGTClientSession");

ciMutex AGTClientSession::_sLock;
AGTClientSession* AGTClientSession::_instance = 0;

AGTClientSession::AGTClientSession() 
: _siteId(""), _hostId(""), _period(_DEFAULT_60_SEC_), _screenshotPeriod(300), _agent(0), _muxChecker(0),
  _serverIP(""), _serverPort(0), _ftpId(""), _ftpPasswd(""), _ftpPort(21),_connectionStatus(ciFalse)
{
	ciDEBUG(3, ("AGTClientSession()"));
	ubcConfig  aIni("UBCVariables");

	ciString strPeriod;
	if (!aIni.get("ROOT","AgentConnectionPeriod", strPeriod)) {
		ciWARN(("AgentConnectionPeriod get failed."));
		_period = _DEFAULT_60_SEC_;
	}else{
		_period = atoi(strPeriod.c_str());
		if(_period==0){
			ciWARN(("PERIOD is 0, _DEFAULT_60_SEC_ will be used"));
			_period = _DEFAULT_60_SEC_;
		}
	}
	ciDEBUG(1,("AgentConnectionPeriod=%d", _period));
}

AGTClientSession::~AGTClientSession() {
	ciDEBUG(3, ("~AGTClientSession()"));
}

AGTClientSession* AGTClientSession::getInstance()
{
	if (!_instance) {
		CI_GUARD(_sLock, "getInstance()");
		if (!_instance) {
			_instance = new AGTClientSession;
		}
	}
	return _instance;
}

void AGTClientSession::clearInstance()
{
	if (_instance) {
		CI_GUARD(_sLock, "getInstance()");
		if (_instance) {
			delete _instance;
			_instance = 0;
		}
	}
}

ciBoolean 
AGTClientSession::init(ShutdownTimer* pApp, MonitorOnOffTimer* pApp2)
{
	ciDEBUG(5, ("AGTClientSession::init()"));	

	// get Host ID, Site ID, Mac Address, Period
	if (!_getHostInfo(_siteId, _hostId, _macAddress, _edition, _period,_screenshotPeriod))
	{
		ciERROR(("get host infomation failed.."));
		return ciFalse;
	}

	if (!initSession(pApp, pApp2)) {
		ciERROR(("initSession() failed."));
		if (!_muxChecker) {
			_muxChecker = new AGTMuxCheckThread(this, pApp, pApp2,_period);
			_muxChecker->start();
		} else {
			ciERROR(("Something wrong!!! AGTMuxCheckThread is not null."));
			return ciFalse;
		}
	}

	// startup 패킷 전송
	_startup();

	return ciTrue;
}

ciBoolean
AGTClientSession::fini()
{
	// shutdown 패킷 발행
	_shutdown();

	if (_muxChecker) {
		_muxChecker->destroy();
		_muxChecker->join();
		_muxChecker = 0;
	}
	if(_agent) {
		_agent->fini();
		delete _agent;
		_agent = 0;
	}
	return ciTrue;
}

ciBoolean
AGTClientSession::initSession(ShutdownTimer* pApp, MonitorOnOffTimer* pApp2)
{
	ciDEBUG(9, ("initSession()"));

	// AgentMux로부터 AgentServer 접속 정보 획득

	if (!_getServerInfo()) {
		ciERROR(("get server infomation failed.."));
		return ciFalse;
	}

	// Agent 객체 생성
	if (!_agent) {
		_agent = new AGTSocketHandler(pApp, pApp2,_period);
		_agent->init(_siteId.c_str(), _hostId.c_str(), 
					_macAddress.c_str(), _edition.c_str(),
					_serverIP.c_str(), _org_serverIP.c_str(),_serverPort, _period, _screenshotPeriod);
	}
	return ciTrue;
}

ciBoolean	
AGTClientSession::getConnectionStatus()
{
	ciGuard aGuard(_connectionStatusLock);
	return _connectionStatus;
}
void	
AGTClientSession::setConnectionStatus(ciBoolean status)
{
	ciGuard aGuard(_connectionStatusLock);
	_connectionStatus = status;
}


ciBoolean
AGTClientSession::_startup()
{
	ciDEBUG(5, ("_startup()"));
	if (_agent) {
		//_agent->startup();
		;
	}	
	return ciTrue;
}

ciBoolean
AGTClientSession::_shutdown()
{
	ciDEBUG(5, ("_shutdown()"))
	if (_agent) {
		//_agent->shutdown();
		;
	}
	return ciTrue;
}

ciBoolean
AGTClientSession::_getHostInfo(ciString& oSiteId, ciString& oHostId, 
							 ciString& oMacAddress, ciString& oEdition, int& oPeriod, int& oScreenshotPeriod)
{
	ciDEBUG(5, ("_getHostInfo()"));

	scratchUtil* sUtil = scratchUtil::getInstance();
	if (!sUtil->readAuthFile(oHostId, oMacAddress, oEdition)) {
		ciERROR(("scratchUtil::readAuthFile() failed."));
		return ciFalse;
	}

	// common 을 인클루드 하지만,  libCommon 을 링크하지 않고, 
	// libInstall 을 링크하는 점에 유의한다.  이는 cci 를 물지 않기 위한 트릭이다.

	ubcConfig  aIni("UBCVariables");
	for(int i=0;i<3;i++){  // 못읽으면, 1초 쉬고, 다시 시도해본다. 3차례까지 시도한다.
		if (!aIni.get("ROOT","SiteId", oSiteId)) {
			::Sleep(1000);
			continue;
		}
		if(oSiteId=="NULL"||oSiteId.empty()){
			::Sleep(1000);
			continue;
		}
	}
	// 그래도 실패하면, hostId 를 이용한다.
	if(oSiteId=="NULL"||oSiteId.empty()){
		if (string::npos != oHostId.find("-")) {
			oSiteId = oHostId.substr(0, oHostId.find("-"));
		} else {
			oSiteId = oHostId;
		}
		ciWARN(("SiteId get failed. set %s", oSiteId.c_str()));
	}

	/*
	ciString strPeriod;
	if (!aIni.get("ROOT","AgentConnectionPeriod", strPeriod)) {
		ciWARN(("AgentConnectionPeriod get failed."));
		oPeriod = _DEFAULT_60_SEC_;
	}else{
		oPeriod = atoi(strPeriod.c_str());
		if(oPeriod==0){
			ciWARN(("PERIOD is 0, _DEFAULT_60_SEC_ will be used"));
			oPeriod = _DEFAULT_60_SEC_;
		}
	}
	*/
	oPeriod = _period;
	ciDEBUG(1,("AgentConnectionPeriod=%d", oPeriod));



	ciString strScreenShotPeriod;
	if (!aIni.get("ROOT","ScreenShotPeriod", strScreenShotPeriod)) {
		ciWARN(("ScreenShotPeriod get failed."));
		oScreenshotPeriod = 60;
	}else{
		oScreenshotPeriod = atoi(strScreenShotPeriod.c_str());
		if(oScreenshotPeriod==0){
			ciWARN(("oScreenshotPeriod is 0, 600 will be used"));
			oScreenshotPeriod = 600;
		}
	}
	ciDEBUG(1,("ScreenShotPeriod=%d", oScreenshotPeriod));



	return ciTrue;
}

ciBoolean 
AGTClientSession::_getServerInfo()
{
	ciString muxIp;
	int muxPort;
	if (!_getMuxServerInfo(muxIp, muxPort)) {
		return ciFalse;
	}

	_org_serverIP="";

	if(ciIniUtil::isHttpOnly()){
		ubcMuxData* pInfo = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(_siteId.c_str());
		if(!pInfo){
			ciERROR(("Mux Connection failed"));
			return ciFalse;
		}
		_serverIP = pInfo->getIpAddress();
// 만약 NAT 를 사용하는 경우라면, 여기서 얻어지는 IP 는  NAT IP 가 되므로 
// 원래 IP 로 되돌려 리턴해 주어야 한다.
		_NAT_IP_MAP(_serverIP.c_str(), _org_serverIP);

		_serverPort = pInfo->socketPort;
		if (_serverPort == 0) {
			ciERROR(("Invalid mux information"));
			return ciFalse;
		}
		_ftpId = pInfo->ftpId;
		_ftpPasswd = pInfo->ftpPasswd;
		_ftpPort = pInfo->ftpPort;

		return ciTrue;
	}

	// dialog to AgentMux
	sockUtil* sUtil = sockUtil::getInstance();

	// SITE=<SITE_ID>,AGT=<HOST_ID>
	ciString sendMsg = "SITE=";
	sendMsg += _siteId;
	sendMsg += ",AGT=";
	sendMsg += _hostId;
	sendMsg += "\n";

	ciDEBUG(1,("sendMsg=%s", sendMsg.c_str()));

	ciString recvMsg,errMsg;
	if (!sUtil->dialog(muxIp.c_str(), muxPort, 10, sendMsg.c_str(), recvMsg,errMsg))
	{
		ciERROR(("AgentMux connect failed. : %s",errMsg.c_str()));
		return ciFalse;
	}
	ciDEBUG(2, ("RECV: %s", recvMsg.c_str()));

	// SERVER_INFO=<IP>:<PORT>:ftpId=<ftpUser>:ftpPasswd=<ftpPasswd>:ftpPort=<ftpPort>\n
	ciStringTokenizer token(recvMsg.c_str(), "=:\n");
	if (token.countTokens() != 9) {
		ciERROR(("invalid message. [%s]", recvMsg.c_str()));
		return ciFalse;
	}

	token.nextToken();
	_serverIP = token.nextToken();
	_serverPort = atoi(token.nextToken().c_str());

	if (_serverPort == 0) {
		ciERROR(("invalid message. [%s]", recvMsg.c_str()));
		return ciFalse;
	}

	token.nextToken();
	_ftpId = token.nextToken();
	token.nextToken();
	_ftpPasswd = token.nextToken();
	token.nextToken();
	_ftpPort = atoi(token.nextToken().c_str());

	return ciTrue;
}

ciBoolean
AGTClientSession::_NAT_IP_MAP(const char* natIp, ciString& orgIp)
{
	ciDEBUG(9, ("_NAT_IP_MAP(%s)", natIp));

	ubcConfig  aIni("UBCConnect");
	if (!aIni.get("NAT",natIp, orgIp)) {
		orgIp="";
		return ciFalse;
	}
	if(orgIp.empty()){
		orgIp="";
		return ciFalse;
	}
	ciWARN(("NAT_IP(%s) --> ORG_IP(%s)", natIp, orgIp.c_str()));
	return ciTrue;
}


ciBoolean 
AGTClientSession::_getMuxServerInfo(ciString& oMuxIp, int& oMuxPort)
{
	ciDEBUG(9, ("getMuxServerInfo()"));

	oMuxIp.clear();
	oMuxPort = 0;

	ubcConfig  aIni("UBCConnect");
	if (!aIni.get("AGENTMUX","IP", oMuxIp)) {
		ciERROR(("[AGENTMUX]IP is not found."));
		return ciFalse;
	}
	ciShort muxPort = 0;
	if (!aIni.get("AGENTMUX","PORT", muxPort)) {
		ciERROR(("[AGENTMUX]PORT is not found."));
		return ciFalse;
	}
	oMuxPort = muxPort;

	return ciTrue;
}

