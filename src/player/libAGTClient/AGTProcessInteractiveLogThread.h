#ifndef _AGTProcessInteractiveLogThread_h_
#define _AGTProcessInteractiveLogThread_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>

////////////////////
// AGTProcessInteractiveLogThread

class AGTProcessInteractiveLogThread : public virtual ciThread {
public:
	AGTProcessInteractiveLogThread();
	virtual ~AGTProcessInteractiveLogThread();

	virtual ciBoolean ExecuteAllInteractiveLog();
	virtual ciBoolean ExecuteInteractiveLog(const char* fileName);
	ciBoolean FTPAllUpload(const char* fileFilter);
	ciBoolean FTPUpload(const char* fileName);
	virtual void DeleteOldInteractiveLog();
	virtual void DeleteOldFiles(const char* fileFilter, ciUShort dayLimit);
	virtual void run();

	ciString date2Str(time_t tm);
	void GetTodayLogName(ciString& todayLog);

	bool	m_bUseHttp;				///<Http를 사용하는지 여부
};

#endif // _AGTProcessInteractiveLogThread_h_
