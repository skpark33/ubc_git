#ifndef _AGTMuxCheckThread_h_
#define _AGTMuxCheckThread_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include "AGTClientSession.h"

////////////////////
// AGTMuxCheckThread

class AGTMuxCheckThread : public virtual ciThread {
private:
	bool _isAlive;
	unsigned int _period;
	AGTClientSession* _session;

public:
	bool isWorking;

	AGTMuxCheckThread(AGTClientSession* session,ShutdownTimer* pApp, MonitorOnOffTimer* pApp2, int pPeriod);
	virtual ~AGTMuxCheckThread();

	void run();
	void destroy();
	ShutdownTimer* m_shutdownTimer;
	MonitorOnOffTimer* m_monitorOnOffTimer;
};

#endif // _AGTMuxCheckThread_h_
