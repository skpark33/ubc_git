 /*! \file AGTProcessManager.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief UBC Agent Client World
 *  (Environment: TAO 1.6.2, Windows 2003 SP2)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2009/07/15 17:01:00
 */

#ifndef _AGTProcessManager_h_
#define _AGTProcessManager_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciMutex.h>
#include <ci/libBase/ciListType.h>

class AGTProcessManager {
public:
	typedef map<ciString,int> _PSMAP;

	static AGTProcessManager* getInstance();
	static void clearInstance();
	virtual ~AGTProcessManager();

	ciBoolean	updateState(const char* binaryName, int state);
	ciBoolean	increaseState(const char* binaryName);
	int			getState(const char* binaryName);
	ciBoolean	startProcess(const char* binaryName);
 	
protected:
	AGTProcessManager();


protected:
	static ciMutex _sLock;
	static AGTProcessManager* _instance;

	_PSMAP	_processStateMap;
	ciMutex	_pLock;

};
		
#endif //_AGTProcessManager_h_
