#ifndef _installUtil_h_
#define _installUtil_h_

#include "ci/libBase/ciBaseType.h"
#include "ci/libThread/ciSyncUtil.h"

class AGTInstallUtil {
public:
	static AGTInstallUtil*	getInstance();
	static void	clearInstance();

	virtual ~AGTInstallUtil() ;

	ciBoolean getHost(int display, ciString& autoHost, ciString& currentHost);
	ciBoolean getHost(int display, ciString& autoHost);

	ciBoolean getHostType(ciLong& hostType);
	ciBoolean	setRegistry(HKEY firstKey, const char* regKey, const char* name, const char* val);
	ciBoolean	setAutoLogon(const char* id, const char* password);
	ciBoolean setProperty(const char* name, const char* value);
	ciBoolean changePassword(const char* id, const char* pwd);

protected:
	AGTInstallUtil();
	ciBoolean	_getHostValueUsingId(const char* brwId, ciString& outVal);
	ciBoolean	_getHostValue(const char* argValue, ciBoolean useComputerName, ciString& outVal);

	static AGTInstallUtil*	_instance;
	static ciMutex _instanceLock;

	ciString _hostId;
	ciString _siteId;
};

#endif // _installUtil_h_
