 /*! \file AGTClientSession.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief UBC Agent Client World
 *  (Environment: TAO 1.6.2, Windows 2003 SP2)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2009/07/15 17:01:00
 */

#ifndef _AGTClientSession_h_
#define _AGTClientSession_h_

#include <ci/libBase/ciBaseType.h>
#include "ci/libThread/ciSyncUtil.h"
#include "AGTSocketHandler.h"

class AGTMuxCheckThread;


class AGTClientSession {
public:
	static AGTClientSession* getInstance();
	static void clearInstance();
	virtual ~AGTClientSession();
 	
	ciBoolean init(ShutdownTimer* pApp, MonitorOnOffTimer* pApp2);
	ciBoolean fini();
	ciBoolean initSession(ShutdownTimer* pApp, MonitorOnOffTimer* pApp2);

	// ScreenShot FTP Server Infomation
	const char*	getFtpIP() {return _serverIP.c_str(); }
	const char*	getFtpId() {return _ftpId.c_str(); }
	const char*	getFtpPasswd() {return _ftpPasswd.c_str(); }
	int			getFtpPort() { return _ftpPort; }
	const char* getSiteId() { return _siteId.c_str(); }
	const char* getHostId() { return _hostId.c_str(); }

	//skpark add 2009/12/17
	ciBoolean	getConnectionStatus();
	void	setConnectionStatus(ciBoolean status);
	int			getPeriod() { return _period;}

	void setHostMsg1(const char* msg) { if(_agent) _agent->setHostMsg1(msg); }
	void setHostMsg2(const char* msg) { if(_agent) _agent->setHostMsg2(msg); }

	ciString  getFwMsg() { if(_agent) return _agent->getFwMsg();  return ciString("NULL"); }
	void  setFwMsg(const char* msg) { if(_agent) _agent->setFwMsg(msg); }


protected:
	AGTClientSession();

	ciBoolean _startup();
	ciBoolean _shutdown();
	ciBoolean _getServerInfo();
	ciBoolean _getMuxServerInfo(ciString& oMuxIp, int& oMuxPort);

	ciBoolean _getHostInfo(ciString& oSiteId, ciString& oHostId, 
		ciString& oMacAddress, ciString& oEdition, int& oPeriod, int& oScreenshotPeriod);

	ciBoolean _NAT_IP_MAP(const char* natIp, ciString& orgIp);

protected:
	static ciMutex _sLock;
	static AGTClientSession* _instance;
	ciString _siteId;
	ciString _hostId;
	ciString _macAddress;
	ciString _edition;
	int _period;

	//skpark add 2009/08/17
	int _screenshotPeriod;
	ciString _serverIP;
	ciString _org_serverIP;
	int	 _serverPort;

	//skpark add 2009/12/17
	ciBoolean _connectionStatus;
	ciMutex _connectionStatusLock;


	ciString _ftpId;
	ciString _ftpPasswd;
	int _ftpPort;

	AGTSocketHandler* _agent;
	AGTMuxCheckThread* _muxChecker;
};
		
#endif //_AGTClientSession_h_
