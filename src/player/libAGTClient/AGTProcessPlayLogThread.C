#include "stdAfx.h"
#include "ci/libDebug/ciDebug.h"
#include "ci/libBase/ciTime.h"

#include "libFTPClient/FTPClient.h"
#include "AGTClientSession.h"
#include "AGTProcessPlayLog.h"
#include "AGTProcessPlayLogThread.h"
#include "common/libCommon/ubcIni.h"
#include "FileServiceWrapDll.h"
//#include "common/libScratch/scPath.h"

#define USERLOG_PATH _T(_COP_CD("C:\\SQISoft\\UTV1.0\\execute\\log\\"))

ciSET_DEBUG(10, "AGTProcessPlayLogThread");

////////////////////
// AGTProcessPlayLogThread

AGTProcessPlayLogThread::AGTProcessPlayLogThread()
: m_bUseHttp(false)
{
	ciDEBUG(2, ("AGTProcessPlayLogThread()"));
}

AGTProcessPlayLogThread::~AGTProcessPlayLogThread() {
	ciDEBUG(2, ("~AGTProcessPlayLogThread()"));
}

void 
AGTProcessPlayLogThread::run() {
	ciDEBUG(5, ("run()"));

	// Agent들의 FTP 업로드 타이밍을 분산
	ciString hostId = AGTClientSession::getInstance()->getHostId();
	int loc = hostId.find("-");
	if (loc != string::npos && loc < hostId.length()) {
		int numId = atoi(hostId.substr(loc+1).c_str());
		ciDEBUG(1, ("Sleep %d ms", numId));
		::Sleep(numId);
	}

	ExecuteAllPlayLog();

	//Http를 아용하는지 여부
	m_bUseHttp = GetUseHttp();
	if(m_bUseHttp)
	{
		HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
		if(hr != S_OK)
		{
			ciDEBUG(1, ("Fail to CoInitializeEx"))
		}
		else
		{
			ciDEBUG(1, ("Success CoInitializeEx"))
		}//if
	}//if

	ciString fileFilter = USERLOG_PATH;
	fileFilter += "Play_";
	fileFilter += AGTClientSession::getInstance()->getHostId();
	fileFilter += "*.csv";

	FTPAllUpload(fileFilter.c_str());

	fileFilter = USERLOG_PATH;
	fileFilter += "Raw__";
	fileFilter += AGTClientSession::getInstance()->getHostId();
	fileFilter += "*.csv";

	FTPAllUpload(fileFilter.c_str());

	DeleteOldPlayLog();

	if(m_bUseHttp)
	{
		CoUninitialize();
	}//if

	ciDEBUG(2, ("AGTProcessPlayLogThread close..."));
	//delete this;
}

void
AGTProcessPlayLogThread::GetTodayLogName(ciString& todayLog)
{
	//ciDEBUG(9, ("GetTodayLogName()"));

	// get PlayLogDayCriteria
	ubcConfig  aIni("UBCVariables");
	ciString dayCriteria;
	if (!aIni.get("ROOT","PlayLogDayCriteria", dayCriteria)) {
		dayCriteria = "00:00";
	}
	if (dayCriteria.empty()) {
		dayCriteria = "00:00";
	}
	ciDEBUG(1, ("[ROOT]PlayLogDayCriteria=%s", dayCriteria.c_str()));

	// parsing
	unsigned short hour, minute;
	int colonLoc = dayCriteria.find(":");
	if (colonLoc == std::string::npos) {
		hour = atoi(dayCriteria.c_str());
		minute = 0;
	} else {
		hour = atoi(dayCriteria.substr(0, colonLoc).c_str());	
		minute = atoi(dayCriteria.substr(colonLoc+1).c_str());
	}
	if (hour >= 24) hour = 0;
	if (minute >= _DEFAULT_60_SEC_) minute = 0;

	// current date
	time_t the_time;
	time(&the_time);
	struct tm m_tm = *localtime(&the_time);

	// set log filename
	todayLog = "Play_";
	if (hour > m_tm.tm_hour 
		|| ( hour == m_tm.tm_hour && minute > m_tm.tm_min)) 
	{
		todayLog += date2Str(the_time-(3600*24));
	} else {
		todayLog += date2Str(the_time);
	}
	todayLog += ".csv";
	ciDEBUG(9, ("TodayLogName(%s)", todayLog.c_str()));
}

ciString 
AGTProcessPlayLogThread::date2Str(time_t tm) 
{
	TCHAR* str;
	struct tm aTM;
#ifdef WIN32
	struct tm *temp = localtime(&tm);
	if (temp) {
		aTM = *temp;
	} else {
		memset((void*)&aTM, 0x00, sizeof(aTM));
	}
#else
	localtime_r(&tm, &aTM);
#endif

	str = (TCHAR*)malloc(sizeof(TCHAR)*20);
	_stprintf(str,_T("%4d%02d%02d"),
		aTM.tm_year+1900,aTM.tm_mon+1,aTM.tm_mday);
	ciString strDate = str;
	free(str);
	return strDate;
}

ciBoolean 
AGTProcessPlayLogThread::ExecuteAllPlayLog()
{
	ciDEBUG(9, ("ExecuteAllPlayLog()"));

	// 경로의 모든 PlayLog 파일에 대해서 실행
	//ciTime today;
	//ciString todayLog = "Play_";
	//todayLog += today.getTimeString("%Y%m%d");
	//todayLog += ".csv";
	ciString todayLog;
	GetTodayLogName(todayLog);

	ciString fileFilter = USERLOG_PATH;
	fileFilter += "Play_2*.csv";
	WIN32_FIND_DATA fileData;
	HANDLE hFind = FindFirstFile(fileFilter.c_str(), &fileData);
	if (hFind == INVALID_HANDLE_VALUE) {
		return ciFalse;
	}

	do {
		if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			continue;
		}
		// today 제외
		if (_tcscmp(fileData.cFileName, todayLog.c_str())==0) {
			ciDEBUG(9, ("Today PlayLog[%s] excluded.", fileData.cFileName));
			continue;
		}

		ciDEBUG(9,("file=%s founded", fileData.cFileName));
		ciString iFileName = USERLOG_PATH;
		iFileName += fileData.cFileName;

		if (ExecutePlayLog(iFileName.c_str())) {
			ciDEBUG(1, ("ExecutePlayLog(%s) succeed.", iFileName.c_str()));
			ciString nFileName = USERLOG_PATH;
			nFileName += "Raw__";
			nFileName += AGTClientSession::getInstance()->getHostId();
			nFileName += (fileData.cFileName+4);
			::remove(nFileName.c_str()); // 기존 파일 삭제
			if (!(::MoveFile(iFileName.c_str(), nFileName.c_str()))) {
				ciERROR(("MoveFile failed.[%s->%s]", iFileName.c_str(), nFileName.c_str()));
			}
		} else {
			ciERROR(("ExecutePlayLog(%s) failed.", iFileName.c_str()));
		}

	} while (FindNextFile(hFind, &fileData));

	return ciTrue;
}

ciBoolean 
AGTProcessPlayLogThread::ExecutePlayLog(const char* iFileName)
{
	AGTProcessPlayLog playLog(AGTClientSession::getInstance()->getSiteId(), 
		AGTClientSession::getInstance()->getHostId());

	ciString oFileName;
	if (!playLog.Execute(iFileName, oFileName))
	{
		return ciFalse;
	}
	return ciTrue;
}

ciBoolean 
AGTProcessPlayLogThread::FTPAllUpload(const char* filter)
{
	ciDEBUG(9, ("FTPAllUpload(%s)",filter));


	WIN32_FIND_DATA fileData;
	HANDLE hFind = FindFirstFile(filter, &fileData);
	if (hFind == INVALID_HANDLE_VALUE) {
		return ciFalse;
	}

	do {
		if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			continue;
		}

		ciDEBUG(9,("file=%s founded", fileData.cFileName));
		ciString iFileName = USERLOG_PATH;
		iFileName += fileData.cFileName;

		if (FTPUpload(iFileName.c_str())) {
			ciDEBUG(1, ("ExecutePlayLog(%s) succeed.", iFileName.c_str()));
			ciString nFileName = USERLOG_PATH;
			nFileName += "FTPUploaded_";
			nFileName += fileData.cFileName;
			::remove(nFileName.c_str()); // 기존 파일 삭제
			if (!(::MoveFile(iFileName.c_str(), nFileName.c_str()))) {
				ciERROR(("MoveFile failed.[%s->%s]", iFileName.c_str(), nFileName.c_str()));
			}
		} else {
			ciERROR(("FTPUpload(%s) failed.", iFileName.c_str()));
		}

	} while (FindNextFile(hFind, &fileData));

	return ciTrue;
}

ciBoolean 
AGTProcessPlayLogThread::FTPUpload(const char* fileName)
{
	ciString oFileName = fileName;

	ciString ip = AGTClientSession::getInstance()->getFtpIP();
	ciString user = AGTClientSession::getInstance()->getFtpId();
	ciString password = AGTClientSession::getInstance()->getFtpPasswd();
	int port = AGTClientSession::getInstance()->getFtpPort();
	//ciString ip = "211.232.57.202";
	//ciString user = "server";
	//ciString password = "rjtlrl2009";
	//int port = 21;
	//ciDEBUG(1, ("%s:%d %s/%s", ip.c_str(), port, user.c_str(), password.c_str()));

	//////////////////////////////////////////////////////////////////////////////////
	// Modyfied by jwh184 2002-02-29

	// 파일명 분리하기
	/*
	ciString sFileName = oFileName;
	int loc = oFileName.find("Play");
	if (loc != string::npos) {
	oFileName = oFileName.substr(loc);
	}else{
	loc = oFileName.find("Raw__");
	if(loc != string::npos) {
	oFileName = oFileName.substr(loc);
	}
	}
	*/
	ciString sFileName = oFileName;
	int loc = oFileName.rfind("\\");
	if (loc != string::npos) {
		oFileName = oFileName.substr(loc+1);
	}

	ciString dFileName = "/Play/"; // FTP 업로드 임시파일명
	dFileName += AGTClientSession::getInstance()->getSiteId();
	dFileName += "/";
	dFileName += oFileName;
	dFileName += ".up";
	ciDEBUG(1, ("SRC[%s] -> DST[%s]", sFileName.c_str(), dFileName.c_str()));

	if(!m_bUseHttp)
	{
		ciDEBUG(1, ("It's not HTTP mode"));
		// FTP Upload
		// 타임아웃 10초, 세 번 재시도
		nsFTP::CFTPClient ftpClient(nsSocket::CreateDefaultBlockingSocketInstance(), 10, 2048, 0, 2);
		// 접속정보 설정
		nsFTP::CLogonInfo loginInfo(ip, port, user, password, user);
		// 로그인
		if (!ftpClient.Login(loginInfo)) {
			ciERROR(("FTP Login failed"));
			return ciFalse;
		}
		ftpClient.SetResumeMode(FALSE);

		// 연결 성공 실패 체크
		if(!ftpClient.IsConnected())
		{
			ciERROR(("FTP Login failed"));
			return ciFalse;
		}

		// 디렉토리 생성(이미 존재한다면 실패할 것이지만 문제없음)
		ciString targetDirectory = "/Play/";
		targetDirectory += AGTClientSession::getInstance()->getSiteId();
		ftpClient.MakeDirectory(targetDirectory);

		// 업로드
		BOOL ret = ftpClient.UploadFile(sFileName.c_str(), 
			dFileName.c_str(), 
			false, 
			nsFTP::CRepresentation(nsFTP::CType::Image()), 
			true); // passive mode
		if (ret) {
			ciString finalFileName = dFileName.substr(0, dFileName.length()-3); // FTP 최종 파일명
			ftpClient.Delete(finalFileName);
			ftpClient.Rename(dFileName, finalFileName);
			ciDEBUG(1, ("DST[%s] -> REN[%s]", dFileName.c_str(), finalFileName.c_str()));
		}

		DWORD errCode = GetLastError();
		ciDEBUG(1, ("FileUpload: GET RESULT[%u]", errCode));

		// 접속 해제
		ftpClient.Logout();

		if (!ret) { return ciFalse; }
	}
	else
	{
		ciDEBUG(1, ("It's HTTP mode"));

		CFileServiceWrap httpFile(ip.c_str(), port);
		ciDEBUG(1, ("Http file service initialized ip=%s, port=%d", ip.c_str(), port));
		httpFile.Login(user.c_str(), password.c_str());
		ciDEBUG(1, ("Http login success id=%s, password=%s", user.c_str(), password.c_str()));
		ciDEBUG(1, ("Trying Playlog PutFile %s ==> %s", sFileName.c_str(), dFileName.c_str()));
		if(!httpFile.PutFile(sFileName.c_str(), dFileName.c_str(), 0, NULL))
		{
			ciERROR(("Http file service error : ", httpFile.GetErrorMsg()));
			return ciFalse;
		}//if
		ciDEBUG(1, ("Success Playlog PutFile %s ==> %s", sFileName.c_str(), dFileName.c_str()));

		ciString finalFileName = dFileName.substr(0, dFileName.length()-3); // FTP 최종 파일명
		//같은 이름의 파일이 서버에 있다면 삭제한다.
		if(httpFile.IsExist(finalFileName.c_str()))
		{
			ciDEBUG(1, ("Trying Playlog DeleteFile %s", finalFileName.c_str()));
			if(!httpFile.DeleteFile(finalFileName.c_str()))
			{
				ciERROR(("Http file delete : %s", httpFile.GetErrorMsg()));
				return ciFalse;
			}//if
			ciDEBUG(1, ("Success Playlog DeleteFile %s", finalFileName.c_str()));
		}//if

		ciDEBUG(1, ("Trying Playlog MoveFile %s ==> %s", dFileName.c_str(), finalFileName.c_str()));
		if(!httpFile.MoveFile(dFileName.c_str(), finalFileName.c_str()))
		{
			ciERROR(("Http file move : %s", httpFile.GetErrorMsg()));
			return ciFalse;
		}//if
		ciDEBUG(1, ("Success Playlog MoveFile %s ==> %s", dFileName.c_str(), finalFileName.c_str()));
	}//if

	///////////////////////////////////////////////////////////////////////////////

	return ciTrue;
}

void
AGTProcessPlayLogThread::DeleteOldPlayLog()
{
	ubcConfig  aIni("UBCVariables");
	ciUShort dayLimit = 0;
	if (!aIni.get("ROOT","PlayLogDayLimit", dayLimit)) {
		dayLimit = 15;
	}
	if (dayLimit == 0) {
		ciWARN(("invalid PlayLogDayLimit value, set 15"));
		dayLimit = 15;
	}
	ciDEBUG(1, ("[ROOT]PlayLogDayLimit=%d", dayLimit));
	{
		ciString fileFilter = USERLOG_PATH;
		fileFilter += "RawI__*.csv";
		DeleteOldFiles(fileFilter.c_str(), dayLimit);
	}
	{
		ciString fileFilter = USERLOG_PATH;
		fileFilter += "FTPUploaded_*.csv";
		DeleteOldFiles(fileFilter.c_str(), dayLimit);
	}
}

void 
AGTProcessPlayLogThread::DeleteOldFiles(const char* fileFilter, ciUShort dayLimit)
{
	ciDEBUG(9, ("DeleteOldPlayLog(%s)", fileFilter));

	WIN32_FIND_DATA fileData;
	HANDLE hFind = FindFirstFile(fileFilter, &fileData);
	if (hFind == INVALID_HANDLE_VALUE) {
		return;
	}

	// 기준 날짜(기본 15일)
	FILETIME cutTime;
	GetSystemTimeAsFileTime(&cutTime);
	LARGE_INTEGER largeInt;
	memcpy(&largeInt, &cutTime, sizeof(FILETIME));
	__int64 IN_DAY = (__int64)10000000*60*60*24;
	largeInt.QuadPart -= IN_DAY*dayLimit; // Day Limit
	memcpy(&cutTime, &largeInt, sizeof(FILETIME));

	do {
		if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			continue;
		}
		FILETIME creationTIme = fileData.ftCreationTime;
		if (cutTime.dwHighDateTime > creationTIme.dwHighDateTime ||
			(cutTime.dwHighDateTime == creationTIme.dwHighDateTime
			&& cutTime.dwLowDateTime > creationTIme.dwLowDateTime)) 
		{
			ciDEBUG(9,("[DEL] file=%s founded", fileData.cFileName));
			ciString iFileName = USERLOG_PATH;
			iFileName += fileData.cFileName;
			::remove(iFileName.c_str());
		}
	} while (FindNextFile(hFind, &fileData));

	return;
}
