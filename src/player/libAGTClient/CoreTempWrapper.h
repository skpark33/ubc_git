#pragma once

#ifdef GETCORETEMPINFO_EXPORTS
#define GETCORETEMPINFO_API __declspec(dllexport)
#else
#define GETCORETEMPINFO_API __declspec(dllimport)
#endif

#ifndef WINAPI
#define WINAPI __stdcall
#endif

#include <iostream>
using namespace std;

typedef struct core_temp_shared_data
{
	unsigned int	uiLoad[256];
	unsigned int	uiTjMax[128];
	unsigned int	uiCoreCnt;
	unsigned int	uiCPUCnt;
	float			fTemp[256];
	float			fVID;
	float			fCPUSpeed;
	float			fFSBSpeed;
	float			fMultipier;	
	char			sCPUName[100];
	unsigned char	ucFahrenheit;
	unsigned char	ucDeltaToTjMax;
}CORE_TEMP_SHARED_DATA,*PCORE_TEMP_SHARED_DATA,**PPCORE_TEMP_SHARED_DATA;

bool GETCORETEMPINFO_API fnGetCoreTempInfo(CORE_TEMP_SHARED_DATA *&pData);
bool WINAPI fnGetCoreTempInfoAlt(CORE_TEMP_SHARED_DATA *pData);

#define UNKNOWN_EXCEPTION 0x20000000

class CCoreTempWrapper
{
public:
	CCoreTempWrapper(void);
	~CCoreTempWrapper(void);
	
	unsigned int GetCoreLoad(int Index);
    unsigned int GetTjMax(int Index);
    unsigned int GetCoreCount();
    unsigned int GetCPUCount();
    float GetTemp(int Index);
    float GetVID();
    float GetCPUSpeed();
    float GetFSBSpeed();
    float GetMultiplier();
    char* GetCPUName();
    bool IsFahrenheit();
    bool IsDistanceToTjMax();
    CORE_TEMP_SHARED_DATA GetDataStruct();

	bool GetData();
	unsigned long GetDllError() { 
		return 0;
		//return GetLastError(); 
	}
	//wchar_t* GetErrorMessage();

	static bool GetCoreTemp(std::string& strCoreTemp);
private:

	PCORE_TEMP_SHARED_DATA m_pCoreTempData;
};
