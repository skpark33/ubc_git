#ifndef _AGTProcessPlayLogThread_h_
#define _AGTProcessPlayLogThread_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>

////////////////////
// AGTProcessPlayLogThread

class AGTProcessPlayLogThread : public virtual ciThread {
public:
	AGTProcessPlayLogThread();
	virtual ~AGTProcessPlayLogThread();

	ciBoolean ExecuteAllPlayLog();
	ciBoolean ExecutePlayLog(const char* fileName);
	ciBoolean FTPAllUpload(const char* fileFilter);
	ciBoolean FTPUpload(const char* fileName);
	void DeleteOldPlayLog();
	void DeleteOldFiles(const char* fileFilter, ciUShort dayLimit);
	void run();

	ciString date2Str(time_t tm);
	void GetTodayLogName(ciString& todayLog);

	bool	m_bUseHttp;				///<Http를 사용하는지 여부
};

#endif // _AGTProcessPlayLogThread_h_
