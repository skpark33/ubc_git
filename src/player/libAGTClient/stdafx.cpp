// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// libDownload.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

// TODO: 필요한 추가 헤더는
// 이 파일이 아닌 STDAFX.H에서 참조합니다.


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Http를 사용하는지 여부를 구한다. \n
/// @return <형: bool> \n
///			<true: http 사용> \n
///			<false: http 사용하지 않음> \n
/////////////////////////////////////////////////////////////////////////////////
bool GetUseHttp()
{
	char szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(szModule, cDrive, cPath, cFilename, cExt);

	//Http 사용 여부
	CString strPath;
	strPath.Format(_T("%s%s\\data\\UBCVariables.ini"), cDrive, cPath);
	char cBuffer[256] = { 0x00 };
	::GetPrivateProfileString(_T("ROOT"), _T("HTTP_ONLY"), _T(""), cBuffer, 256, strPath);
	if(strlen(cBuffer) == 0)
	{
		return false;
	}//if
	
	return true;
}