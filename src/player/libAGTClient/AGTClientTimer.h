#ifndef _AGTClientTimer_H_
#define _AGTClientTimer_H_

#include "ci/libTimer/ciTimer.h"

class AGTSocketHandler;
class AGTProcessPlayLogThread;
class AGTProcessInteractiveLogThread;

#define _DEFAULT_60_SEC_		60

class AGTClientTimer : public ciPollable {
public:
	AGTClientTimer();
	virtual ~AGTClientTimer();

	virtual void initTimer(const char* pname, int interval);
	virtual void processExpired(ciString name, int counter, int interval);

	void setAgent(AGTSocketHandler* _agent);
protected:
	int _interval;
	ciString _hostId;
	AGTSocketHandler* _agent;
	AGTProcessPlayLogThread* _logThread;
	AGTProcessInteractiveLogThread* _logThread2;

	time_t _prevTime;
};

#endif //_AGTClientTimer_H_
