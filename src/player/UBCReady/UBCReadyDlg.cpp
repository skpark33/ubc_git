// UBCReadyDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCReady.h"
#include "UBCReadyDlg.h"
#include <io.h>
#include "scratchUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
/*
//IPC를 위한 메시지 타입 정의
#define		IPC_APP_BRW		1000	///<BRW에서 보내는 IPC 메시지
#define		IPC_APP_FWV		1001	///<FWV에서 보내는 IPC 메시지

#define		IPC_MSG_EXIT	1		///<프로그램을 종료
#define		IPC_MSG_TOPMOST	2		///<TopMost로 창을 설정(full screen)
#define		IPC_MSG_NOMAL	3		///<Normal로 창을 설정(full screen)
#define		IPC_MSG_MIMIZE	4		///<Minimize로 창을 설정

typedef struct _ST_IPC_CMD
{
	int	nVal;
} ST_IPC_CMD;
*/
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CUBCReadyDlg 대화 상자




CUBCReadyDlg::CUBCReadyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCReadyDlg::IDD, pParent)
	, m_bFullScreen(true)
	, m_bUseLogoFile(false)
	, m_nPosX(0)
	, m_nPosY(0)
	, m_nCx(1024)	
	, m_nCy(768)
	, m_nImgPosX(0)	
	, m_nImgPosY(0)
	, m_nImgCx(0)	
	, m_nImgCy(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUBCReadyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CUBCReadyDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
END_MESSAGE_MAP()


// CUBCReadyDlg 메시지 처리기

BOOL CUBCReadyDlg::OnInitDialog()
{
	/*
	this->MoveWindow(0, 0, 0, 0);
	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);
	CString strPath;
	strPath.Format("%s%s", cDrive, cPath);

	//BRW 실행
	scratchUtil* aUtil = scratchUtil::getInstance();
	string strApp, strArg;
	int nRet = aUtil->getBrowserInfo(0, strApp, strArg);
	if(nRet < 0)
	{
		TRACE("ERROR : get property failed\r\n");
	}
	else if(nRet == 0)
	{
		TRACE("NO AUTO START BRW\r\n");
	}
	else
	{
		ShellExecute(NULL, "open", strApp.c_str(), strArg.c_str(), strPath, SW_HIDE);
	}//if
*/
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	//InitUI();

	//ubcRun.bat를 실행시킨다.
	//ShellExecute(NULL, "open", "ubcRun.bat", NULL, strPath, SW_HIDE);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUBCReadyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCReadyDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this);

		CRect rectClient;
		GetClientRect(rectClient);

		dc.FillSolidRect(rectClient, RGB(0, 0, 0));
		if(m_imgLogo.IsValid())
		{
			if(m_bUseLogoFile)
			{
				//밴더의 이미지를 사용한다면 전체화면으로 이미지를 확장해서 그린다.
				m_imgLogo.Draw(dc.m_hDC, rectClient);
			}
			else
			{
				//자제 logo 이미지를 그린다면 원래 이미지 사이즈로 가운데에 그려준다
				m_imgLogo.Draw(dc.m_hDC, CRect(m_nImgPosX, m_nImgPosY, m_nImgPosX+m_nImgCx, m_nImgPosY+m_nImgCy));
			}//if
		}//if

		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCReadyDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


BOOL CUBCReadyDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN)		//ALT key 처리(단일 ALT key 만 눌렸을 때 pMsg->wParam == 18 이 온다)
	{
		switch(pMsg->wParam)
		{
		case VK_F12:
			if(m_bFullScreen)
			{
				ModifyStyle(NULL, WS_CAPTION | WS_SYSMENU);
				ShowWindow(SW_RESTORE);
			}
			else
			{
				ModifyStyle(WS_CAPTION | WS_SYSMENU, NULL);
				//왜인지는 모르겠지만 restore를 한 후에 Maximize를 해야 최대화 된다
				ShowWindow(SW_RESTORE);
				ShowWindow(SW_MAXIMIZE);
			}
			m_bFullScreen = !m_bFullScreen;
			break;
		}//switch
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UI를 초기화 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCReadyDlg::InitUI()
{
	SetWindowText("UBCReady");

	//Log 이미지 설정
	//Data 폴더에 Site명_logo.bmp 파일이 있다면 해당 파일을 보여주고
	//없다면 기본 logo 이미지를 보여준다.
	//site 정보는 Data/variable.ini의 [ROOT]의 site 값에 있다.
	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);

	CString strDataPath, strIniName, strLogoImgName, strSite;
	strDataPath.Format("%s%sdata\\", cDrive, cPath);

	//site 값 읽기
	strIniName = strDataPath;
	strIniName.Append("UBCVariables.ini");
	char cBuffer[1024] = { 0x00 };

	GetPrivateProfileString("ROOT", "SiteId", "", cBuffer, 1024, strIniName);
	strSite = cBuffer;

	//Logo 이미지 파일 확인
	strLogoImgName.Format("%s%s_logo.bmp", strDataPath, strSite);
	if(_access(strLogoImgName, 0) != 0)
	{
		//밴더의 logo 이미지가 없다
		m_bUseLogoFile = false;
		HBITMAP hBitmapTop = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_LOGO));
		m_imgLogo.CreateFromHBITMAP(hBitmapTop);
		if(m_imgLogo.GetWidth() >= m_nCx)
		{
			m_nImgPosX	= m_nPosX;
			m_nImgCx	= m_nCx;
		}
		else
		{
			m_nImgCx	= m_imgLogo.GetWidth();
			m_nImgPosX	= (m_nCx-m_nImgCx)/2;
		}//if

		if(m_imgLogo.GetHeight() >= m_nCy)
		{
			m_nImgPosY	= m_nPosY;
			m_nImgCy	= m_nCy;	
		}
		else
		{
			m_nImgCy	= m_imgLogo.GetHeight();
			m_nImgPosY	= (m_nCy-m_nImgCy)/2;
		}//if	
	}
	else
	{
		//밴더의 logo 이미지 표시
		m_bUseLogoFile = true;
		m_imgLogo.Load(strLogoImgName);
		m_nImgPosX	= m_nPosX;	
		m_nImgPosY	= m_nPosY;	
		m_nImgCx	= m_nCx;	
		m_nImgCy	= m_nCy;	
	}//if

	//전체화면으로 출력
	ModifyStyle(WS_CAPTION | WS_SYSMENU, NULL);
	//왜인지는 모르겠지만 restore를 한 후에 Maximize를 해야 최대화 된다
	//ShowWindow(SW_RESTORE);
	ShowWindow(SW_MAXIMIZE);

	//TopMost 설정
	SetWindowPos(&wndTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
	m_bFullScreen = true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램이 위치하는 좌표와 영역을 설정 \n
/// @param (int) nX : (in) 프로그램이 위치하는 X 좌표
/// @param (int) nY : (in) 프로그램이 위치하는 Y 좌표
/// @param (int) nCx : (in) 프로그램의 넓이
/// @param (int) nCy : (in) 프로그램의 높이
/////////////////////////////////////////////////////////////////////////////////
void CUBCReadyDlg::SetAppPosition(int nX, int nY, int nCx, int nCy)
{
	m_nPosX	=	nX;
	m_nPosY	=	nY;
	m_nCx	=	nCx;
	m_nCy	=	nCy;
}

void CUBCReadyDlg::OnDestroy()
{
//	scratchUtil::clearInstance();

	if(m_imgLogo.IsValid())
	{
		m_imgLogo.Destroy();
	}//if

	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

BOOL CUBCReadyDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch(pCopyDataStruct->dwData)
	{
	case IPC_APP_BRW:
	case IPC_APP_FWV:
		{
			ST_IPC_CMD stCmd;
			memcpy(&stCmd, pCopyDataStruct->lpData, sizeof(ST_IPC_CMD));
			switch(stCmd.nVal)
			{
			case IPC_MSG_EXIT:			//프로그램 종료
				{
					CDialog::OnOK();
				}
				break;
			case IPC_MSG_TOPMOST:		//TopMost 설정
				{
					if(!m_bFullScreen)
					{
						ModifyStyle(WS_CAPTION | WS_SYSMENU, NULL);
						//왜인지는 모르겠지만 restore를 한 후에 Maximize를 해야 최대화 된다
						ShowWindow(SW_RESTORE);
						ShowWindow(SW_MAXIMIZE);
					}//if

					SetWindowPos(&wndTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
					m_bFullScreen = true;
				}
				break;
			case IPC_MSG_NOMAL:			//Normal 설정
				{
					if(!m_bFullScreen)
					{
						ModifyStyle(WS_CAPTION | WS_SYSMENU, NULL);
						//왜인지는 모르겠지만 restore를 한 후에 Maximize를 해야 최대화 된다
						ShowWindow(SW_RESTORE);
						ShowWindow(SW_MAXIMIZE);
					}//if

					SetWindowPos(&wndNoTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
					m_bFullScreen = true;
				}
				break;
			case IPC_MSG_MIMIZE:
				{
					ModifyStyle(NULL, WS_CAPTION | WS_SYSMENU);
					ShowWindow(SW_MINIMIZE);

					m_bFullScreen = false;
				}
				break;
			}//switch
		}
	}//switch

	return CDialog::OnCopyData(pWnd, pCopyDataStruct);
}
