// UBCReadyDlg.h : 헤더 파일
//

#pragma once

#include "ximage.h"

// CUBCReadyDlg 대화 상자
class CUBCReadyDlg : public CDialog
{
// 생성입니다.
public:
	CUBCReadyDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCREADY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

public:
	bool		m_bFullScreen;										///<프로그램이 전체화면으로 출력되는지 여부
	bool		m_bUseLogoFile;										///<프로그램이 밴더의 logo 이미지를 사용하는지 여부
	int			m_nPosX;											///<프로그램이 위치하는 X 좌표
	int			m_nPosY;											///<프로그램이 위치하는 Y 좌표
	int			m_nCx;												///<프로그램의 넓이
	int			m_nCy;												///<프로그램의 높이
	int			m_nImgPosX;											///<Logo 이미지의 X 좌표
	int			m_nImgPosY;											///<Logo 이미지의 Y 좌표	
	int			m_nImgCx;											///<Logo 이미지의 넓이
	int			m_nImgCy;											///<Logo 이미지의 높이
	CxImage		m_imgLogo;											///<화면에 그려줄 logo 이미지 객체
	
	void		InitUI(void);										///<UI를 초기화 한다.
	void		SetAppPosition(int nX, int nY, int nCx, int nCy);	///<프로그램이 위치하는 좌표와 영역을 설정

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnDestroy();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
};
