// UBCReady.h : PROJECT_NAME 응용 프로그램에 대한 주 헤더 파일입니다.
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"		// 주 기호입니다.

//! 시스템의 모니터 정보를 갖는 구조체
/*!
	 \n
*/
typedef struct _st_Monitor
{
	int		nWidth;					// Width
	int		nHeight;				// Height
	int		nWidthVirtual;			// Width Virtual
	int		nHeightVirtual;			// Height Virtual
	int		nBitPerPixel;			// BitPerPixel
	int		nRefresh;				// Refresh
	int		nMonitor;				// Monitors
	int		nMegaPixel;				// MegaPixel
	BOOL	bSameDisplayFormat;		// SameDisplayFormat
	RECT	rcVirtual;
	CArray< MONITORINFOEX, MONITORINFOEX > aryMonitors;
	
}ST_MONITOR;


// CUBCReadyApp:
// 이 클래스의 구현에 대해서는 UBCReady.cpp을 참조하십시오.
//

class CUBCReadyApp : public CWinApp
{
public:
	CUBCReadyApp();

// 재정의입니다.
public:
	ST_MONITOR	m_stInfoMonitor;				///<시스템의 모니터 정보를 갖는다

	bool		GetMonitorInformation(void);	///<모니터 정보를 구한다.
	virtual		BOOL InitInstance();

// 구현입니다.

	DECLARE_MESSAGE_MAP()
};

extern CUBCReadyApp theApp;