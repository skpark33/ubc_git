// UBCReady.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "UBCReady.h"
#include "UBCReadyDlg.h"
#include "scratchUtil.h"
#include "scPath.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUBCReadyApp

BEGIN_MESSAGE_MAP(CUBCReadyApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CUBCReadyApp 생성

CUBCReadyApp::CUBCReadyApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
	m_stInfoMonitor.nMonitor = 1;
	m_stInfoMonitor.nWidthVirtual = 1024;
	m_stInfoMonitor.nHeightVirtual = 768;
	m_stInfoMonitor.nWidth = 1024;
	m_stInfoMonitor.nHeight = 768;
	m_stInfoMonitor.nBitPerPixel= 16;
	m_stInfoMonitor.nRefresh = 60;	
}


// 유일한 CUBCReadyApp 개체입니다.

CUBCReadyApp theApp;


// CUBCReadyApp 초기화

BOOL CUBCReadyApp::InitInstance()
{
	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);
	CString strPath;
	strPath.Format("%s%s", cDrive, cPath);

	
	scratchUtil* aUtil = scratchUtil::getInstance();
	aUtil->myLogOpen("UBCReady.log");

	string mac,edition,hostId;
	if(!aUtil->readAuthFile(hostId,mac,edition)){
		aUtil->myERROR("readAuthFile failed");
		edition = STANDARD_EDITION;
	}
	aUtil->myDEBUG("EDITION=%s", edition.c_str());



	//ShellExecute(NULL, "open", "ubcRun.bat", NULL, strPath, SW_HIDE);

	// ubcRun 에서 할일을 모두 풀어서 한다.
	string ftproot = cDrive;
	ftproot += "\\ftproot";

	aUtil->createDir((ftproot + "\\SQISoft\\Contents\\ENC").c_str());
	aUtil->createDir((ftproot + "\\SQISoft\\Contents\\Temp").c_str());
	aUtil->createDir((ftproot + "\\SQISoft\\UTV1.0\\execute\\config").c_str());

#ifdef _HYUNDAI_KIA_
	/*
	string customer;
	aUtil->getCustomerInfo(customer);
	if (customer == "HYUNDAI" || customer == "KIA") {
		string moveContentsFolderResult = cDrive; 
		moveContentsFolderResult += "\\SQISoft\\moveContentsFolder.result";
		string moveContentsFolderBat = cDrive; 
		moveContentsFolderBat += "\\SQISoft\\UTV1.0\\execute\\moveContentsFolder.bat";
		string createContentsFolderBat = cDrive; 
		createContentsFolderBat += "\\SQISoft\\UTV1.0\\execute\\createContentsFolder.bat";
		// ftproot 와 C:\SQISoft\Contents 를 D 드라이브로 무브하고 소프트 링크를 건다
		bool commandStart = false;
		ifstream in(moveContentsFolderResult.c_str(), ios_base::in);
		if( in.fail() ) {
			aUtil->myDEBUG("%s file does not exist \n",moveContentsFolderResult.c_str());
			int ret = system(moveContentsFolderBat.c_str());
			if(ret==0){
				aUtil->myDEBUG("%s run succeed \n",moveContentsFolderBat.c_str());
				commandStart = true;
			}
			aUtil->myDEBUG("%s run failed %d \n",moveContentsFolderBat.c_str(), ret);
		}else{
			aUtil->myDEBUG("%s file exist \n",moveContentsFolderResult.c_str());
			in.close();
		}
		if(commandStart == false) {
			int ret = system(createContentsFolderBat.c_str());
			if(ret==0){
				aUtil->myDEBUG("%s run succeed \n",createContentsFolderBat.c_str());
			}else{
				aUtil->myDEBUG("%s run failed \n",createContentsFolderBat.c_str());
			}
		}
	}
	*/
#endif

	string command = "C:\\Windows\\system32\\subst.exe X: ";
	command += ftproot;
	

	aUtil->myDEBUG("run %s", command.c_str());
	//ShellExecute(NULL, "open", command.c_str(), NULL, strPath, SW_HIDE);
	string strArg = "X: ";
	strArg += ftproot;
	//aUtil->createProcess("C:\\Windows\\system32\\subst.exe", strArg.c_str(), "C:\\Windows\\system32",false);
	system(command.c_str());
	Sleep(200);

CString var_ini_path;
var_ini_path.Format("%sdata\\UBCVariables.ini", strPath);
char buf[2014]={0};
::GetPrivateProfileString("CUSTOMER_INFO", "NAME", "", buf, 1023, var_ini_path);
if(stricmp(buf,"UBGOLF")==0)
{
	// 골프장일 경우 ==> 딜레이후 ubc_start
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("ROOT", "UBC_READY_DELAY", "10", buf, 1023, var_ini_path);
	int delay = atoi(buf);
	if(delay<=0) delay=10;
	::Sleep(delay*1000);
}
else
{
	// 골프장이 아닌 경우에만
	for(int brwId=0;brwId<2;brwId++){
		string strApp, strArg;

		int nRet = aUtil->getBrowserInfo(brwId, strApp, strArg);
		if(nRet < 0) {
			aUtil->myDEBUG("ERROR : get BRW property failed %s %s\r\n",strApp.c_str(), strArg.c_str());
			continue;
		}
		
		if(nRet == 0){
			aUtil->myDEBUG("NO AUTO START BRW %s %s\r\n", strApp.c_str(), strArg.c_str());
			continue;
		}
	
		aUtil->myDEBUG("execute %s %s %s", strApp.c_str(), strArg.c_str(), strPath);
		//ShellExecute(NULL, "open", strApp.c_str(), strArg.c_str(), strPath, SW_HIDE);
		aUtil->createProcess(strApp.c_str(), strArg.c_str(), strPath,false);
	}
	Sleep(2000);
}

	string command1 = "SelfAutoUpdate.exe +log +ignore_env";
	aUtil->myDEBUG("run %s", command1.c_str());
	//ShellExecute(NULL, "open", command1.c_str(), NULL, strPath, SW_HIDE);
	aUtil->createProcess("SelfAutoUpdate.exe", "+log +ignore_env", strPath,false);
	
	// LG 단말일 경우,  UBCConnect.ini 의 [UBCCENTER], FTPID=ent_center --> ds_ent_center 
	// 로 수정 해야 한다.  이것이 LG 단말인지를 인지할 방법이 없다.  문제는...



	aUtil->myLogClose();
	scratchUtil::clearInstance();

	return FALSE;

	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("UBCReady"));

	CUBCReadyDlg dlg;

	//모니터의 정보를 얻어와서 프로그램이 구동되는 위치를 정한다
	int nPosX, nPosY, nCx, nCy;
	if(GetMonitorInformation())
	{
		//일단 지금은 무조건 첫번째 모니터에 최대화면으로 나오도록한다.
		//나중에는 다중 모니터를 처리할지도 모른다....
		if(m_stInfoMonitor.nMonitor > 0)
		{
			nPosX	=	m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.left;
			nPosY	=	m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.top;
			nCx		=	abs(m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.right - nPosX);
			nCy		=	abs(m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.bottom - nPosY);
		}
		else
		{
			nPosX	=	0;
			nPosY	=	0;
			nCx		=	m_stInfoMonitor.nWidth;
			nCy		=	m_stInfoMonitor.nHeight;
		}//if
	}
	else
	{
		nPosX	=	0;
		nPosY	=	0;
		nCx		=	m_stInfoMonitor.nWidth;
		nCy		=	m_stInfoMonitor.nHeight;
	}//if
	dlg.SetAppPosition(nPosX, nPosY, nCx, nCy);

	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 모니터 정보를 얻는 컬백함수 \n
/// @param (HMONITOR) hMonitor : (in) handle to display monitor
/// @param (HDC) hdcMonitor : (in) handle to monitor DC
/// @param (LPRECT) lprcMonitor : (in) monitor intersection rectangle
/// @param (LPARAM) dwData : (in) data
/////////////////////////////////////////////////////////////////////////////////
BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{
	MONITORINFOEX mi;

	mi.cbSize=sizeof(MONITORINFOEX);
	GetMonitorInfo(hMonitor,&mi);
	theApp.m_stInfoMonitor.aryMonitors.Add(mi);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 모니터 정보를 구한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCReadyApp::GetMonitorInformation()
{
	m_stInfoMonitor.nMonitor = ::GetSystemMetrics(SM_CMONITORS); 
	m_stInfoMonitor.bSameDisplayFormat = ::GetSystemMetrics(SM_SAMEDISPLAYFORMAT); 
	
	m_stInfoMonitor.rcVirtual.left = GetSystemMetrics(SM_XVIRTUALSCREEN);
	m_stInfoMonitor.rcVirtual.top = GetSystemMetrics(SM_YVIRTUALSCREEN);
	m_stInfoMonitor.rcVirtual.right = m_stInfoMonitor.rcVirtual.left + GetSystemMetrics(SM_CXVIRTUALSCREEN);
	m_stInfoMonitor.rcVirtual.bottom = m_stInfoMonitor.rcVirtual.top + GetSystemMetrics(SM_CYVIRTUALSCREEN);


	m_stInfoMonitor.nWidth = ::GetSystemMetrics(SM_CXSCREEN);						// 모니터의 해상도 x
	m_stInfoMonitor.nHeight = ::GetSystemMetrics(SM_CYSCREEN);						// 모니터의 해상도 y
	m_stInfoMonitor.nWidthVirtual = ::GetSystemMetrics(SM_CXVIRTUALSCREEN);			// 가상모니터의 해상도 x
	m_stInfoMonitor.nHeightVirtual = ::GetSystemMetrics(SM_CYVIRTUALSCREEN);		// 가상모니터의 해상도 y
	
	m_stInfoMonitor.nMegaPixel = (m_stInfoMonitor.nWidth * m_stInfoMonitor.nHeight)/(1000*1000);
	m_stInfoMonitor.nMegaPixel = max(1,m_stInfoMonitor.nMegaPixel);

	HDC hDC=NULL;
	hDC = CreateDC("DISPLAY", NULL, NULL, NULL);
	if(hDC)
	{
		EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, (LPARAM)hDC);
		m_stInfoMonitor.nBitPerPixel = GetDeviceCaps(hDC, BITSPIXEL);
		m_stInfoMonitor.nRefresh = GetDeviceCaps(hDC, VREFRESH);
		DeleteDC(hDC);
	}
	else
	{
		return false;
	}//if
	
	return true;
}

