// ServerCheckTesterDlg.h : 헤더 파일
//

#pragma once

#include "ServerCheck.h"


// CServerCheckTesterDlg 대화 상자
class CServerCheckTesterDlg : public CDialog
{
// 생성입니다.
public:
	CServerCheckTesterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SERVERCHECKTESTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()

	CServerCheck	m_svrchk;
	void	Init();
	void	ServerCheck(SERVERCHECK_STATUS servercheck);

public:
	CStatic m_stcCenterIP;
	CStatic m_stcCenterPort;
	CStatic m_stcServerIP;
	CStatic m_stcServerPort;
	CStatic m_stcServerHttpPort;
	CStatic m_stcHttpOnly;
	CStatic m_stcHostID;
	CStatic m_stcMacAddr;
	CStatic m_stcNewServerIP;

	afx_msg void OnBnClickedServerCheck();
};
