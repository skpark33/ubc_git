//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ServerCheckTester.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SERVERCHECKTESTER_DIALOG    102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_STATIC_CENTER_IP            1000
#define IDC_STATIC_CENTER_PORT          1001
#define IDC_STATIC_SERVER_IP            1002
#define IDC_STATIC_SERVER_PORT          1003
#define IDC_STATIC_HOST_ID              1004
#define IDC_STATIC_MAC_ADDR             1005
#define IDC_STATIC_NEW_SERVER_IP        1006
#define IDC_BUTTON_CHANGE_SERVER_IP     1009
#define ID_SERVER_CHECK                 1010
#define IDC_STATIC_SERVER_HTTP_PORT     1012
#define IDC_STATIC_SERVER_HTTP_PORT2    1013
#define IDC_STATIC_HTTP_ONLY            1013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
