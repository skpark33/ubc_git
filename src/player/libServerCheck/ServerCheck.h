#pragma once

enum SERVERCHECK_STATUS {
	SERVERCHECK_STATUS_UNKNOWN_ERROR = 0,			// 알수없는 에러
	SERVERCHECK_STATUS_SERVER_IP_CHANGING,			// 서버IP 변경이 필요
	SERVERCHECK_STATUS_SERVER_IP_CHANGED,			// 서버IP 변경이 반영되었음
	SERVERCHECK_STATUS_NONE,						// 아무런 작업사항 없음
	SERVERCHECK_STATUS_CENTER_CONN_ERROR,			// 센터와 접속 안됨
	SERVERCHECK_STATUS_SERVER_CONN_ERROR,			// 센터로부터 변경된 서버ip를 얻어왔으나 서버와 접속 안됨

	SERVERCHECK_STATUS_NO_HOST,						// 센터에 등록되지 않은 호스트ID
	SERVERCHECK_STATUS_SERVER_OK,					// 센터로부터 변경된 서버ip를 얻어오고 서버와 접속ok
};


// CServerCheck

class CServerCheck
{
public:
	CServerCheck();
	virtual ~CServerCheck();

protected:
	bool		m_bTimeout;

	CString		m_strServerIP;
	UINT		m_nServerPort;
	UINT		m_nServerHttpPort;
	bool		m_bHttpOnly;
	CString		m_strHostId;
	CString		m_strMacAddress;

	CString		m_strNewServerIP;

	bool		m_bConnectingToServer;
	bool		m_bConnectedToServer;
	void		SocketConnect_NonBlock(LPCTSTR lpszServerIP, int nServerPort, DWORD dwTimeout);

	bool		IsValidIP(LPCTSTR lpszIP, int len=0);
	DWORD		GetTickCountGap(DWORD start, DWORD end);

public:

	//LPCTSTR		GetCurrentCenterIP() { return m_strCenterIP; };
	//UINT		GetCurrentCenterPort() { return m_nCenterPort; };
	LPCTSTR		GetCurrentServerIP() { return m_strServerIP; };
	UINT		GetCurrentServerPort() { return m_nServerPort; };
	UINT		GetCurrentServerHttpPort() { return m_nServerHttpPort; };
	bool		IsHttpOnly() { return m_bHttpOnly; };
	LPCTSTR		GetHostId() { return m_strHostId; };
	LPCTSTR		GetMacAddress() { return m_strMacAddress; };
	LPCTSTR		GetNewServerIP() { return m_strNewServerIP; };

public:
	BOOL		Init(LPCTSTR lpszServerIP=NULL, UINT nServerPort=0, UINT nServerHttpPort=0, LPCTSTR lpszHostId=NULL, LPCTSTR lpszMacAddr=NULL);

	SERVERCHECK_STATUS	Check(DWORD dwTimeout=10000);	// time-out = 10sec
	BOOL		ChangeServerIP(LPCTSTR lpszServerIP=NULL); // stop ubc -> change ip-config -> show msg -> rebooting

	SERVERCHECK_STATUS	GetServerIP(DWORD dwTimeout=10000);	// time-out = 10sec

	bool ConnectTest(LPCTSTR lpszServerIP,int retry=3, int timeout=10);
};
