#pragma once
#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"


// CNotifyDialog 대화 상자입니다.

class CNotifyDialog : public CDialog
{
	DECLARE_DYNAMIC(CNotifyDialog)

public:
	CNotifyDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNotifyDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NOTIFY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CString	m_strNotify;
	int		m_nTime;		// default 5 sec

	int		m_nCount;

public:
	CStatic m_icon;
	CStatic m_text;

	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void	SetNotifyMessage(LPCTSTR lpszMessage) { m_strNotify=lpszMessage; };
	void	SetNotifyTime(int nSec) { m_nTime=nSec; };

	CProgressCtrl m_pc;
};
