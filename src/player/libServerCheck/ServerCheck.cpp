// ServerCheck.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerCheck.h"

#include <afxinet.h>

//#include <ci/libConfig/ciEnv.h>
//#include <ci/libConfig/ciXProperties.h>
//#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include "common/libDBUtil/dbUtil.h"
#include "common/libCommon/ubcIni.h"
//#include "CMN/libCommon/ubcIniMux.h"
#include <common/libScratch/scratchUtil.h>
//#include <CMN/libScratch/scProperties.h>
//#include <ci/libConfig/ciXProperties.h>
#include <common/libHttpRequest/HttpRequest.h>

#include "NotifyDialog.h"


ciSET_DEBUG(10, "CServerCheck");


// CServerCheck

CServerCheck::CServerCheck()
:	m_bTimeout ( false )
,	m_strServerIP ( _T("") )
,	m_nServerPort ( 0 )
,	m_nServerHttpPort ( 8080 )
,	m_bHttpOnly ( false )
,	m_strHostId ( _T("") )
,	m_strMacAddress ( _T("") )
,	m_strNewServerIP ( _T("") )
,	m_bConnectedToServer ( false )
{
	//
	ubcConfig aIni("UBCConnect");

	//
	string server_ip="";
	aIni.get("ORB_NAMESERVICE","IP", server_ip);
	m_strServerIP = ( server_ip.length()>0 ? server_ip.c_str() : _T("211.232.57.202") );

	//
	string server_port="";
	aIni.get("ORB_NAMESERVICE","PORT", server_port);
	m_nServerPort = ( server_port.length()>0 ? atoi(server_port.c_str()) : 14109 );

	//
	string server_http_port="";
	aIni.get("AGENTMUX","HTTP_PORT", server_http_port);
	m_nServerHttpPort = ( server_http_port.length()>0 ? atoi(server_http_port.c_str()) : 8080 );

	//
	ubcConfig aIniVar("UBCVariables");
	string http_only="";
	aIniVar.get("ROOT", "HTTP_ONLY", http_only);
	m_bHttpOnly = ( stricmp(http_only.c_str(), "client")==0 ? true : false );

	//
	string host_id="", mac_addr="";
	scratchUtil::getInstance()->readAuthFile(host_id, mac_addr);

	m_strHostId = host_id.c_str();
	m_strMacAddress = mac_addr.c_str();
}

CServerCheck::~CServerCheck()
{
}


// CServerCheck 메시지 처리기입니다.

void CServerCheck::SocketConnect_NonBlock(LPCTSTR lpszServerIP, int nServerPort, DWORD dwTimeout)
{
	ciDEBUG(1,("SocketConnect_NonBlock=,%s,%d", lpszServerIP,nServerPort));
	SOCKET slmSock;
	sockaddr_in	sa = {0};
	timeval		tv = {1, 0};
	fd_set		fdSet, fdE, fdW;
	int			rcv = 0;
	unsigned long	nonblock = 1;

	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr(lpszServerIP);//htonl(ADDRESS);
	sa.sin_port = htons(nServerPort);
	memset(sa.sin_zero, 0, sizeof(sa.sin_zero));

	if((slmSock = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET) goto END;
	if(ioctlsocket(slmSock, FIONBIO, &nonblock) == SOCKET_ERROR) goto END;
	FD_ZERO(&fdSet);

	rcv = connect(slmSock, (sockaddr*)&sa, sizeof(sockaddr_in));
	//ciDEBUG(10,("SocketConnect_NonBlock=%d", rcv));
	if(rcv == SOCKET_ERROR )
	{
		if(WSAGetLastError() != WSAEWOULDBLOCK) goto END;

		tv.tv_sec = 0;
		tv.tv_usec = 100;

		DWORD start_tick = ::GetTickCount();

		while( GetTickCountGap(start_tick, ::GetTickCount()) < dwTimeout)
		{
			FD_SET(slmSock, &fdSet);
			fdE = fdW = fdSet;
			rcv = select(0, 0, &fdW, &fdE, &tv);
			//ciDEBUG(10,("SocketConnect_NonBlock=%d", rcv));

			if(rcv == SOCKET_ERROR) 
			{
				goto END; // 에러
			}
			else if ( rcv > 0 )
			{
				if(FD_ISSET(slmSock, &fdE))
					goto END; // 실패
				else if(FD_ISSET(slmSock, &fdW))
				{
					nonblock = 0;

					if(ioctlsocket(slmSock, FIONBIO, &nonblock) == SOCKET_ERROR) goto END;
					m_bConnectedToServer = true;
					goto END; // 성공
				}
				else
				{
					goto END; // 에러
				}
			}
			::Sleep(0);
		}
		// 실패
		m_bTimeout = true;
	}
	else
	{
		// 성공
		m_bConnectedToServer = true;
	}

END:
	closesocket(slmSock);
}

BOOL CServerCheck::Init(LPCTSTR lpszServerIP, UINT nServerPort, UINT nServerHttpPort, LPCTSTR lpszHostId, LPCTSTR lpszMacAddr)
{
	//
	if(lpszServerIP != NULL)
	{
		m_strServerIP = lpszServerIP;
	}

	//
	if(nServerPort != 0)
	{
		m_nServerPort = nServerPort;
	}

	//
	if(nServerHttpPort != 0)
	{
		m_nServerHttpPort = nServerHttpPort;
	}

	//
	if(lpszHostId != NULL)
	{
		m_strHostId = lpszHostId;
	}

	//
	if(lpszMacAddr != NULL)
	{
		m_strMacAddress = lpszMacAddr;
	}

	ciDEBUG(10,("ServerIP=%s", m_strServerIP) );
	ciDEBUG(10,("ServerPort=%d", m_nServerPort) );
	ciDEBUG(10,("ServerHttpPort=%d", m_nServerHttpPort) );
	ciDEBUG(10,("HostId=%s", m_strHostId) );
	ciDEBUG(10,("MacAddress=%s", m_strMacAddress) );

	return TRUE;
}

SERVERCHECK_STATUS CServerCheck::Check(DWORD dwTimeout)
{
	m_bTimeout = false;
	m_bConnectedToServer = false; 

	// 1. 무조건 center로부터 서버ip를 get

	// 1a. 센터에 hostid, 현재서버ip를 넘겨준다.
	// 1b. 센터는 changeServerIp=1 and serverIpChanged=0 인 경우에만 hostid의 서버ip를 리턴한다.
	// 1c. 현재서버ip와 db의 서버ip와 같은 경우 serverIpChanged을 1로 세팅한다.
	CString send_msg;
	send_msg.Format(_T("hostId=%s&serverIp=%s"), 
		CHttpRequest::ToEncodingString(m_strHostId), 
		CHttpRequest::ToEncodingString(m_strServerIP) );

	ciDEBUG(10,("Send_msg=%s", send_msg) );

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_CENTER);

	CString response = _T("");
	BOOL ret_val = http_request.RequestPost("/UBC_Registration/check_server_ip.asp", send_msg, response);

	if(ret_val)
	{
		ciDEBUG(10,("response=%s", response) );

		if(response == _T("none"))
		{
			ciDEBUG(10,("SERVERCHECK_STATUS_NONE") );
			return SERVERCHECK_STATUS_NONE;	// 아무런 작업사항 없음
		}
		else if(IsValidIP(response))
		{
			if(m_strServerIP == response)
			{
				// 3. 가져온 서버ip값과 현재서버ip값이 같은 경우 하던일을 계속한다.
				ciDEBUG(10,("SERVERCHECK_STATUS_NONE") );
				return SERVERCHECK_STATUS_NONE;	// 아무런 작업사항 없음
			}

			// 4. 가져온 서버ip와 다를 경우 가져온 서버ip에 소켓 접속 테스트를 한다.
			m_strNewServerIP = response;
			SocketConnect_NonBlock(response, (m_bHttpOnly ? m_nServerHttpPort : m_nServerPort), dwTimeout);
			if(m_bConnectedToServer)
			{
				// 4a. 접속 테스트가 성공할 경우 서버를 변경 메시지를 띄우고 변경한다.
				ciDEBUG(10,("SERVERCHECK_STATUS_SERVER_IP_CHANGING") );
				return SERVERCHECK_STATUS_SERVER_IP_CHANGING;	// 서버IP 변경이 필요
			}

			// 4b. 서버에 접속이 안될 경우 하던일을 계속한다.
			ciDEBUG(10,("SERVERCHECK_STATUS_SERVER_CONN_ERROR") );
			return SERVERCHECK_STATUS_SERVER_CONN_ERROR;	// 센터로부터 변경된 서버ip를 얻어왔으나 서버와 접속 안됨
		}
		else if(response == _T("serverIpChanged"))
		{
			ciDEBUG(10,("SERVERCHECK_STATUS_SERVER_IP_CHANGED") );
			return SERVERCHECK_STATUS_SERVER_IP_CHANGED;	// 서버IP 변경이 반영되었음
		}
		else if(response.Find(_T("internal error")) >= 0)
		{
			ciDEBUG(10,("SERVERCHECK_STATUS_UNKNOWN_ERROR") );
			return SERVERCHECK_STATUS_UNKNOWN_ERROR;	// 알수없는 에러
		}
	}
	else
	{
		ciDEBUG(10,("response=%s", response) );
	}

	ciDEBUG(10,("SERVERCHECK_STATUS_CENTER_CONN_ERROR") );
	return SERVERCHECK_STATUS_CENTER_CONN_ERROR;	// 센터와 접속 안됨
}

BOOL CServerCheck::ChangeServerIP(LPCTSTR lpszServerIP)
{
	ciDEBUG(10,("ChangeServerIP() start...") );

	// 1. "서버의 IP Address 가 변경되어 단말을 리부트합니다" 라는 메시지를 내보낸다
	CString msg;
	msg.LoadString(IDS_SVR_ADDR_CHANGE_REBOOT);

	CNotifyDialog dlg;
	dlg.SetNotifyMessage(msg);
	if(dlg.DoModal() == IDCANCEL) return FALSE;

	// 2. UBC 를 Stop 하고
	ciDEBUG(10,("ubcKill start...") );
	string command = "ubcKill.bat";
//	if(system(command.c_str())==0)
	if((int)ShellExecute(NULL, "open", command.c_str(), NULL, NULL, SW_SHOWNORMAL) <= 32)
	{
		//MessageBox("Confirm failed");
		ciERROR(("ubcKill fail !!!") );
		return FALSE;
	}

	// 3. 모든 Configuration file 을 고친뒤
	LPCTSTR new_svr_ip = (lpszServerIP != NULL ? lpszServerIP : m_strNewServerIP);

	ciDEBUG(10,("new_svr_ip=%s", new_svr_ip) );

	if( !IsValidIP(new_svr_ip) ) return FALSE;

	ubcConfig aIni("UBCConnect");
	string oldIp;
	aIni.get("ORB_NAMESERVICE","IP", oldIp);
	ciDEBUG(10, ("oldIp=%s", oldIp.c_str()) );

	ciDEBUG(10, ("changeServerIP start...") );
	if( !dbUtil::getInstance()->changeServerIP(new_svr_ip, ciFalse) )
	{
		//MessageBox("Confirm failed");
		ciERROR(("changeServerIP fail !!!") );
		return FALSE;
	}

	string standby_ip="";
	aIni.get("ORB_NAMESERVICE","STANDBY_IP", standby_ip);
	if(!standby_ip.empty() && standby_ip != oldIp) {
		ciDEBUG(1,("Change STANDY BY IP (%s --> %s)", standby_ip.c_str(), oldIp.c_str()));
		aIni.set("ORB_NAMESERVICE","STANDBY_IP", oldIp.c_str());
	}

	//ubcIniMux::getInstance()->changeIp(oldIp.c_str(), new_svr_ip);

	// 4. 단말을 리부트한다
	ciDEBUG(10, ("rebootSystem start...") );
	command = "rebootSystem.bat";
	//if(system(command.c_str())==0)
	if((int)ShellExecute(NULL, "open", command.c_str(), NULL, NULL, SW_SHOWNORMAL) <= 32)
	{
		ciERROR(("rebootSystem fail !!!") );
		return FALSE;
	}

	ciDEBUG(10,("ChangeServerIP() end.") );
	return TRUE;
}

SERVERCHECK_STATUS CServerCheck::GetServerIP(DWORD dwTimeout)
{
	m_bTimeout = false;
	m_bConnectedToServer = false; 

	CString send_msg;
	send_msg.Format(_T("hostId=%s"), CHttpRequest::ToEncodingString(m_strHostId) );

	ciDEBUG(10, ("send_msg=%s", send_msg) );

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_CENTER);

	CString response = _T("");
	BOOL ret_val = http_request.RequestPost("/UBC_Registration/get_server_ip.asp", send_msg, response);

	if(ret_val)
	{
		ciDEBUG(10, ("response=%s", response) );
		if(response == _T("none"))
		{
			ciDEBUG(10, ("SERVERCHECK_STATUS_NO_HOST") );
			return SERVERCHECK_STATUS_NO_HOST;	// 센터에 등록되지 않은 호스트ID
		}
		else if(IsValidIP(response))
		{
			m_strNewServerIP = response;
			SocketConnect_NonBlock(response, (m_bHttpOnly ? m_nServerHttpPort : m_nServerPort), dwTimeout);
			if(m_bConnectedToServer)
			{
				m_strNewServerIP = response;
				ciDEBUG(10, ("SERVERCHECK_STATUS_SERVER_OK") );
				return SERVERCHECK_STATUS_SERVER_OK;	// 센터로부터 변경된 서버ip를 얻어오고 서버와 접속ok
			}

			ciDEBUG(10, ("SERVERCHECK_STATUS_SERVER_CONN_ERROR") );
			return SERVERCHECK_STATUS_SERVER_CONN_ERROR;	// 센터로부터 변경된 서버ip를 얻어왔으나 서버와 접속 안됨
		}
	}
	else
	{
		ciDEBUG(10, ("response=%s", response) );
	}

	ciDEBUG(10, ("SERVERCHECK_STATUS_CENTER_CONN_ERROR") );
	return SERVERCHECK_STATUS_CENTER_CONN_ERROR;	// 센터와 접속 안됨
}

bool CServerCheck::IsValidIP(LPCTSTR lpszIP, int len)
{
	if(len == 0) len = _tcslen(lpszIP);

	for(int i=0; i<len; i++)
	{
		TCHAR tch = lpszIP[i];
		if((tch < _T('0') || _T('9') < tch) && tch != _T('.'))
		{
			return false;
		}
	}
	return true;
}

DWORD CServerCheck::GetTickCountGap(DWORD start, DWORD end)
{
	if(end >= start) return end-start;
	return (MAXDWORD - start) + end;
}

bool
CServerCheck::ConnectTest(LPCTSTR lpszServerIP, int retry/*=3*/, int timeout/*=10*/)
{
	bool org_value = m_bConnectedToServer;
	for(int i=0;i<retry;i++) {
		SocketConnect_NonBlock(lpszServerIP, m_nServerHttpPort, timeout*1000);
		if(m_bConnectedToServer) {
			m_bConnectedToServer = org_value;
			return true;
		}
		::Sleep(100);
	}
	m_bConnectedToServer = org_value;
	return false;
}

