//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by libServerCheck.rc
//
#define IDP_SOCKETS_INIT_FAILED         101
#define IDD_NOTIFY                      11000
#define IDC_STATIC_ICON                 11001
#define IDC_STATIC_TEXT                 11002
#define IDC_PROGRESS1                   11003
#define IDC_PROGRESS                    11004
#define IDS_SVR_ADDR_CHANGE_REBOOT      11005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2004
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         2003
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
