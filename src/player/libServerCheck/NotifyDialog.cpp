// NotifyDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "NotifyDialog.h"


// CNotifyDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNotifyDialog, CDialog)

CNotifyDialog::CNotifyDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CNotifyDialog::IDD, pParent)
	, m_strNotify ( _T("") )
	, m_nTime ( 5 )
	, m_nCount ( 0 )
{

}

CNotifyDialog::~CNotifyDialog()
{
}

void CNotifyDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_ICON, m_icon);
	DDX_Control(pDX, IDC_STATIC_TEXT, m_text);
	DDX_Control(pDX, IDC_PROGRESS, m_pc);
}


BEGIN_MESSAGE_MAP(CNotifyDialog, CDialog)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CNotifyDialog 메시지 처리기입니다.

BOOL CNotifyDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	HICON ico = LoadIcon(NULL, IDI_WARNING);
	m_icon.SetIcon(ico);

	m_text.SetWindowText(m_strNotify);

	m_pc.SetRange32(0, m_nTime*100);
	m_pc.SetPos(m_nTime*100);

	SetTimer(1025, 50, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CNotifyDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == 1025)
	{
		m_nCount++;
		if(m_nCount/20 == m_nTime)
		{
			KillTimer(1025);
			OnOK();
		}

		m_pc.SetPos(m_nTime*100 - m_nCount*5);
	}
}
