// UBCCleanTestDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCCleanTest.h"
#include "UBCCleanTestDlg.h"
#include <libUBCCleaner/UBCCleaner.h>
#include "StdAfx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CUBCCleanTestDlg 대화 상자




CUBCCleanTestDlg::CUBCCleanTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCCleanTestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUBCCleanTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CUBCCleanTestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CLEAN_BTN, &CUBCCleanTestDlg::OnBnClickedCleanBtn)
	ON_BN_CLICKED(IDC_EXCLUDE_CLEAN_BTN, &CUBCCleanTestDlg::OnBnClickedExcludeCleanBtn)
	ON_BN_CLICKED(IDC_FILE_CLEAN_BTN, &CUBCCleanTestDlg::OnBnClickedFileCleanBtn)
	ON_BN_CLICKED(IDC_ALL_CLEAN_BTN, &CUBCCleanTestDlg::OnBnClickedAllCleanBtn)
END_MESSAGE_MAP()


// CUBCCleanTestDlg 메시지 처리기

BOOL CUBCCleanTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUBCCleanTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCCleanTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCCleanTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CUBCCleanTestDlg::OnBnClickedCleanBtn()
{
	//지정된 패키지를 지우는 테스트
	list<string> lisDeletePackage;
	CUBCCleaner ubcCleaner;
/*
	GetPackageName(3, lisDeletePackage);	

	TRACE("Package count = %d\r\n", lisDeletePackage.size());
*/
	string str = "KIA_Tar";
	lisDeletePackage.push_back(str);

	ULONGLONG ullSize = ubcCleaner.CleanDeletePackage(lisDeletePackage, false);
	CString strMsg;
	strMsg.Format("Clean completed!\r\nDeleted size is %I64u bytes", ullSize);
	AfxMessageBox(strMsg);
}

void CUBCCleanTestDlg::OnBnClickedExcludeCleanBtn()
{
	//지정된 패키지를 제외하고 지우는 테스트
	list<string> listExcludePackage;
	CUBCCleaner ubcCleaner;
/*
	GetPackageName(3, listExcludePackage);

	TRACE("Package count = %d\r\n", listExcludePackage.size());
*/

	listExcludePackage.push_back("KIA_Tar");
	listExcludePackage.push_back("KIA_Flash1");

	ULONGLONG ullSize = ubcCleaner.CleanExcludePackage(listExcludePackage, false);
	CString strMsg;
	strMsg.Format("Clean completed!\r\nDeleted size is %I64u bytes", ullSize);
	AfxMessageBox(strMsg);
}

void CUBCCleanTestDlg::OnBnClickedFileCleanBtn()
{
	//지정된 파일을 지우는 테스트
	list<string> listFiles;
	CUBCCleaner ubcCleaner;
/*
	GetFileName(3, listFiles);

	TRACE("Package count = %d\r\n", listFiles.size());
*/
	string str = "kia_did_flash";
	listFiles.push_back(str);
	ULONGLONG ullSize = ubcCleaner.CleanDeleteFiles(listFiles, false);
	CString strMsg;
	strMsg.Format("Clean completed!\r\nDeleted size is %I64u bytes", ullSize);
	AfxMessageBox(strMsg);
}

void CUBCCleanTestDlg::OnBnClickedAllCleanBtn()
{
	//ENC 폴더 모두를 지우는 테스트
	CUBCCleaner ubcCleaner;
	ULONGLONG ullSize = ubcCleaner.CleanAll(false);
	CString strMsg;
	strMsg.Format("Clean completed!\r\nDeleted size is %I64u bytes", ullSize);
	AfxMessageBox(strMsg);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Config 폴더에서 검색된 처음부터 지정된 개수만큼의 패키지 파일의 이름을 리스트로 만들어서 반환한다. \n
/// @param (int) nCnt : (in) 가져올 개수
/// @param (list<string>&) listPackage : (in/out) 검색된 패키지명 리스트
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleanTestDlg::GetPackageName(int nCnt, list<string>& listPackage)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, cDrive, cPath, cFilename, cExt);

	CString strConfig, strTitle;
	strConfig.Format(_T("%s%sconfig\\*.ini"), cDrive, cPath);

	CFileFind ff;
	BOOL bRet = ff.FindFile(strConfig);
	while(bRet)
	{
		bRet = ff.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(ff.IsDots() == TRUE)
		{
			continue;
		}//if

		//디렉토리인 경우 무시
		if(ff.IsDirectory() == TRUE)
		{
			continue;
		}//if

		strTitle = ff.GetFileTitle();
		strTitle.MakeLower();

		//컨텐츠 패키지파일의 처음에 "__" 문자가 있는 파일은 삭제하면 안된다.
		if(strTitle.GetAt(0) == '_' && strTitle.GetAt(1) == '_')
		{
			continue;
		}//if

		if(nCnt > 0)
		{
			listPackage.push_back((LPSTR)(LPCSTR)ff.GetFileTitle());
			nCnt--;
		}//if

		if(nCnt == 0)
		{
			ff.Close();
			return;
		}//if
	}//while
	ff.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ENC 폴더에서 검색된 처음부터 지정된 개수만큼의 파일과 폴더의 이름을 리스트로 만들어서 반환한다. \n
/// @param (int) nCnt : (in) 가져올 개수
/// @param (list<string>&) listFiles : (in/out) 검색된 파일과 폴더 리스트
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleanTestDlg::GetFileName(int nCnt, list<string>& listFiles)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, cDrive, cPath, cFilename, cExt);

	CString strFile, strTitle;
	strFile.Format(_T("%s%s..\\..\\Contents\\ENC\\*.*"), cDrive, cPath);

	CFileFind ff;
	BOOL bRet = ff.FindFile(strFile);
	while(bRet)
	{
		bRet = ff.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(ff.IsDots() == TRUE)
		{
			continue;
		}//if

		if(nCnt > 0)
		{
			listFiles.push_back((LPSTR)(LPCSTR)ff.GetFileName());
			nCnt--;
		}//if

		if(nCnt == 0)
		{
			ff.Close();
			return;
		}//if
	}//while
	ff.Close();
}
