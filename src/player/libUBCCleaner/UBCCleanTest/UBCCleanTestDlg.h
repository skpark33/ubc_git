// UBCCleanTestDlg.h : 헤더 파일
//

#pragma once

#include <list>
using namespace std;


// CUBCCleanTestDlg 대화 상자
class CUBCCleanTestDlg : public CDialog
{
// 생성입니다.
public:
	CUBCCleanTestDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCCLEANTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedCleanBtn();
	afx_msg void OnBnClickedExcludeCleanBtn();
	afx_msg void OnBnClickedFileCleanBtn();
	afx_msg void OnBnClickedAllCleanBtn();
	DECLARE_MESSAGE_MAP()

public:
	void  GetPackageName(int nCnt, list<string>& listPackage);	///<Config 폴더에서 검색된 처음부터 지정된 개수만큼의 패키지 파일의 이름을 리스트로 만들어서 반환한다.
	void  GetFileName(int nCnt, list<string>& listFiles);		///<ENC 폴더에서 검색된 처음부터 지정된 개수만큼의 파일과 폴더의 이름을 리스트로 만들어서 반환한다.
};
