/************************************************************************************/
/*! @file DeleteInfo.h
	@brief 삭제될 파일의 정보를 갖는 클래스 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2011/10/31\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2011/10/31:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#pragma once

#include <afxtempl.h>

//! 삭제될 파일의 정보를 갖는 클래스
/*!
*/
class CDeleteInfo
{
public:
	CDeleteInfo();							///<생성자
	virtual ~CDeleteInfo();					///<소멸자


	CDeleteInfo& operator= (const CDeleteInfo& clsInfo)
	{
		m_strFileName		= clsInfo.m_strFileName;
		m_strLocation		= clsInfo.m_strLocation;
		m_ullSize			= clsInfo.m_ullSize;

		return *this;
	}

	bool operator== (CDeleteInfo& clsInfo)
	{
		if(m_strFileName		!= clsInfo.m_strFileName)	return false;
		if(m_strLocation		!= clsInfo.m_strLocation)	return false;
		if(m_ullSize			!= clsInfo.m_ullSize)		return false;

		return true;
	}


	CString		m_strFileName;		///<파일의 이름
	CString		m_strLocation;		///<파일의 상대 위치
	ULONGLONG	m_ullSize;			///<파일의 크기
};



class CDeleteInfoArray : public CArray<CDeleteInfo*, CDeleteInfo*>
{
public:
	CDeleteInfoArray();					///<생성자
	virtual ~CDeleteInfoArray();		///<소멸자

	int		Add(CDeleteInfo* pInfo);		///<새로운 엘리먼트 추가(중복된 element가 들어가지 않도록 재정의)
	void	Clear(void);					///<배열을 정리한다.
};
