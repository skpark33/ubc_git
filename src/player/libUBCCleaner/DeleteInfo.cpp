/************************************************************************************/
/*! @file DeleteInfo.cpp
	@brief 삭제될 파일의 정보를 갖는 클래스 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2011/10/31\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2011/10/31:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "StdAfx.h"
#include "DeleteInfo.h"


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CDeleteInfo::CDeleteInfo(void)
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CDeleteInfo::~CDeleteInfo(void)
{
}


/////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CDeleteInfoArray::CDeleteInfoArray()
{
	CArray::CArray();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CDeleteInfoArray::~CDeleteInfoArray()
{
	Clear();
	CArray::~CArray();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 새로운 엘리먼트 추가(중복된 element가 들어가지 않도록 재정의) \n
/// @param (CDeleteInfo*) pInfo : (in) 새로 추가하려는 노드
/// @return 새로 추가된 노드의 인덱스 \n
/////////////////////////////////////////////////////////////////////////////////
int CDeleteInfoArray::Add(CDeleteInfo* pInfo)
{
	CDeleteInfo* pExist = NULL;
	for(int i=0; i<GetCount(); i++)
	{
		pExist = (CDeleteInfo*)CArray::GetAt(i);
		if((*pInfo) == (*pExist))
		{
			return i;
		}//if
	}//if

	int nRet = CArray::Add(pInfo);

	return nRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 배열을 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDeleteInfoArray::Clear()
{
	CDeleteInfo* pFile = NULL;
	for(int i=0; i<GetCount(); i++)
	{
		pFile = GetAt(i);
		delete pFile;
	}//for
	RemoveAll();
}
