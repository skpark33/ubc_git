/************************************************************************************/
/*! @file UBCCleaner.cpp
	@brief 단말에서 사용되지 않는 컨텐츠패키지를 정리하는 기능을 하는 클래스 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2011/10/17\n
	
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2011/10/17:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "StdAfx.h"
#include "UBCCleaner.h"
#include "Dbghelp.h"
#include "shlobj.h"
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciAny.h>
#include "shlwapi.h"
#include <ci/libDebug/ciDebug.h>
//#include "Shobjidl.h"

//#define PATH_CONFIG			_T("SQISoft\\UTV1.0\\execute\\config")	///<컨텐츠패키지 경로
//#define PATH_CONTENTS		_T("SQISoft\\Contents\\ENC")				///<컨텐츠 경로
//#define PATH_TEMP			_T("SQISoft\\Contents\\Temp")				///<임시 경로
//#define PATH_LOG			_T("SQISoft\\UTV1.0\\execute\\log")			///<log 경로
#define PATH_CONFIG			_T("config")								///<컨텐츠패키지 경로
#define PATH_CONTENTS		_T("..\\..\\Contents\\ENC")					///<컨텐츠 경로
#define PATH_TEMP			_T("..\\..\\Contents\\Temp")				///<임시 경로
#define PATH_LOG			_T("log")									///<log 경로
#define PATH_DATA			_T("data")									///<log 경로

#define PATH_WATSON			_T("C:\\Documents and Settings\\All Users\\Application Data\\Microsoft\\Dr Watson") ///<Dr Watson의 경로


#define	READ_PACKAGE		(mngProfile.GetProfileString)
#define WRITE_LOG			(m_mngLog.WriteProfileString)
#define NAME_LOG			_T("UBCClean.log")


#define BUF_SIZE			(1024*100)		

ciSET_DEBUG(10, "libUBCCleaner");


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CUBCCleaner::CUBCCleaner()
: m_ullDeletedSize(0)
, m_strConfigPath(_T(""))
, m_strContentsPath(_T(""))
, m_strTempPath(_T(""))
, m_strLogPath(_T(""))
, m_strDataPath(_T(""))
, m_strCleanLogPath(_T(""))
, m_nCutLevel(2)
, m_nDeleteCount(0)
{

}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CUBCCleaner::~CUBCCleaner()
{
	m_aryUseContents.Clear();
	m_mapUseContentsPackage.RemoveAll();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Clean 작업을 초기화 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::InitClean()
{
	ciDEBUG(1, ("Init clean"));
	//기본경로 설정
	SetPath();

	//이전의 로그파일을 백업하고 만든다.
	if(_access(m_strCleanLogPath+".bak", 0) == 0)	//.bak ==> .bak.bak
	{
		::MoveFileEx(m_strCleanLogPath+".bak", m_strCleanLogPath+".bak.bak", MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING);
	}//if

	if(_access(m_strCleanLogPath, 0) == 0)			//.ini ==> .bak
	{
		::MoveFileEx(m_strCleanLogPath, m_strCleanLogPath+".bak", MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING);
	}//if

	//::DeleteFile(m_strCleanLogPath);
	m_mngLog.Write(m_strCleanLogPath);

	//시작 시간을 기록
	CTime tmNow = CTime::GetCurrentTime();
	CString strNow = tmNow.Format(_T("%Y-%m-%d %H:%M:%S"));
	WRITE_LOG(_T("ROOT"), _T("StartTime"), strNow);

	m_ullDeletedSize = 0;
	m_nDeleteCount = 0;
	m_aryUseContents.Clear();
	m_mapUseContentsPackage.RemoveAll();
	m_aryDeleteFile.RemoveAll();
	m_aryDeletePackage.RemoveAll();
	m_aryDeleteContents.Clear();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Clean 작업을 마무리 한다. \n
/// @param (ULONGLONG) ullSize : (in) 삭제된 파일의 크기
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::EndClean()
{
	CString str;

	//삭제된파일의 수
	str.Format(_T("%d"), m_nDeleteCount);
	WRITE_LOG(_T("ROOT"), _T("DeletedCount"), str);

	//삭제된 전체 사이즈
	str.Format(_T("%I64u"), m_ullDeletedSize);
	WRITE_LOG(_T("ROOT"), _T("DeletedSize"), str);

	//종료 시간을 기록
	CTime tmNow = CTime::GetCurrentTime();
	CString strNow = tmNow.Format(_T("%Y-%m-%d %H:%M:%S"));
	WRITE_LOG(_T("ROOT"), _T("EndTime"), strNow);

	m_mngLog.Write(m_strCleanLogPath);

	m_mapUseContentsPackage.RemoveAll();
	m_aryUseContents.Clear();
	m_aryDeleteFile.RemoveAll();
	m_aryDeletePackage.RemoveAll();
	m_aryDeleteContents.Clear();

	//ENC 폴더의 비어있는 폴더들을 삭제해 준다.
	RemoveEmptyENC();
	/*
	//ENC 폴더 확인
	m_strContentsPath.Replace(_T("/"), _T("\\"));
	::MakeSureDirectoryPathExists(m_strContentsPath + _T("\\"));
	*/
	ciDEBUG(1, ("End clean"));
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일을 삭제한다. \n
/// @param (CString) strFileName : (in) 삭제하려는 파일명
/// @param (CString) strPath : (in) 삭제하려는 파일의 경로
/// @param (ULONGLONG) ullSize : (in) 삭제하려는 파일의 크기
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::RemoveFile(CString strFileName, CString strPath, ULONGLONG ullSize)
{
	WRITE_LOG(strFileName, _T("Path"), strPath);
	if(::DeleteFile(strPath))
	{
		//ciDEBUG(1, ("Delete file : %s", strPath));
		//WRITE_LOG(strFileName, _T("Path"), strPath);
		CString strSize;
		strSize.Format(_T("%I64u"), ullSize);
		WRITE_LOG(strFileName, _T("Size"), strSize);
		WRITE_LOG(strFileName, _T("Result"), _T("Success"));
		m_ullDeletedSize += ullSize;
		m_nDeleteCount++;
	}
	else
	{
		WRITE_LOG(strFileName, _T("Result"), _T("Fail"));
		ciERROR(("Fail to delete file : %s", strPath));
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UBC의 각 경로를 설정한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::SetPath()
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, cDrive, cPath, cFilename, cExt);

	m_strConfigPath.Format(_T("%s%s%s"), cDrive, cPath, PATH_CONFIG);
	m_strContentsPath.Format(_T("%s%s%s"), cDrive, cPath, PATH_CONTENTS);
	m_strTempPath.Format(_T("%s%s%s"), cDrive, cPath, PATH_TEMP);
	m_strLogPath.Format(_T("%s%s%s"), cDrive, cPath, PATH_LOG);
	m_strDataPath.Format(_T("%s%s%s"), cDrive, cPath, PATH_DATA);

	CFileStatus status;
	CFile::GetStatus(m_strConfigPath, status);
	m_strConfigPath = status.m_szFullName;
	ciDEBUG(1, ("Config path = %s", m_strConfigPath));

	CFile::GetStatus(m_strContentsPath, status);
	m_strContentsPath = status.m_szFullName;
	ciDEBUG(1, ("Contents path = %s", m_strContentsPath));

	CFile::GetStatus(m_strTempPath, status);
	m_strTempPath = status.m_szFullName;
	ciDEBUG(1, ("Temp path = %s", m_strTempPath));

	CFile::GetStatus(m_strLogPath, status);
	m_strLogPath = status.m_szFullName;
	ciDEBUG(1, ("Log path = %s", m_strLogPath));

	CFile::GetStatus(m_strDataPath, status);
	m_strDataPath = status.m_szFullName;
	ciDEBUG(1, ("Data path = %s", m_strDataPath));

	m_strCleanLogPath.Format(_T("%s\\%s"), m_strLogPath, NAME_LOG);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠 파일의 location필드값을 잘라내는 level을 설정한다. \n
/// @param (int) nLevel : (in) 잘라내는 level
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::SetCutLocationLevel(int nLevel)
{
	m_nCutLevel = nLevel;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 패키지를 제외하고 단말을 정리한다. \n
/// @param (list<string>&) listExcludePackage : (in) 정리대상에서 제외하는(시스템에 남아야하는) 컨텐츠패키지의 리스트
/// @param (bool) bCleanSystem : (in) 시스템 정리를 같이할지 여부
/// @return <형: ULONGLONG> \n
///			<실제삭제한 총 byte> \n
/////////////////////////////////////////////////////////////////////////////////
ULONGLONG CUBCCleaner::CleanExcludePackage(list<string>& listExcludePackage, bool bCleanSystem)
{
	ciDEBUG(1, ("Begin CleanExcludePackage()"));

	InitClean();

	DeleteTemp();		//임시경로의 파일들을 삭제한다.
	DeleteLog();		//log 폴더를 정리한다.
	DeleteRecycleBin();	//휴지통을 비운다.

	if(bCleanSystem)
	{
		CleanSystem();
	}//if

	//입력받은 패키지리스트를 사용하는 패키지 맵에 추가
	WRITE_LOG(_T("ROOT"), _T("Mode"), _T("ExcludePackage"));
	CString str, strPackageName, strPath;
	str.Format(_T("%d"), listExcludePackage.size());
	WRITE_LOG(_T("ROOT"), _T("PackageCount"), str);
	str = _T("");
	string strList;
	for(std::list<string>::iterator it = listExcludePackage.begin(); it != listExcludePackage.end(); ++it)
	{
		strList = (*it);
		strPackageName = strList.c_str();
		MakeLower(strPackageName);
		strPath = m_strConfigPath;
		strPath += "\\";
		strPath += strPackageName;
		strPath += ".ini";
		//사용하는 패키지 맵에 추가
		m_mapUseContentsPackage.SetAt(strPackageName, strPath);
		str += strPackageName;
		str += _T(", ");
	}//for
	str.Trim();
	str.TrimRight(_T(","));
	WRITE_LOG(_T("ROOT"), _T("PackageList"), str);
	//ciDEBUG(1, ("Package list = %s", str));

	//입력 받아온 패키지 리스트가 없다면...
	if(m_mapUseContentsPackage.GetCount() == 0)
	{
		ciWARN(("Empty exclude package list"));
		return m_ullDeletedSize;
	}//if

	//단말의 컨텐츠패키지 중 사용자가 만든 패키지를 사용하는 패키지로 추가
	AddLocalContentsPackage();

	//사용하는 패키지들로부터 사용하는 컨텐츠 리스트 생성
	MakeUseContentsList();

	DeleteExcludeConfig();			//사용중인것을 제외한 컨텐츠패키지 파일을 삭제한다.
	DeleteExcludeContents();		//사용중인것을 제외한 컨텐츠를 삭제한다.

	EndClean();
	ciDEBUG(1, ("End CleanExcludePackage()"));
	return m_ullDeletedSize;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 패키지를 단말에서 정리한다. \n
/// @param (list<string>&) listDeletePackage : (in) 정리대상(시스템에서 삭제하여야 하는) 컨텐츠패키지의 리스트
/// @param (bool) bCleanSystem : (in) 시스템 정리를 같이할지 여부
/// @return <형: ULONGLONG> \n
///			<실제삭제한 총 byte> \n
/////////////////////////////////////////////////////////////////////////////////
ULONGLONG CUBCCleaner::CleanDeletePackage(list<string>& listDeletePackage, bool bCleanSystem)
{
	ciDEBUG(1, ("Begin CleanContentsPackage()"));

	InitClean();

	DeleteTemp();		//임시경로의 파일들을 삭제한다.
	DeleteLog();		//log 폴더를 정리한다.
	DeleteRecycleBin();	//휴지통을 비운다.

	if(bCleanSystem)
	{
		CleanSystem();
	}//if

	//입력받은 패키지리스트를 제거하여야 하는 패키지 맵에 추가
	WRITE_LOG(_T("ROOT"), _T("Mode"), _T("DeletePackage"));
	CString str, strPackageName, strPath;
	str.Format(_T("%d"), listDeletePackage.size());
	WRITE_LOG(_T("ROOT"), _T("PackageCount"), str);
	str = _T("");
	string strList;
	for(std::list<string>::iterator it = listDeletePackage.begin(); it != listDeletePackage.end(); ++it)
	{
		strList = (*it);
		strPackageName = strList.c_str();
		MakeLower(strPackageName);
		strPath = m_strConfigPath;
		strPath += "\\";
		strPath += strPackageName;
		strPath += ".ini";
		//삭제하려는 패키지 배열에 추가
		m_aryDeletePackage.Add(strPath);
		str += strPackageName;
		str += _T(", ");
	}//for
	str.Trim();
	str.TrimRight(_T(","));
	WRITE_LOG(_T("ROOT"), _T("PackageList"), str);
	//ciDEBUG(1, ("Package list = %s", str));

	//입력받은 패키지 리스트가 없다면...
	if(m_aryDeletePackage.GetCount() == 0)
	{
		ciWARN(("Empty delete package list"));
		return m_ullDeletedSize;
	}//if

	//삭제하려는 패키지들로부터 삭제하는 컨텐츠 리스트 생성
	MakeDeleteContentsList();

	DeleteConfig();			//컨텐츠패키지 파일을 삭제한다.
	DeleteContents();		//컨텐츠를 삭제한다.

	EndClean();
	ciDEBUG(1, ("End CleanContentsPackage()"));
	return m_ullDeletedSize;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 파일(폴더)를 단말에서 정리한다. \n
/// @param ((list<string>&) listFiles : (in) 단말에서 삭제할 파일(폴더) 리스트
/// @param (bool) bCleanSystem : (in) 시스템 정리를 같이할지 여부
/// @return <형: ULONGLONG> \n
///			<실제삭제한 총 byte> \n
/////////////////////////////////////////////////////////////////////////////////
ULONGLONG CUBCCleaner::CleanDeleteFiles(list<string>& listFiles, bool bCleanSystem)
{
	ciDEBUG(1, ("Begin CleanDeleteFiles()"));

	InitClean();

	DeleteTemp();		//임시경로의 파일들을 삭제한다.
	DeleteLog();		//log 폴더를 정리한다.
	DeleteRecycleBin();	//휴지통을 비운다.

	if(bCleanSystem)
	{
		CleanSystem();
	}//if

	//입력받은 패키지리스트를 제거하여야 하는 패키지 맵에 추가
	WRITE_LOG(_T("ROOT"), _T("Mode"), _T("DeleteFiles"));
	CString str, strDelFile;
	str.Format(_T("%d"), listFiles.size());
	WRITE_LOG(_T("ROOT"), _T("FileCount"), str);
	str = _T("");
	string strList;
	for(std::list<string>::iterator it = listFiles.begin(); it != listFiles.end(); ++it)
	{
		strList = (*it);
		strDelFile = strList.c_str();
		MakeLower(strDelFile);
		//삭제하는 파일들 배열에 추가
		m_aryDeleteFile.Add(strDelFile);
		str += strDelFile;
		str += _T(", ");
	}//for
	str.Trim();
	str.TrimRight(_T(","));
	WRITE_LOG(_T("ROOT"), _T("FileList"), str);
	//ciDEBUG(1, ("File list = %s", str));

	//입력받은 패키지 리스트가 없다면...
	if(m_aryDeleteFile.GetCount() == 0)
	{
		ciWARN(("Empty file list"));
		return m_ullDeletedSize;
	}//if

	//지정된 파일들을 ENC 폴더에서 지워준다.
	DeleteENCFiles();

	EndClean();
	ciDEBUG(1, ("End CleanDeleteFiles()"));
	return m_ullDeletedSize;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ENC 폴더의 *.ini와 *xml를 제외하고 삭제한다. \n
/// @param (bool) bCleanSystem : (in) 시스템 정리를 같이할지 여부
/// @return <형: ULONGLONG> \n
///			<실제삭제한 총 byte> \n
/////////////////////////////////////////////////////////////////////////////////
ULONGLONG CUBCCleaner::CleanAll(bool bCleanSystem)
{
	ciDEBUG(1, ("Begin CleanAll()"));

	InitClean();

	DeleteTemp();		//임시경로의 파일들을 삭제한다.
	DeleteLog();		//log 폴더를 정리한다.
	DeleteRecycleBin();	//휴지통을 비운다.

	if(bCleanSystem)
	{
		CleanSystem();
	}//if

	//입력받은 패키지리스트를 제거하여야 하는 패키지 맵에 추가
	WRITE_LOG(_T("ROOT"), _T("Mode"), _T("CleanAll"));
	
	m_aryUseContents.Clear();
	m_mapUseContentsPackage.RemoveAll();

	//ENC 폴더 모두 삭제...
	ClearENC();

	EndClean();
	ciDEBUG(1, ("End CleanAll()"));
	return m_ullDeletedSize;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 단말의 컨텐츠패키지 중 사용자가 만든 패키지를 사용하는 패키지로 추가. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::AddLocalContentsPackage()
{
	//Config 폴더안의 모든 *.ini 파일들의 목록을 만든다.
	BOOL bRet = FALSE;
	CFileFind find;
	CString strServerRegist, strTitle;
	CString strConfig = m_strConfigPath;
	strConfig.Append(_T("\\*.ini"));
	CProfileManager mngProfile;

	bRet = find.FindFile(strConfig);
	while(bRet)
	{
		bRet = find.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		//디렉토리인 경우 무시
		if(find.IsDirectory() == TRUE)
		{
			continue;
		}//if

		//로컬에 존재하는 컨텐츠 패키지 중에서 로컬에서 만든것은 사용하는 패키지로 추가한다.
		if(!mngProfile.Read(find.GetFilePath()))
		{
			continue;
		}//if

		strTitle = find.GetFileTitle();
		MakeLower(strTitle);
		strServerRegist = READ_PACKAGE(_T("Host"), _T("networkuse"));
		if(strServerRegist == _T("0"))
		{
			m_mapUseContentsPackage.SetAt(strTitle, find.GetFilePath());
		}//if
	}//while
	find.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용하는 컨텐츠 리스트를 만든다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::MakeUseContentsList()
{
	POSITION pos;
	CString strKey, strPath, strContentsList, strETCList, token, strName, strLocation;
	CProfileManager mngProfile;
	int nPos = 0;
	ULONGLONG ulVolume = 0;
	CDeleteInfo* pInfoCon = NULL;

	//사용하는 컨텐츠 패키지 목록으로부터 사용하는 컨텐츠 리스트 구성
	for(pos = m_mapUseContentsPackage.GetStartPosition(); pos != NULL; )
	{
		m_mapUseContentsPackage.GetNextAssoc(pos, strKey, strPath);

		if(!mngProfile.Read(strPath))
		{
			continue;
		}//if

		//Contents List
		strContentsList = READ_PACKAGE(_T("Host"), _T("ContentsList"));
		nPos = 0;
		token = strContentsList.Tokenize(_T(","), nPos);
		while(token != _T(""))
		{
			token.Trim();

			strName = READ_PACKAGE(token, _T("filename"));
			ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));
			strLocation = READ_PACKAGE(token, _T("location"));

			if(strName.GetLength() != 0 && ulVolume != 0)
			{
				pInfoCon = new CDeleteInfo;
				pInfoCon->m_strFileName = strName;
				pInfoCon->m_strLocation = strLocation;
				pInfoCon->m_ullSize = ulVolume;

				m_aryUseContents.Add(pInfoCon);
			}//if

			token = strContentsList.Tokenize(_T(","), nPos);
		}//while

		//ETC List
		strETCList = READ_PACKAGE(_T("Host"), _T("EtcList"));
		nPos = 0;
		token = strETCList.Tokenize(_T(","), nPos);
		while(token != _T(""))
		{
			token.Trim();

			strName = READ_PACKAGE(token, _T("filename"));
			ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));
			strLocation = READ_PACKAGE(token, _T("location"));

			if(strName.GetLength() != 0 && ulVolume != 0)
			{
				pInfoCon = new CDeleteInfo;
				pInfoCon->m_strFileName = strName;
				pInfoCon->m_strLocation = strLocation;
				pInfoCon->m_ullSize = ulVolume;

				m_aryUseContents.Add(pInfoCon);
			}//if

			token = strETCList.Tokenize(_T(","), nPos);
		}//while
	}//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 삭제하는 컨텐츠 리스트를 만든다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::MakeDeleteContentsList()
{
	CString strPath, strContentsList, strETCList, token, strName, strLocation;
	CProfileManager mngProfile;
	int nPos = 0;
	ULONGLONG ulVolume = 0;
	CDeleteInfo* pInfoCon = NULL;

	//삭제하려는 컨텐츠 패키지 목록으로부터 삭제하는 컨텐츠 리스트 구성
	for(int i=0; i<m_aryDeletePackage.GetCount(); i++)
	{
		strPath = m_aryDeletePackage.GetAt(i);

		if(!mngProfile.Read(strPath))
		{
			continue;
		}//if

		//Contents List
		strContentsList = READ_PACKAGE(_T("Host"), _T("ContentsList"));
		nPos = 0;
		token = strContentsList.Tokenize(_T(","), nPos);
		while(token != _T(""))
		{
			token.Trim();

			strName = READ_PACKAGE(token, _T("filename"));
			ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));
			strLocation = READ_PACKAGE(token, _T("location"));

			if(strName.GetLength() != 0 && ulVolume != 0)
			{
				pInfoCon = new CDeleteInfo;
				pInfoCon->m_strFileName = strName;
				pInfoCon->m_strLocation = strLocation;
				pInfoCon->m_ullSize = ulVolume;

				m_aryDeleteContents.Add(pInfoCon);
			}//if

			token = strContentsList.Tokenize(_T(","), nPos);
		}//while

		//ETC List
		strETCList = READ_PACKAGE(_T("Host"), _T("EtcList"));
		nPos = 0;
		token = strETCList.Tokenize(_T(","), nPos);
		while(token != _T(""))
		{
			token.Trim();

			strName = READ_PACKAGE(token, _T("filename"));
			ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));
			strLocation = READ_PACKAGE(token, _T("location"));

			if(strName.GetLength() != 0 && ulVolume != 0)
			{
				pInfoCon = new CDeleteInfo;
				pInfoCon->m_strFileName = strName;
				pInfoCon->m_strLocation = strLocation;
				pInfoCon->m_ullSize = ulVolume;

				m_aryDeleteContents.Add(pInfoCon);
			}//if

			token = strETCList.Tokenize(_T(","), nPos);
		}//while
	}//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용중인것을 제외한 컨텐츠패키지 파일들을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteExcludeConfig()
{
	//Config 폴더안의 모든 *.ini 파일들을 조회하여 사용하지 않는 패키지는 삭제
	BOOL bRet = FALSE;
	CFileFind find;
	CString strPath, strTitle, strVal;
	CString strConfig = m_strConfigPath;
	strConfig.Append(_T("\\*.ini"));
	struct _stati64 stStat;

	bRet = find.FindFile(strConfig);
	while(bRet)
	{
		bRet = find.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		//디렉토리인 경우 무시
		if(find.IsDirectory() == TRUE)
		{
			continue;
		}//if

		strTitle = find.GetFileTitle();
		MakeLower(strTitle);

		//컨텐츠 패키지파일의 처음에 "__" 문자가 있는 파일은 삭제하면 안된다.
		if(strTitle.GetAt(0) == '_' && strTitle.GetAt(1) == '_')
		{
			continue;
		}//if

		strPath = find.GetFilePath();
		if(!m_mapUseContentsPackage.Lookup(strTitle, strVal))
		{
			if(_stati64(strPath, &stStat) == 0)
			{
				//파일삭제
				//ciDEBUG(1, ("Trying delete contentspackage file = %s", strPath));
				RemoveFile(strTitle, strPath, stStat.st_size);
			}//if
		}//if		
	}//while
	find.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠패키지 파일들을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteConfig()
{
	//Config 폴더안의 모든 *.ini 파일들을 조회하여 사용하지 않는 패키지는 삭제
	BOOL bRet = FALSE;
	CFileFind find;
	CString strPath, strTitle, strConfig;
	struct _stati64 stStat;

	for(int i=0; i<m_aryDeletePackage.GetCount(); i++)
	{
		strConfig = m_aryDeletePackage.GetAt(i);
		
		bRet = find.FindFile(strConfig);
		while(bRet)
		{
			bRet = find.FindNextFile();

			// . or .. 인 경우 무시 한다.
			if(find.IsDots() == TRUE)
			{
				continue;
			}//if

			//디렉토리인 경우 무시
			if(find.IsDirectory() == TRUE)
			{
				continue;
			}//if

			strTitle = find.GetFileTitle();
			MakeLower(strTitle);

			//컨텐츠 패키지파일의 처음에 "__" 문자가 있는 파일은 삭제하면 안된다.
			if(strTitle.GetAt(0) == '_' && strTitle.GetAt(1) == '_')
			{
				continue;
			}//if

			strPath = find.GetFilePath();
			if(_stati64(strPath, &stStat) == 0)
			{
				//파일삭제
				//ciDEBUG(1, ("Trying delete contentspackage file = %s", strPath));
				RemoveFile(strTitle, strPath, stStat.st_size);
			}//if
		}//while
	}//for
	find.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용중인것을 제외한 컨텐츠파일을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteExcludeContents()
{
	BOOL bRet = FALSE;
	CFileFind find;
	CString strFindPath, strPath, strName, strExt;
	CString strCon = m_strContentsPath;
	strCon.Append(_T("\\*.*"));
	CDeleteInfo* pInfo = NULL;
	struct _stati64 stStat;
	bool bUse = false;

	bRet = find.FindFile(strCon);
	while(bRet)
	{
		bRet = find.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		//디렉토리인 경우 
		if(find.IsDirectory() == TRUE)
		{
			if(IsTarDirectory(find.GetFilePath()))
			{
				//Tar 컨텐츠 삭제
				DeleteExcludeTarContents(find.GetFilePath());
			}
			else
			{
				//부속파일(ENC안의 서브폴더) 컨텐츠 삭제
				DeleteExcludeSubContents(find.GetFilePath());
			}//if
			continue;
		}//if

		//ini와 xml 파일은 삭제하지 않는다.
		strName = find.GetFileName();
		strFindPath = find.GetFilePath();
		strExt = GetExtension(strName);
		if(strExt.CompareNoCase(_T("ini")) == 0 || strExt.CompareNoCase(_T("xml")) == 0)
		{
			continue;
		}//if

		//사용중인 컨텐츠인지 검색
		bUse = false;
		for(int i=0; i<m_aryUseContents.GetCount(); i++)
		{
			pInfo = m_aryUseContents.GetAt(i);
			//사용중인 컨텐츠와 이름, 경로가 같다면 지우지 않는다.
			strPath = GetLocalPath(pInfo->m_strLocation, pInfo->m_strFileName);
			if(strName.CompareNoCase(pInfo->m_strFileName) == 0
				&& strFindPath.CompareNoCase(strPath) == 0)
			{
				bUse = true;
				break;
			}//if
		}//for

		//사용중이 아니면 삭제
		if(!bUse)
		{
			if(_stati64(strFindPath, &stStat) == 0)
			{
				//파일삭제
				//ciDEBUG(1, ("Trying delete contents file = %s", strFindPath));
				RemoveFile(find.GetFileTitle(), strFindPath, stStat.st_size);
			}//if
		}//if
	}//while
	find.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용중인것을 제외한 부속(ENC안의 서브폴더) 컨텐츠파일을 삭제한다. \n
/// @param (CString) strSubPath : (in) 파일을 삭제할 폴더의 경로
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteExcludeSubContents(CString strSubPath)
{
	BOOL bRet = FALSE;
	CFileFind find;
	CString strFindPath, strPath, strName, strExt;
	strSubPath.Append(_T("\\*.*"));
	CDeleteInfo* pInfo = NULL;
	struct _stati64 stStat;
	bool bUse = false;

	bRet = find.FindFile(strSubPath);
	while(bRet)
	{
		bRet = find.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		//디렉토리인 경우 재귀호출
		if(find.IsDirectory() == TRUE)
		{
			DeleteExcludeSubContents(find.GetFilePath());
			continue;
		}//if

		strName = find.GetFileName();
		strFindPath = find.GetFilePath();

		//사용중인 컨텐츠인지 검색
		bUse = false;
		for(int i=0; i<m_aryUseContents.GetCount(); i++)
		{
			pInfo = m_aryUseContents.GetAt(i);
			//사용중인 컨텐츠와 이름, 경로가 같다면 지우지 않는다.
			strPath = GetLocalPath(pInfo->m_strLocation, pInfo->m_strFileName);
			if(strName.CompareNoCase(pInfo->m_strFileName) == 0
				&& strFindPath.CompareNoCase(strPath) == 0)
			{
				bUse = true;
				break;
			}//if
		}//for

		//사용중이 아니면 삭제
		if(!bUse)
		{
			if(_stati64(strFindPath, &stStat) == 0)
			{
				//파일삭제
				//ciDEBUG(1, ("Trying delete contents file = %s", strFindPath));
				RemoveFile(find.GetFileTitle(), strFindPath, stStat.st_size);
			}//if
		}//if
	}//while
	find.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용중인것을 제외한 Tar 컨텐츠파일을 삭제한다. \n
/// @param (CString) strTarPath : (in) 파일을 삭제할 폴더의 경로
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteExcludeTarContents(CString strTarPath)
{
	CDeleteInfo* pInfo = NULL;
	CString strPath, strTarFile, strName;
	strTarFile = strTarPath;
	strTarFile += ".tar";
	bool bUse = false;

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(strTarFile, cDrive, cPath, cFilename, cExt);
	strName = cFilename;

	//사용중인 컨텐츠인지 검색
	bUse = false;
	for(int i=0; i<m_aryUseContents.GetCount(); i++)
	{
		pInfo = m_aryUseContents.GetAt(i);
		//사용중인 컨텐츠와 경로가 같다면 지우지 않는다.
		strPath = GetLocalPath(pInfo->m_strLocation, pInfo->m_strFileName);
		if(strTarFile.CompareNoCase(strPath) == 0)
		{
			bUse = true;
			break;
		}//if
	}//for

	//사용중이 아니면 삭제
	if(!bUse)
	{
		WRITE_LOG(strName, _T("Path"), strTarPath);
		if(!DeleteDirectory(strTarPath))
		{
			WRITE_LOG(strName, _T("Result"), _T("Fail"));
			ciERROR(("Fail to delete folder : %s", strTarPath));
		}
		else
		{
			WRITE_LOG(strName, _T("Size"), _T("Tar Directory"));
			WRITE_LOG(strName, _T("Result"), _T("Success"));
		}//if
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠파일을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteContents()
{
	BOOL bRet = FALSE;
	CFileFind find;
	CString strFindPath, strPath, strName, strExt, strCon, strTar;
	CDeleteInfo* pInfo = NULL;
	struct _stati64 stStat;
	bool bUse = false;

	for(int i=0; i<m_aryDeleteContents.GetCount(); i++)
	{
		pInfo = m_aryDeleteContents.GetAt(i);
		strPath = GetLocalPath(pInfo->m_strLocation, pInfo->m_strFileName);
		//Tar 파일인 경우에는 폴더를 지워주어야 한다.
		strTar = strPath.Right(4);
		if(strTar.CompareNoCase(".tar") == 0)
		{
			strTar = strPath.Left(strPath.GetLength()-4);
			strPath = strTar;
		}//if
		
		bRet = find.FindFile(strPath);
		while(bRet)
		{
			bRet = find.FindNextFile();

			// . or .. 인 경우 무시 한다.
			if(find.IsDots() == TRUE)
			{
				continue;
			}//if

			//디렉토리인 경우
			if(find.IsDirectory() == TRUE)
			{
				if(IsTarDirectory(find.GetFilePath()))
				{
					WRITE_LOG(find.GetFileTitle(), _T("Path"), find.GetFilePath());
					//Tar 폴더 삭제
					if(!DeleteDirectory(find.GetFilePath()))
					{
						WRITE_LOG(find.GetFileTitle(), _T("Result"), _T("Fail"));
						ciERROR(("Fail to delete folder : %s", find.GetFilePath()));
					}
					else
					{
						WRITE_LOG(find.GetFileTitle(), _T("Size"), _T("Tar Directory"));
						WRITE_LOG(find.GetFileTitle(), _T("Result"), _T("Success"));
					}//if
				}//if
				continue;
			}//if

			//ini와 xml 파일은 삭제하지 않는다.
			strName = find.GetFileName();
			strFindPath = find.GetFilePath();
			strExt = GetExtension(strName);
			if(strExt.CompareNoCase(_T("ini")) == 0 || strExt.CompareNoCase(_T("xml")) == 0)
			{
				continue;
			}//if

			if(_stati64(strFindPath, &stStat) == 0)
			{
				//파일삭제
				//ciDEBUG(1, ("Trying delete contents file = %s", strFindPath));
				RemoveFile(find.GetFileTitle(), strFindPath, stStat.st_size);
			}//if
		}//while
	}//for
	find.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 임시경로의 파일들을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteTemp()
{
	//ULONGLONG ullFolderSize = GetFolderSize(m_strTempPath);
	//if(ullFolderSize == 0)
	//{
	//	return;
	//}//if

	if(!DeleteDirectory(m_strTempPath))
	{
		ciWARN(("Fail to delete temp path"));
	}//if

	//임시경로가 삭제되었다면 다시 만들어준다.
	//경로에 "/"를 "\\"로 바꿔야 한다.
	m_strTempPath.Replace(_T("/"), _T("\\"));
	::MakeSureDirectoryPathExists(m_strTempPath + _T("\\"));

	//m_ullDeletedSizde += ullFolderSize;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 휴지통 파일들을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteRecycleBin()
{
	SHQUERYRBINFO si;
	si.cbSize=sizeof(si);

	//휴지통의 정보를 얻어온다.
	if(SHQueryRecycleBin(NULL, &si) != S_OK)
	{
		ciWARN(("Fail to get recyclebin info"));
		return;
	}//if

	//휴지통이 비어있다면
	if(si.i64Size <= 0)
	{
		ciDEBUG(1, ("Empty recyclebin"));
		return;
	}//if

	//휴지통 비우기
	if(SHEmptyRecycleBin(NULL, NULL, SHERB_NOCONFIRMATION |SHERB_NOPROGRESSUI |SHERB_NOSOUND) != S_OK)
	{
		WRITE_LOG(_T("RecycleBin"), _T("Result"), _T("Fail"));
		ciERROR(("Fail to delete recyclebin"));
	}
	else
	{
		WRITE_LOG(_T("RecycleBin"), _T("Path"), _T("RecycleBin"));
		CString strSize;
		strSize.Format(_T("%I64u"), si.i64Size);
		WRITE_LOG(_T("RecycleBin"), _T("Size"), strSize);
		WRITE_LOG(_T("RecycleBin"), _T("Result"), _T("Success"));
		m_ullDeletedSize += si.i64Size;
		m_nDeleteCount += si.i64NumItems;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// log 파일들을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteLog()
{
	//로그폴더안에 있는 모든 *.bak 파일들을 삭제한다.
	BOOL bRet = FALSE;
	CFileFind find;
	CString strBak = m_strLogPath;
	strBak.Append(_T("\\*.bak"));

	bRet = find.FindFile(strBak);
	while(bRet)
	{
		bRet = find.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		//디렉토리인 경우 무시
		if(find.IsDirectory() == TRUE)
		{
			continue;
		}//if

		//파일삭제
		RemoveFile(find.GetFileTitle(), find.GetFilePath(), find.GetLength());
	}//while

	//브라우져가 생성한 파일중에 로그보존기한이 지난 파일은 삭제한다.
	CString strVariablesPath = m_strDataPath;
	strVariablesPath += _T("\\UBCVariables.ini");
	char cBuffer[BUF_SIZE] = { 0x00 };	
	GetPrivateProfileString(_T("ROOT"), _T("PlayLogDayLimit"), _T(""), cBuffer, BUF_SIZE, strVariablesPath);
	int nPeriod = atoi(cBuffer);
	if(nPeriod == 0)
	{
		nPeriod = 15;
		WritePrivateProfileString(_T("ROOT"), _T("PlayLogDayLimit"), _T("15"), strVariablesPath);
	}//if

	//현재시간부터 보존기간을 뺀 시간...
	CTime tmCurrent = CTime::GetCurrentTime();
	CTimeSpan tmPeriod(nPeriod, 0, 0, 0);
	CTime tmPreserve = tmCurrent - tmPeriod;

	CString strFindPath = m_strLogPath;
	strFindPath += _T("\\*UTV_brwClient2*.log");
	bool bWorking = find.FindFile(strFindPath);
	CTime tmLastWrite;
	while(bWorking)
	{
		bWorking = find.FindNextFile();
		if(find.IsDots())
		{
			continue;
		}//if

		if(find.IsDirectory())
		{
			continue;
		}//if

		//ciDEBUG(1, ("Find log = %s", find.GetFileName()));
		find.GetLastWriteTime(tmLastWrite);
		//ciDEBUG(1, ("tmPreserve = %s", tmPreserve.Format(_T("%Y/%m/%d %H:%M:%S"))));
		//ciDEBUG(1, ("tmLastWrite = %s", tmLastWrite.Format(_T("%Y/%m/%d %H:%M:%S"))));
		if(tmLastWrite < tmPreserve)
		{
			//파일삭제
			RemoveFile(find.GetFileTitle(), find.GetFilePath(), find.GetLength());
		}//if		
	}//while
	find.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 폴더와 내부의 폴더, 파일을 삭제한다. \n
/// @param (LPCTSTR) lpDirPath : (in) 정리하려는 폴더 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCCleaner::DeleteDirectory(LPCTSTR lpDirPath)
{
	if(lpDirPath == NULL)// 경로가 없는 경우 되돌아간다.
	{  
		return false;
	}//if

	BOOL bRval = FALSE;
	int nRval = 0;
	//CString strNextDirPath = _T("");
	CString strRoot = _T("");
	CString strPath = lpDirPath;
	strPath.Append(_T("\\*.*"));
	CFileFind find;

	// 폴더가 존재 하는 지 확인 검사 
	bRval = find.FindFile(strPath);
	if(bRval == FALSE)
	{
		return bRval;
	}//if

	while(bRval)
	{
		bRval = find.FindNextFile();
		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		// Directory 일 경우
		if(find.IsDirectory())             
		{
			//strNextDirPath.Format(_T("%s\\*.*") , find.GetFilePath()); 
			// Recursion function 호출            
			DeleteDirectory(find.GetFilePath());            
		}
		else	// file일 경우
		{
			// 파일 삭제
			RemoveFile(find.GetFileTitle(), find.GetFilePath(), find.GetLength()); 
		}
	}//while

	strRoot = find.GetRoot();     
	find.Close(); 
	bRval = RemoveDirectory(strRoot);

	return bRval; 
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 폴더의 안의 파일들 사이즈를 구한다. \n
/// @param (CString) strInPath : (in) 폴더의 경로
/// @return <형: ULONGLONG> \n
///			폴더안의 파일들 사이즈의 총합 \n
/////////////////////////////////////////////////////////////////////////////////
ULONGLONG CUBCCleaner::GetFolderSize(CString strInPath)
{
	if(strInPath.IsEmpty()) return 0;

	ULONGLONG ulTotalSize = 0;

	CFileFind ff;
	BOOL bFindIt = ff.FindFile(strInPath + "\\*.*");
	while(bFindIt)
	{
		bFindIt = ff.FindNextFile();

		if(ff.IsDots()) continue;
		if(ff.IsDirectory())
		{
			ulTotalSize += GetFolderSize(ff.GetFilePath());
		}
		else
		{
			ulTotalSize += ff.GetLength();
		}//if
	}//while

	ff.Close();

	return ulTotalSize;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠 폴더를 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::ClearENC()
{
	BOOL bRet = FALSE;
	CFileFind find;
	CString strExt;
	CString strENC = m_strContentsPath;
	strENC.Append(_T("\\*.*"));

	bRet = find.FindFile(strENC);
	while(bRet)
	{
		bRet = find.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		//디렉토리인 경우
		if(find.IsDirectory() == TRUE)
		{
			//디렉토리중에 xml 이름을 갖고있는 폴더는 pop 플래시에서 사용하므로 지우면 안된다.
			if(find.GetFileTitle() != "xml")
			{
				if(!DeleteDirectory(find.GetFilePath()))
				{
					ciERROR(("Fail to delete folder : %s", find.GetFilePath()));
				}//if
			}//if
			continue;
		}//if

		strExt = GetExtension(find.GetFileName());
		if(strExt.CompareNoCase(_T("ini")) != 0 || strExt.CompareNoCase(_T("xml")) != 0)
		{
			//파일삭제
			RemoveFile(find.GetFileTitle(), find.GetFilePath(), find.GetLength());
		}//if
	}//while
	find.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠 폴더에서 지정된 파일들을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::DeleteENCFiles()
{
	BOOL bRet = FALSE;
	CFileFind find;
	CString strExt, strDel;

	for(int i=0; i<m_aryDeleteFile.GetCount(); i++)
	{
		strDel = m_strContentsPath;
		strDel.Append(_T("\\"));
		strDel.Append(m_aryDeleteFile.GetAt(i));

		bRet = find.FindFile(strDel);
		while(bRet)
		{
			bRet = find.FindNextFile();

			// . or .. 인 경우 무시 한다.
			if(find.IsDots() == TRUE)
			{
				continue;
			}//if

			//디렉토리인 경우
			if(find.IsDirectory() == TRUE)
			{
				WRITE_LOG(find.GetFileTitle(), _T("Path"), find.GetFilePath());
				if(!DeleteDirectory(find.GetFilePath()))
				{
					WRITE_LOG(find.GetFileTitle(), _T("Result"), _T("Fail"));
					ciERROR(("Fail to delete folder : %s", find.GetFilePath()));
				}
				else
				{
					WRITE_LOG(find.GetFileTitle(), _T("Size"), _T("Directory"));
					WRITE_LOG(find.GetFileTitle(), _T("Result"), _T("Success"));
				}//if
				continue;
			}//if

			strExt = GetExtension(find.GetFileName());
			if(strExt.CompareNoCase(_T("ini")) != 0 || strExt.CompareNoCase(_T("xml")) != 0)
			{
				//파일삭제
				RemoveFile(find.GetFileTitle(), find.GetFilePath(), find.GetLength());
			}//if
		}//while
	}//for
	find.Close();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일명에서 확장자를 분리하여 반환 \n
/// @param (const CString&) strFileName : (in) 파일명
/// @return <형: CString> \n
///			<파일의 확장자> \n
/////////////////////////////////////////////////////////////////////////////////
CString	CUBCCleaner::GetExtension(const CString& strFileName)
{
	int nlen = strFileName.GetLength();
	int i;
	for (i = nlen-1; i >= 0; i--)
	{
		if (strFileName[i] == '.')
		{
			return strFileName.Mid(i+1);
		}//if
	}//if

	return CString(_T(""));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 경로에서 파일명을 반환한다. \n
/// @param (const) CString& strFilePath : (in) 파일명을 구하고자하는 전체경로
/// @return <형: CString> \n
///			<파일명> \n
/////////////////////////////////////////////////////////////////////////////////
CString CUBCCleaner::GetFileName(const CString& strFilePath)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);

	TCHAR cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(strFilePath, cDrive, cPath, cFilename, cExt);

	return cFilename;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 서버의 경로로부터 local의 경로를 반환한다. \n
/// @param (LPCSTR) lpszLocation : (in) 서버의 경로
/// @param (LPCSTR) lpszFileName : (in) 파일 명
/// @return <형: CString> \n
///			<로컬의 경로> \n
/////////////////////////////////////////////////////////////////////////////////
CString CUBCCleaner::GetLocalPath(LPCSTR lpszLocation, LPCSTR lpszFileName)
{
	//일반 컨텐츠의 location 값은 /contents/<패키지명|컨텐츠 guid>/...
	//즉, 상위 2단계까지가 서버의 경로를 나타내며 그 이후의 경로는
	//플래쉬 서브폴더 등과 같은 컨텐츠에 종속된 하위 폴더경로이다.
	//로컬에서는 상위2단계 밑의 하위 경로만 취급한다.
	//==> m_nCutLevel값에 따라서 자르는 단계를 조정하도록 수정
	
	int nPos = 0;
	int nLevel = 0;
	CString strLocation = lpszLocation;
	strLocation.Replace(_T("/"), _T("\\")); 
	if(strLocation.GetLength() != 0 && m_nCutLevel > 0)
	{
		while(nLevel < m_nCutLevel)
		{
			nPos = strLocation.Find(_T("\\"), nPos+1);
			if(nPos == -1)
			{
				break;
			}//if

			nLevel++;
		}//while
	}//if
	strLocation.Delete(0, nPos+1);	

	CString strLocalPath = m_strContentsPath;
	strLocalPath += _T("\\");
	if(strLocation.GetLength() != 0)
	{
		strLocalPath += strLocation;
		if(strLocalPath[strLocalPath.GetLength()-1] != _T('\\'))
		{
			strLocalPath += _T("\\");
		}//if
	}//if
	strLocalPath += lpszFileName;

	return strLocalPath;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시스템 정리를 수행한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::CleanSystem()
{
	//Dr.Watson dump 정리
	//폴더안에 있는 모든 파일들을 삭제한다.
	BOOL bRet = FALSE;
	CFileFind find;
	CString strPath = PATH_WATSON;
	strPath.Append(_T("\\*.*"));

	bRet = find.FindFile(strPath);
	while(bRet)
	{
		bRet = find.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		//디렉토리인 경우
		if(find.IsDirectory() == TRUE)
		{
			DeleteDirectory(find.GetFilePath());
			continue;
		}//if

		//파일삭제
		RemoveFile(find.GetFileTitle(), find.GetFilePath(), find.GetLength());
	}//while

	//윈도우즈 temp 정리
	TCHAR szPath[MAX_PATH] = { 0x00 };
	if(::GetTempPath(MAX_PATH, szPath) > 0)
	{
		strPath = szPath;
		strPath.Append(_T("\\*.*"));
		bRet = find.FindFile(strPath);
		while(bRet)
		{
			bRet = find.FindNextFile();

			// . or .. 인 경우 무시 한다.
			if(find.IsDots() == TRUE)
			{
				continue;
			}//if

			//디렉토리인 경우
			if(find.IsDirectory() == TRUE)
			{
				DeleteDirectory(find.GetFilePath());
				continue;
			}//if

			//파일삭제
			RemoveFile(find.GetFileTitle(), find.GetFilePath(), find.GetLength());
		}//while
	}//if
	
	//인터넷 캐쉬정리
	if(SHGetSpecialFolderPath(NULL, szPath, CSIDL_INTERNET_CACHE, FALSE))
	{
		strPath = szPath;
		strPath.Append(_T("\\*.*"));
		bRet = find.FindFile(strPath);
		while(bRet)
		{
			bRet = find.FindNextFile();

			// . or .. 인 경우 무시 한다.
			if(find.IsDots() == TRUE)
			{
				continue;
			}//if

			//디렉토리인 경우
			if(find.IsDirectory() == TRUE)
			{
				DeleteDirectory(find.GetFilePath());
				continue;
			}//if

			//파일삭제
			RemoveFile(find.GetFileTitle(), find.GetFilePath(), find.GetLength());
		}//while
	}//if
	find.Close();
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 서버의 컨텐츠패키지리스트를 얻어온다. \n
/// @param (list<string>&) listContentsPackage : (in/out) 서버에 존재하는 컨텐츠패키지 리스트
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCCleaner::GetContentsPackageList(list<string>& listContentsPackage)
{
	cciCall aCall("bulkget");

	cciEntity aEntity("PM=*/Site=*/Program=*");
	aCall.setEntity(aEntity);

	cciAny nullAny;
	//aCall.addItem("siteId", nullAny);
	aCall.addItem("programId", nullAny);   // 가져오고자 하는 attribute 를 설정한다.  여기서는 siteId 와 programId 만 가져오는걸로 가정
	aCall.wrap();

	//유효기간이 현재이후인 패키지만을 받아온다.
	ciTime now;
	char query[1024];
	//sprintf(query,"validationDate < CAST('%s' as datetime)", now.getTimeString().c_str() );
	sprintf(query,"validationDate > CAST('%s' as datetime)", now.getTimeString().c_str() );
	aCall.addItem("whereClause", query);

	try
	{
		if(!aCall.call())
		{
			ciERROR(("Get %s fail", aEntity.toString()));
			return false;
		}//if

		//while(aCall.hasMore())
		//{
			cciReply reply;
			while(aCall.getReply(reply))
			{
				//ciDEBUG(9, ("get %s succeed", aEntity.toString()));
				//TRACE("get %s succeed\r\n", aEntity.toString());

				if(!reply.unwrap())
				{
					ciWARN(("Invalid data"));
					continue;
				}//if

				string strProgramId;
				//reply.getItem("siteId", strSiteId);
				reply.getItem("programId", strProgramId);  // 이순간 programId 가 얻어진다.
				ciDEBUG(1, ("programId = %s", strProgramId.c_str()));

				// Empty Check 을 해주는것이 좋다.
				if(strProgramId.empty() /* ||  strSiteId.empty()*/)
				{
					// ERROR !!!
					continue;
				}//if

				//strPackageName = strSiteId + "_" + strProgramId ; // ini 명은 아마도 이렇게 얻어진다.
				listContentsPackage.push_back(strProgramId);
			}//while
		//}//while
	}
	catch(CORBA::Exception& ex)
	{
		ciERROR((ex._name()));
		return false;
	}//try

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ENC 폴더내부의 비어있는 폴더들을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCCleaner::RemoveEmptyENC()
{
	BOOL bRet = FALSE;
	CFileFind find;
	CString strExt;
	CString strENC = m_strContentsPath;
	strENC.Append(_T("\\*.*"));

	bRet = find.FindFile(strENC);
	while(bRet)
	{
		bRet = find.FindNextFile();

		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		//디렉토리인 경우
		if(find.IsDirectory() == TRUE)
		{
			//디렉토리중에 xml 이름을 갖고있는 폴더는 pop 플래시에서 사용하므로 지우면 안된다.
			if(find.GetFileTitle() != "xml")
			{
				if(!RemoveEmptyFolder(find.GetFilePath()))
				{
					ciERROR(("Fail to remove empty folder : %s", find.GetFilePath()));
				}//if
			}//if
		}//if
	}//while
	find.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 경로의 비어있는 폴더들을 삭제한다. \n
/// @param (LPCTSTR) lpDirPath : (in) 정리하려는 폴더 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCCleaner::RemoveEmptyFolder(LPCTSTR lpDirPath)
{
	if(lpDirPath == NULL)// 경로가 없는 경우 되돌아간다.
	{  
		return false;
	}//if

	BOOL bRval = FALSE;
	int nRval = 0;
	CString strRoot = _T("");
	CString strPath = lpDirPath;
	strPath.Append(_T("\\*.*"));
	CFileFind find;
	bool bHasFile = false;

	// 폴더가 존재 하는 지 확인 검사 
	bRval = find.FindFile(strPath);
	if(bRval == FALSE)
	{
		return bRval;
	}//if

	while(bRval)
	{
		bRval = find.FindNextFile();
		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		// Directory 일 경우
		if(find.IsDirectory())             
		{
			if(RemoveEmptyFolder(find.GetFilePath()))
			{
				bHasFile = false;
			}//if
			continue;
		}//if

		bHasFile = true;
	}//while

	find.Close(); 

	if(!bHasFile)
	{
		if(!::RemoveDirectory(lpDirPath))
		{
			LPVOID lpMsgBuf;
			DWORD dw = GetLastError(); 

			FormatMessage(
						FORMAT_MESSAGE_ALLOCATE_BUFFER | 
						FORMAT_MESSAGE_FROM_SYSTEM,
						NULL,
						dw,
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
						(LPTSTR) &lpMsgBuf,
						0, NULL );

			ciERROR(("Fail to delete empty filder : %s", (char*)lpMsgBuf));
			LocalFree(lpMsgBuf);
			return false;
		}//if
	}//if

	return bRval; 
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 경로의 디렉토리가 Tar컨턴츠 디렉토리인지 판단한다. \n
/// @param (LPCTSTR) lpPath : (in) Tar 디렉토리인지 검사할 경로
/// @return <형: bool> \n
///			<true: Tar 디렉토리> \n
///			<false: Tar 디렉토리가 아님> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCCleaner::IsTarDirectory(LPCTSTR lpPath)
{
	CString strFindFile = lpPath;
	strFindFile.Append(_T("\\Tar_Info.ini"));
	struct _stati64 stStat;

	if(_stati64(strFindFile, &stStat) == 0)
	{
		//Tar_Info.ini 파일이 디렉토리 내부에 있다면 Tar 파일을 브라우져에서 압축해제한 디렉토리이다.
		return true;		
	}//if

	return false;
}



/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Vista 이하에서 주어진경로의 모든 파일과 폴더를 삭제한다. \n
/// @param (CString) strPath : (in) 삭제하려는 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCCleaner::RemoveUseShellUnderVista(CString strPath)
{
	// 파일명 끝에 \0\0 이 있어야 정상동작.
    // 그렇지 않은 경우 SHFileOperation()는 1026을 돌려준다.
    _tstring from = strPath;
    strFrom.resize(filename.size() + 2);
    strFrom += _T("\0\0");
 
    SHFILEOPSTRUCT shFileOpStruct			= {0,};
    shFileOpStruct.hwnd						= NULL;
    shFileOpStruct.wFunc                    = FO_DELETE;
    shFileOpStruct.pFrom                    = strFrom.c_str();
    shFileOpStruct.pTo						= NULL;
    shFileOpStruct.fFlags                   = FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI;
    shFileOpStruct.fAnyOperationsAborted    = FALSE;
    shFileOpStruct.hNameMappings            = NULL;
    shFileOpStruct.lpszProgressTitle        = NULL;
	
    //if( useRecycleBin ) // 휴지통에 넣기
    //{ 
    //    shFileOpStruct.fFlags |= FOF_ALLOWUNDO;
    //}	
 
    int nRet = SHFileOperation(&shFileOpStruct);
    if(nRet == 0)
    {
        return true;
    }
    else
    {
		TRACE("FAILED SHFileOperation[%d]\r\n", nRet);
        return false;
    }//if
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Vista 이후에서 주어진경로의 모든 파일과 폴더를 삭제한다. \n
/// @param (CString) strPath : (in) 삭제하려는 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCCleaner::RemoveUseShellSinceVista(CString strPath)
{
	//주의 1. XP에서 SHFileOperation와 IFileOperation가 모두 구현된 상태에서 실행할 경우
	//만약, 두 함수를 구현한 후 XP에서 실행시
	//"프로시저 시작 지점 SHCreateItemFromParsingName을(를) DLL SHELL32.dll에서 찾을 수 없습니다."
	//에러가 발생한다.
	//해당 함수가 Vista 이후의 shell32.dll 부터 있는 것이기 때문인데
	//현재 "프로젝트 속성 > 링커 > 입력 > 지연 로드된 DLL" 에 shell32.dll 를 등록하면 된다.

	bool bRet = false;
 
    const TCHAR* pFrom = NULL;
#ifdef UNICODE
        pFrom = filename.c_str();
#else
        USES_CONVERSION;
        pFrom = A2W(strPath.);
#endif
     
    IFileOperation* pfo;
 
    HRESULT hr;
    hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
    if(SUCCEEDED(hr))
    {
        HRESULT hr = CoCreateInstance(CLSID_FileOperation, NULL, CLSCTX_ALL, IID_PPV_ARGS(&pfo));
        if(SUCCEEDED(hr))
        {
            DWORD flags = FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI;
            //if( useRecycleBin ) { flags |= FOF_ALLOWUNDO; }  // 휴지통에 넣기
 
            hr = pfo->SetOperationFlags(flags);
            if(SUCCEEDED(hr))
            {
                IShellItem *psiFrom = NULL;
                hr = SHCreateItemFromParsingName(pFrom, NULL, IID_PPV_ARGS(&psiFrom));
                if(SUCCEEDED(hr))
                {
                    if(SUCCEEDED(hr))
                    {
                        hr = pfo->DeleteItem(psiFrom, NULL);
                        if(SUCCEEDED(hr))
						{
                            TRACE("Delete item success\r\n");
						}
                        else
						{
                            printf("Delete item fail\r\n");
						}//if
                    }//if
                    psiFrom->Release();
                }//if

                if(SUCCEEDED(hr))
                {
                    hr = pfo->PerformOperations();
                    if(SUCCEEDED(hr))
                    {
                        ret = true;
                    }//if
                }//if
            }//if
            pfo->Release();
        }//if
    }//if
    CoUninitialize();
 
    return ret;
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우 버전을 구해온다. \n
/// @return <형: int> \n
///			<윈도우 버전정의 값> \n
/////////////////////////////////////////////////////////////////////////////////
int CUBCCleaner::GetWindowsType()
{
	//return value :
	//-1 : 버전얻기 실패
	//1 : Windows 95,  
	//2 : Windows 98,  
	//3 : Windows ME,  
	//4 : Windows NT,
	//5 : Windows 2000,  
	//6 : Windows XP,  
	//7 : Windows 2003,
	//8 : Windows Vista, 2008
	//9 : Windows 7, 2008 R2

	int nVersion= -1;
	OSVERSIONINFOEX osvi = {0,};
	BOOL version_ex_flag = 0;

	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

	if( !(version_ex_flag = GetVersionEx((OSVERSIONINFO *)&osvi)) )
	{
		osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
		if( !GetVersionEx((OSVERSIONINFO *)&osvi) )
			return -1;
	}

	switch(osvi.dwPlatformId)
	{
	case VER_PLATFORM_WIN32_WINDOWS:    // 윈도우즈 9x 기반의 운영체제인 경우
		{
			if( osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 0 )
			{
				nVersion = 1;           // Windows 95
			}
			else if(osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 10)
			{
				nVersion = 2;           // Windows 98
			}
			else if(osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 90) 
			{
				nVersion = 3;           // Windows ME
			}
		}
		break;
	case VER_PLATFORM_WIN32_NT: // NT 기술 기반의 운영체제인 경우
		{   
			if( osvi.dwMajorVersion <= 4 ) 
			{
				nVersion = 4;           // Windows NT
			}
			else if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 ) 
			{
				nVersion = 5;           //Windows 2000
			}
			else if(version_ex_flag) 
			{
				if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1)
				{
					nVersion = 6;       // Windows XP
				}
				else if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2)
				{
					nVersion = 7;       // Windows 2003
				}
				else if( osvi.dwMajorVersion == 6 && osvi.dwMinorVersion == 0)
				{
					nVersion = 8;       // Windows Vista, 2008
				}
				else if( osvi.dwMajorVersion == 6 && osvi.dwMinorVersion == 1)
				{
					nVersion = 9;       // WIndows7, 2008 R2
				}
			}
		}  
		break;
	}
	return nVersion;
}
*/



