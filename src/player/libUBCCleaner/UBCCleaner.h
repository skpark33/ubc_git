/************************************************************************************/
/*! @file UBCCleaner.h
	@brief 단말에서 사용되지 않는 컨텐츠패키지를 정리하는 기능을 하는 클래스 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2011/10/17\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2011/10/17:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

#include <list>
#include "DeleteInfo.h"
#include <libProfileManager/ProfileManager.h>

//! <단말에서 사용하지 않는 컨텐츠패키지를 정리하는 클래스>
/*!
	서버에존재하는 컨텐츠패키지 리스트를 받아서 \n
	단말에 존재하는 컨텐츠패키지와 비교하여 단말에만 존재하는(서버에서 지워진) \n
	컨텐츠 패키지와 다른곳에서 사용하지않는 컨텐츠파일들을 지운다.> \n
*/
class CUBCCleaner
{
public:
	CUBCCleaner(void);											///<생성자
	virtual ~CUBCCleaner(void);									///<소멸자

	bool		GetContentsPackageList(list<string>& listContentsPackage);	///<서버의 컨텐츠패키지리스트를 얻어온다.
	ULONGLONG	CleanExcludePackage(list<string>& listExcludePackage,
									bool bCleanSystem = true);				///<주어진 패키지를 제외하고 단말을 정리한다.
	ULONGLONG	CleanDeletePackage(list<string>& listDeletePackage,
									bool bCleanSystem = true);				///<주어진 패키지를 단말에서 정리한다.
	ULONGLONG	CleanDeleteFiles(list<string>& listFiles,
									bool bCleanSystem = true);				///<주어진 파일(폴더)를 단말에서 정리한다.
	ULONGLONG	CleanAll(bool bCleanSystem = true);							///<ENC 폴더의 *.ini와 *xml를 제외하고 삭제한다.
	void		SetCutLocationLevel(int nLevel);							///<컨텐츠 파일의 location필드값을 잘라내는 level을 설정한다.

private:
	CProfileManager		m_mngLog;								///<로그를 기록하기위한 클래스
	CMapStringToString	m_mapUseContentsPackage;				///<실제 사용하는 컨텐치 패키지 맵
	CDeleteInfoArray	m_aryUseContents;						///<단말에서 사용하는 컨텐츠 리스트
	CDeleteInfoArray	m_aryDeleteContents;					///<단말에서 삭제하는 컨텐츠 리스트
	CStringArray		m_aryDeleteFile;						///<삭제하려는 파일의 리스트
	CStringArray		m_aryDeletePackage;						///<삭제하려는 패키지 리스트

	int					m_nDeleteCount;							///<삭제된 파일 수
	ULONGLONG			m_ullDeletedSize;						///<삭제된 사이즈(byte)
	CString				m_strConfigPath;						///<컨텐츠패키지 경로
	CString				m_strContentsPath;						///<컨텐츠 경로
	CString				m_strTempPath;							///<임시 경로
	CString				m_strLogPath;							///<log 경로
	CString				m_strDataPath;							///<data 경로
	CString				m_strCleanLogPath;						///<UBCClean의 Log 경로
	int					m_nCutLevel;							///<서버경로를 로컬경로를 만들기위하여 location값을 잘라내는 기준레벨 값

	void		InitClean(void);											///<Clean 작업을 초기화 한다.
	void		EndClean(void);												///<Clean 작업을 마무리 한다.
	void		SetPath(void);												///<UBC의 각 경로를 설정한다.
	void		RemoveFile(CString strFileName,
							CString strPath,
							ULONGLONG ullSize);								///<파일을 삭제한다.
	void		AddLocalContentsPackage(void);								///<단말의 컨텐츠패키지 중 사용자가 만든 패키지를 사용하는 패키지로 추가
	void		MakeUseContentsList(void);									///<사용하는 컨텐츠 리스트를 만든다.
	void		MakeDeleteContentsList(void);								///<삭제하는 컨텐츠 리스트를 만든다.
	void		DeleteExcludeConfig(void);									///<사용중인것을 제외한 컨텐츠패키지 파일들을 삭제한다.
	void		DeleteExcludeContents(void);								///<사용중인것을 제외한 컨텐츠파일을 삭제한다.
	void		DeleteExcludeSubContents(CString strSubPath);				///<사용중인것을 제외한 부속(ENC안의 서브폴더) 컨텐츠파일을 삭제한다.
	void		DeleteExcludeTarContents(CString strTarPath);				///<사용중인것을 제외한 Tar 컨텐츠파일을 삭제한다.
	void		DeleteConfig(void);											///<지정된 컨텐츠패키지 파일들을 삭제한다.
	void		DeleteContents(void);										///<지정된 컨텐츠파일을 삭제한다.
	void		DeleteTemp(void);											///<임시경로의 파일들을 삭제한다.
	void		DeleteLog(void);											///<log 파일들을 삭제한다.
	void		DeleteRecycleBin(void);										///<휴지통 파일들을 삭제한다.
	void		CleanSystem(void);											///<시스템 정리를 수행한다.
	void		ClearENC(void);												///<컨텐츠 폴더를 정리한다.
	void		DeleteENCFiles(void);										///<컨텐츠 폴더에서 지정된 파일들을 삭제한다.
	void		RemoveEmptyENC(void);										///<ENC 폴더내부의 비어있는 폴더들을 삭제한다.
	bool		RemoveEmptyFolder(LPCTSTR lpDirPath);						///<주어진 경로의 비어있는 폴더들을 삭제한다.
	bool		IsTarDirectory(LPCTSTR lpPath);								///<주어진 경로의 디렉토리가 Tar컨턴츠 디렉토리인지 판단한다.

	bool		DeleteDirectory(LPCTSTR lpDirPath);							///<지정된 폴더와 내부의 폴더, 파일을 삭제한다.
	ULONGLONG	GetFolderSize(CString strInPath);							///<폴더의 안의 파일들 사이즈를 구한다.
	
	CString		GetExtension(const CString& strFileName);					///<파일명에서 확장자를 분리하여 반환
	CString		GetFileName(const CString& strFilePath);					///<주어진 경로에서 파일명을 반환한다.
	CString		GetLocalPath(LPCSTR lpszLocation,
							LPCSTR lpszFileName);							///<서버의 경로로부터 local의 경로를 반환한다.

	//다국어나 UTF-8등의 문자열이 있다면 MFC에서 제공하는 함수는 오류가 난다...
	inline void MakeLower(CString& str)
	{
		for( int i = 0; i < str.GetLength(); i++ )
		{
			char c = str[i];
			if( c >= 'A' && c <= 'Z' )
			{
				str.SetAt( i, c + 'a' - 'A' );
			}//if
		}//for
	}//inline

	//int		GetWindowsType(void);						///<윈도우 버전을 구해온다.
	//bool		RemoveUseShellUnderVista(CString strPath);	///<Vista 이하에서 주어진경로의 모든 파일과 폴더를 삭제한다.
	//bool		RemoveUseShellSinceVista(CString strPath);	///<Vista 이후에서 주어진경로의 모든 파일과 폴더를 삭제한다. 
};
