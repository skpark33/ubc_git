#include "StdAfx.h"
#include "SysOnOffTime.h"
#include "ci/libDebug/ciDebug.h"

#define SAFE_RELEASE( x ) { if( x ) { int n = x->Release(); TRACE("Ref count = %d\r\n", n); } x = NULL; }

ciSET_DEBUG(9,"libSysOnOffTime");

void
CSysOnOffTime::CoUnInitialize()
{
	CoUninitialize();
}

bool
CSysOnOffTime::CoInitialize()
{

	ciDEBUG(1, ("CoInitializeEx initialize"));
    HRESULT hr;
	hr = CoInitializeEx(0, COINIT_MULTITHREADED); 
	if(FAILED(hr)) 
	{ 
		ciERROR(("CoInitializeEx initialize fail"));
		return FALSE;
	}//if

	hr =  CoInitializeSecurity(NULL,						// Security descriptor    
								-1,							// COM negotiates authentication service
								NULL,						// Authentication services
								NULL,						// Reserved
								RPC_C_AUTHN_LEVEL_DEFAULT,	// Default authentication level for proxies
								RPC_C_IMP_LEVEL_IMPERSONATE,// Default Impersonation level for proxies
								NULL,                       // Authentication info
								EOAC_NONE,                  // Additional capabilities of the client or server
								NULL);                      // Reserved

	if(FAILED(hr))
	{
		ciERROR(("CoInitializeSecurity initialize fail"));
		return FALSE;
	}//if
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CSysOnOffTime::CSysOnOffTime()
:	m_bIsInitWMI(false)
,	m_pWbemLoc(NULL)
,	m_pWbemService(NULL)
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CSysOnOffTime::~CSysOnOffTime()
{
	CleanWMI();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 마지막으로 윈도우즈가 종료된 시간을 반환한다. \n
/// @return <형: __int64> \n
///			<-1: 오류> \n
///			<0 보다 큰값 : __time64_t 형식의 시간> \n
/////////////////////////////////////////////////////////////////////////////////
__int64 CSysOnOffTime::GetLastShutdownTime()
{
	ciDEBUG(1, ("Begin GetLastShutdownTime"));

	HKEY hKey;
	LONG lRet;
	BYTE btValue[8] = { 0x00 };
	DWORD dwSize = 8;
	CString regKey = _T("SYSTEM\\CurrentControlSet\\Control\\Windows");

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regKey, 0, KEY_ALL_ACCESS, &hKey);
	if(lRet == ERROR_SUCCESS)
	{
		lRet = RegQueryValueEx(hKey, "ShutdownTime", NULL, NULL, (LPBYTE)&btValue, &dwSize);
		if(lRet != ERROR_SUCCESS)
		{
			ciDEBUG(1, ("Reg Query for ShutdownTime failed"));
			return -1;
		}//if
	}
	else
	{
		ciDEBUG(1, ("Reg open for ShutdownTime failed"));
		return-1;
	}//if
	RegCloseKey(hKey);

	//레지스트리에서 얻어오는 값은 byte 형식으로 기록된 FILETIME 나노시간....
	FILETIME tmFile;
	tmFile.dwLowDateTime = btValue[0] + 256 * (btValue[1] + 256 * (btValue[2] + 256 * btValue[3]));
	tmFile.dwHighDateTime = btValue[4] + 256 * (btValue[5] + 256 * (btValue[6] + 256 * btValue[7]));

	CTime tmTemp(tmFile);
	CString strTime = tmTemp.Format("%Y/%m/%d %H:%M:%S");
	ciDEBUG(1, ("Last shutdown time = %s", strTime));
	ciDEBUG(1, ("End GetLastShutdownTime"));
	return tmTemp.GetTime();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 마지막으로 윈도우즈가 부팅된 시간을 반한한다. \n
/// @return <형: __int64> \n
///			<-1: 오류> \n
///			<0 보다 큰값 : __time64_t 형식의 시간> \n
/////////////////////////////////////////////////////////////////////////////////
__int64 CSysOnOffTime::GetLastBootUpTime()
{
	ciDEBUG(1, ("Begin GetLastBootUpTime"));
	if(!IsInitWMI())
	{
		if(!InitWMI())
		{
			return -1;
		}//if
	}//if

	HRESULT hr;
	CComPtr<IEnumWbemClassObject> pEnumerator;
	hr = m_pWbemService->ExecQuery(
									BSTR(L"WQL"), 
									BSTR(L"SELECT * FROM Win32_OperatingSystem"),
									WBEM_FLAG_FORWARD_ONLY, 
									NULL,
									&pEnumerator);

	if(FAILED(hr))
	{
		ciDEBUG(1, ("Query for Operating system failed"));
		CleanWMI();
		return -1;               // Program has failed.
	}//if

	//CComPtr<IWbemClassObject> pclsObj;
	IWbemClassObject *pclsObj;
	ULONG uReturn = 0;
	CString strTime = "";

	while(pEnumerator)
	{
		hr = pEnumerator->Next(WBEM_INFINITE, 1, &pclsObj, &uReturn);

		if(0 == uReturn)
		{
			break;
		}//if

		VARIANT vtProp;

		// Get the value of the Name property
		hr = pclsObj->Get(L"LastBootUpTime", 0, &vtProp, 0, 0);
		if(FAILED(hr))
		{
			ciDEBUG(1, ("Fial to get last bootup time"));
			CleanWMI();
			return -1;       
		}//if

		strTime = vtProp.bstrVal;
		VariantClear(&vtProp);
	}//while

	//정상적으로 시간을 얻어온다면 20120712092251.125599+540 와 같은 형식이다.
	//나노시간(?)까지의 표시와 GMT+9 의 값인듯...
	if(strTime.GetLength() < 14)
	{
		ciDEBUG(1, ("invalid last bootup time = %s", strTime));
		return -1;
	}//if

	ciDEBUG(1, ("last bootup time = %s", strTime));
	//앞에서부터 14자리를 잘라와서 파싱하여 time_t 형식으로 만들어주어야 한다.
	CString strBootTime = strTime.Left(14);
	CString strYear, strMonth, strDay, strHour, strMin, strSec;

	strYear = strBootTime.Left(4);
	strMonth = strBootTime.Mid(4, 2);
	strDay = strBootTime.Mid(6, 2);
	strHour = strBootTime.Mid(8, 2);
	strMin = strBootTime.Mid(10, 2);
	strSec = strBootTime.Right(2);

	CTime tmTemp(atoi(strYear), atoi(strMonth), atoi(strDay), atoi(strHour), atoi(strMin), atoi(strSec));
	CString strTemp = tmTemp.Format("%Y/%m/%d %H:%M:%S");
	ciDEBUG(1, ("Last bootuo time = %s", strTemp));
	ciDEBUG(1, ("End GetLastBootUpTime"));
	return tmTemp.GetTime();
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WMI를 초기화 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CSysOnOffTime::InitWMI()
{
	ciDEBUG(1, ("Begin InitWMI"));

	HRESULT hr;
/*
	hr = CoInitializeEx(0, COINIT_MULTITHREADED); 
	if(FAILED(hr)) 
	{ 
		AfxMessageBox("COM initialize fail");
		return;
	}//if
*/
/*
	hr =  CoInitializeSecurity(NULL,						// Security descriptor    
								-1,							// COM negotiates authentication service
								NULL,						// Authentication services
								NULL,						// Reserved
								RPC_C_AUTHN_LEVEL_DEFAULT,	// Default authentication level for proxies
								RPC_C_IMP_LEVEL_IMPERSONATE,// Default Impersonation level for proxies
								NULL,                       // Authentication info
								EOAC_NONE,                  // Additional capabilities of the client or server
								NULL);                      // Reserved

	if(FAILED(hr))
	{
		ciDEBUG(1, ("Fail to CoInitializeSecurity"));
		return false;
	}//if
*/
	hr = CoCreateInstance(CLSID_WbemAdministrativeLocator, NULL, 
							CLSCTX_INPROC_SERVER, IID_IWbemLocator, (LPVOID *) &m_pWbemLoc);

	if(FAILED(hr))
	{
		ciDEBUG(1, ("Failed to create IWbemLocator object"));
		return false;
	}//if

	hr = m_pWbemLoc->ConnectServer(BSTR(L"root\\cimv2"), NULL, NULL, NULL, WBEM_FLAG_CONNECT_USE_MAX_WAIT, NULL, NULL, &m_pWbemService);
	if(FAILED(hr))
	{
		ciDEBUG(1, ("Failed to connect"));
		CleanWMI();
	}//if

	 hr = CoSetProxyBlanket(m_pWbemService,
							  RPC_C_AUTHN_WINNT,
							  RPC_C_AUTHZ_NONE,
							  NULL,
							  RPC_C_AUTHN_LEVEL_CALL,
							  RPC_C_IMP_LEVEL_IMPERSONATE,
							  NULL,
							  EOAC_NONE);

	 if(FAILED(hr))
	 {
		 ciDEBUG(1, ("Could not set proxy blanket"));
		 CleanWMI();
		 return false;
	 }//if

	 ciDEBUG(1, ("Success om init WMI"));
	 ciDEBUG(1, ("End InitWMI"));
	 return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WMI 관련 콤포넌트를 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CSysOnOffTime::CleanWMI()
{
	SAFE_RELEASE(m_pWbemLoc);
	SAFE_RELEASE(m_pWbemService);
	m_bIsInitWMI = false;
}
