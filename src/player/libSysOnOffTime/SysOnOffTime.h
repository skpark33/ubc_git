/************************************************************************************/
/*! @file SysOnOffTime.h
	@brief 윈도우즈의 마지막 부팅시간과 마지막 종료시간을 구하는 클래스 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2012/07/12\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2012/07/12:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

#include <wbemidl.h>
# pragma comment(lib, "wbemuuid.lib")


//! 윈도우즈의 마지막 부팅시간과 마지막 종료시간을 구하는 클래스
/*!
*/
class CSysOnOffTime
{
public:
	CSysOnOffTime(void);									///<생성자
	virtual~CSysOnOffTime(void);							///<소멸자

	__int64		GetLastShutdownTime(void);					///<마지막으로 윈도우즈가 종료된 시간을 반환한다.
	__int64		GetLastBootUpTime(void);					///<마지막으로 윈도우즈가 부팅된 시간을 반한한다.

	static void CoUnInitialize();
	static bool CoInitialize();

private:
	bool		InitWMI(void);								///<WMI를 초기화 한다.
	bool		IsInitWMI(void)	{ return m_bIsInitWMI; }	///<WMI가 초기화 되었는지 여부를 반환
	void		CleanWMI(void);								///<WMI 관련 콤포넌트를 정리한다.


private:
	bool			m_bIsInitWMI;							///<WMI가 초기화 되었는지 여부
	IWbemLocator*	m_pWbemLoc;								///<IWbemLocator 인터페이스
	IWbemServices*	m_pWbemService;							///<IWbemServices 인터페이스
};
