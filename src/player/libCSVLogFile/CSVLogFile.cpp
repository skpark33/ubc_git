#include "StdAfx.h"
#include "CSVLogFile.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciIni.h>
#include <util/libEncoding/Encoding.h>

ciSET_DEBUG(10, "CSVLogFile");

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CCSVLogFile::CCSVLogFile(void)
: m_strFileName(_T(""))
//, m_bHasBom(false)
, m_bOpen(false)
//, m_pFile(NULL)
, m_bIsUnicodeMode(false)
{
	m_bMultiLang = ciIniUtil::isMultiLang();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CCSVLogFile::~CCSVLogFile(void)
{
	CloseCSV();
	//CEncoding*	pEncoder = CEncoding::getInstance();
	//pEncoder->clearInstance();
}

	
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// CSV 로그파일을 Open(Create) 한다. \n
/// @param (CString) strFileName : (in) 파일의 경로
/// @param (bool) bCreate : (in) 파일을 생성할지 여부
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CCSVLogFile::OpenCSV(string strFileName, bool bCreate)
{
	CloseCSV();
	UINT nMode = 0;
	m_strFileName = strFileName;

	if(bCreate)
	{
		nMode = CFile::modeCreate | CFile::modeWrite | CFile::typeBinary;
		ciDEBUG(1, ("CSV log file create mode : %s", m_strFileName.c_str()));
	}
	else
	{
		nMode = CFile::modeRead | CFile::typeBinary;
		ciDEBUG(1, ("CSV log file read mode : %s", m_strFileName.c_str()));
	}//if

	if(!m_File.Open(m_strFileName.c_str(), nMode))
	{
		ciERROR(("Fail to open CSV log file : %s", m_strFileName.c_str()));
		return false;
	}//if

	if(m_bMultiLang)
	{
		if(bCreate)
		{
			//UTF-8 BOM 기록
			BYTE btHeader[3] = { 0xEF, 0xBB, 0xBF };
			m_File.Write(btHeader, sizeof(BYTE)*3);
		}
		else
		{
			//파일을 읽을때에 BOM이 있는지 확인하고 없다면 다시 처음으로 이동...
			ULONGLONG ullSize = m_File.GetLength();
			if(ullSize == 0)
			{
				ciWARN(("File size is 0 : %s", m_strFileName.c_str()));
				m_File.Close();
				return false;
			}//if

			if(ullSize > 3)
			{
				BYTE btBOM[3] = { 0x00, };
				m_File.Read(btBOM, 3);
				if(!(btBOM[0] == 0xEF && btBOM[1] == 0xBB && btBOM[2] == 0xBF))
				{
					//UTF-8로 파일 전체를 변환
					if(!EncodeFile())
					{
						return false;
					}//if
				}//if
			}//if
		}//if
	}//if

	m_bOpen = true;
	ciDEBUG(1, ("CSV log file opened : %s", m_strFileName.c_str()));

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 유니코드형식의 CSV 파일을 생성한다. \n
/// @param (string) strFileName : (in) 생성하려는 파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CCSVLogFile::CreateUnicodeCSV(string strFileName)
{
	CloseCSV();
	UINT nMode = CFile::modeCreate | CFile::modeWrite | CFile::typeBinary;
	m_strFileName = strFileName;

	ciDEBUG(1, ("CSV unicode log file create mode : %s", m_strFileName.c_str()));

	if(!m_File.Open(m_strFileName.c_str(), nMode))
	{
		ciERROR(("Fail to open CSV log file : %s", m_strFileName.c_str()));
		return false;
	}//if

	//Unicode BOM 기록
	BYTE btHeader[2] = { 0xFF, 0xFE };
	m_File.Write(btHeader, sizeof(BYTE)*2);

	m_bOpen = true;
	m_bIsUnicodeMode = true;
	ciDEBUG(1, ("CSV unicode log file opened : %s", m_strFileName.c_str()));

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 열려있는 파일을 Close 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CCSVLogFile::CloseCSV(void)
{
	if(m_bOpen)
	{
		m_File.Close();
		m_bOpen = false;
		ciDEBUG(1, ("CSV log file closed : %s", m_strFileName.c_str()));
	}//if
}
	

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 라인단위로 CSV 파일을 읽는다. \n
/// @param (string&) strLine : (out) 읽은 파일의 라인문자열
/// @return <형: int> \n
///			<-1: 읽기> 오류 \n
///			<0: EOF> \n
///			그외에는 읽은 라인의 문자열 수 \n
/////////////////////////////////////////////////////////////////////////////////
int CCSVLogFile::ReadLine(string& strLine)
{
	if(m_bIsUnicodeMode)
	{
		ciWARN(("Can't use ReadLine() in unicode mod"));
		return -1;
	}//if

	CString strRead;
	if(!m_File.ReadString(strRead))	//라인단위 읽기
	{
		ciDEBUG(1, ("EOF : %s", m_strFileName.c_str()));
		return 0;
	}//if

	if(m_bMultiLang)
	{
		CEncoding*	pEncoder = CEncoding::getInstance();
		if(!pEncoder->UTF8ToAnsi(strRead, strLine))
		{
			ciERROR(("UTF-8 ==> ANSI : %s", strRead));
			return -1;
		}//if
	}
	else
	{
		strLine = strRead;
	}//if

	ciDEBUG(1, ("ReadLine success : %s", m_strFileName.c_str()));
	return strLine.length();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// CSV 로그파일에 라인을추가한다. \n
/// @param (string) strLine : (in) CSV로그파일에 추가하려는 라인 문자열
/// @return <형: bool> \n
///			<true: 성공> 오류 \n
///			<fasle: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CCSVLogFile::WriteLine(string strLine)
{
	if(m_bIsUnicodeMode)
	{
		ciWARN(("Unicode mod : Use function WriteUnicodeLine()"));
		return false;
	}//if

	string strWrite;
	if(m_bMultiLang)
	{
		CEncoding*	pEncoder = CEncoding::getInstance();
		if(!pEncoder->AnsiToUTF8(strLine.c_str(), strWrite))
		{
			ciERROR(("ANSI ==> UTF-8 : %s", strLine.c_str()));
			return false;
		}//if
	}
	else
	{
		strWrite = strLine;
	}//if
	strWrite += "\r\n";

	m_File.WriteString(strWrite.c_str());
	ciDEBUG(1, ("WriteLine success : %s", m_strFileName.c_str()));

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// CSV 로그파일에 유니코드로 된 라인을추가한다. \n
/// @param (string) strLine : (in) 파일에 쓰려는 Ansi문자열 라인
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CCSVLogFile::WriteUnicodeLine(string strLine)
{
	if(!m_bIsUnicodeMode)
	{
		ciWARN(("Not unicode mod : Use function WriteLine()"));
		return false;
	}//if

	if(!m_bMultiLang)
	{
		ciWARN(("Not multilang mod"));
		return false;
	}//if

	wstring strWrite;
	CEncoding*	pEncoder = CEncoding::getInstance();
	if(!pEncoder->AnsiToUnicode(strLine.c_str(), strWrite))
	{
		ciERROR(("ANSI ==> Unicode : %s", strLine.c_str()));
		return false;
	}//if
	strWrite += L"\r\n";

	//m_File.WriteString(strWrite.c_str());
	m_File.Write((void*)strWrite.c_str(), strWrite.length()*sizeof(WCHAR));
	ciDEBUG(1, ("WriteUnicodeLine success : %s", m_strFileName.c_str()));

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일 전체를 UTF-8로 인코딩 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CCSVLogFile::EncodeFile()
{
	//먼저 파일의 처음으로 이동
	m_File.Seek(0, CFile::begin);
	string strFile = "";
	string strUtf8;
	CString strLine;

	CEncoding*	pEncoder = CEncoding::getInstance();

	while(m_File.ReadString(strLine))	//라인단위 읽기
	{
		if(!pEncoder->AnsiToUTF8(strLine, strUtf8))
		{
			m_File.Close();
			ciERROR(("UTF-8 ==> ANSI : %s", strLine));
			return false;
		}//if

		strFile += strUtf8;
		strFile += "\r\n";
	}//if

	//파일을 닫고 같은 이름으로 생성
	m_File.Close();
	if(!m_File.Open(m_strFileName.c_str(), CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
	{
		ciERROR(("Fail to create file : %s", m_strFileName.c_str()));
		ciERROR(("Fail to file encode : %s", m_strFileName.c_str()));
		return false;
	}//if

	//UTF-8 BOM 기록
	BYTE btHeader[3] = { 0xEF, 0xBB, 0xBF };
	m_File.Write(btHeader, sizeof(BYTE)*3);
	m_File.Write(strFile.c_str(), strFile.length());
	m_File.Close();

	//파일을 읽기 모드로 다시 열기
	if(!m_File.Open(m_strFileName.c_str(), CFile::modeRead | CFile::typeBinary))
	{
		ciERROR(("Fail to file re-open : %s", m_strFileName.c_str()));
		return false;
	}//if
	m_File.Seek(3, CFile::begin);	//BOM 만큼 이동한 파일의 처음으로 다시 이동

	ciDEBUG(1, ("Success file encode : %s", m_strFileName.c_str()));

	return true;
}
