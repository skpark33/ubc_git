//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TestCSVLogFile.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TESTCSVLOGFILE_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_WRITE_PATH_EDT              1000
#define IDC_WRITE_CREATE_BTN            1001
#define IDC_WRITE_LINE_EDT              1002
#define IDC_WRITE_LINE_BTN              1003
#define IDC_READ_PATH_EDT               1004
#define IDC_READ_OPEN_BTN               1005
#define IDC_READ_LINE_EDT               1006
#define IDC_READ_LINE_BTN               1007
#define IDC_WRITE_CREATE_BTN2           1008
#define IDC_PATH_BTN                    1008
#define IDC_CHECK1                      1009
#define IDC_UNICODE_CHECK               1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
