/************************************************************************************/
/*! @file CSVLogFile.h
	@brief CSV 형식의 로그파일을 UTF-8로 저장할 수 있도록 하는 클래스 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2012/03/29\n
	▶ 참고사항:
		- isMultiLang 환경에서 내부 데이터를 UTF-8로 처리함.
		- 서버에서는 UTF-8로 데이터 처리, 클라이언트는 ASCII로 데이터를 처리함

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2012/03/29:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once
using namespace std;
#include <string>


//! CSV 형식의 로그파일을 기록하는 클래스
/*!
isMultiLang() 이 true를 리턴하면, CLIENT 이므로
WriteLine() ==>  ASCII 문자열을 인자로 받아서 UTF-8 로 인코딩하여 파일에 기록합니다.
ReadLine() ==>   UTF-8로 기록되어 있는 파일을 라인 단위로 읽어서 ASCII 문자열로 디코딩하여 인파라미터에 넣어줍니다.
                 읽기 오류시에는 -1, EOF 일때는 0, 정상인 경우 읽은 라인의 문자수를 반환합니다.

isMultiLang() 이 false를 리턴하면, 서버 이므로
WriteLine() 에서 UTF-8 문자열을 받아서 파일에 기록합니다.
ReadLine() 에서는 UTF-8로 기록되어 있는 파일을 라인 단위로 읽어서 인파라미터에 넣어줍니다.
*/
class CCSVLogFile
{
public:
	CCSVLogFile();										///<생성자
	virtual ~CCSVLogFile();								///<소멸자
	
	bool 	OpenCSV(string strFileName, bool bCreate);	///<CSV 로그파일을 Open(Create) 한다.
	bool	CreateUnicodeCSV(string strFileName);		///<유니코드형식의 CSV 파일을 생성한다.
	void	CloseCSV(void);								///<열려있는 파일을 Close 한다.
	
	int		ReadLine(string& strLine);					///<라인단위로 CSV 파일을 읽는다.
	bool	WriteLine(string strLine);					///<CSV 로그파일에 라인을추가한다.
	bool	WriteUnicodeLine(string strLine);			///<CSV 로그파일에 유니코드로 된 라인을추가한다.
	bool	IsOpen(void)	{ return m_bOpen; };		///<파일이 열렸는지 여부

protected:
	//FILE*	m_pFile;									///<파일 핸들
	CStdioFile	m_File;									///<파일 입출력 클래스
	string		m_strFileName;							///<access 하는 파일의 경로
	bool		m_bMultiLang;							///<MultiLang 환경인지 여부
	//bool		m_bHasBom;								///<파일에 해더(BOM)가 있었는지 여부
	bool		m_bOpen;								///<파일이 열렸는지 여부
	bool		m_bIsUnicodeMode;						///<유니코드 모드인지 여부

	bool		EncodeFile(void);						///<파일 전체를 UTF-8로 인코딩 한다.
};
