// AnnounceDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "Announce_ppt_Dlg.h"
#include "MemDC.h"
#include "Host.h"

#define		ID_SLIDE_SHOW_END			4010		//슬라이드 쇼 종료 타이머 ID
#define		TIME_SLIDE_SHOW_END			500			//슬라이드 쇼 종료 타이머 시간

#define		ID_PRESENTAION_SHOW			4012		//Presentation Show 타이머 ID
#define		TIME_PRESENTAION_SHOW		5000		//Presentation Show 타이머 시간

//컨텐츠 파일이 경로에 없을 경우에 스케줄을 노출 시키는 시간
#define		TIME_CONTENTS_NO_EXIST		15					//15 sec

#define		TIME_PPT_PLAYING			(86400)			//PPT Schedule의 Play time	
#define		TIME_PPT_NONE				(10000)			//PPT가 설치되지 않았을 때 Play time



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// COfficeAnnounce
COfficeAnnounce* COfficeAnnounce::_instance = NULL;
CCriticalSection COfficeAnnounce::_lock;
int COfficeAnnounce::_refcnt = 0;


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ingle tone 인스턴스 생성 함수 \n
/// @return <형: COfficeAnnounce*> \n
///			<COfficeAnnounce*: Single tone 인스턴스> \n
///			<NULL: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
COfficeAnnounce* COfficeAnnounce::getInstance()
{
	_lock.Lock();
	if(!_instance) 
	{
		/*
		//처음 인스턴스 생성시에 이전에 실행중인 파워포인트를 강제 종료시킨다.
		__DEBUG__("Terminate Powerpoint", _NULL);
		scratchUtil* aUtil = scratchUtil::getInstance();
		ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
		__DEBUG__("scratchUtil : getPid", (ULONGLONG)ulPid);
		if(ulPid > 0)
		{
			int nRet = aUtil->killProcess(ulPid);
			__DEBUG__("scratchUtil : killProcess", nRet);
		}//if

		Sleep(500);
		*/

		_instance = new COfficeAnnounce();
		if(_instance->m_lpDispatch != NULL)
		{
			_instance->ReleaseDispatch();
		}//if

		COleException pExp;
		if(!_instance->CreateDispatch("Powerpoint.Application", &pExp))
		{
			TCHAR Buf[100];
			pExp.GetErrorMessage(Buf,100);
			__DEBUG__("Powerpoint CreateDispatch fail", Buf);
			delete _instance;
			_instance = NULL;
			_lock.Unlock();
			return NULL;
		}//if

		//Make the application visible but not minimized
		_instance->SetVisible((long)TRUE);
		// ppWindowNormal = 1, ppWindowMinimized = 2, ppWindowMaximized = 3
		_instance->SetWindowState((long) 2);	// Start with PPT visible - makes 
												//  presentation visible, too.
		_refcnt++;
		_lock.Unlock();
		return _instance;
	}//if

	_refcnt++;
	_lock.Unlock();
	return _instance;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Single tone 인스턴스 소멸 함수 \n
/////////////////////////////////////////////////////////////////////////////////
void COfficeAnnounce::clearInstance()
{
	__DEBUG__("clearInstance()",NULL);
	_lock.Lock();
	_refcnt--;

	if(_refcnt == 0)
	{
		_instance->Quit();
		_instance->ReleaseDispatch();
		delete _instance;
		_instance = NULL;

		//실행중인 파워포인트를 강제 종료시킨다.
		__DEBUG__("Terminate Powerpoint", _NULL);
		scratchUtil* aUtil = scratchUtil::getInstance();
		ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
		__DEBUG__("scratchUtil : getPid", (ULONGLONG)ulPid);
		if(ulPid > 0)
		{
			int nRet = aUtil->killProcess(ulPid);
			__DEBUG__("scratchUtil : killProcess", nRet);
		}//if
		//종료후 바로 다시뜨는 경우를 위하여 잠시 쉬어준다.
		::Sleep(1000);
	}//if
	_lock.Unlock();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
COfficeAnnounce::COfficeAnnounce()
{
	__DEBUG__("COfficeAnnounce()",NULL);
	//OleInitialize(NULL);
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
COfficeAnnounce::~COfficeAnnounce()
{
	__DEBUG__("~COfficeAnnounce()",NULL);
	//OleUninitialize();

}

// CAnnounce_ppt_Dlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAnnounce_ppt_Dlg, CAnnounceDlg)

CAnnounce_ppt_Dlg::CAnnounce_ppt_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent /*=NULL*/)
	: CAnnounceDlg(nPosX,nPosY,nCx,nCy,pParent)
,	m_pPPTEventConnectionPoint(NULL)
,	m_pPPTEventSink(NULL)
,	m_fPosX(0)
,	m_fPosY(0)
,	m_fWidth(0)
,	m_fHeight(0)
,	m_strPresentationName(_T(""))
,	m_pptApp(NULL)
,	m_bTopMost(false)
,	m_nLocale(82)		//KOR
{
	__DEBUG__("CAnnounce_ppt_Dlg", _NULL);

	m_bOpen = false;
	m_bFileNotExist = false;
	m_dwStartTick = ::GetTickCount();
}

CAnnounce_ppt_Dlg::~CAnnounce_ppt_Dlg()
{
}

BEGIN_MESSAGE_MAP(CAnnounce_ppt_Dlg, CAnnounceDlg)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_WM_GETMINMAXINFO()
	ON_WM_TIMER()
END_MESSAGE_MAP()

int CAnnounce_ppt_Dlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG__("CAnnounce_ppt_Dlg::OnCreate()", _NULL);
	if (CAnnounceDlg::OnCreate(lpCreateStruct) == -1){
		return -1;
	}
	char szNation[7] = { 0x00 };
	if(GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ICOUNTRY, szNation, 7) != 0)
	{
		m_nLocale = atoi(szNation);
	}//if

	return 0;
}
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 시작하도록 한다. \n
/// @param (CAnnounce*) pAnn : (in) 방송을 해야하는 공지 객체 주소값
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::BeginAnnounce(CAnnounce* pAnn)
{
	if(!m_pAnnPlay)
	{
		m_pAnnPlay = pAnn;
	}
	else if((*m_pAnnPlay == *pAnn) && m_pAnnPlay->m_bPlaying)
	{
		//if(!(((CHost*)m_pParentWnd)->IsWindowVisible())){
			//__DEBUG__("Same Annnounce Shows", pAnn->m_strAnnounceId.c_str());
			ShowWindow(SW_SHOW);
			((CHost*)m_pParentWnd)->SetFocus();
		//}
		return;
	}
	else
	{
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = pAnn;
	}//if

	char	szBuf[_MAX_PATH] = { 0x00 };
	::GetModuleFileName(NULL, szBuf, _MAX_PATH);

	char drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
	_splitpath(szBuf, drive, path, filename, ext);

	m_strMediaFullPath.Format("%s\\SQISOFT\\Contents\\Enc\\announce\\%s",drive, m_pAnnPlay->m_strBgFile.c_str());


	__DEBUG__("BeginAnnounce",	m_strMediaFullPath);
	__DEBUG__("BeginAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());

	CString strLog = m_pAnnPlay->m_strBgFile.c_str();
	WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(),m_pAnnPlay->m_strTitle.c_str(), "TRY", m_pAnnPlay->GetRunningTime(), strLog);

	MoveAnnounceWindow(m_nParentPosX, m_nParentPosY, m_nParentCx, m_nParentCy, m_pAnnPlay);

	__DEBUG__("OpenFile",	_NULL);
	if(OpenFile(0)){
		m_pAnnPlay->m_bPlaying = true;
		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "START", m_pAnnPlay->GetRunningTime(), strLog);
		//__DEBUG__("ShowWindow", 0);
		ShowWindow(SW_SHOW);
		((CHost*)m_pParentWnd)->SetFocus();
	}else{
		SetNoContentsFile();
		this->RedrawWindow();
	}
/*
	__DEBUG__("PPTRunTimeCheck",	_NULL);
	if(!m_pthreadShift)
	{
		m_pthreadShift = ::AfxBeginThread(CAnnounce_ppt_Dlg::PPTRunTimeCheck, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pthreadShift->m_bAutoDelete = TRUE;
		m_bExitThread = false;
		m_hevtShift = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pthreadShift->ResumeThread();
		//EndAnnounce();
	}//if
*/
	SetTransParency(RGB(0,0,0), 1, GetSafeHwnd());
	__DEBUG__("BeginAnnounce end",	_NULL);
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 중지하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::EndAnnounce()
{
	__DEBUG__("EndAnnounce", _NULL);
	if(m_pAnnPlay)
	{
		//__DEBUG__("EndAnnounce 1", m_pAnnPlay->m_strAnnounceId.c_str());

		CString strLog = m_pAnnPlay->m_strBgFile.c_str();

		CloseFile();

		//__DEBUG__("EndAnnounce 2", m_pAnnPlay->m_strAnnounceId.c_str());
		m_bExitThread = true;
		SetEvent(m_hevtShift);
		CloseHandle(m_hevtShift);
		m_hevtShift = NULL;
		m_pthreadShift = NULL;

		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "END", m_pAnnPlay->GetRunningTime(), strLog);

		ShowWindow(SW_HIDE);
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = NULL;
	}//if
	__DEBUG__("EndAnnounce End", NULL);
}
void CAnnounce_ppt_Dlg::OnDestroy()
{
	__DEBUG__("OnDestroy",NULL);
	//CloseFile();

	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if
	CAnnounceDlg::OnDestroy();

}

void CAnnounce_ppt_Dlg::OnPaint()
{
	//__DEBUG__("OnPaint",NULL);
	if(!m_bOpen) return;

	//__DEBUG__("OnPaint Start",NULL);
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	
	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		__DEBUG__("OnPaint m_bFileNotExist",NULL);
		CRect rtClient;
		GetClientRect(rtClient);

		CMemDC memDC(&dc);
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}//if
	else
	{
		__DEBUG__("OnPaint m_bgImage invalid",NULL);
		CRect rtClient;
		GetClientRect(rtClient);
		dc.FillSolidRect(rtClient, RGB(0, 0, 0));
	}

	if(m_ticker && 	m_ticker->IsPlay()){
		m_ticker->BringWindowToTop();
		m_ticker->SetForegroundWindow();
		m_ticker->SetFocus();
	}else{
		BringWindowToTop();
		SetForegroundWindow();
		SetFocus();
	}

	//__DEBUG__("OnPaint End",NULL);
}

void CAnnounce_ppt_Dlg::SetNoContentsFile()
{
	__DEBUG__("Contents file not exist", m_strMediaFullPath);

	//컨텐츠 파일이 없는 경우 액박 표시해 준다.
	HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_XBOX));
	m_bgImage.CreateFromHBITMAP(hBitmap);
	m_bOpen = true;
	m_bFileNotExist = true;
}

bool CAnnounce_ppt_Dlg::Play()
{
	__DEBUG__("Play start", _NULL);
	if(!m_bOpen)
	{
		__DEBUG__("File not opened", _NULL);
		return false;
	}
	if(m_bFileNotExist)
	{
		return true;
	}//if	
	if(m_pptApp == NULL)
	{
		__ERROR__("Can not found MS-PowerPoint program", 0);  
		__ERROR__("Get PPT Object fail !!!", _NULL);

		HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SLIDE_NO));
		m_bgImage.CreateFromHBITMAP(hBitmap);
		return false;
	}

	m_dwStartTick = ::GetTickCount();

	if(!ConnectPPTEventSink())
	{
		__DEBUG__("Fail to ConnectPPTEventSink", _NULL);
		return false;
	}//if

	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;

	pDisp = m_pptApp->GetPresentations();
	if(pDisp == NULL)
	{
		__DEBUG__("GetPresentaions fail !!!", _NULL);
		return false;
	}//if
	pptPresentations.AttachDispatch(pDisp);

	//열려있는 presentation중에 같은 파일이 있는지 검사하여
	//이미 열려있다면 닫아주어야 한다.
	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(m_strPresentationName) == 0)
		{
			pptPresentation.Close();
			pptPresentation.ReleaseDispatch();
			Sleep(500);
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	/*
	msoCTrue 1 지원되지 않음 
	msoFalse 0 False 
	msoTriStateMixed -2 지원되지 않음 
	msoTriStateToggle -3 지원되지 않음 
	msoTrue -1 True 
	*/
	__DEBUG__("Presentation open", m_strMediaFullPath);
	pDisp = pptPresentations.Open(m_strMediaFullPath,      
		(long)0,		//Read-only
		(long)0,		//Untitled
		(long)-1		//WithWindow
		//(long)-1      //WithWindow
		);

	pptPresentations.ReleaseDispatch();
	__DEBUG__("Presentation opened", m_strMediaFullPath);

	return true;
}

bool CAnnounce_ppt_Dlg::Stop()
{
	__DEBUG__("Stop", NULL);

	SlideShowEnd();

	if(m_bFileNotExist)
	{
		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(),"FAIL", m_pAnnPlay->GetRunningTime(), m_pAnnPlay->m_strBgFile.c_str());
	}
	else if(m_pptApp == NULL)
	{
		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(),"FAIL", m_pAnnPlay->GetRunningTime(), m_pAnnPlay->m_strBgFile.c_str());
	}
	else
	{
		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(),"SUCCEED", m_pAnnPlay->GetRunningTime(), m_pAnnPlay->m_strBgFile.c_str());
	}//if
	__DEBUG__("Stop End", NULL);

	return true;
}

bool CAnnounce_ppt_Dlg::OpenFile(int nGrade)
{
	__DEBUG__("OpenFile-Step1",	_NULL);
	if(m_bOpen) 
	{ 
		if(m_pAnnPlay->m_bPlaying)
		{
			__DEBUG__("\t\t\tAlready Open", _NULL);
			return true;
		}
		CloseFile();
	}

	__DEBUG__("OpenFile-Step2",	_NULL);
	CFileStatus fs;
	if(CFile::GetStatus(m_strMediaFullPath, fs ) == FALSE)
	{
		//컨텐츠 파일이 없는 경우 액박 표시해 준다.
		SetNoContentsFile();
		return false;
	}
	m_strPresentationName = fs.m_szFullName;
	m_pptApp = COfficeAnnounce::getInstance();
	if(m_pptApp == NULL)
	{
		__ERROR__("Get PPT Object fail !!!", _NULL);
		return false;
	}//if

	CRect rectClient;
	GetClientRect(&rectClient);
	ClientToScreen(&rectClient);

	//Office는 Point 단위를 사용하므로 pixel을 point로 변환해야한다.
	// 12px 는 9pt(96DPI 기준)
	// Point = (Pixel)*72/(DPI)
	CDC* pDC = CWnd::GetDC();
	int nDpiX = pDC->GetDeviceCaps(LOGPIXELSX);
	int nDpiY = pDC->GetDeviceCaps(LOGPIXELSY);
	m_fPosX = (rectClient.left*72)/nDpiX;
	m_fPosY = (rectClient.top*72)/nDpiY;
	m_fWidth = (rectClient.Width()*72)/nDpiX;
	m_fHeight = (rectClient.Height()*72)/nDpiY;

	SetSlideShowResolution();

	m_bOpen = true;	

	return Play(); 
}

bool CAnnounce_ppt_Dlg::CloseFile()
{
	__DEBUG__("CloseFile() Start",NULL);	
	
	Stop();

	//__DEBUG__("CloseFile() 1",NULL);	
	if(m_bOpen)
	{
	//__DEBUG__("CloseFile() 2",NULL);	
		if(m_pptApp)
		{
	//__DEBUG__("CloseFile() 4",NULL);	
			COfficeAnnounce::clearInstance();
			//delete m_pptApp;
			m_pptApp = NULL;
		}//if
		m_bOpen = false;
	}//if
	__DEBUG__("CloseFile() End",NULL);	
	return true;
}


void CAnnounce_ppt_Dlg::RePlay()
{
	__DEBUG__("PPT Replay",0);
	Stop();
	Play();
}

double CAnnounce_ppt_Dlg::GetTotalPlayTime()
{
	if(m_pAnnPlay){
		return (double)(strtoul(m_pAnnPlay->m_strComment[1].c_str(), NULL,10)*1000); // sec -> millisec
	}
	return 0.0;
}

double CAnnounce_ppt_Dlg::GetCurrentPlayTime()
{
	return (double)GetTickDiff(m_dwStartTick,::GetTickCount());
}

double CAnnounce_ppt_Dlg::GetTickDiff(DWORD dwStart, DWORD dwEnd)
{
	if(dwStart > dwEnd)
		return double((MAXDWORD - dwStart) + dwEnd);
	return double(dwEnd - dwStart);
}


UINT CAnnounce_ppt_Dlg::PPTRunTimeCheck(LPVOID pParam)
{
	CAnnounce_ppt_Dlg* pParent = (CAnnounce_ppt_Dlg*)pParam;

	while(!pParent->m_bExitThread)
	{
		DWORD dwEventRet = WaitForSingleObject(pParent->m_hevtShift, 1000);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			__DEBUG__("Event set : PPTRunTimeCheck", _NULL);
			break;
		}
		//__DEBUG__("Timeout   : PPTRunTimeCheck", _NULL);

		double runTime = pParent->GetTotalPlayTime();
		double currentTime =  pParent->GetCurrentPlayTime();
		
		if( runTime && runTime <= currentTime){
			pParent->RePlay();
		}
	}//while
	__DEBUG__("Exit PlayImageThread", _NULL);
	return 1;
}

void CAnnounce_ppt_Dlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 20000;
	lpMMI->ptMaxTrackSize.y = 10000;
	lpMMI->ptMaxSize.x = 20000;
	lpMMI->ptMaxSize.y = 10000;

	CAnnounceDlg::OnGetMinMaxInfo(lpMMI);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파워포인트 이벤트 싱크 설정 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CAnnounce_ppt_Dlg::ConnectPPTEventSink()
{
	if(m_pptApp == NULL)
	{
		return false;
	}//if

	DisconnectPPTEventSink();

	///*********************** Start of code to get connection point **************
  	//  Declare the events we want to catch.
  	//
  	//  Look for the coclass for Application in the typelib, msppt9.olb
  	//  then look for the word "source." The interface - EApplication -
  	//  is the next search target. When you find it you'll see the 
  	//  following guid a the events you'll be able to sink.
  	//  914934C2-5A91-11CF-8700-00AA0060263B
  	//	static const GUID IID_IEApplication =
  	//	{0x914934C2,0x5A91,0x11CF, {0x87,0x00,0x00,0xAA,0x00,0x60,0x26,0x3b}};
  
  	//  Steps for setting up events.
  	// 1. Get server's IConnectionPointContainer interface.
  	// 2. Call IConnectionPointContainerFindConnectionPoint()
  	//    to find the event we want to catch.
  	// 3. Call IConnectionPoint::Advise() with the IUnknown
  	//    interface of our implementation of the events.

	// Get IDispatch interface for Automation...
	IDispatch *pDisp = NULL;
	pDisp = m_pptApp->m_lpDispatch;

  	// Get server's (PPT) IConnectionPointContainer interface.
  	IConnectionPointContainer *pConnPtContainer;
  	HRESULT hr = pDisp->QueryInterface(
  							IID_IConnectionPointContainer,
  							(void **)&pConnPtContainer);
  	
  	if(!SUCCEEDED(hr)) 
  	{ 
		CString strError;
		strError.Format(_T("Couldn't get IConnectionPointContainer interface."));
		__ERROR__((LPCSTR)strError, 0);

  		return false;
 	}//if
  	
  	// Find connection point for events we're interested in.
  	hr = pConnPtContainer->FindConnectionPoint(
  											IID_IEApplication,
  											&m_pPPTEventConnectionPoint
  											);
  	
  	if(!SUCCEEDED(hr)) 
  	{
		CString strError;
		strError.Format(_T("Couldn't find connection point via event GUID."));
		__ERROR__((LPCSTR)strError, 0);

  		return false;
  	}//if

	m_pPPTEventSink = new CPPTEventSink(this, m_strPresentationName);
  	
	IUnknown *pUnk = NULL;
  	m_pPPTEventSink->QueryInterface(IID_IUnknown, (void**)&pUnk);
  	if(pUnk == NULL)
	{
		CString strError;
		strError.Format(_T("PPTEventSink fail to QueryInterface."));
		__ERROR__((LPCSTR)strError, 0);
	
  		return false;
	}//if
  		
  	// Setup advisory connection!
  	hr = m_pPPTEventConnectionPoint->Advise(pUnk, &m_pPPTEventSink->m_Cookie);
  	if(!SUCCEEDED(hr))
	{
		CString strError;
		strError.Format(_T("PPTEventSink fail to Advise."));
		__ERROR__((LPCSTR)strError, 0);

  		return false;
	}//if
	  
  	// Release IConnectionPointContainer interface.
  	pConnPtContainer->Release();

	return true; 
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파워포인트 이벤트 싱크 해제 함수 \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::DisconnectPPTEventSink()
{
	if(m_pPPTEventConnectionPoint)
	{
		m_pPPTEventConnectionPoint->Unadvise(m_pPPTEventSink->m_Cookie); // PPT releases cookie	
 		m_pPPTEventConnectionPoint->Release();
		m_pPPTEventConnectionPoint = NULL;
	}//if

	if(m_pPPTEventSink)
	{
		m_pPPTEventSink->Release();
		m_pPPTEventSink = NULL;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼가 시작되었음을 설정 \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::SlideShowBegin()
{
	__DEBUG__("SlideShowBegin()", _NULL);
	if(m_pptApp == NULL)
	{
		return;
	}//if
	__DEBUG__("SlideShowBegin()", _NULL);

	//Invalidate(FALSE);	//배경화면을 다시 그려준다

	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowWindow		pptSlideShowWnd;
	
	pDisp = m_pptApp->GetPresentations();
	pptPresentations.AttachDispatch(pDisp);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(m_strPresentationName) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
		__ERROR__("Fail to get presentation", m_strPresentationName);
		Stop();
		return;
	}//if

	pDisp = pptPresentation.GetSlideShowWindow();

	pptSlideShowWnd.AttachDispatch(pDisp);
	pptSlideShowWnd.SetHeight(m_fHeight);
	pptSlideShowWnd.SetWidth(m_fWidth);
	pptSlideShowWnd.SetTop(m_fPosY);
	pptSlideShowWnd.SetLeft(m_fPosX);

	pptSlideShowWnd.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();

	//슬라이드 쇼 창을 topMost로 설정
	m_bTopMost = true;
	//프로그램의 Top Most 속성을 해제해 주어야 뷰어가 위로 올라온다
	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	pclsHost->SetTopMost(false);
	//pclsHost->SetForegroundWindow();

	SetTopMostSlideShow();
	SetTimer(ID_PRESENTAION_SHOW, TIME_PRESENTAION_SHOW, NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼가 끝났음을 설정 \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::SlideShowEnd()
{
	__DEBUG__("SlideShowEnd", NULL);
	if(m_pptApp == NULL)
	{
		return;
	}//if
	__DEBUG__("SlideShowEnd 1", NULL);

	DisconnectPPTEventSink();

	//__DEBUG__("SlideShowEnd 2", NULL);
	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if
	//__DEBUG__("SlideShowEnd 3", NULL);

	/*
	HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SLIDE_WAIT));
	m_bgImage.CreateFromHBITMAP(hBitmap);

	Invalidate(FALSE);		//배경을 다시 그려준다.
	*/

	KillTimer(ID_PRESENTAION_SHOW);
	m_bTopMost = false;
	SetTopMostSlideShow();
	//__DEBUG__("SlideShowEnd 4", NULL);

	//프로그램의 Top Most 속성을 설정
	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	pclsHost->SetTopMost(true);
	//__DEBUG__("SlideShowEnd 5", NULL);

	ClosePresentation();
	//__DEBUG__("SlideShowEnd 6", NULL);

	//SetTimer(ID_PRESENTAION_END, TIME_PRESENTAION_END, NULL);
	m_dwStartTick = m_dwStartTick - (TIME_PPT_PLAYING*1000);
	__DEBUG__("SlideShowEnd End", NULL);

}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ppt 슬라이드 쇼를 닫는다 \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::ClosePresentation()
{
	__DEBUG__("ClosePresentation Start", NULL);

	if(m_pptApp == NULL)
	{
		return;
	}//if
	//__DEBUG__("ClosePresentation 1", NULL);

	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	
	//__DEBUG__("ClosePresentation 1.1", NULL);
	pDisp = m_pptApp->GetPresentations();
	//__DEBUG__("ClosePresentation 1.2", NULL);
	pptPresentations.AttachDispatch(pDisp);

	//__DEBUG__("ClosePresentation 2", NULL);
	//열려있는 presentation중에 같은 파일이 있는지 검사하여
	//이미 열려있다면 닫아주어야 한다.
	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;

	//__DEBUG__("ClosePresentation 3", NULL);
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		//__DEBUG__("ClosePresentation 3.1", NULL);
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		//__DEBUG__("ClosePresentation 3.2", NULL);
		if(strFullName.CompareNoCase(m_strPresentationName) == 0)
		{
			//__DEBUG__("ClosePresentation 3.3", NULL);
			pptPresentation.Close();
			pptPresentation.ReleaseDispatch();
			//__DEBUG__("ClosePresentation 3.4", NULL);
			break;
		}//if
		//__DEBUG__("ClosePresentation 3.5", NULL);
		pptPresentation.ReleaseDispatch();

		pDisp = m_pptApp->GetPresentations();
		//__DEBUG__("ClosePresentation 3.6", NULL);
		if(pDisp)
		{
		//__DEBUG__("ClosePresentation 3.7", NULL);
			pptPresentations.ReleaseDispatch();
			pptPresentations.AttachDispatch(pDisp);
		//__DEBUG__("ClosePresentation 3.8", NULL);

		}
		else
		{
		//__DEBUG__("ClosePresentation 3.9", NULL);
			break;
		}//if
	}//for
	//__DEBUG__("ClosePresentation 4", NULL);
	pptPresentations.ReleaseDispatch();
	__DEBUG__("ClosePresentation End", NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// EnumWindow를 통한 슬라이드쇼 윈도우를 찾는 call back 함수 \n
/// @param (HWND) hwnd : (in) 검색된 윈도우 핸들
/// @param (LPARAM) lParam : (in/out) 슬라이드 쇼 윈도우 정보를 갖는 ST_FindWnd 구조체
/// @return <형: BOOL> \n
///			<TRUE: 계속 검색을 진행한다> \n
///			<FALSE: 검색을 종료> \n
/////////////////////////////////////////////////////////////////////////////////
BOOL CAnnounce_ppt_Dlg::EnumWndProc(HWND hwnd, LPARAM lParam)
{
	/*
	EnumWindows 를 사용하면 작업 표시줄에 나타나는 응용 프로그램을 열거할 수 있다.
	작업 관리자는 화면에 표시되는  top 윈도우 중에서 다음 속성을 만족하는 것을 열거한다.
	 
	WS_EX_APPWINDOW 속성을 가지고 있다.
	WS_EX_TOOLWINDOW 속성을 가지고 있지 않으면서, 소유주(owner) 윈도우가 없다.
	 
	위의 두 가지 조건을 체크하는 코드를 만들어 보면 다음과 같다.
	*/
	ST_FindWnd* pstFindWnd = (ST_FindWnd*)lParam;
	if(pstFindWnd == NULL)
	{
		__DEBUG__("Find window struct is null", _NULL);
		return FALSE;
	}//if

	//파워포인트 메인창과 슬라이드 쇼 창 2개가 있는데,
	//Microsoft PowerPoint - [SQI.ppt [호환 모드]] <== (한글)메인 파워포인트 창의 title
	//Microsoft PowerPoint <== (일본)메인 파워포인트 창의 title
	//PowerPoint 슬라이드 쇼 - [SQI.ppt [호환 모드]] <== 슬라이드 쇼 창의title
	//따라서, "Microsoft PowerPoint"가 있는 창은 파워포인트 메인창이고
	//"PowerPoint"와 슬라이드 쇼 파일명이 있는 창이 찾는 슬라이드 쇼 창이다.
	//단, 일본 office에서는 파일의 확장자(".ppt")가 title바에 나오지 않는다.
	char szTitle[256] = { 0x00 };
	::GetWindowText(hwnd, szTitle, 256);
	CString strTitle = szTitle;
	//__DEBUG__("Slide show", pstFindWnd->szSlideName);
	//__DEBUG__("Enum window", strTitle);

	//슬라이드 쇼 파일명이 있어야 한다.
	if(strTitle.Find(pstFindWnd->szSlideName, 0) == -1)
	{
		return TRUE;
	}//if

	//파워포인트 메인 창
	if(strTitle.Find("Microsoft PowerPoint", 0) != -1)
	{
		return TRUE;
	}//if

	//우리가 찾는 슬라이드 창
	if(strTitle.Find("PowerPoint", 0) != -1)
	{
		DWORD exStyle = GetWindowLong(hwnd, GWL_EXSTYLE);

		BOOL isVisible = ::IsWindowVisible(hwnd);
		BOOL isToolWindow = (exStyle & WS_EX_TOOLWINDOW);
		BOOL isAppWindow = (exStyle & WS_EX_APPWINDOW);
		BOOL isOwned = ::GetWindow(hwnd, GW_OWNER) ? TRUE : FALSE;

		if(isVisible && (isAppWindow || (!isToolWindow && !isOwned)))
		{
			// 응용 프로그램
			__DEBUG__("Success enum window", strTitle);
			pstFindWnd->hWnd = hwnd;

			return FALSE;
		}//if
	}//if	

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼 창을 TopMost로 설정한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::SetTopMostSlideShow()
{
	ST_FindWnd stFindWnd;
	memset(&stFindWnd, 0x00, sizeof(ST_FindWnd));
	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(m_strMediaFullPath, cDrive, cPath, cFilename, cExt);
	strcpy(stFindWnd.szSlideName, cFilename);
	//strcat(stFindWnd.szSlideName, cExt);	//일본 office는 title바에 확장자가 안나온다...
	EnumWindows(EnumWndProc, (LPARAM)&stFindWnd);

	if(stFindWnd.hWnd == NULL)
	{
		__ERROR__("Fail to enum slideshow window", m_strPresentationName);
		return;
	}//if

	if(m_bTopMost)
	{
		::SetWindowPos(stFindWnd.hWnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
		::ShowWindow(stFindWnd.hWnd, SW_SHOW);
		::SetForegroundWindow(stFindWnd.hWnd);
		//KillTimer(ID_PRESENTAION_SHOW);
	}
	else
	{
		::SetWindowPos(stFindWnd.hWnd, HWND_NOTOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
		::ShowWindow(stFindWnd.hWnd, SW_HIDE);
	}//if


	//파워포인트 타이틀 이름으로 창의 핸들을 찾아 TopMost로 설정
	// (예 : PowerPoint 슬라이드 쇼 - [UVC.pps [호환 모드]])
	//HWND hWndPPT;	//PPT slideshow 윈도우 핸들

/*
	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(m_strMediaFullPath, cDrive, cPath, cFilename, cExt);

	CString strWndTitle, strReadOnlyWndTitle, strShow, strMode, strReadOnly;
	switch(m_nLocale)
	{
	case 81 :		//일본
		{
			char szShow[15] = { 0x83, 0x58, 0x83, 0x89, 0x83, 0x43, 0x83, 0x68, 0x83, 0x56, 0x83, 0x87, 0x81, 0x5b, 0x00 };
			char szMode[11] = { 0x8c, 0xdd, 0x8a, 0xb7, 0x83, 0x82, 0x81, 0x5b, 0x83, 0x68, 0x00 };
			char szReadOnly[13] = { 0x93, 0xc7, 0x82, 0xdd, 0x8e, 0xe6, 0x82, 0xe8, 0x90, 0xea, 0x97, 0x70, 0x00 };

			strShow = szShow;
			strMode = szMode;
			strReadOnly = szReadOnly;
		}
		break;
	case 1:			//USA
	case 7:			//러시아
	case 86:		//중국
	case 82:		//KOR
	default :
		{
			strShow = "슬라이드 쇼";
			strMode = "호환 모드";
			strReadOnly = "읽기 전용";
		}
	}//switch
	strWndTitle.Format("PowerPoint %s - [%s%s [%s]]", strShow, cFilename, cExt, strMode);
	strReadOnlyWndTitle.Format("PowerPoint %s - [%s%s [%s] [%s]]", strShow, cFilename, cExt, strReadOnly, strMode);
	__DEBUG__("strWndTitle", strWndTitle);
	__DEBUG__("strReadOnlyWndTitle", strReadOnlyWndTitle);
	
	hWndPPT = ::FindWindow(NULL, strWndTitle);
	if(hWndPPT == NULL)
	{
		hWndPPT = ::FindWindow(NULL, strReadOnlyWndTitle);
		if(hWndPPT == NULL)
		{
			__DEBUG__("PPT window find fail !!!", _NULL);
		}//if
	}//if
*/


/*
	//Title로 창을 찾는 로직이 일본어에서 문제가 있어서 수정함
	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(m_strMediaFullPath, cDrive, cPath, cFilename, cExt);
	CString strMediaName, strWndTitle;
	strMediaName.Format("%s%s", cFilename, cExt);

	scratchUtil* aUtil = scratchUtil::getInstance();
	hWndPPT = aUtil->getWHandle("POWERPNT.EXE");
	char szTitle[256] = { 0x00 };
	::GetWindowText(hWndPPT, szTitle, 256);
	strWndTitle = szTitle;
	if(strWndTitle.Find(strMediaName, 0) == -1)
	{
		//BOOL bRet = EnumChildWindows(hWndPPT, &EnumChildProc, (LPARAM)(LPCSTR)strWndTitle);
		HWND hChild = NULL;
		do
		{
			HWND hChild = ::GetWindow(hWndPPT, GW_CHILD);
			if(hChild != NULL)
			{
				::GetWindowText(hWndPPT, szTitle, 256);
				strWndTitle = szTitle;

				if(strWndTitle.Find(strMediaName, 0) != -1)
				{
					hWndPPT = hChild;
					break;
				}//if
			}//if
		} while(hChild);

		if(hChild == NULL)
		{
			__DEBUG__("Can't found ppt window", _NULL);
			hWndPPT = NULL;
		}//if
	}//if

	__DEBUG__("WindowTitle", strWndTitle);
	__DEBUG__("PPT topmost", m_bTopMost);

	if(m_bTopMost)
	{
		if(hWndPPT)
		{
			::SetWindowPos(hWndPPT, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
			::ShowWindow(hWndPPT, SW_SHOW);
			::SetForegroundWindow(hWndPPT);
			
		}//if
	}
	else
	{
		if(hWndPPT)
		{
			::SetWindowPos(hWndPPT, HWND_NOTOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
			::ShowWindow(hWndPPT, SW_HIDE);
		}//if
		hWndPPT = NULL;
	}//if
*/
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼를 일정한 시간후에 종료하도록 설정 \n
/// @param (float) ftTime : (in) 슬라이드의 재생 시간
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::SetSlideShowEnd(float ftTime)
{
	//SetTimer(ID_SLIDE_SHOW_END, TIME_SLIDE_SHOW_END, NULL);
	int nTime = (int)(ftTime*1000);
	SetTimer(ID_SLIDE_SHOW_END, nTime, NULL);
}

void CAnnounce_ppt_Dlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//CWnd::OnTimer(nIDEvent);
	CAnnounceDlg::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
		/*
	case SCHEDULE_SHOW_ID:
		{
			KillTimer(SCHEDULE_SHOW_ID);
			ShowWindow(SW_SHOW);
		}
		break;

	case SCHEDULE_HIDE_ID:
		{
			KillTimer(SCHEDULE_HIDE_ID);
			ShowWindow(SW_HIDE);
		}
		break;
		*/
	case ID_SLIDE_SHOW_END:
		{
			KillTimer(ID_SLIDE_SHOW_END);
			//Stop();
			m_dwStartTick = m_dwStartTick - (TIME_PPT_PLAYING*1000);
		}
		break;
		/*
	case ID_PRESENTAION_END:
		{
			KillTimer(ID_PRESENTAION_END);
			m_dwStartTick = m_dwStartTick - (TIME_PPT_PLAYING*1000);
		}
		break;
		*/
	case ID_PRESENTAION_SHOW:
		{
			SetTopMostSlideShow();
		}
		break;
	}//switch

	//CSchedule::OnTimer(nIDEvent);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼의 해상도를 시스템의 해상도에 맞춘다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::SetSlideShowResolution()
{
	HKEY hNewKey = NULL;
	LONG lRet = 0;

	int nX, nY, nCx, nCy;
	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	pclsHost->GetAppPosition(nX, nY, nCx, nCy);

	CString strVersion = m_pptApp->GetVersion();
	CString strKey;
	strKey.Format("Software\\Microsoft\\Office\\%s\\PowerPoint\\Options", strVersion);

	//X
	lRet = RegCreateKeyEx(HKEY_CURRENT_USER, strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hNewKey, NULL);
	if(lRet == ERROR_SUCCESS)
	{
		lRet = RegSetValueEx(hNewKey, "SlideShowResolutionX", 0, REG_DWORD, (LPBYTE)&nCx, sizeof(DWORD));
		if(lRet != ERROR_SUCCESS)
		{
			__DEBUG__("SlideShowResolutionX set fail", nCx);
		}//if
		RegCloseKey(hNewKey);
	}//if

	//Y
	lRet = RegCreateKeyEx(HKEY_CURRENT_USER, strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hNewKey, NULL);
	if(lRet == ERROR_SUCCESS)
	{
		lRet = RegSetValueEx(hNewKey, "SlideShowResolutionY", 0, REG_DWORD, (LPBYTE)&nCy, sizeof(int));
		if(lRet != ERROR_SUCCESS)
		{
			__DEBUG__("SlideShowResolutionX set fail", nCy);
		}//if
		RegCloseKey(hNewKey);
	}//if
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Presentation이 Open 된 이후 슬라이드 쇼를 시작하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::PresentationOpen()
{
	__DEBUG__("PresentationOpen() 1",NULL);
	if(m_pptApp == NULL)
	{
		return;
	}//if
	__DEBUG__("PresentationOpen() 2",NULL);

	//Invalidate(FALSE);	//배경화면을 다시 그려준다
	
	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowSettings	pptSlideshow;			///<MS-PowerPoint SlideShow settings
	
	pDisp = m_pptApp->GetPresentations();
	pptPresentations.AttachDispatch(pDisp);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(m_strPresentationName) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
		__ERROR__("Fail to get presentation", m_strPresentationName); 
		Stop();
		return;
	}//if

	pDisp = pptPresentation.GetSlideShowSettings();
	pptSlideshow.AttachDispatch(pDisp);
	pptSlideshow.SetLoopUntilStopped(-1);		//msoTrue
	pptSlideshow.Run();

	pptSlideshow.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
}
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파워포인트에 ESC 키가 눌러져서 BRW를 종료한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_ppt_Dlg::EscapeBRW()
{
	//SlideShowEnd();

	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	::PostMessage(pclsHost->m_hWnd, WM_SEND_STOP_BRW, 0, 0);
}
