// MenuBarDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "MenuBarDlg.h"
#include "Host.h"
//#include "AudioMixer.h"
#include "MemDC.h"

#define		TIMER_MENUBAR_SHOW_ID			7000	// id
#define		TIME_MENUBAR_SHOW_TIME			10		// time(ms)

#define		TIMER_MENUBAR_HIDE_ID			7001	// id
#define		TIME_MENUBAR_HIDE_TIME			10		// time(ms)

//#define		SLIDE_MENU_BAR					10		//메뉴바 슬리이드 모드


// CMenuBarDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMenuBarDlg, CDialog)

CMenuBarDlg::CMenuBarDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMenuBarDlg::IDD, pParent)
	, m_nPosX(0)
	, m_nPosY(0)
	, m_nCx(0)
	, m_nCy(0)
	, m_bVisible(false)
	, m_hWndMenuBtn(NULL)
{
	HBITMAP hBitmapTop = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BG_MENU_BAR));
	m_bgImage.CreateFromHBITMAP(hBitmapTop);
}

CMenuBarDlg::~CMenuBarDlg()
{
	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if
}

void CMenuBarDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PREVIOUS_TEMPLATE_BTN, m_btnPreviousTemplate);
	DDX_Control(pDX, IDC_NEXT_SCHEDULE_BTN, m_btnNextSchedule);
	DDX_Control(pDX, IDC_NEXT_TEMPLATE_BTN, m_btnNextTemplate);
	DDX_Control(pDX, IDC_INCREASE_VOLUME_BTN, m_btnIncreaseVolume);
	DDX_Control(pDX, IDC_DECREASE_VOLUME_BTN, m_btnDecreaseVolume);
	DDX_Control(pDX, IDC_SOUND_MUTE_BTN, m_btnSoundMute);
	DDX_Control(pDX, IDC_PLAY_TIME_BTN, m_btnPlayTime);
	DDX_Control(pDX, IDC_FULL_SCREEN_BTN, m_btnFullScreen);
	DDX_Control(pDX, IDC_EXIT_BTN, m_btnExit);
	DDX_Control(pDX, IDC_HELP_BTN, m_btnHelp);
	DDX_Control(pDX, IDC_EMPTY1_BTN, m_btnEmpty1);
	DDX_Control(pDX, IDC_EMPTY2_BTN, m_btnEmpty2);
}


BEGIN_MESSAGE_MAP(CMenuBarDlg, CDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_PREVIOUS_TEMPLATE_BTN, &CMenuBarDlg::OnBnClickedPreviousTemplateBtn)
	ON_BN_CLICKED(IDC_NEXT_SCHEDULE_BTN, &CMenuBarDlg::OnBnClickedNextScheduleBtn)
	ON_BN_CLICKED(IDC_NEXT_TEMPLATE_BTN, &CMenuBarDlg::OnBnClickedNextTemplateBtn)
	ON_BN_CLICKED(IDC_INCREASE_VOLUME_BTN, &CMenuBarDlg::OnBnClickedIncreaseVolumeBtn)
	ON_BN_CLICKED(IDC_DECREASE_VOLUME_BTN, &CMenuBarDlg::OnBnClickedDecreaseVolumeBtn)
	ON_BN_CLICKED(IDC_SOUND_MUTE_BTN, &CMenuBarDlg::OnBnClickedSoundMuteBtn)
	ON_BN_CLICKED(IDC_PLAY_TIME_BTN, &CMenuBarDlg::OnBnClickedPlayTimeBtn)
	ON_BN_CLICKED(IDC_FULL_SCREEN_BTN, &CMenuBarDlg::OnBnClickedFullScreenBtn)
	ON_BN_CLICKED(IDC_EXIT_BTN, &CMenuBarDlg::OnBnClickedExitBtn)
	ON_BN_CLICKED(IDC_HELP_BTN, &CMenuBarDlg::OnBnClickedHelpBtn)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_EMPTY1_BTN, &CMenuBarDlg::OnBnClickedEmpty1Btn)
	ON_BN_CLICKED(IDC_EMPTY2_BTN, &CMenuBarDlg::OnBnClickedEmpty2Btn)
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CMenuBarDlg 메시지 처리기입니다.


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 메뉴바의 위치를 설정 \n
/// @param (int) nPosX : (in) X 위치
/// @param (int) nPosY : (in) Y 위치
/// @param (int) nCx : (in) 넓이
/// @param (int) nCy : (in) 높이
/////////////////////////////////////////////////////////////////////////////////
void CMenuBarDlg::SetPosition(int nPosX, int nPosY, int nCx, int nCy)
{
	//전체 Cx 넓이중에서 메뉴바가 차지하는 가로 영역은 WIDTH_MENU_BAR 이고,
	//화면의 가운데 출력되어야 한다.
	//다이얼로그의 사이즈를 조정하지 않고 화면밖에서 조금씩 안쪽으로 이동(리사이즈시 CPU 점유율이 올라간다)
#ifdef SLIDE_MENU_BAR
	m_nPosX		= nPosX + (nCx-WIDTH_MENU_BAR)/2;
	m_nPosY		= nPosY;
	m_nCx		= WIDTH_MENU_BAR;
	m_nCy		= nCy;
	MoveWindow(m_nPosX, m_nPosY+nCy, m_nCx, m_nCy);
#else
	m_nPosX		= nPosX + (nCx-WIDTH_MENU_BAR)/2;
	m_nPosY		= nPosY;
	m_nCx		= WIDTH_MENU_BAR;
	m_nCy		= nCy;
	MoveWindow(m_nPosX, m_nPosY, m_nCx, m_nCy);
#endif
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 메뉴바를 화면에 보이도록 한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CMenuBarDlg::ShowMenuBar()
{
#ifdef SLIDE_MENU_BAR
	SetSoundMuteImage();
	SetTimer(TIMER_MENUBAR_SHOW_ID, TIME_MENUBAR_SHOW_TIME, NULL);
	ShowWindow(SW_SHOW);

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
		/*SetWindowPos(&wndTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
		::SetWindowPos(m_hWndMenuBtn, HWND_TOP, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);*/
	}//if
#else
	m_bVisible = true;
	SetSoundMuteImage();
	ShowWindow(SW_SHOW);

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
#endif
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 메뉴바를 화면에서 안보이도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CMenuBarDlg::HideMenuBar()
{
#ifdef SLIDE_MENU_BAR
	SetTimer(TIMER_MENUBAR_HIDE_ID, TIME_MENUBAR_HIDE_TIME, NULL);
	
	if(m_hWndMenuBtn)
	{
		//::SetWindowPos(m_hWndMenuBtn, HWND_TOP, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
	}//if
#else
	m_bVisible = false;
	ShowWindow(SW_HIDE);
	
	if(m_hWndMenuBtn)
	{
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
#endif
}


void CMenuBarDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == TIMER_MENUBAR_SHOW_ID)
	{
		KillTimer(TIMER_MENUBAR_HIDE_ID);
		CRect rectClient;
		GetClientRect(&rectClient);

		int nHeight = rectClient.Height()+10;
		if(nHeight > m_nCy)
		{
			nHeight = m_nCy;
			m_bVisible = true;
			KillTimer(TIMER_MENUBAR_SHOW_ID);
			Invalidate(TRUE);

			if(m_hWndMenuBtn)
			{
				SetForegroundWindow();
				::SetForegroundWindow(m_hWndMenuBtn);
			}//if
		}//if

		int nTop = m_nPosY + (m_nCy-nHeight);
		MoveWindow(m_nPosX, nTop, m_nCx, nHeight);
	}
	else
	{
		m_bVisible = false;
		KillTimer(TIMER_MENUBAR_SHOW_ID);
		CRect rectClient;
		GetClientRect(&rectClient);

		int nHeight = rectClient.Height()-10;
		if(nHeight < 0)
		{
			nHeight = 0;
			ShowWindow(SW_HIDE);
			KillTimer(TIMER_MENUBAR_HIDE_ID);

			if(m_hWndMenuBtn)
			{
				::SetForegroundWindow(m_hWndMenuBtn);
			}//if
		}//if
		int nTop = m_nPosY + (m_nCy-nHeight);
		MoveWindow(m_nPosX, nTop, m_nCx, nHeight);
	}//if

	//Invalidate(TRUE);

	CDialog::OnTimer(nIDEvent);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 메뉴바의 보여지는 상태를 반환 한다. \n
/// @return <형: bool> \n
///			<true: 화면에 보여지는 상태> \n
///			<false: 화면에 보여지지 않는 상태> \n
/////////////////////////////////////////////////////////////////////////////////
bool CMenuBarDlg::GetVisible()
{
	return m_bVisible;
}

void CMenuBarDlg::OnBnClickedPreviousTemplateBtn()
{
	__DEBUG__("OnBnClickedPreviousTemplateBtn", _NULL);

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	m_pParentWnd->PostMessage(WM_PLAY_PREV_TEMPLATE, 0, 0);

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnBnClickedNextScheduleBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	m_pParentWnd->PostMessage(ID_PLAY_NEXT_SCHEDULE, 0, 0);

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnBnClickedNextTemplateBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	m_pParentWnd->PostMessage(WM_PLAY_NEXT_TEMPLATE, 0, 0);

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnBnClickedIncreaseVolumeBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	//CAudioMixer::getInstance()->IncreaseVolume();
	CHost* pHost = (CHost*)m_pParentWnd;
	pHost->IncreaseVolume();

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnBnClickedDecreaseVolumeBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	//CAudioMixer::getInstance()->DecreaseVolume();
	CHost* pHost = (CHost*)m_pParentWnd;
	pHost->DecreaseVolume();

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnBnClickedSoundMuteBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	CHost* pHost = (CHost*)m_pParentWnd;
	bool bSound = pHost->UpdateSoundState();
	pHost->SetSoundValue(!bSound);	
	//SetSoundMuteImage();

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnBnClickedPlayTimeBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	CHost* pHost = (CHost*)m_pParentWnd;
	pHost->ViewTimeDialog();

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnBnClickedFullScreenBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	CHost* pHost = (CHost*)m_pParentWnd;
	pHost->ToggleFullScreen();
	SetFullScreenImage();

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnBnClickedExitBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	HideMenuBar();
	m_pParentWnd->PostMessage(WM_SEND_STOP_BRW, 0, 0);
}

void CMenuBarDlg::OnBnClickedHelpBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!m_bVisible || !m_pParentWnd)
	{
		return;
	}//if

	CHost* pHost = (CHost*)m_pParentWnd;
	pHost->ViewHelp();

	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

BOOL CMenuBarDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_btnPreviousTemplate.SetWindowText("Previous template");
	m_btnPreviousTemplate.LoadBitmap(IDB_BTN_PREVIOUS_TEMPLATE, RGB(255, 255, 255));
	m_btnPreviousTemplate.SetToolTipText("Move to previous template");

	m_btnNextSchedule.SetWindowText("Next schedule");
	m_btnNextSchedule.LoadBitmap(IDB_BTN_NEXT_SCHEDULE, RGB(255, 255, 255));
	m_btnNextSchedule.SetToolTipText("Play next schedule");

	m_btnNextTemplate.SetWindowText("Next template");
	m_btnNextTemplate.LoadBitmap(IDB_BTN_NEXT_TEMPLATE, RGB(255, 255, 255));
	m_btnNextTemplate.SetToolTipText("Move to next template");

	m_btnIncreaseVolume.SetWindowText("Increase volume");
	m_btnIncreaseVolume.LoadBitmap(IDB_BTN_INCREASE_VOLUME, RGB(255, 255, 255));
	m_btnIncreaseVolume.SetToolTipText("Volume up");

	m_btnDecreaseVolume.SetWindowText("Volume down");
	m_btnDecreaseVolume.LoadBitmap(IDB_BTN_DECREASE_VOLUME, RGB(255, 255, 255));
	m_btnDecreaseVolume.SetToolTipText("Decrease volume");

	m_btnSoundMute.SetWindowText("Toggle sound on/off");
	SetSoundMuteImage();
	m_btnSoundMute.SetToolTipText("Toggle sound mute");

	m_btnPlayTime.SetWindowText("Play dime dialog");
	m_btnPlayTime.LoadBitmap(IDB_BTN_TIMER_DLG, RGB(255, 255, 255));
	m_btnPlayTime.SetToolTipText("Show/Hide play time");

	m_btnFullScreen.SetWindowText("Full screen");
	m_btnFullScreen.LoadBitmap(IDB_BTN_WINDOW, RGB(255, 255, 255));
	m_btnFullScreen.SetToolTipText("Window mode");

	m_btnExit.SetWindowText("Exit");
	m_btnExit.LoadBitmap(IDB_BTN_EXIT, RGB(255, 255, 255));
	m_btnExit.SetToolTipText("Exit UBCPlayer");

	m_btnHelp.SetWindowText("Help");
	m_btnHelp.LoadBitmap(IDB_BTN_HELP, RGB(255, 255, 255));
	m_btnHelp.SetToolTipText("View help");

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CMenuBarDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CMenuBarDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
	CMemDC memDC(&dc);

	if(m_bgImage.IsValid())
	{
		CRect rect;
		GetClientRect(rect);

		m_bgImage.Draw2(memDC.m_hDC, rect);
	}//if	
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Sound On/Off 버튼의 이미지를 설정 \n
/////////////////////////////////////////////////////////////////////////////////
void CMenuBarDlg::SetSoundMuteImage()
{
	if(!m_pParentWnd)
	{
		return;
	}//if

	CHost* pHost = (CHost*)m_pParentWnd;
	if(pHost->UpdateSoundState())
	{
		__DEBUG__("IDB_BTN_SOUND_ON", _NULL);
		m_btnSoundMute.LoadBitmap(IDB_BTN_SOUND_ON, RGB(255, 255, 255));
	}
	else
	{
		__DEBUG__("IDB_BTN_SOUND_OFF", _NULL);
		m_btnSoundMute.LoadBitmap(IDB_BTN_SOUND_OFF, RGB(255, 255, 255));
	}//if
	m_btnSoundMute.Invalidate(TRUE);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Full screen/Window 버튼의 이미지를 설정 \n
/// @param (bool) bOn : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CMenuBarDlg::SetFullScreenImage()
{
	if(!m_pParentWnd)
	{
		return;
	}//if

	CHost* pHost = (CHost*)m_pParentWnd;
	if(pHost->GetFullScreen())
	{
		m_btnFullScreen.LoadBitmap(IDB_BTN_WINDOW, RGB(255, 255, 255));
	}
	else
	{
		m_btnFullScreen.LoadBitmap(IDB_BTN_FULL_SCREEN, RGB(255, 255, 255));
	}//if
	m_btnFullScreen.Invalidate(TRUE);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 메뉴버튼의 윈도우 핸들을 설정 \n
/// @param (HWND) hWnd : (in) 메뉴버튼의 윈도우 핸들
/////////////////////////////////////////////////////////////////////////////////
void CMenuBarDlg::SetMenuButtonHandle(HWND hWnd)
{
	m_hWndMenuBtn = hWnd;
}

BOOL CMenuBarDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	//if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
	//{
	//	return TRUE;
	//}//if

	if(pMsg->message == WM_KEYDOWN)
	{
		::AfxGetMainWnd()->PreTranslateMessage(pMsg);
		return true;
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}

void CMenuBarDlg::OnBnClickedEmpty1Btn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnBnClickedEmpty2Btn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if
}

void CMenuBarDlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if

	CDialog::OnRButtonUp(nFlags, point);
}

void CMenuBarDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(m_hWndMenuBtn)
	{
		SetForegroundWindow();
		::SetForegroundWindow(m_hWndMenuBtn);
	}//if

	CDialog::OnLButtonUp(nFlags, point);
}

void CMenuBarDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);

	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}
