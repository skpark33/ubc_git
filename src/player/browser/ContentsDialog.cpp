// ContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "ContentsDialog.h"

#include "Schedule.h"


// CContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CContentsDialog, CDialog)

CContentsDialog::CContentsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CContentsDialog::IDD, pParent)
{

}

CContentsDialog::~CContentsDialog()
{
}

void CContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_TYPE, m_cbxContentsType);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILENAME, m_editContentsFilename);
	DDX_Control(pDX, IDC_BUTTON_BROWSE, m_btnBrowse);
	DDX_Control(pDX, IDOK, m_btnChange);
}


BEGIN_MESSAGE_MAP(CContentsDialog, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, &CContentsDialog::OnBnClickedButtonBrowse)
	ON_BN_CLICKED(IDC_BUTTON_RECOVERY, &CContentsDialog::OnBnClickedButtonRecovery)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CContentsDialog 메시지 처리기입니다.

BOOL CContentsDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_cbxContentsType.AddString("Video");
	m_cbxContentsType.AddString("Ticker");
	m_cbxContentsType.AddString("Image");
	m_cbxContentsType.AddString("Promotion");
	m_cbxContentsType.AddString("TV");
	m_cbxContentsType.AddString("Ticker");
	m_cbxContentsType.AddString("Phone");
	m_cbxContentsType.AddString("Web");
	m_cbxContentsType.AddString("Flash");
	m_cbxContentsType.AddString("WebCam");
	m_cbxContentsType.AddString("RSS");
	m_cbxContentsType.AddString("Typing");
	m_cbxContentsType.AddString("MS Power Point");

	m_cbxContentsType.SetCurSel(m_info.contentsType);


	CString filter;
	switch(m_info.contentsType)
	{
	case CONTENTS_VIDEO:
	case CONTENTS_SMS:
	case CONTENTS_IMAGE:
	case CONTENTS_TYPING:
	case CONTENTS_TICKER:
	case CONTENTS_FLASH:
	case CONTENTS_FLASH_TEMPLATE:
	case CONTENTS_DYNAMIC_FLASH:
	case CONTENTS_PPT:
		m_btnBrowse.EnableWindow(TRUE);
		m_btnChange.EnableWindow(TRUE);
		break;

	case CONTENTS_RSS:
	case CONTENTS_WEBBROWSER:
	case CONTENTS_WEBCAM:
	case CONTENTS_PHONE:
	case CONTENTS_TV:
	case CONTENTS_PROMOTION:
	default:
		m_btnBrowse.EnableWindow(FALSE);
		m_btnChange.EnableWindow(FALSE);
		break;
	}

	m_editContentsFilename.SetWindowText(m_info.filename.c_str());

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CContentsDialog::OnBnClickedButtonBrowse()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString filter;
	switch(m_info.contentsType)
	{
	case CONTENTS_VIDEO:
		{
			filter = "All Video Files (*.avi; *.asf; *.mkv; *.mp4; *.mpg; *.mpeg; *.wmv)|*.avi; *.asf; *.mkv; *.mp4; *.mpg; *.mpeg; *.wmv||";
		}
		break;

	case CONTENTS_SMS:
	case CONTENTS_IMAGE:
	case CONTENTS_TYPING:
	case CONTENTS_TICKER:
		{
			filter = "All Image Files (*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff)|*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff||";
		}
		break;

	case CONTENTS_FLASH:
	case CONTENTS_FLASH_TEMPLATE:
	case CONTENTS_DYNAMIC_FLASH:
		{
			filter = "Flash Files (*.swf)|*.swf||";
		}
		break;

	case CONTENTS_PPT:
		{
			filter = "MS Power Point Files (*.ppt; *.pps)|*.ppt; *.pps||";
		}
		break;
/*
	case CONTENTS_RSS:
	case CONTENTS_WEBBROWSER:
	case CONTENTS_WEBCAM:
	case CONTENTS_PHONE:
	case CONTENTS_TV:
	case CONTENTS_PROMOTION:
	default:
		filter = "All Files (*.*)|*.*||";
		break;*/
	}

	CFileDialog dlg(TRUE, _T(""), _T(""), OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, filter, this);;

	if(dlg.DoModal() == IDOK)
	{
		m_editContentsFilename.SetWindowText(dlg.GetPathName());
	}
}

void CContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CString full_path;
	m_editContentsFilename.GetWindowText(full_path);

	__DEBUG__("Change Contents", full_path);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(full_path, cDrive, cPath, cFilename, cExt);
	CString strNewFile;
	strNewFile.Format("%s%s", cFilename, cExt);

	cciEvent* new_event = new cciEvent();

	cciTime now;
	new_event->setEventTime(now);
	new_event->setEventType("fileUpdated");
	
	new_event->addItem("siteId", ::GetSiteName());
	new_event->addItem("hostId", ::GetHostName());
	new_event->addItem("contentsId", m_info.contentsId);
	new_event->addItem("location", m_info.location);
	//new_event->addItem("filename", m_info.filename);
	new_event->addItem("filename", strNewFile);
	new_event->addItem("contentschange", m_info.filename);

	//::AfxGetMainWnd()->SendMessage(WM_FILE_UPDATE, (WPARAM)new_event);

	CString LocalFullPath, LocalTempFullPath;
	//::GetLocalPath(LocalFullPath, LocalTempFullPath, m_info.location.c_str(), m_info.filename.c_str());
	::GetLocalPath(LocalFullPath, LocalTempFullPath, m_info.location.c_str(), strNewFile);

	if(!::CopyFile(full_path, LocalFullPath, FALSE))
	{
		::AfxMessageBox("Contents File Copy Fail !!!");
	}
	else
		CDialog::OnOK();
}

void CContentsDialog::OnBnClickedButtonRecovery()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	__DEBUG__("Recovery Contents", _NULL);

	cciEvent* new_event = new cciEvent();

	cciTime now;
	new_event->setEventTime(now);
	new_event->setEventType("fileRecovery");
	
	new_event->addItem("siteId", ::GetSiteName());
	new_event->addItem("hostId", ::GetHostName());
	new_event->addItem("contentsId", m_info.contentsId);
	new_event->addItem("location", m_info.location);
	new_event->addItem("filename", m_info.filename);
	new_event->addItem("contentschange", m_info.filename);

	//::AfxGetMainWnd()->SendMessage(WM_FILE_RECOVERY, (WPARAM)new_event);

	//CDialog::OnCancel();
	CDialog::OnOK();
}

void CContentsDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}
