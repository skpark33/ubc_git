#pragma once

#include "afxwin.h"
#include "HoverButton.h"
#include "ximage.h"

//#define		HEIGHT_MENU_BAR				188			///<메뉴바의 높이(2칸 + 4*2의 보더 포함)
#define		HEIGHT_MENU_BAR				220			///<메뉴바의 높이(2칸 + 4*2의 보더 포함)
#define		WIDTH_MENU_BAR				768			///<메뉴바의 넓이(6개의 버튼)
#define		HEIGHT_MENU_BTN				90			///<메뉴바의 버튼한개의 높이
//#define		WIDTH_MENU_BTN				127			///<메뉴바의 버튼한개의 넓이
#define		WIDTH_MENU_BTN				120			///<메뉴바의 버튼한개의 넓이

// CMenuBarDlg 대화 상자입니다.

class CMenuBarDlg : public CDialog
{
	DECLARE_DYNAMIC(CMenuBarDlg)

public:
	CMenuBarDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMenuBarDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MENU_BAR_DLG };

	void		SetPosition(int nPosX, int nPosY, int nCx, int nCy);		///<메뉴바의 위치를 설정
	void		ShowMenuBar(void);											///<메뉴바를 화면에 보이도록 한다
	void		HideMenuBar(void);											///<메뉴바를 화면에서 안보이도록 한다.
	bool		GetVisible(void);											///<메뉴바의 보여지는 상태를 반환 한다.
	void		SetMenuButtonHandle(HWND hWnd);								///<메뉴버튼의 윈도우 핸들을 설정

	virtual		BOOL OnInitDialog();

protected:
	int			m_nPosX;			///<X 위치
	int			m_nPosY;			///<Y 위치
	int			m_nCx;				///<넓이
	int			m_nCy;				///<높이
	bool		m_bVisible;			///<메뉴바가 완전히 보이는 상태인지를 나타내는 falg
	CxImage		m_bgImage;			///<배경 이미지
	HWND		m_hWndMenuBtn;		///<메뉴버튼의 핸들

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedPreviousTemplateBtn();
	afx_msg void OnBnClickedNextScheduleBtn();
	afx_msg void OnBnClickedNextTemplateBtn();
	afx_msg void OnBnClickedIncreaseVolumeBtn();
	afx_msg void OnBnClickedDecreaseVolumeBtn();
	afx_msg void OnBnClickedSoundMuteBtn();
	afx_msg void OnBnClickedPlayTimeBtn();
	afx_msg void OnBnClickedFullScreenBtn();
	afx_msg void OnBnClickedExitBtn();
	afx_msg void OnBnClickedHelpBtn();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();

	CHoverButton		m_btnPreviousTemplate;
	CHoverButton		m_btnNextSchedule;
	CHoverButton		m_btnNextTemplate;
	CHoverButton		m_btnIncreaseVolume;
	CHoverButton		m_btnDecreaseVolume;
	CHoverButton		m_btnSoundMute;
	CHoverButton		m_btnPlayTime;
	CHoverButton		m_btnFullScreen;
	CHoverButton		m_btnExit;
	CHoverButton		m_btnHelp;
	CButton				m_btnEmpty1;
	CButton				m_btnEmpty2;

	void	SetSoundMuteImage(void);		///<Sound On/Off 버튼의 이미지를 설정
	void	SetFullScreenImage(void);		///<Full screen/Window 버튼의 이미지를 설정
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedEmpty1Btn();
	afx_msg void OnBnClickedEmpty2Btn();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
