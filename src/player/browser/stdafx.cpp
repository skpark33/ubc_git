// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// BRW2.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"
#include "BasicInterface.h"


#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383



LPCTSTR GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}

LPCTSTR GetSchedulePath()
{
	static CString config_path = "";

	if(config_path.GetLength() == 0)
	{
		TCHAR cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
		_tsplitpath(::GetAppPath(), cDrive, cPath, cFilename, cExt);

		config_path.Format("%s%sconfig\\%s.ini", cDrive, cPath, ::GetHostName());
	}//if

	return config_path;
}

LPCTSTR	GetSiteName()
{
	static CString strSiteName = "";

	if(strSiteName.GetLength() == 0)
	{
		ciString site_name = "";
		if(ciArgParser::getInstance()->getArgValue("+site", site_name))			//프로그램의 site Id
		{
			strSiteName = site_name.c_str();
		}
		else
		{
			strSiteName = GetINIValue("UBCVariables.ini", "ROOT", "SiteId");	//host의 site Id
		}//if
	}//if

	return strSiteName;
}
// skpark 2012.11.08 스케쥴 유효기간을 무시하지 말도록 설정되어 있는지 검사한다.
// 설정되어있지 않다면 유효기간을 무시한다.
// 설정되어 있다면 유효기간을 무시하지 않는다.
// 
bool DONT_IGNORE_SCHEDULE_ENDDATE()
{
	static int retval = -1;

	if(retval == 1 || retval == 0){
		return (bool)retval;
	}
	
	ciString buf = GetINIValue("UBCVariables.ini", "ROOT", "DONT_IGNORE_SCHEDULE_ENDDATE");	
	if(atoi(buf.c_str())==1) {
		retval = 1;
	}else{
		retval = 0;
	}
	return (bool)retval;
}



LPCTSTR	GetSiteID()
{
	static CString strSiteID = "";

	if(strSiteID.GetLength() == 0)
	{
		strSiteID = GetINIValue("UBCVariables.ini", "ROOT", "SiteId");
	}//if

	return strSiteID;
}

LPCTSTR GetProgramSite()
{
	static CString strProgramSite = "";

	if(strProgramSite.GetLength() == 0)
	{
		ciString str;
		if(ciArgParser::getInstance()->getArgValue("+site", str))
		{
			strProgramSite = str.c_str();
		}//if
	}//if

	return strProgramSite;
}


LPCTSTR	GetHostName()
{
	static CString strHostName = "";

	if(strHostName.GetLength() == 0)
	{
		ciString str;
		if(ciArgParser::getInstance()->getArgValue("+host", str))
		{
			strHostName = str.c_str();
		}
		else
		{
			scratchUtil* aUtil = scratchUtil::getInstance();
			ciString strHost, strMac, strEdition;	
			if(!aUtil->readAuthFile(strHost, strMac, strEdition))
			{
				strHostName = "";
			}
			else
			{
				strHostName = strHost.c_str();
			}//if
		}//if
	}//if

	return strHostName;
}

LPCTSTR	GetAppId()
{
	static CString strAppId = "";

	if(strAppId.GetLength() == 0)
	{
		ciString appId;
		if(ciArgParser::getInstance()->getArgValue("+appId", appId))
		{
			strAppId = appId.c_str();
		}//if
	}//if

	return strAppId;
}

bool GetPhoneConfig(CString& PhoneTemplateId, CString& PhoneFrameId, int& PortNo)
{
	static CString strPhoneTemplateId = "";
	static CString strPhoneFrameId = "";
	static int nPortNo = -1;

	if(strPhoneTemplateId.GetLength() == 0 || strPhoneFrameId.GetLength() == 0 || nPortNo == -1)
	{
		ciProperties scrConfig("BRW");
		ciString cfgFilename = ciEnv::newEnv("PROJECT_CODE");
		cfgFilename += ".properties";
		if ( scrConfig.load("PROJECT_HOME",
				"config",
				cfgFilename.c_str(),
				"Phone") != 0 )
		{
			ciString value;
			ciConfigIterator itrCfg;

			//
			itrCfg = scrConfig.find("TemplateId");
			if ( itrCfg != scrConfig.end() )
			{
				value = (*itrCfg).second;
				strPhoneTemplateId = value.c_str();
			}

			//
			itrCfg = scrConfig.find("FrameId");
			if ( itrCfg != scrConfig.end() )
			{
				value = (*itrCfg).second;
				strPhoneFrameId = value.c_str();
			}

			//
			itrCfg = scrConfig.find("Port");
			if ( itrCfg != scrConfig.end() )
			{
				value = (*itrCfg).second;
				nPortNo = atoi(value.c_str());
			}//if
		}//if
	}//if

	//
	PhoneTemplateId = strPhoneTemplateId;
	PhoneFrameId = strPhoneFrameId;
	PortNo = nPortNo;

	return true;
}

bool GetLocalPath(CString& LocalFullPath, CString& LocalTempFullPath, LPCSTR lpszLocation, LPCSTR lpszFilename)
{
	static CString _LocalPath = _T(""), _LocalTempPath = _T("");

	if(_LocalPath.GetLength() == 0 || _LocalTempPath.GetLength() == 0)
	{
		_LocalPath = GetAppPath();
		_LocalPath += _T("..\\..\\Contents\\ENC\\");

		_LocalTempPath = GetAppPath();
		_LocalTempPath += _T("..\\..\\Contents\\Temp\\");
	}//if

	//공용 컨텐츠의 location 값은 /Ccontents/<패키지명|컨텐츠 guid>/...
	//일반 컨텐츠의 location 값은 /contents/<패키지명|컨텐츠 guid>/...
	//즉, 상위 2단계까지가 서버의 경로를 나타내며 그 이후의 경로는
	//플래쉬 서브폴더 등과 같은 컨텐츠에 종속된 하위 폴더경로이다.
	//로컬에서는 상위2단계 밑의 하위 경로만 취급한다.
	CString strLocation = lpszLocation;

	// skpark  location 이 "\contents\enc\" 인 경우, 이부분이 중복으로 들어간다.
	if(strLocation.CompareNoCase("\\contents\\ENC\\")==0){
		strLocation = "";
	}
	if(strLocation.GetLength() != 0)
	{
		int nIdx = strLocation.Find("/", 1);
		if(nIdx != -1)
		{
			nIdx = strLocation.Find("/", nIdx+1);
			if(nIdx != -1)
			{
				strLocation.Delete(0, nIdx+1);
			}//if
		}//if
	}//if

	LocalFullPath = _LocalPath;
	if(strLocation.GetLength() != 0)
	{
		LocalFullPath += strLocation;
		if(LocalFullPath[LocalFullPath.GetLength()-1] != '\\')
		{
			LocalFullPath += "\\";
		}//if
	}//if
	LocalFullPath += lpszFilename;

	LocalTempFullPath = _LocalTempPath;
	//실행 요청된 프로세서에 따라서 temp 폴더의 경로를 나누어준다.
	//BRW1, BRW2, FirmwareView
	CString strAppName = ::AfxGetAppName();
	if(strAppName.GetLength() != 0)
	{
		LocalTempFullPath += strAppName;
		LocalTempFullPath += _T("\\");
	}//if
	if(strLocation.GetLength() != 0)
	{
		LocalTempFullPath += strLocation;
		if(LocalTempFullPath[LocalTempFullPath.GetLength()-1] != '\\')
		{
			LocalTempFullPath += "\\";
		}//if
	}//if
	LocalTempFullPath += lpszFilename;

	//LocalFullPath를 실제 파일이 존재하는지 확인하여
	//존재하는 파일의 경로라면 중간에 ".."등과 같은 상대경로 문자를 제거하고
	//정확한 경로로 만들어 준다.
	CFileStatus status;
	CFile::GetStatus(LocalFullPath, status);
	LocalFullPath = status.m_szFullName;

	CFile::GetStatus(LocalTempFullPath, status);
	LocalTempFullPath = status.m_szFullName;

	return true;
}


COLORREF GetColorFromString(LPCSTR lpszColor)
{
	COLORREF rgb;
	if(*lpszColor == '#')
		sscanf(lpszColor+1, "%x", &rgb);
	else
		sscanf(lpszColor, "%x", &rgb);

	COLORREF red	= rgb & 0x00FF0000;
	COLORREF green	= rgb & 0x0000FF00;
	COLORREF blue	= rgb & 0x000000FF;

	red = red >> 16;
	blue = blue << 16;

	return red | green | blue;
}

CString GetColorFromString(COLORREF rgb)
{
	COLORREF red	= rgb & 0x000000FF;
	COLORREF green	= rgb & 0x0000FF00;
	COLORREF blue	= rgb & 0x00FF0000;

	green = green >> 8;
	blue = blue >> 16;

	CString ret;
	ret.Format("#%02X%02X%02X", red, green, blue);

	return ret;
}

CTime GetHostTime()
{
	static __time64_t	time_host = 0;
	static CTime		time_run;

	ciString str;
	if(time_host == 0 && ciArgParser::getInstance()->getArgValue("+time", str))
	{
		// ex) 2008:01:01:09:00:00
		if(str.length() > 0)
		{
			int year	= atoi(str.c_str());
			int month	= atoi(str.c_str()+5);
			int day		= atoi(str.c_str()+8);
			int hour	= atoi(str.c_str()+11);
			int minute	= atoi(str.c_str()+14);
			int second	= atoi(str.c_str()+17);

			CTime time_h(year, month, day, hour, minute, second);

			time_host = time_h.GetTime();
			time_run = CTime::GetCurrentTime();
		}//if
	}//if

	if(time_host != 0)
	{
		// 시간 보정
		CTime current_time = CTime::GetCurrentTime();

		__time64_t gap = current_time.GetTime() - time_run.GetTime();
		__time64_t tm = time_host + gap;

//		CTime new_tm(tm);
//		TRACE("Time:%s\r\n", new_tm.Format("%Y/%m/%d %H:%M:%S"));
//		return new_tm;
		return CTime(tm);
	}
	else
	{
		// 현재시간
		return CTime::GetCurrentTime();
	}//if
}

CString ToString(int nValue)
{
	CString str;
	str.Format("%d", nValue);

	return str;
}

CString ToString(double fValue)
{
	CString str;
	str.Format("%.3f", fValue);

	return str;
}

CString ToString(ULONG ulvalue)
{
	CString str;
	str.Format("%ld", ulvalue);
	return str;
}

CString ToString(ULONGLONG ulvalue)
{
	CString str;
	str.Format("%I64u", ulvalue);
	return str;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일명에서 확장자를 분리하여 반환 \n
/// @param (const CString&) strFileName : (in) 파일명
/// @return <형: CString> \n
///			<파일의 확장자> \n
/////////////////////////////////////////////////////////////////////////////////
CString	FindExtension(const CString& strFileName)
{
	int nlen = strFileName.GetLength();
	int i;
	for (i = nlen-1; i >= 0; i--)
	{
		if (strFileName[i] == '.')
		{
			return strFileName.Mid(i+1);
		}//if
	}//if

	return CString(_T(""));
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 제품의 edition 문자열 반환 \n
/// @return <형: CString> \n
///			<"STD" : Standard edition> \n
///			<"PLS" : Plus edition> \n
///			<"ENT" : Enterprise edition> \n
/////////////////////////////////////////////////////////////////////////////////
CString	GetEdition()
{
	static CString strEdition = "";

	if(strEdition.GetLength() == 0)
	{
		scratchUtil* aUtil = scratchUtil::getInstance();
		ciString strHost, strMac, strEdi;	
		if(!aUtil->readAuthFile(strHost, strMac, strEdi))
		{
			strEdition = "";
		}
		else
		{
			strEdition = strEdi.c_str();
		}//if
	}//if

	return strEdition;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 실행프로그램의 파일명(extention 제외)을 반환 \n
/// @return <형: LPCSTR> \n
///			<실행 프로그램의 파일명> \n
/////////////////////////////////////////////////////////////////////////////////
LPCSTR GetAppName()
{
	static CString strAppName;
	
	if(strAppName.GetLength() == 0)
	{
		char cModule[MAX_PATH];
		::ZeroMemory(cModule, MAX_PATH);
		::GetModuleFileName(NULL, cModule, MAX_PATH);

		char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
		_splitpath(cModule, cDrive, cPath, cFilename, cExt);

		strAppName = cFilename;
	}//if

	return strAppName;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// BRW의 아이디(0, 1) \n
/// @return <형: LPCSTR> \n
///			<실행 프로그램의 파일명> \n
/////////////////////////////////////////////////////////////////////////////////
int GetBRWId()
{
	CString str = GetAppName();
	if(str == "UTV_brwClient2")
	{
		return 0;
	}//if
	
	return 1;	
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VARIANT를 문자열로 변환한다. \n
/// @param (VARIANT*) var : (in) VARIANT 변수
/// @return <형: CString> \n
///			<VARIANT 변수의 문자열> \n
/////////////////////////////////////////////////////////////////////////////////
CString	VariantToString(VARIANT* var)
{
	CString str;
	switch(var->vt)
	{
	case VT_BSTR:
		{
			return CString(var->bstrVal);
		}
		break;
	case VT_BSTR | VT_BYREF:
		{
			return CString(*var->pbstrVal);
		}
		break;
	case VT_I4:
		{
			str.Format(_T("%d"), var->lVal);
			return str;
		}
		break;
	case VT_I4 | VT_BYREF:
		{
			str.Format(_T("%d"), *var->plVal);
			return str;
		}
		break;
	case VT_R8:
		{
			str.Format(_T("%f"), var->dblVal);
			return str;
		}
		break;
	default:
		{
			return "";
		}
	}//switch
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠가 방송되는 log를 report용으로 기록한다. \n
/// @param (CString) strContentsId : (in) 컨텐츠 Id
/// @param (CString) strContentsName : (in) 컨텐츠 이름
/// @param (CString) strAction : (in) 컨텐츠 액션문자열(TRY/START/(SUCCEED|FAILE)/END)
/// @param (long) lRunningTime : (in) 컨텐츠 재생시간
/// @param (CString) strFileName : (in) 컨텐츠 파일 이름
/// @param (CString) strPrgId : (in) 프로그램 Id(+host 인자)
/// @param (CString) strDesc : (in) log에 기록할 부가 내용(URL컨텐츠의 대체 이미지 사용 여부 등...)
/////////////////////////////////////////////////////////////////////////////////
void WritePlayLog(CString strContentsId, CString strContentsName, CString strAction, long lRunningTime,
					CString strFileName, CString strPrgId, CString strDesc, int nTemplateIndex)
{
	if(GetEdition() != ENTERPRISE_EDITION)
	{
		return;
	}//if

	//Prorgam name
	if(strPrgId == "")
	{
		ciString strTmp;
		ciArgParser::getInstance()->getArgValue("+host", strTmp);
		if(nTemplateIndex > 0)
			strPrgId.Format("(%d)%s", nTemplateIndex, strTmp.c_str());
		else
			strPrgId = strTmp.c_str();
	}//if

	if(strFileName == "")
	{
		strFileName = "no file";
	}//if

	PLAYLOG(("%s,%s,%s,%s,%s,%d,%s", strPrgId, strContentsId, strContentsName, strFileName, strAction, lRunningTime, strDesc));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 플래쉬의 상호작용 log를 report용으로 기록한다. \n
/// @param (CString) strContentsId : (in) 컨텐츠 Id
/// @param (CString) strKeyword : (in) "|"로 구분되는 플래쉬 컨텐츠 구분 Keyword
/// @param (CString) strPrgId : (in) 프로그램 Id(+host 인자)
/////////////////////////////////////////////////////////////////////////////////
void WriteInterActLog(CString strContentsId, CString strKeyword, CString strPrgId)
{
	//if(GetEdition() != ENTERPRISE_EDITION)
	//{
	//	return;
	//}//if

	//Prorgam name
	if(strPrgId == "")
	{
		ciString strTmp;
		ciArgParser::getInstance()->getArgValue("+host", strTmp);
		strPrgId = strTmp.c_str();
	}//if

	INTERACT_LOG(("%s,%s,%s", strPrgId, strContentsId, strKeyword));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 긴급공지가 방송되는 log를 report용으로 기록한다. \n
/// @param (CString) strContentsId : (in) 컨텐츠 Id
/// @param (CString) strContentsName : (in) 컨텐츠 이름
/// @param (CString) strAction : (in) 컨텐츠 액션문자열(TRY/START/(SUCCEED|FAILE)/END)
/// @param (long) lRunningTime : (in) 컨텐츠 재생시간
/// @param (CString) strFileName : (in) 컨텐츠 파일 이름
/// @param (CString) strPrgId : (in) 프로그램 Id(+host 인자)
/// @param (CString) strDesc : (in) log에 기록할 부가 내용(URL컨텐츠의 대체 이미지 사용 여부 등...)
/////////////////////////////////////////////////////////////////////////////////
void WriteAnnounceLog(CString strContentsId, CString strContentsName, CString strAction, long lRunningTime,
					CString strFileName, CString strPrgId, CString strDesc)
{
	if(GetEdition() != ENTERPRISE_EDITION)
	{
		return;
	}//if

	//Prorgam name
	strPrgId = ID_ANNOUNCE_LOG;

	if(strFileName == "")
	{
		strFileName = "no file";
	}//if

	PLAYLOG(("%s,%s,%s,%s,%s,%d,%s", strPrgId, strContentsId, strContentsName, strFileName, strAction, lRunningTime, strDesc));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Customer Info를 반환한다. \n
/// @return <형: LPCSTR> \n
///			<CString: CustomerInfo name> \n
/////////////////////////////////////////////////////////////////////////////////
LPCSTR GetCoutomerInfo()
{
	static CString strCustomInfo;
	if(strCustomInfo.GetLength() == 0)
	{
		strCustomInfo = GetINIValue("UBCVariables.ini", "CUSTOMER_INFO", "NAME");
	}//if
	return strCustomInfo;
}


bool CALLBACK FPC_EnumFontProc(LPLOGFONT lplf, LPTEXTMETRIC lptm, DWORD dwType, LPARAM lpData)	
{	
	if(strstr(lplf->lfFaceName, "@") != NULL)
		return TRUE;

	CStringArray* paryFontName = (CStringArray*)lpData;
	paryFontName->Add(lplf->lfFaceName);

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시스템에 지정된 폰트가 있는지를 반환 \n
/// @param (CString) strFontName : (in) 검사하려는 폰트 이름
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsAvailableFont(CString strFontName)
{
	HWND hWnd = GetDesktopWindow();
	CStringArray aryFontName;
	EnumFonts(::GetDC(hWnd), 0,(FONTENUMPROC) FPC_EnumFontProc,(LPARAM)&aryFontName); //Enumerate font

	for(int i=0; i<aryFontName.GetSize(); i++)
	{
		if(strFontName == aryFontName.GetAt(i))
		{
			return true;
		}//if
	}//for

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용할 Vidoe render 종류를 반환 \n
/// @return <형: int> \n
///			<값: 1: Overlay, 2:VMR7,....> \n
/////////////////////////////////////////////////////////////////////////////////
int GetVidoeRenderType()
{
	static int nType = -1;

	if( nType == -1 )
	{
		nType = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "VIDEO_RENDER") );

		if(nType == 0)
		{
			nType = E_WINDOWLESS;
			//WriteINIValue("UBCBrowser.ini", "ROOT", "VIDEO_RENDER", "2");
		}//if
	}

	return nType;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용할 Vidoe open하는 방법을 반환 \n
/// @return <형: int> \n
///			<값: 1: 초기화과정에 open, 2:매번 플레이시에 open> \n
/////////////////////////////////////////////////////////////////////////////////
int GetVidoeOpenType()
{
	static int nType = -1;

	if( nType == -1 )
	{
		nType = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "VIDEO_OPEN_TYPE") );

		if(nType == 0)
		{
			nType = E_OPEN_PLAY;
			//WriteINIValue("UBCBrowser.ini", "ROOT", "VIDEO_OPEN_TYPE", "2");
		}//if
	}

	return nType;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠를 한번만 Open. 동일컨텐츠일 경우 재사용. Video의 경우 GetVidoeOpenType이 1일경우에만 동작함.
/// @return <형: int> \n
///			<값: 1:재사용, 0:재사용안함> \n
/////////////////////////////////////////////////////////////////////////////////
int GetVideoContentsSharing()
{
	static int value = -1;

	if( value == -1 )
	{
		value = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "VIDEO_CONTENTS_SHARING") );

		if(value != 0) value=1;
	}

	return value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Open했던 플래시를 닫지않음. 다른 플래시 컨텐츠가 재생될경우에만 새로Open.
/// @return <형: int> \n
///			<값: 1:닫지않음, 0:닫음(기본)> \n
/////////////////////////////////////////////////////////////////////////////////
int GetFlashContentsSharing()
{
	static int value = -1;

	if( value == -1 )
	{
		value = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "FLASH_CONTENTS_SHARING") );

		if(value != 0) value=1;
	}

	return value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Video재생시 터치할 경우 컨트롤 윈도우 표시여부.
/// @return <형: int> \n
///			<값: 1:표시, 0:표시안함(기본)> \n
/////////////////////////////////////////////////////////////////////////////////
int GetShowVideoController()
{
	static int value = -1;

	if( value == -1 )
	{
		value = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "SHOW_VIDEO_CONTROLLER") );

		if(value != 0) value=1;
	}

	return value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 정시컨텐츠를 한번만 재생. Replay하지 않음
/// @return <형: int> \n
///			<값: 1:한번만재생, 0:계속다시재생> \n
/////////////////////////////////////////////////////////////////////////////////
int GetPlayOneTimesTimebaseSchedule(void)
{
	static int play_onetimes = -1;

	if( play_onetimes == -1 )
	{
		play_onetimes = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "TIME_BASE_VIDEO_PLAY_ONE_TIMES") );

		if(play_onetimes != 0) play_onetimes=0;
	}

	return play_onetimes;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 세컨더리 프레임에서 소리 재생
/// @return <형: bool> \n
///			<값: true:소리재생, false:소리없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsSoundOnSecondaryFrame(void)
{
	static int sound_on_secondary = -1;

	if( sound_on_secondary == -1 )
	{
		sound_on_secondary = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "SOUND_ON_SECONDARY_FRAME") );

		if(sound_on_secondary != 0) sound_on_secondary=1;
	}

	if( sound_on_secondary > 0 ) return true;
	return false;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 웹 컨텐츠에서 스크롤바를 표시
/// @return <형: int> \n
///			<값: 1:표시, 0:표시안함(기본)> \n
/////////////////////////////////////////////////////////////////////////////////
int GetShowWebScrollbar()
{
	static int value = -1;

	if( value == -1 )
	{
		value = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "SHOW_WEB_SCROLLBAR") );

		value = (value>0) ? 1 : 0;
	}

	return value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 템플릿 전환되어도 스케줄을 이어서 재생
/// @return <형: int> \n
///			<값: 1:사용, 0:사용안함(기본)> \n
/////////////////////////////////////////////////////////////////////////////////
int GetScheduleResuming(void)
{
	static int value = -1;

	if( value == -1 )
	{
		value = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "SCHEDULE_RESUMING") );

		value = (value>0) ? 1 : 0;
	}

	return value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동기화프로세스 미리 실행
/// @return <형: int> \n
///			<값: 1:사용안함, 0:사용(기본)> \n
/////////////////////////////////////////////////////////////////////////////////
int GetPreOpenSyncProcess(void)
{
	static int value = -1;

	if( value == -1 )
	{
		value = atoi( GetINIValue("UBCBrowser.ini", "SyncProcess", "PRE_OPEN") );

		value = (value>0) ? 1 : 0;
	}

	return value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동기화프로세스 열기 타입 문자열 반환. \n
/// @return <형: LPCSTR> \n
///			<CString: SyncProcess Open Type> \n
/////////////////////////////////////////////////////////////////////////////////
CString GetSyncProcessOpenType(void)
{
	static CString str_value = "NORMAL";
	static int value = -1;

	if( value == -1 )
	{
		value = 1;
		CString value = GetINIValue("UBCBrowser.ini", "SyncProcess", "PROCESS_OPEN_TYPE");
		if(value!="") str_value = value;
	}

	return str_value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동기화프로세스 닫기 타입 문자열 반환. \n
/// @return <형: LPCSTR> \n
///			<CString: SyncProcess Close Type> \n
/////////////////////////////////////////////////////////////////////////////////
CString GetSyncProcessCloseType(void)
{
	static CString str_value = "MINIMIZE";
	static int value = -1;

	if( value == -1 )
	{
		value = 1;
		CString value = GetINIValue("UBCBrowser.ini", "SyncProcess", "PROCESS_CLOSE_TYPE");
		if(value!="") str_value=value;
	}

	return str_value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동기화프로세스 종료 타입 문자열 반환. \n
/// @return <형: LPCSTR> \n
///			<CString: SyncProcess Close Type> \n
/////////////////////////////////////////////////////////////////////////////////
CString GetSyncProcessExitType(void)
{
	static CString str_value = "NOTHING";
	static int value = -1;

	if( value == -1 )
	{
		value = 1;
		CString value = GetINIValue("UBCBrowser.ini", "SyncProcess", "PROCESS_EXIT_TYPE");
		if(value!="") str_value = value;
	}

	return str_value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 현재 재생중인 템플릿 단축기를 14007포트로 전송. \n
/// @return <형: int> \n
///			<값: 1:사용, 0:사용안함(기본)> \n
/////////////////////////////////////////////////////////////////////////////////
int GetSendSyncProcessShortcut(void)
{
	static int value = -1;

	if( value == -1 )
	{
		value = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "SEND_TEMPLATE_SHORTCUT") );

		value = (value>0) ? 1 : 0;
	}

	return value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UserLog에 템플릿인덱스포함 \n
/// @return <형: int> \n
///			<값: 1:사용, 0:사용안함(기본)> \n
/////////////////////////////////////////////////////////////////////////////////
int GetTemplateIndexInUserlog(void)
{
	static int value = -1;

	if( value == -1 )
	{
		value = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "REPORT_TEMPLATE_INDEX_IN_USERLOG") );

		value = (value>0) ? 1 : 0;
	}

	return value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 라이센스(하단 바) 체크 딜레이 \n
/// @return <형: int> \n
///			<값: n:사용(초단위), 0:사용안함(기본)> \n
/////////////////////////////////////////////////////////////////////////////////
int GetLicenseCheckDelay(void)
{
	static int value = -999;

	if( value == -999 )
	{
		value = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "LICENSE_CHECK_DELAY") );

		value = (value<0) ? value : (value*1000); // sec -> msec
	}

	return value;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 ini 파일의 값을 반환한다. \n
/// @param (CString) strFileName : (in) ini파일 이름
/// @param (CString) strSection : (in) ini파일의 section 
/// @param (CString) strKey : (in) 구하려는 값의 key값
/// @return <형: CString> \n
///			<key값의 value> \n
/////////////////////////////////////////////////////////////////////////////////
CString GetINIValue(CString strFileName, CString strSection, CString strKey)
{
	char cBuffer[BUF_SIZE] = { 0x00 };
	CString strPath = GetAppPath();
	strPath += "data\\";
	strPath += strFileName;
	GetPrivateProfileString(strSection, strKey, "", cBuffer, BUF_SIZE, strPath);

	return CString(cBuffer);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 ini 파일의 key에 값을 쓴다. \n
/// @param (CString) strFileName : (in) ini파일 이름
/// @param (CString) strSection : (in) ini파일의 section
/// @param (CString) strKey : (in) 기록하려는 값의 key값
/// @param (CString) strValue : (in) 기록하려는 값
/////////////////////////////////////////////////////////////////////////////////
void WriteINIValue(CString strFileName, CString strSection, CString strKey, CString strValue)
{
	CString strPath = GetAppPath();
	strPath += "data\\";
	strPath += strFileName;
	WritePrivateProfileString(strSection, strKey, strValue, strPath);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 정시스케줄을 사용할지 여부를 반환 \n
/// @return <형: bool> \n
///			<true: 사용함> \n
///			<false: 사용하지 않음> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsUseTimeBaseSchedule()
{
	if(GetEdition() != ENTERPRISE_EDITION)
	{
		return true;
	}//if

	static int use_time_base_schedule = -1;

	if( use_time_base_schedule == -1 )
	{
		use_time_base_schedule = atoi(GetINIValue("UBCVariables.ini", "ROOT", "USE_TIME_BASE_SCHEDULE"));

		if( use_time_base_schedule != 0 ) use_time_base_schedule = 1;
	}

	return (bool)use_time_base_schedule;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 폰트파일인지 구분하여 준다. \n
/// @param (ciString) strFileName : (in) 파일 이름
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsFontFile(CString strFileName)
{
	CString strExt = "";
	int nlen = strFileName.GetLength();
	int i;
	for(i = nlen-1; i >= 0; i--)
	{
		if(strFileName[i] == '.')
		{
			strExt = strFileName.Mid(i+1);
			break;
		}//if
	}//for

	if(strExt.CompareNoCase("fon") == 0 || strExt.CompareNoCase("ttc") == 0 || strExt.CompareNoCase("ttf") == 0)
	{
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ppt파일인지 구분하여 준다. \n
/// @param (ciString) strFileName : (in) 파일 이름
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsPptFile(CString strFileName)
{
	CString strExt = "";
	int nlen = strFileName.GetLength();
	int i;
	for(i = nlen-1; i >= 0; i--)
	{
		if(strFileName[i] == '.')
		{
			strExt = strFileName.Mid(i+1);
			break;
		}//if
	}//for

	if(strExt.CompareNoCase("ppt") == 0 || strExt.CompareNoCase(_T("pptx")) == 0 || strExt.CompareNoCase(_T("pps")) == 0 || strExt.CompareNoCase(_T("ppsx")) == 0)
	{
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// tar파일인지 구분하여 준다. \n
/// @param (ciString) strFileName : (in) 파일 이름
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsTarFile(CString strFileName)
{
	CString strExt = "";
	int nlen = strFileName.GetLength();
	int i;
	for(i = nlen-1; i >= 0; i--)
	{
		if(strFileName[i] == '.')
		{
			strExt = strFileName.Mid(i+1);
			break;
		}//if
	}//for

	if(strExt.CompareNoCase("tar") == 0)
	{
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 폰트를 시스템이 등록한다. \n
/// @param (CString) strFontName : (in) 등록하려는 폰트이름
/// @param (CString) strFontPath : (in) 등록하려는 폰트파일의 경로
/// @return <boo: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool RegisterFont(CString strFontName, CString strFontPath)
{
	char szWinDir[MAX_PATH] = { 0x00 };
	if(GetWindowsDirectory(szWinDir, MAX_PATH) == 0)
	{
		__DEBUG__("Fail to get win dir", _NULL);
		return false;
	}//if
	
	CString strFontDir;
	strFontDir.Format("%s\\fonts\\%s", szWinDir, strFontName);
	
	//폰트파일이 없다면 이동...
	if(_access(strFontDir, 0) != 0)
	{
		if(!MoveFileEx(strFontPath, strFontDir, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
		{
			__DEBUG__("Fail to move font file", strFontPath);
			return false;
		}//if
	}//if

	//레지스트리를 검사하여 폰트가 등록이 안되어 있다면 등록해 준다.
	HKEY hKey = NULL;
	CString strRooyKey = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Fonts\\";
	CString strOpenKey = strRooyKey;
	LONG lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE , strOpenKey, 0, KEY_ALL_ACCESS, &hKey);
	if(lResult != ERROR_SUCCESS)
	{
		__DEBUG__("Fail to get font registry", _NULL);
		return false;
	}//if

	TCHAR    achKey[MAX_KEY_LENGTH];			// buffer for subkey name
    DWORD    cbName;							// size of name string 
    TCHAR    achClass[MAX_PATH] = TEXT("");		// buffer for class name 
    DWORD    cchClassName = MAX_PATH;			// size of class string 
    DWORD    cSubKeys=0;						// number of subkeys 
    DWORD    cbMaxSubKey;						// longest subkey size 
    DWORD    cchMaxClass;						// longest class string 
    DWORD    cValues;							// number of values for key 
    DWORD    cchMaxValue;						// longest value name 
    DWORD    cbMaxValueData;					// longest value data 
    DWORD    cbSecurityDescriptor;				// size of security descriptor 
    FILETIME ftLastWriteTime;					// last write time 
 
    DWORD i; 
 
    TCHAR  achValue[MAX_VALUE_NAME]; 
    DWORD cchValue = MAX_VALUE_NAME; 
 
    // Get the class name and the value count. 
    lResult = RegQueryInfoKey(
        hKey,                    // key handle 
        achClass,                // buffer for class name 
        &cchClassName,           // size of class string 
        NULL,                    // reserved 
        &cSubKeys,               // number of subkeys 
        &cbMaxSubKey,            // longest subkey size 
        &cchMaxClass,            // longest class string 
        &cValues,                // number of values for this key 
        &cchMaxValue,            // longest value name 
        &cbMaxValueData,         // longest value data 
        &cbSecurityDescriptor,   // security descriptor 
        &ftLastWriteTime);       // last write time

	if(lResult != ERROR_SUCCESS)
	{
		__DEBUG__("Failt to get font key information", _NULL);
		return false;
	}//if
 
	CString strLog;
	bool bRegister = false;
	TCHAR buffer[256];
	DWORD dwSize;
    // Enumerate the key values. 
    if(cValues) 
    {
		//strLog.Format("\nNumber of values: %d\n", cValues);
        //TRACE(strLog);

        for(i=0, lResult=ERROR_SUCCESS; i<cValues; i++) 
        { 
            cchValue = MAX_VALUE_NAME; 
            achValue[0] = '\0'; 
            lResult = RegEnumValue(hKey, i, achValue, &cchValue, NULL, NULL,NULL,NULL);
            if(lResult == ERROR_SUCCESS) 
            { 
				//strLog.Format("(%d) %s\n", i+1, achValue);
                //TRACE(strLog);

				//Value의 값을 읽어서 추가하려는 폰트 파일이 등록되어 있는지 확인...
				memset(buffer, 0x00, 256);
				dwSize = 256;
				lResult = RegQueryValueEx(hKey, achValue, 0, NULL, (LPBYTE)buffer, &dwSize);
				if(lResult == ERROR_SUCCESS)
				{
					if(strFontName.CompareNoCase(CString(buffer)) == 0)
					{
						bRegister = true;
						break;
					}//if
				}//if
            }//if 
        }//for
    }//if

	if(bRegister)
	{
		return true;
	}//if

	//Key가 등록되어 있지 않으므로 등록해 준다.
	lResult = RegSetValueEx(hKey, strFontName, 0, REG_SZ, (LPBYTE)strFontName.GetBuffer(), strFontName.GetLength()+1);
	if(lResult != ERROR_SUCCESS)
	{
		__DEBUG__("Fail to create font value", strFontName);
		return false;
	}//if
	strFontName.ReleaseBuffer();
	RegCloseKey(hKey);

	//리소스로 폰트 추가
	if(AddFontResource(strFontDir) == 0)
	{
		__DEBUG__("Fail to add font resource", strFontDir);
		return false;
	}//if
	
	//폰트가 새로 등록되었음을 알림....
	::SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
	
	return true;
} 


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 경로의 폴더를 만든다 \n
/// @param (LPCTSTR) lpszPath : (in) 생성하려는 폴더 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CreatePathDirectory(LPCTSTR lpszPath)
{
    TCHAR szPathBuffer[MAX_PATH];

    size_t len = _tcslen(lpszPath);

    for(size_t i = 0; i < len; i++ )
    {
        szPathBuffer[i] = *(lpszPath + i);
        if(szPathBuffer[i] == _T('\\') || szPathBuffer[i] == _T('/'))
        {
            szPathBuffer[i + 1] = NULL;
            if(!PathFileExists(szPathBuffer))
            {
                if(!::CreateDirectory(szPathBuffer, NULL))
                {
                    if(GetLastError() != ERROR_ALREADY_EXISTS)
					{
                        return false;
					}//if
                }//if
            }//if
        }//if
    }//for

    return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 폴더의 안의 파일들 사이즈를 구한다. \n
/// @param (CString) strInPath : (in) 폴더의 경로
/// @return <형: ULONGLONG> \n
///			폴더안의 파일들 사이즈의 총합 \n
/////////////////////////////////////////////////////////////////////////////////
ULONGLONG GetFolderSize(CString strInPath)
{
	if(strInPath.IsEmpty()) return 0;

	ULONGLONG ulTotalSize = 0;

	CFileFind ff;
	BOOL bFindIt = ff.FindFile(strInPath + "\\*.*");
	while(bFindIt)
	{
		bFindIt = ff.FindNextFile();

		if(ff.IsDots()) continue;
		if(ff.IsDirectory())
		{
			ulTotalSize += GetFolderSize(ff.GetFilePath());
		}
		else
		{
			ulTotalSize += ff.GetLength();
		}//if
	}//while

	ff.Close();

	return ulTotalSize;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// OverlayMixer필터를 사용할지 여부를 반환 \n
/// @return <형: int> \n
///			<값: 1:OverlayMixer를 사용하지 않음, 2:OverlayMixer를 사용함> \n
/////////////////////////////////////////////////////////////////////////////////
int GetUseOverlayMixer()
{
	int nUse;
	CString strType = GetINIValue("UBCBrowser.ini", "ROOT", "USE_OVERLAY_MIXER");
	nUse = atoi(strType);

	if(nUse == 0)
	{
		nUse = E_NOT_USE_OVERLAY_MIXER;
		WriteINIValue("UBCBrowser.ini", "ROOT", "USE_OVERLAY_MIXER", "1");
	}//if

	return nUse;
}
*/