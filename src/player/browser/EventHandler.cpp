// PEventHandler.cpp : implementation file
//

#include "stdafx.h"
#include "brw2.h"
#include "EventHandler.h"
#include "Host.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PEventHandler

CEventHandler::CEventHandler(CWnd* pWnd)
:	m_pWnd(pWnd)
{
}

CEventHandler::~CEventHandler()
{
}

/////////////////////////////////////////////////////////////////////////////
// PEventHandler message handlers

void CEventHandler::processEvent(cciEvent &event)
{
	__DEBUG_BEGIN__(_NULL)

	cciEvent* _event;
	_event = new cciEvent(event);

	if(_event == NULL)
		return;

	if(!_event->isAssigned())
		return;

	__DEBUG__("Receive event !!!", _event->getEventType());
	//m_pWnd->PostMessage(WM_PROCESS_EVENT, (WPARAM)_event);

	__DEBUG_END__(_NULL)
}
