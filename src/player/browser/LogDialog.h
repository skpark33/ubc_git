#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "resource.h"

#include "PropertyGrid.h"

// CLogDialog 대화 상자입니다.

class CDebugObject
{
public:
	CDebugObject() {};
	virtual ~CDebugObject() {};

	virtual CString ToString() = 0;
};

class CLogDialog : public CDialog
{
	DECLARE_DYNAMIC(CLogDialog)

public:

	static CLogDialog*	_instance;
	static CLogDialog*	getInstance(CWnd* pWnd=NULL);
	static void			clearInstance();

	CLogDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLogDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LOG_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CImageList	m_imageList;
	CFont		m_font;
	HTREEITEM	m_hCurrentItem;

	CCriticalSection m_csLogDlg;
	void	ResizeControl();
	void	SetAttribute(LPCTSTR lpszAttr);

	int		GetDepth(HTREEITEM hItem);

public:

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTvnSelchangedTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRclickTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnDebugString(WPARAM wParam, LPARAM lParam);

	CTreeCtrl	m_treectrl;
	CEdit		m_editLOG;
	CEdit		m_editAttribute;

	void	AppendLog(LPCTSTR lpszText);

	void	Add(CWnd* pParent, CWnd* pThis, LPCSTR lpszID, int nImage);
	void	_Add(HTREEITEM hItem, CWnd* pParent, CWnd* pThis, LPCSTR lpszID, int nImage);
	void	Remove(CWnd* pWnd);
	void	_Remove(HTREEITEM hItem, CWnd* pThis);
	void	Play(CWnd* pThis);
	void	_Play(HTREEITEM hItem, CWnd* pThis);
	void	Stop(CWnd* pThis);
	void	_Stop(HTREEITEM hItem, CWnd* pThis);
	afx_msg void OnDefaulttemplateChange();
	afx_msg void OnPlayNextSchedule();


	//CStatic m_propertyCtrl;
	CPropertyGrid	m_propertyCtrl;
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};


