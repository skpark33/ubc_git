#pragma once

//#include "web_browser.h"
//#include "ShockwaveFlash.h"
#include "ximage.h"
#include "PictureEx.h"
#include "PPTEventSink.h"
#include "OverlayInterface.h"
#include "VMRRender.h"
#include "EVRRender.h"
#include "MusicInterface.h"
#include "TVPlayer.h"
//#include "FlashEventSink.h"
#include "Flash/FlashDlg.h"
#include "WebBrowser/ExplorerDlg.h"
#include "VideoControllerWnd.h"


#define		SCHEDULE_STOP_ID			3999	// id


//
// LOG 기록 후 casting 여부 등의 내용을 UTV_MGR 에게 보내기 위한 notification.
//void 	castingStateReport (...); -> 차후 구현
//

enum  E_TEXT_DIRECTION {
	TEXT_DIRECTION_HORIZONTAL = 0,		//가로
	TEXT_DIRECTION_VERTICAL				//세로
};

enum  E_TEXT_ALIGN {
	TEXT_ALIGN_LT = 1,					//left-top
	TEXT_ALIGN_CT,						//center-top
	TEXT_ALIGN_RT,						//right-top
	TEXT_ALIGN_LM,						//left-middle
	TEXT_ALIGN_CM,						//center-middle
	TEXT_ALIGN_RM,						//right-middle
	TEXT_ALIGN_LB,						//left-bottom
	TEXT_ALIGN_CB,						//center-bottom
	TEXT_ALIGN_RB						//right-bottom
};


class CTemplate;
class CFrame;
class CSchedule;

typedef	CArray<CSchedule*, CSchedule*>	CScheduleArray;
typedef	CMapStringToPtr					CScheduleMap;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSchedule
class CSchedule : public CWnd
{
	DECLARE_DYNAMIC(CSchedule)

public:
	static	CSchedule*	GetScheduleObject(CTemplate* pParentParentWnd, CFrame* pParentWn, LPCSTR lpszID, CRect& rect, bool bDefaultSchedule, CString& strErrorMessage);
	static	void		KillPowerpoint();

	CSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CSchedule();

protected:

	DECLARE_MESSAGE_MAP()

	////////////////////////////////////////////////////////////////////////////
	// IDL defined schedule attributes
	ciString		m_siteId;					///<방송될 사이트
	ciString		m_hostId;					///<client host id		
	ciString		m_scheduleId;				///<schedule-id
	ciString		m_templateId;				///<템플릿 id
	ciString		m_frameId;					///<프레임 id
	//ciString		m_requestid;				///<광고신청 id
	ciString		m_contentsId;				///<contents id
	ciTime			m_startDate;				///<예정시작날짜
	ciTime			m_endDate;					///<예정끝날짜
	ciBoolean		m_openningFlag;				///<무조건 처음부터 시작할것인가, 상황에 따라 중간부터 시작할것인가
	ciShort			m_priority;					///<우선순위(0 : 최고,,,,99: 최하)	==> 나중에 부활 가능
	ciLong			m_castingState;				///<CastingStateEnum
	//ciTime		m_castingStateTime;			///<state 가 변경된 시각
	//ciString		m_castingStateHistory;		///<state 가 변경이력
	//ciString		m_result;					///<실패원인등...
	ciBoolean		m_operationalState;			///<고장여부
	ciBoolean		m_adminState;				///<동작여부
	//ciString		m_phoneNumber;				///<전화연결정보 ipaddress/port
	//ciString		m_comment1;					///<comment1
	//ciString		m_comment2;					///<comment2
	//ciString		m_comment3;					///<comment3

	////////////////////////////////////////////////////////////////////////////
	// default Schedule
	//ciString		m_tuchTime;
	ciString		m_parentScheduleId;			///<parentScheduleId
	ciShort			m_playOrder;				///<방송순서
	ciLong			m_timeScope;				///<TimeScopeEnum
	ciString		m_touchTime;				///<Click-Jump 정보값

	////////////////////////////////////////////////////////////////////////////
	// Schedule
	ciString		m_startTime;				///<예정시작시간
	ciString		m_endTime;					///<예정끝시간
	//ciTime		m_fromTime;					///<실제시작시간	
	//ciTime		m_toTime;					///<실제끝시간
	//ciBoolean		m_isOrign;					///<송수신여부 1:송신, 0:수신

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	ciString		m_filename;					///<contents file name
	ciString		m_location;					///<contents location
	ciLong			m_contentsType;				///<
	ciLong			m_contentsState;			///<
	ULONGLONG		m_volume; // filesize		///<
	ciLong			m_runningTime;				///<
	//ciString		m_bgColor;					///<
	//ciString		m_fgColor;					///<
	//ciString		m_font;						///<
	//ciInt			m_fontSize;					///<
	//ciInt			m_playSpeed;				///<
	//ciInt			m_soundVolume;				///<
	//ciStringList	m_promotionValueList;		///<
	ciString		m_contentsName;				///<
	ciString		m_comment[TICKER_COUNT];
	
	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	static	UINT			m_nScheduleID;		///<윈도우 생성시에 사용하는 윈도우 고유 ID
	static	CMapStringToPtr	m_mapSchedule;
	CTime					m_timeStartDate;
	CTime					m_timeEndDate;
	CString					m_strStartTime;		// only schedule
	CString					m_strEndTime;		// only schedule
	CString					m_strMediaFullPath;
	CString					m_strErrorMessage;
	CString					m_strLoadID;
	CTemplate*				m_pParentParentWnd;
	CFrame*					m_pParentWnd;
	CxImage					m_bgImage;
	int						m_nPlayCount;
	bool					m_bOpen;
	bool					m_bMute;
	bool					m_bPlay;
	bool					m_bLoadFromLocal;
	bool					m_bFileNotExist;			///<컨텐츠 파일이 지정된 경로상에 있는지 여부
	bool					m_bDefaultSchedule;
	DWORD					m_dwStartTick;
	bool					m_bIsHDSchedule;			///<HD Schedule 여부
#ifdef _FELICA_
	bool					m_bFelicaSet;
#endif

	// 컨텐츠 카테고리 검사용
	ciString				m_includeCategory;
	CMapStringToString		m_mapInclude;
	ciString				m_excludeCategory;
	CMapStringToString		m_mapExclude;

public:

	bool	IsDefaultSchedule()					{ return m_bDefaultSchedule; };
	LPCSTR	GetScheduleId()						{ return m_scheduleId.c_str(); };
	int		GetPlayOrder()						{ return m_playOrder; };
	int		GetTimeScope()						{ return m_timeScope; };
	LPCSTR	GetTemplateId()						{ return m_templateId.c_str(); };
	LPCSTR	GetParentScheduleId()				{ return m_parentScheduleId.c_str(); };
	LPCSTR	GetLoadID()							{ return m_strLoadID; };
	void	ResetPlayCount()					{ m_nPlayCount = 0; };
	int		GetPlayCount()						{ return m_nPlayCount; };
	int		GetContentsType()					{ return m_contentsType; };
	void	SetHDSchedule(bool bIsHDSchedule)	{ m_bIsHDSchedule = bIsHDSchedule; };	///<HD Schedule 여부를 설정한다.

	void	GetStartTime(time_t& tmStart);												///<SCheduel의 시작 시간을 구한다.

	bool	GetFrameScheduleInfo(FRAME_SCHEDULE_INFO& info);

	virtual LPCSTR	GetContentsTypeString() = 0;
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime() = 0;
	virtual double	GetCurrentPlayTime() = 0;

	virtual bool	IsUseFile();
	virtual	bool	IsBetween();
	virtual bool	IsOpen() { return m_bOpen; };
	virtual bool	IsPlay() { return m_bPlay; };

	virtual	bool	OpenFile(int nGrade) = 0;
	virtual bool	CloseFile();
	virtual bool	Play(void);
	virtual bool	Pause(void) = 0;
	virtual bool	Stop(void);
	virtual void	RePlay(void);							///<스케줄을 처음부터 다시 재생한다.
	virtual void	SoundVolumeMute(bool bMute)	{ m_bMute = bMute; };
	virtual void	SetNoContentsFile(void);				///<컨텐츠 파일이 없는 상태를 설정한다.

	CString	m_debugString;
	virtual CString	ToString();
	virtual void ToGrid(CPropertyGrid* pGrid);

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnDebugString(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDebugGrid(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnErrorMessage(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHideContents(WPARAM wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	virtual	bool	IsPlayableCategory();

public:
	// 스케줄 전환시 깜박임 방지용
	CSchedule*	m_pPrevPlayedSchedule;
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CVideoSchedule
class CVideoSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CVideoSchedule)

public:
	CVideoSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CVideoSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnGraphNotify(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompletePlay(WPARAM wParam, LPARAM lParam);
	
protected:
	DECLARE_MESSAGE_MAP()

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	ciString		m_bgColor;		// 자막용
	ciString		m_fgColor;		// 자막용
	ciString		m_font;			// 자막용
	ciShort			m_fontSize;		// 자막용
	ciShort			m_playSpeed;	// 자막용
	ciShort			m_soundVolume;

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	COLORREF			m_rgbBgColor;
	COLORREF			m_rgbFgColor;
	CBasicInterface*	m_pInterface;
	bool				m_bUseCaption;
	bool				m_bPause;							///<Pause 여부
	bool				m_bCompletePlay;					///<재생이 완료되었는지 여부
	bool				m_bSetPlayendTimer;					///<Play end 타이머가 설정되었는지 여부

	int					m_nCaptionWidth;
	int					m_offset;
	int					m_nTimerId;
	int					m_nMS;
	int					m_nVedioRenderMode;					///<VideoRender의 종류(1: Overlay, 2:VMR7,....)
	int					m_nVedioOpenType;					///<Video open type(1: 초기화과정에 open, 2:매번 플레이시에 open)
	DWORD				m_dwRedrawTime;						///<Video redraw 시간
	CCriticalSection	m_csLock;							///<필터그래프 이벤트 처리 동기화 

	bool			OpenDSRender(void);						///<Directshow render를 옵션에 맞추어 설정하고 그래프 빌더를 구성한다.
	void			CloseDSRender(void);					///<Directshow render를 종료한다.


public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual LPCSTR	GetContentsTypeString() { return "Video"; };

	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play();
	virtual bool	Pause();
	virtual bool	Stop();
	virtual bool	StopEx();
	virtual void	RePlay(void);							///<스케줄을 처음부터 다시 재생한다.
	virtual	void	SoundVolumeMute(bool bMute);

	virtual CString	ToString();
	virtual void ToGrid(CPropertyGrid* pGrid);

	virtual bool	SetCurrentPlayTime(double pos);

	bool	IsPause() { return m_bPause; };

protected:
	static CMapStringToPtr		m_mapContentsReuse;
	static BOOL					m_bVideoContentsSharing;

	bool	IsMusicFile();

	CVideoControllerWnd		m_wndController;
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPictureSchedule
class CPictureSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CPictureSchedule)

public:
	CPictureSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CPictureSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	
protected:
	DECLARE_MESSAGE_MAP()

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	CPictureEx	m_bgGIF;
	bool		m_bAnimatedGif;

public:
	
	virtual LPCSTR	GetContentsTypeString() { return "Image"; };

	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play();
	virtual bool	Pause();
	virtual bool	Stop();

	virtual CString	ToString();
	virtual void	ToGrid(CPropertyGrid* pGrid);

protected:
	bool			OpenGIF(void);							///<GIF 이미지파일을 open한다.
	bool			OpenImage(void);						///<이미지파일을 open한다.
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSMSSchedule
class CSMSSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CSMSSchedule)

public:
	CSMSSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CSMSSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();

	DECLARE_MESSAGE_MAP()

protected:

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	//ciString		m_comment[TICKER_COUNT];	//상위 CScheduel 멤버로 올림
	ciString		m_bgColor;
	ciString		m_fgColor;
	ciString		m_font;
	ciShort			m_fontSize;

	ciLong			m_direction;	///<문자방향 가로 0, 세로 1
	ciLong			m_align;		///<정렬방향 1~9

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	
	COLORREF		m_rgbBgColor;
	COLORREF		m_rgbFgColor;
	CFont			m_fontSMS;
	CString			m_strSMS;
	int				m_nLineCount;			///<Text의 라인 수
	CString			m_strLogFileString;		///<log 파일에 기록하는 파일이름 필드 문자열

	virtual bool	CreateFile();

public:

	virtual LPCSTR	GetContentsTypeString() { return "Text"; };

	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play();
	virtual bool	Pause();
	virtual bool	Stop();

	virtual CString	ToString();
	virtual void	ToGrid(CPropertyGrid* pGrid);

protected:
	// <!-- 메가박스용 (2017.05.10 seventhstone)
	bool			m_bGridMode;
	int				m_nGridWidth;
	int				m_nGridHeight;
	CStringArray	m_arrSMS;
	CUIntArray		m_arrColor;
	void			DrawGridText(CDC* pDC);
	// -->
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHorizontalTickerSchedule
class CHorizontalTickerSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CHorizontalTickerSchedule)

public:
	CHorizontalTickerSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	
	virtual ~CHorizontalTickerSchedule();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	afx_msg LRESULT OnCaptionShift(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	ciString		m_bgColor;
	ciString		m_fgColor;
	ciString		m_font;
	ciShort			m_fontSize;
	ciShort			m_playSpeed;

	ciLong			m_direction;		///<문자방향 가로 0, 세로 1

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	COLORREF		m_rgbBgColor;
	COLORREF		m_rgbFgColor;

	int				m_offset;
	CFont			m_fontTicker;
	CString			m_strTicker;
	CStringW		m_strTickerW;
	int 			m_nTickerWidth;
	int				m_nWidth;
	int				m_nHeight;

	int 			m_nTimerId;
	int				m_nShiftStep;			///<티커를 움직이는 픽셀

	bool			m_bUseUnicode;
	CStringW		m_commentW[TICKER_COUNT];
	CString			m_strLogFileString;		///<log 파일에 기록하는 파일이름 필드 문자열

	virtual bool	CreateFile();

	static void CALLBACK timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2);
	static UINT	CaptionShiftThread(LPVOID param);

public:
	int					m_nMS;
	bool				m_bExitThread;		///<Thread를 종료하는지 여부 플래그
	HANDLE				m_hevtShift;		///<Thread 종료 이벤트 핸들
	CWinThread*			m_pthreadShift;		///<Ticker를 움직이는 offest 조정 쓰레드
	CCriticalSection	m_csOffset;			///<Offeset값 동기화 객체

	virtual LPCSTR	GetContentsTypeString() { return "Horz Ticker"; };

	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play();
	virtual bool	Pause();
	virtual bool	Stop();
	
	void			DecreaseOffset(void);					///<Offset 값을 줄인다.
	void			GetOffset(int* pnOffset);				///<설정된 offset 값을 반환한다.

	virtual CString	ToString();
	virtual void ToGrid(CPropertyGrid* pGrid);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CVerticalTickerSchedule
class CVerticalTickerSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CVerticalTickerSchedule)

public:
	CVerticalTickerSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CVerticalTickerSchedule();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	afx_msg LRESULT OnCaptionShift(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	ciString		m_bgColor;
	ciString		m_fgColor;
	ciString		m_font;
	ciShort			m_fontSize;
	ciShort			m_playSpeed;

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	COLORREF		m_rgbBgColor;
	COLORREF		m_rgbFgColor;

	int				m_nTickerIndex;
	int				m_nMaxTicker;
	int				m_nDelayCount;
	int				m_offset;
	CFont			m_fontTicker;
	CString			m_strTicker1;
	CString			m_strTicker2;
	int				m_nWidth;
	int				m_nHeight;

	int 			m_nTimerId;
	int				m_nMS;
	CString			m_strLogFileString;		///<log 파일에 기록하는 파일이름 필드 문자열

	virtual bool	CreateFile();

	static void CALLBACK timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2);

public:

	virtual LPCSTR	GetContentsTypeString() { return "Vert Ticker"; };

	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play();
	virtual bool	Pause();
	virtual bool	Stop();

	virtual CString	ToString();
	virtual void ToGrid(CPropertyGrid* pGrid);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CTypingTickerSchedule
class CTypingTickerSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CTypingTickerSchedule)

public:
	CTypingTickerSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	
	virtual ~CTypingTickerSchedule();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnCaptionShift(WPARAM wParam, LPARAM lParam);

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	ciString		m_bgColor;
	ciString		m_fgColor;
	ciString		m_font;
	ciShort			m_fontSize;
	ciShort			m_playSpeed;

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	COLORREF		m_rgbBgColor;
	COLORREF		m_rgbFgColor;

	CFont			m_fontTicker;
	int				m_nCommentCount;
	int				m_nCommentIndex;
	CString			m_wstrTickerShow;
	CString			m_wstrTickerSource;
	int				m_nTickerIndex;
	CRect			m_rectTicker;
	CString			m_strLogFileString;		///<log 파일에 기록하는 파일이름 필드 문자열

	int				m_nMS;

	virtual bool	CreateFile();

public:

	virtual LPCSTR	GetContentsTypeString() { return "Typing"; };

	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play();
	virtual bool	Pause();
	virtual bool	Stop();

	virtual CString	ToString();
	virtual void ToGrid(CPropertyGrid* pGrid);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CURLSchedule
class CURLSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CURLSchedule)

public:
	CURLSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CURLSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();

protected:
	DECLARE_MESSAGE_MAP()

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	CExplorerDlg*	m_pdlgIE;				///<Explorer dialog
	CString			m_strLogFileString;		///<log 파일에 기록하는 파일이름 필드 문자열
	int				m_nErrorCode;		///<error code
	bool			m_bNavigateError;	///<Navigate error flag
	//CxImage			m_bgImage;			///<backgroung image

	virtual CString	GetFilePath();
	virtual bool	CreateFile();

public:

	virtual LPCSTR	GetContentsTypeString() { return "Web"; };

	virtual void	SetSchedule(LPCSTR lpszID);

	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play();
	virtual bool	Pause();
	virtual bool	Stop();

	void			SetNavigateError(bool bError, int nErrorCode);	///<Navigate error 결과값을 설정한다.

	virtual CString	ToString();
	virtual void	ToGrid(CPropertyGrid* pGrid);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CFlashSchedule

class CFlashSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CFlashSchedule)

public:
	CFlashSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CFlashSchedule();

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	//afx_msg LRESULT OnRecvFsCommand(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	bool				m_bPlaying;

	CFlashDlg*			m_pdlgFlash;				///<플래쉬 윈도우

	virtual CString	GetFilePath();
	virtual bool	CreateFile();

public:

	virtual LPCSTR	GetContentsTypeString();
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play();
	virtual bool	Pause();
	virtual bool	Stop();
	virtual void	RePlay(void);							///<스케줄을 처음부터 다시 재생한다.
	void			RecvFsCommand(CString strCmd, CString strArg);	///<FsCommand를 처리한다.

	virtual CString	ToString();
	virtual void	ToGrid(CPropertyGrid* pGrid);

	// skpark 2013.3.21 add
	bool			IsPlaying() { return m_bPlaying;}
	bool			CallFunc(CString arg) ;
	bool			m_couldThisReceiveCallFunc;

protected:
	static CMapStringToPtr		m_mapContentsReuse;
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CRSSSchedule
class CRSSSchedule : public CFlashSchedule
{
	DECLARE_DYNAMIC(CRSSSchedule)

public:
	CRSSSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CRSSSchedule();

//protected:
	DECLARE_MESSAGE_MAP()

	ciShort			m_width;			// contents width (pixel)
	ciShort			m_height;			// contents height (pixel)
	ciString		m_bgColor;			///<
	ciString		m_fgColor;			///<
	ciShort			m_fontSize;			///<

	CString			m_strRssFlashPath;	///<RSS 플래쉬 파일의 경로

	int				m_nRssType;			///<Rss의 종류
	CString			m_strErrorString;	///<장애시 문자열
	CString			m_strCategoryString;///<카테고리 문자열
	CString			m_strLineCount;		///<라인 수

public:

	virtual LPCSTR	GetContentsTypeString() { return "RSS"; };

	virtual void	SetSchedule(LPCSTR lpszID);

	bool			CreateFile();

	virtual bool	Play();

	CString			ToString();
	void			ToGrid(CPropertyGrid* pGrid);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CWzardchedule
class CWizardSchedule : public CFlashSchedule
{
	DECLARE_DYNAMIC(CWizardSchedule)

public:
	CWizardSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CWizardSchedule();

//protected:
	//afx_msg LRESULT OnRecvFsCommand(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

	//IConnectionPoint	*m_pFlashEventConnectionPoint;								///<ConnectionPoint 포인터
	//CFlashEventSink		*m_pFlashEventSink;											///<Flash 이벤트 싱크 객체

public:
	ciString			m_wizardXML;				// wizard 용 XML string
	ciString			m_wizardFiles;				// wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

	CString				m_strWizardFlashPath;		///<Wizard 플래쉬 파일의 경로

	virtual LPCSTR		GetContentsTypeString() { return "Wizard"; };

	virtual void		SetSchedule(LPCSTR lpszID);

	bool				CreateFile();

	virtual bool		Play();
	void				RecvFsCommand(CString strCmd, CString strArg);		///<FsCommand를 처리한다.
	
	CString				ToString();
	void				ToGrid(CPropertyGrid* pGrid);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//! <Office _Application 상속 받은 개체>
/*!
	_Application을 single tone으로 사용하기 위하여 상속받음 \n
*/
class COfficeApp : public _Application
{
public:
	static COfficeApp* getInstance();
	static COfficeApp* getInstance(LPCSTR lpszFullpath,	///<Single tone 인스턴스 생성 함수
									//LPCSTR lpszFilename,
									CPPTInvokerInterface* pParent);

	static void clearInstance();							///<Single tone 인스턴스 소멸 함수

	virtual ~COfficeApp();									///<소멸자

	static void SetOpenComplete(CString strFilename, bool bOpen=true);
	static bool IsAllOpen() { return (_refcnt==_mapOpenPPT.GetCount()); };

	static LPCSTR			getTempFolderName();			///<임시파일을 복사할 디렉토리

protected:
	COfficeApp();											///<생성자

	static COfficeApp*		_instance;						///<Single tone 인스턴스
	static CCriticalSection	_lock;							///<동기화 객체
	static int				_refcnt;						///<참조 횟수
	static int				_opencnt;						///<열린 ppt 개수
//	static CString			_tempfolder;					///<임시 디렉토리 이름

	static IConnectionPoint*	_pptEventConnectionPoint;	///<ConnectionPoint 포인터
	static CPPTEventSinkEx*		_pptEventSink;				///<파워포인트 이벤트 싱크 객체

	static bool ConnectPPTEventSink(COfficeApp* instance);						///<파워포인트 이벤트 싱크 설정 함수
	static void DisconnectPPTEventSink();					///<파워포인트 이벤트 싱크 해제 함수

	static CMapStringToString _mapOpenPPT;

public:
	BOOL	Open(LPCSTR lpszFullpath/*, LPCSTR lpszFilename*/);
	BOOL	Play(LPCSTR lpszFullpath);
	BOOL	SlideShowBegin(LPCSTR lpszFullpath);
	//BOOL	SlideShowEnd(LPCSTR lpszFilename);
	BOOL	ClosePresentation(LPCSTR lpszFullpath);

	float				m_fPosX;							///<슬라이드쇼 창이 위치할 X 좌표
	float				m_fPosY;							///<슬라이드쇼 창이 위치할 Y 좌표
	float				m_fWidth;							///<슬라이드쇼 창 넓이
	float				m_fHeight;							///<슬라이드쇼 창 높이

};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPowerPointSchedule

//! MS-Office Power Point contents를 재생하는 클래스
/*!
*/
class CPowerPointSchedule : public CSchedule, public CPPTInvokerInterface
{
	DECLARE_DYNAMIC(CPowerPointSchedule)

public:
	CPowerPointSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);		///<로컬 모드에서의 생성자
	virtual ~CPowerPointSchedule();																					///<소멸자

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);															///<WM_CREATE 이벤트 함수
	afx_msg void OnPaint();																							///<WM_PAINT 이벤트 함수
	afx_msg void OnDestroy();																						///<WM_DESTORY 이벤트 함수
	afx_msg void OnTimer(UINT_PTR nIDEvent);

protected:
	DECLARE_MESSAGE_MAP()

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	//PPT OLE 객체
	COfficeApp*			m_pptApp;													///<MS-PowerPoint 어플리케이션
	//Presentations		m_pptPresentations;											///<MS-PowerPoint Presentations
	//_Presentation		m_pptPresentation;											///<MS-PowerPoint Presentation
	//SlideShowSettings	m_pptSlideshow;												///<MS-PowerPoint SlideShow settings
	//SlideShowWindow		m_pptSlideShowWnd;										///<MS-PowerPoint SlideShow window
	
//	IConnectionPoint	*m_pPPTEventConnectionPoint;								///<ConnectionPoint 포인터
//	CPPTEventSink		*m_pPPTEventSink;											///<파워포인트 이벤트 싱크 객체

	float				m_fPosX;													///<슬라이드쇼 창이 위치할 X 좌표
	float				m_fPosY;													///<슬라이드쇼 창이 위치할 Y 좌표
	float				m_fWidth;													///<슬라이드쇼 창 넓이
	float				m_fHeight;													///<슬라이드쇼 창 높이

	CRect				m_rectBGimage;												///<배경 이미지가 그려질 영역
	CString				m_strPresentationName;										///<이벤트를 처리해야는 Presentation 이름
	bool				m_bTopMost;													///<PPT slideshow 윈도우의 topmost 여부
	int					m_nLocale;													///<OS의 국가코드

public:

	virtual LPCSTR	GetContentsTypeString() { return "MS-Office Power Point"; };	///<schedule의 contents type을 문자열로 반환하는 함수

	virtual void	SetSchedule(LPCSTR lpszID);										///<로컬 모드에서 schedule을 설정하는 함수
	virtual double	GetTotalPlayTime();												///<contents의 전체 재생시간을 반환하는 함수
	virtual double	GetCurrentPlayTime();											///<contens의 현재 재생경과 시간을 반환하는 함수

	virtual	bool	OpenFile(int nGrade);											///<contents 파일을 open하는 함수
	virtual bool	CloseFile();													///<contents 파일을 close 하는 함수

	virtual bool	Play();															///<schedule을 play하는 함수
	virtual bool	Pause();														///<schedule을 일시 정지 하는 함수
	virtual bool	Stop();															///<schedule을 정지하는 함수
	virtual void	RePlay(void);													///<스케줄을 처음부터 다시 재생한다.

	//virtual CString	ToString();														///<schedule의 정보를 문자열로 반환하는 함수
	//virtual void ToGrid(CPropertyGrid* pGrid);										///<shcedule의 정보 문자열을 logdialog에 설정하는 함수 

//	bool			ConnectPPTEventSink(void);										///<파워포인트 이벤트 싱크 설정 함수
//	void			DisconnectPPTEventSink(void);									///<파워포인트 이벤트 싱크 해제 함수
	void			SlideShowEnd(void);												///<슬라이드 쇼가 끝났음을 설정
	void			ClosePresentation(void);										///<ppt 슬라이드 쇼를 닫는다
	void			SetTopMostSlideShow(void);										///<슬라이드 쇼 창을 TopMost로 설정한다.
	void			SetSlideShowResolution(void);									///<슬라이드 쇼의 해상도를 시스템의 해상도에 맞춘다.

	// CPPTInvokerInterface
	virtual void	SetSlideShowEnd(float ftTime);									///<슬라이드 쇼를 일정한 시간후에 종료하도록 설정
	virtual void	SlideShowBegin(void);											///<슬라이드 쇼가 시작되었음을 설정
	virtual void	PresentationOpen(void);											///<Presentation이 Open 된 이후 슬라이드 쇼를 시작하도록 한다.
	virtual void	EscapeBRW(void);												///<파워포인트에 ESC 키가 눌러져서 BRW를 종료한다.
	virtual bool	StopPlay() { return Stop(); }

	typedef struct _ST_FindWnd
	{
		char	szSlideName[256];		///<슬라이드 쇼 이름
		HWND	hWnd;					///<윈도우 핸들
	} ST_FindWnd;

	static BOOL CALLBACK EnumWndProc(HWND hwnd, LPARAM lParam);						///<EnumWindow를 통한 슬라이드쇼 윈도우를 찾는 call back 함수
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CTVSchedule
class CTVSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CTVSchedule)

public:
	CTVSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CTVSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

protected:
	DECLARE_MESSAGE_MAP()

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	ciShort			m_soundVolume;
	ciString		m_bgColor;		// 
	ciString		m_fgColor;		// 

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	CTVPlayer	m_playerTV;			///<TV 클래스
	int			m_nChennel;			///<방송 채널
	TV_MODE		m_nModeTV;			///<TV 모드
	COLORREF	m_rgbBgColor;
	COLORREF	m_rgbFgColor;
	bool		m_bPause;			///<Pause 여부
	bool		m_bHasTV;			///<TV 모듈이 설치되어 있는지 여부


public:
	virtual LPCSTR	GetContentsTypeString() { return "TV"; };

	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play();
	virtual bool	Pause();
	virtual bool	Stop();
	virtual	void	SoundVolumeMute(bool bMute);

	virtual CString	ToString();
	virtual void ToGrid(CPropertyGrid* pGrid);
	afx_msg void OnPaint();
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define		SMS_HEADER		"<html><body background=\"%s\"><span style=\"font-family:%s; font-size:%dpt; color:%s; background-color:%s; \">\n"
#define		SMS_TAIL		"\n</span></body></html>"

#define		TICKER_HEADER	"\n\
<html><head>\n\
<style type=\"text/css\">\n\
#pscroller { width: %dpx; height: %dpx; border: 0px solid black; padding: 0px; }\n\
.someclass{ }\n\
</style>\n\
<script type=\"text/javascript\">\n\
var pausecontent=new Array()\n"

#define		TICKER_CONTENT	"pausecontent[%d]='<center><span style=\"font-family:%s; font-size:%dpt; color:%s; background-color:%s; \">%s</span></center>'\n"

#define		TICKER_TAIL		"\n\
</script>\n\
<script type=\"text/javascript\">\n\
function pausescroller(content, divId, divClass, delay){\n\
this.content=content\n\
this.tickerid=divId\n\
this.delay=delay\n\
this.mouseoverBol=0\n\
this.hiddendivpointer=1\n\
document.write('<div id=\"'+divId+'\" class=\"'+divClass+'\" style=\"position: relative; overflow: hidden\"><div class=\"innerDiv\" style=\"position: absolute; width: 100%%\" id=\"'+divId+'1\">'+content[0]+'</div><div class=\"innerDiv\" style=\"position: absolute; width: 100%%; visibility: hidden\" id=\"'+divId+'2\">'+content[1]+'</div></div>')\n\
var scrollerinstance=this\n\
if (window.addEventListener)\n\
window.addEventListener(\"load\", function(){scrollerinstance.initialize()}, false)\n\
else if (window.attachEvent)\n\
window.attachEvent(\"onload\", function(){scrollerinstance.initialize()})\n\
else if (document.getElementById)\n\
setTimeout(function(){scrollerinstance.initialize()}, 500)\n\
}\n\
\n\
pausescroller.prototype.initialize=function(){\n\
this.tickerdiv=document.getElementById(this.tickerid)\n\
this.visiblediv=document.getElementById(this.tickerid+\"1\")\n\
this.hiddendiv=document.getElementById(this.tickerid+\"2\")\n\
this.visibledivtop=parseInt(pausescroller.getCSSpadding(this.tickerdiv))\n\
this.visiblediv.style.width=this.hiddendiv.style.width=this.tickerdiv.offsetWidth-(this.visibledivtop*2)+\"px\"\n\
this.getinline(this.visiblediv, this.hiddendiv)\n\
this.hiddendiv.style.visibility=\"visible\"\n\
var scrollerinstance=this\n\
document.getElementById(this.tickerid).onmouseover=function(){scrollerinstance.mouseoverBol=1}\n\
document.getElementById(this.tickerid).onmouseout=function(){scrollerinstance.mouseoverBol=0}\n\
if (window.attachEvent)\n\
window.attachEvent(\"onunload\", function(){scrollerinstance.tickerdiv.onmouseover=scrollerinstance.tickerdiv.onmouseout=null})\n\
setTimeout(function(){scrollerinstance.animateup()}, this.delay)\n\
}\n\
\n\
pausescroller.prototype.animateup=function(){\n\
var scrollerinstance=this\n\
if (parseInt(this.hiddendiv.style.top)>(this.visibledivtop+5)){\n\
this.visiblediv.style.top=parseInt(this.visiblediv.style.top)-5+\"px\"\n\
this.hiddendiv.style.top=parseInt(this.hiddendiv.style.top)-5+\"px\"\n\
setTimeout(function(){scrollerinstance.animateup()}, 50)\n\
}\n\
else{\n\
this.getinline(this.hiddendiv, this.visiblediv)\n\
this.swapdivs()\n\
setTimeout(function(){scrollerinstance.setmessage()}, this.delay)\n\
}\n\
}\n\
\n\
pausescroller.prototype.swapdivs=function(){\n\
var tempcontainer=this.visiblediv\n\
this.visiblediv=this.hiddendiv\n\
this.hiddendiv=tempcontainer\n\
}\n\
\n\
pausescroller.prototype.getinline=function(div1, div2){\n\
div1.style.top=this.visibledivtop+\"px\"\n\
div2.style.top=Math.max(div1.parentNode.offsetHeight, div1.offsetHeight)+\"px\"\n\
}\n\
\n\
pausescroller.prototype.setmessage=function(){\n\
var scrollerinstance=this\n\
if (this.mouseoverBol==1)\n\
setTimeout(function(){scrollerinstance.setmessage()}, 100)\n\
else{\n\
var i=this.hiddendivpointer\n\
var ceiling=this.content.length\n\
this.hiddendivpointer=(i+1>ceiling-1)? 0 : i+1\n\
this.hiddendiv.innerHTML=this.content[this.hiddendivpointer]\n\
this.animateup()\n\
}\n\
}\n\
\n\
pausescroller.getCSSpadding=function(tickerobj){\n\
if (tickerobj.currentStyle)\n\
return tickerobj.currentStyle[\"paddingTop\"]\n\
else if (window.getComputedStyle)\n\
return window.getComputedStyle(tickerobj, \"\").getPropertyValue(\"padding-top\")\n\
else\n\
return 0\n\
}\n\
\n\
</script>\n\
</head>\n\
\n\
<body background=\"%s\">\n\
<script type=\"text/javascript\">\n\
new pausescroller(pausecontent, \"pscroller\", \"someclass\", %d)\n\
</script>\n\
</body>\n\
</html>"
