// BRW2.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include <afxole.h>
#include "BRW2.h"
#include "Host.h"
#include "custsite.h"
#include "common/libCommon/utvUtil.h"
#include "ci/libWorld/ciWorld.h"
#include "ci/libDebug/ciArgParser.h"
#include "common/libTimeSyncer/TimeSyncTimer.h"
#include "common/libFlashRollback/libFlashRollback.h"

//#include <util/libEncoding/Encoding.h>

//#include "cci/libPfUtil/cciDSUtil.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//에러확인없이 종료하기
//LONG WINAPI NoMsgExceptionFilter(struct _EXCEPTION_POINTERS* /*ExceptionInfo*/)
LONG CALLBACK NoMsgExceptionFilter(_EXCEPTION_POINTERS* lpExceptionInfo)
{
	// 아무일도 하지 않고 그냥 종료하기

	CString str;
	str.Format("Exception : 0x%08X\r\n", lpExceptionInfo->ExceptionRecord->ExceptionCode);
	__ERROR__(str, _NULL);   
	str.Format("Exception Address : 0x%08p\r\n", lpExceptionInfo->ExceptionRecord->ExceptionAddress);
	__ERROR__(str, _NULL);

	//AfxAbort( );		//비정상종료
	__DEBUG__("Before exit", _NULL);
	_exit(0);
	__DEBUG__("After exit", _NULL);

	return EXCEPTION_EXECUTE_HANDLER;
	//return EXCEPTION_CONTINUE_EXECUTION;
}

void _purecall_handler_function(void)
{
	__ERROR__("Virtual funtion call", _NULL);
	RaiseException(EXCEPTION_ACCESS_VIOLATION, 0, 0, NULL);
}



// CBRW2App

BEGIN_MESSAGE_MAP(CBRW2App, CWinApp)

END_MESSAGE_MAP()


// CBRW2App 생성

CBRW2App::CBRW2App()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.

	m_stInfoMonitor.nMonitor = 1;
	m_stInfoMonitor.nWidthVirtual = 1024;
	m_stInfoMonitor.nHeightVirtual = 768;
	m_stInfoMonitor.nWidth = 1024;
	m_stInfoMonitor.nHeight = 768;
	m_stInfoMonitor.nBitPerPixel= 16;
	m_stInfoMonitor.nRefresh = 60;	

	m_pDispOM = NULL;
}



// 유일한 CBRW2App 개체입니다.

CBRW2App theApp;


// CBRW2App 초기화

BOOL CBRW2App::InitInstance()
{
	SetErrorMode(SEM_NOGPFAULTERRORBOX);					//에러창 뛰우지 않기
	 // skpark 2014.06.16 ERROR 추가
	 // SetErrorMode(SEM_NOGPFAULTERRORBOX | SEM_NOOPENFILEERRORBOX | SEM_FAILCRITICALERRORS | SEM_NOALIGNMENTFAULTEXCEPT ); 
	 // skpark 2014.06.16
	 // WIN7 이상에서는 다음과 같은 레지스트리 작업을 해주는 것이 좋다.
	 // HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Windows\ErrorMode 0 에서 2로 설정 변경 
	 // HKEY_CURRENT_USER\Software\ Microsoft\Windows\Windows Error Reporting\DontShowUI 0 에서 1로 설정 변경 
	 
	 _set_purecall_handler(_purecall_handler_function);		//pure virtual functin call 핸들링
	::SetUnhandledExceptionFilter(NoMsgExceptionFilter);	//에러확인없이 종료하기

	//0xC015000F: 활성화를 해제하고 있는 활성화 컨텍스트는 가장 최근에 활성화한 컨텍스트가 아닙니다.
	//오류가 발생할 경우
	afxAmbientActCtx = FALSE;

	//CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
	//AfxSetAllocStop(624);
	// Initialize OLE libraries
	if(!AfxOleInit())
	{
		AfxMessageBox("OLE initialization failed.  Make sure that the OLE libraries are the correct version.");
		return FALSE;
	}//if

	//COM Server의 응답시간을 설정한다.
	//서버가응답없음 - 전환, 대기 메시지상자가 나오는 문제를 방지하기 위함
	//COleMessageFilter* pFilter = AfxOleGetMessageFilter();
	AfxOleGetMessageFilter()->EnableNotRespondingDialog(FALSE); 
	AfxOleGetMessageFilter()->EnableBusyDialog(FALSE); 
	AfxOleGetMessageFilter()->SetMessagePendingDelay(5000);

	ciArgParser::initialize(__argc,__argv);
	//PROJECT_HOME 환경셋팅
	ciEnv::defaultEnv(ciFalse, "utv1");   //skpark 2010.0916
	
	CString strLogFilename;
	string strHost;
	ciArgParser::getInstance()->getArgValue("+host", strHost);
	strLogFilename.Format("log\\%s_%s.log", ::AfxGetAppName(), strHost.c_str());

	__LOG_INIT__(strLogFilename);
	UserLog::getInstance()->LogOpen();
	
	// skpark 2013.2.21 write시마다 매번 open/close 하는 것으로 바꾼다.
	// 그러나 처음 한번은 어쨌거나 오픈한다.
	InterActLog::getInstance()->LogOpen();

	ciDebug::setDebugOn();
	//skpark 2012.5.11	libDownload 로그
	ciString downloadLog = GetAppPath();
	downloadLog +=  "\\log\\Downloader_" + strHost + ".log";
	ciString backupfile = downloadLog + ".bak";
	::DeleteFile(backupfile.c_str());
	::MoveFile(downloadLog.c_str(), backupfile.c_str());
	ciDebug::logOpen(downloadLog.c_str());
	//ciWorld::ylogOn();

	//Firewall exception 등록
	scratchUtil* aUtil = scratchUtil::getInstance();
	if(!aUtil->AddToExceptionList())
	{
		__DEBUG__("Fail to add windows firewall exception", _NULL);
	}//if

	//+template, +replace 옵션이 있는 경우를 제외하고는 동시에 같은 
	//프로그램이 구동하지 못하도록 막는다.
	if(!CheckDuplicate())
	{
		return FALSE;
	}//if

	// 파워포인트 자동복구 방지 레지스트리 세팅
	DisablePPTAutoRecovery();

	// IE버전 체크
	CheckIEVersion();

	// Browser 에서 timeSync를 한번 수행한다.
	TimeSyncTimer::timeSync();


	//+download_only 옵션의 경우, 같은 Player 가 있을 경우 죽인다.
	//반대로, +download_only 옵션이 없을 경우, 3RDPARTY_BROWSER 가 있으면 죽인다.
	Check_download_only_case();

	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Browser 컨트롤의 no3Dborder, No Scroll 설정
	if( ::GetShowWebScrollbar() )
	{
		AfxEnableControlContainer();
	}
	else
	{
		CCustomOccManager *pMgr = new CCustomOccManager;
		m_pDispOM = new CImpIDispatch;
		AfxEnableControlContainer(pMgr);
	}

	CHost dlg;
	ProtectCopy(dlg.m_strEdition, dlg.m_strVersion, dlg.m_strComputerName, dlg.m_strRegstrationKey, dlg.m_bVerify);

	// skpark same_size_file_problem
	CString use_time_check = GetINIValue("UBCVariables.ini", "ROOT", "USE_TIME_CHECK");
	dlg.m_useTimeCheck = atoi(use_time_check);

	ciString strArgTmplateID;
	if(ciArgParser::getInstance()->getArgValue("+template", strArgTmplateID))
	{
		if(strArgTmplateID.length() > 0)
		{
			dlg.m_strArgTemplateID = strArgTmplateID.c_str();
			__DEBUG__("Template Mode", _NULL);
			//Preview 모드에서는 인증이된것으로 간주하고 edition, key는 preview 로 표시
			dlg.m_bVerify = true;
			dlg.m_strEdition = "Preview mode";
			dlg.m_strRegstrationKey = "Preview mode";
		}
		else
		{
			AfxMessageBox("Invalid +template argument value");
			return FALSE;
		}//if
	}//if

	// 플래시 ocx 롤백
	CFlashRollback fbb;
	fbb.CheckFlashFile();

	//Divx로고 없애기
	DisableDivxLogo();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("UTV\\BRW2"));

	//모니터의 정보를 얻어와서 프로그램이 구동되는 위치를 정한다
	int nPosX, nPosY, nCx, nCy, nMonitorIndex;
	GetMonitorPosition(nPosX, nPosY, nCx, nCy, nMonitorIndex);
	dlg.SetAppPosition(nPosX, nPosY, nCx, nCy, nMonitorIndex);

	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}



	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

int CBRW2App::ExitInstance()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	__DEBUG__("Exit Instance", _NULL);

	if(m_pDispOM)
	{
		delete m_pDispOM;
		m_pDispOM = NULL;
	}//if

	ciArgParser::terminate();
	ciEnv::clearEnv();
	//ACE::fini();

	UserLog::clearInstance(); 
	InterActLog::clearInstance();
	ciWorld::ylogOff();

	CoUninitialize();

	return CWinApp::ExitInstance();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 모니터 정보를 얻는 컬백함수 \n
/// @param (HMONITOR) hMonitor : (in) handle to display monitor
/// @param (HDC) hdcMonitor : (in) handle to monitor DC
/// @param (LPRECT) lprcMonitor : (in) monitor intersection rectangle
/// @param (LPARAM) dwData : (in) data
/////////////////////////////////////////////////////////////////////////////////
BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{
	MONITORINFOEX mi;

	mi.cbSize=sizeof(MONITORINFOEX);
	GetMonitorInfo(hMonitor,&mi);
	theApp.m_stInfoMonitor.aryMonitors.Add(mi);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 모니터 정보를 구한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBRW2App::GetMonitorInformation(void)
{
	m_stInfoMonitor.nMonitor = ::GetSystemMetrics(SM_CMONITORS);
	__DEBUG__("Monitor count", m_stInfoMonitor.nMonitor);
	m_stInfoMonitor.bSameDisplayFormat = ::GetSystemMetrics(SM_SAMEDISPLAYFORMAT); 
	
	m_stInfoMonitor.rcVirtual.left = GetSystemMetrics(SM_XVIRTUALSCREEN);
	m_stInfoMonitor.rcVirtual.top = GetSystemMetrics(SM_YVIRTUALSCREEN);
	m_stInfoMonitor.rcVirtual.right = m_stInfoMonitor.rcVirtual.left + GetSystemMetrics(SM_CXVIRTUALSCREEN);
	m_stInfoMonitor.rcVirtual.bottom = m_stInfoMonitor.rcVirtual.top + GetSystemMetrics(SM_CYVIRTUALSCREEN);


	m_stInfoMonitor.nWidth = ::GetSystemMetrics(SM_CXSCREEN);						// 모니터의 해상도 x
	m_stInfoMonitor.nHeight = ::GetSystemMetrics(SM_CYSCREEN);						// 모니터의 해상도 y
	m_stInfoMonitor.nWidthVirtual = ::GetSystemMetrics(SM_CXVIRTUALSCREEN);			// 가상모니터의 해상도 x
	m_stInfoMonitor.nHeightVirtual = ::GetSystemMetrics(SM_CYVIRTUALSCREEN);		// 가상모니터의 해상도 y
	
	m_stInfoMonitor.nMegaPixel = (m_stInfoMonitor.nWidth * m_stInfoMonitor.nHeight)/(1000*1000);
	m_stInfoMonitor.nMegaPixel = max(1,m_stInfoMonitor.nMegaPixel);

	HDC hDC=NULL;
	hDC = CreateDC("DISPLAY", NULL, NULL, NULL);
	if(hDC)
	{
		EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, (LPARAM)hDC);
		m_stInfoMonitor.nBitPerPixel = GetDeviceCaps(hDC, BITSPIXEL);
		m_stInfoMonitor.nRefresh = GetDeviceCaps(hDC, VREFRESH);
		DeleteDC(hDC);
	}
	else
	{
		__DEBUG__("Fail to GetMonitorInformation", _NULL);
		return false;
	}//if
	
	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 라이센스를 확인한다 \n
/// @param (CString&) strEdition : (out) 제품의 Edition
/// @param (CString&) strVersion : (out) 프로그램 버전
/// @param (CString&) strComputerName : (out) 실행되는 컴퓨터의 이름
/// @param (CString&) strKey : (out) 제품의 Key
/// @param (bool&) bVerified : (out) 제품의 Key인증을 받았는지 여부
/// @return <형: void> \n
/////////////////////////////////////////////////////////////////////////////////
void CBRW2App::ProtectCopy(CString& strEdition, CString& strVersion, CString& strComputerName, CString& strKey, bool& bVerified)
{
	TCHAR  szComputerName[MAX_COMPUTERNAME_LENGTH+1] = { 0x00 };
	DWORD  dwBufSize = MAX_COMPUTERNAME_LENGTH+1;
	//CString strMsg;
	//ciString strAdtMac;

	bVerified = false;
	strVersion = "Trial";
	strComputerName = "Trial";
	strKey = "Trial";

	//먼저 컴퓨터의 이름(호스트 명)을 구한다
	if(!GetComputerNameEx(ComputerNamePhysicalDnsHostname, szComputerName, &dwBufSize))
	{
		__DEBUG__("Get computer name fail", _NULL);
		return;
	}//if
	strComputerName = szComputerName;

	//등록파일을 읽어서 컴퓨터 이름과 네트워크 카드의 맥주소를 비교하여,
	//하나라도 불일치한다면 정품이 아니다.
	ciString strHost, strMac, strEdit, strVer;
	scratchUtil* aUtil = scratchUtil::getInstance();
	if(!aUtil->readAuthFile(strHost, strMac, strEdit))
	{
		__DEBUG__("AuthFile read fail", _NULL);
		return;
	}//if

	if(!ciArgParser::getInstance()->isSet("+static_hostname")) {
		//컴퓨터 이름(호스트네임) 비교
		if(strComputerName != strHost.c_str())
		{
			__DEBUG__("Host name not valid", strComputerName);
			return;
		}//if
	}
	//인증 파일에 있는 맥주소가 system의 network adapter에 있는지 확인
	if(!aUtil->IsExistmacaddr(strMac. c_str()))
	{
		__DEBUG__("Mac address not valid", strMac.c_str());
		return;
	}//if

	if(strEdition == "")
	{
		strEdition = STANDARD_EDITION;
	}//if
	strEdition = strEdit.c_str();
	aUtil->getVersion(strVer, false);
	strVersion = strVer.c_str();
	//strComputerName = strComName.c_str();
	strKey = strHost.c_str();
	//strKey = "DEMO" + strKey;

	//Key 값에 "DEMO" 값이 있다면 인증이 안된것으로 처리...
	if(strKey.Find("DEMO", 0) != -1)
	{
		bVerified = false;
	}
	else
	{
		bVerified = true;
	}//if

	__DEBUG__("Edition", strEdition);
	__DEBUG__("Version", strVersion);
	__DEBUG__("ComputerName", strComputerName);
	__DEBUG__("RegstrationKey", strKey);
	__DEBUG__("bVerified", bVerified);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 중복실행 방지 \n
/// @return <형: bool> \n
///			<true: 중복없음> \n
///			<false: 중복된 실행> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBRW2App::CheckDuplicate()
{
	if(!ciArgParser::getInstance()->isSet("+replace")
		&& !ciArgParser::getInstance()->isSet("+template")
		&& !ciArgParser::getInstance()->isSet("+download_only"))
	{
		CString strAppName = ::AfxGetAppName();
		HANDLE hMutex = ::CreateMutex(NULL, FALSE, strAppName);
		if(hMutex != NULL)
		{
			if(::GetLastError() == ERROR_ALREADY_EXISTS)
			{
				unsigned long lPid = scratchUtil::getInstance()->getPid(strAppName + ".exe");
				HWND hWnd = scratchUtil::getInstance()->getWHandle(lPid);
				if(hWnd)
				{
					::ReleaseMutex(hMutex);
					//활성화된 BRW를 화면에 보여주도록 한다.
					::ShowWindow(hWnd, SW_MAXIMIZE);
					exit(1);
					return FALSE;
				}//if
				// Windows Handle 이 없다면 좀비로 인식하고 죽임.
				__DEBUG__("killProcess",(long)lPid);
				scratchUtil::getInstance()->killProcess(lPid);
			}//if
			::ReleaseMutex(hMutex);
		}//if
	}//if

	return TRUE;
}

bool
CBRW2App::Check_download_only_case()
{
	if(ciArgParser::getInstance()->isSet("+download_only")){
		//download_only 인경우, 다른 브라우저가 있으면 죽인다.
		__DEBUG__("Check_download_only_case","+donwload_only set");

		CString strAppName = ::AfxGetAppName();
		unsigned long lPid = scratchUtil::getInstance()->getPid(strAppName + ".exe");
		HWND hWnd = scratchUtil::getInstance()->getWHandle(lPid);
		if(!hWnd)
		{
			return true;
		}
		__DEBUG__("killProcess",(long)lPid);
		scratchUtil::getInstance()->killProcess(lPid);
		return true;
	}

	//download_only 가 아닌 경우, 3RDPARTY_PLAYER가 있으면 죽인다.
	__DEBUG__("Check_download_only_case","+donwload_only not set");
	return Stop3rdPartyPlayer();

}

bool CBRW2App::Stop3rdPartyPlayer()
{
	__DEBUG__("Stop3rdPartyPlayer()", NULL);
	
	CString strArg = "3RDPARTY_PLAYER";

	CString fullpath = GetINIValue("UBCStarter.ini", strArg, "BINARY_NAME");
	if(fullpath.IsEmpty()){
		__DEBUG__("There is no ", strArg);
		return false;
	}
	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(fullpath, cDrive, cPath, cFilename, cExt);

	CString binary = cFilename;
	binary += cExt;

	if(scratchUtil::getInstance()->getPid(binary)<=0){
		__DEBUG__("process does not run", binary);
		return false;
	}

	if(!scratchUtil::getInstance()->stopProcess(strArg.GetBuffer()))
	{
		__DEBUG__("Fail to call stop", strArg);
		return false;
	}//if


	__DEBUG__("Succeed to call stop", strArg);
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램이 위치할 모니터의 위치를 구한다 \n
/// @param (int&) nPosX : (out) X 위치
/// @param (int&) nPosY : (out) Y 위치
/// @param (int&) nCx : (out) 넓이
/// @param (int&) nCy : (out) 높이
/// @param (int&) nMonitorIndex : (out) 모니터의 인덱스
/////////////////////////////////////////////////////////////////////////////////
void CBRW2App::GetMonitorPosition(int& nPosX, int& nPosY, int& nCx, int& nCy, int& nMonitorIndex)
{
	if(!GetMonitorInformation())
	{
		nPosX	=	0;
		nPosY	=	0;
		nCx		=	m_stInfoMonitor.nWidth;
		nCy		=	m_stInfoMonitor.nHeight;
		nMonitorIndex = 0;

		return;
	}
	
	//멀티모니터 지원
	if(ciArgParser::getInstance()->isSet("+multivision"))
	{
		__DEBUG__("Set multivision", _NULL);
		nPosX	=	m_stInfoMonitor.rcVirtual.left;
		nPosY	=	m_stInfoMonitor.rcVirtual.top;
		nCx		=	m_stInfoMonitor.nWidthVirtual;
		nCy		=	m_stInfoMonitor.nHeightVirtual;
		nMonitorIndex = 0;

		return;
	}//if

	//모니터의 갯수가 1개 이상이면 프로그램의 이름에 따라 프로그램이 위치하는 모니터의 위치를 조정한다
	//프로그램의 이름이 UTV_brwClient2_UBC1.exe 이면 2번째 모니터에 출력하고
	//나머지의 경우에는 1번 모니터에 출력한다.
	ciString strMonitor;
	int nMonitor;
	if(ciArgParser::getInstance()->getArgValue("+monitor", strMonitor))
	{
		nMonitor = atoi(strMonitor.c_str());
		if(nMonitor == 2 && m_stInfoMonitor.aryMonitors.GetCount() > 1)
		{
			nPosX	=	m_stInfoMonitor.aryMonitors.GetAt(1).rcMonitor.left;
			nPosY	=	m_stInfoMonitor.aryMonitors.GetAt(1).rcMonitor.top;
			nCx		=	abs(m_stInfoMonitor.aryMonitors.GetAt(1).rcMonitor.right - nPosX);
			nCy		=	abs(m_stInfoMonitor.aryMonitors.GetAt(1).rcMonitor.bottom - nPosY);
			nMonitorIndex = 1;
		}
		else
		{
			nPosX	=	m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.left;
			nPosY	=	m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.top;
			nCx		=	abs(m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.right - nPosX);
			nCy		=	abs(m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.bottom - nPosY);
			nMonitorIndex = 0;
		}//if
	}
	else
	{
		CString strAppName = ::AfxGetAppName();
		if(strAppName == "UTV_brwClient2_UBC1" && m_stInfoMonitor.aryMonitors.GetCount() > 1)
		{
			nPosX	=	m_stInfoMonitor.aryMonitors.GetAt(1).rcMonitor.left;
			nPosY	=	m_stInfoMonitor.aryMonitors.GetAt(1).rcMonitor.top;
			nCx		=	abs(m_stInfoMonitor.aryMonitors.GetAt(1).rcMonitor.right - nPosX);
			nCy		=	abs(m_stInfoMonitor.aryMonitors.GetAt(1).rcMonitor.bottom - nPosY);
			nMonitorIndex = 1;
		}
		else
		{
			nPosX	=	m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.left;
			nPosY	=	m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.top;
			nCx		=	abs(m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.right - nPosX);
			nCy		=	abs(m_stInfoMonitor.aryMonitors.GetAt(0).rcMonitor.bottom - nPosY);
			nMonitorIndex = 0;
		}//if
	}//if	
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Divxe디코더에서 로고가 나오는것을 막는다. \n
/////////////////////////////////////////////////////////////////////////////////
void CBRW2App::DisableDivxLogo()
{
	HKEY hKey;
	LONG lRet;
	DWORD dwVal;
	CString strRegKey = "Software\\DivXNetworks\\DivX4Windows";

	lRet = RegOpenKeyEx(HKEY_CURRENT_USER, strRegKey, 0, KEY_ALL_ACCESS, &hKey);
	if(lRet == ERROR_SUCCESS)
	{
		dwVal = 0;
		lRet = RegSetValueEx(hKey, _T("AVC 7x Logo"), 0, REG_DWORD, (BYTE*)&dwVal, sizeof(dwVal));  
		if(lRet == ERROR_SUCCESS)
		{
			__DEBUG__("Divx logo disadbled", _NULL);
		}
		else
		{
			__DEBUG__("Fail to divx logo disadbl", _NULL);
		}//if
		RegCloseKey(hKey);
	}
	else
	{
		__DEBUG__("Fail to open registry key", strRegKey);
	}//if


	//WMV 동영상 재생시에 비디오 필터로 "WMVideo Decoder DMO"를 사용하지 않고 
	//"ffdshow Video Decoder"를 사용하여야 하며, 이렇게 하기 위해서 VFW video필터 설정에서
	//"libavcodec"을 사용하도록 필터설정을 변경하여야 한다.
	//[HKEY_CURRENT_USER\Software\GNU\ffdshow]
	//wmv1, wmv2, wmv3 가 해당하는 키값이며, 0x00=disable, 0x01=libavcodec, 0x0c=wmv9 이다.
	//xvid, divx, h264 계열의 동영상도 동일하게 적용...
	DWORD dwVal_ffdshow_wmv = 1;
	if( atoi(GetINIValue("UBCBrowser.ini", "ROOT", "USE_WMV_DECODER_DMO")) )
	{
		__DEBUG__("Use WMVideo Decoder DMO", _NULL);
		dwVal_ffdshow_wmv = 0;
	}
	else
	{
		__DEBUG__("Use FFDShow WMV Codec", _NULL);
		dwVal_ffdshow_wmv = 1;
	}

	DWORD dwVal_ffdshow_mpeg4 = 1;
	if( atoi(GetINIValue("UBCBrowser.ini", "ROOT", "USE_DEFAULT_MPEG4_CODEC")) )
	{
		__DEBUG__("Use System Default MPEG4(xvid,div3,dx50,h264) Codec", _NULL);
		dwVal_ffdshow_mpeg4 = 0;
	}
	else
	{
		__DEBUG__("Use FFDShow MPEG4(xvid,div3,dx50,h264) Codec", _NULL);
		dwVal_ffdshow_mpeg4 = 1;
	}

	strRegKey = "Software\\GNU\\ffdshow";
	lRet = RegOpenKeyEx(HKEY_CURRENT_USER, strRegKey, 0, KEY_ALL_ACCESS, &hKey);
	if(lRet == ERROR_SUCCESS)
	{
		dwVal = 1;
		lRet = RegSetValueEx(hKey, _T("wmv1"), 0, REG_DWORD, (BYTE*)&dwVal_ffdshow_wmv, sizeof(dwVal_ffdshow_wmv));  
		if(lRet == ERROR_SUCCESS)
		{
			__DEBUG__("wmv1 set to using", dwVal_ffdshow_wmv ? "libavcodec" : "WMVideo Decoder DMO");
		}
		else
		{
			__DEBUG__("Fail to set wmv1", _NULL);
		}//if

		lRet = RegSetValueEx(hKey, _T("wmv2"), 0, REG_DWORD, (BYTE*)&dwVal_ffdshow_wmv, sizeof(dwVal_ffdshow_wmv));  
		if(lRet == ERROR_SUCCESS)
		{
			__DEBUG__("wmv2 set to using", dwVal_ffdshow_wmv ? "libavcodec" : "WMVideo Decoder DMO");
		}
		else
		{
			__DEBUG__("Fail to set wmv2", _NULL);
		}//if

		lRet = RegSetValueEx(hKey, _T("wmv3"), 0, REG_DWORD, (BYTE*)&dwVal_ffdshow_wmv, sizeof(dwVal_ffdshow_wmv));  
		if(lRet == ERROR_SUCCESS)
		{
			__DEBUG__("wmv3 set to using", dwVal_ffdshow_wmv ? "libavcodec" : "WMVideo Decoder DMO");
		}
		else
		{
			__DEBUG__("Fail to set wmv3", _NULL);
		}//if

		lRet = RegSetValueEx(hKey, _T("xvid"), 0, REG_DWORD, (BYTE*)&dwVal_ffdshow_mpeg4, sizeof(dwVal_ffdshow_mpeg4));  
		if(lRet == ERROR_SUCCESS)
		{
			__DEBUG__("xvid set to using", dwVal_ffdshow_mpeg4 ? "libavcodec" : "System Default Codec");
		}
		else
		{
			__DEBUG__("Fail to set xvid", _NULL);
		}//if

		lRet = RegSetValueEx(hKey, _T("div3"), 0, REG_DWORD, (BYTE*)&dwVal_ffdshow_mpeg4, sizeof(dwVal_ffdshow_mpeg4));  
		if(lRet == ERROR_SUCCESS)
		{
			__DEBUG__("div3 set to using", dwVal_ffdshow_mpeg4 ? "libavcodec" : "System Default Codec");
		}
		else
		{
			__DEBUG__("Fail to set div3", _NULL);
		}//if

		lRet = RegSetValueEx(hKey, _T("dx50"), 0, REG_DWORD, (BYTE*)&dwVal_ffdshow_mpeg4, sizeof(dwVal_ffdshow_mpeg4));  
		if(lRet == ERROR_SUCCESS)
		{
			__DEBUG__("dx50 set to using", dwVal_ffdshow_mpeg4 ? "libavcodec" : "System Default Codec");
		}
		else
		{
			__DEBUG__("Fail to set dx50", _NULL);
		}//if

		lRet = RegSetValueEx(hKey, _T("h264"), 0, REG_DWORD, (BYTE*)&dwVal_ffdshow_mpeg4, sizeof(dwVal_ffdshow_mpeg4));  
		if(lRet == ERROR_SUCCESS)
		{
			__DEBUG__("h264 set to using", dwVal_ffdshow_mpeg4 ? "libavcodec" : "System Default Codec");
		}
		else
		{
			__DEBUG__("Fail to set h264", _NULL);
		}//if
		RegCloseKey(hKey);
	}
	else
	{
		__DEBUG__("Fail to open registry key", strRegKey);
	}//if
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 패키지 파일안의 모든 wizard xml을 취합하여 하나의 xml로 만들어 반환한다. \n
/// @return <형: LPCTSTR> \n
///			<값: 병합된 xml  데이터> \n
/////////////////////////////////////////////////////////////////////////////////
LPCSTR CBRW2App::GetWizardXML()
{
	static CString strWizardXml = "";

	if(strWizardXml.GetLength() == 0)
	{
		CString strXml;
		CProfileManager mngProfile;
		CString strContentsList = MNG_PROFILE_READ("host", "contentslist");

		strContentsList.Trim();
		if(strContentsList.GetLength() == 0)
		{
			return strWizardXml;
		}//if

		strXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><wizard>";
		//strXml = "<?xml version=\"1.0\"?><wizard>";

		CString strTok, strNode;
		int nPos = 0;
		strTok = strContentsList.Tokenize(",", nPos);
		while(strTok != "")
		{
			strTok.Trim();
			strNode = MNG_PROFILE_READ(strTok, "wizardXML");
			strNode.Replace("<?xml version=\"1.0\"?>", "");
			strXml += strNode;

			strTok = strContentsList.Tokenize(",", nPos);		
		}//while

		strXml += "</wizard>";
/*
		//UTF-8로 인코딩하여 넘겨준다.
		string strUTF8;
		CEncoding*	pEncoder = CEncoding::getInstance();
		if(!pEncoder->AnsiToUTF8(strXml, strUTF8))
		{
			__DEBUG__("Fail to incoding to UTF-8", _NULL);
			strWizardXml = "";
		}
		else
		{
			//////////////////////////////////////////////////
			string strAnsi;
			pEncoder->UTF8ToAnsi(strUTF8.c_str(), strAnsi);
			//////////////////////////////////////////////////
			strWizardXml = strUTF8.c_str();
		}//if
*/
		strWizardXml = strXml;
	}//if

	return strWizardXml;
}

void CBRW2App::DisablePPTAutoRecovery()
{
	//[HKEY_CURRENT_USER\Software\Microsoft\Office\11.0\PowerPoint\Options]		// Office 2003
	//[HKEY_CURRENT_USER\Software\Microsoft\Office\12.0\PowerPoint\Options]		// Office 2007
	//[HKEY_CURRENT_USER\Software\Microsoft\Office\14.0\PowerPoint\Options]		// Office 2010
	//[HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\PowerPoint\Options]		// Office 2013
	//"SaveAutoRecoveryInfo"=dword:00000000

	static char* str_ver[] = {
		"11.0",
		"12.0",
		"14.0",
		"15.0",
		"", // end mark
	};

	HKEY hKey;
	DWORD dwSize = sizeof(DWORD);
	DWORD dwType = REG_DWORD;
	DWORD dwValue = 0;

	for(int i=0; strlen(str_ver[i])>0; i++)
	{
		CString str_subkey;
		str_subkey.Format("Software\\Microsoft\\Office\\%s\\PowerPoint\\Options", str_ver[i]);

		LONG lRet = ::RegOpenKeyEx(HKEY_CURRENT_USER, str_subkey, 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey);
		if(lRet != ERROR_SUCCESS)
		{
			// not exist
			__WARNING__("Not Exist Powerpoint registry", str_ver[i]);
			continue;
		}

		lRet = RegSetValueEx(hKey, "SaveAutoRecoveryInfo", NULL, dwType, (LPBYTE)&dwValue, dwSize);
		if(lRet != ERROR_SUCCESS)
		{
			// fail
			str_subkey.Insert(0, "HKCU\\");
			str_subkey += "\\SaveAutoRecoveryInfo";
			__WARNING__("Fail to set Powerpoint-registry", str_subkey);
		}
		else
		{
			// success
			str_subkey.Insert(0, "HKCU\\");
			str_subkey += "\\SaveAutoRecoveryInfo";
			__DEBUG__("Set Powerpoint-registry", str_subkey);
		}
		RegCloseKey(hKey);
	}
}

void CBRW2App::CheckIEVersion()
{
	HKEY hKey;
	DWORD dwSize = sizeof(DWORD);
	DWORD dwType = REG_DWORD;
	DWORD dwValue = 0;

	CString str_ver = GetINIValue("UBCBrowser.ini", "ROOT", "IE_VERSION");
	if( stricmp(str_ver, "STANDARD") == 0 || 
		stricmp(str_ver, "MANUAL") == 0 )
	{
		bool is_standard = (stricmp(str_ver, "STANDARD")==0);

		int ie_ver = atoi(_getIEVersion());
		switch( ie_ver )
		{
		case 7:
			dwValue = 7000;
			break;
		default:
			{
				__WARNING__("Fail to IE-Version !!! Set to IE8-Standard-Mode", _NULL);
			}
		case 8:
			dwValue = is_standard ? 8000 : 8888;
			break;
		case 9:
			dwValue = is_standard ? 9000 : 9999;
			break;
		case 10:
			dwValue = is_standard ? 10000 : 10001;
			break;
		case 11:
			dwValue = is_standard ? 11000 : 11001;
			break;
		}
	}
	else
	{
		dwValue = atoi(str_ver);
	}

	static char* str_exe[] = {
		"UTV_brwClient2.exe",
		"UTV_brwClient2_UBC1.exe",
		"", // end mark
	};

	CString str_subkey = 
		"SOFTWARE\\Microsoft\\Internet Explorer\\MAIN\\FeatureControl\\FEATURE_BROWSER_EMULATION";

	for(int i=0; strlen(str_exe[i])>0; i++)
	{
		LONG lRet = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, str_subkey, 0, KEY_ALL_ACCESS | KEY_WOW64_32KEY, &hKey);
		if(lRet != ERROR_SUCCESS)
		{
			// not exist
			__WARNING__("Not Exist IE-Compatible registry", str_subkey);
			return;
		}

		lRet = RegSetValueEx(hKey, str_exe[i], NULL, dwType, (LPBYTE)&dwValue, dwSize);
		if(lRet != ERROR_SUCCESS)
		{
			// fail
			CString msg;
			msg.Format("HKLM\\%s\\%s : %d(0x%08X)", str_subkey, str_exe[i], dwValue, dwValue);
			__WARNING__("Fail to set IE-Compatible-registry", msg);
		}
		else
		{
			// success
			CString msg;
			msg.Format("HKLM\\%s\\%s : %d(0x%08X)", str_subkey, str_exe[i], dwValue, dwValue);
			__DEBUG__("Set IE-Compatible-registry", msg);
		}
		RegCloseKey(hKey);
	}
}

CString CBRW2App::_getIEVersion()
{
	CString str_ie_path = 
		"C:\\Program Files\\Internet Explorer\\iexplore.exe";

	// 파일로부터 버전정보데이터의 크기가 얼마인지를 구합니다.
	DWORD info_size = GetFileVersionInfoSize(str_ie_path, 0);
	if( info_size==0 ) return "";

	// 버전정보를 담을 버퍼 할당
	char* buf = new char[info_size];
	if( buf==NULL ) return "";

	// 버전정보데이터를 가져옵니다.
	if( GetFileVersionInfo(str_ie_path, 0, info_size, buf)==0 )
	{
		delete[]buf;
		return "";
	}

	VS_FIXEDFILEINFO* pFineInfo = NULL;
	UINT bufLen = 0;
	// buffer로 부터 VS_FIXEDFILEINFO 정보를 가져옵니다.
	if( VerQueryValue(buf, "\\", (LPVOID*)&pFineInfo, &bufLen)==0 )
	{
		delete[]buf;
		return "";
	}

	WORD majorVer, minorVer, buildNum, revisionNum;
	majorVer = HIWORD(pFineInfo->dwFileVersionMS);
	minorVer = LOWORD(pFineInfo->dwFileVersionMS);
	buildNum = HIWORD(pFineInfo->dwFileVersionLS);
	revisionNum = LOWORD(pFineInfo->dwFileVersionLS);

	// 파일버전 출력
	//strVersion.Format(_T("%02d.%02d.%03d.%03d"), majorVer, minorVer, buildNum, revisionNum);
	CString str_ver;
	str_ver.Format(_T("%d"), majorVer);

	delete[]buf;

	return str_ver;
}
