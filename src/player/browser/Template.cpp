// Template.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "Host.h"
#include "Template.h"


#define		CURRENT_PLAYINFO_FILENAME		"C:\\SQISoft\\UTV1.0\\execute\\flash\\xml\\current_playinfo.xml"

// CTemplate

#define		FRAME_CHECK_ID			2000	// id
#define		FRAME_CHECK_TIME		100	// time
//#define		FRAME_CHECK_TIME		300	// time

#define		TEMPLATE_SHOW_ID		2001	// id
#define		TEMPLATE_SHOW_TIME		10	// time

#define		TEMPLATE_HIDE_ID		2002	// id
#define		TEMPLATE_HIDE_TIME		50	// time

UINT	CTemplate::m_nTemplateID = 0xfffe;

CTemplate*	CTemplate::m_pPhoneTemplate = NULL;
CCriticalSection CTemplate::m_csTemplatePhone;

bool CTemplate::m_bTopmost = true;

CString CTemplate::m_strLastPlayTemplateShortCut = "";


IMPLEMENT_DYNAMIC(CTemplate, CWnd)


CTemplate* CTemplate::GetTemplateObject(CWnd* pParent, LPCSTR lpszID, CString& strErrorMessage)
{
	__DEBUG__("\tCreate Template", lpszID);

	CTemplate* tmpl = new CTemplate(pParent, lpszID);

	if(tmpl != NULL)
	{
		BOOL ret_value = tmpl->Create(NULL, "Template", WS_CHILD, CRect(0,0,100,100), pParent, m_nTemplateID--);
		if(ret_value)
		{
			return tmpl;
		}
		else
		{
			strErrorMessage = "Fail to create Template !!!";
			delete tmpl;
			return NULL;
		}
	}

	strErrorMessage = "Fail to create Template !!!";

	return NULL;
}


CTemplate::CTemplate(CWnd* pParent, LPCSTR lpszID)
:	m_pParentWnd (pParent)
,	m_strErrorMessage ("")
,	m_strLoadID (lpszID)
,	m_nPlayCount(0)
,	m_pclsPalyRule(NULL)
,	m_nShortcutKey (0)
,	m_templateId ("00")
,	m_width (640)
,	m_height (480)
,	m_bgColor ("#000000")
,	m_nSysShortcutKey(0)
,	m_bPlay(false)
{	
	m_templateId = MNG_PROFILE_READ(lpszID, "templateId");
	m_width = atoi(MNG_PROFILE_READ(lpszID, "width"));
	m_height = atoi(MNG_PROFILE_READ(lpszID, "height"));
	m_bgColor = MNG_PROFILE_READ(lpszID, "bgColor");
	m_description = MNG_PROFILE_READ(lpszID, "description");
	m_shortCut = MNG_PROFILE_READ(lpszID, "shortCut");

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}//if

	CString strPhoneTemplateId, stPhoneFrameId;
	int nPortNo;
	::GetPhoneConfig(strPhoneTemplateId, stPhoneFrameId, nPortNo);

	if(strPhoneTemplateId == m_templateId.c_str())
	{
		m_pPhoneTemplate = this;
		m_bPhoneTemplate = true;
	}
	else
		m_bPhoneTemplate = false;

	MakeShortcutKey();

	//
	CString str_short_cut = m_shortCut.c_str();
	str_short_cut.Replace("+", "_"); // '+'를 언더바(_)로 치환
	m_strSyncCommand = GetINIValue("UBCBrowser.ini", "SyncProcess", str_short_cut);
	m_strSyncCommand.Trim(" \t\r\n");

	m_nTemplateIndex = -1;
}

CTemplate::~CTemplate()
{
	__DEBUG__("\tDestroy Template", m_templateId.c_str());

	if(CTemplate::m_pPhoneTemplate == this)
		CTemplate::m_pPhoneTemplate = NULL;
}


BEGIN_MESSAGE_MAP(CTemplate, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_DEBUG_STRING, OnDebugString)
	ON_MESSAGE(WM_DEBUG_GRID, OnDebugGrid)
	ON_MESSAGE(WM_ERROR_MESSAGE, OnErrorMessage)
	ON_MESSAGE(ID_PLAY_NEXT_SCHEDULE, OnPlayNextSchedule)
	ON_MESSAGE(WM_HIDE_CONTENTS, OnHideContents)
	ON_MESSAGE(WM_TOGGLE_SOUND_VOLUME_MUTE, OnSoundVolumeMute)
END_MESSAGE_MAP()


// CTemplate 메시지 처리기입니다.
int CTemplate::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_TEMPLATE_BEGIN__(m_templateId.c_str())

	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	CLogDialog::getInstance()->Add(m_pParentWnd, this, m_templateId.c_str(), 2);

	int nX, nY, nCx, nCy;
	((CHost*)m_pParentWnd)->GetAppPosition(nX, nY, nCx, nCy);

	double xscale=0.0f, yscale=0.0f;
	CHost::GetScale(xscale, yscale);
	if(xscale == 0.0f && yscale == 0.0f)
	{
		// Modified by 정운형 2009-04-16 오후 8:01
		// 변경내역 :  듀얼모니터를 지원하기 위하여 수정
		/*
		int scr_width = GetSystemMetrics(SM_CXSCREEN);
		int scr_height = GetSystemMetrics(SM_CYSCREEN);

		__DEBUG__("SM_CXSCREEN", scr_width);
		__DEBUG__("SM_CYSCREEN", scr_height);
		*/
		int scr_width	= nCx;
		int scr_height	= nCy;
		// Modified by 정운형 2009-04-16 오후 8:01
		// 변경내역 :  듀얼모니터를 지원하기 위하여 수정

		if(ciArgParser::getInstance()->isSet("+aspectratio"))
		{
			int width, height;
			width = scr_width;
			height = m_width * scr_height / scr_width;
			if(height < m_height)
			{
				height = scr_height;
				width = m_height * scr_width / scr_height;

				xscale = ((double)scr_height) / ((double)m_height);
			}
			else
			{
				xscale = ((double)scr_width) / ((double)m_width);
			}
			yscale = xscale;
//			CHost::SetScale(xscale, yscale);	// 템플릿 크기가 다른경우 개별설정 위해 제거
		}
		else
		{
			xscale = ((double)scr_width) / ((double)m_width);
			yscale = ((double)scr_height) / ((double)m_height);
//			CHost::SetScale(xscale, yscale);	// 템플릿 크기가 다른경우 개별설정 위해 제거
		}
	}
	m_fXScale = xscale;
	m_fYScale = yscale;

	//
	CHost::GetTemplateXY(m_nTemplateX, m_nTemplateY);
	if(m_nTemplateX == INT_MAX)
	{
		// Modified by 정운형 2009-04-16 오후 8:01
		// 변경내역 :  듀얼모니터를 지원하기 위하여 수정
		//int scr_width = GetSystemMetrics(SM_CXSCREEN);
		int scr_width = nCx;
		// Modified by 정운형 2009-04-16 오후 8:01
		// 변경내역 :  듀얼모니터를 지원하기 위하여 수정

		m_nTemplateX = (scr_width/2) - ( (m_width * xscale) / 2.0f );

		if(m_nTemplateX < 0) m_nTemplateX = 0;
	}
	if(m_nTemplateY == INT_MAX)
	{
		// Modified by 정운형 2009-04-16 오후 8:01
		// 변경내역 :  듀얼모니터를 지원하기 위하여 수정
		//int scr_height = GetSystemMetrics(SM_CYSCREEN);
		int scr_height = nCy;
		// Modified by 정운형 2009-04-16 오후 8:01
		// 변경내역 :  듀얼모니터를 지원하기 위하여 수정

		m_nTemplateY = (scr_height/2) - ( (m_height * yscale) / 2.0f );

		if(m_nTemplateY < 0) m_nTemplateY = 0;
	}
	//CHost::SetTemplateXY(x, y);	// 템플릿 크기가 다른경우 개별설정 위해 제거
	MoveWindow(m_nTemplateX, m_nTemplateY, m_width*xscale, m_height*yscale);

	CRect rect;
	GetClientRect(rect);

	LoadFrame();

	__DEBUG_TEMPLATE_END__(m_templateId.c_str())

	return 0;
}

void CTemplate::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	CRect rect;
	GetClientRect(rect);

	dc.FillSolidRect(rect, m_rgbBgColor);
}

void CTemplate::OnDestroy()
{
	__DEBUG_TEMPLATE_BEGIN__(m_templateId.c_str())

	KillTimer(TEMPLATE_SHOW_ID);
	KillTimer(TEMPLATE_HIDE_ID);
	KillTimer(FRAME_CHECK_ID);

	CFrame* pFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame)
		{
			pFrame->DestroyWindow();
			delete pFrame;
		}//if
	}//for
	m_aryFrame.RemoveAll();

	CWnd::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_TEMPLATE_END__(m_templateId.c_str())
}

bool CTemplate::OpenFile()
{
	__DEBUG_TEMPLATE_BEGIN__(m_templateId.c_str())

	CFrame* pTmpFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pTmpFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pTmpFrame)
		{
			pTmpFrame->OpenFile();
		}//if
	}//for

	// <!-- 골프장전용 (2016.08.08)
	if( ::GetPreOpenSyncProcess() )
	{
		openSyncProcess(true);

		// 동기화프로세스를 뒷면으로 전환하기 위해 대기후 처리
		//HWND hwnd = NULL;
		//for(int i=0; i<600 && hwnd==NULL; i++) // wait 10-sec
		//{
		//	::Sleep(100);
		//	hwnd = scratchUtil::getInstance()->getWHandle((LPCSTR)m_strSyncProcess);	
		//}

		//scratchUtil::getInstance()->SetForegroundWindowForce(::AfxGetMainWnd()->GetSafeHwnd());
	}
	// -->

	__DEBUG_TEMPLATE_END__(m_templateId.c_str())

	return true;
}

bool CTemplate::IsBetween(bool bDefaultSchedule)
{
	bool between = false;

	//Grade가 1인 메인프레임만 기준으로 판단한다.
	CFrame* pFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame && pFrame->GetGrade() ==1)
		{
			between |= pFrame->IsBetween(bDefaultSchedule);
		}//if
	}//for

	return between;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Tmplate play rule을 설정하여 play하도록 하는 함수 \n
/// @param (CTemplatePlay*) pclsPlayRule : (in) Template play rule을 지정하는 클래스
/// @return <형: void> \n
/////////////////////////////////////////////////////////////////////////////////
void CTemplate::Play(CTemplatePlay* pclsPlayRule)
{
	m_pclsPalyRule = pclsPlayRule;
	if(m_pclsPalyRule)
	{
		m_nPlayCount = m_pclsPalyRule->m_unPlayCount;
	}//if

	__DEBUG__("\tPlay Template", m_templateId.c_str());

	CLogDialog::getInstance()->Play(this);

	//HD Frame이 있다면 dummy 파일을 닫고 overlay를 사용할 수 있도록한다.
	if(GetVidoeOpenType() != E_OPEN_INIT)
	{
		if(HasHDFrame())
		{
			((CHost*)m_pParentWnd)->CloseDummyVideo();
		}
		else if(GetVidoeRenderType() != E_DUMMY_OVERLAY)
		{
			((CHost*)m_pParentWnd)->OpenDummyVideo();
		}//if
	}//if

	// <!-- 골프장전용 (2016.08.08)
	openSyncProcess();
	sendTemplateShortcut();
	// -->

	KillTimer(TEMPLATE_HIDE_ID);
	SetTimer(TEMPLATE_SHOW_ID, TEMPLATE_SHOW_TIME, NULL);

	m_bPlay = true;

	((CHost*)m_pParentWnd)->m_timeDialog.SetTemplateId(m_templateId.c_str());
	if(m_aryFrame.GetCount() > 0)
	{
		OnTimer(FRAME_CHECK_ID); // 즉시 play
		SetTimer(FRAME_CHECK_ID, FRAME_CHECK_TIME, NULL);
	}//if
	if(::GetScheduleResuming())
		CHost::PushBackToShowWindow(GetSafeHwnd());

	// write current-play-info to xml-file
	WriteCurrentPlayInfo();
}	

void CTemplate::Stop()
{
	m_bPlay = false;
	KillTimer(FRAME_CHECK_ID);

	CLogDialog::getInstance()->Stop(this);
/*
	//HD Frame이 있다면 dummy 파일을 닫고 overlay를 사용할 수 있도록한다.
	if(GetVidoeOpenType() != E_OPEN_INIT)
	{
		if(HasHDFrame())
		{
			((CHost*)m_pParentWnd)->StopDummyVideo();
		}
		else
		{
			((CHost*)m_pParentWnd)->PlayDummyVideo();
		}//if
	}//if
*/
	KillTimer(TEMPLATE_SHOW_ID);
	SetTimer(TEMPLATE_HIDE_ID, TEMPLATE_HIDE_TIME, NULL);
	if(::GetScheduleResuming())
		CHost::PushFrontToHideWindow(GetSafeHwnd());

	CFrame* pFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame)
		{
			pFrame->Stop();
		}//if
	}//for
	__DEBUG__("\tStop Template end", m_templateId.c_str());
}

// <!-- 골프장전용 (2016.08.08)
ciULong CTemplate::createProcess(const char *exe, const char* dir, WORD wShowWindow/*=SW_MINIMIZE*/)
{
	//ciDEBUG2(5, ("_createProcess(%s, %s)", exe, dir) );

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	::ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = wShowWindow;
	//si.lpDesktop = "Winsta0\\Default";

	//if(!dir || !strlen(dir)){
	//	dir = UBC_HOME;
	//}

	BOOL bRun = ::CreateProcess(NULL, 
								(LPSTR)exe, 
								NULL, 
								NULL, 
								FALSE, 
								NORMAL_PRIORITY_CLASS, 
								NULL, 
								dir, 
								&si, 
								&pi
							);

	CloseHandle( pi.hThread );
	CloseHandle( pi.hProcess );

	if (bRun)
	{
		//myDEBUG("createProcess : pid(%d)\n", pi.dwProcessId);
		return pi.dwProcessId;
	}
	//myDEBUG("createProcess : fail to CreateProcess\n");

	CString str_exe = exe;
	CString str_dir = dir;
	if( str_exe.Find(" (x86)") > 0 || str_dir.Find(" (x86)") > 0)
	{
		str_exe.Replace(" (x86)", "");
		str_dir.Replace(" (x86)", "");
		return createProcess(str_exe, str_dir, wShowWindow);
	}
	return 0;
}

void CTemplate::openSyncProcess(bool pre_run/*=false*/)
{
	// 템플릿 단축기에 할단된 동기화프로세스값 읽기
	//CString str_short_cut = m_shortCut.c_str();
	//str_short_cut.Replace("+", "_"); // '+'를 언더바(_)로 치환
	//CString str_command = GetINIValue("UBCBrowser.ini", "SyncProcess", str_short_cut);
	//str_command.Trim(" \t\r\n");
	if( m_strSyncCommand.GetLength()==0 || 
		stricmp(m_strSyncCommand,"null")==0 ||
		stricmp(m_strSyncCommand,"resume")==0 ) return;

	__DEBUG__("ShortcutKey", m_shortCut.c_str());
	__DEBUG__("SyncProcess", (LPCSTR)m_strSyncCommand);
	int pos = 0;
	CString str_fullpath = "";
	if( m_strSyncCommand.Find("|") > 0 ) // 프로세스 파라미터는 '|'로 구분 !!!
	{
		str_fullpath = m_strSyncCommand.Tokenize("|", pos);
		m_strSyncCommand.Replace("|", " ");
	}
	else
		str_fullpath = m_strSyncCommand;

	char drv[MAX_PATH], path[MAX_PATH], fn[MAX_PATH], ext[MAX_PATH];
	_splitpath(str_fullpath, drv, path, fn, ext);

	m_strSyncProcess.Format("%s%s", fn, ext); // 파일명만. 프로세스 검사용

	unsigned long pid = scratchUtil::getInstance()->getPid(m_strSyncProcess);
	if( pid>0 )
	{
		showSyncProcess();
		return;
	}

	// 동기화프로세스 실행 안되어있음
	__DEBUG__("No running SyncProcess", (LPCSTR)m_strSyncProcess);

	CString str_path;
	str_path.Format("%s%s", drv, path);

	__DEBUG__("CreateProcess(cmd)", (LPCSTR)m_strSyncCommand);
	__DEBUG__("CreateProcess(cmd)", (LPCSTR)str_path);

	CString str_open_type = ::GetSyncProcessOpenType();
	WORD show_window = SW_NORMAL;
	if( str_open_type == "MINIMIZE" || pre_run) show_window = SW_MINIMIZE;
	else if( str_open_type == "MAXIMIZE" ) show_window = SW_MAXIMIZE;
	//else if( str_open_type == "NORMAL" ) show_window = SW_NORMAL;

	if( createProcess(m_strSyncCommand, str_path, show_window) == 0 )
	{
		// 실행 실패
		__WARNING__("Fail to run !!!", (LPCSTR)m_strSyncCommand);
	}
}

void CTemplate::sendTemplateShortcut()
{
	int flag = ::GetSendSyncProcessShortcut();
	if( flag==0 || m_shortCut.length()==0 ) return;

	CString str_send_shortcut = m_shortCut.c_str();

	if(stricmp(m_strSyncCommand,"resume")==0)
	{
		m_strLastPlayTemplateShortCut = m_shortCut.c_str();
		str_send_shortcut = "CTRL+H";
	}

	HWND hwnd = scratchUtil::getInstance()->getWHandle("UBCFirmwareView.exe");
	if(hwnd==NULL) return;

	COPYDATASTRUCT appInfo;
	appInfo.dwData = UBC_WM_TEMPLATE_SHORTCUT;  // 1000
	appInfo.lpData = (LPVOID)(LPCSTR)str_send_shortcut;
	appInfo.cbData = str_send_shortcut.GetLength()+1;
	__DEBUG__("UBC_WM_TEMPLATE_SHORTCUT", str_send_shortcut);
	::SendMessage(hwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
}

// 동기화 프로세스를 최앞단으로 띄움
void CTemplate::showSyncProcess()
{
	if( m_strSyncCommand.GetLength()==0 ) return;

	// <!-- 오직 골프장전용 (2016.08.10)
	unsigned long sync_pid = scratchUtil::getInstance()->getPid("SetupKioskPrint", true);
	if(sync_pid !=0 )
	{
		if(m_bTopmost == true)
		{
			m_bTopmost = false;
			CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
			pclsHost->SetTopMost(m_bTopmost);
			__DEBUG__("SetTopMost", (int)m_bTopmost);
		}
	}
	else //if(sync_pid == 0)
	{
		if(m_bTopmost == false)
		{
			m_bTopmost = true;
			CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
			pclsHost->SetTopMost(m_bTopmost);
			__DEBUG__("SetTopMost", (int)m_bTopmost);
		}
	}
	// -->


	// 최상단 프로세스의 pid
	HWND fore_hwnd = ::GetForegroundWindow();
	DWORD fore_pid = 0;
	GetWindowThreadProcessId(fore_hwnd, &fore_pid);

	// 동기화프로세스 pid
	if(sync_pid == 0)
	{
		if(m_strSyncProcess.GetLength() == 0)
		{
			//__DEBUG__("fore_pid", (int)fore_pid);
			//__DEBUG__("getpid", getpid());
			if( (stricmp(m_strSyncCommand,"null")==0 || stricmp(m_strSyncCommand,"resume")==0 ) && 
				fore_pid != getpid() )
			{
				__DEBUG__("SetForegroundWindowForce", _NULL);
				scratchUtil::getInstance()->SetForegroundWindowForce(::AfxGetMainWnd()->GetSafeHwnd());
				return;
			}
			return;
		}
		sync_pid = scratchUtil::getInstance()->getPid(m_strSyncProcess);
	}

	if( sync_pid != fore_pid )
	{
		// 동기화프로세스가 최상위 아님
		HWND hwnd = scratchUtil::getInstance()->getWHandle(m_strSyncProcess);
		if(hwnd==NULL) hwnd = scratchUtil::getInstance()->getWHandle("SetupKioskPrint", true);
		if(hwnd != NULL )
		{
			//__DEBUG__("AfxGetMainWnd", (UINT)::AfxGetMainWnd());
			//__DEBUG__("fore_hwnd", (UINT)fore_hwnd);

			//__DEBUG__("fore_pid", (UINT)fore_pid);
			//__DEBUG__("sync_pid", (UINT)sync_pid);
			//__DEBUG__("hwnd", (UINT)hwnd);

			CString str_open_type = ::GetSyncProcessOpenType();
			WORD show_window = SW_NORMAL;
			if( str_open_type == "MINIMIZE" ) return;
			else if( str_open_type == "MAXIMIZE" ) show_window = SW_MAXIMIZE;
			//else if( str_open_type == "NORMAL" ) show_window = SW_NORMAL;

			::ShowWindow(hwnd, show_window);
			scratchUtil::getInstance()->SetForegroundWindowForce(hwnd);
		}
	}
}

void CTemplate::hideSyncProcess()
{
	if(m_strSyncCommand.GetLength()==0 || 
		stricmp(m_strSyncCommand,"null")==0 ||
		stricmp(m_strSyncCommand,"resume")==0 ) return;

	if(m_strSyncProcess.GetLength() == 0) return;

	//if(m_bTopmost==false)
	//{
	//	m_bTopmost = true;
	//	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	//	pclsHost->SetTopMost(true);
	//	__DEBUG__("SetTopMost", 1);
	//}
	scratchUtil::getInstance()->SetForegroundWindowForce(::AfxGetMainWnd()->GetSafeHwnd());

	CString str_close_type = ::GetSyncProcessCloseType();
	str_close_type.MakeUpper();

	if( str_close_type == "NOTHING" )
	{
		m_strSyncProcess = "";
		return;
	}

	HWND hwnd = scratchUtil::getInstance()->getWHandle(m_strSyncProcess);
	if(hwnd != NULL)
	{
		if( str_close_type == "EXIT" )
		{
			unsigned long pid = scratchUtil::getInstance()->getPid(m_strSyncProcess);
			if(pid > 0) scratchUtil::getInstance()->killProcess(pid);
		}
		else //if( str_close_type == "MINIMIZE" )
		{
			::ShowWindow(hwnd, SW_MINIMIZE);
		}
	}

	m_strSyncProcess = "";
}

void CTemplate::killSyncProcess()
{
	CString str_process_exit_type = ::GetSyncProcessExitType();
	if(m_strSyncCommand.GetLength()>0 &&
		stricmp(m_strSyncCommand,"null")!=0 &&
		stricmp(m_strSyncCommand,"resume")!=0 &&
		stricmp(str_process_exit_type,"NOTHING")!=0 )
	{
		int pos = 0;
		CString str_fullpath = "";
		if( m_strSyncCommand.Find("|") > 0 ) // 프로세스 파라미터는 '|'로 구분 !!!
		{
			str_fullpath = m_strSyncCommand.Tokenize("|", pos);
			m_strSyncCommand.Replace("|", " ");
		}
		else
			str_fullpath = m_strSyncCommand;

		char drv[MAX_PATH], path[MAX_PATH], fn[MAX_PATH], ext[MAX_PATH];
		_splitpath(str_fullpath, drv, path, fn, ext);

		m_strSyncProcess.Format("%s%s", fn, ext); // 파일명만. 프로세스 검사용

		if(stricmp(str_process_exit_type,"EXIT")==0)
		{
			if(m_strSyncProcess.GetLength() > 0)
			{
				__DEBUG__("\tSyncProcess", (LPCSTR)m_strSyncProcess);
				unsigned long pid = scratchUtil::getInstance()->getPid(m_strSyncProcess);
				if(pid <= 0)
				{
					__DEBUG__("\tNo SyncProcess in tasklist", (LPCSTR)m_strSyncProcess);
				}
				else
				{
					__DEBUG__("\tKillProcess", (LPCSTR)m_strSyncProcess);
					scratchUtil::getInstance()->killProcess(pid);
				}
			}
		}
		else if(stricmp(str_process_exit_type,"MINIMIZE")==0)
		{
			HWND hwnd = scratchUtil::getInstance()->getWHandle(m_strSyncProcess);
			if(hwnd != NULL ) ::ShowWindow(hwnd, SW_MINIMIZE);
		}
	}
}

bool CTemplate::IsRemainPlayingDefaultSchedule()
{
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		CFrame* pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame && (pFrame->GetGrade() == 1))
		{
			return pFrame->IsRemainPlayingDefaultSchedule();
			break;
		}//if
	}//for
	return false;
}

int CTemplate::GetTemplateIndex()
{
	if(m_nTemplateIndex < 0)
	{
		m_nTemplateIndex = ((CHost*)m_pParentWnd)->GetTemplateIndex(m_templateId.c_str());
	}

	return m_nTemplateIndex;
}
// -->

void CTemplate::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case TEMPLATE_SHOW_ID:
		KillTimer(TEMPLATE_SHOW_ID);
		if(!::GetScheduleResuming())
			ShowWindow(SW_SHOW);

		// <!-- 골프장전용 (2016.08.08)
		showSyncProcess();
		// -->
		break;

	case TEMPLATE_HIDE_ID:
		KillTimer(TEMPLATE_HIDE_ID);
		if(!::GetScheduleResuming())
			ShowWindow(SW_HIDE);

		// <!-- 골프장전용 (2016.08.08)
		hideSyncProcess();
		// -->
		break;

	case FRAME_CHECK_ID:
		{
			if(!m_bPlay)
			{
				__DEBUG__("Template - m_bPlay", m_bPlay);
				KillTimer(FRAME_CHECK_ID);
			}
			else
			{
				//KillTimer(FRAME_CHECK_ID);
				//__DEBUG__("FrameCheckId", m_templateId.c_str());
				CheckFrame();
				//SetTimer(FRAME_CHECK_ID, FRAME_CHECK_TIME, NULL);

				// <!-- 골프장전용 (2016.08.08)
				static unsigned int cnt = 0;
				if(cnt++ % 5 == 0)
					showSyncProcess();
				// -->
			}//if
		}
		break;
	}
}

CString	CTemplate::ToString()
{
	m_csTemplatePhone.Lock();

	CString str;
	str.Format(
		"templateId = %s\r\n"
		"width = %d\r\n"
		"height = %d\r\n"
		"bgColor = %s\r\n"
		"shortCut = %s\r\n"
		"\r\n"
		"m_bPhoneTemplate = %d\r\n"
		"m_PhoneTemplate = %s\r\n"
		,	m_templateId.c_str()
		,	m_width
		,	m_height
		,	m_bgColor.c_str()
		,	m_shortCut.c_str()

		,	m_bPhoneTemplate
		,	( m_pPhoneTemplate == NULL ? "NULL" : m_pPhoneTemplate->GetTemplateId() )

	);

	m_csTemplatePhone.Unlock();

	return str;
}

LRESULT CTemplate::OnDebugString(WPARAM wParam, LPARAM lParam)
{
	m_debugString = ToString();

	return (LRESULT)(LPCTSTR)m_debugString;
}

LRESULT CTemplate::OnDebugGrid(WPARAM wParam, LPARAM lParam)
{
	CPropertyGrid* grid = (CPropertyGrid*)wParam;

	grid->ResetContents();

	HSECTION hs = grid->AddSection("TEMPLATE Attributes");

	grid->AddStringItem(hs, "templateId", m_templateId.c_str());
	grid->AddIntegerItem(hs, "width", m_width);
	grid->AddIntegerItem(hs, "height", m_height);
	grid->AddStringItem(hs, "bgColor", m_bgColor.c_str());
	grid->AddStringItem(hs, "shortCut", m_shortCut.c_str());

	hs = grid->AddSection("member variables");

	grid->AddStringItem(hs, "ERROR MESSAGE", (LPCSTR)m_strErrorMessage);
	grid->AddBoolItem(hs, "m_bPhoneTemplate", m_bPhoneTemplate);
	grid->AddStringItem(hs, "m_PhoneTemplate", ( m_pPhoneTemplate == NULL ? "NULL" : m_pPhoneTemplate->GetTemplateId() ));
	grid->AddStringItem(hs, "m_fXScale", (m_fXScale == 0.0f ? "auto scale" : ::ToString(m_fXScale)) );
	grid->AddStringItem(hs, "m_fYScale", (m_fYScale == 0.0f ? "auto scale" : ::ToString(m_fYScale)) );
	grid->AddIntegerItem(hs, "m_nTemplateX", m_nTemplateX);
	grid->AddIntegerItem(hs, "m_nTemplateY", m_nTemplateY);
	grid->AddIntegerItem(hs, "m_nPlayCount", m_nPlayCount);

	// Modified by 정운형 2009-01-14 오후 1:56
	// 변경내역 :  Play rule 추가
	if(m_pclsPalyRule)
	{
		hs = grid->AddSection("template play rule");

		grid->AddBoolItem(hs, "Use play count", m_pclsPalyRule->m_bUsePlayCount);
		grid->AddIntegerItem(hs, "Playable count", m_pclsPalyRule->m_unPlayCount);
		CString str = "";
		for(int i=0; i<m_pclsPalyRule->m_aryPlayOrder.GetCount(); i++)
		{
			str += m_pclsPalyRule->m_aryPlayOrder.GetAt(i);
		}//for
		grid->AddStringItem(hs, "Play order", (LPCSTR)str);
	}//
	// Modified by 정운형 2009-01-14 오후 1:56
	// 변경내역 :  Play rule 추가

	return 0;
}

LRESULT CTemplate::OnErrorMessage(WPARAM wParam, LPARAM lParam)
{
	if(m_strErrorMessage.GetLength() > 0) m_strErrorMessage.Append(";");
	if(m_strErrorMessage.GetLength() > DEBUG_MSG_LENGTH) m_strErrorMessage.Empty();
	m_strErrorMessage.Append((LPCSTR)wParam);

	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	pclsHost->SendMessage(WM_ERROR_MESSAGE, wParam, lParam);

	return 0;
}

bool CTemplate::LoadFrame()
{
	CString frame_list = MNG_PROFILE_READ(m_strLoadID, "FrameList");

	CFrameArray	listPIPFrame;

	int pos = 0;
	CString token = frame_list.Tokenize(",", pos);
	while(token != "")
	{
		token.Trim();
		CString strErrorMessage;
		CFrame* frame = CFrame::GetFrameObject(this, token, strErrorMessage);
		if(frame != NULL)
		{
			if(frame->IsPIP())
			{
				__DEBUG__("\tPIP Frame", frame->GetFrameId());
				listPIPFrame.Add(frame);
			}
			else
			{
				m_aryFrame.Add(frame);

				if(m_bPhoneTemplate && frame->GetGrade() == 1)
				{
					CFrame::m_csFramePhone.Lock();

					__DEBUG__("\tPhone Frame", frame->GetFrameId());
					CFrame::m_pPhoneFrame = frame;

					CFrame::m_csFramePhone.Unlock();
				}//if
			}//if
		}
		else
		{
			OnErrorMessage((WPARAM)(LPCSTR)strErrorMessage, 0);
			strErrorMessage.Insert(0, "\t");
			__ERROR__(strErrorMessage, token);
		}//if

		token = frame_list.Tokenize(",", pos);
	}//while

	// create PIP frame
	int count = listPIPFrame.GetCount();
	if(count == 0)
	{
		return true;
	}//if

	for(int i=0; i<count; i++)
	{
		CFrame*	frame = listPIPFrame[i];
		CString frame_id = frame->GetFrameId();

		CString strErrorMessage;
		frame = CFrame::CreatePIPFrameObject(this, frame, strErrorMessage);
		if(frame)
		{
			__DEBUG__("\tCreate PIP Frame", frame->GetFrameId());

			CFrame* pTmpFrame = NULL;
			for(int i=0; i<m_aryFrame.GetCount(); i++)
			{
				pTmpFrame = (CFrame*)m_aryFrame.GetAt(i);
				//if(pTmpFrame && (pTmpFrame->FrameInFrame(frame)))
				//{
					__DEBUG__("\tParent Frame of this frame", pTmpFrame->GetFrameId());
					frame->SetPIPParentWnd(pTmpFrame);
					pTmpFrame->AddPIPFrame(frame);

					//jwh184 이곳에서 break 해야하지 않나?
				//	break;
				//}//if
			}//for
			m_aryFrame.Add(frame);
		}
		else
		{
			OnErrorMessage((WPARAM)(LPCSTR)strErrorMessage, 0);
			strErrorMessage.Insert(0, "\t");
			__ERROR__(strErrorMessage, frame_id);
		}//if
	}//for

	return true;
}


LRESULT CTemplate::OnPlayNextSchedule(WPARAM wParam, LPARAM lParam)
{
	CFrame* pFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame && (pFrame->GetGrade() == 1))
		{
			pFrame->PostMessage(ID_PLAY_NEXT_SCHEDULE);
			break;
		}//if
	}//for

	return 0;
}

bool CTemplate::GetFrameScheduleInfo(FRAME_SCHEDULE_INFO_LIST& infoList)
{
	CFrame* pFrame = NULL;
	int i;
	for(i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame && pFrame->IsCurrentPlay())
		{
			FRAME_SCHEDULE_INFO info;
			pFrame->GetFrameScheduleInfo(info);

			info.x		= (int)(((double)info.x)		* m_fXScale) + m_nTemplateX;
			info.y		= (int)(((double)info.y)		* m_fYScale) + m_nTemplateY;
			info.width	= (int)(((double)info.width)	* m_fXScale);
			info.height	= (int)(((double)info.height)	* m_fYScale);

			infoList.Add(info);
		}//if
	}//for

	for(i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame && pFrame->IsPIP() && pFrame->IsCurrentPlay())
		{
			FRAME_SCHEDULE_INFO info;
			pFrame->GetFrameScheduleInfo(info);

			int count = infoList.GetCount();
			for(int j=0; j<count; j++)
			{
				FRAME_SCHEDULE_INFO& parent_info = infoList.GetAt(j);

				if(info.pip_frameId == parent_info.frameId)
				{
					parent_info.pip_x		= (int)(((double)info.x)		* m_fXScale) -		parent_info.x + m_nTemplateX;
					parent_info.pip_y		= (int)(((double)info.y)		* m_fYScale) -		parent_info.y + m_nTemplateY;
					parent_info.pip_width	= (int)(((double)info.width)	* m_fXScale);
					parent_info.pip_height	= (int)(((double)info.height)	* m_fYScale);
				}//if
			}//for
		}//if
	}//for

	return true;
}


void CTemplate::ResetPlayCount()
{
	CFrame* pFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame)
		{
			pFrame->ResetPlayCount();
		}//if
	}//for
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Template play rule를 사용하여 play하는 frame을 변경하는 함수 \n
/// @return <형: void> \n
/////////////////////////////////////////////////////////////////////////////////
void CTemplate::CheckFrame()
{
	CFrame* pFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(!pFrame) continue;

		if(pFrame->IsBetween(false))
		{
			if(pFrame->IsCurrentPlay() == false)
			{
				if(pFrame->GetGrade() == 1)
				{
					//pclsFrame->SetPlayCount(m_nPlayCount);
					pFrame->Play(m_pclsPalyRule);
				}
				else
				{
					pFrame->Play(NULL);
				}//if				
			}//if
		}
		else if(pFrame->IsBetween(true))
		{
			if(pFrame->IsCurrentPlay() == false)
			{
				if(pFrame->GetGrade() == 1)
				{
					//pclsFrame->SetPlayCount(m_nPlayCount);
					pFrame->Play(m_pclsPalyRule);
				}
				else
				{
					pFrame->Play(NULL);
				}//if				
			}//if
		}
		else
		{
			pFrame->Stop();
		}//if
	}//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정되어있는 System shortcut(ALT, CTRL) key 값을 반환 \n
/// @return <형: UINT> \n
///			<VK_MENU: ALT key 값> \n
///			<VK_CONTROL: CTRL key 값> \n
///			<0: 설정된 값 없음> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CTemplate::GetSysShortcutKey()
{
	return m_nSysShortcutKey;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 문자열로 되어있는 shortcut key를 정의된 정수형의 key값으로 변경하는함수 \n
/// @param (CString) strString : (in) 문자열로 되어있는 shortcut key
/// @return <형: UINT> \n
///			<winuser.h>에 정의되어있는 정수형 key 값 \n
/////////////////////////////////////////////////////////////////////////////////
UINT CTemplate::MakeShortcutKeyStringToInt(CString strString)
{
	UINT unKeyValue = 0;
	
	if(strString.GetLength() == 1)
	{
		char cHot = strString.GetAt(0);
		unKeyValue = cHot;
	}
	else if(strString == "F1")		unKeyValue = VK_F1;
	else if(strString == "F2")		unKeyValue = VK_F2;
	else if(strString == "F3")		unKeyValue = VK_F3;
	else if(strString == "F4")		unKeyValue = VK_F4;
	else if(strString == "F5")		unKeyValue = VK_F5;
	else if(strString == "F6")		unKeyValue = VK_F6;
	else if(strString == "F7")		unKeyValue = VK_F7;
	else if(strString == "F8")		unKeyValue = VK_F8;
	else if(strString == "F9")		unKeyValue = VK_F9;
	else if(strString == "F10")		unKeyValue = VK_F10;
	else if(strString == "F11")		unKeyValue = VK_F11;
	else if(strString == "F12")		unKeyValue = VK_F12;
	//if

	return unKeyValue;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 문자열로 되어있는 shortcut key를 파싱하여 정수형의 shortcut key와 system key(ALT, CTRL)로 분리한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CTemplate::MakeShortcutKey()
{
	CString strHotKey = m_shortCut.c_str();
	strHotKey.Remove(_T(' '));
	strHotKey.MakeUpper();			//key down은 대문자 아스키 값으로만 들어온다
	
	if(strHotKey.Find(_T("ALT"), 0) != -1)			//ALT key
	{
		m_nSysShortcutKey = VK_MENU;
		strHotKey.Replace(_T("ALT"), _T(""));
		int nIdx = strHotKey.Find(_T("+"), 0);
		if(nIdx != -1)
		{
			strHotKey.Delete(0, nIdx+1);
		}//if
		m_nShortcutKey  = MakeShortcutKeyStringToInt(strHotKey);
	}
	else if(strHotKey.Find(_T("CTRL"), 0) != -1)	//CTRL key
	{
		m_nSysShortcutKey = VK_CONTROL;
		strHotKey.Replace(_T("CTRL"), _T(""));
		int nIdx = strHotKey.Find(_T("+"), 0);
		if(nIdx != -1)
		{
			strHotKey.Delete(0, nIdx+1);
		}//if
		m_nShortcutKey  = MakeShortcutKeyStringToInt(strHotKey);
	}
	else
	{
		m_nShortcutKey  = MakeShortcutKeyStringToInt(strHotKey);
	}//if	
}


LRESULT CTemplate::OnHideContents(WPARAM wParam, LPARAM lParam)
{
	KillTimer(TEMPLATE_SHOW_ID);
	//SetTimer(TEMPLATE_HIDE_ID, TEMPLATE_HIDE_TIME, NULL);
	ShowWindow(SW_HIDE);

	CFrame* pFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame && pFrame->GetSafeHwnd())
		{
			pFrame->PostMessage(WM_HIDE_CONTENTS, 0, 0);
		}//if
	}//for

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Grade가 1인 컨텐츠의 사운드를 mute(toggle)하는 메시지 처리 \n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CTemplate::OnSoundVolumeMute(WPARAM wParam, LPARAM lParam)
{
	CFrame* pFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pFrame && (pFrame->GetGrade() == 1))
		{
			pFrame->PostMessage(WM_TOGGLE_SOUND_VOLUME_MUTE, wParam, lParam);
			break;
		}//fi
	}//for

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Frame Array에 해당하는 ID의 Frame이 있는지여부 \n
/// @param (CString) strFrameID : (in) FrameID 문자열
/// @param (CFrame*&) pFrame : (in) Frame 객체
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CTemplate::HasFrame(CString strFrameID, CFrame*& pFrame)
{
	CFrame* pTmpFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pTmpFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pTmpFrame && (pTmpFrame->GetFrameId() == strFrameID))
		{
			pFrame = pTmpFrame;
			return true;
		}//if
	}//for

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Scheduel pause \n
/////////////////////////////////////////////////////////////////////////////////
void CTemplate::Pause()
{
	CFrame* pTmpFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pTmpFrame = (CFrame*)m_aryFrame.GetAt(i);
		if(pTmpFrame)
		{
			pTmpFrame->Pause();
		}//if
	}//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 모든 스케줄의 리스트를 구한다. \n
/// @param (CScheduleArray&) listSchedule : (in/out) 스케줄의 리스트
/////////////////////////////////////////////////////////////////////////////////
void CTemplate::GetScheduleList(CScheduleArray& listSchedule)
{
	CFrame* pTmpFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pTmpFrame = (CFrame*)m_aryFrame.GetAt(i);
		//Grade가 1인 메인프레임만 기준으로 판단한다.
		if(pTmpFrame && pTmpFrame->GetGrade() ==1)
		{
			pTmpFrame->GetScheduleList(listSchedule);
		}//if
	}//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// HD Frame이 존재하는지 여부를 반환. \n
/// @return <형: bool> \n
///			<true: HD Frame이 존재함> \n
///			<false: HD Frame이 존재하지않음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CTemplate::HasHDFrame()
{
	//Video render type이 Overlay이면 HD Frame이 있는것과 같게 처리...
	if(GetVidoeRenderType() == E_OVERLAY)
	{
		return true;
	}//if

	bool bRet = false;
	CFrame* pTmpFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pTmpFrame = (CFrame*)m_aryFrame.GetAt(i);
		bRet |= pTmpFrame->IsHDFrame(); 
	}//for

	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 부모 스케줄의 Click 이벤트를 받아 설정된 자식 스케줄을 재생한다. \n
/// @param (CString) strScheduleId : (in) 부모스케줄의 Id
/////////////////////////////////////////////////////////////////////////////////
void CTemplate::PlayClickSchedule(CString strScheduleId)
{
	CFrame* pTmpFrame = NULL;
	for(int i=0; i<m_aryFrame.GetCount(); i++)
	{
		pTmpFrame = (CFrame*)m_aryFrame.GetAt(i);
		pTmpFrame->PlayClickSchedule(strScheduleId);
	}//for
}

BOOL CTemplate::WriteCurrentPlayInfo()
{
	CFile file;

	int cnt = 0;
	do {
		file.Open(CURRENT_PLAYINFO_FILENAME, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary);
		::Sleep(1);
	} while( ++cnt<1000 && file.m_hFile == CFile::hFileNull ); // under 1sec, and open-fail

	// open fail for writing
	if( file.m_hFile == CFile::hFileNull )
	{
		return FALSE;
	}

	char buf[1024];
	_snprintf(buf, sizeof(buf), "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
								"<ubc>\r\n"
								"\t<shortcutkey>%s</shortcutkey>\r\n"
								"</ubc>"
								, m_shortCut.c_str() );

	file.Write(buf, strlen(buf));
	file.Close();

	return TRUE;
}
