#pragma once

#include "ximage.h"
#include "PictureEx.h"
#include "AnnounceDlg.h"
#include "PPTEventSink.h"

// CAnnounce_ppt_Dlg 대화 상자입니다.

//! <Office _Application 상속 받은 개체>
/*!
	_Application을 single tone으로 사용하기 위하여 상속받음 \n
*/
class COfficeAnnounce : public _Application
{
public:
	static COfficeAnnounce* getInstance(void);		///<Single tone 인스턴스 생성 함수
	static void clearInstance(void);			///<Single tone 인스턴스 소멸 함수

	virtual ~COfficeAnnounce();						///<소멸자

protected:
	COfficeAnnounce();								///<생성자

	static COfficeAnnounce*		_instance;			///<Single tone 인스턴스
	static CCriticalSection	_lock;				///<동기화 객체
	static int				_refcnt;			///<참조 횟수
};


class CAnnounce_ppt_Dlg : public CAnnounceDlg, public CPPTInvokerInterface
{
	DECLARE_DYNAMIC(CAnnounce_ppt_Dlg)

protected:
	DECLARE_MESSAGE_MAP()

public:
	CAnnounce_ppt_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAnnounce_ppt_Dlg();

	void		BeginAnnounce(CAnnounce* pAnn);					///<공지 방송을 시작하도록 한다.
	void		EndAnnounce();									///<공지 방송을 중지하도록 한다.

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	static UINT PPTRunTimeCheck(LPVOID param);

protected:
	
	virtual bool	Play();
	virtual bool	Stop();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	bool			OpenDSRender(void);						///<Directshow render를 옵션에 맞추어 설정하고 그래프 빌더를 구성한다.
	void			CloseDSRender(void);					///<Directshow render를 종료한다.

	void			SetNoContentsFile();
	void			RePlay();

	COfficeAnnounce*			m_pptApp;													///<MS-PowerPoint 어플리케이션

	IConnectionPoint	*m_pPPTEventConnectionPoint;								///<ConnectionPoint 포인터
	CPPTEventSink		*m_pPPTEventSink;											///<파워포인트 이벤트 싱크 객체

	float				m_fPosX;													///<슬라이드쇼 창이 위치할 X 좌표
	float				m_fPosY;													///<슬라이드쇼 창이 위치할 Y 좌표
	float				m_fWidth;													///<슬라이드쇼 창 넓이
	float				m_fHeight;													///<슬라이드쇼 창 높이

	CRect				m_rectBGimage;												///<배경 이미지가 그려질 영역
	CString				m_strPresentationName;										///<이벤트를 처리해야는 Presentation 이름
	bool				m_bTopMost;													///<PPT slideshow 윈도우의 topmost 여부
	int					m_nLocale;													///<OS의 국가코드


	DWORD				m_dwStartTick;

	CxImage				m_bgImage;
	bool				m_bOpen;
	bool				m_bFileNotExist;
	CString				m_strMediaFullPath;

	double GetTotalPlayTime();
	double GetCurrentPlayTime();
	double GetTickDiff(DWORD dwStart, DWORD dwEnd);
public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	bool			ConnectPPTEventSink(void);										///<파워포인트 이벤트 싱크 설정 함수
	void			DisconnectPPTEventSink(void);									///<파워포인트 이벤트 싱크 해제 함수
	void			SlideShowEnd(void);												///<슬라이드 쇼가 끝났음을 설정
	void			ClosePresentation(void);										///<ppt 슬라이드 쇼를 닫는다
	void			SetTopMostSlideShow(void);										///<슬라이드 쇼 창을 TopMost로 설정한다.
	void			SetSlideShowResolution(void);									///<슬라이드 쇼의 해상도를 시스템의 해상도에 맞춘다.

	// CPPTInvokerInterface
	virtual void	SetSlideShowEnd(float ftTime);									///<슬라이드 쇼를 일정한 시간후에 종료하도록 설정
	virtual void	SlideShowBegin(void);											///<슬라이드 쇼가 시작되었음을 설정
	virtual void	PresentationOpen(void);											///<Presentation이 Open 된 이후 슬라이드 쇼를 시작하도록 한다.
	virtual void	EscapeBRW(void);												///<파워포인트에 ESC 키가 눌러져서 BRW를 종료한다.
	virtual bool	StopPlay() { return Stop(); }

	typedef struct _ST_FindWnd
	{
		char	szSlideName[256];		///<슬라이드 쇼 이름
		HWND	hWnd;					///<윈도우 핸들
	} ST_FindWnd;

	static BOOL CALLBACK EnumWndProc(HWND hwnd, LPARAM lParam);						///<EnumWindow를 통한 슬라이드쇼 윈도우를 찾는 call back 함수

};
