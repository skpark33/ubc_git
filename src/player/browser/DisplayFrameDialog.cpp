// DisplayFrameDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "DisplayFrameDialog.h"

#include "SelectFrameDialog.h"


#ifndef LWA_ALPHA
#define LWA_ALPHA	0x00000002
#endif

#ifndef WS_EX_LAYERED
#define WS_EX_LAYERED	0x00080000
#endif



COLORREF	g_ColorTable[13] = {
	RGB(255,	255,	255),
	RGB(255,	0,		0),
	RGB(255,	255,	0),
	RGB(0,		255,	0),
	RGB(0,		255,	255),
	RGB(0,		0,		255),
	RGB(255,	0,		255),
	RGB(128,	0,		128),
	RGB(0,		0,		128),
	RGB(0,		128,	128),
	RGB(0,		128,	0),
	RGB(128,	128,	0),
	RGB(128,	0,		0),
};

IMPLEMENT_DYNAMIC(CDisplayFrameWnd, CWnd)

CDisplayFrameWnd::CDisplayFrameWnd() { m_font.CreatePointFont(48*10, "Tahoma"); }
CDisplayFrameWnd::~CDisplayFrameWnd() {}
BEGIN_MESSAGE_MAP(CDisplayFrameWnd, CWnd)
	ON_WM_PAINT()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void CDisplayFrameWnd::OnPaint()
{
	COLORREF rgb = g_ColorTable[ atoi(m_info.frameId.c_str()) % 13 ];

	CPaintDC dc(this);

	CRect client_rect;
	GetClientRect(client_rect);
	dc.FillSolidRect(client_rect, rgb);

	dc.SetTextColor(RGB(0,0,0));
	dc.SetBkColor(rgb);
	dc.SelectObject(&m_font);
	dc.DrawText(m_info.frameId.c_str(), m_info.frameId.length(), client_rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
}

void CDisplayFrameWnd::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnLButtonUp(nFlags, point);

	FRAME_SCHEDULE_INFO* info = new FRAME_SCHEDULE_INFO;

	*info = m_info;

	::AfxGetMainWnd()->PostMessage(WM_CONTENTS_UPDATE, (WPARAM)info);

	/*m_wndParent->DestroyWindow();

	delete m_wndParent;*/
}

void CDisplayFrameWnd::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}




// CDisplayFrameDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDisplayFrameDialog, CDialog)

CDisplayFrameDialog::CDisplayFrameDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CDisplayFrameDialog::IDD, pParent)
{
	byTrans = 160; // 투명도 : 0 ~ 255 
	g_SetLayeredWindowAttributes = NULL;
}

CDisplayFrameDialog::~CDisplayFrameDialog()
{
}

void CDisplayFrameDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
//	DDX_Control(pDX, IDC_STATIC_NUMBER, m_staticNumber);
}


BEGIN_MESSAGE_MAP(CDisplayFrameDialog, CDialog)
	ON_WM_SIZE()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CDisplayFrameDialog 메시지 처리기입니다.

BOOL CDisplayFrameDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	CRect rect;
	GetClientRect(rect);
	m_wndBack.m_info = m_info;
	m_wndBack.Create(NULL, "BackgroundWnd", WS_CHILD | WS_VISIBLE, rect, this, 0xffff);
	m_wndBack.m_wndParent = m_wndParent;

	HMODULE hUser32 = ::GetModuleHandle( _T("user32.dll"));
	if(hUser32)
	{
		g_SetLayeredWindowAttributes = (API_TYPE)::GetProcAddress( hUser32, "SetLayeredWindowAttributes"); 

		if( g_SetLayeredWindowAttributes) 
		{ 
			lOldStyle = GetWindowLong( m_hWnd, GWL_EXSTYLE); 
			SetWindowLong( m_hWnd, GWL_EXSTYLE, lOldStyle | WS_EX_LAYERED); 
			g_SetLayeredWindowAttributes( m_hWnd, 0, byTrans, LWA_ALPHA); 
		}
	}

//	m_staticNumber.SetWindowText(m_info.frameId.c_str());

	if(m_info.pip_x != 0 || m_info.pip_y != 0 || m_info.pip_width != 0 || m_info.pip_height != 0)
	{
		//m_rgn;
		CPoint pts[10];
		pts[0].SetPoint(0,									0);
		pts[1].SetPoint(m_info.width,						0);
		pts[2].SetPoint(m_info.width,						m_info.height);
		pts[3].SetPoint(0,									m_info.height);
		pts[4].SetPoint(0,									m_info.pip_y);
		pts[5].SetPoint(m_info.pip_x,						m_info.pip_y);
		pts[6].SetPoint(m_info.pip_x,						m_info.pip_y + m_info.pip_height);
		pts[7].SetPoint(m_info.pip_x + m_info.pip_width,	m_info.pip_y + m_info.pip_height);
		pts[8].SetPoint(m_info.pip_x + m_info.pip_width,	m_info.pip_y);
		pts[9].SetPoint(0,									m_info.pip_y);

		m_rgn.CreatePolygonRgn(pts, 10, ALTERNATE);

		SetWindowRgn(m_rgn, TRUE);
	}

	FreeLibrary(hUser32);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDisplayFrameDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	CRect rect;
	GetClientRect(rect);

	//if(m_staticNumber.GetSafeHwnd())
	//{
	//	m_staticNumber.MoveWindow(0,0,rect.Width(), rect.Height());
	//}

	if(m_wndBack.GetSafeHwnd())
	{
		m_wndBack.MoveWindow(0,0,rect.Width(), rect.Height());
	}
}

void CDisplayFrameDialog::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnLButtonUp(nFlags, point);
/*
	FRAME_SCHEDULE_INFO* info = new FRAME_SCHEDULE_INFO;

	*info = m_info;

	::AfxGetMainWnd()->PostMessage(WM_CONTENTS_UPDATE, (WPARAM)info);
*/
	/*m_wndParent->DestroyWindow();

	delete m_wndParent;*/
}

void CDisplayFrameDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}
