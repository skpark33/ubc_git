// AnnounceDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "Announce_web_Dlg.h"
#include "MemDC.h"
#include "Host.h"

//동영상 화면갱신 타이머
#define		ID_VIDEO_REPAINT			4014
#define		TIME_VIDEO_REPAINT			1000
//동영상 재생종료 타이머
#define		ID_VIDEO_PLAYEND			4015
#define		TIME_VIDEO_PLAYEND			300

// CAnnounce_web_Dlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAnnounce_web_Dlg, CAnnounceDlg)

CAnnounce_web_Dlg::CAnnounce_web_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent /*=NULL*/)
	: CAnnounceDlg(nPosX,nPosY,nCx,nCy,pParent)
,	m_pdlgIE(NULL)
,	m_nErrorCode(0)
,	m_bNavigateError(false)
{
	__DEBUG__("CAnnounce_web_Dlg", _NULL);

	m_bOpen = false;
	m_bFileNotExist = false;

	m_dwStartTick = ::GetTickCount();
}

CAnnounce_web_Dlg::~CAnnounce_web_Dlg()
{
}

BEGIN_MESSAGE_MAP(CAnnounce_web_Dlg, CAnnounceDlg)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

int CAnnounce_web_Dlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG__("CAnnounce_web_Dlg::OnCreate()", _NULL);
	if (CAnnounceDlg::OnCreate(lpCreateStruct) == -1){
		return -1;
	}


	return 0;
}
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 시작하도록 한다. \n
/// @param (CAnnounce*) pAnn : (in) 방송을 해야하는 공지 객체 주소값
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_web_Dlg::BeginAnnounce(CAnnounce* pAnn)
{
	__DEBUG__("BeginAnnounce", 0);

	if(!m_pAnnPlay)
	{
		m_pAnnPlay = pAnn;
	}
	else if((*m_pAnnPlay == *pAnn) && m_pAnnPlay->m_bPlaying)
	{
		//if(!(((CHost*)m_pParentWnd)->IsWindowVisible())){
			__DEBUG__("Same Annnounce Shows", pAnn->m_strAnnounceId.c_str());
			ShowWindow(SW_SHOW);
			((CHost*)m_pParentWnd)->SetFocus();
		//}
		return;
	}
	else
	{
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = pAnn;
	}//if

	if(m_pAnnPlay->m_strBgFile.empty()){
		m_strMediaFullPath.Empty();
	}else{
		char	szBuf[_MAX_PATH] = { 0x00 };
		::GetModuleFileName(NULL, szBuf, _MAX_PATH);

		char drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
		_splitpath(szBuf, drive, path, filename, ext);

		m_strMediaFullPath.Format("%s\\SQISOFT\\Contents\\Enc\\announce\\%s",drive, m_pAnnPlay->m_strBgFile.c_str());
	}

	__DEBUG__("BeginAnnounce bgFile",	m_strMediaFullPath);
	__DEBUG__("BeginAnnounce announceId", m_pAnnPlay->m_strAnnounceId.c_str());

	CString strLog = m_pAnnPlay->m_strBgFile.c_str();
	WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(),m_pAnnPlay->m_strTitle.c_str(), "TRY", m_pAnnPlay->GetRunningTime(), strLog);

	MoveAnnounceWindow(m_nParentPosX, m_nParentPosY, m_nParentCx, m_nParentCy, m_pAnnPlay);

	if(OpenFile(0)){
		m_pAnnPlay->m_bPlaying = true;
		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "START", m_pAnnPlay->GetRunningTime(), strLog);
		//__DEBUG__("ShowWindow", 0);
		ShowWindow(SW_SHOW);
		((CHost*)m_pParentWnd)->SetFocus();
	}else{
		SetNoContentsFile();
		this->RedrawWindow();
	}

	if(!m_pthreadShift)
	{
		m_pthreadShift = ::AfxBeginThread(CAnnounce_web_Dlg::WebRunTimeCheck, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pthreadShift->m_bAutoDelete = TRUE;
		m_bExitThread = false;
		m_hevtShift = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pthreadShift->ResumeThread();


		if(this->m_pAnnPlay){
			ciString shortCut = this->m_pAnnPlay->m_strComment[8];
			__DEBUG__("Start ShortCut",shortCut.c_str());
			if(!shortCut.empty()){
				// 키보드 이벤트를 날린다.
				this->keyboardSimulator(shortCut.c_str());
			}
		}

		//EndAnnounce();
	}//if
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 중지하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_web_Dlg::EndAnnounce()
{
	__DEBUG__("EndAnnounce", 0);
	if(m_pAnnPlay)
	{
		__DEBUG__("EndAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());



		CString strLog = m_pAnnPlay->m_strBgFile.c_str();

		CloseFile();

		ciString shortCut = this->m_pAnnPlay->m_strComment[9];
		__DEBUG__("End ShortCut",shortCut.c_str());
		if(!shortCut.empty()){
			// 키보드 이벤트를 날린다.
			this->keyboardSimulator(shortCut.c_str());
		}


		m_bExitThread = true;
		SetEvent(m_hevtShift);
		CloseHandle(m_hevtShift);
		m_hevtShift = NULL;
		m_pthreadShift = NULL;

		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "END", m_pAnnPlay->GetRunningTime(), strLog);

		ShowWindow(SW_HIDE);
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = NULL;
	}//if
}
void CAnnounce_web_Dlg::OnDestroy()
{
	__DEBUG__("OnDestroy",0);
	CloseFile();

	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if
	CAnnounceDlg::OnDestroy();

}

CString CAnnounce_web_Dlg::GetFilePath()
{
	//배경 이미지를 지원하기 위하여
	return m_pAnnPlay->m_strBgFile.c_str();
}


void CAnnounce_web_Dlg::OnPaint()
{
	__DEBUG__("OnPaint",NULL);
	if(!m_bOpen) return;

	__DEBUG__("OnPaint Start, m_bFileNotExist",m_bFileNotExist);
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	CMemDC memDC(&dc);
	CRect rtClient;
	GetClientRect(rtClient);
	
	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}else{
		if(m_bgImage.IsValid())
		{
			m_bgImage.Draw2(memDC.GetSafeHdc(), rtClient);
		}//if

	}


	if(m_ticker && 	m_ticker->IsPlay()){
		m_ticker->BringWindowToTop();
		m_ticker->SetForegroundWindow();
		if(!this->m_pAnnPlay || this->m_pAnnPlay->m_strComment[9].empty()){  // comment9 에는 단축키가 들어있다.
			m_ticker->SetFocus();
		}
	}else{
		BringWindowToTop();
		SetForegroundWindow();
		SetFocus();
	}

	static bool isFirstTime = true;
	if(isFirstTime && this->m_pAnnPlay){
		isFirstTime = false;
		ciString shortCut = this->m_pAnnPlay->m_strComment[8];
		__DEBUG__("Start ShortCut (OnPaint)",shortCut.c_str());
		if(!shortCut.empty()){
			// 키보드 이벤트를 날린다.
			this->keyboardSimulator(shortCut.c_str());
		}
	}
	__DEBUG__("OnPaint End",NULL);
}

void CAnnounce_web_Dlg::SetNoContentsFile()
{
	__DEBUG__("Contents file not exist", m_strMediaFullPath);

	//컨텐츠 파일이 없는 경우 액박 표시해 준다.
	HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_XBOX));
	m_bgImage.CreateFromHBITMAP(hBitmap);
	m_bOpen = true;
	m_bFileNotExist = true;
}

bool CAnnounce_web_Dlg::Play()
{
	__DEBUG__("Play start", _NULL);

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		return false;
	}//if

	if(m_pdlgIE)
	{
		m_pdlgIE->DestroyWindow();
		delete m_pdlgIE;
		m_pdlgIE = NULL;
	}//if

	m_pdlgIE = new CExplorerDlg(this);
	m_pdlgIE->setNavigateCallback(this);
	BOOL ret = m_pdlgIE->Create(IDD_WEB_DLG, this);
	if(ret == FALSE)
	{
		__ERROR__("Web control create fail !!!", m_strMediaFullPath);
		m_bOpen = false;

		return false;
	}//if	
	m_pdlgIE->ShowWindow(SW_HIDE);
	CRect rtRect;
	GetClientRect(&rtRect);
	m_pdlgIE->MoveWindow(0, 0, rtRect.Width(), rtRect.Height());
	m_pdlgIE->Navigate(m_pAnnPlay->m_strComment[0].c_str());

	m_dwStartTick = ::GetTickCount();

	return true;


}

bool CAnnounce_web_Dlg::Stop()
{
	__DEBUG__("Stop",0);

	if(m_pdlgIE)
	{
	__DEBUG__("Stop",1);
		m_pdlgIE->DestroyWindow();
	__DEBUG__("Stop",1.1);
		delete m_pdlgIE;
	__DEBUG__("Stop",1.2);
		m_pdlgIE = NULL;
	__DEBUG__("Stop",1.3);
	}//if
	__DEBUG__("Stop",1.4);

	if(m_bNavigateError)
	{
	__DEBUG__("Stop",2);
		CString strDesc;
		strDesc.Format("URL[%s] navigation error[Code=%d]. Image file[%s] played", m_pAnnPlay->m_strComment[0].c_str(), m_nErrorCode, m_pAnnPlay->m_strBgFile.c_str());
		__ERROR__(strDesc,0);
	}
	else
	{
		__DEBUG__("Stop Succeed",0);
	}//if
	return true;

}

bool CAnnounce_web_Dlg::OpenFile(int nGrade)
{
	__DEBUG__("OpenFile",0);

	if(m_bOpen)
	{
		if(m_pAnnPlay->m_bPlaying)
		{
			__DEBUG__("\t\t\tAlready Open", _NULL);
			return true;
		}
		CloseFile();
	}//if
/*
	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			__DEBUG__("BG image open fail", m_strMediaFullPath);
			m_bOpen = false;
		}else{
			CRect rtClient;
			GetClientRect(&rtClient);
			int nWidth = rtClient.Width();
			int nHeight = rtClient.Height();
			DWORD dwWidth = m_bgImage.GetWidth();
			DWORD dwHeight = m_bgImage.GetHeight();
			if(dwWidth != nWidth || dwHeight != nHeight)
			{
				if(!m_bgImage.Resample(nWidth, nHeight, 3))	//default option(bilinear interpolation)
				{
					__DEBUG__("image resample fail", m_strMediaFullPath);
				}//if
			}//if
		}
	}//if
*/
	m_bOpen = true;

	return Play(); 
}

bool CAnnounce_web_Dlg::CloseFile()
{
	__DEBUG__("CloseFile()",NULL);
	

	if(m_bOpen)
	{
		m_bOpen = false;
		Stop();

		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if
	}//if
	return true;
}


void CAnnounce_web_Dlg::RePlay()
{
	__DEBUG__("Web Replay",0);
	Stop();
	Play();
}

double CAnnounce_web_Dlg::GetTotalPlayTime()
{
	if(m_pAnnPlay){
		return (double)(strtoul(m_pAnnPlay->m_strComment[1].c_str(), NULL,10)*1000); // sec -> millisec
	}
	return 0.0;
}

double CAnnounce_web_Dlg::GetCurrentPlayTime()
{
	return (double)GetTickDiff(m_dwStartTick,::GetTickCount());
}

double CAnnounce_web_Dlg::GetTickDiff(DWORD dwStart, DWORD dwEnd)
{
	if(dwStart > dwEnd)
		return double((MAXDWORD - dwStart) + dwEnd);
	return double(dwEnd - dwStart);
}


UINT CAnnounce_web_Dlg::WebRunTimeCheck(LPVOID pParam)
{
	CAnnounce_web_Dlg* pParent = (CAnnounce_web_Dlg*)pParam;

	while(!pParent->m_bExitThread)
	{
		DWORD dwEventRet = WaitForSingleObject(pParent->m_hevtShift, 1000);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			__DEBUG__("Event set : WebRunTimeCheck", _NULL);
			break;
		}
		//__DEBUG__("Timeout   : WebRunTimeCheck", _NULL);

		double runTime = pParent->GetTotalPlayTime();
		double currentTime =  pParent->GetCurrentPlayTime();
		
		if( runTime && runTime <= currentTime){
			//pParent->RePlay(); // replay 하면 죽어버림
			//if(pParent->m_bNavigateError && pParent->m_bOpen == false){
			//if(pParent->m_bNavigateError && pParent->m_strMediaFullPath.IsEmpty()){
			if(pParent->m_bNavigateError){
				// 뭔가 Error 가 난 상황이라면 StopAnnouce 해버린다.
				pParent->EndAnnounce();
				pParent->SetPlayAnnounce();
			}
		}
	}//while
	__DEBUG__("Exit PlayImageThread", _NULL);
	return 1;
}

void CAnnounce_web_Dlg::NavigateError(bool bError, int nErrorCode)
{
	__DEBUG__("NavigateError", bError );

	m_bNavigateError = bError;
	m_nErrorCode = nErrorCode;

	//if(m_pdlgIE && m_bNavigateError && !this->m_strMediaFullPath.IsEmpty())
	if(m_pdlgIE && m_bNavigateError)
	{
		m_pdlgIE->DestroyWindow();
		delete m_pdlgIE;
		m_pdlgIE = NULL;
	}//if

	Invalidate(FALSE);
}
