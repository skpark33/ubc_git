/************************************************************************************/
/*! @file DownloadFile.cpp
	@brief FTP에서 컨텐츠 파일을 다운로드 받기위한 정보를 갖는 CDownloadFile 클래스 구현 파일
	@remarks
	▶ 작성자: 정운현\n
	▶ 작성일: 2009/06/26\n
	▶ 사용파일: atdafx.h, DownloadFile.h\n
	
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************

	@b 작성)
	-# <2009/06/26:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"
#include "DownloadFile.h"

CDownloadFile::CDownloadFile()
:	m_strServerPath("")
,	m_strFileName("")
,	m_ulFileSize(0)
,	m_nContentType(0)
,	m_strContentsId("")
,	m_strContentsName("")
,	m_nDownloadReason(E_REASON_INIT)
,	m_nDownloadState(E_STATE_INIT)
,	m_bDownloadResult(false)
,	m_ullDownloadSize(0)
{
}

CDownloadFile::~CDownloadFile()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 초기상태로 다운로드 값들을 설정한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloadFile::SetInit()
{
	m_nDownloadReason	= E_REASON_INIT;
	m_nDownloadState	= E_STATE_INIT;
	m_ullDownloadSize	= 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시작상태로 다운로드 값들을 설정한다.  \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloadFile::SetStart()
{
	m_nDownloadState	= E_STATE_DOWNLOADING;
	m_tmStartTime		= CTime::GetCurrentTime();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 종료상태로 다운로드 결과값들을 설정한다. \n
/// @param (int) nReason : (in) 다운로드 이유
/// @param (int) nState : (in) 다운로드 상태
/// @param (int) nDownloadSize : (in) 다운로드된 사이즈
/// @param (bool) bResult : (in) 다운로드 결과
/////////////////////////////////////////////////////////////////////////////////
void CDownloadFile::SetEnd(int nReason, int nState, int nDownloadSize, bool bResult)
{
	m_nDownloadReason	= nReason;
	m_nDownloadState	= nState;
	m_ullDownloadSize	= nDownloadSize;
	m_bDownloadResult	= bResult;
	m_tmEndTime			= CTime::GetCurrentTime();
}




//////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 새로운 엘리먼트 추가(중복된 element가 들어가지 않도록 재정의) \n
/// @param (CDownloadFile*) pInfo : (in) 새로 추가하려는 노드
/// @return 새로 추가된 노드의 인덱스 \n
/////////////////////////////////////////////////////////////////////////////////
int CDownloadFileArray::Add(CDownloadFile* pInfo)
{
	CDownloadFile* pExist = NULL;
	for(int i=0; i<GetCount(); i++)
	{
		pExist = (CDownloadFile*)CArray::GetAt(i);
		if((*pInfo) == (*pExist))
		{
			return i;
		}//if
	}//if

	int nRet = CArray::Add(pInfo);

	return nRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 배열을 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloadFileArray::Clear()
{
	CDownloadFile* pFile = NULL;
	for(int i=0; i<GetCount(); i++)
	{
		pFile = GetAt(i);
		delete pFile;
	}//for
	RemoveAll();
}
