// HelpDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "HelpDialog.h"
#include "MemDC.h"

const char* hotkey[][2] = {
	{"F1",			"help"},
	{"F2",			"debug dialog"},
	{"F3",			"contents change"},
//	{"F4",			"timer dialog"},
	{"F5",			"save schedule to local"},
	{"F6",			"reset event count"},
	{"F7",			"show license key"},
	{"F9",			"show/hide play-time dialog"},
	{"F12",			"fullscreen/window"},
	{"Page Up",		"play previous default template"},
	{"Page Down",	"play next default template"},
	{"Insert",		"increase volume up to 5%"},
	{"Delete",		"decrease volume down to 5%"},
	{"End",			"sound on/off"},
	{"Right Arrow",	"play next schedule (only main frame)"},
//	{"Enter",		"request phone"},
	{"Pause",		"toggle play/pause to next default template playing"},
	{"CTRL + Q",	"toggle start/stop browser"},
	{"CTRL + M",	"toggle show/hide menu bar button"},
	{"ESC",			"exit"},
	{"",			""}
};

const char* parameter[][2] = {
	{"+window",			"window mode"},
	{"+replace",		"background initialize and replace mode"},
	{"+mouse",			"view mouse cursor"},
	{"+local",			"load schedule from local (not network)"},
	{"+xscale VALUE",	"resize by x scale (original=100)"},
	{"+yscale VALUE",	"resize by y scale (original=100)"},
	{"+scale VALUE",	"resize by x&y scale (original=100, ignore +xscale & +yscale)"},
	{"+autoscale",		"auto resize, fit to screen (ignore +xscale & +yscale & +scale)"},
	{"+aspectratio",	"keep aspect ratio (must be used with +autoscale)"},
	{"+x POS_X",		"move start point to POS_X"},
	{"+y POS_Y",		"move start point to POS_Y"},
	{"+xcenter",		"move to horizontal center (ignore +x)"},
	{"+ycenter",		"move to vertical center (ignore +y)"},
	{"+center",			"move to center (ignore +x and +y)"},
	{"+dummy FILENAME",	"set dummy filename to FILENAME"},
	{"+site SITENAME",	"set site name to SITENAME"},
	{"+host HOSTNAME",	"set host name to HOSTNAME"},
	{"+appId APPID",	"set app id to APPID"},
	{"+time 2000:01:01:00:00:00", "assume host time to this time"},
	{"+template TEMPLATENUMBER", "playing only TEMPLATENUMBER template in local mode (must be set +local)"},
	{"+thread", "use thread for faster initialize program"},
	{"+menubar", "Show menue bar button"},
	{"", ""}
};

// CHelpDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHelpDialog, CDialog)

CHelpDialog::CHelpDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CHelpDialog::IDD, pParent)
	, m_reposControl (this)
	, m_strEdition("")
	, m_strVersion("")
	, m_strComputerName("")
	,m_strRegstrationKey("")
{

}

CHelpDialog::~CHelpDialog()
{
	//if(m_imageEdition.IsValid())
	//{
	//	m_imageEdition.Destroy();
	//}//if
}

void CHelpDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HOTKEY, m_listctrlHotkey);
	DDX_Control(pDX, IDC_LIST_PARAMETER, m_listctrlParameter);
	DDX_Control(pDX, IDC_STATIC_HOTKEY, m_groupHotkey);
	DDX_Control(pDX, IDC_STATIC_PARAMETER, m_groupParameter);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_VERSION_STATIC, m_staticVersion);
	DDX_Control(pDX, IDC_NAME_STATIC, m_staticName);
	DDX_Control(pDX, IDC_KEY_STATIC, m_staticKey);
	DDX_Control(pDX, IDC_VERSION_EDT, m_edtVersion);
	DDX_Control(pDX, IDC_NAME_EDT, m_edtName);
	DDX_Control(pDX, IDC_KEY_EDT, m_edtKey);
	DDX_Control(pDX, IDC_EDITION_EDT, m_edtEdition);
	DDX_Control(pDX, IDC_EDITION_STATIC, m_staticEdition);
}


BEGIN_MESSAGE_MAP(CHelpDialog, CDialog)
	ON_WM_SIZE()
	ON_WM_MOUSEMOVE()
	ON_WM_NCMOUSEMOVE()
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CHelpDialog 메시지 처리기입니다.

BOOL CHelpDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_reposControl.AddControl(&m_groupHotkey,		REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_RESIZE_DIV2, 1, 2);
	m_reposControl.AddControl(&m_groupParameter,	REPOS_FIX, REPOS_RESIZE_DIV2, REPOS_MOVE, REPOS_MOVE, 1,2);

	m_reposControl.AddControl(&m_listctrlHotkey,	REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_RESIZE_DIV2, 1, 2);
	m_reposControl.AddControl(&m_listctrlParameter,	REPOS_FIX, REPOS_RESIZE_DIV2, REPOS_MOVE, REPOS_MOVE, 1,2);

	m_reposControl.AddControl(&m_btnOK,	REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.AddControl(&m_staticVersion,	REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_reposControl.AddControl(&m_staticName,	REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_reposControl.AddControl(&m_staticKey,		REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);

	m_reposControl.AddControl(&m_edtEdition,	REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_reposControl.AddControl(&m_edtVersion,	REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_reposControl.AddControl(&m_edtName,		REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_reposControl.AddControl(&m_edtKey,		REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	
	m_reposControl.Move();

	//
	m_listctrlHotkey.SetExtendedStyle( LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );
	m_listctrlHotkey.InsertColumn(0, "Key", LVCFMT_LEFT, 120);
	m_listctrlHotkey.InsertColumn(1, "Description", LVCFMT_LEFT, 250);

	m_listctrlParameter.SetExtendedStyle( LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );
	m_listctrlParameter.InsertColumn(0, "Parameter", LVCFMT_LEFT, 120);
	m_listctrlParameter.InsertColumn(1, "Description", LVCFMT_LEFT, 250);

	for(int i=0; true; i++)
	{
		if(hotkey[i][0][0] == 0)
			break;
		m_listctrlHotkey.InsertItem(i, hotkey[i][0]);
		m_listctrlHotkey.SetItemText(i, 1, hotkey[i][1]);
	}

	for(int i=0; true; i++)
	{
		if(parameter[i][0][0] == 0)
			break;
		m_listctrlParameter.InsertItem(i, parameter[i][0]);
		m_listctrlParameter.SetItemText(i, 1, parameter[i][1]);
	}

	m_edtEdition.SetWindowText(m_strEdition);
	m_edtVersion.SetWindowText(m_strVersion);
	m_edtName.SetWindowText(m_strComputerName);
	m_edtKey.SetWindowText(m_strRegstrationKey);

	CString strCustomer = GetCoutomerInfo();
	if(strCustomer == CUSTOMER_NARSHA)
	{
		HICON hIcon = (HICON)LoadImage(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_NARSHA), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
		CStatic* pStatic = (CStatic*)GetDlgItem(IDC_EDITION_STATIC);
		pStatic->SetIcon(hIcon);

		pStatic = (CStatic*)GetDlgItem(IDC_EDITION_EDT);
		pStatic->ShowWindow(SW_HIDE);

		pStatic = (CStatic*)GetDlgItem(IDC_COPY_STATIC);
		pStatic->ShowWindow(SW_HIDE);

		pStatic = (CStatic*)GetDlgItem(IDC_COMPANY_STATIC);
		pStatic->ShowWindow(SW_HIDE);

		pStatic = (CStatic*)GetDlgItem(IDC_RIGHTS_STATIC);
		pStatic->ShowWindow(SW_HIDE);
	}//if

	CenterWindow();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CHelpDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();
}

void CHelpDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}

void CHelpDialog::OnNcMouseMove(UINT nHitTest, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnNcMouseMove(nHitTest, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nHitTest, (LPARAM)MAKELONG(point.x, point.y));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램의 버전등의 정보를 설정한다. \n
/// @param (CString) strEdition : (in) 프로그램의 제품구분(STD, PLS, ENT)
/// @param (CString) strVersion : (in) 프로그램의 버전
/// @param (CString) strName : (in) 프로그램이 설치된 컴퓨터의 이름
/// @param (CString) strKey : (in) 제품의 인증 키
/////////////////////////////////////////////////////////////////////////////////
void CHelpDialog::SetAppInof(CString strEdition, CString strVersion, CString strName, CString strKey)
{
	if(strEdition == STANDARD_EDITION)
	{
		m_strEdition = "UBC Standard Edition";
	}
	else if(strEdition == STANDARD_EDITION)
	{
		m_strEdition = "UBC Standard Plus Edition";
	}
	else
	{
		m_strEdition = "UBC Enterprise Edition";
	}//if
	m_strVersion = ": " + strVersion;
	m_strComputerName = ": " + strName;
	m_strRegstrationKey = ": " + strKey;
}

void CHelpDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	//CMemDC memDC(&dc);

	//if(m_imageEdition.IsValid())
	//{
	//	CRect rect;
	//	m_staticEdition.GetClientRect(rect);

	//	m_imageEdition.Draw2(memDC.m_hDC, rect);
	//}//if	
}
