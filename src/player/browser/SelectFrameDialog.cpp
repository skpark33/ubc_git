// SelectFrameDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "SelectFrameDialog.h"


// CSelectFrameDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSelectFrameDialog, CDialog)

CSelectFrameDialog::CSelectFrameDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectFrameDialog::IDD, pParent)
	, m_infoList (NULL)
{

}

CSelectFrameDialog::~CSelectFrameDialog()
{
	for(int i=0; i<m_dlgList.GetCount(); i++)
	{
		CDisplayFrameDialog* dlg = m_dlgList.GetAt(i);

		dlg->DestroyWindow();

		delete dlg;
	}
	m_dlgList.RemoveAll();
}

void CSelectFrameDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSelectFrameDialog, CDialog)

	ON_WM_MOUSEMOVE()
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CSelectFrameDialog 메시지 처리기입니다.

BOOL CSelectFrameDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	if( m_infoList )
	{
		int count = m_infoList->GetCount();
		for(int i=0; i<count; i++)
		{
			FRAME_SCHEDULE_INFO& info = m_infoList->GetAt(i);

			CDisplayFrameDialog* dlg = new CDisplayFrameDialog(GetParent());
			dlg->m_wndParent = this;
			dlg->m_info = info;
			dlg->Create(IDD_DISPLAY_FRAME_DIALOG, GetParent());
			// Modified by 정운형 2008-12-02 오후 7:38
			// 변경내역 :  dialog 위치 수정
			//dlg->MoveWindow(info.x, info.y, info.width, info.height);
			dlg->MoveWindow(m_clsMainWndRect.left + info.x, m_clsMainWndRect.top + info.y, info.width, info.height);
			// Modified by 정운형 2008-12-02 오후 7:38
			// 변경내역 :  dialog 위치 수정
			dlg->ShowWindow(SW_SHOW);

			m_dlgList.Add(dlg);
		}
	}

	delete m_infoList;

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
/*
void CSelectFrameDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnCancel();

	//DestroyWindow();

	//delete this;

	//::AfxGetMainWnd()->PostMessage(WM_CONTENTS_UPDATE, (WPARAM)0);
}
*/
void CSelectFrameDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// <Main 창의 영역을 설정 \n
/// @param (CRect) clsMainWndRect : (in) CDisplayFrameDialog의 초기 위치를 설정하기 위한 Main 창의 영역
/////////////////////////////////////////////////////////////////////////////////
void CSelectFrameDialog::SetMainWndRect(CRect clsMainWndRect)
{
	m_clsMainWndRect = clsMainWndRect;
}

BOOL CSelectFrameDialog::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}//if
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}

void CSelectFrameDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	CRect rect;
	GetClientRect(&rect);

	dc.FillSolidRect(rect, RGB(200, 200, 200));
	dc.DrawText("Click a frame start playing again", rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
}
