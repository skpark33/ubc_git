/************************************************************************************/
/*! @file Announce.cpp
	@brief 긴급공지 객체 구현 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/09/24\n
	▶ 사용파일: stdafx.h, Announce.h\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2009/09/24:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"
#include "Announce.h"

CAnnounce::CAnnounce()
: m_strMgrId("")			
, m_strSiteId("")		
, m_strAnnounceId("")
, m_strTitle("")		
, m_ulSerialNo(0)
, m_strCreator("")
, m_lPosition(0)
, m_sHeight(0)			
, m_strFont("")
, m_sFontSize(0)
, m_strFgColor("")
, m_strBgColor("")
, m_strBgLocation("")
, m_strBgFile("")
, m_sPlaySpeed(0)
, m_sAlpha(0)
, m_nWidth(0)
, m_bPlaying(false)
//추가분
,m_ContentsType(2) // 티커
,m_LRMargin(0)	
,m_UDMargin(0)		
,m_SourceSize(1)	
,m_SoundVolume(0)	
,m_FileSize(0)		
,m_FileMD5("")
,m_HeightRatio(0)	
,m_Width(0)			
,m_PosX(0)			
,m_PosY(0)			
{
	for(int i=0; i<10; i++)
	{
		m_strComment[i] = "";
	}//for
}


CAnnounce::~CAnnounce()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지의 running time을 반환 \n
/// @return <형: int> \n
///			<공지의 running time (sec)> \n
/////////////////////////////////////////////////////////////////////////////////
int CAnnounce::GetRunningTime()
{
	return (m_tmEndTime - m_tmStartTime);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지의 내용을 xml문서 형태로 만들어 반환한다. \n
/// @return <형: CString> \n
///			<공지의 내용을 xml문서 형식으로 변환한 문자열> \n
/////////////////////////////////////////////////////////////////////////////////
CString CAnnounce::GetXmlMessage()
{
	CString strTmp;
	CString strXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
	strXml += "<emergency>\r\n";
	strXml += "<configure fontFamily=\"";
	strXml += m_strFont.c_str();
	strXml += "\" ";

	strXml += "backgroundColor=\"";
	strTmp = m_strBgColor.c_str();
	strTmp.Replace("#", "0x");
	strXml += strTmp;
	strXml += "\" ";

	strXml += "backgroundImage=\"";
	strXml += m_strBgFile.c_str();
	strXml += "\" ";

	//플래쉬 투명도 설정 사용안함
	strXml += "alpha=\"";
	strTmp.Format("%d\" ", 1);
	strXml += strTmp;

	strXml += "width=\"";
	strTmp.Format("%d\" ", m_nWidth);
	strXml += strTmp;

	strXml += "height=\"";
	strTmp.Format("%d\"/>", m_sHeight);
	strXml += strTmp;

	strXml += "<notices fontSize=\"";
	strTmp.Format("%d\" ", m_sFontSize);
	strXml += strTmp;

	strXml += "color=\"";
	strTmp = m_strFgColor.c_str();
	strTmp.Replace("#", "0x");
	strXml += strTmp;
	strXml += "\" ";

	strXml += "speed=\"";
	strTmp.Format("%d\" ", m_sPlaySpeed);
	strXml += strTmp;

	strXml += "course=\"forward\">\r\n";

/*************************************************************
" 	quotation mark 	&quot; 	&#34;
' 	apostrophe  	&apos; (does not work in IE) 	&#39;
& 	ampersand 		&amp; 	&#38;
< 	less-than 		&lt; 	&#60;
> 	greater-than 	&gt; 	&#62;
*************************************************************/

	//Comment
	for(int i=0; i<10; i++)
	{
		if(m_strComment[i] != "")
		{
			strXml += "<notice>";

			strTmp = m_strComment[i].c_str();
			strTmp.Replace("&", "&amp;");		//& 변환을 제일 먼저 해야한다.
			strTmp.Replace("\"", "&quot;");
			strTmp.Replace("\'", "&apos;");
			strTmp.Replace("<", "&lt;");
			strTmp.Replace(">", "&gt;");

			strXml += strTmp;
			strXml += "</notice>\r\n";
		}//if
	}//if

	strXml += "</notices>\r\n</emergency>";

	return strXml;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 비어있는 공지를 xml문서 형태로 만들어 반환한다. \n
/// @return <형: CString> \n
///			<공지의 내용을 xml문서 형식으로 변환한 문자열> \n
/////////////////////////////////////////////////////////////////////////////////
CString CAnnounce::GetEmptyXmlMessage()
{
	CString strTmp;
	CString strXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
	strXml += "<emergency>\r\n";
	strXml += "<configure fontFamily=\"";
	strXml += m_strFont.c_str();
	strXml += "\" ";

	strXml += "backgroundColor=\"";
	strTmp = m_strBgColor.c_str();
	strTmp.Replace("#", "0x");
	strXml += strTmp;
	strXml += "\" ";

	strXml += "backgroundImage=\"";
	strXml += m_strBgFile.c_str();
	strXml += "\" ";

	//플래쉬 투명도 설정 사용안함
	strXml += "alpha=\"";
	strTmp.Format("%d\" ", 1);
	strXml += strTmp;

	strXml += "width=\"";
	strTmp.Format("%d\" ", m_nWidth);
	strXml += strTmp;

	strXml += "height=\"";
	strTmp.Format("%d\"/>", m_sHeight);
	strXml += strTmp;

	strXml += "<notices fontSize=\"";
	strTmp.Format("%d\" ", m_sFontSize);
	strXml += strTmp;

	strXml += "color=\"";
	strTmp = m_strFgColor.c_str();
	strTmp.Replace("#", "0x");
	strXml += strTmp;
	strXml += "\" ";

	strXml += "speed=\"";
	strTmp.Format("%d\" ", m_sPlaySpeed);
	strXml += strTmp;

	strXml += "course=\"forward\">\r\n";
/*
	//Comment
	for(int i=0; i<10; i++)
	{
		if(m_strComment[i] != "")
		{
			strXml += "<notice text=\"";
			strXml += m_strComment[i].c_str();
			strXml += "\"/>\r\n";
		}//if
	}//if
*/
	
	strXml += "<notice></notice>\r\n</notices>\r\n</emergency>";

	return strXml;
}