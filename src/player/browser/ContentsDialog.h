#pragma once
#include "afxwin.h"


// CContentsDialog 대화 상자입니다.

class CContentsDialog : public CDialog
{
	DECLARE_DYNAMIC(CContentsDialog)

public:
	CContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CContentsDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CONTENTS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_cbxContentsType;
	CEdit m_editContentsFilename;
	afx_msg void OnBnClickedButtonBrowse();
	virtual BOOL OnInitDialog();

	FRAME_SCHEDULE_INFO		m_info;

protected:
	virtual void OnOK();


public:
	CButton m_btnBrowse;
	afx_msg void OnBnClickedButtonRecovery();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	CButton m_btnChange;
};
