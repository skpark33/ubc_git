/************************************************************************************/
/*! @file TemplatePlay.h
	@brief Template의 play rule을 갖는 클래스 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2006/10/27\n
	▶ 참고사항:
		- Host.h에 정의하였다가 파일로 분리
		   
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	-# 2006/10/27:정운형:Host.h에 정의하였다가 다른 클래스에서 include하기 위하여 파일을 분리
************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#include "stdafx.h"
#include "TemplatePlay.h"
#include "Template.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기본 생성자\n
/// 입력되는 문자열은 반드시 '/'를 포함하여야 하며,\n
/// 이를 기준으로 template id와 재생 방법을 결정한다.
/// @param (CString) strTemplate : (in) template play 방법을 갖는 문자열
/////////////////////////////////////////////////////////////////////////////////
CTemplatePlay::CTemplatePlay(CString strTemplate)
:	m_bUsePlayCount(false)
,	m_strTemplateID(_T(""))
,	m_unPlayCount(1)			//Play count를 사용하지 않고 순차적으로 프레임을 재생할때는 play count가 1이다.
,	m_pclsTemplate(NULL)
{
	int nIndx = strTemplate.Find('/', 0);
	if(nIndx != -1)
	{
		m_strTemplateID = strTemplate.Left(nIndx);									//'/'를 기준으로 좌측
		CString strPlayRule = strTemplate.Right(strTemplate.GetLength()-(nIndx+1));	//'/'를 기준으로 우측
		//'('가 있다면 재생 프레임 배열을 만들고, 없다면 template를 반복재생하는 횟수이다.
		if(strPlayRule.Find('(', 0) == -1)
		{
			m_bUsePlayCount = true;
			m_unPlayCount = atoi(strPlayRule);		//반복재생하는 횟수
		}
		else
		{
			m_bUsePlayCount = false;
			strPlayRule.TrimLeft('(');				//왼쪽 '(' 제거
			strPlayRule.TrimRight(')');				//오른쪽 ')' 제거
			//strPlayRule의 'a|b'는 a 다음에 b  schedule을 재생하라는 의미이며,
			//'a-z'는 a 부터 z 까지의 schedule을 순차적으로 재생하라는 의미이다.
			CString strTok, strNum;
			int nPos = 0, nRuleIndx = 0;
			int nStart, nEnd;
			strTok = strPlayRule.Tokenize("|", nPos);
			do
			{
				nRuleIndx = strTok.Find('-', 0);
				if(nRuleIndx == -1)
				{
					m_aryPlayOrder.Add(strTok);
				}
				else
				{
					nStart = atoi(strTok.Left(nRuleIndx));							//'-' 왼쪽이 시작
					nEnd = atoi(strTok.Right(strTok.GetLength()-(nRuleIndx+1)));	//'-' 오른쪽이 끝
					if(nStart <= nEnd)
					{
						for(int i=nStart; i<=nEnd; i++)
						{
							strNum.Format("%d", i);
							m_aryPlayOrder.Add(strNum);
						}//for
					}
					else
					{	//역순으로 된경우...(예: 9-3)
						for(int i=nStart; i>=nEnd; i--)
						{
							strNum.Format("%d", i);
							m_aryPlayOrder.Add(strNum);
						}//for
					}//if
				}//if

				strTok = strPlayRule.Tokenize("|", nPos);
			}while(strTok != "");//while
		}//if
	}
	else
	{
		m_strTemplateID = strTemplate;
	}//if
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 해당하는 Template ID의 template 포인터를 설정하는 함수 \n
/// @param (CTemplate*) pclsTemplate : (in) 해당하는 Template ID의 template 포인터
/// @return <형: void> \n
/////////////////////////////////////////////////////////////////////////////////
void CTemplatePlay::SetTemplatePtr(CTemplate* pclsTemplate)
{
	if(m_strTemplateID == pclsTemplate->GetTemplateId())
	{
		m_pclsTemplate = pclsTemplate;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 해당하는 Template ID의 template 포인터를 반환하는 함수 \n
/// @return <형: CTemplate*> \n
///			해당하는 Template ID의 template 포인터 \n
/////////////////////////////////////////////////////////////////////////////////
CTemplate* CTemplatePlay::GetTemplatePtr()
{
	return m_pclsTemplate;
}
*/
//#endif
// Modified by 정운형 2008-10-14 오전 10:20
// 변경내역 :  TemplatePlayList 변경작업 - dtcInfoListf를 대체하는 클래스