#pragma once

#include "ximage.h"
#include "PictureEx.h"
#include "Flash/FlashDlg.h"

#include "AnnounceDlg.h"
#include "shockwaveflash.h"

// CAnnounce_flash_Dlg 대화 상자입니다.

class CAnnounce_flash_Dlg : public CAnnounceDlg
{
	DECLARE_DYNAMIC(CAnnounce_flash_Dlg)

protected:
	DECLARE_MESSAGE_MAP()

public:
	CAnnounce_flash_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAnnounce_flash_Dlg();

	void		BeginAnnounce(CAnnounce* pAnn);					///<공지 방송을 시작하도록 한다.
	void		EndAnnounce();									///<공지 방송을 중지하도록 한다.

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();

	static UINT FlashRunTimeCheck(LPVOID param);

protected:
	
	virtual bool	Play();
	virtual bool	Stop();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	bool			OpenDSRender(void);						///<Directshow render를 옵션에 맞추어 설정하고 그래프 빌더를 구성한다.
	void			CloseDSRender(void);					///<Directshow render를 종료한다.

	void			SetNoContentsFile();
	void			RePlay();

	CFlashDlg*			m_pdlgFlash;
	DWORD				m_dwStartTick;

	CxImage				m_bgImage;
	bool				m_bOpen;
	bool				m_bFileNotExist;
	CString				m_strMediaFullPath;

	double GetTotalPlayTime();
	double GetCurrentPlayTime();
	double GetTickDiff(DWORD dwStart, DWORD dwEnd);
public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
};
