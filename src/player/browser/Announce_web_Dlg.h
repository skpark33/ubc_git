#pragma once

#include "ximage.h"
#include "PictureEx.h"
#include "WebBrowser/ExplorerDlg.h"

#include "AnnounceDlg.h"

// CAnnounce_web_Dlg 대화 상자입니다.

class CAnnounce_web_Dlg : public CAnnounceDlg, public INavigateCallback
{
	DECLARE_DYNAMIC(CAnnounce_web_Dlg)

protected:
	DECLARE_MESSAGE_MAP()

public:
	CAnnounce_web_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAnnounce_web_Dlg();

	void		BeginAnnounce(CAnnounce* pAnn);					///<공지 방송을 시작하도록 한다.
	void		EndAnnounce();									///<공지 방송을 중지하도록 한다.

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();

	static UINT WebRunTimeCheck(LPVOID param);


	void NavigateError(bool bError, int nErrorCode) ; // INavigateCallback
protected:
	// BRW define member values
	CExplorerDlg*	m_pdlgIE;				///<Explorer dialog
	int				m_nErrorCode;		///<error code
	bool			m_bNavigateError;	///<Navigate error flag

	virtual CString	GetFilePath();

	virtual bool	Play();
	virtual bool	Stop();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	bool			OpenDSRender(void);						///<Directshow render를 옵션에 맞추어 설정하고 그래프 빌더를 구성한다.
	void			CloseDSRender(void);					///<Directshow render를 종료한다.

	void			SetNoContentsFile();
	void			RePlay();

	DWORD				m_dwStartTick;

	CxImage				m_bgImage;
	bool				m_bOpen;
	bool				m_bFileNotExist;
	CString				m_strMediaFullPath;

	double GetTotalPlayTime();
	double GetCurrentPlayTime();
	double GetTickDiff(DWORD dwStart, DWORD dwEnd);

public:
};
