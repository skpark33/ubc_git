#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#include "ProgressCtrlEx.h"

// CPhoneAcceptDialog 대화 상자입니다.

class CPhoneAcceptDialog : public CDialog
{
	DECLARE_DYNAMIC(CPhoneAcceptDialog)

public:
	CPhoneAcceptDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPhoneAcceptDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PHONE_ACCEPT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CWnd*	m_pParentWnd;
	int		m_nCurrentPos;
	cciReply*	m_pReply;
	CString		m_strScheduleId;

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CProgressCtrlEx		m_progressCancel;

	void	Start(LPCSTR lpszScheduleId, cciReply* reply);
	CStatic m_staticText;
	CButton m_btnOK;
	CButton m_btnCancel;
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
