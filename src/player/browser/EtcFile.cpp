/************************************************************************************/
/*! @file EtcFile.cpp
	@brief EtcFile 객체 클래스 구현 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/10/15\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2010/10/15:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"
//#include "BRW2.h"
//#include "Host.h"
#include "EtcFile.h"


CEtcFile::CEtcFile()
	:m_strID("")
	,m_strContentsName("")
	,m_strLocation("")
	,m_strFilename("")
	,m_strBGColor("")	
	,m_strFGColor("")	
	,m_strFont("")
	,m_strPromotionValueList("")
	,m_sFontSize(0) 		
	,m_sPlaySpeed(0)		
	,m_sSoundVolume(0)
	,m_ulContentsType(0)
	,m_ulContentsState(0)
	,m_ulRunningTime(0)
	,m_ulVolume(0)
	,m_strSaveKey("")
	,m_direction(0)
	,m_align(5)
	,m_width(0)
	,m_height(0)
	,m_currentComment(0)
	,m_strWizardXML("")
	,m_strWizardFiles("")
{
	for(int i=0; i<10; i++) m_strComment[i] = "";
}

void CEtcFile::Save(LPCSTR lpszFullPath)
{
	::WritePrivateProfileString(m_strSaveKey, "contentsId", m_strID.c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "contentsName", m_strContentsName.c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "location", m_strLocation.c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "filename", m_strFilename.c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "bgColor", m_strBGColor.c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "fgColor", m_strFGColor.c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "font", m_strFont.c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "promotionvalueList", m_strPromotionValueList.toString(), lpszFullPath);

	::WritePrivateProfileString(m_strSaveKey, "fontSize", ::ToString(m_sFontSize), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "playSpeed", ::ToString(m_sPlaySpeed), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "soundVolume", ::ToString(m_sSoundVolume), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "contentsType", ::ToString(m_ulContentsType), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "contentsState", ::ToString(m_ulContentsState), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "runningTime", ::ToString(m_ulRunningTime), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "volume", ::ToString(m_ulVolume), lpszFullPath);

	::WritePrivateProfileString(m_strSaveKey, "direction", ::ToString(m_direction), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "align", ::ToString(m_align), lpszFullPath);

	::WritePrivateProfileString(m_strSaveKey, "width", ::ToString(m_width), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "height", ::ToString(m_height), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "currentComment", ::ToString(m_currentComment), lpszFullPath);

	::WritePrivateProfileString(m_strSaveKey, "wizardXML", m_strWizardXML.c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "wizardFiles", m_strWizardFiles.c_str(), lpszFullPath);

	CString key, strTmp;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		key.Format("comment%d", i+1);
		strTmp = m_strComment[i].c_str();
		if(strTmp.GetAt(0) == ' ')
		{
			strTmp = "|" + strTmp;
			m_strComment[i] = strTmp;
		}//if

		WritePrivateProfileString(m_strSaveKey, key, m_strComment[i].c_str(), lpszFullPath);
	}//if
	/*
	::WritePrivateProfileString(m_strSaveKey, "comment1", m_strComment[0].c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "comment2", m_strComment[1].c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "comment3", m_strComment[2].c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "comment4", m_strComment[3].c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "comment5", m_strComment[4].c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "comment6", m_strComment[5].c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "comment7", m_strComment[6].c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "comment8", m_strComment[7].c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "comment9", m_strComment[8].c_str(), lpszFullPath);
	::WritePrivateProfileString(m_strSaveKey, "comment10", m_strComment[9].c_str(), lpszFullPath);
	*/
}

void CEtcFile::Load(LPCSTR lpszFullPath, LPCSTR lpszLoadID)
{
	char buf[BUF_SIZE];

	::GetPrivateProfileString(lpszLoadID, "contentsId", "", buf, BUF_SIZE, lpszFullPath);			m_strID = buf;
	::GetPrivateProfileString(lpszLoadID, "contentsName", "", buf, BUF_SIZE, lpszFullPath);			m_strContentsName = buf;
	::GetPrivateProfileString(lpszLoadID, "location", "", buf, BUF_SIZE, lpszFullPath);				m_strLocation = buf;
	::GetPrivateProfileString(lpszLoadID, "filename", "", buf, BUF_SIZE, lpszFullPath);				m_strFilename = buf;
	::GetPrivateProfileString(lpszLoadID, "bgColor", "", buf, BUF_SIZE, lpszFullPath);				m_strBGColor = buf;
	::GetPrivateProfileString(lpszLoadID, "fgColor", "", buf, BUF_SIZE, lpszFullPath);				m_strFGColor = buf;
	::GetPrivateProfileString(lpszLoadID, "font", "", buf, BUF_SIZE, lpszFullPath);					m_strFont = buf;
	::GetPrivateProfileString(lpszLoadID, "promotionvalueList", "", buf, BUF_SIZE, lpszFullPath);	m_strPromotionValueList.fromString(buf);

	::GetPrivateProfileString(lpszLoadID, "fontSize", "", buf, BUF_SIZE, lpszFullPath);				m_sFontSize = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "playSpeed", "", buf, BUF_SIZE, lpszFullPath);			m_sPlaySpeed = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "soundVolume", "", buf, BUF_SIZE, lpszFullPath);			m_sSoundVolume = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "contentsType", "", buf, BUF_SIZE, lpszFullPath);			m_ulContentsType = atol(buf);
	::GetPrivateProfileString(lpszLoadID, "contentsState", "", buf, BUF_SIZE, lpszFullPath);		m_ulContentsState = atol(buf);
	::GetPrivateProfileString(lpszLoadID, "runningTime", "", buf, BUF_SIZE, lpszFullPath);			m_ulRunningTime = atol(buf);
	//::GetPrivateProfileString(lpszLoadID, "volume", "", buf, BUF_SIZE, lpszFullPath);				m_ulVolume = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "volume", "", buf, BUF_SIZE, lpszFullPath);				m_ulVolume = (ULONGLONG)_atoi64(buf);

	::GetPrivateProfileString(lpszLoadID, "direction", "", buf, BUF_SIZE, lpszFullPath);			m_direction = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "align", "", buf, BUF_SIZE, lpszFullPath);				m_align = atoi(buf);

	::GetPrivateProfileString(lpszLoadID, "width", "", buf, BUF_SIZE, lpszFullPath);				m_width = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "height", "", buf, BUF_SIZE, lpszFullPath);				m_height = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "currentComment", "", buf, BUF_SIZE, lpszFullPath);		m_currentComment = atoi(buf);

	::GetPrivateProfileString(lpszLoadID, "wizardXML", "", buf, BUF_SIZE, lpszFullPath);			m_strWizardXML = buf;
	::GetPrivateProfileString(lpszLoadID, "wizardFiles", "", buf, BUF_SIZE, lpszFullPath);			m_strWizardFiles = buf;

	::GetPrivateProfileString(lpszLoadID, "comment1", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[0] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment2", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[1] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment3", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[2] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment4", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[3] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment5", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[4] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment6", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[5] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment7", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[6] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment8", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[7] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment9", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[8] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment10", "", buf, BUF_SIZE, lpszFullPath);			m_strComment[9] = buf;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠 키값을 만들어 반환 \n
/// @return <형: CString> \n
///			<CString: 생성된 컨텐츠의 키값> \n
/////////////////////////////////////////////////////////////////////////////////
CString CContents::GEtcontentsKey()
{
	CString strSaveID, strContentsKey;

	if(m_strFilename == "")
	{
		strSaveID.Format("Contents_%s", m_strContentsName);
	}
	else
	{
		int nIdx = m_strFilename.ReverseFind('.');
		if(nIdx != -1)
		{
			strContentsKey = m_strFilename.Left(nIdx);
		}//if
		strSaveID.Format("Contents_%s", strContentsKey);
	}//if

	return strSaveID;
}
*/