// Schedule.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "Host.h"
#include "Template.h"
#include "Frame.h"
#include "Schedule.h"
#include "VertDraw.h"

#include "MemDC.h"
#include <math.h>

#include <tlhelp32.h>


//#define		USE_MMTIMER		// 멀티미디어 타이머 사용


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSchedule

#define		BUFFER_SIZE					32768
#define		TIMER_MINIMUM_TIME			15
#define		VIDEO_CAPTION_SHIFT_STEP	2
#define		HORIZONTAL_TICKER_CAPTION_SHIFT_STEP	1
#define		VERTICAL_TICKER_CAPTION_SHIFT_STEP		1

#define		SCHEDULE_SHOW_ID			4000	// id
#define		SCHEDULE_SHOW_TIME			100		// time(ms)

#define		SCHEDULE_HIDE_ID			4001	// id
#define		SCHEDULE_HIDE_TIME			100		// time(ms)

#define		CAPTION_SHIFT_ID			4002	// id
#define		CAPTION_SHIFT_TIME			25		// time(ms)

#define		REFRESH_CONTENTS_ID			4003	// id
#define		REFRESH_CONTENTS_TIME		(60*60*1000)		// time(ms)

#define		CAPTION_NEXT_ID				4004	// id
#define		CAPTION_NEXT_TIME			(3*1000)// time(ms) -> 3초후에 다음 항목으로 이동

#define		CAPTION_CLEAR_ID			4005	// id
#define		CAPTION_CLEAR_TIME			(1*1000)// time(ms) -> 1초간 빈화면

#define		ID_SLIDE_SHOW_END			4010		//슬라이드 쇼 종료 타이머 ID
#define		TIME_SLIDE_SHOW_END			500			//슬라이드 쇼 종료 타이머 시간
//#define		TIME_SLIDE_SHOW_END			10			//슬라이드 쇼 종료 타이머 시간

//#define		ID_PRESENTAION_END			4011		//Presentation 종료 타이머 ID
//#define		TIME_PRESENTAION_END		5000		//Presentation 종료 타이머 시간
//#define		TIME_PRESENTAION_END		10			//Presentation 종료 타이머 시간

#define		ID_PRESENTAION_SHOW			4012		//Presentation Show 타이머 ID
#define		TIME_PRESENTAION_SHOW		5000		//Presentation Show 타이머 시간
//#define		TIME_PRESENTAION_SHOW		10			//Presentation Show 타이머 시간


//컨텐츠 파일이 경로에 없을 경우에 스케줄을 노출 시키는 시간
#define		TIME_CONTENTS_NO_EXIST		15					//15 sec

#define		TIME_PPT_PLAYING			(86400)			//PPT Schedule의 Play time	
#define		TIME_PPT_NONE				(10000)			//PPT가 설치되지 않았을 때 Play time
//#define		TIME_PPT_PLAYING			(87)			//PPT Schedule의 Play time	
//#define		TIME_PPT_NONE				(10)			//PPT가 설치되지 않았을 때 Play time


//동영상 화면갱신 타이머
#define		ID_VIDEO_REPAINT			4014
#define		TIME_VIDEO_REPAINT			1000

//동영상 재생종료 타이머
#define		ID_VIDEO_PLAYEND			4015
#define		TIME_VIDEO_PLAYEND			300


#define		MAX_INTERACT_LOG			5			//상호작용 통계의 단계 최대값

	


UINT	CSchedule::m_nScheduleID = 0xf000;

CMapStringToPtr CSchedule::m_mapSchedule;


CSchedule* CSchedule::GetScheduleObject(CTemplate* pParentParent, CFrame* pParent, LPCSTR lpszID, CRect& rect, bool bDefaultSchedule, CString& strErrorMessage)
{
	ciLong	contentsType = atoi(MNG_PROFILE_READ(lpszID, "contentsType"));

	__DEBUG__("\t\t\tCreate Schedule", lpszID);

	switch(contentsType)
	{
	case CONTENTS_VIDEO: // Video
		{
			CVideoSchedule* schedule = new CVideoSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "VideoSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Video Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_SMS: // SMS
		{
			// 임시 막아둠 (SMS -> HorizontalTicker)

			//CSMSSchedule* schedule = new CSMSSchedule(pParentParent, pParent, reply, bDefaultSchedule);
			//if(schedule == NULL) return NULL;
			//BOOL ret = schedule->Create(NULL, "SMSSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			//if(ret == NULL)
			//{
			//	strErrorMessage = "Fail to create SMS Schedule !!!";
			//	delete schedule;
			//	schedule = NULL;
			//}
			//return schedule;

			CHorizontalTickerSchedule* schedule = new CHorizontalTickerSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "HorizontalTickerSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create HorizontalTicker Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_IMAGE: // Image
		{
			CPictureSchedule* schedule = new CPictureSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "ImageSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Image Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_TV: // TV
		{
			CTVSchedule* schedule = new CTVSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "TVSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create TV Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_TICKER: // Ticker
		{
			CVerticalTickerSchedule* schedule = new CVerticalTickerSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "VerticalTickerSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create VerticalTicker Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_WEBBROWSER: // URL WebBrowser (URL 직접navigate)
		{
			CURLSchedule* schedule = new CURLSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "URLSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create URL Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_FLASH: // FLASH
	case CONTENTS_FLASH_TEMPLATE:
	case CONTENTS_DYNAMIC_FLASH:
		{
			CFlashSchedule* schedule = new CFlashSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "FlashSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Flash Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_RSS:
		{
			CRSSSchedule* schedule = new CRSSSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "RSSSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create RSS Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_TEXT:
		{
			CSMSSchedule* schedule = new CSMSSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "SMSSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create SMS Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_TYPING:
		{
			CTypingTickerSchedule* schedule = new CTypingTickerSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "TypingTickerSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create TypingTicker Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_PPT:		//MS-Office Power Point
		{
			//
			KillPowerpoint();

			//
			CPowerPointSchedule* schedule = new CPowerPointSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "PowerPointSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Power Point Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_WIZARD:	//Wizard
		{
			CWizardSchedule* schedule = new CWizardSchedule(pParentParent, pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "WizardSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Wizard Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	default:
		strErrorMessage = "Uknown ContentsType Contents !!!";
		break;
	}

	return NULL;
}

void CSchedule::KillPowerpoint()
{
	//{// skpark 2012.9.6 임시 테스트 코드
	// 0. 처음 한번에 한해서
	// 1. +replace 이고
	// 2. 자기 자신이 POWERPNT 를 가지고 있으며
	// 3. 앞선 브라우저도 POWERPNT 를 가지고 있으며
	// 4. 파워 포인트가 살아 있으며,,,,,
	// 5. 내가 아닌 다른 브라우저가 살아있다면
	// 6. 할 수 없이 앞선 브라우저를 먼저 죽인다.

	static bool run_once = false; //처음 딱 1번만 한다.
	if( run_once ) return;

	// +replace 설정때만 ppt강제종료
	if( !ciArgParser::getInstance()->isSet("+replace") ) return;

	run_once = true;
	scratchUtil* aUtil = scratchUtil::getInstance();

	//실행중인 파워포인트가 있는지 본다.
	__DEBUG__("skpark Terminate Powerpoint", _NULL);
	ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
	__DEBUG__("skpark Powerpoint PID", (unsigned int)ulPid);
	if( ulPid == 0 ) return; // 파워포인트 프로세스 없음

	// skpark 파워포인트가 살아있다면, 브라우저도 있을 살아있기 때문일 가능성이 높다.
	// 브라우저를 죽이지 않는다면, 파워포인트 또한 깨끗하게 죽지를 못한다.
	// 이 경우 모니터가 2개 짜리가 아니라면,  브라우저까지 죽인다.
	int displayCounter = scratchUtil::getInstance()->getMonitorCount();

	//if(displayCounter > 1) return; // 듀얼디스플레이일때 브라우저 안죽임 (why?)

	// 이미 떠있는 브라우저 강제종료
	CString strTerminateApp = ::AfxGetAppName();
	strTerminateApp.Append(".exe");
	__DEBUG__("skpark TerminateApp", strTerminateApp);
	int nMyPid = _getpid();
	__DEBUG__("skpark My PID", nMyPid);
	int nRet = aUtil->ExclusiveKillProcess(strTerminateApp, (ULONG)nMyPid, 10*1000);  // 10초나 준다. PPT 가 잘안죽기 때문에.
	__DEBUG__("skpark ExclusiveKillProcess", nRet);

	// 파워포인트 다시 강제종료
	bool powerpnt_alive = true;
	for(int i=0; i<30 && powerpnt_alive; i++)
	{
		// 파워 포인트가 확실하게 죽었는지 30초동안 확인한다
		ulPid = aUtil->getPid("POWERPNT.EXE");
		if( ulPid <= 0 )
		{
			powerpnt_alive = false;						
			break;
		}

		__DEBUG__("skpark scratchUtil : POWERPNT still alive", (unsigned int)ulPid);
		aUtil->killProcess(ulPid);  
		::Sleep(1000);
	}

	if( powerpnt_alive )
	{
		ulPid = aUtil->getPid("POWERPNT.EXE");
		if( ulPid > 0 )
		{
			// 징하다 아직도 안죽었다.
			__DEBUG__("skpark scratchUtil : POWERPNT still, still, still alive", (unsigned int)ulPid);
			// 그렇다면 제거한다.  제발좀 죽어라..
			aUtil->killProcess(ulPid);  
		}
	}
}

//////////////////////////////////////////////////////////////////////////////


CSchedule::CSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	m_bDefaultSchedule(bDefaultSchedule)
,	m_bOpen(false)
,	m_bMute(false)
,	m_bPlay(false)
,	m_pParentParentWnd(pParentParentWnd)
,	m_pParentWnd(pParentWnd)
,	m_strMediaFullPath ("")
,	m_strErrorMessage ("")
,	m_bLoadFromLocal (true)
,	m_strLoadID (lpszID)
,	m_nPlayCount (0)
,	m_siteId ("00")
,	m_hostId ("00")
,	m_templateId ("00")
,	m_frameId ("00")
,	m_scheduleId ("00")
,	m_startTime ("00:00:00")
,	m_endTime ("00:00:01")
,	m_playOrder (0)
,	m_timeScope (0)
,	m_openningFlag (ciFalse)
,	m_priority (0)
,	m_castingState (0)
,	m_operationalState (0)
,	m_adminState (ciFalse)
,	m_parentScheduleId ("")
,	m_contentsId ("00")
,	m_contentsName ("")
,	m_contentsType (0)
,	m_contentsState (0)
,	m_location ("")
,	m_filename ("")
,	m_volume (0)
,	m_runningTime (1)
,	m_bFileNotExist(false)
,	m_dwStartTick(0)
,	m_bIsHDSchedule(false)
#ifdef _FELICA_
,	m_bFelicaSet(false)
#endif
,	m_touchTime("")
,	m_pPrevPlayedSchedule ( NULL )
{
	SetSchedule(lpszID);

	CSchedule::m_mapSchedule.SetAt(m_scheduleId.c_str(), this);

	CLogDialog::getInstance()->Add(m_pParentWnd, this, m_scheduleId.c_str(), 8 + m_contentsType*2);
}

CSchedule::~CSchedule()
{
	__DEBUG__("\t\t\tDestroy Schedule", m_scheduleId.c_str());

	//CloseFile();

	CSchedule::m_mapSchedule.RemoveKey(m_scheduleId.c_str());

	CLogDialog::getInstance()->Remove(this);
}

IMPLEMENT_DYNAMIC(CSchedule, CWnd)

BEGIN_MESSAGE_MAP(CSchedule, CWnd)
	ON_WM_TIMER()
	ON_MESSAGE(WM_DEBUG_STRING, OnDebugString)
	ON_MESSAGE(WM_DEBUG_GRID, OnDebugGrid)
	ON_MESSAGE(WM_ERROR_MESSAGE, OnErrorMessage)
	ON_MESSAGE(WM_HIDE_CONTENTS, OnHideContents)
	ON_WM_DESTROY()
END_MESSAGE_MAP()



void CSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CWnd::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}


void CSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	__DEBUG__("\t\t\tSet Schedule", lpszID);

	m_siteId = MNG_PROFILE_READ(lpszID, "siteId");
	m_hostId = MNG_PROFILE_READ(lpszID, "hostId");
	m_templateId = MNG_PROFILE_READ(lpszID, "templateId");
	m_frameId = MNG_PROFILE_READ(lpszID, "frameId");
	m_scheduleId = MNG_PROFILE_READ(lpszID, "scheduleId");
	m_startDate = atoi(MNG_PROFILE_READ(lpszID, "startDate"));
	m_endDate = atoi(MNG_PROFILE_READ(lpszID, "endDate")); 
//skpark 2012.11.08
// Default Schedule 의 경우에 한해서,
//DONT_IGNORE_SCHEDULE_ENDDATE Flag 가 셋팅되어 있지 않다면
//endDate 값을 현재시간보다 2030년 더 뒤 값으로 만들어서 무력화 시킨다.
	if(m_bDefaultSchedule && DONT_IGNORE_SCHEDULE_ENDDATE() == ciFalse )
	{
		ciTime aYearAfter("2030/12/31 00:00:00");
		if(m_endDate < aYearAfter)
		{
			m_endDate = aYearAfter;
		}
	}
// skpark end

	m_startTime = MNG_PROFILE_READ(lpszID, "startTime");
	m_endTime = MNG_PROFILE_READ(lpszID, "endTime");
	m_playOrder = atoi(MNG_PROFILE_READ(lpszID, "playOrder"));
	m_timeScope = atoi(MNG_PROFILE_READ(lpszID, "timeScope"));
	m_openningFlag = (bool)atoi(MNG_PROFILE_READ(lpszID, "openningFlag"));
	m_priority = atoi(MNG_PROFILE_READ(lpszID, "priority"));
	m_castingState = atoi(MNG_PROFILE_READ(lpszID, "castingState"));
	m_operationalState = (bool)atoi(MNG_PROFILE_READ(lpszID, "operationalState"));
	m_adminState = (bool)atoi(MNG_PROFILE_READ(lpszID, "adminState"));
	m_parentScheduleId = MNG_PROFILE_READ(lpszID, "parentScheduleId");
	m_contentsId = MNG_PROFILE_READ(lpszID, "contentsId");
	m_contentsName = MNG_PROFILE_READ(lpszID, "contentsName");
	m_contentsType = atoi(MNG_PROFILE_READ(lpszID, "contentsType"));
	m_contentsState = atoi(MNG_PROFILE_READ(lpszID, "contentsState"));
	m_location = MNG_PROFILE_READ(lpszID, "location");
	
	int nLength = m_location.length();
	if(nLength != 0 && m_location[nLength-1] != '/' && m_location[nLength-1] != '\\')
	{
		m_location += '/';
	}//if

	m_filename = MNG_PROFILE_READ(lpszID, "filename");
	m_volume = (ULONGLONG)_atoi64(MNG_PROFILE_READ(lpszID, "volume"));
	m_runningTime = atol(MNG_PROFILE_READ(lpszID, "runningTime"));
	m_touchTime = MNG_PROFILE_READ(lpszID, "touchTime");

	CString key, strTmp, strSpace;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		key.Format("comment%d", i+1);
		strTmp = MNG_PROFILE_READ(lpszID, key);
		strSpace = strTmp.Left(2);
		if(strSpace == "| ")	//첫번째 문자가 공백일경우 ini에서 읽을때 사라지므로 "|"를 붙였다.
		{
			strTmp.Delete(0, 1);
		}//if
		m_comment[i] = strTmp;
	}//for

	if(m_bDefaultSchedule)
	{
		m_strStartTime = m_startTime.c_str();
		m_strEndTime = m_endTime.c_str();

		ciInt nY = m_startDate.getYear();
		nY = (nY <= 1970 ? 1971 : nY);		//1970 을 그대로 CTime에 넣으면 오류발생

		ciInt nM = m_startDate.getMonth();
		nM = (nM <= 0 ? 1 : nM);			//0 을 그대로 CTime에 넣으면 오류발생

		ciInt nD = m_startDate.getDay();
		nD = (nD <= 0 ? 1 : nD);			//0 을 그대로 CTime에 넣으면 오류발생

		CTime start_date(nY, nM, nD, 0, 0, 0);
		m_timeStartDate = start_date;


		nY = m_endDate.getYear();
		nY = (nY <= 1970 ? 1971 : nY);		//1970 을 그대로 CTime에 넣으면 오류발생

		nM = m_endDate.getMonth();
		nM = (nM <= 0 ? 1 : nM);			//0 을 그대로 CTime에 넣으면 오류발생

		nD = m_endDate.getDay();
		nD = (nD <= 0 ? 1 : nD);			//0 을 그대로 CTime에 넣으면 오류발생

		CTime end_date(nY, nM, nD, 23, 59, 59);
		m_timeEndDate = end_date;
	}
	else
	{
		m_strStartTime = m_startTime.c_str();
		m_strEndTime = m_endTime.c_str();

		ciInt nY = m_startDate.getYear();
		nY = (nY <= 1970 ? 1971 : nY);		//1970 을 그대로 CTime에 넣으면 오류발생

		ciInt nM = m_startDate.getMonth();
		nM = (nM <= 0 ? 1 : nM);			//0 을 그대로 CTime에 넣으면 오류발생

		ciInt nD = m_startDate.getDay();
		nD = (nD <= 0 ? 1 : nD);			//0 을 그대로 CTime에 넣으면 오류발생

		int nHour, nMin, nSec;
		nHour = atoi(m_strStartTime.Left(2));
		nMin = atoi(m_strStartTime.Mid(3, 2));
		nSec = atoi(m_strStartTime.Right(2));

		CTime start_date(nY, nM, nD, nHour, nMin, nSec);
		m_timeStartDate = start_date;


		nY = m_endDate.getYear();
		nY = (nY <= 1970 ? 1971 : nY);		//1970 을 그대로 CTime에 넣으면 오류발생

		nM = m_endDate.getMonth();
		nM = (nM <= 0 ? 1 : nM);			//0 을 그대로 CTime에 넣으면 오류발생

		nD = m_endDate.getDay();
		nD = (nD <= 0 ? 1 : nD);			//0 을 그대로 CTime에 넣으면 오류발생

		nHour = atoi(m_strEndTime.Left(2));
		nMin = atoi(m_strEndTime.Mid(3, 2));
		nSec = atoi(m_strEndTime.Right(2));

		CTime end_date(nY, nM, nD, nHour, nMin, nSec);
		m_timeEndDate = end_date;
	}//if

	CString str_include = MNG_PROFILE_READ(lpszID, "includecategory");
	str_include.Trim();
	m_includeCategory = (LPCSTR)str_include;
	if( str_include.GetLength() > 0 )
	{
		m_mapInclude.RemoveAll();

		int pos = 0;
		CString str_token = str_include.Tokenize(",", pos);
		while( str_token != "" )
		{
			CString str_token_lower = str_token;
			str_token_lower.MakeLower();
			m_mapInclude.SetAt(str_token_lower, str_token);

			str_token = str_include.Tokenize(",", pos);
		}
	}

	CString str_exclude = MNG_PROFILE_READ(lpszID, "excludecategory");
	str_exclude.Trim();
	m_excludeCategory = (LPCSTR)str_exclude;
	if( str_exclude.GetLength() > 0 )
	{
		m_mapExclude.RemoveAll();

		int pos = 0;
		CString str_token = str_exclude.Tokenize(",", pos);
		while( str_token != "" )
		{
			CString str_token_lower = str_token;
			str_token_lower.MakeLower();
			m_mapExclude.SetAt(str_token_lower, str_token);

			str_token = str_exclude.Tokenize(",", pos);
		}
	}

	__DEBUG_SCHEDULE_END__(lpszID)
}


bool CSchedule::IsUseFile()
{
	if(	m_contentsType == 4		// TV 제외
	||	m_contentsType == 7		// URL 제외
		)
		return false;

	return true;
}

bool CSchedule::IsBetween()
{
	if(m_bOpen == false) return false;
	if(m_adminState == false) return false;
	if(m_castingState != SCH_READY) return false;
	if(m_contentsState != CON_READY) return false;

	CTime current_time = ::GetHostTime();
	//CString strTmCurrent = current_time.Format("%Y-%m-%d %H:%M:%S");
	//__DEBUG__(strTmCurrent, _NULL);

	if(m_bDefaultSchedule)
	{
		if(m_parentScheduleId.length() > 0)
		{
			CSchedule* schedule;
			if(CSchedule::m_mapSchedule.Lookup(m_parentScheduleId.c_str(), (void*&)schedule))
			{
				return schedule->IsPlay() && schedule->IsPlayableCategory();
			}
			return false;
		}
		else if(m_timeStartDate <= current_time && current_time <= m_timeEndDate)
		{
			int hour = current_time.GetHour();

			switch(m_timeScope)
			{
			case 0: // 24
				return IsPlayableCategory();
				break;

			case 7: // 0-7
				if(0 <= hour && hour < 7)
					return IsPlayableCategory();
				break;

			case 1: // 7-9
				if(7 <= hour && hour < 9)
					return IsPlayableCategory();
				break;

			case 2: // 9-12
				if(9 <= hour && hour < 12)
					return IsPlayableCategory();
				break;

			case 3: // 12-13
				if(12 <= hour && hour < 13)
					return IsPlayableCategory();
				break;

			case 4: // 13-18
				if(13 <= hour && hour < 18)
					return IsPlayableCategory();
				break;

			case 5: // 18-22
				if(18 <= hour && hour < 22)
					return IsPlayableCategory();
				break;

			case 6: // 22-24
				if(22 <= hour)
					return IsPlayableCategory();
				break;
			}//switch
		}//if
	}
	else
	{
		//순환의 개념
		if(m_timeStartDate <= current_time && current_time <= m_timeEndDate)
		{
			CString str_currenttime = current_time.Format("%H:%M:%S");

			if(m_strStartTime < m_strEndTime)
			{
				if(m_strStartTime <= str_currenttime && str_currenttime <= m_strEndTime)
					return IsPlayableCategory();
			}
			else
			{
				if(m_strStartTime <= str_currenttime || str_currenttime <= m_strEndTime)
					return IsPlayableCategory();
			}//if
		}//if
		
		/* 지속의 개념
		if(m_timeStartDate <= current_time && current_time <= m_timeEndDate)
		{
			return true;
		}//if
		*/
	}//if

	return false;
}


bool CSchedule::CloseFile()
{
	Stop();

	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if

	return true;
}


bool CSchedule::Play()
{
	__DEBUG__("\t\t\tPlay Schedule", m_strMediaFullPath);

	CLogDialog::getInstance()->Play(this);

	m_nPlayCount ++;

	KillTimer(SCHEDULE_HIDE_ID);
	SetTimer(SCHEDULE_SHOW_ID, SCHEDULE_SHOW_TIME, NULL);
	m_bPlay = true;
	if(::GetScheduleResuming())
	{
		if(m_pParentWnd && m_pParentWnd->GetGrade()==1)
			CHost::PushFrontToShowWindow(GetSafeHwnd());
		else
			CHost::PushBackToShowWindow(GetSafeHwnd());
	}

#ifdef _FELICA_
	switch(m_contentsType)
	{
	case CONTENTS_VIDEO:
	case CONTENTS_IMAGE:
	case CONTENTS_WEBBROWSER:
	case CONTENTS_FLASH:
	//case CONTENTS_RSS:
	case CONTENTS_WIZARD:
		{
			CString strFelica = m_comment[1].c_str();
			strFelica = strFelica.Left(4);
			if(strFelica.CompareNoCase("http") == 0)
			{
				__DEBUG__("FELICA - setURL", m_comment[1].c_str());
				felicaUtil::getInstance()->setURL(m_comment[1].c_str());
				m_bFelicaSet = true;
			}//if
		}
		break;
	}//switch
#endif

	switch(m_contentsType)
	{
	case CONTENTS_SMS:
	case CONTENTS_TICKER:
	case CONTENTS_WEBBROWSER:
	case CONTENTS_RSS:
	case CONTENTS_TEXT:
	case CONTENTS_TYPING:
		{
			//file name은 중요한 데이터가 아니므로 comment1을 255자까지 로그로 기록한다.
			CString strLog;
			for(int i=0; i<TICKER_COUNT; i++)
			{
				strLog += m_comment[i].c_str();
			}//for

			if(strLog.GetLength() > 255)
			{
				strLog = strLog.Left(255);
			}//if
			
			WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"TRY", m_runningTime, strLog, "", "", m_pParentParentWnd->GetTemplateIndex());
		}
		break;
	default:
		{
			WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"TRY", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());
		}
	}//switch

	return true;
}


bool CSchedule::Stop()
{
	__DEBUG__("\t\t\tStop Schedule", m_strMediaFullPath);

	CLogDialog::getInstance()->Stop(this);

	KillTimer(SCHEDULE_SHOW_ID);
	SetTimer(SCHEDULE_HIDE_ID, SCHEDULE_HIDE_TIME, NULL);
	m_bPlay = false;
	if(::GetScheduleResuming())
		CHost::PushBackToHideWindow(GetSafeHwnd());

#ifdef _FELICA_
	switch(m_contentsType)
	{
	case CONTENTS_VIDEO:
	case CONTENTS_IMAGE:
	case CONTENTS_WEBBROWSER:
	case CONTENTS_FLASH:
	//case CONTENTS_RSS:
	case CONTENTS_WIZARD:
		{
			if(m_bFelicaSet)
			{
				__DEBUG__("FELICA - unsetURL", _NULL);
				felicaUtil::getInstance()->unsetURL();
				m_bFelicaSet = false;
			}//if
		}
		break;
	}//switch
#endif

	switch(m_contentsType)
	{
	case CONTENTS_SMS:
	case CONTENTS_TICKER:
	case CONTENTS_WEBBROWSER:
	case CONTENTS_TEXT:
	case CONTENTS_TYPING:
		{
			//file name은 중요한 데이터가 아니므로 comment1을 255자까지 로그로 기록한다.
			CString strLog;
			for(int i=0; i<TICKER_COUNT; i++)
			{
				strLog += m_comment[i].c_str();
			}//for

			if(strLog.GetLength() > 255)
			{
				strLog = strLog.Left(255);
			}//if

			WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"END", m_runningTime, strLog, "", "", m_pParentParentWnd->GetTemplateIndex());
		}
		break;
	default:
		{
			WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"END", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());
		}
	}//switch

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 스케줄을 처음부터 다시 재생한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CSchedule::RePlay()
{
	//__DEBUG__("\t\t\tReplay Schedule", m_strMediaFullPath);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// SCheduel의 시작 시간을 구한다. \n
/// @param (time_t&) tmStart : (out) Schedule의 시작 시간
/////////////////////////////////////////////////////////////////////////////////
void CSchedule::GetStartTime(time_t& tmStart)
{
	int nYear, nMonth, nDay;
	nYear = m_timeStartDate.GetYear();
	nMonth = m_timeStartDate.GetMonth();
	nDay = m_timeStartDate.GetDay();

	int nHour, nMin, nSec;
	if(m_strStartTime.GetLength() != 8)
	{
		nHour = nMin = nSec = 0;
	}
	else
	{
		nHour = atoi(m_strStartTime.Left(2));
		nMin = atoi(m_strStartTime.Mid(3, 2));
		nSec = atoi(m_strStartTime.Right(2));
	}//if

	CTime tmTmp(nYear, nMonth, nDay, nHour, nMin, nSec);
	tmStart = tmTmp.GetTime();	
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠 파일이 없는 상태를 설정한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CSchedule::SetNoContentsFile()
{
	CString strError;
	strError.Format("Contents file[%s] not exist", m_strMediaFullPath);
	__DEBUG__("skpark 2012 12.17 \t\t\t", strError);
	OnErrorMessage((WPARAM)(LPCSTR)strError, 0);
	
	// skpark 2012.12.18
	// playList 가 2개 이상인 경우는 엑박이 나오지 않고 그냥 패스해야 한다.
	// 단, playList=1 이고, primary 가 아니면서, 다분할 화면인경우는 엑박이 나와야 한다.
	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;

	if((m_pParentWnd->GetDefaultScheduleCount() > 1 || pclsHost->GetCountOfTemplatePlayAry() > 1) && 
	!(m_pParentWnd->GetDefaultScheduleCount()==1 && m_pParentParentWnd->GetFrameCount() > 1 && m_pParentWnd->GetGrade() != 1) ) 
	{	
		__DEBUG__("skpark 2012 12.17 \t\t\tThis should be pass,,,", m_strMediaFullPath);
		m_bOpen = false;
		m_bFileNotExist = true;

	}else{
		//컨텐츠 파일이 없는 경우 액박 표시해 준다.
		HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_XBOX));
		m_bgImage.CreateFromHBITMAP(hBitmap);
		m_bOpen = true;
		m_bFileNotExist = true;
		if(m_runningTime == 0)
		{
			m_runningTime = TIME_CONTENTS_NO_EXIST;		//컨텐츠의 재생 시간을 조정
		}//if
	}
}


void CSchedule::OnTimer(UINT_PTR nIDEvent)
{
	CWnd::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case SCHEDULE_SHOW_ID:
		{
			KillTimer(SCHEDULE_SHOW_ID);
			__DEBUG__("Show Window", m_contentsName.c_str());
			if(!::GetScheduleResuming())
				ShowWindow(SW_SHOW);
			if( m_pPrevPlayedSchedule )
			{
				__DEBUG__("Hide Window(Ex)", _NULL);
				__DEBUG__("Hide Window(Ex)", m_pPrevPlayedSchedule->m_contentsName.c_str());
				if(!::GetScheduleResuming())
					m_pPrevPlayedSchedule->ShowWindow(SW_HIDE);
				m_pPrevPlayedSchedule = NULL;
			}
		}
		break;

	case SCHEDULE_HIDE_ID:
		{
			KillTimer(SCHEDULE_HIDE_ID);
			__DEBUG__("Hide Window", m_contentsName.c_str());
			if(!::GetScheduleResuming())
				ShowWindow(SW_HIDE);
		}
		break;
	}//switch
}


CString	CSchedule::ToString()
{
	CString str;
	if(m_bDefaultSchedule)
	{
		str.Format(
			"siteId = %s\r\n"
			"hostId = %s\r\n"
			"templateId = %s\r\n"
			"frameId = %s\r\n"
			"scheduleId = %s\r\n"
			"startDate = %s\r\n"
			"endDate = %s\r\n"
			"playOrder = %d\r\n"
			"timeScope = %d\r\n"
			"openningFlag = %d\r\n"
			"priority = %d\r\n"
			"castingState = %d\r\n"
			"operationalState = %d\r\n"
			"adminState = %d\r\n"
			"parentScheduleId = %s\r\n"
			"contentsId = %s\r\n"
			"contentsName = %s\r\n"
			"contentsType = %d\r\n"
			"contentsState = %d\r\n"
			"location = %s\r\n"
			"filename = %s\r\n"
			"volume = %I64u\r\n"
			"runningTime = %ld\r\n"
			"m_includeCategory = %s\r\n"
			"m_excludeCategory = %s\r\n"
			,	m_siteId.c_str()
			,	m_hostId.c_str()
			,	m_templateId.c_str()
			,	m_frameId.c_str()

		// schedule attributes
			,	m_scheduleId.c_str()
			,	m_startDate.getTimeString().c_str()
			,	m_endDate.getTimeString().c_str()
			,	m_playOrder
			,	m_timeScope
			,	m_openningFlag
			,	m_priority
			,	m_castingState
			,	m_operationalState
			,	m_adminState
			,	m_parentScheduleId.c_str()

		// contents attributes
			,	m_contentsId.c_str()
			,	m_contentsName.c_str()
			,	m_contentsType
			,	m_contentsState
			,	m_location.c_str()
			,	m_filename.c_str()
			,	m_volume // filesize
			,	m_runningTime
			,	m_includeCategory.c_str()
			,	m_excludeCategory.c_str()
		);
	}
	else
	{
		str.Format(
			"siteId = %s\r\n"
			"hostId = %s\r\n"
			"templateId = %s\r\n"
			"frameId = %s\r\n"
			"scheduleId = %s\r\n"
			"startDate = %s\r\n"
			"endDate = %s\r\n"
			"startTime = %s\r\n"
			"endTime = %s\r\n"
			"openningFlag = %d\r\n"
			"priority = %d\r\n"
			"castingState = %d\r\n"
			"operationalState = %d\r\n"
			"adminState = %d\r\n"
			"contentsId = %s\r\n"
			"contentsName = %s\r\n"
			"contentsType = %d\r\n"
			"contentsState = %d\r\n"
			"location = %s\r\n"
			"filename = %s\r\n"
			"volume = %I64u\r\n"
			"runningTime = %ld\r\n"
			"m_includeCategory = %s\r\n"
			"m_excludeCategory = %s\r\n"
			,	m_siteId.c_str()
			,	m_hostId.c_str()
			,	m_templateId.c_str()
			,	m_frameId.c_str()

		// schedule attributes
			,	m_scheduleId.c_str()
			,	m_startDate.getTimeString().c_str()
			,	m_endDate.getTimeString().c_str()
			,	m_startTime.c_str()
			,	m_endTime.c_str()
			,	m_openningFlag
			,	m_priority
			,	m_castingState
			,	m_operationalState
			,	m_adminState

		// contents attributes
			,	m_contentsId.c_str()
			,	m_contentsName.c_str()
			,	m_contentsType
			,	m_contentsState
			,	m_location.c_str()
			,	m_filename.c_str()
			,	m_volume // filesize
			,	m_runningTime
			,	m_includeCategory.c_str()
			,	m_excludeCategory.c_str()
		);
	}

	CString tmp;
	tmp.Format(
		"\r\n"
		"m_bDefaultSchedule = %d\r\n"
		"m_bOpen = %d\r\n"
		"m_bMute = %d\r\n"
		"m_bPlay = %d\r\n"
		"m_strMediaFullPath = %s\r\n"
		,	m_bDefaultSchedule
		,	m_bOpen
		,	m_bMute
		,	m_bPlay
		,	m_strMediaFullPath
	);

	str.Append(tmp);

	return str;
}

LRESULT CSchedule::OnDebugString(WPARAM wParam, LPARAM lParam)
{
	m_debugString = ToString();

	return (LRESULT)(LPCTSTR)m_debugString;
}

LRESULT CSchedule::OnDebugGrid(WPARAM wParam, LPARAM lParam)
{
	CPropertyGrid* grid = (CPropertyGrid*)wParam;

	ToGrid(grid);

	return 0;
}

void CSchedule::ToGrid(CPropertyGrid* pGrid)
{
	pGrid->ResetContents();

	HSECTION hs = pGrid->AddSection( ( m_bDefaultSchedule ? "DEFAULT SCHEDULE Attributes" : "SCHEDULE Attributes" ) );

	//pGrid->AddStringItem(hs, "siteId", m_siteId.c_str());
	//pGrid->AddStringItem(hs, "hostId", m_hostId.c_str());
	pGrid->AddStringItem(hs, "templateId", m_templateId.c_str());
	pGrid->AddStringItem(hs, "frameId", m_frameId.c_str());
	pGrid->AddStringItem(hs, "scheduleId", m_scheduleId.c_str());
	pGrid->AddStringItem(hs, "startDate", m_startDate.getTimeString().c_str());
	pGrid->AddStringItem(hs, "endDate", m_endDate.getTimeString().c_str());

	if(m_bDefaultSchedule)
	{
		pGrid->AddIntegerItem(hs, "playOrder", m_playOrder);
		pGrid->AddIntegerItem(hs, "timeScope", m_timeScope);
	}
	else
	{
		pGrid->AddStringItem(hs, "startTime", m_startTime.c_str());
		pGrid->AddStringItem(hs, "endTime", m_endTime.c_str());
	}

	pGrid->AddBoolItem   (hs, "openningFlag", m_openningFlag);
	pGrid->AddIntegerItem(hs, "priority", m_priority);
	pGrid->AddIntegerItem(hs, "castingState", m_castingState);
	pGrid->AddBoolItem   (hs, "operationalState", m_operationalState);
	pGrid->AddBoolItem   (hs, "adminState", m_adminState);
	pGrid->AddStringItem (hs, "parentScheduleId", m_parentScheduleId.c_str());
	pGrid->AddStringItem (hs, "contentsId", m_contentsId.c_str());
	pGrid->AddStringItem (hs, "contentsName", m_contentsName.c_str());
	pGrid->AddStringItem(hs, "contentsType", GetContentsTypeString());
	pGrid->AddIntegerItem(hs, "contentsState", m_contentsState);
	pGrid->AddStringItem (hs, "location", m_location.c_str());
	pGrid->AddStringItem (hs, "filename", m_filename.c_str());
	//pGrid->AddIntegerItem(hs, "volume", m_volume);
	pGrid->AddIntegerItem(hs, "runningTime", m_runningTime);
	pGrid->AddStringItem(hs, "includeCategory", m_includeCategory.c_str());
	pGrid->AddStringItem(hs, "excludeCategory", m_excludeCategory.c_str());

	hs = pGrid->AddSection("member variables");

	pGrid->AddStringItem(hs, "ERROR MESSAGE", (LPCSTR)m_strErrorMessage);
	pGrid->AddBoolItem  (hs, "m_bDefaultSchedule", m_bDefaultSchedule);
	pGrid->AddBoolItem  (hs, "m_bOpen", m_bOpen);
	pGrid->AddBoolItem  (hs, "m_bMute", m_bMute);
	pGrid->AddBoolItem  (hs, "m_bPlay", m_bPlay);
	pGrid->AddStringItem(hs, "m_strMediaFullPath", (LPCSTR)m_strMediaFullPath);
	pGrid->AddBoolItem  (hs, "m_bLoadFromLocal", m_bLoadFromLocal);
	pGrid->AddIntegerItem(hs, "m_nPlayCount", m_nPlayCount);
	pGrid->AddBoolItem(hs, "m_bIsHDSchedule", m_bIsHDSchedule);
}

LRESULT CSchedule::OnErrorMessage(WPARAM wParam, LPARAM lParam)
{
	if(m_strErrorMessage.GetLength() > 0) m_strErrorMessage.Append(";");
	if(m_strErrorMessage.GetLength() > DEBUG_MSG_LENGTH) m_strErrorMessage.Empty();
	m_strErrorMessage.Append((LPCSTR)wParam);
	
	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;

	// skpark 2013.2.26  lParam 에 schedule Id 를 넘겨보자...errMsg 를 scheduleId 등 별로 보관했다가 clear 하기 위해서...skpark_manager_err
	//pclsHost->SendMessage(WM_ERROR_MESSAGE, wParam, lParam);
	pclsHost->SendMessageA(WM_ERROR_MESSAGE, wParam, LPARAM(this->m_scheduleId.c_str()));

	return 0;
}

bool CSchedule::GetFrameScheduleInfo(FRAME_SCHEDULE_INFO& info)
{
	info.contentsId	= m_contentsId;
	info.contentsType = m_contentsType;
	// Modified by 정운형 2008-10-31 오후 2:55
	// 변경내역 : location 경로 수정
	//만약 location 경로 끝에 '/'가 없다면 붙여준다.
	int nLength = m_location.length();
	if(nLength != 0 && m_location[nLength-1] != '/' && m_location[nLength-1] != '\\')
	{
		m_location += '/';
	}//if
	// Modified by 정운형 2008-10-31 오후 2:55
	// 변경내역 : location 경로 수정
	info.location	= m_location;
	info.filename	= m_filename;

	return true;
}


LRESULT CSchedule::OnHideContents(WPARAM wParam, LPARAM lParam)
{
	KillTimer(SCHEDULE_SHOW_ID);

	__DEBUG__("Hide Window", m_contentsName.c_str());

	ShowWindow(SW_HIDE);

	return 0;
}


BOOL CSchedule::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(WM_LBUTTONDOWN == pMsg->message)
	{
		__DEBUG__("L-CLICK!!!", _NULL);
		if(atoi(GetINIValue("UBCVariables.ini", "ROOT", "USE_CLICK_SCHEDULE")))
		{
			CHost* pHost = (CHost*)theApp.m_pMainWnd;
			if(m_touchTime.length() != 0)
			{
				//템플릿 이동
				pHost->ClickJumpTemplate(m_touchTime.c_str());
				return TRUE;
			}
			else if(pHost->m_bClickJump)
			{
				pHost->ResetClickJumpTimer();
			}
			else
			{
				//일반 클릭 스케줄
				__DEBUG__("Play click schedule", m_scheduleId.c_str());
				m_pParentParentWnd->PlayClickSchedule(m_scheduleId.c_str());
			}//if
		}//if
	}
	else if((pMsg->message == WM_KEYDOWN)/* && (pMsg->wParam == VK_ESCAPE)*/)
	{
		__DEBUG__("CSchedule::KeyDown", _NULL);
		::AfxGetMainWnd()->PreTranslateMessage(pMsg);
	}//if

	return CWnd::PreTranslateMessage(pMsg);
}

bool CSchedule::IsPlayableCategory()
{
	return CHost::IsPlayableCategory(m_mapInclude, m_mapExclude);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CVideoSchedule

CMapStringToPtr CVideoSchedule::m_mapContentsReuse;
BOOL CVideoSchedule::m_bVideoContentsSharing = -1;

CVideoSchedule::CVideoSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_pInterface(NULL)
,	m_rgbBgColor(0xffffffff)
,	m_rgbFgColor(0xffffffff)
,	m_nTimerId(0)
,	m_bPause(false)
,	m_dwRedrawTime(0)
,	m_bCompletePlay(false)
,	m_bSetPlayendTimer(false)
{
	if( m_bVideoContentsSharing < 0 )
	{
		m_bVideoContentsSharing = ( GetVidoeOpenType()==E_OPEN_INIT && GetVideoContentsSharing() );
		__DEBUG__("\t\t\tVideo Contents Sharing Mode", m_scheduleId.c_str());
	}

	m_nVedioOpenType = GetVidoeOpenType();
	m_nVedioRenderMode = GetVidoeRenderType();

	SetSchedule(lpszID);
}

CVideoSchedule::~CVideoSchedule()
{
	__DEBUG__("\t\t\tDestroy Video Schedule", m_scheduleId.c_str());

	//CloseFile();
}

IMPLEMENT_DYNAMIC(CVideoSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CVideoSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_MESSAGE(WM_DSINTERFACES_GRAPHNOTIFY, OnGraphNotify)
	ON_MESSAGE(WM_DSINTERFACES_COMPLETEPLAY, OnCompletePlay)
END_MESSAGE_MAP()


LRESULT CVideoSchedule::OnGraphNotify(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__("OnGraphNotify", _NULL);
	
	if(m_pInterface)
	{
		m_csLock.Lock();
		if(!m_pInterface->HandleGraphNotify(lParam))
		{
			__DEBUG__("Fail to HandleGraphNotify", _NULL);
		}//if
		m_csLock.Unlock();
	}//if

	return 0;
}


LRESULT CVideoSchedule::OnCompletePlay(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__("OnCompletePlay", _NULL);
	
	if(m_pInterface)
	{
		m_bCompletePlay = true;		//재생이 완료되었음
	}//if

	return 0;
}


void CVideoSchedule::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == ID_VIDEO_REPAINT)
	{
		if(m_pInterface)
		{
			if(!m_pInterface->RePaintVideo())
			{
				__DEBUG__("Video repaint fail", m_strMediaFullPath);
			}//if
		}//if
	}
	else if(nIDEvent == ID_VIDEO_PLAYEND)
	{
		if(!m_bCompletePlay)
		{
			__DEBUG__("Play end timer", _NULL);
			m_bCompletePlay = true;
		}//if
	}//if

	CSchedule::OnTimer(nIDEvent);
}


BOOL CVideoSchedule::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	__DEBUG__("OnEraseBkgnd", _NULL);
	
	DWORD dwNow = GetTickCount();
	//너무 자주 그려지는걸 막기위해 최대 0.2초 후에 그려지도록 한다.
	if((dwNow-m_dwRedrawTime) > 200)
	{
		__DEBUG__("Repaint", _NULL);
		if(m_pInterface)
		{
			if(!m_pInterface->RePaintVideo())
			{
				__DEBUG__("Video repaint fail", m_strMediaFullPath);
			}//if
		}//if
	}//if
	m_dwRedrawTime = GetTickCount();
		
	return TRUE;
	
	//return CSchedule::OnEraseBkgnd(pDC);
}


void CVideoSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	
	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		CRect rtClient;
		GetClientRect(rtClient);

		CMemDC memDC(&dc);
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);

	}//if
}

BOOL CVideoSchedule::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(WM_LBUTTONDOWN == pMsg->message)
	{
		if( m_wndController.IsWindowVisible() )
			m_wndController.Minimize();
		else
		m_wndController.Popup();
	}

	return CSchedule::PreTranslateMessage(pMsg);
}

int CVideoSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	if( GetShowVideoController() )
	{
		BOOL ret = m_wndController.Create(this);
		__DEBUG__("Show Video Controller", ret);
	}

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return 0;
}

void CVideoSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}


void CVideoSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	CString key;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		key.Format("comment%d", i+1);
		m_comment[i] = MNG_PROFILE_READ(lpszID, key);
	}

	m_bgColor = MNG_PROFILE_READ(lpszID, "bgColor");
	m_fgColor = MNG_PROFILE_READ(lpszID, "fgColor");
	m_font = MNG_PROFILE_READ(lpszID, "font");
	m_fontSize = atoi(MNG_PROFILE_READ(lpszID, "fontSize"));
	m_playSpeed = atoi(MNG_PROFILE_READ(lpszID, "playSpeed"));
	m_soundVolume = atoi(MNG_PROFILE_READ(lpszID, "soundVolume"));

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}
/*
	if(	m_comment[0].length() != 0 ||
		m_comment[1].length() != 0 ||
		m_comment[2].length() != 0 ||
		m_comment[3].length() != 0 ||
		m_comment[4].length() != 0 ||
		m_comment[5].length() != 0 ||
		m_comment[6].length() != 0 ||
		m_comment[7].length() != 0 ||
		m_comment[8].length() != 0 ||
		m_comment[9].length() != 0
	)
		m_bUseCaption = true;
	else
		m_bUseCaption = false;
*/
	// 볼륨
	if(m_pInterface)
	{
		m_pInterface->SetVolume(m_soundVolume);
	}//if

	__DEBUG_SCHEDULE_END__(lpszID)
}


double CVideoSchedule::GetTotalPlayTime()
{
	if(m_bFileNotExist)
	{
		return TIME_CONTENTS_NO_EXIST*1000;
	}//if

	REFTIME tm = 0;
	if(m_pInterface)
	{
		tm = m_pInterface->GetTotalPlayTime();
	}//if

	return tm;
}

double CVideoSchedule::GetCurrentPlayTime()
{
	if(m_bFileNotExist)
	{
		DWORD dwEndTick = ::GetTickCount();
		return (double)(dwEndTick - m_dwStartTick);
	}//if

	REFTIME tm = 0;
	if(m_pInterface)
	{
		if(m_bCompletePlay)
		{
			//WMV 영상등 일부 영상이 재생중 멈추는 현상이 나타나는데
			//DShow의 필터그래프에서 재생이 종료되는시점에 미묘하게 stop, pause등을 수행하면 block되는것 같다
			//따라서, 필터그래프에서 재생이 종료처리가된후에 종료할 수 있도록 수정한다.
			KillTimer(ID_VIDEO_PLAYEND);
			m_bSetPlayendTimer = false;
			tm = m_pInterface->GetTotalPlayTime();
		}
		else
		{
			//tm = m_pInterface->GetCurrentPlayTime();
			//tm = 0;
			//오버레이인경우 일부 wmv 동영상 재생시에 EC_CLOCK_CHANGED 이벤트 후에 EC_VMR_RENDERDEVICE_SET 이벤트가 
			//오면서 EC_COMPLETE 이벤트가 일어나지 않으면서 동영상 재생이 종료되지 않는 문제 발생....
			//현재 재생시간이 동영상의 전체 재생시간보다 지나면 종료하도록 처리한다.
			REFTIME tmCur, tmTotal, tmOver;
			tmCur = m_pInterface->GetCurrentPlayTime();
			tmTotal = m_pInterface->GetTotalPlayTime();
			//__DEBUG__("Current time", tmCur);
			//__DEBUG__("Total time", tmTotal);
			if(tmCur < tmTotal)
			{
				tm = tmCur;
			}
			else
			{
				//전체 재생시간이 지났는데도 종료가 안되는것 방지
				tm = tmTotal - 0.1;
				if(!m_bSetPlayendTimer)
				{
					m_bSetPlayendTimer = true;
					SetTimer(ID_VIDEO_PLAYEND, TIME_VIDEO_PLAYEND, NULL);
				}//if
				//m_bCompletePlay = true;
			}//if
		}//if
	}//if

	return tm;
}

bool CVideoSchedule::SetCurrentPlayTime(double pos)
{
	if(m_bFileNotExist) return false;

	if(m_pInterface==NULL) return false;

	if(m_bCompletePlay) return false;

	REFTIME tmTotal = m_pInterface->GetTotalPlayTime();

	if(pos < tmTotal)
	{
		__DEBUG__("SetCurrentPlayTime", pos);
		m_pInterface->SetPosition(pos);
		m_pInterface->RePaintVideo();
		return true;
	}

	return false;
}

bool CVideoSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		__DEBUG__("\t\t\tAlready Open", _NULL);
		return true;
	}

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(nGrade > 1 && !IsMusicFile() && !IsSoundOnSecondaryFrame() )
	{
		__DEBUG__("\t\t\tMute Mode", _NULL);
		m_bMute = true;
	}

	CFileStatus status;
	if(CFile::GetStatus(m_strMediaFullPath, status ) == FALSE)
	{
		//컨텐츠 파일이 없는 경우 액박 표시해 준다.
		SetNoContentsFile();
		m_bUseCaption = false;
	}
	else
	{
		if(m_nVedioOpenType == E_OPEN_INIT)
		{
			m_bOpen = OpenDSRender();
			if(!m_bOpen)
			{
				__DEBUG__("Directshow render open faile", _NULL);
				return false;
			}//if
		}
		else
		{
			m_bOpen = true;
		}//if
	}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true; 
}


bool CVideoSchedule::CloseFile()
{
	if(m_pInterface)
	{
		CloseDSRender();
	}//if
	m_bOpen = false;

	return true;
}

bool CVideoSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_bPause)
	{
		if(m_bFileNotExist)
		{
			Invalidate(FALSE);
		}
		else if(m_pInterface)
		{
			m_pInterface->Play();
		}//if
		m_bPause = false;

		return true;
	}//if

	if( (m_nVedioOpenType == E_OPEN_PLAY && !m_bFileNotExist) ||
		(m_bOpen && m_bVideoContentsSharing)
	  )
	{
		m_bOpen = OpenDSRender();
	}//if

	CSchedule::Play();
	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(), m_contentsName.c_str(), "FAIL", m_runningTime, m_filename.c_str(), "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	if(m_pInterface)
	{
		//m_pInterface->SeekToStart();
		m_pInterface->Play();
		m_bPause = false;
		m_bCompletePlay = false;
		m_pInterface->SetVolume( m_bMute ? 0 : m_soundVolume );
/*
		if(m_nVedioRenderMode == E_WINDOWLESS)
		{
			SetTimer(ID_VIDEO_REPAINT, TIME_VIDEO_REPAINT, NULL);
		}//if
*/
	}//if

	//Invalidate();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

void CVideoSchedule::SoundVolumeMute(bool bMute)
{
	m_bMute = bMute;
	if(m_pInterface)
	{
		m_pInterface->SetVolume(m_bMute ? 0 : m_soundVolume);
	}//if
}

bool CVideoSchedule::Pause()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_pInterface)
	{
		m_pInterface->Pause();
	}//if

	m_bPause = true;

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CVideoSchedule::Stop()
{
	if(m_nVedioRenderMode == E_EVR)
	{
		if(m_pInterface)
		{
			m_pInterface->SetNotifyWindow(NULL);
		}//if

		int delay = 1500 + (rand() % 500); // 최소 1500ms 이상, 최대 2000ms
		m_pParentWnd->_sch = this;
		m_pParentWnd->SetTimer(SCHEDULE_STOP_ID, delay, NULL);
		return true;
	}

	return StopEx();

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	m_wndController.Minimize();

/*
	if(m_nVedioRenderMode == E_WINDOWLESS)
	{
		KillTimer(ID_VIDEO_REPAINT);
	}//if
*/

	m_bCompletePlay = true;
	if(m_nVedioOpenType == E_OPEN_PLAY)
	{
		CloseDSRender();
	}
	else
	{
		if(m_pInterface)
		{
			m_pInterface->Stop();
		}//if
	}//if
	
	if(m_bFileNotExist)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "Content file does not exist", m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CVideoSchedule::StopEx()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	m_wndController.Minimize();

/*
	if(m_nVedioRenderMode == E_WINDOWLESS)
	{
		KillTimer(ID_VIDEO_REPAINT);
	}//if
*/

	m_bCompletePlay = true;
	if(m_nVedioOpenType == E_OPEN_PLAY)
	{
		CloseDSRender();
	}
	else
	{
		if(m_pInterface)
		{
			m_pInterface->Stop();
		}//if
	}//if
	
	if(m_bFileNotExist)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "Content file does not exist", m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 스케줄을 처음부터 다시 재생한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CVideoSchedule::RePlay()
{
	if(m_pInterface)
	{
		__DEBUG__("Begin replay schedule", m_strMediaFullPath);
		if( m_bDefaultSchedule || !GetPlayOneTimesTimebaseSchedule() )
		{
			m_nPlayCount ++; //skpark 2014.06.16 템플릿 반복기능 버그 수정
			m_bCompletePlay = false;
			m_pInterface->SeekToStart();
			m_pInterface->Play();
		}
		__DEBUG__("End replay schedule", m_strMediaFullPath);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Directshow render를 옵션에 맞추어 설정하고 그래프 빌더를 구성한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVideoSchedule::OpenDSRender()
{
	if(m_bIsHDSchedule)
	{
		m_nVedioRenderMode = E_WINDOWLESS;
		//m_nVedioRenderMode = E_OVERLAY;
		//m_nVedioRenderMode = E_EVR;
	}
	else
	{
		m_nVedioRenderMode = GetVidoeRenderType(); 
	}//if

	if( m_bVideoContentsSharing )
	{
		CString key = m_strMediaFullPath.MakeLower();

		if( m_mapContentsReuse.Lookup(key, (void*&)m_pInterface) )
		{
			__DEBUG__("Video Re-use Find", m_strMediaFullPath);
			if( !m_pInterface->ChangeParentWindow(GetSafeHwnd()) )
			{
				__DEBUG__("Fail to Re-use Contents", m_strMediaFullPath);
			}
			return true;
		}
		__DEBUG__("Video Re-use not Find", m_strMediaFullPath);
	}
	else
	{
		CloseDSRender();
	}

	bool bRet = false;
	if( IsMusicFile() )
	{
		__DEBUG__("\t\t\tMusic Interface Mode", m_strMediaFullPath);
		m_pInterface = new CMusicInterface(this);

		if(m_pInterface->Open(m_strMediaFullPath))
		{
			bRet = true;
		}
		else
		{
			__DEBUG__("\t\t\tOpen Failed", m_strMediaFullPath);
			m_bOpen = false;
			bRet = false;
			CString strError;
			strError.Format("Music file[%s] render fail", m_strMediaFullPath);
			OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

			CloseDSRender();
		}//if
	}
	else if(m_nVedioRenderMode == E_DUMMY_OVERLAY
		/*|| m_nVedioRenderMode == E_OVERLAY*/)
	{
		//__DEBUG__("\t\t\tDummy Overlay Interface Mode", _NULL);
		__DEBUG__("\t\t\tDummy Overlay Interface Mode", m_strMediaFullPath);
		m_pInterface = new COverlayInterface(this);
		m_pInterface->SetParentWindow(GetSafeHwnd());
		m_pInterface->SetRenderType(m_nVedioRenderMode);

		if(m_pInterface->Open(m_strMediaFullPath))
		{
			bRet = true;
			//크기조정
			m_pInterface->SetAspectRatioMode(STRETCHED);
			//CreateCaptionImage();
		}
		else
		{
			__DEBUG__("\t\t\tOpen Failed", m_strMediaFullPath);
			m_bOpen = false;
			bRet = false;
			CString strError;
			strError.Format("Video file[%s] overlay-render fail", m_strMediaFullPath);
			OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

			CloseDSRender();
		}//if
	}
	else if(m_nVedioRenderMode == E_EVR)
	{
		__DEBUG__("\t\t\tEVR Mode", m_strMediaFullPath);
		m_pInterface = new CEVRRender(this);
		m_pInterface->SetParentWindow(GetSafeHwnd());
		m_pInterface->SetRenderType(m_nVedioRenderMode);
		if(!m_pInterface->Open(m_strMediaFullPath))
		{
			m_bOpen = false;
			bRet = false;
			CString strError;
			strError.Format("Video file[%s] evr-render fail", m_strMediaFullPath);
			OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

			CloseDSRender();
		}
		else
		{
			bRet = true;
			m_pInterface->SetAspectRatioMode(STRETCHED);

			CRect rect;
			GetClientRect(rect);
			m_pInterface->SetVideoRect(rect);
			m_pInterface->SetPosition(0.1f);
		}
	}
	else// if(m_nVedioRenderMode == E_WINDOWLESS)
	{
		__DEBUG__("\t\t\tVMR Mode", m_strMediaFullPath);
		m_pInterface = new CVMRRender(this);
		m_pInterface->SetParentWindow(GetSafeHwnd());
		m_pInterface->SetRenderType(m_nVedioRenderMode);
		if(!m_pInterface->Open(m_strMediaFullPath))
		{
			m_bOpen = false;
			bRet = false;
			CString strError;
			strError.Format("Video file[%s] vmr-render fail", m_strMediaFullPath);
			OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

			CloseDSRender();
		}
		else
		{
			bRet = true;
			m_pInterface->SetAspectRatioMode(STRETCHED);
		}//if
	}//if

	if( bRet && m_bVideoContentsSharing )
	{
		CString key = m_strMediaFullPath.MakeLower();
		__DEBUG__("Video Re-use Add", m_strMediaFullPath);
		m_mapContentsReuse.SetAt(key, m_pInterface);
	}

	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Directshow render를 종료한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CVideoSchedule::CloseDSRender()
{
	if( m_bVideoContentsSharing )
	{
		CString key = m_strMediaFullPath.MakeLower();

		if( !m_mapContentsReuse.Lookup(key, (void*&)m_pInterface) )
		{
			__DEBUG__("Video Re-use not Find. Already destroyed.", m_strMediaFullPath);
			return;
		}

		m_mapContentsReuse.RemoveKey(key);
	}

	if(m_pInterface)
	{
		__DEBUG__("Inteface close", _NULL);
		m_pInterface->Close();
		delete m_pInterface;
	}//if
	m_pInterface = NULL;
}


CString to_string(ciString* pString, int count)
{
	CString str;

	for(int i=0; i<count; i++)
	{
		if(i != 0) str.Append("\r\n");
		CString tmp;
		tmp.Format("comment[%d] = \"%s\"", i, pString[i].c_str());
		str.Append(tmp);
	}

	return str;
}

void to_string(ciString* pString, int count, CPropertyGrid* pGrid, HSECTION hs)
{
	for(int i=0; i<count; i++)
	{
		CString tmp;
		tmp.Format("comment[%d]", i);
		pGrid->AddStringItem(hs, (LPCSTR)tmp, pString[i].c_str());
	}
}

CString	CVideoSchedule::ToString()
{
	CString str;
	str.Format(
		"-Video Schedule-\r\n"
		"%s\r\n"
		"%s\r\n"
		"bgColor = %s\r\n"
		"fgColor = %s\r\n"
		"font = %s\r\n"
		"fontSize = %d\r\n"
		"playSpeed = %d\r\n"
		"soundVolume = %d\r\n"
		"\r\n"
		"m_bUseCaption = %d\r\n"
		"m_nCaptionWidth = %d\r\n"
		"m_offset = %d\r\n"
		"m_nTimerId = %d\r\n"
		"m_nMS = %d\r\n"
		"m_nVedioRenderMode = %d\r\n"
		"m_nVedioOpenType = %d\r\n"
		,	CSchedule::ToString()
		,	to_string(m_comment, TICKER_COUNT)
		,	m_bgColor.c_str()		// 자막용
		,	m_fgColor.c_str()		// 자막용
		,	m_font.c_str()			// 자막용
		,	m_fontSize		// 자막용
		,	m_playSpeed	// 자막용
		,	m_soundVolume

		,	m_bUseCaption
		,	m_nCaptionWidth
		,	m_offset
		,	m_nTimerId
		,	m_nMS
		,	m_nVedioRenderMode
		,	m_nVedioOpenType
	);

	return str;
}

void CVideoSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("VIDEO Schedule Attributes");

	to_string(m_comment, TICKER_COUNT, pGrid, hs);

	pGrid->AddStringItem(hs, "bgColor", m_bgColor.c_str());
	pGrid->AddStringItem(hs, "fgColor", m_fgColor.c_str());
	pGrid->AddStringItem(hs, "font", m_font.c_str());
	pGrid->AddIntegerItem(hs, "fontSize", m_fontSize);
	pGrid->AddIntegerItem(hs, "playSpeed", m_playSpeed);
	pGrid->AddIntegerItem(hs, "soundVolume", m_soundVolume);

	hs = pGrid->AddSection("member variables");

	pGrid->AddBoolItem(hs, "m_bUseCaption", m_bUseCaption);
	pGrid->AddIntegerItem(hs, "m_nCaptionWidth", m_nCaptionWidth);
	pGrid->AddIntegerItem(hs, "m_offset", m_offset);
	pGrid->AddIntegerItem(hs, "m_nTimerId", m_nTimerId);
	pGrid->AddIntegerItem(hs, "m_nMS", m_nMS);
	pGrid->AddIntegerItem(hs, "m_nVedioRenderMode", m_nVedioRenderMode);
	pGrid->AddIntegerItem(hs, "m_nVedioOpenType", m_nVedioOpenType);
}

bool CVideoSchedule::IsMusicFile()
{
	CString str_ext = m_strMediaFullPath.Right(4);

	if( stricmp(str_ext, ".mp3") == 0 ) return true;
	if( stricmp(str_ext, ".wma") == 0 ) return true;
	if( stricmp(str_ext, ".wav") == 0 ) return true;

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPictureSchedule

CPictureSchedule::CPictureSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_bAnimatedGif(false)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CPictureSchedule::~CPictureSchedule()
{
	__DEBUG__("\t\t\tDestroy Picture Schedule", m_scheduleId.c_str());

	//CloseFile();
}

IMPLEMENT_DYNAMIC(CPictureSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CPictureSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

int CPictureSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rect;
	GetClientRect(rect);

	__DEBUG__("\t\t\tCreate AnimatedGIF", _NULL);
	m_bgGIF.Create("Animated GIF", WS_CHILD, rect, this);

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return 0;
}


void CPictureSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}


void CPictureSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	CMemDC memDC(&dc);

	CRect rtClient;
	GetClientRect(rtClient);

	if(m_bOpen)
	{
		if(m_bFileNotExist && m_bgImage.IsValid())
		{
			memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
			m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
		}
		else if(m_bAnimatedGif)
		{
			SIZE size = m_bgGIF.GetSize();

			memDC.FillSolidRect(size.cx, 0, rtClient.Width(), size.cy, RGB(255,255,255));
			memDC.FillSolidRect(0, size.cy, rtClient.Width(), rtClient.Height(), RGB(255,255,255));
		}
		else
		{
			if(m_bgImage.IsValid())
			{
				m_bgImage.Draw2(memDC.GetSafeHdc(), rtClient);
			}//if
		}//if
	}//if
}


void CPictureSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	__DEBUG_SCHEDULE_END__(lpszID)
}


double CPictureSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CPictureSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CPictureSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		__DEBUG__("\t\t\tAlready Open", _NULL);
		return true;
	}

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(!OpenGIF())
	{
		__DEBUG__("\t\t\tImage Mode", _NULL);
		m_bAnimatedGif = false;
		if(!OpenImage())
		{
			SetNoContentsFile();
			__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
			return true;
		}//if
	}//if

	m_bOpen = true;
	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// GIF 이미지파일을 open한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPictureSchedule::OpenGIF()
{
	CString strExt = FindExtension(m_strMediaFullPath);
	if(strExt.CompareNoCase("gif") != 0)
	{
		return false;
	}//if
	
	if(!m_bgGIF.Load(m_strMediaFullPath))
	{
		if(m_bgGIF.GetSafeHwnd())
		{
			m_bgGIF.ShowWindow(SW_HIDE);
			m_bgGIF.Stop();
			m_bgGIF.UnLoad();
		}//if
		__DEBUG__("Gif image load fail", _NULL);
		return false;
	}//if
		
	if(m_bgGIF.IsAnimatedGIF())
	{
		__DEBUG__("\t\t\tAnimated GIF Mode", _NULL);
		m_bAnimatedGif = true;
		m_bgGIF.ShowWindow(SW_SHOW);
		return true;
	}//if
	
	if(m_bgGIF.GetSafeHwnd())
	{
		m_bgGIF.ShowWindow(SW_HIDE);
		m_bgGIF.Stop();
		m_bgGIF.UnLoad();
	}//if
	
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지파일을 open한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPictureSchedule::OpenImage()
{
	CString strExt = FindExtension(m_strMediaFullPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
	{
		__DEBUG__("image file load fail", m_strMediaFullPath);
		return false;
	}//if

	int nX, nY, nWidth, nHeight;
	//m_pParentWnd->GetFrameRgn(nX, nY, nWidth, nHeight);
	CRect rtClient;
	GetClientRect(&rtClient);
	nWidth = rtClient.Width();
	nHeight = rtClient.Height();
	DWORD dwWidth = m_bgImage.GetWidth();
	DWORD dwHeight = m_bgImage.GetHeight();
	if(dwWidth != nWidth || dwHeight != nHeight)
	{
		if(!m_bgImage.Resample(nWidth, nHeight, 3))	//default option(bilinear interpolation)
		{
			__DEBUG__("image resample fail", m_strMediaFullPath);
		}//if
	}//if

	return true;
}


bool CPictureSchedule::CloseFile()
{
	Stop();

	if(m_bOpen)
	{
		m_bOpen = false;

		if(m_bAnimatedGif)
		{
			m_bgGIF.UnLoad();
		}
		else
		{
			if(m_bgImage.IsValid())
			{
				m_bgImage.Destroy();
			}//if
		}//if

		m_bAnimatedGif = false;
	}//if

	return true;
}


bool CPictureSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CSchedule::Play();
	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	if(m_bAnimatedGif)
	{
		__DEBUG__("\t\t\tPlay Animated GIF", _NULL);
		m_bgGIF.Draw();
	}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


bool CPictureSchedule::Pause()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


bool CPictureSchedule::Stop()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_bAnimatedGif)
	{
		__DEBUG__("\t\t\tStop Animated GIF", _NULL);
		m_bgGIF.Stop();
	}//if

	if(m_bFileNotExist)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "Content file does not exist", m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


CString	CPictureSchedule::ToString()
{
	CString str;
	str.Format(
		"-Picture Schedule-\r\n"
		"%s\r\n"
		"\r\n"
		"m_bAnimatedGif = %d\r\n"
		,	CSchedule::ToString()
		,	m_bAnimatedGif
	);

	return str;
}


void CPictureSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("PICTURE Schedule Attributes");

	hs = pGrid->AddSection("member variables");

	pGrid->AddBoolItem(hs, "m_bAnimatedGif", m_bAnimatedGif);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSMSSchedule


CSMSSchedule::CSMSSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_rgbBgColor(RGB(0,0,0))
,	m_rgbFgColor(RGB(255,255,255))
,	m_nLineCount(0)
,	m_direction(0)
,	m_align(5)
// <!-- 메가박스용 (2017.05.10 seventhstone)
,	m_bGridMode(false)
// -->
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CSMSSchedule::~CSMSSchedule()
{
	if(m_fontSMS.GetSafeHandle())
	{
		m_fontSMS.DeleteObject();
	}//if

	__DEBUG__("\t\t\tDestroy SMS Schedule", m_scheduleId.c_str());

	//CloseFile();
}

IMPLEMENT_DYNAMIC(CSMSSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CSMSSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

int CSMSSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return 0;
}

void CSMSSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}

void CSMSSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	//
	CRect rtClient;
	GetClientRect(rtClient);

	CMemDC memDC(&dc);

	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}
	else if(m_bgImage.IsValid())
	{
#if 1	
		// 바둑판
		for(int x=0; x<rtClient.Width(); x+=m_bgImage.GetWidth())
			for(int y=0; y<rtClient.Height(); y+=m_bgImage.GetHeight())
				m_bgImage.Draw2(memDC.GetSafeHdc(), x, y);
#else
		// 늘이기
		m_bgImage.Draw2(memDC.GetSafeHdc(), 0, 0, rtClient.Width(), rtClient.Height());
#endif
	}
	else
	{
		memDC.FillSolidRect(rtClient, m_rgbBgColor);
	}//if

	memDC.SetBkMode(TRANSPARENT);	//글자의 배경색을 보이게 하기 위하여...
	memDC.SetTextColor(m_rgbFgColor);
	memDC.SetBkColor(m_rgbBgColor);
	memDC.SelectObject(m_fontSMS);

	// <!-- 메가박스용 (2017.05.10 seventhstone)
	if( m_bGridMode )
	{
		DrawGridText((CDC*)&memDC);
		return;
	}
	// -->

	if(m_direction == TEXT_DIRECTION_HORIZONTAL)
	{
		CRect rectText(0, 0, 0, 0);
		memDC.DrawText(m_strSMS, &rectText, DT_CALCRECT);	//글자가 출력될 영역을 계산
		//GetTextExtentPoint32 ==> 문자열
		//CSize sizeText = memDC.GetOutputTextExtent(m_strSMS);	==> 한 문자
		//if(m_nLineCount > 1)
		//{
		//	sizeText.cy = sizeText.cy*m_nLineCount;
		//}//if

		int nPosX = 0, nPosY = 0, nCx = 0, nCy = 0, nGapX = 0, nGapY = 0;
		if(rtClient.Width() <= rectText.Width())
		//if(client_rect.Width() <= sizeText.cx)
		{
			nCx = rtClient.Width();
			nGapX = 0;
		}
		else
		{
			nCx = rectText.Width();
			nGapX = rtClient.Width() - rectText.Width();
			//nCx = sizeText.cx;
			//nGapX = client_rect.Width() - sizeText.cx;
		}//if

		if(rtClient.Height() <= rectText.Height())
		//if(client_rect.Height() <= sizeText.cy)
		{
			nCy = rtClient.Height();
			nGapY = 0;
		}
		else
		{
			nCy = rectText.Height();
			nGapY = rtClient.Height() - rectText.Height();
			//nCy = sizeText.cy;
			//nGapY = client_rect.Height() - sizeText.cy;
		}//if

		UINT uFormat;
		switch(m_align)
		{
		case TEXT_ALIGN_LT:
			{
				nPosX = 0;
				nPosY = 0;
				uFormat = DT_LEFT;
			}
			break;
		case TEXT_ALIGN_CT:
			{
				nPosX = nGapX/2;
				nPosY = 0;
				uFormat = DT_CENTER;
			}
			break;
		case TEXT_ALIGN_RT:
			{
				nPosX = nGapX;
				nPosY = 0;
				uFormat = DT_RIGHT;
			}
			break;
		case TEXT_ALIGN_LM:
			{
				nPosX = 0;
				nPosY = nGapY/2;
				uFormat = DT_LEFT;
			}
			break;
		case TEXT_ALIGN_CM:
			{
				nPosX = nGapX/2;
				nPosY = nGapY/2;
				uFormat = DT_CENTER;
			}
			break;
		case TEXT_ALIGN_RM:
			{
				nPosX = nGapX;
				nPosY = nGapY/2;
				uFormat = DT_RIGHT;
			}
			break;
		case TEXT_ALIGN_LB:
			{
				nPosX = 0;
				nPosY = nGapY;
				uFormat = DT_LEFT;
			}
			break;
		case TEXT_ALIGN_CB:
			{
				nPosX = nGapX/2;
				nPosY = nGapY;
				uFormat = DT_CENTER;
			}
			break;
		case TEXT_ALIGN_RB:
			{
				nPosX = nGapX;
				nPosY = nGapY;
				uFormat = DT_RIGHT;
			}
			break;
		default :	//TEXT_ALIGN_CM
			{
				nPosX = nGapX/2;
				nPosY = nGapY/2;
				uFormat = DT_CENTER;
			}
		}//switch

		CRect rectDraw(nPosX, nPosY, nCx+nPosX, nCy+nPosY);
		//memDC.FillSolidRect(rectDraw, RGB(0, 255, 0));
		memDC.DrawText(m_strSMS, rectDraw, uFormat);
		//memDC.DrawText(m_strSMS, client_rect, DT_RIGHT);
	}
	else
	{
		UINT uFormat;
		switch(m_align)
		{
		case TEXT_ALIGN_LT:
			{
				uFormat = DV_TEXTTOP;
			}
			break;
		case TEXT_ALIGN_CT:
			{
				uFormat = DV_HCENTER | DV_TEXTTOP;
			}
			break;
		case TEXT_ALIGN_RT:
			{
				uFormat = DV_RIGHT | DV_TEXTTOP;
			}
			break;
		case TEXT_ALIGN_LM:
			{
				uFormat = DV_VCENTER | DV_TEXTCENTER;
			}
			break;
		case TEXT_ALIGN_CM:
			{
				uFormat = DV_VCENTER | DV_HCENTER | DV_TEXTCENTER;
			}
			break;
		case TEXT_ALIGN_RM:
			{
				uFormat = DV_VCENTER | DV_RIGHT | DV_TEXTCENTER;
			}
			break;
		case TEXT_ALIGN_LB:
			{
				uFormat = DV_BOTTOM | DV_TEXTBOTTOM;
			}
			break;
		case TEXT_ALIGN_CB:
			{
				uFormat = DV_BOTTOM | DV_HCENTER | DV_TEXTBOTTOM;
			}
			break;
		case TEXT_ALIGN_RB:
			{
				uFormat = DV_BOTTOM | DV_RIGHT | DV_TEXTBOTTOM;
			}
			break;
		default :	//TEXT_ALIGN_CM
			{
				uFormat = DV_VCENTER | DV_HCENTER | DV_TEXTCENTER;
			}
		}//switch

		DrawVertText(memDC, m_strSMS, m_strSMS.GetLength(), &rtClient, uFormat, 3, 5);
	}//if
}

void CSMSSchedule::DrawGridText(CDC* pDC)
{
	int length = m_arrSMS.GetCount();

	for(int i=0; i<length; i++)
	{
		CString text = m_arrSMS.GetAt(i);
		COLORREF color = m_arrColor.GetAt(i);

		CRect rect(m_nGridWidth*i,0,m_nGridWidth*(i+1),m_nGridHeight);

		//rect.left /= 4;
		//rect.right /= 4;

		pDC->SetTextColor(color);

		pDC->DrawText(text, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		//pDC->Draw3dRect(rect, RGB(255,0,0), RGB(0,255,0));
	}
}

bool CSMSSchedule::CreateFile()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	double xscale, yscale;
	m_pParentParentWnd->GetScale(xscale, yscale);

	if(m_fontSMS.GetSafeHandle())
	{
		m_fontSMS.DeleteObject();
	}//if

	if(!IsAvailableFont(m_font.c_str()))
	{
		CString strError;
		strError.Format("SMS schedule create font[%s] fail !!!", m_font.c_str());
		__WARNING__(strError, _NULL);
		m_fontSMS.CreatePointFont(m_fontSize*10 * xscale, "System");
	}
	else
	{
		m_fontSMS.CreatePointFont(m_fontSize*10 * xscale, m_font.c_str());
	}//if

	m_strSMS = "";
	//뒤에서부터 빈라인을 확인하여 라인숫자를 정한다.
	for(m_nLineCount = TICKER_COUNT; m_nLineCount>0; m_nLineCount--)
	{
		if(m_comment[m_nLineCount].length() > 0)
		{
			break;
		}//if
	}//for

	//각 라인을 합쳐서 하나의 문자열로 만든다.
	for(int i=0; i<m_nLineCount; i++)
	{
		m_strSMS.Append(m_comment[i].c_str());
		m_strSMS.Append("\r\n");
	}//for
	m_strSMS.Replace("&", "&&");
	m_strSMS.TrimRight();

	if(m_nLineCount == 0)
	{
		m_strLogFileString = m_filename.c_str();
	}
	else
	{
		m_strLogFileString = m_strSMS.Left(255);
	}//if

	// <!-- 메가박스용 (2017.05.10 seventhstone)
	// sample : <WxH=1280,720>TEST
	CString str = m_strSMS;
	str.Replace("\r", "");
	str.Replace("\n", "");

	int pos = 0;
	while(true)
	{
		CString tag = str.Tokenize("<=,", pos);
		if( tag != "WxH" ) break;

		CString width = str.Tokenize("<=,", pos);
		m_nGridWidth = atoi(width);
		if( m_nGridWidth==0 ) break;

		CString height = str.Tokenize("<=,", pos);
		m_nGridHeight = atoi(height);
		if( m_nGridHeight==0 ) break;

		pos = str.Find('>');
		if( pos<0 ) break;

		m_bGridMode = true;
		m_arrSMS.RemoveAll();
		m_arrColor.RemoveAll();

		USES_CONVERSION;
		CStringW body = A2W( ((LPCSTR)str + pos + 1) ); // ANSI to UCS-2
		COLORREF font_color = m_rgbFgColor;
		bool tag_open = false;
		CString tag_string = "";
		for(int i=0; i<body.GetLength(); i++)
		{
			wchar_t unicode[2] = {0};
			unicode[0] = body.GetAt(i);

			CString ansi = W2A(unicode);

			if( ansi=="<" && tag_open==false)
			{
				tag_open = true;
				continue;
			}
			if( ansi==">" && tag_open )
			{
				tag_open = false;

				if( tag_string.Left(4) == "RGB=")
				{
					int pos2 = 0;
					while(true)
					{
						CString tag = tag_string.Tokenize("=,", pos2);
						if( tag != "RGB" ) break;

						CString str_red = tag_string.Tokenize("=,", pos2);
						if(str_red=="") break;
						int red = atoi(str_red);

						CString str_green = tag_string.Tokenize("=,", pos2);
						if(str_green=="") break;
						int green = atoi(str_green);

						CString str_blue = tag_string.Tokenize("=,", pos2);
						if(str_blue=="") break;
						int blue = atoi(str_blue);

						font_color = RGB(red,green,blue);
						break;
					}
				}
				if( tag_string=="/RGB" )
				{
					font_color = m_rgbFgColor;
				}

				tag_string = "";
				continue;
			}
			if( tag_open )
			{
				tag_string += ansi;
				continue;
			}

			m_arrSMS.Add(ansi);
			m_arrColor.Add(font_color);
		}
		break;
	}
	// -->

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


void CSMSSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	m_bgColor = MNG_PROFILE_READ(lpszID, "bgColor");
	m_fgColor = MNG_PROFILE_READ(lpszID, "fgColor");
	m_font = MNG_PROFILE_READ(lpszID, "font");
	m_fontSize = atoi(MNG_PROFILE_READ(lpszID, "fontSize"));
	m_direction = atoi(MNG_PROFILE_READ(lpszID, "direction"));
	m_align = atoi(MNG_PROFILE_READ(lpszID, "align"));

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}

/*
Quotation mark						&#34	&quot;
Ampersand							&#38	&amp;
Less-than sign						&#60	&lt;
Greater-than sign					&#62	&gt;
*/
/*
	for(int i=0; i<TICKER_COUNT; i++)
	{
		CString str = m_comment[i].c_str();

		str.Replace("&", "&&");
		str.Replace("&", "&amp;");
		str.Replace("\"", "&quot;");
		str.Replace("\'", "&apos;");
		str.Replace("<", "&lt;");
		str.Replace(">", "&gt;");

		m_comment[i] = str;
	}
*/
	//CreateFile();

	__DEBUG_SCHEDULE_END__(lpszID)
}


double CSMSSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CSMSSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CSMSSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		__DEBUG__("\t\t\tAlready Open", _NULL);
		return true;
	}

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			//컨텐츠 파일이 없는 경우 액박 표시해 준다.
			SetNoContentsFile();
		}//if
	}//if

	CreateFile();

	m_bOpen = true;

	__DEBUG__("m_bOpen", m_bOpen);

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return m_bOpen;
}

bool CSMSSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();

		m_bOpen = false;
		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if

		if(m_fontSMS.GetSafeHandle())
		{
			m_fontSMS.DeleteObject();
		}//if
	}//if
	return true;
}

bool CSMSSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CSchedule::Play();

	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CSMSSchedule::Pause()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CSMSSchedule::Stop()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_bFileNotExist)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "Content file does not exist", m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


CString	CSMSSchedule::ToString()
{
	CString str;
	str.Format(
		"-SMS Schedule-\r\n"
		"%s\r\n"
		"%s\r\n"
		"bgColor = %s\r\n"
		"fgColor = %s\r\n"
		"font = %s\r\n"
		"fontSize = %d\r\n"
		"direction = %d\r\n"
		"align = %d\r\n"
		,	CSchedule::ToString()
		,	to_string(m_comment, TICKER_COUNT)
		,	m_bgColor.c_str()		// 자막용
		,	m_fgColor.c_str()		// 자막용
		,	m_font.c_str()			// 자막용
		,	m_fontSize		// 자막용
		,	m_direction
		,	m_align
	);

	return str;
}

void CSMSSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("SMS Schedule Attributes");

	to_string(m_comment, TICKER_COUNT, pGrid, hs);

	pGrid->AddStringItem(hs, "bgColor", m_bgColor.c_str());
	pGrid->AddStringItem(hs, "fgColor", m_fgColor.c_str());
	pGrid->AddStringItem(hs, "font", m_font.c_str());
	pGrid->AddIntegerItem(hs, "fontSize", m_fontSize);
	pGrid->AddIntegerItem(hs, "direction", m_direction);
	pGrid->AddIntegerItem(hs, "align", m_align);

	hs = pGrid->AddSection("member variables");
	pGrid->AddStringItem(hs, "m_strSMS", (LPCSTR)m_strSMS);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHorizontalTickerSchedule


CHorizontalTickerSchedule::CHorizontalTickerSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_offset (0)
,	m_nTimerId (0)
,	m_rgbBgColor(RGB(0,0,0))
,	m_rgbFgColor(RGB(255,255,255))
,	m_bUseUnicode (false)
,	m_strLogFileString("")
,	m_nShiftStep(HORIZONTAL_TICKER_CAPTION_SHIFT_STEP)
,	m_nMS(E_TICKER_NORMAL)
,	m_bExitThread(true)
,	m_hevtShift(NULL)
,	m_pthreadShift(NULL)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CHorizontalTickerSchedule::~CHorizontalTickerSchedule()
{
	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if

	__DEBUG__("\t\t\tDestroy Horizontal Schedule", m_scheduleId.c_str());

	//CloseFile();
}


IMPLEMENT_DYNAMIC(CHorizontalTickerSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CHorizontalTickerSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_MESSAGE(CAPTION_SHIFT_ID, OnCaptionShift)
END_MESSAGE_MAP()


int CHorizontalTickerSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	CPaintDC dc(this);
	dc.SelectObject(&m_fontTicker);
	if( m_bUseUnicode )
	{
		ASSERT(dc.m_hDC != NULL);
		SIZE size;
		VERIFY(::GetTextExtentPoint32W(dc.m_hDC, m_strTickerW, m_strTickerW.GetLength(), &size));

		m_nTickerWidth = size.cx;
	}
	else
	{
		CSize text_size = dc.GetOutputTextExtent(m_strTicker);
		m_nTickerWidth = text_size.cx;
	}

	CRect client_rect;
	GetClientRect(client_rect);
	m_nWidth = client_rect.Width();
	m_nHeight = client_rect.Height();
	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return 0;
}

void CHorizontalTickerSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}

void CHorizontalTickerSchedule::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSchedule::OnTimer(nIDEvent);

	if(nIDEvent == CAPTION_SHIFT_ID)
	{
		OnCaptionShift(0,0);
	}
}

void CHorizontalTickerSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	CRect rtClient;
	GetClientRect(rtClient);

	CMemDC memDC(&dc);

	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		if(m_rgbFgColor == RGB(255, 255, 255))
		{
			memDC.FillSolidRect(rtClient, RGB(0, 0, 0));
		}
		else
		{
			memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		}//if
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}
	else if(m_bgImage.IsValid())
	{
#if 1	
		// 바둑판
		for(int x=0; x<rtClient.Width(); x+=m_bgImage.GetWidth())
			for(int y=0; y<rtClient.Height(); y+=m_bgImage.GetHeight())
				m_bgImage.Draw2(memDC.GetSafeHdc(), x, y);
#else
		// 늘이기
		m_bgImage.Draw(memDC.GetSafeHdc(), 0, 0, rtClient.Width(), rtClient.Height());
#endif
	}
	else
	{
		memDC.FillSolidRect(rtClient, m_rgbBgColor);
	}//if

	if(m_bOpen && m_nTickerWidth != 0)
	{
		int nOffset = 0;
		GetOffset(&nOffset);
		CRect rect = rtClient;
		rect.left = nOffset;
		rect.right = nOffset + m_nTickerWidth;

		memDC.SetBkMode(TRANSPARENT);
		memDC.SetTextColor(m_rgbFgColor);
		memDC.SetBkColor(m_rgbBgColor);
		memDC.SelectObject(m_fontTicker);

		CRect rectText(0, 0, 0, 0);
		if(m_bUseUnicode)
		{
			::DrawTextW(memDC.m_hDC, m_strTickerW, m_strTickerW.GetLength(), &rectText, DT_CALCRECT);	//글자가 출력될 영역을 계산
		}
		else
		{
			memDC.DrawText(m_strTicker, &rectText, DT_CALCRECT);	//글자가 출력될 영역을 계산
		}//if

		int nPosY = 0, nCy = 0, nGapY = 0;
		if(rtClient.Height() <= rectText.Height())
		{
			nCy = rtClient.Height();
			nGapY = 0;
		}
		else
		{
			nCy = rectText.Height();
			nGapY = rtClient.Height() - rectText.Height();
		}//if

		//세로 방향 가운데에 문자 높이만큼만 영역을 설정
		nPosY = nGapY/2;
		rect.top = nPosY;
		rect.bottom = nPosY + nCy;
		//memDC.FillSolidRect(rect, RGB(0, 255, 0));

		do
		{
			if(m_bUseUnicode)
			{
				::DrawTextW(memDC.m_hDC, m_strTickerW, m_strTickerW.GetLength(), rect, DT_SINGLELINE | DT_VCENTER);
			}
			else
			{
				memDC.DrawText(m_strTicker, rect, DT_SINGLELINE | DT_VCENTER);
			}//if
			rect.OffsetRect(m_nTickerWidth, 0);
		}
		while(rect.left < rtClient.right);
	}//if
}

LRESULT CHorizontalTickerSchedule::OnCaptionShift(WPARAM wParam, LPARAM lParam)
{
	DecreaseOffset();

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Offset 값을 줄인다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHorizontalTickerSchedule::DecreaseOffset()
{
	m_csOffset.Lock();
	m_offset -= m_nShiftStep;

	//if(m_offset == -m_nTickerWidth)
	if(m_offset <= -m_nTickerWidth)
	{
		m_offset = 0;
	}//if
	m_csOffset.Unlock();

	Invalidate(FALSE);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 offset 값을 반환한다. \n
/// @param (int*) pnOffset : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CHorizontalTickerSchedule::GetOffset(int* pnOffset)
{
	m_csOffset.Lock();
	(*pnOffset) = m_offset;
	m_csOffset.Unlock();
}


void CALLBACK CHorizontalTickerSchedule::timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2)
{
	::PostMessage((HWND)dwUser, CAPTION_SHIFT_ID, 0, 0);
}

bool CHorizontalTickerSchedule::CreateFile()
{
	double xscale, yscale;
	m_pParentParentWnd->GetScale(xscale, yscale);

	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}

	if(!IsAvailableFont(m_font.c_str()))
	{
		CString strError;
		strError.Format("HorizontalTicker schedule create font[%s] fail !!!", m_font.c_str());
		__WARNING__(strError, _NULL);
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, "System");
	}
	else
	{
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, m_font.c_str());
	}//if

	if(m_bUseUnicode)
	{
		m_strTickerW = L"";
		//CStringW header = L"▶";
		for(int i=0; i<TICKER_COUNT; i++)
		{
			if(m_commentW[i].GetLength() > 0)
			{
				//m_strTickerW.Append(header);
				m_strTickerW.Append(m_commentW[i]);
				m_strTickerW.Append(L"    ");
			}//if
		}//if
		m_strTickerW.Replace(L"&", L"&&");
	}
	else
	{
		m_strTicker = "";
		//CString header;
		//header.LoadString(IDS_TICKER_HEADER);
		for(int i=0; i<TICKER_COUNT; i++)
		{
			if(m_comment[i].length() > 0)
			{
				//m_strTicker.Append(header);
				m_strTicker.Append(m_comment[i].c_str());
				m_strTicker.Append("    ");
			}//if
		}//if
		m_strTicker.Replace("&", "&&");
	}

	if(GetSafeHwnd())
	{
		CPaintDC dc(this);
		dc.SelectObject(&m_fontTicker);
		if( m_bUseUnicode )
		{
			ASSERT(dc.m_hDC != NULL);
			SIZE size;
			VERIFY(::GetTextExtentPoint32W(dc.m_hDC, m_strTickerW, m_strTickerW.GetLength(), &size));

			m_nTickerWidth = size.cx;
		}
		else
		{
			CSize text_size = dc.GetOutputTextExtent(m_strTicker);
			m_nTickerWidth = text_size.cx;
		}

		//
		CRect client_rect;
		GetClientRect(client_rect);
		m_nWidth = client_rect.Width();
		m_nHeight = client_rect.Height();
	}

	m_strLogFileString = "";
	for(int i=0; i<TICKER_COUNT; i++)
	{
		m_strLogFileString += m_comment[i].c_str();
	}//for

	if(m_strLogFileString.GetLength() > 255)
	{
		m_strLogFileString = m_strLogFileString.Left(255);
	}//if

	return true;
}

int ANSItoUNICODE(LPCSTR lpszANSI, CStringW& strUnicode)
{
	WCHAR lpszOut[1024];

	::MultiByteToWideChar(949, 0, lpszANSI, -1, lpszOut, 1024);
	strUnicode = lpszOut;

	return 0;
}


void CHorizontalTickerSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	for(int i=0; i<TICKER_COUNT; i++)
	{
		if(ciArgParser::getInstance()->isSet("+japan"))
		{
			CStringW	wstr;
			ANSItoUNICODE(m_comment[i].c_str(), wstr);
			m_commentW[i] = wstr;
			m_bUseUnicode = true;
		}
		else
			m_bUseUnicode = false;
	}//for

	m_bgColor = MNG_PROFILE_READ(lpszID, "bgColor");
	m_fgColor = MNG_PROFILE_READ(lpszID, "fgColor");
	m_font = MNG_PROFILE_READ(lpszID, "font");
	m_fontSize = atoi(MNG_PROFILE_READ(lpszID, "fontSize"));
	m_playSpeed = atoi(MNG_PROFILE_READ(lpszID, "playSpeed"));
	m_direction = atoi(MNG_PROFILE_READ(lpszID, "direction"));

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}//if

	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}//if

	//속도를 총 5단게로 한다.
	//빠름(1~49) >> 조금빠름(50~99) >> 보통(100~199) >> 조금느림(200~499) >> 아주느림(500~1000)
	if(m_playSpeed < 50)
	{
		//빠름(1~49)
		m_playSpeed = E_TICKER_FAST;
	}
	else if(m_playSpeed >= 50 && m_playSpeed < 100)
	{
		//조금빠름(50~99)
		m_playSpeed = E_TICKER_LITTLE_FAST;
	}
	else if(m_playSpeed >= 100 && m_playSpeed < 200)
	{
		//보통(100~199)
		m_playSpeed = E_TICKER_NORMAL;
	}
	else if(m_playSpeed >= 200 && m_playSpeed < 500)
	{
		//조금느림(200~499)
		m_playSpeed = E_TICKER_LITTLE_SLOW;
	}
	else if(m_playSpeed >= 500 && m_playSpeed <= 1000)
	{
		//아주느림(500~1000)
		m_playSpeed = E_TICKET_SLOW;
	}//if

	//CreateFile();

	__DEBUG_SCHEDULE_END__(lpszID)
}


double CHorizontalTickerSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CHorizontalTickerSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CHorizontalTickerSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		__DEBUG__("\t\t\tAlready Open", _NULL);
		return true;
	}

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			SetNoContentsFile();
		}//if
	}

	CreateFile();
	
	m_bOpen = true;

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CHorizontalTickerSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();
		
		m_bOpen = false;
		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if

		if(m_fontTicker.GetSafeHandle())
		{
			m_fontTicker.DeleteObject();
		}//if
	}
	return true;
}

bool CHorizontalTickerSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CSchedule::Play();

	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	double xscale, yscale;
	m_pParentParentWnd->GetScale(xscale, yscale);

	m_dwStartTick = ::GetTickCount();
	m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;

	//속도를 총 5단게로 한다.
	switch(m_playSpeed)
	{
	case E_TICKER_FAST:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP*3;
			m_nMS = 20;	//시스템에 부하를 적게주는 최대속도이다.
		}
		break;
	case E_TICKER_LITTLE_FAST:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP*2;
			m_nMS = 20;
		}
		break;
	case E_TICKER_NORMAL:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;
			m_nMS = 20;
		}
		break;
	case E_TICKER_LITTLE_SLOW:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;
			m_nMS = 40;
		}
		break;
	case E_TICKET_SLOW:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;
			m_nMS = 80;
		}
		break;
	default:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;
			m_nMS = 20;
		}
	}//switch

#ifdef USE_MMTIMER

	m_nMS = m_playSpeed / 10 / xscale; // 스피드 보정
	//m_nMS = m_playSpeed; // 스피드 보정
	if(m_nMS < TIMER_MINIMUM_TIME) m_nMS = TIMER_MINIMUM_TIME;

	TIMECAPS tc;
	timeGetDevCaps(&tc, sizeof(TIMECAPS));

	if(m_nMS < tc.wPeriodMin) m_nMS = tc.wPeriodMin;
	else if(m_nMS > tc.wPeriodMax) m_nMS = tc.wPeriodMax;

	timeBeginPeriod(m_nMS);
	m_offset = 0;

	if(m_nTimerId == 0)
		m_nTimerId = timeSetEvent(m_nMS, m_nMS, CHorizontalTickerSchedule::timeProc, (DWORD)m_hWnd, TIME_PERIODIC);

	if(!m_nTimerId)
	{
		OnErrorMessage((WPARAM)"Multimedia timer setting error", 0);
	}

#else

	//__DEBUG__("\t\t\tWindow Timer Mode", m_nMS);
	//SetTimer(CAPTION_SHIFT_ID, m_nMS, NULL);
	m_pthreadShift = ::AfxBeginThread(CHorizontalTickerSchedule::CaptionShiftThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_pthreadShift->m_bAutoDelete = TRUE;
	m_bExitThread = false;
	m_hevtShift = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_offset = 0;
	m_pthreadShift->ResumeThread();

#endif

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

UINT CHorizontalTickerSchedule::CaptionShiftThread(LPVOID pParam)
{
	DWORD dwEventRet = 0;
	CHorizontalTickerSchedule* pParent = (CHorizontalTickerSchedule*)pParam;

	while(!pParent->m_bExitThread)
	{
		dwEventRet = WaitForSingleObject(pParent->m_hevtShift, pParent->m_nMS);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			//CloseHandle(pParent->m_hevtShift);
			__DEBUG__("Event set : CaptionShiftThread", _NULL);
			break;
		}//if

		//__DEBUG__("Shift ticker", pParent->m_nMS);
		pParent->DecreaseOffset();
		
	}//while

	//CloseHandle(pParent->m_hevtShift);
	__DEBUG__("Exit CaptionShiftThread", _NULL);

	return 1;
}

bool CHorizontalTickerSchedule::Pause()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CHorizontalTickerSchedule::Stop()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

#ifdef USE_MMTIMER

	timeKillEvent(m_nTimerId);
	timeEndPeriod(m_nMS);
	m_nTimerId = 0;

#else

	//KillTimer(CAPTION_SHIFT_ID);
	SetEvent(m_hevtShift);
	m_bExitThread = true;
	CloseHandle(m_hevtShift);
	m_hevtShift = NULL;

#endif

	if(m_bFileNotExist)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "Content file does not exist", m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


CString	CHorizontalTickerSchedule::ToString()
{
	CString str;
	str.Format(
		"-Horzontal Ticker Schedule-\r\n"
		"%s\r\n"
		"%s\r\n"
		"bgColor = %s\r\n"
		"fgColor = %s\r\n"
		"font = %s\r\n"
		"fontSize = %d\r\n"
		"playSpeed = %d\r\n"
		"direction = %d\r\n"
		"\r\n"
		"m_offset = %d\r\n"
		"m_strTicker = %s\r\n"
		"m_nTickerWidth = %d\r\n"
		"m_nWidth = %d\r\n"
		"m_nHeight = %d\r\n"
		"m_nTimerId = %d\r\n"
		"m_nMS = %d\r\n"
		,	CSchedule::ToString()
		,	to_string(m_comment, TICKER_COUNT)
		,	m_bgColor.c_str()		// 자막용
		,	m_fgColor.c_str()		// 자막용
		,	m_font.c_str()			// 자막용
		,	m_fontSize		// 자막용
		,	m_playSpeed	// 자막용
		,	m_direction

		,	m_offset
		,	m_strTicker 
		,	m_nTickerWidth
		,	m_nWidth
		,	m_nHeight
		,	m_nTimerId
		,	m_nMS
	);
	return str;
}

void CHorizontalTickerSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("HORIZONTAL TICKER Schedule Attributes");

	to_string(m_comment, TICKER_COUNT, pGrid, hs);

	pGrid->AddStringItem(hs, "bgColor", m_bgColor.c_str());
	pGrid->AddStringItem(hs, "fgColor", m_fgColor.c_str());
	pGrid->AddStringItem(hs, "font", m_font.c_str());
	pGrid->AddIntegerItem(hs, "fontSize", m_fontSize);
	pGrid->AddIntegerItem(hs, "playSpeed", m_playSpeed);
	pGrid->AddIntegerItem(hs, "direction", m_direction);

	hs = pGrid->AddSection("member variables");

	pGrid->AddIntegerItem(hs, "m_offset", m_offset);
	pGrid->AddStringItem(hs, "m_strTicker", (LPCSTR)m_strTicker);
	pGrid->AddIntegerItem(hs, "m_nTickerWidth", m_nTickerWidth);
	pGrid->AddIntegerItem(hs, "m_nWidth", m_nWidth);
	pGrid->AddIntegerItem(hs, "m_nHeight", m_nHeight);
	pGrid->AddIntegerItem(hs, "m_nTimerId", m_nTimerId);
	pGrid->AddIntegerItem(hs, "m_nMS", m_nMS);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CVerticalTickerSchedule

CVerticalTickerSchedule::CVerticalTickerSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_offset (0)
,	m_nTickerIndex (0)
,	m_nDelayCount (0)
,	m_nTimerId (0)
,	m_rgbBgColor(RGB(0,0,0))
,	m_rgbFgColor(RGB(255,255,255))
,	m_strLogFileString("")
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CVerticalTickerSchedule::~CVerticalTickerSchedule()
{
	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if

	__DEBUG__("\t\t\tDestroy Vertical Schedule", m_scheduleId.c_str());

	//CloseFile();
}


IMPLEMENT_DYNAMIC(CVerticalTickerSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CVerticalTickerSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_MESSAGE(CAPTION_SHIFT_ID, OnCaptionShift)
END_MESSAGE_MAP()


int CVerticalTickerSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	//
	CRect client_rect;
	GetClientRect(client_rect);
	m_nWidth = client_rect.Width();
	m_nHeight = client_rect.Height();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return 0;
}

void CVerticalTickerSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}


void CVerticalTickerSchedule::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSchedule::OnTimer(nIDEvent);

	if(nIDEvent == CAPTION_SHIFT_ID)
	{
		OnCaptionShift(0,0);
	}
}

void CVerticalTickerSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	double xscale, yscale;
	m_pParentParentWnd->GetScale(xscale, yscale);

	CRect rtClient;
	GetClientRect(rtClient);

	CMemDC memDC(&dc);

	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		if(m_rgbFgColor == RGB(255, 255, 255))
		{
			memDC.FillSolidRect(rtClient, RGB(0, 0, 0));
		}
		else
		{
			memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		}//if
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}
	else if(m_bgImage.IsValid())
	{
#if 1
		// 바둑판
		for(int x=0; x<rtClient.Width(); x+=m_bgImage.GetWidth())
			for(int y=0; y<rtClient.Height(); y+=m_bgImage.GetHeight())
				m_bgImage.Draw2(memDC.GetSafeHdc(), x, y);
#else
		// 늘이기
		m_bgImage.Draw(memDC.GetSafeHdc(), 0, 0, rtClient.Width(), rtClient.Height());
#endif
	}

	memDC.SetBkMode(1);
	memDC.SetTextColor(m_rgbFgColor);

	CFont	tmp_font;
	CSize	text_size;
	int		size;

	// ticker1
	memDC.SelectObject(&m_fontTicker);
	text_size = memDC.GetOutputTextExtent(m_strTicker1);
	size = m_fontSize;
	if(m_nWidth < text_size.cx)
	{
		do
		{
			size = (size*9)/10;
			if(tmp_font.GetSafeHandle())
			{
				tmp_font.DeleteObject();
			}//if

			if(!IsAvailableFont(m_font.c_str()))
			{
				CString strError;
				strError.Format("VerticalTicker schedule create font[%s] fail !!!", m_font.c_str());
				__WARNING__(strError, _NULL);
				tmp_font.CreatePointFont(size*10 * xscale, "System");
			}
			else
			{
				tmp_font.CreatePointFont(size*10 * xscale, m_font.c_str());
			}//if

			memDC.SelectObject(&tmp_font);
			text_size = memDC.GetOutputTextExtent(m_strTicker1);
		}
		while(m_nWidth < text_size.cx);
	}//if

	CRect rect(0, m_offset, m_nWidth, m_offset+m_nHeight);
	memDC.DrawText(m_strTicker1, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	// ticker2
	memDC.SelectObject(&m_fontTicker);
	text_size = memDC.GetOutputTextExtent(m_strTicker2);
	size = m_fontSize;
	if(m_nWidth < text_size.cx)
	{
		do
		{
			size = (size*9)/10;
			if(tmp_font.GetSafeHandle())
			{
				tmp_font.DeleteObject();
			}//if

			if(!IsAvailableFont(m_font.c_str()))
			{
				CString strError;
				strError.Format("VerticalTicker schedule create font[%s] fail !!!", m_font.c_str());
				__WARNING__(strError, _NULL);
				tmp_font.CreatePointFont(size*10 * xscale, "System");
			}
			else
			{
				tmp_font.CreatePointFont(size*10 * xscale, m_font.c_str());
			}//if

			memDC.SelectObject(&tmp_font);
			text_size = memDC.GetOutputTextExtent(m_strTicker2);
		}
		while(m_nWidth < text_size.cx);
	}//if

	rect.OffsetRect(0, m_nHeight);
	memDC.DrawText(m_strTicker2, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

}

LRESULT CVerticalTickerSchedule::OnCaptionShift(WPARAM wParam, LPARAM lParam)
{
	if(m_nDelayCount == 0)
	{
		m_offset -= VERTICAL_TICKER_CAPTION_SHIFT_STEP;

		if(m_offset == -m_nHeight)
		{
			m_offset = 0;
			m_nDelayCount = 30;

			m_strTicker1 = m_strTicker2;
			m_nTickerIndex++;
			if(m_nTickerIndex > m_nMaxTicker)
				m_nTickerIndex = 0;
			m_strTicker2 = m_comment[m_nTickerIndex].c_str();
		}

		Invalidate(FALSE);
	}
	else 
		m_nDelayCount--;

	return 0;
}

void CALLBACK CVerticalTickerSchedule::timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2)
{
	::PostMessage((HWND)dwUser, CAPTION_SHIFT_ID, 0, 0);
}

bool CVerticalTickerSchedule::CreateFile()
{
	double xscale, yscale;
	m_pParentParentWnd->GetScale(xscale, yscale);

	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if

	if(!IsAvailableFont(m_font.c_str()))
	{
		CString strError;
		strError.Format("VerticalTicker schedule create font[%s] fail !!!", m_font.c_str());
		__WARNING__(strError, _NULL);
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, "System");
	}
	else
	{
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, m_font.c_str());
	}//if

	for(int i=0; i<TICKER_COUNT; i++)
	{
		if(m_comment[i].length() > 0)
		{
			m_nMaxTicker = i;
		}
	}

	if(GetSafeHwnd())
	{
		//
		CRect client_rect;
		GetClientRect(client_rect);
		m_nWidth = client_rect.Width();
		m_nHeight = client_rect.Height();
	}

	m_strLogFileString = "";
	for(int i=0; i<TICKER_COUNT; i++)
	{
		m_strLogFileString += m_comment[i].c_str();
	}//for

	if(m_strLogFileString.GetLength() > 255)
	{
		m_strLogFileString = m_strLogFileString.Left(255);
	}//if

	return true;
}


void CVerticalTickerSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	m_bgColor = MNG_PROFILE_READ(lpszID, "bgColor");
	m_fgColor = MNG_PROFILE_READ(lpszID, "fgColor");
	m_font = MNG_PROFILE_READ(lpszID, "font");
	m_fontSize = atoi(MNG_PROFILE_READ(lpszID, "fontSize"));
	m_playSpeed = atoi(MNG_PROFILE_READ(lpszID, "playSpeed"));

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}

	//CreateFile();

	__DEBUG_SCHEDULE_END__(lpszID)
}


double CVerticalTickerSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CVerticalTickerSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CVerticalTickerSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		__DEBUG__("\t\t\tAlready Open", _NULL);
		return true;
	}

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			SetNoContentsFile();
		}//if
	}

	CreateFile();

	m_bOpen = true;

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CVerticalTickerSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();
		
		m_bOpen = false;
		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if

		if(m_fontTicker.GetSafeHandle())
		{
			m_fontTicker.DeleteObject();
		}//if
	}
	return true;
}

bool CVerticalTickerSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CSchedule::Play();

	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	m_strTicker1 = "";
	m_strTicker2 = m_comment[0].c_str();

	double xscale, yscale;
	m_pParentParentWnd->GetScale(xscale, yscale);

	//
	m_dwStartTick = ::GetTickCount();

	m_nMS = m_playSpeed / 5 / yscale; // 스피드 보정
	//m_nMS = m_playSpeed; // 스피드 보정
	if(m_nMS < TIMER_MINIMUM_TIME) m_nMS = TIMER_MINIMUM_TIME;

#ifdef USE_MMTIMER

	TIMECAPS tc;
	timeGetDevCaps(&tc, sizeof(TIMECAPS));

	if(m_nMS < tc.wPeriodMin) m_nMS = tc.wPeriodMin;
	else if(m_nMS > tc.wPeriodMax) m_nMS = tc.wPeriodMax;

	timeBeginPeriod(m_nMS);

	if(m_nTimerId == 0)
		m_nTimerId = timeSetEvent(m_nMS, m_nMS, CVerticalTickerSchedule::timeProc, (DWORD)m_hWnd, TIME_PERIODIC);

	if(!m_nTimerId)
	{
		OnErrorMessage((WPARAM)"Multimedia timer setting error !!!", 0);
	}

#else

	__DEBUG__("\t\t\tWindow Timer Mode", _NULL);
	SetTimer(CAPTION_SHIFT_ID, m_nMS, NULL);

#endif

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CVerticalTickerSchedule::Pause()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CVerticalTickerSchedule::Stop()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

#ifdef USE_MMTIMER

	timeKillEvent(m_nTimerId);
	timeEndPeriod(m_nMS);
	m_nTimerId = 0;

#else

	KillTimer(CAPTION_SHIFT_ID);

#endif

	if(m_bFileNotExist)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "Content file does not exist", m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


CString	CVerticalTickerSchedule::ToString()
{
	CString str;
	str.Format(
		"-Horzontal Ticker Schedule-\r\n"
		"%s\r\n"
		"%s\r\n"
		"bgColor = %s\r\n"
		"fgColor = %s\r\n"
		"font = %s\r\n"
		"fontSize = %d\r\n"
		"playSpeed = %d\r\n"
		"\r\n"
		"m_nTickerIndex = %d\r\n"
		"m_nMaxTicker = %d\r\n"
		"m_nDelayCount = %d\r\n"
		"m_offset = %d\r\n"
		"m_strTicker1 = %s\r\n"
		"m_strTicker2 = %s\r\n"
		"m_nWidth = %d\r\n"
		"m_nHeight = %d\r\n"
		"m_nTimerId = %d\r\n"
		"m_nMS = %d\r\n"

		,	CSchedule::ToString()
		,	to_string(m_comment, TICKER_COUNT)
		,	m_bgColor.c_str()		// 자막용
		,	m_fgColor.c_str()		// 자막용
		,	m_font.c_str()			// 자막용
		,	m_fontSize		// 자막용
		,	m_playSpeed	// 자막용

		,	m_nTickerIndex
		,	m_nMaxTicker
		,	m_nDelayCount
		,	m_offset
		,	m_strTicker1
		,	m_strTicker2
		,	m_nWidth
		,	m_nHeight
		,	m_nTimerId
		,	m_nMS
	);
	return str;
}

void CVerticalTickerSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("VERTICAL TICKER Schedule Attributes");

	to_string(m_comment, TICKER_COUNT, pGrid, hs);

	pGrid->AddStringItem(hs, "bgColor", m_bgColor.c_str());
	pGrid->AddStringItem(hs, "fgColor", m_fgColor.c_str());
	pGrid->AddStringItem(hs, "font", m_font.c_str());
	pGrid->AddIntegerItem(hs, "fontSize", m_fontSize);
	pGrid->AddIntegerItem(hs, "playSpeed", m_playSpeed);

	hs = pGrid->AddSection("member variables");

	pGrid->AddIntegerItem(hs, "m_nTickerIndex", m_nTickerIndex);
	pGrid->AddIntegerItem(hs, "m_nMaxTicker", m_nMaxTicker);
	pGrid->AddIntegerItem(hs, "m_nDelayCount", m_nDelayCount);
	pGrid->AddIntegerItem(hs, "m_offset", m_offset);
	pGrid->AddStringItem(hs, "m_strTicker1", (LPCSTR)m_strTicker1);
	pGrid->AddStringItem(hs, "m_strTicker2", (LPCSTR)m_strTicker2);
	pGrid->AddIntegerItem(hs, "m_nWidth", m_nWidth);
	pGrid->AddIntegerItem(hs, "m_nHeight", m_nHeight);
	pGrid->AddIntegerItem(hs, "m_nTimerId", m_nTimerId);
	pGrid->AddIntegerItem(hs, "m_nMS", m_nMS);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CRSSSchedule


CRSSSchedule::CRSSSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CFlashSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
, m_strRssFlashPath("")
, m_nRssType(0)
, m_strErrorString("")
, m_strCategoryString("")
, m_strLineCount("")
{
	SetSchedule(lpszID);
}

CRSSSchedule::~CRSSSchedule()
{
	__DEBUG__("\t\t\tDestroy RSS Schedule", m_scheduleId.c_str());

	//CloseFile();
}

IMPLEMENT_DYNAMIC(CRSSSchedule, CFlashSchedule)

BEGIN_MESSAGE_MAP(CRSSSchedule, CFlashSchedule)
END_MESSAGE_MAP()


bool CRSSSchedule::CreateFile()
{
	CRect rtClient;
	GetClientRect(&rtClient);
	m_width = rtClient.Width();
	m_height = rtClient.Height();

	//Rss 플래쉬 파일의 경로
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);
	m_strRssFlashPath = szDrive;
	m_strRssFlashPath += szPath;

	m_nRssType = atoi(m_comment[2].c_str());		//Rss 종류
	m_strErrorString = m_comment[3].c_str();		//장애 문자
	m_strCategoryString = m_comment[4].c_str();		//카테고리
	m_strLineCount = m_comment[5].c_str();			//라인 수

	unsigned short LangType = PRIMARYLANGID(GetSystemDefaultLangID());  //프라이머리 언어만 뽑는다.
	if(LangType==LANG_JAPANESE)    //일문 윈도우 
	{
		m_strRssFlashPath += "flash\\JP_";
	}
	else
	{
		m_strRssFlashPath += "flash\\";
	}//if

	switch(m_nRssType)
	{
	case E_RSS_ONE_LINE:		//한 라인 Rss
		{
			m_strRssFlashPath += "Con_rss.swf";
		}
		break;
	case E_RSS_MULTI_LINE:		//Multi line Rss
		{
			m_strRssFlashPath += "Con_rss_line.swf";
		}
		break;
	case E_RSS_DETAIL:		//내용을 보여주는 Rss
		{
			m_strRssFlashPath += "Con_rss_description.swf";
		}
		break;
	default:
		{
			m_strRssFlashPath += "Con_rss.swf";
		}
	}//switch

	//RSS 플래쉬 파일이 존재하여야 한다.
	CFileStatus status;
	if(CFile::GetStatus(m_strRssFlashPath, status ) == FALSE)
	{
		SetNoContentsFile();
	}//if

	//장애 이미지의 경로
	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}//if
	
	return true;
}


void CRSSSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	m_bgColor = MNG_PROFILE_READ(lpszID, "bgColor");
	m_fgColor = MNG_PROFILE_READ(lpszID, "fgColor");
	m_fontSize = atoi(MNG_PROFILE_READ(lpszID, "fontSize"));

	__DEBUG_SCHEDULE_END__(lpszID)
}


bool CRSSSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CSchedule::Play();
	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	if(m_bFileNotExist)
	{
		return true;
	}//if

	if(m_pdlgFlash)
	{
		m_pdlgFlash->Stop();
		m_pdlgFlash->DestroyWindow();
		delete m_pdlgFlash;
		m_pdlgFlash = NULL;
	}//if

	m_pdlgFlash = new CFlashDlg;
	m_pdlgFlash->Create(IDD_FLASH_DLG, this);
	m_pdlgFlash->ShowWindow(SW_HIDE);
	CRect rtRect;
	GetClientRect(&rtRect);
	m_pdlgFlash->MoveWindow(0, 0, rtRect.Width(), rtRect.Height());

	if(!m_pdlgFlash->Play(m_strRssFlashPath, (CWnd*)this))
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "Fail to play flash movie", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	m_bPlaying = true;
	
	//if(m_pFlash != NULL)
	//{
		//fscommand를 사용하지 않으므로 플래쉬가 로딩이 완료될때까지 기다린다.
		//Sleep(500);

		CString strFgColor = "0x";
		strFgColor += m_fgColor.c_str();
		
		CString strBgColor = "0x";
		strBgColor += m_bgColor.c_str();
		
		CString strCall;
		if(m_nRssType == E_RSS_MULTI_LINE)
		{
			strCall.Format("<invoke name=\'makeRss\'><arguments><string>%s</string><string>%s</string><string>%d</string><string>%s</string>\
<string>%s</string><string>%d</string><string>%d</string><string>%s</string><string>%s</string><string>%s</string></arguments></invoke>",
			m_comment[0].c_str(), m_strMediaFullPath, m_fontSize, strFgColor, strBgColor, m_width, m_height,
			m_strErrorString, m_strCategoryString, m_strLineCount);
		}
		else
		{
			strCall.Format("<invoke name=\'makeRss\'><arguments><string>%s</string><string>%s</string><string>%d</string><string>%s</string>\
<string>%s</string><string>%d</string><string>%d</string><string>%s</string><string>%s</string></arguments></invoke>",
			m_comment[0].c_str(), m_strMediaFullPath, m_fontSize, strFgColor, strBgColor, m_width, m_height,
			m_strErrorString, m_strCategoryString);
		}//if
		
		__DEBUG__(strCall, _NULL);
		//m_pFlash->CallFunction(strCall);
		m_pdlgFlash->CallFunc(strCall);
	//}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


CString	CRSSSchedule::ToString()
{
	CString str;
	str.Format(
		"-RSS Schedule-\r\n"
		"%s\r\n"
		"bgColor = %s\r\n"
		"fgColor = %s\r\n"
		"fontSize = %d\r\n"
		,	CFlashSchedule::ToString()
		,	m_bgColor
		,	m_fgColor
		,	m_fontSize
	);
	return str;
}

void CRSSSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CFlashSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("RSS Schedule Attributes");

	hs = pGrid->AddSection("member variables");

	pGrid->AddStringItem(hs, "m_bgColor", m_bgColor.c_str());
	pGrid->AddStringItem(hs, "m_fgColor", m_fgColor.c_str());
	pGrid->AddIntegerItem(hs, "m_fontSize", m_fontSize);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CWizardSchedule

CWizardSchedule::CWizardSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CFlashSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_wizardXML("")
,	m_wizardFiles("")
,	m_strWizardFlashPath("")
{
	SetSchedule(lpszID);
}

CWizardSchedule::~CWizardSchedule()
{
	__DEBUG__("\t\t\tDestroy Wizard Schedule", m_scheduleId.c_str());

	//CloseFile();
}

IMPLEMENT_DYNAMIC(CWizardSchedule, CFlashSchedule)

BEGIN_MESSAGE_MAP(CWizardSchedule, CFlashSchedule)
	//ON_MESSAGE(WM_FLASH_RECV_FSCOMMAND, OnRecvFsCommand)
END_MESSAGE_MAP()


bool CWizardSchedule::CreateFile()
{
	//Rss 플래쉬 파일의 경로
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);
	m_strWizardFlashPath = szDrive;
	m_strWizardFlashPath += szPath;
	m_strWizardFlashPath += "\\FlashWizard\\";
	m_strWizardFlashPath += m_comment[0].c_str();

	//Wizard 플래쉬 파일이 존재하여야 한다.
	CFileStatus status;
	if(CFile::GetStatus(m_strWizardFlashPath, status ) == FALSE)
	{
		SetNoContentsFile();
	}//if

	//장애 이미지의 경로
	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}//if
	
	return true;
}


void CWizardSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	m_wizardXML = MNG_PROFILE_READ(lpszID, "wizardXML");
	m_wizardFiles = MNG_PROFILE_READ(lpszID, "wizardFiles");

	__DEBUG_SCHEDULE_END__(lpszID)
}


bool CWizardSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CSchedule::Play();
	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	if(m_bFileNotExist)
	{
		return true;
	}//if

	m_pdlgFlash = new CFlashDlg;
	m_pdlgFlash->Create(IDD_FLASH_DLG, this);
	m_pdlgFlash->ShowWindow(SW_HIDE);
	CRect rtRect;
	GetClientRect(&rtRect);
	m_pdlgFlash->MoveWindow(0, 0, rtRect.Width(), rtRect.Height());

	if(!m_pdlgFlash->Play(m_strWizardFlashPath, (CWnd*)this))
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "Fail to play flash movie", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	m_bPlaying = true;

	if(/*m_pFlash != NULL && */m_wizardXML.length() != 0)
	{
		CString strCall;
	
		//strCall.Format("<invoke name=\'makeFlash\'><arguments><string>%s</string></arguments></invoke>", m_wizardXML.c_str());
		strCall.Format("<invoke name=\'makeFlash\'><arguments><string>%s</string><string>%s</string></arguments></invoke>",
							theApp.GetWizardXML(), m_contentsId.c_str());
		strCall.Remove(0x09);
		strCall.Replace(" xml:space=\"preserve\"", "");
		strCall.Replace(" xmlns=\"http://www.w3.org/XML/1998/namespace\"", "");
/*
		//////////////////////////////////////////////////////////////////////////
		string strUTF8;
		CEncoding*	pEncoder = CEncoding::getInstance();
		if(!pEncoder->AnsiToUTF8(strCall, strUTF8))
		{
			__DEBUG__("Fail to incoding to UTF-8", _NULL);
		}
		else
		{
#ifdef _DEBUG
			//////////////////////////////////////////////////
			string strAnsi;
			pEncoder->UTF8ToAnsi(strUTF8.c_str(), strAnsi);
			//////////////////////////////////////////////////
#endif
		}//if
		//////////////////////////////////////////////////////////////////////////
*/
		//__DEBUG__(strCall, _NULL);
		m_pdlgFlash->CallFunc(strCall);
		//m_pdlgFlash->CallFunc(strUTF8.c_str());
		/*////////////////////////////////////////////////////////////////////////
		CFile file;
		CString strPath = GetAppPath();
		strPath += "\\BRW_UTF-8.xml";
		file.Open(strPath, CFile::modeCreate|CFile::modeWrite);
		//BYTE btHeader[3] = { 0xEF, 0xBB, 0xBF };
		//file.Write(btHeader, 3);
		//file.Write((void*)strUTF8.c_str(), strUTF8.length());
		file.Write((void*)strCall.GetBuffer(strCall.GetLength()), strCall.GetLength());
		file.Close();
		strCall.ReleaseBuffer();
		////////////////////////////////////////////////////////////////////////*/
	}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


CString	CWizardSchedule::ToString()
{
	CString str;
	str.Format(
		"-Wizard Schedule-\r\n"
		"%s\r\n"
		"wizardXML = %s\r\n"
		"wizardFiles = %s\r\n"
		,	CFlashSchedule::ToString()
		,	m_wizardXML
		,	m_wizardFiles
	);
	return str;
}


void CWizardSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CFlashSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("Wizard Schedule Attributes");

	hs = pGrid->AddSection("member variables");

	pGrid->AddStringItem(hs, "wizardXML", m_wizardXML.c_str());
	pGrid->AddStringItem(hs, "wizardFiles", m_wizardFiles.c_str());
}

/*
LRESULT CWizardSchedule::OnRecvFsCommand(WPARAM wParam, LPARAM lParam)
{
	CString strCmd, strArg;
	strCmd = (LPCSTR)wParam;
	strArg = (LPCSTR)lParam;

	// 플래쉬 위자드에서 넘어오는 형태 (생성여부, 컨텐츠 Id)
	if(strArg.Find("Contents_", 0) == -1)
	{
		strArg.Format("Contents_%s", strArg);
	}//if

	char cBuffer[BUF_SIZE] = { 0x00 };
		
#ifdef _FELICA_
	//위자드에서 새로운 플래쉬를 생성시키면 펠리카를 설정해주어야 한다.
	GetPrivateProfileString(strArg, "comment2", "", cBuffer, BUF_SIZE, GetSchedulePath());
	CString strFelica = cBuffer;
	if(strFelica.GetLength() != 0)
	{
		CString strProtocol = strFelica.Left(4);
		if(strProtocol.CompareNoCase("http") == 0)
		{
			__DEBUG__("FELICA - setURL", strFelica);
			felicaUtil::getInstance()->setURL(strFelica);
			m_bFelicaSet = true;
		}//if
	}
	else
	{
		if(m_bFelicaSet)
		{
			__DEBUG__("FELICA - unsetURL", _NULL);
			felicaUtil::getInstance()->unsetURL();
			m_bFelicaSet = false;
		}//if
	}//if
#endif

	if(strCmd == "0")
	{
		GetPrivateProfileString(strArg, "wizardXML", "", cBuffer, BUF_SIZE, GetSchedulePath());
		CString strXML = cBuffer;

		if(m_pdlgFlash != NULL && strXML.GetLength() != 0)
		{
			CString strCall;
			strCall.Format("<invoke name=\'makeFlash\'><arguments><string>%s</string></arguments></invoke>", strXML);
			strCall.Remove(0x09);
			strCall.Replace(" xml:space=\"preserve\"", "");
			strCall.Replace(" xmlns=\"http://www.w3.org/XML/1998/namespace\"", "");
			__DEBUG__(strCall, _NULL);
			m_pdlgFlash->CallFunc(strCall);
		}//if
	}//if

	return 0;
}
*/


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FsCommand를 처리한다. \n
/// @param (CString) strCmd : (in) command
/// @param (CString) strArg : (in) args
/////////////////////////////////////////////////////////////////////////////////
void CWizardSchedule::RecvFsCommand(CString strCmd, CString strArg)
{
	CString strTmp;
	strTmp.Format("command : %s args : %s", strCmd, strArg);
	__DEBUG__(strTmp, _NULL);

	// 플래쉬 위자드에서 넘어오는 형태 (생성여부, 컨텐츠 Id)
	if(strArg.Find("Contents_", 0) == -1)
	{
		strArg.Format("Contents_%s", strArg);
	}//if
	
#ifdef _FELICA_
	//위자드에서 새로운 플래쉬를 생성시키면 펠리카를 설정해주어야 한다.
	CString strFelica = MNG_PROFILE_READ(strArg, "comment2");
	
	if(strFelica.GetLength() != 0)
	{
		CString strProtocol = strFelica.Left(4);
		if(strProtocol.CompareNoCase("http") == 0)
		{
			__DEBUG__("FELICA - setURL", strFelica);
			felicaUtil::getInstance()->setURL(strFelica);
			m_bFelicaSet = true;
		}//if
	}
	else
	{
		if(m_bFelicaSet)
		{
			__DEBUG__("FELICA - unsetURL", _NULL);
			felicaUtil::getInstance()->unsetURL();
			m_bFelicaSet = false;
		}//if
	}//if
#endif
/*
	if(strCmd == "0")
	{
		CString strXML = MNG_PROFILE_READ(strArg, "wizardXML");
		if(strXML.GetLength() == 0)
		{
			__DEBUG__("strXML length is 0", _NULL);
		}//if
		
		if(m_pdlgFlash != NULL && strXML.GetLength() != 0)
		{
			CString strCall;
			strCall.Format("<invoke name=\'makeFlash\'><arguments><string>%s</string></arguments></invoke>", strXML);
			strCall.Remove(0x09);
			strCall.Replace(" xml:space=\"preserve\"", "");
			strCall.Replace(" xmlns=\"http://www.w3.org/XML/1998/namespace\"", "");
			__DEBUG__(strCall, _NULL);
			m_pdlgFlash->CallFunc(strCall);
		}//if
	}//if
*/
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CTypingTickerSchedule


CTypingTickerSchedule::CTypingTickerSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_nCommentIndex (0)
,	m_rgbBgColor(RGB(0,0,0))
,	m_rgbFgColor(RGB(255,255,255))
,	m_strLogFileString("")
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CTypingTickerSchedule::~CTypingTickerSchedule()
{
	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if

	__DEBUG__("\t\t\tDestroy Horizontal Schedule", m_scheduleId.c_str());

	//CloseFile();
}


IMPLEMENT_DYNAMIC(CTypingTickerSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CTypingTickerSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


int CTypingTickerSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	GetClientRect(m_rectTicker);
	m_rectTicker.DeflateRect(10,10);

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return 0;
}

void CTypingTickerSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}

void CTypingTickerSchedule::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSchedule::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case CAPTION_NEXT_ID:
		KillTimer(CAPTION_NEXT_ID);

		if(m_nCommentIndex > m_nCommentCount)
			m_nCommentIndex = 0;

		m_wstrTickerSource = m_comment[m_nCommentIndex].c_str();

		m_nTickerIndex = 0;
		m_nCommentIndex++;

		SetTimer(CAPTION_CLEAR_ID, CAPTION_CLEAR_TIME, NULL);
		break;

	case CAPTION_CLEAR_ID:
		m_wstrTickerShow = "";
		Invalidate(FALSE);
		SetTimer(CAPTION_SHIFT_ID, m_nMS, NULL);
		break;

	case CAPTION_SHIFT_ID:
		m_nTickerIndex++;
		if(m_nTickerIndex > m_wstrTickerSource.GetLength())
		{
			KillTimer(CAPTION_SHIFT_ID);
			SetTimer(CAPTION_NEXT_ID, CAPTION_NEXT_TIME, NULL);
		}
		m_wstrTickerShow = m_wstrTickerSource.Left(m_nTickerIndex);
		Invalidate(FALSE);
		break;
	}
}

void CTypingTickerSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.
	CRect rtClient;
	GetClientRect(rtClient);

	CMemDC memDC(&dc);

	if(!m_bFileNotExist && m_bgImage.IsValid())
	{
		if(m_rgbFgColor == RGB(255, 255, 255))
		{
			memDC.FillSolidRect(rtClient, RGB(0, 0, 0));
		}
		else
		{
			memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		}//if
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}//if

	else if(m_bgImage.IsValid())
	{	
#if 1	
		// 바둑판
		for(int x=0; x<rtClient.Width(); x+=m_bgImage.GetWidth())
			for(int y=0; y<rtClient.Height(); y+=m_bgImage.GetHeight())
				m_bgImage.Draw2(memDC.GetSafeHdc(), x, y);
#else
		// 늘이기
		m_bgImage.Draw2(memDC.GetSafeHdc(), 0, 0, rtClient.Width(), rtClient.Height());
#endif
	}//if

	if(m_wstrTickerShow.GetLength() != 0)
	{
		memDC.SelectObject(&m_fontTicker);
		memDC.SetBkMode(1); // TRANSPARENT
		memDC.SetTextColor(m_rgbFgColor);
		memDC.DrawText(m_wstrTickerShow, m_rectTicker, DT_MODIFYSTRING);
	}//if
}

bool CTypingTickerSchedule::CreateFile()
{
	double xscale, yscale;
	m_pParentParentWnd->GetScale(xscale, yscale);

	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if

	if(!IsAvailableFont(m_font.c_str()))
	{
		CString strError;
		strError.Format("TypingTicker schedule create font[%s] fail !!!", m_font.c_str());
		__WARNING__(strError, _NULL);
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, "System");
	}
	else
	{
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, m_font.c_str());
	}//if

	m_strLogFileString = "";
	for(int i=0; i<TICKER_COUNT; i++)
	{
		m_strLogFileString += m_comment[i].c_str();
	}//for

	if(m_strLogFileString.GetLength() > 255)
	{
		m_strLogFileString = m_strLogFileString.Left(255);
	}//if

	return true;
}


void CTypingTickerSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	m_nCommentCount = 0;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		if(m_comment[i].length() != 0)
		{
			m_nCommentCount = i;
		}//if
	}//for

	m_bgColor = MNG_PROFILE_READ(lpszID, "bgColor");
	m_fgColor = MNG_PROFILE_READ(lpszID, "fgColor");
	m_font = MNG_PROFILE_READ(lpszID, "font");
	m_fontSize = atoi(MNG_PROFILE_READ(lpszID, "fontSize"));
	m_playSpeed = atoi(MNG_PROFILE_READ(lpszID, "playSpeed"));

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}

	//CreateFile();

	__DEBUG_SCHEDULE_END__(lpszID)
}


double CTypingTickerSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CTypingTickerSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CTypingTickerSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		__DEBUG__("\t\t\tAlready Open", _NULL);
		return true;
	}

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			SetNoContentsFile();
		}//if
	}

	CreateFile();
	
	m_bOpen = true;

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CTypingTickerSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();
	
		m_bOpen = false;
		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if

		if(m_fontTicker.GetSafeHandle())
		{
			m_fontTicker.DeleteObject();
		}//if
	}
	return true;
}

bool CTypingTickerSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CSchedule::Play();

	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	double xscale, yscale;
	m_pParentParentWnd->GetScale(xscale, yscale);

	m_dwStartTick = ::GetTickCount();

	m_nMS = m_playSpeed / xscale; // 스피드 보정
	//m_nMS = m_playSpeed; // 스피드 보정
	if(m_nMS < TIMER_MINIMUM_TIME) m_nMS = TIMER_MINIMUM_TIME;

	SetTimer(CAPTION_NEXT_ID, CAPTION_NEXT_TIME, NULL);

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CTypingTickerSchedule::Pause()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CTypingTickerSchedule::Stop()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	KillTimer(CAPTION_SHIFT_ID);
	KillTimer(CAPTION_NEXT_ID);

	if(m_bFileNotExist)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "Content file does not exist", m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


CString	CTypingTickerSchedule::ToString()
{
	CString str;
	str.Format(
		"-Horzontal Ticker Schedule-\r\n"
		"%s\r\n"
		"%s\r\n"
		"bgColor = %s\r\n"
		"fgColor = %s\r\n"
		"font = %s\r\n"
		"fontSize = %d\r\n"
		"playSpeed = %d\r\n"
		"\r\n"
		,	CSchedule::ToString()
		,	to_string(m_comment, TICKER_COUNT)
		,	m_bgColor.c_str()		// 자막용
		,	m_fgColor.c_str()		// 자막용
		,	m_font.c_str()			// 자막용
		,	m_fontSize		// 자막용
		,	m_playSpeed	// 자막용
	);
	return str;
}

void CTypingTickerSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("TYPING TICKER Schedule Attributes");

	to_string(m_comment, TICKER_COUNT, pGrid, hs);

	pGrid->AddStringItem(hs, "bgColor", m_bgColor.c_str());
	pGrid->AddStringItem(hs, "fgColor", m_fgColor.c_str());
	pGrid->AddStringItem(hs, "font", m_font.c_str());
	pGrid->AddIntegerItem(hs, "fontSize", m_fontSize);
	pGrid->AddIntegerItem(hs, "playSpeed", m_playSpeed);

	hs = pGrid->AddSection("member variables");
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CURLSchedule


CURLSchedule::CURLSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_strLogFileString("")
,	m_pdlgIE(NULL)
,	m_nErrorCode(0)
,	m_bNavigateError(false)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CURLSchedule::~CURLSchedule()
{
	__DEBUG__("\t\t\tDestroy URL Schedule", m_scheduleId.c_str());

	//CloseFile();
}

IMPLEMENT_DYNAMIC(CURLSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CURLSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
END_MESSAGE_MAP()


int CURLSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	m_strMediaFullPath = GetFilePath();
	if(m_strMediaFullPath.GetLength() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}//if

	return 0;
}

void CURLSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}

CString CURLSchedule::GetFilePath()
{
	//예전에는 m_filename이 URL을 나타냈으나
	//배경 이미지를 지원하기 위하여
	//m_filename는 배경 이미지를, m_comment1이 URL을 나타내도록 한다.
	CString strFileName = m_filename.c_str();
	if(strFileName.Find("http", 0) != -1)
	{
		//파일 이름에 "http"가 있다면 URL을 나타낸다.
		m_comment[0] = m_filename.c_str();
		m_filename = "";
	}//if

	return m_filename.c_str();
}

void CURLSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	CRect rtClient;
	GetClientRect(rtClient);

	if(m_bNavigateError && m_bgImage.IsValid())
	{
		CMemDC memDC(&dc);
		m_bgImage.Draw2(memDC.GetSafeHdc(), 0, 0, rtClient.Width(), rtClient.Height());
	}//if
}


bool CURLSchedule::CreateFile()
{
	if(m_comment[0].c_str() == "")
	{
		m_strLogFileString = m_filename.c_str();
	}
	else
	{
		m_strLogFileString = m_comment[0].c_str();
		if(m_strLogFileString.GetLength() > 255)
		{
			m_strLogFileString = m_strLogFileString.Left(255);
		}//if
	}//if

	return true;
}


void CURLSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	__DEBUG_SCHEDULE_END__(lpszID)
}


double CURLSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CURLSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}


bool CURLSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}//if

	CreateFile();

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			__DEBUG__("BG image open fail", m_strMediaFullPath);
			WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "BG image open fail", m_pParentParentWnd->GetTemplateIndex());

			m_bOpen = false;
			return false;
		}//if

		CRect rtClient;
		GetClientRect(&rtClient);
		int nWidth = rtClient.Width();
		int nHeight = rtClient.Height();
		DWORD dwWidth = m_bgImage.GetWidth();
		DWORD dwHeight = m_bgImage.GetHeight();
		if(dwWidth != nWidth || dwHeight != nHeight)
		{
			if(!m_bgImage.Resample(nWidth, nHeight, 3))	//default option(bilinear interpolation)
			{
				__DEBUG__("image resample fail", m_strMediaFullPath);
			}//if
		}//if
	}//if

	m_bOpen = true;

	return true;
}

bool CURLSchedule::CloseFile()
{
	if(m_bOpen)
	{
		m_bOpen = false;
		Stop();

		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if
	}//if

	return true;
}

bool CURLSchedule::Play()
{
	CSchedule::Play();

	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	if(m_pdlgIE)
	{
		m_pdlgIE->DestroyWindow();
		delete m_pdlgIE;
		m_pdlgIE = NULL;
	}//if

	m_pdlgIE = new CExplorerDlg(this);
	BOOL ret = m_pdlgIE->Create(IDD_WEB_DLG, this);
	if(ret == FALSE)
	{
		//WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "Web control create fail");
		//CSchedule::Stop();
		__ERROR__("Web control create fail !!!", m_scheduleId.c_str());
		m_bOpen = false;

		return false;
	}//if	
	m_pdlgIE->ShowWindow(SW_HIDE);
	CRect rtRect;
	GetClientRect(&rtRect);
	m_pdlgIE->MoveWindow(0, 0, rtRect.Width(), rtRect.Height());
	m_pdlgIE->Navigate(m_comment[0].c_str());

	m_dwStartTick = ::GetTickCount();

	return true;
}

bool CURLSchedule::Pause()
{
	Stop();

	return true;
}

bool CURLSchedule::Stop()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_pdlgIE)
	{
		m_pdlgIE->DestroyWindow();
		delete m_pdlgIE;
		m_pdlgIE = NULL;
	}//if

	if(m_bNavigateError)
	{
		CString strDesc;
		if(m_filename.length() == 0)
		{
			strDesc.Format("URL[%s] navigation error[Code=%d]", m_comment[0].c_str(), m_nErrorCode);
		}
		else
		{
			strDesc.Format("URL[%s] navigation error[Code=%d]. Image file[%s] played", m_comment[0].c_str(), m_nErrorCode, m_filename.c_str());
		}//if
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", strDesc, m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_strLogFileString, "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


CString	CURLSchedule::ToString()
{
	CString str;
	str.Format(
		"-URL Schedule-\r\n%s",
		"%s\r\n"
		"URL = %s\r\n"
		"bgImage = %s\r\n"
		, CSchedule::ToString()
		, m_comment[0].c_str()
		, m_filename.c_str());
	return str;
}

void CURLSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("URL Schedule Attributes");

	pGrid->AddStringItem(hs, "URL", m_comment[0].c_str());
	pGrid->AddStringItem(hs, "bgImage", m_filename.c_str());

	hs = pGrid->AddSection("member variables");
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Navigate error 결과값을 설정한다. \n
/// @param (bool) bError : (in) Navigate error 결과값
/// @param (int) nErrorCode : (in) Error code
/////////////////////////////////////////////////////////////////////////////////
void CURLSchedule::SetNavigateError(bool bError, int nErrorCode)
{
	m_bNavigateError = bError;
	m_nErrorCode = nErrorCode;

	if(m_pdlgIE && m_bNavigateError)
	{
		m_pdlgIE->DestroyWindow();
		delete m_pdlgIE;
		m_pdlgIE = NULL;
	}//if

	Invalidate(FALSE);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CFlashSchedule
CMapStringToPtr CFlashSchedule::m_mapContentsReuse;

CFlashSchedule::CFlashSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_bPlaying(false)
,	m_pdlgFlash(NULL)
,   m_couldThisReceiveCallFunc(false)

{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CFlashSchedule::~CFlashSchedule()
{
	__DEBUG__("\t\t\tDestroy Flash Schedule", m_scheduleId.c_str());

	//CloseFile();
}

IMPLEMENT_DYNAMIC(CFlashSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CFlashSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
END_MESSAGE_MAP()

bool			
CFlashSchedule::CallFunc(CString arg) 
{ 
	__DEBUG__("FLASH_INVOKE CallFunc 1", arg);
	if(m_bPlaying && m_pdlgFlash && m_couldThisReceiveCallFunc) { 
	__DEBUG__("FLASH_INVOKE CallFunc 2", arg);
		m_pdlgFlash->CallFunc(arg); 
	__DEBUG__("FLASH_INVOKE CallFunc 3", arg);
		return true;
	} 
	__DEBUG__("FLASH_INVOKE CallFunc 4", arg);
	return false; 
}


void CFlashSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		CRect rtClient;
		GetClientRect(rtClient);

		CMemDC memDC(&dc);
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}//if
}


int CFlashSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		m_strMediaFullPath = GetFilePath();
	}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return 0;
}

void CFlashSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}

CString CFlashSchedule::GetFilePath()
{
	CString LocalFullPath, LocalTempFullPath;
	GetLocalPath(LocalFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	//가져온 로컬 경로가 절대 경로가 아닌경우에는 절대 경로로 만들어 주어야
	//플래쉬 컨텐츠가 재생된다
	if(LocalFullPath.GetAt(1) != ':')
	{
		//절대 경로가 아니다....!!!
		TCHAR szModule[MAX_PATH];
		::ZeroMemory(szModule, MAX_PATH);
		//::GetModuleFileName(NULL, szModule, MAX_PATH);*/
		GetCurrentDirectory(MAX_PATH, szModule);

		TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
		_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

		LocalFullPath = szDrive;
		LocalFullPath.Append("\\Sqisoft");
		LocalFullPath.Append(m_location.c_str());
		LocalFullPath.Append(m_filename.c_str());
	}//if

	return LocalFullPath;
}


bool CFlashSchedule::CreateFile()
{
	CFileStatus status;
	if(CFile::GetStatus(m_strMediaFullPath, status ) == FALSE)
	{
		SetNoContentsFile();
	}//if
	
	return true;
}


void CFlashSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	__DEBUG_SCHEDULE_END__(lpszID)
}


double CFlashSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CFlashSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CFlashSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		__DEBUG__("\t\t\tAlready Open", _NULL);
		return true;
	}

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(CreateFile())
	{
		if(m_pdlgFlash)
		{
			//m_pdlgFlash->Stop();
			//m_pdlgFlash->DestroyWindow();
			//delete m_pdlgFlash;
			CFlashDlg::ClearInstance(m_pdlgFlash);
			m_pdlgFlash = NULL;
		}//if

		if( m_pdlgFlash == NULL )
		{
			//m_pdlgFlash = new CFlashDlg(this);
			//m_pdlgFlash->Create(IDD_FLASH_DLG, this);
			m_pdlgFlash = CFlashDlg::GetInstance(this, m_strMediaFullPath);
			//m_pdlgFlash->ShowWindow(SW_HIDE);

			CRect rtRect;
			GetClientRect(&rtRect);
			m_pdlgFlash->MoveWindow(0, 0, rtRect.Width(), rtRect.Height());
		}

		m_bOpen = true;
		return true;
	}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return false;
}

bool CFlashSchedule::CloseFile()
{
	if(m_bPlaying)
	{
		m_bOpen = false;
		Stop();
	}//if

	if(m_pdlgFlash)
	{
		//m_pdlgFlash->DestroyWindow();
		//delete m_pdlgFlash;
		CFlashDlg::ClearInstance(m_pdlgFlash);
		m_pdlgFlash = NULL;
	}//if

	return true;
}

bool CFlashSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	// skpark 2013.3.21 FLASH_INVOKE 가 준비되어 있지 않다고 표시.
	m_couldThisReceiveCallFunc=false;

	CSchedule::Play();		// 재실행방지 -> 동일多오픈시 플래시 버그 방지 때문
	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());


	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	if(m_bFileNotExist)
	{
		return true;
	}//if
/*
	if(m_pdlgFlash)
	{
		m_pdlgFlash->Stop();
		m_pdlgFlash->DestroyWindow();
		delete m_pdlgFlash;
		m_pdlgFlash = NULL;
	}//if

	m_pdlgFlash = new CFlashDlg(this);
	m_pdlgFlash->Create(IDD_FLASH_DLG, this);
	m_pdlgFlash->ShowWindow(SW_HIDE);
	CRect rtRect;
	GetClientRect(&rtRect);
	m_pdlgFlash->MoveWindow(0, 0, rtRect.Width(), rtRect.Height());
*/
	if(!m_pdlgFlash->Play(m_strMediaFullPath, (CWnd*)this))
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "Fail to play flash movie", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	m_bPlaying = true;
	m_dwStartTick = ::GetTickCount();

	//메인윈도우에 focus를 넘겨야 한다.
	CWnd* pMain = ::AfxGetMainWnd();
	if(pMain)
	{
		pMain->SetFocus();
	}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CFlashSchedule::Pause()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CFlashSchedule::Stop()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

		// skpark 2013.3.21 FLASH_INVOKE 가 준비되어 있지 않다고 표시.
	m_couldThisReceiveCallFunc=false;

/*
	if(m_pdlgFlash)
	{
		m_pdlgFlash->Stop();
		m_pdlgFlash->DestroyWindow();
		delete m_pdlgFlash;
		m_pdlgFlash = NULL;
	}//if
*/
	if(m_pdlgFlash)
	{
		m_pdlgFlash->Stop((CWnd*)this);
	}//if
	m_bPlaying = false;

	//메인윈도우에 focus를 넘겨야 한다.
	CWnd* pMain = ::AfxGetMainWnd();
	if(pMain)
	{
		pMain->SetFocus();
	}//if

	if(m_bFileNotExist)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "Content file does not exist", m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 스케줄을 처음부터 다시 재생한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CFlashSchedule::RePlay()
{
	//__DEBUG__("\t\t\tReplay Schedule", m_strMediaFullPath);

	Stop();
	Play();
}


CString	CFlashSchedule::ToString()
{
	CString str;
	str.Format("-Flash-\r\n%s", CSchedule::ToString());
	return str;
}

void CFlashSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("FLASH Schedule Attributes");

	hs = pGrid->AddSection("member variables");
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 플래쉬 컨텐츠의 종류 뮨자열 반한 \n
/// @return <형: LPCSTR> \n
///			플래쉬 컨텐츠의 종류에 따른 문자열을 반환 한다. \n
/////////////////////////////////////////////////////////////////////////////////
LPCSTR CFlashSchedule::GetContentsTypeString()
{ 
	switch(m_contentsType)
	{
	case CONTENTS_FLASH:			return "Flash";
	case CONTENTS_FLASH_TEMPLATE:	return "Flash template";
	case CONTENTS_DYNAMIC_FLASH:	return "Dynamic flash";
	default:						return "Unknown type";
	}//switch
};

/*
LRESULT CFlashSchedule::OnRecvFsCommand(WPARAM wParam, LPARAM lParam)
{
	CString strCmd, strArg;
	strCmd = (LPCSTR)wParam;
	strArg = (LPCSTR)lParam;

	__DEBUG__("strCmd", strCmd);
	__DEBUG__("strArg", strArg);

	if(strCmd.CompareNoCase("interact_log") == 0)
	{
		//기아. 현대의 플래쉬 컨텐츠 상호작용 통계를 위한...
		//","를 구분자로 3단계 로그(통계)의 단계를 구분함(예 : "차량소개,모닝,내장VR")
		//3단계 고정이므로 항상 3개의 "|" 가 있어야 한다.
		//만약, 3개의 "|"가 변경되지 않았다면 뒤에 부족한개수 만큼의 "|"를 붙여주어야하며,
		//키워드에 ","가 있다면 "_"로 먼저 변경해준다.(DB에서 오류가 나기때문에...???)
		//(예: "차량소개" ==> "차량소개|||")
		strArg.Replace(',', '|');

		//int nCount = strArg.Replace('|', '|');		//'|' 문자개수를 파악
		int nCount = 0;
		char cChar;
		for(int i=0; i<strArg.GetLength(); i++)
		{
			cChar = strArg.GetAt(i);
			if(cChar == '|')
			{
				nCount++;
			}//if
		}//for

		if(nCount > 3)
		{
			__DEBUG__("Invalid interact argument", strArg);
			return 0;
		}//if

		for(;nCount < 3; nCount++)
		{
			strArg.Append("|");
		}//for

		strArg.Replace('|', ',');
		WriteInterActLog(m_contentsId.c_str(), strArg);
	}//if

	return 0;
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FsCommand를 처리한다. \n
/// @param (CString) strCmd : (in) command
/// @param (CString) strArg : (in) args
/////////////////////////////////////////////////////////////////////////////////
void CFlashSchedule::RecvFsCommand(CString strCmd, CString strArg)
{
	__DEBUG__("Flash RecvFsCommand", _NULL);

	__DEBUG__("strCmd", strCmd);
	__DEBUG__("strArg", strArg);

	if(strCmd.CompareNoCase("interact_log") == 0)
	{
		//기아. 현대의 플래쉬 컨텐츠 상호작용 통계를 위한...
		//","를 구분자로 3단계 로그(통계)의 단계를 구분함(예 : "차량소개,모닝,내장VR")
		//3단계 고정이므로 항상 3개의 "|" 가 있어야 한다.
		//만약, 3개의 "|"가 존재하지 않는다면 뒤에 부족한개수 만큼의 "|"를 붙여주어야하며,
		//키워드에 ","가 있다면 "_"로 먼저 변경해준다.(DB에서 오류가 나기때문에...???)
		//(예: "차량소개" ==> "차량소개|||")
		//2012-04-19 5단계로 변경됨
/*
		strArg.Replace(',', '|');
		int nCount = 0;
		char cChar;
		for(int i=0; i<strArg.GetLength(); i++)
		{
			cChar = strArg.GetAt(i);
			if(cChar == '|')
			{
				nCount++;
			}//if
		}//for

		if(nCount > MAX_INTERACT_LOG)
		{
			__DEBUG__("Invalid interact argument", strArg);
			return;
		}//if

		for(;nCount < MAX_INTERACT_LOG; nCount++)
		{
			strArg.Append("|");
		}//for

		strArg.Replace('|', ',');
*/
		int nCount = 0;
		char cChar;
		for(int i=0; i<strArg.GetLength(); i++)
		{
			cChar = strArg.GetAt(i);
			if(cChar == ',')
			{
				nCount++;
			}//if
		}//for

		for(;nCount < MAX_INTERACT_LOG; nCount++)
		{
			strArg.Append(",");
		}//for


		WriteInterActLog(m_contentsId.c_str(), strArg);
	}
	else if(strCmd.CompareNoCase("KH_url") == 0)	//기아 KH매장 고객쉼터 기능
	{
		CHost* pHost = (CHost*)::AfxGetMainWnd();
		pHost->RunIENavigate(strArg);
	}//if
	else if(strCmd.CompareNoCase("FLASH_INVOKE+READY") == 0)	//skpark 2013.3.21 FLASH_INVOKE 호출에 대응할 수 있다는 뜻.
	{
		m_couldThisReceiveCallFunc= true;
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// COfficeApp
COfficeApp* COfficeApp::_instance = NULL;
CCriticalSection COfficeApp::_lock;
int COfficeApp::_refcnt = 0;
int COfficeApp::_opencnt = 0;
IConnectionPoint*	COfficeApp::_pptEventConnectionPoint = NULL;
CPPTEventSinkEx*	COfficeApp::_pptEventSink = NULL;
CMapStringToString COfficeApp::_mapOpenPPT;

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ingle tone 인스턴스 생성 함수 \n
/// @return <형: COfficeApp*> \n
///			<COfficeApp*: Single tone 인스턴스> \n
///			<NULL: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
COfficeApp* COfficeApp::getInstance()
{
	_lock.Lock();

	scratchUtil* aUtil = scratchUtil::getInstance();
	ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
	if( ulPid == 0 )
	{
		// 실행프로세스 없으면 -> 삭제하고 초기화
		DisconnectPPTEventSink();
		delete _instance;
		_instance = NULL;
		_refcnt = 0;
		_opencnt = 0;
		_mapOpenPPT.RemoveAll();
	}

	_lock.Unlock();

	return _instance;
}

COfficeApp* COfficeApp::getInstance(LPCSTR lpszFullpath, CPPTInvokerInterface* pParent)
{
	_lock.Lock();
	while( _instance != NULL )
	{
		scratchUtil* aUtil = scratchUtil::getInstance();
		ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
		if( ulPid == 0 )
		{
			// 실행프로세스 없으면 -> 삭제하고 초기화
			DisconnectPPTEventSink();
			delete _instance;
			_instance = NULL;
			_refcnt = 0;
			_opencnt = 0;
			_mapOpenPPT.RemoveAll();
		}
		else// if( lpszFilename != NULL && pParent != NULL )
		{
			// 이미 있음 & 새로생성 -> 카운트++
			_refcnt++;
			if( _pptEventSink )
				_pptEventSink->AddParentSchedule(lpszFullpath, pParent);

			_instance->Open(lpszFullpath);
		}

		_lock.Unlock();
		return _instance;
	}

	_instance = new COfficeApp();
/*
	////처음 인스턴스 생성시에 이전에 실행중인 파워포인트를 강제 종료시킨다.
	//__DEBUG__("Terminate Powerpoint", _NULL);
	//scratchUtil* aUtil = scratchUtil::getInstance();
	//ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
	//__DEBUG__("scratchUtil : getPid", (ULONGLONG)ulPid);
	//if(ulPid > 0)
	//{
	//	int nRet = aUtil->killProcess(ulPid);
	//	__DEBUG__("scratchUtil : killProcess", nRet);
	//}//if
	//Sleep(500);

	//이미 실행중인 PPT가 있는지 확인
	CLSID clsid;
	CLSIDFromProgID(L"Powerpoint.Application", &clsid);
	IUnknown *pUnk = NULL;
	HRESULT hr = GetActiveObject(clsid, NULL, (IUnknown**)&pUnk);
	if( SUCCEEDED(hr) )
	{
		// 이미 존재 -> 가져옴
		IDispatch *pDisp = NULL;
		hr = pUnk->QueryInterface(IID_IDispatch, (void **)&pDisp);
		_instance->AttachDispatch(pDisp);
	}
	else*/
	{
		// 새로 생성
		if( !_instance->CreateDispatch("Powerpoint.Application") )
		{
			__DEBUG__("Powerpoint CreateDispatch fail", _NULL);
			delete _instance;
			_instance = NULL;
		}
	}

	//if( NULL != pUnk ) pUnk->Release();

	if( _instance == NULL )
	{
		_lock.Unlock();
		return _instance;
	}

	//Make the application visible but not minimized
	_instance->SetVisible((long)TRUE);

	// ppWindowNormal = 1, ppWindowMinimized = 2, ppWindowMaximized = 3
	_instance->SetWindowState((long)2);	// Start with PPT visible - makes 
												//  presentation visible, too.

	ConnectPPTEventSink(_instance);

	_refcnt = 1;
	if( _pptEventSink )
		_pptEventSink->AddParentSchedule(lpszFullpath, pParent);

	_instance->Open(lpszFullpath);

	_lock.Unlock();

	return _instance;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Single tone 인스턴스 소멸 함수 \n
/////////////////////////////////////////////////////////////////////////////////
void COfficeApp::clearInstance()
{
	_lock.Lock();
	_refcnt--;

	if( _instance==NULL || _refcnt > 0 )
	{
		// nothing or remain ref-count
		_lock.Unlock();
		return;
	}

	// ignore event
	if( _pptEventSink )
		_pptEventSink->SetIgnoreEvent(true);

	// quit & kill ppt-process
	scratchUtil* aUtil = scratchUtil::getInstance();
	ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
	if( ulPid > 0 )
	{
		DisconnectPPTEventSink();
		__DEBUG__("Powerpoint Quit", _NULL);
		_instance->Quit();
		_instance->ReleaseDispatch();
	}
	delete _instance;
	_instance = NULL;
	_refcnt = 0;
	_opencnt = 0;
	_mapOpenPPT.RemoveAll();

	__DEBUG__("Terminate Powerpoint", _NULL);
	// 파워 포인트가 확실하게 죽었는지 5초동안 확인한다
	bool powerpnt_alive = true;
	for(int i=0; i<5 && powerpnt_alive; i++)
	{
		::Sleep(1000);

		ulPid = aUtil->getPid("POWERPNT.EXE");
		if( ulPid == 0 )
		{
			powerpnt_alive = false;						
			break;
		}

		__DEBUG__("POWERPNT still alive", (unsigned int)ulPid);
		int nRet = aUtil->killProcess(ulPid);
		__DEBUG__("scratchUtil : kill powerpoint", nRet);
	}

	//실행중인 파워포인트를 강제 종료시킨다.
	if( powerpnt_alive )
	{
		ulPid = aUtil->getPid("POWERPNT.EXE");
		__DEBUG__("scratchUtil : powerpoint PID", (unsigned int)ulPid);
		if(ulPid > 0)
		{
			int nRet = aUtil->killProcess(ulPid);
			__DEBUG__("scratchUtil : kill powerpoint", nRet);
		}//if
	}

	_lock.Unlock();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 임시파일을 복사할 디렉토리 \n
/////////////////////////////////////////////////////////////////////////////////
LPCSTR COfficeApp::getTempFolderName()
{
	static CString str_tmp_foler_name = "";

	if( str_tmp_foler_name.GetLength() == 0 )
	{
		char* tmp_dir = getenv("Temp");
		if( tmp_dir == NULL )
			tmp_dir = getenv("Tmp");

		if( tmp_dir != NULL )
		{
			// get default-temp-dir
			CString str_tmp;
			str_tmp.Format("%s\\UBC_PPT\\", tmp_dir);

			// remove temp-dir
			CString str_arg;
			// cmd.exe /c rmdir /s /q "%temp%\\UBC_PPT\\"
			//str_arg.Format("cmd.exe /c rmdir /s /q \"%s\"", str_tmp);
			str_arg.Format("rmdir /s /q \"%s\"", str_tmp);

			__DEBUG__("Delete Powerpoint Temp Directory", str_arg);
			//scratchUtil::getInstance()->createProcess(str_arg, "");
			system(str_arg);

			// create temp-dir
			__DEBUG__("Create Powerpoint Temp Directory", str_tmp);
			::CreateDirectory(str_tmp, NULL);

			// get temp-dir
			int pid = getpid();
			str_tmp_foler_name.Format("%s%d\\", str_tmp, pid);
			__DEBUG__("Create Powerpoint My Temp Directory", str_tmp_foler_name);
			::CreateDirectory(str_tmp_foler_name, NULL);
		}
	}

	return str_tmp_foler_name;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
COfficeApp::COfficeApp()
{
	m_fPosX = 0.f;
	m_fPosY = 0.f;
	m_fWidth = 0.f;
	m_fHeight = 0.f;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
COfficeApp::~COfficeApp()
{
}

void COfficeApp::SetOpenComplete(CString strFilename, bool bOpen)
{
	strFilename.MakeLower();

	if( bOpen )
	{
		_mapOpenPPT.SetAt(strFilename, strFilename);
	}
	else
	{
		//
		// remove from map(_mapOpenPPT)
		//
	}
}

bool COfficeApp::ConnectPPTEventSink(COfficeApp* instance)
{
	//if(m_pptApp == NULL)
	//{
	//	return false;
	//}//if

	DisconnectPPTEventSink();

	///*********************** Start of code to get connection point **************
  	//  Declare the events we want to catch.
  	//
  	//  Look for the coclass for Application in the typelib, msppt9.olb
  	//  then look for the word "source." The interface - EApplication -
  	//  is the next search target. When you find it you'll see the 
  	//  following guid a the events you'll be able to sink.
  	//  914934C2-5A91-11CF-8700-00AA0060263B
  	//	static const GUID IID_IEApplication =
  	//	{0x914934C2,0x5A91,0x11CF, {0x87,0x00,0x00,0xAA,0x00,0x60,0x26,0x3b}};
  
  	//  Steps for setting up events.
  	// 1. Get server's IConnectionPointContainer interface.
  	// 2. Call IConnectionPointContainerFindConnectionPoint()
  	//    to find the event we want to catch.
  	// 3. Call IConnectionPoint::Advise() with the IUnknown
  	//    interface of our implementation of the events.

	// Get IDispatch interface for Automation...
	IDispatch *pDisp = NULL;
	pDisp = instance->m_lpDispatch;

  	// Get server's (PPT) IConnectionPointContainer interface.
  	IConnectionPointContainer *pConnPtContainer;
  	HRESULT hr = pDisp->QueryInterface(
  							IID_IConnectionPointContainer,
  							(void **)&pConnPtContainer);

  	if( !SUCCEEDED(hr) )
  	{
		CString strError;
		strError.Format(_T("Couldn't get IConnectionPointContainer interface."));
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
 	}//if

  	// Find connection point for events we're interested in.
  	hr = pConnPtContainer->FindConnectionPoint(
  											IID_IEApplication,
  											&_pptEventConnectionPoint
  											);

  	if( !SUCCEEDED(hr) )
  	{
		CString strError;
		strError.Format(_T("Couldn't find connection point via event GUID."));
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
  	}//if

	_pptEventSink = new CPPTEventSinkEx();

	IUnknown *pUnk = NULL;
  	_pptEventSink->QueryInterface(IID_IUnknown, (void**)&pUnk);
  	if( pUnk == NULL )
	{
		CString strError;
		strError.Format(_T("PPTEventSink fail to QueryInterface."));
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
	}//if

  	// Setup advisory connection!
  	hr = _pptEventConnectionPoint->Advise(pUnk, &_pptEventSink->m_Cookie);
  	if( !SUCCEEDED(hr) )
	{
		CString strError;
		strError.Format(_T("PPTEventSink fail to Advise."));
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
	}//if

  	// Release IConnectionPointContainer interface.
  	pConnPtContainer->Release();

	return true; 
}

void COfficeApp::DisconnectPPTEventSink()
{
	if( _pptEventConnectionPoint )
	{
		_pptEventConnectionPoint->Unadvise(_pptEventSink->m_Cookie); // PPT releases cookie
 		_pptEventConnectionPoint->Release();
		_pptEventConnectionPoint = NULL;
	}

	if( _pptEventSink )
	{
		_pptEventSink->Release();
		_pptEventSink = NULL;
	}
}

BOOL COfficeApp::Open(LPCSTR lpszFullpath)
{
	__DEBUG_BEGIN__(lpszFullpath);

	IDispatch *pDisp = NULL;
	Presentations	pptPresentations;
	_Presentation	pptPresentation;

	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
		__DEBUG__("GetPresentaions fail !!!", lpszFullpath);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);

	//열려있는 presentation중에 같은 파일이 있는지 검사하여
	//이미 열려있다면 닫아주어야 한다.
	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if( strFullName.CompareNoCase(lpszFullpath) == 0 )
		{
			//pptPresentation.Close();
			//pptPresentation.ReleaseDispatch();
			//Sleep(500);
			//break;

			pptPresentation.ReleaseDispatch();
			pptPresentations.ReleaseDispatch();
			return TRUE; // 이미 열려있으면 재활용하는걸로 교체
		}
		pptPresentation.ReleaseDispatch();
	}

	/*
	msoCTrue 1 지원되지 않음 
	msoFalse 0 False 
	msoTriStateMixed -2 지원되지 않음 
	msoTriStateToggle -3 지원되지 않음 
	msoTrue -1 True 
	*/
	__DEBUG__("Presentation open", lpszFullpath);
	pDisp = pptPresentations.Open(lpszFullpath,      
		(long)0,		//Read-only
		(long)0,		//Untitled
		(long)-1		//WithWindow
		);
	__DEBUG__("Presentation opened", lpszFullpath);

	pptPresentations.ReleaseDispatch();

	return TRUE;
}

BOOL COfficeApp::Play(LPCSTR lpszFullpath)
{
	__DEBUG__("COfficeApp::Play(Start)", lpszFullpath);

	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowSettings	pptSlideshow;			///<MS-PowerPoint SlideShow settings

	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
		__DEBUG__("GetPresentaions fail !!!", lpszFullpath);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(lpszFullpath) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
		__ERROR__("Fail to get presentation", lpszFullpath); 
		//Stop();
		pptPresentations.ReleaseDispatch();
		return FALSE;
	}//if

/*
	pDisp = pptPresentation.GetSlideShowSettings();

	pptSlideshow.AttachDispatch(pDisp);
	pptSlideshow.SetLoopUntilStopped(-1);		//msoTrue
	pptSlideshow.Run();

	pptSlideshow.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/
	if( _pptEventSink )
		_pptEventSink->SetCurrentPresentationName(lpszFullpath);

	//
/*
	SlideShowSettings	pptSettings;

	pDisp = pptPresentation.GetSlideShowSettings();

	pptSettings.AttachDispatch(pDisp);
	pptSettings.SetRangeType(1);			//ppShowAll
	pptSettings.SetLoopUntilStopped(-1);	//msoTrue
	pptSettings.SetAdvanceMode(2);			//ppSlideShowUseSlideTimings
	pptSettings.SetShowType(1);				//ppShowTypeSpeaker
	pptSettings.Run();

	pptSettings.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/
	pDisp = pptPresentation.GetSlideShowSettings();

	pptSlideshow.AttachDispatch(pDisp);
	pptSlideshow.SetLoopUntilStopped(-1);		//msoTrue
	pptSlideshow.Run();

	pptSlideshow.ReleaseDispatch();


	//
	SlideShowWindow		pptSlideShowWnd;
	SlideShowView		pptSlideShowView;

	pDisp = pptPresentation.GetSlideShowWindow();
	if( pDisp == NULL )
	{
		__ERROR__("Fail to get pDisp", lpszFullpath); 
		return FALSE;
	}

	pptSlideShowWnd.AttachDispatch(pDisp);
	pptSlideShowView.AttachDispatch(pptSlideShowWnd.GetView());
	pptSlideShowView.First(); // 첫장으로 이동

	pptSlideShowView.ReleaseDispatch();
	pptSlideShowWnd.ReleaseDispatch();

	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();

	__DEBUG__("COfficeApp::Play(End)", lpszFullpath);
	return TRUE;
}

BOOL COfficeApp::SlideShowBegin(LPCSTR lpszFullpath)
{
	__DEBUG__("COfficeApp::SlideShowBegin(Start)", lpszFullpath);

	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowWindow		pptSlideShowWnd;

	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
		__DEBUG__("GetPresentaions fail !!!", lpszFullpath);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);
	__DEBUG__("1", lpszFullpath);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(lpszFullpath) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for
	__DEBUG__("2", lpszFullpath);

	if(pptPresentation.m_lpDispatch == NULL)
	{
		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
		__ERROR__("Fail to get presentation", lpszFullpath);
		//Stop();
		pptPresentations.ReleaseDispatch();
		return false;
	}//if
	__DEBUG__("3", lpszFullpath);

	pDisp = pptPresentation.GetSlideShowWindow();
	if( pDisp == NULL ) return FALSE;
	__DEBUG__("4", lpszFullpath);

	pptSlideShowWnd.AttachDispatch(pDisp);
	// 위치, 크기 지정
	__DEBUG__("5-2", m_fWidth);
	pptSlideShowWnd.SetWidth(m_fWidth);
	__DEBUG__("5-1", m_fHeight);
	pptSlideShowWnd.SetHeight(m_fHeight);
	__DEBUG__("5-3", m_fPosY);
	pptSlideShowWnd.SetTop(m_fPosY);
	__DEBUG__("5-4", m_fPosX);
	pptSlideShowWnd.SetLeft(m_fPosX);
	__DEBUG__("6", lpszFullpath);

	pptSlideShowWnd.ReleaseDispatch();
	__DEBUG__("7", lpszFullpath);

	pptPresentation.ReleaseDispatch();
	__DEBUG__("8", lpszFullpath);
	pptPresentations.ReleaseDispatch();

	__DEBUG__("COfficeApp::SlideShowBegin(End)", lpszFullpath);

	return true;
}

BOOL COfficeApp::ClosePresentation(LPCSTR lpszFullpath)
{
	__DEBUG__("COfficeApp::ClosePresentation(Start)", lpszFullpath);

	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowSettings	pptSlideshow;			///<MS-PowerPoint SlideShow settings

	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
		__DEBUG__("GetPresentaions fail !!!", lpszFullpath);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(lpszFullpath) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
		__ERROR__("Fail to get presentation", lpszFullpath); 
		//Stop();
		pptPresentations.ReleaseDispatch();
		return FALSE;
	}//if

/*
	pDisp = pptPresentation.GetSlideShowSettings();

	pptSlideshow.AttachDispatch(pDisp);
	pptSlideshow.SetLoopUntilStopped(-1);		//msoTrue
	pptSlideshow.Run();

	pptSlideshow.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/
	//
/*
	SlideShowSettings	pptSettings;

	pDisp = pptPresentation.GetSlideShowSettings();

	pptSettings.AttachDispatch(pDisp);
	pptSettings.SetRangeType(1);			//ppShowAll
	pptSettings.SetLoopUntilStopped(-1);	//msoTrue
	pptSettings.SetAdvanceMode(2);			//ppSlideShowUseSlideTimings
	pptSettings.SetShowType(1);				//ppShowTypeSpeaker
	pptSettings.Run();

	pptSettings.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/

	//
	SlideShowWindow		pptSlideShowWnd;
	SlideShowView		pptSlideShowView;

	try
	{
		pDisp = pptPresentation.GetSlideShowWindow();
		if( pDisp == NULL ) return FALSE;
	}
	catch(...)
	{
		return FALSE;
	}

	pptSlideShowWnd.AttachDispatch(pDisp);
	pptSlideShowView.AttachDispatch(pptSlideShowWnd.GetView());
	pptSlideShowView.Exit(); // 슬라이드쇼 종료

	pptSlideShowView.ReleaseDispatch();
	pptSlideShowWnd.ReleaseDispatch();

	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();

	__DEBUG__("COfficeApp::ClosePresentation(End)", lpszFullpath);

	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPowerPointSchedule


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 모드에서의 생성자 \n
/// @param (CTemplate*) pParentParentWnd : (in) schedule을 갖는 template의 포인터
/// @param (CFrame*) pParentWnd : (in) schedule을 갖는 frame의 포인터
/// @param (LPCSTR) lpszID : (in) schedule의 id
/// @param (bool) bDefaultSchedule : (in) default schedule인지 여부 플래그 값
/////////////////////////////////////////////////////////////////////////////////
CPowerPointSchedule::CPowerPointSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_fPosX(0)
,	m_fPosY(0)
,	m_fWidth(0)
,	m_fHeight(0)
,	m_strPresentationName(_T(""))
,	m_pptApp(NULL)
,	m_bTopMost(false)
,	m_nLocale(82)		//KOR
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CPowerPointSchedule::~CPowerPointSchedule()
{
	__DEBUG__("\t\t\tDestroy Power Point Schedule", m_scheduleId.c_str());

	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if

	//CloseFile();
}


IMPLEMENT_DYNAMIC(CPowerPointSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CPowerPointSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_WM_TIMER()
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_CREATE 이벤트 함수로 window가 처음 생성될때 호출된다 \n
/// @param (LPCREATESTRUCT) lpCreateStruct : (in) 윈도우 생성 정보
/// @return <형: int> \n
///			<0: 성공> \n
///			<-1: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
int CPowerPointSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}//if

	// OS 정보를 얻어 국가 코드를 얻는다.
	char szNation[7] = { 0x00 };
	if(GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_ICOUNTRY, szNation, 7) != 0)
	{
		m_nLocale = atoi(szNation);
	}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_DESTORY 이벤트 함수 \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_PAINT 이벤트 함수로 window가 그려질때 호출된다 \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::OnPaint()
{
	//CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	if(m_bOpen)
	{
		CPaintDC dc(this);
		CMemDC memDC(&dc);

		CRect rtClient;
		GetClientRect(rtClient);
		
		if(m_bFileNotExist && m_bgImage.IsValid())
		{
			memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
			m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
		}
		else if(m_bgImage.IsValid())
		{
			memDC.FillSolidRect(rtClient, RGB(0, 0, 0));
			//배경 이미지 위치 조정
			int nImgPosX, nImgPosY;
			int nImgWidth = m_bgImage.GetWidth();
			int nImgHeight = m_bgImage.GetHeight();

			if(nImgWidth >= rtClient.Width())
			{
				nImgWidth = rtClient.Width();
				nImgPosX = 0;
			}
			else
			{
				nImgPosX = (rtClient.Width() - nImgWidth)/2;
			}//if

			if(nImgHeight >= rtClient.Height())
			{
				nImgHeight = rtClient.Height();
				nImgPosY = 0;
			}
			else
			{
				nImgPosY = (rtClient.Height() - nImgHeight)/2;
			}//if
			m_rectBGimage.SetRect(nImgPosX, nImgPosY, nImgPosX+nImgWidth, nImgPosY+nImgHeight);
			m_bgImage.Draw2(memDC.m_hDC, m_rectBGimage);
		}//if
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 모드에서 schedule을 설정하는 함수 \n
/// @param (LPCSTR) lpszID : (in) schedule의 id
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	//ppt 파일의 슬라이드 쇼 시간을 정할수가 없으므로 하루동안의 초 = 86400
	m_runningTime = TIME_PPT_PLAYING;

	__DEBUG_SCHEDULE_END__(lpszID)
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents의 전체 재생시간을 반환하는 함수 \n
/// @return <형: double> \n
///			contents의 전체 재생시간 \n
/////////////////////////////////////////////////////////////////////////////////
double CPowerPointSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
	{
		return (double)m_runningTime*1000; // sec -> millisec
	}
	else
	{
		return (double)DBL_MAX;				//double의 최대값
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contens의 현재 재생경과 시간을 반환하는 함수 \n
/// @return <형: double> \n
///			contents가 재생된 경과 시간 \n
///			<값: 설명> \n
/////////////////////////////////////////////////////////////////////////////////
double CPowerPointSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents 파일을 open하는 함수 \n
/// @param (int) nGrade : (in) contens의 재생우선 순위 grade
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		__DEBUG__("\t\t\tAlready Open", _NULL);
		return true;
	}//if

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CFileStatus fs;
	if(CFile::GetStatus(m_strMediaFullPath, fs) == FALSE)
	{
		SetNoContentsFile();

		return true;
	}//if

	// create duplicate
	CString str_tmp_path = COfficeApp::getTempFolderName();
	if( str_tmp_path.GetLength() > 0 )
	{
		char drv[MAX_PATH]={0},dir[MAX_PATH]={0},fn[MAX_PATH]={0},ext[MAX_PATH]={0};
		_splitpath(m_strMediaFullPath, drv, dir, fn, ext);

		str_tmp_path += fn;
		str_tmp_path += ext;

		if( ::CopyFile(m_strMediaFullPath, str_tmp_path, FALSE) )
			m_strPresentationName = str_tmp_path;
		else
			m_strPresentationName = fs.m_szFullName;
	}
	else
	//

	m_strPresentationName = fs.m_szFullName;
	__DEBUG__("COfficeApp::getInstance", _NULL);
	m_pptApp = COfficeApp::getInstance(m_strPresentationName, this); // ppt파일 열기
	if(m_pptApp == NULL)
	{
		__ERROR__("Get PPT Object fail !!!", _NULL);
		return false;
	}//if

	CRect rectClient;
	GetClientRect(&rectClient);
	ClientToScreen(&rectClient);

	//Office는 Point 단위를 사용하므로 pixel을 point로 변환해야한다.
	// 12px 는 9pt(96DPI 기준)
	// Point = (Pixel)*72/(DPI)
	CDC* pDC = CWnd::GetDC();
	int nDpiX = pDC->GetDeviceCaps(LOGPIXELSX);
	int nDpiY = pDC->GetDeviceCaps(LOGPIXELSY);
	m_pptApp->m_fPosX = m_fPosX = (rectClient.left*72)/nDpiX;
	m_pptApp->m_fPosY = m_fPosY = (rectClient.top*72)/nDpiY;
	m_pptApp->m_fWidth = m_fWidth = (rectClient.Width()*72)/nDpiX;
	m_pptApp->m_fHeight = m_fHeight = (rectClient.Height()*72)/nDpiY;

	SetSlideShowResolution();

	m_bOpen = true;

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents 파일을 close 하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::CloseFile()
{
	if(m_bOpen)
	{
		m_bOpen = false;
		Stop();
	}//if

	__DEBUG__("COfficeApp::getInstance", _NULL);
	m_pptApp = COfficeApp::getInstance();
	if( m_pptApp )
	{
		COfficeApp::clearInstance(/*m_strPresentationName*/);
		m_pptApp = NULL;
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 play하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::Play()
{
	CSchedule::Play();
	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_strMediaFullPath);
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	__DEBUG__("COfficeApp::getInstance", _NULL);
//	if( m_pptApp==NULL ) { m_bOpen=FALSE; OpenFile(0); }
	m_pptApp = COfficeApp::getInstance();

	if(!m_bFileNotExist)
	{
		if(m_pptApp == NULL)
		{
			COfficeApp::clearInstance();

			m_bOpen = false;
			OpenFile(0);
			m_pptApp = COfficeApp::getInstance();
			if(m_pptApp == NULL)
			{
				OnErrorMessage((WPARAM)"Can not found MS-PowerPoint program", 0);  
				__ERROR__("Get PPT Object fail !!!", _NULL);
				//return false;

				HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SLIDE_NO));
				m_bgImage.CreateFromHBITMAP(hBitmap);

				m_runningTime = TIME_PPT_NONE;
			}
		}
		/*
		else
		{
			HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SLIDE_SHOW));
			m_bgImage.CreateFromHBITMAP(hBitmap);
		}//if
		*/
	}//if
/*
	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	pclsHost->SetTopMost(true);
	Invalidate(FALSE);	//배경화면을 다시 그려준다
*/
	m_dwStartTick = ::GetTickCount();

	__DEBUG__("m_strPresentationName : ", m_strPresentationName);

	if( m_pptApp )
	{
		if(m_pptApp->Play(m_strPresentationName)==FALSE)
		{
			__WARNING__("Fail to PPT Play !!!", m_strPresentationName);

			COfficeApp::clearInstance();

			m_bOpen = false;
			OpenFile(0);
			m_pptApp = COfficeApp::getInstance();
			if(m_pptApp == NULL)
			{
				__ERROR__("Get PPT Object fail !!!", _NULL);
				return false;
			}//if

			m_pptApp->Play(m_strPresentationName);
		}
/*		if(!ConnectPPTEventSink())
		{
			__DEBUG__("Fail to ConnectPPTEventSink", _NULL);
			return false;
		}//if

		IDispatch *pDisp = NULL;
		Presentations		pptPresentations;		///<MS-PowerPoint Presentations
		_Presentation		pptPresentation;

		pDisp = m_pptApp->GetPresentations();
		if(pDisp == NULL)
		{
			__DEBUG__("GetPresentaions fail !!!", _NULL);
			return false;
		}//if
		pptPresentations.AttachDispatch(pDisp);

		//열려있는 presentation중에 같은 파일이 있는지 검사하여
		//이미 열려있다면 닫아주어야 한다.
		VARIANT paramVariant;
		paramVariant.vt = VT_I4;
		CString strFullName;
		for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
		{
			paramVariant.lVal = lCnt;
			pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
			strFullName = pptPresentation.GetFullName();
			//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
			//대소문자를 구분하지 않고 비교하도록 한다.
			if(strFullName.CompareNoCase(m_strPresentationName) == 0)
			{
				pptPresentation.Close();
				pptPresentation.ReleaseDispatch();
				Sleep(500);
				break;
			}//if
			pptPresentation.ReleaseDispatch();
		}//for

		/*
		msoCTrue 1 지원되지 않음 
		msoFalse 0 False 
		msoTriStateMixed -2 지원되지 않음 
		msoTriStateToggle -3 지원되지 않음 
		msoTrue -1 True 
		* /
		__DEBUG__("Presentation open", m_strMediaFullPath);
		pDisp = pptPresentations.Open(m_strMediaFullPath,      
			(long)0,		//Read-only
			(long)0,		//Untitled
			(long)-1		//WithWindow
			//(long)-1      //WithWindow
			);
		__DEBUG__("Presentation opened", m_strMediaFullPath);

		pptPresentations.ReleaseDispatch();
*/
	}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 일시 정지 하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::Pause()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 정지하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::Stop()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	SlideShowEnd();
	//m_pptApp = COfficeApp::getInstance();
	//if( m_pptApp )
	//{
	//	m_pptApp->SlideShowEnd(m_strPresentationName);
	//}
//	COfficeApp::clearInstance(/*m_strPresentationName*/);
//	m_pptApp = NULL;

	if(m_bFileNotExist)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "Content file does not exist", m_pParentParentWnd->GetTemplateIndex());
	}
	else if(m_pptApp == NULL)
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_filename.c_str(), "", "Fail to run MS-PowerPoint program", m_pParentParentWnd->GetTemplateIndex());
	}
	else
	{
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());
	}//if

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 스케줄을 처음부터 다시 재생한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::RePlay()
{
	//__DEBUG__("\t\t\tReplay Schedule", m_strMediaFullPath);

	//m_dwStartTick = ::GetTickCount();
/*
	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowWindow		pptSlideShowWnd;
	SlideShowView		pptSlideShowView;
	
	pDisp = m_pptApp->GetPresentations();
	pptPresentations.AttachDispatch(pDisp);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(m_strPresentationName) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
		__ERROR__("Fail to get presentation", m_strPresentationName);
		Stop();
		return;
	}//if

	pDisp = pptPresentation.GetSlideShowWindow();

	pptSlideShowWnd.AttachDispatch(pDisp);
	pptSlideShowView.AttachDispatch(pptSlideShowWnd.GetView());
	pptSlideShowView.First();

	SlideShowSettings pptSettings = pptPresentation.GetSlideShowSettings();
	pptSettings.SetRangeType(1);			//ppShowAll
	pptSettings.SetLoopUntilStopped(-1);	//msoTrue
	pptSettings.SetAdvanceMode(2);			//ppSlideShowUseSlideTimings
	pptSettings.SetShowType(1);				//ppShowTypeSpeaker
	pptSettings.Run();

	pptSlideShowView.ReleaseDispatch();
	pptSlideShowWnd.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파워포인트에 ESC 키가 눌러져서 BRW를 종료한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::EscapeBRW()
{
	//SlideShowEnd();

	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	::PostMessage(pclsHost->m_hWnd, WM_SEND_STOP_BRW, 0, 0);
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파워포인트 이벤트 싱크 설정 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::ConnectPPTEventSink()
{
	if(m_pptApp == NULL)
	{
		return false;
	}//if

	DisconnectPPTEventSink();

	///*********************** Start of code to get connection point **************
  	//  Declare the events we want to catch.
  	//
  	//  Look for the coclass for Application in the typelib, msppt9.olb
  	//  then look for the word "source." The interface - EApplication -
  	//  is the next search target. When you find it you'll see the 
  	//  following guid a the events you'll be able to sink.
  	//  914934C2-5A91-11CF-8700-00AA0060263B
  	//	static const GUID IID_IEApplication =
  	//	{0x914934C2,0x5A91,0x11CF, {0x87,0x00,0x00,0xAA,0x00,0x60,0x26,0x3b}};
  
  	//  Steps for setting up events.
  	// 1. Get server's IConnectionPointContainer interface.
  	// 2. Call IConnectionPointContainerFindConnectionPoint()
  	//    to find the event we want to catch.
  	// 3. Call IConnectionPoint::Advise() with the IUnknown
  	//    interface of our implementation of the events.

	// Get IDispatch interface for Automation...
	IDispatch *pDisp = NULL;
	pDisp = m_pptApp->m_lpDispatch;

  	// Get server's (PPT) IConnectionPointContainer interface.
  	IConnectionPointContainer *pConnPtContainer;
  	HRESULT hr = pDisp->QueryInterface(
  							IID_IConnectionPointContainer,
  							(void **)&pConnPtContainer);
  	
  	if(!SUCCEEDED(hr)) 
  	{ 
		CString strError;
		strError.Format(_T("Couldn't get IConnectionPointContainer interface."));
		OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
 	}//if
  	
  	// Find connection point for events we're interested in.
  	hr = pConnPtContainer->FindConnectionPoint(
  											IID_IEApplication,
  											&m_pPPTEventConnectionPoint
  											);
  	
  	if(!SUCCEEDED(hr)) 
  	{
		CString strError;
		strError.Format(_T("Couldn't find connection point via event GUID."));
		OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
  	}//if

	m_pPPTEventSink = new CPPTEventSink(this, m_strPresentationName);
  	
	IUnknown *pUnk = NULL;
  	m_pPPTEventSink->QueryInterface(IID_IUnknown, (void**)&pUnk);
  	if(pUnk == NULL)
	{
		CString strError;
		strError.Format(_T("PPTEventSink fail to QueryInterface."));
		OnErrorMessage((WPARAM)(LPCSTR)strError, 0);
	
  		return false;
	}//if
  		
  	// Setup advisory connection!
  	hr = m_pPPTEventConnectionPoint->Advise(pUnk, &m_pPPTEventSink->m_Cookie);
  	if(!SUCCEEDED(hr))
	{
		CString strError;
		strError.Format(_T("PPTEventSink fail to Advise."));
		OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
	}//if
	  
  	// Release IConnectionPointContainer interface.
  	pConnPtContainer->Release();

	return true; 
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파워포인트 이벤트 싱크 해제 함수 \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::DisconnectPPTEventSink()
{
	if(m_pPPTEventConnectionPoint)
	{
		m_pPPTEventConnectionPoint->Unadvise(m_pPPTEventSink->m_Cookie); // PPT releases cookie	
 		m_pPPTEventConnectionPoint->Release();
		m_pPPTEventConnectionPoint = NULL;
	}//if

	if(m_pPPTEventSink)
	{
		m_pPPTEventSink->Release();
		m_pPPTEventSink = NULL;
	}//if
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼가 시작되었음을 설정 \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::SlideShowBegin()
{
	__DEBUG__("SlideShowBegin()", _NULL);

	__DEBUG__("COfficeApp::getInstance", _NULL);
	m_pptApp = COfficeApp::getInstance();
	if( m_pptApp == NULL )
	{
		return;
	}//if

	m_pptApp->SlideShowBegin(m_strPresentationName);

	//Invalidate(FALSE);	//배경화면을 다시 그려준다

	//슬라이드 쇼 창을 topMost로 설정
	m_bTopMost = true;
	//프로그램의 Top Most 속성을 해제해 주어야 뷰어가 위로 올라온다
	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	pclsHost->SetTopMost(false);
	//pclsHost->SetForegroundWindow();

	SetTopMostSlideShow();
	SetTimer(ID_PRESENTAION_SHOW, TIME_PRESENTAION_SHOW, NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼가 끝났음을 설정 \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::SlideShowEnd()
{
	//if(m_pptApp == NULL)
	//{
	//	return;
	//}//if

	//DisconnectPPTEventSink();

	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if

	/*
	HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SLIDE_WAIT));
	m_bgImage.CreateFromHBITMAP(hBitmap);

	Invalidate(FALSE);		//배경을 다시 그려준다.
	*/

	KillTimer(ID_PRESENTAION_SHOW);
	m_bTopMost = false;
	SetTopMostSlideShow();

	//프로그램의 Top Most 속성을 설정
	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	pclsHost->SetTopMost(true);

	ClosePresentation();

	if(m_bPlay)
	{
		//SetTimer(ID_PRESENTAION_END, TIME_PRESENTAION_END, NULL);
		m_dwStartTick = m_dwStartTick - (TIME_PPT_PLAYING*1000);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ppt 슬라이드 쇼를 닫는다 \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::ClosePresentation()
{
	__DEBUG__("ClosePresentation()", _NULL);

	__DEBUG__("COfficeApp::getInstance", _NULL);
	m_pptApp = COfficeApp::getInstance();
	if( m_pptApp == NULL )
	{
		return;
	}//if

	m_pptApp->ClosePresentation(m_strPresentationName);

/*
	if(m_pptApp == NULL)
	{
		return;
	}//if

	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	
	pDisp = m_pptApp->GetPresentations();
	pptPresentations.AttachDispatch(pDisp);

	//열려있는 presentation중에 같은 파일이 있는지 검사하여
	//이미 열려있다면 닫아주어야 한다.
	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;

	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(m_strPresentationName) == 0)
		{
			pptPresentation.Close();
			pptPresentation.ReleaseDispatch();
			break;
		}//if
		pptPresentation.ReleaseDispatch();

		pDisp = m_pptApp->GetPresentations();
		if(pDisp)
		{
			pptPresentations.ReleaseDispatch();
			pptPresentations.AttachDispatch(pDisp);
		}
		else
		{
			break;
		}//if
	}//for
	pptPresentations.ReleaseDispatch();
*/
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// EnumWindow를 통한 슬라이드쇼 윈도우를 찾는 call back 함수 \n
/// @param (HWND) hwnd : (in) 검색된 윈도우 핸들
/// @param (LPARAM) lParam : (in/out) 슬라이드 쇼 윈도우 정보를 갖는 ST_FindWnd 구조체
/// @return <형: BOOL> \n
///			<TRUE: 계속 검색을 진행한다> \n
///			<FALSE: 검색을 종료> \n
/////////////////////////////////////////////////////////////////////////////////
BOOL CPowerPointSchedule::EnumWndProc(HWND hwnd, LPARAM lParam)
{
	/*
	EnumWindows 를 사용하면 작업 표시줄에 나타나는 응용 프로그램을 열거할 수 있다.
	작업 관리자는 화면에 표시되는  top 윈도우 중에서 다음 속성을 만족하는 것을 열거한다.
	 
	WS_EX_APPWINDOW 속성을 가지고 있다.
	WS_EX_TOOLWINDOW 속성을 가지고 있지 않으면서, 소유주(owner) 윈도우가 없다.
	 
	위의 두 가지 조건을 체크하는 코드를 만들어 보면 다음과 같다.
	*/
	ST_FindWnd* pstFindWnd = (ST_FindWnd*)lParam;
	if(pstFindWnd == NULL)
	{
		__DEBUG__("Find window struct is null", _NULL);
		return FALSE;
	}//if

	//파워포인트 메인창과 슬라이드 쇼 창 2개가 있는데,
	//Microsoft PowerPoint - [SQI.ppt [호환 모드]] <== (한글)메인 파워포인트 창의 title
	//Microsoft PowerPoint <== (일본)메인 파워포인트 창의 title
	//PowerPoint 슬라이드 쇼 - [SQI.ppt [호환 모드]] <== 슬라이드 쇼 창의title
	//따라서, "Microsoft PowerPoint"가 있는 창은 파워포인트 메인창이고
	//"PowerPoint"와 슬라이드 쇼 파일명이 있는 창이 찾는 슬라이드 쇼 창이다.
	//단, 일본 office에서는 파일의 확장자(".ppt")가 title바에 나오지 않는다.
	char szTitle[256] = { 0x00 };
	::GetWindowText(hwnd, szTitle, 256);
	CString strTitle = szTitle;
	//__DEBUG__("Slide show", pstFindWnd->szSlideName);
	//__DEBUG__("Enum window", strTitle);

	//슬라이드 쇼 파일명이 있어야 한다.
	if(strTitle.Find(pstFindWnd->szSlideName, 0) == -1)
	{
		return TRUE;
	}//if

	//파워포인트 메인 창
	if(strTitle.Find("Microsoft PowerPoint") >= 0)
	{
		return TRUE;
	}//if

	//우리가 찾는 슬라이드 창
	if(strTitle.Find("PowerPoint") >= 0)
	{
		DWORD exStyle = GetWindowLong(hwnd, GWL_EXSTYLE);

		BOOL isVisible = ::IsWindowVisible(hwnd);
		BOOL isToolWindow = (exStyle & WS_EX_TOOLWINDOW);
		BOOL isAppWindow = (exStyle & WS_EX_APPWINDOW);
		BOOL isOwned = ::GetWindow(hwnd, GW_OWNER) ? TRUE : FALSE;

		if(isVisible && (isAppWindow || (!isToolWindow && !isOwned)))
		{
			// 응용 프로그램
			__DEBUG__("Success enum window", strTitle);
			pstFindWnd->hWnd = hwnd;

			return FALSE;
		}//if
	}//if	

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼 창을 TopMost로 설정한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::SetTopMostSlideShow()
{
	__DEBUG__("SetTopMostSlideShow()", _NULL);

	ST_FindWnd stFindWnd;
	memset(&stFindWnd, 0x00, sizeof(ST_FindWnd));
	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(m_strMediaFullPath, cDrive, cPath, cFilename, cExt);
	strcpy(stFindWnd.szSlideName, cFilename);
	//strcat(stFindWnd.szSlideName, cExt);	//일본 office는 title바에 확장자가 안나온다...
	EnumWindows(EnumWndProc, (LPARAM)&stFindWnd);

	if(stFindWnd.hWnd == NULL)
	{
		__ERROR__("Fail to enum slideshow window", m_strPresentationName);
		return;
	}//if

	//HWND hwnd = ::GetForegroundWindow();
	if(m_bTopMost)
	{
		//if( hwnd != stFindWnd.hWnd )
		{
			__DEBUG__("HWND_TOPMOST", _NULL);
			::SetWindowPos(stFindWnd.hWnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
			//::ShowWindow(stFindWnd.hWnd, SW_SHOW);
			::SetForegroundWindow(stFindWnd.hWnd);
			//KillTimer(ID_PRESENTAION_SHOW);
		}
	}
	else
	{
		__DEBUG__("HWND_NOTOPMOST", _NULL);
		::SetWindowPos(stFindWnd.hWnd, HWND_NOTOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
		//::ShowWindow(stFindWnd.hWnd, SW_HIDE);
	}//if


	//파워포인트 타이틀 이름으로 창의 핸들을 찾아 TopMost로 설정
	// (예 : PowerPoint 슬라이드 쇼 - [UVC.pps [호환 모드]])
	//HWND hWndPPT;	//PPT slideshow 윈도우 핸들

/*
	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(m_strMediaFullPath, cDrive, cPath, cFilename, cExt);

	CString strWndTitle, strReadOnlyWndTitle, strShow, strMode, strReadOnly;
	switch(m_nLocale)
	{
	case 81 :		//일본
		{
			char szShow[15] = { 0x83, 0x58, 0x83, 0x89, 0x83, 0x43, 0x83, 0x68, 0x83, 0x56, 0x83, 0x87, 0x81, 0x5b, 0x00 };
			char szMode[11] = { 0x8c, 0xdd, 0x8a, 0xb7, 0x83, 0x82, 0x81, 0x5b, 0x83, 0x68, 0x00 };
			char szReadOnly[13] = { 0x93, 0xc7, 0x82, 0xdd, 0x8e, 0xe6, 0x82, 0xe8, 0x90, 0xea, 0x97, 0x70, 0x00 };

			strShow = szShow;
			strMode = szMode;
			strReadOnly = szReadOnly;
		}
		break;
	case 1:			//USA
	case 7:			//러시아
	case 86:		//중국
	case 82:		//KOR
	default :
		{
			strShow = "슬라이드 쇼";
			strMode = "호환 모드";
			strReadOnly = "읽기 전용";
		}
	}//switch
	strWndTitle.Format("PowerPoint %s - [%s%s [%s]]", strShow, cFilename, cExt, strMode);
	strReadOnlyWndTitle.Format("PowerPoint %s - [%s%s [%s] [%s]]", strShow, cFilename, cExt, strReadOnly, strMode);
	__DEBUG__("strWndTitle", strWndTitle);
	__DEBUG__("strReadOnlyWndTitle", strReadOnlyWndTitle);
	
	hWndPPT = ::FindWindow(NULL, strWndTitle);
	if(hWndPPT == NULL)
	{
		hWndPPT = ::FindWindow(NULL, strReadOnlyWndTitle);
		if(hWndPPT == NULL)
		{
			__DEBUG__("PPT window find fail !!!", _NULL);
		}//if
	}//if
*/


/*
	//Title로 창을 찾는 로직이 일본어에서 문제가 있어서 수정함
	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(m_strMediaFullPath, cDrive, cPath, cFilename, cExt);
	CString strMediaName, strWndTitle;
	strMediaName.Format("%s%s", cFilename, cExt);

	scratchUtil* aUtil = scratchUtil::getInstance();
	hWndPPT = aUtil->getWHandle("POWERPNT.EXE");
	char szTitle[256] = { 0x00 };
	::GetWindowText(hWndPPT, szTitle, 256);
	strWndTitle = szTitle;
	if(strWndTitle.Find(strMediaName, 0) == -1)
	{
		//BOOL bRet = EnumChildWindows(hWndPPT, &EnumChildProc, (LPARAM)(LPCSTR)strWndTitle);
		HWND hChild = NULL;
		do
		{
			HWND hChild = ::GetWindow(hWndPPT, GW_CHILD);
			if(hChild != NULL)
			{
				::GetWindowText(hWndPPT, szTitle, 256);
				strWndTitle = szTitle;

				if(strWndTitle.Find(strMediaName, 0) != -1)
				{
					hWndPPT = hChild;
					break;
				}//if
			}//if
		} while(hChild);

		if(hChild == NULL)
		{
			__DEBUG__("Can't found ppt window", _NULL);
			hWndPPT = NULL;
		}//if
	}//if

	__DEBUG__("WindowTitle", strWndTitle);
	__DEBUG__("PPT topmost", m_bTopMost);

	if(m_bTopMost)
	{
		if(hWndPPT)
		{
			::SetWindowPos(hWndPPT, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
			::ShowWindow(hWndPPT, SW_SHOW);
			::SetForegroundWindow(hWndPPT);
			
		}//if
	}
	else
	{
		if(hWndPPT)
		{
			::SetWindowPos(hWndPPT, HWND_NOTOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
			::ShowWindow(hWndPPT, SW_HIDE);
		}//if
		hWndPPT = NULL;
	}//if
*/
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼를 일정한 시간후에 종료하도록 설정 \n
/// @param (float) ftTime : (in) 슬라이드의 재생 시간
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::SetSlideShowEnd(float ftTime)
{
	//SetTimer(ID_SLIDE_SHOW_END, TIME_SLIDE_SHOW_END, NULL);
	int nTime = (int)(ftTime*1000);
	SetTimer(ID_SLIDE_SHOW_END, nTime, NULL);
}

void CPowerPointSchedule::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	//CWnd::OnTimer(nIDEvent);
	CSchedule::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
		/*
	case SCHEDULE_SHOW_ID:
		{
			KillTimer(SCHEDULE_SHOW_ID);
			ShowWindow(SW_SHOW);
		}
		break;

	case SCHEDULE_HIDE_ID:
		{
			KillTimer(SCHEDULE_HIDE_ID);
			ShowWindow(SW_HIDE);
		}
		break;
		*/
	case ID_SLIDE_SHOW_END:
		{
			KillTimer(ID_SLIDE_SHOW_END);
			//Stop();
			m_dwStartTick = m_dwStartTick - (TIME_PPT_PLAYING*1000);
		}
		break;
		/*
	case ID_PRESENTAION_END:
		{
			KillTimer(ID_PRESENTAION_END);
			m_dwStartTick = m_dwStartTick - (TIME_PPT_PLAYING*1000);
		}
		break;
		*/
	case ID_PRESENTAION_SHOW:
		{
			SetTopMostSlideShow();
		}
		break;
	}//switch

	//CSchedule::OnTimer(nIDEvent);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 슬라이드 쇼의 해상도를 시스템의 해상도에 맞춘다. \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::SetSlideShowResolution()
{
	HKEY hNewKey = NULL;
	LONG lRet = 0;

	int nX, nY, nCx, nCy;
	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	pclsHost->GetAppPosition(nX, nY, nCx, nCy);

	CString strVersion = m_pptApp->GetVersion();
	CString strKey;
	strKey.Format("Software\\Microsoft\\Office\\%s\\PowerPoint\\Options", strVersion);

	//X
	lRet = RegCreateKeyEx(HKEY_CURRENT_USER, strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hNewKey, NULL);
	if(lRet == ERROR_SUCCESS)
	{
		lRet = RegSetValueEx(hNewKey, "SlideShowResolutionX", 0, REG_DWORD, (LPBYTE)&nCx, sizeof(DWORD));
		if(lRet != ERROR_SUCCESS)
		{
			__DEBUG__("SlideShowResolutionX set fail", nCx);
		}//if
		RegCloseKey(hNewKey);
	}//if

	//Y
	lRet = RegCreateKeyEx(HKEY_CURRENT_USER, strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hNewKey, NULL);
	if(lRet == ERROR_SUCCESS)
	{
		lRet = RegSetValueEx(hNewKey, "SlideShowResolutionY", 0, REG_DWORD, (LPBYTE)&nCy, sizeof(int));
		if(lRet != ERROR_SUCCESS)
		{
			__DEBUG__("SlideShowResolutionX set fail", nCy);
		}//if
		RegCloseKey(hNewKey);
	}//if
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Presentation이 Open 된 이후 슬라이드 쇼를 시작하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::PresentationOpen()
{
	__DEBUG__("PresentationOpen() 1",NULL);

	__DEBUG__("COfficeApp::getInstance", _NULL);
	m_pptApp = COfficeApp::getInstance();
	if( m_pptApp )
	{
		m_pptApp->SetOpenComplete(m_strPresentationName);
	}//if

	CWnd* main_wnd = ::AfxGetMainWnd();
	::SetForegroundWindow(main_wnd->GetSafeHwnd());
/*
	if(m_pptApp == NULL)
	{
		return;
	}//if
	//_DEBUG__("PresentationOpen() 2",NULL);

	//Invalidate(FALSE);	//배경화면을 다시 그려준다
	
	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowSettings	pptSlideshow;			///<MS-PowerPoint SlideShow settings
	
	pDisp = m_pptApp->GetPresentations();
	pptPresentations.AttachDispatch(pDisp);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(m_strPresentationName) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
		__ERROR__("Fail to get presentation", m_strPresentationName); 
		Stop();
		return;
	}//if

	pDisp = pptPresentation.GetSlideShowSettings();
	pptSlideshow.AttachDispatch(pDisp);
	pptSlideshow.SetLoopUntilStopped(-1);		//msoTrue
	pptSlideshow.Run();

	pptSlideshow.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CTVSchedule

CTVSchedule::CTVSchedule(CTemplate* pParentParentWnd, CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentParentWnd, pParentWnd, lpszID, bDefaultSchedule)
,	m_nChennel(0)
,	m_nModeTV(modeAll)
,	m_bPause(false)
,	m_rgbBgColor(0xffffffff)
,	m_rgbFgColor(0xffffffff)
,	m_bHasTV(false)
{
	SetSchedule(lpszID);
}

CTVSchedule::~CTVSchedule()
{
	__DEBUG__("\t\t\tDestroy TV Schedule", m_scheduleId.c_str());

	//CloseFile();
}

IMPLEMENT_DYNAMIC(CTVSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CTVSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
END_MESSAGE_MAP()


BOOL CTVSchedule::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	return TRUE;

	//return CSchedule::OnEraseBkgnd(pDC);
}


void CTVSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		CRect rtClient;
		GetClientRect(rtClient);

		CMemDC memDC(&dc);
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}//if
}


int CTVSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;
/*
	CRect rect;
	GetClientRect(rect);

	__DEBUG__("\t\t\tCreate TV", _NULL);
	if(!m_playerTV.Create(this, rect))
	{
		__DEBUG__("TV Player create fail", _NULL);
		return -1;
	}//if
*/
	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return 0;
}

void CTVSchedule::OnDestroy()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CSchedule::OnDestroy();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}


void CTVSchedule::SetSchedule(LPCSTR lpszID)
{
	__DEBUG_SCHEDULE_BEGIN__(lpszID)

	//CSchedule::SetSchedule(lpszID);

	CString key;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		key.Format("comment%d", i+1);
		m_comment[i] = MNG_PROFILE_READ(lpszID, key);
	}

	m_bgColor = MNG_PROFILE_READ(lpszID, "bgColor");
	m_fgColor = MNG_PROFILE_READ(lpszID, "fgColor");
	m_soundVolume = atoi(MNG_PROFILE_READ(lpszID, "soundVolume"));

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}//if

	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}//if

	__DEBUG_SCHEDULE_END__(lpszID)
}


double CTVSchedule::GetTotalPlayTime()
{
	return (double)m_runningTime*1000; // sec -> millisec
}

double CTVSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CTVSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		__DEBUG__("\t\t\tAlready Open", _NULL);
		return true;
	}

	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(nGrade > 1)
	{
		__DEBUG__("\t\t\tMute Mode", _NULL);
		m_bMute = true;
	}//if

	CRect rect;
	GetClientRect(rect);

	__DEBUG__("\t\t\tCreate TV", _NULL);
	if(!m_playerTV.Create(this, rect))
	{
		__DEBUG__("TV Player create fail", _NULL);
		SetNoContentsFile();
		m_bHasTV = false;
	}
	else
	{
		m_bHasTV = true;
	}//if
/*
	__DEBUG__("TV Mode", m_comment[2].c_str());
	__DEBUG__("TV Channel", m_comment[3].c_str());
	__DEBUG__("TV Volume", m_soundVolume);

	m_nModeTV = (TV_MODE)atoi(m_comment[2].c_str());
	m_nChennel = atoi(m_comment[3].c_str());

	if(!m_playerTV.SetTVMode(m_nModeTV))
	{
		__DEBUG__("Fail to set TV mode", _NULL);

		return false;
	}//if

	if(!m_playerTV.SetChannel(m_nChennel))
	{
		__DEBUG__("Fail to set TV channel", _NULL);
		
		return false;
	}//if

	int nVolume;
	(m_bMute == true) ? (nVolume = 0) : (nVolume = m_soundVolume);
	if(!m_playerTV.SetVolume(nVolume))
	{
		__DEBUG__("Fail to set TV volume", _NULL);
		
		return false;
	}//if
*/
	m_bOpen = true;

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true; 
}


bool CTVSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();
	}//if

	return true;
}

bool CTVSchedule::Play()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_bPause)
	{
		if(m_bHasTV)
		{
			if(!m_playerTV.Play())
			{
				__DEBUG__("Fail to play TV", _NULL);
				return false;
			}//if
			CHost* pHost = (CHost*)::AfxGetMainWnd();
			pHost->SetTVRunningState(true);
		}//if
		m_bPause = false;

		return true;
	}//if

	m_dwStartTick = ::GetTickCount();
	CSchedule::Play();
	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"START", m_runningTime, m_comment[3].c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());

	if(!m_bOpen)
	{
		__DEBUG__("\t\t\tFile not opend !!!", m_comment[3].c_str());
		__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
		WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_comment[3].c_str(), "", "File not opend", m_pParentParentWnd->GetTemplateIndex());
		CSchedule::Stop();

		return false;
	}//if

	__DEBUG__("TV Mode", m_comment[2].c_str());
	__DEBUG__("TV Channel", m_comment[3].c_str());
	__DEBUG__("TV Volume", m_soundVolume);

	m_nModeTV = (TV_MODE)atoi(m_comment[2].c_str());
	m_nChennel = atoi(m_comment[3].c_str());
	int nVolume;
	(m_bMute == true) ? (nVolume = 0) : (nVolume = m_soundVolume);

	if(m_bHasTV)
	{
		if(!m_playerTV.SetTVMode(m_nModeTV))
		{
			__DEBUG__("Fail to set TV mode", _NULL);
			__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
				WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_comment[3].c_str(), "", "Fail to set TV mode", m_pParentParentWnd->GetTemplateIndex());
			CSchedule::Stop();

			return false;
		}//if

		if(!m_playerTV.SetChannel(m_nChennel))
		{
			__DEBUG__("Fail to set TV channel", _NULL);
			__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
				WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_comment[3].c_str(), "", "Fail to set TV channel", m_pParentParentWnd->GetTemplateIndex());
			CSchedule::Stop();

			return false;
		}//if

		if(!m_playerTV.SetVolume(nVolume))
		{
			__DEBUG__("Fail to set TV volume", _NULL);
			__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
				WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_comment[3].c_str(), "", "Fail to set TV volume", m_pParentParentWnd->GetTemplateIndex());
			CSchedule::Stop();

			return false;
		}//if

		m_playerTV.Stop();
		m_pParentParentWnd->ShowWindow(SW_SHOW);
		m_pParentWnd->ShowWindow(SW_SHOW);
		ShowWindow(SW_SHOW);
		if(!m_playerTV.Play())
		{
			__DEBUG__("Fail to play TV", _NULL);
			return false;
		}//if
		CHost* pHost = (CHost*)::AfxGetMainWnd();
		pHost->SetTVRunningState(true);

		//// check mode,channel,volume
		//::Sleep(1000);
		//TV_MODE tv_mode = m_playerTV.GetTVMode();
		//int channel = m_playerTV.GetChannel();
		//int volume = m_playerTV.GetVolume();
		//__DEBUG__("Check TV Mode", (int)tv_mode);
		//__DEBUG__("Check TV Channel", channel);
		//__DEBUG__("Check TV Volume", volume);

		//if( tv_mode != m_nModeTV )
		//{
		//	__DEBUG__("Fail to set TV mode", (int)tv_mode);
		//	//if( !m_playerTV.SetTVMode(m_nModeTV) )
		//	//{
		//	//	__DEBUG__("Fail to set TV mode", (int)tv_mode);
		//	//}
		//}

		//if( channel != m_nChennel )
		//{
		//	__DEBUG__("Fail to set TV channel", channel);
		//	//if( !m_playerTV.SetChannel(m_nChennel) )
		//	//{
		//	//	__DEBUG__("Fail to set TV channel", channel);
		//	//}
		//}

		//if( volume != nVolume )
		//{
		//	__DEBUG__("Fail to set TV volume", volume);
		//	//if( !m_playerTV.SetVolume(nVolume) )
		//	//{
		//	//	__DEBUG__("Fail to set TV volume", volume);
		//	//}
		//}
	}//if

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

void CTVSchedule::SoundVolumeMute(bool bMute)
{
	m_bMute = bMute;
	if(m_bHasTV)
	{
		if(!m_playerTV.SetVolume(m_bMute ? 0 : m_soundVolume))
		{
			__DEBUG__("Fail to TV mute", _NULL);
		}//if
	}//if
}

bool CTVSchedule::Pause()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_bHasTV)
	{
		if(!m_playerTV.Stop())
		{
			__DEBUG__("Fail to TV stop", _NULL);
		}//if
		CHost* pHost = (CHost*)::AfxGetMainWnd();
		pHost->SetTVRunningState(false);
	}//if
	m_bPause = true;

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CTVSchedule::Stop()
{
	__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_bHasTV)
	{
		if(!m_playerTV.Stop())
		{
			__DEBUG__("Fail to TV stop", _NULL);
		}//if
		CHost* pHost = (CHost*)::AfxGetMainWnd();
		pHost->SetTVRunningState(false);
	}//if
	
	WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"SUCCEED", m_runningTime, m_filename.c_str(), "", "", m_pParentParentWnd->GetTemplateIndex());

	CSchedule::Stop();

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}


CString	CTVSchedule::ToString()
{
	CString str;
	str.Format(
		"-TV Schedule-\r\n"
		"%s\r\n"
		"%s\r\n"
		"bgColor = %s\r\n"
		"fgColor = %s\r\n"
		"soundVolume = %d\r\n"
		"\r\n"
		"m_nModeTV = %d\r\n"
		"m_nChennel = %d\r\n"
		,	CSchedule::ToString()
		,	to_string(m_comment, TICKER_COUNT)
		,	m_bgColor.c_str()		// 자막용
		,	m_fgColor.c_str()		// 자막용
		,	m_soundVolume

		,	m_nModeTV
		,	m_nChennel
	);

	return str;
}

void CTVSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("VIDEO Schedule Attributes");

	to_string(m_comment, TICKER_COUNT, pGrid, hs);

	pGrid->AddStringItem(hs, "bgColor", m_bgColor.c_str());
	pGrid->AddStringItem(hs, "fgColor", m_fgColor.c_str());
	pGrid->AddIntegerItem(hs, "soundVolume", m_soundVolume);

	hs = pGrid->AddSection("member variables");

	pGrid->AddIntegerItem(hs, "m_nModeTV", m_nModeTV);
	pGrid->AddIntegerItem(hs, "m_nChennel", m_nChennel);
}
