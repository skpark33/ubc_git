// ThreadOpenFile.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "ThreadOpenFile.h"
#include "Host.h"


// CThreadOpenFile

IMPLEMENT_DYNCREATE(CThreadOpenFile, CWinThread)

CThreadOpenFile::CThreadOpenFile()
:	m_pParent(NULL)
,	m_pBackWnd(NULL)
,	m_bCancle(false)	
{
}

CThreadOpenFile::~CThreadOpenFile()
{
}

BOOL CThreadOpenFile::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CThreadOpenFile::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CThreadOpenFile, CWinThread)
END_MESSAGE_MAP()


// CThreadOpenFile 메시지 처리기입니다.

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// thread의 부가정보 설정 \n
/// @param (void*) pParent : (in) thread 소유자 포인터
/// @param (void*) pBackWnd : (in) BackWnd 포인터
/////////////////////////////////////////////////////////////////////////////////
void CThreadOpenFile::SetThreadParam(void* pParent, void* pBackWnd)
{
	m_pParent	= pParent;
	m_pBackWnd	= pBackWnd;
}

int CThreadOpenFile::Run()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CHost* pHost		= (CHost*)m_pParent;
	CBackWnd* pBackWnd	= (CBackWnd*)m_pBackWnd;
	bool bSendOpenMsg = false;

	if(!pHost || !pHost->GetSafeHwnd())
	{
		return 0;
	}//if

	__DEBUG_HOST_BEGIN__(_NULL)

	//PRG_CNT_OPEN_FILE 숫자만큼의 진행상황을 나누어 표시한다
	int nCount = (int)pHost->GetCountOfTemplatePlayAry();
	if(nCount == 0)
	{
		//Progress set
		pBackWnd->PostMessage(WM_SET_POS, E_INIT_OPEN_FILE, 300);
		pHost->PostMessage(WM_THREAD_OPEN_FILE, (WPARAM)true, 0);

		__DEBUG_HOST_END__(_NULL)

		return 0;
	}//if
	
	//int nFacter = PRG_CNT_OPEN_FILE/nCount;
	int nFacter = pBackWnd->m_nModeValue[E_INIT_OPEN_FILE]/nCount;
	if(nFacter == 0)
	{
		nFacter = 1;
	}//if

	CTemplatePlay* pclsPlay = NULL;
	CTemplate* pclsTemplate = NULL;
	CTemplateMap mapTemplate;		//한번 open한 template을 중복되서 open하지 않도록...

	if(pclsTemplate = pHost->GetdefaultTemplate())
	{
		pBackWnd->PostMessage(WM_SET_POS, E_INIT_OPEN_FILE, nFacter);
		pclsTemplate->OpenFile();

		pHost->PostMessage(WM_THREAD_OPEN_FILE, (WPARAM)false, 0);
		bSendOpenMsg = true;
	}//if

	for(int i=0; i<nCount; i++)
	{
		if(m_bCancle)
		{
			__DEBUG__("ThreadOpenFile Cnacled !!!", _NULL);
			return 0;
		}//if

		//Progress set
		if(!bSendOpenMsg)
		{
			pBackWnd->PostMessage(WM_SET_POS, E_INIT_OPEN_FILE, nFacter);
			bSendOpenMsg = true;
		}//if

		pclsPlay = pHost->GetAtTemplatePlay(i);
		if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL)
		{
			continue;
		}//if

		if(!mapTemplate.Lookup(pclsPlay->m_strTemplateID, (void*&)pclsTemplate))
		{
			pclsPlay->m_pclsTemplate->OpenFile();
			mapTemplate.SetAt(pclsPlay->m_strTemplateID, pclsPlay->m_pclsTemplate);
		}//if
	}//for
	mapTemplate.RemoveAll();

	pHost->PostMessage(WM_THREAD_OPEN_FILE, (WPARAM)true, 0);

	__DEBUG_HOST_END__(_NULL)

	return 0;
	//return CWinThread::Run();
}
