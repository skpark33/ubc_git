#pragma once



// CThreadSave

class CThreadSave : public CWinThread
{
	DECLARE_DYNCREATE(CThreadSave)

//protected:
public:
	CThreadSave();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CThreadSave();

	void*			m_pParent;							///<thread의 소유자 포인터
	bool			m_bCancle;							///<thread의 작업 취소 여부

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	void	SetThreadParam(void* pParent);				///<thread의 부가정보 설정

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual int Run();
};


