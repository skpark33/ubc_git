// AnnounceDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "Announce_flash_Dlg.h"
#include "MemDC.h"
#include "Host.h"

//동영상 화면갱신 타이머
#define		ID_VIDEO_REPAINT			4014
#define		TIME_VIDEO_REPAINT			1000
//동영상 재생종료 타이머
#define		ID_VIDEO_PLAYEND			4015
#define		TIME_VIDEO_PLAYEND			300

// CAnnounce_flash_Dlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAnnounce_flash_Dlg, CAnnounceDlg)

CAnnounce_flash_Dlg::CAnnounce_flash_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent /*=NULL*/)
	: CAnnounceDlg(nPosX,nPosY,nCx,nCy,pParent)
{
	__DEBUG__("CAnnounce_flash_Dlg", _NULL);
	m_pdlgFlash = NULL;

	m_bOpen = false;
	m_bFileNotExist = false;

	m_dwStartTick = ::GetTickCount();
}

CAnnounce_flash_Dlg::~CAnnounce_flash_Dlg()
{
}

BEGIN_MESSAGE_MAP(CAnnounce_flash_Dlg, CAnnounceDlg)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()

int CAnnounce_flash_Dlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG__("CAnnounce_flash_Dlg::OnCreate()", _NULL);
	if (CAnnounceDlg::OnCreate(lpCreateStruct) == -1){
		return -1;
	}

	return 0;
}
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 시작하도록 한다. \n
/// @param (CAnnounce*) pAnn : (in) 방송을 해야하는 공지 객체 주소값
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_flash_Dlg::BeginAnnounce(CAnnounce* pAnn)
{
	if(!m_pAnnPlay)
	{
		m_pAnnPlay = pAnn;
	}
	else if((*m_pAnnPlay == *pAnn) && m_pAnnPlay->m_bPlaying)
	{
		//if(!(((CHost*)m_pParentWnd)->IsWindowVisible())){
			__DEBUG__("Same Annnounce Shows", pAnn->m_strAnnounceId.c_str());
			ShowWindow(SW_SHOW);
			((CHost*)m_pParentWnd)->SetFocus();
		//}
		return;
	}
	else
	{
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = pAnn;
	}//if

	char	szBuf[_MAX_PATH] = { 0x00 };
	::GetModuleFileName(NULL, szBuf, _MAX_PATH);

	char drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
	_splitpath(szBuf, drive, path, filename, ext);

	m_strMediaFullPath.Format("%s\\SQISOFT\\Contents\\Enc\\announce\\%s",drive, m_pAnnPlay->m_strBgFile.c_str());


	__DEBUG__("BeginAnnounce",	m_strMediaFullPath);
	__DEBUG__("BeginAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());

	CString strLog = m_pAnnPlay->m_strBgFile.c_str();
	WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(),m_pAnnPlay->m_strTitle.c_str(), "TRY", m_pAnnPlay->GetRunningTime(), strLog);

	MoveAnnounceWindow(m_nParentPosX, m_nParentPosY, m_nParentCx, m_nParentCy, m_pAnnPlay);

	__DEBUG__("OpenFile",	_NULL);
	if(OpenFile(0)){
		m_pAnnPlay->m_bPlaying = true;
		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "START", m_pAnnPlay->GetRunningTime(), strLog);
		//__DEBUG__("ShowWindow", 0);
		ShowWindow(SW_SHOW);
		((CHost*)m_pParentWnd)->SetFocus();
	}else{
		SetNoContentsFile();
		this->RedrawWindow();
	}

	__DEBUG__("FlashRunTimeCheck",	_NULL);
	if(!m_pthreadShift)
	{
		m_pthreadShift = ::AfxBeginThread(CAnnounce_flash_Dlg::FlashRunTimeCheck, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pthreadShift->m_bAutoDelete = TRUE;
		m_bExitThread = false;
		m_hevtShift = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pthreadShift->ResumeThread();
		//EndAnnounce();
	}//if

	SetTransParency(RGB(0,0,0), 1, GetSafeHwnd());
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 중지하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_flash_Dlg::EndAnnounce()
{
	if(m_pAnnPlay)
	{
		__DEBUG__("EndAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());

		CString strLog = m_pAnnPlay->m_strBgFile.c_str();

		CloseFile();

		m_bExitThread = true;
		SetEvent(m_hevtShift);
		CloseHandle(m_hevtShift);
		m_hevtShift = NULL;
		m_pthreadShift = NULL;

		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "END", m_pAnnPlay->GetRunningTime(), strLog);

		ShowWindow(SW_HIDE);
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = NULL;
	}//if
}
void CAnnounce_flash_Dlg::OnDestroy()
{
	CloseFile();

	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if
	CAnnounceDlg::OnDestroy();

}

void CAnnounce_flash_Dlg::OnPaint()
{
	__DEBUG__("OnPaint",NULL);
	if(!m_bOpen) return;

	__DEBUG__("OnPaint Start",NULL);
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	
	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		CRect rtClient;
		GetClientRect(rtClient);

		CMemDC memDC(&dc);
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}//if
	else
	{
		CRect rtClient;
		GetClientRect(rtClient);
		dc.FillSolidRect(rtClient, RGB(0, 0, 0));
	}

	if(m_ticker && 	m_ticker->IsPlay()){
		m_ticker->BringWindowToTop();
		m_ticker->SetForegroundWindow();
		m_ticker->SetFocus();
	}else{
		BringWindowToTop();
		SetForegroundWindow();
		SetFocus();
	}

	__DEBUG__("OnPaint End",NULL);
}

void CAnnounce_flash_Dlg::SetNoContentsFile()
{
	__DEBUG__("Contents file not exist", m_strMediaFullPath);

	//컨텐츠 파일이 없는 경우 액박 표시해 준다.
	HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_XBOX));
	m_bgImage.CreateFromHBITMAP(hBitmap);
	m_bOpen = true;
	m_bFileNotExist = true;
}

bool CAnnounce_flash_Dlg::Play()
{
	__DEBUG__("Play start", _NULL);
	if(!m_bOpen)
	{
		__DEBUG__("File not opened", _NULL);
		return false;
	}
	if(m_bFileNotExist)
	{
		return true;
	}//if	
	m_dwStartTick = ::GetTickCount();

	if(!m_pdlgFlash->Play(m_strMediaFullPath, (CWnd*)this))
	{
		__DEBUG__("Flash file play failed", m_strMediaFullPath);
		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(),m_pAnnPlay->m_strTitle.c_str(), "FAIL", m_pAnnPlay->GetRunningTime(), "Fail to play flash movie");
		return false;
	}//if
	m_pAnnPlay->m_bPlaying = true;

	//메인윈도우에 focus를 넘겨야 한다.
	CWnd* pMain = ::AfxGetMainWnd();
	if(pMain)
	{
		pMain->SetFocus();
	}//if
	return true;
}

bool CAnnounce_flash_Dlg::Stop()
{
	if(m_pdlgFlash)
	{
		m_pdlgFlash->Stop();
	}//if
	if(m_pAnnPlay) m_pAnnPlay->m_bPlaying = false;

	//메인윈도우에 focus를 넘겨야 한다.
	CWnd* pMain = ::AfxGetMainWnd();
	if(pMain)
	{
		pMain->SetFocus();
	}//if
	if(m_bFileNotExist)
	{
		__DEBUG__("Play stop : Fail(Content file does not exist)", NULL);
	}
	else
	{
		__DEBUG__("Play stop : Succeed", NULL);
	}//if

	return true;
}

bool CAnnounce_flash_Dlg::OpenFile(int nGrade)
{
	__DEBUG__("OpenFile-Step1",	_NULL);
	if(m_bOpen) 
	{ 
		if(m_pAnnPlay->m_bPlaying)
		{
			__DEBUG__("\t\t\tAlready Open", _NULL);
			return true;
		}
		CloseFile();
	}

	__DEBUG__("OpenFile-Step2",	_NULL);
	CFileStatus status;
	if(CFile::GetStatus(m_strMediaFullPath, status ) == FALSE)
	{
		//컨텐츠 파일이 없는 경우 액박 표시해 준다.
		SetNoContentsFile();
		return false;
	}
	__DEBUG__("OpenFile-Step3",	_NULL);
	if(m_pdlgFlash)
	{
		m_pdlgFlash->Stop();
		m_pdlgFlash->DestroyWindow();
		delete m_pdlgFlash;
		m_pdlgFlash = NULL;
	}//if

	__DEBUG__("OpenFile-Step4",	_NULL);
	m_pdlgFlash = new CFlashDlg(this);
	m_pdlgFlash->Create(IDD_FLASH_DLG, this);
	m_pdlgFlash->ShowWindow(SW_HIDE);
	CRect rtRect;
	GetClientRect(&rtRect);
	__DEBUG__("OpenFile-Step5",	_NULL);
	m_pdlgFlash->MoveWindow(0, 0, rtRect.Width(), rtRect.Height());	
	if(!m_bOpen)	m_bOpen = true;
	return Play(); 
}

bool CAnnounce_flash_Dlg::CloseFile()
{
	__DEBUG__("CloseFile()",NULL);
	
	Stop();

	if(m_bOpen)
	{
		if(m_pdlgFlash)
		{
			m_pdlgFlash->DestroyWindow();
			delete m_pdlgFlash;
			m_pdlgFlash = NULL;
		}//if
		m_bOpen = false;
	}//if
	return true;
}


void CAnnounce_flash_Dlg::RePlay()
{
	__DEBUG__("Flash Replay",0);
	Stop();
	Play();
}

double CAnnounce_flash_Dlg::GetTotalPlayTime()
{
	if(m_pAnnPlay){
		return (double)(strtoul(m_pAnnPlay->m_strComment[1].c_str(), NULL,10)*1000); // sec -> millisec
	}
	return 0.0;
}

double CAnnounce_flash_Dlg::GetCurrentPlayTime()
{
	return (double)GetTickDiff(m_dwStartTick,::GetTickCount());
}

double CAnnounce_flash_Dlg::GetTickDiff(DWORD dwStart, DWORD dwEnd)
{
	if(dwStart > dwEnd)
		return double((MAXDWORD - dwStart) + dwEnd);
	return double(dwEnd - dwStart);
}


UINT CAnnounce_flash_Dlg::FlashRunTimeCheck(LPVOID pParam)
{
	CAnnounce_flash_Dlg* pParent = (CAnnounce_flash_Dlg*)pParam;

	while(!pParent->m_bExitThread)
	{
		DWORD dwEventRet = WaitForSingleObject(pParent->m_hevtShift, 1000);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			__DEBUG__("Event set : FlashRunTimeCheck", _NULL);
			break;
		}
		//__DEBUG__("Timeout   : FlashRunTimeCheck", _NULL);

		double runTime = pParent->GetTotalPlayTime();
		double currentTime =  pParent->GetCurrentPlayTime();
		
		if( runTime && runTime <= currentTime){
			pParent->RePlay();
		}
	}//while
	__DEBUG__("Exit PlayImageThread", _NULL);
	return 1;
}

void CAnnounce_flash_Dlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 20000;
	lpMMI->ptMaxTrackSize.y = 10000;
	lpMMI->ptMaxSize.x = 20000;
	lpMMI->ptMaxSize.y = 10000;

	CAnnounceDlg::OnGetMinMaxInfo(lpMMI);
}
