#pragma once

#include "Announce.h"

// CAnnounceDlg 대화 상자입니다.

class CAnnounceDlg : public CDialog
{
	DECLARE_DYNAMIC(CAnnounceDlg)

public:
	CAnnounceDlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAnnounceDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ANNOUNCE_DLG };

protected:
	CAryAnnounce		m_aryAnnounce;				///<긴급공지 배열
	CCriticalSection	m_csAnnounce;				///<동기화

	int					m_nParentPosX;				///<Player 창의 X 좌표
	int					m_nParentPosY;				///<Player 창의 Y 좌표
	int					m_nParentCx;				///<Player 창의 넓이
	int					m_nParentCy;				///<Player 창의 높이

	int					m_nPosX;					///<공지 창의 X 좌표
	int					m_nPosY;					///<공지 창의 Y 좌표
	int					m_nCx;						///<공지 창의 넓이
	int					m_nCy;						///<공지 창의 높이
	
	CFont				m_fontTicker;				///<공지의 폰트
	COLORREF			m_rgbBgColor;				///<공지의 배경 칼라
	COLORREF			m_rgbFgColor;				///<공지의 폰트 칼라
	CString				m_strAnnounce;				///<공지의 내용
	CAnnounceDlg		*m_ticker;
	unsigned long		m_timerPeriod;

public:
	bool		AddAnnounce(CAnnounce* pAnnounce);				///<Announce 객체를 배열에 추가한다.
	bool		RemoveAnnounce(CString strAnnounceId);			///<Announce 객체를 배열에서 삭제한다.
	bool		RemoveAnnounce(int nIndex);						///<Announce 객체를 배열에서 삭제한다.
	void		ClearAnnounceAry(void);							///<긴급공지 배열을 정리한다.
	CAnnounce*	CheckAnnounce(void);							///<시간이 지난 공지를 배열에서 삭제하고 현재 플레이 되어야 하는 공지를 설정한다.
	void		SetPlayAnnounce();								///<나가야 할 긴급공지를 설정한다.
	virtual void		BeginAnnounce(CAnnounce* pAnn);					///<공지 방송을 시작하도록 한다.
	virtual void		EndAnnounce();									///<공지 방송을 중지하도록 한다.

	void		StartAnnouncePlay(void);						///<긴급공지 기능을 시작하도록 한다.
	void		StopAnnouncePlay(void);							///<긴급공지 기능을 중지하도록 한다.
	void		SetTransParency(COLORREF crKey, int nVal, HWND hwnd=NULL);		///<화면의 투명도를 설정한다.
	bool		IsPlay(void);									///<Play 여부 반환
	void		DecreaseOffset(void);							///<Offset 값을 줄인다.
	void		GetOffset(int* pnOffset);						///<설정된 offset 값을 반환한다.

	static UINT	CaptionShiftThread(LPVOID param);
	static void CALLBACK MMtimeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2);

	int					m_offset;					///<문자영역의 offset
	int 				m_nTickerWidth;				///<공지 문자열의 길이
	int					m_nSpeed;					///<긴급공지 속도
	int					m_nShiftStep;				///<티커를 움직이는 픽셀
	//int					m_nTimerId;					///<Timer ID 
	bool				m_bExitThread;				///<Thread를 종료하는지 여부 플래그
	HANDLE				m_hevtShift;				///<Thread 종료 이벤트 핸들
	CWinThread*			m_pthreadShift;				///<Ticker를 움직이는 offest 조정 쓰레드
	CCriticalSection	m_csOffset;					///<Offeset값 동기화 객체
	CAnnounce*			m_pAnnPlay;					///<Play중인 Announce 객체

protected:

	DECLARE_MESSAGE_MAP()
	
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg virtual void OnPaint();
	afx_msg virtual int OnCreate(LPCREATESTRUCT lpCreateStruct) { return CDialog::OnCreate(lpCreateStruct); }

	virtual void MoveAnnounceWindow(int x, int y, int width, int height, CAnnounce* pAnn, int minimize=1);
	virtual void MoveWindowBySourceSize(int x, int y, int width, int height, CAnnounce* pAnn);
	virtual void MoveWindowByABSCoodinate(int x, int y, int width, int height, CAnnounce* pAnn, int minimize=1);
	virtual void MoveWindowFullScreen(int x, int y, int width, int height, CAnnounce* pAnn);
	virtual void SetTicker(CAnnounceDlg* p) { m_ticker = p;};

	ciBoolean	keyboardSimulator(const char* command);	

	//skpark 2013.11.28  new announce
	void StopOrRemove(const char* announceId, const char* contentsType);
};
