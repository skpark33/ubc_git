#pragma once


#include "ProgressCtrlEx.h"
//#include "DownloadResult.h"
//#include "Schedule.h"
#include "Downloader.h"


// CFtpDownloadDialog 대화 상자입니다.


class CFtpDownloadDialog : public CDialog
{
	DECLARE_DYNAMIC(CFtpDownloadDialog)

public:
	CFtpDownloadDialog(CWnd* pParent/* = NULL*/);   // 표준 생성자입니다.
	virtual ~CFtpDownloadDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FTP_DOWNLOAD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void PostNcDestroy();

	DECLARE_MESSAGE_MAP()

public:

	//LRESULT OnCompleteFtpConnection(WPARAM wParam, LPARAM lParam);
	//LRESULT OnCompleteFileSize(WPARAM wParam, LPARAM lParam);
	//LRESULT OnCompleteFtpDownload(WPARAM wParam, LPARAM lParam);
	//LRESULT OnCompleteInitDownloadResult(WPARAM wParam, LPARAM lParam);	///<Download result기록을 초기화가 완료됨.

	//void				GetConnection();	///<FTP Connection을 가져오는 threda를 시작한다.
	//void				GetFileSize();		///<다운로드 받을 파일들의 사이즈를 구하는 thread를 시작한다
	//void				GetDownload();		///<FTP에서 파일을 다운로드하는 thread를 시작한다

	bool				m_bShowWnd;			///<윈도우를 보여줄지 여부를 갖는 플래그
	CProgressCtrlEx		m_pbarTotal;

	//void				SetDownloadFileArray(CDownloadFileArray* pArr)	{ m_paryDownload = pArr; };
	void				StartDownload(bool bShowWindow=false);

protected:

	//CDownloadFileArray*	m_paryDownload;
	//CDownloadResult*	m_pDownResult;		///<Download result를 기록
	//ULONGLONG			m_nTotalSize;
	//int					m_nRetryCount;		///<재 시도 횟수

	CWnd*				m_pParentWnd;
	//CInternetSession	m_internetSession;
	//CFtpConnection*		m_pFtpConnection;

	//static UINT	GetFtpConnectionThread(LPVOID param);
	//static UINT	GetFileSizeThread(LPVOID param);
	//static UINT	FtpDownloadThread(LPVOID param);
	//static UINT ThreadInitDownloadResult(LPVOID param);		///<Download result기록을 초기화 하는 thread
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
/*
typedef struct {
	CWnd*				pParentWnd;
	CWnd*				pWnd;
	CInternetSession*	pInternetSession;
	CFtpConnection**	ppFtpConnection;
} GET_FTP_CONNECTION_PARAM;

typedef struct {
	CWnd*				pParentWnd;
	CWnd*				pWnd;
	CFtpConnection*		pFtpConnection;
	CDownloadFileArray*	pArray;
} GET_FILESIZE_THREAD_PARAM;

typedef struct {
	CWnd*				pParentWnd;
	CWnd*				pWnd;
	CProgressCtrlEx*	pbarTotal;
	CDownloadFileArray*	pArrayDownload;
	CFtpConnection*		pFtpConnection;
	ULONGLONG			nTotalSize;
} FTP_DOWNLOAD_PARAM;
*/

