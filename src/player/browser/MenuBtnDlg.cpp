// MenuBtnDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "MenuBtnDlg.h"

#ifndef LWA_ALPHA
#define LWA_ALPHA	0x00000002
#endif

#ifndef WS_EX_LAYERED
#define WS_EX_LAYERED	0x00080000
#endif


typedef BOOL (_stdcall *MENUBTN_TRANCPARENCY)(HWND,COLORREF,BYTE,DWORD); 
MENUBTN_TRANCPARENCY menubtn_SetLayeredWindowAttributes; 


// CMenuBtnDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMenuBtnDlg, CDialog)

CMenuBtnDlg::CMenuBtnDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMenuBtnDlg::IDD, pParent)
	, m_pdlgMenuBar(NULL)
{

}

CMenuBtnDlg::~CMenuBtnDlg()
{
}

void CMenuBtnDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MENUBTN_BTN, m_btnMenuBtn);
}


BEGIN_MESSAGE_MAP(CMenuBtnDlg, CDialog)
	ON_BN_CLICKED(IDC_MENUBTN_BTN, &CMenuBtnDlg::OnBnClickedMenubtnBtn)
END_MESSAGE_MAP()


// CMenuBtnDlg 메시지 처리기입니다.

void CMenuBtnDlg::OnBnClickedMenubtnBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_pdlgMenuBar)
	{
		if(!m_pdlgMenuBar->GetVisible())
		{
			m_pdlgMenuBar->ShowMenuBar();
			SetShowImage(false);
		}
		else
		{
			m_pdlgMenuBar->HideMenuBar();
			SetShowImage(true);
		}//if
	}//if

	//메인창이 입력 포커스를 갖도록 한다.
	//::AfxGetMainWnd()->SetFocus();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 메뉴바의 포인터를 설정 \n
/// @param (CMenuBarDlg*) pdlgMenuBar : (in) 메뉴바의 포인터
/////////////////////////////////////////////////////////////////////////////////
void CMenuBtnDlg::SetMenuBar(CMenuBarDlg* pdlgMenuBar)
{
	m_pdlgMenuBar = pdlgMenuBar;
}

BOOL CMenuBtnDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btnMenuBtn.MoveWindow(0, 0, 24, 18);
	m_btnMenuBtn.SetWindowText("Show/Hide Menu bar");
	m_btnMenuBtn.LoadBitmap(IDB_BTN_SHOW_MENUBAR, RGB(255, 255, 255));
	m_btnMenuBtn.SetToolTipText("Show/Hide Menu bar");

	SetTransParency(100);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Show/Hide 이미지를 설정 \n
/// @param (bool) bShow : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CMenuBtnDlg::SetShowImage(bool bShow)
{
	if(bShow)
	{
		m_btnMenuBtn.LoadBitmap(IDB_BTN_SHOW_MENUBAR, RGB(255, 255, 255));	
	}
	else
	{
		m_btnMenuBtn.LoadBitmap(IDB_BTN_HIDE_MENUBAR, RGB(255, 255, 255));
	}//if
	m_btnMenuBtn.Invalidate(TRUE);
	//SetWindowPos(&wndTop, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
}

BOOL CMenuBtnDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	//if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
	//{
	//	return TRUE;
	//}//if

	if(pMsg->message == WM_KEYDOWN)
	{
		::AfxGetMainWnd()->PreTranslateMessage(pMsg);
		return true;
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 화면의 투명도를 설정한다. \n
/// @param (int) nVal : (in) 화면의 투명도 값(0 : 완전 투명, 255 : 불투명)
/////////////////////////////////////////////////////////////////////////////////
void CMenuBtnDlg::SetTransParency(int nVal)
{
	if(nVal == 0)
	{
		//불투명하게 처리
		// Remove WS_EX_LAYERED from this window styles
		SetWindowLong(m_hWnd, GWL_EXSTYLE, GetWindowLong(m_hWnd, GWL_EXSTYLE) & ~WS_EX_LAYERED);

		// Ask the window and its children to repaint
		RedrawWindow(NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_FRAME | RDW_ALLCHILDREN);

		return;
	}//if

	__DEBUG__("SetTransParency Percent", nVal);
	//BYTE btTrans = 255 - ((float)nVal*2.55);
	BYTE btTrans = nVal;

	HMODULE hUser32 = ::GetModuleHandle( _T("user32.dll"));
	if(hUser32)
	{
		menubtn_SetLayeredWindowAttributes = (MENUBTN_TRANCPARENCY)::GetProcAddress( hUser32, "SetLayeredWindowAttributes"); 

		if(menubtn_SetLayeredWindowAttributes) 
		{ 
			LONG lOldStyle = GetWindowLong(m_hWnd, GWL_EXSTYLE); 
			SetWindowLong(m_hWnd, GWL_EXSTYLE, lOldStyle | WS_EX_LAYERED); 
			menubtn_SetLayeredWindowAttributes(m_hWnd, 0, btTrans, LWA_ALPHA);
			__DEBUG__("SetTransParency", btTrans);
		}//if
	}//if
}