// ThreadSave.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "ThreadSave.h"
#include "Host.h"

// CThreadSave

IMPLEMENT_DYNCREATE(CThreadSave, CWinThread)

CThreadSave::CThreadSave()
:	m_pParent(NULL)
,	m_bCancle(false)	
{
}

CThreadSave::~CThreadSave()
{
}

BOOL CThreadSave::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CThreadSave::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CThreadSave, CWinThread)
END_MESSAGE_MAP()


// CThreadSave 메시지 처리기입니다.

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// thread의 부가정보 설정 \n
/// @param (void*) pParent : (in) thread 소유자 포인터
/////////////////////////////////////////////////////////////////////////////////
void CThreadSave::SetThreadParam(void* pParent)
{
	m_pParent	= pParent;
}
int CThreadSave::Run()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CHost* pHost = (CHost*)m_pParent;

	if(!pHost || !pHost->GetSafeHwnd())
	{
		return 0;
	}//if

	CString str_template_list, strDefaultTemplate;

	// first, append default template
	CTemplate* pclsTemplate = pHost->GetdefaultTemplate();
	if(pclsTemplate)
	{
		strDefaultTemplate = pclsTemplate->GetTemplateId();
		str_template_list.Append(pclsTemplate->GetLoadID());
	}//if

	// etc...
	CTemplatePlay* pclsPlay = NULL;
	for(UINT unIdx=0; unIdx<pHost->GetCountOfTemplatePlayAry(); unIdx++)
	{
		if(m_bCancle)
		{
			__DEBUG__("ThreadSave Cnacled !!!", _NULL);
			return 0;
		}//if

		pclsPlay = pHost->GetAtTemplatePlay(unIdx);
		if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL)
		{
			continue;
		}//if

		pclsPlay->m_pclsTemplate->Save();

		if(strDefaultTemplate != pclsPlay->m_pclsTemplate->GetTemplateId())
		{
			if(str_template_list.GetLength() != 0)
				str_template_list.Append(",");

			str_template_list.Append(pclsPlay->m_pclsTemplate->GetLoadID());
		}//if
	}//for

	WritePrivateProfileString("Host", "TemplateList", str_template_list, GetSchedulePath());
	WritePrivateProfileString("Host", "templatePlayList", pHost->GetTemplatePlayList(), GetSchedulePath());

	CString strKey, strContentsList = "";
	CContents* pclsContents = NULL;
	int nCount = pHost->m_mapContents.GetCount();
	POSITION pos = pHost->m_mapContents.GetStartPosition();
	while(pos)
	{
		pHost->m_mapContents.GetNextAssoc(pos, strKey, (void*&)pclsContents);
		if(pclsContents)
		{
			pclsContents->Save(GetSchedulePath());
			strContentsList += strKey;
			strContentsList += ",";
		}//if
	}//while
	strContentsList.TrimRight(",");
	WritePrivateProfileString("Host", "ContentsList", strContentsList, GetSchedulePath());

	if(GetEdition() == ENTERPRISE_EDITION)
	{
		WritePrivateProfileString("Host", "lastUpdateId", pHost->m_strLastUpdateId.c_str(), GetSchedulePath());
		WritePrivateProfileString("Host", "lastUpdateTime", pHost->m_tmLastUpdate.toString(), GetSchedulePath());
	}//if

	pHost->PostMessage(WM_THREAD_SAVE, 0, 0);

	return 0;
	//return CWinThread::Run();
}
