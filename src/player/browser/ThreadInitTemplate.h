#pragma once

#include "Template.h"

// CThreadInitTemplate

class CThreadInitTemplate : public CWinThread
{
	DECLARE_DYNCREATE(CThreadInitTemplate)

public:
	CThreadInitTemplate();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CThreadInitTemplate();

	void*			m_pParent;							///<thread의 소유자 포인터
	void*			m_pBackWnd;							///<Backwnd 포인터
	bool			m_bCancle;							///<thread의 작업 취소 여부

	CTemplate*		AddTemplate(LPCSTR lpszTemplateID);	///Template 객체를 추가

public:
	virtual BOOL	InitInstance();
	virtual int		ExitInstance();
	void			SetThreadParam(void* pParent, void* pBackWnd);		///<thread의 부가정보 설정

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual int Run();
};


