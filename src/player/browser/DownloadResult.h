/************************************************************************************/
/*! @file DownloadResult.h
	@brief 컨텐츠 파일을 다운로드하는 로그를 기록하는 클래스
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/11/22\n
	
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2010/11/22:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

/*===================================================================================

[ROOT]
programId=
totalCount=
currentProgress=
successCount=
failCount=
localExistCount=
contentsList=
successList=
failList=
localExistList=
state=
startTime=(다운로드 시작 시간  _time64 타입)
endTime=(다운로드 완료된 시간 _time64 타입)


[Contents_Id]
state=0.1.2(초기상태, 다운로드시작, 다운로드완료)
contentsId=
contentsName=
contentsType=
filename=
location=
volume=
downloadVolume=
startTime=(다운로드 시작 시간  _time64 타입)
endTime=(다운로드 완료된 시간 _time64 타입)
reason=(실패인경우 : 서버에 없음, 연결끈김등의 오류....)
result=(0,1)

===================================================================================*/

#pragma once

#include "DownloadFile.h"

//에이전트에게 넘겨주는 정보(2000번 사용)
typedef struct
{
	char	programId[256];		//+host의 인자
	char	hostId[256];		//라이센스 키
	char	siteId[256];		//호스트의 site Id
	//char	contentsId[256];	//다운로드중인 컨텐츠의 Id(*이면 전체파일을 업데이트 하는것)
	char	progress[10];		//전체 다운로드 개수( 18/20 - 받은개수/전체개수 )
	int		state;				//현재 다운로드 상태
	int		browserId;			//Player의 Id(0, 1)
} ST_DownloadFileInfo;


//! 컨텐츠 파일을 다운로드하는 로그를 기록하는 클래스
/*!
	UBCDownloadResult.ini 파일에 기록 \n
*/
class CDownloadResult
{
public:
	CDownloadResult(CDownloadFileArray* paryFile);			///<생성자
	~CDownloadResult(void);									///<소멸자

	CString				m_strProgramId;						///<프로그램 Id
	CString				m_strContentsList;					///<다운로드하는 컨텐츠 Id 리스트("," 구분)
	CString				m_strSuccessList;					///<성공한(다운로드+로컬에존재) Id 리스트("," 구분)
	CString				m_strFailList;						///<다운로드를 실패한 Id 리스트("," 구분)
	CString				m_strLoclaExistList;				///<로컬디스크에 존재하여 다운로드 하지 않은 Id 리스트
	CString				m_strFileName;						///<저장하는 ini 파일의 이름(UTV_brwClient2 : UBCDownloadResult.ini, UTV_brwClient2_UBC1 : UBCDownloadResult1.ini)
	CString				m_strCurrentProgress;				///<현재 다운로드 진행율(다운로드 개수/전제 개수)

	int					m_nSuccessCount;					///<성공한(다운로드+로컬에존재) 수
	int					m_nFailCount;						///<다운로드 실패한 수
	int					m_nLocalExistCount;					///<로컬에 존재하는 수
	int					m_nTotalCount;						///<전체 다운로드해야하는 수

	int					m_nState;							///<현재 상태(0:초기상태, 1:다운로드시작, 2:다운로드완료)

	CTime				m_tmStartTime;						///<전체 다운로드 시작시간
	CTime				m_tmEndTime;						///<전체 다운로드 완료시간

	CDownloadFileArray* m_paryFile;							///<다운로드하려는 컨텐츠 파일들의 정보 배열

	ST_DownloadFileInfo	m_stDownloadFileInfo;				///<다운로드 정보를 알려주는 구조체

	void		InitResult(void);							///<다운로드 기록을 초기화 한다.
	//bool		WaitForLock(void);							///<ini 파일의 lock 상태가 unlock상태가 될때까지 대기한다.

public:
	void		BeginDownload(void);						///<다운로드 시작을 기록한다.
	void		EndDownload(void);							///<다운로드 종료를 기록한다.
	void		SendUpdateEvent(bool bWrite);				///<다운로드 상태를 알린다.
};
