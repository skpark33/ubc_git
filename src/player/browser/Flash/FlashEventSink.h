// FlashEventSink.h: interface for the CFlashEventSink class.
//
//////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



class CFlashEventSink : public _IShockwaveFlashEvents  
{
public:
	CFlashEventSink(void* pParent);
	virtual ~CFlashEventSink();
public:
	DWORD m_Cookie;
	int m_refCount;

	/***** IUnknown Methods *****/
    STDMETHODIMP QueryInterface(REFIID riid, void ** ppvObj);
    STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();

	/***** IDispatch Methods *****/
	STDMETHODIMP LoadTypeInfo(ITypeInfo** pptinfo, REFCLSID clsid, LCID lcid);
    STDMETHODIMP GetTypeInfoCount(UINT *iTInfo);
    STDMETHODIMP GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo);
    STDMETHODIMP GetIDsOfNames(REFIID riid, OLECHAR **rgszNames, UINT cNames,  LCID lcid, DISPID *rgDispId);
    STDMETHODIMP Invoke(DISPID dispIdMember, 
                    REFIID riid, 
                    LCID lcid,
                    WORD wFlags, DISPPARAMS* pDispParams,
                    VARIANT* pVarResult, EXCEPINFO* pExcepInfo,       
					UINT* puArgErr);

	//***** EApplication Methods ****
	
protected:
	void*			m_pParent;								///<�θ� â
	
};

