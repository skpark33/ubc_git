#pragma once

#include "shockwaveflash.h"
#include <afxmt.h>
#include <afxtempl.h>

// CFlashDlg 대화 상자입니다.

class CFlashDlg : public CDialog
{
public:
	static CFlashDlg* GetInstance(CWnd* pParentWnd, CString strFullpath);
	static void ClearInstance(CFlashDlg*& pInstance, bool bClear=false);
	static void ClearAllInstance();

protected:
	//static CFlashDlg* _instance;
	static CCriticalSection _cs;
	static CMapStringToPtr	_flashMap;

	DECLARE_DYNAMIC(CFlashDlg)

public:
	CFlashDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFlashDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FLASH_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);

public:
	DECLARE_EVENTSINK_MAP()
	void				FSCommandShockwaveflashOcx(LPCTSTR command, LPCTSTR args);
	void				FlashCallShockwaveflashOcx(LPCTSTR request);

public:
	bool				Play(CString strPath, CWnd* pParent);						///<지정된 경로의 플래쉬파일을 읽어 재생한다.
	void				Stop(CWnd* pParent=NULL);									///<플래쉬파일의 재생을 중지한다.
	void				CallFunc(CString strCmd);									///<플래쉬의 CallFunction을 호출한다.

private:
	CShockwaveFlash		m_ocxFlash;													///<플래쉬 컨트롤
	CString				m_strPath;													///<플래쉬 파일의 경로
	CString				m_strDummyFile;												///<더미 플래쉬 파일 경로

	bool				m_bOpened;

public:
	CWnd*				m_pParent;													///<부모윈도우 핸들

	virtual BOOL PreTranslateMessage(MSG* pMsg);

	LPCSTR	GetKey() { return m_strPath; };
};
