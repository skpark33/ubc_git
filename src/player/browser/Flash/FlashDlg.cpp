// FlashDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "FlashDlg.h"
#include "Schedule.h"


// CFlashDlg 대화 상자입니다.

//CFlashDlg* CFlashDlg::_instance = NULL;
CCriticalSection CFlashDlg::_cs;
CMapStringToPtr	CFlashDlg::_flashMap;

CFlashDlg* CFlashDlg::GetInstance(CWnd* pParentWnd, CString strFullpath)
{
	_cs.Lock();
	__DEBUG__("GetFlashContentsSharing", GetFlashContentsSharing());
	if( !GetFlashContentsSharing() )
	{
		// always return new-object
		CFlashDlg* dlg = new CFlashDlg;
		dlg->Create(IDD_FLASH_DLG, pParentWnd);

		CWnd* parent = dlg->GetParent();
		__DEBUG__("pParentWnd", (int)pParentWnd->GetSafeHwnd());
		__DEBUG__("parent", (int)parent->GetSafeHwnd());

		_cs.Unlock();
		return dlg;
	}

	strFullpath.MakeLower();
	CFlashDlg* instance = NULL;
	if( _flashMap.Lookup(strFullpath, (void*&)instance ) )
	{
		_cs.Unlock();
		__DEBUG__("find FlashDlg", _NULL);
		return instance;
	}
	__DEBUG__("create new FlashDlg", _NULL);
	instance = new CFlashDlg;
	instance->Create(IDD_FLASH_DLG, ::AfxGetMainWnd());
	instance->ShowWindow(SW_HIDE);
	_flashMap.SetAt(strFullpath, (void*)instance);

	_cs.Unlock();
	return instance;
}

void CFlashDlg::ClearInstance(CFlashDlg*& pInstance, bool bClear)
{
	if( pInstance == NULL ) return;

	_cs.Lock();

	if( !GetFlashContentsSharing() )
	{
		// delete right now
		pInstance->Stop();
		pInstance->DestroyWindow();
		delete pInstance;
		pInstance = NULL;

		_cs.Unlock();
		return;
	}

	if( bClear )
	{
		// delete & clear from map
		_flashMap.RemoveKey(pInstance->GetKey());

		pInstance->Stop();
		pInstance->DestroyWindow();
		delete pInstance;
		pInstance = NULL;
	}
	else
	{
		// move to main-wnd (flash keep alive on main-wnd)
		pInstance->SetParent(::AfxGetMainWnd());
	}

	_cs.Unlock();
}

void CFlashDlg::ClearAllInstance()
{
	POSITION pos = _flashMap.GetStartPosition();

	CString key;
	CFlashDlg* instance = NULL;
	while( pos != NULL )
	{
		_flashMap.GetNextAssoc( pos, key, (void*&)instance );

		instance->Stop();
		instance->DestroyWindow();
		delete instance;
		instance = NULL;
	}

	_flashMap.RemoveAll();
}

IMPLEMENT_DYNAMIC(CFlashDlg, CDialog)

CFlashDlg::CFlashDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFlashDlg::IDD, pParent)
	, m_strPath("")
	,m_pParent(NULL)
	,m_strDummyFile("")
	,m_bOpened(false)
{
	m_strDummyFile = ::GetAppPath();
	m_strDummyFile.Append("black.swf");
}

CFlashDlg::~CFlashDlg()
{
}

void CFlashDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SHOCKWAVEFLASH_OCX, m_ocxFlash);
}


BEGIN_MESSAGE_MAP(CFlashDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_MOVE()
END_MESSAGE_MAP()


// CFlashDlg 메시지 처리기입니다.
BEGIN_EVENTSINK_MAP(CFlashDlg, CDialog)
	ON_EVENT(CFlashDlg, IDC_SHOCKWAVEFLASH_OCX, 150, CFlashDlg::FSCommandShockwaveflashOcx, VTS_BSTR VTS_BSTR)
	ON_EVENT(CFlashDlg, IDC_SHOCKWAVEFLASH_OCX, 197, CFlashDlg::FlashCallShockwaveflashOcx, VTS_BSTR)
END_EVENTSINK_MAP()

void CFlashDlg::FSCommandShockwaveflashOcx(LPCTSTR command, LPCTSTR args)
{
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	CString strLog;
	strLog.Format("Recv fscommand [command : %s] [args : %s]", command, args);
	__DEBUG__(strLog, _NULL);

	if( m_pParent == NULL ) return;

	CString strCmd = command;
	CString strArg = args;
	if(strCmd.GetLength() == 1)
	{
		((CWizardSchedule*)m_pParent)->RecvFsCommand(strCmd, strArg);
	}
	else
	{
		((CFlashSchedule*)m_pParent)->RecvFsCommand(strCmd, strArg);
	}//if
}

void CFlashDlg::FlashCallShockwaveflashOcx(LPCTSTR request)
{
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	__DEBUG__("Flash call func", request);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 경로의 플래쉬파일을 읽어 재생한다. \n
/// @param (CString) strPath : (in) 플래쉬파일의 경로
/// @param (CWnd*) pParent : (in) 부모윈도우 주소
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
//bool CFlashDlg::Play(CString strPath, HWND hParent)
bool CFlashDlg::Play(CString strPath, CWnd* pParent)
{
	if(_access(strPath, 0) != 0)
	{
		__DEBUG__("Can't found flash file", strPath);
		return false;
	}//if

	if( !GetFlashContentsSharing() )
	{
		// always play new-object
		if( m_bOpened ) m_ocxFlash.Stop();

		m_strPath = strPath;
		__DEBUG__("New open flash file", m_strPath);
		m_ocxFlash.LoadMovie(0, m_strPath);
		//m_ocxFlash.put_Movie(m_strPath);
		//m_ocxFlash.Play();

		m_pParent = pParent;
		m_bOpened = true;
	}
	else
	{
		// not open  ->  play new-object
		if( !m_bOpened )
		{
			m_strPath = strPath;

			__DEBUG__("New open flash file", m_strPath);

			m_ocxFlash.LoadMovie(0, m_strPath);

			m_strPath.MakeLower();
			m_bOpened = true;
		}

		// different parent  ->  change parent & resize
		if( pParent != NULL && m_pParent != pParent )
		{
			__DEBUG__("Change Parent Window", _NULL);
			m_pParent = pParent;

			CRect parent_rect;
			pParent->GetClientRect(parent_rect);

			SetParent(pParent);
			MoveWindow(0, 0, parent_rect.Width(), parent_rect.Height(), FALSE);
			m_ocxFlash.MoveWindow(0, 0, parent_rect.Width(), parent_rect.Height());
		}
	}

	m_ocxFlash.put_Quality2("high");
	ShowWindow(SW_SHOW);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 플래쉬파일의 재생을 중지한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CFlashDlg::Stop(CWnd* pParent)
{
	if( !GetFlashContentsSharing() )
	{
		// always stop
		//ShowWindow(SW_HIDE);
		m_ocxFlash.Stop();
		//실제는 없는 파일의 완전한 경로를 입력하여 현재 Play중인 플래쉬를 중지 시킨다
		//반드시 완전한 경로를 주어야 한다.
		m_ocxFlash.LoadMovie(0, m_strDummyFile);

		//m_ocxFlash.StopPlay();
		//m_ocxFlash.GotoFrame(0);
		m_bOpened = false;
	}
	else
	{
		if( m_pParent == pParent )
		{
			// play-parent and stop-parent is same  =>  move to main-wnd
			//ShowWindow(SW_HIDE);
			//SetParent(::AfxGetMainWnd());
		}
		else
		{
			// play-parent and stop-parent is differet  ==>  do nothing
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
///플래쉬의 CallFunction을 호출한다. \n
/// @param (CString) strCmd : (in) 호출하는 플래쉬의 외부 함수와 인자를 나타내는 xml 문자
/////////////////////////////////////////////////////////////////////////////////
void CFlashDlg::CallFunc(CString strCmd)
{
	try
	{
		CString strRet = m_ocxFlash.CallFunction(strCmd);
		__DEBUG__("Flash call func returned", strRet);
	}
	catch(COleDispatchException* ex)
	{
		char szCause[255];
		memset(&szCause, 0x00, 255);
		ex->GetErrorMessage(szCause, 255);
		//__DEBUG__("Fail to call flash function, COleDispatchException ", szCause); 
		__DEBUG__("Fail to call flash function, COleDispatchException ", ex->m_strDescription); 
	}
	catch(CException& ex)
	{
		char szCause[255];
		memset(&szCause, 0x00, 255);
		ex.GetErrorMessage(szCause, 255);
		__DEBUG__("Fail to call flash function", szCause); 
	}
	catch(...)
	{
		__DEBUG__("Fail to call flash function, UnknownException ", strCmd); 
	
	}

}


void CFlashDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_ocxFlash.GetSafeHwnd())
	{
		CRect rtRect;
		GetClientRect(&rtRect);

		m_ocxFlash.MoveWindow(0, 0, rtRect.Width(), rtRect.Height());
	}//if
}


void CFlashDlg::OnMove(int x, int y)
{
	CDialog::OnMove(x, y);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


BOOL CFlashDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_LBUTTONDOWN)
	{
		__DEBUG__("CFlashDlg::L-CLICK", _NULL);
		CWnd* parent = GetParent();
		if(parent)
		{
			__DEBUG__("m_pParentWnd", (int)parent->GetSafeHwnd());
			return parent->PreTranslateMessage(pMsg);
		}//if
	}
	else if((pMsg->message == WM_KEYDOWN) /*&& (pMsg->wParam == VK_ESCAPE)*/)
	{
		/*
		if(m_pParentWnd)
		{
			return m_pParentWnd->PreTranslateMessage(pMsg);
		}//if
		*/
		AfxGetMainWnd()->PreTranslateMessage(pMsg);
		return TRUE;
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}
