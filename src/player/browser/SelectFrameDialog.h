#pragma once

#include "DisplayFrameDialog.h"

// CSelectFrameDialog 대화 상자입니다.

class CSelectFrameDialog : public CDialog
{
	DECLARE_DYNAMIC(CSelectFrameDialog)

public:
	CSelectFrameDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSelectFrameDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SELECT_FRAME_DIALOG };

	FRAME_SCHEDULE_INFO_LIST*	m_infoList;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CDisplayFrameDialogList		m_dlgList;
	// Modified by 정운형 2008-12-02 오후 7:38
	// 변경내역 :  dialog 위치 수정
	CRect						m_clsMainWndRect;		///<Main 창의 영역
	// Modified by 정운형 2008-12-02 오후 7:38
	// 변경내역 :  dialog 위치 수정

public:
	void	SetMainWndRect(CRect clsMainWndRect);		///<Main 창의 영역을 설정

	virtual BOOL OnInitDialog();
protected:
	//virtual void OnCancel();
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();
};
