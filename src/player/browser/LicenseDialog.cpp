// LicenseDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "LicenseDialog.h"

// Modified by 정운형 2008-11-03 오후 4:05
// 변경내역 :  LicenseDialog 폰트수정작업
//#define		ICON_WIDTH			60
#define		ICON_WIDTH			55
// Modified by 정운형 2008-11-03 오후 4:05
// 변경내역 :  LicenseDialog 폰트수정작업


#define		DISPLAY_ID		1024
#define		DISPLAY_TIME	100

#ifndef LWA_ALPHA
#define LWA_ALPHA	0x00000002
#endif

#ifndef WS_EX_LAYERED
#define WS_EX_LAYERED	0x00080000
#endif



// CLicenseDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLicenseDialog, CDialog)
CLicenseDialog::CLicenseDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenseDialog::IDD, pParent)
	, m_bLButton (false)
//	, m_bConnectNetwork (false)
//	, m_strTemplate ("Layout 00")
{
}

CLicenseDialog::~CLicenseDialog()
{
}

void CLicenseDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLicenseDialog, CDialog)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
	ON_WM_HELPINFO()
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CLicenseDialog 메시지 처리기입니다.

BOOL CLicenseDialog::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if( pMsg->message == WM_KEYDOWN)
	{
		::AfxGetMainWnd()->PreTranslateMessage(pMsg);
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CLicenseDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	HMODULE hUser32 = ::GetModuleHandle( _T("user32.dll"));
	if(hUser32)
	{
		g_SetLayeredWindowAttributes = (API_TYPE)::GetProcAddress( hUser32, "SetLayeredWindowAttributes"); 

		if( g_SetLayeredWindowAttributes) 
		{ 
			BYTE byTrans = 160; // 투명도 : 0 ~ 255 
			LONG lOldStyle;

			lOldStyle = GetWindowLong( m_hWnd, GWL_EXSTYLE); 
			SetWindowLong( m_hWnd, GWL_EXSTYLE, lOldStyle | WS_EX_LAYERED); 
			g_SetLayeredWindowAttributes( m_hWnd, 0, byTrans, LWA_ALPHA); 
		}
	}

	//Program(site_host)
	CRect rectProgram(5, 5, LICENSE_DIALOG_WIDTH, LICENSE_DIALOG_HEIGHT);
	m_staticTimeProgram.Create("", WS_CHILD | WS_VISIBLE, rectProgram, this, 0xfefd);
	
	m_staticTimeProgram.SetNumberOfLines(1);
	m_staticTimeProgram.SetXCharsPerLine(20);
	m_staticTimeProgram.SetSize(CMatrixStatic::LARGE);
	m_staticTimeProgram.SetDisplayColors(RGB(0, 0, 0), RGB(255, 255, 255), RGB(0, 0, 0));
	
	m_staticTimeProgram.AdjustClientXToSize(20);
	m_staticTimeProgram.AdjustClientYToSize(1);
	ciString strHost, strMac, strEdit, strVer;
	scratchUtil* aUtil = scratchUtil::getInstance();
	if(!aUtil->readAuthFile(strHost, strMac, strEdit)){
		strHost = "Unauthorized";
	}

	CString strProgram;
	strProgram.Format("KEY=%s", strHost.c_str());
	m_staticTimeProgram.SetText(strProgram);
	m_staticTimeProgram.SetAutoPadding(true);

//	CRect rectLayout(0, LICENSE_DIALOG_HEIGHT/3, LICENSE_DIALOG_WIDTH-ICON_WIDTH, LICENSE_DIALOG_HEIGHT/3*2);
//	m_staticTimeTemplate.Create("", WS_CHILD | WS_VISIBLE, rectLayout, this, 0xfefd);
	
//	m_staticTimeTemplate.SetNumberOfLines(1);
//	m_staticTimeTemplate.SetXCharsPerLine(10);
//	m_staticTimeTemplate.SetSize(CMatrixStatic::LARGE);
//	m_staticTimeTemplate.SetDisplayColors(RGB(0, 0, 0), RGB(255, 255, 0), RGB(0, 0, 0));
	
//	m_staticTimeTemplate.AdjustClientXToSize(10);
//	m_staticTimeTemplate.AdjustClientYToSize(1);
//	m_staticTimeTemplate.SetText(m_strTemplate);
//	m_staticTimeTemplate.SetAutoPadding(true);


//	CRect rectCurrent(0, LICENSE_DIALOG_HEIGHT/3*2, LICENSE_DIALOG_WIDTH/2, LICENSE_DIALOG_HEIGHT);
//	m_staticTimeCurrent.Create("", WS_CHILD | WS_VISIBLE, rectCurrent, this, 0xfeff);
	
//	m_staticTimeCurrent.SetNumberOfLines(1);
//	m_staticTimeCurrent.SetXCharsPerLine(10);
//	m_staticTimeCurrent.SetSize(CMatrixStatic::LARGE);
//	m_staticTimeCurrent.SetDisplayColors(RGB(0, 0, 0), RGB(0, 255, 0), RGB(0, 0, 0));
	
//	m_staticTimeCurrent.AdjustClientXToSize(10);
//	m_staticTimeCurrent.AdjustClientYToSize(1);
//	m_staticTimeCurrent.SetText(m_strTemplate);
//	m_staticTimeCurrent.SetAutoPadding(true);
	

//	CRect rectElapse(LICENSE_DIALOG_WIDTH/2, LICENSE_DIALOG_HEIGHT/3*2, LICENSE_DIALOG_WIDTH/2, LICENSE_DIALOG_HEIGHT);
//	m_staticTimeElapse.Create("", WS_CHILD | WS_VISIBLE, rectElapse, this, 0xfefe);
	
//	m_staticTimeElapse.SetNumberOfLines(1);
//	m_staticTimeElapse.SetXCharsPerLine(10);
//	m_staticTimeElapse.SetSize(CMatrixStatic::LARGE);
//	m_staticTimeElapse.SetDisplayColors(RGB(0, 0, 0), RGB(255, 0, 0), RGB(0, 0, 0));
	
//	m_staticTimeElapse.AdjustClientXToSize(10);
//	m_staticTimeElapse.AdjustClientYToSize(1);
//	m_staticTimeElapse.SetText(m_strTemplate);
//	m_staticTimeElapse.SetAutoPadding(true);

	//SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

//	m_timeStart = CTime::GetCurrentTime();
//	SetTimer(DISPLAY_ID, DISPLAY_TIME, NULL);


//	CBitmap bmp;
//	BOOL ret = bmp.LoadBitmap(IDB_NETWORK_STATUS);

//	m_imageList.Create(48, 48, ILC_COLOR32 | ILC_MASK, 0, 2);
//	m_imageList.Add(&bmp, RGB(232,168,192));


	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CLicenseDialog::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnLButtonDown(nFlags, point);

	SetCapture();
	m_bLButton = true;
	m_pointDelta = point;
}

void CLicenseDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);

	if(m_bLButton)
	{
		::ClientToScreen(m_hWnd, &point);
		MoveWindow(
			point.x - m_pointDelta.x,
			point.y - m_pointDelta.y,
			LICENSE_DIALOG_WIDTH, LICENSE_DIALOG_HEIGHT
		);
	}
}

void CLicenseDialog::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnLButtonUp(nFlags, point);

	::ReleaseCapture();

	m_bLButton = false;
}
/*
void CLicenseDialog::OnTimer(UINT nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(DISPLAY_ID == nIDEvent)
	{
//		CTime current_time = CTime::GetCurrentTime();
//		CTimeSpan span = current_time - m_timeStart;

//		CString strTime, strElapse;
//		strTime.Format(_T("%02d;%02d;%02d"), current_time.GetHour(), current_time.GetMinute(), current_time.GetSecond());
//		m_staticTimeCurrent.SetText(strTime);
//		strElapse.Format(_T("+%02d;%02d;%02d"), span.GetHours(), span.GetMinutes(), span.GetSeconds());
//		m_staticTimeElapse.SetText(strElapse);
	}
}
*/
BOOL CLicenseDialog::OnHelpInfo(HELPINFO* pHelpInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return TRUE;
//	return CDialog::OnHelpInfo(pHelpInfo);
}

void CLicenseDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	dc.FillSolidRect(0, 0, LICENSE_DIALOG_WIDTH, LICENSE_DIALOG_HEIGHT, RGB(0,0,0));
	
//	if(m_bConnectNetwork)
//		m_imageList.Draw(&dc, 0, CPoint(LICENSE_DIALOG_WIDTH-ICON_WIDTH, LICENSE_DIALOG_HEIGHT/3*1), ILD_TRANSPARENT);
//	else
//		m_imageList.Draw(&dc, 1, CPoint(LICENSE_DIALOG_WIDTH-ICON_WIDTH, LICENSE_DIALOG_HEIGHT/3*1), ILD_TRANSPARENT);
		
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 방송중인 템플릿 ID를 설정한다 \n
/// @param (LPCSTR) lpszTemplateId : (in) 설명
/////////////////////////////////////////////////////////////////////////////////
//void CLicenseDialog::SetTemplateId(LPCSTR lpszTemplateId)
//{
//	m_timeStart = CTime::GetCurrentTime();
//
//	m_strTemplate.Format("Layout %s", lpszTemplateId);
//	m_staticTimeTemplate.SetText(m_strTemplate);
//	
//	Invalidate(FALSE);
//}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 템플릿 ID를 반환한다. \n
/// @return <형: CString> \n
///			<CString: 설정된 템플릿 ID> \n
/////////////////////////////////////////////////////////////////////////////////
//CString CLicenseDialog::GetTemplateId()
//{
//	return m_strTemplate;
//}
