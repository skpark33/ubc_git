#pragma once

#include "Schedule.h"
#include "TemplatePlay.h"

// CFrame
class CTemplate;

typedef CArray<CFrame*, CFrame*>	CFrameArray;
typedef CMapStringToPtr				CFrameMap;

class CFrame : public CWnd
{
	DECLARE_DYNAMIC(CFrame)

public:
	static	CFrame*	GetFrameObject(CTemplate* pParent, LPCSTR lpszID, CString& strErrorMessage);
	static	CFrame*	CreatePIPFrameObject(CTemplate* pParent, CFrame* pFrame, CString& strErrorMessage);

	CFrame(CTemplate* pParentWnd, LPCSTR lpszID);
	virtual ~CFrame();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnPlayNextSchedule(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDebugString(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDebugGrid(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnErrorMessage(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHideContents(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSoundVolumeMute(WPARAM wParam, LPARAM lParam);

protected:
	DECLARE_MESSAGE_MAP()

	//attribute
	ciString	m_templateId;
	ciString	m_frameId;
	ciShort		m_grade;
	ciShort		m_width;
	ciShort		m_height;
	ciShort		m_x;
	ciShort		m_y;
	ciBoolean	m_isPIP;
	ciString	m_borderStyle;
	ciShort		m_borderThickness;
	ciString	m_borderColor;
	ciShort		m_cornerRadius;

	COLORREF	m_rgbBorderColor;

	//
	static UINT	m_nFrameID;				///<윈도우 생성시에 사용하는 윈도우 고유 ID

	bool		m_bCurrentPlay;

	int			m_nCurrentScheduleIndex;
	int			m_nCurrentDefaultScheduleIndex;
	CSchedule*	m_pCurrentSchedule;

	void		DrawSquareFrame(CDC* pDC);
	void		DrawRoundFrame(CDC* pDC);

	void		PlayNextSchedule();
	bool		GetNextSchedule(int& index);
	bool		GetNextDefaultSchedule(int& index);
	bool		GetNextChildSchedule(int& index);

	bool		SortDefaultSchedule();

	CScheduleArray	m_listSchedule;
	CScheduleArray	m_listDefaultSchedule;
	CScheduleMap	m_mapSchedule;

	CTemplate*	m_pParentWnd;

	CFrame*		m_pPIPParentWnd;
	CFrameArray	m_listPIPFrame;
	void		SetClipRgn(int x, int y, int width, int height);
	void		ResetClipRgn();

	CString		m_strErrorMessage;
	CString		m_strLoadID;

	int			m_nPlayCount;
	bool		m_bPause;					///<Scheduel pause
	bool		m_boolScheduleCheckFlag;
	bool		m_bIsHDFrame;				///<HD 프레임 여부

public:

	static	CFrame*	m_pPhoneFrame;
	static	CCriticalSection m_csFramePhone;

	LPCSTR	GetFrameId() { return m_frameId.c_str(); };
	int 	GetGrade() { return m_grade; };
	bool	IsCurrentPlay() { return m_bCurrentPlay; };
	bool	IsPIP() { return m_isPIP; };
	bool	LoadSchedule(bool bDefaultSchedule);
	bool	CheckSchedule();
	bool	OpenFile();
	bool	IsBetween(bool bDefaultSchedule);

	int					m_nIndexPlayOrder;								///<m_pclsPalyRule의 m_aryPlayOrder 배열의 몇번째 schedule이 재생중인지 나타내는 인덱스
	CTemplatePlay*		m_pclsPalyRule;									///<Tmplate play rule을 갖는 클래스 포인터
	void	Play(CTemplatePlay* pclsPlayRule = NULL);					///<Tmplate play rule을 설정하여 play하도록 하는 함수
	int		FindDefaultScheduleIndexByPlayOrder(CString strPlayOrder);	///<Schedule의 play order로 m_listDefaultSchedule의 해당되는 인덱스를 찾아주는 함수
	void	Stop();
	void	Pause(void);												///<Scheduel pause

	bool	FrameInFrame(CFrame* frame);
	void	SetPIPParentWnd(CFrame* pWnd) { m_pPIPParentWnd = pWnd; };

	LPCSTR	GetLoadID() { return m_strLoadID; };
	void	GetScheduleList(CScheduleArray&	listSchedule);			///<모든 스케줄의 리스트를 구한다.

	CString	m_debugString;
	virtual CString	ToString();

	bool	GetFrameScheduleInfo(FRAME_SCHEDULE_INFO& info);

	void	SetPlayCount(int count) { m_nPlayCount = count; };
	void	ResetPlayCount();
	void	SetScheduleCheck(bool bCheck) { m_boolScheduleCheckFlag = bCheck; };
	void	AddPIPFrame(CFrame* pFrame);								///<PIP프레임의 포인터를 멤버배열에 추가한다.
	void	ClearPIPFrame(void);										///<PIP프레임의 멤버배열을 정리한다.
	void	GetFrameRgn(int& nX, int& nY, int& nWidth, int& nHeight);	///<Frame 윈도우의 영역을 얻어온다.
	void	SetPIPClipRgn(void);										///<원래 Frame의 영역에서 PIP윈도우 영역을 잘라낸다.
	bool	IsHDFrame(void)		{ return m_bIsHDFrame; };				///<HD Frame인지 여부를 반환한다.
	void	PlayClickSchedule(CString strScheduleId);					///<부모 스케줄의 Click 이벤트를 받아 설정된 자식 스케줄을 재생한다.
	bool	FindChildSchedule(CString strScheduleId, int& nIndex);		///<지정된 Id를 부모스케줄로 갖는 스케줄의 인덱스를 찾는다.

	int		GetDefaultScheduleCount() { return m_listDefaultSchedule.GetSize(); }   // skpark 2012.12.18 Frame 에 들어있는 스케쥴갯수

	// <!-- 골프장전용 (2016.08.08)
	bool	IsRemainPlayingDefaultSchedule() { return (m_nCurrentDefaultScheduleIndex!=-1) && (m_nCurrentDefaultScheduleIndex<m_listDefaultSchedule.GetCount()-1) && (m_listDefaultSchedule.GetCount()>1); };
	// -->

	CSchedule*	_sch;
};




