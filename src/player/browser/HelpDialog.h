#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "ximage.h"
#include "ReposControl.h"

// CHelpDialog 대화 상자입니다.

class CHelpDialog : public CDialog
{
	DECLARE_DYNAMIC(CHelpDialog)

public:
	CHelpDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHelpDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HELP_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	// Modified by 정운형 2008-11-17 오후 3:39
	// 변경내역 :  Help dialog control위치 버그수정
	/*
	CRect	m_rectDialog;
	CRect	m_rectGroupHotkey;
	CRect	m_rectGroupParameter;
	CRect	m_rectListctrlHotkey;
	CRect	m_rectListctrlParameter;
	CRect	m_rectOK;
	*/
	// Modified by 정운형 2008-11-17 오후 3:39
	// 변경내역 :  Help dialog control위치 버그수정

	CReposControl	m_reposControl;

public:
	
	CStatic		m_groupHotkey;
	CStatic		m_groupParameter;
	CListCtrl	m_listctrlHotkey;
	CListCtrl	m_listctrlParameter;

	CStatic		m_staticEdition;
	CStatic		m_staticVersion;
	CStatic		m_staticName;
	CStatic		m_staticKey;
	CEdit		m_edtEdition;
	CEdit		m_edtVersion;
	CEdit		m_edtName;
	CEdit		m_edtKey;

	CButton		m_btnOK;

	CString		m_strEdition;				///<프로그램의 제품구분(STD, PLS, ENT)
	CString		m_strVersion;				///<프로그램의 버전
	CString		m_strComputerName;			///<프로그램이 구동하는 컴퓨터의 이름
	CString		m_strRegstrationKey;		///<인증키

	CxImage		m_imageEdition;


	//void	ReposControl();
	void	SetAppInof(CString strEdition, CString strVersion,
								CString strName, CString strKey);	///<프로그램의 버전등의 정보를 설정한다.

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnNcMouseMove(UINT nHitTest, CPoint point);	
	afx_msg void OnPaint();
};
