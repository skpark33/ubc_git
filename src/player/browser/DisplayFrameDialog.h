#pragma once
#include "afxwin.h"


class CSelectFrameDialog;


//
class CDisplayFrameWnd : public CWnd
{
	DECLARE_DYNAMIC(CDisplayFrameWnd)
public:
	CDisplayFrameWnd();
	virtual ~CDisplayFrameWnd();
protected:
	DECLARE_MESSAGE_MAP()
	CFont		m_font;
public:
	FRAME_SCHEDULE_INFO		m_info;
	CSelectFrameDialog*		m_wndParent;
	afx_msg void OnPaint();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};



// CDisplayFrameDialog 대화 상자입니다.


class CDisplayFrameDialog : public CDialog
{
	DECLARE_DYNAMIC(CDisplayFrameDialog)

public:
	CDisplayFrameDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDisplayFrameDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DISPLAY_FRAME_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:

	BYTE byTrans; // 투명도 : 0 ~ 255 
	LONG lOldStyle;
	typedef BOOL (_stdcall *API_TYPE)(HWND,COLORREF,BYTE,DWORD); 
	API_TYPE g_SetLayeredWindowAttributes ;

	virtual BOOL OnInitDialog();

	CStatic m_staticNumber;
	CRgn	m_rgn;

	afx_msg void OnSize(UINT nType, int cx, int cy);

	CDisplayFrameWnd		m_wndBack;

	FRAME_SCHEDULE_INFO		m_info;
	CSelectFrameDialog*		m_wndParent;
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};

typedef		CArray<CDisplayFrameDialog*, CDisplayFrameDialog*>		CDisplayFrameDialogList;