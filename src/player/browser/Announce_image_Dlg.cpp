// AnnounceDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "Announce_image_Dlg.h"
#include "MemDC.h"
#include "Host.h"

// CAnnounce_image_Dlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAnnounce_image_Dlg, CAnnounceDlg)

CAnnounce_image_Dlg::CAnnounce_image_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent /*=NULL*/)
	: CAnnounceDlg(nPosX,nPosY,nCx,nCy,pParent)
{
	__DEBUG__("CAnnounce_image_Dlg", _NULL);
	m_bAnimatedGif = false;
	m_bOpen = false;
	m_bFileNotExist = false;
}

CAnnounce_image_Dlg::~CAnnounce_image_Dlg()
{
}

BEGIN_MESSAGE_MAP(CAnnounce_image_Dlg, CAnnounceDlg)
	ON_WM_CREATE()
	ON_WM_PAINT()
END_MESSAGE_MAP()

int CAnnounce_image_Dlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG__("CAnnounce_image_Dlg::OnCreate()", _NULL);
	if (CAnnounceDlg::OnCreate(lpCreateStruct) == -1){
		return -1;
	}

	CRect rect;
	GetClientRect(rect);

	__DEBUG__("\t\t\tCreate AnimatedGIF", _NULL);
	m_bgGIF.Create("Animated GIF", WS_CHILD, rect, this);


	return 0;
}
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 시작하도록 한다. \n
/// @param (CAnnounce*) pAnn : (in) 방송을 해야하는 공지 객체 주소값
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_image_Dlg::BeginAnnounce(CAnnounce* pAnn)
{
	if(!m_pAnnPlay)
	{
		m_pAnnPlay = pAnn;
	}
	else if((*m_pAnnPlay == *pAnn) && m_pAnnPlay->m_bPlaying)
	{
		//if(!(((CHost*)m_pParentWnd)->IsWindowVisible())){
			__DEBUG__("Same Annnounce Shows", pAnn->m_strAnnounceId.c_str());
			ShowWindow(SW_SHOW);
			((CHost*)m_pParentWnd)->SetFocus();
		//}
	}
	else
	{
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = pAnn;
	}//if

	char	szBuf[_MAX_PATH] = { 0x00 };
	::GetModuleFileName(NULL, szBuf, _MAX_PATH);

	char drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
	_splitpath(szBuf, drive, path, filename, ext);

	m_strMediaFullPath.Format("%s\\SQISOFT\\Contents\\Enc\\announce\\%s",drive, m_pAnnPlay->m_strBgFile.c_str());


	__DEBUG__("BeginAnnounce",	m_strMediaFullPath);
	__DEBUG__("BeginAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());

	CString strLog = m_pAnnPlay->m_strBgFile.c_str();
	WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(),m_pAnnPlay->m_strTitle.c_str(), "TRY", m_pAnnPlay->GetRunningTime(), strLog);

	MoveAnnounceWindow(m_nParentPosX, m_nParentPosY, m_nParentCx, m_nParentCy, m_pAnnPlay);

	OpenFile(0);

	m_pAnnPlay->m_bPlaying = true;
	WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "START", m_pAnnPlay->GetRunningTime(), strLog);

	if(m_pthreadShift)
	{
		ShowWindow(SW_SHOW);
		((CHost*)m_pParentWnd)->SetFocus();
	}
	else
	{
		m_pthreadShift = ::AfxBeginThread(CAnnounce_image_Dlg::PlayImageThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pthreadShift->m_bAutoDelete = TRUE;
		m_bExitThread = false;
		m_hevtShift = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pthreadShift->ResumeThread();
		//EndAnnounce();
	}//if
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 중지하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_image_Dlg::EndAnnounce()
{
	if(m_pAnnPlay)
	{
		__DEBUG__("EndAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());

		CString strLog = m_pAnnPlay->m_strBgFile.c_str();

		if(m_pthreadShift)
		{
			WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "SUCCESS", m_pAnnPlay->GetRunningTime(), strLog);
		}
		else
		{
			WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "FAIL", m_pAnnPlay->GetRunningTime(), strLog);
		}//if

		CloseFile();

		m_bExitThread = true;
		SetEvent(m_hevtShift);
		CloseHandle(m_hevtShift);
		m_hevtShift = NULL;
		m_pthreadShift = NULL;

		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "END", m_pAnnPlay->GetRunningTime(), strLog);

		ShowWindow(SW_HIDE);
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = NULL;
	}//if
}

void CAnnounce_image_Dlg::OnPaint()
{
	__DEBUG__("OnPaint",NULL);
	if(!m_bOpen) return;

	__DEBUG__("OnPaint Start",NULL);
	CPaintDC dc(this); // device context for painting
	CMemDC memDC(&dc);

	CRect rtClient;
	GetClientRect(rtClient);

	rtClient.bottom = this->m_nCy;
	rtClient.right = this->m_nCx;

	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}
	else if(m_bAnimatedGif)
	{
		SIZE size = m_bgGIF.GetSize();

		memDC.FillSolidRect(size.cx, 0, rtClient.Width(), size.cy, RGB(255,255,255));
		memDC.FillSolidRect(0, size.cy, rtClient.Width(), rtClient.Height(), RGB(255,255,255));
	}
	else
	{
		if(m_bgImage.IsValid())
		{
			m_bgImage.Draw2(memDC.GetSafeHdc(), rtClient);
		}//if
	}//if
	
	BringWindowToTop();
	SetForegroundWindow();
	SetFocus();
	if(m_ticker && 	m_ticker->IsPlay()){
		m_ticker->BringWindowToTop();
		m_ticker->SetForegroundWindow();
		m_ticker->SetFocus();
	}

	__DEBUG__("OnPaint End",NULL);
}

UINT CAnnounce_image_Dlg::PlayImageThread(LPVOID pParam)
{
	DWORD dwEventRet = 0;
	CAnnounce_image_Dlg* pParent = (CAnnounce_image_Dlg*)pParam;

	pParent->Play();

	while(!pParent->m_bExitThread)
	{
		dwEventRet = WaitForSingleObject(pParent->m_hevtShift, NULL);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			//CloseHandle(pParent->m_hevtShift);
			__DEBUG__("Event set : PlayImageThread", _NULL);
			break;
		}//if
		__DEBUG__("Event not set : PlayImageThread", _NULL);
		break;
	}//while
	//CloseHandle(pParent->m_hevtShift);
	__DEBUG__("Exit PlayImageThread", _NULL);
	return 1;
}

bool CAnnounce_image_Dlg::Play()
{
	__DEBUG__("Play start", _NULL);
	if(!m_bOpen)
	{
		__DEBUG__("File not opened", _NULL);
		return false;
	}
	
	if(m_bAnimatedGif)
	{
		__DEBUG__("\t\t\tPlay Animated GIF", _NULL);
		m_bgGIF.Draw();
	}
	__DEBUG__("Play end", _NULL);
	return true;
}

bool CAnnounce_image_Dlg::Stop()
{
	if(m_bAnimatedGif)
	{
		__DEBUG__("\t\t\tStop Animated GIF", _NULL);
		m_bgGIF.Stop();
	}//if
	return true;
}


bool CAnnounce_image_Dlg::OpenFile(int nGrade)
{
	if(m_bOpen) 
	{ 
		if(m_pAnnPlay->m_bPlaying)
		{
			__DEBUG__("\t\t\tAlready Open", _NULL);
			return true;
		}
		CloseFile();
	}

	if(!OpenGIF())
	{
		__DEBUG__("\t\t\tImage Mode", _NULL);
		m_bAnimatedGif = false;
		if(!OpenImage())
		{
			SetNoContentsFile();
			return true;
		}//if
	}//if

	m_bOpen = true;
	return true;
}

bool CAnnounce_image_Dlg::OpenImage()
{

	CString strExt = FindExtension(m_strMediaFullPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
	{
		__DEBUG__("image file load fail", m_strMediaFullPath);
		return false;
	}//if
	__DEBUG__("image file load", m_strMediaFullPath);

	int nX, nY, nWidth, nHeight;
	//m_pParentWnd->GetFrameRgn(nX, nY, nWidth, nHeight);
	CRect rtClient;
	GetClientRect(&rtClient);
	nWidth = rtClient.Width();
	nHeight = rtClient.Height();
	DWORD dwWidth = m_bgImage.GetWidth();
	DWORD dwHeight = m_bgImage.GetHeight();
	if(dwWidth != nWidth || dwHeight != nHeight)
	{
		if(!m_bgImage.Resample(nWidth, nHeight, 3))	//default option(bilinear interpolation)
		{
			__DEBUG__("image resample fail", m_strMediaFullPath);
		}else{
			__DEBUG__("image resample width=", nWidth);
			__DEBUG__("image resample height=", nHeight);
		}

	}//if

	this->RedrawWindow(); //skpark
	return true;
}

bool CAnnounce_image_Dlg::OpenGIF()
{
	CString strExt = FindExtension(m_strMediaFullPath);
	if(strExt.CompareNoCase("gif") != 0)
	{
		return false;
	}//if
	
	if(!m_bgGIF.Load(m_strMediaFullPath))
	{
		if(m_bgGIF.GetSafeHwnd())
		{
			m_bgGIF.ShowWindow(SW_HIDE);
			m_bgGIF.Stop();
			m_bgGIF.UnLoad();
		}//if
		__DEBUG__("Gif image load fail", _NULL);
		return false;
	}//if
		
	if(m_bgGIF.IsAnimatedGIF())
	{
		__DEBUG__("\t\t\tAnimated GIF Mode", _NULL);
		m_bAnimatedGif = true;
		m_bgGIF.ShowWindow(SW_SHOW);
		return true;
	}//if
	
	if(m_bgGIF.GetSafeHwnd())
	{
		m_bgGIF.ShowWindow(SW_HIDE);
		m_bgGIF.Stop();
		m_bgGIF.UnLoad();
	}//if
	
	return false;
}


bool CAnnounce_image_Dlg::CloseFile()
{
	__DEBUG__("CloseFile()",NULL);
	
	Stop();

	if(m_bOpen)
	{
		m_bOpen = false;

		if(m_bAnimatedGif)
		{
			__DEBUG__("m_bgGIF.UnLoad()",NULL);
			m_bgGIF.UnLoad();
		}
		else
		{
			if(m_bgImage.IsValid())
			{
				__DEBUG__("m_bgImage.Destroy()",NULL);
				m_bgImage.Clear();
				m_bgImage.Destroy();
				if(m_bgImage.IsValid()){
					__DEBUG__("m_bgImage. still valid ???",NULL);
					//m_bgImage.Resample(0,0,3);
				}else{
					__DEBUG__("m_bgImage. not valid",NULL);
				}
			}//if
		}//if
		m_bAnimatedGif = false;
	}//if
	return true;
}


void CAnnounce_image_Dlg::SetNoContentsFile()
{
	__DEBUG__("Contents file not exist", m_strMediaFullPath);

	//컨텐츠 파일이 없는 경우 액박 표시해 준다.
	HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_XBOX));
	m_bgImage.CreateFromHBITMAP(hBitmap);
	m_bOpen = true;
	m_bFileNotExist = true;

}
