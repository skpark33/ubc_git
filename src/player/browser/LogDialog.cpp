// LogDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "LogDialog.h"


// CLogDialog 대화 상자입니다.


#define		LOG_CLEAR_ID			6000	// id
#define		LOG_CLEAR_TIME			((60*1000) * 15)	// 15 min

#define		TREE_WIDTH				200
#define		LOG_HEIGHT				0


CLogDialog*	CLogDialog::_instance = NULL;


CLogDialog*	CLogDialog::getInstance(CWnd* pWnd)
{
	if(_instance == NULL)
	{
		_instance = new CLogDialog(pWnd);
		_instance->Create(IDD_LOG_DIALOG, pWnd);
		_instance->ShowWindow(SW_HIDE);
	}

	return _instance;
}

void CLogDialog::clearInstance()
{
	if(_instance)
	{
		_instance->DestroyWindow();
		delete _instance;
		_instance = NULL;
	}
}

IMPLEMENT_DYNAMIC(CLogDialog, CDialog)

CLogDialog::CLogDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLogDialog::IDD, pParent)
	, m_hCurrentItem (NULL)
{

}

CLogDialog::~CLogDialog()
{
}

void CLogDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE, m_treectrl);
	DDX_Control(pDX, IDC_EDIT_LOG, m_editLOG);
	DDX_Control(pDX, IDC_EDIT_ATTRIBUTE, m_editAttribute);
	DDX_Control(pDX, IDC_GROUP_PROPERTY, m_propertyCtrl);
}


BEGIN_MESSAGE_MAP(CLogDialog, CDialog)
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE, &CLogDialog::OnTvnSelchangedTree)
	ON_NOTIFY(NM_RCLICK, IDC_TREE, &CLogDialog::OnNMRclickTree)
	ON_MESSAGE(WM_DEBUG_STRING, OnDebugString)
	ON_COMMAND(ID_DEFAULTTEMPLATE_CHANGE, &CLogDialog::OnDefaulttemplateChange)
	ON_COMMAND(ID_PLAY_NEXT_SCHEDULE, &CLogDialog::OnPlayNextSchedule)
	ON_WM_HELPINFO()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CLogDialog 메시지 처리기입니다.

BOOL CLogDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_font.CreatePointFont(9*10, "Tahoma");
	m_treectrl.SetFont(&m_font);
	m_editLOG.SetFont(&m_font);
	m_editAttribute.SetFont(&m_font);

	//
	if( m_imageList.Create(16, 16, ILC_MASK | ILC_COLOR24, 0, 1) )
	{
		CBitmap bmp;
		bmp.LoadBitmap(IDB_TREE);
		m_imageList.Add(&bmp, RGB(1,1,1) );
	}

	m_treectrl.SetImageList(&m_imageList, TVSIL_NORMAL);

	//
	m_editLOG.SetLimitText(UINT_MAX);

	//
	ResizeControl();

	//
	SetTimer(LOG_CLEAR_ID, LOG_CLEAR_TIME, NULL);


	m_editAttribute.ShowWindow(SW_HIDE);
	m_propertyCtrl.ShowWindow(SW_SHOW);


	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CLogDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CDialog::OnOK();
	ShowWindow(SW_HIDE);
}

void CLogDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CDialog::OnCancel();
	ShowWindow(SW_HIDE);
}

void CLogDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == LOG_CLEAR_ID)
	{
		m_csLogDlg.Lock();

		m_editLOG.SetWindowText(_T("--Clear Log--\r\n"));

		m_csLogDlg.Unlock();
	}
}

void CLogDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	ResizeControl();
}

void CLogDialog::OnTvnSelchangedTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pNMTreeView->itemNew.hItem != NULL)
	{
		m_hCurrentItem = pNMTreeView->itemNew.hItem;
		CWnd* wnd = (CWnd*)m_treectrl.GetItemData(pNMTreeView->itemNew.hItem);

		if(wnd->GetSafeHwnd())
		{
			wnd->SendMessage(WM_DEBUG_GRID, (WPARAM)&m_propertyCtrl );
		}//if

//		LPCTSTR attr = (LPCTSTR)wnd->SendMessage(WM_DEBUG_STRING);
//		if(attr)
//			SetAttribute(attr);
//		else
//			SetAttribute("");
	}
}

void CLogDialog::OnNMRclickTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	//
	UINT flags;
	CPoint pt;
	GetCursorPos(&pt);
	m_treectrl.ScreenToClient(&pt);
	HTREEITEM nitem = m_treectrl.HitTest(pt,&flags);
	m_treectrl.SelectItem(nitem);

	//
	CMenu menu;
	menu.LoadMenu(IDR_POPUP_MENU);

	if(m_hCurrentItem != NULL)
	{
		CMenu* sub_menu = NULL;

		switch(GetDepth(m_hCurrentItem))
		{
		case 1:
			sub_menu = menu.GetSubMenu(0);
			break;
		case 2:
			sub_menu = menu.GetSubMenu(1);
			break;
		}

		if(sub_menu != NULL)
		{
			CPoint point;
			GetCursorPos(&point);

			sub_menu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
		}
	}
}

void CLogDialog::ResizeControl()
{
	CRect rect;
	GetClientRect(rect);

	if(m_treectrl.GetSafeHwnd())
	{
		CRect r = rect;
		r.right = r.left + TREE_WIDTH;

		m_treectrl.MoveWindow(r);
	}

	if(m_editLOG.GetSafeHwnd())
	{
		CRect r = rect;
		r.left += TREE_WIDTH + 2;
		r.bottom = r.top + LOG_HEIGHT;

		m_editLOG.MoveWindow(r);
	}

	if(m_editAttribute.GetSafeHwnd())
	{
		CRect r = rect;
		r.left += TREE_WIDTH + 2;
		r.top += LOG_HEIGHT + 2;

		m_editAttribute.MoveWindow(r);
	}

	if(m_propertyCtrl.GetSafeHwnd())
	{
		CRect r = rect;
		r.left += TREE_WIDTH + 2;
		r.top += LOG_HEIGHT + 2;

		m_propertyCtrl.MoveWindow(r);
	}
}

void CLogDialog::SetAttribute(LPCTSTR lpszAttr)
{
	m_editAttribute.SetWindowText(lpszAttr);
}

void CLogDialog::AppendLog(LPCTSTR lpszText)
{
/*
m_csLogDlg.Lock();
	if(m_editLOG.GetSafeHwnd())
	{
		m_editLOG.SetRedraw(FALSE);

		int start, end;
		m_editLOG.GetSel(start, end);
		m_editLOG.SetSel(-1, -1);

		m_editLOG.ReplaceSel(lpszText);
		//m_editLOG.ReplaceSel(_T("\r\n"));

		m_editLOG.SetSel(start, end);

		m_editLOG.SetRedraw(TRUE);
	}
m_csLogDlg.Unlock();
*/
}

void CLogDialog::Add(CWnd* pParent, CWnd* pThis, LPCSTR lpszID, int nImage)
{
	if(pParent == NULL)
	{
		// add root
		HTREEITEM hItem = m_treectrl.InsertItem(lpszID);
		m_treectrl.SetItemImage(hItem, nImage, nImage);
		m_treectrl.SetItemData(hItem, (DWORD)pThis);
		m_treectrl.Expand(hItem, TVE_EXPAND);
		return;
	}

	HTREEITEM hItem = m_treectrl.GetRootItem();
	_Add(hItem, pParent, pThis, lpszID, nImage);
}

void CLogDialog::_Add(HTREEITEM hItem, CWnd* pParent, CWnd* pThis, LPCSTR lpszID, int nImage)
{
	while(hItem)
	{
		CWnd* parent_wnd = (CWnd*)m_treectrl.GetItemData(hItem);

		if(parent_wnd == pParent)
		{
			HTREEITEM item = m_treectrl.InsertItem(lpszID, hItem, TVI_SORT);
			m_treectrl.SetItemImage(item, nImage, nImage);
			m_treectrl.SetItemData(item, (DWORD)pThis);
			if(nImage < 4) m_treectrl.Expand(hItem, TVE_EXPAND);
			return;
		}

		if(m_treectrl.ItemHasChildren(hItem))
		{
			HTREEITEM hChildItem = m_treectrl.GetChildItem(hItem);
			_Add(hChildItem, pParent, pThis, lpszID, nImage);
		}

		hItem = m_treectrl.GetNextSiblingItem(hItem);
	}
}

void CLogDialog::Remove(CWnd* pWnd)
{
	if(m_treectrl.GetSafeHwnd())
	{
		HTREEITEM hItem = m_treectrl.GetRootItem();
		_Remove(hItem, pWnd);
	}
}

void CLogDialog::_Remove(HTREEITEM hItem, CWnd* pWnd)
{
	while(hItem)
	{
		CWnd* wnd = (CWnd*)m_treectrl.GetItemData(hItem);

		if(wnd == pWnd)
		{
			m_treectrl.DeleteItem(hItem);
			return;
		}

		if(m_treectrl.ItemHasChildren(hItem))
		{
			HTREEITEM hChildItem = m_treectrl.GetChildItem(hItem);
			_Remove(hChildItem, pWnd);
		}

		hItem = m_treectrl.GetNextSiblingItem(hItem);
	}
}

void CLogDialog::Play(CWnd* pThis)
{
	if(m_treectrl.GetSafeHwnd())
	{
		HTREEITEM hItem = m_treectrl.GetRootItem();
		_Play(hItem, pThis);
	}
}

void CLogDialog::_Play(HTREEITEM hItem, CWnd* pThis)
{
	while(hItem)
	{
		CWnd* wnd = (CWnd*)m_treectrl.GetItemData(hItem);

		if(wnd == pThis)
		{
			int img, sel_img;
			m_treectrl.GetItemImage(hItem, img, sel_img);
			if(img % 2 == 0)
				m_treectrl.SetItemImage(hItem, img+1, sel_img+1);
			return;
		}

		if(m_treectrl.ItemHasChildren(hItem))
		{
			HTREEITEM hChildItem = m_treectrl.GetChildItem(hItem);
			_Play(hChildItem, pThis);
		}

		hItem = m_treectrl.GetNextSiblingItem(hItem);
	}
}

void CLogDialog::Stop(CWnd* pThis)
{
	if(m_treectrl.GetSafeHwnd())
	{
		HTREEITEM hItem = m_treectrl.GetRootItem();
		_Stop(hItem, pThis);
	}
}

void CLogDialog::_Stop(HTREEITEM hItem, CWnd* pThis)
{
	while(hItem)
	{
		CWnd* wnd = (CWnd*)m_treectrl.GetItemData(hItem);

		if(wnd == pThis)
		{
			int img, sel_img;
			m_treectrl.GetItemImage(hItem, img, sel_img);
			if(img % 2 == 1)
				m_treectrl.SetItemImage(hItem, img-1, sel_img-1);
			return;
		}

		if(m_treectrl.ItemHasChildren(hItem))
		{
			HTREEITEM hChildItem = m_treectrl.GetChildItem(hItem);
			_Stop(hChildItem, pThis);
		}

		hItem = m_treectrl.GetNextSiblingItem(hItem);
	}
}

LRESULT CLogDialog::OnDebugString(WPARAM wParam, LPARAM lParam)
{
	CString* pStr = (CString*)wParam;

	if(pStr)
	{
		AppendLog(*pStr);
		delete pStr;
	}

	return 0;
}

int CLogDialog::GetDepth(HTREEITEM hItem)
{
	int depth = 0;

	HTREEITEM hRootItem = m_treectrl.GetRootItem();

	while(hRootItem != hItem && hItem != NULL)
	{
		hItem = m_treectrl.GetParentItem(hItem);
		depth++;
	}

	return depth;
}

void CLogDialog::OnDefaulttemplateChange()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.

	CString id = m_treectrl.GetItemText(m_hCurrentItem);

	::AfxGetMainWnd()->SendMessage(WM_DEFAULT_TEMPLATE_UPDATE, 0, (LPARAM)(LPCSTR)id);
}

void CLogDialog::OnPlayNextSchedule()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.

	CWnd* wnd = (CWnd*)m_treectrl.GetItemData(m_hCurrentItem);
	if(wnd->GetSafeHwnd())
	{
		wnd->PostMessage(ID_PLAY_NEXT_SCHEDULE);
	}//if
}

BOOL CLogDialog::OnHelpInfo(HELPINFO* pHelpInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return TRUE;
//	return CDialog::OnHelpInfo(pHelpInfo);
}

void CLogDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}
