/************************************************************************************/
/*! @file DownloadResult.cpp
	@brief 컨텐츠 파일을 다운로드하는 로그를 기록하는 클래스
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/11/22\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2010/11/22:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "StdAfx.h"
#include "DownloadResult.h"


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (CDownloadFileArray*) paryFile : (in) 다운로드하려는 컨텐츠 파일들의 정보 배열
/////////////////////////////////////////////////////////////////////////////////
CDownloadResult::CDownloadResult(CDownloadFileArray* paryFile)
:m_strProgramId(_T(""))		
,m_strContentsList(_T(""))	
,m_strSuccessList(_T(""))	
,m_strFailList(_T(""))		
,m_strLoclaExistList(_T(""))
,m_strFileName(_T(""))		
,m_nSuccessCount(0)
,m_nFailCount(0)		
,m_nLocalExistCount(0)	
,m_nTotalCount(0)		
,m_nState(E_STATE_INIT)
,m_paryFile(NULL)
,m_strCurrentProgress(_T("0/0"))
{
	m_paryFile = paryFile;

	InitResult();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CDownloadResult::~CDownloadResult()
{

}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 기록을 초기화 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::InitResult()
{
	if(!m_paryFile)
	{
		return;
	}//if

	memset(&m_stDownloadFileInfo, 0x00, sizeof(ST_DownloadFileInfo));

	CString str = ::AfxGetAppName();
	if(str == "UTV_brwClient2")
	{
		m_strFileName += "UBCDownloadResult.ini";
		m_stDownloadFileInfo.browserId = 0;
	}
	else
	{
		m_strFileName += "UBCDownloadResult1.ini";
		m_stDownloadFileInfo.browserId = 1;
	}//if

	ciString strHost, strMac, strEdit, strVer;
	scratchUtil* aUtil = scratchUtil::getInstance();
	if(aUtil->readAuthFile(strHost, strMac, strEdit))
	{
		strcpy(m_stDownloadFileInfo.hostId, strHost.c_str());
	}//if
	strcpy(m_stDownloadFileInfo.programId, GetHostName());
	strcpy(m_stDownloadFileInfo.siteId, GetSiteID());

	//이전의 파일을 삭제한다.
	CString strPath = GetAppPath();
	strPath += "data\\";
	strPath += m_strFileName;
	DeleteFile(strPath);

	//ProgramId
	m_strProgramId = GetHostName();

	//totalCount
	m_nTotalCount = m_paryFile->GetCount();

	//Progress
	m_strCurrentProgress.Format("%d/%d", m_nSuccessCount, m_nTotalCount);

	//ContentsList
	m_strContentsList = "";
	CString strTmpId;
	for(int i=0; i<m_nTotalCount; i++)
	{
		strTmpId = m_paryFile->GetAt(i)->m_strContentsId;
		m_strContentsList += "Contents_";
		m_strContentsList += strTmpId.Trim();
		m_strContentsList += ",";
	}//for
	m_strContentsList.TrimRight(",");

	//초기값으로 셋팅한다.
	//programId
	WriteINIValue(m_strFileName, "ROOT", "programId", m_strProgramId);

	//totalCount
	WriteINIValue(m_strFileName, "ROOT", "totalCount", ::ToString(m_nTotalCount));

	//currentProgress
	WriteINIValue(m_strFileName, "ROOT", "currentProgress", m_strCurrentProgress);

	//successCount
	WriteINIValue(m_strFileName, "ROOT", "successCount", ::ToString(m_nSuccessCount));

	//failCount
	WriteINIValue(m_strFileName, "ROOT", "failCount", ::ToString(m_nFailCount));

	//localExistCount
	WriteINIValue(m_strFileName, "ROOT", "localExistCount", ::ToString(m_nLocalExistCount));

	//contentsList
	WriteINIValue(m_strFileName, "ROOT", "contentsList", m_strContentsList);

	//successList
	WriteINIValue(m_strFileName, "ROOT", "successList", m_strSuccessList);

	//failList
	WriteINIValue(m_strFileName, "ROOT", "failList", m_strFailList);

	//localExistList
	WriteINIValue(m_strFileName, "ROOT", "localExistList", m_strLoclaExistList);

	//state
	WriteINIValue(m_strFileName, "ROOT", "state", ::ToString(m_nState));

	//startTime
	WriteINIValue(m_strFileName, "ROOT", "startTime", "");

	//endTime
	WriteINIValue(m_strFileName, "ROOT", "endTime", "");

	CDownloadFile* pFile;
	CString strContentsKey;
	for(int i=0; i<m_nTotalCount; i++)
	{
		pFile = m_paryFile->GetAt(i);
		pFile->SetInit();
		strContentsKey.Format("Contents_%s", pFile->m_strContentsId);

		//state
		WriteINIValue(m_strFileName, strContentsKey, "state",::ToString(pFile->m_nDownloadState));	//초기상태

		//contentsId
		WriteINIValue(m_strFileName, strContentsKey, "contentsId", pFile->m_strContentsId);

		//contentsName
		WriteINIValue(m_strFileName, strContentsKey, "contentsName", pFile->m_strContentsName);

		//contentsType
		WriteINIValue(m_strFileName, strContentsKey, "contentsType", ::ToString(pFile->m_nContentType));

		//fileName
		WriteINIValue(m_strFileName, strContentsKey, "fileName", pFile->m_strFileName);

		//location
		WriteINIValue(m_strFileName, strContentsKey, "location", pFile->m_strServerPath);

		//volume
		WriteINIValue(m_strFileName, strContentsKey, "volume", ::ToString(pFile->m_ulFileSize));

		//downloadVolume
		WriteINIValue(m_strFileName, strContentsKey, "downloadVolume", ::ToString(pFile->m_ullDownloadSize));

		//startTime
		WriteINIValue(m_strFileName, strContentsKey, "startTime", "");

		//endTime
		WriteINIValue(m_strFileName, strContentsKey, "endTime", "");

		//reason
		WriteINIValue(m_strFileName, strContentsKey, "reason", ::ToString(pFile->m_nDownloadReason));

		//result
		WriteINIValue(m_strFileName, strContentsKey, "result", ::ToString(pFile->m_bDownloadResult));
	}//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 시작을 기록한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::BeginDownload()
{
	if(!m_paryFile)
	{
		return;
	}//if

	//state
	m_nState = E_STATE_DOWNLOADING;		//다운로드 시작
	WriteINIValue(m_strFileName, "ROOT", "state", ::ToString(m_nState));

	//startTime
	m_tmStartTime = CTime::GetCurrentTime();
	WriteINIValue(m_strFileName, "ROOT", "startTime", ::ToString((ULONG)m_tmStartTime.GetTime()));

	//다운로드 시작을 알림
	SendUpdateEvent(false);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 종료를 기록한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::EndDownload()
{
	if(!m_paryFile)
	{
		return;
	}//if

	//endTime
	m_tmEndTime = CTime::GetCurrentTime();
	WriteINIValue(m_strFileName, "ROOT", "endTime", ::ToString((ULONG)m_tmEndTime.GetTime()));

	//다운로드 결과를 계산, 정리하여 기록...
	CString strContentsKey;
	CDownloadFile* pFile = NULL;
	CString strLocalPath, strTmpPath;
	CFileStatus fs;

	m_nSuccessCount			= 0;
	m_nFailCount			= 0;
	m_nLocalExistCount		= 0;
	m_strSuccessList		= "";
	m_strFailList			= "";
	m_strLoclaExistList		= "";

	for(int i=0; i<m_nTotalCount; i++)
	{
		pFile = m_paryFile->GetAt(i);
		strContentsKey.Format("Contents_%s", pFile->m_strContentsId);

		switch(pFile->m_nDownloadReason)
		{
		case E_REASON_CONNECTION_FAIL:
		case E_REASON_FILE_NOTFOUND:
		case E_REASON_EXCEPTION_FTP:
		case E_REASON_EXCEPTION_FILE:
			{
				//로컬에서 검사하여 존재여부와 사이즈를 비교하여 상태와 결과를 바꾸어 준다.
				GetLocalPath(strLocalPath, strTmpPath, pFile->m_strServerPath, pFile->m_strFileName);
				if(CFile::GetStatus(strLocalPath, fs) == TRUE)
				{
					if(fs.m_size == pFile->m_ulFileSize)
					{
						//로컬에 파일이 존재하고 사이즈가 같다면 성공으로 처리...
						pFile->m_nDownloadState = E_STATE_COMPLETE_SUCCESS;
						pFile->m_nDownloadReason = E_REASON_LOCAL_EXIST;
						pFile->m_bDownloadResult = true;
						pFile->m_tmStartTime = pFile->m_tmEndTime;
						pFile->m_ullDownloadSize = pFile->m_ulFileSize;

						m_nLocalExistCount++;
						m_strLoclaExistList += strContentsKey;
						m_strLoclaExistList += ",";

						m_nSuccessCount++;
						m_strSuccessList += strContentsKey;
						m_strSuccessList += ",";
					}
					else
					{
						//로컬에 존재하고 파일의 사이즈만 틀리다면 실패...
						m_nFailCount++;
						m_strFailList += strContentsKey;
						m_strFailList += ",";
					}//if
				}
				else
				{
					//로컬에도 없다면 실패
					m_nFailCount++;
					m_strFailList += strContentsKey;
					m_strFailList += ",";
				}//if
			}
			break;
		case E_REASON_LOCAL_EXIST:
			{
				pFile->m_tmStartTime = pFile->m_tmEndTime;
				pFile->m_ullDownloadSize = pFile->m_ulFileSize;

				m_nLocalExistCount++;
				m_strLoclaExistList += strContentsKey;
				m_strLoclaExistList += ",";

				m_nSuccessCount++;
				m_strSuccessList += strContentsKey;
				m_strSuccessList += ",";
			}
			break;
		case E_REASON_DOWNLOAD_SUCCESS:
			{
				m_nSuccessCount++;
				m_strSuccessList += strContentsKey;
				m_strSuccessList += ",";
			}
			break;
		case E_REASON_UNKNOWN:
			{
				m_nFailCount++;
				m_strFailList += strContentsKey;
				m_strFailList += ",";
			}
			break;
		}//switch

		//downloadVolume
		WriteINIValue(m_strFileName, strContentsKey, "downloadVolume", ::ToString(pFile->m_ullDownloadSize));

		//startTime
		WriteINIValue(m_strFileName, strContentsKey, "startTime", ::ToString((ULONG)pFile->m_tmStartTime.GetTime()));

		//endTime
		WriteINIValue(m_strFileName, strContentsKey, "endTime", ::ToString((ULONG)pFile->m_tmEndTime.GetTime()));

		//state
		WriteINIValue(m_strFileName, strContentsKey, "state",::ToString(pFile->m_nDownloadState));

		//reason
		WriteINIValue(m_strFileName, strContentsKey, "reason", ::ToString(pFile->m_nDownloadReason));

		//result
		WriteINIValue(m_strFileName, strContentsKey, "result", ::ToString(pFile->m_bDownloadResult));
	}//for

	m_strSuccessList.TrimRight(",");
	m_strFailList.TrimRight(",");
	m_strLoclaExistList.TrimRight(",");
	m_strCurrentProgress.Format("%d/%d", m_nSuccessCount, m_nTotalCount);

	WriteINIValue(m_strFileName, "ROOT", "currentProgress", m_strCurrentProgress);
	WriteINIValue(m_strFileName, "ROOT", "successCount", ::ToString(m_nSuccessCount));
	WriteINIValue(m_strFileName, "ROOT", "failCount", ::ToString(m_nFailCount));
	WriteINIValue(m_strFileName, "ROOT", "localExistCount", ::ToString(m_nLocalExistCount));
	WriteINIValue(m_strFileName, "ROOT", "successList", m_strSuccessList);
	WriteINIValue(m_strFileName, "ROOT", "failList", m_strFailList);
	WriteINIValue(m_strFileName, "ROOT", "localExistList", m_strLoclaExistList);

	if(m_nFailCount == 0)
	{
		m_nState = E_STATE_COMPLETE_SUCCESS;
	}
	else if(m_nFailCount == m_nTotalCount)
	{
		m_nState = E_STATE_COMPLETE_FAIL;
	}
	else
	{
		m_nState = E_STATE_PARTIAL_FAIL;
	}//if

	//state
	WriteINIValue(m_strFileName, "ROOT", "state", ::ToString(m_nState));

	//다운로드 종료를 알림
	SendUpdateEvent(false);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 상태를 알린다. \n
/// @param (bool) bWrite : (in) 파일에 기록하는지 여부
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::SendUpdateEvent(bool bWrite)
{
	if(strlen(m_stDownloadFileInfo.hostId) == 0)
	{
		__DEBUG__("Empty HostId", NULL);
	}//if

	if(bWrite)
	{
		m_strCurrentProgress.Format("%d/%d", m_nSuccessCount, m_nTotalCount);
		WriteINIValue(m_strFileName, "ROOT", "successCount", ::ToString(m_nSuccessCount));
		WriteINIValue(m_strFileName, "ROOT", "currentProgress", m_strCurrentProgress);
	}//if

	__DEBUG__("Send DownloadResult event", _NULL);
	__DEBUG__("State", m_nState);
	__DEBUG__("Current progress", m_strCurrentProgress);

	m_stDownloadFileInfo.state = m_nState;
	sprintf(m_stDownloadFileInfo.progress, "%s", m_strCurrentProgress);

	COPYDATASTRUCT  CDS;
	// Set CDS
	CDS.dwData = 2000;
	CDS.cbData = sizeof(ST_DownloadFileInfo);
	CDS.lpData = (PVOID)&m_stDownloadFileInfo;

	__DEBUG__("Program Id", m_stDownloadFileInfo.programId);
	__DEBUG__("Host Id", m_stDownloadFileInfo.hostId);
	__DEBUG__("Site Id", m_stDownloadFileInfo.siteId);
	__DEBUG__("Progress", m_stDownloadFileInfo.progress);
	__DEBUG__("State", m_stDownloadFileInfo.state);
	__DEBUG__("Browser Id", m_stDownloadFileInfo.browserId);
	
	//Firmware View에 보낸다.
	scratchUtil* aUtil = scratchUtil::getInstance();
	ULONG ulPid = aUtil->getPid("UBCFirmwareView.exe");
	//ULONG ulPid = aUtil->getPid("DownloadResultRecv.exe");
	__DEBUG__("FirmwareView Pid", (int)ulPid);

	HWND hWnd = aUtil->getWHandle(ulPid);
	CString strHandle;
	strHandle.Format("FirmwareView Handle = %d", hWnd);
	__DEBUG__(strHandle, _NULL);

	::SendMessage(hWnd , WM_COPYDATA , 0, (LPARAM)&CDS);
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ini 파일의 lock 상태가 unlock상태가 될때까지 대기한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloadResult::WaitForLock()
{
	CString strValue;
	//먼저 lock 값(1: lock, 0: Unlock)을 읽어서 lock 상태이면 잠시후에 다시 시도한다. 
	strValue = GetINIValue(m_strFileName, "ROOT", "lockState");
	if(strValue == "")
	{
		__DEBUG__("Can't found lockState value", _NULL);
		return true;
	}//if

	__DEBUG__("File lockState", strValue);

	if(strValue == "1")
	{
		HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		DWORD dwRet;
		int nCount = 0;

		while(1)
		{
			dwRet = WaitForSingleObject(hEvent, 500);
			if(dwRet == WAIT_TIMEOUT)
			{
				strValue = GetINIValue(m_strFileName, "ROOT", "lockState");
				if(strValue == "1")
				{
					nCount++;
					if(nCount >= 5)	//3초간 반복 대기하여도 잠금 상태이면 실패
					{
						__DEBUG__("Fail to read : lock state", _NULL);
						CloseHandle(hEvent);
						return false;
					}//if

					continue;
				}
				else if(strValue == "0")
				{
					//잠금 상태가 풀렸으면 값을 읽는다.
					break;
				}//if
			}//if
		}//while
	}//if

	return true;
}
*/