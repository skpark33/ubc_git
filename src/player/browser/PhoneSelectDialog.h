#pragma once
#include "afxcmn.h"


// CPhoneSelectDialog 대화 상자입니다.

class CPhoneSelectDialog : public CDialog
{
	DECLARE_DYNAMIC(CPhoneSelectDialog)

public:
	CPhoneSelectDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPhoneSelectDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PHONE_SELECT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CWnd*		m_pParentWnd;

	CString		m_strSiteId;
	CString		m_strHostId;
	CString		m_strHostName;
	CString		m_strIPAddress;

public:
	CListCtrl	m_listctrlPhone;
	
	void	ResetInfo(LPCSTR lpszSiteId="*", LPCSTR lpszHostId="*");
	bool	GetInfo(CString& strSiteId, CString& strHostId, CString& strHostName, CString& strIPAddress);
	bool	PhoneRequest(LPCSTR lpszSiteId, LPCSTR lpszHostId);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
