#pragma once

// Modified by 정운형 2008-11-03 오후 1:38
// 변경내역 :  LicenseDialog 폰트수정작업
//StaticTime 클래스를 MatrixStatic로 대치하도록함
/*
#include "StaticTime.h"

#define		TIMEDIALOG_WIDTH	245
#define		TIMEDIALOG_HEIGHT	162
*/
#include "MatrixStatic\\MatrixStatic.h"

//MatrixStatic에서 글자의 최대 width는 19, height는 27 이다
//#define		TIMEDIALOG_WIDTH	((19*9) + 30)		//최대 9 글자 + gap
//#define		TIMEDIALOG_HEIGHT	((27*3) + 15)		//chaannel, 현재시간, 경과시간의 3 line
#define		LICENSE_DIALOG_WIDTH	((19*20) + 30)		//최대 24 글자 + gap
#define		LICENSE_DIALOG_HEIGHT	((27) + 15)		//program, chaannel, (현재시간, 경과시간)의 3 line
// Modified by 정운형 2008-11-03 오후 1:38
// 변경내역 :  LicenseDialog 폰트수정작업


// CLicenseDialog 대화 상자입니다.

class CLicenseDialog : public CDialog
{
	DECLARE_DYNAMIC(CLicenseDialog)

public:
	CLicenseDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLicenseDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TIME_DIALOG };

	typedef BOOL (_stdcall *API_TYPE)(HWND,COLORREF,BYTE,DWORD); 
	API_TYPE g_SetLayeredWindowAttributes; 


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CPoint	m_pointDelta;
	bool	m_bLButton;

	// Modified by 정운형 2008-11-03 오후 1:38
	// 변경내역 :  LicenseDialog 폰트수정작업
	//StaticTime 클래스를 MatrixStatic로 대치하도록함
	/*
	CStaticTime	m_staticTimeCurrent;
	CStaticTime	m_staticTimeElapse;
	CStaticTime	m_staticTimeTemplate;
	*/
	//CMatrixStatic	m_staticTimeCurrent;		///<현재 시간을 표시
	//CMatrixStatic	m_staticTimeElapse;			///<프로그램이 시작되고 경과된 시간
	//CMatrixStatic	m_staticTimeTemplate;		///<프로그램에 설정된 default template ID
	CMatrixStatic	m_staticTimeProgram;		///<방송중인 program(site_host)
	// Modified by 정운형 2008-11-03 오후 1:38
	// 변경내역 :  LicenseDialog 폰트수정작업
	//StaticTime 클래스를 MatrixStatic로 대치하도록함

	//CTime		m_timeStart;					///<프로그램이 시작된 시간
	//CImageList	m_imageList;					///<네트웍 상태를 나타내는 이미지
	//CString		m_strTemplate;					///<프로그램에 설정된 default template ID

public:

	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnPaint();

	//bool	m_bConnectNetwork;

	//void	SetTemplateId(LPCSTR lpszTemplateId);		///<방송중인 템플릿 ID를 설정한다
	//CString	GetTemplateId(void);						///<설정된 템플릿 ID를 반환한다.
};
