/************************************************************************************/
/*! @file DownloadFile.h
	@brief FTP에서 컨텐츠 파일을 다운로드 받기위한 정보를 갖는 CDownloadFile 클래스 선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/06/26\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
	@b 작성)
	-# <2009/06/26:정운형:최초작성>

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

//다운로드 결과
enum E_DOWNLOAD_REASON
{
	E_REASON_INIT,				//초기상태
	E_REASON_CONNECTION_FAIL,	//연결을 실패함
	E_REASON_FILE_NOTFOUND,		//서버에 파일이 존재하지 않음
	E_REASON_EXCEPTION_FTP,		//FTP 오류
	E_REASON_EXCEPTION_FILE,	//파일액세스 오류
	E_REASON_LOCAL_EXIST,		//로컬에 존재하여 다운로드 하지 않음
	E_REASON_DOWNLOAD_SUCCESS,	//다운로드 성공
	E_REASON_UNKNOWN,			//알수없는 오류(연결끊김, exception...)
};

//다운로드 상태
enum E_DOWNLOAD_STATE
{
	E_STATE_INIT,				//초기상태
	E_STATE_DOWNLOADING,		//다운로드 상태
	E_STATE_PARTIAL_FAIL,		//일부파일 실패
	E_STATE_COMPLETE_FAIL,		//전체 실패(접속실패등...)
	E_STATE_COMPLETE_SUCCESS	//전체 성공
};



//! FTP에서 컨텐츠 파일을 다운로드 받기위한 정보를 갖는 클래스
/*!
*/
class CDownloadFile
{
public:
	CDownloadFile();
	virtual ~CDownloadFile();

	CDownloadFile& operator= (const CDownloadFile& clsInfo)
	{
		m_strContentsId		= clsInfo.m_strContentsId;
		m_strContentsName	= clsInfo.m_strContentsName;
		m_strServerPath		= clsInfo.m_strServerPath;
		m_strFileName		= clsInfo.m_strFileName;
		m_ulFileSize		= clsInfo.m_ulFileSize;
		m_nContentType		= clsInfo.m_nContentType;

		m_nDownloadReason	= clsInfo.m_nDownloadReason;
		m_nDownloadState	= clsInfo.m_nDownloadState;
		m_bDownloadResult	= clsInfo.m_bDownloadResult;
		m_ullDownloadSize	= clsInfo.m_ullDownloadSize;

		m_tmStartTime		= clsInfo.m_tmStartTime;
		m_tmEndTime			= clsInfo.m_tmEndTime;

		return *this;
	}

	bool operator== (CDownloadFile& clsInfo)
	{
		if(m_strContentsId		!= clsInfo.m_strContentsId)		return false;
		if(m_strContentsName	!= clsInfo.m_strContentsName)	return false;
		if(m_strServerPath		!= clsInfo.m_strServerPath)		return false;
		if(m_strFileName		!= clsInfo.m_strFileName)		return false;
		if(m_ulFileSize			!= clsInfo.m_ulFileSize)		return false;
		if(m_nContentType		!= clsInfo.m_nContentType)		return false;

		return true;
	}

	CString				m_strContentsId;		///<컨텐츠 Id
	CString				m_strContentsName;		///<컨텐츠 이름
	CString				m_strServerPath;		///<서버 location
	CString				m_strFileName;			///<컨텐츠 파일명
	ULONGLONG			m_ulFileSize;			///<파일의 volume
	int					m_nContentType;			///<컨텐츠 타입

//private:
	int					m_nDownloadReason;		///<다운로드 결과 값
	int					m_nDownloadState;		///<다운로드 상태
	bool				m_bDownloadResult;		///<다운로드 결과
	ULONGLONG			m_ullDownloadSize;		///<파일의 다운로드된 사이즈

	CTime				m_tmStartTime;			///<다운로드 시작시간
	CTime				m_tmEndTime;			///<다운로드 완료시간

public:
	void				SetInit(void);						///<초기상태로 다운로드 값들을 설정한다.
	void				SetStart(void);						///<시작상태로 다운로드 값들을 설정한다. 
	void				SetEnd(int nReason,
								int nState,
								int nDownloadSize, 
								bool bResult);				///<종료상태로 다운로드 결과값들을 설정한다.
};



//typedef	CArray<CDownloadFile, CDownloadFile&>	CDownloadFileArray;

class CDownloadFileArray : public CArray<CDownloadFile*, CDownloadFile*>
{
public:
	int				Add(CDownloadFile* pInfo);		///<새로운 엘리먼트 추가(중복된 element가 들어가지 않도록 재정의)
	void			Clear(void);					///<배열을 정리한다.
};