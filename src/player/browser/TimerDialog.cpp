// TimerDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "TimerDialog.h"


#define		TIMER_ID				5000	// id
#define		TIMER_TIME				1000	// time (ms)

#define		TIMER_TOTAL_TIME		10	// second


// CTimerDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTimerDialog, CDialog)

CTimerDialog::CTimerDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CTimerDialog::IDD, pParent)
	, m_pParentWnd (pParent)
{

}

CTimerDialog::~CTimerDialog()
{
}

void CTimerDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TEXT, m_staticText);
	DDX_Control(pDX, IDC_TIME_PROGRESS, m_progressTimer);
	DDX_Control(pDX, IDOK, m_btnOK);
}


BEGIN_MESSAGE_MAP(CTimerDialog, CDialog)
	ON_WM_TIMER()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CTimerDialog 메시지 처리기입니다.


BOOL CTimerDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_progressTimer.SetStyle(PROGRESS_TEXT);
	m_progressTimer.SetRange(0, TIMER_TOTAL_TIME);
	m_progressTimer.SetPos(0);
	CString txt;
	txt.LoadString(IDS_TIMER_TOTAL_TEXT);
	m_progressTimer.SetText(txt);

	txt.LoadString(IDS_CLOSE);
	m_btnOK.SetWindowText(txt);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CTimerDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CDialog::OnOK();
	ShowWindow(SW_HIDE);

	KillTimer(TIMER_ID);
}

void CTimerDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CDialog::OnCancel();
	OnOK();
}

void CTimerDialog::Start()
{
	//
	m_nCurrentPos = TIMER_TOTAL_TIME;
	m_progressTimer.SetPos(m_nCurrentPos);

	CString msg;
	msg.Format(IDS_TIMER_TOTAL_TEXT, m_nCurrentPos);
	m_progressTimer.SetText(msg);

	m_progressTimer.Invalidate();

	//
	SetTimer(TIMER_ID, TIMER_TIME, NULL);

	//
	CenterWindow();
	CRect rect;
	GetWindowRect(rect);
	rect.top += 200;
	rect.bottom += 200;
	MoveWindow(rect);
	ShowWindow(SW_SHOW);
}

void CTimerDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == TIMER_ID)
	{
		m_nCurrentPos--;
		if(m_nCurrentPos == 0)
		{
			OnCancel();
		}
		else
		{
			m_progressTimer.SetPos(m_nCurrentPos);

			CString msg;
			msg.Format(IDS_TIMER_TOTAL_TEXT, m_nCurrentPos);
			m_progressTimer.SetText(msg);

			m_progressTimer.Invalidate();
		}
	}
}



void CTimerDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}
