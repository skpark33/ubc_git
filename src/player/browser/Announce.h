/************************************************************************************/
/*! @file Announce.h
	@brief 긴급공지 객체 정의 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/09/24\n
	
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2009/09/24:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#ifndef _ANNOUNCE_H_
#define _ANNOUNCE_H_

class CAnnounce
{
public:
	CAnnounce();
	virtual ~CAnnounce();

	CAnnounce& operator= (const CAnnounce& clsAnn)
	{
		m_strMgrId			= clsAnn.m_strMgrId;
		m_strSiteId			= clsAnn.m_strSiteId;
		m_strAnnounceId		= clsAnn.m_strAnnounceId;
		m_strTitle			= clsAnn.m_strTitle;
		m_strCreator		= clsAnn.m_strCreator;
		m_tmCreateTime		= clsAnn.m_tmCreateTime;
		m_tmStartTime		= clsAnn.m_tmStartTime;
		m_tmEndTime			= clsAnn.m_tmEndTime;
		m_lPosition			= clsAnn.m_lPosition;
		m_sHeight			= clsAnn.m_sHeight;
		m_strFont			= clsAnn.m_strFont;
		m_strFgColor		= clsAnn.m_strFgColor;
		m_strBgColor		= clsAnn.m_strBgColor;
		m_strBgLocation		= clsAnn.m_strBgLocation;
		m_strBgFile			= clsAnn.m_strBgFile;
		m_sPlaySpeed		= clsAnn.m_sPlaySpeed;
		m_sAlpha			= clsAnn.m_sAlpha;
		m_nWidth			= clsAnn.m_nWidth;
		m_bPlaying			= clsAnn.m_bPlaying;

		for(int i=0; i<10; i++)
		{
			m_strComment[i] = clsAnn.m_strComment[i];
		}//for

		//추가분
		m_ContentsType	=		clsAnn.m_ContentsType;	
		m_LRMargin		=		clsAnn.m_LRMargin;
		m_UDMargin		=		clsAnn.m_UDMargin;
		m_SourceSize	=		clsAnn.m_SourceSize;
		m_SoundVolume	=		clsAnn.m_SoundVolume;
		m_FileSize		=		clsAnn.m_FileSize;
		m_FileMD5		=		clsAnn.m_FileMD5;
		m_HeightRatio	=		clsAnn.m_HeightRatio; 
		m_Width			=		clsAnn.m_Width;
		m_PosX			=		clsAnn.m_PosX;
		m_PosY			=		clsAnn.m_PosY;


		return *this;
	}

	bool operator== (CAnnounce& clsAnn)
	{
		if(m_strMgrId			!= clsAnn.m_strMgrId)		return false;
		if(m_strSiteId			!= clsAnn.m_strSiteId)		return false;
		if(m_strAnnounceId		!= clsAnn.m_strAnnounceId)	return false;
		if(m_strTitle			!= clsAnn.m_strTitle)		return false;
		if(m_strCreator			!= clsAnn.m_strCreator)		return false;
		if(m_lPosition			!= clsAnn.m_lPosition)		return false;
		if(m_sHeight			!= clsAnn.m_sHeight)		return false;
		if(m_strFont			!= clsAnn.m_strFont)		return false;
		if(m_strFgColor			!= clsAnn.m_strFgColor)		return false;
		if(m_strBgColor			!= clsAnn.m_strBgColor)		return false;
		if(m_strBgLocation		!= clsAnn.m_strBgLocation)	return false;
		if(m_strBgFile			!= clsAnn.m_strBgFile)		return false;
		if(m_sPlaySpeed			!= clsAnn.m_sPlaySpeed)		return false;
		if(m_sAlpha				!= clsAnn.m_sAlpha)			return false;
		if(m_nWidth				!= clsAnn.m_nWidth)			return false;
		if(m_bPlaying			!= clsAnn.m_bPlaying)		return false;

		if(m_tmCreateTime.getTime()	!= clsAnn.m_tmCreateTime.getTime())	return false;
		if(m_tmStartTime.getTime()	!= clsAnn.m_tmStartTime.getTime())	return false;
		if(m_tmEndTime.getTime()	!= clsAnn.m_tmEndTime.getTime())	return false;

		for(int i=0; i<10; i++)
		{
			if(m_strComment[i] != clsAnn.m_strComment[i])	return false;
		}//for

		//추가분
		if(m_ContentsType	!=		clsAnn.m_ContentsType)	return false;	
		if(m_LRMargin		!=		clsAnn.m_LRMargin)		return false;
		if(m_UDMargin		!=		clsAnn.m_UDMargin)		return false;
		if(m_SourceSize		!=		clsAnn.m_SourceSize)	return false;
		if(m_SoundVolume	!=		clsAnn.m_SoundVolume)	return false;
		if(m_FileSize		!=		clsAnn.m_FileSize)		return false;
		if(m_FileMD5		!=		clsAnn.m_FileMD5)		return false;
		if(m_HeightRatio	!=		clsAnn.m_HeightRatio)	return false; 
		if(m_Width			!=		clsAnn.m_Width)			return false;
		if(m_PosX			!=		clsAnn.m_PosX)			return false;
		if(m_PosY			!=		clsAnn.m_PosY)			return false;

		return true;
	}

	ciString		m_strMgrId;				///<프로세스ID
	ciString		m_strSiteId;			///<SITE ID
	ciString		m_strAnnounceId;		///<긴급공지 ID
	ciString		m_strTitle;				///<제목
	ciULong			m_ulSerialNo;			///<Key 번호
	ciString		m_strCreator;			///<생성자 ID
	ciTime			m_tmCreateTime;			///<생성 시간
	cciStringList	m_listHostIdList;		///<적용될 호스트 리스트
	ciTime			m_tmStartTime;			///<시작 시간, 0 이면 즉시
	ciTime			m_tmEndTime;			///<끝 시간, 0 이면 계속
	ciLong			m_lPosition;			///<티커 위치(0 : 바닥  1: 최상단. 2; 중앙)
	ciShort			m_sHeight;				///<티커의 두께
	ciString		m_strComment[10];		///<공지글 내용 최대 10줄
	ciString		m_strFont;
	ciShort			m_sFontSize;
	ciString		m_strFgColor;
	ciString		m_strBgColor;
	ciString		m_strBgLocation;		///<백그라운드 파일 경로
	ciString		m_strBgFile;			///<백그라운드 파일 명
	ciShort			m_sPlaySpeed;			///<속도는 1~5 까지의 값을 갖고 1이 제일 느린 속도이다
	ciShort			m_sAlpha;				///<0-100 투명도. 클수록 투명하다 . 추후 구현

	int				m_nWidth;					///<티커의 넓이
	bool			m_bPlaying;					///<방송중인지 여부 flag

	CString			GetXmlMessage(void);		///<공지의 내용을 xml문서 형태로 만들어 반환한다.
	CString			GetEmptyXmlMessage(void);	///<비어있는 공지를 xml문서 형태로 만들어 반환한다.
	int				GetRunningTime(void);		///<공지의 running time을 반환

	// 추가부분
	//cciStringList m_HostNameList;
	ciShort	m_ContentsType;
	ciShort m_LRMargin;
	ciShort m_UDMargin;
	ciShort m_SourceSize;
	ciShort m_SoundVolume;
	ciULongLong m_FileSize;
	ciString m_FileMD5;
	//ciBoolean	m_IsFileChanged;// DB 에는 기록되지 않는 값이다.
	ciFloat	m_HeightRatio; // 가로대 세로비 가로를 1로 했을때, 세로의 값이다. DB 에는 기록되지 않는다.
	ciShort		m_Width;
	ciShort   m_PosX;
	ciShort   m_PosY;



};

typedef	CArray<CAnnounce*, CAnnounce*>	CAryAnnounce;	///<Announce 객체 배열

#endif //_ANNOUNCE_H_