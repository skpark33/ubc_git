#include "stdafx.h"
#include "AudioMixer.h"

CAudioMixer* CAudioMixer::m_instance = 0;
CMutex CAudioMixer::m_mutex;

CAudioMixer* CAudioMixer::getInstance()
{
	if(m_instance == 0)
	{
		m_mutex.Lock();
		if(m_instance == NULL)
		{
			m_instance = new CAudioMixer;
			m_instance->Initialize();
		}
		m_mutex.Unlock();
	}
	return m_instance;
}

void CAudioMixer::clearInstance()
{
	m_mutex.Lock();
	if(m_instance)
	{
		delete m_instance;
		m_instance = NULL;
	}
	m_mutex.Unlock();
}

CAudioMixer::CAudioMixer()
{
	m_hMixer = NULL;
}

CAudioMixer::~CAudioMixer()
{
}


bool CAudioMixer::Initialize()
{
	if (m_hMixer != NULL) {
		return false;
	}

	// get the number of mixer devices present in the system
	m_nNumMixers = ::mixerGetNumDevs();

	m_hMixer = NULL;
	::ZeroMemory(&m_mxcaps, sizeof(MIXERCAPS));

	m_strDstLineName.Empty();
	m_strVolumeControlName.Empty();
	m_dwMinimum = 0;
	m_dwMaximum = 0;
	m_dwVolumeControlID = 0;
	m_dwMuteControlID = 0;
	m_dwVolumeValue = 0;

	//ciULong m_dwChannels; // 채널 갯수

	// open the first mixer
	// A "mapper" for audio mixer devices does not currently exist.
	if (m_nNumMixers != 0)
	{
		if (::mixerOpen(&m_hMixer,
						0,
						NULL,
						/*reinterpret_cast<ciULong>(this->GetSafeHwnd()),*/
						NULL,
						MIXER_OBJECTF_MIXER /*| CALLBACK_WINDOW*/)
			!= MMSYSERR_NOERROR)
		{
			return false;
		}

		// Volume & Mute 정보 초기화
		if (!GetMasterVolumeControl()) {
			return false;
		}
		
		if (::mixerGetDevCaps(reinterpret_cast<UINT>(m_hMixer),
							  &m_mxcaps, sizeof(MIXERCAPS))
			!= MMSYSERR_NOERROR)
		{
			return false;
		}

		if (!GetMasterVolumeValue(m_dwVolumeValue)) {
			return false;
		}

		if (!GetMasterMuteValue(m_bMute)) {
			return false;
		}
	}

	return true;
}


bool CAudioMixer::Uninitialize()
{
	bool bSucc = true;

	if (m_hMixer != NULL)
	{
		bSucc = (::mixerClose(m_hMixer) == MMSYSERR_NOERROR);
		m_hMixer = NULL;
	}

	return bSucc;
}

bool CAudioMixer::GetMasterVolumeControl()
{
	if (m_hMixer == NULL)
	{
		return false;
	}

	// get dwLineID
	MIXERLINE mxl;
	mxl.cbStruct = sizeof(MIXERLINE);
	mxl.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_SPEAKERS;
	if (::mixerGetLineInfo(reinterpret_cast<HMIXEROBJ>(m_hMixer),
						   &mxl,
						   MIXER_OBJECTF_HMIXER |
						   MIXER_GETLINEINFOF_COMPONENTTYPE)
		!= MMSYSERR_NOERROR)
	{
		return false;
	}

	// get dwControlID
	MIXERCONTROL mxc;
	MIXERLINECONTROLS mxlc;
	mxlc.cbStruct = sizeof(MIXERLINECONTROLS);
	mxlc.dwLineID = mxl.dwLineID;	
	mxlc.cControls = 1;
	mxlc.cbmxctrl = sizeof(MIXERCONTROL);
	mxlc.pamxctrl = &mxc;

	// Volume 컨트롤 얻기
	mxlc.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
	if (::mixerGetLineControls(reinterpret_cast<HMIXEROBJ>(m_hMixer),
							   &mxlc,
							   MIXER_OBJECTF_HMIXER |
							   MIXER_GETLINECONTROLSF_ONEBYTYPE)
		!= MMSYSERR_NOERROR)
	{
		return false;
	}

	// store dwControlID
	m_strDstLineName = mxl.szName;
	m_strVolumeControlName = mxc.szName;
	m_dwMinimum = mxc.Bounds.dwMinimum;
	m_dwMaximum = mxc.Bounds.dwMaximum;
	m_dwVolumeControlID = mxc.dwControlID;

	// Mute 컨트롤 얻기
	mxlc.dwControlType = MIXERCONTROL_CONTROLTYPE_MUTE;
	if(::mixerGetLineControls((HMIXEROBJ)m_hMixer, &mxlc, 
		MIXER_OBJECTF_HMIXER | MIXER_GETLINECONTROLSF_ONEBYTYPE) 
		!= MMSYSERR_NOERROR)
	{
		return false;
	}
	m_dwMuteControlID = mxc.dwControlID;

	return true;
}

bool CAudioMixer::GetMasterVolumeValue(DWORD &volume)
{
	if (m_hMixer == NULL)
	{
		return false;
	}

	MIXERCONTROLDETAILS_UNSIGNED mxcdVolume;
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwVolumeControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
	mxcd.paDetails = &mxcdVolume;
	if (::mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_GETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return false;
	}

	DWORD dwVal = mxcdVolume.dwValue;
	volume = (dwVal * 100) / m_dwMaximum;

	return true;
}

bool CAudioMixer::SetMasterVolumeValue(DWORD volume)
{
	if (m_hMixer == NULL)
	{
		return false;
	}

	DWORD dwVal; // 0 ~ 65536
	dwVal = (m_dwMaximum * volume)/100;

	MIXERCONTROLDETAILS_UNSIGNED mxcdVolume = { dwVal };
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwVolumeControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
	mxcd.paDetails = &mxcdVolume;
	if (::mixerSetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_SETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return false;
	}

	m_dwVolumeValue = volume;

	return true;
}


bool CAudioMixer::GetMasterMuteValue(bool &mute)
{
	if (m_hMixer == NULL)
	{
		return false;
	}

	int lVal;

	MIXERCONTROLDETAILS_BOOLEAN mxcdMute;
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwMuteControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
	mxcd.paDetails = &mxcdMute;
	if (::mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_GETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return false;
	}
	
	lVal = mxcdMute.fValue;
	if (lVal) mute = true;
	else mute = false;

	return true;
}

bool CAudioMixer::SetMasterMuteValue(bool mute)
{
	if (m_hMixer == NULL)
	{
		return false;
	}

	int lVal;
	if (mute) lVal = 1;
	else lVal = 0;

	MIXERCONTROLDETAILS_BOOLEAN mxcdMute = { lVal };
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwMuteControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
	mxcd.paDetails = &mxcdMute;
	if (::mixerSetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_SETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return false;
	}
	
	m_bMute = mute;

	return true;
}

bool CAudioMixer::IncreaseVolume(int percent)
{
	if (m_hMixer == NULL)
		return false;

	int volume = m_dwVolumeValue + percent;
	if( volume > 100 ) volume = 100;

	SetMasterVolumeValue(volume);

	return true;
}

bool CAudioMixer::DecreaseVolume(int percent)
{
	if (m_hMixer == NULL)
		return false;

	int volume = m_dwVolumeValue - percent;
	if( volume < 0 ) volume = 0;

	SetMasterVolumeValue(volume);

	return true;
}

bool CAudioMixer::ToggleMute()
{
	if (m_hMixer == NULL)
		return false;

	SetMasterMuteValue( !m_bMute );

	return true;
}



