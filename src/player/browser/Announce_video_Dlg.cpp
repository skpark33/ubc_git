// AnnounceDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "Announce_video_Dlg.h"
#include "MemDC.h"
#include "Host.h"

//동영상 화면갱신 타이머
#define		ID_VIDEO_REPAINT			4014
#define		TIME_VIDEO_REPAINT			1000
//동영상 재생종료 타이머
#define		ID_VIDEO_PLAYEND			4015
#define		TIME_VIDEO_PLAYEND			300

// CAnnounce_video_Dlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAnnounce_video_Dlg, CAnnounceDlg)

CAnnounce_video_Dlg::CAnnounce_video_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent /*=NULL*/)
	: CAnnounceDlg(nPosX,nPosY,nCx,nCy,pParent)
{
	__DEBUG__("CAnnounce_video_Dlg", _NULL);
	m_pInterface = NULL;
	m_nVedioRenderMode = GetVidoeRenderType();
	m_bCompletePlay= false;


	m_bOpen = false;
	m_bFileNotExist = false;
}

CAnnounce_video_Dlg::~CAnnounce_video_Dlg()
{
}

BEGIN_MESSAGE_MAP(CAnnounce_video_Dlg, CAnnounceDlg)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	//ON_WM_TIMER()
	ON_MESSAGE(WM_DSINTERFACES_GRAPHNOTIFY, OnGraphNotify)
	ON_MESSAGE(WM_DSINTERFACES_COMPLETEPLAY, OnCompletePlay)
END_MESSAGE_MAP()

int CAnnounce_video_Dlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG__("CAnnounce_video_Dlg::OnCreate()", _NULL);
	if (CAnnounceDlg::OnCreate(lpCreateStruct) == -1){
		return -1;
	}


	return 0;
}
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 시작하도록 한다. \n
/// @param (CAnnounce*) pAnn : (in) 방송을 해야하는 공지 객체 주소값
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_video_Dlg::BeginAnnounce(CAnnounce* pAnn)
{
	if(!m_pAnnPlay)
	{
		m_pAnnPlay = pAnn;
	}
	else if((*m_pAnnPlay == *pAnn) && m_pAnnPlay->m_bPlaying)
	{
		//if(!(((CHost*)m_pParentWnd)->IsWindowVisible())){
			__DEBUG__("Same Annnounce Shows", pAnn->m_strAnnounceId.c_str());
			ShowWindow(SW_SHOW);
			((CHost*)m_pParentWnd)->SetFocus();
		//}
		return;
	}
	else
	{
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = pAnn;
	}//if

	char	szBuf[_MAX_PATH] = { 0x00 };
	::GetModuleFileName(NULL, szBuf, _MAX_PATH);

	char drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
	_splitpath(szBuf, drive, path, filename, ext);

	m_strMediaFullPath.Format("%s\\SQISOFT\\Contents\\Enc\\announce\\%s",drive, m_pAnnPlay->m_strBgFile.c_str());


	__DEBUG__("BeginAnnounce",	m_strMediaFullPath);
	__DEBUG__("BeginAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());

	CString strLog = m_pAnnPlay->m_strBgFile.c_str();
	WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(),m_pAnnPlay->m_strTitle.c_str(), "TRY", m_pAnnPlay->GetRunningTime(), strLog);

	MoveAnnounceWindow(m_nParentPosX, m_nParentPosY, m_nParentCx, m_nParentCy, m_pAnnPlay);

	if(OpenFile(0)){
		m_pAnnPlay->m_bPlaying = true;
		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "START", m_pAnnPlay->GetRunningTime(), strLog);
		//__DEBUG__("ShowWindow", 0);
		ShowWindow(SW_SHOW);
	}else{
		SetNoContentsFile();
		//this->RedrawWindow();
		__DEBUG__("Window without contents played", NULL);
		m_pAnnPlay->m_bPlaying = true;
		ShowWindow(SW_SHOW);
	}
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 중지하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounce_video_Dlg::EndAnnounce()
{
	if(m_pAnnPlay)
	{
		__DEBUG__("EndAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());

		CString strLog = m_pAnnPlay->m_strBgFile.c_str();

		CloseFile();

		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "END", m_pAnnPlay->GetRunningTime(), strLog);

		ShowWindow(SW_HIDE);
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = NULL;
	}//if
}
void CAnnounce_video_Dlg::OnDestroy()
{
	CloseFile();

	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if
	CAnnounceDlg::OnDestroy();

}

void CAnnounce_video_Dlg::OnPaint()
{
	__DEBUG__("OnPaint",NULL);
	if(!m_bOpen) return;

	__DEBUG__("OnPaint Start",NULL);
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	
	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		CRect rtClient;
		GetClientRect(rtClient);

		CMemDC memDC(&dc);
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}//if

	if(m_ticker && 	m_ticker->IsPlay()){
		m_ticker->BringWindowToTop();
		m_ticker->SetForegroundWindow();
		m_ticker->SetFocus();
	}else{
		BringWindowToTop();
		SetForegroundWindow();
		SetFocus();
	}

	__DEBUG__("OnPaint End",NULL);
}

void CAnnounce_video_Dlg::SetNoContentsFile()
{
	__DEBUG__("Contents file not exist", m_strMediaFullPath);

	//컨텐츠 파일이 없는 경우 액박 표시해 준다.
	HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_XBOX));
	m_bgImage.CreateFromHBITMAP(hBitmap);
	m_bOpen = true;
	m_bFileNotExist = true;
}

bool CAnnounce_video_Dlg::Play()
{
	__DEBUG__("Play start", _NULL);
	if(!m_bOpen)
	{
		__DEBUG__("File not opened", _NULL);
		return false;
	}
	
	//m_dwStartTick = ::GetTickCount();

	if(m_pInterface)
	{
		m_pInterface->SeekToStart();
		m_pInterface->Play();
		m_bCompletePlay = false;
		m_pInterface->SetVolume( this->m_pAnnPlay->m_SoundVolume );
		__DEBUG__("Set Timer", _NULL);
		//SetTimer(ID_VIDEO_PLAYEND, TIME_VIDEO_PLAYEND, NULL);
	}//if

	//this->RedrawWindow();
	return true;
}

bool CAnnounce_video_Dlg::Stop()
{
	m_bCompletePlay = true;
	if(m_pInterface)
	{
		//KillTimer(ID_VIDEO_PLAYEND);
		m_pInterface->Stop();
	}//if
	
	if(m_bFileNotExist)
	{
		__DEBUG__("Play stop : Fail(Content file does not exist)", NULL);
	}
	else
	{
		__DEBUG__("Play stop : Succeed", NULL);
	}//if

	return true;
}

bool CAnnounce_video_Dlg::OpenFile(int nGrade)
{
	if(m_bOpen) 
	{ 
		if(m_pAnnPlay->m_bPlaying)
		{
			__DEBUG__("\t\t\tAlready Open", _NULL);
			return true;
		}
		CloseFile();
	}

	CFileStatus status;
	if(CFile::GetStatus(m_strMediaFullPath, status ) == FALSE)
	{
		//컨텐츠 파일이 없는 경우 액박 표시해 준다.
		SetNoContentsFile();
		return false;
	}
	m_bOpen = OpenDSRender();
	if(!m_bOpen)
	{
		__DEBUG__("Directshow render open faile", _NULL);
		return false;
	}

	return Play(); 
}

bool CAnnounce_video_Dlg::CloseFile()
{
	__DEBUG__("CloseFile()",NULL);
	
	Stop();

	if(m_bOpen)
	{
		if(m_pInterface)
		{
			CloseDSRender();
		}//if
		m_bOpen = false;
	}//if
	return true;
}

void CAnnounce_video_Dlg::CloseDSRender()
{
	if(m_pInterface)
	{
		__DEBUG__("Inteface close", _NULL);
		m_pInterface->Close();
		delete m_pInterface;
	}//if
	m_pInterface = NULL;
}

bool CAnnounce_video_Dlg::OpenDSRender()
{
	CloseDSRender();

	//m_nVedioRenderMode = GetVidoeRenderType(); 
	m_nVedioRenderMode = E_WINDOWLESS;

	bool bRet = false;
	if(m_nVedioRenderMode == E_DUMMY_OVERLAY
		/*|| m_nVedioRenderMode == E_OVERLAY*/)
	{
		//__DEBUG__("\t\t\tDummy Overlay Interface Mode", _NULL);
		__DEBUG__("\t\t\tDummy Overlay Interface Mode", m_strMediaFullPath);
		m_pInterface = new COverlayInterface(this);
		m_pInterface->SetParentWindow(GetSafeHwnd());
		m_pInterface->SetRenderType(m_nVedioRenderMode);

		if(m_pInterface->Open(m_strMediaFullPath))
		{
			bRet = true;
			//크기조정
			m_pInterface->SetAspectRatioMode(STRETCHED);
			//CreateCaptionImage();
		}
		else
		{
		__DEBUG__("\t\t\tOpen Failed", m_strMediaFullPath);
			m_bOpen = false;
			bRet = false;
			CString strError;
			strError.Format("Video file[%s] render fail", m_strMediaFullPath);
			__DEBUG__("ERROR", strError);

			CloseDSRender();
		}//if
	}
	else
	{
		m_pInterface = new CVMRRender(this);
		m_pInterface->SetParentWindow(GetSafeHwnd());
		m_pInterface->SetRenderType(m_nVedioRenderMode);
		if(!m_pInterface->Open(m_strMediaFullPath))
		{
			m_bOpen = false;
			bRet = false;
			CString strError;
			strError.Format("Video file[%s] render fail", m_strMediaFullPath);
			__DEBUG__("ERROR", strError);

			CloseDSRender();
		}
		else
		{
			bRet = true;
			m_pInterface->SetAspectRatioMode(STRETCHED);
		}//if
	}//if

	return bRet;
}

void CAnnounce_video_Dlg::RePlay()
{
	if(m_pInterface)
	{
		__DEBUG__("Begin replay schedule", m_strMediaFullPath);
		m_bCompletePlay = false;
		m_pInterface->SeekToStart();
		m_pInterface->Play();
		__DEBUG__("End replay schedule", m_strMediaFullPath);
	}//if
}


LRESULT CAnnounce_video_Dlg::OnGraphNotify(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__("OnGraphNotify", _NULL);
	
	if(m_pInterface)
	{
		m_csLock.Lock();
		if(!m_pInterface->HandleGraphNotify(lParam))
		{
			__DEBUG__("Fail to HandleGraphNotify", _NULL);
		}//if
		m_csLock.Unlock();
	}//if

	return 0;
}


LRESULT CAnnounce_video_Dlg::OnCompletePlay(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__("OnCompletePlay", _NULL);
	
	if(m_pInterface)
	{
		m_bCompletePlay = true;		//재생이 완료되었음
		RePlay();
	}//if

	return 0;
}

/*
void CAnnounce_video_Dlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == ID_VIDEO_PLAYEND && m_pInterface)
	{
		REFTIME tmCur, tmTotal, tmOver;
		tmCur = m_pInterface->GetCurrentPlayTime();
		tmTotal = m_pInterface->GetTotalPlayTime();
		if(tmCur >= tmTotal) {
			if(!m_bCompletePlay)
			{
				__DEBUG__("Play end timer", _NULL);
				m_bCompletePlay = true;
				RePlay();
			}//if
		}
	}//if
}
*/

BOOL CAnnounce_video_Dlg::OnEraseBkgnd(CDC* pDC)
{
/*
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	__DEBUG__("OnEraseBkgnd", _NULL);
	
	static DWORD m_dwRedrawTime = 0;

	DWORD dwNow = GetTickCount();
	//너무 자주 그려지는걸 막기위해 최대 0.2초 후에 그려지도록 한다.
	if((dwNow-m_dwRedrawTime) > 200)
	{
		__DEBUG__("Repaint", _NULL);
		if(m_pInterface)
		{
			if(!m_pInterface->RePaintVideo())
			{
				__DEBUG__("Video repaint fail", m_strMediaFullPath);
			}//if
		}//if
	}//if
	m_dwRedrawTime = GetTickCount();
		
	return TRUE;
*/	
	//return CSchedule::OnEraseBkgnd(pDC);
	return FALSE;
}



