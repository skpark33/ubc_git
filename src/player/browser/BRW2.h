// BRW2.h : PROJECT_NAME 응용 프로그램에 대한 주 헤더 파일입니다.
//

#pragma once


#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"		// 주 기호입니다.
//#include "ProfileManager.h"
#include <common/libProfileManager/ProfileManager.h>

#define MNG_PROFILE				(theApp.m_mngProfile)
#define MNG_PROFILE_READ		(theApp.m_mngProfile.GetProfileString)

//! 시스템의 모니터 정보를 갖는 구조체
/*!
	 \n
*/
typedef struct _st_Monitor
{
	int		nWidth;					// Width
	int		nHeight;				// Height
	int		nWidthVirtual;			// Width Virtual
	int		nHeightVirtual;			// Height Virtual
	int		nBitPerPixel;			// BitPerPixel
	int		nRefresh;				// Refresh
	int		nMonitor;				// Monitors
	int		nMegaPixel;				// MegaPixel
	BOOL	bSameDisplayFormat;		// SameDisplayFormat
	RECT	rcVirtual;
	CArray< MONITORINFOEX, MONITORINFOEX > aryMonitors;
	
}ST_MONITOR;


// CBRW2App:
// 이 클래스의 구현에 대해서는 BRW2.cpp을 참조하십시오.
//

class CBRW2App : public CWinApp
{
public:
	CBRW2App();

// 재정의입니다.
public:
	virtual BOOL InitInstance();

// 구현입니다.

	DECLARE_MESSAGE_MAP()

public:
	class CImpIDispatch* m_pDispOM;
	virtual int ExitInstance();

	ST_MONITOR			m_stInfoMonitor;				///<시스템의 모니터 정보를 갖는다
	CProfileManager		m_mngProfile;					///<컨텐츠 패키지파일(*.ini) 메모리 엑세스 클래스

	bool	GetMonitorInformation(void);				///<모니터 정보를 구한다.
	void	ProtectCopy(CString& strEdition,
						CString& strVersion,
						CString& strComputerName,
						CString& strKey,
						bool& bVerified);				///<라이센스를 확인한다.
	bool	CheckDuplicate(void);						///<중복실행 방지
	void	GetMonitorPosition(int& nPosX,
								int& nPosY,
								int& nCx,
								int& nCy,
								int& nMonitorIndex);	///<프로그램이 위치할 모니터의 위치를 구한다 

	void	DisableDivxLogo(void);						///<Divxe디코더에서 로고가 나오는것을 막는다.
	LPCSTR	GetWizardXML(void);							///<패키지 파일안의 모든 wizard xml을 취합하여 하나의 xml로 만들어 반환한다.

	bool	Check_download_only_case();
	bool	Stop3rdPartyPlayer();

	void	DisablePPTAutoRecovery();

	void	CheckIEVersion();
protected:
	CString	_getIEVersion();
};

extern CBRW2App theApp;