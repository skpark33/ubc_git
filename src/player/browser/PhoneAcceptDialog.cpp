// PhoneAcceptDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "PhoneAcceptDialog.h"

#include "Template.h"


#define		PHONE_CANCEL_ID				5000	// id
#define		PHONE_CANCEL_TIME			1000	// time (ms)

#define		RINGING_ID					5001	// id
#define		RINGING_TIME				3000	// time(ms)

#define		PHONE_CANCEL_TOTAL_TIME		30	// second

#define		RINGING_SOUND_FILE			"c:\\Windows\\Media\\ringin.wav"

// CPhoneAcceptDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPhoneAcceptDialog, CDialog)

CPhoneAcceptDialog::CPhoneAcceptDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPhoneAcceptDialog::IDD, pParent)
	, m_pParentWnd (pParent)
{

}

CPhoneAcceptDialog::~CPhoneAcceptDialog()
{
}

void CPhoneAcceptDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CANCEL_PROGRESS, m_progressCancel);
	DDX_Control(pDX, IDC_STATIC_TEXT, m_staticText);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CPhoneAcceptDialog, CDialog)
	ON_WM_TIMER()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CPhoneAcceptDialog 메시지 처리기입니다.


BOOL CPhoneAcceptDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_progressCancel.SetStyle(PROGRESS_TEXT);
	m_progressCancel.SetRange(0, PHONE_CANCEL_TOTAL_TIME);
	m_progressCancel.SetPos(0);

	CString txt;
	txt.LoadString(IDS_PHONE_CANCEL_TOTAL_TEXT);
	m_progressCancel.SetText(txt);

	txt.LoadString(IDS_YES);
	m_btnOK.SetWindowText(txt);

	txt.LoadString(IDS_NO);
	m_btnCancel.SetWindowText(txt);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPhoneAcceptDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CDialog::OnOK();
	ShowWindow(SW_HIDE);

	KillTimer(PHONE_CANCEL_ID);
	KillTimer(RINGING_ID);

	if(CTemplate::m_pPhoneTemplate != NULL /*&& CFrame::m_pPhoneFrame != NULL*/)
	{
		/*if(m_pParentWnd)
			m_pParentWnd->SendMessage(WM_PHONE_SCHEDULE_UPDATE, (WPARAM)(LPCSTR)m_strScheduleId, (LPARAM)m_pReply);*/
	}

	if(m_pReply)
	{
		delete m_pReply;
		m_pReply = NULL;
	}
}

void CPhoneAcceptDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CDialog::OnCancel();
	ShowWindow(SW_HIDE);

	KillTimer(PHONE_CANCEL_ID);
	KillTimer(RINGING_ID);

	cciEvent hbEvent;
	cciEntity source("Site=%s/Host=%s/Process=%s", ::GetSiteName(), ::GetHostName(), ::GetAppId());
	hbEvent.setEntity(source);
	hbEvent.setEventType("phoneReject");

	cciTime eventTime;
	hbEvent.setEventTime(eventTime);

	hbEvent.addItem("scheduleId", m_strScheduleId);

	if (!hbEvent.post())
	{
		::AfxMessageBox(IDS_FAIL_TO_SEND_VIDEOPHONE_REJECT_MESSAGE, MB_ICONSTOP);		// Fail to send a VideoPhone Reject Message !!!
	}

	if(m_pReply)
	{
		delete m_pReply;
		m_pReply = NULL;
	}
}

void CPhoneAcceptDialog::Start(LPCSTR lpszScheduleId, cciReply* reply)
{
	//
	m_strScheduleId = lpszScheduleId;
	m_pReply = reply;

	//
	m_nCurrentPos = PHONE_CANCEL_TOTAL_TIME;
	m_progressCancel.SetPos(m_nCurrentPos);

	CString msg;
	msg.Format(IDS_PHONE_CANCEL_TOTAL_TEXT, m_nCurrentPos);
	m_progressCancel.SetText(msg);

	m_progressCancel.Invalidate();

	//
//	SetTimer(PHONE_CANCEL_ID, PHONE_CANCEL_TIME, NULL);
//	SetTimer(RINGING_ID, RINGING_TIME, NULL);

	//
//	CenterWindow();
//	ShowWindow(SW_SHOW);
	OnOK();
}

void CPhoneAcceptDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == PHONE_CANCEL_ID)
	{
		m_nCurrentPos--;
		if(m_nCurrentPos == 0)
		{
			OnCancel();
		}
		else
		{
			m_progressCancel.SetPos(m_nCurrentPos);

			CString msg;
			msg.Format(IDS_PHONE_CANCEL_TOTAL_TEXT, m_nCurrentPos);
			m_progressCancel.SetText(msg);

			m_progressCancel.Invalidate();
		}
	}
	else if(nIDEvent == RINGING_ID)
	{
		sndPlaySound(RINGING_SOUND_FILE, SND_ASYNC);
	}
}



void CPhoneAcceptDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}
