#pragma once

#include <atlimage.h>

class CVideoSchedule;

// CVideoControllerWnd

class CVideoControllerWnd : public CWnd
{
	DECLARE_DYNAMIC(CVideoControllerWnd)

public:
	CVideoControllerWnd();
	virtual ~CVideoControllerWnd();

protected:
	DECLARE_MESSAGE_MAP()

	CVideoSchedule* m_pVideoSchedule;
	double	m_fTotalTime;
	double	m_fCurrentTime;

	CRgn	m_rgn;

	bool	m_bFocusCaptured;
	double	m_fCurrentPos;

	CBitmap	m_bmpPlay;
	CBitmap	m_bmpPause;

	CBitmap	m_bmpProgressHead;
	CBitmap	m_bmpProgressTail;
	CBitmap	m_bmpProgressComplete;
	CBitmap	m_bmpProgressIncomplete;

	CImage	m_imgCurrentMark;
	CBitmap	m_bmpTimelineMask;
	CBitmap	m_bmpTimelineCore;

	CRect	m_rectButton;
	CRect	m_rectHead;
	CRect	m_rectBody;
	CRect	m_rectTail;
	CSize	m_sizeTimeline;

	BOOL	CreateRegion();
	void	GetVideoTimeInfo();

	BOOL	LoadPNG(CImage& imgPNG, UINT nIDResource);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	BOOL	Create(CVideoSchedule* pParentWnd);

	BOOL	Popup();
	BOOL	Minimize();
};


