#pragma once

#include "Frame.h"
//#include "ximage.h"
#include "TemplatePlay.h"

// CTemplate

class CTemplate : public CWnd
{
	DECLARE_DYNAMIC(CTemplate)

public:
	static	CTemplate*	GetTemplateObject(CWnd* pParent, LPCSTR lpszID, CString& strErrorMessage);

	CTemplate(CWnd* pParent, LPCSTR lpszID);
	virtual ~CTemplate();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnDebugString(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDebugGrid(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnErrorMessage(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayNextSchedule(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHideContents(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSoundVolumeMute(WPARAM wParam, LPARAM lParam);

protected:
	DECLARE_MESSAGE_MAP()

	// frame attributes
	ciString	m_templateId;
	ciShort		m_width;
	ciShort		m_height;
	ciString	m_bgColor;
	ciString	m_description;
	ciString	m_shortCut;

	bool		m_bPhoneTemplate;
	COLORREF	m_rgbBgColor;
	CPtrArray	m_aryFrame;			///<Frame 배열

	double		m_fXScale;
	double		m_fYScale;

	int			m_nTemplateX;
	int			m_nTemplateY;

	static UINT	m_nTemplateID;		///<윈도우 생성시에 사용하는 윈도우 고유 ID

	CWnd*		m_pParentWnd;
	CString		m_strErrorMessage;
	CString		m_strLoadID;
	int			m_nPlayCount;
	UINT		m_nShortcutKey;
	bool		m_bPlay;

	UINT		m_nSysShortcutKey;

public:

	static	CTemplate*	m_pPhoneTemplate;
	static	CCriticalSection m_csTemplatePhone;

	bool	LoadFrame();
	bool	OpenFile();
	bool	IsBetween(bool bDefaultSchedule);
	bool	IsPlay()			{ return m_bPlay; };

	CTemplatePlay*		m_pclsPalyRule;					///<Tmplate play rule을 갖는 클래스 포인터
	void	Play(CTemplatePlay* pclsPlayRule = NULL);	///<Tmplate play rule을 설정하여 play하도록 하는 함수
	void	Stop();
	void	Pause(void);								///<Scheduel pause

	LPCSTR	GetTemplateId() { return m_templateId.c_str(); };
	void	GetScale(double& fXScale, double& fYScale) { fXScale = m_fXScale; fYScale = m_fYScale; };
	LPCSTR	GetLoadID() { return m_strLoadID; };

	int		GetFrameCount() { return m_aryFrame.GetCount(); }  //skpark 2012.12.18  프레임갯수를 구함.

	CString	m_debugString;
	virtual CString	ToString();

	bool	GetFrameScheduleInfo(FRAME_SCHEDULE_INFO_LIST& infoList);

	int		GetPlayCount() { return m_nPlayCount; };
	void	ResetPlayCount();

	UINT	GetShortcutKey() { return m_nShortcutKey; };

	void	CheckFrame(void);										///<Template play rule를 사용하여 play하는 frame을 변경하는 함수
	void	GetScheduleList(CScheduleArray&	listSchedule);			///<모든 스케줄의 리스트를 구한다.

	UINT	GetSysShortcutKey(void);						///<설정되어있는 System shortcut(ALT, CTRL) key 값을 반환
	UINT	MakeShortcutKeyStringToInt(CString strString);	///<문자열로 되어있는 shortcut key를 정의된 정수형의 key값으로 변경하는함수
	void	MakeShortcutKey(void);							///<문자열로 되어있는 shortcut key를 파싱하여 정수형의 shortcut key와 system key(ALT, CTRL)로 분리한다

	bool	HasFrame(CString strFrameID, CFrame*& pFrame);	///<Frame Array에 해당하는 ID의 Frame이 있는지여부
	bool	HasHDFrame(void);								///<HD Frame이 존재하는지 여부를 반환.
	void	PlayClickSchedule(CString strScheduleId);		///<부모 스케줄의 Click 이벤트를 받아 설정된 자식 스케줄을 재생한다.

protected:
	BOOL	WriteCurrentPlayInfo();

	// <!-- 골프장전용 (2016.08.08)
	CString	m_strSyncCommand;
	CString	m_strSyncProcess;
	static bool	m_bTopmost;
	ciULong	createProcess(const char *exe, const char* dir, WORD wShowWindow=SW_MINIMIZE);
	void	openSyncProcess(bool pre_run=false);
	void	sendTemplateShortcut();
	void	showSyncProcess();
	void	hideSyncProcess();

	static	CString		m_strLastPlayTemplateShortCut;
	int		m_nTemplateIndex;
public:
	bool	IsScheduleResumingTemplate() { return (stricmp(m_strSyncCommand,"resume")==0); };
	bool	IsRemainPlayingDefaultSchedule();
	static	LPCSTR	GetLastPlayTemplateShortCut() { return m_strLastPlayTemplateShortCut; };

	int		GetTemplateIndex();
	LPCSTR	GetSyncCommand() { return m_strSyncCommand; };

	void	killSyncProcess();
	// -->
};


typedef CArray<CTemplate*, CTemplate*> CTemplateArray;
typedef CMapStringToPtr		CTemplateMap;


