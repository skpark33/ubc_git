// VideoControllerWnd.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "VideoControllerWnd.h"

#include "Schedule.h"


#define		TIMER_CHECK_ID			(1023)
#define		TIMER_CHECK_TIME		(500)	// 1 sec

#define		TIMER_MINIMIZE_ID		(1022)
#define		TIMER_MINIMIZE_TIME		(10*1000)	// 10 sec


// CVideoControllerWnd

IMPLEMENT_DYNAMIC(CVideoControllerWnd, CWnd)

CVideoControllerWnd::CVideoControllerWnd()
{
	m_pVideoSchedule = NULL;

	m_fTotalTime   = 0.0f;
	m_fCurrentTime = 0.0f;

	m_fCurrentPos = 0.0f;

	m_bFocusCaptured = false;
}

CVideoControllerWnd::~CVideoControllerWnd()
{
}


BEGIN_MESSAGE_MAP(CVideoControllerWnd, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CVideoControllerWnd 메시지 처리기입니다.


BOOL CVideoControllerWnd::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	//if(WM_LBUTTONDOWN == pMsg->message)
	//{
	//	SetTimer(TIMER_MINIMIZE_ID, TIMER_MINIMIZE_TIME, NULL);
	//}

	return CWnd::PreTranslateMessage(pMsg);
}

int CVideoControllerWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	//
	//CreateRegion();

	return 0;
}

void CVideoControllerWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
	if( m_bFocusCaptured == false )
	{
		if( m_rectBody.PtInRect(point) )
		{
			KillTimer(TIMER_MINIMIZE_ID);

			SetCapture();
			m_bFocusCaptured = true;

			if( m_pVideoSchedule != NULL && m_fTotalTime > 0.0f )
			{
				CRect rect;
				GetClientRect(rect);

				m_fCurrentPos = (double)(point.x-m_rectBody.left) * m_fTotalTime / (double)m_rectBody.Width();
				if( m_fCurrentPos > m_fTotalTime-1.0f ) m_fCurrentPos = m_fTotalTime - 1.0f;
				if( m_fCurrentPos < 0.0f ) m_fCurrentPos = 0.0f;

				m_pVideoSchedule->Pause();//SetCurrentPlayTime(m_fCurrentPos);
			}
		}
		else if( m_rectButton.PtInRect(point) )
		{
			if( m_pVideoSchedule )
			{
				if( m_pVideoSchedule->IsPause() )
					m_pVideoSchedule->Play();
				else
					m_pVideoSchedule->Pause();

				Invalidate(FALSE);
			}
		}
	}

	CWnd::OnLButtonDown(nFlags, point);
}

void CVideoControllerWnd::OnLButtonUp(UINT nFlags, CPoint point)
{
	if( m_bFocusCaptured )
	{
		SetTimer(TIMER_MINIMIZE_ID, TIMER_MINIMIZE_TIME, NULL);

		::ReleaseCapture();
		m_bFocusCaptured = false;

		m_pVideoSchedule->SetCurrentPlayTime(m_fCurrentPos);
		m_pVideoSchedule->Play();

		Invalidate(FALSE);
	}

	CWnd::OnLButtonUp(nFlags, point);
}

void CVideoControllerWnd::OnMouseMove(UINT nFlags, CPoint point)
{
	if( m_bFocusCaptured )
	{
		m_fCurrentPos = (double)(point.x-m_rectBody.left) * m_fTotalTime / (double)m_rectBody.Width();
		if( m_fCurrentPos > m_fTotalTime-1.0f ) m_fCurrentPos = m_fTotalTime - 1.0f;
		if( m_fCurrentPos < 0.0f ) m_fCurrentPos = 0.0f;

		m_pVideoSchedule->SetCurrentPlayTime(m_fCurrentPos);

		Invalidate(FALSE);
	}

	CWnd::OnMouseMove(nFlags, point);
}

void CVideoControllerWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	if( m_fTotalTime == 0.0f ) return;

	int pos_x = 0;
	if( m_bFocusCaptured )
		pos_x = (int)((double)m_rectBody.Width() * m_fCurrentPos / m_fTotalTime);
	else
		pos_x = (int)((double)m_rectBody.Width() * m_fCurrentTime / m_fTotalTime);

	CRect rect_comp = m_rectBody;
	CRect rect_incomp = m_rectBody;

	rect_comp.right = rect_incomp.left = rect_comp.left + pos_x;

	CDC memdc;
	memdc.CreateCompatibleDC(&dc);

	if( m_pVideoSchedule )
	{
		if( m_pVideoSchedule->IsPause() && !m_bFocusCaptured )
			memdc.SelectObject(&m_bmpPlay);
		else
			memdc.SelectObject(&m_bmpPause);
		dc.BitBlt(m_rectButton.left,m_rectButton.top,m_rectButton.Width(),m_rectButton.Height(), &memdc, 0,0, SRCCOPY);
	}

	memdc.SelectObject(&m_bmpProgressHead);
	dc.BitBlt(m_rectHead.left,m_rectHead.top,m_rectHead.Width(),m_rectHead.Height(), &memdc, 0,0, SRCCOPY);

	memdc.SelectObject(&m_bmpProgressComplete);
	dc.StretchBlt(rect_comp.left,rect_comp.top,rect_comp.Width(),rect_comp.Height(), &memdc, 0,0,1,rect_comp.Height(), SRCCOPY);

	memdc.SelectObject(&m_bmpProgressIncomplete);
	dc.StretchBlt(rect_incomp.left,rect_incomp.top,rect_incomp.Width(),rect_incomp.Height(), &memdc, 0,0,1,rect_incomp.Height(), SRCCOPY);

	memdc.SelectObject(&m_bmpProgressTail);
	dc.BitBlt(m_rectTail.left,m_rectTail.top,m_rectTail.Width(),m_rectTail.Height(), &memdc, 0,0, SRCCOPY);

//	CPoint pt = m_rectBody.CenterPoint();
//	m_imgCurrentMark.Draw(dc.GetSafeHdc(), rect_comp.right-m_imgCurrentMark.GetWidth()/2, pt.y-m_imgCurrentMark.GetHeight()/2);

	CPoint pt = m_rectBody.CenterPoint();
	memdc.SelectObject(&m_bmpTimelineMask);
	dc.BitBlt(rect_comp.right-m_sizeTimeline.cx/2,pt.y-m_sizeTimeline.cy/2,m_sizeTimeline.cx,m_sizeTimeline.cy, &memdc, 0,0, SRCPAINT);
	memdc.SelectObject(&m_bmpTimelineCore);
	dc.BitBlt(rect_comp.right-m_sizeTimeline.cx/2,pt.y-m_sizeTimeline.cy/2,m_sizeTimeline.cx,m_sizeTimeline.cy, &memdc, 0,0, SRCAND);

}

void CVideoControllerWnd::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case TIMER_CHECK_ID:
		KillTimer(TIMER_CHECK_ID);

		GetVideoTimeInfo();
		Invalidate(FALSE);

		SetTimer(TIMER_CHECK_ID, TIMER_CHECK_TIME, NULL);
		break;

	case TIMER_MINIMIZE_ID:
		Minimize();
		break;
	}

	CWnd::OnTimer(nIDEvent);
}

BOOL CVideoControllerWnd::CreateRegion()
{
	if( !GetSafeHwnd() ) return FALSE;

	CRect rect;
	GetClientRect(rect);
	int width = rect.Width();
	int height = rect.Height();

	//CPoint ptVertex[24];
	//ptVertex[0].x = 0;			ptVertex[0].y = 11;
	//ptVertex[1].x = 1;			ptVertex[1].y = 9;
	//ptVertex[2].x = 2;			ptVertex[2].y = 7;
	//ptVertex[3].x = 7;			ptVertex[3].y = 2;
	//ptVertex[4].x = 9;			ptVertex[4].y = 1;
	//ptVertex[5].x = 11;			ptVertex[5].y = 0;
	//ptVertex[6].x = width-11;	ptVertex[6].y = 0;
	//ptVertex[7].x = width-9;	ptVertex[7].y = 1;
	//ptVertex[8].x = width-7;	ptVertex[8].y = 2;
	//ptVertex[9].x = width-2;	ptVertex[9].y = 7;
	//ptVertex[10].x = width-1;	ptVertex[10].y = 9;
	//ptVertex[11].x = width;		ptVertex[11].y = 12;
	//ptVertex[12].x = width;		ptVertex[12].y = 17;
	//ptVertex[13].x = width-1;	ptVertex[13].y = 20;
	//ptVertex[14].x = width-2;	ptVertex[14].y = 22;
	//ptVertex[15].x = width-7;	ptVertex[15].y = 27;
	//ptVertex[16].x = width-9;	ptVertex[16].y = 28;
	//ptVertex[17].x = width-12;	ptVertex[17].y = 29;
	//ptVertex[18].x = 12;		ptVertex[18].y = 29;
	//ptVertex[19].x = 9;			ptVertex[19].y = 28;
	//ptVertex[20].x = 7;			ptVertex[20].y = 27;
	//ptVertex[21].x = 2;			ptVertex[21].y = 22;
	//ptVertex[22].x = 1;			ptVertex[22].y = 20;
	//ptVertex[23].x = 0;			ptVertex[23].y = 18;

	CPoint ptVertex[28];
	ptVertex[ 0].x=0;		ptVertex[ 0].y=7;
	ptVertex[ 1].x=1;		ptVertex[ 1].y=5;
	ptVertex[ 2].x=2;		ptVertex[ 2].y=4;
	ptVertex[ 3].x=3;		ptVertex[ 3].y=3;
	ptVertex[ 4].x=4;		ptVertex[ 4].y=2;
	ptVertex[ 5].x=5;		ptVertex[ 5].y=1;
	ptVertex[ 6].x=8;		ptVertex[ 6].y=0;

	ptVertex[ 7].x=width-8;	ptVertex[ 7].y=0;
	ptVertex[ 8].x=width-6;	ptVertex[ 8].y=1;
	ptVertex[ 9].x=width-4;	ptVertex[ 9].y=2;
	ptVertex[10].x=width-3;	ptVertex[10].y=3;
	ptVertex[11].x=width-2;	ptVertex[11].y=4;
	ptVertex[12].x=width-1;	ptVertex[12].y=6;
	ptVertex[13].x=width;	ptVertex[13].y=7;

	ptVertex[14].x=width;	ptVertex[14].y=height-10;//-8;
	ptVertex[15].x=width-1;	ptVertex[15].y=height-8;//-6;
	ptVertex[16].x=width-2;	ptVertex[16].y=height-6;//-4;
	ptVertex[17].x=width-3;	ptVertex[17].y=height-4;//-3;
	ptVertex[18].x=width-4;	ptVertex[18].y=height-3;//-2;
	ptVertex[19].x=width-5;	ptVertex[19].y=height-2;//-1;
	ptVertex[20].x=width-11;ptVertex[20].y=height;

	ptVertex[21].x=8;		ptVertex[21].y=height;
	ptVertex[22].x=5;		ptVertex[22].y=height-1;
	ptVertex[23].x=4;		ptVertex[23].y=height-2;
	ptVertex[24].x=3;		ptVertex[24].y=height-3;
	ptVertex[25].x=2;		ptVertex[25].y=height-4;
	ptVertex[26].x=1;		ptVertex[26].y=height-6;
	ptVertex[27].x=0;		ptVertex[27].y=height-8;

	m_rgn.CreatePolygonRgn(ptVertex, 28, ALTERNATE);

	return SetWindowRgn(m_rgn, TRUE);
}

void CVideoControllerWnd::GetVideoTimeInfo()
{
	if( m_pVideoSchedule == NULL ) return;

	m_fTotalTime   = m_pVideoSchedule->GetTotalPlayTime();
	m_fCurrentTime = m_pVideoSchedule->GetCurrentPlayTime();
}

BOOL CVideoControllerWnd::Create(CVideoSchedule* pParentWnd)
{
	if( pParentWnd == NULL ) return FALSE;
	if( !pParentWnd->GetSafeHwnd() ) return FALSE;

	//
	m_bmpPlay.LoadBitmap(IDB_VIDEO_PBAR_PLAY);
	m_bmpPause.LoadBitmap(IDB_VIDEO_PBAR_PAUSE);

	BITMAP bmp;
	m_bmpPlay.GetObject(sizeof(bmp), &bmp);
	CSize size_button(bmp.bmWidth, bmp.bmHeight);

	//
	m_bmpProgressHead.LoadBitmap(IDB_VIDEO_PBAR_HEAD);

	m_bmpProgressHead.GetObject(sizeof(bmp), &bmp);
	CSize size_head(bmp.bmWidth, bmp.bmHeight);

	//
	m_bmpProgressTail.LoadBitmap(IDB_VIDEO_PBAR_TAIL);

	m_bmpProgressTail.GetObject(sizeof(bmp), &bmp);
	CSize size_tail(bmp.bmWidth, bmp.bmHeight);

	//
	m_bmpProgressComplete.LoadBitmap(IDB_VIDEO_PBAR_COMPLETE);

	m_bmpProgressComplete.GetObject(sizeof(bmp), &bmp);
	CSize size_comp(bmp.bmWidth, bmp.bmHeight);

	//
	m_bmpProgressIncomplete.LoadBitmap(IDB_VIDEO_PBAR_INCOMPLETE);

	m_bmpProgressIncomplete.GetObject(sizeof(bmp), &bmp);
	CSize size_incomp(bmp.bmWidth, bmp.bmHeight);

	//
	LoadPNG(m_imgCurrentMark, IDR_CURRENT_MARK);
	m_bmpTimelineMask.LoadBitmap(IDB_VIDEO_TIMELINE_MASK);
	m_bmpTimelineCore.LoadBitmap(IDB_VIDEO_TIMELINE_CORE);

	m_bmpTimelineMask.GetObject(sizeof(bmp), &bmp);
	m_sizeTimeline.SetSize(bmp.bmWidth, bmp.bmHeight);

	//
	m_pVideoSchedule = pParentWnd;

	CRect parent_rect;
	pParentWnd->GetWindowRect(parent_rect);

	CRect client_rect;
	client_rect.left   = parent_rect.left  + (parent_rect.Width()*20)/100; // left_gap=20%
	client_rect.right  = parent_rect.right - (parent_rect.Width()*20)/100; // right_gap=20%
	client_rect.bottom = parent_rect.bottom - (parent_rect.Height()*34)/1000; // bottom_gap=3.4%
	client_rect.top    = client_rect.bottom - size_button.cy;

	//
	CRect rect = client_rect;
	rect.MoveToXY(0,0);

	m_rectButton = rect;
	m_rectButton.right = m_rectButton.left + size_button.cx;

	m_rectHead = rect;
	m_rectHead.left = m_rectButton.right;
	m_rectHead.right = m_rectHead.left + size_head.cx;

	m_rectTail = rect;
	m_rectTail.left = m_rectTail.right - size_tail.cx;

	m_rectBody = rect;
	m_rectBody.left = m_rectHead.right;
	m_rectBody.right = m_rectTail.left;

	//
	return CreateEx( NULL, NULL, _T("Controller"), WS_POPUP/*|WS_CAPTION|WS_VISIBLE*/, client_rect, m_pVideoSchedule, NULL );
}

BOOL CVideoControllerWnd::Popup()
{
	if( !GetSafeHwnd() ) return FALSE;

	GetVideoTimeInfo();

	ShowWindow(SW_SHOW);

	SetTimer(TIMER_CHECK_ID, TIMER_CHECK_TIME, NULL);

	SetTimer(TIMER_MINIMIZE_ID, TIMER_MINIMIZE_TIME, NULL);

	return TRUE;
}

BOOL CVideoControllerWnd::Minimize()
{
	if( !GetSafeHwnd() ) return FALSE;

	KillTimer(TIMER_CHECK_ID);
	KillTimer(TIMER_MINIMIZE_ID);

	ShowWindow(SW_HIDE);

	return TRUE;
}

BOOL CVideoControllerWnd::LoadPNG(CImage& imgPNG, UINT nIDResource)
{
	HMODULE hInst = NULL;
	HRSRC hResource = ::FindResource(hInst, MAKEINTRESOURCE(nIDResource), _T("PNG"));
	if( hResource == NULL ) return FALSE;

	DWORD img_size = ::SizeofResource(hInst, hResource);
	if( img_size == 0 ) return FALSE;

	const void* pResourceData = ::LockResource(::LoadResource(hInst, hResource));
	if( pResourceData == NULL ) return FALSE;

	HGLOBAL buf = ::GlobalAlloc(GMEM_MOVEABLE, img_size);
	if( buf == NULL ) return FALSE;

	void* buf_lock = ::GlobalLock(buf);
	if( buf_lock == NULL )
	{
		::GlobalFree(buf);
		return FALSE;
	}

	::CopyMemory(buf_lock, pResourceData, img_size);
	HRESULT hr = NULL;
	IStream* pStream = NULL;
	if( ::CreateStreamOnHGlobal(buf, FALSE, &pStream) == S_OK )
	{
		hr = imgPNG.Load(pStream);
	}

	::GlobalUnlock(buf);          
	::GlobalFree(buf);

	return TRUE; 
}
