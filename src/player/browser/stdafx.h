// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.
#include <afxtempl.h>        // MFC 자동화 클래스입니다.
#include <afxinet.h>
#include <afxmt.h>
#include <Mshtml.h>


#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

static const GUID CLSID_EnhancedVideoRenderer = {0xfa10746c, 0x9b63, 0x4b6c, {0xbc, 0x49, 0xfc, 0x30,  0xe, 0xa5, 0xf2, 0x56} };
static const GUID MR_VIDEO_RENDER_SERVICE =     {0x1092a86c, 0xab1a, 0x459a, {0xa3, 0x36, 0x83, 0x1f, 0xbc, 0x4d, 0x11, 0xff} };

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

//#import "C:\\WINDOWS\\SysWOW64\\Macromed\\Flash\\Flash11e.ocx" named_guids
//#import "C:\\WINDOWS\\system32\\Macromed\\Flash\\Flash10l.ocx" named_guids
//using namespace ShockwaveFlashObjects;
//#include <atlctl.h>


#ifdef _VBROKER
#include <corba.h>
#endif
#include "ace/ace.h"
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciAny.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>
#include <cci/libWrapper/cciEvent.h>
#include "common/libCommon/utvMacro.h"
#include "ci/libDebug/ciArgParser.h"
#include "scratchUtil.h"

#define		DEBUG_MSG_LENGTH		8192

#define		WM_DEBUG_STRING					 (WM_USER+98)
#define		WM_DEBUG_GRID					 (WM_USER+99)

#define		WM_DSINTERFACES_GRAPHNOTIFY		 (WM_USER+100)
#define		WM_DSINTERFACES_COMPLETEPLAY	 (WM_USER+101)
//#define		WM_PROCESS_EVENT				(WM_USER+101)
//#define		WM_SCHEDULE_UPDATE				(WM_USER+102)
//#define		WM_DEFAULT_SCHEDULE_UPDATE		(WM_USER+103)
//#define		WM_PHONE_SCHEDULE_UPDATE		(WM_USER+104)
//#define		WM_PHONE_REJECT					(WM_USER+105)
//#define		WM_PHONE_COMPLETE				(WM_USER+106)
#define		WM_DEFAULT_TEMPLATE_UPDATE		 (WM_USER+107)
//#define		WM_FILE_UPDATE					(WM_USER+108)
#define		WM_ERROR_MESSAGE				 (WM_USER+109)
//#define		WM_COMPLETE_FTP_CONNECTION		 (WM_USER+110)
//#define		WM_COMPLETE_FILE_SIZE			 (WM_USER+111)
//#define		WM_COMPLETE_FTP_DOWNLOAD		 (WM_USER+112)
#define		WM_CONTENTS_UPDATE				 (WM_USER+113)
#define		WM_PLAY_PREV_TEMPLATE			 (WM_USER+114)
#define		WM_PLAY_NEXT_TEMPLATE			 (WM_USER+115)
#define		WM_PLAY_PREV_DEFAULT_TEMPLATE	 (WM_USER+116)
#define		WM_PLAY_NEXT_DEFAULT_TEMPLATE	 (WM_USER+117)
#define		WM_VIEW_MOUSE_CURSOR			 (WM_USER+118)
#define		WM_TOGGLE_SOUND_VOLUME_MUTE		 (WM_USER+119)		//Grade가 1인 컨텐츠의 사운드를 mute(toggle)하는 메시지	
#define		WM_HIDE_CONTENTS				 (WM_USER+120)
#define		WM_SET_POS						 (WM_USER+121)		//초기화 과정에 프로그래시브 바 위치 설정 메시지
#define		WM_SET_LOG						 (WM_USER+122)		//초기화 과정에 Log 설정 메시지
#define		WM_SET_ERROR_MODE				 (WM_USER+123)		//초기화 과정에 장애모드 설정
	
#define		WM_ANNOUNCE_SHIFT				 (WM_USER+132)		//공지의 움직임 메시지
#define		WM_HOOK_MOUSE					 (WM_USER+133)		//마우스 hook 이벤트 메시지
#define		WM_BRW_PAUSE					 (WM_USER+134)		//BRW pause 종료 메시지
#define		WM_SEND_STOP_BRW				 (WM_USER+135)		//BRW 종료 메시지를 보내는 함수 호출
//#define	WM_COMPLETE_INIT_DOWNLOAD_RESULT (WM_USER+136)		//다운로드 초기화 완료 메시지
#define		WM_PROGRESS_TAR					 (WM_USER+137)		//tar 파일을 해제하는 진행율을 알리는 메시지
#define		WM_COMPLETE_TAR					 (WM_USER+138)		//tar 파일을 해제 완료를 알리는 메시지
//#define		WM_FLASH_RECV_FSCOMMAND			 (WM_USER+139)		//플래쉬컨트롤에서 fscommand를 받았음을 알리는 메시지
#define		WM_ENDRUN_IE					 (WM_USER+140)		//시스템에 IE 프로그램의 사용이 종료되었음을 알리는 메시지


enum  CONTENTS_TYPE {
	CONTENTS_NOT_DEFINE = -1,
	CONTENTS_VIDEO = 0,
	CONTENTS_SMS,			//1. Horizontal Ticker
	CONTENTS_IMAGE,
	CONTENTS_PROMOTION,
	CONTENTS_TV,
	CONTENTS_TICKER,		//5. Vertical Ticker
	CONTENTS_PHONE,
	CONTENTS_WEBBROWSER,
	CONTENTS_FLASH,
	CONTENTS_WEBCAM,
	CONTENTS_RSS,			// 10.
	CONTENTS_CLOCK,
	CONTENTS_TEXT,			// 12. 이전 SMS
	CONTENTS_FLASH_TEMPLATE,
	CONTENTS_DYNAMIC_FLASH,
	CONTENTS_TYPING,
	CONTENTS_PPT,			//16
	CONTENTS_ETC,		// 2010.10.12 장광수 기타컨텐츠추가
	CONTENTS_WIZARD,	// 2010.10.21 정운형 마법사컨텐츠추가
	CONTENTS_FILE		//플래쉬 부속파일
	//CONTENTS_HWP,
	//CONTENTS_EXCEL
};
/*
enum E_VIDEO_RENDER_TYPE
{ 
	E_DUMMY_OVERLAY = 1,	//Dummy Overlay 모드
	E_WINDOWLESS,			//Windowless 모드
	E_WINDOWED,				//Windowed 모드
	E_EVR,
	E_OVERLAY = 9			//Overlay 모드
};

enum E_OS_VERSION
{
	E_OS_WINXP = 5,
	E_OS_WIN7
};
*/

enum E_VIDEO_OPEN_TYPE
{
	E_OPEN_INIT = 1,		//초기화 과정에 open
	E_OPEN_PLAY				//플레이시기에 매번 open
};

enum E_TICKER_SPEED
{
	E_TICKER_FAST = 1001,
	E_TICKER_LITTLE_FAST,
	E_TICKER_NORMAL,
	E_TICKER_LITTLE_SLOW,
	E_TICKET_SLOW
};

enum E_RSS_TYPE
{
	E_RSS_ONE_LINE = 0,		//단일 라인 RSS
	E_RSS_MULTI_LINE,		//Multi line RSS
	E_RSS_DETAIL			//내용이 있는 RSS
};

bool		DONT_IGNORE_SCHEDULE_ENDDATE();
LPCTSTR		GetAppPath();
LPCTSTR		GetSchedulePath();
LPCTSTR		GetSiteName();
LPCTSTR		GetSiteID();				///<UBCVariables.ini에 있는 SiteID를 반환
LPCTSTR		GetProgramSite();			///<+site의 값을 반환
LPCTSTR		GetHostName();
LPCTSTR		GetAppId();
bool		GetPhoneConfig(CString& PhoneTemplateId, CString& PhoneFrameId, int& PortNo);
bool		GetLocalPath(CString& LocalFullPath, CString& LocalTempFullPath, LPCSTR lpszLocation, LPCSTR lpszFilename);
COLORREF	GetColorFromString(LPCSTR lpszColor);
CString		GetColorFromString(COLORREF rgb);
CTime		GetHostTime();
CString		FindExtension(const CString& strFileName);		///<파일명에서 확장자를 분리하여 반환
CString		GetEdition(void);								///<제품의 edition 문자열 반환
LPCSTR		GetAppName(void);								///<실행프로그램의 파일명(extention 제외)을 반환
int			GetBRWId(void);									///<BRW의 아이디(0, 1)
int			GetVidoeRenderType(void);						///<사용할 Vidoe render 종류를 반환(1: Overlay, 2:VMR7,....)
int			GetVidoeOpenType(void);							///<사용할 Vidoe open하는 방법을 반환(1: 초기화과정에 open, 2:매번 플레이시에 open)
int			GetVideoContentsSharing(void);					///<비디오 컨텐츠를 한번만 Open. 동일컨텐츠일 경우 재사용. Video의 경우 GetVidoeOpenType이 1일경우에만 동작함. (1:재사용, 0:재사용안함(기본))
int			GetFlashContentsSharing(void);					///<플래시 컨텐츠를 한번만 Open. 동일컨텐츠일 경우 재사용. (1:재사용, 0:재사용안함(기본))
int			GetShowVideoController(void);					///<Video재생시 터치할 경우 컨트롤 윈도우 표시여부. (1:표시, 0:표시안함(기본))
int			GetPlayOneTimesTimebaseSchedule(void);			///<정시컨텐츠를 한번만 재생. Replay하지 않음 (1:한번만재생, 0:계속다시재생)
//int			GetUseOverlayMixer(void);					///<OverlayMixer필터를 사용할지 여부를 반환
bool		IsSoundOnSecondaryFrame();						///세컨더리 프레임에서 소리 재생
int			GetShowWebScrollbar(void);						///<웹 컨텐츠에서 스크롤바를 표시 (1:표시, 0:표시안함(기본))
int			GetScheduleResuming(void);						///<템플릿 전환되어도 스케줄을 이어서 재생 (1:사용, 0:사용안함(기본))
int			GetPreOpenSyncProcess(void);					///<동기화프로세스 미리 실행 (1:사용, 0:사용안함(기본))
CString		GetSyncProcessOpenType(void);					///<동기화프로세스 열기 타입 문자열 반환 (MINIMIZE, MAXIMIZE, NORMAL(기본))
CString		GetSyncProcessCloseType(void);					///<동기화프로세스 닫기 타입 문자열 반환 (NOTHING, EXIT, MINIMIZE(기본))
CString		GetSyncProcessExitType(void);					///<동기화프로세스 종료 타입 문자열 반환 (NOTHING(기본), EXIT, MINIMIZE)
int			GetSendSyncProcessShortcut(void);				///<현재 재생중인 템플릿 단축기를 14007포트로 전송 (1:사용, 0:사용안함(기본))
int			GetTemplateIndexInUserlog(void);				///<UserLog에 템플릿인덱스포함 (1:사용, 0:사용안함(기본))
int			GetLicenseCheckDelay(void);						///<라이센스(하단 바) 체크 딜레이 (n:사용(초단위), 0:사용안함(기본))

CString		ToString(int nValue);
CString		ToString(double fValue);
CString		ToString(ULONG ulValue);
CString		ToString(ULONGLONG ulvalue);

CString		VariantToString(VARIANT* var);					///<VARIANT를 문자열로 변환한다.
void		WritePlayLog(CString strContentsId,
						 CString strContentsName,
						 CString strAction,
						 long lRunningTime,
						 CString strFileName="",
						 CString strPrgId="",
						 CString strDesc="",
						 int nTemplateIndex=0);				///<컨텐츠가 방송되는 log를 report용으로 기록한다.
void		WriteInterActLog(CString strContentsId,
						 CString strKeyword,
						 CString strPrgId="");				///<플래쉬의 상호작용 log를 report용으로 기록한다.
void		WriteAnnounceLog(CString strContentsId,
						 CString strContentsName,
						 CString strAction,
						 long lRunningTime,
						 CString strFileName="",
						 CString strPrgId="",
						 CString strDesc="");				///<긴급공지가 방송되는 log를 report용으로 기록한다.
LPCSTR		GetCoutomerInfo(void);							///<Customer Info를 반환한다.
bool		IsAvailableFont(CString strFontName);			///<시스템에 지정된 폰트가 있는지를 반환
CString		GetINIValue(CString strFileName,
						CString strSection,
						CString strKey);					///<지정된 ini 파일의 값을 반환한다.
void		WriteINIValue(CString strFileName,
						CString strSection,
						CString strKey,
						CString strValue);					///<지정된 ini 파일의 key에 값을 쓴다.
bool		IsUseTimeBaseSchedule(void);					///<정시스케줄을 사용할지 여부를 반환
bool		IsFontFile(CString strFileName);				///<폰트파일인지 구분하여 준다.
bool		IsPptFile(CString strFileName);					///<PPT파일인지 구분하여 준다.
bool		IsTarFile(CString strFileName);					///<tar파일인지 구분하여 준다.
bool		RegisterFont(CString strFontName,
						 CString strFontPath);				///<폰트를 시스템이 등록한다.
bool		CreatePathDirectory(LPCTSTR lpszPath);			///<주어진 경로의 폴더를 만든다
ULONGLONG	GetFolderSize(CString strInPath);				///<폴더의 안의 파일들 사이즈를 구한다.


#include "LOG.h"
#include "LogDialog.h"
#include "PropertyGrid.h"
#include "UserLog.h"
#include "InterActLog.h"

typedef struct {
	ciString	siteId;
	ciString	hostId;
	ciString	templateId;
	ciString	frameId;
	ciShort		grade;
	ciShort		width;
	ciShort		height;
	ciShort		x;
	ciShort		y;
	ciBoolean	isPIP;
	ciString	borderStyle;
	ciShort		borderThickness;
	ciString	borderColor;
	ciShort		cornerRadius;

	ciString	pip_frameId;
	ciShort		pip_width;
	ciShort		pip_height;
	ciShort		pip_x;
	ciShort		pip_y;

	COLORREF	rgbBorderColor;

	ciString	contentsId;
	ciLong		contentsType;
	ciString	location;
	ciString	filename;
} FRAME_SCHEDULE_INFO;

typedef		CArray<FRAME_SCHEDULE_INFO, FRAME_SCHEDULE_INFO&>		FRAME_SCHEDULE_INFO_LIST;

#define		USE_AUTO_MOUSE_CURSOR_HIDE	1
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define BUF_SIZE			(1024*100)
#define	TICKER_COUNT		(10)

#define CUSTOMER_NARSHA		("NARSHA")		//Narsha

//#define _USE_VMR							//동영상 재생을 VMR을 쓰는지를 결정하는 정의					

#define	ID_ANNOUNCE_LOG		("NOTICE")		//긴급공지 로그 ID 문자열

#ifdef _FELICA_
	#include <libFelicaUtil/felicaUtil.h>
#endif

/////////////////////////////////////////////////////////////////////////////////
//5초간격으로 펌웨어뷰에게 보내는 상태 보고에서 
//브라우져에 오류가 있는경우에 보내는 메시지
#define	STAT_MSG_PACKAGE_DOWNLOAD		("Fail to download contents package file")
#define	STAT_MSG_PACKAGE_READ			("Fail to read contents package file")
#define	STAT_MSG_PACKAGE_DATA			("Contents package file is incorrect")
#define	STAT_MSG_DOWNLOAD_CANCEL		("Contents download was canceled")
#define	STAT_MSG_DOWNLOAD_FAIL			("Fail to contents file download")
#define STAT_MSG_CONTENTS_UNAVAILABLE	("All contents file is unavailable to play")
#define STAT_MSG_CONTENTS_EXPIRED		("All contents file has been expired")


