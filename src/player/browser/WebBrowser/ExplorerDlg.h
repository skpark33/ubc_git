#pragma once

#include "web_browser.h"


// CExplorerDlg 대화 상자입니다.

interface INavigateCallback {
public:
	virtual		void NavigateError(bool bError, int nErrorCode) = 0;

};


class CExplorerDlg : public CDialog
{
	DECLARE_DYNAMIC(CExplorerDlg)

public:
	CExplorerDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CExplorerDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_WEB_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	DECLARE_MESSAGE_MAP()
	DECLARE_EVENTSINK_MAP()

	void NewWindow2(LPDISPATCH* ppDisp, BOOL* Cancel);
	void NavigateComplete2(LPDISPATCH pDisp, VARIANT* URL);
	void DocumentComplete(LPDISPATCH pDisp, VARIANT* URL);
	virtual void NavigateError(LPDISPATCH pDisp, VARIANT* URL, VARIANT* Frame, VARIANT* StatusCode, BOOL* Cancel);
	void DownloadComplete();

	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	void Navigate(CString strURL);		///<주어진 URL로 이동한다.

	void	setNavigateCallback(INavigateCallback*	p) { m_callback = p;}
private:
	CWeb_browser	m_ocxExplorer;		///<IE ocx	

	INavigateCallback*	m_callback; // 결과를 어느곳에 전달하고자 할때...셋팅해서 사용함.

	void HideBodyScrollbar();
};

