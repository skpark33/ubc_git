// ExplorerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "ExplorerDlg.h"
#include "MemDC.h"
#include "Schedule.h"


// CExplorerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CExplorerDlg, CDialog)

CExplorerDlg::CExplorerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExplorerDlg::IDD, pParent)
	,m_callback(NULL)
{

}

CExplorerDlg::~CExplorerDlg()
{
	__DEBUG__("~CExplorerDlg()",0);
	__DEBUG__("~CExplorerDlg()",1);
}

void CExplorerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXPLORER_OCX, m_ocxExplorer);
}


BEGIN_MESSAGE_MAP(CExplorerDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CExplorerDlg 메시지 처리기입니다.
BEGIN_EVENTSINK_MAP(CExplorerDlg, CDialog)
	ON_EVENT(CExplorerDlg, IDC_EXPLORER_OCX, 251, CExplorerDlg::NewWindow2, VTS_PDISPATCH VTS_PBOOL)
	ON_EVENT(CExplorerDlg, IDC_EXPLORER_OCX, 252, CExplorerDlg::NavigateComplete2, VTS_DISPATCH VTS_PVARIANT)
	ON_EVENT(CExplorerDlg, IDC_EXPLORER_OCX, 259, CExplorerDlg::DocumentComplete, VTS_DISPATCH VTS_PVARIANT)
	ON_EVENT(CExplorerDlg, IDC_EXPLORER_OCX, 271, CExplorerDlg::NavigateError, VTS_DISPATCH VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PBOOL)
	ON_EVENT(CExplorerDlg, IDC_EXPLORER_OCX, 104, CExplorerDlg::DownloadComplete, VTS_NONE)
END_EVENTSINK_MAP()

void CExplorerDlg::NewWindow2(LPDISPATCH* ppDisp, BOOL* Cancel)
{
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	__DEBUG__("New Window", _NULL);
}

void CExplorerDlg::NavigateComplete2(LPDISPATCH pDisp, VARIANT* URL)
{
	__DEBUG__("NavigateComplete2", _NULL);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_ocxExplorer.get_Application() == pDisp)
	{
		__DEBUG__("NavigateComplete2", _NULL);

		m_ocxExplorer.Invalidate(FALSE);
	}//if
}

void CExplorerDlg::DocumentComplete(LPDISPATCH pDisp, VARIANT* URL)
{
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	__DEBUG__("DocumentComplete", _NULL);

	if(m_ocxExplorer.get_Application() != pDisp)
	{
		return;
	}//if

	if( ::GetShowWebScrollbar() )
		m_ocxExplorer.EnableScrollBar(SB_BOTH, ESB_ENABLE_BOTH);
	else
		m_ocxExplorer.EnableScrollBar(SB_BOTH, ESB_DISABLE_BOTH);

	m_ocxExplorer.Invalidate(TRUE);
	SetTimer(2047, 100, NULL); // call repaint timer
}

void CExplorerDlg::NavigateError(LPDISPATCH pDisp, VARIANT* URL, VARIANT* Frame, VARIANT* StatusCode, BOOL* Cancel)
{
	__DEBUG__("NavigateError", _NULL);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_ocxExplorer.get_Application() == pDisp)
	{
		__DEBUG__("NavigateError code", StatusCode->lVal);
		if(m_callback==0 && m_pParentWnd)
		{
			CURLSchedule* pSchedule = (CURLSchedule*)m_pParentWnd;
			pSchedule->SetNavigateError(true, StatusCode->lVal);
			m_ocxExplorer.Stop();
			m_ocxExplorer.ShowWindow(SW_HIDE);
		}//if

		if(m_callback){
		__DEBUG__("NavigateError code", StatusCode->lVal);
			m_callback->NavigateError(true, StatusCode->lVal);
			m_ocxExplorer.Stop();
			m_ocxExplorer.ShowWindow(SW_HIDE);
		}
	}//if
}

void CExplorerDlg::OnSize(UINT nType, int cx, int cy)
{
	__DEBUG__("OnSize", _NULL);
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(GetSafeHwnd() && m_ocxExplorer.GetSafeHwnd())
	{
		CRect rtRect;
		GetClientRect(&rtRect);

		m_ocxExplorer.MoveWindow(0, 0, rtRect.Width(), rtRect.Height());
	}//if
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 URL로 이동한다. \n
/// @param (CString) strURL : (in) 이동할 URL
/////////////////////////////////////////////////////////////////////////////////
void CExplorerDlg::Navigate(CString strURL)
{
	__DEBUG__("Navigate", _NULL);
	m_ocxExplorer.put_Silent(TRUE);
	if(m_callback==0 && m_pParentWnd)
	{
		CURLSchedule* pSchedule = (CURLSchedule*)m_pParentWnd;
		pSchedule->SetNavigateError(false, 0l);
	}//if

	if(m_callback){
		m_callback->NavigateError(false, 0l);
	}

	ShowWindow(SW_SHOW);
	m_ocxExplorer.Navigate(strURL,  NULL, NULL, NULL, NULL);
}

BOOL CExplorerDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->message == WM_LBUTTONDOWN)
	{
		__DEBUG__("CFlashDlg::L-CLICK", _NULL);
		if(m_callback==0 && m_pParentWnd)
		{
			return m_pParentWnd->PreTranslateMessage(pMsg);
		}//if
	}
	else if((pMsg->message == WM_KEYDOWN) /*&& (pMsg->wParam == VK_ESCAPE)*/)
	{
	__DEBUG__("PreTranslateMessage", 1);
		/*
		if(m_pParentWnd)
		{
			return m_pParentWnd->PreTranslateMessage(pMsg);
		}//if
		*/
		AfxGetMainWnd()->PreTranslateMessage(pMsg);
		return TRUE;
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}

void CExplorerDlg::OnTimer(UINT_PTR nIDEvent)
{
	__DEBUG__("OnTimer", nIDEvent);

	if( nIDEvent == 2047 )
	{
		KillTimer(2047);
		//m_ocxExplorer.Invalidate(TRUE);
		CRect rtRect;
		GetClientRect(&rtRect);

		m_ocxExplorer.MoveWindow(0, 0, rtRect.Width()-1, rtRect.Height()-1, FALSE);
		m_ocxExplorer.MoveWindow(0, 0, rtRect.Width(), rtRect.Height());
	}

	CDialog::OnTimer(nIDEvent);
}

void CExplorerDlg::DownloadComplete()
{
	if( !::GetShowWebScrollbar() )
		HideBodyScrollbar();
}

void CExplorerDlg::HideBodyScrollbar()
{
	HRESULT hr;
	CComPtr<IDispatch> pDisp;
	pDisp = m_ocxExplorer.get_Document();
	if( pDisp==NULL ) return;

	CComPtr<IHTMLDocument2> pDoc;
	hr = pDisp->QueryInterface(IID_IHTMLDocument2, (LPVOID*)&pDoc);
	if( !SUCCEEDED(hr) || pDoc==NULL ) return;

	CComPtr<IHTMLElement> pElement;
	hr = pDoc->get_body(&pElement);
	if( !SUCCEEDED(hr) || pElement==NULL ) return;

	CComPtr<IHTMLBodyElement> pBody;
	hr = pElement->QueryInterface(IID_IHTMLBodyElement, (LPVOID*)&pBody);
	if( !SUCCEEDED(hr) || pBody==NULL ) return;
	pBody->put_scroll(CComBSTR("no"));

	// 위만 적용하면 안되는 경우가 많아서
	// body에 overflow:hidden 스타일속성까지 강제로 추가 (seventhstone 2016-01-15 )
	CComPtr<IHTMLStyle> pStyle; 
	hr = pElement->get_style(&pStyle);
	if( !SUCCEEDED(hr) || pStyle==NULL ) return;
	pStyle-> put_overflow(CComBSTR("hidden"));
}
