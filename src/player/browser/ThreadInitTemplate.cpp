// ThreadInitTemplate.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "ThreadInitTemplate.h"
#include "Host.h"


// CThreadInitTemplate

IMPLEMENT_DYNCREATE(CThreadInitTemplate, CWinThread)

CThreadInitTemplate::CThreadInitTemplate()
:	m_pParent(NULL)
,	m_pBackWnd(NULL)
,	m_bCancle(false)
{
}

CThreadInitTemplate::~CThreadInitTemplate()
{
}

BOOL CThreadInitTemplate::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CThreadInitTemplate::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CThreadInitTemplate, CWinThread)
END_MESSAGE_MAP()


// CThreadInitTemplate 메시지 처리기입니다.



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// thread의 부가정보 설정 \n
/// @param (void*) pParent : (in) thread 소유자 포인터
/// @param (void*) pBackWnd : (in) BackWnd 포인터
/////////////////////////////////////////////////////////////////////////////////
void CThreadInitTemplate::SetThreadParam(void* pParent, void* pBackWnd)
{
	m_pParent	= pParent;
	m_pBackWnd	= pBackWnd;
}


int CThreadInitTemplate::Run()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CHost* pHost		= (CHost*)m_pParent;
	CBackWnd* pBackWnd	= (CBackWnd*)m_pBackWnd;

	if(!pHost || !pHost->GetSafeHwnd())
	{
		pHost->PostMessage(WM_THREAD_INIT_TEPLATE, (WPARAM)false, 0);
	}//if

	__DEBUG_HOST_BEGIN__(_NULL)

	CString strMsg;
	CMapStringToString map_templateId;

	int nCallCount = 0;		//Call하는 과정중에 진행상태를 표시하는 값
	// 가져올 Template ID 리스트 생성

	pHost->m_scheduleList.clear();
	pHost->m_defaultscheduleList.clear();

	{
		// add schedule template id
		cciCall call("get");

		cciEntity aEntity("BM=*/Site=%s/Host=%s/ScheduleView=*", ::GetSiteName(), ::GetHostName());
		call.setEntity(aEntity);

		cciAny nullAny;
		call.addItem("attributes", nullAny);

		try
		{
			__DEBUG__("Get ScheduleView Call", call.toString());

			if(!call.call())
			{
				strMsg = "Fail to get ScheduleView Call !!!";
				pHost->PostMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strMsg, 0);
				__ERROR__(strMsg, _NULL);
				pBackWnd->PostMessage(WM_SET_ERROR_MODE, (WPARAM)(LPCSTR)strMsg);

				pHost->PostMessage(WM_THREAD_INIT_TEPLATE, (WPARAM)false, 0);
			}//if

			cciReply reply;
			while(call.getReply(reply))
			{
				//Progress set
				pBackWnd->PostMessage(WM_SET_POS, E_INIT_TEMPLATE, 1);
				nCallCount += 1;

				CString str = reply.toString();
				if(str.GetLength() < 10) continue;

				__DEBUG__("Get ScheduleView Reply", reply.toString());

				reply.unwrap();

				ciString templateId;
				if(reply.getItem("templateId", templateId))
				{
					__DEBUG__("Add TemplateID", templateId.c_str());
					map_templateId.SetAt(templateId.c_str(), templateId.c_str());

					pHost->m_bScheduleReply = true;
					pHost->m_scheduleList.addItem(reply);
				}//if
			}//while
		}
		catch(CORBA::Exception& ex)
		{
			strMsg = "Get ScheduleView Call Exception !!!";
			pHost->PostMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strMsg, 0);
			__ERROR__(strMsg, _NULL);
			pBackWnd->PostMessage(WM_SET_ERROR_MODE, (WPARAM)(LPCSTR)strMsg);

			pHost->PostMessage(WM_THREAD_INIT_TEPLATE, (WPARAM)false, 0);
		}//try
	}//schedule

	{
		// add default schedule template id
		cciCall call("get");

		cciEntity aEntity("BM=*/Site=%s/Host=%s/DefaultScheduleView=*", ::GetSiteName(), ::GetHostName());
		call.setEntity(aEntity);

		cciAny nullAny;
		call.addItem("attributes", nullAny);

		try
		{
			__DEBUG__("Get DefaultScheduleView Call", call.toString());

			if(!call.call())
			{
				strMsg = "Fail to get DefaultScheduleView Call !!!";
				pHost->PostMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strMsg, 0);
				__ERROR__(strMsg, _NULL);
				pBackWnd->PostMessage(WM_SET_ERROR_MODE, (WPARAM)(LPCSTR)strMsg);

				pHost->PostMessage(WM_THREAD_INIT_TEPLATE, (WPARAM)false, 0);
			}//if

			cciReply reply;
			while(call.getReply(reply))
			{
				//Progress set
				pBackWnd->PostMessage(WM_SET_POS, E_INIT_TEMPLATE, 1);
				nCallCount += 1;

				CString str = reply.toString();
				if(str.GetLength() < 10) continue;

				__DEBUG__("Get DefaultScheduleView Reply", reply.toString());

				reply.unwrap();

				ciString templateId;
				if(reply.getItem("templateId", templateId))
				{
					__DEBUG__("Add TemplateID", templateId.c_str());
					map_templateId.SetAt(templateId.c_str(), templateId.c_str());
					pHost->m_bScheduleReply = true;
					pHost->m_defaultscheduleList.addItem(reply);
				}//if
			}//while
		}
		catch(CORBA::Exception& ex)
		{
			strMsg = "Get DefaultScheduleView Call Exception !!!";
			pHost->PostMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strMsg, 0);
			__ERROR__(strMsg, _NULL);
			pBackWnd->PostMessage(WM_SET_ERROR_MODE, (WPARAM)(LPCSTR)strMsg);

			pHost->PostMessage(WM_THREAD_INIT_TEPLATE, (WPARAM)false, 0);
		}//try
	}// default schedule

	// Modified by 정운형 2008-11-07 오후 1:42
	// 변경내역 : Template play list는 존재하나, template가 없는경우 오류처리
	if(map_templateId.GetCount() == 0)
	{
		strMsg = "Empty template list !!!";
		pHost->PostMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strMsg, 0);
		__WARNING__(strMsg, _NULL);
		pBackWnd->PostMessage(WM_SET_ERROR_MODE, (WPARAM)(LPCSTR)strMsg);

		pHost->PostMessage(WM_THREAD_INIT_TEPLATE, (WPARAM)false, 0);
	}//if
	// Modified by 정운형 2008-11-07 오후 1:42
	// 변경내역 : Template play list는 존재하나, template가 없는경우 오류처리
	/*
	// add phone template id
	CString strPhoneTemplateId, strPhoneFrameId;
	int nPortNo;
	::GetPhoneConfig(strPhoneTemplateId, strPhoneFrameId, nPortNo);
	map_templateId.SetAt(strPhoneTemplateId, strPhoneTemplateId);
	__DEBUG__("Add PhoneTemplateID", strPhoneTemplateId);
	*/

	//TemplatePlayList와 TemplateList에 일치하는 Template만을 만들어 준다
	CTemplatePlay* pclsPlay = NULL;
	CTemplate* pRetTemplate = NULL;
	CString strTemplateID;

	//440에서 nCallCount을 뺀 숫자만큼의 진행상황을 나누어 표시한다
	int nAryCount = pHost->GetCountOfTemplatePlayAry();
	if(nAryCount == 0)
	{
		pBackWnd->PostMessage(WM_SET_ERROR_MODE, (WPARAM)(LPCSTR)"Empty template play list !!!", 0);
		pHost->PostMessage(WM_THREAD_INIT_TEPLATE, (WPARAM)false, 0);
	}//if

	int nFacter, nTotal;
	nTotal = PRG_CNT_INIT_TEMPLATE - nCallCount;
	if(nTotal <= 0)
	{
		nFacter = 1;
	}
	else
	{
		nFacter = nTotal/nAryCount;
		if(nFacter == 0)
		{
			nFacter = 1;
		}//if
	}//if		

	CTemplateMap mapTemplate;		//한번 만들어진 template이 중복되서 만들어 지지 않도록...
	for(UINT i=0; i<nAryCount; i++)
	{
		//Progress set
		pBackWnd->PostMessage(WM_SET_POS, E_INIT_TEMPLATE, nFacter);

		pclsPlay = pHost->GetAtTemplatePlay(i);
		if(!map_templateId.Lookup(pclsPlay->m_strTemplateID, strTemplateID))
		{
			//template list에 없는 Play list는 삭제 한다
			delete pclsPlay;				//객체삭제
			pHost->RemoveAtTemplatePlay(i);	//배열에서 삭제
			i--;							//인덱스 조정
			nAryCount--;
		}
		else
		{
			//Template를 생성하고 PlayList에 생성된 Template 포인터를 설정
			if(!mapTemplate.Lookup(strTemplateID, (void*&)pRetTemplate))
			{
				pRetTemplate = pHost->AddTemplate(strTemplateID, false, NULL);
				//pRetTemplate = AddTemplate(strTemplateID);
				if(!IsWindow(pRetTemplate->m_hWnd))
				{
					TRACE("FAIL\r\n");
				}//if
				if(pRetTemplate)
				{
					pclsPlay->m_pclsTemplate = pRetTemplate;
					mapTemplate.SetAt(strTemplateID, pRetTemplate);
				}//if
			}
			else
			{
				//이미 template이 만들어져 있다면 template 포인터만 할당한다
				pclsPlay->m_pclsTemplate = pRetTemplate;
			}//if
		}//if
	}//for
	mapTemplate.RemoveAll();

	// Modified by 정운형 2009-01-14 오전 11:53
	// 변경내역 :  default template 설정 수정
	//Play list의 playcount가 0이 아닌 첫번째 template를 default template로 설정
	UINT unPlayCount = 0;
	CString strDefaultTemplateID;
	for(int i=0; i<nAryCount; i++)
	{
		pHost->GetPlayCountOfTemplatePlayAry(i, unPlayCount);
		if(unPlayCount != 0)
		{
			pHost->SetPlayingTemplateIndex(i);
			pHost->SetDefaultTemplatePlay(i);
			break;
		}//if
	}//for
	// Modified by 정운형 2009-01-14 오전 11:53
	// 변경내역 :  default template 설정 수정

	//templatePlayList의 갯수가 1개 이면 Pause 모드로 설정하여,
	//Template가 변하지 않도록 한다.
	if(nAryCount <= 1)
	{
		pHost->m_bPause = true;
	}//if

	pHost->PostMessage(WM_THREAD_INIT_TEPLATE, (WPARAM)true, 0);

	__DEBUG_HOST_END__(_NULL)

	return 0;
	//return CWinThread::Run();
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Template 객체를 추가 \n
/// @param (LPCSTR) lpszTemplateID : (in) 추가하려는 template id
/// @return <형: CTemplate*> \n
///			<CTemplate*: 성공> \n
///			<NULL: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
CTemplate* CThreadInitTemplate::AddTemplate(LPCSTR lpszTemplateID)
{
	CHost* pHost		= (CHost*)m_pParent;
	CBackWnd* pBackWnd	= (CBackWnd*)m_pBackWnd;
	CString strMsg;

	CTemplate* pReturn = NULL;
	cciCall call("get");

	cciEntity aEntity("RM=*/Site=*/Template=%s", lpszTemplateID);
	call.setEntity(aEntity);

	cciAny nullAny;
	call.addItem("attributes", nullAny);

	try
	{
		__DEBUG__("Get Template Call", call.toString());

		if(!call.call())
		{
			strMsg = "Fail to get Template Call !!!";
			pHost->PostMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strMsg, 0);
			__ERROR__(strMsg, _NULL);
			pBackWnd->PostMessage(WM_SET_ERROR_MODE, (WPARAM)(LPCSTR)strMsg);
			return pReturn;
		}

		cciReply reply;
		while(call.getReply(reply))
		{
			CString str = reply.toString();
			if(str.GetLength() < 10) continue;

			__DEBUG__("Get Template Reply", reply.toString());

			reply.unwrap();

			ciString templateId;
			if(reply.getItem("templateId", templateId))
			{
				CTemplate* tmpl = pHost->GetTemplateByTemplatID(templateId.c_str());
				if(tmpl)
				{
					tmpl->InitFrame(false, NULL);
					pReturn = tmpl;
				}
				else
				{
					CString strErrorMessage;
					tmpl = CTemplate::GetTemplateObject(pHost, reply, strErrorMessage);
					if(tmpl)
					{
						pReturn = tmpl;
					}
					else
					{
						pHost->PostMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strErrorMessage, 0);
						__ERROR__(strErrorMessage, reply.toString());
					}//if
				}//if
			}//if
		}//while
	}
	catch(CORBA::Exception& ex)
	{
		strMsg = "Get Template Call Exception !!!";
		pHost->PostMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strMsg, 0);
		__ERROR__(strMsg, _NULL);
		pBackWnd->PostMessage(WM_SET_ERROR_MODE, (WPARAM)(LPCSTR)strMsg);
		return pReturn;
	}//try

	return pReturn;
}