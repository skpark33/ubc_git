// Host.h : 헤더 파일
//

#pragma once

#include "Template.h"
#include "TimeDialog.h"
#include "LicenseDialog.h"
#include "TemplatePlay.h"
#include "ProgressCtrlEx.h"
#include "SelectFrameDialog.h"
#include "MenuBarDlg.h"
#include "MenuBtnDlg.h"
#include "AnnounceDlg.h"
#include "HelpDialog.h"
#include "OverlayInterface.h"
#include "UBCAudioControl.h"
#include "Downloader.h"

//초기화중에 프로그래시브바 계산하기 위한 정의
#define		PRG_CNT_MAX					1000		///<최대 값
#define		CNT_INIT_MODE				6			///<초기화 모드 갯수

//초기화 과정 상태 구분
enum E_INIT_MODE {
	E_INIT_UI			= 0,	///<UI 초기화 과정
	E_INIT_HOST			,		///<Host 초기화 과정(LoadHost)
	E_INIT_TEMPLATE		,		///<Template 초기화 과정(LoadTemplate)
	E_INIT_CHECK_FILE	,		///<Check file 과정
	E_INIT_OPEN_FILE	,		///<Open file 과정
	E_INIT_HANDLER				///<Init handler 과정
};

//브라우져의 상태 구분
enum E_STATE_BRW
{
	E_BRW_INIT = 1,
	E_BRW_DOWNLOADING,
	E_BRW_PLAYING
};


/*
#define		PRG_CNT_INIT_UI				10			///<Init UI 과정
#define		PRG_CNT_INIT_HOST			50			///<Init Host 과정
#define		PRG_CNT_INIT_TEMPLATE		440			///<Init Template
#define		PRG_CNT_CHECK_FILE			100			///<Check File 과정
#define		PRG_CNT_OPEN_FILE			390			///<OPen File 과정
#define		PRG_CNT_INIT_HANDLER		10			///<Init Handler 과정
*/
//
class CBackWnd : public CWnd
{
	DECLARE_DYNAMIC(CBackWnd)
public:
	CBackWnd();
	virtual ~CBackWnd();
	
protected:
	DECLARE_MESSAGE_MAP()
	CString			m_strLog;									///<Log의 내용				
	int				m_nBeforeMode;								///<이전의 초기화 mode 구분
	int				m_nPos;										///<Progress bar의 포지션
	bool			m_bLogoMode;								///<밴더의 Logo 배경 이미지를 사용하는지 여부
	CxImage			m_bgImage;									///<배경 이미지
	CProgressCtrlEx*m_pprogressInit;							///<초기화 Progress bar
	CProgressCtrlEx*m_pprogressDownload;						///<다운로드 Progress bar
	CProgressCtrlEx*m_pprogressTar;								///<Tar extract Progress bar

public:
	static int		m_nModeValue[CNT_INIT_MODE];				///<각 초기화 모드의 유효 카운트 값

	void	AddLog(LPCSTR lpszLog, bool bNextNewLine=false);	///<Log를 추가한다
	void	ClearLog(void);										///<Log를 삭제한다
	void	SetPos(int nMode, int nPos);						///<Porgrerss bar의 posotion을 조정한다
	void	SetErrorMode(CString strMsg);						///<장애 모드 표시
	void	SetLogoImage(CString strLogoPath);					///<밴더별 logo 이미지를 설정
	void	InitDownload(void);									///<다운로드 초기화
	void	EndDownload(void);									///<다운로드 종료
	void	SetDownloadProgress(int nProgress, CString strMsg);	///<다운로드 상태를 설정한다.
	void	InitTarExtract(void);								///<Tar 풀기 초기화
	void	EndTarExtract(void);								///<Tar 풀기 종료
	void	SetTarExtractProgress(int nProgress, CString strMsg);///<Tar 풀기 진행 상태를 설정한다.

	afx_msg void	OnPaint();
	afx_msg void	OnMouseMove(UINT nFlags, CPoint point);
	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnMove(int x, int y);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnSetPos(WPARAM wParam, LPARAM lParam);			///<Porgrerss bar의 posotion을 조정 메시지
	afx_msg LRESULT OnAddLog(WPARAM wParam, LPARAM lParam);			///<Log를 추가 메시지
	afx_msg LRESULT OnSetErrorMode(WPARAM wParam, LPARAM lParam);	///<Error 모드 설정
};



/////////////////////////////////////////////////////////////////////////////////////////////////////

// CHost 대화 상자

class CHost : public CDialog
{
// 생성입니다.
public:
	CHost(CWnd* pParent = NULL);	// 표준 생성자입니다.

	class AnnounceMsg {
	public:
		CString announceId;
		int		msgNo;
	};

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HOST_DIALOG };

	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnClose();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDebugString(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDebugGrid(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT OnErrorMessage(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnStateDownload(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnProgressDownload(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFailCountDownload(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayNextSchedule(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnContentsUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayPrevDefaultTemplate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayNextDefaultTemplate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayPrevTemplate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayNextTemplate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMouseHook(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPauseBRW(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDefaultTemplateChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnProgressTar(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompleteTar(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSendStop(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEndRunIE(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnAnnounceMsg(WPARAM wParam, LPARAM lParam);	///<Error 모드 설정

// 구현입니다.
protected:
	HICON m_hIcon;

	DECLARE_MESSAGE_MAP()

	// host attribute
	ciShort			m_soundVolume;
	ciString		m_defaultTemplate;
	ciBoolean		m_adminState;
	ciBoolean		m_operationalState;
	ciString		m_templatePlayList;

	ciString		m_strDescription;			///<Host 설명
	ciString		m_strNetworkUse;			///<네트웍 사용여부(BRW에서는 항상 "1")
	ciString		m_strSite;					///<site 명

	CTemplate*			m_pCurrentTemplate;
	CCriticalSection	m_csHost;
	CCriticalSection	m_csMouse;

	CHelpDialog			m_dlgHelp;
	CMenuBarDlg*		m_pdlgMenuBar;				///<메뉴바
	CAnnounceDlg*		m_pdlgAnnounce;				///<긴급공지 Dialog
	CAnnounceDlg*		m_pdlgAnnounce_image;				///<긴급공지 Dialog (컨텐츠용)
	CAnnounceDlg*		m_pdlgAnnounce_video;				///<긴급공지 Dialog (컨텐츠용)
	CAnnounceDlg*		m_pdlgAnnounce_flash;				///<긴급공지 Dialog (컨텐츠용)
	CAnnounceDlg*		m_pdlgAnnounce_ppt;				///<긴급공지 Dialog (컨텐츠용)
	CAnnounceDlg*		m_pdlgAnnounce_web;				///<긴급공지 Dialog (컨텐츠용)
	CSelectFrameDialog*	m_pdlgSelectFrame;
	CBrush				m_brushBack;				// 대화창 바탕색
	CBackWnd			m_wndBack;
	CDownloader			m_libDownloader;			///<다운로드 클래스

	CPtrArray			m_aryTemplatePlay;			///<Template play 방법을 갖는 배열
	UINT				m_unPlayingTemplateIndex;	///<m_aryTemplatePlay에서 현재 할성화되어 play 중인 template의 인덱스
	UBCAudioControl		m_libAudioControl;			///<Audio control lib

	COverlayInterface	m_interface;

	static	double		m_fXScale;
	static	double		m_fYScale;

	static int			m_nTemplateX;				// template를 x,y를 기준으로 이동
	static int			m_nTemplateY;

	bool				m_bInitialize;
	bool				m_bFullScreen;
	bool				m_bTopMostMode;
	bool				m_bViewMouse;
	bool				m_bLogoMode;				///<밴더의 Logo를 배경에 보여주는지 여부
	bool				m_bLoadFromLocal;
	bool				m_bShowMenuButton;			///<메뉴버튼을 보여주는지 여부
	bool				m_bSound;					///<Sound On/Off 값
	bool				m_bIsTVPlay;				///<TV 스케줄이 방송중인지를 나타내는 플래그
	bool				m_bIsPlayDummy;				///<Dummy video가 재생중인지 여부
	bool				m_bPackageDown;				///<패키지 파일을 새로 받았는지 여부
	
	int					m_nPosX;					///<프로그램이 위치하는 X 좌표
	int					m_nPosY;					///<프로그램이 위치하는 Y 좌표
	int					m_nCx;						///<프로그램의 넓이
	int					m_nCy;						///<프로그램의 높이
	int					m_nMonitorIndex;			///<프로그램이 위치한 모니터의 인덱스
	int					m_nFailCount;				///<컨텐츠 파일 다운로드 실패한 개수
	int					m_nTotalCount;				///<컨텐츠 파일 다운로드 전체 개수
	DWORD				m_dwMouseStart;				///<마우스가 x, y좌표방향으로 10포인트 이상 움직인 시간
	DWORD				m_dwClickJumpWaitTime;		///<클릭점프 타이머의 대기 시간;
	HINSTANCE			m_hHookDll;					///<마우스 hook dll의 핸들
	
	CRect				m_rectMenuBar;				///<메뉴바의 영역크기
	CString				m_strTerminateApp;			///<프로그램이 초기화하고나서 종료시켜야하는 App 실행파일 이름
	time_t				m_tmPausTime;				///<BRW를 대기상태에서 다시 실행시키는 시간

public:
	//For Enterprise
	ciString			m_strLastUpdateId;
	cciTime				m_tmLastUpdate;

	CMenuBtnDlg			m_dlgMenuBtn;				///<메뉴바 출력을 조정하는 버튼 dialog
	CTimeDialog			m_timeDialog;
	CLicenseDialog		m_licenseDialog;
	bool				m_bPauseTemplate;			///<Template pause 여부
	bool				m_bClickJump;				///<클릭점프스케줄이 진행중인지 여부
	bool				m_bVerify;					///<인증을 받았는지 여부
	bool				m_bExitPauseBRWThread;		///<PauseBRW thread를 종료할지 여부
	bool				m_bExitNotifyThread;		///<Notify thread의 종료여부 flag
	bool				m_bExitIEStateThread;		///<IEState thread의 종료여부 flag
	bool				m_bExitResourceThread;		///<자원 모니터 thread 종료여부 flag
	CWinThread*			m_pThreadPauseBRW;			///<PauseBRW thread
	HANDLE				m_hEventThreadPauseBRW;		///<PauseBRW thread Event
	HANDLE				m_hEventThreadNotify;		///<Notify thread Event
	HANDLE				m_hEvtThreadIEState;		///<IEState thread Event
	HANDLE				m_hEvtThreadResource;		///<자원모니터 thread event

	CString				m_strErrorMessage;
	CString				m_debugString;
	CString				m_strArgTemplateID;			///<실행 옵션으로 +template 옵션이 주어졌을경우, template id
	CString				m_strEdition;				///<제품의 구분(STD, PLS, ENT)
	CString				m_strVersion;				///<프로그램의 버전
	CString				m_strComputerName;			///<프로그램이 구동하는 컴퓨터의 이름
	CString				m_strRegstrationKey;		///<인증키
	CString				m_strReturnTemplateId;		///<일정시간뒤에 다시 이동할 템플릿 Id

	int					m_nStateBRW;				///<브라우져의 상태 구분 값
	int					m_nPid;						///<브라우져의 PID
	CString				m_strStateMsg;				///<브라우져의 상태 메시지

	bool				m_useTimeCheck;				//skpark same_size_file_problem
protected:
	// 초기화 함수
	bool		InitParameter();
	bool		DownloadPackageFile(void);				///<서버로부터 스케줄파일(ini)파일을 다운로드한다.
	int			LoadHost();
	bool		LoadTemplate();
	bool		CheckFile(void);
	bool		OpenFile();
	bool		CheckSchedule(bool bForward = true);				///<template play list를 순환하며 재생할 template를 결정한다.
	bool		_checkSchedule(bool bForward = true);				///<template play list를 순환하며 time schedule을 재생할 template를 결정한다.
	bool		_checkDefaultSchedule(bool bForward = true);
	bool		CheckScheduleTime(time_t& tmTime, bool& bIsOpen);	///<스케줄파일에 존재하는 모든 스케줄의 시간을 검사한다.

	static UINT	ThreadPauseBRW(LPVOID pParam);						///<BRW를 지정된 시간까지 중지 시키는 thread
	static UINT	ThreadExtractTarFile(LPVOID param);					///<Tar 파일을 푸는 thread
	static UINT	ThreadNotifyState(LPVOID param);					///<브라우져의 상태를 주기적으로 알리는 thread
	static UINT	ThreadIEState(LPVOID param);						///<시스템에 IE가 구동중인지 검사하는 thread
	static UINT	ThreadMonitorResource(LPVOID param);				///<브라우져가 사용하는 자원을 모니터하는 thread

public:
	void		SetDefaultTemplateId(LPCSTR lpszTemplateId);
	CTemplate*	GetdefaultTemplate(void);													///<defaultTemplate를 반환
	void		SetDefaultTemplatePlay(UINT unIndex);										///<TemplatePlayAry의 인덱스로 default template를 설정
	bool		SetDefaultTemplatePlay(CString strTemplateId);								///<Template id로 default template를 설정
	bool		CreateTemplatePlayAry(const char* szTemplatePlayList);						///<TemplatePlayAry를 생성
	UINT		GetCountOfTemplatePlayAry(void);											///<TemplatePlayAry의 count를 반환
	bool		GetPlayCountOfTemplatePlayAry(UINT unIndex, UINT& unPlayCount);				///<TemplatePlayAry의 지정된 인덱스의 play count를 반환
	CTemplate*	GetTemplateByTemplatID(CString strTmplateID);								///<TemplateId로 TemplatePlay Ary의 Template pointer 반환
	CTemplatePlay* GetAtTemplatePlay(UINT unIndex);											///<TemplatePalyArray에서 지정된 인덱스의 CTemplatePlay 객체 반환
	CTemplatePlay* GetAtTemplatePlay(LPCSTR lpszTemplateId);								///<TemplatePalyArray에서 지정된 template id의 CTemplatePlay 객체 반환
	void		RemoveAtTemplatePlay(UINT unIndex);											///<TemplatePlayArray에서 지정된 인덱스의 객체 삭제
	
	UINT		GetPlayingTemplateIndex(void);												///<현재 할성화되어 play 중인 template의 인덱스를 반환
	CString		GetTemplatePlayList(void);													///<Template play list를 반환

	void		StartBRW(void);																///<BRW application을 시작한다
	void		StopBRW(void);																///<BRW application을 종료한다
	bool		InitUI(void);																///<BRW application의 UI를 구성 한다
	void		InitWndPostion();															///<BRW의 화면의 크기와 위치를 초기화 한다
	void		SetAppPosition(int nX, int nY, int nCx, int nCy, int nMonitorIndex);		///<프로그램이 위치하는 좌표와 영역을 설정
	void		GetAppPosition(int& nX, int& nY, int& nCx, int& nCy);						///<프로그램이 위치하는 좌표와 영역을 반환 
	void		SetTopMost(bool bTopMost);													///<프로그램을 topmost로 설정한다.
	void		MoveToSmallWnd(void);														///<F12 key를 눌렀을때 작아진 화면으로 변경함
	void		ProcessReplace(void);														///<+replace 옵션으로 초기화후에 다른 프로그램을 종료시킨다.
	void		ProcessStartup();															///<프로그램 시작전에 UI와 Arg등 기타 설정을 적용
	void		ViewTimeDialog(void);														///<Time dialog를 보여준다.
	void		ViewLicense(void);														///<Time dialog를 보여준다.
	void		ViewHelp(void);																///<Help dialog를 출력한다
	void		ToggleFullScreen(void);														///<Full screen/Window 상태를 전환한다
	bool		GetFullScreen(void);														///<Full screen/Window 상태를 반환한다.
	bool		UpdateSoundState(void);														///<Sound On/Off 상태를 갱신하여 반환한다.
	void		ProcessCTRLKey(MSG* pMsg);													///<CTRL key가 눌렸을 때 처리를 한다.
	void		StartCoursorHook(void);														///<Coursor hook를 시작한다.
	void		StopCoursorHook(void);														///<Coursor hook를 종료한다.
	void		SetShowCursor(bool bVisible);												///<Cursor의 vissible 상태를 설정한다.
	bool		IncreaseVolume(void);														///<Audio volume을 올린다.
	bool		DecreaseVolume(void);														///<Audio volume을 내린다.
	void		PauseBRW(void);																///<지정된 시간까지 BRW를 중단시킨다.
	void		FrameModify(void);															///<Frame의 컨텐츠를 즉시 변경한다(F3 Key)
	void		SetSoundValue(bool bSound);													///<Grade가 1인 컨텐츠의 사운드를 On/Off 한다
	void		SendStopBRW(void);															///<BRW를 종료시키라는 메시지를 호출한다.
	void		SetTVRunningState(bool bIsPlay);											///<TV 스케줄이 방송중인지를 설정한다.
	bool		GetTVRunningState(void);													///<TV 스케줄이 방송중인지를 반환한다.
	bool		IsPlayDummy(void)		{ return m_bIsPlayDummy; };							///<Dummy video가 재생중인지 여부를 반환.
	void		OpenDummyVideo(void);														///<Dummy video를 open한다.
	void		CloseDummyVideo(void);														///<Dummy video를 close한다.
	void		ClickJumpTemplate(CString strParam);										///<클릭이벤트를 받아 특정 템플릿으로이동한다.
	void		TemplateCheck(void);														///<템플릿의 재생순서를 확인한다.
	void		ResetClickJumpTimer(void);													///<클릭점프 스케줄의 타이머를 리셋한다.
	void		CheckTarFile(void);															///<Tar 파일이 있는지 확인하고 있다면 풀어준다.
	void		RunIENavigate(CString strURL);												///<주어진 URL로 IE를 뛰운다.

	static void	GetTemplateXY(int& x, int& y)	{ x = m_nTemplateX; y = m_nTemplateY; };
	static void	GetScale(double& fXScale, double& fYScale) { fXScale=m_fXScale; fYScale=m_fYScale; };

	virtual CString	ToString();

//skpark 2013.11.28 new announce
//	bool		ReadAnnounce(int contentsType);															///<Announce.ini 파일에서 긴급공지를 읽어온다.
	bool		ReadAnnounce(const char* announceId, bool contentsType);															///<Announce.ini 파일에서 긴급공지를 읽어온다.
	int			ReadAllAnnounce();	//모든 isStart 가 true 인 announce를 읽어온다.
	
	void		MakeTrialAnnounce(void);													///<인증이 안되었음을 공지로 알린다.

	bool		Stop3rdPartyPlayer();
	bool		Start3rdPartyPlayer();

	//bool		isMonitorOn();
	//bool		isMonitorOff();

protected:
	CString		m_errScheduleId; //skpark 2013.2.16 errMsg 를 scheduleId 등 별로 보관했다가 clear 하기 위해서...skpark_manager_err

//skpark 2013.11.28 new announce
	bool		WaitAnnounceLock(CProfileManager& mngProf);
	int			CreateAnnounceObject(CProfileManager& mngProf);

	// 단말 카테코리 체크 (컨텐츠 카테고리 검사용)
protected:
	static		CCriticalSection	m_csCategoryCheck;
	static		CMapStringToString	m_mapCategory;
public:
	static		bool	CheckCategory();
	static		bool	IsPlayableCategory(CMapStringToString& mapInclude, CMapStringToString& mapExclude);

protected:
	int			m_nDownloadStatus;

	void		CheckUpdater();

	void		SendStopSignal();
	void		killSyncProcess();

	static CArray<HWND, HWND>	m_lsShowWindowHwnd;
	static CArray<HWND, HWND>	m_lsHideWindowHwnd;
public:
	static void	PushFrontToShowWindow(HWND hWnd, int idx=0);
	static void	PushBackToShowWindow(HWND hWnd);
	static void	PushFrontToHideWindow(HWND hWnd, int idx=0);
	static void	PushBackToHideWindow(HWND hWnd);

	int		GetTemplateIndex(LPCSTR lpszTemplateId);
};
