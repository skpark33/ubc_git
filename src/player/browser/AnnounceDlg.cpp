// AnnounceDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "AnnounceDlg.h"
#include "MemDC.h"
#include "Host.h"

#define		ID_ANNOUNCE_CHECK			5000		//긴급공지 체크타이머 ID
#define		TIME_ANNOUNCE_CHECK			10000		//긴급공지 체크타이머 시간

#define		CAPTION_SHIFT_ID			4002		//긴급공지 이동타이머  ID


#ifndef LWA_ALPHA
#define LWA_ALPHA	0x00000002
#endif

#ifndef WS_EX_LAYERED
#define WS_EX_LAYERED	0x00080000
#endif


typedef BOOL (_stdcall *TRANCPARENCY_TYPE)(HWND,COLORREF,BYTE,DWORD); 
TRANCPARENCY_TYPE ann_SetLayeredWindowAttributes; 

// CAnnounceDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAnnounceDlg, CDialog)

CAnnounceDlg::CAnnounceDlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent /*=NULL*/)
	: CDialog(CAnnounceDlg::IDD, pParent)
	, m_nParentPosX(0)
	, m_nParentPosY(0)
	, m_nParentCx(0)
	, m_nParentCy(0)
	, m_nPosX(0)
	, m_nPosY(0)
	, m_nCx(0)
	, m_nCy(0)
	, m_pAnnPlay(NULL)
	, m_offset(0)
	, m_nTickerWidth(0)
	, m_rgbBgColor(RGB(0,0,0))
	, m_rgbFgColor(RGB(255,255,255))
	, m_strAnnounce("")
	, m_bExitThread(true)
	, m_hevtShift(NULL)
	, m_pthreadShift(NULL)
//	, m_nTimerId(0)
	, m_nSpeed(E_TICKER_NORMAL)
	, m_nShiftStep(1)
{
	m_nParentPosX	= nPosX;
	m_nParentPosY	= nPosY;
	m_nParentCx		= nCx;
	m_nParentCy		= nCy;
	m_ticker		= 0;

	m_timerPeriod   = 0;
	m_timerPeriod = atoi(GetINIValue("UBCVariables.ini", "ROOT", "TIME_ANNOUNCE_CHECK"));
	if(m_timerPeriod==0){
		m_timerPeriod = TIME_ANNOUNCE_CHECK; //default 10초
	}

}

CAnnounceDlg::~CAnnounceDlg()
{

}

BEGIN_MESSAGE_MAP(CAnnounceDlg, CDialog)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CAnnounceDlg 메시지 처리기입니다.


BOOL CAnnounceDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 긴급공지 배열을 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounceDlg::ClearAnnounceAry()
{
	m_csAnnounce.Lock();

	CAnnounce* pAnnounce = NULL;
	for(int i=0; i<m_aryAnnounce.GetSize(); i++)
	{
		pAnnounce = m_aryAnnounce.GetAt(i);
		if(pAnnounce)
		{
			delete pAnnounce;
		}//if
	}//for
	m_aryAnnounce.RemoveAll();

	m_csAnnounce.Unlock();
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Announce 객체를 배열에 추가한다. \n
/// @param (CAnnounce*) pAnnounce : (in) 추가하려는 announce 객체
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CAnnounceDlg::AddAnnounce(CAnnounce* pAnnounce)
{
	m_csAnnounce.Lock();
	
	//배열에 같은 ID의 공지가 있다면 이전의 공지를 삭제하고 
	//새로운 공지를 배열에 추가한다.
	CAnnounce* pOld = NULL;
	for(int i=0; i<m_aryAnnounce.GetCount(); i++)
	{
		pOld = (CAnnounce*)m_aryAnnounce.GetAt(i);
		if(pOld->m_strAnnounceId == pAnnounce->m_strAnnounceId)
		{
			__DEBUG__("Same id announce exist, will be deleted",_NULL);
			//같은 id의 공지가 실행중이라면 실행중인 공지가 변경된것이므로 중지 시킨다
			if(pOld->m_bPlaying && m_pAnnPlay && strcmp(m_pAnnPlay->m_strAnnounceId.c_str(), pAnnounce->m_strAnnounceId.c_str()) == 0)
			{
				EndAnnounce();
			}//if
			delete pOld;
			m_aryAnnounce.RemoveAt(i);
			break;
		}//if
	}//if
	
	__DEBUG__("New announce added", pAnnounce->m_strAnnounceId.c_str());
	m_aryAnnounce.Add(pAnnounce);

	m_csAnnounce.Unlock();
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Announce 객체를 배열에서 삭제한다. \n
/// @param (CString) strAnnounceId : (in) 삭제하려는 announce 객체 Id
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CAnnounceDlg::RemoveAnnounce(CString strAnnounceId)
{
	m_csAnnounce.Lock();

	__DEBUG__("RemoveAnnounce", _NULL);

	bool bRemove = false;
	CAnnounce* pAnn = NULL;
	for(int i=0; i<m_aryAnnounce.GetSize(); i++)
	{
		pAnn = m_aryAnnounce.GetAt(i);
		if(strcmp(strAnnounceId, pAnn->m_strAnnounceId.c_str()) == 0)
		{
			if(m_pAnnPlay && strcmp(m_pAnnPlay->m_strAnnounceId.c_str(), strAnnounceId) == 0)
			{
				__DEBUG__("End announce", strAnnounceId);
				EndAnnounce();
			}//if

			delete pAnn;
			m_aryAnnounce.RemoveAt(i);
			bRemove = true;
			break;
		}//if
	}//for

	m_csAnnounce.Unlock();
	return bRemove;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Announce 객체를 배열에서 삭제한다. \n
/// @param (int) nIndex : (in) 삭제하려는 announce 객체 Index
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CAnnounceDlg::RemoveAnnounce(int nIndex)
{
	m_csAnnounce.Lock();
	__DEBUG__("Remove Announce", nIndex);

	if(m_aryAnnounce.GetSize() < nIndex+1)
	{
		return false;
	}//if

	CAnnounce* pAnn = m_aryAnnounce.GetAt(nIndex);
	if(m_pAnnPlay && strcmp(m_pAnnPlay->m_strAnnounceId.c_str(), pAnn->m_strAnnounceId.c_str()) == 0)
	{
		EndAnnounce();
	}//if

	delete pAnn;
	m_aryAnnounce.RemoveAt(nIndex);

	m_csAnnounce.Unlock();
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 긴급공지 기능을 시작하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounceDlg::StartAnnouncePlay()
{
	__DEBUG__("StartAnnouncePlay()", NULL);
	SetPlayAnnounce();
	//SetTimer(ID_ANNOUNCE_CHECK, TIME_ANNOUNCE_CHECK, NULL);
	//__DEBUG__("StartAnnouncePlay()  1", (int)m_timerPeriod);
	UINT ret = SetTimer(ID_ANNOUNCE_CHECK, m_timerPeriod, NULL);
	//__DEBUG__("SetTimer retval", ret);
	__DEBUG__("StartAnnouncePlay() End", (int)m_timerPeriod);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 긴급공지 기능을 중지하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounceDlg::StopAnnouncePlay()
{
	__DEBUG__("StopAnnouncePlay()", NULL);
	KillTimer(ID_ANNOUNCE_CHECK);
	EndAnnounce();
	ClearAnnounceAry();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시간이 지난 공지를 배열에서 삭제하고 현재 플레이 되어야 하는 공지를 설정한다. \n
/// @return <형: CAnnounce*> \n
///			<CAnnounce*: 현재 플레이 되어야하는 공지> \n
///			<NULL: 플레이될 공지가 없음> \n
/////////////////////////////////////////////////////////////////////////////////
CAnnounce* CAnnounceDlg::CheckAnnounce()
{
	m_csAnnounce.Lock();

	//__DEBUG__("CheckAnnounce(size)", m_aryAnnounce.GetSize());


	CTime tmNow = CTime::GetCurrentTime();
	CString strTmNow, strTmStart, strTmEnd;

	//현재 시간이 시작/종료 시간사이에 있는 공지는 출력을하고,
	//종료 시간이 지난 공지는 삭제한다.
	CAnnounce* pAnn = NULL;
	CAnnounce* pAnnNew = NULL;
	for(int i=0; i<m_aryAnnounce.GetSize(); i++)
	{
		pAnn = m_aryAnnounce.GetAt(i);
		if(pAnn->m_strAnnounceId == "TrialAnnounce")
		{
			__DEBUG__("TrialAnnounce",_NULL);
			pAnnNew = pAnn;
			break;
		}//if

		CTime tmStart(pAnn->m_tmStartTime.getTime());
		CTime tmEnd(pAnn->m_tmEndTime.getTime());

		strTmNow = tmNow.Format("%Y/%m/%d %H:%M:%S");
		strTmStart = tmStart.Format("%Y/%m/%d %H:%M:%S");
		strTmEnd = tmEnd.Format("%Y/%m/%d %H:%M:%S");

		CString debugStr = strTmNow + "," + strTmStart + "," + strTmEnd;

		//__DEBUG__("Now,Start,End", debugStr);

		//지난 공지 삭제
		if(tmEnd <= tmNow)
		{
			__DEBUG__("Remove Announce", pAnn->m_strAnnounceId.c_str());
			RemoveAnnounce(i);
			i--;
			continue;
		}//if

		//새로운 공지 시작
		if(tmStart <= tmNow)
		{
			//__DEBUG__("new announce", _NULL);
			if(pAnnNew != NULL)
			{
				//__DEBUG__("announce exist", _NULL);
				if(pAnn->m_tmStartTime > pAnnNew->m_tmStartTime)
				{
					//__DEBUG__("more lately exist", _NULL);
					pAnnNew = pAnn;
				}//if
				//__DEBUG__("announce exist 2", _NULL);
			}
			else
			{
				//__DEBUG__("first announce founded", _NULL);
				pAnnNew = pAnn;
			}//if
		}//if
	}//for
	//__DEBUG__("CheckAnnounce() end", _NULL);
	return pAnnNew;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 나가야 할 긴급공지를 설정한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounceDlg::SetPlayAnnounce()
{
	//__DEBUG__("SetPlayAnnounce()",NULL);

	CAnnounce* pPlayAnn = CheckAnnounce();
	if(pPlayAnn)
	{
		//공지를 시작한다.
		//m_pAnnPlay = pPlayAnn;
		//__DEBUG__("BeginAnnounce Again", _NULL);
		BeginAnnounce(pPlayAnn);
		//__DEBUG__("BeginAnnounce Again End", _NULL);
	}
	else
	{
		//나갈 공지가 없다.
		//__DEBUG__("None exist announce to play", _NULL);
		EndAnnounce();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 시작하도록 한다. \n
/// @param (CAnnounce*) pAnn : (in) 방송을 해야하는 공지 객체 주소값
/////////////////////////////////////////////////////////////////////////////////
void CAnnounceDlg::BeginAnnounce(CAnnounce* pAnn)
{
	if(!m_pAnnPlay)
	{
		m_pAnnPlay = pAnn;
	}
	else if((*m_pAnnPlay == *pAnn) && m_pAnnPlay->m_bPlaying)
	{
		ShowWindow(SW_SHOW);
		((CHost*)m_pParentWnd)->SetFocus();
		return;
	}
	else
	{
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = pAnn;
	}//if

	__DEBUG__("BeginAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());

	CString strLog = "";
	for(int i=0; i<TICKER_COUNT; i++)
	{
		strLog += m_pAnnPlay->m_strComment[i].c_str();
	}//for

	if(strLog.GetLength() > 255)
	{
		strLog = strLog.Left(255);
	}//if
	WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "TRY", m_pAnnPlay->GetRunningTime(), strLog);

	//불투명하게 처리한 후에 다시 투명도를 설정하여야 동영상 위에서 깜박이지 않는다.
	//SetTransParency(0, 0);
	SetTransParency(GetColorFromString(m_pAnnPlay->m_strBgColor.c_str()), m_pAnnPlay->m_sAlpha);

	switch(m_pAnnPlay->m_lPosition)
	{
	case 0:		//바닥
		{
			m_nPosX	= m_nParentPosX;
			//m_nPosY	= (m_nParentPosY + m_nParentCy) - m_pAnnPlay->m_sHeight - 34;	//메뉴버튼을 가리지 않게 한다.
			m_nPosY	= (m_nParentPosY + m_nParentCy) - m_pAnnPlay->m_sHeight - 20;	//메뉴버튼을 가리지 않게 한다.
			m_nCx	= m_nParentCx;
			m_nCy = m_pAnnPlay->m_sHeight;
		}
		break;
	case 1:		//최상단
		{
			m_nPosX	= m_nParentPosX;
			m_nPosY	= m_nParentPosY;
			m_nCx	= m_nParentCx;
			m_nCy = m_pAnnPlay->m_sHeight;
		}
		break;
	case 2:		//중앙
		{
			m_nPosX	= m_nParentPosX;
			m_nPosY	= ((m_nParentPosY + m_nParentCy)/2) - m_pAnnPlay->m_sHeight;
			m_nCx	= m_nParentCx;
			m_nCy = m_pAnnPlay->m_sHeight;
		}
		break;
	case 3:		//지정된 위치와 넓이
		{
			/*m_nPosX	= m_nParentPosX;
			m_nPosY	= ((m_nParentPosY + m_nParentCy)/2) - m_pAnnPlay->m_sHeight;
			m_nCx	= m_nParentCx;
			m_nCy = m_pAnnPlay->m_sHeight;*/
		}
		break;
	default:
		{
			m_nPosX	= m_nParentPosX;
			m_nPosY	= (m_nParentPosY + m_nParentCy) - m_pAnnPlay->m_sHeight - 34;	//메뉴버튼을 가리지 않게 한다.
			m_nCx	= m_nParentCx;
			m_nCy = m_pAnnPlay->m_sHeight;
		}
	}//switch

	MoveWindow(m_nPosX, m_nPosY, m_nCx, m_nCy);

	double xscale, yscale;
	((CHost*)m_pParentWnd)->GetScale(xscale, yscale);

	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if

	if(!IsAvailableFont(m_pAnnPlay->m_strFont.c_str()))
	{
		__DEBUG__("Fail to create font", m_pAnnPlay->m_strFont.c_str());
		m_fontTicker.CreatePointFont((m_pAnnPlay->m_sFontSize*10), "System");
	}
	else
	{
		m_fontTicker.CreatePointFont(m_pAnnPlay->m_sFontSize*10, m_pAnnPlay->m_strFont.c_str());
	}//if

	m_strAnnounce = "";
	for(int i=0; i<10; i++)
	{
		if(m_pAnnPlay->m_strComment[i].length() > 0)
		{
			m_strAnnounce.Append(m_pAnnPlay->m_strComment[i].c_str());
			m_strAnnounce.Append("    ");
		}//if
	}//for
	//&문자가 한개만 있으면 DrawText에서 특수문자로 처리하여 정상적으로 그려주지 못한다.
	m_strAnnounce.Replace("&", "&&");

	if(m_pAnnPlay->m_strBgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_pAnnPlay->m_strBgColor.c_str()+1);
	}//if

	if(m_pAnnPlay->m_strFgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_pAnnPlay->m_strFgColor.c_str()+1);
	}//if

	if(GetSafeHwnd())
	{
		CPaintDC dc(this);
		dc.SelectObject(&m_fontTicker);
		
		CSize text_size = dc.GetOutputTextExtent(m_strAnnounce);
		m_nTickerWidth = text_size.cx;
	}//if

	m_offset = 0;
	m_nShiftStep = 1;
	m_pAnnPlay->m_bPlaying = true;
	WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "START", m_pAnnPlay->GetRunningTime(), strLog);
/*
	//속도는 1~5 까지의 값을 갖고 1이 제일 느린 속도이다
	//속도가 1일때는 40 ms, 5일때는 8 ms를 설정한다.
	//m_unMMtimeSpeed = (5/m_pAnnPlay->m_sPlaySpeed)*8;
	m_nSpeed = (5/m_pAnnPlay->m_sPlaySpeed)*8;
	m_nTimerId = SetTimer(CAPTION_SHIFT_ID, m_nSpeed, NULL);
*/

	//속도를 총 5단게로 한다.
	switch(m_pAnnPlay->m_sPlaySpeed)
	{
	case E_TICKER_FAST:
		{
			m_nShiftStep = 3;
			m_nSpeed = 20;	//시스템에 부하를 적게주는 최대속도이다.
		}
		break;
	case E_TICKER_LITTLE_FAST:
		{
			m_nShiftStep = 2;
			m_nSpeed = 20;
		}
		break;
	case E_TICKER_NORMAL:
		{
			m_nShiftStep = 1;
			m_nSpeed = 20;
		}
		break;
	case E_TICKER_LITTLE_SLOW:
		{
			m_nShiftStep = 1;
			m_nSpeed = 40;
		}
		break;
	case E_TICKET_SLOW:
		{
			m_nShiftStep = 1;
			m_nSpeed = 80;
		}
		break;
	default:
		{
			m_nShiftStep = 1;
			m_nSpeed = 20;
		}
	}//switch

	if(m_pthreadShift)
	{
		ShowWindow(SW_SHOW);
		((CHost*)m_pParentWnd)->SetFocus();
	}
	else
	{
		m_pthreadShift = ::AfxBeginThread(CAnnounceDlg::CaptionShiftThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pthreadShift->m_bAutoDelete = TRUE;
		m_bExitThread = false;
		m_hevtShift = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pthreadShift->ResumeThread();
		//EndAnnounce();
	}//if
}


UINT CAnnounceDlg::CaptionShiftThread(LPVOID pParam)
{
	DWORD dwEventRet = 0;
	CAnnounceDlg* pParent = (CAnnounceDlg*)pParam;

	while(!pParent->m_bExitThread)
	{
		dwEventRet = WaitForSingleObject(pParent->m_hevtShift, pParent->m_nSpeed);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			//CloseHandle(pParent->m_hevtShift);
			__DEBUG__("Event set : CaptionShiftThread", _NULL);
			break;
		}//if

		pParent->DecreaseOffset();
		
	}//while

	//CloseHandle(pParent->m_hevtShift);
	__DEBUG__("Exit CaptionShiftThread", _NULL);

	return 1;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Offset 값을 줄인다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounceDlg::DecreaseOffset()
{
	m_csOffset.Lock();
	m_offset -= m_nShiftStep;

	//if(m_offset == -m_nTickerWidth)
	if(m_offset <= -m_nTickerWidth)
	{
		m_offset = 0;
	}//if
	m_csOffset.Unlock();

	Invalidate(FALSE);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 공지 방송을 중지하도록 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CAnnounceDlg::EndAnnounce()
{
	if(m_pAnnPlay)
	{
		__DEBUG__("EndAnnounce", m_pAnnPlay->m_strAnnounceId.c_str());

		CString strLog = "";
		for(int i=0; i<TICKER_COUNT; i++)
		{
			strLog += m_pAnnPlay->m_strComment[i].c_str();
		}//for

		if(strLog.GetLength() > 255)
		{
			strLog = strLog.Left(255);
		}//if

		if(m_pthreadShift)
		{
			WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "SUCCESS", m_pAnnPlay->GetRunningTime(), strLog);
		}
		else
		{
			WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "FAIL", m_pAnnPlay->GetRunningTime(), strLog);
		}//if

		m_bExitThread = true;
		SetEvent(m_hevtShift);
		//m_bExitThread = true;
		CloseHandle(m_hevtShift);
		m_hevtShift = NULL;
		m_pthreadShift = NULL;

		WriteAnnounceLog(m_pAnnPlay->m_strAnnounceId.c_str(), m_pAnnPlay->m_strTitle.c_str(), "END", m_pAnnPlay->GetRunningTime(), strLog);

		if(m_fontTicker.GetSafeHandle())
		{
			m_fontTicker.DeleteObject();
		}//if
		m_strAnnounce = "";
		m_nTickerWidth = 0;
		m_rgbBgColor = RGB(0,0,0);
		m_rgbFgColor = RGB(255,255,255);
		m_offset = 0;
		ShowWindow(SW_HIDE);
		m_pAnnPlay->m_bPlaying = false;
		m_pAnnPlay = NULL;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Play 여부 반환 \n
/// @return <형: bool> \n
///			<true: Play 중> \n
///			<false: Play 안되고 있음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CAnnounceDlg::IsPlay()
{
	if(m_pAnnPlay)
	{
		return m_pAnnPlay->m_bPlaying;
	}//if

	return false;
}


void CAnnounceDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == ID_ANNOUNCE_CHECK)
	{
		//__DEBUG__("OnTimer",(int)m_timerPeriod);
		SetPlayAnnounce();
	}//if

	CDialog::OnTimer(nIDEvent);
}

void CAnnounceDlg::OnDestroy()
{
	StopAnnouncePlay();

	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 화면의 투명도를 설정한다. \n
/// @param (COLORREF) crKey : (in) 투명도를 설정할 컬러키
/// @param (int) nVal : (in) 화면의 투명도 값(0 : 완전 투명, 255 : 불투명)
/////////////////////////////////////////////////////////////////////////////////
void CAnnounceDlg::SetTransParency(COLORREF crKey, int nVal, HWND hwnd)
{
	if( hwnd==NULL ) hwnd=GetSafeHwnd();

	if(nVal == 0)
	{
		//불투명하게 처리
		// Remove WS_EX_LAYERED from this window styles
		SetWindowLong(hwnd, GWL_EXSTYLE, GetWindowLong(hwnd, GWL_EXSTYLE) & ~WS_EX_LAYERED);

		// Ask the window and its children to repaint
		RedrawWindow(NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_FRAME | RDW_ALLCHILDREN);

		return;
	}//if

	//Announce 객체에서는 투명도를 0~100% 값으로 설정하나,
	//API에서는 0~255(0:완전투명, 255:불투명)으로 처리하므로 값을 변환한다.
	__DEBUG__("SetTransParency Percent", nVal);
	BYTE btTrans = 255 - ((float)nVal*2.55);

	HMODULE hUser32 = ::GetModuleHandle( _T("user32.dll"));
	if(hUser32)
	{
		ann_SetLayeredWindowAttributes = (TRANCPARENCY_TYPE)::GetProcAddress( hUser32, "SetLayeredWindowAttributes"); 

		if(ann_SetLayeredWindowAttributes) 
		{ 
			LONG lOldStyle = GetWindowLong(hwnd, GWL_EXSTYLE); 
			SetWindowLong(hwnd, GWL_EXSTYLE, lOldStyle | WS_EX_LAYERED); 
			//ann_SetLayeredWindowAttributes(hwnd, crKey, btTrans, LWA_ALPHA|LWA_COLORKEY);
			ann_SetLayeredWindowAttributes(hwnd, 0, btTrans, LWA_ALPHA|LWA_COLORKEY);
			__DEBUG__("SetTransParency", btTrans);
		}//if
	}//if
}


void CAnnounceDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	if(m_pAnnPlay && m_pAnnPlay->m_bPlaying)
	{
		CRect client_rect;
		GetClientRect(client_rect);

		CMemDC memDC(&dc);
		memDC.FillSolidRect(client_rect, m_rgbBgColor);

		int nOffset = 0;
		GetOffset(&nOffset);
		CRect rect = client_rect;
		rect.left = nOffset;
		rect.right = nOffset + m_nTickerWidth;

		memDC.SetBkMode(1);
		memDC.SetTextColor(m_rgbFgColor);
		memDC.SelectObject(m_fontTicker);

		do
		{
			memDC.DrawText(m_strAnnounce, rect, DT_SINGLELINE | DT_VCENTER);
			rect.OffsetRect(m_nTickerWidth, 0);
		} while(rect.left < client_rect.right);
	}//if
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 offset 값을 반환한다. \n
/// @param (int*) pnOffset : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CAnnounceDlg::GetOffset(int* pnOffset)
{
	m_csOffset.Lock();
	(*pnOffset) = m_offset;
	m_csOffset.Unlock();
}



void CAnnounceDlg::MoveWindowBySourceSize(int x, int y, int width, int height, CAnnounce* pAnn)
{
	__DEBUG__("MoveWindowBySourceSize",NULL);
	__DEBUG__("x=",x);
	__DEBUG__("y=",y);
	__DEBUG__("width=",width);
	__DEBUG__("height=",height);

	int fsize = pAnn->m_sHeight;

	int maxMargin = ((100-fsize));

	int lr =  pAnn->m_LRMargin;
	if(lr>maxMargin) {
		lr = maxMargin;
	}
	if(lr<0){
		lr = 0;
	}
	lr = (width*lr)/100;  // 백분률로 환산

	int ud = pAnn->m_UDMargin;
	if(ud>maxMargin) {
		ud = maxMargin;
	}
	if(ud<0) {
		ud = 0;
	}
	ud = (height*ud)/100; // 백분률로 환산
	
	int align = pAnn->m_lPosition; 

	__DEBUG__("align=", align);
	__DEBUG__("fsize=", fsize);
	__DEBUG__("lr=", lr);
	__DEBUG__("ud=", ud);

	float	fwidth=0;
	float	fheight=0;
	float	ratio = pAnn->m_HeightRatio;

	if(width >= height){
		if(ratio >= (9.0/16.0) ) {  // 세로가 비교적 길다. ex 10/16
			fheight = height;
			fwidth = fheight*(1.0/ratio);
		}else{    // ex 5/16 가로가 비교적 길다. 
			fwidth =  width;
			fheight = float(fwidth)*ratio;
		}
	}else{
		if(ratio >= (16.0/9.0) ) {  // 세로가 비교적 길다. ex 20/9
			fheight = height;
			fwidth = fheight*(1.0/ratio);
		}else{    // ex 2/9 가로가 비교적 길다. 
			fwidth =  width;
			fheight = fwidth*ratio;
		}
	}

	fheight = fheight*(float(fsize)/100.0);
	fwidth  = fwidth*(float(fsize)/100.0);
	
	__DEBUG__("HeightRatio=", ratio);
	__DEBUG__("fwidth=", fwidth);
	__DEBUG__("fheight=", fheight);

	int fx =	int(x+(width/2) - (fwidth/2));
	int fy = 	int(y+(height/2) - (fheight/2));

	switch(align){
		case 0: //center 
			break;  
		case 1: //Top-Left
			fx = x+lr;
			fy = y+ud;
			break;
		case 2: //Top-Middle
			fy = y+ud;
			break;
		case 3: //TOP-Right
			fx = x+width-fwidth-lr;
			fy = y+ud;
			break;
		case 4: //Middle-Left
			fx = x+lr;
			break;
		case 5:	//center
			break;  
		case 6: //Middle-Right
			fx = x+width-fwidth-lr;
			break;
		case 7: //Bottom-Left
			fx = x+lr;
			fy = y+height-fheight-ud;
			break;
		case 8: //Bottom-Middle
			fy = y+height-fheight-ud;
			break;
		case 9: //Bottom-Right;
			fx = x+width-fwidth-lr;
			fy = y+height-fheight-ud;
			break;
		default: break; //center
	}

	m_nPosX	= fx;
	m_nPosY	= fy;
	m_nCx	= int(fwidth);
	m_nCy = int(fheight);

	__DEBUG__("m_nPosX=",	m_nPosX);
	__DEBUG__("m_nPosY=",	m_nPosY);
	__DEBUG__("m_nCx=",		m_nCx);
	__DEBUG__("m_nCy=",		m_nCy);

	MoveWindow(m_nPosX, m_nPosY, m_nCx, m_nCy);
	//this->RedrawWindow();
	//this->OnPaint();
}

void CAnnounceDlg::MoveWindowByABSCoodinate(int x, int y, int width, int height, CAnnounce* pAnn, int minimize/*=1*/)
{
	__DEBUG__("MoveWindowByABSCoodinate",NULL);
	__DEBUG__("x=",x);
	__DEBUG__("y=",y);
	__DEBUG__("width=",width);
	__DEBUG__("height=",height);

	m_nPosX	= pAnn->m_PosX;
	m_nPosY	= pAnn->m_PosY;
	m_nCx	= pAnn->m_Width/minimize;
	m_nCy = pAnn->m_sHeight/minimize;

	__DEBUG__("m_nPosX=",	m_nPosX);
	__DEBUG__("m_nPosY=",	m_nPosY);
	__DEBUG__("m_nCx=",		m_nCx);
	__DEBUG__("m_nCy=",		m_nCy);

	MoveWindow(m_nPosX, m_nPosY, m_nCx, m_nCy);
	//this->RedrawWindow();
	//this->OnPaint();

}
void CAnnounceDlg::MoveWindowFullScreen(int x, int y, int width, int height, CAnnounce* pAnn)
{
	__DEBUG__("MoveWindowFullScreen",NULL);
	__DEBUG__("x=",x);
	__DEBUG__("y=",y);
	__DEBUG__("width=",width);
	__DEBUG__("height=",height);

	m_nPosX	= x;
	m_nPosY	= y;
	m_nCx	= width;
	m_nCy = height;

	__DEBUG__("m_nPosX=",	m_nPosX);
	__DEBUG__("m_nPosY=",	m_nPosY);
	__DEBUG__("m_nCx=",		m_nCx);
	__DEBUG__("m_nCy=",		m_nCy);

	MoveWindow(m_nPosX, m_nPosY, m_nCx, m_nCy);
	//this->RedrawWindow();
	//this->OnPaint();

}
void CAnnounceDlg::MoveAnnounceWindow(int x, int y, int width, int height, CAnnounce* pAnn,int minimize/*=1*/)
{
	__DEBUG__("MoveAnnounceWindow() start", NULL);
	if(pAnn){
		if(pAnn->m_SourceSize==1){  //sourc 비율
			MoveWindowBySourceSize(x, y, width, height, pAnn);
		}else if(m_pAnnPlay->m_SourceSize==0){ //전체스크린
			MoveWindowFullScreen(x, y, width, height,pAnn);
		}else{ //2 절대사이즈
			MoveWindowByABSCoodinate(x, y, width, height,pAnn, minimize);
		}
	}else{
		__DEBUG__("MoveAnnounceWindow() Fail: CAnnounce Object is null", NULL);
	}
	__DEBUG__("MoveAnnounceWindow() end", NULL);

}

BOOL CAnnounceDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(pMsg->wParam == VK_ESCAPE)
	{
		__DEBUG__("ESC pressed", NULL);
		CDialog::OnOK();	
		return true;
	}
	
	if(pMsg->wParam == VK_F12)
	{
		__DEBUG__("F12 pressed", NULL);
		if(m_pAnnPlay && m_pAnnPlay->m_ContentsType != 2){
			static int toggle=0;
			toggle++;
			if((toggle)%4==1){
					ModifyStyle(NULL, WS_CAPTION | WS_SYSMENU | WS_MAXIMIZEBOX | WS_SIZEBOX);
					MoveAnnounceWindow(10, 10, int(m_nParentCx/4), int(m_nParentCy/4), m_pAnnPlay, 4);
					//BringWindowToTop();
					//SetForegroundWindow();
					//SetFocus();
					//if(m_ticker && 	m_ticker->IsPlay()){
					//	m_ticker->BringWindowToTop();
					//	m_ticker->SetForegroundWindow();
					//	m_ticker->SetFocus();
					//}
			}else if((toggle)%4==0){
					ModifyStyle(WS_CAPTION | WS_SYSMENU | WS_MAXIMIZEBOX | WS_SIZEBOX, NULL);
					MoveAnnounceWindow(m_nParentPosX, m_nParentPosY, m_nParentCx, m_nParentCy, m_pAnnPlay);
					//BringWindowToTop();
					//SetForegroundWindow();
					//SetFocus();
					//if(m_ticker && 	m_ticker->IsPlay()){
					//	m_ticker->BringWindowToTop();
					//	m_ticker->SetForegroundWindow();
					//	m_ticker->SetFocus();
					//}
			}

		}else{
			//CDialog::OnOK();
		}
		return true;
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}
/*
BOOL CAnnounceDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
	{
		return TRUE;
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}
*/


ciBoolean
CAnnounceDlg::keyboardSimulator(const char* command)
{

	if(!this->m_pParentWnd) return false;

	__DEBUG__("keyboardSimulator", command);

	HWND brwHwnd = this->m_pParentWnd->GetSafeHwnd();

	/*
	HWND brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2.exe");
	if(brwHwnd==0){
		brwHwnd = scratchUtil::getInstance()->getWHandle("UTV_brwClient2_UBC1.exe");
	}
	*/
	if(brwHwnd){
		COPYDATASTRUCT appInfo;
		appInfo.dwData = UBC_WM_KEYBOARD_EVENT;  // 1000
		appInfo.lpData = (char*)command;
		appInfo.cbData = strlen(command)+1;
		//::PostMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
		__DEBUG__("keyboardSimulator", command);
		::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
	}

	return ciTrue;
}

//skpark 2013.11.28
void
CAnnounceDlg::StopOrRemove(const char* announceId, const char* contentsType)
{
	CString strId = announceId;
	CString debugStr = announceId;
	debugStr += " / ";
	debugStr += contentsType;

	if(strId == "*")
	{
		__DEBUG__("Stop announce play", debugStr);
		StopAnnouncePlay();			//모든 긴급공지를 중지
	}
	else
	{
		__DEBUG__("announce removed", debugStr);
		RemoveAnnounce(strId);	//해당하는 ID의 긴급공지를 중단
	}//if
}