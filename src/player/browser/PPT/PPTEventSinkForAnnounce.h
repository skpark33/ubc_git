// PPTEventSinkForAnnounce.h: interface for the CPPTEventSinkForAnnounce class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PPTEVENTSINKForAnnounce_H__B871B535_7661_4DEA_86DA_20479AB6F90F__INCLUDED_)
#define AFX_PPTEVENTSINKForAnnounce_H__B871B535_7661_4DEA_86DA_20479AB6F90F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "msppt9.h"

// 파워포인트 Type Library의 EApplication 인터페이스 ID
// EApplication : 파워포인트 어플리케이션 이벤트에 대한 인터페이스
static const GUID IID_IEApplication =  
    {0x914934C2,0x5A91,0x11CF, {0x87,0x00,0x00,0xAA,0x00,0x60,0x26,0x3b}};

static BYTE I4_parms[] = VTS_I4;
static BYTE I4I4_parms[] = VTS_I4 VTS_I4;

#define DEFAULT_STARTPAGE_INDEX		1

struct IEApplication : public IDispatch // Pretty much copied from typelib
{
    STDMETHODIMP QueryInterface(REFIID riid, void ** ppvObj) = 0; //HRESULT _stdcall
    STDMETHODIMP_(ULONG) AddRef()  = 0;  // Note the underscore
    STDMETHODIMP_(ULONG) Release() = 0;

    STDMETHODIMP GetTypeInfoCount(UINT *iTInfo) = 0;
    STDMETHODIMP GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo) = 0;
    STDMETHODIMP GetIDsOfNames(REFIID riid, OLECHAR **rgszNames, 
                                  UINT cNames,  LCID lcid, DISPID *rgDispId) = 0;
    STDMETHODIMP Invoke(DISPID dispIdMember, REFIID riid, LCID lcid,
                                  WORD wFlags, DISPPARAMS* pDispParams,
                                  VARIANT* pVarResult, EXCEPINFO* pExcepInfo,
                                  UINT* puArgErr) = 0;
};

class CPPTEventSinkForAnnounce : public IEApplication  
{
public:
	CPPTEventSinkForAnnounce(void* pParent, CString strPresentationName);
	virtual ~CPPTEventSinkForAnnounce();
public:
	DWORD m_Cookie;
	int m_refCount;

	/***** IUnknown Methods *****/
    STDMETHODIMP QueryInterface(REFIID riid, void ** ppvObj);
    STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();

	/***** IDispatch Methods *****/
	STDMETHODIMP LoadTypeInfo(ITypeInfo** pptinfo, REFCLSID clsid, LCID lcid);
    STDMETHODIMP GetTypeInfoCount(UINT *iTInfo);
    STDMETHODIMP GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo);
    STDMETHODIMP GetIDsOfNames(REFIID riid, OLECHAR **rgszNames, UINT cNames,  LCID lcid, DISPID *rgDispId);
    STDMETHODIMP Invoke(DISPID dispIdMember, 
                    REFIID riid, 
                    LCID lcid,
                    WORD wFlags, DISPPARAMS* pDispParams,
                    VARIANT* pVarResult, EXCEPINFO* pExcepInfo,       
					UINT* puArgErr);

	//***** EApplication Methods ****
	STDMETHODIMP SlideShowBegin(IUnknown* Wn);
	STDMETHODIMP SlideShowEnd(IUnknown* Pres);
	STDMETHODIMP PresentationClose(IUnknown* Pres);
	STDMETHODIMP PresentationOpen(IUnknown* Pres);
	STDMETHODIMP SlideShowEscape(IUnknown* Pres);

	 ///// This event used to check un-shown animations, //
    /////  and to reset the m_shapeCount for the new slide. //
    STDMETHODIMP SlideShowNextSlide(IUnknown* Wn);
	
protected:
	void*			m_pParent;								///<부모 창
	int				m_nSlideCount;							///<슬라이드 갯수
	int				m_nActiveSlideNum;						///<현재 진행중인 슬라이드 번호
	bool			m_bIgnoreEvent;							///<이벤트를 무시할지 여부를 나타내는 플래그
	CString			m_strPresentationName;					///<이벤트를 처리해야는 Presentation 이름
};

#endif // !defined(AFX_PPTEVENTSINKForAnnounce_H__B871B535_7661_4DEA_86DA_20479AB6F90F__INCLUDED_)
