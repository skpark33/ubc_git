// PPTEventSinkForAnnounce.cpp: implementation of the CPPTEventSinkForAnnounce class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PPTEventSinkForAnnounce.h"
#include "../Announce_ppt_Dlg.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CPPTEventSinkForAnnounce::CPPTEventSinkForAnnounce(void* pParent, CString strPresentationName)
:	m_refCount(0)
,	m_pParent(NULL)
,	m_nSlideCount(0)
,	m_nActiveSlideNum(0)
,	m_bIgnoreEvent(false)
,	m_strPresentationName(_T(""))
{	
	m_pParent	= pParent;
	m_strPresentationName = strPresentationName;
}

CPPTEventSinkForAnnounce::~CPPTEventSinkForAnnounce()
{

}


/******************************************************************************
*   IUnknown Interfaces -- All COM objects must implement, either directly or 
*   indirectly, the IUnknown interface.
******************************************************************************/
STDMETHODIMP CPPTEventSinkForAnnounce::QueryInterface(REFIID riid, void ** ppvObj)
{
	if (riid == IID_IUnknown)
	{
		*ppvObj = static_cast<IEApplication*>(this);
	}	
	else if (riid == IID_IEApplication)
	{	
		*ppvObj = static_cast<IEApplication*>(this);
	}
	else if (riid == IID_IDispatch)
	{	
		*ppvObj = static_cast<IDispatch*>(this);
	}	
	else
	{
	    char clsidStr[256];
	    WCHAR wClsidStr[256];
	    char txt[512];

	    StringFromGUID2(riid, (LPOLESTR)&wClsidStr, 256);
	    // Convert down to ANSI
	    WideCharToMultiByte(CP_ACP, 0, wClsidStr, -1, clsidStr, 256, NULL, NULL);
	    sprintf(txt, "riid is : %s: Unsupported Interface", clsidStr);

		*ppvObj = NULL;
		return E_NOINTERFACE;
	}
	
	static_cast<IUnknown*>(*ppvObj)->AddRef();
	return S_OK;
}

STDMETHODIMP_(ULONG) CPPTEventSinkForAnnounce::AddRef()
{
	return ++m_refCount;
}

STDMETHODIMP_(ULONG) CPPTEventSinkForAnnounce::Release()
{
	if (--m_refCount == 0)
	{
		delete this;
		return 0;
	}
	return m_refCount;
}

/******************************************************************************
*   IDispatch Interface -- This interface allows this class to be used as an
*   automation server, allowing its functions to be called by other COM
*   objects.  NOT USED IN THIS DEMO
******************************************************************************/
STDMETHODIMP CPPTEventSinkForAnnounce::GetTypeInfoCount(UINT *iTInfo)
{
   return E_NOTIMPL;
}

STDMETHODIMP CPPTEventSinkForAnnounce::GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo)
{
   return E_NOTIMPL;
}

STDMETHODIMP CPPTEventSinkForAnnounce::GetIDsOfNames(REFIID riid,  
                                         OLECHAR **rgszNames, 
                                         UINT cNames,  LCID lcid,
                                         DISPID *rgDispId)
{
   HRESULT hr = E_FAIL;
   return hr;
}


STDMETHODIMP CPPTEventSinkForAnnounce::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid,
                                  WORD wFlags, DISPPARAMS* pDispParams,
                                  VARIANT* pVarResult, EXCEPINFO* pExcepInfo,
                                  UINT* puArgErr)
{
   if ((riid != IID_NULL))
      return E_INVALIDARG;
   
   HRESULT hr = S_OK;  // Initialize

   if(m_strPresentationName == "")
   {
	   __DEBUG__("Empty presentation name", _NULL);
	   return S_OK;
   }//if

   if(m_bIgnoreEvent)
   {
	   __DEBUG__("\tFile name : ", m_strPresentationName);
	   __DEBUG__("\tIgnore Event : ", m_bIgnoreEvent);
	   __DEBUG__("Dispid : ", dispIdMember);
	   return S_OK;
   }//if

/*
   __DEBUG__("========================================================", _NULL);
   __DEBUG__("\tFile name : ", m_strPresentationName);
   switch(dispIdMember)
   {
   case 2001	: __DEBUG__("\tWindowSelectionChange", dispIdMember); break;
   case 2002	: __DEBUG__("\tWindowBeforeRightClick", dispIdMember); break;
   case 2003	: __DEBUG__("\tWindowBeforeDubleClick", dispIdMember); break;
   case 2004	: __DEBUG__("\tPresentationClose", dispIdMember); break;
   case 2005	: __DEBUG__("\tPresentationSave", dispIdMember); break;
   case 2006	: __DEBUG__("\tPresentationOpen", dispIdMember); break;
   case 2007	: __DEBUG__("\tNewPresentation", dispIdMember); break;
   case 2008	: __DEBUG__("\tPresentationNewSlide", dispIdMember); break;
   case 2009	: __DEBUG__("\tWindowActivate", dispIdMember); break;
   case 2010	: __DEBUG__("\tWindowDeactivate", dispIdMember); break;
   case 2011	: __DEBUG__("\tSlideShowBegin", dispIdMember); break;
   case 2012	: __DEBUG__("\tSlideShowNextBuild", dispIdMember); break;
   case 2013	: __DEBUG__("\tSlideShowNextSlide", dispIdMember); break;
   case 2014	: __DEBUG__("\tSlideShowEnd", dispIdMember); break;
   case 2015	: __DEBUG__("\tPresentationPrint", dispIdMember); break;
   case 2016	: __DEBUG__("\tSlideSelectionChanged", dispIdMember); break;
   case 2017	: __DEBUG__("\tColorSchemeChanged", dispIdMember); break;
   case 2018	: __DEBUG__("\tPresentationSave", dispIdMember); break;
   case 2019	: __DEBUG__("\tSlideShowNextClick", dispIdMember); break;
   default		: __DEBUG__("\Unknown", dispIdMember); break;
   }//switch
   __DEBUG__("========================================================", _NULL);
 */

   switch (dispIdMember)
   {
   case 2004:  // PresentationClose( IUnknown* Pres)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   PresentationClose( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   PresentationClose( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;

   case 2006:  // PresentationOpen( IUnknown* Pres)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   PresentationOpen( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   PresentationOpen( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;

   case 2011:   //  SlideShowBegin( SlideShowWindow* Wn)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   SlideShowBegin(/*(SlideShowWindow*)*/*(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   SlideShowBegin(/*(SlideShowWindow*)*/pDispParams->rgvarg[0].punkVal );
				   //SlideShowWindow derived from IUnknown, so cast the punkVal -  but not needed
			   }//if
		   }//if
	   }
	   break;

   case 2013:   // SlideShowNextSlide( SlideShowWindow* Wn)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   SlideShowNextSlide( /*(SlideShowWindow*)*/*(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   SlideShowNextSlide( /*(SlideShowWindow*)*/pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;

   case 2014:   // SlideShowEnd(IUnknown* Pres)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   SlideShowEnd( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   SlideShowEnd( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;
   case 2009:	// ESC key
	   {
		   if(pDispParams->cArgs != 2)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   //SlideShowEscape( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   //SlideShowEscape( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;
   }//switch
 
   return hr;
}

/******************************************************************************
*   EApplication Interface 
/*************************************************************************/


STDMETHODIMP CPPTEventSinkForAnnounce::PresentationClose(IUnknown* Pres)
{
	TRACE(" PresentationClose   \r\n");
	//::Sleep(500);
	return S_OK; 
}

STDMETHODIMP CPPTEventSinkForAnnounce::PresentationOpen(IUnknown* Pres)
{
	CAnnounce_ppt_Dlg* pSchedule = (CAnnounce_ppt_Dlg*)m_pParent;
	pSchedule->PresentationOpen();

	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSinkForAnnounce::SlideShowBegin(IUnknown* Wn)
{
    LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;
	Slides pptSlides;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Wn->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptSlideshowWindow.AttachDispatch(lpDispatch);

	// Get Presentation
	pptPresentation.AttachDispatch(pptSlideshowWindow.GetPresentation());
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if

	// Get Slides
	pptSlides.AttachDispatch(pptPresentation.GetSlides());
		
	// 슬라이드의 전체 수를 구한다.
	m_nSlideCount = pptSlides.GetCount();
	//SlideShowSettings pptSettings = pptPresentation.GetSlideShowSettings();
	//pptSettings.SetLoopUntilStopped(-1);		//msoTrue
	//m_nSlideCount = (int)pptSettings.GetEndingSlide();
	m_nActiveSlideNum = 1;      
	
	pptSlideshowWindow.ReleaseDispatch();
	pptSlides.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();

	CAnnounce_ppt_Dlg* pSchedule = (CAnnounce_ppt_Dlg*)m_pParent;
	pSchedule->SlideShowBegin();
	
	//m_switch = 0; // be sure one-time switch is set to 0
	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSinkForAnnounce::SlideShowEnd(IUnknown* Pres)
{
	LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Pres->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptPresentation.AttachDispatch(lpDispatch);
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if

	CAnnounce_ppt_Dlg* pSchedule = (CAnnounce_ppt_Dlg*)m_pParent;
	//pSchedule->SlideShowEnd();
	pSchedule->Stop();

	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSinkForAnnounce::SlideShowNextSlide(IUnknown* Wn)
{
	LPDISPATCH lpDispatch;
    SlideShowWindow pptSlideshowWindow;
	SlideShowView pptSlideShowView;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Wn->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptSlideshowWindow.AttachDispatch(lpDispatch);

	// Get SlideShowView	
	pptSlideShowView.AttachDispatch(pptSlideshowWindow.GetView());
	m_nActiveSlideNum = pptSlideShowView.GetCurrentShowPosition();

	__DEBUG__("Active slide show number : ", m_nActiveSlideNum);
	
	/*pptSlideshowWindow.ReleaseDispatch();
	pptSlideShowView.ReleaseDispatch();*/
	
	if(m_nActiveSlideNum >= m_nSlideCount)
	{
		//m_bIgnoreEvent = true;
		_Slide pptSlide = pptSlideShowView.GetSlide();
		SlideShowTransition pptTransition = pptSlide.GetSlideShowTransition();
		if(pptTransition.GetAdvanceOnTime() == -1/*msoTrue*/)
		{
			float ftTime = pptTransition.GetAdvanceTime();
			__DEBUG__("Last slide waiting time", ftTime);
			if(ftTime != 0)
			{
				CAnnounce_ppt_Dlg* pSchedule = (CAnnounce_ppt_Dlg*)m_pParent;
				pSchedule->SetSlideShowEnd(ftTime);
			}//if
		}//if
		//pSchedule->Stop();
	}//if

	pptSlideshowWindow.ReleaseDispatch();
	pptSlideShowView.ReleaseDispatch();

	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSinkForAnnounce::SlideShowEscape(IUnknown* Pres)
{
	/*
	LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Pres->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptPresentation.AttachDispatch(lpDispatch);
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if
	*/

	m_bIgnoreEvent = true;
	CAnnounce_ppt_Dlg* pSchedule = (CAnnounce_ppt_Dlg*)m_pParent;
	//pSchedule->SlideShowEnd();
	pSchedule->EscapeBRW();

	//Sleep(50);
	return S_OK; 
}
