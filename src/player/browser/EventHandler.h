#pragma once

#include "cci/libWrapper/cciEventHandler.h"


class CEventHandler : public cciEventHandler
{
public:
	CEventHandler(CWnd* pWnd);
	virtual ~CEventHandler();

protected:
	CWnd*		m_pWnd;

public:

	void processEvent(cciEvent& event);
};


