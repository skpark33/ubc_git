#pragma once

#include "ximage.h"
#include "PictureEx.h"
#include "OverlayInterface.h"

#include "AnnounceDlg.h"

// CAnnounce_video_Dlg 대화 상자입니다.

class CAnnounce_video_Dlg : public CAnnounceDlg
{
	DECLARE_DYNAMIC(CAnnounce_video_Dlg)

protected:
	DECLARE_MESSAGE_MAP()

public:
	CAnnounce_video_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAnnounce_video_Dlg();

	void		BeginAnnounce(CAnnounce* pAnn);					///<공지 방송을 시작하도록 한다.
	void		EndAnnounce();									///<공지 방송을 중지하도록 한다.

	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnGraphNotify(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompletePlay(WPARAM wParam, LPARAM lParam);

protected:
	
	virtual bool	Play();
	virtual bool	Stop();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	bool			OpenDSRender(void);						///<Directshow render를 옵션에 맞추어 설정하고 그래프 빌더를 구성한다.
	void			CloseDSRender(void);					///<Directshow render를 종료한다.

	void			SetNoContentsFile();
	void			RePlay();

	CBasicInterface*	m_pInterface;
	int					m_nVedioRenderMode;
	bool				m_bCompletePlay;
	CCriticalSection	m_csLock;							///<필터그래프 이벤트 처리 동기화 

	CxImage				m_bgImage;
	bool				m_bOpen;
	bool				m_bFileNotExist;
	CString				m_strMediaFullPath;

public:
};
