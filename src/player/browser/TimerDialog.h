#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "ProgressCtrlEx.h"

// CTimerDialog 대화 상자입니다.

class CTimerDialog : public CDialog
{
	DECLARE_DYNAMIC(CTimerDialog)

public:
	CTimerDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTimerDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TIMER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CWnd*	m_pParentWnd;
	int		m_nCurrentPos;

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CStatic m_staticText;
	CProgressCtrlEx		m_progressTimer;

	void	Start();
	CButton m_btnOK;
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
