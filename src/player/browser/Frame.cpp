// Frame.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "Host.h"
#include "Template.h"
#include "Frame.h"

#include "MemDC.h"


// CFrame

#define		SCHEDULE_CHECK_ID		3000	// id
//#define		SCHEDULE_CHECK_TIME		100		// time(ms)
#define		SCHEDULE_CHECK_TIME		50		// time(ms)
//#define		SCHEDULE_CHECK_TIME		300		// time(ms)

#define		FRAME_SHOW_ID			3001	// id
#define		FRAME_SHOW_TIME			50		// time(ms)

#define		FRAME_HIDE_ID			3002	// id
#define		FRAME_HIDE_TIME			50		// time(ms)


UINT	CFrame::m_nFrameID = 0xff00;

CFrame*	CFrame::m_pPhoneFrame = NULL;
CCriticalSection CFrame::m_csFramePhone;


IMPLEMENT_DYNAMIC(CFrame, CWnd)


CFrame*	CFrame::GetFrameObject(CTemplate* pParent, LPCSTR lpszID, CString& strErrorMessage)
{
	__DEBUG__("\t\tCreate Frame", lpszID);

	// check valid
	CFrame* frame = new CFrame(pParent, lpszID);

	if(frame != NULL)
	{
		if(frame->IsPIP())	// PIP 일경우 그냥 리턴 (나중에 create)
			return frame;

		return CreatePIPFrameObject(pParent, frame, strErrorMessage);
	}

	strErrorMessage = "Fail to create frame !!!";

	return NULL;
}

CFrame* CFrame::CreatePIPFrameObject(CTemplate* pParent, CFrame* pFrame, CString& strErrorMessage)
{
	BOOL ret = pFrame->Create(NULL, "Frame", WS_CHILD, CRect(0,0,100,100), pParent, m_nFrameID--);
	if(ret)
	{
		return pFrame;
	}
	else
	{
		strErrorMessage = "Fail to create Frame !!!";
		__ERROR__("\t\tFail to create Frame !!!", _NULL);

		delete pFrame;
		return NULL;
	}
}


CFrame::CFrame(CTemplate* pParentWnd, LPCSTR lpszID)
:	m_nCurrentScheduleIndex(-1)
,	m_nCurrentDefaultScheduleIndex(-1)
,	m_pCurrentSchedule(NULL)
,	m_bCurrentPlay(false)
,	m_pPIPParentWnd(NULL)
,	m_pParentWnd(pParentWnd)
,	m_strErrorMessage ("")
,	m_strLoadID (lpszID)
,	m_nPlayCount (0)
,	m_boolScheduleCheckFlag (false)
,	m_pclsPalyRule(NULL)
,	m_nIndexPlayOrder(-1)
,	m_bPause(false)
,	m_bIsHDFrame(false)
{
	m_templateId = MNG_PROFILE_READ(lpszID, "templateId");
	m_frameId = MNG_PROFILE_READ(lpszID, "frameId");
	m_grade = atoi(MNG_PROFILE_READ(lpszID, "grade"));
	m_width = atoi(MNG_PROFILE_READ(lpszID, "width"));
	m_height = atoi(MNG_PROFILE_READ(lpszID, "height"));
	m_x = atoi(MNG_PROFILE_READ(lpszID, "x"));
	m_y = atoi(MNG_PROFILE_READ(lpszID, "y"));
	m_isPIP = atoi(MNG_PROFILE_READ(lpszID, "isPIP"));
	m_borderStyle = MNG_PROFILE_READ(lpszID, "borderStyle");
	m_borderThickness = atoi(MNG_PROFILE_READ(lpszID, "borderThickness"));
	m_borderColor = MNG_PROFILE_READ(lpszID, "borderColor");
	m_cornerRadius = atoi(MNG_PROFILE_READ(lpszID, "cornerRadius"));

	if(atoi(MNG_PROFILE_READ(lpszID, "comment1")) == 9)
	{
		m_bIsHDFrame = true;
	}
	else
	{
		m_bIsHDFrame = false;
	}//if

	if(m_borderColor.length() > 1)
	{
		m_rgbBorderColor = ::GetColorFromString(m_borderColor.c_str()+1);
	}

	CString strPhoneTemplateId, stPhoneFrameId;
	int nPortNo;
	::GetPhoneConfig(strPhoneTemplateId, stPhoneFrameId, nPortNo);

	if(strPhoneTemplateId == m_templateId.c_str() &&
		stPhoneFrameId == m_frameId.c_str() )
	{
		m_pPhoneFrame = this;
	}

	_sch = NULL;
}

CFrame::~CFrame()
{
	__DEBUG__("\t\tDestroy Frame", m_frameId.c_str());

	if(CFrame::m_pPhoneFrame == this)
		CFrame::m_pPhoneFrame = NULL;
}


BEGIN_MESSAGE_MAP(CFrame, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_MESSAGE(ID_PLAY_NEXT_SCHEDULE, OnPlayNextSchedule)
	ON_MESSAGE(WM_DEBUG_STRING, OnDebugString)
	ON_MESSAGE(WM_DEBUG_GRID, OnDebugGrid)
	ON_MESSAGE(WM_ERROR_MESSAGE, OnErrorMessage)
	ON_MESSAGE(WM_HIDE_CONTENTS, OnHideContents)
	ON_MESSAGE(WM_TOGGLE_SOUND_VOLUME_MUTE, OnSoundVolumeMute)
END_MESSAGE_MAP()


// CFrame 메시지 처리기입니다.


int CFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	__DEBUG_FRAME_BEGIN__(m_frameId.c_str())

	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	CLogDialog::getInstance()->Add(m_pParentWnd, this, m_frameId.c_str(), 4);

	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);
	MoveWindow(
		(int)(m_x * xscale), 
		(int)(m_y * yscale), 
		(int)(m_width * xscale), 
		(int)(m_height * yscale)
	);

	LoadSchedule(true);
	//if(IsUseTimeBaseSchedule())
	//{
		LoadSchedule(false);
	//}//if
	
	__DEBUG_FRAME_END__(m_frameId.c_str())

	return 0;
}

void CFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	if(m_borderStyle == "solid")
	{
		if(m_borderThickness > 0)
		{
			if(m_cornerRadius == 0)
				DrawSquareFrame(&dc);	// 직사각형
			else
				DrawRoundFrame(&dc);	// 둥근 사각형
		}
	}
	else if(m_borderStyle == "inset")
	{
		//
	}
	else if(m_borderStyle == "outset")
	{
		//
	}
	else if(m_borderStyle == "none")
	{
		//
	}
}

void CFrame::OnDestroy()
{
	__DEBUG_FRAME_BEGIN__(m_frameId.c_str())

	KillTimer(FRAME_SHOW_ID);
	KillTimer(FRAME_HIDE_ID);

	m_boolScheduleCheckFlag = false;
	KillTimer(SCHEDULE_CHECK_ID);

	// delete schedule
	int count = m_listSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listSchedule[i];
		schedule->DestroyWindow();
		delete schedule;
	}
	m_listSchedule.RemoveAll();

	// delete default schedule
	count = m_listDefaultSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listDefaultSchedule[i];
		schedule->DestroyWindow();
		delete schedule;
	}
	m_listDefaultSchedule.RemoveAll();

	ClearPIPFrame();

	m_mapSchedule.RemoveAll();

	CWnd::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	__DEBUG_FRAME_END__(m_frameId.c_str())
}

void CFrame::DrawSquareFrame(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);

	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);

	int xborder_thickness = (int)(m_borderThickness * xscale);
	int yborder_thickness = (int)(m_borderThickness * yscale);

	// top
	CRect top;
	top = rect;
	top.bottom = top.top + yborder_thickness;
	pDC->FillSolidRect(top, m_rgbBorderColor);

	// bottom
	CRect bottom;
	bottom = rect;
	bottom.top = bottom.bottom - yborder_thickness;
	pDC->FillSolidRect(bottom, m_rgbBorderColor);

	// left
	CRect left;
	left = rect;
	left.right = left.left + xborder_thickness;
	pDC->FillSolidRect(left, m_rgbBorderColor);

	// right
	CRect right;
	right = rect;
	right.left = right.right - xborder_thickness;
	pDC->FillSolidRect(right, m_rgbBorderColor);
}

void CFrame::DrawRoundFrame(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);

	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);

	int xborder_thickness = (int)(m_borderThickness * xscale);
	int yborder_thickness = (int)(m_borderThickness * yscale);

	// draw mask
	{
		CMemDCAlt memDC(pDC, rect, xborder_thickness, yborder_thickness, SRCAND);

		memDC.FillSolidRect(rect, RGB(255,255,255));

		CBrush brushBlue(RGB(0, 0, 0));
		CBrush* pOldBrush = memDC.SelectObject(&brushBlue);

		CPen penBlack;
		penBlack.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
		CPen* pOldPen = memDC.SelectObject(&penBlack);

		memDC.RoundRect(rect, CPoint(xborder_thickness, yborder_thickness));

		memDC.SelectObject(pOldBrush);
		memDC.SelectObject(pOldPen);

		brushBlue.DeleteObject();
		penBlack.DeleteObject();
	}

	// draw original
	{
		CMemDCAlt memDC(pDC, rect, xborder_thickness, yborder_thickness, SRCPAINT);

		CBrush brushBlue(m_rgbBorderColor);
		CBrush* pOldBrush = memDC.SelectObject(&brushBlue);

		CPen penBlack;
		penBlack.CreatePen(PS_SOLID, 1, m_rgbBorderColor);
		CPen* pOldPen = memDC.SelectObject(&penBlack);

		memDC.RoundRect(rect, CPoint(xborder_thickness, yborder_thickness));

		memDC.SelectObject(pOldBrush);
		memDC.SelectObject(pOldPen);

		brushBlue.DeleteObject();
		penBlack.DeleteObject();
	}
}

void CFrame::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case FRAME_SHOW_ID:
		KillTimer(FRAME_SHOW_ID);
		if(!::GetScheduleResuming())
			ShowWindow(SW_SHOW);
		break;

	case FRAME_HIDE_ID:
		KillTimer(FRAME_HIDE_ID);
		if(!::GetScheduleResuming())
			ShowWindow(SW_HIDE);
		break;

	case SCHEDULE_CHECK_ID:
		{
			KillTimer(SCHEDULE_CHECK_ID);
			if(m_boolScheduleCheckFlag)
			{
				if(CheckSchedule())
				{
					m_boolScheduleCheckFlag = true;
					SetTimer(SCHEDULE_CHECK_ID, SCHEDULE_CHECK_TIME, NULL);
				}
				else
					m_boolScheduleCheckFlag = false;
			}
		}
		break;

	case SCHEDULE_STOP_ID:
		KillTimer(SCHEDULE_STOP_ID);
		((CVideoSchedule*)_sch)->StopEx();
		break;
	}
}

bool CFrame::CheckSchedule()
{
	//if(m_bPause == true)
	//{
	//	return true;
	//}//if

	if(m_bCurrentPlay == false)
	{
		return false;
	}//if

	if(m_pCurrentSchedule == NULL)
	{
		PlayNextSchedule();
		return true;
	}//if

	int new_sch_index;
	CSchedule* old_schedule = NULL;
	double lCur, lTotal;

	old_schedule = m_pCurrentSchedule;

	if(m_pCurrentSchedule->IsDefaultSchedule())
	{
		if(GetNextSchedule(new_sch_index))		//정시 스케줄이 최우선
		{
			m_nCurrentScheduleIndex = new_sch_index;
			m_pCurrentSchedule = m_listSchedule[new_sch_index];

			if(old_schedule != m_pCurrentSchedule)
			{
				m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
				old_schedule->Stop();
				m_pCurrentSchedule->Play();
			}
			else 
			{
				m_pCurrentSchedule->RePlay();	//RePlay
			}//if
		}
		else if(GetNextChildSchedule(new_sch_index))
		{
			//부모스케줄이 설정된 스케줄을 먼저 검사하여 부모스케줄이 방송중이면 
			//연결된 자식 스케줄이 우선적으로 방송되어야 한다.(정시 스케줄은 자식 스케줄이 될 수 없다)
			CSchedule* pNewChild = m_listDefaultSchedule[new_sch_index];
			//m_pCurrentSchedule = m_listDefaultSchedule[new_sch_index];

			if(strcmp(old_schedule->GetParentScheduleId(), pNewChild->GetParentScheduleId()) != 0)
			{
				//부모 스케줄이 다르다면 재생중인 스케줄을 변경
				m_nCurrentDefaultScheduleIndex = new_sch_index;
				m_pCurrentSchedule = pNewChild;
				m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
				old_schedule->Stop();
				m_pCurrentSchedule->Play();
			}
			else
			{
				//부모 스케줄이 같다면 재생시간이 지났는지를 보고 스케줄이 변경되어야 하는지를 판단...
				lCur = old_schedule->GetCurrentPlayTime();
				lTotal = old_schedule->GetTotalPlayTime();

				if((old_schedule->IsBetween() &&  lTotal <= lCur) ||
					old_schedule->IsBetween() == false)
				{
					if(old_schedule != pNewChild)
					{
						m_nCurrentDefaultScheduleIndex = new_sch_index;
						m_pCurrentSchedule = pNewChild;
						m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
						old_schedule->Stop();
						m_pCurrentSchedule->Play();
					}
					else
					{
						m_pCurrentSchedule->RePlay();	//RePlay
					}//if
				}
				else if(!m_pCurrentSchedule->IsPlay())
				{
					m_pCurrentSchedule->Play();
				}//if
			}//if
		}
		else
		{
			lCur = m_pCurrentSchedule->GetCurrentPlayTime();
			lTotal = m_pCurrentSchedule->GetTotalPlayTime();

			if((m_pCurrentSchedule->IsBetween() && lTotal <= lCur) ||
				m_pCurrentSchedule->IsBetween() == false)
			{

				if (::GetScheduleResuming() && stricmp(m_pParentWnd->GetSyncCommand(),"null")==0)
				{
					m_boolScheduleCheckFlag = false;
					KillTimer(SCHEDULE_CHECK_ID);

					__DEBUG__("PostMessage(WM_PLAY_NEXT_DEFAULT_TEMPLATE)", _NULL);
					::AfxGetMainWnd()->PostMessage(WM_PLAY_NEXT_DEFAULT_TEMPLATE);
					return false;
				}

				if(GetNextDefaultSchedule(new_sch_index))
				{
					m_pCurrentSchedule = m_listDefaultSchedule[new_sch_index];

					CHost* pclsHost = (CHost*)::AfxGetMainWnd();
					if(m_grade == 1 && pclsHost->m_bPauseTemplate == false)
					{
						int play_count = m_pCurrentSchedule->GetPlayCount();
						//단축키를 이용하여 재생되었을 경우에는 play count가 0인 경우가 있다.
						//이를 처리하기 위해서는 템플릿의 play count가 0인경우 별도 처리하여야 한다.
						if(m_nPlayCount != 0 || (m_nPlayCount==0 && ::GetScheduleResuming()))
						{
							play_count++;
						}//if
						//if(m_nPlayCount >= 0 && play_count >= m_nPlayCount && !pclsHost->m_bClickJump)
						if( (play_count > m_nPlayCount && (!pclsHost->m_bClickJump || ::GetScheduleResuming())) ||
							(play_count > m_nPlayCount+1 && m_nPlayCount==0 && pclsHost->m_bClickJump && ::GetScheduleResuming()) )
						{
							m_boolScheduleCheckFlag = false;
							KillTimer(SCHEDULE_CHECK_ID);

							__DEBUG__("PostMessage(WM_PLAY_NEXT_DEFAULT_TEMPLATE)", play_count);
							::AfxGetMainWnd()->PostMessage(WM_PLAY_NEXT_DEFAULT_TEMPLATE);
							m_pCurrentSchedule = old_schedule;
							return false;
						}//if
					}//if

					m_nCurrentDefaultScheduleIndex = new_sch_index;

					if(old_schedule != m_pCurrentSchedule)
					{
						m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
						old_schedule->Stop();
						m_pCurrentSchedule->Play();
					}
					else
					{
						m_pCurrentSchedule->RePlay();	//RePlay
					}//if					
				}
				else
				{
					m_pCurrentSchedule->RePlay();	//RePlay			
				}//if
			}
			else if(!m_pCurrentSchedule->IsPlay())
			{
				m_pCurrentSchedule->Play();
			}//if
		}//if
	}
	else
	{
		lCur = m_pCurrentSchedule->GetCurrentPlayTime();
		lTotal = m_pCurrentSchedule->GetTotalPlayTime();

		if((m_pCurrentSchedule->IsBetween() && lTotal <= lCur) ||
			m_pCurrentSchedule->IsBetween() == false)
		{
			if(GetNextSchedule(new_sch_index))
			{
				m_nCurrentScheduleIndex = new_sch_index;
				m_pCurrentSchedule = m_listSchedule[new_sch_index];

				if(old_schedule != m_pCurrentSchedule)
				{
					m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
					old_schedule->Stop();
					m_pCurrentSchedule->Play();
				}
				else
				{
					m_pCurrentSchedule->RePlay();	//RePlay
				}//if
			}
			else if(GetNextChildSchedule(new_sch_index))
			{
				//부모스케줄이 설정된 스케줄을 먼저 검사하여 부모스케줄이 방송중이면 
				//연결된 자식 스케줄이 우선적으로 방송되어야 한다.(정시 스케줄은 자식 스케줄이 될 수 없다)
				CSchedule* pNewChild = m_listDefaultSchedule[new_sch_index];
				//m_pCurrentSchedule = m_listDefaultSchedule[new_sch_index];

				if(strcmp(old_schedule->GetParentScheduleId(), pNewChild->GetParentScheduleId()) != 0)
				{
					//부모 스케줄이 다르다면 재생중인 스케줄을 변경
					m_nCurrentDefaultScheduleIndex = new_sch_index;
					m_pCurrentSchedule = pNewChild;
					m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
					old_schedule->Stop();
					m_pCurrentSchedule->Play();
				}
				else
				{
					//부모 스케줄이 같다면 재생시간이 지났는지를 보고 스케줄이 변경되어야 하는지를 판단...
					lCur = old_schedule->GetCurrentPlayTime();
					lTotal = old_schedule->GetTotalPlayTime();

					if((old_schedule->IsBetween() &&  lTotal <= lCur) ||
						old_schedule->IsBetween() == false)
					{
						if(old_schedule != pNewChild)
						{
							m_nCurrentDefaultScheduleIndex = new_sch_index;
							m_pCurrentSchedule = pNewChild;
							m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
							old_schedule->Stop();
							m_pCurrentSchedule->Play();
						}
						else
						{
							m_pCurrentSchedule->RePlay();	//RePlay
						}//if
					}
					else if(!m_pCurrentSchedule->IsPlay())
					{
						m_pCurrentSchedule->Play();
					}//if
				}//if
			}
			else if(GetNextDefaultSchedule(new_sch_index))
			{
				m_pCurrentSchedule = m_listDefaultSchedule[new_sch_index];

				CHost* pclsHost = (CHost*)::AfxGetMainWnd();
				if(m_grade == 1 && pclsHost->m_bPauseTemplate == false)
				{
					int play_count = m_pCurrentSchedule->GetPlayCount();
					//단축키를 이용하여 재생되었을 경우에는 play count가 0인 경우가 있다.
					//이를 처리하기 위해서는 템플릿의 play count가 0인경우 별도 처리하여야 한다.
					if(m_nPlayCount != 0)
					{
						play_count++;
					}//if
					//if(m_nPlayCount >= 0 && play_count >= m_nPlayCount && !pclsHost->m_bClickJump)
					if( (play_count > m_nPlayCount && !pclsHost->m_bClickJump) ||
						(play_count > m_nPlayCount+1 && m_nPlayCount==0 && pclsHost->m_bClickJump && ::GetScheduleResuming()) )
					{
						m_boolScheduleCheckFlag = false;
						KillTimer(SCHEDULE_CHECK_ID);

						__DEBUG__("PostMessage(WM_PLAY_NEXT_DEFAULT_TEMPLATE)", _NULL);
						::AfxGetMainWnd()->PostMessage(WM_PLAY_NEXT_DEFAULT_TEMPLATE);
						m_pCurrentSchedule = old_schedule;
						return false;
					}//if
				}//if

				if(old_schedule != m_pCurrentSchedule)
				{
					m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
					old_schedule->Stop();
					m_nCurrentDefaultScheduleIndex = new_sch_index;
					m_pCurrentSchedule->Play();
				}
				else
				{
					m_pCurrentSchedule->RePlay();	//RePlay
				}//if	
			}
			else
			{
				m_pCurrentSchedule->RePlay();	//RePlay
			}//if			
		}
		else if(!m_pCurrentSchedule->IsPlay())
		{
			m_pCurrentSchedule->Play();
		}
		//if
	}//if

	return true;
}

bool CFrame::OpenFile()
{
	__DEBUG_FRAME_BEGIN__(m_frameId.c_str())

	POSITION pos = m_mapSchedule.GetStartPosition();
	if(pos == NULL)
	{
		__WARNING__("\t\t(Default)Schedule Map is NULL !!!", _NULL);
	}
	else
	{
		while(pos != NULL)
		{
			CString scheduleId;
			CSchedule* schedule;
			m_mapSchedule.GetNextAssoc( pos, scheduleId, (void*&)schedule );

			if(schedule->IsOpen() == false)
				schedule->OpenFile(m_grade);
		}
	}

	__DEBUG_FRAME_END__(m_frameId.c_str())

	return true;
}

bool CFrame::IsBetween(bool bDefaultSchedule)
{
	bool between = false;
	if(bDefaultSchedule)
	{
		if(m_pclsPalyRule != NULL && m_pclsPalyRule->m_bUsePlayCount == false)	//schedule의 play order을 지정하여 재생하는 경우
		{
			//m_pclsPalyRule->m_aryPlayOrder에 있는 schedule만을 검사한다
			CString strPlayOrder;
			CSchedule* pclsSchedule = NULL;
			int nIdx;
			for(int i=0; i<m_pclsPalyRule->m_aryPlayOrder.GetSize(); i++)
			{
				strPlayOrder = m_pclsPalyRule->m_aryPlayOrder.GetAt(i);
				nIdx = FindDefaultScheduleIndexByPlayOrder(strPlayOrder);
				if(nIdx != -1)
				{
					pclsSchedule = m_listDefaultSchedule.GetAt(nIdx);
					if(pclsSchedule->IsDefaultSchedule() == bDefaultSchedule)
					{
						between |= pclsSchedule->IsBetween();
					}//if
				}//
			}//for
		}
		else
		{
			int count = m_listDefaultSchedule.GetCount();
			for(int i=0; i<count; i++)
			{
				CSchedule* schedule = m_listDefaultSchedule.GetAt(i);
				if(schedule->IsDefaultSchedule() == bDefaultSchedule)
					between |= schedule->IsBetween();
			}//for
		}//if
	}
	else
	{
		int count = m_listSchedule.GetCount();
		for(int i=0; i<count; i++)
		{
			CSchedule* schedule = m_listSchedule.GetAt(i);
			if(schedule->IsDefaultSchedule() == bDefaultSchedule)
				between |= schedule->IsBetween();
		}
	}

	return between;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Tmplate play rule을 설정하여 play하도록 하는 함수 \n
/// @param (CTemplatePlay*) pclsPlayRule : (in) Template play rule을 지정하는 클래스
/// @return <형: void> \n
/////////////////////////////////////////////////////////////////////////////////
void CFrame::Play(CTemplatePlay* pclsPlayRule)
{
	if(m_bCurrentPlay == true) return;

	__DEBUG__("\t\tPlay Frame", m_frameId.c_str());

	m_pclsPalyRule = pclsPlayRule;
	if(m_pclsPalyRule)
	{
		m_nPlayCount = m_pclsPalyRule->m_unPlayCount;
		m_nIndexPlayOrder = -1;
	}//if

	CLogDialog::getInstance()->Play(this);

	KillTimer(FRAME_HIDE_ID);
	SetTimer(FRAME_SHOW_ID, FRAME_SHOW_TIME, NULL);
	m_bCurrentPlay = true;

	/*
	if(m_pPIPParentWnd)
	{
		m_pPIPParentWnd->SetClipRgn(m_x, m_y, m_width, m_height);
	}
	*/
	if(m_listPIPFrame.GetCount() != 0)
	{
		SetPIPClipRgn();
	}//if

	if(	::GetScheduleResuming() <= 0 ||
		m_nCurrentDefaultScheduleIndex >= m_listDefaultSchedule.GetCount()-1 )
	{
		// 1. 이전 템플릿재생을 사용하지 않는 경우
		// 2. 이전 템플릿재생이 남아있을 경우
		__DEBUG__("Reset DefaultScheduleIndex !!!", _NULL);
		ResetPlayCount();
	}

	PlayNextSchedule();

	if(::GetScheduleResuming())
	{
		if(m_grade==1)
		{
			// Primary Frame을 맨처음 보여줌
			CHost::PushFrontToShowWindow(GetSafeHwnd());
		}
		else
		{
			// Secondary Frame을 나중에 보여줌
			CHost::PushBackToShowWindow(GetSafeHwnd());
		}
	}
}

void CFrame::Stop()
{
	if(m_bCurrentPlay == false) return;

	__DEBUG__("\t\tStop Frame", m_frameId.c_str());

	CLogDialog::getInstance()->Stop(this);

	m_bCurrentPlay = false;

	m_boolScheduleCheckFlag = false;
	KillTimer(SCHEDULE_CHECK_ID);

	KillTimer(FRAME_SHOW_ID);
	SetTimer(FRAME_HIDE_ID, FRAME_HIDE_TIME, NULL);
	if(::GetScheduleResuming())
		CHost::PushBackToHideWindow(GetSafeHwnd());

	//if(m_pPIPParentWnd)
	if(m_listPIPFrame.GetCount() != 0)
	{
		ResetClipRgn();
	}//if

	if(m_pCurrentSchedule)
	{
		m_pCurrentSchedule->Stop();
		m_pCurrentSchedule = NULL;
	}

	m_nCurrentScheduleIndex = -1;
	if(::GetScheduleResuming() <= 0)
	{
		// 이전 템플릿재생을 사용하지 않는 경우 ==> 무조건 처음부터 재생
		m_nCurrentDefaultScheduleIndex = -1;
	}
	else
	{
		if(m_nCurrentDefaultScheduleIndex>=m_listDefaultSchedule.GetCount()-1)
		{
			CString template_id = m_pParentWnd->GetTemplateId();
			CString frame_id = this->GetFrameId();
			__DEBUG__("Reset DefaultScheduleIndex !!!", (template_id + "_" + frame_id));
			ResetPlayCount();
			m_nCurrentDefaultScheduleIndex = -1;
		}
	}
}

void CFrame::PlayNextSchedule()
{
	__DEBUG_FRAME_BEGIN__(m_frameId.c_str())

	if(m_listSchedule.GetCount() > 0)
	{
		int index;
		if(GetNextSchedule(index))
		{
			CSchedule* old_schedule = m_pCurrentSchedule;

			m_nCurrentScheduleIndex = index;
			m_pCurrentSchedule = m_listSchedule[index];

			if(old_schedule && old_schedule != m_pCurrentSchedule)
			{
				m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
				old_schedule->Stop();
			}//if
			//skpark 2013.2.16 성공한 경우에 성공 메세지를 Host 객체에 보내어 Error 를 clear 하도록 한다.skpark_manager_err
			//m_pCurrentSchedule->Play();
			if(m_pCurrentSchedule->Play()){
				m_pCurrentSchedule->OnErrorMessage((WPARAM)"ok", (LPARAM)m_pCurrentSchedule->GetScheduleId());
			}
			

			m_boolScheduleCheckFlag = true;
			SetTimer(SCHEDULE_CHECK_ID, SCHEDULE_CHECK_TIME, NULL);

			__DEBUG__("\t\tPlay Schedule", _NULL);

			return;
		}
	}

	if(m_listDefaultSchedule.GetCount() > 0)
	{
		int index;
		/*
		if(GetNextChildSchedule(index))
		{
			CSchedule* old_schedule = m_pCurrentSchedule;
			//부모스케줄이 설정된 스케줄을 먼저 검사하여 부모스케줄이 방송중이면 
			//연결된 자식 스케줄이 우선적으로 방송되어야 한다.(정시 스케줄은 자식 스케줄이 될 수 없다)
			CSchedule* pNewChild = m_listDefaultSchedule[index];
			//m_pCurrentSchedule = m_listDefaultSchedule[new_sch_index];

			if(strcmp(old_schedule->GetParentScheduleId(), pNewChild->GetParentScheduleId()) != 0)
			{
				//부모 스케줄이 다르다면 재생중인 스케줄을 변경
				m_nCurrentDefaultScheduleIndex = index;
				m_pCurrentSchedule = pNewChild;
				if(old_schedule)
				{
					old_schedule->Stop();
				}//if

				m_pCurrentSchedule->Play();
			}
			else if(
			{
				//부모 스케줄이 같다면 재생시간이 지났는지를 보고 스케줄이 변경되어야 하는지를 판단...
				lCur = old_schedule->GetCurrentPlayTime();
				lTotal = old_schedule->GetTotalPlayTime();

				if((old_schedule->IsBetween() &&  lTotal <= lCur) ||
					old_schedule->IsBetween() == false)
				{
					if(old_schedule != pNewChild)
					{
						m_nCurrentDefaultScheduleIndex = new_sch_index;
						m_pCurrentSchedule = pNewChild;
						old_schedule->Stop();
						m_pCurrentSchedule->Play();
					}
					else if(m_pCurrentSchedule->GetContentsType() == CONTENTS_VIDEO		//rewind 기능 구현할 것
						|| m_pCurrentSchedule->GetContentsType() == CONTENTS_FLASH
						|| m_pCurrentSchedule->GetContentsType() == CONTENTS_PPT)
					{
						m_pCurrentSchedule->Stop();
						m_pCurrentSchedule->Play();
					}//if
				}
				else if(!m_pCurrentSchedule->IsPlay())
				{
					m_pCurrentSchedule->Play();
				}//if
			}//if
		}
		else
		{
		*/
			if(GetNextDefaultSchedule(index))
			{
				CSchedule* old_schedule = m_pCurrentSchedule;

				m_pCurrentSchedule = m_listDefaultSchedule[index];
				if(m_grade == 1)
				{
					int play_count = m_pCurrentSchedule->GetPlayCount();
					if( (m_nPlayCount > 0 && play_count >= m_nPlayCount) ||
						(m_nPlayCount == 0 && play_count >= 1 && ::GetScheduleResuming()) )
					{
						m_boolScheduleCheckFlag = false;
						KillTimer(SCHEDULE_CHECK_ID);

						//jwh184 이부분을 수정해야 할것 같네~~~~
						__DEBUG__("PostMessage(WM_PLAY_NEXT_DEFAULT_TEMPLATE)", play_count);
						::AfxGetMainWnd()->PostMessage(WM_PLAY_NEXT_DEFAULT_TEMPLATE);
						/*
						if(old_schedule && old_schedule != m_pCurrentSchedule)
						old_schedule->Stop();
						*/
						m_pCurrentSchedule = old_schedule;

						return;
					}//if
				}//if

				m_nCurrentDefaultScheduleIndex = index;

				if(old_schedule && old_schedule != m_pCurrentSchedule)
				{
					m_pCurrentSchedule->m_pPrevPlayedSchedule = old_schedule;
					old_schedule->Stop();
				}//if
				m_pCurrentSchedule->Play();

				m_boolScheduleCheckFlag = true;
				SetTimer(SCHEDULE_CHECK_ID, SCHEDULE_CHECK_TIME, NULL);

				__DEBUG__("\t\tPlay DefaultSchedule", _NULL);

				return;
			}//if
		//}//if
	}//if

	__DEBUG_FRAME_END__(m_frameId.c_str())
}

bool CFrame::GetNextSchedule(int& index)
{
	if(m_listSchedule.GetCount() == 0)
	{
		return false;
	}

	index = m_nCurrentScheduleIndex+1;

	bool find = false;
	bool round = false;

	while(1)
	{
		if(index >= m_listSchedule.GetCount())
		{
			if(round == true)
			{
				return false;
			}
			else
			{
				round = true;
				index = 0;
			}
		}

		CSchedule* schedule = m_listSchedule[index];

		if(schedule->IsBetween())
		{
			return true;
		}

		index++;
	}

	return false;
}

bool CFrame::GetNextDefaultSchedule(int& index)
{

	if(m_listDefaultSchedule.GetCount() == 0)
	{
		return false;
	}//if

	if(m_pclsPalyRule != NULL && m_pclsPalyRule->m_bUsePlayCount == false)	//schedule의 play order을 지정하여 재생하는 경우
	{
		//m_pclsPalyRule->m_aryPlayOrder에 있는 schedule만을 검사한다
		CString strPlayOrder;
		int nNewIndexPlayOrder = m_nIndexPlayOrder+1;
		bool bRound = false;

		while(1)
		{
			if(nNewIndexPlayOrder >= m_pclsPalyRule->m_aryPlayOrder.GetSize())
			{
				if(bRound == true)
				{
					break;
				}
				else
				{
					bRound = true;
					nNewIndexPlayOrder = 0;
				}//if
			}//if

			strPlayOrder= m_pclsPalyRule->m_aryPlayOrder.GetAt(nNewIndexPlayOrder);
			index = FindDefaultScheduleIndexByPlayOrder(strPlayOrder);
			if(index == -1)
			{
				nNewIndexPlayOrder++;
				continue;
			}//if

			CSchedule* pclsSchedule = m_listDefaultSchedule[index];
			if(strlen(pclsSchedule->GetParentScheduleId()) != 0)
			{
				nNewIndexPlayOrder++;
				continue;
			}//if
				
			if(pclsSchedule->IsBetween() /*&& schedule->GetTimeScope() != 0 */) // 임시 막아둠 -> 종일 방송도 같이 방송
			{
				m_nIndexPlayOrder = nNewIndexPlayOrder;
				return true;
			}//if
			
			nNewIndexPlayOrder++;
		}//while

		return false;
	}//if

	index = m_nCurrentDefaultScheduleIndex+1;
	/*if(index >= m_listDefaultSchedule.GetCount())
		index = 0;*/

	bool find = false;
	bool round = false;

	while(1) // 지정된 시간대만 검색
	{
		if(index >= m_listDefaultSchedule.GetCount())
		{
			if(round == true)
			{
				break;
			}
			else
			{
				round = true;
				index = 0;
			}//if
		}//if

		CSchedule* schedule = m_listDefaultSchedule[index];
		if(strlen(schedule->GetParentScheduleId()) != 0)
		{
			index++;
			continue;
		}//if
		
		if(schedule->IsBetween() /*&& schedule->GetTimeScope() != 0 */) // 임시 막아둠 -> 종일 방송도 같이 방송
		{
			return true;
		}//if
		
		index++;
	}//while
/*
	index = m_nCurrentDefaultScheduleIndex+1;
	if(index >= m_listDefaultSchedule.GetCount())
		index = 0;

	find = false;
	round = false;

	while(1) // 24시간 검색
	{
		if(index >= m_listDefaultSchedule.GetCount())
		{
			if(round == true)
			{
				return false;
			}
			else
			{
				round = true;
				index = 0;
			}//if
		}//if

		CSchedule* schedule = m_listDefaultSchedule[index];
		if(strlen(schedule->GetParentScheduleId()) != 0)
		{
			index++;
			continue;
		}//if
		
		if(schedule->IsBetween() && schedule->GetTimeScope() == 0 )
		{
			return true;
		}//if
		
		index++;
	}//while
*/
	return false;
}


bool CFrame::GetNextChildSchedule(int& index)
{
	if(atoi(GetINIValue("UBCVariables.ini", "ROOT", "USE_CLICK_SCHEDULE")))
	{
		//__DEBUG__("USE_CLICK_SCHEDULE is set to true", _NULL);
		return false;
	}//if

	if(m_listDefaultSchedule.GetCount() == 0)
	{
		return false;
	}//if

	if(m_pclsPalyRule != NULL && m_pclsPalyRule->m_bUsePlayCount == false)	//schedule의 play order을 지정하여 재생하는 경우
	{
		//m_pclsPalyRule->m_aryPlayOrder에 있는 schedule만을 검사한다
		CString strPlayOrder;
		int nNewIndexPlayOrder = m_nIndexPlayOrder+1;
		bool bRound = false;

		while(1)
		{
			if(nNewIndexPlayOrder >= m_pclsPalyRule->m_aryPlayOrder.GetSize())
			{
				if(bRound == true)
				{
					break;
				}
				else
				{
					bRound = true;
					nNewIndexPlayOrder = 0;
				}//if
			}//if

			strPlayOrder= m_pclsPalyRule->m_aryPlayOrder.GetAt(nNewIndexPlayOrder);
			index = FindDefaultScheduleIndexByPlayOrder(strPlayOrder);
			if(index == -1)
			{
				nNewIndexPlayOrder++;
				continue;
			}//if

			CSchedule* pclsSchedule = m_listDefaultSchedule[index];
			if((strlen(pclsSchedule->GetParentScheduleId()) != 0) && pclsSchedule->IsBetween())
			{
				m_nIndexPlayOrder = nNewIndexPlayOrder;
				return true;
			}//if

			nNewIndexPlayOrder++;
		}//while

		return false;
	}//if

	index = m_nCurrentDefaultScheduleIndex+1;
	/*if(index >= m_listDefaultSchedule.GetCount())
		index = 0;*/

	bool find = false;
	bool round = false;

	while(1) // 지정된 시간대만 검색
	{
		if(index >= m_listDefaultSchedule.GetCount())
		{
			if(round == true)
			{
				break;
			}
			else
			{
				round = true;
				index = 0;
			}//if
		}//if

		CSchedule* pclsSchedule = m_listDefaultSchedule[index];

		if((strlen(pclsSchedule->GetParentScheduleId()) != 0) && pclsSchedule->IsBetween())
		{
			return true;
		}//if

		index++;
	}//while

	return false;
}


bool CFrame::SortDefaultSchedule()
{
	__DEBUG_FRAME_BEGIN__(m_frameId.c_str())

	int count = m_listDefaultSchedule.GetCount();
	for(int i=0; i<count-1; i++)
	{
		for(int j=i+1; j<count; j++)
		{
			CSchedule* schedule1 = m_listDefaultSchedule[i];
			CSchedule* schedule2 = m_listDefaultSchedule[j];

			if(schedule1->GetPlayOrder() > schedule2->GetPlayOrder())
			{
				m_listDefaultSchedule.SetAt(i, schedule2);
				m_listDefaultSchedule.SetAt(j, schedule1);

				if(m_pCurrentSchedule == schedule1)
					m_nCurrentDefaultScheduleIndex = j;
				if(m_pCurrentSchedule == schedule2)
					m_nCurrentDefaultScheduleIndex = i;
			}
		}
	}

	__DEBUG_FRAME_END__(m_frameId.c_str())

	return true;
}

/*
void CFrame::SetClipRgn(int x, int y, int width, int height)
{
	__DEBUG_FRAME_BEGIN__(m_frameId.c_str())

	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);

	CPoint pts[4];
	pts[0].SetPoint((x - m_x) * xscale,			(y - m_y) * yscale);
	pts[1].SetPoint((x - m_x + width) * xscale,	(y - m_y) * yscale);
	pts[2].SetPoint((x - m_x + width) * xscale,	(y - m_y + height) * yscale);
	pts[3].SetPoint((x - m_x) * xscale,			(y - m_y + height) * yscale);

	CRgn rgn_in;
	rgn_in.CreatePolygonRgn(pts, 4, ALTERNATE);

	pts[0].SetPoint(0,					0);
	pts[1].SetPoint(m_width * xscale,	0);
	pts[2].SetPoint(m_width * xscale,	m_height * yscale);
	pts[3].SetPoint(0,					m_height * yscale);

	CRgn rgn_out;
	rgn_out.CreatePolygonRgn(pts, 4, ALTERNATE);

	CRgn rgn_xor;
	rgn_xor.CreateRectRgn( 0, 0, 50, 50 );

	rgn_xor.CombineRgn(&rgn_in, &rgn_out, RGN_XOR);

	SetWindowRgn((HRGN)rgn_xor.GetSafeHandle(), TRUE);

	__DEBUG_FRAME_END__(m_frameId.c_str())
}
*/


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 원래 Frame의 영역에서 PIP윈도우 영역을 잘라낸다. \n
/////////////////////////////////////////////////////////////////////////////////
void CFrame::SetPIPClipRgn()
{
	__DEBUG_FRAME_BEGIN__(m_frameId.c_str())

	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);

	CPoint pts[4];
	int nX, nY, nWidth, nHeight;
	CRgn rgnInner, rgnOuter, rgnFrame;

	//rgnFrame.CreateRectRgn(0, 0, 50, 50);		//최종 Clip된 영역을 만들기 위한 변수

	//원래 Frame의 영역
	pts[0].SetPoint(0,					0);
	pts[1].SetPoint(m_width * xscale,	0);
	pts[2].SetPoint(m_width * xscale,	m_height * yscale);
	pts[3].SetPoint(0,					m_height * yscale);

	rgnOuter.CreatePolygonRgn(pts, 4, ALTERNATE);

	//m_listPIPFrame의 수 많큼 루프를 돌면서 영역을 clip한다.
	CFrame* pPIPFrame;
	for(int i=0; i<m_listPIPFrame.GetCount(); i++)
	{
		pPIPFrame = (CFrame*)m_listPIPFrame.GetAt(i);
		pPIPFrame->GetFrameRgn(nX, nY, nWidth, nHeight);

		pts[0].SetPoint((nX - m_x) * xscale,			(nY - m_y) * yscale);
		pts[1].SetPoint((nX - m_x + nWidth) * xscale,	(nY - m_y) * yscale);
		pts[2].SetPoint((nX - m_x + nWidth) * xscale,	(nY - m_y + nHeight) * yscale);
		pts[3].SetPoint((nX - m_x) * xscale,			(nY - m_y + nHeight) * yscale);

		rgnInner.CreatePolygonRgn(pts, 4, ALTERNATE);

		rgnOuter.CombineRgn(&rgnInner, &rgnOuter, RGN_XOR);
		rgnInner.DeleteObject();
	}//for

	//rgn_xor.CombineRgn(&rgn_in, &rgn_out, RGN_XOR);

	SetWindowRgn((HRGN)rgnOuter.GetSafeHandle(), TRUE);

	__DEBUG_FRAME_END__(m_frameId.c_str())
}

void CFrame::ResetClipRgn()
{
	__DEBUG_FRAME_BEGIN__(m_frameId.c_str())

	SetWindowRgn(NULL, TRUE);

	__DEBUG_FRAME_END__(m_frameId.c_str())
}

bool CFrame::FrameInFrame(CFrame* frame)
{
	return	(m_x <= frame->m_x && frame->m_x <= m_x+m_width) &&
			(m_y <= frame->m_y && frame->m_y <= m_y+m_height);
}

CString	CFrame::ToString()
{
	CString str;
	str.Format(
		"templateId = %s\r\n"
		"frameId = %s\r\n"
		"grade = %d\r\n"
		"width = %d\r\n"
		"height = %d\r\n"
		"x = %d\r\n"
		"y = %d\r\n"
		"isPIP = %d\r\n"
		"borderStyle = %d\r\n"
		"borderThickness = %d\r\n"
		"borderColor = %s\r\n"
		"cornerRadius = %d\r\n"
		"\r\n"
		"m_bCurrentPlay = %d\r\n"
		"m_pCurrentSchedule = %s\r\n"
		"m_pPIPParentWnd = %s\r\n"
		"m_bIsHDFrame = %s\r\n"
		,	m_templateId.c_str()
		,	m_frameId.c_str()
		,	m_grade
		,	m_width
		,	m_height
		,	m_x
		,	m_y
		,	m_isPIP
		,	m_borderStyle.c_str()
		,	m_borderThickness
		,	m_borderColor.c_str()
		,	m_cornerRadius

		,	m_bCurrentPlay
		,	((m_pCurrentSchedule == NULL) ? "NULL" : m_pCurrentSchedule->GetScheduleId())
		,	((m_pPIPParentWnd == NULL) ? "NULL" : m_pPIPParentWnd->GetFrameId())
		,	m_bIsHDFrame

	);

	return str;
}

LRESULT CFrame::OnDebugString(WPARAM wParam, LPARAM lParam)
{
	m_debugString = ToString();

	return (LRESULT)(LPCTSTR)m_debugString;
}

LRESULT CFrame::OnDebugGrid(WPARAM wParam, LPARAM lParam)
{
	CPropertyGrid* grid = (CPropertyGrid*)wParam;

	grid->ResetContents();

	HSECTION hs = grid->AddSection("FRAME Attributes");

	grid->AddStringItem(hs, "templateId", m_templateId.c_str());
	grid->AddStringItem(hs, "frameId", m_frameId.c_str());
	grid->AddIntegerItem(hs, "grade", m_grade);
	grid->AddIntegerItem(hs, "width", m_width);
	grid->AddIntegerItem(hs, "height", m_height);
	grid->AddIntegerItem(hs, "x", m_x);
	grid->AddIntegerItem(hs, "y", m_y);
	grid->AddBoolItem(hs, "isPIP", m_isPIP);
	grid->AddStringItem(hs, "borderStyle", m_borderStyle.c_str());
	grid->AddIntegerItem(hs, "borderThickness", m_borderThickness);
	grid->AddStringItem(hs, "borderColor", m_borderColor.c_str());
	grid->AddIntegerItem(hs, "cornerRadius", m_cornerRadius);

	hs = grid->AddSection("member variables");

	grid->AddStringItem(hs, "ERROR MESSAGE", (LPCSTR)m_strErrorMessage);
	grid->AddBoolItem(hs, "m_bCurrentPlay", m_bCurrentPlay);
	grid->AddStringItem(hs, "m_pCurrentSchedule", ((m_pCurrentSchedule == NULL) ? "NULL" : m_pCurrentSchedule->GetScheduleId()));
	grid->AddIntegerItem(hs, "m_nCurrentScheduleIndex", m_nCurrentScheduleIndex );
	grid->AddIntegerItem(hs, "m_nCurrentDefaultScheduleIndex", m_nCurrentDefaultScheduleIndex );
	grid->AddStringItem(hs, "m_pPIPParentWnd", ((m_pPIPParentWnd == NULL) ? "NULL" : m_pPIPParentWnd->GetFrameId()));
	grid->AddIntegerItem(hs, "m_nPlayCount", m_nPlayCount);
	grid->AddBoolItem(hs, "m_bIsHDFrame", m_bIsHDFrame);

	return 0;
}

LRESULT CFrame::OnErrorMessage(WPARAM wParam, LPARAM lParam)
{
	if(m_strErrorMessage.GetLength() > 0) m_strErrorMessage.Append(";");
	if(m_strErrorMessage.GetLength() > DEBUG_MSG_LENGTH) m_strErrorMessage.Empty();
	m_strErrorMessage.Append((LPCSTR)wParam);

	CHost* pclsHost = (CHost*)theApp.m_pMainWnd;
	pclsHost->SendMessage(WM_ERROR_MESSAGE, wParam, lParam);

	return 0;
}

LRESULT CFrame::OnPlayNextSchedule(WPARAM wParam, LPARAM lParam)
{
	if(m_bCurrentPlay)
	{
		PlayNextSchedule();
	}

	return 0;
}

bool CFrame::LoadSchedule(bool bDefaultSchedule)
{
	CString schedule_list;
	if(bDefaultSchedule)
	{
		schedule_list = MNG_PROFILE_READ(m_strLoadID, "DefaultScheduleList");
	}
	else
	{
		schedule_list = MNG_PROFILE_READ(m_strLoadID, "ScheduleList");
	}//if

	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);

	CRect rect;
	GetClientRect(rect);
	rect.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale);

	int pos = 0;
	CString token = schedule_list.Tokenize(",", pos);
	while(token != "")
	{
		CString strErrorMessage;
		CSchedule* schedule = CSchedule::GetScheduleObject(m_pParentWnd, this, token, rect, bDefaultSchedule, strErrorMessage);
		if(schedule)
		{
			if(GetVidoeOpenType() != E_OPEN_INIT)
			{
				schedule->SetHDSchedule(m_bIsHDFrame);	//HD 여부 설정
			}//if

			if(bDefaultSchedule)
			{
				m_listDefaultSchedule.Add(schedule);
			}
			else
			{
				m_listSchedule.Add(schedule);
			}//if

			m_mapSchedule.SetAt(schedule->GetScheduleId(), schedule);
		}
		else
		{
			OnErrorMessage((WPARAM)(LPCSTR)strErrorMessage, 0);
			strErrorMessage.Insert(0, "\t\t");
			__ERROR__(strErrorMessage, token);
		}//if

		token = schedule_list.Tokenize(",", pos);
	}//while

	if(bDefaultSchedule)
	{
		SortDefaultSchedule();
	}//if

	return true;
}


bool CFrame::GetFrameScheduleInfo(FRAME_SCHEDULE_INFO& info)
{
	info.templateId			= m_templateId;
	info.frameId			= m_frameId;
	info.grade				= m_grade;
	info.width				= m_width;
	info.height				= m_height;
	info.x					= m_x;
	info.y					= m_y;
	info.isPIP				= m_isPIP;
	info.borderStyle		= m_borderStyle;
	info.borderThickness	= m_borderThickness;
	info.borderColor		= m_borderColor;
	info.cornerRadius		= m_cornerRadius;
	info.rgbBorderColor		= m_rgbBorderColor;

	info.pip_frameId		= "";
	if(m_pPIPParentWnd)	info.pip_frameId = m_pPIPParentWnd->GetFrameId();
	info.pip_width			= 0;
	info.pip_height			= 0;
	info.pip_x				= 0;
	info.pip_y				= 0;

	if(m_pCurrentSchedule)
	{
		m_pCurrentSchedule->GetFrameScheduleInfo(info);
	}

	return true;
}

void CFrame::ResetPlayCount()
{
	int count = m_listDefaultSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listDefaultSchedule[i];

		schedule->ResetPlayCount();
	}
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Schedule의 play order로 m_listDefaultSchedule의 해당되는 인덱스를 찾아주는 함수 \n
/// @param (CString) strPlayOrder : (in) schedule의 play order 문자열
/// @return <형: int> \n
///			<-1: 해당하는 play order의 인덱스를 찾을 수 없음> \n
///			<0 또는 0보다 큰값: 지정된 play order를 갖는 schedule의 m_listDefaultSchedule에서의 인덱스> \n
/////////////////////////////////////////////////////////////////////////////////
int CFrame::FindDefaultScheduleIndexByPlayOrder(CString strPlayOrder)
{
	int nPlayOrder = atoi(strPlayOrder);
	CSchedule* pclsSchedule = NULL;
	for(int i=0; i<m_listDefaultSchedule.GetSize(); i++)
	{
		pclsSchedule = m_listDefaultSchedule.GetAt(i);
		if(pclsSchedule->GetPlayOrder() == nPlayOrder)
		{
			return i;
		}//if
	}//for

	return -1;
}


LRESULT CFrame::OnHideContents(WPARAM wParam, LPARAM lParam)
{
	KillTimer(FRAME_SHOW_ID);
	//SetTimer(FRAME_HIDE_ID, FRAME_HIDE_TIME, NULL);
	ShowWindow(SW_HIDE);

	int count = m_listSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listSchedule[i];
		if(schedule->GetSafeHwnd())
		{
			schedule->PostMessage(WM_HIDE_CONTENTS, 0, 0);
		}//if
	}//if

	count = m_listDefaultSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listDefaultSchedule[i];
		if(schedule->GetSafeHwnd())
		{
			schedule->PostMessage(WM_HIDE_CONTENTS, 0, 0);
		}//if
	}//if

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Grade가 1인 컨텐츠의 사운드를 mute(toggle)하는 메시지 처리 \n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CFrame::OnSoundVolumeMute(WPARAM wParam, LPARAM lParam)
{
	int count = m_listSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listSchedule[i];
		schedule->SoundVolumeMute((bool)wParam);
	}//if

	count = m_listDefaultSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listDefaultSchedule[i];
		schedule->SoundVolumeMute((bool)wParam);
	}//if

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Scheduel pause \n
/////////////////////////////////////////////////////////////////////////////////
void CFrame::Pause()
{
	m_bPause = !m_bPause;

	// Pause Schedule
	if(m_pCurrentSchedule)
	{
		if(m_bPause)
		{
			KillTimer(SCHEDULE_CHECK_ID);
			m_pCurrentSchedule->Pause();
		}
		else
		{
			SetTimer(SCHEDULE_CHECK_ID, SCHEDULE_CHECK_TIME, NULL);
			m_pCurrentSchedule->Play();
		}//if
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 모든 스케줄의 리스트를 구한다. \n
/// @param (CScheduleArray&) listSchedule : (in/out) 스케줄의 리스트
/////////////////////////////////////////////////////////////////////////////////
void CFrame::GetScheduleList(CScheduleArray& listSchedule)
{
	if(m_listSchedule.GetCount() != 0)
	{
		listSchedule.Append(m_listSchedule);
	}//if

	if(m_listDefaultSchedule.GetCount() != 0)
	{
		listSchedule.Append(m_listDefaultSchedule);
	}//if
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// PIP프레임의 포인터를 멤버배열에 추가한다. \n
/// @param (CFrame*) pFrame : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CFrame::AddPIPFrame(CFrame* pFrame)
{
	m_listPIPFrame.Add(pFrame);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// PIP프레임의 멤버배열을 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CFrame::ClearPIPFrame()
{
	m_listPIPFrame.RemoveAll();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Frame 윈도우의 영역을 얻어온다. \n
/// @param (int&) nX : (out) 윈도우 X 좌표 값
/// @param (int&) nY : (out) 윈도우 Y 좌표 값
/// @param (int&) nWidth : (out) 윈도우 넓이
/// @param (int&) nHeight : (out) 윈도우 높이
/////////////////////////////////////////////////////////////////////////////////
void CFrame::GetFrameRgn(int& nX, int& nY, int& nWidth, int& nHeight)
{
	nX		= m_x;
	nY		= m_y;
	nWidth	= m_width;
	nHeight = m_height;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 Id를 부모스케줄로 갖는 스케줄의 인덱스를 찾는다. \n
/// @param (CString) strScheduleId : (in) 부모스케줄의 Id
/// @param (int&) nIndex : (in) 찾은 스케줄의 인덱스
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CFrame::FindChildSchedule(CString strScheduleId, int& nIndex)
{
	if(!atoi(GetINIValue("UBCVariables.ini", "ROOT", "USE_CLICK_SCHEDULE")))
	{
		__DEBUG__("USE_CLICK_SCHEDULE is set to false", _NULL);
		return false;
	}//if

	if(m_listDefaultSchedule.GetCount() == 0)
	{
		return false;
	}//if

	if(m_pclsPalyRule != NULL && m_pclsPalyRule->m_bUsePlayCount == false)	//schedule의 play order을 지정하여 재생하는 경우
	{
		//m_pclsPalyRule->m_aryPlayOrder에 있는 schedule만을 검사한다
		CString strPlayOrder;
		int nNewIndexPlayOrder = m_nIndexPlayOrder+1;
		bool bRound = false;

		while(1)
		{
			if(nNewIndexPlayOrder >= m_pclsPalyRule->m_aryPlayOrder.GetSize())
			{
				if(bRound == true)
				{
					break;
				}
				else
				{
					bRound = true;
					nNewIndexPlayOrder = 0;
				}//if
			}//if

			strPlayOrder= m_pclsPalyRule->m_aryPlayOrder.GetAt(nNewIndexPlayOrder);
			nIndex = FindDefaultScheduleIndexByPlayOrder(strPlayOrder);
			if(nIndex == -1)
			{
				nNewIndexPlayOrder++;
				continue;
			}//if

			CSchedule* pclsSchedule = m_listDefaultSchedule[nIndex];
			if((strcmp(pclsSchedule->GetParentScheduleId(), strScheduleId) == 0) && pclsSchedule->IsBetween())
			{
				m_nIndexPlayOrder = nNewIndexPlayOrder;
				return true;
			}//if

			nNewIndexPlayOrder++;
		}//while

		return false;
	}//if

	nIndex = m_nCurrentDefaultScheduleIndex+1;
	if(nIndex >= m_listDefaultSchedule.GetCount())
	{
		nIndex = 0;
	}//if

	bool bRound = false;

	while(1)
	{
		if(nIndex >= m_listDefaultSchedule.GetCount())
		{
			if(bRound == true)
			{
				break;
			}
			else
			{
				bRound = true;
				nIndex = 0;
			}//if
		}//if

		CSchedule* pclsSchedule = m_listDefaultSchedule[nIndex];

		if((strcmp(pclsSchedule->GetParentScheduleId(), strScheduleId) == 0) && pclsSchedule->IsBetween())
		{
			return true;
		}//if

		nIndex++;
	}//while

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 부모 스케줄의 Click 이벤트를 받아 설정된 자식 스케줄을 재생한다. \n
/// @param (CString) strScheduleId : (in) 부모스케줄의 Id
/////////////////////////////////////////////////////////////////////////////////
void CFrame::PlayClickSchedule(CString strScheduleId)
{
	if(strScheduleId.GetLength() == 0)
	{
		return;
	}//if

	int nNewIndex;
	CSchedule* OldSchedule = m_pCurrentSchedule;

	if(FindChildSchedule(strScheduleId, nNewIndex))
	{
		//클릭 이벤트로 재생되는 스케줄은 한번만 running time 동안 재생되고 끝난다.
		CSchedule* pNewChild = m_listDefaultSchedule[nNewIndex];

		if(!pNewChild->IsPlay())
		{
			__DEBUG__("Click schedule will be play", pNewChild->GetScheduleId());
			m_nCurrentDefaultScheduleIndex = nNewIndex;
			m_pCurrentSchedule = pNewChild;
			if(OldSchedule)
			{
				OldSchedule->Stop();
				//이전 스케줄의 Play count를 줄여서
				//클릭 스케줄이 끝나면 이전 스케줄이 나오도록 한다.
				OldSchedule->ResetPlayCount();
			}//if

			m_pCurrentSchedule->Play();

			return;
		}//if
	}//if

	__DEBUG__("Not set click schedule to play", _NULL);
}