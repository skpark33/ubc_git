// PhoneSelectDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "PhoneSelectDialog.h"

#include "Frame.h"


// CPhoneSelectDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPhoneSelectDialog, CDialog)

CPhoneSelectDialog::CPhoneSelectDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPhoneSelectDialog::IDD, pParent)
	, m_pParentWnd(pParent)
{

}

CPhoneSelectDialog::~CPhoneSelectDialog()
{
}

void CPhoneSelectDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PHONE_LIST, m_listctrlPhone);
}


BEGIN_MESSAGE_MAP(CPhoneSelectDialog, CDialog)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CPhoneSelectDialog 메시지 처리기입니다.

BOOL CPhoneSelectDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//
	SetWindowPos(&wndTopMost , 0  ,0, 480 ,200,SWP_NOMOVE);
	MoveWindow(0,0,480,200, TRUE);
	CenterWindow();

	//
	CRect rect;
	GetClientRect(rect);

	//
	m_listctrlPhone.SetExtendedStyle(m_listctrlPhone.GetExtendedStyle() | LVS_EX_FULLROWSELECT );

	CString col;
	col.LoadString(IDS_SITE_ID);	m_listctrlPhone.InsertColumn(0, col, LVCFMT_LEFT, 120);
	col.LoadString(IDS_HOST_ID);	m_listctrlPhone.InsertColumn(1, col, LVCFMT_LEFT, 120);
	col.LoadString(IDS_HOST_NAME);	m_listctrlPhone.InsertColumn(2, col, LVCFMT_LEFT, 120);
	col.LoadString(IDS_IP_ADDRESS);	m_listctrlPhone.InsertColumn(3, col, LVCFMT_LEFT, 120);

	m_listctrlPhone.MoveWindow(rect);

	m_listctrlPhone.SetFocus();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPhoneSelectDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();

	int index = m_listctrlPhone.GetSelectionMark();
	if(index >= 0)
	{
		ShowWindow(SW_HIDE);

		m_strSiteId		= m_listctrlPhone.GetItemText(index, 0);
		m_strHostId		= m_listctrlPhone.GetItemText(index, 1);
		m_strHostName	= m_listctrlPhone.GetItemText(index, 2);
		m_strIPAddress	= m_listctrlPhone.GetItemText(index, 3);

		PhoneRequest(m_strSiteId, m_strHostId);
	}
}

void CPhoneSelectDialog::ResetInfo(LPCSTR lpszSiteId, LPCSTR lpszHostId)
{
	m_listctrlPhone.DeleteAllItems();

	//
	CString strEntity;
	strEntity.Format("SM=*/Site=%s/Host=%s", lpszSiteId, lpszHostId);

	cciCall call("get");

	cciEntity aEntity(strEntity);
	call.setEntity(aEntity);

	call.addItem("attributes", "*");

	int count = 0;

	try
	{
		if(!call.call())
		{
			::AfxMessageBox(IDS_FAIL_TO_INITIALIZE_HOST_INFO, MB_ICONSTOP);		// Fail to initialize Host-Info !!!
			return;
		}

		cciReply reply;
		while(call.getReply(reply))
		{
			CString str = reply.toString();
			if(str.GetLength() < 10) continue;

			ciString	SiteId;
			ciString	HostId;
			ciString	HostName;
			ciString	IpAddress;

			reply.unwrap();

			reply.getItem("siteId", SiteId);
			reply.getItem("hostId", HostId);
			reply.getItem("hostName", HostName);
			reply.getItem("ipAddress", IpAddress);

			if( strcmp(SiteId.c_str(), ::GetSiteName()) == 0 &&
				strcmp(HostId.c_str(), ::GetHostName()) == 0)
				continue;

			m_listctrlPhone.InsertItem(count, "");
			m_listctrlPhone.SetItemText(count, 0, SiteId.c_str());
			m_listctrlPhone.SetItemText(count, 1, HostId.c_str());
			m_listctrlPhone.SetItemText(count, 2, HostName.c_str());
			m_listctrlPhone.SetItemText(count, 3, IpAddress.c_str());

			count++;
		}
	}
	catch(CORBA::Exception& ex)
	{
	}

	if(count > 0)
		m_listctrlPhone.SetItemState(0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
}

bool CPhoneSelectDialog::GetInfo(CString& strSiteId, CString& strHostId, CString& strHostName, CString& strIPAddress)
{
	strSiteId		= m_strSiteId;
	strHostId		= m_strHostId;
	strHostName		= m_strHostName;
	strIPAddress	= m_strIPAddress;

	return true;
}

bool CPhoneSelectDialog::PhoneRequest(LPCSTR lpszSiteId, LPCSTR lpszHostId)
{
	CFrame::m_csFramePhone.Lock();

	if(CFrame::m_pPhoneFrame == NULL)
	{
		CFrame::m_csFramePhone.Unlock();

		::AfxMessageBox(IDS_NOT_EXIST_VIDEOPHONE_TEMPLATE_INFO, MB_ICONSTOP);		// Not exist VideoPhone Template-Info !!!
		return false;
	}

	CString strPhoneTemplateid, strPhoneFrameId;
	int nPortNo;
	GetPhoneConfig(strPhoneTemplateid, strPhoneFrameId, nPortNo);

	cciCall call("phoneRequest");

	cciEntity entity("RM=*");
	call.setEntity(entity);

	call.addItem("templateId", strPhoneTemplateid);
	call.addItem("frameId", CFrame::m_pPhoneFrame->GetFrameId() );
	call.addItem("ASiteId", ::GetSiteName());	// 송신 
	call.addItem("AHostId", ::GetHostName());	// 송신
	call.addItem("ZSiteId", lpszSiteId);		// 수신
	call.addItem("ZHostId", lpszHostId);		// 수신
	call.addItem("portNo", (ciLong)nPortNo);

	CFrame::m_csFramePhone.Unlock();

	try
	{
		if(!call.call())
		{
			::AfxMessageBox(IDS_FAIL_TO_SEND_VIDEOPHONE_REQUEST_MESSAGE, MB_ICONSTOP);		// Fail to send a VideoPhone Request Message !!!
			return false;
		}
	}
	catch(CORBA::Exception& ex)
	{
		return false;
	}

	return true;
}

void CPhoneSelectDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}
