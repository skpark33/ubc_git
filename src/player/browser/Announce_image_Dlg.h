#pragma once

#include "ximage.h"
#include "PictureEx.h"

#include "AnnounceDlg.h"

// CAnnounce_image_Dlg 대화 상자입니다.

class CAnnounce_image_Dlg : public CAnnounceDlg
{
	DECLARE_DYNAMIC(CAnnounce_image_Dlg)

protected:
	DECLARE_MESSAGE_MAP()

public:
	CAnnounce_image_Dlg(int nPosX, int nPosY, int nCx, int nCy, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAnnounce_image_Dlg();

	void		BeginAnnounce(CAnnounce* pAnn);					///<공지 방송을 시작하도록 한다.
	void		EndAnnounce();									///<공지 방송을 중지하도록 한다.

	static UINT PlayImageThread(LPVOID param);
	afx_msg void OnPaint();
// 그림 함수들
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

protected:
	
	virtual bool	Play();
	virtual bool	Stop();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	bool			OpenGIF(void);							///<GIF 이미지파일을 open한다.
	bool			OpenImage(void);						///<이미지파일을 open한다.
	void			SetNoContentsFile();

	CxImage			m_bgImage;
	CPictureEx		m_bgGIF;
	bool			m_bAnimatedGif;
	bool			m_bOpen;
	bool			m_bFileNotExist;

	CString			m_strMediaFullPath;


public:
};
