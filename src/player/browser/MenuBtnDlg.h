#pragma once
#include "afxwin.h"
#include "MenuBarDlg.h"


// CMenuBtnDlg 대화 상자입니다.

class CMenuBtnDlg : public CDialog
{
	DECLARE_DYNAMIC(CMenuBtnDlg)

public:
	CMenuBtnDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMenuBtnDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MENUBTN_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedMenubtnBtn();

	CHoverButton	m_btnMenuBtn;
	CMenuBarDlg*	m_pdlgMenuBar;

	void	SetMenuBar(CMenuBarDlg* pdlgMenuBar);		///<메뉴바의 포인터를 설정
	void	SetShowImage(bool bShow);					///<Show/Hide 이미지를 설정
	void	SetTransParency(int nVal);					///<화면의 투명도를 설정한다.


	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
