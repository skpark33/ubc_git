// TVPlayer.cpp
//

#include "stdafx.h"
#include <afxinet.h>

#include "TVPlayer.h"

#include "NoTVInterface.h"
#include <common/libCommon/ubcIni.h>
#include <common/libScratch/scratchUtil.h>

#include <common/libHttpRequest/HttpRequest.h>

#define	_TV_DVICO_


#ifdef	_TV_DVICO_
#include "DvicoTVInterface.h"
#endif

#ifdef	_TV_SKY_
#include "SkyTVInterface.h"
#endif


CTVPlayer::CTVPlayer()
:	m_eModule ( deviceNONE )
,	m_TVInterface ( NULL )
{
}

CTVPlayer::~CTVPlayer()
{
	Destroy();
}

BOOL CTVPlayer::Create(CWnd* pParentWnd, const RECT& rect, bool bScanningWnd, DEVICE_MODULE module)
{
	string site, host, mac;
	scratchUtil::getInstance()->readAuthFile(host, mac);

	// 1. get auth-ex-info (projectEx.dat)
	// 1.1 if (ok) return
	int auth_ex = scratchUtil::getInstance()->readAuthFileEx(host);
	if(auth_ex < 0 || (auth_ex & AUTH_EX_TV) == 0)
	{
		//
		int idx = host.find('-');
		if( idx > 0 )
		{
			site = host.substr(0,idx);
		}

		CString send_msg;
		send_msg.Format("siteId=%s&hostId=%s", site.c_str(), host.c_str());

		CHttpRequest http_request;
		http_request.Init(CHttpRequest::CONNECTION_CENTER);

		CString response;
		BOOL ret_val = http_request.RequestPost("/UBC_Registration/authorize_tv.asp", send_msg, response);

		if( ret_val )
		{
			// 2.1 if (none) not exist host
			if(response == _T("none"))
			{
				module = deviceNONE;
			}
			// 2.2 if (denied) return false
			else if(response == _T("denied"))
			{
				module = deviceNONE;
			}
			// 2.3 if (ok) write (projectEx.dat), return true
			else if(response == _T("ok"))
			{
				if(auth_ex<0) auth_ex=0;
				auth_ex |= AUTH_EX_TV;
				scratchUtil::getInstance()->writeAuthFileEx(host.c_str(), auth_ex);
			}
			else
			{
				module = deviceNONE;
			}
		}
		else
			module = deviceNONE;
	}
	else
	{
		// auth ok
	}

	m_eModule = module;

	BOOL ret_val = FALSE;

	switch(m_eModule)
	{
	default: // no support module
		m_TVInterface = new CNoTVInterface(bScanningWnd);
		ret_val = m_TVInterface->Create(pParentWnd, rect);
		break;

#ifdef	_TV_DVICO_
	case deviceDVICO:
		m_TVInterface = new CDvicoTVInterface(bScanningWnd);
		ret_val = m_TVInterface->Create(pParentWnd, rect);
		break;
#endif

#ifdef	_TV_SKY_
	case deviceSKY:
		m_TVInterface = new CSkyTVInterface();
		ret_val = m_TVInterface->Create(pParentWnd, rect);
		break;
#endif

	}

	if(ret_val == FALSE) Destroy();

	return ret_val;
}

BOOL CTVPlayer::Destroy(bool bPerfect)
{
	if(m_TVInterface != NULL)
	{
		m_eModule = deviceNONE;

		m_TVInterface->Destroy(bPerfect);
		delete m_TVInterface;
		m_TVInterface = NULL;
	}
	return TRUE;
}

TV_MODE	CTVPlayer::GetTVMode()
{
	if(m_TVInterface)
	{
		return m_TVInterface->GetTVMode();
	}
	return modeAll;
}

int CTVPlayer::GetChannel()
{
	if(m_TVInterface)
	{
		return m_TVInterface->GetChannel();
	}
	return -1;
}

int CTVPlayer::GetVolume()
{
	if(m_TVInterface)
	{
		return m_TVInterface->GetVolume();
	}
	return -1;
}

int CTVPlayer::GetMute()
{
	if(m_TVInterface)
	{
		return m_TVInterface->GetMute();
	}
	return -1;
}

BOOL CTVPlayer::SetTVMode(TV_MODE mode, BOOL bNTSC)
{
	if(m_TVInterface)
	{
		return m_TVInterface->SetTVMode(mode, bNTSC);
	}
	return FALSE;
}

BOOL CTVPlayer::SetChannel(int nChannel)
{
	if(m_TVInterface)
	{
		return m_TVInterface->SetChannel(nChannel);
	}
	return FALSE;
}

BOOL CTVPlayer::SetVolume(int nVol)
{
	if(m_TVInterface)
	{
		return m_TVInterface->SetVolume(nVol);
	}
	return FALSE;
}

BOOL CTVPlayer::SetMute(BOOL bMute)
{
	if(m_TVInterface)
	{
		return m_TVInterface->SetMute(bMute);
	}
	return FALSE;
}

BOOL CTVPlayer::MoveWindow(int x, int y, int cx, int cy)
{
	if(m_TVInterface)
	{
		return m_TVInterface->MoveWindow(x, y, cx, cy);
	}
	return FALSE;
}

BOOL CTVPlayer::Play()
{
	if(m_TVInterface)
	{
		return m_TVInterface->Play();
	}
	return FALSE;
}

BOOL CTVPlayer::Stop()
{
	if(m_TVInterface)
	{
		return m_TVInterface->Stop();
	}
	return FALSE;
}

BOOL CTVPlayer::StartScan()
{
	if(m_TVInterface)
	{
		return m_TVInterface->StartScan();
	}
	return FALSE;
}

BOOL CTVPlayer::StopScan()
{
	if(m_TVInterface)
	{
		return m_TVInterface->StopScan();
	}
	return FALSE;
}

BOOL CTVPlayer::ClearChannelList(bool bDataDelete, bool bFileDelete)
{
	if(m_TVInterface)
	{
		return m_TVInterface->ClearChannelList(bDataDelete, bFileDelete);
	}
	return FALSE;
}

BOOL CTVPlayer::LoadChannelList()
{
	if(m_TVInterface)
	{
		return m_TVInterface->LoadChannelList();
	}
	return FALSE;
}

BOOL CTVPlayer::SaveChannelList()
{
	if(m_TVInterface)
	{
		return m_TVInterface->SaveChannelList();
	}
	return FALSE;
}

BOOL CTVPlayer::AddChannel(TVCHANNEL_ITEM* pItem)
{
	if(m_TVInterface)
	{
		return m_TVInterface->AddChannel(pItem);
	}
	return FALSE;
}

BOOL CTVPlayer::DeleteChannel(TVCHANNEL_ITEM* pItem)
{
	if(m_TVInterface)
	{
		return m_TVInterface->DeleteChannel(pItem);
	}
	return FALSE;
}

BOOL CTVPlayer::GetTvChannelList(TVCHANNEL_ITEMLIST& listChannel, TV_MODE mode)
{
	if(m_TVInterface)
	{
		return m_TVInterface->GetTvChannelList(listChannel, mode);
	}
	return FALSE;
}
