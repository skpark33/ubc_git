// TVPlayer.cpp
//

#include "stdafx.h"
#include "NoTVInterface.h"


IMPLEMENT_DYNAMIC(CNoTVInterface, CWnd)


CNoTVInterface::CNoTVInterface(bool bScanningWnd)
{
}

CNoTVInterface::~CNoTVInterface()
{
}


BEGIN_MESSAGE_MAP(CNoTVInterface, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CNoTVInterface::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 __super::OnPaint()을(를) 호출하지 마십시오.

	CRect rect;
	GetClientRect(rect);

	dc.FillSolidRect(rect, RGB(64,64,64));

	CString msg = "No TV Interface Card !!!";
	dc.SetTextColor(RGB(255,255,255));
	CFont* old_font = dc.SelectObject(&m_font);
	dc.SetBkMode(TRANSPARENT);
	dc.DrawText(msg, rect, DT_MODIFYSTRING);
	dc.SelectObject(old_font);
}

BOOL CNoTVInterface::Create(CWnd* pParentWnd, const RECT& rect)
{
	static UINT uID = 0xff00;

	m_font.CreatePointFont(16*10, _T("Arial"));

	return CWnd::Create(NULL, _T("preview"), WS_CHILD | WS_VISIBLE, rect, pParentWnd, uID++);
}
