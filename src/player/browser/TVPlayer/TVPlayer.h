// TVPlayer.h
//


#pragma once

#include "TVInterface.h"


class CTVPlayer
{
public:
	CTVPlayer();
	virtual ~CTVPlayer();

	enum DEVICE_MODULE
	{
		deviceNONE = 0,
		deviceDVICO,
		deviceSKY,
	};

protected:
	DEVICE_MODULE	m_eModule;
	CTVInterface*	m_TVInterface;

public:
	BOOL	Create(CWnd* pParentWnd, const RECT& rect, bool bScanningWnd=false, DEVICE_MODULE module = deviceDVICO);
	BOOL	Destroy(bool bPerfect=false);

	TV_MODE	GetTVMode();
	int		GetChannel();
	int		GetVolume();
	int		GetMute();

	BOOL	SetTVMode(TV_MODE mode, BOOL bNTSC=TRUE); // ntsc/pal
	BOOL	SetChannel(int nChannel);
	BOOL	SetVolume(int nVol);		// min(0~100)max
	BOOL	SetMute(BOOL bMute=TRUE);
	BOOL	MoveWindow(int x, int y, int cx, int cy);

	BOOL	Play();
	BOOL	Stop();

	BOOL	StartScan();
	BOOL	StopScan();

	BOOL	ClearChannelList(bool bDataDelete=true, bool bFileDelete=true);
	BOOL	LoadChannelList();
	BOOL	SaveChannelList();
	BOOL	AddChannel(TVCHANNEL_ITEM* pItem);
	BOOL	DeleteChannel(TVCHANNEL_ITEM* pItem);
	BOOL	GetTvChannelList(TVCHANNEL_ITEMLIST& listChannel, TV_MODE mode=modeAll);
};

