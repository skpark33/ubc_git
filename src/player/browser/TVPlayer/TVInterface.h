// TVPlayer.h
//


#pragma once

#include <afxtempl.h>


#define		WM_ADD_CHANNEL_INFO			(WM_USER + 2000)
#define		WM_CHANGE_CHANNEL_INFO		(WM_USER + 2001)
#define		WM_CHANGE_SCAN_CHANNEL		(WM_USER + 2002)
#define		WM_COMPLETE_SCAN_CHANNEL	(WM_USER + 2003)


enum TV_MODE
{
	modeAll = 0,
	modeAnalogAir,
	modeAnalogCatv,
	modeDigitalAir,
	modeDigitalCatv,
	modeComposite,
	modeSvhs,
};

typedef struct {
	TV_MODE	eTVMode;
	int		nChannel;
	CString	strName;
} TVCHANNEL_ITEM;

typedef CArray<TVCHANNEL_ITEM, TVCHANNEL_ITEM&>		TVCHANNEL_ITEMLIST;


class CTVInterface
{
public:
	CTVInterface();
	virtual ~CTVInterface();

protected:
	static TVCHANNEL_ITEMLIST	m_ChannelList;

	CWnd*		m_pParentWnd;

	int			m_nStartupDelay;

	LPCTSTR		GetAppPath();
	BOOL		DeleteINIFile();
	int 		GetINIValueInt(LPCTSTR lpszFileName, LPCTSTR lpszSection, LPCTSTR lpszKey);
	CString		GetINIValueString(LPCTSTR lpszFileName, LPCTSTR lpszSection, LPCTSTR lpszKey);
	void		WriteINIValueInt(LPCTSTR lpszFileName, LPCTSTR lpszSection, LPCTSTR lpszKey, int nValue);
	void		WriteINIValueString(LPCTSTR lpszFileName, LPCTSTR lpszSection, LPCTSTR lpszKey, LPCTSTR lpszValue);

	int			m_nCurrentChannelIndex;
	int			GetPrevChannelIndex(TV_MODE eTVMode);
	int			GetNextChannelIndex(TV_MODE eTVMode);

public:
	virtual BOOL	Create(CWnd* pParentWnd, const RECT& rect) = 0;
	virtual void	Destroy(bool bPerfect) = 0;

	virtual TV_MODE	GetTVMode() = 0;
	virtual int		GetChannel() = 0;
	virtual int		GetVolume() = 0;
	virtual int		GetMute() = 0;

	virtual BOOL	SetTVMode(TV_MODE mode, BOOL bNTSC) = 0;
	virtual BOOL	SetChannel(int nChannel) = 0;
	virtual BOOL	SetVolume(int nVol) = 0;		// min(0~100)max
	virtual BOOL	SetMute(BOOL bMute) = 0;
	virtual	BOOL	MoveWindow(int x, int y, int cx, int cy) = 0;

	virtual BOOL	Play() = 0;
	virtual BOOL	Stop() = 0;

	virtual BOOL	StartScan() = 0;
	virtual BOOL	StopScan() = 0;

	BOOL		ClearChannelList(bool bDataDelete, bool bFileDelete);
	BOOL		LoadChannelList();
	BOOL		SaveChannelList();
	BOOL		AddChannel(TVCHANNEL_ITEM* pItem);
	BOOL		DeleteChannel(TVCHANNEL_ITEM* pItem);
	BOOL		GetTvChannelList(TVCHANNEL_ITEMLIST& listChannel, TV_MODE mode);
};

