// TVPlayer.cpp
//

#include "stdafx.h"
#include "DvicoTVInterface.h"


#define		VOLUME_MAX		15


#define		WM_STOP_TV					(WM_USER + 1000)
#define		WM_CONTROL_TV				(WM_USER + 1001)


#define		TVCONTROL_CHANNEL_UP		1
#define		TVCONTROL_CHANNEL_DOWN		2
#define		TVCONTROL_VOLUME_UP			3
#define		TVCONTROL_VOLUME_DOWN		4
#define		TVCONTROL_VOLUME_MUTE		5


#define		MESSAGE_VIEW_TIMER_ID		1022
#define		MESSAGE_VIEW_TIMER_TIME		(100)
#define		MESSAGE_STOP_TIMER_ID		1023
#define		MESSAGE_STOP_TIMER_TIME		(5*1000)


CDvicoTV103		CDvicoTVInterface::m_tv;
CMapPtrToPtr	CDvicoTVInterface::_mapInterfacePtr;
CMutex			CDvicoTVInterface::_mutexMap;

CReceiveMessageWnd CDvicoTVInterface::_wndReceiveMessage;


IMPLEMENT_DYNAMIC(CDvicoTVInterface, CWnd)


CDvicoTVInterface::CDvicoTVInterface(bool bScanningWnd)
:	m_nTvStandard ( CDvicoTV103::formatNTSC )
,	m_nSourceType ( CDvicoTV103::sourceTuner )
,	m_nSignalType ( CDvicoTV103::signalAir )
,	m_nCountryCode ( 82 )
,	m_nChannelNumber ( 11 )
,	m_nAudioVolume ( VOLUME_MAX/2 )
,	m_bPlay ( false )

,	m_eTvMode ( modeAnalogAir )
,	m_bNTSC ( TRUE )
,	m_bMute ( FALSE )
,	m_nTxResolution ( -1 )

,	m_bScanningWnd ( bScanningWnd )

{
	m_rect.SetRect(0, 0, 1, 1);
}

CDvicoTVInterface::~CDvicoTVInterface()
{
}


BEGIN_MESSAGE_MAP(CDvicoTVInterface, CWnd)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_MESSAGE(WM_STOP_TV, OnStopTV)
	ON_MESSAGE(WM_CONTROL_TV, OnControlTV)
END_MESSAGE_MAP()


BEGIN_EVENTSINK_MAP(CDvicoTVInterface, CWnd)
   ON_EVENT(CDvicoTVInterface, 0xfeff, 1, OnScanEvent, VTS_NONE)
END_EVENTSINK_MAP()


LRESULT CDvicoTVInterface::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	switch(message)
	{
	case WM_PVRDLL_NOTIFY:
		DWORD device_index = wParam >> 16;		// multicard사용시 index
		DWORD command = wParam & 0x0000ffff;
		switch(command)
		{
		case NOTIFY2_EVENTCODE_AVPID_CHANGED:
			{
				long video_pid = (long)(lParam) >> 16;
				long audio_pid = (long)(lParam) & 0xFFFF;
				if(m_tv.GetSafeHwnd())
				{
					m_tv.SetAvPid(video_pid, audio_pid);
				}
			}
			break;

		case NOTIFY2_EVENTCODE_VIDEO_DEC_CHANGED:
			if(m_tv.GetSafeHwnd() && m_nTxResolution != (long)lParam)
			{
				m_tv.put_VideoDecoderResolution((long)lParam);
				m_nTxResolution = (long)lParam;
			}
			break;

		case NOTIFY2_EVENTCODE_VIRTUAL_CHANNEL_CHANGED:
			{
				PsiVirtualChannelInfo* p_info = (PsiVirtualChannelInfo*)lParam;
				CopyMemory(&m_PsiVirtualChannelInfo, p_info, sizeof(PsiVirtualChannelInfo));

				int cur_channel = m_tv.get_Channel();
				for(int i=0; i<m_PsiVirtualChannelInfo.numbers; i++)
				{
					OneVirtualChannel* p_vch = &m_PsiVirtualChannelInfo.channels[i];
					for(int j=0; j<m_ChannelList.GetCount(); j++)
					{
						TVCHANNEL_ITEM& item = m_ChannelList.GetAt(j);
						if(item.nChannel == cur_channel)
						{
							TRACE(_T("Channel Changed : %s -> %s\r\n"), item.strName, p_vch->shortName);
							item.strName = p_vch->shortName;
							m_pParentWnd->SendMessage(WM_CHANGE_CHANNEL_INFO, (WPARAM)&item);
						}
					}
				}
			}
			break;
		}
		break;
	}

	return CWnd::WindowProc(message, wParam, lParam);
}

void CDvicoTVInterface::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 __super::OnPaint()을(를) 호출하지 마십시오.

}

void CDvicoTVInterface::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	__super::OnTimer(nIDEvent);

	if(nIDEvent == MESSAGE_STOP_TIMER_ID)
	{
		KillTimer(MESSAGE_VIEW_TIMER_ID);
		KillTimer(MESSAGE_STOP_TIMER_ID);

		m_wndCaption.ShowWindow(SW_HIDE);
	}
	else if(nIDEvent == MESSAGE_VIEW_TIMER_ID)
	{
		KillTimer(MESSAGE_VIEW_TIMER_ID);
		m_wndCaption.ShowWindow(SW_SHOW);
		m_wndCaption.Invalidate(FALSE);
	}
}

LRESULT	CDvicoTVInterface::OnStopTV(WPARAM wParam, LPARAM lParam)
{
	return Stop();
}

LRESULT	CDvicoTVInterface::OnControlTV(WPARAM wParam, LPARAM lParam)
{
	m_wndCaption.ShowWindow(SW_HIDE);

	switch(wParam)
	{
	case TVCONTROL_CHANNEL_UP:
		{
			int next_index = GetNextChannelIndex(m_eTvMode);
			if(next_index < 0)
			{
				SetChannel(m_nChannelNumber+1);
				m_wndCaption.UpdateText( GetDefaultChannelName(m_eTvMode, m_nChannelNumber) );
			}
			else
			{
				TVCHANNEL_ITEM& item = m_ChannelList.GetAt(next_index);
				SetChannel(item.nChannel);
				m_wndCaption.UpdateText( item.strName );
			}
		}
		break;
	case TVCONTROL_CHANNEL_DOWN:
		{
			int prev_index = GetPrevChannelIndex(m_eTvMode);
			if(prev_index < 0)
			{
				SetChannel(m_nChannelNumber-1);
				m_wndCaption.UpdateText( GetDefaultChannelName(m_eTvMode, m_nChannelNumber) );
			}
			else
			{
				TVCHANNEL_ITEM& item = m_ChannelList.GetAt(prev_index);
				SetChannel(item.nChannel);
				m_wndCaption.UpdateText( item.strName );
			}
		}
		break;
	case TVCONTROL_VOLUME_UP:
		{
			if(m_nAudioVolume < VOLUME_MAX)
				m_tv.put_Volume(++m_nAudioVolume);
			CString msg;
			msg.Format(_T("Volume %02d"), m_nAudioVolume*100/VOLUME_MAX);
			m_wndCaption.UpdateText( msg );
		}
		break;
	case TVCONTROL_VOLUME_DOWN:
		{
			if(m_nAudioVolume > 0)
				m_tv.put_Volume(--m_nAudioVolume);
			CString msg;
			msg.Format(_T("Volume %02d"), m_nAudioVolume*100/VOLUME_MAX);
			m_wndCaption.UpdateText( msg );
		}
		break;
	case TVCONTROL_VOLUME_MUTE:
		SetMute(!m_bMute);
		m_wndCaption.UpdateText( (m_bMute) ? _T("Mute On") : _T("Mute Off") );
		break;
	}

	SetTimer(MESSAGE_VIEW_TIMER_ID, MESSAGE_VIEW_TIMER_TIME, NULL);
	SetTimer(MESSAGE_STOP_TIMER_ID, MESSAGE_STOP_TIMER_TIME, NULL);
	return 0;
}

void CDvicoTVInterface::OnScanEvent()
{
	// TODO: Add your message handler code here
	long lEventCode = m_tv.get_ScanEventCode();
	if (lEventCode == EVENT_SCAN_CHANNEL_CHANGED)
	{
		long curr_channel = m_tv.get_CurrentScanningChannel();
		long strength     = m_tv.get_SignalStrength();
		bool is_locked    = m_tv.get_IsChannelLocked() ? true : false;

		m_pParentWnd->SendMessage(WM_CHANGE_SCAN_CHANNEL, curr_channel);

		if (is_locked)
		{
			TVCHANNEL_ITEM item;
			item.eTVMode = m_eTvMode;
			item.nChannel = curr_channel;
			item.strName = GetDefaultChannelName(m_eTvMode, curr_channel);
			m_ChannelList.Add(item);
			TRACE(_T("Channel Scanned : %s\r\n"), item.strName);
			m_pParentWnd->SendMessage(WM_ADD_CHANNEL_INFO, (WPARAM)&item);
		}
	}
	else if (lEventCode == EVENT_SCAN_FINISHED)
	{
		m_pParentWnd->SendMessage(WM_COMPLETE_SCAN_CHANNEL);
	}
}

CString CDvicoTVInterface::GetDefaultChannelName(TV_MODE eTvMode, int nChannel)
{
	CString name;
	switch(eTvMode)
	{
	case modeAnalogAir:
		name.Format(_T("%d - AIR"), nChannel);
		break;
	case modeAnalogCatv:
		name.Format(_T("%d - CATV"), nChannel);
		break;
	case modeDigitalAir:
		name.Format(_T("%d - HD AIR"), nChannel);
		break;
	case modeDigitalCatv:
		name.Format(_T("%d - HD CATV"), nChannel);
		break;
	}
	return name;
}

BOOL CDvicoTVInterface::Create(CWnd* pParentWnd, const RECT& rect)
{
	static UINT uID = 0xff00;

	m_pParentWnd = pParentWnd;

	if(_wndReceiveMessage.GetSafeHwnd() == NULL)
	{
		CWnd* wnd = CWnd::FromHandle( ::GetDesktopWindow() );
		_wndReceiveMessage.Create(NULL, _T("receive_message_wnd"), 
									WS_CHILD | WS_CAPTION /*| WS_VISIBLE*/, CRect(0,0,1,1), wnd, uID++);	// 작업표시줄에 윈도우 생성 (ipc 수신 땜시...)
	}

	if(CWnd::Create(NULL, _T("preview"), WS_CHILD | WS_VISIBLE, rect, pParentWnd, uID++) == NULL)
	{
		return FALSE;
	}

	m_rect = rect;
	m_rect.MoveToXY(0, 0);

	if(m_tv.GetSafeHwnd() == NULL)
	{
		BOOL ret_val = m_tv.Create(NULL, _T("hdtv"), WS_CHILD | WS_VISIBLE, CRect(0,0,1,1), 
									( m_bScanningWnd ? this : ::AfxGetMainWnd() ), 0xfeff);

		if(ret_val == FALSE) return FALSE;

		if(m_tv.GetSafeHwnd() != NULL)
		{
			if(!m_bScanningWnd)
			{
				::Sleep(m_nStartupDelay);
			}

			BOOL ret_val = TRUE;
			TRY
			{
				m_tv.Activate();
			}
			CATCH(CException, ex)
			{
				ret_val = FALSE;
			}
			END_CATCH

			if(ret_val == FALSE) return FALSE;

			LONG error = m_tv.get_ErrorCode();   // errNone이 아니면 동작을 멈춰야합니다.
			if (error != CDvicoTV103::errNone) return FALSE;
		}
	}

	BOOL create = m_wndCaption.CreateEx(
		0,
		::AfxRegisterWndClass(0, ::AfxGetApp()->LoadCursor(IDC_ARROW), (HBRUSH)GetStockObject(WHITE_BRUSH)),
		_T("Caption"),
		WS_POPUP /*| WS_VISIBLE*/,
		rect,
		this, NULL, NULL);

	__DEBUG__("Before Lock", _NULL);
	_mutexMap.Lock();
	_mapInterfacePtr.SetAt(this, this);
	_mutexMap.Unlock();
	__DEBUG__("After Lock", _NULL);

	MoveWindow(rect.left, rect.top, m_rect.Width(), m_rect.Height());

	return TRUE;
}

void CDvicoTVInterface::Destroy(bool bPerfect)
{
	Stop();

	CWnd* wnd = ::AfxGetMainWnd();
	if(m_tv.GetSafeHwnd())
	{
		m_tv.put_NotifyWindow((long)wnd->GetSafeHwnd());
		m_tv.put_PreviewWindow((long)wnd->GetSafeHwnd());
	}

	__DEBUG__("Before Lock", _NULL);
	_mutexMap.Lock();
	_mapInterfacePtr.RemoveKey(this);
	_mutexMap.Unlock();
	__DEBUG__("After Lock", _NULL);

	//if(_mapInterfacePtr.GetCount() == 0)
	if(bPerfect && m_tv.GetSafeHwnd())
	{
		m_tv.Deactivate();
		m_tv.DestroyWindow();
	}

	DestroyWindow();
}

void CDvicoTVInterface::_setMode(TV_MODE mode, BOOL bNTSC)
{
	switch(mode)
	{
	case modeAll:
//		return FALSE;
		break;

	case modeAnalogAir:
		m_nTvStandard = (bNTSC) ? CDvicoTV103::formatNTSC : CDvicoTV103::formatPAL;
		m_nSourceType = CDvicoTV103::sourceTuner;
		m_nSignalType = CDvicoTV103::signalAir;
		break;

	case modeAnalogCatv:
		m_nTvStandard = (bNTSC) ? CDvicoTV103::formatNTSC : CDvicoTV103::formatPAL;
		m_nSourceType = CDvicoTV103::sourceTuner;
		m_nSignalType = CDvicoTV103::signalCatv;
		break;

	case modeDigitalAir:
		m_nTvStandard = CDvicoTV103::formatATSC;
		m_nSourceType = CDvicoTV103::sourceTuner;
		m_nSignalType = CDvicoTV103::signalAir;
		break;

	case modeDigitalCatv:
		m_nTvStandard = CDvicoTV103::formatATSC;
		m_nSourceType = CDvicoTV103::sourceTuner;
		m_nSignalType = CDvicoTV103::signalCatv;
		break;

	case modeComposite:
		m_nTvStandard = (bNTSC) ? CDvicoTV103::formatNTSC : CDvicoTV103::formatPAL;
		m_nSourceType = CDvicoTV103::sourceVideo;
		m_nSignalType = CDvicoTV103::signalAir;
		break;

	case modeSvhs:
		m_nTvStandard = (bNTSC) ? CDvicoTV103::formatNTSC : CDvicoTV103::formatPAL;
		m_nSourceType = CDvicoTV103::sourceSvhs;
		m_nSignalType = CDvicoTV103::signalAir;
		break;
	}
}

TV_MODE	CDvicoTVInterface::GetTVMode()
{
	long tuner_type  = m_tv.get_TunerMode();
	long source_type = m_tv.get_Source();
	long signal_type = m_tv.get_SignalType();

	if( ( tuner_type  == CDvicoTV103::formatNTSC || tuner_type  == CDvicoTV103::formatPAL ) &&
		source_type == CDvicoTV103::sourceTuner &&
		signal_type == CDvicoTV103::signalAir )
			return modeAnalogAir;

	if( ( tuner_type  == CDvicoTV103::formatNTSC || tuner_type  == CDvicoTV103::formatPAL ) &&
		source_type == CDvicoTV103::sourceTuner &&
		signal_type == CDvicoTV103::signalCatv )
			return modeAnalogCatv;

	if( tuner_type  == CDvicoTV103::formatATSC &&
		source_type == CDvicoTV103::sourceTuner &&
		signal_type == CDvicoTV103::signalAir )
			return modeDigitalAir;

	if( tuner_type  == CDvicoTV103::formatATSC &&
		source_type == CDvicoTV103::sourceTuner &&
		signal_type == CDvicoTV103::signalCatv )
			return modeDigitalCatv;

	if( source_type == CDvicoTV103::sourceVideo )
			return modeComposite;

	if( source_type == CDvicoTV103::sourceSvhs )
			return modeSvhs;

	return modeAll;
}

int CDvicoTVInterface::GetChannel()
{
	return m_tv.get_Channel();
}

int CDvicoTVInterface::GetVolume()
{
	long vol = m_tv.get_Volume();
	return (vol * 100) / VOLUME_MAX;
}

int CDvicoTVInterface::GetMute()
{
	return m_tv.get_Mute();
}

BOOL CDvicoTVInterface::SetTVMode(TV_MODE mode, BOOL bNTSC)
{
	m_eTvMode = mode;
	m_bNTSC = bNTSC;

	if(m_tv.GetSafeHwnd()==NULL) return FALSE;
	if(!m_bPlay) return TRUE;

	Stop();
	Play();

	return TRUE;
}

BOOL CDvicoTVInterface::SetChannel(int nChannel)
{
	m_nChannelNumber = nChannel;

	SetChannelIndex();

	if(m_tv.GetSafeHwnd()==NULL) return FALSE;
	if(!m_bPlay) return TRUE;

	m_tv.put_Channel(m_nChannelNumber);

	return TRUE;
}

BOOL CDvicoTVInterface::SetVolume(int nVol)
{
	int val = nVol * VOLUME_MAX / 100;
	if(val < 0) val = 0;
	else if(val > VOLUME_MAX) val = VOLUME_MAX;

	m_nAudioVolume = val;

	if(m_tv.GetSafeHwnd()==NULL) return FALSE;
	if(!m_bPlay) return TRUE;

	m_tv.put_Volume(val);

	return TRUE;
}

BOOL CDvicoTVInterface::SetMute(BOOL bMute)
{
	m_bMute = bMute;

	if(m_tv.GetSafeHwnd()==NULL) return FALSE;
	if(!m_bPlay) return TRUE;

	m_tv.put_Mute(bMute);
	if(!bMute) m_tv.put_Volume(m_nAudioVolume);

	return TRUE;
}

BOOL CDvicoTVInterface::MoveWindow(int x, int y, int cx, int cy)
{
	m_rect.SetRect(x, y, x+cx, y+cy);

	if(GetSafeHwnd())
	{
		CWnd::MoveWindow(x, y, cx, cy);

		if(m_wndCaption.GetSafeHwnd())
		{
			CRect window_rect;
			GetWindowRect(window_rect);

			m_wndCaption.MoveWindow(window_rect.left, window_rect.top, m_rect);
			m_wndCaption.Invalidate(FALSE);
		}

		if(m_tv.GetSafeHwnd()==NULL) return FALSE;
		if(!m_bPlay) return TRUE;

//		m_tv.MoveWindow(0, 0, cx, cy);
		m_tv.SetVideoRect(0, 0, cx, cy);

		return TRUE;
	}
	return FALSE;
}

BOOL CDvicoTVInterface::Play()
{
	if(m_tv.GetSafeHwnd()==NULL) return FALSE;
	if(m_bPlay) return TRUE;

	SendAllStopMessage();

	Prepare();

	m_bPlay = true;
	SetChannelIndex();

	m_tv.Play();

	return TRUE;
}

BOOL CDvicoTVInterface::Stop()
{
	if(m_tv.GetSafeHwnd()==NULL) return FALSE;
	if(!m_bPlay) return FALSE;

	m_bPlay = false;
	m_nTxResolution = -1;
	m_nCurrentChannelIndex = -1;

	m_tv.SetVideoRect(0, 0, 1, 1);
	m_tv.Stop();

	return TRUE;
}

void CDvicoTVInterface::Prepare()
{
	if(m_tv.GetSafeHwnd()==NULL) return;

	m_tv.put_NotifyWindow((long)GetSafeHwnd());
	m_tv.put_PreviewWindow((long)GetSafeHwnd());

	_setMode(m_eTvMode, m_bNTSC);

	if (m_nTvStandard >= CDvicoTV103::formatATSC)
	{
		m_tv.put_TunerMode(CDvicoTV103::tmodeATSC);
		m_tv.put_Source(CDvicoTV103::sourceTuner);
	}
	else
	{
		m_tv.put_Support88xAudio(TRUE);    // Gold version only
		m_tv.put_AudioCable(TRUE);
		m_tv.put_TunerMode(CDvicoTV103::tmodeAnalogTv);
		m_tv.put_Source(m_nSourceType);
	}

	m_tv.put_CountryCode(m_nCountryCode);
	m_tv.put_SignalType(m_nSignalType);
	m_tv.put_IsAirSignal(m_nSignalType == CDvicoTV103::signalAir);

	m_tv.put_Channel(m_nChannelNumber);
	m_tv.put_Volume(m_nAudioVolume);
	m_tv.put_Mute(m_bMute);
	m_tv.MoveWindow(0, 0, m_rect.Width(), m_rect.Height());
	m_tv.SetVideoRect(0, 0, m_rect.Width(), m_rect.Height());
	//m_tv.put_IsVMRRenderer(TRUE);
}

BOOL CDvicoTVInterface::StartScan()
{
	Stop();
	Prepare();
	m_tv.put_ScanModulations(CDvicoTV103::vchmodAll); // 8VSB, 64QAM, 256QAM 모두 스캔
	m_tv.put_ScanNow(TRUE);

	return TRUE;
}

BOOL CDvicoTVInterface::StopScan()
{
	m_tv.put_ScanNow(FALSE);
	m_tv.Stop();

	return TRUE;
}

void CDvicoTVInterface::SendAllStopMessage()
{
	void* key;
	void* value;

	__DEBUG__("Before Lock", _NULL);
	_mutexMap.Lock();

	POSITION pos = _mapInterfacePtr.GetStartPosition();

	while ( pos != NULL )
	{
		_mapInterfacePtr.GetNextAssoc(pos, key, (void*&)value);

		CDvicoTVInterface* tv_interface = (CDvicoTVInterface*)value;
		if(tv_interface->GetSafeHwnd() && tv_interface->IsPlaying())
			tv_interface->SendMessage(WM_STOP_TV);
	}

	_mutexMap.Unlock();
	__DEBUG__("After Lock", _NULL);
}

void CDvicoTVInterface::SendControlMessage(int control)
{
	void* key;
	void* value;

	__DEBUG__("Before Lock", _NULL);
	_mutexMap.Lock();

	POSITION pos = _mapInterfacePtr.GetStartPosition();

	while ( pos != NULL )
	{
		_mapInterfacePtr.GetNextAssoc(pos, key, (void*&)value);

		CDvicoTVInterface* tv_interface = (CDvicoTVInterface*)value;
		if(tv_interface->GetSafeHwnd() && tv_interface->IsPlaying())
			tv_interface->SendMessage(WM_CONTROL_TV, control);
	}

	_mutexMap.Unlock();
	__DEBUG__("After Lock", _NULL);
}

void CDvicoTVInterface::SetChannelIndex()
{
	m_nCurrentChannelIndex = -1;
	int count = m_ChannelList.GetCount();
	for(int i=0; i<count; i++)
	{
		TVCHANNEL_ITEM& item = m_ChannelList.GetAt(i);
		if(item.eTVMode == m_eTvMode && item.nChannel == m_nChannelNumber)
		{
			m_nCurrentChannelIndex = i;
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////

static CReceiveMessageWnd		g_wnd;
static UINT msgKillTVModule = ::RegisterWindowMessage(_T("GLOBAL_KILL_TV_MODULE"));
static UINT msgControlTVModule = ::RegisterWindowMessage(_T("GLOBAL_CONTROL_TV_MODULE"));


IMPLEMENT_DYNAMIC(CReceiveMessageWnd, CWnd)


CReceiveMessageWnd::CReceiveMessageWnd()
{
}

CReceiveMessageWnd::~CReceiveMessageWnd()
{
}


BEGIN_MESSAGE_MAP(CReceiveMessageWnd, CWnd)
	ON_REGISTERED_MESSAGE(msgKillTVModule, OnKillTVModule)
	ON_REGISTERED_MESSAGE(msgControlTVModule, OnControlTVModule)
END_MESSAGE_MAP()


LRESULT CReceiveMessageWnd::OnKillTVModule(WPARAM wParam, LPARAM lParam)
{
	CDvicoTVInterface::SendAllStopMessage();
	return 0;
}

LRESULT CReceiveMessageWnd::OnControlTVModule(WPARAM wParam, LPARAM lParam)
{
	CDvicoTVInterface::SendControlMessage(wParam);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CCaptionWnd, CWnd)


CCaptionWnd::CCaptionWnd()
{
}

CCaptionWnd::~CCaptionWnd()
{
}


BEGIN_MESSAGE_MAP(CCaptionWnd, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CCaptionWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 __super::OnPaint()을(를) 호출하지 마십시오.

	CRect rect;
	GetClientRect(rect);

	dc.FillSolidRect(rect, RGB(0,0,0));

	CFont font;
	font.CreatePointFont(m_nFontSize*10, "Arial");
	dc.SelectObject(&font);

	dc.SetTextColor(RGB(0,255,0));
	dc.SetBkColor(RGB(0,0,0));
	dc.DrawText(m_strMessage, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
}

int CCaptionWnd::GetMessagwWidth()
{
	CDC* dc = GetDC();

	CFont font;
	font.CreatePointFont(m_nFontSize*10, "Arial");

	CFont* old_font = dc->SelectObject(&font);
	CSize size = dc->GetOutputTextExtent(m_strMessage);

	dc->SelectObject(old_font);
	ReleaseDC(dc);

	return size.cx;
}

void CCaptionWnd::MoveWindow(int x, int y, CRect rectParent)
{
	m_rectParent = rectParent;
	m_nWindowLeft = x;
	m_nWindowTop = y;

	UpdateText();
}

void CCaptionWnd::UpdateText(LPCTSTR lpszText)
{
	if(lpszText) m_strMessage = lpszText;

	int right = m_rectParent.right - m_rectParent.Width()/20;
	m_nFontSize = m_rectParent.Height() / 20;

	int left = right - GetMessagwWidth() - m_nFontSize;

	CRect rect(left,m_nFontSize,right,m_nFontSize*3);
	rect.OffsetRect(m_nWindowLeft, m_nWindowTop);

	TRACE(_T("(%d, %d, %d, %d)\r\n"), rect.left, rect.top, rect.right, rect.bottom);
	CWnd::MoveWindow(rect, FALSE);
}
