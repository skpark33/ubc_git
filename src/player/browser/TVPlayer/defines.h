#define WM_DSHOW_GRAPH_NOTIFY    (WM_USER + 2000)
#define WM_PVRDLL_NOTIFY         (WM_USER + 2001)

#define NOTIFY2_EVENTCODE_EOS                      0xbad1
#define NOTIFY2_EVENTCODE_PMTPID_CHANGED           0xbad2
#define NOTIFY2_EVENTCODE_AVPID_CHANGED            0xbad3
#define NOTIFY2_EVENTCODE_DECODER_CHANGED          0xbad4
#define NOTIFY2_EVENTCODE_AUDIO_DEC_CHANGED        0xbad5
#define NOTIFY2_EVENTCODE_VIDEO_DEC_CHANGED        0xbad6
#define NOTIFY2_EVENTCODE_PROGRAM_INFO_CHANGED     0xbad7
#define NOTIFY2_EVENTCODE_VIRTUAL_CHANNEL_CHANGED  0xbad8
#define NOTIFY2_EVENTCODE_DVB_EPG_CHANGED          0xbad9
#define NOTIFY2_EVENTCODE_PROGRAM_CHANGED          0xbada
#define NOTIFY2_EVENTCODE_AUDIO_STREAM_CHANGED     0xbadb
#define NOTIFY2_EVENTCODE_AUDIO_SAMPLING_CHANGED   0xbadc
#define NOTIFY2_EVENTCODE_TTXPID_CHANGED           0xbadd
#define NOTIFY2_EVENTCODE_MULTIVIEW_SET            0xbade
#define NOTIFY2_EVENTCODE_EPG_CHANGED              0xbadf
#define NOTIFY2_EVENTCODE_RATING_REGION_CHANGED    0xbae0
#define NOTIFY2_EVENTCODE_EXTENDED_TEXT_CHANGED    0xbae1
#define NOTIFY2_EVENTCODE_ENSEMBLE_INFO_CHANGED    0xbae2

#define EVENT_COMPLETE	                           0x1
#define EVENT_SCAN_CHANNEL_CHANGED	               0x100
#define EVENT_SCAN_FINISHED	                     0x101
#define EVENT_MOTION_CHANGED	                     0x102
#define EVENT_CHANNEL_CHANGED	                     0x103
#define EVENT_RECORDING_OVERFLOWED	               0x104
#define EVENT_OUTOF_DISK_SPACE	                  0x105
#define EVENT_STREAM_POSITION_CHANGED	            0x106
#define EVENT_SOURCE_CHANGED	                     0x107
#define EVENT_HARDWARE_INITIALIZING	               0x108
#define EVENT_PLAYRATE_CHANGED	                  0x109
#define EVENT_ENCODING_PARAM_CHANGED	            0x10B

#define MAX_ZULU_ADAPTERS                          16
#define MAX_TS_PROGRAMS                            64
#define MAX_ES_NUMBERS			                     16
#define MAX_VIRTUAL_CHANNELS	                     100
#define MAX_STRING_NUM			                     256
#define MAX_STRING_BYTE			                     256
#define MAX_EIT_ITEM			                        126

enum PlayRate {
   PLAY_16X_REVERSE     = -16000,   // - x16
   PLAY_8X_REVERSE      = -8000,    // - x8
   PLAY_4X_REVERSE      = -4000,    // - x4
   PLAY_2X_REVERSE      = -2000,    // - x2
   PLAY_1X_REVERSE      = -1000,    // - x1
   PVR_PLAY_08X_REVERSE = -800,     // x0.8
   PVR_PLAY_05X_REVERSE = -500,     // x0.5
   PVR_PLAY_03X_REVERSE = -300,     // x0.3
   PVR_PLAY_02X_REVERSE = -200,     // x0.2
   PVR_PLAY_01X_REVERSE = -100,     // x0.1
   PVR_PLAY_01X_FORWARD = 100,      // x0.1
   PVR_PLAY_02X_FORWARD = 200,      // x0.2
   PVR_PLAY_03X_FORWARD = 300,      // x0.3
   PVR_PLAY_05X_FORWARD = 500,      // x0.5
   PVR_PLAY_08X_FORWARD = 800,      // x0.8
   PLAY_1X_FORWARD      = 1000,     // x1
   PLAY_2X_FORWARD      = 2000,     // x2
   PLAY_4X_FORWARD      = 4000,     // x4
   PLAY_8X_FORWARD      = 8000,     // x8
   PLAY_16X_FORWARD     = 16000,    // x16
};

enum VCH_MODULATION_TYPE
{
	VCH_MODULATION_NONE = 0,
	VCH_MODULATION_ANALOG,
	VCH_MODULATION_64QAM,
	VCH_MODULATION_256QAM,
	VCH_MODULATION_8VSB,
	VCH_MODULATION_16VSB,
	VCH_MODULATION_COFDM_16QAM,
	VCH_MODULATION_COFDM_32QAM,
	VCH_MODULATION_COFDM_64QAM,
	VCH_MODULATION_QPSK,
	VCH_MODULATION_8PSK,
	VCH_MODULATION_TURBOCODED,
	VCH_MODULATION_64QAM_ENCRYPTED = 0x10,
	VCH_MODULATION_256QAM_ENCRYPTED,
	VCH_MODULATION_QPSK_ENCRYPTED,
	VCH_MODULATION_8PSK_ENCRYPTED,
	VCH_MODULATION_TURBOCODED_ENCRYPTED,
};

typedef enum VCH_SERVICE_TYPE
{
	VCH_SERVICE_NONE = 0,
	VCH_SERVICE_ANALOG_TV,
	VCH_SERVICE_ATSC_DTV,
	VCH_SERVICE_ATSC_AUDIO_ONLY,
	VCH_SERVICE_ATSC_DATA,
};

const DWORD NUMOF_PRODUCTS                = 47;
const DWORD PRODUCT_CODE_TV2              = 10421;
const DWORD PRODUCT_CODE_HDTV1            = 10431;
const DWORD PRODUCT_CODE_HDTV20           = 10432;
const DWORD PRODUCT_CODE_HDTV21           = 10432;
const DWORD PRODUCT_CODE_HDTV3S           = 10433;
const DWORD PRODUCT_CODE_HDRS             = 10434;
const DWORD PRODUCT_CODE_HDTV3ST          = 10435;
const DWORD PRODUCT_CODE_HDTV3G           = 10438;
const DWORD PRODUCT_CODE_HDRG             = 10439;
const DWORD PRODUCT_CODE_HDTV3SPLUS       = 10440;
const DWORD PRODUCT_CODE_DVBT1            = 10442;
const DWORD PRODUCT_CODE_DVBT2            = 10443;
const DWORD PRODUCT_CODE_DVBT3            = 10444;
const DWORD PRODUCT_CODE_DVBT_ZX          = 10445;
const DWORD PRODUCT_CODE_DVBT_HYBRID      = 10446;
const DWORD PRODUCT_CODE_DVBT_DUAL        = 10447;
const DWORD PRODUCT_CODE_DVBT_DUAL4       = 10448;
const DWORD PRODUCT_CODE_DVBS1            = 10450;
const DWORD PRODUCT_CODE_DVBS2            = 10451;
const DWORD PRODUCT_CODE_USB_DVBT1        = 10482;
const DWORD PRODUCT_CODE_USB_DVBT2        = 10483;
const DWORD PRODUCT_CODE_USB_DVBT_NANO    = 10484;
const DWORD PRODUCT_CODE_USB_DVBT_NANO2   = 10485;
const DWORD PRODUCT_CODE_USB_DVBT_DUAL4   = 10486;
const DWORD PRODUCT_CODE_USB_DVBT_DUAL    = 10487;
const DWORD PRODUCT_CODE_USB_DVBT_DUAL2   = 10488;
const DWORD PRODUCT_CODE_USB_DVBT_DUAL3   = 10489;
const DWORD PRODUCT_CODE_USB_DVBT_NANOPLUS= 10491;
const DWORD PRODUCT_CODE_USB_ATSC1        = 10588; // LG062F NIM
const DWORD PRODUCT_CODE_USB_ATSC2        = 10589; // SANYO0627 NIM
const DWORD PRODUCT_CODE_USB_ATSCX1       = 10590; // XC3028+LG3303
const DWORD PRODUCT_CODE_USB_ATSCX2       = 10591; // XC3028+SH1409
const DWORD PRODUCT_CODE_USB_ATSCX0       = 10599; // XC3028 Basic (Experimental)
const DWORD PRODUCT_CODE_HDTV5LITE        = 10531;
const DWORD PRODUCT_CODE_HDTV5LX          = 10532;
const DWORD PRODUCT_CODE_HDTV5G           = 10538;
const DWORD PRODUCT_CODE_HDTV5GX          = 10540;
const DWORD PRODUCT_CODE_HDTV5LITE_RT     = 10541;
const DWORD PRODUCT_CODE_HDTV5DUAL        = 10544;
const DWORD PRODUCT_CODE_HDTV5GOLD_RT     = 10548;
const DWORD PRODUCT_CODE_HDTV6LITE        = 10542;
const DWORD PRODUCT_CODE_HDTV6GOLD        = 10549;
const DWORD PRODUCT_CODE_HDTV5NANO1       = 10550; // XC3028+SH1409
const DWORD PRODUCT_CODE_HDTV6COOL        = 10551; // XC3028 + Auvitek8501
const DWORD PRODUCT_CODE_HDTV5HOME3       = 10552; // Qjingja4000 + Auvitek8501
const DWORD PRODUCT_CODE_USB_TDMB1        = 10601;
const DWORD PRODUCT_CODE_USB_ATSC_MASTER  = 10603;


struct TunerModel {
   DWORD productId;
   char  name[24];
};

const int NUMOF_MODELS = 44;
const TunerModel MODELS[NUMOF_MODELS] = {
      { PRODUCT_CODE_TV2        ,      "FusionTV2" },
      { PRODUCT_CODE_HDTV1      ,      "HDTV1" },
      { PRODUCT_CODE_HDTV20     ,      "HDTV2" },
      { PRODUCT_CODE_HDTV3S     ,      "HDTV3 Silver" },
      { PRODUCT_CODE_HDRS       ,      "HDR Silver" },
      { PRODUCT_CODE_HDTV3ST    ,      "HDTV3 SilverT" },
      { PRODUCT_CODE_HDTV5LITE  ,      "HDTV5 Lite" },
      { PRODUCT_CODE_HDTV5LX    ,      "HDTV5 LiteX" },
      { PRODUCT_CODE_HDTV3G     ,      "HDTV3 Gold" },
      { PRODUCT_CODE_HDRG       ,      "HDR Gold" },
      { PRODUCT_CODE_HDTV5G     ,      "HDTV5 Gold" },
      { PRODUCT_CODE_HDTV5GX    ,      "HDTV5 GX" },
      { PRODUCT_CODE_HDTV3SPLUS ,      "HDTV3 Silver Plus" },
      { PRODUCT_CODE_DVBT1      ,      "DVB-T1" },
      { PRODUCT_CODE_DVBT2      ,      "DVB-T Plus" },
      { PRODUCT_CODE_DVBT3      ,      "DVB-T3" },
      { PRODUCT_CODE_DVBS1      ,      "DVB-S1" },
      { PRODUCT_CODE_DVBS2      ,      "DVB-S2" },
      { PRODUCT_CODE_USB_DVBT1  ,      "USB DVB-T1" },
      { PRODUCT_CODE_USB_DVBT2  ,      "USB DVB-T2" },
      { PRODUCT_CODE_USB_ATSC1  ,      "USB ATSC1" },
      { PRODUCT_CODE_USB_ATSC2  ,      "USB ATSC2" },
      { PRODUCT_CODE_DVBT_ZX    ,      "DVBT-PRO" },
      { PRODUCT_CODE_DVBT_HYBRID,      "DVB-T Hybrid"},
      { PRODUCT_CODE_DVBT_DUAL,        "DVB-T Dual(PCI)"},
      { PRODUCT_CODE_USB_DVBT_DUAL,    "DVB-T Dual(USB)"},
      { PRODUCT_CODE_DVBT_DUAL4,       "DVB-T Dual4(Express)"},
      { PRODUCT_CODE_HDTV5LITE_RT,     "HDTV5 Lite-RT" },
      { PRODUCT_CODE_HDTV5GOLD_RT,     "HDTV5 Gold-RT" },
      { PRODUCT_CODE_HDTV5NANO1,       "HDTV5 Nano1" },
      { PRODUCT_CODE_HDTV6COOL,        "HDTV6 Cool" },
      { PRODUCT_CODE_HDTV5HOME3,       "HDTV5 Home3" },
      { PRODUCT_CODE_HDTV6LITE  ,      "HDTV6 Lite" },
      { PRODUCT_CODE_HDTV6GOLD  ,      "HDTV6 Gold" },
      { PRODUCT_CODE_USB_TDMB1,        "T-DMB1 (USB)"},
      { PRODUCT_CODE_USB_DVBT_NANO,    "DVB-T Nano"},
      { PRODUCT_CODE_USB_DVBT_NANO2,   "DVB-T Nano2"},
      { PRODUCT_CODE_USB_DVBT_DUAL2,   "DVB-T Dual2"},
      { PRODUCT_CODE_USB_DVBT_DUAL3,   "DVB-T Dual3"},
      { PRODUCT_CODE_USB_DVBT_DUAL4,   "DVB-T Dual4"},
      { PRODUCT_CODE_USB_ATSC_MASTER,  "ATSC/DMB Duo"},
      { PRODUCT_CODE_USB_DVBT_NANOPLUS,"DVB-T Hybrid1"},
      { PRODUCT_CODE_USB_ATSCX1,       "ATSC X1"},
      { PRODUCT_CODE_USB_ATSCX2,       "ATSC X2"},
   };

struct PsiProgramInfo {
   DWORD numbers;
   WORD  programs[MAX_TS_PROGRAMS];
   WORD  pids[MAX_TS_PROGRAMS-1];
   WORD  tsid;
};

struct OneVirtualChannel
{
	TCHAR shortName[40];
	WORD  majorChannel;
	WORD  minorChannel;
	WORD  programNumber;
	BYTE  modulation;          // MODULATION_NONE..MODULATION_16VSB
	BYTE  serviceType;         // SERVICE_NONE..SERVICE_ATSC_DATA
	WORD  channelTsid;         // channel TSID
	WORD  sourceId;            // source ID for EIT
	WORD  encrypted;
	WORD  pcrPid;
	WORD  esNumbers;
	WORD  esType[MAX_ES_NUMBERS];
	WORD  esPid[MAX_ES_NUMBERS];
};

struct PsiVirtualChannelInfo 
{
      DWORD             numbers;
      OneVirtualChannel channels[MAX_VIRTUAL_CHANNELS];
      WORD              tsid;                // transport stream id
      TCHAR             networkName[40];     // name of the network provider 
      WORD              onid;                // original network id
};

struct MultipleString
{
	WORD           numbers;
	WORD           size[MAX_STRING_NUM];
	TCHAR          text[MAX_STRING_BYTE];
};

struct OneEventTable 
{
	WORD           eventId;
	SYSTEMTIME     startTime;
	DWORD          duration;
	BYTE           etmLocation;
	MultipleString title;
	MultipleString extText;
	DWORD          components;
	DWORD          genre;
	TCHAR          rating[MAX_STRING_NUM];
};

struct PsiEpgInfo 
{
	DWORD          tsId;       // transport stream id
	DWORD          onId;       // original network id
	DWORD          sourceId;
	DWORD          version;
	DWORD          numbers;
	OneEventTable  events[MAX_EIT_ITEM];
};


// -- Device information structure
struct TOneDeviceInfo {
   DWORD    oem;
   DWORD    uiNumber;
   DWORD    productId;
   DWORD    capabilities;
   DWORD    capExt;
   DWORD    used;
   DWORD    valid;
   DWORD    isGold;
};

struct TAllDeviceInfo {
   DWORD          numbers;
   LONG           currIndex;
   TOneDeviceInfo deviceInfo[MAX_ZULU_ADAPTERS];
};