// TVPlayer.h
//


#pragma once

#include <afxmt.h>

#include "TVInterface.h"


///////////////////////////////////////////////////////////
class CNoTVInterface : public CWnd, public CTVInterface
{
	DECLARE_DYNAMIC(CNoTVInterface)

public:
	CNoTVInterface(bool bScanningWnd);
	virtual ~CNoTVInterface();

protected:
	DECLARE_MESSAGE_MAP()

	CFont	m_font;

public:
	afx_msg void OnPaint();

public:

	virtual BOOL	Create(CWnd* pParentWnd, const RECT& rect);
	virtual void	Destroy(bool bPerfect) {};

	virtual TV_MODE	GetTVMode() { return modeAll; };
	virtual int		GetChannel() { return -1; };
	virtual int		GetVolume() { return -1; };
	virtual int		GetMute() { return -1; };

	virtual BOOL	SetTVMode(TV_MODE mode, BOOL bNTSC) { return TRUE; };		// tv모드 설정
	virtual BOOL	SetChannel(int nChannel) { return TRUE; };					// 채널 설정
	virtual BOOL	SetVolume(int nVol) { return TRUE; };						// min(0~100)max
	virtual BOOL	SetMute(BOOL bMute) { return TRUE; };						// 조용히 설정
	virtual BOOL	MoveWindow(int x, int y, int cx, int cy) { return TRUE; };	// 출력 윈도우 위치 설정

	virtual BOOL	Play() { return TRUE; };
	virtual BOOL	Stop() { return TRUE; };

	virtual BOOL	StartScan() { return TRUE; };
	virtual BOOL	StopScan() { return TRUE; };

};
