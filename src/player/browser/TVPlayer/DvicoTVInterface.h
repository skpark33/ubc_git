// TVPlayer.h
//


#pragma once

#include <afxmt.h>

#include "defines.h"
#include "TVInterface.h"
#include "DvicoTV103.h"


class CReceiveMessageWnd;


/////////////////////////////////////////////////////////////
// 방송 자막 윈도우
/////////////////////////////////////////////////////////////
class CCaptionWnd : public CWnd
{
	DECLARE_DYNAMIC(CCaptionWnd)

public:
	CCaptionWnd();
	virtual ~CCaptionWnd();

protected:

	DECLARE_MESSAGE_MAP()

	CRect		m_rectParent;
	int			m_nWindowLeft;
	int			m_nWindowTop;

	int			GetMessagwWidth();

public:
	afx_msg void OnPaint();

	int			m_nFontSize;
	CString		m_strMessage;		// 화면 디스플레이 메시지

	void		MoveWindow(int x, int y, CRect rectParent);
	void		UpdateText(LPCTSTR lpszText=NULL);
};


///////////////////////////////////////////////////////////
class CDvicoTVInterface : public CWnd, public CTVInterface
{
	DECLARE_DYNAMIC(CDvicoTVInterface)

public:
	CDvicoTVInterface(bool bScanningWnd);
	virtual ~CDvicoTVInterface();

protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
	DECLARE_EVENTSINK_MAP()

public:
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

protected:
	static	CDvicoTV103			m_tv;				// tv모듈 인스턴스
	static	CMapPtrToPtr		_mapInterfacePtr;	// CDvicoTVInterface 포인터 맵
	static	CMutex				_mutexMap;

	static	CReceiveMessageWnd	_wndReceiveMessage;	// tv 모듈 중지 ipc용 윈도우

	TV_MODE			m_eTvMode;	// tv수신 모드 : modeAnalogAir, modeAnalogCatv, modeDigitalAir, modeDigitalCatv, modeComposite, modeSvhs
	BOOL			m_bNTSC;	// 아나로그 수신 모드 : TRUE=NTSC, FALSE=PAL
	BOOL			m_bMute;	// 조용히
	CRect			m_rect;		// 출력 윈도우 위치

	DWORD			m_nTvStandard;		// ntsc, pal, dtv
	DWORD			m_nSourceType;		// svhs, composite, tuner
	DWORD			m_nSignalType;		// air, catv
	DWORD			m_nCountryCode;		// 국가코드 (하단에 국가코드 example)
	LONG			m_nChannelNumber;	// 채널
	DWORD			m_nAudioVolume;		// 볼륨

	long			m_nTxResolution;	// TxResolution 값
	bool			m_bPlay;			// 재생중 플래그
	CCaptionWnd		m_wndCaption;

	void			Prepare();			// 재생준비 세팅

	void			_setMode(TV_MODE mode, BOOL bNTSC);	// 현재tv모드 설정

	// scan param&func
	PsiVirtualChannelInfo	m_PsiVirtualChannelInfo;	// 가상채널정보
	bool			m_bScanningWnd;			// 채널스캔용 윈도우 flag
	void			OnScanEvent();			// ocx로부터 이벤트 수신
	CString			GetDefaultChannelName(TV_MODE eTvMode, int nChannel);

	void			SetChannelIndex();		// ChannelList에서 현재채널index검색

public:
	afx_msg	LRESULT	OnStopTV(WPARAM wParam, LPARAM lParam);		// tv중지 메시지 처리
	afx_msg	LRESULT	OnControlTV(WPARAM wParam, LPARAM lParam);		// tv중지 메시지 처리

	virtual BOOL	Create(CWnd* pParentWnd, const RECT& rect);
	virtual void	Destroy(bool bPerfect);

	virtual TV_MODE	GetTVMode();
	virtual int		GetChannel();
	virtual int		GetVolume();
	virtual int		GetMute();

	virtual BOOL	SetTVMode(TV_MODE mode, BOOL bNTSC);		// tv모드 설정
	virtual BOOL	SetChannel(int nChannel);					// 채널 설정
	virtual BOOL	SetVolume(int nVol);						// min(0~100)max
	virtual BOOL	SetMute(BOOL bMute);						// 조용히 설정
	virtual	BOOL	MoveWindow(int x, int y, int cx, int cy);	// 출력 윈도우 위치 설정

	virtual BOOL	Play();
	virtual BOOL	Stop();

	virtual BOOL	StartScan();
	virtual BOOL	StopScan();

	bool			IsPlaying() { return m_bPlay; };

	static	void	SendAllStopMessage();		// 모든 인스턴스 중지
	static	void	SendControlMessage(int control);		// 인스턴스 컨트롤

};

/*
COUNTRY_CODE
      {"Australia", 61},
      {"France",    33},
      {"Germany",   49},
      {"Japan",     81},
      {"Korea",     82},
      {"Singapore", 65},
      {"Spain",     34},
      {"U.K.",      44},
      {"U.S.",      1 },
      {"Vietname",  84}
*/


/////////////////////////////////////////////////////////////
// HWND_BROADCAST 메시지 수신 윈도우 (KillTVModule과의 IPC용)
/////////////////////////////////////////////////////////////
class CReceiveMessageWnd : public CWnd
{
	DECLARE_DYNAMIC(CReceiveMessageWnd)

public:
	CReceiveMessageWnd();
	virtual ~CReceiveMessageWnd();

protected:

	DECLARE_MESSAGE_MAP()

public:

	afx_msg LRESULT OnKillTVModule(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnControlTVModule(WPARAM wParam, LPARAM lParam);
};

