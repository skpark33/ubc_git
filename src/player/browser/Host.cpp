// Host.cpp : 구현 파일
//

#include "stdafx.h"
#include "BRW2.h"
#include "Host.h"
#include "Schedule.h"

#include "EventHandler.h"
#include "MemDC.h"
#include "common/libCommon/ubcHost.h"
#include "installUtil.h"
#include "cci/libValue/cciEnumList.h"
#include "ci/libBase/ciStringUtil.h"
//#include "common/libCommon/ubcIniMux.h"
#include "Tlhelp32.h"
//#include "dbt.h"
//#include "common/libCommon/SecurityIni.h"

#include "Announce_image_Dlg.h"
#include "Announce_video_Dlg.h"
#include "Announce_flash_Dlg.h"
#include "Announce_ppt_Dlg.h"
#include "Announce_web_Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//system key down state를 알아보는 매크로
// 참고 : (GetKeyState(VK_CONTROL) & 0x80000000) 가 1 이면 눌러진것
#define IsCTRLpressed()	((GetKeyState(VK_CONTROL) & (1<<(sizeof(SHORT)*8-1))) != 0)
#define IsSHIFTpressed()((GetKeyState(VK_SHIFT) & (1<<(sizeof(SHORT)*8-1))) != 0)


#define		HEART_BEAT_ID			1000	// id
#define		HEART_BEAT_TIME			(60*1000)	// time(ms)

#define		LOAD_HOST_ID			1002	// id
//#define		LOAD_HOST_TIME			250		// time(ms)
#define		LOAD_HOST_TIME			100		// time(ms)

#define		LOAD_TEMPLATE_ID		1003	// id
//#define		LOAD_TEMPLATE_TIME		250		// time(ms)
#define		LOAD_TEMPLATE_TIME		100		// time(ms)

#define		CHECKFILE_ID			1005	// id
//#define		CHECKFILE_TIME			250		// time(ms)
#define		CHECKFILE_TIME			100		// time(ms)

#define		OPENFILE_ID				1006	// id
//#define		OPENFILE_TIME			250		// time(ms)
#define		OPENFILE_TIME			100		// time(ms)

#define		INIT_HANDLER_ID			1007	// id
//#define		INIT_HANDLER_TIME		250		// time(ms)
#define		INIT_HANDLER_TIME		100		// time(ms)

#define		TEMPLATE_CHECK_ID		1008	// id
//#define		TEMPLATE_CHECK_TIME		250		// time(ms)
#define		TEMPLATE_CHECK_TIME		300		// time(ms)

#define		HOST_SHOW_ID			1009
#define		HOST_SHOW_TIME			100

#define		BRW_EXIT_ID				1010		//초기화 오류로 BRW 종료시키는 타이머
#define		BRW_EXIT_TIME			3000		//초기화 오류로 BRW 종료시키는 타이머 시간

#define		BACKGROUND_COLOR		RGB(0,0,0)

#define		MAX_INIT_HOST_FAIL_COUNT	3

#define		TIMER_COURSOR_HIDE		1011
#define		TIME_COURSOR_HIDE		5000

#define		TIMER_CLICK_JUMP		1012

#define		TIMER_ID_CHECK_CATEGORY		1013
#define		TIMER_TIME_CHECK_CATEGORY	(60*1000)

#define		TIMER_CHECK_UPDATER_ID		1014
#define		TIMER_CHECK_UPDATER_TIME	(10*1000)

#define		TIMER_PPT_ALL_OPEN_ID		1015
#define		TIMER_PPT_ALL_OPEN_TIME		(5000)

// <!-- 깜박임방지 (2016-08-29)
#define		TIMER_SHOWWINDOW_ID			1016
#define		TIMER_SHOWWINDOW_TIME		(100)
// -->

// <!-- 라이센스 체크 딜레이 (2016-11-14)
#define		TIMER_LICENSE_CHECK_ID		1017
// -->

#define		WM_USER_ANNOUNCE		23456

typedef BOOL (*lpProcSetHook)(HWND);		//HookDll의 SetHook 함수 포인터
typedef BOOL (*lpProcRemoveHook)(void);		//HookDll의 RemoveHook 함수 포인터


//초기화중에 프로그래시브바 계산하기 위한 정의
/*----------------------------------------------------------
1) Init UI						: 10
2) Init Host(Load Host)			: 50
3) Init Template(Load Template)	: 440
4) Check File					: 100
5) Open File					: 390
6) Init Handler					: 10
-----------------------------------------------------------*/

//초기화 각 상태의 유효 카운트 값
//int CBackWnd::m_nModeValue[CNT_INIT_MODE] = { 10, 50, 440, 100, 390, 10 };
//int CBackWnd::m_nModeValue[CNT_INIT_MODE] = { 10, 60, 500, 600, 990, 1000 };
int CBackWnd::m_nModeValue[CNT_INIT_MODE] = { 10, 60, 500, 750, 990, 1000 };

#define		DLG_FRM_SIZE				4			///<Dialog fraem의 크기

#define		PRG_INIT_HEIGHT				20			///<초기화과정을 보여주는 프로그래시브바 높이
#define		PRG_INIT_TOP_GAP			214			///<초기화과정을 보여주는 프로그래시브바의 상단으로부터의 거리
#define		PRG_INIT_BOTTOM_GAP			70			///<초기화과정을 보여주는 프로그래시브바의 하단으로부터의 거리
#define		PRG_INIT_LEFT_GAP			22			///<초기화과정을 보여주는 프로그래시브바의 좌측으로부터의 거리
#define		PRG_INIT_RIGHT_GAP			22			///<초기화과정을 보여주는 프로그래시브바의 우측으로부터의 거리 

#define		PRG_FTP_HEIGHT				20											///<FTP과정을 보여주는 프로그래시브바 높이
#define		PRG_FTP_TOP_GAP				(PRG_INIT_TOP_GAP + PRG_INIT_HEIGHT + 10)	///<FTP과정을 보여주는 프로그래시브바의 상단으로부터의 거리
#define		PRG_FTP_BOTTOM_GAP			46											///<FTP과정을 보여주는 프로그래시브바의 하단으로부터의 거리
#define		PRG_FTP_LEFT_GAP			22											///<FTP과정을 보여주는 프로그래시브바의 좌측으로부터의 거리
#define		PRG_FTP_RIGHT_GAP			22											///<FTP과정을 보여주는 프로그래시브바의 우측으로부터의 거리 


int CHost::m_nTemplateX = 0;
int CHost::m_nTemplateY = 0;

double CHost::m_fXScale = 1.0f;
double CHost::m_fYScale = 1.0f;


//TV 레지스터 메시지
#define  TVCONTROL_CHANNEL_UP		1
#define  TVCONTROL_CHANNEL_DOWN		2
#define  TVCONTROL_VOLUME_UP		3
#define  TVCONTROL_VOLUME_DOWN		4
#define	 TVCONTROL_VOLUME_MUTE		5

static UINT msgControlTVModule = ::RegisterWindowMessage(_T("GLOBAL_CONTROL_TV_MODULE"));


typedef struct _ST_EXTRACT_TAR_PARAM
{
	void*				pParentWnd;				///<부모 포인터
	CStringArray		aryTar;					///<Tar 파일명
	bool				bDelete;				///<Tar 해제후에 파일을 지울지 여부
} ST_EXTRACT_TAR_PARAM;


IMPLEMENT_DYNAMIC(CBackWnd, CWnd)

CBackWnd::CBackWnd()
: m_strLog ("")
, m_pprogressInit(NULL)
, m_pprogressDownload(NULL)
, m_pprogressTar(NULL)
, m_nPos(0)
, m_bLogoMode(false)
, m_nBeforeMode(E_INIT_UI)
{ 
	CString strCustomer = GetCoutomerInfo();
	if(strCustomer == CUSTOMER_NARSHA)
	{
		HBITMAP hBitmapTop = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_LOADING_NARSHA_BG));
		m_bgImage.CreateFromHBITMAP(hBitmapTop);
	}
	else
	{
		HBITMAP hBitmapTop = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_LOADING_BG));
		m_bgImage.CreateFromHBITMAP(hBitmapTop);
	}//if
}

CBackWnd::~CBackWnd()
{
	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if

	if(m_pprogressInit)
	{
		delete m_pprogressInit;
		m_pprogressInit = NULL;
	}//if

	if(m_pprogressDownload)
	{
		delete m_pprogressDownload;
		m_pprogressDownload = NULL;
	}//if

	if(m_pprogressTar)
	{
		delete m_pprogressTar;
		m_pprogressTar = NULL;
	}//if
}

BEGIN_MESSAGE_MAP(CBackWnd, CWnd)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_CREATE()
	ON_WM_MOVE()
	ON_WM_SIZE()
	ON_MESSAGE(WM_SET_POS, OnSetPos)
	ON_MESSAGE(WM_SET_LOG, OnAddLog)
	ON_MESSAGE(WM_SET_ERROR_MODE, OnSetErrorMode)
END_MESSAGE_MAP()

int CBackWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	m_pprogressInit = new CProgressCtrlEx;
	m_pprogressInit->Create(WS_CHILD, CRect(0, 0, 0, 0), this, 1234);
	m_pprogressInit->SetBkColor(RGB(32, 34, 36));
	m_pprogressInit->SetForeColor(RGB(26, 80, 184));

	m_pprogressInit->SetTextBkColor(RGB(255, 255, 255));		//프로그래시브바 위에 글자색
	//m_pprogressInit->SetTextColor(RGB(0, 0, 255));
	m_pprogressInit->SetTextForeColor(RGB(26, 80, 184));		//바탕의 글자색

	m_pprogressInit->SetStyle(PROGRESS_TEXT);
	
	m_pprogressInit->SetRange(0, PRG_CNT_MAX);
	m_pprogressInit->SetPos(m_nPos);
	m_pprogressInit->SetText(m_strLog);
	m_pprogressInit->ShowWindow(SW_SHOW);

	return 0;
}

void CBackWnd::OnPaint()
{
	CPaintDC dc(this);
	CMemDC memDC(&dc);

	if(m_bgImage.IsValid())
	{
		CRect rect;
		GetClientRect(rect);

		if(!m_bLogoMode)
		{
			m_bgImage.Draw2(memDC.m_hDC, rect);
		}
		else
		{
			memDC.FillSolidRect(rect, RGB(0, 0, 0));	//바탕은 검정으로

			//로고의 이미지를 확대하지 않고 중앙에 표시하도록 한다
			int nLogoWidth, nLogoHeight, nPosX, nPosY;
			if(rect.Width() > m_bgImage.GetWidth())
			{
				nLogoWidth = m_bgImage.GetWidth();
				nPosX = (rect.Width() - m_bgImage.GetWidth())/2;
			}
			else
			{
				nLogoWidth = rect.Width();
				nPosX = 0;
			}

			if(rect.Height() > m_bgImage.GetHeight())
			{
				nLogoHeight = m_bgImage.GetHeight();
				nPosY = (rect.Height() - m_bgImage.GetHeight())/2;
			}
			else
			{
				nLogoHeight = rect.Height();
				nPosY = 0;
			}//if

			m_bgImage.Draw2(memDC.m_hDC, CRect(nPosX, nPosY, nPosX+nLogoWidth, nPosY+nLogoHeight));
		}//if
	}//if	
}


void CBackWnd::OnMove(int x, int y)
{
	CWnd::OnMove(x, y);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	CRect rectClient;
			
	WINDOWPLACEMENT stWndPlc;
	memset(&stWndPlc, 0x00, sizeof(WINDOWPLACEMENT));
	::GetWindowPlacement(this->GetSafeHwnd(), &stWndPlc);
	rectClient = stWndPlc.rcNormalPosition;
	
	int nPosX, nPosY, nCx, nCy;
	nPosX	= rectClient.left + PRG_INIT_LEFT_GAP + DLG_FRM_SIZE;
	nPosY	= rectClient.top + (rectClient.Height() - (PRG_INIT_BOTTOM_GAP + DLG_FRM_SIZE));
	nCx		= (rectClient.right - (PRG_INIT_RIGHT_GAP + DLG_FRM_SIZE)) - nPosX;
	nCy		= PRG_INIT_HEIGHT;

	m_pprogressInit->MoveWindow(nPosX-3, nPosY-3, nCx, nCy, TRUE);

	if(m_pprogressDownload)
	{
		nPosX	= rectClient.left + PRG_FTP_LEFT_GAP + DLG_FRM_SIZE;
		nPosY	= rectClient.top + (rectClient.Height() - (PRG_FTP_BOTTOM_GAP + DLG_FRM_SIZE));
		nCx		= (rectClient.right - (PRG_FTP_RIGHT_GAP + DLG_FRM_SIZE)) - nPosX;
		nCy		= PRG_FTP_HEIGHT;

		m_pprogressDownload->MoveWindow(nPosX-3, nPosY-3, nCx, nCy);
	}//if

	if(m_pprogressTar)
	{
		nPosX	= rectClient.left + PRG_FTP_LEFT_GAP + DLG_FRM_SIZE;
		nPosY	= rectClient.top + (rectClient.Height() - (PRG_FTP_BOTTOM_GAP + DLG_FRM_SIZE));
		nCx		= (rectClient.right - (PRG_FTP_RIGHT_GAP + DLG_FRM_SIZE)) - nPosX;
		nCy		= PRG_FTP_HEIGHT;

		m_pprogressTar->MoveWindow(nPosX-3, nPosY-3, nCx, nCy);
	}//if
}

void CBackWnd::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	CRect rectClient;
			
	WINDOWPLACEMENT stWndPlc;
	memset(&stWndPlc, 0x00, sizeof(WINDOWPLACEMENT));
	::GetWindowPlacement(this->GetSafeHwnd(), &stWndPlc);
	rectClient = stWndPlc.rcNormalPosition;
	
	int nPosX, nPosY, nCx, nCy;
	nPosX	= rectClient.left + PRG_INIT_LEFT_GAP + DLG_FRM_SIZE;
	nPosY	= rectClient.top + (rectClient.Height() - (PRG_INIT_BOTTOM_GAP + DLG_FRM_SIZE));
	nCx		= (rectClient.right - (PRG_INIT_RIGHT_GAP + DLG_FRM_SIZE)) - nPosX;
	nCy		= PRG_INIT_HEIGHT;

	m_pprogressInit->MoveWindow(nPosX-3, nPosY-3, nCx, nCy, TRUE);

	if(m_pprogressDownload)
	{
		nPosX	= rectClient.left + PRG_FTP_LEFT_GAP + DLG_FRM_SIZE;
		nPosY	= rectClient.top + (rectClient.Height() - (PRG_FTP_BOTTOM_GAP + DLG_FRM_SIZE));
		nCx		= (rectClient.right - (PRG_FTP_RIGHT_GAP + DLG_FRM_SIZE)) - nPosX;
		nCy		= PRG_FTP_HEIGHT;

		m_pprogressDownload->MoveWindow(nPosX-3, nPosY-3, nCx, nCy);
	}//if

	if(m_pprogressTar)
	{
		nPosX	= rectClient.left + PRG_FTP_LEFT_GAP + DLG_FRM_SIZE;
		nPosY	= rectClient.top + (rectClient.Height() - (PRG_FTP_BOTTOM_GAP + DLG_FRM_SIZE));
		nCx		= (rectClient.right - (PRG_FTP_RIGHT_GAP + DLG_FRM_SIZE)) - nPosX;
		nCy		= PRG_FTP_HEIGHT;

		m_pprogressTar->MoveWindow(nPosX-3, nPosY-3, nCx, nCy);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Porgrerss bar의 posotion을 조정한다 \n
/// @param (int) nMode : (in) 초기화 모드 구분
/// @param (int) nPos : (in) 더하려는 프로그래시브 바 값
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::SetPos(int nMode, int nPos)
{
	if(nMode < E_INIT_UI)
	{
		return;
	}//if

	//모드가 새로운 모드로 바뀌었다면
	//이전 모드 값까지 프로그래시바를 채워준다
	if(nMode != m_nBeforeMode)
	{
		m_nPos = m_nModeValue[nMode-1];
		m_pprogressInit->SetPos(m_nPos);

		m_nBeforeMode = nMode;
	}//if

	m_nPos += nPos;

	//모드에서 갖을 수있는 값 이상이라면
	//값을 더하지 않는다
	if(m_nPos >= m_nModeValue[nMode])
	{
		m_nPos = m_nModeValue[nMode];
	}//if

	if(m_nPos >= PRG_CNT_MAX-1)
	{
		m_nPos = PRG_CNT_MAX;
	}//if

	m_pprogressInit->SetPos(m_nPos);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Log를 추가한다 \n
/// @param (LPCSTR) lpszLog : (in) 추가할 log 문자열
/// @param (bool) bNextNewLine : (in) newline을 하는지 여부
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::AddLog(LPCSTR lpszLog, bool bNextNewLine)
{
	if(bNextNewLine)
	{
		m_strLog.Append(lpszLog);
	}
	else
	{
		m_strLog = lpszLog;
	}//if

	m_pprogressInit->SetText(m_strLog);
	m_pprogressInit->Invalidate();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 장애 모드 표시 \n
/// @param (CString) strMsg : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::SetErrorMode(CString strMsg)
{
	ClearLog();

	if(!m_bLogoMode)
	{
		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if

		HBITMAP hBitmapTop = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_LOADING_ERROR));
		m_bgImage.CreateFromHBITMAP(hBitmapTop);
	}//if

	AddLog(strMsg);

	Invalidate();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 밴더별 logo 이미지를 설정 \n
/// @param (CString) strLogoPath : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::SetLogoImage(CString strLogoPath)
{
	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if

	m_bLogoMode = true;

	CString strExt = FindExtension(strLogoPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	m_bgImage.Load(strLogoPath, nImgType);
}


void CBackWnd::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnMouseMove(nFlags, point);
	//::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
	::AfxGetMainWnd()->PostMessage(WM_MOUSEMOVE, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Log를 삭제한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::ClearLog(void)
{
	m_strLog = _T("");
	m_nBeforeMode = E_INIT_UI;
	m_nPos = 0;

	m_pprogressInit->SetPos(m_nPos);
	m_pprogressInit->SetText("");
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Porgrerss bar의 posotion을 조정 메시지 \n
/// @param (WPARAM) wParam : (in) 초기화 모드 구분 값
/// @param (LPARAM) lParam : (in) 더하려는 프로그래시브 바 위치 값
/// @return <형: LRESULT> \n
///			<0: > \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CBackWnd::OnSetPos(WPARAM wParam, LPARAM lParam)
{
	SetPos((int)wParam, (int)lParam);
	
	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Log를 추가 메시지 \n
/// @param (WPARAM) wParam : (in) 설정하려는 문자열 포인터(LPCSTR)
/// @param (LPARAM) lParam : (in) New line 여부
/// @return <형: LRESULT> \n
///			<0: > \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CBackWnd::OnAddLog(WPARAM wParam, LPARAM lParam)
{
	AddLog((LPCSTR)wParam, (bool)lParam);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Log를 추가 메시지 \n
/// @param (WPARAM) wParam : (in) 설정하려는 문자열 포인터(LPCSTR)
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<0: > \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CBackWnd::OnSetErrorMode(WPARAM wParam, LPARAM lParam)
{
	SetErrorMode((LPCSTR)wParam);

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 초기화 \n
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::InitDownload()
{
	m_pprogressDownload = new CProgressCtrlEx;
	m_pprogressDownload->Create(WS_CHILD, CRect(0, 0, 0, 0), this, 4567);
	m_pprogressDownload->SetStyle(PROGRESS_TEXT);
	m_pprogressDownload->SetBkColor(RGB(32, 34, 36));
	m_pprogressDownload->SetForeColor(RGB(31, 200, 248));
	m_pprogressDownload->SetTextBkColor(RGB(255, 255, 255));			//프로그래시브바 위에 글자색
	//m_pbarTotal.SetTextColor(RGB(0, 0, 255));
	m_pprogressDownload->SetTextForeColor(RGB(31, 200, 248));		//바탕의 글자색
	m_pprogressDownload->SetRange(0, 1000);
	m_pprogressDownload->SetPos(0);
	m_pprogressDownload->SetText("");

	CRect rectClient;

	WINDOWPLACEMENT stWndPlc;
	memset(&stWndPlc, 0x00, sizeof(WINDOWPLACEMENT));
	::GetWindowPlacement(this->GetSafeHwnd(), &stWndPlc);
	rectClient = stWndPlc.rcNormalPosition;

	//GetClientRect(&rectClient);
	int nPosX, nPosY, nCx, nCy;
	nPosX	= rectClient.left + PRG_FTP_LEFT_GAP + DLG_FRM_SIZE;
	nPosY	= rectClient.top + (rectClient.Height() - (PRG_FTP_BOTTOM_GAP + DLG_FRM_SIZE));
	nCx		= (rectClient.right - (PRG_FTP_RIGHT_GAP + DLG_FRM_SIZE)) - nPosX;
	nCy		= PRG_FTP_HEIGHT;

	m_pprogressDownload->MoveWindow(nPosX-3, nPosY-3, nCx, nCy);
	m_pprogressDownload->ShowWindow(SW_SHOW);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 종료 \n
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::EndDownload()
{
	__DEBUG__("EndDownload()", _NULL);
	if(m_pprogressDownload)
	{
	__DEBUG__("EndDownload() start", _NULL);
		m_pprogressDownload->ShowWindow(SW_HIDE);
		delete m_pprogressDownload;
		m_pprogressDownload = NULL;
	__DEBUG__("EndDownload() end", _NULL);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 진행상태를 설정한다. \n
/// @param (int) nProgress : (in) 백분욜 진행율
/// @param (CString) strMsg : (in) 메시지
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::SetDownloadProgress(int nProgress, CString strMsg)
{
	if(m_pprogressDownload)
	{
		if(nProgress != 0)
		{
			m_pprogressDownload->SetPos(nProgress);
		}//if
		m_pprogressDownload->SetText(strMsg);
		m_pprogressDownload->Invalidate();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Tar 풀기 초기화 \n
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::InitTarExtract()
{
	m_pprogressTar = new CProgressCtrlEx;
	m_pprogressTar->Create(WS_CHILD, CRect(0, 0, 0, 0), this, 9876);
	m_pprogressTar->SetStyle(PROGRESS_TEXT);
	m_pprogressTar->SetBkColor(RGB(32, 34, 36));
	m_pprogressTar->SetForeColor(RGB(31, 200, 248));
	m_pprogressTar->SetTextBkColor(RGB(255, 255, 255));			//프로그래시브바 위에 글자색
	//m_pbarTotal.SetTextColor(RGB(0, 0, 255));
	m_pprogressTar->SetTextForeColor(RGB(31, 200, 248));		//바탕의 글자색
	m_pprogressTar->SetRange(0, 100);
	m_pprogressTar->SetPos(0);
	m_pprogressTar->SetText("");

	CRect rectClient;

	WINDOWPLACEMENT stWndPlc;
	memset(&stWndPlc, 0x00, sizeof(WINDOWPLACEMENT));
	::GetWindowPlacement(this->GetSafeHwnd(), &stWndPlc);
	rectClient = stWndPlc.rcNormalPosition;

	//GetClientRect(&rectClient);
	int nPosX, nPosY, nCx, nCy;
	nPosX	= rectClient.left + PRG_FTP_LEFT_GAP + DLG_FRM_SIZE;
	nPosY	= rectClient.top + (rectClient.Height() - (PRG_FTP_BOTTOM_GAP + DLG_FRM_SIZE));
	nCx		= (rectClient.right - (PRG_FTP_RIGHT_GAP + DLG_FRM_SIZE)) - nPosX;
	nCy		= PRG_FTP_HEIGHT;

	m_pprogressTar->MoveWindow(nPosX-3, nPosY-3, nCx, nCy);
	m_pprogressTar->ShowWindow(SW_SHOW);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Tar 풀기 종료 \n
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::EndTarExtract()
{
	if(m_pprogressTar)
	{
		m_pprogressTar->ShowWindow(SW_HIDE);
		delete m_pprogressTar;
		m_pprogressTar = NULL;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Tar 풀기 진행 상태를 설정한다. \n
/// @param (int) nProgress : (in/out) 설명
/// @param (CString) strMsg : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CBackWnd::SetTarExtractProgress(int nProgress, CString strMsg)
{
	if(m_pprogressTar)
	{
		m_pprogressTar->SetPos(nProgress);
		CString strText, strTmp;
		int nIdx = strMsg.ReverseFind('\\');
		strText.Format("Extract [%s] files", strMsg.Right(strMsg.GetLength() - nIdx -1));
		m_pprogressTar->SetText(strText);
		m_pprogressTar->Invalidate();
	}//if
}









////////////////////////////////////////////////////////////////////////////////////////
// CHost 대화 상자
CHost::CHost(CWnd* pParent /*=NULL*/)
:	CDialog(CHost::IDD, pParent)
,	m_pCurrentTemplate (NULL)
,	m_bInitialize (false)
,	m_soundVolume (0)
,	m_defaultTemplate ("00")
,	m_adminState (ciFalse)
,	m_operationalState (ciFalse)
,	m_strArgTemplateID(_T(""))
,	m_strDescription("")
,	m_strNetworkUse("1")
,	m_strSite("")
,	m_templatePlayList("")
,	m_strLastUpdateId("")
,	m_bFullScreen (false)
,	m_bTopMostMode (true)
,	m_bViewMouse (true)
,	m_strErrorMessage ("")
,	m_interface(this)
,	m_bLoadFromLocal(false)
,	m_unPlayingTemplateIndex(0)
,	m_bPauseTemplate(false)
,	m_nPosX(0)
,	m_nPosY(0)
,	m_nCx(1024)
,	m_nCy(960)
,	m_strEdition("")
,	m_strVersion("")
,	m_strComputerName("")
,	m_strRegstrationKey("")
,	m_nMonitorIndex(0)
,	m_bSound(true)
,	m_bLogoMode(false)
,	m_strTerminateApp("")
,	m_pdlgSelectFrame(NULL)
,	m_pdlgMenuBar(NULL)
,	m_bShowMenuButton(true)
,	m_bVerify(false)
,	m_pdlgAnnounce(NULL)
,	m_pdlgAnnounce_image(NULL)
,	m_pdlgAnnounce_video(NULL)
,	m_pdlgAnnounce_flash(NULL)
,	m_pdlgAnnounce_ppt(NULL)
,	m_pdlgAnnounce_web(NULL)
,	m_pThreadPauseBRW(NULL)
,	m_bExitPauseBRWThread(true)
,	m_hEventThreadPauseBRW(NULL)
,	m_dwMouseStart(0)
,	m_hHookDll(NULL)
,	m_tmPausTime(0)
,	m_bIsTVPlay(false)
,	m_bIsPlayDummy(false)
,	m_strReturnTemplateId("")
,	m_bClickJump(false)
,	m_dwClickJumpWaitTime(0)
,	m_nStateBRW(E_BRW_INIT)
,	m_bExitNotifyThread(false)
,	m_hEventThreadNotify(NULL)
,	m_nFailCount(0)
,	m_nTotalCount(0)
,	m_bPackageDown(false)
,	m_hEvtThreadIEState(NULL)
,	m_bExitIEStateThread(false)
,	m_nPid(0)
,	m_strStateMsg("")
,	m_hEvtThreadResource(NULL)
,	m_bExitResourceThread(false)
,	m_nDownloadStatus (1)
,	m_useTimeCheck(false)  //skpark same_size_file_problem
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CHost::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CHost, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_WM_HELPINFO()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_WM_SYSCOMMAND()
	ON_WM_GETMINMAXINFO()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_DEBUG_STRING, OnDebugString)
	ON_MESSAGE(WM_DEBUG_GRID, OnDebugGrid)
	ON_MESSAGE(WM_ERROR_MESSAGE, OnErrorMessage)
	ON_MESSAGE(WM_STATE_DOWNLOAD, OnStateDownload)
	ON_MESSAGE(WM_PROGRESS_DOWNLOAD, OnProgressDownload)
	ON_MESSAGE(WM_FAILCOUNT_DOWNLOAD, OnFailCountDownload)
	ON_MESSAGE(ID_PLAY_NEXT_SCHEDULE, OnPlayNextSchedule)
	ON_MESSAGE(WM_CONTENTS_UPDATE, OnContentsUpdate)
	ON_MESSAGE(WM_PLAY_PREV_DEFAULT_TEMPLATE, OnPlayPrevDefaultTemplate)
	ON_MESSAGE(WM_PLAY_NEXT_DEFAULT_TEMPLATE, OnPlayNextDefaultTemplate)
	ON_MESSAGE(WM_PLAY_PREV_TEMPLATE, OnPlayPrevTemplate)
	ON_MESSAGE(WM_PLAY_NEXT_TEMPLATE, OnPlayNextTemplate)
	ON_MESSAGE(WM_HOOK_MOUSE, OnMouseHook)
	ON_MESSAGE(WM_BRW_PAUSE, OnPauseBRW)
	ON_MESSAGE(WM_DEFAULT_TEMPLATE_UPDATE, OnDefaultTemplateChanged)
	ON_MESSAGE(WM_PROGRESS_TAR, OnProgressTar)
	ON_MESSAGE(WM_COMPLETE_TAR, OnCompleteTar)
	ON_MESSAGE(WM_SEND_STOP_BRW, OnSendStop)
	ON_MESSAGE(WM_ENDRUN_IE, OnEndRunIE)
	ON_MESSAGE(WM_USER_ANNOUNCE, OnAnnounceMsg)
END_MESSAGE_MAP()



// CHost 메시지 처리기

void CHost::OnDestroy()
{
	__DEBUG__("OnDestroy", _NULL);

	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

BOOL CHost::OnInitDialog()
{
	//프로그램의 메인 아이콘 변경
	CString strCustomer = GetCoutomerInfo();
	if(strCustomer == CUSTOMER_NARSHA)
	{
		m_hIcon = (HICON)LoadImage(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_NARSHA), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
	}//if

	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	__DEBUG__("SiteID", GetSiteName());
	__DEBUG__("HostID", GetHostName());

	//Notify thread
	m_nPid = _getpid();
	m_strStateMsg = "";
	if(!ciArgParser::getInstance()->isSet("+download_only")
		&& !ciArgParser::getInstance()->isSet("+template"))
	{
		CWinThread* pThread = ::AfxBeginThread(CHost::ThreadNotifyState, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		pThread->m_bAutoDelete = TRUE;
		m_bExitNotifyThread = false;
		m_hEventThreadNotify = CreateEvent(NULL, FALSE, FALSE, NULL);
		pThread->ResumeThread();
	}//if

	if(InitParameter() == false)
	{
		__ERROR__("Fail to initialize parameter !!!", _NULL);
		CDialog::OnOK();
	}//if

	if(!InitUI())
	{
		__ERROR__("Fail to initialize UI !!!", _NULL);
		CDialog::OnOK();
	}//if

#ifdef _FELICA_
	__DEBUG__("FELICA - felicaServerStart", _NULL);
	felicaUtil::getInstance()->felicaServerStart(this->m_hWnd);
#endif

	StartBRW();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CHost::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CHost::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

HBRUSH CHost::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if(pWnd == this)
	{
		return (HBRUSH)m_brushBack;
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

BOOL CHost::OnHelpInfo(HELPINFO* pHelpInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	ViewHelp();

	return TRUE;
//	return CDialog::OnHelpInfo(pHelpInfo);
}


// 초기화
bool CHost::InitParameter()
{
	__DEBUG_HOST_BEGIN__(_NULL)

	//replace
	if(ciArgParser::getInstance()->isSet("+replace"))
	{
		__DEBUG__("+replace mod", _NULL);
		m_bTopMostMode = false;
		m_strTerminateApp = ::AfxGetAppName();
		m_strTerminateApp.Append(".exe");
		m_tmPausTime = 0;

		ciString strPauseTime;
		if(ciArgParser::getInstance()->getArgValue("+replace", strPauseTime))
		{
			if(strPauseTime.length() != 0)
			{
				m_tmPausTime = atol(strPauseTime.c_str());
			}//if
		}//if
		__DEBUG__("Replace waiting time", m_tmPausTime);
	}
	else if(ciArgParser::getInstance()->isSet("+download_only"))
	{
		__DEBUG__("+download_only mod", _NULL);
		//m_bTopMostMode = true;
		m_bTopMostMode = false;
		m_strTerminateApp = "";
		return true;
	}
	else
	{
		m_bTopMostMode = true;
		m_strTerminateApp = "";
	}//if

	if(ciArgParser::getInstance()->isSet("+local"))
	{
		__DEBUG__("Local Mode", _NULL);

		m_bLoadFromLocal = true;
	}
	else
	{
		__DEBUG__("Network Mode", _NULL);

		m_bLoadFromLocal = false;
	}//if


	//크기
	ciString xscale;
	if(ciArgParser::getInstance()->getArgValue("+xscale", xscale))
	{
		if(xscale.length() > 0)
			m_fXScale = atof(xscale.c_str());
		if(m_fXScale == 0.0f)
			m_fXScale = 1.0f;
		else
			m_fXScale /= 100.0f;
		__DEBUG__("XScale", m_fXScale);
	}//if

	ciString yscale;
	if(ciArgParser::getInstance()->getArgValue("+yscale", yscale))
	{
		if(yscale.length() > 0)
			m_fYScale = atof(yscale.c_str());
		if(m_fYScale == 0.0f)
			m_fYScale = 1.0f;
		else
			m_fYScale /= 100.0f;
		__DEBUG__("YScale", m_fYScale);
	}//if

	ciString scale;
	if(ciArgParser::getInstance()->getArgValue("+scale", scale))
	{
		double fScale = 0.0f;
		if(scale.length() > 0)
			fScale = atof(scale.c_str());
		if(fScale == 0.0f)
			fScale = 1.0f;
		else
			fScale /= 100.0f;

		m_fXScale = fScale;
		m_fYScale = fScale;

		__DEBUG__("XYScale", fScale);
	}//if

	//scale
	if( ciArgParser::getInstance()->isSet("+autoscale"))
	{
		__DEBUG__("AutoScale Mode", _NULL);

		m_fXScale = 0.0f;
		m_fYScale = 0.0f;
	}//if

	// template를 x,y를 기준으로 이동
	ciString template_x, template_y;
	if(ciArgParser::getInstance()->getArgValue("+x", template_x))
	{
		if(template_x.length() > 0)
			m_nTemplateX = atoi(template_x.c_str());
		__DEBUG__("Move to X", m_nTemplateX);
	}//if

	if(ciArgParser::getInstance()->getArgValue("+y", template_y))
	{
		if(template_y.length() > 0)
			m_nTemplateY = atoi(template_y.c_str());
		__DEBUG__("Move to Y", m_nTemplateY);
	}//if

	if(ciArgParser::getInstance()->isSet("+xcenter"))
	{
		__DEBUG__("X Center Mode", _NULL);

		m_nTemplateX = INT_MAX;
	}//if

	if(ciArgParser::getInstance()->isSet("+ycenter"))
	{
		__DEBUG__("Y Center Mode", _NULL);

		m_nTemplateY = INT_MAX;
	}//if

	if(ciArgParser::getInstance()->isSet("+center"))
	{
		__DEBUG__("XY Center Mode", _NULL);

		m_nTemplateX = INT_MAX;
		m_nTemplateY = INT_MAX;
	}//if

	//원격 접속으로 동영상 화면을 보기 위해서는 컨텐츠에서 Overlay 믹서를 사용해선 안된다.
	//이를 방지하기 위하여 미리 Overlay 믹서를 하나 열어 다른 컨텐츠에서 사용 못하도록 한다.
	//단, 비디오 모드가 오버레이로 세팅이 되어 있다면 열지 않는다.
	if(GetVidoeOpenType() == E_OPEN_INIT && GetVidoeRenderType() != E_OVERLAY)
	{
		OpenDummyVideo();
	}//if
	
	__DEBUG_HOST_END__(_NULL)

	return true;
}


void CHost::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case TIMER_COURSOR_HIDE:
		{
			DWORD dwNow = GetTickCount();
			if(dwNow - m_dwMouseStart > TIME_COURSOR_HIDE)
			{
				if(m_bViewMouse)
				{
					__DEBUG__("Cursor auto hide", _NULL);
					SetShowCursor(false);
				}//if				
			}//if
		}
		break;

	case BRW_EXIT_ID:
		{
			KillTimer(BRW_EXIT_ID);
			__ERROR__("BRW exit timer", _NULL);
			PostMessage(WM_SEND_STOP_BRW, 0, 0);
		}
		break;

	case HOST_SHOW_ID:
		{
			__DEBUG__("FWV : OnTimer", _NULL);
			__DEBUG__("m_bFullScreen", m_bFullScreen);
			KillTimer(HOST_SHOW_ID);
			
			//무조건 최대화면으로 나오게 한다
			m_bFullScreen = false;
			ToggleFullScreen();
		}
		break;

	case LOAD_HOST_ID:
		KillTimer(LOAD_HOST_ID);
		switch(LoadHost())
		{
		case -1:
			{
				CString msg;
				msg.LoadString(IDS_HOST_STATE_IS_UNUSED);
				OnErrorMessage((WPARAM)(LPCSTR)msg, 0);

				//SetTimer(BRW_EXIT_ID, BRW_EXIT_TIME, NULL);
			}
			break;
		case -2:
			{
				CString msg;
				msg.Format("Fail to load schedule file [%s.ini]", ::GetHostName());
				OnErrorMessage((WPARAM)(LPCSTR)msg, 0);
				if(m_bPackageDown)
				{
					m_strStateMsg = STAT_MSG_PACKAGE_READ;
				}
				else
				{
					m_strStateMsg = STAT_MSG_PACKAGE_DOWNLOAD;
				}//if

				//SetTimer(BRW_EXIT_ID, BRW_EXIT_TIME, NULL);
			}
			break;
		case -3:
			{
				CString msg;
				msg.LoadString(IDS_INVALID_TEMPLATE_PLAY_LIST);
				CString strError;
				strError.Format(_T("%s : %s"), msg, m_templatePlayList.c_str());
				OnErrorMessage((WPARAM)(LPCSTR)strError, 0);
				m_strStateMsg = STAT_MSG_PACKAGE_DATA;
				//SetTimer(BRW_EXIT_ID, BRW_EXIT_TIME, NULL);
			}
			break;
		case -4:
			{
				CString msg;
				msg.Format("Empty playl list in schedule [%s]", ::GetHostName());
				OnErrorMessage((WPARAM)(LPCSTR)msg, 0);
				m_strStateMsg = STAT_MSG_PACKAGE_DATA;
				//SetTimer(BRW_EXIT_ID, BRW_EXIT_TIME, NULL);
			}
			break;
		default:
			m_wndBack.AddLog("ok", true);
			m_wndBack.AddLog("Initializing data... ");
			SetTimer(LOAD_TEMPLATE_ID, LOAD_TEMPLATE_TIME, NULL);
		}
		break;

	case LOAD_TEMPLATE_ID:
		KillTimer(LOAD_TEMPLATE_ID);
		
		if(LoadTemplate())
		{
			m_wndBack.AddLog("ok", true);
			m_wndBack.AddLog("Checking files... ");
			SetTimer(CHECKFILE_ID, CHECKFILE_TIME, NULL);
		}
		else
		{
			CString msg;
			msg.Format("Fail to load template list in schedule [%s]", ::GetHostName());
			OnErrorMessage((WPARAM)(LPCSTR)msg, 0);
			m_strStateMsg = STAT_MSG_PACKAGE_DATA;
			//SetTimer(BRW_EXIT_ID, BRW_EXIT_TIME, NULL);
		}//if
		break;

	case CHECKFILE_ID:
		{
			KillTimer(CHECKFILE_ID);

			// 1. getpid of other-browser
			CString strOtherAppName;
			CString strOtherFilename = GetAppName();
			if(strOtherFilename == "UTV_brwClient2")
			{
				strOtherAppName = "UTV_brwClient2_UBC1";
				strOtherFilename = "UTV_brwClient2_UBC1.exe";
			}
			else
			{
				strOtherAppName = "UTV_brwClient2";
				strOtherFilename = "UTV_brwClient2.exe";
			}
			int pid = scratchUtil::getInstance()->getPid(strOtherFilename);

			if( pid == 0 )
			{
				// 1.1 no other-browser
				//   ==> run normal-process
				__DEBUG__("No Other Browser", strOtherAppName);
				CheckFile();
			}
			else
			{
				// 2. get window-handle of other-browser
				HWND hwnd = scratchUtil::getInstance()->getWHandle(pid);
				if( hwnd == 0 )
				{
					// 2.1 no window-handle of other-browser
					//   ==> re-check after 1-sec
					static int count = 0;
					count++;
					if( count <= 10 )
					{
						__DEBUG__("No Window Handle of Other Browser", strOtherAppName);
						SetTimer(CHECKFILE_ID, 1000, NULL);
						break;
					}
				}

				// 3. get download_status of other-browser
				DWORD dwRet = 0;
				COPYDATASTRUCT appInfo;
				appInfo.dwData = UBC_WM_BRW_GET_DOWNLOAD_STATUS;  // 40000
				appInfo.lpData = NULL;
				appInfo.cbData = 0;
				int status = ::SendMessageTimeout(hwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo, SMTO_ABORTIFHUNG|SMTO_NORMAL, 1000, &dwRet);
				if( status )
				{
					// success ==> get status-value from INI
					status = atoi(GetINIValue("UBCBrowser.ini", strOtherAppName, "Current_Package_Status"));
				}

				__DEBUG__("Status of Other Browser", status);
				if( status == 0 )
				{
					// 3.1 timeout or no-status
					//   ==> re-check after 1-sec
					static int count = 0;
					count++;
					if( count <= 10 )
					{
						SetTimer(CHECKFILE_ID, 1000, NULL);
						break;
					}
				}
				else
				{
					// 4. get download-package-name 
					CString str_current_package = GetINIValue("UBCBrowser.ini", strOtherAppName, "Current_Package_Name");
					__DEBUG__("Current Package of Other Browser", str_current_package);
					__DEBUG__("Current Package of This Browser", ::GetHostName());
					if( stricmp(str_current_package, ::GetHostName()) != 0 )
					{
						// 4.1 different package
						//   ==> run normal-process
						__DEBUG__("Different Package", _NULL);
						CheckFile();
					}
					else
					{
						switch( status )
						{
						case 1: // both is in initializing
							if( stricmp(GetAppName(), "UTV_brwClient2") == 0 )
							{
								// is UTV_brwClient2 (not UTV_brwClient2_UBC1)
								//   ==> run normal-process
								__DEBUG__("Download Package", _NULL);
								CheckFile();
							}
							break;

						case 2: // other-browser is in download => re-check after 1-sec
							__DEBUG__("Download Package on Other Browser", _NULL);
							SetTimer(CHECKFILE_ID, 1000, NULL);
							break;

						case 3: // other-browser is in download-complete => run normal-process without downloading
							__DEBUG__("Complete Package on Other Browser", _NULL);
							SetTimer(OPENFILE_ID, OPENFILE_TIME, NULL);
							break;
						}
					}
				}
			}
		}
		break;

	case  OPENFILE_ID:
		{
			KillTimer(OPENFILE_ID);
			OpenFile();
		}
		break;

	case INIT_HANDLER_ID:
		{
			m_wndBack.SetPos(E_INIT_HANDLER, 10);
			KillTimer(INIT_HANDLER_ID);

			//서버로부터 공지 확인
			if(m_bVerify && (GetEdition() == ENTERPRISE_EDITION))
			{
				//skpark 2013.11.28 new announce
				ReadAllAnnounce();
				//긴급공지 확인
				//ReadAnnounce(2); // ticker  type 을 위해
				//ReadAnnounce(-1);// ticker type 이 아닌것을 위해 한번더
			}//if

			m_wndBack.SetPos(E_INIT_HANDLER, 10);

			if(m_tmPausTime != 0)
			{
				PauseBRW();
			}
			else
			{
				//프로그램 시작전 UI와 Arg적용, etc...
				ProcessStartup();
				__DEBUG__("After ProcessStartup", _NULL);
			}//if
		}
		break;

	case TEMPLATE_CHECK_ID:
		{
			TemplateCheck();
		}
		break;

	case TIMER_CLICK_JUMP:
		{
			KillTimer(TIMER_CLICK_JUMP);
			m_bClickJump = false;
			m_dwClickJumpWaitTime = 0;
			if(m_strReturnTemplateId.GetLength() != 0)
			{
				SetDefaultTemplatePlay(m_strReturnTemplateId);
			}//if

			SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
			if(::GetScheduleResuming())
				SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL); // <!-- 깜박임방지 (2016-08-29) -->
		}
		break;

	case TIMER_ID_CHECK_CATEGORY:
		KillTimer(TIMER_ID_CHECK_CATEGORY);

		if( CheckCategory() )
			SetTimer(TIMER_ID_CHECK_CATEGORY, TIMER_TIME_CHECK_CATEGORY, NULL);
		else
			SetTimer(TIMER_ID_CHECK_CATEGORY, 1000, NULL); // 실패시 1초간격 체크

		break;

	case TIMER_CHECK_UPDATER_ID:
		KillTimer(TIMER_CHECK_UPDATER_ID);

		CheckUpdater();

		SetTimer(TIMER_CHECK_UPDATER_ID, TIMER_CHECK_UPDATER_TIME, NULL);
		break;

	case TIMER_PPT_ALL_OPEN_ID:
		KillTimer(TIMER_PPT_ALL_OPEN_ID);

		if( COfficeApp::IsAllOpen() )
			SetTimer(TIMER_ID_CHECK_CATEGORY, 1000, NULL);
		else
			SetTimer(TIMER_PPT_ALL_OPEN_ID, TIMER_PPT_ALL_OPEN_TIME, NULL);
		break;

	// <!-- 깜박임방지 (2016-08-29)
	case TIMER_SHOWWINDOW_ID:
		// 한번에 ShowWindow/HideWindow 실행하여 깜박임 방지함
		{
			int cnt = m_lsHideWindowHwnd.GetCount();
			for(int i=0; i<cnt; i++)
			{
				HWND hwnd = m_lsHideWindowHwnd.GetAt(i);
				::ShowWindow(hwnd, SW_HIDE);
			}
			m_lsHideWindowHwnd.RemoveAll();

			cnt = m_lsShowWindowHwnd.GetCount();
			for(int i=0; i<cnt; i++)
			{
				HWND hwnd = m_lsShowWindowHwnd.GetAt(i);
				::ShowWindow(hwnd, SW_SHOW);
			}
			m_lsShowWindowHwnd.RemoveAll();

		}
		break;
	// -->

	// <!-- 라이센스 체크 딜레이 (2016-11-14)
	case TIMER_LICENSE_CHECK_ID:
		KillTimer(TIMER_LICENSE_CHECK_ID);
		((CBRW2App*)::AfxGetApp())->ProtectCopy(m_strEdition, m_strVersion, m_strComputerName, m_strRegstrationKey, m_bVerify);
		__DEBUG__("TIMER_LICENSE_CHECK_ID", m_bVerify);
		if(!m_bVerify)
		{
			//인증이 안되었을 때...
#ifndef _DEBUG
			MakeTrialAnnounce();

			if(m_pdlgAnnounce)
			{
				if(!m_pdlgAnnounce->IsPlay())
				{
					__DEBUG__("Start announce play", _NULL);
					m_pdlgAnnounce->StartAnnouncePlay();
				}//if
			}//if
#endif
		}//if
		break;
	// -->

	}//switch
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// template play list를 순환하며 재생할 template를 결정한다. \n
/// @param (bool) bForward : (in) schedule의 검색 방향(true : index가 증가하는 방향, false : index 가 감소하는 방향)
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::CheckSchedule(bool bForward)
{
	// schedule check
	if(_checkSchedule(bForward))
	{
		return true;
	}

	// default schedule check
	if(_checkDefaultSchedule(bForward))
	{
		return true;
	}

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// template play list를 순환하며 time schedule을 재생할 template를 결정한다. \n
/// @param (bool) bForward : (in) schedule의 검색 방향(true : index가 증가하는 방향, false : index 가 감소하는 방향)
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::_checkSchedule(bool bForward)
{	
	int nCountArray = GetCountOfTemplatePlayAry();
	if(nCountArray == 0)
	{
		return false;
	}//if

	int nSeed;
	if(bForward)
	{
		nSeed = 1;
	}
	else
	{
		nSeed = -1;
	}//if

	//Template play list에서 template을 가져와서 변경하는 template를 비교한다
	int nNewIndex = GetPlayingTemplateIndex();
	CTemplatePlay* pclsTemplatePlay = NULL;
	do
	{
		if(bForward && nNewIndex == nCountArray)
		{
			nNewIndex = -1;
			continue;
		}//if

		if(!bForward && nNewIndex == -1)
		{
			nNewIndex = nCountArray;
			continue;
		}//if
			
		pclsTemplatePlay = GetAtTemplatePlay(nNewIndex);
		if((pclsTemplatePlay == NULL) || (pclsTemplatePlay->m_pclsTemplate == NULL))
		{
			continue;
		}//if

		if(pclsTemplatePlay->m_pclsTemplate->IsBetween(false))
		{
			if(m_pCurrentTemplate != pclsTemplatePlay->m_pclsTemplate)
			{
				CTemplate* pclsOldTemplate = NULL;
				if(m_pCurrentTemplate)
				{
					pclsOldTemplate = m_pCurrentTemplate;
				}//if

				m_pCurrentTemplate = pclsTemplatePlay->m_pclsTemplate;
				if(pclsOldTemplate && (pclsOldTemplate != m_pCurrentTemplate))
				{
					if(!::GetScheduleResuming())
						pclsOldTemplate->PostMessage(WM_HIDE_CONTENTS, 0, 0);	// <!-- 깜박임방지 (2016-08-29) -->
					pclsOldTemplate->Stop();
				}//if

				if(m_pCurrentTemplate->IsPlay())
				{
					m_pCurrentTemplate->Stop();
				}//if

				m_pCurrentTemplate->Play(pclsTemplatePlay);
			}
			else if(!m_pCurrentTemplate->IsPlay())
			{
				m_pCurrentTemplate->Play(pclsTemplatePlay);
			}//if//if

			if(nNewIndex != GetPlayingTemplateIndex())
			{
				SetDefaultTemplatePlay(nNewIndex);
			}//if
			return true;
		}//if
	} while((nNewIndex = nNewIndex+nSeed) != GetPlayingTemplateIndex()); //while

	return false;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// template play list를 순환하며 default schedule을 재생할 template를 결정한다. \n
/// @param (bool) bForward : (in) schedule의 검색 방향(true : index가 증가하는 방향, false : index 가 감소하는 방향)
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::_checkDefaultSchedule(bool bForward)
{
	int nCountArray = GetCountOfTemplatePlayAry();
	if(nCountArray == 0)
	{
		return false;
	}//if

	int nSeed;
	if(bForward)
	{
		nSeed = 1;
	}
	else
	{
		nSeed = -1;
	}//if

	//Template play list에서 template을 가져와서 변경하는 template를 비교한다
	int nNewIndex = GetPlayingTemplateIndex();
	CTemplatePlay* pclsTemplatePlay = NULL;
	do
	{
		if(bForward && nNewIndex == nCountArray)
		{
			nNewIndex = -1;
			continue;
		}//if

		if(!bForward && nNewIndex == -1)
		{
			nNewIndex = nCountArray;
			continue;
		}//if

		pclsTemplatePlay = GetAtTemplatePlay(nNewIndex);
		if((pclsTemplatePlay == NULL) || (pclsTemplatePlay->m_pclsTemplate == NULL))
		{
			continue;
		}//if

		if(pclsTemplatePlay->m_pclsTemplate->IsBetween(true))
		{
			if(m_pCurrentTemplate != pclsTemplatePlay->m_pclsTemplate)
			{
				CTemplate* pclsOldTemplate = NULL;
				if(m_pCurrentTemplate)
				{
					pclsOldTemplate = m_pCurrentTemplate;
				}//if

				m_pCurrentTemplate = pclsTemplatePlay->m_pclsTemplate;
				if(pclsOldTemplate && (pclsOldTemplate != m_pCurrentTemplate))
				{
					if(!::GetScheduleResuming())
						pclsOldTemplate->PostMessage(WM_HIDE_CONTENTS, 0, 0);	// <!-- 깜박임방지 (2016-08-29) -->
					pclsOldTemplate->Stop();
				}//if

				if(m_pCurrentTemplate->IsPlay())
				{
					m_pCurrentTemplate->Stop();
				}//if

				m_pCurrentTemplate->Play(pclsTemplatePlay);
			}
			else if(!m_pCurrentTemplate->IsPlay())
			{
				m_pCurrentTemplate->Play(pclsTemplatePlay);
			}//if

			if(nNewIndex != GetPlayingTemplateIndex())
			{
				SetDefaultTemplatePlay(nNewIndex);
			}//if
			return true;
		}//if
	} while((nNewIndex = nNewIndex+nSeed) != GetPlayingTemplateIndex()); //while

	return false;
}

bool CHost::CheckFile()
{
	m_nStateBRW = E_BRW_DOWNLOADING;			//다운로드 시작
	//ftp 모듈의 패시브 모드 설정에 따라서 사설 아이피를 사용하는 경우에
	//ftp 검색이 안되는 경우가 있으므로
	//+local 옵션이 있는 경우에는 파일 체크를 하지 않도록 처리함
	if(ciArgParser::getInstance()->isSet("+local"))
	{
		m_wndBack.SetPos(E_INIT_CHECK_FILE, 100);
		PostMessage(WM_STATE_DOWNLOAD, E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);
		return true;
	}//if

	m_wndBack.AddLog("Downloading files... ");
	m_wndBack.InitDownload();
	m_libDownloader.SetProgressNotifyWnd(this->m_hWnd);
	//skpark same_size_file_problem 
	if(ciArgParser::getInstance()->isSet("+use_time_check") || m_useTimeCheck == true){ //skpark same_size_file_problem 
		// 이경우, file time 을 비교 하지 않으므로 더 빠르게 다운로드 된다.
		m_libDownloader.SetCheckFileTime(true);
	}

	//skpark 2014.02.17 // download_only mode 라면 
	if(ciArgParser::getInstance()->isSet("+download_only")){
		// skpark 2014.03.07 3rdparty player 가 존재하면 죽여준다.
		Stop3rdPartyPlayer();
	}

	try
	{
		__DEBUG__("Begin download playcontents file", _NULL);
		int nRet = m_libDownloader.DownloadPlayContents(GetProgramSite(), ::GetHostName(), ::GetBRWId());
		if(nRet != E_DNRET_FAIL_MISSING_FILE)
		{
			m_nDownloadStatus = 2;
			__DEBUG__("Success on request download playcontents files", ::GetHostName());
			return true;
		}//if
	}
	catch(CException& ex)
	{
		char szCause[255];
		memset(&szCause, 0x00, 255);
		ex.GetErrorMessage(szCause, 255);
		__DEBUG__("Exception : DownloadPlayContents()", szCause);
		return false;
	}//try

	__DEBUG__("Fail to load package file", ::GetHostName());
	m_wndBack.SetPos(E_INIT_CHECK_FILE, 100);
	// skpark 2012.12.14 실패했는데도, 성공으로 날리고 있다.
	// 실패한 경우, 실패한 것으로 바로 잡는다.
	//PostMessage(WM_STATE_DOWNLOAD, E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);
	PostMessage(WM_STATE_DOWNLOAD, E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_FAIL);
	return false;
}

bool CHost::OpenFile()
{
	__DEBUG_HOST_BEGIN__(_NULL)

	//PRG_CNT_OPEN_FILE 숫자만큼의 진행상황을 나누어 표시한다
	int nCount = (int)GetCountOfTemplatePlayAry();
	if(nCount == 0)
	{
		//Progress set
		m_wndBack.SetPos(E_INIT_OPEN_FILE, 300);
	}
	else
	{
		//int nFacter = PRG_CNT_OPEN_FILE/nCount;
		int nFacter = m_wndBack.m_nModeValue[E_INIT_OPEN_FILE]/nCount;
		if(nFacter == 0)
		{
			nFacter = 1;
		}//if

		CTemplatePlay* pclsPlay = NULL;
		CTemplate* pclsTemplate = NULL;
		CTemplateMap mapTemplate;		//한번 open한 template을 중복되서 open하지 않도록...
		for(int i=0; i<nCount; i++)
		{
			//Progress set
			m_wndBack.SetPos(E_INIT_OPEN_FILE, nFacter);

			//pclsPlay = (CTemplatePlay*)m_aryTemplatePlay.GetAt(i);
			pclsPlay = GetAtTemplatePlay(i);
			if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL)
			{
				continue;
			}//if

			if(!mapTemplate.Lookup(pclsPlay->m_strTemplateID, (void*&)pclsTemplate))
			{
				pclsPlay->m_pclsTemplate->OpenFile();
				mapTemplate.SetAt(pclsPlay->m_strTemplateID, pclsPlay->m_pclsTemplate);
			}//if
		}//for
		mapTemplate.RemoveAll();
	}//if

	m_wndBack.AddLog("ok", true);
	//Tar 파일 확인
	CheckTarFile();
	/*
	m_wndBack.AddLog("ok", true);
	m_wndBack.AddLog("Registering event-receiver... ");
	SetTimer(INIT_HANDLER_ID, INIT_HANDLER_TIME, NULL);
	*/

	// 단말 카테코리(컨텐츠 카테고리 검사용) 검사
//	SetTimer(TIMER_ID_CHECK_CATEGORY, 1000, NULL);
	SetTimer(TIMER_PPT_ALL_OPEN_ID, TIMER_PPT_ALL_OPEN_TIME, NULL);

	__DEBUG_HOST_END__(_NULL)

	return true;
}


LRESULT CHost::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	// 팝업 방지
	if(message == WM_NCACTIVATE)
	{
		wParam = 0;
		lParam = 0;
	}
	else if(message == WM_ACTIVATE)
	{
		wParam = 0;
		lParam = 0;
	}
	/*
	else if(message == WM_DEVICECHANGE && wParam == DBT_DEVICEREMOVECOMPLETE)
	{
		__DEBUG__("+++++++++++++++++++++++++++", _NULL);
		__DEBUG__("Device changed", _NULL);
		__DEBUG__("+++++++++++++++++++++++++++", _NULL);
	}//if
	*/
	return CDialog::WindowProc(message, wParam, lParam);
}


BOOL CHost::PreTranslateMessage(MSG* pMsg)
{
	// 펠리카 접촉 콜백 등록
#ifdef _FELICA_
	if(felicaUtil::getInstance()->isFelicaMsg(pMsg))
	{
		if(!felicaUtil::getInstance()->preTranslateMessage(pMsg))
		{
			//__DEBUG__("fail - preTranslateMessage", _NULL);
			//TRACE("fail - preTranslateMessage\r\n");
		}//if
	}//if
#endif

	if(pMsg->message == WM_RBUTTONUP)
	{
		__DEBUG__("CHost::R-Button", _NULL);
	}//if

	if((pMsg->message == WM_KEYDOWN))
	{
		if(pMsg->wParam == VK_ESCAPE)
		{
			__DEBUG__("Escape", _NULL);
			PostMessage(WM_SEND_STOP_BRW, 0, 0);
			return TRUE;
		}
		else if(pMsg->wParam == VK_RETURN)
		{
			return TRUE;
		}//if
	}//if

	if(pMsg->message == WM_SYSKEYDOWN && (pMsg->wParam != 18))		//ALT key 처리(단일 ALT key 만 눌렸을 때 pMsg->wParam == 18 이 온다)
	{
		CTemplatePlay* pclsPlay = NULL;
		for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
		{
			pclsPlay = GetAtTemplatePlay(unIdx);
			if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL)
			{
				continue;
			}//if

			//각 template이 ALT+# key로 shortcut key가 설정되어 있는지 검사한다
			if((pclsPlay->m_pclsTemplate->GetSysShortcutKey() == VK_MENU)
				&& (pMsg->wParam == pclsPlay->m_pclsTemplate->GetShortcutKey()))
			{
				//지금 설정된 defaultTemplate와 같다면 변경하지 않는다.
				//if(m_defaultTemplate != pclsPlay->m_pclsTemplate->GetTemplateId())
				//{
					SetDefaultTemplatePlay(pclsPlay->m_pclsTemplate->GetTemplateId());
				//}//if
				break;
			}//if
		}//for

		return CDialog::PreTranslateMessage(pMsg);
		//return TRUE;
	}//if

	// 팝업 방지
	if( pMsg->message == WM_NCACTIVATE )
	{
		pMsg->wParam = 0;
		pMsg->lParam = 0;
	}
	else if( pMsg->message == WM_ACTIVATE )
	{
		pMsg->wParam = 0;
		pMsg->lParam = 0;
	}
	else if(pMsg->message == WM_KEYDOWN)
	{
		__DEBUG__("WM_KEYDOWN", (unsigned int)pMsg->wParam);

		if(IsCTRLpressed() && (pMsg->wParam != 17))		//CTRL key 처리(단일 CTLR key 만 눌렸을 때 pMsg->wParam == 17 이 온다)
		{
			ProcessCTRLKey(pMsg);
			//return TRUE;
			return CDialog::PreTranslateMessage(pMsg);
		}//if

		switch(pMsg->wParam)
		{
		/*
		case VK_ESCAPE:
			SendStopBRW();
			return TRUE;
		*/
		case VK_PRIOR: // page up
			if(stricmp(GetCoutomerInfo(),"UBGOLF")==0)
			{
				__WARNING__("UBGOLF must skip WM_KEYDOWN(VK_PRIOR) !!!", _NULL);
			}
			else
			{
				OnPlayPrevTemplate(0,0);
			}
			return TRUE;

		case VK_NEXT: // page down
			if(stricmp(GetCoutomerInfo(),"UBGOLF")==0)
			{
				__WARNING__("UBGOLF must skip WM_KEYDOWN(VK_NEXT) !!!", _NULL);
			}
			else
			{
				OnPlayNextTemplate(0,0);
			}
			return TRUE;

		case VK_F2:
			{
				//CLogDialog::getInstance(this)->ShowWindow(SW_SHOW);
				CLogDialog* pLogDlg = CLogDialog::getInstance(this);
				if(pLogDlg->IsWindowVisible())
				{
					pLogDlg->ShowWindow(SW_HIDE);
				}
				else
				{
					pLogDlg->ShowWindow(SW_SHOW);
				}//if
			}
			return TRUE;

		case VK_F3:
			FrameModify();
			return TRUE;

		case VK_F7:
			ViewLicense();
			return TRUE;

		case VK_F9:
			ViewTimeDialog();
			return TRUE;

		case VK_F12:
			ToggleFullScreen();
			return TRUE;

		case VK_INSERT:
			m_libAudioControl.SetVolumeUp();
			return TRUE;

		case VK_DELETE:
			m_libAudioControl.SetVolumeDown();
			return TRUE;

		case VK_END:
			{
				bool bSound = UpdateSoundState();
				SetSoundValue(!bSound);	
			}
			return TRUE;

		case VK_RIGHT:
			{
				PostMessage(ID_PLAY_NEXT_SCHEDULE);
			}
			return TRUE;

		case VK_PAUSE:
			{
				if(m_pCurrentTemplate)
				{
					m_pCurrentTemplate->Pause();
				}//if
			}
			return TRUE;

		case VK_UP:		//TV 채널 UP
			{
				if(GetTVRunningState())
				{
					LRESULT lRet = ::SendMessage(HWND_BROADCAST, msgControlTVModule, TVCONTROL_CHANNEL_UP, NULL);
					__DEBUG__("TV channel up", lRet);
					return TRUE;
				}//if
			}

		case VK_DOWN:	//TV 채널 DOWN
			{
				if(GetTVRunningState())
				{
					LRESULT lRet = ::SendMessage(HWND_BROADCAST, msgControlTVModule, TVCONTROL_CHANNEL_DOWN, NULL);
					__DEBUG__("TV channel down", lRet);
					return TRUE;
				}//if
			}

		case VK_OEM_PLUS:	// '+'
			{
				if(IsSHIFTpressed() && GetTVRunningState())
				{
					LRESULT lRet = ::SendMessage(HWND_BROADCAST, msgControlTVModule, TVCONTROL_VOLUME_UP, NULL);
					__DEBUG__("TV volume up", lRet);
					return TRUE;
				}//if
			}

		case VK_OEM_MINUS:	// '_'
			{
				if(IsSHIFTpressed() && GetTVRunningState())
				{
					LRESULT lRet = ::SendMessage(HWND_BROADCAST, msgControlTVModule, TVCONTROL_VOLUME_DOWN, NULL);
					__DEBUG__("TV volume down", lRet);
					return TRUE;
				}//if
			}

		case VK_BACK:
			{
				if(GetTVRunningState())
				{
					LRESULT lRet = ::SendMessage(HWND_BROADCAST, msgControlTVModule, TVCONTROL_VOLUME_MUTE, NULL);
					__DEBUG__("TV volume mute", lRet);
					return TRUE;
				}//if
			}

		default:
			{
				CTemplatePlay* pclsPlay = NULL;
				for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
				{
					pclsPlay = GetAtTemplatePlay(unIdx);
					if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL)
					{
						continue;
					}//if

					if((pclsPlay->m_pclsTemplate->GetSysShortcutKey() == 0)
						&& (pMsg->wParam == pclsPlay->m_pclsTemplate->GetShortcutKey()))
					{
						//지금 설정된 defaultTemplate와 같다면 변경하지 않는다.
						//if(m_defaultTemplate != pclsPlay->m_pclsTemplate->GetTemplateId())
						//{
							SetDefaultTemplatePlay(pclsPlay->m_pclsTemplate->GetTemplateId());
						//}//if
						break;
					}//if
				}//for
			}
			//return TRUE;
		}//if
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}

CString	CHost::ToString()
{
	CString str;

	str.Format(
		"siteId = %s\r\n"
		"hostId = %s\r\n"
		"soundVolume = %d\r\n"
		"defaultTemplate = %s\r\n"
		"adminState = %d\r\n"
		"operationalState = %d\r\n"
		"Network use = %s\r\n"
		"Description = %s\r\n"

		"\r\n"
		"m_bInitialize = %d\r\n"
		"m_fXScale = %.2f\r\n"
		"m_fYScale = %.2f\r\n"

		, ::GetSiteName()
		, ::GetHostName()
		, m_soundVolume
		, m_defaultTemplate.c_str()
		, m_adminState
		, m_operationalState
		, (m_strNetworkUse == "1" ? "True" : "False")
		, m_strDescription

		, m_bInitialize
		, m_fXScale
		, m_fYScale
	);

	return str;
}

LRESULT CHost::OnDebugString(WPARAM wParam, LPARAM lParam)
{
	m_debugString = ToString();

	return (LRESULT)(LPCTSTR)m_debugString;
}

LRESULT CHost::OnDebugGrid(WPARAM wParam, LPARAM lParam)
{
	CPropertyGrid* grid = (CPropertyGrid*)wParam;

	grid->ResetContents();

	HSECTION hs = grid->AddSection("HOST Attributes");

	grid->AddStringItem(hs, "siteId", ::GetSiteName());
	grid->AddStringItem(hs, "hostId", ::GetHostName());
	grid->AddIntegerItem(hs, "soundVolume", m_soundVolume);
	grid->AddStringItem(hs, "defaultTemplate", m_defaultTemplate.c_str());
	grid->AddIntegerItem(hs, "adminState", m_adminState);
	grid->AddIntegerItem(hs, "operationalState", m_operationalState);
	grid->AddStringItem(hs, "TemplatePlayList", m_templatePlayList.c_str());
	grid->AddStringItem(hs, "Network use", (m_strNetworkUse == "1" ? "True" : "False"));
	grid->AddStringItem(hs, "Description", m_strDescription.c_str());

	hs = grid->AddSection("member variables");

	grid->AddStringItem(hs, "ERROR MESSAGE", (LPCSTR)m_strErrorMessage);
	grid->AddBoolItem(hs, "m_bInitialize", m_bInitialize);
	grid->AddStringItem(hs, "m_fXScale", (m_fXScale == 0.0f ? "auto scale" : ::ToString(m_fXScale)) );
	grid->AddStringItem(hs, "m_fYScale", (m_fYScale == 0.0f ? "auto scale" : ::ToString(m_fYScale)) );
	grid->AddStringItem(hs, "m_nTemplateX", (m_nTemplateX == INT_MAX ? "center" : ::ToString(m_nTemplateX)) );
	grid->AddStringItem(hs, "m_nTemplateY", (m_nTemplateY == INT_MAX ? "center" : ::ToString(m_nTemplateY)) );
	grid->AddBoolItem(hs, "m_bFullScreen", m_bFullScreen);
	grid->AddBoolItem(hs, "m_bTopMostMode", m_bTopMostMode);
	grid->AddBoolItem(hs, "m_bViewMouse", m_bViewMouse);
	grid->AddBoolItem(hs, "m_bLoadFromLocal", m_bLoadFromLocal);
	grid->AddBoolItem(hs, "m_bPauseTemplate", m_bPauseTemplate);
	grid->AddBoolItem(hs, "m_bSound", m_bSound);

	return 0;
}

LRESULT CHost::OnErrorMessage(WPARAM wParam, LPARAM lParam)
{
	if(m_strErrorMessage.GetLength() > 0) m_strErrorMessage.Append(";");
	if(m_strErrorMessage.GetLength() > DEBUG_MSG_LENGTH) m_strErrorMessage.Empty();
	m_strErrorMessage.Append((LPCSTR)wParam);

	CString str = (LPCSTR)wParam;

	// skpark 2013.2.16 errMsg 를 scheduleId 등 별로 보관했다가 clear 하기 위해서...  skpark_manager_err
	// this->m_strStateMsg 가 # 로 시작하면 download err 가 아니고 scheduleError 이다.
	// 이경우 m_errMap 에 아무것도 없다면 m_strStateMsg 를 clear해준다.
	if(!this->m_strStateMsg.IsEmpty() && this->m_strStateMsg[0] == '#'){ // schedule err msg 가 있음
		if(m_errScheduleId.IsEmpty()){ // 그러나 기존의 Error 가 없음.
			m_strStateMsg = ""; 
		}
	}
	if(lParam){
		const char* ptr = (const char*)lParam;
		if(ptr && strlen(ptr)){ // scheuleId 가 넘어온 경우임...
			__ERROR__("SCHEDULE ID=", ptr);
			if(str.IsEmpty() || str.CompareNoCase("ok")==0){  // clear case 이다.
				if(m_errScheduleId.CompareNoCase(ptr)==0 ){
					// scheduleId 가 일치하므로 에러를 삭제한다. 단, 에러가 스케쥴에러여야 한다.				
					if(!this->m_strStateMsg.IsEmpty() && this->m_strStateMsg[0] == '#'){ // schedule err msg 가 있음
						m_strStateMsg="";
					}
					m_errScheduleId = "";
				}
			}else{ // clear case 가 아니다. 뭔가 스케쥴 에러가 났다.
				if(m_strStateMsg.IsEmpty() || this->m_strStateMsg[0] == '#'){ 
					// 이 경우, 선에러가 없거나, 있다 하더라도 스케쥴 에러이다. 이 경우만 Schedule Error 를 표시한다.
					m_strStateMsg = str;
					m_errScheduleId = ptr;
				}else{
					//schedule Error 가 났더라도, 이미 스케쥴에러가 아닌 다른 에러가 있으면 해당 에러가 더 우선시되므로 에러 표시를 하지 않는다.
				}
			}
		}
	}

	__ERROR__(str, _NULL);
	if(m_wndBack.GetSafeHwnd())
	{
		m_wndBack.SetErrorMode(str);
	}//if

	return 0;
}

LRESULT CHost::OnStateDownload(WPARAM wParam, LPARAM lParam)
{
	int nState = (int)wParam;
	int nRet = (int)lParam;
	if(nState == E_DNSTATE_COMPLETE_DOWNLOAD
		|| nState == E_DNSTATE_CANCEL_DOWNLOAD)
	{
		if(nRet == E_DNRET_SUCCESS
			|| nRet == E_DNRET_FAIL_EMPTY_LIST)
		{
			// success
			__DEBUG__("Contents download success", _NULL);
			m_wndBack.AddLog("ok", true);
			// skpark  2012.12.14 // success 했을경우 msg를 없앤다.
			m_strStateMsg = "";
		}
		else if(nRet == E_DNRET_CANCEL)
		{
			// canceled
			__DEBUG__("Contents download canceled", _NULL);
			m_wndBack.AddLog("cancled.", true);
			m_strStateMsg = STAT_MSG_DOWNLOAD_CANCEL;
		}
		else
		{
			// fault
			__DEBUG__("Contents download fail", _NULL);
			m_wndBack.AddLog("failed.", true);
			m_strStateMsg = STAT_MSG_DOWNLOAD_FAIL;
		}//if

		//__DEBUG__(pszRet[nRet], _NULL);
		//__DEBUG__("Downloader return", nRet);
		m_wndBack.EndDownload();
		m_wndBack.AddLog("Loading files... ");

		//skpark 2014.02.17 // download_only mode 라면 종료한다.!!!
		if(ciArgParser::getInstance()->isSet("+download_only")){

			// skpark 2014.03.07 3rdparty player 가 존재하면 시작한다.
			Start3rdPartyPlayer();

			CString strArg;
			CString strAppName = GetAppName();
			if(strAppName == "UTV_brwClient2")
			{
				strArg = "BROWSER";
			}
			else
			{
				strArg = "BROWSER1";
			}//if
			if(!scratchUtil::getInstance()->dontStartAgain(strArg.GetBuffer()))
			{
				__DEBUG__("Fail to call dontStartAgain", strArg);
			}//if
			__DEBUG__("PostMessage(WM_CLOSE,0,0)", _NULL);
			PostMessage(WM_CLOSE, 0, 0);
		}else{
			m_nDownloadStatus = 3;
			SetTimer(OPENFILE_ID, OPENFILE_TIME, NULL);
		}
	}
	else
	{
		m_wndBack.SetDownloadProgress(0, pszState[nState]);
	}//if	

	__DEBUG__("OnStateDownload End",_NULL);
	return 0;
}


LRESULT CHost::OnProgressDownload(WPARAM wParam, LPARAM lParam)
{
	int nRate = (int)wParam;
	LPCSTR lpstrText = (LPCSTR)lParam;
	
	m_wndBack.SetDownloadProgress(nRate, lpstrText);

	return 0;
}

LRESULT CHost::OnFailCountDownload(WPARAM wParam, LPARAM lParam)
{
	m_nFailCount = (int)wParam;
	m_nTotalCount = (int)lParam;

	return 0;
}


void CHost::ViewHelp()
{
	if(m_dlgHelp.IsWindowVisible())
	{
		m_dlgHelp.ShowWindow(SW_HIDE);
	}
	else
	{
		m_dlgHelp.ShowWindow(SW_SHOW);
	}//if
}


int CHost::LoadHost()
{
	if(_access(GetSchedulePath(), 0) != 0 )
	{
		__DEBUG__("Package file access fail", GetSchedulePath());
		return -2;
	}//if

	//Progress set
	m_wndBack.SetPos(E_INIT_HOST, 10);

	if(!MNG_PROFILE.Read(GetSchedulePath()))
	{
		__DEBUG__("Package file read fail", GetSchedulePath());
		return -2;
	}//if

	m_soundVolume = atoi(MNG_PROFILE_READ("Host", "soundVolume"));
	m_defaultTemplate = MNG_PROFILE_READ("Host", "defaultTemplate");
	m_adminState = atoi(MNG_PROFILE_READ("Host", "adminState"));
	m_operationalState = atoi(MNG_PROFILE_READ("Host", "operationalState"));

	m_wndBack.SetPos(E_INIT_HOST, 10);

	m_strDescription = MNG_PROFILE_READ("Host", "description");
	m_strNetworkUse = MNG_PROFILE_READ("Host", "networkUse");
	m_strSite = MNG_PROFILE_READ("Host", "site");
	if(GetEdition() == ENTERPRISE_EDITION)
	{
		m_strLastUpdateId = MNG_PROFILE_READ("Host", "lastUpdateId");
		m_tmLastUpdate.fromString(MNG_PROFILE_READ("Host", "lastUpdateTime"));
	}//if

	if(m_strArgTemplateID != _T(""))
	{
		m_templatePlayList = m_strArgTemplateID;
		m_templatePlayList.append(_T("/1"));
	}
	else
	{
		m_templatePlayList = MNG_PROFILE_READ("Host", "templatePlayList");
	}//if

	//Progress set
	m_wndBack.SetPos(E_INIT_HOST, 10);

	if(m_templatePlayList == "")
	{
		return -4; //Empty TemplatePlayList
	}//if

	if(!CreateTemplatePlayAry(m_templatePlayList.c_str()))
	{
		return -3;	//TemplatePlayList parsing error!!!
	}//if

	if(GetCountOfTemplatePlayAry() == 0)
	{
		return -4; //Empty TemplatePlayList
	}//if

	//Progress set
	m_wndBack.SetPos(E_INIT_HOST, 20);

	return 0;
}

bool CHost::LoadTemplate()
{
	CString template_list;
	if(m_strArgTemplateID != _T(""))
	{
		template_list = _T("Template");
		template_list.Append(m_strArgTemplateID);
	}
	else
	{
		template_list = MNG_PROFILE_READ("Host", "TemplateList");
	}//if
	
	int pos = 0;
	CMapStringToString map_templateId;
	CString token = template_list.Tokenize(",", pos);
	while(token != "")
	{
		token.Trim();
		token.Replace("Template", "");
		if(token != "")
		{
			map_templateId.SetAt(token, token);
		}//if

		token = template_list.Tokenize(",", pos);
	}//while

	if(map_templateId.GetCount() == 0)
	{
		OnErrorMessage((WPARAM)(LPCSTR)"Empty template list", 0);

		return false;
	}//if

	//TemplatePlayList와 TemplateList에 일치하는 Template만을 만들어 준다
	UINT unPlayCount = GetCountOfTemplatePlayAry();
	CTemplatePlay* pclsPlay = NULL;
	CTemplate* pRetTemplate = NULL;
	CString strTemplateID;

	//440 숫자만큼의 진행상황을 나누어 표시한다
	int nAryCount = GetCountOfTemplatePlayAry();
	if(nAryCount == 0)
	{
		m_wndBack.SetErrorMode("Empty template play list !!!");
		return false;
	}//if

	int nFacter = m_wndBack.m_nModeValue[E_INIT_TEMPLATE]/nAryCount;
	if(nFacter == 0)
	{
		nFacter = 1;
	}//if

	CTemplateMap mapTemplate;		//한번 만들어진 template이 중복되서 만들어 지지 않도록...
	for(UINT i=0; i<nAryCount; i++)
	{
		//Progress set
		m_wndBack.SetPos(E_INIT_TEMPLATE, nFacter);

		pclsPlay = (CTemplatePlay*)m_aryTemplatePlay.GetAt(i);
		if(!map_templateId.Lookup(pclsPlay->m_strTemplateID, strTemplateID))
		{
			//template list에 없는 Play list는 삭제 한다
			delete pclsPlay;				//객체삭제
			m_aryTemplatePlay.RemoveAt(i);	//배열에서 삭제
			i--;							//인덱스 조정
			nAryCount--;
		}
		else
		{
			//Template를 생성하고 PlayList에 생성된 Template 포인터를 설정
			if(!mapTemplate.Lookup(strTemplateID, (void*&)pRetTemplate))
			{
				CString strErrorMessage;
				CTemplate* tmpl = CTemplate::GetTemplateObject(this, "Template" + strTemplateID, strErrorMessage);
				if(tmpl)
				{
					pclsPlay->m_pclsTemplate = tmpl;
					mapTemplate.SetAt(strTemplateID, tmpl);
				}
				else
				{
					OnErrorMessage((WPARAM)(LPCSTR)strErrorMessage, 0);
				}//if
			}
			else
			{
				//이미 template이 만들어져 있다면 template 포인터만 할당한다
				pclsPlay->m_pclsTemplate = pRetTemplate;
			}
		}//if
	}//for
	mapTemplate.RemoveAll();

	unPlayCount = 0;
	CString strDefaultTemplateID;

	for(int i=0; i<nAryCount; i++)
	{
		GetPlayCountOfTemplatePlayAry(i, unPlayCount);
		if(unPlayCount != 0)
		{
			SetDefaultTemplatePlay(i);
			break;
		}//if
	}//for

	//templatePlayList의 갯수가 1개 이면 Pause 모드로 설정하여,
	//Template가 변하지 않도록 한다.
	if(nAryCount <= 1)
	{
		m_bPauseTemplate = true;
	}//if

	return true;	
}

LRESULT CHost::OnPlayNextSchedule(WPARAM wParam, LPARAM lParam)
{
	__DEBUG_HOST_BEGIN__(_NULL)

	if(m_pCurrentTemplate != NULL)
	{
		m_pCurrentTemplate->PostMessage(ID_PLAY_NEXT_SCHEDULE);
	}

	__DEBUG_HOST_END__(_NULL)

	return 0;
}


LRESULT CHost::OnContentsUpdate(WPARAM wParam, LPARAM lParam)
{
	if(m_pdlgSelectFrame)
	{
		m_pdlgSelectFrame->DestroyWindow();
		delete m_pdlgSelectFrame;
		m_pdlgSelectFrame = NULL;
	}//if

	if(m_pCurrentTemplate)
	{
		m_pCurrentTemplate->Pause();
	}//if

	return 0;
}

LRESULT CHost::OnPlayPrevDefaultTemplate(WPARAM wParam, LPARAM lParam)
{
	__DEBUG_HOST_BEGIN__(_NULL)

	CString strDefaultTemplateID;
	int nPlayListCount = GetCountOfTemplatePlayAry();
	//Play list가 없다면 오류....
	if(nPlayListCount == 0)
	{
		return 0;
	}//if

	//Play list count가 1 이라면, 첫번째 template이 default template이 된다
	if(nPlayListCount == 1)
	{
		SetDefaultTemplatePlay(0);
		return 0;
	}//if

	KillTimer(TEMPLATE_CHECK_ID);
	if(::GetScheduleResuming())
		KillTimer(TIMER_SHOWWINDOW_ID);	// <!-- 깜박임방지 (2016-08-29) -->

	//이전 default template을 play하기위하여 현재 play중인 teemplate-1 을 새로운 인덱스로 설정
	int nNewIdx = GetPlayingTemplateIndex() - 1;

	UINT unPlayCount = 0;
	for(int i=0; i<nPlayListCount; i++)
	{
		if(nNewIdx < 0)
		{
			nNewIdx = nPlayListCount - 1;		//tempalte list의 맨마지막
		}//if

		if(nNewIdx == GetPlayingTemplateIndex())
		{
			//현재 play중인 인덱스와 같다면 현재 template를 다시 시작한다.
			//m_pCurrentTemplate->Stop();
			m_pCurrentTemplate = NULL;

			break;
		}//if

		if(!GetPlayCountOfTemplatePlayAry(nNewIdx, unPlayCount))
		{
			break;		//play count 가져오기 실패
		}
		else if(unPlayCount != 0)
		{
			//play count가 0이 아니라면, 새로운 인덱스로 default template 인덱스를 변경
			SetDefaultTemplatePlay(nNewIdx);
			break;
		}//if

		nNewIdx--;
	}//for

	_checkDefaultSchedule(false);

	SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
	if(::GetScheduleResuming())
		SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL);	// <!-- 깜박임방지 (2016-08-29) -->

	__DEBUG_HOST_END__(_NULL)

	return 0;
}

LRESULT CHost::OnPlayNextDefaultTemplate(WPARAM wParam, LPARAM lParam)
{
	__DEBUG_HOST_BEGIN__(_NULL)
	
	CString strDefaultTemplateID;
	int nPlayListCount = GetCountOfTemplatePlayAry();
	//Play list가 없다면 오류....
	if(nPlayListCount == 0)
	{
		return 0;
	}//if

	//Play list count가 1 이라면, 첫번째 template이 default template이 된다
	if(nPlayListCount == 1)
	{
		SetDefaultTemplatePlay(0);
		return 0;
	}//if

	KillTimer(TEMPLATE_CHECK_ID);
	if(::GetScheduleResuming())
		KillTimer(TIMER_SHOWWINDOW_ID);	// <!-- 깜박임방지 (2016-08-29) -->

	//다음 default template을 play하기위하여 현재 play중인 teemplate+1 을 새로운 인덱스로 설정
	int nNewIdx = GetPlayingTemplateIndex() + 1;

	UINT unPlayCount = 0;
	for(int i=0; i<nPlayListCount; i++)
	{
		if(nNewIdx >= nPlayListCount)
		{
			nNewIdx = 0;			//tempalte list의 맨처음

			// <!-- 이어서 재생 (2016-08-29)
			// 마지막 재생한 템플릿의 단축키
			CString str_resuming_shortcut = CTemplate::GetLastPlayTemplateShortCut();
			if( str_resuming_shortcut.GetLength() > 0 )
			{
				// 이전에 재생한 템플릿의 단축키가 존재
				UINT unKeyValue_resuming[2] = {0,'0'};
				CString strShortcut = str_resuming_shortcut;
				unKeyValue_resuming[0] = strShortcut.Right(1).GetAt(0); // 마지막 문자만 추출
				if( unKeyValue_resuming[0]=='9' )
				{
					// 0~9 => A~Z
					unKeyValue_resuming[0] = 'A';
					__DEBUG__("next Resuming Shortcut", (int)unKeyValue_resuming[0]);
				}
				else
				{
					// 다음 문자
					unKeyValue_resuming[0]++;
					__DEBUG__("next Resuming Shortcut", (int)unKeyValue_resuming[0]);
				}

				UINT unSyskey = 0, unKeyValue = 0;
				if(strShortcut.Find(_T("ALT"), 0) != -1)			//ALT key
				{
					unSyskey = VK_MENU;
					strShortcut.Replace(_T("ALT"), _T(""));
					int nIdx = strShortcut.Find(_T("+"), 0);
					if(nIdx != -1)
					{
						strShortcut.Delete(0, nIdx+1);
					}//if
				}
				else if(strShortcut.Find(_T("CTRL"), 0) != -1)	//CTRL key
				{
					unSyskey = VK_CONTROL;
					strShortcut.Replace(_T("CTRL"), _T(""));
					int nIdx = strShortcut.Find(_T("+"), 0);
					if(nIdx != -1)
					{
						strShortcut.Delete(0, nIdx+1);
					}//if
				}//if

				if(strShortcut.GetLength() == 1)
				{
					char cHot = strShortcut.GetAt(0);
					unKeyValue = cHot;
				}
				else if(strShortcut == "F1")		unKeyValue = VK_F1;
				else if(strShortcut == "F2")		unKeyValue = VK_F2;
				else if(strShortcut == "F3")		unKeyValue = VK_F3;
				else if(strShortcut == "F4")		unKeyValue = VK_F4;
				else if(strShortcut == "F5")		unKeyValue = VK_F5;
				else if(strShortcut == "F6")		unKeyValue = VK_F6;
				else if(strShortcut == "F7")		unKeyValue = VK_F7;
				else if(strShortcut == "F8")		unKeyValue = VK_F8;
				else if(strShortcut == "F9")		unKeyValue = VK_F9;
				else if(strShortcut == "F10")		unKeyValue = VK_F10;
				else if(strShortcut == "F11")		unKeyValue = VK_F11;
				else if(strShortcut == "F12")		unKeyValue = VK_F12;

				CTemplatePlay* pclsPlay = NULL;
				for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
				{
					pclsPlay = GetAtTemplatePlay(unIdx);
					if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL) continue;

					//각 template이 shortcut key가 설정되어 있는지 검사한다
					if( pclsPlay->m_pclsTemplate->GetSysShortcutKey() != unSyskey
						|| unKeyValue != pclsPlay->m_pclsTemplate->GetShortcutKey() ) continue;

					__DEBUG__("Check RemainPlayingDefaultSchedule", pclsPlay->m_pclsTemplate->GetTemplateId());
					if(::GetScheduleResuming() && pclsPlay->m_pclsTemplate->IsRemainPlayingDefaultSchedule())
					{
						// 이전 재생하던 템플릿에서 재생하지못한 스케줄 존재
						__DEBUG__("Set shortcut template", pclsPlay->m_pclsTemplate->GetTemplateId());
						SetDefaultTemplatePlay(pclsPlay->m_pclsTemplate->GetTemplateId());
						break;
					}

					// 다음 문자로 다음템플릿 찾기
					bool find_resuming = false;
					for(int i=0; i<2 && find_resuming==false; i++)
					{
						__DEBUG__("find next shortcut template", (int)unKeyValue_resuming[i]);	// 다음문자
						for(UINT j=0; j<GetCountOfTemplatePlayAry(); j++)
						{
							pclsPlay = GetAtTemplatePlay(j);
							if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL) continue;

							if( pclsPlay->m_pclsTemplate->GetSysShortcutKey() != unSyskey
								|| unKeyValue_resuming[i] != pclsPlay->m_pclsTemplate->GetShortcutKey() ) continue;

							// 다음문자와 매칭되는 템플릿 찾음
							find_resuming = true;
							__DEBUG__("Set shortcut template", pclsPlay->m_pclsTemplate->GetTemplateId());
							SetDefaultTemplatePlay(pclsPlay->m_pclsTemplate->GetTemplateId());
							break;
						}
					}
					if(find_resuming) break;
				}//for

				break;
			}
			// -->

		}//if

		if(nNewIdx == GetPlayingTemplateIndex())
		{
			//현재 play중인 인덱스와 같다면 현재 template를 다시 시작한다.
			//m_pCurrentTemplate->Stop();
			m_pCurrentTemplate = NULL;

			break;
		}//if

		if(!GetPlayCountOfTemplatePlayAry(nNewIdx, unPlayCount))
		{
			break;		//play count 가져오기 실패
		}
		else if(unPlayCount != 0)
		{
			//play count가 0이 아니라면, 새로운 인덱스로 default template 인덱스를 변경
			SetDefaultTemplatePlay(nNewIdx);
			break;
		}//if

		nNewIdx++;
	}//for

	_checkDefaultSchedule(true);

	SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
	if(::GetScheduleResuming())
		SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL);	// <!-- 이어서 재생 (2016-08-29) -->

	__DEBUG_HOST_END__(_NULL)

	return 0;
}

LRESULT CHost::OnPlayPrevTemplate(WPARAM wParam, LPARAM lParam)
{
	__DEBUG_HOST_BEGIN__(_NULL)

	CString strDefaultTemplateID;
	int nPlayListCount = GetCountOfTemplatePlayAry();
	//Play list가 없다면 오류....
	if(nPlayListCount == 0)
	{
		return 0;
	}//if

	//Play list count가 1 이라면, 첫번째 template이 default template이 된다
	if(nPlayListCount == 1)
	{
		SetDefaultTemplatePlay(0);
		return 0;
	}//if

	KillTimer(TEMPLATE_CHECK_ID);
	if(::GetScheduleResuming())
		KillTimer(TIMER_SHOWWINDOW_ID);	// <!-- 이어서 재생 (2016-08-29) -->

	//이전 template을 play하기위하여 현재 play중인 teemplate-1 을 새로운 인덱스로 설정
	int nNewIdx = GetPlayingTemplateIndex() - 1;
	if(nNewIdx < 0)
	{
		nNewIdx = GetCountOfTemplatePlayAry() - 1;		//tempalte list의 맨마지막
	}//if

	//새로운 인덱스의 template play count와 상관없이 변경
	SetDefaultTemplatePlay(nNewIdx);

	CheckSchedule(false);

	SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
	if(::GetScheduleResuming())
		SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL);	// <!-- 이어서 재생 (2016-08-29) -->

	__DEBUG_HOST_END__(_NULL)

	return 0;
}

LRESULT CHost::OnPlayNextTemplate(WPARAM wParam, LPARAM lParam)
{
	__DEBUG_HOST_BEGIN__(_NULL)
	
	CString strDefaultTemplateID;
	int nPlayListCount = GetCountOfTemplatePlayAry();
	//Play list가 없다면 오류....
	if(nPlayListCount == 0)
	{
		return 0;
	}//if

	//Play list count가 1 이라면, 첫번째 template이 default template이 된다
	if(nPlayListCount == 1)
	{
		SetDefaultTemplatePlay(0);
		return 0;
	}//if

	KillTimer(TEMPLATE_CHECK_ID);
	if(::GetScheduleResuming())
		KillTimer(TIMER_SHOWWINDOW_ID);	// <!-- 이어서 재생 (2016-08-29) -->

	//다음 template을 play하기위하여 현재 play중인 teemplate+1 을 새로운 인덱스로 설정
	int nNewIdx = GetPlayingTemplateIndex() + 1;
	if(nNewIdx >= nPlayListCount)
	{
		nNewIdx = 0;			//tempalte list의 맨처음
	}//if

	//새로운 인덱스의 template play count와 상관없이 변경
	SetDefaultTemplatePlay(nNewIdx);
	
	CheckSchedule(true);

	SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
	if(::GetScheduleResuming())
		SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL);	// <!-- 이어서 재생 (2016-08-29) -->

	__DEBUG_HOST_END__(_NULL)

	return 0;
}


void CHost::SetDefaultTemplateId(LPCSTR lpszTemplateId)
{
	__DEBUG__("Default Template Change", lpszTemplateId);

	m_csHost.Lock();

	if(m_pCurrentTemplate != NULL)
	{
		if(strcmp(m_pCurrentTemplate->GetTemplateId(), lpszTemplateId) == 0)
		{
			m_pCurrentTemplate->Stop();
			m_pCurrentTemplate = NULL;
		}//if
	}//if

	m_defaultTemplate = lpszTemplateId;

	m_csHost.Unlock();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// defaultTemplate를 반환 \n
/// @return <형: CTemplate*> \n
///			<CTemplate*: DefaultTemplate> \n
///			<NULL: 설정된 DefaultTemplate 없음> \n
/////////////////////////////////////////////////////////////////////////////////
CTemplate* CHost::GetdefaultTemplate()
{
	if(m_defaultTemplate != "")
	{
		return GetTemplateByTemplatID(m_defaultTemplate.c_str());
	}//if

	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 문자열을 파싱하여, TemplatePlayAry를 생성 한다.\n
/// 문자열에서 각 tempate은 ','으로 구분되며, 다시 구분된 토큰에서 '/'으로\n
/// 재생방법이나 재생 schedule order의 배열을 파싱한다.\n
/// (ex 1) templateid1/playcount, templateid2/(frameid1|frameid3), templateid3/(frameid2-frameid5), .....\n
/// playcount는 template의 반복횟수를 의미하며, '|'는 순차재생, '-'는 연속된 일련의 재생순서를 나타낸다.\n
/// @param (const char*) szTemplatePlayList : (in) template play 방법을 갖는 문자열
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::CreateTemplatePlayAry(const char* szTemplatePlayList)
{
	CString strSrc(szTemplatePlayList);
	//strSrc.Remove(' ');
	strSrc.Trim();
	if(strSrc.GetLength() == 0)
	{
		return false;
	}//if

	CTemplatePlay* pclsTemplatePlay = NULL;
	//tempalte play 순서를 구분하는 ','가 없으면 하나의 template만 존재하는 경우
	if(strSrc.Find(',', 0) == -1)
	{
		pclsTemplatePlay = new CTemplatePlay(strSrc);
		m_aryTemplatePlay.Add(pclsTemplatePlay);
		return true;
	}//if

	CString strTok;
	int nPos = 0;
	//','를 기준으로 각 template를 분리하고,
	//분리된 각 template에 '/'가 있다면, CTemplatePlay 객체를 생성한다.
	strTok = strSrc.Tokenize(",", nPos);
	while(strTok != "")
	{
		if(strTok.Find('/', 0) == -1)
		{	//잘못된 형식
			return false;
		}//if

		strTok.Trim();
		pclsTemplatePlay = new CTemplatePlay(strTok);
		if(pclsTemplatePlay->m_strTemplateID == "")
		{
			delete pclsTemplatePlay;
		}
		else
		{
			m_aryTemplatePlay.Add(pclsTemplatePlay);
		}//if

		strTok = strSrc.Tokenize(",", nPos);		
	}//while

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// TemplatePlayAry의 count를 반환\n
/// @param (void)
/// @return <형: UINT> \n
///			<m_AryTemplatePlay의 count를 반환> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CHost::GetCountOfTemplatePlayAry(void)
{
	m_csHost.Lock();

	UINT unCount = m_aryTemplatePlay.GetCount();

	m_csHost.Unlock();

	return unCount;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// TemplatePlayAry의 지정된 인덱스의 play count를 반환\n
/// @param (UINT) unIndex : (in) TemplatePlayAry의 인덱스
/// @param (UINT&) unPlayCount : (out) 주어진 인덱스의 template의 playcount
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::GetPlayCountOfTemplatePlayAry(UINT unIndex, UINT& unPlayCount)
{
	m_csHost.Lock();

	CTemplatePlay* pclsTemplatePlay = (CTemplatePlay*)m_aryTemplatePlay.GetAt(unIndex);
	if(pclsTemplatePlay)
	{
		unPlayCount = pclsTemplatePlay->m_unPlayCount;

		m_csHost.Unlock();

		return true;
	}//if
	
	m_csHost.Unlock();

	return false;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// TemplatePlayAry의 인덱스로 default template를 설정 \n
/// @param (UINT) unIndex : (in) default tempalte로 설정하려는 template play arrary의 인덱스
/////////////////////////////////////////////////////////////////////////////////
void CHost::SetDefaultTemplatePlay(UINT unIndex)
{
	m_csHost.Lock();

	m_unPlayingTemplateIndex = unIndex;

	CTemplatePlay* pclsTemplatePlay = GetAtTemplatePlay(unIndex);
	if(pclsTemplatePlay)
	{
		SetDefaultTemplateId(pclsTemplatePlay->m_strTemplateID);		
	}//if

	m_csHost.Unlock();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Template id로 default template를 설정 \n
/// @param (CString) strTemplateId : (in) 설정하려는 template id
/////////////////////////////////////////////////////////////////////////////////
bool CHost::SetDefaultTemplatePlay(CString strTemplateId)
{
	m_csHost.Lock();

	CTemplatePlay* pclsTemplatePlay = NULL;
	for(int i=0; i<m_aryTemplatePlay.GetSize(); i++)
	{
		pclsTemplatePlay = (CTemplatePlay*)m_aryTemplatePlay.GetAt(i);
		if(strcmp(pclsTemplatePlay->m_pclsTemplate->GetTemplateId(), strTemplateId) == 0)
		{
			m_unPlayingTemplateIndex = i;
			SetDefaultTemplateId(strTemplateId);
			
			m_csHost.Unlock();
			return true;
		}//if
	}//for

	m_csHost.Unlock();
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// TemplateId로 TemplatePlay Ary의 Template pointer 반환 \n
/// @param (CString) strTmplateID : (in) Template ID
/// @return <형: CTemplate*> \n
///			<CTemplate*: 성공> \n
///			<NULL: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
CTemplate* CHost::GetTemplateByTemplatID(CString strTmplateID)
{
	m_csHost.Lock();

	CTemplatePlay* pclsTemplatePlay = NULL;
	for(UINT unIndx=0; unIndx<m_aryTemplatePlay.GetCount(); unIndx++)
	{
		pclsTemplatePlay = (CTemplatePlay*)m_aryTemplatePlay.GetAt(unIndx);
		if(pclsTemplatePlay && (pclsTemplatePlay->m_strTemplateID == strTmplateID))
		{
			m_csHost.Unlock();

			return pclsTemplatePlay->m_pclsTemplate;
		}//if
	}//for

	m_csHost.Unlock();

	return NULL;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 현재 할성화되어 play 중인 template의 인덱스를 반환 \n
/// @return <형: UINT> \n
///			template play list에서 활성화되어 paly중인 노드의 인덱스 \n
/////////////////////////////////////////////////////////////////////////////////
UINT CHost::GetPlayingTemplateIndex(void)
{
	m_csHost.Lock();

	UINT unIndex = m_unPlayingTemplateIndex;

	m_csHost.Unlock();

	return unIndex;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// TemplatePalyArray에서 지정된 인덱스의 CTemplatePlay 객체 반환 \n
/// @param (UINT) unIndex : (in) TemplatePalyArray의 인덱스
/// @return <형: CTemplatePlay*> \n
///			<CTemplatePlay*: 성공> \n
///			<NULL: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
CTemplatePlay* CHost::GetAtTemplatePlay(UINT unIndex)
{
	m_csHost.Lock();

	if(unIndex >= m_aryTemplatePlay.GetCount())
	{
		m_csHost.Unlock();

		return NULL;
	}//if

	CTemplatePlay* pclsTmpPlay = (CTemplatePlay*)m_aryTemplatePlay.GetAt(unIndex);

	m_csHost.Unlock();

	return pclsTmpPlay;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// TemplatePalyArray에서 지정된 template id의 CTemplatePlay 객체 반환 \n
/// @param (LPCSTR) lpszTemplateId : (in) Template id
/// @return <형: CTemplatePlay*> \n
///			<CTemplatePlay*: 지정된 template id의 CTemplatePlay 객체> \n
/////////////////////////////////////////////////////////////////////////////////
CTemplatePlay* CHost::GetAtTemplatePlay(LPCSTR lpszTemplateId)
{
	m_csHost.Lock();

	CTemplatePlay* pPlay = NULL;
	for(int i=0; i<m_aryTemplatePlay.GetCount(); i++)
	{
		pPlay = (CTemplatePlay*)m_aryTemplatePlay.GetAt(i);
		if(strcmp(pPlay->m_strTemplateID, lpszTemplateId) == 0)
		{
			return pPlay;
		}//if
	}//for

	m_csHost.Unlock();

	return NULL;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// TemplatePlayArray에서 지정된 인덱스의 객체 삭제 \n
/// @param (UINT) unIndex : (in) TemplatePalyArray의 인덱스
/////////////////////////////////////////////////////////////////////////////////
void CHost::RemoveAtTemplatePlay(UINT unIndex)
{
	m_csHost.Lock();

	m_aryTemplatePlay.RemoveAt(unIndex);

	m_csHost.Unlock();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// BRW application을 시작한다\n
/////////////////////////////////////////////////////////////////////////////////
void CHost::StartBRW(void)
{
	m_nStateBRW = E_BRW_INIT;
	m_wndBack.ClearLog();
	m_wndBack.AddLog("Starting browser... ok", true);

	CLogDialog::getInstance(this)->Add(NULL, this, ::GetHostName(), 1);

	__DEBUG__("GetLicenseCheckDelay", GetLicenseCheckDelay());
	switch(GetLicenseCheckDelay())
	{
	case 0: // 바로 생성
		if(!m_bVerify)
		{
			//인증이 안되었을 때...
#ifndef _DEBUG
			MakeTrialAnnounce();
#endif
		}//if
		break;

	case -1: // 생성 안함
		break;

	default: // 딜레이 후 생성
		SetTimer(TIMER_LICENSE_CHECK_ID, GetLicenseCheckDelay(), NULL);
		break;
	}

	m_wndBack.SetPos(E_INIT_UI, 5);

	if(!m_bLoadFromLocal)
	{
		m_wndBack.AddLog("Downloading package file form server... ");
		if(!DownloadPackageFile())
		{
			m_bPackageDown = false;
		}
		else
		{
			m_bPackageDown = true;
		}//if
	}//if
	
	m_wndBack.AddLog("Loading data... ");
	if(ciArgParser::getInstance()->isSet("+download_only"))
	{
		SetTimer(CHECKFILE_ID, CHECKFILE_TIME, NULL);
	}
	else
	{
		SetTimer(LOAD_HOST_ID, LOAD_HOST_TIME, NULL);
		SetTimer(TIMER_CHECK_UPDATER_ID, TIMER_CHECK_UPDATER_TIME, NULL);
	}//if

	//Progress set
	m_wndBack.SetPos(E_INIT_UI, 5);

	__DEBUG__("StartBRW end",_NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// BRW application을 종료한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::StopBRW(void)
{
	__DEBUG__("Begin Stop BRW", _NULL);

//#ifndef _DEBUG
	//마우스 커서 
	if(ciArgParser::getInstance()->isSet("+mouse"))
	{
		__DEBUG__("Show Mouse Cursor Mode", _NULL);
		StopCoursorHook();
	}
	else
	{
		__DEBUG__("Hide Mouse Cursor Mode", _NULL);
		SetShowCursor(true);
	}//if
//#endif

	KillTimer(TEMPLATE_CHECK_ID);
	if(::GetScheduleResuming())
		KillTimer(TIMER_SHOWWINDOW_ID);	// <!-- 이어서 재생 (2016-08-29) -->

	//긴급공지 중지
	if(m_pdlgAnnounce)
	{
		m_pdlgAnnounce->DestroyWindow();
		delete m_pdlgAnnounce;
		m_pdlgAnnounce = NULL;
	}//if
	//긴급공지 중지
	if(m_pdlgAnnounce_image)
	{
		m_pdlgAnnounce_image->DestroyWindow();
		delete m_pdlgAnnounce_image;
		m_pdlgAnnounce_image = NULL;
	}//if
	//긴급공지 중지
	if(m_pdlgAnnounce_video)
	{
		m_pdlgAnnounce_video->DestroyWindow();
		delete m_pdlgAnnounce_video;
		m_pdlgAnnounce_video = NULL;
	}//if
	//긴급공지 중지
	if(m_pdlgAnnounce_flash)
	{
		m_pdlgAnnounce_flash->DestroyWindow();
		delete m_pdlgAnnounce_flash;
		m_pdlgAnnounce_flash = NULL;
	}//if
	if(m_pdlgAnnounce_ppt)
	{
		m_pdlgAnnounce_ppt->DestroyWindow();
		delete m_pdlgAnnounce_ppt;
		m_pdlgAnnounce_ppt = NULL;
	}//if
	if(m_pdlgAnnounce_web)
	{
		m_pdlgAnnounce_web->DestroyWindow();
		delete m_pdlgAnnounce_web;
		m_pdlgAnnounce_web = NULL;
	}//if
	//ThreadPauseBRW
	if(!m_bExitPauseBRWThread)
	{
		m_bExitPauseBRWThread = true;
		SetEvent(m_hEventThreadPauseBRW);
	}//if

	if(m_pCurrentTemplate)
	{
		m_pCurrentTemplate->Stop();
	}//if

	CTemplatePlay* pclsPlay = NULL;
	for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
	{
		pclsPlay = GetAtTemplatePlay(unIdx);
		if(pclsPlay && pclsPlay->m_pclsTemplate && pclsPlay->m_pclsTemplate->GetSafeHwnd())
		{
			//pclsPlay->m_pclsTemplate->PostMessage(WM_HIDE_CONTENTS, 0, 0);
			pclsPlay->m_pclsTemplate->Stop();
		}//if
	}//for

	m_wndBack.ClearLog();
	m_wndBack.ShowWindow(SW_SHOW);
	m_wndBack.AddLog(_T("Stop browser..."));

	InitWndPostion();
	
	pclsPlay = NULL;
	CTemplateMap mapTemplate;		//한번 삭제한 template이 중복되서 삭제되지 않도록...
	CTemplate* pclsTemplate = NULL;
	for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
	{
		pclsPlay = GetAtTemplatePlay(unIdx);
		if(pclsPlay == NULL || pclsPlay->m_pclsTemplate == NULL)
		{
			continue;
		}//if

		if(!mapTemplate.Lookup(pclsPlay->m_strTemplateID, (void*&)pclsTemplate))
		{
			mapTemplate.SetAt(pclsPlay->m_strTemplateID, pclsPlay->m_pclsTemplate);
			pclsPlay->m_pclsTemplate->DestroyWindow();
			delete pclsPlay->m_pclsTemplate;
		}//if
		pclsPlay->m_pclsTemplate = NULL;
		delete pclsPlay;
	}//for
	mapTemplate.RemoveAll();
	m_aryTemplatePlay.RemoveAll();

	m_bInitialize				= false;
	m_pCurrentTemplate			= NULL;
	m_soundVolume				= 0;
	m_defaultTemplate			= _T("00");
	m_adminState				= ciFalse;
	m_operationalState			= ciFalse;
	m_templatePlayList			= _T("");
	m_strDescription			= _T("");
	m_strNetworkUse				= _T("1");
	m_strSite					= _T("");
	m_strLastUpdateId			= _T("");
	m_strErrorMessage			= _T("");
	m_bPauseTemplate			= false;
	m_strTerminateApp			= _T("");
	m_tmPausTime				= 0;
	m_strReturnTemplateId		= _T("");
	m_bClickJump				= false;
	m_dwClickJumpWaitTime		= 0;
	m_nFailCount				= 0;
	m_nTotalCount				= 0;
	m_bPackageDown				= false;
	m_nStateBRW					= E_BRW_INIT;
	m_nPid						= 0;
	m_strStateMsg				= "";

	m_wndBack.AddLog(_T("Press 'CTRL + Q' key to restart"));


	__DEBUG__("End Stop BRW", _NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// BRW application의 UI를 구성 한다 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::InitUI(void)
{
	this->SetWindowText("UTV_BRW");
	CRect rectClient(0, 0, m_nCx, m_nCy);
	m_brushBack.CreateSolidBrush(BACKGROUND_COLOR);
	m_wndBack.Create(NULL, "BackgroundWnd", WS_CHILD | WS_VISIBLE, rectClient, this, 0xffff);

	//Site의 자체 logo 이미지 파일을 사용하는 경우
	CString strLogoFile;
	char drive[_MAX_DRIVE]	= { 0x00 };
	char dir[_MAX_DIR]		= { 0x00 };
	char fname[_MAX_FNAME]	= { 0x00 };
	char ext[_MAX_EXT]		= { 0x00 };

	_splitpath(GetAppPath(), drive, dir, fname, ext);

	CString strBmpName;
	if(m_nCx > m_nCy)
	{
		strBmpName = "_logo_h.bmp";
	}
	else
	{
		strBmpName = "_logo_v.bmp";
	}//if

	//PE, PLUS 에디션에서만 벤더의 로고 파일을 표시한다.
	if(GetEdition() != ENTERPRISE_EDITION)
	{
		//SiteId
		CString strPath, strSiteId;
		char cBuffer[BUF_SIZE] = { 0x00 };
		strPath.Format("%s%sdata\\UBCVariables.ini", drive, dir);

		GetPrivateProfileString("ROOT", "SiteId", "", cBuffer, BUF_SIZE, strPath);
		strSiteId = cBuffer;

		strLogoFile.Format("%s%sdata\\%s%s", drive, dir, strSiteId, strBmpName);

		if(::PathFileExists(strLogoFile))
		{
			m_bLogoMode = true;
			m_wndBack.SetLogoImage(strLogoFile);
		}
		else
		{	
			strLogoFile.Format("%s%sdata\\sqi%s", drive, dir, strBmpName);
			m_bLogoMode = true;
			m_wndBack.SetLogoImage(strLogoFile);
		}//if
	}//if

	//밴더의 로고가 없다면 UBC 로고 파일을 읽도록한다
	if(!m_bLogoMode)
	{
		CString strCustomer = GetCoutomerInfo();
		strCustomer.MakeLower();
		if(strCustomer == "")
		{
			strCustomer = "sqi";
		}//if

		strLogoFile.Format("%s%sdata\\%s%s", drive, dir, strCustomer, strBmpName);

		if(::PathFileExists(strLogoFile))
		{
			m_bLogoMode = true;
			m_wndBack.SetLogoImage(strLogoFile);
		}
		else
		{
			strLogoFile.Format("%s%sdata\\sqi%s", drive, dir, strBmpName);
			if(::PathFileExists(strLogoFile))
			{
				m_bLogoMode = true;
				m_wndBack.SetLogoImage(strLogoFile);
			}//if
		}//if
	}//if

	SetTopMost(m_bTopMostMode);
	InitWndPostion();

	if(m_timeDialog.Create(IDD_TIME_DIALOG))
	{
		m_timeDialog.ModifyStyleEx(WS_EX_APPWINDOW, WS_EX_TOOLWINDOW);
		m_timeDialog.MoveWindow(0, 0,  TIMEDIALOG_WIDTH, TIMEDIALOG_HEIGHT);

		m_timeDialog.ShowWindow(SW_HIDE);
	}//if
	if(m_licenseDialog.Create(IDD_TIME_DIALOG))
	{
		m_licenseDialog.ModifyStyleEx(WS_EX_APPWINDOW, WS_EX_TOOLWINDOW);
		m_licenseDialog.MoveWindow(10, 10,  LICENSE_DIALOG_WIDTH, LICENSE_DIALOG_HEIGHT);

		m_licenseDialog.ShowWindow(SW_HIDE);
	}//if

	m_dlgHelp.SetAppInof(m_strEdition, m_strVersion, m_strComputerName, m_strRegstrationKey);
	if(m_dlgHelp.Create(IDD_HELP_DIALOG))
	{
		m_dlgHelp.ShowWindow(SW_HIDE);
	}//if

	if(!m_libAudioControl.Init())
	{
		__DEBUG__("LibAudioControl init fail", _NULL);
	}//if

	//메뉴바
	m_pdlgMenuBar = new CMenuBarDlg(this);
	m_pdlgMenuBar->Create(IDD_MENU_BAR_DLG, this);
	m_pdlgMenuBar->SetPosition(m_rectMenuBar.left, m_rectMenuBar.top, m_rectMenuBar.Width(), m_rectMenuBar.Height());

	//메뉴 버튼
	m_dlgMenuBtn.Create(IDD_MENUBTN_DLG, this);
	m_dlgMenuBtn.SetMenuBar(m_pdlgMenuBar);
	int nPosX, nPosY;
	nPosX = (m_nPosX+m_nCx) - (24);
	nPosY = (m_nPosY+m_nCy) - (18);
	m_dlgMenuBtn.MoveWindow(nPosX, nPosY, 24, 18);

	if(ciArgParser::getInstance()->isSet("+menubar"))
	{
		m_bShowMenuButton = true;
	}
	else
	{
		m_bShowMenuButton = false;
	}//if

	m_pdlgMenuBar->SetMenuButtonHandle(m_dlgMenuBtn.GetSafeHwnd());

	srand( (unsigned)time( NULL ) );

	return true;
}


BOOL CHost::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(!m_bInitialize && 
		pCopyDataStruct->dwData != UBC_WM_BRW_GET_DOWNLOAD_STATUS)
	{
		//화면의 초기화를 끝내지 못했다면 메시지를 무시~
		return CDialog::OnCopyData(pWnd, pCopyDataStruct);
	}//if

	switch(pCopyDataStruct->dwData)
	{
	case 800:	//Mini mize & Pause
		{
			__DEBUG__("Mini mize & Pause", _NULL);
			if(m_pCurrentTemplate)
			{
				m_pCurrentTemplate->Pause();
			}//if

			ShowWindow(SW_HIDE);
		}
		break;
	case 801:	//Maximize & Play
		{
			__DEBUG__("Maximize & Play", _NULL);
			if(m_pCurrentTemplate)
			{
				m_pCurrentTemplate->Pause();
			}//if

			ShowWindow(SW_SHOW);
		}
		break;
	case 900:	//Show
		{
			__DEBUG__("FWV : Show window receive....!!!!", _NULL);

			SetTimer(HOST_SHOW_ID, HOST_SHOW_TIME, NULL);
		}
		break;
	case 901:	//Sound On/Off
		{
			__DEBUG__("FWV : pCopyDataStruct->cbData", (int)pCopyDataStruct->cbData);
			__DEBUG__("FWV : pCopyDataStruct->lpData", (char*)pCopyDataStruct->lpData);
			char* pszArray = NULL;
			int nSize = (pCopyDataStruct->cbData*sizeof(char));
			pszArray = (char*)malloc(nSize);
			memset(pszArray, 0x00, nSize);
			memcpy(pszArray, pCopyDataStruct->lpData, nSize);

			CString strArray;
			strArray.Format("%s", pszArray);
			free(pszArray);
			//원래는 Display 1,2면의 Sound값이 변했다는 의미로 사용하기위해
			//배열을 사용했지만, 하드웨어 Sound값을 On/Off 했다는 의미로 사용하기로 변경함.
			// Sound On : 1, Sound Off : 0
			__DEBUG__("FWV : Receive sound on/off string", strArray);

			int nSound = atoi(strArray);
			SetSoundValue(nSound);
		}
		break;
	case 902:		//Announce Start
		{
			//skpark 2013.11.28 new announce
			CString strAnnounceId = (char*)pCopyDataStruct->lpData;
			__DEBUG__("Announce start receive", strAnnounceId);
			//CString strContentsType = (char*)pCopyDataStruct->lpData;
			//strContentsType = strContentsType.Mid(0,1);
			//__DEBUG__("Announce start receive", strContentsType);

			AnnounceMsg* msg = new AnnounceMsg();
			msg->announceId = strAnnounceId;
			msg->msgNo = 902;

			::PostMessageA(this->GetSafeHwnd(), WM_USER_ANNOUNCE, NULL, (LPARAM)msg);
			break;
/*
			if(!m_bVerify)
			{
				__DEBUG__("DEMO key : ignore event", _NULL);
			}
			else
			{
				//skpark 2013.11.28 new announce
				ReadAnnounce(strAnnounceId, true);
				//ReadAnnounce(atoi(strContentsType));
				//긴급공지 시작
				if(m_pdlgAnnounce)
				{
					if(!m_pdlgAnnounce->IsPlay())
					{
						__DEBUG__("Start announce play", _NULL);
						m_pdlgAnnounce->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_image)
				{
					if(!m_pdlgAnnounce_image->IsPlay())
					{
						__DEBUG__("Start announce_image play", _NULL);
						m_pdlgAnnounce_image->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_video)
				{
					if(!m_pdlgAnnounce_video->IsPlay())
					{
						__DEBUG__("Start announce_video play", _NULL);
						m_pdlgAnnounce_video->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_flash)
				{
					if(!m_pdlgAnnounce_flash->IsPlay())
					{
						__DEBUG__("Start announce_flash play", _NULL);
						m_pdlgAnnounce_flash->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_ppt)
				{
					if(!m_pdlgAnnounce_ppt->IsPlay())
					{
						__DEBUG__("Start announce_ppt play", _NULL);
						m_pdlgAnnounce_ppt->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_web)
				{
					if(!m_pdlgAnnounce_web->IsPlay())
					{
						__DEBUG__("Start announce_web play", _NULL);
						m_pdlgAnnounce_web->StartAnnouncePlay();
					}//if
				}//if

			}//if
*/
		}
		break;
	case 903:		//Announce Stop
		{
			//skpark 2013.11.28 new announce
			CString strAnnounceId = (char*)pCopyDataStruct->lpData;
			__DEBUG__("Announce stop receive", strAnnounceId);
			//CString strContentsType = (char*)pCopyDataStruct->lpData;
			//__DEBUG__("Announce stop receive", strContentsType);

			AnnounceMsg* msg = new AnnounceMsg();
			msg->announceId = strAnnounceId;
			msg->msgNo = 903;

			::PostMessageA(this->GetSafeHwnd(), 23456, NULL, (LPARAM)msg);
			break;
/*
			if(!m_bVerify)
			{
				__DEBUG__("DEMO key : ignore event", _NULL);
			}
			else
			{
				//skpark 2013.11.28 new announce
				ReadAnnounce(strAnnounceId,false);
				//ReadAnnounce(atoi(strContentsType));
			}//if
*/
		}
		break;
	case 9005:		//Coursor hook
		{
			if(m_bFullScreen)
			{
				MOUSEHOOKSTRUCT* pstMouse = (MOUSEHOOKSTRUCT*)pCopyDataStruct->lpData;
				PostMessage(WM_HOOK_MOUSE, pstMouse->pt.x, pstMouse->pt.y);
			}//if
		}
		break;
	case 1000:		//Template shortcut
		{
			__DEBUG__("Template shortcut : pCopyDataStruct->cbData", (int)pCopyDataStruct->cbData);
			__DEBUG__("Template shortcut : pCopyDataStruct->lpData", (char*)pCopyDataStruct->lpData);
			char* pszShortcut = NULL;
			int nSize = (pCopyDataStruct->cbData*sizeof(char));
			pszShortcut = (char*)malloc(nSize);
			memset(pszShortcut, 0x00, nSize);
			memcpy(pszShortcut, pCopyDataStruct->lpData, nSize);

			CString strShortcut;
			strShortcut.Format("%s", pszShortcut);
			free(pszShortcut);
			__DEBUG__("Template shortcut : Receive template shortcut key", strShortcut);
			strShortcut.Remove(_T(' '));
			strShortcut.MakeUpper();

			// skpark 2013.3.19  "END", "INSERT", "DELETE" 등이 오면 Volume 을 조정하고 끝낸다. 템플릿에 전달하지 않는다.
			if(strShortcut == "END")		{
				__DEBUG__("END Key pressed", strShortcut);
				bool bSound = UpdateSoundState();
				SetSoundValue(!bSound);	
				break;
			}
			if(strShortcut == "INSERT")		{
				__DEBUG__("INSERT Key pressed", strShortcut);
				m_libAudioControl.SetVolumeUp();
				break;
			}
			if(strShortcut == "DELETE")		{
				__DEBUG__("DELETE Key pressed", strShortcut);
				m_libAudioControl.SetVolumeDown();
				break;
			}
			// skpark 2013.3.20  "FLASH_INVOKE+문자열\n"  형태의 데이터가 오면 무조건 현재 돌고 있는 Flash 에 invoke 로 전달한다.
			if(strShortcut.Find(_T("FLASH_INVOKE"), 0) != -1)	{//CALL_FLASH key
				__DEBUG__("FLASH_INVOKE Key pressed", strShortcut);

				//strShortcut.Replace(_T("FLASH_INVOKE"), _T(""));
				//int nIdx = strShortcut.Find(_T("+"), 0);
				//if(nIdx != -1)
				//{
				//	strShortcut.Delete(0, nIdx+1);
				//}//if

				CString szBuf = "";
				szBuf += "<invoke name=\'FLASH_INVOKE\'><arguments>";
				szBuf += "<string>"+strShortcut+"</string>";
				szBuf += "</arguments></invoke>";

				__DEBUG__("FLASH_INVOKE CallFunction()", szBuf);

				// 여기서 Flash CallFunction 을 해야 한다.
				if(!m_pCurrentTemplate){
					__DEBUG__("FLASH_INVOKE ERROR CurrentTemplate missing()", szBuf);
					break;
				}
				CScheduleArray listSchedule;
				m_pCurrentTemplate->GetScheduleList(listSchedule);
				if(listSchedule.GetCount()==0){
					__DEBUG__("FLASH_INVOKE ERROR CurrentSchedule missing()", szBuf);
					break;
				}
				int matched = 0;
				CSchedule* pSchedule = NULL;
				for(int i=0; i<listSchedule.GetCount(); i++)
				{
					pSchedule = (CSchedule*)listSchedule.GetAt(i);
					if(!pSchedule) {
						continue;
					}
					ciLong	contentsType = pSchedule->GetContentsType();
					if(	contentsType != 	CONTENTS_FLASH ){
						continue;
					}
					// Flash founded;
					CFlashSchedule* flashSchedule = dynamic_cast<CFlashSchedule*>(pSchedule);
					if(flashSchedule == 0){
						// It's not flash
						continue;
					}
					if(flashSchedule->CallFunc(szBuf)){
						matched++;
						__DEBUG__("FLASH_INVOKE CallFunc() called succeed", szBuf);
					}else{
						__DEBUG__("FLASH_INVOKE CallFunc() called failed", szBuf);
					}
				}		
				char buf[10];
				sprintf(buf,"%d", matched);
				__DEBUG__("FLASH_INVOKE CallFunc() called times", buf);
				break;
			}
			
			// skpark end

			// <!-- 이어서 재생 (2016-08-29)
			UINT unKeyValue_resuming[2] = {0,'0'};
			CString str_resuming_shortcut = "";
			
			if( strShortcut == "CTRL+H" )
			{
				// Ctrl+H 단축키만 특별처리 (첫템플릿으로 이동하는 대신 이어서재생)
				__DEBUG__("Find Home Shortcut", strShortcut);
				str_resuming_shortcut = CTemplate::GetLastPlayTemplateShortCut();
				if( str_resuming_shortcut.GetLength() != 0)
				{
					// 이전에 재생한 템플릿의 단축키가 존재
					strShortcut = str_resuming_shortcut;
					unKeyValue_resuming[0] = strShortcut.Right(1).GetAt(0);
					if( unKeyValue_resuming[0]=='9' )
					{
						// 0~9 => A~Z
						unKeyValue_resuming[0] = 'A';
						__DEBUG__("next Resuming Shortcut", (int)unKeyValue_resuming[0]);
					}
					else
					{
						// 다음 문자
						unKeyValue_resuming[0]++;
						__DEBUG__("next Resuming Shortcut", (int)unKeyValue_resuming[0]);
					}
				}
			}
			// -->

			UINT unSyskey = 0, unKeyValue = 0;
			if(strShortcut.Find(_T("ALT"), 0) != -1)			//ALT key
			{
				unSyskey = VK_MENU;
				strShortcut.Replace(_T("ALT"), _T(""));
				int nIdx = strShortcut.Find(_T("+"), 0);
				if(nIdx != -1)
				{
					strShortcut.Delete(0, nIdx+1);
				}//if
			}
			else if(strShortcut.Find(_T("CTRL"), 0) != -1)	//CTRL key
			{
				unSyskey = VK_CONTROL;
				strShortcut.Replace(_T("CTRL"), _T(""));
				int nIdx = strShortcut.Find(_T("+"), 0);
				if(nIdx != -1)
				{
					strShortcut.Delete(0, nIdx+1);
				}//if
			}//if

			if(strShortcut.GetLength() == 1)
			{
				char cHot = strShortcut.GetAt(0);
				unKeyValue = cHot;
			}
			else if(strShortcut == "F1")		unKeyValue = VK_F1;
			else if(strShortcut == "F2")		unKeyValue = VK_F2;
			else if(strShortcut == "F3")		unKeyValue = VK_F3;
			else if(strShortcut == "F4")		unKeyValue = VK_F4;
			else if(strShortcut == "F5")		unKeyValue = VK_F5;
			else if(strShortcut == "F6")		unKeyValue = VK_F6;
			else if(strShortcut == "F7")		unKeyValue = VK_F7;
			else if(strShortcut == "F8")		unKeyValue = VK_F8;
			else if(strShortcut == "F9")		unKeyValue = VK_F9;
			else if(strShortcut == "F10")		unKeyValue = VK_F10;
			else if(strShortcut == "F11")		unKeyValue = VK_F11;
			else if(strShortcut == "F12")		unKeyValue = VK_F12;

			CTemplatePlay* pclsPlay = NULL;
			for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
			{
				pclsPlay = GetAtTemplatePlay(unIdx);
				if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL) continue;

				//각 template이 shortcut key가 설정되어 있는지 검사한다
				if((pclsPlay->m_pclsTemplate->GetSysShortcutKey() != unSyskey)
					|| (unKeyValue != pclsPlay->m_pclsTemplate->GetShortcutKey())) continue;

				if( str_resuming_shortcut.GetLength()==0 )
				{
					// 이전에 재생하던 템플릿의 단축키가 없음
					//지금 설정된 defaultTemplate와 같다면 변경하지 않는다.
					//if(m_defaultTemplate != pclsPlay->m_pclsTemplate->GetTemplateId())
					//{
						SetDefaultTemplatePlay(pclsPlay->m_pclsTemplate->GetTemplateId());
						__DEBUG__("Set shortcut template", pclsPlay->m_pclsTemplate->GetTemplateId());
					//}//if
					break;
				}

				__DEBUG__("Check RemainPlayingDefaultSchedule", pclsPlay->m_pclsTemplate->GetTemplateId());
				if(pclsPlay->m_pclsTemplate->IsRemainPlayingDefaultSchedule())
				{
					__DEBUG__("Set shortcut template", pclsPlay->m_pclsTemplate->GetTemplateId());
					SetDefaultTemplatePlay(pclsPlay->m_pclsTemplate->GetTemplateId());
					break;
				}

				bool find_resuming = false;
				for(int i=0; i<2 && find_resuming==false; i++)
				{
					__DEBUG__("find next shortcut template", (int)unKeyValue_resuming[i]);
					for(UINT j=0; j<GetCountOfTemplatePlayAry(); j++)
					{
						pclsPlay = GetAtTemplatePlay(j);
						if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL) continue;

						if((pclsPlay->m_pclsTemplate->GetSysShortcutKey() != unSyskey)
							|| (unKeyValue_resuming[i] != pclsPlay->m_pclsTemplate->GetShortcutKey())) continue;

						find_resuming = true;
						__DEBUG__("Set shortcut template", pclsPlay->m_pclsTemplate->GetTemplateId());
						SetDefaultTemplatePlay(pclsPlay->m_pclsTemplate->GetTemplateId());
						break;
					}
				}
			}//for
		}
		break;

	case UBC_WM_BRW_GET_DOWNLOAD_STATUS:
		// write current_package_name to UBCBrowser.ini
		__DEBUG__("Current Package Name", ::GetHostName());
		WriteINIValue("UBCBrowser.ini", GetAppName(), "Current_Package_Name", ::GetHostName());
		WriteINIValue("UBCBrowser.ini", GetAppName(), "Current_Package_Status", ::ToString(m_nDownloadStatus) );

		return m_nDownloadStatus;
		break;

	}//switch

	return CDialog::OnCopyData(pWnd, pCopyDataStruct);
}

LRESULT CHost::OnAnnounceMsg(WPARAM wParam, LPARAM lParam)
{
	AnnounceMsg* msg = (AnnounceMsg*)lParam;
	CString strAnnounceId = msg->announceId;
	int msgNo = msg->msgNo;
	delete msg;

	switch(msgNo)
	{
		case 902:
		{
			if(!m_bVerify)
			{
				__DEBUG__("DEMO key : ignore event", _NULL);
			}
			else
			{
				//skpark 2013.11.28 new announce
				ReadAnnounce(strAnnounceId, true);
				//ReadAnnounce(atoi(strContentsType));
				//긴급공지 시작
				if(m_pdlgAnnounce)
				{
					if(!m_pdlgAnnounce->IsPlay())
					{
						__DEBUG__("Start announce play", _NULL);
						m_pdlgAnnounce->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_image)
				{
					if(!m_pdlgAnnounce_image->IsPlay())
					{
						__DEBUG__("Start announce_image play", _NULL);
						m_pdlgAnnounce_image->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_video)
				{
					if(!m_pdlgAnnounce_video->IsPlay())
					{
						__DEBUG__("Start announce_video play", _NULL);
						m_pdlgAnnounce_video->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_flash)
				{
					if(!m_pdlgAnnounce_flash->IsPlay())
					{
						__DEBUG__("Start announce_flash play", _NULL);
						m_pdlgAnnounce_flash->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_ppt)
				{
					if(!m_pdlgAnnounce_ppt->IsPlay())
					{
						__DEBUG__("Start announce_ppt play", _NULL);
						m_pdlgAnnounce_ppt->StartAnnouncePlay();
					}//if
				}//if
				if(m_pdlgAnnounce_web)
				{
					if(!m_pdlgAnnounce_web->IsPlay())
					{
						__DEBUG__("Start announce_web play", _NULL);
						m_pdlgAnnounce_web->StartAnnouncePlay();
					}//if
				}//if

			}//if
		}
		break;
		case 903 :
		{
			if(!m_bVerify)
			{
				__DEBUG__("DEMO key : ignore event", _NULL);
			}
			else
			{
				//skpark 2013.11.28 new announce
				ReadAnnounce(strAnnounceId,false);
				//ReadAnnounce(atoi(strContentsType));
			}//if
		}
		break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램이 위치하는 좌표와 영역을 설정 \n
/// @param (int) nX : (in) 프로그램이 위치하는 X 좌표
/// @param (int) nY : (in) 프로그램이 위치하는 Y 좌표
/// @param (int) nCx : (in) 프로그램의 넓이
/// @param (int) nCy : (in) 프로그램의 높이
/// @param (int) nMonitorIndex : (in) 프로그램이 위치하는 모니터의 인덱스
/////////////////////////////////////////////////////////////////////////////////
void CHost::SetAppPosition(int nX, int nY, int nCx, int nCy, int nMonitorIndex)
{
	CString strAppName = GetAppName();
	if(atoi(GetINIValue("UBCBrowser.ini", strAppName, "UsePosition")))
	{
		int nWidth, nHeight;
		m_nPosX	=	atoi(GetINIValue("UBCBrowser.ini", strAppName, "PosX"));
		m_nPosY	=	atoi(GetINIValue("UBCBrowser.ini", strAppName, "PosY"));
		nWidth	=	atoi(GetINIValue("UBCBrowser.ini", strAppName, "Width"));
		nHeight	=	atoi(GetINIValue("UBCBrowser.ini", strAppName, "Height"));
		if(nWidth != 0)
		{
			m_nCx = nWidth;
		}
		else
		{
			m_nCx = nCx;
		}//if

		if(nHeight != 0)
		{
			m_nCy = nHeight;
		}
		else
		{
			m_nCy = nCy;
		}//if
	}
	else
	{
		m_nPosX	=	nX;
		m_nPosY	=	nY;
		m_nCx	=	nCx;
		m_nCy	=	nCy;
	}//if
	m_nMonitorIndex	= nMonitorIndex;

	CString str;
	str.Format("PosX=%d, PosY=%d, Cx=%d, Cy=%d, MonIdx=%d", m_nPosX, m_nPosY, m_nCx, m_nCy, nMonitorIndex);
	__DEBUG__(str, _NULL);

	m_rectMenuBar.top		= m_nPosY + (m_nCy - HEIGHT_MENU_BAR);
	m_rectMenuBar.left		= m_nPosX;
	m_rectMenuBar.bottom	= m_rectMenuBar.top + HEIGHT_MENU_BAR;
	m_rectMenuBar.right		= m_rectMenuBar.left + m_nCx;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램이 위치하는 좌표와 영역을 반환 \n
/// @param (int&) nX : (out) 프로그램이 위치하는 X 좌표
/// @param (int&) nY : (out) 프로그램이 위치하는 Y 좌표
/// @param (int&) nCx : (out) 프로그램의 넓이
/// @param (int&) nCy : (out) 프로그램의 높이
/////////////////////////////////////////////////////////////////////////////////
void CHost::GetAppPosition(int& nX, int& nY, int& nCx, int& nCy)
{
	nX	= m_nPosX;
	nY	= m_nPosY;
	nCx	= m_nCx;
	nCy	= m_nCy;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Grade가 1인 컨텐츠의 사운드를 On/Off 한다\n
/// @param (bool) bSound : (in) true : SoundOn, false : SoundOff
/////////////////////////////////////////////////////////////////////////////////
void CHost::SetSoundValue(bool bSound)
{
	__DEBUG__("Set sound value", bSound);
	m_bSound = bSound;
	//ini 파일에 Sound On/Off 를 기록한다.
	CString strSnd, strMon;
	strMon.Format("%d", m_nMonitorIndex);
	strSnd.Format("%d", (int)m_bSound);

	WriteINIValue("UBCBrowser.ini", GetAppName(), "monitor", strMon);
	WriteINIValue("UBCBrowser.ini", GetAppName(), "sound", strSnd);
	
	if(m_bSound)
	{
		//Sound On 일때 오디오 믹서가 Mute로 되어있다면...
		BOOL bAudio = m_libAudioControl.IsMute();
		__DEBUG__("System mixer mute value", bAudio);
		if(bAudio)
		{
			__DEBUG__("Set system mixer mute off", _NULL);
			if(!m_libAudioControl.SetMute(FALSE))
			{
				__DEBUG__("Set system mixer mute off fail", _NULL);
			}//if
		}//if
	}
	else
	{
		//UbcVariables.ini의 MONITOR_COUNT값이 2라면 브라우져가 2개가 구동될수 있다.
		//이경우에는 UBCBrowser.ini의 각 브라우져의 sound값이  모두 0일때 시스템 사운드 믹서를 뮤트시킨다.
		//MONITOR_COUNT값이 1이라면 브라우져는 한개만 구동될 수 있으므로 sound값이 0이면 시스템 사운드 믹서를 뮤트시킨다.
		CString strMonCount = GetINIValue("UbcVariables.ini", "ROOT", "MONITOR_COUNT");
		if(strMonCount == "2")
		{
			int nSound1 = atoi(GetINIValue("UBCBrowser.ini", "UTV_brwClient2", "sound"));
			int nSound2 = atoi(GetINIValue("UBCBrowser.ini", "UTV_brwClient2_UBC1", "sound"));
			if(nSound1 == 0 && nSound2 == 0)
			{
				__DEBUG__("Sound1", nSound1);
				__DEBUG__("Sound2", nSound2);
				__DEBUG__("Set system mixer mute on", _NULL);
				if(!m_libAudioControl.SetMute(TRUE))
				{
					__DEBUG__("Set system mixer mute on fail", _NULL);
				}//if
			}//if
		}
		else
		{
			__DEBUG__("Set system mixer mute on", _NULL);
			if(!m_libAudioControl.SetMute(TRUE))
			{
				__DEBUG__("Set system mixer mute on fail", _NULL);
			}//if
		}//if


/*
		if(::GetSystemMetrics(SM_CMONITORS) == 1)
		{
			__DEBUG__("Set system mixer mute on", _NULL);
			if(!m_libAudioControl.SetMute(TRUE))
			{
				__DEBUG__("Set system mixer mute on fail", _NULL);
			}//if
		}//if
*/
	}//if

	CTemplatePlay* pclsPlay = NULL;
	for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
	{
		pclsPlay = GetAtTemplatePlay(unIdx);
		if(pclsPlay && pclsPlay->m_pclsTemplate)
		{
			//Scheduel에서는  Sound On/Off 값이 아니라 Mute On/Off 값으로 다루어 진다.
			pclsPlay->m_pclsTemplate->PostMessage(WM_TOGGLE_SOUND_VOLUME_MUTE, (WPARAM)!m_bSound, 0);
		}//if
	}//for

	//메뉴바가 보여지는 상태라면 메뉴바의 사운드 버튼 이미지를 바꾸어 준다.
	if(m_pdlgMenuBar && m_pdlgMenuBar->GetVisible())
	{
		m_pdlgMenuBar->SetSoundMuteImage();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// BRW의 화면의 크기와 위치를 초기화 한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::InitWndPostion()
{
	if(m_nCy == 0)
	{
		m_nCy = 768;
	}//if

	int nWidth, nHeight, nGapX, nGapY;
	if(ciArgParser::getInstance()->isSet("+download_only") || !m_bLogoMode)
	{
		nWidth = 585 + (DLG_FRM_SIZE*2);		//원본 이미지 크기 + Dialog의 프레임 사이즈
		nHeight = 284 + (DLG_FRM_SIZE*2);		//원본 이미지 크기 + Dialog의 프레임 사이즈

		if(ciArgParser::getInstance()->isSet("+download_only"))
		{
			ModifyStyle(NULL, WS_CAPTION | WS_SYSMENU /*| WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_SIZEBOX*/);
			int nScreenX = GetSystemMetrics(SM_CXSCREEN);
			int nScreenY = GetSystemMetrics(SM_CYSCREEN);
			MoveWindow(nScreenX-nWidth, nScreenY-nHeight, nWidth, nHeight);
		}
		else
		{
			if(m_nCx > nWidth)
			{
				nGapX = (m_nCx-nWidth)/2; 
			}
			else
			{
				nGapX = 0;
			}//if

			if(m_nCy > nHeight)
			{
				nGapY = (m_nCy-nHeight)/10; 
			}
			else
			{
				nGapY = 0;
			}//if

			MoveWindow(m_nPosX+nGapX, m_nPosY+nGapY, nWidth, nHeight);
		}//if
		m_wndBack.MoveWindow(0, 0, nWidth, nHeight);
		m_bFullScreen = false;
	}
	else
	{
		//밴더의 로고 파일이 있는경우는 Full screen으로 밴더의 logo 이미지를
		//초기 로딩과정에 보여준다
		nWidth = m_nCx;
		nHeight = m_nCy;
		nGapX = 0;
		nGapY = 0;

		ModifyStyle(WS_CAPTION | WS_SYSMENU | WS_SIZEBOX, NULL);
		MoveWindow(m_nPosX, m_nPosY, m_nCx, m_nCy);
		m_bFullScreen = true;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램을 topmost로 설정한다. \n
/// @param (bool) bTopMost : (in) 창을 topmost로 설정하는지 여부
/////////////////////////////////////////////////////////////////////////////////
void CHost::SetTopMost(bool bTopMost)
{
//#ifndef _DEBUG
	if(bTopMost)
	{
		int no_topmost = atoi( GetINIValue("UBCBrowser.ini", "ROOT", "NO_TOPMOST") );
		HWND hwnd_mode = ( no_topmost ? HWND_NOTOPMOST : HWND_TOPMOST );

		__DEBUG__("Player set Topmost mode", (no_topmost ? "NOTOPMOST": "TOPMOST") );
		BringWindowToTop();
		SetForegroundWindow();
		SetFocus();
		::SetWindowPos(this->m_hWnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED | SWP_SHOWWINDOW);
		//if(!SetWindowPos(&wndTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED | SWP_SHOWWINDOW))
		//{
		//	__DEBUG__("Set Topmost fail", _NULL);
		//}//if

		if(m_bShowMenuButton && m_dlgMenuBtn.GetSafeHwnd())
		{
			m_dlgMenuBtn.ShowWindow(SW_SHOW);
		}//if
	}
	else
	{
		__DEBUG__("Player set noTopmost mode", _NULL);
		//SetWindowPos(&wndNoTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
		::SetWindowPos(this->m_hWnd, HWND_NOTOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
	}//if
//#endif
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// F12 key를 눌렀을때 작아진 화면으로 변경함 \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::MoveToSmallWnd()
{
	int nGapX, nGapY;
	int nWidth = 585 + (DLG_FRM_SIZE*2);		//원본 이미지 크기 + Dialog의 프레임 사이즈
	int nHeight = 284 + (DLG_FRM_SIZE*2);		//원본 이미지 크기 + Dialog의 프레임 사이즈

	if(m_nCx > nWidth)
	{
		nGapX = (m_nCx-nWidth)/2; 
	}
	else
	{
		nGapX = 0;
	}//if

	if(m_nCy > nHeight)
	{
		nGapY = (m_nCy-nHeight)/10; 
	}
	else
	{
		nGapY = 0;
	}//if

	MoveWindow(m_nPosX+nGapX, m_nPosY+nGapY, nWidth, nHeight);
	m_bFullScreen = false;
}

void CHost::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	CRect rectClient;
	GetClientRect(&rectClient);
	if(m_wndBack.GetSafeHwnd() && m_wndBack.IsWindowVisible())
	{
		m_wndBack.MoveWindow(rectClient.left, rectClient.top, rectClient.Width(), rectClient.Height());
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// +replace 옵션으로 초기화후에 다른 프로그램을 종료시킨다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::ProcessReplace()
{
	if(m_strTerminateApp == "")
	{
		//SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
		return;
	}//if

	//먼저 자기자신을 topmost로 설정한다.
	m_bTopMostMode = true;
	SetTopMost(m_bTopMostMode);

	//자기자신과 핸들이 틀린 프로세서를 찾아서 종료시킨다.
	__DEBUG__("TerminateApp", m_strTerminateApp);
	int nMyPid = _getpid();
	__DEBUG__("My PID", nMyPid);
	scratchUtil* aUtil = scratchUtil::getInstance();
	int nRet = aUtil->ExclusiveKillProcess(m_strTerminateApp, (ULONG)nMyPid);
	__DEBUG__("ExclusiveKillProcess", nRet);

	//화면에 프로그램을 보여준다.
	Sleep(500);
	ShowWindow(SW_SHOW);
	SetForegroundWindow();
	SetTopMost(m_bTopMostMode);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Template play list를 반환 \n
/// @return <형: CString> \n
///			<Template play list> \n
/////////////////////////////////////////////////////////////////////////////////
CString CHost::GetTemplatePlayList()
{
	return m_templatePlayList.c_str();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램 시작전에 UI와 Arg등 기타 설정을 적용 \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::ProcessStartup()
{
	///////////////////////////////////////////////////////////
	// 화면 크기 조정
	int width, height;
	if(ciArgParser::getInstance()->isSet("+window"))
	{
		__DEBUG__("Window Mode", _NULL);
		// 창모드 1024 x 768 출력
		ModifyStyle(NULL, WS_CAPTION | WS_SYSMENU);
		width = 1024 + GetSystemMetrics(SM_CXDLGFRAME)*2;
		height = 960 + GetSystemMetrics(SM_CYCAPTION) + GetSystemMetrics(SM_CYDLGFRAME)*2;
		MoveWindow(m_nPosX,m_nPosY, width, height);

		m_bFullScreen = false;
	}
	else if(ciArgParser::getInstance()->isSet("+download_only"))
	{
		__DEBUG__("download_only Mode", _NULL);
		//download_only
		m_bTopMostMode = true;
		SetTopMost(m_bTopMostMode);

		CString strPackageFileMsg, strContentsFileMsg;
		if(m_bPackageDown)
		{
			strPackageFileMsg.Format("Package file [ %s ] download complete, ", ::GetHostName()); 
		}
		else
		{
			strPackageFileMsg.Format("Package file [ %s ] download fail, ", ::GetHostName());
		}//if

		if(m_nFailCount != 0)
		{
			//다운로드에 실패한 파일이 있다면 대기상태로....
			strContentsFileMsg.Format("%d contents file(s) fail to download (%d/%d)", m_nFailCount, m_nTotalCount-m_nFailCount, m_nTotalCount);
		}
		else
		{
			strContentsFileMsg.Format("Contents file(s) download complete (%d/%d)", m_nTotalCount, m_nTotalCount);
		}//if

		m_wndBack.AddLog(strPackageFileMsg + strContentsFileMsg);
		__DEBUG__("download_only Mode end", _NULL);
		return;
	}
	else
	{
		__DEBUG__("Fullscreen Mode", _NULL);
		//// 전체화면으로 출력
		//ModifyStyle(WS_CAPTION | WS_SYSMENU | WS_MAXIMIZEBOX | WS_SIZEBOX, NULL);
		//MoveWindow(m_nPosX, m_nPosY, m_nCx, m_nCy);
		//m_bTopMostMode = true;
		//SetTopMost(m_bTopMostMode);
		//m_bFullScreen = true;

		MoveToSmallWnd();
		m_bFullScreen = false;
		//ToggleFullScreen();
		ToggleFullScreen();
	}//if

	///////////////////////////////////////////////////////////
	//sound mute 설정
	if(ciArgParser::getInstance()->isSet("+mute"))
	{
		SetSoundValue(false);
	}
	else
	{
		//ini 파일에서 Sound On/Off값을 가져온다
		bool bSound = UpdateSoundState();
		SetSoundValue(bSound);
	}//if

	//긴급공지 시작
	if(m_pdlgAnnounce)
	{
		if(!m_pdlgAnnounce->IsPlay())
		{
			__DEBUG__("Check Announce", _NULL);
			m_pdlgAnnounce->StartAnnouncePlay();
		}//if
	}//if
	//긴급공지 시작
	if(m_pdlgAnnounce_image)
	{
		if(!m_pdlgAnnounce_image->IsPlay())
		{
			__DEBUG__("Check Announce_contents", _NULL);
			m_pdlgAnnounce_image->StartAnnouncePlay();
		}//if
	}//if
	//긴급공지 시작
	if(m_pdlgAnnounce_video)
	{
		if(!m_pdlgAnnounce_video->IsPlay())
		{
			__DEBUG__("Check Announce_video", _NULL);
			m_pdlgAnnounce_video->StartAnnouncePlay();
		}//if
	}//if
	//긴급공지 시작
	if(m_pdlgAnnounce_flash)
	{
		if(!m_pdlgAnnounce_flash->IsPlay())
		{
			__DEBUG__("Check Announce_flash", _NULL);
			m_pdlgAnnounce_flash->StartAnnouncePlay();
		}//if
	}//if
	if(m_pdlgAnnounce_ppt)
	{
		if(!m_pdlgAnnounce_ppt->IsPlay())
		{
			__DEBUG__("Check Announce_ppt", _NULL);
			m_pdlgAnnounce_ppt->StartAnnouncePlay();
		}//if
	}//if
	if(m_pdlgAnnounce_web)
	{
		if(!m_pdlgAnnounce_web->IsPlay())
		{
			__DEBUG__("Check Announce_web", _NULL);
			m_pdlgAnnounce_web->StartAnnouncePlay();
		}//if
	}//if

	//메뉴버튼
	if(m_bShowMenuButton)
	{
		m_dlgMenuBtn.ShowWindow(SW_SHOW);
	}
	else
	{
		m_dlgMenuBtn.ShowWindow(SW_HIDE);
	}//if

	//Replace 옵션 처리
	__DEBUG__("TerminateApp", m_strTerminateApp);
	if(m_strTerminateApp.GetLength() != 0)
	{
		ProcessReplace();
	}//if

//#ifndef _DEBUG
	//마우스 커서 
	if(ciArgParser::getInstance()->isSet("+mouse"))
	{
		__DEBUG__("Show Mouse Cursor Mode", _NULL);
		StartCoursorHook();
	}
	else
	{
		__DEBUG__("Hide Mouse Cursor Mode", _NULL);
		SetShowCursor(false);
	}//if
//#endif
/*
	//자원 모니터 thread 시작
	CWinThread* pThread = ::AfxBeginThread(CHost::ThreadMonitorResource, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;
	m_bExitResourceThread = false;
	m_hEvtThreadResource = CreateEvent(NULL, FALSE, FALSE, NULL);
	pThread->ResumeThread();
*/
	m_bInitialize = true;
	m_nStateBRW = E_BRW_PLAYING;		//방송 시작
	m_wndBack.ShowWindow(SW_HIDE);
	SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
	if(::GetScheduleResuming())
		SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL);	// <!-- 이어서 재생 (2016-08-29) -->
	__DEBUG__("ProcessStartup End", _NULL);

}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Time dialog를 보여준다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::ViewTimeDialog()
{
	if(m_timeDialog.IsWindowVisible())
	{
		m_timeDialog.ShowWindow(SW_HIDE);
	}
	else
	{
		m_timeDialog.m_bConnectNetwork = !m_bLoadFromLocal;
		if(m_timeDialog.GetTemplateId() == "")
		{
			m_timeDialog.SetTemplateId(m_defaultTemplate.c_str());
		}//if

		CRect clsParentRect, clsTimeDialogRect;
		m_timeDialog.GetWindowRect(&clsTimeDialogRect);
		this->GetWindowRect(&clsParentRect);
		m_timeDialog.MoveWindow(clsParentRect.left+10, clsParentRect.top+10, clsTimeDialogRect.Width(), clsTimeDialogRect.Height());
		m_timeDialog.ShowWindow(SW_SHOW);
	}//if
}
void CHost::ViewLicense()
{
	if(m_licenseDialog.IsWindowVisible())
	{
		m_licenseDialog.ShowWindow(SW_HIDE);
	}
	else
	{
//		m_licenseDialog.m_bConnectNetwork = !m_bLoadFromLocal;
//		if(m_licenseDialog.GetTemplateId() == "")
//		{
//			m_licenseDialog.SetTemplateId(m_defaultTemplate.c_str());
//		}//if

		CRect clsParentRect, clsTimeDialogRect;
		m_licenseDialog.GetWindowRect(&clsTimeDialogRect);
		this->GetWindowRect(&clsParentRect);
		m_licenseDialog.MoveWindow(clsParentRect.left+10, clsParentRect.top+10, clsTimeDialogRect.Width(), clsTimeDialogRect.Height());
		m_licenseDialog.ShowWindow(SW_SHOW);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Full screen/Window 상태를 전환한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::ToggleFullScreen()
{
	if(m_bFullScreen)
	{
		ModifyStyle(NULL, WS_CAPTION | WS_SYSMENU | WS_MAXIMIZEBOX | WS_SIZEBOX);
		MoveToSmallWnd();
		m_bTopMostMode = false;
		SetTopMost(m_bTopMostMode);

		if(m_pdlgMenuBar)
		{
			m_pdlgMenuBar->SetFullScreenImage();
		}//if

		if(m_bShowMenuButton)
		{
			m_dlgMenuBtn.ShowWindow(SW_SHOW);
		}//if

//#ifndef _DEBUG
		//마우스 커서 
		if(ciArgParser::getInstance()->isSet("+mouse"))
		{
			__DEBUG__("StopCoursorHook", _NULL);
			StopCoursorHook();
		}
		else
		{
			__DEBUG__("Show mouse cursor", _NULL);
			SetShowCursor(true);
		}//if
//#endif
	}
	else
	{
		ModifyStyle(WS_CAPTION | WS_SYSMENU | WS_MAXIMIZEBOX | WS_SIZEBOX, NULL);
		MoveWindow(m_nPosX, m_nPosY, m_nCx, m_nCy);
		m_bTopMostMode = true;
		SetTopMost(m_bTopMostMode);
		m_bFullScreen = true;

		if(m_pdlgMenuBar)
		{
			m_pdlgMenuBar->SetFullScreenImage();
		}//if

		if(m_bShowMenuButton)
		{
			m_dlgMenuBtn.ShowWindow(SW_SHOW);
		}//if

//#ifndef _DEBUG
		//마우스 커서 
		if(ciArgParser::getInstance()->isSet("+mouse"))
		{
			__DEBUG__("StartCoursorHook", _NULL);
			StartCoursorHook();
		}
		else
		{
			__DEBUG__("Hide mouse cursor", _NULL);
			SetShowCursor(false);
		}//if
//#endif
	}
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Full screen/Window 상태를 반환한다. \n
/// @return <형: bool> \n
///			<true: Full screen mode> \n
///			<false: Window mode> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::GetFullScreen()
{
	return m_bFullScreen;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Sound On/Off 상태를 갱신하여 반환한다. \n
/// @return <형: bool> \n
///			<true: Sound On> \n
///			<false: Sound Off> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::UpdateSoundState()
{
	CString strSound = GetINIValue("UBCBrowser.ini", GetAppName(), "sound");
	m_bSound = (bool)atoi(strSound);
	__DEBUG__("Sound value", m_bSound);

	bool bMute = m_libAudioControl.IsMute();
	__DEBUG__("System mixer", bMute);

	return m_bSound;
}


void CHost::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch(nID)
	{
	case SC_MAXIMIZE:
		{
			ToggleFullScreen();
			return;
		}
		break;
	case SC_CLOSE:
		{
			__DEBUG__("SC_CLOSE", _NULL);
			PostMessage(WM_SEND_STOP_BRW, 0, 0);
			return;
		}
		break;
	}//switch

	CDialog::OnSysCommand(nID, lParam);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// CTRL key가 눌렸을 때 처리를 한다. \n
/// @param (MSG*) pMsg : (in) Key 이벤트 메시지
/////////////////////////////////////////////////////////////////////////////////
void CHost::ProcessCTRLKey(MSG* pMsg)
{
	switch(pMsg->wParam)
	{
	case 'Q':		//BRW 시작/정지
		{
			if(m_bInitialize)
			{
				StopBRW();
				SetForegroundWindow();
			}
			else
			{
				StartBRW();
				SetForegroundWindow();
			}//if
			return;
		}
		break;
	case 'M':
		{
			m_bShowMenuButton = !m_bShowMenuButton;
			if(m_bShowMenuButton)
			{
				m_dlgMenuBtn.SetShowImage(true);
				m_dlgMenuBtn.ShowWindow(SW_SHOW);
				this->SetForegroundWindow();

				if(!installUtil::getInstance()->setMenuOption(true))
				{
					__DEBUG__("Has failed to show the menu bar to write on", _NULL); 
				}
				else
				{
					__DEBUG__("Show menu bar", _NULL);
				}//if
			}
			else
			{
				if(m_pdlgMenuBar && m_pdlgMenuBar->GetVisible())
				{
					m_pdlgMenuBar->HideMenuBar();
				}//if

				m_dlgMenuBtn.SetShowImage(false);
				m_dlgMenuBtn.ShowWindow(SW_HIDE);
				this->SetForegroundWindow();

				if(!installUtil::getInstance()->setMenuOption(false))
				{
					__DEBUG__("Has failed to hide the menu bar to write on", _NULL); 
				}
				else
				{
					__DEBUG__("Hide menu bar", _NULL);
				}//if
			}//if
			return;
		}
		break;
	}//switch

	CTemplatePlay* pclsPlay = NULL;
	for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
	{
		pclsPlay = GetAtTemplatePlay(unIdx);
		if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL)
		{
			continue;
		}//if

		//각 template이 CTRL+# key로 shortcut key가 설정되어 있는지 검사한다
		if((pclsPlay->m_pclsTemplate->GetSysShortcutKey() == VK_CONTROL)
			&& (pMsg->wParam == pclsPlay->m_pclsTemplate->GetShortcutKey()))
		{
			//지금 설정된 defaultTemplate와 같다면 변경하지 않는다.
			//if(m_defaultTemplate != pclsPlay->m_pclsTemplate->GetTemplateId())
			//{
				SetDefaultTemplatePlay(pclsPlay->m_pclsTemplate->GetTemplateId());
			//}//if
			break;
		}//if
	}//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 인증이 안되었음을 공지로 알린다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::MakeTrialAnnounce()
{
	//+template의 preview 에서는 나오지 않도록 한다.
	if(m_strArgTemplateID.GetLength() != 0)
	{
		return;
	}//if

	CAnnounce* pAnnounce = new CAnnounce;

	pAnnounce->m_strAnnounceId = "TrialAnnounce";
	pAnnounce->m_strTitle = "TrialAnnounce";
	//pAnnounce->m_tmStartTime;
	//pAnnounce->m_tmEndTime;
	
	pAnnounce->m_lPosition = 0;		//바닥
	pAnnounce->m_sHeight = 50;
	pAnnounce->m_strComment[0] = "Powered by UBC.";
	pAnnounce->m_strComment[1] = "Copyright by SQISoft.";
	//pAnnounce->m_strComment[0] = "Trial version";
	//pAnnounce->m_strComment[1] = "Registration required to use this software.";
	pAnnounce->m_strFont = "Thaoma";
	pAnnounce->m_sFontSize = 30;
	pAnnounce->m_strFgColor = "#0000FF";
	pAnnounce->m_strBgColor = "#FF0000";
	//pAnnounce->m_strBgColor = "0x000000";
	pAnnounce->m_sPlaySpeed = 2;
	pAnnounce->m_sAlpha = 40;

	pAnnounce->m_nWidth = m_nCx;

	if(!m_pdlgAnnounce)
	{
		m_pdlgAnnounce = new CAnnounceDlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
		BOOL ret = m_pdlgAnnounce->Create(IDD_ANNOUNCE_DLG, this);
	}//if

	m_pdlgAnnounce->AddAnnounce(pAnnounce);
}


void CHost::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	lpMMI->ptMaxSize.x = m_nCx;
	lpMMI->ptMaxSize.y = m_nCy;
	lpMMI->ptMaxTrackSize.x = m_nCx;
	lpMMI->ptMaxTrackSize.y = m_nCy;

	CDialog::OnGetMinMaxInfo(lpMMI);
}


LRESULT CHost::OnMouseHook(WPARAM wParam, LPARAM lParam)
{
	if(!m_bViewMouse)
	{
		__DEBUG__("Show cursor", _NULL);
		SetShowCursor(true);
	}//if
	m_dwMouseStart = GetTickCount();

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Coursor hook를 시작한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::StartCoursorHook()
{
#ifdef _DEBUG
	return;
#endif

	if(m_hHookDll)
	{
		FreeLibrary(m_hHookDll);
		m_hHookDll = NULL;
	}//if

#ifdef _DEBUG
	m_hHookDll = LoadLibrary("HookDll_d.dll");
#else
	m_hHookDll = LoadLibrary("HookDll.dll");
#endif
	
	if(!m_hHookDll)
	{
		__ERROR__("Load HookDll.dll fail.", _NULL);
		return;
	}//if

	FARPROC pFunc = NULL;
	pFunc = GetProcAddress(m_hHookDll, "SetHook");
	if(!pFunc)
	{
		__ERROR__("Fail to get SetHook proc address.", _NULL);
		FreeLibrary(m_hHookDll);
		return;
	}//if

	lpProcSetHook pSetHook = NULL;
	pSetHook = (lpProcSetHook)pFunc;

	if(!pSetHook(this->GetSafeHwnd()))
	{
		__ERROR__("Mouse hook start fail.", _NULL);
		FreeLibrary(m_hHookDll);
		return;
	}//if
	__DEBUG__("Mouse hook begin", _NULL);

	SetShowCursor(true);
	m_dwMouseStart = GetTickCount();
	SetTimer(TIMER_COURSOR_HIDE, TIME_COURSOR_HIDE, 0);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Coursor hook를 종료한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::StopCoursorHook(void)
{
#ifdef _DEBUG
	return;
#endif

	KillTimer(TIMER_COURSOR_HIDE);

	if(!m_hHookDll)
	{
		__DEBUG__("Library HookDll.dll not loded.", _NULL);
		return;
	}//if

	FARPROC pFunc = NULL;
	pFunc = GetProcAddress(m_hHookDll, "RemoveHook");
	if(!pFunc)
	{
		__ERROR__("Fail to get RemoveHook proc address.", _NULL);
		return;
	}//if

	lpProcRemoveHook pRemoveHook = NULL;
	pRemoveHook = (lpProcRemoveHook)pFunc;

	if(!pRemoveHook())
	{
		__ERROR__("Mouse hook stop fail.", _NULL);
		return;
	}//if
	__DEBUG__("Mouse hook end", _NULL);

	if(!FreeLibrary(m_hHookDll))
	{
		__ERROR__("Free Library fail.", _NULL);
		return;
	}//if

	SetShowCursor(true);
	m_hHookDll = NULL;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Cursor의 vissible 상태를 설정한다. \n
/// @param (bool) bVisible : (in) Cursor Show/Hide flag
/////////////////////////////////////////////////////////////////////////////////
void CHost::SetShowCursor(bool bVisible)
{
	int nRet = 0;
	if(bVisible)	//Show cursor
	{
		if(!m_bViewMouse)
		{
			do
			{
				//Counter가 0이거나 이보다 커야 cursor가 보인다.
				nRet = ShowCursor(TRUE);
			} while(nRet < 0);
		}//if
		__DEBUG__("Cursor show", nRet);
		m_bViewMouse = true;
	}
	else			//Hide cursor
	{
		if(m_bViewMouse)
		{
			do
			{
				//Counter가 0보다 작아야 cursor가 숨겨진다.
				nRet = ShowCursor(FALSE);
			} while(nRet >= 0);
		}//if
		__DEBUG__("Cursor hide", nRet);
		m_bViewMouse = false;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Audio volume을 올린다.\n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::IncreaseVolume()
{
	if(!m_libAudioControl.SetVolumeUp())
	{
		__DEBUG__("Fail to IncreaseVolume", _NULL);
		return false;
	}//if

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Audio volume을 내린다.\n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::DecreaseVolume()
{
	if(!m_libAudioControl.SetVolumeDown())
	{
		__DEBUG__("Fail to DecreaseVolume", _NULL);
		return false;
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 시간까지 BRW를 중단시킨다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::PauseBRW()
{
	time_t tmNow;
	time(&tmNow);

	CTime tmPause(m_tmPausTime);
	CString strTmPause = tmPause.Format("%Y-%m-%d %H:%M:%S");

	//CTime tmEnd(tmNow);
	CTime tmEnd = CTime::GetCurrentTime();
	CString strTmNow = tmEnd.Format("%Y-%m-%d %H:%M:%S");

	__DEBUG__("Reservation time", strTmPause);
	__DEBUG__("Current time", strTmNow);

	//시간이 과거이거나 현재시간이라면 대기하지 않는다.
	if(m_tmPausTime <= tmNow)
	{
		__DEBUG__("BRW start", _NULL);
		ProcessStartup();
	__DEBUG__("After ProcessStartup", _NULL);
		//ProcessReplace();
		//SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
		return;
	}//if

	CString strMsg;
	strMsg.Format("The schedule [%s] will be played at %s", ::GetHostName(), strTmPause);

	// skpark 2013.2.26 error Msg 를 Manager 까지 전달하기 위해 skpark_manager_err
	m_errScheduleId = "#pauseBrowser";
	this->m_strStateMsg = strMsg;

	__DEBUG__(strMsg, m_tmPausTime);
	m_wndBack.AddLog(strMsg);
	m_wndBack.ShowWindow(SW_SHOW);

	m_pThreadPauseBRW = ::AfxBeginThread(ThreadPauseBRW, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_pThreadPauseBRW->m_bAutoDelete = TRUE;
	m_bExitPauseBRWThread = false;
	m_hEventThreadPauseBRW = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_pThreadPauseBRW->ResumeThread();
}


LRESULT CHost::OnPauseBRW(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__("Receive OnPauseBRW", _NULL);

	// skpark 2013.2.26 pauseBrowser ErrMsg 를 clear 하기 위해  skpark_manager_err
	if(m_errScheduleId.CompareNoCase("#pauseBrowser")==0){
		this->m_strStateMsg = "";
		m_errScheduleId = "";
	}
	ProcessStartup();
	__DEBUG__("After ProcessStartup", _NULL);

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// BRW를 지정된 시간까지 중지 시키는 thread \n
/// @param (LPVOID) pParam : (in) thread parameter
/// @return <형: UINT> \n
///			<값: 1> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CHost::ThreadPauseBRW(LPVOID pParam)
{
	DWORD dwEventRet = 0;
	CHost* pParent = (CHost*)pParam;

	CTime tmPause(pParent->m_tmPausTime);
	CString strTmPause = tmPause.Format("%Y-%m-%d %H:%M:%S");
	__DEBUG__("Reservation time", strTmPause);

	for(int counter=0;;counter++) { 

		time_t tmNow;
		time(&tmNow);

		CTime tmEnd(tmNow);
		CString strTmNow = tmEnd.Format("%Y-%m-%d %H:%M:%S");

		__DEBUG__("Current time", strTmNow);

		if(pParent->m_tmPausTime <= tmNow)
		{
			if(counter==0){  // 처음에 한번 시간체크를 해준다.
				//대기할 시간이 과거이거나 현재...
				__DEBUG__("Exit thread : no waiting", NULL);
					
				pParent->PostMessage(WM_BRW_PAUSE, 0, 0);
				pParent->m_bExitPauseBRWThread = true;
				CloseHandle(pParent->m_hEventThreadPauseBRW);
				return 1;
			}
			__DEBUG__("Exit thread :  waiting end", NULL);
			break;
		}//if

		// skpark 2013.2.19  그냥 5초씩만 기다리며, while 문을 돌다가
		// 시간이 되면 while 을 break 하는 방식으로 바꾼다.
		// 왜냐하면, 단말에서 시계가 변경되어, 실제로는 해당사항이 없는데도
		// 이 함수로 들어오는 경우가 있기 때문이다.
		// 시계가 다시 올바르게 변경되어도, 무조건 처음에 계산한 시간대로
		// 움직이는 기존 함수는 변경된 시간을 반영하지 못하기 때문이다.
		// 다시말해 시계 자체가 자꾸 바뀌는 환경에서는 waitForSingleObject 한번으로 시간을 기다려서는 안되며
		// 계속 시계를 체크하는 방식으로 바꾸지 않을 수 없다.

		//DWORD dwWaitTime = (pParent->m_tmPausTime - tmNow)*1000;
		//__DEBUG__("BRW will be paused", (UINT)dwWaitTime);

		DWORD dwWaitTime = 5*1000;
		__DEBUG__("BRW will be paused", (UINT)dwWaitTime);

		dwEventRet = WaitForSingleObject(pParent->m_hEventThreadPauseBRW, dwWaitTime);
	} 

	if(dwEventRet == WAIT_OBJECT_0)
	{
		__DEBUG__("Exit thread : event set", _NULL);
	}
	else if(dwEventRet == WAIT_TIMEOUT)
	{
		__DEBUG__("Exit thread : BRW start", strTmPause);
		//대기 시간 완료
		pParent->PostMessage(WM_BRW_PAUSE, 0, 0);
	}//if
	pParent->m_bExitPauseBRWThread = true;
	CloseHandle(pParent->m_hEventThreadPauseBRW);

	return 1;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 스케줄파일에 존재하는 모든 스케줄의 시간을 검사한다. \n
/// @param (time_t&) tmTime : (out) 현재 시간보다 이후에 재생될 스케줄 중 가장 빠른 스케줄의 시간
/// @param (bool&) bIsOpen : (out) 스케줄의 파일이 정상적으로 open 되었는지를 반환
/// @return <형: bool> \n
///			<true: 스케줄이 있음> \n
///			<false: 스케줄이 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::CheckScheduleTime(time_t& tmTime, bool& bIsOpen)
{
	//모든 template의 스케줄을 구해서
	//현재 시간보다 이후인 스케줄이 있다면
	//그 시간을 구해서 true를 반환
	//현재 시간보다 이후인 스케줄이 없다면
	//시간은 0, true를 반환
	//스케줄이 없다면 시간은 0, false를 반환
	CTemplatePlay* pclsPlay = NULL;
	CScheduleArray	listSchedule;
	for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
	{
		pclsPlay = GetAtTemplatePlay(unIdx);
		if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL)
		{
			continue;
		}//if

		pclsPlay->m_pclsTemplate->GetScheduleList(listSchedule);	//Primary 프레임 스케줄만 가져오도록 수정함.
	}//for

	if(listSchedule.GetCount() == 0)
	{
		tmTime = 0;
		return false;
	}//if

	time_t tmNow, tmTmp;
	time(&tmNow);
	tmTime = tmTmp = 0;
	bIsOpen = false;
	CSchedule* pSchedule = NULL;
	for(int i=0; i<listSchedule.GetCount(); i++)
	{
		pSchedule = (CSchedule*)listSchedule.GetAt(i);
		pSchedule->GetStartTime(tmTmp);
		bIsOpen |= pSchedule->IsOpen();

		if(tmTmp >= tmNow)
		{
			if(tmTime == 0 || tmTmp <= tmTime)
			{
				tmTime = tmTmp;
			}//if
		}//if
	}//for

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Frame의 컨텐츠를 즉시 변경한다(F3 Key) \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::FrameModify()
{
	if(!m_bFullScreen)
	{
		return;
	}//if

	if(m_pCurrentTemplate)
	{
		m_pCurrentTemplate->Pause();

		FRAME_SCHEDULE_INFO_LIST* info_list = new FRAME_SCHEDULE_INFO_LIST;
		m_pCurrentTemplate->GetFrameScheduleInfo(*info_list);

		int count = info_list->GetCount();
		if(count > 0)
		{
			if(m_pdlgSelectFrame)
			{
				m_pdlgSelectFrame->DestroyWindow();
				delete m_pdlgSelectFrame;
				m_pdlgSelectFrame = NULL;
			}//if
			m_pdlgSelectFrame = new CSelectFrameDialog(this);

			CRect clsParentRect;
			this->GetWindowRect(&clsParentRect);
			m_pdlgSelectFrame->SetMainWndRect(clsParentRect);

			m_pdlgSelectFrame->m_infoList = info_list;
			m_pdlgSelectFrame->Create(IDD_SELECT_FRAME_DIALOG, this);
			m_pdlgSelectFrame->CenterWindow();
			m_pdlgSelectFrame->ShowWindow(SW_SHOW);
		}
		else
		{
			delete info_list;
		}//if
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 서버로부터 패키지(ini)파일을 다운로드한다. \n
/// @return <형: bool> \n
///			<true: 정상> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::DownloadPackageFile()
{
	if(GetEdition() != ENTERPRISE_EDITION)
	{
		return false;
	}//if

	int nRet;
	CString strConfigFile = ::GetHostName();
	strConfigFile += ".ini";

	try
	{
		__DEBUG__("Package file path", m_libDownloader.GetPackagePath());
		if(m_libDownloader.DownloadPackageFile(strConfigFile, GetProgramSite(), ::GetBRWId()))
		{
			__DEBUG__("Success download package file", strConfigFile);
			return true;
		}//if
	}
	catch(CException& ex)
	{
		char szCause[255];
		memset(&szCause, 0x00, 255);
		ex.GetErrorMessage(szCause, 255);
		__DEBUG__("Exception : DownloadPackageFile()", szCause);
		return false;
	}//try

	__DEBUG__("Fail to download package file", strConfigFile);

	return false;
}


LRESULT CHost::OnSendStop(WPARAM wParam, LPARAM lParam)
{
	if(!ciArgParser::getInstance()->isSet("+template")
		&& !ciArgParser::getInstance()->isSet("+download_only"))
	{
		//Auto 값이 설정되어있어도 다시 시작하지 않도록 한다.
		CString strArg;
		CString strAppName = GetAppName();
		if(strAppName == "UTV_brwClient2")
		{
			strArg = "BROWSER";
		}
		else
		{
			strArg = "BROWSER1";
		}//if

		__DEBUG__("Call dontStartAgain", strArg);
		if(!scratchUtil::getInstance()->dontStartAgain(strArg.GetBuffer()))
		{
			__DEBUG__("Fail to call dontStartAgain", strArg);
		}//if
	}//if

	killSyncProcess();

	PostMessage(WM_CLOSE, 0, 0);
	return 0;
}

void CHost::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	__DEBUG__("Begin OnClose", _NULL);

	StopBRW();
	SendStopSignal();	// 연동프로세스에 종료신호 보내기

	// 펠리카 서버 종료
#ifdef _FELICA_
	__DEBUG__("FELICA - felicaServerStop", _NULL);
    felicaUtil::getInstance()->felicaServerStop();
#endif

	CloseDummyVideo();

	if(m_pdlgSelectFrame)
	{
		m_pdlgSelectFrame->DestroyWindow();
		delete m_pdlgSelectFrame;
		m_pdlgSelectFrame = NULL;
	}//if

	if(m_pdlgMenuBar)
	{
		m_pdlgMenuBar->DestroyWindow();
		delete m_pdlgMenuBar;
		m_pdlgMenuBar = NULL;
	}//if

	//ThreadIEState
	if(m_hEvtThreadIEState)
	{
		m_bExitIEStateThread = true;
		SetEvent(m_hEvtThreadIEState);
		//m_bExitIEStateThread = true;
		CloseHandle(m_hEvtThreadIEState);
		m_hEvtThreadIEState = NULL;
	}//if
/*
	//Thread Resource
	if(m_hEvtThreadResource)
	{
		m_bExitResourceThread = true;
		SetEvent(m_hEvtThreadResource);
		//m_bExitIEStateThread = true;
		CloseHandle(m_hEvtThreadResource);
		m_hEvtThreadResource = NULL;
	}//if
*/
	if(!ciArgParser::getInstance()->isSet("+download_only")
		&& !ciArgParser::getInstance()->isSet("+template"))
	{
		//ThreadNotify
		m_bExitNotifyThread = true;
		SetEvent(m_hEventThreadNotify);
		//m_bExitNotifyThread = true;
		CloseHandle(m_hEventThreadNotify);
		m_hEventThreadNotify = NULL;
		Sleep(300);
	}//if

	if(m_brushBack.GetSafeHandle())
	{
		m_brushBack.DeleteObject();
	}//if

	m_libAudioControl.Fini();
	scratchUtil::clearInstance();
	CLogDialog::clearInstance();
	installUtil::clearInstance();

	__DEBUG__("End OnClose", _NULL);

	CDialog::OnClose();
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// TV 스케줄이 방송중인지를 설정한다. \n
/// @param (bool) bIsPlay : (in) TV 스케줄 방송 여부
/////////////////////////////////////////////////////////////////////////////////
void CHost::SetTVRunningState(bool bIsPlay)
{
	m_bIsTVPlay = bIsPlay;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// TV 스케줄이 방송중인지를 반환한다. \n
/// @return <형: bool> \n
///			<true: TV 스케줄 방송 중> \n
///			<false: TV 스케줄 방송중이 아님> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHost::GetTVRunningState()
{
	return m_bIsTVPlay;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Announce.ini 파일에서 긴급공지를 읽어온다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
//bool CHost::ReadAnnounce(int contentsType)
//{
//	CProfileManager mngProf;
//	CString strValue, strPath;
//	//먼저 lock 값(1: lock, 0: Unlock)을 읽어서 lock 상태이면 잠시후에 다시 시도한다.
//	strPath = GetAppPath();
//	if(contentsType == 2) {
//		strPath += "\\data\\UBCAnnounce.ini";
//	}else{
//		strPath += "\\data\\UBCAnnounce_CONTENTS.ini";
//	}
//	if(!mngProf.Read(strPath))
//	{
//		__DEBUG__("Fail to read announce file", strPath);
//		return false;
//	}//if
//
//	strValue = mngProf.GetProfileString("ANNOUNCE", "lock");
//	//strValue = GetINIValue("UBCAnnounce.ini", "ANNOUNCE", "lock");
//	if(strValue == "")
//	{
//		__DEBUG__("Can't found Announce lock value", _NULL);
//		return false;
//	}//if
//
//	if(strValue == "1")
//	{
//		HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
//		DWORD dwRet;
//		int nCount = 0;
//
//		while(1)
//		{
//			dwRet = WaitForSingleObject(hEvent, 500);
//			if(dwRet == WAIT_TIMEOUT)
//			{
//				//strValue = GetINIValue("UBCAnnounce.ini", "ANNOUNCE", "lock");
//				strValue = mngProf.GetProfileString("ANNOUNCE", "lock");
//				if(strValue == "1")
//				{
//					nCount++;
//					if(nCount >= 5)	//3초간 반복 대기하여도 잠금 상태이면 실패
//					{
//						__DEBUG__("Fail to read announce : lock state", _NULL);
//						CloseHandle(hEvent);
//						return false;
//					}//if
//
//					continue;
//				}
//				else if(strValue == "0")
//				{
//					//잠금 상태가 풀렸으면 값을 읽는다.
//					break;
//				}//if
//			}//if
//		}//while
//	}//if
//
//	//announceId 가 "*" 인경우는 수행할 긴급공지가 없다는 뜻이되므로, 수행하고 있는
//	//긴급공지를 중단한다. 이 경우 isStart 값은 언제나 false 이다.
//	//유효한 데이터 (isStart 가 1 이고, announceId 가 "*" 가 아닌) 를 발견하면 해당 긴급공지를 수행한다.
//	//strValue = GetINIValue("UBCAnnounce.ini", "ANNOUNCE", "isStart");
//	strValue = mngProf.GetProfileString("ANNOUNCE", "isStart");
//	if(strValue == "0")
//	{
//		__DEBUG__("Announce isStart is 0", _NULL);
//		//strValue = GetINIValue("UBCAnnounce.ini", "ANNOUNCE", "announceId");
//		strValue = mngProf.GetProfileString("ANNOUNCE", "announceId");
//
//		if(contentsType==2){
//			if(m_pdlgAnnounce)
//			{
//				if(strValue == "*")
//				{
//					__DEBUG__("Stop announce play", strValue);
//					m_pdlgAnnounce->StopAnnouncePlay();			//모든 긴급공지를 중지
//				}
//				else
//				{
//					__DEBUG__("announce removed", strValue);
//					m_pdlgAnnounce->RemoveAnnounce(strValue);	//해당하은 ID의 긴급공지를 중단
//				}//if
//			}//if
//		}else{
//			if(m_pdlgAnnounce_image)
//			{
//				if(strValue == "*")
//				{
//					__DEBUG__("Stop announce image play", strValue);
//					m_pdlgAnnounce_image->StopAnnouncePlay();			//모든 긴급공지를 중지
//				}
//				else
//				{
//					__DEBUG__("announce image removed", strValue);
//					m_pdlgAnnounce_image->RemoveAnnounce(strValue);	//해당하은 ID의 긴급공지를 중단
//				}//if
//			}//if
//			if(m_pdlgAnnounce_video)
//			{
//				if(strValue == "*")
//				{
//					__DEBUG__("Stop announce video play", strValue);
//					m_pdlgAnnounce_video->StopAnnouncePlay();			//모든 긴급공지를 중지
//				}
//				else
//				{
//					__DEBUG__("announce video removed", strValue);
//					m_pdlgAnnounce_video->RemoveAnnounce(strValue);	//해당하은 ID의 긴급공지를 중단
//				}//if
//			}//if
//			if(m_pdlgAnnounce_flash)
//			{
//				if(strValue == "*")
//				{
//					__DEBUG__("Stop announce flash play", strValue);
//					m_pdlgAnnounce_flash->StopAnnouncePlay();			//모든 긴급공지를 중지
//				}
//				else
//				{
//					__DEBUG__("announce flash removed", strValue);
//					m_pdlgAnnounce_flash->RemoveAnnounce(strValue);	//해당하은 ID의 긴급공지를 중단
//				}//if
//			}//if
//			if(m_pdlgAnnounce_web)
//			{
//				if(strValue == "*")
//				{
//					__DEBUG__("Stop announce flash play", strValue);
//					m_pdlgAnnounce_web->StopAnnouncePlay();			//모든 긴급공지를 중지
//				}
//				else
//				{
//					__DEBUG__("announce flash removed", strValue);
//					m_pdlgAnnounce_web->RemoveAnnounce(strValue);	//해당하은 ID의 긴급공지를 중단
//				}//if
//			}//if
//		}
//		return true;
//	}//if
//
//	CAnnounce* pAnnounce = new CAnnounce;
//
//	pAnnounce->m_strMgrId		= mngProf.GetProfileString("ANNOUNCE", "mgrId");
//	pAnnounce->m_strSiteId		= mngProf.GetProfileString("ANNOUNCE", "siteId");
//	pAnnounce->m_strAnnounceId	= mngProf.GetProfileString("ANNOUNCE", "announceId");
//	pAnnounce->m_strTitle		= mngProf.GetProfileString("ANNOUNCE", "title");
//	//pAnnounce->m_ulSerialNo		= atol(GetINIValue("UBCAnnounce.ini", "ANNOUNCE", "serialNo"));
//	pAnnounce->m_strCreator		= mngProf.GetProfileString("ANNOUNCE", "creator");
//	pAnnounce->m_tmCreateTime	= atol(mngProf.GetProfileString("ANNOUNCE", "createTime"));
//	//pAnnounce->m_listHostIdList
//	pAnnounce->m_tmStartTime	= atol(mngProf.GetProfileString("ANNOUNCE", "startTime"));
//	pAnnounce->m_tmEndTime		= atol(mngProf.GetProfileString("ANNOUNCE", "endTime"));
//	pAnnounce->m_lPosition		= atol(mngProf.GetProfileString("ANNOUNCE", "position"));
//	pAnnounce->m_sHeight		= atoi(mngProf.GetProfileString("ANNOUNCE", "height"));
//	pAnnounce->m_strComment[0]	= mngProf.GetProfileString("ANNOUNCE", "comment1");
//	pAnnounce->m_strComment[1]	= mngProf.GetProfileString("ANNOUNCE", "comment2");
//	pAnnounce->m_strComment[2]	= mngProf.GetProfileString("ANNOUNCE", "comment3");
//	pAnnounce->m_strComment[3]	= mngProf.GetProfileString("ANNOUNCE", "comment4");
//	pAnnounce->m_strComment[4]	= mngProf.GetProfileString("ANNOUNCE", "comment5");
//	pAnnounce->m_strComment[5]	= mngProf.GetProfileString("ANNOUNCE", "comment6");
//	pAnnounce->m_strComment[6]	= mngProf.GetProfileString("ANNOUNCE", "comment7");
//	pAnnounce->m_strComment[7]	= mngProf.GetProfileString("ANNOUNCE", "comment8");
//	pAnnounce->m_strComment[8]	= mngProf.GetProfileString("ANNOUNCE", "comment9");
//	pAnnounce->m_strComment[9]	= mngProf.GetProfileString("ANNOUNCE", "comment10");
//	pAnnounce->m_strFont		= mngProf.GetProfileString("ANNOUNCE", "font");
//	pAnnounce->m_sFontSize		= atoi(mngProf.GetProfileString("ANNOUNCE", "fontSize"));
//	pAnnounce->m_strFgColor		= mngProf.GetProfileString("ANNOUNCE", "fgColor");
//	pAnnounce->m_strBgColor		= mngProf.GetProfileString("ANNOUNCE", "bgColor");
//	pAnnounce->m_strBgLocation	= mngProf.GetProfileString("ANNOUNCE", "bglocation");
//	pAnnounce->m_strBgFile		= mngProf.GetProfileString("ANNOUNCE", "bgFile");
//	pAnnounce->m_sPlaySpeed		= atoi(mngProf.GetProfileString("ANNOUNCE", "playSpeed"));
//	pAnnounce->m_sAlpha			= atoi(mngProf.GetProfileString("ANNOUNCE", "alpha"));
//
//	CString contentsTypeStr = mngProf.GetProfileString("ANNOUNCE","contentsType");
//	if(!contentsTypeStr.IsEmpty()){
//		pAnnounce->m_ContentsType =	atoi(contentsTypeStr);	
//	}else{
//		pAnnounce->m_ContentsType =	2; // Default Value
//	}
//
//	if(pAnnounce->m_ContentsType != 2) // ticker 가 아닌 타입이라면,
//	{  
//		pAnnounce->m_LRMargin		=	atoi(mngProf.GetProfileString("ANNOUNCE","lrMargin"));		
//		pAnnounce->m_UDMargin		=	atoi(mngProf.GetProfileString("ANNOUNCE","udMargin"));		
//		pAnnounce->m_SourceSize		=	atoi(mngProf.GetProfileString("ANNOUNCE","sourceSize"));	
//		pAnnounce->m_SoundVolume	=	atoi(mngProf.GetProfileString("ANNOUNCE","soundVolume"));	
//		pAnnounce->m_FileSize		=	ciStringUtil::atoull(mngProf.GetProfileString("ANNOUNCE","fileSize"));		
//		pAnnounce->m_FileMD5		=	mngProf.GetProfileString("ANNOUNCE","fileMD5");		
//		pAnnounce->m_Width			=	atoi(mngProf.GetProfileString("ANNOUNCE","width"));				
//		pAnnounce->m_PosX			=	atoi(mngProf.GetProfileString("ANNOUNCE","posX"));				
//		pAnnounce->m_PosY			=	atoi(mngProf.GetProfileString("ANNOUNCE","posY"));					  			
//		pAnnounce->m_HeightRatio	=	atof(mngProf.GetProfileString("ANNOUNCE","heightRatio"));
//	}
//
//
//	//속도를 총 5단게로 한다.
//	//빠름(1~49) >> 조금빠름(50~99) >> 보통(100~199) >> 조금느림(200~499) >> 아주느림(500~1000)
//	if(pAnnounce->m_sPlaySpeed < 50)
//	{
//		//빠름(1~49)
//		pAnnounce->m_sPlaySpeed = E_TICKER_FAST;
//	}
//	else if(pAnnounce->m_sPlaySpeed >= 50 && pAnnounce->m_sPlaySpeed < 100)
//	{
//		//조금빠름(50~99)
//		pAnnounce->m_sPlaySpeed = E_TICKER_LITTLE_FAST;
//	}
//	else if(pAnnounce->m_sPlaySpeed >= 100 && pAnnounce->m_sPlaySpeed < 200)
//	{
//		//보통(100~199)
//		pAnnounce->m_sPlaySpeed = E_TICKER_NORMAL;
//	}
//	else if(pAnnounce->m_sPlaySpeed >= 200 && pAnnounce->m_sPlaySpeed < 500)
//	{
//		//조금느림(200~499)
//		pAnnounce->m_sPlaySpeed = E_TICKER_LITTLE_SLOW;
//	}
//	else if(pAnnounce->m_sPlaySpeed >= 500 && pAnnounce->m_sPlaySpeed <= 1000)
//	{
//		//아주느림(500~1000)
//		pAnnounce->m_sPlaySpeed = E_TICKET_SLOW;
//	}//if
//
//	__DEBUG__("mgrId", pAnnounce->m_strMgrId.c_str());
//	__DEBUG__("siteId", pAnnounce->m_strSiteId.c_str());
//	__DEBUG__("announceId", pAnnounce->m_strAnnounceId.c_str());
//	__DEBUG__("Title", pAnnounce->m_strTitle.c_str());
//	__DEBUG__("ContentsType", pAnnounce->m_ContentsType);
//
//	pAnnounce->m_nWidth = m_nCx;
//
//	__DEBUG__("Announc add", pAnnounce->m_strAnnounceId.c_str());
//
//	switch(pAnnounce->m_ContentsType) {
//		case 1:
//			if(!m_pdlgAnnounce_image)
//			{
//				m_pdlgAnnounce_image = new CAnnounce_image_Dlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
//				m_pdlgAnnounce_image->SetTicker(m_pdlgAnnounce);
//				m_pdlgAnnounce_image->Create(IDD_ANNOUNCE_DLG, this);
//			}
//			m_pdlgAnnounce_image->AddAnnounce(pAnnounce);
//			if(m_pdlgAnnounce_video && m_pdlgAnnounce_video->IsPlay()){	m_pdlgAnnounce_video->StopAnnouncePlay();}
//			if(m_pdlgAnnounce_flash && m_pdlgAnnounce_flash->IsPlay()){	m_pdlgAnnounce_flash->StopAnnouncePlay();}
//			if(m_pdlgAnnounce_web && m_pdlgAnnounce_web->IsPlay()){	m_pdlgAnnounce_web->StopAnnouncePlay();}
//			
//			break;
//		case 0:
//			if(!m_pdlgAnnounce_video)
//			{
//				m_pdlgAnnounce_video = new CAnnounce_video_Dlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
//				m_pdlgAnnounce_video->SetTicker(m_pdlgAnnounce);
//				m_pdlgAnnounce_video->Create(IDD_ANNOUNCE_DLG, this);
//			}
//			m_pdlgAnnounce_video->AddAnnounce(pAnnounce);
//			if(m_pdlgAnnounce_image && m_pdlgAnnounce_image->IsPlay()){	m_pdlgAnnounce_image->StopAnnouncePlay();}
//			if(m_pdlgAnnounce_flash && m_pdlgAnnounce_flash->IsPlay()){	m_pdlgAnnounce_flash->StopAnnouncePlay();}
//			if(m_pdlgAnnounce_web && m_pdlgAnnounce_web->IsPlay()){	m_pdlgAnnounce_web->StopAnnouncePlay();}
//			break;
//		case 3:
//			if(!m_pdlgAnnounce_flash)
//			{
//				m_pdlgAnnounce_flash = new CAnnounce_flash_Dlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
//				m_pdlgAnnounce_flash->SetTicker(m_pdlgAnnounce);
//				m_pdlgAnnounce_flash->Create(IDD_ANNOUNCE_DLG, this);
//			}
//			m_pdlgAnnounce_flash->AddAnnounce(pAnnounce);
//			if(m_pdlgAnnounce_video && m_pdlgAnnounce_video->IsPlay()){	m_pdlgAnnounce_video->StopAnnouncePlay();}
//			if(m_pdlgAnnounce_image && m_pdlgAnnounce_image->IsPlay()){	m_pdlgAnnounce_image->StopAnnouncePlay();}
//			if(m_pdlgAnnounce_web && m_pdlgAnnounce_web->IsPlay()){	m_pdlgAnnounce_web->StopAnnouncePlay();}
//			break;
//		case 4:
//			if(!m_pdlgAnnounce_web)
//			{
//				m_pdlgAnnounce_web = new CAnnounce_web_Dlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
//				m_pdlgAnnounce_web->SetTicker(m_pdlgAnnounce);
//				m_pdlgAnnounce_web->Create(IDD_ANNOUNCE_DLG, this);
//			}
//			m_pdlgAnnounce_web->AddAnnounce(pAnnounce);
//			if(m_pdlgAnnounce_video && m_pdlgAnnounce_video->IsPlay()){	m_pdlgAnnounce_video->StopAnnouncePlay();}
//			if(m_pdlgAnnounce_image && m_pdlgAnnounce_image->IsPlay()){	m_pdlgAnnounce_image->StopAnnouncePlay();}
//			if(m_pdlgAnnounce_flash && m_pdlgAnnounce_flash->IsPlay()){	m_pdlgAnnounce_flash->StopAnnouncePlay();}
//			break;
//		case 2:
//			if(!m_pdlgAnnounce)
//			{
//				m_pdlgAnnounce = new CAnnounceDlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
//				m_pdlgAnnounce->Create(IDD_ANNOUNCE_DLG, this);
//			}
//			m_pdlgAnnounce->AddAnnounce(pAnnounce);
//			break;
//		default:
//			__DEBUG__("Invalid Annonce TYPE ERROR=", pAnnounce->m_ContentsType);
//			return false;
//	}
//	return true;
//}
//skpark 2013.11.28 new announce
bool CHost::ReadAnnounce(const char* announceId, bool isStart)
{
	__DEBUG__("ReadAnnounce()", announceId);

	CString  strPath = GetAppPath();
	strPath += "\\data\\announce\\UBCAnnounce_";
	strPath += announceId;
	strPath += ".ini";
		
	CProfileManager mngProf;
	if(!mngProf.Read(strPath))
	{
		__DEBUG__("Fail to read announce file", strPath);
		return false;
	}

	WaitAnnounceLock(mngProf);
	// lock released

	CString contentsTypeStr = mngProf.GetProfileString("ANNOUNCE","contentsType");
	int contentsType = atoi(contentsTypeStr);

	__DEBUG__("Announce isStart is ", isStart);
	if(isStart == 0)
	{
		switch(contentsType){
			case 2: if(m_pdlgAnnounce) 			m_pdlgAnnounce->StopOrRemove(announceId,		"ticker");	break;
			case 1: if(m_pdlgAnnounce_image) 	m_pdlgAnnounce_image->StopOrRemove(announceId,	"image");	break;
			case 0: if(m_pdlgAnnounce_video) 	m_pdlgAnnounce_video->StopOrRemove(announceId,	"video");	break;
			case 3: if(m_pdlgAnnounce_flash) 	m_pdlgAnnounce_flash->StopOrRemove(announceId,	"flash");	break;
			case 5: if(m_pdlgAnnounce_ppt) 	m_pdlgAnnounce_ppt->StopOrRemove(announceId,	"ppt");	break;
			case 4: if(m_pdlgAnnounce_web) 		m_pdlgAnnounce_web->StopOrRemove(announceId,		"web");		break;
			default : __DEBUG__("Invalid Annonce TYPE ERROR=", contentsType);								return false;
		}
		return true;
	}//if
	return bool(CreateAnnounceObject(mngProf));
}

//skpark 2013.11.28 new announce
bool CHost::WaitAnnounceLock(CProfileManager& mngProf)
{
	//먼저 lock 값(1: lock, 0: Unlock)을 읽어서 lock 상태이면 잠시후에 다시 시도한다.
	CString strLock = mngProf.GetProfileString("ANNOUNCE", "lock");
	if(strLock == "")
	{
		__DEBUG__("Can't found Announce lock value", _NULL);
		return false;
	}
	if(strLock == "1")
	{
		HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		DWORD dwRet;
		int nCount = 0;

		while(1)
		{
			dwRet = WaitForSingleObject(hEvent, 500);
			if(dwRet == WAIT_TIMEOUT)
			{
				//strValue = GetINIValue("UBCAnnounce.ini", "ANNOUNCE", "lock");
				strLock = mngProf.GetProfileString("ANNOUNCE", "lock");
				if(strLock == "1")
				{
					nCount++;
					if(nCount >= 5)	//3초간 반복 대기하여도 잠금 상태이면 실패
					{
						__DEBUG__("Fail to read announce : lock state", _NULL);
						CloseHandle(hEvent);
						return false;
					}//if

					continue;
				}
				else if(strLock == "0")
				{
					//잠금 상태가 풀렸으면 값을 읽는다.
					break;
				}//if
			}//if
		}//while
	}//if
	return true;
}

//skpark 2013.11.28 new announce
int CHost::ReadAllAnnounce()
{
	__DEBUG__("ReadAllAnnounce()",NULL);

	// 현재 있는것을 말끔히 재거한다.
	if(m_pdlgAnnounce) 			m_pdlgAnnounce->StopOrRemove("*",		"ticker");	
	if(m_pdlgAnnounce_image) 	m_pdlgAnnounce_image->StopOrRemove("*",	"image");	
	if(m_pdlgAnnounce_video) 	m_pdlgAnnounce_video->StopOrRemove("*",	"video");	
	if(m_pdlgAnnounce_flash) 	m_pdlgAnnounce_flash->StopOrRemove("*",	"flash");	
	if(m_pdlgAnnounce_ppt) 	m_pdlgAnnounce_ppt->StopOrRemove("*",	"ppt");	
	if(m_pdlgAnnounce_web) 		m_pdlgAnnounce_web->StopOrRemove("*",		"web");	

	CString  find_path = GetAppPath();
	find_path += "\\data\\announce\\";
	
	CString find_str = find_path + "UBCAnnounce_*.ini";

	ciStringList iniList;
	installUtil::getInstance()->getFileList(find_str, iniList);

	int retval = 0;
	ciStringList::iterator itr;
	for( itr=iniList.begin(); itr!=iniList.end(); itr++ )
	{
		CString iniFileName = itr->c_str();
		CString strPath = find_path + iniFileName;

		__DEBUG__("ReadAllAnnounce()", iniFileName);

		CProfileManager mngProf;
		if(!mngProf.Read(strPath))
		{
			__DEBUG__("Fail to read announce file", strPath);
			continue;
		}
	
		WaitAnnounceLock(mngProf);
		// lock released

		int isStart	= atoi(mngProf.GetProfileString("ANNOUNCE", "isStart"));
		CString announceId = mngProf.GetProfileString("ANNOUNCE","announceId");
		CString contentsTypeStr = mngProf.GetProfileString("ANNOUNCE","contentsType");
		int contentsType = atoi(contentsTypeStr);

		__DEBUG__("Announce isStart is ", isStart);
		__DEBUG__("Announce announceId is ", announceId);
		__DEBUG__("Announce contentsType is ", contentsTypeStr);

		if(isStart == 0)
		{
			switch(contentsType){
				case 2: if(m_pdlgAnnounce) 			m_pdlgAnnounce->StopOrRemove(announceId,		"ticker");	break;
				case 1: if(m_pdlgAnnounce_image) 	m_pdlgAnnounce_image->StopOrRemove(announceId,	"image");	break;
				case 0: if(m_pdlgAnnounce_video) 	m_pdlgAnnounce_video->StopOrRemove(announceId,	"video");	break;
				case 3: if(m_pdlgAnnounce_flash) 	m_pdlgAnnounce_flash->StopOrRemove(announceId,	"flash");	break;
				case 5: if(m_pdlgAnnounce_ppt) 	m_pdlgAnnounce_ppt->StopOrRemove(announceId,	"ppt");	break;
				case 4: if(m_pdlgAnnounce_web) 		m_pdlgAnnounce_web->StopOrRemove(announceId,	"web");		break;
				default : __DEBUG__("Invalid Annonce TYPE ERROR=", contentsType);								return false;
			}
			continue;
		}
		retval += CreateAnnounceObject(mngProf);
	}
	return retval;
}

int CHost::CreateAnnounceObject(CProfileManager& mngProf)
{
	__DEBUG__("CreateAnnounceObject()", NULL);

	CAnnounce* pAnnounce = new CAnnounce;

	pAnnounce->m_strMgrId		= mngProf.GetProfileString("ANNOUNCE", "mgrId");
	pAnnounce->m_strSiteId		= mngProf.GetProfileString("ANNOUNCE", "siteId");
	pAnnounce->m_strAnnounceId	= mngProf.GetProfileString("ANNOUNCE", "announceId");
	pAnnounce->m_strTitle		= mngProf.GetProfileString("ANNOUNCE", "title");
	pAnnounce->m_strCreator		= mngProf.GetProfileString("ANNOUNCE", "creator");
	pAnnounce->m_tmCreateTime	= atol(mngProf.GetProfileString("ANNOUNCE", "createTime"));
	pAnnounce->m_tmStartTime	= atol(mngProf.GetProfileString("ANNOUNCE", "startTime"));
	pAnnounce->m_tmEndTime		= atol(mngProf.GetProfileString("ANNOUNCE", "endTime"));
	pAnnounce->m_lPosition		= atol(mngProf.GetProfileString("ANNOUNCE", "position"));
	pAnnounce->m_sHeight		= atoi(mngProf.GetProfileString("ANNOUNCE", "height"));
	pAnnounce->m_strComment[0]	= mngProf.GetProfileString("ANNOUNCE", "comment1");
	pAnnounce->m_strComment[1]	= mngProf.GetProfileString("ANNOUNCE", "comment2");
	pAnnounce->m_strComment[2]	= mngProf.GetProfileString("ANNOUNCE", "comment3");
	pAnnounce->m_strComment[3]	= mngProf.GetProfileString("ANNOUNCE", "comment4");
	pAnnounce->m_strComment[4]	= mngProf.GetProfileString("ANNOUNCE", "comment5");
	pAnnounce->m_strComment[5]	= mngProf.GetProfileString("ANNOUNCE", "comment6");
	pAnnounce->m_strComment[6]	= mngProf.GetProfileString("ANNOUNCE", "comment7");
	pAnnounce->m_strComment[7]	= mngProf.GetProfileString("ANNOUNCE", "comment8");
	pAnnounce->m_strComment[8]	= mngProf.GetProfileString("ANNOUNCE", "comment9");
	pAnnounce->m_strComment[9]	= mngProf.GetProfileString("ANNOUNCE", "comment10");
	pAnnounce->m_strFont		= mngProf.GetProfileString("ANNOUNCE", "font");
	pAnnounce->m_sFontSize		= atoi(mngProf.GetProfileString("ANNOUNCE", "fontSize"));
	pAnnounce->m_strFgColor		= mngProf.GetProfileString("ANNOUNCE", "fgColor");
	pAnnounce->m_strBgColor		= mngProf.GetProfileString("ANNOUNCE", "bgColor");
	pAnnounce->m_strBgLocation	= mngProf.GetProfileString("ANNOUNCE", "bglocation");
	pAnnounce->m_strBgFile		= mngProf.GetProfileString("ANNOUNCE", "bgFile");
	pAnnounce->m_sPlaySpeed		= atoi(mngProf.GetProfileString("ANNOUNCE", "playSpeed"));
	pAnnounce->m_sAlpha			= atoi(mngProf.GetProfileString("ANNOUNCE", "alpha"));

	CString contentsTypeStr = mngProf.GetProfileString("ANNOUNCE","contentsType");
	if(!contentsTypeStr.IsEmpty()){
		pAnnounce->m_ContentsType =	atoi(contentsTypeStr);	
	}else{
		pAnnounce->m_ContentsType =	2; // Default Value
	}

	if(pAnnounce->m_ContentsType != 2) // ticker 가 아닌 타입이라면,
	{  
		pAnnounce->m_LRMargin		=	atoi(mngProf.GetProfileString("ANNOUNCE","lrMargin"));		
		pAnnounce->m_UDMargin		=	atoi(mngProf.GetProfileString("ANNOUNCE","udMargin"));		
		pAnnounce->m_SourceSize		=	atoi(mngProf.GetProfileString("ANNOUNCE","sourceSize"));	
		pAnnounce->m_SoundVolume	=	atoi(mngProf.GetProfileString("ANNOUNCE","soundVolume"));	
		pAnnounce->m_FileSize		=	ciStringUtil::atoull(mngProf.GetProfileString("ANNOUNCE","fileSize"));		
		pAnnounce->m_FileMD5		=	mngProf.GetProfileString("ANNOUNCE","fileMD5");		
		pAnnounce->m_Width			=	atoi(mngProf.GetProfileString("ANNOUNCE","width"));				
		pAnnounce->m_PosX			=	atoi(mngProf.GetProfileString("ANNOUNCE","posX"));				
		pAnnounce->m_PosY			=	atoi(mngProf.GetProfileString("ANNOUNCE","posY"));					  			
		pAnnounce->m_HeightRatio	=	atof(mngProf.GetProfileString("ANNOUNCE","heightRatio"));
	}

	//속도를 총 5단게로 한다.
	//빠름(1~49) >> 조금빠름(50~99) >> 보통(100~199) >> 조금느림(200~499) >> 아주느림(500~1000)
	if(pAnnounce->m_sPlaySpeed < 50)
	{
		//빠름(1~49)
		pAnnounce->m_sPlaySpeed = E_TICKER_FAST;
	}
	else if(pAnnounce->m_sPlaySpeed >= 50 && pAnnounce->m_sPlaySpeed < 100)
	{
		//조금빠름(50~99)
		pAnnounce->m_sPlaySpeed = E_TICKER_LITTLE_FAST;
	}
	else if(pAnnounce->m_sPlaySpeed >= 100 && pAnnounce->m_sPlaySpeed < 200)
	{
		//보통(100~199)
		pAnnounce->m_sPlaySpeed = E_TICKER_NORMAL;
	}
	else if(pAnnounce->m_sPlaySpeed >= 200 && pAnnounce->m_sPlaySpeed < 500)
	{
		//조금느림(200~499)
		pAnnounce->m_sPlaySpeed = E_TICKER_LITTLE_SLOW;
	}
	else if(pAnnounce->m_sPlaySpeed >= 500 && pAnnounce->m_sPlaySpeed <= 1000)
	{
		//아주느림(500~1000)
		pAnnounce->m_sPlaySpeed = E_TICKET_SLOW;
	}//if

	__DEBUG__("mgrId", pAnnounce->m_strMgrId.c_str());
	__DEBUG__("siteId", pAnnounce->m_strSiteId.c_str());
	__DEBUG__("announceId", pAnnounce->m_strAnnounceId.c_str());
	__DEBUG__("Title", pAnnounce->m_strTitle.c_str());
	__DEBUG__("ContentsType", pAnnounce->m_ContentsType);

	pAnnounce->m_nWidth = m_nCx;

	__DEBUG__("Announc add", pAnnounce->m_strAnnounceId.c_str());

	switch(pAnnounce->m_ContentsType) {
		case 1:
			if(!m_pdlgAnnounce_image)
			{
				m_pdlgAnnounce_image = new CAnnounce_image_Dlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
				m_pdlgAnnounce_image->SetTicker(m_pdlgAnnounce);
				m_pdlgAnnounce_image->Create(IDD_ANNOUNCE_DLG, this);
			}
			m_pdlgAnnounce_image->AddAnnounce(pAnnounce);
			if(m_pdlgAnnounce_video && m_pdlgAnnounce_video->IsPlay()){	m_pdlgAnnounce_video->StopAnnouncePlay();}
			if(m_pdlgAnnounce_flash && m_pdlgAnnounce_flash->IsPlay()){	m_pdlgAnnounce_flash->StopAnnouncePlay();}
			if(m_pdlgAnnounce_web && m_pdlgAnnounce_web->IsPlay()){	m_pdlgAnnounce_web->StopAnnouncePlay();}
			if(m_pdlgAnnounce_ppt && m_pdlgAnnounce_ppt->IsPlay()){	m_pdlgAnnounce_ppt->StopAnnouncePlay();}
			
			break;
		case 0:
			if(!m_pdlgAnnounce_video)
			{
				m_pdlgAnnounce_video = new CAnnounce_video_Dlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
				m_pdlgAnnounce_video->SetTicker(m_pdlgAnnounce);
				m_pdlgAnnounce_video->Create(IDD_ANNOUNCE_DLG, this);
			}
			m_pdlgAnnounce_video->AddAnnounce(pAnnounce);
			if(m_pdlgAnnounce_image && m_pdlgAnnounce_image->IsPlay()){	m_pdlgAnnounce_image->StopAnnouncePlay();}
			if(m_pdlgAnnounce_flash && m_pdlgAnnounce_flash->IsPlay()){	m_pdlgAnnounce_flash->StopAnnouncePlay();}
			if(m_pdlgAnnounce_web && m_pdlgAnnounce_web->IsPlay()){	m_pdlgAnnounce_web->StopAnnouncePlay();}
			if(m_pdlgAnnounce_ppt && m_pdlgAnnounce_ppt->IsPlay()){	m_pdlgAnnounce_ppt->StopAnnouncePlay();}
			break;
		case 3:
			if(!m_pdlgAnnounce_flash)
			{
				m_pdlgAnnounce_flash = new CAnnounce_flash_Dlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
				m_pdlgAnnounce_flash->SetTicker(m_pdlgAnnounce);
				m_pdlgAnnounce_flash->Create(IDD_ANNOUNCE_DLG, NULL);
			}
			m_pdlgAnnounce_flash->AddAnnounce(pAnnounce);
			if(m_pdlgAnnounce_video && m_pdlgAnnounce_video->IsPlay()){	m_pdlgAnnounce_video->StopAnnouncePlay();}
			if(m_pdlgAnnounce_image && m_pdlgAnnounce_image->IsPlay()){	m_pdlgAnnounce_image->StopAnnouncePlay();}
			if(m_pdlgAnnounce_web && m_pdlgAnnounce_web->IsPlay()){	m_pdlgAnnounce_web->StopAnnouncePlay();}
			if(m_pdlgAnnounce_ppt && m_pdlgAnnounce_ppt->IsPlay()){	m_pdlgAnnounce_ppt->StopAnnouncePlay();}
			break;
		case 5:
			if(!m_pdlgAnnounce_ppt)
			{
				m_pdlgAnnounce_ppt = new CAnnounce_ppt_Dlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
				m_pdlgAnnounce_ppt->SetTicker(m_pdlgAnnounce);
				m_pdlgAnnounce_ppt->Create(IDD_ANNOUNCE_DLG, NULL);
			}
			m_pdlgAnnounce_ppt->AddAnnounce(pAnnounce);
			if(m_pdlgAnnounce_video && m_pdlgAnnounce_video->IsPlay()){	m_pdlgAnnounce_video->StopAnnouncePlay();}
			if(m_pdlgAnnounce_image && m_pdlgAnnounce_image->IsPlay()){	m_pdlgAnnounce_image->StopAnnouncePlay();}
			if(m_pdlgAnnounce_web && m_pdlgAnnounce_web->IsPlay()){	m_pdlgAnnounce_web->StopAnnouncePlay();}
			if(m_pdlgAnnounce_flash && m_pdlgAnnounce_flash->IsPlay()){	m_pdlgAnnounce_flash->StopAnnouncePlay();}
			break;
		case 4:
			if(!m_pdlgAnnounce_web)
			{
				m_pdlgAnnounce_web = new CAnnounce_web_Dlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
				m_pdlgAnnounce_web->SetTicker(m_pdlgAnnounce);
				m_pdlgAnnounce_web->Create(IDD_ANNOUNCE_DLG, this);
			}
			m_pdlgAnnounce_web->AddAnnounce(pAnnounce);
			if(m_pdlgAnnounce_video && m_pdlgAnnounce_video->IsPlay()){	m_pdlgAnnounce_video->StopAnnouncePlay();}
			if(m_pdlgAnnounce_image && m_pdlgAnnounce_image->IsPlay()){	m_pdlgAnnounce_image->StopAnnouncePlay();}
			if(m_pdlgAnnounce_flash && m_pdlgAnnounce_flash->IsPlay()){	m_pdlgAnnounce_flash->StopAnnouncePlay();}
			if(m_pdlgAnnounce_ppt && m_pdlgAnnounce_ppt->IsPlay()){	m_pdlgAnnounce_ppt->StopAnnouncePlay();}
			break;
		case 2:
			if(!m_pdlgAnnounce)
			{
				m_pdlgAnnounce = new CAnnounceDlg(m_nPosX, m_nPosY, m_nCx, m_nCy, this);
				m_pdlgAnnounce->Create(IDD_ANNOUNCE_DLG, this);
			}
			m_pdlgAnnounce->AddAnnounce(pAnnounce);
			break;
		default:
			__DEBUG__("Invalid Annonce TYPE ERROR=", pAnnounce->m_ContentsType);
			return 0;
	}
	return 1;
}




LRESULT CHost::OnDefaultTemplateChanged(WPARAM wParam, LPARAM lParam)
{
	SetDefaultTemplatePlay((LPCSTR)lParam);

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Dummy video를 open한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::OpenDummyVideo()
{
	if(m_bIsPlayDummy)
	{
		return;
	}//if

	__DEBUG__("Open dummy file", _NULL);
	ciString dummy_name = "";
	ciArgParser::getInstance()->getArgValue("+dummy", dummy_name);
	if(dummy_name.length() == 0)
	{
		dummy_name = "dummy.avi";
	}//if

	CString strDummyPath = ::GetAppPath();
	strDummyPath.Append(dummy_name.c_str());
	m_interface.Open(strDummyPath); // NULL play
	m_bIsPlayDummy = true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Dummy video를 close한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::CloseDummyVideo()
{
	if(!m_bIsPlayDummy)
	{
		return;
	}//if

	__DEBUG__("Close dummy file", _NULL);
	m_interface.Close();
	m_bIsPlayDummy = false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 클릭이벤트를 받아 특정 템플릿으로이동한다. \n
/// @param (CString) strParam : (in)"넘어가야할템플릿Id:지속시간(초):다시돌아가야할템플릿Id" (예 : Templete01:30:Template02)
/////////////////////////////////////////////////////////////////////////////////
void CHost::ClickJumpTemplate(CString strParam)
{
	KillTimer(TEMPLATE_CHECK_ID);
	if(::GetScheduleResuming())
		KillTimer(TIMER_SHOWWINDOW_ID);	// <!-- 이어서 재생 (2016-08-29) -->

	CString strTok, strTargetId, strBackId;
	int nPlayTime;
	int nPos = 0;
	//','를 기준으로 분리
	nPos = strParam.Find(",", 0);
	if(nPos == -1)
	{
		__DEBUG__("Param psrsing error", strParam);
		SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
		if(::GetScheduleResuming())
			SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL);	// <!-- 이어서 재생 (2016-08-29) -->
		return;
	}//if

	//이동할 템플릿 Id
	strTargetId = strParam.Left(nPos);
	strParam.Delete(0, nPos+1);

	nPos = strParam.Find(",", 0);
	if(nPos == -1)
	{
		__DEBUG__("Param psrsing error", strParam);
		SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
		if(::GetScheduleResuming())
			SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL);	// <!-- 이어서 재생 (2016-08-29) -->
		return;
	}//if

	//재생시간
	m_dwClickJumpWaitTime = atoi(strParam.Left(nPos));
	strParam.Delete(0, nPos+1);

	CTemplatePlay* pclsPlay = NULL;
	for(UINT unIdx=0; unIdx<GetCountOfTemplatePlayAry(); unIdx++)
	{
		pclsPlay = GetAtTemplatePlay(unIdx);
		if(pclsPlay  == NULL || pclsPlay->m_pclsTemplate == NULL)
		{
			continue;
		}//if

		if(strcmp(pclsPlay->m_pclsTemplate->GetTemplateId(), strTargetId) == 0)
		{
			__DEBUG__("Jump tp Template", pclsPlay->m_pclsTemplate->GetTemplateId());
			KillTimer(TIMER_CLICK_JUMP);
			SetDefaultTemplatePlay(pclsPlay->m_pclsTemplate->GetTemplateId());
			m_bClickJump = true;

			//Play시간뒤에 돌아갈 템플릿 Id
			m_strReturnTemplateId = strParam;

			//Play가 끝나는 시간 설정
			if(m_dwClickJumpWaitTime != 0)
			{
				SetTimer(TIMER_CLICK_JUMP, m_dwClickJumpWaitTime*1000, NULL);
			}//if

			TemplateCheck();	//변경된 템플릿을 직접 재생...
			//CheckSchedule();
			
			return;
		}//if
	}//for

	__DEBUG__("Can't found click jump template", strParam);
	SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
	if(::GetScheduleResuming())
		SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL);	// <!-- 이어서 재생 (2016-08-29) -->
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 템플릿의 재생순서를 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::TemplateCheck()
{
	KillTimer(TEMPLATE_CHECK_ID);
	if(::GetScheduleResuming())
		KillTimer(TIMER_SHOWWINDOW_ID);	// <!-- 이어서 재생 (2016-08-29) -->
	if(!CheckSchedule())
	{
		__DEBUG__("CheckSchedule() return false", _NULL); 

		CString msg;
		msg.Format("Package [%s] does not have anything to play", ::GetHostName());
		time_t tmSchedule;
		bool bIsOpen;
		if(!CheckScheduleTime(tmSchedule, bIsOpen))
		{
			OnErrorMessage((WPARAM)(LPCSTR)msg, 0);
			m_wndBack.ShowWindow(SW_SHOW);
			KillTimer(TEMPLATE_CHECK_ID);
			if(::GetScheduleResuming())
				KillTimer(TIMER_SHOWWINDOW_ID);	// <!-- 이어서 재생 (2016-08-29) -->
			m_strStateMsg = STAT_MSG_CONTENTS_UNAVAILABLE;
		}
		else
		{
			if(tmSchedule == 0)
			{
				if(m_pCurrentTemplate)
				{
					m_pCurrentTemplate->Stop();
					m_pCurrentTemplate = NULL;
				}//if

				if(bIsOpen)
				{
					msg += " - All pckage(s) playing time has been expired";
					m_strStateMsg = STAT_MSG_CONTENTS_EXPIRED;
				}
				else
				{
					//Contents file open fail
					msg += " - All pckage(s) contents file has missing or can not play";
					m_strStateMsg = STAT_MSG_CONTENTS_UNAVAILABLE;
				}//if
				OnErrorMessage((WPARAM)(LPCSTR)msg, 0);
				m_wndBack.ShowWindow(SW_SHOW);
				KillTimer(TEMPLATE_CHECK_ID);
				if(::GetScheduleResuming())
					KillTimer(TIMER_SHOWWINDOW_ID);	// <!-- 이어서 재생 (2016-08-29) -->
			}
			else
			{
				m_tmPausTime = tmSchedule;
				PauseBRW();
			}//if
		}//if
	}
	else
	{
		SetTimer(TEMPLATE_CHECK_ID, TEMPLATE_CHECK_TIME, NULL);
		if(::GetScheduleResuming())
			SetTimer(TIMER_SHOWWINDOW_ID, TIMER_SHOWWINDOW_TIME, NULL);	// <!-- 이어서 재생 (2016-08-29) -->
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 클릭점프 스케줄의 타이머를 리셋한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::ResetClickJumpTimer()
{
	if(m_dwClickJumpWaitTime != 0)
	{
		KillTimer(TIMER_CLICK_JUMP);

		//Play가 끝나는 시간 설정
		SetTimer(TIMER_CLICK_JUMP, m_dwClickJumpWaitTime*1000, NULL);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Tar 파일이 있는지 확인하고 있다면 풀어준다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHost::CheckTarFile()
{
	char cBuffer[BUF_SIZE] = { 0x00 };
	CString strEtcList = MNG_PROFILE_READ("Host", "EtcList");

	if(strEtcList.GetLength() == 0)
	{
		m_wndBack.AddLog("ok", true);
		m_wndBack.AddLog("Registering event-receiver... ");
		SetTimer(INIT_HANDLER_ID, INIT_HANDLER_TIME, NULL);
	}//if

	int nPos = 0;
	int nIdx = 0;
	CString strFileName, strExt, strFullPath, strTmp, strFolder, strInfo;
	CFileStatus fs, fs2;
	ULONGLONG ulTotalSize = 0;
	ULONGLONG ulTarSize;
	CStringArray aryTar;
	CString token = strEtcList.Tokenize(",", nPos);
	while(token != "")
	{
		token.Trim();
		strFileName = MNG_PROFILE_READ(token, "filename");
		strExt = strFileName.Right(4);
		if(strExt.CompareNoCase(".tar") == 0)
		{
			::GetLocalPath(strFullPath, strTmp, "", strFileName);
			if(CFile::GetStatus(strFullPath, fs) == TRUE)
			{				
				nIdx = strFullPath.ReverseFind('.');
				strFolder = strFullPath.Left(nIdx);
				if(CFile::GetStatus(strFolder, fs2) == TRUE)
				{
					//해당 폴더가 있다면 폴더안의 Tar_Info.ini 파일에서
					//tar 원본파일의 크기를 읽어서 현재 tar 파일과 크기가 다를때만 푼다.
					strInfo = strFolder;
					strInfo += "\\Tar_Info.ini";
					GetPrivateProfileString("Tar", "size", "", cBuffer, BUF_SIZE, strInfo);
					ulTarSize = (ULONGLONG)_atoi64(cBuffer);
					if(ulTarSize != fs.m_size)
					{
						aryTar.Add(strFullPath);
						ulTotalSize += fs.m_size;
					}//if
				}
				else
				{
					//폴더가 없다면 풀어야 한다.
					aryTar.Add(strFullPath);
					ulTotalSize += fs.m_size;
				}//if
			}
			else
			{
				//tar 파일이 없다...
				__DEBUG__("Invalid tar file path", strFullPath);
			}//if
		}//if

		token = strEtcList.Tokenize(_T(","), nPos);
	}//while

	if(aryTar.GetCount() == 0 || ulTotalSize == 0)
	{
		m_wndBack.AddLog("ok", true);
		m_wndBack.AddLog("Registering event-receiver... ");
		SetTimer(INIT_HANDLER_ID, INIT_HANDLER_TIME, NULL);
		return;
	}//if

	//Tar 파일 압축을 풀어주어야 한다.
	ST_EXTRACT_TAR_PARAM* pParam;
	pParam = new ST_EXTRACT_TAR_PARAM;
	pParam->pParentWnd = this;
	pParam->aryTar.Copy(aryTar);
	if(m_strArgTemplateID != _T(""))
	{
		pParam->bDelete = false;
	}
	else
	{
		pParam->bDelete = true;
	}//if

	m_wndBack.AddLog("Extract folder files...");
	m_wndBack.InitTarExtract();
	CWinThread* pThread = ::AfxBeginThread(CHost::ThreadExtractTarFile, pParam, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;
	pThread->ResumeThread();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Tar 파일을 푸는 thread \n
/// @param (LPVOID) param : (in) thread param
/// @return <형: UINT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CHost::ThreadExtractTarFile(LPVOID param)
{
	ST_EXTRACT_TAR_PARAM* pParam = (ST_EXTRACT_TAR_PARAM*)param;
	CHost* pHost = (CHost*)pParam->pParentWnd;

	STARTUPINFO stStartupInfo = {0}; 
	PROCESS_INFORMATION stProcessInfo;
	CString strExcute = ::GetAppPath();
	strExcute += "tar.exe";
	CString strSrcPath, strArg, strWorkingDir, strFolderName;
	CFileStatus fs;
	int nIdx;
	BOOL bRet;
	for(int i=0; i<pParam->aryTar.GetCount(); i++)
	{
		stStartupInfo.cb = sizeof(STARTUPINFO);
		strSrcPath = pParam->aryTar.GetAt(i);
		strArg.Format(" -xf \"%s\"", strSrcPath);
		nIdx = strSrcPath.ReverseFind('\\');
		strWorkingDir = strSrcPath.Left(nIdx);

		bRet =  CreateProcess(strExcute,
							(LPSTR)(LPCTSTR)strArg,
							NULL,
							NULL,
							FALSE,
							0,
							NULL,
							strWorkingDir,
							&stStartupInfo,
							&stProcessInfo);

		if(!bRet)
		{
			DWORD dwErrNo = GetLastError();

			LPVOID lpMsgBuf = NULL;
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
						| FORMAT_MESSAGE_IGNORE_INSERTS 
						| FORMAT_MESSAGE_FROM_SYSTEM     
						,	NULL
						, dwErrNo
						, 0
						, (LPTSTR)&lpMsgBuf
						, 0
						, NULL);

			CString strErrMsg;
			if(!lpMsgBuf)
			{
				strErrMsg.Format(_T("Unknow Error - CreateProcess [%d]"), dwErrNo);
			}
			else
			{
				strErrMsg.Format(_T("%s [%d]"), (LPCTSTR)lpMsgBuf, dwErrNo);
			}//if

			LocalFree( lpMsgBuf );
			__DEBUG__(strErrMsg, strSrcPath);

			continue;
		}//if

		//tar 파일이 풀릴때까지 대기한다.
		nIdx = strSrcPath.ReverseFind('.');
		strFolderName = strSrcPath.Left(nIdx);
		ULONGLONG ulFolderSize = 0;
		int nRate = 0;
		CFile::GetStatus(strSrcPath, fs);
		do
		{
			ulFolderSize = GetFolderSize(strFolderName);

			nRate = (ulFolderSize* 100) / fs.m_size;
			pHost->PostMessage(WM_PROGRESS_TAR, nRate, (LPARAM)(LPCSTR)strFolderName);
		} while(::WaitForSingleObject(stProcessInfo.hProcess, 200) == WAIT_TIMEOUT);	//while

		//폴더가 다 풀리면 폴더 수정날자를 변경하기위하여 폴더안에 ini 파일을 하나 만든다.
		CString strInfo, strSize;
		strInfo.Format("%s\\Tar_Info.ini", strFolderName);
		strSize.Format("%I64d", fs.m_size);
		WritePrivateProfileString("Tar", "size", strSize, strInfo);

		//tar 파일 삭제
		if(pParam->bDelete)
		{
			if(!DeleteFile(strSrcPath))
			{
				__DEBUG__("Fail to deleting file", strSrcPath);
			}//if
		}//if

		CloseHandle(stProcessInfo.hProcess);
		CloseHandle(stProcessInfo.hThread);
	}//for

	pHost->PostMessage(WM_COMPLETE_TAR, 0, 0);

	delete pParam;
	return 0;
}

LRESULT CHost::OnProgressTar(WPARAM wParam, LPARAM lParam)
{
	m_wndBack.SetTarExtractProgress((int)wParam, (LPCSTR)lParam);

	return 0;
}


LRESULT CHost::OnCompleteTar(WPARAM wParam, LPARAM lParam)
{
	m_wndBack.EndTarExtract();
	m_wndBack.AddLog("ok", true);
	m_wndBack.AddLog("Registering event-receiver... ");
	SetTimer(INIT_HANDLER_ID, INIT_HANDLER_TIME, NULL);

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 브라우져의 상태를 주기적으로 알리는 thread \n
/// @param (LPVOID) param : (in) 부모 포인터
/// @return <형: UINT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CHost::ThreadNotifyState(LPVOID param)
{
	DWORD dwEventRet = 0;
	CHost* pParent = (CHost*)param;
	CString strCmd, strBeforeCmd = "";
	COPYDATASTRUCT CDS;
	DWORD dwRet = 0;
	
	scratchUtil* aUtil = scratchUtil::getInstance();

	while(!pParent->m_bExitNotifyThread)
	{
/*
		dwEventRet = WaitForSingleObject(pParent->m_hEventThreadNotify, 5000);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			//CloseHandle(pParent->m_hEventThreadNotify);
			
			break;
		}//if
*/
		//5초마다 브라우져의 상태를 알린다.
		//"브라우저상태/브라우저ID/패키지명/PID/ERROR_STR"
		strCmd.Format("%d/%d/%s/%d/%s\0", pParent->m_nStateBRW, ::GetBRWId(), ::GetHostName(), pParent->m_nPid, pParent->m_strStateMsg);
		if(strCmd != strBeforeCmd)
		{
			strBeforeCmd = strCmd;
			__DEBUG__("BRW state msg", strCmd);
		}//if

		memset(&CDS, 0x00, sizeof(COPYDATASTRUCT));
		CDS.dwData = 30000;
		CDS.cbData = strCmd.GetLength();
		CDS.lpData = (PVOID)strCmd.GetBuffer(strCmd.GetLength());

		//Firmware View에 보낸다.
		scratchUtil* aUtil = scratchUtil::getInstance();
		ULONG ulPid = aUtil->getPid(_T("UBCFirmwareView.exe"));
		if(ulPid > 0)
		{
			HWND hWnd = aUtil->getWHandle(ulPid);		
			if(hWnd)
			{
				if(!::SendMessageTimeout(hWnd, WM_COPYDATA, 0, (LPARAM)&CDS, SMTO_ABORTIFHUNG|SMTO_NORMAL, 1000, &dwRet))
				{
					DWORD dwErrNo = GetLastError();

					LPVOID lpMsgBuf = NULL;
					FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
									| FORMAT_MESSAGE_IGNORE_INSERTS 
									| FORMAT_MESSAGE_FROM_SYSTEM     
									,	NULL
									, dwErrNo
									, 0
									, (LPTSTR)&lpMsgBuf
									, 0
									, NULL);

					CString strErrMsg;
					if(!lpMsgBuf)
					{
						strErrMsg.Format(_T("SendMessageTimeout - Unknow Error [%d]"), dwErrNo);
					}
					else
					{
						strErrMsg.Format(_T("SendMessageTimeout - %s [%d]"), (LPCTSTR)lpMsgBuf, dwErrNo);
					}//if

					LocalFree(lpMsgBuf);
					__DEBUG__(strErrMsg, _NULL);
				}//if
				//::SendMessage(hWnd , WM_COPYDATA , 0, (LPARAM)&CDS);
				//::PostMessage(hWnd , WM_COPYDATA , 0, (LPARAM)&CDS);
				strCmd.ReleaseBuffer();
			}
			else
			{
				__DEBUG__("Fail to get FirmwareView Handle", _NULL);
			}//if
		}
		else
		{
			__DEBUG__("Fail to find UBCFirmwareView.exe", _NULL);
		}//if

		dwEventRet = WaitForSingleObject(pParent->m_hEventThreadNotify, 5000);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			//CloseHandle(pParent->m_hEventThreadNotify);
			break;
		}//if
	}//while
	//aUtil->clearInstance();

	//pParent->m_bExitNotifyThread = true;

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 브라우져가 사용하는 자원을 모니터하는 thread \n
/// @param (LPVOID) param : (in) 부모 포인터
/// @return <형: UINT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CHost::ThreadMonitorResource(LPVOID param)
{
/*
	DWORD dwEventRet = 0;
	CHost* pParent = (CHost*)param;	
	scratchUtil* aUtil = scratchUtil::getInstance();

	DWORD dwFirstHandleCount = 0;
	DWORD dwFirstGDICount = 0;
	DWORD dwCurrentHandleCount = dwFirstHandleCount;
	DWORD dwCurrentGDICount = dwFirstGDICount;
	DWORD dwMaxHandleCount = dwFirstHandleCount;
	DWORD dwMaxGDICount = dwFirstGDICount;

	CString strHandle, strGDI;

	//////////////////////////////////////////////////////////////
	dwFirstHandleCount = aUtil->GetUsingHandleCount();
	dwMaxHandleCount = dwFirstHandleCount;
	dwFirstGDICount = aUtil->GetUsingGDICount();
	dwMaxGDICount = dwFirstGDICount;
	//////////////////////////////////////////////////////////////

	while(!pParent->m_bExitResourceThread)
	{
		dwEventRet = WaitForSingleObject(pParent->m_hEvtThreadResource, 60000);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			//CloseHandle(pParent->m_hEventThreadNotify);
			break;
		}//if

		dwCurrentHandleCount = aUtil->GetUsingHandleCount();
		if(dwCurrentHandleCount > dwMaxHandleCount)
		{
			dwMaxHandleCount = dwCurrentHandleCount;
		}//if

		dwCurrentGDICount = aUtil->GetUsingGDICount();
		if(dwCurrentGDICount > dwMaxGDICount)
		{
			dwMaxGDICount = dwCurrentGDICount;
		}//if

		strHandle.Format("[Handle]	%d ==> %d, MAX : %d", dwFirstHandleCount, dwCurrentHandleCount, dwMaxHandleCount);
		strGDI.Format("[GDI]	%d ==> %d, MAX : %d", dwFirstGDICount, dwCurrentGDICount, dwMaxGDICount);
		__DEBUG__("###########################################################", _NULL);
		__DEBUG__(strHandle, _NULL);
		__DEBUG__(strGDI, _NULL);
		__DEBUG__("###########################################################", _NULL);
	}//while
*/
	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 URL로 IE를 뛰운다. \n
/// @param (CString) strURL : (in) 이동할 URL
/////////////////////////////////////////////////////////////////////////////////
void CHost::RunIENavigate(CString strURL)
{
	if(strURL.GetLength() == 0)
	{
		return;
	}//if

	//IE의 경로를 구한다.
	TCHAR szPath[MAX_PATH] = { 0x00 };
	CString strIEPath;
	if(!SHGetSpecialFolderPath(NULL, szPath, CSIDL_PROGRAM_FILES, FALSE))
	{
		strIEPath = _T("C:\\Program Files\\");
	}
	else
	{
		strIEPath = szPath;
		strIEPath += _T("\\");
	}//if
	strIEPath += "Internet Explorer\\iexplore.exe";

	STARTUPINFO stStartupInfo = {0}; 
	PROCESS_INFORMATION stProcessInfo;
	CString strPath;
	BOOL bRet;
	strPath.Format("iexplore.exe %s", strURL);
	
	stStartupInfo.cb = sizeof(STARTUPINFO);
	bRet =  CreateProcess(strIEPath,
						(LPSTR)(LPCSTR)strPath,
						NULL,
						NULL,
						FALSE,
						0,
						NULL,
						NULL,
						&stStartupInfo,
						&stProcessInfo);

	if(!bRet)
	{
		DWORD dwErrNo = GetLastError();

		LPVOID lpMsgBuf = NULL;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
					| FORMAT_MESSAGE_IGNORE_INSERTS 
					| FORMAT_MESSAGE_FROM_SYSTEM     
					,	NULL
					, dwErrNo
					, 0
					, (LPTSTR)&lpMsgBuf
					, 0
					, NULL);

		CString strErrMsg;
		if(!lpMsgBuf)
		{
			strErrMsg.Format(_T("Unknow Error - CreateProcess [%d]"), dwErrNo);
		}
		else
		{
			strErrMsg.Format(_T("%s [%d]"), (LPCTSTR)lpMsgBuf, dwErrNo);
		}//if

		LocalFree( lpMsgBuf );
		__DEBUG__(strErrMsg, strPath);

		return;
	}//if

	//BRW Stop & Hide
	StopBRW();
	this->ShowWindow(SW_HIDE);

	//IEState thread
	CWinThread* pThread = ::AfxBeginThread(CHost::ThreadIEState, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;
	m_bExitIEStateThread = false;
	m_hEvtThreadIEState = CreateEvent(NULL, FALSE, FALSE, NULL);
	pThread->ResumeThread();		
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시스템에 IE가 구동중인지 검사하는 thread \n
/// @param (LPVOID) param : (in) 부모 포인터
/// @return <형: UINT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CHost::ThreadIEState(LPVOID param)
{
	DWORD dwEventRet = 0;
	CHost* pParent = (CHost*)param;

	while(!pParent->m_bExitIEStateThread)
	{
		dwEventRet = WaitForSingleObject(pParent->m_hEvtThreadIEState, 500);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			//CloseHandle(pParent->m_hEventThreadNotify);

			break;
		}//if

		HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if(hSnapshot == INVALID_HANDLE_VALUE)
		{
			__DEBUG__("Fail to create snapshot handle", _NULL);
			pParent->PostMessage(WM_ENDRUN_IE, 0, 0);
			return 0;
		}//if

		PROCESSENTRY32 pe32 ;
		pe32.dwSize = sizeof(PROCESSENTRY32);

		if(!Process32First(hSnapshot, &pe32))
		{
			__DEBUG__("Process32First failed.", _NULL);
			pParent->PostMessage(WM_ENDRUN_IE, 0, 0);
			return 0;
		}//if

		bool bFound = false;
		do
		{
			if(stricmp(pe32.szExeFile, "iexplore.exe") == 0)
			{		
				bFound = true;
				break;
			}//if
		} while(Process32Next(hSnapshot, &pe32));

		if(!bFound)
		{
			//실행중인 IE가 없다.
			__DEBUG__("Not found IE", _NULL);
			pParent->PostMessage(WM_ENDRUN_IE, 0, 0);
			return 0;
		}//if
	}//while

	return 0;
}


LRESULT CHost::OnEndRunIE(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__("Receive EndRunIE", _NULL);
	
	//Start BRW & Show
	StartBRW();
	this->ShowWindow(SW_SHOW);

	return 0;
}

bool CHost::Stop3rdPartyPlayer()
{
	__DEBUG__("Stop3rdPartyPlayer()", NULL);
	
	CString strArg = "3RDPARTY_PLAYER";

	CString fullpath = GetINIValue("UBCStarter.ini", strArg, "BINARY_NAME");
	if(fullpath.IsEmpty()){
		__DEBUG__("There is no ", strArg);
		return false;
	}
	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(fullpath, cDrive, cPath, cFilename, cExt);

	CString binary = cFilename;
	binary += cExt;

	if(scratchUtil::getInstance()->getPid(binary)<=0){
		__DEBUG__("process does not run", binary);
		return false;
	}

	if(!scratchUtil::getInstance()->stopProcess(strArg.GetBuffer()))
	{
		__DEBUG__("Fail to call stop", strArg);
		return false;
	}//if


	__DEBUG__("Succeed to call stop", strArg);
	return true;
}
bool CHost::Start3rdPartyPlayer()
{
	__DEBUG__("Start3rdPartyPlayer()", NULL);

	CString strArg = "3RDPARTY_PLAYER";

	CString fullpath = GetINIValue("UBCStarter.ini", strArg, "BINARY_NAME");
	if(fullpath.IsEmpty()){
		__DEBUG__("There is no ", strArg);
		return false;
	}
	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(fullpath, cDrive, cPath, cFilename, cExt);

	CString binary = cFilename;
	binary += cExt;

	__DEBUG__("Start3rdPartyPlayer()", binary);

	if(scratchUtil::getInstance()->getPid(binary)>0){
		__DEBUG__("process does already run", binary);
		return false;
	}

	if(!scratchUtil::getInstance()->startProcess(strArg.GetBuffer()))
	{
		__DEBUG__("Fail to call start", strArg);
		return false;
	}//if
	__DEBUG__("Succeed to call start", strArg);
	return true;
}


// 단말 카테코리 체크 (컨텐츠 카테고리 검사용)
CCriticalSection CHost::m_csCategoryCheck;
CMapStringToString CHost::m_mapCategory;

bool CHost::CheckCategory()
{
	static CString str_category = "";
	static int empty_count = 0;

	CString tmp_category = GetINIValue("UBCVariables.ini", "ROOT", "Category");
	tmp_category.Trim();

	if( tmp_category.GetLength() == 0 ) // empty일경우
	{
		// 1초간격으로 3번시도 (파일오픈상태의 empty리턴 방지)
		if( empty_count<3 )
		{
			empty_count++;
			return false;
		}
	}

	empty_count = 0;

	if( tmp_category == str_category ) return true;
	str_category = tmp_category;

	m_csCategoryCheck.Lock();
	m_mapCategory.RemoveAll();

	int pos = 0;
	CString str_token = str_category.Tokenize("/", pos);
	while( str_token != "" )
	{
		CString str_token_lower = str_token;
		str_token_lower.MakeLower();
		m_mapCategory.SetAt(str_token_lower, str_token);

		str_token = str_category.Tokenize("/", pos);
	}

	m_csCategoryCheck.Unlock();

	return true;
}

bool CHost::IsPlayableCategory(CMapStringToString& mapInclude, CMapStringToString& mapExclude)
{
	if( mapInclude.GetCount() == 0 && mapExclude.GetCount() == 0 )
		return true;

	m_csCategoryCheck.Lock();

	if( mapInclude.GetCount() > 0 )
	{
		POSITION pos = mapInclude.GetStartPosition();
		while( pos != NULL )
		{
			CString str_key, str_value;
			mapInclude.GetNextAssoc( pos, str_key, str_value );
			str_key.Trim();

			if( str_key.GetLength() == 0 ) continue;

			if( !m_mapCategory.Lookup(str_key, str_value) )
			{
				m_csCategoryCheck.Unlock();
				return false;
			}
		}
	}

	if( mapExclude.GetCount() > 0 )
	{
		POSITION pos = mapExclude.GetStartPosition();
		while( pos != NULL )
		{
			CString str_key, str_value;
			mapExclude.GetNextAssoc( pos, str_key, str_value );
			str_key.Trim();

			if( str_key.GetLength() == 0 ) continue;

			if( m_mapCategory.Lookup(str_key, str_value) )
			{
				m_csCategoryCheck.Unlock();
				return false;
			}
		}
	}

	m_csCategoryCheck.Unlock();
	return true;
}

/*
bool		
CHost::isMonitorOn()
{
	static int monitorOn = -1;

	if(monitorOn >=0) return bool(monitorOn);

	CString strPath = GetAppPath();
	strPath += "config\\";
	strPath += GetHostName();
	strPath += ".ini";
	monitorOn = GetPrivateProfileIntA("Host", "monitorOn", 0, strPath);

	return bool(monitorOn);
}
bool		
CHost::isMonitorOff()
{
	static int monitorOff = -1;

	if(monitorOff >=0) return bool(monitorOff);

	CString strPath = GetAppPath();
	strPath += "config\\";
	strPath += GetHostName();
	strPath += ".ini";
	monitorOff = GetPrivateProfileIntA("Host", "monitorOff", 0, strPath);

	return bool(monitorOff);

}
*/

#include <Psapi.h>

void CHost::CheckUpdater()
{
	// not topmost => skip
	if( m_bTopMostMode == false ) return;

	// get wnd & hwnd
	//CWnd* wnd = CWnd::GetForegroundWindow();
	//if( wnd == NULL ) return;

	HWND hwnd = ::GetForegroundWindow();//wnd->GetSafeHwnd();
	if( hwnd == NULL ) return;

	// get pid from hwnd
	ULONG pid = 0;
	::GetWindowThreadProcessId( hwnd, &pid );

	// get filename from pid
	HANDLE hProcess = NULL;
	if( (hProcess=OpenProcess(PROCESS_QUERY_INFORMATION|PROCESS_VM_READ, FALSE, pid)) == NULL )
		return;

	HMODULE hModule = NULL;
	DWORD cbNeeded = 0;
	char sz_fn[MAX_PATH] = {0};
	if( EnumProcessModules(hProcess, &hModule, sizeof(hModule), &cbNeeded) )
	{
		int n = GetModuleBaseName(hProcess, hModule, sz_fn, MAX_PATH);
		sz_fn[n] = 0;

		//
		CString str_fn = sz_fn;
		str_fn.MakeLower();
		if( str_fn.Find("utv_starter.exe") >= 0 )
		{
			::SetForegroundWindow(GetSafeHwnd());
		}
	}

	CloseHandle(hProcess);
}

void CHost::SendStopSignal()
{
	if( ::GetSendSyncProcessShortcut()==0 ) return;
	// 연동프로세스에 종료신호 보내기
	HWND hwnd = scratchUtil::getInstance()->getWHandle("UBCFirmwareView.exe");
	if(hwnd==NULL) return;

	static const char* close_signal = "E";

	COPYDATASTRUCT appInfo;
	appInfo.dwData = UBC_WM_TEMPLATE_SHORTCUT;  // 1000
	appInfo.lpData = (char*)close_signal;
	appInfo.cbData = strlen(close_signal)+1;
	__DEBUG__("UBC_WM_TEMPLATE_SHORTCUT", close_signal);
	::SendMessage(hwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
}

void CHost::killSyncProcess()
{
	CTemplatePlay* pclsTemplatePlay = NULL;
	for(int i=0; i<m_aryTemplatePlay.GetSize(); i++)
	{
		pclsTemplatePlay = (CTemplatePlay*)m_aryTemplatePlay.GetAt(i);
		pclsTemplatePlay->m_pclsTemplate->killSyncProcess();
	}//for
}

CArray<HWND, HWND>	CHost::m_lsShowWindowHwnd;
CArray<HWND, HWND>	CHost::m_lsHideWindowHwnd;

void CHost::PushFrontToShowWindow(HWND hWnd, int idx/*=0*/)
{
	__DEBUG__("PushFrontToShowWindow", (int)hWnd);
	if(idx>m_lsHideWindowHwnd.GetCount()) idx=m_lsHideWindowHwnd.GetCount();
	m_lsShowWindowHwnd.InsertAt(idx, hWnd);
}

void CHost::PushBackToShowWindow(HWND hWnd)
{
	__DEBUG__("PushBackToShowWindow", (int)hWnd);
	m_lsShowWindowHwnd.Add(hWnd);
}

void CHost::PushFrontToHideWindow(HWND hWnd, int idx/*=0*/)
{
	__DEBUG__("PushFrontToHideWindow", (int)hWnd);
	if(idx>m_lsHideWindowHwnd.GetCount()) idx=m_lsHideWindowHwnd.GetCount();
	m_lsHideWindowHwnd.InsertAt(idx, hWnd);
}

void CHost::PushBackToHideWindow(HWND hWnd)
{
	__DEBUG__("PushBackToHideWindow", (int)hWnd);
	m_lsHideWindowHwnd.Add(hWnd);
}

int CHost::GetTemplateIndex(LPCSTR lpszTemplateId)
{
	if(!::GetTemplateIndexInUserlog()) return 0;

	int nAryCount = GetCountOfTemplatePlayAry();
	CTemplatePlay* pclsPlay = NULL;
	CTemplate* pclsTemplate = NULL;
	for(UINT i=0; i<nAryCount; i++)
	{
		pclsPlay = (CTemplatePlay*)m_aryTemplatePlay.GetAt(i);
		if(pclsPlay==NULL) continue;
		if( pclsPlay->m_strTemplateID == lpszTemplateId )
		{
			if(i < nAryCount-1) return 1;
			return 2;
		}
	}
	return 0;
}
