/************************************************************************************/
/*! @file EtcFile.h
	@brief EtcFile 객체 클래스 정의 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/10/15\n
	▶ 참고사항:
		
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2010/10/15:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

class CEtcFile
{
public:
	CEtcFile();
	virtual ~CEtcFile() {};

	CEtcFile& operator= (const CEtcFile& clsFile)
	{
		m_strID					= clsFile.m_strID;
		m_strContentsName		= clsFile.m_strContentsName;
		m_strLocation			= clsFile.m_strLocation;
		m_strFilename			= clsFile.m_strFilename;
		m_strBGColor			= clsFile.m_strBGColor;
		m_strFGColor			= clsFile.m_strFGColor;
		m_strFont				= clsFile.m_strFont;
		m_strPromotionValueList	= clsFile.m_strPromotionValueList;

		m_sFontSize				= clsFile.m_sFontSize;
		m_sPlaySpeed			= clsFile.m_sPlaySpeed;
		m_sSoundVolume			= clsFile.m_sSoundVolume;
		m_ulContentsType			= clsFile.m_ulContentsType;
		m_ulContentsState		= clsFile.m_ulContentsState;
		m_ulRunningTime			= clsFile.m_ulRunningTime;
		m_ulVolume				= clsFile.m_ulVolume;

		m_direction				= clsFile.m_direction;
		m_align					= clsFile.m_align;

		m_width					= clsFile.m_width;
		m_height				= clsFile.m_height;
		m_currentComment		= clsFile.m_currentComment;

		m_strSaveKey			= clsFile.m_strSaveKey;

		m_strWizardXML			= clsFile.m_strWizardXML;
		m_strWizardFiles		= clsFile.m_strWizardFiles;
		
		for(int i=0; i<10; i++)
		{
			m_strComment[i]	= clsFile.m_strComment[i];
		}//for

		return *this;
	}

	bool operator== (const CEtcFile & clsFile)
	{
		if(m_strID					!= clsFile.m_strID)						return false;
		if(m_strContentsName		!= clsFile.m_strContentsName)			return false;
		if(m_strLocation			!= clsFile.m_strLocation)				return false;
		if(m_strFilename			!= clsFile.m_strFilename)				return false;
		if(m_strBGColor				!= clsFile.m_strBGColor)				return false;
		if(m_strFGColor				!= clsFile.m_strFGColor)				return false;
		if(m_strFont				!= clsFile.m_strFont)					return false;

		if(m_sFontSize				!= clsFile.m_sFontSize)					return false;
		if(m_sPlaySpeed				!= clsFile.m_sPlaySpeed)				return false;
		if(m_sSoundVolume			!= clsFile.m_sSoundVolume)				return false;
		if(m_ulContentsType			!= clsFile.m_ulContentsType)			return false;
		if(m_ulContentsState			!= clsFile.m_ulContentsState)		return false;
		if(m_ulRunningTime			!= clsFile.m_ulRunningTime)				return false;
		if(m_ulVolume				!= clsFile.m_ulVolume)					return false;

		if(m_direction				!= clsFile.m_direction)					return false;
		if(m_align					!= clsFile.m_align)						return false;

		if(m_width					!= clsFile.m_width)						return false;
		if(m_height					!= clsFile.m_height)					return false;
		if(m_currentComment			!= clsFile.m_currentComment)			return false;

		if(m_strSaveKey				!= clsFile.m_strSaveKey)				return false;

		if(m_strWizardXML			!= clsFile.m_strWizardXML)				return false;
		if(m_strWizardFiles			!= clsFile.m_strWizardFiles)			return false;

		for(int i=0; i<10; i++)
		{
			if(m_strComment[i] != clsFile.m_strComment[i])					return false;
		}//for

		if(m_strPromotionValueList	== clsFile.m_strPromotionValueList)
		{
			return true;
		}
		else
		{
			return false;
		}//if
	}

	bool operator!= (const CEtcFile & clsFile)
	{
		if(m_strID					!= clsFile.m_strID)						return true;
		if(m_strContentsName		!= clsFile.m_strContentsName)			return true;
		if(m_strLocation			!= clsFile.m_strLocation)				return true;
		if(m_strFilename			!= clsFile.m_strFilename)				return true;
		if(m_strBGColor				!= clsFile.m_strBGColor)				return true;
		if(m_strFGColor				!= clsFile.m_strFGColor)				return true;
		if(m_strFont				!= clsFile.m_strFont)					return true;

		if(m_sFontSize				!= clsFile.m_sFontSize)					return true;
		if(m_sPlaySpeed				!= clsFile.m_sPlaySpeed)				return true;
		if(m_sSoundVolume			!= clsFile.m_sSoundVolume)				return true;
		if(m_ulContentsType			!= clsFile.m_ulContentsType)			return true;
		if(m_ulContentsState		!= clsFile.m_ulContentsState)			return true;
		if(m_ulRunningTime			!= clsFile.m_ulRunningTime)				return true;
		if(m_ulVolume				!= clsFile.m_ulVolume)					return true;

		if(m_direction				!= clsFile.m_direction)					return true;
		if(m_align					!= clsFile.m_align)						return true;

		if(m_width					!= clsFile.m_width)						return true;
		if(m_height					!= clsFile.m_height)					return true;
		if(m_currentComment			!= clsFile.m_currentComment)			return true;

		if(m_strSaveKey				!= clsFile.m_strSaveKey)				return true;

		if(m_strWizardXML			!= clsFile.m_strWizardXML)				return true;
		if(m_strWizardFiles			!= clsFile.m_strWizardFiles)			return true;

		for(int i=0; i<10; i++)
		{
			if(m_strComment[i] != clsFile.m_strComment[i])					return true;
		}//for

		if(m_strPromotionValueList	== clsFile.m_strPromotionValueList)
		{
			return false;
		}
		else
		{
			return true;
		}//if
	} 

//protected:
	// attributes
	ciString		m_strID;
	ciString		m_strContentsName;
	ciString		m_strLocation;
	ciString		m_strFilename;
	ciString		m_strComment[10];
	ciString		m_strBGColor; 				// background color
	ciString		m_strFGColor; 				// foreground color
	ciString		m_strFont; 					// font
	cciStringList	m_strPromotionValueList;
	ciShort			m_sFontSize; 				// font size
	ciShort			m_sPlaySpeed; 				// tiker 등이 플레이 되는 속도
	ciShort 		m_sSoundVolume;
	ciLong 			m_ulContentsType;
	ciLong			m_ulContentsState;
	ciLong 			m_ulRunningTime;
	ULONGLONG		m_ulVolume;					// file size

	ciLong			m_direction;
	ciLong			m_align;

	ciShort			m_width;					// contents width (pixel)
	ciShort			m_height;					// contents height (pixel)
	ciShort			m_currentComment;			// 현재 문자가 들어가야할 자리번호

	CString			m_strSaveKey;				///<컨텐츠 개체를 저장하는 key

	ciString		m_strWizardXML;				// wizard 용 XML string
	ciString		m_strWizardFiles;			// wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

//public:
	// member function
	void		Save(LPCSTR lpszFullPath);
	void		Load(LPCSTR lpszFullPath, LPCSTR lpszLoadID);
	//CString		GEtcontentsKey(void);
};


typedef	CArray<CEtcFile*, CEtcFile*>	CEtcFileArray;