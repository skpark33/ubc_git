#pragma once

#include <Mmsystem.h> // Volume Control

class CAudioMixer {
public:
	static CAudioMixer*	getInstance();
	static void clearInstance();

	virtual ~CAudioMixer() ;

//protected:

	CAudioMixer();
	static CMutex m_mutex;
	static CAudioMixer* m_instance;

	bool	Initialize();
	bool	Uninitialize();
	bool	GetMasterVolumeControl();
	bool	GetMasterVolumeValue(DWORD &volume);
	bool	SetMasterVolumeValue(DWORD volume);
	bool	GetMasterMuteValue(bool &mute);
	bool	SetMasterMuteValue(bool mute);

	UINT		m_nNumMixers;
	HMIXER		m_hMixer;
	MIXERCAPS	m_mxcaps;

	CString m_strDstLineName;
	CString m_strVolumeControlName;

	DWORD	m_dwMinimum;
	DWORD	m_dwMaximum;
	DWORD	m_dwVolumeControlID;
	DWORD	m_dwMuteControlID;

	//
	DWORD	m_dwVolumeValue;
	bool	m_bMute;

public:

	bool	IncreaseVolume(int percent=5); // increase volume up to 10%
	bool	DecreaseVolume(int percent=5); // decrease volume down to 10%

	bool	ToggleMute();
};
