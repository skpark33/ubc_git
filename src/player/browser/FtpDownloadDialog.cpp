// FtpDownloadDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "BRW2.h"
#include "FtpDownloadDialog.h"
//#include "CMN/libCommon/ubcIniMux.h"
//#include "Dbghelp.h"

//#define SIZE_FILE_BUF	(1024*1024)	//1M
//#define MAX_RETRY_COUNT	3

// CFtpDownloadDialog 대화 상자입니다.


IMPLEMENT_DYNAMIC(CFtpDownloadDialog, CDialog)

CFtpDownloadDialog::CFtpDownloadDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFtpDownloadDialog::IDD, pParent)
	, m_pParentWnd (pParent)
	//, m_nTotalSize (0)
	//, m_pFtpConnection (NULL)
	, m_bShowWnd(true)
	//, m_nRetryCount(0)
	//, m_paryDownload(NULL)
	//, m_pDownResult(NULL)
{

}

CFtpDownloadDialog::~CFtpDownloadDialog()
{
	//if(m_pFtpConnection)
	//{
	//	m_pFtpConnection->Close();
	//	delete m_pFtpConnection;
	//	m_pFtpConnection = NULL;
	//}//if

	//if(m_pDownResult)
	//{
	//	delete m_pDownResult;
	//}//if
}

void CFtpDownloadDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_PROGRESS_CURRENT, m_pbarCurrent);
	DDX_Control(pDX, IDC_PROGRESS_TOTAL, m_pbarTotal);
	//DDX_Control(pDX, IDC_STATIC_STATUS, m_staticStatus);
}


BEGIN_MESSAGE_MAP(CFtpDownloadDialog, CDialog)
	//ON_MESSAGE(WM_COMPLETE_FTP_CONNECTION, OnCompleteFtpConnection)
	//ON_MESSAGE(WM_COMPLETE_FILE_SIZE, OnCompleteFileSize)
	//ON_MESSAGE(WM_COMPLETE_FTP_DOWNLOAD, OnCompleteFtpDownload)
	//ON_MESSAGE(WM_COMPLETE_INIT_DOWNLOAD_RESULT, OnCompleteInitDownloadResult)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CFtpDownloadDialog 메시지 처리기입니다.

void CFtpDownloadDialog::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::PostNcDestroy();

	delete this;
}

BOOL CFtpDownloadDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFtpDownloadDialog::StartDownload(bool bShowWindow)
{
	//
	m_pbarTotal.SetStyle(PROGRESS_TEXT);
	m_pbarTotal.SetBkColor(RGB(32, 34, 36));
	m_pbarTotal.SetForeColor(RGB(31, 200, 248));
	m_pbarTotal.SetTextBkColor(RGB(255, 255, 255));			//프로그래시브바 위에 글자색
	//m_pbarTotal.SetTextColor(RGB(0, 0, 255));
	m_pbarTotal.SetTextForeColor(RGB(31, 200, 248));		//바탕의 글자색
	m_pbarTotal.SetRange(0, 1000);
	m_pbarTotal.SetPos(0);
	m_pbarTotal.SetText("");
	
	if(bShowWindow)
	{
		//CenterWindow();
		ShowWindow(SW_SHOW);
	}//if
/*
	CWinThread* pThread = ::AfxBeginThread(CFtpDownloadDialog::ThreadInitDownloadResult, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;
	pThread->ResumeThread();

	m_nRetryCount = 0;
*/
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// <함수설명 기술> \n
/// @param (WPARAM) wParam : (in) 0
/// @param (LPARAM) lParam : (in) 0
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CFtpDownloadDialog::OnCompleteInitDownloadResult(WPARAM wParam, LPARAM lParam)
{
	if(m_pDownResult)
	{
		GetConnection();
	}
	else
	{
		CString str = "Fail to initialize download result.";
		m_pbarTotal.SetText(str);
		m_pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)str);
		__ERROR__(str, _NULL);

		//m_pParentWnd->PostMessage(WM_COMPLETE_FTP_DOWNLOAD, FALSE);
		//DestroyWindow();
		GetConnection();
	}//if

	return 0;
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// <함수설명 기술> \n
/// @param (LPVOID) param : (in) Thread owner 포인터
/// @return <형: UINT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CFtpDownloadDialog::ThreadInitDownloadResult(LPVOID param)
{
	CFtpDownloadDialog* pParent = (CFtpDownloadDialog*)param;
	
	//다운로드 기록 초기화
	pParent->m_pDownResult = new CDownloadResult(pParent->m_paryDownload);

	//다운로드 기록 시작
	if(pParent->m_pDownResult)
	{
		pParent->m_pDownResult->BeginDownload();
	}//if

	pParent->PostMessage(WM_COMPLETE_INIT_DOWNLOAD_RESULT, 0, 0);

	return 0;
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FTP Connection을 가져오는 threda를 시작한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CFtpDownloadDialog::GetConnection()
{
	m_pbarTotal.SetText("Connecting to server...");

	GET_FTP_CONNECTION_PARAM* param = new GET_FTP_CONNECTION_PARAM;
	if(param != NULL)
	{
		
		param->pParentWnd		= m_pParentWnd;
		param->pWnd				= this;
		param->pInternetSession	= &m_internetSession;
		param->ppFtpConnection	= &(m_pFtpConnection);

		CWinThread* pThread = ::AfxBeginThread(CFtpDownloadDialog::GetFtpConnectionThread, param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		pThread->m_bAutoDelete = TRUE;
		pThread->ResumeThread();
	}//if
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 받을 파일들의 사이즈를 구하는 thread를 시작한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CFtpDownloadDialog::GetFileSize()
{
	m_pbarTotal.SetText("Checking download file size...");

	GET_FILESIZE_THREAD_PARAM* param = new GET_FILESIZE_THREAD_PARAM;
	if(param != NULL)
	{
		param->pParentWnd		= m_pParentWnd;
		param->pWnd				= this;
		param->pFtpConnection	= m_pFtpConnection;
		param->pArray			= m_paryDownload;

		CWinThread* pThread = ::AfxBeginThread(CFtpDownloadDialog::GetFileSizeThread, param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		pThread->m_bAutoDelete = TRUE;
		pThread->ResumeThread();
	}//if
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FTP에서 파일을 다운로드하는 thread를 시작한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CFtpDownloadDialog::GetDownload()
{
	m_pbarTotal.SetText("Starting file download...");
	
	FTP_DOWNLOAD_PARAM* param = new FTP_DOWNLOAD_PARAM;
	if(param != NULL)
	{
		param->pParentWnd		= m_pParentWnd;
		param->pWnd				= this;
		param->pbarTotal		= &m_pbarTotal;
		param->pArrayDownload	= m_paryDownload;
		param->pFtpConnection	= m_pFtpConnection;
		param->nTotalSize		= m_nTotalSize;

		CWinThread* pThread = ::AfxBeginThread(CFtpDownloadDialog::FtpDownloadThread, param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		pThread->m_bAutoDelete = TRUE;
		pThread->ResumeThread();
	}//if
}
*/
/*
LRESULT CFtpDownloadDialog::OnCompleteFtpConnection(WPARAM wParam, LPARAM lParam)
{
	if(wParam == TRUE)
	{
		GetFileSize();	
	}
	else
	{
		// get ftp connection ERROR !!!
		m_pbarTotal.SetText("Fail to get FTP connection.");
		m_pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)"Fail to get FTP connection.");
		__ERROR__("\tFail to get FTP connection.", _NULL);

		if(m_nRetryCount++ < MAX_RETRY_COUNT)
		{
			GetConnection();
		}
		else
		{
			if(m_pDownResult)
			{
				//m_pDownResult->m_nState = E_STATE_COMPLETE_FAIL;
				//m_pDownResult->m_nSuccessCount = 0;
				int nCount = m_paryDownload->GetCount();
				CDownloadFile* pInfo = NULL;
				for(int i=0; i<nCount; i++)
				{
					pInfo = m_paryDownload->GetAt(i);
					pInfo->SetEnd(E_REASON_CONNECTION_FAIL, E_STATE_COMPLETE_FAIL, 0, false);
				}//for
				//m_pDownResult->m_nFailCount = nCount;
				m_pDownResult->EndDownload();
			}//if

			m_pParentWnd->PostMessage(WM_COMPLETE_FTP_DOWNLOAD, FALSE);
			DestroyWindow();
		}//if
	}//if

	return 0;
}
*/
/*
LRESULT CFtpDownloadDialog::OnCompleteFileSize(WPARAM wParam, LPARAM lParam)
{
	if(wParam == TRUE)
	{
		int nFialCount = 0;
		int nLocalCount = 0;
		int nCount = m_paryDownload->GetCount();
		CDownloadFile* pInfo = NULL;
		for(int i=0; i<nCount; i++)
		{
			pInfo = m_paryDownload->GetAt(i);
			if(pInfo->m_nDownloadState == E_STATE_COMPLETE_FAIL)			//서버에서 파일을 찾지 못함
			{
				nFialCount++;
			}
			else if(pInfo->m_nDownloadState == E_STATE_COMPLETE_SUCCESS)	//로컬에 파일이 존재함
			{
				nLocalCount++;
			}
			else
			{
				m_nTotalSize += pInfo->m_ulFileSize;
			}//if
		}//if

		__DEBUG__("FTP : Download array count", nCount);
		__DEBUG__("FTP : File not found count", nFialCount);
		__DEBUG__("FTP : Local exist count", nLocalCount);
		__DEBUG__("FTP : Download total size", m_nTotalSize);

		if(nFialCount == nCount || nLocalCount == nCount)
		{
			if(m_pDownResult)
			{
				m_pDownResult->EndDownload();
			}//if

			m_pParentWnd->PostMessage(WM_COMPLETE_FTP_DOWNLOAD, TRUE);
			DestroyWindow();
		}
		else
		{
			GetDownload();
		}//if
	}
	else	//실제 이 루틴을 타는 경우는 없다?
	{
		m_pbarTotal.SetText("Fail to get FTP file size.");
		// get ftp file size ERROR !!!
		m_pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)"Fail to get FTP file size.");
		__ERROR__("\tFail to get FTP file size.", _NULL);

		if(m_nRetryCount++ < MAX_RETRY_COUNT)
		{
			GetFileSize();
		}
		else
		{
			if(m_pDownResult)
			{
				//m_pDownResult->m_nState = E_STATE_COMPLETE_FAIL;
				m_pDownResult->EndDownload();
			}//if

			m_pParentWnd->PostMessage(WM_COMPLETE_FTP_DOWNLOAD, FALSE);
			DestroyWindow();
		}//if
	}//if

	return 0;
}
*/
/*
LRESULT CFtpDownloadDialog::OnCompleteFtpDownload(WPARAM wParam, LPARAM lParam)
{
	if(wParam == TRUE)
	{
		if(m_pDownResult)
		{
			m_pDownResult->EndDownload();
		}//if

		m_pbarTotal.SetText("File transfer successful.");
		// get ftp file download success
		m_pParentWnd->PostMessage(WM_COMPLETE_FTP_DOWNLOAD, TRUE);
		DestroyWindow();
	}
	else
	{
		m_pbarTotal.SetText("Fail to get FTP file download.");
		// get ftp file download ERROR !!!
		m_pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)"Fail to get FTP file download.");
		__ERROR__("\tFail to get FTP file download.", _NULL);

		if(m_nRetryCount++ < MAX_RETRY_COUNT)
		{
			GetDownload();
		}
		else
		{
			if(m_pDownResult)
			{
				m_pDownResult->EndDownload();
			}//if

			m_pParentWnd->PostMessage(WM_COMPLETE_FTP_DOWNLOAD, FALSE);
			DestroyWindow();
		}//if
	}//if

	return 0;
}
*/
/*
UINT CFtpDownloadDialog::GetFtpConnectionThread(LPVOID pParam)
{
	GET_FTP_CONNECTION_PARAM* param = (GET_FTP_CONNECTION_PARAM*)pParam;

	CString strServerAddr, strServerId, strServerPwd, strServerPort;

	ubcMuxData* pFtpInfo = ubcIniMux::getInstance()->getMuxData(GetProgramSite());
	if(pFtpInfo)
	{
		strServerAddr = pFtpInfo->getFTPAddress();
		strServerPort.Format("%d", pFtpInfo->ftpPort);
		strServerId = pFtpInfo->ftpId.c_str();
		strServerPwd = pFtpInfo->ftpPasswd.c_str();
		if(!pFtpInfo->isAvail())
		{
			__DEBUG__("None ftp server available", _NULL);
			ubcMux::clearInstance();
			param->pWnd->PostMessage(WM_COMPLETE_FTP_CONNECTION, FALSE);
			delete param;

			return 0;
		}//if

		ubcMux::clearInstance();
	}
	else
	{
		__DEBUG__("Fail to get ftp server information", _NULL);
		ubcMux::clearInstance();
		param->pWnd->PostMessage(WM_COMPLETE_FTP_CONNECTION, FALSE);
		delete param;

		return 0;
	}//if

	__DEBUG__("ServerAddr", strServerAddr);
	__DEBUG__("ServerPort", strServerPort);
	__DEBUG__("ServerId",	strServerId);
	__DEBUG__("ServerPwd",	strServerPwd);

	try
	{
		CFtpConnection* connect = param->pInternetSession->GetFtpConnection(strServerAddr,
			strServerId,
																			strServerPwd,
																			atoi(strServerPort),
																			TRUE);
		*(param->ppFtpConnection) = connect;
		if(*(param->ppFtpConnection) != NULL)
		{
			__DEBUG__("FTP connect ok", _NULL);
			param->pWnd->PostMessage(WM_COMPLETE_FTP_CONNECTION, TRUE);
			delete param;

			return 0;
		}//if
	}
	catch(CInternetException* ex)
	{
		CString msg;
		TCHAR szCause[1024];
		ex->GetErrorMessage(szCause, 1024);
		msg.Format("FTP exception : (%d) %s", ex->m_dwError, szCause);
		ex->Delete();
		__ERROR__(msg, _NULL);

		param->pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)msg);
	}//try

	__DEBUG__("FTP connect fail", _NULL);
	param->pWnd->PostMessage(WM_COMPLETE_FTP_CONNECTION, FALSE);
	delete param;

	return 0;
}
*/
/*
UINT CFtpDownloadDialog::GetFileSizeThread(LPVOID pParam)
{
	GET_FILESIZE_THREAD_PARAM* param = (GET_FILESIZE_THREAD_PARAM*)pParam;

	CFtpFileFind clsFind(param->pFtpConnection);
	BOOL bFind = FALSE;
	CString strLocalPath, strTmpPath, strServerPath;
	CFileStatus fs;
	CDownloadFile* pInfo = NULL;
	ULONGLONG ulLocalSize = 0;

	__DEBUG__("Check file array count", param->pArray->GetCount());
	for(int i=0; i<param->pArray->GetCount(); i++)
	{
		try
		{
			pInfo = param->pArray->GetAt(i);

			//Get remote size 
			strServerPath = pInfo->m_strServerPath;
			strServerPath.Append(pInfo->m_strFileName);

			bFind = clsFind.FindFile(strServerPath);
			if(bFind)
			{
				clsFind.FindNextFile();
				pInfo->m_ulFileSize = clsFind.GetLength();

				//Get local size
				if(IsFontFile(pInfo->m_strFileName))
				{
					char szWinDir[MAX_PATH] = { 0x00 };
					if(GetWindowsDirectory(szWinDir, MAX_PATH) == 0)
					{
						__DEBUG__("Fail to get win dir", _NULL);
						pInfo->SetEnd(E_REASON_EXCEPTION_FILE, E_STATE_COMPLETE_FAIL, 0, false);
						continue;
					}//if

					CString strFontDir;
					strFontDir.Format("%s\\fonts\\%s", szWinDir, pInfo->m_strFileName);

					if(CFile::GetStatus(strFontDir, fs) == TRUE)
					{
						if(fs.m_size == pInfo->m_ulFileSize)
						{
							pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
						}//if
					}//if
				}
				else
				{
					//Tmp 와 ENC 폴더 양쪽을 비교해교하여야 한다.
					GetLocalPath(strLocalPath, strTmpPath, pInfo->m_strServerPath, pInfo->m_strFileName);
					if(CFile::GetStatus(strLocalPath, fs) == TRUE)
					{
						if(fs.m_size == pInfo->m_ulFileSize)
						{
							pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
							if(IsTarFile(pInfo->m_strFileName))
							{
								//tar 파일을 압축해제 한다.
								if(!ExtractTarFile(strLocalPath, strLocalPath))
								{
									__ERROR__("Tar file extract fail", strLocalPath);
								}//if
							}//if
						}//if
					}
					else if(CFile::GetStatus(strTmpPath, fs) == TRUE)
					{
						if(fs.m_size == pInfo->m_ulFileSize)
						{
							pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
							if(IsTarFile(pInfo->m_strFileName))
							{
								//tar 파일을 압축해제 한다.
								if(!ExtractTarFile(strTmpPath, strLocalPath))
								{
									__ERROR__("Tar file extract fail", strLocalPath);
								}//if
							}//if

							//Tmp폴더에 있는 파일은 ENC 폴더로 옮겨준다.
							if(!MoveFileEx(strTmpPath, strLocalPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
							{
								__DEBUG__("Contents file replace fail", strLocalPath);
							}//if
						}//if
					}//if
				}//if
			}
			else
			{
				//Remote에 없다면 local에서만 비교한다.
				__DEBUG__("File find fail in remote path", strServerPath);
				//서버에는 없지만 로컬에 있다면...
				//Tmp 와 ENC 폴더 양쪽을 비교해교하여야 한다.
				GetLocalPath(strLocalPath, strTmpPath, pInfo->m_strServerPath, pInfo->m_strFileName);
				if(CFile::GetStatus(strLocalPath, fs) == TRUE)
				{
					if(fs.m_size == pInfo->m_ulFileSize)
					{
						pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
						if(IsTarFile(pInfo->m_strFileName))
						{
							//tar 파일을 압축해제 한다.
							if(!ExtractTarFile(strTmpPath, strLocalPath))
							{
								__ERROR__("Tar file extract fail", strLocalPath);
							}//if
						}//if
					}//if
				}
				else if(CFile::GetStatus(strTmpPath, fs) == TRUE)
				{
					if(fs.m_size == pInfo->m_ulFileSize)
					{
						pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
						if(IsTarFile(pInfo->m_strFileName))
						{
							//tar 파일을 압축해제 한다.
							if(!ExtractTarFile(strTmpPath, strLocalPath))
							{
								__ERROR__("Tar file extract fail", strLocalPath);
							}//if
						}//if

						//Tmp폴더에 있는 파일은 ENC 폴더로 옮겨준다.
						if(!MoveFileEx(strTmpPath, strLocalPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
						{
							__DEBUG__("Contents file replace fail", strLocalPath);
						}//if
					}//if
				}
				else
				{
					pInfo->SetEnd(E_REASON_FILE_NOTFOUND, E_STATE_COMPLETE_FAIL, 0, false);
				}//if
			}//if
		}
		catch(CInternetException* ex)
		{
			CString msg;
			TCHAR szCause[1024];
			ex->GetErrorMessage(szCause, 1024);
			msg.Format("FTP file find exception : (%d) %s", ex->m_dwError, szCause);
			ex->Delete();

			__ERROR__(msg, _NULL);
			pInfo->SetEnd(E_REASON_EXCEPTION_FTP, E_STATE_COMPLETE_FAIL, 0, false);
			param->pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)msg);
		}//try
	}//for
	clsFind.Close();

	param->pWnd->PostMessage(WM_COMPLETE_FILE_SIZE, TRUE);
	delete param;

	return 0;
}
*/
/*
UINT CFtpDownloadDialog::FtpDownloadThread(LPVOID pParam)
{
	//
	FTP_DOWNLOAD_PARAM* param = (FTP_DOWNLOAD_PARAM*)pParam;
	CFtpDownloadDialog* pParent = (CFtpDownloadDialog*)param->pWnd;

	ULONGLONG nCompleteDownloadSize = 0;
	ULONGLONG nCurrentDownloadSize = 0;
	ULONGLONG nCurrentFileSize = 0;

	DWORD	dwStartTick = 0;
	DWORD	dwEndTick = 0;

	CString strError, strStatus, strServerPath, strLocalFullPath, strLocalTempFullPath;
	CDownloadFile* pInfo = NULL;
	CFile* fileServer = NULL;
	CFile fileLocal;
	int nCountDownload = 0;
	int nCountProgress = 0;
	int nRead = 0;
	int nRemainSecCurrent, nRemainSecTotal, nPercentTotal;
	int nCount = param->pArrayDownload->GetCount();
	BYTE* pbtBuf = new BYTE[SIZE_FILE_BUF];

	//다운로드 상태를 알리는 주기
	CString strPeriod = GetINIValue("UBCVariables.ini", "ROOT", "AgentConnectionPeriod");
	DWORD dwPeriod = atoi(strPeriod)*1000;
	if(dwPeriod == 0)
	{
		dwPeriod = 60*1000;
	}//if
	DWORD dwEventTick = ::GetTickCount();

	for(int i=0; i<nCount; i++)
	{
		pInfo = param->pArrayDownload->GetAt(i);
		if(pInfo->m_nDownloadState != E_STATE_INIT)
		{
			nCountProgress++;
			continue;
		}//if

		//다운로드 시작
		pInfo->SetStart();

		nCompleteDownloadSize += nCurrentFileSize;
		nCurrentFileSize = pInfo->m_ulFileSize;
		nCurrentDownloadSize = 0;

		param->pbarTotal->SetText("00%% (00:00:00)");

		dwStartTick = 0;
		dwEndTick = 0;
		try
		{
			strServerPath = pInfo->m_strServerPath;
			strServerPath.Append(pInfo->m_strFileName);

			GetLocalPath(strLocalFullPath, strLocalTempFullPath, pInfo->m_strServerPath, pInfo->m_strFileName);

			__DEBUG__("LocalFullPath", strLocalFullPath);
			__DEBUG__("LocalTempFullPath", strLocalTempFullPath);

			::MakeSureDirectoryPathExists(strLocalFullPath);
			::MakeSureDirectoryPathExists(strLocalTempFullPath);

			// download
			fileServer = param->pFtpConnection->OpenFile(strServerPath);
			if(!fileServer)
			{
				strError.Format("Can't connect to ServerPath : %s", strServerPath);
				param->pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strError);
				__ERROR__(strError, _NULL);

				pInfo->SetEnd(E_REASON_FILE_NOTFOUND, E_STATE_COMPLETE_FAIL, 0, false);
				nCountProgress++;
				continue;
			}//if

			if(!fileLocal.Open(strLocalTempFullPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
			{
				strError.Format("Can't create to LocalTempFullPath : %s", strLocalTempFullPath);
				param->pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strError);
				__ERROR__(strError, _NULL);

				pInfo->SetEnd(E_REASON_EXCEPTION_FILE, E_STATE_COMPLETE_FAIL, 0, false);
				fileServer->Close();
				delete fileServer;
				nCountProgress++;
				continue;
			}//if

			dwStartTick = ::GetTickCount();
			memset(pbtBuf, 0x00, SIZE_FILE_BUF);

			// read & write
			try
			{
				nRead = 0;
				while((nRead = fileServer->Read(pbtBuf, SIZE_FILE_BUF)) != 0)
				{
					fileLocal.Write(pbtBuf, nRead);
					nCurrentDownloadSize += nRead;

					dwEndTick = ::GetTickCount();

					nRemainSecCurrent = (dwEndTick-dwStartTick) * (pInfo->m_ulFileSize - nCurrentDownloadSize) / nCurrentDownloadSize / 1000;
					nRemainSecTotal = (dwEndTick-dwStartTick) * ( param->nTotalSize - nCompleteDownloadSize - nCurrentDownloadSize) / nCurrentDownloadSize / 1000;

					CTimeSpan tmspRemainTotal(nRemainSecTotal);
					nPercentTotal = (nCompleteDownloadSize+nCurrentDownloadSize)*1000/param->nTotalSize;
					strStatus.Format("%02d%% (%s)", nPercentTotal/10, tmspRemainTotal.Format("%H:%M:%S"));
					param->pbarTotal->SetText(strStatus);
					param->pbarTotal->SetPos(nPercentTotal);

					if(pParent->m_pDownResult)
					{
						pParent->m_pDownResult->m_nSuccessCount = nCountDownload;
						//다운로드 상태 알림
						if((dwEndTick - dwEventTick) >= dwPeriod)
						{
							pParent->m_pDownResult->SendUpdateEvent(true);
							dwEventTick = dwEndTick;
						}//if
					}//if					
				}//while
			}
			catch(CFileException* ex)
			{
				CString msg;
				TCHAR szCause[1024];
				ex->GetErrorMessage(szCause, 1024);
				msg.Format("(%d) (%s) %s", ex->m_cause, ex->m_strFileName, szCause);
				ex->Delete();

				strError.Format("File R/W Exception : %s", msg);
				param->pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strError);
				__ERROR__(strError, _NULL);

				pInfo->SetEnd(E_REASON_EXCEPTION_FILE, E_STATE_COMPLETE_FAIL, 0, false);
				fileServer->Close();
				delete fileServer;
				fileLocal.Close();
				nCountProgress++;
				continue;
			}//try

			fileLocal.Close();

			//Rename
			//폰트파일은 Windows/fonts 폴더에 복사해 준다.
			if(IsFontFile(strLocalTempFullPath))
			{
				if(!RegisterFont(pInfo->m_strFileName, strLocalTempFullPath))
				{
					__DEBUG__("Font register fail", pInfo->m_strFileName);
				}
				else
				{
					__DEBUG__("Font register success", pInfo->m_strFileName);
				}//if
			}
			else if(IsPptFile(strLocalFullPath))
			{
				//PPT 파일은 읽기전용 속성을 해제하여 덮어써야한다.
				//읽기 전용속성을 주지 않으면 파일 Open과정에 파일의 크기가 변경되는 경우가 있다...
				SetFileAttributes(strLocalFullPath, FILE_ATTRIBUTE_NORMAL);		//읽기전용 해제

				if(!MoveFileEx(strLocalTempFullPath, strLocalFullPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
				{
					strError.Format("Contents file replace fail : %s", strLocalFullPath);
					param->pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strError);
					__ERROR__(strError, _NULL);
				}//if

				SetFileAttributes(strLocalFullPath, FILE_ATTRIBUTE_READONLY);	//읽기전용 설정
			}
			else
			{
				if(MoveFileEx(strLocalTempFullPath, strLocalFullPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
				{
					if(IsTarFile(strLocalFullPath))
					{
						if(!ExtractTarFile(strLocalFullPath, strLocalFullPath))
						{
							__ERROR__("Tar file extract fail", strLocalFullPath);
						}//if
					}//if
				}
				else
				{
					strError.Format("Contents file replace fail : %s", strLocalFullPath);
					param->pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strError);
					__ERROR__(strError, _NULL);
				}//if				
			}//if

			pInfo->SetEnd(E_REASON_DOWNLOAD_SUCCESS, E_STATE_COMPLETE_SUCCESS, nCurrentDownloadSize, true);

			fileServer->Close();
			delete fileServer;
			nCountProgress++;
			nCountDownload++;

			if(pParent->m_pDownResult)
			{
				pParent->m_pDownResult->m_nSuccessCount = nCountDownload;
				//다운로드 상태 알림
				if((dwEndTick - dwEventTick) >= dwPeriod)
				{
					pParent->m_pDownResult->SendUpdateEvent(true);
					dwEventTick = dwEndTick;
				}//if
			}//if				
		}
		catch(CInternetException* ex)
		{
			CString msg;
			TCHAR szCause[1024];
			ex->GetErrorMessage(szCause, 1024);
			msg.Format("(%d) %s", ex->m_dwError, szCause);
			ex->Delete();

			strError.Format("Internet Exception : %s", msg);
			param->pParentWnd->SendMessage(WM_ERROR_MESSAGE, (WPARAM)(LPCSTR)strError);
			__ERROR__(strError, _NULL);

			pInfo->SetEnd(E_REASON_EXCEPTION_FTP, E_STATE_COMPLETE_FAIL, 0, false);
			//fileServer->Close();
			//delete fileServer;
			//fileLocal.Close();
			nCountProgress++;
		}//try
	}//for

	if(nCountProgress == nCount)
		param->pWnd->PostMessage(WM_COMPLETE_FTP_DOWNLOAD, TRUE);
	else
		param->pWnd->PostMessage(WM_COMPLETE_FTP_DOWNLOAD, FALSE);

	delete[]pbtBuf;
	delete param;

	return 0;
}
*/

void CFtpDownloadDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if(m_pbarTotal.GetSafeHwnd() != NULL)
	{
		CRect rect;
		GetClientRect(&rect);

		m_pbarTotal.MoveWindow(rect.left, rect.top, rect.Width(), rect.Height(), TRUE);
	}//if
}
