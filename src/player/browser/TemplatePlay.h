/************************************************************************************/
/*! @file TemplatePlay.h
	@brief Template의 play rule을 갖는 클래스 정의파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2006/10/27\n
	▶ 참고사항:
		- Host.h에 정의하였다가 파일로 분리
		   
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	-# 2006/10/27:정운형:Host.h에 정의하였다가 다른 클래스에서 include하기 위하여 파일을 분리
************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

class CTemplate;

//! TemplatePlay 방법을 갖는 클래스
/*!
	기존의 dtcInfoList를 대체하는 클래스로서, \n
	template의 ID, 재생반복횟수, 재생방법 플래그, \n
	schedule의 play order 배열을 갖는다.\n
	재생 방법 플래그가 true이면, unPlaycount 횟수 만큼 template가반복 재생되며,\n
	false이면 m_aryPlayOrder의 멤버 순서에따라 schedule이 재생된다.\n
*/
class CTemplatePlay
{
public:
	CTemplatePlay(CString strTemplate);							///<기본 생성자
	virtual~CTemplatePlay(void)	{ m_aryPlayOrder.RemoveAll(); }	///<소멸자

	/*void		SetTemplatePtr(CTemplate* pclsTemplate);		///<해당하는 Template ID의 template 포인터를 설정하는 함수
	CTemplate*	GetTemplatePtr(void);							///<해당하는 Template ID의 template 포인터를 반환하는 함수*/

//protected:
	CString			m_strTemplateID;							///<template의 ID
	UINT			m_unPlayCount;								///<template가 반복재생되는 횟수
	bool			m_bUsePlayCount;							///<template가 반복재생될것인지, play order 배열을 이용하여 재생될것인지를 나타내는 플래그
	CStringArray	m_aryPlayOrder;								///<Frame에서 재생하는 schedule의 play order을 갖는 배열
	CTemplate*		m_pclsTemplate;								///<해당하는 Template ID의 template 포인터
};