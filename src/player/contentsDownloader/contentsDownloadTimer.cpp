#include "common/libHttpRequest/HttpRequest.h"
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libBase/ciTime.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include "common/libCommon/ubcIni.h"
#include "contentsDownloadTimer.h"
#include "contentsDownloaderWorld.h"


ciSET_DEBUG(9, "contentsDownloadTimer");


contentsDownloadTimer::contentsDownloadTimer(const char* site) 
{
	ciDEBUG2(5, ("contentsDownloadTimer(%s)", site));

	_status = DS_INIT;
	_isInfoExist = ciFalse;
	_downloadedSize = 0;
	_site = site;

	//
	ciStringTokenizer aTokens(contentsDownloaderWorld::getDownloadLimitTime(), ":-");
	if( aTokens.countTokens() == 0 )
	{
		ciDEBUG(5, ("empty DownloadLimitTime") );
		return;
	}
	if( aTokens.countTokens() != 4 )
	{
		ciWARN( ("INVALID DownloadLimitTime FORMAT !!! (%s)", contentsDownloaderWorld::getDownloadLimitTime()) );
		return;
	}

	int start_hour = atoi(aTokens.nextToken().c_str());
	int start_min = atoi(aTokens.nextToken().c_str());
	int end_hour = atoi(aTokens.nextToken().c_str());
	int end_min = atoi(aTokens.nextToken().c_str());

	char tmp[64]={0};
	sprintf(tmp, "%02d:%02d", start_hour, start_min);
	_downloadStartTime = tmp;
	sprintf(tmp, "%02d:%02d", end_hour, end_min);
	_downloadEndTime = tmp;
	ciDEBUG2(5, ("DownloadLimitTime=%s-%s", _downloadStartTime.c_str(), _downloadEndTime.c_str()));
}

contentsDownloadTimer::~contentsDownloadTimer()
{
	ciDEBUG2(5, ("~contentsDownloadTimer()"));
}

extern ciBoolean createFullDirectory(const char* path, ciBoolean isFilenameIncluded);

void contentsDownloadTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG2(5, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));
	_name = name;

	if( _status == DS_INIT || _status == DS_LOGINING )
	{
		_status = DS_LOGINING;

		if( !m_objFileSvcClient.Login(_id.c_str(), _pwd.c_str()) )
		{
			// login failed !!!
			ciWARN2( ("%s::LOGIN FAIL !!!(%s,%s)", _name.c_str(), _id.c_str(), _pwd.c_str()) );
			return;
		}

		_status = DS_READY;
		ciDEBUG2(5,  ("%s::login succeed", _name.c_str()) );
	}

	// complete(or error)  ==>  skip
	if( _status == DS_COMPLETED || _status == DS_ERROR )
	{
		ciDEBUG2(5, ("%s::do nothing(status:%d)", _name.c_str(), _status) );
		return;
	}

	// info isn't exist  ==>  skip
	{
		ciGuard aGuard(_lock);
		if( _isInfoExist == ciFalse )
		{
			if( counter % 60 == 0 )
			{
				ciDEBUG2(5, ("%s::no download-contents", _name.c_str()) );
			}
			return;
		}
	}

	// ready  ==>  start download
	ciDEBUG2(5, ("%s::start download (%s)", _name.c_str(), _info.toString(ciFalse).c_str()) );
	_status = DS_DOWNLOADING;
	_downloadedSize = 0;

	BOOL result = FALSE;
	ciBoolean download_now = ciTrue;

	if( !_info.timeFlag.empty() &&
		stricmp(_info.timeFlag.c_str(),"y") == 0 &&
		!_isNowDownloadTime() )
	{
		download_now = ciFalse;
		ciWARN(("%s::NOT DOWNLOAD-TIME !!! (%s)", _name.c_str(), _info.toString(ciFalse).c_str()));
	}

	if( download_now )
	{
		try
		{
			// check file(_info.tempPath) size  ==> same size ==> not download
			//									==> different size ==> download

			// get file-status
			struct _stati64 fs;
			if( _stati64(_info.tempPath.c_str(), &fs) == 0 )
			{
				// open success
				if( _info.volume == fs.st_size )
				{
					// same size => already exist => no download
					ciDEBUG2(5, ("%s::already exist, skip download (%s)", _name.c_str(), _info.tempPath.c_str()) );
					result = TRUE;
				}
				else
				{
					// different size => must download
					ciDEBUG2(5, ("%s::different size, download (%s)", _name.c_str(), _info.tempPath.c_str()) );
				}
			}
			else
			{
				// open error => not exist => must download
				ciDEBUG2(5, ("%s::not exist, download (%s)", _name.c_str(), _info.tempPath.c_str()) );
			}

			if( result == FALSE )
			{
				ciDEBUG2(5, ("%s::download (%s) (%s)", _name.c_str(), _info.locationOri.c_str(), _info.tempPath.c_str()) );
				if( ciArgParser::getInstance()->isSet("+http_download") )
				{
					ciDEBUG2(5, ("ServerIP=%s", contentsDownloaderWorld::getCMSServerIP()) );
					ciDEBUG2(5, ("ServerPort=%d", contentsDownloaderWorld::getCMSServerPort()) );

					// HTTP protocol download
					CHttpRequest http_request;
					http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, contentsDownloaderWorld::getCMSServerIP(), contentsDownloaderWorld::getCMSServerPort());
					http_request.SetBinaryDownloadMode(true, _info.tempPath.c_str(), &_downloadedSize);
					if( ciArgParser::getInstance()->isSet("+HMCSFS") )
					{
						// for HMCSFS (add additionalHeader)
						http_request.AddAdditionalHeader(_getAdditionHttpHeader().c_str());
					}
					createFullDirectory(_info.tempPath.c_str());

					//
					CString url;
					url.Format(_T("%s"), contentsDownloaderWorld::getContentsDownloadURL());
					//url.Format(_T("%s"), _info.locationOri.c_str());

					CString param;
					param.Format(_T("location=%s"), _info.locationOri.c_str());

					CString str_buf;
					if( !http_request.RequestPost(url, param, str_buf) )
					{
						ciWARN2( ("HTTP_DOWNLOAD FAIL !!! (URL:%s?%s, ErrCode:%s)", http_request.GetRequestURL(), param, http_request.GetErrorMsg()) );
						result = FALSE;
					}
					else
					{
						ciDEBUG(5, ("HTTP_DOWNLOAD success (URL:%s?%s)", http_request.GetRequestURL(), param) );
						result = TRUE;
					}
				}
				else
				{
					// UBC_HTTP download
					result = m_objFileSvcClient.GetFile(_info.location.c_str(), _info.tempPath.c_str(), 0, (IProgressHandler*)this);
				}
			}
		}
		catch (CString& e)
		{
			result = FALSE;
			ciWARN2( ("%s::EXCEPTION !!! (%s) (%s)", _name.c_str(), (LPCSTR)e, _info.toString(ciFalse).c_str()) );
		}
		catch(...)
		{
			result = FALSE;
			ciWARN2( ("%s::EXCEPTION !!! (%s)", _name.c_str(), _info.toString(ciFalse).c_str()) );
		}
	}

	if( result )
	{
		_info.existTempPath = ciTrue;

		if( _info.contentsType != OFS_BROCHURE_CONTENTS_TYPE )
		{
			_info.replaceFile();
		}
	}

	_status = ( result==TRUE ? DS_COMPLETED : DS_ERROR );
	ciDEBUG2(5, ("%s::new status (%d) (%s)", _name.c_str(), _status, m_objFileSvcClient.GetErrorMsg()) );

	return;
}

void contentsDownloadTimer::initServerInfo(const char* ip, ciLong port, const char* id, const char* pwd)
{
	ciDEBUG2(5, ("initServerInfo(%s,%d,%s,%s)", ip, port, id, pwd) );

	_ip		= ip;
	_port	= port;
	_id		= id;
	_pwd	= pwd;

	m_objFileSvcClient.SetHttpServerInfo(_ip.c_str(), _port);
}

void contentsDownloadTimer::ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath)
{
/*
	enum FS_PROGRESS_FLAG
	{
		FS_START = 0,
		FS_DOING,
		FS_STOP,
		FS_COMPLETE,
		FS_ERROR
	};
*/

	ciDEBUG2(5, ("ProgressEvent(%s)(%I64u)", _info.filename.c_str(), uSendBytes) );

	_downloadedSize = uSendBytes;
/*

	//FS_START = 0,
	//FS_DOING,
	//FS_STOP,
	//FS_COMPLETE,
	//FS_ERROR

	if(nFlag == CFileServiceWrap::FS_DOING && m_ulNewFileSize > 0)
	{
		int progress = (uSendBytes * 100) / m_ulNewFileSize;

		m_pc.SetPos(progress);

		CString str_progress;
		str_progress.Format("%d %%", progress);

		m_stcProgress.SetWindowText(str_progress);

		m_editNewFileSize.SetWindowText( ::ToMoneyTypeString(m_ulNewFileSize) );
	}

	TraceLog(("ProgressEvent::OnBytesReceived end"));
*/
}

void contentsDownloadTimer::setContentsInfo(contentsInfo* info)
{
	ciGuard aGuard(_lock);

	_status = DS_READY;

	if( info == NULL )
	{
		ciDEBUG2(5, ("%s::setContentsInfo(NULL)", _name.c_str()) );
		_isInfoExist = ciFalse;
		_info.init();
		return;
	}

	ciDEBUG2(5, ("%s::setContentsInfo(%s)", _name.c_str(), info->toString(ciFalse).c_str()) );
	_info = *info;
	_isInfoExist = ciTrue;
}

ciBoolean contentsDownloadTimer::_isNowDownloadTime()
{
	if( _downloadStartTime.empty() || _downloadEndTime.empty() )
		return ciTrue;

	ciTime now;
	char buf[32];
	sprintf(buf, "%02d:%02d", now.getHour(), now.getMinute());
	ciString str_now = buf;

	if( _downloadStartTime < _downloadEndTime )
	{
		// ex) 08:00-23:00
		if( _downloadStartTime <= str_now && str_now < _downloadEndTime )
			return ciTrue;
	}
	else
	{
		// ex) 23:00-08:00
		if( _downloadStartTime <= str_now || str_now < _downloadEndTime )
			return ciTrue;
	}

	return ciFalse;
}
