#include "common/libHttpRequest/HttpRequest.h"
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <util/libEncoding/Encoding.h>
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchUtil.h"
#include "topmostCheckTimer.h"


ciSET_DEBUG(9, "topmostCheckTimer");


topmostCheckTimer::topmostCheckTimer()
{
	ciDEBUG2(5, ("topmostCheckTimer()") );

	ubcConfig aIni("UBCVariables");
	ciString buf;
	aIni.get("OFS_CMS_INFO", "TopmostFilename", buf, "");
	//ciStringUtil::trim(buf);
	ciDEBUG2(5, ("TopmostFilename=%s", buf.c_str()) );

	ciStringTokenizer aTokens(buf.c_str(), ",");
	int cnt=aTokens.countTokens();
	for(int i=0; i<cnt; i++)
	{
		ciString filename = aTokens.nextToken();
		ciStringUtil::trim(filename);
		ciDEBUG2(5, ("add topmostFilename(%s)", filename.c_str()) );
		if( filename.length() == 0 ) continue;

		_checkingProcessList.push_back(filename.c_str());
	}
}

topmostCheckTimer::~topmostCheckTimer()
{
	ciDEBUG2(5, ("~topmostCheckTimer()"));
}

void topmostCheckTimer::processExpired(ciString name, int counter, int interval)
{
	//ciDEBUG2(5, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	ciStringList::iterator itr = _checkingProcessList.begin();
	ciStringList::iterator end = _checkingProcessList.end();
	scratchUtil* aUtil = scratchUtil::getInstance();
	for( ; itr!=end; itr++ )
	{
		ciString filename = (*itr);

		HWND hwnd = aUtil->getWHandle(filename.c_str());

		if( hwnd )
		{
			DWORD dwExStyle = ::GetWindowLong(hwnd, GWL_EXSTYLE);
			if ( !(dwExStyle & WS_EX_TOPMOST) )
			{
				::SetWindowPos(hwnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
				::ShowWindow(hwnd, SW_SHOW);
				::SetForegroundWindow(hwnd);
			}
		}
	}

	return;
}

void topmostCheckTimer::addProcess(const char* filename)
{
	_checkingProcessList.push_back(filename);
}
