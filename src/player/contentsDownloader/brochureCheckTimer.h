#ifndef _brochureCheckTimer_H_
#define _brochureCheckTimer_H_

//#include "afxwin.h"
//#include "afxcmn.h"

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>

class contentsCheckTimer;


class brochureCheckTimer : public ciWinPollable
{
public:
	brochureCheckTimer(const char* site, const char* host, contentsCheckTimer* timer);
	virtual	~brochureCheckTimer();

	static	ciString	flashPath;
	static	ciString	flashSubPath;
	static	ciString	flashFilename;

	virtual	void	processExpired(ciString name, int counter, int interval);

protected:
	ciString	_site;
	ciString	_host;
	contentsCheckTimer*		_contentsCheckTimer;

	ciString	_cmsServerIP;
	ciLong		_cmsServerPort;
	ciString	_cmsIdUrl;
	ciString	_cmsXmlUrl;
	ciString	_brochureXmlPath;

//	ciString	_clientIP;
//	ciLong		_clientPort;

	ciULong		_prevFlashPid;
	ciString	_prevBrochureId;
	ciTime		_prevBrochureTime;

	ciBoolean	_getCMSInfo();
//	ciBoolean	_getClientInfo();
//	ciString	_createPacket(const char* bId);
	ciBoolean	_getFlashEnv();

	ciBoolean	_getBrochureInfo(const char* xml, ciString& bId, ciTime& tm, ciString& err);
	ciBoolean	_checkBrochure(const char* bId, ciTime tm);
//	ciBoolean	_pushBrochureId(const char* bId, ciString& retval);

	ciBoolean	_getBrochureXML(const char* bId, ciString& xml);
	ciBoolean	_contentsCheck(const char* xml);
	ciBoolean	_writeXML(const char* xml);
	ciBoolean	_killFlashProcess();
	ciULong		_startFlashProcess();
	ciULong		_getFlashPID();

	ciString	_getXMLforKHQS(const char* xml);
	ciString	_getXMLforHMCSFS(const char* xml);
};

#endif //_brochureCheckTimer_H_
