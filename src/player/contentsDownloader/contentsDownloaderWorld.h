 /*! \file contentsDownloaderWorld.h
 *  Copyright ⓒ 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _contentsDownloaderWorld_h_
#define _contentsDownloaderWorld_h_

#include <afxwin.h>
#include <afxcmn.h>

#include <cci/libWorld/cciORBWorld.h> 
#include "ofsSocketSession.h"
#include "common/libCommon/ubcHost.h"

class ubcHostView;
//class brochureCheckTimer;
class contentsCheckTimer;
class contentsManagerTimer;
//class pptCheckTimer;
class topmostCheckTimer;

class contentsDownloaderWorld : public cciORBWorld
{
public:
	contentsDownloaderWorld();
	~contentsDownloaderWorld();

	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

	static const char* getCMSServerIP() { return _cmsServerIP.c_str(); };
	static ciLong getCMSServerPort() { return _cmsServerPort; };
	static const char* getContentsDownloadListURL() { return _contentsDownloadListURL.c_str(); };
	static const char* getBrochureIdURL() { return _brochureIdURL.c_str(); };
	static const char* getBrochureXmlURL() { return _brochureXmlURL.c_str(); };
	static const char* getDownloadLimitTime() { return _downloadLimitTime.c_str(); };
	static const char* getContentsDownloadURL() { return _contentsDownloadURL.c_str(); };

protected:
	static ciString _cmsServerIP;
	static ciLong _cmsServerPort;
	static ciString _contentsDownloadListURL;
	static ciString _brochureIdURL;
	static ciString _brochureXmlURL;
	static ciString _downloadLimitTime;
	static ciString _contentsDownloadURL;

	ofsSocketSession*			_session;

//	brochureCheckTimer*			_brochureCheckTimer;
	contentsCheckTimer*			_contentsCheckTimer;
	contentsManagerTimer*		_contentsManagerTimer;
	topmostCheckTimer*			_topmostCheckTimer;

	void		_initCMSInfo();

	void		_releaseMutexLock();

	ciBoolean	_getSiteId(const char* host, ciString& brand, ciString& site, ubcHostView* pView);

	int			_cleanupDirectory(const char* szDir, int recur); // recur=0 : szDir하위만 삭제(szDir자체는 보존)
																// recur=1 : szDir자체까지 포함하여 하위전체삭제
};

ciString _getAdditionHttpHeader();

#endif //_contentsDownloaderWorld_h_
