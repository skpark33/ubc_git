#ifndef _topmostCheckTimer_H_
#define _topmostCheckTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>


class topmostCheckTimer : public ciWinPollable
{
public:
	topmostCheckTimer();
	virtual	~topmostCheckTimer();

	virtual	void	processExpired(ciString name, int counter, int interval);

	void	addProcess(const char* filename);

protected:
	ciStringList _checkingProcessList;
};

#endif //_topmostCheckTimer_H_
