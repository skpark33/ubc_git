/*! \file contentsDownloaderWorld.C
 *  Copyright ⓒ 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
*/

#include "contentsDownloaderWorld.h"

#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libConfig/ciEnv.h>

#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libPfUtil/cciDSUtil.h>

#include "common/libScratch/scratchUtil.h"
#include "common/libScratch/OrbConn.h"
#include "common/libInstall/installUtil.h"
//#include "common/libCommon/ubcIniMux.h"
#include "common/libCommon/ubcMuxRegacy.h"
#include "common/libCommon/ubcIni.h"
#include "common/libCommon/ubcHostView.h"
#include "common/libCommon/ubcCenter.h"

#include "common/libCommon/ubcEnterprise.h"
//#include "libTimeSyncer/TimeSyncTimer.h"
//#include "libAudioControl/UBCAudioControl.h"

//#include "libCustomerCopyURL/CustomerCopyURL.h"
//#include "brochureCheckTimer.h"
#include "contentsCheckTimer.h"
#include "contentsManagerTimer.h"
#include "topmostCheckTimer.h"

//#include "SVR/libPlan/planTimer.h"


#define		DEFAULT_CMS_SERVER_IP				"127.0.0.1"
#define		DEFAULT_CMS_SERVER_PORT				(8000)

#define		DEFAULT_CONTENTS_DOWNLOAD_LIST_URL	"pmis/api/contentsFileList/Api034.do"
#define		DEFAULT_BROCHURE_ID_URL				"pmis/api/contentsLastUpdateTime/Api034.do"
#define		DEFAULT_BROCHURE_XML_URL			"pmis/api/contentsList/Api034.do"
#define		DEFAULT_CONTENTS_DOWNLOAD_URL		"pmis/api/contentsFileDownload/Api034.do"


ciString contentsDownloaderWorld::_cmsServerIP = "";
ciLong contentsDownloaderWorld::_cmsServerPort = 0;
ciString contentsDownloaderWorld::_contentsDownloadListURL = "";
ciString contentsDownloaderWorld::_brochureIdURL = "";
ciString contentsDownloaderWorld::_brochureXmlURL = "";
ciString contentsDownloaderWorld::_downloadLimitTime = "";
ciString contentsDownloaderWorld::_contentsDownloadURL = "";


void p_gracefullExit(void)
{
	 printf("R0625 - pure virtual function call error 를 안나게 한다.");
	 cciORBWorld* world = (cciORBWorld*)cciORBWorld::getWorld();
	 if(world){
		 world->fini(0);
	 }
}
LONG WINAPI p_NoMsgExceptionFilter(struct _EXCEPTION_POINTERS* /*ExceptionInfo*/)
{
    // 아무일도 하지 않고 그냥 종료하기
	 printf("error 창을 를 안나게 한다.\n");
	 cciORBWorld* world = (cciORBWorld*)cciORBWorld::getWorld();
	 if(world){
		 world->fini(0);
	 }

	return EXCEPTION_EXECUTE_HANDLER;
}

//ciLong	contentsDownloaderWorld::_eventId = 0;

ciSET_DEBUG(10, "contentsDownloaderWorld");

contentsDownloaderWorld::contentsDownloaderWorld()
{
	ciDEBUG2(5, ("contentsDownloaderWorld()"));

	_session = 0;
	//_brochureCheckTimer = 0;
	_contentsCheckTimer = 0;
	_contentsManagerTimer = 0;
	_topmostCheckTimer = 0;
}

contentsDownloaderWorld::~contentsDownloaderWorld()
{
	ciDEBUG2(5, ("~contentsDownloaderWorld()"));

	//if( _brochureCheckTimer )
	//{
	//	ciDEBUG2(1,("delete _brochureCheckTimer"));
	//	_brochureCheckTimer->stopTimer();
	//	delete _brochureCheckTimer;
	//}

	if( _contentsCheckTimer )
	{
		ciDEBUG2(1,("delete _contentsCheckTimer"));
		_contentsCheckTimer->stopTimer();
		delete _contentsCheckTimer;
	}

	if( _contentsManagerTimer )
	{
		ciDEBUG2(1,("delete _contentsManagerTimer"));
		_contentsManagerTimer->stopTimer();
		delete _contentsManagerTimer;
	}

	if( _topmostCheckTimer )
	{
		ciDEBUG2(1,("delete _topmostCheckTimer"));
		_topmostCheckTimer->stopTimer();
		delete _topmostCheckTimer;
	}
}

ciBoolean 
contentsDownloaderWorld::init(int& p_argc, char** &p_argv) 
{
	ciDEBUG2(5, ("init(%d)", p_argc));	

	cciORBWorld::_mirId = "contentsDownloader";
	::SetUnhandledExceptionFilter(p_NoMsgExceptionFilter);
	_set_purecall_handler(p_gracefullExit);

	// Arg 는 한번 init 하면 다시 또 init 하지는 않는다.
	ciArgParser::initialize(p_argc,p_argv);

	// +bg 옵션과 +w 옵션은 양립할 수 없다.
	if(ciArgParser::getInstance()->isSet("+bg")){
		ciArgParser::getInstance()->removeArg("+w");
	}

	if(cciORBWorld::init(p_argc,p_argv) == ciFalse)
	{
		ciERROR2(("cciORBWorld::init() is FAILED"));
		return ciFalse;
	}
	/*
	ciDebug::setDebugOn();
	ciWorld::ylogOn();
	*/
	ciEnv::defaultEnv(true, "utv1");

#ifdef _COP_MSC_
	if(ciIniUtil::isHttpOnly()){
		ciDEBUG2(1,("CoInitializeEx() : COM INITIALIZE"));
		CoInitializeEx(NULL, COINIT_MULTITHREADED);
	}
#endif

	// contents check timer
	ciString buf = "";
	if( ciArgParser::getInstance()->getArgValue("+path", buf) )
		contentsCheckTimer::flashPath = buf;
	if( ciArgParser::getInstance()->getArgValue("+exe", buf) )
	{
		contentsCheckTimer::flashSubPath = buf;
		if( buf.length() > 4 && stricmp(buf.c_str()+buf.length()-4,".exe") != 0 )
			buf += ".exe";
		contentsCheckTimer::flashFilename = buf;
	}
	if( ciArgParser::getInstance()->getArgValue("+argument", buf) )
		contentsCheckTimer::flashArgument = buf;

	//
	_initCMSInfo();

	//
	ciULong flash_pid = contentsCheckTimer::_getFlashPID();
	ciDEBUG2(5, ("checkFlashPID=%u", flash_pid) );
	if( flash_pid == 0 )
	{
		ciDEBUG2(5, ("no flash process ==> start flash process") );
		contentsCheckTimer::_startFlashProcess();
	}

	//
    ciDEBUG2(5, ("init(%d) end", p_argc));
	for(int i=0; i<p_argc; i++)
	{
		ciDEBUG2(5, ("line%d:%s", i, p_argv[i]));
	}

	// get siteId, hostId, edtion
	ciString brand="", site="", host="", mac="", edition="";

	if( ciArgParser::getInstance()->getArgValue("+site", buf) ) site = buf;
	if( ciArgParser::getInstance()->getArgValue("+host", buf) ) host = buf;

	if( site.empty() || host.empty() )
	{
		scratchUtil::getInstance()->readAuthFile(host, mac, edition);

		//ubcHostInfo* aHostInfo = 0;
		ubcHostView aView("*", host.c_str());
		while( /*brand.empty() ||*/ site.empty() )
		{
			ciWARN2(("site is empty"));
			::Sleep(1000);

			// get siteId
			ciBoolean retval = _getSiteId(host.c_str(), brand, site, &aView);
			if( !retval )
			{
				ciERROR2(("getSiteId communication failed"));
			}
		}
	}

	//
	ubcMuxData* aData = NULL;
	while( true )
	{
		::Sleep(1000);
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(site.c_str());
		if( aData != NULL ) break;
		ciWARN2(("site(%s) is empty", site.c_str()));
	}

	ciString str_addr, str_ftpid, str_ftppwd;
	ciLong ftp_port;

	str_addr = aData->getFTPAddress();
	ftp_port = aData->ftpPort;
	str_ftpid = aData->ftpId.c_str();
	str_ftppwd = aData->ftpPasswd.c_str();

	//
	_contentsCheckTimer = new contentsCheckTimer(site.c_str(), host.c_str(), str_addr.c_str(), ftp_port, str_ftpid.c_str(), str_ftppwd.c_str());
	_contentsCheckTimer->initTimer("contents check timer", 10); // 10-sec until init
	_contentsCheckTimer->startTimer();

	// clean-up tmp-folder
	//_cleanupDirectory("C:\\SQISoft\\Contents\\Temp", 0);

	// topmost check timer
	if( ciArgParser::getInstance()->getArgValue("+KHQS", buf) )
	{
		_topmostCheckTimer = new topmostCheckTimer();
		_topmostCheckTimer->addProcess("pptview.exe");
		_topmostCheckTimer->initTimer("topmost check timer", 1); // every 1-sec
		_topmostCheckTimer->startTimer();
	}
	//else if( ciArgParser::getInstance()->getArgValue("+HMCSFS", buf) )
	//{
	//	_topmostCheckTimer = new topmostCheckTimer();
	//	_topmostCheckTimer->addProcess("pptview.exe");
	//	_topmostCheckTimer->addProcess("powerpnt.exe");
	//	_topmostCheckTimer->addProcess("excel.exe");
	//	_topmostCheckTimer->addProcess("iexplore.exe");
	//	_topmostCheckTimer->addProcess("XPlatform.exe");
	//	_topmostCheckTimer->addProcess("SmartBoard.exe");
	//	_topmostCheckTimer->initTimer("topmost check timer", 1); // every 1-sec
	//	_topmostCheckTimer->startTimer();
	//}

	return ciTrue;
}

ciBoolean contentsDownloaderWorld::fini(long p_finiCode)
{
	if( _session )
	{
		_session->destroy();
		_session = 0;
	}

	_releaseMutexLock();

	return cciORBWorld::fini(p_finiCode); 
}

void contentsDownloaderWorld::_initCMSInfo()
{
	ubcConfig aIni("UBCVariables");

	// get cms-server-ip
	aIni.get("OFS_CMS_INFO", "ServerIP", _cmsServerIP, "");
	ciStringUtil::trim(_cmsServerIP);
	ciDEBUG2(5, ("ServerIP=%s", _cmsServerIP.c_str()) );
	if( _cmsServerIP.length() == 0 )
	{
		_cmsServerIP = DEFAULT_CMS_SERVER_IP;
		aIni.set("OFS_CMS_INFO", "ServerIP", _cmsServerIP.c_str());
		ciDEBUG2(5, ("set to default_cms_server_ip(%s)", _cmsServerIP.c_str()) );
	}

	// get cms-server-port
	ciString buf = "";
	aIni.get("OFS_CMS_INFO", "ServerPort", buf, "");
	ciStringUtil::trim(buf);
	ciDEBUG2(5, ("ServerPort=%s", buf.c_str()) );
	_cmsServerPort = atoi(buf.c_str());
	if( _cmsServerPort == 0 )
	{
		_cmsServerPort = DEFAULT_CMS_SERVER_PORT;
		aIni.set("OFS_CMS_INFO", "ServerPort", _cmsServerPort);
		ciDEBUG2(5, ("set to default_cms_server_port(%d)", _cmsServerPort) );
	}

	// get cms-contents-download-list-url
	aIni.get("OFS_CMS_INFO", "ContentsUrl", _contentsDownloadListURL, "");
	ciStringUtil::trim(_contentsDownloadListURL);
	ciDEBUG2(5, ("ContentsUrl=%s", _contentsDownloadListURL.c_str()) );
	if( _contentsDownloadListURL.length() == 0 )
	{
		_contentsDownloadListURL = DEFAULT_CONTENTS_DOWNLOAD_LIST_URL;
		aIni.set("OFS_CMS_INFO", "ContentsUrl", _contentsDownloadListURL.c_str());
		ciDEBUG2(5, ("set to default_contents_download_list_url(%s)", _contentsDownloadListURL.c_str()) );
	}

	// get cms-brochure-id-url
	aIni.get("OFS_CMS_INFO", "BrochureIdUrl", _brochureIdURL, "");
	ciStringUtil::trim(_brochureIdURL);
	ciDEBUG2(5, ("BrochureIdUrl=%s", _brochureIdURL.c_str()) );
	if( _brochureIdURL.length() == 0 )
	{
		_brochureIdURL = DEFAULT_BROCHURE_ID_URL;
		aIni.set("OFS_CMS_INFO", "BrochureIdUrl", _brochureIdURL.c_str());
		ciDEBUG2(5, ("set to default_brochure_id_url(%s)", _brochureIdURL.c_str()) );
	}

	// get cms-brochure-xml-url
	aIni.get("OFS_CMS_INFO", "BrochureXmlUrl", _brochureXmlURL, "");
	ciStringUtil::trim(_brochureXmlURL);
	ciDEBUG2(5, ("BrochureXmlUrl=%s", _brochureXmlURL.c_str()) );
	if( _brochureXmlURL.length() == 0 )
	{
		_brochureXmlURL = DEFAULT_BROCHURE_XML_URL;
		aIni.set("OFS_CMS_INFO", "BrochureXmlUrl", _brochureXmlURL.c_str());
		ciDEBUG2(5, ("set to default_brochure_xml_url(%s)", _brochureXmlURL.c_str()) );
	}

	// get cms-contents-download-url
	aIni.get("OFS_CMS_INFO", "ContentsDownloadUrl", _contentsDownloadURL, "");
	ciStringUtil::trim(_contentsDownloadURL);
	ciDEBUG2(5, ("ContentsDownloadUrl=%s", _contentsDownloadURL.c_str()) );
	if( _contentsDownloadURL.length() == 0 )
	{
		_contentsDownloadURL = DEFAULT_CONTENTS_DOWNLOAD_URL;
		aIni.set("OFS_CMS_INFO", "ContentsDownloadUrl", _contentsDownloadURL.c_str());
		ciDEBUG2(5, ("set to default_contents_download_url(%s)", _contentsDownloadURL.c_str()) );
	}

	// get download-limit-time
	aIni.get("OFS_CMS_INFO", "DownloadLimitTime", _downloadLimitTime, "");
}

void contentsDownloaderWorld::_releaseMutexLock()
{
	HANDLE hMutex = ::OpenMutex(MUTEX_ALL_ACCESS, FALSE, "contentsDownloader");
	if(hMutex)
	{
		::ReleaseMutex(hMutex);
		::CloseHandle(hMutex);
	}
}

ciBoolean contentsDownloaderWorld::_getSiteId(const char* host, ciString& brand, ciString& site, ubcHostView* pView)
{
//	ciString siteName,siteCode, siteType,chainNo, phoneNo1/*, mobileNo*/,hostName;
//	ciLong hostType=99;

	ciBoolean retval = pView->get();

	brand = pView->brandId;
	site  = pView->siteId;
	//siteName = pView->siteName;
	//siteCode = pView->businessCode;
	//siteType = pView->businessType;
	//chainNo = pView->chainNo;
	//phoneNo1 = pView->phoneNo1;
	////mobileNo = pView->mobileNo;
	//hostType = pView->hostType;
	//hostName = pView->hostName;

	if( retval == ciFalse /*|| brand.empty()*/ || site.empty() )
	{
		ciERROR2(("getSiteId failed"));
	}

	return retval;
}

int contentsDownloaderWorld::_cleanupDirectory(const char* szDir, int recur)
{
	//
	char findpath[MAX_PATH] = {0};
	char subpath[MAX_PATH] = {0};

	strcpy(findpath, szDir);
	if( strcmp(findpath+strlen(findpath)-4,_T("\\*.*")) != NULL )
		strcat(findpath, _T("\\*.*")); // add wildcard to tail

	//
	WIN32_FIND_DATA fd;
	HANDLE handle = FindFirstFile(findpath, &fd);
	if( handle == INVALID_HANDLE_VALUE )
	{
		// invalid handle
		if( recur > 0 ) RemoveDirectory(szDir);
		return -1;
	}

	//
	int ret = 1;
	while( ret )
	{
		sprintf(subpath, "%s\\%s", szDir, fd.cFileName);

		if( fd.dwFileAttributes & FILE_ATTRIBUTE_READONLY )
		{
			// read only => change to normal
			SetFileAttributes(subpath, FILE_ATTRIBUTE_NORMAL);
		}

		if( fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			// directory
			if(strcmp(fd.cFileName, _T(".")) && strcmp(fd.cFileName, _T("..")))
			{
				// not dot(.) dir => sub-path recursive
				_cleanupDirectory(subpath, recur+1);
			}
		}
		else
		{
			// file => delete
			::SetFileAttributes(subpath, FILE_ATTRIBUTE_NORMAL); //읽기전용 해제
			::DeleteFile(subpath);
		}

		ret = FindNextFile(handle, &fd);
	}
	FindClose(handle);

	if( recur > 0 ) ::RemoveDirectory(szDir);

	return 0;
}


#include "cryptopp/cryptlib.h"
#include "cryptopp/modes.h"
#include "cryptopp/aes.h"
#include "cryptopp/filters.h"
#include "cryptopp/base64.h"

typedef unsigned int uint;

void hex2byte(const char *in, uint len, byte *out)
{
	for (uint i = 0; i < len; i+=2) {
		char c0 = in[i+0];
		char c1 = in[i+1];
		byte c = (
			((c0 & 0x40 ? (c0 & 0x20 ? c0-0x57 : c0-0x37) : c0-0x30)<<4) |
			((c1 & 0x40 ? (c1 & 0x20 ? c1-0x57 : c1-0x37) : c1-0x30))
			);
		out[i/2] = c;
	}
}

ciString _encrypt_aes128(ciString str)
{
	static ciString enc_key = "62623531383630313761373432313366"; //bb5186017a74213f 헥사스트링
	static ciString enc_iv = "00000000000000000000000000000000";
	static bool init = false;

	//키할당
	static byte key[CryptoPP::AES::MIN_KEYLENGTH];
	if( init == false )
	{
		memset(key, 0x00, CryptoPP::AES::MIN_KEYLENGTH);
		const char* rawKey = enc_key.c_str();
		hex2byte(rawKey, strlen(rawKey), key);
	}

	//iv할당
	static byte iv[CryptoPP::AES::BLOCKSIZE];
	if( init == false )
	{
		memset(iv, 0x00, CryptoPP::AES::BLOCKSIZE);
		const char* rawIv = enc_iv.c_str();
		hex2byte(rawIv, strlen(rawIv), iv);
	}
	init = true;

	ciString cipher, encoded;
	try
	{
		// CBC
		//CryptoPP::CBC_Mode<CryptoPP::AES>::Encryption e;
		//e.SetKeyWithIV(key, sizeof(key), iv, sizeof(iv));
		// ECB
		CryptoPP::ECB_Mode<CryptoPP::AES>::Encryption e;
		e.SetKey(key, sizeof(key));

		CryptoPP::StringSource ss(str, true, new CryptoPP::StreamTransformationFilter(e, new CryptoPP::StringSink(cipher), CryptoPP::BlockPaddingSchemeDef::PKCS_PADDING));

		// base64 output
		//CryptoPP::StringSource(cipher, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(encoded), false)); //base64encoder 두번째 인자에 false를 줘야 마지막애 개행문자가 추가안됨

		// hexastring output
		for(int i=0; i<cipher.length(); i++)
		{
			char buf[16] = {0};
			sprintf(buf, "%02x", (BYTE)cipher[i]);
			encoded += buf;
		}

		return encoded;
	}
	catch(const CryptoPP::Exception& e)
	{
		std::cerr << e.what() << std::endl;
		//return NULL;
	}

	return "";
}

ciString _getAdditionHttpHeader()
{
/*
	deviceType: UBC
	deviceId: MACAddress
	timeStamp: 접속시간unixtime_____클라이언트ip
*/
	static ciString tag_device_type = _encrypt_aes128("deviceType");
	static ciString tag_device_id   = _encrypt_aes128("deviceId");
	static ciString tag_time_stamp  = _encrypt_aes128("timeStamp");

	ciString devicetype = "UBC";

	static ciString mac = "";
	if( mac.length() == 0 ) mac = scratchUtil::getInstance()->GetMacaddr();

	static ciString ip;
	if( ip.length() == 0 ) scratchUtil::getInstance()->getIpAddress(ip);
	ciTime now;
	char timestamp[64];
	sprintf(timestamp, "%I64d_____%s", now.getTime(), ip.c_str());

	char buf[1024] = {0};
	sprintf(buf, 
		"%s: %s\r\n"
		"%s: %s\r\n"
		"%s: %s\r\n"
			, tag_device_type.c_str(), _encrypt_aes128(devicetype).c_str()
			, tag_device_id.c_str(),   _encrypt_aes128(mac).c_str()
			, tag_time_stamp.c_str(),  _encrypt_aes128(timestamp).c_str()
	);

	ciDEBUG2(5, ("additional header(org) = %s, %s, %s", devicetype.c_str(), mac.c_str(), timestamp ));
	ciDEBUG2(5, ("additional header(enc) = %s", buf));

	return buf;
}
