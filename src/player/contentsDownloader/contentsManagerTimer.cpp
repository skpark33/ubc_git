#include "common/libHttpRequest/HttpRequest.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciTime.h>
#include "contentsCheckTimer.h"
#include "contentsManagerTimer.h"
#include "contentsDownloadTimer.h"


ciSET_DEBUG(9, "contentsManagerTimer");


contentsManagerTimer::contentsManagerTimer(const char* site) 
{
	ciDEBUG2(5, ("contentsManagerTimer(%s)", site));

	_site = site;
	_contentsDownloadTimer = NULL;
}

contentsManagerTimer::~contentsManagerTimer()
{
	ciDEBUG2(5, ("~contentsManagerTimer()"));
}

void contentsManagerTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG2(5, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));
	_name = name;

	if( _deleteingList.size() > 0 )
	{
		_DeleteFileInList();
		return;
	}

	// _contentsDownloadTimer is null  ==>  create new one & return
	if( _contentsDownloadTimer == NULL )
	{
		ciString timer_name = name;
		ciStringUtil::stringReplace(timer_name, "Mng", "Dwn");

		ciDEBUG2(5, ("%s::create contentsDownloadTimer(%s)", name.c_str(), timer_name.c_str()));

		_contentsDownloadTimer = new contentsDownloadTimer(_site.c_str());
		_contentsDownloadTimer->initTimer(timer_name.c_str(), 1); // every 1-sec
		_contentsDownloadTimer->initServerInfo(_ip.c_str(), _port, _id.c_str(), _pwd.c_str());
		_contentsDownloadTimer->startTimer();
		return;
	}

	contentsDownloadTimer::DOWNLOAD_STATUS status = _contentsDownloadTimer->getStatus();
	// _contentsDownloadTimer is DS_INIT  ==>  do nothing & return
	if( contentsDownloadTimer::DS_INIT == status )
	{
		ciDEBUG2(5, ("%s::do nothing (status:%d)", name.c_str(), status));
		return;
	}

	// _contentsDownloadTimer is DS_LOGINING  ==>  do nothing & return
	if( contentsDownloadTimer::DS_LOGINING == status )
	{
		ciDEBUG2(5, ("%s::do nothing (status:%d)", name.c_str(), status));
		return;
	}

	// _contentsDownloadTimer is DS_READY
	//		_downloadList is null  ==>  do nothing & return
	//		_downloadList is exist  ==>  get header from _downloadList  ==>  push to _contentsDownloadTimer
	if( contentsDownloadTimer::DS_READY == status )
	{
		ciGuard aGuard(_lock);

		// get header from _downloadList 
		contentsInfo* info = _getHeader();

		// _downloadList is null  ==>  do nothing & return
		if( info == NULL )
		{
			if( counter % 60 == 0 )
			{
				ciDEBUG2(5, ("%s::nothing to download", name.c_str()) );
			}
			return;
		}
		ciDEBUG2(5, ("%s::setContentsInfo(%s)", name.c_str(), info->toString(ciFalse).c_str()) );

		// _downloadList is exist  ==>  push to _contentsDownloadTimer
		_contentsDownloadTimer->setContentsInfo(info);

		return;
	}

	// _contentsDownloadTimer is DS_DOWNLOADING
	//		getDownloadedSize is equal over 1-min  ==>  delete _contentsDownloadTimer & re-queue header
	if( contentsDownloadTimer::DS_DOWNLOADING == status )
	{
		static ULONGLONG prev_downloaded_size = 0;
		static ciString prev_download_path = "";
		static int holding_count = 0;

		ciGuard aGuard(_lock);

		if( prev_download_path != _contentsDownloadTimer->getLocalPath() ||
			prev_downloaded_size != _contentsDownloadTimer->getDownloadedSize() )
		{
			holding_count = 0;
			ciDEBUG2(5, ("%s::reset holding-count (%s)", name.c_str(), prev_download_path.c_str()) );
		}
		else
		{
			// equal size & path
			holding_count++;
			if( holding_count <= 60 )
			{
				ciDEBUG2(5, ("%s::download holding (%d,%I64u,%s)", name.c_str(), holding_count, prev_downloaded_size, prev_download_path.c_str()) );
				return;
			}

			ciWARN2( ("%s::OVER HOLDING-COUNT (%s)", name.c_str(), prev_download_path.c_str()) );

			// equal status over 1-min
			delete _contentsDownloadTimer;
			_contentsDownloadTimer = NULL;

			// re-queue header
			//ciDEBUG2(5, ("%s::_requeueHeader(%s)", name.c_str(), prev_download_path.c_str()) );
			//_requeueHeader();
			ciDEBUG2(5, ("%s::_popHeader(%s)", name.c_str(), prev_download_path.c_str()) );
			_popHeader();

			// set null-info
			ciDEBUG2(5, ("%s::set null-contents", name.c_str()) );
			_contentsDownloadTimer->setContentsInfo(NULL);
		}

		return;
	}

	// _contentsDownloadTimer is DS_COMPLETED
	//		pop header from _downloadList & set null-info to _contentsDownloadTimer
	if( contentsDownloadTimer::DS_COMPLETED == status )
	{
		ciGuard aGuard(_lock);

		contentsInfo* info = _getHeader();
		if( info == NULL )
		{
			// DS_COMPLETED, but nothing-info ==> INVALID
			ciERROR2( ("%s::INVALID PROCESS STATUS !!!", name.c_str()) );
			return;
		}

		// pop header
		ciDEBUG2(5, ("%s::complete, pop-header(%s)", name.c_str(), info->toString(ciFalse).c_str()) );
		_popHeader();

		// set null-info
		ciDEBUG2(5, ("%s::set null-contents", name.c_str()) );
		_contentsDownloadTimer->setContentsInfo(NULL);

		return;
	}

	// _contentsDownloadTimer is DS_ERROR
	//		re-queue header & set null-info to _contentsDownloadTimer
	if( contentsDownloadTimer::DS_ERROR == status )
	{
		ciGuard aGuard(_lock);

		contentsInfo* info = _getHeader();
		if( info == NULL )
		{
			// DS_ERROR, but nothing-info ==> INVALID
			ciERROR2( ("%s::INVALID PROCESS STATUS !!!", name.c_str()) );
			return;
		}

		// re-queue header
		//ciWARN2( ("%s::DOWNLOAD-ERROR, requeue-header(%s)", name.c_str(), info->toString(ciFalse).c_str()) );
		//_requeueHeader();
		ciWARN2( ("%s::DOWNLOAD-ERROR, pop-header(%s)", name.c_str(), info->toString(ciFalse).c_str()) );
		_popHeader();

		// set null-info
		ciDEBUG2(5, ("%s::set null-contents", name.c_str()) );
		_contentsDownloadTimer->setContentsInfo(NULL);

		return;
	}


	//
	// invalid status
	//
	ciWARN2( ("%s::INVALID STATUS !!!", name.c_str()) );

	return;
}

void contentsManagerTimer::initServerInfo(const char* ip, ciLong port, const char* id, const char* pwd)
{
	ciDEBUG2(5, ("initServerInfo(%s,%d,%s,%s)", ip, port, id, pwd) );

	_ip		= ip;
	_port	= port;
	_id		= id;
	_pwd	= pwd;
}

contentsInfo* contentsManagerTimer::_getHeader()
{
	contentsInfoListIter itr = _downloadList.begin();
	contentsInfoListIter end = _downloadList.end();
	if( itr == end ) return NULL;

	contentsInfo* tmp = (*itr);
	ciDEBUG2(5, ("%s::_getHeader(%s)", _name.c_str(), tmp->toString(ciFalse).c_str()) );
	return tmp;
}

ciBoolean contentsManagerTimer::_popHeader()
{
	contentsInfoListIter itr = _downloadList.begin();
	contentsInfoListIter end = _downloadList.end();
	if( itr == end ) return ciFalse;

	contentsInfo* tmp = (*itr);

	_downloadList.pop_front();

	ciDEBUG2(5, ("%s::addToCompleteList (%s)", _name.c_str(), tmp->toString(ciFalse).c_str()) );
	_completeList.push_front( tmp );

	return ciTrue;
}

ciBoolean contentsManagerTimer::_requeueHeader()
{
	contentsInfoListIter itr = _downloadList.begin();
	contentsInfoListIter end = _downloadList.end();
	if( itr == end ) return ciFalse;
	contentsInfo* tmp = (*itr);

	ciDEBUG2(5, ("%s::_requeueHeader (%s)", _name.c_str(), tmp->toString(ciFalse).c_str()) );
	_downloadList.pop_front();
	_downloadList.push_back( tmp );

	return ciTrue;
}


ciBoolean contentsManagerTimer::pushbackToDownloadList(contentsInfo* info)
{
	if( info == NULL ) return ciFalse;

	ciGuard aGuard(_lock);

	// check already-download
	contentsInfoListIter itr = _completeList.begin();
	contentsInfoListIter end = _completeList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->isEqual(info) )
		{
			// already exist in complete-list => pass
			ciDEBUG2(5, ("%s::already exist in complete-list (%s)", _name.c_str(), info->toString(ciFalse).c_str()) );
			return ciFalse;
		}
	}

	// check duplicate
	itr = _downloadList.begin();
	end = _downloadList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->isEqual(info) )
		{
			// exist duplicate => pass
			ciDEBUG2(5, ("%s::already exist in downloading-list (%s)", _name.c_str(), info->toString(ciFalse).c_str()) );
			return ciFalse;
		}
	}

	// add clone to download-list
	ciDEBUG2(5, ("%s::pushbackToDownloadList (%s)", _name.c_str(), info->toString(ciFalse).c_str()) );
	_downloadList.push_front( info->duplicate() );

	return ciTrue;
}

ciBoolean contentsManagerTimer::pushbackToDeleteList(contentsInfo* info)
{
	if( info == NULL ) return ciFalse;

	ciGuard aGuard(_lock);

	// check already-delete
	contentsInfoListIter itr = _completeList.begin();
	contentsInfoListIter end = _completeList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->isEqual(info) )
		{
			// already exist in complete-list => pass
			ciDEBUG2(5, ("already exist in complete-list (%s)", info->toString(ciFalse).c_str()) );
			return ciFalse;
		}
	}

	// check duplicate
	itr = _deleteingList.begin();
	end = _deleteingList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->isEqual(info) )
		{
			// exist duplicate => pass
			ciDEBUG2(5, ("already exist in deleting-list (%s)", info->toString(ciFalse).c_str()) );
			return ciFalse;
		}
	}

	// add clone to download-list
	ciDEBUG2(5, ("pushbackToDeleteList (%s)", info->toString(ciFalse).c_str()) );
	_deleteingList.push_front( info->duplicate() );

	return ciTrue;
}

ciBoolean contentsManagerTimer::clearCompleteInfoList()
{
	ciDEBUG2(5, ("%s::clearCompleteInfoList()", _name.c_str()) );
	ciGuard aGuard(_lock);

	// check duplicate
	contentsInfoListIter itr = _completeList.begin();
	contentsInfoListIter end = _completeList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		delete tmp;
	}

	_completeList.clear();

	return ciTrue;
}

ciLong contentsManagerTimer::getRemainingCount()
{
	ciGuard aGuard(_lock);

	//
	int count = _deleteingList.size();
	if( count > 0 ) count--; // remove dummy

	//
	count += _downloadList.size();

	return count;
}

ciBoolean contentsManagerTimer::isDownloadingContents(const char* location)
{
	if( location == NULL ) return ciFalse;

	//
	// only use in brochure-contents-manager
	//

	ciGuard aGuard(_lock);

	ciString str_location = "/";
	str_location += location;
	ciStringUtil::stringReplace(str_location, "//", "/");

	// check downloading-list
	contentsInfoListIter itr = _downloadList.begin();
	contentsInfoListIter end = _downloadList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( stricmp(tmp->location.c_str(), str_location.c_str()) == 0 )
		{
			// exist in downloading-list
			return ciTrue;
		}
	}

	return ciFalse;
}

ciBoolean contentsManagerTimer::isDeletingContents(const char* filename, ULONGLONG filesize)
{
	if( filename == NULL ) return ciFalse;

	ciString str_filename_lower = filename;
	ciStringUtil::toLower(str_filename_lower);

	//
	// only use in delete-contents-manager
	//

	ciGuard aGuard(_lock);

	// check downloading-list
	contentsInfoListIter itr = _deleteingList.begin();
	contentsInfoListIter end = _deleteingList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);

		ciString str_location_lower = tmp->location;
		ciStringUtil::toLower(str_location_lower);

		if( str_location_lower.length() == 0 &&
			strstr(str_location_lower.c_str(), str_filename_lower.c_str()) == NULL )
		{
			// different => continue
			continue;
		}

		if( tmp->volume != filesize )
		{
			// different => continue
			continue;
		}

		// find same-file
		return ciTrue;
	}

	// not find same-file
	return ciFalse;
}

ciBoolean contentsManagerTimer::_DeleteFileInList()
{
	ciDEBUG2(5, ("_DeleteFileInList()") );
	ciGuard aGuard(_lock);

	if( _deleteingList.size() == 0 ) return ciTrue;

	contentsInfoList not_del_list;

	// check downloading-list
	contentsInfoListIter itr = _deleteingList.begin();
	contentsInfoListIter end = _deleteingList.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		if( tmp->location.length() == 0 )
		{
			// skip dummy
			not_del_list.push_back(tmp);
			continue;
		}

		if( ::access(tmp->localPath.c_str(), 00) != 0 )
		{
			// deleting-file is not exist
			ciDEBUG2(5, ("deleting-file is not exist (%s)", tmp->localPath.c_str()));
			//delete tmp;
			_completeList.push_back(tmp);
			continue;
		}
		::SetFileAttributes(tmp->localPath.c_str(), FILE_ATTRIBUTE_NORMAL); //읽기전용 해제
		if( ::DeleteFile(tmp->localPath.c_str()) )
		{
			// success to delete ==> delete info
			ciDEBUG2(5, ("success to delete (%s)", tmp->localPath.c_str()));
			//delete tmp;
			_completeList.push_back(tmp);

			// delete ppt-subfolder
			char drv[MAX_PATH]={0},dir[MAX_PATH]={0},fn[MAX_PATH]={0},ext[MAX_PATH]={0};
			_splitpath(tmp->localPath.c_str(), drv, dir, fn, ext);
			if( stricmp(ext,".ppt")==0 || stricmp(ext,".pptx")==0 )
			{
				ciDEBUG2(5, ("this is ppt-file (%s)", tmp->localPath.c_str()) );

				// delete thumbnail-directory
				sprintf(ext, "%s%s%s\\thumbs", drv, dir, fn);
				ciDEBUG2(5, ("clear thumbnail-directory (%s)", ext) );
				contentsInfo::removeDirectory(ext);

				// delete ppt-directory
				sprintf(ext, "%s%s%s", drv, dir, fn);
				ciDEBUG2(5, ("clear ppt-directory (%s)", ext) );
				contentsInfo::removeDirectory(ext);
			}

			continue;
		}

		// fail to delete
		ciWARN2(("FAIL TO DELETE !!! (ErrCode:%u) (%s)", ::GetLastError(), tmp->localPath.c_str()));
		not_del_list.push_back(tmp);
	}
	_deleteingList.clear();

	//
	itr = not_del_list.begin();
	end = not_del_list.end();
	for( ; itr!=end; itr++)
	{
		contentsInfo* tmp = (*itr);
		_deleteingList.push_back(tmp);
	}

	return ciTrue;
}
