#ifndef _contentsCheckTimer_H_
#define _contentsCheckTimer_H_

#include <ci/libTimer/ciWinTimer.h>
#include <ci/libThread/ciSyncUtil.h>

//class contentsInfoList;
class contentsManagerTimer;
#include "contents.h"


class contentsCheckTimer	: public ciWinPollable
{
public:
	contentsCheckTimer(	const char* site, 
						const char* host, 
						const char* ip, 
						ciLong port, 
						const char* id, 
						const char* pwd );
	virtual ~contentsCheckTimer();

	virtual void processExpired(ciString name, int counter, int interval);

	ciBoolean	isInitialized() { return _initialize; };
	ciBoolean	isDownloadingContents(const char* location);

	static ciString		flashPath;
	static ciString		flashSubPath;
	static ciString		flashFilename;
	static ciString		flashArgument;
protected:
	ciBoolean	_initialize;
	ciLong		_all_check_time;

	ciString	_site;
	ciString	_host;

	ciString	_svrIp;
	ciLong		_svrPort;
	ciString	_svrId;
	ciString	_svrPwd;

	//ciString	_cmsServerIp;
	//ciLong		_cmsServerPort;
	//ciString	_contentsDownloadListURL;
	//ciString	_brochureIdURL;
	//ciString	_brochureXMLURL;

	ciTime		_lastSyncTime;
	ciTime		_lastestContentsTime;

	ciString	_prevBrochureId;
	ciTime		_prevBrochureTime;
	ciString	_runningBrochureId;
	ciTime		_runningBrochureTime;

	ciString	_brochureXmlPath;

	contentsManagerTimer*	_brochConMngTimer;
	contentsManagerTimer*	_goodsConMngTimer;
	contentsManagerTimer*	_unionConMngTimer;
	contentsManagerTimer*	_deleteConMngTimer;

	void		_initTimer();
	ciBoolean	_checkContentsFile(contentsInfoList* infoList, int& goodsCount, int& brochureCount);
	void		_clearContentsInfoList(contentsInfoList* infoList);
	ciBoolean	_setLastUpdateTime(ciTime tm);
	ciBoolean	_updateLastSyncTime();

	ciString	_getAnsiXML(const char* xml);
	ciLong		_getDownloadRemainCount();
	ciLong		_getAllRemainCount();
	ciBoolean	_httpRequestPost(const char* url, const char* param, ciString& result);

	ciBoolean	_getContentsDownloadListXML(ciString& strAnsiXml);
	ciBoolean	_getBrochureId(ciString& brochureId, ciTime& brochureTime);
	ciBoolean	_getBrochureXml(const char* brochureId, ciString& strXML);
	ciLong		_getBrochureContentsInfoList(const char* xml, contentsInfoList* infoList);
	ciBoolean	_writeXML(const char* xml, ciBoolean overwrite=ciTrue);

	ciString	_getXMLforKHQS(contentsInfoList* infoList);
	ciString	_getXMLforHMCSFS(contentsInfoList* infoList);

	ciBoolean	_replaceContentsFiles(contentsInfoList* infoList);

	const char*	_getContentsType(ciString location);

	int			_getProcessCount(const char* exename);

public:
	static ciBoolean	_killFlashProcess(const char* process_name=NULL);
	static ciULong		_createProcess(const char *exe, const char* dir, const char* argument, BOOL minFlag=FALSE);
	static ciULong		_startFlashProcess();
	static ciULong		_getFlashPID();

};

#endif //_contentsCheckTimer_H_
