// CustomerCopyURLTesterDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CCustomerCopyURLTesterDlg dialog
class CCustomerCopyURLTesterDlg : public CDialog
{
// Construction
public:
	CCustomerCopyURLTesterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CUSTOMERCOPYURLTESTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_editCustomerID;
	CEdit m_editCommandList;
protected:
	virtual void OnOK();
};
