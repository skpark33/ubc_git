// ConfigVariables.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CustomerCopyURL.h"

#include <ci/libDebug/ciDebug.h>
#include <common/libCommon/ubcIni.h>
#include "libHttpRequest/HttpRequest.h"


#define		LOG_PATH		"C:\\SQISoft\\UTV1.0\\execute\\log"
#define		LOCAL_PATH		"C:\\SQISoft\\Contents\\ENC"


ciSET_DEBUG(10, "CCustomerCopyURL");


// CConfigVariables

CCustomerCopyURL::CCustomerCopyURL()
{
}

CCustomerCopyURL::~CCustomerCopyURL()
{
}

bool CCustomerCopyURL::GetCustomerId(ciString& strCustomerId)
{
	ubcConfig aIni("UBCVariables");
	ciString customer_id = "";
	aIni.get("CUSTOMER_INFO", "NAME", customer_id);
	strCustomerId = customer_id.c_str();

	return (customer_id.length() > 0);
}

bool CCustomerCopyURL::Download()
{
	ciDEBUG(3, ("Download()"));

	CHttpRequest http_req;
	http_req.Init(CHttpRequest::CONNECTION_SERVER);

	ciString str_customer_id;
	if( !GetCustomerId(str_customer_id) )
		return false;

	CString send_msg;
	send_msg.Format("customerId=%s", str_customer_id.c_str());

	ciDEBUG(3, ("send_msg=%s", send_msg));

	CStringArray line_list;
	if( !http_req.RequestPost("/UBC_Player/get_copy_url.asp", send_msg, line_list) )
		return false;
	if( line_list.GetCount() == 0)
		return false;

	const CString& first_list = line_list.GetAt(0);
	if(first_list != "Exist" && first_list != "Nothing")
		return false;

	for(int i=1; i<line_list.GetCount(); i++)
	{
		const CString& line = line_list.GetAt(i);
		ciDEBUG(3, ("line(%d):%s", i, line));
		if(line == "CodePage=utf-8") continue;

		int pos = 0;
		CString alias = line.Tokenize("=", pos);
		CString copy_url = line.Tokenize(",", pos);

		// "$(siteId)" 를 UBCSite.xml 의 siteId부분으로 치환
		if( copy_url.Find("$(siteId)") > 0 )
		{
			copy_url.Replace("$(siteId)", GetSiteId());
		}

		// wget command = wget -o "test.log" -P "C:\SQISoft\Contents\ENC" -m "http://www.naver.com/"
		CString command;
		command.Format("wget -o \"%s\\CopyURL_%s.log\" -P \"%s\" -m \"%s\" -nH"
			, LOG_PATH
			, alias
			, LOCAL_PATH
			, copy_url );

		ciDEBUG(3, ("command=%s", command));

		STARTUPINFO si = {0};
		si.cb = sizeof(si);
		si.dwFlags = STARTF_USESHOWWINDOW;
		si.wShowWindow = SW_MINIMIZE;

		PROCESS_INFORMATION pi = {0};

		if( ::CreateProcess( NULL,   // No module name (use command line). 
			(LPSTR)(LPCSTR)command, // Command line. 
			NULL,             // Process handle not inheritable. 
			NULL,             // Thread handle not inheritable. 
			FALSE,            // Set handle inheritance to FALSE. 
			0,                // No creation flags. 
			NULL,             // Use parent's environment block. 
			NULL,             // Use parent's starting directory. 
			&si,              // Pointer to STARTUPINFO structure.
			&pi )             // Pointer to PROCESS_INFORMATION structure.
			)
		{
			// success
			::WaitForSingleObject(pi.hProcess, INFINITE); 
			::CloseHandle(pi.hProcess);

			if( copy_url.Find("?") > 0 )
			{
				// '?'가 들어갈 경우 '@'로 바뀌고 모든 파라미터가 뒤에 붙음
				DWORD svr_type;
				CString str_server, str_obj;
				INTERNET_PORT port;
				::AfxParseURL(copy_url, svr_type, str_server, str_obj, port);

				CString str_oldpath;
				str_oldpath.Format("%s%s", LOCAL_PATH, str_obj);
				str_oldpath.Replace("/", "\\");
				str_oldpath.Replace("?", "@");

				int pos = str_oldpath.Find("@");
				if( pos > 0 )
				{
					CString str_newpath;
					str_newpath = str_oldpath.Left(pos);

					ciDEBUG(3, ("MoveFile(%s ==> %s)", str_oldpath, str_newpath));
					::MoveFile(str_oldpath, str_newpath);
				}
			}
		}
		else
		{
			// fail
		}
	}

	return true;
}

const char*	CCustomerCopyURL::GetSiteId()
{
	static CString str_siteid = "";
	if( str_siteid.GetLength() == 0 )
	{
		//
		// get from C:\SQISoft\UTV1.0\execute\flash\xml\UBCSite.xml
		// <siteId>...</siteId>
		//

		CString xml_path = _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\");
		xml_path += "flash\\xml\\UBCSite.xml";

		CFile file;
		if( file.Open(xml_path, CFile::modeRead | CFile::typeBinary) )
		{
			int size = file.GetLength();
			char* buf = new char[size+1];
			::ZeroMemory(buf, size+1);

			file.Read((LPVOID)buf, size);
			CString str_buf = buf;

			int pos_start = str_buf.Find("<siteId>");
			int pos_end = str_buf.Find("</siteId>", pos_start);
			if( pos_start > 0 && pos_end > 0 )
			{
				pos_start += 8;
				str_siteid = str_buf.Mid(pos_start, pos_end-pos_start);
			}

			delete[]buf;
		}
	}

	return str_siteid;
}


//////////////////////////////////////////////////////////////////
CCustomerCopyURLTimer* CCustomerCopyURLTimer::_instance = NULL;
ciMutex CCustomerCopyURLTimer::_instanceLock;

CCustomerCopyURLTimer* CCustomerCopyURLTimer::getInstance()
{
	if(!_instance) {
		ciGuard aGuard(_instanceLock);
		if(!_instance) {
			_instance = new CCustomerCopyURLTimer;
		}
	}
	return _instance;
}

void CCustomerCopyURLTimer::clearInstance()
{
	if(_instance) {
		ciGuard aGuard(_instanceLock);
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}

CCustomerCopyURLTimer::CCustomerCopyURLTimer()
{
	_isAlive = true;
}

CCustomerCopyURLTimer::~CCustomerCopyURLTimer()
{
	_isAlive = false;
	::Sleep(200);
	stop();
}

void CCustomerCopyURLTimer::run()
{
	bool is_copied_url = false;
	time_t last_success_time = 0;
	time_t last_try_time = 0;
	while(_isAlive)
	{
		time_t cur_time = CTime::GetCurrentTime().GetTime();

		if(cur_time-last_success_time > (60*60*24))	// every 24-hour
			is_copied_url = false;

		if(is_copied_url == false && cur_time-last_try_time > 60) // every 1-minute
		{
			last_try_time = cur_time;

			CCustomerCopyURL ccu;
			if(ccu.Download())
			{
				is_copied_url = true;
				last_success_time = CTime::GetCurrentTime().GetTime();
			}
		}

		::Sleep(100);
	}
}
