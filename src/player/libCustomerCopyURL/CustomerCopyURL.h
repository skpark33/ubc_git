#pragma once

#include "ci/libThread/ciThread.h"

class CStringArray;

////////////////////////////////////////////////////
class CCustomerCopyURL
{
public:
	CCustomerCopyURL();
	virtual ~CCustomerCopyURL();

	bool	GetCustomerId(ciString& strCustomerId);
	const char*	GetSiteId();
	bool	Download();
};

////////////////////////////////////////////////////
class CCustomerCopyURLTimer : public ciThread
{
public:
	static CCustomerCopyURLTimer*	getInstance();
	static void	clearInstance();

protected:
	static CCustomerCopyURLTimer*		_instance;
	static ciMutex			_instanceLock;

	CCustomerCopyURLTimer();
public:
	virtual ~CCustomerCopyURLTimer();

	virtual void run();

protected:
	bool	_isAlive;
};
