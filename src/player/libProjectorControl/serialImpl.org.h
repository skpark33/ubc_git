 /*! \file serialImpl.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _serialImpl_h_
#define _serialImpl_h_

#include <ci/libBase/ciBaseType.h>
#include "libSerial/serialListener.h"


class serialImpl : public CSerialListener
{
public:
	virtual bool open(	const char* device="COM1", 
							int baudrate=CSerial::EBaud9600, 
							int dataBit=CSerial::EData8, 
							int parityBit=CSerial::EParNone, 
							int stopBit=CSerial::EStop1,
							unsigned long eof=27 ); // Ctrl+'[');

	BOOL	Init(LPCSTR szDevice = "COM1", 
				int nBaudRate = CSerial::EBaud9600, 
				int nDataBit = CSerial::EData8, 
				int nParityBit = CSerial::EParNone, 
				int nStopBit = CSerial::EStop1,
				char chEOF = '\n',
				int nTimeout = 3000 );

	BOOL	Send(LPCSTR lpszBuf, DWORD size);
	BOOL	Read(LPSTR lpszBuf, DWORD& size, DWORD max_size);

protected:
	int		m_nTimeout;
};

#endif //_serialImpl_h_
