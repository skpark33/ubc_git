#pragma once

#include <list>
#include <windows.h>
#include "serialImpl.h"
#include <ci/libThread/ciSyncUtil.h>
#include <ci/libBase/ciTime.h>


///////////////////////////////////
typedef enum
{
	eProjectorType_UNKNOWN = 0,
	eProjectorType_start = 1,		// DO NOT USE. Just start...
	eProjectorType_SHINDORICO = 1,	// 신도리코
	eProjectorType_LG,				// LG
	eProjectorType_CANNON,			// Cannon
	eProjectorType_LG_NEW,			// LG new ver (BX286)

	eProjectorType_count,			// DO NOT USE. Just count...
}
eProjectorType;


///////////////////////////////////
typedef enum
{
	eProjectorRV_UNKNOWN = 0,			// 알수없는 에러
	eProjectorRV_SUCCESS,				// 명령 성공
	eProjectorRV_FAIL_TO_INIT,			// 초기화 실패
	eProjectorRV_FAIL_TO_SEND,			// 명령전송 실패
	eProjectorRV_FAIL_TO_RECEIVE,		// 명령수신 실패
	eProjectorRV_FAIL_TO_RUN_COMMAND,	// 명령실행 에러
	eProjectorRV_INVALID_RETURN_VALUE,	// 알수없는 리턴값

	eProjectorRV_count,					// DO NOT USE. Just count...
}
eProjectorReturnValue;


///////////////////////////////////
typedef enum
{
	eProjectorStatus_UNKNOWN = 0,
	eProjectorStatus_PowerOn,		// ON
	eProjectorStatus_PowerOff,		// OFF

	eProjectorStatus_count,			// DO NOT USE. Just count...
}
eProjectorStatus;


///////////////////////////////////
typedef struct
{
	eProjectorType type;
	eProjectorReturnValue ret_value;
} eProjectorReturnValuePair;

typedef std::list<eProjectorReturnValuePair>	eProjectorReturnValueList;

//////////////////////////////////////
typedef	list<serialImpl*>		serialImplList;


///////////////////////////////////
class CProjectorControl
{
public:
	static bool		AllPowerOn(eProjectorReturnValueList* rv_list=NULL);		// 하나라도 성공하면 true리턴. rv_list에 각 타입의 결과값 출력
	static bool		AllPowerOff(eProjectorReturnValueList* rv_list=NULL);		// 하나라도 성공하면 true리턴. rv_list에 각 타입의 결과값 출력
	static bool		AllPowerStatus(eProjectorReturnValueList* rv_list=NULL);	// 하나라도 파워가 켜져있으면 true리턴. rv_list에 각 타입의 결과값 출력
	static eProjectorType	SmartPowerOn();
	static eProjectorType	SmartPowerOff();

	CProjectorControl();
	virtual ~CProjectorControl();
	const char* ToString(eProjectorType ePT);

protected:
	eProjectorType	m_eProjectorType;

	//char			m_szDevice[16];
	ciStringList	m_serialPortList;
	int				m_nBaudRate;
	int				m_nDataBit;
	int				m_nParityBit;
	int				m_nStopBit;
	char			m_chEOF;
	int				m_nTimeout;

	char			m_szProjectorID[16];

	//serialImpl		m_serial;
	list<serialImpl*>	m_serialList;

	eProjectorReturnValue		ExecuteSerialCommand(LPCSTR szCommand, int nCmdSize, ciString& strOutStr);

public:

	void	Init(eProjectorType ePT,
				 LPCSTR szDevice = NULL, 
				 int nBaudRate = CSerial::EBaudUnknown, 
				 int nDataBit = CSerial::EDataUnknown, 
				 int nParityBit = CSerial::EParUnknown, 
				 int nStopBit = CSerial::EStopUnknown );
	void	SetProjectorID(int id);

	void	Close();

	eProjectorReturnValue		PowerOn();
	eProjectorReturnValue		PowerOff();

	eProjectorReturnValue		PowerStatus(eProjectorStatus& status);

	eProjectorReturnValue		GetLampHour(int& hour);	// 램프사용시간. 실패시 -1 리턴.
};

class ProjectorWrapper {
public:
	static ProjectorWrapper*	getInstance();
	static void	clearInstance();

	virtual ~ProjectorWrapper() ;

	ciBoolean	projectorOn();
	ciBoolean	projectorOff();
	int			getState();
	int			getLampHour();

	int			getLastState() { return _lastState; }
	time_t		getLastStateTime() { return _lastStateTime.getTime(); }
	void		setLastState(int state);

protected:
	ProjectorWrapper();
	static ProjectorWrapper*	_instance;
	static ciMutex			_instanceLock;

	ciBoolean	projectorOn(int eType);
	ciBoolean	projectorOff(int eType);
	int			getState(int eType);
	int			getLampHour(int eType);


	ciMutex	_lock;
	int		_pType;

	ciTime	_lastStateTime;
	int		_lastState;
protected:
};


