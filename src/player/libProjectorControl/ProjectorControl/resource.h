//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ProjectorControl.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PROJECTORCONTROL_DIALOG     102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_COMBO_PROJECTOR_TYPE        1000
#define IDC_BUTTON_POWER_ON             1001
#define IDC_BUTTON_POWER_OFF            1002
#define IDC_BUTTON1                     1003
#define IDC_BUTTON_POWER_STATUS         1003
#define IDC_LIST                        1004
#define IDC_BUTTON_PS_START             1005
#define IDC_BTN_PS_END                  1006
#define IDC_BUTTON_PS_END               1006
#define IDC_EDIT_INTERVAL               1007
#define IDC_STATIC_REMAIN               1008
#define IDC_BUTTON2                     1009
#define IDC_BUTTON_LAMP_HOUR            1009
#define IDC_COMBO_COMPORT               1010
#define IDC_COMBO2                      1011
#define IDC_COMBO_PID                   1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
