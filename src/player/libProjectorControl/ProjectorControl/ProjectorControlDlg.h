// ProjectorControlDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CProjectorControlDlg 대화 상자
class CProjectorControlDlg : public CDialog
{
// 생성입니다.
public:
	CProjectorControlDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PROJECTORCONTROL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()

	int		m_nInterval;
	int		m_nRemainInterval;

	int		GetProjectorID() { return m_cbxPID.GetCurSel(); }; 
public:
	CComboBox	m_cbxProjectorType;
	CComboBox	m_cbxComPort;
	CComboBox	m_cbxPID;

	CButton		m_btnPowerOn;
	CButton		m_btnPowerOff;
	CButton		m_btnPowerStatus;
	CButton		m_btnLampHour;
	CButton		m_btnClose;

	CListCtrl	m_lc;
	CStatic		m_stcRemain;
	CEdit		m_editInterval;
	CButton		m_btnStart;
	CButton		m_btnEnd;

	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnCbnSelchangeComboProjectorType();
	afx_msg void OnBnClickedButtonPowerOn();
	afx_msg void OnBnClickedButtonPowerOff();
	afx_msg void OnBnClickedButtonPowerStatus();
	afx_msg void OnBnClickedButtonLampHour();
	afx_msg void OnBnClickedButtonPsStart();
	afx_msg void OnBnClickedButtonPsEnd();
};
