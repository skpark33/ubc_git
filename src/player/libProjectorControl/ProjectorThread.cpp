#include "stdafx.h"

#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include "ProjectorControl.h"

#include "ProjectorThread.h"

ciSET_DEBUG(10, "ProjectorThread");

ProjectorThread* 	ProjectorThread::_instance = 0; 
ciMutex 	ProjectorThread::_instanceLock;

ProjectorThread*	
ProjectorThread::getInstance() {
	if(!_instance) {
		ciGuard aGuard(_instanceLock);
		if(!_instance) {
			_instance = new ProjectorThread;
		}
	}
	return _instance;
}

void	
ProjectorThread::clearInstance() {
	if(_instance) {
		ciGuard aGuard(_instanceLock);
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}

ProjectorThread::ProjectorThread() 
{
    ciDEBUG(7, ("ProjectorThread()"));
	_isRun = ciFalse;
	_action = STATE;
	_retry = 1;
	_ptype = -1;
}

ProjectorThread::~ProjectorThread() 
{
    ciDEBUG(7, ("~ProjectorThread()"));
}

void
ProjectorThread::run()
{
	if(_ptype < 0){
		ciERROR(("projector type does not set"));
	}
	ciDEBUG2(7,("run ProjectorThread()"));
	{	
		ciGuard aGuard(_lock);
		_isRun = ciTrue;
	}
	ciShort retval=0;
	int off_failCounter=0;
	int on_failCounter=0;

	switch(_action) {
		case STATE : 
			{
				retval = ProjectorWrapper::getInstance()->getState(_ptype); 
				break;
			}
		case OFF : 
			{
				while(_retry > off_failCounter){
					if(!ProjectorWrapper::getInstance()->projectorOff(_ptype)){
						ciERROR(("Project Off failed retry=%d", off_failCounter));
						off_failCounter++;
						::SLEEP(1000*10);
						continue;
					}
					ciDEBUG(1,("Project Off Succeed"));
					break;
				}
				break;
			}
		case ON : 
			{
				while(_retry > on_failCounter){
					if(!ProjectorWrapper::getInstance()->projectorOn(_ptype)){
						ciERROR(("Project On failed retry=%d", on_failCounter));
						on_failCounter++;
						::SLEEP(1000*10);
						continue;
					}
					ciDEBUG(1,("Project On Succeed"));
					break;
				}
				break;
			}
	}
	{	
		ciGuard aGuard(_lock);
		_isRun = ciFalse;
	}
	ciDEBUG2(7,("run ProjectorThread() end"));
	return;
}

ciBoolean
ProjectorThread::isRun()
{
	ciDEBUG(7,("isRun()"));
	ciGuard aGuard(_lock);
	return _isRun;
}
