#include "stdafx.h"

#include <ci/libDebug/ciDebug.h>
#include <CMN/libCommon/ubcIni.h>
#include "ProjectorControl.h"

#define		MAX_LINE_LENGTH		1024

ciSET_DEBUG(10, "CProjectorControl");


static char* szProjectorTypeString[eProjectorType_count] = {
	"eProjectorType_UNKNOWN",
	"eProjectorType_SHINDORICO",
	"eProjectorType_LG",
	"eProjectorType_CANNON",
	"eProjectorType_LG_NEW",
};

static char* szProjectorRVString[eProjectorRV_count] = {
	"eProjectorRV_UNKNOWN",
	"eProjectorRV_SUCCESS",
	"eProjectorRV_FAIL_TO_INIT",
	"eProjectorRV_FAIL_TO_SEND",
	"eProjectorRV_FAIL_TO_RECEIVE",
	"eProjectorRV_FAIL_TO_RUN_COMMAND",
	"eProjectorRV_INVALID_RETURN_VALUE",
};

static char* szProjectorStatusString[eProjectorStatus_count] = {
	"eProjectorStatus_UNKNOWN",
	"eProjectorStatus_PowerOn",
	"eProjectorStatus_PowerOff",
};



bool CProjectorControl::AllPowerOn(eProjectorReturnValueList* rv_list)
{
	bool succ_on = false;

	for(int i=eProjectorType_start; i<eProjectorType_count; i++)
	{
		CProjectorControl pc;
		pc.Init((eProjectorType)i);
		eProjectorReturnValue ret_val = pc.PowerOn();

		if(rv_list)
		{
			eProjectorReturnValuePair pair;
			pair.type = (eProjectorType)i;
			pair.ret_value = ret_val;

			rv_list->push_back(pair);
		}

		if(ret_val == eProjectorRV_SUCCESS) succ_on = true;
	}

	return succ_on;
}

bool CProjectorControl::AllPowerOff(eProjectorReturnValueList* rv_list)
{
	bool succ_off = false;

	for(int i=eProjectorType_start; i<eProjectorType_count; i++)
	{
		CProjectorControl pc;
		pc.Init((eProjectorType)i);
		eProjectorReturnValue ret_val = pc.PowerOff();

		if(rv_list)
		{
			eProjectorReturnValuePair pair;
			pair.type = (eProjectorType)i;
			pair.ret_value = ret_val;

			rv_list->push_back(pair);
		}

		if(ret_val == eProjectorRV_SUCCESS) succ_off = true;
	}

	return succ_off;
}

bool CProjectorControl::AllPowerStatus(eProjectorReturnValueList* rv_list)
{
	bool succ_on_status = false;

	for(int i=eProjectorType_start; i<eProjectorType_count; i++)
	{
		CProjectorControl pc;
		pc.Init((eProjectorType)i);
		eProjectorStatus status;
		eProjectorReturnValue ret_val = pc.PowerStatus(status);

		if(rv_list)
		{
			eProjectorReturnValuePair pair;
			pair.type = (eProjectorType)i;
			pair.ret_value = ret_val;

			rv_list->push_back(pair);
		}

		if(ret_val == eProjectorRV_SUCCESS && status == eProjectorStatus_PowerOn) succ_on_status = true;
	}

	return succ_on_status;
}

eProjectorType CProjectorControl::SmartPowerOn()
{
	ciDEBUG(10,("SmartPowerOn()"));

	ubcConfig aIni("UBCVariables");
	ciLong pre_prj_type = 0;
	aIni.get("PROJECTOR","ProjectorType", pre_prj_type);

	ciDEBUG(10,("get [PROJECTOR]_ProjectorType from UBCVariables.ini = %d_%s", pre_prj_type, szProjectorTypeString[pre_prj_type] ));

	if(0 < pre_prj_type && pre_prj_type < eProjectorType_count)
	{
		for(int i=0; i<2; i++) // loop twice because pre-success
		{
			CProjectorControl pc;
			pc.Init((eProjectorType)pre_prj_type);
			eProjectorReturnValue ret_val = pc.PowerOn();

			if(ret_val == eProjectorRV_SUCCESS)
			{
				//success
				ciDEBUG(10,("success ProjectorType(%d_%s) power on", pre_prj_type, szProjectorTypeString[pre_prj_type] ));

				// check status
				for(int j=0; j<6; j++) // loop during 1min
				{
					::Sleep(10 * 1000); // sleep 10sec
					eProjectorStatus status = eProjectorStatus_UNKNOWN;
					ret_val = pc.PowerStatus(status);

					if(ret_val == eProjectorRV_SUCCESS && status == eProjectorStatus_PowerOn)
					{
						ciDEBUG(10,("success ProjectorType(%d_%s) power-on status", pre_prj_type, szProjectorTypeString[pre_prj_type] ));
						return (eProjectorType)pre_prj_type;
					}
				}
			}
			::Sleep(10 * 1000); // sleep 10sec
		}
	}

	ciWARN(("fail to power on previous ProjectorType(%d_%s) !!!", pre_prj_type, szProjectorTypeString[pre_prj_type] ));

	// fail or not-setting
	ciLong new_prj_type_retry = 0;
	for(ciLong new_prj_type=eProjectorType_start; new_prj_type<eProjectorType_count; new_prj_type++)
	{
		if(new_prj_type == pre_prj_type) continue; // skip pre_projector_type

		CProjectorControl pc;
		pc.Init((eProjectorType)new_prj_type);
		eProjectorReturnValue ret_val = pc.PowerOn();

		if(ret_val == eProjectorRV_SUCCESS)
		{
			aIni.set("PROJECTOR","ProjectorType", new_prj_type);
			ciDEBUG(10,("success ProjectorType(%d_%s) power on", new_prj_type, szProjectorTypeString[new_prj_type] ));

			// check status
			for(int j=0; j<6; j++) // loop during 1min
			{
				eProjectorStatus status = eProjectorStatus_UNKNOWN;
				ret_val = pc.PowerStatus(status);

				if(ret_val == eProjectorRV_SUCCESS && status == eProjectorStatus_PowerOn)
				{
					ciDEBUG(10,("success ProjectorType(%d_%s) power-on status", pre_prj_type, szProjectorTypeString[pre_prj_type] ));
					return (eProjectorType)pre_prj_type;
				}
				::Sleep(10 * 1000); // sleep 10sec
			}
		}

		if(new_prj_type_retry != new_prj_type)
		{
			// loop twice
			new_prj_type_retry = new_prj_type;
			new_prj_type--;
		}

		::Sleep(10 * 1000); // sleep 10sec
	}

	ciWARN(("All fail to power on !!!"));

	return (eProjectorType)0;
}

eProjectorType CProjectorControl::SmartPowerOff()
{
	ciDEBUG(10,("SmartPowerOff()"));

	ubcConfig aIni("UBCVariables");
	ciLong pre_prj_type = 0;
	aIni.get("PROJECTOR","ProjectorType", pre_prj_type);

	ciDEBUG(10,("get [PROJECTOR]_ProjectorType from UBCVariables.ini = %d_%s", pre_prj_type, szProjectorTypeString[pre_prj_type] ));

	if(0 < pre_prj_type && pre_prj_type < eProjectorType_count)
	{
		for(int i=0; i<2; i++) // loop twice because pre-success
		{
			CProjectorControl pc;
			pc.Init((eProjectorType)pre_prj_type);
			eProjectorReturnValue ret_val = pc.PowerOff();

			if(ret_val == eProjectorRV_SUCCESS)
			{
				//success
				ciDEBUG(10,("success ProjectorType(%d_%s) power off", pre_prj_type, szProjectorTypeString[pre_prj_type] ));

				// check status
				for(int j=0; j<6; j++) // loop during 1min
				{
					::Sleep(10 * 1000); // sleep 10sec
					eProjectorStatus status = eProjectorStatus_UNKNOWN;
					ret_val = pc.PowerStatus(status);

					if(ret_val == eProjectorRV_SUCCESS && status == eProjectorStatus_PowerOff)
					{
						ciDEBUG(10,("success ProjectorType(%d_%s) power-off status", pre_prj_type, szProjectorTypeString[pre_prj_type] ));
						return (eProjectorType)pre_prj_type;
					}
				}
			}
			::Sleep(10 * 1000); // sleep 10sec
		}
	}

	ciWARN(("fail to power off previous ProjectorType(%d_%s) !!!", pre_prj_type, szProjectorTypeString[pre_prj_type] ));

	// fail or not-setting
	ciLong new_prj_type_retry = 0;
	for(ciLong new_prj_type=eProjectorType_start; new_prj_type<eProjectorType_count; new_prj_type++)
	{
		if(new_prj_type == pre_prj_type) continue; // skip pre_projector_type

		CProjectorControl pc;
		pc.Init((eProjectorType)new_prj_type);
		eProjectorReturnValue ret_val = pc.PowerOff();

		if(ret_val == eProjectorRV_SUCCESS)
		{
			aIni.set("PROJECTOR","ProjectorType", new_prj_type);
			ciDEBUG(10,("success ProjectorType(%d_%s) power off", new_prj_type, szProjectorTypeString[new_prj_type] ));

			// check status
			for(int j=0; j<6; j++) // loop during 1min
			{
				eProjectorStatus status = eProjectorStatus_UNKNOWN;
				ret_val = pc.PowerStatus(status);

				if(ret_val == eProjectorRV_SUCCESS && status == eProjectorStatus_PowerOff)
				{
					ciDEBUG(10,("success ProjectorType(%d_%s) power-off status", pre_prj_type, szProjectorTypeString[pre_prj_type] ));
					return (eProjectorType)pre_prj_type;
				}
				::Sleep(10 * 1000); // sleep 10sec
			}
		}

		if(new_prj_type_retry != new_prj_type)
		{
			// loop twice
			new_prj_type_retry = new_prj_type;
			new_prj_type--;
		}

		::Sleep(10 * 1000); // sleep 10sec
	}

	ciWARN(("All fail to power off !!!"));

	return (eProjectorType)0;
}

CProjectorControl::CProjectorControl()
:	m_eProjectorType ( eProjectorType_UNKNOWN )
,	m_nBaudRate ( 0 )
,	m_nDataBit ( 0 )
,	m_nParityBit ( 0 )
,	m_nStopBit ( 0 )
,	m_chEOF ( 0 )
,	m_nTimeout ( 3000 )
{
	strcpy(m_szProjectorID, "01");
}

CProjectorControl::~CProjectorControl()
{
	Close();  //skpark 2011.09.07
}

void CProjectorControl::Init(eProjectorType ePT, LPCSTR szDevice, int nBaudRate, int nDataBit, int nParityBit, int nStopBit)
{
	ciString basic_serial_port = "";

	m_eProjectorType = ePT;
	if(szDevice) basic_serial_port = szDevice;
	m_nBaudRate	= nBaudRate;
	m_nDataBit = nDataBit;
	m_nParityBit = nParityBit;
	m_nStopBit = nStopBit;

	ciDEBUG(10,("ProjectorType(%d_%s)", ePT, szProjectorTypeString[ePT] ));
	switch(ePT)
	{
	case eProjectorType_SHINDORICO:
		if(m_nBaudRate == CSerial::EBaudUnknown) m_nBaudRate = CSerial::EBaud115200;
		m_chEOF = '\n'; // 0x0A
		m_nTimeout = 2000;
		break;
	case eProjectorType_LG:
		m_chEOF = 'x';
		m_nTimeout = 1500;
		break;
	case eProjectorType_CANNON:
		m_chEOF = '\r'; // 0x0D
		m_nTimeout = 2000;
		break;
	case eProjectorType_LG_NEW:
		m_chEOF = '\r'; // 0x0D
		m_nTimeout = 1000;
		break;
	}

	if(basic_serial_port.length() == 0)			basic_serial_port = "COM1";
	m_serialPortList.push_back(basic_serial_port);
	if(m_nBaudRate  == CSerial::EBaudUnknown)	m_nBaudRate  = CSerial::EBaud9600;
	if(m_nDataBit   == CSerial::EDataUnknown)	m_nDataBit   = CSerial::EData8;
	if(m_nParityBit == CSerial::EParUnknown)	m_nParityBit = CSerial::EParNone;
	if(m_nStopBit   == CSerial::EStopUnknown)	m_nStopBit   = CSerial::EStop1;

	ciDEBUG(10,("device(%s)", basic_serial_port.c_str()) );
	ciDEBUG(10,("baudrate(%d)", m_nBaudRate));
	ciDEBUG(10,("databit(%d)", m_nDataBit));
	ciDEBUG(10,("paritybit(%d)", m_nParityBit));
	ciDEBUG(10,("stopbit(%d)", m_nStopBit));

	// check additional-serial-port
	ubcConfig aIni("UBCVariables");
	ciString add_serial_port = "";
	aIni.get("PROJECTOR","AdditionalComPort", add_serial_port);
	ciDEBUG(10,("AdditionComPortList(%s)", add_serial_port.c_str()) );

	if(add_serial_port.length() > 0)
	{
		char* tokens = (char*)add_serial_port.c_str();
		char* token = strtok( tokens, "," );
		while( token != NULL )
		{
			m_serialPortList.push_back(token);
			ciDEBUG(10,("AdditionComPort(%s)", token) );
			token = strtok( NULL, "," );
		}
	}

	// create serial-object
	ciStringList::iterator itr = m_serialPortList.begin();
	ciStringList::iterator end = m_serialPortList.end();
	for(; itr!=end; itr++)
	{
		ciString& port = (ciString&)*itr;
		if(port.length() == 0) continue;

		serialImpl* serial = new serialImpl;
		m_serialList.push_back(serial);
	}
}

void CProjectorControl::SetProjectorID(int id)
{
	sprintf(m_szProjectorID, "%02d", id);
}

void CProjectorControl::Close()
{
	ciStringList::iterator port_itr = m_serialPortList.begin();

	serialImplList::iterator itr = m_serialList.begin();
	serialImplList::iterator end = m_serialList.end();

	for(; itr!=end; itr++, port_itr++ )
	{
		ciString& str_port = (ciString&)*port_itr;
		serialImpl* serial = (serialImpl*)*itr;
		if(serial == NULL) continue;

		ciDEBUG(10,("Destroy Serial-Port(%s)", str_port.c_str()));
		serial->close();
		delete serial;
	}

	m_serialList.clear();
	m_serialPortList.clear();
}

eProjectorReturnValue CProjectorControl::ExecuteSerialCommand(LPCSTR szCommand, int nCmdSize, ciString& strOutStr)
{
	ciDEBUG(10,("Execute(%s)", szCommand));

	ciStringList::iterator port_itr = m_serialPortList.begin();

	serialImplList::iterator begin = m_serialList.begin();
	serialImplList::iterator end   = m_serialList.end();

	eProjectorReturnValue ret_val = eProjectorRV_SUCCESS;

	for(serialImplList::iterator itr=begin; itr!=end; itr++, port_itr++)
	{
		ciString& str_port = (ciString&)*port_itr;
		serialImpl* serial = (serialImpl*)*itr;
		if(serial == NULL) continue;

		//
		if( !serial->Init(str_port.c_str(), m_nBaudRate, m_nDataBit, m_nParityBit, m_nStopBit, m_chEOF) )
		{
			ciWARN(("FAIL_TO_INIT(%s)", str_port.c_str()));
			serial->close();
			if(itr==begin) ret_val = eProjectorRV_FAIL_TO_INIT;
		}

		//
		if( !serial->Send(szCommand, nCmdSize) )
		{
			ciWARN(("FAIL_TO_SEND(%s)", str_port.c_str()));
			serial->close();
			if(itr==begin) ret_val = eProjectorRV_FAIL_TO_SEND;
		}

		//
		char recv_buf[MAX_LINE_LENGTH] = {0};
		DWORD recv_size = 0;
		BOOL read = serial->Read(recv_buf, recv_size, MAX_LINE_LENGTH);
		strOutStr = recv_buf;
		serial->close();

		//
		ciString str = "";
		for(int idx=0; idx<recv_size; idx++)
		{
			if(32 <= recv_buf[idx] && recv_buf[idx] < 127)
			{
				str += recv_buf[idx];
			}
			else
			{
				char buf[16] = {0};
				sprintf(buf, "\\%02x", recv_buf[idx]);
				str += buf;
			}
		}
		ciDEBUG(10,("Read(%s)(%s)", str_port.c_str(), str.c_str()));

		//
		if( !read )
		{
			ciWARN(("FAIL_TO_RECEIVE(%s)", str_port.c_str()));
			if(itr==begin) ret_val = eProjectorRV_FAIL_TO_RECEIVE;
		}
	}

	//
	ciDEBUG(10,("ExecuteResult=%d_%s", ret_val, szProjectorRVString[ret_val]));
	return ret_val;
}

eProjectorReturnValue CProjectorControl::PowerOn()
{
	eProjectorReturnValue ret_val = eProjectorRV_UNKNOWN;
	ciString out_str[3];
	ciStringList succ_power_on, fail_power_on;

	switch(m_eProjectorType)
	{
	case eProjectorType_SHINDORICO:
		{
			static const char cmd_power_on[] = "#P1\r\n";
			succ_power_on.push_back("#P11\r\n");
			fail_power_on.push_back("#P10\r\n");

			ret_val = ExecuteSerialCommand(cmd_power_on, strlen(cmd_power_on), out_str[0] );
		}
		break;

	case eProjectorType_LG:
		{
			char cmd_power_on_all[] = "ka 00 01\r";
			char cmd_power_on[] = "ka 01 01\r";
			memcpy(cmd_power_on+3, m_szProjectorID, 2);

			char cmd_power_on_succ[] = "a 01 OK01x";
			char cmd_power_on_fail[] = "a 01 NG01x";
			memcpy(cmd_power_on_succ+2, m_szProjectorID, 2);
			memcpy(cmd_power_on_fail+2, m_szProjectorID, 2);
			succ_power_on.push_back(cmd_power_on_succ);
			fail_power_on.push_back(cmd_power_on_fail);

			ExecuteSerialCommand(cmd_power_on_all, strlen(cmd_power_on_all), out_str[0] );
			ExecuteSerialCommand(cmd_power_on_all, strlen(cmd_power_on_all), out_str[1] );
			ret_val = ExecuteSerialCommand(cmd_power_on, strlen(cmd_power_on), out_str[2] );
		}
		break;

	case eProjectorType_CANNON:
		{
			static const char cmd_power_on[] = "00!\r";
			succ_power_on.push_back("00!\r");
			fail_power_on.push_back("00\"\r");

			ExecuteSerialCommand(cmd_power_on, strlen(cmd_power_on), out_str[0] );
			ret_val = ExecuteSerialCommand(cmd_power_on, strlen(cmd_power_on), out_str[1] );
		}
		break;

	case eProjectorType_LG_NEW:
		{
			char cmd_power_on_all[] = "~0000 1\r";
			char cmd_power_on[] = "~0100 1\r";
			memcpy(cmd_power_on+1, m_szProjectorID, 2);

			char succ_str_sample1[] = "RS232 power on...";
			char succ_str_sample2[] = " RS232 power on..."; succ_str_sample2[0] = 0x01;
			char succ_str_sample3[] = "S081";
			char succ_str_sample4[] = " fffff0S081"; succ_str_sample4[0] = 0xff;

			succ_power_on.push_back(succ_str_sample1);
			succ_power_on.push_back(succ_str_sample2);
			succ_power_on.push_back(succ_str_sample3);
			succ_power_on.push_back(succ_str_sample4);
			fail_power_on.push_back("F");

			ExecuteSerialCommand(cmd_power_on_all, strlen(cmd_power_on_all), out_str[0] );
			ExecuteSerialCommand(cmd_power_on_all, strlen(cmd_power_on_all), out_str[1] );
			ret_val = ExecuteSerialCommand(cmd_power_on, strlen(cmd_power_on), out_str[2] );
		}
		break;

	default:
		break;
	}
	ciDEBUG(10,("PowerOn(%d_%s)=%d_%s", m_eProjectorType, szProjectorTypeString[m_eProjectorType], ret_val, szProjectorRVString[ret_val] ));

	//
	if(ret_val == eProjectorRV_SUCCESS)
	{
		ret_val = eProjectorRV_INVALID_RETURN_VALUE;

		for (ciStringList::iterator itr = succ_power_on.begin(); itr != succ_power_on.end(); itr++ )
		{
			if( memcmp (out_str[0].c_str(), (*itr).c_str(), (*itr).size() ) == 0 ||
				memcmp (out_str[1].c_str(), (*itr).c_str(), (*itr).size() ) == 0 ||
				memcmp (out_str[2].c_str(), (*itr).c_str(), (*itr).size() ) == 0 )
			{
				ret_val = eProjectorRV_SUCCESS;
				break;
			}
		}

		if(ret_val != eProjectorRV_SUCCESS)
		{
			for (ciStringList::iterator itr = fail_power_on.begin(); itr != fail_power_on.end(); itr++ )
			{
				if( memcmp (out_str[0].c_str(), (*itr).c_str(), (*itr).size() ) == 0 ||
					memcmp (out_str[1].c_str(), (*itr).c_str(), (*itr).size() ) == 0 ||
					memcmp (out_str[2].c_str(), (*itr).c_str(), (*itr).size() ) == 0 )
				{
					ret_val = eProjectorRV_FAIL_TO_RUN_COMMAND;
					break;
				}
			}
		}
	}

	ciDEBUG(10,("PowerOn Result=%d_%s", ret_val, szProjectorRVString[ret_val] ));
	return ret_val;
}

eProjectorReturnValue CProjectorControl::PowerOff()
{
	eProjectorReturnValue ret_val = eProjectorRV_UNKNOWN;
	ciString out_str[3];
	ciStringList succ_power_off, fail_power_off;

	switch(m_eProjectorType)
	{
	case eProjectorType_SHINDORICO:
		{
			static const char cmd_power_off[] = "#P0\r\n";
			succ_power_off.push_back("#P01\r\n");
			fail_power_off.push_back("#P00\r\n");

			ret_val = ExecuteSerialCommand(cmd_power_off, strlen(cmd_power_off), out_str[0] );
		}
		break;

	case eProjectorType_LG:
		{
			char cmd_power_off_all[] = "ka 00 00\r";
			char cmd_power_off[] = "ka 01 00\r";
			memcpy(cmd_power_off+3, m_szProjectorID, 2);

			char cmd_power_off_succ[] = "a 01 OK00x";
			char cmd_power_off_fail[] = "a 01 NG00x";
			memcpy(cmd_power_off_succ+2, m_szProjectorID, 2);
			memcpy(cmd_power_off_fail+2, m_szProjectorID, 2);

			succ_power_off.push_back(cmd_power_off_succ);
			fail_power_off.push_back(cmd_power_off_fail);

			ExecuteSerialCommand(cmd_power_off_all, strlen(cmd_power_off_all), out_str[0] );
			ExecuteSerialCommand(cmd_power_off_all, strlen(cmd_power_off_all), out_str[1] );
			ret_val = ExecuteSerialCommand(cmd_power_off, strlen(cmd_power_off), out_str[2] );
		}
		break;

	case eProjectorType_CANNON:
		{
			static const char cmd_power_off[] = "00\"\r";
			succ_power_off.push_back("00\"\r");
			fail_power_off.push_back("00!\r");

			ExecuteSerialCommand(cmd_power_off, strlen(cmd_power_off), out_str[0] );
			ret_val = ExecuteSerialCommand(cmd_power_off, strlen(cmd_power_off), out_str[1] );
		}
		break;

	case eProjectorType_LG_NEW:
		{
			char cmd_power_off_all[] = "~0000 0\r";
			char cmd_power_off[] = "~0100 0\r";
			memcpy(cmd_power_off+1, m_szProjectorID, 2);

			succ_power_off.push_back("S0");
			succ_power_off.push_back("P");
			fail_power_off.push_back("F");

			ExecuteSerialCommand(cmd_power_off_all, strlen(cmd_power_off_all), out_str[0] );
			ExecuteSerialCommand(cmd_power_off_all, strlen(cmd_power_off_all), out_str[1] );
			ret_val = ExecuteSerialCommand(cmd_power_off, strlen(cmd_power_off), out_str[2] );
		}
		break;

	default:
		break;
	}
	ciDEBUG(10,("PowerOff(%d_%s)=%d_%s", m_eProjectorType, szProjectorTypeString[m_eProjectorType], ret_val, szProjectorRVString[ret_val] ));

	//
	if(ret_val == eProjectorRV_SUCCESS)
	{
		ret_val = eProjectorRV_INVALID_RETURN_VALUE;

		for (ciStringList::iterator itr = succ_power_off.begin(); itr != succ_power_off.end(); itr++ )
		{
			if( memcmp (out_str[0].c_str(), (*itr).c_str(), (*itr).size() ) == 0 ||
				memcmp (out_str[1].c_str(), (*itr).c_str(), (*itr).size() ) == 0 ||
				memcmp (out_str[2].c_str(), (*itr).c_str(), (*itr).size() ) == 0 )
			{
				ret_val = eProjectorRV_SUCCESS;
				break;
			}
		}

		if(ret_val != eProjectorRV_SUCCESS)
		{
			for (ciStringList::iterator itr = fail_power_off.begin(); itr != fail_power_off.end(); itr++ )
			{
				if( memcmp (out_str[0].c_str(), (*itr).c_str(), (*itr).size() ) == 0 ||
					memcmp (out_str[1].c_str(), (*itr).c_str(), (*itr).size() ) == 0 ||
					memcmp (out_str[2].c_str(), (*itr).c_str(), (*itr).size() ) == 0 )
				{
					ret_val = eProjectorRV_FAIL_TO_RUN_COMMAND;
					break;
				}
			}
		}
	}

	ciDEBUG(10,("PowerOff Result=%d_%s", ret_val, szProjectorRVString[ret_val] ));
	return ret_val;
}

eProjectorReturnValue CProjectorControl::PowerStatus(eProjectorStatus& status)
{
	eProjectorReturnValue ret_val = eProjectorRV_UNKNOWN;
	status = eProjectorStatus_UNKNOWN;
	ciString out_str = "";
	ciStringList succ_power_on, succ_power_off;

	switch(m_eProjectorType)
	{
	case eProjectorType_SHINDORICO:
		{
			static const char cmd_power_status[] = "#QS\r\n";
			succ_power_on.push_back("#QS3\r\n");
			succ_power_on.push_back("#QS4\r\n");
			succ_power_on.push_back("#QS5\r\n");
			succ_power_on.push_back("#QS6\r\n");
			succ_power_off.push_back("#QS0\r\n");
			succ_power_off.push_back("#QS1\r\n");
			succ_power_off.push_back("#QS2\r\n");
			succ_power_off.push_back("#QS7\r\n");
			succ_power_off.push_back("#QS8\r\n");
			succ_power_off.push_back("#QS9\r\n");

			ret_val = ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );
		}
		break;

	case eProjectorType_LG:
		{
			char cmd_power_status[] = "ka 01 FF\r";
			memcpy(cmd_power_status+3, m_szProjectorID, 2);

			char cmd_power_status_on[] = "a 01 OK01x";
			char cmd_power_status_off[] = "a 01 OK00x";
			memcpy(cmd_power_status_on+2, m_szProjectorID, 2);
			memcpy(cmd_power_status_off+2, m_szProjectorID, 2);
			succ_power_on.push_back(cmd_power_status_on);
			succ_power_off.push_back(cmd_power_status_off);

			ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );
			ret_val = ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );
		}
		break;

	case eProjectorType_CANNON:
		{
			static const char cmd_power_status[] = "00vP\r";
			succ_power_on.push_back("00vP1\r");
			succ_power_off.push_back("00vP0\r");

			ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );
			ret_val = ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );
		}
		break;

	case eProjectorType_LG_NEW:
		{
			char cmd_power_status[] = "~01150 1\r";
			memcpy(cmd_power_status+1, m_szProjectorID, 2);

			succ_power_on.push_back("P");
			succ_power_off.push_back("F");

			ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );
			ret_val = ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );

			const char* c_out_str = out_str.c_str();
			if(_strnicmp(c_out_str, "OK", 2) == 0)
			{
				if(c_out_str[2] == '0')
					succ_power_off.push_back(out_str.c_str());
				else if(c_out_str[2] == '1')
					succ_power_on.push_back(out_str.c_str());
			}
		}
		break;

	default:
		break;
	}
	ciDEBUG(10,("PowerStatus(%d_%s)=%d_%s", m_eProjectorType, szProjectorTypeString[m_eProjectorType], ret_val, szProjectorRVString[ret_val] ));

	//
	if(ret_val == eProjectorRV_SUCCESS)
	{
		ret_val = eProjectorRV_INVALID_RETURN_VALUE;

		for (ciStringList::iterator itr = succ_power_on.begin(); itr != succ_power_on.end(); itr++ )
		{
			if( memcmp (out_str.c_str(), (*itr).c_str(), (*itr).size() ) == 0 )
			{
				ret_val = eProjectorRV_SUCCESS;
				status = eProjectorStatus_PowerOn;
				break;
			}
		}

		if(ret_val != eProjectorRV_SUCCESS)
		{
			for (ciStringList::iterator itr = succ_power_off.begin(); itr != succ_power_off.end(); itr++ )
			{
				if( memcmp (out_str.c_str(), (*itr).c_str(), (*itr).size() ) == 0 )
				{
					ret_val = eProjectorRV_SUCCESS;
					status = eProjectorStatus_PowerOff;
					break;
				}
			}
		}
	}

	ciDEBUG(10,("PowerStatus Result=%d_%s, %d_%s", ret_val, szProjectorRVString[ret_val], status, szProjectorStatusString[status] ));
	return ret_val;
}

eProjectorReturnValue CProjectorControl::GetLampHour(int& hour)
{
	eProjectorReturnValue ret_val = eProjectorRV_UNKNOWN;
	ciString out_str = "";
	hour = -1;

	switch(m_eProjectorType)
	{
	case eProjectorType_SHINDORICO:
		{
			static const char cmd_power_status[] = "#QU\r\n";

			ret_val = ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );

			if(ret_val == eProjectorRV_SUCCESS && strncmp(out_str.c_str(), cmd_power_status, 3) == 0)
			{
				hour = strtol(out_str.c_str()+3, NULL, 16);
			}
		}
		break;

	case eProjectorType_LG:
		break;

	case eProjectorType_CANNON:
		break;

	case eProjectorType_LG_NEW:
		{
			char cmd_power_status[] = "~01150 1\r";
			memcpy(cmd_power_status+1, m_szProjectorID, 2);

			ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );
			ret_val = ExecuteSerialCommand(cmd_power_status, strlen(cmd_power_status), out_str );

			const char* c_out_str = out_str.c_str();
			if(_strnicmp(c_out_str, "OK", 2) == 0)
			{
				char buf[5] = {0};
				strncpy(buf, c_out_str+3, 4);
				hour = atoi(buf);
			}
		}
		break;

	default:
		break;
	}

	if(hour < 0) 
		ciWARN(("GetLampHour FAIL !!!"));

	ciDEBUG(10,("Lamp Hour(%d_%s)=%d_%s,%d", m_eProjectorType, szProjectorTypeString[m_eProjectorType], ret_val, szProjectorRVString[ret_val], hour));

	return ret_val;
}

/*
//shindorico

prefix #(0x23)
suffix CRLF(\r\n, 0x0d 0x0a)

cmd		command		ack		comment
------------------------------------------------------------------------
#P1		power on	P10		fail
					P11		pass
#P0		power off	P00		fail
					P01		pass
#QS		power status
		power on	#QS3	starting
		power on	#QS4	warming up
		power on	#QS5	image can be displayed but still warming up
		power on	#QS6	image can be displayed
		power off	#QS0	not defined
		power off	#QS1	booting
		power off	#QS2	standby
		power off	#QS7	failur
		power off	#QS8	cooling down
		power off	#QS9	fatal error

#QU		used hour of lamp
					QUhhhh	Hhhh hours in hexadecimal format

//lg

suffix CR(\r, 0x0d)

cmd			command			ack				comment
------------------------------------------------------------------------
ka XX 01	power on		a 01 OK01x		ok		( XX=> 01-99=projector id, 00=all)
							a 01 NG01x		fail

ka XX 00	power off		a 01 OK00x		ok		( XX=> 01-99=projector id, 00=all)
							a 01 NG00x		fail

ka XX FF	power status	a 01 OK01x		p_on	( XX=> 01-99=projector id)
							a 01 OK00x		p_off


// cannon

prefix 00(0x30 0x30)
suffix CR(\r, 0x0d)

cmd		command		ack		comment
------------------------------------------------------------------------
00!		power on	00!		pass
00"		power off	00"		pass
00vP	power status
					00vP1	power on
					00vP0	power off


// lg (new model - bx286)

prefix ~(0x7E)
suffix CR(\r, 0x0d)

cmd		command		ack		comment
------------------------------------------------------------------------
~XX00 1	power on	P		pass	( XX=> 01-99=projector id, 00=all)
					F		fail
~XX00 0	power off	P		pass
					F		fail
~XX150 1 			OKabbbbccdddde
		power status		a=0/1 off/on
		lamp hour			bbbb=lamp hour
		source				cc=00/01/02/03/04/05 None/VGA1/VGA2/S-Video/Video/HDMI
		fw.ver.				dddd=FW Version
		display mode		e=0/1/2/3/4/5 None/Presentation/Bright/Movie/RGB/Customer
when status change	INFOn	n=0		stanby
							n=1		warming
							n=2		cooling
							n=3		out of range
							n=4		lamp fail
							n=6		fan lock
							n=7		over temperature
							n=8		lamp hours running out
							n=9		cover open
*/


const char* CProjectorControl::ToString(eProjectorType ePT)
{
	switch(ePT)
	{
	case eProjectorType_SHINDORICO: return "SHINDORICO";
	case eProjectorType_LG:			return "LG";
	case eProjectorType_CANNON:		return "CANNON";
	case eProjectorType_LG_NEW:		return "LG (NewModel)";
	}
	return "UNKNOWN";
}


ProjectorWrapper* 	ProjectorWrapper::_instance = 0; 
ciMutex 	ProjectorWrapper::_instanceLock;

ProjectorWrapper*	
ProjectorWrapper::getInstance() {
	if(!_instance) {
		ciGuard aGuard(_instanceLock);
		if(!_instance) {
			_instance = new ProjectorWrapper;
		}
	}
	return _instance;
}

void	
ProjectorWrapper::clearInstance() {
	if(_instance) {
		ciGuard aGuard(_instanceLock);
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}


ProjectorWrapper::ProjectorWrapper() 
{
    ciDEBUG(10, ("ProjectorWrapper()"));
	_pType=-1;
	_lastState=-2;
}
ProjectorWrapper::~ProjectorWrapper() 
{
    ciDEBUG(10, ("~ProjectorWrapper()"));
}

ciBoolean
ProjectorWrapper::projectorOn(int pType)
{
	eProjectorType eType =  (eProjectorType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = projector.ToString(eType);
	ciDEBUG(10,("projectorOn(%s)", eTypeStr.c_str()));

	eProjectorStatus eStatus = eProjectorStatus_UNKNOWN;
	eProjectorReturnValue  ret_val = projector.PowerStatus(eStatus);
	if(ret_val == eProjectorRV_SUCCESS ) {
		this->_pType = pType;
		if (eStatus == eProjectorStatus_PowerOn){
			ciWARN(("%s BEAM PROJECTOR IS ALREADY POWER ON", eTypeStr.c_str()));
			setLastState(1);
			return ciTrue;
		}
	}
	
	ret_val = projector.PowerOn();
	if(ret_val == eProjectorRV_SUCCESS){
		ciDEBUG(10,("%s BEAM PROJECTOR IS POWER ON SUCCEED", eTypeStr.c_str()));
		setLastState(1);
		return ciTrue;
	}
	ciDEBUG(10,("%s BEAM PROJECTOR IS POWER ON FAILED %d", eTypeStr.c_str(), ret_val));
	return ciFalse;
}

ciBoolean
ProjectorWrapper::projectorOff(int pType)
{
	eProjectorType eType = (eProjectorType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = projector.ToString(eType);
	ciDEBUG(10,("projectorOff(%s)", eTypeStr.c_str()));

	eProjectorStatus eStatus = eProjectorStatus_UNKNOWN;
	eProjectorReturnValue  ret_val = projector.PowerStatus(eStatus);
	if(ret_val == eProjectorRV_SUCCESS ){
		this->_pType = pType;
		if(eStatus == eProjectorStatus_PowerOff){
			ciWARN(("%s BEAM PROJECTOR IS ALREADY POWER OFF", eTypeStr.c_str()));
			setLastState(0);
			return ciTrue;
		}
	}
	
	ret_val = projector.PowerOff();
	if(ret_val == eProjectorRV_SUCCESS){
		ciDEBUG(10,("%s BEAM PROJECTOR IS POWER OFF SUCCEED", eTypeStr.c_str()));
		setLastState(0);
		return ciTrue;
	}
	ciDEBUG(10,("%s BEAM PROJECTOR IS POWER OFF FAILED %d", eTypeStr.c_str(), ret_val));
	return ciFalse;
}

ciBoolean
ProjectorWrapper::projectorOff()
{
	ciGuard aGuard(_lock);

	if(_pType <= 0){
		if( projectorOff(eProjectorType_LG) || 
			projectorOff(eProjectorType_SHINDORICO) ||
			projectorOff(eProjectorType_CANNON) ){
			return ciTrue;
		}
		return ciFalse;
	}

	if( projectorOff(_pType) ){
		return ciTrue;
	}
	return ciFalse;
}
ciBoolean
ProjectorWrapper::projectorOn()
{
	ciGuard aGuard(_lock);

	if(_pType <= 0){
		if( projectorOn(eProjectorType_LG) || 
			projectorOn(eProjectorType_SHINDORICO) ||
			projectorOn(eProjectorType_CANNON) ){
			return ciTrue;
		}
		return ciFalse;
	}
	if( projectorOn(_pType) ){
		return ciTrue;
	}
	return ciFalse;

}

int
ProjectorWrapper::getState(int pType)
{
	eProjectorType eType = (eProjectorType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = projector.ToString(eType);
	ciDEBUG(10,("getState(%s)", eTypeStr.c_str()));

	eProjectorStatus eStatus = eProjectorStatus_UNKNOWN;
	eProjectorReturnValue  ret_val = projector.PowerStatus(eStatus);

	if(ret_val == eProjectorRV_SUCCESS && eStatus == eProjectorStatus_PowerOff){
		ciDEBUG(10,("%s BEAM PROJECTOR IS POWER OFF", eTypeStr.c_str()));
		_pType = pType;
		setLastState(0);
		return 0;
	}
	if(ret_val == eProjectorRV_SUCCESS && eStatus == eProjectorStatus_PowerOn){
		ciDEBUG(10,("%s BEAM PROJECTOR IS POWER ON", eTypeStr.c_str()));
		_pType = pType;
		setLastState(1);
		return 1;
	}
	ciDEBUG(10,("GET STATUS FAILED FROM %s BEAM PROJECTOR", eTypeStr.c_str()));
	setLastState(-1);
	return -1;
}

int
ProjectorWrapper::getState()
{
	ciGuard aGuard(_lock);
	if(_pType <= 0){
		int retval = getState(eProjectorType_LG);
		if(retval >= 0){
			_pType = eProjectorType_LG;
			return retval;
		}
		retval = getState(eProjectorType_SHINDORICO);
		if(retval >= 0){
			_pType = eProjectorType_SHINDORICO;
			return retval;
		}
		retval = getState(eProjectorType_CANNON);
		if(retval >= 0){
			_pType = eProjectorType_CANNON;
			return retval;
		}
		return -1;
	}

	int retval = getState(_pType);
	if(retval >= 0){
		return retval;
	}
	return -1;
}


void
ProjectorWrapper::setLastState(int state)
{
	ciTime now;
	ciDEBUG(10,("setLastState(%d,%s)", state, now.getTimeString().c_str()));

	this->_lastState = state;
	this->_lastStateTime = now;

}

int
ProjectorWrapper::getLampHour(int pType)
{
	eProjectorType eType = (eProjectorType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = projector.ToString(eType);
	ciDEBUG(10,("getLampHour(%s)", eTypeStr.c_str()));

	int hour = -1;
	eProjectorReturnValue  ret_val = projector.GetLampHour(hour);

	if(ret_val == eProjectorRV_SUCCESS && hour >= 0){
		ciDEBUG(10,("%s BEAM PROJECTOR LAMP HOUR = %d", eTypeStr.c_str(), hour));
		_pType = pType;
		return hour;
	}

	ciDEBUG(10,("GET LAMPHOUR FAILED FROM %s BEAM PROJECTOR", eTypeStr.c_str()));
	return -1;
}

int
ProjectorWrapper::getLampHour()
{
	ciGuard aGuard(_lock);
	if(_pType <= 0){
		int retval = getLampHour(eProjectorType_LG);
		if(retval >= 0){
			_pType = eProjectorType_LG;
			return retval;
		}
		retval = getLampHour(eProjectorType_SHINDORICO);
		if(retval >= 0){
			_pType = eProjectorType_SHINDORICO;
			return retval;
		}
		retval = getLampHour(eProjectorType_CANNON);
		if(retval >= 0){
			_pType = eProjectorType_CANNON;
			return retval;
		}
		return -1;
	}

	int retval = getLampHour(_pType);
	if(retval >= 0){
		return retval;
	}
	return -1;
}
