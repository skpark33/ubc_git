#include "stdafx.h"

#include <ci/libDebug/ciDebug.h>
#include <common/libCommon/ubcIni.h>
#include "ProjectorControl.h"

#define		MAX_LINE_LENGTH		1024

ciSET_DEBUG(10, "CProjectorControl");


static const char* szPjTypeString[ePrjType_end] = {
	"ePrjType_UNKNOWN",
	"ePrjType_SHINDORICO",		// 1
	"ePrjType_LG",				// 2
	"ePrjType_CANNON",			// 3
	"ePrjType_LG(NEW MODEL)",	// 4
	"ePrjType_NEC",				// 5
	"ePrjType_SAMSUNG",			// 6
	"ePrjType_EPSON",			// 7
};

static const char* szPrjRetValString[ePrjRetVal_end] = {
	"ePrjRetVal_UNKNOWN",
	"ePrjRetVal_SUCCESS_TO_RUN_COMMAND",
	"ePrjRetVal_FAIL_TO_INIT",
	"ePrjRetVal_FAIL_TO_SEND",
	"ePrjRetVal_FAIL_TO_RECEIVE",
	"ePrjRetVal_FAIL_TO_RUN_COMMAND",
	"ePrjRetVal_INVALID_RETURN_VALUE",
	"ePrjRetVal_NOT_SUPPORT",
};

static const char* szPrjStatusString[ePrjStatus_end] = {
	"ePrjStatus_UNKNOWN",
	"ePrjStatus_POWER_ON",
	"ePrjStatus_POWER_OFF",
};



bool CProjectorControl::AllPowerOn(ePrjRetValList* rv_list)
{
	ciDEBUG(10, ("AllPowerOn()") );

	bool succ_on = false;

	for(int i=ePrjType_start; i<ePrjType_end; i++)
	{
		CProjectorControl pc;
		pc.Init( (ePrjType)i );
		ePrjRetVal ret_val = pc.PowerOn();

		if(rv_list)
		{
			ePrjRetValPair pair;
			pair.type = (ePrjType)i;
			pair.ret_value = ret_val;

			rv_list->push_back(pair);
		}

		if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND) succ_on = true;
	}

	ciDEBUG(10, ("~AllPowerOn()") );
	return succ_on;
}

bool CProjectorControl::AllPowerOff(ePrjRetValList* rv_list)
{
	ciDEBUG(10, ("AllPowerOff()") );

	bool succ_off = false;

	for(int i=ePrjType_start; i<ePrjType_end; i++)
	{
		CProjectorControl pc;
		pc.Init( (ePrjType)i );
		ePrjRetVal ret_val = pc.PowerOff();

		if(rv_list)
		{
			ePrjRetValPair pair;
			pair.type = (ePrjType)i;
			pair.ret_value = ret_val;

			rv_list->push_back(pair);
		}

		if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND) succ_off = true;
	}

	ciDEBUG(10, ("~AllPowerOff()") );
	return succ_off;
}

bool CProjectorControl::AllPowerStatus(ePrjRetValList* rv_list)
{
	ciDEBUG(10, ("AllPowerStatus()") );

	bool succ_on_status = false;

	for(int i=ePrjType_start; i<ePrjType_end; i++)
	{
		CProjectorControl pc;
		pc.Init( (ePrjType)i );
		ePrjStatus status;
		ePrjRetVal ret_val = pc.PowerStatus(status);

		if(rv_list)
		{
			ePrjRetValPair pair;
			pair.type = (ePrjType)i;
			pair.ret_value = ret_val;

			rv_list->push_back(pair);
		}

		if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND && status == ePrjStatus_POWER_ON) succ_on_status = true;
	}

	ciDEBUG(10, ("~AllPowerStatus()") );
	return succ_on_status;
}

ePrjType CProjectorControl::SmartPowerOn()
{
	ciDEBUG(10, ("SmartPowerOn()") );

	ubcConfig aIni("UBCVariables");
	ciLong pre_prj_type = ePrjType_UNKNOWN;
	aIni.get("PROJECTOR", "ProjectorType", pre_prj_type);

	if(pre_prj_type == ePrjType_UNKNOWN){
		aIni.get("ROOT", "MonitorType", pre_prj_type);
	}

	ciDEBUG(10, ("get prev-ProjectorType from UBCVariables.ini = %d_%s", pre_prj_type, ToString((ePrjType)pre_prj_type)) );

	if(ePrjType_start <= pre_prj_type && pre_prj_type < ePrjType_end)
	{
		for(int i=0; i<2; i++) // loop twice because pre-success
		{
			CProjectorControl pc;
			pc.Init( (ePrjType)pre_prj_type );
			ePrjRetVal ret_val = pc.PowerOn();

			if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
			{
				//success
				ciDEBUG(10, ("success prev-ProjectorType(%d_%s) power on", pre_prj_type, ToString((ePrjType)pre_prj_type)) );

				// check status
				for(int j=0; j<6; j++) // loop during 1min
				{
					::Sleep(10 * 1000); // sleep 10sec
					ePrjStatus status = ePrjStatus_UNKNOWN;
					ret_val = pc.PowerStatus(status);

					if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND && status == ePrjStatus_POWER_ON)
					{
						ciDEBUG(10, ("success prev-ProjectorType(%d_%s) power-on status", pre_prj_type, ToString((ePrjType)pre_prj_type)) );
						aIni.set("PROJECTOR", "ProjectorType", pre_prj_type);
						return (ePrjType)pre_prj_type;
					}
				}
			}
			::Sleep(10 * 1000); // sleep 10sec
		}
	}

	ciWARN( ("fail to power on prev-ProjectorType(%d_%s) !!!", pre_prj_type, ToString((ePrjType)pre_prj_type)) );
/*
	// fail or not-setting
	for(ciLong new_prj_type=ePrjType_start; new_prj_type<ePrjType_end; new_prj_type++)
	{
		if(new_prj_type == pre_prj_type) continue; // skip pre_projector_type

		for(int i=0; i<2; i++) // loop twice because pre-success
		{
			CProjectorControl pc;
			pc.Init( (ePrjType)new_prj_type );
			ePrjRetVal ret_val = pc.PowerOn();

			if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
			{
				aIni.set("PROJECTOR", "ProjectorType", new_prj_type);
				ciDEBUG(10, ("success ProjectorType(%d_%s) power on", new_prj_type, ToString((ePrjType)new_prj_type)) );

				// check status
				for(int j=0; j<6; j++) // loop during 1min
				{
					ePrjStatus status = ePrjStatus_UNKNOWN;
					ret_val = pc.PowerStatus(status);

					if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND && status == ePrjStatus_POWER_ON)
					{
						ciDEBUG(10, ("success ProjectorType(%d_%s) power-on status", new_prj_type, ToString((ePrjType)new_prj_type)) );
						return (ePrjType)new_prj_type;
					}
					::Sleep(10 * 1000); // sleep 10sec
				}
			}
		}

		::Sleep(10 * 1000); // sleep 10sec
	}
*/
	ciWARN( ("All fail to power on !!!") );

	return ePrjType_UNKNOWN;
}

ePrjType CProjectorControl::SmartPowerOff()
{
	ciDEBUG(10, ("SmartPowerOff()") );

	ubcConfig aIni("UBCVariables");
	ciLong pre_prj_type = ePrjType_UNKNOWN;
	aIni.get("PROJECTOR", "ProjectorType", pre_prj_type);

	if(pre_prj_type == ePrjType_UNKNOWN){
		aIni.get("ROOT", "MonitorType", pre_prj_type);
	}

	ciDEBUG(10, ("get prev-ProjectorType from UBCVariables.ini = %d_%s", pre_prj_type, ToString((ePrjType)pre_prj_type)) );

	if(ePrjType_start <= pre_prj_type && pre_prj_type < ePrjType_end)
	{
		for(int i=0; i<2; i++) // loop twice because pre-success
		{
			CProjectorControl pc;
			pc.Init( (ePrjType)pre_prj_type );
			ePrjRetVal ret_val = pc.PowerOff();

			if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
			{
				//success
				ciDEBUG(10, ("success prev-ProjectorType(%d_%s) power off", pre_prj_type, ToString((ePrjType)pre_prj_type)) );

				// check status
				for(int j=0; j<6; j++) // loop during 1min
				{
					::Sleep(10 * 1000); // sleep 10sec
					ePrjStatus status = ePrjStatus_UNKNOWN;
					ret_val = pc.PowerStatus(status);

					if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND && status == ePrjStatus_POWER_OFF)
					{
						ciDEBUG(10, ("success prev-ProjectorType(%d_%s) power-off status", pre_prj_type, ToString((ePrjType)pre_prj_type)) );
						aIni.set("PROJECTOR", "ProjectorType", pre_prj_type);
						return (ePrjType)pre_prj_type;
					}
				}
			}
			::Sleep(10 * 1000); // sleep 10sec
		}
	}

	ciWARN( ("fail to power off prev-ProjectorType(%d_%s) !!!", pre_prj_type, ToString((ePrjType)pre_prj_type)) );
/*
	// fail or not-setting
	for(ciLong new_prj_type=ePrjType_start; new_prj_type<ePrjType_end; new_prj_type++)
	{
		if(new_prj_type == pre_prj_type) continue; // skip pre_projector_type

		for(int i=0; i<2; i++) // loop twice because pre-success
		{
			CProjectorControl pc;
			pc.Init( (ePrjType)new_prj_type );
			ePrjRetVal ret_val = pc.PowerOff();

			if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
			{
				aIni.set("PROJECTOR", "ProjectorType", new_prj_type);
				ciDEBUG(10, ("success ProjectorType(%d_%s) power off", new_prj_type, ToString((ePrjType)new_prj_type)) );

				// check status
				for(int j=0; j<6; j++) // loop during 1min
				{
					ePrjStatus status = ePrjStatus_UNKNOWN;
					ret_val = pc.PowerStatus(status);

					if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND && status == ePrjStatus_POWER_OFF)
					{
						ciDEBUG(10, ("success ProjectorType(%d_%s) power-off status", new_prj_type, ToString((ePrjType)new_prj_type)) );
						return (ePrjType)new_prj_type;
					}
					::Sleep(10 * 1000); // sleep 10sec
				}
			}
		}

		::Sleep(10 * 1000); // sleep 10sec
	}
*/
	ciWARN( ("All fail to power off !!!") );
	return ePrjType_UNKNOWN;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
CProjectorControl::CProjectorControl()
:	m_ePrjType ( ePrjType_UNKNOWN )
,	m_nBaudRate ( 0 )
,	m_nDataBit ( 0 )
,	m_nParityBit ( 0 )
,	m_nStopBit ( 0 )
,	m_chEOF ( 0 )
,	m_nTimeout ( 3000 )
{
	strcpy(m_szProjectorID, "01");
	m_nProjectorID = 1;
}

CProjectorControl::~CProjectorControl()
{
	Close();  //skpark 2011.09.07
}

void CProjectorControl::Init(ePrjType ePT, LPCSTR szDevice, int nBaudRate, int nDataBit, int nParityBit, int nStopBit)
{
	ciString basic_serial_port = "";

	m_ePrjType	 = ePT;
	if(szDevice) basic_serial_port = szDevice;
	m_nBaudRate	 = nBaudRate;
	m_nDataBit	 = nDataBit;
	m_nParityBit = nParityBit;
	m_nStopBit	 = nStopBit;

	ciDEBUG(10, ("ProjectorType(%d_%s)", ePT, ToString((ePrjType)ePT)) );
	switch(ePT)
	{
	case ePrjType_SHINDORICO:
		if(m_nBaudRate == CSerial::EBaudUnknown) m_nBaudRate = CSerial::EBaud115200;
		m_chEOF = '\n'; // 0x0A
		m_nTimeout = 2000;
		break;

	case ePrjType_LG:
		m_chEOF = 'x';
		m_nTimeout = 1500;
		break;

	case ePrjType_CANNON:
		m_chEOF = '\r'; // 0x0D
		m_nTimeout = 2000;
		break;

	case ePrjType_LG_NEWMODEL:
		m_chEOF = '\r'; // 0x0D
		m_nTimeout = 1000;
		break;

	case ePrjType_NEC:
		if(m_nBaudRate == CSerial::EBaudUnknown) m_nBaudRate = CSerial::EBaud38400;
		break;

	case ePrjType_SAMSUNG:
		break;

	case ePrjType_EPSON:
		m_chEOF = '\r'; // 0x0D
		m_nTimeout = 1000;
		break;
	}

	if(basic_serial_port.length() == 0)			basic_serial_port = "COM1";
	m_serialPortList.push_back(basic_serial_port);
	if(m_nBaudRate  == CSerial::EBaudUnknown)	m_nBaudRate  = CSerial::EBaud9600;
	if(m_nDataBit   == CSerial::EDataUnknown)	m_nDataBit   = CSerial::EData8;
	if(m_nParityBit == CSerial::EParUnknown)	m_nParityBit = CSerial::EParNone;
	if(m_nStopBit   == CSerial::EStopUnknown)	m_nStopBit   = CSerial::EStop1;

	ciDEBUG(10, ("device(%s)", basic_serial_port.c_str()) );
	ciDEBUG(10, ("baudrate(%d)", m_nBaudRate) );
	ciDEBUG(10, ("databit(%d)", m_nDataBit) );
	ciDEBUG(10, ("paritybit(%d)", m_nParityBit) );
	ciDEBUG(10, ("stopbit(%d)", m_nStopBit) );

	// check additional-serial-port
	ubcConfig aIni("UBCVariables");
	ciString add_serial_port = "";
	aIni.get("PROJECTOR", "AdditionalComPort", add_serial_port);
	ciDEBUG(10, ("AdditionComPortList(%s)", add_serial_port.c_str()) );

	if(add_serial_port.length() > 0)
	{
		char* tokens = (char*)add_serial_port.c_str();
		char* token = strtok( tokens, "," );
		while( token != NULL )
		{
			m_serialPortList.push_back(token);
			ciDEBUG(10, ("AdditionComPort(%s)", token) );
			token = strtok( NULL, "," );
		}
	}

	// create serial-object
	ciStringList::iterator itr = m_serialPortList.begin();
	ciStringList::iterator end = m_serialPortList.end();
	for( ; itr!=end; itr++)
	{
		ciString& port = (ciString&)*itr;
		if(port.length() == 0) continue;

		serialImpl* serial = new serialImpl;
		m_serialList.push_back(serial);
	}
}

void CProjectorControl::SetProjectorID(int id)
{
	m_nProjectorID = id;
	sprintf(m_szProjectorID, "%02d", m_nProjectorID);
}

void CProjectorControl::Close()
{
	ciStringList::iterator port_itr = m_serialPortList.begin();

	serialImplListIter itr = m_serialList.begin();
	serialImplListIter end = m_serialList.end();

	for( ; itr!=end; itr++, port_itr++ )
	{
		ciString& str_port = (ciString&)*port_itr;
		serialImpl* serial = (serialImpl*)*itr;
		if(serial == NULL) continue;

		ciDEBUG(10, ("Destroy Serial-Port(%s)", str_port.c_str()) );
		serial->close();
		delete serial;
	}

	m_serialList.clear();
	m_serialPortList.clear();
}

ciString CProjectorControl::GetBinaryString(LPBYTE szBin, int size)
{
	if(size<0) size = strlen((char*)szBin);
	ciString str_buf = "";
	for(int idx=0; idx<size; idx++)
	{
		if(32 <= szBin[idx] && szBin[idx] < 127)
		{
			str_buf += szBin[idx];
		}
		else
		{
			str_buf += " ";
		}

		char buf[16] = {0};
		sprintf(buf, "(\\%02X)", szBin[idx]);
		str_buf += buf;
	}
	return str_buf;
}

ePrjRetVal CProjectorControl::ExecuteSerialCommand(CSerialValue& cmd, CSerialValue& outVal)
{
	ciDEBUG(10, ("Execute(%s)", GetBinaryString(cmd.m_value, cmd.m_size).c_str()) );

	ciStringList::iterator port_itr = m_serialPortList.begin();

	serialImplListIter begin = m_serialList.begin();
	serialImplListIter end   = m_serialList.end();

	ePrjRetVal ret_val = ePrjRetVal_SUCCESS_TO_RUN_COMMAND;

	for(serialImplListIter itr=begin; itr!=end; itr++, port_itr++)
	{
		ciString& str_port = (ciString&)*port_itr;
		serialImpl* serial = (serialImpl*)*itr;
		if(serial == NULL) continue;

		//
		if( !serial->Init(str_port.c_str(), m_nBaudRate, m_nDataBit, m_nParityBit, m_nStopBit, m_chEOF) )
		{
			ciWARN( ("FAIL_TO_INIT(%s)", str_port.c_str()) );
			serial->close();
			ret_val = ePrjRetVal_FAIL_TO_INIT; // COM1 일때만 리턴값
			continue;
		}

		//
		if( !serial->Send((LPCSTR)cmd.m_value, cmd.m_size) )
		{
			ciWARN( ("FAIL_TO_SEND(%s)", str_port.c_str()) );
			serial->close();
			ret_val = ePrjRetVal_FAIL_TO_SEND; // COM1 일때만 리턴값
			continue;
		}

		//
		BYTE recv_buf[MAX_LINE_LENGTH] = {0};
		DWORD recv_size = 0;
		BOOL read = serial->Read((LPSTR)recv_buf, recv_size, MAX_LINE_LENGTH);
		outVal.Set(recv_buf, recv_size);
		serial->close();

		ciDEBUG(10, ("Read(%s)(%s)", str_port.c_str(), GetBinaryString(recv_buf, recv_size).c_str()) );

		//
		if( /*!read ||*/ recv_size==0 )
		{
			ciWARN( ("FAIL_TO_RECEIVE(%s)", str_port.c_str()) );
			ret_val = ePrjRetVal_FAIL_TO_RECEIVE; // COM1 일때만 리턴값
			continue;
		}
	}

	//
	ciDEBUG(10, ("ExecuteResult=%d_%s", ret_val, ToString((ePrjRetVal)ret_val)) );
	return ret_val;
}

bool CProjectorControl::IsIn(CSerialValue* sVal, CSerialValueList& sValList)
{
	bool find = false;

	CSerialValueIter itr = sValList.begin();
	CSerialValueIter end = sValList.end();
	for ( ; (itr!=end) && (find!=true); itr++ )
	{
		if( (*itr).m_size <= 0) continue;

		for(int i=0; i<3; i++)
		{
			if( memcmp (sVal[i].m_value, (*itr).m_value, (*itr).m_size ) == 0)
			{
				find = true;
				break;
			}
		}
	}
	return find;
}

ePrjRetVal CProjectorControl::PowerOn()
{
	ePrjRetVal ret_val = ePrjRetVal_UNKNOWN;
	CSerialValue cmd_val, succ_val, fail_val;
	CSerialValue out_val[6];
	CSerialValueList succ_power_on_list, fail_power_on_list;

	switch(m_ePrjType)
	{
	case ePrjType_SHINDORICO:
		{
			static const BYTE cmd_power_on[]  = "#P1\r\n";
			static const BYTE succ_power_on[] = "#P11\r\n";
			static const BYTE fail_power_on[] = "#P10\r\n";

			succ_val.Set(succ_power_on);
			succ_power_on_list.push_back(succ_val);

			fail_val.Set(fail_power_on);
			fail_power_on_list.push_back(fail_val);

			cmd_val.Set(cmd_power_on);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i]);
		}
		break;

	case ePrjType_LG:
		{
			static const BYTE cmd_power_on_all[] = "ka 00 01\r";
			static BYTE cmd_power_on[] = "ka 01 01\r";
			memcpy(cmd_power_on+3, m_szProjectorID, 2);

			static BYTE succ_power_on[] = "a 01 OK01x";
			static BYTE fail_power_on[] = "a 01 NG01x";
			memcpy(succ_power_on+2, m_szProjectorID, 2);
			memcpy(fail_power_on+2, m_szProjectorID, 2);

			succ_val.Set(succ_power_on);
			succ_power_on_list.push_back(succ_val);

			fail_val.Set(fail_power_on);
			fail_power_on_list.push_back(fail_val);

			cmd_val.Set(cmd_power_on);
			for(int i=0; i<6 /*&& ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND*/; i++) // 성공해도 전체 전원on명령 실행 => 멀티스크린용
			{
				if(i == 5) cmd_val.Set(cmd_power_on_all); // if fail, try power-on-all in the end
				else if(i > 0)
				{
					char buf[16];
					sprintf(buf, "%02d", m_nProjectorID-1+i);
					memcpy(cmd_power_on+3, buf, 2);
					cmd_val.Set(cmd_power_on); 
				}
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
			}
		}
		break;

	case ePrjType_CANNON:
		{
			static const BYTE cmd_power_on[]  = "00!\r";
			static const BYTE succ_power_on[] = "00!\r";
			static const BYTE fail_power_on[] = "00\"\r";

			succ_val.Set(succ_power_on);
			succ_power_on_list.push_back(succ_val);

			fail_val.Set(fail_power_on);
			fail_power_on_list.push_back(fail_val);

			cmd_val.Set(cmd_power_on);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	case ePrjType_LG_NEWMODEL:
		{
			static const BYTE cmd_power_on_all[] = "~0000 1\r";
			static BYTE cmd_power_on[] = "~0100 1\r";
			memcpy(cmd_power_on+1, m_szProjectorID, 2);

			static BYTE succ_power_on_1[] = "RS232 power on...";
			static BYTE succ_power_on_2[] = " RS232 power on..."; succ_power_on_2[0] = 0x01;
			static BYTE succ_power_on_3[] = "S081";
			static BYTE succ_power_on_4[] = " fffff0S081"; succ_power_on_4[0] = 0xff;
			static BYTE fail_power_on[]   = "F";

			succ_val.Set(succ_power_on_1);
			succ_power_on_list.push_back(succ_val);
			succ_val.Set(succ_power_on_2);
			succ_power_on_list.push_back(succ_val);
			succ_val.Set(succ_power_on_3);
			succ_power_on_list.push_back(succ_val);
			succ_val.Set(succ_power_on_4);
			succ_power_on_list.push_back(succ_val);

			fail_val.Set(fail_power_on);
			fail_power_on_list.push_back(fail_val);

			cmd_val.Set(cmd_power_on);
			for(int i=0; i<6 /*&& ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND*/; i++) // 성공해도 전체 전원off명령 실행 => 멀티스크린용
			{
				if(i == 5) cmd_val.Set(cmd_power_on_all); // if fail, try power-on-all in the end
				else if(i > 0)
				{
					char buf[16];
					sprintf(buf, "%02d", m_nProjectorID-1+i);
					memcpy(cmd_power_on+1, buf, 2);
					cmd_val.Set(cmd_power_on); 
				}
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
			}
		}
		break;

	case ePrjType_NEC:
		{
			static const BYTE cmd_power_on_all[] = {0x02, 0x00, 0x00, 0x00, 0x00, 0x02};
			static BYTE cmd_power_on[]           = {0x02, 0x00, 0x00, 0x00, 0x00, 0x02};
			cmd_power_on[2] = (BYTE)m_nProjectorID;
			cmd_power_on[5] = (BYTE)m_nProjectorID + 0x02; // CheckSum

			static BYTE succ_power_on[] = {0x22, 0x00, 0x00};
			static BYTE fail_power_on[] = {0xA2, 0x00, 0x00};
			succ_power_on[2] = (BYTE)m_nProjectorID;
			fail_power_on[2] = (BYTE)m_nProjectorID;

			succ_val.Set(succ_power_on, sizeof(succ_power_on));
			succ_power_on_list.push_back(succ_val);

			fail_val.Set(fail_power_on, sizeof(fail_power_on));
			fail_power_on_list.push_back(fail_val);

			cmd_val.Set(cmd_power_on, sizeof(cmd_power_on));
			for(int i=0; i<3 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
			{
				if(i== 2) cmd_val.Set(cmd_power_on_all, sizeof(cmd_power_on_all)); // if fail, try power-on-all in the end
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
			}
		}
		break;

	case ePrjType_SAMSUNG:
		{
			static const BYTE cmd_power_on_all[] = {0xAA, 0x11, 0xFE, 0x01, 0x01, 0x11};
			static BYTE cmd_power_on[]           = {0xAA, 0x11, 0xFF/*0x00*/, 0x01, 0x01, 0x12/*0x13*/};
			cmd_power_on[2] = (m_nProjectorID == 0) ? 0xFF : (BYTE)m_nProjectorID;
			cmd_power_on[5] = (m_nProjectorID == 0) ? 0x12 : ((BYTE)m_nProjectorID + 0x13);

			static BYTE succ_power_on[] = {0xAA, 0xFF, 0xFF, 0x03, 0x41/*'A'*/, 0x11, 0x01};
			static BYTE fail_power_on[] = {0xAA, 0xFF, 0xFF, 0x03, 0x4E/*'N'*/, 0x11};
			//
			succ_power_on[2] = (m_nProjectorID == 0) ? 0xFF : (BYTE)m_nProjectorID;
			fail_power_on[2] = (m_nProjectorID == 0) ? 0xFF : (BYTE)m_nProjectorID;

			succ_val.Set(succ_power_on, sizeof(succ_power_on));
			succ_power_on_list.push_back(succ_val);

			fail_val.Set(fail_power_on, sizeof(fail_power_on));
			fail_power_on_list.push_back(fail_val);

			//
			succ_power_on[2] = 0xFE;
			fail_power_on[2] = 0xFE;

			succ_val.Set(succ_power_on, sizeof(succ_power_on));
			succ_power_on_list.push_back(succ_val);

			fail_val.Set(fail_power_on, sizeof(fail_power_on));
			fail_power_on_list.push_back(fail_val);

			//
			cmd_val.Set(cmd_power_on, sizeof(cmd_power_on));
			for(int i=0; i<4 /*&& ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND*/; i++)
			{
				if(i == 3) cmd_val.Set(cmd_power_on_all, sizeof(cmd_power_on_all)); // if fail, try power-on-all in the end
				else if(i == 2)
				{
					cmd_power_on[2] += 1;
					cmd_power_on[5] += 1;
					cmd_val.Set(cmd_power_on, sizeof(cmd_power_on));
				}
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
			}
		}
		break;

	case ePrjType_EPSON:
		{
			static const BYTE cmd_power_on[]  = "PWR ON\r";

			static BYTE succ_power_on_1[] = "PWR=01\r";
			static BYTE succ_power_on_2[] = "PWR=02\r";

			static BYTE fail_power_on_1[]   = "PWR=00\r";
			static BYTE fail_power_on_2[]   = "PWR=03\r";
			static BYTE fail_power_on_3[]   = "PWR=04\r";
			static BYTE fail_power_on_4[]   = "PWR=05\r";

			succ_val.Set(succ_power_on_1);
			succ_power_on_list.push_back(succ_val);
			succ_val.Set(succ_power_on_2);
			succ_power_on_list.push_back(succ_val);

			fail_val.Set(fail_power_on_1);
			fail_power_on_list.push_back(fail_val);
			fail_val.Set(fail_power_on_2);
			fail_power_on_list.push_back(fail_val);
			fail_val.Set(fail_power_on_3);
			fail_power_on_list.push_back(fail_val);
			fail_val.Set(fail_power_on_4);
			fail_power_on_list.push_back(fail_val);

			cmd_val.Set(cmd_power_on);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	default:
		break;
	}

	ciDEBUG(10, ("PowerOn(%d_%s)=%d_%s", m_ePrjType, ToString((ePrjType)m_ePrjType), ret_val, ToString((ePrjRetVal)ret_val)) );

	//
	//if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
	{
		ret_val = ePrjRetVal_INVALID_RETURN_VALUE;

		if( IsIn(out_val, succ_power_on_list) )
			ret_val = ePrjRetVal_SUCCESS_TO_RUN_COMMAND;
		else if( IsIn(out_val, fail_power_on_list) )
			ret_val = ePrjRetVal_FAIL_TO_RUN_COMMAND;
	}

	ciDEBUG(10, ("PowerOn Result=%d_%s", ret_val, ToString((ePrjRetVal)ret_val)) );
	return ret_val;
}

ePrjRetVal CProjectorControl::PowerOff()
{
	ePrjRetVal ret_val = ePrjRetVal_UNKNOWN;
	CSerialValue cmd_val, succ_val, fail_val;
	CSerialValue out_val[6];
	CSerialValueList succ_power_off_list, fail_power_off_list;

	switch(m_ePrjType)
	{
	case ePrjType_SHINDORICO:
		{
			static const BYTE cmd_power_off[]  = "#P0\r\n";
			static const BYTE succ_power_off[] = "#P01\r\n";
			static const BYTE fail_power_off[] = "#P00\r\n";

			succ_val.Set(succ_power_off);
			succ_power_off_list.push_back(succ_val);

			fail_val.Set(fail_power_off);
			fail_power_off_list.push_back(fail_val);

			cmd_val.Set(cmd_power_off);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	case ePrjType_LG:
		{
			static BYTE cmd_power_off_all[] = "ka 00 00\r";
			static BYTE cmd_power_off[] = "ka 01 00\r";
			memcpy(cmd_power_off+3, m_szProjectorID, 2);

			static BYTE succ_power_off[] = "a 01 OK00x";
			static BYTE fail_power_off[] = "a 01 NG00x";
			memcpy(succ_power_off+2, m_szProjectorID, 2);
			memcpy(fail_power_off+2, m_szProjectorID, 2);

			succ_val.Set(succ_power_off);
			succ_power_off_list.push_back(succ_val);

			fail_val.Set(fail_power_off);
			fail_power_off_list.push_back(fail_val);

			cmd_val.Set(cmd_power_off);
			for(int i=0; i<3 /*&& ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND*/; i++) // 성공해도 전체 전원off명령 실행 => 멀티스크린용
			{
				if(i== 2) cmd_val.Set(cmd_power_off_all); // if fail, try power-off-all in the end
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
			}
		}
		break;

	case ePrjType_CANNON:
		{
			static const BYTE cmd_power_off[] = "00\"\r";
			static const BYTE succ_power_off[] = "00\"\r";
			static const BYTE fail_power_off[] = "00!\r";

			succ_val.Set(succ_power_off);
			succ_power_off_list.push_back(succ_val);

			fail_val.Set(fail_power_off);
			fail_power_off_list.push_back(fail_val);

			cmd_val.Set(cmd_power_off);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	case ePrjType_LG_NEWMODEL:
		{
			static const BYTE cmd_power_off_all[] = "~0000 0\r";
			static BYTE cmd_power_off[] = "~0100 0\r";
			memcpy(cmd_power_off+1, m_szProjectorID, 2);

			static const BYTE succ_power_off_1[] = "S0";
			static const BYTE succ_power_off_2[] = "P";
			static const BYTE fail_power_off[]   = "F";

			succ_val.Set(succ_power_off_1);
			succ_power_off_list.push_back(succ_val);
			succ_val.Set(succ_power_off_2);
			succ_power_off_list.push_back(succ_val);

			fail_val.Set(fail_power_off);
			fail_power_off_list.push_back(fail_val);

			cmd_val.Set(cmd_power_off);
			for(int i=0; i<3 /*&& ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND*/; i++)
			{
				if(i== 2) cmd_val.Set(cmd_power_off_all); // if fail, try power-off-all in the end
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
			}
		}
		break;

	case ePrjType_NEC:
		{
			static const BYTE cmd_power_off_all[] = {0x02, 0x01, 0x00, 0x00, 0x00, 0x03};
			static BYTE cmd_power_off[]           = {0x02, 0x01, 0x00, 0x00, 0x00, 0x03};
			cmd_power_off[2] = (BYTE)m_nProjectorID;
			cmd_power_off[5] = (BYTE)m_nProjectorID + 0x03; // CheckSum

			static BYTE succ_power_off[] = {0x22, 0x01, 0x00};
			static BYTE fail_power_off[] = {0xA2, 0x01, 0x00};
			succ_power_off[2] = (BYTE)m_nProjectorID;
			fail_power_off[2] = (BYTE)m_nProjectorID;

			succ_val.Set(succ_power_off, sizeof(succ_power_off));
			succ_power_off_list.push_back(succ_val);

			fail_val.Set(fail_power_off, sizeof(fail_power_off));
			fail_power_off_list.push_back(fail_val);

			cmd_val.Set(cmd_power_off, sizeof(cmd_power_off));
			for(int i=0; i<3 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
			{
				if(i== 2) cmd_val.Set(cmd_power_off_all, sizeof(cmd_power_off_all)); // if fail, try power-off-all in the end
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
			}
		}
		break;

	case ePrjType_SAMSUNG:
		{
			static const BYTE cmd_power_off_all[] = {0xAA, 0x11, 0xFE, 0x01, 0x00, 0x10};
			static BYTE cmd_power_off[]			  = {0xAA, 0x11, 0xFF/*0x00*/, 0x01, 0x00, 0x11/*0x12*/};
			cmd_power_off[2] = (m_nProjectorID == 0) ? 0xFF : (BYTE)m_nProjectorID;
			cmd_power_off[5] = (m_nProjectorID == 0) ? 0x11 : ((BYTE)m_nProjectorID + 0x12);

			static BYTE succ_power_off[] = {0xAA, 0xFF, 0xFF, 0x03, 0x41/*'A'*/, 0x11, 0x00};
			static BYTE fail_power_off[] = {0xAA, 0xFF, 0xFF, 0x03, 0x4E/*'N'*/, 0x11};
			//
			succ_power_off[2] = (m_nProjectorID == 0) ? 0xFF : (BYTE)m_nProjectorID;
			fail_power_off[2] = (m_nProjectorID == 0) ? 0xFF : (BYTE)m_nProjectorID;

			succ_val.Set(succ_power_off, sizeof(succ_power_off));
			succ_power_off_list.push_back(succ_val);

			fail_val.Set(fail_power_off, sizeof(fail_power_off));
			fail_power_off_list.push_back(fail_val);

			//
			succ_power_off[2] = 0xFE;
			fail_power_off[2] = 0xFE;

			succ_val.Set(succ_power_off, sizeof(succ_power_off));
			succ_power_off_list.push_back(succ_val);

			fail_val.Set(fail_power_off, sizeof(fail_power_off));
			fail_power_off_list.push_back(fail_val);

			//
			cmd_val.Set(cmd_power_off, sizeof(cmd_power_off));
			for(int i=0; i<4 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
			{
				if(i == 3) cmd_val.Set(cmd_power_off_all, sizeof(cmd_power_off_all)); // if fail, try power-off-all in the end
				else if(i == 3)
				{
					cmd_power_off[2] += 1;
					cmd_power_off[5] += 1;
					cmd_val.Set(cmd_power_off, sizeof(cmd_power_off));
				}
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
			}
		}
		break;

	case ePrjType_EPSON:
		{
			static const BYTE cmd_power_off[]  = "PWR OFF\r";

			static BYTE succ_power_off_1[]   = "PWR=00\r";
			static BYTE succ_power_off_2[]   = "PWR=03\r";
			static BYTE succ_power_off_3[]   = "PWR=04\r";

			static BYTE fail_power_off_1[] = "PWR=01\r";
			static BYTE fail_power_off_2[] = "PWR=02\r";
			static BYTE fail_power_off_3[] = "PWR=05\r";

			succ_val.Set(succ_power_off_1);
			succ_power_off_list.push_back(succ_val);
			succ_val.Set(succ_power_off_2);
			succ_power_off_list.push_back(succ_val);
			succ_val.Set(succ_power_off_3);
			succ_power_off_list.push_back(succ_val);

			fail_val.Set(fail_power_off_1);
			fail_power_off_list.push_back(fail_val);
			fail_val.Set(fail_power_off_2);
			fail_power_off_list.push_back(fail_val);
			fail_val.Set(fail_power_off_3);
			fail_power_off_list.push_back(fail_val);

			cmd_val.Set(cmd_power_off);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	default:
		break;
	}

	ciDEBUG(10, ("PowerOff(%d_%s)=%d_%s", m_ePrjType, ToString((ePrjType)m_ePrjType), ret_val, ToString((ePrjRetVal)ret_val)) );

	//
	//if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
	{
		ret_val = ePrjRetVal_INVALID_RETURN_VALUE;

		if( IsIn(out_val, succ_power_off_list) )
			ret_val = ePrjRetVal_SUCCESS_TO_RUN_COMMAND;
		else if( IsIn(out_val, fail_power_off_list) )
			ret_val = ePrjRetVal_FAIL_TO_RUN_COMMAND;
	}

	ciDEBUG(10, ("PowerOff Result=%d_%s", ret_val, ToString((ePrjRetVal)ret_val)) );
	return ret_val;
}

ePrjRetVal CProjectorControl::PowerStatus(ePrjStatus& status)
{
	ePrjRetVal ret_val = ePrjRetVal_UNKNOWN;
	CSerialValue cmd_val, on_val, off_val;
	CSerialValue out_val[3];
	CSerialValueList succ_power_on_list, succ_power_off_list;

	switch(m_ePrjType)
	{
	case ePrjType_SHINDORICO:
		{
			static const BYTE cmd_power_status[] = "#QS\r\n";
			static const BYTE succ_power_on_1[]  = "#QS3\r\n";
			static const BYTE succ_power_on_2[]  = "#QS4\r\n";
			static const BYTE succ_power_on_3[]  = "#QS5\r\n";
			static const BYTE succ_power_on_4[]  = "#QS6\r\n";
			static const BYTE succ_power_off_1[] = "#QS0\r\n";
			static const BYTE succ_power_off_2[] = "#QS1\r\n";
			static const BYTE succ_power_off_3[] = "#QS2\r\n";
			static const BYTE succ_power_off_4[] = "#QS7\r\n";
			static const BYTE succ_power_off_5[] = "#QS8\r\n";
			static const BYTE succ_power_off_6[] = "#QS9\r\n";

			on_val.Set(succ_power_on_1);
			succ_power_on_list.push_back(on_val);
			on_val.Set(succ_power_on_2);
			succ_power_on_list.push_back(on_val);
			on_val.Set(succ_power_on_3);
			succ_power_on_list.push_back(on_val);
			on_val.Set(succ_power_on_4);
			succ_power_on_list.push_back(on_val);

			off_val.Set(succ_power_off_1);
			succ_power_off_list.push_back(off_val);
			off_val.Set(succ_power_off_2);
			succ_power_off_list.push_back(off_val);
			off_val.Set(succ_power_off_3);
			succ_power_off_list.push_back(off_val);
			off_val.Set(succ_power_off_4);
			succ_power_off_list.push_back(off_val);
			off_val.Set(succ_power_off_5);
			succ_power_off_list.push_back(off_val);
			off_val.Set(succ_power_off_6);
			succ_power_off_list.push_back(off_val);

			cmd_val.Set(cmd_power_status);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	case ePrjType_LG:
		{
			static BYTE cmd_power_status[] = "ka 01 FF\r";
			memcpy(cmd_power_status+3, m_szProjectorID, 2);

			static BYTE succ_power_on[]  = "a 01 OK01x";
			static BYTE succ_power_off[] = "a 01 OK00x";
			memcpy(succ_power_on+2, m_szProjectorID, 2);
			memcpy(succ_power_off+2, m_szProjectorID, 2);

			on_val.Set(succ_power_on);
			succ_power_on_list.push_back(on_val);

			off_val.Set(succ_power_off);
			succ_power_off_list.push_back(off_val);

			cmd_val.Set(cmd_power_status);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	case ePrjType_CANNON:
		{
			static const BYTE cmd_power_status[] = "00vP\r";
			static const BYTE succ_power_on[]  = "00vP1\r";
			static const BYTE succ_power_off[] = "00vP0\r";

			on_val.Set(succ_power_on);
			succ_power_on_list.push_back(on_val);

			off_val.Set(succ_power_off);
			succ_power_off_list.push_back(off_val);

			cmd_val.Set(cmd_power_status);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	case ePrjType_LG_NEWMODEL:
		{
			static BYTE cmd_power_status[] = "~01150 1\r";
			memcpy(cmd_power_status+1, m_szProjectorID, 2);

			static const BYTE succ_power_on[]  = "P";
			static const BYTE succ_power_off[] = "F";

			on_val.Set(succ_power_on);
			succ_power_on_list.push_back(on_val);

			off_val.Set(succ_power_off);
			succ_power_off_list.push_back(off_val);

			cmd_val.Set(cmd_power_status);
			int i=0;
			for( ; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );

			const BYTE* c_out_str = out_val[i-1].m_value;
			if(_strnicmp((char*)c_out_str, "OK", 2) == 0)
			{
				if(c_out_str[2] == '0')
				{
					off_val.Set(c_out_str);
					succ_power_off_list.push_back(off_val);
				}
				else if(c_out_str[2] == '1')
				{
					off_val.Set(c_out_str);
					succ_power_on_list.push_back(off_val);
				}
			}
		}
		break;

	case ePrjType_NEC:
		{
			static BYTE cmd_power_status[] = {0x00, 0x85, 0x00, 0x00, 0x01, 0x01, 0x87};
			cmd_power_status[2] = (BYTE)m_nProjectorID;
			cmd_power_status[6] = (BYTE)m_nProjectorID + 0x87; // CheckSum

			static BYTE succ_power_on [] = {0x20, 0x85, 0x00};
			static BYTE succ_power_off[] = {0xA0, 0x85, 0x00};
			succ_power_on[2]  = (BYTE)m_nProjectorID;
			succ_power_off[2] = (BYTE)m_nProjectorID;

			on_val.Set(succ_power_on, sizeof(succ_power_on));
			succ_power_on_list.push_back(on_val);

			off_val.Set(succ_power_off, sizeof(succ_power_off));
			succ_power_off_list.push_back(off_val);

			cmd_val.Set(cmd_power_status, sizeof(cmd_power_status));
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	case ePrjType_SAMSUNG:
		{
			static BYTE cmd_power_status_all[]  = {0xAA, 0x11, 0xFE, 0x00, 0x0F};
			static BYTE cmd_power_status[]		= {0xAA, 0x11, 0xFF/*0x00*/, 0x00, 0x10/*0x11*/};
			cmd_power_status[2] = (m_nProjectorID == 0) ? 0xFF : (BYTE)m_nProjectorID;
			cmd_power_status[4] = (m_nProjectorID == 0) ? 0x10 : ((BYTE)m_nProjectorID + 0x11);

			static BYTE succ_power_on [] = {0xAA, 0xFF, 0xFF, 0x03, 0x41/*'A'*/, 0x11, 0x01};
			static BYTE succ_power_off[] = {0xAA, 0xFF, 0xFF, 0x03, 0x41/*'A'*/, 0x11, 0x00};
			//
			succ_power_on[2]  = (m_nProjectorID == 0) ? 0xFF : (BYTE)m_nProjectorID;
			succ_power_off[2] = (m_nProjectorID == 0) ? 0xFF : (BYTE)m_nProjectorID;

			on_val.Set(succ_power_on, sizeof(succ_power_on));
			succ_power_on_list.push_back(on_val);

			off_val.Set(succ_power_off, sizeof(succ_power_off));
			succ_power_off_list.push_back(off_val);

			//
			succ_power_on[2]  = 0xFE;
			succ_power_off[2] = 0xFE;

			on_val.Set(succ_power_on, sizeof(succ_power_on));
			succ_power_on_list.push_back(on_val);

			off_val.Set(succ_power_off, sizeof(succ_power_off));
			succ_power_off_list.push_back(off_val);

			//
			cmd_val.Set(cmd_power_status, sizeof(cmd_power_status));
			for(int i=0; i<3 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
			{
				if(i== 2) cmd_val.Set(cmd_power_status_all, sizeof(cmd_power_status_all)); // if fail, try power-status-all in the end
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
			}
		}
		break;

	case ePrjType_EPSON:
		{
			static const BYTE cmd_power_status[] = "PWR?\r";

			static BYTE succ_power_on_1[] = "PWR=01\r";
			static BYTE succ_power_on_2[] = "PWR=02\r";

			static BYTE succ_power_off_1[]   = "PWR=00\r";
			static BYTE succ_power_off_2[]   = "PWR=03\r";
			static BYTE succ_power_off_3[]   = "PWR=04\r";

			on_val.Set(succ_power_on_1);
			succ_power_on_list.push_back(on_val);
			on_val.Set(succ_power_on_2);
			succ_power_on_list.push_back(on_val);

			off_val.Set(succ_power_off_1);
			succ_power_off_list.push_back(off_val);
			off_val.Set(succ_power_off_2);
			succ_power_off_list.push_back(off_val);
			off_val.Set(succ_power_off_3);
			succ_power_off_list.push_back(off_val);

			cmd_val.Set(cmd_power_status);
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );
		}
		break;

	default:
		break;
	}

	ciDEBUG(10, ("PowerStatus(%d_%s)=%d_%s", m_ePrjType, ToString((ePrjType)m_ePrjType), ret_val, ToString((ePrjRetVal)ret_val)) );

	//
	//if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
	{
		ret_val = ePrjRetVal_INVALID_RETURN_VALUE;

		if( IsIn(out_val, succ_power_on_list) )
		{
			ret_val = ePrjRetVal_SUCCESS_TO_RUN_COMMAND;
			if(m_ePrjType == ePrjType_NEC)
			{
				// NEC만 예외처리
				status = ePrjStatus_POWER_OFF;
				for(int i=0; i<3; i++)
				{
					if(out_val[i].m_value[7] == 1)
					{
						status = ePrjStatus_POWER_ON;
						break;
					}
				}
			}
			else
			{
				status = ePrjStatus_POWER_ON;
			}
		}
		else if( IsIn(out_val, succ_power_off_list) )
		{
			if(m_ePrjType == ePrjType_NEC)
				// NEC만 예외처리
				ret_val = ePrjRetVal_FAIL_TO_RUN_COMMAND;
			else
				ret_val = ePrjRetVal_SUCCESS_TO_RUN_COMMAND;

			status = ePrjStatus_POWER_OFF;
		}
		else
		{
			CSerialValueIter itr = succ_power_off_list.begin();
			CSerialValueIter end = succ_power_off_list.end();
			for ( ; itr!=end; itr++ )
			{
				if( (*itr).m_size <= 0) continue;

				ciDEBUG(10, ("succ_power_off_list(%s)", GetBinaryString((*itr).m_value, (*itr).m_size).c_str()) );
			}
		}
	}

	ciDEBUG(10, ("PowerStatus Result=%d_%s, %d_%s", ret_val, ToString((ePrjRetVal)ret_val), status, ToString((ePrjStatus)status)) );
	return ret_val;
}

ePrjRetVal CProjectorControl::GetLampHour(int& hour)
{
	ePrjRetVal ret_val = ePrjRetVal_NOT_SUPPORT;
	CSerialValue cmd_val;
	CSerialValue out_val[2];
	hour = -1;

	switch(m_ePrjType)
	{
	case ePrjType_SHINDORICO:
		{
			static const BYTE cmd_lamp_hour[] = "#QU\r\n";

			cmd_val.Set(cmd_lamp_hour);
			int i=0;
			for( ; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );

			const BYTE* c_out_str = out_val[i-1].m_value;
			if(strncmp((char*)c_out_str, (char*)cmd_lamp_hour, 3) == 0)
			{
				hour = strtol((char*)(c_out_str+3), NULL, 16);
			}
		}
		break;

	case ePrjType_LG:
		break;

	case ePrjType_CANNON:
		break;

	case ePrjType_LG_NEWMODEL:
		{
			BYTE cmd_lamp_hour[] = "~01150 1\r";
			memcpy(cmd_lamp_hour+1, m_szProjectorID, 2);

			cmd_val.Set(cmd_lamp_hour);
			int i=0;
			for( ; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );

			const BYTE* c_out_str = out_val[i-1].m_value;
			if(_strnicmp((char*)c_out_str, "OK", 2) == 0)
			{
				BYTE buf[5] = {0};
				strncpy((char*)buf, (char*)(c_out_str+3), 4);
				hour = atoi((char*)buf);
			}
		}
		break;

	case ePrjType_NEC:
		{
			static BYTE cmd_lamp_hour[] = {0x03, 0x8C, 0x00, 0x00, 0x00, 0x8F};
			cmd_lamp_hour[2] = (BYTE)m_nProjectorID;
			cmd_lamp_hour[5] = (BYTE)m_nProjectorID + 0x8F; // CheckSum

			cmd_val.Set(cmd_lamp_hour, sizeof(cmd_lamp_hour));
			for(int i=0; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );

			for(int i=0; i<2; i++)
			{
				const BYTE* c_out_str = out_val[i].m_value;
				if((BYTE)c_out_str[0] == 0x23 && (BYTE)c_out_str[1] == 0x8C)
				{
					hour = 0;
					for(int i=8; i>=5; i--)
						hour = hour* 256 + (int)c_out_str[i];
					hour /= 60; // min
					hour /= 60; // hour
					break;
				}
			}
		}
		break;

	case ePrjType_SAMSUNG:
		break;

	case ePrjType_EPSON:
		{
			static const BYTE cmd_lamp_hour[] = "LAMP?\r";

			cmd_val.Set(cmd_lamp_hour);
			int i=0;
			for( ; i<2 && ret_val!=ePrjRetVal_SUCCESS_TO_RUN_COMMAND; i++)
				ret_val = ExecuteSerialCommand(cmd_val, out_val[i] );

			const BYTE* c_out_str = out_val[i-1].m_value;
			if(strncmp((char*)c_out_str, (char*)cmd_lamp_hour, 4) == 0)
			{
				//hour = strtol((char*)(c_out_str+5), NULL, 16);
				hour = atoi((char*)(c_out_str+5));
			}
		}
		break;

	default:
		break;
	}

	if(hour < 0) 
		ciWARN( ("GetLampHour FAIL !!!") );

	ciDEBUG(10, ("Lamp Hour(%d_%s)=%d_%s,%d", m_ePrjType, ToString((ePrjType)m_ePrjType), ret_val, ToString((ePrjRetVal)ret_val), hour) );

	return ret_val;
}

const char* CProjectorControl::ToString(ePrjType ePT)
{
	if( ePrjType_start <= ePT && ePT < ePrjType_end )
	{
		return szPjTypeString[ePT];
	}
	return szPjTypeString[0];
}

const char*	CProjectorControl::ToString(ePrjRetVal ePRV)
{
	if( ePrjRetVal_start <= ePRV && ePRV < ePrjRetVal_end )
	{
		return szPrjRetValString[ePRV];
	}
	return szPrjRetValString[0];
}

const char*	CProjectorControl::ToString(ePrjStatus ePS)
{
	if( ePrjStatus_start <= ePS && ePS < ePrjStatus_end )
	{
		return szPrjStatusString[ePS];
	}
	return szPrjStatusString[0];
}

/*
//shindorico

prefix #(0x23)
suffix CRLF(\r\n, 0x0d 0x0a)

cmd		command		ack		comment
------------------------------------------------------------------------
#P1		power on	P10		fail
					P11		pass
#P0		power off	P00		fail
					P01		pass
#QS		power status
		power on	#QS3	starting
		power on	#QS4	warming up
		power on	#QS5	image can be displayed but still warming up
		power on	#QS6	image can be displayed
		power off	#QS0	not defined
		power off	#QS1	booting
		power off	#QS2	standby
		power off	#QS7	failur
		power off	#QS8	cooling down
		power off	#QS9	fatal error

#QU		used hour of lamp
					QUhhhh	Hhhh hours in hexadecimal format

//lg

suffix CR(\r, 0x0d)

cmd			command			ack				comment
------------------------------------------------------------------------
ka XX 01	power on		a 01 OK01x		ok		( XX=> 01-99=projector id, 00=all)
							a 01 NG01x		fail

ka XX 00	power off		a 01 OK00x		ok		( XX=> 01-99=projector id, 00=all)
							a 01 NG00x		fail

ka XX FF	power status	a 01 OK01x		p_on	( XX=> 01-99=projector id)
							a 01 OK00x		p_off


// cannon

prefix 00(0x30 0x30)
suffix CR(\r, 0x0d)

cmd		command		ack		comment
------------------------------------------------------------------------
00!		power on	00!		pass
00"		power off	00"		pass
00vP	power status
					00vP1	power on
					00vP0	power off


// lg (new model - bx286)

prefix ~(0x7E)
suffix CR(\r, 0x0d)

cmd		command		ack		comment
------------------------------------------------------------------------
~XX00 1	power on	P		pass	( XX=> 01-99=projector id, 00=all)
					F		fail
~XX00 0	power off	P		pass
					F		fail
~XX150 1 			OKabbbbccdddde
		power status		a=0/1 off/on
		lamp hour			bbbb=lamp hour
		source				cc=00/01/02/03/04/05 None/VGA1/VGA2/S-Video/Video/HDMI
		fw.ver.				dddd=FW Version
		display mode		e=0/1/2/3/4/5 None/Presentation/Bright/Movie/RGB/Customer
when status change	INFOn	n=0		stanby
							n=1		warming
							n=2		cooling
							n=3		out of range
							n=4		lamp fail
							n=6		fan lock
							n=7		over temperature
							n=8		lamp hours running out
							n=9		cover open

// samsung
9600/8/None/1/None

prefix (0xAA)
suffix nothing

cmd		command		ack		comment
------------------------------------------------------------------------
 head  cmd   id    len   data  sum
       power
(0xAA)(0x11)(0xFF)(0x01)(0x01)(sum=0x12)	power on (id=0)
(0xAA)(0x11)(0xFE)(0x01)(0x01)(sum=0x11)	power on (all)
(0xAA)(0x11)(0xFF)(0x01)(0x00)(sum=0x11)	power off (id=0)
(0xAA)(0x11)(0xFE)(0x01)(0x00)(sum=0x10)	power off (all)
      status
(0xAA)(0x11)(0xFF)(0x00)(sum=0x10)	power status (id=0)

Ack =>
(0xAA)(0xFF)(0xFF)(0x03)('A')(0x11)(power)(sum)
Nak =>
(0xAA)(0xFF)(0xFF)(0x03)('N')(0x11)(err_code)(sum)


//NEC
38400 bps
(NP600 series, NP610 Series, VT60/VT70/VT80/VT90 series, VT700: 19200bps)
8/No parity/1/Full duplex

ID1 ID2 PID Model Data  sum
02H 00H 00H 00H   00H   01H				power on (all)
=> No return
02H 00H 01H 00H   00H   03H				power on
=> success
22H 00H 01H xxH   00H   CKS
            10H         33H // each 기종
            20H         43H
            40H         63H
            50H         73H
            60H         83H
            70H         93H
            D0H         F3H
            80H         A3H
            90H         B3H
=> fail                 ↓ErrorCode
A2H 00H 01H xxH   02H   DATA01 DATA02 CKS
            10H // each 기종
            20H
            40H
            50H
            60H
            70H
            D0H
            80H
            90H

02H 01H 00H 00H   00H   03H				power off (all)
=> No return
02H 01H 01H 00H   00H   04H				power off
=> success
22H 01H 01H xxH   00H   CKS
            10H         34H // each 기종
            20H         44H
            40H         64H
            50H         74H
            60H         84H
            70H         94H
            D0H         F4H
            80H         A4H
            90H         B4H
=> fail                 ↓ErrorCode
A2H 01H 01H xxH   02H   DATA01 DATA02 CKS // each 기종
            10H // each 기종
            20H
            40H
            50H
            60H
            70H
            D0H
            80H
            90H

00H 85H 01H 00H   01H   01H   CKS			power status
=> success
20H 85H 01H xxH   10H   DATA01 ... DATA16 CKS
            10H // each 기종
            20H
            40H
            50H
            60H
            70H
            D0H
            80H
            90H
DATA03 Projector status
00H : Idling
01H : Power On

=> fail                 ↓ErrorCode
A0H 85H 01H xxH   02H   DATA01 DATA02 CKS
            10H // each 기종
            20H
            40H
            50H
            60H
            70H
            D0H
            80H
            90H

03H 8CH 01H 00H   00H   90H				lamp hour
=> suceess
23H 8CH 01H xxH   10H   DATA01 .. DATA16 CKS
            10H // each 기종
            20H
            40H
            50H
            60H
            70H
            D0H
            80H
            90H
DATA01 .. 04 : Lamp Hour Meter(Normal mode) (second)
=> fail                 ↓ErrorCode
A3H 8CH 01H xxH   02H   DATA01 DATA02 CKS
            10H // each 기종
            20H
            40H
            50H
            60H
            70H
            D0H
            80H
            90H
*/



ProjectorWrapper* 	ProjectorWrapper::_instance = 0; 
ciMutex 	ProjectorWrapper::_instanceLock;

ProjectorWrapper*	
ProjectorWrapper::getInstance() {
	if(!_instance) {
		ciGuard aGuard(_instanceLock);
		if(!_instance) {
			_instance = new ProjectorWrapper;
		}
	}
	return _instance;
}

void	
ProjectorWrapper::clearInstance() {
	if(_instance) {
		ciGuard aGuard(_instanceLock);
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}

ProjectorWrapper::ProjectorWrapper() 
{
    ciDEBUG(10, ("ProjectorWrapper()") );
	_pType=-1;
	_lastState=-2;
}
ProjectorWrapper::~ProjectorWrapper() 
{
    ciDEBUG(10, ("~ProjectorWrapper()") );
}

ciBoolean	
ProjectorWrapper::getProjectorType()
{
	ciDEBUG(10, ("getProjectorType()") );

	ciShort monitorType=0;
	ubcConfig aIni("UBCVariables");
	aIni.get("ROOT","MonitorType", monitorType); 
	ciDEBUG(9, ("[ROOT]MonitorType is %d", monitorType));

	if(monitorType > 0){
		_pType = monitorType;
		return ciTrue;
	}
	return ciFalse;
}

ciBoolean
ProjectorWrapper::projectorOn(int pType)
{
	ePrjType eType =  (ePrjType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = CProjectorControl::ToString(eType);
	ciDEBUG(10, ("projectorOn(%s)", eTypeStr.c_str()) );

	ePrjStatus eStatus = ePrjStatus_UNKNOWN;
	ePrjRetVal  ret_val = projector.PowerStatus(eStatus);
	if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND ) {
		this->_pType = pType;
		if (eStatus == ePrjStatus_POWER_ON){
			ciWARN( ("%s BEAM PROJECTOR IS ALREADY POWER ON", eTypeStr.c_str()) );
			setLastState(1);
			return ciTrue;
		}
	}
	
	ret_val = projector.PowerOn();
	if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND){
		ciDEBUG(10, ("%s BEAM PROJECTOR IS POWER ON SUCCEED", eTypeStr.c_str()) );
		setLastState(1);
		return ciTrue;
	}
	ciDEBUG(10, ("%s BEAM PROJECTOR IS POWER ON FAILED %d", eTypeStr.c_str(), ret_val) );
	return ciFalse;
}

ciBoolean
ProjectorWrapper::projectorOff(int pType)
{
	ePrjType eType = (ePrjType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = CProjectorControl::ToString(eType);
	ciDEBUG(10, ("projectorOff(%s)", eTypeStr.c_str()) );

	ePrjStatus eStatus = ePrjStatus_UNKNOWN;
	ePrjRetVal  ret_val = projector.PowerStatus(eStatus);
	if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND ){
		this->_pType = pType;
		if(eStatus == ePrjStatus_POWER_OFF){
			ciWARN( ("%s BEAM PROJECTOR IS ALREADY POWER OFF", eTypeStr.c_str()) );
			setLastState(0);
			return ciTrue;
		}
	}
	
	ret_val = projector.PowerOff();
	if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND){
		ciDEBUG(10, ("%s BEAM PROJECTOR IS POWER OFF SUCCEED", eTypeStr.c_str()) );
		setLastState(0);
		return ciTrue;
	}
	ciDEBUG(10, ("%s BEAM PROJECTOR IS POWER OFF FAILED %d", eTypeStr.c_str(), ret_val) );
	return ciFalse;
}

ciBoolean
ProjectorWrapper::projectorOff()
{
	ciGuard aGuard(_lock);
	this->getProjectorType(); // skpark 2012.8.20

	ciDEBUG2(1,("projectorOff start"));
	if(_pType <= 0){
		/*
		for(int i=ePrjType_start; i<ePrjType_end; i++)
		{
			if( projectorOff((ePrjType)i) ){
				ciDEBUG2(1,("projectorOff end (succeed)"));
				return ciTrue;
			}
		}
		ciDEBUG2(1,("projectorOff end (fail)"));
		*/
		return ciFalse;
	}

	if( projectorOff(_pType) ){
		ciDEBUG2(1,("projectorOff end (succeed)"));
		return ciTrue;
	}
	ciDEBUG2(1,("projectorOff end (fail)"));
	return ciFalse;
}

ciBoolean
ProjectorWrapper::projectorOn()
{
	ciGuard aGuard(_lock);
	this->getProjectorType(); // skpark 2012.8.20

	ciDEBUG2(1,("projectorOn start"));
	if(_pType <= 0){
		/*
		for(int i=ePrjType_start; i<ePrjType_end; i++)
		{
			if( projectorOn((ePrjType)i) ){
				ciDEBUG2(1,("projectorOn end (succeed)"));
				return ciTrue;
			}
		}
		ciDEBUG2(1,("projectorOn end (fail)"));
		*/
		return ciFalse;
	}
	if( projectorOn(_pType) ){
		ciDEBUG2(1,("projectorOn end (succeed)"));
		return ciTrue;
	}
	ciDEBUG2(1,("projectorOn end (fail)"));
	return ciFalse;
}

int
ProjectorWrapper::getState(int pType)
{
	ePrjType eType = (ePrjType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = CProjectorControl::ToString(eType);
	ciDEBUG(10, ("getState(%s)", eTypeStr.c_str()) );

	ePrjStatus eStatus = ePrjStatus_UNKNOWN;
	ePrjRetVal  ret_val = projector.PowerStatus(eStatus);

	if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND && eStatus == ePrjStatus_POWER_OFF) {
		ciDEBUG(10, ("%s BEAM PROJECTOR IS POWER OFF", eTypeStr.c_str()) );
		_pType = pType;
		setLastState(0);
		return 0;
	}
	if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND && eStatus == ePrjStatus_POWER_ON) {
		ciDEBUG(10, ("%s BEAM PROJECTOR IS POWER ON", eTypeStr.c_str()) );
		_pType = pType;
		setLastState(1);
		return 1;
	}
	ciDEBUG(10, ("GET STATUS FAILED FROM %s BEAM PROJECTOR", eTypeStr.c_str()) );
	setLastState(-1);
	return -1;
}

int
ProjectorWrapper::getState()
{
	ciGuard aGuard(_lock);
	this->getProjectorType(); // skpark 2012.8.20
	
	if(_pType <= 0){
		/*	
		for(int i=ePrjType_start; i<ePrjType_end; i++)
		{
			int retval = getState((ePrjType)i);
			if(retval >= 0){
				_pType = (ePrjType)i;
				return retval;
			}
		}
		*/
		return -1;
	}

	int retval = getState(_pType);
	if(retval >= 0){
		return retval;
	}
	return -1;
}

void
ProjectorWrapper::setLastState(int state)
{
	ciTime now;
	ciDEBUG(10, ("setLastState(%d,%s)", state, now.getTimeString().c_str()) );
	ciGuard aGuard(_stateLock);
	this->_lastState = state;
	this->_lastStateTime = now;
}
int			
ProjectorWrapper::getLastState()		
{ 
	ciGuard aGuard(_stateLock);
	return _lastState; 
}

time_t		
ProjectorWrapper::getLastStateTime()	
{ 
	ciGuard aGuard(_stateLock);
	return _lastStateTime.getTime(); 
}

int
ProjectorWrapper::getLampHour(int pType)
{
	ePrjType eType = (ePrjType)pType;

	CProjectorControl projector;
	projector.Init(eType);

	ciString eTypeStr = CProjectorControl::ToString(eType);
	ciDEBUG(10, ("getLampHour(%s)", eTypeStr.c_str()) );

	int hour = -1;
	ePrjRetVal  ret_val = projector.GetLampHour(hour);

	if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND && hour >= 0){
		ciDEBUG(10, ("%s BEAM PROJECTOR LAMP HOUR = %d", eTypeStr.c_str(), hour) );
		_pType = pType;
		return hour;
	}

	ciDEBUG(10, ("GET LAMPHOUR FAILED FROM %s BEAM PROJECTOR", eTypeStr.c_str()) );
	return -1;
}

int
ProjectorWrapper::getLampHour()
{
	ciGuard aGuard(_lock);
	this->getProjectorType(); // skpark 2012.8.20

	// 신도리코에 한해서만, 값을 구한다.  skpark 2012.8.22
	if(_pType != ePrjType_SHINDORICO){
		return -1;
	}

	if(_pType <= 0){
		for(int i=ePrjType_start; i<ePrjType_end; i++)
		{
			int retval = getLampHour((ePrjType)i);
			if(retval >= 0){
				_pType = (ePrjType)i;
				return retval;
			}
		}
		return -1;
	}

	int retval = getLampHour(_pType);
	if(retval >= 0){
		return retval;
	}
	return -1;
}


