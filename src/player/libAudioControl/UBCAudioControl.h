#ifndef _UBCAudioControl_h_
#define _UBCAudioControl_h_

class UBCAudioControl {
public:	
	UBCAudioControl();
	virtual ~UBCAudioControl();

	BOOL Init();
	BOOL Fini();

	int GetVolume();	
	BOOL SetVolume(int volume);
	BOOL SetVolumeUp();
	BOOL SetVolumeDown();

	BOOL IsMute();
	BOOL SetMute(BOOL mute);
	BOOL ToggleMute();

protected:
	BOOL IsWindows7();

	BOOL _IsWindows7;
	HINSTANCE _hAudioCtl;
};

#endif // _UBCAudioControl_h_
