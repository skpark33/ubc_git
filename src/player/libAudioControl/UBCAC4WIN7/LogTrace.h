//  LogTrace.cpp -- Interface for the CLogTrace class
//  A class to do debug logging

#ifndef __LOGTRACE_H__
#define __LOGTRACE_H__

#include <afxmt.h>
#include <iostream>
#include <fstream>
using namespace std;

#ifdef _UNICODE
    #define tofstream wofstream 
#else 
    #define tofstream ofstream 
#endif // _UNICODE

#define frDEBUG(msg)	CLogTrace::getInstance()->Debug msg
#define frERROR(msg)	CLogTrace::getInstance()->Error msg
#define frWARN(msg)		CLogTrace::getInstance()->Warn msg

#define PTHREAD_SELF()	GetCurrentThreadId()

class CLogTrace
{
// Construction/Destruction
public:
	static CLogTrace*	getInstance() ;
	static void			clearInstance();

	CLogTrace();
	virtual ~CLogTrace();

protected:
	BOOL m_bActive;
	CString m_strFileName;
	BOOL m_bTimeStamp;

// Operations
public:
	void LogOpen(LPCTSTR fileName);
	void LogClose();

	void Debug(LPCTSTR format,...);
	void Error(LPCTSTR format,...);
	void Warn(LPCTSTR format,...);

	CString curTimeStr();
	CString time2Str(time_t tm);

protected:
	tofstream	_log;

	static CLogTrace*	_instance;
	static CCriticalSection	_instanceLock;

// Inlines
public:
	inline void SetActive(BOOL bSet)
	{
		m_bActive = bSet;
	}
	inline CString GetFileName()
	{
		return m_strFileName;
	}
};

#endif // __LOGTRACE_H__</PRE>