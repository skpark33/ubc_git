#include "stdafx.h"
#include "UBCAudioControlWin7.h"

extern "C" {

	// UBCAC4WIN7 Methods

	int UBC_AC_Init()
	{
		frDEBUG((_T("UBC_AC_Init()")));
		return TRUE;
	}

	int UBC_AC_Fini()
	{
		frDEBUG((_T("UBC_AC_Fini()")));
		return TRUE;
	}

	int GetVolume()
	{
		int volume = 0;

		HRESULT hr;
		CoInitialize(NULL);
		IAudioEndpointVolume *endpointVolume = _GetEndpointVista();
		if (endpointVolume) {
			float fVal;
			hr = endpointVolume->GetMasterVolumeLevelScalar(&fVal);
			endpointVolume->Release();
			if (SUCCEEDED(hr)) volume = (int)(fVal * 100.0);
		}
		CoUninitialize();
		frDEBUG((_T("GetVolume(%d)"), volume));
		return volume;
	}

	int SetVolume(LPCTSTR strVolume)
	{
		frDEBUG((_T("SetVolume(%s)"), strVolume));
		HRESULT hr;
		CoInitialize(NULL);
		IAudioEndpointVolume *endpointVolume = _GetEndpointVista();
		if (endpointVolume) {
			int volume = _ttoi(strVolume);
			float fVal = volume / 100.0f;
			hr = endpointVolume->SetMasterVolumeLevelScalar(fVal, NULL);
			endpointVolume->Release();
		}
		CoUninitialize();

		if (!SUCCEEDED(hr)) 
		{
			frERROR((_T("SetVolume[%s] Failed"), strVolume));
			return FALSE;
		}
		return TRUE;
	}

	int SetVolumeUp()
	{
		frDEBUG((_T("SetVolumeUp()")));
		HRESULT hr;
		CoInitialize(NULL);
		IAudioEndpointVolume *endpointVolume = _GetEndpointVista();
		if (endpointVolume) {
			hr = endpointVolume->VolumeStepUp(NULL);
			endpointVolume->Release();
		}
		CoUninitialize();

		if (!SUCCEEDED(hr)) 
		{
			frERROR((_T("SetVolumeUp()")));
			return FALSE;
		}
		return TRUE;
	}

	int SetVolumeDown()
	{
		frDEBUG((_T("SetVolumeDown()")));
		HRESULT hr;
		CoInitialize(NULL);
		IAudioEndpointVolume *endpointVolume = _GetEndpointVista();
		if (endpointVolume) {
			hr = endpointVolume->VolumeStepDown(NULL);
			endpointVolume->Release();
		}
		CoUninitialize();

		if (!SUCCEEDED(hr)) 
		{
			frERROR((_T("SetVolumeDown()")));
			return FALSE;
		}
		return TRUE;
	}

	int IsMute()
	{
		int mute = FALSE;
		HRESULT hr;
		CoInitialize(NULL);
		IAudioEndpointVolume *endpointVolume = _GetEndpointVista();
		if (endpointVolume) {
			BOOL bMute;
			hr = endpointVolume->GetMute(&bMute);
			endpointVolume->Release();
			if (SUCCEEDED(hr)) mute = bMute;
		}
		CoUninitialize();
		frDEBUG((_T("IsMute(%s)"), mute ? _T("MUTE"):_T("NOMUTE")));
		return mute;
	}

	int SetMute(LPCTSTR strMute)
	{
		frDEBUG((_T("SetMute(%s)"), strMute));
		HRESULT hr;
		CoInitialize(NULL);
		IAudioEndpointVolume *endpointVolume = _GetEndpointVista();
		if (endpointVolume) {
			CString strTemp = strMute;
			BOOL mute = FALSE;
			if (strTemp == _T("1") || strTemp == _T("TRUE")) mute = TRUE;
			hr = endpointVolume->SetMute(mute, NULL);
			endpointVolume->Release();
		}
		CoUninitialize();

		if (!SUCCEEDED(hr)) 
		{
			frERROR((_T("SetMute(%s)"), strMute));
			return FALSE;
		}
		return TRUE;
	}

	int ToggleMute()
	{
		frDEBUG((_T("ToggleMute()")));
		if (IsMute()) {
			return SetMute(TEXT("0"));
		} else {
			return SetMute(TEXT("1"));
		}
	}

	IAudioEndpointVolume* _GetEndpointVista()
	{
		HRESULT hr;
		IMMDeviceEnumerator *deviceEnumerator = NULL;
		hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), 
			NULL, 
			CLSCTX_INPROC_SERVER, 
			__uuidof(IMMDeviceEnumerator), 
			(LPVOID *)&deviceEnumerator);
		if (!SUCCEEDED(hr)) 
		{
			frERROR((_T("CoCreateInstance() Failed.")));
			return 0;
		}

		IMMDevice *defaultDevice = NULL;

		hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
		deviceEnumerator->Release();
		deviceEnumerator = NULL;
		if (!SUCCEEDED(hr))
		{
			frERROR((_T("GetDefaultAudioEndpoint() Failed.")));
			return 0;
		}

		IAudioEndpointVolume *endpointVolume = NULL;
		hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume), 
			CLSCTX_INPROC_SERVER, 
			NULL, 
			(LPVOID *)&endpointVolume);
		defaultDevice->Release();
		defaultDevice = NULL; 

		if (!SUCCEEDED(hr)) 
		{
			frERROR((_T("Activate() Failed.")));
			return 0;
		}
		return endpointVolume;
	}

} // extern
