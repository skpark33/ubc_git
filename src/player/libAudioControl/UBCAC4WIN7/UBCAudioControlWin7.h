#pragma once

// Microsoft SDK v7.0 for VIsta & Windows 7
#include <objbase.h>
#include <mmdeviceapi.h>
#include <endpointvolume.h>

extern "C" {

	int UBC_AC_Init();
	int UBC_AC_Fini();

	int GetVolume();	
	int SetVolume(LPCTSTR strVolume);
	int SetVolumeUp();
	int SetVolumeDown();

	int IsMute();
	int SetMute(LPCTSTR strMute);
	int ToggleMute();

	IAudioEndpointVolume* _GetEndpointVista();

} // extern
