///////////////////////////////////////////////////////////////////////
//  LogTrace.cpp -- Implementation of the CLogTrace class


#include "stdafx.h"
#include "LogTrace.h"

//////////////////////////////////////////////////////
//  Construction/Destruction

CCriticalSection CLogTrace::_instanceLock;
CLogTrace* CLogTrace::_instance = 0; 

CLogTrace* CLogTrace::getInstance() {
	if(!_instance) {
		_instanceLock.Lock();
		if(!_instance) {
			_instance = new CLogTrace;
		}
		_instanceLock.Unlock();
	}
	return _instance;
}

void CLogTrace::clearInstance() {
	if(_instance) {
		_instanceLock.Lock();
		if(_instance) {
			delete _instance;
			_instance =0;
		}
		_instanceLock.Unlock();
	}
}

CLogTrace::CLogTrace()
: m_bActive(FALSE), m_bTimeStamp(TRUE), m_strFileName(_T(""))
{
}

CLogTrace::~CLogTrace()
{
}

void CLogTrace::LogOpen(LPCTSTR filename) 
{
	m_strFileName = filename;

	// log file open
	_log.open(m_strFileName, ios_base::out | ios_base::app);
	if( _log.fail() ) {
		_tprintf(_T("%s open failed.\n"), m_strFileName);
		m_bActive=false;
		return ;
	} 
	_tprintf(_T("%s has opened.\n"), m_strFileName);
	_log << "\n--------------------\n"
		<< "LogTrace has opened."
		<< "\n--------------------\n"
		<< endl;
	m_bActive=true;
	return;
}

void CLogTrace::LogClose() 
{
	if(m_bActive) {
		_log << "LogTrace has close." << endl;
		_log.close();
	}
	m_bActive = false;
	_tprintf(_T("log has closed.\n"));	
	return;
}

void CLogTrace::Debug(LPCTSTR pFormat,...) 
{
	if(!m_bActive) return;

	TCHAR aBuf[4096];
	va_list	aVp;
	va_start(aVp, pFormat);
	_vstprintf(aBuf, pFormat, aVp);
	va_end(aVp);

	CString strTime = curTimeStr();
	_log << strTime
		<< _T("\t") 
		<< aBuf 
		<< endl;
}

void CLogTrace::Error(LPCTSTR pFormat,...) 
{
	if(!m_bActive) return;

	TCHAR aBuf[4096];
	va_list	aVp;
	va_start(aVp, pFormat);
	_vstprintf(aBuf, pFormat, aVp);
	va_end(aVp);

	CString strTime = curTimeStr();
	_log << strTime
		<< _T("\tERROR : ") 
		<< aBuf 
		<< endl;
}

void CLogTrace::Warn(LPCTSTR pFormat,...) 
{
	if(!m_bActive) return;

	TCHAR aBuf[4096];
	va_list	aVp;
	va_start(aVp, pFormat);
	_vstprintf(aBuf, pFormat, aVp);
	va_end(aVp);

	CString strTime = curTimeStr();
	_log << strTime
		<< _T("\tWARN : ") 
		<< aBuf 
		<< endl;
}

CString CLogTrace::curTimeStr() {
	time_t the_time;
	time(&the_time);
	return time2Str(the_time);
}

CString CLogTrace::time2Str(time_t tm) {
	TCHAR* str;
	struct tm aTM;
#ifdef WIN32
	struct tm *temp = localtime(&tm);
	if (temp) {
		aTM = *temp;
	} else {
		memset((void*)&aTM, 0x00, sizeof(aTM));
	}
#else
	localtime_r(&tm, &aTM);
#endif

	str = (TCHAR*)malloc(sizeof(TCHAR)*20);
	_stprintf(str,_T("%4d-%02d-%02d %02d:%02d:%02d"),
		aTM.tm_year+1900,aTM.tm_mon+1,aTM.tm_mday,
		aTM.tm_hour,aTM.tm_min,aTM.tm_sec);
	CString strTime = str;
	free(str);
	return strTime;
}
