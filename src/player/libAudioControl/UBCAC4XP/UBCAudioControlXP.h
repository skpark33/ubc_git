#pragma once

extern "C" {

	int UBC_AC_Init();
	int UBC_AC_Fini();

	int GetVolume();	
	int SetVolume(LPCTSTR strVolume);
	int SetVolumeUp();
	int SetVolumeDown();

	int IsMute();
	int SetMute(LPCTSTR strMute);
	int ToggleMute();

} // extern
