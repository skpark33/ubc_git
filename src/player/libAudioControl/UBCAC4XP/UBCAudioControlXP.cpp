#include "stdafx.h"
#include "UBCAudioControlXP.h"

// Microsoft SDK for Windows XP
#include <windows.h>
#include <Mmsystem.h>

//
// Volume Members
//

UINT m_nNumMixers = 0;
HMIXER m_hMixer = NULL;
MIXERCAPS m_mxcaps;

CString m_strDstLineName, m_strVolumeControlName;
unsigned long m_dwMinimum = 0;
unsigned long m_dwMaximum = 0;
unsigned long m_dwVolumeControlID = 0;
unsigned long m_dwMuteControlID = 0;

BOOL amdUninitialize();
BOOL amdInitialize();
BOOL amdGetMasterVolumeControl();
BOOL amdGetMasterVolumeValue(unsigned long &dwVal);
BOOL amdSetMasterVolumeValue(unsigned long dwVal);
BOOL amdGetMasterMuteValue(BOOL &lVal);
BOOL amdSetMasterMuteValue(BOOL lVal);

extern "C" {

	// UBCAC4XP Methods

	int UBC_AC_Init()
	{
		return amdInitialize();
	}

	int UBC_AC_Fini()
	{
		return amdUninitialize();
	}

	int GetVolume()
	{
		unsigned long volume = 0;
		amdGetMasterVolumeValue(volume);
		return (int)volume;
	}

	int SetVolume(LPCTSTR strVolume)
	{
		unsigned long volume = _ttol(strVolume);
		return amdSetMasterVolumeValue(volume);
	}

	int SetVolumeUp()
	{
		int volume = GetVolume();
		volume += 5; // 5% up
		if (volume > 100) volume = 100;
		CString strVolume;
		strVolume.Format(_T("%d"), volume);
		return SetVolume(strVolume);
	}

	int SetVolumeDown()
	{
		int volume = GetVolume();
		volume -= 5; // 5% up
		if (volume < 0) volume = 0;
		CString strVolume;
		strVolume.Format(_T("%d"), volume);
		return SetVolume(strVolume);
	}

	int IsMute()
	{
		BOOL mute = FALSE;
		amdGetMasterMuteValue(mute);
		return mute;
	}

	int SetMute(LPCTSTR strMute)
	{
		CString strTemp = strMute;
		BOOL mute = FALSE;
		//if (strTemp == _T("1") || strTemp.CompareNoCase(_T("TRUE")) != -1) mute = TRUE;
		if (strTemp == _T("1") || strTemp == _T("TRUE")) mute = TRUE;
		return amdSetMasterMuteValue(mute);
	}

	int ToggleMute()
	{
		if (IsMute()) {
			return SetMute(TEXT("0"));
		} else {
			return SetMute(TEXT("1"));
		}
	}

} // extern


BOOL amdInitialize()
{
	if (m_hMixer != NULL) {
		return FALSE;
	}

	// get the number of mixer devices present in the system
	m_nNumMixers = ::mixerGetNumDevs();

	m_hMixer = NULL;
	::ZeroMemory(&m_mxcaps, sizeof(MIXERCAPS));

	m_dwMinimum = 0;
	m_dwMaximum = 0;
	m_dwVolumeControlID = 0;
	m_dwMuteControlID = 0;

	//unsigned long m_dwChannels; // 채널 갯수

	// open the first mixer
	// A "mapper" for audio mixer devices does not currently exist.
	if (m_nNumMixers != 0)
	{
		if (::mixerOpen(&m_hMixer,
						0,
						NULL,
						/*reinterpret_cast<unsigned long>(this->GetSafeHwnd()),*/
						NULL,
						MIXER_OBJECTF_MIXER /*| CALLBACK_WINDOW*/)
			!= MMSYSERR_NOERROR)
		{
			return FALSE;
		}

		// Volume & Mute 정보 초기화
		if (!amdGetMasterVolumeControl()) {
			return FALSE;
		}
		
		if (::mixerGetDevCaps(reinterpret_cast<UINT>(m_hMixer),
							  &m_mxcaps, sizeof(MIXERCAPS))
			!= MMSYSERR_NOERROR)
		{
			return FALSE;
		}
	}

	return TRUE;
}

BOOL amdUninitialize()
{
	BOOL bSucc = TRUE;

	if (m_hMixer != NULL)
	{
		bSucc = (::mixerClose(m_hMixer) == MMSYSERR_NOERROR);
		m_hMixer = NULL;
	}

	return bSucc;
}

BOOL amdGetMasterVolumeControl()
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	// get dwLineID
	MIXERLINE mxl;
	mxl.cbStruct = sizeof(MIXERLINE);
	mxl.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_SPEAKERS;
	if (::mixerGetLineInfo(reinterpret_cast<HMIXEROBJ>(m_hMixer),
						   &mxl,
						   MIXER_OBJECTF_HMIXER |
						   MIXER_GETLINEINFOF_COMPONENTTYPE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}

	// get dwControlID
	MIXERCONTROL mxc;
	MIXERLINECONTROLS mxlc;
	mxlc.cbStruct = sizeof(MIXERLINECONTROLS);
	mxlc.dwLineID = mxl.dwLineID;	
	mxlc.cControls = 1;
	mxlc.cbmxctrl = sizeof(MIXERCONTROL);
	mxlc.pamxctrl = &mxc;

	// Volume 컨트롤 얻기
	mxlc.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
	if (::mixerGetLineControls(reinterpret_cast<HMIXEROBJ>(m_hMixer),
							   &mxlc,
							   MIXER_OBJECTF_HMIXER |
							   MIXER_GETLINECONTROLSF_ONEBYTYPE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}

	// store dwControlID
	m_strDstLineName = mxl.szName;
	m_strVolumeControlName = mxc.szName;
	m_dwMinimum = mxc.Bounds.dwMinimum;
	m_dwMaximum = mxc.Bounds.dwMaximum;
	m_dwVolumeControlID = mxc.dwControlID;

	// Mute 컨트롤 얻기
	mxlc.dwControlType = MIXERCONTROL_CONTROLTYPE_MUTE;
	if(::mixerGetLineControls((HMIXEROBJ)m_hMixer, &mxlc, 
		MIXER_OBJECTF_HMIXER | MIXER_GETLINECONTROLSF_ONEBYTYPE) 
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	m_dwMuteControlID = mxc.dwControlID;

	return TRUE;
}

BOOL amdGetMasterVolumeValue(unsigned long &volume)
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	MIXERCONTROLDETAILS_UNSIGNED mxcdVolume;
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwVolumeControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
	mxcd.paDetails = &mxcdVolume;
	if (::mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_GETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	
	unsigned long dwVal = mxcdVolume.dwValue;
	if(dwVal==0) {
		volume = 0;
	}else{
		volume = (dwVal * 100) / m_dwMaximum;
	}

	return TRUE;
}

BOOL amdSetMasterVolumeValue(unsigned long volume)
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	unsigned long dwVal; // 0 ~ 65536
	dwVal = (m_dwMaximum * volume)/100;

	MIXERCONTROLDETAILS_UNSIGNED mxcdVolume = { dwVal };
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwVolumeControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
	mxcd.paDetails = &mxcdVolume;
	if (::mixerSetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_SETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	return TRUE;
}


BOOL amdGetMasterMuteValue(BOOL &mute)
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	long lVal;

	MIXERCONTROLDETAILS_BOOLEAN mxcdMute;
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwMuteControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
	mxcd.paDetails = &mxcdMute;
	if (::mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_GETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	
	lVal = mxcdMute.fValue;
	if (lVal) mute = TRUE;
	else mute = FALSE;

	return TRUE;
}

BOOL amdSetMasterMuteValue(BOOL mute)
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	long lVal;
	if (mute) lVal = 1;
	else lVal = 0;

	MIXERCONTROLDETAILS_BOOLEAN mxcdMute = { lVal };
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwMuteControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
	mxcd.paDetails = &mxcdMute;
	if (::mixerSetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_SETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	return TRUE;
}
