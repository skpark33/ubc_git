// UBCDeviceWizardView.h : CUBCDeviceWizardView 클래스의 인터페이스
//


#pragma once
#include "afxwin.h"

#include "HoverButton.h"

class CUBCDeviceWizardView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CUBCDeviceWizardView();
	DECLARE_DYNCREATE(CUBCDeviceWizardView)

public:
	enum{ IDD = IDD_UBCDEVICEWIZARD_FORM };

// 특성입니다.
public:
	CUBCDeviceWizardDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CUBCDeviceWizardView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	CHoverButton	m_btnVideo;
	CHoverButton	m_btnCleaning;
	CHoverButton	m_btnNetworkTest;
	CHoverButton	m_btnNetwork;
	CHoverButton	m_btnSound;
	CHoverButton	m_btnMisc;
	CHoverButton	m_btnConfig;
	CHoverButton	m_btnExit;

	CString			GetINIValue(CString strFileName, CString strSection, CString strKey);					///< 지정된 ini 파일의 값을 반환한다.
	LPCTSTR			GetAppPath(void);																		///< 실행프로그램의 경로를 반환

	afx_msg void OnBnClickedButtonVideo();
	afx_msg void OnBnClickedButtonCleaning();
	afx_msg void OnBnClickedButtonNetworkTest();
	afx_msg void OnBnClickedButtonNetwork();
	afx_msg void OnBnClickedButtonSound();
	afx_msg void OnBnClickedButtonMisc();
	afx_msg void OnBnClickedButtonConfig();
	afx_msg void OnBnClickedButtonExit();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

#ifndef _DEBUG  // UBCDeviceWizardView.cpp의 디버그 버전
inline CUBCDeviceWizardDoc* CUBCDeviceWizardView::GetDocument() const
   { return reinterpret_cast<CUBCDeviceWizardDoc*>(m_pDocument); }
#endif

