// NetworkConfigDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCDeviceWizard.h"
#include "NetworkConfigDialog.h"
#include "NetworkTestDialog.h"

#include "CommandRedir.h"

// CNetworkConfigDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNetworkConfigDialog, CDialog)

CNetworkConfigDialog::CNetworkConfigDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CNetworkConfigDialog::IDD, pParent)
	, m_reposControl(this)
{
}

CNetworkConfigDialog::~CNetworkConfigDialog()
{
}

void CNetworkConfigDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//
	DDX_Control(pDX, IDC_STATIC_INTERFACE_NAME, m_staticInterfaceName[0]);
	DDX_Control(pDX, IDC_STATIC_NETWORK_CARD_NAME, m_staticNetworkCardName[0]);

	DDX_Control(pDX, IDC_CHECK_DHCP_IP, m_checkDHCPIP[0]);
	DDX_Control(pDX, IDC_CHECK_STATIC_IP, m_checkStaticIP[0]);

	DDX_Control(pDX, IDC_STATIC_IPADDRESS, m_staticIPAddress[0]);
	DDX_Control(pDX, IDC_STATIC_SUBNET_MASK, m_staticSubnetMask[0]);
	DDX_Control(pDX, IDC_STATIC_DEFAULT_GATEWAY, m_staticDefaultGateway[0]);

	DDX_Control(pDX, IDC_IPADDRESS, m_ipIPAddress[0]);
	DDX_Control(pDX, IDC_SUBNET_MASK, m_ipSubnetMask[0]);
	DDX_Control(pDX, IDC_DEFAULT_GATEWAY, m_ipDefaultGateway[0]);

	DDX_Control(pDX, IDC_CHECK_DHCP_DNS, m_checkDHCPDNS[0]);
	DDX_Control(pDX, IDC_CHECK_STATIC_DNS, m_checkStaticDNS[0]);

	DDX_Control(pDX, IDC_STATIC_MAIN_DNS, m_staticMainDNS[0]);
	DDX_Control(pDX, IDC_STATIC_SUB_DNS, m_staticSubDNS[0]);

	DDX_Control(pDX, IDC_MAIN_DNS, m_ipMainDNS[0]);
	DDX_Control(pDX, IDC_SUB_DNS, m_ipSubDNS[0]);

	//
	DDX_Control(pDX, IDC_STATIC_INTERFACE_NAME2, m_staticInterfaceName[1]);
	DDX_Control(pDX, IDC_STATIC_NETWORK_CARD_NAME2, m_staticNetworkCardName[1]);

	DDX_Control(pDX, IDC_CHECK_DHCP_IP2, m_checkDHCPIP[1]);
	DDX_Control(pDX, IDC_CHECK_STATIC_IP2, m_checkStaticIP[1]);

	DDX_Control(pDX, IDC_STATIC_IPADDRESS2, m_staticIPAddress[1]);
	DDX_Control(pDX, IDC_STATIC_SUBNET_MASK2, m_staticSubnetMask[1]);
	DDX_Control(pDX, IDC_STATIC_DEFAULT_GATEWAY2, m_staticDefaultGateway[1]);

	DDX_Control(pDX, IDC_IPADDRESS2, m_ipIPAddress[1]);
	DDX_Control(pDX, IDC_SUBNET_MASK2, m_ipSubnetMask[1]);
	DDX_Control(pDX, IDC_DEFAULT_GATEWAY2, m_ipDefaultGateway[1]);

	DDX_Control(pDX, IDC_CHECK_DHCP_DNS2, m_checkDHCPDNS[1]);
	DDX_Control(pDX, IDC_CHECK_STATIC_DNS2, m_checkStaticDNS[1]);

	DDX_Control(pDX, IDC_STATIC_MAIN_DNS2, m_staticMainDNS[1]);
	DDX_Control(pDX, IDC_STATIC_SUB_DNS2, m_staticSubDNS[1]);

	DDX_Control(pDX, IDC_MAIN_DNS2, m_ipMainDNS[1]);
	DDX_Control(pDX, IDC_SUB_DNS2, m_ipSubDNS[1]);

	//
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CNetworkConfigDialog, CDialog)
	ON_BN_CLICKED(IDC_CHECK_DHCP_IP, &CNetworkConfigDialog::OnBnClickedCheckDhcpIp)
	ON_BN_CLICKED(IDC_CHECK_STATIC_IP, &CNetworkConfigDialog::OnBnClickedCheckStaticIp)
	ON_BN_CLICKED(IDC_CHECK_DHCP_DNS, &CNetworkConfigDialog::OnBnClickedCheckDhcpDns)
	ON_BN_CLICKED(IDC_CHECK_STATIC_DNS, &CNetworkConfigDialog::OnBnClickedCheckStaticDns)
	ON_BN_CLICKED(IDC_CHECK_DHCP_IP2, &CNetworkConfigDialog::OnBnClickedCheckDhcpIp2)
	ON_BN_CLICKED(IDC_CHECK_STATIC_IP2, &CNetworkConfigDialog::OnBnClickedCheckStaticIp2)
	ON_BN_CLICKED(IDC_CHECK_DHCP_DNS2, &CNetworkConfigDialog::OnBnClickedCheckDhcpDns2)
	ON_BN_CLICKED(IDC_CHECK_STATIC_DNS2, &CNetworkConfigDialog::OnBnClickedCheckStaticDns2)
	ON_BN_CLICKED(IDC_BUTTON_TEST, &CNetworkConfigDialog::OnBnClickedButtonTest)
END_MESSAGE_MAP()


BOOL CNetworkConfigDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_reposControl.AddControl(&m_btnOK, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnCancel, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	GetAdaptersList();
	GetAdaptersInfo(false);

	RefreshInformation();

	if(m_adapterInfoList.GetCount() <= 1)
	{
		CRect rect;
		GetWindowRect(rect);
		rect.right  = rect.left + rect.Width() / 2;
		MoveWindow(rect);
		CenterWindow();
		m_reposControl.Move();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CNetworkConfigDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();

	if(RunCommand())
	{
		//sucess
	}
	else
	{
		//fail
	}
}

// CNetworkConfigDialog 메시지 처리기입니다.

void CNetworkConfigDialog::OnBnClickedCheckDhcpIp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_checkDHCPIP[0].SetCheck(BST_CHECKED);
	m_checkStaticIP[0].SetCheck(BST_UNCHECKED);

	m_staticIPAddress[0].EnableWindow(FALSE);
	m_staticSubnetMask[0].EnableWindow(FALSE);
	m_staticDefaultGateway[0].EnableWindow(FALSE);
	m_ipIPAddress[0].EnableWindow(FALSE);
	m_ipSubnetMask[0].EnableWindow(FALSE);
	m_ipDefaultGateway[0].EnableWindow(FALSE);

	m_checkDHCPDNS[0].EnableWindow(TRUE);
}

void CNetworkConfigDialog::OnBnClickedCheckStaticIp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_checkDHCPIP[0].SetCheck(BST_UNCHECKED);
	m_checkStaticIP[0].SetCheck(BST_CHECKED);

	m_staticIPAddress[0].EnableWindow(TRUE);
	m_staticSubnetMask[0].EnableWindow(TRUE);
	m_staticDefaultGateway[0].EnableWindow(TRUE);
	m_ipIPAddress[0].EnableWindow(TRUE);
	m_ipSubnetMask[0].EnableWindow(TRUE);
	m_ipDefaultGateway[0].EnableWindow(TRUE);

	m_checkDHCPDNS[0].EnableWindow(FALSE);

	m_checkDHCPDNS[0].SetCheck(FALSE);
	m_checkStaticDNS[0].SetCheck(TRUE);

	OnBnClickedCheckStaticDns();
}

void CNetworkConfigDialog::OnBnClickedCheckDhcpDns()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_checkDHCPDNS[0].SetCheck(BST_CHECKED);
	m_checkStaticDNS[0].SetCheck(BST_UNCHECKED);

	m_staticMainDNS[0].EnableWindow(FALSE);
	m_staticSubDNS[0].EnableWindow(FALSE);
	m_ipMainDNS[0].EnableWindow(FALSE);
	m_ipSubDNS[0].EnableWindow(FALSE);
}

void CNetworkConfigDialog::OnBnClickedCheckStaticDns()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_checkDHCPDNS[0].SetCheck(BST_UNCHECKED);
	m_checkStaticDNS[0].SetCheck(BST_CHECKED);

	m_staticMainDNS[0].EnableWindow(TRUE);
	m_staticSubDNS[0].EnableWindow(TRUE);
	m_ipMainDNS[0].EnableWindow(TRUE);
	m_ipSubDNS[0].EnableWindow(TRUE);
}

void CNetworkConfigDialog::OnBnClickedCheckDhcpIp2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_checkDHCPIP[1].SetCheck(BST_CHECKED);
	m_checkStaticIP[1].SetCheck(BST_UNCHECKED);

	m_staticIPAddress[1].EnableWindow(FALSE);
	m_staticSubnetMask[1].EnableWindow(FALSE);
	m_staticDefaultGateway[1].EnableWindow(FALSE);
	m_ipIPAddress[1].EnableWindow(FALSE);
	m_ipSubnetMask[1].EnableWindow(FALSE);
	m_ipDefaultGateway[1].EnableWindow(FALSE);

	m_checkDHCPDNS[1].EnableWindow(TRUE);
}

void CNetworkConfigDialog::OnBnClickedCheckStaticIp2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_checkDHCPIP[1].SetCheck(BST_UNCHECKED);
	m_checkStaticIP[1].SetCheck(BST_CHECKED);

	m_staticIPAddress[1].EnableWindow(TRUE);
	m_staticSubnetMask[1].EnableWindow(TRUE);
	m_staticDefaultGateway[1].EnableWindow(TRUE);
	m_ipIPAddress[1].EnableWindow(TRUE);
	m_ipSubnetMask[1].EnableWindow(TRUE);
	m_ipDefaultGateway[1].EnableWindow(TRUE);

	m_checkDHCPDNS[1].EnableWindow(FALSE);

	m_checkDHCPDNS[1].SetCheck(FALSE);
	m_checkStaticDNS[1].SetCheck(TRUE);

	OnBnClickedCheckStaticDns2();
}

void CNetworkConfigDialog::OnBnClickedCheckDhcpDns2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_checkDHCPDNS[1].SetCheck(BST_CHECKED);
	m_checkStaticDNS[1].SetCheck(BST_UNCHECKED);

	m_staticMainDNS[1].EnableWindow(FALSE);
	m_staticSubDNS[1].EnableWindow(FALSE);
	m_ipMainDNS[1].EnableWindow(FALSE);
	m_ipSubDNS[1].EnableWindow(FALSE);
}

void CNetworkConfigDialog::OnBnClickedCheckStaticDns2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_checkDHCPDNS[1].SetCheck(BST_UNCHECKED);
	m_checkStaticDNS[1].SetCheck(BST_CHECKED);

	m_staticMainDNS[1].EnableWindow(TRUE);
	m_staticSubDNS[1].EnableWindow(TRUE);
	m_ipMainDNS[1].EnableWindow(TRUE);
	m_ipSubDNS[1].EnableWindow(TRUE);
}

void CNetworkConfigDialog::GetAdaptersList()
{
	IP_ADAPTER_INFO adapterInfo[100];
	ULONG ulOutputBufLen = sizeof(adapterInfo);

	DWORD dwRet = ::GetAdaptersInfo(adapterInfo, &ulOutputBufLen);

	switch(dwRet)
	{
	case ERROR_SUCCESS:
		{
			PIP_ADAPTER_INFO ainfo = &(adapterInfo[0]);
			while(ainfo)
			{
				ADAPTER_INFO info;
				info.AdapterName = ainfo->AdapterName;
				info.NetworkCardName = ainfo->Description;

				m_adapterInfoList.Add(info);

				ainfo = ainfo->Next;
			}
		}
		break;

	case ERROR_BUFFER_OVERFLOW:
	case ERROR_INVALID_PARAMETER:
	case ERROR_NO_DATA:
	case ERROR_NOT_SUPPORTED:
		::AfxMessageBox("Can not access network card information !!!", MB_ICONSTOP);
		break;
	}
}

void CNetworkConfigDialog::GetAdaptersInfo(bool bReset)
{
	int count = m_adapterInfoList.GetCount();

	for(int i=0; i<count && i<2; i++)
	{
		ADAPTER_INFO& info = m_adapterInfoList.GetAt(i);

		DWORD type;
		CString strValue;
		DWORD dwValue;

		GetInterfaceName(info.AdapterName, "Name",		type, info.InterfaceName, dwValue);

		GetNetworkCardKey(info.AdapterName, "EnableDHCP",		type, strValue, info.EnableDHCP);
		GetNetworkCardKey(info.AdapterName, "IPAddress",		type, info.IPAddress, dwValue);
		GetNetworkCardKey(info.AdapterName, "DefaultGateway",	type, info.DefaultGateway, dwValue);
		GetNetworkCardKey(info.AdapterName, "SubnetMask",		type, info.SubnetMask, dwValue);
		GetNetworkCardKey(info.AdapterName, "DhcpServer",		type, info.DhcpServer, dwValue);

		CString dns_server;
		GetNetworkCardKey(info.AdapterName, "NameServer",		type, dns_server, dwValue);
		if(dns_server.GetLength() > 0)
		{
			int pos = 0;
			info.NameServer = dns_server.Tokenize(",", pos);
			info.SubNameServer = dns_server.Tokenize(",", pos);
		}
		else
		{
			info.NameServer = "";
			info.SubNameServer = "";
		}
	}
}

bool CNetworkConfigDialog::GetInterfaceName(LPCSTR lpszInterface, LPCTSTR key, DWORD &type, CString &strValue, DWORD &dwValue)
{
	CString sub_key;
	sub_key.Format("SYSTEM\\CurrentControlSet\\Control\\Network\\{4D36E972-E325-11CE-BFC1-08002BE10318}\\%s\\Connection", lpszInterface);

	return GetRegistryKey(sub_key, key, type, strValue, dwValue);
}

bool CNetworkConfigDialog::GetNetworkCardKey(LPCSTR lpszInterface, LPCTSTR key, DWORD &type, CString &strValue, DWORD &dwValue)
{
	CString sub_key = "SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\";
	sub_key.Append(lpszInterface);

	return GetRegistryKey(sub_key, key, type, strValue, dwValue);
}

bool CNetworkConfigDialog::GetRegistryKey(LPCSTR lpszSubKey, LPCTSTR key, DWORD &type, CString &strValue, DWORD &dwValue)
{
	HKEY hKey;
	LONG lRes;

	if(RegOpenKeyEx(
			HKEY_LOCAL_MACHINE, 
			lpszSubKey, 
			0L,
			KEY_READ, 
			&hKey
		) != ERROR_SUCCESS)
	{
		CString msg;
		msg.Format("Error1\r\n\r\n%s=%s", lpszSubKey, key);
		//::AfxMessageBox(msg);
		return false;
	}

	DWORD len = 8192;
	BYTE buf[8192];
	::ZeroMemory(buf, sizeof(buf));

	lRes = RegQueryValueEx(hKey,
			key, 
			0,
			&type,
			buf,
			&len);

	RegCloseKey(hKey);

	if(lRes == ERROR_SUCCESS)
	{
		switch(type)
		{
		case REG_SZ:
		case REG_EXPAND_SZ:
		case REG_MULTI_SZ:
			strValue = buf;
			return true;
			break;

		case REG_DWORD:
			dwValue = *((DWORD*)buf);
			return true;
			break;
		}
	}
	else
	{
		CString msg;
		msg.Format("Error2(%d)%d %s\r\n\r\n%s=%s", lRes, len, buf, lpszSubKey, key);
		//::AfxMessageBox(msg);
	}

	return false;
}

void CNetworkConfigDialog::RefreshInformation()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int i=0;

	for(; i<m_adapterInfoList.GetCount() && i<2; i++)
	{
		m_staticNetworkCardName[i].EnableWindow(TRUE);

		m_checkDHCPIP[i].EnableWindow(TRUE);
		m_checkStaticIP[i].EnableWindow(TRUE);

		m_staticIPAddress[i].EnableWindow(TRUE);
		m_staticSubnetMask[i].EnableWindow(TRUE);
		m_staticDefaultGateway[i].EnableWindow(TRUE);

		m_ipIPAddress[i].EnableWindow(TRUE);
		m_ipSubnetMask[i].EnableWindow(TRUE);
		m_ipDefaultGateway[i].EnableWindow(TRUE);

		m_checkDHCPDNS[i].EnableWindow(TRUE);
		m_checkStaticDNS[i].EnableWindow(TRUE);

		m_staticMainDNS[i].EnableWindow(TRUE);
		m_staticSubDNS[i].EnableWindow(TRUE);

		m_ipMainDNS[i].EnableWindow(TRUE);
		m_ipSubDNS[i].EnableWindow(TRUE);


		ADAPTER_INFO& info = m_adapterInfoList.GetAt(i);

		m_staticInterfaceName[i].SetWindowText(" " + info.InterfaceName);
		m_staticNetworkCardName[i].SetWindowText(" " + info.NetworkCardName);

		if(info.EnableDHCP)
		{
			m_checkDHCPIP[i].SetCheck(TRUE);
			m_checkStaticIP[i].SetCheck(FALSE);
			if(i == 0)
				OnBnClickedCheckDhcpIp();
			else
				OnBnClickedCheckDhcpIp2();
		}
		else
		{
			m_checkDHCPIP[i].SetCheck(FALSE);
			m_checkStaticIP[i].SetCheck(TRUE);
			if(i == 0)
				OnBnClickedCheckStaticIp();
			else
				OnBnClickedCheckStaticIp2();
		}

		m_ipIPAddress[i].SetAddress(AddrToDWORD(info.IPAddress));
		m_ipSubnetMask[i].SetAddress(AddrToDWORD(info.SubnetMask));
		m_ipDefaultGateway[i].SetAddress(AddrToDWORD(info.DefaultGateway));

		if(info.NameServer.GetLength() > 0 || info.EnableDHCP==0 )
		{
			m_checkDHCPDNS[i].SetCheck(FALSE);
			m_checkStaticDNS[i].SetCheck(TRUE);
			if(i == 0)
				OnBnClickedCheckStaticDns();
			else
				OnBnClickedCheckStaticDns2();
		}
		else
		{
			m_checkDHCPDNS[i].SetCheck(TRUE);
			m_checkStaticDNS[i].SetCheck(FALSE);
			if(i == 0)
				OnBnClickedCheckDhcpDns();
			else
				OnBnClickedCheckDhcpDns2();
		}

		m_ipMainDNS[i].SetAddress(AddrToDWORD(info.NameServer));
		m_ipSubDNS[i].SetAddress(AddrToDWORD(info.SubNameServer));
	}

	for(; i<2; i++)
	{
		m_staticNetworkCardName[i].EnableWindow(FALSE);

		m_checkDHCPIP[i].EnableWindow(FALSE);
		m_checkStaticIP[i].EnableWindow(FALSE);

		m_staticIPAddress[i].EnableWindow(FALSE);
		m_staticSubnetMask[i].EnableWindow(FALSE);
		m_staticDefaultGateway[i].EnableWindow(FALSE);

		m_ipIPAddress[i].EnableWindow(FALSE);
		m_ipSubnetMask[i].EnableWindow(FALSE);
		m_ipDefaultGateway[i].EnableWindow(FALSE);

		m_checkDHCPDNS[i].EnableWindow(FALSE);
		m_checkStaticDNS[i].EnableWindow(FALSE);

		m_staticMainDNS[i].EnableWindow(FALSE);
		m_staticSubDNS[i].EnableWindow(FALSE);

		m_ipMainDNS[i].EnableWindow(FALSE);
		m_ipSubDNS[i].EnableWindow(FALSE);
	}

	UpdateWindow();
}

DWORD CNetworkConfigDialog::AddrToDWORD(LPCSTR lpszAddress)
{
	CString strAddr = lpszAddress;

	DWORD addr[4];
	::ZeroMemory(addr, sizeof(addr));

	int count = 0;
	int pos = 0;
	CString token = strAddr.Tokenize(".", pos);
	while(token != "" && count < 4)
	{
		addr[count] = atoi(token);

		token = strAddr.Tokenize(".", pos);
		count++;
	}

	return (addr[0] << 24) | (addr[1] << 16) | (addr[2] << 8) | (addr[3]);
}

CString CNetworkConfigDialog::AddrToString(DWORD dwAddress)
{
	DWORD addr[4];
	::ZeroMemory(addr, sizeof(addr));

	addr[0] = (dwAddress & 0xff000000) >> 24;
	addr[1] = (dwAddress & 0x00ff0000) >> 16;
	addr[2] = (dwAddress & 0x0000ff00) >> 8;
	addr[3] = (dwAddress & 0x000000ff);

	CString strAddr;
	strAddr.Format("%d.%d.%d.%d", addr[0], addr[1], addr[2], addr[3]);

	return strAddr;
}

bool CNetworkConfigDialog::RunCommand()
{
	CString command;

	for(int i=0; i<m_adapterInfoList.GetCount() && i<2; i++)
	{
		ADAPTER_INFO& info = m_adapterInfoList.GetAt(i);

		CString interface_name = info.InterfaceName;

		DWORD ipaddress;
		DWORD subnet_mask;
		DWORD gateway;
		DWORD main_dns;
		DWORD sub_dns;

		m_ipIPAddress[i].GetAddress(ipaddress);
		m_ipSubnetMask[i].GetAddress(subnet_mask);
		m_ipDefaultGateway[i].GetAddress(gateway);
		m_ipMainDNS[i].GetAddress(main_dns);
		m_ipSubDNS[i].GetAddress(sub_dns);

		BeginWaitCursor();

		if(m_checkDHCPIP[i].GetCheck() && info.EnableDHCP == 0)
		{
			command.Format("netsh interface ip set address name=\"%s\" source=dhcp", interface_name);

			CString msg = RunProcess(command);
			if(msg.GetLength() > 10)
			{
				EndWaitCursor();
				if(msg.Find("DGNET.DLL") < 0) {
					::MessageBox(m_hWnd, msg, "Error Message", MB_ICONSTOP);
				}
				GetAdaptersInfo(true);
				RefreshInformation();
				return false;
			}
		}
		else
		{
			if( AddrToDWORD(info.IPAddress) != ipaddress ||
				AddrToDWORD(info.SubnetMask) != subnet_mask ||
				AddrToDWORD(info.DefaultGateway) != subnet_mask	)

			{
				command.Format("netsh interface ip set address name=\"%s\" source=static addr=\"%s\" mask=\"%s\" gateway=\"%s\" gwmetric=0"
					, interface_name
					, AddrToString(ipaddress)
					, AddrToString(subnet_mask)
					, AddrToString(gateway)
				);
			
				CString msg = RunProcess(command);
				if(msg.GetLength() > 10)
				{
					EndWaitCursor();
					if(msg.Find("DGNET.DLL") < 0) {
						::MessageBox(m_hWnd, msg, "Error Message", MB_ICONSTOP);
					}
					GetAdaptersInfo(true);
					RefreshInformation();
					return false;
				}
			}
		}

		if(m_checkDHCPDNS[i].GetCheck() && info.NameServer.GetLength() != 0 )
		{
			command.Format("netsh interface ip set dns name=\"%s\" source=dhcp", interface_name);
		
			CString msg = RunProcess(command);
			if(msg.GetLength() > 10)
			{
				EndWaitCursor();
				if(msg.Find("DGNET.DLL") < 0) {
					::MessageBox(m_hWnd, msg, "Error Message", MB_ICONSTOP);
				}
				GetAdaptersInfo(true);
				RefreshInformation();
				return false;
			}
		}
		else
		{
			if( AddrToDWORD(info.NameServer) != main_dns )
			{
				command.Format("netsh interface ip set dns name=\"%s\" source=static addr=\"%s\""
					, interface_name
					, AddrToString(main_dns)
				);

				CString msg = RunProcess(command);
				if(msg.GetLength() > 10)
				{
					EndWaitCursor();
					if(msg.Find("DGNET.DLL") < 0) {
						::MessageBox(m_hWnd, msg, "Error Message", MB_ICONSTOP);
					}
					GetAdaptersInfo(true);
					RefreshInformation();
					return false;
				}
			}

			if( AddrToDWORD(info.SubNameServer) != sub_dns )
			{
				command.Format("netsh interface ip add dns name=\"%s\" addr=\"%s\""
					, interface_name
					, AddrToString(sub_dns)
				);

				CString msg = RunProcess(command);
				if(msg.GetLength() > 10)
				{
					EndWaitCursor();
					if(msg.Find("DGNET.DLL") < 0) {
						::MessageBox(m_hWnd, msg, "Error Message", MB_ICONSTOP);
					}
					GetAdaptersInfo(true);
					RefreshInformation();
					return false;
				}
			}
		}

		EndWaitCursor();
	}

	::AfxMessageBox("Network setting is changed", MB_ICONINFORMATION);

	return true;
}

CString CNetworkConfigDialog::RunProcess(LPCSTR lpszCommand)
{
/*	FILE* pipe = _popen(lpszCommand, "rt");

	if(pipe)
	{
		CString ret_msg;

		char buf[8192];
		while( !feof(pipe) )
		{
			::ZeroMemory(buf, sizeof(buf));
			if( fgets( buf, 8192, pipe ) != NULL )
				ret_msg.Append(buf);
		}

		_pclose(pipe);

		return ret_msg;
	}

	return "";
*/
	CCommandRedirector redir;

	redir.Open(lpszCommand);
	redir.Close();

	return redir.m_strResult;
}


// 네트워크에서 변경사항만 적용
//

void CNetworkConfigDialog::OnBnClickedButtonTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CNetworkTestDialog dlg;
	dlg.DoModal();
}
