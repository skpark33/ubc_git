// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// UBCDeviceWizard.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Customer Info를 반환한다. \n
/// @return <형: CString> \n
///			<CString: CustomerInfo name> \n
/////////////////////////////////////////////////////////////////////////////////
CString GetCoutomerInfo()
{
	CString strUBCVariables = "";
	strUBCVariables.Format("%sdata\\UBCVariables.ini", GetAppPath());

	char cBuffer[BUF_SIZE] = { 0x00 };

	GetPrivateProfileString("CUSTOMER_INFO", "NAME", "", cBuffer, BUF_SIZE, strUBCVariables);
	return cBuffer;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램이 실행된 경로를 반환한다. \n
/// @return <형: CString> \n
///			<값: 프로그램이 실행된 경로> \n
/////////////////////////////////////////////////////////////////////////////////
CString GetAppPath()
{
	CString	_mainDirectory = _T("");

	TCHAR str[MAX_PATH];
	::ZeroMemory(str, MAX_PATH);
	::GetModuleFileName(NULL, str, MAX_PATH);
	int length = _tcslen(str) - 1;
	while( (length > 0) && (str[length] != _T('\\')) )
		str[length--] = 0;

	_mainDirectory = str;

	return _mainDirectory;
}


