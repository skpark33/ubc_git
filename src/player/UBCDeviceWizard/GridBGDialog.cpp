// GridBGDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCDeviceWizard.h"
#include "GridBGDialog.h"

#include "MemDC.h"

// CGridBGDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CGridBGDialog, CDialog)

CGridBGDialog::CGridBGDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CGridBGDialog::IDD, pParent)
	, m_dlgDisplayConfig(this)
{

}

CGridBGDialog::~CGridBGDialog()
{
}

void CGridBGDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CGridBGDialog, CDialog)
	ON_WM_PAINT()
	ON_MESSAGE(WM_CHANGE_RESOLUTION, OnChangeResolution)
END_MESSAGE_MAP()


// CGridBGDialog 메시지 처리기입니다.

BOOL CGridBGDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	CRect rect(0,0,0,0);
	rect.right += ::GetSystemMetrics(SM_CXSCREEN);
	rect.bottom += ::GetSystemMetrics(SM_CYSCREEN);

	MoveWindow(rect);

	m_dlgDisplayConfig.Create(IDD_DISPLAY_CONFIG, this);
	m_dlgDisplayConfig.CenterWindow();
	m_dlgDisplayConfig.ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CGridBGDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	CMemDC memDC(&dc);

	CRect client_rect;
	GetClientRect(client_rect);

	// 바탕화면
	memDC.FillSolidRect(client_rect, RGB(0,0,0));

	// 간단 해상도 정보
	CRect text_rect = client_rect;
	text_rect.DeflateRect(5,5);
	CString msg;
	memDC.SetTextColor(RGB(255,255,255));
	msg.Format("Width : %d\r\nHeight : %d", client_rect.Width(), client_rect.Height());
	memDC.DrawText(msg, text_rect, DT_MODIFYSTRING | DT_LEFT | DT_TOP);

	// 최외곽  테두리
	CRect rect = client_rect;
//	ResizeByRatio(rect);
//	rect.OffsetRect(m_nPosX, m_nPosY);
	memDC.Draw3dRect(rect, RGB(255,255,255), RGB(255,255,255));

	for(int i=1; i<4; i++)
	{
		rect = client_rect;
		rect.right *= i;
		rect.right /= 4;

//		ResizeByRatio(rect);
//		rect.OffsetRect(m_nPosX, m_nPosY);
		memDC.Draw3dRect(rect, RGB(255,255,255), RGB(255,255,255));

		rect.left = rect.right - 50;
		rect.right += 50;
		rect.DeflateRect(0, 10);

		CString text;
		text.Format("%d/4", i);

		memDC.SetTextColor(RGB(255,255,255));
		memDC.SetBkColor(RGB(0,0,0));
		memDC.DrawText(text, rect, DT_TOP | DT_CENTER | DT_SINGLELINE);
		memDC.DrawText(text, rect, DT_BOTTOM | DT_CENTER | DT_SINGLELINE);
	}

	for(int i=1; i<4; i++)
	{
		rect = client_rect;
		rect.bottom *= i;
		rect.bottom /= 4;

//		ResizeByRatio(rect);
//		rect.OffsetRect(m_nPosX, m_nPosY);
		memDC.Draw3dRect(rect, RGB(255,255,255), RGB(255,255,255));

		rect.top = rect.bottom - 50;
		rect.bottom += 50;
		rect.DeflateRect(10, 0);

		CString text;
		text.Format("%d/4", i);

		memDC.SetTextColor(RGB(255,255,255));
		memDC.SetBkColor(RGB(0,0,0));
		memDC.DrawText(text, rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		memDC.DrawText(text, rect, DT_RIGHT | DT_VCENTER | DT_SINGLELINE);

	}
}

LRESULT CGridBGDialog::OnChangeResolution(WPARAM wParam, LPARAM lParam)
{
	CRect rect(0,0,0,0);
	rect.right += ::GetSystemMetrics(SM_CXSCREEN);
	rect.bottom += ::GetSystemMetrics(SM_CYSCREEN);

	MoveWindow(rect);

	Invalidate(FALSE);

	return 0;
}
