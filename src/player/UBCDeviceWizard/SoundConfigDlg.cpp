// SoundConfig.cpp : implementation file
//

#include "stdafx.h"
#include "UBCDeviceWizard.h"
#include "SoundConfigDlg.h"
#include "common\libScratch\scratchUtil.h"

// CSoundConfigDlg dialog

IMPLEMENT_DYNAMIC(CSoundConfigDlg, CDialog)

CSoundConfigDlg::CSoundConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSoundConfigDlg::IDD, pParent)
{
	m_hMixer = NULL;
	m_strDstLineName = _T("");
	m_strVolumeControlName = _T("");
	m_dwMinimum = 0;
	m_dwMaximum = 0;
	m_dwVolumeControlID = 0;
	m_dwMuteControlID = 0;
}

CSoundConfigDlg::~CSoundConfigDlg()
{
}

void CSoundConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RADIO_DIS_A, m_rdoDisA);
	DDX_Control(pDX, IDC_RADIO_DIS_B, m_rdoDisB);
	DDX_Control(pDX, IDC_CHECK_MUTE, m_chkMute);
	DDX_Control(pDX, IDC_SLIDER_VOLUME, m_sldVolume);
	DDX_Control(pDX, IDC_STATIC_VOLUME, m_sttVolume);
}


BEGIN_MESSAGE_MAP(CSoundConfigDlg, CDialog)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CSoundConfigDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHECK_MUTE, &CSoundConfigDlg::OnBnClickedCheckMute)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
END_MESSAGE_MAP()


// CSoundConfigDlg message handlers

BOOL CSoundConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitSound();

	std::string strDisMute = "";
	scratchUtil::getInstance()->displaySound(strDisMute);

	if(strDisMute.length() > 0 && strDisMute[0] == '1')
		m_rdoDisA.SetCheck(TRUE);
	else
		m_rdoDisA.SetCheck(FALSE);

	if(strDisMute.length() > 1 && strDisMute[1] == '1')
		m_rdoDisB.SetCheck(TRUE);
	else
		m_rdoDisB.SetCheck(FALSE);
	
	int nDisCnt = scratchUtil::getInstance()->getMonitorCount();
	if(nDisCnt < 2){
		m_rdoDisB.SetCheck(FALSE);
		m_rdoDisB.EnableWindow(FALSE);
	}

	LONG vol = 0;
	GetVolumeValue(vol);
	m_sldVolume.SetRange(0, 100);
	m_sldVolume.SetPos(vol);

	CString szTemp;
	szTemp.Format(_T("Volume : %d"), vol);
	m_sttVolume.SetWindowText(szTemp);

	BOOL bMute = FALSE;
	GetMuteValue(bMute);
	m_chkMute.SetCheck(bMute);
	if(bMute)
		m_sldVolume.EnableWindow(FALSE);
	else
		m_sldVolume.EnableWindow(TRUE);

	m_bMute = bMute;
	m_bDisA = m_rdoDisA.GetCheck();
	m_bDisB = m_rdoDisB.GetCheck();
	m_lVolume = vol;

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSoundConfigDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default
	CDialog::OnClose();
}

void CSoundConfigDlg::OnDestroy()
{
	CDialog::OnDestroy();
	// TODO: Add your message handler code here
	UninitSound();
}

BOOL CSoundConfigDlg::PreTranslateMessage(MSG* pMsg)
{
	return CDialog::PreTranslateMessage(pMsg);
}

void CSoundConfigDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if(pScrollBar != NULL && pScrollBar->m_hWnd == m_sldVolume.m_hWnd){
		CString szTemp;
		szTemp.Format(_T("Volume : %d"), m_sldVolume.GetPos());
		m_sttVolume.SetWindowText(szTemp);
	}

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CSoundConfigDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if(pScrollBar != NULL && pScrollBar->m_hWnd == m_sldVolume.m_hWnd){
		CString szTemp;
		szTemp.Format(_T("Volume : %d"), m_sldVolume.GetPos());
		m_sttVolume.SetWindowText(szTemp);
	}

	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}

BOOL CSoundConfigDlg::InitSound()
{
	if (m_hMixer != NULL) {
		return ciFalse;
	}

	// get the number of mixer devices present in the system
	int nNumMixers = ::mixerGetNumDevs();
	MIXERCAPS mxcaps;

	m_hMixer = NULL;
	::ZeroMemory(&mxcaps, sizeof(MIXERCAPS));

	m_strDstLineName.Empty();
	m_strVolumeControlName.Empty();
	m_dwMinimum = 0;
	m_dwMaximum = 0;
	m_dwVolumeControlID = 0;
	m_dwMuteControlID = 0;

	//ciULong m_dwChannels; // 채널 갯수

	// open the first mixer
	// A "mapper" for audio mixer devices does not currently exist.
	if (nNumMixers != 0)
	{
		if (::mixerOpen(&m_hMixer,
						0,
						NULL,
						/*reinterpret_cast<ciULong>(this->GetSafeHwnd()),*/
						NULL,
						MIXER_OBJECTF_MIXER /*| CALLBACK_WINDOW*/)
			!= MMSYSERR_NOERROR)
		{
			return FALSE;
		}

		// Volume & Mute 정보 초기화
		if (!InitVolumeControl()) {
			return FALSE;
		}
		
		if (::mixerGetDevCaps(reinterpret_cast<UINT>(m_hMixer),
							  &mxcaps, sizeof(MIXERCAPS))
			!= MMSYSERR_NOERROR)
		{
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CSoundConfigDlg::UninitSound()
{
	BOOL bSucc = TRUE;

	if (m_hMixer != NULL)
	{
		bSucc = (::mixerClose(m_hMixer) == MMSYSERR_NOERROR);
		m_hMixer = NULL;
	}

	return bSucc;
}


BOOL CSoundConfigDlg::InitVolumeControl()
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	// get dwLineID
	MIXERLINE mxl;
	mxl.cbStruct = sizeof(MIXERLINE);
	mxl.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_SPEAKERS;
	if (::mixerGetLineInfo(reinterpret_cast<HMIXEROBJ>(m_hMixer),
						   &mxl,
						   MIXER_OBJECTF_HMIXER |
						   MIXER_GETLINEINFOF_COMPONENTTYPE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}

	// get dwControlID
	MIXERCONTROL mxc;
	MIXERLINECONTROLS mxlc;
	mxlc.cbStruct = sizeof(MIXERLINECONTROLS);
	mxlc.dwLineID = mxl.dwLineID;	
	mxlc.cControls = 1;
	mxlc.cbmxctrl = sizeof(MIXERCONTROL);
	mxlc.pamxctrl = &mxc;

	// Volume 컨트롤 얻기
	mxlc.dwControlType = MIXERCONTROL_CONTROLTYPE_VOLUME;
	if (::mixerGetLineControls(reinterpret_cast<HMIXEROBJ>(m_hMixer),
							   &mxlc,
							   MIXER_OBJECTF_HMIXER |
							   MIXER_GETLINECONTROLSF_ONEBYTYPE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}

	// store dwControlID
	m_strDstLineName = mxl.szName;
	m_strVolumeControlName = mxc.szName;
	m_dwMinimum = mxc.Bounds.dwMinimum;
	m_dwMaximum = mxc.Bounds.dwMaximum;
	m_dwVolumeControlID = mxc.dwControlID;

	// Mute 컨트롤 얻기
	mxlc.dwControlType = MIXERCONTROL_CONTROLTYPE_MUTE;
	if(::mixerGetLineControls((HMIXEROBJ)m_hMixer, &mxlc, 
		MIXER_OBJECTF_HMIXER | MIXER_GETLINECONTROLSF_ONEBYTYPE) 
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	m_dwMuteControlID = mxc.dwControlID;

	return TRUE;
}

BOOL CSoundConfigDlg::GetVolumeValue(LONG &volume)
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	MIXERCONTROLDETAILS_UNSIGNED mxcdVolume;
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwVolumeControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
	mxcd.paDetails = &mxcdVolume;
	if (::mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_GETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	
	ciULong dwVal = mxcdVolume.dwValue;
	volume = (dwVal * 100) / m_dwMaximum;

	return TRUE;
}

BOOL CSoundConfigDlg::SetVolumeValue(LONG volume)
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	ciULong dwVal; // 0 ~ 65536
	dwVal = (m_dwMaximum * volume)/100;

	MIXERCONTROLDETAILS_UNSIGNED mxcdVolume = { dwVal };
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwVolumeControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
	mxcd.paDetails = &mxcdVolume;
	if (::mixerSetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_SETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	return TRUE;
}


BOOL CSoundConfigDlg::GetMuteValue(BOOL &mute)
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	LONG lVal;

	MIXERCONTROLDETAILS_BOOLEAN mxcdMute;
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwMuteControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
	mxcd.paDetails = &mxcdMute;
	if (::mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_GETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	
	lVal = mxcdMute.fValue;
	if (lVal) mute = TRUE;
	else mute = FALSE;

	return TRUE;
}

BOOL CSoundConfigDlg::SetMuteValue(BOOL mute)
{
	if (m_hMixer == NULL)
	{
		return FALSE;
	}

	LONG lVal;
	if (mute) lVal = 1;
	else lVal = 0;

	MIXERCONTROLDETAILS_BOOLEAN mxcdMute = { lVal };
	MIXERCONTROLDETAILS mxcd;
	mxcd.cbStruct = sizeof(MIXERCONTROLDETAILS);
	mxcd.dwControlID = m_dwMuteControlID;
	mxcd.cChannels = 1;
	mxcd.cMultipleItems = 0;
	mxcd.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
	mxcd.paDetails = &mxcdMute;
	if (::mixerSetControlDetails(reinterpret_cast<HMIXEROBJ>(m_hMixer),
								 &mxcd,
								 MIXER_OBJECTF_HMIXER |
								 MIXER_SETCONTROLDETAILSF_VALUE)
		!= MMSYSERR_NOERROR)
	{
		return FALSE;
	}
	
	return TRUE;
}
void CSoundConfigDlg::OnBnClickedCheckMute()
{
	if(m_chkMute.GetCheck())
		m_sldVolume.EnableWindow(FALSE);
	else
		m_sldVolume.EnableWindow(TRUE);
}

void CSoundConfigDlg::OnBnClickedOk()
{
	BOOL bDisA = m_rdoDisA.GetCheck();
	BOOL bDisB = m_rdoDisB.GetCheck();
	BOOL bMute = m_chkMute.GetCheck();
	LONG lVol = m_sldVolume.GetPos();

	if(m_bDisA != bDisA || m_bDisB != bDisB){
		std::string szSound = "00";

		if(bDisA)	szSound[0] = '1';
		else		szSound[0] = '0';
		
		if(bDisB)	szSound[1] = '1';
		else		szSound[1] = '0';
		
		scratchUtil::getInstance()->displaySound(szSound);
	}

	if(m_bMute != bMute){
		SetMuteValue(bMute);
	}

	if(!bMute && m_lVolume != lVol){
		SetVolumeValue(lVol+1);
	}

	OnOK();
}

