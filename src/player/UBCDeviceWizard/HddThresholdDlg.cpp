// HddThresholdDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HddThresholdDlg.h"
#include "common/libInstall/installUtil.h"

// CHddThresholdDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHddThresholdDlg, CDialog)

CHddThresholdDlg::CHddThresholdDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHddThresholdDlg::IDD, pParent)
	, m_nSelect(0)
	, m_nHddThreshold(100)
{
}

CHddThresholdDlg::~CHddThresholdDlg()
{
}

void CHddThresholdDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RB_SELECT1, m_nSelect);
	DDX_Text(pDX, IDC_EB_VALUE, m_nHddThreshold);
	DDX_Control(pDX, IDC_SPIN_VALUE, m_spHddThreshold);
	DDX_Control(pDX, IDC_PROGRESS_HDD_USAGE, m_pgcHddUsage);
}

BEGIN_MESSAGE_MAP(CHddThresholdDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CHddThresholdDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CHddThresholdDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_RB_SELECT1, &CHddThresholdDlg::OnBnClickedRbSelect)
	ON_BN_CLICKED(IDC_RB_SELECT2, &CHddThresholdDlg::OnBnClickedRbSelect)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_VALUE, &CHddThresholdDlg::OnDeltaposSpinValue)
	ON_REGISTERED_MESSAGE(WM_PROGRESSEX_THRESHOLD_CHANGE, &CHddThresholdDlg::OnHddThresholdChange)
END_MESSAGE_MAP()


// CHddThresholdDlg 메시지 처리기입니다.

BOOL CHddThresholdDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_EB_VALUE)->EnableWindow(m_nSelect == 0);
	GetDlgItem(IDC_SPIN_VALUE)->EnableWindow(m_nSelect == 0);

	m_spHddThreshold.SetRange32(50, 100);

	int nDiskUsed   = installUtil::getInstance()->getDiskUsed();
	m_nHddThreshold = installUtil::getInstance()->getThreshold();

	// 초기값은 100
	if(m_nHddThreshold < 50) m_nHddThreshold = 100;
	m_spHddThreshold.SetPos32(m_nHddThreshold);

	CString strHddUsage;
	strHddUsage.Format("%3d%%", nDiskUsed);

	m_pgcHddUsage.SetStyle(PROGRESS_TEXT);
	m_pgcHddUsage.SetRange(0, 100);
	m_pgcHddUsage.SetText(strHddUsage);
	m_pgcHddUsage.SetPos(nDiskUsed);
	m_pgcHddUsage.VisibleThreshold(true);
	m_pgcHddUsage.EnableThreshold(true);
	m_pgcHddUsage.SetThresholeMinMax(50, 100);
	m_pgcHddUsage.SetThreshole(m_nHddThreshold);
	m_pgcHddUsage.SetThresholdNotify(GetSafeHwnd(), WM_PROGRESSEX_THRESHOLD_CHANGE);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CHddThresholdDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	// 임계치 설정
	if(m_nSelect == 0)
	{
		installUtil::getInstance()->setThreshold(m_nHddThreshold);
	}
	// 즉시 청소
	else
	{
		installUtil::getInstance()->cleanContents();
	}

	OnOK();
}

void CHddThresholdDlg::OnBnClickedCancel()
{
	OnCancel();
}

void CHddThresholdDlg::OnBnClickedRbSelect()
{
	CButton* pSelect = (CButton*)GetDlgItem(IDC_RB_SELECT1);
	m_nSelect = (pSelect->GetCheck() ? 0 : 1);

	GetDlgItem(IDC_EB_VALUE)->EnableWindow(m_nSelect == 0);
	GetDlgItem(IDC_SPIN_VALUE)->EnableWindow(m_nSelect == 0);
}

void CHddThresholdDlg::OnDeltaposSpinValue(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	m_pgcHddUsage.SetThreshole(pNMUpDown->iPos + pNMUpDown->iDelta);

	*pResult = 0;
}

HRESULT CHddThresholdDlg::OnHddThresholdChange(WPARAM wParam, LPARAM lParam)
{
	m_nHddThreshold = (int)wParam;
	m_spHddThreshold.SetPos32(m_nHddThreshold);

	UpdateData(FALSE);

	return 0;
}
