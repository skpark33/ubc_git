// DisplayConfigDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCDeviceWizard.h"
#include "DisplayConfigDialog.h"

#include "DisplayChangeDialog.h"

// CDisplayConfigDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDisplayConfigDialog, CDialog)

CDisplayConfigDialog::CDisplayConfigDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CDisplayConfigDialog::IDD, pParent)
	, m_bRotation (true)
	, m_bFixedAspectRatio (false)
{

}

CDisplayConfigDialog::~CDisplayConfigDialog()
{
}

void CDisplayConfigDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_WIDTH_LONG, m_btnWidthLong);
	DDX_Control(pDX, IDC_BUTTON_WIDTH_SHORT, m_btnWidthShort);
	DDX_Control(pDX, IDC_BUTTON_HEIGHT_LONG, m_btnHeightLong);
	DDX_Control(pDX, IDC_BUTTON_HEIGHT_SHORT, m_btnHeightShort);
	DDX_Control(pDX, IDC_BUTTON_ROTATION, m_btnRotation);
	DDX_Control(pDX, IDC_BUTTON_ASPECT_RATIO, m_btnAspectRatio);
	DDX_Control(pDX, IDC_SLIDER_WIDTH, m_sliderWidth);
	DDX_Control(pDX, IDC_SLIDER_HEIGHT, m_sliderHeight);
	DDX_Control(pDX, IDC_STATIC_WIDTH, m_staticWidth);
	DDX_Control(pDX, IDC_STATIC_HEIGHT, m_staticHeight);
	DDX_Control(pDX, IDC_FRAME_WIDTH, m_frameWidth);
	DDX_Control(pDX, IDC_FRAME_HEIGHT, m_frameHeight);
	DDX_Control(pDX, IDC_FRAME_FREE, m_frameFree);
}


BEGIN_MESSAGE_MAP(CDisplayConfigDialog, CDialog)
	ON_MESSAGE(WM_CHANGE_RESOLUTION, OnChangeResolution)
	ON_MESSAGE(WM_RESTORE_RESOLUTION, OnRestoreResolution)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_BN_CLICKED(IDC_BUTTON_WIDTH_SHORT, &CDisplayConfigDialog::OnBnClickedButtonWidthShort)
	ON_BN_CLICKED(IDC_BUTTON_WIDTH_LONG, &CDisplayConfigDialog::OnBnClickedButtonWidthLong)
	ON_BN_CLICKED(IDC_BUTTON_HEIGHT_LONG, &CDisplayConfigDialog::OnBnClickedButtonHeightLong)
	ON_BN_CLICKED(IDC_BUTTON_HEIGHT_SHORT, &CDisplayConfigDialog::OnBnClickedButtonHeightShort)
	ON_BN_CLICKED(IDC_BUTTON_ROTATION, &CDisplayConfigDialog::OnBnClickedButtonRotation)
	ON_BN_CLICKED(IDC_BUTTON_ASPECT_RATIO, &CDisplayConfigDialog::OnBnClickedButtonAspectRatio)
END_MESSAGE_MAP()


// CDisplayConfigDialog 메시지 처리기입니다.

BOOL CDisplayConfigDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
/*
	m_cbxStandard.AddString("640 x 480");
	m_cbxStandard.AddString("720 x 480");
	m_cbxStandard.AddString("720 x 576");
	m_cbxStandard.AddString("800 x 480");
	m_cbxStandard.AddString("800 x 600");
	m_cbxStandard.AddString("848 x 480");
	m_cbxStandard.AddString("960 x 720");
	m_cbxStandard.AddString("1024 x 768");
	m_cbxStandard.AddString("1152 x 864");
	m_cbxStandard.AddString("1280 x 768");
	m_cbxStandard.AddString("1280 x 800");
	m_cbxStandard.AddString("1280 x 960");
	m_cbxStandard.AddString("1280 x 1024");
	m_cbxStandard.AddString("1360 x 1020");
	m_cbxStandard.AddString("1400 x 1050");
	m_cbxStandard.AddString("1520 x 1140");
	m_cbxStandard.AddString("1600 x 1200");
	m_cbxStandard.AddString("1680 x 1050");
	m_cbxStandard.AddString("1792 x 1344");
	m_cbxStandard.AddString("1800 x 1440");
	m_cbxStandard.AddString("1920 x 1080");
	m_cbxStandard.AddString("1920 x 1200");
	m_cbxStandard.AddString("1920 x 1440");
	m_cbxStandard.AddString("2048 x 1536");

	m_cbxRotation.AddString("480 x 640");
	m_cbxRotation.AddString("480 x 720");
	m_cbxRotation.AddString("576 x 720");
	m_cbxRotation.AddString("480 x 800");
	m_cbxRotation.AddString("600 x 800");
	m_cbxRotation.AddString("480 x 848");
	m_cbxRotation.AddString("720 x 960");
	m_cbxRotation.AddString("768 x 1024");
	m_cbxRotation.AddString("864 x 1152");
	m_cbxRotation.AddString("768 x 1280");
	m_cbxRotation.AddString("800 x 1280");
	m_cbxRotation.AddString("960 x 1280");
	m_cbxRotation.AddString("1024 x 1280");
	m_cbxRotation.AddString("1020 x 1360");
	m_cbxRotation.AddString("1050 x 1400");
	m_cbxRotation.AddString("1140 x 1520");
	m_cbxRotation.AddString("1200 x 1600");
	m_cbxRotation.AddString("1050 x 1680");
	m_cbxRotation.AddString("1344 x 1792");
	m_cbxRotation.AddString("1440 x 1800");
	m_cbxRotation.AddString("1080 x 1920");
	m_cbxRotation.AddString("1200 x 1920");
	m_cbxRotation.AddString("1440 x 1920");
	m_cbxRotation.AddString("1536 x 2048");

	m_cbxCustomWidth.AddString("480");
	m_cbxCustomWidth.AddString("576");
	m_cbxCustomWidth.AddString("600");
	m_cbxCustomWidth.AddString("640");
	m_cbxCustomWidth.AddString("720");
	m_cbxCustomWidth.AddString("768");
	m_cbxCustomWidth.AddString("800");
	m_cbxCustomWidth.AddString("848");
	m_cbxCustomWidth.AddString("864");
	m_cbxCustomWidth.AddString("960");
	m_cbxCustomWidth.AddString("1020");
	m_cbxCustomWidth.AddString("1024");
	m_cbxCustomWidth.AddString("1050");
	m_cbxCustomWidth.AddString("1080");
	m_cbxCustomWidth.AddString("1140");
	m_cbxCustomWidth.AddString("1152");
	m_cbxCustomWidth.AddString("1200");
	m_cbxCustomWidth.AddString("1280");
	m_cbxCustomWidth.AddString("1344");
	m_cbxCustomWidth.AddString("1360");
	m_cbxCustomWidth.AddString("1400");
	m_cbxCustomWidth.AddString("1440");
	m_cbxCustomWidth.AddString("1520");
	m_cbxCustomWidth.AddString("1536");
	m_cbxCustomWidth.AddString("1600");
	m_cbxCustomWidth.AddString("1680");
	m_cbxCustomWidth.AddString("1792");
	m_cbxCustomWidth.AddString("1800");
	m_cbxCustomWidth.AddString("1920");
	m_cbxCustomWidth.AddString("2048");

	m_cbxCustomHeight.AddString("480");
	m_cbxCustomHeight.AddString("576");
	m_cbxCustomHeight.AddString("600");
	m_cbxCustomHeight.AddString("640");
	m_cbxCustomHeight.AddString("720");
	m_cbxCustomHeight.AddString("768");
	m_cbxCustomHeight.AddString("800");
	m_cbxCustomHeight.AddString("848");
	m_cbxCustomHeight.AddString("864");
	m_cbxCustomHeight.AddString("960");
	m_cbxCustomHeight.AddString("1020");
	m_cbxCustomHeight.AddString("1024");
	m_cbxCustomHeight.AddString("1050");
	m_cbxCustomHeight.AddString("1080");
	m_cbxCustomHeight.AddString("1140");
	m_cbxCustomHeight.AddString("1152");
	m_cbxCustomHeight.AddString("1200");
	m_cbxCustomHeight.AddString("1280");
	m_cbxCustomHeight.AddString("1344");
	m_cbxCustomHeight.AddString("1360");
	m_cbxCustomHeight.AddString("1400");
	m_cbxCustomHeight.AddString("1440");
	m_cbxCustomHeight.AddString("1520");
	m_cbxCustomHeight.AddString("1536");
	m_cbxCustomHeight.AddString("1600");
	m_cbxCustomHeight.AddString("1680");
	m_cbxCustomHeight.AddString("1792");
	m_cbxCustomHeight.AddString("1800");
	m_cbxCustomHeight.AddString("1920");
	m_cbxCustomHeight.AddString("2048");
*/
	m_btnWidthLong.LoadBitmap(IDB_WIDTH_LONG, RGB(232,168,192));
	m_btnWidthShort.LoadBitmap(IDB_WIDTH_SHORT, RGB(232,168,192));
	m_btnHeightLong.LoadBitmap(IDB_HEIGHT_SHORT, RGB(232,168,192));
	m_btnHeightShort.LoadBitmap(IDB_HEIGHT_LONG, RGB(232,168,192));
	OnBnClickedButtonAspectRatio();

	//
	m_currentResolution.cx = ::GetSystemMetrics(SM_CXSCREEN);
	m_currentResolution.cy = ::GetSystemMetrics(SM_CYSCREEN);
	//m_currentResolution.cx = 1080;
	//m_currentResolution.cy = 1920;

	OnBnClickedButtonRotation();
	for(int i=0; i<m_listWidth.GetCount(); i++)
	{
		if(m_listWidth.GetAt(i) == m_currentResolution.cx && m_listHeight.GetAt(i) == m_currentResolution.cy)
		{
			m_sliderWidth.SetPos(i);
			m_sliderHeight.SetPos(i);
			RefreshStatic();
			return TRUE;
		}
	}

	OnBnClickedButtonRotation();
	for(int i=0; i<m_listWidth.GetCount(); i++)
	{
		if(m_listWidth.GetAt(i) == m_currentResolution.cx && m_listHeight.GetAt(i) == m_currentResolution.cy)
		{
			m_sliderWidth.SetPos(i);
			m_sliderHeight.SetPos(i);
			RefreshStatic();
			return TRUE;
		}
	}

	OnBnClickedButtonRotation();
	ResetResolutionList(2);
	for(int i=0; i<m_listWidth.GetCount(); i++)
	{
		if(m_listWidth.GetAt(i) == m_currentResolution.cx)
		{
			m_sliderWidth.SetPos(i);
			break;
		}
	}
	for(int i=0; i<m_listHeight.GetCount(); i++)
	{
		if(m_listHeight.GetAt(i) == m_currentResolution.cy)
		{
			m_sliderHeight.SetPos(i);
			break;
		}
	}
	RefreshStatic();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDisplayConfigDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CString str_width, str_height;

	m_staticWidth.GetWindowText(str_width);
	m_staticHeight.GetWindowText(str_height);

	int width = atoi(str_width);
	int height = atoi(str_height);

	if(width != 0 && height != 0 && (m_currentResolution.cx != width || m_currentResolution.cy != height))
	{
		SIZE size;
		size.cx = width;
		size.cy = height;

		if( ChangeResolution( size) == false)
		{
			OnRestoreResolution(0,0);
			AfxMessageBox("Can not change resolution !!!", MB_ICONSTOP);
		}
		else
		{
			CDisplayChangeDialog dlg(this);
			dlg.DoModal();
		}
	}
}

void CDisplayConfigDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnCancel();

	GetParent()->PostMessage(WM_CLOSE);
}

bool CDisplayConfigDialog::ChangeResolution(SIZE size)
{
	DEVMODE stMode;
	::ZeroMemory(&stMode, sizeof(DEVMODE));
	stMode.dmSize		= sizeof(DEVMODE);
	stMode.dmBitsPerPel	= 32;			// 32비트 칼라로 변경
	stMode.dmPelsWidth	= size.cx;		// 프로그램 실행위한 최소 가로 해상도
	stMode.dmPelsHeight	= size.cy;		// 프로그램 실행위한 최소 세로 해상도
	stMode.dmFields		= DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	//ChangeDisplaySettings(&stMode, CDS_FULLSCREEN );		// 프로그램 종료 후 원래 해상도로 복구
	LONG ret_value = ChangeDisplaySettings(&stMode, CDS_UPDATEREGISTRY );	// 프로그램 종료 후에도 해상도 유지

	GetParent()->SendMessage(WM_CHANGE_RESOLUTION);
	CenterWindow();
	::AfxGetMainWnd()->CenterWindow();

	if(ret_value == DISP_CHANGE_SUCCESSFUL)
		return true;

	return false;
}

LRESULT CDisplayConfigDialog::OnChangeResolution(WPARAM wParam, LPARAM lParam)
{
	m_currentResolution.cx = ::GetSystemMetrics(SM_CXSCREEN);
	m_currentResolution.cy = ::GetSystemMetrics(SM_CYSCREEN);

	return 0;
}

LRESULT CDisplayConfigDialog::OnRestoreResolution(WPARAM wParam, LPARAM lParam)
{
	ChangeResolution(m_currentResolution);

	return 0;
}

void CDisplayConfigDialog::ResetResolutionList(int type)
{
	m_listWidth.RemoveAll();
	m_listHeight.RemoveAll();

	switch(type)
	{
	case 0:
		m_listWidth.Add(640); m_listHeight.Add(480);
		m_listWidth.Add(720); m_listHeight.Add(480);
		m_listWidth.Add(720); m_listHeight.Add(576);
		m_listWidth.Add(800); m_listHeight.Add(480);
		m_listWidth.Add(800); m_listHeight.Add(600);
		m_listWidth.Add(848); m_listHeight.Add(480);
		m_listWidth.Add(960); m_listHeight.Add(720);
		m_listWidth.Add(1024); m_listHeight.Add(768);
		m_listWidth.Add(1152); m_listHeight.Add(864);
		m_listWidth.Add(1280); m_listHeight.Add(768);
		m_listWidth.Add(1280); m_listHeight.Add(800);
		m_listWidth.Add(1280); m_listHeight.Add(960);
		m_listWidth.Add(1280); m_listHeight.Add(1024);
		m_listWidth.Add(1360); m_listHeight.Add(1020);
		m_listWidth.Add(1400); m_listHeight.Add(1050);
		m_listWidth.Add(1520); m_listHeight.Add(1140);
		m_listWidth.Add(1600); m_listHeight.Add(1200);
		m_listWidth.Add(1680); m_listHeight.Add(1050);
		m_listWidth.Add(1792); m_listHeight.Add(1344);
		m_listWidth.Add(1800); m_listHeight.Add(1440);
		m_listWidth.Add(1920); m_listHeight.Add(1080);
		m_listWidth.Add(1920); m_listHeight.Add(1200);
		m_listWidth.Add(1920); m_listHeight.Add(1440);
		m_listWidth.Add(2048); m_listHeight.Add(1536);

		m_btnRotation.EnableWindow(TRUE);

		m_frameWidth.ShowWindow(SW_SHOW);
		m_frameHeight.ShowWindow(SW_HIDE);
		m_frameFree.ShowWindow(SW_HIDE);

		break;

	case 1:
		m_listHeight.Add(640); m_listWidth.Add(480);
		m_listHeight.Add(720); m_listWidth.Add(480);
		m_listHeight.Add(720); m_listWidth.Add(576);
		m_listHeight.Add(800); m_listWidth.Add(480);
		m_listHeight.Add(800); m_listWidth.Add(600);
		m_listHeight.Add(848); m_listWidth.Add(480);
		m_listHeight.Add(960); m_listWidth.Add(720);
		m_listHeight.Add(1024); m_listWidth.Add(768);
		m_listHeight.Add(1152); m_listWidth.Add(864);
		m_listHeight.Add(1280); m_listWidth.Add(768);
		m_listHeight.Add(1280); m_listWidth.Add(800);
		m_listHeight.Add(1280); m_listWidth.Add(960);
		m_listHeight.Add(1280); m_listWidth.Add(1024);
		m_listHeight.Add(1360); m_listWidth.Add(1020);
		m_listHeight.Add(1400); m_listWidth.Add(1050);
		m_listHeight.Add(1520); m_listWidth.Add(1140);
		m_listHeight.Add(1600); m_listWidth.Add(1200);
		m_listHeight.Add(1680); m_listWidth.Add(1050);
		m_listHeight.Add(1792); m_listWidth.Add(1344);
		m_listHeight.Add(1800); m_listWidth.Add(1440);
		m_listHeight.Add(1920); m_listWidth.Add(1080);
		m_listHeight.Add(1920); m_listWidth.Add(1200);
		m_listHeight.Add(1920); m_listWidth.Add(1440);
		m_listHeight.Add(2048); m_listWidth.Add(1536);

		m_btnRotation.EnableWindow(TRUE);

		m_frameWidth.ShowWindow(SW_HIDE);
		m_frameHeight.ShowWindow(SW_SHOW);
		m_frameFree.ShowWindow(SW_HIDE);

		break;

	case 2:
		m_listWidth.Add(480); m_listHeight.Add(480);
		m_listWidth.Add(576); m_listHeight.Add(576);
		m_listWidth.Add(600); m_listHeight.Add(600);
		m_listWidth.Add(640); m_listHeight.Add(640);
		m_listWidth.Add(720); m_listHeight.Add(720);
		m_listWidth.Add(768); m_listHeight.Add(768);
		m_listWidth.Add(800); m_listHeight.Add(800);
		m_listWidth.Add(848); m_listHeight.Add(848);
		m_listWidth.Add(864); m_listHeight.Add(864);
		m_listWidth.Add(960); m_listHeight.Add(960);
		m_listWidth.Add(1020); m_listHeight.Add(1020);
		m_listWidth.Add(1024); m_listHeight.Add(1024);
		m_listWidth.Add(1050); m_listHeight.Add(1050);
		m_listWidth.Add(1080); m_listHeight.Add(1080);
		m_listWidth.Add(1140); m_listHeight.Add(1140);
		m_listWidth.Add(1152); m_listHeight.Add(1152);
		m_listWidth.Add(1200); m_listHeight.Add(1200);
		m_listWidth.Add(1280); m_listHeight.Add(1280);
		m_listWidth.Add(1344); m_listHeight.Add(1344);
		m_listWidth.Add(1360); m_listHeight.Add(1360);
		m_listWidth.Add(1400); m_listHeight.Add(1400);
		m_listWidth.Add(1440); m_listHeight.Add(1440);
		m_listWidth.Add(1520); m_listHeight.Add(1520);
		m_listWidth.Add(1536); m_listHeight.Add(1536);
		m_listWidth.Add(1600); m_listHeight.Add(1600);
		m_listWidth.Add(1680); m_listHeight.Add(1680);
		m_listWidth.Add(1792); m_listHeight.Add(1792);
		m_listWidth.Add(1800); m_listHeight.Add(1800);
		m_listWidth.Add(1920); m_listHeight.Add(1920);
		m_listWidth.Add(2048); m_listHeight.Add(2048);

		m_bRotation = false;
		RefreshButton();

		m_btnRotation.EnableWindow(FALSE);

		m_frameWidth.ShowWindow(SW_HIDE);
		m_frameHeight.ShowWindow(SW_HIDE);
		m_frameFree.ShowWindow(SW_SHOW);

		break;
	}

	m_sliderWidth.SetRange(0, m_listWidth.GetCount()-1);
	m_sliderHeight.SetRange(0, m_listHeight.GetCount()-1);
}

void CDisplayConfigDialog::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);

	if(pScrollBar && pScrollBar->GetSafeHwnd() == m_sliderWidth.GetSafeHwnd())
	{
		int pos = m_sliderWidth.GetPos();
		if(m_bFixedAspectRatio)
			m_sliderHeight.SetPos(pos);

		RefreshStatic();
	}
}

void CDisplayConfigDialog::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);

	if(pScrollBar && pScrollBar->GetSafeHwnd() == m_sliderHeight.GetSafeHwnd())
	{
		int pos = m_sliderHeight.GetPos();
		if(m_bFixedAspectRatio)
			m_sliderWidth.SetPos(pos);

		RefreshStatic();
	}
}

void CDisplayConfigDialog::OnBnClickedButtonWidthShort()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int pos = m_sliderWidth.GetPos();
	if(pos > 0)
		pos--;
	m_sliderWidth.SetPos(pos);

	if(m_bFixedAspectRatio)
		m_sliderHeight.SetPos(pos);

	RefreshStatic();
}

void CDisplayConfigDialog::OnBnClickedButtonWidthLong()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int pos = m_sliderWidth.GetPos();
	if(pos < m_listWidth.GetCount())
		pos++;
	m_sliderWidth.SetPos(pos);

	if(m_bFixedAspectRatio)
		m_sliderHeight.SetPos(pos);

	RefreshStatic();
}

void CDisplayConfigDialog::OnBnClickedButtonHeightLong()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int pos = m_sliderHeight.GetPos();
	if(pos < m_listHeight.GetCount())
		pos++;
	m_sliderHeight.SetPos(pos);

	if(m_bFixedAspectRatio)
		m_sliderWidth.SetPos(pos);

	RefreshStatic();
}

void CDisplayConfigDialog::OnBnClickedButtonHeightShort()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int pos = m_sliderHeight.GetPos();
	if(pos > 0)
		pos--;
	m_sliderHeight.SetPos(pos);

	if(m_bFixedAspectRatio)
		m_sliderWidth.SetPos(pos);

	RefreshStatic();
}

void CDisplayConfigDialog::OnBnClickedButtonRotation()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_bRotation = !m_bRotation;

	int width_pos = m_sliderWidth.GetPos();
	int height_pos = m_sliderHeight.GetPos();

	if(m_bRotation)
	{
		if(m_bFixedAspectRatio)
		{
			ResetResolutionList(1);
		}
		else
		{
			ResetResolutionList(2);
		}
	}
	else
	{
		if(m_bFixedAspectRatio)
		{
			ResetResolutionList(0);
		}
		else
		{
			ResetResolutionList(2);
		}
	}

	RefreshButton();

	m_sliderWidth.SetPos(height_pos);
	m_sliderHeight.SetPos(width_pos);

	RefreshStatic();
}

void CDisplayConfigDialog::OnBnClickedButtonAspectRatio()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_bFixedAspectRatio = !m_bFixedAspectRatio;

	int width_pos = m_sliderWidth.GetPos();

	if(m_bFixedAspectRatio)
	{
		if(m_bRotation)
			ResetResolutionList(1);
		else
			ResetResolutionList(0);
	}
	else
	{
		ResetResolutionList(2);
	}

	RefreshButton();

	m_sliderWidth.SetPos(width_pos);
	m_sliderHeight.SetPos(width_pos);

	RefreshStatic();
}

void CDisplayConfigDialog::RefreshStatic()
{
	CString str;

	int pos = m_sliderWidth.GetPos();
	str.Format("%d", m_listWidth.GetAt(pos));
	m_staticWidth.SetWindowText(str);

	pos = m_sliderHeight.GetPos();
	str.Format("%d", m_listHeight.GetAt(pos));
	m_staticHeight.SetWindowText(str);
}

void CDisplayConfigDialog::RefreshButton()
{
	//
	if(m_bRotation)
	{
		m_btnRotation.LoadBitmap(IDB_ROTATION_ON, RGB(232,168,192));
		m_btnRotation.Invalidate();
	}
	else
	{
		m_btnRotation.LoadBitmap(IDB_ROTATION, RGB(232,168,192));
		m_btnRotation.Invalidate();
	}

	//
	if(m_bFixedAspectRatio)
	{
		m_btnAspectRatio.LoadBitmap(IDB_ASPECT_RATIO, RGB(232,168,192));
		m_btnAspectRatio.Invalidate();
	}
	else
	{
		m_btnAspectRatio.LoadBitmap(IDB_ASPECT_RATIO_OFF, RGB(232,168,192));
		m_btnAspectRatio.Invalidate();
	}
}
