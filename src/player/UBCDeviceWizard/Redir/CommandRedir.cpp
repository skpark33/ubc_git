#include "StdAfx.h"
#include "CommandRedir.h"

CCommandRedirector::CCommandRedirector(void)
:	m_strResult ("")
{
}

CCommandRedirector::~CCommandRedirector(void)
{
}

void CCommandRedirector::WriteStdOut(LPCSTR pszOutput)
{
	m_strResult.Append(pszOutput);
}

void CCommandRedirector::WriteStdError(LPCSTR pszError)
{
	m_strResult.Append(pszError);
}
