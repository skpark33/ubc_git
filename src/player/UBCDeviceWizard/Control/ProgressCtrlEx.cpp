/*******************************************/
/*                                         */
/* - title : CProgressCtrlEx.cpp               */
/* - description : ProgressCtrl Extends    */
/* - author : you !                        */
/* - date : 2002-09-13                     */
/*                                         */
/*******************************************/

#include "stdafx.h"
#include "ProgressCtrlEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CProgressCtrlEx::CProgressCtrlEx()
{
	m_nMin = 0;
	m_nMax = 100;

	m_nPos = 0;
	m_nStep = 10;

	m_clrFore		= ::GetSysColor(COLOR_HIGHLIGHT);
	m_clrBk			= ::GetSysColor(COLOR_WINDOW);
	m_clrTextFore	= ::GetSysColor(COLOR_HIGHLIGHT);
	m_clrTextBk		= ::GetSysColor(COLOR_WINDOW);
	m_clrText = RGB(255, 0, 0);
	m_strText.Empty();
	m_nStyle = PROGRESS_NONE;	// 공통 ProgressCtrl

	m_font.CreatePointFont(90, _T("Tahoma"));

	m_bThresholdEditing = false;
	m_MouseType = MOUSE_IDLE;

	// 임계영역 표시 및 조정
	m_bThresholdVisible = false;
	m_bThresholdEnable = false;
	m_nThresholdMin = 0;
	m_nThresholdMax = 100;
	m_nThreshold = m_nThresholdMax;
	m_clrThreshold = RGB(255, 0, 0);

	m_hNotifyWnd = NULL;
	m_nNotifyMsg = 0;

	// test
//	m_bThresholdVisible = true;
//	m_bThresholdEnable = true;
//	m_nThreshold = 75;
}

CProgressCtrlEx::~CProgressCtrlEx()
{
}


BEGIN_MESSAGE_MAP(CProgressCtrlEx, CProgressCtrl)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


void CProgressCtrlEx::OnPaint() 
{
	if(m_nStyle == PROGRESS_NONE) {	// 일반 ProgressCtrl 일때
		CProgressCtrl::OnPaint();
		return;
	}

	CPaintDC dc(this);

	CRect rcClient, rcLeft, rcRight;
	GetClientRect(rcClient);
	rcLeft = rcRight = rcClient;

	CFont *old_font = dc.SelectObject(&m_font);

	/* 사용자가 지정한 위치 값(m_nPos)을 기준으로 */
	/* m_nMin과 m_nMax 범위에서 해당되는 퍼센트를 구한다 */
	double Fraction = (double)(m_nPos - m_nMin) / (double)(m_nMax - m_nMin);

	/* Progress 진행바를 그리기 위한 범위 값을 구한다 */
    rcLeft.right = rcLeft.left + (int)((rcLeft.right - rcLeft.left) * Fraction);

	/* rcLeft : 진행바 영역     */
	/* rcRight : 진행바를 제외한 영역 */
	rcLeft.right = (int)((rcClient.right - rcClient.left) * Fraction);
	rcRight.left = rcLeft.right;

	switch(m_nStyle) {
	case PROGRESS_TEXT : {	
			/* 1. 진행바를 를 그린다 */
		    dc.FillSolidRect(rcLeft, m_clrFore);	/* 푸른색 */

			/* 2. 진행바를 제외한 나머지 부분을 그린다 */
			dc.FillSolidRect(rcRight, m_clrBk);		/* 흰색 */

			if(m_strText.IsEmpty())
			{
				CString str;
				str.Format(_T("%d%%"), (int)(Fraction*100.0));
				m_strText = str;
			}

			char szTemp[256] = {0};
			strcpy(szTemp, m_strText);
			CString strText(szTemp);

			if(m_bThresholdVisible)
			{
				CPen pen(PS_SOLID, 4, m_clrThreshold);
				int nThreshold = rcClient.left + (rcClient.Width() / 100.) * m_nThreshold;
				CPen* oldpen = dc.SelectObject(&pen);
				dc.MoveTo(nThreshold, rcClient.top);
				dc.LineTo(nThreshold, rcClient.bottom);
				dc.SelectObject(&oldpen);

				CString str;
		        str.Format(_T(" (%d%%)"), m_nThreshold);
				strText += str;
			}

	        dc.SetBkMode(1); // TRANSPARENT

			/* CRgn class를 사용하는 이유 ? */
			/*  : 문자열의 특정 범위까지만 색상에 변화를 주어야 하기 때문에 */
			/* 1. 진행바 영역에 없을때 문자열 그리기 */
			CRgn rgn;
	        rgn.CreateRectRgn(rcLeft.left, rcLeft.top, rcLeft.right, rcLeft.bottom);
	        dc.SelectClipRgn(&rgn);
	        dc.SetTextColor(m_clrTextBk);

	        dc.DrawText(strText, rcClient, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

			/* 2. 진행바 영역에 있을때 문자열 그리기 */
	        rgn.DeleteObject();
	        rgn.CreateRectRgn(rcRight.left, rcRight.top, rcRight.right, rcRight.bottom);
	        dc.SelectClipRgn(&rgn);
	        dc.SetTextColor(m_clrTextFore);

	        dc.DrawText(strText, rcClient, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

			dc.Draw3dRect(rcClient, RGB(128,128,128), RGB(128,128,128));
		} break;
	case PROGRESS_BITMAP : {
			int nHBmp, nVBmp;

			/* 1. SetBitmap 함수로 설정한 비트맵과 호환된는 DC를 만든다 */
			CDC memDC;
			memDC.CreateCompatibleDC(&dc);

			CBitmap* pOldBitmap = (CBitmap*)memDC.SelectObject(&m_Bitmap);

			/* 2. 컨트롤과 동일한 영역을 차지하는 비트맵과 DC를 만든다 */
			CDC leftDC, rightDC;
			leftDC.CreateCompatibleDC(&dc);
			rightDC.CreateCompatibleDC(&dc);

			CBitmap leftBmp, rightBmp;
			leftBmp.CreateCompatibleBitmap(&memDC, rcClient.Width(), rcClient.Height());
			rightBmp.CreateCompatibleBitmap(&memDC, rcClient.Width(), rcClient.Height());
	
			CBitmap* pOldleftBmp = (CBitmap*)leftDC.SelectObject(&leftBmp);
			CBitmap* pOldrightBmp = (CBitmap*)rightDC.SelectObject(&rightBmp);

			/* 비트맵 크기와 컨트롤 크기가 일치하지 않으므로 타이 형식으로 leftDC에 비트맵을 그린다 */
			nHBmp = rcClient.Width() / m_nWidth;
			if(rcClient.Width() % m_nWidth) nHBmp++;

			nVBmp = rcClient.Height() / m_nHeight;
			if(rcClient.Height() % m_nHeight) nVBmp++;

			for(int x = 0; x < nHBmp; x++) {
				for(int y = 0; y < nVBmp; y++) {
					leftDC.BitBlt(m_nWidth * x, m_nHeight * y, m_nWidth, m_nHeight,
								&memDC, 0, 0, SRCCOPY);
				}
			}

			/* 기본 색상을 설정한다 */
			rightDC.FillSolidRect(rcClient, m_clrBk);

			/* Font Object를 설정한다 */
			CFont* pFont = GetFont();	/* 설정된 폰트를 얻는다 */
			CFont* pOldFont = (CFont*)rightDC.SelectObject(pFont);

			CString str;
			str.Format(_T("%d%%"), m_nPos);
			CSize sz = memDC.GetTextExtent(str);	/* 문자열의 사이즈를 구한다 */

			if(rcRight.Width() > sz.cx) {
				rightDC.SetTextColor(m_clrText);
				rightDC.TextOut(2, (rcLeft.Height() / 2) - (sz.cy / 2) + 1, str);
			}

			/* Progress Ctrl의 기본적인 3D 영역은 그리ㄱ지 않기 위해서 영역을 설정한다 */
			rcLeft.DeflateRect(1, 1);
			rcRight.DeflateRect(1, 1);

			/* 진행바 그리기 */
			memDC.BitBlt(1, 1, rcLeft.Width(), rcLeft.Height(), &leftDC, 0, 0, SRCCOPY);

			/* 진행바를 제외한 나머지 부분 그리기 */
			memDC.BitBlt(rcRight.left, rcRight.top, rcRight.Width(), rcRight.Height(), &rightDC, 0, 0, SRCCOPY);

			leftDC.SelectObject(pOldleftBmp);
			rightDC.SelectObject(pOldrightBmp);
			memDC.SelectObject(pOldBitmap);

			rightDC.SelectObject(pOldFont);
	
			leftBmp.DeleteObject();
			rightBmp.DeleteObject();
		}
		break;
	}

	dc.SelectObject(old_font);
}

BOOL CProgressCtrlEx::OnEraseBkgnd(CDC* pDC) 
{
	if(m_nStyle != PROGRESS_NONE)
		return TRUE;

	return CProgressCtrl::OnEraseBkgnd(pDC);
}

void CProgressCtrlEx::SetBitmap(int nId)
{
	/* 비트맵 핸들을 얻는다 */
	HBITMAP hBmp = (HBITMAP)::LoadImage(AfxGetApp()->m_hInstance,
		MAKEINTRESOURCE(nId),
		IMAGE_BITMAP,
		0,
		0,
		0);

	m_Bitmap.Detach();
	m_Bitmap.Attach(hBmp);

	BITMAP bmp;
	m_Bitmap.GetObject(sizeof(bmp), &bmp);

	m_nWidth = bmp.bmWidth;
	m_nHeight = bmp.bmHeight;
}

void CProgressCtrlEx::SetText(CString strText)
{
	m_strText = strText;
}

int CProgressCtrlEx::SetPos(int nPos)
{
	if(m_nMin <= nPos && nPos <= m_nMax) {
		m_nPos = nPos;
		Invalidate();
	}

	return CProgressCtrl::SetPos(m_nPos);
}

void CProgressCtrlEx::SetRange(int nLower, int nUpper)
{
	if(nLower <= nUpper) {
		m_nMin = nLower;
		m_nMax = nUpper;
	}

	CProgressCtrl::SetRange32(m_nMin, m_nMax);
}

int CProgressCtrlEx::SetStep(int nStep)
{
	m_nStep = nStep;

	return CProgressCtrl::SetStep(m_nStep);
}

int CProgressCtrlEx::StepIt()
{
	m_nPos += m_nStep;
	
	if(m_nPos > m_nMax)
		m_nPos = m_nMin + (m_nPos - m_nMax);

	Invalidate();

	return CProgressCtrl::StepIt();
}

void CProgressCtrlEx::SetForeColor(COLORREF clrFore)
{
	m_clrFore = clrFore;
}

void CProgressCtrlEx::SetBkColor(COLORREF clrBk)
{
	m_clrBk = clrBk;
}

void CProgressCtrlEx::SetTextForeColor(COLORREF clrTextFore)
{
	m_clrTextFore = clrTextFore;
}

void CProgressCtrlEx::SetTextBkColor(COLORREF clrTextBk)
{
	m_clrTextBk = clrTextBk;
}

void CProgressCtrlEx::SetTextColor(COLORREF clrText)
{
	m_clrText = clrText;
}

void CProgressCtrlEx::SetStyle(int nStyle)
{
	m_nStyle = nStyle;
}

// 임계영역 표시 및 조정
void CProgressCtrlEx::VisibleThreshold(bool bVisible)
{
	m_bThresholdVisible = bVisible;
	Invalidate();
}

bool CProgressCtrlEx::GetThresholdVisible()
{
	return m_bThresholdVisible;
}

void CProgressCtrlEx::EnableThreshold(bool bEnable)
{
	m_bThresholdEnable = bEnable;
	Invalidate();
}

bool CProgressCtrlEx::GetThresholdEnable()
{
	return m_bThresholdEnable;
}

void CProgressCtrlEx::SetThreshole(int nThreshold)
{
	m_nThreshold = nThreshold;
	
	if(m_nThreshold < m_nThresholdMin) m_nThreshold = m_nThresholdMin;
	if(m_nThreshold > m_nThresholdMax) m_nThreshold = m_nThresholdMax;

	Invalidate();
}

void CProgressCtrlEx::SetThresholeMinMax(int nMin, int nMax)
{
	if(nMin < 0) nMin = 0;
	if(nMax > 100) nMax = 100;

	if(nMin > nMax) nMax = nMin;

	m_nThresholdMin = nMin;
	m_nThresholdMax = nMax;

	if(m_nThreshold < m_nThresholdMin) m_nThreshold = m_nThresholdMin;
	if(m_nThreshold > m_nThresholdMax) m_nThreshold = m_nThresholdMax;

	Invalidate();
}

int CProgressCtrlEx::GetThreshold()
{
	return m_nThreshold;
}

void CProgressCtrlEx::SetThresholdColor(COLORREF clrThreshold)
{
	m_clrThreshold = clrThreshold;
}

COLORREF CProgressCtrlEx::GetThresholdColor()
{
	return m_clrThreshold;
}

void CProgressCtrlEx::SetThresholdNotify(HWND hWnd, UINT nNotifyMsg)
{
	m_hNotifyWnd = hWnd;
	m_nNotifyMsg = nNotifyMsg;
}

void CProgressCtrlEx::OnLButtonDown(UINT nFlags, CPoint point)
{
	CProgressCtrl::OnLButtonDown(nFlags, point);

	if(m_nStyle != PROGRESS_TEXT) return;
	if(!m_bThresholdVisible) return;
	if(!m_bThresholdEnable) return;

	SetFocus();
	
	if(m_MouseType == MOUSE_SIZING_LEFT)
	{
		m_bThresholdEditing = true;
		SetCapture();
		SetCursor(m_MouseType);
	}
}

void CProgressCtrlEx::OnLButtonUp(UINT nFlags, CPoint point)
{
	CProgressCtrl::OnLButtonUp(nFlags, point);

	if(m_nStyle != PROGRESS_TEXT) return;
	if(!m_bThresholdVisible) return;
	if(!m_bThresholdEnable) return;

	if(m_bThresholdEditing)
	{
		m_bThresholdEditing = false;
		ReleaseCapture();
	}

	m_MouseType = MOUSE_IDLE;
	SetCursor(m_MouseType);
}

void CProgressCtrlEx::OnMouseMove(UINT nFlags, CPoint point)
{
	CProgressCtrl::OnMouseMove(nFlags, point);

	if(m_nStyle != PROGRESS_TEXT) return;
	if(!m_bThresholdVisible) return;
	if(!m_bThresholdEnable) return;
	
	CRect rcClient;
	GetClientRect(rcClient);

	if(!m_bThresholdEditing)
	{
		int nThreshold = rcClient.left + (rcClient.Width() / 100.) * m_nThreshold;

		if( point.x > nThreshold-2 &&
			point.x < nThreshold+2 )
		{
			m_MouseType = MOUSE_SIZING_LEFT;
		}
		else
		{
			m_MouseType = MOUSE_IDLE;
		}

		SetCursor(m_MouseType);
	}
	else
	{
		int nCurThreshold = 100;

		if(point.x < rcClient.left) nCurThreshold = 0;
		else if(point.x > rcClient.right) nCurThreshold = 100;
		else
		{
			nCurThreshold = (point.x - rcClient.left) * 100. / rcClient.Width();
		}

		if(nCurThreshold < m_nThresholdMin) nCurThreshold = m_nThresholdMin;
		if(nCurThreshold > m_nThresholdMax) nCurThreshold = m_nThresholdMax;

		m_nThreshold = nCurThreshold;

		RedrawWindow();

		if(m_hNotifyWnd && ::IsWindow(m_hNotifyWnd))
		{
			::SendMessage(m_hNotifyWnd, m_nNotifyMsg, (WPARAM)m_nThreshold, NULL);
		}
	}
}

BOOL CProgressCtrlEx::SetCursor(MOUSE_TYPE mouseType)
{
	switch(mouseType)
	{
	case MOUSE_SIZING_LEFT_TOP:
	case MOUSE_OVER_LEFT_TOP:
	case MOUSE_SIZING_RIGHT_BOTTOM:
	case MOUSE_OVER_RIGHT_BOTTOM:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENWSE));
		return TRUE;
		break;
	case MOUSE_SIZING_BOTTOM:
	case MOUSE_OVER_BOTTOM:
	case MOUSE_SIZING_TOP:
	case MOUSE_OVER_TOP:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENS));
		return TRUE;
		break;
	case MOUSE_SIZING_LEFT_BOTTOM:
	case MOUSE_OVER_LEFT_BOTTOM:
	case MOUSE_SIZING_RIGHT_TOP:
	case MOUSE_OVER_RIGHT_TOP:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENESW));
		return TRUE;
		break;
	case MOUSE_SIZING_LEFT:
	case MOUSE_OVER_LEFT:
	case MOUSE_SIZING_RIGHT:
	case MOUSE_OVER_RIGHT:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		return TRUE;
		break;
	case MOUSE_SIZING_CENTER:
	case MOUSE_OVER_CENTER:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
		return TRUE;
		break;
	default:
		break;
	}

	return FALSE;
}
