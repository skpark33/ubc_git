#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "ReposControl.h"


// CNetworkConfigDialog 대화 상자입니다.

class CNetworkConfigDialog : public CDialog
{
	DECLARE_DYNAMIC(CNetworkConfigDialog)

public:
	CNetworkConfigDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNetworkConfigDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NETWORK_CONFIG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CReposControl	m_reposControl;

	ADAPTER_INFO_LIST	m_adapterInfoList;
	void	GetAdaptersList();
	void	GetAdaptersInfo(bool bReset);

	bool	GetRegistryKey(LPCSTR lpszInterface, LPCTSTR key, DWORD &type, CString &strValue, DWORD &dwValue);
	bool	GetInterfaceName(LPCSTR lpszInterface, LPCTSTR key, DWORD &type, CString &strValue, DWORD &dwValue);
	bool	GetNetworkCardKey(LPCSTR lpszInterface, LPCTSTR key, DWORD &type, CString &strValue, DWORD &dwValue);

	DWORD	AddrToDWORD(LPCSTR lpszAddress);
	CString	AddrToString(DWORD dwAddress);

	bool	RunCommand();
	CString RunProcess(LPCSTR lpszCommand);

	void	RefreshInformation();

public:

	CStatic		m_staticInterfaceName[2];
	CStatic		m_staticNetworkCardName[2];

	CButton		m_checkDHCPIP[2];
	CButton		m_checkStaticIP[2];

	CStatic		m_staticIPAddress[2];
	CStatic		m_staticSubnetMask[2];
	CStatic		m_staticDefaultGateway[2];

	CIPAddressCtrl	m_ipIPAddress[2];
	CIPAddressCtrl	m_ipSubnetMask[2];
	CIPAddressCtrl	m_ipDefaultGateway[2];

	CButton		m_checkDHCPDNS[2];
	CButton		m_checkStaticDNS[2];

	CStatic		m_staticMainDNS[2];
	CStatic		m_staticSubDNS[2];

	CIPAddressCtrl	m_ipMainDNS[2];
	CIPAddressCtrl	m_ipSubDNS[2];

	afx_msg void OnBnClickedCheckDhcpIp();
	afx_msg void OnBnClickedCheckStaticIp();
	afx_msg void OnBnClickedCheckDhcpDns();
	afx_msg void OnBnClickedCheckStaticDns();

	afx_msg void OnBnClickedCheckDhcpIp2();
	afx_msg void OnBnClickedCheckStaticIp2();
	afx_msg void OnBnClickedCheckDhcpDns2();
	afx_msg void OnBnClickedCheckStaticDns2();

	afx_msg void OnBnClickedButtonTest();
	CButton m_btnOK;
	CButton m_btnCancel;
};
