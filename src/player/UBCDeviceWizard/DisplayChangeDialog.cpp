// DisplayChangeDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCDeviceWizard.h"
#include "DisplayChangeDialog.h"


// CDisplayChangeDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDisplayChangeDialog, CDialog)

CDisplayChangeDialog::CDisplayChangeDialog(CWnd* pParent)
	: CDialog(CDisplayChangeDialog::IDD, pParent)
	, m_pParentWnd (pParent)
{
}

CDisplayChangeDialog::~CDisplayChangeDialog()
{
}

void CDisplayChangeDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_ICON, m_staticIcon);
	DDX_Control(pDX, IDC_PROGRESS_REMAIN_TIME, m_progressRemainTime);
}


BEGIN_MESSAGE_MAP(CDisplayChangeDialog, CDialog)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CDisplayChangeDialog 메시지 처리기입니다.

void CDisplayChangeDialog::PostNcDestroy()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::PostNcDestroy();

//	delete this;
}

BOOL CDisplayChangeDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	StartCount();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDisplayChangeDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	KillTimer(1024);
	m_pParentWnd->PostMessage(WM_CHANGE_RESOLUTION);

	CDialog::OnOK();
//	DestroyWindow();
}

void CDisplayChangeDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	KillTimer(1024);
	m_pParentWnd->PostMessage(WM_RESTORE_RESOLUTION);

	CDialog::OnCancel();
//	DestroyWindow();
}

void CDisplayChangeDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	CString msg;

	switch(nIDEvent)
	{
	case 1024:
		m_nRemainTime--;

		if(m_nRemainTime == 0)
		{
			OnCancel();
		}
		else
		{
			m_progressRemainTime.SetPos(m_nRemainTime);
			msg.Format("It will be undo previous resolution after %d seconds.", m_nRemainTime);
			m_progressRemainTime.SetText(msg);
		}
		break;
	}
}

void CDisplayChangeDialog::StartCount()
{
	//
	m_staticIcon.SetIcon( ::LoadIcon(NULL, IDI_QUESTION) );

	//
	m_nRemainTime = 30;

	m_progressRemainTime.SetStyle(PROGRESS_TEXT);
	m_progressRemainTime.SetText("It will be undo previous resolution after 30 seconds.");
	m_progressRemainTime.SetRange(0, 30);
	m_progressRemainTime.SetPos(30);

	//
	SetTimer(1024, 1000, NULL);
}
