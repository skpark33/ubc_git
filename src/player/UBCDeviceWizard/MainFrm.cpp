// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "UBCDeviceWizard.h"
#include "MainFrm.h"
#include "common\libScratch\scratchUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
{
	// TODO: 여기에 멤버 초기화 코드를 추가합니다.
}

CMainFrame::~CMainFrame()
{
}


BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	//cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE | WS_THICKFRAME | WS_SYSMENU;
	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE | WS_BORDER;// | WS_SYSMENU;
	cs.style &= ~(WS_VISIBLE | FWS_ADDTOTITLE);

	return TRUE;
}


// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame 메시지 처리기




int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	//프로그램의 메인 아이콘 변경
	CString strCustomer = GetCoutomerInfo();
	if(strCustomer == CUSTOMER_NARSHA)
	{
		HICON hIcon = (HICON)LoadImage(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_NARSHA), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
		this->SetIcon(hIcon, FALSE);
	}//if

	SetMenu(NULL);

	CString str_title;
	str_title.LoadString(AFX_IDS_APP_TITLE);
	SetWindowText(str_title);

	CenterWindow();
	return 0;
}

void CMainFrame::hide()
{
	ShowWindow(SW_HIDE);

}
void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CFrameWnd::OnShowWindow(bShow, nStatus);
	this->CenterWindow();
}
void CMainFrame::OnClose()
{
	// TODO: Add your message handler code here and/or call default
	scratchUtil::clearInstance();

	CFrameWnd::OnClose();
}
