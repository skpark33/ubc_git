#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "ProgressCtrlEx.h"

// CDisplayChangeDialog 대화 상자입니다.

class CDisplayChangeDialog : public CDialog
{
	DECLARE_DYNAMIC(CDisplayChangeDialog)

public:
	CDisplayChangeDialog(CWnd* pParent);   // 표준 생성자입니다.
	virtual ~CDisplayChangeDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DISPLAY_CHANGE };

protected:
	virtual void PostNcDestroy();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	int		m_nRemainTime;
	CWnd*	m_pParentWnd;

public:

	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CStatic			m_staticIcon;
	//CProgressCtrl	m_progressRemainTime;
	CProgressCtrlEx	m_progressRemainTime;

	void	StartCount();
};
