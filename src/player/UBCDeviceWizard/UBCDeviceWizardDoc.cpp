// UBCDeviceWizardDoc.cpp : CUBCDeviceWizardDoc 클래스의 구현
//

#include "stdafx.h"
#include "UBCDeviceWizard.h"

#include "UBCDeviceWizardDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUBCDeviceWizardDoc

IMPLEMENT_DYNCREATE(CUBCDeviceWizardDoc, CDocument)

BEGIN_MESSAGE_MAP(CUBCDeviceWizardDoc, CDocument)
END_MESSAGE_MAP()


// CUBCDeviceWizardDoc 생성/소멸

CUBCDeviceWizardDoc::CUBCDeviceWizardDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CUBCDeviceWizardDoc::~CUBCDeviceWizardDoc()
{
}

BOOL CUBCDeviceWizardDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CUBCDeviceWizardDoc serialization

void CUBCDeviceWizardDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}


// CUBCDeviceWizardDoc 진단

#ifdef _DEBUG
void CUBCDeviceWizardDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CUBCDeviceWizardDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CUBCDeviceWizardDoc 명령
