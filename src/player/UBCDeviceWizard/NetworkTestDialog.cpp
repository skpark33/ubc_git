// NetworkTestDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCDeviceWizard.h"
#include "NetworkTestDialog.h"
#include "common/libScratch/scProperties.h"


#define		NETWORK_TEST_URL			"http://211.232.57.202:8080/ubchome/NetworkTest.htm"
#define		NARASHA_NETWORK_TEST_URL	"http://211.232.57.202:8080/ubchome/Narsha_NetworkTest.htm"

#define		BACKGROUND_COLOR			RGB(0,0,0)


IMPLEMENT_DYNAMIC(CBackWnd, CWnd)

CBackWnd::CBackWnd() : m_strText("Now connecting to server...") { m_font.CreatePointFont(20*10, "Tahoma"); }
CBackWnd::~CBackWnd() {}
BEGIN_MESSAGE_MAP(CBackWnd, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

void CBackWnd::OnPaint()
{
	CPaintDC dc(this);

	CRect client_rect;
	GetClientRect(client_rect);
	dc.FillSolidRect(client_rect, BACKGROUND_COLOR);
	dc.SetTextColor(RGB(128,128,128));
	dc.SetBkColor(BACKGROUND_COLOR);
	dc.SelectObject(&m_font);
	dc.DrawText(m_strText, client_rect, DT_MODIFYSTRING | DT_CENTER | DT_VCENTER /*| DT_SINGLELINE*/);
}



// CNetworkTestDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNetworkTestDialog, CDialog)

CNetworkTestDialog::CNetworkTestDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CNetworkTestDialog::IDD, pParent)
	, m_bShowMessage (false)
{

}

CNetworkTestDialog::~CNetworkTestDialog()
{
}

void CNetworkTestDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IE, m_webBrowser);
}


BEGIN_MESSAGE_MAP(CNetworkTestDialog, CDialog)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CNetworkTestDialog 메시지 처리기입니다.

BOOL CNetworkTestDialog::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	ciEnv::defaultEnv(ciFalse, "utv1");
	//utvUtil::getInstance()->defaultEnv();

	scIni aIni("..\\data\\UBCConnect.ini");
	ciString testURL = "";
	ciString customerName = GetCoutomerInfo();

	aIni.get("DEVICEWIZARD", "TestURL", testURL, NETWORK_TEST_URL);		
	if(!customerName.empty() && customerName != "UBC")
	{
		ciString buf = customerName + "_NetworkTest";
		ciStringUtil::stringReplace(testURL,"NetworkTest",buf.c_str());
	}
	m_strTestURL = testURL.c_str();
	
/*
	ciProperties scrConfig("DEVICEWIZARD");
	ciString cfgFilename = ciEnv::newEnv("PROJECT_CODE");
	cfgFilename += ".properties";
	if ( scrConfig.load("PROJECT_HOME",
			"config",
			cfgFilename.c_str(),
			"Network") != 0 )
	{
		ciString site_id;
		ciConfigIterator itrCfg = scrConfig.find("TestURL");
		if ( itrCfg != scrConfig.end() )
		{
			site_id = (*itrCfg).second;
			m_strTestURL = site_id.c_str();
		}
	}

	if(m_strTestURL.GetLength() == 0)
	{
		m_strTestURL = NETWORK_TEST_URL;
	}
*/
	CRect rect;
	m_webBrowser.GetWindowRect(rect);
	ScreenToClient(rect);

	m_wndBack.Create(NULL, "BackgroundWnd", WS_CHILD | WS_VISIBLE, rect, this, 0xffff);

	m_webBrowser.MoveWindow(0,0,0,0);

	SetTimer(1023, 1000, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CNetworkTestDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == 1023)
	{
		KillTimer(1023);
		m_webBrowser.Navigate(m_strTestURL, NULL, NULL, NULL, NULL);
	}
}

BEGIN_EVENTSINK_MAP(CNetworkTestDialog, CDialog)
	ON_EVENT(CNetworkTestDialog, IDC_IE, 259, CNetworkTestDialog::DocumentCompleteIe, VTS_DISPATCH VTS_PVARIANT)
END_EVENTSINK_MAP()

void CNetworkTestDialog::DocumentCompleteIe(LPDISPATCH pDisp, VARIANT* URL)
{
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	CString bstr(URL->bstrVal);

	TRACE("=============%s\r\n", m_webBrowser.get_LocationName()); //error -> "Internet Explorer에서 웹 페이지를 표시할 수 없습니다."

	CString buf = bstr;
	buf.Append("   bstr=");
	buf.Append(bstr);
	buf.Append(",\r\nm_strTestUrl=");
	buf.Append(m_strTestURL);
	buf.Append(",\r\nget_LocationName=");
	buf.Append(m_webBrowser.get_LocationName());


	//if( bstr != m_strTestURL || m_webBrowser.get_LocationName().Find("Internet") == 0)
	if(  m_webBrowser.get_LocationName().Find("Internet") == 0)
	{
		if( m_bShowMessage == false )
		{
			//m_wndBack.m_strText = m_webBrowser.get_LocationName();
			m_wndBack.m_strText = buf;
			m_wndBack.Invalidate();

			m_bShowMessage = true;
			::AfxMessageBox("Can not connect to server !!!", MB_ICONSTOP);
		}
	}
	else
	{
		m_wndBack.ShowWindow(SW_HIDE);

		CRect rect;
		m_wndBack.GetWindowRect(rect);
		ScreenToClient(rect);

		m_webBrowser.MoveWindow(rect, FALSE);

		m_webBrowser.ShowWindow(SW_SHOW);
	}
}