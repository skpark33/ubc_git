#pragma once

#include "resource.h"
#include "afxcmn.h"
#include "ProgressCtrlEx.h"

// CHddThresholdDlg 대화 상자입니다.

class CHddThresholdDlg : public CDialog
{
	DECLARE_DYNAMIC(CHddThresholdDlg)

public:
	CHddThresholdDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHddThresholdDlg();

	// Input/Output
	int m_nSelect;			// 0=reserve, 1=immediate
	int m_nHddThreshold;	// 여유공간 하한 값(%)
	CSpinButtonCtrl m_spHddThreshold;
	CProgressCtrlEx m_pgcHddUsage;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HDD_THRESHOLD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedRbSelect();
	afx_msg void OnDeltaposSpinValue(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg HRESULT OnHddThresholdChange(WPARAM wParam, LPARAM lParam);
};
