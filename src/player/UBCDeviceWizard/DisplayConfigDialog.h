#pragma once
#include "afxwin.h"

#include "HoverButton.h"
#include "afxcmn.h"

// CDisplayConfigDialog 대화 상자입니다.

class CDisplayConfigDialog : public CDialog
{
	DECLARE_DYNAMIC(CDisplayConfigDialog)

public:
	CDisplayConfigDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDisplayConfigDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DISPLAY_CONFIG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	SIZE	m_currentResolution;
	bool	ChangeResolution(SIZE size);

	CUIntArray	m_listWidth;
	CUIntArray	m_listHeight;

	bool		m_bRotation;
	bool		m_bFixedAspectRatio;

	void		ResetResolutionList(int type);	// 0=normal, 1=rotation, 2=custom
	void		RefreshStatic();
	void		RefreshButton();

public:

	afx_msg LRESULT	OnChangeResolution(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnRestoreResolution(WPARAM wParam, LPARAM lParam);

	CHoverButton	m_btnWidthLong;
	CHoverButton	m_btnWidthShort;
	CHoverButton	m_btnHeightLong;
	CHoverButton	m_btnHeightShort;
	CHoverButton	m_btnRotation;
	CHoverButton	m_btnAspectRatio;
	CSliderCtrl		m_sliderWidth;
	CSliderCtrl		m_sliderHeight;
	CStatic			m_staticWidth;
	CStatic			m_staticHeight;
	CStatic			m_frameWidth;
	CStatic			m_frameHeight;
	CStatic			m_frameFree;

	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnBnClickedButtonWidthShort();
	afx_msg void OnBnClickedButtonWidthLong();
	afx_msg void OnBnClickedButtonHeightLong();
	afx_msg void OnBnClickedButtonHeightShort();
	afx_msg void OnBnClickedButtonRotation();
	afx_msg void OnBnClickedButtonAspectRatio();
};
