// UBCDeviceWizardView.cpp : CUBCDeviceWizardView 클래스의 구현
//

#include "stdafx.h"
#include "UBCDeviceWizard.h"

#include "UBCDeviceWizardDoc.h"
#include "UBCDeviceWizardView.h"

#include "GridBGDialog.h"
#include "NetworkConfigDialog.h"
#include "NetworkTestDialog.h"
#include "SoundConfigDlg.h"
#include "HddThresholdDlg.h"
#include "scratchUtil.h"

#include "common/libScratch/scPath.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define		ID_OS_LANGE_CHECK		100			//OS 언어를 확인하여 Network 설정 버튼의 활성화를 정하는 타이머 ID
#define		TM_OS_LANGE_CHECK		50			//OS 언어를 확인하여 Network 설정 버튼의 활성화를 정하는 타이머 시간


// CUBCDeviceWizardView

IMPLEMENT_DYNCREATE(CUBCDeviceWizardView, CFormView)

BEGIN_MESSAGE_MAP(CUBCDeviceWizardView, CFormView)
	ON_BN_CLICKED(IDC_BUTTON_VIDEO, &CUBCDeviceWizardView::OnBnClickedButtonVideo)
	ON_BN_CLICKED(IDC_BUTTON_CLEANING, &CUBCDeviceWizardView::OnBnClickedButtonCleaning)
	ON_BN_CLICKED(IDC_BUTTON_NETWORK_TEST, &CUBCDeviceWizardView::OnBnClickedButtonNetworkTest)
	ON_BN_CLICKED(IDC_BUTTON_NETWORK, &CUBCDeviceWizardView::OnBnClickedButtonNetwork)
	ON_BN_CLICKED(IDC_BUTTON_SOUND_CONFIG, &CUBCDeviceWizardView::OnBnClickedButtonSound)
	ON_BN_CLICKED(IDC_BUTTON_MISC, &CUBCDeviceWizardView::OnBnClickedButtonMisc)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG, &CUBCDeviceWizardView::OnBnClickedButtonConfig)
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CUBCDeviceWizardView::OnBnClickedButtonExit)
	ON_WM_TIMER()
END_MESSAGE_MAP()

// CUBCDeviceWizardView 생성/소멸

CUBCDeviceWizardView::CUBCDeviceWizardView()
	: CFormView(CUBCDeviceWizardView::IDD)
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CUBCDeviceWizardView::~CUBCDeviceWizardView()
{
}

void CUBCDeviceWizardView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_VIDEO, m_btnVideo);
	DDX_Control(pDX, IDC_BUTTON_CLEANING, m_btnCleaning);
	DDX_Control(pDX, IDC_BUTTON_NETWORK_TEST, m_btnNetworkTest);
	DDX_Control(pDX, IDC_BUTTON_NETWORK, m_btnNetwork);
	DDX_Control(pDX, IDC_BUTTON_SOUND_CONFIG, m_btnSound);
	DDX_Control(pDX, IDC_BUTTON_MISC, m_btnMisc);
	DDX_Control(pDX, IDC_BUTTON_CONFIG, m_btnConfig);
	DDX_Control(pDX, IDC_BUTTON_EXIT, m_btnExit);
}

BOOL CUBCDeviceWizardView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CUBCDeviceWizardView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	m_btnVideo.LoadBitmap(IDB_DISPLAY, RGB(232,168,192));
	m_btnCleaning.LoadBitmap(IDB_CLEANING, RGB(232,168,192));
	m_btnNetworkTest.LoadBitmap(IDB_NETWORK_TEST, RGB(232,168,192));
	m_btnNetwork.LoadBitmap(IDB_NETWORK, RGB(232,168,192));
	m_btnSound.LoadBitmap(IDB_SOUND_BUTTON, RGB(232,168,192));
	m_btnMisc.LoadBitmap(IDB_MISC, RGB(232,168,192));
	m_btnConfig.LoadBitmap(IDB_CONFIG, RGB(232,168,192));
	m_btnExit.LoadBitmap(IDB_EXIT, RGB(232,168,192));

	SetScrollSizes(MM_TEXT, CSize(1,1));

	if(PRIMARYLANGID(GetSystemDefaultLangID()) != LANG_KOREAN
		&& PRIMARYLANGID(GetSystemDefaultLangID()) != LANG_ENGLISH)
	{
		SetTimer(ID_OS_LANGE_CHECK, TM_OS_LANGE_CHECK, NULL);
	}//if
}


// CUBCDeviceWizardView 진단

#ifdef _DEBUG
void CUBCDeviceWizardView::AssertValid() const
{
	CFormView::AssertValid();
}

void CUBCDeviceWizardView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CUBCDeviceWizardDoc* CUBCDeviceWizardView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCDeviceWizardDoc)));
	return (CUBCDeviceWizardDoc*)m_pDocument;
}
#endif //_DEBUG


// CUBCDeviceWizardView 메시지 처리기

void CUBCDeviceWizardView::OnBnClickedButtonVideo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CGridBGDialog dlg;
	dlg.DoModal();
}

void CUBCDeviceWizardView::OnBnClickedButtonCleaning()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CHddThresholdDlg dlg;
	dlg.DoModal();
}

void CUBCDeviceWizardView::OnBnClickedButtonNetworkTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CNetworkTestDialog dlg;
	dlg.DoModal();
}

void CUBCDeviceWizardView::OnBnClickedButtonNetwork()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CNetworkConfigDialog dlg;
	dlg.DoModal();
}

void CUBCDeviceWizardView::OnBnClickedButtonSound()
{
	CSoundConfigDlg dlg;
	dlg.DoModal();
}

void CUBCDeviceWizardView::OnBnClickedButtonMisc()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strAppPath, strCmd;
	CString strOption = "";
	CString strVal = GetINIValue(_T("UBCInstall.ini"), _T("ROOT"), _T("InstalledVersion"));
	CString strVersion = GetINIValue(_T("UBCInstall.ini"), _T("ROOT"), _T("Version"));
	if(strVal.GetLength() == 0)
	{
#ifdef _DEBUG
		strAppPath = _UBC_CD("C:\\SQISoft\\UTV1.0\\execute\\UBCExtraInstall.exe");
#else if
		strAppPath = GetAppPath();
		strAppPath += "UBCExtraInstall.exe";
#endif
	}
	else if( atoi(strVersion) <= 1 )
	{
		strAppPath = GetAppPath();
		strAppPath += "UBCInstall.exe";
	}
	else if( atoi(strVersion) >= 2 )
	{
		strAppPath = GetAppPath();
		strAppPath += "UBCInstaller.exe";
	}//if

	string strHost, strMac, strEdition;
	scratchUtil* aUtil = scratchUtil::getInstance();
	aUtil->readAuthFile(strHost, strMac, strEdition);
	if(strEdition.length() != 0)
	{
		strAppPath += " +";
		strAppPath += strEdition.c_str();
	}//if	

	//if(!system(strAppPath))
	//{
	//}//if

	STARTUPINFO stStartupInfo = {0}; 
	stStartupInfo.cb = sizeof(STARTUPINFO);
	PROCESS_INFORMATION stProcessInfo; 
	BOOL bRet =  CreateProcess(//strAppPath,
								NULL,
								(LPSTR)(LPCSTR)strAppPath,
								//NULL,
								NULL,
								NULL,
								FALSE,
								0,
								NULL,
								GetAppPath(),
								&stStartupInfo,
								&stProcessInfo);

	if(!bRet)
	{
		system(strAppPath);
	}
	else
	{
		CloseHandle(stProcessInfo.hProcess);
		CloseHandle(stProcessInfo.hThread);
	}//if

}

void CUBCDeviceWizardView::OnBnClickedButtonConfig()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strAppPath;
	strAppPath = _UBC_CD("C:\\SQISoft\\UTV1.0\\execute\\UBCConfigWizard.exe");

	PROCESS_INFORMATION stProcessInfo = {0}; 
	STARTUPINFO stStartupInfo = {0}; 
	stStartupInfo.cb = sizeof(STARTUPINFO);
	BOOL bRet =  CreateProcess(//strAppPath,
								NULL,
								(LPSTR)(LPCSTR)strAppPath,
								//NULL,
								NULL,
								NULL,
								FALSE,
								0,
								NULL,
								GetAppPath(),
								&stStartupInfo,
								&stProcessInfo);

	if(!bRet)
	{
		system(strAppPath);
	}
	else
	{
		CloseHandle(stProcessInfo.hProcess);
		CloseHandle(stProcessInfo.hThread);
	}//if

}

void CUBCDeviceWizardView::OnBnClickedButtonExit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	::AfxGetMainWnd()->PostMessage(WM_CLOSE);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 ini 파일의 값을 반환한다. \n
/// @param (CString) strFileName : (in) ini파일 이름
/// @param (CString) strSection : (in) ini파일의 section 
/// @param (CString) strKey : (in) 구하려는 값의 key값
/// @return <형: CString> \n
///			<key값의 value> \n
/////////////////////////////////////////////////////////////////////////////////
CString CUBCDeviceWizardView::GetINIValue(CString strFileName, CString strSection, CString strKey)
{
	char cBuffer[BUF_SIZE] = { 0x00 };
	CString strPath = GetAppPath();
	strPath += _T("data\\");
	strPath += strFileName;
	GetPrivateProfileString(strSection, strKey, _T(""), cBuffer, BUF_SIZE, strPath);

	return CString(cBuffer);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 실행프로그램의 경로를 반환 \n
/// @return <형: LPCTSTR> \n
///			<실행프로그램의 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR CUBCDeviceWizardView::GetAppPath(void)
{
	static CString strAppPath = _T("");

	if(strAppPath.GetLength() == 0)
	{
		CString strPath;
		TCHAR szModule[MAX_PATH];
		::ZeroMemory(szModule, MAX_PATH);
		::GetModuleFileName(NULL, szModule, MAX_PATH);

		TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
		_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);
		strPath = szDrive;
		strPath += szPath;
		strAppPath = strPath;
	}//if

	return strAppPath;
}


void CUBCDeviceWizardView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(nIDEvent == ID_OS_LANGE_CHECK)
	{
		KillTimer(ID_OS_LANGE_CHECK);
		CWnd* pBtn = (CWnd*)GetDlgItem(IDC_BUTTON_NETWORK);
		pBtn->EnableWindow(FALSE);
	}//if

	CFormView::OnTimer(nIDEvent);
}
