#pragma once
#include "WebBrowser.h"



//
class CBackWnd : public CWnd
{
	DECLARE_DYNAMIC(CBackWnd)
public:
	CBackWnd();
	virtual ~CBackWnd();
protected:
	DECLARE_MESSAGE_MAP()
	CFont		m_font;
public:
	afx_msg void OnPaint();
	CString		m_strText;
};



// CNetworkTestDialog 대화 상자입니다.

class CNetworkTestDialog : public CDialog
{
	DECLARE_DYNAMIC(CNetworkTestDialog)

public:
	CNetworkTestDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNetworkTestDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NETWORK_TEST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	DECLARE_EVENTSINK_MAP()

	void DocumentCompleteIe(LPDISPATCH pDisp, VARIANT* URL);

	bool		m_bShowMessage;
	CBackWnd	m_wndBack;
	CString		m_strTestURL;

public:

	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CWebBrowser m_webBrowser;
};
