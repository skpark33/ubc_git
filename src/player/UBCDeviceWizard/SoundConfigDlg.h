#pragma once
#include "afxwin.h"
#include <Mmsystem.h> // Volume Control
#include "afxcmn.h"

#pragma comment(lib, "Winmm.lib")

// CSoundConfigDlg dialog

class CSoundConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CSoundConfigDlg)

public:
	CSoundConfigDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSoundConfigDlg();

// Dialog Data
	enum { IDD = IDD_SOUND_CONFIG };

	CButton m_rdoDisA;
	CButton m_rdoDisB;
	CButton m_chkMute;
	CStatic m_sttVolume;
	CSliderCtrl m_sldVolume;

private:
	HMIXER m_hMixer;
	CString m_strDstLineName;
	CString m_strVolumeControlName;
	LONG	m_dwMinimum;
	LONG	m_dwMaximum;
	LONG	m_dwVolumeControlID;
	LONG	m_dwMuteControlID;
	BOOL	m_bMute;
	BOOL	m_bDisA;
	BOOL	m_bDisB;
	LONG	m_lVolume;

	BOOL InitSound();
	BOOL UninitSound();
	BOOL InitVolumeControl();
	BOOL GetVolumeValue(LONG &volume);
	BOOL SetVolumeValue(LONG volume);
	BOOL GetMuteValue(BOOL &mute);
	BOOL SetMuteValue(BOOL mute);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnClose();
	afx_msg void OnBnClickedCheckMute();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedOk();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};
