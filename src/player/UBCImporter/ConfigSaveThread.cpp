// ConfigSaveThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCImporter.h"
#include "ConfigSaveThread.h"

#include "UBCImporterDlg.h"


// CConfigSaveThread

IMPLEMENT_DYNCREATE(CConfigSaveThread, CWinThread)

CConfigSaveThread::CConfigSaveThread()
{
	m_pParent = NULL;
}

CConfigSaveThread::~CConfigSaveThread()
{
}

BOOL CConfigSaveThread::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CConfigSaveThread::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CConfigSaveThread, CWinThread)
END_MESSAGE_MAP()


// CConfigSaveThread 메시지 처리기입니다.

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Set parent params \n
/// @param (void*) pParent : (in) thread owner window pointer
/////////////////////////////////////////////////////////////////////////////////
void CConfigSaveThread::SetThreadParam(void* pParent)
{
	m_pParent = pParent;
}

int CConfigSaveThread::Run()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CUBCImporterDlg* pParent = (CUBCImporterDlg*)m_pParent;
	if(pParent->CopyConfig())
	{
		pParent->PostMessage(WM_SAVE_CONFIG_COMPELETE, 1, 0);
	}
	else
	{
		pParent->PostMessage(WM_SAVE_CONFIG_COMPELETE, 0, 0);
	}//if

	return 0;

	//return CWinThread::Run();
}
