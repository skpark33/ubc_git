// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// UBCImporter.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"



CString ToString(int nValue)
{
	CString str;
	str.Format("%d", nValue);

	return str;
}

CString ToString(double fValue)
{
	CString str;
	str.Format("%.3f", fValue);

	return str;
}

CString ToString(ULONG ulValue)
{
	CString str;
	str.Format("%ld", ulValue);

	return str;
}

CString ToString(ULONGLONG ulValue)
{
	CString str;
	str.Format("%I64d", ulValue);

	return str;
}

CString ToMoneyTypeString(LPCTSTR lpszStr)
{
	CString str = lpszStr;

	int nLength = str.Find(_T("."));
	if(nLength<0)
        nLength = str.GetLength();

	while( nLength > 3 )
	{
		nLength = nLength - 3;
		str.Insert( nLength, ',' );
	}

	return str;
}

CString ToMoneyTypeString(int value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%d"), value);
	return ToMoneyTypeString(buf);
}

CString GetTimeZoneString(int nTimeZone)
{
	switch(nTimeZone)
	{
	case 0: return "All Day";
	case 7: return "0 ~ 7h";
	case 1: return "7 ~ 9h";
	case 2: return "9 ~ 12h";
	case 3: return "12 ~ 13h";
	case 4: return "13 ~ 18h";
	case 5: return "18 ~ 22h";
	case 6: return "22 ~ 24h";
	}

	return "";
}

CString GetContentsTypeString(int nContentsType)
{
	switch(nContentsType)
	{
	case 0: return "Video";
	case 1:	return "Ticker";
	case 2:	return "Image";
	case 3:	return "Promotion";
	case 4:	return "TV";
	case 5:	return "Ticker2";
	case 6:	return "Phone";
	case 7:	return "WebPage";
	case 8:	return "Flash";
	case 9:	return "WebCam";
	case 10:return "RSS";
	case 11:return "Typing";
	}

	return "";
}

COLORREF GetColorFromString(LPCSTR lpszColor)
{
	COLORREF rgb;
	if(*lpszColor == '#')
		sscanf(lpszColor+1, "%x", &rgb);
	else
		sscanf(lpszColor, "%x", &rgb);

	COLORREF red	= rgb & 0x00FF0000;
	COLORREF green	= rgb & 0x0000FF00;
	COLORREF blue	= rgb & 0x000000FF;

	red = red >> 16;
	blue = blue << 16;

	return red | green | blue;
}

CString GetColorFromString(COLORREF rgb)
{
	COLORREF red	= rgb & 0x000000FF;
	COLORREF green	= rgb & 0x0000FF00;
	COLORREF blue	= rgb & 0x00FF0000;

	green = green >> 8;
	blue = blue >> 16;

	CString ret;
	ret.Format("#%02X%02X%02X", red, green, blue);

	return ret;
}

void GetColorRGB(COLORREF rgb, int& r, int& g, int& b)
{
	r = rgb & 0x000000FF;
	g = rgb & 0x0000FF00;
	b = rgb & 0x00FF0000;

	g >>= 8;
	b >>= 16;
}

LPCTSTR GetDataPath()
{
	static CString config_path = "";

	if(config_path.GetLength() == 0)
	{
//		CString path = ciEnv::newEnv("PROJECT_HOME");

//		config_path.Format("%s\\config\\%s.ini", path, ::GetHostName());
	}

	return config_path;
}

LPCTSTR GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Customer Info를 반환한다. \n
/// @return <형: CString> \n
///			<CString: CustomerInfo name> \n
/////////////////////////////////////////////////////////////////////////////////
CString GetCoutomerInfo()
{
	CString strUBCVariables = "";
	strUBCVariables.Format("%sdata\\UBCVariables.ini", GetAppPath());

	char cBuffer[BUF_SIZE] = { 0x00 };

	GetPrivateProfileString("CUSTOMER_INFO", "NAME", "", cBuffer, BUF_SIZE, strUBCVariables);
	return cBuffer;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램이 구동된 드라이브를 반환한다. \n
/// @return <형: LPCSTR> \n
///			프로그램이 구동된 드라이브 문자열 \n
/////////////////////////////////////////////////////////////////////////////////
LPCSTR GetAppDrive()
{
	static CString	strAppDrive = _T("");

	if(strAppDrive.GetLength() == 0)
	{
		CString strDrv;
		TCHAR szModule[MAX_PATH];
		::ZeroMemory(szModule, MAX_PATH);
		::GetModuleFileName(NULL, szModule, MAX_PATH);

		TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
		_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);
		
		strDrv = szDrive;
		strDrv.Append("\\");
		strAppDrive = strDrv;
	}//if

	return strAppDrive;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 실행중인 UBC를 중단 시킨다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool StopUBC()
{
	CString strCmd, strExcute;
	STARTUPINFO stStartupInfo = {0}; 
	PROCESS_INFORMATION stProcessInfo;
	BOOL bRet;
	
	strExcute.Format("%s%s", GetAppPath(), "ubckill.bat");

	

	stStartupInfo.cb = sizeof(STARTUPINFO);
	bRet =  CreateProcess(strExcute,
						NULL,
						NULL,
						NULL,
						FALSE,
						0,
						NULL,
						GetAppPath(),
						&stStartupInfo,
						&stProcessInfo);
	
	if(!bRet)
	{
		DWORD dwErrNo = GetLastError();

		LPVOID lpMsgBuf = NULL;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
					| FORMAT_MESSAGE_IGNORE_INSERTS 
					| FORMAT_MESSAGE_FROM_SYSTEM     
					,	NULL
					, dwErrNo
					, 0
					, (LPTSTR)&lpMsgBuf
					, 0
					, NULL);

		CString strErrMsg;
		if(!lpMsgBuf)
		{
			strErrMsg.Format(_T("Unknow Error - CreateProcess [%d]\r\n"), dwErrNo);
		}
		else
		{
			strErrMsg.Format(_T("%s [%d]\r\n"), (LPCTSTR)lpMsgBuf, dwErrNo);
		}//if

		LocalFree( lpMsgBuf );
		TRACE(strErrMsg);

		return false;
	}//if

	if(::WaitForSingleObject(stProcessInfo.hProcess, 3000) == WAIT_TIMEOUT)
	{
		TRACE("Time out wiating process\r\n");
	}//if

	return true;
}
bool StartUBC()
{
	CString strCmd, strExcute;
	STARTUPINFO stStartupInfo = {0}; 
	PROCESS_INFORMATION stProcessInfo;
	BOOL bRet;
	
	strExcute.Format("%s%s", GetAppPath(), "SelfAutoUpdate.exe");
	strCmd = " +log +ignore_env";

	stStartupInfo.cb = sizeof(STARTUPINFO);
	bRet =  CreateProcess(strExcute,
						(LPSTR)(LPCTSTR)strCmd,
						NULL,
						NULL,
						FALSE,
						0,
						NULL,
						GetAppPath(),
						&stStartupInfo,
						&stProcessInfo);
	
	if(!bRet)
	{
		DWORD dwErrNo = GetLastError();

		LPVOID lpMsgBuf = NULL;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
					| FORMAT_MESSAGE_IGNORE_INSERTS 
					| FORMAT_MESSAGE_FROM_SYSTEM     
					,	NULL
					, dwErrNo
					, 0
					, (LPTSTR)&lpMsgBuf
					, 0
					, NULL);

		CString strErrMsg;
		if(!lpMsgBuf)
		{
			strErrMsg.Format(_T("Unknow Error - CreateProcess [%d]\r\n"), dwErrNo);
		}
		else
		{
			strErrMsg.Format(_T("%s [%d]\r\n"), (LPCTSTR)lpMsgBuf, dwErrNo);
		}//if

		LocalFree( lpMsgBuf );
		TRACE(strErrMsg);

		return false;
	}//if

	if(::WaitForSingleObject(stProcessInfo.hProcess, 3000) == WAIT_TIMEOUT)
	{
		TRACE("Time out wiating process\r\n");
	}//if

	return true;
}
