#include "stdafx.h"
#include <afxmt.h>
#include "resource.h"
#include "DataContainer.h"
#include "Schedule.h"
#include "Enviroment.h"
#include "PreventChar.h"
#include "libXmlParser.h"

// Modified by 정운형 2009-01-07 오후 5:59
// 변경내역 :  CLI 연동 수정
//#include "resource.h"
//#include "MainFrm.h"
// Modified by 정운형 2009-01-07 오후 5:59
// 변경내역 :  CLI 연동 수정

#define DC_BUFF_SIZE		102400

#ifdef _UBC_IMPORTER_
	#define CONTENTS_VIDEO		0		// Schedule.h의 enum 값을 참조하여 선언할것.
	#define CONTENTS_ETC		17		// Schedule.h의 enum 값을 참조하여 선언할것.
	#define CONTENTS_WIZARD     18		// Schedule.h의 enum 값을 참조하여 선언할것.
#endif


// Ini에서 읽어 올때 값의 앞부분에 공백이 있는 경우 공백이 제거된 상태로 값이 구해지기 때문에
// 앞부분이 공백이 있는 문자열은 문자열앞에 | 문자를 추가하여 로드하고 저장하도록 한다.
CString ForSaveComment(CString strRawValue)
{
	if(strRawValue.GetLength() > 0 && strRawValue[0] == ' ') strRawValue = "|"+strRawValue;
	return strRawValue;
}

CString ForLoadComment(CString strRawValue)
{
	if( strRawValue.GetLength() > 1 && strRawValue.Left(2) == "| ") strRawValue.Delete(0);
	return strRawValue;
}

CString CreateGUID()
{
	GUID guid;
	WCHAR szPackID[40];

	if(CoCreateGuid(&guid) != S_OK)
	{
		//_tprintf(_T("GUID 생성 실패"));
		return _T("");
	}

	if(!StringFromGUID2(guid, szPackID, 39))
	{
		//_tprintf(_T("GUID 변환 실패"));
		return _T("");
	}

	//_tprintf(_T("%s"), szPackID);

	CString strGUID;
	strGUID = szPackID;
	//strGUID.Remove('-');
	//strGUID.Remove('{');
	//strGUID.Remove('}');
	//strGUID = "G_" + sGUID_plane;

	return strGUID;
}

////////////////////////////////////////////////////////////////////////////////
// CONTENTS_INFO
////////////////////////////////////////////////////////////////////////////////
bool CONTENTS_INFO::Save(LPCTSTR lpszFullPath, LPCTSTR lpszSaveID, bool bNew)
{
	// 0000911: 마법사컨텐츠의 저장되는 XML내에 마법사컨텐츠 자신의 컨텐츠ID를 넣어서 저장하도록함
	if(CONTENTS_WIZARD == nContentsType && !strWizardXML.IsEmpty())
	{
		CXmlParser xmlWizardData;

		if( xmlWizardData.LoadXML(strWizardXML)    == XMLPARSER_SUCCESS &&
			xmlWizardData.SetXPath("/FlashWizard") == XMLPARSER_SUCCESS )
		{
			if(xmlWizardData.SetAttribute("id", strId) != XMLPARSER_SUCCESS)
			{
				xmlWizardData.NewAttribute("id");
				xmlWizardData.SetAttribute("id", strId);
			}

			// xml 저장
			CString strXml = xmlWizardData.GetXML("/");
			strXml.Replace("\t",_T(""));
			strXml.Replace("\r",_T(""));
			strXml.Replace("\n",_T(""));
			strWizardXML = strXml;
		}
	}

	::WritePrivateProfileString(lpszSaveID, "contentsId"  , strId                    , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "contentsType", ::ToString(nContentsType), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "contentsName", strContentsName          , lpszFullPath);

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
/*		if(bNew){
			strSeverLocation = "/contents/";
			strSeverLocation += GetEnvPtr()->m_strPackage;
			strSeverLocation += "/";
		}
		else*/
		if(strSeverLocation.IsEmpty() || strSeverLocation.MakeLower().Compare("\\contents\\enc\\")==0)
		{
			strSeverLocation = "/contents/";
			strSeverLocation += GetEnvPtr()->m_strPackage;
			strSeverLocation += "/";
		}
		::WritePrivateProfileString(lpszSaveID, "location", strSeverLocation, lpszFullPath);
	}
	else
	{
		::WritePrivateProfileString(lpszSaveID, "location", "\\contents\\ENC\\", lpszFullPath);
	}

	::WritePrivateProfileString(lpszSaveID, "filename"          , strFilename                  , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "runningTime"       , ::ToString(nRunningTime)     , lpszFullPath);
																							 
	::WritePrivateProfileString(lpszSaveID, "volume"            , ::ToString(nFilesize)        , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "contentsState"     , ::ToString(nContentsState)   , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "promotionValueList", strPromotionValueList        , lpszFullPath);

	::WritePrivateProfileString(lpszSaveID, "comment1"          , ForSaveComment(strComment[0]), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "comment2"          , ForSaveComment(strComment[1]), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "comment3"          , ForSaveComment(strComment[2]), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "comment4"          , ForSaveComment(strComment[3]), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "comment5"          , ForSaveComment(strComment[4]), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "comment6"          , ForSaveComment(strComment[5]), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "comment7"          , ForSaveComment(strComment[6]), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "comment8"          , ForSaveComment(strComment[7]), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "comment9"          , ForSaveComment(strComment[8]), lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "comment10"         , ForSaveComment(strComment[9]), lpszFullPath);

	::WritePrivateProfileString(lpszSaveID, "bgColor"           , strBgColor                   , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "fgColor"           , strFgColor                   , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "font"              , strFont                      , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "fontSize"          , ::ToString(nFontSize)        , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "playSpeed"         , ::ToString(nPlaySpeed)       , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "soundVolume"       , ::ToString(nSoundVolume)     , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "direction"         , ::ToString(nDirection)       , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "align"             , ::ToString(nAlign)           , lpszFullPath);

	::WritePrivateProfileString(lpszSaveID, "width"             , ::ToString(nWidth)           , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "height"            , ::ToString(nHeight)          , lpszFullPath);
	::WritePrivateProfileString(lpszSaveID, "currentComment"    , ::ToString(nCurrentComment)  , lpszFullPath);

	// 0000611: 플레쉬 컨텐츠 마법사
	::WritePrivateProfileString(lpszSaveID, "wizardXML"         , strWizardXML                 , lpszFullPath);	// wizard 용 XML string
	::WritePrivateProfileString(lpszSaveID, "wizardFiles"       , strWizardFiles               , lpszFullPath);	// wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

	return true;
}

bool CONTENTS_INFO::Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID)
{
	char buf[DC_BUFF_SIZE];

	::GetPrivateProfileString(lpszLoadID, "contentsId"        , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strId           = buf;
	::GetPrivateProfileString(lpszLoadID, "contentsType"      , "0", buf, DC_BUFF_SIZE, lpszFullPath); nContentsType   = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "contentsName"      , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strContentsName = buf;
	strLocation = "\\Contents\\ENC\\";													
	::GetPrivateProfileString(lpszLoadID, "location"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strSeverLocation = buf;
	::GetPrivateProfileString(lpszLoadID, "filename"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strFilename      = buf;
	::GetPrivateProfileString(lpszLoadID, "runningTime"       , "0", buf, DC_BUFF_SIZE, lpszFullPath); nRunningTime     = atol(buf);
	::GetPrivateProfileString(lpszLoadID, "volume"            , "0", buf, DC_BUFF_SIZE, lpszFullPath); nFilesize        = _atoi64(buf);
	::GetPrivateProfileString(lpszLoadID, "contentsState"     , "0", buf, DC_BUFF_SIZE, lpszFullPath); nContentsState   = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "promotionValueList", _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strPromotionValueList = buf;
	::GetPrivateProfileString(lpszLoadID, "comment1"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[0]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment2"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[1]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment3"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[2]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment4"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[3]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment5"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[4]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment6"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[5]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment7"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[6]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment8"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[7]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment9"          , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[8]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment10"         , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[9]    = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "bgColor"           , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strBgColor       = buf;
	::GetPrivateProfileString(lpszLoadID, "fgColor"           , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strFgColor       = buf;
	::GetPrivateProfileString(lpszLoadID, "font"              , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strFont          = buf;
	::GetPrivateProfileString(lpszLoadID, "fontSize"          , "0", buf, DC_BUFF_SIZE, lpszFullPath); nFontSize        = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "playSpeed"         , "0", buf, DC_BUFF_SIZE, lpszFullPath); nPlaySpeed       = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "soundVolume"       , "0", buf, DC_BUFF_SIZE, lpszFullPath); nSoundVolume     = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "direction"         , "0", buf, DC_BUFF_SIZE, lpszFullPath); nDirection       = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "align"             , "5", buf, DC_BUFF_SIZE, lpszFullPath); nAlign           = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "width"             , "0", buf, DC_BUFF_SIZE, lpszFullPath); nWidth           = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "height"            , "0", buf, DC_BUFF_SIZE, lpszFullPath); nHeight          = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "currentComment"    , "2", buf, DC_BUFF_SIZE, lpszFullPath); nCurrentComment  = atoi(buf);

	// 0000611: 플레쉬 컨텐츠 마법사
	::GetPrivateProfileString(lpszLoadID, "wizardXML"         , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strWizardXML     = buf;	// wizard 용 XML string
	::GetPrivateProfileString(lpszLoadID, "wizardFiles"       , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strWizardFiles   = buf;	// wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

	// file size 는 열때마다 계산한다. copy 등에의해 수정 한경우도 있으므로
	CFileFind ff;
	CString szPath;
	szPath.Format("%s%s%s", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONTENTS_PATH, strFilename);
	if(ff.FindFile(szPath))
	{
		ff.FindNextFile();
		nFilesize = ff.GetLength();
	}
	ff.Close();

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// PLAYCONTENTS_INFO
////////////////////////////////////////////////////////////////////////////////
bool PLAYCONTENTS_INFO::Save(LPCTSTR lpszFullPath, CONTENTS_INFO* pContentsInfo, bool bNew)
{
	CString app_name;
	app_name.Format("Template%sFrame%sSchedule%s", strTemplateId, strFrameId, strId);	// Template77Frame02Schedule7702_0001

	::WritePrivateProfileString(app_name, "templateId"      , strTemplateId         , lpszFullPath);
	::WritePrivateProfileString(app_name, "frameId"         , strFrameId            , lpszFullPath);
	::WritePrivateProfileString(app_name, "scheduleId"      , strId                 , lpszFullPath);
	::WritePrivateProfileString(app_name, "startDate"       , ::ToString(nStartDate), lpszFullPath);
	::WritePrivateProfileString(app_name, "endDate"         , ::ToString(nEndDate  ), lpszFullPath);
	::WritePrivateProfileString(app_name, "startTime"       , strStartTime          , lpszFullPath);
	::WritePrivateProfileString(app_name, "endTime"         , strEndTime            , lpszFullPath);
	::WritePrivateProfileString(app_name, "playOrder"       , ::ToString(nPlayOrder), lpszFullPath);
	::WritePrivateProfileString(app_name, "timeScope"       , ::ToString(nTimeScope), lpszFullPath);
	::WritePrivateProfileString(app_name, "openningFlag"    , "1"                   , lpszFullPath);
	::WritePrivateProfileString(app_name, "priority"        , ::ToString(nPriority) , lpszFullPath);
	::WritePrivateProfileString(app_name, "castingState"    , "3"                   , lpszFullPath);
	::WritePrivateProfileString(app_name, "operationalState", "1"                   , lpszFullPath);
	::WritePrivateProfileString(app_name, "adminState"      , "1"                   , lpszFullPath);
	::WritePrivateProfileString(app_name, "parentScheduleId", strParentPlayContentsId, lpszFullPath);
	::WritePrivateProfileString(app_name, "touchTime"       , strTouchTime          , lpszFullPath);

	if(pContentsInfo)
		pContentsInfo->Save(lpszFullPath, app_name, bNew);

	return true;
}

bool PLAYCONTENTS_INFO::Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID)
{
	char buf[DC_BUFF_SIZE];

	::GetPrivateProfileString(lpszLoadID, "templateId"      , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strTemplateId   = buf;
	::GetPrivateProfileString(lpszLoadID, "frameId"         , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strFrameId      = buf;
	::GetPrivateProfileString(lpszLoadID, "scheduleId"      , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strId           = buf;
	::GetPrivateProfileString(lpszLoadID, "startDate"       , "0", buf, DC_BUFF_SIZE, lpszFullPath); nStartDate          = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "endDate"         , "0", buf, DC_BUFF_SIZE, lpszFullPath); nEndDate            = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "startTime"       , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strStartTime    = buf;
	::GetPrivateProfileString(lpszLoadID, "endTime"         , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strEndTime      = buf;
	::GetPrivateProfileString(lpszLoadID, "playOrder"       , "0", buf, DC_BUFF_SIZE, lpszFullPath); nPlayOrder          = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "timeScope"       , "0", buf, DC_BUFF_SIZE, lpszFullPath); nTimeScope          = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "priority"        , "3", buf, DC_BUFF_SIZE, lpszFullPath); nPriority           = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "parentScheduleId", _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strParentPlayContentsId = buf;
	::GetPrivateProfileString(lpszLoadID, "touchTime"       , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strTouchTime    = buf;
	::GetPrivateProfileString(lpszLoadID, "contentsId"      , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strContentsId   = buf;

	if(!strContentsId.IsEmpty())
	{
		CONTENTS_INFO* pContentsInfo = CDataContainer::getInstance()->GetContents(strContentsId);
		if(pContentsInfo == NULL)
		{
			pContentsInfo = new CONTENTS_INFO;
			pContentsInfo->Load(lpszFullPath, lpszLoadID);

			char szTemp[DC_BUFF_SIZE];
			if(!CDataContainer::getInstance()->AddContents(pContentsInfo, szTemp))
			{
				CString strTemp;
				strTemp.Format(DATACONTAINER_MSG003, strContentsId, szTemp);
//				AfxMessageBox(strTemp, MB_ICONWARNING);
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// FRAME_INFO
////////////////////////////////////////////////////////////////////////////////
bool FRAME_INFO::AddPlayContents(PLAYCONTENTS_INFO* pInPlayContentsInfo)
{
	pInPlayContentsInfo->strTemplateId = strTemplateId;
	pInPlayContentsInfo->strFrameId = strId;
	CString strTmpID = pInPlayContentsInfo->strId;

	pInPlayContentsInfo->strId.Format("%s%s_%s", pInPlayContentsInfo->strTemplateId, pInPlayContentsInfo->strFrameId, strTmpID);

	if(pInPlayContentsInfo->bIsDefaultPlayContents)
		arCyclePlayContentsList.Add(pInPlayContentsInfo);
	else
		arTimePlayContentsList.Add(pInPlayContentsInfo);

	return true;
}

bool FRAME_INFO::DeletePlayContents(PLAYCONTENTS_INFO* pInPlayContentsInfo)
{
	int nCount = arCyclePlayContentsList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)arCyclePlayContentsList.GetAt(i);
		if(pInPlayContentsInfo == pPlayContentsInfo)
		{
			delete pPlayContentsInfo;
			arCyclePlayContentsList.RemoveAt(i);
			return true;
		}
	}

	nCount = arTimePlayContentsList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)arTimePlayContentsList.GetAt(i);
		if(pInPlayContentsInfo == pPlayContentsInfo)
		{
			delete pPlayContentsInfo;
			arTimePlayContentsList.RemoveAt(i);
			return true;
		}
	}

	return false;
}

void FRAME_INFO::DeletePlayContentsList()
{
	int nCount = arCyclePlayContentsList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)arCyclePlayContentsList.GetAt(i);
		delete pPlayContentsInfo;
	}
	arCyclePlayContentsList.RemoveAll();

	//
	nCount = arTimePlayContentsList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)arTimePlayContentsList.GetAt(i);
		delete pPlayContentsInfo;
	}
	arTimePlayContentsList.RemoveAll();
}

bool FRAME_INFO::Save(LPCTSTR lpszFullPath, PLAYCONTENTS_INFO_LIST* pCyclePlayContentsInfoList, PLAYCONTENTS_INFO_LIST* pTimePlayContentsInfoList)
{
	TCHAR szDrive[_MAX_DRIVE] = { 0x00 };
	TCHAR szDir  [_MAX_DIR  ]	= { 0x00 };
	TCHAR szFname[_MAX_FNAME]	= { 0x00 };
	TCHAR szExt  [_MAX_EXT  ]	= { 0x00 };

	_tsplitpath(lpszFullPath, szDrive, szDir, szFname, szExt);
	CString strHostName = szFname;

	CString app_name;
	app_name.Format("Template%sFrame%s", strTemplateId, strId);	// Template77Frame02

	::WritePrivateProfileString(app_name, "templateId"     , strTemplateId                      , lpszFullPath);
	::WritePrivateProfileString(app_name, "frameId"        , strId                              , lpszFullPath);
	::WritePrivateProfileString(app_name, "grade"          , ::ToString(nGrade         )        , lpszFullPath);
	::WritePrivateProfileString(app_name, "width"          , ::ToString(rcRect.Width() )        , lpszFullPath);
	::WritePrivateProfileString(app_name, "height"         , ::ToString(rcRect.Height())        , lpszFullPath);
	::WritePrivateProfileString(app_name, "x"              , ::ToString(rcRect.left    )        , lpszFullPath);
	::WritePrivateProfileString(app_name, "y"              , ::ToString(rcRect.top     )        , lpszFullPath);
	::WritePrivateProfileString(app_name, "isPIP"          , ::ToString(bIsPIP         )        , lpszFullPath);
	::WritePrivateProfileString(app_name, "zIndex"         , ::ToString(nZIndex        )        , lpszFullPath);
	::WritePrivateProfileString(app_name, "alpha"          , ::ToString(nAlpha         )        , lpszFullPath);
	::WritePrivateProfileString(app_name, "isTV"           , ::ToString(bIsTV          )        , lpszFullPath);
	::WritePrivateProfileString(app_name, "borderStyle"    , strBorderStyle                     , lpszFullPath);
	::WritePrivateProfileString(app_name, "borderThickness", ::ToString(nBorderThickness)       , lpszFullPath);
	::WritePrivateProfileString(app_name, "borderColor"    , ::GetColorFromString(crBorderColor), lpszFullPath);
	::WritePrivateProfileString(app_name, "cornerRadius"   , ::ToString(nCornerRadius)          , lpszFullPath);
	::WritePrivateProfileString(app_name, "comment1"       , ForSaveComment(strComment[0])      , lpszFullPath); // 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
	::WritePrivateProfileString(app_name, "comment2"       , ForSaveComment(strComment[1])      , lpszFullPath);
	::WritePrivateProfileString(app_name, "comment3"       , ForSaveComment(strComment[2])      , lpszFullPath);

	CString strIdList;
	if(pCyclePlayContentsInfoList)
	{
		for(int i=0; i<pCyclePlayContentsInfoList->GetCount(); i++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = pCyclePlayContentsInfoList->GetAt(i);
			if(strIdList.GetLength() != 0)
			{
				strIdList.Append(",");
			}

			if(pPlayContentsInfo->strId.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				// 0000737: 패키지 INI 파일에 패키지 ID길이 가 계속 늘어남
				//pPlayContentsInfo->strId.Replace(SAMPLE_FILE_KEY, strHostName);
				pPlayContentsInfo->strId = pPlayContentsInfo->strId.Left(pPlayContentsInfo->strId.Find(SAMPLE_FILE_KEY))
									 + strHostName
									 + pPlayContentsInfo->strId.Right(6);
			}

			if(pPlayContentsInfo->strParentPlayContentsId.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				//pPlayContentsInfo->strId.Replace(SAMPLE_FILE_KEY, strHostName);
				pPlayContentsInfo->strParentPlayContentsId = pPlayContentsInfo->strParentPlayContentsId.Left(pPlayContentsInfo->strParentPlayContentsId.Find(SAMPLE_FILE_KEY))
												   + strHostName
												   + pPlayContentsInfo->strParentPlayContentsId.Right(6);
			}

			strIdList.Append(app_name + "Schedule" + pPlayContentsInfo->strId);
		}
	}
	::WritePrivateProfileString(app_name, "DefaultScheduleList", strIdList, lpszFullPath);

	strIdList = _T("");
	if(pTimePlayContentsInfoList)
	{
		for(int i=0; i<pTimePlayContentsInfoList->GetCount(); i++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = pTimePlayContentsInfoList->GetAt(i);
			if(strIdList.GetLength() != 0)
			{
				strIdList.Append(",");
			}

			if(pPlayContentsInfo->strId.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				// 0000737: 패키지 INI 파일에 패키지 ID길이 가 계속 늘어남
				pPlayContentsInfo->strId = pPlayContentsInfo->strId.Left(pPlayContentsInfo->strId.Find(SAMPLE_FILE_KEY))
									 + strHostName
									 + pPlayContentsInfo->strId.Right(6);
			}

			if(pPlayContentsInfo->strParentPlayContentsId.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				pPlayContentsInfo->strParentPlayContentsId = pPlayContentsInfo->strParentPlayContentsId.Left(pPlayContentsInfo->strParentPlayContentsId.Find(SAMPLE_FILE_KEY))
												   + strHostName
												   + pPlayContentsInfo->strParentPlayContentsId.Right(6);
			}

			strIdList.Append(app_name + "Schedule" + pPlayContentsInfo->strId);
		}
	}
	::WritePrivateProfileString(app_name, "ScheduleList", strIdList, lpszFullPath);

	return true;
}

bool FRAME_INFO::Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID)
{
	char buf[DC_BUFF_SIZE];

	::GetPrivateProfileString(lpszLoadID, "templateId"         , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strTemplateId          = buf;
	::GetPrivateProfileString(lpszLoadID, "frameId"            , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strId                  = buf;
	::GetPrivateProfileString(lpszLoadID, "grade"              , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); nGrade                 = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "x"                  , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); rcRect.left            = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "y"                  , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); rcRect.top             = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "width"              , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); rcRect.right           = rcRect.left + atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "height"             , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); rcRect.bottom          = rcRect.top + atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "isPIP"              , "0", buf, DC_BUFF_SIZE, lpszFullPath); bIsPIP                 = (bool)atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "zIndex"             , "0", buf, DC_BUFF_SIZE, lpszFullPath); nZIndex                = (bool)atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "alpha"              , "0", buf, DC_BUFF_SIZE, lpszFullPath); nAlpha                 = (bool)atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "isTV"               , "0", buf, DC_BUFF_SIZE, lpszFullPath); bIsTV                  = (bool)atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "borderStyle"        , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strBorderStyle         = buf;
	::GetPrivateProfileString(lpszLoadID, "borderThickness"    , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); nBorderThickness       = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "borderColor"        , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); crBorderColor          = ::GetColorFromString(buf);
	::GetPrivateProfileString(lpszLoadID, "cornerRadius"       , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); nCornerRadius          = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "DefaultScheduleList", _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strCyclePlayContentsIdList = buf;
	::GetPrivateProfileString(lpszLoadID, "ScheduleList"       , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strTimeBasePlayContentsIdList  = buf;
	::GetPrivateProfileString(lpszLoadID, "comment1"           , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[0]          = ForLoadComment(buf); // 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
	::GetPrivateProfileString(lpszLoadID, "comment2"           , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[1]          = ForLoadComment(buf);
	::GetPrivateProfileString(lpszLoadID, "comment3"           , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strComment[2]          = ForLoadComment(buf);

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// TEMPLATE_INFO
////////////////////////////////////////////////////////////////////////////////
bool TEMPLATE_INFO::Save(LPCTSTR lpszFullPath, FRAME_LINK_LIST* pFrameLinkList)
{
	CString app_name;
	app_name.Format("Template%s", strId);

	::WritePrivateProfileString(app_name, "templateId" , strId                          , lpszFullPath);
	::WritePrivateProfileString(app_name, "width"      , ::ToString(rcRect.Width())     , lpszFullPath);
	::WritePrivateProfileString(app_name, "height"     , ::ToString(rcRect.Height())    , lpszFullPath);
	::WritePrivateProfileString(app_name, "bgColor"    , ::GetColorFromString(crBgColor), lpszFullPath);
	::WritePrivateProfileString(app_name, "bgImage"    , strBgImage                     , lpszFullPath);
	::WritePrivateProfileString(app_name, "bgType"     , ::ToString(nBgType)            , lpszFullPath);
	::WritePrivateProfileString(app_name, "description", strDescription                 , lpszFullPath);
	::WritePrivateProfileString(app_name, "shortCut"   , strShortCut                    , lpszFullPath);

	CString strIdList;
	if(pFrameLinkList)
	{
		for(int i=0; i<pFrameLinkList->GetCount(); i++)
		{
			FRAME_LINK& frame_link = pFrameLinkList->GetAt(i);

			if(strIdList.GetLength() != 0)
				strIdList.Append(",");
			strIdList.Append(app_name + "Frame" + frame_link.pFrameInfo->strId);
		}
	}
	::WritePrivateProfileString(app_name, "FrameList", strIdList, lpszFullPath);

	return true;
}

bool TEMPLATE_INFO::Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID)
{
	char buf[DC_BUFF_SIZE];

	::GetPrivateProfileString(lpszLoadID, "templateId" , _T("") , buf, DC_BUFF_SIZE, lpszFullPath); strId          = buf;
	::GetPrivateProfileString(lpszLoadID, "width"      , _T("") , buf, DC_BUFF_SIZE, lpszFullPath);	rcRect.right   = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "height"     , _T("") , buf, DC_BUFF_SIZE, lpszFullPath);	rcRect.bottom  = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "bgColor"    , _T("") , buf, DC_BUFF_SIZE, lpszFullPath);	crBgColor      = ::GetColorFromString(buf);
	::GetPrivateProfileString(lpszLoadID, "bgImage"    , _T("") , buf, DC_BUFF_SIZE, lpszFullPath);	strBgImage     = buf;
	::GetPrivateProfileString(lpszLoadID, "bgType"     , "0", buf, DC_BUFF_SIZE, lpszFullPath);	nBgType        = (bool)atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "description", _T("") , buf, DC_BUFF_SIZE, lpszFullPath);	strDescription = buf;
	::GetPrivateProfileString(lpszLoadID, "shortCut"   , _T("") , buf, DC_BUFF_SIZE, lpszFullPath);	strShortCut    = buf;
	::GetPrivateProfileString(lpszLoadID, "FrameList"  , _T("") , buf, DC_BUFF_SIZE, lpszFullPath);	strFrameList   = buf;

	return true;
}


////////////////////////////////////////////////////////////////////////////////
// TEMPLATE_LINK
////////////////////////////////////////////////////////////////////////////////
void TEMPLATE_LINK::DeleteFrameList()
{
	int nCount = arFrameLinkList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		FRAME_LINK& frm_link = arFrameLinkList.GetAt(i);
		frm_link.pFrameInfo->DeletePlayContentsList();
		delete frm_link.pFrameInfo;
	}
	arFrameLinkList.RemoveAll();
}

void TEMPLATE_LINK::RecalcLayout()
{
	int nWidth = TEMPLATE_WIDTH;
	int nHeight = pTemplateInfo->rcRect.Width() * TEMPLATE_HEIGHT / TEMPLATE_WIDTH;
	if(nHeight < pTemplateInfo->rcRect.Height())
	{
		nHeight = TEMPLATE_HEIGHT;
		nWidth = pTemplateInfo->rcRect.Height() * TEMPLATE_WIDTH / TEMPLATE_HEIGHT;

		fScale = ((double)TEMPLATE_HEIGHT) / ((double)pTemplateInfo->rcRect.Height());
	}
	else
	{
		fScale = ((double)TEMPLATE_WIDTH) / ((double)pTemplateInfo->rcRect.Width());
	}
}

// 강제로 PIP 로 설정등 과 같이 프레임에 강제 설정해야 하는 정보를 이 함수에서 설정한다.
void TEMPLATE_LINK::SetForceFrameInfo()
{
	ReCalcPIP();
}

void TEMPLATE_LINK::ReCalcPIP()
{
	int nCount = arFrameLinkList.GetCount();

	// 강제로 PIP 값 설정
	for(int i=0; i<nCount; i++)
	{
		FRAME_LINK& selframe_link = arFrameLinkList.GetAt(i);

		bool bIsInclusion = false;	// 다른 프레임에 포함되어 있는지
		bool bIsOverlap   = false;	// 다른 프레임과 겹쳐있는지
		bool bHaveFrame   = false;	// 다른 프레임을 포함하고 있는지

		bool bIsPIP = false;

		for(int j=0; j<nCount; j++)
		{
			FRAME_LINK& frame_link = arFrameLinkList.GetAt(j);
			if(selframe_link.pFrameInfo->strId == frame_link.pFrameInfo->strId)
			{
				bIsPIP = selframe_link.pFrameInfo->bIsPIP;
				continue;
			}

			// 다른 프레임과 겹쳐있는지 여부 체크
			CRect rcOverlap;
			if(selframe_link.pFrameInfo->rcRect == frame_link.pFrameInfo->rcRect)
			{
				bIsInclusion = true;
				bHaveFrame = true;
			}
			else if( ::IntersectRect(rcOverlap, selframe_link.pFrameInfo->rcRect, frame_link.pFrameInfo->rcRect) )
			{
				// 다른 프레임에 포함되어 있는지 여부 체크
				if( selframe_link.pFrameInfo->rcRect == rcOverlap )
				{
					bIsInclusion = true;
				}
				// 다른 프레임을 포함하고 있는지 여부 체크
				else if( frame_link.pFrameInfo->rcRect == rcOverlap )
				{
					bHaveFrame = true;
				}
				else
				{
					bIsOverlap = true;
				}
			}
		}

		// PIP를 갖고있는(포함하는) 프레임은 PIP로 설정할 수 없도록한다
		if(bHaveFrame) selframe_link.pFrameInfo->bIsPIP = false;
		// 다른 프레임과 일부분이 겹치는 프레임은 PIP속성을 설정할 수 있다(사용자가 직접 설정, 이전값 유지) 
		else if(bIsOverlap) selframe_link.pFrameInfo->bIsPIP = bIsPIP;
		// 다른 프레임안에 완전히 포함되며 겹쳐있지 않은 프레임은 자동으로 PIP속성을 갖도록한다
		else if(bIsInclusion) selframe_link.pFrameInfo->bIsPIP = true;
		// 그외 다른 프레임과 겹치지 않는 프레임은 PIP로 설정할 수 없도록한다
		else selframe_link.pFrameInfo->bIsPIP = false;
	}
}

////////////////////////////////////////////////////////////////////////////////
// CDataContainer
////////////////////////////////////////////////////////////////////////////////

CDataContainer*	CDataContainer::_instance;
CCriticalSection CDataContainer::_cs;

CDataContainer* CDataContainer::getInstance()
{
	_cs.Lock();
	if(_instance == NULL)
	{
		_instance = new CDataContainer();
	}
	_cs.Unlock();

	return _instance;
}

void CDataContainer::clearInstance()
{
	_cs.Lock();
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
	_cs.Unlock();
}

CDataContainer::CDataContainer()
:	m_strSoundVolume("100")
,	m_strDefaultTemplate(_T(""))
,	m_strAdminState("1")
,	m_strOperationalState("0")
,	m_strDescription(_T(""))
,	m_strNetworkUse("0")
,	m_strSite(_T(""))
,	m_strLastWrittenUser(_T(""))
,	m_strLastWrittenTime(_T(""))
{
}

CDataContainer::~CDataContainer()
{
	DeleteAllData();
}

bool CDataContainer::DeleteAllData(bool bIncludeBackup/*=true*/)
{
	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		m_ContentsMap.GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );
		if(pContentsInfo) delete pContentsInfo;
	}
	m_ContentsMap.RemoveAll();

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);
		template_link.DeleteFrameList();
		if(template_link.pTemplateInfo) delete template_link.pTemplateInfo;
	}
	m_AllTemplateList.RemoveAll();
	m_PlayTemplateList.RemoveAll();

	// Template Size 삭제-구현석
	GetEnvPtr()->m_lsTempRect.RemoveAll();

	m_strSoundVolume		= "100";
	m_strDefaultTemplate	= _T("");
	m_strAdminState			= _T("1");
	m_strOperationalState	= _T("0");

	if(bIncludeBackup) DeleteAllBackupData();

	return true;
}

bool CDataContainer::AddContents(CONTENTS_INFO* pInContentsInfo, LPSTR szMessage)
{
	CONTENTS_INFO* pContentsInfo = NULL;
	int nSeq = 1;
	int nIdx;
	CString strId, strNewId, strTmpName;

	// 2009-05-11 contents name 없으면 무조건 skip
	if(pInContentsInfo->strContentsName.Trim().IsEmpty())
	{
		if(szMessage) sprintf(szMessage, DATACONTAINER_MSG004);
		return false;
	}

	//contents id가 없을 경우
	if(pInContentsInfo->strId.GetLength() == 0)
	{
		if(pInContentsInfo->strFilename == _T(""))
		{
			strTmpName = pInContentsInfo->strContentsName;
			strTmpName.Replace(" ", _T(""));
		}
		else
		{
			nIdx = pInContentsInfo->strFilename.Find('.', 0);
			if(nIdx != -1)
			{
				strTmpName = pInContentsInfo->strFilename.Left(nIdx);
			}
			else
			{
				strTmpName = pInContentsInfo->strFilename;
			}
		}

		// [], 는 (), 로 치환함
//		strTmpName.Replace("[", "(");
//		strTmpName.Replace("]", ")");
//		strTmpName.Replace(",", "_");
		strTmpName = CPreventChar::GetInstance()->ConvertToAvailableChar(strTmpName);

		strId.Format("%s_%02d", strTmpName, nSeq);
		strNewId = strId;

		while(m_ContentsMap.Lookup(strNewId, (void*&)pContentsInfo))
		{
			if(nSeq == 99)
			{
				strId = strNewId;
				nSeq = 1;
			}
			strNewId.Format("%s_%02d", strId, nSeq++);
		}

		pInContentsInfo->strId = strNewId;

		// 0000911: 마법사컨텐츠의 저장되는 XML내에 마법사컨텐츠 자신의 컨텐츠ID를 넣어서 저장하도록함
		if(CONTENTS_WIZARD == pInContentsInfo->nContentsType && !pInContentsInfo->strWizardXML.IsEmpty())
		{
			CXmlParser xmlWizardData;

			if( xmlWizardData.LoadXML(pInContentsInfo->strWizardXML) == XMLPARSER_SUCCESS &&
				xmlWizardData.SetXPath("/FlashWizard")               == XMLPARSER_SUCCESS )
			{
				if(xmlWizardData.SetAttribute("id", strNewId) != XMLPARSER_SUCCESS)
				{
					xmlWizardData.NewAttribute("id");
					xmlWizardData.SetAttribute("id", strNewId);
				}

				// xml 저장
				CString strXml = xmlWizardData.GetXML("/");
				strXml.Replace("\t",_T(""));
				strXml.Replace("\r",_T(""));
				strXml.Replace("\n",_T(""));
				pInContentsInfo->strWizardXML = strXml;
			}
		}
	}

	//contents의 id가 중복되지 않도록 해 준다
	if(m_ContentsMap.Lookup(pInContentsInfo->strId, (void*&)pContentsInfo ))
	{
		//같은 contents 객체이면 추가하지 않는다.
		if(*pContentsInfo == *pInContentsInfo)
		{
			if(szMessage) sprintf(szMessage, DATACONTAINER_MSG005);
			delete pInContentsInfo;
			return false;
		}

		//같은 contents가 아니라면 중복되지 않는 id를 만들어 준다
		nSeq = 1;
		strId = pInContentsInfo->strId;
		strNewId = strId;
		while(m_ContentsMap.Lookup(strNewId, (void*&)pContentsInfo))
		{
			if(nSeq == 99)
			{
				strId = strNewId;
				nSeq = 1;
			}//if
			strNewId.Format("%s_%02d", strId, nSeq++);
		}//while

		pInContentsInfo->strId = strNewId;
	}//if


	// [], 는 (), 로 치환함
//	pInContentsInfo->strContentsName.Replace("[", "(");
//	pInContentsInfo->strContentsName.Replace("]", ")");
//	pInContentsInfo->strContentsName.Replace(",", "_");
	pInContentsInfo->strContentsName = CPreventChar::GetInstance()->ConvertToAvailableChar(pInContentsInfo->strContentsName);

	//이제 contents map에 없었거나, 중복되지 않은 id로 만들어주었다
	//마지막으로 contents의 이름이 중복되지 않도록 해 준다
	nSeq = 1;
	CString strName, strNewName;
	strName = pInContentsInfo->strContentsName;
	strNewName = strName;
	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos)
	{
		m_ContentsMap.GetNextAssoc(pos, strId, (void*&)pContentsInfo);
		if((pContentsInfo != NULL) && (strNewName == pContentsInfo->strContentsName))
		{
			if(nSeq == 99)
			{
				strName = strNewName;
				nSeq = 1;
			}//if
			strNewName.Format("%s_%02d", strName, nSeq++);
			pos = m_ContentsMap.GetStartPosition();
		}//if
	}//while

	pInContentsInfo->strContentsName = strNewName;
	m_ContentsMap.SetAt(pInContentsInfo->strId, pInContentsInfo);

	return true;
}

bool CDataContainer::AddTemplate(TEMPLATE_INFO* pInTemplateInfo)
{
	// 구현석 같은 템플릿 제거. 2009-6-15 같은 템플릿이 있으면 제대로 표현 못하는 버그 있음.
	for(int i = 0; i < m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == pInTemplateInfo->strId) return false;
	}

	// Template Size 저장-구현석
	POSITION pos = GetEnvPtr()->m_lsTempRect.Find(pInTemplateInfo->rcRect);
	if(!pos) GetEnvPtr()->m_lsTempRect.AddTail(pInTemplateInfo->rcRect);

	TEMPLATE_LINK link(pInTemplateInfo);
	link.RecalcLayout();
	m_AllTemplateList.Add(link);

	return true;
}

int CDataContainer::AddPlayTemplate(LPCTSTR lpszTemplateId, int nPlayTimes)
{
	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			TEMPLATE_LINK link(template_link.pTemplateInfo, nPlayTimes, template_link.fScale);
			link.arFrameLinkList.Copy(template_link.arFrameLinkList);
			m_PlayTemplateList.Add(link);
			return m_PlayTemplateList.GetCount()-1;
		}
	}

	return -1;
}

int CDataContainer::AddPlayTemplate(LPCTSTR lpszTemplateId, LPCTSTR szPlayTimes)
{
	CString szTimes = szPlayTimes;
	int nRet = szTimes.Find(_T("("));
	if(nRet == -1){
		return AddPlayTemplate(lpszTemplateId, atoi(szPlayTimes));
	}else{
		return AddPlayTemplate(lpszTemplateId, 1);
	}
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 열려있는 host의 설명을 반환한다. \n
/// @return <형: CString> \n
///			열려있는 host의 설명 \n
/////////////////////////////////////////////////////////////////////////////////
CString CDataContainer::GetDescription(void)
{
	return m_strDescription;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 열려있는 host의 설명을 설정한다. \n
/// @param (CString) strDesc : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CDataContainer::SetDescription(CString strDesc)
{
	m_strDescription = strDesc;
}


TEMPLATE_INFO* CDataContainer::CreateNewTemplate()
{
	// new template id 설정
	CRect clsOrgRect(0,0,0,0);

	int nMinId = 1;

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link= m_AllTemplateList.GetAt(i);

		// Modified by 정운형 2009-03-04 오후 6:17
		// 변경내역 :  Template 추가시에 사이즈 조정
		if(i == 0)
		{
			clsOrgRect = template_link.pTemplateInfo->rcRect;
		}
		// Modified by 정운형 2009-03-04 오후 6:17
		// 변경내역 :  Template 추가시에 사이즈 조정

		if(nMinId == atoi(template_link.pTemplateInfo->strId))
		{
			nMinId = atoi(template_link.pTemplateInfo->strId) + 1;
			i = -1;
		}
	}

	TEMPLATE_INFO* pTemplateInfo = new TEMPLATE_INFO;
	pTemplateInfo->strId.Format("%02d", nMinId);
	//pTemplateInfo->rcRect.SetRect(0,0,DC_BUFF_SIZE,768);
	pTemplateInfo->rcRect = clsOrgRect;
	pTemplateInfo->crBgColor = RGB(0,0,0);

	if( AddTemplate(pTemplateInfo) == false )
	{
		delete pTemplateInfo;
		pTemplateInfo = NULL;
	}

	return pTemplateInfo;
}

FRAME_INFO* CDataContainer::CreateNewFrame(LPCTSTR lpszTemplateId)
{
	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(lpszTemplateId);
	if(pTemplateLink == NULL) return NULL;

	// new frame id 설정
	int nMinId = 1;
	bool bHasPrimaryFrame = false;
	int nCount = pTemplateLink->arFrameLinkList.GetCount();
	int nMaxZIndex = 0;

	for(int i=0; i<nCount; i++)
	{
		FRAME_LINK& frame_link = pTemplateLink->arFrameLinkList.GetAt(i);

		if((frame_link.pFrameInfo->nGrade == 1) && bHasPrimaryFrame == false)
		{
			bHasPrimaryFrame = true;
		}

		if(nMinId == atoi(frame_link.pFrameInfo->strId))
		{
			nMinId = atoi(frame_link.pFrameInfo->strId) + 1;
			i = -1;
		}

		if(frame_link.pFrameInfo->nZIndex < PIP_ZINDEX && nMaxZIndex < frame_link.pFrameInfo->nZIndex)
		{
			nMaxZIndex = frame_link.pFrameInfo->nZIndex;
		}
	}

	FRAME_INFO* pFrameInfo = new FRAME_INFO;
	pFrameInfo->strTemplateId = pTemplateLink->pTemplateInfo->strId;
	pFrameInfo->strId.Format("%02d", nMinId);
	pFrameInfo->nGrade = (bHasPrimaryFrame ? 2 : 1);
	pFrameInfo->rcRect.SetRect(0,0,320,240);
	pFrameInfo->strBorderStyle = "solid";
	pFrameInfo->nBorderThickness = 1;
	pFrameInfo->nZIndex = ++nMaxZIndex;

	FRAME_LINK frame_link(pFrameInfo);
	pTemplateLink->arFrameLinkList.Add(frame_link);

	// play리스트에도 추가
	nCount = m_PlayTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);
		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			template_link.arFrameLinkList.Add(frame_link);
		}
	}

	return pFrameInfo;	
}


// Modified by 정운형 2009-02-19 오후 8:31
// 변경내역 :  template 추가 버그 수정
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기존의 template size, color등 기본정보만 복사하여 새로운 template를 생성한다. \n
/// @param (LPCTSTR) lpszSrcTemplateID : (in) 복사하려는 template id
/// @return <형: LPCTSTR> \n
///			새로 생성한 template의 id \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR CDataContainer::CreateCopyTemplate(LPCTSTR lpszSrcTemplateID)
{
	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(lpszSrcTemplateID);
	if(pTemplateLink == NULL) return _T("");

	// USTB 는 같은 크기의 템플릿만 가능함-구현석
	if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
	{
		for(int i=0; i < m_PlayTemplateList.GetCount(); i++)
		{
			TEMPLATE_LINK& PlayTempLink = m_PlayTemplateList.GetAt(i);
			if( PlayTempLink.pTemplateInfo->rcRect.Width()  != pTemplateLink->pTemplateInfo->rcRect.Width()  ||
				PlayTempLink.pTemplateInfo->rcRect.Height() != pTemplateLink->pTemplateInfo->rcRect.Height() )
			{
				AfxMessageBox(DATACONTAINER_MSG006, MB_ICONERROR);
				return _T("");
			}
		}
	}

	TEMPLATE_INFO* pNewTemplateInfo = CreateNewTemplate();
	if(pNewTemplateInfo == NULL) return _T("");

	/////////////////////////////////////////////////////////////////////
	//Template info 멤버 복사(id는 복사하지 않는다)
	CString strNewTemplateId = pNewTemplateInfo->strId;
	*pNewTemplateInfo = *(pTemplateLink->pTemplateInfo);
	pNewTemplateInfo->strId = strNewTemplateId;
	pNewTemplateInfo->strShortCut = _T("");

	/////////////////////////////////////////////////////////////////////
	//Template Link 멤버 복사
	TEMPLATE_LINK* pNewTemplateLink = GetTemplateLink(pNewTemplateInfo->strId);
	if(pNewTemplateLink == NULL) return _T("");

	//Frame Link, Frame Info
	for(int i=0; i<pTemplateLink->arFrameLinkList.GetCount(); i++)
	{
		FRAME_LINK FrameLink = pTemplateLink->arFrameLinkList.GetAt(i);
		FRAME_LINK NewFrameLink;
		NewFrameLink.Copy(FrameLink);

		// 새로생성된 템플릿ID로 설정하고 레이아웃만 생성하므로 복사된 플레이컨텐츠리스트는 삭제한다.
		NewFrameLink.pFrameInfo->strTemplateId = pNewTemplateInfo->strId;
		NewFrameLink.pFrameInfo->DeletePlayContentsList();

		pNewTemplateLink->arFrameLinkList.Add(NewFrameLink);
	}

	pNewTemplateLink->nPlayTimes = pTemplateLink->nPlayTimes;
	pNewTemplateLink->fScale	 = pTemplateLink->fScale;
	for(int i=0; i<5; i++)
	{
		pNewTemplateLink->rcViewRect[i] = pTemplateLink->rcViewRect[i];
	}

	pNewTemplateLink->rcViewRectItem    = pTemplateLink->rcViewRectItem;
	pNewTemplateLink->rcViewRectInfo[0] = pTemplateLink->rcViewRectInfo[0];
	pNewTemplateLink->rcViewRectInfo[1] = pTemplateLink->rcViewRectInfo[1];
	/////////////////////////////////////////////////////////////////////

	//Template Play Link에 추가
	AddPlayTemplate(pNewTemplateInfo->strId, pNewTemplateLink->nPlayTimes);

	return pNewTemplateInfo->strId;
}
// Modified by 정운형 2009-02-19 오후 8:31
// 변경내역 :  template 추가 버그 수정


bool CDataContainer::DeleteContents(LPCTSTR lpszContentsId)
{
	int nTemplateCount = m_AllTemplateList.GetCount();

	for(int nTemplateIdx=0; nTemplateIdx<nTemplateCount; nTemplateIdx++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(nTemplateIdx);

		CStringArray astrDelPlayContentsList;

		int nFrameCount = template_link.arFrameLinkList.GetCount();
		for(int nFrameIdx=0; nFrameIdx<nFrameCount; nFrameIdx++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(nFrameIdx);

			int nPlayContentsCount = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
			for(int nPlayContentsIdx=nPlayContentsCount-1; nPlayContentsIdx>=0; nPlayContentsIdx--)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(nPlayContentsIdx);
				if(pPlayContentsInfo->strContentsId == lpszContentsId)
				{
					astrDelPlayContentsList.Add(pPlayContentsInfo->strId);
					frame_link.pFrameInfo->DeletePlayContents(pPlayContentsInfo);
				}
				//// 0000611: 플레쉬 컨텐츠 마법사
				//else
				//{
				//	CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
				//	if(pContentsInfo)
				//	{
				//		pContentsInfo->strWizardFiles += ",";
				//		pContentsInfo->strWizardFiles.Replace(lpszContentsId+CString(","),_T(""));
				//		pContentsInfo->strWizardFiles.TrimRight(",");
				//	}
				//}
			}

			nPlayContentsCount = frame_link.pFrameInfo->arTimePlayContentsList.GetCount();
			for(int nPlayContentsIdx=nPlayContentsCount-1; nPlayContentsIdx>=0; nPlayContentsIdx--)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(nPlayContentsIdx);
				if(pPlayContentsInfo->strContentsId == lpszContentsId)
				{
					frame_link.pFrameInfo->DeletePlayContents(pPlayContentsInfo);
				}
				//// 0000611: 플레쉬 컨텐츠 마법사
				//else
				//{
				//	CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
				//	if(pContentsInfo)
				//	{
				//		pContentsInfo->strWizardFiles += ",";
				//		pContentsInfo->strWizardFiles.Replace(lpszContentsId+CString(","),_T(""));
				//		pContentsInfo->strWizardFiles.TrimRight(",");
				//	}
				//}
			}
		}

		// 삭제된 플레이컨텐츠가 부모플레이컨텐츠로 설정된 플레이컨텐츠의 설정을 클리어한다
		for(int nDelPlayContentsIdx=0; nDelPlayContentsIdx<astrDelPlayContentsList.GetSize() ; nDelPlayContentsIdx++)
		{
			for(int nFrameIdx=0; nFrameIdx<nFrameCount; nFrameIdx++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(nFrameIdx);

				int nPlayContentsCount = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
				for(int nPlayContentsIdx=0; nPlayContentsIdx<nPlayContentsCount; nPlayContentsIdx++)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(nPlayContentsIdx);
					if(pPlayContentsInfo->strParentPlayContentsId == astrDelPlayContentsList[nDelPlayContentsIdx])
					{
						pPlayContentsInfo->strParentPlayContentsId.Empty();
					}
				}
			}
		}
	}

	CONTENTS_INFO* pContentsInfo = NULL;
	if(m_ContentsMap.Lookup( lpszContentsId, (void*&)pContentsInfo ))
	{
		BOOL ret_value = m_ContentsMap.RemoveKey(lpszContentsId);
		delete pContentsInfo;

		DeleteContentsInWizards(lpszContentsId);

		return true;
	}

	return false;
}

bool CDataContainer::DeleteTemplate(LPCTSTR lpszTemplateId)
{
	// delete from play list
	int nCount = m_PlayTemplateList.GetCount();
	for(int i=nCount-1; i>=0; i--)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			m_PlayTemplateList.RemoveAt(i);
			break;
		}
	}

	// delete from all list
	nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			template_link.DeleteFrameList();
			if(template_link.pTemplateInfo) delete template_link.pTemplateInfo;
			template_link.pTemplateInfo = NULL;
			m_AllTemplateList.RemoveAt(i);
			CheckTemplateRect();
			return true;
		}
	}

	CheckTemplateRect();
	return false;
}

bool CDataContainer::DeleteContentsInWizards(CString strDelContentsId)
{
	if(strDelContentsId.IsEmpty()) return true;

	CXmlParser xmlWizardData;

	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo = NULL;
		m_ContentsMap.GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(strContentsId == strDelContentsId) continue;
		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_WIZARD) continue;

		pContentsInfo->strWizardFiles += ",";
		pContentsInfo->strWizardFiles.Replace(strDelContentsId+CString(","),_T(""));
		pContentsInfo->strWizardFiles.TrimRight(",");

		CString strXPath;
		if(!pContentsInfo->strWizardXML.IsEmpty() &&
			xmlWizardData.LoadXML(pContentsInfo->strWizardXML) == XMLPARSER_SUCCESS)
		{
			strXPath.Format("//context [@id=\"%s\"]", strDelContentsId);
			if(xmlWizardData.SetXPath(strXPath) == XMLPARSER_SUCCESS)
			{
				for(int i=0; i<xmlWizardData.GetSelCount() ;i++)
				{
					xmlWizardData.SetAttribute("id", _T(""), i);
				}
			}

			strXPath.Format("//action [@id=\"%s\"]", strDelContentsId);
			if(xmlWizardData.SetXPath(strXPath) == XMLPARSER_SUCCESS)
			{
				for(int i=0; i<xmlWizardData.GetSelCount() ;i++)
				{
					xmlWizardData.SetAttribute("id", _T(""), i);
					xmlWizardData.SetAttribute("name", _T(""), i);
				}
			}

			// xml 저장
			CString strXml = xmlWizardData.GetXML("/");
			strXml.Replace("\t",_T(""));
			strXml.Replace("\r",_T(""));
			strXml.Replace("\n",_T(""));
			pContentsInfo->strWizardXML = strXml;
		}
	}

	return true;
}

void CDataContainer::CheckTemplateRect()
{
	// 존재하지 않는 template Size 삭제-구현석
	POSITION pos = GetEnvPtr()->m_lsTempRect.GetTailPosition();

	while(pos)
	{
		bool bRemove = true;
		POSITION posOld = pos;
		CRect rcTemp = GetEnvPtr()->m_lsTempRect.GetPrev(pos);

		int nCount = m_AllTemplateList.GetCount();
		for(int i=0; i<nCount; i++)
		{
			TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);
			if(!template_link.pTemplateInfo) continue;

			if(template_link.pTemplateInfo->rcRect == rcTemp)
				bRemove = false;
		}

		if(bRemove)
		{
			GetEnvPtr()->m_lsTempRect.RemoveAt(posOld);
		}
	}
}

bool CDataContainer::CheckPrimaryFrame()
{
	CString strMsg;
	return CheckPrimaryFrame(true, strMsg);
}

bool CDataContainer::CheckPrimaryFrame(bool bMsg, CString& strOutMsg)
{
	CString szMsg;
	int nCount = m_PlayTemplateList.GetCount();

	for(int i=0; i<nCount; i++)
	{
		bool bPriFrm = false;
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);
		int nCount2 = template_link.arFrameLinkList.GetCount();

		for(int j=0; j<nCount2; j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			if(1 == frame_link.pFrameInfo->nGrade)
			{
				bPriFrm = true;
				bool bCheck = false;
			
				if(frame_link.pFrameInfo->arCyclePlayContentsList.GetCount() > 0) bCheck = true;
				if(frame_link.pFrameInfo->arTimePlayContentsList.GetCount()  > 0) bCheck = true;
				if(!bCheck)
				{
#ifdef _ML_ENG_
					szMsg.Format(DATACONTAINER_MSG001, frame_link.pFrameInfo->strId, template_link.pTemplateInfo->strId);
#else
					szMsg.Format(DATACONTAINER_MSG001, template_link.pTemplateInfo->strId, frame_link.pFrameInfo->strId);
#endif
//					if(AfxMessageBox(szMsg, MB_ICONWARNING|MB_YESNO) != IDYES)
//						return false;
//					else
//						continue;
					if(bMsg) AfxMessageBox(szMsg, MB_ICONWARNING);
					else     strOutMsg = szMsg;
					return false;
				}
			}
		}

		if(!bPriFrm)
		{
			szMsg.Format(DATACONTAINER_MSG002, template_link.pTemplateInfo->strId);
//			if(AfxMessageBox(szMsg, MB_ICONWARNING|MB_YESNO) != IDYES)
//				return false;
//			else
//				continue;
			if(bMsg) AfxMessageBox(szMsg, MB_ICONWARNING);
			else     strOutMsg = szMsg;
			return false;
		}
	}

	return true;
}

bool CDataContainer::DeleteFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId)
{
	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(lpszTemplateId);
	if(pTemplateLink == NULL) return false;

	// delete from play list
	int nCount = m_PlayTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);
		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			int nCount2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<nCount2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);
				if(frame_link.pFrameInfo->strId == lpszFrameId)
				{
					template_link.arFrameLinkList.RemoveAt(j);
					break;
				}
			}
		}
	}

	// delete from all list
	nCount = pTemplateLink->arFrameLinkList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		FRAME_LINK& frame_link= pTemplateLink->arFrameLinkList.GetAt(i);
		if(frame_link.pFrameInfo->strId == lpszFrameId)
		{
			delete frame_link.pFrameInfo;
			pTemplateLink->arFrameLinkList.RemoveAt(i);
			break;
		}
	}

	return true;
}

bool CDataContainer::DeletePlayTemplate(int nIndex)
{
	if(nIndex < 0 || nIndex > m_PlayTemplateList.GetCount()-1)
		return false;

	m_PlayTemplateList.RemoveAt(nIndex);

	return true;
}

CONTENTS_INFO* CDataContainer::GetContents(LPCTSTR lpszContentsId)
{
	CONTENTS_INFO* pContentsInfo = NULL;
	m_ContentsMap.Lookup( lpszContentsId, (void*&)pContentsInfo );
	return pContentsInfo;
}

TEMPLATE_LINK* CDataContainer::GetTemplateLink(LPCTSTR lpszTemplateId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
			return &(m_AllTemplateList.GetAt(i));
	}
	return NULL;
}

FRAME_LINK* CDataContainer::GetFrameLink(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			int nCount2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<nCount2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				if(frame_link.pFrameInfo->strId == lpszFrameId)
					return &(template_link.arFrameLinkList.GetAt(j));
			}
		}
	}
	return NULL;
}

PLAYCONTENTS_INFO* CDataContainer::GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszPlayContentsId)
{
	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(lpszTemplateId);
	if(!pTemplateLink) return NULL;

	for(int i=0; i<pTemplateLink->arFrameLinkList.GetCount() ;i++)
	{
		FRAME_INFO* pFrameInfo = pTemplateLink->arFrameLinkList[i].pFrameInfo;
		if(!pFrameInfo) return NULL;

		for(int j=0; j<pFrameInfo->arCyclePlayContentsList.GetCount() ;j++)
		{
			if(pFrameInfo->arCyclePlayContentsList[j]->strId == lpszPlayContentsId)
			{
				return pFrameInfo->arCyclePlayContentsList[j];
			}
		}
	}

	return NULL;
}

PLAYCONTENTS_INFO* CDataContainer::GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId, LPCTSTR lpszPlayContentsId)
{
	FRAME_LINK* pFrameLink = GetFrameLink(lpszTemplateId, lpszFrameId);
	if(!pFrameLink) return NULL;

	for(int i=0; i<pFrameLink->pFrameInfo->arCyclePlayContentsList.GetCount() ;i++)
	{
		if(pFrameLink->pFrameInfo->arCyclePlayContentsList[i]->strId == lpszPlayContentsId)
		{
			return pFrameLink->pFrameInfo->arCyclePlayContentsList[i];
		}
	}

	return NULL;
}

bool CDataContainer::MoveFordwardPlayTemplate(int nIndex)
{
	int nCount = m_PlayTemplateList.GetCount();

	if(nIndex <= 0 || nIndex > nCount-1 )
		return false;

	TEMPLATE_LINK target_info; target_info = m_PlayTemplateList.GetAt(nIndex-1);
	TEMPLATE_LINK source_info; source_info = m_PlayTemplateList.GetAt(nIndex);

	m_PlayTemplateList.SetAt(nIndex-1, source_info);
	m_PlayTemplateList.SetAt(nIndex,   target_info);

	return true;
}

bool CDataContainer::MoveBackwardPlayTemplate(int nIndex)
{
	int nCount = m_PlayTemplateList.GetCount();

	if(nIndex < 0 || nIndex >= nCount-1 )
		return false;

	TEMPLATE_LINK target_info; target_info = m_PlayTemplateList.GetAt(nIndex+1);
	TEMPLATE_LINK source_info; source_info = m_PlayTemplateList.GetAt(nIndex);

	m_PlayTemplateList.SetAt(nIndex+1, source_info);
	m_PlayTemplateList.SetAt(nIndex,   target_info);

	return true;
}

bool CDataContainer::IsExistContentsInTemplate(LPCTSTR lpszTemplateId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			int nCount2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<nCount2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				if(frame_link.pFrameInfo->arCyclePlayContentsList.GetCount() > 0) return true;
				if(frame_link.pFrameInfo->arTimePlayContentsList.GetCount()   > 0) return true;
			}
		}
	}

	return false;
}

bool CDataContainer::IsExistContentsInFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			int nCount2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<nCount2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				if(frame_link.pFrameInfo->strId == lpszFrameId)
				{
					if(frame_link.pFrameInfo->arCyclePlayContentsList.GetCount() > 0) return true;
					if(frame_link.pFrameInfo->arTimePlayContentsList.GetCount()   > 0) return true;
				}
			}
		}
	}

	return false;
}

bool CDataContainer::IsUsedShortcut(LPCTSTR szShortcut)
{
	if(!szShortcut) return false;

	CString szTemp = szShortcut;
	if(szTemp.IsEmpty()) return false;

	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strShortCut == szShortcut) return true;
	}

	return false;
}

bool CDataContainer::IsExistPlayTemplateList(LPCTSTR lpszTemplateId)
{
	int nCount = m_PlayTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);
		if(template_link.pTemplateInfo->strId == lpszTemplateId) return true;
	}

	return false;
}

bool CDataContainer::IsExistPlayContentsUsingContents(LPCTSTR lpszContentsId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		int nCount2 = template_link.arFrameLinkList.GetCount();
		for(int j=0; j<nCount2; j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			int nCount3 = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
			for(int k=0; k<nCount3; k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);
				if(pPlayContentsInfo->strContentsId == lpszContentsId) return true;

				// 0000611: 플레쉬 컨텐츠 마법사
				CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
				if(pContentsInfo)
				{
					CString strWizardFiles = pContentsInfo->strWizardFiles + ",";
					if(strWizardFiles.Find(lpszContentsId+CString(",")) >= 0) return true;
				}
			}

			nCount3 = frame_link.pFrameInfo->arTimePlayContentsList.GetCount();
			for(int k=0; k<nCount3; k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);
				if(pPlayContentsInfo->strContentsId == lpszContentsId) return true;

				// 0000611: 플레쉬 컨텐츠 마법사
				CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
				if(pContentsInfo)
				{
					CString strWizardFiles = pContentsInfo->strWizardFiles + ",";
					if(strWizardFiles.Find(lpszContentsId+CString(",")) >= 0) return true;
				}
			}
		}
	}

	return false;
}

int CDataContainer::GetContentsUsingCount(LPCTSTR lpszContentsId)
{
	int nUsingCount = 0;

	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		int nCount2 = template_link.arFrameLinkList.GetCount();
		for(int j=0; j<nCount2; j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			int nCount3 = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
			for(int k=0; k<nCount3; k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);
				if(pPlayContentsInfo->strContentsId == lpszContentsId) nUsingCount++;
				// 0000611: 플레쉬 컨텐츠 마법사
				else
				{
					CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
					if(pContentsInfo)
					{
						CString strWizardFiles = pContentsInfo->strWizardFiles + ",";
						if(strWizardFiles.Find(lpszContentsId+CString(",")) >= 0) nUsingCount++;
					}
				}
			}
	
			nCount3 = frame_link.pFrameInfo->arTimePlayContentsList.GetCount();
			for(int k=0; k<nCount3; k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);
				if(pPlayContentsInfo->strContentsId == lpszContentsId) nUsingCount++;
				// 0000611: 플레쉬 컨텐츠 마법사
				else
				{
					CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
					if(pContentsInfo)
					{
						CString strWizardFiles = pContentsInfo->strWizardFiles + ",";
						if(strWizardFiles.Find(lpszContentsId+CString(",")) >= 0) nUsingCount++;
					}
				}
			}
		}
	}

	return nUsingCount;
}

bool CDataContainer::Load(LPCTSTR lpszFullPath)
{
	DeleteAllData();

	char buf[DC_BUFF_SIZE];

	::GetPrivateProfileString("Host", "soundVolume"     , _T(""), buf, DC_BUFF_SIZE, lpszFullPath); m_strSoundVolume      = buf;
	::GetPrivateProfileString("Host", "defaultTemplate" , _T(""), buf, DC_BUFF_SIZE, lpszFullPath);	m_strDefaultTemplate  = buf;
	::GetPrivateProfileString("Host", "adminState"      , _T(""), buf, DC_BUFF_SIZE, lpszFullPath);	m_strAdminState       = buf;
	::GetPrivateProfileString("Host", "operationalState", _T(""), buf, DC_BUFF_SIZE, lpszFullPath);	m_strOperationalState = buf;
	::GetPrivateProfileString("Host", "description"     , _T(""), buf, DC_BUFF_SIZE, lpszFullPath);	m_strDescription      = buf;
	::GetPrivateProfileString("Host", "networkUse"      , _T(""), buf, DC_BUFF_SIZE, lpszFullPath);	m_strNetworkUse       = buf;
	::GetPrivateProfileString("Host", "site"            , _T(""), buf, DC_BUFF_SIZE, lpszFullPath);	m_strSite             = buf;
	::GetPrivateProfileString("Host", "lastUpdateId"    , _T(""), buf, DC_BUFF_SIZE, lpszFullPath);	m_strLastWrittenUser  = buf;
	::GetPrivateProfileString("Host", "lastUpdateTime"  , _T(""), buf, DC_BUFF_SIZE, lpszFullPath);	m_strLastWrittenTime  = buf;
	::GetPrivateProfileString("Host", "TemplateList"    , _T(""), buf, DC_BUFF_SIZE, lpszFullPath);	CString strTemplateList = buf;
	::GetPrivateProfileString("Host", "templatePlayList", _T(""), buf, DC_BUFF_SIZE, lpszFullPath); CString strPlayTemplateList = buf;
	::GetPrivateProfileString("Host", "ContentsList"    , _T(""), buf, DC_BUFF_SIZE, lpszFullPath); CString strContentsList = buf;
	::GetPrivateProfileString("Host", "EtcList"         , _T(""), buf, DC_BUFF_SIZE, lpszFullPath); CString strEtcList    = buf;

	int nTemplatePos = 0;
	CString strTemplateId = strTemplateList.Tokenize(",", nTemplatePos);
	while(strTemplateId != _T(""))
	{
		TEMPLATE_INFO* pTemplateInfo = new TEMPLATE_INFO;
		pTemplateInfo->Load(lpszFullPath, strTemplateId);

		if(!AddTemplate(pTemplateInfo))
		{
			// 구현석 같은 템플릿 제거. 2009-6-15 같은 템플릿이 있으면 제대로 표현 못하는 버그 있음.
			delete pTemplateInfo;
			pTemplateInfo = NULL;
			strTemplateId = strTemplateList.Tokenize(",", nTemplatePos);
			continue;
		}

		//
		int nFramePos = 0;
		CString strFrameId = pTemplateInfo->strFrameList.Tokenize(",", nFramePos);
		while(strFrameId != _T(""))
		{
			FRAME_INFO* pFrameInfo = CreateNewFrame(pTemplateInfo->strId);
			pFrameInfo->Load(lpszFullPath, strFrameId);

			int nPlayContentsPos = 0;
			CString strPlayContentsId = pFrameInfo->strCyclePlayContentsIdList.Tokenize(",", nPlayContentsPos);
			while(strPlayContentsId != _T(""))
			{
				// 순환플레이컨텐츠상의 __preview_ 키워드가 있으면 무시한다
				//if(strPlayContentsId.Find(PREVIEW_FILE_KEY) < 0)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
					pPlayContentsInfo->Load(lpszFullPath, strPlayContentsId);
					pPlayContentsInfo->bIsDefaultPlayContents = true;

					pFrameInfo->arCyclePlayContentsList.Add(pPlayContentsInfo);
				}

				strPlayContentsId = pFrameInfo->strCyclePlayContentsIdList.Tokenize(",", nPlayContentsPos);
			}

			// 정시플레이컨텐츠 입력은 메인프레임에만 할수 있도록 하므로 로드시에도 메인프레임만 로드한다
			if(pFrameInfo->nGrade != 1) pFrameInfo->strTimeBasePlayContentsIdList = _T("");

			nPlayContentsPos = 0;
			strPlayContentsId = pFrameInfo->strTimeBasePlayContentsIdList.Tokenize(",", nPlayContentsPos);
			while(strPlayContentsId != _T(""))
			{
				// 정시플레이컨텐츠상의 __preview_ 키워드가 있으면 무시한다
				//if(strPlayContentsId.Find(PREVIEW_FILE_KEY) < 0)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
					pPlayContentsInfo->Load(lpszFullPath, strPlayContentsId);
					pPlayContentsInfo->bIsDefaultPlayContents = false;

					pFrameInfo->arTimePlayContentsList.Add(pPlayContentsInfo);
				}

				strPlayContentsId = pFrameInfo->strTimeBasePlayContentsIdList.Tokenize(",", nPlayContentsPos);
			}

			strFrameId = pTemplateInfo->strFrameList.Tokenize(",", nFramePos);
		}

		strTemplateId = strTemplateList.Tokenize(",", nTemplatePos);
	}

	// 강제로 PIP 로 설정등 과 같이 프레임에 강제 설정해야 하는 정보를 이 함수에서 설정한다.
	SetForceInfo();

	nTemplatePos = 0;
	strTemplateId = strPlayTemplateList.Tokenize(",", nTemplatePos);
	while(strTemplateId != _T(""))
	{
		int pos = 0;
		CString tmpl_id = strTemplateId.Tokenize("/", pos);
		CString play_times = strTemplateId.Tokenize("/", pos);

		// Modified by 정운형 2009-02-12 오후 5:58
		// 변경내역 :  Template id 파싱 오류
		tmpl_id.Trim();
		// Modified by 정운형 2009-02-12 오후 5:58
		// 변경내역 :  Template id 파싱 오류
		AddPlayTemplate(tmpl_id, play_times);

		strTemplateId = strPlayTemplateList.Tokenize(",", nTemplatePos);
	}

	//strContentsList.Replace(" ", _T(""));
	int nPos = 0;
	CString strTok = strContentsList.Tokenize(",", nPos);
	while(strTok != _T(""))
	{
		strTok.TrimLeft();
		strTok.TrimRight();
		CString strContentsID = strTok;
		strContentsID.Replace("Contents_", _T(""));
		CONTENTS_INFO* pContentsInfo = CDataContainer::getInstance()->GetContents(strContentsID);
		if(pContentsInfo == NULL)
		{
			pContentsInfo = new CONTENTS_INFO;
			pContentsInfo->Load(lpszFullPath, strTok);

			char szTemp[DC_BUFF_SIZE];
			if(!CDataContainer::getInstance()->AddContents(pContentsInfo, szTemp)){
				CString strTemp;
				strTemp.Format(DATACONTAINER_MSG007, strContentsID, szTemp);
//				AfxMessageBox(strTemp, MB_ICONWARNING);
			}
		}

		strTok = strContentsList.Tokenize(",", nPos);
	}

	// ETC리스트는 컨텐츠리스트와 ini 파일에 따로 저장되어 있지만
	// 프로그램 내부 처리상 같은 리스트로 관리하는것이 편하여 그렇게 함.
	nPos = 0;
	strTok = strEtcList.Tokenize(",", nPos);
	while(strTok != _T(""))
	{
		strTok.TrimLeft();
		strTok.TrimRight();
		CString strEtcID = strTok;
		strEtcID.Replace("Etc_", _T(""));
		CONTENTS_INFO* pEtcInfo = CDataContainer::getInstance()->GetContents(strEtcID);
		if(pEtcInfo == NULL)
		{
			pEtcInfo = new CONTENTS_INFO;
			pEtcInfo->Load(lpszFullPath, strTok);

			char szTemp[DC_BUFF_SIZE];
			if(!CDataContainer::getInstance()->AddContents(pEtcInfo, szTemp)){
				CString strTemp;
				strTemp.Format(DATACONTAINER_MSG007, strEtcID, szTemp);
//				AfxMessageBox(strTemp, MB_ICONWARNING);
			}
		}

		strTok = strEtcList.Tokenize(",", nPos);
	}

	// Load를 호출하는 부분에서 Load후 데이터를 변경시키므로 호출하는 부분의 Load 함수 마지막에서 호출하도록 함.
	//CreateBackupData();

	return true;
}

bool CDataContainer::Save(LPCTSTR lpszFullPath, bool bNew, CString strPreviewTemplateID/*=_T("")*/)
{
	// host
	WritePrivateProfileString("Host", "soundVolume"     , m_strSoundVolume     , lpszFullPath);
	WritePrivateProfileString("Host", "defaultTemplate" , m_strDefaultTemplate , lpszFullPath);
	WritePrivateProfileString("Host", "adminState"      , m_strAdminState      , lpszFullPath);
	WritePrivateProfileString("Host", "operationalState", m_strOperationalState, lpszFullPath);
	WritePrivateProfileString("Host", "description"     , m_strDescription     , lpszFullPath);

	if(!strPreviewTemplateID.IsEmpty())
	{
		WritePrivateProfileString("Host", "defaultTemplate" , strPreviewTemplateID , lpszFullPath);
	}

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		WritePrivateProfileString("Host", "networkUse", m_strNetworkUse, lpszFullPath);
		//WritePrivateProfileString("Host", "site", m_strSite, lpszFullPath);
	}
	else
	{
		//Studio에서 저장할때는 networkUse는 항상 0, site는 공백으로 저장한다
		WritePrivateProfileString("Host", "networkUse", "0", lpszFullPath);
		//WritePrivateProfileString("Host", "site", _T(""), lpszFullPath);
	}

	// Enterprise 관련 추가 항목
	WritePrivateProfileString("Host", "site"          , m_strSite           , lpszFullPath);
	WritePrivateProfileString("Host", "lastUpdateId"  , m_strLastWrittenUser, lpszFullPath);
	WritePrivateProfileString("Host", "lastUpdateTime", m_strLastWrittenTime, lpszFullPath);

	// save template, frame, playcontents
	CString strTemplateIdList;
	int nTemplateCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nTemplateCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(!strPreviewTemplateID.IsEmpty() && strPreviewTemplateID != template_link.pTemplateInfo->strId)
		{
			continue;
		}

		if(strTemplateIdList.GetLength() > 0) strTemplateIdList.Append(",");

		strTemplateIdList.Append("Template" + template_link.pTemplateInfo->strId);

		template_link.pTemplateInfo->Save(lpszFullPath, &(template_link.arFrameLinkList));

		int nFrameCount = template_link.arFrameLinkList.GetCount();
		for(int j=0; j<nFrameCount; j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			frame_link.pFrameInfo->Save(lpszFullPath, &(frame_link.pFrameInfo->arCyclePlayContentsList), &(frame_link.pFrameInfo->arTimePlayContentsList));

			for(int k=0; k<frame_link.pFrameInfo->arCyclePlayContentsList.GetCount(); k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);
				CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);

				pPlayContentsInfo->Save(lpszFullPath, pContentsInfo, bNew);
			}

			// 정시플레이컨텐츠 입력은 메인프레임에만 할수 있도록 하므로 저장시에도 메인프레임만 저장한다
			if(frame_link.pFrameInfo->nGrade == 1)
			{
				for(int k=0; k<frame_link.pFrameInfo->arTimePlayContentsList.GetCount(); k++)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);
					CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);

					pPlayContentsInfo->Save(lpszFullPath, pContentsInfo, bNew);
				}
			}
		}
	}

	::WritePrivateProfileString("Host", "TemplateList", strTemplateIdList, lpszFullPath);

	// playlist
	CString strPlayTemplateIdList = _T("");
	int nPlayTemplateCount = m_PlayTemplateList.GetCount();
	for(int i=0; i<nPlayTemplateCount; i++)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);

		if(!strPreviewTemplateID.IsEmpty() && strPreviewTemplateID != template_link.pTemplateInfo->strId)
		{
			continue;
		}

		if(strPlayTemplateIdList.GetLength() > 0) strPlayTemplateIdList.Append(",");

		strPlayTemplateIdList.Append(template_link.pTemplateInfo->strId);
		strPlayTemplateIdList.Append("/");
		strPlayTemplateIdList.Append(::ToString(template_link.nPlayTimes));
	}

	::WritePrivateProfileString("Host", "templatePlayList", strPlayTemplateIdList, lpszFullPath);

	SaveContents(lpszFullPath, bNew);

	if(strPreviewTemplateID.IsEmpty())
	{
		// 변경사항을 체크하기위해 백업본을 생성한다.
		CreateBackupData();
	}

	return true;
}

bool CDataContainer::SaveContents(LPCTSTR lpszFullPath, bool bNew)
{
	// Modified by 정운형 2009-03-09 오후 4:04
	// 변경내역 :  Contents map을 읽어서 Contents list를 config 파일에 저장한다.
	// ETC리스트는 컨텐츠리스트와 ini 파일에 따로 저장되어 있지만
	// 프로그램 내부 처리상 같은 리스트로 관리하는것이 편하여 그렇게 함.
	CString strEtcList;
	CString strContentsList;
	CONTENTS_INFO_MAP* pmapContents = CDataContainer::getInstance()->GetContentsMap();
	POSITION pos = pmapContents->GetStartPosition();
	while(pos)
	{
		CString strContentsID, strContentsSection;
		CONTENTS_INFO* pContentsInfo = NULL;
		pmapContents->GetNextAssoc(pos, strContentsID, (void*&)pContentsInfo);
		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType == CONTENTS_ETC)
		{
			strContentsSection.Format("Etc_%s", pContentsInfo->strId);
			if(pContentsInfo->Save(lpszFullPath, strContentsSection, bNew))
			{
				strEtcList += "Etc_";
				strEtcList += pContentsInfo->strId;
				strEtcList += ",";
			}
		}
		else
		{
			strContentsSection.Format("Contents_%s", pContentsInfo->strId);
			if(pContentsInfo->Save(lpszFullPath, strContentsSection, bNew))
			{
				strContentsList += "Contents_";
				strContentsList += pContentsInfo->strId;
				strContentsList += ",";
			}

			// 0000611: 플레쉬 컨텐츠 마법사
			// 플레쉬위저드 컨텐츠의 경우 관련 컨텐츠가 있는지 확인하여 기타리스트에 추가한다.
			if( pContentsInfo->nContentsType == CONTENTS_WIZARD &&
			   !pContentsInfo->strWizardFiles.IsEmpty()         )
			{
				int nPos = 0;
				CString strTok = pContentsInfo->strWizardFiles.Tokenize(",", nPos);
				while(strTok != _T(""))
				{
					strTok.Trim();
					CString strContentsID = strTok;
					strTok = pContentsInfo->strWizardFiles.Tokenize(",", nPos);

					// 기타리스트에서 정확하게 일치하는 컨텐츠가 있는지 확인
					if(strEtcList.Find("Etc_" + strContentsID + ",") >= 0) continue;

					CONTENTS_INFO* pEtcInfo = CDataContainer::getInstance()->GetContents(strContentsID);
					if(!pEtcInfo) continue;

					strContentsSection.Format("Etc_%s", strContentsID);
					if(pEtcInfo->Save(lpszFullPath, strContentsSection, bNew))
					{
						strEtcList += "Etc_";
						strEtcList += strContentsID;
						strEtcList += ",";
					}
				}
			}
		}
	}

	//Contents List 저장
	if(strContentsList != _T(""))
	{
		strContentsList.TrimRight(", ");
		::WritePrivateProfileString("Host", "ContentsList", strContentsList, lpszFullPath);
	}
	// Modified by 정운형 2009-03-09 오후 4:04
	// 변경내역 :  Contents map을 읽어서 Contents list를 config 파일에 저장한다.

	//Etc List 저장
	if(strEtcList != _T(""))
	{
		strEtcList.TrimRight(", ");
		::WritePrivateProfileString("Host", "EtcList", strEtcList, lpszFullPath);
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Preview를 위하여 지정된 template config 항목을 저장한다 \n
/// @param (LPCTSTR) lpszFullPath : (in) 저장할 config 파일의 경로
/// @param (CString) strTemplateID : (in) 저장할 template id
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDataContainer::SavePreview(LPCTSTR lpszFullPath, CString strTemplateID, bool bNew)
{
	return Save(lpszFullPath, bNew, strTemplateID);
}

// 강제로 PIP 로 설정등 과 같이 프레임에 강제 설정해야 하는 정보를 이 함수에서 설정한다.
void CDataContainer::SetForceInfo()
{
	for(int i=0; i<m_AllTemplateList.GetCount() ;i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);
		template_link.SetForceFrameInfo();
	}
}

void CDataContainer::DeleteAllBackupData()
{
	POSITION pos = m_ContentsMapOld.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo = NULL;
		m_ContentsMapOld.GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );
		if(pContentsInfo) delete pContentsInfo;
	}
	m_ContentsMapOld.RemoveAll();

	int nCount = m_AllTemplateListOld.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateListOld.GetAt(i);

		template_link.DeleteFrameList();
		delete template_link.pTemplateInfo;
	}
	m_AllTemplateListOld.RemoveAll();
	m_PlayTemplateListOld.RemoveAll();
}

void CDataContainer::CreateBackupData()
{
	DeleteAllBackupData();

	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pSrcContentsInfo = NULL;
		m_ContentsMap.GetNextAssoc( pos, strContentsId, (void*&)pSrcContentsInfo );

		if(pSrcContentsInfo)
		{
			CONTENTS_INFO* pContentsInfo = new CONTENTS_INFO;
			*pContentsInfo = *pSrcContentsInfo;
			m_ContentsMapOld[strContentsId] = (void*&)pContentsInfo;
		}
	}

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& SrcTemplateLink = m_AllTemplateList.GetAt(i);
		TEMPLATE_LINK BackTemplateLink;
		BackTemplateLink.Copy(SrcTemplateLink);
		m_AllTemplateListOld.Add(BackTemplateLink);
	}

	for(int i=0; i<m_PlayTemplateList.GetCount(); i++)
	{
		m_PlayTemplateListOld.Add(m_PlayTemplateList.GetAt(i));
	}
}

bool CDataContainer::IsModified()
{
	if(m_ContentsMap.GetCount() != m_ContentsMapOld.GetCount()) return true;

	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfoNew;
		m_ContentsMap.GetNextAssoc( pos, strContentsId, (void*&)pContentsInfoNew );

		CONTENTS_INFO* pContentsInfoOld = NULL;
		if(!m_ContentsMapOld.Lookup(strContentsId, (void*&)pContentsInfoOld)) return true;

		if(!pContentsInfoNew && !pContentsInfoOld) continue;

		if(!pContentsInfoNew) return true;
		if(!pContentsInfoOld) return true;

		if( !(*pContentsInfoNew == *pContentsInfoOld) ) return true;
	}

	if(m_AllTemplateList.GetCount() != m_AllTemplateListOld.GetCount()) return true;
	if(m_PlayTemplateList.GetCount() != m_PlayTemplateListOld.GetCount()) return true;

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		if( !(m_AllTemplateListOld.GetAt(i) == m_AllTemplateList.GetAt(i)) ) return true;
	}

	for(int i=0; i<m_PlayTemplateList.GetCount(); i++)
	{
		if( !(m_PlayTemplateListOld.GetAt(i) == m_PlayTemplateList.GetAt(i)) ) return true;
	}

	return false;
}

// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
bool CDataContainer::CheckOverlayModeFrame(CString strTemplateId, CString strOverlayModeFrameId, bool bMsg/*=true*/)
{
	if(strTemplateId.IsEmpty()) strOverlayModeFrameId.Empty();

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId != strTemplateId && !strTemplateId.IsEmpty()) continue;

		// 같은 레이아웃의 다른 프레임에 고화질 동영상 전용으로 설정되어 있는지 체크
		bool bOverlayModeFrame = !strOverlayModeFrameId.IsEmpty();
		for(int j=0; j<template_link.arFrameLinkList.GetCount() ;j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			if( frame_link.pFrameInfo->strId != strOverlayModeFrameId &&
				frame_link.pFrameInfo->strComment[0] == _T("9")       )
			{
				if(bOverlayModeFrame)
				{
					CString strMsg;
					strMsg.Format(DATACONTAINER_MSG008, template_link.pTemplateInfo->strId);
					if(bMsg) AfxMessageBox(strMsg);
					return false;
				}

				bOverlayModeFrame = true;
			}
		}

		// 고화질 동영상 전용으로 설정된 프레임이 없다면 다른 프레임에 동영상 플레이컨텐츠가 설정되었는지 체크할 필요 없음.
		if(!bOverlayModeFrame) continue;

		// 같은 레이아웃의 다른 프레임에 동영상 플레이컨텐츠가 설정되어 있는지 체크
		for(int j=0; j<template_link.arFrameLinkList.GetCount() ;j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			if( frame_link.pFrameInfo->strId != strOverlayModeFrameId &&
			    frame_link.pFrameInfo->strComment[0] != _T("9")       )
			{
				for(int k=0; k<frame_link.pFrameInfo->arCyclePlayContentsList.GetCount() ;k++)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);

					if(pPlayContentsInfo)
					{
						CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
						if(pContentsInfo && pContentsInfo->nContentsType == CONTENTS_VIDEO)
						{
							CString strMsg;
							if(strOverlayModeFrameId.IsEmpty())
							{
								strMsg.Format(DATACONTAINER_MSG009
											, template_link.pTemplateInfo->strId
											, frame_link.pFrameInfo->strId
											);
							}
							else
							{
								strMsg.Format(DATACONTAINER_MSG010
											, template_link.pTemplateInfo->strId
											, frame_link.pFrameInfo->strId
											);
							}
							if(bMsg) AfxMessageBox(strMsg);
							return false;
						}
					}
				}

				for(int k=0; k<frame_link.pFrameInfo->arTimePlayContentsList.GetCount() ;k++)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);

					if(pPlayContentsInfo)
					{
						CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
						if(pContentsInfo && pContentsInfo->nContentsType == CONTENTS_VIDEO)
						{
							CString strMsg;
							if(strOverlayModeFrameId.IsEmpty())
							{
								strMsg.Format(DATACONTAINER_MSG009
											, template_link.pTemplateInfo->strId
											, frame_link.pFrameInfo->strId
											);
							}
							else
							{
								strMsg.Format(DATACONTAINER_MSG010
											, template_link.pTemplateInfo->strId
											, frame_link.pFrameInfo->strId
											);
							}
							if(bMsg) AfxMessageBox(strMsg);
							return false;
						}
					}
				}
			}
		}
	}

	return true;
}

// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
bool CDataContainer::CheckContentsCanBeAddedToFrame(CString strTemplateId, CString strFrameId, CString strContentsId)
{
	CONTENTS_INFO* pContents = GetContents(strContentsId);
	if(!pContents) return true;

	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(strTemplateId);
	if(!pTemplateLink) return true;

	if(pContents->nContentsType == CONTENTS_ETC)
	{
		return false;
	}

	// 창일향 : 메인 프레임이 선택되었을 때는 패키지를 추가도 삭제도,  편집도 할 수 없다.  메인 프레임외에 다른 프레임들만 선택하고 플레이컨텐츠 추가, 삭제, 편집이 가능하다.
	if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer )
	{
		FRAME_LINK* pFrameLink = GetFrameLink(strTemplateId, strFrameId);
		if(pFrameLink && pFrameLink->pFrameInfo->nGrade == 1)
		{
			return false;
		}
	}

	if(pContents->nContentsType != CONTENTS_VIDEO)
	{
		return true;
	}

	// 같은 레이아웃의 다른 프레임에 고화질 동영상 전용으로 설정되어 있는지 체크
	CString strOverlayModeFrameId;
	for(int j=0; j<pTemplateLink->arFrameLinkList.GetCount() ;j++)
	{
		FRAME_LINK& frame_link = pTemplateLink->arFrameLinkList.GetAt(j);

		if( frame_link.pFrameInfo->strComment[0] == _T("9") )
		{
			strOverlayModeFrameId = frame_link.pFrameInfo->strId;
			break;
		}
	}

	// 고화질 동영상 전용으로 설정된 프레임이 없다면 다른 프레임에 동영상 플레이컨텐츠가 설정되었는지 체크할 필요 없음.
	if(strOverlayModeFrameId.IsEmpty()) return true;

	// 비디오 컨텐츠를 넣으려는 프레임이 고화질 동영상 전용 프레임인경우 true 리턴
	return (strFrameId == strOverlayModeFrameId);
}

// 0000891: 어떤 프레임에 자식 플레이컨텐츠만 있고, 자식 플레이컨텐츠외에 아무런 다른 플레이컨텐츠가 없다면 저장시 경고해주어야함.
bool CDataContainer::CheckPlayContentsRelation()
{
	for(int i=0; i<m_PlayTemplateList.GetCount(); i++)
	{
		bool bPriFrm = false;
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);

		for(int j=0; j<template_link.arFrameLinkList.GetCount(); j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			int nPlayContentsCount = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
			if(nPlayContentsCount == 0) continue;

			int nParentPlayContentsCount = 0;
			for(int nPlayContentsIdx=0; nPlayContentsIdx<nPlayContentsCount; nPlayContentsIdx++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(nPlayContentsIdx);
				if(pPlayContentsInfo && !pPlayContentsInfo->strParentPlayContentsId.IsEmpty())
				{
					nParentPlayContentsCount++;
				}
			}

			// 자식 플레이컨텐츠외에 아무런 다른 플레이컨텐츠가 없다면
			if(nParentPlayContentsCount == nPlayContentsCount)
			{
				CString szMsg;
#ifdef _ML_ENG_
				szMsg.Format(DATACONTAINER_MSG003, frame_link.pFrameInfo->strId, template_link.pTemplateInfo->strId);
#else
				szMsg.Format(DATACONTAINER_MSG003, template_link.pTemplateInfo->strId, frame_link.pFrameInfo->strId);
#endif
				AfxMessageBox(szMsg, MB_ICONWARNING);
				return false;
			}
		}
	}

	return true;
}
