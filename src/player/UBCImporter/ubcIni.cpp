/*
 *  Copyright �� 2002 SQISoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2002/12/05
 *  File name   : scProperties.C
 */

//----------------------------------------------------------------------------
//	Include Header File
//---------------------------------------------------------------------------- 

#include "StdAfx.h"
#include "ubcIni.h"
#include "COP/ciEnv.h"
#include "COP/ciStringUtil.h"
#include "COP/ciStringTokenizer.h"

#define	INI_MAX_LINE			1024
#define	INI_ITEM_DELIMITER		'.'

ubcIni::ubcIni(const char* filename, const char* configPath, const char* sub)
{

	const char* defaultConfig = _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\config");
	if(configPath==0 || strlen(configPath)==0){
		_fullpath = defaultConfig;
	}else{
		if(strcmp(configPath,"CONFIGROOT")==0){
			const char* cfg = ciEnv::getenv(configPath);
			if(cfg && strlen(cfg)){
				_fullpath = cfg;
			}else{
				_fullpath = defaultConfig;
			}
		}else{
			_fullpath = configPath;
		}
	}

	_fullpath += "\\";
	if(sub && strlen(sub)){
		_fullpath += sub;
		_fullpath += "\\";
	}
	_fullpath += filename;
	_fullpath += ".ini";

}

boolean 
ubcIni::get(const char* section,const char* key, ciString& value, const char* defaultValue)
{
	if(section==0 || strlen(section)==0 || key==0 || strlen(key)==0)	{return false;}

#ifdef WIN32
	char	szValue[1024*100];
	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString(section,key,defaultValue,szValue,sizeof(szValue),_fullpath.c_str());
	value=szValue;
#endif
	if (value.empty()) value = defaultValue;
	//utvDEBUG(("ini value founded : %s.%s=%s", section,key, value.c_str()));
	return true;
}

boolean 
ubcIni::get(const char* section,const char* key, CString& value, const char* defaultValue)
{
	if(section==0 || strlen(section)==0 || key==0 || strlen(key)==0)	{return false;}

#ifdef WIN32
	char	szValue[1024*100];
	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString(section,key,defaultValue,szValue,sizeof(szValue),_fullpath.c_str());
	value=szValue;
#endif
	if (value.IsEmpty()) value = defaultValue;
	//utvDEBUG(("ini value founded : %s.%s=%s", section,key, value.GetBuffer()));
	return true;
}

boolean 
ubcIni::_get(const char* section,const char* key, int& value)
{
	if(section==0 || strlen(section)==0 || key==0 || strlen(key)==0)	{return false;}
#ifdef WIN32
	char	szValue[32];
	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString(section,key,"0",szValue,sizeof(szValue),_fullpath.c_str());
	value=atoi(szValue);
#endif
	//utvDEBUG(("ini value founded : %s.%s=%d", section,key,value));
	return true;
}

boolean 
ubcIni::get(const char* section,const char* key, ciBoolean& value)
{
	ciString strVal;
	get(section, key, strVal);
	if (strVal.empty()) return ciFalse;
	ciStringUtil::toLower(strVal);
	if (strVal == "true" || strVal == "1") value = true;
	else value = false;
	return ciTrue;
}

boolean 
ubcIni::get(const char* section,const char* key, ciShort& value)
{
	int i=(int)value;
	_get(section,key,i);
	value=(ciShort)i;
	return ciTrue;
}
boolean 
ubcIni::get(const char* section,const char* key, ciLong& value)
{
	int i=(int)value;
	_get(section,key,i);
	value=(ciLong)i;
	return ciTrue;
}
boolean 
ubcIni::get(const char* section,const char* key, ciUShort& value)
{
	int i=(int)value;
	_get(section,key,i);
	value=(ciUShort)i;
	return ciTrue;
}
boolean 
ubcIni::get(const char* section,const char* key, ciULong& value)
{
	int i=(int)value;
	_get(section,key,i);
	value=(ciULong)i;
	return ciTrue;
}
boolean 
ubcIni::get(const char* section,const char* key, ciTime& value)
{
	if(section==0 || strlen(section)==0 || key==0 || strlen(key)==0)	{return false;}

#ifdef WIN32
	char	szValue[1024*100];
	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString(section,key,"",szValue,sizeof(szValue),_fullpath.c_str());
	value.set(szValue);
#endif
	//utvDEBUG(("ini value founded : %s.%s=%s",section,key,szValue));
	return true;
}

boolean 
ubcIni::set(const char* section, const char* key , const char* value)
{
   	if(!section || strlen(section)==0) {
		return false;
	}
	if(!key || strlen(key)==0) {
		return false;
	}
#ifdef WIN32
	//utvDEBUG(("ini value update : %s.%s=%s", section,key, value));
	return WritePrivateProfileString(section,key,value,_fullpath.c_str());
#endif
	return true;
}

boolean 
ubcIni::set(const char* section, const char* key , ciBoolean value)
{
	if (value) 
		return set(section, key, "TRUE");
	else 
		return set(section, key, "FALSE");
}

boolean 
ubcIni::set(const char* section, const char* key , ciShort value)
{
	char buf[32];
	sprintf(buf,"%d",value);
	return set(section,key,buf);
}

boolean 
ubcIni::set(const char* section, const char* key , ciUShort value)
{
	char buf[32];
	sprintf(buf,"%d",value);
	return set(section,key,buf);
}

boolean 
ubcIni::set(const char* section, const char* key , ciLong value)
{
	char buf[32];
	sprintf(buf,"%d",value);
	return set(section,key,buf);
}

boolean 
ubcIni::set(const char* section, const char* key , ciULong value)
{
	char buf[32];
	sprintf(buf,"%d",value);
	return set(section,key,buf);
}

boolean 
ubcIni::set(const char* section, const char* key , ciTime value)
{
	return set(section,key,value.getTimeString());
}

boolean
ubcConfig::lvc_setProperty(const char* name, const char* value)
{
	ciDEBUG(1,("lvc_setProperty(%s=%s)",name,value));

	if(strcmp(name,"STARTER.APP_LIST")==0){
		ubcConfig aIni("UBCStarter");
		if (!aIni.set("ROOT","APP_LIST", value)) {
			ciERROR(("setProperties %s=%s failed", name,value));
			return false;
		}
		ciDEBUG(1,("UBCStarter.ini %s=%s succeed", name,value));
		return true;
	}
	if(strncmp(name,"STARTER.APP_",strlen("STARTER.APP_"))==0) {
		ciStringTokenizer aTokenizer(name,".");
		if(aTokenizer.countTokens() < 3) {
			return false;
		}
		aTokenizer.nextToken();
		ciString buf = aTokenizer.nextToken();
		ciString appId = buf.substr(4);
		ciString attr = aTokenizer.nextToken();
		ciString val = value;
		if(val == "TRUE" || val == "True") val="true";
		if(val == "FALSE" || val == "False") val="false";

		ubcConfig aIni("UBCStarter");
		if(!aIni.set(appId.c_str(),attr.c_str(),val.c_str())){
			ciERROR(("setProperties %s=%s failed", name,val.c_str()));
			return false;
		}
		ciDEBUG(1,("UBCStarter.ini [%s],%s=%s succeed", appId.c_str(),attr.c_str(),val.c_str()));
		return true;
	}
	ciWARN(("%s=%s is not supported by low version compatibility function", name,value));
	return false;
}

boolean
ubcConfig::lvc_getProperty(const char* name, ciString& value)
{
	if(strncmp(name,"STARTER.APP_",strlen("STARTER.APP_"))==0) {
		ciStringTokenizer aTokenizer(name,".");
		if(aTokenizer.countTokens() < 3) {
			return false;
		}
		aTokenizer.nextToken();
		ciString buf = aTokenizer.nextToken();
		
		ciString filename = "UBCStarter";
		ciString section;
		if(buf=="APP_LIST"){
			section = "ROOT";
		}else{
			section = buf.substr(4);
		}
		ciString attr = aTokenizer.nextToken();

		ubcConfig aIni(filename.c_str());
		if(!aIni.get(section.c_str(),attr.c_str(),value)){
			ciERROR(("ini [%s],%s get failed", section.c_str(), attr.c_str()));
			return false;
		}
		if(value == "TRUE" || value == "True") value="true";
		if(value == "FALSE" || value == "False") value="false";

		ciDEBUG(1,("UBCStarter.ini [%s],%s=%s get succeed", section.c_str(),attr.c_str(),value.c_str()));
		return true;
	}
	ciWARN(("%s is not supported by low version compatibility function",name));
	return false;
}

