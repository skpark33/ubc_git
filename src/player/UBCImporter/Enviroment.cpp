#include "stdafx.h"
#include "Enviroment.h"
#ifndef _UBC_IMPORTER_
#include "mainfrm.h"
#include "SimpleFtp.h"
#else
#include "UBCImporterDlg.h"
#endif
#include "LocalHostInfo.h"
#include "libFTPClient\FTPClient.h"

#ifdef _TAO
#include "CopModule.h"
#include "CMN/libCommon/ubcMux.h"
#include "CMN/libCommon/ubcHost.h"
#endif//_TAO

CString g_szTraceLogBuf = _T("");
CEnviroment* CEnviroment::m_pThis = NULL;

CEnviroment::CEnviroment() :
#ifdef _UBCSTUDIO_EE_
	m_Edition(eStudioEE),
	m_Authority(eAuthNoUser),
#else
	m_Edition(eStudioPE),
	m_Authority(eAuthSuper),
#endif//_UBCSTUDIO_EE_
	m_bLicense(false)
{
	m_TotalContentsSize = 0;

	m_lMaxContentsSize = 0;
	m_nMaxContentsCount = 0;
	m_nMaxPlayTemplateCount = 0;

	m_bUseTimeBasePlayContents = false;
	m_bUseClickPlayContents = false;	// 0000782: 부모-자식 스케줄 기능을 수정한다.

	m_bUseTV = false;

	m_szSite.Empty();
	m_szLoginID.Empty();
	m_szPassword.Empty();

	m_strPackage.Empty();

	m_lsSite.RemoveAll();
	m_lsHost.RemoveAll();
	m_lsPackage.RemoveAll();
	m_lsTempRect.RemoveAll();

	InitPath();
	InitLoginInfo();

	Load_ini();
#ifdef _UBCSTUDIO_EE_
	Load_ee_ini();
#else
	Load_pe_ini();
#endif//_UBCSTUDIO_EE_
}

CEnviroment::~CEnviroment()
{
	RemovePackage();
	RemoveHost();
}

CEnviroment* CEnviroment::GetObject()
{
	if(m_pThis)
		return m_pThis;
	m_pThis = new CEnviroment;
	return m_pThis;
}

void CEnviroment::Release()
{
	if(!m_pThis)	return;
	delete m_pThis;
	m_pThis = NULL;
}

void CEnviroment::InitPath()
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	m_szDrive = szDrive;
	m_szPath = szPath;
	m_szFile = m_szName = szFilename;
	m_szFile += m_szExt = szExt;
	m_szModule = szModule;
}

void CEnviroment::Log(const char* szfilename, int nLine, const char* szMsg)
{
#ifdef _TAO
	copTrace(szfilename, nLine, szMsg);
#endif//_TAO
}

void CEnviroment::InitLoginInfo()
{
#ifdef _UBCSTUDIO_EE_
	CString szFile = m_szDrive + m_szPath + _T("data\\");
	szFile += LOGIN_DAT;

	std::ifstream ifLogin(szFile, ios::in);
	if( ifLogin.fail() ) {
		ifLogin.close();
		return;
	}

	SLoginInfo Info;
	ZeroMemory(&Info, sizeof(Info));
	ifLogin.read(reinterpret_cast<char*>(&Info),sizeof(SLoginInfo));
	if( ifLogin.fail() ) {
		ifLogin.close();
		return;
	}
	ifLogin.close();

	m_szSite = Info.szSite;
	m_szLoginID = Info.szID;
	m_szPassword = Encrypt(Info.szPW, Info.nPWSize);
#endif//_UBCSTUDIO_EE_
}

void CEnviroment::SaveLoginInfo(bool bCheck)
{
	SLoginInfo Info;
	ZeroMemory(&Info, sizeof(Info));

	CString szFile = m_szDrive + m_szPath + _T("data\\");
	szFile += LOGIN_DAT;

	std::ofstream ofLogin(szFile, ios_base::binary);
	if( ofLogin.fail() ) {
		ofLogin.close();
		return;
	}

	if(bCheck){
		strcpy(Info.szSite, m_szSite);
		strcpy(Info.szID, m_szLoginID);
		strcpy(Info.szPW, m_szPassword);

		Info.nPWSize = strlen(Info.szPW);
		Encrypt(Info.szPW, Info.nPWSize);
	}

	ofLogin.write(reinterpret_cast<char*>(&Info),sizeof(SLoginInfo));
	if( ofLogin.fail() ) {
		ofLogin.close();
		return;
	}

	ofLogin.close();
}

char* CEnviroment::Encrypt(char* buf, int nSize)
{
	char cKey = 'A';
	for(int i = 0; i < nSize; i++){
		cKey++;
		if(cKey >= 'z')	cKey = '0';
		buf[i] ^= cKey;
	}
	return buf;
}

UINT CEnviroment::InitPackageList(bool bAll)
{
	if(bAll || m_lsPackage.GetCount() == 0){// 전체를 갱신
		RemovePackage();
#ifdef _TAO
		copGetPackage((GetEnvPtr()->m_Authority==CEnviroment::eAuthSuper)?_T("*"):GetEnvPtr()->m_szSite);
#endif//_TAO
	}else{// Local drive package 만 갱신
		RemovePackageWithout(STR_ENV_SERVER);
	}

#ifndef _UBC_IMPORTER_
	CPtrArray& arLocalHost = ((CMainFrame*)AfxGetMainWnd())->m_aryLocalHostInfo;
#else
	CPtrArray& arLocalHost = ((CUBCImporterDlg*)AfxGetMainWnd())->m_aryLocalHostInfo;
#endif

	CString strDrive, strNetworkUse, strHostName, strDesc, strSite, strWrittenTime, strWrittenID;

	for(int i = 0; i < arLocalHost.GetCount(); i++)
	{
		CLocalHostInfo* pLocalHost = (CLocalHostInfo*)arLocalHost.GetAt(i);
		if(!pLocalHost)	continue;

		pLocalHost->GetLocalHostInfo(strDrive, strNetworkUse, strHostName, strDesc, strSite, strWrittenTime, strWrittenID);

		if(strHostName.Find(SAMPLE_FILE_KEY) != -1) continue;
		if(strHostName.Find(PREVIEW_FILE_KEY) != -1) continue;

		SPackageInfo stLocalinfo;
		SPackageInfo* pServerInfo = FindPackage(strHostName, STR_ENV_SERVER);

		stLocalinfo.bModify = false;

		if(pServerInfo)
		{
			// 로컬에 존재하면 서버는 보이지 않도록 함
			pServerInfo->bShow = false;

			// 서버와 동일한 ProcID 를 부여함
			stLocalinfo.szProcID = pServerInfo->szProcID;

			// 파일 비교후 변경 여부 체크
			if(strWrittenTime.IsEmpty())
			{
				stLocalinfo.bModify = (strcmp(pServerInfo->szUpdateTime, STR_ENV_EMPTY_TIME) != 0);
			}
			else
			{
				stLocalinfo.bModify = (strWrittenTime.Compare(pServerInfo->szUpdateTime) != 0);
			}
		}

		stLocalinfo.szDrive = strDrive;
		stLocalinfo.tmUpdateTime = 0;
		stLocalinfo.szSiteID = strSite;
		stLocalinfo.szPackage = strHostName;
		stLocalinfo.szDescript = strDesc;
		stLocalinfo.szUpdateUser = (strWrittenID.IsEmpty()?m_szLoginID:strWrittenID);

		if(strWrittenTime.IsEmpty() && !stLocalinfo.szProcID.IsEmpty())
			stLocalinfo.szUpdateTime = STR_ENV_EMPTY_TIME;
		else
			stLocalinfo.szUpdateTime = strWrittenTime;

		AddPackage(&stLocalinfo);
	}

	return 0;
}

void CEnviroment::RemovePackage()
{
	POSITION pos = m_lsPackage.GetHeadPosition();
	
	while(pos){
		SPackageInfo* pInfo = m_lsPackage.GetNext(pos);
		if(!pInfo) continue;
		delete pInfo;
		pInfo = NULL;
	}
	m_lsPackage.RemoveAll();
}

void CEnviroment::RemovePackageWithout(LPCTSTR szLocation)
{
	SPackageInfo* pInfo;
	POSITION pos = m_lsPackage.GetTailPosition();

	while(pos){
		POSITION posCur = pos;
		pInfo = m_lsPackage.GetPrev(pos);
		if(!pInfo)	continue;

		if(szLocation && pInfo->szDrive == szLocation)
			continue;

		delete pInfo;
		pInfo = NULL;

		m_lsPackage.RemoveAt(posCur);
	}
}

void CEnviroment::AddPackage(SPackageInfo* pInfo)
{
	SPackageInfo* pNewInfo = FindPackage(pInfo->szPackage, pInfo->szDrive);

	if(!pNewInfo)
	{
		pNewInfo = new SPackageInfo;
		*pNewInfo = *pInfo;
		m_lsPackage.AddTail(pNewInfo);
	}

	if(strlen(pInfo->szSiteID) && pInfo->szDrive == STR_ENV_SERVER){
		POSITION pos = m_lsSite.Find(pInfo->szSiteID);
		if(pos == 0){
			m_lsSite.AddTail(pInfo->szSiteID);
		}
	}
}

void CEnviroment::ChangeCurPackage(SPackageInfo* pInfo)
{
	ASSERT(pInfo);
	SPackageInfo* pNewInfo = FindPackage(pInfo->szPackage, pInfo->szDrive);
//	if(!pNewInfo){
//		pNewInfo = FindPackage(pInfo->szPackage, STR_ENV_SERVER);
//	}
	if(!pNewInfo)
	{
		pNewInfo = new SPackageInfo;
		*pNewInfo = *pInfo;
		m_lsPackage.AddTail(pNewInfo);
	}

	m_PackageInfo = *pInfo;
}

SPackageInfo* CEnviroment::FindPackage(CString strInPackage, CString strInDrive)
{
	strInPackage.MakeLower();
	strInDrive.MakeLower();

	SPackageInfo* pInfo;
	POSITION pos = m_lsPackage.GetHeadPosition();

	while(pos)
	{
		pInfo = m_lsPackage.GetNext(pos);
		if(!pInfo) continue;

		CString szDrv = pInfo->szDrive;
		CString szPackage = pInfo->szPackage;

		szDrv.MakeLower();
		szPackage.MakeLower();

		if(szDrv == strInDrive && szPackage == strInPackage)
			return pInfo;
	}

	return NULL;
}

void CEnviroment::GetTempMaxSize(int& cx, int& cy)
{
	cx = 0;
	cy = 0;

	POSITION pos = m_lsTempRect.GetHeadPosition();
	while(pos){
		CRect rc = m_lsTempRect.GetNext(pos);
		if(cx < rc.Width())
			cx = rc.Width();
		if(cy < rc.Height())
			cy = rc.Height();
	}

	if(cx == 0)
		cx = 0x7FFFFFFF;
	if(cy == 0)
		cy = 0x7FFFFFFF;
}

void CEnviroment::GetTempMinSize(int& cx, int& cy)
{
	cx = 0x7FFFFFFF;
	cy = 0x7FFFFFFF;

	POSITION pos = m_lsTempRect.GetHeadPosition();
	for(int i = 0; i < m_lsTempRect.GetCount(); i++){
		CRect rc = m_lsTempRect.GetNext(pos);
		if(cx > rc.Width())
			cx = rc.Width();
		if(cy > rc.Height())
			cy = rc.Height();
	}

	if(cx == 0x7FFFFFFF)
		cx = 0;
	if(cy == 0x7FFFFFFF)
		cy = 0;
}

void CEnviroment::InitHostInfo(CString strSiteId, CString strHostId, CString strWhere/*=""*/)
{
#ifdef _TAO
	ubcHost aHostInfo;

	if(strSiteId.IsEmpty()) strSiteId = (m_Authority == eAuthSuper ? m_szSite : _T("*"));
	if(strHostId.IsEmpty()) strHostId = _T("*");

	if(strWhere.IsEmpty())
	{
		aHostInfo.init(strSiteId, strHostId);
	}
	else
	{
		aHostInfo.init(strSiteId, strHostId, strWhere);
	}

//	aHostInfo.printIt();

	ubcHostInfoList* list = aHostInfo.get();
	ubcHostInfoList::iterator itr;

	RemoveHost();

	for(itr=list->begin(); itr!=list->end(); itr++)
	{
		ubcHostInfo* aData  = (*itr);
		if(!aData) continue;

		SHostInfo* pInfo = new SHostInfo;

		pInfo->displayNo = 1;
		pInfo->siteId = aData->siteId.c_str();
		pInfo->hostId= aData->hostId.c_str();
		pInfo->operationalState = aData->operationalState;
		pInfo->displayCounter  = aData->displayCounter;
		pInfo->autoPackage = aData->autoSchedule1.c_str();
		pInfo->currentPackage = aData->currentSchedule1.c_str();
		pInfo->lastPackage = aData->lastSchedule1.c_str();
		pInfo->lastPackageTime = aData->lastScheduleTime1.getTime();
		pInfo->networkUse = aData->networkUse1;

		m_lsHost.AddTail(pInfo);

		if(pInfo->displayCounter != 2)
			continue;

		pInfo = new SHostInfo;

		pInfo->displayNo = 2;
		pInfo->siteId = aData->siteId.c_str();
		pInfo->hostId= aData->hostId.c_str();
		pInfo->operationalState = aData->operationalState;
		pInfo->displayCounter  = aData->displayCounter;
		pInfo->autoPackage = aData->autoSchedule2.c_str();
		pInfo->currentPackage = aData->currentSchedule2.c_str();
		pInfo->lastPackage = aData->lastSchedule2.c_str();
		pInfo->lastPackageTime = aData->lastScheduleTime2.getTime();
		pInfo->networkUse = aData->networkUse2;

		m_lsHost.AddTail(pInfo);
	}
#endif//_TAO
}

void CEnviroment::InitHostInfo(CString strPlayContentsId, CList<SHostInfo*>& lsHost)
{
#ifdef _TAO
	ubcHost aHostInfo;

	aHostInfo.init(strPlayContentsId);

//	aHostInfo.printIt();

	ubcHostInfoList* list = aHostInfo.get();
	ubcHostInfoList::iterator itr;

	for(itr=list->begin(); itr!=list->end(); itr++)
	{
		ubcHostInfo* aData  = (*itr);
		if(!aData) continue;

		SHostInfo* pInfo = new SHostInfo;

		pInfo->displayNo = 1;
		pInfo->siteId = aData->siteId.c_str();
		pInfo->hostId= aData->hostId.c_str();
		pInfo->operationalState = aData->operationalState;
		pInfo->displayCounter  = aData->displayCounter;
		pInfo->autoPackage = aData->autoSchedule1.c_str();
		pInfo->currentPackage = aData->currentSchedule1.c_str();
		pInfo->lastPackage = aData->lastSchedule1.c_str();
		pInfo->lastPackageTime = aData->lastScheduleTime1.getTime();
		pInfo->networkUse = aData->networkUse1;

		lsHost.AddTail(pInfo);

		if(pInfo->displayCounter != 2)
			continue;

		pInfo = new SHostInfo;

		pInfo->displayNo = 2;
		pInfo->siteId = aData->siteId.c_str();
		pInfo->hostId= aData->hostId.c_str();
		pInfo->operationalState = aData->operationalState;
		pInfo->displayCounter  = aData->displayCounter;
		pInfo->autoPackage = aData->autoSchedule2.c_str();
		pInfo->currentPackage = aData->currentSchedule2.c_str();
		pInfo->lastPackage = aData->lastSchedule2.c_str();
		pInfo->lastPackageTime = aData->lastScheduleTime2.getTime();
		pInfo->networkUse = aData->networkUse2;

		lsHost.AddTail(pInfo);
	}
#endif//_TAO
}

void CEnviroment::RemoveHost()
{
	POSITION pos = m_lsHost.GetHeadPosition();
	while(pos){
		SHostInfo* pInfo = m_lsHost.GetNext(pos);
		if(!pInfo) continue;
		delete pInfo;
		pInfo = NULL;
	}
	m_lsHost.RemoveAll();
}

bool CEnviroment::Connect(CSimpleFtp* pFtp, LPCTSTR szSite)
{
#ifdef _TAO
	if(!pFtp)	return false;
	ubcMuxData* aData = ubcMux::getInstance()->getMuxData(szSite);
	if(!aData)	return false;
	
	CString szMsg;
//	TraceLog(("Connect(%s)",aData->ipAddress.c_str()));
	return pFtp->Connect(aData->getFTPAddress(),aData->ftpId.c_str(),aData->ftpPasswd.c_str(),aData->ftpPort);
#else
	return false;
#endif//_TAO
}
// skpark 2010.10.07  사용되지 않는 코드 제외
//bool CEnviroment::GetFtpInfo(LPCTSTR szSite, SFtpInfo* pInfo)
//{
//#ifdef _TAO
//	ubcMuxData* aData = ubcMux::getInstance()->getMuxData(szSite);
//	if(!aData)	return false;
//	
//	strncpy(pInfo->szPmId, aData->pmId.c_str(), sizeof(pInfo->szPmId));
//	strncpy(pInfo->szIP, aData->ipAddress.c_str(), sizeof(pInfo->szIP));
//	strncpy(pInfo->szID, aData->ftpId.c_str(), sizeof(pInfo->szID));
//	strncpy(pInfo->szPW, aData->ftpPasswd.c_str(), sizeof(pInfo->szPW));
//	pInfo->nPort = aData->ftpPort;
//	return true;
//#else
//	return false;
//#endif//_TAO
//}

bool CEnviroment::GetFile(LPCTSTR szFile, LPCTSTR szRemote, LPCTSTR szLocal, LPCTSTR szSite)
{
	bool bRet = false;
#ifdef _TAO
//	CSimpleFtp ftp;
	nsFTP::CFTPClient FtpClient;

	ubcMuxData* aData = NULL;
	if(szSite)
		aData = ubcMux::getInstance()->getMuxData(szSite);
	else
		aData = ubcMux::getInstance()->getMuxData(m_szSite);

	if(!aData){
		return bRet;
	}

	//CString szMsg;
	//if(!ftp.Connect(aData->ipAddress.c_str(),aData->ftpId.c_str(),aData->ftpPasswd.c_str(),aData->ftpPort)){
	//	return bRet;
	//}
	//ftp.SetTimeOut(300);
	//if(ftp.GetFile(szFile, szRemote, szLocal) != CSimpleFtp::eSuccess){
	//	return bRet;
	//}
	//if(ftp.IsTimeOut()){
	//	return bRet;
	//}

	nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
	if (!FtpClient.Login(loginInfo)) {
		return bRet;
	}

	FtpClient.SetResumeMode(false);
	FtpClient.ChangeWorkingDirectory("/");

	bRet = FtpClient.DownloadFile(
			tstring(szRemote)+tstring(szFile), 
			tstring(szLocal)+tstring(szFile),
			nsFTP::CRepresentation(nsFTP::CType::Image()), true);

	FtpClient.Logout();

	// 파일 다운로드가 안됐지만 파일이 생성된경우 파일을 삭제해야 함.
	if(!bRet){
		CString szBuf = szLocal;
		szBuf += szFile;

		CFileFind ff;
		if(ff.FindFile(szBuf)){
			::DeleteFile(szBuf);
		}
		ff.Close();
	}
#endif//_TAO

	return bRet;
}

bool CEnviroment::PutFile(LPCTSTR szFile, LPCTSTR szLocal, LPCTSTR szRemote, LPCTSTR szSite)
{
	bool bRet = false;
#ifdef _TAO
//	CSimpleFtp ftp;
	nsFTP::CFTPClient FtpClient;

	ubcMuxData* aData = NULL;
	if(szSite)
		aData = ubcMux::getInstance()->getMuxData(szSite);
	else
		aData = ubcMux::getInstance()->getMuxData(m_szSite);

	if(!aData){
		return bRet;
	}
	
	//CString szMsg;
	//if(!ftp.Connect(aData->ipAddress.c_str(),aData->ftpId.c_str(),aData->ftpPasswd.c_str(),aData->ftpPort)){
	//	return bRet;
	//}
	//ftp.SetTimeOut(300);
	//if(ftp.PutFile(szFile, szLocal, szRemote) != CSimpleFtp::eSuccess){
	//	return bRet;
	//}
	//if(ftp.IsTimeOut()){
	//	return bRet;
	//}

	nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
	if (!FtpClient.Login(loginInfo)) {
		return bRet;
	}

	FtpClient.SetResumeMode(false);
	FtpClient.ChangeWorkingDirectory("/");

	FtpClient.MakeDirectory(tstring(szRemote));
	bRet = FtpClient.UploadFile(
			tstring(szLocal)+tstring(szFile), 
			tstring(szRemote)+tstring(szFile),
			false, nsFTP::CRepresentation(nsFTP::CType::Image()), true);

	FtpClient.Logout();
#endif//_TAO
	return bRet;
}

void CEnviroment::SetPathOpenFile(LPCTSTR str)
{
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);
	::WritePrivateProfileString("STUDIO", "PathOpenFile", str, szPath);
	m_szPathOpenFile = str;
}

bool CEnviroment::IsValidOpenFolder()
{
	if(m_szPathOpenFile.IsEmpty())
		return false;

	CString szFolder = m_szPathOpenFile;
	if(szFolder.Right(1)=="\\")
		szFolder += "*.*";

	CFileFind ff;
	return ff.FindFile(szFolder);
}

void CEnviroment::Load_ini()
{
	char buf[1024];
	CString szPath, szTemp;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, UBCVARS_INI);

	::GetPrivateProfileString(_T("CUSTOMER_INFO"), _T("NAME"), _T("SQISOFT"), buf, 1024, szPath);
	szTemp = buf;
	if(szTemp == _T("SQISOFT"))
		m_Customer = eSQISOFT;
	else if(szTemp == _T("NARSHA"))
		m_Customer = eNARSHA;
	else if(szTemp == _T("LOTTE"))
		m_Customer = eLOTTE;
	else if(szTemp == _T("ADASSET"))
		m_Customer = eADASSET;
	else
		m_Customer = eSQISOFT;

	::GetPrivateProfileString(_T("STUDIO"), _T("TOTAL_CONTENTS"), _T("0"), buf, 1024, szPath);
	m_TotalContentsSize = _ttoi64(buf);

	::GetPrivateProfileString(_T("STUDIO"), _T("PathOpenFile"), _T(""), buf, 1024, szPath);
	m_szPathOpenFile = buf;

	::GetPrivateProfileString(_T("STUDIO"), _T("MAX_CONTENTS_SIZE"), _T("0"), buf, 1024, szPath);
	m_lMaxContentsSize = _ttoi64(buf);

	::GetPrivateProfileString(_T("STUDIO"), _T("MAX_CONTENTS_COUNT"), _T("0"), buf, 1024, szPath);
	m_nMaxContentsCount = _ttoi(buf);

	::GetPrivateProfileString(_T("STUDIO"), _T("MAX_PLAY_TEMPLATE_COUNT"), _T("0"), buf, 1024, szPath);
	m_nMaxPlayTemplateCount = _ttoi(buf);

	::GetPrivateProfileString(_T("ROOT"), _T("USE_TV"), _T("0"), buf, 1024, szPath);
	m_bUseTV = (_ttoi(buf) == 1);

	// 정시플레이컨텐츠은 PE버젼 이거나 USE_TIME_BASE_SCHEDULE=1 인경우 활성화 되도록 한다
	::GetPrivateProfileString(_T("ROOT"), _T("USE_TIME_BASE_SCHEDULE"), _T("0"), buf, 1024, szPath);
	m_bUseTimeBasePlayContents = (_ttoi(buf) == 1 || m_Edition == eStudioPE);

	// 0000782: 부모-자식 스케줄 기능을 수정한다.
	::GetPrivateProfileString(_T("ROOT"), _T("USE_CLICK_SCHEDULE"), _T("0"), buf, 1024, szPath);
	m_bUseClickPlayContents = (_ttoi(buf) == 1);
}

void CEnviroment::Load_ee_ini()
{
	char buf[1024];
	CString szPath, szTemp;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, _UBCSTUDIO_EE_ENV_INI);

	::GetPrivateProfileString(_T("STUDIO"), _T("TYPE"), _T("UBC"), buf, 1024, szPath);
	szTemp = buf;
	if(szTemp == _T("UBC"))
		m_StudioType = eUBCType;
	else if(szTemp == _T("USTB"))
		m_StudioType = eUSTBType;
	else
		m_StudioType = eUBCType;

	::GetPrivateProfileString(_T("STUDIO"), _T("TOTAL_CONTENTS"), _T("0"), buf, 1024, szPath);
	m_TotalContentsSize = _ttoi64(buf);
}

void CEnviroment::Load_pe_ini()
{
	CString szPath;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, _UBCSTUDIO_PE_ENV_INI);

	m_StudioType = eUBCType;
	m_TotalContentsSize = 0;
}

bool CEnviroment::GetFileSize(CString szPath, ULONGLONG& ulSize)
{
	CFileFind ff;
	bool bRet = true;

	if(ff.FindFile(szPath)){
		ff.FindNextFile();
		ulSize = ff.GetLength();
	}else{
		ulSize = 0;
		bRet = false;
	}

	ff.Close();
	return bRet;
}

SYSTEMTIME CEnviroment::CheckTime(SYSTEMTIME& stSrc)
{
	if(stSrc.wYear <= 1970){
		stSrc.wYear = 1970;
		if(stSrc.wMonth == 1 && stSrc.wDay == 1 && stSrc.wHour < 9)
			stSrc.wHour = 9;
	}
	return stSrc;
}

bool CEnviroment::GetAutoUpdate()
{
	char buf[1024];
	CString szPath;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, UBCVARS_INI);

	::GetPrivateProfileString(_T("ROOT"), _T("AutoUpdateFlag"), _T("1"), buf, 1024, szPath);
	int nValue = atoi(buf);

	return (nValue != 0);
}

void CEnviroment::SetAutoUpdate(bool bValue)
{
	char buf[1024];
	CString szPath, szTemp;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, UBCVARS_INI);

	::WritePrivateProfileString(_T("ROOT"), _T("AutoUpdateFlag"), ToString((int)bValue), szPath);
}