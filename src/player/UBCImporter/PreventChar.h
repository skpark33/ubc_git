//////////////////////////////////////////////////////////////////////////
//
// 컨텐츠 명과 화일명으로 사용할수 없는 특수문자는 다음과 같다.
// 1) 원래 MS 윈도우즈에서 파일명으로 사용할 수 없는 문자
// <>\/*|":
// 2) 추가적으로 사용할 수 없는 문자
// [],!%`' 
//
// [],<> 는 괄호로 치환하고, 나머지는 모두 언더스코어로 치환한다.
//
//////////////////////////////////////////////////////////////////////////
#pragma once

class CPreventChar
{
private:
	static CPreventChar* _instance;

	CStringArray m_astrPreventChar;
	CStringArray m_astrConvertChar;

public:
	CPreventChar(void);
	~CPreventChar(void);

	static	CPreventChar* GetInstance();

	CString	GetPreventChar();

	bool	IsPreventChar(CString strValue);
	bool	IsPreventChar(TCHAR szValue);

	bool	HavePreventChar(CString strValue);
	CString	ConvertToAvailableChar(CString strValue);
};
