/*******************************************/
/*                                         */
/* - title : CProgressCtrlEx.h                 */
/* - description : ProgressCtrl Extends    */
/* - author : you !                        */
/* - date : 2002-09-13                     */
/*                                         */
/*******************************************/

#pragma once

#define PROGRESS_NONE	1
#define PROGRESS_TEXT	2
#define PROGRESS_BITMAP	3

class CProgressCtrlEx : public CProgressCtrl
{
public:
	CProgressCtrlEx();
	~CProgressCtrlEx();

public:
	int SetPos(int nPos);
	void SetRange(int nLower, int nUpper);
	int SetStep(int nStep);
	int StepIt();
	void SetTextBkColor(COLORREF clrTextBk);
	void SetTextForeColor(COLORREF clrTextFore);
	void SetBkColor(COLORREF clrBk);
	void SetForeColor(COLORREF clrFore);
	void SetTextColor(COLORREF clrText);

	void SetStyle(int nStyle);
	void SetText(CString strText);
	void SetBitmap(int nId);

	void SetFont(int nFontSize, LPCSTR lpszFaceName);		///<폰트를 설정

protected:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	CFont			m_font;

	DECLARE_MESSAGE_MAP()

private:
	CBitmap m_Bitmap;
	int m_nHeight;
	int m_nWidth;

	int m_nMin;
	int m_nMax;

	int m_nPos;
	int m_nStep;

	int m_nStyle;

	COLORREF m_clrBk;
	COLORREF m_clrFore;
	COLORREF m_clrTextBk;
	COLORREF m_clrTextFore;
	COLORREF m_clrText;

	CString m_strText;
};
