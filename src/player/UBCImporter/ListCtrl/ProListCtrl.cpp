// ProListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "ProListCtrl.h"
#include "ProgressCtrlEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProListCtrl

CProListCtrl::CProListCtrl()
: m_nProgressColumn(-1)
{
}

CProListCtrl::~CProListCtrl()
{
}


BEGIN_MESSAGE_MAP(CProListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CProListCtrl)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDraw)
	ON_WM_DESTROY()
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProListCtrl message handlers
void CProListCtrl::OnCustomDraw( NMHDR* pNMHDR, LRESULT* pResult )
{
	NMLVCUSTOMDRAW* pLVCD = (NMLVCUSTOMDRAW*)pNMHDR;

	*pResult = CDRF_DODEFAULT;

	if (CDDS_PREPAINT == pLVCD->nmcd.dwDrawStage)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;
	}else if (CDDS_ITEMPREPAINT == pLVCD->nmcd.dwDrawStage)
	{
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
		return;
	}else if ( (CDDS_SUBITEM | CDDS_ITEMPREPAINT) == pLVCD->nmcd.dwDrawStage )
	{
		*pResult = CDRF_NOTIFYPOSTPAINT;
		return;
	}else if ( (CDDS_SUBITEM | CDDS_ITEMPOSTPAINT) == pLVCD->nmcd.dwDrawStage )
	{
		int nItem = pLVCD->nmcd.dwItemSpec;
		int nSubItem = pLVCD->iSubItem;
		////////////////////////////////////////////////////
		//if (1 != nSubItem)
		//	return;
		if(nSubItem != m_nProgressColumn)
		{
			return;
		}//if
		///////////////////////////////////////////////////
		
		CRect rcSubItem;
		this->GetSubItemRect(nItem, nSubItem, LVIR_BOUNDS, rcSubItem);
		
		CProgressCtrlEx* pCtrl = (CProgressCtrlEx*)this->GetItemData(nItem);
		if (NULL == pCtrl)
		{
			pCtrl = new CProgressCtrlEx;
			if (rcSubItem.Width() > 100)
				rcSubItem.right = rcSubItem.left + 100;

			//pCtrl->Create(WS_CHILD|WS_VISIBLE|PBS_SMOOTH, rcSubItem, 
			//				   this, 0x1000 + nItem);
			pCtrl->Create(NULL, rcSubItem, this, 0x1000 + nItem);
			pCtrl->SetStyle(PROGRESS_TEXT);
			pCtrl->SetRange(0, 100);
			ASSERT(pCtrl->GetSafeHwnd());
			////////////////////////////////////////
			//pCtrl->SetPos( nItem*10 % 100 );
			////////////////////////////////////////
			this->SetItemData(nItem, (DWORD)pCtrl);
		}

		////////////////////////////////////////
		CString strPercent = GetItemText(nItem, nSubItem);
		pCtrl->SetText(strPercent);
		strPercent.Replace(" %%", "");
		pCtrl->SetPos(atoi(strPercent));
		////////////////////////////////////////
			
		if (rcSubItem.Width() > 100)
			rcSubItem.right = rcSubItem.left + 100;
		pCtrl->MoveWindow(rcSubItem);
		pCtrl->ShowWindow(SW_SHOW);
		*pResult = CDRF_SKIPDEFAULT;
		return;
	}
}

void CProListCtrl::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
	InvalidateProgressCtrls();
}

void CProListCtrl::OnVScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
	InvalidateProgressCtrls();
}

void CProListCtrl::InvalidateProgressCtrls()
{
	int nFirst = GetTopIndex();
	int nLast = nFirst + GetCountPerPage();

	//Hide the other items.
	int nCount = this->GetItemCount();
	CProgressCtrlEx* pCtrl;
	for(int i = 0; i < nFirst; i++)
	{
		pCtrl = (CProgressCtrlEx*)this->GetItemData(i);
		if (NULL != pCtrl)
			pCtrl->ShowWindow(SW_HIDE);		
	}
	for(int i = nLast; i < nCount; i++)
	{
		pCtrl = (CProgressCtrlEx*)this->GetItemData(i);
		if (NULL != pCtrl)
			pCtrl->ShowWindow(SW_HIDE);		
	}

	//Invalidate
	CRect rc(0,0,0,0);
	CRect rcSubItem;
	for(; nFirst < nLast; nFirst++)
	{
		GetSubItemRect(nFirst, 1, LVIR_BOUNDS, rcSubItem);
		VERIFY( rc.UnionRect(rc, rcSubItem) );
	}

	InvalidateRect(rc);

//  Using this method, the flicker seems better that above. But I am not sure why, and think it's non-effective.
//	for(; nFirst < nLast; nFirst++)
//	{
//		GetSubItemRect(nFirst, 1, LVIR_BOUNDS, rcSubItem);
//		InvalidateRect(rcSubItem);
//	}	
}

void CProListCtrl::OnDestroy()
{
	int nCount = this->GetItemCount();
	CProgressCtrlEx* pCtrl;
	for(int i = 0; i < nCount; i++)
	{
		pCtrl = (CProgressCtrlEx*)this->GetItemData(i);
		if (NULL != pCtrl)
			delete pCtrl;
		this->SetItemData(i, 0);
	}
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그래시브 컨트롤이 위치하는 컬럼을 설정한다. \n
/// @param (int) nIndex : (in) 프로그래시브 컨트롤이 위치하는 컬럼 인덱스
/////////////////////////////////////////////////////////////////////////////////
void CProListCtrl::SetProgressColumn(int nIndex)
{
	m_nProgressColumn = nIndex;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그래시브 컨트롤이 위치하는 컬럼을 반환한다. \n
/// @return <형: int> \n
///			<프로그래시브 컨트롤이 위치하는 컬럼 인덱스> \n
/////////////////////////////////////////////////////////////////////////////////
int CProListCtrl::GetProgressColumn()
{
	return m_nProgressColumn;
}

BOOL CProListCtrl::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	InvalidateProgressCtrls();

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
