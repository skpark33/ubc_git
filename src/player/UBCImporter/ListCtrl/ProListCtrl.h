#if !defined(AFX_PROLISTCTRL_H__81C51345_D304_48FE_AE90_1A71500AB2E9__INCLUDED_)
#define AFX_PROLISTCTRL_H__81C51345_D304_48FE_AE90_1A71500AB2E9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProListCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProListCtrl window

class CProListCtrl : public CListCtrl
{
// Construction
public:
	CProListCtrl();

// Attributes
public:

// Operations
public:
	void InvalidateProgressCtrls();
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProListCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProListCtrl();

	void	SetProgressColumn(int nIndex);		///<프로그래시브 컨트롤이 위치하는 컬럼을 설정한다.
	int		GetProgressColumn(void);			///<프로그래시브 컨트롤이 위치하는 컬럼을 반환한다.

	// Generated message map functions
protected:
	//{{AFX_MSG(CProListCtrl)
	afx_msg void OnCustomDraw( NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnDestroy();
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	int		m_nProgressColumn;					///<프로그래시브 컨트롤이 위치하는 컬럼 인덱스

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROLISTCTRL_H__81C51345_D304_48FE_AE90_1A71500AB2E9__INCLUDED_)
