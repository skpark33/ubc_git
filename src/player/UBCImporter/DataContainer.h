#pragma once
#include <afxmt.h>

#define PIP_ZINDEX		1000

CString ForSaveComment(CString strRawValue);
CString	ForLoadComment(CString strRawValue);
CString CreateGUID();

///////////////////////////////////////////////////////////////////////////////
class CONTENTS_INFO
{
public:
	// attributes
	CString		strId                ;
	int 		nContentsType        ;
	CString		strContentsName      ;
	CString		strLocation          ;	// local contents location
	CString		strSeverLocation     ;	// server contents location
	CString		strFilename          ;
	ULONG 		nRunningTime         ;
	CString		strComment[10]       ;
	CString		strBgColor           ;	// background color
	CString		strFgColor           ;	// foreground color
	CString		strFont              ;	// font
	int			nFontSize            ;	// font size
	int			nPlaySpeed           ;	// tiker 등이 플레이 되는 속도
	int 		nSoundVolume         ;
	int			nDirection           ;	// 문자방향 가로 0, 세로 1
	int			nAlign               ;	// 정렬방향 1~9
								     
	CString		strChngFileName      ;	///<컨텐츠 이름에 '[', ']' 문자가 있어서 이름이 변경되었을 경우 병경된 이름
	bool		bNameChanged         ;	///<컨텐츠의 이름이 변경 되었는지 여부
	bool		bLocalFileExist      ;	///<컨텐츠의 경로에 실제 파일이 존재하는지 여부
	bool		bServerFileExist     ;
								     
	int			nWidth               ;
	int			nHeight              ;
	int			nCurrentComment      ;

	//							
	ULONGLONG	nFilesize            ;
	int			nContentsState       ;
	CString		strPromotionValueList;

	// 0000611: 플레쉬 컨텐츠 마법사
	CString		strWizardXML         ;  // wizard 용 XML string
	CString		strWizardFiles       ;  // wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

	CONTENTS_INFO()
	: nContentsType   (-1)
	, nRunningTime    (0)
	, nFontSize       (36)	// font size
	, nPlaySpeed      (0)		// tiker 등이 플레이 되는 속도
	, nSoundVolume    (0)
	, nDirection      (0)		// 문자방향 가로 0, 세로 1
	, nAlign          (5)		// 정렬방향 1~9
	, bNameChanged    (false)	///<컨텐츠의 이름이 변경 되었는지 여부
	, bLocalFileExist (false)	///<컨텐츠의 경로에 실제 파일이 존재하는지 여부
	, bServerFileExist(true)
	, nWidth          (0)
	, nHeight         (0)
	, nCurrentComment (0)
	, nFilesize       (0)
	, nContentsState  (CON_READY)
	{};

	virtual ~CONTENTS_INFO() {};

	CONTENTS_INFO& operator= (const CONTENTS_INFO& info)
	{
		strId                 = info.strId                ;
		nContentsType         = info.nContentsType        ;
		strContentsName       = info.strContentsName      ;
		strLocation           = info.strLocation          ;
		strSeverLocation      = info.strSeverLocation     ;
		strFilename           = info.strFilename          ;
		nRunningTime          = info.nRunningTime         ;
		strBgColor            = info.strBgColor           ;
		strFgColor            = info.strFgColor           ;
		strFont               = info.strFont              ;
		nFontSize             = info.nFontSize            ;
		nPlaySpeed            = info.nPlaySpeed           ;
		nSoundVolume          = info.nSoundVolume         ;
		nDirection            = info.nDirection           ;
		nAlign                = info.nAlign               ;
		strChngFileName       = info.strChngFileName      ;
		bNameChanged          = info.bNameChanged         ;
		bLocalFileExist       = info.bLocalFileExist      ;
		bServerFileExist      = info.bServerFileExist     ;
													 
		nWidth                = info.nWidth               ;
		nHeight               = info.nHeight              ;
		nCurrentComment       = info.nCurrentComment      ;
													 
		nFilesize             = info.nFilesize            ;
		nContentsState        = info.nContentsState       ;
		strPromotionValueList = info.strPromotionValueList;

		// 0000611: 플레쉬 컨텐츠 마법사
		strWizardXML          = info.strWizardXML         ;	// wizard 용 XML string
		strWizardFiles        = info.strWizardFiles       ;	// wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

		for(int i=0; i<10; i++) strComment[i] = info.strComment[i];

		return *this;
	}

	bool operator== (const CONTENTS_INFO & info)
	{
		if(strId                 != info.strId                ) return false;
		if(nContentsType         != info.nContentsType        ) return false;
		if(strContentsName       != info.strContentsName      ) return false;
		if(strLocation           != info.strLocation          ) return false;
		if(strSeverLocation      != info.strSeverLocation     ) return false;
		if(strFilename           != info.strFilename          ) return false;
		if(nRunningTime          != info.nRunningTime         ) return false;
		if(strBgColor            != info.strBgColor           ) return false;
		if(strFgColor            != info.strFgColor           ) return false;
		if(strFont               != info.strFont              ) return false;
		if(nFontSize             != info.nFontSize            ) return false;
		if(nPlaySpeed            != info.nPlaySpeed           ) return false;
		if(nSoundVolume          != info.nSoundVolume         ) return false;
		if(nDirection            != info.nDirection           ) return false;
		if(nAlign                != info.nAlign               ) return false;
		if(strChngFileName       != info.strChngFileName      ) return false;
		if(bNameChanged          != info.bNameChanged         ) return false;
//		if(bLocalFileExist       != info.bLocalFileExist      ) return false;	// 2010.11.10 변경유무 판단시 필요없는 항목이므로 체크하지 않음
//		if(bServerFileExist      != info.bServerFileExist     ) return false;	// ..
														 
		if(nWidth                != info.nWidth               ) return false;
		if(nHeight               != info.nHeight              ) return false;
		if(nCurrentComment       != info.nCurrentComment      ) return false;
														 
		if(nFilesize             != info.nFilesize            ) return false;
		if(nContentsState        != info.nContentsState       ) return false;
		if(strPromotionValueList != info.strPromotionValueList) return false;

		// 0000611: 플레쉬 컨텐츠 마법사
		if(strWizardXML          != info.strWizardXML         ) return false; // wizard 용 XML string
		if(strWizardFiles        != info.strWizardFiles       ) return false; // wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

		for(int i=0; i<10; i++) if(strComment[i] != info.strComment[i]) return false;

		return true;
	} 

	// member function
	bool		Save(LPCTSTR lpszFullPath, LPCTSTR lpszSaveID, bool bNew);
	bool		Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID);
};

typedef CMapStringToPtr CONTENTS_INFO_MAP;
typedef	CArray<CONTENTS_INFO*, CONTENTS_INFO*>	CONTENTS_INFO_LIST;

///////////////////////////////////////////////////////////////////////////////
class PLAYCONTENTS_INFO
{
public:
	// attributes
	CString		strTemplateId      ;
	CString		strFrameId         ;
	CString		strId              ;
	int 		nStartDate         ;
	int			nEndDate           ;
	CString		strStartTime       ;		// only time playcontents
	CString		strEndTime         ;		// only time playcontents
	int 		nPlayOrder         ;		// only cycle playcontents
	int			nTimeScope         ;		// only cycle playcontents
	CString		strContentsId      ;
	int			nPriority          ;
	CString		strParentPlayContentsId;
	bool		bIsDefaultPlayContents ;
	CString		strTouchTime       ;		// 클릭스케쥴 시, 다른 페이지로 이동하는 기능

	PLAYCONTENTS_INFO()
	:	nStartDate        (0)
	,	nEndDate          (0)
	,	nPlayOrder        (0)
	,	nTimeScope        (-1)
	,	nPriority         (3)
	,	bIsDefaultPlayContents(true)
	{};
	virtual ~PLAYCONTENTS_INFO() {};
	bool operator== (const PLAYCONTENTS_INFO & info)
	{
		if(strTemplateId       != info.strTemplateId      ) return false;
		if(strFrameId          != info.strFrameId         ) return false;
		if(strId               != info.strId              ) return false;
		if(nStartDate          != info.nStartDate         ) return false;
		if(nEndDate            != info.nEndDate           ) return false;
		if(strStartTime        != info.strStartTime       ) return false;
		if(strEndTime          != info.strEndTime         ) return false;
		if(nPlayOrder          != info.nPlayOrder         ) return false;
		if(nTimeScope          != info.nTimeScope         ) return false;
		if(strContentsId       != info.strContentsId      ) return false;
		if(nPriority           != info.nPriority          ) return false;
		if(strParentPlayContentsId != info.strParentPlayContentsId) return false;
		if(bIsDefaultPlayContents  != info.bIsDefaultPlayContents ) return false;
		if(strTouchTime        != info.strTouchTime       ) return false;
		return true;
	}

	// member function
	bool		Save(LPCTSTR lpszFullPath, CONTENTS_INFO* pContentsInfo, bool bNew);
	bool		Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID);
};

typedef CArray<PLAYCONTENTS_INFO*, PLAYCONTENTS_INFO*> PLAYCONTENTS_INFO_LIST;


///////////////////////////////////////////////////////////////////////////////
class FRAME_INFO
{
public:
	// attributes
	CString				strTemplateId         ;
	CString				strId                 ;
	int					nGrade                ;
	CRect				rcRect                ;
	CString				strBorderStyle        ;
	int					nBorderThickness      ;
	COLORREF			crBorderColor         ;
	int					nCornerRadius         ;
	bool				bIsPIP                ;
	int					nZIndex               ;
	int					nAlpha                ;
	bool				bIsTV                 ;
	CString				strDescription        ;
	CString				strComment[3]         ;

	CString				strCyclePlayContentsIdList;
	CString				strTimeBasePlayContentsIdList;

	PLAYCONTENTS_INFO_LIST	arCyclePlayContentsList;
	PLAYCONTENTS_INFO_LIST	arTimePlayContentsList;

	FRAME_INFO()
	:	nGrade (0)
	,	rcRect(0,0,0,0)
	,	strBorderStyle("none")
	,	nBorderThickness(0)
	,	crBorderColor (0)
	,	nCornerRadius (0)
	,	bIsPIP (false)
	,	nZIndex(0)
	,	nAlpha(0)
	,	bIsTV(false)
	{};
	virtual ~FRAME_INFO() { DeletePlayContentsList(); };
	FRAME_INFO& operator= (const FRAME_INFO& info)
	{
		DeletePlayContentsList();

		strTemplateId    = info.strTemplateId   ;
		strId            = info.strId           ;
		nGrade           = info.nGrade          ;
		rcRect           = info.rcRect          ;
		strBorderStyle   = info.strBorderStyle  ;
		nBorderThickness = info.nBorderThickness;
		crBorderColor    = info.crBorderColor   ;
		nCornerRadius    = info.nCornerRadius   ;
		bIsPIP           = info.bIsPIP          ;
		nZIndex          = info.nZIndex         ;
		nAlpha           = info.nAlpha          ;
		bIsTV            = info.bIsTV           ;
		strDescription   = info.strDescription  ;
		strComment[0]    = info.strComment[0]   ;
		strComment[1]    = info.strComment[1]   ;
		strComment[2]    = info.strComment[2]   ;

		for(int i=0; i<info.arCyclePlayContentsList.GetCount(); i++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
			*pPlayContentsInfo = *(info.arCyclePlayContentsList.GetAt(i));
			arCyclePlayContentsList.Add(pPlayContentsInfo);
		}

		for(int i=0; i<info.arTimePlayContentsList.GetCount(); i++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
			*pPlayContentsInfo = *(info.arTimePlayContentsList.GetAt(i));
			arTimePlayContentsList.Add(pPlayContentsInfo);
		}

		return *this;
	}

	bool operator== (const FRAME_INFO & info)
	{
		if(strTemplateId    != info.strTemplateId   ) return false;
		if(strId            != info.strId           ) return false;
		if(nGrade           != info.nGrade          ) return false;
		if(rcRect           != info.rcRect          ) return false;
		if(strBorderStyle   != info.strBorderStyle  ) return false;
		if(nBorderThickness != info.nBorderThickness) return false;
		if(crBorderColor    != info.crBorderColor   ) return false;
		if(nCornerRadius    != info.nCornerRadius   ) return false;
		if(bIsPIP           != info.bIsPIP          ) return false;
		if(nZIndex          != info.nZIndex         ) return false;
		if(nAlpha           != info.nAlpha          ) return false;
		if(bIsTV            != info.bIsTV           ) return false;
		if(strDescription   != info.strDescription  ) return false;
		if(strComment[0]    != info.strComment[0]   ) return false;
		if(strComment[1]    != info.strComment[1]   ) return false;
		if(strComment[2]    != info.strComment[2]   ) return false;

		if(arCyclePlayContentsList.GetCount() != info.arCyclePlayContentsList.GetCount()) return false;
		if(arTimePlayContentsList.GetCount() != info.arTimePlayContentsList.GetCount()) return false;

		for(int i=0; i<arCyclePlayContentsList.GetCount() ;i++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo1 = (PLAYCONTENTS_INFO*)arCyclePlayContentsList.GetAt(i);
			PLAYCONTENTS_INFO* pPlayContentsInfo2 = (PLAYCONTENTS_INFO*)info.arCyclePlayContentsList.GetAt(i);
			if( !(*pPlayContentsInfo1 == *pPlayContentsInfo2) ) return false;
		}

		for(int i=0; i<arTimePlayContentsList.GetCount() ;i++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo1 = (PLAYCONTENTS_INFO*)arTimePlayContentsList.GetAt(i);
			PLAYCONTENTS_INFO* pPlayContentsInfo2 = (PLAYCONTENTS_INFO*)info.arTimePlayContentsList.GetAt(i);
			if( !(*pPlayContentsInfo1 == *pPlayContentsInfo2) ) return false;
		}

		return true;
	}

	// member function
	bool		Save(LPCTSTR lpszFullPath, PLAYCONTENTS_INFO_LIST* pCyclePlayContentsInfoList, PLAYCONTENTS_INFO_LIST* pTimePlayContentsInfoList);
	bool		Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID);

	bool		AddPlayContents(PLAYCONTENTS_INFO* pInPlayContentsInfo);
	bool		DeletePlayContents(PLAYCONTENTS_INFO* pInPlayContentsInfo);
	void		DeletePlayContentsList();
};

typedef		CArray<FRAME_INFO*, FRAME_INFO*>	FRAME_INFO_LIST;


/////////////////////////////////////////////////////////////////////////////
class FRAME_LINK
{
public:
	FRAME_INFO*	pFrameInfo;

	CRect		rcViewRect[5];	// 0 = LayoutVIew-Template_Wnd, 1 = LayoutVIew-Frame_Wnd, 2 = PackageVIew-Template_Wnd, 3 = PackageVIew-Frame_Wnd, 4 = SelectTemplateDialog-Template_Wnd
	CRect		rcViewRectSizing[5][9];

	FRAME_LINK(FRAME_INFO* info = NULL)
	: pFrameInfo(info)
	{
		for(int i=0; i<5; i++)
		{
			rcViewRect[i].SetRect(0,0,0,0);
			for(int j=0; j<9; j++)
				rcViewRectSizing[i][j].SetRect(0,0,0,0);
		}
	};
	virtual ~FRAME_LINK() {};
	FRAME_LINK& operator= (const FRAME_LINK& info)
	{
		pFrameInfo = info.pFrameInfo;

		for(int i=0; i<5; i++)
		{
			rcViewRect[i] = info.rcViewRect[i];
			for(int j=0; j<9; j++)
				rcViewRectSizing[i][j] = info.rcViewRectSizing[i][j];
		}

		return *this;
	}

	bool operator== (const FRAME_LINK & info)
	{
		if(!pFrameInfo && !info.pFrameInfo) return true;
		if(!pFrameInfo) return false;
		if(!info.pFrameInfo) return false;

		return (*pFrameInfo == *info.pFrameInfo);
	}

	void Copy(const FRAME_LINK& info)
	{
		if(pFrameInfo) delete pFrameInfo;
		pFrameInfo = new FRAME_INFO;
		*pFrameInfo = *info.pFrameInfo;

		for(int i=0; i<5; i++)
		{
			rcViewRect[i] = info.rcViewRect[i];
			for(int j=0; j<9; j++)
				rcViewRectSizing[i][j] = info.rcViewRectSizing[i][j];
		}
	}
};

typedef		CArray<FRAME_LINK, FRAME_LINK&>		FRAME_LINK_LIST;


///////////////////////////////////////////////////////////////////////////////
class TEMPLATE_INFO
{
public:
	// attributes
	CString		strId         ;
	CRect		rcRect        ;
	COLORREF	crBgColor     ; 
	CString		strBgImage    ;
	int			nBgType       ; // 0 : 늘이기, 1: 바둑판, 2: 중앙
	CString		strDescription;
	CString		strShortCut   ;
	CString		strFrameList  ;

	TEMPLATE_INFO()
	: rcRect (0,0,0,0)
	, crBgColor (0)
	, nBgType (0)
	{
	};
	virtual ~TEMPLATE_INFO() {};
	TEMPLATE_INFO& operator= (const TEMPLATE_INFO& info)
	{
		strId          = info.strId         ;
		rcRect         = info.rcRect        ;
		crBgColor      = info.crBgColor     ;
		strBgImage     = info.strBgImage    ;
		nBgType        = info.nBgType       ;
		strDescription = info.strDescription;
		strShortCut    = info.strShortCut   ;
		strFrameList   = info.strFrameList  ;

		return *this;
	}
	bool operator== (const TEMPLATE_INFO & info)
	{
		if(strId          != info.strId         ) return false;
		if(rcRect         != info.rcRect        ) return false;
		if(crBgColor      != info.crBgColor     ) return false;
		if(strBgImage     != info.strBgImage    ) return false;
		if(nBgType        != info.nBgType       ) return false;
		if(strDescription != info.strDescription) return false;
		if(strShortCut    != info.strShortCut   ) return false;
		return true;
	}

	// member function
	bool		Save(LPCTSTR lpszFullPath, FRAME_LINK_LIST* pFrameLinkList);
	bool		Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID);
};

typedef		CArray<TEMPLATE_INFO*, TEMPLATE_INFO*>	TEMPLATE_INFO_LIST;


/////////////////////////////////////////////////////////////////////////////
class TEMPLATE_LINK
{
public:
	TEMPLATE_INFO*		pTemplateInfo    ;
	int					nPlayTimes       ;
	double				fScale           ;
	CRect				rcViewRect[5]    ;	// 0 = LayoutVIew-Template_Wnd, 1 = LayoutVIew-Frame_Wnd, 2 = PackageVIew-Template_Wnd, 3 = PackageVIew-Frame_Wnd, 4 = SelectTemplateDialog-Template_Wnd
	CRect				rcViewRectItem   ;	// 아이템 박스.
	CRect				rcViewRectInfo[2];	// 정보상자 박스. 0=크기, 1=반복회수

	FRAME_LINK_LIST		arFrameLinkList  ;

	TEMPLATE_LINK(TEMPLATE_INFO* pInTemplateInfo=NULL, int nInPlayTimes=1, double fInScale=1.0f)
	:	pTemplateInfo (pInTemplateInfo)
	,	nPlayTimes (nInPlayTimes)
	,	fScale (fInScale)
	{
		for(int i=0; i<5; i++)
			rcViewRect[i].SetRect(0,0,0,0);
		rcViewRectItem.SetRect(0,0,0,0);
		rcViewRectInfo[0].SetRect(0,0,0,0);
		rcViewRectInfo[1].SetRect(0,0,0,0);
	};
	virtual ~TEMPLATE_LINK() {};

	TEMPLATE_LINK&  operator= (const TEMPLATE_LINK& info)
	{
		pTemplateInfo = info.pTemplateInfo;
		nPlayTimes    = info.nPlayTimes   ;
		fScale        = info.fScale       ;

		for(int i=0; i<5; i++)
			rcViewRect[i] = info.rcViewRect[i];

		rcViewRectItem    = info.rcViewRectItem;
		rcViewRectInfo[0] = info.rcViewRectInfo[0];
		rcViewRectInfo[1] = info.rcViewRectInfo[1];

		arFrameLinkList.Copy(info.arFrameLinkList);

		return *this;
	}

	bool			operator== (const TEMPLATE_LINK& info)
	{
		if(!pTemplateInfo && !info.pTemplateInfo) return true;
		if(!pTemplateInfo) return false;
		if(!info.pTemplateInfo) return false;

		if( !(*pTemplateInfo == *info.pTemplateInfo) ) return false;
		if(nPlayTimes != info.nPlayTimes) return false;

		if(arFrameLinkList.GetCount() != info.arFrameLinkList.GetCount()) return false;
		for(int i=0; i<arFrameLinkList.GetCount() ; i++)
		{
			if( !(arFrameLinkList.GetAt(i) == info.arFrameLinkList.GetAt(i)) ) return false;
		}

		return true;
	}

	void			Copy(TEMPLATE_LINK& info)
	{
		if(pTemplateInfo) delete pTemplateInfo;
		pTemplateInfo = new TEMPLATE_INFO;
		*pTemplateInfo = *info.pTemplateInfo;

		nPlayTimes = info.nPlayTimes;
		fScale     = info.fScale    ;

		DeleteFrameList();

		int nCnt = info.arFrameLinkList.GetCount();
		for(int i=0; i<nCnt ;i++)
		{
			FRAME_LINK& SrcFrameLink = info.arFrameLinkList.GetAt(i);
			FRAME_LINK BackFrameLink;
			BackFrameLink.Copy(SrcFrameLink);
			arFrameLinkList.Add(BackFrameLink);
		}

		for(int i=0; i<5; i++)
			rcViewRect[i] = info.rcViewRect[i];

		rcViewRectItem    = info.rcViewRectItem;
		rcViewRectInfo[0] = info.rcViewRectInfo[0];
		rcViewRectInfo[1] = info.rcViewRectInfo[1];
	}

	void			DeleteFrameList();
	void			RecalcLayout();

	// 강제로 PIP 로 설정등 과 같이 프레임에 강제 설정해야 하는 정보를 이 함수에서 설정한다.
	void			SetForceFrameInfo();
	void			ReCalcPIP();
};

typedef		CArray<TEMPLATE_LINK, TEMPLATE_LINK&>	TEMPLATE_LINK_LIST;


/////////////////////////////////////////////////////////////////////////////
class CDataContainer
{
protected:
	CDataContainer(void);
	virtual ~CDataContainer(void);

	static CDataContainer*	_instance;
	static CCriticalSection	_cs;

	CONTENTS_INFO_MAP		m_ContentsMap;
	TEMPLATE_LINK_LIST		m_AllTemplateList;
	TEMPLATE_LINK_LIST		m_PlayTemplateList;

	CONTENTS_INFO_MAP		m_ContentsMapOld;
	TEMPLATE_LINK_LIST		m_AllTemplateListOld;
	TEMPLATE_LINK_LIST		m_PlayTemplateListOld;

public:
	CString					m_strSoundVolume;
	CString					m_strDefaultTemplate;
	CString					m_strAdminState;
	CString					m_strOperationalState;
	CString					m_strDescription;
	CString					m_strNetworkUse;
	CString					m_strSite;
	CString					m_strLastWrittenUser;
	CString					m_strLastWrittenTime;

	static					CDataContainer* getInstance();
	static void 			clearInstance();

	bool					DeleteAllData(bool bIncludeBackup=true);
	void					DeleteAllBackupData();
							
	bool					Save(LPCTSTR lpszFullPath, bool bNew, CString strPreviewTemplateID=_T(""));
	bool					SaveContents(LPCTSTR lpszFullPath, bool bNew);
	bool					Load(LPCTSTR lpszFullpath);
	void					CreateBackupData();

	CONTENTS_INFO_MAP*		GetContentsMap() { return &m_ContentsMap; };
	TEMPLATE_LINK_LIST*		GetAllTemplateList() { return &m_AllTemplateList; };
	TEMPLATE_LINK_LIST*		GetPlayTemplateList() { return &m_PlayTemplateList; };

	bool					AddContents(CONTENTS_INFO* info, LPSTR =NULL);
	bool					AddTemplate(TEMPLATE_INFO* info);
	int 					AddPlayTemplate(LPCTSTR lpszTemplateId, int nPlayTimes=1);
	int 					AddPlayTemplate(LPCTSTR lpszTemplateId, LPCTSTR szPlayTimes);

	TEMPLATE_INFO*			CreateNewTemplate();
	FRAME_INFO*				CreateNewFrame(LPCTSTR lpszTemplateId);
	LPCTSTR					CreateCopyTemplate(LPCTSTR lpszSrcTemplateID);	///<기존의 template size, color등 기본정보만 복사하여 새로운 template를 생성한다. 

	bool					DeleteContents(LPCTSTR lpszContentsId);
	bool					DeleteTemplate(LPCTSTR lpszTemplateId);
	bool					DeleteFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId);
	bool					DeletePlayTemplate(int nIndex);
	bool					DeleteContentsInWizards(CString strContentsId);

	CONTENTS_INFO*			GetContents(LPCTSTR lpszContentsId);
	TEMPLATE_LINK*			GetTemplateLink(LPCTSTR lpszTemplateId);
	FRAME_LINK*				GetFrameLink(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId);
	PLAYCONTENTS_INFO*		GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszPlayContentsId);
	PLAYCONTENTS_INFO*		GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId, LPCTSTR lpszPlayContentsId);

	bool					MoveFordwardPlayTemplate(int nIndex);
	bool					MoveBackwardPlayTemplate(int nIndex);

	bool					IsModified();
	bool					IsUsedShortcut(LPCTSTR szShortcut);
	bool					IsExistContentsInTemplate(LPCTSTR lpszTemplateId);
	bool					IsExistContentsInFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId);
	bool					IsExistPlayTemplateList(LPCTSTR lpszTemplateId);
	bool					IsExistPlayContentsUsingContents(LPCTSTR lpszContentsId);
	int						GetContentsUsingCount(LPCTSTR lpszContentsId);
	bool					SavePreview(LPCTSTR lpszFullPath, CString strTemplateID, bool bNew);		///<Preview를 위하여 지정된 template config 항목을 저장한다
	CString					GetDescription(void);											///<열려있는 host의 설명을 반환한다.
	void					SetDescription(CString strDesc);								///<열려있는 host의 설명을 설정한다.
	void                    CheckTemplateRect();
	bool					CheckPrimaryFrame();
	bool					CheckPrimaryFrame(bool bMsg, CString& strOutMsg);
	bool					CheckOverlayModeFrame(CString strTemplateId, CString strOverlayModeFrameId, bool bMsg=true); // 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
	bool					CheckContentsCanBeAddedToFrame(CString strTemplateId, CString strFrameId, CString strContentsId); // 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
	bool					CheckPlayContentsRelation(); // 0000891: 어떤 프레임에 자식 플레이컨텐츠만 있고, 자식 플레이컨텐츠외에 아무런 다른 플레이컨텐츠가 없다면 저장시 경고해주어야함.

	// 강제로 PIP 로 설정등 과 같이 프레임에 강제 설정해야 하는 정보를 이 함수에서 설정한다.
	void					SetForceInfo();
};

