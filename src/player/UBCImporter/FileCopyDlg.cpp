// FileCopyDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCImporter.h"
#include "FileCopyDlg.h"
#include <Dbghelp.h>
#include "ciArgParser.h"  // skpark USB_Device
#include "scratchUtil.h"	// skpark USB_Device
#include "EjectRemovableDrive.h" //skpark USB_Device

#define		TIMER_INIT		1400
#define		TIMER_COPY		1500
#define		SIZE_FILE_BUF	1024*1024


// CFileCopyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFileCopyDlg, CDialog)

CFileCopyDlg::CFileCopyDlg(CCopyInfoArray* m_paryCopyInfo,
						   CWnd* pParent /*=NULL*/)
	: CDialog(CFileCopyDlg::IDD, pParent)
	, m_paryCopyInfo(m_paryCopyInfo)
	, m_bCancel(false)
	, m_bWorking(false)
	, m_nFailCount(0)
	, pThreadCopy(NULL)
{

}

CFileCopyDlg::~CFileCopyDlg()
{
	m_imglistContents.Detach();
}

void CFileCopyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FILE_LIST, m_listFile);
}


BEGIN_MESSAGE_MAP(CFileCopyDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CFileCopyDlg::OnBnClickedButtonCancel)
	ON_WM_TIMER()
	ON_MESSAGE(WM_COMPLETE_FILE_COPY, OnCompleteFileCopy)
	ON_MESSAGE(WM_STATUS_FILE_COPY, OnStatusFileCopy)
	ON_MESSAGE(WM_CANCEL_FILE_COPY, OnCancelFileCopy)
	ON_MESSAGE(WM_FAIL_FILE_COPY, OnFailFileCopy)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_OK, &CFileCopyDlg::OnBnClickedButtonOk)
END_MESSAGE_MAP()


// CFileCopyDlg 메시지 처리기입니다.

void CFileCopyDlg::OnBnClickedButtonCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_bWorking && pThreadCopy)
	{
		pThreadCopy->SuspendThread();
		int nRet = MessageBox("Cancel contents file copy?", "Cancel", MB_OKCANCEL|MB_ICONQUESTION);
		if(nRet == IDOK)
		{
			m_bCancel = true;
			
		}//if
		pThreadCopy->ResumeThread();
	}//if
}

BOOL CFileCopyDlg::OnInitDialog()
{
	__DEBUG__("OnInitDialog()");

	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	CButton* pBtn = (CButton*)GetDlgItem(IDC_BUTTON_OK);
	pBtn->EnableWindow(FALSE);
	pBtn = (CButton*)GetDlgItem(IDC_BUTTON_CANCEL);
	pBtn->EnableWindow(FALSE);

	CRect clsRect;
	m_listFile.GetClientRect(clsRect);

	if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
	{
		m_listFile.InsertColumn(0, "컨텐츠패키지(방송계획) 이름", LVCFMT_LEFT, (clsRect.Width()/100)*30);
		m_listFile.InsertColumn(1, "파일 이름", LVCFMT_LEFT, (clsRect.Width()/100)*30);
		m_listFile.InsertColumn(2, "상태", LVCFMT_LEFT, (clsRect.Width()/100)*10);
		m_listFile.InsertColumn(3, "결과", LVCFMT_LEFT, (clsRect.Width()/100)*30);
	}
	else
	{
		m_listFile.InsertColumn(0, "ContentsPackage(BroadcastPlan)", LVCFMT_LEFT, (clsRect.Width()/100)*30);
		m_listFile.InsertColumn(1, "File Name", LVCFMT_LEFT, (clsRect.Width()/100)*30);
		m_listFile.InsertColumn(2, "Status", LVCFMT_LEFT, (clsRect.Width()/100)*10);
		m_listFile.InsertColumn(3, "Result", LVCFMT_LEFT, (clsRect.Width()/100)*30);
	}//if
	m_listFile.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_SUBITEMIMAGES|LVS_EX_GRIDLINES);
	m_listFile.SetProgressColumn(2);

	CBitmap bmpContents;
	bmpContents.LoadBitmap(IDB_CONTENTS);
	m_imglistContents.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1);
	m_imglistContents.Add(&bmpContents, RGB(255, 255, 255));
	m_listFile.SetImageList(&m_imglistContents, LVSIL_SMALL);
	bmpContents.Detach();

	if(this->GetSafeHwnd()==NULL){
		__DEBUG__("Window Handle is null");
	}else{
		__DEBUG__("Window Handle Created");
	}

	SetTimer(TIMER_INIT, 100, NULL);
	__DEBUG__("OnInitDialog() end");

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 리스트 초기화 \n
/////////////////////////////////////////////////////////////////////////////////
void CFileCopyDlg::InitList()
{
	__DEBUG__("InitList()");
	LVITEM item;
	item.mask = LVIF_IMAGE;
	CCopyInfo* pCopy = NULL;
	for(int i=0; i<m_paryCopyInfo->GetCount(); i++)
	{
		pCopy = m_paryCopyInfo->GetAt(i);
		m_listFile.InsertItem(i, pCopy->m_strPackageName);
		m_listFile.SetItemText(i, 1, pCopy->m_strFileName);
		//m_listFile.SetItemData(i, (DWORD)0);

		item.iItem = i;
		if(pCopy->m_nContentType == CONTENTS_PACKAGE)
		{
			//item.iItem = i;
			item.iSubItem = 0;
			item.mask = LVIF_IMAGE;
			//item.iImage = 21;
			m_listFile.SetItem(&item);

			//item.iItem = i;
			item.iSubItem = 1;
			//item.mask = LVIF_IMAGE;
			item.iImage = 21;
			m_listFile.SetItem(&item);
		}
		else if(pCopy->m_nContentType == CONTENTS_BPI)
		{
			//item.iItem = i;
			item.iSubItem = 0;
			//item.mask = LVIF_IMAGE;
			item.iImage = 22;
			m_listFile.SetItem(&item);

			//item.iItem = i;
			item.iSubItem = 1;
			//item.mask = LVIF_IMAGE;
			item.iImage = 22;
			m_listFile.SetItem(&item);
		}
		else
		{
			//item.iItem = i;
			item.iSubItem = 0;
			//item.mask = LVIF_IMAGE;
			item.iImage = 21;
			m_listFile.SetItem(&item);

			//item.iItem = i;
			item.iSubItem = 1;
			//item.mask = LVIF_IMAGE;
			item.iImage = pCopy->m_nContentType + 1;
			m_listFile.SetItem(&item);
		}//if
		//m_listFile.SetItem(&item);
	}//for	

	SetTimer(TIMER_COPY, 10, NULL);
	__DEBUG__("InitList() end");
}

void CFileCopyDlg::OnTimer(UINT_PTR nIDEvent)
{
	__DEBUG__("OnTimer(%d)", nIDEvent);
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == TIMER_COPY)
	{
		KillTimer(TIMER_COPY);
		BeginCopy();
	}
	else if(nIDEvent == TIMER_INIT)
	{
		KillTimer(TIMER_INIT);
		InitList();
	}//if

	CDialog::OnTimer(nIDEvent);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일 복사를 시작한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CFileCopyDlg::BeginCopy()
{
	__DEBUG__("BeginCopy()");
	m_bWorking = true;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_BUTTON_OK);
	pBtn->EnableWindow(FALSE);
	pBtn = (CButton*)GetDlgItem(IDC_BUTTON_CANCEL);
	pBtn->EnableWindow(TRUE);

	pThreadCopy = ::AfxBeginThread(CFileCopyDlg::FileCopyThread,
											this,
											THREAD_PRIORITY_NORMAL,
											0,
											CREATE_SUSPENDED);
	pThreadCopy->m_bAutoDelete = TRUE;
	pThreadCopy->ResumeThread();
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일을 복사하는 thread \n
/// @param (LPVOID) param : (in) 부모윈도우 포인터
/// @return <형: UINT> \n
///			<0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CFileCopyDlg::FileCopyThread(LPVOID param)
{
	__DEBUG__("FileCopyThread()");

	CFileCopyDlg* pParent = (CFileCopyDlg*)param;
	CCopyInfo* pCopy = NULL;
	CString strSrc, strTarget, strLocation;
	BYTE* buf = NULL;
	TCHAR szError[1024];
	CFile fileSrc, fileTarget;
	CFileException exSrc, exTarget;
	ULONGLONG ulRead, ulTotal;
	int nRate;

	pParent->m_nFailCount = 0;

	buf = (BYTE*)malloc(sizeof(BYTE)*SIZE_FILE_BUF);

	for(int i=0; i<pParent->m_paryCopyInfo->GetCount(); i++)
	{
		if(pParent->m_bCancel)
		{
			pParent->PostMessage(WM_CANCEL_FILE_COPY, 0, 0);	//cancel
			free(buf);
			__DEBUG__("FileCopy Canceled 1");
			return 0;
		}//if

		pCopy = pParent->m_paryCopyInfo->GetAt(i);
		if(pCopy->m_ulFileSize == 0)
		{
			pParent->SendMessage(WM_FAIL_FILE_COPY, (WPARAM)i, (LPARAM)(LPSTR)"File size is Zero");	//error
			__DEBUG__("File Size 0");
			continue;
		}//if
		
		//경로 만들기
		if(pCopy->m_nContentType == CONTENTS_PACKAGE || pCopy->m_nContentType == CONTENTS_BPI)
		{
			strSrc.Format("%s\\%s%s", pCopy->m_strSrcDrive, IMPORT_CONFIG_PATH, pCopy->m_strFileName);
			strTarget.Format("%s\\%s%s", pCopy->m_strTargetDrive, IMPORT_CONFIG_PATH, pCopy->m_strFileName);
		}
		else if(pCopy->m_strLocation.GetLength() != 0)
		{
			strLocation = pCopy->m_strLocation;

			strLocation.Replace("/", "\\");
			int nIdx = strLocation.Find(_T("\\"), 1);
			if(nIdx != -1)
			{
				nIdx = strLocation.Find(_T("\\"), nIdx+1);
				if(nIdx != -1)
				{
					strLocation.Delete(0, nIdx+1);
				}//if
			}//if
			//strLocation.TrimLeft("/");
			strLocation.TrimLeft("\\");
			strSrc.Format("%s\\%s%s%s", pCopy->m_strSrcDrive, IMPORT_CONTENTS_PATH, strLocation, pCopy->m_strFileName);
			strTarget.Format("%s\\%s%s%s", pCopy->m_strTargetDrive, IMPORT_CONTENTS_PATH, strLocation, pCopy->m_strFileName);
		}
		else
		{
			strSrc.Format("%s\\%s%s", pCopy->m_strSrcDrive, IMPORT_CONTENTS_PATH, pCopy->m_strFileName);
			strTarget.Format("%s\\%s%s", pCopy->m_strTargetDrive, IMPORT_CONTENTS_PATH, pCopy->m_strFileName);
		}//if

		__DEBUG__("MakeSureDirectoryPathExists(%s)", strTarget);
		::MakeSureDirectoryPathExists(strTarget);

		//파일 복사
		try
		{
			__DEBUG__("fileSrc Open(%s)", strSrc);
			if(fileSrc.Open(strSrc, CFile::modeRead|CFile::shareDenyNone|CFile::typeBinary, &exSrc))
			{
				__DEBUG__("fileSrc Open(%s) succeed", strSrc);
				//타겟 파일이 히든으로 존재할때 쓰기 오류가 나므로 속성을 바꾸어 준다
				SetFileAttributes(strTarget, FILE_ATTRIBUTE_NORMAL);

				if(fileTarget.Open(strTarget, CFile::modeCreate|CFile::modeWrite|CFile::typeBinary, &exTarget))
				{
					__DEBUG__("fileTarget Open(%s) succeed", strTarget);
					ulRead = 0;
					ulTotal = 0;
					memset(buf, 0x00, SIZE_FILE_BUF);
					DWORD dwStart = ::GetTickCount();
					while((ulRead = fileSrc.Read(buf, SIZE_FILE_BUF)) != 0)
					{
						if(pParent->m_bCancel)
						{
							fileSrc.Close();
							fileTarget.Close();
							free(buf);
							__DEBUG__("fileTarget (%s) canceled", strTarget);

							pParent->PostMessage(WM_CANCEL_FILE_COPY, 0, 0);		//cancel
							return 0;
						}//if

						fileTarget.Write(buf, ulRead);
						ulTotal += ulRead;
						nRate = (ulTotal*100)/pCopy->m_ulFileSize;

						DWORD dwNow = ::GetTickCount();
						if(dwNow - dwStart >= 500)
						{
							pParent->PostMessage(WM_STATUS_FILE_COPY, (WPARAM)i, (LPARAM)nRate);
							dwStart = ::GetTickCount();
						}//if
					}//while
					fileTarget.Close();
					pParent->PostMessage(WM_STATUS_FILE_COPY, (WPARAM)i, (LPARAM)100);
					pParent->SendMessage(WM_FAIL_FILE_COPY, (WPARAM)i, (LPARAM)(LPSTR)"Success");	//error
					fileSrc.Close();
				}
				else
				{
					__DEBUG__("target file(%s) open failed try again", strTarget);
					fileSrc.Close();
					// skpark 2013.2.20
					// targetFile 을 open 하는데 실패한경우인데,  이 경우 CopyFile 로 다시한번 더 시도한다.
					if(::MoveFileEx(strSrc, strTarget, MOVEFILE_REPLACE_EXISTING|MOVEFILE_COPY_ALLOWED)){
						pParent->PostMessage(WM_STATUS_FILE_COPY, (WPARAM)i, (LPARAM)100);
						pParent->SendMessage(WM_FAIL_FILE_COPY, (WPARAM)i, (LPARAM)(LPSTR)"Success(2)");	//error
					}else{
						pParent->m_nFailCount++;
						exTarget.GetErrorMessage(szError, 1024);
						pParent->SendMessage(WM_FAIL_FILE_COPY, (WPARAM)i, (LPARAM)(LPSTR)"File write fail");	//error
						__DEBUG__("target file(%s) open failed finaly", strTarget);
					}
				}//if
			}
			else
			{
				__DEBUG__("fileSrc Open(%s) failed, failCount=%d, error=%s", strSrc, pParent->m_nFailCount,szError);
				pParent->m_nFailCount++;
				// skpark 
				memset(szError,0x00,sizeof(szError));
				exSrc.GetErrorMessage(szError, 1024);
				//AfxMessageBox(szError);
				//pParent->SendMessage(WM_FAIL_FILE_COPY, (WPARAM)i, (LPARAM)(LPSTR)"File does not exist");	//error
				pParent->SendMessage(WM_FAIL_FILE_COPY, (WPARAM)i, (LPARAM)(LPSTR)szError);	//error
			}//if
		}
		catch(CFileException* ex)
		{
			pParent->m_nFailCount++;
			CString msg;
			ex->GetErrorMessage(szError, 1024);
			msg.Format("(%d) (%s) %s", ex->m_cause, ex->m_strFileName, szError);
			ex->Delete();
			__DEBUG__("Exception : %s", msg);
			pParent->SendMessage(WM_FAIL_FILE_COPY, (WPARAM)i, (LPARAM)(LPSTR)szError);	//error
		}//try
	}//for

	free(buf);
	pParent->PostMessage(WM_COMPLETE_FILE_COPY, 0, 0);
	__DEBUG__("FileCopyThread() exit");
	return 0;
}

LRESULT CFileCopyDlg::OnCompleteFileCopy(WPARAM wParam, LPARAM lParam)
{
	m_bWorking = false;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_BUTTON_OK);
	pBtn->EnableWindow(TRUE);
	pBtn = (CButton*)GetDlgItem(IDC_BUTTON_CANCEL);
	pBtn->EnableWindow(FALSE);

	// skpark USB_Device
	if(ciArgParser::getInstance()->isSet("+USB_AUTO_IMPORT")){

		ciString program;
		ciArgParser::getInstance()->getArgValue("+ini", program);
		if(program.empty()){
			return 0;
		}
		// FileCopy 가 종료했으므로, 성공한 경우에 한해서 !!!
		// starter 옵션을 고치고, 브라우저를 다시 띠우게 된다. firmwareView 도 refresh 한다.
		CString strArg;
		strArg.Format("+ini %s +display 0 +nomsg +reservationStart", program.c_str());
		ciString debugMsg = "socketAgent call : import ";
		debugMsg += strArg;
		//AfxMessageBox(debugMsg.c_str());
		if(!scratchUtil::getInstance()->socketAgent("127.0.0.1", 14007, "import", strArg))
		{
			debugMsg = "socketAgent call fail : import ";
			debugMsg += strArg;
			//AfxMessageBox(debugMsg.c_str());
		}
		// USB Drive 도 자동으로 빼준다.
		ciString driveStr;
		ciArgParser::getInstance()->getArgValue("+drive", driveStr);
		if(!driveStr.empty()){
			//debugMsg = "Eject " ;
			//debugMsg += driveStr;
			//AfxMessageBox(debugMsg.c_str());
			if(false == CEjectRemovableDrive::GetInstance()->EjectDrive(driveStr[0],false)){
				//debugMsg += " failed";
				//AfxMessageBox(debugMsg.c_str());
			}
		}
		::Sleep(2000);
		OnBnClickedButtonOk();
	}
	return 0;
}

LRESULT CFileCopyDlg::OnStatusFileCopy(WPARAM wParam, LPARAM lParam)
{
	int nIdx = (int)wParam;
	if(nIdx < 0 || nIdx > m_listFile.GetItemCount()-1)
	{
		return 0;
	}//if

	//m_listFile.SetItemData(nIdx, (DWORD)lParam);
	CString strPercent;
	strPercent.Format("%d %%", (int)lParam);
	m_listFile.SetItemText(nIdx, 2, strPercent);

	//진행중인 항목위 보이게 한다.
	m_listFile.EnsureVisible(nIdx+5, FALSE);
	//m_listFile.Invalidate();
	return 0;
}


LRESULT CFileCopyDlg::OnCancelFileCopy(WPARAM wParam, LPARAM lParam)
{
	m_bWorking = false;
	
	this->m_pParentWnd->PostMessage(WM_COMPLETE_FILE_COPY, 0, E_COPY_CANCEL);
	this->PostMessageA(WM_CLOSE);

	return 0;
}


LRESULT CFileCopyDlg::OnFailFileCopy(WPARAM wParam, LPARAM lParam)
{
	int nIdx = (int)wParam;
	if(nIdx < 0 || nIdx > m_listFile.GetItemCount()-1)
	{
		return 0;
	}//if

	CString strMsg = (LPSTR)lParam;
	m_listFile.SetItemText(nIdx, 3, strMsg);

	//진행중인 항목위 보이게 한다.
	m_listFile.EnsureVisible(nIdx+5, FALSE);
	//m_listFile.Invalidate();

	return 0;
}

void CFileCopyDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	
	CDialog::OnClose();
}

void CFileCopyDlg::OnBnClickedButtonOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_bCancel)
	{
		this->m_pParentWnd->PostMessage(WM_COMPLETE_FILE_COPY, 0, E_COPY_CANCEL);
	}
	else if(m_nFailCount != 0)
	{
		this->m_pParentWnd->PostMessage(WM_COMPLETE_FILE_COPY, 0, E_COPY_FAIL);
	}
	else
	{
		this->m_pParentWnd->PostMessage(WM_COMPLETE_FILE_COPY, 0, E_COPY_SUCCESS);
	}//if

	this->PostMessageA(WM_CLOSE);
}
