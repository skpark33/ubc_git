// FileCopyDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCImporter.h"
#include "FileCopyDialog.h"

#include "Dbghelp.h"
#include "shlwapi.h"

// CFileCopyDialog 대화 상자입니다.

#define SIZE_FILE_BUF	(1024*1024)	//1M
//#define		BUF_SIZE				(1024*1024)	// 파일 복사 버퍼 = 1 MByte


IMPLEMENT_DYNAMIC(CFileCopyDialog, CDialog)

CFileCopyDialog::CFileCopyDialog(CWnd* pParent, LPCSTR lpszDrive)
:	CDialog(CFileCopyDialog::IDD, pParent)
,	m_pParentWnd (pParent)
,	m_strTargetDrive (lpszDrive)
,	m_ulTotalSize (0)
,	m_pThread (NULL)
,	m_bProcessThread (true)
{

}

CFileCopyDialog::~CFileCopyDialog()
{
}

void CFileCopyDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_CURRENT, m_pbarCurrent);
	DDX_Control(pDX, IDC_PROGRESS_TOTAL, m_pbarTotal);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_staticStatus);
	DDX_Control(pDX, IDC_BUTTON_CANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CFileCopyDialog, CDialog)
	ON_WM_TIMER()
	ON_MESSAGE(WM_COMPLETE_FILE_COPY, OnCompleteFileCopy)
	ON_MESSAGE(WM_DISPLAY_STATUS, OnDisplayStatus)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CFileCopyDialog::OnBnClickedButtonCancel)
END_MESSAGE_MAP()


// CFileCopyDialog 메시지 처리기입니다.

//void CFileCopyDialog::PostNcDestroy()
//{
//	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
//
//	CDialog::PostNcDestroy();
//
//	delete this;
//}

BOOL CFileCopyDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_btnCancel.SetToolTipText("Cancel");
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	SetTimer(1020, 250, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFileCopyDialog::OnBnClickedButtonCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	m_mutex.Lock();
	m_bProcessThread = false;
	m_mutex.Unlock();

	if(m_pThread)
	{
		try
		{
			DWORD dwExitCode;

			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, INFINITE);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}
			delete m_pThread;
		}
		catch(...)
		{
		}

		m_pThread = NULL;
	}

	m_mapContents.RemoveAll();
	CDialog::OnCancel();
}

void CFileCopyDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == 1020)
	{
		KillTimer(1020);
		Start(true);
	}
}

void CFileCopyDialog::Start(bool bShowWindow)
{
	if(CDataContainer::getInstance()->GetContentsMap()->GetCount() == 0)
	{
		m_pParentWnd->PostMessage(WM_COMPLETE_FILE_COPY, 1); // all ok

		OnBnClickedButtonCancel();

		return;
	}//if

	m_pbarTotal.SetStyle(PROGRESS_TEXT);
	m_pbarTotal.SetRange(0, 1000);
	m_pbarTotal.SetPos(0);
	m_pbarTotal.SetText("");

	m_pbarCurrent.SetStyle(PROGRESS_TEXT);
	m_pbarCurrent.SetRange(0, 1000);
	m_pbarCurrent.SetPos(0);
	m_pbarCurrent.SetText("");

	//
	if(bShowWindow)
	{
		CenterWindow();
		ShowWindow(SW_SHOW);
	}

	m_staticStatus.SetWindowText("");
	m_ulTotalSize = GetTotalFileSize();

	//
	FILE_COPY_PARAM* param = new FILE_COPY_PARAM;
	if(param != NULL)
	{
		param->pParentWnd		= m_pParentWnd;
		param->pWnd				= this;
//		param->pStatic			= &m_staticStatus;
//		param->pbarCurrent		= &m_pbarCurrent;
//		param->pbarTotal		= &m_pbarTotal;
		param->ulTotalSize		= m_ulTotalSize;
		param->strTargetDrive	= m_strTargetDrive;

		//
		m_pThread = ::AfxBeginThread(CFileCopyDialog::FileCopyThread, param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = FALSE;
		m_pThread->ResumeThread();
	}

}

ULONGLONG CFileCopyDialog::GetTotalFileSize()
{
	ULONGLONG total_size = 0;

	CONTENTS_INFO_MAP* contents_map = CDataContainer::getInstance()->GetContentsMap();

	//중복된 컨텐츠 파일 크기계산 방지
	CMapStringToString mapContents;
	CONTENTS_INFO* stContents = NULL;

	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString contents_id;
		CONTENTS_INFO* contents_info;
		contents_map->GetNextAssoc( pos, contents_id, (void*&)contents_info );

		CString file_fullpath;
		file_fullpath.Format("%s%s", contents_info->strLocation, contents_info->strFilename);

		//중복 방지
		if(m_mapContents.Lookup(file_fullpath, (void*&)stContents))
		{
			continue;
		}
		else
		{
			m_mapContents.SetAt(file_fullpath, contents_info);
		}//if

		CFileStatus fs;
		if(CFile::GetStatus(file_fullpath, fs))
		{
			contents_info->nFilesize = fs.m_size;
			total_size += fs.m_size;
		}
	}

	return total_size;
}

LRESULT CFileCopyDialog::OnCompleteFileCopy(WPARAM wParam, LPARAM lParam)
{
	m_pParentWnd->PostMessage(WM_COMPLETE_FILE_COPY, wParam, lParam);

	m_mapContents.RemoveAll();
	OnBnClickedButtonCancel();

	return 0;
}

LRESULT CFileCopyDialog::OnDisplayStatus(WPARAM wParam, LPARAM lParam)
{
	DISPLAY_PARAM* param = (DISPLAY_PARAM*)wParam;

	CString		title			= param->title;
	CString		current_text	= param->current_text;
	int			current_pos		= param->current_pos;
	CString		total_text		= param->total_text;
	int			total_pos		= param->total_pos;

	delete param;

	m_staticStatus.SetWindowText(title);

	m_pbarCurrent.SetText(current_text);
	m_pbarCurrent.SetPos(current_pos);

	m_pbarTotal.SetText(total_text);
	m_pbarTotal.SetPos(total_pos);

	return 0;
}


/////////////////////////////////////////////////////////////////////////////

UINT CFileCopyDialog::FileCopyThread(LPVOID pParam)
{
	//
	FILE_COPY_PARAM* param = (FILE_COPY_PARAM*)pParam;

	CWnd*				pParentWnd		= param->pParentWnd;
	CFileCopyDialog*	pWnd			= param->pWnd;
//	CStatic*			pStatic			= param->pStatic;
//	CProgressCtrlEx*	pbarCurrent		= param->pbarCurrent;
//	CProgressCtrlEx*	pbarTotal		= param->pbarTotal;
	ULONGLONG			ulTotalSize		= param->ulTotalSize;
	CString				strTargetDrive	= param->strTargetDrive;

	delete param;

	//
	CString target_path = strTargetDrive;
	//target_path.Append(EXPORT_CONTENTS_PATH);
	target_path.Append(IMPORT_CONTENTS_PATH);
	if(::MakeSureDirectoryPathExists(target_path) == FALSE)
	{
		if(::PathFileExists(target_path) == FALSE)
		{
			pWnd->PostMessage(WM_COMPLETE_FILE_COPY, 2); // fail to make folder
			// Modified by 정운형 2008-12-15 오후 7:05
			// 변경내역 :  오류처리
			return 0;
			// Modified by 정운형 2008-12-15 오후 7:05
			// 변경내역 :  오류처리
		}
	}

	//
	ULONGLONG ulCompleteDownloadSize = 0;
	ULONGLONG ulCurrentDownloadSize = 0;
	ULONGLONG ulCurrentFileSize = 0;
	BYTE* buf = new BYTE[SIZE_FILE_BUF];

	CStringArray* paryError = NULL;
	CString strCompleteFile;

	int i=-1;
	CONTENTS_INFO_MAP* pmapContents = &(pWnd->m_mapContents);
	int count = pmapContents->GetCount();
	POSITION pos = pmapContents->GetStartPosition();

	while(pos != NULL)
	{
		i++;
		CString contents_id;
		CONTENTS_INFO* contents_info;
		pmapContents->GetNextAssoc( pos, contents_id, (void*&)contents_info );

		ulCompleteDownloadSize += ulCurrentFileSize;
		ulCurrentFileSize = contents_info->nFilesize;
		ulCurrentDownloadSize = 0;

		CString status;
		status.Format("%s (%d/%d)", contents_info->strFilename, i+1, count);
		//pStatic->SetWindowText(status);

		//pbarCurrent->SetText("00%% (00:00:00)");
		//pbarCurrent->SetPos(0);

		//pbarTotal->SetText("00%% (00:00:00)");
		////pbarTotal->SetPos(0);

		//
		DISPLAY_PARAM* disp_param = new DISPLAY_PARAM;
		disp_param->title = status;
		disp_param->current_text = "00%% (00:00:00)";
		disp_param->current_pos = 0;
		disp_param->total_text = "00%% (00:00:00)";
		disp_param->total_pos = 0;

		pWnd->PostMessage(WM_DISPLAY_STATUS, (WPARAM)disp_param);

		//
		DWORD	dwStartTick = 0;
		DWORD	dwEndTick = 0;

		if(pWnd->m_bProcessThread == false)
		{
			delete []buf;

			if(paryError)
			{
				delete paryError;
			}//if

			pParentWnd->PostMessage(WM_COMPLETE_FILE_COPY, 0);	// cancel
			return 0;
		}

		if(contents_info->strFilename == "" || contents_info->strFilename.Left(7) == _T("http://")){
			continue;
		}

		try
		{
			CString source_path = contents_info->strLocation;
			source_path.Append(contents_info->strFilename);

			CString target_path = strTargetDrive;
			//target_path.Append(EXPORT_CONTENTS_PATH);
			target_path.Append(IMPORT_CONTENTS_PATH);
			target_path.Append(contents_info->strFilename);

			CFile source_file, target_file;
			CFileException exSrc, exTarget;
			if(source_file.Open(source_path, CFile::modeRead|CFile::shareDenyNone|CFile::typeBinary, &exSrc))
			{
				//타겟 파일이 히든으로 존재할때 쓰기 오류가 나므로 속성을 바꾸어 준다
				DWORD dwOldAtt = GetFileAttributes(target_path);
				SetFileAttributes(target_path, FILE_ATTRIBUTE_NORMAL);

				if(target_file.Open(target_path, CFile::modeCreate|CFile::modeWrite|CFile::typeBinary, &exTarget))
				{
					dwStartTick = ::GetTickCount();

					int read;
					while((read = source_file.Read(buf, SIZE_FILE_BUF)) != 0)
					{
						pWnd->m_mutex.Lock();
						if(pWnd->m_bProcessThread == false)
						{
							pWnd->m_mutex.Unlock();
							source_file.Close();
							target_file.Close();
							delete []buf;

							if(paryError)
							{
								delete paryError;
							}//if

							pParentWnd->PostMessage(WM_COMPLETE_FILE_COPY, 0); // cancel
							return 0;
						}//if

						target_file.Write(buf, read);
						ulCurrentDownloadSize += read;

						dwEndTick = ::GetTickCount();

						if(ulCurrentDownloadSize == 0)
						{
							TRACE("!!!\r\n");
						}

						if(contents_info->nFilesize == 0)
						{
							TRACE("!!!\r\n");
						}

						// 
						int remain_sec_current = (dwEndTick-dwStartTick) * (contents_info->nFilesize - ulCurrentDownloadSize) / ulCurrentDownloadSize / 1000;
						int remain_sec_total = (dwEndTick-dwStartTick) * ( ulTotalSize - ulCompleteDownloadSize - ulCurrentDownloadSize) / ulCurrentDownloadSize / 1000;

						//
						DISPLAY_PARAM* disp_param = new DISPLAY_PARAM;
						disp_param->title = status;

						//
						CString bar_text;
						CTimeSpan remain_time_current(remain_sec_current);
						int percent_current = ulCurrentDownloadSize*1000/contents_info->nFilesize;
						bar_text.Format("%02d%% (%s)", percent_current/10, remain_time_current.Format("%H:%M:%S"));
						//pbarCurrent->SetText(bar_text);
						//pbarCurrent->SetPos(percent_current);
						disp_param->current_text = bar_text;
						disp_param->current_pos = percent_current;

						//
						CTimeSpan remain_time_total(remain_sec_total);
						int percent_total = (ulCompleteDownloadSize+ulCurrentDownloadSize)*1000/ulTotalSize;
						bar_text.Format("%02d%% (%s)", percent_total/10, remain_time_total.Format("%H:%M:%S"));
						//pbarTotal->SetText(bar_text);
						//pbarTotal->SetPos(percent_total);
						disp_param->total_text = bar_text;
						disp_param->total_pos = percent_total;

						pWnd->PostMessage(WM_DISPLAY_STATUS, (WPARAM)disp_param);

						pWnd->m_mutex.Unlock();
					}//while
					target_file.Close();

#ifdef _USE_DRM
					//타겟 파일을 히든으로 속성을 바꾸어 준다
					if(dwOldAtt == 0xffffffff)
					{
						SetFileAttributes(target_path, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM);
					}
					else
					{
						SetFileAttributes(target_path, dwOldAtt);
					}//if
#endif
				}
				else
				{
					TCHAR szError[1024];
					exTarget.GetErrorMessage(szError, 1024);

					if(paryError == NULL)
					{
						paryError = new CStringArray;
					}//if
					paryError->Add(contents_info->strFilename + " : " + szError);
					continue;

					//pWnd->PostMessage(WM_COMPLETE_FILE_COPY, 5); // target open error
					//return 0;
				}//if
				source_file.Close();
			}
			else
			{
				TCHAR szError[1024];
				exSrc.GetErrorMessage(szError, 1024);

				//Open source file fail
				if(paryError == NULL)
				{
					paryError = new CStringArray;
				}//if
				paryError->Add(contents_info->strFilename + " : " + szError);
				continue;
			}//if
		}
		catch(CFileException* ex)
		{
			CString msg;
			TCHAR szCause[1024];
			ex->GetErrorMessage(szCause, 1024);
			msg.Format("(%d) (%s) %s", ex->m_cause, ex->m_strFileName, szCause);
			ex->Delete();

			if(paryError == NULL)
			{
				paryError = new CStringArray;
			}//if
			paryError->Add(msg);
			continue;

			//pWnd->PostMessage(WM_COMPLETE_FILE_COPY, 3); // file exception
			//return 0;
		}//try
	}//while

	delete[]buf;

	if(paryError == NULL)
	{
		pWnd->PostMessage(WM_COMPLETE_FILE_COPY, 1, 0); // all ok
	}
	else
	{
		pWnd->PostMessage(WM_COMPLETE_FILE_COPY, 1, (LPARAM)paryError); // some file copy fail
	}//if

	return 0;
}
