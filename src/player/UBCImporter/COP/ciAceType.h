/*! \file ciAceType.h
 *  Copyright ⓒ 2003, WINCC Inc. All rights reserved.
 *
 *  \brief 해당 파일에 대한 설명 
 *
 *  \author  csjung
 *  \version EOT C2.0
 *  \date    2003/04/26
 */

#ifndef __ciAceType_h__
#define __ciAceType_h__

#include "ace/OS.h"
#include "ace/Event_Handler.h"
#include "ace/Message_Queue_T.h"
#include "ace/Message_Block.h"
#include "ace/Reactor.h"
#include "ace/Svc_Handler.h"
#include "ace/Synch.h"
#include "ace/SOCK_Stream.h"
#include "ace/Task.h"
#include "ace/Thread_Manager.h"
#include "ace/Timer_Heap_T.h"
#include "ace/Timer_Queue_Adapters.h"
#include "ace/Process.h"
#include "ace/Process_Manager.h"
#include "ace/Event_Handler.h"

#define	MAXCOUNT_CANSEND_TIMEOUT	10
#define	MAXCOUNT_CANRECV_TIMEOUT	10
#define	BUS_MAXNUM_QUEUESIZE		(4096 * ACE_Message_Queue_Base::DEFAULT_HWM)

//typedef ACE_OS								ciAOS;
typedef ACE_HANDLE			                ciAHandle;
typedef ACE_Reactor_Mask					ciAReactorMask;
typedef ACE_Reactor			                ciAReactor;
typedef ACE_Message_Block	            	ciAMessageBlock;
typedef ACE_Time_Value                    	ciATimeValue;
typedef ACE_Thread_Manager					ciAThreadManager;
typedef ACE_Message_Queue<ACE_MT_SYNCH> 	ciAMessageQueue;
typedef ACE_mutexattr_t						ciAMutexAttr;
typedef ACE_Condition_Attributes			ciACondAttr;
typedef ACE_INET_Addr						ciAInetAddr;

typedef ACE_Event_Handler					ciAEventHandler;	// madmage	2003/05/14

typedef ACE_Task<ACE_MT_SYNCH>	ciATask_MtSynch;
typedef ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_MT_SYNCH>	ciASvcHandler_Ss_MtSynch;

/////////////////////////////////////////////////////////////////////////////////////
// Process class set 
// madmage 2003/05/14
typedef ACE_Process_Options					ciAProcessOptions;
typedef ACE_Process_Manager					ciAProcessManager;
//////////////////////////////////////////////////////////////////////////////////////

/*
typedef ACE_Event_Handler_Handle_Timeout_Upcall<ACE_Null_Mutex>  Upcall;
typedef ACE_Timer_Heap_T<ACE_Event_Handler *, Upcall, ACE_Null_Mutex>    Timer_Heap;
typedef ACE_Timer_Heap_Iterator_T<ACE_Event_Handler *, Upcall, ACE_Null_Mutex> Timer_Heap_Iterator;
typedef ACE_Thread_Timer_Queue_Adapter<Timer_Heap>  Thread_Timer_Queue;
*/

#if defined (HPUX)
# if defined (IOR)
# undef IOR
# endif //IOR
# if defined (SERVER)
# undef SERVER
# endif //SERVER
#endif //HPUX

#endif
