/*
*  Copyright ⓒ 2002 iTelComSoft Inc.
*  All Rights Reserved.
*
*  Created by  : Lim chae suk
*  Modified by :
*  Last updae  : 2002/12/05
*  File name   : ciConfig.h
*/

#ifndef _ci_Config_H_
#define _ci_Config_H_

//----------------------------------------------------------------------------
//	Include Header File
//----------------------------------------------------------------------------
#include "COP/ciBaseType.h"
#include <map>

#define	COP_TNS			"COP_TNS"
#define	COP_USR			"COP_USR"
#define	COP_PWD			"COP_PWD"
#define	COP_SM_USR		"COP_SM_USR"
#define	COP_SM_PWD		"COP_SM_PWD"

#define	COPST_TNS			"COPST_TNS"
#define	COPST_USR			"COPST_USR"
#define	COPST_PWD			"COPST_PWD"
#define	COPST_SM_USR		"COPST_SM_USR"
#define	COPST_SM_PWD		"COPST_SM_PWD"	

#define CFG_VALUE_DELIMITER     ","

/// ciConfig : Configuration File 제어하는 Class
//--------------------------------------------------------------------
//! ciConfig Class
//--------------------------------------------------------------------
/*
Configuration File Class
*/

typedef map<ciString, ciString> ciConfigMap;
typedef ciConfigMap::value_type	ciConfigValueType;
typedef ciConfigMap::iterator		ciConfigIterator;

class COP_CI_API ciConfig// : public map<ciString, ciString>
{
protected:
	FILE*		_fd;
	ciString	_errMsg;

protected:
	int		parserFile( void );
	int		parserString( char* );
	void    replaceNewLine( char*, char* );

public:
	ciString getError( void );
	int		 load( const char*, const char*, const char*, const char* );

	static const char* getValue(const char* module, const char* entry,
		const char* dir = "config",  const char* file="cop.cfg");

public:
	ciConfig();
	~ciConfig();

	ciConfigIterator	begin();
	ciConfigIterator	end();
	ciBoolean			insert(ciConfigValueType& pVal);
	void				clear();
	ciConfigIterator	find(const char* pVal);
	ciConfigIterator	find(ciString& pVal);
	ciBoolean			empty();

	size_t				size() const;

	// load 함수를 호출하기 전에 호출해야지만 유효하다.
	virtual void        setEnvTranslateFlag(ciBoolean envTranslateFlag);

private:
	ciConfigMap	_configMap;
	ciBoolean _envTranslateFlag;
};

//typedef ciConfig::value_type	ciConfigValueType;
//typedef ciConfig::iterator		ciConfigIterator;

#define	GET_CONFIG_ID(ITR)		(*ITR).first
#define	GET_CONFIG_VALUE(ITR)	(*ITR).second

class COP_CI_API ciCommonConfig
{
protected:
	static	ciConfig*	_common;
public:
	static	ciConfig* getInstance();
};


#endif // _KC_CONFIG_H_

