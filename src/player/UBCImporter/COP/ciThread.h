#ifndef __CI_THREAD_H__
#define __CI_THREAD_H__

#include "ciBaseType.h"
#include "ciSyncUtil.h"
#include "ciRunnable.h"

class ciThreadGroup;

enum UNEXPECTED_REASON {
	NO_MEMORY = 0
};

struct UnExpectedException {
	UNEXPECTED_REASON reason;

	UnExpectedException();
	UnExpectedException(UNEXPECTED_REASON reason);
	~UnExpectedException();
};

typedef enum {
	THREAD_CREATE,
	THREAD_PROCESSING,
	THREAD_SUSPEND,
	THREAD_DEAD
} THREAD_STATUS;

extern "C" {
	void* exec_thread_adapter( void* th );
}
	
class COP_CI_API ciThread {
	private:

#ifdef _COP_MSC_
		HANDLE _handle;
		DWORD _id;
#else // UNIX PTHREAD
		ciULong _handle;
		pthread_t _id;
#endif
		ciMutex  _lock;
		ciSyncEvent  _joinevent;

		THREAD_STATUS _status;
		ciLong _priority;
		ciRunnable * _runobj;
		void execproc();
		void terminate();
		//static void* exec_thread( ciThread *th );
		static void* exec_thread( void* th );

		friend void* exec_thread_adapter( void* th );

	public:

		ciThread();
		ciThread(ciRunnable* robj);
		virtual ~ciThread();

		void start();

		void join();
		void join(ciLong millis);
		void join(ciLong millis, ciLong nanos);

		ciBoolean setPriority(ciLong newPriority);
		ciLong getPriority();

		int suspend();
		int resume();
		void stop();

		ciBoolean isAlive();
		void checkAccess();

		ciThreadGroup& getThreadGroup();

		static void sleep(ciLong seconds);
		static void msleep(ciLong millis);
		static void msleep(ciLong millis, ciLong nanos);

		static ciThread& currentThread();
		virtual void run();
};

class COP_CI_API ciThreadGroup
{
};

#endif

