#ifndef _ciEnv_h_
#define _ciEnv_h_

#include "COP/ciBaseType.h"
#include "COP/ciSyncUtil.h"
#include "COP/ciPath.h"
#include <map>

class COP_CI_API ciEnv {
public:
	typedef map<ciString, ciString*>  _ENVMap;
	static char*		getenv(const char* pName);
	static ciBoolean	getenv(const char* pName, ciString& pValue);
	static ciBoolean	getenv2(const char* pName, ciString& pValue);

	static const char*	newEnv(const char* pName);
	static void			clearEnv();

	static void setEnv(const char* name, const char* value);
	static ciBoolean defaultEnv(ciBoolean ignore_env=ciFalse,
									const char* projectCode=0, 
									const char* home=0);

protected:

	static _ENVMap	_map;
	static ciMutex _maplock;
};

#endif // _ciEnv_h_
