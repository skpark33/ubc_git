/*
 *  Copyright �� 200l SQISoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2002/12/05
 *  File name   : ciFileUtil.h
 */

#ifndef _ciFileUtil_h_
#define _ciFileUtil_h_

#include "COP/ciBaseType.h"

class COP_CI_API ciFileUtil
{
public:
	static ciBoolean openFile(FILE*& outFd, 
							const char* pMode,
							const char* pFilename, 
							const char* pSubPath = "", 
							const char* pHomeDir = "CONFIGROOT");

	static void makePath(ciString& outFullPath,
						const char* pFilename, 
						const char* pSubPath, 
						const char* pHomeDir);

	static ciBoolean isPathStartFromRoot(const char* pPath);

	static void addPath(ciString& root, const char* added);
	static void CreateDir(const char* pPath);
};

#endif 

