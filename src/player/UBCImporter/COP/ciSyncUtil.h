#ifndef _ciSyncUtil_h_
#define _ciSyncUtil_h_

#include "ciBaseType.h"

#ifdef _COP_MSC_
	class ACE_Thread_Mutex;
#endif

#ifndef _COP_MSC_ 
#include <pthread.h>
#include <semaphore.h>
#endif

class COP_CI_API ciCriticalSection
{
public:
	ciCriticalSection();
	~ciCriticalSection();

	void lock();
	void unlock();

protected:
#ifdef _COP_MSC_
	CRITICAL_SECTION _handle;
#else // UNIX PTHREAD
	pthread_mutex_t _handle;
#endif

};

class COP_CI_API ciMutex
{
public:
	ciMutex();
	~ciMutex();
	
	void lock(long millis = COP_INFINITE);
	void unlock();

	friend class ciCondition;

#ifdef _COP_MSC_
	ACE_Thread_Mutex& getMutex();
#endif

protected:
#ifdef _COP_MSC_
	class ciMutexImpl* m;
#else // UNIX PTHREAD
	pthread_mutex_t _handle;
#endif
};

class COP_CI_API ciSemaphore
{
public:
	ciSemaphore(long maxvalue = 100);
	~ciSemaphore();

	void lock(long millis = COP_INFINITE);
	void unlock(long count = 1);

protected:
#ifdef _COP_MSC_
	HANDLE _handle;
#else // UNIX PTHREAD
	sem_t  _handle;
#endif
};

class COP_CI_API ciSyncEvent {
public:
	ciSyncEvent();
	~ciSyncEvent();

	void post();
	void wait( long millis = COP_INFINITE );
	void reset();
	void pulse();

protected:
#ifdef _COP_MSC_
	HANDLE _handle;
#else // UNIX PTHREAD
	pthread_cond_t _handle;
#endif
};


class COP_CI_API ciCondition {
public:
	enum WakeupStatus {
		NOTIFIED,
		TIMEDOUT,
		INVALID
	};

	ciCondition(ciMutex& mut);
	~ciCondition();

	WakeupStatus wait(long millis = COP_INFINITE);

#ifndef _COP_MSC_
	void wait(struct timespec s);
#endif

	void notify();
	void notifyAll();

protected:
#ifdef _COP_MSC_
	class ciConditionImpl* m;
#else // UNIX PTHREAD
	ciMutex& _mutex;
	pthread_cond_t _handle;
#endif

	long _count;
};


class COP_CI_API ciSyncSignal {
public:
#ifdef _COP_MSC_
	HANDLE _sem;
	HANDLE _reqmutex;
#else //UNIX PTHREAD
	sem_t _sem;
	pthread_mutex_t _reqmutex;
	pthread_cond_t _done_cond;
#endif

	ciSyncSignal();
	virtual ~ciSyncSignal();

	void lock();
	void unlock();
	void wait(bool *done);
	void notify(bool *done);
};

template<class LOCK>
class ciBaseGuard {
protected:
	LOCK *_lock;
	bool _owner;

public:
	ciBaseGuard(LOCK &t);
	~ciBaseGuard();
};

template<class LOCK>
ciBaseGuard<LOCK>::ciBaseGuard(LOCK& t)	: _lock(&t), _owner(false) { 
	_lock->lock();
	_owner = true; 
}

template<class LOCK>
ciBaseGuard<LOCK>::~ciBaseGuard() {
	if (_owner)
		_lock->unlock();
}

typedef ciBaseGuard<ciMutex> ciGuard;

#define CI_GUARD(__lock__, __fn__) \
    ciGuard aGuard(__lock__)
#endif //_ciSyncUtil_h_
