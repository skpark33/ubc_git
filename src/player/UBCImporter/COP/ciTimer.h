/*! \class ciTimer
 *  Copyright �� 2002, K3F. All rights reserved.
 *
 *  \brief 
 *  (Environment: ACE 5.3.1, Tru64 5.1)
 *
 *  \author tface
 *  \version 1.0
 *  \date 2003/04/24 18:01:00
 */

#ifndef _ciTimer_h_
#define _ciTimer_h_

#include "ciBaseType.h"
#include "ciThread.h"

class ciPollable;
/***********************************
/ add    : stjoo
/ reason : ciATask_MtSynch is not enable.
/ status : ING
*******************************************/
class COP_CI_API ciTimer : public ciThread
{
public:
	ciTimer(const char* pTimerName="defualt Timer");
	ciTimer(const char* pTimerName, ciPollable *pPollable);
	virtual ~ciTimer();

	virtual void run();
	virtual void processExpired();

    virtual void startTimer();
    virtual void stopTimer() ;
    virtual void setInterval(int pInterval);

protected:
	int 			_counter ;
    int 			_interval;
	ciBoolean		_started;
	ciString		_name;

	ciPollable *_pollable;
};

class COP_CI_API ciPollable {
public:
    ciPollable() { _timer = 0; }
    virtual ~ciPollable() { 
		stopTimer();
		if(_timer) delete _timer; 
	}

    virtual void processExpired(ciString name, int counter, int interval) = 0;

    virtual void initTimer(const char* pname, int interval = 60)  
	{ 
		if(_timer) {
			stopTimer();
			delete _timer; 
		}
		_timer = new ciTimer(pname, this); 
	    _timer->setInterval(interval);
	}
    virtual void startTimer()                   { if(_timer) _timer->startTimer(); }
    virtual void stopTimer()                    { if(_timer) _timer->stopTimer(); }
    virtual void setInterval(int pInterval)     { if(_timer) _timer->setInterval(pInterval); }

protected:

    ciTimer* _timer;
};


#endif // _ciTimer_h_
