/*
 *  Copyright ⓒ 2002 SQISSoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2005/12/05
 *  File name   : ciXConfig.h
 */

#ifndef _ciXConfig_h_
#define _ciXConfig_h_

//----------------------------------------------------------------------------
//	Include Header File
//----------------------------------------------------------------------------
#include "COP/ciBaseType.h"
#include "COP/ciSyncUtil.h"
#include <map>

typedef map<ciString, ciString>			ciConfigEntry;
typedef map<ciString, ciConfigEntry*>	ciXConfigMap;

class COP_CI_API ciXConfig
{
public:
	ciXConfig();
	virtual ~ciXConfig();

	virtual ciBoolean		load(const char* pFilename, 
								const char* pSubPath="", 
								const char* pHomeDir="CONFIGROOT");

	virtual ciBoolean		write(const char* pFileName,
								const char* pSubPath="", 
								const char* pHomeDir="CONFIGROOT");

	virtual ciBoolean		cfg_multi_load(const char* pFileName);

	virtual void			printIt();
	virtual void			clear();

	virtual ciXConfigMap*	getMap() { return &_configMap; }
	virtual void			setMap(ciXConfigMap* configMap);

	virtual ciConfigEntry*	findEntry(const char* pEntry);
	virtual const char*		findValue(const char* pEntry, const char* pName);
	virtual ciBoolean		findValue(const char* pEntry, const char* pName, ciString& pOutValue);

	// load 함수를 호출하기 전에 호출해야지만 유효하다.
	virtual void			setEnvTranslateFlag(ciBoolean envTranslateFlag);

protected:
	virtual ciBoolean		_load(const char* pFilename, 
								const char* pSubPath="", 
								const char* pHomeDir="CONFIGROOT");

	virtual ciBoolean		_parserFile(ciConfigEntry* pEntry);
	virtual ciBoolean		_parserString(char* ptr,ciConfigEntry* pEntry);
	virtual void    		_replaceNewLine(char*, char*);

	FILE*			_fd;

	ciMutex			_configLock;
	ciBoolean       _envTranslateFlag;
	ciXConfigMap	_configMap;
};

#endif 

