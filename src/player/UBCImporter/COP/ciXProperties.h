/*
 *  Copyright ⓒ 2002 SQISSoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2005/12/05
 *  File name   : ciXProperties.h
 */

#ifndef _ciXProperties_h_
#define _ciXProperties_h_

//----------------------------------------------------------------------------
//	Include Header File
//----------------------------------------------------------------------------
#include <COP/ciConfig.h>
#include <COP/ciXConfig.h>

/* 
  ciXPropertyMap : Property 의 데이터 구조 class
*/

typedef map<string,string>		PropertyMap;
typedef PropertyMap::iterator	ciXPropertyIterator;

class COP_CI_API ciXPropertyMap
{
public:
	ciXPropertyMap();
	virtual	~ciXPropertyMap();

	virtual int             size();
	virtual void			clear();

	virtual ciBoolean		get(const char* pName, ciString& pValue);
	virtual ciBoolean		get(const char* pName, CString& pValue);
	virtual ciBoolean		get(const char* pName, ciShort& pValue);
	virtual ciBoolean		get(const char* pName, ciLong& pValue);
	virtual ciBoolean		get(const char* pName, ciUShort& pValue);
	virtual ciBoolean		get(const char* pName, ciULong& pValue);
	virtual ciBoolean		get(const char* pName, ciDouble& pValue);
	virtual ciBoolean		get(const char* pName, ciBoolean& pValue);

	virtual ciBoolean		set(const char* pName, const char*	pValue);
	virtual ciBoolean		set(const char* pName, ciShort		pValue);
	virtual ciBoolean		set(const char* pName, ciLong		pValue);
	virtual ciBoolean		set(const char* pName, ciUShort		pValue);
	virtual ciBoolean		set(const char* pName, ciULong		pValue);
	virtual ciBoolean		set(const char* pName, ciDouble	pValue);
	virtual ciBoolean		set(const char* pName, ciBoolean	pValue);

	virtual ciBoolean		remove(const char* pName);

	virtual int				gets(const char* pattern, ciXPropertyMap& outMap,ciBoolean wildOnly=ciFalse);

	ciXPropertyIterator		begin() { return _map.begin(); }
	ciXPropertyIterator		end() { return _map.end(); }

protected:
	PropertyMap	_map;
	ciMutex		_lock;
};

/* 
  ciXPropteries : Property 의 wrapper class

*/
class COP_CI_API ciXProperties : public ciXPropertyMap
{
public:
	static  ciXProperties*	getInstance(const char* filename="PROJECT_CODE");
	static  ciXProperties*  initInstance(const char* filename="PROJECT_CODE");
	static  void			clearInstance(const char* filename="PROJECT_CODE");
	static  void			clearInstance();
	static  ciBoolean		putInstance(const char* filename, ciXProperties* pInst);

	ciXProperties();
	ciXProperties(const char* pFilename,
                  const char* pSubPath="",
                  const char* pHomeDir="CONFIGROOT");
	virtual	~ciXProperties();

    virtual ciBoolean       load(ciBoolean envTransFlag=ciTrue);
	virtual ciBoolean		write(const char* path=0);

	virtual const char*		getDefaultFileName();
	virtual void			setDefaultFileName();
	
protected:

	ciString	_path;
	ciString	_defaultFileName;
	
private:
	typedef map<string,ciXProperties*>	_InstMap;
	static _InstMap	_instMap;
	static ciMutex  _instLock;
};

#endif 

