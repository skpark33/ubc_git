// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <fstream>

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


// XP Theme (for VS2005)
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#include <afxmt.h>							//Mutex
#include "common/libCommon/utvMacro.h"
//#include "Dlls_ML/common/ubcdefine.h"

#define		BUF_SIZE						(1024*100)
#define		CUSTOMER_NARSHA					("NARSHA")
#define		_UBC_IMPORTER_
#define		IMPORT_CONFIG_PATH				("Sqisoft\\UTV1.0\\execute\\config\\")
#define		IMPORT_DATA_PATH				("Sqisoft\\UTV1.0\\execute\\data\\")
#define		IMPORT_CONTENTS_PATH			("Sqisoft\\contents\\ENC\\")
#define		SAMPLE_FILE_KEY					("__template_")
#define		PREVIEW_FILE_KEY				("__preview_")
#define		EXCUTE_PATH						("Sqisoft\\UTV1.0\\execute\\")
#define		EXCUTE_FILE						("SelfAutoUpdate.exe")

#define		WM_COMPLETE_FILE_COPY			(WM_USER + 110)		//파일복사 완료
#define		WM_STATUS_FILE_COPY				(WM_USER + 111)		//파일복사 상태표시
#define		WM_CANCEL_FILE_COPY				(WM_USER + 112)		//파일복사 취소
#define		WM_FAIL_FILE_COPY				(WM_USER + 113)		//파일복사 실패
//#define		WM_INFOIMPORT_COMPELETE			(WM_USER + 114)		
//#define		WM_SAVE_CONFIG_COMPELETE		(WM_USER + 115)
#define		WM_CHECKED_COUNT				(WM_USER + 200)		//인스톨 리스트에서 체크된 개수를 알리는 메시지


enum  CONTENTS_TYPE {
	CONTENTS_NOT_DEFINE = -1,
	CONTENTS_VIDEO = 0,
	CONTENTS_SMS,			//1. Horizontal Ticker
	CONTENTS_IMAGE,
	CONTENTS_PROMOTION,
	CONTENTS_TV,
	CONTENTS_TICKER,		//5. Vertical Ticker
	CONTENTS_PHONE,
	CONTENTS_WEBBROWSER,
	CONTENTS_FLASH,
	CONTENTS_WEBCAM,
	CONTENTS_RSS,			// 10.
	CONTENTS_CLOCK,
	CONTENTS_TEXT,			// 12. 이전 SMS
	CONTENTS_FLASH_TEMPLATE,
	CONTENTS_DYNAMIC_FLASH,
	CONTENTS_TYPING,
	CONTENTS_PPT,			//16
	CONTENTS_ETC,		// 2010.10.12 장광수 기타컨텐츠추가
	CONTENTS_WIZARD,	// 2010.10.21 정운형 마법사컨텐츠추가
	CONTENTS_FILE,		//플래쉬 부속파일
	CONTENTS_PACKAGE = 499,
	CONTENTS_BPI = 500
};


//컨텐츠 파일 복사 결과
enum E_COPY_RESULT
{
	E_COPY_FAIL,
	E_COPY_SUCCESS,
	E_COPY_CANCEL
};


CString		ToString(int nValue);
CString		ToString(double fValue);
CString		ToString(ULONG ulValue);
CString		ToString(ULONGLONG ulValue);

CString		ToMoneyTypeString(LPCTSTR lpszStr);
CString		ToMoneyTypeString(int value);

CString		GetTimeZoneString(int nTimeZone);
CString		GetContentsTypeString(int nContentsType);

COLORREF	GetColorFromString(LPCSTR lpszColor);
CString		GetColorFromString(COLORREF rgb);
void		GetColorRGB(COLORREF rgb, int& r, int& g, int& b);

LPCTSTR		GetDataPath();
LPCTSTR		GetAppPath();
CString		GetCoutomerInfo(void);							///<Customer Info를 반환한다.
LPCSTR		GetAppDrive(void);								///<프로그램이 구동된 드라이브를 반환한다.
bool		StopUBC(void);									///<실행중인 UBC를 중단 시킨다.
bool		StartUBC(void);									///<실행중인 UBC를 중단 시킨다.

#define		__DEBUG__	scratchUtil::getInstance()->myDEBUG

using namespace std;