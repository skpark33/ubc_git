// UBCImporterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCImporter.h"
#include "UBCImporterDlg.h"
#include "LocalHostInfo.h"
#include "shlwapi.h"
#include "Dbghelp.h"
#include "scratchUtil.h"
#include "ciArgParser.h"
//#include "STT/UBC/ubcIni.h"
#include "ubcIni.h"
#include "DriveSelectDialog.h"
#include "EjectRemovableDrive.h"
#include <io.h>
#include "FileCopyDlg.h"
#include "BpiInfo.h"
#include "ProfileManager.h"
#include "AnnounceInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define		TIMER_LIST_INIT			300


//리스트 패키지
static TCHAR* pszListPackage[] = { _T("Drive"),
								_T("컨텐츠패키지"),
								_T("수정날자"),
								_T("설명")
							  };

static TCHAR* pszListPackageEng[] = { _T("Drive"),
									_T("ContentsPackage"),
									_T("Last Modified"),
									_T("Desc.")
								};

//리스트 방송계획
static TCHAR* pszListBPI[] = { _T("Drive"),
								_T("방송계획"),
								_T("조직"),
								_T("수정날자"),
								_T("시작날자"),
								_T("종료날자"),
								_T("설명")
							  };

static TCHAR* pszListBPIEng[] = { _T("Drive"),
								_T("BroadcastPlan"),
								_T("Group"),
								_T("Last Modified"),
								_T("Start Date"),
								_T("End Date"),
								_T("Desc.")
							  };

//리스트 긴급공지
static TCHAR* pszListAnnounce[] = { _T("Drive"),
								_T("긴급공지"),
								_T("조직"),
								_T("수정날자"),
								_T("시작날자"),
								_T("종료날자"),
								_T("설명")
							  };

static TCHAR* pszListAnnounceEng[] = { _T("Drive"),
								_T("Announcement"),
								_T("Group"),
								_T("Last Modified"),
								_T("Start Date"),
								_T("End Date"),
								_T("Desc.")
							  };

//Type
static TCHAR* pszType[] = { _T("컨텐츠패키지"),
							_T("방송계획"),
							_T("긴급공지"),
						};

static TCHAR* pszTypeEng[] = { _T("ContentsPackage"),
							_T("BroadcastPlan"),
							_T("Announcement"),
						};


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CUBCImporterDlg 대화 상자




CUBCImporterDlg::CUBCImporterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCImporterDlg::IDD, pParent)
	//, m_strSrcPackageName(_T(""))
	//, m_strSrcDrive(_T(""))
	, m_strTargetDrive(_T(""))
	, m_bImport(true)
	, m_strArgDrive(_T(""))
	//, m_pdlgNotify(NULL)
	, m_bSilentImport(false)
	, m_strArgPackageName("")
	//, m_strSrcPackagePath("")
	, m_paryCopyInfo(NULL)
	, m_nCheckCount(0)
	, m_strArgBpiName("")
	, m_nType(E_IMPORT_PACKAGE)
	, m_bSilentExport(false)
	, m_announce_exist(false)
	, m_strArgAnnounceName("UBCAnnounce.ini")
{	
	m_nDisplayA = 0;
	m_nDisplayB = 0;
	m_bMessage = true;
	m_strDescription = _T("");
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUBCImporterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_HOST_LIST, m_listHost);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_TYPE_COMBO, m_comboType);
	DDX_Control(pDX, IDC_ACTION_COMBO, m_comboAction);
	DDX_Control(pDX, IDC_SEARCH_BTN, m_btnRefresh);
}

BEGIN_MESSAGE_MAP(CUBCImporterDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CUBCImporterDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CUBCImporterDlg::OnBnClickedCancel)
	ON_MESSAGE(WM_COMPLETE_FILE_COPY, OnCompleteFileCopy)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_MESSAGE(WM_CHECKED_COUNT, OnCheckedCount)
	ON_BN_CLICKED(IDC_SEARCH_BTN, &CUBCImporterDlg::OnBnClickedSearchBtn)
END_MESSAGE_MAP()


// CUBCImporterDlg 메시지 처리기

BOOL CUBCImporterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	CString strCustomer = GetCoutomerInfo();
	if(strCustomer == CUSTOMER_NARSHA)
	{
		m_hIcon = (HICON)LoadImage(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_NARSHA), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
	}//if

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	this->SetWindowText("UBC Importer");

	m_btnRefresh.SetToolTipText("Refresh");

	//m_btnOK.LoadBitmap(IDB_POP_BTN_SELECT, RGB(255, 255, 255));
	m_btnOK.SetToolTipText("Select");

	//m_btnCancel.LoadBitmap(IDB_POP_BTN_CANCEL, RGB(255, 255, 255));
	m_btnCancel.SetToolTipText("Cancel");

	//if(m_bImport)
	//{
	//	m_btnImport.LoadBitmap(IDB_IMPORT, RGB(255, 255, 255));
	//	m_btnExport.LoadBitmap(IDB_EXPORT_DISABLE, RGB(255, 255, 255));
	//}
	//else
	//{
	//	m_btnImport.LoadBitmap(IDB_IMPORT_DISABLE, RGB(255, 255, 255));
	//	m_btnExport.LoadBitmap(IDB_EXPORT, RGB(255, 255, 255));
	//}//if

	if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
	{
		m_comboType.AddString(pszType[0]);
		m_comboType.AddString(pszType[1]);
		m_comboType.AddString(pszType[2]);
	}
	else
	{
		m_comboType.AddString(pszTypeEng[0]);
		m_comboType.AddString(pszTypeEng[1]);
		m_comboType.AddString(pszTypeEng[2]);

		CButton* pBtn = (CButton*)GetDlgItem(IDOK);
		pBtn->SetWindowTextA("Ok");
		pBtn = (CButton*)GetDlgItem(IDCANCEL);
		pBtn->SetWindowTextA("Cancel");
	}//if	

	m_comboType.SetCurSel(m_nType);
	CreateListColumn();

	m_comboAction.AddString("Import");
	m_comboAction.AddString("Export");
	if(m_bImport)
	{
		m_comboAction.SetCurSel(0);
	}
	else
	{
		m_comboAction.SetCurSel(1);
	}//if

	if(m_bSilentImport)
	{
		RunSilentImport();
	}
	else if(m_bSilentExport)
	{
		RunSilentExport();
	}
	else
	{
		SetTimer(TIMER_LIST_INIT, 10, NULL);
	}//if

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 리스트 컨트롤의 컬럼을 만든다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::CreateListColumn()
{
	m_listHost.DeleteAllItems();
	m_listHost.DeleteAllColumns();

	CRect clsRect;
	m_listHost.GetClientRect(clsRect);
	switch(m_nType)
	{
	case E_IMPORT_PACKAGE:
		{
			if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
			{
				m_listHost.InsertColumn(0, pszListPackage[0], LVCFMT_LEFT, (clsRect.Width()/100)*17);
				m_listHost.InsertColumn(1, pszListPackage[1], LVCFMT_LEFT, (clsRect.Width()/100)*35);
				m_listHost.InsertColumn(2, pszListPackage[2], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(3, pszListPackage[3], LVCFMT_LEFT, (clsRect.Width()/100)*32);
			}
			else
			{
				m_listHost.InsertColumn(0, pszListPackageEng[0], LVCFMT_LEFT, (clsRect.Width()/100)*17);
				m_listHost.InsertColumn(1, pszListPackageEng[1], LVCFMT_LEFT, (clsRect.Width()/100)*35);
				m_listHost.InsertColumn(2, pszListPackageEng[2], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(3, pszListPackageEng[3], LVCFMT_LEFT, (clsRect.Width()/100)*32);
			}//if
		}
		break;
	case E_IMPORT_BPI:
		{
			if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
			{
				m_listHost.InsertColumn(0, pszListBPI[0], LVCFMT_LEFT, (clsRect.Width()/100)*17);
				m_listHost.InsertColumn(1, pszListBPI[1], LVCFMT_LEFT, (clsRect.Width()/100)*35);
				m_listHost.InsertColumn(2, pszListBPI[2], LVCFMT_LEFT, (clsRect.Width()/100)*20);
				m_listHost.InsertColumn(3, pszListBPI[3], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(4, pszListBPI[4], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(5, pszListBPI[5], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(6, pszListBPI[6], LVCFMT_LEFT, (clsRect.Width()/100)*27);
			}
			else
			{
				m_listHost.InsertColumn(0, pszListBPIEng[0], LVCFMT_LEFT, (clsRect.Width()/100)*17);
				m_listHost.InsertColumn(1, pszListBPIEng[1], LVCFMT_LEFT, (clsRect.Width()/100)*35);
				m_listHost.InsertColumn(2, pszListBPIEng[2], LVCFMT_LEFT, (clsRect.Width()/100)*20);
				m_listHost.InsertColumn(3, pszListBPIEng[3], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(4, pszListBPIEng[4], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(5, pszListBPIEng[5], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(6, pszListBPIEng[6], LVCFMT_LEFT, (clsRect.Width()/100)*27);
			}//if
		}
		break;
	case E_IMPORT_ANNOUNCE:
		{
			if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
			{
				m_listHost.InsertColumn(0, pszListAnnounce[0], LVCFMT_LEFT, (clsRect.Width()/100)*17);
				m_listHost.InsertColumn(1, pszListAnnounce[1], LVCFMT_LEFT, (clsRect.Width()/100)*35);
				m_listHost.InsertColumn(2, pszListAnnounce[2], LVCFMT_LEFT, (clsRect.Width()/100)*20);
				m_listHost.InsertColumn(3, pszListAnnounce[3], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(4, pszListAnnounce[4], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(5, pszListAnnounce[5], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(6, pszListAnnounce[6], LVCFMT_LEFT, (clsRect.Width()/100)*27);
			}
			else
			{
				m_listHost.InsertColumn(0, pszListAnnounceEng[0], LVCFMT_LEFT, (clsRect.Width()/100)*17);
				m_listHost.InsertColumn(1, pszListAnnounceEng[1], LVCFMT_LEFT, (clsRect.Width()/100)*35);
				m_listHost.InsertColumn(2, pszListAnnounceEng[2], LVCFMT_LEFT, (clsRect.Width()/100)*20);
				m_listHost.InsertColumn(3, pszListAnnounceEng[3], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(4, pszListAnnounceEng[4], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(5, pszListAnnounceEng[5], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(6, pszListAnnounceEng[6], LVCFMT_LEFT, (clsRect.Width()/100)*27);
			}//if
		}
		break;
	default:
		{
			if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
			{
				m_listHost.InsertColumn(0, pszListPackage[0], LVCFMT_LEFT, (clsRect.Width()/100)*17);
				m_listHost.InsertColumn(1, pszListPackage[1], LVCFMT_LEFT, (clsRect.Width()/100)*35);
				m_listHost.InsertColumn(2, pszListPackage[2], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(3, pszListPackage[3], LVCFMT_LEFT, (clsRect.Width()/100)*32);
			}
			else
			{
				m_listHost.InsertColumn(0, pszListPackageEng[0], LVCFMT_LEFT, (clsRect.Width()/100)*17);
				m_listHost.InsertColumn(1, pszListPackageEng[1], LVCFMT_LEFT, (clsRect.Width()/100)*35);
				m_listHost.InsertColumn(2, pszListPackageEng[2], LVCFMT_LEFT, (clsRect.Width()/100)*30);
				m_listHost.InsertColumn(3, pszListPackageEng[3], LVCFMT_LEFT, (clsRect.Width()/100)*32);
			}//if
		}
	}//switch

	m_listHost.SetExtendedStyle(LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
}


void CUBCImporterDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default
	CDialog::OnClose();
}

void CUBCImporterDlg::OnDestroy()
{
	ClearLocoalHostInfoArray();
	ClearBpiInfoArray();
	ClearAnnounceInfoArray();
	ClearCopyInfoArray();

	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CUBCImporterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCImporterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		//CDialog::OnPaint();

		//CPaintDC dc(this);

		////내부 컨트롤 다시 그리기
		//m_listHost.Invalidate();
		//m_btnOK.Invalidate();
		//m_btnCancel.Invalidate();

		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCImporterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CUBCImporterDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_nCheckCount == 0)
	{
		return;
	}//if

	if(m_bImport)		//Import
	{
		//프로그램이 실행된 경로로부터 import 하려는 대상 드라이브 문자열을 가져온다
		m_strTargetDrive = GetAppDrive();
	}
	else				//Export
	{
		if(m_strArgDrive.CompareNoCase("all") == 0)
		{
			//프로그램이 실행된 드라이브는 선택못하도록 해야 한다.
			CDriveSelectDialog dlg;
			dlg.SetExceptDrive(GetAppDrive());
			if(dlg.DoModal() != IDOK) return;

			m_strTargetDrive = dlg.m_strDrive;
		}
		else
		{
			m_strTargetDrive = m_strArgDrive;
		}//if
	}//if
/*
	//Source와 Target이 같다면 아무것도 하지 않는다.
	if(m_strSrcDrive.CompareNoCase(m_strTargetDrive) == 0)
	{
		AfxMessageBox("Select another drive !\r\nCan not proccess on source drive.");
		return;
	}//if
*/
	CString strExt, strType, strPath;
	switch(m_nType)
	{
	case E_IMPORT_PACKAGE:
		{
			strExt = "ini";
			if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
			{
				strType = pszType[0];
			}
			else
			{
				strType = pszTypeEng[0];
			}//if
			strPath = IMPORT_CONFIG_PATH;
		}
		break;
	case E_IMPORT_BPI:
		{
			strExt = "bpi";
			if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
			{
				strType = pszType[1];
			}
			else
			{
				strType = pszTypeEng[1];
			}//if
			strPath = IMPORT_CONFIG_PATH;
		}
		break;
	case E_IMPORT_ANNOUNCE:
		{
			strExt = "ini";
			if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
			{
				strType = pszType[2];
			}
			else
			{
				strType = pszTypeEng[2];
			}//if
			strPath = IMPORT_DATA_PATH;
		}
		break;
	default:
		{
			strExt = "ini";
			if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
			{
				strType = pszType[0];
			}
			else
			{
				strType = pszTypeEng[0];
			}//if
			strPath = IMPORT_CONFIG_PATH;
		}
	}//switch

	//체크된 리스트 아이템을 중복확인을 하고....
	CString strTargetPath, strSrcPath;
	m_arySrcPackage.RemoveAll();
	for(int i=0; i<m_listHost.GetItemCount(); i++)
	{
		if(!ListView_GetCheckState(m_listHost.GetSafeHwnd(), i))
		{
			continue;
		}//if

		if(m_nType == E_IMPORT_ANNOUNCE){
			strTargetPath.Format("%s%sannounce\\%s_local.%s", m_strTargetDrive, strPath, m_listHost.GetItemText(i, 1), strExt);
		}else{
			strTargetPath.Format("%s%s%s.%s", m_strTargetDrive, strPath, m_listHost.GetItemText(i, 1), strExt);
		}
		strSrcPath.Format("%s%s%s.%s", m_listHost.GetItemText(i, 0), strPath, m_listHost.GetItemText(i, 1), strExt);

		if(_access(strTargetPath, 0) == 0 )	//동일한 파일이 존재한다.
		{
			//같은 이름의 config 파일이 있다면 겹쳐 쓸것인지 확인한다.
			if(m_bMessage)
			{
				CString strMsg;
				if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
				{
					strMsg.Format("\"%s\" 은 존재하는 %s 입니다.\r\n덮어 쓰시겠습니까 ?", m_listHost.GetItemText(i, 1), strType);
				}
				else
				{
					strMsg.Format("\"%s\" is already exist %s.\r\nOverwrite that ?", m_listHost.GetItemText(i, 1), strType);
				}//if
				if(MessageBox(strMsg, "Duplicate", MB_OKCANCEL|MB_ICONQUESTION) == IDOK)
				{
					m_arySrcPackage.Add(strSrcPath);;
				}//if
			}//if
		}
		else
		{
			m_arySrcPackage.Add(strSrcPath);
		}//if
	}//for
	
	BeginWaitCursor();
	SetEnableUI(false);
	StopUBC();				//먼저 실행중인 UBC system 프로그램들을 전부 종료한다

	CWinThread* pThread = ::AfxBeginThread(CUBCImporterDlg::ThreadMakeCopyInfoList,
											this,
											THREAD_PRIORITY_NORMAL,
											0,
											CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;
	pThread->ResumeThread();

	ClearLocoalHostInfoArray();
	ClearBpiInfoArray();
	ClearAnnounceInfoArray();
}

void CUBCImporterDlg::OnBnClickedCancel()
{
	ClearLocoalHostInfoArray();
	ClearBpiInfoArray();
	ClearAnnounceInfoArray();
	ClearCopyInfoArray();

	OnCancel();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Host list를 list control에 설정 \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::InitList()
{
	BeginWaitCursor();
	SetEnableUI(false);

	ClearLocoalHostInfoArray();
	ClearBpiInfoArray();
	ClearAnnounceInfoArray();

	InitDriveLabel();
	CString strAppDrive = GetAppDrive();
	strAppDrive.MakeUpper();

	POSITION pos = m_mapDriveToLabel.GetStartPosition();
	if(pos != NULL)
	{
		CString strDrive, strLabel;
		UINT unType;
		while(pos != NULL)
		{
			m_mapDriveToLabel.GetNextAssoc(pos, strDrive, strLabel);
			unType = GetDriveType(strDrive);

			if(m_bImport)
			{
				//Import이면 프로그램을 실행시킨 드라이브와
				//다른 드라이브만 목록에 추가한다.
				if(m_strArgDrive.CompareNoCase("all") == 0)
				{
					if(strDrive == strAppDrive)
						continue;
				}
				else
				{
					if(unType != DRIVE_REMOVABLE)
						continue;
				}//if
			}
			else
			{
				//Export이면 프로그램을 실행시킨 드라이브와
				//같은 드라이브만 목록에 추가한다.
				if(strDrive != strAppDrive)
					continue;
			}//if

			if(strLabel == "")
			{
				switch(unType)
				{
				case DRIVE_REMOVABLE:
					strLabel = "Removable Disk";
					break;
				case DRIVE_FIXED:
					strLabel = "Local Disk";
					break;
				case DRIVE_REMOTE:
					strLabel = "Remote Disk";
					break;
				case DRIVE_CDROM:
					strLabel = "Optical Disk";
					break;
				case DRIVE_RAMDISK:
					strLabel = "RAM Disk";
					break;
				default:
					strLabel = "Unknown Disk";
					break;
				}//switch
			}//if

			CString strPath = strDrive;
			if(m_nType == E_IMPORT_ANNOUNCE)
			{
				strPath.Append(IMPORT_DATA_PATH);
			}
			else
			{
				strPath.Append(IMPORT_CONFIG_PATH);
			}//if

			switch(m_nType)
			{
			case E_IMPORT_PACKAGE:
				{
					GetHostInfo(strPath, strDrive);
				}
				break;
			case E_IMPORT_BPI:
				{
					GetBpiInfo(strPath, strDrive);
				}
				break;
			case E_IMPORT_ANNOUNCE:
				{
					GetAnnounceInfo(strPath, strDrive);
				}
				break;
			default:
				{
					GetHostInfo(strPath, strDrive);
				}
			}//switch
		}//while
	}//if

	m_listHost.DeleteAllItems();

	/*
	if(nCount == 0)
	{
		m_listHost.GetClientRect(&rc);
		m_listHost.ClientToScreen(&rc);
		ScreenToClient(&rc);
		InvalidateRect(&rc);
		return;
	}//if
	*/	
	
	int nCount, nIdx;
	switch(m_nType)
	{
	case E_IMPORT_PACKAGE:
		{
			nCount = m_aryLocalHostInfo.GetCount();
			CLocalHostInfo* pclsHost = NULL;
			for(int i=0; i<nCount; i++)
			{
				pclsHost = (CLocalHostInfo*)m_aryLocalHostInfo.GetAt(i);
				nIdx = m_listHost.InsertItem(i, pclsHost->m_strDrive, 0);
				m_listHost.SetItemText(nIdx, 1, pclsHost->m_strHostName);
				m_listHost.SetItemText(nIdx, 2, pclsHost->m_strLastWrittenTime);
				m_listHost.SetItemText(nIdx, 3, pclsHost->m_strDescription);
			}//for

			//가장 최근에 저장된 host 정보가 가장 상단으로 오도록 정렬한다
			m_listHost.SetColumnSorting(2, CListCtrlEx::Descending);
			m_listHost.SortOnColumn(2, TRUE);
		}
		break;
	case E_IMPORT_BPI:
		{
			nCount = m_aryBpiInfo.GetCount();
			CBpiInfo* pBpi = NULL;
			for(int i=0; i<nCount; i++)
			{
				pBpi = (CBpiInfo*)m_aryBpiInfo.GetAt(i);
				nIdx = m_listHost.InsertItem(i, pBpi->m_strDrive, 0);
				m_listHost.SetItemText(nIdx, 1, pBpi->m_strBpiName);
				m_listHost.SetItemText(nIdx, 2, pBpi->m_strSiteId);
				m_listHost.SetItemText(nIdx, 3, pBpi->m_strLastWrittenTime);
				m_listHost.SetItemText(nIdx, 4, pBpi->m_strStartDate);
				m_listHost.SetItemText(nIdx, 5, pBpi->m_strEndDate);
				m_listHost.SetItemText(nIdx, 6, pBpi->m_strDescription);
			}//for

			//가장 최근에 저장된 Bpi 정보가 가장 상단으로 오도록 정렬한다
			m_listHost.SetColumnSorting(3, CListCtrlEx::Descending);
			m_listHost.SortOnColumn(3, TRUE);
		}
		break;
	case E_IMPORT_ANNOUNCE:
		{
			nCount = m_aryAnnounceInfo.GetCount();
			CAnnounceInfo* pAnn = NULL;
			for(int i=0; i<nCount; i++)
			{
				pAnn = (CAnnounceInfo*)m_aryAnnounceInfo.GetAt(i);
				nIdx = m_listHost.InsertItem(i, pAnn->m_strDrive, 0);
				m_listHost.SetItemText(nIdx, 1, pAnn->m_strAnnounceName);
				m_listHost.SetItemText(nIdx, 2, pAnn->m_strSiteId);
				m_listHost.SetItemText(nIdx, 3, pAnn->m_strLastWrittenTime);
				m_listHost.SetItemText(nIdx, 4, pAnn->m_strStartDate);
				m_listHost.SetItemText(nIdx, 5, pAnn->m_strEndDate);
				m_listHost.SetItemText(nIdx, 6, pAnn->m_strDescription);
			}//for

			//가장 최근에 저장된 Bpi 정보가 가장 상단으로 오도록 정렬한다
			m_listHost.SetColumnSorting(3, CListCtrlEx::Descending);
			m_listHost.SortOnColumn(3, TRUE);
		}
		break;
	default:
		{
			nCount = m_aryLocalHostInfo.GetCount();
			CLocalHostInfo* pclsHost = NULL;
			for(int i=0; i<nCount; i++)
			{
				pclsHost = (CLocalHostInfo*)m_aryLocalHostInfo.GetAt(i);
				nIdx = m_listHost.InsertItem(i, pclsHost->m_strDrive, 0);
				m_listHost.SetItemText(nIdx, 1, pclsHost->m_strHostName);
				m_listHost.SetItemText(nIdx, 2, pclsHost->m_strLastWrittenTime);
				m_listHost.SetItemText(nIdx, 3, pclsHost->m_strDescription);
			}//for

			//가장 최근에 저장된 host 정보가 가장 상단으로 오도록 정렬한다
			m_listHost.SetColumnSorting(2, CListCtrlEx::Descending);
			m_listHost.SortOnColumn(2, TRUE);
		}
	}//switch

/*
	m_listHost.GetClientRect(&rc);
	m_listHost.ClientToScreen(&rc);
	ScreenToClient(&rc);
	InvalidateRect(&rc);
*/
	EndWaitCursor();
	SetEnableUI(true);
}


void CUBCImporterDlg::InitDriveLabel()
{
	m_mapVolumeToLabel.RemoveAll();
	m_mapDriveToLabel.RemoveAll();

	//
	GetProcessVolume();

	//
	CStringArray	drive_list;
	int nPos =0;
	char drive[] = "?:\\";
	DWORD dwDriveList=::GetLogicalDrives();
	while(dwDriveList)
	{
		if(dwDriveList & 1)
		{
			drive[0] = 'A' + nPos;
			drive_list.Add(drive);
		}
		dwDriveList >>=1;
		nPos++;
	}

	//
	char volume[255];
	for(int i=0; i<drive_list.GetCount(); i++)
	{
		CString drive = drive_list.GetAt(i);

		BOOL ret = GetVolumeNameForVolumeMountPoint(drive, volume, 255);

		if(ret)
		{
			CString label;
			if(m_mapVolumeToLabel.Lookup(volume, label))
				m_mapDriveToLabel.SetAt(drive, label);
		}
	}
}

void  CUBCImporterDlg::GetProcessVolume()
{
	char buf[MAX_PATH];           // buffer for unique volume identifiers
	HANDLE hVol;                  // handle for the volume scan
	BOOL bFlag;                   // generic results flag

	// Open a scan for volumes.
	hVol = FindFirstVolume (buf, MAX_PATH );

	if (hVol == INVALID_HANDLE_VALUE)
		return;

	// We have a volume; process it.
	bFlag = _GetProcessVolume (hVol, buf, MAX_PATH);

	// Do while we have volumes to process.
	while (bFlag) 
	{
		bFlag = _GetProcessVolume (hVol, buf, MAX_PATH);
	}

	// Close out the volume scan.
	bFlag = FindVolumeClose( hVol );
}

BOOL CUBCImporterDlg::_GetProcessVolume(HANDLE hVol, char* lpBuf, int iBufSize)
{
	DWORD dwSysFlags;            // flags that describe the file system
	char FileSysNameBuf[MAX_PATH];
	char labelname[MAX_PATH];

	BOOL ret_val = GetVolumeInformation( lpBuf, labelname, MAX_PATH, NULL, NULL, &dwSysFlags, FileSysNameBuf, MAX_PATH);

	if(ret_val)
	{
		m_mapVolumeToLabel.SetAt(lpBuf, labelname);
	}

	return FindNextVolume( hVol, lpBuf, iBufSize );
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// locla host info 배열을 정리한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::ClearLocoalHostInfoArray()
{
	if(m_aryLocalHostInfo.GetCount())
	{
		CLocalHostInfo* pclsHost = NULL;
		for(int i=0; i<m_aryLocalHostInfo.GetCount(); i++)
		{
			pclsHost = (CLocalHostInfo*)m_aryLocalHostInfo.GetAt(i);
			if(pclsHost)
			{
				delete pclsHost;
			}//if
		}//for
		m_aryLocalHostInfo.RemoveAll();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Bpi info 배열을 정리한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::ClearBpiInfoArray()
{
	if(m_aryBpiInfo.GetCount())
	{
		CBpiInfo* pclsBpi = NULL;
		for(int i=0; i<m_aryBpiInfo.GetCount(); i++)
		{
			pclsBpi = (CBpiInfo*)m_aryBpiInfo.GetAt(i);
			if(pclsBpi)
			{
				delete pclsBpi;
			}//if
		}//for
		m_aryBpiInfo.RemoveAll();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Announce info 배열을 정리한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::ClearAnnounceInfoArray()
{
	if(m_aryAnnounceInfo.GetCount())
	{
		CAnnounceInfo* pclsAnn = NULL;
		for(int i=0; i<m_aryAnnounceInfo.GetCount(); i++)
		{
			pclsAnn = (CAnnounceInfo*)m_aryAnnounceInfo.GetAt(i);
			if(pclsAnn)
			{
				delete pclsAnn;
			}//if
		}//for
		m_aryAnnounceInfo.RemoveAll();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 경로의 호스트 정보를 구한다 \n
/// @param (LPCSTR) lpszPath : (in) Config 파일이 있는 경로
/// @param (CString) strDrive : (in) Config 파일이 있는 드라이브 문자열
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCImporterDlg::GetHostInfo(LPCSTR lpszPath, CString strDrive)
{
	CString find_path = lpszPath;
	find_path.Append("\\*.ini");
	CString strHostName, strHostPath;
	CLocalHostInfo* pclsHost = NULL;
	CProfileManager mngProf;

	CFileFind clsFinder;
	BOOL bWorking = clsFinder.FindFile(find_path);
	while (bWorking)
	{
		bWorking = clsFinder.FindNextFile();

		if(clsFinder.IsDots())
		{
			continue;
		}//if

		if(clsFinder.IsDirectory() == FALSE)
		{

			//샘플파일과 preview 파일을 제외한 호스트 정보를 읽어서
			//배열에 넣어준다
			strHostName = clsFinder.GetFileTitle();
			
			if(strHostName.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				continue;
			}//if

			
			if(strHostName.Find(PREVIEW_FILE_KEY, 0) != -1)
			{
				continue;
			}//if
			
			//ini 파일에서 정보를 읽어온다
			strHostPath.Format("%s%s.ini", lpszPath, strHostName);
			if(!mngProf.Read(strHostPath))
			{
				continue;
			}//if
			
			pclsHost = new CLocalHostInfo;
			pclsHost->m_strDescription = mngProf.GetProfileString("Host", "description");
			//strNetworkUse = mngProf.GetProfileString("Host", "networkUse");
			//strSite = mngProf.GetProfileString("Host", "site");
			pclsHost->m_strLastWrittenTime = mngProf.GetProfileString("Host", "lastUpdateTime");
			//strWrittenID = mngProf.GetProfileString("Host", "lastUpdateId");

			//pclsHost = new CLocalHostInfo;
			//pclsHost->SetLocalHostInfo(strDrive, strNetworkUse, strHostName,
			//							strDesc, strSite, strWrittenTime, strWrittenID);

			pclsHost->m_strDrive = strDrive;
			pclsHost->m_strHostName = strHostName;

			m_aryLocalHostInfo.Add(pclsHost);
		}//if
	}//while

	clsFinder.Close();
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 경로의 Bpi 정보를 구한다 \n
/// @param (LPCSTR) lpszPath : (in) Bpi 파일이 있는 경로
/// @param (CString) strDrive : (in) Bpi 파일이 있는 드라이브 문자열
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCImporterDlg::GetBpiInfo(LPCSTR lpszPath, CString strDrive)
{
	CString find_path = lpszPath;
	find_path.Append("\\*.bpi");
	CString strFileName, strFilePath, strDesc, strWrittenTime;
	CBpiInfo* pclsBpi = NULL;
	CProfileManager mngProf;

	CFileFind clsFinder;
	BOOL bWorking = clsFinder.FindFile(find_path);
	while (bWorking)
	{
		bWorking = clsFinder.FindNextFile();

		if(clsFinder.IsDots())
		{
			continue;
		}//if

		if(clsFinder.IsDirectory() == FALSE)
		{
			strFileName = clsFinder.GetFileTitle();
			
			//ini 파일에서 정보를 읽어온다
			strFilePath.Format("%s%s.bpi", lpszPath, strFileName);
			if(!mngProf.Read(strFilePath))
			{
				continue;
			}//if
			
			pclsBpi = new CBpiInfo;
			pclsBpi->m_strDrive = strDrive;
			pclsBpi->m_strBpiName = strFileName;
			pclsBpi->m_strSiteId = mngProf.GetProfileString("BP", "siteId");
			pclsBpi->m_strLastWrittenTime = mngProf.GetProfileString("BP", "createTime");
			pclsBpi->m_strStartDate = mngProf.GetProfileString("BP", "startDate");
			pclsBpi->m_strEndDate = mngProf.GetProfileString("BP", "endDate");
			pclsBpi->m_strDescription = mngProf.GetProfileString("BP", "description");

			m_aryBpiInfo.Add(pclsBpi);
		}//if
	}//while

	clsFinder.Close();
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 경로의 긴급공지 정보를 구한다 \n
/// @param (LPCSTR) lpszPath : (in) 긴급공지 파일이 있는 경로
/// @param (CString) strDrive : (in) 긴급공지 파일이 있는 드라이브 문자열
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCImporterDlg::GetAnnounceInfo(LPCSTR lpszPath, CString strDrive)
{
	CString find_path = lpszPath;
	find_path.Append("\\");
	find_path.Append(m_strArgAnnounceName);
	CString strFileName, strFilePath, strDesc, strWrittenTime;
	CAnnounceInfo* pclsAnn = NULL;
	CProfileManager mngProf;

	CFileFind clsFinder;
	BOOL bWorking = clsFinder.FindFile(find_path);
	CTime tmModify;
	while (bWorking)
	{
		bWorking = clsFinder.FindNextFile();

		if(clsFinder.IsDots())
		{
			continue;
		}//if

		if(clsFinder.IsDirectory() == FALSE)
		{
			strFileName = clsFinder.GetFileTitle();
			
			//ini 파일에서 정보를 읽어온다
			strFilePath.Format("%s%s.ini", lpszPath, strFileName);
			if(!mngProf.Read(strFilePath))
			{
				continue;
			}//if

			clsFinder.GetLastWriteTime(tmModify);
			
			pclsAnn = new CAnnounceInfo;
			pclsAnn->m_strDrive = strDrive;
			pclsAnn->m_strAnnounceName = strFileName;
			pclsAnn->m_strSiteId = mngProf.GetProfileString("ANNOUNCE", "siteId");
			pclsAnn->m_strLastWrittenTime = tmModify.Format("%Y/%m/%d %H:%M:%S");

			CTime tmStart(_atoi64(mngProf.GetProfileString("ANNOUNCE", "startTime")));
			CTime tmEnd(_atoi64(mngProf.GetProfileString("ANNOUNCE", "endTime")));

			pclsAnn->m_strStartDate = tmStart.Format("%Y/%m/%d %H:%M:%S");
			pclsAnn->m_strEndDate = tmEnd.Format("%Y/%m/%d %H:%M:%S");
			pclsAnn->m_strDescription = mngProf.GetProfileString("ANNOUNCE", "comment1");

			m_aryAnnounceInfo.Add(pclsAnn);
		}//if
	}//while

	clsFinder.Close();
	return true;
}


LRESULT CUBCImporterDlg::OnCompleteFileCopy(WPARAM wParam, LPARAM lParam)
{
	if(m_bMessage && !m_bSilentImport)
	{
		CString strMsg, strImport, strResult, strType;
		if(m_bImport)
		{
			strImport = "import";
		}
		else
		{
			strImport = "export";
		}//if

		if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
		{
			if(lParam == E_COPY_FAIL)
			{
				strResult = "실패하였습니다.";
			}
			else if(lParam == E_COPY_SUCCESS)
			{
				strResult = "성공하였습니다.";
			}
			else
			{
				strResult = "취소되었습니다.";
			}//if

			switch(m_nType)
			{
			case E_IMPORT_PACKAGE:
				{
					strType = pszType[0];
				}
				break;
			case E_IMPORT_BPI:
				{
					strType = pszType[1];
				}
				break;
			case E_IMPORT_ANNOUNCE:
				{
					strType = pszType[2];
				}
				break;
			default:
				{
					strType = pszType[1];
				}
			}//switch

			strMsg.Format("%s의 %s 작업이 %s", strType, strImport, strResult);
		}
		else
		{
			if(lParam == E_COPY_FAIL)
			{
				strResult = "failed.";
			}
			else if(lParam == E_COPY_SUCCESS)
			{
				strResult = "success.";
			}
			else
			{
				strResult = "canceled.";
			}//if

			switch(m_nType)
			{
			case E_IMPORT_PACKAGE:
				{
					strType = pszTypeEng[0];
				}
				break;
			case E_IMPORT_BPI:
				{
					strType = pszTypeEng[1];
				}
				break;
			case E_IMPORT_ANNOUNCE:
				{
					strType = pszTypeEng[2];
				}
				break;
			default:
				{
					strType = pszTypeEng[0];
				}
			}//switch

			strMsg.Format("%s %s %s", strType, strImport, strResult);
		}//if

		//if(!this->m_bSilentExport && !this->m_bSilentImport){
		
		this->ShowWindow(SW_SHOW);	
		this->SetForegroundWindow();
		AfxMessageBox(strMsg);
		//}
	}//if

	SetEnableUI(true);
	EndWaitCursor();

/*
	//////////////////////////////////////////////////////////////////////////////////
	bool bSuccess;
	if(wParam == false && m_bMessage)
	{
		AfxMessageBox("Fail to load contents package !!!");
	}
	else
	{
		if(m_bImport)
		{
			//Property 파일의 내용을 변경하고 Starter를 구동 시킨다.
			if(!WriteProperty())
			{
				if(m_bMessage)
				{
					AfxMessageBox("Fail to write property file");
				}//if
			}
			else
			{
				CString strCmd, strStarterPath;
				UINT nRet;
				strStarterPath.Format("%s%s%s +log +ignore_env", m_strTargetDrive, EXCUTE_PATH, EXCUTE_FILE);

				nRet = WinExec(strStarterPath, SW_NORMAL);
				if(nRet < 31)
				{
					if(m_bMessage)
					{
						AfxMessageBox("Fail to running UBC Starter !!!");
					}//if
				}
				else
				{
					bSuccess = true;
				}//if

				scratchUtil::getInstance()->socketAgent("127.0.0.1",14007,"refresh","");
			}//if
		}
		else
		{
			if(m_bMessage)
			{
				AfxMessageBox("Contents package export complete !!!");
			}//if
			bSuccess = true;
		}//if
	}//if
*/

	/*
	// Import 및 Export가 성공한경우 USB드라이브를 자동으로 제거하고 안내 메세지를 출력하도록 함.
	if(bSuccess)
	{
		CString strDrive;
		if(m_bImport) strDrive = m_strSrcDrive[0];
		else          strDrive = m_strTargetDrive[0];

		if(!strDrive.IsEmpty() && GetDriveType(strDrive) == DRIVE_REMOVABLE)
		{
			CEjectRemovableDrive::GetInstance()->EjectDrive(strDrive.GetAt(0));

			// USB 메모리 장치를 빼도 됩니다
			if(m_bMessage) AfxMessageBox(_T("Please remove the USB memory device"));
		}
	}
*/

	ClearLocoalHostInfoArray();
	ClearBpiInfoArray();
	ClearAnnounceInfoArray();

	OnOK();

	return 0;
}


int CUBCImporterDlg::DeleteFolder(LPCSTR lpszTargetPath)
{
	char path[MAX_PATH];
	::ZeroMemory(path, sizeof(path));
	strcpy(path, lpszTargetPath);

	SHFILEOPSTRUCT FileOp;//쉘 구조체 

	FileOp.hwnd = NULL;
	FileOp.wFunc = FO_DELETE;//지우고
	FileOp.pFrom = path;//인자로 받은 폴더
	FileOp.pTo = NULL;//복사할때 사용
	FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI ;//화면에 메세지 표시 않함
	FileOp.fAnyOperationsAborted = false;//나머진 뭔지 모르겠네요
	FileOp.hNameMappings = NULL;
	FileOp.lpszProgressTitle = NULL;

	return SHFileOperation(&FileOp);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 변경된 host 정보를 property 파일에 기록하여 Start가 brw를 구종 시킬 수 있도록 한다 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCImporterDlg::WriteProperty()
{
	//한번에 여러개의 패키지를 import하였다면 프로퍼티를 바꾸지 않는다.
	if(m_arySrcPackage.GetCount() != 1)
	{
		return true;
	}//if

	for(int i = 0; i < 4; i++)
	{
		if(!WriteBrowserProperty(i))
		{
			return false;
		}//if
	}//for
	
	return true;
}

bool CUBCImporterDlg::WriteBrowserProperty(int nNo)
{
	if(nNo > 3)
	{
		return false;
	}//if

	int nPos;
	CString szBrowser, szProperty, szArg, szBuff, szTemp;
	int nMonitor = scratchUtil::getInstance()->getMonitorCount();

	if(nNo == 0)
	{
		szBrowser.Format("STARTER.APP_BROWSER");
	}
	else
	{
		szBrowser.Format("STARTER.APP_BROWSER%d", nNo);
	}//if

	szProperty = szBrowser + ".AUTO_STARTUP";
	ciString szBuffbuf;
	if(!ubcConfig::lvc_getProperty(szProperty, szBuffbuf))
	{
		return false;
	}//if
	szBuff = szBuffbuf.c_str();


	// 모니터 갯수보다 높은수 이면 사용안하게 함.
	if(nMonitor <= nNo)
	{
		ubcConfig::lvc_setProperty(szProperty, "false");
		return true;
	}//if

	switch(nNo)
	{
	case 0:// 0번을 설정하지 않을때는 기존과 동일하게
		if(!m_nDisplayA) return true;
		break;
	case 1:// 1번을 설정하지 않을때는 기존과 동일하게
		if(!m_nDisplayB) return true;
		break;
	default://나머지는 사용안함.
		ubcConfig::lvc_setProperty(szProperty, "false");
		return true;
	}//switch

	szProperty = szBrowser + ".ARGUMENT";
	szBuffbuf="";
	if(!ubcConfig::lvc_getProperty(szProperty, szBuffbuf))
	{
		return false;
	}//if
	szBuff = szBuffbuf.c_str();

	szTemp = szBrowser + ".AUTO_STARTUP";
	ubcConfig::lvc_setProperty(szTemp, "true");

	//먼저 +host 옵션이 있는지 보고 없다면 바로 뒤에 새로 만들어 붙여준다
	if(szBuff.Find("+host", 0) == -1)
	{
		szArg = szBuff;
		szArg.Append(" +host ");
		szArg.Append(m_arySrcPackage.GetAt(0));
	}
	else
	{
		//있다면 host 명을 변경
		szArg = "";
		nPos = 0;
		szTemp = szBuff.Tokenize(" \t", nPos);

		while(szTemp != "")
		{
			if(szTemp == "+host")
			{
				szTemp = szBuff.Tokenize(" \t", nPos);
				szArg += "+host ";
				szArg += m_arySrcPackage.GetAt(0);
				szArg += " ";
			}
			else
			{
				szArg += szTemp;
				szArg += " ";
			}//if

			szTemp = szBuff.Tokenize(" \t", nPos);
		}//while
	}//if

	//+local 옵션을 앞에 붙여준다
	int nIdx = szArg.Find("+local", 0);
	if(nIdx != -1)
	{
		szArg.Delete(nIdx, 6);
	}//if
	
	szArg.Insert(0, "+local ");

	if(!ubcConfig::lvc_setProperty(szProperty, szArg))
	{
		return false;
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 실행인자 설정 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCImporterDlg::SetArgument()
{
/********************************************************************
입력되는 실행인자들

+self							: 라이센스가 있는 단말의 스튜디오에서 패키지 내보내기 하는 경우
+drive    [드라이브 (c:\)]		: 소스 패키지가 있는 드라이브 명(+all은 모든 드라이브에있는 패키지 파일을 보여준다)
+ini      [컨텐츠패키지명]		: 소스 패키지 명
+display  [0|01|1]				: (0 -> 1면), (01 -> 양면), (1 -> 2면)
+bpi      [방송계획명]			: 방송계획파일 명
+Announce [긴급공지명]			: 긴급공지파일 명
*********************************************************************/
	__DEBUG__("SetArgument()");

	ciString str;
	ciArgParser* pParser = ciArgParser::getInstance();
	CString strArg = theApp.m_lpCmdLine;
	CString szMessage;

	/////////////////////////////////////////////////
	// 출력 될 디스플레이
	if(pParser->isSet("+display"))
	{
		pParser->getArgValue("+display", str);
		if(str.length() > 1)	//양면
		{
			m_nDisplayA = 1;
			m_nDisplayB = 1;
		}
		else if(str == "0")		//1면
		{
			m_nDisplayA = 1;
		}
		else if(str == "1")		//2면
		{
			m_nDisplayB = 1;
		}//if
	}//if
	
	if(0 == m_nDisplayA && 0 == m_nDisplayB)
	{
		m_nDisplayA = 1;
	}//if


	//////////////////////////////////////////////////
	// no message 이면 실행할때는 경고창을 안띄운다.
	if(pParser->isSet("+nomsg"))
	{
		m_bMessage = false;
	}
	else
	{
		m_bMessage = true;
	}//if


	/////////////////////////////////////////////////
	// Import/Export 여부
	if(pParser->isSet("+export"))
	{
		m_bImport = false;
	}
	else
	{
		m_bImport = true;
	}//if

	/////////////////////////////////////////////////
	// +bpi
	if(pParser->isSet("+bpi"))		// 매니져에서 방송계획 Export
	{
		pParser->getArgValue("+drive", str);
		m_strArgDrive = str.c_str();
		if(m_strArgDrive.GetLength() == 1)
		{
			m_strArgDrive.Append(":\\");
		}
		else if(m_strArgDrive.GetLength() != 3)
		{
			szMessage = _T("Error : Invalid +drive argument.\n\nMust have drive letter and \":\\\" !");
			szMessage += _T("\n") + strArg;
			AfxMessageBox(szMessage);

			return false;
		}//if

		m_nType = E_IMPORT_BPI;
		pParser->getArgValue("+bpi", str);
		m_strArgBpiName = str.c_str();
		if(m_strArgBpiName.GetLength() > 4)
		{
			CString strExt = m_strArgBpiName.Right(4);
			if(strExt.CompareNoCase(".bpi") != 0)
			{
				m_strArgBpiName.Append(".bpi");
			}//if

			m_bSilentExport = true;
		}//if
		return true;
	}//if

	/////////////////////////////////////////////////
	// +Announce
	if(pParser->isSet("+announce_exist")) // USB 에서 단말에 긴급공지 Import
	{		
		m_announce_exist = true;
	}
	if(pParser->isSet("+announce"))		// 스튜디오에서 긴급공지 Export
	{
		pParser->getArgValue("+drive", str);
		m_strArgDrive = str.c_str();
		if(m_strArgDrive.GetLength() == 1)
		{
			m_strArgDrive.Append(":\\");
		}
		/*
		else if(m_strArgDrive.GetLength() != 3)
		{
			szMessage = _T("Error : Invalid +drive argument.\n\nMust have drive letter and \":\\\" !");
			szMessage += _T("\n") + strArg;
			AfxMessageBox(szMessage);

			return false;
		}//if
		*/

		m_nType = E_IMPORT_ANNOUNCE;
		m_bSilentExport = true;
		m_strArgAnnounceName = "UBCAnnounce.ini";
		
		return true;
	}//if

	/////////////////////////////////////////////////
	// +drive
	if(!pParser->isSet("+drive"))
	{
		szMessage = _T("Error : Unknown drive.\n\nMust have drive info !");
		szMessage += _T("\n") + strArg;
		AfxMessageBox(szMessage);
		return false;
	}//if	

	/////////////////////////////////////////////////
	// +self
	if(pParser->isSet("+self"))		// License 가 존재하는 PC 의 Studio 가 실행
	{
		/////////////////////////////////////////////////
		// +ini
		if(!pParser->isSet("+ini"))
		{
			szMessage = _T("Error : Unknown package file.\n\nMust have package file info !");
			szMessage += _T("\n") + strArg;
			AfxMessageBox(szMessage);
			return false;
		}//if

		pParser->getArgValue("+drive", str);
		m_strArgDrive = str.c_str();
		if(m_strArgDrive.GetLength() == 1)
		{
			m_strArgDrive.Append(":\\");
		}
		else if(m_strArgDrive.GetLength() != 3)
		{
			szMessage = _T("Error : Invalid +drive argument.\n\nMust have drive letter and \":\\\" !");
			szMessage += _T("\n") + strArg;
			AfxMessageBox(szMessage);

			return false;
		}//if

		m_bSilentImport = true;
		pParser->getArgValue("+ini", str);
		m_strArgPackageName = str.c_str();
		if(m_strArgPackageName.GetLength() > 4)
		{
			CString strExt = m_strArgPackageName.Right(4);
			if(strExt.CompareNoCase(".ini") != 0)
			{
				m_strArgPackageName.Append(".ini");
			}//if
		}//if
		return true;
	}//if

	//////////////////////////////////////////////////
	if(pParser->isSet("+drive"))
	{
		pParser->getArgValue("+drive", str);
		m_strArgDrive = str.c_str();
		if(m_strArgDrive.CompareNoCase("all") == 0)
		{
			return true;
		}//if

		if(m_strArgDrive.GetLength() == 1)
		{
			m_strArgDrive.Append(":\\");
		}
		else if(m_strArgDrive.GetLength() != 3)
		{
			szMessage = _T("Error : Invalid +drive argument.\n\nMust have drive letter and \":\\\" !");
			szMessage += _T("\n") + strArg;
			AfxMessageBox(szMessage);

			return false;
		}//if
		
		m_bSilentImport = true;

		/////////////////////////////////////////////////
		// +ini
		if(!pParser->isSet("+ini"))
		{
			szMessage = _T("Error : Unknown package file.\n\nMust have package file info !");
			szMessage += _T("\n") + strArg;
			AfxMessageBox(szMessage);
			return false;
		}//if

		pParser->getArgValue("+ini", str);
		m_strArgPackageName = str.c_str();
		if(m_strArgPackageName.GetLength() > 4)
		{
			CString strExt = m_strArgPackageName.Right(4);
			if(strExt.CompareNoCase(".ini") != 0)
			{
				m_strArgPackageName.Append(".ini");
			}//if
		}//if
		return true;
	}//if
	__DEBUG__("SilentImport=%d, ini=%s, m_announce_exist=%d", m_bSilentImport, m_strArgPackageName, m_announce_exist);

	szMessage = _T("Error : Unknown argument info !");
	szMessage += _T("\n") + strArg;
	AfxMessageBox(szMessage);
	//ciArgParser::terminate();
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 매니져를 통한 방송계획 Export를 수행한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::RunSilentExport()
{
	this->ShowWindow(SW_HIDE);

	if(!m_bSilentExport)
	{
		PostMessage(WM_CLOSE, 0, 0);
		return;
	}//if

	//프로그램이 실행된 드라이브는 선택못하도록 해야 한다.
	CDriveSelectDialog dlg;
	dlg.SetExceptDrive(GetAppDrive());
	if(dlg.DoModal() != IDOK) return;

	m_strTargetDrive = dlg.m_strDrive;

	CString strSrcPath;	
	if(m_nType == E_IMPORT_BPI)
	{
		strSrcPath.Format("%s%s%s", GetAppDrive(), IMPORT_CONFIG_PATH, m_strArgBpiName);
	}
	else
	{
		strSrcPath.Format("%s%s%s", GetAppDrive(), IMPORT_DATA_PATH, m_strArgAnnounceName);
	}//if
	m_arySrcPackage.RemoveAll();
	m_arySrcPackage.Add(strSrcPath);
	
	SetEnableUI(false);
	StopUBC();				//먼저 실행중인 UBC system 프로그램들을 전부 종료한다

	CWinThread* pThread = ::AfxBeginThread(CUBCImporterDlg::ThreadMakeCopyInfoList,
											this,
											THREAD_PRIORITY_NORMAL,
											0,
											CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;
	pThread->ResumeThread();

	ClearLocoalHostInfoArray();
	ClearBpiInfoArray();
	ClearAnnounceInfoArray();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UI를 숨기고 import 기능을 실행 \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::RunSilentImport()
{
	__DEBUG__("RunSilentImport()");
	
	this->ShowWindow(SW_HIDE);

	if(!m_bSilentImport || m_strArgPackageName == "")
	{
		PostMessage(WM_CLOSE, 0, 0);
		return;
	}//if

	m_bImport = true;
	m_strTargetDrive = GetAppDrive();	//프로그램이 실행된 경로로부터 import 하려는 대상 드라이브 문자열을 가져온다
	m_arySrcPackage.RemoveAll();


	if(m_strArgPackageName.IsEmpty() || m_strArgPackageName == "NONE"){
		// package 가 명시되지 않았으므로 announce_only case 이다.
		m_nType = E_IMPORT_ANNOUNCE;
	}else{
		CString strSrcPath;
		strSrcPath.Format("%s%s%s", m_strArgDrive, IMPORT_CONFIG_PATH, m_strArgPackageName);
		m_arySrcPackage.Add(strSrcPath);
	}

	if(m_announce_exist){
		CString strSrcPath;
		strSrcPath.Format("%s%s%s", m_strArgDrive, IMPORT_DATA_PATH, "UBCAnnounce.ini");
		m_arySrcPackage.Add(strSrcPath);
	}



	SetEnableUI(false);
	StopUBC();				//먼저 실행중인 UBC system 프로그램들을 전부 종료한다

	CWinThread* pThread = ::AfxBeginThread(CUBCImporterDlg::ThreadMakeCopyInfoList,
											this,
											THREAD_PRIORITY_NORMAL,
											0,
											CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;
	pThread->ResumeThread();

	ClearLocoalHostInfoArray();
	ClearBpiInfoArray();
	ClearAnnounceInfoArray();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 복사할 파일정보 배열을 만든다. \n
/// @return <형: bool> \n
///			<true: 다운로드할 파일이 있음> \n
///			<false: 다운로드할 파일이 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCImporterDlg::MakeCopyInfoList()
{
	__DEBUG__("MakeCopyInfoList()");
	CString strContentsList, strETCList;
	CCopyInfo* pCopy = NULL;

	ClearCopyInfoArray();
	m_paryCopyInfo = new CCopyInfoArray();

	int nPos;
	CProfileManager mngProf;
	CString strFileName, token, strPackageName, strPath;
	ULONGLONG ulVolume = 0;
	char szDrive[MAX_PATH] = { 0x00 };
	char szName[MAX_PATH] = { 0x00 };
	char szExt[MAX_PATH] = { 0x00 };
	CFileStatus fs;

	for(int i=0; i<m_arySrcPackage.GetCount(); i++)
	{
		strPath = m_arySrcPackage.GetAt(i);
		__DEBUG__("MakeCopyInfoList(%s)", strPath);
		//패키지파일 명
		_splitpath(strPath, szDrive, NULL, szName, szExt);
		strFileName.Format("%s%s", szName, szExt);
		strPackageName = szName;

		if(!mngProf.Read(strPath))
		{
			if(m_bMessage)
			{
				CString str;
				if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
				{
					str.Format(_T("\"%s\" 패키지파일을 찾을 수 없습니다.\r\n\"%s\" 패키지의 모든 컨텐츠 파일은 복사되지 않습니다."), szName, szName);
				}
				else
				{
					str.Format(_T("Can not found \"%s\" contents package file.\r\n\"%s\"'s contents file are not copied."), szName, szName);
				}//if
				AfxMessageBox(str);
				__DEBUG__(str);

			}//if
			__DEBUG__("MakeCopyInfoList(%s) read failed", strPath);
			continue;
		}//if

		CFile::GetStatus(strPath, fs);

		//패키지파일추가
		pCopy = new CCopyInfo();
		pCopy->m_strPackageName = strPackageName;
		pCopy->m_strFileName = strFileName;
		pCopy->m_ulFileSize = fs.m_size;
		if(strcmp(szExt, ".bpi") == 0)
		{
			pCopy->m_nContentType = CONTENTS_BPI;
		}
		else
		{
			pCopy->m_nContentType = CONTENTS_PACKAGE;
		}//if
		pCopy->m_strLocation = IMPORT_CONFIG_PATH;
		pCopy->m_strSrcDrive = szDrive;
		pCopy->m_strTargetDrive = m_strTargetDrive;

		m_paryCopyInfo->Add(pCopy);

		strContentsList = mngProf.GetProfileString(_T("Host"), _T("ContentsList"));

		//__DEBUG__("ContentsList=%s", strContentsList);


		nPos = 0;
		token = strContentsList.Tokenize(_T(","), nPos);
		while(token != _T(""))
		{
			token.Trim();

			strFileName = mngProf.GetProfileString(token, _T("filename"), _T(""));
			ulVolume = (ULONGLONG)_atoi64(mngProf.GetProfileString(token, _T("volume"), _T("")));

			if(strFileName.GetLength() != 0 && ulVolume != 0)
			{
				pCopy = new CCopyInfo();
				pCopy->m_strPackageName = strPackageName;
				pCopy->m_strFileName = strFileName;
				pCopy->m_ulFileSize = ulVolume;
				pCopy->m_nContentType = atoi(mngProf.GetProfileString(token, _T("contentsType")));
				pCopy->m_strLocation = mngProf.GetProfileString(token, _T("location"));
				pCopy->m_strSrcDrive = szDrive;
				pCopy->m_strTargetDrive = m_strTargetDrive;

				__DEBUG__("Contents=%s", pCopy->m_strFileName);
				m_paryCopyInfo->Add(pCopy);
			}//if

			token = strContentsList.Tokenize(_T(","), nPos);
		}//while

		//ETCList
		strETCList = mngProf.GetProfileString(_T("Host"), _T("EtcList"));
		nPos = 0;
		token = strETCList.Tokenize(_T(","), nPos);
		while(token != _T(""))
		{
			token.Trim();

			strFileName = mngProf.GetProfileString(token, _T("filename"));
			ulVolume = (ULONGLONG)_atoi64(mngProf.GetProfileString(token, _T("volume"), _T("")));

			if(strFileName.GetLength() != 0 && ulVolume != 0)
			{
				pCopy = new CCopyInfo();
				pCopy->m_strPackageName = strPackageName;
				pCopy->m_strFileName = strFileName;
				pCopy->m_ulFileSize = ulVolume;
				pCopy->m_nContentType = atoi(mngProf.GetProfileString(token, _T("contentsType")));
				pCopy->m_strLocation = mngProf.GetProfileString(token, _T("location"));
				pCopy->m_strSrcDrive = szDrive;
				pCopy->m_strTargetDrive = m_strTargetDrive;

				m_paryCopyInfo->Add(pCopy);
			}//if

			token = strETCList.Tokenize(_T(","), nPos);
		}//while
	}//for

	if(m_paryCopyInfo->GetCount() == 0)
	{
		__DEBUG__("MakeCopyInfoList exit(false)");
		return false;
	}//if
	__DEBUG__("MakeCopyInfoList exit(true)");
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 복사할 파일정보 배열을 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::ClearCopyInfoArray()
{
	if(m_paryCopyInfo)
	{
		delete m_paryCopyInfo;
		m_paryCopyInfo = NULL;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UI의 Enable 상태를 설정 \n
/// @param (bool) bEnable : (in)  Enable 상태
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::SetEnableUI(bool bEnable)
{
	m_comboType.EnableWindow(bEnable);
	m_comboAction.EnableWindow(bEnable);
	m_btnRefresh.EnableWindow(bEnable);
	m_btnOK.EnableWindow(bEnable);
	m_btnCancel.EnableWindow(bEnable);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 복사할 파일의 정보배열을 만드는 thread \n
/// @param (LPVOID) pParam : (in) 부모 포인터
/// @return <형: UINT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CUBCImporterDlg::ThreadMakeCopyInfoList(LPVOID pParam)
{
	CUBCImporterDlg* pParent = (CUBCImporterDlg*)pParam;

	__DEBUG__("ThreadMakeCopyInfoList(m_nType=%d)", pParent->m_nType);


	if(pParent->m_nType == E_IMPORT_ANNOUNCE || pParent->m_announce_exist)
	{
		if(pParent->CopyAnnounce())
		{
			if(pParent->m_nType == E_IMPORT_ANNOUNCE) {
				pParent->PostMessage(WM_COMPLETE_FILE_COPY, 0, E_COPY_SUCCESS);
			}
			__DEBUG__("CopyAnnounce() succeed");
		}
		else
		{
			if(pParent->m_nType == E_IMPORT_ANNOUNCE) {
				pParent->PostMessage(WM_COMPLETE_FILE_COPY, 0, E_COPY_FAIL);
			}
			__DEBUG__("CopyAnnounce() failed");
		}//if

		if(pParent->m_nType == E_IMPORT_ANNOUNCE){
			if(pParent->m_announce_exist){
				StartUBC();  // ubc를 다시 띄워준다.
			}
			return 0;
		}
	}
	
	if(pParent->m_nType == E_IMPORT_BPI)
	{
		pParent->ParseBpiFile();
	}//if

	__DEBUG__("ThreadMakeCopyInfoList(m_nType=%d)", pParent->m_nType);

	if(pParent->MakeCopyInfoList())
	{
		pParent->CopyPackageFiles();
		//pParent->PostMessage(WM_COMPLETE_FILE_COPY, (WPARAM)true, 0);
	}
	else
	{
		pParent->PostMessage(WM_COMPLETE_FILE_COPY, (WPARAM)E_COPY_FAIL, 0);
	}//if	
	
	return 0;
}


LRESULT CUBCImporterDlg::OnCheckedCount(WPARAM wParam, LPARAM lParam)
{
	m_nCheckCount = (int)lParam;

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Package파일들을 복사한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::CopyPackageFiles()
{
	__DEBUG__("CopyPackageFiles()");
	CFileCopyDlg dlg(m_paryCopyInfo, this);
	dlg.DoModal();
	__DEBUG__("CopyPackageFiles() end");
}

void CUBCImporterDlg::OnBnClickedSearchBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nSel = m_comboType.GetCurSel();
	m_nType = nSel;
	
	CreateListColumn();

	nSel = m_comboAction.GetCurSel();
	if(nSel == 0)
	{
		m_bImport = true;
	}
	else
	{
		m_bImport = false;
	}//if

	InitList();
}


void CUBCImporterDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == TIMER_LIST_INIT)
	{
		KillTimer(TIMER_LIST_INIT);
		InitList();
	}//if

	CDialog::OnTimer(nIDEvent);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 방송계획파일을 분석하여 복사할 파일 리스트를 만든다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCImporterDlg::ParseBpiFile()
{
	CString strPath, strTPList, strPackage, token;
	CProfileManager mngProf;
	CStringArray aryTmp;
	char szDrive[MAX_PATH] = { 0x00 };
	char szPath[MAX_PATH] = { 0x00 };
	char szName[MAX_PATH] = { 0x00 };
	int nPos;

	for(int i=0; i<m_arySrcPackage.GetCount(); i++)
	{
		strPath = m_arySrcPackage.GetAt(i);

		_splitpath(strPath, szDrive, szPath, szName, NULL);

		if(!mngProf.Read(strPath))
		{
			if(m_bMessage)
			{
				CString str;
				if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
				{
					str.Format(_T("\"%s\" 방송계획파일을 찾을 수 없습니다.\r\n\"%s\" 방송계획의 모든 컨텐츠 파일은 복사되지 않습니다."), szName, szName);
				}
				else
				{
					str.Format(_T("Can not found \"%s\" broadcast plan file.\r\n\"%s\"'s contents file are not copied."), szName, szName);
				}//if
				AfxMessageBox(str);
			}//if
			continue;
		}//if

		//방송계획파일 추가
		aryTmp.Add(strPath);

		//패키지 추가
		strTPList = mngProf.GetProfileString("BP", "TpList");

		nPos = 0;
		token = strTPList.Tokenize(_T(","), nPos);
		while(token != _T(""))
		{
			token.Trim();

			strPackage = szDrive;
			strPackage += szPath;
			strPackage += mngProf.GetProfileString(token, _T("programId"), _T(""));
			strPackage += ".ini";
			
			aryTmp.Add(strPackage);

			token = strTPList.Tokenize(_T(","), nPos);
		}//while
	}//for

	//m_arySrcPackage를 비우고 생성한 aryTmp를 할당...
	m_arySrcPackage.RemoveAll();
	m_arySrcPackage.Copy(aryTmp);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 긴급공지 파일을 복사한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCImporterDlg::CopyAnnounce()
{
	__DEBUG__("CopyAnnounce()");

	char szDrive[MAX_PATH] = { 0x00 };
	char szPath[MAX_PATH] = { 0x00 };
	char szName[MAX_PATH] = { 0x00 };
	char szExt[MAX_PATH] = { 0x00 };
	CFileStatus fs;
	CString strTargetPath, strPath;
	bool bRet = true;

	CStringArray tempArray;

	for(int i=0;i<m_arySrcPackage.GetCount(); i++)
	{
		strPath = m_arySrcPackage.GetAt(i);
		__DEBUG__("CopyAnnounce(%s)", strPath);
	
		_splitpath(strPath, szDrive, szPath, szName, szExt);

		if(strcmp(szName,"UBCAnnounce") != 0){
			__DEBUG__("not announce (%s)", strPath);
			tempArray.Add(strPath);
			continue;
		}


		strTargetPath.Format("%s%sannounce\\%s_local%s", m_strTargetDrive, szPath, szName, szExt);
		//strTargetPath.Format("%s%s%s%s", m_strTargetDrive, szPath, szName, szExt);
		//복사해야하는 경로 만들어주기
		//경로에 "/"를 "\\"로 바꿔야 한다.
		strTargetPath.Replace(_T("/"), _T("\\"));
		::MakeSureDirectoryPathExists(strTargetPath);

		if(CFile::GetStatus(strPath, fs) == FALSE)
		{
			if(m_bMessage)
			{
				CString str;
				if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
				{
					str.Format(_T("\"%s\" 긴급공지 파일을 찾을 수 없습니다.\r\n"), szName);
				}
				else
				{
					str.Format(_T("Can not fount \"%s\" announcement file.\r\n"), szName);
				}//if
				AfxMessageBox(str);
				__DEBUG__(str);
			}//if
			continue;
		}//if

		if(!CopyFile(strPath, strTargetPath, FALSE))
		{
			bRet = false;
			if(m_bMessage)
			{
				CString str;
				if(PRIMARYLANGID(GetSystemDefaultLangID()) == LANG_KOREAN)
				{
					str.Format(_T("\"%s\" 긴급공지 파일을 복사하지 못했습니다.\r\n"), szName);
				}
				else
				{
					str.Format(_T("Fail to copy \"%s\" announcement file.\r\n"), szName);
				}//if
				AfxMessageBox(str);
				__DEBUG__(str);
			}//if
		}else{
			__DEBUG__("%s-->%s file copy succeed", strPath, strTargetPath);
		}
	}//for
	m_arySrcPackage.RemoveAll();
	m_arySrcPackage.Copy(tempArray);
	__DEBUG__("CopyAnnounce exit(%d)", bRet);
	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소스의 모든 파일을 타겟의 위치로 복사한다. \n
/// @param (CString) strSrc : (in) 원본파일들이 있는 경로
/// @param (CString) strTarget : (in) 파일들을 복사하려는 대상 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCImporterDlg::CopyFiles(CString strSrc, CString strTarget)
{
	//SHFILEOPSTRUCT의 경로에 끝에는 NULL이 두개 들어가므로...
	TCHAR szSrc[MAX_PATH+1] = { 0x00 };
	TCHAR szTarget[MAX_PATH+1] = { 0x00 };
	strcpy(szSrc, strSrc);
	strcpy(szTarget, strTarget);

	SHFILEOPSTRUCT stFileOps;
	memset(&stFileOps, 0x00, sizeof(SHFILEOPSTRUCT));
	stFileOps.hwnd = this->m_hWnd;
	stFileOps.wFunc = FO_COPY;
	stFileOps.pFrom = szSrc;
	stFileOps.pTo = szTarget;
	stFileOps.fFlags = FOF_NOCONFIRMATION | FOF_NOCONFIRMMKDIR | FOF_SIMPLEPROGRESS;
	stFileOps.fAnyOperationsAborted = FALSE;

	TRACE("Copy files : %s ==> %s\r\n", szSrc, szTarget);
	if(SHFileOperation(&stFileOps) == 0)
	{
		TRACE("Success copy files : %s ==> %s\r\n", szSrc, szTarget);
		return true;
	}//if

	TRACE("Fail copy files : %s ==> %s\r\n", szSrc, szTarget);

	return false;
}
