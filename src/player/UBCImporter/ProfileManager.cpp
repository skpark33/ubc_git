#include "StdAfx.h"
#include "ProfileManager.h"
#include <IMAGEHLP.H>
#pragma comment(lib, "imagehlp.lib")

CProfileManager::CProfileManager(LPCTSTR lpszFilename)
:	m_strFilename (lpszFilename)
{
	Read();
}

CProfileManager::CProfileManager()
{
}

CProfileManager::~CProfileManager(void)
{
	m_strFilename = _T("");

	ClearInstance();
}

void CProfileManager::ClearInstance()
{
	POSITION pos;
	CString key;
	CMapStringToString* group;

	for( pos = m_mapGroup.GetStartPosition(); pos != NULL; )
	{
		m_mapGroup.GetNextAssoc( pos, key, (void*&)group );

		delete group;
	}

	m_mapGroup.RemoveAll();
}

LPTSTR CProfileManager::GetLine(LPTSTR& tch)
{
	LPTSTR line = tch;

	if(*tch == 0)
		return NULL;

	while(*tch != _T('\n') && *tch != 0)
	{
		if(*tch == _T('\r'))
			*tch = 0;
		tch++;
	}

	if(*tch != 0)
	{
		*tch = 0;
		tch++;
	}

	while(*line != 0 && *line == _T(' '))
		line++;

	return line;
}

void CProfileManager::DeleteBracket(CString& str)
{
	int len = str.GetLength();

	if(len > 0)
	{
		if(	(str[0] == _T('[') && str[len-1] == _T(']')) ||
			(str[0] == _T('{') && str[len-1] == _T('}')) ||
			(str[0] == _T('(') && str[len-1] == _T(')')) ||
			(str[0] == _T('\'') && str[len-1] == _T('\'')) ||
			(str[0] == _T('\"') && str[len-1] == _T('\"')) )
		{
			str.Delete(len-1);
			str.Delete(0);
		}
	}
}

CString Tokenize(LPCTSTR lpszSource, LPCTSTR lpszTokens, int& iStart )
{
	if( (lpszTokens == NULL) || (*lpszTokens == 0) )
	{
		if (iStart < _tcslen(lpszSource))
		{
			return lpszSource;
		}
	}
	else
	{
		LPCTSTR pszPlace = lpszSource + iStart;
		LPCTSTR pszEnd = lpszSource + _tcslen(lpszSource);
		if( pszPlace < pszEnd )
		{
			int nIncluding = _tcsspn( pszPlace, lpszTokens );

			if( (pszPlace+nIncluding) < pszEnd )
			{
				pszPlace += nIncluding;
				int nExcluding = _tcscspn( pszPlace, lpszTokens );

				int iFrom = iStart+nIncluding;
				int nUntil = nExcluding;
				iStart = iFrom+nUntil+1;

				CString str = lpszSource;

				return( str.Mid( iFrom, nUntil ) );
			}
		}
	}

	// return empty string, done tokenizing
	iStart = -1;

	return (LPTSTR)(lpszSource + iStart);
}

BOOL CProfileManager::Read(CString strFilename /*= _T("")*/)
{
	if(!strFilename.IsEmpty())
	{
		m_strFilename = strFilename;
	}

	ClearInstance();

	CFile file;
	if(file.Open(m_strFilename, CFile::modeRead | CFile::typeBinary))
	{
		int size = (int)file.GetLength();
		LPBYTE buf = new BYTE[size+2];
		::memset(buf, 0, size+2);

		file.Read(buf, size);

#ifdef UNICODE
		if(buf[0] == 0xff && buf[1] == 0xfe)
#endif
		{
			CString group_name = _T("");

#ifdef UNICODE
			LPTSTR buff = (LPTSTR)(buf+2);
#else
			LPTSTR buff = (LPTSTR)(buf);
#endif
			LPTSTR line = GetLine(buff);
			while(line != NULL)
			{
				if(line[0] == _T('['))
				{
					group_name = line;
					DeleteBracket(group_name);

					// 대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
					group_name.MakeLower();
				}
				else if(line[0] != 0)
				{
					int pos = 0;

					CString key = Tokenize(line, _T("="), pos);
					CString value = (LPTSTR)(line + pos);

					// 대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
					key.MakeLower();

					CMapStringToString* group;
					if(m_mapGroup.Lookup(group_name, (void*&)group ))
					{
						group->SetAt(key, value);
//						TRACE(_T("%s=%s\r\n"), key, value);
					}
					else
					{
						group = new CMapStringToString;
						group->SetAt(key, value);
						m_mapGroup.SetAt(group_name, group);
//						TRACE(_T("[%s]\r\n"), group_name);
//						TRACE(_T("%s=%s\r\n"), key, value);
					}
				}

				line = GetLine(buff);
			}
		}

		delete []buf;
		file.Close(); // skpark 2012.10.16
		return TRUE;
	}

	return FALSE;
}

BOOL CProfileManager::Write(CString strFilename /*= _T("")*/)
{
	if(!strFilename.IsEmpty())
	{
		m_strFilename = strFilename;
	}

	TCHAR drive[MAX_PATH], dir[MAX_PATH], fname[MAX_PATH], ext[MAX_PATH];
	_splitpath(m_strFilename, drive, dir, fname, ext );

	CString path;
	path.Format(_T("%s%s"), drive, dir);
	path.Replace(_T("/"), _T("\\"));

	//::CreateDirectory(path, NULL);
	MakeSureDirectoryPathExists(path);

	CFile file;
	if(file.Open(m_strFilename, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
	{
#ifdef UNICODE
		unsigned short header = 0xfeff;
		file.Write(&header, sizeof(short));
#endif

		POSITION pos;
		CString key;
		CMapStringToString* group;

		for( pos = m_mapGroup.GetStartPosition(); pos != NULL; )
		{
			m_mapGroup.GetNextAssoc( pos, key, (void*&)group );

			CString line;
			line.Format(_T("[%s]\r\n"), key);

			file.Write((LPCTSTR)line, line.GetLength()*sizeof(TCHAR));

			//
			POSITION pos_sub;
			CString key_sub;
			CString value_sub;

			for( pos_sub = group->GetStartPosition(); pos_sub != NULL; )
			{
				group->GetNextAssoc( pos_sub, key_sub, value_sub );

				CString line;
				line.Format(_T("%s=%s\r\n"), key_sub, value_sub);

				file.Write((LPCTSTR)line, line.GetLength()*sizeof(TCHAR));
			}
		}
		file.Close();

		return TRUE;
	}

	return FALSE;
}

// cgs
CString	CProfileManager::GetProfileString(CString strTabName, CString strKeyName, CString strDefault /*= _T("")*/)
{
	strTabName.MakeLower();
	strKeyName.MakeLower();

	CMapStringToString* group;
	if(m_mapGroup.Lookup(strTabName, (void*&)group ))
	{
		CString value;
		if(group->Lookup(strKeyName, value))
		{
			return value;
		}
	}

	return _T("");
}

DWORD CProfileManager::GetProfileString(CString strTabName, CString strKeyName, CString strDefault, CString& strReturnedString)
{
	strReturnedString = GetProfileString(strTabName, strKeyName, strDefault);
	return strReturnedString.GetLength();
}

int CProfileManager::GetProfileInt(CString strTabName, CString strKeyName, int nDefault/*=0*/)
{
	CString strDefault;
	strDefault.Format(_T("%d"), nDefault);

	return _ttoi(GetProfileString(strTabName, strKeyName, strDefault));
}

DWORD CProfileManager::WriteProfileString(CString strTabName, CString strKeyName, CString strString, BOOL bImmediately/*=FALSE*/)
{
	strTabName.MakeLower();
	strKeyName.MakeLower();

	CMapStringToString* group;
	if(m_mapGroup.Lookup(strTabName, (void*&)group ))
	{
		group->SetAt(strKeyName, strString);
	}
	else
	{
		group = new CMapStringToString;
		group->SetAt(strKeyName, strString);
		m_mapGroup.SetAt(strTabName, group);
	}

	if(bImmediately)
	{
		Write();
	}

	return TRUE;
}

DWORD CProfileManager::WriteProfileInt(CString strTabName, CString strKeyName, int nValue, BOOL bImmediately/*=FALSE*/)
{
	CString strString;
	strString.Format(_T("%d"), nValue);

	return WriteProfileString(strTabName, strKeyName, strString, bImmediately);
}
