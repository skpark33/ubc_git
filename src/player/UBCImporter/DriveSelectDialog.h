#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "EditEx.h"
#include "HoverButton.h"

// CDriveSelectDialog 대화 상자입니다.

class CDriveSelectDialog : public CDialog
{
	DECLARE_DYNAMIC(CDriveSelectDialog)

public:
	CDriveSelectDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDriveSelectDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DRIVE_SELECT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CImageList	m_ilDrives;

	CMapStringToString	m_mapVolumeToLabel;
	CMapStringToString	m_mapDriveToLabel;

public:
	CListCtrl		m_lcDrives;
	CString			m_strDrive;
	CString			m_strExceptDrive;							///<보여주지 말아야 할 드라이브 문자
	
	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;

	void	GetProcessVolume ();
	BOOL	_GetProcessVolume (HANDLE hVol, char* lpBuf, int iBufSize);
	void	InitDriveLabel();

	bool	GetHostName(LPCSTR lpszPath, CString strHostName);
	void	InitDriveListCtrl();
	void	SetExceptDrive(CString strDrive);					///<보여주지 말아야 할 드라이브 문자를 설정한다.
};


