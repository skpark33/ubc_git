/************************************************************************************/
/*! @file CopyInfo.h
	@brief 복사 대상이되는 컨텐츠 파일의 정보를 갖는 CCopyInfo 클래스 선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2011/05/09\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
	@b 작성)
	-# <2011/05/09:정운형:최초작성>

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/
#pragma once
#include <afxtempl.h>



//! 복사 대상이되는 컨텐츠 파일의 정보를 갖는 클래스
/*!
*/
class CCopyInfo
{
public:
	CCopyInfo();
	virtual ~CCopyInfo();

	CCopyInfo& operator= (const CCopyInfo& clsInfo)
	{
		m_strPackageName	= clsInfo.m_strPackageName;
		m_strSrcDrive		= clsInfo.m_strSrcDrive;
		m_strTargetDrive	= clsInfo.m_strTargetDrive;
		m_strLocation		= clsInfo.m_strLocation;
		m_strFileName		= clsInfo.m_strFileName;
		m_ulFileSize		= clsInfo.m_ulFileSize;
		m_nContentType		= clsInfo.m_nContentType;

		return *this;
	}

	bool operator== (CCopyInfo& clsInfo)
	{
		//if(m_strPackageName		!= clsInfo.m_strPackageName)	return false;
		if(m_strSrcDrive		!= clsInfo.m_strSrcDrive)		return false;
		if(m_strTargetDrive		!= clsInfo.m_strTargetDrive)	return false;
		if(m_strLocation		!= clsInfo.m_strLocation)		return false;
		if(m_strFileName		!= clsInfo.m_strFileName)		return false;
		if(m_ulFileSize			!= clsInfo.m_ulFileSize)		return false;
		if(m_nContentType		!= clsInfo.m_nContentType)		return false;

		return true;
	}

	CString				m_strPackageName;		///<패키지 명
	CString				m_strSrcDrive;			///<소스 드라이브
	CString				m_strTargetDrive;		///<타겟 드라이브
	CString				m_strLocation;			///<컨텐츠 파일의 경로
	CString				m_strFileName;			///<컨텐츠 파일의 이름
	ULONGLONG			m_ulFileSize;			///<파일의 volume
	int					m_nContentType;			///<컨텐츠 타입
};



class CCopyInfoArray : public CArray<CCopyInfo*, CCopyInfo*>
{
public:
	CCopyInfoArray();
	virtual ~CCopyInfoArray();

	int		Add(CCopyInfo* pInfo);		///<새로운 엘리먼트 추가(중복된 element가 들어가지 않도록 재정의)
	void	Clear(void);				///<배열을 정리한다.
};