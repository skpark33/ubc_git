//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCImporter.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_UBCIMPORTER_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDR_SPLASH_GIF                  128
#define IDR_SPLASH_IMP_GIF              128
#define IDB_CHECK                       132
#define IDD_FILE_COPY_DLG               136
#define IDB_BTN_CANCEL                  137
#define IDR_PREPARE_SAVE_GIF            145
#define IDD_NOTIFY_SAVE_DLG             146
#define IDB_POP_BTN_SELECT              148
#define IDB_POP_BTN_CANCEL              149
#define IDB_SPLASH                      150
#define IDB_SPLASH_IMP                  150
#define IDB_OPEN_BG                     151
#define IDB_IMPORT                      154
#define IDB_EXPORT                      155
#define IDB_IMPORT_DISABLE              156
#define IDB_EXPORT_DISABLE              157
#define IDB_BITMAP1                     158
#define IDB_HEADER_ARROW                158
#define IDB_CHECK_BOXES                 159
#define IDB_NARSHA_SPLASH               160
#define IDB_NARSHA_SPLASH_IMP           160
#define IDR_NARSHA_SPLASH_GIF           162
#define IDR_NARSHA_SPLASH_IMP_GIF       162
#define IDI_ICON1                       163
#define IDR_NARSHA                      163
#define IDR_SPLASH_IMP_GIF1             163
#define IDR_SPLASH_EXP_GIF              163
#define IDR_NARSHA_SPLASH_EXP_GIF       164
#define IDB_NARSHA_SPLASH_EXP           166
#define IDB_SPLASH_EXP                  167
#define IDB_BITMAP2                     173
#define IDB_CONTENTS                    173
#define IDB_DRIVE_LIST                  211
#define IDD_DRIVE_SELECT                212
#define IDB_BTN_OK                      294
#define IDC_HOST_LIST                   1000
#define IDC_HOST_NAME_EDT               1001
#define IDC_STATIC_STATUS               1004
#define IDC_PROGRESS_CURRENT            1005
#define IDC_PROGRESS_TOTAL              1006
#define IDC_BUTTON_CANCEL               1007
#define IDC_IMPORT_BTN                  1008
#define IDC_BUTTON_OK                   1008
#define IDC_EXPORT_BTN                  1009
#define IDC_FILE_LIST                   1009
#define IDC_TYPE_COMBO                  1012
#define IDC_SEARCH_BTN                  1013
#define IDC_ACTION_COMBO                1015
#define IDC_LIST_DRIVES                 1089
#define IDC_EDIT_HOST_NAME              1090
#define IDC_EDIT_DESC                   1091

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        174
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
