// NotifySaveDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCImporter.h"
#include "NotifySaveDlg.h"


// CNotifySaveDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNotifySaveDlg, CDialog)

CNotifySaveDlg::CNotifySaveDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNotifySaveDlg::IDD, pParent)
{

}

CNotifySaveDlg::~CNotifySaveDlg()
{
	m_gifSave.Stop();
	m_gifSave.UnLoad();
}

void CNotifySaveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CNotifySaveDlg, CDialog)
END_MESSAGE_MAP()


// CNotifySaveDlg 메시지 처리기입니다.

BOOL CNotifySaveDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_gifSave.Create("Animated GIF", WS_CHILD|WS_VISIBLE, CRect(0, 0, 0, 0), this);
	m_gifSave.Load(MAKEINTRESOURCE(IDR_PREPARE_SAVE_GIF),_T("GIF"));

	SIZE stSize = m_gifSave.GetSize();
	this->MoveWindow(0, 0, stSize.cx, stSize.cy);
	this->CenterWindow();

	m_gifSave.ShowWindow(SW_SHOW);
	m_gifSave.Draw();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
