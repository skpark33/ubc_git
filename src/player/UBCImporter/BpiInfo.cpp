/************************************************************************************/
/*! @file BpiInfo.cpp
	@brief 방송계획 파일(*.bpi)의 정보를 갖는 클래스 구현 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2011/07/13\n
	
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2011/07/13:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "StdAfx.h"
#include "BpiInfo.h"


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CBpiInfo::CBpiInfo(void)
: m_strDrive("")		
, m_strBpiName("")			
, m_strSiteId("")			
, m_strLastWrittenTime("")	
, m_strStartDate("")			
, m_strEndDate("")			
, m_strDescription("")
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CBpiInfo::~CBpiInfo(void)
{
}
