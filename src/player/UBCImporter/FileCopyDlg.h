#pragma once
#include "afxcmn.h"
#include "ListCtrl/ProListCtrl.h"
#include "CopyInfo.h"


// CFileCopyDlg 대화 상자입니다.

class CFileCopyDlg : public CDialog
{
	DECLARE_DYNAMIC(CFileCopyDlg)

public:
	CFileCopyDlg(CCopyInfoArray* m_paryCopyInfo,
				CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFileCopyDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FILE_COPY_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CProListCtrl		m_listFile;						///<복사할 파일의 리스트 컨트롤
	CCopyInfoArray*		m_paryCopyInfo;					///<복사할 파일(컨텐츠) 배열
	CWinThread*			pThreadCopy;					///<복사작업을 하는 thred
	CImageList			m_imglistContents;				///<리스트 이미지 리스트
	bool				m_bCancel;						///<취소여부
	bool				m_bWorking;						///<다운로드 작업을 진행중인지 여부
	int					m_nFailCount;					///<실패한 개수

	afx_msg void OnBnClickedButtonCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnCompleteFileCopy(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnStatusFileCopy(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCancelFileCopy(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFailFileCopy(WPARAM wParam, LPARAM lParam);
	virtual BOOL OnInitDialog();

private:
	void	InitList(void);								///<리스트 초기화
	void	BeginCopy(void);							///<파일 복사를 시작한다.

	static UINT	FileCopyThread(LPVOID param);			///<파일을 복사하는 thread
public:
	afx_msg void OnClose();
	afx_msg void OnBnClickedButtonOk();
};
