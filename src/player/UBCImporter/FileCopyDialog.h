#pragma once


#include "ProgressCtrlEx.h"
#include "Schedule.h"
#include "DataContainer.h"

// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업
#include "HoverButton.h"
// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업


// CFileCopyDialog 대화 상자입니다.


class CFileCopyDialog : public CDialog
{
	DECLARE_DYNAMIC(CFileCopyDialog)

public:
	CFileCopyDialog(CWnd* pParent, LPCSTR lpszDrive);   // 표준 생성자입니다.
	virtual ~CFileCopyDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FILE_COPY_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	//virtual void PostNcDestroy();
//	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CWnd*				m_pParentWnd;
	CONTENTS_INFO_MAP	m_mapContents;		///<컨텐츠 파일의 중복된 복사를 방지를 위한 맵(Key:파일의 경로, Val:CONTENTS_INFO*) 

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	CHoverButton			m_btnCancel;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	ULONGLONG			m_ulTotalSize;
	CString				m_strTargetDrive;

	ULONGLONG	GetTotalFileSize();

	CWinThread*	m_pThread;

	static UINT	FileCopyThread(LPVOID param);

public:

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnCompleteFileCopy(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDisplayStatus(WPARAM wParam, LPARAM lParam);

	CStatic				m_staticStatus;
	CProgressCtrlEx		m_pbarCurrent;
	CProgressCtrlEx		m_pbarTotal;

	bool	m_bProcessThread;
	CMutex	m_mutex;

	void	Start(bool bShowWindow=false);
	afx_msg void OnBnClickedButtonCancel();
};

typedef struct {
	CWnd*				pParentWnd;
	CFileCopyDialog*	pWnd;
//	CStatic*			pStatic;
//	CProgressCtrlEx*	pbarCurrent;
//	CProgressCtrlEx*	pbarTotal;
	ULONGLONG			ulTotalSize;
	CString				strTargetDrive;
} FILE_COPY_PARAM;

typedef struct {
	CString		title;
	CString		current_text;
	int			current_pos;
	CString		total_text;
	int			total_pos;
} DISPLAY_PARAM;

