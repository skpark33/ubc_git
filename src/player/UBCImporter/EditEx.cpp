// Control\EditEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"

#include "EditEx.h"

// CEditEx

IMPLEMENT_DYNAMIC(CEditEx, CEdit)
CEditEx::CEditEx()
{
}

CEditEx::~CEditEx()
{
}


BEGIN_MESSAGE_MAP(CEditEx, CEdit)
END_MESSAGE_MAP()



// CEditEx 메시지 처리기입니다.

int CEditEx::GetValueInt()
{
	CEdit::GetWindowText(m_str);

	m_str.Replace(_T(","), _T(""));

	return _ttoi(m_str);
}

LPCTSTR CEditEx::GetValueIntMoneyTypeString()
{
	CEdit::GetWindowText(m_str);

	return ::ToMoneyTypeString(m_str);
}

LPCTSTR CEditEx::GetValueString()
{
	CEdit::GetWindowText(m_str);

	return m_str;
}

void CEditEx::SetValue(int value)
{
	SetWindowText(ToString(value));
}

void CEditEx::SetValue(LPCTSTR value)
{
	SetWindowText(value);
}

void CEditEx::SetValueToMoneyType(int value)
{
	SetWindowText(ToMoneyTypeString(value));
}

