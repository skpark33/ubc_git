// UBCImporterDlg.h : 헤더 파일
//

#pragma once

#include "resource.h"		// 주 기호입니다.
#include "HoverButton.h"
#include "PictureEx.h"
#include "ximage.h"
//#include "NotifySaveDlg.h"
#include "afxwin.h"
#include "ListCtrl/ListCtrlEx.h"
#include "CopyInfo.h"

//Import/Export 하는 종류
enum E_IMPORT_TYPE
{
	E_IMPORT_PACKAGE,		//컨텐츠 패키지
	E_IMPORT_BPI,			//방송계획
	E_IMPORT_ANNOUNCE		//긴급공지
};


// CUBCImporterDlg 대화 상자
class CUBCImporterDlg : public CDialog
{
// 생성입니다.
public:
	CUBCImporterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

	void	InitList(void);												///<Host list를 list control에 설정
	void	ClearLocoalHostInfoArray(void);								///<locla host info 배열을 정리한다
	void	ClearBpiInfoArray(void);									///<Bpi info 배열을 정리한다
	void	ClearAnnounceInfoArray(void);								///<Announce info 배열을 정리한다
	bool	GetHostInfo(LPCSTR lpszPath, CString strDrive);				///<지정된 경로의 호스트 정보를 구한다
	bool	GetBpiInfo(LPCSTR lpszPath, CString strDrive);				///<지정된 경로의 Bpi 정보를 구한다
	bool	GetAnnounceInfo(LPCSTR lpszPath, CString strDrive);			///<지정된 경로의 긴급공지 정보를 구한다
	BOOL	_GetProcessVolume(HANDLE hVol, char* lpBuf, int iBufSize);
	void	GetProcessVolume();
	void	InitDriveLabel();
	int		DeleteFolder(LPCSTR lpszTargetPath);
	bool	WriteProperty(void);										///<변경된 host 정보를 property 파일에 기록하여 Start가 brw를 구종 시킬 수 있도록 한다
	bool	SetArgument(void);											///<실행인자 설정
	void	RunSilentImport(void);										///<UI를 숨기고 import 기능을 실행
	bool	WriteBrowserProperty(int);
	bool	MakeCopyInfoList(void);										///<복사할 파일정보 배열을 만든다.
	void	ClearCopyInfoArray(void);									///<복사할 파일정보 배열을 정리한다.
	void	SetEnableUI(bool bEnable);									///<UI의 Enable 상태를 설정
	void	CopyPackageFiles(void);										///<Package파일들을 복사한다.
	void	CreateListColumn(void);										///<리스트 컨트롤의 컬럼을 만든다.
	void	ParseBpiFile(void);											///<방송계획파일을 분석하여 복사할 파일 리스트를 만든다.
	void	RunSilentExport(void);										///<매니져를 통한 방송계획 Export를 수행한다.
	bool	CopyAnnounce(void);											///<긴급공지 파일을 복사한다.
	bool	CopyFiles(CString strSrc, CString strTarget);				///<소스의 모든 파일을 타겟의 위치로 복사한다.

	CPtrArray			m_aryLocalHostInfo;				///<locla system에 존재하는 host 정보들의 배열
	CPtrArray			m_aryBpiInfo;					///<locla system에 Bpi 정보들의 배열
	CPtrArray			m_aryAnnounceInfo;				///<locla system에 긴급공지 정보들의 배열

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCIMPORTER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;

	CString				m_strDescription;				///<패키지 설명
	CString				m_strArgDrive;					///<실행인자로 입력된 드라이브
	CString				m_strArgPackageName;			///<실행인자로 입력된 package 이름
	CString				m_strArgBpiName;				///<실행인자로 입력된 방송계획 이름
	CString				m_strArgAnnounceName;			///<실행인자로 입력된 긴급공지 이름
	CString				m_strTargetDrive;				///<Import/Export 대상 드라이브
	CCopyInfoArray*		m_paryCopyInfo;					///<복사할 파일(컨텐츠) 배열
	CStringArray		m_arySrcPackage;				///<Source 패키지경로의 배열

	CComboBox			m_comboType;					///<패키지, 방송계획 여부
	CComboBox			m_comboAction;					///<Import, Export 여부
	CHoverButton		m_btnRefresh;					///<새로고침 버튼
	CHoverButton		m_btnOK;						///<select 버튼
	CHoverButton		m_btnCancel;					///<cancel 버튼
	CListCtrlEx			m_listHost;						///<host 정보를 보여주는 list 컨트롤
	bool				m_bImport;						///<Import/Export 구분
	bool				m_bSilentImport;				///<UI를 숨기는 import 여부
	int					m_nCheckCount;					///<리스트컨트롤 체크된 개수
	int					m_nDisplayA;
	int					m_nDisplayB;
	int					m_nType;						///<Import/Export 종류(Package, Bpi, Announce)
	bool				m_bMessage;
	bool				m_bSilentExport;				///<방송계획 매니져에서 export 옵션으로 실행했는지 여부
	bool				m_announce_exist;				// announce 가 USB 에 있으므로 이를 단말로 Import
	CMapStringToString	m_mapVolumeToLabel;
	CMapStringToString	m_mapDriveToLabel;

	//CNotifySaveDlg*		m_pdlgNotify;					///<편성관리 저장, preview기능 수행중 config 파일의 저장을 알리는 대화상자 

	static UINT	ThreadMakeCopyInfoList(LPVOID pParam);	///<복사할 파일의 정보배열을 만드는 thread

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnCompleteFileCopy(WPARAM wParam, LPARAM lParam);	///<complete file copy message handler
	afx_msg LRESULT OnCheckedCount(WPARAM wParam, LPARAM lParam);
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedSearchBtn();
	DECLARE_MESSAGE_MAP()	
	
};
