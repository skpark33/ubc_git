// DriveSelectDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCImporter.h"
#include "DriveSelectDialog.h"

#define		TIMER_EDIT_SEL			6789			///<Host name select timer

// CDriveSelectDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDriveSelectDialog, CDialog)

CDriveSelectDialog::CDriveSelectDialog(CWnd* pParent /*=NULL*/)
:	CDialog(CDriveSelectDialog::IDD, pParent)
,	m_strDrive("")
,	m_strExceptDrive("")
{

}

CDriveSelectDialog::~CDriveSelectDialog()
{
}

void CDriveSelectDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_DRIVES, m_lcDrives);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CDriveSelectDialog, CDialog)
END_MESSAGE_MAP()


// CDriveSelectDialog 메시지 처리기입니다.

BOOL CDriveSelectDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_btnOK.LoadBitmap(IDB_BTN_OK, RGB(255, 255, 255));
	m_btnCancel.SetToolTipText("Cancel");
	m_btnOK.SetToolTipText("OK");
	
	SetWindowText("Select target drive");
	m_btnOK.SetWindowText("OK");

	CBitmap bmp;
	bmp.LoadBitmap(IDB_DRIVE_LIST);

	if(m_ilDrives.Create(48, 48, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilDrives.Add(&bmp, RGB(255,255,255)); //바탕의 회색이 마스크
	}

	m_lcDrives.SetExtendedStyle(m_lcDrives.GetExtendedStyle() | LVS_EX_FULLROWSELECT);
	m_lcDrives.SetImageList(&m_ilDrives, LVSIL_NORMAL);
	m_lcDrives.SetImageList(&m_ilDrives, LVSIL_SMALL);

	m_lcDrives.InsertColumn(0, "Label+Drive", 0, 150);
	m_lcDrives.InsertColumn(1, "Drive", 0, 150);

	InitDriveLabel();
	InitDriveListCtrl();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDriveSelectDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	POSITION pos = m_lcDrives.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int nItem = m_lcDrives.GetNextSelectedItem(pos);
		m_strDrive = m_lcDrives.GetItemText(nItem, 1);
	}
	else
	{
		::AfxMessageBox("Select target drive !!!", MB_ICONWARNING);
		return;
	}//if

	CDialog::OnOK();
}


void  CDriveSelectDialog::GetProcessVolume()
{
	char buf[MAX_PATH];           // buffer for unique volume identifiers
	HANDLE hVol;                  // handle for the volume scan
	BOOL bFlag;                   // generic results flag

	// Open a scan for volumes.
	hVol = FindFirstVolume (buf, MAX_PATH );

	if (hVol == INVALID_HANDLE_VALUE)
		return;

	// We have a volume; process it.
	bFlag = _GetProcessVolume (hVol, buf, MAX_PATH);

	// Do while we have volumes to process.
	while (bFlag) 
	{
		bFlag = _GetProcessVolume (hVol, buf, MAX_PATH);
	}

	// Close out the volume scan.
	bFlag = FindVolumeClose( hVol );
}

BOOL CDriveSelectDialog::_GetProcessVolume(HANDLE hVol, char* lpBuf, int iBufSize)
{
	DWORD dwSysFlags;            // flags that describe the file system
	char FileSysNameBuf[MAX_PATH];
	char labelname[MAX_PATH];

	BOOL ret_val = GetVolumeInformation( lpBuf, labelname, MAX_PATH, NULL, NULL, &dwSysFlags, FileSysNameBuf, MAX_PATH);

	if(ret_val)
	{
		m_mapVolumeToLabel.SetAt(lpBuf, labelname);
	}

	return FindNextVolume( hVol, lpBuf, iBufSize );
}

void CDriveSelectDialog::InitDriveLabel()
{
	m_mapVolumeToLabel.RemoveAll();
	m_mapDriveToLabel.RemoveAll();

	//
	GetProcessVolume();

	//
	CStringArray	drive_list;
	int nPos =0;
	char drive[] = "?:\\";
	DWORD dwDriveList=::GetLogicalDrives();
	while(dwDriveList)
	{
		if(dwDriveList & 1)
		{
			drive[0] = 'A' + nPos;
			drive_list.Add(drive);
		}
		dwDriveList >>=1;
		nPos++;
	}

	//
	char volume[255];
	for(int i=0; i<drive_list.GetCount(); i++)
	{
		CString drive = drive_list.GetAt(i);

		BOOL ret = GetVolumeNameForVolumeMountPoint(drive, volume, 255);

		if(ret)
		{
			CString label;
			if(m_mapVolumeToLabel.Lookup(volume, label))
				m_mapDriveToLabel.SetAt(drive, label);
		}
	}
}


void CDriveSelectDialog::InitDriveListCtrl()
{
	m_lcDrives.DeleteAllItems();

	int count = 0;

	POSITION pos = m_mapDriveToLabel.GetStartPosition();
	if(pos == NULL)
	{
		return;
	}//if

	CString str, drive, label;
	while(pos != NULL)
	{
		m_mapDriveToLabel.GetNextAssoc(pos, drive, label);

		if(drive.CompareNoCase(m_strExceptDrive) == 0)
		{
			continue;
		}//if

		UINT type = GetDriveType(drive);
		if(label == "")
		{
			switch(type)
			{
			case DRIVE_REMOVABLE:
				label = "Removable Disk";
				break;
			case DRIVE_FIXED:
				label = "Local Disk";
				break;
			case DRIVE_REMOTE:
				label = "Remote Disk";
				break;
			case DRIVE_CDROM:
				label = "Optical Disk";
				break;
			case DRIVE_RAMDISK:
				label = "RAM Disk";
				break;
			default:
				label = "Unknown Disk";
				break;
			}//switch
		}//if

		int index = -1;
		for(int i=0; i<m_lcDrives.GetItemCount(); i++, index++)
		{
			CString drv = m_lcDrives.GetItemText(i, 1);
			if(drive < drv)
			{
				break;
			}//if
		}//for
		index++;
		count++;

		str.Format(" %s (%s)", label, drive.Left(2));
		m_lcDrives.InsertItem(index, str, type);
		m_lcDrives.SetItemText(index, 1, drive);

		continue;
	}//while

	if(count == 0)
	{
		AfxMessageBox("Can't found any drives !!!", MB_ICONWARNING);
		//CDialog::OnCancel();
	}
	//else
	//{
	//	//첫번째 드라이브 선택...
	//	m_lcDrives.SetSelectionMark(0);
	//	m_lcDrives.SetItemState(0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);
	//	m_lcDrives.SetFocus();
	//}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 보여주지 말아야 할 드라이브 문자를 설정한다. \n
/// @param (CString) strDrive : (in) 보여주지 말아야 할 드라이브 문자
/////////////////////////////////////////////////////////////////////////////////
void CDriveSelectDialog::SetExceptDrive(CString strDrive)
{
	m_strExceptDrive = strDrive;
}