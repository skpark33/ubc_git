// CheckListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "CheckListCtrl.h"

#include "ximage.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCheckListCtrl

CCheckListCtrl::CCheckListCtrl() : m_blInited(FALSE)
, m_nSortCol(0)
, m_bAscend(false)
, m_bCheckBoxType(false)
, m_nColumnCount(0)

, m_cx(0)
, m_cy(0)
, m_bLButton(false)
, m_bScrollBkImg(false)
{
	m_listSortType.Add(SORTTYPE_STRING);
	m_listTextColor.Add(RGB(0,0,0));
	m_listBackColor.Add(RGB(255,255,255));
}

CCheckListCtrl::~CCheckListCtrl()
{
}


BEGIN_MESSAGE_MAP(CCheckListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT_EX(LVN_ITEMCHANGED, OnItemChanged)		
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnNMCustomdraw)
	ON_NOTIFY_REFLECT_EX(LVN_COLUMNCLICK, OnLvnColumnclick)
	ON_WM_ERASEBKGND()

//	ON_WM_ERASEBKGND()
	ON_WM_HSCROLL()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_MOUSEWHEEL()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_VSCROLL()
	ON_NOTIFY(HDN_ENDTRACKA, 0, OnHdnEndtrack)
	ON_NOTIFY(HDN_ENDTRACKW, 0, OnHdnEndtrack)
	ON_NOTIFY(HDN_BEGINTRACKA, 0, OnHdnBegintrack)
	ON_NOTIFY(HDN_BEGINTRACKW, 0, OnHdnBegintrack)
	ON_WM_SYSKEYDOWN()
	ON_WM_TIMER()
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCheckListCtrl message handlers

BOOL CCheckListCtrl::OnItemChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	NMLISTVIEW* pNMLV = (NMLISTVIEW*)pNMHDR;
	*pResult = 0;

	if ( m_blInited && LVIF_STATE == pNMLV->uChanged && m_bCheckBoxType)
	{
		BOOL blAllChecked = TRUE;
		int nCount = GetItemCount();
		for(int nItem = 0; nItem < nCount; nItem++)
		{
			if ( !ListView_GetCheckState(GetSafeHwnd(), nItem) )
			{
				blAllChecked = FALSE;
				break;
			}
		}

		HDITEM hdItem;
		hdItem.mask = HDI_IMAGE;
		if (blAllChecked)
			hdItem.iImage = 1;
		else
			hdItem.iImage = 0;

		VERIFY( m_checkHeadCtrl.SetItem(0, &hdItem) );
	}

	return FALSE;
}

BOOL CCheckListCtrl::Initialize(bool bIsCheckBoxType, bool bIsRowColorType, UINT nID)
{
	VERIFY( m_checkHeadCtrl.SubclassWindow( GetHeaderCtrl()->GetSafeHwnd() ) );

	m_bRowColorType = bIsRowColorType;

	if (m_blInited)
		return TRUE;

	if(bIsCheckBoxType)
	{
		SetExtendedStyle( GetExtendedStyle() | LVS_EX_CHECKBOXES );
		m_checkHeadCtrl.m_bCheckBoxType = true;

		if(nID == 0)
		{
			HINSTANCE oldHand = AfxGetResourceHandle();
//			AfxSetResourceHandle(::GetModuleHandle(CONTROL_LIB_DLL_NAME));

			VERIFY( m_checkImgList.Create(IDB_CHECK_BOXES, 16, 5, RGB(255,0,255)) );

//			AfxSetResourceHandle(oldHand);
		}
		else
		{
			CBitmap		bmp;
			bmp.LoadBitmap(nID);
			m_checkImgList.Create(16, 16, ILC_COLORDDB | ILC_MASK, 0, 1); // 16x16x9개
			m_checkImgList.Add(&bmp, RGB(255, 0, 255)); // 마스크색 지정

			SetImageList(&m_checkImgList, LVSIL_SMALL);
		}

		CBitmap		bmp;
		bmp.LoadBitmap(IDB_HEADER_ARROW);
		m_checkImgList2.Create(11, 16, ILC_COLORDDB | ILC_MASK, 0, 1); // 16x16x9개
		m_checkImgList2.Add(&bmp, RGB(255, 0, 255)); // 마스크색 지정
		m_checkHeadCtrl.SetImageList(&m_checkImgList2);

		HDITEM hdItem;
		hdItem.mask = HDI_IMAGE | HDI_FORMAT;
		VERIFY( m_checkHeadCtrl.GetItem(0, &hdItem) );
		hdItem.iImage = 0;
		hdItem.fmt |= HDF_IMAGE;

		VERIFY( m_checkHeadCtrl.SetItem(0, &hdItem) );
	}
	else
	{
		if(nID == 0)
		{
			HINSTANCE oldHand = AfxGetResourceHandle();
//			HMODULE module = ::GetModuleHandle(CONTROL_LIB_DLL_NAME);
//			AfxSetResourceHandle(module);

			VERIFY( m_checkImgList.Create(IDB_CHECK_BOXES, 16, 5, RGB(255,0,255)) );

//			AfxSetResourceHandle(oldHand);
		}
		else
		{
			CBitmap		bmp;
			bmp.LoadBitmap(nID);
			m_checkImgList.Create(16, 16, ILC_COLORDDB | ILC_MASK, 0, 1); // 16x16x9개
			m_checkImgList.Add(&bmp, RGB(255, 0, 255)); // 마스크색 지정
	
			SetImageList(&m_checkImgList, LVSIL_SMALL);
		}

		CBitmap		bmp;
		bmp.LoadBitmap(IDB_HEADER_ARROW);
		m_checkImgList2.Create(11, 16, ILC_COLORDDB | ILC_MASK, 0, 1); // 16x16x9개
		m_checkImgList2.Add(&bmp, RGB(255, 0, 255)); // 마스크색 지정
		m_checkHeadCtrl.SetImageList(&m_checkImgList2);
	}

	m_bCheckBoxType = bIsCheckBoxType;
	m_blInited = TRUE;

	return TRUE;
}

void CCheckListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );

	int nItem = static_cast<int>( pLVCD->nmcd.dwItemSpec );

	switch(pLVCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:

        *pResult = CDRF_NOTIFYITEMDRAW;
		break;

	case CDDS_ITEMPREPAINT:
		if(m_bRowColorType)
		{
			COLORREF crText=RGB(0,0,0);
			COLORREF clrBk=RGB(255,255,255);

			if(GetRowColor(nItem, crText, clrBk))
			{
				pLVCD->clrText   = crText;
				pLVCD->clrTextBk = clrBk;
			}

			*pResult = CDRF_DODEFAULT;
		}
		else
		{
			COLORREF crText=RGB(0,0,0);
			COLORREF clrBk=RGB(255,255,255);

			if(GetItemColor(nItem, pLVCD->iSubItem, crText, clrBk))
			{
				pLVCD->clrText   = crText;
				pLVCD->clrTextBk = clrBk;
			}

			*pResult = CDRF_NOTIFYSUBITEMDRAW;
		}
		break;

	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		{
			COLORREF crText=RGB(0,0,0);
			COLORREF clrBk=RGB(255,255,255);

			if(GetItemColor(nItem, pLVCD->iSubItem, crText, clrBk))
			{
				pLVCD->clrText   = crText;
				pLVCD->clrTextBk = clrBk;
			}
		}

		*pResult = CDRF_NEWFONT;
		break;

	default:

		*pResult = CDRF_DODEFAULT;
		break;
	}
}

bool CCheckListCtrl::GetRowColor(int nRow, COLORREF& crText, COLORREF& clrBk)
{
	return false;
}

bool CCheckListCtrl::GetItemColor(int nRow, int nCol, COLORREF& crText, COLORREF& clrBk)
{
	if(nCol >= 0 && nCol < m_nColumnCount)
		crText = m_listTextColor.GetAt(nCol);
	if(nCol >= 0 && nCol < m_nColumnCount)
		clrBk = m_listBackColor.GetAt(nCol);

	return true;
}

BOOL CCheckListCtrl::OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int	nSortCol = pNMLV->iSubItem;

	if(m_bCheckBoxType && nSortCol == 0)
		return TRUE;

	// 정렬한다.

	SORT_PARAM	sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = nSortCol;
	sort_param.bAscend = (m_nSortCol != nSortCol) ? true : !m_bAscend;
	sort_param.sortType = m_listSortType.GetAt(nSortCol);

	// 현재 정렬기준 컬럼과 정렬모드를 저장한다.
	m_nSortCol = nSortCol;
	m_bAscend = sort_param.bAscend;

	m_checkHeadCtrl.SetSortArrow( nSortCol );
    _SortItems(&SortColumn, (LPARAM)&sort_param);

	return TRUE;
}

BOOL CCheckListCtrl::_SortItems(PFNLVCOMPARE pfnCompare, DWORD data)
{
    return SortItems(&SortColumn, data);
}

int CALLBACK CCheckListCtrl::SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam)
{
	SORT_PARAM*	ptrSortParam = (SORT_PARAM *)lSortParam;
	CListCtrl*	pListCtrl = (CListCtrl *)CWnd::FromHandle(ptrSortParam->hWnd);

	LVFINDINFO	lvFind;
	lvFind.flags = LVFI_PARAM;
	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	//lvFind2.flags = LVFI_PARAM;
	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	CString strItem1 = pListCtrl->GetItemText(nIndex1, ptrSortParam->nCol);
	CString	strItem2 = pListCtrl->GetItemText(nIndex2, ptrSortParam->nCol);

	CString		strItemPadding1;
	CString		strItemPadding2;

	switch(ptrSortParam->sortType)
	{
	default:
	case SORTTYPE_STRING:
		strItemPadding1 = strItem1;
		strItemPadding2 = strItem2;
		break;

	case SORTTYPE_NUMBER:
		{
			strItem1.Replace(_T(","), _T(""));
			strItem2.Replace(_T(","), _T(""));

			double i1 = _tstof(strItem1);
			double i2 = _tstof(strItem2);

			strItemPadding1.Format(_T("%020.5f"), i1);
			strItemPadding2.Format(_T("%020.5f"), i2);
		}
		break;
	}

	if (ptrSortParam->bAscend == true)
		return strItemPadding1.Compare(strItemPadding2);
	else
		return strItemPadding2.Compare(strItemPadding1);
}

BOOL CCheckListCtrl::Sort(int col, bool ascend)
{
	if(col<0)
	{
		col = m_nSortCol;
		ascend = m_bAscend;
	}
	else
	{
		m_nSortCol = col;
		m_bAscend = ascend;
	}

	//
	SORT_PARAM	sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = col;
	sort_param.bAscend = ascend;
	sort_param.sortType = m_listSortType.GetAt(col);

	m_checkHeadCtrl.SetSortArrow( col );

//    return SortItems(&SortColumn, (LPARAM)&sort_param);
	return _SortItems(&SortColumn, (LPARAM)&sort_param);
}

bool CCheckListCtrl::SetColumnType(int nCol, SORT_TYPE sortType, COLORREF crText, COLORREF crBack)
{
	if(m_nColumnCount <= nCol)
		return false;

	m_listSortType.SetAt(nCol, sortType);

	if(crText != 0xFFFFFFFF)
        m_listTextColor.SetAt(nCol, crText);

	if(crBack != 0xFFFFFFFF)
		m_listBackColor.SetAt(nCol, crBack);

	return true;
}

BOOL CCheckListCtrl::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

/*	if(m_bRowColorType)
	{
		CRect rcClient;
		GetClientRect( &rcClient );

		pDC->FillRect(rcClient, &CBrush(::GetSysColor(COLOR_BTNFACE)));
		return TRUE;
	}
	else
*/

	if (m_bmBkImg.GetSafeHandle())
		return TRUE;

	if(m_bRowColorType)
	{
	}
	else
	{
		CListCtrl::OnEraseBkgnd(pDC);

		CRect rect;
		GetClientRect(rect);

		SCROLLINFO si;
		ZeroMemory(&si, sizeof(SCROLLINFO));
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_POS;
		GetScrollInfo(SB_HORZ, &si);
		rect.left -= si.nPos;

		for(int i=0; i<=GetHeaderCtrl()->GetItemCount();i++)
		{
			rect.right = rect.left+GetColumnWidth(i);

			CBrush brush(m_listBackColor.GetAt(i));
			pDC->FillRect(&rect, &brush);

			rect.left += GetColumnWidth(i);
		}

/*
		CRect rect;
		GetClientRect(rect);

		int count = GetItemCount();
		if(count > 0)
		{
			CRect tmp_rect;
			GetItemRect(count-1, tmp_rect, LVIR_LABEL   );
			rect.top = tmp_rect.bottom;
		}

		//
		CHeaderCtrl* header_ctrl = GetHeaderCtrl();
		count = header_ctrl->GetItemCount();
		int width = 0;

		for(int i=0; i<count; i++)
			width += GetColumnWidth(i);

		rect.right = width;
		pDC->FillSolidRect(rect, RGB(255,255,255));

		//
		GetClientRect(rect);
		if(rect.Width() > width)
		{
			rect.left = width;
			pDC->FillSolidRect(rect, RGB(255,255,255));
		}
*/
		return FALSE;
	}

	return CListCtrl::OnEraseBkgnd(pDC);
}

int CCheckListCtrl::InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat, int nWidth, int nSubItem)
{
	int ret_value = CListCtrl::InsertColumn(nCol, lpszColumnHeading, nFormat, nWidth, nSubItem);

	if(ret_value >= 0)
	{
		m_listSortType.Add(SORTTYPE_STRING);
		m_listTextColor.Add(RGB(0,0,0));
		m_listBackColor.Add(RGB(255,255,255));

		m_nColumnCount++;
	}

	return ret_value;
}


///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////
///////////////////////////////////////////////////


BOOL CCheckListCtrl::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(pMsg->message == WM_MOUSEMOVE)
	{
		if(m_bLButton)
		{
			if (m_bmBkImg.GetSafeHandle())
				InvalidateRect(NULL, FALSE);
		}
	}

	return CListCtrl::PreTranslateMessage(pMsg);
}



BOOL CCheckListCtrl::SetBkImage(UINT nID)
{
	return _SetBkImage(MAKEINTRESOURCE(nID), FALSE);
}

BOOL CCheckListCtrl::SetBkImage(LPCTSTR lpszName)
{
	return _SetBkImage(lpszName, FALSE);
}

BOOL CCheckListCtrl::SetBkImageFromFile(LPCTSTR lpszFileName)
{
	return _SetBkImage(lpszFileName, TRUE);
}

BOOL CCheckListCtrl::SetBkImage(CBitmap *pBitmap)
{
	if (m_bmBkImg.GetSafeHandle())
		m_bmBkImg.DeleteObject();

	if(!pBitmap)
	{
		return FALSE;
	}
	ASSERT_POINTER(pBitmap,CBitmap);
	ASSERT(pBitmap->m_hObject);

	m_cx = m_cy = 0;
	
	// copy image to local bitmap object...
	CPaintDC dc(this);
	CDC memDC,memDC2;
	BITMAP bmSrc;
	pBitmap->GetBitmap( &bmSrc );
	memDC.CreateCompatibleDC(&dc);
	m_bmBkImg.CreateCompatibleBitmap(&dc, bmSrc.bmWidth, bmSrc.bmHeight);
	CBitmap* pbmOld = memDC.SelectObject(&m_bmBkImg);

	memDC2.CreateCompatibleDC(&dc);
	CBitmap* pbmOld2 = memDC2.SelectObject(pBitmap);

	memDC.BitBlt(0,0,bmSrc.bmWidth, bmSrc.bmHeight,&memDC2,0,0,SRCCOPY);

	// get the bitmap size
	BITMAP bm;
	m_bmBkImg.GetBitmap( &bm );
	m_cx = bm.bmWidth;
	m_cy = bm.bmHeight;
	
	if (GetSafeHwnd() && ::IsWindow(GetSafeHwnd()))
		InvalidateRect(NULL, FALSE);

	memDC.SelectObject(pbmOld);
	memDC2.SelectObject(pbmOld2);
	ReleaseDC(&memDC2);
	ReleaseDC(&memDC);
	return TRUE;
}

BOOL CCheckListCtrl::_SetBkImage(LPCTSTR lpszName, BOOL bFromFile)
{
	if (m_bmBkImg.GetSafeHandle())
		m_bmBkImg.DeleteObject();

	m_cx = m_cy = 0;

	if (lpszName)
	{
		// setting flags
		UINT nFlag = LR_CREATEDIBSECTION;
		if (bFromFile)
			nFlag |= LR_LOADFROMFILE;

//		HBITMAP hbmTemp = (HBITMAP)::LoadImage( AfxGetResourceHandle(), lpszName, IMAGE_BITMAP, 0, 0, nFlag);
		HBITMAP hbmTemp = NULL;
		CxImage image;
		if(image.Load(lpszName))
		{
			hbmTemp = image.MakeBitmap();
		}

		if (hbmTemp)
		{
			// get the bitmap size
			m_bmBkImg.Attach(hbmTemp);
			BITMAP bm;
			m_bmBkImg.GetBitmap( &bm );
			m_cx = bm.bmWidth;
			m_cy = bm.bmHeight;
		}
	}

	if (GetSafeHwnd() && ::IsWindow(GetSafeHwnd()))
		InvalidateRect(NULL);

	return TRUE;

}

void CCheckListCtrl::OnPaint() 
{
	if(!m_bmBkImg.GetSafeHandle())
	{	// 로드된 비트맵이 없으면
		CListCtrl::OnPaint();
		return;
	}


	CRect rcClip, rcClient;
	GetClientRect(&rcClient);

	int x = rcClient.Width()-m_cx;
	int y = rcClient.Height()-m_cy;
	x = max(x, 0); y = max(y, 0);

	// 이걸 사용하면 그림이 같이 스크롤된다.
	// 이 때는 OnHScroll과 OnVScroll에서 Invalidate할 필요가 별로 없다
	// 주의!! 뭔가 잘 안된다. -_-;;; 가급적이면 쓰지 말 것.

	if(m_bScrollBkImg)
	{
		SCROLLINFO hinfo = { sizeof(SCROLLINFO) };
		GetScrollInfo(SB_HORZ, &hinfo, SIF_ALL);
		SCROLLINFO vinfo = { sizeof(SCROLLINFO) };
		GetScrollInfo(SB_VERT, &vinfo, SIF_ALL);
		x -= (int)(rcClient.Width() * ((double)hinfo.nPos/hinfo.nPage));
		y -= (int)(rcClient.Height() * ((double)vinfo.nPos/vinfo.nPage));

		if (((x+m_cx)<0) || ((y+m_cy)<0)) {
			CListCtrl::OnPaint();
			return;
		}
	}
/**/


	CPaintDC dc(this);
	dc.GetClipBox(&rcClip);

	COLORREF clrBack = GetBkColor();
	if (clrBack == (COLORREF)-1)
		clrBack = ::GetSysColor(COLOR_WINDOW);



	// step 1. 메모리 DC를 만들어서 원래 윈도우 이미지를 그 위에 그리게 한다
	CDC memDC;
	CBitmap bitmap;
	memDC.CreateCompatibleDC(&dc);
	bitmap.CreateCompatibleBitmap(&dc, rcClient.Width(), rcClient.Height());
	CBitmap* pbmOld1 = memDC.SelectObject(&bitmap);

	HPEN basePen, oldPen;
	basePen = CreatePen(PS_SOLID, 1, clrBack);
	oldPen = (HPEN)SelectObject(memDC.m_hDC,basePen);
	
	HBRUSH baseBrush, oldBrush;
	baseBrush = CreateSolidBrush(clrBack);
	oldBrush = (HBRUSH)SelectObject(memDC.m_hDC,baseBrush);

	Rectangle(memDC.m_hDC,0,0,rcClient.Width(),rcClient.Height());
	DeleteObject(SelectObject(memDC.m_hDC,oldPen));
	DeleteObject(SelectObject(memDC.m_hDC,oldBrush));


	// 이게 된다는게 놀라운데... -_-;
	CWnd::DefWindowProc(WM_PAINT, (WPARAM)memDC.m_hDC, 0);

	if(GetStyle()&LVS_REPORT)
	{	
		CHeaderCtrl* pHeader=GetHeaderCtrl();
		CRect rc; CString a;
		pHeader->GetItemRect(0,&rc);

		memDC.BitBlt(0,0,rcClient.Width(),rc.Height(),&dc,0,0,SRCCOPY);
	}



	// step 2. 메모리 DC를 또 만들고 1의 결과물에서 마스크 이미지를 뜬다.
	CDC maskDC;
	CBitmap maskBitmap;
	maskDC.CreateCompatibleDC(&dc);
	maskBitmap.CreateBitmap(rcClient.Width(), rcClient.Height(), 1, 1, NULL);	// 흑백
	CBitmap* pbmOld2 = maskDC.SelectObject(&maskBitmap);

	// 마스크 만들기
	COLORREF crOld1 = memDC.SetBkColor(clrBack);
	COLORREF crOld2 = memDC.SetTextColor(RGB(0, 0, 0));
	maskDC.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &memDC, rcClip.left, rcClip.top, SRCCOPY);
	memDC.SetBkColor(RGB(0, 0, 0));        
	memDC.SetTextColor(RGB(255, 255, 255));
	// 흰색을 검은색으로 바꾼다
	memDC.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &maskDC, rcClip.left, rcClip.top, SRCAND);
	memDC.SetTextColor(crOld2);
	memDC.SetBkColor(crOld1);



	// step 3. 메모리 DC를 하나 더 만들고 배경 비트맵을 준비한다.
	CDC imgDC;
	imgDC.CreateCompatibleDC(&dc);
	CBitmap* pbmOld3 = imgDC.SelectObject(&m_bmBkImg);

	// step 4. 깜박임을 막기 위해서 DC를 하나 더 만든다. -_-;;;
	CDC mixDC;
	CBitmap bmpImage;
	mixDC.CreateCompatibleDC(&dc);
	bmpImage.CreateCompatibleBitmap(&dc, rcClient.Width(), rcClient.Height());
	CBitmap* pbmOld4 = mixDC.SelectObject(&bmpImage);

	mixDC.FillSolidRect(0, 0, rcClient.Width(), rcClient.Height(), clrBack);
	mixDC.BitBlt(x, y, m_cx, m_cy, &imgDC, 0, 0, SRCCOPY);
	crOld1 = mixDC.SetTextColor(RGB(0, 0, 0));
	crOld2 = mixDC.SetBkColor(RGB(255, 255, 255));
	mixDC.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &maskDC, rcClip.left, rcClip.top, SRCAND);
	mixDC.SetBkColor(crOld2);
	mixDC.SetTextColor(crOld1);
	mixDC.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &memDC, rcClip.left, rcClip.top, SRCPAINT);
	dc.BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &mixDC, rcClip.left, rcClip.top, SRCCOPY);



	mixDC.SelectObject(pbmOld4);
	imgDC.SelectObject(pbmOld3);
	maskDC.SelectObject(pbmOld2);
	memDC.SelectObject(pbmOld1);
}
/*
BOOL CCheckListCtrl::OnEraseBkgnd(CDC* pDC) 
{
	// 배경 비트맵이 있다면 OnPaint에서 다 처리한다.
	if (m_bmBkImg.GetSafeHandle())
		return TRUE;

	return CListCtrl::OnEraseBkgnd(pDC);
}
*/
void CCheckListCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (m_bmBkImg.GetSafeHandle())
		InvalidateRect(NULL, FALSE);

	CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CCheckListCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (m_bmBkImg.GetSafeHandle())
		InvalidateRect(NULL, FALSE);

	CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CCheckListCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	InvalidateRect(NULL, FALSE);

	CListCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CCheckListCtrl::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	InvalidateRect(NULL, FALSE);

	CListCtrl::OnKeyUp(nChar, nRepCnt, nFlags);
}

BOOL CCheckListCtrl::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	if (m_bmBkImg.GetSafeHandle())
		InvalidateRect(NULL, FALSE);

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}

void CCheckListCtrl::OnSize(UINT nType, int cx, int cy)
{
	if (m_bmBkImg.GetSafeHandle())
		InvalidateRect(NULL, FALSE);

	CListCtrl::OnSize(nType, cx, cy);
}

void CCheckListCtrl::OnHdnEndtrack(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	m_bLButton = false;
}

void CCheckListCtrl::OnHdnBegintrack(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	m_bLButton = true;
}

/*
void CCheckListCtrl::PreSubclassWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CListCtrl::PreSubclassWindow();

//	VERIFY( m_checkHeadCtrl.SubclassWindow( GetHeaderCtrl()->GetSafeHwnd() ) );
}
*/

int CCheckListCtrl::GetColumnCount()
{
	CHeaderCtrl* pHeaderCtrl = GetHeaderCtrl();
	return (pHeaderCtrl->GetItemCount());
}

void CCheckListCtrl::AdjustColumnWidth()
{
	SetRedraw(FALSE);
	int nColumnCount = GetColumnCount();

	for (int i = 0; i < nColumnCount; i++)
	{
		SetColumnWidth(i, LVSCW_AUTOSIZE);
		int nColumnWidth = GetColumnWidth(i);
		SetColumnWidth(i, LVSCW_AUTOSIZE_USEHEADER);
		int nHeaderWidth = GetColumnWidth(i); 
		SetColumnWidth(i, max(nColumnWidth, nHeaderWidth));
	}
	SetRedraw(TRUE);

}
