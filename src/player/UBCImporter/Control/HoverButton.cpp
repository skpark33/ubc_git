// HoverButton.cpp : implementation file
//

#include "stdafx.h"
#include "HoverButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHoverButton

CHoverButton::CHoverButton()
{
	m_pParentWnd = NULL;

	m_bHover = FALSE;
	m_bTracking = FALSE;

	m_transparentColor = RGB(255,255,255);
}

CHoverButton::~CHoverButton()
{
}

IMPLEMENT_DYNAMIC(CHoverButton, CBitmapButton)

BEGIN_MESSAGE_MAP(CHoverButton, CBitmapButton)
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_MESSAGE(WM_MOUSEHOVER, OnMouseHover)
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////
 //	CHoverButton message handlers
		
void CHoverButton::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (!m_bTracking)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE|TME_HOVER;
		tme.dwHoverTime = 1;
		m_bTracking = _TrackMouseEvent(&tme);
	}

	CBitmapButton::OnMouseMove(nFlags, point);
}

BOOL CHoverButton::PreTranslateMessage(MSG* pMsg) 
{
	InitToolTip();
	m_ToolTip.RelayEvent(pMsg);

	return CButton::PreTranslateMessage(pMsg);
}

// Set the tooltip with a string resource
void CHoverButton::SetToolTipText(int nId, BOOL bActivate)
{
	CString sText;

	// load string resource
	sText.LoadString(nId);
	// If string resource is not empty
	if (sText.IsEmpty() == FALSE) SetToolTipText(sText, bActivate);
}

// Set the tooltip with a CString
void CHoverButton::SetToolTipText(LPCTSTR lpszTT, BOOL bActivate)
{
	// We cannot accept NULL pointer
	if (lpszTT == NULL) return;

	// Initialize ToolTip
	InitToolTip();

	// If there is no tooltip defined then add it
	if (m_ToolTip.GetToolCount() == 0)
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_ToolTip.AddTool(this, lpszTT, rectBtn, 1);
	}

	// Set text for tooltip
	m_ToolTip.UpdateTipText(lpszTT, this, 1);
	m_ToolTip.Activate(bActivate);
}

void CHoverButton::InitToolTip()
{
	if (m_ToolTip.m_hWnd == NULL)
	{
		// Create ToolTip control
		m_ToolTip.Create(this);
		// Create inactive
		m_ToolTip.Activate(FALSE);
	}
}

// Activate the tooltip
void CHoverButton::ActivateTooltip(BOOL bActivate)
{
	// If there is no tooltip then do nothing
	if (m_ToolTip.GetToolCount() == 0) return;

	// Activate tooltip
	m_ToolTip.Activate(bActivate);
}

void CHoverButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	CDC *mydc=CDC::FromHandle(lpDrawItemStruct->hDC);

	CDC * pMemDC = new CDC;
	pMemDC -> CreateCompatibleDC(mydc);

	CBitmap * pOldBitmap;
	pOldBitmap = pMemDC -> SelectObject(&m_bitmapHover);
	
	CPoint point(0,0);	

	// [-- modified by socket7
	if(GetStyle() & WS_DISABLED)
	{
		mydc->TransparentBlt(0,0,m_ButtonSize.cx,m_ButtonSize.cy, pMemDC, m_ButtonSize.cx*3,0,m_ButtonSize.cx,m_ButtonSize.cy, m_transparentColor);
	}
	else if(lpDrawItemStruct->itemState & ODS_SELECTED)
	{
//		mydc->BitBlt(0,0,m_ButtonSize.cx,m_ButtonSize.cy,pMemDC,m_ButtonSize.cx,0,SRCCOPY); // modified by socket7
		//mydc->TransparentBlt(0,0,m_ButtonSize.cx,m_ButtonSize.cy, pMemDC, m_ButtonSize.cx, 0, m_ButtonSize.cx,m_ButtonSize.cy, m_transparentColor);
		mydc->TransparentBlt(0,0,m_ButtonSize.cx,m_ButtonSize.cy, pMemDC, m_ButtonSize.cx*2, 0, m_ButtonSize.cx,m_ButtonSize.cy, m_transparentColor);
	} 
	else
	{
		if(m_bHover)
		{
//			mydc->BitBlt(0,0,m_ButtonSize.cx,m_ButtonSize.cy,pMemDC,m_ButtonSize.cx*2,0,SRCCOPY); // modified by socket7
			//mydc->TransparentBlt(0,0,m_ButtonSize.cx,m_ButtonSize.cy, pMemDC, m_ButtonSize.cx*2, 0, m_ButtonSize.cx,m_ButtonSize.cy, m_transparentColor);
			mydc->TransparentBlt(0,0,m_ButtonSize.cx,m_ButtonSize.cy, pMemDC, m_ButtonSize.cx, 0, m_ButtonSize.cx,m_ButtonSize.cy, m_transparentColor);
		}else
		{
//			mydc->BitBlt(0,0,m_ButtonSize.cx,m_ButtonSize.cy,pMemDC,0,0,SRCCOPY); // modified by socket7
			mydc->TransparentBlt(0,0,m_ButtonSize.cx,m_ButtonSize.cy, pMemDC, 0, 0, m_ButtonSize.cx,m_ButtonSize.cy, m_transparentColor);
		}	
	}

	// clean up
	pMemDC -> SelectObject(pOldBitmap);
	delete pMemDC;
}

// Load a bitmap from the resources in the button, the bitmap has to have 3 buttonsstates next to each other: Up/Down/Hover
BOOL CHoverButton::LoadBitmap(UINT bitmapid, COLORREF transparentColor)
{
	ModifyStyle(0, BS_OWNERDRAW);

	m_transparentColor = transparentColor;

	m_bitmapHover.DeleteObject();
	m_bitmapHover.LoadBitmap(bitmapid);

	BITMAP bitmapbits;
	m_bitmapHover.GetBitmap(&bitmapbits);

	m_ButtonSize.cy=bitmapbits.bmHeight;
	m_ButtonSize.cx=bitmapbits.bmWidth/3;

	// Disable 상태의 버튼 이미지를 기존 비트맵에 추가한다
	int nUnitBytes = (bitmapbits.bmWidthBytes/3);

	DWORD dwCurrSize = bitmapbits.bmWidthBytes * bitmapbits.bmHeight;  // 현재비트맵의 버퍼
	DWORD dwUnitSize = nUnitBytes * bitmapbits.bmHeight;  // 그레이 이미지에 필요한 버퍼
	DWORD dwTotSize = dwCurrSize + dwUnitSize;

	BYTE* pCurrBuff = new BYTE[dwCurrSize];
	memset(pCurrBuff, 0x00, sizeof(pCurrBuff));

	BYTE* pTotBuff = new BYTE[dwTotSize];
	memset(pTotBuff, 0x00, sizeof(pTotBuff));

	// 현재비트맵을 구한다
	DWORD dwRead = m_bitmapHover.GetBitmapBits( dwCurrSize, pCurrBuff );  // pBuff에 bitmap 데이터 읽기

	for(int nHeight=0; nHeight<bitmapbits.bmHeight ; nHeight++)
	{
		memcpy(pTotBuff  + ((bitmapbits.bmWidthBytes+nUnitBytes) * nHeight)
			 , pCurrBuff + (bitmapbits.bmWidthBytes * nHeight)
			 , bitmapbits.bmWidthBytes
			 );

		BYTE* pGrayBuff = pTotBuff  + ((bitmapbits.bmWidthBytes+nUnitBytes) * nHeight) + bitmapbits.bmWidthBytes;
		BYTE* pColrBuff = pCurrBuff + (bitmapbits.bmWidthBytes * nHeight);
//		memcpy(pGrayBuff, pColrBuff, nUnitBytes);

		for(int i=0; i<nUnitBytes ; i+=4)
		{
			BYTE gray = long(pColrBuff[i] * 0.3 + pColrBuff[i+1] * 0.59 + pColrBuff[i+2] * 0.11);
//			BYTE gray = (pColrBuff[i] + pColrBuff[i+1] + pColrBuff[i+2]) / 3;
			if(gray > 230) gray = 230;
			if(gray < 120) gray = 120;

			pGrayBuff[i  ] = gray;  // blue 
			pGrayBuff[i+1] = gray;  // green 
			pGrayBuff[i+2] = gray;  // red 
		}
	}

	m_bitmapHover.DeleteObject();
	m_bitmapHover.CreateBitmap(bitmapbits.bmWidth + m_ButtonSize.cx, bitmapbits.bmHeight, bitmapbits.bmPlanes, bitmapbits.bmBitsPixel, pTotBuff);

	delete [] pCurrBuff;
	delete [] pTotBuff;

	SetWindowPos(
		NULL,
		0,
		0,
		m_ButtonSize.cx,
		m_ButtonSize.cy,
		SWP_NOMOVE | SWP_NOOWNERZORDER
	);

	return TRUE;
}

LRESULT CHoverButton::OnMouseHover(WPARAM wparam, LPARAM lparam) 
{
	m_bHover=TRUE;
	Invalidate();

	// 버튼이 한번 눌리고 나면 툴팁이 사라지기 때문에 마우스 호버 이벤트시 강제로 툴팁을 보여주도록 함
	m_ToolTip.Pop();
	return 0;
}

LRESULT CHoverButton::OnMouseLeave(WPARAM wparam, LPARAM lparam)
{
	m_bTracking = FALSE;
	m_bHover=FALSE;
	Invalidate();
	return 0;
}

void CHoverButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	CBitmapButton::OnLButtonUp(nFlags, point);

	if(m_pParentWnd) m_pParentWnd->SendMessage(WM_COMMAND, m_command);
}
