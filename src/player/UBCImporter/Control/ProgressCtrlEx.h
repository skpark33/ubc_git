/*******************************************/
/*                                         */
/* - title : CProgressCtrlEx.h                 */
/* - description : ProgressCtrl Extends    */
/* - author : you !                        */
/* - date : 2002-09-13                     */
/*                                         */
/*******************************************/

#pragma once
#include <afxcmn.h>		

#define PROGRESS_NONE	1
#define PROGRESS_TEXT	2
#define PROGRESS_BITMAP	3

class CProgressCtrlEx : public CProgressCtrl
{
public:
	CProgressCtrlEx();
	~CProgressCtrlEx();

	enum MOUSE_TYPE { 
		//MOUSE_IDLE = -1,
		MOUSE_IDLE = 0,
		MOUSE_OVER_LEFT_TOP,
		MOUSE_OVER_TOP,
		MOUSE_OVER_RIGHT_TOP,
		MOUSE_OVER_LEFT,
		MOUSE_OVER_CENTER,
		MOUSE_OVER_RIGHT,
		MOUSE_OVER_LEFT_BOTTOM,
		MOUSE_OVER_BOTTOM,
		MOUSE_OVER_RIGHT_BOTTOM,
		MOUSE_SIZING_LEFT_TOP,
		MOUSE_SIZING_TOP,
		MOUSE_SIZING_RIGHT_TOP,
		MOUSE_SIZING_LEFT,
		MOUSE_SIZING_CENTER,
		MOUSE_SIZING_RIGHT,
		MOUSE_SIZING_LEFT_BOTTOM,
		MOUSE_SIZING_BOTTOM,
		MOUSE_SIZING_RIGHT_BOTTOM,
	};

public:
	int SetPos(int nPos);
	void SetRange(int nLower, int nUpper);
	int SetStep(int nStep);
	int StepIt();
	void SetTextBkColor(COLORREF clrTextBk);
	void SetTextForeColor(COLORREF clrTextFore);
	void SetBkColor(COLORREF clrBk);
	void SetForeColor(COLORREF clrFore);
	void SetTextColor(COLORREF clrText);

	void SetStyle(int nStyle);
	void SetText(CString strText);
	void SetBitmap(int nId);

	// 임계영역 표시 및 조정
	void VisibleThreshold(bool bVisible);
	bool GetThresholdVisible();
	void EnableThreshold(bool bEnable);
	bool GetThresholdEnable();
	void SetThreshole(int nThreshold);
	void SetThresholeMinMax(int nMin, int nMax);
	int  GetThreshold();
	void SetThresholdColor(COLORREF clrThreshold);
	COLORREF GetThresholdColor();

protected:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	CFont	m_font;

	DECLARE_MESSAGE_MAP()

private:
	CBitmap m_Bitmap;
	int m_nHeight;
	int m_nWidth;

	int m_nMin;
	int m_nMax;

	int m_nPos;
	int m_nStep;

	int m_nStyle;

	COLORREF m_clrBk;
	COLORREF m_clrFore;
	COLORREF m_clrTextBk;
	COLORREF m_clrTextFore;
	COLORREF m_clrText;

	CString m_strText;

	bool m_bThresholdEditing;
	MOUSE_TYPE m_MouseType;

	// 임계영역 표시 및 조정
	bool m_bThresholdVisible;
	bool m_bThresholdEnable;
	int  m_nThreshold;
	int  m_nThresholdMin;
	int  m_nThresholdMax;
	COLORREF m_clrThreshold;

	BOOL SetCursor(MOUSE_TYPE mouseType);

public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
