// CheckHeadCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "CheckHeadCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCheckHeadCtrl

CCheckHeadCtrl::CCheckHeadCtrl()
{
	m_bCheckBoxType = false;
	m_iSortColumn = -1;
	m_bSortAscending = true;
}

CCheckHeadCtrl::~CCheckHeadCtrl()
{
}


BEGIN_MESSAGE_MAP(CCheckHeadCtrl, CHeaderCtrl)
	ON_NOTIFY_REFLECT_EX(HDN_ITEMCLICK, OnItemClicked)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCheckHeadCtrl message handlers

BOOL CCheckHeadCtrl::OnItemClicked(NMHDR* pNMHDR, LRESULT* pResult)
{
	NMHEADER* pNMHead = (NMHEADER*)pNMHDR;
	*pResult = 0;

	int nItem = pNMHead->iItem;
	if (0 != nItem || !m_bCheckBoxType)
		return FALSE;

	HDITEM hdItem;
	hdItem.mask = HDI_IMAGE;

	VERIFY( GetItem(nItem, &hdItem) );

	if (hdItem.iImage == 1)
		hdItem.iImage = 0;
	else
		hdItem.iImage = 1;

	VERIFY( SetItem(nItem, &hdItem) );

	// check all (select/deselect)
	BOOL bl = hdItem.iImage == 1 ? TRUE : FALSE;
	CListCtrl* pListCtrl = (CListCtrl*)GetParent();
	int nCount = pListCtrl->GetItemCount();	
	for(nItem = 0; nItem < nCount; nItem++)
	{
		ListView_SetCheckState(pListCtrl->GetSafeHwnd(), nItem, bl);
	}

	return TRUE;
}

void CCheckHeadCtrl::SetSortArrow( const int iSortColumn )
{
	if(m_iSortColumn == iSortColumn)
		m_bSortAscending = !m_bSortAscending;
	else
	{
		if(m_iSortColumn >= 0)
		{
			HDITEM    itemOld;
			itemOld.mask = HDI_FORMAT;
			GetItem(m_iSortColumn, &itemOld);
			itemOld.fmt &= ~(HDF_IMAGE);
			SetItem(m_iSortColumn, &itemOld);
		}

		m_bSortAscending = TRUE;
	}

	m_iSortColumn = iSortColumn;
//	m_bSortAscending = bSortAscending;

#define USE_BITMAP_HEADER

#ifndef USE_BITMAP_HEADER

	// change the item to owner drawn.
	HD_ITEM hditem;
	hditem.mask = HDI_FORMAT;
//	VERIFY( GetItem( iSortColumn, &hditem ) );
	GetItem( iSortColumn, &hditem );
	hditem.fmt |= HDF_OWNERDRAW;
//	VERIFY( SetItem( iSortColumn, &hditem ) );
	SetItem( iSortColumn, &hditem );

#else

	HDITEM    itemNew;
	itemNew.mask = HDI_FORMAT | HDI_IMAGE;
	GetItem(m_iSortColumn, &itemNew);
	itemNew.mask = HDI_FORMAT | HDI_IMAGE;
	itemNew.iImage = (m_bSortAscending == true) ? 2 : 3;
	itemNew.fmt = itemNew.fmt | HDF_IMAGE | HDF_BITMAP_ON_RIGHT;
	SetItem(m_iSortColumn, &itemNew);

#endif
	// invalidate the header control so it gets redrawn
	Invalidate();
}
//
//void CCheckHeadCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
//{
//	// attath to the device context.
//	CDC dc;
//	VERIFY( dc.Attach( lpDrawItemStruct->hDC ) );
//
//	// save the device context.
//	const int iSavedDC = dc.SaveDC();
//
//	// get the column rect.
//	CRect rc( lpDrawItemStruct->rcItem );
//
//	// set the clipping region to limit drawing within the column.
//	CRgn rgn;
//	VERIFY( rgn.CreateRectRgnIndirect( &rc ) );
//	(void)dc.SelectObject( &rgn );
//	VERIFY( rgn.DeleteObject() );
//
//	// draw the background,
//	CBrush brush( GetSysColor( COLOR_3DFACE ) );
//	dc.FillRect( rc, &brush );
//
//	// get the column text and format.
//	TCHAR szText[ 256 ];
//	HD_ITEM hditem;
//
//	hditem.mask = HDI_TEXT | HDI_FORMAT;
//	hditem.pszText = szText;
//	hditem.cchTextMax = 255;
//
//	VERIFY( GetItem( lpDrawItemStruct->itemID, &hditem ) );
//
//	// determine the format for drawing the column label.
//	UINT uFormat = DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER | DT_END_ELLIPSIS ;
//
//	if( hditem.fmt & HDF_CENTER)
//		uFormat |= DT_CENTER;
//	else if( hditem.fmt & HDF_RIGHT)
//		uFormat |= DT_RIGHT;
//	else
//		uFormat |= DT_LEFT;
//
//	// adjust the rect if the mouse button is pressed on it.
//	if( lpDrawItemStruct->itemState == ODS_SELECTED )
//	{
//		rc.left++;
//		rc.top += 2;
//		rc.right++;
//	}
//
//	CRect rcIcon( lpDrawItemStruct->rcItem );
//	const int iOffset = ( rcIcon.bottom - rcIcon.top ) / 4;
//
//	// adjust the rect further if the sort arrow is to be displayed.
//	if( lpDrawItemStruct->itemID == (UINT)m_iSortColumn )
//		rc.right -= 3 * iOffset;
//
//	rc.left += iOffset;
//	rc.right -= iOffset;
//
//	// draw the column label.
//	if( rc.left < rc.right )
//	{
//		dc.SetBkMode(TRANSPARENT);
//		(void)dc.DrawText( szText, -1, rc, uFormat );
//	}
//
//	// draw the sort arrow.
//	if( lpDrawItemStruct->itemID == (UINT)m_iSortColumn )
//	{
//		// set up the pens to use for drawing the arrow.
//		CPen penLight( PS_SOLID, 1, GetSysColor( COLOR_3DHILIGHT ) );
//		CPen penShadow( PS_SOLID, 1, GetSysColor( COLOR_3DSHADOW ) );
//		CPen* pOldPen = dc.SelectObject( &penLight );
//
//#define GAP	1
//
//		if( m_bSortAscending )
//		{
//			//// draw the arrow pointing upwards.
//			//dc.MoveTo( rcIcon.right - 2 * iOffset -GAP, iOffset);
//			//dc.LineTo( rcIcon.right - iOffset -GAP, rcIcon.bottom - iOffset - 1 );
//			//dc.LineTo( rcIcon.right - 3 * iOffset - 2 -GAP, rcIcon.bottom - iOffset - 1 );
//			//(void)dc.SelectObject( &penShadow );
//			//dc.MoveTo( rcIcon.right - 3 * iOffset - 1 -GAP, rcIcon.bottom - iOffset - 1 );
//			//dc.LineTo( rcIcon.right - 2 * iOffset -GAP, iOffset - 1);		
//			CImageList* pImageList = GetImageList();
//			pImageList->Draw(&dc, 2, CPoint(rcIcon.right - 16, rcIcon.top), ILD_NORMAL);
//		}
//		else
//		{
//			//// draw the arrow pointing downwards.
//			//dc.MoveTo( rcIcon.right - iOffset - 1 -GAP, iOffset );
//			//dc.LineTo( rcIcon.right - 2 * iOffset - 1 -GAP, rcIcon.bottom - iOffset );
//			//(void)dc.SelectObject( &penShadow );
//			//dc.MoveTo( rcIcon.right - 2 * iOffset - 2 -GAP, rcIcon.bottom - iOffset );
//			//dc.LineTo( rcIcon.right - 3 * iOffset - 1 -GAP, iOffset );
//			//dc.LineTo( rcIcon.right - iOffset - 1 -GAP, iOffset );		
//			CImageList* pImageList = GetImageList();
//			pImageList->Draw(&dc, 3, CPoint(rcIcon.right - 16, rcIcon.top), ILD_NORMAL);
//		}
//
//		// restore the pen.
//		(void)dc.SelectObject( pOldPen );
//	}
//
//	// restore the previous device context.
//	VERIFY( dc.RestoreDC( iSavedDC ) );
//
//	// detach the device context before returning.
//	(void)dc.Detach();
//}
//
