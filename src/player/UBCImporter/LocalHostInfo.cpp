/************************************************************************************/
/*! @file LocalHostInfo.cpp
	@brief Local system의 존재하는 host 정보를 갖는 객체 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/01/28\n
	▶ 사용파일: "staafx.h", "LocalHostInfo.h"\n
	
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2009/01/28:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"
#include "LocalHostInfo.h"





/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CLocalHostInfo::CLocalHostInfo()
:	m_strDrive(_T(""))
//,	m_strNetworkUse(_T(""))
,	m_strHostName(_T(""))
,	m_strDescription(_T(""))
//,	m_strSite(_T(""))
,	m_strLastWrittenTime(_T(""))
//,	m_strLastWrittenUser(_T(""))
{
	
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CLocalHostInfo::~CLocalHostInfo()
{
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// local host 정보를 설정 \n
/// @param (CString) strDrive : (in) host 정보가 존재하는 local system 드라이브 문자열
/// @param (CString) strNetworkUse : (in) Network use 여부(BRW에서 만든 ini 파일은 1, Studio에서 만든 ini 파일은 0)
/// @param (CString) strHostName : (in) local system에  존재하는 host 이름
/// @param (CString) strDescription : (in) Host에 대한 설명
/// @param (CString) strSite : (in) Host의 site 명
/// @param (CString) strLastWrittenTime : (in) 마지막으로 저장된 시간
/////////////////////////////////////////////////////////////////////////////////
void CLocalHostInfo::SetLocalHostInfo(CString strDrive, CString strNetworkUse,
										CString strHostName, CString strDescription,
										CString strSite, CString strLastWrittenTime,
										CString strLastWrittenUser)
{
	m_strDrive				= strDrive;
	m_strNetworkUse			= strNetworkUse;
	m_strHostName			= strHostName;
	m_strDescription		= strDescription;
	m_strSite				= strSite;
	m_strLastWrittenTime	= strLastWrittenTime;
	m_strLastWrittenUser	= strLastWrittenUser;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// local host 정보를 반환 \n
/// @param (CString&) strDrive : (in) host 정보가 존재하는 local system 드라이브 문자열
/// @param (CString&) strNetworkUse : (in) Network use 여부(BRW에서 만든 ini 파일은 1, Studio에서 만든 ini 파일은 0)
/// @param (CString&) strHostName : (in) local system에  존재하는 host 이름
/// @param (CString&) strDescription : (in) Host에 대한 설명
/// @param (CString&) strSite : (in) Host의 site 명
/// @param (CString&) strLastWrittenTime : (in) 마지막으로 저장된 시간
/////////////////////////////////////////////////////////////////////////////////
void CLocalHostInfo::GetLocalHostInfo(CString& strDrive, CString& strNetworkUse,
										CString& strHostName, CString& strDescription,
										CString& strSite, CString& strLastWrittenTime,
										CString& strLastWrittenUser)
{
	strDrive			= m_strDrive;
	strNetworkUse		= m_strNetworkUse;
	strHostName			= m_strHostName;
	strDescription		= m_strDescription;
	strSite				= m_strSite;
	strLastWrittenTime	= m_strLastWrittenTime;
	strLastWrittenUser  = m_strLastWrittenUser;
}
*/