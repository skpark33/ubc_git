//////////////////////////////////////////////////////////////////////////
//
// 컨텐츠 명과 화일명으로 사용할수 없는 특수문자는 다음과 같다.
// 1) 원래 MS 윈도우즈에서 파일명으로 사용할 수 없는 문자
// <>\/*|":
// 2) 추가적으로 사용할 수 없는 문자
// [],!%`' 
//
// [],<> 는 괄호로 치환하고, 나머지는 모두 언더스코어로 치환한다.
//
//////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "PreventChar.h"

CPreventChar::CPreventChar(void)
{
	m_astrPreventChar.Add("[" );	m_astrConvertChar.Add("(");
	m_astrPreventChar.Add("]" );	m_astrConvertChar.Add(")");
	m_astrPreventChar.Add("<" );	m_astrConvertChar.Add("(");
	m_astrPreventChar.Add(">" );	m_astrConvertChar.Add(")");
	m_astrPreventChar.Add("," );	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add("!" );	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add("%" );	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add("`" );	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add("'" );	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add("\\");	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add("/" );	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add("*" );	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add("|" );	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add("\"");	m_astrConvertChar.Add("_");
	m_astrPreventChar.Add(":" );	m_astrConvertChar.Add("_");
}

CPreventChar::~CPreventChar(void)
{
}

CPreventChar* CPreventChar::_instance;
CPreventChar* CPreventChar::GetInstance()
{
	if(_instance == NULL) _instance = new CPreventChar();
	return _instance;
}

CString CPreventChar::GetPreventChar()
{
	CString strResult;
	for(int i=0; i<m_astrPreventChar.GetCount() ;i++)
	{
		if(!strResult.IsEmpty()) strResult += " ";
		strResult += m_astrPreventChar[i];
	}

	return strResult;
}

bool CPreventChar::IsPreventChar(CString strValue)
{
	for(int i=0; i<m_astrPreventChar.GetCount() ;i++)
	{
		if(m_astrPreventChar[i] == strValue) return true;
	}

	return false;
}

bool CPreventChar::IsPreventChar(TCHAR szValue)
{
	return IsPreventChar(CString(szValue));
}

bool CPreventChar::HavePreventChar(CString strValue)
{
	for(int i=0; i<strValue.GetLength() ;i++)
	{
		if(IsPreventChar(strValue[i])) return true;
	}

	return false;
}

CString CPreventChar::ConvertToAvailableChar(CString strValue)
{
	if(!HavePreventChar(strValue)) return strValue;

	CString strResult = strValue;

	for(int i=0; i<m_astrPreventChar.GetCount() ;i++)
	{
		strResult.Replace(m_astrPreventChar[i], m_astrConvertChar[i]);
	}

	return strResult;
}
