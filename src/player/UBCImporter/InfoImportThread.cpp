// InfoImportThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCImporter.h"
#include "InfoImportThread.h"

#include "UBCImporterDlg.h"
#include "DataContainer.h"


// CInfoImportThread

IMPLEMENT_DYNCREATE(CInfoImportThread, CWinThread)

CInfoImportThread::CInfoImportThread()
:	m_pParent(NULL)
,	m_strDrive(_T(""))
,	m_strHost(_T(""))
{

}

CInfoImportThread::~CInfoImportThread()
{
}

BOOL CInfoImportThread::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CInfoImportThread::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CInfoImportThread, CWinThread)
END_MESSAGE_MAP()


// CInfoImportThread 메시지 처리기입니다.

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Set parent params \n
/// @param (void*) pParent : (in) thread owner window pointer
/// @param (CString) strDrive : (in) import 하려는 host 정보가 있는 드라이브
/// @param (CString) strHost : (in) import 하려는 host 이름
/////////////////////////////////////////////////////////////////////////////////
void CInfoImportThread::SetThreadParam(void* pParent, CString strDrive, CString strHost)
{
	m_pParent = pParent;
	m_strDrive = strDrive;
	m_strHost = strHost;
}

int CInfoImportThread::Run()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CUBCImporterDlg* pParent = (CUBCImporterDlg*)m_pParent;
	CDataContainer* pCon = CDataContainer::getInstance();
	CString strPath = m_strDrive;
	strPath.Append(IMPORT_CONFIG_PATH);
	strPath.Append(m_strHost);
	strPath.Append(".ini");

	bool bRet = pCon->Load(strPath);
	if(!bRet)
	{
		pParent->PostMessage(WM_INFOIMPORT_COMPELETE, (WPARAM)bRet, 0);
	}

	// location 수정
	CString strRootPath = m_strDrive;
	strRootPath.Append(IMPORT_ROOT_PATH);

	CONTENTS_INFO_MAP* mapContents = CDataContainer::getInstance()->GetContentsMap();
	POSITION pos = mapContents->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsID;
		CONTENTS_INFO* infoContents;
		mapContents->GetNextAssoc(pos, strContentsID, (void*&)infoContents);

		infoContents->strLocation.Insert(0, strRootPath);
	}

	pParent->PostMessage(WM_INFOIMPORT_COMPELETE, (WPARAM)bRet, 0);

	return 0;
}
