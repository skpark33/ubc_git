#ifndef _serialSocketAcceptor_h_
#define _serialSocketAcceptor_h_

#include <ace/SOCK_Acceptor.h>
#include <ace/Acceptor.h>
#include "serialGW/serialSocketSession.h"

class serialSocketAcceptor : public ACE_Acceptor<serialSocketSession, ACE_SOCK_ACCEPTOR> {
public:
	serialSocketAcceptor();
	virtual ~serialSocketAcceptor();
		

    virtual int make_svc_handler(serialSocketSession*& pSvcHandler);
    virtual int activate_svc_handler(serialSocketSession* pSvcHandler);

protected:
	typedef ACE_Acceptor<serialSocketSession, ACE_SOCK_ACCEPTOR> inherited;
};

#endif
