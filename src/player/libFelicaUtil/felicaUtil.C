#include "ci/libDebug/ciDebug.h"
#include "common/libCommon/ubcIni.h"
#include "felicaUtil.h"
#include "felica.h"

ciSET_DEBUG(10,"felicaUtil");

felicaUtil* 	felicaUtil::_instance = 0; 
#define INTERVAL	500

felicaUtil*	
felicaUtil::getInstance() {
	if(!_instance) {
		if(!_instance) {
			_instance = new felicaUtil;
		}
	}
	return _instance;
}

void	
felicaUtil::clearInstance() {
	if(_instance) {
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}


felicaUtil::felicaUtil() 
{
    ciDEBUG(1,("felicaUtil()"));
	_is_start_polling = ciFalse;
	_msg_no_find = 0;
	_msg_no_lose = 0;
	_url="";
	_wnd = 0;

    memset(_card_idm,0x00,8);
    memset(_card_pmm,0x00,8);

	memset(_pre_card_idm,0x00,8);
	_last_found_time = 0;	
	_push_succeed = ciFalse;

	ubcConfig aIni("UBCVariables");
	_useFelica = ciFalse;
	aIni.get("ROOT","USE_FELICA", _useFelica);

	ciDEBUG(1,("felicaUtil() end"));

}

felicaUtil::~felicaUtil() 
{
    ciDEBUG(1,("~felicaUtil()\n"));
}

void
felicaUtil::felicaServerStart(HWND pwnd)
{
	ciDEBUG(1,("felicaServerStart"));

	if(!_useFelica){
		ciDEBUG(1,("This is not felica version"));
		return;
	}
	/*
	if(!LoadLibrary("felica.dll")){
		ciERROR(("felica.dll load failed"));
		return;
	}
	if(!LoadLibrary("rw.dll")){
		ciERROR(("rw.dll load failed"));
		return;
	}
	*/
	_wnd = pwnd;

	if(!this->open()){
		return;
	}
	setCallBackMessage();

	if(!_url.empty()){
		if(!startPolling()){
			return;
		}
	}
/*
	if(!felicaUtil::getInstance()->polling_once()){
		return;
	}

	if(!felicaUtil::getInstance()->push("http://www.naver.com/")){
		return;
	}
*/	
	return;
}
void
felicaUtil::felicaServerStop()
{
	if(!_useFelica){
		ciDEBUG(1,("This is not felica version"));
		return;
	}
	stopPolling();
	this->close();
}


ciBoolean
felicaUtil::open() 
{
    ciDEBUG(1,("open"));

    // initialize library
    if (!initialize_library()) {
        ciERROR(("Can't initialize library"));
		error_routine();       
        return ciFalse;
    }

    // open reader writer
    if (!open_reader_writer_auto()) {
        ciERROR(("Can't open reader writer"));
		error_routine();       
        return ciFalse;
    }

    unsigned char equipment_number[8];
    unsigned char system_parameters[8];
    unsigned char access_key_version[2];
    
	if (!rw_attention(equipment_number, system_parameters, access_key_version)) {
        ciERROR(("Can't execute attention"));
		error_routine();       
        return ciFalse;
    }

	ciDEBUG(1,("%s",print_vector("equipment_number=", equipment_number,8)));
	return ciTrue;
}

ciBoolean
felicaUtil::close() 
{
    ciDEBUG(1,("close"));
	return	close_reader_writer();
}

ciBoolean
felicaUtil::polling_once() 
{
    ciDEBUG(1,("polling_once"));
    // polling
    structure_polling polling;
    unsigned char system_code[2] = {0xfe, 0x00}; // system code
    polling.system_code = system_code;
    polling.time_slot = 0x00;

    unsigned char number_of_cards = 0;


	if(_card_idm[0] != 0){
		memcpy(_pre_card_idm,_card_idm,8);
	}

    structure_card_information card_information;
    card_information.card_idm = _card_idm;
    card_information.card_pmm = _card_pmm;

    if (!polling_and_get_card_information(&polling, &number_of_cards, &card_information)) {
        ciERROR(("Can't find FeliCa"));
		 error_routine();       
        return ciFalse;
    }
	ciDEBUG(1,("%s",print_vector("idm =", _card_idm, 8)));
	ciDEBUG(1,("%s",print_vector("pdm =", _card_pmm, 8)));

	return ciTrue;
}



ciBoolean
felicaUtil::preTranslateMessage(MSG* pMsg)
{
	if(!_useFelica){
		ciDEBUG(1,("This is not felica version"));
		return ciFalse;
	}
	if(pMsg->message == _msg_no_find){
		ciDEBUG(1,("card found"));
		ciGuard aGuard(_urlLock);

		if(_url.empty()){
			_push_succeed = ciFalse;
			return ciFalse;
		}
		ciDEBUG(1,("URL=%s",_url.c_str()));
		time_t now = time(0);
		if(_push_succeed && now - _last_found_time < 3 ){
			// 마지막으로 카드를 발견한지 3 초 이내이면, 이는 같은 카드가 계속 붙어 있는걸로 본다.
			ciDEBUG(1,("now - last found time =%ld", now-_last_found_time));
			_push_succeed = ciFalse;
			_last_found_time = now;
			return ciFalse;
		}
		_last_found_time = now;

		stopPolling();
		::Sleep(100);
		if(!polling_once()){
			ciERROR(("polling_once failed"));
			startPolling();
			return ciFalse;
		}		

		if(!push()){
			ciERROR(("push failed"));
			_push_succeed = ciFalse;
			startPolling();
			return ciFalse;		
		}else{
			_push_succeed = ciTrue;
		}
		startPolling();
		return ciTrue;
	}

	if(pMsg->message == _msg_no_lose){
		ciDEBUG(1,("card lost"));
		ciGuard aGuard(_urlLock);
		_push_succeed = ciFalse;
		return ciTrue;
	}

	return ciFalse;
}



void 
felicaUtil::setCallBackMessage()
{
	_msg_no_find = RegisterWindowMessage(FELICA_FIND_CARD_MSG); 
	_msg_no_lose = RegisterWindowMessage(FELICA_LOSE_CARD_MSG);
}

ciBoolean
felicaUtil::stopPolling()
{
	ciDEBUG(1,("stop polling"));
	if(stop_polling() == false){
		ciERROR(("stop_polling failed"));
		error_routine();
		return ciFalse;
	}
	this->_is_start_polling = false;
	return ciTrue;
}

ciBoolean
felicaUtil::startPolling()
{
	ciDEBUG(1,("start polling"));

	for(int i=0;i<10;i++){
		if(_startPolling()){
			return ciTrue;
		}
		this->close();
		for(;i<10;i++){
			if(this->open()){
				break;		
			}
			::Sleep(300);
		}
	}
	return ciFalse;
}

ciBoolean
felicaUtil::_startPolling()
{
	ciDEBUG(1,("_start polling"));

	if(_is_start_polling){
		ciWARN(("Already plolled"));
		return ciTrue;
	}

	structure_call_back_parameters call_back_parameters;

	unsigned char find[10];
	unsigned char lose[10];
	
	memset(find,0x00,sizeof(find));
	memset(lose,0x00,sizeof(lose));
	
	memcpy(find,FELICA_FIND_CARD_MSG,strlen(FELICA_FIND_CARD_MSG));
	memcpy(lose,FELICA_LOSE_CARD_MSG,strlen(FELICA_LOSE_CARD_MSG));

	call_back_parameters.message_of_card_find	= find;
	call_back_parameters.message_of_card_loss	= lose;
	call_back_parameters.handle					= (int)_wnd;						
	call_back_parameters.interval				= INTERVAL;							
	call_back_parameters.retry_count			= 0;							

	unsigned char	system_code[2] = { 0xfe, 0x00 };
	structure_polling polling;
	polling.system_code = system_code;
	polling.time_slot =0x0f;

	if(set_call_back_parameters(&call_back_parameters) == false){
		ciERROR(("set_call_back_parameters failed"));
		error_routine();
		return ciFalse;
	}
	if(start_polling(&polling) == false){
		ciERROR(("start_polling failed"));
		error_routine();
		return ciFalse;
	}
	
	ciDEBUG(1,("start polling succeed"));
	this->_is_start_polling = true;
	
	return ciTrue;
}

void
felicaUtil::setURL(const char* url)
{
	ciDEBUG(1,("setURL(%s)",url));
	if(!_useFelica){
		ciDEBUG(1,("This is not felica version"));
		return;
	}
	ciGuard aGuard(_urlLock);
	_url = url;
	if(_is_start_polling){
		stopPolling();
	}
	startPolling();
}
void
felicaUtil::unsetURL()
{
	ciDEBUG(1,("unsetURL"));
	if(!_useFelica){
		ciDEBUG(1,("This is not felica version"));
		return;
	}
	ciGuard aGuard(_urlLock);
	_url = "";
	_push_succeed = ciFalse;
}

const char*
felicaUtil::getURL()
{
	ciGuard aGuard(_urlLock);
	return _url.c_str();
}

ciBoolean
felicaUtil::push() 
{
	ciDEBUG(1,("push(%s)",_url.c_str()));
	if(_url.empty()){
		ciWARN(("URL is null"));
		return ciFalse;
	}

	unsigned char url[256];
	{
		ciGuard aGuard(_urlLock); 
		memset(url,0x00,256);
		memcpy(url,_url.c_str(),_url.size());
	}

	int url_size = _url.size();
	int param_size = url_size + 2; 
	int	data_size = param_size + 6; // data_section size

	unsigned char send_data[256];

	send_data[0] = 0x01; // separate_parts_number 
	send_data[1] = 0x02; // run browser command 
	send_data[2] = unsigned char(0x00ff & param_size);   // little endian
	send_data[3] = unsigned char((0xff00 & param_size)>>8);
	send_data[4] = unsigned char(0x00ff & url_size);  // little endian
	send_data[5] = unsigned char((0xff00 & url_size)>>8);
	memcpy(&send_data[6],url,url_size);

	unsigned int checksum = 0;
	for(int i=0;i<(6+url_size);i++) {
		checksum += send_data[i];
	}
	checksum = 0 - checksum;

	send_data[6+url_size] = unsigned char((0xff00 & checksum)>>8); // check_sum (big endian)
	send_data[7+url_size] = unsigned char(0x00ff & checksum);

	int len = 1+7+url_size;

	ciDEBUG(1,("%s",print_vector("send_data", send_data,len)));

	if (!rw_push(_card_idm, len, send_data)) {
       ciERROR(("Can't push.\n"));
		error_routine();       
       return ciFalse;
    }

	enumeration_felica_error_type err_type = FELICA_ERROR_NOT_OCCURRED;
	get_last_error_type(&err_type);

	ciDEBUG(1,("err_type = %d", err_type));
	ciDEBUG(1,("send_data succeed"));

	// activate2
    unsigned char action_flag = 0x00;
    unsigned char status;

    if (!rw_activate_2(_card_idm, &action_flag, &status)) {
       ciERROR(("Can't activate2"));
		error_routine();       
		return ciFalse;
    }
    Sleep(1000);
	ciDEBUG(1,("activate result=%d", status));
    
	return ciTrue;
}

ciBoolean
felicaUtil::_isStayAttached()
{
	// 성공한 적이 없다. 새카드로 본다
	if(!_push_succeed){
		return ciFalse;
	}

	// 성공한 적이 있다.

	// 같은 카드가 아니다.
	if(!_isSameCard()){
		_push_succeed = ciFalse;
		return ciFalse;
	}

	// 같은카디이다.
	return ciTrue;
}

ciBoolean
felicaUtil::_isSameCard()
{
    for (int i = 0; i < 8; i++) {
		if(_card_idm[i] != _pre_card_idm[i]){
			return ciFalse;
		}
    }
	return ciTrue;
}

const char* 
felicaUtil::print_vector(char* title, unsigned char* vector, int length)
{
	_bufStr=title;
	char buf[256];
    if (title != NULL) {
        fprintf(stdout, "%s ", title);
    }
	_bufStr+=" ";

    for (int i = 0; i < length - 1; i++) {
        fprintf(stdout, "%02x ", vector[i]);
        sprintf(buf, "%02x ", vector[i]);
		 _bufStr += buf;
    }

	fprintf(stdout, "%02x", vector[length - 1]);
    sprintf(buf, "%02x", vector[length - 1]);
	 _bufStr += buf;
    fprintf(stdout, "\n");

	return _bufStr.c_str();
}

void 
felicaUtil::error_routine(void)
{
    enumernation_felica_error_type felica_error_type;
    enumernation_rw_error_type rw_error_type;
    get_last_error_types(&felica_error_type, &rw_error_type);
    ciERROR(("felica_error_type: %d\n", felica_error_type));
    ciERROR(("rw_error_type: %d\n", rw_error_type));
    close_reader_writer();
    dispose_library();
}

