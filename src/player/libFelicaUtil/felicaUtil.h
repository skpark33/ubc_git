#ifndef _felicaUtil_h_
#define _felicaUtil_h_

using namespace std;

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciMutex.h>

#define	FELICA_FIND_CARD_MSG	"FIND_CARD"
#define FELICA_LOSE_CARD_MSG	"LOSE_CARD"

class felicaUtil {
public:

	static felicaUtil*	getInstance();
	static void	clearInstance();

	virtual ~felicaUtil() ;

	void		felicaServerStart(HWND pwnd);
	void		felicaServerStop();
	ciBoolean	preTranslateMessage(MSG* pMsg);

	void		setURL(const char* url);
	void		unsetURL();

	ciBoolean	isFelicaMsg(MSG* pMsg) 
	{ return 
	((_msg_no_find && _msg_no_lose && (pMsg->message == _msg_no_find || pMsg->message == _msg_no_lose)) ? ciTrue : ciFalse); }

	const char*	getURL();

protected:
	ciBoolean	open();
	ciBoolean	close();
	ciBoolean	polling_once();

	void		setCallBackMessage();
	ciBoolean	startPolling();
	ciBoolean	_startPolling();
	ciBoolean	stopPolling();

	

	ciBoolean	push();
	const char*	print_vector(char* title, unsigned char* vector, int length);
	void		error_routine();

	ciBoolean	_isStayAttached();
	ciBoolean	_isSameCard();

	felicaUtil();

	static felicaUtil*	_instance;

    unsigned char _card_idm[8];
    unsigned char _card_pmm[8];

	unsigned char _pre_card_idm[8];
	time_t		  _last_found_time;		
	ciBoolean	  _push_succeed;


	ciString	_bufStr;
	ciBoolean	_is_start_polling;

	unsigned long		_msg_no_find;
	unsigned long		_msg_no_lose;

	ciString		_url;
	ciMutex			_urlLock;
	HWND			_wnd;
	ciBoolean		_useFelica;
};



#endif // _felicaUtil_h_
