#pragma once

#include <afxmt.h>
#include "FTDefine.h"
#include "FTSocket.h"

struct SFCMsgInfo{
	SOCKET s;
	BYTE flag;
	CString file;
	CString path;
};

class CFileCopy
{
public:
	static CFileCopy* Create();
	static CFileCopy* GetObject();
	static void Release();

	bool Add(const SOCKET, LP_SFTM_COMPLETESOCKREQ);
	bool Remove(LPCSTR sIni);
	bool Run();

	void SetDonloadRoot(LPCSTR path);
	void SetTemporaryRoot(LPCSTR path);
private:
	CFileCopy();
	~CFileCopy();

	void StartCopy(LPCSTR, const SFCMsgInfo*);
	int CopyContents(LPCSTR);
public:
private:
	CString m_szDownloadPath;
	CString m_szTemporaryPath;

	BOOL		m_bThread;
	CWinThread* m_pThread;
	CCriticalSection m_cs;
	CMapStringToPtr  m_mapInis;

	static CFileCopy* m_pObject;
	static unsigned int CopyThread(LPVOID);
};