#pragma once

#include <afxmt.h>
#include "FTDefine.h"
#include "FTSocket.h"
#include "ProgressCtrlEx.h"

#define WM_FT_COMPLETECLIENT	(WM_USER + 1)
#define WM_FT_NOTIFICATION		(WM_USER + 2)

typedef CProgressCtrlEx* LP_PROGRESS;

struct SCompetClient{
	BYTE flag;
	char file[MAX_PATH];
	char path[MAX_PATH];
};

struct SNotification{
	SOCKET	s;
	char	ip[16];
	int		port;
	char	noti[FT_MAX_DATA];
};

struct SFileInfo{
	BYTE		flag;// EFileFlag
	BYTE		type;// EFileType
	ULONGLONG	size;
	char		szFile[MAX_PATH];
	char		szLocalPath[MAX_PATH];
	char		szRemotePath[MAX_PATH];

	SFileInfo(){
		size = 0;
		flag = EFT_FF_INIT;
		type = EFT_FT_NORMAL;
		memset(szFile, 0, sizeof(szFile));
		memset(szLocalPath, 0, sizeof(szLocalPath));
		memset(szRemotePath, 0, sizeof(szRemotePath));
	}
};

class CFTMsgSocket : public CFTSocket
{
public:
	CFTMsgSocket(const SOCKET s=INVALID_SOCKET, const int af=AF_INET);
	~CFTMsgSocket();
	void SetParent(CWnd*);
	void Post(UINT Msg, WPARAM wParam, LPARAM lParam);
	void Message(const char*, int);
	void SetDonloadPath(LPCSTR);
	void SetTemporaryPath(LPCSTR);
	void ClearFileList();
	bool SetINI(LPCSTR, LPCSTR =FT_CONFIG_PATH);
	bool AddFile(LPCSTR, LPCSTR =FT_CONTENTS_PATH);
	bool RemoveFile(LPCSTR);
	bool CreateNexFile(LPCSTR);
	void SetInfoCtrl(LP_PROGRESS, LP_PROGRESS, CWnd*);
	void SetCurPos();
	void SetTotPos();
	int SetBuffer();
	void SendAckMsg();
	

	const CMapStringToPtr* GetFiles(){
		return &m_mapFiles;
	}
	void SetServer(bool b){
		m_bServer = b;
	}
protected:
	void CompleteSock();
	bool SendFileData(LP_SFTM_SENDFILEREQ, bool =false);
	bool FindFileInServer(LPCSTR, ULONGLONG);

	void RecvData(const char*, int);
	void CREATEFILE_REQ(LP_SFTM_MESSAGE);
	void CREATEFILE_RES(LP_SFTM_MESSAGE);
	void COMPLETEFILE_REQ(LP_SFTM_MESSAGE);
	void COMPLETEFILE_RES(LP_SFTM_MESSAGE);
	void SENDFILE_REQ(LP_SFTM_MESSAGE);
	void SENDFILE_RES(LP_SFTM_MESSAGE);
	void COMPLETESOCK_REQ(LP_SFTM_MESSAGE);
	void COMPLETESOCK_RES(LP_SFTM_MESSAGE);
	void ACK_REQ(LP_SFTM_MESSAGE);
	void ACK_RES(LP_SFTM_MESSAGE);
	void FILECOPY_NOTI(LP_SFTM_MESSAGE);
public:
protected:
	CFile	m_file;
	UINT	m_SendCnt;
	CWnd*	m_pParent;
	bool	m_bServer;
	CString m_szCurFile;
	CString m_szINIFile;
	CString m_szDownloadPath;
	CString m_szTemporaryPath;
	CMapStringToPtr m_mapFiles;
	
	CWnd*		m_pStatus;
	ULONGLONG	m_FileSize;
	ULONGLONG	m_FileDown;
	ULONGLONG	m_TotalSize;
	ULONGLONG	m_TotalDown;
	LP_PROGRESS	m_pFileProg;
	LP_PROGRESS	m_pTotalProg;

	SFTM_MESSAGE m_SendBuff[2];
	SFTM_MESSAGE m_RecvBuff[2];
};