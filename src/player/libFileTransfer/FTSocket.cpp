#include "stdafx.h"
#include "FTSocket.h"

int FTStartup()
{

	WSADATA wsa;
	return WSAStartup(MAKEWORD(2,2), &wsa);
}

int FTCleanup()
{
	return WSACleanup();
}

CFTSocket::CFTSocket(const SOCKET s, const int af)
{
	m_hSocket = s;
	m_addr_family = af;
	m_dwTickCount = ::GetTickCount();
	memset(&m_addr, 0, sizeof(m_addr));
}

CFTSocket::~CFTSocket()
{
	if(m_hSocket != INVALID_SOCKET)
		Close();
}

const SOCKET CFTSocket::Open(const int af, const int type, const int protocol)
{
	m_addr_family = af;
	m_hSocket = socket(af, type, protocol);
	return m_hSocket;
}

int CFTSocket::Close()
{
	int nr = closesocket(m_hSocket);
	m_hSocket = INVALID_SOCKET;
	return nr;
}

int CFTSocket::SetOption(int optname, const char * optval, int optlen)
{
	return setsockopt(m_hSocket, SOL_SOCKET, optname, optval, optlen);
}

bool CFTSocket::Listen(const int port)
{
	memset((char*)&m_addr, 0, sizeof(m_addr));
	m_addr.sin_family = m_addr_family;
	m_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	m_addr.sin_port = htons(port);

	if (bind(m_hSocket, (struct sockaddr *)&m_addr, sizeof(m_addr)) < 0){
		return false;
	}

//	if(listen(m_hSocket, SOMAXCONN)){
	if(listen(m_hSocket, 5) != 0){
		return false;
	}

	return true;
}

const SOCKET CFTSocket::Accept(struct sockaddr_in& cli_addr)
{
	memset((char*)&cli_addr, 0, sizeof(cli_addr));
	int clilen = sizeof(cli_addr);
	return accept(m_hSocket, (struct sockaddr *)&cli_addr, &clilen);
}

int CFTSocket::Connect(const char* ip, const int port)
{
	memset((char*)&m_addr, 0, sizeof(m_addr));
	m_addr.sin_family = m_addr_family;
	m_addr.sin_addr.s_addr = inet_addr(ip);
	m_addr.sin_port = htons(port);
	return connect(m_hSocket, (sockaddr*)&m_addr, sizeof(m_addr));
}

int CFTSocket::Select(fd_set* readfds, fd_set* writefds, fd_set* exceptfds, const struct timeval* timeout)
{
	return select(0, readfds, writefds, exceptfds, timeout);
}

int CFTSocket::Read(char* buf, const int size, const int flag)
{
//	m_dwTickCount = ::GetTickCount();
	return recv(m_hSocket, buf, size, flag);
}

int CFTSocket::Write(const char* buf, const int size, const int flag)
{
	m_dwTickCount = ::GetTickCount();
	//return send(m_hSocket, buf, size, flag);
	int total_send_size = 0;
	while(total_send_size < size)
	{
		int send_size = send(m_hSocket, buf+total_send_size, size-total_send_size, flag);
		if(send_size == SOCKET_ERROR ) return SOCKET_ERROR;
		total_send_size += send_size;
	}
	return total_send_size;
}

const SOCKET CFTSocket::GetHandle() const
{
	return m_hSocket;
}

const struct sockaddr_in CFTSocket::GetAddress()
{
	return m_addr;
}

void CFTSocket::SetAddress(struct sockaddr_in addr)
{
	m_addr = addr;
}

const char* CFTSocket::GetIP()
{
	return inet_ntoa(m_addr.sin_addr);
}

const int CFTSocket::GetPort()
{
	return m_addr.sin_port;
}

const DWORD CFTSocket::SetTickCount()
{
	return (m_dwTickCount = ::GetTickCount());
}

const DWORD CFTSocket::CheckTickCount()
{
	return (::GetTickCount() - m_dwTickCount);
}

int CFTSocket::GetError(char* szError)
{
	int errcode = WSAGetLastError();

	if(szError){
		sprintf(szError, "%d(0x%x)", errcode, errcode);
	}

	return errcode;
}

bool CFTSocket::EnableNonBlockSock(bool bEnable)
{
#ifdef WIN32
	
	unsigned long ulParam = (bEnable == true) ? 1 : 0;
	
	if ( ioctlsocket(m_hSocket, FIONBIO, &ulParam) != 0 )
	{
		// error
		return false;
	}

#else

	int nFlags = 0;

	// If the fcntl() function fails, a value of -1 is returned and errno is set to indicate the error.
	nFlags = fcntl(m_hSocket, F_GETFL, 0);

	if ( nFlags < 0 )
	{
		// error
		return false;
	}

	if ( bEnable )
	{
		if ( (nFlags & O_NONBLOCK) == O_NONBLOCK )
			return true;

		if ( fcntl(m_hSocket, F_SETFL, nFlags | O_NONBLOCK) < 0 )
		{
			// error
			return false;
		}

		return true;
	}

	if ( (nFlags & O_NONBLOCK) == O_NONBLOCK )
	{
		if ( fcntl(m_hSocket, F_SETFL, nFlags & ~O_NONBLOCK) < 0 )
		{
			// error
			return false;
		}
	}

#endif	
	return true;
}