#include "stdafx.h"
#include "FTServer.h"
#include "FileCopy.h"

#ifdef __ciDEBUG__
#include <ci/libDebug/ciDebug.h>
#endif

#ifdef __ciDEBUG__
ciSET_DEBUG(10,"libFileTrasfer");
#endif

CFTServer::CFTServer()
{
	m_pParent = NULL;
	FD_ZERO(&m_readset);
	m_bThreadStop = true;
	m_pRecvThread = NULL;
	m_pAcceptThread = NULL;
	m_szDownloadPath = _T("");
	m_szTemporaryPath = _T("");
	m_pSocket = new CFTSocket();

#ifdef __ciDEBUG__
	ciDEBUG(1, ("Create CFTServer"));
#else
	TRACE(">>> Create CFTServer\n");
#endif
}

CFTServer::~CFTServer()
{
	CFileCopy::Release();

	m_bThreadStop = true;
	if(m_pSocket){
		delete m_pSocket;
		m_pSocket = NULL;
	}

	m_cs.Lock();
	POSITION pos = m_mapClient.GetStartPosition();
	while(pos){
		SOCKET s;
		CFTMsgSocket* pSocket=NULL;
		m_mapClient.GetNextAssoc(pos, s, pSocket);
		if(pSocket)	delete pSocket;
	}
	m_mapClient.RemoveAll();
	m_cs.Unlock();


	if(m_pAcceptThread){
		::WaitForSingleObject(m_pAcceptThread->m_hThread, 5000);
		m_pAcceptThread = NULL;
	}

	if(m_pRecvThread){
		::WaitForSingleObject(m_pRecvThread->m_hThread, 5000);
		m_pRecvThread = NULL;
	}
#ifdef __ciDEBUG__
	ciDEBUG(1, ("Destroy CFTServer"));
#else
	TRACE(">>> Destroy CFTServer\n");
#endif
}

void CFTServer::SetParent(CWnd* pWnd)
{
	m_pParent = pWnd;
}

void CFTServer::SetDonloadRoot(LPCSTR path)
{
	if(!path)	return;
#ifdef __ciDEBUG__
	ciDEBUG(1, ("SetDonloadRoot(%s)", path));
#endif
	m_szDownloadPath = path;

#ifdef __ciDEBUG__
	ciDEBUG(1, ("SetDonloadRoot(%s)", m_szDownloadPath));
#endif

	CFileCopy::Create()->SetDonloadRoot(path);
}

void CFTServer::SetTemporaryRoot(LPCSTR path)
{
	if(!path)	return;
	m_szTemporaryPath = path;
	CFileCopy::Create()->SetTemporaryRoot(path);
}

int CFTServer::GetOption(int optname)
{
	int buf=0;
	int len = sizeof(buf);
	int nRet = getsockopt(m_pSocket->GetHandle(),SOL_SOCKET,optname,(char*)&buf,&len);
	return buf;
}

bool CFTServer::Start(const int nPort)
{
	if(!m_pSocket)
		return false;
	if(m_pSocket->Open() == INVALID_SOCKET)
		return false;
	if(!m_pSocket->Listen(nPort))
		return false;

#ifdef __ciDEBUG__
	ciDEBUG(1, ("before Start Server (%d)", nPort));
#else	
	TRACE(">>> CFTServer::Start(%d)\n", nPort);
#endif
//	int nRet = 0;
//	nRet = m_pSocket->SetOption(SO_SNDBUF, (const char*)&m_SendBuff, sizeof(m_SendBuff));
//	nRet = m_pSocket->SetOption(SO_RCVBUF, (const char*)&m_RecvBuff, sizeof(m_RecvBuff));
	
	FD_ZERO(&m_readset);

	if(!m_pAcceptThread){
		m_bThreadStop = false;
		m_pAcceptThread = AfxBeginThread(AcceptThread, (LPVOID)this);
	}

	CFileCopy::Create()->Run();

#ifdef __ciDEBUG__
	ciDEBUG(1, ("after Start Server (%d)", nPort));
#else	
	TRACE(">>> CFTServer::Start(%d)\n", nPort);
#endif
	return true;
}

void CFTServer::CheckInvalidSocket()
{
	m_cs.Lock();
	// 기존 소켓을 체크한다. 60초이상 송수신이 없다면 제거
	CFTMsgSocket* pSocket = NULL;
	POSITION pos = m_mapClient.GetStartPosition();
	while(pos){
		SOCKET s;		
		pSocket = NULL;
		m_mapClient.GetNextAssoc(pos, s, pSocket);
		
		if(!pSocket)	continue;

		if(pSocket->CheckTickCount() < FT_TIMEOUT*2)
			continue;

		FD_CLR(s, &m_readset);
		delete pSocket;
		m_mapClient.RemoveKey(s);
	}
	m_cs.Unlock();
}

void CFTServer::AddClientSocket(const SOCKET s, const struct sockaddr_in addr)
{
	if(!m_pRecvThread){
		m_pRecvThread = AfxBeginThread(RecvThread, (LPVOID)this);
	}

	CheckInvalidSocket();

	m_cs.Lock();
// Add 시작.....
	CFTMsgSocket* pSocket = NULL;
	if(m_mapClient.Lookup(s, pSocket)){
		m_cs.Unlock();
		return;
	}

	pSocket = new CFTMsgSocket(s);
	if(!pSocket){
		m_cs.Unlock();
		return;
	}

	pSocket->SetDonloadPath(m_szDownloadPath);
	pSocket->SetTemporaryPath(m_szTemporaryPath);
	pSocket->SetAddress(addr);
	pSocket->SetParent(m_pParent);

//	pSocket->SetBuffer();
//	pSocket->EnableNonBlockSock(false);

//	struct timeval tv_timeo = { 30, 0 };
//	pSocket->SetOption(SO_SNDTIMEO, (const char*)&tv_timeo, sizeof(tv_timeo));
//	pSocket->SetOption(SO_RCVTIMEO, (const char*)&tv_timeo, sizeof(tv_timeo));

	m_mapClient.SetAt(s, pSocket);
	FD_SET(s, &m_readset);

	m_cs.Unlock();
#ifdef __ciDEBUG__
	ciDEBUG(1, ("CFTServer::AddClientSocket(%d)", s));
#else	
	TRACE(">>> CFTServer::AddClientSocket(%d)\n", s);
#endif
}

unsigned int CFTServer::AcceptThread(LPVOID pVoid)
{
	CFTServer* pServer = (CFTServer*)pVoid;
	if(!pServer || !pServer->m_pSocket){
		AfxEndThread(0);
		return 0;
	}

	struct sockaddr_in cli_addr;
	int clilen = sizeof(cli_addr);
	SOCKET hSocket = pServer->m_pSocket->GetHandle();
#ifdef __ciDEBUG__
	ciDEBUG(1, ("Run AcceptThread"));
#else	
	TRACE(">>> Run AcceptThread\n");
#endif

	while(1){
		SOCKET s = accept(hSocket, (struct sockaddr *)&cli_addr, &clilen);

		if(pServer->m_bThreadStop)
			break;

		if(s == INVALID_SOCKET) {
			Sleep(1000);
			continue;
		}

		pServer->AddClientSocket(s, cli_addr);
	}
#ifdef __ciDEBUG__
	ciDEBUG(1, ("AcceptThread is terminated"));
#else	
	TRACE(">>> CFTServer::AcceptThread is terminated\n");
#endif
	AfxEndThread(0);
	return 0;
}

unsigned int CFTServer::RecvThread(LPVOID pVoid)
{
	CFTServer* pServer = (CFTServer*)pVoid;
	if(!pServer || !pServer->m_pSocket){
		AfxEndThread(0);
		return 0;
	}

	fd_set rset;
	struct timeval tv;
	//char buf[FT_BUFFER_SIZE+10];
	char buf[FT_BUFFER_SIZE];
#ifdef __ciDEBUG__
	ciDEBUG(1, ("Run RecvThread"));
#else	
	TRACE(">>> Run RecvThread\n");
#endif

	while(1){
		tv.tv_sec = 10;
		tv.tv_usec = 0;
		rset = pServer->m_readset;

		int nSel = pServer->m_pSocket->Select(&rset, NULL, NULL, &tv);

		if(pServer->m_bThreadStop)
			break;

		if(nSel == 0){// time out
			pServer->CheckInvalidSocket();
			continue;
		}

		if(nSel == SOCKET_ERROR){
			Sleep(1000);
			continue;
		}

		pServer->m_cs.Lock();

		for(int i = 0; i < nSel; i++){
			POSITION pos = pServer->m_mapClient.GetStartPosition();
			while(pos){
				SOCKET s;
				CFTMsgSocket* pSocket=NULL;
				pServer->m_mapClient.GetNextAssoc(pos, s, pSocket);

				if(!pSocket){
					FD_CLR(s, &pServer->m_readset);
					pServer->m_mapClient.RemoveKey(s);
#ifdef __ciDEBUG__
	ciDEBUG(1, ("CFTServer::RecvThread(%d) Remove socket 1", s));
#else	
	TRACE(">>> CFTServer::RecvThread(%d) Remove socket 1\n", s);
#endif
					continue;
				}

				if (FD_ISSET(s, &rset)) {
					memset(buf, 0, sizeof(buf));
					int rv = pSocket->Read(buf, sizeof(buf), 0);

					if(rv == SOCKET_ERROR){
						FD_CLR(s, &pServer->m_readset);
						delete pSocket;
						pServer->m_mapClient.RemoveKey(s);
#ifdef __ciDEBUG__
	ciDEBUG(1, ("CFTServer::RecvThread(%d) Remove socket 2", s));
#else	
	TRACE(">>> CFTServer::RecvThread(%d) Remove socket 2\n", s);
#endif
					}
					else if(rv == -1){
						FD_CLR(s, &pServer->m_readset);
						delete pSocket;
						pServer->m_mapClient.RemoveKey(s);
#ifdef __ciDEBUG__
	ciDEBUG(1, ("CFTServer::RecvThread(%d) Remove socket 3", s));
#else	
	TRACE(">>> CFTServer::RecvThread(%d) Remove socket 3\n", s);
#endif
					}
					else if(rv == 0){
						DWORD dwSize; 
						ioctlsocket(s, FIONREAD, &dwSize); 
						if(dwSize != 0) break;

						FD_CLR(s, &pServer->m_readset);
						delete pSocket;
						pServer->m_mapClient.RemoveKey(s);
#ifdef __ciDEBUG__
	ciDEBUG(1, ("CFTServer::RecvThread(%d) Remove socket 4", s));
#else	
	TRACE(">>> CFTServer::RecvThread(%d) Remove socket 4\n", s);
#endif
					}
					else{
						pSocket->Message(buf, rv);
					}

					break;
				}
			}
		}

		pServer->m_cs.Unlock();
	}
#ifdef __ciDEBUG__
	ciDEBUG(1, ("RecvThread is terminated"));
#else	
	TRACE(">>> CFTServer::RecvThread is terminated\n");
#endif
	AfxEndThread(0);
	return 0;
}