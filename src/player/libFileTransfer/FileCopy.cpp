// FileCopy.cpp : implementation file
//

#include "stdafx.h"
#include "FileCopy.h"
#include "common/libProfileManager/ProfileManager.h"
#include <IMAGEHLP.H>
#pragma comment(lib, "imagehlp.lib")

#ifdef __ciDEBUG__
#include <ci/libDebug/ciDebug.h>
#endif


#ifdef __ciDEBUG__
ciSET_DEBUG(10,"libFileTrasfer");
#endif

CFileCopy* CFileCopy::m_pObject = NULL;

CFileCopy::CFileCopy()
{
	m_bThread = FALSE;
	m_pThread = NULL;
	m_szDownloadPath.Empty();
	m_szTemporaryPath.Empty();
}

CFileCopy::~CFileCopy()
{
	if(m_pThread){
		m_bThread = FALSE;
		::WaitForSingleObject(m_pThread->m_hThread, 5000);
		m_pThread = NULL;
	}

	POSITION pos = m_mapInis.GetStartPosition();
	while(pos){
		CString szIni;
		SFCMsgInfo* pInfo = NULL;
		m_mapInis.GetNextAssoc(pos, szIni, (void*&)pInfo);
		if(!pInfo)	continue;
		delete pInfo;
	}
	m_mapInis.RemoveAll();
}

CFileCopy* CFileCopy::Create()
{
#ifdef __ciDEBUG__
	ciDEBUG(1, ("start Create()"));
#endif
	if(m_pObject)	return m_pObject;
#ifdef __ciDEBUG__
	ciDEBUG(1, ("new Create()"));
#endif
	m_pObject = new CFileCopy;
#ifdef __ciDEBUG__
	ciDEBUG(1, ("end Create()"));
#endif
	return m_pObject;
}

void CFileCopy::Release()
{
	if(!m_pObject)	return;

	delete m_pObject;
	m_pObject = NULL;
}

CFileCopy* CFileCopy::GetObject()
{
	return Create();
}

bool CFileCopy::Add(const SOCKET s, LP_SFTM_COMPLETESOCKREQ pMsg)
{
	SFCMsgInfo* pInfo = new SFCMsgInfo;
	pInfo->s = s;
	pInfo->flag = pMsg->flag;
	pInfo->file = pMsg->file;
	pInfo->path = pMsg->path;

	m_cs.Lock();
	m_mapInis.SetAt(pMsg->file, (void*)pInfo);
	m_cs.Unlock();

	return true;
}

bool CFileCopy::Remove(LPCSTR sIni)
{
	SFCMsgInfo* pInfo = NULL;
	m_cs.Lock();
	m_mapInis.Lookup(sIni, (void*&)pInfo);
	if(pInfo)	delete pInfo;
	m_mapInis.RemoveKey(sIni);
	m_cs.Unlock();

	return true;
}

void CFileCopy::SetDonloadRoot(LPCSTR path)
{
#ifdef __ciDEBUG__
	ciDEBUG(1, ("SetDonloadRoot()"));
#endif
	if(!path)	return;
#ifdef __ciDEBUG__
	ciDEBUG(1, ("SetDonloadRoot(%s)",path));
#endif
	m_szDownloadPath = path;
#ifdef __ciDEBUG__
	ciDEBUG(1, ("SetDonloadRoot(%s)", m_szDownloadPath));
#endif

}

void CFileCopy::SetTemporaryRoot(LPCSTR path)
{
	if(!path)	return;
	m_szTemporaryPath = path;
}

bool CFileCopy::Run()
{
	if(!m_pThread){
		m_bThread = TRUE;
		m_pThread = AfxBeginThread(CopyThread, NULL);
	}
#ifdef __ciDEBUG__
	ciDEBUG(1, ("Run FilCopy Thread"));
#else	
	TRACE(">>> Run FilCopy Thread\n");
#endif
	return true;
}

unsigned int CFileCopy::CopyThread(LPVOID pVoid)
{
#ifdef __ciDEBUG__
	ciDEBUG(1, ("skpark:Start FilCopy Thread"));
#endif

	while(m_pObject->m_bThread){
		Sleep(1000);
		m_pObject->m_cs.Lock();
		POSITION pos = m_pObject->m_mapInis.GetStartPosition();
		while(pos){
			CString szIni;
			SFCMsgInfo* pInfo = NULL;
			m_pObject->m_mapInis.GetNextAssoc(pos, szIni, (void*&)pInfo);
			if(!pInfo)	continue;
			m_pObject->StartCopy(szIni, pInfo);
			delete pInfo;
		}
		m_pObject->m_mapInis.RemoveAll();
		m_pObject->m_cs.Unlock();
	}
#ifdef __ciDEBUG__
	ciDEBUG(1, ("Stop FilCopy Thread"));
#else	
	TRACE(">>> Stop FilCopy Thread\n");
#endif
	AfxEndThread(0);
	return 0;
}

void CFileCopy::StartCopy(LPCSTR sIni, const SFCMsgInfo* pInfo)
{
	if(!sIni || strlen(sIni) == 0 || !pInfo)
		return;

#ifdef __ciDEBUG__
	ciDEBUG(1, ("File Copy Start: %s", sIni));
#else	
	TRACE(">>> CFileCopy::StartCopy(%s)\n", sIni);
#endif

	DWORD dwErr;
	SFTM_MESSAGE msg;
	BYTE  bFlag = pInfo->flag;

	if(sIni != NULL && strlen(sIni) != 0)
	{
		CString szSrcIniPath = m_szTemporaryPath + FT_CONFIG_PATH;
		CString szDesIniPath = m_szDownloadPath + FT_CONFIG_PATH;
		szSrcIniPath += sIni;
		szDesIniPath += sIni;

		// 0000552: 이름이 소문자로 되어있는 스케줄을 대문자로 다시저장하여 단말에 보내는 경우 대문자로 변경해야하므로 먼저 삭제한다
		//CFile::Remove(szDesIniPath);
		if(!::CopyFile(szSrcIniPath, szDesIniPath, FALSE))
		{
			dwErr = GetLastError();
		}

		CProfileManager objIniManager(szSrcIniPath);

		//char buf[1024*100];
		//ZeroMemory(buf, sizeof(buf));
		//::GetPrivateProfileString("Host", "ContentsList", "", buf, sizeof(buf), szSrcIniPath);
		CString strTmp1 = objIniManager.GetProfileString("Host", "ContentsList");

		//::GetPrivateProfileString("Host", "EtcList", "", buf, sizeof(buf), szSrcIniPath);
		CString strTmp2 = objIniManager.GetProfileString("Host", "EtcList");

		int iStart = 0;
		CString szContentsList = strTmp1 + (!strTmp1.IsEmpty() && !strTmp2.IsEmpty() ? "," : "") + strTmp2;
		CString szContents;
		while(!(szContents = szContentsList.Tokenize(",", iStart)).IsEmpty())
		{
			//ZeroMemory(buf, sizeof(buf));
			//::GetPrivateProfileString(szContents, "filename", "", buf, sizeof(buf), szSrcIniPath);
			CString strId       = objIniManager.GetProfileString(szContents, "contentsId");
			CString strLocation = objIniManager.GetProfileString(szContents, "location");
			CString strFileName = objIniManager.GetProfileString(szContents, "filename");
			CString strParentId = objIniManager.GetProfileString(szContents, "parentId");
			if(strFileName.IsEmpty()) continue;

			// 부속파일처리 관련 수정
			CString strCompareStr = "/contents/" + (!strParentId.IsEmpty() ? strParentId : strId) + "/";
			if(strLocation.MakeLower().Find(strCompareStr.MakeLower()) == 0)
			{
				strLocation = strLocation.Mid(strCompareStr.GetLength());
			}

			if(CopyContents(strLocation + strFileName) != EFT_FF_SUCCESS)
			{
				InitMessage(&msg);
				msg.header.id = htons(FTM_FILECOPY_NOTI);
				msg.header.len = htons(sizeof(SFTM_FILECOPYNOTI));

				msg.msg.msg2001.flag = EFT_FF_COPYERR;
				strncpy(msg.msg.msg2001.file, strLocation + strFileName, sizeof(msg.msg.msg2001.file)-1);

				send(pInfo->s, (char*)&msg, sizeof(SFTM_MESSAGE), 0);
			}
		}
	}
	else
	{
		bFlag = EFT_FF_COPYERR;
	}

	InitMessage(&msg);
	msg.header.id = htons(FTM_COMPLETESOCK_RES);
	msg.header.len = htons(sizeof(SFTM_COMPLETESOCKRES));

	msg.msg.msg1004.flag = bFlag;
	strncpy(msg.msg.msg1004.file, pInfo->file, sizeof(msg.msg.msg1004.file)-1);
	strncpy(msg.msg.msg1004.path, pInfo->path, sizeof(msg.msg.msg1004.path)-1);

	int nRet = send(pInfo->s, (char*)&msg, sizeof(SFTM_MESSAGE), 0);
}

int CFileCopy::CopyContents(LPCSTR szFile)
{
	if(!szFile || strlen(szFile) == 0)
	{
		#ifdef __ciDEBUG__
			ciERROR(("skpark : CopyContents : file size is 0"));
		#else
			TRACE(">>> CFileCopy::CopyContentsfile size is 0");
		#endif
		return EFT_FF_COPYERR;
	}
#ifdef __ciDEBUG__
	ciDEBUG(1, ("Contents Copy : %s", szFile));
#else	
	TRACE(">>> CFileCopy::CopyContents(%s)\n", szFile);
#endif

	CString szSrcPath = m_szTemporaryPath + FT_CONTENTS_PATH;
	CString szDesPath = m_szDownloadPath + FT_CONTENTS_PATH;
	szSrcPath += szFile;
	szDesPath += szFile;

	CFileFind ffSrc;
	if(!ffSrc.FindFile(szSrcPath)){
		ffSrc.Close();
#ifdef __ciDEBUG__
	ciDEBUG(1, ("src file not found : %s", szSrcPath));
#endif		
	return EFT_FF_SUCCESS;// 없으면 전송 안한걸로 치부하고 성공했다고 봄.
	}
	ffSrc.FindNextFile();

	CFileFind ffDes;
	if(ffDes.FindFile(szDesPath)){
		ffDes.FindNextFile();

		if(ffSrc.GetLength() == ffDes.GetLength()){
			ffSrc.Close();
			ffDes.Close();
#ifdef __ciDEBUG__
	ciDEBUG(1, ("same file exist : src=%s, des=%s", szSrcPath, szDesPath));
#endif		
			return EFT_FF_SUCCESS; // 똑같은것이 존재하면 무조건 성공했다고 봄.
		}
	}

	ffSrc.Close();
	ffDes.Close();

	TCHAR lpExistingFileName[MAX_PATH+1] = {0};
	_tcscpy(lpExistingFileName, szSrcPath);

	TCHAR lpNewFileName[MAX_PATH+1] = {0};
	_tcscpy(lpNewFileName, szDesPath);

	// 부속파일처리 관련 수정
	TCHAR drive[MAX_PATH], dir[MAX_PATH], fname[MAX_PATH], ext[MAX_PATH];
	_splitpath(lpNewFileName, drive, dir, fname, ext );

	CString path;
	path.Format(_T("%s%s"), drive, dir);
	path.Replace(_T("/"), _T("\\"));
	MakeSureDirectoryPathExists(path);

	//if(!::CopyFile(szSrcPath, szDesPath, FALSE))
	if(!::MoveFileEx( lpExistingFileName, lpNewFileName
					, MOVEFILE_REPLACE_EXISTING			// 새 파일이 이미 있을 경우 덮어 쓴다. 이 플래그는 파일에 대해서만 사용할 수 있으며 디렉토리에는 적용되지 않는다
					| MOVEFILE_WRITE_THROUGH			// 파일이 실제로 완전히 이동되기 전에는 리턴하지 않는다. 파일 이동 후 이동된 새 파일을 곧바로 사용하고자 할 때는 이 플래그를 사용해야 한다.
//					| MOVEFILE_COPY_ALLOWED				// 파일이 다른 드라이브간에 이동될 때는 CopyFile, DeleteFile 함수 호출을 시뮬레이트하도록 한다
//					| MOVEFILE_FAIL_IF_NOT_TRACKABLE    // 2000이상. 원본 파일이 링크 소스이고 이동 후에 위치를 찾을 수 없을 때 실패한다. 이 상황은 대상 볼륨이 FAT로 포맷되어 있을 때 발생한다
					))
	{
		DWORD dwErr = GetLastError();
#ifdef __ciDEBUG__
	ciDEBUG(1, ("MoveFileEx fail : src=%s, des=%s, errCode=%d", lpExistingFileName, lpNewFileName, dwErr));

#endif		
		// skpark 2013.2.12 
		// 소프트링크 문제 즉, errCode==17 로 실패하면 옵션을 바꾸고 한번 더 한다.
		if(dwErr==17) {
			if(!::MoveFileEx( lpExistingFileName, lpNewFileName
							, MOVEFILE_REPLACE_EXISTING			// 새 파일이 이미 있을 경우 덮어 쓴다. 이 플래그는 파일에 대해서만 사용할 수 있으며 디렉토리에는 적용되지 않는다
		//					| MOVEFILE_WRITE_THROUGH			// 파일이 실제로 완전히 이동되기 전에는 리턴하지 않는다. 파일 이동 후 이동된 새 파일을 곧바로 사용하고자 할 때는 이 플래그를 사용해야 한다.
							| MOVEFILE_COPY_ALLOWED				// 2013.2.12 skpark 파일이 다른 드라이브간에 이동될 때는 CopyFile, DeleteFile 함수 호출을 시뮬레이트하도록 한다
		//					| MOVEFILE_FAIL_IF_NOT_TRACKABLE    // 2000이상. 원본 파일이 링크 소스이고 이동 후에 위치를 찾을 수 없을 때 실패한다. 이 상황은 대상 볼륨이 FAT로 포맷되어 있을 때 발생한다
							))
			{	
				DWORD dwErr = GetLastError();
	#ifdef __ciDEBUG__
				ciDEBUG(1, ("MoveFileEx fail : src=%s, des=%s, errCode=%d", lpExistingFileName, lpNewFileName, dwErr));
	#endif		
				return EFT_FF_COPYERR;
			}
		}else{
			return EFT_FF_COPYERR;
		}
	}
#ifdef __ciDEBUG__
	ciDEBUG(1, ("MoveFileEx succeed : src=%s, des=%s", lpExistingFileName, lpNewFileName));
#endif		

	return EFT_FF_SUCCESS;
}