#include "stdafx.h"
#include "FTDefine.h"

void InitMessage(LP_SFTM_MESSAGE pMsg)
{
	ZeroMemory(pMsg, sizeof(SFTM_MESSAGE));
	memcpy(pMsg->header.check, FT_HEADER_CHECK, sizeof(pMsg->header.check));
}