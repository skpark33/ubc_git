#pragma once

#include <afxmt.h>
#include <afxtempl.h>
#include "FTMsgSocket.h"

#ifdef _FT_LIBLINK
class CFTServer
#else
class AFX_EXT_CLASS CFTServer
#endif
{
public:
	CFTServer();
	~CFTServer();
	void SetParent(CWnd*);
	bool Start(const int nPort=FT_PORT);
	void SetDonloadRoot(LPCSTR);
	void SetTemporaryRoot(LPCSTR);
	int GetOption(int optname=SO_SNDBUF);
protected:
	void AddClientSocket(const SOCKET s, const struct sockaddr_in addr);
	void CheckInvalidSocket();
public:
protected:
	CWnd* m_pParent;
	fd_set m_readset;
	bool m_bThreadStop;
	CFTSocket* m_pSocket;
	CCriticalSection m_cs;
	CString m_szDownloadPath;
	CString m_szTemporaryPath;

	SFTM_MESSAGE m_SendBuff[2];
	SFTM_MESSAGE m_RecvBuff[2];

	CWinThread* m_pAcceptThread;
	CWinThread* m_pRecvThread;
	CMap<SOCKET,SOCKET,CFTMsgSocket*,CFTMsgSocket*> m_mapClient;

	static unsigned int AcceptThread(LPVOID);
	static unsigned int RecvThread(LPVOID);
};