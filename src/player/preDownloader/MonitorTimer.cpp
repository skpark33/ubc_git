#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciTime.h>
#include "monitorTimer.h"
#include "common/libCommon/ubcMonitor.h"

ciSET_DEBUG(9, "monitorTimer");


monitorTimer::monitorTimer(ciString& site, ciString& host) 
{
	_site = site;
	_host = host;
	ciDEBUG(3, ("monitorTimer()"));
}

monitorTimer::~monitorTimer() {
	ciDEBUG(3, ("~monitorTimer()"));
}

void
monitorTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(3, ("\n\nprocessExpired(%s,%d,%d)", name.c_str(), counter, interval));

	ciTime now;

	char nowStr[10];
	memset(nowStr,0x00,10);
	sprintf(nowStr, "%02d:%02d", now.getHour(), now.getMinute());

	ciDEBUG(3, ("processExpired(%s,%s,%d,%d)", nowStr, name.c_str(), counter, interval));

	ubcMonitor aMonitor(_site.c_str(),_host.c_str());
	if(!aMonitor.get()){
		ciERROR(("Get Monitor Information failed"));
		return;
	}
	if(counter==1){
		aMonitor.writeVariables();
	}
	aMonitor.doMonitor();
	ciDEBUG(3, ("processExpired(%s,%s,%d,%d) End\n\n", nowStr, name.c_str(), counter, interval));
	return;
}
