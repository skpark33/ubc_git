#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciTime.h>
#include "powerOnTimer.h"
#include "common/libCommon/ubcPowerOn.h"

ciSET_DEBUG(9, "powerOnTimer");


powerOnTimer::powerOnTimer(ciString& site, ciString& host) 
{
	_site = site;
	_host = host;
	ciDEBUG(3, ("powerOnTimer()"));
}

powerOnTimer::~powerOnTimer() {
	ciDEBUG(3, ("~powerOnTimer()"));
}

void
powerOnTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(3, ("\n\nprocessExpired(%s,%d,%d)", name.c_str(), counter, interval));

	ciTime now;

	char nowStr[10];
	memset(nowStr,0x00,10);
	sprintf(nowStr, "%02d:%02d", now.getHour(), now.getMinute());

	ciDEBUG(3, ("processExpired(%s,%s,%d,%d)", nowStr, name.c_str(), counter, interval));

	ubcPowerOn aPowerOn(_site.c_str(),_host.c_str());
	if(!aPowerOn.get()){
		ciERROR(("Get PowerOn Information failed"));
		return;
	}
	if(counter==1){
		aPowerOn.writeVariables();
	}
	aPowerOn.doPowerOn();
	ciDEBUG(3, ("processExpired(%s,%s,%d,%d) End\n\n", nowStr, name.c_str(), counter, interval));
	return;
}
