#ifndef _tcpGuestSocketSession_h_
#define _tcpGuestSocketSession_h_

#include <ci/libSocket/ciTaskMutex.h>
#include <ci/libThread/ciMutex.h>
#include <ci/libBase/ciListType.h>
#include <map>
#include <ci/libTimer/ciTimer.h>

#include "tcpGuestSocketHandler.h"


class tcpGuestSocketSession : public tcpGuestSocketHandler {
public:
	tcpGuestSocketSession();
	virtual ~tcpGuestSocketSession();

	int	svc();

	ciBoolean  readline(ciString& outVal);
	ciBoolean	writeline(const char* inVal);
	ciBoolean	writeline(ciString& inVal);
	int		readAndWrite(const char* filename, int filesize,int seq);

	int	deleteOldFile(const char* filename, int duration);
	int writeXML(const char* filename);

protected:	
	ciBoolean	_getFileInfo(const char* command, ciString& filename,int& filesize);
	ciMutex	_sockLock;
};

#endif