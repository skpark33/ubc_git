 /*! \file CInstallApache.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#pragma once


class CInstallApache 
{
public:
	enum CACHE_TYPE { NO_CACHE, CACHE_SERVER, CACHE_CLIENT };

	CInstallApache() { runManually = false;};
	virtual ~CInstallApache() {};

	CACHE_TYPE	 getType();

	bool  isInstalled();
	bool  download();
	
	bool  isRegisterStartMenu();
	bool  registerStartMenu();
	bool  unregisterStartMenu();
	
	bool  isRegisterFirewall();
	bool  registerFirewall();
	bool  unregisterFirewall();

	unsigned long  isRun();
	bool  run();
	bool  kill();

	bool runManually;
protected:

};
		
