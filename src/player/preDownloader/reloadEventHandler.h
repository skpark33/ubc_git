#ifndef _reloadEventHandler_h_
#define _reloadEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

class reloadEventHandler : public cciEventHandler {
public:
	reloadEventHandler(const char* siteId, const char* hostId) 
		{ _siteId =siteId; _hostId = hostId;}
	virtual ~reloadEventHandler() {}
   	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean		addHand();
	void			setSiteId(const char* siteId) { _siteId = siteId; }

protected:
	ciBoolean	_isAlreadyDone(const char* requestedProgramId, time_t requestedTime, int side);
	ciULong _eventKey;
	ciMutex	_eventKeyLock;

	ciString _siteId;
	ciString _hostId;

};


#endif // _reloadEventHandler_h_
