#ifndef _powerOnTimer_H_
#define _powerOnTimer_H_

#include <ci/libTimer/ciWinTimer.h>

class powerOnTimer : public ciWinPollable {
public:
	powerOnTimer(ciString& site, ciString& host);
	virtual ~powerOnTimer();

	virtual void processExpired(ciString name, int counter, int interval);


protected:

	ciString _site;
	ciString _host;
};

#endif //_powerOnTimer_H_
