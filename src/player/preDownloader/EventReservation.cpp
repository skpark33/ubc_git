/************************************************************************************/
/*! @file EventReservation.cpp
	@brief Reservation 이벤트의 속성을 갖는 클래스 선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/08/13\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2009/08/13:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "atlstr.h"
#include "EventReservation.h"
//#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/ccicall.h>
#include <cci/libvalue/cciany.h>
//#include "common/libInstall/installUtil.h"
//#include "common/libScratch/scratchUtil.h"



ciSET_DEBUG(10, "EventReservation");

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CEventReservation::CEventReservation(ciString strReservationId,
									 ciString strSiteId,
									 ciString strProgramId,
									 ciShort sSide,
									 ciTime tmFrom,
									 ciTime tmTo)
:	m_bComlete(false)
{
	m_strReservationId	= strReservationId;
	m_strSiteId			= strSiteId;
	m_strProgramId		= strProgramId;
	m_sSide				= sSide;
	m_tmFrom			= tmFrom;
	m_tmTo				= tmTo;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CEventReservation::~CEventReservation()
{
	ClearContentsList();
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 예약작업 내용을 파일에 기록한다 \n
/// @param (bool) bComplete : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CEventReservation::Save(bool bComplete)
{
	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);

	CString strConfigPath; 
	strConfigPath.Format("%s%sdata/preDownloader.ini", cDrive, cPath);
	
	m_bComlete = bComplete;
	if(bComplete)
	{
		//파일의 다운로드가 끝났다면 bComplete 값만 변경
		::WritePrivateProfileString(m_strReservationId.c_str(), "DownloadComplete", "1", strConfigPath);
	}
	else
	{
		CString strIdList, strTmp;
		char cBuf[1024] = { 0x00 };

		::GetPrivateProfileString("Reservation", "ReservationIdList", "", cBuf, 1024, strConfigPath);
		strIdList = cBuf;
		strIdList.Replace(" ", "");
		strIdList += ",";
		strIdList += m_strReservationId.c_str();

		//예약 정보를 저장
		::WritePrivateProfileString(m_strReservationId.c_str(), "ReservationId", m_strReservationId.c_str(), strConfigPath);
		::WritePrivateProfileString(m_strReservationId.c_str(), "SiteId", m_strSiteId.c_str(), strConfigPath);
		::WritePrivateProfileString(m_strReservationId.c_str(), "ProgramId", m_strProgramId.c_str(), strConfigPath);
		strTmp.Format("%d", m_sSide);
		::WritePrivateProfileString(m_strReservationId.c_str(), "Side", strTmp, strConfigPath);
		strTmp.Format("%ld", (ULONG)m_tmFrom.getTime());
		::WritePrivateProfileString(m_strReservationId.c_str(), "FromTime", strTmp, strConfigPath);
		strTmp.Format("%ld", (ULONG)m_tmTo.getTime());
		::WritePrivateProfileString(m_strReservationId.c_str(), "ToTime", strTmp, strConfigPath);
		::WritePrivateProfileString(m_strReservationId.c_str(), "DownloadComplete", "0", strConfigPath);
	}//if
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Call을 해서 서버로부터 컨텐츠 객체를 가져온다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEventReservation::GetContents()
{
	cciCall call("get");

	cciEntity aEntity;

	//컨텐츠를 가져올때 site는 프로그램명에서 가져온다.
	//(예 : programId가 SQI_foo 이면 SQI가 site 값이 된다.
	CString strSiteId = m_strProgramId.c_str();
	int nIndex = strSiteId.Find("_", 0);
	if(nIndex == -1)
	{
		ciDEBUG(1, ("Invalid progrmaId foramt %s", m_strProgramId.c_str()));
		return false;
	}//if
	strSiteId = strSiteId.Left(nIndex);

	aEntity.set("PM=*/Site=%s/Program=%s/Contents=*", strSiteId, m_strProgramId.c_str());
	
	call.setEntity(aEntity);

	cciAny nullAny;
	call.addItem("attributes", nullAny);

	try
	{
		ciDEBUG(3, ("Get Contents Call=%s", call.toString()));

		if(!call.call())
		{
			ciERROR(("Fail to get Contents Call"));
			return false; // call fail
		}//if

		int reply_count = 0;
		cciReply reply;
		cciStringList listValue;
		ClearContentsList();
		CContents* pConts = NULL;

		while(call.hasMore()) { // skpark add aCall.hasMore()!
		while(call.getReply(reply))		// skpark add aCall.hasMore()!
		{
			ciString str = reply.toString();
			if(str.length() < 10) continue;

			reply_count++;

			ciDEBUG(3, ("Get Contents Reply=%s", reply.toString()));

			reply.unwrap();

			pConts = new CContents;

			reply.getItem("contentsId",			pConts->m_strID);
			reply.getItem("contentsName",		pConts->m_strContentsName);
			reply.getItem("location",			pConts->m_strLocation);
			reply.getItem("filename",			pConts->m_strFilename);
			reply.getItem("comment1",			pConts->m_strComment[0]);
			reply.getItem("comment2",			pConts->m_strComment[1]);
			reply.getItem("comment3",			pConts->m_strComment[2]);
			reply.getItem("comment4",			pConts->m_strComment[3]);
			reply.getItem("comment5",			pConts->m_strComment[4]);
			reply.getItem("comment6",			pConts->m_strComment[5]);
			reply.getItem("comment7",			pConts->m_strComment[6]);
			reply.getItem("comment8",			pConts->m_strComment[7]);
			reply.getItem("comment9",			pConts->m_strComment[8]);
			reply.getItem("comment10",			pConts->m_strComment[9]);
			reply.getItem("bgColor",			pConts->m_strBGColor);
			reply.getItem("fgColor",			pConts->m_strFGColor);
			reply.getItem("font",				pConts->m_strFont); 
			reply.getItem("promotionValueList",	listValue);
			pConts->m_strPromotionValueList	= listValue.toString();
			reply.getItem("fontSize",			pConts->m_sFontSize);
			reply.getItem("playSpeed",			pConts->m_sPlaySpeed);
			reply.getItem("soundVolume",		pConts->m_sSoundVolume);
			reply.getItem("contentsType",		pConts->m_ulContentsType);
			reply.getItem("contentsState",		pConts->m_ulContentsState);
			reply.getItem("runningTime",		pConts->m_ulRunningTime);
			reply.getItem("volume",				pConts->m_ulVolume);

			ciDEBUG(1, ("contentsId=%s",	pConts->m_strID.c_str()));
			ciDEBUG(1, ("contentsName=%s",	pConts->m_strContentsName.c_str()));
			ciDEBUG(1, ("location=%s",		pConts->m_strLocation.c_str()));
			ciDEBUG(1, ("filename=%s",		pConts->m_strFilename.c_str()));
			ciDEBUG(1, ("contentsType=%d",	pConts->m_ulContentsType));
			ciDEBUG(1, ("volume=%d",		pConts->m_ulVolume));

			pConts->m_bDownLoad = true;

			//리스트에 추가
			m_listContents.push_back(pConts);
			
		}}//while

		if(reply_count == 0)
		{
			ciERROR(("Fail to get Contents Call"));
			return false; // call fail
		}//if

		return true; // ok
	}
	catch(CORBA::Exception& ex)
	{
		ciERROR(("Get Contents Call Exception"));
		return false; // call fail
	}//try
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Contents list를 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CEventReservation::ClearContentsList()
{
	ListContents::iterator itr;
	for(itr=m_listContents.begin(); itr!=m_listContents.end(); itr++)
	{
		CContents* pCont =  (*itr);
		if(pCont)
		{
			delete pCont;
			pCont = NULL;
		}//if
	}//for
	m_listContents.clear();
}

