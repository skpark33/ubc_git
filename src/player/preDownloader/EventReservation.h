/************************************************************************************/
/*! @file EventReservation.h
	@brief Reservation 이벤트의 속성을 갖는 클래스 선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/08/13\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2009/08/13:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#ifndef _EventReservation_h_
#define _EventReservation_h_

#include "Contents.h"

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>


typedef		list<CContents*>  ListContents;		///<다운로드할 컨텐츠 정보를 갖는 리스트

class CEventReservation
{
public:
	CEventReservation(ciString strReservationId,
						 ciString strSiteId,
						 ciString strProgramId,
						 ciShort sSide,
						 ciTime tmFrom,
						 ciTime tmTo);		///<생성자
	virtual ~CEventReservation();			///<소멸자

	ciString		m_strReservationId;		///<예약 Id
	ciString		m_strSiteId;			///<Site Id
	ciString		m_strProgramId;			///<Program Id
	ciShort			m_sSide;				///<Display side(1: 앞면, 2:뒷면, 3:양면)
	ciTime			m_tmFrom;				///<예약 시작시간
	ciTime			m_tmTo;					///<예약 종료시간
	bool			m_bComlete;				///<작업이 완료되었는지 여부
	ListContents	m_listContents;			///<다운로드할 컨텐츠 리스트

	//void			Save(bool bComplete);		///<예약작업 내용을 파일에 기록한다
	bool			GetContents(void);			///<Call을 해서 서버로부터 컨텐츠 객체를 가져온다.
	void			ClearContentsList(void);	///<Contents list를 정리한다.
};

#endif // _EventReservation_h_
