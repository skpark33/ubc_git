/************************************************************************************/
/*! @file Contents.h
	@brief Contents 객체 클래스 정의 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/06/19\n
	▶ 참고사항:
		
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2009/06/19:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#ifndef _Contents_h_
#define _Contents_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <cci/libValue/ccistringlist.h>

class CContents
{
public:
	CContents();
	virtual ~CContents() {};

	CContents& operator= (const CContents& clsConts)
	{
		m_strID					= clsConts.m_strID;
		m_strContentsName		= clsConts.m_strContentsName;
		m_strLocation			= clsConts.m_strLocation;
		m_strFilename			= clsConts.m_strFilename;
		m_strBGColor			= clsConts.m_strBGColor;
		m_strFGColor			= clsConts.m_strFGColor;
		m_strFont				= clsConts.m_strFont;
		m_strPromotionValueList	= clsConts.m_strPromotionValueList;

		m_sFontSize				= clsConts.m_sFontSize;
		m_sPlaySpeed			= clsConts.m_sPlaySpeed;
		m_sSoundVolume			= clsConts.m_sSoundVolume;
		m_ulContentsType			= clsConts.m_ulContentsType;
		m_ulContentsState		= clsConts.m_ulContentsState;
		m_ulRunningTime			= clsConts.m_ulRunningTime;
		m_ulVolume				= clsConts.m_ulVolume;

		m_direction				= clsConts.m_direction;
		m_align					= clsConts.m_align;

		m_width					= clsConts.m_width;
		m_height				= clsConts.m_height;
		m_currentComment		= clsConts.m_currentComment;

		m_bDownLoad				= clsConts.m_bDownLoad;

		m_strWizardXML			= clsConts.m_strWizardXML;
		m_strWizardFiles		= clsConts.m_strWizardFiles;
		
		for(int i=0; i<10; i++)
		{
			m_strComment[i]	= clsConts.m_strComment[i];
		}//for

		return *this;
	}

	bool operator== (const CContents & clsConts)
	{
		if(m_strID					!= clsConts.m_strID)					return false;
		if(m_strContentsName		!= clsConts.m_strContentsName)			return false;
		if(m_strLocation			!= clsConts.m_strLocation)				return false;
		if(m_strFilename			!= clsConts.m_strFilename)				return false;
		if(m_strBGColor				!= clsConts.m_strBGColor)				return false;
		if(m_strFGColor				!= clsConts.m_strFGColor)				return false;
		if(m_strFont				!= clsConts.m_strFont)					return false;

		if(m_sFontSize				!= clsConts.m_sFontSize)				return false;
		if(m_sPlaySpeed				!= clsConts.m_sPlaySpeed)				return false;
		if(m_sSoundVolume			!= clsConts.m_sSoundVolume)				return false;
		if(m_ulContentsType			!= clsConts.m_ulContentsType)			return false;
		if(m_ulContentsState			!= clsConts.m_ulContentsState)			return false;
		if(m_ulRunningTime			!= clsConts.m_ulRunningTime)			return false;
		if(m_ulVolume				!= clsConts.m_ulVolume)					return false;

		if(m_direction				!= clsConts.m_direction)				return false;
		if(m_align					!= clsConts.m_align)					return false;

		if(m_width					!= clsConts.m_width)					return false;
		if(m_height					!= clsConts.m_height)					return false;
		if(m_currentComment			!= clsConts.m_currentComment)			return false;

		if(m_strWizardXML			!= clsConts.m_strWizardXML)				return false;
		if(m_strWizardFiles			!= clsConts.m_strWizardFiles)			return false;

		for(int i=0; i<10; i++)
		{
			if(m_strComment[i] != clsConts.m_strComment[i])					return false;
		}//for

		if(m_strPromotionValueList	== clsConts.m_strPromotionValueList)
		{
			return true;
		}
		else
		{
			return false;
		}//if
	}

	bool operator!= (const CContents & clsConts)
	{
		if(m_strID					!= clsConts.m_strID)					return true;
		if(m_strContentsName		!= clsConts.m_strContentsName)			return true;
		if(m_strLocation			!= clsConts.m_strLocation)				return true;
		if(m_strFilename			!= clsConts.m_strFilename)				return true;
		if(m_strBGColor				!= clsConts.m_strBGColor)				return true;
		if(m_strFGColor				!= clsConts.m_strFGColor)				return true;
		if(m_strFont				!= clsConts.m_strFont)					return true;

		if(m_sFontSize				!= clsConts.m_sFontSize)				return true;
		if(m_sPlaySpeed				!= clsConts.m_sPlaySpeed)				return true;
		if(m_sSoundVolume			!= clsConts.m_sSoundVolume)				return true;
		if(m_ulContentsType			!= clsConts.m_ulContentsType)			return true;
		if(m_ulContentsState		!= clsConts.m_ulContentsState)			return true;
		if(m_ulRunningTime			!= clsConts.m_ulRunningTime)			return true;
		if(m_ulVolume				!= clsConts.m_ulVolume)					return true;

		if(m_direction				!= clsConts.m_direction)				return true;
		if(m_align					!= clsConts.m_align)					return true;

		if(m_width					!= clsConts.m_width)					return true;
		if(m_height					!= clsConts.m_height)					return true;
		if(m_currentComment			!= clsConts.m_currentComment)			return true;

		if(m_strWizardXML			!= clsConts.m_strWizardXML)				return true;
		if(m_strWizardFiles			!= clsConts.m_strWizardFiles)			return true;

		for(int i=0; i<10; i++)
		{
			if(m_strComment[i] != clsConts.m_strComment[i])					return true;
		}//for

		if(m_strPromotionValueList	== clsConts.m_strPromotionValueList)
		{
			return false;
		}
		else
		{
			return true;
		}//if
	} 

//protected:
	// attributes
	ciString		m_strID;					///<컨텐츠 ID
	ciString		m_strContentsName;			///<컨텐츠 이름
	ciString		m_strLocation;				///<서버의 컨텐츠 저장위치
	ciString		m_strFilename;				///<컨텐츠 파일이름
	ciString		m_strComment[10];			///<Comment
	ciString		m_strBGColor; 				///<background color
	ciString		m_strFGColor; 				///<foreground color
	ciString		m_strFont; 					///<font
	cciStringList	m_strPromotionValueList;
	ciShort			m_sFontSize; 				///<font size
	ciShort			m_sPlaySpeed; 				///<tiker 등이 플레이 되는 속도
	ciShort 		m_sSoundVolume;				///<Sound volume
	ciLong 			m_ulContentsType;			///<컨텐츠 종류
	ciLong			m_ulContentsState;			///<컨텐츠의 상태
	ciLong 			m_ulRunningTime;			///<컨텐츠 재생시간
	ciLong			m_ulVolume;					///<file size

	ciLong			m_direction;
	ciLong			m_align;

	ciShort			m_width;					// contents width (pixel)
	ciShort			m_height;					// contents height (pixel)
	ciShort			m_currentComment;			// 현재 문자가 들어가야할 자리번호

	ciString		m_strWizardXML;				// wizard 용 XML string
	ciString		m_strWizardFiles;			// wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

	bool			m_bDownLoad;				///<컨텐츠 파일을 다운로드 하는지 여부

	void			Load(LPCSTR lpszFullPath, LPCSTR lpszLoadID);
};


enum  CONTENTS_TYPE {
	CONTENTS_NOT_DEFINE = -1,
	CONTENTS_VIDEO = 0,
	CONTENTS_SMS,			//1. Horizontal Ticker
	CONTENTS_IMAGE,
	CONTENTS_PROMOTION,
	CONTENTS_TV,
	CONTENTS_TICKER,		//5. Vertical Ticker
	CONTENTS_PHONE,
	CONTENTS_WEBBROWSER,
	CONTENTS_FLASH,
	CONTENTS_WEBCAM,
	CONTENTS_RSS,			// 10.
	CONTENTS_CLOCK,
	CONTENTS_TEXT,			// 12. 이전 SMS
	CONTENTS_FLASH_TEMPLATE,
	CONTENTS_DYNAMIC_FLASH,
	CONTENTS_TYPING,
	CONTENTS_PPT,			//16
	CONTENTS_ETC,		// 2010.10.12 장광수 기타컨텐츠추가
	CONTENTS_WIZARD,	// 2010.10.21 정운형 마법사컨텐츠추가
};

#endif // _Contents_h_