#ifndef _preSocketSession_h_
#define _preSocketSession_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libBase/ciTime.h>
#include "reservedEventHandler.h"

////////////////////
class preSocketSession;

class preSocketHandler : public ciThread {
public:
	static preSocketHandler*	getInstance();
	static void	clearInstance();

	virtual ~preSocketHandler() {}

	void setSession(preSocketSession* session) { _session=session;}
	void fini();
	void run();
	void destroy();
	void push(const char* recvMsg);

protected:
	preSocketHandler() : _session(0), _isAlive(ciTrue) {}
	static preSocketHandler*		_instance;
	static ciMutex			_instanceLock;

	ciMutex				_listLock;
	ciStringList		_list;
	ciBoolean			_isAlive;
	preSocketSession*	_session;
};

class preSocketSession : public ciThread {
public:
	preSocketSession(int port, reservedEvtHandler* handler);
	virtual ~preSocketSession();

	ciBoolean init();
	void fini();

	void run();
	void destroy();

	ciBoolean _processCmd(const char* recvMsg);

protected:
	reservedEvtHandler* _handler;
	ciBoolean _isAlive;
	int _serverFd;
	int _serverPort;
};

#endif // _preSocketSession_h_
