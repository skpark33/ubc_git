#ifndef _monitorTimer_H_
#define _monitorTimer_H_

#include <ci/libTimer/ciWinTimer.h>

class monitorTimer : public ciWinPollable {
public:
	monitorTimer(ciString& site, ciString& host);
	virtual ~monitorTimer();

	virtual void processExpired(ciString name, int counter, int interval);


protected:

	ciString _site;
	ciString _host;
};

#endif //_monitorTimer_H_
