/************************************************************************************/
/*! @file reservedEventHandler.h
	@brief scheduleReserved 이벤트 처리를 위한 객체선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/08/05\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2009/08/05:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#ifndef _reservedEventHandler_h_
#define _reservedEventHandler_h_

#include "EventReservation.h"

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

//#include <afxinet.h>를 헤더에서 include하면 오류가 난다
class CInternetSession;
class CFtpConnection;

typedef		list<CEventReservation*>  ListReservation;				///<Reservation 리스트
typedef		map<ciString, time_t>	EventHistory  ; // 가장 최근에 받은 이벤트를 종류별로 기록한다.

class reservedEvtHandler : public cciEventHandler
{
public:
	reservedEvtHandler(const char* siteId, const char* hostId);					///<생성자
	virtual ~reservedEvtHandler();												///<소멸자

   	virtual void 	processEvent(cciEvent& pEvent);								///<이벤트 처리 함수
	ciBoolean		addHand();													///<이벤트 등록 함수

	void			GetReservationFromServer(void);								///<이벤트를 등록하기전에 서버에서 예약된 정보를 가져온다.

	void			ClearReservationList(void);									///<Reservation list를 정리한다
	bool			GetDownloadReservation(CEventReservation** pReservation);	///<Download를 진행할 예약정보 가져온다.

	void			SetEventHistory(time_t eventTime,   // 최근에 받은 예약 Event 이력을 기록한다.  5분간만 유지한다.
								const char* eventName,    
								const char* reservationId,
								const char* programId,
								ciShort side,
								ciBoolean isStart=1);

	bool			IsAlredyReceived(time_t eventTime,		// 이미 받은 이벤트를 또 받은것은 아닌가 살핀다.
								const char* eventName,
								const char* reservationId,
								const char* programId,
								ciShort side,
								ciBoolean isStart=1);
	void			ClearEventHistory();	// EventHistory 를 지운다.


	bool			AddNewReservation(ciString strReservationId,
										ciString strSiteId,
										ciString strProgramId,
										ciShort sSide,
										ciTime tmFrom,
										ciTime tmTo);							///<새로운 Reservation을 추가한다.
	bool			ExpireReservation(ciString reservationId,
										ciBoolean isStart,
										ciString programId,
										ciShort side,
										ciTime fromTime,
										ciTime toTime,
										ciString removedProgramId,
										ciULong eventTime);							///Expired Reservation을 처리한다.
	bool			ReservationRemoved(ciString strReservationId,
									ciString strProgramId,
									ciShort sSide);								///<ReservationRemoved 이벤트를 처리한다.

	int				GetReservationCount(void);									///<등록된 reservation 수를 반환한다.
	bool			CheckReservation(ciString strReservationId);				///<예약작업이 등록되어있는지 확인한다.
	void			DeleteReservation(ciString strReservationId,
										bool bDeleteConfig = false);			///<해당하는 Id의 reservation을 지운다.
	void			SetReservationComplete(ciString strReservationId);			///<예약작업이 완료된 상태로 설정한다

	bool			GetFTPConnection(CInternetSession* pssInternet,
									CFtpConnection** pConnection,
									const char* programSiteId = 0);				///<FTP connection을 맺는다.
	bool			CheckDownloadFile(CFtpConnection* pConnection,
										CEventReservation* pReservation,
										bool& bCancel);							///<전체 컨텐츠 리스트에서 Download 해야하는 파일을 확인한다.
	bool			FTPDownLoad(CFtpConnection* pConnection,
									CEventReservation* pReservation,
									bool& bCancel);								///<컨텐츠 파일을 다운로드한다
	bool			GetDynamicFlashSubFiles(CFtpConnection* pConnection,
									CEventReservation* pReservation,
									ListContents* plistSub,
									bool& bCancel);								///<Dynamic Flash 컨텐츠의 SUB 파일들을 list에 추가한다.
	bool			GetProgramFile(CFtpConnection* pConnection,
									ciString strProgramId);						///<program file(ini)을 받는다


	void			GetLocalPath(ciString strFileName,
									ciString& strLocalPath,
									ciString& strVirtualPath,
									bool bContents = true);						///<컨텐츠 파일의 로컬 경로와 버추얼폴더(X 드라이브) 경로를 만들어 준다
	void			GetVirtualPath(ciString strFileName,
									ciString& strVirtualPath,
									ciString& strVirtualTmpPath);				///<컨텐츠 파일의 버추얼폴더(X 드라이브) 경로를 만들어 준다

	void			ReservationExpired(ciString strReservationId,
									bool isStart,
									ciString strProgramId,
									ciShort sSide,
									time_t onTime);								///<ReservationExpired 이벤트를 처리한다.

	void			SetWorkingDownLoad(bool bWork);								///<download thread를 진행하는 m_bWorkingDownload값을 설정
	bool			GetWorkingDownLoad(void);									///<download thread를 진행하는 m_bWorkingDownload값을 반환
	void			SetWorkingReservationId(ciString strReservationId);			///<download를 진행하는 ReservationId를 설정
	ciString		GetWorkingReservationId(void);								///<download를 진행하는 ReservationId를 반환

	bool			CopyContentsFile(ciString strPrgId);						///<컨텐츠 파일을 버추얼드라이브(X)에서 실행드라이브로 복사한다.

	void			AddEtcListToContentsList(CEventReservation* pReservation);	///<프로그램파일에서 Etc 파일 리스트를 읽어 Contents 리스트에 추가한다.
	bool			IsFontFile(ciString strFileName);							///<폰트파일인지 구분하여 준다.
	bool			RegisterFont(ciString strFontName, ciString strFontPath);	///<폰트를 시스템이 등록한다.

public:
	ciString		m_siteId;				///<물리적인 SiteId
	ciString		m_hostId;				///<물리적인 Hostid
	ciString		m_strUBCVariablePath;	///<UBC Variables ini 파일의 경로

	ciMutex			m_mutexReservation;		///<Reservation 동기화 객체
	ListReservation	m_listReservation;		///<Reservation 리스트

	HANDLE			m_hThreadMain;			///<메인 thread 핸들
	HANDLE			m_hThreadDownload;		///<컨텐츠 download thread 핸들

	bool			m_bExitMainThread;		///<메인 thread를 종료하는지 여부

protected:
	ciMutex			m_mutexWork;			///<m_bWorkingDownload 동기화 객체
	bool			m_bWorkingDownload;		///<현재 download thread가 진행중인지 여부

	ciMutex			m_mutexReservationId;	///<m_strReservationId 동기화 객체
	ciString		m_strReservationId;		///<현재 다운로드중인 ReservationId

	ciString		m_strConfigPath;		///<예약정보를 기록하는 ini 파일의 경로
	ciString		m_strProgramFile;		///<임시경로에 받은 programfile 경로

	EventHistory	m_eventHistory;			// 최근 10분동안 받은 이벤트를 보관한다.
	ciMutex			m_eventHistoryLock;

};


#endif // _reservedEventHandler_h_
