/************************************************************************************/
/*! @file Contents.cpp
	@brief Contents 객체 클래스 구현 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/06/19\n
	▶ 사용파일: stdafx.h, BRW2.h, Host.h, Contents.h\n
	▶ 시스템 리소스: \n
	▶ 참고사항:
		- <참고사항 기술>
		- <참고사항 기술>

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2009/06/19:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#include "Contents.h"

#define BUF_SIZE		1024


CContents::CContents()
	:m_strID("")
	,m_strContentsName("")
	,m_strLocation("")
	,m_strFilename("")
	,m_strBGColor("")	
	,m_strFGColor("")	
	,m_strFont("")
	,m_strPromotionValueList("")
	,m_sFontSize(0) 		
	,m_sPlaySpeed(0)		
	,m_sSoundVolume(0)
	,m_ulContentsType(0)
	,m_ulContentsState(0)
	,m_ulRunningTime(0)
	,m_ulVolume(0)
	,m_bDownLoad(true)
	,m_direction(0)
	,m_align(5)
	,m_width(0)
	,m_height(0)
	,m_currentComment(0)
	,m_strWizardXML("")
	,m_strWizardFiles("")
{
	for(int i=0; i<10; i++) m_strComment[i] = "";
}


void CContents::Load(LPCSTR lpszFullPath, LPCSTR lpszLoadID)
{
	char buf[BUF_SIZE];

	::GetPrivateProfileString(lpszLoadID, "contentsId", "", buf, BUF_SIZE, lpszFullPath);			m_strID = buf;
	::GetPrivateProfileString(lpszLoadID, "contentsName", "", buf, BUF_SIZE, lpszFullPath);			m_strContentsName = buf;
	::GetPrivateProfileString(lpszLoadID, "location", "", buf, BUF_SIZE, lpszFullPath);				m_strLocation = buf;
	::GetPrivateProfileString(lpszLoadID, "filename", "", buf, BUF_SIZE, lpszFullPath);				m_strFilename = buf;
	::GetPrivateProfileString(lpszLoadID, "bgColor", "", buf, BUF_SIZE, lpszFullPath);				m_strBGColor = buf;
	::GetPrivateProfileString(lpszLoadID, "fgColor", "", buf, BUF_SIZE, lpszFullPath);				m_strFGColor = buf;
	::GetPrivateProfileString(lpszLoadID, "font", "", buf, BUF_SIZE, lpszFullPath);					m_strFont = buf;
	::GetPrivateProfileString(lpszLoadID, "promotionvalueList", "", buf, BUF_SIZE, lpszFullPath);	m_strPromotionValueList.fromString(buf);

	::GetPrivateProfileString(lpszLoadID, "fontSize", "", buf, BUF_SIZE, lpszFullPath);				m_sFontSize = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "playSpeed", "", buf, BUF_SIZE, lpszFullPath);			m_sPlaySpeed = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "soundVolume", "", buf, BUF_SIZE, lpszFullPath);			m_sSoundVolume = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "contentsType", "", buf, BUF_SIZE, lpszFullPath);			m_ulContentsType = atol(buf);
	::GetPrivateProfileString(lpszLoadID, "contentsState", "", buf, BUF_SIZE, lpszFullPath);		m_ulContentsState = atol(buf);
	::GetPrivateProfileString(lpszLoadID, "runningTime", "", buf, BUF_SIZE, lpszFullPath);			m_ulRunningTime = atol(buf);
	//::GetPrivateProfileString(lpszLoadID, "volume", "", buf, BUF_SIZE, lpszFullPath);				m_ulVolume = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "volume", "", buf, BUF_SIZE, lpszFullPath);				m_ulVolume = (ULONGLONG)_atoi64(buf);

	::GetPrivateProfileString(lpszLoadID, "direction", "", buf, BUF_SIZE, lpszFullPath);			m_direction = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "align", "", buf, BUF_SIZE, lpszFullPath);				m_align = atoi(buf);

	::GetPrivateProfileString(lpszLoadID, "width", "", buf, BUF_SIZE, lpszFullPath);				m_width = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "height", "", buf, BUF_SIZE, lpszFullPath);				m_height = atoi(buf);
	::GetPrivateProfileString(lpszLoadID, "currentComment", "", buf, BUF_SIZE, lpszFullPath);		m_currentComment = atoi(buf);

	::GetPrivateProfileString(lpszLoadID, "wizardXML", "", buf, BUF_SIZE, lpszFullPath);			m_strWizardXML = buf;
	::GetPrivateProfileString(lpszLoadID, "wizardFiles", "", buf, BUF_SIZE, lpszFullPath);			m_strWizardFiles = buf;

	::GetPrivateProfileString(lpszLoadID, "comment1", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[0] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment2", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[1] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment3", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[2] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment4", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[3] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment5", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[4] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment6", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[5] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment7", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[6] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment8", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[7] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment9", "", buf, BUF_SIZE, lpszFullPath);				m_strComment[8] = buf;
	::GetPrivateProfileString(lpszLoadID, "comment10", "", buf, BUF_SIZE, lpszFullPath);			m_strComment[9] = buf;
}
