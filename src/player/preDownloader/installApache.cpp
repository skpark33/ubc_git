#include <afx.h>
#include "afxwin.h"
#include "afxext.h"
#include <afxdisp.h>        // MFC Automation classes

#include "common/libFileServiceWrap/FileServiceWrapDll.h"
#include "InstallApache.h"
#include <ci/libDebug/ciDebug.h>
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/ScratchUtil.h"
#include "ci/libFile/ciFileUtil.h"

#define APACHE_HOME "\\3rdparty\\Apache2.2"
#define LOCAL_HOME  _COP_CD("C:\\SQISOFT");
#define NO_OF_APACHE_FILE	2187

ciSET_DEBUG(10,"CInstallApache");

bool
CInstallApache::download()
{
	if(isInstalled() == true) {
		return true;
	}
	ubcConfig aIni("UBCConnect");
	ciString centerIp;
	aIni.get("UBCCENTER","FTP_IP", centerIp);
	ciShort	port=8080;
	aIni.get("UBCCENTER","HTTPPORT", port);
	if(port==0){
		port = 8080;
	}
	ciString id;
	aIni.get("UBCCENTER","FTPID", id);
	ciString pwd;
	aIni.get("UBCCENTER","FTPPASSWD", pwd);

	ciDEBUG(1,("CenterIP=%s, port=%ld, id=%s, pwd=%s", centerIp.c_str(), port, id.c_str(), pwd.c_str()));

	if(centerIp.empty()){
		ciERROR(("[UBCCENTER]FTP_IP get failed."));
		return false;
	}
	if(id.empty()){
		ciERROR(("[UBCCENTER]FTPID get failed."));
		return false;
	}
	if(pwd.empty()){
		ciERROR(("[UBCCENTER]FTPPASSWD get failed."));
		return false;
	}

	CFileServiceWrap objFileSvcClient(centerIp.c_str(), port);

	if(!objFileSvcClient.Login(id.c_str(), pwd.c_str()))
	{
		ciERROR((objFileSvcClient.GetErrorMsg()));
		return false;
	}

	ciString localPath = LOCAL_HOME;
	localPath += APACHE_HOME;
	ciString remotePath = APACHE_HOME;

	ciString listFile = "FileList.txt";

	ciString localFile = localPath + "\\" + listFile;
	ciString remoteFile = remotePath + "\\" + listFile;

	bool bRet = objFileSvcClient.GetFile(remoteFile.c_str(), localFile.c_str(), 0, NULL);
	if(!bRet)
	{
		ciERROR((objFileSvcClient.GetErrorMsg()));
		return false;
	}
	ciDEBUG(1,("FileList download succeed"));

    FILE* fd = fopen(localFile.c_str(), "r");
	if(!fd) {
		ciERROR(("(%s) file open failed", localFile.c_str()));
	    return false;
    }

	int counter=0;
	char line[1024];
	memset(line, 0x00, sizeof(line));
    while(fgets(line, 1024-1, fd) != NULL) {
        char *ptr = line;
        for( ;*ptr == ' ' || *ptr == '\t' || *ptr == '.'; ptr++ );
        if( *ptr == '#' || *ptr == '\0' || *ptr == '\n' ) {
			memset(line, 0x00, sizeof(line));
            continue;
        }
        ciStringUtil::rightTrim(ptr);

		localFile = localPath + ptr;
		remoteFile = remotePath + ptr;
		bRet = objFileSvcClient.GetFile(remoteFile.c_str(), localFile.c_str(), 0, NULL);
		if(!bRet)
		{
			ciERROR((objFileSvcClient.GetErrorMsg()));
		}
		else
		{
			ciDEBUG(1,("%s file download succeed", localFile.c_str()));
			counter++;
		}
		memset(line, 0x00, sizeof(line));
	}
	fclose(fd);
	objFileSvcClient.Logout();

	ciDEBUG(1,("%d files are downloaded", counter));
	if(counter < NO_OF_APACHE_FILE){ // 아파치 파일은 2187 개다.
		ciWARN(("Number of download file less than %d !!!", NO_OF_APACHE_FILE));
	}
	return true;
}


CInstallApache::CACHE_TYPE
CInstallApache::getType()
{
	ubcConfig aIni("UBCVariables");
	ciString cacheServer;
	aIni.get("ROOT","ContentsCacheServer", cacheServer);

	if(cacheServer.empty()){
		ciDEBUG(1,("This does not use Cache"));
		return NO_CACHE;
	}
	if(cacheServer == "localhost"){
		ciDEBUG(1,("This is ContentsCacheServer"));
		return CACHE_SERVER;
	}
	ciDEBUG(1,("This is ContentsCacheClient from %s", cacheServer.c_str()));
	return CACHE_CLIENT;
}

bool
CInstallApache::isInstalled()
{
	ciString httpdFile= LOCAL_HOME;
	httpdFile += APACHE_HOME;
	/*
	httpdFile += "\\bin\\httpd.exe";

	if(::access(httpdFile.c_str(),0) == 0){
		ciDEBUG(1,("Apache already exist"));
		return ciTrue;
	}
	*/
	ciStringList outList;
	int count =  ciFileUtil::listDir(httpdFile.c_str(), outList);
	if(count < NO_OF_APACHE_FILE){
		ciERROR(("%d Apache file is missing", NO_OF_APACHE_FILE-count));
		return false;
	}
	ciDEBUG(1,("Apache already exist"));
	return true;
}

bool
CInstallApache::isRegisterStartMenu()
{
	char TargetPath[MAX_PATH+1];
	::ZeroMemory(TargetPath, sizeof(TargetPath));
	::SHGetSpecialFolderPath (NULL, TargetPath, CSIDL_STARTUP, FALSE);

	ciString startFile = TargetPath;
	startFile += "\\StartContentsCacheServer.lnk";

	if(::access(startFile.c_str(),0) == 0){
		ciDEBUG(1,("%s file already exist", startFile.c_str()));
		return true;
	}

	ciDEBUG(1,("%s file does not exist", startFile.c_str()));
	return false;
}
bool
CInstallApache::registerStartMenu()
{
	if(isRegisterStartMenu()) return true;

	char TargetPath[MAX_PATH+1];
	::ZeroMemory(TargetPath, sizeof(TargetPath));
	::SHGetSpecialFolderPath (NULL, TargetPath, CSIDL_STARTUP, FALSE);

	ciString target = TargetPath;
	target += "\\StartContentsCacheServer.lnk";

	ciString source = LOCAL_HOME;
	source += APACHE_HOME;
	source += "\\StartContentsCacheServer.lnk";

	if(!::CopyFile(source.c_str(), target.c_str(), FALSE)){
		ciERROR(("%s --> %s file copy failed", source.c_str(), target.c_str()));
		return false;
	}
	ciDEBUG(1, ("%s --> %s file copy succeed", source.c_str(), target.c_str()));
	return true;
}

bool
CInstallApache::unregisterStartMenu()
{
	if(!isRegisterStartMenu()) return true;

	char TargetPath[MAX_PATH+1];
	::ZeroMemory(TargetPath, sizeof(TargetPath));
	::SHGetSpecialFolderPath (NULL, TargetPath, CSIDL_STARTUP, FALSE);

	ciString target = TargetPath;
	target += "\\StartContentsCacheServer.lnk";

	if(!::DeleteFile(target.c_str())){
		ciERROR(("%s file delete failed", target.c_str()));
		return false;
	}
	ciDEBUG(1, ("%s file delete succeed", target.c_str()));
	return true;
}
bool
CInstallApache::isRegisterFirewall()
{
	ciString httpdFile= LOCAL_HOME;
	httpdFile += APACHE_HOME;
	httpdFile += "\\bin\\httpd.exe";

	bool bRet = scratchUtil::getInstance()->IsAppExceptionAdded(httpdFile);
	if(bRet){
		ciDEBUG(1,("%s already registered in firewallExceptionList", httpdFile.c_str()));
	}else{
		ciDEBUG(1,("%s does not registered in firewallExceptionList", httpdFile.c_str()));
	}
	return bRet;
}
bool
CInstallApache::registerFirewall()
{
	if(isRegisterFirewall()) return true;
	
	ciString httpdFile= LOCAL_HOME;
	httpdFile += APACHE_HOME;
	httpdFile += "\\bin\\httpd.exe";

	bool bRet = scratchUtil::getInstance()->AddToExceptionList(httpdFile);
	if(bRet){
		ciDEBUG(1,("httpd.exe registered in firewallExceptionList succeed"));
	}else{
		ciDEBUG(1,("httpd.exe registered in firewallExceptionList failed"));
	}
	return bRet;
}
bool
CInstallApache::unregisterFirewall()
{
	if(!isRegisterFirewall()) return true;

	ciString httpdFile= LOCAL_HOME;
	httpdFile += APACHE_HOME;
	httpdFile += "\\bin\\httpd.exe";

	bool bRet = scratchUtil::getInstance()->RemoveFromExceptionList(httpdFile);
	if(bRet){
		ciDEBUG(1,("httpd.exe unregistered in firewallExceptionList succeed"));
	}else{
		ciDEBUG(1,("httpd.exe unregistered in firewallExceptionList failed"));
	}
	return bRet;
}

unsigned long
CInstallApache::isRun()
{
	unsigned long  pid = scratchUtil::getInstance()->getPid("httpd.exe");
	if(pid > 0){
		ciDEBUG(1,("httpd.exe already run"));
		return pid;
	}
	ciDEBUG(1,("httpd.exe does not run "));
	return pid;
}
bool
CInstallApache::run()
{
	if(isRun() > 0) return true;

	ciString httpdFile= "";
	httpdFile += LOCAL_HOME;
	httpdFile += APACHE_HOME;
	httpdFile += "\\bin\\httpd.exe";

	ciString arg = " -w -f ";
	arg += LOCAL_HOME;
	arg += APACHE_HOME;
	arg += "\\conf\\httpd.conf -d ";
	arg += LOCAL_HOME;
	arg += APACHE_HOME;
	arg += "\\.";

	unsigned long pid = scratchUtil::getInstance()->createProcess(httpdFile.c_str(),arg.c_str(),0,true);
	if(pid > 0){
		ciDEBUG(1,("%s %s  run succeed", httpdFile.c_str(), arg.c_str()));
		runManually = true;
		return true;
	}
	ciERROR(("%s %s  run failed", httpdFile.c_str(), arg.c_str()));
	return false;
}
bool
CInstallApache::kill()
{
	bool	retval = true;
	for(int i=0;i<4;i++){
		int pid = isRun();
		if(pid < 0){
			ciDEBUG(1,("httpd.exe does not run"));
			return true;
		}
		if(scratchUtil::getInstance()->killProcess(pid)){
			ciDEBUG(1,("httpd.exe kill succeed"));
		}else{
			ciDEBUG(1,("httpd.exe kill failed"));
			retval = false;
		}
	}
	return retval;
}