 /*! \file preDownloaderWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _preDownloaderWorld_h_
#define _preDownloaderWorld_h_


#include <cci/libWorld/cciORBWorld.h> 
#include "preSocketSession.h"
#include "reloadEventHandler.h"
#include "reservedEventHandler.h"
#include "libAnnounce/announceEventHandler.h"
#include "common/libCommon/ubcHost.h"

class ubcHostView;
class monitorTimer;
class powerOnTimer;

class preDownloaderWorld : public cciORBWorld {
public:
	preDownloaderWorld();
	~preDownloaderWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);



protected:
	ciBoolean		_connectTest();

	void			_addHand(const char* host, const char* site);
	void			_createMutexLock();
	ubcHostInfo*	_getHostInfo(ubcHost& aHost, const char* host, const char* site);
	ciBoolean		_getCurrentSchedule(ubcHostInfo* aHostInfo, int displayCounter);
	ciBoolean		_getSiteId(const char* host, ciString& site, ubcHostView* pView);
	ciBoolean		_hostInfoProcess(ubcHostView* pView);
	ciBoolean		_changeVolume(ciShort volume);
	ciBoolean		_changeMute(ciShort mute);


	void			_toUTF8(ciString& xmlBuf);

	reloadEventHandler*		_eventHandler;
	reservedEvtHandler*		_evtHandlerReserved;
	announceEventHandler*		_evtHandlerAnnounce;
	monitorTimer*				_monitorTimer;
	powerOnTimer*				_powerOnTimer;

	preSocketSession* _session;
	ciBoolean	_eventRegister;

//	static ciLong _eventId;
};
		
#endif //_preDownloaderWorld_h_
