// UBCConfigWizard.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "UBCConfigWizard.h"
#include "UBCConfigWizardDlg.h"

#include "UpdateDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUBCConfigWizardApp

BEGIN_MESSAGE_MAP(CUBCConfigWizardApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CUBCConfigWizardApp 생성

CUBCConfigWizardApp::CUBCConfigWizardApp()
{
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
	m_hMutex = NULL;
}


// 유일한 CUBCConfigWizardApp 개체입니다.

CUBCConfigWizardApp theApp;


BOOL SetThreadLocaleEx(LCID locale)
{
	USES_CONVERSION;
	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if(::GetVersionEx(&osv) == FALSE) //get Version Failed 
		return ::SetThreadLocale(locale);

	switch(osv.dwMajorVersion)
	{
	case 6:  //WINDOWS VISTA
		{
			unsigned (__stdcall* SetThreadUILanguage)(LANGID);
			CStringA strThreaUILanguage = T2A(_T("SetThreadUILanguage"));

			HINSTANCE hIns = ::LoadLibrary(_T("kernel32.dll"));  
			if(hIns == NULL) //no Instance
				return ::SetThreadLocale(locale);

			SetThreadUILanguage = (unsigned (__stdcall* )(LANGID))::GetProcAddress(hIns,strThreaUILanguage);   
			if(SetThreadUILanguage == NULL) //procAddress Getfailed 
				return ::SetThreadLocale(locale);

			LANGID lang = SetThreadUILanguage(locale);
			return (lang==LOWORD(locale))?TRUE:FALSE;
		} 
	default: //WINDOWS 2000, XP
		return ::SetThreadLocale(locale);
	}

}

// CUBCConfigWizardApp 초기화

BOOL CUBCConfigWizardApp::InitInstance()
{

	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if(!AfxOleInit())
	{
		AfxMessageBox("OLE initialization failed.  Make sure that the OLE libraries are the correct version.");
		return FALSE;
	}//if

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("SQISoft"));

	if( CheckDuplicateProcess() ) return FALSE;

	bool find_up_update = false;
	for(int i=1; i<__argc; i++)
	{
		if( _tcsicmp(__argv[i], "+noupdate")==0 )
			find_up_update = true;
	}
	if( !find_up_update )
	{
		CUpdateDlg update_dlg;
		update_dlg.DoModal();
	}

	// arguments - language
	LANGID lang_id = PRIMARYLANGID(GetSystemDefaultLangID());
	for(int i=1; i<__argc; i++)
	{
		if(      _tcsicmp(__argv[i], "+eng")==0 )	lang_id = LANG_ENGLISH;
		else if( _tcsicmp(__argv[i], "+jpn")==0 )	lang_id = LANG_JAPANESE;
		else if( _tcsicmp(__argv[i], "+kor")==0 )	lang_id = LANG_KOREAN;
	}

	switch(lang_id)
	{
	case LANG_KOREAN:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_KOREAN, SUBLANG_KOREAN) , SORT_DEFAULT));
		break;
	case LANG_JAPANESE:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_JAPANESE, SORT_JAPANESE_UNICODE) , SORT_DEFAULT));
		break;
	default:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
		break;
	}

	CUBCConfigWizardDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

bool CUBCConfigWizardApp::CheckDuplicateProcess()
{
	m_hMutex = CreateMutex(NULL, FALSE, "UBC Config Wizard");

	if( GetLastError() == ERROR_ALREADY_EXISTS )
	{
		CloseHandle(m_hMutex);

		CString str_title;
		str_title.LoadString(AFX_IDS_APP_TITLE);

		CWnd* pWnd = CWnd::FindWindow(NULL, str_title);
		if( pWnd )
		{
			pWnd->ShowWindow(SW_RESTORE);
			pWnd->SetForegroundWindow();
			//exit(0);

			return true;
		}
	}

	return false;
}

int CUBCConfigWizardApp::ExitInstance()
{
	if( m_hMutex ) ::ReleaseMutex(m_hMutex);

	return CWinApp::ExitInstance();
}
