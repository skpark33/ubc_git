// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <afxsock.h>		// MFC 소켓 확장

#include <afxinet.h>


#define		WM_CHANGE_FOCUS			(WM_USER+9999)

#define		WM_END_EDIT				(WM_USER+10000)
#define		WM_CANCEL_EDIT			(WM_USER+10001)

#define		WM_END_COMBOBOX			(WM_USER+10002)
#define		WM_CANCEL_COMBOBOX		WM_CANCEL_EDIT


#define		FOR_GC(LIST,ITER)			int cnt_##ITER=LIST.GetCount();		for(int ITER=0; ITER<cnt_##ITER; ITER++)
#define		FOR_GIC(LIST,ITER)			int cnt_##ITER=LIST.GetItemCount();	for(int ITER=0; ITER<cnt_##ITER; ITER++)

#define		FOR_GC_REV(LIST,ITER)		int cnt_##ITER=LIST.GetCount();		for(int ITER=cnt_##ITER-1; ITER>=0; ITER--)
#define		FOR_GIC_REV(LIST,ITER)		int cnt_##ITER=LIST.GetItemCount();	for(int ITER=cnt_##ITER-1; ITER>=0; ITER--)


//
class VALUE_ITEM
{
public:
	VALUE_ITEM() { bIsDefault=false; };
	~VALUE_ITEM() {};

	CString		strName;
	CString		strValue;
	bool		bIsDefault;
};

typedef CArray<VALUE_ITEM*, VALUE_ITEM*>		VALUES_LIST;


//
class SETTING_ITEM
{
public:
	SETTING_ITEM() { bModified=false; };
	~SETTING_ITEM() {
		int cnt = listValues.GetCount();
		for(int i=0; i<cnt; i++)
		{
			VALUE_ITEM* val_item = listValues.GetAt(i);
			if(val_item) delete val_item;
		}
		listValues.RemoveAll();
	};

	CString		strFilename;
	CString		strSection;
	CString		strKey;
	CString		strTitle;
	CString		strValueType;
	VALUES_LIST	listValues;

	CString		strNewValue;
	CString		strInitValue;
	bool		bModified;
};

typedef CArray<SETTING_ITEM*, SETTING_ITEM*>	SETTINGS_LIST;


//
typedef struct
{
	int row;
	int col;
	CString* value;
	CWnd* control;
} EDIT_RESULT;


//
typedef struct
{
	int row;
	int col;
	VALUE_ITEM* value;
	CWnd* control;
} COMBOBOX_RESULT;


int UTF8ToAnsi( char* szSrc, char* strDest, int destSize );
LPCTSTR GetConfigDataPath();
CString LoadString(UINT nID);

//#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
//#endif


