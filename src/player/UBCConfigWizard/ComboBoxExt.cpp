// ComboBoxExt.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCConfigWizard.h"
#include "ComboBoxExt.h"


// CComboBoxExt

IMPLEMENT_DYNAMIC(CComboBoxExt, CComboBox)

CComboBoxExt::CComboBoxExt(CWnd* pParent, CRect& rect, DWORD dwStyle, UINT nID, int nRow, int nCol)
:	m_nRow (nRow)
,	m_nCol (nCol)
{
	m_bEndEdit = false;

	Create(WS_VISIBLE | WS_CHILD | WS_TABSTOP | WS_GROUP | WS_VSCROLL | CBS_DROPDOWNLIST | CBS_AUTOHSCROLL, rect, pParent, nID);

	SetFont(pParent->GetFont());
	SetFocus();
}

CComboBoxExt::~CComboBoxExt()
{
}


BEGIN_MESSAGE_MAP(CComboBoxExt, CComboBox)
	ON_WM_KILLFOCUS()
	ON_CONTROL_REFLECT(CBN_SELCHANGE, &CComboBoxExt::OnCbnSelchange)
	ON_CONTROL_REFLECT(CBN_CLOSEUP, &CComboBoxExt::OnCbnCloseup)
END_MESSAGE_MAP()


// CComboBoxExt 메시지 처리기입니다.


BOOL CComboBoxExt::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE )
	{
		CancelEdit();
		return TRUE;
	}
	if( pMsg->message == WM_KEYDOWN && ( pMsg->wParam == VK_TAB || pMsg->wParam == VK_RETURN ) )
	{
		GetParent()->SetFocus();
	}

	return CComboBox::PreTranslateMessage(pMsg);
}

void CComboBoxExt::OnKillFocus(CWnd* pNewWnd)
{
	CComboBox::OnKillFocus(pNewWnd);

	EndEdit();
}

void CComboBoxExt::CancelEdit()
{
	if( m_bEndEdit ) return;
	m_bEndEdit = true;

	if( IsWindow(GetSafeHwnd()) )
	{
		m_bEndEdit = true;
		CWnd* pOwner = GetOwner();
		if( pOwner )
			pOwner->PostMessage(WM_CANCEL_COMBOBOX, (WPARAM)(CWnd*)this, NULL );
	}
}

void CComboBoxExt::EndEdit()
{
	if( m_bEndEdit ) return;
	m_bEndEdit = true;

	CWnd* pOwner = GetOwner();
	if( pOwner )
	{
		COMBOBOX_RESULT* cr = new COMBOBOX_RESULT;

		cr->row = m_nRow;
		cr->col = m_nCol;
		cr->value = (VALUE_ITEM*)GetItemData(GetCurSel());
		cr->control = this;

		pOwner->PostMessage(WM_END_COMBOBOX, (WPARAM)cr, NULL );
	}
}

void CComboBoxExt::OnCbnSelchange()
{
	//EndEdit();
}

void CComboBoxExt::OnCbnCloseup()
{
	EndEdit();
}

void CComboBoxExt::AddString(LPCSTR lpszString, VALUE_ITEM* pValueItem)
{
	int idx = CComboBox::AddString(lpszString);
	SetItemData(idx, (DWORD)pValueItem);
}

void CComboBoxExt::SetInitValue(LPCSTR lpszInitVal)
{
	int cnt = GetCount();
	for(int i=0; i<cnt; i++)
	{
		VALUE_ITEM* val_item = (VALUE_ITEM*)GetItemData(i);

		if( stricmp(val_item->strName, lpszInitVal)==0 )
		{
			SetCurSel(i);
			break;
		}
	}
}
