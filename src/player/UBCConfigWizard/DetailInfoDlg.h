#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CDetailInfoDlg 대화 상자입니다.

class CDetailInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CDetailInfoDlg)

public:
	CDetailInfoDlg(SETTING_ITEM* sItem, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDetailInfoDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DETAIL_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	SETTING_ITEM*	m_pSettingItem;
public:

	virtual BOOL OnInitDialog();
	CEdit		m_editFilename;
	CEdit		m_editSection;
	CEdit		m_editKey;
	CEdit		m_editTitle;
	CEdit		m_editValueType;
	CListCtrl	m_lcComboboxValues;
};
