// UBCConfigWizardDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "ListCtrlEx.h"
#include "ReposControl2.h"


// CUBCConfigWizardDlg 대화 상자


class CUBCConfigWizardDlg : public CDialog
{
// 생성입니다.
public:
	CUBCConfigWizardDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCCONFIGWIZARD_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()

	CReposControl2	m_repos;

	SETTINGS_LIST	m_listSettings;

	bool		InitXML();
	bool		InitList();

	bool		SaveSettings();

	void		ClearList();

	int			GetStringBetweenTag(CString& strSource, LPCSTR lpszStart, LPCSTR lpszEnd, CString& strSub, int nStartIdx=0);
	bool		GetSettingsList(CString& strSource, SETTINGS_LIST& listSettings);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnBnClickedButtonSaveSettings();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnChangeFocus(WPARAM wParam, LPARAM lParam);

	//CListCtrl	m_lcSettings;
	CListCtrlEx	m_lcSettings;
	CButton		m_btnSaveSettings;
	CButton		m_btnClose;

	afx_msg void OnNMDblclkListSettings(NMHDR *pNMHDR, LRESULT *pResult);
};
