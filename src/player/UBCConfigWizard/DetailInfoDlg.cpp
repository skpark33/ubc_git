// DetailInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCConfigWizard.h"
#include "DetailInfoDlg.h"


// CDetailInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDetailInfoDlg, CDialog)

CDetailInfoDlg::CDetailInfoDlg(SETTING_ITEM* sItem, CWnd* pParent /*=NULL*/)
	: CDialog(CDetailInfoDlg::IDD, pParent)
	, m_pSettingItem (sItem)
{

}

CDetailInfoDlg::~CDetailInfoDlg()
{
}

void CDetailInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FILENAME, m_editFilename);
	DDX_Control(pDX, IDC_EDIT_SECTION, m_editSection);
	DDX_Control(pDX, IDC_EDIT_KEY, m_editKey);
	DDX_Control(pDX, IDC_EDIT_TITLE, m_editTitle);
	DDX_Control(pDX, IDC_EDIT_VALUE_TYPE, m_editValueType);
	DDX_Control(pDX, IDC_LIST_COMBOBOX_VALUES, m_lcComboboxValues);
}


BEGIN_MESSAGE_MAP(CDetailInfoDlg, CDialog)
END_MESSAGE_MAP()


// CDetailInfoDlg 메시지 처리기입니다.

BOOL CDetailInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_lcComboboxValues.SetExtendedStyle(m_lcComboboxValues.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );
	m_lcComboboxValues.InsertColumn(0, ::LoadString(IDS_COLUMN_SETTINGS), 0, 200);
	m_lcComboboxValues.InsertColumn(1, ::LoadString(IDS_COLUMN_VALUE), 0, 50);

	if( m_pSettingItem )
	{
		m_editFilename.SetWindowText(m_pSettingItem->strFilename);
		m_editSection.SetWindowText(m_pSettingItem->strSection);
		m_editKey.SetWindowText(m_pSettingItem->strKey);
		m_editTitle.SetWindowText(m_pSettingItem->strTitle);

		int value_type = 0;
		if(      m_pSettingItem->strValueType == "combobox" ) value_type = IDS_VALUE_TYPE_COMBOBOX;
		else if( m_pSettingItem->strValueType == "integer" ) value_type = IDS_VALUE_TYPE_INTEGER;
		else if( m_pSettingItem->strValueType == "string" ) value_type = IDS_VALUE_TYPE_STRING;
		if( value_type ) m_editValueType.SetWindowText(::LoadString(value_type));

		FOR_GC(m_pSettingItem->listValues, i)
		{
			VALUE_ITEM* v_item = m_pSettingItem->listValues.GetAt(i);

			m_lcComboboxValues.InsertItem(i, v_item->strName);
			m_lcComboboxValues.SetItemText(i, 1, v_item->strValue);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
