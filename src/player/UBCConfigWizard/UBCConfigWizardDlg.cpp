// UBCConfigWizardDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCConfigWizard.h"
#include "UBCConfigWizardDlg.h"

#include "DetailInfoDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		SETTINGS_START_TAG		"<settings>"
#define		SETTINGS_END_TAG		"</settings>"

#define		ITEM_START_TAG			"<item>"
#define		ITEM_END_TAG			"</item>"

#define		VALUES_START_TAG		"<values>"
#define		VALUES_END_TAG			"</values>"

#define		LOCATION_START_TAG		"<location"
#define		LOCATION_END_TAG		"/>"
	
#define		FILE_START_TAG			"file=\""
#define		FILE_END_TAG			"\""

#define		SECTION_START_TAG		"section=\""
#define		SECTION_END_TAG			"\""

#define		KEY_START_TAG			"key=\""
#define		KEY_END_TAG				"\""

#define		TITLE_START_TAG			"<title>"
#define		TITLE_END_TAG			"</title>"

#define		VALUE_TYPE_START_TAG	"<type>"
#define		VALUE_TYPE_END_TAG		"</type>"

#define		VALUE_ITEM_START_TAG	"<item"
#define		VALUE_ITEM_END_TAG		"/>"

#define		VALUE_NAME_START_TAG	"name=\""
#define		VALUE_NAME_END_TAG		"\""

#define		VALUE_VALUE_START_TAG	"value=\""
#define		VALUE_VALUE_END_TAG		"\""

#define		VALUE_DEFAULT_START_TAG	"default=\""
#define		VALUE_DEFAULT_END_TAG	"\""


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CUBCConfigWizardDlg 대화 상자




CUBCConfigWizardDlg::CUBCConfigWizardDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCConfigWizardDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUBCConfigWizardDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SETTINGS, m_lcSettings);
	DDX_Control(pDX, IDC_BUTTON_SAVE_SETTINGS, m_btnSaveSettings);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
}

BEGIN_MESSAGE_MAP(CUBCConfigWizardDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_SAVE_SETTINGS, &CUBCConfigWizardDlg::OnBnClickedButtonSaveSettings)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CUBCConfigWizardDlg::OnBnClickedButtonClose)
	ON_WM_SIZE()
	ON_MESSAGE(WM_CHANGE_FOCUS, OnChangeFocus)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SETTINGS, &CUBCConfigWizardDlg::OnNMDblclkListSettings)
END_MESSAGE_MAP()


// CUBCConfigWizardDlg 메시지 처리기

BOOL CUBCConfigWizardDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	CString str_title;
	str_title.LoadString(AFX_IDS_APP_TITLE);
	SetWindowText(str_title);

	CenterWindow();

	m_repos.SetParent(this);
	m_repos.AddControl((CWnd*)&m_lcSettings, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnSaveSettings, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnClose, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	//
	InitXML();
	InitList();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUBCConfigWizardDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCConfigWizardDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCConfigWizardDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CUBCConfigWizardDlg::OnOK()
{
	//CDialog::OnOK();
}

void CUBCConfigWizardDlg::OnCancel()
{
	//CDialog::OnCancel();
}

bool CUBCConfigWizardDlg::InitXML()
{
	// open file
	CFile file;
	if( !file.Open("TerminalSettings.xml", CFile::modeRead | CFile::typeBinary) )
	{
		return false;
	}

	char* buf_utf8 = new char[file.GetLength()+1];
	::ZeroMemory(buf_utf8, file.GetLength()+1);
	file.Read(buf_utf8, file.GetLength());

	char* buf_ansi = new char[file.GetLength()+1];
	::ZeroMemory(buf_ansi, file.GetLength()+1);
	UTF8ToAnsi(buf_utf8, buf_ansi, file.GetLength());

	CString str_source = buf_ansi;
	delete[]buf_utf8;
	delete[]buf_ansi;

	// get <language>
	CString str_start_tag;
	str_start_tag.LoadString(IDS_CURRENT_LANGUAGE_START_TAG);
	CString str_end_tag;
	str_end_tag.LoadString(IDS_CURRENT_LANGUAGE_END_TAG);

	//if( str_start_tag.Find("japan") >= 0 )
	//{
	//	static CFont font;
	//	font.CreatePointFont(8*10, _T("MS Gothic"));
	//	m_lcSettings.SetFont(&font);
	//}

	CString str_language;
	if( GetStringBetweenTag(str_source, str_start_tag, str_end_tag, str_language) < 0 ) return false;

	// parsing <settings>
	CString str_settings;
	if( GetStringBetweenTag(str_language, SETTINGS_START_TAG, SETTINGS_END_TAG, str_settings) < 0 ) return false;

	// parsing <item>
	// parsing <values>
	// add to m_listSettings
	if( !GetSettingsList(str_settings, m_listSettings) ) return false;

	return true;
}

bool CUBCConfigWizardDlg::InitList()
{
	// init style & column
	m_lcSettings.SetExtendedStyle(m_lcSettings.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	CImageList gapImage;
	gapImage.Create(1,20,ILC_COLORDDB,1,0); //2번째 파라미터로 높이조절.....
	m_lcSettings.SetImageList(&gapImage,LVSIL_SMALL);

	int idx = 0;
	m_lcSettings.InsertColumn(idx++,  ::LoadString(IDS_COLUMN_SETTINGS)/*"설정"*/, 0, 280);
	m_lcSettings.InsertColumn(idx++, ::LoadString(IDS_COLUMN_VALUE)/*"값"*/, 0, 150);

	// insert to listctrl
	FOR_GC(m_listSettings, i)
	{
		SETTING_ITEM* set_item = m_listSettings.GetAt(i);

		//
		CString filepath = GetConfigDataPath();
		filepath += set_item->strFilename;

		char buf[1025] = {0};
		::GetPrivateProfileString(set_item->strSection, set_item->strKey, "", buf, 1024, filepath);
		set_item->strInitValue = buf;

		//
		m_lcSettings.InsertItem(i, set_item->strTitle);
		m_lcSettings.SetItemData(i, (DWORD)set_item);

		if( set_item->strValueType == "combobox" )
		{
			// combobox
			bool find_in_list = false;
			FOR_GC(set_item->listValues, j)
			{
				VALUE_ITEM* val_item = set_item->listValues.GetAt(j);

				if( val_item->strValue == set_item->strInitValue )
				{
					m_lcSettings.SetItemText(i, 1, val_item->strName);
					set_item->strInitValue = val_item->strName;
					find_in_list = true;
					break;
				}
			}

			//
			if( !find_in_list )
			{
				FOR_GC(set_item->listValues, j)
				{
					VALUE_ITEM* val_item = set_item->listValues.GetAt(j);

					if( val_item->bIsDefault )
					{
						m_lcSettings.SetItemText(i, 1, val_item->strName);
						set_item->strInitValue = "";
						set_item->strNewValue = val_item->strValue;
						set_item->bModified = true;
						break;
					}
				}
			}
		}
		else
		{
			// integer, string, etc...
			m_lcSettings.SetItemText(i, 1, set_item->strInitValue);
			set_item->strNewValue = set_item->strInitValue;
		}
	}

	return true;
}

bool CUBCConfigWizardDlg::SaveSettings()
{
	// save
	FOR_GC(m_listSettings, i)
	{
		SETTING_ITEM* set_item = m_listSettings.GetAt(i);

		if( set_item && set_item->bModified )
		{
			// save
			CString filepath = GetConfigDataPath();
			filepath += set_item->strFilename;

			::WritePrivateProfileString(set_item->strSection, set_item->strKey, set_item->strNewValue, filepath);

			// reset modified-flag
			set_item->bModified = false;
		}
	}

	return true;
}

void CUBCConfigWizardDlg::ClearList()
{
	FOR_GC(m_listSettings, i)
	{
		SETTING_ITEM* set_item = m_listSettings.GetAt(i);
		if(set_item) delete set_item;
	}
	m_listSettings.RemoveAll();
}

BOOL CUBCConfigWizardDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_SYSKEYDOWN && pMsg->wParam == VK_F4)
	{
		OnBnClickedButtonClose();
		return TRUE;
	}

	if(pMsg->message == WM_COMMAND && pMsg->wParam == WM_DESTROY)
	{
		OnBnClickedButtonClose();
		return TRUE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CUBCConfigWizardDlg::OnBnClickedButtonSaveSettings()
{
	SaveSettings();

	m_lcSettings.Invalidate();
}

void CUBCConfigWizardDlg::OnBnClickedButtonClose()
{
	if( m_lcSettings.IsModified() )
	{
		int ret = ::AfxMessageBox(IDS_MSG_CHANGED_VALUE_EXIST_SAVE_BEFORE_EXIT, MB_ICONWARNING | MB_YESNOCANCEL);
		if( ret == IDCANCEL ) return;
		if( ret == IDYES ) { SaveSettings(); };
	}

	ClearList();
	CDialog::OnCancel();
}

int CUBCConfigWizardDlg::GetStringBetweenTag(CString& strSource, LPCSTR lpszStart, LPCSTR lpszEnd, CString& strSub, int nStartIdx)
{
	int start_idx = strSource.Find(lpszStart, nStartIdx);
	if( start_idx<0 ) return -1;
	start_idx += strlen(lpszStart);

	int end_idx = strSource.Find(lpszEnd, start_idx);
	if( end_idx<0 ) return -1;

	strSub = strSource.Mid(start_idx, end_idx-start_idx);
	return end_idx + strlen(lpszEnd);
}

bool CUBCConfigWizardDlg::GetSettingsList(CString& strSource, SETTINGS_LIST& listSettings)
{
	int end_idx = 0;

	do
	{
		CString str_item;
		if( (end_idx=GetStringBetweenTag(strSource, ITEM_START_TAG, ITEM_END_TAG, str_item, end_idx)) < 0 ) break;

		CString str_location, str_title, str_values;
		if( GetStringBetweenTag(str_item, LOCATION_START_TAG, LOCATION_END_TAG, str_location) < 0 ) break;
		if( GetStringBetweenTag(str_item, TITLE_START_TAG, TITLE_END_TAG, str_title) < 0 ) break;
		if( GetStringBetweenTag(str_item, VALUES_START_TAG, VALUES_END_TAG, str_values) < 0 ) break;

		CString str_file, str_section, str_key;
		if( GetStringBetweenTag(str_location, FILE_START_TAG, FILE_END_TAG, str_file) < 0 ) break;
		if( GetStringBetweenTag(str_location, SECTION_START_TAG, SECTION_END_TAG, str_section) < 0 ) break;
		if( GetStringBetweenTag(str_location, KEY_START_TAG, KEY_END_TAG, str_key) < 0 ) break;

		CString str_value_type;
		if( GetStringBetweenTag(str_values, VALUE_TYPE_START_TAG, VALUE_TYPE_END_TAG, str_value_type) < 0 ) break;

		SETTING_ITEM* set_item = new SETTING_ITEM;
		set_item->strFilename  = str_file;
		set_item->strSection   = str_section;
		set_item->strKey       = str_key;
		set_item->strTitle     = str_title;
		set_item->strValueType = str_value_type;

		listSettings.Add(set_item);

		int item_end_idx = 0;
		do
		{
			int tmp_end_idx = 0;
			CString str_value_name, str_value_value, str_value_default;
			if( GetStringBetweenTag(str_values, VALUE_NAME_START_TAG, VALUE_NAME_END_TAG, str_value_name, item_end_idx) < 0 ) break;
			if( (tmp_end_idx=GetStringBetweenTag(str_values, VALUE_VALUE_START_TAG, VALUE_VALUE_END_TAG, str_value_value, item_end_idx)) < 0 ) break;
			GetStringBetweenTag(str_values, VALUE_DEFAULT_START_TAG, VALUE_DEFAULT_END_TAG, str_value_default, tmp_end_idx);

			VALUE_ITEM* val_item = new VALUE_ITEM;
			val_item->strName      = str_value_name;
			val_item->strValue     = str_value_value;
			val_item->bIsDefault   = atoi(str_value_default);

			set_item->listValues.Add(val_item);

			item_end_idx = tmp_end_idx;
		}
		while(1);
	}
	while(1);

	return true;
}

void CUBCConfigWizardDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )
	{
		m_repos.MoveControl(TRUE);
	}
}

LRESULT CUBCConfigWizardDlg::OnChangeFocus(WPARAM wParam, LPARAM lParam)
{
	m_btnSaveSettings.SetFocus();

	return 0;
}

void CUBCConfigWizardDlg::OnNMDblclkListSettings(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	int idx = -1;
	//for(int i=0; i<m_lcSettings.GetItemCount(); i++)
	FOR_GIC(m_lcSettings, i)
	{
		if( m_lcSettings.GetItemState(i, LVIS_SELECTED) )
		{
			idx = i;
			break;
		}
	}
	if( idx < 0 ) return;

	SETTING_ITEM* item = (SETTING_ITEM*)m_lcSettings.GetItemData(idx);

	CDetailInfoDlg dlg(item);
	dlg.DoModal();
}
