// UBCConfigGenView.h : CUBCConfigGenView 클래스의 인터페이스
//


#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#include "ReposControl2.h"


#define		VALUE_COMBOBOX_COUNT		(10)

class CUBCConfigGenView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CUBCConfigGenView();
	DECLARE_DYNCREATE(CUBCConfigGenView)

public:
	enum{ IDD = IDD_UBCCONFIGGEN_FORM };

// 특성입니다.
public:
	CUBCConfigGenDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CUBCConfigGenView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

public:
	CListCtrl	m_lcSettings;
	CComboBox	m_cbxFilename;
	CEdit		m_editSection;
	CEdit		m_editKey;
	CTabCtrl	m_tcLanguage;
	CEdit		m_editTitle;
	CComboBox	m_cbxValueType;
	CStatic		m_stcGroup;
	CStatic		m_stcValue;
	CEdit		m_editValueName[VALUE_COMBOBOX_COUNT];
	CEdit		m_editValueValue[VALUE_COMBOBOX_COUNT];

	afx_msg void OnLvnItemchangedListSettings(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTcnSelchangeTabLanguage(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeComboValueType();

	void	ListSelectChange(int idx=-1);
	void	TabSelectChange(int idx=-1);

protected:

	CReposControl2	m_repos;

	int		m_nLastSelectedListIndex;
	CString	m_nLastSelectedTabString;

	void	SetValueTypeComboBox(LPCTSTR lpszValueType);
	CString	GetCurValueTypeString();
	CString	GetCurLanguageString();

	void	ApplyChangedValue(int idx);
	int 	AddItem(SETTING_ITEM* set_item);
	bool 	UpdateItem(int idx);

	void	ClearValues();

	int		GetSelectedItem();

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnAddNewLanguage();
	afx_msg void OnAddNewItem();
	afx_msg void OnDeleteSelectedItem();
	afx_msg void OnMoveToUpItem();
	afx_msg void OnMoveToDownItem();
	afx_msg LRESULT OnApplyChangedValue(WPARAM wParam, LPARAM lParam);
};

#ifndef _DEBUG  // UBCConfigGenView.cpp의 디버그 버전
inline CUBCConfigGenDoc* CUBCConfigGenView::GetDocument() const
   { return reinterpret_cast<CUBCConfigGenDoc*>(m_pDocument); }
#endif

