// UBCConfigGenView.cpp : CUBCConfigGenView 클래스의 구현
//

#include "stdafx.h"
#include "UBCConfigGen.h"

#include "UBCConfigGenDoc.h"
#include "UBCConfigGenView.h"

#include "InputSingleDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUBCConfigGenView

IMPLEMENT_DYNCREATE(CUBCConfigGenView, CFormView)

BEGIN_MESSAGE_MAP(CUBCConfigGenView, CFormView)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_SETTINGS, &CUBCConfigGenView::OnLvnItemchangedListSettings)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_LANGUAGE, &CUBCConfigGenView::OnTcnSelchangeTabLanguage)
	ON_CBN_SELCHANGE(IDC_COMBO_VALUE_TYPE, &CUBCConfigGenView::OnCbnSelchangeComboValueType)
	ON_COMMAND(ID_ADD_NEW_LANGUAGE, &CUBCConfigGenView::OnAddNewLanguage)
	ON_COMMAND(ID_ADD_NEW_ITEM, &CUBCConfigGenView::OnAddNewItem)
	ON_COMMAND(ID_DELETE_SELECTED_ITEM, &CUBCConfigGenView::OnDeleteSelectedItem)
	ON_COMMAND(ID_MOVE_TO_UP_ITEM, &CUBCConfigGenView::OnMoveToUpItem)
	ON_COMMAND(ID_MOVE_TO_DOWN_ITEM, &CUBCConfigGenView::OnMoveToDownItem)
	ON_WM_SIZE()
	ON_MESSAGE(WM_APPLY_CHANGED_VALUE, OnApplyChangedValue)
END_MESSAGE_MAP()

// CUBCConfigGenView 생성/소멸

CUBCConfigGenView::CUBCConfigGenView()
	: CFormView(CUBCConfigGenView::IDD)
{
	m_nLastSelectedListIndex = -1;
}

CUBCConfigGenView::~CUBCConfigGenView()
{
}

void CUBCConfigGenView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SETTINGS, m_lcSettings);
	DDX_Control(pDX, IDC_COMBO_FILENAME, m_cbxFilename);
	DDX_Control(pDX, IDC_EDIT_SECTION, m_editSection);
	DDX_Control(pDX, IDC_EDIT_KEY, m_editKey);
	DDX_Control(pDX, IDC_TAB_LANGUAGE, m_tcLanguage);
	DDX_Control(pDX, IDC_EDIT_TITLE, m_editTitle);
	DDX_Control(pDX, IDC_COMBO_VALUE_TYPE, m_cbxValueType);
	DDX_Control(pDX, IDC_STATIC_GROUP, m_stcGroup);
	DDX_Control(pDX, IDC_STATIC_VALUE, m_stcValue);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_1, m_editValueName[0]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_1, m_editValueValue[0]);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_2, m_editValueName[1]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_2, m_editValueValue[1]);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_3, m_editValueName[2]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_3, m_editValueValue[2]);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_4, m_editValueName[3]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_4, m_editValueValue[3]);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_5, m_editValueName[4]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_5, m_editValueValue[4]);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_6, m_editValueName[5]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_6, m_editValueValue[5]);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_7, m_editValueName[6]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_7, m_editValueValue[6]);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_8, m_editValueName[7]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_8, m_editValueValue[7]);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_9, m_editValueName[8]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_9, m_editValueValue[8]);
	DDX_Control(pDX, IDC_EDIT_VALUE_NAME_10, m_editValueName[9]);
	DDX_Control(pDX, IDC_EDIT_VALUE_VALUE_10, m_editValueValue[9]);
}

BOOL CUBCConfigGenView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CUBCConfigGenView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	SetScrollSizes(MM_TEXT, CSize(1,1));

	m_repos.SetParent(this);
	m_repos.AddControl((CWnd*)&m_lcSettings, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_tcLanguage, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editTitle, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_cbxValueType, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	for(int i=0; i<VALUE_COMBOBOX_COUNT; i++)
	{
		m_repos.AddControl((CWnd*)&m_editValueName[i], REPOS_FIX, REPOS_FIX, REPOS_RESIZE_DIV2, REPOS_FIX, 2);
		m_repos.AddControl((CWnd*)&m_editValueValue[i], REPOS_RESIZE_DIV2, REPOS_FIX, REPOS_MOVE, REPOS_FIX, 2);
	}
	m_repos.AddControl((CWnd*)&m_stcGroup, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_stcValue, REPOS_RESIZE_DIV2, REPOS_FIX, REPOS_MOVE, REPOS_FIX, 2);

	//
	m_lcSettings.SetExtendedStyle(m_lcSettings.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcSettings.InsertColumn(0, _T("파일이름"), 0, 105);
	m_lcSettings.InsertColumn(1, _T("섹션"), 0, 125);
	m_lcSettings.InsertColumn(2, _T("키"), 0, 205);

	m_cbxValueType.AddString(_T("콤보박스"));
	m_cbxValueType.AddString(_T("숫자"));
	m_cbxValueType.AddString(_T("문자열"));

	CUBCConfigGenDoc* pDoc = GetDocument();

	//
	CStringArray list_language;
	pDoc->GetLanguageList(list_language);
	FOR_GC(list_language, i)
	{
		const CString& str_language = list_language.GetAt(i);
		m_tcLanguage.InsertItem(i, str_language);
	}
	m_tcLanguage.SetCurSel(0);
	m_nLastSelectedTabString = GetCurLanguageString();

	//
	CStringArray list_filename;
	pDoc->GetFilenameList(list_filename);
	FOR_GC(list_filename, j)
	{
		const CString& str_fn = list_filename.GetAt(j);
		m_cbxFilename.AddString(str_fn);
	}

	//
	int cnt = pDoc->GetSettingsListCount();
	for(int i=0; i<cnt; i++)
	{
		SETTING_ITEM* s_item = pDoc->GetSettingItem(i);
		AddItem(s_item);
	}
}


// CUBCConfigGenView 진단

#ifdef _DEBUG
void CUBCConfigGenView::AssertValid() const
{
	CFormView::AssertValid();
}

void CUBCConfigGenView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CUBCConfigGenDoc* CUBCConfigGenView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCConfigGenDoc)));
	return (CUBCConfigGenDoc*)m_pDocument;
}
#endif //_DEBUG


// CUBCConfigGenView 메시지 처리기

void CUBCConfigGenView::OnLvnItemchangedListSettings(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if( m_nLastSelectedListIndex>=0 )
		ApplyChangedValue(m_nLastSelectedListIndex);

	int idx = GetSelectedItem();
	if( idx<0 )
	{
		m_nLastSelectedListIndex=-1;
		ClearValues();
		return;
	}

	static bool is_in_updating = false;

	if( !is_in_updating )
	{
		is_in_updating = true;
		ListSelectChange(idx);
		is_in_updating = false;
	}
}

void CUBCConfigGenView::OnTcnSelchangeTabLanguage(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	if( m_nLastSelectedListIndex>=0 )
		ApplyChangedValue(m_nLastSelectedListIndex);

	m_nLastSelectedTabString = GetCurLanguageString();

	TabSelectChange();
}

void CUBCConfigGenView::OnCbnSelchangeComboValueType()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if( m_nLastSelectedListIndex>=0 )
		ApplyChangedValue(m_nLastSelectedListIndex);

	TabSelectChange();
}

void CUBCConfigGenView::ListSelectChange(int idx)
{
	if( idx<0 )
	{
		idx = GetSelectedItem();
		if( idx<0 ) return;
	}

	SETTING_ITEM* s_item = (SETTING_ITEM*)m_lcSettings.GetItemData(idx);
	if( s_item==NULL ) return;

	TabSelectChange(idx);

	m_cbxFilename.SetWindowText(s_item->strFilename);
	m_editSection.SetWindowText(s_item->strSection);
	m_editKey.SetWindowText(s_item->strKey);

	m_nLastSelectedListIndex = idx;
	UpdateItem(idx);
}

void CUBCConfigGenView::TabSelectChange(int idx)
{
	if( idx<0 )
	{
		idx = GetSelectedItem();
		if( idx<0 ) return;
	}

	SETTING_ITEM* s_item = (SETTING_ITEM*)m_lcSettings.GetItemData(idx);
	if( s_item==NULL ) return;

	CString str_language = GetCurLanguageString();

	CString str_title;
	s_item->mapTitle.Lookup(str_language, str_title);
	m_editTitle.SetWindowText(str_title);

	SetValueTypeComboBox(s_item->strValueType);

	int cnt = s_item->listValues.GetCount();
	int i=0;
	for(; i<cnt; i++)
	{
		VALUE_ITEM* v_item = s_item->listValues.GetAt(i);

		CString str_val_name;
		v_item->mapName.Lookup(str_language, str_val_name);

		m_editValueName[i].SetWindowText(str_val_name);
		m_editValueValue[i].SetWindowText(v_item->strValue);
	}

	for( ; i<VALUE_COMBOBOX_COUNT; i++)
	{
		m_editValueName[i].SetWindowText(_T(""));
		m_editValueValue[i].SetWindowText(_T(""));
	}
}

void CUBCConfigGenView::SetValueTypeComboBox(LPCTSTR lpszValueType)
{
	BOOL enable_combo = FALSE;
	if( _tcsicmp(lpszValueType, _T("combobox")) == 0 )		{ m_cbxValueType.SetCurSel(0); enable_combo=TRUE; }
	else if( _tcsicmp(lpszValueType, _T("integer")) == 0 )	m_cbxValueType.SetCurSel(1);
	else if( _tcsicmp(lpszValueType, _T("string")) == 0 )	m_cbxValueType.SetCurSel(2);
	else m_cbxValueType.SetCurSel(-1);

	for(int i=0; i<VALUE_COMBOBOX_COUNT; i++)
	{
		m_editValueName[i].EnableWindow(enable_combo);
		m_editValueValue[i].EnableWindow(enable_combo);
	}
}

CString CUBCConfigGenView::GetCurValueTypeString()
{
	int idx = m_cbxValueType.GetCurSel();

	switch(idx)
	{
	case 0: return _T("combobox");
	case 1: return _T("integer");
	case 2: return _T("string");
	}

	return _T("");
}

CString	CUBCConfigGenView::GetCurLanguageString()
{
	int tab_idx = m_tcLanguage.GetCurSel();
	if( tab_idx<0 ) return _T("");

	TCHAR buf[1024] = {0};
	TCITEM ti = {0};
	ti.mask = TCIF_TEXT;
	ti.pszText = buf;
	ti.cchTextMax = 1023;
	m_tcLanguage.GetItem(tab_idx, &ti);

	CString str_language;
	str_language.Format(_T("<%s>"), buf);

	return str_language;
}

void CUBCConfigGenView::ApplyChangedValue(int idx)
{
	int last_idx = m_nLastSelectedListIndex;
	m_nLastSelectedListIndex = idx;

	if( last_idx<0 ) return;

	SETTING_ITEM* s_item = (SETTING_ITEM*)m_lcSettings.GetItemData(last_idx);
	if( s_item==NULL ) return;

	SETTING_ITEM bak_item;
	bak_item.Copy(*s_item);

	m_cbxFilename.GetWindowText(s_item->strFilename);
	m_editSection.GetWindowText(s_item->strSection);
	m_editKey.GetWindowText(s_item->strKey);

	CString str_title;
	m_editTitle.GetWindowText(str_title);
	s_item->mapTitle.SetAt(m_nLastSelectedTabString, str_title);
	s_item->strValueType = GetCurValueTypeString();

	for(int i=0; i<VALUE_COMBOBOX_COUNT; i++)
	{
		CString str_value_name;
		m_editValueName[i].GetWindowText(str_value_name);

		CString str_value_value;
		m_editValueValue[i].GetWindowText(str_value_value);

		if( str_value_name.GetLength()==0 && str_value_value.GetLength()==0 ) break;

		VALUE_ITEM* v_item = NULL;
		if( s_item->listValues.GetCount() <= i )
		{
			v_item = new VALUE_ITEM;
			s_item->listValues.Add(v_item);
		}
		else
		{
			v_item = s_item->listValues.GetAt(i);
		}

		v_item->mapName.SetAt(m_nLastSelectedTabString, str_value_name);
		v_item->strValue = str_value_value;
	}

	if( !bak_item.IsEqual(*s_item) )
	{
		GetDocument()->SetModifiedFlag();
	}
}

int CUBCConfigGenView::AddItem(SETTING_ITEM* sItem)
{
	LVFINDINFO lfi = {0};
	lfi.flags = LVFI_PARAM;
	lfi.lParam = (LPARAM)sItem;

	int idx = m_lcSettings.FindItem(&lfi);

	if( idx<0 )
	{
		idx = m_lcSettings.GetItemCount();
		m_lcSettings.InsertItem(idx, _T(""));
		m_lcSettings.SetItemData(idx, (DWORD)sItem);
	}

	UpdateItem(idx);

	return idx;
}

bool CUBCConfigGenView::UpdateItem(int idx)
{
	SETTING_ITEM* s_item = (SETTING_ITEM*)m_lcSettings.GetItemData(idx);

	m_lcSettings.SetItemText(idx, 0, s_item->strFilename);
	m_lcSettings.SetItemText(idx, 1, s_item->strSection);
	m_lcSettings.SetItemText(idx, 2, s_item->strKey);

	return true;
}

void CUBCConfigGenView::ClearValues()
{
	m_nLastSelectedListIndex = -1;

	m_cbxFilename.SetWindowText(_T(""));
	m_editSection.SetWindowText(_T(""));
	m_editKey.SetWindowText(_T(""));

	m_editTitle.SetWindowText(_T(""));
	for(int i=0; i<VALUE_COMBOBOX_COUNT; i++)
	{
		m_editValueName[i].SetWindowText(_T(""));
		m_editValueValue[i].SetWindowText(_T(""));
	}
}

int CUBCConfigGenView::GetSelectedItem()
{
	FOR_GIC(m_lcSettings, i)
	{
		if( m_lcSettings.GetItemState(i, LVIS_SELECTED) )
			return i;
	}
	return -1;
}

void CUBCConfigGenView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if( GetSafeHwnd() && m_lcSettings.GetSafeHwnd() )
	{
		m_repos.MoveControl();
	}
}

void CUBCConfigGenView::OnAddNewLanguage()
{
	CInputSingleDlg dlg;
	dlg.SetDialogInfo(_T("언어 문자열 입력"), _T("언어 문자열"));

	if( dlg.DoModal()==IDOK )
	{
		CString str_language;
		dlg.GetValue(str_language);

		if( GetDocument()->AddLanguageString(str_language) )
		{
			int i = m_tcLanguage.GetItemCount();
			m_tcLanguage.InsertItem(i, str_language);
		}
	}
}

void CUBCConfigGenView::OnAddNewItem()
{
	SETTING_ITEM* s_item = GetDocument()->AddNewSetting();

	int idx = AddItem(s_item);

	FOR_GIC(m_lcSettings, i)
	{
		if( idx==i )
			m_lcSettings.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
		else
			m_lcSettings.SetItemState(i, NULL, LVIS_SELECTED);
	}

	ListSelectChange(idx);
}

void CUBCConfigGenView::OnDeleteSelectedItem()
{
	FOR_GIC_REV(m_lcSettings, i)
	{
		if( m_lcSettings.GetItemState(i, LVIS_SELECTED) )
		{
			SETTING_ITEM* s_item = (SETTING_ITEM*)m_lcSettings.GetItemData(i);

			CString msg;
			msg.Format(_T("%s 파일에서\r\n\r\n[%s]\r\n%s\r\n\r\n항목을 삭제하시겠습니까 ?"), s_item->strFilename, s_item->strSection, s_item->strKey);

			int ret_val = ::AfxMessageBox(msg, MB_ICONWARNING | MB_YESNO);
			if( ret_val == IDNO ) return;

			if( GetDocument()->DeleteSetting(s_item) )
			{
				m_nLastSelectedListIndex = -1;
				m_lcSettings.DeleteItem(i);
			}
		}
	}

	ClearValues();
}

void CUBCConfigGenView::OnMoveToUpItem()
{
	int idx = GetSelectedItem();
	if( idx<0 ) return;

	SETTING_ITEM* s_item = (SETTING_ITEM*)m_lcSettings.GetItemData(idx);
	if( idx>0 && GetDocument()->MoveUpSetting(s_item) )
	{
		SETTING_ITEM* replace_item = (SETTING_ITEM*)m_lcSettings.GetItemData(idx-1);

		ApplyChangedValue(idx);
		ClearValues();

		m_lcSettings.SetItemData(idx, (DWORD)replace_item);
		m_lcSettings.SetItemData(idx-1, (DWORD)s_item);

		m_lcSettings.SetItemState(idx, NULL, LVIS_SELECTED | LVIS_FOCUSED);
		m_lcSettings.SetItemState(idx-1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);

		UpdateItem(idx);
		UpdateItem(idx-1);
	}
}

void CUBCConfigGenView::OnMoveToDownItem()
{
	int idx = GetSelectedItem();
	if( idx<0 ) return;

	SETTING_ITEM* s_item = (SETTING_ITEM*)m_lcSettings.GetItemData(idx);
	if( idx<m_lcSettings.GetItemCount()-1 && GetDocument()->MoveDownSetting(s_item) )
	{
		SETTING_ITEM* replace_item = (SETTING_ITEM*)m_lcSettings.GetItemData(idx+1);

		ApplyChangedValue(idx);
		ClearValues();

		m_lcSettings.SetItemData(idx, (DWORD)replace_item);
		m_lcSettings.SetItemData(idx+1, (DWORD)s_item);

		m_lcSettings.SetItemState(idx, NULL, LVIS_SELECTED | LVIS_FOCUSED);
		m_lcSettings.SetItemState(idx+1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);

		UpdateItem(idx);
		UpdateItem(idx+1);
	}
}

LRESULT CUBCConfigGenView::OnApplyChangedValue(WPARAM wParam, LPARAM lParam)
{
	if( m_nLastSelectedListIndex>=0 )
		ApplyChangedValue(m_nLastSelectedListIndex);

	return 0;
}
