// InputSingleDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCConfigGen.h"
#include "InputSingleDlg.h"


// CInputSingleDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CInputSingleDlg, CDialog)

CInputSingleDlg::CInputSingleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputSingleDlg::IDD, pParent)
	, m_reposControl ( this )
{
	m_bNumberValue = false;
	m_nDialogHeight = -1;
}

CInputSingleDlg::~CInputSingleDlg()
{
}

void CInputSingleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_GROUP, m_stcGroup);
	DDX_Control(pDX, IDC_STATIC_NAME, m_stcName);
	DDX_Control(pDX, IDC_EDIT_VALUE, m_editValue);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CInputSingleDlg, CDialog)
	ON_WM_GETMINMAXINFO()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CInputSingleDlg 메시지 처리기입니다.

BOOL CInputSingleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	if(m_bNumberValue)
		m_editValue.ModifyStyle(NULL, ES_NUMBER);

	//
	CDC* dc = GetDC();
	CFont font;
	font.CreatePointFont(8*10, _T("Tahoma"));

	CFont* old_font = dc->SelectObject(&font);
	CSize name_size = dc->GetOutputTextExtent(m_strName);

	CRect name_rect;
	m_stcName.GetWindowRect(name_rect);

	if(name_rect.Width() < name_size.cx)
	{
		int diff = name_size.cx - name_rect.Width();
		name_rect.right += diff;
		ScreenToClient(name_rect);
		m_stcName.MoveWindow(name_rect);

		CRect edit_rect;
		m_editValue.GetWindowRect(edit_rect);
		edit_rect.left += diff;
		ScreenToClient(edit_rect);
		m_editValue.MoveWindow(edit_rect);
	}
	dc->SelectObject(old_font);
	ReleaseDC(dc);

	//
	m_reposControl.AddControl((CWnd*)&m_stcGroup, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl((CWnd*)&m_editValue, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl((CWnd*)&m_btnOK, REPOS_RESIZE_DIV2, REPOS_FIX, REPOS_RESIZE_DIV2, REPOS_FIX, 2);
	m_reposControl.AddControl((CWnd*)&m_btnCancel, REPOS_RESIZE_DIV2, REPOS_FIX, REPOS_RESIZE_DIV2, REPOS_FIX, 2);

	CRect client_rect;
	GetWindowRect(client_rect);
	m_nDialogHeight = client_rect.Height();

	//
	SetWindowText(m_strTitle);
	m_stcName.SetWindowText(m_strName);
	m_editValue.SetWindowText(m_strValue);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CInputSingleDlg::OnOK()
{
	m_editValue.GetWindowText(m_strValue);

	CDialog::OnOK();
}

void CInputSingleDlg::SetDialogInfo(LPCTSTR lpszTitle, LPCTSTR lpszName, LPCTSTR lpszValue)
{
	if(lpszTitle)	m_strTitle=lpszTitle;
	if(lpszName)	m_strName =lpszName;
	if(lpszValue)	m_strValue=lpszValue;
}

void CInputSingleDlg::SetDialogInfo(UINT nTitleID, UINT nNameID, LPCSTR lpszValue)
{
	m_strTitle.LoadString(nTitleID);
	m_strName.LoadString(nNameID);
	if(lpszValue)	m_strValue=lpszValue;
}

void CInputSingleDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	if(m_nDialogHeight > 0)
	{
		lpMMI->ptMinTrackSize.y = m_nDialogHeight;
		lpMMI->ptMaxTrackSize.y = m_nDialogHeight;
	}
	lpMMI->ptMinTrackSize.x = 200;

	CDialog::OnGetMinMaxInfo(lpMMI);
}

void CInputSingleDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	m_reposControl.MoveControl(TRUE);
}
