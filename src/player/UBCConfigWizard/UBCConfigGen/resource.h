//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCConfigGen.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_UBCCONFIGGEN_FORM           101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_UBCConfigGenTYPE            129
#define IDC_LIST_SETTINGS               1000
#define IDC_TAB_LANGUAGE                1001
#define IDC_EDIT_SECTION                1002
#define IDC_EDIT_KEY                    1003
#define IDC_COMBO_FILENAME              1004
#define IDC_EDIT_TITLE                  1005
#define IDC_COMBO_VALUE_TYPE            1006
#define IDC_EDIT_VALUE_NAME_1           1007
#define IDC_EDIT_VALUE_VALUE_1          1008
#define IDC_EDIT_VALUE_NAME_2           1009
#define IDC_EDIT_VALUE_VALUE_2          1010
#define IDC_EDIT_VALUE_NAME_3           1011
#define IDC_EDIT_VALUE_VALUE_3          1012
#define IDC_EDIT_VALUE_NAME_4           1013
#define IDC_EDIT_VALUE_VALUE_4          1014
#define IDC_EDIT_VALUE_NAME_5           1015
#define IDC_EDIT_VALUE_VALUE_5          1016
#define IDC_EDIT_VALUE_NAME_6           1017
#define IDC_EDIT_VALUE_VALUE_6          1018
#define IDC_EDIT_VALUE_NAME_7           1019
#define IDC_EDIT_VALUE_VALUE_7          1020
#define IDC_EDIT_VALUE_NAME_8           1021
#define IDC_EDIT_VALUE_VALUE_8          1022
#define IDC_EDIT_VALUE_NAME_9           1023
#define IDC_EDIT_VALUE_VALUE_9          1024
#define IDC_EDIT_VALUE_NAME_10          1025
#define IDC_EDIT_VALUE_VALUE_10         1026
#define IDD_INPUT_SINGLE                1027
#define IDC_STATIC_GROUP                1028
#define IDC_STATIC_NAME                 1029
#define IDC_EDIT_VALUE                  1030
#define IDC_STATIC_VALUE                1031
#define ID_DELETE_SELECTED              32778
#define ID_ADD_NEW_LANGUAGE             32781
#define ID_ADD_NEW_ITEM                 32782
#define ID_DELETE_SELECTED_ITEM         32783
#define ID_MOVE_TO_UP_ITEM              32784
#define ID_MOVE_TO_DOWN_ITEM            32785

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32787
#define _APS_NEXT_CONTROL_VALUE         1032
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
