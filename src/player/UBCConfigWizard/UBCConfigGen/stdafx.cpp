// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// UBCConfigGen.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


int UTF8toUNICODE(LPCSTR lpszUTF8, CStringW& strUnicode)
{
	int nOutLen = ::MultiByteToWideChar(CP_UTF8, 0, lpszUTF8, -1, NULL, 0);

	if(nOutLen > 0)
	{
		LPWSTR lpwzOut;
		if( (lpwzOut = new WCHAR[nOutLen+1]) != NULL)
		{
			::MultiByteToWideChar(CP_UTF8, 0, lpszUTF8, -1, lpwzOut, nOutLen+1);
			strUnicode = lpwzOut;
			delete[] lpwzOut;
		}
	}

	return nOutLen;
}

int UNICODEtoUTF8(LPCWSTR lpwzUnicode, CStringA& strUTF8)
{
	int nOutLen = ::WideCharToMultiByte(CP_UTF8, 0, lpwzUnicode, -1, NULL, 0, NULL, NULL);

	if(nOutLen>0)
	{
		LPSTR lpszOut;

		if( (lpszOut = new CHAR[nOutLen + 1]) != NULL )
		{
			nOutLen = ::WideCharToMultiByte(CP_UTF8, 0, lpwzUnicode, -1, lpszOut, nOutLen + 1, NULL, NULL);
			strUTF8 = lpszOut;
			delete[] lpszOut;
		}
	}

	return nOutLen;
}
