// UBCConfigGenDoc.cpp : CUBCConfigGenDoc 클래스의 구현
//

#include "stdafx.h"
#include "UBCConfigGen.h"

#include "UBCConfigGenDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		SETTINGS_START_TAG		_T("<settings>")
#define		SETTINGS_END_TAG		_T("</settings>")

#define		ITEM_START_TAG			_T("<item>")
#define		ITEM_END_TAG			_T("</item>")

#define		VALUES_START_TAG		_T("<values>")
#define		VALUES_END_TAG			_T("</values>")

#define		LOCATION_START_TAG		_T("<location")
#define		LOCATION_END_TAG		_T("/>")
	
#define		FILE_START_TAG			_T("file=\"")
#define		FILE_END_TAG			_T("\"")

#define		SECTION_START_TAG		_T("section=\"")
#define		SECTION_END_TAG			_T("\"")

#define		KEY_START_TAG			_T("key=\"")
#define		KEY_END_TAG				_T("\"")

#define		TITLE_START_TAG			_T("<title>")
#define		TITLE_END_TAG			_T("</title>")

#define		VALUE_TYPE_START_TAG	_T("<type>")
#define		VALUE_TYPE_END_TAG		_T("</type>")

#define		VALUE_ITEM_START_TAG	_T("<item")
#define		VALUE_ITEM_END_TAG		_T("/>")

#define		VALUE_NAME_START_TAG	_T("name=\"")
#define		VALUE_NAME_END_TAG		_T("\"")

#define		VALUE_VALUE_START_TAG	_T("value=\"")
#define		VALUE_VALUE_END_TAG		_T("\"")

#define		VALUE_DEFAULT_START_TAG	_T("default=\"")
#define		VALUE_DEFAULT_END_TAG	_T("\"")


// CUBCConfigGenDoc

IMPLEMENT_DYNCREATE(CUBCConfigGenDoc, CDocument)

BEGIN_MESSAGE_MAP(CUBCConfigGenDoc, CDocument)
END_MESSAGE_MAP()


// CUBCConfigGenDoc 생성/소멸

CUBCConfigGenDoc::CUBCConfigGenDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CUBCConfigGenDoc::~CUBCConfigGenDoc()
{
	FOR_GC(m_listSettings, i)
	{
		SETTING_ITEM* s_item = m_listSettings.GetAt(i);
		if(s_item) delete s_item;
	}
	m_listSettings.RemoveAll();
}

BOOL CUBCConfigGenDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	TCHAR buf[1024] = {0};
	::GetModuleFileName(NULL, buf, 1023);

	int idx = _tcslen(buf)-1;
	while( buf[idx]!=_T('\\') )
	{
		buf[idx] = 0;
		idx--;
	}

	_tcscat(buf, _T("TerminalSettings.xml"));

	OnOpenDocument(buf);
	SetPathName(buf);

	return TRUE;
}




// CUBCConfigGenDoc serialization

void CUBCConfigGenDoc::Serialize(CArchive& ar)
{
	if( ar.IsStoring() )
	{
		CString str_uni;
		GenerateXML(str_uni);

		CStringA str_utf8;
		::UNICODEtoUTF8(str_uni, str_utf8);
		unsigned char header[3] = { 0xEF, 0xBB, 0xBF };

		CFile* file = ar.GetFile();
		file->Write(header, 3);
		file->Write((LPCSTR)str_utf8, str_utf8.GetLength());
	}
	else
	{
		CFile* file = ar.GetFile();

		int buf_size = file->GetLength()+1;
		char* buf = new char[buf_size];
		::ZeroMemory(buf, buf_size);

		ar.Read(buf, buf_size);

		CString str_uni;
		::UTF8toUNICODE(buf, str_uni);

		LoadXML(str_uni);

		delete[]buf;
	}
}


// CUBCConfigGenDoc 진단

#ifdef _DEBUG
void CUBCConfigGenDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CUBCConfigGenDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CUBCConfigGenDoc 명령

bool CUBCConfigGenDoc::LoadXML(CString& str)
{
	CString str_start_language_tag;
	CString str_end_language_tag= _T("<terminal>");

	int start_idx = 0;
	int end_idx = 0;

	do
	{
		end_idx = str.Find(str_end_language_tag);
		if( end_idx<0 ) return false;
		end_idx += str_end_language_tag.GetLength();

		if( (start_idx=GetNextTag(str, str_start_language_tag, end_idx))<0 ) return false;
		if( str_start_language_tag==_T("</terminal>") ) return true;

		AddLanguageString(str_start_language_tag);

		str_end_language_tag = str_start_language_tag;
		str_end_language_tag.Insert(1, _T("/"));

		CString str_language;
		if( GetStringBetweenTag(str, str_start_language_tag, str_end_language_tag, str_language)<0 ) return false;

		CString str_settings_list;
		if( GetStringBetweenTag(str_language, SETTINGS_START_TAG, SETTINGS_END_TAG, str_settings_list)<0 ) return false;

		if( !GetSettingsList(str_settings_list, str_start_language_tag, m_listSettings) ) return false;
	} while(1);
}

int CUBCConfigGenDoc::GetNextTag(CString& strSource, CString& strNextTag, int nStartIdx)
{
	int start_idx = strSource.Find(_T("<"), nStartIdx);
	if( start_idx<0 ) return -1;

	int end_idx = strSource.Find(_T(">"), start_idx);
	if( end_idx<0 ) return -1;
	end_idx++;

	strNextTag = strSource.Mid(start_idx, end_idx-start_idx);
	return end_idx;
}

int CUBCConfigGenDoc::GetStringBetweenTag(CString& strSource, LPCTSTR lpszStart, LPCTSTR lpszEnd, CString& strSub, int nStartIdx)
{
	int start_idx = strSource.Find(lpszStart, nStartIdx);
	if( start_idx<0 ) return -1;
	start_idx += _tcslen(lpszStart);

	int end_idx = strSource.Find(lpszEnd, start_idx);
	if( end_idx<0 ) return -1;

	strSub = strSource.Mid(start_idx, end_idx-start_idx);
	return end_idx + _tcslen(lpszEnd);
}

bool CUBCConfigGenDoc::GetSettingsList(CString& strSource, LPCTSTR lpszLanguage, SETTINGS_LIST& listSettings)
{
	int end_idx = 0;

	do
	{
		CString str_item;
		if( (end_idx=GetStringBetweenTag(strSource, ITEM_START_TAG, ITEM_END_TAG, str_item, end_idx)) < 0 ) break;

		CString str_location, str_title, str_values;
		if( GetStringBetweenTag(str_item, LOCATION_START_TAG, LOCATION_END_TAG, str_location) < 0 ) break;
		if( GetStringBetweenTag(str_item, TITLE_START_TAG, TITLE_END_TAG, str_title) < 0 ) break;
		if( GetStringBetweenTag(str_item, VALUES_START_TAG, VALUES_END_TAG, str_values) < 0 ) break;

		CString str_file, str_section, str_key;
		if( GetStringBetweenTag(str_location, FILE_START_TAG, FILE_END_TAG, str_file) < 0 ) break;
		if( GetStringBetweenTag(str_location, SECTION_START_TAG, SECTION_END_TAG, str_section) < 0 ) break;
		if( GetStringBetweenTag(str_location, KEY_START_TAG, KEY_END_TAG, str_key) < 0 ) break;

		CString str_value_type;
		if( GetStringBetweenTag(str_values, VALUE_TYPE_START_TAG, VALUE_TYPE_END_TAG, str_value_type) < 0 ) break;

		str_file.Trim(_T(" \t"));
		str_section.Trim(_T(" \t"));
		str_key.Trim(_T(" \t"));
		str_value_type.Trim(_T(" \t"));

		SETTING_ITEM* s_item = new SETTING_ITEM;
		s_item->strFilename  = str_file;	AddFilename(str_file);
		s_item->strSection   = str_section;
		s_item->strKey       = str_key;
		s_item->strValueType = str_value_type;

		s_item = AddSetting(s_item);
		s_item->mapTitle.SetAt(lpszLanguage, str_title);

		int item_end_idx = 0;
		do
		{
			int tmp_end_idx = 0;
			CString str_value_name, str_value_value, str_value_default;
			if( GetStringBetweenTag(str_values, VALUE_NAME_START_TAG, VALUE_NAME_END_TAG, str_value_name, item_end_idx) < 0 ) break;
			if( (tmp_end_idx=GetStringBetweenTag(str_values, VALUE_VALUE_START_TAG, VALUE_VALUE_END_TAG, str_value_value, item_end_idx)) < 0 ) break;
			GetStringBetweenTag(str_values, VALUE_DEFAULT_START_TAG, VALUE_DEFAULT_END_TAG, str_value_default, tmp_end_idx);

			str_value_name.Trim(_T(" \t"));
			str_value_value.Trim(_T(" \t"));

			VALUE_ITEM* v_item = new VALUE_ITEM;
			v_item->strValue = str_value_value;
			v_item = AddValue(s_item, v_item);
			v_item->mapName.SetAt(lpszLanguage, str_value_name);

			//set_item->listValues.Add(val_item);

			item_end_idx = tmp_end_idx;
		}
		while(1);
	}
	while(1);

	return true;
}

bool CUBCConfigGenDoc::AddLanguageString(CString strLanguage)
{
	strLanguage.Replace(_T("<"), _T(""));
	strLanguage.Replace(_T(">"), _T(""));

	FOR_GC(m_listLanguage, i)
	{
		const CString& str_lan = m_listLanguage.GetAt(i);
		if( str_lan == strLanguage ) return false;
	}

	m_listLanguage.Add(strLanguage);
	return true;
}

bool CUBCConfigGenDoc::AddFilename(CString strFilename)
{
	CString key = strFilename;
	key.MakeLower();

	CString value;
	if( m_mapFilename.Lookup(key, value) ) return false;

	m_mapFilename.SetAt(key, strFilename);
	return true;
}

void CUBCConfigGenDoc::GetFilenameList(CStringArray& listFilename)
{
	POSITION pos = m_mapFilename.GetStartPosition();
	while( pos != NULL )
	{
		CString str_key, str_value;
		m_mapFilename.GetNextAssoc( pos, str_key, str_value );

		listFilename.Add(str_value);
	}
}

SETTING_ITEM* CUBCConfigGenDoc::AddSetting(SETTING_ITEM* sItem)
{
	FOR_GC(m_listSettings, i)
	{
		SETTING_ITEM* s_item = m_listSettings.GetAt(i);

		if( _tcsicmp(s_item->strFilename, sItem->strFilename) == 0 &&
			_tcsicmp(s_item->strSection, sItem->strSection) == 0 &&
			_tcsicmp(s_item->strKey, sItem->strKey) == 0 )
		{
			delete sItem;
			return s_item;
		}
	}

	m_listSettings.Add(sItem);
	return sItem;
}

VALUE_ITEM* CUBCConfigGenDoc::AddValue(SETTING_ITEM* sItem, VALUE_ITEM* vItem)
{
	FOR_GC(sItem->listValues, i)
	{
		VALUE_ITEM* v_item = sItem->listValues.GetAt(i);
		if( _tcsicmp(vItem->strValue, v_item->strValue) == 0 )
		{
			delete vItem;
			return v_item;
		}
	}

	sItem->listValues.Add(vItem);
	return vItem;
}

SETTING_ITEM* CUBCConfigGenDoc::AddNewSetting()
{
	static int idx = 0;
	SETTING_ITEM* s_item = new SETTING_ITEM;

	s_item->strFilename;
	s_item->strSection.Format(_T("Section%d"), idx);
	s_item->strKey.Format(_T("Key%d"), idx);
	s_item->strValueType=_T("combobox");

	idx++;

	return AddSetting(s_item);
}

bool CUBCConfigGenDoc::DeleteSetting(SETTING_ITEM* sItem)
{
	FOR_GC(m_listSettings, i)
	{
		SETTING_ITEM* s_item = m_listSettings.GetAt(i);

		if( s_item == sItem )
		{
			m_listSettings.RemoveAt(i);
			delete s_item;
			return true;
		}
	}
	return false;
}

bool CUBCConfigGenDoc::MoveUpSetting(SETTING_ITEM* sItem)
{
	int set_cnt = m_listSettings.GetCount();
	for(int i=1; i<set_cnt; i++)
	{
		SETTING_ITEM* s_item = m_listSettings.GetAt(i);

		if( sItem == s_item )
		{
			SETTING_ITEM* replace_item = m_listSettings.GetAt(i-1);

			m_listSettings.SetAt(i-1, s_item);
			m_listSettings.SetAt(i, replace_item);

			return true;
		}
	}

	return false;
}

bool CUBCConfigGenDoc::MoveDownSetting(SETTING_ITEM* sItem)
{
	int set_cnt = m_listSettings.GetCount();
	for(int i=0; i<set_cnt-1; i++)
	{
		SETTING_ITEM* s_item = m_listSettings.GetAt(i);

		if( sItem == s_item )
		{
			SETTING_ITEM* replace_item = m_listSettings.GetAt(i+1);

			m_listSettings.SetAt(i+1, s_item);
			m_listSettings.SetAt(i, replace_item);

			return true;
		}
	}

	return false;
}

void CUBCConfigGenDoc::GenerateXML(CString& str)
{
	str = _T("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<terminal>\r\n");

	FOR_GC(m_listLanguage, i)
	{
		CString str_language = m_listLanguage.GetAt(i);

		str_language.Insert(0, _T("<"));
		str_language += _T(">");

		//<korean>
		str += _T("\t");
		str += str_language;
		str += _T("\r\n");

		//<settings>
		str += _T("\t\t");
		str += SETTINGS_START_TAG;
		str += _T("\r\n");

		FOR_GC(m_listSettings, j)
		{
			SETTING_ITEM* s_item = m_listSettings.GetAt(j);

			//<item>
			str += _T("\t\t\t");
			str += ITEM_START_TAG;
			str += _T("\r\n");

			CString str_buf;
			//<location file="UBCBrowser.ini" section="ROOT" key="VIDEO_OPEN_TYPE" />
			str_buf.Format(_T("\t\t\t\t%s %s%s%s %s%s%s %s%s%s %s\r\n"), 
				LOCATION_START_TAG, 
				FILE_START_TAG, s_item->strFilename, FILE_END_TAG, 
				SECTION_START_TAG, s_item->strSection, SECTION_END_TAG, 
				KEY_START_TAG, s_item->strKey, KEY_END_TAG, 
				LOCATION_END_TAG
			);
			str += str_buf;

			//<title>비디오파일 열기 방식</title>
			str += _T("\t\t\t\t");
			str += TITLE_START_TAG;

			str_buf = _T("");
			s_item->mapTitle.Lookup(str_language, str_buf);
			str += str_buf;

			str += TITLE_END_TAG;
			str += _T("\r\n");

			//<values>
			str += _T("\t\t\t\t");
			str += VALUES_START_TAG;
			str += _T("\r\n");

			//<type>combobox</type>
			str += _T("\t\t\t\t\t");
			str += VALUE_TYPE_START_TAG;
			str += s_item->strValueType;
			str += VALUE_TYPE_END_TAG;
			str += _T("\r\n");

			//
			FOR_GC(s_item->listValues, k)
			{
				VALUE_ITEM* v_item = s_item->listValues.GetAt(k);

				CString str_name;
				v_item->mapName.Lookup(str_language, str_name);

				//<item name="재생할 때마다 새로 열기" value="2" default="1" />
				if( k==0 )
				{
					str_buf.Format(_T("\t\t\t\t\t%s %s%s%s %s%s%s %s1%s %s\r\n"), 
						VALUE_ITEM_START_TAG, 
						VALUE_NAME_START_TAG, str_name, VALUE_NAME_END_TAG, 
						VALUE_VALUE_START_TAG, v_item->strValue, VALUE_VALUE_END_TAG, 
						VALUE_DEFAULT_START_TAG, VALUE_DEFAULT_END_TAG, 
						VALUE_ITEM_END_TAG
					);
				}
				else
				{
					str_buf.Format(_T("\t\t\t\t\t%s %s%s%s %s%s%s %s\r\n"), 
						VALUE_ITEM_START_TAG, 
						VALUE_NAME_START_TAG, str_name, VALUE_NAME_END_TAG, 
						VALUE_VALUE_START_TAG, v_item->strValue, VALUE_VALUE_END_TAG, 
						VALUE_ITEM_END_TAG
					);
				}
				str += str_buf;
			}

			//</values>
			str += _T("\t\t\t\t");
			str += VALUES_END_TAG;
			str += _T("\r\n");

			//</item>
			str += _T("\t\t\t");
			str += ITEM_END_TAG;
			str += _T("\r\n");
		}

		//</settings>
		str += _T("\t\t");
		str += SETTINGS_END_TAG;
		str += _T("\r\n");

		//</korean>
		str_language.Insert(1, _T("/"));
		str += _T("\t");
		str += str_language;
		str += _T("\r\n");
	}

	str += _T("</terminal>");
}
