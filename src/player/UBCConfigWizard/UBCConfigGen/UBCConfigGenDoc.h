// UBCConfigGenDoc.h : CUBCConfigGenDoc 클래스의 인터페이스
//


#pragma once


class CUBCConfigGenDoc : public CDocument
{
protected: // serialization에서만 만들어집니다.
	CUBCConfigGenDoc();
	DECLARE_DYNCREATE(CUBCConfigGenDoc)

// 특성입니다.
public:

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// 구현입니다.
public:
	virtual ~CUBCConfigGenDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

	SETTINGS_LIST	m_listSettings;
	CStringArray	m_listLanguage;
	CMapStringToString	m_mapFilename;

	bool			LoadXML(CString& str);
	void			GenerateXML(CString& str);

	int				GetNextTag(CString& strSource, CString& strNextTag, int nStartIdx=0	);
	int				GetStringBetweenTag(CString& strSource, LPCTSTR lpszStart, LPCTSTR lpszEnd, CString& strSub, int nStartIdx=0);
	bool			GetSettingsList(CString& strSource, LPCTSTR lpszLanguage, SETTINGS_LIST& listSettings);

	SETTING_ITEM*	AddSetting(SETTING_ITEM* item);
	VALUE_ITEM*		AddValue(SETTING_ITEM* sItem, VALUE_ITEM* vItem);

public:
	bool			AddLanguageString(CString strLanguage);
	void			GetLanguageList(CStringArray& listLanguage) { listLanguage.Copy(m_listLanguage); };

	bool			AddFilename(CString strFilename);
	void			GetFilenameList(CStringArray& listFilename);

	int				GetSettingsListCount() { return m_listSettings.GetCount(); };
	SETTING_ITEM*	GetSettingItem(int idx) { return m_listSettings.GetAt(idx); };

	SETTING_ITEM*	AddNewSetting();
	bool			DeleteSetting(SETTING_ITEM* sItem);
	bool			MoveUpSetting(SETTING_ITEM* sItem);
	bool			MoveDownSetting(SETTING_ITEM* sItem);
};
