// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <afxsock.h>		// MFC 소켓 확장

#include <afxtempl.h>		// MFC 소켓 확장


#define		WM_APPLY_CHANGED_VALUE		(WM_USER + 1029)


#define		FOR_GC(LIST,ITER)			int cnt_##ITER=LIST.GetCount();		for(int ITER=0; ITER<cnt_##ITER; ITER++)
#define		FOR_GIC(LIST,ITER)			int cnt_##ITER=LIST.GetItemCount();	for(int ITER=0; ITER<cnt_##ITER; ITER++)

#define		FOR_GC_REV(LIST,ITER)		int cnt_##ITER=LIST.GetCount();		for(int ITER=cnt_##ITER-1; ITER>=0; ITER--)
#define		FOR_GIC_REV(LIST,ITER)		int cnt_##ITER=LIST.GetItemCount();	for(int ITER=cnt_##ITER-1; ITER>=0; ITER--)


int UTF8toUNICODE(LPCSTR lpszUTF8, CStringW& strUnicode);
int UNICODEtoUTF8(LPCWSTR lpwzUnicode, CStringA& strUTF8);


//
class VALUE_ITEM
{
public:
	VALUE_ITEM() {};
	~VALUE_ITEM() {};

	void Copy(VALUE_ITEM& vItem)
	{
		strValue = vItem.strValue;

		POSITION pos = vItem.mapName.GetStartPosition();
		while( pos )
		{
			CString key, value;
			vItem.mapName.GetNextAssoc(pos, key, value);
			mapName.SetAt(key ,value);
		}
	};

	bool IsEqual(VALUE_ITEM& vItem)
	{
		if( _tcscmp(strValue, vItem.strValue)!=0 ) return false;

		if( mapName.GetCount() != vItem.mapName.GetCount() ) return false;
		POSITION pos = vItem.mapName.GetStartPosition();
		while( pos )
		{
			CString key, value;
			vItem.mapName.GetNextAssoc(pos, key, value);

			CString val;
			mapName.Lookup(key, val);
			if( _tcscmp(value, val)!=0 ) return false;
		}
		return true;
	};

	CMapStringToString	mapName;	// pair of (language, name)
	CString			strValue;
};

typedef CArray<VALUE_ITEM*, VALUE_ITEM*>		VALUES_LIST;


//
class SETTING_ITEM
{
public:
	SETTING_ITEM() {};
	~SETTING_ITEM() {
		for(int i=0; i<listValues.GetCount(); i++)
		{
			VALUE_ITEM* val_item = listValues.GetAt(i);
			if( val_item ) delete val_item;
		}
		listValues.RemoveAll();
	};

	void	Copy(SETTING_ITEM& sItem)
	{
		strFilename	 = sItem.strFilename;
		strSection	 = sItem.strSection;
		strKey		 = sItem.strKey;
		strValueType = sItem.strValueType;

		POSITION pos = sItem.mapTitle.GetStartPosition();
		while( pos )
		{
			CString key, value;
			sItem.mapTitle.GetNextAssoc(pos, key, value);
			mapTitle.SetAt(key, value);
		}

		FOR_GC(sItem.listValues, i)
		{
			VALUE_ITEM* v_item = sItem.listValues.GetAt(i);

			VALUE_ITEM* item = new VALUE_ITEM;
			listValues.Add(item);

			item->Copy(*v_item);
		}
	};

	bool IsEqual(SETTING_ITEM& sItem)
	{
		if( _tcscmp(strFilename, sItem.strFilename)!=0 ) return false;
		if( _tcscmp(strSection, sItem.strSection)!=0 ) return false;
		if( _tcscmp(strKey, sItem.strKey)!=0 ) return false;
		if( _tcscmp(strValueType, sItem.strValueType)!=0 ) return false;

		if( mapTitle.GetCount() != sItem.mapTitle.GetCount() ) return false;
		POSITION pos = sItem.mapTitle.GetStartPosition();
		while( pos )
		{
			CString key, value;
			sItem.mapTitle.GetNextAssoc(pos, key, value);

			CString val;
			mapTitle.Lookup(key, val);
			if( _tcscmp(value, val)!=0 ) return false;
		}

		if( listValues.GetCount() != sItem.listValues.GetCount() ) return false;
		FOR_GC(sItem.listValues, i)
		{
			VALUE_ITEM* v_item1 = sItem.listValues.GetAt(i);
			VALUE_ITEM* v_item2 = listValues.GetAt(i);

			if( !v_item1->IsEqual(*v_item2) ) return false;
		}
		return true;
	}

	CString			strFilename;
	CString			strSection;
	CString			strKey;
	CMapStringToString	mapTitle;	// pair of (language, title)
	CString			strValueType;
	VALUES_LIST		listValues;
};

typedef CArray<SETTING_ITEM*, SETTING_ITEM*>	SETTINGS_LIST;


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


