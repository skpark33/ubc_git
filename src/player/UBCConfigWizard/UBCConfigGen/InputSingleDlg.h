#pragma once
#include "afxwin.h"

#include "ReposControl2.h"

// CInputSingleDlg 대화 상자입니다.

class CInputSingleDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputSingleDlg)

public:
	CInputSingleDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CInputSingleDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_INPUT_SINGLE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CReposControl2	m_reposControl;
	int		m_nDialogHeight;

	CString	m_strTitle;
	CString	m_strName;
	CString	m_strValue;

	bool	m_bNumberValue;

public:
	CStatic m_stcGroup;
	CStatic	m_stcName;
	CEdit	m_editValue;

	CButton m_btnOK;
	CButton m_btnCancel;

	void	SetDialogInfo(LPCTSTR lpszTitle, LPCTSTR lpszName, LPCTSTR lpszValue=NULL);
	void	SetDialogInfo(UINT nTitleID, UINT nNameID, LPCSTR lpszValue=NULL);
	void	SetNumberValue(bool isNumber=true) { m_bNumberValue=isNumber; };

	void	GetValue(int& value) { value=_ttoi(m_strValue); };
	void	GetValue(CString& value) { value=m_strValue; };
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
