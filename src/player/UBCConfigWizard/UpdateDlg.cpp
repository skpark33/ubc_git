// UpdateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCConfigWizard.h"
#include "UpdateDlg.h"

#include <io.h>
#include "common/libHttpRequest/HttpRequest.h"


#define		WM_CONNECT_TO_SERVER		(WM_USER + 128)
#define		WM_COMPLETE_UPDATE			(WM_USER + 130)
#define		WM_NOTHING_TO_UPDATE		(WM_USER + 131)
#define		WM_FAIL_TO_CONNECT			(WM_USER + 132)
#define		WM_FAIL_TO_WRITE			(WM_USER + 133)

#define		TEMP_CONFIG_FILENAME		"TerminalSettings.tmp.xml"
#define		BACKUP_CONFIG_FILENAME		"TerminalSettings.bak.xml"
#define		CONFIG_FILENAME				"TerminalSettings.xml"


// CUpdateDlg 대화 상자입니다.

bool CUpdateDlg::m_bRunThread = false;

IMPLEMENT_DYNAMIC(CUpdateDlg, CDialog)

CUpdateDlg::CUpdateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUpdateDlg::IDD, pParent)
{

}

CUpdateDlg::~CUpdateDlg()
{
}

void CUpdateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_stcStatus);
}


BEGIN_MESSAGE_MAP(CUpdateDlg, CDialog)
	ON_MESSAGE(WM_CONNECT_TO_SERVER, OnConnectToServer)
	ON_MESSAGE(WM_COMPLETE_UPDATE, OnCompleteUpdate)
	ON_MESSAGE(WM_NOTHING_TO_UPDATE, OnNothingToUpdate)
	ON_MESSAGE(WM_FAIL_TO_CONNECT, OnFailToConnect)
	ON_MESSAGE(WM_FAIL_TO_WRITE, OnFailToWrite)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CUpdateDlg 메시지 처리기입니다.

BOOL CUpdateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CUpdateDlg::m_bRunThread = true;

	CWinThread* thread = ::AfxBeginThread(UpdateDownloadFunc, (LPVOID)GetSafeHwnd(), THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
	thread->m_bAutoDelete = true;
	thread->ResumeThread();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CUpdateDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();
}

void CUpdateDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnCancel();
}

void CUpdateDlg::OnTimer(UINT_PTR nIDEvent)
{
	if( nIDEvent == 1025 )
	{
		OnCancel();
	}

	CDialog::OnTimer(nIDEvent);
}

LRESULT CUpdateDlg::OnConnectToServer(WPARAM wParam, LPARAM lParam)
{
	CString msg;
	msg.LoadString(IDS_MSG_CONNECT_TO_SERVER);
	m_stcStatus.SetWindowText(msg);
	return 0;
}

LRESULT CUpdateDlg::OnCompleteUpdate(WPARAM wParam, LPARAM lParam)
{
	CString msg;
	msg.LoadString(IDS_MSG_COMPLETE_UPDATE);
	m_stcStatus.SetWindowText(msg);

	SetTimer(1025, 1000, NULL);
	return 0;
}

LRESULT CUpdateDlg::OnNothingToUpdate(WPARAM wParam, LPARAM lParam)
{
	OnCancel();
	return 0;
}

LRESULT CUpdateDlg::OnFailToConnect(WPARAM wParam, LPARAM lParam)
{
	CString msg;
	msg.LoadString(IDS_MSG_FAIL_TO_CONNECT);
	m_stcStatus.SetWindowText(msg);

	SetTimer(1025, 1000, NULL);
	return 0;
}

LRESULT CUpdateDlg::OnFailToWrite(WPARAM wParam, LPARAM lParam)
{
	CString msg;
	msg.LoadString(IDS_MSG_FAIL_TO_WRITE);
	m_stcStatus.SetWindowText(msg);

	SetTimer(1025, 1000, NULL);
	return 0;
}

UINT UpdateDownloadFunc(LPVOID pParam)
{
	HWND parent_wnd = (HWND)pParam;

	//
	::PostMessage(parent_wnd, WM_CONNECT_TO_SERVER, NULL, NULL);

	//
	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_CENTER);

	//
	CString out_msg;
	if( !http_request.RequestGet("UBC_Player/TerminalSettings.xml", out_msg) || out_msg.GetLength()==0 )
	{
		::PostMessage(parent_wnd, WM_FAIL_TO_CONNECT, NULL, NULL);
		return FALSE;
	}

	CFileStatus fs;
	if( !CFile::GetStatus(CONFIG_FILENAME, fs) || (int)fs.m_size!=out_msg.GetLength() )
	{
		// write to temp
		CFile tmp_file;
		if( !tmp_file.Open(TEMP_CONFIG_FILENAME, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
		{
			::PostMessage(parent_wnd, WM_FAIL_TO_WRITE, NULL, NULL);
			return FALSE;
		}

		//unsigned char header[3] = { 0xEF, 0xBB, 0xBF };
		TRY
		{
			//tmp_file.Write(header, sizeof(header));
			tmp_file.Write((LPVOID)(LPCSTR)out_msg, out_msg.GetLength());
			tmp_file.Close();
		}
		CATCH(CFileException, ex)
		{
			::PostMessage(parent_wnd, WM_FAIL_TO_WRITE, NULL, NULL);
			return FALSE;
		}
		END_CATCH

		// delete old backup-file
		::DeleteFile(BACKUP_CONFIG_FILENAME);
		// move file to backup-file
		if( _access(CONFIG_FILENAME,0)==0 && !::MoveFile(CONFIG_FILENAME, BACKUP_CONFIG_FILENAME) )
		{
			::PostMessage(parent_wnd, WM_FAIL_TO_WRITE, NULL, NULL);
			return FALSE;
		}
		// move tmp-file to file
		if( !::MoveFile(TEMP_CONFIG_FILENAME, CONFIG_FILENAME) )
		{
			::MoveFile(BACKUP_CONFIG_FILENAME, CONFIG_FILENAME);
			::PostMessage(parent_wnd, WM_FAIL_TO_WRITE, NULL, NULL);
			return FALSE;
		}
		//::DeleteFile(BACKUP_CONFIG_FILENAME);

		//
		::PostMessage(parent_wnd, WM_COMPLETE_UPDATE, NULL, NULL);
	}
	else
	{
		::PostMessage(parent_wnd, WM_NOTHING_TO_UPDATE, NULL, NULL);
	}

	return TRUE;
}
