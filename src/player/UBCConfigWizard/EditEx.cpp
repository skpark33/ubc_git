// EditEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCConfigWizard.h"
#include "EditEx.h"


// CEditEx

IMPLEMENT_DYNAMIC(CEditEx, CEdit)

CEditEx::CEditEx(CWnd* pParent, CRect& rect, DWORD dwStyle, UINT nID, LPCSTR lpszInitText, int nRow, int nCol)
:	m_strInitText (lpszInitText)
,	m_nRow (nRow)
,	m_nCol (nCol)
{
	m_bEndEdit = false;

	DWORD dwEditStyle = /*WS_BORDER |*/ WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL | ES_LEFT | dwStyle;
	if( !Create(dwEditStyle, rect, pParent, nID) ) return;

	SetFont(pParent->GetFont());
	SetWindowText(lpszInitText);
	SetFocus();

	SetSel(0, -1);
	SetSel(-1, 0);
}

CEditEx::~CEditEx()
{
}


BEGIN_MESSAGE_MAP(CEditEx, CEdit)
	ON_WM_KILLFOCUS()
END_MESSAGE_MAP()


// CEditEx 메시지 처리기입니다.


void CEditEx::OnKillFocus(CWnd* pNewWnd)
{
	CEdit::OnKillFocus(pNewWnd);

	EndEdit();
}

void CEditEx::CancelEdit()
{
	if( IsWindow(GetSafeHwnd()) )
	{
		m_bEndEdit = true;
		CWnd* pOwner = GetOwner();
		if( pOwner )
			pOwner->PostMessage(WM_CANCEL_EDIT, (WPARAM)(CWnd*)this, NULL );
	}
}

void CEditEx::EndEdit()
{
	if( m_bEndEdit ) return;

	CString* str = new CString;
	GetWindowText(*str);

	CWnd* pOwner = GetOwner();
	if( pOwner )
	{
		EDIT_RESULT* er = new EDIT_RESULT;

		er->row = m_nRow;
		er->col = m_nCol;
		er->value = str;
		er->control = this;

		pOwner->PostMessage(WM_END_EDIT, (WPARAM)er, NULL );
	}
}

BOOL CEditEx::PreTranslateMessage(MSG* pMsg)
{
	//if( pMsg->message == WM_SYSCHAR ) return TRUE;
	if( pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE )
	{
		CancelEdit();
		return TRUE;
	}
	if( pMsg->message == WM_KEYDOWN && ( pMsg->wParam == VK_TAB || pMsg->wParam == VK_RETURN ) )
	{
		GetParent()->SetFocus();
	}

	return CEdit::PreTranslateMessage(pMsg);
}
