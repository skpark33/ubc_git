#pragma once


// CEditEx

class CEditEx : public CEdit
{
	DECLARE_DYNAMIC(CEditEx)

public:
	CEditEx(CWnd* pParent, CRect& rect, DWORD dwStyle, UINT nID, LPCSTR lpszInitText, int nRow, int nCol);
	virtual ~CEditEx();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnKillFocus(CWnd* pNewWnd);

protected:
	CString	m_strInitText;
	int		m_nRow;
	int		m_nCol;
	bool	m_bEndEdit;

	void	CancelEdit();
	void	EndEdit();
};
