//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCConfigWizard.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_UBCCONFIGWIZARD_DIALOG      102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_CONFIG_UPDATE               129
#define IDD_UPDATE                      129
#define IDD_DETAIL_INFO                 130
#define IDC_STATIC_STATUS               1000
#define IDC_LIST_SETTINGS               1001
#define IDC_BUTTON_SAVE_SETTINGS        1002
#define IDC_BUTTON_CLOSE                1003
#define IDC_EDIT_FILENAME               1004
#define IDC_EDIT_SECTION                1005
#define IDC_EDIT_KEY                    1006
#define IDC_LIST_COMBOBOX_VALUES        1007
#define IDC_EDIT_TITLE                  1008
#define IDC_EDIT_VALUE_TYPE             1009
#define IDS_CURRENT_LANGUAGE_START2_TAG 9998
#define IDS_CURRENT_LANGUAGE_START_TAG  9998
#define IDS_CURRENT_LANGUAGE_END_TAG    9999
#define IDS_MSG_CONNECT_TO_SERVER       10000
#define IDS_MSG_COMPLETE_UPDATE         10001
#define IDS_MSG_FAIL_TO_CONNECT         10002
#define IDS_MSG_FAIL_TO_WRITE           10003
#define IDS_MSG_VALUE_CHANGE_EXIT_NOW   10004
#define IDS_MSG_CHANGED_VALUE_EXIST_SAVE_BEFORE_EXIT 10004
#define IDS_COLUMN_SETTINGS             10005
#define IDS_COLUMN_VALUE                10006
#define IDS_VALUE_TYPE_COMBOBOX         10007
#define IDS_VALUE_TYPE_INTEGER          10008
#define IDS_VALUE_TYPE_STRING           10009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
