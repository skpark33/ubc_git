#pragma once
#include "afxwin.h"


// CUpdateDlg 대화 상자입니다.

class CUpdateDlg : public CDialog
{
	DECLARE_DYNAMIC(CUpdateDlg)

public:
	CUpdateDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CUpdateDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UPDATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

public:
	CStatic	m_stcStatus;

	static	bool	m_bRunThread;

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnConnectToServer(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompleteUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNothingToUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFailToConnect(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFailToWrite(WPARAM wParam, LPARAM lParam);
};


static UINT UpdateDownloadFunc(LPVOID pParam);
