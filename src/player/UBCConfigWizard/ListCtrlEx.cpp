// ListCtrlEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCConfigWizard.h"
#include "ListCtrlEx.h"

#include "EditEx.h"
#include "ComboBoxExt.h"

// CListCtrlEx

IMPLEMENT_DYNAMIC(CListCtrlEx, CListCtrl)

CListCtrlEx::CListCtrlEx()
{
}

CListCtrlEx::~CListCtrlEx()
{
}


BEGIN_MESSAGE_MAP(CListCtrlEx, CListCtrl)
	ON_NOTIFY_REFLECT(NM_CLICK, &CListCtrlEx::OnNMClick)
	ON_MESSAGE(WM_END_EDIT, OnEndEdit)
	ON_MESSAGE(WM_CANCEL_EDIT, OnCancelEdit)
	ON_MESSAGE(WM_END_COMBOBOX, OnEndCombobox)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CListCtrlEx::OnNMCustomdraw)
END_MESSAGE_MAP()



// CListCtrlEx 메시지 처리기입니다.


void CListCtrlEx::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	int row = pNMItemActivate->iItem;
	int col = pNMItemActivate->iSubItem;

	if( row < 0 || col < 1 ) return;

	ModifyStyle(NULL, LVS_SHOWSELALWAYS);

	CRect rect;
	GetSubItemRect(row, col, LVIR_BOUNDS, rect);

	CString str_val = GetItemText(row, col);
	SETTING_ITEM* set_item = (SETTING_ITEM*)GetItemData(row);

	if( set_item->strValueType == "integer" )
	{
		CEditEx* edit = new CEditEx(this, rect, ES_NUMBER, 1000, str_val, row, col);
	}
	else if( set_item->strValueType == "string" )
	{
		CEditEx* edit = new CEditEx(this, rect, NULL, 1000, str_val, row, col);
	}
	else if( set_item->strValueType == "combobox" )
	{
		rect.bottom += 200;
		CComboBoxExt* cbx = new CComboBoxExt(this, rect, NULL, 1000, row, col);

		int val_cnt = set_item->listValues.GetCount();
		for(int i=0; i<val_cnt; i++)
		{
			VALUE_ITEM* val_item = set_item->listValues.GetAt(i);
			cbx->AddString(val_item->strName, val_item);
			cbx->SetItemHeight(i, 20);
		}
		cbx->SetInitValue(str_val);
		cbx->ShowDropDown();
	}

	*pResult = 0;
}

void CListCtrlEx::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	*pResult = 0;

	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
	int idx = static_cast<int>( pLVCD->nmcd.dwItemSpec );

	switch(pLVCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		break;

	case CDDS_ITEMPREPAINT:
		{
			SETTING_ITEM* set_item = (SETTING_ITEM*)GetItemData(idx);

			if( set_item && set_item->bModified )
			{
				pLVCD->clrText   = RGB(0,0,0);
				pLVCD->clrTextBk = RGB(255,128,128);

				*pResult = CDRF_NOTIFYITEMDRAW;
			}
			else
				*pResult = CDRF_DODEFAULT;
		}
		break;

	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		*pResult = CDRF_NEWFONT;
		break;

	default:
		*pResult = CDRF_DODEFAULT;
		break;
	}
}

LRESULT CListCtrlEx::OnEndEdit(WPARAM wParam ,LPARAM lParam)
{
	ModifyStyle(LVS_SHOWSELALWAYS, NULL);
	::AfxGetMainWnd()->PostMessage(WM_CHANGE_FOCUS);

	EDIT_RESULT* er = (EDIT_RESULT*)wParam;

	//CString old_val = GetItemText(er->row, er->col);
	//if( old_val != *(er->value) )
	//{
	//	SETTING_ITEM* set_item = (SETTING_ITEM*)GetItemData(er->row);
	//	set_item->bModified = true;

	//	m_bModified=true;
	//}
	SETTING_ITEM* set_item = (SETTING_ITEM*)GetItemData(er->row);
	set_item->strNewValue = *(er->value);
	set_item->bModified = (set_item->strInitValue != set_item->strNewValue);

	SetItemText(er->row, er->col, *(er->value));
	delete er->value;

	CWnd* wnd = (CWnd*)er->control;
	wnd->DestroyWindow();
	delete wnd;

	delete er;

	return 0;
}

LRESULT CListCtrlEx::OnCancelEdit(WPARAM wParam ,LPARAM lParam)
{
	ModifyStyle(LVS_SHOWSELALWAYS, NULL);
	::AfxGetMainWnd()->PostMessage(WM_CHANGE_FOCUS);

	CWnd* wnd = (CWnd*)wParam;
	wnd->DestroyWindow();
	delete wnd;

	return 0;
}

LRESULT CListCtrlEx::OnEndCombobox(WPARAM wParam ,LPARAM lParam)
{
	ModifyStyle(LVS_SHOWSELALWAYS, NULL);
	::AfxGetMainWnd()->PostMessage(WM_CHANGE_FOCUS);

	COMBOBOX_RESULT* cr = (COMBOBOX_RESULT*)wParam;

	SETTING_ITEM* set_item = (SETTING_ITEM*)GetItemData(cr->row);

	set_item->strNewValue = cr->value->strValue;
	set_item->bModified = (set_item->strInitValue != cr->value->strName);

	SetItemText(cr->row, cr->col, cr->value->strName);

	CWnd* wnd = (CWnd*)cr->control;
	wnd->DestroyWindow();
	delete wnd;

	delete cr;

	return 0;
}

bool CListCtrlEx::IsModified()
{
	int cnt = GetItemCount();
	for(int i=0; i<cnt; i++)
	{
		SETTING_ITEM* set_item = (SETTING_ITEM*)GetItemData(i);
		if( set_item && set_item->bModified )
			return true;
	}

	return false;
}
