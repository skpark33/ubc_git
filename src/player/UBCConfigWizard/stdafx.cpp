// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// UBCConfigWizard.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"



int UTF8ToAnsi( char* szSrc, char* strDest, int destSize )
{
	//WCHAR  szUnicode[81920];
	//char  szAnsi[81920];

	//int nSize = MultiByteToWideChar(CP_UTF8, 0, szSrc, -1, 0, 0);

	//printf("nSize=%d\n", nSize);

	//int nUnicodeSize = MultiByteToWideChar(CP_UTF8, 0, szSrc, -1 , szUnicode, nSize);
	//printf("nUnicodeSize=%d\n", nUnicodeSize);

	//int nAnsiSize = WideCharToMultiByte(CP_ACP, 0, szUnicode, nUnicodeSize, szAnsi, sizeof( szAnsi ), NULL, NULL);
	//printf("nAnsiSize=%d\n", nAnsiSize);
	////assert( destSize > nAnsiSize );
	//memcpy(strDest, szAnsi, nAnsiSize);
	//strDest[ nAnsiSize ] = 0;
	//return nAnsiSize;

	// 2011-05-06 : Heap영역 사용으로 변경 (by seventhstone)
	LPWSTR lpwzIn;
	LPSTR lpszOut;
	int nInBuf = ::MultiByteToWideChar(CP_UTF8, 0, szSrc, -1, NULL, 0);
	int nOutLen = -1;

	if( nInBuf > 0 )
	{
		if( (lpwzIn = new WCHAR[nInBuf + 1]) != NULL )
		{
			memset(lpwzIn, 0, sizeof(WCHAR)*(nInBuf + 1) );
			::MultiByteToWideChar(CP_UTF8, 0, szSrc, -1, lpwzIn, nInBuf + 1);

			nOutLen = ::WideCharToMultiByte(CP_ACP, 0, lpwzIn, -1, NULL, 0, NULL, NULL);

			if( (lpszOut = new char[nOutLen + 1]) != NULL )
			{
				nOutLen = ::WideCharToMultiByte(CP_ACP, 0, lpwzIn, -1, lpszOut, nOutLen + 1, NULL, NULL);
				memset(strDest, 0, nOutLen + 1 );
				memcpy(strDest, lpszOut, nOutLen);
				delete[] lpszOut;
			}
			else
				nOutLen = -1;

			delete[] lpwzIn;
		}
	}

	return nOutLen;
}


LPCTSTR GetConfigDataPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
		_mainDirectory += "Data\\";
	}

	return _mainDirectory;
}

CString LoadString(UINT nID)
{
	CString str;
	str.LoadString(nID);
	return str;
}
