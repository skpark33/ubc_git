#pragma once


// CComboBoxExt

class CComboBoxExt : public CComboBox
{
	DECLARE_DYNAMIC(CComboBoxExt)

public:
	CComboBoxExt(CWnd* pParent, CRect& rect, DWORD dwStyle, UINT nID, int nRow, int nCol);
	virtual ~CComboBoxExt();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	DECLARE_MESSAGE_MAP()

	int		m_nRow;
	int		m_nCol;
	bool	m_bEndEdit;

	void	CancelEdit();
	void	EndEdit();

public:
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnCbnSelchange();
	afx_msg void OnCbnCloseup();

	void	AddString(LPCSTR lpszString, VALUE_ITEM* pValueItem);

	void	SetInitValue(LPCSTR lpszInitVal);
};



