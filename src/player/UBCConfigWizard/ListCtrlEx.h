#pragma once


// CListCtrlEx

class CListCtrlEx : public CListCtrl
{
	DECLARE_DYNAMIC(CListCtrlEx)

public:
	CListCtrlEx();
	virtual ~CListCtrlEx();

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnEndEdit(WPARAM wParam ,LPARAM lParam);
	afx_msg LRESULT OnCancelEdit(WPARAM wParam ,LPARAM lParam);
	afx_msg LRESULT OnEndCombobox(WPARAM wParam ,LPARAM lParam);

	bool	IsModified();
};
