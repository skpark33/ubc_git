// AdminLoginDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCHostAuthorizer.h"
#include "AdminLoginDialog.h"


// CAdminLoginDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAdminLoginDialog, CDialog)

CAdminLoginDialog::CAdminLoginDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CAdminLoginDialog::IDD, pParent)
{

}

CAdminLoginDialog::~CAdminLoginDialog()
{
}

void CAdminLoginDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_EMAIL, m_editEmail);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_editPassword);
}


BEGIN_MESSAGE_MAP(CAdminLoginDialog, CDialog)
END_MESSAGE_MAP()


// CAdminLoginDialog 메시지 처리기입니다.

BOOL CAdminLoginDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAdminLoginDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	// 바이너리에서 id 및 패스워드가 보이지 않도록 꼼수
	int email[] = { 'u'*1, 'b'*2, 'c'*3, '@'*4, 's'*5, 'q'*6, 'i'*7, 's'*8, 'o'*9, 'f'*10, 't'*11, '.'*12, 'c'*13, 'o'*14, 'm'*15, 0, };
	int password[] = { 's'*1, 'q'*2, 'i'*3, 'c'*4, 'o'*5, 'p'*6, 0, };

	CString str_email;
	m_editEmail.GetWindowText(str_email);

	CString str_pw;
	m_editPassword.GetWindowText(str_pw);

	bool login = true;

	//
	int i=0;
	for(i=0; i<str_email.GetLength(); i++)
	{
		if( (email[i] == 0) ||
			(str_email[i] * (i+1) != email[i]) )
		{
			login = false;
			break;
		}
	}
	if(login && email[i] != 0) login = false;

	//
	i=0;
	for(i=0; i<str_pw.GetLength(); i++)
	{
		if( (password[i] == 0) ||
			(str_pw[i] * (i+1) != password[i]) )
		{
			login = false;
			break;
		}
	}
	if(login && password[i] != 0) login = false;

	//
	if( !login )
	{
		CString caption, text;
		GetWindowText(caption);
		text.LoadString(IDS_WARNING_CANT_LOGIN);
		::MessageBox(GetSafeHwnd(), text, caption, MB_ICONWARNING);

		m_editPassword.SetFocus();
		//m_editEmail.SetSel(0, -1);

		return;
	}

	CDialog::OnOK();
}
