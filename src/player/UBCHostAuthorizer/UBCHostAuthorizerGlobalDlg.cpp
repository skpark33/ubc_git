// UBCHostAuthorizerGlobalDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCHostAuthorizer.h"
#include "UBCHostAuthorizerGlobalDlg.h"

#include "AdminLoginDialog.h"


#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>

#include "common/libCommon/ubcIni.h"
//#include "common/libCommon/ubcIniMux.h"
#include "common/libDBUtil/dbUtil.h"
#include "common/libInstall/installUtil.h"
#include "common/libScratch/scratchUtil.h"

#include "libInstallWeb/installWebUtil.h"
#include "libServerCheck/ServerCheck.h"


ciSET_DEBUG(10, "CUBCHostAuthorizerGlobalDlg");


#define		TIMER_ID_ADMIN_LOGIN		1026
#define		TIMER_TIME_ADMIN_LOGIN		10


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUBCHostAuthorizerGlobalDlg 대화 상자


CUBCHostAuthorizerGlobalDlg::CUBCHostAuthorizerGlobalDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCHostAuthorizerGlobalDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bAdminLoginMode = false;
	m_bHostAuthentication = false;
	m_bNeedRebooting = false;
}

void CUBCHostAuthorizerGlobalDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_INTRO, m_stcIntro);
	DDX_Control(pDX, IDC_EDIT_NAME, m_editName);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_cbxType);
	DDX_Control(pDX, IDC_STATIC_SERVER_SETTINGS, m_stcServerSettings);
	DDX_Control(pDX, IDC_STATIC_SERVER_IP, m_stcServerIP);
	DDX_Control(pDX, IDC_IPADDR_SERVER, m_ipaddrServer);
	DDX_Control(pDX, IDC_STATIC_SERVER_TYPE, m_stcServerType);
	DDX_Control(pDX, IDC_COMBO_SERVER_TYPE, m_cbxServerType);
	DDX_Control(pDX, IDC_BUTTON_AUTO_SEARCH, m_btnAutoSearch);
	DDX_Control(pDX, IDC_BUTTON_APPLY, m_btnApply);
	DDX_Control(pDX, IDC_BUTTON_REBOOTING, m_btnRebooting);
	DDX_Control(pDX, IDCANCEL, m_btnClose);
}

BEGIN_MESSAGE_MAP(CUBCHostAuthorizerGlobalDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_AUTHENTICATE, &CUBCHostAuthorizerGlobalDlg::OnBnClickedButtonAuthenticate)
	ON_BN_CLICKED(IDC_BUTTON_AUTO_SEARCH, &CUBCHostAuthorizerGlobalDlg::OnBnClickedButtonAutoSearch)
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CUBCHostAuthorizerGlobalDlg::OnBnClickedButtonApply)
	ON_BN_CLICKED(IDC_BUTTON_REBOOTING, &CUBCHostAuthorizerGlobalDlg::OnBnClickedButtonRebooting)
END_MESSAGE_MAP()


// CUBCHostAuthorizerGlobalDlg 메시지 처리기

BOOL CUBCHostAuthorizerGlobalDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	CString app_title;
	app_title.LoadString(IDS_APP_TITLE);
	SetWindowText(app_title);

	//
	m_strEdition = STANDARD_EDITION;
	if(ciArgParser::getInstance()->isSet("+PLS"))
	{
		ciDEBUG(10, ("PLUS_EDITION") );
		m_strEdition = PLUS_EDITION;
	}
	if(ciArgParser::getInstance()->isSet("+ENT"))
	{
		ciDEBUG(10, ("ENTERPRISE_EDITION") );
		m_strEdition = ENTERPRISE_EDITION;
	}

	GetComputerName(m_strHostId);

	//
	InitControl();

	//Added by jwh
	CenterWindow();
	::SetWindowPos(this->m_hWnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED | SWP_SHOWWINDOW);

	// check admin login
	if(ciArgParser::getInstance()->isSet("+force"))
	{
		ciDEBUG(10, ("+force") );
		SetTimer(TIMER_ID_ADMIN_LOGIN, TIMER_TIME_ADMIN_LOGIN, NULL);
	}

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUBCHostAuthorizerGlobalDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCHostAuthorizerGlobalDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();

		CWnd* banner = GetDlgItem(IDC_STATIC_BANNER);
		CRect rect_banner;
		banner->GetWindowRect(rect_banner);
		ScreenToClient(rect_banner);

		BITMAP bmpinfo;
		m_bmpBackground.GetBitmap(&bmpinfo);

		CDC* pDC = GetDC();

		CDC memDC;
		memDC.CreateCompatibleDC(pDC);
		memDC.SelectObject(&m_bmpBackground);

		pDC->BitBlt(rect_banner.left, rect_banner.top, bmpinfo.bmWidth, rect_banner.Height(), &memDC, 0, (bmpinfo.bmHeight - rect_banner.Height())/2, SRCCOPY);

		ReleaseDC(pDC);
	}
}

void CUBCHostAuthorizerGlobalDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(TIMER_ID_ADMIN_LOGIN == nIDEvent)
	{
		KillTimer(nIDEvent);

		ciDEBUG(10, ("Admin Login") );

		CAdminLoginDialog dlg;
		if(dlg.DoModal() == IDCANCEL)
		{
			ciWARN( ("Admin Login FAIL !!!") );

			MessageBoxEx(IDS_ERROR_ADMIN_LOGIN_FAIL, MB_ICONSTOP);

			CDialog::OnOK();
		}
		else
		{
			ciDEBUG(10, ("Admin Login Success !!!") );
			m_bAdminLoginMode = true;
		}
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCHostAuthorizerGlobalDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CUBCHostAuthorizerGlobalDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();
}

void CUBCHostAuthorizerGlobalDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(m_bHostAuthentication == false)
	{
		ciDEBUG(10, ("NO Host Authentication") );

		int ret = MessageBoxEx(IDS_WARNING_NOT_AUTHENTICATE, MB_ICONWARNING | MB_YESNO);

		if(ret == IDNO) return;
	}

	if(m_bNeedRebooting == true)
	{
		ciDEBUG(10, ("Need Rebooting") );

		int ret = MessageBoxEx(IDS_WARNING_NEED_TO_REBOOT_NOW, MB_ICONWARNING | MB_YESNO);

		if(ret == IDYES)
		{
			ciDEBUG(10, ("Rebooting...") );
			OnBnClickedButtonRebooting();
			//return;
		}
	}

	//
	CDialog::OnCancel();

	//
	// just close...
	//
}

void CUBCHostAuthorizerGlobalDlg::OnBnClickedButtonAuthenticate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	// 외극 한정 버전 - 딜러id,단말기type로 인증

	BeginWaitCursor();

	CString name;
	m_editName.GetWindowText(name);
	int type = static_cast<int>(m_cbxType.GetItemData(m_cbxType.GetCurSel()));

	ciString siteId, hostId, errString, ip, mac;
	siteId = name;
	char buf[64] = {0};
	sprintf(buf, "%d", type);
	hostId = buf;

	installWebUtil::getInstance()->SetOnceTimeForceAuth(m_bAdminLoginMode);
	int ret = installWebUtil::getInstance()->authByNAME(siteId, hostId, m_strEdition, errString, ip, mac, false);

	ciDEBUG(10, ("Site=%s", siteId.c_str()) );
	ciDEBUG(10, ("Host=%s", hostId.c_str()) );
	ciDEBUG(10, ("Edition=%s", m_strEdition) );
	ciDEBUG(10, ("ErrString=%s", errString.c_str()) );
	ciDEBUG(10, ("IP=%s", ip.c_str()) );
	ciDEBUG(10, ("MacAddr=%s", mac.c_str()) );

	CString com_name;
	if( !GetComputerName(com_name) )
	{
		EndWaitCursor();

		ciERROR( ("GetComputerName FAIL !!!") );

		CString format, text;

		format.LoadString(IDS_ERROR_INTERNAL);
		text.Format(format, "GetComputerName");

		MessageBoxEx(text, MB_ICONSTOP);
		return;
	}

	ciDEBUG(10, ("GetComputerName=%s", (LPCTSTR)com_name) );

	if(ret == 2 || ret == 1 )
	{
		// write hostname,hosttype
		ubcConfig aIni("UBCVariables");
		aIni.set("GLOBAL","HostName", (LPCSTR)name);
		aIni.set("GLOBAL","HostType", (ciLong)type);

		if ( com_name != hostId.c_str() )
		{
			ciDEBUG(10, ("SetComputerName=%s", hostId.c_str()) );

			//
			if( !SetComputerName(hostId.c_str()) )
			{
				EndWaitCursor();

				ciERROR( ("SetComputerName FAIL !!!") );

				CString format, text;

				format.LoadString(IDS_ERROR_INTERNAL);
				text.Format(format, "SetComputerName");

				MessageBoxEx(text, MB_ICONSTOP);
				return;
			}

			m_strHostId = hostId.c_str();

			CString msg;
			msg.LoadString(IDS_WARNING_NEED_TO_REBOOT);
			errString += (LPCSTR)msg;

			ret = 2;
		}
	}

	switch(ret)
	{
	case 2:		// AUTH_SUCCEED_BUT_REBOOTING
		ciDEBUG(10, ("AUTH_SUCCEED_BUT_REBOOTING=%s", errString.c_str()) );
		InitControl();
		CheckAndModifyVNCBinary();
		m_btnRebooting.EnableWindow(TRUE);
		m_bNeedRebooting = true;
		EndWaitCursor();
		MessageBoxEx(errString.c_str(), MB_ICONINFORMATION);
		break;
	case 1:		// AUTH_SUCCEED
		ciDEBUG(10, ("AUTH_SUCCEED=%s", errString.c_str()) );
		InitControl();
		CheckAndModifyVNCBinary();
		EndWaitCursor();
		MessageBoxEx(errString.c_str(), MB_ICONINFORMATION);
		break;
	case 0:		// AUTH_AUTHENTICATION_FAIL
		ciDEBUG(10, ("AUTH_AUTHENTICATION_FAIL=%s", errString.c_str()) );
		EndWaitCursor();
		MessageBoxEx(errString.c_str(), MB_ICONSTOP);
		break;
	case -1:	// 	AUTH_GET_MAC_FAIL
		ciDEBUG(10, ("AUTH_GET_MAC_FAIL=%s", errString.c_str()) );
		EndWaitCursor();
		MessageBoxEx(errString.c_str(), MB_ICONSTOP);
		break;
	case -2:	// 	AUTH_COMMUNICATION_FAIL
		ciDEBUG(10, ("AUTH_COMMUNICATION_FAIL=%s", errString.c_str()) );
		EndWaitCursor();
		MessageBoxEx(errString.c_str(), MB_ICONSTOP);
		break;
	case -4:	// 	AUTH_WRITE_FILE_FAIL
		ciDEBUG(10, ("AUTH_WRITE_FILE_FAIL=%s", errString.c_str()) );
		EndWaitCursor();
		MessageBoxEx(errString.c_str(), MB_ICONSTOP);
		break;
	default:	// unknown error	
		ciDEBUG(10, ("unknown error=%s", errString.c_str()) );
		EndWaitCursor();
		MessageBoxEx(errString.c_str(), MB_ICONSTOP);
		break;
	}

	ciDEBUG(10, ("OnBnClickedButtonAutoAuthenticate() end.") );
}

void CUBCHostAuthorizerGlobalDlg::OnBnClickedButtonAutoSearch()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	ciDEBUG(10, ("OnBnClickedButtonAutoSearch() begin...") );

	BeginWaitCursor();

	CServerCheck check;
	check.Init();

	CString format, text;

	int ret_val = check.GetServerIP(10000);

	EndWaitCursor();

	switch(ret_val)
	{
	default:
	case SERVERCHECK_STATUS_UNKNOWN_ERROR:			// unknown error
		format.LoadString(IDS_ERROR_INTERNAL);
		text.Format(format, "ServerCheck");
		ciDEBUG(10, ("SERVERCHECK_STATUS_UNKNOWN_ERROR=%s", (LPCTSTR)text) );
		MessageBoxEx(text, MB_ICONSTOP);
		break;

	case SERVERCHECK_STATUS_SERVER_CONN_ERROR:
		{
			CString msg;
			msg.LoadString(IDS_WARNING_SERVER_CONN_ERROR_REALLY_USE);
			msg += "=%s";
			ciWARN( (msg, check.GetNewServerIP()) );
		}
		if(MessageBoxEx(IDS_WARNING_SERVER_CONN_ERROR_REALLY_USE, MB_ICONWARNING | MB_YESNO) == IDYES)
		{
			ciWARN( ("SetAddress=%s", check.GetNewServerIP()) );
			m_ipaddrServer.SetAddress( AddrToDWORD(check.GetNewServerIP()) );
		}
		break;

	case SERVERCHECK_STATUS_CENTER_CONN_ERROR:		// server fail & center fail
		ciDEBUG(10, ("SERVERCHECK_STATUS_CENTER_CONN_ERROR") );
		MessageBoxEx(IDS_FAIL_SERVER_CHECK, MB_ICONSTOP);
		break;

	case SERVERCHECK_STATUS_NO_HOST:			// not exist host-id
		ciDEBUG(10, ("SERVERCHECK_STATUS_NO_HOST") );
		MessageBoxEx(IDS_FAIL_NOT_EXIST_ON_HOST, MB_ICONSTOP);
		break;

	case SERVERCHECK_STATUS_SERVER_OK:						// server ok
		ciDEBUG(10, ("SERVERCHECK_STATUS_SERVER_OK=%s", check.GetNewServerIP()) );
		m_ipaddrServer.SetAddress( AddrToDWORD(check.GetNewServerIP()) );
		MessageBoxEx(IDS_SUCCESS_SERVER_CHECK, MB_ICONINFORMATION);
		break;
	}

	ciDEBUG(10, ("OnBnClickedButtonAutoSearch() end.") );
}

void CUBCHostAuthorizerGlobalDlg::OnBnClickedButtonApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	ciDEBUG(10, ("OnBnClickedButtonApply() begin...") );

	BeginWaitCursor();

	DWORD ip_addr;
	m_ipaddrServer.GetAddress(ip_addr);

	ciString szIP = DWORDToAddr(ip_addr);

	ciShort nServerType = static_cast<ciShort>(m_cbxServerType.GetItemData(m_cbxServerType.GetCurSel()));

	ubcConfig aIni("UBCConnect");
	ciString oldIp;
	aIni.get("ORB_NAMESERVICE","IP", oldIp);

	ubcConfig aIniVar("UBCVariables");
	ciShort nOldServerType;
	aIniVar.get("ROOT","USE_AXIS2", nOldServerType);

	ciDEBUG(10, ("szIP=%s", szIP.c_str()) );
	ciDEBUG(10, ("oldIp=%s", oldIp.c_str()) );
	ciDEBUG(10, ("nServerType=%d", nServerType) );
	ciDEBUG(10, ("nOldServerType=%d", nOldServerType) );

	if(!dbUtil::getInstance()->changeServerIP(szIP.c_str(), ciFalse))
	{
		EndWaitCursor();

		CString msg;
		msg.LoadString(IDS_ERROR_SERVER_IP_CHANGE_FAIL);
		msg += "=%s";
		ciERROR( (msg, szIP.c_str()) );

		MessageBoxEx(IDS_ERROR_SERVER_IP_CHANGE_FAIL, MB_ICONSTOP);
		return;
	}

	// 2015. 6. 4. USE_AXIS2 설정 저장. ssb.
	aIniVar.set("ROOT","USE_AXIS2", nServerType);

	EndWaitCursor();

	if(oldIp != szIP || nOldServerType != nServerType)
	{
//		int counter = ubcIniMux::getInstance()->changeIp(oldIp.c_str(), szIP.c_str());

		ciDEBUG(10, ("change oldIp=%s", oldIp.c_str()) );
		ciDEBUG(10, ("change szIp=%s", szIP.c_str()) );
		ciDEBUG(10, ("change nServerType=%d", nServerType) );
		ciDEBUG(10, ("change nOldServerType=%d", nOldServerType) );

		m_btnRebooting.EnableWindow(TRUE);
		m_bNeedRebooting = true;

		CString fmt, buf, msg = "";
		fmt.LoadString(IDS_WARNING_SERVER_IP_CHANGE);
		buf.Format(fmt, oldIp.c_str(), szIP.c_str());
		msg += buf;
		buf.LoadString(IDS_WARNING_NEED_TO_REBOOT);
		msg += buf;
		MessageBoxEx(msg, MB_ICONWARNING);
	}
	else
	{
		CString msg;
		msg.LoadString(IDS_NOTIFY_COMPLETE_TO_APPLY_SERVER_IP);
		ciDEBUG(10, (msg) );
		MessageBoxEx(msg, MB_ICONINFORMATION);
	}

	ciDEBUG(10, ("OnBnClickedButtonApply() end.") );
}

void CUBCHostAuthorizerGlobalDlg::OnBnClickedButtonRebooting()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	ciDEBUG(10, ("OnBnClickedButtonRebooting() begin...") );

	BeginWaitCursor();

	// rebiooting
	STARTUPINFO                 sINFO = {0};
	PROCESS_INFORMATION         pINFO = {0};

	sINFO.cb = sizeof( sINFO );

	if( !CreateProcess( NULL, "rebootSystem.bat", NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &sINFO, &pINFO ) ) // CREATE_NO_WINDOW = 윈도우없이 실행
	{
		EndWaitCursor();

		CString msg;
		msg.LoadString(IDS_WARNING_CANT_REBOOTING);

		ciERROR( (msg) );
		MessageBoxEx(IDS_WARNING_CANT_REBOOTING, MB_ICONWARNING);
		return;
	}

	WaitForSingleObject( pINFO.hProcess, INFINITE );

	CloseHandle( pINFO.hProcess );
	CloseHandle( pINFO.hThread );

	EndWaitCursor();

	ciDEBUG(10, ("OnBnClickedButtonRebooting() end.") );

	CDialog::OnOK();
}

DWORD CUBCHostAuthorizerGlobalDlg::AddrToDWORD(LPCSTR lpszAddress)
{
	CString strAddr = lpszAddress;

	DWORD addr[4];
	::ZeroMemory(addr, sizeof(addr));

	int count = 0;
	int pos = 0;
	CString token = strAddr.Tokenize(".", pos);
	while(token != "" && count < 4)
	{
		addr[count] = atoi(token);

		token = strAddr.Tokenize(".", pos);
		count++;
	}

	return (addr[0] << 24) | (addr[1] << 16) | (addr[2] << 8) | (addr[3]);
}

CString	CUBCHostAuthorizerGlobalDlg::DWORDToAddr(DWORD dwAddress)
{
	LPBYTE pAddr = (LPBYTE)&dwAddress;

	char buf[128];
	sprintf(buf, "%d.%d.%d.%d", *(pAddr+3), *(pAddr+2), *(pAddr+1), *(pAddr+0));

	return buf;
}

int CUBCHostAuthorizerGlobalDlg::MessageBoxEx(LPCSTR lpszText, UINT nType)
{
	CString caption;
	GetWindowText(caption);
	return ::MessageBox(GetSafeHwnd(), lpszText, caption, nType);
}

int CUBCHostAuthorizerGlobalDlg::MessageBoxEx(DWORD nText, UINT nType)
{
	CString caption, text;
	GetWindowText(caption);
	text.LoadString(nText);
	return ::MessageBox(GetSafeHwnd(), text, caption, nType);
}

void CUBCHostAuthorizerGlobalDlg::InitControl()
{
	//
	ciString host="", mac="", edition="";
	if( scratchUtil::getInstance()->readAuthFile(host,mac,edition) )
	{
		ciDEBUG(10, ("host=%s, mac=%s, edition=%s", host.c_str(), mac.c_str(), edition.c_str()) );
		if(m_bmpBackground.GetSafeHandle()) m_strEdition = edition.c_str();
		m_bHostAuthentication = true;
	}
	else
	{
		ciDEBUG(10, ("ERROR: readAuth fail") );
		m_bHostAuthentication = false;
	}

	//
	ubcConfig aIni("UBCVariables");
	ciString host_name;
	ciLong host_type;
	aIni.get("GLOBAL","HostName", host_name);
	aIni.get("GLOBAL","HostType", host_type);
	if(host_type == 0) host_type = -1;

	//
	m_editName.SetWindowText(host_name.c_str());

	//
	if(m_cbxType.GetCount() <= 0)
	{
		m_cbxType.AddString("Gallery1");	m_cbxType.SetItemData(0, 2);
		m_cbxType.AddString("Gallery2");	m_cbxType.SetItemData(1, 7);
		m_cbxType.AddString("Beam");		m_cbxType.SetItemData(2, 3);	
		m_cbxType.AddString("PDP");			m_cbxType.SetItemData(3, 5);	
		m_cbxType.AddString("DID1");		m_cbxType.SetItemData(4, 1);	
		m_cbxType.AddString("DID2");		m_cbxType.SetItemData(5, 6);	
		m_cbxType.AddString("Etc");			m_cbxType.SetItemData(6, 99);	
	}
	switch(host_type)
	{
	default:
	case 2:		m_cbxType.SetCurSel(0); break;
	case 7:		m_cbxType.SetCurSel(1); break;
	case 3:		m_cbxType.SetCurSel(2); break;
	case 5:		m_cbxType.SetCurSel(3); break;
	case 1:		m_cbxType.SetCurSel(4); break;
	case 6:		m_cbxType.SetCurSel(5); break;
	case 99:	m_cbxType.SetCurSel(6); break;
	}

	//
	if(m_bmpBackground.GetSafeHandle())
	{
		m_bmpBackground.DeleteObject();
	}

	//
	if( m_strEdition == ENTERPRISE_EDITION )
	{
		m_stcServerSettings.EnableWindow(TRUE);
		m_stcServerIP.EnableWindow(TRUE);
		m_ipaddrServer.EnableWindow(TRUE);
		m_stcServerType.EnableWindow(TRUE);
		m_cbxServerType.EnableWindow(TRUE);
		m_btnAutoSearch.EnableWindow(TRUE);
		m_btnApply.EnableWindow(TRUE);

		//
		static bool first_init = false;
		if(!first_init)
		{
			first_init = true;
			ubcConfig aIni1("UBCConnect");
			ciString ip = "";
			aIni1.get("ORB_NAMESERVICE","IP", ip);
			m_ipaddrServer.SetAddress(AddrToDWORD(ip.c_str()));
		}

		//
		ciString customerInfo;
		ubcConfig aIni2("UBCVariables");
		aIni2.get("CUSTOMER_INFO","NAME",customerInfo);
		if( customerInfo == "NARSHA" )
		{
			m_bmpBackground.LoadBitmap(IDB_NARSHA);

			//
			CString text;
			text.LoadString(IDS_THANKS_FOR_USING_NARSHA);
			m_stcIntro.SetWindowText(text);
		}
		else
		{
			m_bmpBackground.LoadBitmap(IDB_ENTERPRISE);

			//
			CString text;
			text.LoadString(IDS_THANKS_FOR_USING_UBC_ENTERPRISE);
			m_stcIntro.SetWindowText(text);
		}

		// 2015. 6. 5. USE_AXIS2 설정 가져오기. ssb.
		m_cbxServerType.ResetContent();
		CString strServerTypeWindow, strServerTypeLinux;
		strServerTypeWindow.LoadString(IDS_SERVER_TYPE_WINDOW);
		strServerTypeLinux.LoadString(IDS_SERVER_TYPE_LINUX);
		m_cbxServerType.AddString(strServerTypeWindow);
		m_cbxServerType.SetItemData(0, 0);
		m_cbxServerType.AddString(strServerTypeLinux);
		m_cbxServerType.SetItemData(1, 1);
		ciShort nServerType = -1;
		ubcConfig aIni3("UBCVariables");
		aIni3.get("ROOT", "USE_AXIS2", nServerType);
		switch (nServerType)
		{
		case 0:
			m_cbxServerType.SetCurSel(0);
			break;
		case 1:
			m_cbxServerType.SetCurSel(1);
			break;
		default:
			m_cbxServerType.SetCurSel(0);
			break;
		}
	}
	else
	{
		m_stcServerSettings.EnableWindow(FALSE);
		m_stcServerIP.EnableWindow(FALSE);
		m_ipaddrServer.EnableWindow(FALSE);
		m_stcServerType.EnableWindow(FALSE);
		m_cbxServerType.EnableWindow(FALSE);
		m_btnAutoSearch.EnableWindow(FALSE);
		m_btnApply.EnableWindow(FALSE);

		if( m_strEdition == PLUS_EDITION )
		{
			m_bmpBackground.LoadBitmap(IDB_STANDARD_PLUS);

			//
			CString text;
			text.LoadString(IDS_THANKS_FOR_USING_UBC_STANDARD_PLUS);
			m_stcIntro.SetWindowText(text);
		}
		else
		{
			m_bmpBackground.LoadBitmap(IDB_STANDARD);

			//
			CString text;
			text.LoadString(IDS_THANKS_FOR_USING_UBC_STANDARD);
			m_stcIntro.SetWindowText(text);
		}
	}

	//
	m_btnRebooting.EnableWindow(FALSE);

	Invalidate(FALSE);
}

BOOL CUBCHostAuthorizerGlobalDlg::GetComputerName(CString& strName)
{
	DWORD size = 1024;
	char name[1024] = {0};
	BOOL ret = ::GetComputerNameEx(ComputerNamePhysicalDnsHostname, name, &size);
	strName = name;
	return ret;
}

BOOL CUBCHostAuthorizerGlobalDlg::SetComputerName(LPCSTR lpszName)
{
	return ::SetComputerNameEx(ComputerNamePhysicalDnsHostname, lpszName);
}

bool CUBCHostAuthorizerGlobalDlg::CheckAndModifyVNCBinary()
{
	ciDEBUG(10, ("CheckAndModifyVNCBinary() begin...") );

	int pos = 0;
	CString host = m_strHostId.Tokenize("-", pos);
	if(host != "") host = m_strHostId.Tokenize("-", pos);

	ciDEBUG(10, ("Edition=%s", (LPCTSTR)m_strEdition) );
	ciDEBUG(10, ("HostNo=%sd", (LPCTSTR)host) );

	if(m_strEdition == "ENT")
	{
		ciDEBUG(10, ("Enterprise") );

		if( !installUtil::getInstance()->updateVNCBinary(host) )
		{
			CString msg;
			msg.LoadString(IDS_ERROR_VNC_BINARY_UPDATE_FAIL);
			msg += "=%s";

			ciERROR( (msg, (LPCTSTR)host) );
			MessageBoxEx(IDS_ERROR_VNC_BINARY_UPDATE_FAIL, MB_ICONSTOP);
			return false;
		}
		else
		{
			if( !installUtil::getInstance()->setProperty("STARTER.APP_VNC_Proxy.AUTO_STARTUP","false",ciTrue) )
			{
				CString msg;
				msg.LoadString(IDS_ERROR_VNC_PROPERTY_UPDATE_FAIL);

				ciERROR( (msg) );
				MessageBoxEx(IDS_ERROR_VNC_PROPERTY_UPDATE_FAIL, MB_ICONSTOP);
				return false;
			}
		}
	}
	else
	{
		ciDEBUG(10, ("Not Enterprise") );

		installUtil::getInstance()->restoreVNCBinary("");

		// add vnc_proxy ip
		ciString ip;
		ubcConfig cIni("UBCConnect");
		cIni.get("VNCMANAGER","KINGYOO",ip);
		if(ip.empty())
		{
			ciDEBUG(10, ("VNCManager-KINGYOO") );
			//cIni.set("VNCMANAGER","KINGYOO","211.232.57.221");
			cIni.set("VNCMANAGER","KINGYOO","211.232.57.218");
		}

		// run vnc_proxy
		ciString value;
		ubcConfig aIni("UBCStarter");
		if(aIni.get("VNC_Proxy","AUTO_STARTUP",value))
		{
			ciDEBUG(10, ("VNC_Proxy=%s", value.c_str()) );
			if(value != "true")
			{
				ciDEBUG(10, ("AUTO_STARTUP") );
				installUtil::getInstance()->setProperty("STARTER.APP_VNC_Proxy.AUTO_STARTUP","true",ciTrue);
			}
		}

		// set +local
		ciBoolean isLocal=ciFalse;
		installUtil::getInstance()->getLocalOption(isLocal);
		ciDEBUG(10, ("getLocalOption=%d", isLocal) );
		if(!isLocal)
		{
			ciDEBUG(10, ("setLocalOption=%d", isLocal) );
			installUtil::getInstance()->setLocalOption(ciTrue,ciTrue);
		}
	}

	ciDEBUG(10, ("CheckAndModifyVNCBinary() end.") );

	return true;
}

