#pragma once

#define	_NULL				((LPCTSTR)NULL)

#define	__DEBUG__(a, b)		__log__(_T("DEBUG"), __FUNCTION__, __LINE__, a, b)
#define	__ERROR__(a, b)		__log__(_T("ERROR"), __FUNCTION__, __LINE__, a, b)
#define	__WARNING__(a, b)	__log__(_T("WARNING"), __FUNCTION__, __LINE__, a, b)
#define	__LOG__(a, b)		__log__(_T(""), __FUNCTION__, __LINE__, a, b)

#define	__DEBUG_BEGIN__(a)				__DEBUG__(_T("begin..."), a);
#define	__DEBUG_END__(a)				__DEBUG__(_T("end."), a);

#define	__DEBUG_HOST_BEGIN__(a)			__DEBUG__(_T("begin..."), a);
#define	__DEBUG_HOST_END__(a)			__DEBUG__(_T("end."), a);

#define	__DEBUG_TEMPLATE_BEGIN__(a)		__DEBUG__(_T("\tbegin..."), a);
#define	__DEBUG_TEMPLATE_END__(a)		__DEBUG__(_T("\tend."), a);

#define	__DEBUG_FRAME_BEGIN__(a)		__DEBUG__(_T("\t\tbegin..."), a);
#define	__DEBUG_FRAME_END__(a)			__DEBUG__(_T("\t\tend."), a);

#define	__DEBUG_SCHEDULE_BEGIN__(a)		__DEBUG__(_T("\t\t\tbegin..."), a);
#define	__DEBUG_SCHEDULE_END__(a)		__DEBUG__(_T("\t\t\tend."), a);

void	__LOG_INIT__(LPCTSTR lpszLogFilename=NULL, bool bEraseOldLog=true);
void	__LOG_STOP__();
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, bool bValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, int nValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, unsigned int nValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, double fValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, LONGLONG nValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, ULONGLONG nValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, LPCTSTR strValue);

void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, bool bValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, int nValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, unsigned int nValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, double fValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, LONGLONG nValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, ULONGLONG nValue);
void	__log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, LPCTSTR strValue);
