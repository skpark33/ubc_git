#pragma once
#include "afxwin.h"


// CAdminLoginDialog 대화 상자입니다.

class CAdminLoginDialog : public CDialog
{
	DECLARE_DYNAMIC(CAdminLoginDialog)

public:
	CAdminLoginDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAdminLoginDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ADMIN_LOGIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

public:
	CEdit	m_editEmail;
	CEdit	m_editPassword;

};
