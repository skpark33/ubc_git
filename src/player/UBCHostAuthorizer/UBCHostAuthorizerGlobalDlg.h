// UBCHostAuthorizerGlobalDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
//
//#include "afxwin.h"
//#include <stdlib.h>
//#include <stdio.h>
//#include <list>
//#include <fstream>
//#include "cci/libWorld/cciGUIORBThread.h"
//
//using namespace std;
//#include "afxdtctl.h"


// CUBCHostAuthorizerGlobalDlg 대화 상자
class CUBCHostAuthorizerGlobalDlg : public CDialog
{
// 생성입니다.
public:
	CUBCHostAuthorizerGlobalDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCHOSTAUTHORIZER_GLOBAL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()

	CBitmap		m_bmpBackground;
	CString		m_strEdition;

	DWORD		AddrToDWORD(LPCSTR lpszAddress);
	CString		DWORDToAddr(DWORD dwAddress);
	int			MessageBoxEx(LPCSTR lpszText, UINT nType);
	int			MessageBoxEx(DWORD nText, UINT nType);

	bool		m_bAdminLoginMode;
	bool		m_bHostAuthentication;
	bool		m_bNeedRebooting;

	void		InitControl();
	BOOL		GetComputerName(CString& strName);
	BOOL		SetComputerName(LPCSTR lpszName);
	bool		CheckAndModifyVNCBinary();

	CString		m_strHostId;

public:
	afx_msg void	OnTimer(UINT_PTR nIDEvent);

	afx_msg void	OnBnClickedButtonAuthenticate();
	afx_msg void	OnBnClickedButtonAutoSearch();
	afx_msg void	OnBnClickedButtonApply();
	afx_msg void	OnBnClickedButtonRebooting();

	CStatic			m_stcIntro;
	CEdit			m_editName;
	CComboBox		m_cbxType;
	CStatic			m_stcServerSettings;
	CStatic			m_stcServerIP;
	CIPAddressCtrl	m_ipaddrServer;
	CStatic			m_stcServerType;
	CComboBox		m_cbxServerType;
	CButton			m_btnAutoSearch;
	CButton			m_btnApply;
	CButton			m_btnRebooting;
	CButton			m_btnClose;
};
