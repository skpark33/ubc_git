#pragma once


// Message
#define		WM_TIME_SYNC_RECEIVE					(WM_USER + 1402)


class CTimeSyncSocket;

class CTimeSyncClient
{
public:
	static CTimeSyncClient*	getInstance();
	static void	clearInstance();

protected:
	static CTimeSyncClient*	_instance;

	CTimeSyncClient(void);
public:
	virtual ~CTimeSyncClient(void);

protected:
	HWND	m_hMessageReceiveWnd;

	CTimeSyncSocket*	m_pSyncSocket;
	char	m_chBroadcast;
	UINT	m_nSyncUDPPort;

	bool	DeleteSocket();

public:
	void	SetMessageReceiveWnd(HWND hWnd) { m_hMessageReceiveWnd = hWnd; };

	void	SetSyncUDPPort(UINT nPort) { m_nSyncUDPPort = nPort; };

	bool	StartSync();
	bool	StopSync();

	UINT	GetSyncPort() { return m_nSyncUDPPort; };
};
