#include "StdAfx.h"

#include "TimeSyncClient.h"
#include "TimeSyncSocket.h"

#define		DEFAULT_SYNC_UDP_PORT		(8800)		// UDP 14000 port


CTimeSyncClient* CTimeSyncClient::_instance = 0; 

CTimeSyncClient* CTimeSyncClient::getInstance()
{
	if( _instance==NULL ) _instance = new CTimeSyncClient;
	return _instance;
}

void CTimeSyncClient::clearInstance()
{
	if( _instance )
	{
		delete _instance;
		_instance = NULL;
	}
}

CTimeSyncClient::CTimeSyncClient()
{
	m_hMessageReceiveWnd = NULL;
	m_pSyncSocket = NULL;
	m_nSyncUDPPort = DEFAULT_SYNC_UDP_PORT;
}

CTimeSyncClient::~CTimeSyncClient()
{
	DeleteSocket();
}

bool CTimeSyncClient::DeleteSocket()
{
	if( m_pSyncSocket )
	{
		m_pSyncSocket->ShutDown();
		m_pSyncSocket->Close();
		delete m_pSyncSocket;
		m_pSyncSocket = NULL;
	}
	return true;
}

bool CTimeSyncClient::StartSync()
{
	DeleteSocket();

	m_pSyncSocket = new CTimeSyncSocket(m_hMessageReceiveWnd);
	if( !m_pSyncSocket->Create(m_nSyncUDPPort, SOCK_DGRAM) )
	{
		DeleteSocket();
		return false;
	}

	m_chBroadcast = 1;
	m_pSyncSocket->SetSockOpt(SO_BROADCAST, (char*)&m_chBroadcast, sizeof(m_chBroadcast));

	return true;
}

bool CTimeSyncClient::StopSync()
{
	return DeleteSocket();
}
