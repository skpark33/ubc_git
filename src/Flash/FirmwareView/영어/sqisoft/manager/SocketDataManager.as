﻿package sqisoft.manager
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	
	public class SocketDataManager extends Sprite
	{
		public function SocketDataManager()
		{
			super();
		}
		
		public static function socketDataParser( str:String ):String
		{
			var receiveArr:Array = str.split( "|" );
			
			if ( receiveArr[ 1 ].toLowerCase() == "ok" )
			{
				return receiveArr[ 0 ];
			}
			else if ( receiveArr[ 1 ].toLowerCase() == "no" )
			{
				//
			}
		}
		
		/**
		 * Stage를 반환하는 메서드 
		 * @return 
		 */		
		public static function getStage():Stage
		{
			return sm.stage;
		}
		
	}
}