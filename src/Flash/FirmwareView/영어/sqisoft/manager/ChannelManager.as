﻿package sqisoft.manager
{
	public class ChannelManager
	{
		public static function init():void
		{
			channelArr = [];
			advtsArr = [];
			
			currentChannelNum = -1;
			autoChannelNum = -1;
			selectedAdvtsNum = -1;
		}
		
		public static function valueBindingChannel( value:String ):int
		{
			var negative:int = -1;
			var len:int = channelArr.length;
			for ( var i:int=0; i<len; i++ )
			{
				if ( channelArr[ i ][ "set_ad" ] == value ) return i;
			}
			return negative;
		}
		
		public static function valueBindingAdvts( value:String ):int
		{
			var negative:int = -1;
			var len:int = advtsArr.length;
			for ( var i:int=0; i<len; i++ )
			{
				if ( advtsArr[ i ][ "name" ] == value ) return i;
			}
			return negative;
		}
		
		private static var _channelArr:Array;
		public static function get channelArr():Array
		{			
			return _channelArr;
		}
		
		public static function set channelArr( value:Array ):void
		{			
			_channelArr = value;
		}
		
		private static var _advtsArr:Array;
		public static function get advtsArr():Array
		{			
			return _advtsArr;
		}
		
		public static function set advtsArr( value:Array ):void
		{			
			_advtsArr = value;
		}
		
		private static var _recoveryArr:Array;
		public static function get recoveryArr():Array
		{			
			return _recoveryArr;
		}
		
		public static function set recoveryArr( value:Array ):void
		{			
			_recoveryArr = value;
		}
		
		private static var _currentChannelInfo:Array = [];
		public static function get currentChannelInfo():Array
		{			
			return _currentChannelInfo;
		}
		
		public static function set currentChannelInfo( value:Array ):void
		{			
			_currentChannelInfo = value;
		}
		
		private static var _holydayInfo:Array = [];
		public static function get holydayInfo():Array
		{			
			return _holydayInfo;
		}
		
		public static function set holydayInfo( value:Array ):void
		{			
			_holydayInfo = value;
		}
		
		private static var _shutdownInfo:String;
		public static function get shutdownInfo():String
		{			
			return _shutdownInfo;
		}
		
		public static function set shutdownInfo( value:String ):void
		{			
			_shutdownInfo = value;
		}
		
		private static var _displayNum:Number;
		public static function get displayNum():Number
		{			
			return _displayNum;
		}
		
		public static function set displayNum( value:Number ):void
		{			
			_displayNum = value;
		}
		
		private static var _currentChannelNum:Number;
		public static function get currentChannelNum():Number
		{			
			return _currentChannelNum;
		}
		
		public static function set currentChannelNum( value:Number ):void
		{			
			_currentChannelNum = value;
		}
		
		private static var _autoChannelNum:Number;
		public static function get autoChannelNum():Number
		{			
			return _autoChannelNum;
		}
		
		public static function set autoChannelNum( value:Number ):void
		{			
			_autoChannelNum = value;
		}
		
		private static var _selectChannelNum:Number;
		public static function get selectChannelNum():Number
		{			
			return _selectChannelNum;
		}
		
		public static function set selectChannelNum( value:Number ):void
		{			
			_selectChannelNum = value;
		}
		
		private static var _selectedAdvtsNum:Number;
		public static function get selectedAdvtsNum():Number
		{			
			return _selectedAdvtsNum;
		}
		
		public static function set selectedAdvtsNum( value:Number ):void
		{			
			_selectedAdvtsNum = value;
		}
	}
}