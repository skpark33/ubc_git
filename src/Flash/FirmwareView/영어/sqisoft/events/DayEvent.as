﻿package sqisoft.events
{
	import flash.events.Event;
	
	
	public class DayEvent extends Event
	{
		public static const DATE_SELECT:String = "dateSelect";
		
		public function DayEvent( type:String, date:int, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
			
			_date = date;
		}
		
		override public function clone():Event
		{
			return new DayEvent( type, _date, bubbles, cancelable );
		}
		
		private var _date:int;
		public function get date():int
		{
			return _date;
		}
	}
}