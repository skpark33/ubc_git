﻿package sqisoft.events
{
	import flash.events.Event;
	
	
	public class LineScrollEvent extends Event
	{
		public static const LINE_SCROLL_CHANGE:String = "lineScrollChange";
		
		public function LineScrollEvent( type:String, position:Number, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
			
			_position = position;
		}
		
		override public function clone():Event
		{
			return new LineScrollEvent( type, _position, bubbles, cancelable );
		}
		
		private var _position:Number;
		public function get position():Number
		{
			return _position;
		}
	}
}