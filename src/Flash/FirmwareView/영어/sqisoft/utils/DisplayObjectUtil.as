﻿package sqisoft.utils
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	public class DisplayObjectUtil
	{
		public static function rotateTo( displayObject:DisplayObject, tx:Number, ty:Number ):void
		{
			var dx:Number = tx - displayObject.x;
			var dy:Number = ty - displayObject.y;
			
			displayObject.rotation = Math.atan2( dy, dx ) * 180 / Math.PI;
		}
		
		private static var dictionaryDelay:Dictionary = new Dictionary( true );
		
		public static function showAfter( displayObject:DisplayObject, delay:uint ):void
		{
			dictionaryDelay[ displayObject ] = new DelayInfo( delay, 0 );
			
			displayObject.visible = false;
			displayObject.addEventListener( Event.ENTER_FRAME, onShowEnter );
		}
		
		private static function onShowEnter( evt:Event ):void
		{
			var displayObject:DisplayObject = evt.currentTarget as DisplayObject;
			
			var delayInfo:DelayInfo = dictionaryDelay[ displayObject ];
			
			if( ++delayInfo.count > delayInfo.delay )
			{
				displayObject.visible = true;
				displayObject.removeEventListener( Event.ENTER_FRAME, onShowEnter );
			}
		}
		
		private static var dictionaryInfo:Dictionary = new Dictionary();
		public static function smoothMove( displayObject:DisplayObject,
										   speed:Number,
										   targetX:Number,
										   targetY:Number ):void
		{
			dictionaryInfo[ displayObject ] = new DictionaryInfo( speed, targetX, targetY );
			displayObject.addEventListener( Event.ENTER_FRAME, onEnter );			
		}
		private static function onEnter( evt:Event ):void
		{
			var displayObject:DisplayObject = evt.currentTarget as DisplayObject;			
			var dictionaryInfo:DictionaryInfo = dictionaryInfo[ displayObject ];
			
			displayObject.x += dictionaryInfo.speed * ( dictionaryInfo.targetX - displayObject.x );
			displayObject.y += dictionaryInfo.speed * ( dictionaryInfo.targetY - displayObject.y );			
			
			if( Math.abs( displayObject.x - dictionaryInfo.targetX ) < 1 && Math.abs( displayObject.y - dictionaryInfo.targetY ) <  1 )
			{
				displayObject.x = dictionaryInfo.targetX;
				displayObject.y = dictionaryInfo.targetY;
				displayObject.removeEventListener( Event.ENTER_FRAME, onEnter );
			}
		}
	}
}

class DictionaryInfo
{
	public var speed:Number;
	public var targetX:Number;
	public var targetY:Number;
	public function DictionaryInfo( speed:Number, targetX:Number, targetY:Number )
	{
		this.speed = speed;
		this.targetX = targetX;
		this.targetY = targetY;
	}
}

class DelayInfo
{
	public var delay:Number;
	public var count:Number;
	
	public function DelayInfo( delay:Number, count:Number )
	{
		this.delay = delay;
		this.count = count;
	}
}
