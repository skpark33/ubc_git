﻿package sqisoft.controls.calendarClasses
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import sqisoft.events.DayEvent;
	
	
	[ Event( name="dateSelect", type="sqisoft.events.DayEvent" ) ];
	public class Day extends Sprite
	{
		private var dayClip:MovieClip;
		
		public function Day( skin:MovieClip=null )
		{
			super();
			
			if ( skin == null ) dayClip = new DayClip();
			else dayClip = skin;
			
			setLayout();			
			setEvent();
			defaultSetting();
		}
		
		private function setLayout():void
		{
			addChild( dayClip );
		}
		
		private function setEvent():void
		{
			if ( checkValid() == true )
			{
				dayClip.addEventListener( MouseEvent.ROLL_OVER, onClipOver );
				dayClip.addEventListener( MouseEvent.ROLL_OUT, onClipOut );
				dayClip.addEventListener( MouseEvent.CLICK, onClipClick );
			}
			else
			{
				throw new Error( "DayClip에 설정한 Label명이 틀립니다" );
			}			
		}
		
		private function defaultSetting():void
		{
			dayClip.bg.gotoAndStop( 1 );
			dayClip.txt.selectable = false;
		}
		
		private var labelArr:Array = [ "out", "over", "selected" ];
		private var frameArr:Array = [];
		
		private function checkValid():Boolean
		{
			var ary:Array = dayClip.bg.currentLabels;
			
			var len:int = labelArr.length;
			for( var i:int=0; i<len; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
				
		private var targetFrame:int = 1;
		private function onClipOver( evt:MouseEvent ):void
		{
			if( dayClip.bg.currentLabel != "selected" )
			{
				targetFrame = frameArr[ 1 ];
				dayClip.bg.addEventListener( Event.ENTER_FRAME, onEnter );
			}	
		}
		
		private function onClipOut( evt:MouseEvent ):void
		{	
			if( dayClip.bg.currentLabel != "selected" )
			{
				targetFrame = frameArr[ 0 ];
				dayClip.bg.addEventListener( Event.ENTER_FRAME, onEnter );
			}	
		}
		
		private function onClipClick( evt:MouseEvent=null ):void
		{			
			dayClip.bg.removeEventListener( Event.ENTER_FRAME, onEnter );
			dayClip.bg.gotoAndStop( frameArr[ 2 ] );
			
			dispatchEvent( new DayEvent( DayEvent.DATE_SELECT, index ) );
		}
		
		private function onEnter( evt:Event ):void
		{
			if ( dayClip.bg.currentFrame > targetFrame )
			{
				dayClip.bg.prevFrame();
			}
			else if ( dayClip.bg.currentFrame < targetFrame )
			{
				dayClip.bg.nextFrame();
			}
			else
			{
				removeEventListener( Event.ENTER_FRAME, onEnter );
			}
		}
		
		public function select():void
		{
			dayClip.bg.removeEventListener( Event.ENTER_FRAME, onEnter );
			dayClip.bg.gotoAndStop( frameArr[ 2 ] );
		}
		
		public function reset():void
		{
			dayClip.bg.gotoAndStop( frameArr[ 0 ] );
		}
		
		protected function setData():void
		{
			dayClip.txt.text = index.toString();
		}
				
		private var _dataProvider:Object;		
		public function get dataProvider():Object
		{
			return _dataProvider
		}
		
		public function set dataProvider( value:Object ):void
		{
			_dataProvider = value;	
			setData();
		}
		
		private var _index:int;
		public function get index():int
		{
			return _index;
		}
		
		public function set index( value:int ):void
		{
			_index = value;
		}
		
		public function get text():String
		{
			return dayClip.txt.text;
		}
		
		public function set text( value:String ):void
		{
			dayClip.txt.text = value;
		}
		
		public function get textColor():uint
		{
			return dayClip.txt.textColor;
		}
		public function set textColor( value:uint ):void
		{
			dayClip.txt.textColor = value;
		}
	}
}















