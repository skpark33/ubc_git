﻿package sqisoft.controls
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.text.TextFieldAutoSize;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import sqisoft.events.CustomEvent;
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class CheckBox extends Sprite
	{
		private var checkBox:MovieClip = new CheckBoxClip();
		private var txt_spaceX:uint = 2;
		private var init_label:String = "CheckBox";
		
		public function CheckBox( skin:MovieClip=null )
		{
			super();
			
			setLayout();
			setEvent();
			defaultSetting();
		}
		
		private function setLayout():void
		{
			checkBox.txt.x = checkBox.out.x + checkBox.out.width + txt_spaceX;
	    	checkBox.txt.autoSize = TextFieldAutoSize.LEFT;
			checkBox.txt.selectable = false;
			checkBox.txt.mouseEnabled = false;
			
			checkBox.txt.text = init_label;
			
			updateCheckBox();
			setCheckBoxArea();
			
			addChild( checkBox );
		}
				
		private function setEvent(): void
		{
			if( checkValid() == true )
			{
				checkBox.addEventListener( MouseEvent.CLICK, onCheckBoxClipClick );
			}
			else
			{
				throw new Error( "checkBox 설정한 Label명이 틀립니다" );
			}
		}
		
		private function defaultSetting():void
		{
			checkBox.gotoAndStop(1);
		}
		
		private var labelArr:Array = [ "deselect", "select" ];
		private var frameArr:Array = [];
		
		private function checkValid(): Boolean
		{
			var ary:Array = checkBox.currentLabels;
			
			for( var i:int=0; i<labelArr.length; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
		
		private var targetFrame:int = 1;
		private function onCheckBoxClipEnter( evt:Event ):void
		{
			if ( checkBox.currentFrame > targetFrame )
			{
				checkBox.prevFrame();
			}
			else if ( checkBox.currentFrame < targetFrame )
			{
				checkBox.nextFrame();
			}
			else
			{
				checkBox.removeEventListener( Event.ENTER_FRAME, onCheckBoxClipEnter );
			}
		}
		
		private function onCheckBoxClipClick( evt:MouseEvent ):void
		{
			//trace( "onCheckBoxClipClick" + selected );
			//trace( "onCheckBoxClipClick:::::::" + label);
			//trace( "onCheckBoxClipClick:::::::" + evt.currentTarget.currentFrameLabel);
			//trace( "onCheckBoxClipClick:::::::" + evt.currentTarget.currentLabel);
			selected = !selected;
			
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT, label ) );
			//dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT, selected ) );
		}
		
		
		public function deleteEvent(): void
		{
			checkBox.removeEventListener( MouseEvent.CLICK, onCheckBoxClipClick );
		}
		
		
		private var _selected:Boolean = false;
		public function get selected():Boolean
		{
		 	return _selected;
		}
		
		public function set selected( value:Boolean ):void
		{
			_selected = value;
			updateCheckBox();
			//dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT, selected ) );
		}
		
		private function updateCheckBox():void
		{
			targetFrame = ( selected == false ) ? frameArr[ 0 ] : frameArr[ 1 ];
			checkBox.addEventListener( Event.ENTER_FRAME, onCheckBoxClipEnter );
		}
		
		private function setCheckBoxArea():void
		{
			if ( checkBox.txt.visible == true )
				checkBox.area.width = checkBox.txt.x + checkBox.txt.textWidth + 5;
			else
				checkBox.area.width = checkBox.out.width;
			
			checkBox.area.height = checkBox.txt.textHeight + 2;
		}
		
		/*public function get currentFrameLabel():String
		{
			return checkBox.currentFrameLabel;
		}*/
		
		public function get label():String
		{
			return checkBox.txt.text;
		}
		
		public function set label( value:String ):void
		{
			checkBox.txt.text = value;
		}
		
		public function get labelColor():uint
		{
			return checkBox.txt.textColor;
		}
		
		public function set labelColor( value:uint ):void
		{
			checkBox.txt.textColor = value;
		}
		
		override public function set buttonMode( value:Boolean ):void
		{
			checkBox.area.buttonMode = value;
			
		}
		
		override public function get buttonMode():Boolean
		{
			return checkBox.area.buttonMode;
		}
	}
}