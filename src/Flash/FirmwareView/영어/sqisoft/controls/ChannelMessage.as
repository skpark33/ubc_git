﻿package sqisoft.controls
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFieldType;
	
	import sqisoft.net.FirmwareProtocol;	
	import sqisoft.controls.StepButton;
	import sqisoft.events.CustomEvent;
	import sqisoft.manager.ChannelManager;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class ChannelMessage extends Sprite
	{
		private var stepClip:StepButton;
		private var messageClip:MovieClip;
		private var parentDialog:MovieClip;
		 
		private var type:String;
				
		public function ChannelMessage( _parentDialog:MovieClip, _type:String="blank", skin:MovieClip=null )
		{
			super();
			
			if ( skin == null ) messageClip = new MessageClip();
			else messageClip = skin;
			
			parentDialog = _parentDialog;
			type = _type;
			
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}
		
		private function onStage( evt:Event ):void
		{
			setLayout();
			defaultSetting();
		}
		
		private var stepClipArr:Array;
		private function setLayout():void
		{
			stepClipArr = [];
			
			var layoutArr:Array = [ "ok_mc", "cancel_mc" ];
			for ( var i:int=0; i<layoutArr.length; i++ )
			{
				stepClip = new StepButton( messageClip[ layoutArr[ i ] ] );
				stepClip.name = layoutArr[ i ];
				
				stepClip.addEventListener( CustomEvent.CUSTOM_EVENT, onClickHandler );
				
				stepClipArr.push( stepClip );
			}
			addChild( messageClip );
		}
				
		private function defaultSetting():void
		{
			messageClip.clip.gotoAndStop( type );
			
			if ( type == "blank" || type == "channel" || type == "advts" || type == "validation" || type == "auto" || type == "current" || type == "registered" || type == "empty" || type == "service" )
			{
				messageClip.cancel_mc.visible = false;
				
				messageClip.ok_mc.x = ( messageClip.width - messageClip.ok_mc.width ) / 2;
			}
		}
		
		private function onClickHandler( evt:CustomEvent ):void
		{
			switch ( evt.currentTarget.name )
			{
				case "ok_mc":
					sendMessageInfo();
					break;
				case "cancel_mc":
					parentDialog.closeMessagePopup();
					break;
				default:
					trace( "nothing" );
			}
		}
				
		private function sendMessageInfo():void
		{
			if ( type == "reboot" )
			{
				parentDialog.socket.send( FirmwareProtocol.FV_REBOOT );
			}
			else if ( type == "shutdown" )
			{
				parentDialog.socket.send( FirmwareProtocol.FV_SHUTDOWN );
			}
			else if ( type == "delete" )
			{
				parentDialog.socket.send( FirmwareProtocol.FV_CH_DELETE + "|" + queryDelete() );
			}
			else if ( type == "deleteall" )
			{
				parentDialog.socket.send( FirmwareProtocol.FV_DELETE_ALL );
			}
			parentDialog.closeMessagePopup();
		}
		
		private function queryDelete():String
		{
			var query:String = "";
			query += "ChannelName=" + ChannelManager.advtsArr[ ChannelManager.selectedAdvtsNum ].name;
			
			return query;
		}
	}
}