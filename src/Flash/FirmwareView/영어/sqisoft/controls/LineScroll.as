﻿package sqisoft.controls
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.utils.Timer;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	
	import sqisoft.events.LineScrollEvent;
	
	[ Event( name="lineScrollChange", type="sqisoft.events.LineScrollEvent" ) ]
	public class LineScroll extends Sprite
	{
		private var scrollClip:MovieClip;
		
		private var scrollTimer:Timer = new Timer( 40, 0 );
		private var offSetY:Number = 0;
		
		private var minScrollY:Number;
		private var maxScrollY:Number;
		
		public var minTargetValue:Number;
		public var maxTargetValue:Number;
		
		private var _cursorType:String;
		
		public function LineScroll( skin:MovieClip=null, cursorType:String="rect" )
		{
			super();
			
			if ( skin == null ) scrollClip = new LineScrollClip();
			else scrollClip = skin;
			_cursorType = cursorType;
						
			setLayout();
			setEvent();
			defaultSetting();
		}
		
		private function setLayout(): void
		{
			scrollClip.track_mc.addChild( scrollClip.bg_mc );
			scrollClip.track_mc.addChild( scrollClip.cursor_mc );
			
			if ( !contains( scrollClip ) )
				addChild( scrollClip );
			
			scrollClip.bg_mc.y = 0;
			scrollClip.cursor_mc.y = 0;
		}
		
		private function setEvent(): void
		{
			scrollClip.cursor_mc.addEventListener( MouseEvent.MOUSE_DOWN, onCursorMouseDown );
			scrollClip.track_mc.addEventListener( MouseEvent.MOUSE_DOWN, onTrackMouseDown );
			
			scrollTimer.addEventListener( TimerEvent.TIMER, onScrollTime );
		}
		
		private function defaultSetting():void
		{
			scrollClip.cursor_mc.buttonMode = true;
			scrollClip.track_mc.buttonMode = true;
			
			minTargetValue = 0;
			maxTargetValue = 100;
		
			minScrollY = 0;
			
			if ( _cursorType == "dot" )
			{
				maxScrollY = scrollClip.track_mc.height;
			}
			else
			{
				maxScrollY = scrollClip.track_mc.height - scrollClip.cursor_mc.height;
			}
		}
		
		private function onCursorMouseDown( evt:MouseEvent ):void
		{
			scrollTimer.start();
			
			offSetY = scrollClip.cursor_mc.mouseY;
			
			stage.addEventListener( MouseEvent.MOUSE_UP, onStageMouseUp );
		}
		
		private function onStageMouseUp( evt:MouseEvent ):void
		{
			scrollTimer.stop();
		}
				
		private function onScrollTime( evt:TimerEvent ):void
		{
			scrollClip.cursor_mc.y = scrollClip.track_mc.mouseY - offSetY;
			
			checkBound();
		}
		
		private function checkBound():void
		{
			if ( scrollClip.cursor_mc.y < minScrollY ) scrollClip.cursor_mc.y = minScrollY;
			if ( scrollClip.cursor_mc.y > maxScrollY ) scrollClip.cursor_mc.y = maxScrollY;
			
			dispatchEvent( new LineScrollEvent( LineScrollEvent.LINE_SCROLL_CHANGE, position ) );
		}
		
		private function onTrackMouseDown( evt:MouseEvent ):void
		{
			var clickPosition:Number = scrollClip.track_mc.mouseY - scrollClip.cursor_mc.height;
			scrollClip.cursor_mc.y = (clickPosition < 0) ? 0 : clickPosition;
			
			dispatchEvent( new LineScrollEvent( LineScrollEvent.LINE_SCROLL_CHANGE, position ) );
		}
		
		private var _useWheelMouse:Boolean = false;
		public function get useWheelMouse():Boolean
		{
			return _useWheelMouse;
		}
		
		public function set useWheelMouse( value:Boolean ):void
		{
			_useWheelMouse = value;
			
			if ( _useWheelMouse == true )
			{
				scrollClip.mouseChildren = true;
				scrollClip.mouseEnabled = true;
				scrollClip.alpha = 1;
			}
			else
			{
				scrollClip.mouseChildren = false;
				scrollClip.mouseEnabled = false;
				scrollClip.alpha = 0.5;
			}
		}
				
		private var _position:Number;
		public function get position():Number
		{
			_position = ( maxTargetValue - minTargetValue ) / ( maxScrollY - minScrollY ) * ( scrollClip.cursor_mc.y - minScrollY ) + minTargetValue;
			return _position;
		}
		
		public function set position( value:Number ):void
		{
			_position = ( maxScrollY - minScrollY ) / ( maxTargetValue - minTargetValue ) * ( value - minTargetValue ) + minScrollY;
			scrollClip.cursor_mc.y = _position;
			
			checkBound();
		}
	}
}
