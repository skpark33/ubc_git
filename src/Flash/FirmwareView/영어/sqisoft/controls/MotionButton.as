﻿package sqisoft.controls
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import sqisoft.events.CustomEvent;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class MotionButton extends Sprite
	{
		protected var btnItem:MovieClip;
		
		public function MotionButton( btnClip:MovieClip )
		{
			super();
			
			btnItem = btnClip;
			
			setEvent();
			defaultSetting();
		}
				
		private function setEvent(): void
		{
			if( checkValid() == true )
			{
				btnItem.addEventListener( MouseEvent.ROLL_OUT, onBtnItemOut );
				btnItem.addEventListener( MouseEvent.ROLL_OVER, onBtnItemOver );
				btnItem.addEventListener( MouseEvent.CLICK, onBtnItemClick );
			}
			else
			{
				throw new Error( "btnItem에 설정한 Label명이 틀립니다" );
			}
		}
		
		private function defaultSetting():void
		{
			btnItem.gotoAndStop(1);
		}
		
		public function setFrame( frame:Number ):void
		{
			for( var item:Object in btnItem )
			{
				btnItem[ item ].gotoAndStop( frame );
			}
		}
		
		private var labelArr:Array = [ "out", "over", "selected", "disabled" ];
		private var frameArr:Array = [];
		
		private function checkValid(): Boolean
		{
			var ary:Array = btnItem.currentLabels;
			
			for( var i:int=0; i<labelArr.length; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
		
		private var targetFrame:int = 1;
		private function onBtnItemOut( evt:MouseEvent ):void
		{
			if( btnItem.currentLabel != "selected" || btnItem.currentLabel != "disabled" )
			{
				targetFrame = frameArr[ 0 ];
				btnItem.addEventListener( Event.ENTER_FRAME, onBtnItemEnter );
			}
		}
		
		private function onBtnItemOver( evt:MouseEvent ):void
		{	
			if( btnItem.currentLabel != "selected" || btnItem.currentLabel != "disabled" )
			{
				targetFrame = frameArr[ 1 ];
				btnItem.addEventListener( Event.ENTER_FRAME, onBtnItemEnter );
			}
		}
		
		private function onBtnItemEnter( evt:Event ):void
		{
			if ( btnItem.currentFrame > targetFrame )
			{
				btnItem.prevFrame();
			}
			else if ( btnItem.currentFrame < targetFrame )
			{
				btnItem.nextFrame();
			}
			else
			{
				btnItem.removeEventListener( Event.ENTER_FRAME, onBtnItemEnter );
			}
		}
		
		private function onBtnItemClick( evt:MouseEvent ):void
		{
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}
		
		private var _no:Number = 0;
		public function get no():Number
		{
			return _no;
		}
		
		public function set no( value:Number ):void
		{
			_no = no;
		}
		
		public function get label():String
		{
			return btnItem.txt.text;
		}
		
		public function set label( value:String ):void
		{
			btnItem.txt.text = value;
		}
		
		public function get enabled():Boolean
		{
			return btnItem.hasEventListener( MouseEvent.CLICK );
		}
		
		public function set enabled( value:Boolean ):void
		{
			if ( value )
			{
				btnItem.gotoAndStop( frameArr[ 0 ] );
				
				btnItem.addEventListener( MouseEvent.ROLL_OUT, onBtnItemOut );
				btnItem.addEventListener( MouseEvent.ROLL_OVER, onBtnItemOver );
				btnItem.addEventListener( MouseEvent.CLICK, onBtnItemClick );
			}
			else
			{
				btnItem.gotoAndStop( frameArr[ 3 ] );
				
				btnItem.removeEventListener( MouseEvent.ROLL_OUT, onBtnItemOut );
				btnItem.removeEventListener( MouseEvent.ROLL_OVER, onBtnItemOver );
				btnItem.removeEventListener( MouseEvent.CLICK, onBtnItemClick );
			}
		}
	}
}