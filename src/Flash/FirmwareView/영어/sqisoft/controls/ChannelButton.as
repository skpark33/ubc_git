﻿package sqisoft.controls
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import sqisoft.events.CustomEvent;
	import sqisoft.manager.ChannelManager;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class ChannelButton extends MovieClip
	{
		private var channelClip:MovieClip;
		
		public function ChannelButton( skin:MovieClip=null )
		{
			super();
			
			if ( skin == null ) channelClip = new ChannelClip();
			else channelClip = skin;
			
			setLayout();
			setEvent();
			defaultSetting();
		}
		
		private function setLayout():void
		{
			addChild( channelClip );
		}
				
		private function setEvent(): void
		{
			if( checkValid() == true )
			{
				channelClip.addEventListener( MouseEvent.CLICK, onChannelClick );
				updateItemStatus();
			}
			else
			{
				throw new Error( "btnItem에 설정한 Label명이 틀립니다" );
			}
		}
		
		private function defaultSetting():void
		{
			channelClip.gotoAndStop(1);
			channelClip.icon_mc.gotoAndStop(1);
			channelClip.bg.gotoAndStop(1);
			channelClip.ch_txt.gotoAndStop(1);
			channelClip.ch_bg.gotoAndStop(1);
			channelClip.ch_bg._img.gotoAndStop(1);
		}
		
		private var labelArr:Array = [ "deselect", "select" ];
		private var frameArr:Array = [];
		
		private function checkValid(): Boolean
		{
			var ary:Array = channelClip.currentLabels;
			
			for( var i:int=0; i<labelArr.length; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
		
		private var targetFrame:int = 1;
		private function onChannelEnter( evt:Event ):void
		{
			if ( channelClip.currentFrame > targetFrame )
			{
				channelClip.prevFrame();
			}
			else if ( channelClip.currentFrame < targetFrame )
			{
				channelClip.nextFrame();
			}
			else
			{
				channelClip.removeEventListener( Event.ENTER_FRAME, onChannelEnter );
			}
		}
		
		private function onChannelClick( evt:MouseEvent ):void
		{
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}
		
		public function selectedItem():void
		{
			targetFrame = frameArr[ 1 ];
			channelClip.removeEventListener( MouseEvent.CLICK, onChannelClick );
			channelClip.addEventListener( Event.ENTER_FRAME, onChannelEnter );
		}
		
		public function deselectedItem():void
		{
			targetFrame = frameArr[ 0 ];
			channelClip.addEventListener( MouseEvent.CLICK, onChannelClick );
			channelClip.addEventListener( Event.ENTER_FRAME, onChannelEnter );
		}
		
		public function updateItemStatus():void
		{
			channelClip.addEventListener( Event.ENTER_FRAME, onAutoChannel );
			channelClip.addEventListener( Event.ENTER_FRAME, onCurrentChannel );
		}
		
		private function onAutoChannel( evt:Event ):void
		{
			if ( ChannelManager.autoChannelNum == no )
			{
				channelClip.icon_mc.nextFrame();
			}
			else
			{
				channelClip.icon_mc.prevFrame();
			}
			
			if ( channelClip.icon_mc.currentFrame == 1 || channelClip.icon_mc.currentFrame == channelClip.icon_mc.totalFrames )
			{
				channelClip.removeEventListener( Event.ENTER_FRAME, onAutoChannel );
			}
		}
		
		private function onCurrentChannel( evt:Event ):void
		{
			if ( ChannelManager.currentChannelNum == no )
			{
				channelClip.ch_bg.nextFrame();
			}
			else
			{
				channelClip.ch_bg.prevFrame();
			}
			
			if ( channelClip.ch_bg.currentFrame == 1 || channelClip.ch_bg.currentFrame == channelClip.ch_bg.totalFrames )
			{
				channelClip.removeEventListener( Event.ENTER_FRAME, onCurrentChannel );
			}
		}
		
		private var _bindNum:Number;
		public function get bindNum():Number
		{
			return _bindNum;
		}
		
		public function set bindNum( value:Number ):void
		{
			_bindNum = value;
		}
		
		private var _no:Number;
		public function get no():Number
		{
			return _no;
		}
		
		public function set no( value:Number ):void
		{
			_no = value;
		}
		
		public function settingChannel( advtsArr:Array ):void
		{
			if ( bindNum < 0 )
			{
				channelClip.bg.gotoAndStop("none");
				channelClip.ch_bg._img.gotoAndStop("none");
				
				channelName = "NONE";
				channelDesc = "";
				sitename = "";
				networkuse = "";
			}
			else
			{
				channelClip.bg.gotoAndStop( no );
				channelClip.ch_bg._img.gotoAndStop( no );
				
				channelName = advtsArr[ bindNum ].name;
				channelDesc = advtsArr[ bindNum ].desc;
				sitename = advtsArr[ bindNum ].sitename;
				networkuse = advtsArr[ bindNum ].networkuse;
			}
			channelClip.ch_txt.gotoAndStop( no );
		}
		
		public function selectedChannelModify():String
		{
			var query:String = "";
			query += "Ch=ch" + ChannelManager.selectChannelNum + ","
			query += "ChannelName=" + channelName + ","
			query += "SiteName=" + sitename + ","
			query += "NetworkUse=" + networkuse + ","
			query += "Display=" + ChannelManager.displayNum;
			
			return query;
		}
		
		public function get channelName():String
		{
			var returnName:String = channelClip.channelName_txt.text;
			if ( returnName == "NONE" ) returnName = "";
			return returnName;
		}
		
		public function set channelName( value:String ):void
		{
			channelClip.channelName_txt.text = value;
		}
		
		public function get channelDesc():String
		{
			return channelClip.channelDesc_txt.text;
		}
		
		public function set channelDesc( value:String ):void
		{
			channelClip.channelDesc_txt.text = value;
		}
		
		private var _sitename:String;
		public function get sitename():String
		{
			return _sitename;
		}
		
		public function set sitename( value:String ):void
		{
			_sitename = value;
		}
		
		private var _networkuse:String;
		public function get networkuse():String
		{
			return _networkuse;
		}
		
		public function set networkuse( value:String ):void
		{
			_networkuse = value;
		}
		
		public function get enable():Boolean
		{
			return channelClip.hasEventListener( MouseEvent.CLICK );
		}
		
		public function set enable( value:Boolean ):void
		{
			if ( value )
			{
				channelClip.gotoAndStop( frameArr[ 0 ] );
				channelClip.addEventListener( MouseEvent.CLICK, onChannelClick );
			}
			else
			{
				channelClip.gotoAndStop( frameArr[ 3 ] );
				channelClip.removeEventListener( MouseEvent.CLICK, onChannelClick );
			}
		}
	}
}