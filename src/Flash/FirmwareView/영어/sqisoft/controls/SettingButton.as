﻿package sqisoft.controls
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.FocusEvent;
	
	import fl.managers.FocusManager;


	
	import sqisoft.controls.StepButton;
	import sqisoft.events.CustomEvent;
	import sqisoft.utils.StringUtil;
	
	import sqisoft.manager.ChannelManager;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class SettingButton extends Sprite
	{
		private var settingClip:MovieClip;
		private var applyClip:StepButton;
		private var clearClip:StepButton;
		private var upClip:MovieClip;
		private var downClip:MovieClip;
		private var str:String;
	
		private var focusManager:FocusManager;
				
		public function SettingButton( skin:MovieClip=null )
		{
			super();
			
			settingClip = skin;
			
			setEvent();
			defaultSetting();
		}
		
		private function setLayout():void
		{
			if ( !contains( settingClip ) )
				addChild( settingClip );
		}
				
		public function setEvent(): void
		{
			if( checkValid() == true )
			{
				if ( settingClip.name == "display"||  settingClip.name == "mouse" )
				{
					for( var item:Object in settingClip )
					{
						if ( item == "FULL" || item == "SRC" || item == "Show" || item == "hide" )
							settingClip[ item ].addEventListener( MouseEvent.CLICK, onRadioClick );
					}
				}
				else if ( settingClip.name == "update" )
				{	
					for(var _item:Object in settingClip)
					{
						if( _item == "Auto" || _item == "Off" )
							settingClip[ _item ].addEventListener( MouseEvent.CLICK, onRadioClick );
						else if( _item == "txt" )
						{
							settingClip[ _item ].addEventListener( MouseEvent.MOUSE_DOWN, onSettingClipDown );
							settingClip[ _item ].addEventListener( MouseEvent.MOUSE_UP, onSettingClipUp );
							settingClip[ _item ].addEventListener( MouseEvent.ROLL_OUT, onSettingClipOut );
							settingClip[ _item ].addEventListener( MouseEvent.ROLL_OVER, onSettingClipOver );
						}
					}
				}
				else if ( settingClip.name == "shutdown" )
				{
					settingClip.txt.addEventListener( MouseEvent.MOUSE_DOWN, onSettingClipDown );
					settingClip.txt.addEventListener( MouseEvent.MOUSE_UP, onSettingClipUp );
					//settingClip.addEventListener( MouseEvent.ROLL_OUT, onSettingClipOut );
					//settingClip.addEventListener( MouseEvent.ROLL_OVER, onSettingClipOver );
					
					applyClip = new StepButton( settingClip.apply_mc );
					applyClip.addEventListener( CustomEvent.CUSTOM_EVENT, onApplyClick );
					
					clearClip = new StepButton( settingClip.clear_mc );
					clearClip.addEventListener( CustomEvent.CUSTOM_EVENT, onClearClick );
					
					settingClip.up_mc.bg.gotoAndStop( 1 );
					settingClip.up_mc.addEventListener( MouseEvent.MOUSE_OUT, onUpOut );
					settingClip.up_mc.addEventListener( MouseEvent.MOUSE_OVER, onUpOver );
					settingClip.up_mc.addEventListener( MouseEvent.MOUSE_UP, onUpUp);
					settingClip.up_mc.addEventListener( MouseEvent.MOUSE_DOWN, onUpDown);
					settingClip.up_mc.addEventListener( MouseEvent.CLICK, onUpClick );
					
					settingClip.down_mc.bg.gotoAndStop( 1 );
					settingClip.down_mc.addEventListener( MouseEvent.MOUSE_OUT, onDownOut );
					settingClip.down_mc.addEventListener( MouseEvent.MOUSE_OVER, onDownOver );
					settingClip.down_mc.addEventListener( MouseEvent.MOUSE_UP, onDownUp);
					settingClip.down_mc.addEventListener( MouseEvent.MOUSE_DOWN, onDownDown);
					settingClip.down_mc.addEventListener( MouseEvent.CLICK, onDownClick );
					
					settingClip.hour_txt.addEventListener( FocusEvent.FOCUS_OUT, onCheckTextField );
					settingClip.hour_txt.addEventListener( FocusEvent.FOCUS_IN, onHourFocusIn );
					settingClip.min_txt.addEventListener( FocusEvent.FOCUS_OUT, onCheckTextField );
					settingClip.min_txt.addEventListener( FocusEvent.FOCUS_IN, onMinFocusIn );
				}
				else
				{
					//settingClip.addEventListener( MouseEvent.CLICK, onSettingClipClick );
					settingClip.addEventListener( MouseEvent.MOUSE_DOWN, onSettingClipDown );
					settingClip.addEventListener( MouseEvent.MOUSE_UP, onSettingClipUp );
				}
				settingClip.addEventListener( MouseEvent.ROLL_OUT, onSettingClipOut );
				settingClip.addEventListener( MouseEvent.ROLL_OVER, onSettingClipOver );
			}
			else
			{
				throw new Error( "settingClip에 설정한 Label명이 틀립니다" );
			}
		}
		
		private function defaultSetting():void
		{
			
			for( var item:Object in settingClip )
			{
			
				if ( settingClip[ item ] is MovieClip )
				{
					//trace( "item : " + item );
					if ( item == "icon_mc" || item == "original_image" )
					{
						//trace("if");
						settingClip[ item ].gotoAndStop( settingClip.name );
					}
					else
					{
						
						settingClip[ item ].gotoAndStop( 1 );
					}
				}
			}
			settingClip.gotoAndStop( 1 );
			settingClip.txt.gotoAndStop( settingClip.name );
		}
		
		public function setFrame( frame:Number ):void
		{
			for( var item:Object in settingClip )
			{
				if ( item == "FULL" || item == "SRC" || item == "Auto" || item == "Off" || item == "Show" || item == "hide" ) return;
				
				if ( settingClip[ item ] is MovieClip )
					settingClip[ item ].gotoAndStop( frame );
			}
		}
		
		private var labelArr:Array = [ "out", "over" ];
		private var frameArr:Array = [];
		
		private function checkValid(): Boolean
		{
			var ary:Array = settingClip.currentLabels;
			
			for( var i:int=0; i<labelArr.length; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
		
		private var targetFrame:int = 1;
		private function onSettingClipOut( evt:MouseEvent ):void
		{
			targetFrame = frameArr[ 0 ];
			settingClip.addEventListener( Event.ENTER_FRAME, onSettingClipEnter );
		}
		
		private function onSettingClipOver( evt:MouseEvent ):void
		{	
			targetFrame = frameArr[ 1 ];
			settingClip.addEventListener( Event.ENTER_FRAME, onSettingClipEnter );
		}
		
		private function onSettingClipEnter( evt:Event ):void
		{
			if ( settingClip.currentFrame > targetFrame )
			{
				settingClip.prevFrame();
			}
			else if ( settingClip.currentFrame < targetFrame )
			{
				settingClip.nextFrame();
			}
			else
			{
				settingClip.removeEventListener( Event.ENTER_FRAME, onSettingClipEnter );
			}
		}
		
		private function onSettingClipDown( evt:MouseEvent ):void
		{
			//trace( "onSettingClipDown:: " +  evt.currentTarget.name );
			var str = settingClip.name + "_down";
			settingClip.gotoAndStop( "out" );
			settingClip.txt.gotoAndStop( str );
		}
		
		private function onSettingClipUp( evt:MouseEvent ):void
		{
			
			//trace( "onSettingClipUp:: " +  evt.currentTarget.name );
			if( evt.currentTarget.name == "txt" )
			{
				dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
				var str_ = settingClip.name;
				settingClip.txt.gotoAndStop( str_ );
			}
			
			else
			{
				dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
				var str = settingClip.name;
				settingClip.txt.gotoAndStop( str );
			}
		}
				
		/*private function onSettingClipClick( evt:MouseEvent ):void
		{
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}*/
		
		public function onRadioDeselect( evt:Event ):void
		{
			var target:MovieClip = evt.target as MovieClip;
			
			target.prevFrame();
			if ( target.currentFrame == 1 )
			{
				target.mouseChildren = true;
				target.mouseEnabled = true;
				target.removeEventListener( Event.ENTER_FRAME, onRadioDeselect );
			}
		}
		
		public function onRadioSelect( evt:Event ):void
		{
			var target:MovieClip = evt.target as MovieClip;
			
			target.nextFrame();
			if ( target.currentFrame == target.totalFrames )
			{
				target.mouseChildren = false;
				target.mouseEnabled = false;
				target.removeEventListener( Event.ENTER_FRAME, onRadioSelect );
			}
		}
		
		private var selectedDisplayRadio:MovieClip;
		private function onRadioClick( evt:MouseEvent ):void
		{
			if ( selectedDisplayRadio != null )
				selectedDisplayRadio.addEventListener( Event.ENTER_FRAME, onRadioDeselect );
			selectedDisplayRadio = evt.target as MovieClip;
			selectedDisplayRadio.addEventListener( Event.ENTER_FRAME, onRadioSelect);
			
			var arg:Object = displayRadioQuery( evt.target.name );
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT, arg ) );
		}
		
		private function displayRadioQuery( value:String ):String
		{
			var query:String;
			if ( value == "FULL" || value == "SRC" )
				query = "OPT=" + value;
			else if ( value == "Show" || value == "hide" )
				query = "Show=" + ( ( value == "hide" ) ? "0" : "1" );
			else if( value == "Auto" || value == "Off")
			{
				query = "Auto=" + ( ( value == "Off" ) ? "0" : "1" );
			}
			return query;
		}
		
		private function onCheckTextField( evt:FocusEvent=null ):void
		{
			if ( settingClip.hour_txt.text != "" )
			{
				var hour:Number = Number( StringUtil.digit( Number( settingClip.hour_txt.text ) ) );
			
				if (hour > 23)
				{
					settingClip.hour_txt.text = StringUtil.digit( 0 );
				}
				else
				{
					settingClip.hour_txt.text = StringUtil.digit( hour );
				}
			}
			
			if ( settingClip.min_txt.text != "" )
			{	
				var min:Number = Number( StringUtil.digit( Number( settingClip.min_txt.text ) ) );
			
				if (min > 59)
				{
					settingClip.min_txt.text = StringUtil.digit( 0 );
				}
				else
				{
					settingClip.min_txt.text = StringUtil.digit( min );
				}
			}
		}
		
		private function onHourFocusIn( evt:FocusEvent ):void
		{
			str = "hour_FocusIn";
			focusManager = new FocusManager ( settingClip.up_mc );
			focusManager.setFocus( settingClip.hour_txt );
		}
		private function onMinFocusIn( evt:FocusEvent ):void
		{
			str = "min_FocusIn";
			focusManager = new FocusManager ( settingClip.up_mc );
			focusManager.setFocus( settingClip.min_txt );
		}
		
		private function onApplyClick( evt:CustomEvent ):void
		{
			//trace( "onApplyClick:::::" + evt.currentTarget.name );

			focusManager = new FocusManager( settingClip.apply_mc );
			focusManager.setFocus( null );
			
			var arg:Object = {};
			
			if ( ( settingClip.hour_txt.text != "" && settingClip.min_txt.text == "" ) )
			{
				settingClip.min_txt.text = "00";
				onCheckTextField();
			}
			else if( ( settingClip.hour_txt.text == "" && settingClip.min_txt.text != "" ) )
			{
				
				settingClip.hour_txt.text = "00";
				onCheckTextField();
			}
			onCheckTextField();
			arg = shutdownQuery();
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT, arg ) );
		}
		
		private function onClearClick( evt:CustomEvent ):void
		{
			trace( "onClearClick:::::" + evt.currentTarget.name );
			var arg:Object = {};

			settingClip.hour_txt.text = "";
			settingClip.min_txt.text = "";
						
			arg = shutdownQuery();
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT, arg ) );
		}
		
		private function onUpOut ( evt:MouseEvent ):void
		{
			settingClip.up_mc.bg.gotoAndStop( "out" );
		}
		
		private function onUpOver ( evt:MouseEvent ):void
		{
			settingClip.up_mc.bg.gotoAndStop( "over" );
		}
		
		private function onUpUp ( evt:MouseEvent ):void
		{
			settingClip.up_mc.bg.gotoAndStop( "over" );
		}
		
		private function onUpDown ( evt :MouseEvent ):void
		{
			settingClip.up_mc.bg.gotoAndStop( "selected" );
		}
		
		private function onUpClick ( evt:MouseEvent ):void
		{
			switch ( str ) 
			{
				case "hour_FocusIn" :
				if ( Number ( settingClip.hour_txt.text ) > 00 && Number ( settingClip.hour_txt.text ) < 23 ) 
				{
					settingClip.hour_txt.text = String( Number ( settingClip.hour_txt.text ) + 1 );
				}
				else if ( Number( settingClip.hour_txt.text == "00" || settingClip.hour_txt.text == "" ) ) 
				{
					settingClip.hour_txt.text="1";
				}
				else if ( Number( settingClip.hour_txt.text == "23" ) ) 
				{
					settingClip.hour_txt.text="00";
				}
				break;
			
			case "min_FocusIn" :
				if ( Number ( settingClip.min_txt.text ) >= 0 && Number ( settingClip.min_txt.text ) < 59 ) 
				{
					settingClip.min_txt.text = String ( Number ( settingClip.min_txt.text ) +1 );
				}
				else if ( Number ( settingClip.min_txt.text == "59" ) ) 
				{
					settingClip.min_txt.text="00";
				}
				break;
			}
		}
		
		private function onDownOut ( evt:MouseEvent ):void
		{
			settingClip.down_mc.bg.gotoAndStop( "out" );
		}
		
		private function onDownOver ( evt:MouseEvent ):void
		{
			settingClip.down_mc.bg.gotoAndStop( "over" );
		}
		
		private function onDownUp ( evt:MouseEvent ):void
		{
			settingClip.down_mc.bg.gotoAndStop( "over" );
		}
		
		private function onDownDown ( evt :MouseEvent ):void
		{
			settingClip.down_mc.bg.gotoAndStop( "selected" );
		}
		
		private function onDownClick ( evt:MouseEvent ):void
		{
			switch ( str ) 
			{
				case "hour_FocusIn" :
				if ( Number ( settingClip.hour_txt.text ) > 1 && Number ( settingClip.hour_txt.text ) < 25 )
				{
					settingClip.hour_txt.text = String ( Number ( settingClip.hour_txt.text ) -1 );
				}
			 	else if ( Number ( settingClip.hour_txt.text == "00" || settingClip.hour_txt.text == "" ) ) 
				{
					settingClip.hour_txt.text = "23";
				}
			 	else if ( Number ( settingClip.hour_txt.text == "01" || settingClip.hour_txt.text == "1" ) ) 
				{
					settingClip.hour_txt.text = "00";
				}
				break;
			
			case "min_FocusIn" :
				if ( Number ( settingClip.min_txt.text ) > 1 && Number ( settingClip.min_txt.text ) < 60 ) 
				{
					settingClip.min_txt.text = String ( Number ( settingClip.min_txt.text ) -1 );
				}
				 else if ( Number ( settingClip.min_txt.text == "01" || settingClip.min_txt.text == "1"  ) ) 
				 {
					settingClip.min_txt.text = "00" ;
				 }
				else if ( Number ( settingClip.min_txt.text == "00" || settingClip.min_txt.text == "" ) ) 
				{
					settingClip.min_txt.text="59";
				}
				break;
			}
		}
		
		private function shutdownQuery():String
		{
			var query:String;
			if ( settingClip.hour_txt.text == "" && settingClip.min_txt.text == "" )
				query = "ShutdownTime=NOSET" + "^" + ChannelManager.holydayInfo.slice(1).join("^");
			else
				query = "ShutdownTime=" + ( settingClip.hour_txt.text + ":" + settingClip.min_txt.text ) + "^" + ChannelManager.holydayInfo.slice(1).join("^");
			
			trace( "쿼리!!!!"+ query );
			
			return query;
			
		}
		
		public function currentStatusInfo( value:String ):void		
		{
			//trace("커런트스테이터스인포" + value);
			if ( value.indexOf(":") > 0 || value == "NOSET" )
			{
				var hour:String;
				var min:String;
				
				if ( value == "" ||  value == "NOSET" )
				{
					settingClip.hour_txt.text = "";
					settingClip.min_txt.text = "";
				}
				else
				{
					var shutdownTimeInfo:Array = value.split(":");
					
					hour = shutdownTimeInfo[ 0 ];
					min = shutdownTimeInfo[ 1 ];
					
					//trace( "hour" + hour + "min" + min  );
					
					settingClip.hour_txt.text = hour;
					settingClip.min_txt.text = min;
				}				
			}
			else
			{
				value = ( value == "0" || value == "1" ) ? ( value == "0" ) ? "hide" : "Show" : value;
				settingClip[ value ].dispatchEvent( new MouseEvent( MouseEvent.CLICK ) );
			}
		}
		
		public function deleteEventUpdate():void
		{
			trace("deleteEventUpdate");
			settingClip.Auto.removeEventListener( MouseEvent.CLICK, onRadioClick );
			settingClip.Off.removeEventListener( MouseEvent.CLICK, onRadioClick );
		}
		public function deleteEventShutdown():void
		{
//			settingClip.txt.removeEventListener( MouseEvent.MOUSE_DOWN, onSettingClipDown );
//			settingClip.txt.removeEventListener( MouseEvent.MOUSE_UP, onSettingClipUp );
			
			applyClip.removeEventListener( CustomEvent.CUSTOM_EVENT, onApplyClick );
			
			clearClip.removeEventListener( CustomEvent.CUSTOM_EVENT, onClearClick );
			
			settingClip.up_mc.removeEventListener( MouseEvent.MOUSE_OUT, onUpOut );
			settingClip.up_mc.removeEventListener( MouseEvent.MOUSE_OVER, onUpOver );
			settingClip.up_mc.removeEventListener( MouseEvent.MOUSE_UP, onUpUp);
			settingClip.up_mc.removeEventListener( MouseEvent.MOUSE_DOWN, onUpDown);
			settingClip.up_mc.removeEventListener( MouseEvent.CLICK, onUpClick );
			
			settingClip.down_mc.removeEventListener( MouseEvent.MOUSE_OUT, onDownOut );
			settingClip.down_mc.removeEventListener( MouseEvent.MOUSE_OVER, onDownOver );
			settingClip.down_mc.removeEventListener( MouseEvent.MOUSE_UP, onDownUp);
			settingClip.down_mc.removeEventListener( MouseEvent.MOUSE_DOWN, onDownDown);
			settingClip.down_mc.removeEventListener( MouseEvent.CLICK, onDownClick );
			
//			settingClip.hour_txt.addEventListener( FocusEvent.FOCUS_OUT, onCheckTextField );
//			settingClip.hour_txt.addEventListener( FocusEvent.FOCUS_IN, onHourFocusIn );
//			settingClip.min_txt.addEventListener( FocusEvent.FOCUS_OUT, onCheckTextField );
//			settingClip.min_txt.addEventListener( FocusEvent.FOCUS_IN, onMinFocusIn );
		}
		
		public function set editionType( value:String ):void
		{
			settingClip.editionType = value;
		}
		public function get editionType():String
		{
			return settingClip.editionType;
		}
	}
}