﻿package sqisoft.controls
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
		
	import flash.events.FocusEvent;
	import fl.managers.FocusManager;
	
	import sqisoft.net.FirmwareProtocol;
	import sqisoft.controls.StepButton;
	import sqisoft.events.CustomEvent;
	import sqisoft.manager.ChannelManager;
	
	import sqisoft.controls.CheckBox;
	
	import sqisoft.utils.StringUtil;
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class SettingShutdown extends Sprite
	{
		private var str:String;
		private var stepClip:StepButton;
		private var shutdownClip:MovieClip;
		private var parentDialog:MovieClip;
		
		private var focusManager:FocusManager = new FocusManager ( null );
		///private var format:TextFormat;
		
		private var editionType:String;
		
		private var chk:Array = [];		
		
		public function SettingShutdown( _parentDialog:MovieClip, skin:MovieClip=null )
		{
			//trace( "SettingShutdown" );
			super();
			
			if ( skin == null ) shutdownClip = new ShutdownClip();
			else shutdownClip = skin;
			
			parentDialog = _parentDialog;
			editionType = parentDialog.editionType;
			//editionType = "ENT";
			
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}
		
		private function onStage( evt:Event ):void
		{
			//trace( "onStage" );
			setLayout();
			setEvent();
			//defaultSetting();
			makeTimeButton();
			parsingShutdownInfo();
			//makeCheckBox();
			
		}
		
		private var comboButton:Array = [];
		private function setLayout():void
		{
			var layoutArr:Array = [ "ok_mc", "cancel_mc" ];
			for ( var i:int=0; i<layoutArr.length; i++ )
			{
				stepClip = new StepButton( shutdownClip[ layoutArr[ i ] ] );
				stepClip.name = layoutArr[ i ];
				stepClip.addEventListener( CustomEvent.CUSTOM_EVENT, onClickHandler );
			}

			for( var k:int = 0; k<ChannelManager.holydayInfo.length; k++ )
			{
				if ( ChannelManager.holydayInfo[ k ].indexOf(":") > 0 )
				{
					shutdownClip.all_combo.hour_txt.text = String ( ChannelManager.holydayInfo[ k ] ).split(":")[0];
					shutdownClip.all_combo.min_txt.text = String ( ChannelManager.holydayInfo[ k ] ).split(":")[1];
				}
				/*else if ( ChannelManager.holydayInfo[ k ] == "NOSET" )
				{
					shutdownClip.all_combo.hour_txt.text = "";
					shutdownClip.all_combo.min_txt.text = "";
				}*/
				else
				{
					//trace("엘스엘스");
				}
			}
			
			addChild( shutdownClip );
			defaultCheckBox()
			
		}
				
		private function defaultCheckBox():void
		{
			//trace( "defaultChekcBox" );
			
			for(var i:int = 0; i<7; i++ )
			{
				//trace("111" + i );
				chk[i] = new CheckBox();
				chk[i].name = "chk"+i;
				shutdownClip.chk_list.addChild( chk[i] );
				chk[i].y = i * 46.5;
				
				//trace("sdfalksieug;laje" + checkBox.name  );
			}
		}
		
		
		private function setEvent():void
		{
			
		}
		
		private function onClickHandler( evt:CustomEvent ):void
		{
			switch ( evt.currentTarget.name )
			{
				case "ok_mc":
					//trace( evt.currentTarget.name );
					//trace( "parentDialog:" + typeof parentDialog );
					if( editionType != "ENT" )
						sendShutdownInfo();
					//trace( "parentDialog:" + parentDialog );
					break;
				case "cancel_mc":
					//trace( evt.currentTarget.name );
					parentDialog.closeShutdownPopup();
					break;
				/*case "apply_mc":
					trace( evt.currentTarget.name );
					break;
				case "clear_mc":
					trace( evt.currentTarget.name );
					break;*/
				default:
					trace( "nothing" );
			}
		}
		
		
/*		private function onComboHandler( evt:CustomEvent ):void
		{
			//trace( "onComboHandler:" + evt.currentTarget.name );
			switch ( evt.currentTarget.name )
			{
				case "apply_mc":
					trace( evt.currentTarget.name );
					break;
				case "clear_mc":
					trace( evt.currentTarget.name );
					break;
				default:
					trace( "nothing" );
			}
		}
		*/
		
		private var selectedDayInfo:Array;
		private var selectedTimeInfo:Array;
		private function parsingShutdownInfo():void
		{
			var shutdownInfo:Array;
			var temp:String = ChannelManager.shutdownInfo.substr( 1, ChannelManager.shutdownInfo.length-2 );
		
//			if( temp != "" )
//			{
//			
				shutdownInfo = temp.split(",");
				selectedDayInfo = [];
				selectedTimeInfo = [];
				
				var tempString:String;
				var len:int = shutdownInfo.length;
	
				for ( var i:int=0; i<len; i++ )
				{
					tempString = shutdownInfo[i];
					selectedDayInfo.push( tempString.split("|")[0] );
					selectedTimeInfo.push( tempString.split("|")[1] );
				}
//			}
			makeCheckBox();
		}
		
		//private var monthArr:Array = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
		private var weekArr:Array = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
		private var checkBox:CheckBox;
		private var checkBoxArr:Array = [];
		public function makeCheckBox():void
		{
			trace("makeCheckBox()");
			/*var len:int = weekArr.length;
			for( var i:int=0; i<len; i++ )
			{
				checkBox = shutdownClip[ "chk" + i ];
				checkBox.label = weekArr[ i ].substr( 0, 3 ).toUpperCase();
				checkBox.labelColor = 0xFE4A4A;
				checkBox.addEventListener( CustomEvent.CUSTOM_EVENT, onCheckHandler );
				checkBoxArr.push( checkBox );
			}*/
			
			var len:int = weekArr.length;
			for( var i:int=0; i<len; i++ )
			{
				checkBox = chk[i];
				checkBox.label = weekArr[ i ].substr( 0, 3 ).toUpperCase();
				//checkBox.name = "chk"+[i] ;
				checkBox.labelColor = 0xFE4A4A;
				checkBox.addEventListener( CustomEvent.CUSTOM_EVENT, onCheckHandler );
				//checkBox.addEventListener( CustomEvent.CUSTOM_EVENT, onSelectedHandler );
				checkBoxArr.push( checkBox );
			}
			
			setCheckBox();
			//trace( "ChannelManager.holydayInfo : " + ChannelManager.holydayInfo );
		}
		
		private var stepButtonUp:StepButton;
		private var stepButtonDown:StepButton;
		private var stepButtonApply:StepButton;
		private var stepButtonClear:StepButton;
		private var upArr:Array = [];
		private var downArr:Array = [];

		public function makeTimeButton():void
		{
			//trace( "makeTimeButton");
			//var len:int = upArr.length;
			for( var i:int=0; i<7; i++ )
			{
				shutdownClip[ "chk" + i + "time" ].stop();
				
				shutdownClip[ "chk" + i + "time" ].hour_txt.selectable = false;
				shutdownClip[ "chk" + i + "time" ].min_txt.selectable = false;
				
				shutdownClip[ "chk" + i + "time" ].hour_txt.restrict = "0-9";
				shutdownClip[ "chk" + i + "time" ].min_txt.restrict = "0-9";
				
				stepButtonUp = new StepButton ( shutdownClip[ "chk" + i + "time" ].up_mc );
				stepButtonUp.name = "chk" + i + "time";
				stepButtonUp.visibleDisabled();
				upArr.push( stepButtonUp );
				
				stepButtonDown = new StepButton ( shutdownClip[ "chk" + i + "time" ].down_mc );
				stepButtonDown.name = "chk" + i + "time";
				stepButtonDown.visibleDisabled();
				downArr.push( stepButtonDown );
			}
		}
		
		private function timeSelected( selecteItem:String ):void
		{	
			var tempSelected:String = selecteItem+"time";
			//trace( "tempSelected ________++++++++"+ tempSelected);
			
			var len:int = upArr.length;
			for( var i:int=0; i<len; i++ )
			{
				if( upArr[ i ].name == tempSelected )
				{				
					upArr[i].visibleSet();
					downArr[i].visibleSet();
					
					shutdownClip[ "chk" + i + "time" ].hour_txt.selectable = true;
					shutdownClip[ "chk" + i + "time" ].min_txt.selectable = true;
					
					shutdownClip[ "chk" + i + "time" ].hour_txt.text = "";
					shutdownClip[ "chk" + i + "time" ].min_txt.text = "";
										
					shutdownClip[ "chk" + i + "time" ].gotoAndStop( "selected" );
					
					if ( !upArr[i].hasEventListener( CustomEvent.CUSTOM_EVENT ) )
						upArr[i].addEventListener( CustomEvent.CUSTOM_EVENT, onUpHandler );
					if ( !downArr[i].hasEventListener( CustomEvent.CUSTOM_EVENT ) )
						downArr[i].addEventListener( CustomEvent.CUSTOM_EVENT, onDownHandler );
					if ( !shutdownClip[ "chk" + i + "time" ].hour_txt.hasEventListener( FocusEvent.FOCUS_OUT ) )
						shutdownClip[ "chk" + i + "time" ].hour_txt.addEventListener( FocusEvent.FOCUS_OUT, onCheckTextField );
					if ( !shutdownClip[ "chk" + i + "time" ].hour_txt.hasEventListener( FocusEvent.FOCUS_IN ) )
						shutdownClip[ "chk" + i + "time" ].hour_txt.addEventListener( FocusEvent.FOCUS_IN, onFocusIn );
					if ( !shutdownClip[ "chk" + i + "time" ].min_txt.hasEventListener( FocusEvent.FOCUS_OUT ) )
						shutdownClip[ "chk" + i + "time" ].min_txt.addEventListener( FocusEvent.FOCUS_OUT, onMinCheckTextField );
					if ( !shutdownClip[ "chk" + i + "time" ].min_txt.hasEventListener( FocusEvent.FOCUS_IN ) )
						shutdownClip[ "chk" + i + "time" ].min_txt.addEventListener( FocusEvent.FOCUS_IN, onFocusIn );
					
				}
			}
		}
		
		private function timeDeselected( selecteItem:String ):void
		{
			var tempSelected:String = selecteItem + "time";
			
			var len:int = upArr.length;
			for( var i:int=0; i<len; i++ )
			{
				if( upArr[ i ].name == tempSelected )
				{
					upArr[i].visibleDisabled();
					downArr[i].visibleDisabled();
					
					shutdownClip[ "chk" + i + "time" ].hour_txt.selectable = false;
					shutdownClip[ "chk" + i + "time" ].min_txt.selectable = false;
					
					shutdownClip[ "chk" + i + "time" ].gotoAndStop( "deselected" );

					upArr[i].removeEventListener( CustomEvent.CUSTOM_EVENT, onUpHandler );
					downArr[i].removeEventListener( CustomEvent.CUSTOM_EVENT, onDownHandler );

					shutdownClip[ "chk" + i + "time" ].hour_txt.removeEventListener( FocusEvent.FOCUS_OUT, onCheckTextField );					
					shutdownClip[ "chk" + i + "time" ].hour_txt.removeEventListener( FocusEvent.FOCUS_IN, onFocusIn );
					shutdownClip[ "chk" + i + "time" ].min_txt.removeEventListener( FocusEvent.FOCUS_OUT, onMinCheckTextField );
					shutdownClip[ "chk" + i + "time" ].min_txt.removeEventListener( FocusEvent.FOCUS_IN, onFocusIn );
				}
			}
		}
		
		function onCheckTextField( evt:FocusEvent ):void
		{
			if ( evt.currentTarget.text != "" )
			{
				var hour:Number = Number( StringUtil.digit( Number( evt.currentTarget.text ) ) );
				if (hour > 23)
				{
					evt.currentTarget.text = StringUtil.digit( 0 );
				}
				else
				{
					evt.currentTarget.text = StringUtil.digit( hour );
				}
			}
		}
		
		function onMinCheckTextField( evt:FocusEvent ):void
		{
			if ( evt.currentTarget.text != "" )
			{	
				var min:Number = Number( StringUtil.digit( Number( evt.currentTarget.text ) ) );
			
				if (min > 59)
				{
					evt.currentTarget.text = StringUtil.digit( 0 );
				}
				else
				{
					evt.currentTarget.text = StringUtil.digit( min );
				}
			}
		}
		
		function onFocusIn( evt:FocusEvent ):void
		{
			if( evt.currentTarget.name == "hour_txt" )
				str = "hour_FocusIn";
			else
				str = "min_FocusIn";
			focusManager = new FocusManager ( this );
		}		
		
		function onUpHandler( evt:CustomEvent ):void
		{
			switch ( str ) 
			{
				case "hour_FocusIn" :
				if ( Number ( shutdownClip[evt.currentTarget.name].hour_txt.text ) > 00 && Number ( shutdownClip[evt.currentTarget.name].hour_txt.text ) < 23 ) 
				{
					shutdownClip[evt.currentTarget.name].hour_txt.text = String( Number ( shutdownClip[evt.currentTarget.name].hour_txt.text ) + 1 );
				}
				else if ( Number( shutdownClip[evt.currentTarget.name].hour_txt.text == "00" || shutdownClip[evt.currentTarget.name].hour_txt.text == "" ) ) 
				{
					shutdownClip[evt.currentTarget.name].hour_txt.text="1";
				}
				else if ( Number( shutdownClip[evt.currentTarget.name].hour_txt.text == "23" ) ) 
				{
					shutdownClip[evt.currentTarget.name].hour_txt.text="00";
				}
				break;
			
			case "min_FocusIn" :
				if ( Number ( shutdownClip[evt.currentTarget.name].min_txt.text ) >= 0 && Number ( shutdownClip[evt.currentTarget.name].min_txt.text ) < 59 ) 
				{
					shutdownClip[evt.currentTarget.name].min_txt.text = String ( Number ( shutdownClip[evt.currentTarget.name].min_txt.text ) + 1 );
				}
				else if ( Number ( shutdownClip[evt.currentTarget.name].min_txt.text == "59" ) ) 
				{
					shutdownClip[evt.currentTarget.name].min_txt.text="00";
				}
				break;
			}
		}
		
		function onDownHandler( evt:CustomEvent ):void
		{
			trace( evt.currentTarget.name );
					
			switch ( str ) 
			{
				case "hour_FocusIn" :
				if ( Number ( shutdownClip[evt.currentTarget.name].hour_txt.text ) > 1 && Number ( shutdownClip[evt.currentTarget.name].hour_txt.text ) < 25 )
				{
					shutdownClip[evt.currentTarget.name].hour_txt.text = String ( Number ( shutdownClip[evt.currentTarget.name].hour_txt.text ) -1 );
				}
			 	else if ( Number ( shutdownClip[evt.currentTarget.name].hour_txt.text == "00" || shutdownClip[evt.currentTarget.name].hour_txt.text == "" ) ) 
				{
					shutdownClip[evt.currentTarget.name].hour_txt.text = "23";
				}
			 	else if ( Number ( shutdownClip[evt.currentTarget.name].hour_txt.text == "01" || shutdownClip[evt.currentTarget.name].hour_txt.text == "1" ) ) 
				{
					shutdownClip[evt.currentTarget.name].hour_txt.text = "00";
				}
				break;
			
			case "min_FocusIn" :
				if ( Number ( shutdownClip[evt.currentTarget.name].min_txt.text ) > 1 && Number ( shutdownClip[evt.currentTarget.name].min_txt.text ) < 60 ) 
				{
					shutdownClip[evt.currentTarget.name].min_txt.text = String ( Number ( shutdownClip[evt.currentTarget.name].min_txt.text ) -1 );
				}
				 else if ( Number ( shutdownClip[evt.currentTarget.name].min_txt.text == "01" || shutdownClip[evt.currentTarget.name].min_txt.text == "1"  ) ) 
				 {
					shutdownClip[evt.currentTarget.name].min_txt.text = "00" ;
				 }
				else if ( Number ( shutdownClip[evt.currentTarget.name].min_txt.text == "00" || shutdownClip[evt.currentTarget.name].min_txt.text == "" ) ) 
				{
					shutdownClip[evt.currentTarget.name].min_txt.text="59";
				}
				break;
			}
		}
		
		function onCheckHandler( evt:CustomEvent ):void
		{			
			
			//trace("evt.currentTarget.name: " + evt.currentTarget.name);
				//focusManager.setFocus( null );
			if( evt.currentTarget.selected == false )
			{
				timeDeselected( evt.currentTarget.name );
			}
			else
			{
				timeSelected( evt.currentTarget.name );
			}
		}
		
		private function setCheckBox():void
		{
			//trace("setCheckBox" + selectedDayInfo);
			var len:int = selectedDayInfo.length;
			//trace( "setCheckBox ==  " + selectedDayInfo[0] );
			if( selectedDayInfo.length >0 && selectedDayInfo[0] != "" )
			{
				for( var i:int=0; i<len; i++ )
				{
					checkBox = chk[ selectedDayInfo[ i ] ];
					checkBox.selected = true;
					timeSelected( "chk" + selectedDayInfo[ i ] );
				}
				//makeTimeButton();
				for( var j:int=0; j<len; j++ )
				{ 
					shutdownClip[ "chk" + selectedDayInfo[ j ] + "time" ].hour_txt.text = selectedTimeInfo[ j ].split(":")[0];
					shutdownClip[ "chk" + selectedDayInfo[ j ] + "time" ].min_txt.text = selectedTimeInfo[ j ].split(":")[1];
				}
				if( editionType == "ENT" )
				{
					for( var k:int=0; k<checkBoxArr.length; k++ )
					{
						checkBoxArr[k].deleteEvent();
					}
				}
			}
			else
			{
				if( editionType == "ENT" )
				{
					for( var l:int=0; l<checkBoxArr.length; l++ )
					{
						checkBoxArr[l].deleteEvent();
					}
				}
				
				return;
			}
				//setTimeInfo();
				trace( "setCheckBox" + checkBox.selected );
				
			
		}
		
		private function sendShutdownInfo():void
		{
			var shutdownInfo:Array = [];
			
			var flag:Boolean;
		
			var getCheckBox:String = getCheckBoxInfo();
			//if ( getCheckBox != "" ) shutdownInfo.push( getCheckBox );
			
			//trace( "sendHolidayInfo((((((((((((()))))))))))" + getCheckBox);
			if( getCheckBox != "" )
				parentDialog.socket.send( FirmwareProtocol.FV_WEEK_SHUTDOWNTIME + "|WeekShutdownTime=(" + getCheckBox + ")" );
			else
				parentDialog.socket.send( FirmwareProtocol.FV_WEEK_SHUTDOWNTIME + "|WeekShutdownTime=()" );
			parentDialog.closeShutdownPopup();
		}
		
		private function getCheckBoxInfo():String
		{
			
			var checkArr:Array = [];
			var len:int = weekArr.length;
			for ( var i:int=0; i<len; i++ )
			{
				//trace("getCheckBoxInfo" + shutdownClip.chk_list.chk1 );
				
				if ( chk[i].selected ) 
				{
					
					if ( shutdownClip["chk" + i + "time"].hour_txt.text != "" )
					{
						var hour:Number = Number( StringUtil.digit( Number( shutdownClip["chk" + i + "time"].hour_txt.text ) ) );
						if (hour > 23)
						{
							shutdownClip["chk" + i + "time"].hour_txt.text = StringUtil.digit( 0 );
						}
						else
						{
							shutdownClip["chk" + i + "time"].hour_txt.text = StringUtil.digit( hour );
						}
					}
					else
					{
						shutdownClip["chk" + i + "time"].hour_txt.text = "00";
					}
					if ( shutdownClip["chk" + i + "time"].min_txt.text != "" )
					{	
						var min:Number = Number( StringUtil.digit( Number( shutdownClip["chk" + i + "time"].min_txt.text ) ) );
				
						if (min > 59)
						{
							shutdownClip["chk" + i + "time"].min_txt.text = StringUtil.digit( 0 );
						}
						else
						{
							shutdownClip["chk" + i + "time"].min_txt.text = StringUtil.digit( min );
						}
					}
					else
					{
						shutdownClip["chk" + i + "time"].min_txt.text = "00";
					}
					checkArr.push( i + "|" + shutdownClip[ "chk" + i + "time" ].hour_txt.text + ":" + shutdownClip[ "chk" + i + "time" ].min_txt.text );
				}
			}
			return checkArr.join(",");
		}
	}
}