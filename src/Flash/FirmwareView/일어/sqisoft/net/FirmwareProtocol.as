﻿package sqisoft.net
{
	public class FirmwareProtocol
	{
		public static const FV_STUDIO:String = "FV_STUDIO";
		public static const FV_SHUTDOWN:String = "FV_SHUTDOWN";
		public static const FV_REBOOT:String = "FV_REBOOT";
		public static const FV_UBCSTART:String = "FV_UBCSTART";
		public static const FV_UBCSTOP:String = "FV_UBCSTOP";
		public static const FV_AUTO_ON:String = "FV_AUTO_ON";
		public static const FV_AUTO_OFF:String = "FV_AUTO_OFF";
		public static const FV_QUERY_CURRENT_CH:String = "FV_QUERY_CURRENT_CH";
		public static const FV_CH_PLAY:String = "FV_CH_PLAY";
		public static const FV_CH_STOP:String = "FV_CH_STOP";
		public static const FV_CH_MODIFY:String = "FV_CH_MODIFY";
		public static const FV_CH_ADD:String = "FV_CH_ADD";
		public static const FV_CH_DELETE:String = "FV_CH_DELETE";
		public static const FV_CH_RECOVERY:String = "FV_CH_RECOVERY";
		public static const FV_SCHEDULE_MODIFY:String = "FV_SCHEDULE_MODIFY";
		public static const FV_DELETE_ALL:String = "FV_DELETE_ALL";
		public static const FV_EXPORT:String = "FV_EXPORT";
		public static const FV_IMPORT:String = "FV_IMPORT";
		public static const FV_REFRESH:String = "FV_REFRESH";
		public static const FV_QUERY_RECOVERY:String = "FV_QUERY_RECOVERY";
		public static const FV_QUERY_UBC_STATUS:String = "FV_QUERY_UBC_STATUS";
		public static const FV_DISPLAY_SET:String = "FV_DISPLAY_SET";
		public static const FV_TOOLS:String = "FV_TOOLS";
		public static const FV_UPDATE:String = "FV_UPDATE";
		public static const FV_REGISTER:String = "FV_REGISTER";
		public static const FV_SHUTDOWNTIME:String = "FV_SHUTDOWNTIME";
		public static const FV_WEEK_SHUTDOWNTIME:String = "FV_WEEK_SHUTDOWNTIME";
		public static const FV_MOUSE:String = "FV_MOUSE";
		public static const FV_STARTER:String = "FV_STARTER";
    }
}
