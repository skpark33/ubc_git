﻿package sqisoft.net
{
	import flash.net.Socket;
	import flash.errors.IOError;
	
	import flash.events.ProgressEvent;
	import flash.events.Event;
	import flash.events.SecurityErrorEvent
	import flash.events.IOErrorEvent;
		
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	import sqisoft.events.FirmwareSocketEvent;
	
	
	[ Event( name = "SOCKET_DATA_COMPLETE", type = "sqisoft.events.FirmwareSocketEvent" ) ]
    public class FirmwareSocket extends Socket
	{
		private var response:String;
		private var pingTimer:Timer;
		private var isReceive:Boolean;
		
		private var host:String;
		private var port:uint;
		
		public function FirmwareSocket( _host:String=null, _port:uint=0 )
		{
			super();
			
			configureListeners();
			
			host = _host;
			port = _port;
			
			isReceive = true;
			
			super.timeout = 3000;
			
			socketConnect();
		}
		
		private function socketConnect():void
		{
			if ( host && port ) super.connect( host, port );
		}
		
		private function configureListeners():void
		{
			addEventListener( ProgressEvent.SOCKET_DATA, receive );
			addEventListener( Event.CONNECT, connectHandler );
			addEventListener( Event.CLOSE, closeHandler );			
			addEventListener( IOErrorEvent.IO_ERROR, ioErrorHandler );
			addEventListener( SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler );
		}
		
		public function send( str:String ):void
		{
			if ( isReceive == false ) return;			
			isReceive = false;
			
			response = "";
			
			str = str.split("undefined").join("");
			str += "\n";
			
			trace("send str이야.." + str );
			
			try
			{
				writeUTFBytes( str );
			}
			catch(e:IOError)
			{
				trace( e );
			}
			
			flush();
		}
		
		private function receive( evt:ProgressEvent ):void
		{
			isReceive = true;
			
			var str:String = readUTFBytes( bytesAvailable );
			response += str;
			
			dispatchEvent( new FirmwareSocketEvent( FirmwareSocketEvent.SOCKET_DATA_COMPLETE, response ) );
		}
		
		public function ping():void
		{
			send( "ping" );
			
			var delay:uint = 10000;
			var repeat:uint = 0;
			
			pingTimer = new Timer( delay, repeat );
			pingTimer.addEventListener( TimerEvent.TIMER, onPing );
			pingTimer.start();
		}
		
		private function onPing( evt:TimerEvent ):void
		{
			send( "ping" );
		}
		
		private function connectHandler( evt:Event ):void
		{
			trace( "connectHandler: " + evt );
			
			response = "connect";
			dispatchEvent( new FirmwareSocketEvent( FirmwareSocketEvent.SOCKET_DATA_COMPLETE, response ) );
		}
		
		private function closeHandler( evt:Event ):void
		{
			trace( "closeHandler: " + evt );
			trace( response.toString() );
			
			pingTimer.stop();
			
			socketConnect();
		}
		
		private function ioErrorHandler( evt:IOErrorEvent ):void
		{
			trace( "ioErrorHandler: " + evt );
		}
		
		private function securityErrorHandler( evt:SecurityErrorEvent ):void
		{
			trace( "securityErrorHandler: " + evt );
			
			socketConnect();
		}
    }
}
