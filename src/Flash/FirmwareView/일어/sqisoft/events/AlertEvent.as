﻿package sqisoft.events
{
	public class AlertEvent
	{
		// 확인버튼 클릭시 발생하는 이벤트 타입(문자열)
		public static const ALERT_CONFIRM:String = "AlertWinOK";

		// 취소버튼 클릭시 발생하는 이벤트
		public static const ALERT_CANCEL:String = "cancel";

		// 얼럿창에 메시지를 띄울 때 발생하는 이벤트
		public static const ALERT:String = "alert";
	}
}