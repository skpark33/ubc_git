﻿package sqisoft.events
{
	import flash.events.Event;
	
	
	public class FirmwareSocketEvent extends Event
	{
		public static const SOCKET_DATA_COMPLETE:String = "socketDataComplete";
		
		public function FirmwareSocketEvent( type:String, response:String, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
			
			_response = response;
		}
		
		override public function clone():Event
		{
			return new FirmwareSocketEvent( type, _response, bubbles, cancelable );
		}
		
		private var _response:String;
		public function get response():String
		{
			return _response;
		}
	}
}