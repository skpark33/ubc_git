﻿package sqisoft.controls
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import sqisoft.events.CustomEvent;
		
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class StepButton extends Sprite
	{
		private var stepClip:MovieClip;
		
		public function StepButton( skin:MovieClip )
		{
			//trace( "stepButton생성" );
			super();
			
			stepClip = skin;
			
			setEvent();
			defaultSetting();
		}
		
		private function setEvent():void
		{
			//trace( "EventSet" );
			if( checkValid() == true )
			{
				stepClip.addEventListener( MouseEvent.ROLL_OUT, onStepClipOut );
				stepClip.addEventListener( MouseEvent.ROLL_OVER, onStepClipOver );
				//stepClip.addEventListener( MouseEvent.CLICK, onStepClipClick );
				stepClip.addEventListener( MouseEvent.MOUSE_DOWN, selectedItem );
				stepClip.addEventListener( MouseEvent.MOUSE_UP, deselectedItem );
				
			}
			else
			{
				throw new Error( "stepClip에 설정한 Label명이 틀립니다" );
			}
		}
		
		private function defaultSetting():void
		{
			//trace( "DefaultSetting" );
			stepClip.bg.gotoAndStop(1);
		}
		
		private var labelArr:Array = [ "out", "over", "selected", "disabled" ];
		private var frameArr:Array = [];
		
		private function checkValid(): Boolean
		{
			//trace( "checkValid" );
			var ary:Array = stepClip.bg.currentLabels;
			
			for( var i:int=0; i<labelArr.length; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
		
		private function onStepClipOut( evt:MouseEvent ):void
		{
			//trace( "onStepClipOut" );
			stepClip.bg.gotoAndStop( frameArr[ 0 ] );
		}
		
		private function onStepClipOver( evt:MouseEvent ):void
		{	
			//trace( "onStepClipOver" );
			stepClip.bg.gotoAndStop( frameArr[ 1 ] );			
		}
		
		private function onStepClipClick( evt:MouseEvent ):void
		{	
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}

		public function selectedItem( evt:MouseEvent ):void
		{
			//trace( "selectedItem" );
			stepClip.bg.gotoAndStop( frameArr[ 2 ] );
						
			stepClip.removeEventListener( MouseEvent.ROLL_OUT, onStepClipOut );
			stepClip.removeEventListener( MouseEvent.ROLL_OVER, onStepClipOver );		
		}
		
		public function deselectedItem( evt:MouseEvent ):void
		{
			//trace( "deselectedItem" );
			stepClip.bg.gotoAndStop( frameArr[ 0 ] );
			
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
			
			stepClip.addEventListener( MouseEvent.ROLL_OUT, onStepClipOut );
			stepClip.addEventListener( MouseEvent.ROLL_OVER, onStepClipOver );
		}
		
		//public function displaySelectedItem():void
//		{
//			trace( "selectedItem" );
//			stepClip.bg.gotoAndStop( frameArr[ 2 ] );
//						
//			stepClip.removeEventListener( MouseEvent.ROLL_OUT, onStepClipOut );
//			stepClip.removeEventListener( MouseEvent.ROLL_OVER, onStepClipOver );		
//		}
//		
//		public function displayDeselectedItem():void
//		{
//			stepClip.bg.gotoAndStop( frameArr[ 0 ] );
//			
//			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
//			
//			stepClip.addEventListener( MouseEvent.ROLL_OUT, onStepClipOut );
//			stepClip.addEventListener( MouseEvent.ROLL_OVER, onStepClipOver );
//		}
		
		public function disabled():void
		{
			//trace( "disabled" );
			stepClip.visible = false;
			
			stepClip.removeEventListener( MouseEvent.ROLL_OUT, onStepClipOut );
			stepClip.removeEventListener( MouseEvent.ROLL_OVER, onStepClipOver );
			stepClip.removeEventListener( MouseEvent.MOUSE_DOWN, selectedItem );
			stepClip.removeEventListener( MouseEvent.MOUSE_UP, deselectedItem );
		}
		
		public function visibleDisabled():void
		{
			//trace( "visibleDisabled" );
			//stepClip.visible = false;
			
			//trace( "visibleDisabled" );
			stepClip.removeEventListener( MouseEvent.ROLL_OUT, onStepClipOut );
			stepClip.removeEventListener( MouseEvent.ROLL_OVER, onStepClipOver );
			stepClip.removeEventListener( MouseEvent.MOUSE_DOWN, selectedItem );
			stepClip.removeEventListener( MouseEvent.MOUSE_UP, deselectedItem );
			//trace( "visibleDisabled" );
		}
		public function visibleSet():void
		{
			//trace( "visibleDisabled" );
			//stepClip.visible = false;
			
			//trace( "visibleDisabled" );
			stepClip.addEventListener( MouseEvent.ROLL_OUT, onStepClipOut );
			stepClip.addEventListener( MouseEvent.ROLL_OVER, onStepClipOver );
			stepClip.addEventListener( MouseEvent.MOUSE_DOWN, selectedItem );
			stepClip.addEventListener( MouseEvent.MOUSE_UP, deselectedItem );
			//trace( "visibleDisabled" );
		}
		
		public function get label():String
		{	
			return stepClip.txt.text;
		}
		
		public function set label( value:String ):void
		{
			stepClip.txt.text = value;
		}
		
		private var _enabled:Boolean = true;
		public function get enabled():Boolean
		{
			return _enabled;
		}
		
		public function set enabled( value:Boolean ):void
		{
			if ( _enabled == value ) return;
			
			_enabled = value;
			
			if ( _enabled )
			{
				stepClip.mouseChildren = true;
				stepClip.mouseEnabled = true;
				stepClip.alpha = 1;
			}
			else
			{
				stepClip.mouseChildren = false;
				stepClip.mouseEnabled = false;
				stepClip.alpha = 0.5;
			}
		}
	}
}