﻿package sqisoft.controls
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
		
	
	public class CalTest extends Sprite
	{
		private var calClip:MovieClip;
		
		public function CalTest( skin:MovieClip=null )
		{
			super();
			
			if ( skin == null ) calClip = new CalendarTest();
			else calClip = skin;
			
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}
		
		private function onStage( evt:Event ):void
		{
			setLayout();
			setEvent();
			defaultSetting();
		}
		
		private function setLayout():void
		{
			addChild( calClip );
		}
		
		private function setEvent():void
		{
			//calClip.btn.addEventListener( MouseEvent.CLICK, onClickHandler );
		}
		
		private function defaultSetting():void
		{			
			
		}
		
		private function onClickHandler( evt:MouseEvent ):void
		{
			trace( evt.currentTarget.name );
		}
	}
}