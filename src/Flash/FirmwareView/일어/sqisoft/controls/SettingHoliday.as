﻿package sqisoft.controls
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import sqisoft.net.FirmwareProtocol;
	import sqisoft.controls.StepButton;
	import sqisoft.events.CustomEvent;
	import sqisoft.manager.ChannelManager;
	
	import sqisoft.controls.Calendar;
	import sqisoft.controls.CheckBox;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class SettingHoliday extends Sprite
	{
		private var stepClip:StepButton;
		private var holidayClip:MovieClip;
		private var parentDialog:MovieClip;
		private var format:TextFormat;
		
		private var editionType:String;
		
		private var calendar_cb:Calendar;
		
		private var chk:Array = [];		
		
		public function SettingHoliday( _parentDialog:MovieClip, skin:MovieClip=null )
		{
			//trace("SettingHoliday");
			super();
			
			if ( skin == null ) holidayClip = new HolidayClip();
			else holidayClip = skin;
			
			parentDialog = _parentDialog;
			editionType = parentDialog.editionType;
			//editionType = "ENT";
			
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}
		
		private function onStage( evt:Event ):void
		{
			//trace("onStage");
			
			setLayout();
			setEvent();
			defaultSetting();
			parsingHolidayInfo();
			
			//defaultCheckBox();
		}
		
		private function setLayout():void
		{
			//trace("setLayout");
			
			var layoutArr:Array = [ "right_mc", "left_mc", "ok_mc", "cancel_mc" ];
			for ( var i:int=0; i<layoutArr.length; i++ )
			{
				stepClip = new StepButton( holidayClip[ layoutArr[ i ] ] );
				stepClip.name = layoutArr[ i ];
				
				stepClip.addEventListener( CustomEvent.CUSTOM_EVENT, onClickHandler );
			}
			addChild( holidayClip );
			defaultCalendar();
			defaultCheckBox();
		}
		
		private function setEvent():void
		{
			//trace("setEvent");
		}
		
		private function defaultSetting():void
		{
			//trace("defaultSetting");
			
			format = new TextFormat();
			format.font = "Verdana";
            format.color = 0x999999;
            format.size = 20;
			
			holidayClip.list_cb.setStyle( "textFormat", format );
		}
		
		private function defaultCalendar():void
		{
			//trace("setCalendar" + holidayClip);
			//private var calendar:Calendar;
			calendar_cb = new Calendar();
			holidayClip.calendar_loader.addChild( calendar_cb );
			//trace("endSetCalendar");
		}
		
		private function defaultCheckBox():void
		{
			//trace( "defaultChekcBox" );
			
			for(var i:int = 0; i<7; i++ )
			{
				//trace("111" + i );
				chk[i] = new CheckBox();
				holidayClip.chk_list.addChild( chk[i] );
				chk[i].x = i * 85.85;
				
				//trace("sdfalksieug;laje" + checkBox.name  );
			}
		}
		
		private function onClickHandler( evt:CustomEvent ):void
		{
			//trace("onClickHandler");
			
			switch ( evt.currentTarget.name )
			{
				case "right_mc":
					if( editionType !="ENT" )
						rightClickHandler();
					break;
				case "left_mc":
					if( editionType !="ENT" )
						leftClickHandler();
					break;
				case "ok_mc":
					if( editionType !="ENT" )
						sendHolidayInfo();
					break;
				case "cancel_mc":
					parentDialog.closeHolidayPopup();
					break;
				default:
					trace( "nothing" );
			}
		}
		
		private var time:String;
		private var selectedDayInfo:Array;
		private var selectedWeekInfo:Array;
		private function parsingHolidayInfo():void
		{
			//trace( "parsingHolidayInfo::::::::::::" + time );
			time = ChannelManager.holydayInfo[0];
			//trace( "parsingHolidayInfo::::::::::::" + ChannelManager.holydayInfo );
			selectedDayInfo = [];
			selectedWeekInfo = [];
			var len:int = ChannelManager.holydayInfo.length;
			for ( var i:int=1; i<len; i++ )
			{
				if ( ChannelManager.holydayInfo[ i ].indexOf(".") > 0 )
				{
					selectedDayInfo.push( ChannelManager.holydayInfo[ i ] );
				}
				else
				{
					selectedWeekInfo.push( ChannelManager.holydayInfo[ i ] );
				}
			}
			makeCheckBox();
			makeDayList();
		}
		
		private var monthArr:Array = [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ];
		private var weekArr:Array = [ "日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日" ];
		private var checkBox:CheckBox;
		private var checkBoxArr:Array = [];
		public function makeCheckBox():void
		{
			//trace( "makeCheckBox" + ChannelManager.holydayInfo);
			var len:int = weekArr.length;
			for( var i:int=0; i<len; i++ )
			{
				checkBox = chk[i];
				checkBox.label = weekArr[ i ].substr( 0, 3 ).toUpperCase();
				checkBox.labelColor = 0xFE4A4A;
				//checkBox.addEventListener( CustomEvent.CUSTOM_EVENT, onSelectedHandler );
				checkBoxArr.push( checkBox );
			}
			setCheckBox();
			//trace( "ChannelManager.holydayInfo : " + ChannelManager.holydayInfo );
		}
		
		private function setCheckBox():void
		{
			//trace( "selectedWeekInfo:::::::::::::::" + selectedWeekInfo[0] + "  " + len );
			var len:int = selectedWeekInfo.length;
			if( selectedWeekInfo.length > 0 && selectedWeekInfo[0] != "" )
			{
				for( var i:int=0; i<len; i++ )
				{
					//trace( "setCheckBox : " + len );
					checkBox = chk [ selectedWeekInfo[ i ] ];
					checkBox.selected = true;
				}
			}
			//trace( "setCheckBox" + checkBox.selected );
			
			if( editionType == "ENT" )
			{
				for( var j:int=0; j<checkBoxArr.length; j++ )
				{
					checkBoxArr[j].deleteEvent();
				}
			}
		}
		
		
		/*function onSelectedHandler( evt:CustomEvent ):void
		{
			trace("onSelectedHandler:::::::::::::::" + evt.currentTarget.label );
			trace("onSelectedHandler:::::::::::::::" + evt.currentTarget.selected );
		}*/
		
		private function makeDayList():void
		{
			//trace( "makeDayList" );
			var len:int = selectedDayInfo.length;
			for ( var i:int=0; i<len; i++ )
			{
				var now:Date = new Date();
				
				var month:String = selectedDayInfo[ i ].split(".")[ 0 ];
				var day:String = selectedDayInfo[ i ].split(".")[ 1 ];
				
				var d:Date = calendar_cb.selectedDate = new Date( now.getFullYear(), Number(month) - 1, day );
				if ( d == null ) return;
				
				var labelString:String = d.getFullYear() + "年 " + monthArr[ d.getMonth() ] + " " + d.getDate() + " " + weekArr[ d.getDay() ];
				var dataString:String = ( d.getMonth() + 1 ) + "." + d.getDate();
				
				if ( checkList( dataString ) == false )	holidayClip.list_cb.addItem( { label:labelString, data:dataString } );
			}
		}
		
		private function checkList( value:String ):Boolean
		{
			//trace( "checkList" );
			var len:int = holidayClip.list_cb.length;
			for ( var i:int=0; i<len; i++ )
			{
				if ( value == holidayClip.list_cb.getItemAt( i ).data )
				{
					return true;
				}
			}
			return false;
		}
		
		private function rightClickHandler():void
		{
			//trace( "rightClickHandler"  + calendar_cb.selectedDate);
			var d:Date = calendar_cb.selectedDate;
	
			if ( d == null ) return;
			
			var labelString:String = d.getFullYear() + "年 " + monthArr[ d.getMonth() ] + " " + d.getDate() + " " + weekArr[ d.getDay() ];
			var dataString:String = ( d.getMonth() + 1 ) + "." + d.getDate();
			
			if ( checkList( dataString ) == false )	holidayClip.list_cb.addItem( { label:labelString, data:dataString } );
		}
		
		private function leftClickHandler():void
		{
			//trace( "leftClickHandler" );
			if ( holidayClip.list_cb.selectedIndex < 0 ) return;	
			holidayClip.list_cb.removeItemAt( holidayClip.list_cb.selectedIndex );
		}
		
		private function sendHolidayInfo():void
		{
			//trace( "sendHolidayInfotime:::::::::" + time );
			var holidayInfo:Array = [];
			holidayInfo.push( time );
			
			var getList:String = getListInfo();
			//trace( "sendHolidayInfoGGGGGGetList:::::::::" + getList );
			if ( getList != "" ) holidayInfo.push( getList );
			
			var getCheckBox:String = getCheckBoxInfo();
			//trace( "sendHolidayInfoGGGGGGetCheckBox:::::::::" + getCheckBox );
			if ( getCheckBox != "" ) holidayInfo.push( getCheckBox );
			
			parentDialog.socket.send( FirmwareProtocol.FV_SHUTDOWNTIME + "|" + queryHoliday( holidayInfo ) );
			parentDialog.closeHolidayPopup();
		}
		
		private function queryHoliday( infoArr:Array ):String
		{
			//trace( "queryHoliday" );
			var query:String = "ShutdownTime=" + infoArr.join("^");
			
			return query;
		}
		
		private function getListInfo():String
		{
			//trace( "getListInfo" );
			var listArr:Array = [];
			var len:int = holidayClip.list_cb.length;
			for ( var i:int=0; i<len; i++ )
			{
				listArr.push( holidayClip.list_cb.getItemAt( i ).data );
			}
			return listArr.join("^");
		}
		
		private function getCheckBoxInfo():String
		{
			//trace( "getCheckBoxInfo" );
			var checkArr:Array = [];
			var len:int = weekArr.length;
			for ( var i:int=0; i<len; i++ )
			{
				if ( chk[i].selected ) checkArr.push( i );
			}
			return checkArr.join("^");
		}
	}
}