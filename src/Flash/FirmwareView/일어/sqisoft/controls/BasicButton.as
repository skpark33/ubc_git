﻿package sqisoft.controls
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import sqisoft.events.CustomEvent;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class BasicButton extends Sprite
	{
		protected var clip:MovieClip;
		
		public function BasicButton( skin:MovieClip )
		{
			super();
			
			clip = skin;
			
			setLayout();			
			setEvent();
			defaultSetting();
		}
		
		private function setLayout():void
		{
			if ( !contains( clip ) )
				addChild( clip );
		}
		
		private function setEvent():void
		{
			if ( checkValid() == true )
			{
				clip.addEventListener( MouseEvent.ROLL_OVER, onClipOver );
				clip.addEventListener( MouseEvent.ROLL_OUT, onClipOut );
				clip.addEventListener( MouseEvent.CLICK, onClipClick );
			}
			else
			{
				throw new Error( "DayClip에 설정한 Label명이 틀립니다" );
			}			
		}
		
		private function defaultSetting():void
		{
			clip.bg.gotoAndStop( 1 );
		}
		
		private var labelArr:Array = [ "out", "over", "selected" ];
		private var frameArr:Array = [];
		
		private function checkValid():Boolean
		{
			var ary:Array = clip.bg.currentLabels;
			
			var len:int = labelArr.length;
			for( var i:int=0; i<len; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
				
		private var targetFrame:int = 1;
		private function onClipOver( evt:MouseEvent ):void
		{
			if( clip.bg.currentLabel != "selected" )
			{
				targetFrame = frameArr[ 1 ];
				clip.bg.addEventListener( Event.ENTER_FRAME, onEnter );
			}	
		}
		
		private function onClipOut( evt:MouseEvent ):void
		{	
			if( clip.bg.currentLabel != "selected" )
			{
				targetFrame = frameArr[ 0 ];
				clip.bg.addEventListener( Event.ENTER_FRAME, onEnter );
			}	
		}
		
		private function onClipClick( evt:MouseEvent=null ):void
		{			
			
			clip.bg.gotoAndStop( frameArr[ 2 ] );
			clip.bg.removeEventListener( Event.ENTER_FRAME, onEnter );
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}
		
		private function onEnter( evt:Event ):void
		{
			if ( clip.bg.currentFrame > targetFrame )
			{
				clip.bg.prevFrame();
			}
			else if ( clip.bg.currentFrame < targetFrame )
			{
				clip.bg.nextFrame();
			}
			else
			{
				removeEventListener( Event.ENTER_FRAME, onEnter );
			}
		}
	}
}















