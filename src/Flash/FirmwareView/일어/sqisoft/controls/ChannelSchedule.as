﻿package sqisoft.controls
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFieldType;
	
	import sqisoft.net.FirmwareProtocol;	
	import sqisoft.controls.StepButton;
	import sqisoft.events.CustomEvent;
	import sqisoft.manager.ChannelManager;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class ChannelSchedule extends Sprite
	{
		private var stepClip:StepButton;
		private var scheduleClip:MovieClip;
		private var parentDialog:MovieClip;
		 
		private var type:String;
				
		public function ChannelSchedule( _parentDialog:MovieClip, _type:String="import", skin:MovieClip=null )
		{
			super();
			
			if ( skin == null ) scheduleClip = new ScheduleClip();
			else scheduleClip = skin;
			
			parentDialog = _parentDialog;
			type = _type;
			
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}
		
		private function onStage( evt:Event ):void
		{
			setLayout();
			setEvent();
			defaultSetting();
		}
		
		private var stepClipArr:Array;
		private function setLayout():void
		{
			stepClipArr = [];
			
			var layoutArr:Array = [ "ok_mc", "cancel_mc" ];
			for ( var i:int=0; i<layoutArr.length; i++ )
			{
				stepClip = new StepButton( scheduleClip[ layoutArr[ i ] ] );
				stepClip.name = layoutArr[ i ];
				
				stepClip.addEventListener( CustomEvent.CUSTOM_EVENT, onClickHandler );
				
				stepClipArr.push( stepClip );
			}
			addChild( scheduleClip );
		}
		
		private function setEvent():void
		{
			scheduleClip.user_mc.addEventListener( MouseEvent.CLICK, onCreatorClick );
			scheduleClip.server_mc.addEventListener( MouseEvent.CLICK, onCreatorClick );
		}
		
		private function defaultSetting():void
		{
			scheduleClip.user_mc.gotoAndStop( 1 );
			scheduleClip.server_mc.gotoAndStop( 1 );
			scheduleClip.subject.gotoAndStop( type );
			
			makeSchedule();
			
			if ( networkuse == "0" )
			{
				scheduleClip.user_mc.dispatchEvent( new MouseEvent( MouseEvent.CLICK ) );
			}
			else
			{
				scheduleClip.server_mc.dispatchEvent( new MouseEvent( MouseEvent.CLICK ) );
			}
		}
		
		private function onClickHandler( evt:CustomEvent ):void
		{
			switch ( evt.currentTarget.name )
			{
				case "ok_mc":
					sendScheduleInfo();
					break;
				case "cancel_mc":
					parentDialog.closeSchedulePopup();
					break;
				default:
					trace( "nothing" );
			}
		}
		
		private function makeSchedule():void
		{
			if ( type == "import" )
			{
				scheduleClip.channelName_txt.type = TextFieldType.INPUT;
				scheduleClip.channelName_txt.alpha = 1;
				
				channelName = "";
				channelDesc = "";
				sitename = "";
				networkuse = "1";
				
				scheduleClip.user_mc.mouseChildren = false;
				scheduleClip.user_mc.mouseEnabled = false;
				
				scheduleClip.sitename_txt.type = TextFieldType.INPUT;
				scheduleClip.sitename_txt.selectable = true;
				scheduleClip.sitename_txt.alpha = 1;
			}
			else
			{
				scheduleClip.channelName_txt.type = TextFieldType.DYNAMIC;
				scheduleClip.channelName_txt.alpha = 0.5;
				
				channelName = ChannelManager.advtsArr[ ChannelManager.selectedAdvtsNum ].name;
				channelDesc = ChannelManager.advtsArr[ ChannelManager.selectedAdvtsNum ].desc;
				sitename = ChannelManager.advtsArr[ ChannelManager.selectedAdvtsNum ].sitename;
				networkuse = ChannelManager.advtsArr[ ChannelManager.selectedAdvtsNum ].networkuse;
				
				//scheduleStatus();
			}
		}
		
		private function scheduleStatus():void
		{
			scheduleClip.sitename_txt.type = ( networkuse == "1" ) ? TextFieldType.INPUT : TextFieldType.DYNAMIC;
			scheduleClip.sitename_txt.selectable = ( networkuse == "1" ) ? true : false;
			scheduleClip.sitename_txt.alpha = scheduleClip.site_bg.alpha = ( networkuse == "1" ) ? 1 : 0.5;
		}
		
		private function onCreatorDeselect( evt:Event ):void
		{
			var target:MovieClip = evt.target as MovieClip;
			
			target.prevFrame();
			if ( target.currentFrame == 1 )
			{
				target.mouseChildren = true;
				target.mouseEnabled = true;
				target.removeEventListener( Event.ENTER_FRAME, onCreatorDeselect );
			}
		}
		
		private function onCreatorSelect( evt:Event ):void
		{
			var target:MovieClip = evt.target as MovieClip;
			
			target.nextFrame();
			if ( target.currentFrame == target.totalFrames )
			{
				target.mouseChildren = false;
				target.mouseEnabled = false;
				target.removeEventListener( Event.ENTER_FRAME, onCreatorSelect );
			}
		}
		
		private var creatorRadioClip:MovieClip;
		private function onCreatorClick( evt:MouseEvent ):void
		{
			if ( creatorRadioClip != null )
				creatorRadioClip.addEventListener( Event.ENTER_FRAME, onCreatorDeselect );
			creatorRadioClip = evt.target as MovieClip;
			creatorRadioClip.addEventListener( Event.ENTER_FRAME, onCreatorSelect);
			trace( "creatorRadioClip.name : " + creatorRadioClip.name );
			networkuse = ( creatorRadioClip.name == "user_mc" ) ? "0" : "1";
			//scheduleStatus();
		}
		
		private function sendScheduleInfo():void
		{
			if ( checkOverlap() == true )
			{
				// 중복 메세지
				return;
			}
			
			if ( type == "import" )
			{
				if( channelName != "" )
					parentDialog.socket.send( FirmwareProtocol.FV_CH_ADD + "|" + queryImportSchedule() );
			}
			else
			{
				parentDialog.socket.send( FirmwareProtocol.FV_SCHEDULE_MODIFY + "|" + querySchedule() );
			}
			parentDialog.closeSchedulePopup();
		}
		
		private function querySchedule():String
		{
			//trace( "ChannelManager.selectChannelNum ::::::::::::: " + ChannelManager.selectChannelNum );
			
			var query:String = "";
			query += "Ch=ch" + ChannelManager.selectChannelNum + ","
			query += "ChannelName=" + channelName + ","
			query += "SiteName=" + sitename + ","
			query += "NetworkUse=" + networkuse + ","
			query += "Desc=" + channelDesc;
			
			return query;
		}
		
		private function queryImportSchedule():String
		{
			//ChannelName=,SiteName=,NetworkUse=,Desc=
			//trace( "ChannelManager.selectChannelNum ::::::::::::: " + ChannelManager.selectChannelNum );
			
			var query:String = "";
			query += "ChannelName=" + channelName + ","
			query += "SiteName=" + sitename + ","
			query += "NetworkUse=1,"
			query += "Desc=" + channelDesc;
			
			return query;
		}
		
		private function checkOverlap():Boolean
		{
			var len:int = ChannelManager.advtsArr.length;
			for( var i:int=0; i<len; i++ )
			{
				if ( ChannelManager.advtsArr[ i ] == channelName ) return true;
			}
			return false;
		}
		
		public function disabled():void
		{
			scheduleClip.user_mc.visible = false;
		}
		
		public function get channelName():String
		{
			return scheduleClip.channelName_txt.text;
		}
		
		public function set channelName( value:String ):void
		{
			scheduleClip.channelName_txt.text = value;
		}
		
		public function get channelDesc():String
		{
			return scheduleClip.channelDesc_txt.text;
		}
		
		public function set channelDesc( value:String ):void
		{
			scheduleClip.channelDesc_txt.text = value;
		}
		
		public function get sitename():String
		{
			if ( networkuse == "0" ) return "";
			return scheduleClip.sitename_txt.text;
		}
		
		public function set sitename( value:String ):void
		{
			scheduleClip.sitename_txt.text = value;
		}
		
		private var _networkuse:String;
		public function get networkuse():String
		{
			return _networkuse;
		}
		
		public function set networkuse( value:String ):void
		{
			_networkuse = value;
		}
	}
}