﻿package sqisoft.controls
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import sqisoft.controls.calendarClasses.Day;
	import sqisoft.events.DayEvent;
	import sqisoft.utils.DisplayObjectUtil;
	

	public class Calendar extends Sprite
	{
		private var calendarClip:MovieClip;
		private var day:Day;
		private var _parent:Object;
		
		private var startX:Number;
		private var startY:Number;
		private var spaceX:Number;
		private var spaceY:Number;
				
		public function Calendar( calendarClip:MovieClip=null,
								  day:Day=null,
								  startX:Number=8,
								  startY:Number=70,
								  spaceX:Number=37,
								  spaceY:Number=25 )
		{
			super();
			
			if ( calendarClip == null ) this.calendarClip = new CalendarClip();
			else this.calendarClip = calendarClip;
			
			if ( day == null ) this.day = new Day();
			else this.day = day;
			
			this.startX = startX; 
			this.startY = startY; 
			this.spaceX = spaceX;
			this.spaceY = spaceY;	
			
			setLayout();
			setEvent();
			defaultSetting();
			
			makeCalendar();
			
			//_parent = this;
		}
		
		private function setLayout():void
		{
			addChild( calendarClip );
		}
		
		private function setEvent():void
		{
			//trace( calendarClip );
			calendarClip.prev_btn.addEventListener( MouseEvent.CLICK, onPrevMonth );
			calendarClip.next_btn.addEventListener( MouseEvent.CLICK, onNextMonth );
		}
		
		private function defaultSetting():void
		{
			makeDay();
		}
		
		public var now:Date = new Date();
		public var currentDate:Date = new Date();
		private function makeCalendar():void
		{
			//trace( "makeCalendar" );
			
			calendarClip.year_txt.text = currentDate.getFullYear();
			calendarClip.month_txt.text = currentDate.getMonth() + 1;
			
			removeCalendar();
			
			var tempDate:Date = new Date( currentDate.getFullYear(), currentDate.getMonth(), 1 );
			var posY:Number = startY;
			var day:Day;
			for( var i:int=1; i<32; i++ )
			{
				tempDate.setDate( i );
				if ( tempDate.getMonth() != currentDate.getMonth() ) break;
				
				day	= dayArr[ i - 1 ];
				day.index = i;
				day.text = String( i );
				day.x = startX + spaceX * tempDate.getDay();
				
				if ( tempDate.getDay() == 0 && i != 1 )
					posY += spaceY;
				
				if ( tempDate.getDay() == 0 )
					day.textColor = 0xFF0000;
				else if ( tempDate.getDay() == 6 )
					day.textColor = 0x0066FF;
				else
					day.textColor = 0x333333;
					
				if ( tempDate.toDateString() == now.toDateString() ){
					day.textColor = 0xCC0033;
				}
				
				day.addEventListener( DayEvent.DATE_SELECT, onSelect );
				
				if ( tempDate.toDateString() == selectedDate.toDateString() )
				{
					day.select();
					selectedDay = day;
				}
				else
				{
					day.reset();
				}
				
				day.y = posY;
				DisplayObjectUtil.showAfter( day, ( day.x + day.y ) /20 );
				
				calendarClip.addChild( day );
			}
		}
		
		private function removeCalendar():void
		{
			var len:int = dayArr.length;
			for( var i:int=0; i<len; i++ )
			{
				if ( calendarClip.contains( dayArr[ i ] ) == true )
					calendarClip.removeChild( dayArr[ i ] );
			}
		}
		
		private var selectedDay:Day;		
		public var selectedDate:Date = new Date();		
		private function onSelect( evt:DayEvent ):void
		{
			if ( selectedDay != null ) selectedDay.reset();
			selectedDay = evt.currentTarget as Day;
			
			selectedDate.setFullYear( currentDate.getFullYear() );
			selectedDate.setMonth( currentDate.getMonth() );
			selectedDate.setDate( evt.date );
		}
		
		private function onNextMonth( evt:MouseEvent ):void
		{
			currentDate.setMonth( currentDate.getMonth() + 1 );
			makeCalendar();
		}
		
		private function onPrevMonth( evt:MouseEvent ):void
		{
			currentDate.setMonth( currentDate.getMonth() - 1 );
			makeCalendar();
		}
		
		private var dayArr:Array = [];
		private function makeDay():void
		{
			for( var i:int = 0; i<31; i++ )
			{
				var day:Day= new ( day as Object ).constructor();
				dayArr.push( day );
			}
		}
	}
}
