﻿package sqisoft.controls.modifyClasses
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import sqisoft.events.CustomEvent;
	import sqisoft.manager.ChannelManager;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class ListItem extends Sprite
	{
		private var listItemClip:MovieClip;
		
		public function ListItem( skin:MovieClip=null )
		{
			super();
			
			if ( skin == null ) listItemClip = new ListItemClip();
			else listItemClip = skin;
			
			setLayout();
			setEvent();
			defaultSetting();
		}
		
		private function setLayout():void
		{
			addChild( listItemClip );
		}
				
		private function setEvent(): void
		{	
			listItemClip.addEventListener( MouseEvent.CLICK, onListClick );
		}
		
		private function defaultSetting():void
		{
			listItemClip.gotoAndStop(1);
			listItemClip.use_icon_mc.gotoAndStop(1);
			listItemClip.net_icon_mc.gotoAndStop(1);
			listItemClip.bg.gotoAndStop(1);
		}
				
		private function onListEnter( evt:Event ):void
		{
			if ( ChannelManager.selectedAdvtsNum == no )
			{
				listItemClip.nextFrame();
			}
			else
			{
				listItemClip.prevFrame();
			}
			
			if ( listItemClip.currentFrame == 1 || listItemClip.currentFrame == listItemClip.totalFrames )
			{
				removeEventListener( Event.ENTER_FRAME, onListEnter );
			}
		}
		
		private function onUseIconEnter( evt:Event ):void
		{
			if ( ChannelManager.selectedAdvtsNum == no )
			{
				listItemClip.use_icon_mc.nextFrame();
			}
			else
			{
				listItemClip.use_icon_mc.prevFrame();
			}
			
			if ( listItemClip.use_icon_mc.currentFrame == 1 || listItemClip.use_icon_mc.currentFrame == listItemClip.use_icon_mc.totalFrames )
			{
				removeEventListener( Event.ENTER_FRAME, onUseIconEnter );
			}
		}
		
		private function onListClick( evt:MouseEvent ):void
		{
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}
		
		public function selectedItem():void
		{
			removeEventListener( MouseEvent.CLICK, onListClick );
			updateItemlStatus();
		}
		
		public function deselectedItem():void
		{
			addEventListener( MouseEvent.CLICK, onListClick );
			updateItemlStatus();
		}
		
		public function updateItemlStatus():void
		{
			addEventListener( Event.ENTER_FRAME, onListEnter );
			addEventListener( Event.ENTER_FRAME, onUseIconEnter );
		}
		
		private var _no:Number;
		public function get no():Number
		{
			return _no;
		}
		
		public function set no( value:Number ):void
		{
			_no = value;
		}
		
		public function setList():void
		{
			channelName = ChannelManager.advtsArr[ no ].name;
			channelDesc = ChannelManager.advtsArr[ no ].desc;
			sitename = ChannelManager.advtsArr[ no ].sitename;
			networkuse = ChannelManager.advtsArr[ no ].networkuse;
						
			listItemClip.bg.gotoAndStop( 1 + ( no % 2 ) );
			listItemClip.net_icon_mc.gotoAndStop( ( networkuse == "0" ) ? "user" : "server" );
		}
				
		public function selectedAdvtsModify():String
		{
			var query:String = "";
			query += "Ch=ch" + ChannelManager.selectChannelNum + ","
			query += "ChannelName=" + channelName + ","
			query += "SiteName=" + sitename + ","
			query += "NetworkUse=" + networkuse;
			
			return query;
		}
		
		public function get channelName():String
		{
			return listItemClip.channelName_txt.text;
		}
		
		public function set channelName( value:String ):void
		{
			listItemClip.channelName_txt.text = value;
		}
		
		public function get channelDesc():String
		{
			return listItemClip.channelDesc_txt.text;
		}
		
		public function set channelDesc( value:String ):void
		{
			listItemClip.channelDesc_txt.text = value;
		}
		
		public function get sitename():String
		{
			return listItemClip.sitename_txt.text;
		}
		
		public function set sitename( value:String ):void
		{
			listItemClip.sitename_txt.text = value;
		}
		
		private var _networkuse:String;
		public function get networkuse():String
		{
			return _networkuse;
		}
		
		public function set networkuse( value:String ):void
		{
			_networkuse = value;
		}
	}
}