﻿package sqisoft.controls.modifyClasses
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import sqisoft.events.CustomEvent;
	import sqisoft.manager.ChannelManager;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class RecoveryList extends Sprite
	{
		private var recoveryListClip:MovieClip;
		
		public function RecoveryList( skin:MovieClip=null )
		{
			super();
			
			if ( skin == null ) recoveryListClip = new RecoveryListClip();
			else recoveryListClip = skin;
			
			setLayout();
			setEvent();
			defaultSetting();
		}
		
		private function setLayout():void
		{
			addChild( recoveryListClip );
		}
				
		private function setEvent(): void
		{
			if( checkValid() == true )
			{
				recoveryListClip.addEventListener( MouseEvent.CLICK, onRecoveryClipClick );
			}
			else
			{
				throw new Error( "recoveryListClip 설정한 Label명이 틀립니다" );
			}
		}
		
		private function defaultSetting():void
		{
			recoveryListClip.gotoAndStop(1);
			recoveryListClip.bg.gotoAndStop(1);
		}
		
		private var labelArr:Array = [ "deselect", "select" ];
		private var frameArr:Array = [];
		
		private function checkValid(): Boolean
		{
			var ary:Array = recoveryListClip.currentLabels;
			
			for( var i:int=0; i<labelArr.length; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
		
		private var targetFrame:int = 1;
		private function onRecoveryClipEnter( evt:Event ):void
		{
			trace( targetFrame , recoveryListClip.currentFrame );
			if ( recoveryListClip.currentFrame > targetFrame )
			{
				recoveryListClip.prevFrame();
			}
			else if ( recoveryListClip.currentFrame < targetFrame )
			{
				recoveryListClip.nextFrame();
			}
			else
			{
				recoveryListClip.removeEventListener( Event.ENTER_FRAME, onRecoveryClipEnter );
			}
		}
		
		private function onRecoveryClipClick( evt:MouseEvent ):void
		{
			selected = !selected;
			
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT, label ) );
		}
		
		private var _selected:Boolean = false;
		public function get selected():Boolean
		{
		 	return _selected;
		}
		
		public function set selected( value:Boolean ):void
		{
			_selected = value;
			updateCheckBox();
		}
		
		private function updateCheckBox():void
		{
			targetFrame = ( selected == false ) ? frameArr[ 0 ] : frameArr[ 1 ];
			recoveryListClip.addEventListener( Event.ENTER_FRAME, onRecoveryClipEnter );
		}
		
		public function setBg():void
		{
			recoveryListClip.bg.gotoAndStop( 1 + ( no % 2 ) );
		}
		
		private var _no:Number;
		public function get no():Number
		{
			return _no;
		}
		
		public function set no( value:Number ):void
		{
			_no = value;
			setBg();
		}
		
		public function get label():String
		{
			return recoveryListClip.txt.text;
		}
		
		public function set label( value:String ):void
		{
			recoveryListClip.txt.text = value;
		}
	}
}