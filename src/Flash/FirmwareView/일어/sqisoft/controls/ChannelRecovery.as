﻿package sqisoft.controls
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import sqisoft.net.FirmwareProtocol;	
	import sqisoft.controls.StepButton;
	import sqisoft.controls.modifyClasses.RecoveryList;
	import sqisoft.controls.LineScroll;	
	import sqisoft.events.CustomEvent;
	import sqisoft.events.LineScrollEvent;	
	import sqisoft.manager.ChannelManager;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class ChannelRecovery extends Sprite
	{
		private var stepClip:StepButton;
		private var recoveryClip:MovieClip;
		private var lineScroll:LineScroll;
		private var maskPanel:Sprite;
		private var recoveryPanel:Sprite;
		private var parentDialog:MovieClip;
				
		public function ChannelRecovery( _parentDialog:MovieClip, skin:MovieClip=null )
		{
			super();
			
			if ( skin == null ) recoveryClip = new RecoveryClip();
			else recoveryClip = skin;
			
			parentDialog = _parentDialog;
			
			maskPanel = new Sprite();
			recoveryPanel = new Sprite();
			
			lineScroll = new LineScroll( new LineScrollSmallClip() );
			
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}
		
		private function onStage( evt:Event ):void
		{
			setLayout();
			setEvent();
			defaultSetting();
			recoveryStatus();
		}
		
		private var stepClipArr:Array;
		private function setLayout():void
		{
			stepClipArr = [];
			
			var layoutArr:Array = [ "ok_mc", "cancel_mc", "deleteall_mc" ];
			for ( var i:int=0; i<layoutArr.length; i++ )
			{
				stepClip = new StepButton( recoveryClip[ layoutArr[ i ] ] );
				stepClip.name = layoutArr[ i ];
				
				stepClip.addEventListener( CustomEvent.CUSTOM_EVENT, onClickHandler );
				
				stepClipArr.push( stepClip );
			}
			addChild( recoveryClip );
		}
		
		private function setEvent():void
		{
			lineScroll.addEventListener( LineScrollEvent.LINE_SCROLL_CHANGE, onScrollChange );
		}	
		
		private function onCursorMouseWheel( evt:MouseEvent ):void
		{
			lineScroll.position -= evt.delta * 10 ;
		}
		
		private function defaultSetting():void
		{			
			maskPanel.graphics.beginFill( 0xFF0000 );
			maskPanel.graphics.drawRect( startX, startY, sizeW, sizeH );
			recoveryClip.addChild( maskPanel );
			recoveryClip.addChild( recoveryPanel );
			recoveryPanel.mask = maskPanel;
			
			lineScroll.x = 564;
			lineScroll.y = 188;
			recoveryClip.addChild( lineScroll );
		}
		
		private function onClickHandler( evt:CustomEvent ):void
		{
			switch ( evt.currentTarget.name )
			{
				case "ok_mc":
					sendRecoveryList();
					break;
				case "cancel_mc":
					parentDialog.closeRecoveryPopup();
					break;
				case "deleteall_mc":
					deleteRecovery();
					break;
				default:
					trace( "nothing" );
			}
		}
		
		private function recoveryStatus():void
		{
			if ( ChannelManager.recoveryArr.length > 0 )
			{
				stepClipArr[0].enabled = true;
				stepClipArr[2].enabled = true;
			}
			else
			{
				stepClipArr[0].enabled = false;
				stepClipArr[2].enabled = false;
			}
			makeRecoveryList();
		}
		
		private var startX:Number = 192;
		private var startY:Number = 188;
		private var spaceY:Number = 30;
		
		private var sizeW:Number = 380;
		private var sizeH:Number = 210;
		
		private var recoveryList:RecoveryList;
		private var selectRecoveryList:RecoveryList;
		private var selectedRecoveryList:RecoveryList;
		private var recoveryListArr:Array = [];
		public function makeRecoveryList():void
		{
			removeRecoveryList();
			
			trace( "ChannelManager.recoveryArr.length ChannelManager.recoveryArr.length : " + ChannelManager.recoveryArr.length );
			var len:int = ChannelManager.recoveryArr.length;
			for( var i:int=0; i<len; i++ )
			{
				recoveryList = new RecoveryList();
				recoveryList.no = i;
				
				recoveryList.x = startX;
				recoveryList.y = startY + spaceY * i;
				
				recoveryList.label = ChannelManager.recoveryArr[ i ];
				
				if ( !recoveryList.hasEventListener( CustomEvent.CUSTOM_EVENT ) )
					recoveryList.addEventListener( CustomEvent.CUSTOM_EVENT, onClickRecoveryList );
				
				recoveryPanel.addChild( recoveryList );
				recoveryListArr.push( recoveryList );
			}
			
			if ( !recoveryClip.contains( recoveryPanel ) )
				recoveryClip.addChild( recoveryPanel );
			updateScrollStatus();
		}
		
		private function onClickRecoveryList( evt:CustomEvent ):void
		{
			trace( "evt.target.name : " + evt.target.name );
			trace( "evt.args : " + evt.args );
		}
				
		private function removeRecoveryList():void
		{
			var len:int = recoveryListArr.length;
			for ( var i:int=0; i<len; i++ )
			{
				if ( recoveryListArr[ i ] == null ) return;
				
				if ( recoveryPanel.contains( recoveryListArr[ i ] ) == true )
				{
					recoveryPanel.removeChild( recoveryListArr[ i ] );
				}
			}
			recoveryListArr = [];
		}
		
		private function updateScrollStatus():void
		{
			lineScroll.minTargetValue = recoveryPanel.y;
			lineScroll.maxTargetValue = recoveryPanel.height - maskPanel.height;
			
			if ( recoveryPanel.height > sizeH )
			{
				lineScroll.useWheelMouse = true;
				addEventListener( MouseEvent.MOUSE_WHEEL, onCursorMouseWheel );
			}
			else
			{
				lineScroll.useWheelMouse = false;
				removeEventListener( MouseEvent.MOUSE_WHEEL, onCursorMouseWheel );
			}
		}
		
		private function onScrollChange( evt:LineScrollEvent ):void
		{
			recoveryPanel.y = -1 * evt.position;
		}
		
		private function sendRecoveryList():void
		{
			var sendList:Array = [];
			
			var len:int = recoveryListArr.length;
			for ( var i:int=0; i<len; i++ )
			{
				if ( recoveryListArr[ i ] == null ) return;
				
				if ( recoveryListArr[ i ].selected == true )
					sendList.push( recoveryListArr[ i ].label );
			}
			parentDialog.socket.send( FirmwareProtocol.FV_CH_RECOVERY + "|" + queryRecovery( sendList ) );
			parentDialog.closeRecoveryPopup();
		}
		
		private function queryRecovery( list:Array ):String
		{
			var query:String = "INIFiles=" + list.join(";");
			
			return query;
		}
		
		private function deleteRecovery():void
		{
			parentDialog.openMessagePopup( "deleteall" );
		}
		
		public function removeListPanel():void
		{
			recoveryClip.removeChild( recoveryPanel );
		}
	}
}