﻿package sqisoft.controls
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import sqisoft.net.FirmwareProtocol;	
	import sqisoft.controls.StepButton;
	import sqisoft.controls.modifyClasses.ListItem;
	import sqisoft.controls.LineScroll;	
	import sqisoft.events.CustomEvent;
	import sqisoft.events.LineScrollEvent;	
	import sqisoft.manager.ChannelManager;
	
	
	[ Event( name="customEvent", type="sqisoft.events.CustomEvent" ) ];
	public class ChannelModify extends Sprite
	{
		public static const DEFAULT_LIST_COUNT:int = 10;
		
		private var stepClip:StepButton;
		private var modifyClip:MovieClip;
		private var lineScroll:LineScroll;
		private var maskPanel:Sprite;
		private var listPanel:Sprite;
		private var parentDialog:MovieClip;
		
		public function ChannelModify( _parentDialog:MovieClip, skin:MovieClip=null )
		{
			super();
			
			if ( skin == null ) modifyClip = new ModifyClip();
			else modifyClip = skin;
			
			parentDialog = _parentDialog;
			
			maskPanel = new Sprite();
			listPanel = new Sprite();
			lineScroll = new LineScroll();
			
			addEventListener(Event.ADDED_TO_STAGE, onStage);
		}
		
		private function onStage( evt:Event ):void
		{
			setLayout();
			setEvent();
			defaultSetting();
			makeAdvtsList();
		}
		
		private function setLayout():void
		{
			var layoutArr:Array = [ "refresh", "recovery", "usb_export", "usb_import", "server_import", "delete_mc", "apply_mc", "exit_mc", "modify_mc" ];
			for ( var i:int=0; i<layoutArr.length; i++ )
			{
				stepClip = new StepButton( modifyClip[ layoutArr[ i ] ] );
				stepClip.name = layoutArr[ i ];
				
				stepClip.addEventListener( CustomEvent.CUSTOM_EVENT, onClickHandler );
			}
			addChild( modifyClip );
		}
		
		private function setEvent():void
		{
			lineScroll.addEventListener( LineScrollEvent.LINE_SCROLL_CHANGE, onScrollChange );
		}	
		
		private function onCursorMouseWheel( evt:MouseEvent ):void
		{
			lineScroll.position -= evt.delta * 10 ;
		}
		
		private function defaultSetting():void
		{			
			maskPanel.graphics.beginFill( 0xFF0000 );
			maskPanel.graphics.drawRect( startX, startY, sizeW, sizeH );
			modifyClip.addChild( maskPanel );
			modifyClip.addChild( listPanel );
			listPanel.mask = maskPanel;
			
			lineScroll.x = 725;
			lineScroll.y = 233;
			modifyClip.addChild( lineScroll );
		}
		
		private function onClickHandler( evt:CustomEvent ):void
		{
			switch ( evt.currentTarget.name )
			{
				case "refresh":
					parentDialog.socket.send( FirmwareProtocol.FV_REFRESH );
					break;
				case "recovery":
					parentDialog.socket.send( FirmwareProtocol.FV_QUERY_RECOVERY );
					break;
				case "usb_export":
					parentDialog.socket.send( FirmwareProtocol.FV_EXPORT );
					break;
				case "usb_import":
					parentDialog.socket.send( FirmwareProtocol.FV_IMPORT );
					break;
				case "server_import":
					parentDialog.openSchedulePopup( "import" );
					break;
				case "modify_mc": // Reload버튼
					if ( ChannelManager.selectedAdvtsNum < 0 )
						parentDialog.openMessagePopup( "advts" );
					else
						//trace("querySyncSchedule::" + querySyncSchedule() );
						parentDialog.socket.send( "FV_CH_DOWNLOAD" + "|" + querySyncSchedule() );
					//parentDialog.openSchedulePopup( "modify" );
					break;
				case "delete_mc":
					channelDelete();
					break;				
				case "apply_mc":
					channelModifyApply();
					break;
				case "exit_mc":
					trace("팝업 exit");
					parentDialog.closeModifyPopup();
					break;
				default:
					trace( "nothing" );
			}
		}
		
		private var startX:Number = 40;
		private var startY:Number = 233;
		private var spaceY:Number = 30;
		
		private var sizeW:Number = 685;
		private var sizeH:Number = 300;
		
		private var oldListPannelH:Number;
		
		private var listItem:ListItem;
		private var selectAdvtsListItem:ListItem;
		private var selectedAdvtsListItem:ListItem;
		private var listItemArr:Array = [];
		public function disabled():void
		{
			trace(stepClip.name);
			stepClip.disabled();
		}
		public function makeAdvtsList():void
		{
			removeAdvtsList();
			
			var len:int = ChannelManager.advtsArr.length;
			for( var i:int=0; i<len; i++ )
			{
				listItem = new ListItem();
				listItem.no = i;
				
				listItem.x = startX;
				listItem.y = startY + spaceY * i;
				
				listItem.setList();
				if ( !listItem.hasEventListener( CustomEvent.CUSTOM_EVENT ) )
					listItem.addEventListener( CustomEvent.CUSTOM_EVENT, onClickAdvtsList );
				
				listPanel.addChild( listItem );
				listItemArr.push( listItem );
			}
			oldListPannelH = listPanel.height;
			modifyClip.addChild( listPanel );
			updateScrollStatus( len );
			
			if ( ChannelManager.selectedAdvtsNum < 0 ) return;
			listItemArr[ ChannelManager.selectedAdvtsNum ].dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}
		
		private function removeAdvtsList():void
		{
			for ( var i:int=0; i<listItemArr.length; i++ )
			{
				if ( listItemArr[ i ] == null ) return;
				
				if ( listPanel.contains( listItemArr[ i ] ) == true )
					listPanel.removeChild( listItemArr[ i ] );
			}
			listItemArr = [];
		}
		
		private function onClickAdvtsList( evt:CustomEvent ):void
		{
			if ( selectedAdvtsListItem != null ) selectedAdvtsListItem.deselectedItem();
			selectAdvtsListItem = evt.target as ListItem;
			selectAdvtsListItem.selectedItem();
			
			ChannelManager.selectedAdvtsNum = selectAdvtsListItem.no;
			channelName = selectAdvtsListItem.channelName;
			
			if ( selectedAdvtsListItem == selectAdvtsListItem )
			{
				ChannelManager.selectedAdvtsNum = -1;
				channelName = "";
				selectedAdvtsListItem = null;
			}
			else
			{
				selectedAdvtsListItem = selectAdvtsListItem;
			}
		}
		
		private function updateScrollStatus( len:int ):void
		{
			if ( len <= ChannelModify.DEFAULT_LIST_COUNT )
			{
				if ( this.hasEventListener( MouseEvent.MOUSE_WHEEL ) )
					removeEventListener( MouseEvent.MOUSE_WHEEL, onCursorMouseWheel );
				lineScroll.useWheelMouse = false;
			}
			else
			{
				if ( !this.hasEventListener( MouseEvent.MOUSE_WHEEL ) )
					addEventListener( MouseEvent.MOUSE_WHEEL, onCursorMouseWheel );
				lineScroll.useWheelMouse = true;
			}			
			lineScroll.minTargetValue = 0;
			lineScroll.maxTargetValue = listPanel.height - maskPanel.height;
			lineScroll.position = lineScroll.position + ( oldListPannelH - listPanel.height );
		}
		
		private function onScrollChange( evt:LineScrollEvent ):void
		{
			listPanel.y = -1 * evt.position;
		}
		
		public function blankAdvtsModify():String
		{
			var query:String = "";
			query += "Ch=ch" + ChannelManager.selectChannelNum + ","
			query += "ChannelName=,"
			query += "SiteName=," 
			query += "NetworkUse=";
			
			return query;
		}
		
		private function channelModifyApply():void
		{
			if ( channelName != "" && checkOverlap() == true )
			{
				parentDialog.openMessagePopup( "registered" );
				return;
			}
			
			var query:String;
			if ( ChannelManager.selectedAdvtsNum < 0 )
				query = blankAdvtsModify();
			else
				query = listItemArr[ ChannelManager.selectedAdvtsNum ].selectedAdvtsModify();
			parentDialog.socket.send( FirmwareProtocol.FV_CH_MODIFY + "|" + query );
			parentDialog.updateChannelStatus();
		}
		
		private function checkOverlap():Boolean
		{
			var len:int = ChannelManager.channelArr.length;
			for( var i:int=0; i<len; i++ )
			{
				if ( ChannelManager.channelArr[ i ].set_ad == channelName ) return true;
			}
			return false;
		}
		
		private function channelDelete():void
		{
			if ( ChannelManager.selectedAdvtsNum < 0 )
			{
				parentDialog.openMessagePopup( "advts" );
				return;
			}
			
			var len:int = ChannelManager.currentChannelInfo.length;
			for ( var i:int=0; i<len; i++ )
			{
				trace( "ChannelManager.currentChannelInfo[ i ].currentChannelName : " + ChannelManager.currentChannelInfo[ i ].currentChannelName );
				if ( ChannelManager.currentChannelInfo[ i ].currentChannelName == channelName )
				{
					parentDialog.openMessagePopup( "current" );
					return;
				}
				
				if ( ChannelManager.currentChannelInfo[ i ].autoChannelName == channelName )
				{
					parentDialog.openMessagePopup( "auto" );
					return;
				}
			}			
			parentDialog.openMessagePopup( "delete" );
		}
		
		private function querySyncSchedule():String
		{
			trace( "ChannelManager.selectChannelNum ::::::::::::: " + ChannelManager.selectChannelNum );
			
			var query:String = "";
			query += "Ch=0,"
			query += "ChannelName=" + selectAdvtsListItem.channelName + ","
			query += "SiteName=" + selectAdvtsListItem.sitename + ","
			query += "NetworkUse=1,"
			query += "Display=1" ;
			
			return query;
		}
				
		private var _channelNum:String;
		public function get channelNum():String
		{
			return _channelNum;
		}
		
		public function set channelNum( value:String ):void
		{
			_channelNum = value;
			modifyClip.channelNum_txt.text = "Ch" + value;
		}
		
		public function get channelName():String
		{
			return modifyClip.channelName_txt.text;
		}
		
		public function set channelName( value:String ):void
		{
			modifyClip.channelName_txt.text = value;
		}
	}
}