﻿package kr.umhannum.utils
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.utils.getQualifiedClassName;

	public class ClassUtil
	{
		public static function getDisplayObjectAssets(container:DisplayObjectContainer, linkageName:String):Array
		{
			var tempArray:Array = [];
			var len:int = container.numChildren;
			for ( var i:int=0; i<len; i++ )
			{
				var displayObject:DisplayObject = container.getChildAt(i) as DisplayObject;
				var className:String = getQualifiedClassName(displayObject);
				if (className.indexOf(linkageName) > -1)
				{
					tempArray.push({name:displayObject.name, displayObject:displayObject});
				}
			}
			return tempArray.sortOn("name");
		}
	}
}