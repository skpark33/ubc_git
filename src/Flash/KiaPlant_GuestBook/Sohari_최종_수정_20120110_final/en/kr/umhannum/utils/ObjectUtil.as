package kr.umhannum.utils
{
	public class ObjectUtil
	{
		// xml의 속성을 오브젝트로 반환하는 함수
		public static function getAttributesObject(xmlList:XMLList):Object
		{
			var attributesInfo:Object = {};
			var attributesList:XMLList = xmlList as XMLList;
			var len:int = attributesList.length();
			trace("len : " + len);
			for ( var i:int=0; i<len; i++ )
			{
				var attributes_obj:Object = attributesList[i];
				attributesInfo[attributes_obj.name().toString()] = attributes_obj.toString();
			}
			return attributesInfo;
		}
	}
}