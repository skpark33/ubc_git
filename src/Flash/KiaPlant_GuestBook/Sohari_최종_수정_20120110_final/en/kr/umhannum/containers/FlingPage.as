﻿package kr.umhannum.containers
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.BlurFilter;
	
	import kr.umhannum.core.SimpleComponent;
	import kr.umhannum.events.FlingPageEvent;
	import kr.umhannum.events.ImageLoaderEvent;
	import kr.umhannum.manager.DepthManager;
	import kr.umhannum.net.ImageLoader;
	
	[Event( name="select", type="flash.events.FlingPageEvent")]
	public class FlingPage extends SimpleComponent
	{
		public static const THUMBNAIL_WIDTH:int = 143;
		public static const THUMBNAIL_HEIGHT:int = 169;
		
		//public static const LIST_FILE_PATH:String = "c:/sqisoft/contents/enc/Plugins/Wedding/GuestBook/KIA_GUESTBOOK/";
		public static const LIST_FILE_PATH:String = "./Plugins/Wedding/GuestBook/KIA_GUESTBOOK/";
		
		public var targetX:Number;
		public var originalX:Number;
		public function FlingPage(position:XMLList)
		{
			super();
			
			this.position = position;
			
			initialize();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			createFlingPageBounds();
		}
		
		private function createFlingPageBounds():void
		{
			var background:Sprite = new Sprite();
			background.graphics.beginFill(0xffffff, 0);
			background.graphics.drawRect(0, 0, guestbook.STAGE_WIDTH, guestbook.STAGE_HEIGHT);
			background.graphics.endFill();
			this.addChild(background);
		}
		
		private function createImage():void
		{
			var len:int = data.length();
			trace("len : " + len);
			for ( var i:int=0; i<len; i++ )
			{
				var imageLoader:ImageLoader = new ImageLoader();
				imageLoader.x = position[i].@x;
				imageLoader.y = position[i].@y;
				imageLoader.no = i;
				
				imageLoader.load(LIST_FILE_PATH+data[i]);
				imageLoader.doubleClickEnabled = true;
				imageLoader.addEventListener(MouseEvent.CLICK, clickHandler);
				imageLoader.addEventListener(MouseEvent.DOUBLE_CLICK, doubleClickHandler);
				imageLoader.addEventListener(ImageLoaderEvent.DATA_COMPLETE, dataCompleteHandler);
				this.addChild(imageLoader);
			}
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			trace("clickHandler");
			var target:ImageLoader = event.currentTarget as ImageLoader;
			DepthManager.bringToTop(target);
			
			this.dispatchEvent(new FlingPageEvent(FlingPageEvent.SELECT, this.no, target.no));
		}
		
		private function doubleClickHandler(event:MouseEvent):void
		{
			trace("doubleClickHandler");
		}
		
		private function dataCompleteHandler(event:ImageLoaderEvent):void
		{
			//trace("dataCompleteHandler : ");
			var imageLoader:ImageLoader = event.currentTarget as ImageLoader;
			imageLoader.width = FlingPage.THUMBNAIL_WIDTH;
			imageLoader.height = FlingPage.THUMBNAIL_HEIGHT;
			imageLoader.rotation = position[imageLoader.no].@rotation;
		}
		
		private var _data:XMLList;
		public function get data():XMLList
		{
			return _data;
		}

		public function set data(value:XMLList):void
		{
			_data = value;
			
			createImage();
		}
		
		private var _position:XMLList;
		public function get position():XMLList
		{
			return _position;
		}

		public function set position(value:XMLList):void
		{
			_position = value;
			
			//trace("position value : " + value);
		}
		
		private var _blurred:Boolean = false;
		/**
		 * 필터 적용 여부
		 * @return 
		 * 
		 */
		public function get blurred():Boolean
		{
			return _blurred;
		}
		
		public function set blurred(value:Boolean):void
		{
			if (_blurred && !value)
				this.filters = [];
			_blurred = value;
			
			if (value)
			{
				trace("블러");
				this.filters = [new BlurFilter()];
			}
		}
	}
}