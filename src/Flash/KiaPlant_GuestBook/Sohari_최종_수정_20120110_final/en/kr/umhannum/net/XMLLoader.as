﻿package kr.umhannum.net
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import kr.umhannum.events.XMLLoaderEvent;
	import kr.umhannum.utils.ExternalInterfaceUtil;
	
	
	[Event(name="dataComplete", type="kr.umhannum.events.XMLLoaderEvent")]
	public class XMLLoader extends EventDispatcher
	{
		private var urlLoader:URLLoader = new URLLoader();
		private var urlRequest:URLRequest = new URLRequest();
		
		public function XMLLoader()
		{
			super();
		}
		
		public function load(url:String):void
		{
			urlRequest.url = url;
			urlLoader.addEventListener(Event.COMPLETE, completeHandler);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			urlLoader.load(urlRequest);
		}
		
		private function completeHandler(event:Event):void
		{
			var xml:XML;			
			try
			{
				xml = new XML(urlLoader.data);
			}
			catch (event:Error)
			{
				xml = <error></error>;
			}			
			this.dispatchEvent(new XMLLoaderEvent(XMLLoaderEvent.DATA_COMPLETE, xml));
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void
		{
			trace("ioErrorHandler: " + event.text);
			ExternalInterfaceUtil.externalInterface("alert", event.toString());
			ExternalInterfaceUtil.externalInterface("alert", event.text);
		}
	}
}
