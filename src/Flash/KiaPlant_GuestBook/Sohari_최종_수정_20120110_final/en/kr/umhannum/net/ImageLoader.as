﻿package kr.umhannum.net
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	
	import kr.umhannum.events.ImageLoaderEvent;
	
	
	[Event( name="progress", type="flash.events.ProgressEvent")]
	[Event( name="dataComplete", type="sqisoft.events.ImageLoaderEvent")]
	public class ImageLoader extends MovieClip
	{
		public function ImageLoader()
		{
			super();
			
			configureListeners(loader.contentLoaderInfo);
		}
		
		private function configureListeners(dispatcher:IEventDispatcher):void
		{
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			dispatcher.addEventListener(Event.UNLOAD, unloadHandler);
		}
		
		public function load(url:String, checkPolicyFile:Boolean=false):void
		{
			loader.unload();
			
			var urlRequest:URLRequest = new URLRequest();
			urlRequest.url = url;
			
			var context:LoaderContext = new LoaderContext();
			context.applicationDomain = ApplicationDomain.currentDomain;
			context.checkPolicyFile = checkPolicyFile;
			
			loader.load(urlRequest, context);
		}
		
		public function unload():void
		{
			loader.unload();
		}
		
		protected function completeHandler(event:Event):void
		{
			//trace("completeHandler");
			var image:Bitmap = loader.content as Bitmap;
			image.smoothing = true;
			dispatchEvent(new ImageLoaderEvent(ImageLoaderEvent.DATA_COMPLETE, image));
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void
		{
			trace("ioErrorHandler: " + event);
		}
		
		protected function progressHandler(event:ProgressEvent):void
		{
			dispatchEvent(event.clone());
		}
		
		protected function unloadHandler(event:Event):void
		{
			loader.unload();
		}
		
		public function get contentLoaderInfo():LoaderInfo
		{
			return loader.contentLoaderInfo;
		}
		
		public function get content():DisplayObject
		{
			return loader.content;
		}
		
		private var _loader:Loader;
		public function get loader():Loader
		{
			if (!_loader)
			{
				_loader = new Loader();
				this.addChild(_loader);
			}
			return _loader;
		}
		
		private var _no:int;
		public function get no():int
		{
			return _no;
		}
		
		public function set no(value:int):void
		{
			_no = value;
		}
	}
}
