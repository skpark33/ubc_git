package kr.umhannum.events
{
	import flash.events.Event;
	
	public class TweenStatusEvent extends Event
	{
		public static const PLAY:String = "play";
		public static const STOP:String = "stop";
		public static const UPDATE:String = "update";
		public static const COMPLETE:String = "complete";
		public function TweenStatusEvent(type:String, status:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_status = status;
		}
		
		override public function clone():Event
		{
			return new TweenStatusEvent(type, status, bubbles, cancelable); 
		}
		
		private var _status:String;
		public function get status():String
		{
			return _status;
		}
	}
}