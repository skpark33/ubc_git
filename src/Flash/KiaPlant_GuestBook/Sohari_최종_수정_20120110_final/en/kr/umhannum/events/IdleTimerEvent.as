package kr.umhannum.events
{
	import flash.events.Event;
	
	public class IdleTimerEvent extends Event
	{
		public static const TIMER_COMPLETE:String = "timerComplete";
		
		public function IdleTimerEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new IdleTimerEvent(type, bubbles, cancelable);
		}
	}
}