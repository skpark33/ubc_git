package kr.umhannum.events
{
	import flash.events.Event;
	
	public class StopWatchEvent extends Event
	{
		public static const COMPLETE:String = "complete";
		
		public function StopWatchEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new StopWatchEvent(type, bubbles, cancelable);
		}
	}
}