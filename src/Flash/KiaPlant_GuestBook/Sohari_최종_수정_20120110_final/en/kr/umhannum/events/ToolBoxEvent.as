package kr.umhannum.events
{
	import flash.events.Event;
	
	public class ToolBoxEvent extends Event
	{
		public static const TOOL_TYPE:String = "toolType";
		public static const PEN_COLOR:String = "penColor";
		public static const PEN_SIZE:String = "penSize";
		public static const ERASER_SIZE:String = "eraserSize";
		public function ToolBoxEvent(type:String, no:int, value:uint, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_no = no;
			_value = value;
		}
		
		override public function clone():Event
		{
			return new ToolBoxEvent(type, no, value, bubbles, cancelable);
		}
		
		private var _no:int;
		public function get no():int
		{
			return _no;
		}
		
		private var _value:uint;
		public function get value():uint
		{
			return _value;
		}
	}
}