package kr.umhannum.controls.toolBoxClasses
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import kr.umhannum.events.ToolBoxEvent;
	
	[Event(name="penSize", type="kr.umhannum.events.ToolBoxEvent")]
	public class ToolBoxPenSizeItem extends MovieClip
	{
		public function ToolBoxPenSizeItem()
		{
			super();
			
			configureListeners();
			commitProperties();
		}
		
		private function configureListeners():void
		{
			this.addEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		private function commitProperties():void
		{
			this.stop();
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			parent.dispatchEvent(new ToolBoxEvent(ToolBoxEvent.PEN_SIZE, no, size));
		}
		
		private var _no:Number = 1;
		public function get no():Number
		{
			return _no;
		}
		
		[Inspectable(name="no", type="Number", defaultValue=1)]
		public function set no(value:Number):void
		{
			_no = value;
			
			this.gotoAndStop(value);
		}
		
		private var _size:uint = 4;
		public function get size():uint
		{
			return _size;
		}
		
		[Inspectable(name="size", type="Number", defaultValue=4)]
		public function set size(value:uint):void
		{
			_size = value;
		}
	}
}