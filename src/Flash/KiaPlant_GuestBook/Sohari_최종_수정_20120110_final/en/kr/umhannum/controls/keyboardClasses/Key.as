package kr.umhannum.controls.keyboardClasses
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import kr.umhannum.events.KeyEvent;
	
	
	[Event(name="key", type="kr.umhannum.events.KeyEvent")]
	public class Key extends MovieClip
	{
		public var txt:TextField;
		public function Key()
		{
			super();
			
			configureListeners();
			commitProperties();
		}
		
		private function configureListeners():void
		{
			this.addEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		private function commitProperties():void
		{
			this.stop();
			
			txt.mouseEnabled = false;
			txt.mouseWheelEnabled = false;
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			parent.dispatchEvent(new KeyEvent(KeyEvent.KEY, value));
		}
		
		private var _value:String;
		public function get value():String
		{
			return _value;
		}
		
		[Inspectable(name="value", type="String")]
		public function set value(value:String):void
		{
			_value = value;
			
			txt.text = value;
		}
	}
}