package kr.umhannum.controls
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	
	import kr.umhannum.core.SimpleComponent;
	import kr.umhannum.events.KeyEvent;
	
	[Event(name="send", type="kr.umhannum.events.KeyEvent")]
	public class Keyboard extends SimpleComponent
	{
		public var id_txt:TextField;
		public var id_mc:MovieClip;
		public var addr_txt:TextField;
		public var addr_mc:MovieClip;
		public var close_btn:SimpleButton;
		public var delete_btn:SimpleButton;
		public var send_btn:SimpleButton;
		
		private var target_txt:TextField;
		public function Keyboard()
		{
			super();
		}
		
		override protected function configureListeners(dispatcher:IEventDispatcher=null):void
		{
			super.configureListeners(dispatcher);
			
			id_txt.addEventListener(MouseEvent.CLICK, clickHandler);
			addr_txt.addEventListener(MouseEvent.CLICK, clickHandler);
			close_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			delete_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			send_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			
			this.addEventListener(KeyEvent.KEY, keyHandler);
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			id();
		}
		
		private function id():void
		{
			target_txt = id_txt;
			id_mc.filters = [glowFilter];
			addr_mc.filters = [];
		}
		
		private function addr():void
		{
			target_txt = addr_txt;
			id_mc.filters = [];
			addr_mc.filters = [glowFilter];
		}
		
		private function keyHandler(event:KeyEvent):void
		{
			trace("event.value : " + event.value);
			
			trace("target_txt : " + target_txt);
			trace(event.value.toLowerCase());
			target_txt.appendText(event.value.toLowerCase());			
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			var expression:String = String(event.currentTarget.name).split("_")[0];
			switch (expression)
			{
				case "id":
					trace("메일 아이디");
					id();
					break;
				case "addr":
					trace("메일 주소");
					addr();
					break;
				case "close":
					trace("닫기");
					this.dispatchEvent(new Event(Event.CLOSE));
					break;
				case "delete":
					trace("삭제");
					target_txt.replaceText(target_txt.length-1, target_txt.length, "");
					break;
				case "send":
					trace("메일 보내기");
					var mailAddress:String = id_txt.text + "@" + addr_txt.text;
					trace("mailAddress : " + mailAddress);
					this.dispatchEvent(new KeyEvent(KeyEvent.SEND, mailAddress));
					break;
				default:
					break;
			}
		}
		
		private var _glowFilter:GlowFilter;
		public function get glowFilter():GlowFilter
		{
			if (!_glowFilter)
			{
				_glowFilter = new GlowFilter();
				_glowFilter.blurX = 8;
				_glowFilter.blurY = 8;
				_glowFilter.color = 0x106AB0;
				_glowFilter.strength = 4;
				//_glowFilter.color = 0xffffff;
			}
			return _glowFilter;
		}
	}
}