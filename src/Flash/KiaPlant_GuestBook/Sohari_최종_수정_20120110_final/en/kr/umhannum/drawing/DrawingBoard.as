﻿package kr.umhannum.drawing
{
	import ascb.drawing.Pen;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.ui.Mouse;
	
	import kr.umhannum.core.SimpleComponent;
	
	public class DrawingBoard extends SimpleComponent
	{
		public static const PEN_SIZE:uint = 4;
		public static const PEN_COLOR:uint = 0;
		public static const ERASER_SIZE:uint = 4;
		public static const ERASER_COLOR:uint = 0xf1f1f1;
		
		private var ready:Boolean;
		
		public var board_mc:MovieClip;
		public var drawingTool:DrawingTool;
		
		public var isHideTool:Boolean;
		
		private var pen:Pen;
		private var canvas:Sprite;
		
		private var boundary:Rectangle;
				
		public function DrawingBoard()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			if (!canvas)
			{
				canvas = new Sprite();
				canvas.graphics.beginFill(0xffffff, 0);
				trace( "board_mc.width : " + board_mc.width + "board_mc.height : " + board_mc.height );
				canvas.graphics.drawRect(0, 0, board_mc.width, board_mc.height);
				canvas.graphics.endFill();
				board_mc.addChild(canvas);
			}
			
			if (!pen)
			{
				pen = new Pen(canvas.graphics);
			}
		}
		
		override protected function configureListeners(dispatcher:IEventDispatcher=null):void
		{
			super.configureListeners(dispatcher);
			
			board_mc.addEventListener(Event.MOUSE_LEAVE, mouseLeaveHandler);
			board_mc.addEventListener(MouseEvent.ROLL_OUT, rollOutHandler);
			board_mc.addEventListener(MouseEvent.ROLL_OVER, rollOverHandler);
			board_mc.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			
			configureStageListeners();
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			boundary = board_mc.getBounds(this);
			
			lineStyleUpdate();
		}
		
		/**
		 * 스테이지에 이벤트를 등록하는 함수 
		 * 
		 */		
		private function configureStageListeners():void
		{
			stage.addEventListener(Event.MOUSE_LEAVE, stageMouseLeaveHandler);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, stageMouseDownHandler);
			stage.addEventListener(MouseEvent.MOUSE_UP, stageMouseUpHandler);
		}
		
		/**
		 * 스테이지에 이벤트를 제거하는 함수 
		 * 
		 */		
		private function removeStageListeners():void
		{
			stage.removeEventListener(Event.MOUSE_LEAVE, stageMouseLeaveHandler);
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, stageMouseDownHandler);
			stage.removeEventListener(MouseEvent.MOUSE_UP, stageMouseUpHandler);
		}
		
		/**
		 * 스테이지에 Event.MOUSE_LEAVE 이벤트의 핸들러 함수 
		 * 마우스가 스테이지 밖으로 나갔을 때 발생하는 이벤트
		 * @param event
		 * 
		 */		
		private function stageMouseLeaveHandler(event:Event):void
		{
			trace("stageMouseLeaveHandler");
			ready = false;
			drawEnd();
		}

		/**
		 * 스테이지에 MouseEvent.MOUSE_DOWN 이벤트의 핸들러 함수 
		 * @param event
		 * 
		 */		
		private function stageMouseDownHandler(event:MouseEvent):void
		{
			ready = true;
		}
		
		/**
		 * 스테이지에 MouseEvent.MOUSE_UP 이벤트의 핸들러 함수 
		 * @param event
		 * 
		 */		
		private function stageMouseUpHandler(event:MouseEvent):void
		{
			trace("stageMouseUpHandler");
			ready = false;
			drawEnd();
		}
		
		private function mouseDownHandler(event:MouseEvent):void
		{
			ready = true;
			drawBegin();
		}
		
		private function mouseLeaveHandler(event:Event):void
		{
			ready = false;
			drawEnd();
		}
		
		private function drawBegin():void
		{
			board_mc.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			
			pen.moveTo(board_mc.mouseX, board_mc.mouseY);
		}
		
		private function drawEnd():void
		{
			board_mc.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
		}
		
		private function draw():void
		{
			trace("drawing");
			pen.lineTo(board_mc.mouseX, board_mc.mouseY);
		}
		
		private function mouseMoveHandler(event:MouseEvent):void
		{
			trace("mouseMoveHandler");
			
			draw();
		}
		
		private function rollOutHandler(event:MouseEvent):void
		{
			trace("rollOutHandler");
			// trace("ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ");
			Mouse.show();
			drawingTool.visible = false;
			drawingTool.removeEventListener(Event.ENTER_FRAME, toolEnterFrameHandler);
			
			if (ready) drawEnd();
				
		}
		
		private function rollOverHandler(event:MouseEvent):void
		{
			trace("rollOverHandler");
			// trace("ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ");
			Mouse.hide();
			drawingTool.visible = isHideTool ? false : true;
			drawingTool.addEventListener(Event.ENTER_FRAME, toolEnterFrameHandler);
			
			if (ready) drawBegin();
		}
		
		private function toolEnterFrameHandler(event:Event):void
		{
			drawingTool.x = board_mc.mouseX;
			drawingTool.y = board_mc.mouseY;
			
			checkBoundary();
		}
		
		private function checkBoundary():void
		{
			trace("boundary : " + boundary);
			//trace(mouseX, mouseY);
			
			if (mouseX < boundary.x || mouseX > boundary.width)
			{
				trace("좌우 경계 밖임");
				drawEnd();
			}
			
			if (mouseY < boundary.y || mouseY > boundary.height)
			{
				trace("상하 경계 밖임");
				drawEnd();
			}
		}
		
		private var _color:uint = DrawingBoard.PEN_COLOR;
		public function color(no:int, color:uint):void
		{
			_color = color;
			
			drawingTool.colorType = no;
			
			lineStyleUpdate();
		}
		
		public function clear():void
		{
			pen.clear();
			
			lineStyleUpdate();
		}
		
		public function lineStyleUpdate():void
		{
			if (toolType == DrawingToolType.PEN)
				pen.lineStyle(penSize, _color);
			else
				pen.lineStyle(eraserSize, DrawingBoard.ERASER_COLOR);
		}
		
		private var _penSize:int = DrawingBoard.PEN_SIZE;
		public function get penSize():int
		{
			return _penSize;
		}
		
		public function set penSize(value:int):void
		{
			_penSize = value;
			
			lineStyleUpdate();
		}
		
		private var _eraserSize:int = DrawingBoard.ERASER_SIZE;
		public function get eraserSize():int
		{
			return _eraserSize;
		}
		
		public function set eraserSize(value:int):void
		{
			_eraserSize = value;
			
			lineStyleUpdate();
		}
		
		private var _toolType:String = DrawingToolType.PEN;
		public function get toolType():String
		{
			return _toolType;
		}

		public function set toolType(value:String):void
		{
			_toolType = value;
			drawingTool.toolType = value;
			
			lineStyleUpdate();
		}
	}
}
