package kr.umhannum.media
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.ActivityEvent;
	import flash.events.IEventDispatcher;
	import flash.media.Camera;
	import flash.media.Video;
	
	public class SnapshotCamera extends Sprite
	{
		public static const CAMERA_WIDTH:int = 640;
		public static const CAMERA_HEIGHT:int = 480;
		public static const CAMERA_FPS:int = 30;
		
		private var camera:Camera;
		
		public var video_mc:MovieClip;
		private var fps:int;
		public function SnapshotCamera()
		{
			super();
			
			initializeCamera();
		}
		
		private function initializeCamera():void
		{
			if (!camera)
			{
				camera = Camera.getCamera();
				if (null == camera)
				{
					throw new Error("웹캠을 초기화 할 수 없습니다. 웹캠이 필요합니다.");
				}
				else
				{
					camera.setMode(SnapshotCamera.CAMERA_WIDTH, SnapshotCamera.CAMERA_HEIGHT, SnapshotCamera.CAMERA_FPS);
					camera.addEventListener(ActivityEvent.ACTIVITY,activityHandler);
					
					video_mc.video.attachCamera(camera);
				}
			}
		}
		
		private function createVideo():void
		{
			
		}
		
		private function activityHandler(event:ActivityEvent):void
		{
			trace("activityHandler: " + event);
		}
	}
}