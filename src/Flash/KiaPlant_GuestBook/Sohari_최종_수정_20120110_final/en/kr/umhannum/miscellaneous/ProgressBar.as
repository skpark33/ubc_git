package kr.umhannum.miscellaneous
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public class ProgressBar extends MovieClip
	{
		[Embed(source="/assets/Skin.swf", symbol="ProgressBar_Skin")]
		private var ProgressBar_Skin:Class;
		
		private var backCover:Sprite;
		private var eventListenerStack:Array;
		private var initialized:Boolean;
		public function ProgressBar()
		{
			super();
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler, false, 0, true);
			addEventListener(Event.REMOVED_FROM_STAGE, removeFromStageHandler, false, 0, true);
		}
		
		/**
		 * Event.ADDED_TO_STAGE 이벤트 핸들러
		 * @param event
		 * 
		 */		
		private function addedToStageHandler(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			
			stageInitialize();
		}
		
		/**
		 * Event.REMOVED_FROM_STAGE 이벤트 핸들러
		 * @param event
		 * 
		 */		
		private function removeFromStageHandler(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, removeFromStageHandler);
		}
		
		public final function initialize():void
		{
			if (!initialized)
			{
				initialized = true;
				
				createChildren();
				commitProperties();
				configureListeners();
				
				createBackCover();
				createProgressBar();
			}
		}
		
		private function createChildren():void
		{
		}
		
		private function commitProperties():void
		{
			
		}
		
		private function configureListeners(dispatcher:IEventDispatcher=null):void
		{
			
		}
		
		private function removeListeners(dispatcher:IEventDispatcher=null):void
		{
			
		}
		
		/**
		 * 스테이지에 붙고 나서 실행 되는 함수 
		 * @param event
		 * 
		 */		
		private function stageInitialize():void
		{
			initialize();
		}
		
		private function createBackCover():void
		{
			var backCover:Sprite = new Sprite();
			backCover.x = -1 * this.x;
			backCover.y = -1 * this.y;
			backCover.graphics.beginFill(0x000000, 0.5);
			backCover.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			backCover.graphics.endFill();
			this.addChild(backCover);
		}
		
		private function createProgressBar():void
		{
			var skinAssets:Sprite = new ProgressBar_Skin() as Sprite;
			var movieClip:MovieClip = skinAssets.getChildByName("assets") as MovieClip;
			trace("movieClip : " + movieClip);
			movieClip.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
			this.addChild(movieClip);
		}
		
		private function enterFrameHandler(event:Event):void
		{
			trace("enterFrameHandler");
			
			var target:MovieClip = event.currentTarget as MovieClip;
			target.nextFrame();
			if (target.currentFrame == target.totalFrames)
			{
				trace("끝");
				target.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		
		public function dispose():void
		{
			var len:int = this.numChildren;
			while (len > 0)
			{
				this.removeChild(this.getChildAt(0));
				len--;
			}
			
			if (eventListenerStack)
			{
				while (eventListenerStack.length > 0)
				{
					var obj:Object = eventListenerStack.shift();
					this.removeEventListener(obj.type, obj.listener, obj.useCapture);
				}
				eventListenerStack = null;
			}
		}
	}
}















