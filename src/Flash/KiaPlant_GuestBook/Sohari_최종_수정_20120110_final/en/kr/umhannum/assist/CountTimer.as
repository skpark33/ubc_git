﻿package kr.umhannum.assist
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class CountTimer extends Timer
	{
		private var _parameters:Object;
		private var _timerFunc:Function;
		private var _completeFunc:Function;
		public function CountTimer(delay:Number, repeatCount:int=0, timerFunc:Function=null, completeFunc:Function=null, parameters:Object=null)
		{
			super(delay, repeatCount);
			
			_timerFunc = timerFunc;
			_completeFunc = completeFunc;
			_parameters = parameters;
			
			configureListeners();
		}
		
		private function configureListeners():void
		{
			this.addEventListener(TimerEvent.TIMER, timerHandler, false, 0, true);
			this.addEventListener(TimerEvent.TIMER_COMPLETE, timerCompleteHandler, false, 0, true);
		}
		
		private function timerHandler(event:TimerEvent):void
		{
			if (null == _timerFunc) return;
			
			if (null == _parameters)
				_timerFunc();
			else
				_timerFunc(_parameters);
		}
		
		private function timerCompleteHandler(event:TimerEvent):void
		{
			if (null == _completeFunc) return;
			
			if (null == _parameters)
				_completeFunc();
			else
				_completeFunc(_parameters);
		}
		
		public function removeListeners():void
		{
			this.removeEventListener(TimerEvent.TIMER, timerHandler, false);
			this.removeEventListener(TimerEvent.TIMER_COMPLETE, timerCompleteHandler, false);
		}
	}
}