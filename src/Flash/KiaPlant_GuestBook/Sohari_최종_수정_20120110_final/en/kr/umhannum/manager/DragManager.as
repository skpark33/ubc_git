﻿package kr.umhannum.manager
{
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilter;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	
	public class DragManager extends EventDispatcher
	{
		private var _instance:MovieClip;
		private var _handle:MovieClip;
		public function DragManager(instance:MovieClip, handle:MovieClip=null)
		{
			super();
			
			_instance = instance;
			
			if (null == handle)
				_handle = instance;
			else
				_handle = handle;
			
			commitProperties();
		}
		
		private function commitProperties():void
		{
			_handle.buttonMode = true;
		}
		
		/**
		 * 드래그 객체의 핸들에 이벤트를 등록하는 함수
		 * 
		 */		
		private function configureHandleListeners():void
		{
			_handle.addEventListener(MouseEvent.ROLL_OUT, rollOutHandler);
			_handle.addEventListener(MouseEvent.ROLL_OVER, rollOverHandler);
			_handle.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
		}
		
		/**
		 * 드래그 객체의 핸들에 이벤트를 제거하는 함수 
		 * 
		 */		
		private function removeHandleListeners():void
		{
			_handle.removeEventListener(MouseEvent.ROLL_OUT, rollOutHandler);
			_handle.removeEventListener(MouseEvent.ROLL_OVER, rollOverHandler);
			_handle.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
		}
		
		/**
		 * MouseEvent.ROLL_OUT 이벤트의 핸들러 함수 
		 * @param event
		 * 
		 */		
		protected function rollOutHandler(event:MouseEvent):void
		{
			_instance.filters = [];
		}
		
		/**
		 * MouseEvent.ROLL_OVER 이벤트의 핸들러 함수
		 * @param event
		 * 
		 */		
		protected function rollOverHandler(event:MouseEvent):void
		{
			_instance.filters = [this.filter];
		}
		
		/**
		 * MouseEvent.MOUSE_DOWN 이벤트의 핸들러 함수
		 * @param event
		 * 
		 */		
		private function mouseDownHandler(event:MouseEvent):void
		{
			trace("mouseDownHandler");
			if (hasChildEvent(event)) return;
			
			mouseDownBehavior();
		}
		
		/**
		 * 자식 개체에서 이벤트를 가지고 있는지 여부를 반환하는 함수 
		 * @param event
		 * @return 
		 * 
		 */		
		private function hasChildEvent(event:MouseEvent):Boolean
		{
			var interactiveObject:InteractiveObject = event.target as InteractiveObject;
			trace("interactiveObject : "+  interactiveObject);
			trace("interactiveObject : "+  interactiveObject.name);
			if (interactiveObject is TextField)
			{
				trace("텍스트 필드는 드래그 기능 껏다가 UP할때 활성화 시켜주기, 입력 필드는 리턴 시키기");
				var textField:TextField = interactiveObject as TextField;
				if (textField.type == TextFieldType.DYNAMIC)
					textField.selectable = false;
				else
					return true;
			}
			
			if (interactiveObject.hasEventListener(MouseEvent.CLICK) || 
				interactiveObject.parent.hasEventListener(MouseEvent.CLICK)) return true;
			
			return false;
		}
		
		private function mouseDownBehavior():void
		{
			configureStageListeners();
			
			_instance.startDrag(false);
		}
		
		/**
		 * MouseEvent.MOUSE_UP 이벤트 이후에 대한 함수
		 * 
		 */		
		protected function mouseUpBehavior():void
		{
			removeStageListeners();
			
			_instance.stopDrag();
		}
		
		/**
		 * 스테이지에 이벤트를 등록하는 함수 
		 * 
		 */		
		private function configureStageListeners():void
		{
			_instance.addEventListener(Event.MOUSE_LEAVE, stageMouseLeaveHandler);
			_instance.addEventListener(MouseEvent.MOUSE_UP, stageMouseUpHandler);
		}
		
		/**
		 * 스테이지에 이벤트를 제거하는 함수 
		 * 
		 */		
		private function removeStageListeners():void
		{
			_instance.removeEventListener(Event.MOUSE_LEAVE, stageMouseLeaveHandler);
			_instance.removeEventListener(MouseEvent.MOUSE_UP, stageMouseUpHandler);
		}
		
		/**
		 * 스테이지에 Event.MOUSE_LEAVE 이벤트의 핸들러 함수 
		 * 마우스가 스테이지 밖으로 나갔을 때 발생하는 이벤트
		 * @param event
		 * 
		 */		
		private function stageMouseLeaveHandler(event:Event):void
		{
			mouseUpBehavior();
		}
		
		/**
		 * 스테이지에 MouseEvent.MOUSE_UP 이벤트의 핸들러 함수 
		 * @param event
		 * 
		 */		
		private function stageMouseUpHandler(event:MouseEvent):void
		{
			mouseUpBehavior();
		}
		
		private var _dragEnabled:Boolean = false;
		/**
		 * 드래그 가능 여부
		 * @return 
		 * 
		 */		
		public function get dragEnabled():Boolean
		{
			return _dragEnabled;
		}
		
		public function set dragEnabled(value:Boolean):void
		{
			if (_dragEnabled && !value)
			{
				removeHandleListeners();
			}
			_dragEnabled = value;
			
			if (value)
			{
				configureHandleListeners();
			}
		}
		
		private var _filter:BitmapFilter = new GlowFilter();
		/**
		 * 적용되는 필터
		 * @return 
		 * 
		 */
		public function get filter():BitmapFilter
		{
			return _filter;
		}
		
		public function set filter(value:BitmapFilter):void
		{
			_filter = value;
		}
	}
}