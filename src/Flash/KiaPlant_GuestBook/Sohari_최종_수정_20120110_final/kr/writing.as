﻿package
{
	import com.adobe.images.JPGEncoder;
	import com.adobe.images.PNGEncoder;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;
	
	import kr.umhannum.containers.PhotographBox;
	import kr.umhannum.controls.ConfirmAlert;
	import kr.umhannum.controls.EmailQuestionAlert;
	import kr.umhannum.controls.Keyboard;
	import kr.umhannum.controls.QuestionAlert;
	import kr.umhannum.controls.ToolBox;
	import kr.umhannum.drawing.DrawingBoard;
	import kr.umhannum.events.AlertEvent;
	import kr.umhannum.events.CustomEvent;
	import kr.umhannum.events.KeyEvent;
	import kr.umhannum.events.SocketEvent;
	import kr.umhannum.events.ToolBoxEvent;
	import kr.umhannum.miscellaneous.ProgressBar;
	import kr.umhannum.utils.Base64;
	
	public class writing extends Sprite
	{
		public static const PROGRESS_BAR_X:int = 184;
		public static const PROGRESS_BAR_Y:int = 670;
		
		public static const OK:String = "ok";
		
		public var close_btn:SimpleButton;
		public var photogrphBox:PhotographBox;
		public var cancel_btn:SimpleButton;
		public var save_btn:SimpleButton; 
		public var toolBox:ToolBox;
		public function writing()
		{
			super();
			
			loaderInfo.addEventListener(Event.INIT, initHandler);
		}
		
		private function initialize():void
		{
			trace("writing stage : " + stage);
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.EXACT_FIT;
			
			// 사진 저장 작업이 있었는지 여부
			guestbook.savedPhotograph = false;
			
			configureListeners();
		}
		
		private function configureListeners():void
		{
			close_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			cancel_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			save_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			
			toolBox.addEventListener(ToolBoxEvent.PEN_COLOR, penColorHandler);
			toolBox.addEventListener(ToolBoxEvent.PEN_SIZE, penSizeHandler);
			toolBox.addEventListener(ToolBoxEvent.ERASER_SIZE, eraserSizeHandler);
			
			toolBox.addEventListener(CustomEvent.CUSTOM_EVENT, customEventHandler);
		}
		
		private function removeListeners():void
		{
		}
		
		private function initHandler(event:Event):void
		{
			initialize();
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			trace("guestbook clickHandler");
			
			var expression:String = String(event.currentTarget.name).split("_")[0];
			switch (expression)
			{
				case "close":
					trace("닫기");
					this.dispatchEvent(new Event(Event.CLOSE));
					break;
				case "cancel":
					trace("다시쓰기");
					drawingCancel();
					//photographyCancel();
					break;
				case "save":
					trace("저장하기");
					photogrphBox.visibleCaptureButton(false);
					createQuestionAlert();
					trace("numChildren : " + numChildren);
					break;
				default:
					break;
			}
		}
		
		private var questionAlert:QuestionAlert;
		private function createQuestionAlert():void
		{
			if (!questionAlert)
			{
				questionAlert = new QuestionAlert();
				questionAlert.addEventListener(AlertEvent.YES, yesHandler);
				questionAlert.addEventListener(AlertEvent.NO, noHandler);
				//questionAlert.alpha = 0;
				questionAlert.attach();
				this.addChild(questionAlert);
			}
		}
		
		private function yesHandler(event:AlertEvent):void
		{
			if (questionAlert)
			{
				this.removeChild(questionAlert);
				questionAlert.dispose();
				questionAlert = null;
			}
			
			trace("이미지 저장 프로세스");
			// 캡쳐하기 전에 그리기 도구 강제로 숨기기
			drawingBoard.isHideTool = true;
			trace("photogrphBox : " + photogrphBox);
			// 캡쳐하기 전에 카메라도 강제로 숨기기
			if (null != photogrphBox.snapshotCamera && !photogrphBox.snapshoted)
				photogrphBox.snapshotCamera.visible = false;
			
			capture();
			
			// 캡쳐 한 후에 그리기 도구 강제로 보이기
			drawingBoard.isHideTool = false;
			// 캡쳐 한 후에 카메라도 강제로 보이기
			if (null != photogrphBox.snapshotCamera && !photogrphBox.snapshoted)
				photogrphBox.snapshotCamera.visible = true;
		}
		
		private function noHandler(event:AlertEvent):void
		{
			if (questionAlert)
			{
				this.removeChild(questionAlert);
				questionAlert.dispose();
				questionAlert = null;
			}
			photogrphBox.visibleCaptureButton(true);
		}
		
		private function drawingCancel():void
		{
			photogrphBox.clearDrawingBoard();
		}
		
		private function photographyCancel():void
		{
			photogrphBox.clearSnapshot();
		}
		
		private function penColorHandler(event:ToolBoxEvent):void
		{
			trace("penColorHandler", event.value);
			drawingBoard.color(event.no, event.value);
		}
		
		private function penSizeHandler(event:ToolBoxEvent):void
		{
			trace("penSizeHandler", event.value);
			drawingBoard.penSize = event.value;
		}
		
		private function eraserSizeHandler(event:ToolBoxEvent):void
		{
			trace("eraserSizeHandler", event.value);
			drawingBoard.eraserSize = event.value;
		}
		
		private function customEventHandler(event:CustomEvent):void
		{
			trace("customEventHandler", event.args);
			
			drawingBoard.toolType = String(event.args);
		}
		
		private function capture():void
		{
			var mailBitmapData:BitmapData = new BitmapData(photogrphBox.width, photogrphBox.height, true, 0xffffff);
			mailBitmapData.draw(photogrphBox);
			
			//var encoder:JPGEncoder = new JPGEncoder(80); // jpg 용
			//var byteArray:ByteArray = encoder.encode(mailBitmapData); // jpg 용
			var byteArray:ByteArray = PNGEncoder.encode(mailBitmapData); // png 용
			captureData = Base64.encodeByteArray(byteArray);
			//trace("captureData : " + captureData);
			
			// 사진 저장 작업이 있었는지 여부
			guestbook.savedPhotograph = true;
			// 서버 전송
			sendData();
		}
		
		private function sendData():void
		{
			dispatchEvent(new SocketEvent(SocketEvent.SEND, query, mailAlert));
		}
		
		public function mailAlert():void
		{
			trace("이메일 팝업창 띄우기");
			createEmailQuestionAlert();
		}
		
		private var emailQuestionAlert:EmailQuestionAlert;
		private function createEmailQuestionAlert():void
		{
			if (!emailQuestionAlert)
			{
				emailQuestionAlert = new EmailQuestionAlert();
				emailQuestionAlert.addEventListener(AlertEvent.YES, emailYesHandler);
				emailQuestionAlert.addEventListener(AlertEvent.NO, emailNoHandler);
				//emailQuestionAlert.alpha = 0;
				emailQuestionAlert.attach();
				this.addChild(emailQuestionAlert);
			}
		}
		
		private function emailYesHandler(event:AlertEvent):void
		{
			trace("키보드 생성 프로세스");
			createKeyboard();
			
			if (emailQuestionAlert)
			{
				this.removeChild(emailQuestionAlert);
				emailQuestionAlert.dispose();
				emailQuestionAlert = null;
			}
		}
		
		private function emailNoHandler(event:AlertEvent):void
		{
			if (emailQuestionAlert)
			{
				this.removeChild(emailQuestionAlert);
				emailQuestionAlert.dispose();
				emailQuestionAlert = null;
			}
			trace("닫기");
			this.dispatchEvent(new Event(Event.CLOSE));
		}
		
		private var keyboard:Keyboard;
		private function createKeyboard():void
		{
			trace("createEmailKeyboard");
			
			if (!keyboard)
			{
				keyboard = new Keyboard();
				keyboard.addEventListener(KeyEvent.SEND, sendHandler);
				keyboard.addEventListener(Event.CLOSE, closeHandler);
				keyboard.attach();
				this.addChild(keyboard);
			}
		}
		
		private function removeKeyboard():void
		{
			trace("removeEmailKeyboard");
			
			if (keyboard)
			{
				this.removeChild(keyboard);
				keyboard.dispose();
				keyboard = null;
			}
		}
		
		private function sendHandler(event:KeyEvent):void
		{
			trace("sendHandler", event.value);
			_mailAddress = event.value;
			trace("이메일 보내기 모듈");
			sendMail();
			removeKeyboard();
		}
		
		private function closeHandler(event:Event):void
		{
			trace("키보드 닫기");
			removeKeyboard();
		}
		
		private function sendMail():void
		{
			trace("guestbook.configure.server : " + guestbook.configure.server);
			var urlRequest:URLRequest = new URLRequest(guestbook.configure.server);
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			urlLoader.addEventListener(Event.COMPLETE, completeHandler);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onIoErrorHandler);
			urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			
			var urlVariables:URLVariables = new URLVariables();
			urlVariables.email = mailAddress;
			urlVariables.objByteData = captureData;
			
			urlRequest.data = urlVariables;
			urlRequest.method = URLRequestMethod.POST;
			
			urlLoader.load(urlRequest);
		}
		
		private var progressBar:ProgressBar;
		private function createProgressBar():void
		{
			if (!progressBar)
			{
				progressBar = new ProgressBar();
				trace("progressBar : " + progressBar);
				progressBar.x = writing.PROGRESS_BAR_X;
				progressBar.y = writing.PROGRESS_BAR_Y;
				progressBar.addEventListener(Event.COMPLETE, progressCompleteHandler);
				stage.addChild(progressBar);
			}
		}
		
		private function progressCompleteHandler(event:Event):void
		{
			if (progressBar)
			{
				stage.removeChild(progressBar);
				progressBar.removeEventListener(Event.COMPLETE, progressCompleteHandler);
				progressBar = null;
			}
			createConfirmAlert();
		}
		
		private function progressHandler(event:ProgressEvent):void
		{
			trace("event bytesLoaded : " + event.bytesLoaded);
			trace("event bytesTotal : " + event.bytesTotal);
		}
		
		private function completeHandler(event:Event):void
		{
			var urlLoader:URLLoader = event.currentTarget as URLLoader;
			trace("completeHandler: " + urlLoader.data);
			
			if (urlLoader.data == writing.OK)
			{
				trace("데이터 전송 성공");
				createProgressBar();
			}
			else
			{
				trace("데이터 전송 실패");
			}
		}
		
		// 이메일 IOErrorEvent Handler
		private function onIoErrorHandler(event:IOErrorEvent):void
		{
			throw new Error( "IOError 입니다. 잠시 후, 다시 시도해주세요." );
		}
		
		// 이메일 SecurityErrorEvent Handler
		private function securityErrorHandler(event:SecurityErrorEvent):void
		{
			throw new Error( "SecurityError 입니다. 잠시 후, 다시 시도해주세요." );
		}
		
		private var confirmAlert:ConfirmAlert;
		private function createConfirmAlert():void
		{
			if (!confirmAlert)
			{
				confirmAlert = new ConfirmAlert();
				confirmAlert.addEventListener(AlertEvent.OK, okHandler);
				//confirmAlert.alpha = 0;
				confirmAlert.attach();
				this.addChild(confirmAlert);
			}
		}
		
		private function okHandler(event:AlertEvent):void
		{
			if (confirmAlert)
			{
				this.removeChild(confirmAlert);
				confirmAlert.dispose();
				confirmAlert = null;
			}
			this.dispatchEvent(new Event(Event.CLOSE));
		}
		
		private var _mailAddress:String;
		public function get mailAddress():String
		{
			return _mailAddress;
		}
		
		private var _captureData:String
		public function get captureData():String
		{
			return _captureData;
		}

		public function set captureData(value:String):void
		{
			_captureData = value;
		}
		
		private var _drawingBoard:DrawingBoard;
		public function get drawingBoard():DrawingBoard
		{
			if (!_drawingBoard)
				_drawingBoard = photogrphBox.drawingBoard;
			return _drawingBoard;
		}
		
		private var _query:String;
		public function get query():String
		{
			_query = "GUEST_BOOK|NAME=KIA_GUESTBOOK|SIZE=" + captureData.length + "\nJPG=" + captureData;
			return _query;
		}
	}
}