﻿package
{
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.geom.Rectangle;
	
	import kr.umhannum.containers.DetailBox;
	import kr.umhannum.containers.FlingPageBox;
	import kr.umhannum.containers.flingPageBoxClasses.FlingPageBoxSacleMode;
	import kr.umhannum.controls.DeleteAlert;
	import kr.umhannum.events.AlertEvent;
	import kr.umhannum.events.SocketEvent;
	import kr.umhannum.miscellaneous.ProgressBar;
	
	public class Detail extends Sprite
	{
		public static const DELETE_CODE_NUM:int = 5;
		public static const TOTAL_IMAGE_NUM:int = 35;
		
		public static const LIST_FILE_PATH:String = "./Plugins/Wedding/GuestBook/KIA_GUESTBOOK/";
		//public static const LIST_FILE_PATH:String = "c:/sqisoft/contents/enc/Plugins/Wedding/GuestBook/KIA_GUESTBOOK/";
		
		public var data:XMLList;
		
		private var pageNum:int;
		private var imageNum:int;
		private var repeatNum:int;
		
		public var close_btn:SimpleButton;
		public var prev_btn:SimpleButton;
		public var next_btn:SimpleButton;
		
		public var detailBox:DetailBox;
		public var deleteAlert_mc:DeleteAlert;
		public function Detail()
		{
			super();
			
			loaderInfo.addEventListener(Event.INIT, initHandler);
		}
		
		private function initialize():void
		{
			repeatNum = 0;
			
			// 이미지 삭제 작업이 있었는지 여부
			guestbook.deletedPhotograph = false;
			
			deleteAlert_mc.visible = false;
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.EXACT_FIT;
			
			configureListeners();
		}
		
		private function configureListeners():void
		{
			close_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			prev_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			next_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			detailBox.addEventListener(MouseEvent.CLICK, mouseDownHandler);
			detailBox.addEventListener(ProgressEvent.PROGRESS, progressHandler);			
		}
		
		private function removeListeners():void
		{
		}
		
		private function initHandler(event:Event):void
		{
			initialize();
		}
		
		private function createDeleteAlert():void
		{
			deleteAlert_mc.visible = true;
			deleteAlert_mc.addEventListener(AlertEvent.YES, yesHandler);
			deleteAlert_mc.addEventListener(AlertEvent.NO, noHandler);
		}
		
		private function yesHandler(event:AlertEvent):void
		{
			deleteAlert_mc.visible = false;
			
			trace("이미지 삭제하기");
			guestbook.deletedPhotograph = true;
			
			// 서버 전송
			sendData();
		}
		
		private function noHandler(event:AlertEvent):void
		{
			deleteAlert_mc.visible = false;
			
			repeatNum = 0;
		}
		
		private function sendData():void
		{
			dispatchEvent(new SocketEvent(SocketEvent.SEND, deleteQuery));
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			trace("clickHandler");
			var expression:String = String(event.currentTarget.name).split("_")[0];
			switch (expression)
			{
				case "close":
					trace("닫기");
					dispatchEvent(new Event(Event.CLOSE));
					break;
				case "prev":
					trace("이전");
					prev();
					break;
				case "next":
					trace("다음");
					next();
					break;
				default:
					break;
			}
		}
		
		private function mouseDownHandler(event:MouseEvent):void
		{
			trace("mouseDownHandler");
			repeatNum++;
			trace("repeatNum : " + repeatNum);
			if (repeatNum == Detail.DELETE_CODE_NUM)
			{
				trace("삭제하기");
				createDeleteAlert();
			}
		}
		
		private function progressHandler(event:ProgressEvent):void
		{
			//trace("event.bytesLoaded : " + event.bytesLoaded);
			//trace("event.bytesTotal : " + event.bytesTotal);
		}
		
		private function next():void
		{
			imageNum++;
			//trace("imageNum : " + imageNum);
			//trace("totalImageNum : " + totalImageNum);
			if (imageNum == totalImageNum)
			{
				pageNum++;
				if (pageNum == totalPageNum)
				{
					pageNum = 0;
				}
				imageNum = 0;
			}
			//createProgressBar();
			load(pageNum, imageNum);
		}
		
		private function prev():void
		{
			imageNum--;
			//trace("imageNum : " + imageNum);
			//trace("totalImageNum : " + totalImageNum);
			if (imageNum < 0)
			{
				pageNum--;
				if (pageNum < 0)
				{
					pageNum = totalPageNum - 1;
				}
				imageNum = totalImageNum - 1;
			}
			//createProgressBar();
			load(pageNum, imageNum);
		}
		
		public function load(pageNum:int, imageNum:int):void
		{
			repeatNum = 0;
			
			this.pageNum = pageNum;
			this.imageNum = imageNum;
			
			detailBox.load(LIST_FILE_PATH + data[pageNum].filename[imageNum]);
		}
		
		public function unload():void
		{
			detailBox.unload();
		}
		
		private var _totalPageNum:int;
		public function get totalPageNum():int
		{
			_totalPageNum = data.length();
			return _totalPageNum;
		}
		
		private var _totalImageNum:int;
		public function get totalImageNum():int
		{
			_totalImageNum = XMLList(data[pageNum].filename).length();
			return _totalImageNum;
		}
		
		private var _deleteQuery:String;
		public function get deleteQuery():String
		{
			var filename:String = data[pageNum].filename[imageNum];
			_deleteQuery = "GUEST_BOOK|DELE=KIA_GUESTBOOK|SIZE=" + filename.length + "\nJPG=" + filename;
			return _deleteQuery;
		}
		
		/*private var progressBar:ProgressBar;
		private function createProgressBar():void
		{
			if (!progressBar)
			{
				progressBar = new ProgressBar();
				progressBar.x = writing.PROGRESS_BAR_X;
				progressBar.y = writing.PROGRESS_BAR_Y;
				progressBar.addEventListener(Event.COMPLETE, progressCompleteHandler);
				stage.addChild(progressBar);
			}
		}
		
		private function progressCompleteHandler(event:Event):void
		{
			if (progressBar)
			{
				stage.removeChild(progressBar);
				progressBar.removeEventListener(Event.COMPLETE, progressCompleteHandler);
				progressBar = null;
			}
			load(pageNum, imageNum);
		}*/
		
		/*private var flingPageBox:FlingPageBox;
		private function createFlingPageBox():void
		{
			if (!flingPageBox)
			{
				flingPageBox = new FlingPageBox();
				flingPageBox.loop = true;
				flingPageBox.boundary = new Rectangle(0, 0, guestbook.STAGE_WIDTH, guestbook.STAGE_HEIGHT);
				flingPageBox.scaleMode = FlingPageBoxSacleMode.NO_SCALE;
				var len:int = filenameList.length();
				for ( var i:int=0; i<len; i++ )
				{
					var position:XMLList = position[i%2].value as XMLList;
					var flingPage:FlingPage = new FlingPage(position);
					flingPage.no = i;
					flingPage.data = filenameList[i].filename as XMLList;
					flingPage.addEventListener(FlingPageEvent.SELECT, selectHandler);
					flingPageBox.addChild(flingPage);
				}
				
				var childIndex:int = this.getChildIndex(guestbook_btn);
				this.addChildAt(flingPageBox, childIndex);
				flingPageBox.initialize();
			}
		}*/
	}
}