﻿package kr.umhannum.net
{
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.Socket;
	import flash.utils.Timer;
	
	import kr.umhannum.events.UBCSocketEvent;
	import kr.umhannum.events.UBCSocketStatusEvent;
	
	
	[ Event(name = "SOCKET_DATA_COMPLETE", type = "sqisoft.events.FirmwareSocketEvent") ]
	public class UBCSocket extends Socket
	{
		private var response:String;
		private var pingTimer:Timer;
		private var received:Boolean;
		
		private var host:String;
		private var port:uint;
		
		public function UBCSocket(host:String=null, port:uint=0)
		{
			super();
			
			this.host = host;
			this.port = port;
			
			received = true;
			
			super.timeout = 3000;
			
			configureListeners();
			socketConnect();
		}
		
		private function socketConnect():void
		{
			if (host && port) super.connect(host, port);
		}
		
		private function configureListeners():void
		{
			addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
			addEventListener(Event.CONNECT, connectHandler);
			addEventListener(Event.CLOSE, closeHandler);
			addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		}
		
		public function send(str:String):void
		{
			if (!received) return;			
			received = false;
			
			response = "";
			
			str = str.split("undefined").join("");
			str += "\n";
			
			trace("socketString : " + str);
			try
			{
				writeUTFBytes(str);
			}
			catch(event:IOError)
			{
				trace(event);
			}
			flush();
		}
		
		private function socketDataHandler(event:ProgressEvent):void
		{
			received = true;
			
			var bytes:String = readUTFBytes(bytesAvailable);
			response += bytes;
			
			dispatchEvent(new UBCSocketEvent(UBCSocketEvent.SOCKET_DATA_COMPLETE, response));
		}
		
		public function ping():void
		{
			send("ping");
			
			var delay:uint = 10000;
			var repeat:uint = 0;
			
			pingTimer = new Timer(delay, repeat);
			pingTimer.addEventListener(TimerEvent.TIMER, onPing);
			pingTimer.start();
		}
		
		private function onPing(event:TimerEvent):void
		{
			send("ping");
		}
		
		private function connectHandler(event:Event):void
		{
			trace("connectHandler: " + event);
			
			response = "connect";
			dispatchEvent(new UBCSocketEvent(UBCSocketEvent.SOCKET_DATA_COMPLETE, response));
		}
		
		private function closeHandler(event:Event):void
		{
			trace("closeHandler: " + event);
			trace(response.toString());
			// pingTimer.stop();
			
			//socketConnect();
			var status:String = "closeHandler : " + event.toString();
			dispatchEvent(new UBCSocketStatusEvent(UBCSocketStatusEvent.SOCKET_STATUS, status));
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void
		{
			trace("ioErrorHandler: " + event);
			
			var status:String = "ioErrorHandler : " + event.toString();
			dispatchEvent(new UBCSocketStatusEvent(UBCSocketStatusEvent.SOCKET_STATUS, status));
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void
		{
			trace("securityErrorHandler: " + event);
			
			//socketConnect();
			
			var status:String = "securityErrorHandler : " + event.toString();
			dispatchEvent(new UBCSocketStatusEvent(UBCSocketStatusEvent.SOCKET_STATUS, status));
		}
	}
}
