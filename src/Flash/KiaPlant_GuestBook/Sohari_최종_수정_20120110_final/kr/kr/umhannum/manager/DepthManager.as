﻿package kr.umhannum.manager
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	
	public class DepthManager
	{
		/**
		 * DepthManager constructor 
		 * 
		 */		
		public function DepthManager()
		{
			super();
		}
		
		public static function bringToBottom(target:DisplayObject):void
		{
			var parent:DisplayObjectContainer = target.parent;
			if (parent == null) return;
			
			if (parent.getChildIndex(target) != 0)
				parent.setChildIndex(target, 0);
		}
		
		public static function bringToTop(target:DisplayObject):void
		{
			var parent:DisplayObjectContainer = target.parent;
			if (parent == null) return;
			
			var index:int = parent.getChildIndex(target);
			var maxIndex:int = parent.numChildren-1;
			for (var i:int=0; i<=maxIndex; i++)
			{
				var mi:DisplayObject = parent.getChildAt(i);
			}
			
			if (parent.getChildIndex(target) != maxIndex)
				parent.setChildIndex(target, maxIndex);
		}
		
		public static function isTop(target:DisplayObject):Boolean
		{
			var parent:DisplayObjectContainer = target.parent;
			if (parent == null) return true;
			
			return parent.numChildren - 1 == parent.getChildIndex(target);
		}
		
		public static function isBottom(target:DisplayObject):Boolean
		{
			var parent:DisplayObjectContainer = target.parent;
			if (parent == null) return true;
			
			var depth:int = parent.getChildIndex(target);
			if (depth == 0)
				return true;
			else
				return false;
		}
	}
}