package kr.umhannum.core
{
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	import kr.umhannum.effect.TweenStatusType;
	import kr.umhannum.events.TweenStatusEvent;
	import kr.umhannum.manager.DragManager;
	
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.core.easing.IEasing;
	import org.libspark.betweenas3.easing.Sine;
	import org.libspark.betweenas3.tweens.IObjectTween;
	import org.libspark.betweenas3.tweens.ITween;
	
	
	public class SimpleComponent extends MovieClip
	{
		public var owner:DisplayObjectContainer;
		
		private var tween:ITween;
		private var initialized:Boolean; // 초기화 여부
		protected var eventListenerStack:Array;
		public function SimpleComponent()
		{
			super();
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler, false, 0, true);
			addEventListener(Event.REMOVED_FROM_STAGE, removeFromStageHandler, false, 0, true);
		}
		
		public final function initialize():void
		{
			if (!initialized)
			{
				initialized = true;
				
				createChildren();
				commitProperties();
				configureListeners();
			}
		}
		
		protected function createChildren():void
		{
		}
		
		protected function commitProperties():void
		{
		}
		
		protected function configureListeners(dispatcher:IEventDispatcher=null):void
		{
		}
		
		protected function removeListeners(dispatcher:IEventDispatcher=null):void
		{
		}
		
		/**
		 * Event.ADDED_TO_STAGE 이벤트 핸들러
		 * @param event
		 * 
		 */		
		private function addedToStageHandler(event:Event):void
		{
			// 스테이지에 붙고 나서 ADDED_TO_STAGE 이벤트를 제거해줘야 두번 실행 되는 오류를 막을 수 있다.
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			
			stageInitialize();
		}
		
		/**
		 * Event.REMOVED_FROM_STAGE 이벤트 핸들러
		 * @param event
		 * 
		 */		
		private function removeFromStageHandler(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, removeFromStageHandler);
			dispose();
		}
		
		/**
		 * 스테이지에 붙고 나서 실행 되는 함수 
		 * @param event
		 * 
		 */		
		protected function stageInitialize():void
		{
			initialize();
			initializeOwner();
		}
		
		private function initializeOwner():void
		{
			owner = this;
			while (!(null != basedClass && owner is basedClass) && !(owner is Stage))
			{
				var DefinitionClass:Class = getDefinitionByName(getQualifiedClassName(owner)) as Class;
				owner = owner.parent;
			}
		}
		
		override public function addEventListener(type:String, listener:Function, useCapture:Boolean=false, priority:int=0, useWeakReference:Boolean=false):void
		{
			if( !eventListenerStack ) eventListenerStack = [];
			eventListenerStack[eventListenerStack.length] = {type:type, listener:listener, useCapture:useCapture};
			
			super.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		override public function removeEventListener(type:String, listener:Function, useCapture:Boolean=false):void
		{
			if (eventListenerStack)
			{
				var removeIndex:int = -1;
				var len:int = eventListenerStack.length;
				for ( var i:int=0; i<len; i++ )
				{
					var obj:Object = eventListenerStack[i];
					if (obj.type == type && obj.listener == listener)
					{
						removeIndex = i;
					}
				}
				if (removeIndex > 0) eventListenerStack.splice(removeIndex, 1);
			}
			super.removeEventListener(type, listener, useCapture);
		}
		
		public function dispose():void
		{
			var len:int = this.numChildren;
			while (len > 0)
			{
				this.removeChild(this.getChildAt(0));
				len--;
			}
			
			if (eventListenerStack)
			{
				while (eventListenerStack.length > 0)
				{
					var obj:Object = eventListenerStack.shift();
					this.removeEventListener(obj.type, obj.listener, obj.useCapture);
				}
				eventListenerStack = null;
			}
		}
		
		/**
		 * component를 보여주는 함수 
		 * 
		 */		
		public function show():void
		{
			this.visible = true;
			
			if (null != tween) tween.stop();
			tween = BetweenAS3.tween(this, {alpha:1}, null, 0.5, Sine.easeInOut);
			tween.play();
		}
		
		/**
		 * component를 숨겨주는 함수
		 * 
		 */		
		public function hide():void
		{
			if (null != tween) tween.stop();
			tween = BetweenAS3.tween(this, {alpha:0}, null, 0.5, Sine.easeInOut);
			tween.play();
			tween.onComplete = function():void
			{
				this.visible = false;
			}
		}
		
		/**
		 * component를 붙이는 함수
		 * 
		 */		
		public function attach():void
		{
			if (null != tween) tween.stop();
			tween = getOpenTween();
			tween.play();
			tween.onComplete = function()
			{
				this.visible = true;
				dispatchEvent(new TweenStatusEvent(TweenStatusEvent.COMPLETE, TweenStatusType.OPEN));
			}
		}
		
		/**
		 * component를 떼어내는 함수
		 * 
		 */		
		public function detach():void
		{
			if (null != tween) tween.stop();
			tween = getCloseTween();
			tween.play();
			tween.onComplete = function():void
			{
				this.visible = false;
				dispatchEvent(new TweenStatusEvent(TweenStatusEvent.COMPLETE, TweenStatusType.CLOSE));
				dispose();
				tween = null;
			}
		}
		
		private function visibled(value:Boolean):void
		{
			this.visible = value;
		}
		
		private var _dragManager:DragManager;
		protected function get dragManager():DragManager
		{
			if (!_dragManager)
				_dragManager = new DragManager(this);
			return _dragManager;
		}
		
		private var _dragEnabled:Boolean = false;
		/**
		 * 드래그 가능 여부
		 * @return 
		 * 
		 */		
		public function get dragEnabled():Boolean
		{
			return _dragEnabled;
		}
		
		public function set dragEnabled(value:Boolean):void
		{
			if (_dragEnabled && !value)
			{
				dragManager.dragEnabled = false;
			}
			_dragEnabled = value;
			
			if (value)
			{
				dragManager.dragEnabled = true;
			}
		}
		
		private var _basedClass:Class;
		protected function get basedClass():Class
		{
			return _basedClass;
		}
		
		protected function set basedClass(value:Class):void
		{
			_basedClass = value;
		}
		
		private var _no:int;
		public function get no():int
		{
			return _no;
		}
		
		public function set no(value:int):void
		{
			_no = value;
		}
		
		private var _openTween:IObjectTween;
		/**
		 * open 함수 때, 실행되는 Tween을 반환하는 함수
		 * @return 
		 * 
		 */		
		protected function getOpenTween():IObjectTween
		{
			if (!_openTween)
				_openTween = BetweenAS3.tween(this, {alpha:1}, null, 0.5, Sine.easeInOut);
			return _openTween;
		}
		
		/**
		 * open 함수 때, 실행되는 Tween을 설정하는 함수
		 * @param target
		 * @param to
		 * @param from
		 * @param time
		 * @param easing
		 * 
		 */		
		protected function setOpenTween(target:Object, to:Object, from:Object=null, time:Number=1.0, easing:IEasing=null):void
		{
			_openTween = BetweenAS3.tween(target, to, from, time, easing);
		}
		
		private var _closeTween:IObjectTween;
		/**
		 * close 함수 때, 실행되는 Tween을 반환하는 함수
		 * @return 
		 * 
		 */		
		protected function getCloseTween():IObjectTween
		{
			if (!_closeTween)
				_closeTween = BetweenAS3.tween(this, {alpha:0}, null, 0.5, Sine.easeInOut);
			return _closeTween;
		}
		
		/**
		 * close 함수 때, 실행되는 Tween을 설정하는 함수
		 * @param target
		 * @param to
		 * @param from
		 * @param time
		 * @param easing
		 * 
		 */		
		protected function setCloseTween(target:Object, to:Object, from:Object=null, time:Number=1.0, easing:IEasing=null):void
		{
			_closeTween = BetweenAS3.tween(target, to, from, time, easing);
		}
	}
}