package kr.umhannum.events
{
	import flash.events.Event;
	
	public class SocketEvent extends Event
	{
		public static const SEND:String = "send";
		public function SocketEvent(type:String, query:String, callLaterFunc:Function=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_query = query;
			_callLaterFunc = callLaterFunc;
		}
		
		override public function clone():Event
		{
			return new SocketEvent(type, query, callLaterFunc, bubbles, cancelable);
		}
		
		private var _query:String;
		public function get query():String
		{
			return _query;
		}
		
		private var _callLaterFunc:Function;
		public function get callLaterFunc():Function
		{
			return _callLaterFunc;
		}
	}
}