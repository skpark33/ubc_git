﻿package kr.umhannum.events
{
	import flash.display.Bitmap;
	import flash.events.Event;
	
	
	public class ImageLoaderEvent extends Event
	{
		public static const DATA_COMPLETE:String = "dataComplete";
		
		public function ImageLoaderEvent( type:String, image:Bitmap, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
			
			_image = image;
		}
		
		override public function clone():Event
		{
			return new ImageLoaderEvent( type, image, bubbles, cancelable );
		}
		
		private var _image:Bitmap;
		public function get image():Bitmap
		{
			return _image;
		}
	}
}