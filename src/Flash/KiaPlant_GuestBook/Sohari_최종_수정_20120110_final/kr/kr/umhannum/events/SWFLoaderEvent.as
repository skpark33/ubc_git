package kr.umhannum.events
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	public class SWFLoaderEvent extends Event
	{
		public static const LOAD_COMPLETE:String = "loadComplete";
		
		public function SWFLoaderEvent(type:String, displayObject:DisplayObject, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_displayObject = displayObject;
		}
		
		override public function clone():Event
		{
			return new SWFLoaderEvent(type, _displayObject, bubbles, cancelable);
		}
		
		private var _displayObject:DisplayObject;
		public function get displayObject():DisplayObject
		{
			return _displayObject;
		}
	}
}