package kr.umhannum.events
{
	import flash.events.Event;
	
	public class ChangeEvent extends Event
	{
		public static const CHANGE:String = "change";
		
		public function ChangeEvent(type:String, value:Object=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_value = value;
		}
		
		override public function clone():Event
		{
			return new CustomEvent(type, value, bubbles, cancelable);
		}
		
		private var _value:Object;
		public function get value():Object
		{
			return _value;
		}
	}
}