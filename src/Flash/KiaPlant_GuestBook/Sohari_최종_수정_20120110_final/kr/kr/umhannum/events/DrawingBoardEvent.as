﻿package kr.umhannum.events
{
	import flash.events.Event;
	import flash.display.BitmapData;
	
	
	public class DrawingBoardEvent extends Event
	{
		public static const DRAWINGBOARD_COMPLETE:String = "drawingBoardComplete";
		
		public function DrawingBoardEvent( type:String, bitmapData:BitmapData=null, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
			
			_bitmapData = bitmapData;
		}
		
		override public function clone():Event
		{
			return new DrawingBoardEvent( type, _bitmapData, bubbles, cancelable );
		}
		
		private var _bitmapData:BitmapData;
		public function get bitmapData():BitmapData
		{
			return _bitmapData;
		}
	}
}