package kr.umhannum.events
{
	import flash.events.Event;
	
	public class AlertEvent extends Event
	{
		public static const YES:String = "yes";
		public static const NO:String = "no";
		public static const OK:String = "ok";
		public function AlertEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new AlertEvent(type, bubbles, cancelable);
		}
	}
}