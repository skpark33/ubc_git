package kr.umhannum.events
{
	import flash.events.Event;
	
	public class UBCSocketStatusEvent extends Event
	{
		public static const SOCKET_STATUS:String = "socketStatus";
		
		public function UBCSocketStatusEvent(type:String, status:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_status = status;
		}
		
		override public function clone():Event
		{
			return new UBCSocketEvent(type, status, bubbles, cancelable);
		}
		
		private var _status:String;
		public function get status():String
		{
			return _status;
		}
	}
}