package kr.umhannum.events
{
	import flash.events.Event;
	
	public class ToggleButtonEvent extends Event
	{
		public static const CHANGE_BUTTON:String = "changeButton";
		public function ToggleButtonEvent(type:String, label:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_label = label;
		}
		
		override public function clone():Event
		{
			return new ToggleButtonEvent(type, _label, bubbles, cancelable); 
		}
		
		private var _label:String;
		public function get label():String
		{
			return _label;
		}
	}
}