﻿package kr.umhannum.events
{
	import flash.events.Event;
	
	public class FlingPageEvent extends Event
	{
		public static const SELECT:String = "select";
		public function FlingPageEvent(type:String, pageNum:int, imageNum:int, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_pageNum = pageNum;
			_imageNum = imageNum;
		}
		
		override public function clone():Event
		{
			return new FlingPageEvent(type, pageNum, imageNum, bubbles, cancelable);
		}
		
		private var _pageNum:int;
		public function get pageNum():int
		{
			return _pageNum;
		}
		
		private var _imageNum:int;
		public function get imageNum():int
		{
			return _imageNum;
		}
	}
}