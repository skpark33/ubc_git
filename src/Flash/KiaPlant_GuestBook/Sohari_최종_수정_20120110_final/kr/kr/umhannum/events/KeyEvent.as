package kr.umhannum.events
{
	import flash.events.Event;
	
	public class KeyEvent extends Event
	{
		public static const KEY:String = "key";
		public static const SEND:String = "send";
		public function KeyEvent(type:String, value:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_value = value;
		}
		
		override public function clone():Event
		{
			return new KeyEvent(type, value, bubbles, cancelable);
		}
		
		private var _value:String;
		public function get value():String
		{
			return _value;
		}
	}
}