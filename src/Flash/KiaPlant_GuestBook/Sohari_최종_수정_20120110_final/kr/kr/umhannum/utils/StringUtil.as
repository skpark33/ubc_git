package kr.umhannum.utils
{
	public class StringUtil
	{
		/**
		 * 문자열의 맨 앞과 뒤의 줄바꿈과 탭간격을 없애줍니다.
		 * @param value
		 * @return 
		 */
		public static function removeWhitespace( value:String ):String
		{
			var result:String = trim(value.replace(/\t+/g, ""));
			
			return result;
		}
		
		/**
		 * 문자열 좌우 공백을 제거하는 메서드
		 * @param value
		 * @return
		 */
		public static function trim(value:String):String 
		{
			if (value == null) return '';
			return value.replace(/^\s+|\s+$/g, "");
		}
		
		/**
		 * 결과값이 10 미만일 경우 앞에 '0'을 붙여주고,
		 * 10 이상일 경우 그대로 반환
		 * 
		 */
		public static function digit(value:Number):String
		{
			if ( value < 10 ) return "0" + value;
			else return "" + value;
		}
		
		/**
		 * 결과값이 10 미만일 경우 앞에 '00'을 붙여주고,
		 * 결과값이 100 미만일 경우 앞에 '0'을 붙여주고,
		 * 100 이상일 경우 그대로 반환
		 * 
		 */
		public static function digit2(value:Number):String
		{
			if ( value < 10 ) return "00" + value;
			else if ( value < 100 ) return "0" + value;
			else return value;
		}
	}
}