﻿package kr.umhannum.utils
{
	import flash.external.ExternalInterface;

	public class ExternalInterfaceUtil
	{
		public static const REPORT_CARD_MESSAGE:String = "성적표가 존재하지 않습니다.";
		public static const SAMPLE_MESSAGE:String = "샘플에서는 제공하지 않습니다.";
		public static const SUBMIT_FAILED_MESSAGE:String = "문제를 풀지 않았습니다. 문제를 다 풀고 제출하세요.";
		public static const HURRY_UP_MESSAGE:String = "시험 종료 5분전입니다.";
		public static const TIME_OUT_MESSAGE:String = "남은 시험 시간이 다 되었습니다.";
		
		public static function externalInterface(...rest):String
		{
			var result:String;
			var len:int = rest.length;
			switch(len)
			{
				case 1:
					result = String(ExternalInterface.call(rest[0]));
					break;
				case 2:
					result = String(ExternalInterface.call(rest[0], rest[1]));
					break;
				case 3:
					result = String(ExternalInterface.call(rest[0], rest[1], rest[2]));
					break;
				case 4:
					result = String(ExternalInterface.call(rest[0], rest[1], rest[2], rest[3]));
					break;
				case 5:
					result = String(ExternalInterface.call(rest[0], rest[1], rest[2], rest[3], rest[4]));
					break;
				case 6:
					result = String(ExternalInterface.call(rest[0], rest[1], rest[2], rest[3], rest[4], rest[5]));
					break;
				case 7:
					result = String(ExternalInterface.call(rest[0], rest[1], rest[2], rest[3], rest[4], rest[5], rest[6]));
					break;
				default:
					break;
			}
			return result;
		}
	}
}