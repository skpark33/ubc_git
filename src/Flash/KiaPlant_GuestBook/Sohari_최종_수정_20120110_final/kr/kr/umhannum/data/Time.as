package kr.umhannum.data
{
	public class Time
	{
		/**
		 * 현재 재생 시간 
		 */			
		private var _currentTime:Number;
		public function get currentTime():Number
		{
			return _currentTime;
		}
		
		public function set currentTime(value:Number):void
		{
			if (isNaN(value)) return;
			
			_currentTime = value;
		}
		
		/**
		 * 총 재생 시간 
		 */			
		private var _totalTime:Number;
		public function get totalTime():Number
		{
			return _totalTime;
		}
		
		public function set totalTime(value:Number):void
		{
			if (isNaN(value)) return;
			
			_totalTime = value;
		}
		
		public function Time(currentTime:Number=0, totalTime:Number=0)
		{
			_currentTime = currentTime;
			_totalTime = totalTime;
		}
		
		public function clone():Time
		{
			return new Time(_currentTime, _totalTime);
		}
	}
}