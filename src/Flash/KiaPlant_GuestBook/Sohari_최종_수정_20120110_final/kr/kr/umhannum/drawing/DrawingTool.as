package kr.umhannum.drawing
{
	import flash.display.MovieClip;
	import flash.events.IEventDispatcher;
	
	import kr.umhannum.core.SimpleComponent;
	
	public class DrawingTool extends SimpleComponent
	{
		public var originalX:Number;
		public var originalY:Number;
		
		public var pen_mc:MovieClip;
		public var eraser_mc:MovieClip;
		public function DrawingTool()
		{
			super();
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			this.originalX = this.x;
			this.originalY = this.y;
			
			//this.visible = false;
			this.mouseEnabled = false;
			this.mouseChildren = false;
			
			pen_mc.gotoAndStop(1);
			eraser_mc.gotoAndStop(1);
			
			toolType = DrawingToolType.PEN;
		}
		
		private var _colorType:int;
		public function get colorType():int
		{
			return _colorType;
		}
		
		public function set colorType(value:int):void
		{
			_colorType = value;
			
			pen_mc.gotoAndStop(value);
		}
		
		private var _toolType:String;
		public function get toolType():String
		{
			return _toolType;
		}
		
		public function set toolType(value:String):void
		{
			_toolType = value;
			
			if (value == DrawingToolType.PEN)
			{
				pen_mc.visible = true;
				eraser_mc.visible = false;
			}
			else
			{
				pen_mc.visible = false;
				eraser_mc.visible = true;
			}
		}
	}
}