package kr.umhannum.controls
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;
	
	import kr.umhannum.events.ChangeEvent;
	import kr.umhannum.events.ToggleButtonEvent;
	
	public class Language extends MovieClip
	{
		private var comboToggle:ToggleButton;
		
		public var combo_mc:MovieClip;
		public var korean_btn:SimpleButton;
		public var english_btn:SimpleButton;
		public function Language()
		{
			super();
			
			createChildren();
			configureListeners();
			commitProperties();
		}
		
		private function createChildren():void
		{
			comboToggle = new ToggleButton(this.combo_mc);
			this.addChild(comboToggle);
		}
		
		private function configureListeners():void
		{
			comboToggle.addEventListener(ToggleButtonEvent.CHANGE_BUTTON, toggleHandler);
			korean_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			korean_btn.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			korean_btn.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			english_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			english_btn.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			english_btn.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
		}
		
		private function commitProperties():void
		{
			comboToggle.stop();
			
			korean_btn.visible = false;
			english_btn.visible = false;
		}
		
		private function toggleHandler(event:ToggleButtonEvent):void
		{
			if (event.label == "open")
			{
				open();
			}
			else
			{
				close();
			}
		}
		
		private function open():void
		{
			korean_btn.visible = true;
			english_btn.visible = true;
		}
		
		private function close():void
		{
			korean_btn.visible = false;
			english_btn.visible = false;
		}
		
		private function mouseDownHandler(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
		}
		
		private function mouseUpHandler(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			var expression:String = String(event.currentTarget.name).split("_")[0];
			switch (expression)
			{
				case "korean":
					trace("한국어");
					dispatchEvent(new ChangeEvent(ChangeEvent.CHANGE, LanguageType.KOREAN));
					comboToggle.toggle();
					break;
				case "english":
					trace("영어");
					dispatchEvent(new ChangeEvent(ChangeEvent.CHANGE, LanguageType.ENGLISH));
					comboToggle.toggle();
					break;
				default:
					break;
			}
		}
	}
}