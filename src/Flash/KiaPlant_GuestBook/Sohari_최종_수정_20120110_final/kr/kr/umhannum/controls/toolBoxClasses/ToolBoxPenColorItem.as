package kr.umhannum.controls.toolBoxClasses
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import kr.umhannum.events.ToolBoxEvent;
	
	[Event(name="penColor", type="kr.umhannum.events.ToolBoxEvent")]
	public class ToolBoxPenColorItem extends MovieClip
	{
		public function ToolBoxPenColorItem()
		{
			super();
			
			configureListeners();
			commitProperties();
		}
		
		private function configureListeners():void
		{
			this.addEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		private function commitProperties():void
		{
			this.stop();
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			parent.dispatchEvent(new ToolBoxEvent(ToolBoxEvent.PEN_COLOR, no, color));
		}
		
		private var _no:Number = 1;
		public function get no():Number
		{
			return _no;
		}
		
		[Inspectable(name="no", type="Number", defaultValue=1)]
		public function set no(value:Number):void
		{
			_no = value;
			
			this.gotoAndStop(value);
		}
		
		private var _color:uint = 0x000000;
		public function get color():uint
		{
			return _color;
		}
		
		[Inspectable(name="color", type="Color", defaultValue=0x000000)]
		public function set color(value:uint):void
		{
			_color = value;
		}
	}
}