package kr.umhannum.controls
{
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	import kr.umhannum.core.SimpleComponent;
	import kr.umhannum.events.AlertEvent;
	
	public class ConfirmAlert extends SimpleComponent
	{
		public var ok_btn:SimpleButton;
		public function ConfirmAlert()
		{
			super();
		}
		
		override protected function configureListeners(dispatcher:IEventDispatcher=null):void
		{
			super.configureListeners(dispatcher);
			
			ok_btn.addEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			var expression:String = String(event.currentTarget.name).split("_")[0];
			switch (expression)
			{
				case "ok":
					trace("확인");
					this.dispatchEvent(new AlertEvent(AlertEvent.OK));
					break;
				default:
					break;
			}
		}
	}
}