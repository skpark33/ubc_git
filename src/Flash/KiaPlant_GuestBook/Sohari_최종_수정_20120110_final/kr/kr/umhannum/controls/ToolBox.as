package kr.umhannum.controls
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	
	import kr.umhannum.core.SimpleComponent;
	import kr.umhannum.drawing.DrawingToolType;
	import kr.umhannum.events.CustomEvent;
	import kr.umhannum.events.ToolBoxEvent;
	
	public class ToolBox extends SimpleComponent
	{
		public var pen_btn:SimpleButton;
		public var pen_mc:MovieClip;
		public var eraser_btn:SimpleButton;
		public var eraser_mc:MovieClip;
		public function ToolBox()
		{
			super();
		}
		
		override protected function configureListeners(dispatcher:IEventDispatcher=null):void
		{
			super.configureListeners(dispatcher);
			
			this.addEventListener(ToolBoxEvent.PEN_COLOR, penColorHandler);
			this.addEventListener(ToolBoxEvent.PEN_SIZE, penSizeHandler);
			this.addEventListener(ToolBoxEvent.ERASER_SIZE, eraserSizeHandler);
			
			pen_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			eraser_btn.addEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			pen_mc.gotoAndStop(1);
			eraser_mc.gotoAndStop(1);
		}
		
		private function penColorHandler(event:ToolBoxEvent):void
		{
			var colorTransform:ColorTransform = new ColorTransform();
			colorTransform.color = event.value;
			pen_mc.transform.colorTransform = colorTransform;
		}
		
		private function penSizeHandler(event:ToolBoxEvent):void
		{
			pen_mc.gotoAndStop(event.no);
		}
		
		private function eraserSizeHandler(event:ToolBoxEvent):void
		{
			eraser_mc.gotoAndStop(event.no);
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			var expression:String = String(event.currentTarget.name).split("_")[0];
			switch (expression)
			{
				case "pen":
					trace("펜");
					dispatchEvent(new CustomEvent(CustomEvent.CUSTOM_EVENT, DrawingToolType.PEN));
					break;
				case "eraser":
					trace("지우개");
					dispatchEvent(new CustomEvent(CustomEvent.CUSTOM_EVENT, DrawingToolType.ERASER));
					break;
				default:
					break;
			}
		}
	}
}