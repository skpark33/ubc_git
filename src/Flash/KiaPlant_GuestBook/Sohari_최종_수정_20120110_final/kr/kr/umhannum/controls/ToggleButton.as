package kr.umhannum.controls
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import kr.umhannum.events.ToggleButtonEvent;
	
	[Event(name="changeButton", type="kr.umhannum.events.ToggleButtonEvent")]
	public class ToggleButton extends MovieClip
	{
		private var _clip:MovieClip;
		public function ToggleButton(clip:MovieClip)
		{
			super();
			
			_clip = clip;
			
			configureListeners();
			commitProperties();
		}
		
		private function configureListeners():void
		{
			_clip.addEventListener(MouseEvent.CLICK, clickHandler);
			_clip.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			_clip.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
		}
		
		private function mouseDownHandler(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
		}
		
		private function mouseUpHandler(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			toggle();
		}
		
		private function commitProperties():void
		{
			_clip.gotoAndStop(1);
		}
		
		public function toggle():void
		{
			if (_clip.currentFrame == _clip.totalFrames)
				_clip.gotoAndStop(1);
			else
				_clip.nextFrame();
			
			this.dispatchEvent(new ToggleButtonEvent(ToggleButtonEvent.CHANGE_BUTTON, _clip.currentFrameLabel));
		}
	}
}