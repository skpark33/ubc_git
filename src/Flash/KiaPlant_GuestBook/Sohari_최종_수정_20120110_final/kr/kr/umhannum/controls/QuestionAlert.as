package kr.umhannum.controls
{
	import flash.display.SimpleButton;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	import kr.umhannum.core.SimpleComponent;
	import kr.umhannum.events.AlertEvent;
	
	public class QuestionAlert extends SimpleComponent
	{
		public var yes_btn:SimpleButton;
		public var no_btn:SimpleButton;
		public function QuestionAlert()
		{
			super();
		}
		
		override protected function configureListeners(dispatcher:IEventDispatcher=null):void
		{
			super.configureListeners(dispatcher);
			
			yes_btn.addEventListener(MouseEvent.CLICK, clickHandler);
			no_btn.addEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			var expression:String = String(event.currentTarget.name).split("_")[0];
			switch (expression)
			{
				case "yes":
					trace("예");
					this.dispatchEvent(new AlertEvent(AlertEvent.YES));
					break;
				case "no":
					trace("아니오");
					this.dispatchEvent(new AlertEvent(AlertEvent.NO));
					break;
				default:
					break;
			}
		}
	}
}