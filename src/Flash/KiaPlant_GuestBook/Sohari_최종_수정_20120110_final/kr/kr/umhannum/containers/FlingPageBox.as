﻿package kr.umhannum.containers
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	import kr.umhannum.containers.flingPageBoxClasses.FlingPageBoxSacleMode;
	
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.easing.Sine;
	import org.libspark.betweenas3.tweens.ITween;
	
	public class FlingPageBox extends Sprite
	{
		[Embed(source="/assets/Skin.swf", symbol="Pagination_Skin")]
		private var Pagination_Skin:Class;
		
		private var eventListenerStack:Array;
		private var paginationArray:Array;
		
		private var direction:int;
		private var countPageNum:int;
		
		private var old_mouseX:Number;
		private var distanceX:Number;
		private var movePageNum:Number;
		private var old_pageNum:Number;
		private var currentPageNum:Number;
		
		private var initialized:Boolean;
		private var mouseMoving:Boolean;
		private var holding:Boolean;
		public var tweening:Boolean;
		
		private var selectedNum:MovieClip;
		public function FlingPageBox()
		{
			super();
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler, false, 0, true);
			addEventListener(Event.REMOVED_FROM_STAGE, removeFromStageHandler, false, 0, true);
		}
		
		/**
		 * Event.ADDED_TO_STAGE 이벤트 핸들러
		 * @param event
		 * 
		 */		
		private function addedToStageHandler(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			
			stageInitialize();
		}
		
		/**
		 * Event.REMOVED_FROM_STAGE 이벤트 핸들러
		 * @param event
		 * 
		 */		
		private function removeFromStageHandler(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, removeFromStageHandler);
			
			removePagination();
		}
		
		public final function initialize():void
		{
			if (!initialized)
			{
				initialized = true;
				
				createChildren();
				commitProperties();
				configureListeners();
				
				arrange();
				createPagination();
			}
		}
		
		protected function createChildren():void
		{
		}
		
		protected function commitProperties():void
		{
			trace("guestbook.lastPageNum : " + guestbook.lastPageNum);
			
			old_pageNum = currentPageNum = guestbook.lastPageNum;
			minimumPageNum = loop && totalPageNum > 2 ? 0 : totalPageNum;
			mouseMoving = false;
		}
		
		protected function configureListeners(dispatcher:IEventDispatcher=null):void
		{
			stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
		}
		
		protected function removeListeners(dispatcher:IEventDispatcher=null):void
		{
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
		}
		
		private function mouseDownHandler(event:MouseEvent):void
		{
			movePageNum = 0;
			distanceX = 0;
			old_mouseX = event.stageX;
			
			if (stage) stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			this.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			this.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		private function mouseUpHandler(event:MouseEvent):void
		{
			snapPage();
			
			mouseMoving = false;
			
			if (stage) stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			this.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			this.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		private function mouseMoveHandler(event:MouseEvent):void
		{
			mouseMoving = true;
			distanceX = event.stageX - old_mouseX;
			//trace("distanceX : " + distanceX);
		}
		
		private function enterFrameHandler(event:Event):void
		{
			if (!mouseMoving) return;
			
			movePageNum = distanceX / boundary.width;
			currentPageNum = ((old_pageNum - movePageNum) - old_pageNum) / (distanceX - old_mouseX) * (distanceX - old_mouseX) + old_pageNum;
			
			moveFlingPage();
		}
		
		/**
		 * 스테이지에 붙고 나서 실행 되는 함수 
		 * @param event
		 * 
		 */		
		protected function stageInitialize():void
		{
			initialize();
		}
		
		private function updatePagePosition():void
		{
			var len:int = flingPageArray.length;
			for ( var i:int=0; i<len; i++ )
			{
				var flingPage:FlingPage = flingPageArray[i] as FlingPage;
				flingPage.originalX = flingPage.x;
			}
		}
		
		private function moveFlingPage():void
		{
			var len:int = flingPageArray.length;
			for ( var i:int=0; i<len; i++ )
			{
				var flingPage:FlingPage = flingPageArray[i] as FlingPage;
				flingPage.targetX = flingPage.originalX + distanceX;
				flingPage.x += 0.8 * (flingPage.targetX - flingPage.x);
			}
		}
		
		public function prev():void
		{
			if (tweening) return;
			
			movePageNum = 1;
			currentPageNum--;
			snapPage();
		}
		
		public function next():void
		{
			if (tweening) return;
			
			movePageNum = -1;
			currentPageNum++;
			snapPage();
		}
		
		public function gotoPage(value:int):void
		{
			trace("페이지 바로가기");
			// if (value == 0) return; // 이걸 왜 막았는지..ㅎㅎ 모르겠고, 이걸 해제하니깐, 페이지 네비 정렬 오류가 사라짐
			
			direction = -1 * value;
			holding = false;;			
			old_pageNum = currentPageNum = value;
			
			skipMove();
		}
		
		private function snapPage():void
		{
			currentPageNum = Math.round(currentPageNum);
			if (isNaN(movePageNum) || Math.abs(movePageNum) < 0.5)
			{
				// trace("현재페이지 유지");
				direction = 0;
				currentPageNum = old_pageNum;
				holding = true;
			}
			else
			{
				// trace("페이지이동");
				if (movePageNum < 0)
				{
					if (totalPageNum == minimumPageNum && currentPageNum == totalPageNum)
					{
						// trace("현재페이지 유지");
						direction = 0;
						currentPageNum = old_pageNum;
						holding = true;
					}
					else
					{
						// trace("왼쪽으로 이동");
						direction = -1;
						holding = false;
					}
				}
				else
				{
					if (totalPageNum == minimumPageNum && currentPageNum == -1)
					{
						// trace("현재페이지 유지");
						direction = 0;
						currentPageNum = old_pageNum;
						holding = true;
					}
					else
					{
						// trace("오른쪽으로 이동");
						direction = 1;
						holding = false;
					}
				}
			}
			
			// 바로가기면, 페이지 스킵 형태로 이동, 아니면, 스냅 형태로 이동
			snapTween();
			
			if (currentPageNum < 0)	
				currentPageNum = totalPageNum-1;
			else if (currentPageNum > totalPageNum-1)
				currentPageNum = 0;
			
			old_pageNum = currentPageNum;
			
			// 마지막 페이지 기록
			guestbook.lastPageNum = currentPageNum;
		}
		
		private function snapTween():void
		{
			countPageNum = 0;
			tweening = true;
			
			var len:int = flingPageArray.length;
			for ( var i:int=0; i<len; i++ )
			{
				var flingPage:FlingPage = flingPageArray[i] as FlingPage;
				flingPage.targetX = flingPage.originalX + offset;
				
				var snapTween:ITween = BetweenAS3.tween(flingPage, {x:flingPage.targetX}, null, 0.2, Sine.easeInOut);
				snapTween.play();
				snapTween.onComplete = function():void
				{
					countPageNum++;
					if (countPageNum == totalPageNum)
					{
						tweening = false;
						// trace("페이지 재정렬");
						arrange();
					}
				};
			}
		}
		
		private function skipMove():void
		{
			var len:int = flingPageArray.length;
			for ( var i:int=0; i<len; i++ )
			{
				var flingPage:FlingPage = flingPageArray[i] as FlingPage;
				flingPage.x = flingPage.originalX + offset;
			}
			arrange();
		}
		
		private function arrange():void
		{
			if (holding) return;
			
			var len:int = flingPageArray.length;
			for ( var i:int=0; i<len; i++ )
			{
				var flingPage:FlingPage = flingPageArray[i] as FlingPage;
				if (i >= currentPageNum-1 && i <= currentPageNum+1)
				{
					// trace("i : " + i);
				}
			}
			
			if (len < 3)
			{
				updatePagePosition();
				return;
			}
			
			var referenceValue:MovieClip;
			var returnValue:MovieClip;
			if (direction < 0)
			{
				// trace("왼쪽으로 이동");
				// 왼쪽으로 이동
				referenceValue = flingPageArray[flingPageArray.length-1];
				returnValue = flingPageArray.shift();		
				returnValue.x = referenceValue.x + boundary.width;
				returnValue.y = referenceValue.y;
				flingPageArray.push(returnValue);
				
			}
			else
			{
				// trace("오른쪽으로 이동");
				// 오른쪽으로 이동
				referenceValue = flingPageArray[0];
				returnValue = flingPageArray.pop();
				returnValue.x = referenceValue.x - boundary.width;
				returnValue.y = referenceValue.y;
				flingPageArray.unshift(returnValue);
			}
			updatePagePosition();
		}
		
		private function createPagination():void
		{
			// trace("createPagination");
			removePagination();
			
			paginationArray = [];
			var len:int = flingPageArray.length;
			for ( var i:int=0; i<len; i++ )
			{
				var skinAssets:Sprite = new Pagination_Skin() as Sprite;
				var movieClip:MovieClip = skinAssets.getChildByName("assets") as MovieClip;
				movieClip.no = i;
				movieClip.txt.text = String(1+i);
				movieClip.x = (boundary.width - 38 * totalPageNum + 10 ) / 2 + 38 * i;
				movieClip.y = 63;
				movieClip.addEventListener(Event.ENTER_FRAME, numHandler);
				paginationArray.push(movieClip);
				stage.addChild(movieClip);
			}
			// trace("paginationArray : " + paginationArray);
		}
		
		private function removePagination():void
		{
			// trace("paginationArray : " + paginationArray);
			if (null == paginationArray) return;
			
			var len:int = paginationArray.length;
			for ( var i:int=0; i<len; i++ )
			{
				var movieClip:MovieClip = paginationArray[i] as MovieClip;
				movieClip.removeEventListener(Event.ENTER_FRAME, numHandler);
				stage.removeChild(movieClip);
				movieClip = null;
				paginationArray[i] = null;
			}
			paginationArray = [];
		}
		
		private function numHandler(event:Event):void
		{
			var target:MovieClip = event.currentTarget as MovieClip;
			if (target.no == currentPageNum)
			{
				target.nextFrame();
			}
			else
			{
				target.prevFrame();
			}
		}
		
		private function createFlingPage(child:DisplayObject):FlingPage
		{
			var flingPage:FlingPage = child as FlingPage;
			flingPage.originalX = flingPage.x = position;
			//flingPage.no = totalPageNum;
			// trace("position : " + position);
			flingPage.y = 0;
			flingPage.graphics.beginFill(0xffffff, 0);
			flingPage.graphics.drawRect(0, 0, boundary.width, boundary.height);
			flingPage.graphics.endFill();
			
			return flingPage;
		}
		
		override public function addChild(child:DisplayObject):DisplayObject
		{
			if (null == flingPageArray) flingPageArray = [];
			
			var flingPage:FlingPage = createFlingPage(child) as FlingPage;
			flingPageArray.push(flingPage);
			
			return super.addChild(flingPage);
		}
		
		override public function removeChild(child:DisplayObject):DisplayObject
		{
			var len:int = flingPageArray.length;
			for ( var i:int=0; i<len; i++ )
			{
				var flingPage:FlingPage = flingPageArray[i] as FlingPage;
				if (flingPage.contains(child))
				{
					flingPageArray.splice(i, 1);
					break;
				}
			}
			return super.removeChild(flingPage);
		}
		
		override public function addEventListener(type:String, listener:Function, useCapture:Boolean=false, priority:int=0, useWeakReference:Boolean=false):void
		{
			if( !eventListenerStack ) eventListenerStack = [];
			eventListenerStack[eventListenerStack.length] = {type:type, listener:listener, useCapture:useCapture};
			
			super.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		override public function removeEventListener(type:String, listener:Function, useCapture:Boolean=false):void
		{
			if (eventListenerStack)
			{
				var removeIndex:int = -1;
				var len:int = eventListenerStack.length;
				for ( var i:int=0; i<len; i++ )
				{
					var obj:Object = eventListenerStack[i];
					if (obj.type == type && obj.listener == listener)
					{
						removeIndex = i;
					}
				}
				if (removeIndex > 0) eventListenerStack.splice(removeIndex, 1);
			}
			super.removeEventListener(type, listener, useCapture);
		}
		
		public function dispose():void
		{
			var len:int = this.numChildren;
			while (len > 0)
			{
				this.removeChild(this.getChildAt(0));
				len--;
			}
			flingPageArray = [];
			
			if (eventListenerStack)
			{
				while (eventListenerStack.length > 0)
				{
					var obj:Object = eventListenerStack.shift();
					this.removeEventListener(obj.type, obj.listener, obj.useCapture);
				}
				eventListenerStack = null;
			}
		}
		
		private var _flingPageArray:Array;
		public function get flingPageArray():Array
		{
			if (null == _flingPageArray)
				_flingPageArray = [];
			return _flingPageArray;
		}
		
		public function set flingPageArray(value:Array):void
		{
			_flingPageArray = value;
		}
		
		private var _minimumPageNum:int;
		public function get minimumPageNum():int
		{
			return _minimumPageNum;
		}
		
		public function set minimumPageNum(value:int):void
		{
			_minimumPageNum = value;
		}
		
		private var _totalPageNum:int;
		public function get totalPageNum():int
		{
			_totalPageNum = flingPageArray.length;
			return _totalPageNum;
		}
		
		private var _offset:Number;
		public function get offset():Number
		{
			_offset = direction * boundary.width;
			return _offset;
		}
		
		private var _position:Number;
		protected function get position():Number
		{
			_position = totalPageNum * boundary.width;
			return _position;
		}
		
		private var _boundary:Rectangle;
		public function get boundary():Rectangle
		{
			if (null == _boundary)
				_boundary = new Rectangle();
			return _boundary;
		}
		
		public function set boundary(value:Rectangle):void
		{
			_boundary = value;
		}
		
		private var _scaleMode:String = FlingPageBoxSacleMode.NO_SCALE;
		public function get scaleMode():String
		{
			return _scaleMode;
		}
		
		public function set scaleMode(value:String):void
		{
			_scaleMode = value;
		}
		
		private var _loop:Boolean = false;
		public function get loop():Boolean
		{
			return _loop;
		}
		
		public function set loop(value:Boolean):void
		{
			_loop = value;
		}
		
		private var _blurred:Boolean;
		public function get blurred():Boolean
		{
			return _blurred;
		}
		
		public function set blurred(value:Boolean):void
		{
			if (_blurred && !value)
				FlingPage(flingPageArray[currentPageNum]).blurred = false;
			_blurred = value;
			
			if (value)
				FlingPage(flingPageArray[currentPageNum]).blurred = true;
		}
	}
}