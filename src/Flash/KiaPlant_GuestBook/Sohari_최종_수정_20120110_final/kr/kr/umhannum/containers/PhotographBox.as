package kr.umhannum.containers
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.media.Video;
	
	import kr.umhannum.assist.CountTimer;
	import kr.umhannum.controls.ReCaptrueAlert;
	import kr.umhannum.core.SimpleComponent;
	import kr.umhannum.drawing.DrawingBoard;
	import kr.umhannum.effect.TweenStatusType;
	import kr.umhannum.events.AlertEvent;
	import kr.umhannum.events.DrawingBoardEvent;
	import kr.umhannum.events.TweenStatusEvent;
	import kr.umhannum.media.SnapshotCamera;
	
	import org.libspark.betweenas3.BetweenAS3;
	import org.libspark.betweenas3.easing.*;
	import org.libspark.betweenas3.tweens.ITween;
	
	public class PhotographBox extends SimpleComponent
	{
		private var tween:ITween;
		public var snapshotCamera:SnapshotCamera;
		public var count_mc:MovieClip;
		public var capture_btn:SimpleButton;
		
		public var drawingBoard:DrawingBoard;
		public function PhotographBox()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
		}
		
		override protected function configureListeners(dispatcher:IEventDispatcher=null):void
		{
			super.configureListeners(dispatcher);
			
			capture_btn.addEventListener(MouseEvent.CLICK, clickHandler);			
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			snapshoted = false;
			
			count_mc.gotoAndStop(1);
			count_mc.visible = false;
		}
		
		private function createSnapshotCamera():void
		{
			trace("createSnapshotCamera");
			/*if (!snapshotCamera)
			{
				snapshotCamera = new SnapshotCamera(PhotographBox.CAMERA_WIDTH, PhotographBox.CAMERA_HEIGHT);
				cameraBox.addChild(snapshotCamera);
			}*/
		}
		
		// 사진 촬영 여부
		public var snapshoted:Boolean;
		private function clickHandler(event:MouseEvent):void
		{
			if (snapshoted)
			{
				trace("다시 촬영하시겠습니까?");
				createReCaptrueAlert();
				return;
			}
			trace("clickHandler");
			removeSnapshot();
			counter();
			snapshoted = true;
		}
		
		private var reCaptrueAlert:ReCaptrueAlert;
		private function createReCaptrueAlert():void
		{
			if (!reCaptrueAlert)
			{
				var bounds:Rectangle = this.getBounds(stage);
				trace("bounds : " + bounds);
				reCaptrueAlert = new ReCaptrueAlert();
				reCaptrueAlert.x = -1 * bounds.x;
				reCaptrueAlert.y = -1 * bounds.y;
				reCaptrueAlert.addEventListener(AlertEvent.YES, emailYesHandler);
				reCaptrueAlert.addEventListener(AlertEvent.NO, emailNoHandler);
				reCaptrueAlert.alpha = 0;
				reCaptrueAlert.attach();
				this.addChild(reCaptrueAlert);
			}
		}
		
		private function emailYesHandler(event:AlertEvent):void
		{
			if (reCaptrueAlert)
			{
				this.removeChild(reCaptrueAlert);
				reCaptrueAlert.dispose();
				reCaptrueAlert = null;
			}
			removeSnapshot();
			counter();
			snapshoted = true;
		}
		
		private function emailNoHandler(event:AlertEvent):void
		{
			if (reCaptrueAlert)
			{
				this.removeChild(reCaptrueAlert);
				reCaptrueAlert.dispose();
				reCaptrueAlert = null;
			}
		}
		
		private var countTimer:CountTimer;
		private function counter():void
		{
			if (!countTimer)
			{
				countTimer = new CountTimer(1500, 2, countMotion, countComplete);
				countTimer.start();
			}
			countMotion();
		}
		
		private function countMotion():void
		{
			if (null != tween) tween.stop();
			tween = BetweenAS3.tween(count_mc, {alpha:1, scaleX:1, scaleY:1}, {alpha:0, scaleX:0.5, scaleY:0.5}, 1, Elastic.easeInOut);
			tween.onPlay = function():void
			{
				counted = false;
				
				count_mc.visible = true;
				count_mc.gotoAndStop(1+countTimer.currentCount);
				
				capture_btn.visible = false;
			};
			
			tween.onComplete = function():void
			{
				if (counted)
				{
					count_mc.visible = false;
					capture_btn.visible = true;
					
					createSnapshot();
				}
			};
			tween.play();
		}
		
		private var counted:Boolean;
		private function countComplete():void
		{
			trace("countComplete");
			counted = true;
			
			countTimer.removeListeners();
			countTimer = null;
		}
		
		private var snapshotBitmap:Bitmap;
		private function createSnapshot():void
		{
			if (!snapshotBitmap)
			{
				var bitmapWidth:int = snapshotCamera.width;
				var bitmapHeight:int = snapshotCamera.height;
				var bitmapData:BitmapData = new BitmapData(bitmapWidth, bitmapHeight);
				bitmapData.draw(snapshotCamera);
				snapshotBitmap = new Bitmap(bitmapData);
				snapshotCamera.addChild(snapshotBitmap);
			}
		}
		
		private function removeSnapshot():void
		{
			trace("snapshotBitmap", snapshotBitmap);
			if (snapshotBitmap)
			{
				trace("통과");
				snapshotCamera.removeChild(snapshotBitmap);
				snapshotBitmap.bitmapData = null;
				snapshotBitmap = null;
			}
			snapshoted = false;
		}
		
		public function clearDrawingBoard():void
		{
			drawingBoard.clear();
		}
		
		public function clearSnapshot():void
		{
			removeSnapshot();
		}
		
		public function visibleCaptureButton(value:Boolean):void
		{
			capture_btn.visible = value;
		}
	}
}