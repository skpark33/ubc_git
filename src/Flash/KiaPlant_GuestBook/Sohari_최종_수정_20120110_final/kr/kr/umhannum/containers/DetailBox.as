package kr.umhannum.containers
{
	import flash.events.IEventDispatcher;
	import flash.events.ProgressEvent;
	
	import kr.umhannum.core.SimpleComponent;
	import kr.umhannum.events.ImageLoaderEvent;
	import kr.umhannum.net.ImageLoader;
	
	public class DetailBox extends SimpleComponent
	{
		public function DetailBox()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			initializeImageLoader();
		}
		
		override protected function configureListeners(dispatcher:IEventDispatcher=null):void
		{
			super.configureListeners(dispatcher);
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
		}
		
		private var imageLoader:ImageLoader;
		private function initializeImageLoader():void
		{
			if (!imageLoader)
			{
				imageLoader = new ImageLoader();
				imageLoader.addEventListener(ImageLoaderEvent.DATA_COMPLETE, dataCompleteHandler);
				imageLoader.addEventListener(ProgressEvent.PROGRESS, progressHandler);
				this.addChild(imageLoader);
			}
		}
		
		private function dataCompleteHandler(event:ImageLoaderEvent):void
		{
			trace("dataCompleteHandler : ");
		}
		
		private function progressHandler(event:ProgressEvent):void
		{
			dispatchEvent(event.clone());
		}
		
		public function load(url:String):void
		{
			imageLoader.load(url);
		}
		
		public function unload():void
		{
			imageLoader.unload();
		}
	}
}