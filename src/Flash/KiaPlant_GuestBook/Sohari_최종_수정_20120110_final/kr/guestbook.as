﻿package
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.geom.Rectangle;
	import flash.system.Security;
	import flash.text.TextField;
	
	import kr.umhannum.containers.FlingPage;
	import kr.umhannum.containers.FlingPageBox;
	import kr.umhannum.containers.flingPageBoxClasses.FlingPageBoxSacleMode;
	import kr.umhannum.controls.Language;
	import kr.umhannum.controls.LanguageType;
	import kr.umhannum.events.ChangeEvent;
	import kr.umhannum.events.FlingPageEvent;
	import kr.umhannum.events.SWFLoaderEvent;
	import kr.umhannum.events.SocketEvent;
	import kr.umhannum.events.UBCSocketEvent;
	import kr.umhannum.events.UBCSocketStatusEvent;
	import kr.umhannum.events.XMLLoaderEvent;
	import kr.umhannum.net.SWFLoader;
	import kr.umhannum.net.UBCSocket;
	import kr.umhannum.net.XMLLoader;
	
	public class guestbook extends Sprite
	{
		public static const STAGE_WIDTH:int = 768;
		public static const STAGE_HEIGHT:int = 1366;
		
		public static const LANGUAGE_X:int = 606;
		public static const LANGUAGE_Y:int = 0;
		
		public static const PREVIEW_PER_PAGE:int = 35;
		
		public static const XML_PATH:String = "./Plugins/data/guestbook.xml";
		public static const LIST_XML_PATH:String = "./Plugins/data/KIA_GUESTBOOKlist.xml";
		public static const WRITE_FILENAME:String = "./writing_kr.swf";
		public static const DETAIL_FILENAME:String = "./Detail_kr.swf";
		//public static const LIST_FILE_PATH:String = "./Plugins/Wedding/GuestBook/KIA_GUESTBOOK/";
		
		//public static const XML_PATH:String = "c:/sqisoft/contents/enc/Plugins/data/guestbook.xml";
		//public static const LIST_XML_PATH:String = "c:/sqisoft/contents/enc/Plugins/data/KIA_GUESTBOOKlist.xml";
		
		private var writeLoader:SWFLoader;
		private var detailLoader:SWFLoader;
		
		private var flingPageBox:FlingPageBox;
		
		private var currentSocketType:String;
		private var currentSocketData:String;
		private var callLaterFunc:Function;
		
		// 마지막 페이지 기록
		public static var lastPageNum:int;
		// 삭제 작업이 있었는지 유무
		public static var deletedPhotograph:Boolean;
		// 사진 저장이 있었는지 유무
		public static var savedPhotograph:Boolean;
		
		// public var debug_txt:TextField;
		public var prev_btn:SimpleButton;
		public var next_btn:SimpleButton;
		public var guestbook_btn:SimpleButton;
		private var xmlLoader:XMLLoader;
		public function guestbook()
		{
			super();
			
			initialize();
		}
		
		private function initialize():void
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.EXACT_FIT;
			
			// debug_txt.text = "";
			
			initializeXMLLoad();
			configureListeners();
		}
		
		private function configureListeners():void
		{
			prev_btn.addEventListener(MouseEvent.MOUSE_DOWN, clickHandler);
			next_btn.addEventListener(MouseEvent.MOUSE_DOWN, clickHandler);
			guestbook_btn.addEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		private function clickHandler(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
			
			var expression:String = String(event.currentTarget.name).split("_")[0];
			switch (expression)
			{
				case "prev":
					flingPageBox.prev();
					break;
				case "next":
					flingPageBox.next();
					break;
				case "guestbook":
					flingPageBoxBlurred(true);
					createWrite();
					break;
				default:
					break;
			}
		}
		
		private function createWrite():void
		{
			if (!writeLoader)
			{
				writeLoader = new SWFLoader();
				writeLoader.addEventListener(SWFLoaderEvent.LOAD_COMPLETE, writeLoadCompleteHandler);
				writeLoader.addEventListener(ProgressEvent.PROGRESS, progressHandler);
				writeLoader.load(guestbook.WRITE_FILENAME);
				
				addChild(writeLoader);
			}
		}
		
		private function removeWrite():void
		{
			if (writeLoader)
			{
				writeLoader.removeEventListener(SWFLoaderEvent.LOAD_COMPLETE, writeLoadCompleteHandler);
				writeLoader.removeEventListener(ProgressEvent.PROGRESS, progressHandler);
				writeLoader.unload();
				
				removeChild(writeLoader);
				writeLoader = null;
			}
		}
		
		private function writeLoadCompleteHandler(event:SWFLoaderEvent):void
		{
			trace("writeLoadCompleteHandler");
			
			var displayObject:DisplayObject = event.displayObject as DisplayObject;
			var content:Sprite = displayObject as Sprite;
			content.addEventListener(Event.CLOSE, writeCloseHandler);
			content.addEventListener(SocketEvent.SEND, writeSendHandler);
			
			// debug_txt.text = stageBounds.x + " :: " + stageBounds.width + " :: " + stageBounds.height;
		}
		
		private function writeCloseHandler(event:Event):void
		{
			flingPageBoxBlurred(false);
			
			removeWrite();
			
			if (guestbook.savedPhotograph)
				list_xmlloader.load(guestbook.LIST_XML_PATH);
		}
		
		private function writeSendHandler(event:SocketEvent):void
		{
			currentSocketType = "write";
			currentSocketData = String(event.query);
			// debug_txt.appendText("event.callLaterFunc : " + event.callLaterFunc + "\n");
			if (null != event.callLaterFunc)
				callLaterFunc = event.callLaterFunc as Function;
			
			connectionDataSocket();
		}
		
		private var language:Language;
		private function createLanguage():void
		{
			if (!language)
			{
				language = new Language();
				language.x = guestbook.LANGUAGE_X;
				language.y = guestbook.LANGUAGE_Y;
				language.addEventListener(ChangeEvent.CHANGE, languageChangeHandler);
				stage.addChild(language);
			}
		}
		
		private function languageChangeHandler(event:ChangeEvent):void
		{
			currentSocketType = "language";
			currentSocketData = String(event.value);
			
			connectLanguageSocket();
		}
		
		private function createDetail():void
		{
			if (!detailLoader)
			{
				detailLoader = new SWFLoader();
				detailLoader.addEventListener(SWFLoaderEvent.LOAD_COMPLETE, detailLoadCompleteHandler);
				detailLoader.addEventListener(ProgressEvent.PROGRESS, progressHandler);
				detailLoader.load(guestbook.DETAIL_FILENAME);
				
				addChild(detailLoader);
			}
		}
		
		private function removeDetail():void
		{
			if (detailLoader)
			{
				detailLoader.removeEventListener(SWFLoaderEvent.LOAD_COMPLETE, detailLoadCompleteHandler);
				detailLoader.removeEventListener(ProgressEvent.PROGRESS, progressHandler);
				detailLoader.unload();
				
				removeChild(detailLoader);
				detailLoader = null;
			}
			
			if (guestbook.deletedPhotograph)
				list_xmlloader.load(guestbook.LIST_XML_PATH);
			
			flingPageBoxBlurred(false);
		}
		
		private function detailLoadCompleteHandler(event:SWFLoaderEvent):void
		{
			var displayObject:DisplayObject = event.displayObject as DisplayObject;
			var content:Detail = displayObject as Detail;
			content.data = filenameList;
			content.load(pageNum, imageNum);
			content.addEventListener(Event.CLOSE, detailCloseHandler);
			content.addEventListener(SocketEvent.SEND, detailSendHandler);
		}
		
		private function detailCloseHandler(event:Event):void
		{
			removeDetail();
		}
		
		private function detailSendHandler(event:SocketEvent):void
		{
			currentSocketType = "detail";
			currentSocketData = String(event.query);
			if (null != event.callLaterFunc)
				callLaterFunc = event.callLaterFunc as Function;
			
			connectionDataSocket();
		}
		
		private function progressHandler(event:ProgressEvent):void
		{
			var swfLoader:SWFLoader = event.currentTarget as SWFLoader;
			//trace("progressHandler bytesLoaded : " + event.bytesLoaded);
			//trace("progressHandler bytesTotal : " + event.bytesTotal);
			if (event.bytesLoaded == event.bytesTotal )
			{
			}
		}
		
		// XMLLoad 초기화
		private function initializeXMLLoad():void
		{
			if (!xmlLoader)
			{
				xmlLoader = new XMLLoader();
				xmlLoader.addEventListener(XMLLoaderEvent.DATA_COMPLETE, onXMLLoadComplete);
				xmlLoader.load(guestbook.XML_PATH);
			}
		}
		
		// XMLLoad Complete Handler
		private var dataXML:XML;
		public static var configure:XMLList;
		private var position:XMLList;
		private function onXMLLoadComplete(event:XMLLoaderEvent):void
		{
			dataXML = event.xml as XML;
			//trace("dataXML : " + dataXML);
			configure = dataXML.configure as XMLList;
			position = dataXML..position as XMLList;
			
			//initializeSocket();
			call_listXML();
		}
		
		// UBC플레이어와 소켓 초기화
		private var dataSocket:UBCSocket;		
		private function connectionDataSocket():void
		{
			// debug_txt.text = "connectionDataSocket\n";
			// debug_txt.text = "dataSocket : " + dataSocket + "\n";
			
			if (!dataSocket)
			{
				dataSocket = new UBCSocket(configure.url, configure.dataPort);
				dataSocket.addEventListener(Event.CLOSE, dataSocketCloseHandler);
				dataSocket.addEventListener(UBCSocketStatusEvent.SOCKET_STATUS, socketStatusHandler);
				dataSocket.addEventListener(UBCSocketEvent.SOCKET_DATA_COMPLETE, socketDataCompleteHandler);
			}
		}
		
		private var languageSocket:UBCSocket;
		private function connectLanguageSocket():void
		{
			if (!languageSocket)
			{
				languageSocket = new UBCSocket(configure.url, configure.languagePort);
				languageSocket.addEventListener(Event.CHANGE, languageSocketCloseHandler);
				languageSocket.addEventListener(UBCSocketStatusEvent.SOCKET_STATUS, socketStatusHandler);
				languageSocket.addEventListener(UBCSocketEvent.SOCKET_DATA_COMPLETE, socketDataCompleteHandler);
			}
		}
		
		private function socketStatusHandler(event:UBCSocketStatusEvent):void
		{
			// debug_txt.appendText("socketStatusHandler\n");
			// debug_txt.appendText("socket status : " + event.status + "\n");
		}
		
		private function dataSocketCloseHandler(event:Event):void
		{
			// debug_txt.appendText("dataSocketCloseHandler\n");
			// debug_txt.appendText("dataSocket.connected : " + dataSocket.connected + "\n");
			
			dataSocket.removeEventListener(Event.CLOSE, dataSocketCloseHandler);
			dataSocket.removeEventListener(UBCSocketEvent.SOCKET_DATA_COMPLETE, socketDataCompleteHandler);
			dataSocket.close();
			dataSocket = null;
		}
		
		private function languageSocketCloseHandler(event:Event):void
		{
			// debug_txt.appendText("languageSocketCloseHandler\n");
			// debug_txt.appendText("languageSocket.connected : " + languageSocket.connected + "\n");
			
			languageSocket.removeEventListener(Event.CLOSE, languageSocketCloseHandler);
			languageSocket.removeEventListener(UBCSocketEvent.SOCKET_DATA_COMPLETE, socketDataCompleteHandler);
			languageSocket.close();
			languageSocket = null;
		}
		
		// 소켓 연결 Complete Handler
		private function socketDataCompleteHandler(event:UBCSocketEvent):void
		{
			trace(event.currentTarget, "event.response : " + event.response);
			// debug_txt.appendText("currentSocketType : " + currentSocketType + " :: " + "event.response : " + event.response + "\n");
			// debug_txt.appendText(dataSocket.connected + "\n");
			
			if (event.response == "connect")
			{
				if ("detail" == currentSocketType)
				{
					dataSocket.send(currentSocketData);
				}
				else if ("write" == currentSocketType)
				{
					dataSocket.send(currentSocketData);
				}
				else
				{
					languageSocket.send(currentSocketData);
				}
			}
			else if (event.response == "OK")
			{
				if ("detail" == currentSocketType)
				{
					removeDetail();
				}
				else if ("write" == currentSocketType)
				{
					// debug_txt.appendText("callLaterFunc : " + callLaterFunc);
					callLaterFunc();
				}
			}
		}
		
		private var list_xmlloader:XMLLoader;
		private function call_listXML():void
		{
			if (!list_xmlloader)
			{
				list_xmlloader = new XMLLoader();
				list_xmlloader.addEventListener(XMLLoaderEvent.DATA_COMPLETE, listDataCompleteHandler);
				list_xmlloader.load(guestbook.LIST_XML_PATH);
			}
		}
		
		private var filenameList:XMLList;
		private function listDataCompleteHandler(event:XMLLoaderEvent):void
		{
			filenameList = generateXML(event.xml);
			
			createFlingPageBox();
			createLanguage();
		}
		
		private function generateXML(xml:XML):XMLList
		{
			var filenameList:XMLList = xml..filename as XMLList;
			var result:XML = new XML(<list/>);
			var pagesNum:int = int((filenameList.length()-1)/guestbook.PREVIEW_PER_PAGE)+1;
			for ( var i:int=0; i<pagesNum; i++ )
			{
				var files:XML = new XML(<files/>);
				files.@no = 1+i;
				
				var startIndex:int = i*guestbook.PREVIEW_PER_PAGE;
				var len:int = (i+1)*guestbook.PREVIEW_PER_PAGE;
				for ( var j:int=startIndex; j<len; j++ )
				{
					if (null == filenameList[j]) continue;
					
					files.appendChild(filenameList[j]);
				}
				result.appendChild(files);
			}
			return result.files;
		}
		
		private function createFlingPageBox():void
		{
			removeFlingPageBox();
			
			if (!flingPageBox)
			{
				flingPageBox = new FlingPageBox();
				flingPageBox.loop = true;
				flingPageBox.boundary = new Rectangle(0, 0, guestbook.STAGE_WIDTH, guestbook.STAGE_HEIGHT);
				flingPageBox.scaleMode = FlingPageBoxSacleMode.NO_SCALE;
				var len:int = filenameList.length();
				for ( var i:int=0; i<len; i++ )
				{
					var position:XMLList = position[i%2].value as XMLList;
					var flingPage:FlingPage = new FlingPage(position);
					flingPage.no = i;
					flingPage.data = filenameList[i].filename as XMLList;
					flingPage.addEventListener(FlingPageEvent.SELECT, selectHandler);
					flingPageBox.addChild(flingPage);
				}
				
				var childIndex:int = this.getChildIndex(guestbook_btn);
				this.addChildAt(flingPageBox, childIndex);
				
				guestbook.lastPageNum = len-1;
				
				flingPageBox.initialize();
				flingPageBox.gotoPage(guestbook.lastPageNum);
			}
		}
		
		private function removeFlingPageBox():void
		{
			if (flingPageBox)
			{
				this.removeChild(flingPageBox);
				flingPageBox.dispose();
				flingPageBox = null;
			}
		}
		
		private function selectHandler(event:FlingPageEvent):void
		{
			_pageNum = event.pageNum;
			_imageNum = event.imageNum;
			
			createDetail();
		}
		
		public function flingPageBoxBlurred(value:Boolean):void
		{
			flingPageBox.blurred = value;
		}
		
		private var _pageNum:int;
		public function get pageNum():int
		{
			return _pageNum;
		}
		
		private var _imageNum:int;
		public function get imageNum():int
		{
			return _imageNum;
		}
	}
}