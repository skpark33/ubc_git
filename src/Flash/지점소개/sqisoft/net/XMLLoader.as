﻿package sqisoft.net
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;


	
	//import flash.system.System;
	
	import flash.events.IOErrorEvent;

	
	import sqisoft.events.XMLLoaderEvent;
	
	//System.useCodePage=true;
	
	[ Event( name="dataComplete", type="sqisoft.events.XMLLoaderEvent" ) ]
	public class XMLLoader extends EventDispatcher
	{
		private var urlLoader:URLLoader = new URLLoader();
		private var urlRequest:URLRequest = new URLRequest();
		private var ioE:Event;
		private var openE:Event;
		
		public function XMLLoader()
		{
			super();
		}
		
		public function load( url:String ):void
		{
			urlRequest.url = url;
			//urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			urlLoader.addEventListener( IOErrorEvent.IO_ERROR, ioErrorHandler );
			urlLoader.addEventListener( Event.COMPLETE, onComplete );
			urlLoader.addEventListener(Event.OPEN, openHandler);
			
			urlLoader.load( urlRequest );
		}
		
		private function onComplete( evt:Event ):void
		{
			//trace("::::::::::::::::::::::::::::::::::::::::::::::::::" + urlLoader.data )
			//var bin:ByteArray = ByteArray(urlLoader.data);
			//trace(bin);
			var xml:XML;			
			try
			{
				xml = new XML( urlLoader.data );
			}
			catch ( evt:Error )
			{
				xml = <error></error>;
			}			
			dispatchEvent( new XMLLoaderEvent( XMLLoaderEvent.DATA_COMPLETE, xml ) );
		}
		
		private function ioErrorHandler( e:IOErrorEvent ):void
		{
			//trace("ioError");
			//trace(e);
			ioE = new Event( "fail" );
			this.dispatchEvent( ioE );

			//dispatchEvent( new XMLLoaderEvent ( XMLLoaderEvent.DATA_FAIL ) ); 
		}
		
		/*private function openHadnler( e:Event ):void
		{
			
		}*/
		
		private function openHandler( e:Event ):void
		{
			openE = new Event( "open" );
			this.dispatchEvent( openE );
			//openTimer.start();
		}
	}
}
