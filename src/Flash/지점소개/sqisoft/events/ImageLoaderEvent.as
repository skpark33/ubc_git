﻿package sqisoft.events
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	public class ImageLoaderEvent extends Event
	{
		public static const DATA_COMPLETE:String = "dataComplete";
		
		public function ImageLoaderEvent( type:String, displayObject:DisplayObject, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
			
			_displayObject = displayObject;
		}
		
		override public function clone():Event
		{
			return new ImageLoaderEvent( type, _displayObject, bubbles, cancelable );
		}
		
		private var _displayObject:DisplayObject;
		public function get displayObject():DisplayObject
		{
			return _displayObject;
		}
	}
}