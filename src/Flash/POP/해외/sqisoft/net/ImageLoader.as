﻿package sqisoft.net
{
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	import sqisoft.events.ImageLoaderEvent;
	//import sqisoft.com.BmpEncoder.BMPDecoder;
	
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	
	[ Event( name = "progress", type = "flash.events.ProgressEvent") ]
	[ Event( name = "dataComplete", type = "sqisoft.events.ImageLoaderEvent") ]
	public class ImageLoader extends Sprite
	{
		private var context:LoaderContext = new LoaderContext();
		private var loader:Loader = new Loader();
		private var urlRequest:URLRequest = new URLRequest();
		private var url:String;
		private var errorTimer:Timer = new Timer( 2000, 0 );
		
		public function ImageLoader()
		{
			super();
			addChild( loader );
			//trace(loader.data);
		}
		
		public function load( _url:String, isCheckPolicyFile:Boolean=false ):void
		{
			url = _url;
			
			urlRequest.url = _url;
			context.checkPolicyFile = isCheckPolicyFile;
			
			loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onComplete );
			loader.contentLoaderInfo.addEventListener( ProgressEvent.PROGRESS, onProgress );
			loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, onIoError );
			//loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, catchNoJpg);
			
			//loader.load( new URLRequest( url ), new LoaderContext( true ) );
			//trace( context.toString() );
			loader.load( urlRequest, context );
			//trace( urlRequest.data );
			
		}
		

		public function unload():void
		{
			loader.unload();
		}
		
		//private function catchNoJpg( evt:IOErrorEvent ):void
//		{
//			if( url.substr(url.length-3, url.length) == "bmp" )
//				loadBMPFile();
//		}
		private function onProgress( evt: ProgressEvent ):void
		{
			dispatchEvent( evt.clone() );
		}
		
		private function onComplete( evt: Event ):void
		{		
			var displayObject:DisplayObject = loader.content;
			
			dispatchEvent( new ImageLoaderEvent( ImageLoaderEvent.DATA_COMPLETE, displayObject ) );
			//trace(displayObject);
		}
		
		private function onIoError( evt:IOErrorEvent ):void
		{
			trace( "dsfasdfasdfadsfsonIoError" );
			Object( this.parent.parent ).gotoAndStop(2);
			errorTimer.start();
			errorTimer.addEventListener( TimerEvent.TIMER ,onErrorTimer );
		}


		function onErrorTimer( evt:TimerEvent ):void
		{
			trace( "onErrorTimer" );
			errorTimer.stop();	
			dispatchEvent(new Event("error") );
		}
		
		//private function loadBMPFile(  ):void 
//		{ 
//			var loader:URLLoader = new URLLoader(); 
//			loader.dataFormat = URLLoaderDataFormat.BINARY; 
//			loader.addEventListener( Event.COMPLETE, onCatcnNoCompleteLoad ); 
//			loader.load( new URLRequest( url ) ); 
//			
//		} 
		
		//private function onCatcnNoCompleteLoad( e:Event ):void 
//		{ 
//			trace("onCatcnNoCompleteLoad");
//			var loader:URLLoader = e.currentTarget as URLLoader; 
//			var decoder:BMPDecoder = new BMPDecoder(); 
//			var bd:BitmapData = decoder.decode( loader.data ); 
//			var image:Bitmap = new Bitmap(bd);
//			
//			image.width = parent.width;
//			image.height = parent.height;
//			
//
//			
//			//var displayObject:DisplayObject = DisplayObject( image );
//			
//			//trace(displayObject);
//			//image.width = 170;
//			//image.height =  100;
//			
//			addChild(image);
//			//dispatchEvent( new ImageLoaderEvent( ImageLoaderEvent.DATA_COMPLETE, image ) );
//		}
	}
}
