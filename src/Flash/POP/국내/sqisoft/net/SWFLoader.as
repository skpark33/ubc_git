﻿package sqisoft.net
{
	import flash.display.AVM1Movie;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	
	import sqisoft.events.SWFLoaderEvent;
	
	[Event(name="progress", type="flash.events.ProgressEvent")]
	[Event(name="loadComplete", type="kr.umhannum.events.SWFLoaderEvent")]
	public class SWFLoader extends Sprite
	{
		private var urlRequest:URLRequest;
		private var loader:Loader;
		public function SWFLoader()
		{
			super();
			
			urlRequest = new URLRequest();
			loader = new Loader();
			this.addChild(loader);
			
			configureListeners(loader.contentLoaderInfo);
		}
		
		private function configureListeners(dispatcher:IEventDispatcher):void
		{
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(Event.INIT, initHandler);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			dispatcher.addEventListener(Event.UNLOAD, unloadHandler);
		}
		
		public function load(url:String, checkPolicyFile:Boolean=false):void
		{
			loader.unload();
			
			urlRequest.url = url;
			
			var context:LoaderContext = new LoaderContext();
			context.applicationDomain = ApplicationDomain.currentDomain;
			context.checkPolicyFile = checkPolicyFile;
			
			loader.load(urlRequest, context);
		}
		
		public function unload():void
		{
			//loader.unload();
			loader.unloadAndStop();
		}
		
		protected function completeHandler(event:Event):void
		{
		}
		
		protected function initHandler(event:Event):void
		{
			//if (loader.content is AVM1Movie)
//			{
//				throw new Error("Action Script 2.0으로 개발된 콘텐츠는 사용 할 수 없습니다.");
//			}
			var content:DisplayObject = loader.content as DisplayObject;
			dispatchEvent(new SWFLoaderEvent(SWFLoaderEvent.LOAD_COMPLETE, content));
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void
		{
			trace("ioErrorHandler: " + event);
		}
		
		protected function progressHandler(event:ProgressEvent):void
		{
			dispatchEvent(event.clone());
		}
		
		protected function unloadHandler(event:Event):void
		{
			loader.unload();
		}
		
		public function get contentLoaderInfo():LoaderInfo
		{
			return loader.contentLoaderInfo;
		}
		
		public function get content():DisplayObject
		{
			return loader.content;
		}
	}
}