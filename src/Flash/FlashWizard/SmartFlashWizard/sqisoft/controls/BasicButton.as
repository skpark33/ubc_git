﻿package sqisoft.controls
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	
	public class BasicButton extends Sprite
	{
		protected var btnItem:MovieClip;
		
		public function BasicButton( btnClip:MovieClip )
		{
			super();
			
			btnItem = btnClip;
			
			setEvent();
			defaultSetting();
		}
				
		private function setEvent(): void
		{
			if( checkValid() == true )
			{
				//btnItem.addEventListener( MouseEvent.ROLL_OUT, onBtnItemOut );
				//btnItem.addEventListener( MouseEvent.ROLL_OVER, onBtnItemOver );
				btnItem.addEventListener( MouseEvent.CLICK, onBtnItemClick );
			}
			else
			{
				throw new Error( "btnItem에 설정한 Label명이 틀립니다" );
			}
		}
		
		private function defaultSetting():void
		{
			btnItem.gotoAndStop(1);
		}
		
		private var labelArr:Array = [ "out", "disabled" ];
		private var frameArr:Array = [];
		
		private function checkValid(): Boolean
		{
			var ary:Array = btnItem.currentLabels;
			
			for( var i:int=0; i<labelArr.length; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
		
		private function onBtnItemOut( evt:MouseEvent ):void
		{
			btnItem.gotoAndStop( frameArr[ 0 ] );
		}
		
		private function onBtnItemOver( evt:MouseEvent ):void
		{	
			btnItem.gotoAndStop( frameArr[ 1 ] );
		}
		
		private function onBtnItemClick( evt:MouseEvent ):void
		{	
			dispatchEvent( new MouseEvent( MouseEvent.CLICK ) );
		}
		
		public function selectedItem():void
		{
			btnItem.gotoAndStop( frameArr[ 2 ] );
			
			btnItem.removeEventListener( MouseEvent.ROLL_OUT, onBtnItemOut );
			btnItem.removeEventListener( MouseEvent.ROLL_OVER, onBtnItemOver );
			btnItem.removeEventListener( MouseEvent.CLICK, onBtnItemClick );
		}
		
		public function deselectedItem():void
		{
			/*btnItem.gotoAndStop( frameArr[ 0 ] );
			
			btnItem.addEventListener( MouseEvent.ROLL_OUT, onBtnItemOut );
			btnItem.addEventListener( MouseEvent.ROLL_OVER, onBtnItemOver );
			btnItem.addEventListener( MouseEvent.CLICK, onBtnItemClick );*/
		}
		
		public function get label():String
		{
			return btnItem.txt.text;
		}
		
		public function set label( value:String ):void
		{
			btnItem.txt.text = value;
		}

		private var _no:Number = 0;
		public function get no():Number
		{
			return _no;
		}
		
		public function set no( value:Number ):void
		{
			_no = value;
		}

		public function get imgFrame():Number
		{
			return btnItem._img.currentFrame;
		}
		
		public function set imgFrame( value:Number ):void
		{
			btnItem._img.gotoAndStop(value);
		}
		
		public function get enabled():Boolean
		{
			return btnItem.hasEventListener( MouseEvent.CLICK );
		}
		
		public function get img():MovieClip
		{
			return btnItem._img;
		}
		
		public function set enabled( value:Boolean ):void
		{
			if ( value )
			{
				btnItem.gotoAndStop( frameArr[ 0 ] );
				
				btnItem.addEventListener( MouseEvent.ROLL_OUT, onBtnItemOut );
				btnItem.addEventListener( MouseEvent.ROLL_OVER, onBtnItemOver );
				btnItem.addEventListener( MouseEvent.CLICK, onBtnItemClick );
			}
			else
			{
				btnItem.gotoAndStop( frameArr[ 3 ] );
				
				btnItem.removeEventListener( MouseEvent.ROLL_OUT, onBtnItemOut );
				btnItem.removeEventListener( MouseEvent.ROLL_OVER, onBtnItemOver );
				btnItem.removeEventListener( MouseEvent.CLICK, onBtnItemClick );
			}
		}
	}
}