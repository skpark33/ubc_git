﻿package umhannum.display
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	
	public class BasicShape extends Sprite
	{
		public static const HARD_TYPE:int = 0;
		public static const NORMAL_TYPE:int = 1;
		public static const SOFT_TYPE:int = 2;
		public static const NO:int = -1;
		
		protected var _shape:Shape = new Shape();
		private var _dsf:DropShadowFilter;
		
		private var _originX:Number = 0;
		private var _originY:Number = 0;
		
		protected var _fillColor:Number = 0xFFFFFF;
		protected var _fillAlpha:Number = 1.0;
		protected var _lineColor:Number = 0x000000;
		protected var _lineAlpha:Number = 1.0;
		protected var _lineThickness:Number = 0;
		
				
		public function BasicShape()
		{
			super();
			addChild( _shape );
		}

		protected function drawShape():void
		{			
		}
		
		public function get originX():Number
		{
			return _originX;
		}
		
		public function set originX( x:Number ):void
		{
			_originX = x;
			_shape.x = -x;
		}
		
		public function get originY():Number
		{
			return _originY;
		}
		
		public function set originY( y:Number ):void
		{
			_originY = y;
			_shape.y = -y;
		}
		
		public function set fillColor( rgb:Number ):void
		{
			_fillColor = rgb;
			drawShape();
		}	
		
		public function set fillAlpha( alp:Number ):void
		{
			_fillAlpha = alp;
			drawShape();
		}
		
		public function set lineColor( rgb:Number ):void
		{
			_lineColor = rgb;
			drawShape();
		}
		
		public function set lineThickness( thk:Number ):void
		{
			_lineThickness = thk;
			drawShape();
		}	
		
		public function set lineAlpha( alp:Number ):void
		{
			_lineAlpha = alp;
			drawShape();
		}
		
		public function set dropShadow( value:int ):void
		{
			if ( value == BasicShape.HARD_TYPE )
			{
				_dsf = new DropShadowFilter( 2,40,0x000000,0.7,2.0,2.0 );
				this.filters = [ _dsf ];
			}
			else if ( value == BasicShape.NORMAL_TYPE )
			{
				_dsf = new DropShadowFilter( 2,40,0x000000,0.5,5.0,5.0 );
				this.filters = [ _dsf ];
			}
			else if ( value == BasicShape.SOFT_TYPE )
			{
				_dsf = new DropShadowFilter( 2,40,0x000000,0.3,3.0,3.0 );
				this.filters = [ _dsf ];
			}
			else if ( value == BasicShape.NO )
			{
				this.filters = [];
			}
			else
			{
			}
		}
	}
}