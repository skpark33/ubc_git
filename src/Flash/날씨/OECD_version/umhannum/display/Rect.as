﻿package umhannum.display
{
	import flash.display.Graphics;
	
	public class Rect extends BasicShape
	{
		private var _width:Number;
		private var _height:Number;
		private var _ellipseWidth:Number;
		private var _ellipseHeight:Number;
		
		public function Rect( width:Number=100, height:Number=100, ellipseWidth:Number=0, ellipseHeight:Number=0 )
		{
			super();
			
			_width = width;
			_height = height;
			_ellipseWidth = ellipseWidth;
			_ellipseHeight = ellipseHeight;
			
			drawShape();
		}
		
		override protected function drawShape(): void
		{
			var g:Graphics = this.graphics;
			g.clear();
			
			g.lineStyle( _lineThickness, _lineColor, _lineAlpha, true );
			g.beginFill( _fillColor, _fillAlpha );
			g.drawRoundRect( 0, 0, _width, _height, _ellipseWidth, _ellipseHeight );
			g.endFill();
		}
		
		public function set ew ( r:Number ):void
		{
			_ellipseWidth = r;
			drawShape();
		}
		
		public function set eh ( r:Number ):void
		{
			_ellipseHeight = r;
			drawShape();
		}	
		
		override public function get width():Number
		{
			return _width;
		}
		
		override public function set width( value:Number ):void
		{
			_width = value;
			drawShape();
		}	
		
		override public function get height():Number
		{ 
			return _height;
		}
		
		override public function set height( value:Number ):void
		{
			_height = value;
			drawShape();
		}
	}
}