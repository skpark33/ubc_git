﻿package umhannum.output
{
	public class Output
	{
		public static function object( args:Object ):void
		{
			for (var item in args)
			{
				trace( "object." + item + " = " + args[ item ] );
			}
		}
		
		public static function array( arr:Array, name:String="" ):void
		{
			for (var item in arr)
			{
				trace( name + "[ " + item + " ] = " + arr[ item ] );
			}
		}
	}
}