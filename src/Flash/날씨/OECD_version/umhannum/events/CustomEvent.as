﻿package umhannum.events
{
	import flash.events.Event;
	
	
	public class CustomEvent extends Event
	{
		public static const CUSTOM_EVENT:String = "customEvent";
		
		public function CustomEvent( type:String, args:Object=null, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
			
			if ( args == null ) _args = {};
			else _args = args;
		}
		
		override public function clone():Event
		{
			return new CustomEvent( type, _args, bubbles, cancelable );
		}
		
		private var _args:Object;
		public function get args():Object
		{
			return _args;
		}
	}
}