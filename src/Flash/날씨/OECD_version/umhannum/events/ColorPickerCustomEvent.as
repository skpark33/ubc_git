﻿package umhannum.events
{
	import fl.event.ColorPickerEvent;
	
	
	public class ColorPickerCustomEvent extends ColorPickerEvent
	{
		public static const COLORPICKER_CUSTOM_EVENT:String = "colorPickerCustomEvent";		
		
		public function ColorPickerCustomEvent( type:String, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super( type, bubbles, cancelable );
		}
	}
}