﻿package umhannum.events
{
	import flash.events.Event;
	
	
	public class XMLLoaderEvent extends Event
	{
		public static const DATA_COMPLETE:String = "dataComplete";
		
		public function XMLLoaderEvent( type:String, xml:XML=null, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
			
			if ( xml == null ) _xml = <error></error>;
			else _xml = xml;
		}
		
		override public function clone():Event
		{
			return new XMLLoaderEvent( type, _xml, bubbles, cancelable );
		}
		
		private var _xml:XML;
		public function get xml():XML
		{
			return _xml;
		}
	}
}