﻿package umhannum.text
{
	import flash.display.Sprite;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextFieldAutoSize;
	
	import flash.utils.Timer;
	import flash.events.Event;
	import flash.events.TextEvent;
	
	import umhannum.text.TextTicker;
	
	public class TextTicker_MoveType extends Sprite
	{
		private var maskPanel:Sprite;
		private var textTicker:TextTicker;
		private var space:int;
		
		public function TextTicker_MoveType()
		{
			super();
			
			maskPanel = new Sprite();
			textTicker = new TextTicker();
			
			space = 5;
		}
		
		private function makeClipLED():void
		{
			removeClipLED();
			
			var clipLEDXNum:int = ( textTicker.width + space ) / space;
			var clipLEDYNum:int = ( textTicker.height + space ) / space;
			
			var clipLEDTotalNum:int = clipLEDXNum * clipLEDYNum;
			for ( var i:int=0; i<clipLEDTotalNum; i++ )
			{
				var clipLED:ClipLED = new ClipLED();
				clipLED.x = 0 + space * int( i % clipLEDXNum );
				clipLED.y = 0 + space * int( i / clipLEDXNum );
				
				maskPanel.addChild( clipLED );
			}
			
			maskPanel.mask = textTicker;
			trace( "maskPanel.numChildren : " + maskPanel.numChildren );
			
			if ( !contains( maskPanel ) )
				addChild( maskPanel );
		}
		
		private function removeClipLED():void
		{
			var len:int = maskPanel.numChildren - 1;
			for ( var i:int=len; i>=0; i-- )
			{
				maskPanel.removeChildAt(i);
			}
		}
		
		private var _text:String = "";
		public function set text( value:String ):void
		{
			_text = value;
		}
		
		public function get text():String
		{
			return _text;
		}
		
		public function updateTextField():void
		{
			textTicker.font = new Ticker().fontName;
			textTicker.fontSize = 50;
			textTicker.autoSize = TextFieldAutoSize.LEFT;
			textTicker.embedFonts = true;
			textTicker.antiAliasType = AntiAliasType.ADVANCED;
			textTicker.gridFitType = GridFitType.SUBPIXEL;
			textTicker.border = true;
			
			addChild( textTicker );
			
			makeClipLED();
		}
		
		private var _marquee:Boolean = false;
		private var originalTextWidth:Number = 0;
		
		public function set marquee( value:Boolean ):void
		{
			if ( value == _marquee ) return;
			
			_marquee = value;
			if ( value == true )
			{
				textTicker.text = _text+ "   ";
				originalTextWidth = textTicker.width;
				textTicker.text = _text + "   " + _text + "   ";
				
				trace( "textTicker.width : " + textTicker.width );
				
				this.addEventListener( Event.ENTER_FRAME, onMarqueeEnter );
			}
			else
			{
				textTicker.text = _text;
				
				this.removeEventListener( Event.ENTER_FRAME, onMarqueeEnter);
			}
			updateTextField();
		}
		
		public function get marquee():Boolean
		{
			return _marquee;
		}
		
		private function onMarqueeEnter( evt:Event ):void
		{
			this.x -= 3;			
			trace( "this.x : " + this.x + " ::: " + "this.originalTextWidth : " + this.originalTextWidth );
			if ( this.x < -originalTextWidth )
			{
				this.x += originalTextWidth;
			}
		}
	}
}
