﻿package umhannum.text
{
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextFieldAutoSize;
	
	import flash.utils.Timer;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.events.TextEvent;
	
	import umhannum.text.TextTicker;
	
	public class TextTicker_MoveType extends Sprite
	{
		private var maskPanel:Sprite;
		private var textTicker:TextTicker;
		private var space:int;
		
		public function TextTicker_MoveType()
		{
			super();
			
			maskPanel = new Sprite();
			textTicker = new TextTicker();
			
			space = 10;
		}
		
		private function makeClipLED():void
		{
			removeClipLED();
			
			var bitmapData:BitmapData = new BitmapData( space, space, false, 0x000000 );
			bitmapData.draw( new ClipLED() );
			
			var clipLEDXNum:int = ( 768 + space ) / space;
			var clipLEDYNum:int = ( 80 + space ) / space;
			var clipLEDTotalNum:int = clipLEDXNum * clipLEDYNum;
			trace( "clipLEDTotalNum : " + clipLEDTotalNum );
			for ( var i:int=0; i<clipLEDTotalNum; i++ )
			{
				var bitmap:Bitmap = new Bitmap( bitmapData );
				
				bitmap.x = 0 + space * int( i % clipLEDXNum);
				bitmap.y = 0 + space * int( i / clipLEDXNum);
				
				maskPanel.addChild( bitmap );
			}
			
			maskPanel.mask = textTicker;
			trace( "maskPanel.numChildren : " + maskPanel.numChildren );
			
			if ( !contains( maskPanel ) )
				addChild( maskPanel );
		}
		
		private function removeClipLED():void
		{
			var len:int = maskPanel.numChildren - 1;
			for ( var i:int=len; i>=0; i-- )
			{
				maskPanel.removeChildAt(i);
			}
		}
		
		public function get text():String
		{
			return textTicker.text;
		}
		
		public function set text( value:String ):void
		{
			textTicker.text = value;
			
			updateTextField();
		}
		
		public function updateTextField():void
		{
			textTicker.font = new Ticker().fontName;
			textTicker.fontSize = 50;
			textTicker.autoSize = TextFieldAutoSize.LEFT;
			textTicker.embedFonts = true;
			
			textTicker.border = true;
			//textTicker.marquee = true;
			
			addChild( textTicker );
			_marqueeTimer.addEventListener( TimerEvent.TIMER, onMarqueeTimer );
			_marqueeTimer.start();
			
			makeClipLED();
		}
		
		private var _marqueeTimer:Timer = new Timer( 30, 0 );
		private function onMarqueeTimer( evt:TimerEvent ):void
		{
			trace( "textTicker.scrollH : " + textTicker.scrollH );
			trace( "textTicker.maxScrollH : " + textTicker.maxScrollH );
			textTicker.scrollH += 2;
		}
	}
}
