﻿package umhannum.text
{
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class TextTicker extends TextField
	{
		public static const MARQUEE_FORWARD:String = "forward";
		public static const MARQUEE_BACKWARD:String = "backward";
		public static const MARQUEE_BOUNCE:String = "bounce";
		public static const MARQUEE_NONE:String = "none";
		
		private var format:TextFormat = new TextFormat();
			
		public function TextTicker()
		{
			super();
			this.selectable = false;
		}
		
		public function set backColor( value:Number ):void
		{
			if ( value >= 0 )
			{
				this.background = true;
				super.backgroundColor = value;
			}
			else
			{
				this.background = false;
			}
		}
		
		private var _marqueeTimer:Timer = new Timer( 30, 0 );
		private var _marquee:Boolean = false;
		private var _orginalTextWidth:Number = 0;
		
		public function set marquee( value:Boolean ):void
		{
			trace( "_text : " + _text );
			trace( "value : " + value );
			if ( value == _marquee ) return;
			
			_marquee = value;
			if ( value == true )
			{
				super.text = _text+ "   ";
				_orginalTextWidth = this.textWidth;
				super.text = _text + "   " + _text + "   ";
				
				_marqueeTimer.addEventListener( TimerEvent.TIMER, onMarqueeTimer );
				_marqueeTimer.start();
			}
			else
			{
				super.text = _text;
				
				_marqueeTimer.removeEventListener(TimerEvent.TIMER, onMarqueeTimer);
				_marqueeTimer.stop();
			}
		}
		
		public function get marquee():Boolean
		{
			return _marquee;
		}
		
		private function onMarqueeTimer( evt:TimerEvent ):void
		{
			trace( "this.scrollH : " + this.scrollH );
			trace( "this.maxScrollH : " + this.maxScrollH );
			this.scrollH += 2;
			if ( this.scrollH > this.maxScrollH - 10 )
			{
				this.scrollH -= _orginalTextWidth;
			}
		}
		
		private var _text:String = "";
		override  public function set text( value:String ):void
		{
			_text = value;
			super.text = _text;
		}
		
		override public function get text():String
		{
			_text = super.text;			
			return _text;
		}
		
		private var _font:String = "Dotum";
		public function set font( value:String ):void
		{
			_font = value;
			format.font = _font;
			this.defaultTextFormat = format;
			this.setTextFormat( format );
		}
		
		public function get font():String
		{
			return _font;
		}
		
		private var _align:String = TextFormatAlign.LEFT;
		public function set align( value:String ):void
		{
			_align = value;
			format.align = _align;
			this.defaultTextFormat = format;
			this.setTextFormat( format );
		}
		
		public function get align():String
		{
			return _align;
		}
		
		private var _bold:Boolean = false;
		public function set bold( value:Boolean ):void
		{
			_bold = value;
			format.bold = _bold;
			this.defaultTextFormat = format;
			this.setTextFormat( format );
		}
		
		private var _fontSize:Number = 11;
		public function set fontSize( value:Number ):void
		{
			_fontSize = value;
			format.size = _fontSize;
			//this.height = this.textHeight;
			this.defaultTextFormat = format;
			this.setTextFormat( format );
		}
	}
}
