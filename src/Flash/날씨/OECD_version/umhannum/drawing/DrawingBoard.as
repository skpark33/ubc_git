﻿package umhannum.drawing
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.FocusEvent;
	import flash.ui.Mouse;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.text.TextField;
	
	import fl.controls.ColorPicker;
	import fl.controls.Slider;
	import fl.controls.TextInput;
	
	import fl.events.ColorPickerEvent;
	import fl.events.ComponentEvent;
	import fl.events.SliderEvent;
	
	import fl.managers.FocusManager;
	
	import ascb.drawing.Pen;
	import umhannum.controls.BasicButton;
	import umhannum.manager.SaveJPGManager;
	
	
	public class DrawingBoard extends MovieClip
	{
		private var bitmapData:BitmapData;
		private var boardArr:Array;
		private var boardW:Number;
		private var boardH:Number;
		private var currentBitmapData:Number;
		private var date:Date;
		private var fm:FocusManager;
		private var filename:String;
		private var pen:Pen;
		private var rgb:Number;
		private var old_rgb:Number;
		private var selectedItem:BasicButton;
		private var sprite:Sprite;
		private var saveJPGManager:SaveJPGManager;
		private var thickness:Number;
		
		private var pen_btn:BasicButton;
		private var eraser_btn:BasicButton;
				
		public function DrawingBoard()
		{
			super();
			
			makeMenu();
			setLayout();
			setEvent();
			defaultSetting();
		}
		
		private function setLayout():void
		{
			sprite = new Sprite();
			board.canvas.addChild( sprite );
			
			pen = new Pen( sprite.graphics );
			
			fm = new FocusManager( stage );
			
			pen_btn = new BasicButton( pen_mc );
			eraser_btn = new BasicButton( eraser_mc );
		}
		
		private function setEvent():void
		{
			board.addEventListener( MouseEvent.MOUSE_UP, onDrawingStop );
			board.addEventListener( MouseEvent.MOUSE_DOWN, onDrawingStart );
			board.addEventListener( MouseEvent.ROLL_OUT, onboardRollOut );
			board.addEventListener( MouseEvent.ROLL_OVER, onboardRollOver );
		
			stage.addEventListener( MouseEvent.MOUSE_UP, onStageMouseUp );
			stage.addEventListener( MouseEvent.MOUSE_DOWN, onStageMouseDown );
		
			colorPicker.addEventListener( Event.CLOSE, onColorPickerClose );
			colorPicker.addEventListener( ColorPickerEvent.CHANGE, onColorChange );
			
			slider.addEventListener( SliderEvent.CHANGE, onThicknessChange );
			
			txt.addEventListener( FocusEvent.FOCUS_IN, onFocusIn );
			txt.addEventListener( FocusEvent.FOCUS_OUT, onFocusOut );
			txt.addEventListener( ComponentEvent.ENTER, onThicknessEnter );
			
			pen_btn.addEventListener( MouseEvent.CLICK, onToolClick );
			eraser_btn.addEventListener( MouseEvent.CLICK, onToolClick );
			
			pen_btn.dispatchEvent( new MouseEvent( MouseEvent.CLICK ) );
		}
		
		private function defaultSetting():void
		{
			boardArr = [];
			
			boardW = board.canvas.width;
			boardH = board.canvas.height;
			
			bitmapData = new BitmapData( boardW, boardH, false, 0xFFFFFF );	
			currentBitmapData = -1;
			saveBitmapData();
			
			date = new Date();
			
			rgb = 0;
			thickness = 1;
			
			slider.minimum = 1;
			slider.maximum = 30;
			
			pen_btn.buttonMode = true;
			pen_btn.name = "pen_btn";
			
			eraser_btn.buttonMode = true;
			eraser_btn.name = "eraser_btn";
			
			txt.text = String( thickness );
			txt.restrict = "0-9";
			txt.maxChars = 2;
			
			board.dragClip.visible = false;
			
			lineStyleUpdate();
		}
		
		private function onDrawingStart( evt:MouseEvent=null ):void
		{
			//trace( "onDrawingStart.eventPhase : " + evt.eventPhase );
			if ( evt != null && evt.eventPhase == 2 ) fm.setFocus( stage );
			
			trace( "onDrawingStart" );
			trace( "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ" );
			
			pen.moveTo( board.mouseX, board.mouseY );
			
			board.dragClip.x = board.mouseX;
			board.dragClip.y = board.mouseY;
			
			board.removeEventListener( MouseEvent.ROLL_OVER, onDrawRollOver );
			
			board.addEventListener( MouseEvent.MOUSE_MOVE, onDrawing );
			board.addEventListener( MouseEvent.ROLL_OUT, onDrawRollOut );
			
			trace( "currentBitmapData, boardArr.length : ", currentBitmapData, boardArr.length );
			if ( currentBitmapData < boardArr.length - 1  )	boardArr.splice( currentBitmapData + 1 );
		}
		
		private function onDrawingStop( evt:MouseEvent=null ):void
		{
			// trace( "onDrawingStop" );
			// trace( "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ" );
			
			board.addEventListener( MouseEvent.ROLL_OVER, onDrawRollOver );		
			
			board.removeEventListener( MouseEvent.MOUSE_MOVE, onDrawing );
			board.removeEventListener( MouseEvent.ROLL_OUT, onDrawRollOut );
			
			saveBitmapData();
		}
		
		private function onDrawing( evt:MouseEvent ):void
		{
			pen.lineTo( board.mouseX, board.mouseY );
		}
		
		private function onDrawRollOut( evt:MouseEvent ):void
		{
			// trace( "onDrawRollOut" );
			// trace( "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ" );
			onDrawingStop();
		}
		
		private function onDrawRollOver( evt:MouseEvent ):void
		{
			// trace( "onDrawRollOver" );
			// trace( "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ" );
			onDrawingStart();
		}
		
		private function onboardRollOut( evt:MouseEvent ):void
		{
			// trace( "onboardRollOut" );
			// trace( "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ" );
			Mouse.show();
			board.dragClip.visible = false;
			board.dragClip.removeEventListener( Event.ENTER_FRAME, onDragClipEnter );
		}
		
		private function onboardRollOver( evt:MouseEvent ):void
		{
			// trace( "onboardRollOver" );
			// trace( "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ" );
			Mouse.hide();
			board.dragClip.visible = true;
			board.dragClip.addEventListener( Event.ENTER_FRAME, onDragClipEnter );	
		}
		
		private function onDragClipEnter( evt:Event ):void
		{
			board.dragClip.x = board.mouseX;
			board.dragClip.y = board.mouseY;
		}
		
		private function onStageMouseUp( evt:MouseEvent ):void
		{
			// trace( "onStageMouseUp" );
			// trace( "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ" );
		
			board.removeEventListener( MouseEvent.ROLL_OVER, onDrawRollOver );
		}
		
		private function onStageMouseDown( evt:MouseEvent ):void
		{
			// trace( "onStageMouseDown.eventPhase : " + evt.eventPhase );
			if ( !(evt.target is TextField) ) fm.setFocus( stage );
			
			if ( evt.eventPhase == 2 )
			{
				// trace( "onStageMouseDown" );
				// trace( "ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ" );
				
				board.addEventListener( MouseEvent.ROLL_OVER, onDrawRollOver );
			}	
		}
		
		private function onColorChange( evt:ColorPickerEvent ):void
		{
			// trace( "evt.color : " + evt.color );
			rgb = evt.color;
			
			lineStyleUpdate();
		}
		
		private function onColorPickerClose( evt:Event ):void
		{
			// trace( "colorPicker.selectedColor : " + colorPicker.selectedColor );	
			rgb = colorPicker.selectedColor;
			
			lineStyleUpdate();
		}
		
		private function onThicknessChange( evt:SliderEvent ):void
		{
			// trace( "evt.target.value : " + evt.target.value );
			thickness = Number( evt.target.value );
			
			lineStyleUpdate();
		}
		
		private function onThicknessEnter( evt:ComponentEvent=null ):void
		{
			thickness = Math.min( 30, Number( txt.text ));
			
			lineStyleUpdate();
		}
		
		private function lineStyleUpdate():void
		{
			txt.text = String( thickness );
			slider.value = thickness;
			
			pen.lineStyle( thickness, rgb );
		}
		
		private function onFocusIn( evt:FocusEvent ):void
		{
			trace( "onFocusIn" );
			fm.setFocus( txt );
		}
		
		private function onFocusOut( evt:FocusEvent ):void
		{
			trace( "onFocusOut" );
			onThicknessEnter();
		}
		
		private function onToolClick( evt:MouseEvent ):void
		{
			if (evt.eventPhase == 2)
			{
				if ( selectedItem != null ) selectedItem.deselectedItem();
				selectedItem = evt.target as BasicButton;
				selectedItem.selectedItem();
				board.dragClip.gotoAndStop( selectedItem.name );
				
				if ( selectedItem.name == "eraser_btn" )
				{
					colorPicker.enabled = false;
					colorPicker.editable = false;
					
					old_rgb = rgb;
					rgb = 16777215;
				}					
				else
				{
					colorPicker.enabled = true;
					colorPicker.editable = true;
					
					rgb = old_rgb;
				}
				lineStyleUpdate();
			}
		}
		
		private var startX:Number = 470;
		private var startY:Number = 16;
		private var spaceX:Number = 80;
		
		private var menuArr:Array = [ "Undo","Redo","Clear", "Save" ];
		private var selectedMenu:BasicButton;		
		
		private function makeMenu():void
		{
			var menuItem:BasicButton;
			var len:int = menuArr.length;
			for (var i:int=0; i<len; i++)
			{
				menuItem = new BasicButton();
				menuItem.x = startX + spaceX * i;
				menuItem.y = startY;
				
				menuItem.label = menuArr[ i ];
				menuItem.name = menuArr[ i ];
				
				menuItem.buttonMode = true;
				
				menuItem.addEventListener( MouseEvent.CLICK, onClickMenu );
				
				addChild( menuItem );
			}
		}
		
		private function onClickMenu( evt:MouseEvent ):void
		{
			if (evt.eventPhase == 2)
			{
				selectedMenu = evt.target as BasicButton;
				
				if ( selectedMenu.name == "Undo" )
				{
					currentBitmapData--;
					if ( currentBitmapData < 0 )
					{
						currentBitmapData = 0;
					}
					else
					{
						trace(" undo ");
						pen.beginBitmapFill( boardArr[ currentBitmapData ] );
						pen.lineStyle();
						pen.drawRect( 0, 0, boardW, boardH );
						pen.endFill();
						
						lineStyleUpdate();
					}
				}
				else if ( selectedMenu.name == "Redo" )
				{
					currentBitmapData++;
					if ( currentBitmapData > boardArr.length - 1 )
					{
						currentBitmapData = boardArr.length - 1;
					}
					else
					{
						trace(" redo ");
						pen.beginBitmapFill( boardArr[ currentBitmapData ] );
						pen.lineStyle();
						pen.drawRect( 0, 0, boardW, boardH );
						pen.endFill();
						
						lineStyleUpdate();
					}
				}
				else if ( selectedMenu.name == "Clear" )
				{
					trace("Clear");
					
					pen.clear();
					
					lineStyleUpdate();
					saveBitmapData();
				}
				else if ( selectedMenu.name == "Save" )
				{
					if ( currentBitmapData == 0 )
					{
						messageAlert();
						return;
					}
					trace("Save");
					filename = String( date.valueOf() );
					saveJPGManager = new SaveJPGManager( bitmapData );
					saveJPGManager.save( filename + ".jpg" );
				}
			}
		}
		
		private function saveBitmapData()
		{
			bitmapData.draw( board.canvas );	
			boardArr.push( bitmapData.clone() );
			currentBitmapData++;
		}
		
		private function messageAlert()
		{
			var messageAlertItem:MessageAlertItem = new MessageAlertItem();
			messageAlertItem.x = ( stage.width - messageAlertItem.width ) / 2;
			messageAlertItem.y = ( stage.height - messageAlertItem.height ) / 2;
			
			messageAlertItem.addEventListener( "lastFrame", onLastFrame );
			addChild( messageAlertItem );
		}
		
		private function onLastFrame( evt:Event ):void
		{
			removeChild( evt.target as DisplayObject );
		}
	}
}
