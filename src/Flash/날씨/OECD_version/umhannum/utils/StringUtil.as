﻿package umhannum.utils
{
	public class StringUtil
	{
		public static function digit( value:Number ):String
		{
			if ( value < 10 ) return "0" + value;
			else return "" + value;
		}
	}
}