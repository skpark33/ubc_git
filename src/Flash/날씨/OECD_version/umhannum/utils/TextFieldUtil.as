﻿package umhannum.utils
{
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	
	public class TextFieldUtil
	{
		public static function formatTextField( tf:TextField, letterSpacing:Object=1, kerning:Object=null ):void
		{
			var format:TextFormat = new TextFormat();
			format.letterSpacing = letterSpacing;
			format.kerning = kerning;
			
			tf.defaultTextFormat = format;
			tf.setTextFormat(format);
		}
	}
}