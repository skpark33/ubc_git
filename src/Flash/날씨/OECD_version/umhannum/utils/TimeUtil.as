﻿package umhannum.utils
{
	import flash.display.Stage;	
	import umhannum.utils.StringUtil;
	
	
	public class TimeUtil
	{
		public static function displayTime( currentframe:Number, totalframes:Number, fps:Number ):String
		{
			var timeMin:Number = int( currentframe / fps / 60 );
			var timeSec:Number = int( currentframe / fps % 60 );
			
			var timeTotalMin:Number = int( totalframes / fps / 60 );
			var timeTotalSec:Number = int( totalframes / fps % 60 );
			trace( StringUtil.digit( timeSec ) );
			var time:String = StringUtil.digit( timeMin ) + ":" + StringUtil.digit( timeSec ) + " / " + StringUtil.digit( timeTotalMin ) + ":" + StringUtil.digit( timeTotalSec );
			
			return time;
		}
	}
}
