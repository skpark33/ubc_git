﻿package umhannum.net
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import umhannum.events.XMLLoaderEvent;
	
	
	[ Event( name="dataComplete", type="sqisoft.events.XMLLoaderEvent" ) ]
	public class XMLLoader extends EventDispatcher
	{
		private var urlLoader:URLLoader = new URLLoader();
		private var urlRequest:URLRequest = new URLRequest();
		
		public function XMLLoader()
		{
			super();
		}
		
		public function load( url:String ):void
		{
			urlRequest.url = url;
			urlLoader.addEventListener( Event.COMPLETE, onComplete );
			urlLoader.load( urlRequest );
		}
		
		private function onComplete( evt:Event ):void
		{
			var xml:XML;			
			try
			{
				xml = new XML( urlLoader.data );
			}
			catch ( evt:Error )
			{
				xml = <error></error>;
			}			
			dispatchEvent( new XMLLoaderEvent( XMLLoaderEvent.DATA_COMPLETE, xml ) );
		}
	}
}
