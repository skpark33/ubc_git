﻿package umhannum.net
{
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	
	import umhannum.events.ImageLoaderEvent;
	
	
	[ Event( name = "progress", type = "flash.events.ProgressEvent") ]
	[ Event( name = "dataComplete", type = "umhannum.events.ImageLoaderEvent") ]
	public class ImageLoader extends Sprite
	{
		private var context:LoaderContext = new LoaderContext();
		private var loader:Loader = new Loader();
		private var urlRequest:URLRequest = new URLRequest();
		
		public function ImageLoader()
		{
			super();
			addChild( loader );
		}
		
		public function load( url:String, isCheckPolicyFile:Boolean=false ):void
		{
			urlRequest.url = url;
			context.checkPolicyFile = isCheckPolicyFile;
			
			loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onComplete );
			loader.contentLoaderInfo.addEventListener( ProgressEvent.PROGRESS, onProgress );
			loader.load( urlRequest, context );
		}
		
		private function onProgress( evt: ProgressEvent ):void
		{
			dispatchEvent( evt.clone() );
		}
		
		private function onComplete( evt: Event ):void
		{
			var displayObject:DisplayObject = loader.content;
			
			dispatchEvent( new ImageLoaderEvent( ImageLoaderEvent.DATA_COMPLETE, displayObject ) );
		}
	}
}
