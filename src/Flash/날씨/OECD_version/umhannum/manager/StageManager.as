﻿package umhannum.manager
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	
	public class StageManager extends Sprite
	{
		private static var sm:StageManager = new StageManager();
		
		public function StageManager()
		{
			super();
		}
		
		/**
		 * StageManager 인스턴스를 생성하는 메서드 
		 * @return : StageManager
		 */		
		public static function getInstance():StageManager
		{			
			return sm;
		}
		
		/**
		 * Stage를 반환하는 메서드 
		 * @return 
		 */		
		public static function getStage():Stage
		{
			return sm.stage;
		}
		
	}
}