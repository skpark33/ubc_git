﻿package umhannum.manager
{
	import flash.display.MovieClip;
	
	public class SelectManager
	{
		private static var _sm:MovieClip;
		
		public function SelectManager()
		{
			super();
		}
		
		/**
		 * SelectManager 인스턴스를 가져오는 getter 
		 * @return : SelectManager
		 */
		public static function get sm():MovieClip
		{			
			return _sm;
		}
		
		/**
		 * SelectManager 인스턴스를 등록하는 setter 
		 * @return : SelectManager
		 */
		public static function set sm( value:MovieClip ):void
		{			
			_sm = value;
		}
	}
}