﻿package umhannum.manager
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.net.FileReference;
	import flash.utils.ByteArray;

	import umhannum.images.JPGEncoder;

	public class SaveJPGManager extends Bitmap
	{
		private var _fileRef:FileReference;
		private var _encoder:JPGEncoder;
		private var _jpegQuality:Number = 80;

		public function SaveJPGManager( bitmapData:BitmapData=null, pixelSnapping:String="auto", smoothing:Boolean=false )
		{
			super( bitmapData, pixelSnapping, smoothing );
			
			_fileRef = new FileReference();
			_encoder = new JPGEncoder( _jpegQuality );
		}

		public function set jpegQuality( value:Number ):void
		{
			_jpegQuality = value;
			
			_encoder = new JPGEncoder( _jpegQuality );
		}
		
		public function get jpegQuality():Number
		{
			return _jpegQuality;
		}
		
		public function save( defaultFileName:String = "image.jpg" ):void
		{
			var byteArr:ByteArray = _encoder.encode( bitmapData );
			_fileRef.save( byteArr, defaultFileName );
			byteArr.clear();
		}
	}
}