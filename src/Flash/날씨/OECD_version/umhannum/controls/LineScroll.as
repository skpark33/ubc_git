﻿package umhannum.controls
{
	import flash.display.MovieClip;
	import flash.utils.Timer;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	
	import umhannum.events.LineScrollEvent;
	
	[ Event( name="lineScrollChange", type="umhannum.events.LineScrollEvent" ) ]
	public class LineScroll extends MovieClip
	{
		private var scrollTimer:Timer = new Timer( 40, 0 );
		private var offSetX:Number = 0;
		
		private var minScrollX:Number;
		private var maxScrollX:Number;
		
		public var minTargetValue:Number;
		public var maxTargetValue:Number;
		
		private var _cursorType:String;
		
		public function LineScroll( cursorType:String="dot" )
		{
			super();
			
			_cursorType = cursorType;
			
			setLayout();
			setEvent();
			defaultSetting();
		}
		
		private function setLayout(): void
		{
			track_mc.addChild( bg_mc );
			track_mc.addChild( progress_mc );
			track_mc.addChild( cursor_mc );
			
			bg_mc.x = 0;
			cursor_mc.x = 0;
		}
		
		private function setEvent(): void
		{
			cursor_mc.addEventListener( MouseEvent.MOUSE_DOWN, onCursorMouseDown );
			track_mc.addEventListener( MouseEvent.MOUSE_DOWN, onTrackMouseDown );
			
			progress_mc.addEventListener( Event.ENTER_FRAME, onProgressEnterFrame );
			scrollTimer.addEventListener( TimerEvent.TIMER, onScrollTime );
		}
		
		private function defaultSetting():void
		{
			cursor_mc.buttonMode = true;
			track_mc.buttonMode = true;
			
			minTargetValue = 0;
			maxTargetValue = 100;
		
			minScrollX = 0;
			
			if ( _cursorType == "dot" )
			{
				maxScrollX = track_mc.width;
			}
			else
			{
				maxScrollX = track_mc.width - cursor_mc.width;
			}
		}
		
		private function onCursorMouseDown( evt:MouseEvent ):void
		{
			scrollTimer.start();
			
			offSetX = cursor_mc.mouseX;
			
			stage.addEventListener( MouseEvent.MOUSE_UP, onStageMouseUp );
		}
		
		private function onStageMouseUp( evt:MouseEvent ):void
		{
			scrollTimer.stop();
		}
		
		private function onScrollTime( evt:TimerEvent ):void
		{
			cursor_mc.x = track_mc.mouseX - offSetX;
			
			checkBound();
		}
		
		private function checkBound():void
		{
			if ( cursor_mc.x < minScrollX ) cursor_mc.x = minScrollX;
			if ( cursor_mc.x > maxScrollX ) cursor_mc.x = maxScrollX;
			
			dispatchEvent( new LineScrollEvent( LineScrollEvent.LINE_SCROLL_CHANGE, position ) );
		}
		
		private function onTrackMouseDown( evt:MouseEvent ):void
		{
			cursor_mc.x = track_mc.mouseX;
			
			dispatchEvent( new LineScrollEvent( LineScrollEvent.LINE_SCROLL_CHANGE, position ) );
		}
		
		private function onProgressEnterFrame( evt:Event ):void
		{
			progress_mc.width = cursor_mc.x;
		}
		
		private var _position:Number;
		private function get position():Number
		{
			_position = ( maxTargetValue - minTargetValue ) / ( maxScrollX - minScrollX ) * ( cursor_mc.x - minScrollX ) + minTargetValue;
			return _position;
		}
		
		private function set position( value:Number ):void
		{
			_position = ( maxScrollX - minScrollX ) / ( maxTargetValue - minTargetValue ) * ( value - minTargetValue ) + minScrollX;
			cursor_mc.x = _position;
		}
	}
}
