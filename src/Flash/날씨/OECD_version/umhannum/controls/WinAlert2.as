﻿package umhannum.controls
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.text.TextField;
	import flash.events.MouseEvent;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.events.Event;
	
	import umhannum.events.AlertEvent;
	//import 
	/* --------------------------------------------------------------------------------------
		경고창의 확인버튼을 클릭하여 창을 닫을 경우 : AlertWinOK 이벤트 발생
		-------------------------------------------------------------------------------------- */
	[Embed(source="./controlsAssets/commonAsset.swf", symbol="Alert2")]
	public class WinAlert2 extends MovieClip
	{
		public var msg:MovieClip;
		public var close_btn:SimpleButton;
		public var cancel_btn:SimpleButton;
		public var confirm_btn:SimpleButton;
		public var bg:MovieClip;
		public var outLine:MovieClip;
		private var timer:Timer;
		public var txt:TextField;
		

		// Constructor
		public function WinAlert2()
		{
			super();
			setTextField();
			this.visible = false;
			
			this.confirm_btn.addEventListener("mouseDown", onConfirmBtnDown);
			this.cancel_btn.addEventListener("mouseDown", onCloseBtnDown);
			this.close_btn.addEventListener("mouseDown", onCloseBtnDown);
		}
		
		// set TextField
		private function setTextField() : void
		{
			this.txt.autoSize = "left";
			this.txt.multiline = true;
			this.txt.selectable = false;
		}
		
		// show msg
		public function show(str:String) : void
		{
			this.txt.text = str;
			var _h:Number = txt.y + txt.height;
			if(_h > 16)
			{
				outLine.height = _h + 59;
				bg.height = _h + 59;
				confirm_btn.y = _h + 59 - 38;
				cancel_btn.y = _h + 59 - 38;
			}else{
				outLine.height = 110;
				bg.height = 110;
				confirm_btn.y = 110 - 38;
				cancel_btn.y = 110 - 38;
			}
			
			this.visible = true;
		}
		
		// hide window
		public function hide() : void
		{
			this.visible = false;
			this.txt.text = "";
			
			/*
			timer = new Timer(15, 10);
			timer.addEventListener("timer", onAlertWinHide);
			timer.start();
			*/
		}
		
		private function onAlertWinHide(evt:TimerEvent) : void
		{
			this.alpha -= 0.1;
			if(timer.currentCount == timer.repeatCount)
			{
				this.visible = false;
				this.alpha = 1.0;
			}
		}
		
		
		// confirm_btn down
		private function onConfirmBtnDown(evt:MouseEvent) : void
		{
			//hide();
			this.visible = false;
			dispatchEvent(new Event(AlertEvent.ALERT_CONFIRM));
		}
		
		// close_btn down
		private function onCloseBtnDown(evt:MouseEvent) : void
		{
			//hide();
			this.visible = false;
			dispatchEvent(new Event(AlertEvent.ALERT_CANCEL));
		}
		
	}
}