﻿package umhannum.controls
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import umhannum.events.CustomEvent;
	
	
	[ Event( name="CUSTOM_EVENT", type="umhannum.events.CustomEvent" ) ];
	public class DesignButton extends MovieClip
	{
		public function DesignButton()
		{
			super();
			
			setEvent();
			defaultSetting();
		}
				
		private function setEvent(): void
		{
			if( checkValid() == true )
			{
				addEventListener( MouseEvent.ROLL_OUT, onItemOut );
				addEventListener( MouseEvent.ROLL_OVER, onItemOver );
				addEventListener( MouseEvent.CLICK, onItemClick );
			}
			else
			{
				throw new Error( "Frame에 설정한 Label명이 틀립니다" );
			}
		}
		
		private function defaultSetting():void
		{
			gotoAndStop(1);
			buttonMode = true;
			emphasizeClip.visible = false;
		}
		
		private var labelArr:Array = [ "out", "over", "selected", "disabled" ];
		private var frameArr:Array = [];
		
		private function checkValid(): Boolean
		{
			var ary:Array = currentLabels;
			
			for( var i:int=0; i<labelArr.length; i++ )
			{
				frameArr[ i ] = ary[ i ].frame;
				if( ary[ i ].name != labelArr[ i ] ) return false;
			}			
			return true;
		}
		
		private function onItemOut( evt:MouseEvent ):void
		{
			gotoAndStop( frameArr[ 0 ] );
		}
		
		private function onItemOver( evt:MouseEvent ):void
		{	
			gotoAndStop( frameArr[ 1 ] );
		}
		
		private function onItemClick( evt:MouseEvent ):void
		{	
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}
		
		public function selectedItem():void
		{
			gotoAndStop( frameArr[ 2 ] );
			
			removeEventListener( MouseEvent.ROLL_OUT, onItemOut );
			removeEventListener( MouseEvent.ROLL_OVER, onItemOver );
			removeEventListener( MouseEvent.CLICK, onItemClick );
		}
		
		public function deselectedItem():void
		{
			gotoAndStop( frameArr[ 0 ] );
			
			addEventListener( MouseEvent.ROLL_OUT, onItemOut );
			addEventListener( MouseEvent.ROLL_OVER, onItemOver );
			addEventListener( MouseEvent.CLICK, onItemClick );
		}
		
		public function get emphasize():Boolean
		{
			return emphasizeClip.visible;
		}
		
		public function set emphasize( value:Boolean ):void
		{
			emphasizeClip.visible = value;
		}
		
		public function get enable():Boolean
		{
			return hasEventListener( MouseEvent.CLICK );
		}
		
		public function set enable( value:Boolean ):void
		{
			if ( value )
			{
				buttonMode = true;
				
				addEventListener( MouseEvent.ROLL_OUT, onItemOut );
				addEventListener( MouseEvent.ROLL_OVER, onItemOver );
				addEventListener( MouseEvent.CLICK, onItemClick );
				
				gotoAndStop( frameArr[ 0 ] );
			}
			else
			{
				buttonMode = false;
				
				removeEventListener( MouseEvent.ROLL_OUT, onItemOut );
				removeEventListener( MouseEvent.ROLL_OVER, onItemOver );
				removeEventListener( MouseEvent.CLICK, onItemClick );
				
				gotoAndStop( frameArr[ 3 ] );
			}
		}
	}
}