﻿package umhannum.controls
{
	import flash.events.MouseEvent;
	
	import umhannum.controls.DesignButton;
	import umhannum.events.CustomEvent;
	
	
	public class DesignToggleButton extends DesignButton
	{
		public function DesignToggleButton()
		{
			super();
		}
				
		override private function onItemClick( evt:MouseEvent ):void
		{
			isToggle = isToggle ? false : true;
			updateToggleStatus();
			
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}
		
		public function updateToggleStatus():void
		{
			check_mc.gotoAndStop( isToggle ? "off" : "on" );
		}
		
		private var _isToggle:Boolean = false;
		public function get isToggle():Boolean
		{
			return _isToggle;
		}
		
		public function set isToggle( value:Boolean ):void
		{
			_isToggle = value;
		}
	}
}