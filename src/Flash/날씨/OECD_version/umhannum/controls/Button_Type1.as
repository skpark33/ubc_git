﻿package umhannum.controls
{
	import flash.events.MouseEvent;	
	import mx.core.SpriteAsset;
	
	
	public class Button_Type1 extends BasicButton
	{
		[ Embed( source="./controlsAssets/controls_skin01.swf", symbol="Button_Type1") ]
		
		private var ButtonClass:Class;
		private var button:SpriteAsset;
		
		public function Button_Type1()
		{
			button = new ButtonClass() as SpriteAsset;
			addChild( button );
			
			super( button["inMc"] );
		}
	}
}