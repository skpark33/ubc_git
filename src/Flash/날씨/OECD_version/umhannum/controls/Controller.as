﻿package umhannum.controls
{
	import flash.display.MovieClip;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import umhannum.controls.DesignButton;
	import umhannum.events.CustomEvent;
	import umhannum.events.LineScrollEvent;
	import umhannum.utils.TimeUtil;
	
	
	public class Controller extends MovieClip
	{
		private var fps:Number = 45;
		private var selectedItem:DesignButton;
		private var controlArr:Array = [ "play_mc", "pause_mc", "return_mc" ];
		
		private var _targetClip:MovieClip;
		
		public function Controller()
		{
			super();
		}
		
		public function init():void
		{
			if ( _targetClip != null )
			{
				setEvent();
				defaultSetting();
			}
			else
			{
				throw new Error( "대상 무비클립이 없습니다." );
			}
		}
		
		private function setEvent(): void
		{
			var menuItem:DesignButton;
			var len:int = controlArr.length;
			for ( var i:int=0; i<len; i++ )
			{
				menuItem = this[ controlArr[ i ] ];
				menuItem.check_mc.gotoAndStop( i + 1 );
				menuItem.addEventListener( CustomEvent.CUSTOM_EVENT, onControlHandler );
			}
			caption_mc.addEventListener( MouseEvent.CLICK, onCaptionHandler );
			scroll_mc.addEventListener( LineScrollEvent.LINE_SCROLL_CHANGE, onScrollChange );
		}
		
		private function defaultSetting():void
		{
			scroll_mc.minTargetValue = 1;
			scroll_mc.maxTargetValue = targetClip.totalFrames;

			play_mc.buttonMode = true;
			pause_mc.buttonMode = true;
			return_mc.buttonMode = true;
			caption_mc.buttonMode = true;
		}
		
		private function onControlHandler( evt:CustomEvent ):void
		{
			trace( evt.eventPhase );
			trace( evt.target );
			trace( evt.currentTarget.name );
			
			if ( selectedItem != null ) selectedItem.deselectedItem();
			selectedItem = evt.target as DesignButton;
			selectedItem.selectedItem();
			trace( evt.target.name );
		}
		
		private function onCaptionHandler( evt:MouseEvent ):void
		{
			trace( evt.currentTarget.name );
		}
		
		private function onScrollChange( evt:LineScrollEvent ):void
		{
			var position:Number = Math.round( evt.position );
			var target:MovieClip = evt.target as MovieClip;
			
			targetClip.gotoAndStop( position );
			
			time_txt.text = TimeUtil.displayTime( position, target.maxTargetValue, fps ); 
			
			if ( position == target.maxTargetValue )
			{
				alarm_mc.gotoAndPlay(2);
			}
		}
		
		public function get targetClip():MovieClip
		{
			return _targetClip;
		}
		
		public function set targetClip( value:MovieClip ):void
		{
			_targetClip = value;
		}
	}
}