﻿package umhannum.controls
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import umhannum.events.CustomEvent;
	
	
	[ Event( name="CUSTOM_EVENT", type="umhannum.events.CustomEvent" ) ];
	public class SpriteTest extends Sprite
	{
		public function SpriteTest()
		{
			super();
			setLayout();
		}
		
		public function setLayout():void
		{
			addEventListener( MouseEvent.CLICK, onClick );
		}
		
		public function onClick( evt:MouseEvent ):void
		{
			dispatchEvent( new CustomEvent( CustomEvent.CUSTOM_EVENT ) );
		}
	}
}