// MusicInterface.cpp: implementation of the CMusicInterface class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MusicInterface.h"
#include "Shlwapi.h" 
#include "Mpconfig.h" // Overlay
#include "dshowutil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//const IID IID_IMixerPinConfig2 = { 0xebf47182, 0x8764, 0x11d1, { 0x9e, 0x69, 0x0, 0xc0, 0x4f, 0xd7, 0xc1, 0x5b} };

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CMusicInterface::CMusicInterface(CWnd* pWnd)
: CBasicInterface(pWnd)
{
	
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CMusicInterface::~CMusicInterface()
{
	if(m_bOpen)
	{
		Stop();
		Close();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용한 모든 필터를 해제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CMusicInterface::ReleaseAllInterfaces()
{
	__DEBUG__("Begin ReleaseAllInterfaces", _NULL);

	if(m_bIsPlay)
	{
		Stop();
	}//if
	
	HRESULT hr;
	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx != NULL)
	{
		LIF(pMediaEventEx->SetNotifyWindow((OAHWND)NULL, 0, 0));		//이벤트 통지를 처리하는 윈도우를 등록
	}//if

	//그래프의 필터를 열거한다.
	IEnumFilters *pEnum = NULL;
	LIF(m_pGraphBuilder->EnumFilters(&pEnum));
	if(SUCCEEDED(hr))
	{
		IBaseFilter *pFilter = NULL;
		while(S_OK == pEnum->Next(1, &pFilter, NULL))
		{
			CString strFilterName;
			FILTER_INFO stFilterInfo;
			hr = pFilter->QueryFilterInfo(&stFilterInfo);
			if(SUCCEEDED(hr))
			{
				strFilterName = stFilterInfo.achName;
				__DEBUG__("Release Filter", strFilterName);
				if( stFilterInfo.pGraph ) SAFE_RELEASE(stFilterInfo.pGraph);
			}//if

			//필터를 삭제한다.
			LIF(m_pGraphBuilder->RemoveFilter(pFilter));

			//열거자를 리셋한다.
			pEnum->Reset();
			//SAFE_RELEASE(pFilter);
			if( pFilter->Release() != 0 )
			{
				__DEBUG__("NOT NULL !!!!!!!!!!!!!!!!!!!!!!!!!!!!", strFilterName);
			}
		}//while
		//SAFE_RELEASE(pEnum);
		if( pEnum->Release() != 0 )
		{
			__DEBUG__("IEnumFilters is NOT NULL !!!!!!!!!!!!!!!!!!!!!!!!!!!!", NULL);
		}
	}//if

	//Release and zero DirectShow interfaces
	//SAFE_RELEASE(m_pGraphBuilder);
	__DEBUG__("End ReleaseAllInterfaces", _NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 open한다. \n
/// @param (LPCSTR) lpszFilename : (in) 동영상파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CMusicInterface::Open(LPCSTR lpszFilename)
{
	if(m_bOpen)
	{
		Close();
	}//if

	m_strFilePath = lpszFilename;
	if(m_strFilePath.GetLength() == 0)
	{
		__DEBUG__("Invlid file path", m_strFilePath);
		return false;
	}//if

	//USES_CONVERSION;
	WCHAR szFileName[MAX_PATH] = { 0x00 };
	//wcsncpy(FileName, T2W(strFile), NUMELMS(FileName));
	MultiByteToWideChar(CP_ACP, 0, (LPCTSTR)m_strFilePath, -1, szFileName, MAX_PATH);

	HRESULT hr;
	JIB(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC, IID_IGraphBuilder, (void **)&m_pGraphBuilder));
	
	// Overlay Mixer
	//JIB(CoCreateInstance(CLSID_OverlayMixer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void **)&m_pFilter)); 
	// Add it to the filter graph.
	if(SUCCEEDED(hr))
	{
		//Render file
		//JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));

		//WMV 파일인 경우 필터 설정
		//if(IsWindowsMediaFile(lpszFilename))
		//{
		//	if(!SetWindowsMediaFilters(szFileName))
		//	{
		//		__DEBUG__("Fail to se window media file filter", m_strFilePath);
		//		JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));
		//		//return false;
		//	}
		//	else
		//	{
		//		//Video render 직접추가
		//		//JIB(CoCreateInstance(CLSID_VideoRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pRender));
		//		//if(SUCCEEDED(hr))
		//		//{
		//		//	//Add to filter graph
		//		//	JIB(m_pGraphBuilder->AddFilter(m_pRender, L"Video Render"));
		//		//	//m_pRender->Release();
		//		//}//if

		//		////오버레이믹서와 video render 연결
		//		//CComPtr<IPin> pPinOut;
		//		//CComPtr<IPin> pPinIn;
		//		//pPinOut = GetOutPin(m_pFilter, 0);
		//		//pPinIn = GetInPin(m_pRender, 0);
		//		//JIB(m_pGraphBuilder->Connect(pPinOut, pPinIn));
		//	}//if
		//}
		//else
		{
			//Render file
			JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));
		}//if

/*
		// Have the graph builder construct its the appropriate graph automatically	
		hr = m_pGraphBuilder->RenderFile(lpszwFileInfo, NULL);
		if(hr && hr != VFW_S_PARTIAL_RENDER && hr != VFW_S_AUDIO_NOT_RENDERED)
		{
			TRACE("Video file render faile\r\n");
			return false;
		}//if
*/

/*
		if(::PathFileExists(m_strSamiPath))		// file이 존재하면
        {
            JIB(m_pGraphBuilder->RenderFile(lpszwSamiName, NULL));
            CreateSamiFilter();
        }//if
*/
		// m_pOvMix ColorKey 지정
		//if(!SetColorKey(m_pFilter))
		//{
		//	return false;
		//}//if

		CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
		if(pMediaEventEx != NULL)
		{
			LIF(pMediaEventEx->SetNotifyWindow((OAHWND)(m_hParentWnd), WM_DSINTERFACES_GRAPHNOTIFY, 0)); // 이벤트 통지를 처리하는 윈도우를 등록
		}//if

		// 음성 파일 재생을 위해 JIF 제거
		//JIF(m_pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
		//JIF(m_pVideoWindow->put_MessageDrain((OAHWND)( m_hParentWnd)));	//메세지를 받는 윈도우를 결정
		//JIF(m_pVideoWindow->put_WindowStyle(WS_CHILD));
		//CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		//if(pVideoWindow != NULL)
		//{
		//	LIF(pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
		//	LIF(pVideoWindow->put_MessageDrain((OAHWND)(m_hParentWnd)));	//메세지를 받는 윈도우를 결정
		//	//LIF(pVideoWindow->put_WindowStyle(WS_CHILD/*|WS_CLIPCHILDREN*/));
		//	LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
		//	LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
		//	//LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
		//	LIF(pVideoWindow->put_BackgroundPalette(0));
		//}//if

		//CRect rc;
		//::GetClientRect(m_hParentWnd, &rc);
		//SetVideoRect(rc);		// 윈도우의 크기를 정함

		//SetAspectRatioMode(STRETCHED);
		m_bOpen = true;
		AddToRot(m_pGraphBuilder);

		return true;
	}//if

	return false;
}
