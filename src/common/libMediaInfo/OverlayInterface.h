// OverlayInterface.h: interface for the COverlayInterface class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "BasicInterface.h"


//! Overlay를 사용한 동영상 재생 클래스
/*!
*/
class COverlayInterface : public CBasicInterface
{
public:
	COverlayInterface(CWnd* pWnd);								///<생성자
	virtual ~COverlayInterface(void);							///<소멸자

	// Methods
	virtual bool	ChangeParentWindow(HWND hParentWnd);		///<동영상이 그려질 윈도우 핸들을 변경한다.
	virtual void	ReleaseAllInterfaces(void);					///<사용한 모든 필터를 해제한다.

	virtual bool	SetAspectRatioMode(ASPECT_RATIO_MODE mode);	///<Aspectratio mode를 설정한다.
	virtual bool	GetVideoRect(LPRECT pRect);					///<동영상의 크기를 얻어온다.
	virtual bool	SetVideoRect(LPRECT pRect);					///<동영상의 크기를 설정한다.

	virtual bool	Open(LPCSTR lpszFilename);					///<동영상 파일을 open한다.
	virtual bool	RePaintVideo(void);							///<동영상을 다시 그려준다.
	//virtual void	DisplayChanged(void);						///<모니터가 변경되었을때 처리를 해 준다.

protected:
	DWORD			m_dColorKey;								///<Overlay key color값
	IBaseFilter*	m_pRender;									///<랜더 필더

	bool			ShowFilterPropertyPage(IBaseFilter *pFilter, HWND hwndParent);	///<필터의 속성 정보를 보여준다.
	void			ShowPropertyPage(void);											///<필터속성 페이지

	bool			CreateSamiFilter(void);											///<자막필터를 생성한다.
	bool			SetColorKey(IBaseFilter* pOverlayMixer);						///<오버레이색상키를 생성한다.
};

