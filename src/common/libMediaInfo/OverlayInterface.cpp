// OverlayInterface.cpp: implementation of the COverlayInterface class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OverlayInterface.h"
#include "Shlwapi.h" 
#include "Mpconfig.h" // Overlay
#include "dshowutil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//const IID IID_IMixerPinConfig2 = { 0xebf47182, 0x8764, 0x11d1, { 0x9e, 0x69, 0x0, 0xc0, 0x4f, 0xd7, 0xc1, 0x5b} };

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
COverlayInterface::COverlayInterface(CWnd* pWnd)
: CBasicInterface(pWnd)
, m_pRender(NULL)
{
	
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
COverlayInterface::~COverlayInterface()
{
	if(m_bOpen)
	{
		Stop();
		Close();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상을 다시 그려준다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool COverlayInterface::RePaintVideo()
{
	//m_csLock.Lock();
	if(m_pParentWnd)
	{
		m_pParentWnd->Invalidate();
	}//if
	//m_csLock.Unlock();

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Aspectratio mode를 설정한다. \n
/// @param (ASPECT_RATIO_MODE) mode : (in) Aspectraio mode를 설정한다.
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool COverlayInterface::SetAspectRatioMode(ASPECT_RATIO_MODE mode)
{
	if(m_pFilter == NULL)
	{
		return false;
	}//if

	HRESULT hr;
	CComPtr<IPin> pPin;
	JIB(m_pFilter->FindPin(L"Input0", &pPin));
	if(SUCCEEDED(hr))
	{
		CComQIPtr<IMixerPinConfig, &IID_IMixerPinConfig> pMixerPinConfig(pPin);
		if(pMixerPinConfig == NULL)
		{
			__DEBUG__("Fail to get IMixerPinConfig", _NULL);
			return false;
		}//if

		JIB(pMixerPinConfig->SetAspectRatioMode((AM_ASPECT_RATIO_MODE)mode));
	}
	else
	{
		IBaseFilter* pFilter = FindFilterFromName(m_pGraphBuilder, "Video Renderer");
		
		CComQIPtr<IVMRAspectRatioControl> pAspectRatio(pFilter);
		if(pAspectRatio == NULL)
		{
			__DEBUG__("Fail to get IVMRAspectRatioControl", _NULL);
			return false;
		}//if

		JIB(pAspectRatio->SetAspectRatioMode(mode));
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 크기를 얻어온다. \n
/// @param (LPRECT) pRect : (out) 동영상의 영역
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool COverlayInterface::GetVideoRect(LPRECT pRect)
{
	if(m_pGraphBuilder == NULL)
	{
		return false;
	}//if

	HRESULT hr;
	memset(pRect, 0x00, sizeof(RECT));
	CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
	if(pVideoWindow == NULL)
	{
		return false;
	}//if

	LIF(pVideoWindow->GetWindowPosition(&pRect->left,
										&pRect->top,
										&pRect->right,
										&pRect->bottom));
	//	==> 왜 더하지?
	pRect->right += pRect->left;
	pRect->bottom += pRect->top;
	// <==
	/*pRect->right = (pRect->right - pRect->left);
	pRect->left = 0;
	pRect->bottom = (pRect->bottom - pRect->top);
	pRect->top = 0;*/

	if(SUCCEEDED(hr))
	{
		return true;
	}//if
	
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 크기를 설정한다. \n
/// @param (LPRECT) pRect : (in) 동영상의 영역
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool COverlayInterface::SetVideoRect(LPRECT pRect)
{
	if(m_pGraphBuilder == NULL)
	{
		return false;
	}//if

	HRESULT hr;
	CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
	if(pVideoWindow == NULL)
	{
		return false;
	}//if

	JIB(pVideoWindow->SetWindowPosition(pRect->left,
										pRect->top, 
										pRect->right- pRect->left, 
										pRect->bottom - pRect->top)
	);
	
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상이 그려질 윈도우 핸들을 변경한다. \n
/////////////////////////////////////////////////////////////////////////////////
bool COverlayInterface::ChangeParentWindow(HWND hParentWnd)
{
	HRESULT hr;

	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx != NULL)
	{
		LIF(pMediaEventEx->SetNotifyWindow((OAHWND)(m_hParentWnd), WM_DSINTERFACES_GRAPHNOTIFY, 0)); // 이벤트 통지를 처리하는 윈도우를 등록
	}//if
	else
		return false;

	CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
	if(pVideoWindow != NULL)
	{
		LIF(pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
		LIF(pVideoWindow->put_MessageDrain((OAHWND)(m_hParentWnd)));	//메세지를 받는 윈도우를 결정
		//LIF(pVideoWindow->put_WindowStyle(WS_CHILD/*|WS_CLIPCHILDREN*/));
		LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
		LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
		//LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
		LIF(pVideoWindow->put_BackgroundPalette(0));
	}
	else
		return false;

	CRect rc;
	::GetClientRect(m_hParentWnd, &rc);
	if(!SetVideoRect(rc)) return false;		// 윈도우의 크기를 정함

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용한 모든 필터를 해제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void COverlayInterface::ReleaseAllInterfaces()
{
	__DEBUG__("Begin ReleaseAllInterfaces", _NULL);

	if(m_bIsPlay)
	{
		Stop();
	}//if

	HRESULT hr;
	CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
	if(pVideoWindow != NULL)
	{
		LIF(pVideoWindow->put_Visible(OAFALSE));		//윈도우를 비표시함
		LIF(pVideoWindow->put_Owner(NULL));				//기존의 부모 윈도우를 삭제
	}//if
	
	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx != NULL)
	{
		LIF(pMediaEventEx->SetNotifyWindow((OAHWND)NULL, 0, 0));		//이벤트 통지를 처리하는 윈도우를 등록
	}//if

	//그래프의 필터를 열거한다.
	IEnumFilters *pEnum = NULL;
	LIF(m_pGraphBuilder->EnumFilters(&pEnum));
	if(SUCCEEDED(hr))
	{
		IBaseFilter *pFilter = NULL;
		FILTER_INFO stFilterInfo;
		CString strFilterName;

		while(S_OK == pEnum->Next(1, &pFilter, NULL))
		{
			hr = pFilter->QueryFilterInfo(&stFilterInfo);
			if(SUCCEEDED(hr))
			{
				strFilterName = stFilterInfo.achName;
				__DEBUG__("Release Filter", strFilterName);
				if( stFilterInfo.pGraph ) SAFE_RELEASE(stFilterInfo.pGraph);
			}//if

			//필터를 삭제한다.
			LIF(m_pGraphBuilder->RemoveFilter(pFilter));

			//열거자를 리셋한다.
			pEnum->Reset();
			SAFE_RELEASE(pFilter);
		}//while
		SAFE_RELEASE(pEnum);
	}//if

	//Release and zero DirectShow interfaces
	//SAFE_RELEASE(m_pGraphBuilder);
	__DEBUG__("End ReleaseAllInterfaces", _NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 open한다. \n
/// @param (LPCSTR) lpszFilename : (in) 동영상파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool COverlayInterface::Open(LPCSTR lpszFilename)
{
	if(m_bOpen)
	{
		Close();
	}//if

	m_strFilePath = lpszFilename;
	if(m_strFilePath.GetLength() == 0)
	{
		__DEBUG__("Invlid file path", m_strFilePath);
		return false;
	}//if

	//USES_CONVERSION;
	WCHAR szFileName[MAX_PATH] = { 0x00 };
	//wcsncpy(FileName, T2W(strFile), NUMELMS(FileName));
	MultiByteToWideChar(CP_ACP, 0, (LPCTSTR)m_strFilePath, -1, szFileName, MAX_PATH);

	HRESULT hr;
	JIB(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC, IID_IGraphBuilder, (void **)&m_pGraphBuilder));
	
	// Overlay Mixer
	JIB(CoCreateInstance(CLSID_OverlayMixer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void **)&m_pFilter)); 
	// Add it to the filter graph.
	if(SUCCEEDED(hr))
	{
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Overlay Mixer"));
		m_pFilter->Release();

		//Render file
		//JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));

		//WMV 파일인 경우 필터 설정
		if(IsWindowsMediaFile(lpszFilename))
		{
			if(!SetWindowsMediaFilters(szFileName))
			{
				__DEBUG__("Fail to se window media file filter", m_strFilePath);
				JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));
				//return false;
			}
			else
			{
				//Video render 직접추가
				JIB(CoCreateInstance(CLSID_VideoRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pRender));
				if(SUCCEEDED(hr))
				{
					//Add to filter graph
					JIB(m_pGraphBuilder->AddFilter(m_pRender, L"Video Render"));
					m_pRender->Release();
				}//if

				//오버레이믹서와 video render 연결
				CComPtr<IPin> pPinOut;
				CComPtr<IPin> pPinIn;
				pPinOut = GetOutPin(m_pFilter, 0);
				pPinIn = GetInPin(m_pRender, 0);
				JIB(m_pGraphBuilder->Connect(pPinOut, pPinIn));
			}//if
		}
		else
		{
			//Render file
			JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));
		}//if

/*
		// Have the graph builder construct its the appropriate graph automatically	
		hr = m_pGraphBuilder->RenderFile(lpszwFileInfo, NULL);
		if(hr && hr != VFW_S_PARTIAL_RENDER && hr != VFW_S_AUDIO_NOT_RENDERED)
		{
			TRACE("Video file render faile\r\n");
			return false;
		}//if
*/

/*
		if(::PathFileExists(m_strSamiPath))		// file이 존재하면
        {
            JIB(m_pGraphBuilder->RenderFile(lpszwSamiName, NULL));
            CreateSamiFilter();
        }//if
*/
		// m_pOvMix ColorKey 지정
		if(!SetColorKey(m_pFilter))
		{
			return false;
		}//if

		CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
		if(pMediaEventEx != NULL)
		{
			LIF(pMediaEventEx->SetNotifyWindow((OAHWND)(m_hParentWnd), WM_DSINTERFACES_GRAPHNOTIFY, 0)); // 이벤트 통지를 처리하는 윈도우를 등록
		}//if

		// 음성 파일 재생을 위해 JIF 제거
		//JIF(m_pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
		//JIF(m_pVideoWindow->put_MessageDrain((OAHWND)( m_hParentWnd)));	//메세지를 받는 윈도우를 결정
		//JIF(m_pVideoWindow->put_WindowStyle(WS_CHILD));
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			LIF(pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
			LIF(pVideoWindow->put_MessageDrain((OAHWND)(m_hParentWnd)));	//메세지를 받는 윈도우를 결정
			//LIF(pVideoWindow->put_WindowStyle(WS_CHILD/*|WS_CLIPCHILDREN*/));
			LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
			LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
			//LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
			LIF(pVideoWindow->put_BackgroundPalette(0));
		}
		else
			return false;

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함

		//SetAspectRatioMode(STRETCHED);
		m_bOpen = true;
		AddToRot(m_pGraphBuilder);

		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 필터속성 페이지 \n
/////////////////////////////////////////////////////////////////////////////////
void COverlayInterface::ShowPropertyPage()
{

    // Show the property page for the selected DMO or filter
/*
	
	if( m_pVC != NULL )
	{
		ShowFilterPropertyPage(m_pVC, NULL);
	}*/
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 필터의 속성 정보를 보여준다. \n
/// @param (IBaseFilter) *pFilter : (in/out) 속성을 보고자하는 필터
/// @param (HWND) hwndParent : (in/out) 속성을 표시할 윈도우 핸들
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool COverlayInterface::ShowFilterPropertyPage(IBaseFilter *pFilter, HWND hwndParent)
{
    HRESULT hr;
    ISpecifyPropertyPages *pSpecify=0;

    if(!pFilter)
	{
        return false;
	}//if

    // Discover if this filter contains a property page
    JIB(pFilter->QueryInterface(IID_ISpecifyPropertyPages, (void **)&pSpecify));	
    if(SUCCEEDED(hr)) 
    {
        do 
        {
            FILTER_INFO FilterInfo;
            hr = pFilter->QueryFilterInfo(&FilterInfo);
            if(FAILED(hr))
			{
                break;
			}//if

            CAUUID caGUID;			
            hr = pSpecify->GetPages(&caGUID);
            if(FAILED(hr))
			{
                break;
			}//if
			
            SAFE_RELEASE(pSpecify);
        			
            // Display the filter's property page
            OleCreatePropertyFrame(
                hwndParent,             // Parent window
                0,                      // x (Reserved)
                0,                      // y (Reserved)
                FilterInfo.achName,     // Caption for the dialog box
                1,                      // Number of filters
                (IUnknown **)&pFilter,  // Pointer to the filter 
                caGUID.cElems,          // Number of property pages
                caGUID.pElems,          // Pointer to property page CLSIDs
                0,                      // Locale identifier
                0,                      // Reserved
                NULL                    // Reserved
            );
            CoTaskMemFree(caGUID.pElems);
            FilterInfo.pGraph->Release(); 

        } while(0);
    }//if

//  pFilter->Release();
    return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 자막필터를 생성한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool COverlayInterface::CreateSamiFilter()
{
	if(m_pGraphBuilder == NULL)
	{
		return false;
	}//if

    HRESULT hr;
    IBaseFilter* pSAMI;

    JIB(m_pGraphBuilder->FindFilterByName( L"SAMI (CC) Parser", &pSAMI));
    if(SUCCEEDED(hr))			// 자막이 존재하면
    {
        IAMStreamSelect *pStrm = NULL;
        JIB(pSAMI->QueryInterface(IID_IAMStreamSelect, (void**)&pStrm));
        if(SUCCEEDED(hr))		// 인터페이스 연결이 성공하면 
        {
            DWORD dwStreams = 0;
            pStrm->Count(&dwStreams);		// 이용가능한 스트림의 수를 얻어오고

            // Select French and "GreenText"   지정한 스트림을 유효화 시키고
            LIF(pStrm->Enable(1, AMSTREAMSELECTENABLE_ENABLEALL));
            LIF(pStrm->Enable(3, AMSTREAMSELECTENABLE_ENABLEALL));

            // Print the name of each logical stream.
            for(DWORD index = 0; index < dwStreams; index++)	// 이용가능한 스트림의 수만큼
            {
                DWORD dwFlags;
                WCHAR *wszName;
                LIF(pStrm->Info(index, NULL, &dwFlags, NULL, NULL, &wszName, NULL, NULL));	// 스트림의 정보를 얻어오고
                CoTaskMemFree(wszName);		// the CoTaskMemAlloc or CoTaskMemRealloc function에 의해 이전에 잡힌 메모리를 해제시킴
            }//for
            SAFE_RELEASE(pStrm);
			SAFE_RELEASE(pSAMI);
        }//if
    }//if

    return false;
} 


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 오버레이색상키를 생성한다. \n
/// @param (IBaseFilter) *pOverlayMixer : (in) 오버레이믹서 필터 인터페이스
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool COverlayInterface::SetColorKey(IBaseFilter *pOverlayMixer)
{
	if(pOverlayMixer == NULL)
	{
		return false;
	}//if

    IPin* pPin = NULL;
    BOOL bFound = FALSE;
    IEnumPins *pEnum;
	HRESULT hr;

    JIB(pOverlayMixer->EnumPins(&pEnum));
    while(pEnum->Next(1, &pPin, 0) == S_OK)				// 첫번째 인수 = 핀의 수
    {
        PIN_DIRECTION PinDirThis;						// 핀의 방향을 나타냄
        pPin->QueryDirection(&PinDirThis);				// 핀의 방향을 얻어옴
        if(bFound = (PINDIR_INPUT == PinDirThis))		// 입력받은 핀과 같으면
		{
            break;
		}//if

        SAFE_RELEASE(pPin);								// 일치하는 것이 없으면 제거
    }//while

	CComQIPtr<IOverlay> pOverlay(pPin);
	CComQIPtr<IMixerPinConfig2, &IID_IMixerPinConfig2> pMixerPinConfig2(pPin);
	if(pOverlay == NULL || pMixerPinConfig2 == NULL)
	{
		SAFE_RELEASE(pPin);
		SAFE_RELEASE(pEnum);
		return false;
	}//if

	COLORKEY clrkey;
	JIB(pOverlay->GetColorKey(&clrkey));

	DWORD dColorKey = clrkey.HighColorValue;// = 0x100010;
	m_dColorKey = dColorKey;

	LIF(pOverlay->SetPalette(0, NULL));
	LIF(pOverlay->SetColorKey(&clrkey));

	//AM_ASPECT_RATIO_MODE mode = (AM_ASPECT_RATIO_MODE)m_AspectRatioMode;
	//m_pMixerPinConfig2->GetAspectRatioMode( &mode);	// 윈도우의 사이즈 변경에서의 어스펙트비(가로세로 비율) 보정 모드를 얻어옴

    SAFE_RELEASE(pPin);
    SAFE_RELEASE(pEnum);

    /*if(pOverlayMixer)
        m_pOverlayMixer = NULL;*/

	return true;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 모니터가 변경되었을때 처리를 해 준다. \n
/////////////////////////////////////////////////////////////////////////////////
void COverlayInterface::DisplayChanged()
{
}
*/