/************************************************************************************/
/*! @file VMRRender.cpp
	@brief VMR을 이용한 동영상 재생 클래스 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/01/07\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2010/01/07:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"
#include "VMRRender.h"
#include <math.h>
#include <io.h>
#include <D3d9.h>
#include <Vmr9.h>
#include <d3dx9tex.h>
//#include "dshowutil.h"


//static HRESULT SetRenderingMode(IBaseFilter* pBaseFilter, VMRMode mode)
//{
//    // Test VMRConfig, VMRMonitorConfig
//    IVMRFilterConfig* pConfig;
//
//    HRESULT hr = pBaseFilter->QueryInterface(IID_IVMRFilterConfig, (LPVOID *)&pConfig);
//    if(SUCCEEDED(hr))
//    {
//        hr = pConfig->SetRenderingMode(mode);
//        hr = pConfig->SetRenderingPrefs(RenderPrefs_ForceOverlays|RenderPrefs_AllowOverlays);
//        pConfig->Release();
//    }//if
//    return hr;
//}//if


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (HWND) hwnd : (in) 동영상을 재생할 창의 핸들
/// @param (int) nMode : (in) 동영상을 랜더링 할 모드
/////////////////////////////////////////////////////////////////////////////////
CVMRRender::CVMRRender(CWnd* pWnd)
: CBasicInterface(pWnd)
//, m_pVideoDisplayControl(NULL)
{
	
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CVMRRender::~CVMRRender()
{
	if(m_bOpen)
	{
		Stop();
		Close();
	}//if

	m_hParentWnd = NULL;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 오픈한다. \n
/// @param (LPCSTR) lpszFilename : (in) 재생하려는 파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::Open(LPCSTR lpszFilename)
{
	__DEBUG__("Open", lpszFilename);

	if(m_bOpen)
	{
		Close();
	}//if

	m_strFilePath = lpszFilename;
	if(m_strFilePath.GetLength() == 0)
	{
		__DEBUG__("Invlid file path", m_strFilePath);
		return false;
	}//if

	//USES_CONVERSION;
	WCHAR szFileName[MAX_PATH] = { 0x00 };
	//wcsncpy(FileName, T2W(strFile), NUMELMS(FileName));
	MultiByteToWideChar(CP_ACP, 0, (LPCTSTR)m_strFilePath, -1, szFileName, MAX_PATH);

	HRESULT hr;
	JIB(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (LPVOID *)&m_pGraphBuilder));
	//JIB(CoCreateInstance(CLSID_FilterGraphNoThread, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (LPVOID *)&m_pGraphBuilder));
	
	//Setting render filter
	if(!SetRenderFilter())
	{
		Close();
		return false;
	}//if

	//Render file
	//JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));

	if(IsWindowsMediaFile(lpszFilename))
	{
		if(!SetWindowsMediaFilters(szFileName))
		{
			__DEBUG__("Fail to se window media file filter", m_strFilePath);
			JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));
			//return false;
		}//if
	}
	else
	{
		//Render file
		//JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));
		if(FAILED(hr=(m_pGraphBuilder->RenderFile(szFileName, NULL)))) 
		{
			__DEBUG__("RenderFile failed", NULL); 
			m_bOpen = true; // close 시키기 위해 flag 를 true 로 바꾼다.
			Close();
			return false;
		}


	}//if

	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx == NULL)
	{
		__DEBUG__("Fail to get IMediaEventEx", _NULL);
		m_bOpen = true; // close 시키기 위해 flag 를 true 로 바꾼다.
		return false;
	}//if

	LIF(pMediaEventEx->SetNotifyWindow((OAHWND)(m_hParentWnd), WM_DSINTERFACES_GRAPHNOTIFY, 0)); // 이벤트 통지를 처리하는 윈도우를 등록
	/*
	LIF(pMediaEventEx->CancelDefaultHandling(EC_DEVICE_LOST));
	LIF(pMediaEventEx->CancelDefaultHandling(EC_REPAINT));
	LIF(pMediaEventEx->CancelDefaultHandling(EC_WINDOW_DESTROYED));
	LIF(pMediaEventEx->CancelDefaultHandling(EC_VMR_RECONNECTION_FAILED));
	LIF(pMediaEventEx->CancelDefaultHandling(EC_EXTDEVICE_MODE_CHANGE));
	LIF(pMediaEventEx->CancelDefaultHandling(EC_VMR_SURFACE_FLIPPED));
	*/
	if(m_bIsWindowMedia)
	{
		LIF(pMediaEventEx->CancelDefaultHandling(EC_DISPLAY_CHANGED));	//모니터가 변경될때에 동영상이 멈추는 문제처리
	}//if
	
	if(m_nRenderType == E_WINDOWED || m_nRenderType == E_OVERLAY)
	{
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			LIF(pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
			LIF(pVideoWindow->put_MessageDrain((OAHWND)(m_hParentWnd)));	//메세지를 받는 윈도우를 결정
			LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
			LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
			LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
			LIF(pVideoWindow->put_BackgroundPalette(0));
			//LIF(pVideoWindow->put_WindowStyle(WS_OVERLAPPED));			//  렌더링을 위한 구문
		}//if

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함
	}//if

	m_bOpen = true;
	AddToRot(m_pGraphBuilder);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 비디오 랜더를 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetRenderFilter()
{
	if(!m_pGraphBuilder)
	{
		__DEBUG__("m_pGraphBuilder is NULL", _NULL);
		return false;
	}//if

	bool bRet = false;
	switch(m_nRenderType)
	{
	case E_WINDOWLESS:
		{
			if( (GetOSVersion() == E_OS_WIN7 && GetVMRMode() == E_AUTO_VMR_MODE ) || // win7 & vmr-auto-mode
				GetVMRMode() == E_MANUAL_VMR9_MODE )								// manual-vmr9-mode
			{
				__DEBUG__("VMR9 windowless mode", _NULL);
				bRet = SetVMR9WindowLess();
			}
			else
			{
				__DEBUG__("VMR7 windowless mode", _NULL);
				bRet = SetVMR7WindowLess();
			}//if
		}
		break;
	case E_WINDOWED:
		{
			if(GetOSVersion() == E_OS_WIN7)
			{
			}
			else
			{
				__DEBUG__("VMR7 windowed mode", _NULL);
				bRet = SetVMR7Windowed();
			}//if
		}
		break;
		/*
	case E_EVR:
		{
			__DEBUG__("Enhanced video renderer mode", _NULL);
			bRet = SetEnhancedVideoRenderer();
		}
		break;
		*/
	case E_OVERLAY:
		{
			__DEBUG__("Overlay Mixer mode", _NULL);
			bRet = SetOverlay();
		}
		break;
	default :
		{
			__DEBUG__("Default Windowed mode", _NULL);
			bRet = SetDefaultWindowed();
		}
	}//switch
	
	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VMR7 windowless 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetVMR7WindowLess()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_VideoMixingRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Video Mixing Renderer 7"));
		m_pFilter->Release();

		//Set rendering mode
		CComQIPtr<IVMRFilterConfig> pConfig(m_pFilter);
		if(pConfig == NULL)
		{
			__DEBUG__("Fail to get IID_IVMRFilterConfig", _NULL);
			Close();
			return false;
		}//if

		JIB(pConfig->SetRenderingMode(VMRMode_Windowless));

		CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
		if(pWindowlessControl == NULL)
		{
			__DEBUG__("Fail to get IID_IVMRWindowlessControl", _NULL);
			Close();
			return false;
		}//if
		
		JIB(pWindowlessControl->SetVideoClippingWindow(m_hParentWnd));
		//JIB(pWindowlessControl->SetBorderColor(RGB(0, 0, 0)));

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		JIB(pWindowlessControl->SetVideoPosition(NULL, &rc));

		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VMR9 windowless 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetVMR9WindowLess()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_VideoMixingRenderer9, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		//JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Video Mixing Renderer 9"));
		m_pGraphBuilder->AddFilter(m_pFilter, L"Video Mixing Renderer 9");

		//Set rendering mode
		CComQIPtr<IVMRFilterConfig9> pConfig(m_pFilter);
		if(pConfig == NULL)
		{
			__DEBUG__("Fail to get IID_IVMRFilterConfig9", _NULL);
			m_pFilter->Release();
			Close();
			return false;
		}//if

		//JIB(pConfig->SetRenderingMode(VMR9Mode_Windowless));
		pConfig->SetRenderingMode(VMR9Mode_Windowless);
		//LIF(pConfig->SetNumberOfStreams(1));

		CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
		if(pWindowlessControl == NULL)
		{
			__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
			m_pFilter->Release();
			return false;
		}//if

		//JIB(pWindowlessControl->SetVideoClippingWindow(m_hParentWnd));
		pWindowlessControl->SetVideoClippingWindow(m_hParentWnd);
		//JIB(pWindowlessControl->SetBorderColor(RGB(0, 0, 0)));

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		//JIB(pWindowlessControl->SetVideoPosition(NULL, &rc));
		pWindowlessControl->SetVideoPosition(NULL, &rc);

		m_pFilter->Release();

		return true;
	}//if
	__DEBUG__("Fail to get CoCreateInstance of VMR9WindowLess", _NULL);
	return false;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// EnhancedVideoRenderer를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetEnhancedVideoRenderer()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_EnhancedVideoRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Enhanced Video Renderer"));
		m_pFilter->Release();

		IMFGetService *pService = NULL;
		IMFVideoDisplayControl *pDisplay = NULL;

		hr = m_pFilter->QueryInterface(__uuidof(IMFGetService), (void**)&pService); 
		if(SUCCEEDED(hr)) 
		{ 
			hr = pService->GetService(MR_VIDEO_RENDER_SERVICE, 
										__uuidof(IMFVideoDisplayControl),
										(void**)&pDisplay);
		}
		else
		{
			return false;
		}//if

		// Set the clipping window.
		if(SUCCEEDED(hr))
		{
			hr = pDisplay->SetVideoWindow(m_hParentWnd);
		}//if

		if(SUCCEEDED(hr))
		{
			m_pVideoDisplayControl = pDisplay;
			m_pVideoDisplayControl->AddRef();
		}//if

		SAFE_RELEASE(pService);
		SAFE_RELEASE(pDisplay);

		return true;
	}//if

	return false;
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VMR7 windowed 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetVMR7Windowed()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_VideoMixingRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Video Mixing Renderer 7"));
		m_pFilter->Release();

		//이곳에서 설정하면 왠지 Active Window에서 재생이 된다...
/*
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			LIF(pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
			LIF(pVideoWindow->put_MessageDrain((OAHWND)(m_hParentWnd)));	//메세지를 받는 윈도우를 결정
			LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
			LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
			LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
			LIF(pVideoWindow->put_BackgroundPalette(0));
			//LIF(pVideoWindow->put_WindowStyle(WS_OVERLAPPED));				//  렌더링을 위한 구문
		}//if

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함
*/
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// System default windowed 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetDefaultWindowed()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_VideoRendererDefault, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Default Video Render"));
		m_pFilter->Release();

		//이곳에서 설정하면 왠지 Active Window에서 재생이 된다...
/*
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			LIF(pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
			LIF(pVideoWindow->put_MessageDrain((OAHWND)(m_hParentWnd)));	//메세지를 받는 윈도우를 결정
			LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
			LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
			LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
			LIF(pVideoWindow->put_BackgroundPalette(0));
			//LIF(pVideoWindow->put_WindowStyle(WS_OVERLAPPED));				//  렌더링을 위한 구문
		}//if

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함
*/
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Overlay 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetOverlay()
{
	HRESULT hr;
	//IBaseFilter* pOverlay = NULL;

	JIB(CoCreateInstance(CLSID_OverlayMixer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Overlay Mixer"));
		m_pFilter->Release();
/*
		JIB(CoCreateInstance(CLSID_VideoRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
		//JIB(CoCreateInstance(CLSID_VideoRendererDefault, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
		if(SUCCEEDED(hr))
		{
			//Add to filter graph
			JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Video Renderer"));
			m_pFilter->Release();

			return true;
		}//if
*/
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상이 그려질 윈도우 핸들을 변경한다. \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::ChangeParentWindow(HWND hParentWnd)
{
	__DEBUG__("ChangeParentWindow", _NULL);

	if(!m_pGraphBuilder)
	{
		__DEBUG__("m_pGraphBuilder is NULL", _NULL);
		return false;
	}//if

	bool bRet = false;
	HRESULT hr;
	switch(m_nRenderType)
	{
	case E_WINDOWLESS:
		{
			if(GetOSVersion() == E_OS_WIN7)
			{
				__DEBUG__("VMR9 windowless mode", _NULL);

				//bRet = SetVMR9WindowLess();
				{
					CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
					if(pWindowlessControl == NULL)
					{
						__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
						return false;
					}//if

					__DEBUG__("SetVideoClippingWindow", _NULL);
					JIB(pWindowlessControl->SetVideoClippingWindow(hParentWnd));
					//JIB(pWindowlessControl->SetBorderColor(RGB(0, 0, 0)));

					CRect rc;
					::GetClientRect(hParentWnd, &rc);
					__DEBUG__("SetVideoPosition", _NULL);
					JIB(pWindowlessControl->SetVideoPosition(NULL, &rc));
				}
			}
			else
			{
				__DEBUG__("VMR7 windowless mode", _NULL);
				//bRet = SetVMR7WindowLess();
				{
					CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
					if(pWindowlessControl == NULL)
					{
						__DEBUG__("Fail to get IID_IVMRWindowlessControl", _NULL);
						Close();
						return false;
					}//if
					
					__DEBUG__("SetVideoClippingWindow", _NULL);
					JIB(pWindowlessControl->SetVideoClippingWindow(hParentWnd));
					//JIB(pWindowlessControl->SetBorderColor(RGB(0, 0, 0)));

					CRect rc;
					::GetClientRect(hParentWnd, &rc);
					__DEBUG__("SetVideoPosition", _NULL);
					JIB(pWindowlessControl->SetVideoPosition(NULL, &rc));
				}
			}//if
		}
		break;
	}//switch

	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx == NULL)
	{
		__DEBUG__("Fail to get IMediaEventEx", _NULL);
		return false;
	}//if

	__DEBUG__("SetNotifyWindow", _NULL);
	LIF(pMediaEventEx->SetNotifyWindow((OAHWND)(hParentWnd), WM_DSINTERFACES_GRAPHNOTIFY, 0)); // 이벤트 통지를 처리하는 윈도우를 등록

	if(m_nRenderType == E_WINDOWED || m_nRenderType == E_OVERLAY)
	{
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			__DEBUG__("E_WINDOWED or E_OVERLAY", _NULL);
			LIF(pVideoWindow->put_Owner((OAHWND)(hParentWnd)));			//부모윈도우를 결정
			LIF(pVideoWindow->put_MessageDrain((OAHWND)(hParentWnd)));	//메세지를 받는 윈도우를 결정
			LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
			LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
			LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
			LIF(pVideoWindow->put_BackgroundPalette(0));
			//LIF(pVideoWindow->put_WindowStyle(WS_OVERLAPPED));			//  렌더링을 위한 구문
		}//if

		CRect rc;
		::GetClientRect(hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함
	}//if

	SetParentWindow(hParentWnd);
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 그래프빌더의 모든 필터와 인터페이스들을 제거한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CVMRRender::ReleaseAllInterfaces()
{
	__DEBUG__("Begin ReleaseAllInterfaces", _NULL);

	if(m_bIsPlay)
	{
		Stop();
	}//if

	HRESULT hr;
	if(m_nRenderType == E_WINDOWED || m_nRenderType == E_OVERLAY)
	{
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			__DEBUG__("Window visible and Owner release", _NULL);
			LIF(pVideoWindow->put_Owner(NULL));				//기존의 부모 윈도우를 삭제
			LIF(pVideoWindow->put_MessageDrain(NULL));		//메세지를 받는 윈도우를 결정
			LIF(pVideoWindow->put_AutoShow(OAFALSE));		// -1 온(=OATURE), 0 오프(=OAFLASE)
			LIF(pVideoWindow->put_Visible(OAFALSE));		//윈도우를 비표시함
		}//if
	}//if

{
	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
/*
	//먼저 정지할때까지 기다린다.(2초)
	__DEBUG__("Begin Wait 2sec until stop", _NULL);
	long evCode;
	pMediaEventEx->WaitForCompletion(2000, &evCode);
	__DEBUG__("End Wait 2sec until stop", evCode);
*/
	if(pMediaEventEx != NULL)
	{
		__DEBUG__("Event release", _NULL);
		LIF(pMediaEventEx->SetNotifyWindow((OAHWND)NULL, 0, 0));		//이벤트 통지를 처리하는 윈도우를 등록
	}//if
}
	HandleGraphNotify(0);

/*
	//그래프를 정지한다.
	__DEBUG__("Begin Stop", _NULL);
	Stop();
	__DEBUG__("End Stop", _NULL);
*/

	//그래프의 필터를 열거한다.
	IEnumFilters *pEnum = NULL;
	LIF(m_pGraphBuilder->EnumFilters(&pEnum));
	FILTER_INFO stFilterInfo;
	CString strFilterName;
	int nRefCount = 0;
	if(SUCCEEDED(hr))
	{
		pEnum->Reset();

		IBaseFilter *pFilter = NULL;
		while(S_OK == pEnum->Next(1, &pFilter, NULL))
		{
///*			
			//필터를 삭제한다.
			hr = pFilter->QueryFilterInfo(&stFilterInfo);
			if(SUCCEEDED(hr))
			{
				strFilterName = stFilterInfo.achName;
				__DEBUG__("Release Filter", strFilterName);
				if( stFilterInfo.pGraph ) SAFE_RELEASE(stFilterInfo.pGraph);
			}//if
//*/		
			LIF(m_pGraphBuilder->RemoveFilter(pFilter));

			//열거자를 리셋한다.
			pEnum->Reset();
			//SAFE_RELEASE(pFilter);
			if( pFilter->Release() != 0 )
			{
				__DEBUG__("NOT NULL !!!!!!!!!!!!!!!!!!!!!!!!!!!!", strFilterName);
			}
/*			
			int nRef;
			do
			{
				nRef = pFilter->Release ();
				__DEBUG__("Filter ref count", nRef);
			}while(nRef > 1);
*/		
		}//while
		SAFE_RELEASE(pEnum);
	}//if

/*
	if(m_nRenderType == E_EVR)
	{
		SAFE_RELEASE(m_pVideoDisplayControl);
	}//if
*/

	__DEBUG__("End ReleaseAllInterfaces", _NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상을 다시 그려준다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::RePaintVideo()
{
	//동영상이 재생중이 아닐때 다시 그리면 멈출수가 있다.
	if(!m_bIsPlay)
	{
		__DEBUG__("Not play state", _NULL);
		return false;
	}//if

	//m_csLock.Lock();
	bool bRet = false;
	if(m_nRenderType == E_WINDOWLESS)
	{
		if(GetOSVersion() == E_OS_WIN7)
		{
			CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
			if(pWindowlessControl != NULL)
			{
				HRESULT hr;
				//JIB(pWindowlessControl->RepaintVideo(m_hParentWnd, hdc));
				HDC hDC = ::GetDC(m_hParentWnd);
				__DEBUG__("Begin repaint video", _NULL);
				if(SUCCEEDED(pWindowlessControl->RepaintVideo(m_hParentWnd, hDC)))
				{
					bRet = true;
				}//if
				__DEBUG__("End repaint video", _NULL);
				::ReleaseDC(m_hParentWnd, hDC);
			}
			else
			{
				__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
			}//if				
		}
		else
		{
			CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
			if(pWindowlessControl != NULL)
			{
				HRESULT hr;
				//JIB(pWindowlessControl->RepaintVideo(m_hParentWnd, hdc));
				HDC hDC = ::GetDC(m_hParentWnd);
				__DEBUG__("Begin repaint video", _NULL);
				if(SUCCEEDED(pWindowlessControl->RepaintVideo(m_hParentWnd, hDC)))
				{
					bRet = true;
				}//if
				__DEBUG__("End repaint video", _NULL);
				::ReleaseDC(m_hParentWnd, hDC);
			}
			else
			{
				__DEBUG__("Fail to get IID_IVMRWindowlessControl", _NULL);
			}//if
		}//if
	}
/*
	else if(m_nRenderType == E_EVR)
	{
		if(m_pVideoDisplayControl == NULL)
		{
			__DEBUG__("m_pVideoDisplayControl is NULL", _NULL);
			return false;
		}//if

		HRESULT hr;
		JIB(m_pVideoDisplayControl->RepaintVideo());
	}//if
*/
	//m_csLock.Unlock();
	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 영역을 설정한다. \n
/// @param (LPRECT) pRect : (in) 설정하려는 동영상의 영역
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetVideoRect(LPRECT pRect)
{
	HRESULT hr;
	if(m_nRenderType == E_WINDOWLESS)
	{
		if(GetOSVersion() == E_OS_WIN7)
		{
			CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->SetVideoPosition(NULL, pRect));
		}
		else
		{
			CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->SetVideoPosition(NULL, pRect));
		}//if
	}
/*
	else if(m_nRenderType == E_EVR)
	{
		if(m_pVideoDisplayControl == NULL)
		{
			__DEBUG__("m_pVideoDisplayControl is NULL", _NULL);
			return false;
		}//if

		HRESULT hr;
		JIB(m_pVideoDisplayControl->SetVideoPosition(NULL, pRect));
	}
*/
	else
	{
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow == NULL)
		{
			__DEBUG__("Fail to get IVideoWindow", _NULL);
			return false;
		}//if

		JIB(pVideoWindow->SetWindowPosition(pRect->left,
											pRect->top,
											(pRect->right-pRect->left),
											(pRect->bottom-pRect->top)));
	}//if

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 원본영역을 반한한다. \n
/// @param (LPRECT) pRect : (out) 동영상의 원본 영역
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::GetVideoRect(LPRECT pRect)
{
	memset(pRect, 0x00, sizeof(RECT));
	HRESULT hr;
	if(m_nRenderType == E_WINDOWLESS)
	{
		if(GetOSVersion() == E_OS_WIN7)
		{
			CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->GetNativeVideoSize(&pRect->right, &pRect->bottom, NULL, NULL));
		}
		else
		{
			CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->GetNativeVideoSize(&pRect->right, &pRect->bottom, NULL, NULL));
		}//if
	}
/*
	else if(m_nRenderType == E_EVR)
	{
		HRESULT hr;
		if(m_pVideoDisplayControl == NULL)
		{
			__DEBUG__("m_pVideoDisplayControl is NULL", _NULL);
			return false;
		}//if

		SIZE size;
		JIB(m_pVideoDisplayControl->GetNativeVideoSize(&size, NULL));
		pRect->right = size.cx;
		pRect->bottom= size.cy;
	}
*/
	else
	{
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow == NULL)
		{
			__DEBUG__("Fail to get IVideoWindow", _NULL);
			return false;
		}//if

		JIB(pVideoWindow->GetWindowPosition(&pRect->left, &pRect->top, &pRect->right, &pRect->bottom));
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 재생시에 화면의 비율을 유지하는지를 설정한다. \n
/// @param (ASPECT_RATIO_MODE) mode : (in) STRETCHED, LETTER_BOX, ....
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetAspectRatioMode(ASPECT_RATIO_MODE mode)
{
	bool bRet = FALSE;
	HRESULT hr;

	if(m_nRenderType == E_WINDOWLESS)
	{
		if(GetOSVersion() == E_OS_WIN7)
		{
			CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->SetAspectRatioMode(mode));
		}
		else
		{
			CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->SetAspectRatioMode(mode));
		}//if
	}
/*
	else if(m_nRenderType == E_EVR)
	{
		HRESULT hr;
		if(m_pVideoDisplayControl == NULL)
		{
			__DEBUG__("m_pVideoDisplayControl is NULL", _NULL);
			return false;
		}//if

		JIB(m_pVideoDisplayControl->SetAspectRatioMode(mode));
	}
*/
	else if(m_nRenderType == E_OVERLAY)
	{
		CComPtr<IPin> pPin;
		JIB(m_pFilter->FindPin(L"Input0", &pPin));
		if(SUCCEEDED(hr))
		{
			CComQIPtr<IMixerPinConfig, &IID_IMixerPinConfig> pMixerPinConfig(pPin);
			if(pMixerPinConfig == NULL)
			{
				__DEBUG__("Fail to get IMixerPinConfig", _NULL);
				return false;
			}//if

			JIB(pMixerPinConfig->SetAspectRatioMode((AM_ASPECT_RATIO_MODE)mode))
		}//if
	}
	else
	{
		CComQIPtr<IVMRAspectRatioControl> pAspectRatio(m_pFilter);
		if(pAspectRatio == NULL)
		{
			__DEBUG__("Fail to get IVMRAspectRatioControl", _NULL);
			return false;
		}//if

		JIB(pAspectRatio->SetAspectRatioMode(mode));
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우 버전을 구한다.(major 버전만 보고 판단한다.)\n
/// @return <형: int> \n
///			<-1: error> \n
///			<5: WinXP> \n
///			<6: Vista, Win7> \n
/////////////////////////////////////////////////////////////////////////////////
int	CVMRRender::GetOSVersion()
{
	int os_ver = -1;

	if( os_ver < 0 )
	{
		OSVERSIONINFO stOSver;
		memset(&stOSver, 0x00, sizeof(OSVERSIONINFO));
		stOSver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

		if( !GetVersionEx(&stOSver) )
		{        
			__DEBUG__("Get OS version fail", _NULL);
		}
		else
		{
			os_ver = stOSver.dwMajorVersion;
		}
	}
	return os_ver;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 현재화면의 이미지를 얻어온다. \n
/// @param (BYTE**) pByte : (out) 이미지 데이터
/// @param (double) dbPos : (in) 이미지를 얻어올 위치(시간)
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::GetCurrentImage(BYTE** pByte, double dbPos)
{
	if(m_nRenderType != E_WINDOWLESS)
	{
		return false;
	}//if

	//샘플의 위치로 이동
	SetPosition(dbPos);
	if(!Play())
	{
		return false;
	}//if

	if(!Pause())
	{
		return false;
	}//if

	Sleep(500);		//잠시 기다려야지만 이미지를 얻어온다...ㅠㅠ

	HRESULT hr;
	if(GetOSVersion() == E_OS_WIN7)
	{
		CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
		if(pWindowlessControl == NULL)
		{
			return false;
		}//if

		JIB(pWindowlessControl->GetCurrentImage(pByte));
	}
	else
	{
		CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
		if(pWindowlessControl == NULL)
		{
			return false;
		}//if
		
		JIB(pWindowlessControl->GetCurrentImage(pByte));
	}//if

	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl == NULL)
	{
		return false;
	}//if

	JIB(pMediaControl->Stop());

	return true;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 모니터가 변경되었을때 처리를 해 준다. \n
/////////////////////////////////////////////////////////////////////////////////
void CVMRRender::DisplayChanged()
{
	if(m_nRenderType != E_WINDOWLESS)
	{
		SetAspectRatioMode(STRETCHED);
		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함
		RePaintVideo();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Video render의 type을 설정한다. \n
/// @param (int) nType : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CVMRRender::SetRenderType(int nType)
{
	if(GetUseOverlayMixer() == E_NOT_USE_OVERLAY_MIXER)
	{
		nType = E_WINDOWLESS;
	}//if

	m_nRenderType = nType;
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 수동VMR7 모드값을 얻어온다.\n
/// @return <형: E_VMR_MODE> \n
///			<E_AUTO_VMR_MODE: 자동VMR선택 모드> \n
///			<E_MANUAL_VMR7_MODE: 수동VMR7 모드> \n
///			<E_MANUAL_VMR9_MODE: 수동VMR9 모드> \n
/////////////////////////////////////////////////////////////////////////////////
CVMRRender::E_VMR_MODE CVMRRender::GetVMRMode()
{
	static E_VMR_MODE vmr_mode = E_INVALID_VMR_MODE;

	if( vmr_mode == E_INVALID_VMR_MODE )
	{
		char path[MAX_PATH] = {0};
		::GetModuleFileName(NULL, path, MAX_PATH);
		int length = strlen(path) - 1;
		while( (length > 0) && (path[length] != _T('\\')) )
			path[length--] = 0;
		strcat(path, "data\\UBCBrowser.ini");

		char buf[1024] = { 0x00 };
		GetPrivateProfileString("ROOT", "VMR_MODE", "", buf, 1024, path);

		if( stricmp(buf, "vmr7") == 0 )
			vmr_mode = E_MANUAL_VMR7_MODE;
		else if( stricmp(buf, "vmr9") == 0 )
			vmr_mode = E_MANUAL_VMR9_MODE;
		else
			vmr_mode = E_AUTO_VMR_MODE;
	}

	return vmr_mode;
}
