/************************************************************************************/
/*! @file EVRRender.cpp
@brief VMR을 이용한 동영상 재생 클래스 구현파일
@remarks
▶ 작성자: 정운형\n
▶ 작성일: 2010/01/07\n

************************************************************************************
- @b 추가 @b 및 @b 변경사항
************************************************************************************
@b 작성)
-# 2010/01/07:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"

//#include <reftime.h>
#include <math.h>
#include <io.h>

#include "dshowutil.h"
#include "EVRRender.h"


#ifndef __DEBUG__
	#define __DEBUG__(x, y)
#endif

#ifndef __WARNING__
	#define __WARNING__(x, y)
#endif

#ifndef _NULL
	#define _NULL
#endif


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (HWND) hwnd : (in) 동영상을 재생할 창의 핸들
/// @param (int) nMode : (in) 동영상을 랜더링 할 모드
/////////////////////////////////////////////////////////////////////////////////
CEVRRender::CEVRRender(CWnd* pWnd)
: CBasicInterface(pWnd)
{
	m_pWindowlessControl	= NULL;
	if(pWnd) m_hParentWnd = pWnd->m_hWnd;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CEVRRender::~CEVRRender()
{
	if(m_bOpen)
	{
		Stop();
		Close();
	}//if

	m_hParentWnd = NULL;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 오픈한다. \n
/// @param (LPCSTR) lpszFilename : (in) 재생하려는 파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::Open(LPCTSTR lpszFilename)
{
	__DEBUG_BEGIN__(lpszFilename)

	Close();
	m_strFilePath = lpszFilename;
	if(m_strFilePath.GetLength() == 0)
	{
		__WARNING__("Invlid file path (path is NULL)", _NULL);
		return false;
	}//if

	//USES_CONVERSION;
	WCHAR szFileName[MAX_PATH] = { 0x00 };
#ifdef UNICODE
	wcsncpy(szFileName, (LPCWSTR)m_strFilePath, NUMELMS(szFileName));
#else
	MultiByteToWideChar(CP_ACP, 0, (LPCSTR)m_strFilePath, -1, szFileName, MAX_PATH);
#endif

	//HRESULT hr = CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);
	HRESULT hr;
	LIF(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (LPVOID *)&m_pGraphBuilder));
	if(FAILED(hr))
	{
		__WARNING__("Fail to create FilterGraph !!!", _NULL);
		return false;
	}//if

	//Setting render
	if(!SetRenderFilter())
	{
		__WARNING__("Fail to SetRenderFilter !!!", _NULL);
		Close();
		return false;
	}//if

	if (IsWindowsMediaFile(lpszFilename))
	{
		__DEBUG__("WMV file", m_strFilePath);
		if(!SetWindowsMediaFilters(szFileName))
		{
			__WARNING__("Fail to SetWindowsMediaFilters !!!", m_strFilePath);
			JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));
			//return false;
		}//if
	}
	else
	{
		//Render file
		LIF(m_pGraphBuilder->RenderFile(szFileName, NULL));
		if(FAILED(hr))
		{
			__WARNING__("Fail to RenderFile !!!", m_strFilePath);
			m_bOpen = true; // close 시키기 위해 flag 를 true 로 바꾼다.
			Close();
			return false;
		}//if
	}

	//CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	//if(pMediaControl == NULL)
	//{
	//	__WARNING__("Fail to IMediaControl !!!", m_strFilePath);
	//	return false;
	//}//if
	//JIB(pMediaControl->StopWhenReady());

	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx != NULL)
	{
		LIF(pMediaEventEx->SetNotifyWindow((OAHWND)(m_hParentWnd), WM_DSINTERFACES_GRAPHNOTIFY, 0)); // 이벤트 통지를 처리하는 윈도우를 등록
	}//if

	m_bOpen = true;
	AddToRot(m_pGraphBuilder);

	//SetAspectRatioMode(CBasicInterface::ASPECT_RATIO_MODE::STRETCHED);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 비디오 랜더를 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::SetRenderFilter()
{
	if(!m_pGraphBuilder)
	{
		return false;
	}//if

	return SetEVR();
}

bool CEVRRender::SetEVR()
{
	HRESULT hr;
	JIB(CoCreateInstance(CLSID_EnhancedVideoRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		LIF(m_pGraphBuilder->AddFilter(m_pFilter, L"EVR"));
		if(FAILED(hr))
		{
			__WARNING__("Fail to AddFilter of EnhancedVideoRenderer !!!", m_strFilePath);
		}
		else
		{
			m_pFilter->Release();
		}
	}

	CComQIPtr<IMFGetService> pService(m_pFilter);
	if (pService == NULL) 
	{
		__WARNING__("Fail to IMFGetService !!!", m_strFilePath);
		return false;
	}

	// Set the clipping window.
	IMFVideoDisplayControl *pDisplay = NULL;
	JIB(pService->GetService(MR_VIDEO_RENDER_SERVICE, __uuidof(IMFVideoDisplayControl), (void**)&pDisplay));

	LIF(pDisplay->SetVideoWindow(m_hParentWnd));
	if (FAILED(hr))
	{
		__WARNING__("Fail to SetVideoWindow !!!", m_strFilePath);
	}
	else
	{
		m_pWindowlessControl = pDisplay;
		//m_pWindowlessControl->AddRef();
	}

	//SAFE_RELEASE(pService);
	SAFE_RELEASE(pDisplay);

	return SUCCEEDED(hr); 
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 그래프빌더의 모든 필터와 인터페이스들을 제거한다. \n
/// @return <형: BOOL> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
void CEVRRender::ReleaseAllInterfaces()
{
	if(!m_bOpen)
	{
		return;
	}//if

	HRESULT hr;
	LIF(m_pGraphBuilder->Abort());
	if(FAILED(hr))
	{
		__WARNING__("Fail to Abort !!!", m_strFilePath);
	}//if

	//그래프를 정지한다.
	Stop();

	CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
	if(pVideoWindow == NULL)
	{
		__WARNING__("Fail to IVideoWindow !!!", m_strFilePath);
	}
	else
	{
		LIF(pVideoWindow->put_Visible(OAFALSE));		//윈도우를 비표시함
		LIF(pVideoWindow->put_Owner(NULL));			//기존의 부모 윈도우를 삭제
	}//if

	//이벤트 통지를 처리하는 윈도우등록 해제
	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx==NULL)
	{
		__WARNING__("Fail to IMediaEventEx !!!", m_strFilePath);
	}
	else
	{
		LIF(pMediaEventEx->SetNotifyWindow((OAHWND)NULL, 0, 0));
	}//if

	//그래프의 필터를 열거한다.
	IEnumFilters *pEnum = NULL;
	LIF(m_pGraphBuilder->EnumFilters(&pEnum));
	if(FAILED(hr))
	{
		return;
	}//if

	LIF(pEnum->Reset());

	IBaseFilter *pFilter = NULL;
	while(S_OK == pEnum->Next(1, &pFilter, NULL))
	{
		FILTER_INFO stFilterInfo;
		LIF(pFilter->QueryFilterInfo(&stFilterInfo));
		if(SUCCEEDED(hr))
		{
			CString strFilterName;
			strFilterName = stFilterInfo.achName;
			__DEBUG__("Release Filter", strFilterName);
			if( stFilterInfo.pGraph ) SAFE_RELEASE(stFilterInfo.pGraph);
		}//if

		//필터를 삭제한다.
		LIF(m_pGraphBuilder->RemoveFilter(pFilter));
		if(FAILED(hr))
		{
		}//if

		//열거자를 리셋한다.
		LIF(pEnum->Reset());
		SAFE_RELEASE(pFilter);
	}//while
	//pEnum->Release();
	SAFE_RELEASE(pEnum);

	//SAFE_RELEASE(m_pWindowlessControl);
	//SAFE_RELEASE(m_pFilter);
	//SAFE_RELEASE(m_pGraphBuilder);

	__DEBUG__("ReleaseAllInterfaces() complete", _NULL);

	return;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상을 다시 그려준다. \n
/// @param (HDC) hdc : (in/out) 설명
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::RePaintVideo(/*HDC hdc*/)
{
	if(m_pWindowlessControl==NULL) return false;

	if(m_pParentWnd)
	{
		m_pParentWnd->Invalidate(FALSE);
	}//if

	HRESULT hr;
	JIB(m_pWindowlessControl->RepaintVideo());

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 영역을 설정한다. \n
/// @param (LPRECT) pRect : (in) 설정하려는 동영상의 영역
/// @return <형: HRESULT> \n
///			<S_OK: 성공> \n
///			<S_FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::SetVideoRect(LPRECT pRect)
{
	if(m_pWindowlessControl==NULL) return false;

	HRESULT hr;
	JIB(m_pWindowlessControl->SetVideoPosition(NULL, pRect));

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 원본영역을 반한한다. \n
/// @param (LPRECT) pRect : (out) 동영상의 원본 영역
/// @return <형: HRESULT> \n
///			<S_OK: 성공> \n
///			<S_FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::GetVideoRect(LPRECT pRect)
{
	if(m_pWindowlessControl==NULL) return false;

	HRESULT hr;

	pRect->left = 0;
	pRect->top = 0;
	SIZE size;
	LIF(m_pWindowlessControl->GetNativeVideoSize(&size, NULL));
	pRect->right = size.cx;
	pRect->bottom= size.cy;
	RECT rtSrc, rtDest;
	LIF(m_pWindowlessControl->GetVideoPosition(NULL, &rtDest));
	if(!SUCCEEDED(hr))
	{
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 재생시에 화면의 비율을 유지하는지를 설정한다. \n
/// @param (ASPECT_RATIO_MODE) mode : (in) STRETCHED, LETTER_BOX, ....
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::SetAspectRatioMode(ASPECT_RATIO_MODE mode)
{
	if(m_pWindowlessControl==NULL) return false;
/*
	enum MFVideoAspectRatioMode {
		MFVideoARMode_None	= 0,				// (stretch) Do not maintain the aspect ratio of the video. Stretch the video to fit the output rectangle.
		MFVideoARMode_PreservePicture	= 0x1,	// (letterbox) Preserve the aspect ratio of the video by letterboxing or within the output rectangle.
		MFVideoARMode_PreservePixel	= 0x2,		// Currently the EVR ignores this flag.
		MFVideoARMode_NonLinearStretch	= 0x4,	// Apply a non-linear horizontal stretch if the aspect ratio of the destination rectangle does not match the aspect ratio of the source rectangle.
		MFVideoARMode_Mask	= 0x7				// Bitmask to validate flag values. This value is not a valid flag.
	} 	MFVideoAspectRatioMode;
*/
	HRESULT hr;MFVideoARMode_None;
	JIB(m_pWindowlessControl->SetAspectRatioMode(mode));

	return true;
}
