// VMR9Interface.h: interface for the CVMR9Interface class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "BasicInterface.h"

//#include <dshow.h>
#include <D3d9.h>
#include <Vmr9.h>
#include <d3dx9tex.h>

#pragma comment( lib, "strmiids.lib" )
#pragma comment( lib, "Quartz.lib" )
#pragma comment( lib, "d3d9.lib" )
#pragma comment( lib, "d3dx9.lib" )

#define WM_MEDIA_NOTIF		WM_APP + 1

class CVMR9Interface : public CBasicInterface
{
	// Constructor / destructor
public:
	CVMR9Interface(CWnd* pWnd);
	virtual ~CVMR9Interface();

	// Methods
	virtual BOOL	ReleaseAllInterfaces();

	virtual BOOL	SetAspectRatioMode(ASPECT_RATIO_MODE mode);
	virtual HRESULT	GetVideoRect(LPRECT pRect);
	virtual HRESULT	SetVideoRect(LPRECT pRect);
	virtual	double	GetTotalPlayTime();
	virtual	double	GetCurrentPlayTime();
	virtual void	SetPosition(double pos);

	virtual BOOL	Open(LPCSTR lpszFilename) { return Open(lpszFilename, 0); };
	virtual HRESULT	Play();
	virtual HRESULT	Pause();
	virtual HRESULT	Stop();
	virtual BOOL	SeekToStart();


public:
	// Graph configuration
//	void	SetNumberOfLayer(int nNumberOfLayer);
	IBaseFilter*	AddFilter(const char* pszName, const GUID& clsid);
	BOOL	Open(LPCSTR lpszFilename, int nLayer);
	BOOL	ResetGraph();

	// Layer control
	int		GetAlphaLayer(int nLayer);
	BOOL	SetAlphaLayer(int nLayer, int nAlpha);
	DWORD	GetLayerZOrder(int nLayer);
	BOOL	SetLayerZOrder(int nLayer, DWORD dwZOrder);
	BOOL	SetLayerRect(int nLayer, RECT layerRect);

	// Bitmap control
	BOOL	SetBitmap(const char* pszBitmapFileName, int nAlpha, COLORREF cTransColor, RECT bitmapRect);
	BOOL	SetBitmapParams(int nAlpha, COLORREF cTransColor, RECT bitmapRect);
	BOOL	UpdateBitmapParams(int nAlpha, COLORREF cTransColor, RECT bitmapRect);

	// Reflected from window
	BOOL	Repaint();
	BOOL	Resize();

	// helper
	LPCTSTR	GetLastError();

	CRect	m_rect;
	void	SetRect(CRect& rect) { m_rect = rect; };

protected:
	// INIT helper methods
	void	InitDefaultValues();

	// GRAPH methods
	BOOL	BuildFilterGraph();
	BOOL	BuildVMR();
	BOOL	BuildSoundRenderer();
	BOOL	RenderGraph();

	// DIRECT3D methods
	BOOL BuildDirect3d();
	BOOL GetBitmapSize(const char* pszBitmapFileName,long& lHeight, long& lWidth);


	// LAYER helper methods
	BOOL IsValidLayer(int nLayer);
	VMR9NormalizedRect NormalizeRect(LPRECT pRect);

	// DSOW helper methods
	HRESULT AddToRot(IUnknown *pUnkGraph);
	void RemoveFromRot();
	IPin* GetPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir);
	void ReportError(const char* pszError, HRESULT hrCode);
	HRESULT GetNextFilter(IBaseFilter *pFilter, PIN_DIRECTION Dir, IBaseFilter **ppNext);
	BOOL RemoveFilterChain(IBaseFilter* pFilter, IBaseFilter* pStopFilter);
	HRESULT AddFilterByClsid(IGraphBuilder *pGraph, LPCWSTR wszName, const GUID& clsid, IBaseFilter **ppF);

	// Attributes
protected:
//	HWND						m_hParentWnd;

//	IGraphBuilder*				m_pGraphBuilder;
//	IMediaControl*				m_pMC;
//	IMediaEvent*				m_pMediaEvent;
//	IMediaEventEx*				m_pMediaEventEx;

//	IMediaPosition*				m_pMP;
//	IMediaSeeking*				m_pMS;
//	IBasicAudio*				m_pBA;



	DWORD						m_dwRotId;
	char						m_pszErrorDescription[1024+MAX_ERROR_TEXT_LEN];
	int							m_nNumberOfStream;
	// MEDIA WINDOW
	// SRC interfaces array
	IBaseFilter*				m_srcFilterArray[10];
	// SOUND interfaces
	IBaseFilter*				m_pDirectSoundFilter;
	// GRAPH interfaces
	IUnknown*					m_pGraphUnknown;
	IFilterGraph*				m_pFilterGraph;
	IFilterGraph2*				m_pFilterGraph2;
	// VMR9 interfaces
	IBaseFilter*				m_pVMRBaseFilter;
	IVMRFilterConfig9*			m_pVMRFilterConfig;
	IVMRMixerBitmap9*			m_pVMRMixerBitmap;
	IVMRMixerControl9*			m_pVMRMixerControl;
	IVMRMonitorConfig9*			m_pVMRMonitorConfig;
	IVMRWindowlessControl9*		m_pVMRWindowlessControl;
	// DIRECT3D interfaces
	IDirect3D9*					m_pD3DDirect3d;
	IDirect3DDevice9*			m_pD3DDevice;
	IDirect3DSurface9*			m_pD3DSurface;

};


