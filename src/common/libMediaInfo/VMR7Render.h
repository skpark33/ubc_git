/************************************************************************************/
/*! @file VMR7Render.h
	@brief VMR7을 이용한 동영상 재생 클래스 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/01/06\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 정운형)
	-# <2010/01/06:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#include <dshow.h>
//#include <D3d9.h>
//#include <Vmr9.h>
//#include <d3dx9tex.h>

#pragma comment( lib, "strmiids.lib" )
#pragma comment( lib, "Quartz.lib" )
//#pragma comment( lib, "d3d9.lib" )
//#pragma comment( lib, "d3dx9.lib" )

#define SAFE_RELEASE(i) {if (i) i->Release(); i = NULL;}
#define JIF( x ) if( FAILED(hr = ( x )) ) { return hr; }


//! VMR7을 이용한 동영상 재생 클래스
/*!
*/
class CVMR7Render
{
private:
	// Encapsulate VMR7 functionality in this class
    IGraphBuilder				*m_pGrapgBuilder;		///<그래프빌더 인터페이스
    IMediaControl				*m_pMediaControl;		///<미디어 컨트롤 인터페이스
    IMediaSeeking				*m_pMediaSeeking;		///<미디어 seeking 인터페이스
    IVMRWindowlessControl		*m_WindowlessControl;	///<VMR windowless 컨트롤 인터페이스
	IBasicAudio					*m_pAudio;				///<Basic audio

	DWORD						m_dwRotId;				///<Rot 테이블에 등록되는 아이디
	HWND						m_hWnd;					///<동영상이 그려질 창의 윈도우 핸들
	RECT						m_rcWnd;				///<동영상이 그려질 창의 영역
	CString						m_strFile;				///<동영상 파일의 경로
	CString						m_strError;				///<에러 메시지

public: 
	CVMR7Render(HWND hwnd);								///<생성자
	virtual ~CVMR7Render(void);							///<소멸자

	bool			Open(CString strFile);							///<동영상 파일을 오픈한다.
	bool			Play(void);										///<동영상 파일을 재생한다.
	bool			Pause(void);									///<동영상 파일의 재생을 일시 중지 한다.
	bool			Stop(void);										///<동영상 파일의 재생을 중지 한다.
	bool			Close(void);									///<동영상 파일을 닫는다.
	bool			SetVolume(int nVolume);							///<동영상의 볼륨을 설정한다.

	double			GetTotalPlayTime(void);							///<동영상 파일의 총 재생시간을 반환한다.
	double			GetCurrentPlayTime(void);						///<동영상 파일의 현재 재생시간을 반환한다.
	bool            SeekToPosition(REFTIME rt, BOOL bFlushData);	///<동영상의 지정된 위치로 이동한다.
	bool			SeekToStart(void);								///<동영상의 처음 위치로 이동한다.

	bool			GetVideoRect(LPRECT pRect);						///<동영상의 영역을 구한다.
	bool			SetVideoRect(LPRECT pRect);						///<동영상의 영역을 설정한다.

	bool			SetAspectRatioMode(int nMode);					///<동영상의 화면 비율 재생 모드를 설정한다.
	bool			RePaintVideo(HDC hdc);							///<동영상을 다시 그려준다.

	CString			GetErrorMsg(void);								///<Error 메시지를 반환

private:
	HRESULT			AddToRot(IUnknown *pUnkGraph);					///<Running time object 테이블에 등록을 한다.
	void			RemoveFromRot(void);							///<Running time object 테이블에서 제거한다.
};