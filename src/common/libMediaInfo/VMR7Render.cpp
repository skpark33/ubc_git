/************************************************************************************/
/*! @file VMR7Render.cpp
	@brief VMR7을 이용한 동영상 재생 클래스 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/01/07\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2010/01/07:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"
#include "VMR7Render.h"
#include <reftime.h>


//static HRESULT SetRenderingMode(IBaseFilter* pBaseFilter, VMRMode mode)
//{
//    // Test VMRConfig, VMRMonitorConfig
//    IVMRFilterConfig* pConfig;
//
//    HRESULT hr = pBaseFilter->QueryInterface(IID_IVMRFilterConfig, (LPVOID *)&pConfig);
//    if(SUCCEEDED(hr))
//    {
//        hr = pConfig->SetRenderingMode(mode);
//        hr = pConfig->SetRenderingPrefs(RenderPrefs_ForceOverlays|RenderPrefs_AllowOverlays);
//        pConfig->Release();
//    }//if
//    return hr;
//}//if


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (HWND) hwnd : (in) 동영상을 재생할 창의 핸들
/////////////////////////////////////////////////////////////////////////////////
CVMR7Render::CVMR7Render(HWND hwnd)
: m_pGrapgBuilder(NULL)
 ,m_pMediaControl(NULL)
 ,m_pMediaSeeking(NULL)
 ,m_WindowlessControl(NULL)
 ,m_pAudio(NULL)
 ,m_dwRotId(-1)
 ,m_hWnd(hwnd)
 ,m_strFile("")
 ,m_strError("")
{
	::GetClientRect(m_hWnd, &m_rcWnd);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CVMR7Render::~CVMR7Render()
{
	Stop();
	Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 오픈한다. \n
/// @param (CString) strFile : (in) 재생하려는 파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::Open(CString strFile)
{
	m_strError = "";

	if(strFile.GetLength() == 0)
	{
		m_strError.Format("Invlid file path (%s)", strFile);
		return false;
	}//if

	m_strFile = strFile;
	if(_access(strFile, 00) != 0)
	{
		m_strError.Format("Can't access media file (%s)", m_strFile);
		return false;
	}//if

	USES_CONVERSION;
	HRESULT         hres;
	WCHAR           FileName[MAX_PATH];

	wcsncpy(FileName, T2W(strFile), NUMELMS(FileName));

	hres = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (LPVOID *)&m_pGrapgBuilder);
	if(FAILED(hres))
	{
		m_strError.Format("Fail to get IID_IGraphBuilder");
		return false;
	}//if

	//VMR
	IBaseFilter *pVmr = NULL;
	hres = CoCreateInstance(CLSID_VideoMixingRenderer, NULL, CLSCTX_INPROC, IID_IBaseFilter, (LPVOID *)&pVmr);
	if(FAILED(hres))
	{
		m_strError.Format("Fail to get VideoMixingRenderer");
		SAFE_RELEASE(m_pGrapgBuilder);
		return false;
	}//if

	//Add to filter graph
	hres = m_pGrapgBuilder->AddFilter(pVmr, L"Video Mixing Renderer");
	if(FAILED(hres))
	{
		m_strError.Format("Fail to add filter to filter graph");
		SAFE_RELEASE(pVmr);
		SAFE_RELEASE(m_pGrapgBuilder);
		return false;
	}//if

	//Set rendering mode
	IVMRFilterConfig *pConfig = NULL;
	hres = pVmr->QueryInterface(IID_IVMRFilterConfig, (LPVOID *)&pConfig);
	if(!SUCCEEDED(hres))
	{
		m_strError.Format("Fail to get IID_IVMRFilterConfig");
		SAFE_RELEASE(pVmr);
		SAFE_RELEASE(m_pGrapgBuilder);
		return false;
	}//if

	hres = pConfig->SetRenderingMode(VMRMode_Windowless);
	if(!SUCCEEDED(hres))
	{
		m_strError.Format("Fail to SetRenderingMode");
		SAFE_RELEASE(pConfig);
		SAFE_RELEASE(pVmr);
		SAFE_RELEASE(m_pGrapgBuilder);
		return false;
	}//if
	pConfig->Release();

	//Set window
	hres = pVmr->QueryInterface(IID_IVMRWindowlessControl, (LPVOID *)&m_WindowlessControl);
	if(!SUCCEEDED(hres))
	{
		m_strError.Format("Fail to get IID_IVMRWindowlessControl");
		SAFE_RELEASE(pConfig);
		SAFE_RELEASE(pVmr);
		SAFE_RELEASE(m_pGrapgBuilder);
		return false;
	}//if

	if(!SetAspectRatioMode(VMR_ARMODE_NONE))
	{
		m_strError.Format("Fail to SetAspectRatioMode");
	}//if

	hres = m_WindowlessControl->SetVideoClippingWindow(m_hWnd);
	if(!SUCCEEDED(hres))
	{
		m_strError.Format("Fail to set SetVideoClippingWindow");
		SAFE_RELEASE(m_WindowlessControl);
		SAFE_RELEASE(pConfig);
		SAFE_RELEASE(pVmr);
		SAFE_RELEASE(m_pGrapgBuilder);
		return false;
	}//if

	hres = m_pGrapgBuilder->RenderFile(FileName, NULL);
	if(FAILED(hres))
	{
		SAFE_RELEASE(m_WindowlessControl);
		SAFE_RELEASE(pConfig);
		SAFE_RELEASE(pVmr);
		SAFE_RELEASE(m_pGrapgBuilder);
		m_strError.Format("Fail to render file(%s)", m_strFile);
		return false;
	}//if

	hres = m_pGrapgBuilder->QueryInterface(IID_IMediaControl, (LPVOID *)&m_pMediaControl);
	if(FAILED(hres))
	{
		SAFE_RELEASE(m_WindowlessControl);
		SAFE_RELEASE(pConfig);
		SAFE_RELEASE(pVmr);
		SAFE_RELEASE(m_pGrapgBuilder);
		m_strError.Format("Fail to get IID_IMediaControl");
		return false;
	}//if

	hres = m_pGrapgBuilder->QueryInterface(IID_IMediaSeeking, (LPVOID *)&m_pMediaSeeking);
	if(FAILED(hres))
	{
		SAFE_RELEASE(m_pMediaControl);
		SAFE_RELEASE(m_WindowlessControl);
		SAFE_RELEASE(pConfig);
		SAFE_RELEASE(pVmr);
		SAFE_RELEASE(m_pGrapgBuilder);
		m_strError.Format("Fail to get IID_IMediaSeeking");
		return false;
	}//if

	hres = m_pGrapgBuilder->QueryInterface(IID_IBasicAudio, (LPVOID*)&m_pAudio);
	if(FAILED(hres))
	{
		SAFE_RELEASE(m_pMediaSeeking);
		SAFE_RELEASE(m_pMediaControl);
		SAFE_RELEASE(m_WindowlessControl);
		SAFE_RELEASE(pConfig);
		SAFE_RELEASE(pVmr);
		SAFE_RELEASE(m_pGrapgBuilder);
		m_strError.Format("Fail to get IID_IBasicAudio");
		return false;
	}//if

	pVmr->Release();
	AddToRot(m_pGrapgBuilder);

	SetVideoRect(&m_rcWnd);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 닫는다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::Close()
{
	//RemoveFromRot();
	
	SAFE_RELEASE(m_pAudio);
	SAFE_RELEASE(m_pMediaControl);	
	SAFE_RELEASE(m_pMediaSeeking);	
	SAFE_RELEASE(m_WindowlessControl);
	SAFE_RELEASE(m_pGrapgBuilder);

	RemoveFromRot();

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 영역을 구한다. \n
/// @param (LPRECT) pRect : (out) 설정하려는 동영상의 영역
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::GetVideoRect(LPRECT pRect)
{
	m_strError = "";

	if(m_WindowlessControl)
    {
        RECT src={0};
        HRESULT hr = m_WindowlessControl->GetVideoPosition(&src, pRect);
		if(FAILED(hr))
		{
			m_strError = "Fail to GetVideoPosition";
			return false;
		}//if

		return true;
    }//if

	m_strError = "IVMRWindowlessControl is NULL";
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 영역을 설정한다. \n
/// @param (LPRECT) pRect : (in) 설정된 동영상의 역역
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::SetVideoRect(LPRECT pRect)
{
	m_strError = "";

	if(m_WindowlessControl)
    {
        HRESULT hr = m_WindowlessControl->SetVideoPosition(NULL, pRect);
		if(FAILED(hr))
		{
			m_strError = "Fail to SetVideoPosition";
			return false;
		}//if

		return true;
    }//if

	m_strError = "IVMRWindowlessControl is NULL";
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 재생한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::Play()
{
	m_strError = "";

	if(m_pMediaControl == NULL)
	{
		m_strError = "IMediaControl is NULL";
		return false;
	}//if

	HRESULT hr=S_OK;

	SeekToPosition((REFTIME)0, FALSE);

	hr = m_pMediaControl->Run();
	if(SUCCEEDED(hr))
	{
		return true;
	}//if

	m_strError = "Fail to IMediaControl Run";
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 재생을 일시 중지 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::Pause()
{
	m_strError = "";

	if(m_pMediaControl == NULL)
	{
		m_strError = "IMediaControl is NULL";
		return false;
	}//if

	HRESULT hr = m_pMediaControl->Pause();
	if(SUCCEEDED(hr))
	{
		return true;
	}//if

	m_strError = "Fail to IMediaControl Pause";
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 재생을 중지 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::Stop()
{
	m_strError = "";

	if(m_pMediaControl == NULL)
	{
		m_strError = "IMediaControl is NULL";
		return false;
	}//if

	HRESULT hr = m_pMediaControl->Stop();
	if(SUCCEEDED(hr))
	{
		return true;
	}//if

	m_strError = "Fail to IMediaControl Stop";
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 볼륨을 설정한다. \n
/// @param (int) nVolume : (in) 0~100 까지의 볼륨 값
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::SetVolume(int nVolume)
{
	m_strError = "";
	if(m_pAudio)
	{
		// 볼륨조절
		double lf_cur_sound = ((1 - pow(10.0f, -10)) * ((double)nVolume / 100)) + pow(10.0f, -10);// 핵심코드
		lf_cur_sound = 10 * log10(lf_cur_sound) * 100;
		long l_cur_sound = (long)lf_cur_sound;

		HRESULT hr =  m_pAudio->put_Volume(l_cur_sound);
		if(SUCCEEDED(hr))
		{
			return true;
		}
		else
		{
			m_strError.Format("Faile to set volume(%d) %s", nVolume, m_strFile);
			return false;
		}//if
	}//if

	m_strError.Format("Basic audio interface is NULL");
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 총 재생시간을 반환한다. \n
/// @return <형: double> \n
///			<double: 동영상 파일의 총 재생시간> \n
/////////////////////////////////////////////////////////////////////////////////
double CVMR7Render::GetTotalPlayTime()
{
	REFTIME tm = 0.0f;
	HRESULT hr;
    LONGLONG lDuration;

    if(m_pMediaSeeking != NULL)
    {
        hr = m_pMediaSeeking->GetDuration(&lDuration);
        if(SUCCEEDED(hr))
        {
            return double(lDuration) / UNITS;
        }//if
    }//if

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 현재 재생시간을 반환한다. \n
/// @return <형: double> \n
///			<double: 동영상 파일의 현재 재생시간> \n
/////////////////////////////////////////////////////////////////////////////////
double CVMR7Render::GetCurrentPlayTime()
{
	REFTIME rt = (REFTIME) 0;
	HRESULT hr;
	LONGLONG lPosition;

	if(m_pMediaSeeking != NULL)
	{
		hr = m_pMediaSeeking->GetPositions(&lPosition, NULL);
		if(SUCCEEDED(hr))
		{
			return double(lPosition) / UNITS;
		}//if
	}//if

	return rt;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 지정된 위치로 이동한다. \n
/// @param (REFTIME) rt : (in) 이동하려는 동영상의 시간
/// @param (BOOL) bFlushData : (in) 이동된 위치 화면의 갱신여부
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::SeekToPosition(REFTIME rt, BOOL bFlushData)
{
	HRESULT hr;
    LONGLONG llTime = LONGLONG(rt * double(UNITS));

    if(m_pMediaControl != NULL && m_pMediaControl != NULL)
    {
        FILTER_STATE fs;
        hr = m_pMediaControl->GetState(100, (OAFilterState *)&fs);
        hr = m_pMediaSeeking->SetPositions(&llTime, AM_SEEKING_AbsolutePositioning, NULL, 0);

        // This gets new data through to the renderers
        if(fs == State_Stopped && bFlushData)
        {
            hr = m_pMediaControl->Pause();
            hr = m_pMediaControl->GetState(INFINITE, (OAFilterState *)&fs);
            hr = m_pMediaControl->Stop();
        }//if

        if(SUCCEEDED(hr))
        {
            return true;
        }//if
    }//if

    return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 처음 위치로 이동한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::SeekToStart()
{
	return SeekToPosition(0, FALSE);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 화면 비율 재생 모드를 설정한다. \n
/// @param (int) nMode : (in/out) 설명
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::SetAspectRatioMode(int nMode)
{
	m_strError = "";

	if(m_WindowlessControl)
	{

		HRESULT hr = m_WindowlessControl->SetAspectRatioMode(VMR_ASPECT_RATIO_MODE(nMode));
		if(SUCCEEDED(hr))
		{
			return true;
		}
		else
		{
			m_strError = "Fail to SetAspectRatioMode";
			return false;
		}//if
	}//if

	m_strError = "IVMRWindowlessControl is NULL";
	return false;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상을 다시 그려준다. \n
/// @param (HDC) hdc : (in/out) 설명
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMR7Render::RePaintVideo(HDC hdc)
{
	m_strError = "";

	if(m_WindowlessControl)
	{
		HRESULT hr = m_WindowlessControl->RepaintVideo(m_hWnd, hdc);
		if(SUCCEEDED(hr))
		{
			return true;
		}
		else
		{
			m_strError = "Fail to RepaintVideo";
			return false;
		}//if
	}//if

	m_strError = "IVMRWindowlessControl is NULL";
	return false;
}


// Function name	: CVMR7Graph::AddToRot
// Description	    : let the graph instance be accessible from graphedit
// Return type		: HRESULT 
// Argument         : IUnknown *pUnkGraph
// Argument         : DWORD *pdwRegister
HRESULT CVMR7Render::AddToRot(IUnknown *pUnkGraph) 
{
	if(pUnkGraph == NULL)
	{
		return E_INVALIDARG;
	}//if

    IMoniker * pMoniker;
    IRunningObjectTable *pROT;
    if(FAILED(GetRunningObjectTable(0, &pROT)))
	{
        return E_FAIL;
    }//if

    WCHAR wsz[256];
    wsprintfW(wsz, L"FilterGraph %08x pid %08x", (DWORD_PTR)pUnkGraph, GetCurrentProcessId());
    HRESULT hr = CreateItemMoniker(L"!", wsz, &pMoniker);
    if(SUCCEEDED(hr))
	{
        hr = pROT->Register(0, pUnkGraph, pMoniker, &m_dwRotId);
        pMoniker->Release();
    }//if
    pROT->Release();

    return hr;
}


// Function name	: CVMR7Graph::RemoveFromRot
// Description	    : remove the graph instance accessibility from graphedit
// Return type		: void 
void CVMR7Render::RemoveFromRot()
{
	if(m_dwRotId != -1)
	{
		IRunningObjectTable *pROT;
		if(SUCCEEDED(GetRunningObjectTable(0, &pROT)))
		{
			pROT->Revoke(m_dwRotId);
			m_dwRotId = -1;
			pROT->Release();
		}//if
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Error 메시지를 반환 \n
/// @return <형: CString> \n
///			<Error 메시지> \n
/////////////////////////////////////////////////////////////////////////////////
CString CVMR7Render::GetErrorMsg()
{
	return m_strError;
}