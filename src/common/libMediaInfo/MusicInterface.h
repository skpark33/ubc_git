// MusicInterface.h: interface for the CMusicInterface class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "BasicInterface.h"


//! Music 재생전용 클래스
/*!
*/
class CMusicInterface : public CBasicInterface
{
public:
	CMusicInterface(CWnd* pWnd);								///<생성자
	virtual ~CMusicInterface(void);							///<소멸자

	// Methods
	virtual bool	ChangeParentWindow(HWND hParentWnd) { return true; };
	virtual void	ReleaseAllInterfaces(void);					///<사용한 모든 필터를 해제한다.

	virtual bool	SetAspectRatioMode(ASPECT_RATIO_MODE mode) { return true; };
	virtual bool	GetVideoRect(LPRECT pRect) { return true; };
	virtual bool	SetVideoRect(LPRECT pRect) { return true; };

	virtual bool	Open(LPCSTR lpszFilename);					///<동영상 파일을 open한다.
};

