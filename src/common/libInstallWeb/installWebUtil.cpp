// ServerCheck.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "installWebUtil.h"
#include "resource.h"

#include <afxinet.h>

#include <ci/libDebug/ciDebug.h>

#include <common/libCommon/ubcIni.h>
#include <common/libScratch/scratchUtil.h>
#include <common/libHttpRequest/HttpRequest.h>


ciSET_DEBUG(10, "installWebUtil");


// common/libInstall/installUtil.h 파일로부터 복사
#define AUTH_SUCCEED_BUT_REBOOTING	2	// 신규추가
#define AUTH_SUCCEED				1
#define AUTH_AUTHENTICATION_FAIL	0
#define AUTH_GET_MAC_FAIL			-1
#define AUTH_COMMUNICATION_FAIL		-2
#define AUTH_WRITE_FILE_FAIL		-4
#define AUTH_GET_IP_FAIL			-8	// 신규추가


#define		AUTH_BY_IP_ASP			"authorize_host_by_ip.asp"
#define		AUTH_BY_MAC_ASP			"authorize_host_by_mac.asp"
#define		AUTH_BY_NAME_ASP		"authorize_host_by_global.asp"

// CServerCheck

installWebUtil* installWebUtil::_instance = NULL; 
//CString installWebUtil::_logFilename = "log\\installWebUtil.log";

installWebUtil* installWebUtil::getInstance()
{
	if(!_instance)
	{
		_instance = new installWebUtil;
	}
	return _instance;
}

void installWebUtil::clearInstance()
{
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
}

installWebUtil::installWebUtil()
{
	//
	m_bOnceTimeForceAuth = false;
}

installWebUtil::~installWebUtil()
{
}

LPCSTR installWebUtil::GetEditionString(LPCSTR edition)
{
	if(strcmp(edition, PLUS_EDITION) == 0) return "UBC STANDARD PLUS";
	if(strcmp(edition, ENTERPRISE_EDITION) == 0) return "UBC ENTERPRISE";
	return "UBC STANDARD";
}

int	installWebUtil::auth(LPCSTR siteId, LPCSTR hostId, LPCSTR edition,
		string& errString, string& mac, bool reset)
{
	ciDEBUG(10, ("auth() begin...") );

	ciDEBUG(10, ("Site=%s", siteId) );
	ciDEBUG(10, ("Host=%s", hostId) );
	ciDEBUG(10, ("Edition=%s", edition) );

	int ret_value = AUTH_SUCCEED;

	scratchUtil* sUtil = scratchUtil::getInstance();

	if(mac.empty())
	{
		string real_mac = sUtil->GetMacaddr();
		if(real_mac.empty())
		{
			CString msg;
			msg.LoadString(IDS_FAIL_GET_MAC_ADDR);

			ciERROR( (msg) );

			errString = (LPCSTR)msg;
			return AUTH_GET_MAC_FAIL;
		}
		if(!real_mac.empty())
		{
			mac = real_mac;
		}
	}
	ciDEBUG(10, ("MacAddr=%s", mac.c_str()) );

	//
	int err_code = 0;
	string new_edition = "";
	CString response = "";

	//
	if(m_bOnceTimeForceAuth)
	{
		ciDEBUG(10, ("Admin Force Auth") );

		err_code = 999;
	}
	else
	{
		CString send_msg;
		send_msg.Format("siteId=%s&hostId=%s&macAddr=%s&reset=%d", siteId, hostId, mac.c_str(), reset);

		ciDEBUG(10, ("SendMsg=%s", send_msg) );

		CHttpRequest http_request;
		http_request.Init(CHttpRequest::CONNECTION_CENTER);

		CStringArray response_list;
		BOOL ret_val = http_request.RequestPost("/UBC_Registration/authorize_host.asp", send_msg, response_list);

		if( !ret_val || response_list.GetSize() > 1)
			return AUTH_COMMUNICATION_FAIL;

		response = response_list.GetAt(0);
		if( response.GetAt(0) == '<' || response.Find("|") < 0)
		{
			CString msg;
			msg.LoadString(IDS_FAIL_CONNECT_TO_SERVER);
			errString = (LPCSTR)msg;

			msg += "=%s";
			ciERROR( (msg, (LPCTSTR)response) );

			return AUTH_COMMUNICATION_FAIL;
		}

		ciDEBUG(10, ("Response=%s", (LPCTSTR)response) );
	}

	//
	if(response.Find('|') > 0)
	{
		int pos = 0;
		err_code = atoi(response.Tokenize("|", pos));
		new_edition = response.Tokenize("|", pos);
	}

	if( err_code == 0 )
	{
		CString msg;
		msg.LoadString(IDS_FAIL_CONNECT_TO_SERVER);
		errString = (LPCSTR)msg;
		ciERROR( (errString.c_str()) );
		return AUTH_COMMUNICATION_FAIL;
	}

	switch(err_code)
	{
	case 1:	// auth success
		{
			CString msg;
			msg.LoadString(IDS_SUCCESS_AUTHENTICATION);
			errString = (LPCSTR)msg;
			//return AUTH_SUCCEED;
		}
		break;
	case 2:	// already auth
		{
			CString msg;
			msg.LoadString(IDS_SUCCESS_ALREADY_AUTHENTICATION);
			errString = (LPCSTR)msg;
			//return AUTH_SUCCEED;
		}
		break;
	case 3:	//  already auth another host
		{
			CString msg;
			msg.LoadString(IDS_FAIL_ALREADY_AUTHENTICATION_BY_ANOTHER_HOST);
			errString = (LPCSTR)msg;
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_AUTHENTICATION_FAIL;
		}
		break;
	case 4:	// reset auth success
		{
			CString msg;
			msg.LoadString(IDS_SUCCESS_RESET_AUTHENTICATION);
			errString = (LPCSTR)msg;
			//return AUTH_SUCCEED;
		}
		break;
	case 5:	// mac-addr null
		{
			CString msg;
			msg.LoadString(IDS_FAIL_INVALID_MAC_ADDRESS);
			errString = (LPCSTR)msg;
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_AUTHENTICATION_FAIL;
		}
		break;
	case 6:	// not exist host
		{
			CString msg;
			msg.LoadString(IDS_FAIL_NOT_EXIST_HOST);
			errString = (LPCSTR)msg;
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_AUTHENTICATION_FAIL;
		}
		break;
	case 7: // not match mac-address (in reset)
		{
			CString msg;
			msg.LoadString(IDS_FAIL_RESET_AUTHENTICATION);
			errString = (LPCSTR)msg;
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_AUTHENTICATION_FAIL;
		}
		break;
	case 999: // admin force auth
		{
			m_bOnceTimeForceAuth = false;

			CString msg;
			msg.LoadString(IDS_SUCCESS_AUTHENTICATION_BY_FORCE);
			errString = (LPCSTR)msg;
			//return AUTH_SUCCEED;
		}
		break;
	default: // unknown error
		{
			CString msg;
			msg.LoadString(IDS_UNKNOWN_ERROR);
			errString = (LPCSTR)msg;
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_AUTHENTICATION_FAIL;
		}
		break;
	}

	CString notice = "";
	if( new_edition.empty() )
	{
		new_edition = edition;
	}
	if( new_edition != edition )
	{
		CString format;
		format.LoadString(IDS_INFO_EDITION_CHANGED);

		notice.Format(format, GetEditionString(edition), GetEditionString(new_edition.c_str()));

		ret_value = AUTH_SUCCEED_BUT_REBOOTING;
	}

	if( !sUtil->writeAuthFile(hostId, mac.c_str(), new_edition.c_str()) )
	{
		CString msg;
		msg.LoadString(IDS_FAIL_WRITE_AUTHENTICATION);
		errString = (LPCSTR)msg;
		ciDEBUG(10, (errString.c_str()) );
		return AUTH_WRITE_FILE_FAIL;
	}
	errString += notice;
	ciDEBUG(10, (errString.c_str()) );
	return ret_value;
}

int installWebUtil::_authBy(LPCSTR lpszModuleName, string& siteId, string& hostId, LPCSTR edition, string& errString, string& ip, string& mac, bool reset)
{
	ciDEBUG(10, ("_authBy() begin...") );

	int ret_value = AUTH_SUCCEED;

	scratchUtil* sUtil = scratchUtil::getInstance();

	//
	if(ip.empty())
	{
		string real_ip;
		sUtil->getIpAddress(real_ip);
		if(real_ip.empty())
		{
			CString msg;
			msg.LoadString(IDS_FAIL_GET_IP_ADDR);

			ciERROR( (msg) );

			errString = (LPCSTR)msg;
			return AUTH_GET_IP_FAIL;
		}
		if(!real_ip.empty())
		{
			ip = real_ip;
		}
	}
	ciDEBUG(10, ("IP=%s", ip.c_str()) );

	//
	if(mac.empty())
	{
		string real_mac = sUtil->GetMacaddr();
		if(real_mac.empty())
		{
			CString msg;
			msg.LoadString(IDS_FAIL_GET_MAC_ADDR);

			ciERROR( (msg) );

			errString = (LPCSTR)msg;
			return AUTH_GET_MAC_FAIL;
		}
		if(!real_mac.empty())
		{
			mac = real_mac;
		}
	}
	ciDEBUG(10, ("MacAddr=%s", mac.c_str()) );

	//
	int err_code = 0;
	string new_edition = "";
	CString response = "";

	//
	if(m_bOnceTimeForceAuth)
	{
		ciDEBUG(10, ("Admin Force Auth") );

		err_code = 999;
	}
	else
	{
		CString url;
		url.Format("/UBC_Registration/%s", lpszModuleName);

		CString send_msg;
		send_msg.Format("ipAddr=%s&macAddr=%s&reset=%d&hostName=%s&hostType=%s", ip.c_str(), mac.c_str(), reset, siteId.c_str(), hostId.c_str() );

		ciDEBUG(10, ("POST=%s", url) );
		ciDEBUG(10, ("SendMsg=%s", send_msg) );

		CHttpRequest http_request;
		http_request.Init(CHttpRequest::CONNECTION_CENTER);

		BOOL ret_val = http_request.RequestPost(url, send_msg, response);

		if( !ret_val || response.GetAt(0) == '<' || response.Find("|") < 0)
		{
			CString msg;
			msg.LoadString(IDS_FAIL_CONNECT_TO_SERVER);
			errString = (LPCSTR)msg;

			msg += "=%s";
			ciERROR( (msg, (LPCTSTR)response) );

			return AUTH_COMMUNICATION_FAIL;
		}
		ciDEBUG(10, ("Response=%s", (LPCTSTR)response) );
	}

	//
	if(response.Find('|') > 0)
	{
		int pos = 0;
		err_code = atoi(response.Tokenize("|", pos));
		hostId = response.Tokenize("|", pos);
		if(hostId != "")
		{
			//
			int pos2 = 0;
			CString host_id = hostId.c_str();
			siteId = host_id.Tokenize("-", pos2);

			//
			new_edition = response.Tokenize("|", pos);
		}
	}

	if( err_code == 0 )
	{
		CString msg;
		msg.LoadString(IDS_FAIL_CONNECT_TO_SERVER);
		errString = (LPCSTR)msg;
		ciERROR( (errString.c_str()) );
		return AUTH_COMMUNICATION_FAIL;
	}

	switch(err_code)
	{
	case 1:	// auth success
		{
			CString msg;
			msg.LoadString(IDS_SUCCESS_AUTHENTICATION);
			errString = (LPCSTR)msg;
			//return AUTH_SUCCEED;
		}
		break;
	case 2:	// already auth
		{
			CString msg;
			msg.LoadString(IDS_SUCCESS_ALREADY_AUTHENTICATION);
			errString = (LPCSTR)msg;
			//return AUTH_SUCCEED;
		}
		break;
	case 3:	//  already auth another host
		{
			CString msg;
			msg.LoadString(IDS_FAIL_ALREADY_AUTHENTICATION_BY_ANOTHER_HOST);
			errString = (LPCSTR)msg;
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_AUTHENTICATION_FAIL;
		}
		break;
	case 4:	// reset auth success
		{
			CString msg;
			msg.LoadString(IDS_SUCCESS_RESET_AUTHENTICATION);
			errString = (LPCSTR)msg;
			//return AUTH_SUCCEED;
		}
		break;
	case 5:	// mac-addr null
		{
			CString msg;
			msg.LoadString(IDS_FAIL_INVALID_MAC_ADDRESS);
			errString = (LPCSTR)msg;
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_AUTHENTICATION_FAIL;
		}
		break;
	case 6:	// not exist host
		{
			CString msg;
			msg.LoadString(IDS_FAIL_NOT_EXIST_HOST);
			errString = (LPCSTR)msg;
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_AUTHENTICATION_FAIL;
		}
		break;
	case 999: // admin force auth
		{
			m_bOnceTimeForceAuth = false;

			CString msg;
			msg.LoadString(IDS_SUCCESS_AUTHENTICATION_BY_FORCE);
			errString = (LPCSTR)msg;
			//return AUTH_SUCCEED;
		}
		break;
	default: // unknown error
		{
			CString msg;
			msg.LoadString(IDS_UNKNOWN_ERROR);
			errString = (LPCSTR)msg;
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_AUTHENTICATION_FAIL;
		}
		break;
	}

	CString notice = "";
	if( new_edition.empty() )
	{
		new_edition = edition;
	}
	if( new_edition != edition )
	{
		CString format;
		format.LoadString(IDS_INFO_EDITION_CHANGED);

		notice.Format(format, GetEditionString(edition), GetEditionString(new_edition.c_str()));

		ret_value = AUTH_SUCCEED_BUT_REBOOTING;
	}

	if( !sUtil->writeAuthFile(hostId.c_str(), mac.c_str(), new_edition.c_str()) )
	{
		CString msg;
		msg.LoadString(IDS_FAIL_WRITE_AUTHENTICATION);
		errString = (LPCSTR)msg;
		ciDEBUG(10, (errString.c_str()) );
		return AUTH_WRITE_FILE_FAIL;
	}
	errString += notice;
	ciDEBUG(10, (errString.c_str()) );
	return ret_value;
}

int installWebUtil::authByIP(string& siteId, string& hostId, LPCSTR edition, string& errString, string& ip, string& mac, bool reset)
{
	return _authBy(AUTH_BY_IP_ASP, siteId, hostId, edition, errString, ip, mac, reset);
}

int installWebUtil::authByMAC(string& siteId, string& hostId, LPCSTR edition, string& errString, string& ip, string& mac, bool reset)
{
	return _authBy(AUTH_BY_MAC_ASP, siteId, hostId, edition, errString, ip, mac, reset);
}

int installWebUtil::authByNAME(string& siteId, string& hostId, LPCSTR edition, string& errString, string& ip, string& mac, bool reset)
{
	return _authBy(AUTH_BY_NAME_ASP, siteId, hostId, edition, errString, ip, mac, reset);
}

int installWebUtil::authByIP()
{
	ciDEBUG(10, ("authByIP() begin...") );

	ciString siteId="", hostId="", edition="", errString="", ip="", mac="";

	if(scratchUtil::getInstance()->readAuthFile(hostId,mac,edition))
	{
		ciDEBUG(10, ("Host=%s", hostId.c_str()) );
		ciDEBUG(10, ("MacAddr=%s", mac.c_str()) );
		ciDEBUG(10, ("Edition=%s", edition.c_str()) );

		if(edition == ENTERPRISE_EDITION)
		{
			errString = "Already authorized";
			ciDEBUG(10, (errString.c_str()) );
			return AUTH_SUCCEED;
		}
	}

	CString com_name="";
	{
		DWORD size = 1024;
		char name[1024] = {0};
		BOOL ret = ::GetComputerNameEx(ComputerNamePhysicalDnsHostname, name, &size);
		com_name = name;
	}
	ciDEBUG(10, ("GetComputerName=%s", (LPCTSTR)com_name) );

	int ret = authByIP(siteId, hostId, edition.c_str(), errString, ip, mac, false);

	if( (ret == 2 || ret == 1 ) && com_name != hostId.c_str() )
	{
		ciDEBUG(10, ("SetComputerName=%s", hostId.c_str()) );
		::SetComputerNameEx(ComputerNamePhysicalDnsHostname, hostId.c_str());
		ret = AUTH_SUCCEED_BUT_REBOOTING;
	}

	return ret;
}

int installWebUtil::unauth(LPCSTR siteId, LPCSTR hostId)
{
	return -1;
}

int installWebUtil::resetAuth(LPCSTR siteId, LPCSTR hostId)
{
	return -1;
}
