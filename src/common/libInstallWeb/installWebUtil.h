#pragma once

#include <list>
#include <string>

using namespace std;

#ifdef _INSTALLWEB
#define AFX_INSTALLWEB_DLL __declspec( dllexport )
#else
#define AFX_INSTALLWEB_DLL __declspec( dllimport )
#endif


// CServerCheck

//class AFX_INSTALLWEB_DLL installWebUtil
class installWebUtil
{
protected:
	static installWebUtil*	_instance;
	installWebUtil();

public:
	static installWebUtil* getInstance();
	static void	clearInstance();
	static CString _logFilename;

	virtual ~installWebUtil();

protected:
	LPCSTR		GetEditionString(LPCSTR edition);

	bool		m_bOnceTimeForceAuth;	// 한번만 강제인증

	int			_authBy(LPCSTR lpszModuleName, string& siteId, string& hostId, LPCSTR edition, string& errString, string& ip, string& mac, bool reset);

public:

	void		SetOnceTimeForceAuth(bool bForce) { m_bOnceTimeForceAuth=bForce; };

	int			auth(LPCSTR siteId, LPCSTR hostId, LPCSTR edition,
					string& errString, string& mac, bool reset=false);

	int			authByIP(string& siteId, string& hostId, LPCSTR edition, 
					string& errString, string& ip, string& mac, bool reset=false);

	int			authByMAC(string& siteId, string& hostId, LPCSTR edition,
					string& errString, string& ip, string& mac, bool reset=false);

	int			authByNAME(string& siteId, string& hostId, LPCSTR edition,
					string& errString, string& ip, string& mac, bool reset=false);		// siteId=hostName, hostId=hostType

	int			authByIP();

	int			unauth(LPCSTR siteId, LPCSTR hostId);

	int			resetAuth(LPCSTR siteId, LPCSTR hostId);
};
