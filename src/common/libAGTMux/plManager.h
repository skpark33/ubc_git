#ifndef _plManager_h_
#define _plManager_h_

#include <ci/libThread/ciMutex.h>
#include <ci/libBase/ciTime.h>
#include <CMN/libCommon/ubcMux.h>

typedef map<ciString,ciStringSet*>	 PLMap;   // key : programId, value:pmId

class PMOEle  : public ubcMuxData {
public:
	PMOEle() : currentConnectionCount(0) {}
	ciLong		currentConnectionCount;	
	ciTime		lastUpdateTime;
};



class plManager   {
public:
	static plManager*	getInstance();
	static void	clearInstance();

	virtual		~plManager() ;

	void	clear();
	int		loadpl();
	void	insert(const char* programId, const char* pmId);
	void	remove(const char* programId, const char* pmId);
	void	remove(const char* programId);
	int		find(const char* programId, ciStringSet& outSet);

	ciBoolean	withCD() { return 	_withContentsDistributor; }
	// 1. get muxData with siteId;  <-- for socket or vnc info
	// --> 1) get pmId with siteid from Site DB 
	// --> 2) get mux data with pmId from PMO DB

	ciBoolean	getPmId(const char* siteId, ciString& outVal);
	ubcMuxData*	getMuxData(const char* pmId);

	// 2.ContentsDistributor 를 사용하는 경우!
	// get  muxData with programId <--  for ftpInfo
	// --> 1) get pmIdList from programLocation with programId from  PL Managed Cache
	// --> 2) get most idle mux data within pmIdList from PMO DB where lastUpdateTime is less than 90 sec
    //        --> if there is no idle mux data where lastUpdateTime is less than 90 sec,  random mux selected

	ciBoolean	getPmList(const char* programId, ciStringSet& outSet);
	ubcMuxData*	getMuxData(ciStringSet& pmIdSet);

	// 3. ContentsDistributor 를 사용하지 않고, NAS 로 엮여 있는 경우
	// NewVersion !!! get  muxData with programId <--  for ftpInfo
	// 사실상 programId 는 무시되고,
	// 그냥 제일 한가한 FTP ID 를 리턴하게 된다.
	ubcMuxData*	getMuxData();

protected:
	static plManager*	_instance;
	static ciMutex		_instanceLock;

	plManager() ;

	ciBoolean	_getRandomId(ciStringSet& pmIdSet, ciString& outVal);

	ubcMuxMap	_muxMap;
	ciMutex		_muxMapLock;

	PLMap		_map;
	ciMutex		_mapLock;

	ciBoolean	_withContentsDistributor;

};

#endif // _plManager_h_
