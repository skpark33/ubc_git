#ifndef _AGTMuxSession_h_
#define _AGTMuxSession_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>

////////////////////
// AGTMuxSession

class ubcMuxData;

class AGTMuxSession : public ciThread {
public:
	AGTMuxSession();
	virtual ~AGTMuxSession();

	ciBoolean init();
	void fini();

	void run();
	void destroy();

	ciBoolean getAGTServerInfo(const char* pMsg, ciString& oServerInfo);
	int getKey(const char* pMsg, ciString& oKey);
	ciBoolean getMuxServerInfo(ciString& oMuxIp, int& oMuxPort);
	ciBoolean loadAGTServerList();

protected:
	ubcMuxData*	_getMuxData(ciString& siteId, ciString& programId);
	ciBoolean _getKey(const char* macAddress, ciString& siteId, ciString& hostId);
	int	_getSiteId(const char* hostId, ciString& siteId);

	//map<ciString,ciString> _agtServerTable;
	//unsigned int _counter;

	ciBoolean _isAlive;
	int _serverFd;
	int _serverPort;
};

#endif // _AGTMuxSession_h_
