#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libConfig/ciPath.h>

#include <ci/libDebug/ciArgParser.h>
#include <ci/libBase/ciStringTokenizer.h>

#include <cci/libUtil/cciCorbaMacro.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciBoolean.h>
#include <cci/libValue/cciValueInclude.h>

#include <cci/libObject/cciObject.h>
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciCall.h>

#include <CMN/libCommon/ubcIni.h>

#if defined (_COP_MSC_)
     #include <windows.h>
     #include <atltime.h>
#endif#

#include "plManager.h"

ciSET_DEBUG(10, "plManager");

plManager* 	plManager::_instance = 0; 
ciMutex 	plManager::_instanceLock;


plManager*	
plManager::getInstance() {
	if(!_instance) {
		ciGuard aGuard(_instanceLock);
		if(!_instance) {
			_instance = new plManager;
		}
	}
	return _instance;
}
void	
plManager::clearInstance() {
	if(_instance) {
		ciGuard aGuard(_instanceLock);
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}


plManager::plManager() 
{
    ciDEBUG(7, ("plManager...()"));

	_withContentsDistributor = ciFalse;
	if(ciArgParser::getInstance()->isSet("+withCD")){
		_withContentsDistributor = ciTrue;
	}

	if(_withContentsDistributor){
		ciDEBUG(1,("_withContentsDistributor case"));
		loadpl();
	}else{
		ciDEBUG(1,("no _withContentsDistributor case"));
	}
}

plManager::~plManager() 
{
    ciDEBUG(7, ("~plManager()"));
	clear();
}

int
plManager::loadpl()
{
    ciDEBUG(7, ("loadpl()"));	
	
	cciCall call("get");
	cciEntity aEntity("OD=OD/ProgramLocation=*");
	call.setEntity(aEntity);

	cciAny nullAny;
	call.addItem("programId", nullAny);
	call.addItem("pmId", nullAny);
	call.wrap();
	
	ciString callString=call.toString();
	ciDEBUG(5,(callString.c_str()));

	ciString errString;
	try {
		if(!call.call()){
			errString = callString + " failed";
			ciERROR((errString.c_str()));
			return 0;
		}
	}catch(CORBA::Exception& ex){
		errString = callString + " failed(";
		errString += ex._name();
		errString += ")";
		ciERROR((errString.c_str()));
		return 0;
	}

	clear();
	int counter=0;

	while(call.hasMore()) {
		cciReply reply;
		if (!call.getReply(reply)) {
			ciERROR(("getReply Fail"));
			continue;
		}

		if(reply.unwrap()){
			ciString programId;
			if(!reply.getItem("programId",			programId)) continue;
			ciString pmId;
			if(!reply.getItem("pmId",			pmId)) continue;

			this->insert(programId.c_str(), pmId.c_str());
			counter++;
			ciDEBUG(5,("%s %s data inserted", programId.c_str(), pmId.c_str()));
		}
	}
	ciWARN(("%d ProgramLocation FOUND", counter));
	return counter;
}

void	
plManager::clear()
{
	ciDEBUG(5,("clear()"));
	ciGuard aGuard(_mapLock);
	PLMap::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		ciStringSet* aSet = itr->second;
		if(aSet) {
			aSet->clear();
			delete aSet;
		}
	}
	_map.clear();
}

void	
plManager::insert(const char* programId, const char* pmId)
{
	ciDEBUG(5,("insert(%s,%s)", programId, pmId));
	ciGuard aGuard(_mapLock);
	ciStringSet* aSet = 0;
	PLMap::iterator itr = _map.find(programId);
	if(itr != _map.end()){
		aSet = itr->second;
	}else{
		aSet = new ciStringSet();
		_map.insert(PLMap::value_type(programId, aSet));
	}
	aSet->insert(pmId);
}

void	
plManager::remove(const char* programId)
{
	ciDEBUG(5,("remove(%s)", programId));
	ciGuard aGuard(_mapLock);
	PLMap::iterator itr = _map.find(programId);
	if(itr != _map.end()){
		ciStringSet* aSet = itr->second;
		if(aSet) {
			aSet->clear();
			delete aSet;
		}
		_map.erase(itr);
		ciDEBUG(5,("removed(%s)", programId));
	}
}

void	
plManager::remove(const char* programId, const char* pmId)
{
	ciDEBUG(5,("remove(%s,%s)", programId, pmId));
	ciGuard aGuard(_mapLock);
	PLMap::iterator itr = _map.find(programId);
	if(itr != _map.end()){
		ciStringSet* aSet = itr->second;
		ciStringSet::iterator jtr = aSet->find(pmId);
		if(jtr != aSet->end()){
			aSet->erase(jtr);
			ciDEBUG(5,("removed(%s,%s)", programId, pmId));
		}
	}
}

int		
plManager::find(const char* programId, ciStringSet& outSet)
{
	ciDEBUG(5,("find(%s)", programId));

	ciString buf = programId;
	int len = buf.length();
	if(buf.substr(len-4)==".ini"){
		buf = buf.substr(0,len-4);
	}

	ciGuard aGuard(_mapLock);
	PLMap::iterator itr = _map.find(buf);
	if(itr != _map.end()){
			ciStringSet* aSet = itr->second;
			outSet = *aSet;
			ciDEBUG(5,("finded(%s)", buf.c_str()));
			return outSet.size();
	}
	return 0;
}

// 1. get muxData with siteId;  <-- for socket or vnc info
// --> 1) get pmId with siteid from Site DB 
// --> 2) get mux data with pmId from PMO DB

ciBoolean	
plManager::getPmId(const char* siteId, ciString& outVal)
{
	ciDEBUG(1,("getPmId(%s)", siteId));

	cciCall aCall("get");

	cciEntity aEntity("PM=*/Site=%s", siteId);
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);

	aCall.wrap();
	try
	{
		if(!aCall.call()) {
			ciERROR(("get %s fail", aEntity.toString()));
			return ciFalse;
		}
		cciReply reply;
		while(aCall.getReply(reply))
		{
			ciDEBUG(9, ("get %s succeed", aEntity.toString()));
			if(!reply.unwrap()){
				ciWARN(("Invalid data"));
				continue;
			}
			reply.getItem("mgrId", outVal);
			if(outVal.empty()){
				ciWARN(("Invalid data"));
				continue;
			}
		}
	}
	catch(CORBA::Exception& ex)
	{
		ciERROR((ex._name()));
		return ciFalse;
	}
	return ciTrue;
}
ubcMuxData*	
plManager::getMuxData(const char* pmId)
{
	ciDEBUG(1,("getMuxData(%s)", pmId));

	{
		ciGuard aGuard(_muxMapLock);
		ubcMuxMap::iterator itr = _muxMap.find(pmId);
		if(itr!=_muxMap.end()){
			ciDEBUG(1,("%s pm muxData founded", pmId));
			return itr->second;
		}
	}

	cciCall aCall("get");

	cciEntity aEntity("OD=OD/PMO=%s", pmId);
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("pmId", nullAny);
	aCall.addItem("ipAddress", nullAny);
	aCall.addItem("ipAddress1", nullAny);
	aCall.addItem("ipAddress2", nullAny);
	aCall.addItem("ftpAddress", nullAny);
	aCall.addItem("ftpAddress1", nullAny);
	aCall.addItem("ftpAddress2", nullAny);
	aCall.addItem("ftpMirror", nullAny);
	aCall.addItem("ftpMirror1", nullAny);
	aCall.addItem("ftpMirror2", nullAny);
	aCall.addItem("ftpId", nullAny);
	aCall.addItem("ftpPasswd", nullAny);
	aCall.addItem("ftpPort", nullAny);
	aCall.addItem("socketPort", nullAny);

	aCall.wrap();

	try
	{
		if(!aCall.call()) {
			ciERROR(("get %s fail", aEntity.toString()));
			return ciFalse;
		}

		cciReply reply;
		while(aCall.getReply(reply))
		{
			ciDEBUG(9, ("get %s succeed", aEntity.toString()));

			ubcMuxData *aData = new ubcMuxData();

			if(!reply.unwrap()){
				ciWARN(("Invalid data"));
				continue;
			}
			reply.getItem("pmId", aData->pmId);
			if(aData->pmId.empty()){
				ciWARN(("Invalid data"));
				continue;
			}
			reply.getItem("ipAddress", aData->ipAddress);
			reply.getItem("ipAddress1", aData->ipAddress1);
			reply.getItem("ipAddress2", aData->ipAddress2);
			reply.getItem("ftpAddress", aData->ftpAddress);
			reply.getItem("ftpAddress1", aData->ftpAddress1);
			reply.getItem("ftpAddress2", aData->ftpAddress2);
			reply.getItem("ftpMirror", aData->ftpMirror);
			reply.getItem("ftpMirror1", aData->ftpMirror1);
			reply.getItem("ftpMirror2", aData->ftpMirror2);
			reply.getItem("ftpId", aData->ftpId);
			reply.getItem("ftpPasswd", aData->ftpPasswd);
			reply.getItem("ftpPort", aData->ftpPort);
			reply.getItem("socketPort", aData->socketPort);

			ciDEBUG(9,("%s pm founded", aData->pmId.c_str()));
			ciGuard aGuard(_muxMapLock);
			_muxMap.insert(ubcMuxMap::value_type(aData->pmId, aData));
			return aData;
		}
	}
	catch(CORBA::Exception& ex)
	{
		ciERROR((ex._name()));
		return ciFalse;
	}
	return 0;
}


// 2. get  muxData with programId <--  for ftpInfo
// --> 1) get pmIdList from programLocation with programId from  PL Managed Cache
// --> 2) get most idle mux data within pmIdList from PMO DB where lastUpdateTime is less than 90 sec
//        --> if there is no idle mux data where lastUpdateTime is less than 90 sec,  random mux selected

ciBoolean	
plManager::getPmList(const char* programId, ciStringSet& outSet)
{
	ciDEBUG(1,("getPmList(%s)", programId));
	return 	this->find(programId, outSet);
}

ubcMuxData*	
plManager::getMuxData(ciStringSet& pmIdSet)
{
	ciDEBUG(1,("getMuxData(...)"));

	cciCall aCall("bulkget");

	cciEntity aEntity("OD=OD/PMO=*");
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("pmId", nullAny);
	aCall.addItem("currentConnectionCount", nullAny);
	aCall.addItem("lastUpdateTime", nullAny);
	aCall.wrap();

	ciTime now;
	now = now - 60*5;

	ciString pmIdSetStr="";
	ciStringSet::iterator itr;
	for(itr=pmIdSet.begin();itr!=pmIdSet.end();itr++){
		if(!pmIdSetStr.empty()){
			pmIdSetStr += ",";
		}
		pmIdSetStr += "'";
		pmIdSetStr += (*itr);
		pmIdSetStr += "'";
	}

	ciString whereClause = "pmId in (";
	whereClause += pmIdSetStr;
	whereClause += ") and currentConnectionCount >= 0 and adminState = 1" ;
	aCall.addItem("whereClause", whereClause);

	ciULong min = 0;
	ciString minPmId;

	try
	{
		if(!aCall.call()) {
			ciERROR(("get %s fail", aEntity.toString()));
			return 0;
		}

		cciReply reply;
		while(aCall.getReply(reply))
		{
			ciDEBUG(9, ("get %s succeed", aEntity.toString()));

			if(!reply.unwrap()){
				ciWARN(("Invalid data"));
				continue;
			}

			ciString pmId;
			reply.getItem("pmId", pmId);
			if(pmId.empty()){
				ciWARN(("Invalid data"));
				continue;
			}
			ciTime lastUpdateTime;
			reply.getItem("lastUpdateTime", lastUpdateTime);

			if(lastUpdateTime < now){
				ciWARN(("%s(%s) is old data", pmId.c_str(), lastUpdateTime.getTimeString().c_str()));
				continue;
			}

			ciLong currentConnectionCount=0;
			reply.getItem("currentConnectionCount", currentConnectionCount);

			if( min >= currentConnectionCount) {
				min = currentConnectionCount;
				minPmId = pmId;
				ciDEBUG(9,("%s pm founded", pmId.c_str()));
			}
		}
	}
	catch(CORBA::Exception& ex)
	{
		ciERROR((ex._name()));
		return 0;
	}

	ciDEBUG(9,("%s pm is most idle ", minPmId.c_str()));
	if(minPmId.empty()){
		ciDEBUG(9,("min pm is null random id wiill be selected"));
		if(!_getRandomId(pmIdSet, minPmId)){
			ciERROR(("pmIdSet empty"));
			return 0;
		}
	}

	return getMuxData(minPmId.c_str());
}

// 3. get  muxData with programId <--  for ftpInfo
// --> 2) get most idle mux data  from PMO DB where lastUpdateTime is less than 90 sec
//  --> if there is no idle mux data where lastUpdateTime is less than 90 sec, 1 will be selected

ubcMuxData*	
plManager::getMuxData()
{
	ciDEBUG(1,("getMuxData()"));

	cciCall aCall("bulkget");

	cciEntity aEntity("OD=OD/PMO=*");
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("pmId", nullAny);
	aCall.addItem("currentConnectionCount", nullAny);
	aCall.addItem("lastUpdateTime", nullAny);
	aCall.wrap();

	ciTime now;
	now = now - 60*5;
	
	ciString whereClause = "adminState = 1";
	if(this->withCD()){
		whereClause = "currentConnectionCount >= 0 and adminState = 1" ;
	}
	aCall.addItem("whereClause", whereClause);

	ciULong min = 0;
	ciString minPmId;
	ciStringSet pmIdSet;

	try
	{
		if(!aCall.call()) {
			ciERROR(("get %s fail", aEntity.toString()));
			return 0;
		}

		cciReply reply;
		while(aCall.getReply(reply))
		{
			ciDEBUG(9, ("get %s succeed", aEntity.toString()));

			if(!reply.unwrap()){
				ciWARN(("Invalid data"));
				continue;
			}

			ciString pmId;
			reply.getItem("pmId", pmId);
			if(pmId.empty()){
				ciWARN(("Invalid data"));
				continue;
			}
			if(this->withCD()){
				ciTime lastUpdateTime;
				reply.getItem("lastUpdateTime", lastUpdateTime);
				if(lastUpdateTime < now){
					ciWARN(("%s(%s) is old data", pmId.c_str(), lastUpdateTime.getTimeString().c_str()));
					continue;
				}
				ciLong currentConnectionCount=0;
				reply.getItem("currentConnectionCount", currentConnectionCount);

				if( min >= currentConnectionCount) {
					min = currentConnectionCount;
					minPmId = pmId;
					ciDEBUG(9,("%s pm founded", pmId.c_str()));
				}
			}else{
				pmIdSet.insert(ciStringSet::value_type(pmId));		
			}
		}
	}
	catch(CORBA::Exception& ex)
	{
		ciERROR((ex._name()));
		return 0;
	}
	if(ciFalse == this->withCD()){
		ciDEBUG(9,("random pmId will be selected"));
		this->_getRandomId(pmIdSet, minPmId);
	}

	if(minPmId.empty()){
		ciDEBUG(9,("min pm is null 1 wiill be selected"));
		minPmId = "1";
	}
	ciDEBUG(9,("%s pm is most idle ", minPmId.c_str()));

	return getMuxData(minPmId.c_str());
}

ciBoolean
plManager::_getRandomId(ciStringSet& pmIdSet, ciString& outVal)
{
	static int seed =0;

	int mosu = pmIdSet.size() ;
	if(mosu == 0)  {
		return ciFalse;
	}
	int index = seed % mosu;
	seed++;

	int i=0;
	ciStringSet::iterator itr;
	for(itr=pmIdSet.begin();itr!=pmIdSet.end();itr++,i++){
		if(index == i) {
			outVal = (*itr);
			return ciTrue;
		}
	}

	return ciFalse;
}