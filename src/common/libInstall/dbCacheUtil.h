#ifndef _dbCacheUtil_h_
#define _dbCacheUtil_h_

#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

//#include "libxml/xmlreader.h"

class dbCache
{
public:
	dbCache() : classname("") { nvMap = new ciStringMap(); }
	virtual ~dbCache() { clear(); }
	void clear() {
		nvMap->clear();
		delete nvMap;
		classname="";
	}
	const char* getValue(const char* attrname);
	
	ciString		classname;
	ciStringMap*	nvMap;
};
typedef list<dbCache*>	dbCacheList;

class dbCacheUtil {
public:
	dbCacheUtil();
	virtual ~dbCacheUtil() ;

	virtual void close();
	virtual void clearData();

	virtual int insertData(const char* classname);
	virtual ciBoolean selectData(const char* siteId, const char* programId);

	virtual const char*	getValue(const char* classname, const char* attrname);
	dbCacheList&		getDataList() { return _dataList; }

	void	printIt();
	ciBoolean writeIt(const char* filename, const char* configPath="", const char* sub="");
	void	toString(ciString& outVal);

protected:

	int			_callData(cciEntity& entity, const char* classname);

	ciMutex			_dataLock;
	dbCacheList		_dataList;

};

#endif