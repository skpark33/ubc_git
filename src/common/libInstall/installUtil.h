#ifndef _installUtil_h_
#define _installUtil_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>

#define		UBC_3RDPARTY_PATH	_COP_CD("C:\\SQISoft\\3RDPARTY")
#define		UBC_EXE_PATH		_COP_CD("C:\\SQISoft\\UTV1.0\\execute")
#define		UBC_CONTENT_PATH	_COP_CD("C:\\SQISoft\\Contents\\Enc")

#define		CODEC_FILE			"klcodec_latest.exe"
#define		CODEC_OLD_FILE		"klcodec470f.exe"
#define		CODEC_VERIFY		_COP_PROGRAM_FILES("C:\\Program Files\\K-Lite Codec Pack\\install.log")

#define AUTH_SUCCEED				1
#define AUTH_AUTHENTICATION_FAIL	0
#define AUTH_GET_MAC_FAIL			-1
#define AUTH_COMMUNICATION_FAIL		-2
#define AUTH_WRITE_FILE_FAIL		-4

#define SHUTDOWNTIME_DELIMETER		'^'
#define SHUTDOWNTIME_DELIMETER_D	"^"

class installUtil {
public:
	static installUtil*	getInstance();
	static void	clearInstance();

	virtual ~installUtil() ;

	int run(ciBoolean authOnly);

	ciBoolean	setServerEnv();

	int	auth(const char* siteId, const char* hostId, const char* edition,
		ciString& errString, 
		ciString& mac,
		ciBoolean reset=ciFalse);

	int	authByServer(ciString& siteId, ciString& hostId, ciString& msgString);
	int	authByServer(ciString& msgString);
	int	authByServer() { ciString buf; return authByServer(buf); }

	ciBoolean	getMuxServerInfo(ciString& oMuxIp, int& oMuxPort);


	ciBoolean   setSiteId(const char* siteId);
	ciBoolean   getSiteId(ciString& siteId);
	ciBoolean   setHostType(ciLong hostType);
	ciBoolean   getHostType(ciLong& hostType);
	int   getSiteId(const char* hostId, ciString& outSite, ciString& outSiteName,
					ciString& outSiteCode,ciString& outSiteType,
					ciString& outChainNo, ciString& outPhoneNo1, ciLong& outHostType);

	ciBoolean   installVNC(const char* foo, const char* portStr);
	ciBoolean	updateVNCAdmin(const char* foo, const char* portStr);
	ciBoolean	restoreVNCBinary(const char* portStr);
	ciBoolean	updateVNCBinary(const char* portStr);
	ciBoolean	checkVNCAdmin(const char* foo, const char* portStr);
	ciBoolean	checkVNCPasswd(const char* passwd=0);
	ciBoolean	updateVNCBackground();

	ciBoolean   installCodec(const char* command);
	ciBoolean	oldCodecSetting();
	ciBoolean	lastestCodecSetting();
	ciBoolean	lastestCodecSettingWin7();
	ciBoolean	codecSettingRollback();
	ciBoolean	checkCodecSetting();
	ciBoolean	checkCodecSettingWin7();

	ciBoolean	setAutoLogon(const char* password);

	ciBoolean   installDivX();
	ciBoolean   installFLV();

	ciBoolean	installFlashPlayer();

	ciBoolean	runSoftDriveBat();
	ciBoolean	installFTP();
	ciBoolean	updateFTPAdmin();

	ciBoolean	setComputerName(const char* prefix, const char* port);
	ciBoolean	setComputerName(const char* hostId);

	ciBoolean	drm1(ciBoolean visible);
	ciBoolean	setRegistry1(int value);

	ciBoolean	setErrorReport();
	ciBoolean	setFirewallException();
	ciBoolean	checkFirewallException();
	ciBoolean	setFWVFirewallException();

	ciBoolean	setRegistry(HKEY firstKey, const char* regKey, const char* name, const char* val);
	ciBoolean	setRegistry(HKEY firstKey, const char* regKey, const char* name, int val);
	ciBoolean	setRegistry_DWORD(HKEY firstKey, const char* regKey, const char* name, int val);
	ciBoolean	checkRegistry(HKEY module, ciString& regKey);
	ciBoolean	getRegList(HKEY hKey, ciStringList& reval_);

	ciBoolean	checkVerify(const char* filename);

	ciBoolean	printFirewallException();

	ciBoolean	getShutdownTime(ciString& timeStr);
	ciBoolean	getWeekShutdownTime(ciString& timeStr);
	ciBoolean	setShutdownTime(ciString& timeStr, ciBoolean realTime);
	ciBoolean	setWeekShutdownTime(ciString& timeStr, ciBoolean realTime);
	ciBoolean	calcShutdownTime(ciString& timeStr);


	const char* getConfigPath();
	const char* get3rdPartyPath();

	ciBoolean	getChannelName(ciString& res,int display);
	
	ciBoolean getChannelName(ciString& browserId, ciString& host, ciBoolean& networkUse, ciBoolean isActive=ciFalse);
	ciBoolean setChannelName(const char* host, 
					  const char* site,
					  ciBoolean isNetwork,
					  ciBoolean realTime);

	ciBoolean getChannelName(const char* argValue, ciString& host, ciString& site, ciBoolean& networkUse);
	ciBoolean setChannelName(const char* argValue, const char* startUpName,
					  const char* host, 
					  const char* site,
					  const char* startUp,
					  ciBoolean isNetwork,
					  ciBoolean realTime);
	ciBoolean setChannelName(int brw_no,const char* host,const char* site);

	ciBoolean getMenuOption(ciString& arg);
	ciBoolean setMenuOption(ciBoolean on);

	ciBoolean getDisplayOption(ciString& arg);
	ciBoolean setDisplayOption(const char* arg, ciBoolean realTime);
	
	ciBoolean getMouseOption(ciBoolean& show);
	ciBoolean setMouseOption(ciBoolean show, ciBoolean realTime);

	ciBoolean getLocalOption(ciBoolean& isLocal);
	ciBoolean setLocalOption(ciBoolean isLocal, ciBoolean realTime);

	ciBoolean setAutoUpdate(ciBoolean autoValue);
	ciBoolean getAutoUpdate(ciBoolean& autoValue);

	ciBoolean getHost(int display, ciString& autoHost, ciString& currentHost);
	ciBoolean getHost(int display, ciString& autoHost);
	ciBoolean getAllCurrentHost(int display,ciStringSet& outList);
	ciBoolean getCurrentHost(int display,ciString& host);

	ciBoolean startUBC();
	ciBoolean startUBC(const char* option);
	ciBoolean stopUBC();
	ciBoolean	isOptionExist(const char* argName, const char* optionName);
	ciBoolean getFileList(const char* path, ciStringList& outList, const char* pattern=0);
	void	getCurrentBRW(ciString& outVal);
	void	getBRWProperties(const char* proName, ciBoolean autoStart, ciString& outVal);
	void	getBRWProperties(ciBoolean autoStart, ciString& arg, ciString& debug, ciString& startUp);
	void	getCurrentBRWID(const char* inVal, ciString& outVal);
	ciBoolean setProperty(const char* name, const char* value,ciBoolean realTime);

	void	updateArg(const char* argValue,
					   const char* host, const char* site, ciBoolean isNetwork,
					   ciString& newValue);
	ciBoolean runBrowser(int display, const char* host, const char* site, ciBoolean networkUse);
	ciBoolean runBrowser(int display, const char* host, const char* site, const char* addArg, ciBoolean networkUse);
	ciBoolean runBrowser(int display, const char* additionalArg);
	
	ciBoolean stopBrowser(int display, const char* targetSchedule);
	ciBoolean stopBrowser2(int display, const char* targetSchedule);
	ciBoolean stopBrowser2(int display);
	ciBoolean stopBrowser3(int display);

	void	writeBrowser(ciTime& now, const char* host, int display, const char* by);

	ciBoolean	muteMinus();
	ciBoolean	muteMinus(const char* argName);
	ciBoolean	disableProcess();
	ciBoolean	ableProcess();

	ciBoolean	propertyMigration1(const char* siteId="");
	ciBoolean	runBrowserAfterFirmware(ciString& edition);
	ciBoolean	setPreRunningId(const char* argName, const char* pre_id);
	ciBoolean	autoBrowserExist(int idx=-1);

	ciBoolean	hostNameIdentify(const char* key);
	ciBoolean	vncPortIdentify(const char* key);

	ciBoolean	getHostValue(const char* argValue, ciBoolean useComputerName, ciString& outVal);

	const char*	editionStr(const char* edition);

	ciBoolean	setPrivilege();
	ciBoolean	changeServerIP(const char* ip);
	ciBoolean	checkHDD();

	int			getThreshold();
	void		setThreshold(ciShort threshold);
	int			getDiskUsed();
	ciBoolean	cleanContents();

	time_t getLastHDDCheckTime();
	void setLastHDDCheckTime(time_t now);

	ciBoolean	toMSSQL();
	ciBoolean	toMYSQL();

protected:
	installUtil();

	ciBoolean	_getCurrentIni(ciStringSet& outvalList);
	ciBoolean	_cleanContents(ciStringSet& programList);


	ciBoolean	_setMenuOption(const char* argName, ciBoolean on, ciBoolean realTime);
	ciBoolean	_setMouseOption(const char* argName, ciBoolean show, ciBoolean realTime);
	ciBoolean	_setLocalOption(const char* argName, ciBoolean isLocal, ciBoolean realTime);
	ciBoolean	_setDisplayOption(const char* argName, const char* arg, ciBoolean realTime);
	ciBoolean	_getHostValueUsingId(const char* brwId, ciString& outVal);
	ciBoolean	_getHostValueUsingId2(const char* brwId, ciString& outVal);
	void		_getActiveBRWProperties(ciString& outVal);

	ciBoolean _setProperty(const char* name, const char* value);

	ciBoolean	_setFirewallException(const char* regKey, const char* filename);
	ciBoolean	_checkFirewallException(const char* regKey,int max);

	static installUtil*	_instance;
	static ciMutex			_instanceLock;

	ciString _hostId;
	ciString _siteId;
};

#endif // _installUtil_h_
