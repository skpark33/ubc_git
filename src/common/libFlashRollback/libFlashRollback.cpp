// libFlashRollback.cpp : DLL 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "resource.h"
#include "libFlashRollback.h"
#include <io.h>
#include <Winbase.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		FLASH_REG_PATH		"SOFTWARE\\Macromedia\\FlashPlayerActiveX"

// 유일한 응용 프로그램 개체입니다.
/*
CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	// MFC를 초기화합니다. 초기화하지 못한 경우 오류를 인쇄합니다.
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: 오류 코드를 필요에 따라 수정합니다.
		_tprintf(_T("심각한 오류: MFC를 초기화하지 못했습니다.\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: 응용 프로그램의 동작은 여기에서 코딩합니다.
	}

	return nRetCode;
}
*/

CFlashRollback::CFlashRollback()
{
}

CFlashRollback::~CFlashRollback()
{
}

int CFlashRollback::CheckFlashFile()
{
	m_strErrMessage = _T("");

	CString str_flashpath;
	if( !getCurrentFlashPath(str_flashpath) )
	{
		m_strErrMessage = _T("Fail to get flash-path !!!");
		return FALSE;
	}

	CString str_ver;
	if( !getCurrentFlashVersion(str_flashpath, str_ver) )
	{
		m_strErrMessage = _T("Fail to get flash-version !!!");
		return FALSE;
	}

	if( str_ver != _T("20.00.000.267") )
	{
		m_strErrMessage = _T("Flash file is compatible version.");
		return -1;
	}

	CString str_rollbackpath;
	getFlashRollbackPath(str_flashpath, str_rollbackpath);

	if( !writeFlashRollbackFile(str_rollbackpath) )
	{
		m_strErrMessage = _T("Fail to write flash-rollback-file !!!");
		return FALSE;
	}

	if( !replaceToFlashRollbackFile(str_flashpath, str_rollbackpath) )
	{
		m_strErrMessage = _T("Fail to rollback flash-file !!!");
		return FALSE;
	}

	return TRUE;
}

BOOL CFlashRollback::getRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, CString& strValue, DWORD dwOption)
{
	//__FUNC_BEGIN__

	HKEY hKey;
	DWORD dwSize = 1024;
	DWORD dwType = REG_SZ;
	char szValue[1024] = {0};

	LONG lRet = ::RegOpenKeyEx(hMainKey, lpszSubKey, 0, KEY_ALL_ACCESS | dwOption, &hKey);
	if(lRet != ERROR_SUCCESS)
	{
		//__WARN__(("Fail to open value (%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, GetErrorMessage(lRet)));
		//__FUNC_END__
		return FALSE;
	}

	BOOL ret_val = TRUE;
	lRet = RegQueryValueEx(hKey, lpszValueName, NULL, &dwType, (LPBYTE)&szValue, &dwSize);
	if(lRet != ERROR_SUCCESS)
	{
		//__WARN__(("Fail to get value (%s\\%s\\%s) !!! (%s)", szHKEY[(int)hMainKey & 0x0F], lpszSubKey, lpszValueName, GetErrorMessage(lRet)));
		ret_val = FALSE;
	}

	RegCloseKey(hKey);

	strValue = szValue;

	//__DEBUG__(("Value=%s", szValue));
	//__FUNC_END__
	return ret_val;
}

BOOL CFlashRollback::getCurrentFlashPath(CString& strFullpath)
{
	return getRegValue(HKEY_LOCAL_MACHINE, FLASH_REG_PATH, "PlayerPath", strFullpath);
}

BOOL CFlashRollback::getCurrentFlashVersion(LPCTSTR lpszFlashPath, CString& strVersion)
{
	// 파일로부터 버전정보데이터의 크기가 얼마인지를 구합니다.
	DWORD info_size = GetFileVersionInfoSize(lpszFlashPath, 0);
	if( info_size==0 ) return FALSE;

	// 버전정보를 담을 버퍼 할당
	char* buf = new char[info_size];
	if( buf==NULL ) return FALSE;

	// 버전정보데이터를 가져옵니다.
	if( GetFileVersionInfo(lpszFlashPath, 0, info_size, buf)==0 )
	{
		delete[]buf;
		return FALSE;
	}

	VS_FIXEDFILEINFO* pFineInfo = NULL;
	UINT bufLen = 0;
	// buffer로 부터 VS_FIXEDFILEINFO 정보를 가져옵니다.
	if( VerQueryValue(buf, "\\", (LPVOID*)&pFineInfo, &bufLen)==0 )
	{
		delete[]buf;
		return FALSE;
	}

	WORD majorVer, minorVer, buildNum, revisionNum;
	majorVer = HIWORD(pFineInfo->dwFileVersionMS);
	minorVer = LOWORD(pFineInfo->dwFileVersionMS);
	buildNum = HIWORD(pFineInfo->dwFileVersionLS);
	revisionNum = LOWORD(pFineInfo->dwFileVersionLS);

	// 파일버전 출력
	//printf("version : %d,%d,%d,%d\n", majorVer, minorVer, buildNum, revisionNum);
	strVersion.Format(_T("%02d.%02d.%03d.%03d"), majorVer, minorVer, buildNum, revisionNum);

	delete[]buf;

	return TRUE;
}

void CFlashRollback::getFlashRollbackPath(LPCTSTR lpszFlashPath, CString& strRollbackPath)
{
	char drv[MAX_PATH], dir[MAX_PATH], fn[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(lpszFlashPath, drv, dir, fn, ext);

	strRollbackPath.Format(_T("%s%s%s.tmp"), drv, dir, fn);
}

BOOL CFlashRollback::writeFlashRollbackFile(LPCTSTR lpszRollbackPath)
{
	HRSRC pRes = FindResource(NULL,(LPTSTR)(IDR_FLASH_OCX_228),_T("FLASH"));
	if( pRes==NULL ) return FALSE;

    DWORD file_size = SizeofResource(NULL, pRes);
	if( file_size==NULL ) return FALSE;

	HANDLE hRes = LoadResource(NULL, (struct HRSRC__ *)pRes);
	if( hRes==NULL ) return FALSE;

	LPSTR lpRes = (char *)LockResource(hRes);
	if( lpRes==NULL ) return FALSE;

	CFile file;
	if(file.Open(lpszRollbackPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
	{
		LPBYTE pBuf = new BYTE[file_size];
		memcpy(pBuf, lpRes, file_size);

		file.Write(pBuf, file_size);
		file.Close();

		delete[]pBuf;
	}

	UnlockResource(hRes);
	FreeResource(hRes);

	return TRUE;
}

BOOL CFlashRollback::replaceToFlashRollbackFile(LPCTSTR lpszFlashPath, LPCTSTR lpszRollbackPath)
{
	CString str_org_path = lpszFlashPath;
	str_org_path.Replace(_T("."), _T(".org."));

	if( _taccess(str_org_path, 00)==0 )
	{
		// delete org-file
		if( !::DeleteFile(str_org_path) ) return FALSE;
	}

	// move ocx to org-file
	if( !::MoveFile(lpszFlashPath, str_org_path) ) return FALSE;

	// move tmp to ocx
	if( !::MoveFile(lpszRollbackPath, lpszFlashPath) )
	{
		::MoveFile(str_org_path, lpszFlashPath);
		return FALSE;
	}

	return TRUE;
}
