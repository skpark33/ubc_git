// 다음 ifdef 블록은 DLL에서 내보내기하는 작업을 쉽게 해 주는 매크로를 만드는 
// 표준 방식입니다. 이 DLL에 들어 있는 파일은 모두 명령줄에 정의된 _EXPORTS 기호로
// 컴파일되며, 동일한 DLL을 사용하는 다른 프로젝트에서는 이 기호를 정의할 수 없습니다.
// 이렇게 하면 소스 파일에 이 파일이 들어 있는 다른 모든 프로젝트에서는 
// LIBFLASHROLLBACK_API 함수를 DLL에서 가져오는 것으로 보고, 이 DLL은
// 이 DLL은 해당 매크로로 정의된 기호가 내보내지는 것으로 봅니다.
/*
#ifdef LIBFLASHROLLBACK_EXPORTS
#define LIBFLASHROLLBACK_API __declspec(dllexport)
#else
#define LIBFLASHROLLBACK_API __declspec(dllimport)
#endif
*/

// 이 클래스는 libFlashRollback.dll에서 내보낸 것입니다.
//class LIBFLASHROLLBACK_API CFlashRollback {
class CFlashRollback {
public:
	CFlashRollback();
	~CFlashRollback();

	LPCTSTR	GetErrorMessage() { return m_strErrMessage; };

	int CheckFlashFile();

protected:
	CString	m_strErrMessage;

	BOOL getRegValue(HKEY hMainKey, LPCSTR lpszSubKey, LPCSTR lpszValueName, CString& strValue, DWORD dwOption=KEY_WOW64_32KEY);
	BOOL getCurrentFlashPath(CString& strFullpath);
	BOOL getCurrentFlashVersion(LPCTSTR lpszFlashPath, CString& strVersion);
	void getFlashRollbackPath(LPCTSTR lpszFlashPath, CString& strRollbackPath);
	BOOL writeFlashRollbackFile(LPCTSTR lpszRollbackPath);
	BOOL replaceToFlashRollbackFile(LPCTSTR lpszFlashPath, LPCTSTR lpszRollbackPath);
};

//extern LIBFLASHROLLBACK_API int nlibFlashRollback;
//LIBFLASHROLLBACK_API int fnlibFlashRollback(void);
