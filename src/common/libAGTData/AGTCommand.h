#ifndef _AGTCommand_h_
#define _AGTCommand_h_

#include "AGTData.h"

class AGTCommand : public AGTData {
public:
	AGTCommand();
	virtual ~AGTCommand();

	void setCommand(const char* command);
	const char* getCommand();

	ciBoolean fromString(const char* strValue);
	const char* toString();

protected:
	ciString _command;
};

typedef list<AGTCommand>  AGTCommandList;
#endif //_AGTCommand_h_
