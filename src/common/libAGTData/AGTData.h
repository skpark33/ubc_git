#ifndef _AGTData_h_
#define _AGTData_h_

#include "ci/libBase/ciBaseType.h"
#include "ci/libBase/ciListType.h"
#include "ci/libBase/ciTime.h"

class AGTData {
public:
	AGTData();
	virtual ~AGTData();

	ciBoolean addItem(const char* name, const char* value);
	ciBoolean addItem(const char* name, bool value);
	ciBoolean addItem(const char* name, short value);
	ciBoolean addItem(const char* name, int value);
	ciBoolean addItem(const char* name, long value);
	ciBoolean addItem(const char* name, unsigned short value);
	ciBoolean addItem(const char* name, unsigned int value);
	ciBoolean addItem(const char* name, unsigned long value);
	ciBoolean addItem(const char* name, unsigned long long value);
	ciBoolean addItem(const char* name, float value);
	ciBoolean addItem(const char* name, ciStringList& value);

	ciBoolean getItem(const char* name, ciString& value);
	ciBoolean getItem(const char* name, ciStringList& value);
	ciBoolean getItem(const char* name, bool& value);
	ciBoolean getItem(const char* name, short& value);
	ciBoolean getItem(const char* name, int& value);
	ciBoolean getItem(const char* name, long& value);
	ciBoolean getItem(const char* name, unsigned short& value);
	ciBoolean getItem(const char* name, unsigned int& value);
	ciBoolean getItem(const char* name, unsigned long& value);
	ciBoolean getItem(const char* name, ciTime& value);
	ciBoolean getItem(const char* name, unsigned long long& value);
	ciBoolean getItem(const char* name, float& value);

	ciBoolean modifyItem(const char* name, const char* value);
	ciStringMap* getRepository() {
		return &_repo;
	}

	virtual ciBoolean fromString(const char* strValue);
	virtual ciBoolean addString(const char* strValue);
	virtual const char* toString();

	virtual const char* serialize();
	virtual ciBoolean deserialize(const char* strValue);

protected:
	ciStringMap _repo;
	ciString _strBuffer;
};

#endif //_AGTData_h_
