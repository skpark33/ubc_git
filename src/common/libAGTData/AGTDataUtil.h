#ifndef _AGTDataUtil_h_
#define _AGTDataUtil_h_

#include <map>
#include <vector>
#include <ci/libBase/ciBaseType.h>

typedef map<ciString, ciString> ParseQuoteMap;

class AGTDataUtil {
public:
    AGTDataUtil();
    virtual ~AGTDataUtil();

    //
    // Member Method Definition
    //
                
    ciBoolean tranQuote(ciString& pTarget);
    ciBoolean restoreQuote(ciString& pTarget, ciBoolean pQuote = ciFalse);
    void clear();
    
    //
    // Static Method Definition
    //
    
    static int countBracket(const char* pTarget);
    static ciBoolean charReplace(
                ciString& pTarget, 
                const char pFrom, 
                const char pTo, 
                int pStart = 1, 
                int pEnd = -1);
                
    static ciBoolean stringReplace(
                ciString& pTarget, 
                const char* pFrom,
                const char* pTo, 
                int pStart = 1, 
                int pEnd = -1);
                
    static void trimBracket(ciString& pTarget);
    static void trimQuote(ciString& pTarget);
    static void trimQuote(ciWString& pTarget);
    
 protected:
    ParseQuoteMap _quoteTable;
    ciBoolean _hasEscape;
};

#endif //_AGTDataUtil_h_
