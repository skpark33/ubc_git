#ifndef _AGTDownloadResult_h_
#define _AGTDownloadResult_h_

#include "ci/libBase/ciBaseType.h"
#include "ci/libBase/ciListType.h"
#include "ci/libBase/ciTime.h"
#include "ci/libThread/ciMutex.h"
#include "AGTData.h"

#define DN_INIT			0
#define DN_START		1
#define DN_PART_FAIL	2
#define DN_FAIL			3
#define DN_SUCCESS		4
#define DN_SUCCESS_NO_DN		5

#define E_REASON_INIT				0    
#define E_REASON_CONNECTION_FAIL	1
#define E_REASON_FILE_NOTFOUND		2
#define E_REASON_EXCEPTION_FTP		3
#define E_REASON_EXCEPTION_FILE		4
#define E_REASON_LOCAL_EXIST		5
#define E_REASON_DOWNLOAD_SUCCESS	6
#define E_REASON_COPY_FAIL			7
#define E_REASON_UNKNOWN			8

#define DN_INI_NAME		"UBCDownloadResult"

class DownloadProgressMsg
{
public:
	DownloadProgressMsg() {clear();}
	~DownloadProgressMsg()	{clear();}

	void clear() {
		memset(programId,0x00,256);
		memset(hostId,0x00,256);
		memset(siteId,0x00,256);
		browserId = 0;
		memset(progress,0x00,12);
		memset(fileName,0x00,256);
		state = 0;
		alreadyExist = 0;
	}

	char programId[256];  
	char hostId[256];  
	char siteId[256];  
	char progress[12];
	int  state;
	int  browserId; 
	char	fileName[256];		//다운로그 result를 기록하는 파일의 이름
	int  alreadyExist;
};

class AGTContentsResult {
public:
	AGTContentsResult() : _contentsType(0), _volume(0), _currentVolume(0), 
							_result(ciFalse), _reason(0), _downState(0) {}
	AGTContentsResult(AGTContentsResult* p);
	virtual ~AGTContentsResult() {}

	const char*	toString();
	ciBoolean	fromString(const char* str);

	const char*	getContentsId() { return _contentsId.c_str(); }
	void	set( const char* contentsId,
				const char* contentsName,
				ciLong contentsType,
				const char* filename,
				const char* location,
				ciULongLong volume,
				ciTime& startTime,
				ciTime& endTime,
				ciULongLong currentVolume,
				ciBoolean result,
				ciLong reason,
				ciLong downState);


	ciString	_contentsId;
	ciString	_contentsName;	
	ciLong		_contentsType;	
	ciString	_filename;		
	ciString	_location;		
	ciULongLong	_volume;			
	ciTime		_startTime; 		
	ciTime		_endTime; 		
	ciULongLong	_currentVolume;	
	ciBoolean	_result;			
	ciLong		_reason;			
	ciLong		_downState;	

	ciString	_strBuffer;
};
typedef list<AGTContentsResult*>	AGTContentsResultList;


class AGTDownloadResult : public AGTData {
public:
	AGTDownloadResult(DownloadProgressMsg* pInfo) ;
	AGTDownloadResult(AGTDownloadResult* pInfo) ;
	AGTDownloadResult();
	virtual ~AGTDownloadResult() { clear();}

	ciBoolean hasMoreContents();
	const char*	toString(int maxNoOfContents);
	ciBoolean	fromString(const char* str);
	void		clear();

	const char*	getProgramId() { return _programId.c_str(); }
	const char*	getHostId() { return _hostId.c_str(); }
	const char*	getSiteId() { return _siteId.c_str(); }
	ciLong		getState() { return _programState; }
	const char*	getProgress() { return _progress.c_str(); }

	const char*	summary();

	const char* getEntity(const char* downloadId);
	const char* getHostEntity();
	const char* getWhereClause();
	void pushContents(AGTContentsResult* ele);
	void pushContents( const char* contentsId,
				const char* contentsName,
				ciLong contentsType,
				const char* filename,
				const char* location,
				ciULongLong volume,
				ciTime& startTime,
				ciTime& endTime,
				ciULongLong currentVolume,
				ciBoolean result,
				ciLong reason,
				ciLong downState);


	void	set(AGTDownloadResult* pCopy);

	ciString	_siteId;
	ciString	_hostId;
	ciShort		_brwId;	
	ciString	_programId;	
	ciString	_progress;
	ciString	_fileName;	
	ciLong		_programState;	
	ciTime		_programStartTime; 		
	ciTime		_programEndTime; 		
	ciShort		_alreadyExist;

protected:

	//ciString	_strBuffer;
	ciMutex					_contentsListLock;
	AGTContentsResultList	_contentsList;


};
typedef list<AGTDownloadResult*>	AGTDownloadResultList;



#endif //_AGTDownloadResult_h_
