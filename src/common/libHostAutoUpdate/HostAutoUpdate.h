#pragma once

#ifdef _HOSTAUTOUPDATE
#else
extern class CString;
#endif


// CServerCheck

//class AFX_HOSTAUTOUPDATE_DLL CHostAutoUpdate
class CHostAutoUpdate
{
public:
	CHostAutoUpdate();
	virtual ~CHostAutoUpdate();

protected:
	BOOL		RequestPost(LPCTSTR lpUrl, CString& strSendMsg, CString& strOutMsg);
	CString		ConvertToURLString(CString str, bool bReturnNullString=false);

	ciString	m_strCenterIP;
	UINT		m_nCenterPort;

public:
	int 		hostAutoUpdate(const char* enterpriseKey, const char* password, const char* siteId, const char* hostId, const char* filename);

	ciString	m_httpFullString;
};
