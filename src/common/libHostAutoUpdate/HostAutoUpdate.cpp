// ServerCheck.cpp : 구현 파일입니다.
//

#include "stdafx.h"

#include <afxinet.h>

#include "ci/libConfig/ciEnv.h"
#include "ci/libBase/ciStringUtil.h"
#include "ci/libBase/ciStringTokenizer.h"

#include "common/libCommon/ubcIni.h"
#include "common/libHttpRequest/HttpRequest.h"

#include "HostAutoUpdate.h"


// CHostAutoUpdate

CHostAutoUpdate::CHostAutoUpdate()
{
	//
	//ubcConfig aIni("UBCConnect");
	ubcIni aIni("UBCConnect", _COP_CD("C:\\Project\\ubc\\config"), "data");

	//
	string center_ip="";
	aIni.get("UBCCENTER","IP", center_ip);
	m_strCenterIP = ( center_ip.length()>0 ? center_ip.c_str() : _T("211.232.57.218") );

	//
	string center_port="";
	aIni.get("UBCCENTER","WEBPORT", center_port);
	m_nCenterPort = ( center_port.length()>0 ? atoi(center_port.c_str()) : 8080 );
}

CHostAutoUpdate::~CHostAutoUpdate()
{
}

int CHostAutoUpdate::hostAutoUpdate(const char* enterpriseKey, const char* password, const char* siteId, const char* hostId, const char* filename)
{
	ciString _fullpath;
	// 
	const char* defaultConfig = _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\config");
	const char* cfg = ciEnv::getenv("CONFIGROOT");
	if(cfg && strlen(cfg))
	{
		_fullpath = cfg;
	}
	else
	{
		_fullpath = defaultConfig;
	}

	_fullpath += "\\data\\";
	_fullpath += filename;
	_fullpath += ".ini";

	//
	CString url;
	url.Format(_T("http://%s:%d/UBC_Manager/host_auto_update.asp"), m_strCenterIP.c_str(), m_nCenterPort);

	CString send_msg;
	send_msg.Format(_T("enterpriseKey=%s&password=%s&siteId=%s&hostId=%s"), CHttpRequest::ToEncodingString(enterpriseKey), CHttpRequest::ToEncodingString(password), CHttpRequest::ToEncodingString(siteId), CHttpRequest::ToEncodingString(hostId) );

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_SERVER);

	CString response = _T("");
	BOOL ret_val = http_request.RequestPost(url, send_msg, response);
	m_httpFullString = url + "?" + send_msg;

	if(ret_val)
	{
		if(response == _T("denied"))
		{
			// wrong password
			return -1;
		}
		else if(response == _T("none"))
		{
			// no item
			CFile file;
			if(file.Open(_fullpath.c_str(), CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
			{
			}
			return 0;
		}
		else if(response.GetLength() > 1 && response.GetAt(0) == _T('o') && response.GetAt(1) == _T('k'))
		{
			CFile file;
			if(file.Open(_fullpath.c_str(), CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
			{
				file.Write((LPCTSTR)response+2, response.GetLength()-2);
			}
			return 0;
		}
	}

	// invalid result
	return -2;
}
