//  InterActLog.h -- Interface for the InterActLog class
//  A class to do debug logging

#ifndef __INTERACTLOG_H__
#define __INTERACTLOG_H__

#include <iostream>
#include <fstream>
using namespace std;

#ifdef _UNICODE
    #define tofstream wofstream 
#else 
    #define tofstream ofstream 
#endif // _UNICODE

#define INTERACT_LOG(msg)	InterActLog::getInstance()->Write msg

class InterActLog
{
// Construction/Destruction
public:
	static InterActLog*	getInstance() ;
	static void clearInstance();

	InterActLog();
	virtual ~InterActLog();

protected:
	BOOL m_bActive;
	CString m_strFileName;
	BOOL m_bTimeStamp;
	
	//skpark 2013.2.21 매번 Open/Close 한다. tomorrow 가 필요없게된다.
	//time_t m_tomorrow; // 다음날 0시

	unsigned short _hour, _minute;

// Operations
public:
	void LogOpen(bool has_comment=false);
	void LogOpen(LPCTSTR fileName);
	void LogClose(bool has_comment=false);

	void CheckLogFile();
	void Write(LPCTSTR format,...);

	CString curTimeStr();
	CString time2Str(time_t tm);
	CString todayStr();
	CString date2Str(time_t tm);

protected:
	tofstream	_log;

	static InterActLog*	_instance;
	static CCriticalSection	_instanceLock;

// Inlines
public:
	inline void SetActive(BOOL bSet)
	{
		m_bActive = bSet;
	}
	inline CString GetFileName()
	{
		return m_strFileName;
	}
};

#endif // __INTERACTLOG_H__</PRE>