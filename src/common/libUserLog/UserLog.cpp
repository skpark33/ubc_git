///////////////////////////////////////////////////////////////////////
//  UserLog.cpp -- Implementation of the UserLog class

#include "stdafx.h"
#include "UserLog.h"
#include "common/libScratch/scPath.h"


#define DELIMITER	_T(",")
#define USERLOG_PATH _T(_UBC_CD("C:\\SQISoft\\UTV1.0\\execute\\log\\"))
#define VARIABLES_INI	_T(_UBC_CD("C:\\SQISoft\\UTV1.0\\execute\\data\\UBCVariables.ini"))

//////////////////////////////////////////////////////
//  Construction/Destruction

CCriticalSection UserLog::_instanceLock;
UserLog* UserLog::_instance = 0; 

UserLog* UserLog::getInstance() {
	if(!_instance) {
		_instanceLock.Lock();
		if(!_instance) {
			_instance = new UserLog;
		}
		_instanceLock.Unlock();
	}
	return _instance;
}

void UserLog::clearInstance() {
	if(_instance) {
		_instanceLock.Lock();
		if(_instance) {
			_instance->LogClose();
			delete _instance;
			_instance =0;
		}
		_instanceLock.Unlock();
	}
}

UserLog::UserLog()
: m_bActive(FALSE), m_bTimeStamp(TRUE), m_strFileName(_T("")), m_tomorrow(0)
{
}

UserLog::~UserLog()
{
}

void UserLog::LogOpen() 
{
	// get PlayLogDayCriteria
	char	szValue[2048];
	memset(szValue, 0x00, sizeof(szValue));
	GetPrivateProfileString("ROOT","PlayLogDayCriteria","",szValue,sizeof(szValue),VARIABLES_INI);
	CString dayCriteria = szValue;
	if (dayCriteria.IsEmpty()) dayCriteria = "00:00";

	// parsing
	unsigned short hour, minute;
	int colonLoc = dayCriteria.Find(":");
	if (colonLoc == -1) {
		hour = atoi(dayCriteria);
		minute = 0;
	} else {
		hour = atoi(dayCriteria.Left(colonLoc));	
		minute = atoi(dayCriteria.Right(dayCriteria.GetLength()-colonLoc-1));
	}
	if (hour >= 24) hour = 0;
	if (minute >= 60) minute = 0;

	// current date
	time_t the_time;
	time(&the_time);
	struct tm m_tm = *localtime(&the_time);

	// set log filename
	if (hour > m_tm.tm_hour 
		|| ( hour == m_tm.tm_hour && minute > m_tm.tm_min)) 
	{
		m_strFileName.Format(_T("%sPlay_%s.csv"), USERLOG_PATH, date2Str(the_time-(3600*24)));
		// set tomorrow
		m_tomorrow = the_time;
	} else {
		m_strFileName.Format(_T("%sPlay_%s.csv"), USERLOG_PATH, date2Str(the_time));
		// set tomorrow
		m_tomorrow = the_time + (3600*24);
	}
	struct tm m_tm2 = *localtime(&m_tomorrow);
	m_tm2.tm_hour = hour;
	m_tm2.tm_min = minute;
	m_tm2.tm_sec = 0;
	m_tomorrow = mktime(&m_tm2);
	
	// log file open
	_log.open(m_strFileName, ios_base::out | ios_base::app);
	if( _log.fail() ) {
		_tprintf(_T("%s open failed.\n"), m_strFileName);
		m_bActive=false;
		return ;
	} 
	_tprintf(_T("%s has opened.\n"), m_strFileName);
	_log //<< "\n--------------------\n"
		<< "\nUserLog has opened."
		//<< "\n--------------------\n"
		<< endl;
	m_bActive=true;
	return;
}

void UserLog::LogOpen(LPCTSTR filename) 
{
	m_strFileName = filename;

	// log file open
	_log.open(m_strFileName, ios_base::out | ios_base::app);
	if( _log.fail() ) {
		_tprintf(_T("%s open failed.\n"), m_strFileName);
		m_bActive=false;
		return ;
	} 
	_tprintf(_T("%s has opened.\n"), m_strFileName);
	_log //<< "\n--------------------\n"
		<< "\nUserLog has opened."
		//<< "\n--------------------\n"
		<< endl;
	m_bActive=true;
	return;
}

void UserLog::LogClose() 
{
	if(m_bActive) {
		_log << "LogTrace has close." << endl;
		_log.close();
	}
	m_bActive = false;
	_tprintf(_T("log has closed.\n"));	
	return;
}

void UserLog::CheckLogFile()
{
	time_t the_time;
	time(&the_time);
	if (m_tomorrow <= the_time) {
		// ���� ����
		LogClose();
		LogOpen();
	}
}

void UserLog::Write(LPCTSTR pFormat,...) 
{
	if(!m_bActive) return;

	TCHAR aBuf[4096];
	va_list	aVp;
	va_start(aVp, pFormat);
	_vstprintf(aBuf, pFormat, aVp);
	va_end(aVp);

	if (m_tomorrow) CheckLogFile();

	CString strTime = curTimeStr();
	_log << strTime
		<< DELIMITER
		<< aBuf 
		<< endl;
}

CString UserLog::curTimeStr() {
	time_t the_time;
	time(&the_time);
	return time2Str(the_time);
}

CString UserLog::time2Str(time_t tm) {
	TCHAR* str;
	struct tm aTM;
#ifdef WIN32
	struct tm *temp = localtime(&tm);
	if (temp) {
		aTM = *temp;
	} else {
		memset((void*)&aTM, 0x00, sizeof(aTM));
	}
#else
	localtime_r(&tm, &aTM);
#endif

	str = (TCHAR*)malloc(sizeof(TCHAR)*20);
	_stprintf(str,_T("%4d/%02d/%02d %02d:%02d:%02d"),
		aTM.tm_year+1900,aTM.tm_mon+1,aTM.tm_mday,
		aTM.tm_hour,aTM.tm_min,aTM.tm_sec);
	CString strTime = str;
	free(str);
	return strTime;
}

CString UserLog::todayStr() {
	time_t the_time;
	time(&the_time);
	return date2Str(the_time);
}

CString UserLog::date2Str(time_t tm) {
	TCHAR* str;
	struct tm aTM;
#ifdef WIN32
	struct tm *temp = localtime(&tm);
	if (temp) {
		aTM = *temp;
	} else {
		memset((void*)&aTM, 0x00, sizeof(aTM));
	}
#else
	localtime_r(&tm, &aTM);
#endif

	str = (TCHAR*)malloc(sizeof(TCHAR)*20);
	_stprintf(str,_T("%4d%02d%02d"),
		aTM.tm_year+1900,aTM.tm_mon+1,aTM.tm_mday);
	CString strTime = str;
	free(str);
	return strTime;
}
