//  UserLog.h -- Interface for the UserLog class
//  A class to do debug logging

#ifndef __USERLOG_H__
#define __USERLOG_H__

#include <iostream>
#include <fstream>
using namespace std;

#ifdef _UNICODE
    #define tofstream wofstream 
#else 
    #define tofstream ofstream 
#endif // _UNICODE

#define PLAYLOG(msg)	UserLog::getInstance()->Write msg

class UserLog
{
// Construction/Destruction
public:
	static UserLog*	getInstance() ;
	static void clearInstance();

	UserLog();
	virtual ~UserLog();

protected:
	BOOL m_bActive;
	CString m_strFileName;
	BOOL m_bTimeStamp;
	time_t m_tomorrow; // ������ 0��

// Operations
public:
	void LogOpen();
	void LogOpen(LPCTSTR fileName);
	void LogClose();

	void CheckLogFile();
	void Write(LPCTSTR format,...);

	CString curTimeStr();
	CString time2Str(time_t tm);
	CString todayStr();
	CString date2Str(time_t tm);

protected:
	tofstream	_log;

	static UserLog*	_instance;
	static CCriticalSection	_instanceLock;

// Inlines
public:
	inline void SetActive(BOOL bSet)
	{
		m_bActive = bSet;
	}
	inline CString GetFileName()
	{
		return m_strFileName;
	}
};

#endif // __USERLOG_H__</PRE>