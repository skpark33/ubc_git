#pragma once

#define CLI_PORT		14999
#define CLI_HEADER		"=[ubc_cli_agent]="

int CliTransStartup();
int CliTransCleanup();
int RemoteCLICall(LPCTSTR szCommand, LPCTSTR strIp, int nPort = CLI_PORT);

class CCTSocket
{
public:
	CCTSocket(const SOCKET s=INVALID_SOCKET, const int af=AF_INET);
	~CCTSocket();

	const SOCKET Open(const int af=AF_INET, const int type=SOCK_STREAM, const int protocol=0);
	int Close();
	int SetOption(int optname, const char * optval, int optlen);
	bool Listen(const int port);
	const SOCKET Accept(struct sockaddr_in& cli_addr);
	int Connect(const char* ip, const int port);
	int Select(fd_set* readfds, fd_set* writefds, fd_set* exceptfds, const struct timeval* timeout);
	int Read(char* buf, const int size, const int flag=0);
	int Write(const char* buf, const int size, const int flag=0);

	const SOCKET GetHandle() const;
	const struct sockaddr_in GetAddress();
	void SetAddress(struct sockaddr_in addr);
	const char* GetIP();
	const int GetPort();
	const DWORD SetTickCount();
	const DWORD CheckTickCount();
	int GetError(char* szError=NULL);
	bool EnableNonBlockSock(bool bEnable);
protected:
public:
protected:
	SOCKET m_hSocket;
	int m_addr_family;
	DWORD  m_dwTickCount;
	struct sockaddr_in m_addr;
};