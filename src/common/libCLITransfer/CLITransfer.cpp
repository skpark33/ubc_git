#include "StdAfx.h"
#include "CLITransfer.h"
#include "CTSocket.h"

CUBCLock CCLITransfer::s_mapLock;
CMapStringToPtr	CCLITransfer::s_mapCLI;

CCLITransfer::CCLITransfer(IResultHandler* pHandler/*=NULL*/)
: m_pHandler(pHandler)
, m_nThreadCnt(0)
, m_nRunThreadCnt(0)
, m_nItemIndex(0)
, m_bStop(false)
{
	m_strCLIID.Format("%u",time(NULL));

	memset(m_szCommand, 0x00, sizeof(m_szCommand));
	::InitializeCriticalSection(&m_csLock);
	CCLITransfer::RegisterCLI(m_strCLIID, this);
}

CCLITransfer::~CCLITransfer(void)
{
	DeleteAllItem();
	DeleteCriticalSection(&m_csLock);
	CCLITransfer::UnRegisterCLI(m_strCLIID);
}

void 
CCLITransfer::setHandler(CCLITransfer::IResultHandler* p) 
{ 
	CUBCGuard alock(&m_myLock); 
	m_pHandler = p; 
}
CCLITransfer::IResultHandler* 
CCLITransfer::getHandler() 
{  
	CUBCGuard alock(&m_myLock); 
	return m_pHandler; 
}
void 
CCLITransfer::setStop(bool p)	
{ 
	CUBCGuard alock(&m_myLock); 
	m_bStop = p;
}
bool 
CCLITransfer::getStop()			
{ 
	CUBCGuard alock(&m_myLock); 
	return m_bStop;
}


// Data 처리
LPCTSTR CCLITransfer::GetCommand()
{
	return m_szCommand;
}

void CCLITransfer::SetCommand(LPCTSTR cmd)
{
	_tcscpy( m_szCommand, cmd );
}

void CCLITransfer::MoveFirst()
{
	::EnterCriticalSection(&m_csLock);

	InterlockedExchange(&m_nItemIndex, 0);

	::LeaveCriticalSection(&m_csLock);
}

CCLITransfer::ITEM* CCLITransfer::GetNextItem()
{
	ITEM* pItem = NULL;

	::EnterCriticalSection(&m_csLock);

	if(m_nItemIndex < 0 || m_nItemIndex >= m_ItemArray.GetCount())
	{
		::LeaveCriticalSection(&m_csLock);
		return NULL;
	}

	pItem = (ITEM*)m_ItemArray[m_nItemIndex++];

	::LeaveCriticalSection(&m_csLock);

	return pItem;
}

int	CCLITransfer::AddItem(ITEM item)
{
	return AddItem(item.szIp, item.nPort);
}

int	CCLITransfer::AddItem(LPCTSTR szIP, int nPort /*= CLI_PORT*/)
{
	ITEM* pItem = new ITEM;
	_tcscpy( pItem->szIp, szIP );
	pItem->nPort = nPort;

	return m_ItemArray.Add(pItem);
}

int	CCLITransfer::AddItem(LPCTSTR szIP,LPCTSTR szArg, int nPort /*= CLI_PORT*/)
{
	ITEM* pItem = new ITEM;
	_tcscpy( pItem->szIp, szIP );
	_tcscpy( pItem->arg, szArg );
	pItem->nPort = nPort;

	return m_ItemArray.Add(pItem);
}

bool CCLITransfer::DeleteItem(int nIndex)
{
	::EnterCriticalSection(&m_csLock);

	if(m_nItemIndex < 0 || m_nItemIndex >= m_ItemArray.GetCount())
	{
		::LeaveCriticalSection(&m_csLock);
		return false;
	}

	ITEM* pItem = (ITEM*)m_ItemArray[nIndex];
	if(pItem) delete pItem;

	m_ItemArray.RemoveAt(nIndex);

	::LeaveCriticalSection(&m_csLock);

	return true;
}

bool CCLITransfer::DeleteAllItem()
{
	::EnterCriticalSection(&m_csLock);

	for(int i=0; i<m_ItemArray.GetCount() ;i++)
	{
		ITEM* pItem = (ITEM*)m_ItemArray[i];
		if(pItem) delete pItem;
	}

	m_ItemArray.RemoveAll();

	::LeaveCriticalSection(&m_csLock);

	return true;
}

int	CCLITransfer::GetCount()
{
	return m_ItemArray.GetCount();
}

// Processing
bool CCLITransfer::Run(int nThreadCnt/*=20*/)
{
	if(IsRunning()) return false;

	m_nItemIndex = 0;
	m_nThreadCnt = nThreadCnt;
	//m_bStop = false;
	setStop(false);
	CWinThread* pThread = AfxBeginThread(&CCLITransfer::RunThread, (LPVOID)this);
	pThread->m_bAutoDelete = true;

	return true;
}

void CCLITransfer::Stop()
{
	//m_bStop = true;
	setStop(true);
}

bool CCLITransfer::IsRunning()
{
	//return (m_nRunThreadCnt > 0 && !m_bStop);
	return (m_nRunThreadCnt > 0 && !getStop());
}

UINT CCLITransfer::RunThread(LPVOID param)
{
	CCLITransfer* pData = (CCLITransfer*)param;
	if(!pData) return 0;

	CPtrArray aThread;

	pData->m_nRunThreadCnt = (pData->GetCount() > pData->m_nThreadCnt ? pData->m_nThreadCnt : pData->GetCount());
	for(int i=0; i<pData->m_nRunThreadCnt ;i++)
	{
		CWinThread* pThread = AfxBeginThread(&CCLITransfer::CliThread, (LPVOID)(LPCSTR)pData->m_strCLIID, 0, 0, CREATE_SUSPENDED);
		pThread->m_bAutoDelete = true;
		aThread.Add(pThread);
	}

	for(int i=0; i<aThread.GetCount() ;i++)
	{
		CWinThread* pThread = (CWinThread*)aThread[i];
		if(pThread)
		{
			pThread->ResumeThread();
		}
	}

	//while(pData->m_nRunThreadCnt > 0 && !pData->m_bStop)
	while(pData->m_nRunThreadCnt > 0 && !pData->getStop())
	{
		Sleep(1000);
	}

	if(pData->getHandler()) pData->getHandler()->CLIFinishEvent();

	return 0;
}

UINT CCLITransfer::CliThread(LPVOID param)
{
	CString id = (LPCSTR)param;
	if(id.IsEmpty()) return 0;

	while(CCLITransfer::IsStop(id)==false)
	{
		ITEM item;
		CString command;
		ITEM* pItem = CCLITransfer::GetNextCommand(id,command,item);
		if(!pItem){
			break;
		}
		if(CCLITransfer::BeforeEvent(id,command,item)){
			item.nResult = RemoteCLICall(command,item.szIp,item.nPort);
			if(CCLITransfer::SetResult(id,pItem,item.nResult)){
				CCLITransfer::AfterEvent(id,item);
			}
		}
	}
	CCLITransfer::Decrement(id);
	return 0;
}


void CCLITransfer::RegisterCLI(CString& cliId, CCLITransfer* obj)
{
	CUBCGuard aGuard(&s_mapLock);
	s_mapCLI.SetAt(cliId,obj);
}
void CCLITransfer::UnRegisterCLI(CString& cliId)
{
	CUBCGuard aGuard(&s_mapLock);
	s_mapCLI.RemoveKey(cliId);
}
bool CCLITransfer::IsRegisterCLI(CString& cliId)
{
	CUBCGuard aGuard(&s_mapLock);
	CCLITransfer* dummy;
	return s_mapCLI.Lookup(cliId, (void*&)dummy);
}

CCLITransfer::ITEM* CCLITransfer::GetNextCommand(CString& cliId, CString& retCommand, ITEM& retItem)
{
	CUBCGuard aGuard(&s_mapLock);
	CCLITransfer* dummy=0;
	if(!s_mapCLI.Lookup(cliId, (void*&)dummy) || dummy==0){
		return false;
	}
	ITEM* pItem = dummy->GetNextItem();
	if(pItem==0){
		return false;
	}
	CString command = dummy->GetCommand();
	if(pItem->arg[0] != 0){  //skpark 2013.4.26 command 에 ip 별로 다른 arg 가 있을 수 있다.
		command.Append(" ");
		command.Append(pItem->arg);
	}
	retCommand = command;
	retItem = *pItem;

	return pItem;
}

bool CCLITransfer::SetResult(CString& cliId, ITEM* pItem, int result)
{
	CUBCGuard aGuard(&s_mapLock);
	CCLITransfer* dummy=0;
	if(!s_mapCLI.Lookup(cliId, (void*&)dummy)|| dummy==0){
		return false;
	}
	if(pItem==0){
		return false;
	}
	pItem->nResult = result;
	return true;
}

bool	CCLITransfer::IsStop(CString& cliId)
{
	CUBCGuard aGuard(&s_mapLock);
	CCLITransfer* dummy=0;
	if(s_mapCLI.Lookup(cliId, (void*&)dummy) && dummy!=0){
		return dummy->getStop();
	}
	return true;
}
bool	CCLITransfer::BeforeEvent(CString& cliId, CString& command, ITEM& item)
{
	CUBCGuard aGuard(&s_mapLock);
	CCLITransfer* dummy=0;
	if(s_mapCLI.Lookup(cliId, (void*&)dummy) && dummy!=0){
		IResultHandler* pHandler = dummy->getHandler();
		if(pHandler){
			pHandler->CLIBeforeEvent(item, command);
			return true;
		};
	}
	return false;
}
bool	CCLITransfer::AfterEvent(CString& cliId, ITEM& item)
{
	CUBCGuard aGuard(&s_mapLock);
	CCLITransfer* dummy=0;
	if(s_mapCLI.Lookup(cliId,(void*&) dummy) && dummy!=0){
		IResultHandler* pHandler = dummy->getHandler();
		if(pHandler){
			pHandler->CLIResultEvent(item);
			return true;
		};
	}
	return false;
}

bool	CCLITransfer::Decrement(CString& cliId)
{
	CUBCGuard aGuard(&s_mapLock);
	CCLITransfer* dummy=0;
	if(s_mapCLI.Lookup(cliId, (void*&)dummy) && dummy!=0){
		InterlockedDecrement(&(dummy->m_nRunThreadCnt));
		return true;
	}
	return false;
}