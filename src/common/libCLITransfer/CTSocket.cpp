#include "stdafx.h"
#include "CTSocket.h"

int CliTransStartup()
{
	WSADATA wsa;
	return WSAStartup(MAKEWORD(2,2), &wsa);
}

int CliTransCleanup()
{
	return WSACleanup();
}

int RemoteCLICall(LPCTSTR szCommand, LPCTSTR strIp, int nPort)
{
	CCTSocket client;

	if(client.Open() == INVALID_SOCKET) return -1;
	if(client.Connect(strIp, nPort) == SOCKET_ERROR) return -1;

	char szPacket[4096] = CLI_HEADER;
	_tcscat(szPacket, szCommand);
	client.Write(szPacket, _tcslen(szPacket));

	SOCKET s = client.GetHandle();
	if(s == INVALID_SOCKET)
	{
		return -1;
	}

	fd_set readset, rset;
	struct timeval tv;
	char buf[4096] = {0};

	FD_ZERO(&readset);
	FD_SET(s, &readset);

	tv.tv_sec = 1;
	tv.tv_usec = 0;
	rset = readset;

	int nSel = 0;
	int nWaitCnt = 30;
	while(nWaitCnt>0)
	{
		nWaitCnt--;
		nSel = client.Select(&rset, NULL, NULL, &tv);

		if(nSel > 0) break;
		else if(nSel == SOCKET_ERROR)
		{
			client.Close();
			return -1;
		}

		Sleep(1000);
	}

	if(nWaitCnt < 0)
	{
		client.Close();
		return -1;
	}

	if(FD_ISSET(s, &rset))
	{
		for(int i = 0; i < nSel; i++)
		{
			memset(buf, 0, sizeof(buf));
			int nResult = client.Read(buf, sizeof(buf), 0);

			if(nResult == SOCKET_ERROR)
			{
				FD_CLR(s, &readset);
			}
			else if(nResult == -1)
			{
				FD_CLR(s, &readset);
			}
			else if(nResult == 0)
			{
				DWORD dwSize;
				ioctlsocket(s, FIONREAD, &dwSize); 
				if(dwSize == 0)
				{
					FD_CLR(s, &readset);
					break;
				}
			}
			else
			{
				client.Close();
				return _ttoi(buf);
			}
		}
	}

	client.Close();

	return 0;
}

CCTSocket::CCTSocket(const SOCKET s, const int af)
{
	m_hSocket = s;
	m_addr_family = af;
	m_dwTickCount = ::GetTickCount();
	memset(&m_addr, 0, sizeof(m_addr));
}

CCTSocket::~CCTSocket()
{
	if(m_hSocket != INVALID_SOCKET)
		Close();
}

const SOCKET CCTSocket::Open(const int af, const int type, const int protocol)
{
	m_addr_family = af;
	m_hSocket = socket(af, type, protocol);
	return m_hSocket;
}

int CCTSocket::Close()
{
	int nr = closesocket(m_hSocket);
	m_hSocket = INVALID_SOCKET;
	return nr;
}

int CCTSocket::SetOption(int optname, const char * optval, int optlen)
{
	return setsockopt(m_hSocket, SOL_SOCKET, optname, optval, optlen);
}

bool CCTSocket::Listen(const int port)
{
	memset((char*)&m_addr, 0, sizeof(m_addr));
	m_addr.sin_family = m_addr_family;
	m_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	m_addr.sin_port = htons(port);

	if (bind(m_hSocket, (struct sockaddr *)&m_addr, sizeof(m_addr)) < 0) return false;

	if(listen(m_hSocket, SOMAXCONN) != 0) return false;
//	if(listen(m_hSocket, 5) != 0) return false;

	return true;
}

const SOCKET CCTSocket::Accept(struct sockaddr_in& cli_addr)
{
	memset((char*)&cli_addr, 0, sizeof(cli_addr));
	int clilen = sizeof(cli_addr);
	return accept(m_hSocket, (struct sockaddr *)&cli_addr, &clilen);
}

int CCTSocket::Connect(const char* ip, const int port)
{
	memset((char*)&m_addr, 0, sizeof(m_addr));
	m_addr.sin_family = m_addr_family;
	m_addr.sin_addr.s_addr = inet_addr(ip);
	m_addr.sin_port = htons(port);
	return connect(m_hSocket, (sockaddr*)&m_addr, sizeof(m_addr));
}

int CCTSocket::Select(fd_set* readfds, fd_set* writefds, fd_set* exceptfds, const struct timeval* timeout)
{
	return select(0, readfds, writefds, exceptfds, timeout);
}

int CCTSocket::Read(char* buf, const int size, const int flag)
{
//	m_dwTickCount = ::GetTickCount();
	return recv(m_hSocket, buf, size, flag);
}

int CCTSocket::Write(const char* buf, const int size, const int flag)
{
	m_dwTickCount = ::GetTickCount();
	return send(m_hSocket, buf, size, flag);
}

const SOCKET CCTSocket::GetHandle() const
{
	return m_hSocket;
}

const struct sockaddr_in CCTSocket::GetAddress()
{
	return m_addr;
}

void CCTSocket::SetAddress(struct sockaddr_in addr)
{
	m_addr = addr;
}

const char* CCTSocket::GetIP()
{
	return inet_ntoa(m_addr.sin_addr);
}

const int CCTSocket::GetPort()
{
	return m_addr.sin_port;
}

const DWORD CCTSocket::SetTickCount()
{
	return (m_dwTickCount = ::GetTickCount());
}

const DWORD CCTSocket::CheckTickCount()
{
	return (::GetTickCount() - m_dwTickCount);
}

int CCTSocket::GetError(char* szError)
{
	int errcode = WSAGetLastError();

	if(szError) sprintf(szError, "%d(0x%x)", errcode, errcode);

	return errcode;
}

bool CCTSocket::EnableNonBlockSock(bool bEnable)
{
#ifdef WIN32

	unsigned long ulParam = (bEnable == true) ? 1 : 0;
	
	if ( ioctlsocket(m_hSocket, FIONBIO, &ulParam) != 0 )
	{
		// error
		return false;
	}

#else

	int nFlags = 0;

	// If the fcntl() function fails, a value of -1 is returned and errno is set to indicate the error.
	nFlags = fcntl(m_hSocket, F_GETFL, 0);

	if ( nFlags < 0 )
	{
		// error
		return false;
	}

	if ( bEnable )
	{
		if ( (nFlags & O_NONBLOCK) == O_NONBLOCK )
			return true;

		if ( fcntl(m_hSocket, F_SETFL, nFlags | O_NONBLOCK) < 0 )
		{
			// error
			return false;
		}

		return true;
	}

	if ( (nFlags & O_NONBLOCK) == O_NONBLOCK )
	{
		if ( fcntl(m_hSocket, F_SETFL, nFlags & ~O_NONBLOCK) < 0 )
		{
			// error
			return false;
		}
	}

#endif	
	return true;
}