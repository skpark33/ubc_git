#pragma once
#include "CTSocket.h"


class CUBCLock {
public:
	CUBCLock()			{::InitializeCriticalSection(&m_Lock); 	}
	virtual ~CUBCLock() {::DeleteCriticalSection(&m_Lock); }

	LPCRITICAL_SECTION get() { return &m_Lock; }
protected:
	CRITICAL_SECTION m_Lock;
};

class CUBCGuard {
public:
	CUBCGuard(CUBCLock* lock) : m_Lock(lock)	{::EnterCriticalSection(m_Lock->get());}
	virtual ~CUBCGuard() { 	::LeaveCriticalSection(m_Lock->get()); }

protected:
	CUBCLock* m_Lock;
};


class CCLITransfer
{
public:
	struct ITEM
	{
		TCHAR	szIp[255];
		int		nPort;
		int		nResult;
		TCHAR	arg[255]; // skpark 2013.4.26 ip 별로 인수가 다른 경우가 있다.

		ITEM()
		{
			memset(szIp, 0x00, sizeof(szIp));
			memset(arg, 0x00, sizeof(arg));
			nPort=CLI_PORT;
			nResult=0;
		}
		ITEM& operator= (const ITEM& info)
		{
			memcpy(szIp,info.szIp,sizeof(info.szIp));
			memcpy(arg,info.arg,sizeof(info.arg));
			nPort = info.nPort;
			nResult=info.nResult;
			return *this;
		}
	};

	// 진행 상태 이벤트
	interface IResultHandler
	{
		virtual void CLIResultEvent(ITEM item) = 0;
		virtual void CLIFinishEvent() = 0;
		virtual void CLIBeforeEvent(ITEM item,LPCTSTR cmd) { };
	};


protected:
	IResultHandler* m_pHandler;

	// Data 및 배열 관련
	CPtrArray m_ItemArray;
	LONG	m_nItemIndex;
	TCHAR	m_szCommand[4096];

	// Thread 관련
	LONG	m_nThreadCnt;
	LONG	m_nRunThreadCnt;
	bool	m_bStop;
	CString m_strCLIID;

	static UINT	RunThread(LPVOID param);
	static UINT	CliThread(LPVOID param);

	static CUBCLock s_mapLock;
	static CMapStringToPtr	s_mapCLI;

	static void		RegisterCLI(CString& cliId, CCLITransfer* p) ;
	static void		UnRegisterCLI(CString& cliId) ;
	static bool		IsRegisterCLI(CString& cliId) ;
	static ITEM*	GetNextCommand(CString& cliId, CString& retCommand, ITEM& retItem);
	static bool		SetResult(CString& cliId, ITEM* pItem, int result);
	static bool		IsStop(CString& cliId);
	static bool		BeforeEvent(CString& cliId, CString& command, ITEM& item);
	static bool		AfterEvent(CString& cliId, ITEM& item);
	static bool		Decrement(CString& cliId);


	CRITICAL_SECTION m_csLock;
	CUBCLock m_myLock;

	// CliThread에서 사용
	void	MoveFirst();
	ITEM*	GetNextItem();

public:
	CCLITransfer(IResultHandler* pHandler=NULL);
	virtual ~CCLITransfer(void);

	void setHandler(IResultHandler* p);
	IResultHandler* getHandler();
	void setStop(bool p);
	bool getStop();

	// 호출명령 관련
	LPCTSTR GetCommand();
	void	SetCommand(LPCTSTR cmd);

	// 대상 호스트 배열 관련
	int		AddItem(ITEM item);
	int		AddItem(LPCTSTR szIP, int nPort = CLI_PORT);
	int		AddItem(LPCTSTR szIP, LPCTSTR szArg,int nPort = CLI_PORT);
	bool	SetItem(int nIndex, ITEM item);
	bool	DeleteItem(int nIndex);
	bool	DeleteAllItem();

	int		GetCount();

	// Processing
	bool	Run(int nThreadCnt=20);

	bool	IsRunning();
	void	Stop();
};
