#ifndef _TimeSyncTimer_H_
#define _TimeSyncTimer_H_

#include <ci/libTimer/ciTimer.h>

class TimeSyncTimer : public ciPollable {
protected:
	int _interval;
	ciString _sttId;
	ciString _timeHost;
	int _timePort;

public:
	TimeSyncTimer();
	virtual ~TimeSyncTimer();

	void setHostInfo(const char* timeHost, int timePort);
	ciBoolean setHostInfoFromIni(); // from UBCConnect.ini

	virtual void initTimer(const char* pname, int interval = 60);
	virtual void processExpired(ciString name, int counter, int interval);
	
	static ciBoolean timeSync(const char* timeHost, int timePort);
	static ciBoolean timeSync();
	ciBoolean connectTest() { return _connectTest(0); };
	ciBoolean connectTest(int timeout) { return _connectTest(timeout); }
	ciBoolean _connectTest(int timeout);



};

#endif //_TimeSyncTimer_H_
