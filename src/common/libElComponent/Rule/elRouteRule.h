/** \class elRouteRule
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elRouteRule_h_
#define _elRouteRule_h_

#include "elRule.h"

class elRouteRule : public elRule
{
public:
    elRouteRule(string strRuleKeyword = EL_KEYWORD_ROUTE_RULE);
    virtual ~elRouteRule(void);

    bool        Set(JsonObject* pRouteObj, elRule* pParentRule, elRule* pComposedRule);
    elRuleIdMap GetChildRuleMap(string keyword = EL_KEYWORD_JOB_RULE);

public:
    string          route_id;
    string          route_type;
    string          route_name;
    vector<string>  create_endpoint_vec;

protected:
    bool        SetJobRule_(JsonArray* pJobRuleArr);

protected:
    JsonObject* p_route_obj_;
    elRule*     p_top_rule_;
    elRule*     p_parent_rule_;

    JsonArray*  p_job_rule_arr_;
    elRuleIdMap job_rule_map_;
    
};

#endif // _elRouteRule_h_