/** \class elRuleRepository
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elRuleRepository_h_
#define _elRuleRepository_h_

#include "elComponentRule.h"

class elRuleRepository
{
public:
	static elRuleRepository* GetInstance(const char* pathFileName = "C:\\SQISoft\\UTV1.0\\execute\\data\\ELiga.cfg");
	static void ClearInstance();

    virtual ~elRuleRepository(void);
    virtual bool    Init(string pathFileName, string eligaRuleName);
    elRule*         GetEligaRule(string keyword, string id);
    elRule*         GetEligaRule(string id);

public:
    string          GetHostName();
    void            SetGlobalValue(string globalName, string globalValue);
    string          GetGlobalValue(string globalName);
    string          SetGlobalPropertyBind(string source); // Trans source -> return target
    string          GetPropertyCodeName(string category, string code);

protected:
    elRuleRepository(void);

    bool            ReadELigaCfg_(string& pathFileName);
    bool            SetRepository_(string& strEligaCfg, string& eligaRuleKeyword) ;
    bool            SetELigaComponentRule_(string& strELigaCfg);

protected:
    string          path_file_name_;
    string          str_eliga_cfg_; 
    string          eliga_component_rule_key_; 
    ciMutex         repo_lock_;

    elComponentRule*    p_eliga_component_rule_;
    JsonObject*         eliga_root_obj_;
    JsonObject*         eliga_component_obj_;

    map<string, string> global_map_;

private:
    static ciMutex           _single_lock;
	static elRuleRepository* _instance;
};

#endif // _elRuleRepository_h_