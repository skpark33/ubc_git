#include "elGatewayRule.h"
#include "elRouteRule.h"

ciSET_DEBUG(5, "elGatewayRule");

elGatewayRule::elGatewayRule(string strRuleKeyword)  
    : p_gateway_rule_obj_(NULL), p_route_rule_arr_(NULL), p_parent_rule_(NULL), p_top_rule_(NULL)
{
    ciDEBUG(5, ("elGatewayRule(%s)", strRuleKeyword.c_str()));
    rule_keyword = strRuleKeyword;
}

elGatewayRule::~elGatewayRule(void)
{
    ciDEBUG(5, ("~elGatewayRule()"));
}

/// Gateway 속성을 설정한다.
/// Route Array를 생성한다.
bool elGatewayRule::Set(JsonObject* pGatewayObj, elRule* pParentRule, elRule* pComposedRule)
{
    ciDEBUG(5, ("Set(elGatewayRule)"));

    JsonValue*  temp_value = NULL;

    if (!pGatewayObj) {
        return false;
    }
    if (!pComposedRule) {
        return false;
    }
    p_gateway_rule_obj_ = pGatewayObj;
    p_parent_rule_ = pParentRule;
    p_top_rule_ = pComposedRule;

    id = gw_id = p_gateway_rule_obj_->getString("GatewayID");
    ciDEBUG(5, ("GatewayID : [%s]", gw_id.c_str()));
    if (gw_id == "") {
        return false;
    }
    type = gw_type = p_gateway_rule_obj_->getString("GatewayType");
    if (gw_type == "") {
        return false;
    }
    name = gw_name = p_gateway_rule_obj_->getString("GatewayName");
    local_ip = p_gateway_rule_obj_->getString("LocalIP");
    if (local_ip == "") {
        local_ip = "127.0.0.1";
    }
    parent_id = (pParentRule == NULL) ? "" : pParentRule->id;

    oper_state = p_gateway_rule_obj_->getString("OperState");
    if (oper_state == "") {
        oper_state = "0";
    }
    admin_state = p_gateway_rule_obj_->getString("AdminState");
    if (admin_state == "") {
        admin_state = "0";
    }

    // Add to ComponentRule!
    p_top_rule_->Add(gw_id, this);

    // RouteRule
    temp_value = p_gateway_rule_obj_->get("RouteArr");
    if (!temp_value->isArray()) {
        return false;
    }
    p_route_rule_arr_ = (JsonArray*) temp_value;

    if (!SetRouteRule_(p_route_rule_arr_)) {
        return false;
    }

    return true;
}


elRuleIdMap elGatewayRule::GetChildRuleMap(string keyword)
{
    if (keyword == EL_KEYWORD_ROUTE_RULE) {
        return route_rule_map_;
    }

    // default
    return route_rule_map_;
}

bool elGatewayRule::SetRouteRule_(JsonArray* pRouteRuleArr)
{
    if (!pRouteRuleArr) {
        ciERROR(("Failed! pRouteRuleArr is NULL "));
        return false;
    }

    ciDEBUG(5, ("SetRouteRule_() RouteArr count : %d ", pRouteRuleArr->getCount() ));

    JsonObject*   route_rule_obj = NULL;
    elRouteRule*  p_route_rule = NULL;

    //elRuleIdMap route_rule_map;
    for (int i = 0; i < pRouteRuleArr->getCount(); i++) {
        route_rule_obj = (JsonObject*) pRouteRuleArr->get(i);
        if (route_rule_obj == NULL) {
            ciERROR(("Failed! pRouteRuleArr[%d] is NULL ", i));
            return false;
        }
        // new elRouteRule
        p_route_rule = new elRouteRule(EL_KEYWORD_ROUTE_RULE);
        if (!p_route_rule->Set(route_rule_obj, this, p_top_rule_)) {
            ciERROR(("Failed! pRouteRuleArr[%d] Route Rule Set", i));
            return false;
        }

        // Make up elEndPointRule Map
        route_rule_map_.insert(elRuleIdMap::value_type(p_route_rule->route_id, p_route_rule));
    }

    return true;
}
