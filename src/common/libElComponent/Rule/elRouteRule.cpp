#include "elRouteRule.h"
#include "elJobRule.h"

ciSET_DEBUG(5, "elRouteRule");

elRouteRule::elRouteRule(string strRuleKeyword)
    : p_route_obj_(NULL), p_parent_rule_(NULL), p_top_rule_(NULL)
{
    ciDEBUG(5, ("elRouteRule()"));
    rule_keyword = strRuleKeyword;
}

elRouteRule::~elRouteRule(void)
{
    ciDEBUG(5, ("~elRouteRule()"));
}

bool elRouteRule::Set(JsonObject* pRouteObj, elRule* pParentRule, elRule* pComposedRule)
{
    ciDEBUG(5, ("Set()"));

    JsonValue*  temp_value = NULL;

    if (!pRouteObj) {
        return false;
    }
    if (!pRouteObj->isObject()) {
        return false;
    }
    p_route_obj_ = pRouteObj;
    p_parent_rule_ = pParentRule;
    p_top_rule_ = pComposedRule;

    id = route_id = p_route_obj_->getString("RouteID");
    if (route_id == "") {
        return false;
    }
    type = route_type = p_route_obj_->getString("RouteType");
    if (route_type == "") {
        return false;
    }
    name = route_name = p_route_obj_->getString("RouteName");
    parent_id = (pParentRule == NULL) ? "" : pParentRule->id;

    oper_state = p_route_obj_->getString("OperState");
    if (oper_state == "") {
        oper_state = "0";
    }
    admin_state = p_route_obj_->getString("AdminState");
    if (admin_state == "") {
        admin_state = "0";
    }

    temp_value = p_route_obj_->get("CreateEndPoint");
    if (!temp_value->isArray()) {
        return false;
    }
    JsonArray* p_array = (JsonArray*) temp_value;
    SetStringVector(p_array, create_endpoint_vec);


    // Add to Top Rule!
    p_top_rule_->Add(route_id, this);

    // RouteRule
    temp_value = p_route_obj_->get("JobArr");
    if (!temp_value->isArray()) {
        return false;
    }
    p_job_rule_arr_ = (JsonArray*) temp_value;

    if (!SetJobRule_(p_job_rule_arr_)) {
        return false;
    }

    return true;
}


elRuleIdMap elRouteRule::GetChildRuleMap(string keyword)
{
    if (keyword == EL_KEYWORD_JOB_RULE) {
        return job_rule_map_;
    }

    // default
    return job_rule_map_;
}

bool elRouteRule::SetJobRule_(JsonArray* pJobRuleArr)
{
    if (!pJobRuleArr) {
        ciERROR(("Failed! pJobRuleArr is NULL "));
        return false;
    }

    ciDEBUG(5, ("SetJobRule_() JobRuleArr count : %d ", pJobRuleArr->getCount() ));

    JsonObject*   job_rule_obj = NULL;
    elJobRule*    p_job_rule = NULL;

    //elRuleIdMap job_rule_map;
    for (int i = 0; i < pJobRuleArr->getCount(); i++) {
        job_rule_obj = (JsonObject*) pJobRuleArr->get(i);
        if (job_rule_obj == NULL) {
            ciERROR(("Failed! pJobRuleArr[%d] is NULL ", i));
            return false;
        }
        // new elJobRule
        p_job_rule = new elJobRule(EL_KEYWORD_JOB_RULE);
        if (!p_job_rule->Set(job_rule_obj, this, p_top_rule_)) {
            ciERROR(("Failed! pJobRuleArr[%d] Job Rule Set", i));
            return false;
        }

        // Make up elJobRule Map
        job_rule_map_.insert(elRuleIdMap::value_type(p_job_rule->job_id, p_job_rule));
    }
    
    return true;
}