#include "elRule.h"

ciSET_DEBUG(5, "elRule");

elRule::elRule(void)
{
}

elRule::~elRule(void)
{
}


JsonObject* elRule::GetChildObject(string keyword, JsonObject* pJsonObj)
{
    ciDEBUG(5, ("GetChildObject(%s)", keyword.c_str()));

    JsonValue*  temp_value = NULL;

    if ((temp_value = pJsonObj->get(keyword.c_str())) == NULL) {
        ciERROR(("Failed! %s can not found ", keyword.c_str()));
        return NULL;
    }
    if (temp_value->isObject() == false) {
        ciERROR(("Failed! GetChildObject is not JsonObject "));
        return NULL;
    }
    return (JsonObject*) temp_value; 
}

JsonArray* elRule::GetChildArray(string keyword, JsonObject* pJsonObj)
{
    ciDEBUG(5, ("GetChildArray(%s)", keyword.c_str()));

    JsonValue*  temp_value = NULL;

    if ((temp_value = pJsonObj->get(keyword.c_str())) == NULL) {
        ciERROR(("Failed! %s can not found ", keyword.c_str()));
        return NULL;
    }
    if (temp_value->isArray() == false) {
        ciERROR(("Failed! GetChildArray value is not JsonArray "));
        return NULL;
    }
    return (JsonArray*) temp_value; 
}

bool elRule::SetStringVector(JsonArray* pStringJsonArr, vector<string>& rOutStringVector)
{
    ciDEBUG(5, ("SetStringVector()"));

    if (!pStringJsonArr) {
        return false;
    }

    string str_temp("");
    for (int i = 0; i < pStringJsonArr->getCount(); i++) {
        str_temp = pStringJsonArr->getString(i);
        rOutStringVector.push_back(str_temp);        
    }

    return true;
}
