/** \class elPropertyRule
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elPropertyRule_h_
#define _elPropertyRule_h_

#include "elRule.h"
#include "common/libElComponent/Base/elMysqlSession.h"

typedef struct _elPropertyBinder {
    string  name; // map key
    string  bind;
    string  value;
    string  select;
} elPropertyBinder;

typedef struct _elPropertyCode {
    string  category; // map key
    string  code; // map key
    string  name;
    string  alias;
    string  group;
} elPropertyCode;

class elPropertyRule : public elRule
{
public:
    elPropertyRule(string strRuleKeyword);
    ~elPropertyRule(void);

    bool        Set(JsonObject* pPropertyObj, elRule* pParentRule, elRule* pComposedRule);    
    string      BindProperty(string query);
    string      GlobalBindProperty(string query);
    string      GetCodeName(string category, string code);

public:
    string          property_id;
    string          property_type;
    string          property_name;
    string          data_source;

    map<string, elPropertyBinder>   prop_bind_map; // <elPropertyBinder.name, elPropertyBinder>
    map<string, elPropertyCode>     prop_code_map; // <elPropertyCode.category_code elPropertyCode>

protected:
    bool            Start_();
    void            Stop_();
    bool            SetPropertyBinder_(JsonArray* pArrayObj);    
    bool            SetGlobalPropertyBinder_(JsonArray* pArrayObj);
    bool            SetPropertyCode_(JsonArray* pArrayObj);

protected:
    JsonObject*     p_property_obj_;
    JsonArray*      p_binder_array_;
    JsonArray*      p_code_array_;
    elRule*         p_top_rule_;
    elRule*         p_parent_rule_;

    elMysqlSession  db_session_;

    bool            is_ep_db;
    string          ep_type_;
    string          name_;
    string          user_;
    string          pass_;
    string          host_;
    long            port_; 
    string          url_;

    ciMutex         lock_;

};

#endif //_elPropertyRule_h_