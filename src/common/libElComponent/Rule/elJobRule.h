/** \class elJobRule
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elJobRule_h_
#define _elJobRule_h_

#include "elRule.h"

class elJobRule : public elRule
{
public:
    elJobRule(string strRuleKeyword = EL_KEYWORD_JOB_RULE);
    virtual ~elJobRule(void);

    bool        Set(JsonObject* pJobObj, elRule* pParentRule, elRule* pComposedRule);
    elRuleIdMap GetChildRuleMap(string keyword);

public:
    string          job_id;
    string          job_type;
    string          job_name;
    string          schedule;

protected:
    bool            SetCommandRule_(string strCommandKeyword, JsonArray* pCommandRuleArr);

protected:
    JsonObject*     p_job_obj_;
    elRule*         p_top_rule_;
    elRule*         p_parent_rule_;

    JsonArray*      p_from_rule_arr_;
    JsonArray*      p_to_rule_arr_;
    elRuleIdMap     from_rule_map_;
    elRuleIdMap     to_rule_map_;
};

#endif // _elJobRule_h_