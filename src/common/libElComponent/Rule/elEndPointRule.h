/** \class elEndPointRule
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elEndPointRule_h_
#define _elEndPointRule_h_

#include "elRule.h"

class elEndPointRule : public elRule
{
public:
    elEndPointRule(string strRuleKeyword = EL_KEYWORD_ENDPOINT_RULE);
    virtual ~elEndPointRule(void);

    virtual bool Set(JsonObject* pEndPointObj, elRule* pParentRule, elRule* pComposedRule);

public:
    string      ep_id;
    string      ep_type;
    string      ep_name;
    string      group;
    string      user;
    string      pass;
    string      host;
    long        port;
    string      url;
    string      protocol;
    string      return_type;
    string      redirect_url;

protected:
    JsonObject* p_endpoint_obj_;
    elRule*     p_top_rule_;
    elRule*     p_parent_rule_;
};

#endif // _elEndPointRule_h_