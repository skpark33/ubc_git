/** \class elComponentRule
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elComponentRule_h_
#define _elComponentRule_h_

#include "elRule.h"

class elComponentRule : public elRule
{
public:
    elComponentRule(string strRuleKeyword = EL_KEYWORD_COMPONENT_RULE);
    virtual ~elComponentRule(void);

    virtual bool    Set(JsonObject* pComponentRule, elRule* pParentRule, elRule* pComposedRule);
    virtual void    Add(string id, elRule* pRule);
    virtual void    Remove(string keyword, string id);
    elRule*         GetRule(string keyword, string id);
    elRule*         GetRule(string id);

public:
    string          component_id;
    string          component_type;
    string          component_name;

protected:
    bool            SetDefineRuleConstant_(JsonObject* pDefRuleType);
    bool            SetEndPointRule_(JsonArray* pEndPointArr);
    bool            SetPropertyRule_(JsonArray* pPropertyArr);
    bool            SetGatewayRule_(JsonArray* pGatewayArr);

    void            ClearDefineConstVMap_(elDefineConstVMap& defConstVMap);
    void            ClearRuleKeyMMap_(elRuleKeyMMap& comRuleIdMMap);

protected:
    JsonObject*     p_component_obj_;
    elRule*         p_top_rule_;
    elRule*         p_parent_rule_;

    //[ ELigaGatewayRule Repository
    elDefineConstVMap      define_const_vmap_;
    elRuleKeyMMap          component_rule_mmap_;
    elRuleIdMap            component_rule_id_map_;

    string                 path_file_name_;
    string                 str_eliga_cfg_;    

    ciMutex                repo_lock_;
};

#endif // _elComponentRule_h_