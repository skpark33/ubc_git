/** \class elRule
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elRule_h_
#define _elRule_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>
#include "common/libElComponent/Base/JsonBase.h"

/// Top Keyword
#define EL_NULL_RULE         ""
#define EL_ROOT_RULE         "ELigaRootRule"
#define EL_COMPONENT_RULE    "ELigaComponentRule"

/// ELigaComponentRule Keyword
#define EL_KEYWORD_DEFAULT_RULE             "DefaultRule"
#define EL_KEYWORD_COMPONENT_RULE           "ELigaComponentRule"
#define EL_KEYWORD_DEFINE_RULE_CONSTANT     "DefineRuleConstant"
#define EL_KEYWORD_ENDPOINT_RULE            "ELigaEndPoint"
#define EL_KEYWORD_PROPERTY_RULE            "ELigaProperty"
#define EL_KEYWORD_GATEWAY_RULE             "ELigaGateway"
#define EL_KEYWORD_ROUTE_RULE               "RouteArr"
#define EL_KEYWORD_JOB_RULE                 "JobArr"
#define EL_KEYWORD_FROM_COMMAND_RULE        "FromCommand"
#define EL_KEYWORD_TO_COMMAND_RULE          "ToCommand"

/// ELigaComponentRule::DefineRuleConstant Keyword
#define EL_COM_DFC_COMP_KEY            "ComponentKeyword"
#define EL_COM_DFC_EP_TYPE             "EndPointType"
#define EL_COM_DFC_COMP_TYPE           "ComponentType"
#define EL_COM_DFC_COMP_ID             "ComponentID"
#define EL_COM_DFC_ROUTE_TYPE          "RouteType"
#define EL_COM_DFC_JOB_TYPE            "JobType"

class elRule
{
public:    
    virtual ~elRule(void);

    virtual bool    Set(JsonObject* pMyObj, elRule* pParentRule, elRule* pComposedRule) = 0;
    virtual void    Add(string id, elRule* pRule) {}
    virtual void    Remove(string id) {}
    virtual elRule* GetRule(string id) { return NULL; }

    JsonObject*     GetChildObject(string keyword, JsonObject* pJsonObj);
    JsonArray*      GetChildArray(string keyword, JsonObject* pJsonObj);
    bool            SetStringVector(JsonArray* pStringJsonArr, vector<string>& rOutStringVector);

public: // member
    string  id;
    string  parent_id;
    string  type;
    string  name;
    string  rule_keyword;
    int     seq;
    string  oper_state;
    string  admin_state;

protected:
    elRule(void);
};

typedef vector<string>                elRuleConstVec;       // "ELigaEndPoint", "ELigaGateway"
typedef map<string, elRuleConstVec>	  elDefineConstVMap;  // <"GatewayType", vector<"elDbGateway">>
typedef map<string, elRule*>          elRuleIdMap;       // <id, elRule> : <"OFS_DB_GW_1", elRule*>
typedef map<string, elRuleIdMap>      elRuleKeyMMap;    // <RuleKeyword, elRuleIdMap> : <"ELigaGateway", <"OFS_DB_GW_1", elRule*>>


/// RouteTypeArr �ڷ���
/***
class elRouteTypePack
{
public:
    elRouteTypePack() : route_type("") {}
    elRouteTypePack(string& routeType, vector<string>& jobTypeArr) { 
        route_type = routeType;
        job_type_arr = jobTypeArr;
    }
    ~elRouteTypePack() {}

    void Set(string& routeType, vector<string>& jobTypeArr) { 
        route_type = routeType;
        job_type_arr.clear();
        job_type_arr = jobTypeArr;
    }

public:
    string          route_type;
    vector<string>  job_type_arr;
};

/// Job�� From�� To�� Property �ڷ���
class elJobPropertyPack
{
public:
    elJobPropertyPack() : end_point(""), entity(""), command("") {}
    elJobPropertyPack(string& endPoint, string& strEntity, vector<string>& fieldArr, string& strCommand, vector<string>& cmdArgv) {
        end_point = endPoint;
        entity = strEntity;
        field_arr = fieldArr;
        command = strCommand;
        cmd_argv = cmdArgv;
    }
    ~elJobPropertyPack() {}

    void Set(string& endPoint, string& strEntity, vector<string>& fieldArr, string& strCommand, vector<string>& cmdArgv) {
        end_point = endPoint;
        entity = strEntity;
        field_arr.clear();
        field_arr = fieldArr;
        command = strCommand;
        cmd_argv.clear();
        cmd_argv = cmdArgv;
    }

public:
    string          end_point;
    string          entity;
    vector<string>  field_arr;
    string          command;
    vector<string>  cmd_argv;
};
***/

#endif // _elRule_h_