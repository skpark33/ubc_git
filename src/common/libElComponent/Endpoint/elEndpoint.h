/** \class elEndpoint
 *  Copyright ⓒ 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief elEndpoint는 기본적으로 타이머와 쓰레드를 담고 있다.
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elEndpoint_h_
#define _elEndpoint_h_

#include "common/libElComponent/Component/elComponent.h"
#include "common/libElComponent/Rule/elEndPointRule.h"
#include "common/libElComponent/Base/elMySQLDriver.h"

class elEndpoint : public elComponent
{
public:
    elEndpoint(void);
    virtual ~elEndpoint(void);

    // 향후 Submit에 PasingRule을 추가한다.
    virtual bool Submit(string el_command_type, string el_command_id, string str_command, string& entity, elRecord& field_name_arr, elRecordList& out_records) = 0;

    virtual void    ProcessExpired(string name, int counter, int interval_sec, int interval_msec=0);
    virtual void    Update();
    virtual bool    Init(string my_id);
    virtual void    End();
    virtual bool    Start(string option = ADMIN_STATE_START_NO);
    virtual bool    Stop();

protected:
    void    SetChildComponent_();

protected:
    elRuleRepository*       p_rule_repository_;
    elEndPointRule*         p_my_rule_;
    elRuleIdMap             child_rule_map_;
    vector<elComponent*>    child_vec_;
        
};

#endif //_elEndpoint_h_