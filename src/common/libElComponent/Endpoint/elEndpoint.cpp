#include "elEndpoint.h"

ciSET_DEBUG(5, "elEndpoint");

elEndpoint::elEndpoint(void) : p_rule_repository_(NULL), p_my_rule_(NULL)
{
    ciDEBUG(5, ("elEndPoint()"));
}

elEndpoint::~elEndpoint(void)
{
    ciDEBUG(5, ("~elEndPoint()"));
}

void elEndpoint::ProcessExpired(string name, int counter, int interval_sec, int interval_msec) 
{
    ciDEBUG(5, ("ProcessExpired(%s, %d, %d, %d)", name.c_str(), counter, interval_sec, interval_msec));
}

void elEndpoint::Update() 
{
    ciDEBUG(5, ("Update()"));
}

bool elEndpoint::Init(string my_id)
{
    ciDEBUG(5, ("InitEndpoint(%s)", my_id.c_str()));

    return false;
}

void elEndpoint::End()
{
    ciDEBUG(5, ("EndEndpoint(%s)"));

}

bool elEndpoint::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    return true;
}

bool elEndpoint::Stop()
{
    ciDEBUG(5, ("Stop()"));

    return true;
}

