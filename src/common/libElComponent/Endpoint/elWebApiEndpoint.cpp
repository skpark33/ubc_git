#include "elWebApiEndpoint.h"
#include "common/libElComponent/Base/JsonBase.h"
#include <ci/libBase/ciStringUtil.h>

ciSET_DEBUG(5, "elWebApiEndpoint");

elWebApiEndpoint::elWebApiEndpoint(void)
{
}

elWebApiEndpoint::~elWebApiEndpoint(void)
{
}

// ELigaEndPoint에 선언된 URL은 하나만 등록 될 수 있어 WEBAPI등 같은 타입의 Endpoint를 생성해도 여러 URL을 사용할 수 가 없다. 
// 일단 url을 el_command_type에 따라 Submit메서드의 entity 필드에서 읽을 수 있도록 임시 조치한다.
bool elWebApiEndpoint::Submit(string el_command_type, string el_command_id, string str_command, string& entity, elRecord& field_name_arr, elRecordList& records)
{
    ciDEBUG(5, ("Submit(%s, %s, %s)", el_command_type.c_str(), el_command_id.c_str(), str_command.c_str()));

    string out_str_result("");
    bool ret_val;

    entity_ = entity;
    field_name_arr_ = field_name_arr;

    if (el_command_type == "WEB_POST") {
        webapi_session_.InitSession(host_, entity, port_);
        if (!webapi_session_.SendPostResult(str_command, out_str_result)) {
            ciERROR(("Failed! Submit:SendPostResult WEB_POST"));
            return false;
        }
    } else if (el_command_type == "WEB_GET") {
        webapi_session_.InitSession(host_, entity, port_);
        if (!webapi_session_.SendGetResult(str_command, out_str_result)) {
            ciERROR(("Failed! Submit:SendGetResult WEB_GET"));
            return false;
        }
    } else if (el_command_type == "WEB_EVENT_POST") {
        webapi_session_.InitSession(host_, entity, port_);
        //Content-Type: application/json 추가
        string strHeader("");
        if (return_type_ == "JSON") {
            //strHeader = "Content-Type: application/x-www-form-urlencoded\r\nContent-Type: application/json\r\nUser-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
            strHeader = "Content-Type: application/json\r\nUser-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
        } else if(return_type_ == "XML") {
            strHeader = "Content-Type: application/xml\r\nUser-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
        }
        if (!webapi_session_.SendPostResult(str_command, out_str_result, strHeader)) {
            ciERROR(("Failed! Submit:SendPostResult WEB_EVENT_POST"));
            return false;
        }
        return true;
    } else {
        ciERROR(("Unknown command type : %s, %s", el_command_type.c_str(), el_command_id.c_str() ));
        return false;
    }

    
    if (return_type_ == "JSON") {
        ret_val = ParseJsonResult_(out_str_result, records);
    } else if (return_type_ == "XML") {
        ret_val = ParseXmlResult_(out_str_result, records);
    } else {
        ciERROR(("Unknown return type : %s", return_type_.c_str(), el_command_id.c_str() ));
        ret_val = false;
    }

    return ret_val;
}

bool elWebApiEndpoint::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    // elCommand마다 초기화 할 수 있기 때문에 같은 id로 이미 초기화 했으면 다시 안한다.
    if (IsInit() == true && my_id == id_) {
        ciDEBUG(5, ("Already Init(%s)", my_id.c_str()));
        return true;
    }

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elEndPointRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        is_init_ = false;
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;
    
    group_ = p_my_rule_->group;
    user_ = p_my_rule_->user;
    pass_ = p_my_rule_->pass;
    host_ = p_my_rule_->host;
    port_ = p_my_rule_->port;
    url_  = p_my_rule_->url; 
    protocol_ = p_my_rule_->protocol;
    return_type_ = p_my_rule_->return_type;
    ciStringUtil::safeToUpper(return_type_);

    webapi_session_.InitSession(host_, url_, port_);
    
    is_init_ = true;
    return true;
}

void elWebApiEndpoint::End()
{
    ciDEBUG(5, ("End(%s)", id_.c_str()));

    Stop();
    is_init_ = false;
}

bool elWebApiEndpoint::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    if (is_start_ == true) {
        ciDEBUG(5, ("Already Start(%s)", id_.c_str()));
        return true;
    }

    if (option == ADMIN_STATE_START_NO) {
        is_start_ = true;
        return true;
    } else if (option == ADMIN_STATE_START_ALL) {
        InitTimer("elEndpoint Timer", 60);
        StartTimer();
        StartThread();
    } else if (option == ADMIN_STATE_START_TIMER) {
        StartTimer();
    } else if (option == ADMIN_STATE_START_THREAD) {
        StartThread();
    } else if (option == ADMIN_STATE_START_CHILD) {
    } else {
        ciERROR(("Failed! Start option(%s)", option.c_str()));
        return false;
    }

    is_start_ = true;
    return true;
}

bool elWebApiEndpoint::Stop()
{
    ciDEBUG(5, ("Stop()"));

    if (is_start_ == false) {
        ciDEBUG(5, ("Already Stop(%s)", id_.c_str()));
        return true;
    }

    StopTimer();
    StopThread();

    // Stop child
    for (unsigned int i = 0; i < child_vec_.size(); i++) {
        child_vec_[i]->Stop();
    }

    is_start_ = false;

    return true;
}

bool elWebApiEndpoint::ParseJsonResult_(string& str_result, elRecordList& records)
{
    ciDEBUG(5, ("ParseResult_(%s)", str_result.c_str() ));

    JsonValue* temp_value = NULL;
    JsonObject* post_result_obj = NULL;
    JsonArray* record_array = NULL;

    string resultMsg;
    string resultFlag;
    string entity;
    string siteId;
    string attrNames;

    JsonValue* json_value = JsonValue::create(str_result.c_str(), 0, (int) str_result.length());
    if (json_value == NULL) {
        ciERROR(("Failed! ParseJsonResult_:str_result, JsonValue::create "));
        return false;
    }

    if (json_value->isObject() == false) {
        ciERROR(("Failed! ParseJsonResult_:json_value is not JsonObject "));
        delete json_value;
        return false;
    }
    post_result_obj = (JsonObject*) json_value;

    resultMsg = post_result_obj->getString("resultMsg");
    resultFlag = post_result_obj->getString("resultFlag");
    entity = post_result_obj->getString("entity");
    //siteId = post_result_obj->getString("siteId");

    if (resultFlag == "false") {
        ciERROR(("Failed! JsonValue is NULL "));
        delete post_result_obj;
        return false;
    }

    temp_value = post_result_obj->get("records");
    if (!temp_value) {
        ciERROR(("Failed! JsonValue is NULL "));
        delete post_result_obj;
        return false;
    } else if (!temp_value->isArray()) {
        ciERROR(("Failed! ParseJsonResult_:post_result_obj is not JsonArray "));
        delete post_result_obj;
        return false;
    }

    if(!GetRecordList_((JsonArray*) temp_value, records)) {
        delete post_result_obj;
        return false;
    }
    
    delete post_result_obj;
    return true;
}

bool elWebApiEndpoint::ParseXmlResult_(string& str_result, elRecordList& records)
{
    ciDEBUG(5, ("ParseXmlResult_(%s)", str_result.c_str() ));
    return true;
}


bool elWebApiEndpoint::GetRecordList_(JsonArray* pStringArrArr, elRecordList& records)
{
    ciDEBUG(5, ("GetRecordList_()"));

    string str_temp("");
    JsonValue* temp_value = NULL;
    JsonArray* json_record = NULL;
    
    for (int i = 0; i < pStringArrArr->getCount(); i++) {
        temp_value = pStringArrArr->get(i);
        if (!temp_value) {
            ciERROR(("Failed! JsonValue(%d) is NULL ", i));
            return false;
        } else if (!temp_value->isArray()) {
            ciERROR(("Failed! record(%d) is not JsonArray ", i));
            return false;
        }

        elRecord record;
        if (!GetRecord_((JsonArray*) temp_value, record)) {
            ciERROR(("Failed! GetRecord_(%d)", i));
            return false;
        }
        records.push_back(record);        
    }
    ciDEBUG(5, ("End GetRecordList_(Record size:%d)", records.size()));

    return true;
}

bool elWebApiEndpoint::GetRecord_(JsonArray* pStringArr, elRecord& record)
{
    ciDEBUG(5, ("GetRecord_(%s)", pStringArr->toString().c_str()));

    string str_temp("");
    JsonValue* temp_value = NULL;
    JsonString* temp_string = NULL;
    
    if (field_name_arr_.size() != pStringArr->getCount()) {
        ciERROR(("Failed! FieldName size[%d] != Record field size[%d] ", field_name_arr_.size(), pStringArr->getCount()));
        return false;
    }

    for (int i = 0; i < pStringArr->getCount(); i++) {
        temp_value = pStringArr->get(i);
        if (!temp_value) {
            ciERROR(("Failed! JsonValue[%d] is NULL ", i));
            return false;
        } else if (!temp_value->isString()) {
            ciERROR(("Failed! record[%d] is not JsonString ", i));
            return false;
        }

        temp_string = (JsonString*) temp_value;
        const char* sz_temp = NULL;
        sz_temp = temp_string->getString();
        if (!sz_temp) {
            ciERROR(("Failed! getString sz_temp[%d] is NULL ", i));
            return false;
        }
        str_temp = sz_temp;

        elField field;
        field.name = field_name_arr_[i].name;
        field.type = field_name_arr_[i].type;
        field.value = str_temp;
        field.is_exist = true;

        //default converting
        if (field.value == "") {
            if (field.type == EL_FIELD_TYPE_DATETIME) {
                field.value = "1970-01-01 00:00:00";
            } else if (field.type != EL_FIELD_TYPE_VAR_STRING) {
                if (field.type != EL_FIELD_TYPE_STRING) {
                    field.value = "NULL";
                }
            }
        } else {
            // 따옴표 이스케이프 처리
            string quote("'");
            string escapeQuote("''");
            ciStringUtil::stringReplace(field.value, quote.c_str(), escapeQuote.c_str());
        }
        record.push_back(field);
    }
    ciDEBUG(5, ("End GetRecord_(Field size:%d)", record.size()));
    return true;
}
