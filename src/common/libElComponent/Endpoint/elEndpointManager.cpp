#include "elEndpointManager.h"

ciSET_DEBUG(5, "elEndpointManager");


ciMutex elEndpointManager::_single_lock;
elEndpointManager* elEndpointManager::_instance = 0;

elEndpointManager* elEndpointManager::GetInstance()
{
	if (!_instance) {
		CI_GUARD(_single_lock, "GetInstance()");
		if (!_instance) {
			_instance = new elEndpointManager;
		}
    }
	return _instance;
}

void elEndpointManager::ClearInstance()
{
	if (_instance) {
		CI_GUARD(_single_lock, "getInstance()");
		if (_instance) {
			delete _instance;
			_instance = 0;
		}
	}
}

elEndpointManager::elEndpointManager(void) 
{
    ciDEBUG(5, ("elEndpointManager()"));
}

elEndpointManager::~elEndpointManager(void)
{
    ciDEBUG(5, ("~elEndpointManager()"));

    m_endpoint_map_.clear();
}

void elEndpointManager::AddEndpoint(string endpoint_id, elEndpoint* endpoint)
{
    ciDEBUG(5, ("AddEndpoint(%s)", endpoint_id.c_str()));

    if (!endpoint) {
        ciERROR(("AddEndpoint() endpoint is NULL!"));
        return;
    }

    if (endpoint_id == "") {
        ciERROR(("AddEndpoint() Empty endpoint ID!"));
        return;
    }

    {
    ciGuard guard(m_endpoint_map_lock_);

    m_endpoint_map_[endpoint_id] = endpoint;
    }
}

void elEndpointManager::DelEndpoint(string endpoint_id)
{
    ciDEBUG(5, ("DelEndpoint(%s)", endpoint_id.c_str()));
    if (endpoint_id == "") {
        ciERROR(("GetEndpoint() Empty endpoint ID!"));
        return;
    }


    ciGuard guard(m_endpoint_map_lock_);

	map<string, elEndpoint*>::iterator itr = m_endpoint_map_.find(endpoint_id);
	if(itr != m_endpoint_map_.end()) {
        ciDEBUG(5, ("DelEndpoint(%s) not exist!", (itr->first).c_str()));
        // delete는 하지않는다.
        m_endpoint_map_.erase(itr);
	}

}

elEndpoint* elEndpointManager::GetEndpoint(string endpoint_id)
{
    ciDEBUG(5, ("GetEndpoint(%s)", endpoint_id.c_str()));

    if (endpoint_id == "") {
        ciERROR(("GetEndpoint() Empty endpoint ID!"));
        return NULL;
    }

    ciGuard guard(m_endpoint_map_lock_);

	map<string, elEndpoint*>::iterator itr = m_endpoint_map_.find(endpoint_id);
	if(itr == m_endpoint_map_.end()) {
        ciDEBUG(5, ("GetEndpoint(%s) not exist!", endpoint_id.c_str()));
		return NULL;
	}
    return itr->second;
}

