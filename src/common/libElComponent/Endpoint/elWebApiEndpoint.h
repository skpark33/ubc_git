/** \class elMysqlEndpoint
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief elMysqlEndpoint 
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2016/01/20 11:01:00
 */

#ifndef _elWebApiEndpoint_h_
#define _elWebApiEndpoint_h_

#include "elEndpoint.h"
#include "common/libElComponent/Base/elMySQLDriver.h"
#include "common/libElComponent/Base/elHttpSession.h"

class elWebApiEndpoint : public elEndpoint
{
public:
    elWebApiEndpoint(void);
    ~elWebApiEndpoint(void);

    virtual bool Submit(string el_command_type, string el_command_id, string str_command, string& entity, elRecord& field_name_arr, elRecordList& records);

    bool    Init(string my_id);
    void    End();
    bool    Start(string option = ADMIN_STATE_START_NO);
    bool    Stop();

protected:
    bool    ParseJsonResult_(string& str_result, elRecordList& records);
    bool    ParseXmlResult_(string& str_result, elRecordList& records);
    bool    GetRecordList_(JsonArray* pStringJsonArr, elRecordList& records);
    bool    GetRecord_(JsonArray* pStringArr, elRecord& record);

protected:
    ciMutex		    queue_lock_;

    string          group_;
    string          user_;
    string          pass_;
    string          host_;
    ciLong          port_;    
    string          url_;
    string          protocol_;
    string          return_type_;

    string          entity_;
    elRecord        field_name_arr_;

    elHttpSession   webapi_session_;
};

#endif //_elWebApiEndpoint_h_