/** \class elEndpointManager
 *  Copyright ⓒ 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief elEndpointManager
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2016/03/29 11:01:00
 */

#ifndef _elEndpointManager_h_
#define _elEndpointManager_h_

#include "elEndpoint.h"

class elEndpointManager
{
public:
    static  elEndpointManager*  GetInstance();
    static  void                ClearInstance();

    virtual     ~elEndpointManager(void);

    void        AddEndpoint(string endpoint_id, elEndpoint* endpoint);
    void        DelEndpoint(string endpoint_id);
    elEndpoint* GetEndpoint(string endpoint_id);

protected:
    elEndpointManager(void);

protected:
    // 외부에서 주입된 Endpoint
    // key : endpoint_id
    map<string, elEndpoint*>  m_endpoint_map_; 
    ciMutex                   m_endpoint_map_lock_;

private:
    static ciMutex              _single_lock;
    static elEndpointManager*   _instance;
    
};

#endif // _elEndpointManager_h_