/** \class elJob
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elJob_h_
#define _elJob_h_

#include "elComponent.h"
#include "elCommand.h"

#include <ci/libBase/ciTime.h>

#define MAX_SECONDS  61 // index 0 - 59, 60 per sec(*)
#define MAX_MINUTES  61 // index 0 - 59, 60 per min(*)
#define MAX_HOURS    25 // index 0 - 23, 24 per hour(*)
#define MAX_MDAYS    32 // index 1 - 31, 32 per mday(*)
#define MAX_WDAYS     8 // index 0 - 6, 0 = Sunday, 7 per wday(*)
#define MAX_MONTHS   13 // index 1 - 12, 13 per mon(*)

typedef struct _elScheduleData {
    bool sec  [MAX_SECONDS];
    bool min  [MAX_MINUTES];
    bool hour [MAX_HOURS];
    bool mday [MAX_MDAYS];
    bool wday [MAX_WDAYS];
    bool mon  [MAX_MONTHS];
} elScheduleData;

class elJob : public elComponent
{
public:
    elJob(void);
    virtual ~elJob(void);

    void    ProcessExpired(string name, int counter, int interval_sec, int interval_msec=0);
    void    Update();
    bool    Init(string my_id);
    void    End();
    bool    Start(string option = ADMIN_STATE_START_ALL);
    bool    Stop();
    void    run();

    void    Execute();

public:
    string  schedule;

protected:
    void    SetChildComponent_();
    void    SetSchedule_(string sch);
    void    DoSchedule_();
    void    Release_();

protected:
    elRuleRepository*   p_rule_repository_;
    elJobRule*          p_my_rule_;
    elRuleIdMap         from_command_rule_map_;
    elRuleIdMap         to_command_rule_map_;
    vector<elCommand*>  from_command_vec_;
    vector<elCommand*>  to_command_vec_;

    elScheduleData      schedule_tm_;
    ciTime              executed_time_;
    bool                is_execute_first_;
};

#endif // _elJob_h_