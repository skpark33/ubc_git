#include "elTimer.h"
#include "elComponent.h"

elTimer::elTimer(const char* pTimerName, elComponent* polling_component) : name_(pTimerName), 
        polling_component_(polling_component), counter_(0), interval_msec_(60*1000), started_(false) 
{
} 

elTimer::~elTimer() 
{ 
    StopTimer();
}

void elTimer::StartTimer() 
{
    StopTimer();
    started_ = true;
    start();
}

void elTimer::StopTimer() 
{
    if(started_) {
	    started_ = false;
	    stop(); 
    }
}

void elTimer::SetInterval(int interval_sec, int interval_msec) 
{
    interval_sec_ = interval_sec; 
    interval_msec_= interval_msec;
}


void elTimer::run() 
{
    while(started_) {
        ::Sleep(interval_sec_*1000);
        ::Sleep(interval_msec_);
        counter_++;
        if(polling_component_) {
            polling_component_->ProcessExpired(name_, counter_, interval_sec_, interval_msec_);
        }
    }
}
