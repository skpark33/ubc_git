#include "elRoute.h"

ciSET_DEBUG(5, "elRoute");

elRoute::elRoute(void)
{
    ciDEBUG(5, ("elRoute()"));
}

elRoute::~elRoute(void)
{
    ciDEBUG(5, ("~elRoute()"));
    End();
}

void elRoute::ProcessExpired(string name, int counter, int interval_sec, int interval_msec)
{
    ciDEBUG(5, ("ProcessExpired(%s, %d, %d, %d)", name.c_str(), counter, interval_sec, interval_msec));

}

void elRoute::Update()
{
    ciDEBUG(5, ("Update()"));
}

bool elRoute::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    if (IsInit() == true && my_id == id_) {
        ciDEBUG(5, ("Already Init(%s)", my_id.c_str()));
        return true;
    }

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elRouteRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        is_init_ = false;
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;

    job_rule_map_ = p_my_rule_->GetChildRuleMap(EL_KEYWORD_JOB_RULE);

    is_init_ = true;

    return true;
}

void elRoute::End()
{
    ciDEBUG(5, ("End()"));

    Stop();
    Release_();
    is_init_ = false;
}

bool elRoute::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    if (is_start_ == true) {
        ciDEBUG(5, ("Already Start(%s)", id_.c_str()));
        return true;
    }

    if (option == ADMIN_STATE_START_NO) {
        // Do noting
    } else if (option == ADMIN_STATE_START_ALL) {
        //InitTimer("elRouteTimer", 60);
        //StartTimer();
        //StartThread();        
    } else {
        ciWARN(("--- %s No Start - option(%s)", id_.c_str(), option.c_str()));
        return false;
    }
    SetChildComponent_();

    oper_state_ = "1";
    is_start_ = true;

    return true;
}

bool elRoute::Stop()
{
    ciDEBUG(5, ("Stop()"));

    if (is_start_ == false) {
        ciDEBUG(5, ("Already Stop(%s)", id_.c_str()));
        return true;
    }

    StopTimer();
    StopThread();

    // Stop child
    for (unsigned int i = 0; i < job_vec_.size(); i++) {
        job_vec_[i]->Stop();
    }

    oper_state_ = "0";
    is_start_ = false;

    return true;
}

void elRoute::SetChildComponent_()
{
    ciDEBUG(5, ("SetChildComponent_()"));

	elRuleIdMap::iterator itr;
	for(itr = job_rule_map_.begin(); itr != job_rule_map_.end(); itr++) {
		elRule* ele = itr->second;
        elJob* job = new elJob();
        job->Init(ele->id);
        job->Start(ele->admin_state);
        job_vec_.push_back(job);
	}
}

void elRoute::Release_()
{
    ciDEBUG(5, ("Release_()"));

    for (size_t i = 0; i < job_vec_.size(); i++) {
        delete job_vec_[i];
    }
    job_rule_map_.clear();
}