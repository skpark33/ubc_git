#include "elJob.h"


ciSET_DEBUG(5, "elJob");

elJob::elJob(void) : is_execute_first_(true)
{
}

elJob::~elJob(void)
{
    End();
}

void elJob::ProcessExpired(string name, int counter, int interval_sec, int interval_msec)
{
    ciDEBUG(5, ("ProcessExpired(%s, %d, %d, %d)", name.c_str(), counter, interval_sec, interval_msec));
}

void elJob::Update()
{
    ciDEBUG(5, ("Update()"));
}

bool elJob::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    if (IsInit() == true && my_id == id_) {
        ciDEBUG(5, ("Already Init(%s)", my_id.c_str()));
        return true;
    }

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elJobRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        is_init_ = false;
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;

    schedule = p_my_rule_->schedule;
    SetSchedule_(schedule);

    from_command_rule_map_ = p_my_rule_->GetChildRuleMap(EL_KEYWORD_FROM_COMMAND_RULE);
    to_command_rule_map_   = p_my_rule_->GetChildRuleMap(EL_KEYWORD_TO_COMMAND_RULE);

    is_init_ = true;

    return true;
}

void elJob::End()
{
    ciDEBUG(5, ("End()"));

    Stop();
    Release_();
    is_init_ = false;
}

bool elJob::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    if (is_start_ == true) {
        ciDEBUG(5, ("Already Start(%s)", id_.c_str()));
        return true;
    }

    if (option == ADMIN_STATE_START_NO) {
        // Do noting
    } else if (option == ADMIN_STATE_START_ALL) {
        //InitTimer("elJob Timer", 60);
        //StartTimer();
        StartThread();        
    } else {
        ciWARN(("--- %s No Start - option(%s)", id_.c_str(), option.c_str()));
        return false;
    }
    SetChildComponent_();
    ciWARN(("+++ %s Start - option(%s)", id_.c_str(), option.c_str()));

    oper_state_ = "1";
    is_start_ = true;

    return true;
}

bool elJob::Stop()
{
    ciDEBUG(5, ("Stop()"));

    if (is_start_ == false) {
        ciDEBUG(5, ("Already Stop(%s)", id_.c_str()));
        return true;
    }

    StopTimer();
    StopThread();

    // Stop child
    for (unsigned int i = 0; i < from_command_vec_.size(); i++) {
        from_command_vec_[i]->Stop();
    }
    for (unsigned int i = 0; i < to_command_vec_.size(); i++) {
        to_command_vec_[i]->Stop();
    }

    oper_state_ = "0";
    is_start_ = false;

    return true;
}

void elJob::SetChildComponent_()
{
    ciDEBUG(5, ("SetChildComponent_()"));

    string  from_endpoint("");

	elRuleIdMap::iterator itr;
    // FromCommand 객체 vector를 생성한다.
	for(itr = from_command_rule_map_.begin(); itr != from_command_rule_map_.end(); itr++) {
		elRule* ele = itr->second;
        elCommand* command = new elCommand();
        command->Init(ele->id);
        from_endpoint = command->endpoint_id;
        command->Start(ele->admin_state);
        from_command_vec_.push_back(command);
	}
    ciDEBUG(5, ("from_command_vec_ size(%d)", from_command_vec_.size()));
    // ToCommand 객체 vector를 생성한다.
	for(itr = to_command_rule_map_.begin(); itr != to_command_rule_map_.end(); itr++) {
		elRule* ele = itr->second;
        elCommand* command = new elCommand();

        command->Init(ele->id);
        //if (from_endpoint == command->end_point && ) {
        //    // from_endpoint 와 to_endpoint가 같은면 원천 데이터를 지울수 있으므로 Start하지 않는다.
        //    ciERROR(("Failed! Equal from_endpoint[%s] == to_endpoint[%s]", from_endpoint.c_str(), command->end_point.c_str()));
        //    continue;
        //}
        command->Start(ele->admin_state);
        to_command_vec_.push_back(command);
	}
    ciDEBUG(5, ("to_command_vec_ size(%d)", to_command_vec_.size()));
}

void elJob::run()
{
    ciDEBUG(5, ("run() "));

    while(is_thread_start_) {
        DoSchedule_();
        Sleep(500); // 500ms
    }
    ciDEBUG(5, ("run return #######################... "));
}

void elJob::SetSchedule_(string sch)
{
    ciDEBUG(5, ("SetSchedule_(%s) ", sch.c_str()));

    // Schedule Format
    // "sec min hour mday wday mon"
    // "* 0,5,10,15,20,25,30,35,40,45,50,55 * * * *"
    //
    vector<string> scheduleVec;    
    ciStringUtil::Tokenizer(schedule.c_str(), " ", &scheduleVec);

    vector<string> secVec;    
    vector<string> minVec;
    vector<string> hourVec;
    vector<string> mdayVec;
    vector<string> wdayVec;
    vector<string> monVec;
    
    ciStringUtil::Tokenizer(scheduleVec[0].c_str(), ",", &secVec);
    ciStringUtil::Tokenizer(scheduleVec[1].c_str(), ",", &minVec);
    ciStringUtil::Tokenizer(scheduleVec[2].c_str(), ",", &hourVec);
    ciStringUtil::Tokenizer(scheduleVec[3].c_str(), ",", &wdayVec);
    ciStringUtil::Tokenizer(scheduleVec[4].c_str(), ",", &mdayVec);
    ciStringUtil::Tokenizer(scheduleVec[5].c_str(), ",", &monVec);
    
    memset(&schedule_tm_, 0, sizeof(schedule_tm_));
    // sec    
    for (unsigned int i = 0; i < secVec.size(); i++) {
        if (secVec[i] == "*") {
            schedule_tm_.sec[MAX_SECONDS-1] = true;
        } else {
            int idx = atoi(secVec[i].c_str());
            if (idx >= 0 && idx <= 59) {
                schedule_tm_.sec[idx] = true;
            }            
        }
    }

    // min
    for (unsigned int i = 0; i < minVec.size(); i++) {
        if (minVec[i] == "*") {
            schedule_tm_.min[MAX_MINUTES-1] = true; // 60
        } else {
            int idx = atoi(minVec[i].c_str());
            if (idx >= 0 && idx <= 59) {
                schedule_tm_.min[idx] = true;
            }            
        }
    }

    // hour
    for (unsigned int i = 0; i < hourVec.size(); i++) {
        if (hourVec[i] == "*") {
            schedule_tm_.hour[MAX_HOURS-1] = true; // 24
        } else {
            int idx = atoi(hourVec[i].c_str());
            if (idx >= 0 && idx <= 23) {
                schedule_tm_.hour[idx] = true;
            }            
        }
    }

    // mday
    for (unsigned int i = 0; i < mdayVec.size(); i++) {
        if (mdayVec[i] == "*") {
            schedule_tm_.mday[MAX_MDAYS-1] = true; // 32
        } else {
            int idx = atoi(mdayVec[i].c_str());
            if (idx >= 1 && idx <= 31) {
                schedule_tm_.mday[idx] = true;
            }            
        }
    }

    // wday
    for (unsigned int i = 0; i < wdayVec.size(); i++) {
        if (wdayVec[i] == "*") {
            schedule_tm_.wday[MAX_WDAYS-1] = true; // 7
        } else {
            int idx = atoi(wdayVec[i].c_str());
            if (idx >= 0 && idx <= 6) {
                schedule_tm_.wday[idx] = true;
            }            
        }
    }

    // mon
    for (unsigned int i = 0; i < monVec.size(); i++) {
        if (monVec[i] == "*") {
            schedule_tm_.mon[MAX_MONTHS-1] = true; // 13
        } else {
            int idx = atoi(monVec[i].c_str());
            if (idx >= 1 && idx <= 12) {
                schedule_tm_.mon[idx] = true;
            }            
        }
    }

}

void elJob::DoSchedule_()
{
    ciTime cur_time;
    
    if (schedule_tm_.sec[MAX_SECONDS-1] == true || schedule_tm_.sec[cur_time.getSecond()] == true) {
        ciDEBUG(5, ("%s DoSchedule(%s) %d sec", id_.c_str(), schedule.c_str(), cur_time.getSecond()));
    } else {
        return;
    }
    if (schedule_tm_.min[MAX_MINUTES-1] == true || schedule_tm_.min[cur_time.getMinute()] == true) {
        ciDEBUG(5, ("%s DoSchedule(%s) %d min", id_.c_str(), schedule.c_str(), cur_time.getMinute()));
    } else {
        return;
    }
    if (schedule_tm_.hour[MAX_HOURS-1] == true || schedule_tm_.hour[cur_time.getHour()] == true) {
        ciDEBUG(5, ("%s DoSchedule(%s) %d hour", id_.c_str(), schedule.c_str(), cur_time.getHour()));
    } else {
        return;
    }
    if (schedule_tm_.mday[MAX_MDAYS-1] == true || schedule_tm_.mday[cur_time.getDay()] == true) {
        ciDEBUG(5, ("%s DoSchedule(%s) %d mday", id_.c_str(), schedule.c_str(), cur_time.getDay()));
    } else {
        return;
    }
    if (schedule_tm_.wday[MAX_WDAYS-1] == true || schedule_tm_.wday[cur_time.getWday()] == true) {
        ciDEBUG(5, ("%s DoSchedule(%s) %d wday", id_.c_str(), schedule.c_str(), cur_time.getWday()));
    } else {
        return;
    } 
    if (schedule_tm_.mon[MAX_MONTHS-1] == true || schedule_tm_.mon[cur_time.getMonth()] == true) {
        ciDEBUG(5, ("%s DoSchedule(%s) %d mon", id_.c_str(), schedule.c_str(), cur_time.getMonth()));
    } else {
        return;
    } 

    // 정해진 시간에 연속 실행되지 않도록 실행시간 기록
    if (is_execute_first_ || !(executed_time_ == cur_time)) {
        Execute();
        
        executed_time_ = cur_time;
        is_execute_first_ = false;
        ciDEBUG(5, ("[%s] %s Executed DoSchedule(%s) ", cur_time.getTimeString().c_str(), id_.c_str(), schedule.c_str()));
    }    
}

void elJob::Execute()
{
    ciDEBUG(5, ("Execute(%s) JOB ", id_.c_str() ));
    
    elRecordList record_list;
    for (unsigned int i = 0; i < from_command_vec_.size(); i++) {
        
        // FromCommand Execute
        ciWARN(("Execute(%s) JOB ", id_.c_str() ));
        if (from_command_vec_[i]->Execute(record_list)) {
            // 2016-04-04 일단 레코드결과가 0이라도 에러가 아니라면 from_command로 넘긴다. 
            //            실제 elEndpoint에서 레코드0인 경우 로직에 맞게 처리하라.
            //            프로그램들이 죽는지 확인할 것
            //if (record_list.size() == 0) {
            //    // 첫 FromCommand의 레코드결과가 0이면 연결된 FromCommand를 실행하지 않아야 한다.
            //    ciDEBUG(5, ("Record list is 0 ! "));
            //    return;
            //}
            ciDEBUG(5, ("Record list is %d ! ", record_list.size() ));
            // FromCommand로 부터 실행해야 할 복수개의 ToCommand 리스트를 실행한다.
            for (unsigned int j = 0; j < to_command_vec_.size(); j++) {
                // parter_command_id 와 같으면 실행한다.
                if (from_command_vec_[i]->command_id == to_command_vec_[j]->partner_command_id) {
                    // FromCommand와 ToCommand의 대상이 같으면 데이터를 덮어쓰므로 피한다. end_point와 entity 둘중하나는 틀려야 한다.
                    if ( (from_command_vec_[i]->endpoint_id != to_command_vec_[j]->endpoint_id)
                      || (from_command_vec_[i]->entity    != to_command_vec_[j]->entity) ) 
                    {
                        // ToCommand Execute
                        to_command_vec_[j]->Execute(record_list);
                    }
                }
                ciDEBUG(5, ("%s from_command_vec_[%d]->command_id(%s) : to_command_vec_[%d]->partner_command_id(%s) "
                    , id_.c_str(), i, from_command_vec_[i]->command_id.c_str(), j, to_command_vec_[j]->partner_command_id.c_str()));
                ciDEBUG(5, ("%s from_command_vec_[%d]->end_point(%s) : to_command_vec_[%d]->end_point(%s) "
                    , id_.c_str(), i, from_command_vec_[i]->endpoint_id.c_str(), j, to_command_vec_[j]->endpoint_id.c_str()));
                ciDEBUG(5, ("%s from_command_vec_[%d]->entity(%s) : to_command_vec_[%d]->entity(%s) "
                    , id_.c_str(), i, from_command_vec_[i]->entity.c_str(), j, to_command_vec_[j]->entity.c_str()));

            }
        }
        record_list.clear();
        ciWARN(("Execute(%s) JOB Command[%s] ", id_.c_str(), from_command_vec_[i]->command_id.c_str() ));

    }
    ciDEBUG(5, ("End Execute() "));
}

void elJob::Release_()
{
    ciDEBUG(5, ("Release_()"));

    for (size_t i = 0; i < from_command_vec_.size(); i++) {
        delete from_command_vec_[i];
    }
    from_command_rule_map_.clear();

    for (size_t i = 0; i < to_command_vec_.size(); i++) {
        delete to_command_vec_[i];
    }
    to_command_rule_map_.clear();

}