#include <ci/libBase/ciTime.h>

#include "common/libElComponent/Rule/elPropertyRule.h"
#include "common/libElComponent/Endpoint/elMysqlEndpoint.h"
#include "common/libElComponent/Endpoint/elWebApiEndpoint.h"
#include "common/libElComponent/Endpoint/elEndpointManager.h"
#include "elCommand.h"

#include <ctype.h>

ciSET_DEBUG(5, "elCommand");

elCommand::elCommand(void)
{
    is_start_ = false;
    
    p_rule_repository_ = NULL;
    p_my_rule_ = NULL;
    p_endpoint_rule_ = NULL;
    p_endpoint_ = NULL;
}

elCommand::~elCommand(void)
{
    ciDEBUG(5, ("~elCommand()"));
    End();
}

void elCommand::ProcessExpired(string name, int counter, int interval_sec, int interval_msec)
{
    ciDEBUG(5, ("ProcessExpired(%s, %d, %d, %d)", name.c_str(), counter, interval_sec, interval_msec));
}

void elCommand::Update()
{
    ciDEBUG(5, ("Update()"));
}

bool elCommand::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    if (IsInit() == true && my_id == id_) {
        ciDEBUG(5, ("Already Init(%s)", my_id.c_str()));
        return true;
    }

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elCommandRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        is_init_ = false;
        return false;
    }
    id_ = command_id = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;

    endpoint_id = p_my_rule_->end_point;
    entity = p_my_rule_->entity;
    partner_command_id = p_my_rule_->partner_command_id;
    //fleld_arr_vec = p_my_rule_->field_arr_vec;
    
    ciDEBUG(5, ("Init fields Size(%d)(%d)", p_my_rule_->field_arr_vec.size(), p_my_rule_->field_type_vec.size()));
    if (p_my_rule_->field_arr_vec.size() != p_my_rule_->field_type_vec.size()) {
        ciERROR(("Failed! fields Size(%d)(%d)", p_my_rule_->field_arr_vec.size(), p_my_rule_->field_type_vec.size() ));
        is_init_ = false;
        return false;
    }

    for (size_t i = 0; i < p_my_rule_->field_arr_vec.size(); i++) {
        elField field;
        field.value = p_my_rule_->field_arr_vec[i];        
        field.type = p_my_rule_->field_type_vec[i];
        field.is_exist = true;
        field_name_arr_.push_back(field);
        //ciDEBUG(5, ("Init fields(%d)(%s)(%s)", i, field.value.c_str(), field.type.c_str()));
    }
    command = p_my_rule_->command;
    property_id = p_my_rule_->property_id;
    argv_vec = p_my_rule_->argv_vec;

    // EndPointType 을 확인하여 elEndPoint 객체를 생성한다.
    // 향후 endpoint id나 type을 넘겨 Endpoint Factory나 싱글턴으로 생성하도록 수정한다.
    p_endpoint_rule_ = (elEndPointRule*) p_rule_repository_->GetEligaRule(endpoint_id);

    // 2016-03-29 주석처리, 
    // endpoint의 생성을 하지 않는다.
    if (p_endpoint_rule_->type == "MYSQL") {
        p_endpoint_ = new elMysqlEndpoint;
    } else if (p_endpoint_rule_->type == "WEBAPI") {
        p_endpoint_ = new elWebApiEndpoint;
    } else {
        // 2016-03-29
        // endpoint의 생성은 client에서 생성하여 elEndpointManager로 Add하고 객체들은 ID로 Get한다.
        elEndpointManager* endpoint_manager = elEndpointManager::GetInstance();
        p_endpoint_ = endpoint_manager->GetEndpoint(endpoint_id);
    }

    // endpoint 초기화하고 시작한다.
    if (!p_endpoint_->Init(endpoint_id)) {
        ciERROR(("Failed! Init Endpoint(%s)", endpoint_id.c_str()));
        is_init_ = false;
        return false;
    }
    if (!p_endpoint_->Start()) {
        ciERROR(("Failed! Start Endpoint(%s)", endpoint_id.c_str()));
        is_init_ = false;
        return false;
    }

    is_init_ = true;
    return true;
}

void elCommand::End()
{
    ciDEBUG(5, ("End()"));

    Stop();
    Release_();
    is_init_ = false;
}


bool elCommand::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    if (option == ADMIN_STATE_START_NO) {
        // Do noting
    } else if (option == ADMIN_STATE_START_ALL) {
    } else if (option == ADMIN_STATE_START_TIMER) {
    } else if (option == ADMIN_STATE_START_THREAD) {
    } else if (option == ADMIN_STATE_START_CHILD) {
    } else {
        ciWARN(("--- %s No Start - option(%s)", id_.c_str(), option.c_str()));
        return false;
    }
    ciWARN(("+++ %s Start - option(%s)", id_.c_str(), option.c_str()));
    is_start_ = true;

    return true;
}

bool elCommand::Stop()
{
    ciDEBUG(5, ("Stop()"));

    // Stop Timer
    // Stop Thread
    is_start_ = false;

    return true;
}

/// record_list는 FromCommand 실행 결과를 ToCommand로 전달하는 인자
bool elCommand::Execute(elRecordList& record_list)
{    
    ciDEBUG(5, ("Execute(%s)", command_id.c_str()));
    ciGuard guard(execute_lock_);
    ciDEBUG(5, ("Execute Guard "));

    string str_raw_command("");
    string str_exec_command("");
    p_rule_repository_ = elRuleRepository::GetInstance();
    elPropertyRule* pPropertyRule = NULL;
    elPropertyRule* pGlobalPropertyRule = NULL;

    if (!is_start_) {
        ciDEBUG(5, ("No started CommandID(%s)", command_id.c_str()));
        return false;
    }

    if (property_id != "") {
        pPropertyRule = (elPropertyRule*) p_rule_repository_->GetEligaRule(property_id);
    }
    pGlobalPropertyRule = (elPropertyRule*) p_rule_repository_->GetEligaRule("GLOBAL_PROPERTY");

    // GLOBAL_PROPERTY 바인딩 #{value} 바인딩Value가 없으면 Command 바뀌는 것 없음.
    if (pGlobalPropertyRule) {
        str_raw_command = pGlobalPropertyRule->GlobalBindProperty(command);
    }
    if (str_raw_command == "") {
        ciDEBUG(5, ("No Command string! CommandID(%s)", command_id.c_str()));
        return false;
    }

    // DEFINED PROPERTY 바인딩 #{value} 바인딩Value가 없으면 Command 바뀌는 것 없음.
    if (pPropertyRule) {
        str_raw_command = pPropertyRule->BindProperty(str_raw_command);            
    }    
    if (str_raw_command == "") {
        ciDEBUG(5, ("No Command string! CommandID(%s)", command_id.c_str()));
        return false;
    }

    // [2016-03-02, RepeatBlock 처리, #[*], #[1] ValueBinder들을 실행결과가 있는 경우 record의 value 순서대로 대치 시킨다.
    // elMysqlEndpoint는 Value 바인딩을 직접 처리하므로 일단 WebApiEndpoint 일 경우만 처리한다.
    // 차기 버전에선 elCommand에서 Pasing하도록 통일한다. 
    if (p_endpoint_->GetType() == "WEBAPI") {
        int ret_val = -1;
        if(ret_val = SetExecCommand(str_raw_command, record_list, str_exec_command) <= 0) {
            ciERROR(("SetExecCommand Failed! ret_val(%d) RawCommand(%s) ", ret_val, str_raw_command.c_str()));
            return false;
        }
    } else {
        // EndPoint가 MYSQL인 경우 RawCommand 그대로 넘긴다.
        str_exec_command = str_raw_command;
    }

    // ]

    ciTime curTime;
    ciWARN2(("##### Execute Start! CommandType(%s) CommandID(%s) EndPointType(%s) RecordList(%d) \n exec Command(%s)"
        , type_.c_str(), command_id.c_str(), p_endpoint_->GetType().c_str(), record_list.size(), str_exec_command.c_str()));

    bool b_ret = p_endpoint_->Submit(this->type_, this->id_, str_exec_command, entity, field_name_arr_, record_list);
    
    if (b_ret) {
        ciWARN2(("##### Execute Success! EXEC StartTime(%s)! CommandType(%s) CommandID(%s) RecordList(%d) \n exec Command(%s)", curTime.getTimeString().c_str() , type_.c_str(), command_id.c_str(), record_list.size(), str_exec_command.c_str()));
    } else {
        ciWARN2(("##### Execute Failed! EXEC StartTime(%s)! CommandType(%s) CommandID(%s) RecordList(%d) \n exec Command(%s)", curTime.getTimeString().c_str() , type_.c_str(), command_id.c_str(), record_list.size(), str_exec_command.c_str()));
    }
    return b_ret;
}

int elCommand::SetExecCommand(string str_raw_command, elRecordList& records, string& str_exe_command_out)
{
    ciDEBUG(5, ("SetExecCommand(%s)", str_raw_command.c_str()));

    if (str_raw_command == "") {
        ciERROR(("SetCommand() raw command is NULL!!!"));
        return -1;
    }

    int ret_num = 0;
    string str_temp_command = str_raw_command;
    string str_repeat_block("");
    string command_front("");
    string command_end("");
    ciStringVector repeat_block_vec;

    do {
        command_front = "";
        command_end = "";

        if (!ExtractRepeatBlock_(str_temp_command, command_front, str_repeat_block, command_end)) {
            ciERROR(("Failed ExtractRepeatBlock_()"));
            return -2;
        }

        // [ repeat block 만 binding 처리
        if (str_repeat_block != "") {
            if (!IncreaseCommandRecords_(str_repeat_block, records, repeat_block_vec)) {
                ciERROR(("Failed IncreaseCommandRecords_()"));
                return -3;
            }
            // ]

            str_temp_command = command_front;
            for (size_t i = 0; i < repeat_block_vec.size(); i++) {
                str_temp_command += repeat_block_vec[i];
                if ((i + 1) < repeat_block_vec.size()) {
                    str_temp_command += ",";
                }
            }
            str_temp_command += command_end;
            repeat_block_vec.clear();
        }
    } while(str_repeat_block != "");
    ciDEBUG(5, ("Before BindCommandValueAtRecords_... str_temp_command(%s)", str_temp_command.c_str()));

    // 전체 binding 처리는 repeat block을 모두 binding 처리 후 마지막에 한다.
    if (0 > BindCommandValueAtRecords_(records, str_temp_command)) {
        ciERROR(("Failed BindCommandValueAtRecords_()"));
        return -4;
    }
    
    str_exe_command_out = str_temp_command;

    return (int) str_exe_command_out.size();
}

// return : @RepeatBlockStart에서 @RepeatBlockEnd 까지의 문자열
bool elCommand::ExtractRepeatBlock_(string& str_raw_command, string& command_front_out, string& str_repeat_block, string& command_end_out)
{
    ciDEBUG(5, ("ExtractRepeatBlock_"));

    if (str_raw_command == "") {
        ciERROR(("Failed ExtractRepeatBlock_(). str_raw_command empty"));
        return false;
    }

    size_t start_pos = 0;
    size_t start_tail_pos = 0;
    size_t pos = 0;
    size_t end_pos = 0;
    size_t end_tail_pos = 0;
    string findString("");
    str_repeat_block = "";

    if ((start_pos = str_raw_command.find("@RepeatBlockStart")) != string::npos) {
        start_tail_pos = start_pos + 17;

        //@RepeatBlockEnd가 없으면 에러
        end_pos = str_raw_command.find("@RepeatBlockEnd", start_tail_pos);
        if (end_pos == string::npos) {
            command_front_out = "";
            command_end_out = "";
            str_repeat_block = "";
            ciERROR(("Failed ExtractRepeatBlock_(). Not exist @RepeatBlockEnd"));
            return false;
        }
        end_tail_pos = end_pos + 15;

        str_repeat_block = str_raw_command.substr(start_tail_pos, end_pos - start_tail_pos);
        command_front_out = str_raw_command.substr(0, start_pos);
        command_end_out   = str_raw_command.substr(end_tail_pos, str_raw_command.size() - end_tail_pos);
    } else {
        // @RepeatBlockStart가 없는 경우
        // @RepeatBlockEnd가 있으면 에러
        end_pos = str_raw_command.find("@RepeatBlockEnd", start_tail_pos);
        if (end_pos != string::npos) {
            ciERROR(("Failed ExtractRepeatBlock_(). Not exist @RepeatBlockStart"));
            return false;
        }
        str_repeat_block = "";
        command_front_out = str_raw_command;
        command_end_out   = "";
    }
    ciDEBUG(5, ("find start_pos=%d, end_pos=%d, command_front_out(%s), str_repeat_block(%s), command_end_out(%s)", start_pos, end_pos, command_front_out.c_str(), str_repeat_block.c_str(), command_end_out.c_str()));

    return true;
}

// record 갯수 만큼 반복하여 텍스트를 생성
bool elCommand::IncreaseCommandRecords_(string& str_nobind_block, elRecordList& records, ciStringVector& repeat_block_vec_out)
{
    ciDEBUG(5, ("IncreaseCommandRecords_(%s, %d)", str_nobind_block.c_str(), records.size()));

    string str_bind_cmd("");

    for (unsigned int i = 0; i < records.size(); i++) {
        str_bind_cmd = str_nobind_block;
        if (!BindCommandValue_(records[i], str_bind_cmd)) {
            ciERROR(("Failed BindCommandValue_()"));
            return false;
        }

        repeat_block_vec_out.push_back(str_bind_cmd);
        str_bind_cmd = "";
    }
    
    return true;
}

int elCommand::BindCommandValue_(elRecord& record, string& str_bind_command_inout)
{
    ciDEBUG(5, ("BindCommandValue_(%d)", record.size()));

    if (str_bind_command_inout == "") {
        return 0;
    }

    size_t start_pos = 0;
    size_t start_tail_pos = 0;
    size_t end_pos = 0;
    size_t end_tail_pos = 0;
    size_t delim_pos = 0;

    string findString("");
    string str_idx("");
    string str_row_idx("");
    string str_col_idx("");
    string str_temp("");
    int    idx = 0;

    if (str_bind_command_inout == "") {
        return -1;
    }

    // #[number] 형식 확인  
    int i = 0;
    ciDEBUG(5, ("Original : (%s) \n", str_bind_command_inout.c_str()));
    while ((start_pos = str_bind_command_inout.find("#[")) != string::npos) {
        start_tail_pos = start_pos+2;
        end_pos = str_bind_command_inout.find("]", start_tail_pos);
        if (end_pos == string::npos) {
            ciERROR(("str_bind_command_inout not exist ']' ", idx));
            return -2;
        }

        str_idx = str_bind_command_inout.substr(start_tail_pos, end_pos-start_tail_pos);
        delim_pos = str_idx.find(",");
        if (delim_pos == string::npos) {
            str_row_idx = "*";
        } else {
            str_row_idx = str_idx.substr(0, delim_pos);
        }    
        str_col_idx = str_idx.substr(delim_pos+1);

        findString = str_bind_command_inout.substr(start_pos, end_pos-start_pos+1);
        str_bind_command_inout.erase(start_pos, findString.length());

        // 이 함수에서는 row_idx는 무시한다.
        str_temp = "";
        if (str_col_idx == "*") {
            for (size_t j = 0; j < record.size(); j++) {
                if (record[j].value != "NULL") {
                    //str_temp += "'" + record[j].value + "'";
                    str_temp += record[j].value;
                } else {
                    //str_temp += record[j].value;
                    str_temp += "";
                }
                if (j != record.size()-1) {
                    str_temp += ",";
                }
            }
            str_bind_command_inout.insert(start_pos, str_temp);
            ciDEBUG(5, ("ReplaceString #[*] => %s\n", str_temp.c_str()));
        } else {
            idx = atoi(str_col_idx.c_str()) - 1;
            if (idx > (int) record.size() - 1) {
                ciERROR(("BindCommandValue_ idx=%d is too long", idx));
                return -3;
            } 
            if (idx < 0) {
                ciERROR(("BindCommandValue_ idx=%d is less than 0", idx));
                return -4;
            }

            if (record[idx].value != "NULL") {
                //str_temp = "'" + record[idx].value + "'";
                str_temp += record[idx].value;
            } else {
                //str_temp = record[idx].value;
                str_temp += "";
            }
            str_bind_command_inout.insert(start_pos, str_temp);
            ciDEBUG(5, ("ReplaceString #[%d] => %s\n", idx, str_temp.c_str()));
        }
        
        ciDEBUG(5, ("findString : %s\n", findString.c_str()));
        ciDEBUG(5, ("find start_pos=%d, end_pos=%d, idx=%d, findString:%s\n", start_pos, end_pos, idx, findString.c_str()));
    }
    ciDEBUG(5, ("Result : [%s] \n", str_bind_command_inout.c_str()));

    return (int) str_bind_command_inout.size();
}


int elCommand::BindCommandValueAtRecords_(elRecordList& records, string& str_bind_command_inout)
{
    ciDEBUG(5, ("BindCommandValueAtRecords_(%d)", records.size()));

    if (str_bind_command_inout == "") {
        return 0;
    }

    size_t start_pos = 0;
    size_t start_tail_pos = 0;
    size_t end_pos = 0;
    size_t end_tail_pos = 0;
    size_t delim_pos = 0;

    string findString("");
    string str_idx("");
    string str_row_idx("");
    string str_col_idx("");
    string str_temp("");
    size_t row_idx = 0;
    size_t col_idx = 0;

    if (str_bind_command_inout == "") {
        return -1;
    }

    // #[number] 형식 확인  
    int i = 0;
    ciDEBUG(5, ("Original : (%s) \n", str_bind_command_inout.c_str()));
    while ((start_pos = str_bind_command_inout.find("#[")) != string::npos) {
        start_tail_pos = start_pos+2;
        end_pos = str_bind_command_inout.find("]", start_tail_pos);
        if (end_pos == string::npos) {
            ciERROR(("str_bind_command_inout not exist ']' "));
            return -2;
        }

        // #[row,column] 형식
        // rowidx, colinx 반드시 모두 있어야 한다.
        // 따라서 이 함수는 @RepeatBlock 에서는 사용할 수 없다.
        str_idx = str_bind_command_inout.substr(start_tail_pos, end_pos-start_tail_pos);
        delim_pos = str_idx.find(",");
        if (delim_pos == string::npos) {
            // ','가 없으면 에러
            ciERROR(("Failed BindCommandValueAtRecords_. not exist ',' "));
            return -3;
        } else {
            str_row_idx = str_idx.substr(0, delim_pos);
            if (!IsDigit_(str_row_idx)) {
                ciERROR(("Failed BindCommandValueAtRecords_! str_row_idx is not digit "));
                return -4;
            }
        }
        str_col_idx = str_idx.substr(delim_pos+1);
        if (!IsDigit_(str_col_idx)) {
            ciERROR(("Failed BindCommandValueAtRecords_! str_col_idx is not digit "));
            return -5;
        }

        findString = str_bind_command_inout.substr(start_pos, end_pos-start_pos+1);
        str_bind_command_inout.erase(start_pos, findString.length());

        row_idx = atoi(str_row_idx.c_str());
        col_idx = atoi(str_col_idx.c_str());
        
        if (row_idx > records.size() - 1) {
            ciERROR(("Failed BindCommandValueAtRecords_! row_idx=%d is too long", row_idx));
            return -6;
        }
        if (col_idx > records[row_idx].size() - 1) {
            ciERROR(("Failed BindCommandValueAtRecords_! col_idx=%d is too long", col_idx));
            return -7;
        }

        if (records[row_idx][col_idx].value == "NULL") {
            str_temp = "";
        } else {
            str_temp = records[row_idx][col_idx].value;
        }
        str_bind_command_inout.insert(start_pos, str_temp);
        ciDEBUG(5, ("ReplaceString #[%d,%d] => %s\n", row_idx, col_idx, str_temp.c_str()));
        
        ciDEBUG(5, ("find start_pos=%d, end_pos=%d, idx=#[%d,%d], findString:%s\n", start_pos, end_pos, row_idx, col_idx, findString.c_str()));
    }
    ciDEBUG(5, ("Result : [%s] \n", str_bind_command_inout.c_str()));

    return (int) str_bind_command_inout.size();
}

bool elCommand::IsDigit_(string str_digit)
{
    for (size_t i = 0; i < str_digit.size(); i++) {
        if (0 == isdigit(str_digit[i])) {
            return false;
        }
    }
    return true;
}

void elCommand::Release_()
{
    ciDEBUG(5, ("Release_()"));

}