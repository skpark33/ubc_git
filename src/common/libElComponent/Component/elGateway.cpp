#include "elGateway.h"

ciSET_DEBUG(5, "elGateway");


elGateway::elGateway(void) : p_rule_repository_(NULL), p_my_rule_(NULL), m_eliga_cfg_path("")
{
    ciDEBUG(5, ("elGateway()"));
}

elGateway::~elGateway(void)
{
    ciDEBUG(5, ("~elGateway()"));
    End();
}

void elGateway::ProcessExpired(string name, int counter, int interval_sec, int interval_msec) 
{
    ciDEBUG(5, ("ProcessExpired(%s, %d, %d, %d)", name.c_str(), counter, interval_sec, interval_msec));
}

void elGateway::Update() 
{
    ciDEBUG(5, ("Update()"));
}

bool elGateway::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    if (IsInit() == true && my_id == id_) {
        ciDEBUG(5, ("Already Init(%s)", my_id.c_str()));
        return true;
    }

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elGatewayRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;

    route_rule_map_ = p_my_rule_->GetChildRuleMap(EL_KEYWORD_ROUTE_RULE);

    is_init_ = true;
    return true;
}

bool elGateway::Init(string my_id, string eligaCfgPath)
{
    ciDEBUG(5, ("Init(%s, %s)", my_id.c_str(), eligaCfgPath.c_str()));

    if (IsInit() == true && my_id == id_ && m_eliga_cfg_path == eligaCfgPath) {
        ciDEBUG(5, ("Already Init(%s, %s)", my_id.c_str(), eligaCfgPath.c_str()));
        return true;
    }

    // Rule 파일이 바뀌면 elRuleRepository 재성성
    if (m_eliga_cfg_path != eligaCfgPath) {
        elRuleRepository::ClearInstance();
    }
    p_rule_repository_ = elRuleRepository::GetInstance(eligaCfgPath.c_str());
    p_my_rule_ = (elGatewayRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        is_init_ = false;
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    //oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;

    route_rule_map_ = p_my_rule_->GetChildRuleMap(EL_KEYWORD_ROUTE_RULE);

    m_eliga_cfg_path = eligaCfgPath;

    is_init_ = true;
    return true;
}

void elGateway::End()
{
    ciDEBUG(5, ("End()"));

    Stop();
    Release_();
    is_init_ = false;
}

bool elGateway::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    if (is_start_ == true) {
        ciDEBUG(5, ("Already Start(%s)", id_.c_str()));
        return true;
    }

    if (option == ADMIN_STATE_START_NO) {
        // Do noting
    } else if (option == ADMIN_STATE_START_ALL) {
        SetChildComponent_();
    } else if (option == ADMIN_STATE_START_TIMER) {
    } else if (option == ADMIN_STATE_START_THREAD) {
    } else if (option == ADMIN_STATE_START_CHILD) {
        SetChildComponent_();
    } else {
        ciERROR(("Failed! Start option(%s)", option.c_str()));
        return false;
    }

    oper_state_ = "1";
    is_start_ = true;

    return true;
}

bool elGateway::Stop()
{
    ciDEBUG(5, ("Stop()"));

    if (is_start_ == false) {
        ciDEBUG(5, ("Already Stop(%s)", id_.c_str()));
        return true;
    }

    // Stop child
    for (unsigned int i = 0; i < route_vec_.size(); i++) {
        route_vec_[i]->Stop();
    }

    oper_state_ = "0";
    is_start_ = false;

    return true;
}


void elGateway::SetChildComponent_()
{
    ciDEBUG(5, ("SetChildComponent_()"));

	elRuleIdMap::iterator itr;
	for(itr=route_rule_map_.begin(); itr!=route_rule_map_.end(); itr++) {
		elRule* ele = itr->second;
        elRoute* route = new elRoute();
        route->Init(ele->id);
        route->Start(ele->admin_state);
        route_vec_.push_back(route);
	}
}


void elGateway::Release_()
{
    ciDEBUG(5, ("Release_()"));

    for (size_t i = 0; i < route_vec_.size(); i++) {
        delete route_vec_[i];
    }
    route_rule_map_.clear();
}