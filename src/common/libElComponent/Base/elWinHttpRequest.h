/** \class elWinHttpRequest
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief elWinHttpRequest
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2016/01/22 11:01:00
 */

#ifndef _elWinHttpRequest_h_
#define _elWinHttpRequest_h_

#include <afxinet.h>
#include <afxtempl.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>

class elWinHttpRequest
{
public:
    elWinHttpRequest(void);
    virtual ~elWinHttpRequest(void);
	
    enum CONNECTION_TYPE
	{
		CONNECTION_USER_DEFINED = 0,
		CONNECTION_CENTER,
		CONNECTION_SERVER,
	};

protected:
	class CBufferDeleter
	{
	public:
		CBufferDeleter(char* buf) { Init(); m_buf=buf; };
		CBufferDeleter(wchar_t* wbuf) { Init(); m_wbuf=wbuf; };
		CBufferDeleter(CHttpFile* hf) { Init(); m_httpfile=hf; };
		CBufferDeleter(CHttpConnection* hc) { Init(); m_httpconn=hc; };
		~CBufferDeleter() { Clear(); };

	protected:
		char*		m_buf;
		wchar_t*	m_wbuf;
		CHttpFile*	m_httpfile;
		CHttpConnection*	m_httpconn;

		void	Init()
					{
						m_buf = NULL;
						m_wbuf = NULL;
						m_httpfile = NULL;
						m_httpconn = NULL;
					};

        void	Clear()
					{
						if(m_buf) delete[]m_buf;
						if(m_wbuf) delete[]m_wbuf;
						if(m_httpfile) { m_httpfile->Close(); delete m_httpfile; }
						if(m_httpconn) { m_httpconn->Close(); delete m_httpconn; }
					};

	};

protected:
	static int m_nSessionTimeout;

 	CONNECTION_TYPE	m_connType;
	CString	m_strServerIP;
	UINT	m_nServerPort;

	CString	m_strRequestURL;
	CString	m_strErrorMsg;

	BOOL	Request(bool isGet, LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg, string strHttpHeader="");
	void	GetLineList(CString& str, CStringArray& line_list);

	BOOL	HttpToHttps(const char* http, CString& https);
	BOOL	IpToDomainName(const char* strHttpServerIp, CString& domain);
	BOOL	isUseSSL(int forceValue=-1);
	BOOL	isUseApache(int forceValue=-1);

	CString	GetINIPath();

	CString	AsciiToBase64(CString strAscii);
	CString	Base64ToAscii(CString strBase64);

public:
	BOOL	Init(CONNECTION_TYPE conType, LPCTSTR lpszServerIP=NULL, UINT nServerPort=0);
			// CONNECTION_CENTER, CONNECTION_SERVER => connect to UBC-center or UBC-server
			// CONNECTION_USER_DEFINED => connect to user defined server,port

	BOOL	RequestGet(LPCTSTR lpUrl, CString &strOutMsg, string strHttpHeader="");
	BOOL	RequestGet(LPCTSTR lpUrl, CStringArray &strOutMsgList, string strHttpHeader="");
	BOOL	RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg, string strHttpHeader="");
	BOOL	RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CStringArray& strOutMsgList, string strHttpHeader="");

	static CString	ToEncodingString(CString str, bool bReturnNullToString=false);
	static CString	ToString(int nValue);

	void	GetServerInfo(CString& strIP, UINT& nPort) { strIP=m_strServerIP; nPort=m_nServerPort; };
	LPCSTR	GetErrorMsg() { return (LPCSTR)m_strErrorMsg; };
	LPCSTR	GetRequestURL() { return (LPCSTR)m_strRequestURL; };

	VOID	DontUseSSL() { isUseSSL(0); } 
	VOID	DontUseApache() { isUseApache(0); } 
	VOID	SetPort(UINT p) { m_nServerPort = p; }
	VOID	SetServerIP(LPCTSTR p) { m_strServerIP = p; }


};


class elHttpCriticalSection
{
public:
	elHttpCriticalSection();
	~elHttpCriticalSection();
	void lock();
	void unlock();
protected:
	CRITICAL_SECTION _handle;
};

#endif //_elWinHttpRequest_h_