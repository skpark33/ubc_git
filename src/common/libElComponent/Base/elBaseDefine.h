#ifndef _elBaseDefine_h_
#define _elBaseDefine_h_

// field type mapping
//TINYBLOB, TINYTEXT       L + 1 bytes, where L < 2^8    (255 Bytes)
//BLOB, TEXT               L + 2 bytes, where L < 2^16   (64 Kilobytes)
//MEDIUMBLOB, MEDIUMTEXT   L + 3 bytes, where L < 2^24   (16 Megabytes)
//LONGBLOB, LONGTEXT       L + 4 bytes, where L < 2^32   (4 Gigabytes)
#define EL_CLIENT_MULTI_QUERIES   "CLIENT_MULTI_STATEMENTS"  
#define EL_FIELD_TYPE_DECIMAL     "DECIMAL"
#define EL_FIELD_TYPE_NEWDECIMAL  "NUMERIC"
#define EL_FIELD_TYPE_TINY        "TINYINT"
#define EL_FIELD_TYPE_SHORT       "SMALLINT"
#define EL_FIELD_TYPE_LONG        "INTEGER"
#define EL_FIELD_TYPE_FLOAT       "FLOAT"
#define EL_FIELD_TYPE_DOUBLE      "DOUBLE"
#define EL_FIELD_TYPE_NULL        "NULL"
#define EL_FIELD_TYPE_TIMESTAMP   "TIMESTAMP"
#define EL_FIELD_TYPE_LONGLONG    "BIGINT"
#define EL_FIELD_TYPE_INT24       "MEDIUMINT"
#define EL_FIELD_TYPE_DATE        "DATE"
#define EL_FIELD_TYPE_TIME        "TIME"
#define EL_FIELD_TYPE_DATETIME    "DATETIME"
#define EL_FIELD_TYPE_YEAR        "YEAR"
#define EL_FIELD_TYPE_NEWDATE     "NEWDATE"
#define EL_FIELD_TYPE_ENUM        "ENUM"
#define EL_FIELD_TYPE_SET         "SET"
#define EL_FIELD_TYPE_TINY_BLOB   "TINYBLOB"
#define EL_FIELD_TYPE_MEDIUM_BLOB "MEDIUMBLOB"
#define EL_FIELD_TYPE_LONG_BLOB   "LONGBLOB"
#define EL_FIELD_TYPE_BLOB        "BLOB"
#define EL_FIELD_TYPE_VAR_STRING  "VARCHAR"
#define EL_FIELD_TYPE_STRING      "STRING"
#define EL_FIELD_TYPE_CHAR        "TINYINT"
#define EL_FIELD_TYPE_INTERVAL    "INTERVAL"
#define EL_FIELD_TYPE_GEOMETRY    "GEOMETRY"
#define EL_FIELD_TYPE_BIT         "BIT"

typedef struct _elDbFieldInfo {
    string  name;             /* Name of column */
    string  org_name;         /* Original column name, if an alias */
    string  type;             /* Type of field. See mysql_com.h for types */
    unsigned long length;     /* Width of column (create length) */
    unsigned long max_length; /* Max width for selected set */
    unsigned int name_length;
    unsigned int org_name_length;
    string  table;            /* Org table name, if table was an alias */
    string  org_table;        /* Org table name, if table was an alias */
    string  db;               /* Database for table */
    string  catalog;	      /* Catalog for table */
    string  def;              /* Default value (set by mysql_list_fields) */
    unsigned int table_length;
    unsigned int org_table_length;
    unsigned int db_length;
    unsigned int catalog_length;
    unsigned int def_length;
    unsigned int flags;         /* Div flags */
    unsigned int decimals;      /* Number of decimals in field */
    unsigned int charsetnr;     /* Character set */
} elDbFieldInfo;

typedef struct _elField {
    string  value;
    string  name;
    string  type;
    bool    is_exist;
} elField;

typedef vector<elDbFieldInfo>       elFieldInfos;
typedef vector<elField>             elRecord;
typedef vector<elRecord>            elRecordList;

#endif //_elBaseDefine_h_