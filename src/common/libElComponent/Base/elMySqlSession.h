/** \class elMysqlSession
 *  Copyright ⓒ 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief elMysqlSession는 향후 elSession 추상클래스로 부터 상속
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/11/26 11:01:00
 */

#ifndef _elMysqlSession_h_
#define _elMysqlSession_h_

#include "elMySQLDriver.h"

class elMysqlSession
{
public:
    elMysqlSession(void);
    ~elMysqlSession(void);

    void    StartDB(string host, int port, string name, string user, string pass);
    void    StopDB();

    bool    Select(string str_sql);
    bool    GetRecordList(elRecordList& selected_records);
    bool    Next(elRecord& record);

    bool    Execute(string str_sql);
    int     ExecuteSequential(string str_sql, elRecordList& values);
    bool    ExecuteRecord(string& str_sql_inout, elRecordList& values, bool is_transaction);
    bool    ExecuteDeleteInsert(string str_sql, elRecordList& values);
    bool    ExecuteInsert(string str_sql, elRecordList& values);
    bool    ExecuteBulkInsert(string table_name, elRecordList& values);

    bool    IsConnected();

protected:
    bool    VerifyDB_();
    bool    ReplaceBindQuery_(string& str_result_sql, elRecord& record);
    string  ExtractQueryBoundary_(string& str_sql_in, string& str_sql_out);

    ciMutex		    queue_lock_;
    ciMutex         db_session_lock_;
    
    elMySQLDriver*        p_mysql_driver_;

    string          db_type_;
    string          db_name_;
    string          db_user_;
    string          db_pass_;
    string          db_host_;
    ciLong          db_port_;    

};

#endif //_elMysqlSession_h_