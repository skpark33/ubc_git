#include "elMysqlSession.h"

ciSET_DEBUG(5, "elMysqlSession");

elMysqlSession::elMysqlSession(void) : p_mysql_driver_(NULL)
{
}

elMysqlSession::~elMysqlSession(void)
{
    StopDB();
}

bool elMysqlSession::VerifyDB_()
{
    ciDEBUG(5, ("VerifyDB_()"));

    if (!p_mysql_driver_) {
        ciERROR(("Execute() DB Session is NULL!!!"));
        return false;
    }

    if (!p_mysql_driver_->isConnected()) {
        StartDB(db_host_, db_port_, db_name_, db_user_, db_pass_);

        if(!p_mysql_driver_->isConnected()) {
            ciERROR(("Select() DB Session cannot connect!!!"));
            return false;
        }
    }

    return true;
}

bool    elMysqlSession::IsConnected()
{
    return p_mysql_driver_->isConnected();
}

void elMysqlSession::StartDB(string host, int port, string name, string user, string pass)
{
    ciDEBUG(5, ("StartDB(%s, %d, %s, %s)", host.c_str(), port, name.c_str(), user.c_str()));

    db_host_ = host;
    db_port_ = port;
    db_name_ = name;
    db_user_ = user;
    db_pass_ = pass;
    
    if (p_mysql_driver_ != NULL) {
        StopDB();
    }

    p_mysql_driver_ = new elMySQLDriver(db_name_.c_str(), db_user_.c_str(), db_pass_.c_str(), db_host_.c_str(), db_port_);
    {
        //ciGuard aGuard(db_session_lock_);
        if (!p_mysql_driver_->connect()) {
            ciERROR(("StartDB_ Start Failed!!!"));
            return;
        }
    }

}

void elMysqlSession::StopDB()
{
    ciDEBUG(5, ("StopDB()"));

    if (p_mysql_driver_) {
        {
            //ciGuard aGuard(db_session_lock_);
            p_mysql_driver_->disconnect();
            delete p_mysql_driver_;
            p_mysql_driver_ = NULL;
        }//ciGuard
    }
}

bool elMysqlSession::Select(string str_sql)
{
    ciDEBUG(5, ("Select(%s)", str_sql.c_str()));

    if (str_sql == "") {
        ciERROR(("Select() SQL is NULL!!!"));
        return false;
    }
    if (!VerifyDB_()) {
        return false;
    }

    {
        //ciGuard aGuard(db_session_lock_);
        if (p_mysql_driver_->executeGet(str_sql) == ciFalse) {
            const char* codeStr = p_mysql_driver_->errorMsg();
            ciERROR(("%d:%s", p_mysql_driver_->errorCode(), codeStr ));
            return false;
        }
    }

    return true;
}

bool elMysqlSession::GetRecordList(elRecordList& selected_records)
{
    ciDEBUG(5, ("GetRecordList()"));
    if(!p_mysql_driver_) {
	    ciERROR(("p_mysql_driver_ is null"));
        return ciFalse;
    }

    int counter = 0;
    while (p_mysql_driver_->next()) {
        elRecord record;
		int len = p_mysql_driver_->getFieldCount();        
        for(int i = 0; i < len ; i++ ) {
            elField db_field = p_mysql_driver_->getField(i);
		    if(!db_field.is_exist) {
			    ciERROR(("db_field[%d] is not exist",i));
                return false;			
		    }
            if(!len) {
                ciERROR(("db_field 0 is not exist"));
                return false;
            }
            record.push_back(db_field);
        }
        selected_records.push_back(record);
        counter++;
    }

    if(!counter) {
	    ciDEBUG(5,("%d row selected", counter));
        return false;
    }
    ciDEBUG(5,("%d row selected", counter));

    return true;
}

bool elMysqlSession::Next(elRecord& record)
{
    ciDEBUG(5, ("Next()"));
    if(!p_mysql_driver_) return ciFalse;

	int len = p_mysql_driver_->getFieldCount();        
    for(int i = 0; i < len ; i++ ) {
        elField db_field = p_mysql_driver_->getField(i);
		if(!db_field.is_exist) {
			ciERROR(("db_field[%d] is not exist",i));
            return false;			
		} 
        record.push_back(db_field);
    }

    if(!len) {
        ciERROR(("db_field 0 is not exist"));
        return false;
    }
    ciDEBUG(5,("%d field selected", len));

    return true;
}

bool elMysqlSession::Execute(string str_sql)
{
    ciDEBUG(5, ("Execute(%s)", str_sql.c_str()));

    if (str_sql == "") {
        ciERROR(("Execute() SQL is NULL!!!"));
        return false;
    }
ciDEBUG(5, ("Execute(%s)", str_sql.c_str()));
    if (!VerifyDB_()) {
        return false;
    }

    {
        //ciGuard aGuard(db_session_lock_);
        if (!p_mysql_driver_->execute(str_sql)) {
            const char* codeStr = p_mysql_driver_->errorMsg();
            ciERROR(("Failed Execute() %d:%s", p_mysql_driver_->errorCode(), codeStr ));
            return false;
        }
    }

    return true;
}

bool elMysqlSession::ReplaceBindQuery_(string& str_result_sql, elRecord& record)
{
    size_t pos;
    size_t pos2;
    string findString("");
    string str_idx("");
    string str_temp("");
    int    idx = 0;

    if (str_result_sql == "") {
        return false;
    }

    // #[number] 형식 확인  
    int i = 0;
    ciDEBUG(5, ("Original : [%s] \n", str_result_sql.c_str()));
    while ((pos = str_result_sql.find("#[")) != string::npos) {
        pos2 = str_result_sql.find("]", pos+2);
        str_idx = str_result_sql.substr(pos+2, pos2-pos-2);
        findString = str_result_sql.substr(pos, pos2-pos+1);
        str_result_sql.erase(pos, findString.length());

        str_temp = "";
        if (str_idx == "*") {
            for (size_t j = 0; j < record.size(); j++) {
                if (record[j].value != "NULL") {
                    str_temp += "'" + record[j].value + "'";
                } else {
                    str_temp += record[j].value;
                }
                if (j != record.size()-1) {
                    str_temp += ",";
                }
            }
            str_result_sql.insert(pos, str_temp);
            ciDEBUG(5, ("ReplaceString #[*] => %s\n", str_temp.c_str()));
        } else {
            idx = atoi(str_idx.c_str()) - 1;
            if (idx > (int) record.size() - 1) {
                ciERROR(("ReplaceBindQuery_ index=%d is too long", idx));
                return false;
            }

            if (record[idx].value != "NULL") {
                str_temp = "'" + record[idx].value + "'";
            } else {
                str_temp = record[idx].value;
            }
            str_result_sql.insert(pos, str_temp);
            ciDEBUG(5, ("ReplaceString #[%d] => %s\n", idx, str_temp.c_str()));
        }
        
        ciDEBUG(5, ("findString : %s\n", findString.c_str()));
        ciDEBUG(5, ("find pos=%d, pos2=%d, idx=%d, findString:%s\n", pos, pos2, idx, findString.c_str()));
    }
    ciDEBUG(5, ("Result : [%s] \n", str_result_sql.c_str()));
  
    return true;
}

/// return type - 쿼리 범위 : [*] TABLE 별, [?] ROW 별
string  elMysqlSession::ExtractQueryBoundary_(string& str_sql_in, string& str_sql_out)
{
    size_t pos;
    size_t pos2;
    string findString("");
    string str_boundary("");

    if (str_sql_in == "") {
        return str_boundary;
    }

    // #[number] 형식 확인  
    int i = 0;
    str_sql_out = str_sql_in;
    ciDEBUG(5, ("Original : %s \n", str_sql_in.c_str()));
    if ((pos = str_sql_out.find("@[")) != string::npos) {
        pos2 = str_sql_out.find("]", pos+2);
        str_boundary = str_sql_out.substr(pos+2, pos2-pos-2);
        findString = str_sql_out.substr(pos, pos2-pos+1);

        str_sql_out.erase(pos, findString.length());

        ciDEBUG(5, ("str_boundary : %s\n", str_boundary.c_str()));
        ciDEBUG(5, ("find pos=%d, pos2=%d, findString:%s\n", pos, pos2, findString.c_str()));
        ciDEBUG(5, ("str_sql_out:%d : [%s] \n", i, str_sql_out.c_str()));
    } else {
        // default boundary @[*]
        str_boundary = "*";
    }
    ciDEBUG(5, ("str_sql_out @[%s] : %s \n", str_boundary.c_str(), str_sql_out.c_str()));
    return str_boundary;
}

int elMysqlSession::ExecuteSequential(string str_sql, elRecordList& values)
{
    ciDEBUG(5, ("ExecuteSequential(%s)", str_sql.c_str()));

    if (str_sql == "") {
        ciERROR(("ExecuteSequential() SQL is NULL!!!"));
        return -1;
    }
    if (!VerifyDB_()) {
        return -2;
    }

    string sql_temp("");
    string sql_boundary("");
    vector<string> sql_vector;
    ciStringUtil::Tokenizer(str_sql.c_str(), ";", &sql_vector);

    if (sql_vector.size() == 0) {
        ciERROR(("ExecuteSequential() Not exist sql"));
        return -3;
    }

    //string sql_delete = sql_vector[0];           // 향후 확인로직 추가
    //string sql_insert_values = sql_vector[0];    // 향후 확인로직 추가
    //string sql_commit = " COMMIT; ";

    string sql_start_tracsaction = " START TRANSACTION ";
    if (!Execute(sql_start_tracsaction)) {
        ciERROR(("Fail START TRANSACTION(%s)", sql_start_tracsaction.c_str()));
        return -4;
    }


    for (size_t i = 0; i < sql_vector.size(); i++) {
        sql_boundary = ExtractQueryBoundary_(sql_vector[i], sql_temp);
ciDEBUG(5, ("1. ExecuteSequential(%s)", sql_boundary.c_str()));
        if (sql_boundary == "*") {
ciDEBUG(5, ("2. ExecuteSequential(%s)", sql_temp.c_str()));
            // 보강 필요 : Value 바인딩 처리 - ExecuteOne을 생성?
            if (!Execute(sql_temp)) {
                ciERROR(("Failed Execute @[*]:%d !!!(%s)", i, sql_temp.c_str()));
                Execute("ROLLBACK");
                return -5;
            }
ciDEBUG(5, ("3. ExecuteSequential(%s)", sql_temp.c_str()));
        } else if (sql_boundary == "?") {
            if (!ExecuteRecord(sql_temp, values, false)) {
                ciERROR(("Failed ExecuteRecord @[?]:%d !!!(%s)", i, sql_temp.c_str()));
                Execute("ROLLBACK");
                return -6;
            }
        } else {
            ciERROR(("Failed ExtractQueryBoundary_:%d, @[%s] !!!(%s)", i, sql_boundary.c_str(), sql_temp.c_str()));
            return -7;
        }
    }
ciDEBUG(5, ("4. ExecuteSequential(%s)", sql_temp.c_str()));

    if (!Execute("COMMIT")) {
        ciERROR(("Fail ExecuteSequential(%s)", sql_temp.c_str()));
        Execute("ROLLBACK");
        return -8;
    }

    ciDEBUG(5, ("Success ExecuteSequential(Last SQL : %s)", sql_temp.c_str()));

    return (int) sql_vector.size();
}

bool elMysqlSession::ExecuteRecord(string& str_sql_inout, elRecordList& values, bool is_transaction)
{
    ciDEBUG(5, ("ExecuteRecord(%s, %d)", str_sql_inout.c_str(), values.size()));

    if (str_sql_inout == "") {
        ciERROR(("ExecuteRecord() SQL is NULL!!!"));
        return false;
    }
    if (!VerifyDB_()) {
        ciERROR(("ExecuteRecord() VerifyDB_ is false!!!"));
        return false;
    }

    if (values.size() == 0) {
        ciERROR(("ExecuteRecord() values.size() is 0!!!"));
        return false;
    }

    string str_sql_temp("");

    for (unsigned int i = 0; i < values.size(); i++) {
        str_sql_temp = str_sql_inout;
        ReplaceBindQuery_(str_sql_temp, values[i]);

        if (!Execute(str_sql_temp)) {
            ciERROR(("Failed Execute[%d](%s)", i, str_sql_inout.c_str()));
            if (is_transaction) {
                Execute("ROLLBACK");
            }
            str_sql_inout = str_sql_temp;
            return false;
        }

        str_sql_temp = "";
    }
    
    if (is_transaction) {
        Execute("COMMIT");
    }

    str_sql_inout = str_sql_temp;
    return true;
}

// P1. INSERT 시 한글필드가 깨진다
// P2. INSERT 시 DATETIME 필드가 NULL이면 에러난다.
// P3. SET AUTOCOMMIT=0 를 사용못한다.
// P4. DELETE; INSERT; 쿼리를 한번에 못한다.
bool elMysqlSession::ExecuteDeleteInsert(string str_sql, elRecordList& values)
{
    ciDEBUG(5, ("ExecuteDeleteInsert(%s)", str_sql.c_str()));

    if (str_sql == "") {
        ciERROR(("ExecuteDeleteInsert() SQL is NULL!!!"));
        return false;
    }
    if (!VerifyDB_()) {
        return false;
    }

    if (values.size() == 0) {
        return false;
    }

    // str_sql : " START TRANSACTION; DELETE FROM iot.IOT_BEACON WHERE BINARY siteId='LC4855' AND lastUpdateTime > STR_TO_DATE('#{iot_beacon.lastUpdateTime}', '%Y-%m-%d %H:%i:%S'); INSERT INTO iot.IOT_BEACON VALUES ",
    string sql_temp("");
    vector<string> sql_vector;
    ciStringUtil::Tokenizer(str_sql.c_str(), ";", &sql_vector);

    if (sql_vector.size() == 0 || sql_vector.size() != 2) {
        ciERROR(("ExecuteDeleteInsert() SQL Format is wrong! sql(%d)", sql_vector.size()));
        return false;
    }

    //string sql_delete = sql_vector[0];           // 향후 확인로직 추가
    //string sql_insert_values = sql_vector[0];    // 향후 확인로직 추가
    //string sql_commit = " COMMIT; ";

    string sql_start_tracsaction = " START TRANSACTION ";
    if (!Execute(sql_start_tracsaction)) {
        ciERROR(("Fail START TRANSACTION(%s)", sql_start_tracsaction.c_str()));
        return false;
    }

    // DELETE Check and Execute
    sql_temp = sql_vector[0];
    ciStringUtil::safeRemoveSpace(sql_temp);
    ciStringUtil::safeToUpper(sql_temp);
    if (sql_temp.substr(0, 6) != "DELETE") {
        ciERROR(("Query is not DELETE (%s)", sql_vector[0].c_str()));
        Execute("ROLLBACK");
        return false;
    }

    // INSERT Check
    sql_temp = sql_vector[1];
    ciStringUtil::safeRemoveSpace(sql_temp);
    ciStringUtil::safeToUpper(sql_temp);
    if (sql_temp.substr(0, 6) != "INSERT") {
        ciERROR(("Query is not INSERT (%s)", sql_vector[1].c_str()));
        Execute("ROLLBACK");
        return false;
    }

    //str_sql : "SET INSERT INTO table VALUES" + " (#{values})
    string str_delete_query("");
    string str_insert_query("");
    for (unsigned int i = 0; i < values.size(); i++) {
        // DELETE FROM test.IOT_BROCHURE_CONTENTS_GOODS_TEST WHERE BINARY siteId LIKE '#{Global.BrandID}%' AND brochureId='#[3]' AND contentsId='#[4]' AND goodsId='#[5]' ;
        str_delete_query = sql_vector[0];
        ReplaceBindQuery_(str_delete_query, values[i]);
        if (!Execute(str_delete_query)) {
            ciERROR(("Fail DELETE[%d](%s)", i, str_delete_query.c_str()));
            Execute("ROLLBACK");
            return false;
        }

        str_insert_query = sql_vector[1];
        str_insert_query += " (";
        for (unsigned int j = 0; j < values[i].size(); j++) {
            if (values[i][j].value=="NULL") {
                str_insert_query += values[i][j].value;
            } else {
                str_insert_query += "'";
                str_insert_query += values[i][j].value;
                str_insert_query += "'";
            }
            if (j !=  values[i].size()-1) {
                str_insert_query += ",";
            } else {
                // values close
                str_insert_query += ");";
            }
        }
        //str_insert_query += sql_commit;
        if (!Execute(str_insert_query)) {
            ciERROR(("Fail INSERT[%d](%s)", i, str_insert_query.c_str()));
            Execute("ROLLBACK");
            return false;
        }
        str_delete_query = "";
        str_insert_query = "";
    }
    if (!Execute("COMMIT")) {
        ciERROR(("Fail ExecuteDeleteInsert(%s)", str_insert_query.c_str()));
        Execute("ROLLBACK");
        return false;
    }
    ciDEBUG(5, ("Success ExecuteDeleteInsert(%s)", str_insert_query.c_str()));
    return true;
}

bool elMysqlSession::ExecuteInsert(string str_sql, elRecordList& values)
{
    ciDEBUG(5, ("ExecuteInsert(%s)", str_sql.c_str()));
/***    
    if (str_sql == "") {
        ciERROR(("ExecuteInsert() SQL is NULL!!!"));
        return false;
    }
    if (!VerifyDB_()) {
        return false;
    }
    if (values.size() == 0) {
        return false;
    }


    // str_sql : " SET AUTOCOMMIT=0; INSERT INTO iot.IOT_BEACON VALUES ",
    vector<string> sql_vector;
    ciStringUtil::Tokenizer(str_sql.c_str(), ";", &sql_vector);

    if (sql_vector.size() != 3) {
        ciERROR(("ExecuteInsert() SQL Format is wrong! sql(%d)", sql_vector.size()));
        return false;
    }
    string sql_set_autocommit_0 = sql_vector[0]; // 향후 확인로직 추가
    string sql_insert_values = sql_vector[1];    // 향후 확인로직 추가
    string sql_commit = " COMMIT; ";
    

    //str_sql : "SET INSERT INTO table VALUES" + " (#{values})
    string str_insert_query("");
    for (unsigned int i = 0; i < values.size(); i++) {
        str_insert_query = str_sql;
        str_insert_query += " (";
        for (unsigned int j = 0; j < values[i].size(); j++) {
            str_insert_query += "'";
            str_insert_query += values[i][j].value;
            str_insert_query += "'";
            if (j !=  values[i].size()-1) {
                str_insert_query += ",";
            } else {
                // values close
                str_insert_query += ");";
            }
        }
        str_insert_query += sql_commit;
        if (!Execute(str_insert_query)) {
            return false;
        }
    }
***/
    return true;
}

bool elMysqlSession::ExecuteBulkInsert(string table_name, elRecordList& values)
{
    ciDEBUG(5, ("ExecuteBulkInsert(%s)", table_name.c_str()));
/***
    if (table_name == "") {
        ciERROR(("ExecuteBulkInsert() table_name is NULL!!!"));
        return false;
    }
    if (!VerifyDB_()) {     
        return false;
    }
    if (values.size() == 0) {
        return false;
    }

    //Use the multiple-row INSERT syntax to reduce communication overhead between the client and the server if you need to insert many rows:
    //INSERT INTO yourtable VALUES (1,2), (5,5), ...;
    string str_sql("INSERT INTO ");
    str_sql += table_name;
    str_sql += " VALUES ";
    for (unsigned int i = 0; i < values.size(); i++) {
        str_sql += " (";
        for (unsigned int j = 0; j < values[i].size(); j++) {
            str_sql += "'";
            str_sql += values[i][j].value;
            str_sql += "'";
            if (j !=  values[i].size()-1) {
                str_sql += ",";
            }
        }
        if (i !=  values.size()-1) {
            str_sql += ",";
        }
    }

    {
        ciGuard aGuard(db_session_lock_);
        if (!p_mysql_driver_->execute(str_sql)) {
            const char* codeStr = p_mysql_driver_->errorMsg();
            ciERROR(("Failed Execute() %d:%s", p_mysql_driver_->errorCode(), codeStr ));
            return false;
        }
    }

    ciDEBUG(5, ("Success! ExecuteBulkInsert(%s)", table_name.c_str()));
***/
    return true;
}
