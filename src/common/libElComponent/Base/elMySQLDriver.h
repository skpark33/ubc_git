#ifndef _elMySQLDriver_h_
#define _elMySQLDriver_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libThread/ciMutex.h>
#include <mysql.h>
#include <errno.h>
#include "elBaseDefine.h"

class elMySQLDriver
{
public:
	elMySQLDriver(const char* pDbName, const char* pDbUser, const char* pDbPass, const char* pDbHost="localhost", int pPort = 3306);
	virtual ~elMySQLDriver();

	virtual ciBoolean	connect();
	virtual ciBoolean	disconnect();

	virtual ciBoolean	execute(const char* pSql);
	virtual ciBoolean	execute(ciString pSql) { return execute(pSql.c_str()); }
	virtual ciBoolean	executeGet(ciString pSql) { return _execute(pSql.c_str()); }

	virtual ciBoolean	isConnected()   { return _connected; }
	virtual int			errorCode()	{ if(_mysql) return mysql_errno(_mysql); return 0;  } 
    virtual const char*	errorMsg() 	{ if(_mysql) return mysql_error(_mysql); return ""; };

	virtual ciBoolean	next();
	virtual const char*	getValueString(int idx);
	virtual int			getFieldCount() { return _num_fields; }
    virtual elField     getField(int idx);

protected:

	virtual ciBoolean	_execute(const char* pSql);
	virtual void		_closeSession();
	virtual void		_clearResult();
    virtual string      _getFieldType(int type);

	ciString	    _dbName;
	ciString	    _dbUser;
	ciString	    _dbPasswd;
	ciString	    _dbHost;
	int			    _dbPort;

	MYSQL*		    _mysql;
	ciBoolean	    _connected;
	ciMutex		    _lock;

	MYSQL_RES*      _res;
	int 			_num_fields;
	MYSQL_FIELD* 	_fields;
	MYSQL_ROW		_row;

    elFieldInfos    _field_infos;
};

#endif // _elMySQLDriver_h_
