#include "elWinHttpRequest.h"
#include <atlenc.h>

#include <ci/libConfig/ciIni.h>
#include <ci/libDebug/ciDebug.h>
//#ifdef _STT_LIB_
//#include <UBC/ubcIni.h>
//#else
//#include <common/libCommon/ubcIni.h>
//#endif

#include <util/libEncoding/Encoding.h>

ciSET_DEBUG(5, "elWinHttpRequest");

#define		DEFAULT_CENTER_IP			_T("ubccenter.sqisoft.com")
#define		DEFAULT_CENTER_WEBPORT		8080

#define		DEFAULT_SERVER_IP			_T("127.0.0.1")
#define		DEFAULT_SERVER_WEBPORT		8080

#define		HTTP_SESSION_TIMEOUT_SEC	(60)//*1000)	// 1min

int elWinHttpRequest::m_nSessionTimeout = -1;


elWinHttpRequest::elWinHttpRequest(void)
:	m_connType ( CONNECTION_USER_DEFINED )
,	m_strServerIP ( DEFAULT_SERVER_IP )
,	m_nServerPort ( DEFAULT_SERVER_WEBPORT )
{
	if( m_nSessionTimeout < 0 )
	{
		m_nSessionTimeout = GetPrivateProfileInt(_T("ROOT"), _T("HTTP_SESSION_TIMEOUT_SEC"), HTTP_SESSION_TIMEOUT_SEC, GetINIPath());
		if( m_nSessionTimeout < 5 ) m_nSessionTimeout = 5;

		m_nSessionTimeout *= 1000; // sec ==> msec
	}
}

elWinHttpRequest::~elWinHttpRequest(void)
{
}

BOOL elWinHttpRequest::Init(CONNECTION_TYPE conType, LPCTSTR lpszServerIP, UINT nServerPort)
{
	m_connType = conType;

	switch(conType)
	{
	case CONNECTION_USER_DEFINED:
		m_strServerIP = (lpszServerIP ? lpszServerIP : DEFAULT_SERVER_IP);
		m_nServerPort = (nServerPort ? nServerPort : DEFAULT_SERVER_WEBPORT);
		break;
/****
	case CONNECTION_CENTER:
		{
			//ubcConfig aIni("UBCConnect");
#ifdef _STT_LIB_
			ubcIni aIni("UBCConnect", "CONFIGROOT", "data");
#else
			ubcIni aIni("UBCConnect", "", "data");
#endif
			ciString center_ip="", center_port="";

			//
			aIni.get("UBCCENTER", "IP", center_ip);
			m_strServerIP = ( center_ip.length()>0 ? center_ip.c_str() : DEFAULT_CENTER_IP );

			//
			aIni.get("UBCCENTER", "WEBPORT", center_port);
			m_nServerPort = ( center_port.length()>0 ? atoi(center_port.c_str()) : DEFAULT_CENTER_WEBPORT );
		}
		break;

	case CONNECTION_SERVER:
		{
			//ubcConfig aIni("UBCConnect");
#ifdef _STT_LIB_
			ubcIni aIni("UBCConnect", "CONFIGROOT", "data");
#else
			ubcIni aIni("UBCConnect", "", "data");
#endif
			ciString server_ip="", server_port="";

			//
			aIni.get("ORB_NAMESERVICE", "IP", server_ip);
			m_strServerIP = ( server_ip.length()>0 ? server_ip.c_str() : DEFAULT_SERVER_IP );

			//
			aIni.get("AGENTMUX", "HTTP_PORT", server_port);
			m_nServerPort = ( server_port.length()>0 ? atoi(server_port.c_str()) : DEFAULT_SERVER_WEBPORT );
		}
		break;
***/
	default:
		return FALSE;
		break;
	}
	return TRUE;
}

BOOL elWinHttpRequest::Request(bool isGet, LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg, string strHttpHeader)
{
    ciDEBUG(5, ("host(%s) port(%d) isGet(%d), lpUrl(%s) lpszSendMsg(%s) strHttpHeader(%s) "
        , (LPCTSTR) m_strServerIP, m_nServerPort, isGet, lpUrl, lpszSendMsg, strHttpHeader.c_str() ));

	CString strUrl;
	if(isUseApache() && m_connType!=CONNECTION_CENTER){ // center is only use ASP
		strUrl =  lpUrl;
		int urlSize = strUrl.GetLength();
		CString ext,name;
		if(urlSize > 4) {
			name = strUrl.Mid(0,urlSize-3);
			ext = strUrl.Mid(urlSize-3);
			if(ext.CompareNoCase("asp")==0){
				strUrl = name + "php";
				lpUrl = strUrl;
			}
		}
	}

	//
	if( _tcsnicmp( lpUrl, _T("http://"), 7) )
	{
		m_strRequestURL.Format(_T("http://%s:%d%s%s"), 
			m_strServerIP,
			m_nServerPort,
			(lpUrl[0] == _T('/') ? "" : _T("/")),
			lpUrl
		);
        ciDEBUG(5, ("1 ############### m_strServerIP(%s), m_strRequestURL(%s), m_nServerPort(%d), lpUrl(%s)", (LPCTSTR)m_strServerIP, (LPCTSTR)m_strRequestURL, m_nServerPort, lpUrl));
	}
	else
	{
		m_strRequestURL = lpUrl;
        ciDEBUG(5, ("2 ############### m_strServerIP(%s), m_strRequestURL(%s), m_nServerPort(%d), lpUrl(%s)", (LPCTSTR)m_strServerIP, (LPCTSTR)m_strRequestURL, m_nServerPort, lpUrl));
	}

	//
	bool is_https = false;
	if(m_connType != CONNECTION_CENTER) // center => only use http, not https!!!
	{
		//ciString https;
		//if(ciIniUtil::HttpToHttps(m_strRequestURL, https))
		//{
		//	m_strRequestURL = https.c_str();
		//}
		CString https;
		if(HttpToHttps(m_strRequestURL, https))
		{
			is_https = true;
			m_strRequestURL = https;
		}
	}

	//
	DWORD dwSearviceType;
	CString strServer, strObject;
	INTERNET_PORT nPort;

	if(!AfxParseURL((LPCTSTR)m_strRequestURL, dwSearviceType, strServer, strObject, nPort))
	{
		return FALSE;
	}
    ciDEBUG(5, ("m_strServerIP(%s) m_strRequestURL(%s) dwSearviceType(%lu) strServer(%s) strObject(%s) nPort(%hu)",
        (LPCTSTR)m_strServerIP, (LPCTSTR)m_strRequestURL, dwSearviceType, (LPCTSTR)strServer, (LPCTSTR)strObject, nPort));

	BOOL bRet = FALSE;

	try
	{
		CInternetSession Session;
		Session.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT,			(DWORD)m_nSessionTimeout);
		Session.SetOption(INTERNET_OPTION_CONTROL_RECEIVE_TIMEOUT,	(DWORD)m_nSessionTimeout );
		Session.SetOption(INTERNET_OPTION_CONTROL_SEND_TIMEOUT,		(DWORD)m_nSessionTimeout );
		Session.SetOption(INTERNET_OPTION_DATA_SEND_TIMEOUT,		(DWORD)m_nSessionTimeout );
		Session.SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT,		(DWORD)m_nSessionTimeout );

		CHttpConnection* pServer = Session.GetHttpConnection(strServer, nPort);
		if(pServer==NULL) return FALSE;
		CBufferDeleter hc_deleter(pServer);

		DWORD flag;
		if(isUseSSL() && is_https)
			flag = INTERNET_FLAG_EXISTING_CONNECT | INTERNET_FLAG_SECURE; // | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID | INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
		else
			flag = INTERNET_FLAG_EXISTING_CONNECT;

		CHttpFile *pFile = NULL;
		if(isGet)
		{
			//CString strHeader = "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\nAccept: */*\r\n";
            //CString strHeader = "";
            //CString strHeader  = L"Content-Type: application/x-www-form-urlencoded;charset=UTF-8";
            CString strHeader  = L"Content-Type: application/json;charset=UTF-8";
            //if (strHttpHeader != "") {
            //    strHeader = strHttpHeader.c_str();
            //}
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_GET, strObject, NULL, 1, NULL, NULL, flag);
            if(pFile==NULL) {
                ciERROR(("pServer->OpenRequest strObject(%s) strHeader(%s), msg(%s)", (LPCTSTR)strObject, (LPCTSTR)strHeader, lpszSendMsg));
                return FALSE;
            }
            if (!pFile->SendRequest(strHeader, (LPVOID)(LPCTSTR)strHeader, strHeader.GetLength())) {
                ciERROR(("pFile->SendRequest strObject(%s) strHeader(%s), msg(%s)", (LPCTSTR) strObject, (LPCTSTR) strHeader, lpszSendMsg));
                return FALSE;
            }
            CString cStr = pFile->GetObject();
            ciWARN(("SendRequest strObject(%s) strHeader(%s), msg(%s) dump(%s)", (LPCTSTR) strObject, (LPCTSTR) strHeader, lpszSendMsg, (LPCTSTR)cStr));
		}
		else
		{
			CString strHeader = "Content-Type: application/x-www-form-urlencoded\r\nUser-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
            if (strHttpHeader != "") {
                strHeader = strHttpHeader.c_str();
                //ciWARN(("strHeader:%s", (const char*) strHeader.GetBuffer()));
            }
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_POST, strObject, NULL, 1, NULL, NULL, flag);
            if(pFile==NULL) {
                ciERROR(("pServer->OpenRequest strObject(%s) strHeader(%s), msg(%s)", (LPCTSTR)strObject, (LPCTSTR)strHeader, lpszSendMsg));
                return FALSE;
            }
			pFile->SendRequest(strHeader, (LPVOID)lpszSendMsg, (DWORD) _tcslen(lpszSendMsg));

            ciWARN(("SendRequest strHeader(%s), msg(%s) ", (LPCTSTR)strHeader, lpszSendMsg));
		}
		CBufferDeleter hf_deleter(pFile);

		//
		char szLen[32]="";
		DWORD dwLenSize = sizeof(szLen);
		pFile->QueryInfo( HTTP_QUERY_CONTENT_LENGTH, szLen, &dwLenSize );
		int length = atoi( szLen);

        DWORD dwRet;
        pFile->QueryInfoStatusCode(dwRet);

        bRet = pFile->ReadString(strOutMsg);
        ciDEBUG(5, ("pFile->Read strOutMsg(%s)", (LPCTSTR)strOutMsg));

        ciDEBUG(5, ("QueryInfo StatusCode(%lu) strOutMsg(%s)", dwRet, (LPCTSTR)strOutMsg));

#if 0
        // 응답이 application/json;UTF-8 일 경우 못읽는 경우 발생
        // pFile->QueryInfo에서 contents의 길이를 못읽는 상황이 생긴다.
		TCHAR* buf = new TCHAR[length+1];
		CBufferDeleter buf_deleter(buf);
		::ZeroMemory(buf, sizeof(TCHAR)*(length+1));
        DWORD dwReadSize = pFile->Read(buf, length);

		if( dwReadSize != strlen(buf) || 
			(buf[0] == '<' && strnicmp("<?xml", buf, 5) != 0) || // not xml
			(buf[3] == '<' && strnicmp("<?xml", buf+3, 5) != 0) // not xml
		 )
		{
			// html tag, this module can't receive html result --> something wrong
			bRet = FALSE;
			m_strErrorMsg = buf;
		}
		else
		{
			const char* utf8_idx = strstr(buf, "CodePage=utf-8");

			if( utf8_idx != NULL )
			{
				std::string out_msg;
				UTF8toA(buf, out_msg);
				strOutMsg = out_msg.c_str();
			}
			else
				strOutMsg = buf;

			bRet = TRUE;
		}
#endif
	}
	catch (CInternetException* e)
	{
		TCHAR szError[255] = {0};
		e->GetErrorMessage(szError, 255);
		e->Delete();
		this->m_strErrorMsg = m_strRequestURL;
		this->m_strErrorMsg.Append(" Error = ");
		this->m_strErrorMsg.Append(szError);
		return bRet;
	}
	catch (CException* e)
	{
		TCHAR szError[255] = {0};
		e->GetErrorMessage(szError, 255);
		e->Delete();
		this->m_strErrorMsg = m_strRequestURL;
		this->m_strErrorMsg.Append(" Error = ");
		this->m_strErrorMsg.Append(szError);
		return bRet;
	}
	catch (...)
	{
		this->m_strErrorMsg = m_strRequestURL;
		this->m_strErrorMsg.Append(" Error = ");
		this->m_strErrorMsg.Append("Unknown Error");
		return bRet;
	}

	return bRet;
}

BOOL elWinHttpRequest::RequestGet(LPCTSTR lpUrl, CString &strOutMsg, string strHttpHeader)
{
	if( lpUrl==NULL ) return FALSE;

	CString str_ascii_url = lpUrl;
	CString str_encode_url = "";
/***
#ifdef _STT_LIB_
	ubcIni aIni("UBCVariables", "CONFIGROOT", "data");
#else
	ubcIni aIni("UBCVariables", "", "data");
#endif
	ciLong use_base64 = 0;
	aIni.get("ROOT", "USE_HTTP_BASE64", use_base64);
***/
    ciLong use_base64 = 0;
	if( str_ascii_url.Find('?') > 0 )
	{
		// exist params
		int i = 0;
		int pos = 0;
		str_encode_url = str_ascii_url.Tokenize("?", pos); // get address
		if( use_base64 )
		{
			i = 1;
			str_encode_url += "?enc=1&uid=dWJj&upd=c3FpY29w"; // "enc=1&uid=ubc&upd=sqicop" ==> encoding to base64
		}
		else
			str_encode_url += "?";

		CString str_param_list = str_ascii_url.Tokenize("?", pos); // get param list
		pos = 0;
		CString str_param = "";
		while( (str_param=str_param_list.Tokenize("&", pos)) != "" ) // get param
		{
			if( i++ > 0 ) str_encode_url += "&";
			if( str_param.Find('=') > 0 )
			{
				int pos2 = 0;
				str_encode_url += str_param.Tokenize("=", pos2);
				str_encode_url += "=";

				if( use_base64 )
					str_encode_url += ToEncodingString(AsciiToBase64(str_param.Tokenize("=", pos2)));
				else
					str_encode_url += ToEncodingString(str_param.Tokenize("=", pos2));
			}
			else
				str_encode_url += str_param;
		}
	}
	else
	{
		// no params
		str_encode_url = str_ascii_url;
		if( use_base64 )
		{
			str_encode_url += "?enc=1&uid=dWJj&upd=c3FpY29w";
		}
	}

	return Request(true, str_encode_url, NULL, strOutMsg, strHttpHeader);
}

BOOL elWinHttpRequest::RequestGet(LPCTSTR lpUrl, CStringArray &strOutMsgList, string strHttpHeader)
{
	CString strOutMsg;
	BOOL ret_val = RequestGet(lpUrl, strOutMsg, strHttpHeader);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

BOOL elWinHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg, string strHttpHeader)
{
	LPTSTR send_msg = (LPTSTR)lpszSendMsg;
	CString str_base64_param = "";
/***
#ifdef _STT_LIB_
	ubcIni aIni("UBCVariables", "CONFIGROOT", "data");
#else
	ubcIni aIni("UBCVariables", "", "data");
#endif
	ciLong use_base64 = 0;
	aIni.get("ROOT", "USE_HTTP_BASE64", use_base64);
***/
    ciLong use_base64 = 0;

	if( send_msg != NULL )
	{
		if( use_base64 ) str_base64_param = "enc=1&uid=dWJj&upd=c3FpY29w";

		CString str_param_list = send_msg; // get param list
		int pos = 0;
		CString str_param = "";
		while( (str_param=str_param_list.Tokenize("&", pos)) != "" )
		{
			if( str_base64_param.GetLength() > 0 ) str_base64_param += "&";
			if( str_param.Find('=') > 0 )
			{
				int pos2 = 0;
				str_base64_param += str_param.Tokenize("=", pos2); // get key
				str_base64_param += "=";

				if( use_base64 )
					str_base64_param += ToEncodingString(AsciiToBase64(str_param.Tokenize("=", pos2))); // get value(base64)
				else
					str_base64_param += ToEncodingString(str_param.Tokenize("=", pos2)); // get value
			}
			else
				str_base64_param += str_param;
		}

		send_msg = (LPTSTR)(LPCTSTR)str_base64_param;
	}
	else if( use_base64 )
	{
		str_base64_param = "enc=1&uid=dWJj&upd=c3FpY29w";
		send_msg = (LPTSTR)(LPCTSTR)str_base64_param;
	}

	return Request(false, lpUrl, send_msg, strOutMsg, strHttpHeader);
}

BOOL elWinHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CStringArray& strOutMsgList, string strHttpHeader)
{
	CString strOutMsg;
	BOOL ret_val = RequestPost(lpUrl, lpszSendMsg, strOutMsg, strHttpHeader);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

CString elWinHttpRequest::ToEncodingString(CString str, bool bReturnNullToString)
{
	if(str.GetLength() == 0) return (bReturnNullToString ? _T("(null)") : _T(""));
	str.Replace(_T("%"), _T("%25"));
	str.Replace(_T(" "), _T("%20"));
	str.Replace(_T("&"), _T("%26"));
	str.Replace(_T("="), _T("%3d"));
	str.Replace(_T("+"), _T("%2b"));
	str.Replace(_T("?"), _T("%3f"));
	str.Replace(_T("/"), _T("%2f"));
	return str;
}

CString elWinHttpRequest::ToString(int nValue)
{
	TCHAR buf[16] = {0};
	_stprintf(buf, _T("%d"), nValue);

	return buf;
}

void elWinHttpRequest::GetLineList(CString& str, CStringArray& line_list)
{
	int pos = 0;
	CString token = str.Tokenize(_T("\r\n"), pos);
	while(token != _T(""))
	{
		line_list.Add(token);
		token = str.Tokenize(_T("\r\n"), pos);
	}
}

#define UBC_SERVER_EXECUTE_PATH		_T("Project\\ubc\\config")
#define UBC_CLIENT_EXECUTE_PATH		_T("SQISoft\\UTV1.0\\execute")
#define UBCVARS_INI					_T("UBCVariables.ini")

CString	elWinHttpRequest::GetINIPath()
{
	TCHAR szModule[MAX_PATH+1] = {0};
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	CString program_path = szModule;
	program_path.MakeLower();

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath_s(szModule, szDrive, MAX_PATH, szPath, MAX_PATH, szFilename, MAX_PATH, szExt, MAX_PATH);

	CString strPath;
	if(program_path.Find("\\bin8") > 0)
	{
		// server
		strPath.Format("%s\\%s\\data\\%s", szDrive, UBC_SERVER_EXECUTE_PATH, UBCVARS_INI);
	}
	else
	{
		// client
		strPath.Format("%s\\%s\\data\\%s", szDrive, UBC_CLIENT_EXECUTE_PATH, UBCVARS_INI);
	}

	return strPath;
}

BOOL
elWinHttpRequest::isUseSSL(int forceValue/*=-1*/)
{
	static int iUseSSL = -1;
	static elHttpCriticalSection criticalSection;

	criticalSection.lock();
	if(forceValue >= 0){
		criticalSection.unlock();
		iUseSSL=forceValue;
		return BOOL(iUseSSL);
	}
	if(iUseSSL >= 0){
		criticalSection.unlock();
		return BOOL(iUseSSL);
	}

	CString strPath = GetINIPath();

	BOOL bUseSSL = GetPrivateProfileInt(_T("ROOT"), _T("USE_SSL"), 0, strPath);
	iUseSSL = bUseSSL;
	criticalSection.unlock();
	return bUseSSL;
}
BOOL
elWinHttpRequest::isUseApache(int forceValue/*=-1*/)
{
	static int iUseApache = -1;
	static elHttpCriticalSection criticalSection;

	criticalSection.lock();
	if(forceValue >= 0){
		criticalSection.unlock();
		iUseApache=forceValue;
		return BOOL(iUseApache);
	}
	if(iUseApache >= 0){
		criticalSection.unlock();
		return BOOL(iUseApache);
	}

	CString strPath = GetINIPath();

	BOOL bUseApache = GetPrivateProfileInt(_T("ROOT"), _T("USE_AXIS2"), 0, strPath);
	iUseApache = bUseApache;
	criticalSection.unlock();
	return bUseApache;
}
BOOL
elWinHttpRequest::IpToDomainName(const char* strHttpServerIp, CString& domain)
{
	static map<string,string>	ipDomainMap;
	static elHttpCriticalSection criticalSection;
	
	criticalSection.lock();
	map<string,string>::iterator itr = ipDomainMap.find(strHttpServerIp);
	if(itr!=ipDomainMap.end()){
		domain = itr->second.c_str();
		criticalSection.unlock();
		return true;
	}

	CString strPath = GetINIPath();

	char buf[1024] = {0};
	DWORD retval = GetPrivateProfileString(_T("SSLDomainNameList"), strHttpServerIp, "", buf, 1024, strPath);
	if(retval > 0){
		domain = buf;
		ipDomainMap.insert(map<string,string>::value_type(strHttpServerIp, (const char*)domain.GetBuffer()));
	}else{
		domain = strHttpServerIp;
	}
	
	criticalSection.unlock();
	return (retval >0 ? true : false);
}

BOOL 
elWinHttpRequest::HttpToHttps(const char* http, CString& https)
{
	if(isUseSSL() == false) //
	{
		https = http;
		return false;
	}
	if(strncmp(http, "http://", 7) != 0)
	{
		https = http;
		return false;
	}

	char* ip_end_ptr = (char*)http+7; // ip-pointer after of "http://"
	while(*ip_end_ptr != ':' && *ip_end_ptr != '/' && *ip_end_ptr != 0) // find end char (:, /, NULL)
		ip_end_ptr++;
	char ip_end_char = *ip_end_ptr;

	*ip_end_ptr = 0;
	string ip = http+7;

	*ip_end_ptr = ip_end_char;
	string url = ip_end_ptr;

	CString domainName;
	if(IpToDomainName(ip.c_str(), domainName))
	{
		https = "https://";
		https += domainName;
		https += url.c_str();

		return true;
	}
	else
	{
		https = http;
	}

	return false;
}

CString elWinHttpRequest::AsciiToBase64(CString strAscii)
{
	int nDestLen = Base64EncodeGetRequiredLength(strAscii.GetLength());

	CString strBase64;
	Base64Encode((const BYTE*)(LPCSTR)strAscii, strAscii.GetLength(),strBase64.GetBuffer(nDestLen), &nDestLen);
	strBase64.ReleaseBuffer(nDestLen);

	return strBase64;
}

CString elWinHttpRequest::Base64ToAscii(CString strBase64)
{
	int nDecLen = Base64DecodeGetRequiredLength(strBase64.GetLength());

	CString strAscii;
	Base64Decode(strBase64, strBase64.GetLength(), (BYTE*)strAscii.GetBuffer(nDecLen), &nDecLen);
	strAscii.ReleaseBuffer(nDecLen);
	return strAscii;
}

//////////////////////////////////////////////////////////
// elHttpCriticalSection
//////////////////////////////////////////////////////////

elHttpCriticalSection::elHttpCriticalSection() {
	::InitializeCriticalSection(&_handle);
}

elHttpCriticalSection::~elHttpCriticalSection() {
	::DeleteCriticalSection(&_handle);
}

void elHttpCriticalSection::lock() {
	::EnterCriticalSection(&_handle);
}

void elHttpCriticalSection::unlock() {
	::LeaveCriticalSection(&_handle);
}
