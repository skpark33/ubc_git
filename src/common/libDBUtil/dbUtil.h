#ifndef _dbUtil_h_
#define _dbUtil_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>

class dbUtil {
public:
	static dbUtil*	getInstance();
	static void	clearInstance();

	virtual ~dbUtil() ;

	ciBoolean	changePMO(ciBoolean force, const char* mgrId);
#ifdef _COP_MSC_
	ciBoolean	changeServerIP(const char* ip,ciBoolean isServer=ciTrue);
#endif
	ciBoolean	hostAutoUpdate(ciString& siteId,ciString& enterpriseKey, ciString& password, ciString& hostId, ciString& customer);

	void		setErr(int code, const char* fmt,...);

	const char* getErrString() { return _errString.c_str();}
	int getErrCode() { return _errCode; }

protected:
	dbUtil();

	static dbUtil*	_instance;
	static ciMutex			_instanceLock;

	ciString _errString;
	int		 _errCode;
};

#endif // _dbUtil_h_
