#include "stdafx.h"

#include "HttpDownload.h"
#include <string>
#include <util/libEncoding/Encoding.h>

#define		DOWNLOAD_CHUNK_SIZE		1024*16   // 16k씩 받는다.

bool
CHttpDownload::Example()
{
	CHttpDownload aObj;

	if(!aObj.InternetOpen()){
		return false;
	}


	aObj.SetURL("HTTP://211.232.57.163:8080");
	aObj.SetSourceDir("announce");
	aObj.SetTargetDir("C:\\Project\\Temp\\test1");

	aObj.DownloadFile("foo_baby1.avi");
	aObj.DownloadFile("wileLife.wmv");

	aObj.InternetClose();

}

CHttpDownload::CHttpDownload(IHttpDownloadUI* ui/*=0*/) 
{ 
	m_hInternet  = 0;
	m_hOpenURL = 0;
	m_ui = ui;

	m_stop = false;

	setErr();
}

CHttpDownload::~CHttpDownload() 
{	
	InternetClose(); 
}


void		
CHttpDownload::SetTargetDir(CString dir) 
{ 
	m_targetDir = dir; 
	makeFolder(m_targetDir); 
}

bool	
CHttpDownload::InternetOpen()
{
	try{
		if(!(m_hInternet = ::InternetOpen("HTTP Download", INTERNET_OPEN_TYPE_PRECONFIG , NULL, NULL, NULL))){
			 setErr(NETWORK_CONNECTION_FAIL, "Error : Network connection failed");
			 if(m_ui) m_ui->OnConnectError(m_errCode,m_errMsg);
 			 return false;
		 }
		 setErr();

		 // 서버당 커넥션 수를 100개로 늘려준다.
		DWORD dwData = 0;
		DWORD dwSize = sizeof(dwData);
		//InternetQueryOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_SERVER, &dwData, &dwSize);

		//ciDEBUG(1,("BEFORE INTERNET_OPTION_MAX_CONNS_PER_SERVER=%ld", dwData));

		dwData = 100;
		InternetSetOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_SERVER, &dwData, sizeof(dwData));

		//InternetQueryOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_SERVER, &dwData, &dwSize);
		//ciDEBUG(1,("AFTER INTERNET_OPTION_MAX_CONNS_PER_SERVER=%ld", dwData));


		//InternetQueryOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_1_0_SERVER, &dwData, &dwSize);
		//ciDEBUG(1,("BEFORE INTERNET_OPTION_MAX_CONNS_PER_1_0_SERVER=%ld", dwData));

		dwData = 100;
		InternetSetOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_1_0_SERVER, &dwData, sizeof(dwData));

		//InternetQueryOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_1_0_SERVER, &dwData, &dwSize);
		//ciDEBUG(1,("AFTER INTERNET_OPTION_MAX_CONNS_PER_1_0_SERVER=%ld", dwData));

		return true;
	}catch (CString err) {
		setErr(EXCEPTION_CATCEHD, err);
		return false;
	}
	return false;

}

void
CHttpDownload::InternetClose()
{
	try{
		if(m_hOpenURL){
			InternetCloseHandle(m_hOpenURL);
			m_hOpenURL = 0;
		}
		if(m_hInternet){
			InternetCloseHandle(m_hInternet);
			m_hInternet  = 0;
		}
		setErr();
	}catch (CString err) {
		setErr(EXCEPTION_CATCEHD, err);
	}
}

bool		
CHttpDownload::ConnectTest()
{
	try{
		 CString url = m_url;

		 HINTERNET hOpenURL = InternetOpenUrl( m_hInternet, url, NULL, 0, INTERNET_FLAG_RELOAD, 0 );

		 if(hOpenURL == 0)
		 {
			m_errMsg.Format("Error : Open URL(%s) failed ", url);
			m_errCode = URL_OPEN_FAILED;

			if(m_ui) m_ui->OnConnectError(m_errCode,m_errMsg);
			return false;
		 }

		 // 연결정보 확인
		 
		 TCHAR szStatusCode[20];
		 DWORD dwCodeSize = 20;
		 HttpQueryInfo(hOpenURL, HTTP_QUERY_STATUS_CODE, szStatusCode, &dwCodeSize, NULL);
		 long nStatusCode = _ttol(szStatusCode);
		 
		 if (nStatusCode != HTTP_STATUS_OK) // 정상 적으로 연결안됨
		 {    
			m_errMsg.Format("HTTP %d Error : URL(%s) open failed", nStatusCode, url);
			m_errCode = HTTP_ERROR;

			InternetCloseHandle(hOpenURL);
			if(m_ui) m_ui->OnConnectError(m_errCode,m_errMsg);
			return false;
		 }
		 
		 InternetCloseHandle(hOpenURL);
		 return true;
	}catch (CString err) {
		setErr(EXCEPTION_CATCEHD, err);
		return false;
	}
	return false;
}

int		
CHttpDownload::DownloadFile(CString filename, unsigned long ulGivenFileSize)
{
	try {

		if(m_hOpenURL){
			InternetCloseHandle(m_hOpenURL);
			m_hOpenURL = 0;
		}

		 CString url = m_url;
		 url.Append("/");
		 url.Append(m_sourceDir);
		 url.Append("/");
		 url.Append(filename);
		
		 if(!this->_EncodingToUTF8(url)){
			return -1;
		 }
		 
		this->_URLEncoding(url);

		 if(!(m_hOpenURL = InternetOpenUrl( m_hInternet, url, NULL, 0, INTERNET_FLAG_RELOAD, 0 )))
		 {
			m_errMsg.Format("Error : Open URL(%s) failed ", url);
			m_errCode = URL_OPEN_FAILED;

			InternetCloseHandle(m_hOpenURL);
			m_hOpenURL = 0;
			if(m_ui) m_ui->OnConnectError(m_errCode,m_errMsg);
			return -2;
		 }

		 // 연결정보 확인
		 
		 TCHAR szStatusCode[20];
		 DWORD dwCodeSize = 20;
		 HttpQueryInfo(m_hOpenURL, HTTP_QUERY_STATUS_CODE, szStatusCode, &dwCodeSize, NULL);
		 long nStatusCode = _ttol(szStatusCode);
		 
		 if (nStatusCode != HTTP_STATUS_OK) // 정상 적으로 연결안됨
		 {    
			m_errMsg.Format("HTTP %d Error : URL(%s) open failed", nStatusCode, url);
			m_errCode = HTTP_ERROR;

			InternetCloseHandle(m_hOpenURL);
			m_hOpenURL = 0;
			if(m_ui) m_ui->OnConnectError(m_errCode,m_errMsg);
			return -3;
		 }

		// 파일용량 확인
		TCHAR szFileSizeBuff[1024];
		//wchar_t szFileSizeBuff[1024];
		memset(szFileSizeBuff,0x00,sizeof(szFileSizeBuff));
		DWORD dwBuffSize = sizeof(szFileSizeBuff);

		//if(!InternetQueryDataAvailable(m_hOpenURL, &dwBuffSize, 0, 0)){
		if(!HttpQueryInfo(m_hOpenURL, HTTP_QUERY_CONTENT_LENGTH, szFileSizeBuff, &dwBuffSize, NULL)){
			m_errMsg.Format("HTTP Error : URL(%s) file size get failed 1",url);
			m_errCode = FILE_SIZE_ERROR;
			return -4;
		
		}
		//unsigned long ulFileSize = dwBuffSize; 
		unsigned long ulFileSize = strtoul(szFileSizeBuff,NULL,10);  
		if(ulFileSize == 0){
			m_errMsg.Format("HTTP Error : URL(%s) file size get failed 2",url);
			m_errCode = FILE_SIZE_ERROR;
			return -5;
		}


		if(m_ui) m_ui->OnBeforeDownload(filename,ulFileSize);


		CString targetFile = m_targetDir;
		targetFile.Append("\\");
		targetFile.Append(filename);

		// 다운 받을 파일이 있으면 0, 파일이 없으면 -1;
		if(::access(targetFile,0) == 0) // 다운받을 파일이 이미 있다면
		{
			// 이미 있는 파일용량을 확인해 모두 받아 졌는지 검사
			WIN32_FIND_DATA fi;
			FindFirstFile(targetFile,&fi);
			unsigned long ulLocalFilesize = fi.nFileSizeHigh + fi.nFileSizeLow;
		 
			if(ulFileSize == ulLocalFilesize) //받은 파일 용량이 받을 파일 용량과 같을 경우 
			{
				m_errMsg.Format("Local File Size: %d == Server File Size: %d, File(%s) already downloaded",ulLocalFilesize,ulFileSize, targetFile);
				m_errCode = ALEADY_EXIST;
				InternetCloseHandle(m_hOpenURL);
				m_hOpenURL = 0;
				if(m_ui) m_ui->OnAfterDownload(m_errCode, m_errMsg,filename,ulFileSize,ulLocalFilesize);
				return 0;
			}
			 // 파일 용량이 다를경우

			if(ulGivenFileSize> 0 && ulGivenFileSize != ulFileSize){
				// 그런데, 만약 ini 의 파일 사이즈와 CacheServer 에 있는 fileSize 가
				// 다르다면, Cache Server 에는 올바른 컨텐츠가 있는 것이 아니므로
				// 다운로드 받아서는 안된다.
				m_errMsg.Format("ini File Size: %d != Server File Size: %d, File(%s) in Cache Server is not latest one",ulGivenFileSize,ulFileSize, targetFile);
				m_errCode = FILE_NOT_LATEST;
				InternetCloseHandle(m_hOpenURL);
				m_hOpenURL = 0;
				if(m_ui) m_ui->OnAfterDownload(m_errCode, m_errMsg,filename,ulFileSize,ulLocalFilesize);

				return -6;
			}
		}

		// 파일 새로 생성
		HANDLE hFile = CreateFile( targetFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
		if (hFile==INVALID_HANDLE_VALUE)
		{
			m_errMsg.Format("Local File(%s) create failed", targetFile);
			m_errCode = FILE_CREATE_FAIL;

			InternetCloseHandle(m_hOpenURL);
			m_hOpenURL = 0;
			if(m_ui) m_ui->OnAfterDownload(m_errCode, m_errMsg,filename,ulFileSize,0);
			return -7;
		}

		INTERNET_BUFFERS ib;
		char *m_pBuffer[DOWNLOAD_CHUNK_SIZE] = {NULL}; // Read form file in 16k chunks
		 
		//ResetBuffer;
		memset(&ib, 0, sizeof(INTERNET_BUFFERS));
		ib.dwStructSize = sizeof(INTERNET_BUFFERS);
		ib.lpvBuffer = m_pBuffer;
		ib.dwBufferLength = DOWNLOAD_CHUNK_SIZE;
		 
		DWORD dwWritten=0;
		unsigned long Recv = 0;
		BOOL bOk = TRUE;
		 
	   // Data 다운로드
		do
		{
			bOk = InternetReadFileEx( m_hOpenURL, &ib, NULL, NULL);
			if(!bOk && GetLastError()==ERROR_IO_PENDING)
			{ 
				bOk = TRUE;  
			}   
			// 읽은 파일 쓰기
			WriteFile( hFile, ib.lpvBuffer, ib.dwBufferLength, &dwWritten, NULL );
			// 다운받은 양
			Recv += (unsigned long)dwWritten;
			if(m_ui) m_ui->OnDownload(filename,ulFileSize,Recv);

		}while( !m_stop && bOk && (ib.dwBufferLength !=0 ) );
		CloseHandle(hFile);

		if(Recv != ulFileSize){
			m_errMsg.Format("Download ERROR : %s file Transfer fail : Recv size=%ld != File size=%ld", targetFile, Recv, ulFileSize);
			m_errCode = FILE_DOWNLOAD_FAIL;
			InternetCloseHandle(m_hOpenURL);
			m_hOpenURL = 0;
			if(m_ui) m_ui->OnAfterDownload(m_errCode, m_errMsg,filename,ulFileSize,Recv);
			return -8;
		}

		m_errMsg.Format("Download Complete : %s file Transfer complete : Recv size=%ld == File size=%ld",
			targetFile, Recv, ulFileSize);
		m_errCode = DOWNLOAD_SUCCEED;
		 
		InternetCloseHandle(m_hOpenURL);
		m_hOpenURL = 0;
		if(m_ui) m_ui->OnAfterDownload(m_errCode, m_errMsg,filename,ulFileSize,Recv);
		return 1;
	}catch (CString err) {
		setErr(EXCEPTION_CATCEHD, err);
		return -9;
	}
	return -10;
}

void CHttpDownload::_URLEncoding(CString& url)
{
	char output[4096];
	memset(output,0x00,4096);

	const char* input = url;

	int opt_inx, ipt_inx;
    for ( ipt_inx = 0 , opt_inx =0 ; input[ipt_inx]; ipt_inx++,opt_inx++)
    {
        int char_val = input[ipt_inx]; 

        if ( char_val < 0 )
		{
			char_val +=256; 
		}

		if (char_val <= 0x1F || 
            char_val == 0x7F ||
            char_val >=0x80 ||
            char_val == ' ' ||
            char_val == '{' ||
            char_val == '}' ||
            char_val == '[' ||
            char_val == ']' ||
            char_val == '^' ||
            char_val == '`' ||
            char_val == '#' ||
            char_val == ';' ||
            char_val == '?' ||
            char_val == '@' ||
            char_val == '=' ||
            char_val == '&'     )
//        if( char_val >=0x80 || 
//            char_val == ' ' )
        {
            output[opt_inx]='%';

			int UpperBit = char_val / 0x10;
			if (UpperBit >=0 && UpperBit <=9 )
			{
                output[++opt_inx]= UpperBit+'0';
			}
            else 
			{
                output[++opt_inx]= UpperBit+'A'-10;
			}


            int LowerBit = char_val % 0x10;
            if (LowerBit >=0 && LowerBit <=9 )
			{
                output[++opt_inx]= LowerBit+'0';
			}
            else 
			{
                output[++opt_inx]= LowerBit+'A'-10;
			}
        }
        else
		{
            output[opt_inx]=char_val;    
		}
    }
    output[opt_inx]=0;
	url = output;
}

bool		
CHttpDownload::_EncodingToUTF8(CString& url) 
{ 
	std::string utf8str;
	if(!CEncoding::getInstance()->AnsiToUTF8(url, utf8str)){
		setErr(ENCODING_FAIL,"URL Encoding Fail");
		return false;
	}
	url = utf8str.c_str(); 
	return true;
}
