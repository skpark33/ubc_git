// HttpRequestTesterDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CHttpRequestTesterDlg dialog
class CHttpRequestTesterDlg : public CDialog
{
// Construction
public:
	CHttpRequestTesterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_HTTPREQUESTTESTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	virtual void OnOK();
public:
	CComboBox m_cbxServer;
	CEdit m_editServerIP;
	CEdit m_editServerPort;
	CEdit m_editURL;
	CEdit m_editSSLUrl;
	CEdit m_editUseSSL;
	CEdit m_editRequest;
	CEdit m_editResult;
	afx_msg void OnBnClickedButtonRequestGet();
};
