// HttpRequestTesterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HttpRequestTester.h"
#include "HttpRequestTesterDlg.h"

#include "../HttpRequest.h"

#include "ci/libConfig/ciIni.h"
#include "ci/libDebug/ciDebug.h"

ciSET_DEBUG(10,"CHttpRequestTesterDlg");

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CHttpRequestTesterDlg dialog




CHttpRequestTesterDlg::CHttpRequestTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHttpRequestTesterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CHttpRequestTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_SERVER, m_cbxServer);
	DDX_Control(pDX, IDC_EDIT_SERVER_IP, m_editServerIP);
	DDX_Control(pDX, IDC_EDIT_SERVER_PORT, m_editServerPort);
	DDX_Control(pDX, IDC_EDIT_URL, m_editURL);
	DDX_Control(pDX, IDC_EDIT_SSL_URL, m_editSSLUrl);
	DDX_Control(pDX, IDC_EDIT_USE_SSL, m_editUseSSL);
	DDX_Control(pDX, IDC_EDIT_REQUEST, m_editRequest);
	DDX_Control(pDX, IDC_EDIT_RESULT, m_editResult);
}

BEGIN_MESSAGE_MAP(CHttpRequestTesterDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_REQUEST_GET, &CHttpRequestTesterDlg::OnBnClickedButtonRequestGet)
END_MESSAGE_MAP()


// CHttpRequestTesterDlg message handlers

BOOL CHttpRequestTesterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	m_cbxServer.InsertString(0, "User Defined");
	m_cbxServer.InsertString(1, "UBC Center");
	m_cbxServer.InsertString(2, "UBC Server");
	m_cbxServer.SetCurSel(2);

	m_editUseSSL.SetWindowText(ciIniUtil::isUseSSL() ? "True" : "False");

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CHttpRequestTesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CHttpRequestTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CHttpRequestTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CHttpRequestTesterDlg::OnOK() // RequestPost
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();

	int server_idx = m_cbxServer.GetCurSel();

	CString server_ip;
	m_editServerIP.GetWindowText(server_ip);

	CString server_port;
	m_editServerPort.GetWindowText(server_port);

	CString str_url;
	m_editURL.GetWindowText(str_url);

	CString str_request;
	m_editRequest.GetWindowText(str_request);

	//
	CHttpRequest http_req;
	http_req.Init((CHttpRequest::CONNECTION_TYPE)server_idx, server_ip, _ttoi(server_port));

	CString str_result;
	BOOL ret_val = http_req.RequestPost(str_url, str_request, str_result);

	m_editResult.SetWindowText(str_result);

	if(ret_val)
		::AfxMessageBox("Success", MB_ICONINFORMATION);
	else
		::AfxMessageBox("FAIL !!!", MB_ICONSTOP);

	if(ciIniUtil::isUseSSL())
		m_editSSLUrl.SetWindowText(http_req.GetRequestURL());
}

void CHttpRequestTesterDlg::OnBnClickedButtonRequestGet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int server_idx = m_cbxServer.GetCurSel();

	CString server_ip;
	m_editServerIP.GetWindowText(server_ip);

	CString server_port;
	m_editServerPort.GetWindowText(server_port);

	CString str_url;
	m_editURL.GetWindowText(str_url);

	//
	CHttpRequest http_req;
	http_req.Init((CHttpRequest::CONNECTION_TYPE)server_idx, server_ip, _ttoi(server_port));

	CString str_result;
	BOOL ret_val = http_req.RequestGet(str_url, str_result);

	m_editResult.SetWindowText(str_result);

	if(ret_val)
		::AfxMessageBox("Success", MB_ICONINFORMATION);
	else
		::AfxMessageBox("FAIL !!!", MB_ICONSTOP);

	if(ciIniUtil::isUseSSL())
		m_editSSLUrl.SetWindowText(http_req.GetRequestURL());
}
