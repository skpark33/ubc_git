// HttpRequestTester.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CHttpRequestTesterApp:
// See HttpRequestTester.cpp for the implementation of this class
//

class CHttpRequestTesterApp : public CWinApp
{
public:
	CHttpRequestTesterApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CHttpRequestTesterApp theApp;