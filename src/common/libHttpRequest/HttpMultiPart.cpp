#include "stdafx.h"
#include "HttpMultiPart.h"
#include "ci/libDebug/ciDebug.h"

ciSET_DEBUG(10, "HttpMultiPart");

size_t 
HttpMultiPart::send_file(const char* fileDir, const char *filename)
{
    FILE *fp = fopen(fileDir, "rb");
    if(fp == NULL){
        ciDEBUG(1,("%s file can't read\n", fileDir));
        fclose(fp);
        return -1;
    }
 
    fseek(fp, 0, SEEK_END);
    int file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

	ciDEBUG(1,("send file name : %s\n", fileDir));
    char *fileBuf = (char*)calloc(sizeof(char), file_size+1);
	memset(fileBuf,0x00,file_size+1);

    if(fread(fileBuf, 1, file_size, fp) == -1){
		ciDEBUG(1,("fread error : \n", fileDir));
		free(fileBuf);
		fclose(fp);
		return -1;
    }
	fclose(fp);

	int read_size = strlen(fileBuf);

	ciDEBUG(1,("fileSize=%d, readSize=%d", file_size, read_size));

	size_t retval = send_byte(fileBuf,file_size,filename);
	free(fileBuf);
	return retval;
}

size_t 
HttpMultiPart::make_pre_body(char *boundary, const char *filename, CString& outval)
{
	ciDEBUG(1,("make_pre_body()"));

	char pre_body[4096];
	sprintf(pre_body, 
		"--%s\r\nContent-Disposition: form-data; name=\"siteId\"\r\n\r\n%s\r\n"
		"--%s\r\nContent-Disposition: form-data; name=\"hostId\"\r\n\r\n%s\r\n"
		"--%s\r\nContent-Disposition: form-data; name=\"returnType\"\r\n\r\n%s\r\n"
		"--%s\r\nContent-Disposition: form-data; name=\"upfile\";filename=\"%s\"\r\n"
		"Content-Type:application/octect-stream\r\n\r\n"
//		"Content-Type:text/plain\r\n\r\n"
		,	boundary, siteId
		,	boundary, hostId
		,	boundary, "XML"
		,	boundary, filename
	);
	outval = pre_body;
	return strlen(pre_body);
}

size_t 
HttpMultiPart::send_byte(char *fileBuf, size_t file_size,  const char *filename)
{
	ciDEBUG(1,("send_byte()"));

	char boundary[] ="----WebKitFormBoundaryu8FzpUGNDgydoA4z";
    CString pre_body;
	int pre_len = make_pre_body(boundary, filename, pre_body);

    char post_body[4096];
    sprintf(post_body, "\r\n--%s--\r\n", boundary);
    int post_len = strlen(post_body);

    int body_len = pre_len + file_size + post_len;

    char *bodyline = (char*)calloc(sizeof(char), body_len+1);

	memset(bodyline,0x00,body_len+1);
	memcpy(bodyline, pre_body, pre_len);
    memcpy(bodyline+pre_len, fileBuf, file_size);
    memcpy(bodyline+pre_len+file_size, post_body, post_len);

    char headline[4096];
 
    //make header
    sprintf(headline, "POST %s HTTP/1.1\r\n"
		"Host: %s:%d\r\nConnection: keep-alive\r\n"
        "Content-Length: %d\r\n"
        "Content-Type: multipart/form-data; boundary=%s\r\n"
		"Accept-Charset: utf-8\r\n\r\n"
		, jsp, servIP, servPort, body_len, boundary);
    int head_len = strlen(headline);
    int packet_len = head_len+body_len;

	char *packet = (char*)calloc(sizeof(char), packet_len+1);
	memset(packet,0x00,packet_len+1);
	memcpy(packet, headline, head_len);
    memcpy(packet+head_len, bodyline, body_len);

	free(bodyline);

	int sock = _makeSocket();
	if(sock <= 0){
		ciERROR(("socket error"));
		free(packet);
		return -2;
	}

	int flag = 1; 
	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof(int));

	ciDEBUG(1,("before write()"));

	int sent_len = 0;
	int send_left = packet_len;
	while(send_left > 0){
		//int write_len = write(sock, packet, packet_len);
		int sending_byte = send_left;
		if(sending_byte > max_buffer) {
			sending_byte = max_buffer;
		}
		int write_len = send(sock, packet+sent_len, sending_byte,0);
		if(write_len <= 0) {
			ciERROR(("send error : %d", WSAGetLastError()));
			return -3;
		}
		send_left = send_left - write_len;
		sent_len += write_len;
	}
	ciDEBUG(1,("after write()"));
 
	if(sent_len != packet_len) {
		ciWARN2(("sent something wrong, packet_len(%d) != sent_len(%d)", packet_len, sent_len));
		return  (sent_len - packet_len);
	}
/*
	int padding_len = sent_len % max_buffer;
	if(padding_len > 0) {
		char* dummy = (char*)calloc(sizeof(char), padding_len+1);
		memset(dummy,0x00,padding_len+1);
		send(sock, dummy, padding_len,0);
		free(dummy);
	}
*/	
	//::Sleep(1000);
    
	shutdown(sock, SD_BOTH);
	closesocket(sock);
    free(packet);

	return sent_len;
}


int
HttpMultiPart::_makeSocket()
{
	ciDEBUG(1,("_makeSocket(%s,%d)", servIP, servPort));
    struct sockaddr_in servAddr;
    int sock;
    char ip[20];
    /* Create areliable, stream socket using TCP */
	if((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        ciERROR(("socket create failed\n"));
		return -1;
	}
    struct hostent *host_entry;
    host_entry = gethostbyname(servIP);
    for(int ndx = 0; NULL != host_entry->h_addr_list[ndx]; ndx++){
        strcpy(ip, inet_ntoa(*(struct in_addr*)host_entry->h_addr_list[ndx]));
    }
     
    memset(&servAddr, 0, sizeof(servAddr)); //Zero ou structure
    servAddr.sin_family = AF_INET; //Internet address family
    servAddr.sin_addr.s_addr = inet_addr(ip); //Server IP address
    servAddr.sin_port = htons(servPort);//Server port
 
    /* Establish the connection to the web server */
	if(connect(sock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
		ciERROR(("connect() failed\n"));
		close(sock);
		return -2;
	}

	int iVal = 0;
    int iSize = sizeof(iVal);
    getsockopt(sock, SOL_SOCKET, SO_SNDBUF , (char *)&iVal, &iSize);
 
	if(iVal > 0) {
		max_buffer = iVal;
	}

    ciDEBUG(1, ("The max message size = %d", max_buffer));

	return sock;
}
 

