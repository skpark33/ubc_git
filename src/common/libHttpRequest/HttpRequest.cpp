// HttpRequest.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HttpRequest.h"

#include <afxinet.h>
#include <atlenc.h>

#include <ci/libConfig/ciIni.h>
#include <ci/libDebug/ciDebug.h>
#ifdef _STT_LIB_
#include <UBC/ubcIni.h>
#else
#include <common/libCommon/ubcIni.h>
#endif

#include <util/libEncoding/Encoding.h>
//#include <ci/libAes256/ciElCryptoAes256.h>

#define		DEFAULT_CENTER_IP			_T("ubccenter.sqisoft.com")
#define		DEFAULT_CENTER_WEBPORT		8080

#define		DEFAULT_SERVER_IP			_T("127.0.0.1")
#define		DEFAULT_SERVER_WEBPORT		8080

#define		HTTP_SESSION_TIMEOUT_SEC	(60)//*1000)	// 1min


int CHttpRequest::m_nSessionTimeout = -1;


// CHttpRequest

CHttpRequest::CHttpRequest()
:	m_connType ( CONNECTION_USER_DEFINED )
,	m_strServerIP ( DEFAULT_SERVER_IP )
,	m_nServerPort ( DEFAULT_SERVER_WEBPORT )
,	m_bBinaryMode ( false )
,	m_nDownloadedSize ( 0 )
,	m_pDownloadedSize ( NULL )
{
	if( m_nSessionTimeout < 0 )
	{
		m_nSessionTimeout = GetPrivateProfileInt(_T("ROOT"), _T("HTTP_SESSION_TIMEOUT_SEC"), HTTP_SESSION_TIMEOUT_SEC, GetINIPath());
		if( m_nSessionTimeout < 5 ) m_nSessionTimeout = 5;

		m_nSessionTimeout *= 1000; // sec ==> msec
	}
}

CHttpRequest::~CHttpRequest()
{
}

BOOL CHttpRequest::Init(CONNECTION_TYPE conType, LPCTSTR lpszServerIP, UINT nServerPort)
{
	m_connType = conType;

	switch(conType)
	{
	case CONNECTION_USER_DEFINED:
		m_strServerIP = (lpszServerIP ? lpszServerIP : DEFAULT_SERVER_IP);
		m_nServerPort = (nServerPort ? nServerPort : DEFAULT_SERVER_WEBPORT);
		break;

	case CONNECTION_CENTER:
		{
			//ubcConfig aIni("UBCConnect");
#ifdef _STT_LIB_
			ubcIni aIni("UBCConnect", "CONFIGROOT", "data");
#else
			ubcIni aIni("UBCConnect", "", "data");
#endif
			ciString center_ip="", center_port="";

			//
			aIni.get("UBCCENTER", "IP", center_ip);
			m_strServerIP = ( center_ip.length()>0 ? center_ip.c_str() : DEFAULT_CENTER_IP );

			//
			aIni.get("UBCCENTER", "WEBPORT", center_port);
			m_nServerPort = ( center_port.length()>0 ? atoi(center_port.c_str()) : DEFAULT_CENTER_WEBPORT );
		}
		break;

	case CONNECTION_SERVER:
		{
			//ubcConfig aIni("UBCConnect");
#ifdef _STT_LIB_
			ubcIni aIni("UBCConnect", "CONFIGROOT", "data");
#else
			ubcIni aIni("UBCConnect", "", "data");
#endif
			ciString server_ip="", server_port="";

			//
			aIni.get("ORB_NAMESERVICE", "IP", server_ip);
			m_strServerIP = ( server_ip.length()>0 ? server_ip.c_str() : DEFAULT_SERVER_IP );

			//
			aIni.get("AGENTMUX", "HTTP_PORT", server_port);
			m_nServerPort = ( server_port.length()>0 ? atoi(server_port.c_str()) : DEFAULT_SERVER_WEBPORT );
		}
		break;

	default:
		return FALSE;
		break;
	}
	return TRUE;
}

BOOL CHttpRequest::Request(bool isGet, LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg)
{
	CString strUrl;
	if(isUseApache() && m_connType!=CONNECTION_CENTER){ // center is only use ASP
		strUrl =  lpUrl;
		int urlSize = strUrl.GetLength();
		CString ext,name;
		if(urlSize > 4) {
			name = strUrl.Mid(0,urlSize-3);
			ext = strUrl.Mid(urlSize-3);
			if(ext.CompareNoCase("asp")==0){
				strUrl = name + "php";
				lpUrl = strUrl;
			}
		}
	}

	//
	if( _tcsnicmp( lpUrl, _T("http://"), 7) )
	{
		m_strRequestURL.Format(_T("http://%s:%d%s%s"), 
			m_strServerIP,
			m_nServerPort,
			(lpUrl[0] == _T('/') ? "" : _T("/")),
			lpUrl
		);
	}
	else
	{
		m_strRequestURL = lpUrl;
	}

	//
	bool is_https = false;
	if(m_connType != CONNECTION_CENTER) // center => only use http, not https!!!
	{
		//ciString https;
		//if(ciIniUtil::HttpToHttps(m_strRequestURL, https))
		//{
		//	m_strRequestURL = https.c_str();
		//}
		CString https;
		if(HttpToHttps(m_strRequestURL, https))
		{
			is_https = true;
			m_strRequestURL = https;
		}
	}

	//
	//if( ciIniUtil::encriptMethod()>0 && m_connType!=CONNECTION_CENTER ) // center is only use ASP
	//{
	//	// use AES256 encrypt
	//	if( m_strRequestURL.Find(".aspx") < 0 )
	//	{
	//		// .asp --> .aspx
	//		m_strRequestURL.Replace(".asp", ".aspx");
	//	}
	//}

	//
	DWORD dwSearviceType;
	CString strServer, strObject;
	INTERNET_PORT nPort;

	if(!AfxParseURL(m_strRequestURL, dwSearviceType, strServer, strObject, nPort))
	{
		m_strErrorMsg = "1";
		return FALSE;
	}

	BOOL bRet = FALSE;

	try
	{
		CInternetSession Session;
		Session.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT,			(DWORD)m_nSessionTimeout);
		Session.SetOption(INTERNET_OPTION_CONTROL_RECEIVE_TIMEOUT,	(DWORD)m_nSessionTimeout );
		Session.SetOption(INTERNET_OPTION_CONTROL_SEND_TIMEOUT,		(DWORD)m_nSessionTimeout );
		Session.SetOption(INTERNET_OPTION_DATA_SEND_TIMEOUT,		(DWORD)m_nSessionTimeout );
		Session.SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT,		(DWORD)m_nSessionTimeout );

		CHttpConnection* pServer = Session.GetHttpConnection(strServer, nPort);
		if(pServer==NULL)
		{
			m_strErrorMsg = "2";
			return FALSE;
		}
		CBufferDeleter hc_deleter(pServer);

		DWORD flag;
		if(isUseSSL() && is_https)
			flag = INTERNET_FLAG_EXISTING_CONNECT | INTERNET_FLAG_DONT_CACHE | INTERNET_FLAG_SECURE; // | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID | INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
		else
			flag = INTERNET_FLAG_EXISTING_CONNECT | INTERNET_FLAG_DONT_CACHE;

		CHttpFile *pFile = NULL;
		if(isGet)
		{
			CString strHeader = "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\nAccept: */*\r\n";
			if( m_strAdditionalHeader.GetLength() > 0 ) strHeader.Insert(0, m_strAdditionalHeader);
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_GET, strObject, NULL, 1, NULL, NULL, flag);
			if(pFile==NULL)
			{
				m_strErrorMsg = "3";
				return FALSE;
			}
			pFile->SendRequest(strHeader, (LPVOID)(LPCTSTR)strHeader, strHeader.GetLength());
		}
		else
		{
			CString strHeader = "Content-Type: application/x-www-form-urlencoded\r\nUser-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
			if( m_strAdditionalHeader.GetLength() > 0 ) strHeader.Insert(0, m_strAdditionalHeader);
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_POST, strObject, NULL, 1, NULL, NULL, flag);
			if(pFile==NULL)
			{
				m_strErrorMsg = "4";
				return FALSE;
			}
			pFile->SendRequest(strHeader, (LPVOID)lpszSendMsg, _tcslen(lpszSendMsg));
		}
		CBufferDeleter hf_deleter(pFile);

		if( m_bBinaryMode == false )
		{
			//
			char szLen[32]="";
			DWORD dwLenSize = sizeof(szLen);
			pFile->QueryInfo( HTTP_QUERY_CONTENT_LENGTH, szLen, &dwLenSize );
			int length = atoi( szLen);

			TCHAR* buf = NULL;
			DWORD dwReadSize = 0;
			if( length > 0 )
			{
				buf = new TCHAR[length+1];
				::ZeroMemory(buf, sizeof(TCHAR)*(length+1));
				dwReadSize = pFile->Read(buf, length);
			}
			else
			{
				int buf_size = 1024*1024; // 1MB init
				buf = new TCHAR[buf_size+1];
				::ZeroMemory(buf, buf_size+1);

				while(true)
				{
					char tmp[1024];
					::ZeroMemory(tmp, 1024);

					int read_size = pFile->Read(tmp, 1024);
					if( read_size==0 ) break;

					if( dwReadSize+read_size > buf_size )
					{
						buf_size *= 2;
						char* swap = new char[buf_size+1];
						::ZeroMemory(swap, buf_size+1);
						::memcpy(swap, buf, dwReadSize);
						delete[]buf;
						buf = swap;
					}

					::memcpy(buf+dwReadSize, tmp, read_size);
					dwReadSize+=read_size;
				}
			}
			CBufferDeleter buf_deleter(buf);

			//
			//if( ciIniUtil::encriptMethod()>0 && m_connType!=CONNECTION_CENTER ) // center is only use ASP
			//{
			//	// use AES256 decrypt
			//	//ciElCryptoAes256 aes256;
			//	//strOutMsg = aes256.Decrypt((LPCSTR)buf).c_str();
			//	//strOutMsg = ciAes256Util::Decrypt((LPCSTR)buf).c_str();

			//	bRet = TRUE;
			//}

			//
			if( dwReadSize != strlen(buf) || 
				(buf[0] == '<' && strnicmp("<?xml", buf, 5) != 0) || // not xml
				(buf[3] == '<' && strnicmp("<?xml", buf+3, 5) != 0) // not xml
			  )
			{
				// html tag, this module can't receive html result --> something wrong
				bRet = FALSE;
				m_strErrorMsg = buf;
			}
			else
			{
				const char* utf8_idx = strstr(buf, "CodePage=utf-8");

				if( utf8_idx != NULL )
				{
					std::string out_msg;
					UTF8toA(buf, out_msg);
					strOutMsg = out_msg.c_str();
				}
				else
					strOutMsg = buf;

				bRet = TRUE;
			}
		}
		else
		{
			m_nDownloadedSize = 0;
			CFile file;
			if( file.Open(m_strDownloadFullpath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
			{
				DWORD dwReadSize = 0;
				static const int buf_size = (1024*1024); // 1MB buffer
				TCHAR* buf = new TCHAR[buf_size];
				CBufferDeleter buf_deleter(buf);
				do
				{
					dwReadSize = pFile->Read(buf, buf_size);
					if( dwReadSize > 0 )
					{
						file.Write(buf, dwReadSize);
						m_nDownloadedSize += dwReadSize;
						if( m_pDownloadedSize!=NULL ) *m_pDownloadedSize=m_nDownloadedSize;
					}
				} while( dwReadSize != 0 );

				bRet = TRUE;
			}
			else
			{
				m_strErrorMsg.Format(_T("Fail to open local_file !!! (%s)"), m_strDownloadFullpath);
				bRet = FALSE;
			}
		}
	}
	catch (CInternetException* e)
	{
		TCHAR szError[255] = {0};
		e->GetErrorMessage(szError, 255);
		e->Delete();
		this->m_strErrorMsg = m_strRequestURL;
		this->m_strErrorMsg.Append(" Error = ");
		this->m_strErrorMsg.Append(szError);
		return bRet;
	}
	catch (CFileException* e)
	{
		TCHAR szError[255] = {0};
		e->GetErrorMessage(szError, 255);
		e->Delete();
		this->m_strErrorMsg = m_strRequestURL;
		this->m_strErrorMsg.Append(" Error = ");
		this->m_strErrorMsg.Append(szError);
		return bRet;
	}
	catch (CException* e)
	{
		TCHAR szError[255] = {0};
		e->GetErrorMessage(szError, 255);
		e->Delete();
		this->m_strErrorMsg = m_strRequestURL;
		this->m_strErrorMsg.Append(" Error = ");
		this->m_strErrorMsg.Append(szError);
		return bRet;
	}
	catch (...)
	{
		this->m_strErrorMsg = m_strRequestURL;
		this->m_strErrorMsg.Append(" Error = ");
		this->m_strErrorMsg.Append("Unknown Error");
		return bRet;
	}

	return bRet;
}

BOOL CHttpRequest::RequestGet(LPCTSTR lpUrl, CString &strOutMsg)
{
	if( lpUrl==NULL ) return FALSE;

	CString str_ascii_url = lpUrl;
	CString str_encoded_url = "";

	//ciBoolean use_encrypt = ( ciIniUtil::encriptMethod()>0 && m_connType!=CONNECTION_CENTER );
	//if( use_encrypt ) // center is only use ASP
	//{
	//	if( str_ascii_url.Find('?') > 0 )
	//		str_ascii_url += "&body=";
	//	else
	//		str_ascii_url += "?body=";

	//	// add certification code
	//	//ciElCryptoAes256 aes256;
	//	//str_ascii_url += aes256.Encrypt("/", true).c_str();
	//	//str_ascii_url += ciAes256Util::Encrypt("/", true).c_str();
	//}

	if( str_ascii_url.Find('?') > 0 )
	{
		// exist param-list

		int pos = 0;

		// get address
		str_encoded_url = str_ascii_url.Tokenize("?", pos);
		str_encoded_url += "?";

		// get param-list
		CString str_param_list = str_ascii_url.Tokenize("?", pos);

		// split & encode a param-list
		pos = 0;
		CString str_param = "";
		while( (str_param=str_param_list.Tokenize("&", pos)) != "" ) // split param
		{
			if( pos > 0 ) str_encoded_url += "&";
			if( str_param.Find('=') <= 0 )
			{
				// invalid param
				str_encoded_url += str_param;
				continue;
			}

			// valid param
			int pos2 = 0;
			str_encoded_url += str_param.Tokenize("=", pos2);
			str_encoded_url += "=";


			//if( !use_encrypt )
			{
				str_encoded_url += ToEncodingString(str_param.Tokenize("&", pos2));
				continue;
			}

			// convert param to encrypt
			CString buf = str_param.Tokenize("&", pos2);
			//ciElCryptoAes256 aes256;
			//str_encoded_url += ToEncodingString(aes256.Encrypt((LPCSTR)buf, false).c_str());
			//str_encoded_url += ToEncodingString(ciAes256Util::Encrypt((LPCSTR)buf, false).c_str());
		}
	}
	else
	{
		// no params
		str_encoded_url = str_ascii_url;
	}

	return Request(true, str_encoded_url, NULL, strOutMsg);
}

BOOL CHttpRequest::RequestGet(LPCTSTR lpUrl, CStringArray &strOutMsgList)
{
	CString strOutMsg;
	BOOL ret_val = RequestGet(lpUrl, strOutMsg);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

BOOL CHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg)
{
	if( lpUrl==NULL ) return FALSE;

	CString str_ascii_msg = "";
	if( lpszSendMsg ) str_ascii_msg = lpszSendMsg;
	CString str_encoded_msg = "";

	//ciBoolean use_encrypt = ( ciIniUtil::encriptMethod()>0 && m_connType!=CONNECTION_CENTER );

	if( str_ascii_msg.GetLength() > 0 )
	{
		// exist params

		// split & encode a param-list
		int pos = 0;
		CString str_param = "";
		while( (str_param=str_ascii_msg.Tokenize("&", pos)) != "" ) // split param
		{
			if( str_encoded_msg.GetLength() > 0 ) str_encoded_msg += "&";
			if( str_param.Find('=') <= 0 )
			{
				// invalid param
				str_encoded_msg += str_param;
				continue;
			}

			// valid param
			int pos2 = 0;
			str_encoded_msg += str_param.Tokenize("=", pos2);
			str_encoded_msg += "=";

			//if( !use_encrypt )
			{
				str_encoded_msg += ToEncodingString(str_param.Tokenize("&", pos2));
				continue;
			}

			// convert param to encrypt
			CString buf = str_param.Tokenize("&", pos2);
			//ciElCryptoAes256 aes256;
			//str_encoded_msg += ToEncodingString(aes256.Encrypt((LPCSTR)buf, false).c_str());
			//str_encoded_msg += ToEncodingString(ciAes256Util::Encrypt((LPCSTR)buf, false).c_str());
		}
	}
	else
	{
		// no params
		str_encoded_msg = str_ascii_msg;
	}

	//if( use_encrypt ) // center is only use ASP
	//{
	//	if( str_encoded_msg.GetLength() > 0 ) str_encoded_msg += "&";
	//	str_encoded_msg += "body=";

	//	// add certification code
	//	//ciElCryptoAes256 aes256;
	//	//str_encoded_msg += ToEncodingString(aes256.Encrypt("/", true).c_str());
	//	//str_encoded_msg += ToEncodingString(ciAes256Util::Encrypt("/", true).c_str());
	//}

	return Request(false, lpUrl, str_encoded_msg, strOutMsg);
}

BOOL CHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CStringArray& strOutMsgList)
{
	CString strOutMsg;
	BOOL ret_val = RequestPost(lpUrl, lpszSendMsg, strOutMsg);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

CString CHttpRequest::ToEncodingString(CString str, bool bReturnNullToString)
{
	if(str.GetLength() == 0) return (bReturnNullToString ? _T("(null)") : _T(""));
	str.Replace(_T("%"), _T("%25"));
	str.Replace(_T(" "), _T("%20"));
	str.Replace(_T("&"), _T("%26"));
	str.Replace(_T("="), _T("%3d"));
	str.Replace(_T("+"), _T("%2b"));
	str.Replace(_T("?"), _T("%3f"));
	str.Replace(_T("/"), _T("%2f"));
	return str;
}

CString CHttpRequest::ToString(int nValue)
{
	TCHAR buf[16] = {0};
	_stprintf(buf, _T("%d"), nValue);

	return buf;
}

void CHttpRequest::GetLineList(CString& str, CStringArray& line_list)
{
	int pos = 0;
	CString token = str.Tokenize(_T("\r\n"), pos);
	while(token != _T(""))
	{
		line_list.Add(token);
		token = str.Tokenize(_T("\r\n"), pos);
	}
}

#define UBC_SERVER_EXECUTE_PATH		_T("Project\\ubc\\config")
#define UBC_CLIENT_EXECUTE_PATH		_T("SQISoft\\UTV1.0\\execute")
#define UBCVARS_INI					_T("UBCVariables.ini")

CString	CHttpRequest::GetINIPath()
{
	TCHAR szModule[MAX_PATH+1] = {0};
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	CString program_path = szModule;
	program_path.MakeLower();

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath_s(szModule, szDrive, MAX_PATH, szPath, MAX_PATH, szFilename, MAX_PATH, szExt, MAX_PATH);

	CString strPath;
	if(program_path.Find("\\bin8") > 0)
	{
		// server
		strPath.Format("%s\\%s\\data\\%s", szDrive, UBC_SERVER_EXECUTE_PATH, UBCVARS_INI);
	}
	else
	{
		// client
		strPath.Format("%s\\%s\\data\\%s", szDrive, UBC_CLIENT_EXECUTE_PATH, UBCVARS_INI);
	}

	return strPath;
}

BOOL
CHttpRequest::isUseSSL(int forceValue/*=-1*/)
{
	static int iUseSSL = -1;
	static httpCriticalSection criticalSection;

	criticalSection.lock();
	if(forceValue >= 0){
		criticalSection.unlock();
		iUseSSL=forceValue;
		return BOOL(iUseSSL);
	}
	if(iUseSSL >= 0){
		criticalSection.unlock();
		return BOOL(iUseSSL);
	}

	CString strPath = GetINIPath();

	BOOL bUseSSL = GetPrivateProfileInt(_T("ROOT"), _T("USE_SSL"), 0, strPath);
	iUseSSL = bUseSSL;
	criticalSection.unlock();
	return bUseSSL;
}
BOOL
CHttpRequest::isUseApache(int forceValue/*=-1*/)
{
	static int iUseApache = -1;
	static httpCriticalSection criticalSection;

	criticalSection.lock();
	if(forceValue >= 0){
		criticalSection.unlock();
		iUseApache=forceValue;
		return BOOL(iUseApache);
	}
	if(iUseApache >= 0){
		criticalSection.unlock();
		return BOOL(iUseApache);
	}

	CString strPath = GetINIPath();

	BOOL bUseApache = GetPrivateProfileInt(_T("ROOT"), _T("USE_AXIS2"), 0, strPath);
	iUseApache = bUseApache;
	criticalSection.unlock();
	return bUseApache;
}
BOOL
CHttpRequest::IpToDomainName(const char* strHttpServerIp, CString& domain)
{
	static map<string,string>	ipDomainMap;
	static httpCriticalSection criticalSection;
	
	criticalSection.lock();
	map<string,string>::iterator itr = ipDomainMap.find(strHttpServerIp);
	if(itr!=ipDomainMap.end()){
		domain = itr->second.c_str();
		criticalSection.unlock();
		return true;
	}

	CString strPath = GetINIPath();

	char buf[1024] = {0};
	DWORD retval = GetPrivateProfileString(_T("SSLDomainNameList"), strHttpServerIp, "", buf, 1024, strPath);
	if(retval > 0){
		domain = buf;
		ipDomainMap.insert(map<string,string>::value_type(strHttpServerIp, (const char*)domain.GetBuffer()));
	}else{
		domain = strHttpServerIp;
	}
	
	criticalSection.unlock();
	return (retval >0 ? true : false);
}

BOOL 
CHttpRequest::HttpToHttps(const char* http, CString& https)
{
	if(isUseSSL() == false) //
	{
		https = http;
		return false;
	}
	if(strncmp(http, "http://", 7) != 0)
	{
		https = http;
		return false;
	}

	char* ip_end_ptr = (char*)http+7; // ip-pointer after of "http://"
	while(*ip_end_ptr != ':' && *ip_end_ptr != '/' && *ip_end_ptr != 0) // find end char (:, /, NULL)
		ip_end_ptr++;
	char ip_end_char = *ip_end_ptr;

	*ip_end_ptr = 0;
	string ip = http+7;

	*ip_end_ptr = ip_end_char;
	string url = ip_end_ptr;

	CString domainName;
	if(IpToDomainName(ip.c_str(), domainName))
	{
		https = "https://";
		https += domainName;
		https += url.c_str();

		return true;
	}
	else
	{
		https = http;
	}

	return false;
}

CString CHttpRequest::AsciiToBase64(CString strAscii)
{
	int nDestLen = Base64EncodeGetRequiredLength(strAscii.GetLength());

	CString strBase64;
	Base64Encode((const BYTE*)(LPCSTR)strAscii, strAscii.GetLength(),strBase64.GetBuffer(nDestLen), &nDestLen);
	strBase64.ReleaseBuffer(nDestLen);

	return strBase64;
}

CString CHttpRequest::Base64ToAscii(CString strBase64)
{
	int nDecLen = Base64DecodeGetRequiredLength(strBase64.GetLength());

	CString strAscii;
	Base64Decode(strBase64, strBase64.GetLength(), (BYTE*)strAscii.GetBuffer(nDecLen), &nDecLen);
	strAscii.ReleaseBuffer(nDecLen);
	return strAscii;
}

BOOL CHttpRequest::AddAdditionalHeader(LPCTSTR lpHeader)
{
	if( lpHeader==NULL ) return FALSE;

	m_strAdditionalHeader = lpHeader;
	m_strAdditionalHeader += "\r\n";
	m_strAdditionalHeader.Replace("\r\n\r\n", "\r\n");

	return TRUE;
}

//////////////////////////////////////////////////////////
// httpCriticalSection
//////////////////////////////////////////////////////////

httpCriticalSection::httpCriticalSection() {
	::InitializeCriticalSection(&_handle);
}

httpCriticalSection::~httpCriticalSection() {
	::DeleteCriticalSection(&_handle);
}

void httpCriticalSection::lock() {
	::EnterCriticalSection(&_handle);
}

void httpCriticalSection::unlock() {
	::LeaveCriticalSection(&_handle);
}
