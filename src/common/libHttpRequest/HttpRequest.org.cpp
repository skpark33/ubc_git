// HttpRequest.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HttpRequest.h"

#include <afxinet.h>

#include <ci/libDebug/ciDebug.h>
#include <CMN/libCommon/ubcIni.h>

#define		DEFAULT_CENTER_IP			_T("211.232.57.218")
#define		DEFAULT_CENTER_WEBPORT		8080

#define		DEFAULT_SERVER_IP			_T("127.0.0.1")
#define		DEFAULT_SERVER_WEBPORT		8080

#define		HTTP_SESSION_TIMEOUT		(60*1000)	// 1min


// CHttpRequest

CHttpRequest::CHttpRequest()
:	m_connType ( CONNECTION_USER_DEFINED )
,	m_strServerIP ( DEFAULT_SERVER_IP )
,	m_nServerPort ( DEFAULT_SERVER_WEBPORT )
{
}

CHttpRequest::~CHttpRequest()
{
}

BOOL CHttpRequest::Init(CONNECTION_TYPE conType, LPCTSTR lpszServerIP, UINT nServerPort)
{
	m_connType = conType;

	switch(conType)
	{
	case CONNECTION_USER_DEFINED:
		m_strServerIP = (lpszServerIP ? lpszServerIP : DEFAULT_SERVER_IP);
		m_nServerPort = (nServerPort ? nServerPort : DEFAULT_SERVER_WEBPORT);
		break;

	case CONNECTION_CENTER:
		{
			//ubcConfig aIni("UBCConnect");
			ubcIni aIni("UBCConnect", "CONFIGROOT", "data");
			ciString center_ip="", center_port="";

			//
			aIni.get("UBCCENTER", "IP", center_ip);
			m_strServerIP = ( center_ip.length()>0 ? center_ip.c_str() : DEFAULT_CENTER_IP );

			//
			aIni.get("UBCCENTER", "WEBPORT", center_port);
			m_nServerPort = ( center_port.length()>0 ? atoi(center_port.c_str()) : DEFAULT_CENTER_WEBPORT );
		}
		break;

	case CONNECTION_SERVER:
		{
			//ubcConfig aIni("UBCConnect");
			ubcIni aIni("UBCConnect", "CONFIGROOT", "data");
			ciString server_ip="", server_port="";

			//
			aIni.get("ORB_NAMESERVICE", "IP", server_ip);
			m_strServerIP = ( server_ip.length()>0 ? server_ip.c_str() : DEFAULT_SERVER_IP );

			//
			aIni.get("AGENTMUX", "HTTP_PORT", server_port);
			m_nServerPort = ( server_port.length()>0 ? atoi(server_port.c_str()) : DEFAULT_SERVER_WEBPORT );
		}
		break;

	default:
		return FALSE;
		break;
	}
	return TRUE;
}

BOOL CHttpRequest::Request(bool isGet, LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg)
{
	//
	CString url;
	if( _tcsnicmp( lpUrl, _T("http://"), 7) )
	{
		url.Format(_T("http://%s:%d%s%s"), 
			m_strServerIP,
			m_nServerPort,
			(lpUrl[0] == _T('/') ? "" : _T("/")),
			ToEncodingString(lpUrl)
		);
	}
	else
		url = lpUrl;

	//
	DWORD dwSearviceType;
	CString strServer, strObject;
	INTERNET_PORT nPort;

	if(!AfxParseURL(url, dwSearviceType, strServer, strObject, nPort))
	{
		return FALSE;
	}

	CInternetSession Session;
	Session.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT,			HTTP_SESSION_TIMEOUT);
	Session.SetOption(INTERNET_OPTION_CONTROL_RECEIVE_TIMEOUT,	HTTP_SESSION_TIMEOUT );
	Session.SetOption(INTERNET_OPTION_CONTROL_SEND_TIMEOUT,		HTTP_SESSION_TIMEOUT );
	Session.SetOption(INTERNET_OPTION_DATA_SEND_TIMEOUT,		HTTP_SESSION_TIMEOUT );
	Session.SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT,		HTTP_SESSION_TIMEOUT );

	BOOL bRet = FALSE;
	DWORD dwReadSize;

	try
	{
		CHttpConnection* pServer = Session.GetHttpConnection(strServer, nPort);
		if(pServer==NULL) return FALSE;
		CBufferDeleter hc_deleter(pServer);

		CHttpFile *pFile = NULL;
		if(isGet)
		{
			CString strHeader = "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\nAccept: */*\r\n";
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_GET, strObject);
			if(pFile==NULL) return FALSE;
			pFile->SendRequest(strHeader, (LPVOID)(LPCTSTR)strHeader, strHeader.GetLength());
		}
		else
		{
			CString strHeader = "Content-Type: application/x-www-form-urlencoded\r\nUser-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_POST, strObject);
			if(pFile==NULL) return FALSE;
			pFile->SendRequest(strHeader, (LPVOID)lpszSendMsg, _tcslen(lpszSendMsg));
		}
		CBufferDeleter hf_deleter(pFile);

		//
		char szLen[32]="";
		DWORD dwLenSize = sizeof(szLen);
		pFile->QueryInfo( HTTP_QUERY_CONTENT_LENGTH, szLen, &dwLenSize );
		int length = atoi( szLen);

		TCHAR* buf = new TCHAR[length+1];
		CBufferDeleter buf_deleter(buf);
		::ZeroMemory(buf, sizeof(TCHAR)*(length+1));
		dwReadSize = pFile->Read(buf, length);

		//
		if(dwReadSize != strlen(buf) || buf[0] == '<')
			bRet = FALSE;
		else
		{
			strOutMsg = buf;
			bRet = TRUE;
		}

		if(pFile) pFile->Close();
		if(pServer) pServer->Close();
	}
	catch (CInternetException* e)
	{
		TCHAR szError[255];
		e->GetErrorMessage(szError, 255);
		e->Delete();
		return bRet;
	}
	catch (CException* e)
	{
		TCHAR szError[255];
		e->GetErrorMessage(szError, 255);
		e->Delete();
		return bRet;
	}
	catch (...)
	{
		return bRet;
	}

	return bRet;
}

BOOL CHttpRequest::RequestGet(LPCTSTR lpUrl, CString &strOutMsg)
{
	return Request(true, lpUrl, NULL, strOutMsg);
}

BOOL CHttpRequest::RequestGet(LPCTSTR lpUrl, CStringArray &strOutMsgList)
{
	CString strOutMsg;
	BOOL ret_val = Request(true, lpUrl, NULL, strOutMsg);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

BOOL CHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg)
{
	return Request(false, lpUrl, lpszSendMsg, strOutMsg);
}

BOOL CHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CStringArray& strOutMsgList)
{
	CString strOutMsg;
	BOOL ret_val = Request(false, lpUrl, lpszSendMsg, strOutMsg);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

CString CHttpRequest::ToEncodingString(CString str, bool bReturnNullToString)
{
	if(str.GetLength() == 0) return (bReturnNullToString ? _T("(null)") : _T(""));
	str.Replace(_T("%"), _T("%25"));
	str.Replace(_T(" "), _T("%20"));
	str.Replace(_T("&"), _T("%26"));
	str.Replace(_T("="), _T("%3d"));
	str.Replace(_T("+"), _T("%2b"));
	str.Replace(_T("?"), _T("%3f"));
	return str;
}

CString CHttpRequest::ToString(int nValue)
{
	TCHAR buf[16] = {0};
	_stprintf(buf, _T("%d"), nValue);

	return buf;
}

void CHttpRequest::GetLineList(CString& str, CStringArray& line_list)
{
	int pos = 0;
	CString token = str.Tokenize(_T("\r\n"), pos);
	while(token != _T(""))
	{
		line_list.Add(token);
		token = str.Tokenize(_T("\r\n"), pos);
	}
}
