#pragma once

#include <wininet.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>

class IHttpDownloadUI 
{
public:
	virtual void OnConnectError(int errCode, CString errMsg)  {}
	virtual void OnBeforeDownload(CString filename, unsigned long srcFileSize) {}
	virtual void OnDownload(CString filename, 
						unsigned long srcFileSize, unsigned long recvFileSize) {}
	virtual void OnAfterDownload(int errCode, CString errMsg, CString filename, 
						unsigned long srcFileSize, unsigned long recvFileSize) {}

};


class CHttpDownload
{
public:
	static		bool  Example();


	CHttpDownload(IHttpDownloadUI* ui=0);
	virtual ~CHttpDownload();

	bool	InternetOpen();   // 단지 Internet 연결
	void	InternetClose();   // 종료
	int		DownloadFile(CString filename,  unsigned long ulIniFilesize=0);
	void	Stop() { m_stop = true; }
	bool	ConnectTest();

	void		SetURL(CString url) { m_url = url;}
	void		SetSourceDir(CString dir) { m_sourceDir = dir; }
	void		SetTargetDir(CString dir) ;

	int			getErrCode() { return m_errCode; }
	CString&	getErrMsg() { return m_errMsg; }
	void		setErr(int code=0, const char* msg="") { m_errCode=code; m_errMsg = msg;}

	enum {	FILE_SIZE_ERROR = -9,
			ENCODING_FAIL=-8,
			EXCEPTION_CATCEHD = -7,
			FILE_NOT_LATEST = -6,
			FILE_DOWNLOAD_FAIL = -5,
			FILE_CREATE_FAIL = -4,
			HTTP_ERROR	= -3,
			URL_OPEN_FAILED = -2,
			NETWORK_CONNECTION_FAIL = -1,
			NOT_INIT = 0, 
			DOWNLOAD_SUCCEED = 1, 
			ALEADY_EXIST = 2
	};
protected:
	void	_URLEncoding(CString& url);
	bool	_EncodingToUTF8(CString& url);

	CString		m_url; // 접속 IP:Port
	HINTERNET	m_hInternet; // internet connection
	HINTERNET	m_hOpenURL; // url connection
	CString		m_errMsg;
	int			m_errCode;

	CString		m_targetDir;
	CString		m_sourceDir;

	IHttpDownloadUI*	m_ui;

	bool		m_stop;
};