#pragma once

#include <afxinet.h>
#include <atlenc.h>
#include <io.h>

class HttpMultiPart
{
public:
	HttpMultiPart(const char* psiteId,
				  const char* phostId,
				  const char* pIp, int pPort, const char* pjsp, const char* pattrname)
		: siteId(psiteId), hostId(phostId), servIP(pIp), servPort(pPort), jsp(pjsp), attrname(pattrname), max_buffer(8196) {}
	virtual ~HttpMultiPart() {}

	size_t send_file(const char *fileDir, const char *filename);
	size_t send_byte(char *fileBuf, size_t file_size,  const char *filename );

	virtual size_t make_pre_body(char* boundary, const char* filename, CString& outval);

protected:
	int _makeSocket();

	CString servIP;
	CString attrname;
	CString jsp;
	int	servPort;
	CString	siteId;
	CString hostId;
	int max_buffer;
};