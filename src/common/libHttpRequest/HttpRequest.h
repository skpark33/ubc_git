#pragma once

#include <afxinet.h>
#include <afxtempl.h>

// CHttpRequest

class CHttpRequest
{
protected:
	class CBufferDeleter
	{
	public:
		CBufferDeleter(char* buf) { Init(); m_buf=buf; };
		CBufferDeleter(wchar_t* wbuf) { Init(); m_wbuf=wbuf; };
		CBufferDeleter(CHttpFile* hf) { Init(); m_httpfile=hf; };
		CBufferDeleter(CHttpConnection* hc) { Init(); m_httpconn=hc; };
		~CBufferDeleter() { Clear(); };

	protected:
		char*		m_buf;
		wchar_t*	m_wbuf;
		CHttpFile*	m_httpfile;
		CHttpConnection*	m_httpconn;

		void	Init()
					{
						m_buf = NULL;
						m_wbuf = NULL;
						m_httpfile = NULL;
						m_httpconn = NULL;
					};
		void	Clear()
					{
						if(m_buf) delete[]m_buf;
						if(m_wbuf) delete[]m_wbuf;
						if(m_httpfile) { m_httpfile->Close(); delete m_httpfile; }
						if(m_httpconn) { m_httpconn->Close(); delete m_httpconn; }
					};
	};

////////////////////////////////////////////////////////////////////////////////
public:
	CHttpRequest();
	virtual ~CHttpRequest();

	enum CONNECTION_TYPE
	{
		CONNECTION_USER_DEFINED = 0,
		CONNECTION_CENTER,
		CONNECTION_SERVER,
	};

protected:
	static int m_nSessionTimeout;

 	CONNECTION_TYPE	m_connType;
	CString	m_strServerIP;
	UINT	m_nServerPort;

	CString	m_strRequestURL;
	CString	m_strErrorMsg;

	CString	m_strAdditionalHeader;

	bool	m_bBinaryMode;
	CString	m_strDownloadFullpath;
	ULONGLONG	m_nDownloadedSize;
	ULONGLONG*	m_pDownloadedSize;

	BOOL	Request(bool isGet, LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg);
	void	GetLineList(CString& str, CStringArray& line_list);

	BOOL	HttpToHttps(const char* http, CString& https);
	BOOL	IpToDomainName(const char* strHttpServerIp, CString& domain);
	BOOL	isUseSSL(int forceValue=-1);
	BOOL	isUseApache(int forceValue=-1);

	CString	GetINIPath();

	CString	AsciiToBase64(CString strAscii);
	CString	Base64ToAscii(CString strBase64);

public:
	BOOL	Init(CONNECTION_TYPE conType, LPCTSTR lpszServerIP=NULL, UINT nServerPort=0);
			// CONNECTION_CENTER, CONNECTION_SERVER => connect to UBC-center or UBC-server
			// CONNECTION_USER_DEFINED => connect to user defined server,port

	BOOL	RequestGet(LPCTSTR lpUrl, CString &strOutMsg);
	BOOL	RequestGet(LPCTSTR lpUrl, CStringArray &strOutMsgList);
	BOOL	RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg);
	BOOL	RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CStringArray& strOutMsgList);

	static CString	ToEncodingString(CString str, bool bReturnNullToString=false);
	static CString	ToString(int nValue);

	void	GetServerInfo(CString& strIP, UINT& nPort) { strIP=m_strServerIP; nPort=m_nServerPort; };
	LPCSTR	GetErrorMsg() { return (LPCSTR)m_strErrorMsg; };
	LPCSTR	GetRequestURL() { return (LPCSTR)m_strRequestURL; };

	VOID	DontUseSSL() { isUseSSL(0); } 
	VOID	DontUseApache() { isUseApache(0); } 
	VOID	SetPort(UINT p) { m_nServerPort = p; }
	VOID	SetServerIP(LPCTSTR p) { m_strServerIP = p; }

	BOOL	AddAdditionalHeader(LPCTSTR lpHeader);
	void	SetBinaryDownloadMode(bool bMode=true, LPCTSTR lpszPath=_T(""), ULONGLONG* pDownloadedSize=NULL) { m_bBinaryMode=bMode; m_strDownloadFullpath=lpszPath; m_pDownloadedSize=pDownloadedSize; };
	ULONGLONG	GetDownloadedSize() { return m_nDownloadedSize; };
};

class httpCriticalSection
{
public:
	httpCriticalSection();
	~httpCriticalSection();
	void lock();
	void unlock();
protected:
	CRITICAL_SECTION _handle;
};
