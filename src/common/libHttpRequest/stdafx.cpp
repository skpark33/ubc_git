// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// libHttpRequest.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

// TODO: 필요한 추가 헤더는
// 이 파일이 아닌 STDAFX.H에서 참조합니다.


unsigned long long 
atoull(const char* str)
{
	unsigned long long retval = 0ll;
	const char* ptr = str;
	while(*ptr != 0)
	{
		retval = retval*10 + (*ptr-'0');
		ptr++;
	}
	return retval;
}

void makeFolder(const char* pszPath)
{
	TCHAR DirName[256];
	LPCTSTR p = pszPath;
	TCHAR* q = DirName; 
	while(*p)
	{
		if (('\\' == *p) || ('/' == *p))
		{
			if (':' != *(p-1))
			{
				CreateDirectory(DirName, NULL);
			}
		}
		*q++ = *p++;
		*q = '\0';
	}
	CreateDirectory(DirName, NULL);
}
