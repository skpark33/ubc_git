//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TestProfile.rc
//
#define IDOK2                           2
#define ID_SAVE_BTN                     2
#define ID_OPEN_BTN                     5
#define ID_WRITE_BTN                    6
#define ID_READ_BTN                     7
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TestProfile_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_PATH_EDT                    1000
#define IDC_WRITE_SECTION_EDT           1001
#define IDC_WRITE_VALUE_EDT             1002
#define IDC_READ_SECTION_EDT            1003
#define IDC_READ_VALUE_EDT              1004
#define IDC_WRITE_KEY_EDT               1005
#define IDC_READ_KEY_EDT                1006
#define IDC_BUTTON1                     1007
#define IDC_SAVE_BTN                    1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
