// TestProfileDlg.h : 헤더 파일
//

#pragma once

#include <libProfileManager/ProfileManager.h>


// CTestProfileDlg 대화 상자
class CTestProfileDlg : public CDialog
{
// 생성입니다.
public:
	CTestProfileDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TestProfile_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedOpenBtn();
	afx_msg void OnBnClickedWriteBtn();
	afx_msg void OnBnClickedReadBtn();
	afx_msg void OnBnClickedOk();
	DECLARE_MESSAGE_MAP()
public:
	
	CString				m_strPath;
	CString				m_strWriteSection;
	CString				m_strWriteKey;
	CString				m_strWriteValue;
	CString				m_strReadSection;
	CString				m_strReadKey;
	CString				m_strReadValue;

	CProfileManager		m_libProfile;
	afx_msg void OnBnClickedSaveBtn();
};
