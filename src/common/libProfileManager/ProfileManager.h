/************************************************************************************/
/*! @file ProfileManager.h
	@brief UTF-8, ASCii 형식의 ini 파일 access 클래스
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2012/03/20\n
	▶ 참고사항:
		- 정미석 과장이 최초 작성한 클래스를 수정

  @warning ※ Multi_Lang 모드에서 ini 파일의 각 value는 클래스 내부에서 UTF-8 string으로 저장하여 다룸\n
		   ※ Multi_Lang 모드가 아니라면 기존과 동일하게 처리함. \n


************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2012/03/20:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

#include <afxmt.h>
#include <afxtempl.h>

//typedef	CMap<CStringW, CStringW, CStringW, CStringW>	CMapWstr;
//typedef	CMap<CStringW, CStringW, void*, void*>			CMapPtr;

class CProfileManager
{
protected:
	CStringW		m_strFilename;			///<파일의 경로(유니코드) 
	//CMapPtr		m_mapGroup;
	CMapStringToPtr	m_mapGroup;
	bool			m_bMultiLang;			///<MultiLang 환경인지 여부
	bool			m_bHasBom;				///<파일에 해더(BOM)가 있었는지 여부

	LPTSTR	GetLine(LPTSTR& tch);
	void	DeleteBracket(CString& str);
	BOOL	AddNode(CString strTab, CString strKey, CString strValue);	///<해당하는 tab의 Key 값에 값을 넣어준다.
	BOOL	AutoFolderMake(WCHAR *pszDir);								///<주어진 경로의 폴더를 생성한다.

public:
	CProfileManager();
	CProfileManager(LPCSTR lpszFilename, bool bUTF8 = false);
	virtual ~CProfileManager(void);

	BOOL	SetFileName(CString strFilename, bool bUTF8 = false);		///<파일의 경로를 설정한다.
	void	ClearInstance();

	//다국어나 UTF-8등의 문자열이 있다면 MFC에서 제공하는 함수는 오류가 난다...
	inline void MakeLower(CString& str)
	{
		for( int i = 0; i < str.GetLength(); i++ )
		{
			char c = str[i];
			if( c >= 'A' && c <= 'Z' )
			{
				str.SetAt( i, c + 'a' - 'A' );
			}//if
		}//for
	}//inline

	BOOL	Read(CString strFilename = _T(""), bool bUTF8 = false);		///<ini파일을 읽는다.
	BOOL	Write(CString strFilename = _T(""), bool bUTF8 = false);	///<ini 파일을 기록한다.

	CString	GetProfileString(CString strTabName, CString strKeyName, CString strDefault = _T(""));
	DWORD	GetProfileString(CString strTabName, CString strKeyName, CString strDefault, CString& strReturnedString);
	int		GetProfileInt(CString lpTabName, CString lpKeyName, int nDefault=0);

	DWORD	WriteProfileString(CString lpTabName, CString lpKeyName, CString strString, BOOL bImmediately=FALSE);
	DWORD	WriteProfileInt(CString lpTabName, CString lpKeyName, int nValue, BOOL bImmediately=FALSE);

	POSITION	GetStartPosition() { return m_mapGroup.GetStartPosition(); };
	void		GetNextAssoc(POSITION& rNextPosition, CString& rKey, void*& rValue) const
				{ m_mapGroup.GetNextAssoc(rNextPosition, rKey, rValue); };

	// ubcCachedIni 에서 사용하기위해 추가
protected:
	CCriticalSection	m_lockInstance;

public:
	CString	Tokenize(LPCTSTR lpszSource, LPCTSTR lpszTokens, int& iStart );
	CString	toString();
	void	fromString(CString& str);

	void	Lock()		{ m_lockInstance.Lock(); };
	void	Unlock()	{ m_lockInstance.Unlock(); };
};
