#include "StdAfx.h"
#include "ProfileManager.h"
#include <IMAGEHLP.H>
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciIni.h>
#pragma comment(lib, "imagehlp.lib")
#include <util/libEncoding/Encoding.h>


ciSET_DEBUG(10, "ProfileManager");


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (LPCTSTR) lpszFilename : (in) 파일의 경로(bUTF8이 true인경우 UTF-8로된 경로)
/// @param (bool) bUTF8 : (in) 파일의 경로가 UTF-8 문자열인지 여부
/////////////////////////////////////////////////////////////////////////////////
CProfileManager::CProfileManager(LPCTSTR lpszFilename, bool bUTF8/* = false*/)
: m_bHasBom(false)
//, m_strFilename(lpszFilename)
{
	m_bMultiLang = ciIniUtil::isMultiLang();

	//Read();
	Read(lpszFilename, bUTF8);
}

CProfileManager::CProfileManager()
: m_bHasBom(false)
{
	m_bMultiLang = ciIniUtil::isMultiLang();
}

CProfileManager::~CProfileManager(void)
{
	//m_strFilename = _T("");
	m_strFilename = L"";

	ClearInstance();
	//CEncoding::clearInstance();
}

void CProfileManager::ClearInstance()
{
	POSITION pos;
	CString key;
	CMapStringToString* group;

	for(pos = m_mapGroup.GetStartPosition(); pos != NULL; )
	{
		m_mapGroup.GetNextAssoc( pos, key, (void*&)group );

		delete group;
	}

	m_mapGroup.RemoveAll();
}

LPTSTR CProfileManager::GetLine(LPTSTR& tch)
{
	LPTSTR line = tch;

	if(*tch == 0)
		return NULL;

	while(*tch != _T('\n') && *tch != 0)
	{
		if(*tch == _T('\r'))
			*tch = 0;
		tch++;
	}

	if(*tch != 0)
	{
		*tch = 0;
		tch++;
	}

	while(*line != 0 && *line == _T(' '))
		line++;

	return line;
}

void CProfileManager::DeleteBracket(CString& str)
{
	int len = str.GetLength();

	if(len > 0)
	{
		if(	(str[0] == _T('[') && str[len-1] == _T(']')) ||
			(str[0] == _T('{') && str[len-1] == _T('}')) ||
			(str[0] == _T('(') && str[len-1] == _T(')')) ||
			(str[0] == _T('\'') && str[len-1] == _T('\'')) ||
			(str[0] == _T('\"') && str[len-1] == _T('\"')) )
		{
			str.Delete(len-1);
			str.Delete(0);
		}
	}
}

CString Tokenize(LPCTSTR lpszSource, LPCTSTR lpszTokens, int& iStart )
{
	if( (lpszTokens == NULL) || (*lpszTokens == 0) )
	{
		if (iStart < _tcslen(lpszSource))
		{
			return lpszSource;
		}
	}
	else
	{
		LPCTSTR pszPlace = lpszSource + iStart;
		LPCTSTR pszEnd = lpszSource + _tcslen(lpszSource);
		if( pszPlace < pszEnd )
		{
			int nIncluding = _tcsspn( pszPlace, lpszTokens );

			if( (pszPlace+nIncluding) < pszEnd )
			{
				pszPlace += nIncluding;
				int nExcluding = _tcscspn( pszPlace, lpszTokens );

				int iFrom = iStart+nIncluding;
				int nUntil = nExcluding;
				iStart = iFrom+nUntil+1;

				CString str = lpszSource;

				return( str.Mid( iFrom, nUntil ) );
			}
		}
	}

	// return empty string, done tokenizing
	iStart = -1;

	return (LPTSTR)(lpszSource + iStart);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ini파일을 읽는다. \n
/// @param (CString) strFilename : (in) 읽을 파일의 경로
/// @param (bool) bUTF8 : (in) 파일의 경로가 UTF-8 문자열인지 여부
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
BOOL CProfileManager::Read(CString strFilename /*= _T("")*/, bool bUTF8/* = false*/)
{
	ciDEBUG(1,("Read(%s,%d)", strFilename, bUTF8));
	if(!strFilename.IsEmpty())
	{
		//m_strFilename = strFilename;
		if(!SetFileName(strFilename, bUTF8))
		{
			ciERROR(("Fail to set file name"));
			return FALSE;
		}//if
		ciDEBUG(1,("SetFileName() succeed"));

	}//if

	ClearInstance();
	ciDEBUG(1,("ClearInstance() succeed"));

	//CFile file;
	//if(file.Open(m_strFilename, CFile::modeRead | CFile::typeBinary))
	FILE* pFile = _wfopen(m_strFilename, L"rb");
	if(pFile)
	{
		//int size = (int)file.GetLength();
		fseek(pFile, 0, SEEK_END);
		int size = ftell(pFile);
		if(size <= 0)
		{
			ciWARN(("File size is 0"));
			fclose(pFile);
			return FALSE;
		}//if
		fseek(pFile, 0, SEEK_SET);
		LPBYTE buf = new BYTE[size+3];
		::memset(buf, 0, size+3);

		//file.Read(buf, size);
		fread(buf, 1, size, pFile);

#ifdef UNICODE
		if(buf[0] == 0xff && buf[1] == 0xfe)
#endif
		{
			CString group_name = _T("");

#ifdef UNICODE
			LPTSTR buff = (LPTSTR)(buf+2);
#else
			//MultiLang 환경에서는 파일 앞부분(BOM)에 UTF-8 형식의 파일인지 구분하는 값을 보고
			//UTF-8 파일이 아니라면 UTF-8로 변환해서 내부 데이터에 담아야 한다.
			//2012-06-08 서버쪽에서 bpi 파일을 읽지 못하는 문제로 BOM에 개행문자 추가 함
			LPTSTR lpHead;
			if(!(buf[0] == 0xEF && buf[1] == 0xBB && buf[2] == 0xBF))
			{
				//BOM이 없는 파일은 변환
				lpHead = (LPTSTR)buf;
				m_bHasBom = false;
			}
			else
			{
				if(buf[3] == 0x0A)		//Line feed
				{
					//BOM과 개행이 있다면 4바이트 이동해서...
					lpHead = (LPTSTR)(buf+4);
				}
				else
				{
					//BOM이 있다면 3바이트 이동해서...
					lpHead = (LPTSTR)(buf+3);
				}//if
				m_bHasBom = true;
			}//if
#endif
			LPTSTR line = GetLine(lpHead);
			while(line != NULL)
			{
				if(line[0] == _T('['))
				{
					group_name = line;
					DeleteBracket(group_name);

					// 대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
					//MakeLower(group_name);
				}
				else if(line[0] != 0)
				{
					int pos = 0;

					CString key = Tokenize(line, _T("="), pos);
					CString value = (LPTSTR)(line + pos);

					//값 추가
					if(!AddNode(group_name, key, value))
					{
						ciERROR(("Fail to add new node : section = %s, key = %s, value = %s", group_name, key, value)); 
						delete []buf;
						return FALSE;
					}//if
				}//if

				line = GetLine(lpHead);
			}//while
		}

		delete []buf;
		//file.Close();
		fclose(pFile);
		
		if(m_bMultiLang)
		{
			//Multi long 인데 BOM이 없었다면 UTF-8로 변환된 내용으로 파일을 써준다.
			if(!m_bHasBom)
			{
				//기존파일백업
				if(!::MoveFileExW(m_strFilename, m_strFilename+L".bak", FALSE))
				{
					ciWARN(("Fail to back up original file"));
				}//if

				if(!Write())
				{
					ciERROR(("Encoded file write fail"));
					return FALSE;
				}//if
			}//if
		}
		else
		{
			//Multi long이 아닌데 BOM이 있었다면 Ansi로 변환된 내용으로 파일을 써준다.
			if(m_bHasBom)
			{
				//기존파일백업
				if(!::MoveFileExW(m_strFilename, m_strFilename+L".bak", FALSE))
				{
					ciWARN(("Fail to back up original file"));
				}//if

				if(!Write())
				{
					ciERROR(("Encoded file write fail"));
					return FALSE;
				}//if
			}//if
		}//if
		ciDEBUG(1,("Read(%s,%d) end", strFilename, bUTF8));
		return TRUE;
	}//if

	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ini 파일을 기록한다. \n
/// @param (CString) strFilename : (in) 저장할 파일의 경로
/// @param (bool) bUTF8 : (in) 파일의 경로가 UTF-8 문자열인지 여부
/// @return <형: BOOL> \n
///			<값: 설명> \n
///			<값: 설명> \n
/////////////////////////////////////////////////////////////////////////////////
BOOL CProfileManager::Write(CString strFilename /*= _T("")*/, bool bUTF8/* = false*/)
{
	ciDEBUG(1,("Write(%s,%d)", strFilename, bUTF8));

	if(!strFilename.IsEmpty())
	{
		//m_strFilename = strFilename;
		if(!SetFileName(strFilename, bUTF8))
		{
			ciERROR(("Fail to set file name"));
			return FALSE;
		}//if
	}//if

	WCHAR drive[MAX_PATH], dir[MAX_PATH], fname[MAX_PATH], ext[MAX_PATH];
	_wsplitpath(m_strFilename, drive, dir, fname, ext );

	CStringW path;
	path.Format(L"%s%s", drive, dir);
	path.Replace(L"/", L"\\");

	//::CreateDirectory(path, NULL);
	//MakeSureDirectoryPathExists(path);
	AutoFolderMake((LPWSTR)(LPCWSTR)path);

	//CFile file;
	//if(file.Open(m_strFilename, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
	FILE* pFile = _wfopen(m_strFilename, L"wb");
	if(pFile)
	{
#ifdef UNICODE
		unsigned short header = 0xfeff;
		//file.Write(&header, sizeof(short));
		fwrite(header, 1, sizeof(unsigned short), pFile);
#endif

		if(m_bMultiLang)
		{
			//UTF-8 BOM 기록
			BYTE btHeader[4] = { 0xEF, 0xBB, 0xBF, 0x0A };
			//file.Write(btHeader, sizeof(BYTE)*3);
			fwrite(btHeader, 1, 4, pFile);

			//2012-06-08 서버쪽에서 bpi 파일을 읽지 못하는 문제로 BOM에 개행문자 추가 함
			//BYTE btNewLine = '\r\n';
			//fwrite((void*)btNewLine, 1, 1, pFile);
		}//if

		POSITION pos;
		CString key;
		CMapStringToString* group;

		for( pos = m_mapGroup.GetStartPosition(); pos != NULL; )
		{
			m_mapGroup.GetNextAssoc( pos, key, (void*&)group );

			CString line;
			line.Format(_T("[%s]\r\n"), key);

			//file.Write((LPCTSTR)line, line.GetLength()*sizeof(TCHAR));
			fwrite((LPCTSTR)line, sizeof(TCHAR), line.GetLength()*sizeof(TCHAR), pFile);

			//
			POSITION pos_sub;
			CString key_sub;
			CString value_sub;

			for( pos_sub = group->GetStartPosition(); pos_sub != NULL; )
			{
				group->GetNextAssoc( pos_sub, key_sub, value_sub );

				CString line;
				line.Format(_T("%s=%s\r\n"), key_sub, value_sub);

				//file.Write((LPCTSTR)line, line.GetLength()*sizeof(TCHAR));
				fwrite((LPCTSTR)line, sizeof(TCHAR), line.GetLength()*sizeof(TCHAR), pFile);
			}
		}
		//file.Close();
		fclose(pFile);

		return TRUE;
	}

	return FALSE;
}

// cgs
CString	CProfileManager::GetProfileString(CString strTabName, CString strKeyName, CString strDefault /*= _T("")*/)
{
	MakeLower(strTabName);
	MakeLower(strKeyName);

	string strFindTab, strFindKey;
	if(m_bMultiLang)
	{
		CEncoding*	pEncoder = CEncoding::getInstance();
		if(!pEncoder->AnsiToUTF8(strTabName, strFindTab))
		{
			ciERROR(("Ansi ==> UTF8 : %s", strTabName));
			return _T("");
		}//if

		if(!pEncoder->AnsiToUTF8(strKeyName, strFindKey))
		{
			ciERROR(("Ansi ==> UTF8 : %s", strKeyName));
			return _T("");
		}//if
	}
	else
	{
		strFindTab = strTabName;
		strFindKey = strKeyName;
	}//if

	CMapStringToString* group;
	if(m_mapGroup.Lookup(strFindTab.c_str(), (void*&)group ))
	{
		CString value;
		if(group->Lookup(strFindKey.c_str(), value))
		{
			if(m_bMultiLang)
			{
				//내부의 UTF-8 문자를 Ansi로 변환해서 반환
				string strAsc = "";
				CEncoding*	pEncoder = CEncoding::getInstance();
				if(!pEncoder->UTF8ToAnsi(value, strAsc))
				{
					ciERROR(("UTF8 ==> Ansi : %s", value));
				}//if
				return strAsc.c_str();
			}//if
			return value;
		}//if
	}//if
	ciERROR(("%s value not founded", strFindKey.c_str()));
	return _T("");
}

DWORD CProfileManager::GetProfileString(CString strTabName, CString strKeyName, CString strDefault, CString& strReturnedString)
{
	strReturnedString = GetProfileString(strTabName, strKeyName, strDefault);
	return strReturnedString.GetLength();
}

int CProfileManager::GetProfileInt(CString strTabName, CString strKeyName, int nDefault/*=0*/)
{
	CString strDefault;
	strDefault.Format(_T("%d"), nDefault);

	return _ttoi(GetProfileString(strTabName, strKeyName, strDefault));
}

DWORD CProfileManager::WriteProfileString(CString strTabName, CString strKeyName, CString strString, BOOL bImmediately/*=FALSE*/)
{
	MakeLower(strTabName);
	MakeLower(strKeyName);

	string strTab, strKey, strValue;
	if(m_bMultiLang)
	{
		//Ansi를 UTF-8로 변환
		CEncoding*	pEncoder = CEncoding::getInstance();
		if(!pEncoder->AnsiToUTF8(strTabName, strTab))
		{
			ciERROR(("Ansi ==> UTF-8 : %s", strTabName));
			return FALSE;
		}//if

		if(!pEncoder->AnsiToUTF8(strKeyName, strKey))
		{
			ciERROR(("Ansi ==> UTF-8 : %s", strKeyName));
			return FALSE;
		}//if

		if(!pEncoder->AnsiToUTF8(strString, strValue))
		{
			ciERROR(("Ansi ==> UTF-8 : %s", strString));
			return FALSE;
		}//if
	}
	else
	{
		strTab = strTabName;
		strKey = strKeyName;
		strValue = strString;
	}//if

	CMapStringToString* group;
	if(m_mapGroup.Lookup(strTab.c_str(), (void*&)group ))
	{
		group->SetAt(strKey.c_str(), strValue.c_str());
	}
	else
	{
		group = new CMapStringToString;
		group->SetAt(strKey.c_str(), strValue.c_str());
		m_mapGroup.SetAt(strTab.c_str(), group);
	}//if

	if(bImmediately)
	{
		Write();
	}//if

	return TRUE;
}

DWORD CProfileManager::WriteProfileInt(CString strTabName, CString strKeyName, int nValue, BOOL bImmediately/*=FALSE*/)
{
	CString strString;
	strString.Format(_T("%d"), nValue);

	return WriteProfileString(strTabName, strKeyName, strString, bImmediately);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 해당하는 tab의 Key 값에 값을 넣어준다. \n
/// @param (CString) strTab : (in) tab 이름(section)
/// @param (CString) strKey : (in) key 값
/// @param (CString) strValue : (in) 추가할 value
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
BOOL CProfileManager::AddNode(CString strTab, CString strKey, CString strValue)
{
	// 대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
	MakeLower(strTab);
	MakeLower(strKey);

	string strNewTab, strNewKey, strNewVal;
	if(m_bMultiLang)
	{
		if(!m_bHasBom)
		{
			//multi lang이면서 파일에 BOM이 없었다면 UTF-8로 인코딩하여야 한다.
			CEncoding*	pEncoder = CEncoding::getInstance();
			if(!pEncoder->AnsiToUTF8(strTab, strNewTab))
			{
				ciERROR(("Ansi ==> UTF-8 : %s", strTab));
				return FALSE;
			}//if

			if(!pEncoder->AnsiToUTF8(strKey, strNewKey))
			{
				ciERROR(("Ansi ==> UTF-8 : %s", strKey));
				return FALSE;
			}//if

			if(!pEncoder->AnsiToUTF8(strValue, strNewVal))
			{
				ciERROR(("Ansi ==> UTF-8 : %s", strValue));
				return FALSE;
			}//if
		}
		else
		{
			strNewTab = strTab;
			strNewKey = strKey;
			strNewVal = strValue;
		}//if
	}
	else
	{
		if(!m_bHasBom)
		{
			strNewTab = strTab;
			strNewKey = strKey;
			strNewVal = strValue;
		}
		else
		{
			//multi lang이 아닌데 BOM이 있었다면 Ansi로 변환해야한다.
			CEncoding*	pEncoder = CEncoding::getInstance();
			if(!pEncoder->UTF8ToAnsi(strTab, strNewTab))
			{
				ciERROR(("UTF-8 ==> Ansi : %s", strTab));
				return FALSE;
			}//if

			if(!pEncoder->UTF8ToAnsi(strKey, strNewKey))
			{
				ciERROR(("UTF-8 ==> Ansi : %s", strKey));
				return FALSE;
			}//if

			if(!pEncoder->UTF8ToAnsi(strValue, strNewVal))
			{
				ciERROR(("UTF-8 ==> Ansi : %s", strValue));
				return FALSE;
			}//if
		}//if
	}//if

	CMapStringToString* group;
	if(m_mapGroup.Lookup(strNewTab.c_str(), (void*&)group))
	{
		group->SetAt(strNewKey.c_str(), strNewVal.c_str());
	}
	else
	{
		group = new CMapStringToString;
		group->SetAt(strNewKey.c_str(), strNewVal.c_str());
		m_mapGroup.SetAt(strNewTab.c_str(), group);
	}//if

	return TRUE;
}

CString CProfileManager::Tokenize(LPCTSTR lpszSource, LPCTSTR lpszTokens, int& iStart )
{
	if( (lpszTokens == NULL) || (*lpszTokens == 0) )
	{
		if (iStart < _tcslen(lpszSource))
		{
			return lpszSource;
		}
	}
	else
	{
		LPCTSTR pszPlace = lpszSource + iStart;
		LPCTSTR pszEnd = lpszSource + _tcslen(lpszSource);
		if( pszPlace < pszEnd )
		{
			int nIncluding = _tcsspn( pszPlace, lpszTokens );

			if( (pszPlace+nIncluding) < pszEnd )
			{
				pszPlace += nIncluding;
				int nExcluding = _tcscspn( pszPlace, lpszTokens );

				int iFrom = iStart+nIncluding;
				int nUntil = nExcluding;
				iStart = iFrom+nUntil+1;

				CString str = lpszSource;

				return( str.Mid( iFrom, nUntil ) );
			}
		}
	}

	// return empty string, done tokenizing
	iStart = -1;

	return (LPTSTR)(lpszSource + iStart);
}

CString	CProfileManager::toString()
{
	CString	strBuf;

	POSITION pos;
	CString key;
	CMapStringToString* group;

	for( pos = m_mapGroup.GetStartPosition(); pos != NULL; )
	{
		m_mapGroup.GetNextAssoc( pos, key, (void*&)group );

		CString line;
		line.Format(_T("[%s]\r\n"), key);

		//file.Write((LPCTSTR)line, line.GetLength()*sizeof(TCHAR));
		strBuf.Append(line);
		//
		POSITION pos_sub;
		CString key_sub;
		CString value_sub;

		for( pos_sub = group->GetStartPosition(); pos_sub != NULL; )
		{
			group->GetNextAssoc( pos_sub, key_sub, value_sub );

			CString line;
			line.Format(_T("%s=%s\r\n"), key_sub, value_sub);

			strBuf.Append(line);
		}
	}
	return strBuf;
}

void CProfileManager::fromString(CString& buf)
{
	CString group_name = _T("");

#ifdef UNICODE
	LPTSTR buff = (LPTSTR)(buf.GetBuffer()+2);
#else
	LPTSTR buff = (LPTSTR)(buf.GetBuffer());
#endif
	LPTSTR line = GetLine(buff);
	while(line != NULL)
	{
		if(line[0] == _T('['))
		{
			group_name = line;
			DeleteBracket(group_name);

			// 대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
			group_name.MakeLower();
		}
		else if(line[0] != 0)
		{
			int pos = 0;

			CString key = Tokenize(line, _T("="), pos);
			CString value = (LPTSTR)(line + pos);

			// 대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
			//key.MakeLower();
			MakeLower(key);

			CMapStringToString* group;
			if(m_mapGroup.Lookup(group_name, (void*&)group ))
			{
				group->SetAt(key, value);
			}
			else
			{
				group = new CMapStringToString;
				group->SetAt(key, value);
				m_mapGroup.SetAt(group_name, group);
			}
		}
		line = GetLine(buff);
	}
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 경로의 폴더를 생성한다. \n
/// @param (WCHAR) *pszDir : (in) 폴더를 생성할 경로
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
BOOL CProfileManager::AutoFolderMake(WCHAR *pszDir)
{
	BOOL bRet = FALSE;

	int nLen = (int)wcslen(pszDir);
	WCHAR* pszSubDir = NULL;

	_wfinddata_t fdata;
	long hFind;
	for(int i = nLen - 1; i >= 0; i--)
	{
		if(pszDir[i] == L'\\')
		{
			pszSubDir = new WCHAR[i+1];
			memset(pszSubDir, 0, i*2+2);
			memcpy(pszSubDir, pszDir, i*2);
			if(hFind = _wfindfirst(pszSubDir, &fdata) == -1L)
			{
				if(!AutoFolderMake(pszSubDir))
				{
					delete pszSubDir;
					return bRet;
				}//if
			}//if
			delete pszSubDir;
			break;
		}//if
	}//for
	bRet = ::CreateDirectoryW(pszDir, NULL);

	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일의 경로를 설정한다. \n
/// @param (CString) strFilename : (in) 설정할 파일의 경로
/// @param (bool) bUTF8 : (in) 파일의 경로가 UTF-8 문자열인지 여부
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
BOOL CProfileManager::SetFileName(CString strFilename, bool bUTF8/* = false*/)
{
	//파일경로명은 Unicode로 해야한다.
	if(bUTF8)
	{
		wstring strPath;
		CEncoding*	pEncoder = CEncoding::getInstance();
		if(!pEncoder->UTF8ToUnicode(strFilename, strPath))
		{
			ciERROR(("UTF8 ==> Unicode : %s", strFilename));
			return FALSE;
		}//if
		m_strFilename = strPath.c_str();
	}
	else
	{
		wstring strPath;
		CEncoding*	pEncoder = CEncoding::getInstance();
		if(!pEncoder->AnsiToUnicode(strFilename, strPath))
		{
			ciERROR(("Ansi ==> Unicode : %s", strFilename));
			return FALSE;
		}//if
		m_strFilename = strPath.c_str();
	}//if

	return TRUE;
}

