/*
 *  Copyright ⓒ 2002 SQISSoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2005/12/05
 *  File name   : scProperties.h
 */

#ifndef _scProperties_h_
#define _scProperties_h_

//----------------------------------------------------------------------------
//	Include Header File
//----------------------------------------------------------------------------
#include <cstdio>
#include <string>
#include <list>
#include <map>
#include <fstream>

/* 
  scProperties : Property 의 데이터 구조 class
*/

typedef map<string,string>		scPropertyMap;
typedef scPropertyMap::iterator	scPropertyIterator;

#define FILE_PROPERTY_TYPE	0
#define FILE_INI_TYPE		1

class absProperties
{
public:
	absProperties() {};
	virtual	~absProperties() {}
	
	virtual boolean		get(const char* pName, string& pValue, const char* defaltValue="")=0;
	virtual boolean		set(const char* pName, const char* pValue)=0;

protected:
	int	_fileType;
	string _fullpath;
};


class scProperties : public virtual absProperties
{
public:
	scProperties();
	virtual	~scProperties() { clear(); }

	virtual boolean		load();
	virtual boolean		get(const char* pName, string& pValue, const char* defaltValue="");
	virtual boolean		set(const char* pName, const char* pValue);
	virtual boolean		write();

	int						size() { return _map.size(); }
	void					clear() { _map.clear(); }	
	scPropertyIterator		begin() { return _map.begin(); }
	scPropertyIterator		end() { return _map.end(); }

protected:
	scPropertyMap	_map;

};

class scIni : public virtual absProperties
{
public:
	scIni(const char* filename, const char* path="");
	virtual	~scIni() {}

	virtual boolean		get(const char* pName, string& pValue, const char* defaltValue="");
	virtual boolean		get(const char* section, const char* key, string& pValue, const char* defaltValue="");
	virtual boolean		set(const char* pName, const char* pValue);
	virtual boolean		set(const char* section, const char* key, const char* pValue);


protected:

};


#endif 

