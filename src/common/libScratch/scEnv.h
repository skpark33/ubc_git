#ifndef _scEnv_h_
#define _scEnv_h_

#include <windows.h>
#include <stdio.h>

#	define SYS_ENV_REGKEY   "SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment"
#	define USER_ENV_REGKEY  "Environment"


class scEnv {
public:

	static scEnv*	getInstance();
	static void	clearInstance();

	virtual ~scEnv() ;

	void	setSysEnv(const char *pszName, const char *pszValue);    /* 시스템 환경변수 추가 */
	void	setUserEnv(const char *pszName, const char *pszValue);  /* 사용자 환경변수 추가 */
	int		getUserEnv(const char *pszName, char *buff, int size);    /* 사용자 환경변수 조회 */
	int		getSysEnv(const char *pszName, char *buff, int size);    /* 시스템 환경변수 조회 */
	void	delUserEnv(const char *pszName);  /* 사용자 환경변수 삭제 */
	void	delSysEnv(const char *pszName);   /* 시스템 환경변수 삭제 */


protected:
	scEnv();

	static scEnv*	_instance;

};

#endif // _scEnv_h_
