#ifndef _scratchUtil_h_
#define _scratchUtil_h_

#ifdef _COP_LINUX_
#include "common/libScratch/linuxScratchUtil.h"
#else

#include <cstdio>
#include <string>
#include <list>
#include <map>
#include <set>
#include <fstream>
#include <vector>

#include "NetworkAdapter.h"
#include "WinFireWall.h"

//#include <common/libScratch/scPath.h>

//#define scOPEN_DEBUG(x)			scratchUtil::getInstance()->myLogOpen(x)
//#define scCLOSE_DEBUG()			scratchUtil::getInstance()->myLogClose()

//#define	scDEBUG		scratchUtil::getInstance()->myDEBUG
//#define	scWARN		scratchUtil::getInstance()->myWARN
//#define	scERROR		scratchUtil::getInstance()->myERROR

/*------------------------------------------------------*/
//UBCReady IPC
//IPC를 위한 메시지 타입 정의
#define		IPC_APP_BRW		1000	///<BRW에서 보내는 IPC 메시지
#define		IPC_APP_FWV		1001	///<FWV에서 보내는 IPC 메시지

#define		IPC_MSG_EXIT	1		///<프로그램을 종료
#define		IPC_MSG_TOPMOST	2		///<TopMost로 창을 설정(full screen)
#define		IPC_MSG_NOMAL	3		///<Normal로 창을 설정(full screen)
#define		IPC_MSG_MIMIZE	4		///<Minimize로 창을 설정

#define		STANDARD_EDITION		"STD"
#define		PLUS_EDITION			"PLS"
#define		ENTERPRISE_EDITION		"ENT"

#define		SCREEN_SHOT_SERVER_PATH   "ScreenShot"

typedef struct _ST_IPC_CMD
{
	int	nVal;
} ST_IPC_CMD;
/*------------------------------------------------------*/

class IPInfo
{
public:
	IPInfo() { isAlive=false;owner = true; ftpPort="21"; svrPort="14007";id="sqi";pwd="utvdev7";shutdownTime="";userCreate="false";}
	~IPInfo() {}
	string ip;			// mandatory
	string hostName;	// mandatory

	string ftpPort;
	string svrPort;
	string id;
	string pwd;
	string shutdownTime;
	string userCreate;
	string mac;
	string monitorCount;
	
	boolean isAlive;
	boolean owner;
	string description;
};

typedef  list<IPInfo*>  IPInfoList;
typedef  map<string,IPInfo*>	IPMap;

class authInfo {
public:
	unsigned char  host[20];
	unsigned char  mac[20];
	authInfo(){
	}
};

class authInfoEE {
public:
	unsigned char  host[20];
	unsigned char  mac[20];
	unsigned char  edition[20];
	authInfoEE(){
		memset(host,0,20);
		memset(mac,0,20);
		memset(edition,0,20);
	}
};

class authInfoEx {
public:
	unsigned char  host[20];
	unsigned char  tv[20];
	unsigned char  studio[20];
	unsigned char  reserved2[20]; // 아직 미사용
	unsigned char  reserved1[20]; // 아직 미사용
	authInfoEx(){
		memset(host,0,20);
		memset(tv,0,20);
		memset(studio,0,20);
		memset(reserved2,0,20);
		memset(reserved1,0,20);
	}
};

#define		AUTH_EX_TV			0x00000001
#define		AUTH_EX_STUDIO		0x00000002
#define		AUTH_EX_RESERVED2	0x00000004
#define		AUTH_EX_RESERVED1	0x00000008

class scratchUtil {
public:

	static scratchUtil*	getInstance();
	static void	clearInstance();

	virtual ~scratchUtil() ;


	boolean isFlashInstalled();

	void mylog(ofstream& out, const char *pFormat,...);

	boolean createDir(const char* path);
	boolean ANSI2UTF8(const char *in, char *out, int nOut);

	int 	AnsiToUTF8( char* szSrc, char* strDest, int destSize );
	int 	UTF8ToAnsi( char* szSrc, char* strDest, int destSize );
	int 	AnsiToUTF8_tiny( char* szSrc, char* strDest, int destSize );
	int 	UTF8ToAnsi_tiny( char* szSrc, char* strDest, int destSize );
	void 	WCharToChar( char* pstrDest, const wchar_t* pwstrSrc );
	int 	WCHARToAnsi(WCHAR	*szUnicode, char* strDest);

	boolean socketAgent(const char* ipAddress,
				 int portNo, 
				 const char* directive, 
				 const char* data);
	boolean socketAgent(const char* ipAddress,
				 int portNo, 
				 const char* directive, 
				 const char* data,
				 string& retval);
	boolean socketAgent2(const char* ipAddress,
				 int portNo, 
				 const char* directive, 
				 const char* data,
				 string& retval);
	boolean socketAgent3(const char* ipAddress,
				 int portNo, 
				 const char* directive, 
				 const char* data,
				 string& retval);

	boolean	connectTest(const char* ipAddress, int portNo);

	void clearInfo(IPInfoList& pList, IPMap& pMap);
	
	boolean ping(int portNo,int waitTime, IPInfoList& outIPList, const char* directive="ping");
	
	
	boolean autoSearch(int port=14008);

	boolean readAuthFile(string& host, string& mac);
	boolean readAuthFile(string& host, string& mac, string& edition);
	boolean writeAuthFile(const char* host, const char* mac, const char* edition=STANDARD_EDITION);

	boolean readLimitFile(string& host, string& site, string& user);
	boolean writeLimitFile(const char* host, const char* site, const char* user);

	boolean powerOn(const char* mac, unsigned long port);

	int		loadAdapter(int nType = E_ADT_TYPE_LOCAL);					///<load system network adapter
	int		getadaptercount(void);										///<get system network adapter count
	string	getAdaptermacaddr(int nIndex = 0);							///<get system network adapter mac address at given index
	bool	IsExistmacaddr(const char* szMacaddr);						///<check system has a given mac address(network adapter)
	string	GetMacaddr(void);											///<get system network adapter mac address

	void	updateStudioLink()	;					/// 임시함수

	void	splitResult(const char* buf,string& hostName,string& shutdownTime,string& mac,string& monitorCount);

	bool	stopProcess(char* processId);
	bool	stopProcess(HWND sttHwnd, char* processId);
	bool 	stopProcess(HWND sttHwnd, char* processId, const char* binaryName, int confirmWaitTime);
	bool	dontStartAgain(char* processId);
	bool	startProcess(char* processId);
	bool	setBrwAdmin(const char* val);

	unsigned long createProcess(const char *exename, const char* arg,const char* dir=0, BOOL minFlag=true);
	boolean	ubcKill(int portNo=14007);

	boolean getMacAddress(string& mac);
	boolean	getIpAddress(string& strAddr);
	boolean	getVersion(string& version, boolean isServer);
	boolean	getVersionTmp(string& version, boolean isServer);


	unsigned long	getPid(const char* exename, bool likeCond=false);
	bool		getProcessList(const char* keyword, list<string>& outList);
	HWND		getWHandle(const char* exename, bool likeCond=false);
	HWND		getWHandle(unsigned long pid);
	bool		extractProcessCommandLine( const char* exename, string& outValue );
	bool		extractProcessCommandLine( unsigned long pid, string& outValue );

	int			killProcess(unsigned long pid);
	int			killProcess(unsigned long pid, bool gracefully, unsigned int waitTime=3000);
	int			killGracefully(unsigned long pid, unsigned int waitTime);
	unsigned long  waitBeforeKill(const char* binaryName,int waitTime);
	int			ExclusiveKillProcess(const char* cExeName, unsigned long ulExceptPid);				///<지정된 pid를 제외한 exe 이름의 프로세스를 모두 종료시킨다.
	int			ExclusiveKillProcess(const char* cExeName, unsigned long ulExceptPid, int waitTime);				///<지정된 pid를 제외한 exe 이름의 프로세스를 모두 종료시킨다.

	bool		killBrowser(char* processId, const char* binaryName, int confirmWaitTime, bool byStarter);
	bool		killBrowser(int confirmWaitTime, bool byStarter);
	bool		killBrowser(int displayNo, int confirmWaitTime, bool byStarter);

	int			getFocus(const char* appTitle);
	int			getFocus(HWND& hWnd);

	void myLogOpen(const char *filename) ;
	void myLogClose() ;
	void myDEBUG(const char *pFormat,...); 
	void myERROR(const char *pFormat,...) ;
	void myWARN(const char *pFormat,...);

	bool DoAdditionalJob(void);						///<Running "additionaljob.bat" 

	void toggleTaskbar();
	void showTaskbar(bool bShow=true);
	void showBalloonTip(bool bShow=true);
	const char* getVersion(char* version, unsigned int size);

	void makeFolder(const char* pszPath);

	boolean displaySound(string& array);

	boolean getBrowserINIValue(const char* sector, 
								 const char* arg, 
								 const char* defaultValue,
								 string& value);

	boolean setBrowserINIValue(const char* sector, 
								 const char* arg, 
								 const char* value);

	int getBrowserInfo(int display,string& binary,string& arg);
	//int getPreDownloaderInfo(string& binary, string& arg);

	void	rightTrim(char* str);
	void	divide(const char* pTarget, const char pDeli,string& pFirst, string& pOther);

	/*------------------------------------------------------*/
	//UBCReady IPC
	bool	ExitUBCReady(HWND hSender);					///<UBCReady를 종료시키는 메시지를 보낸다.
	bool	SendCmdUBCReady(HWND hSender, int nCmd);	///<UBCReady에 종료를 포함한 기타 정의된 메시지를 보낸다.
	/*------------------------------------------------------*/

	int	getScreenShotFile(const char* hostId, set<string>& fileList, int startHour=0, int endHour=0);
	int	getScreenShotFile2(const char* hostId, set<string>& fileList, int startHour=0, int endHour=0);

	bool	hasDoubleByte(const char* ptr);

	const char* getLang();
	const char* getSysLang();
	bool setLang();

	BOOL	IsWindows7();
	BOOL	removeIconTray(const char* windowName, unsigned int tray_id);

	int		GetUnusedContentsFiles(list<string>& lsFiles);
	int		GetUnusedContentsFiles(list<string>& lsFiles, const char lastDrive);

	void	getOSInfo(string& pInfo);

	void	FindDrive(list<string>& astrDriveList);
	int		removeDirectory(const char *strDirectory);

	void	getThisPath(string& outVal);
	int		getMonitorCount();
	double	getInstalledVersion(const char* subdir="data");

	//
	bool	AddToExceptionList(void);					///<윈도우즈 방화벽의 예외 리스트에 프로그램을 추가한다.
	bool	AddToExceptionList(string strAppPath);		///<윈도우즈 방화벽의 예외 리스트에 주어진 경로의 프로그램을 추가한다.
	bool	RemoveFromExceptionList(void);				///<윈도우즈 방화벽의 예외 리스트에서 프로그램을 제거한다.
	bool	RemoveFromExceptionList(string strAppPath);	///<윈도우즈 방화벽의 예외 리스트에서 주어진 경로의 프로그램을 제거한다.
	bool	IsAppExceptionAdded(void);					///<프로그램이 윈도우즈의 방화벽에 등록되어 있는지 확인한다.				
	bool	IsAppExceptionAdded(string strAppPath);		///<주어진 경로의 프로그램이 윈도우즈의 방화벽에 등록되어 있는지 확인한다.
	bool	IsTcpPortAdded(long lPort);					///<주어진 TCP 포트가 방화벽 예외에 추가되어 있는지 확인한다.
	bool	AddTcpPort(long lPort);						///<주어진 TCP 포트를 방화벽 예외에 추가한다.
	bool	RemoveTcpPort(long lPort);					///<주어진 TCP 포트를 방화벽 예외에서 제거한다.
	bool	IsUdpPortAdded(long lPort);					///<주어진 UDP 포트가 방화벽 예외에 추가되어 있는지 확인한다.
	bool	AddUdpPort(long lPort);						///<주어진 UDP 포트를 방화벽 예외에 추가한다.
	bool	RemoveUdpPort(long lPort);					///<주어진 UDP 포트를 방화벽 예외에서 제거한다.

	bool	IsPortAdded(long lPort);					///<주어진 TCP와 UDP 포트를 방화벽 예외에 추가되어 있는지 확인한다.
	bool	AddPort(long lPort);						///<주어진 TCP와 UDP 포트를 방화벽 예외에 추가한다.
	bool	RemovePort(long lPort);						///<주어진 TCP와 UDP 포트를 방화벽 예외에서 제거한다.

//	bool	SetDisableWindowsUpdate(void);				///<윈도우즈 자동 업데이트가 되지 않도록 설정한다.
/*
	//Process resource 관련
	HANDLE		GetProcessHandle(CString strProcName);		///<주어진 프로세스의 핸들을 반환한다.(1개이상인경우에는 첫번째)
	DWORD		GetUsingHandleCount(HANDLE hProc);			///<주어진 프로세스의 핸들사용량(count)를 반환한다.
	DWORD		GetUsingHandleCount(CString strProcName);	///<주어진 프로세스의 핸들사용량(count)를 반환한다.
	DWORD		GetUsingHandleCount(void);					///<자신이 사용하는 핸들사용량(count)를 반환한다.

	DWORD		GetUsingGDICount(HANDLE hProc);				///<주어진 프로세스의 GDI 사용량(count)을 반환한다.
	DWORD		GetUsingGDICount(CString strProcName);		///<주어진 프로세스의 GDI 사용량(count)을 반환한다.
	DWORD		GetUsingGDICount(void);						///<자신이 사용하는 GDI 사용량(count)을 반환한다.
*/
	//
	int readAuthFileEx(string& host);						// 기타 인증 정보 읽기 (AUTH_EX_TV, AUTH_EX_STUDIO...)
	boolean writeAuthFileEx(const char* host, int auth_ex);	// 기타 인증 정보 쓰기

	//
	boolean	monitorPowerSaveOn();
	boolean	monitorPowerSaveOff();

	boolean getCustomerInfo(string& value);

	int CreateGUID(char* buf, int size);

	//int getGMT();

	boolean deleteFolder(const char* location);

	bool	isGScert();

	void	SetForegroundWindowForce(HWND hWnd);

protected:
	scratchUtil();

	static scratchUtil*	_instance;

	CNetworkAdapterList	m_clsAdapterList;			///<Network adapter list class

	ofstream	_log;
	BOOL	_hasLog;

};



#endif
#endif // _scratchUtil_h_
