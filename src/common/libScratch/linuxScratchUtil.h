#ifndef _linuxScratchUtil_h_
#define _linuxScratchUtil_h_

#include <ci/libBase/ciListType.h>
#include <ci/libMSCCompat/ciMSCCompatTypes.h>
#include <cstdio>
#include <string>
#include <list>
#include <map>
#include <set>
#include <fstream>
#include <vector>

#define     SCREEN_SHOT_SERVER_PATH   "ScreenShot"


class authInfo {
public:
	unsigned char  host[20];
	unsigned char  mac[20];
	authInfo(){
	}
};

class authInfoEE {
public:
	unsigned char  host[20];
	unsigned char  mac[20];
	unsigned char  edition[20];
	authInfoEE(){
		memset(host,0,20);
		memset(mac,0,20);
		memset(edition,0,20);
	}
};

class authInfoEx {
public:
	unsigned char  host[20];
	unsigned char  tv[20];
	unsigned char  studio[20];
	unsigned char  reserved2[20]; // ¾ÆÁ÷ ¹Ì»ç¿ë
	unsigned char  reserved1[20]; // ¾ÆÁ÷ ¹Ì»ç¿ë
	authInfoEx(){
		memset(host,0,20);
		memset(tv,0,20);
		memset(studio,0,20);
		memset(reserved2,0,20);
		memset(reserved1,0,20);
	}
};

class scratchUtil {
public:

	static scratchUtil*	getInstance();
	static void	clearInstance();

	virtual ~scratchUtil() ;

	bool	removeDirectory(const char *strDirectory);
	bool	deleteFolder(const char* location);

	int 	CreateGUID(char* buf, int size);
	int 	getScreenShotFile(const char* hostId, set<string>& fileList,int startHour,int endHour);
	int 	getScreenShotFile2(const char* hostId, set<string>& fileList,int startHour,int endHour);
	int 	getScreenShotFile(const char* hostId, set<string>& fileList,int startHour,int endHour,const char* pext);

	boolean readLimitFile(string& host, string& site, string& user);
	boolean getIpAddress(string& strAddr);

protected:


	scratchUtil();

	static scratchUtil*	_instance;
};


#endif // _scratchUtil_h_
