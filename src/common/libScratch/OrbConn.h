#pragma once

#include <list>

struct SOrbConnInfo
{
	char	key[20];
	char	name[50];
	char	ip[30];
	int		port;
	bool	use;
};

typedef std::list<SOrbConnInfo*> OrbConList;

class COrbConn
{
public:
	static COrbConn* GetObject();
	static void Release();

	bool Add(SOrbConnInfo* pInfo);
	bool Mod(SOrbConnInfo* pInfo);
	bool Del(SOrbConnInfo* pInfo);
	bool Del(const char * szKey);

	SOrbConnInfo* Get(const char * szKey);
	SOrbConnInfo* GetCur();
	
	bool SetCur(SOrbConnInfo* pInfo);
	bool SetCur(const char * szKey);
	
	OrbConList* GetList();
	void ReloadList();
protected:
	COrbConn();
	~COrbConn();

	void RemoveList();
	void WriteIni(SOrbConnInfo* pInfo);
public:
protected:
	char m_szIniPath[MAX_PATH];
	OrbConList m_InfoList;
	SOrbConnInfo m_CurInfo;
	static COrbConn* m_pOrbCon;
};