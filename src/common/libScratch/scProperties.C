/*
 *  Copyright �� 2002 SQISoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2002/12/05
 *  File name   : scProperties.C
 */

//----------------------------------------------------------------------------
//	Include Header File
//---------------------------------------------------------------------------- 
#include "stdAfx.h"
#include "scProperties.h"
#include "scratchUtil.h"
#include "scPath.h"

#define	PROPERTY_MAX_LINE			1024
#define	PROPERTY_ITEM_DELIMITER		'='


scProperties::scProperties()
{
	_fileType = FILE_PROPERTY_TYPE;
	_fullpath = _UBC_CD("C:\\SQISoft\\UTV1.0\\execute\\config\\utv1.properties");
}

boolean
scProperties::write()
{
	if(_fileType != FILE_PROPERTY_TYPE) {
		return true;
	}
	
	FILE* fd = fopen(_fullpath.c_str(), "w");
	if(!fd) {
		return false;
    }
	scPropertyIterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++) {
		fprintf(fd, "%s=%s\n", itr->first.c_str(), itr->second.c_str());
	}
	fclose(fd);
	return true;
}

boolean
scProperties::load()
{
	if(_fileType != FILE_PROPERTY_TYPE) {
		return true;
	}
	
	clear();

    FILE* fd = fopen(_fullpath.c_str(), "r");
	if(!fd) {
		   return false;
    }

	char str[PROPERTY_MAX_LINE];
	memset(str, 0x00, sizeof(str));
	while(fgets(str, PROPERTY_MAX_LINE-1, fd) != NULL) {
		char *p = str;
		for( ;*p == ' ' || *p == '\t'; p++ );
		if( *p == '#' || *p == '\0' || *p == '\n' ) {
			memset(str, 0x00, sizeof(str));
			continue;
		}
		scratchUtil::getInstance()->rightTrim(p);
		string key = "";
		string value = "";
		scratchUtil::getInstance()->divide(p,PROPERTY_ITEM_DELIMITER,key,value);
		_map.insert(scPropertyMap::value_type(key, value));
		memset(str, 0x00, sizeof(str));
	}

	fclose(fd);
	return true;
}

boolean 
scProperties::get(const char* name , string& value, const char* defaultValue)
{
	if(!name && strlen(name)==0) {
		return false;
	}
	scPropertyIterator itr = _map.find(name);
	if(itr == _map.end()) {
		if(defaultValue && strlen(defaultValue)){
			value = defaultValue;
		}
		return false;
	}
	value = itr->second;
	if(value.empty() && defaultValue && strlen(defaultValue)){
		value = defaultValue;
	}

	return true;
}

boolean 
scProperties::set(const char* name , const char* value)
{
   	if(!name && strlen(name)==0) {
		return false;
	}
	scPropertyIterator itr = _map.find(name);
	if(itr == _map.end()) {
		return false;
	}
	_map.erase(itr);
	_map.insert(scPropertyMap::value_type(name, value));
	return true;
}


scIni::scIni(const char* filename, const char* path)
{
	_fileType = FILE_INI_TYPE;
	if(path==0 || strlen(path)==0){
		_fullpath = _UBC_CD("C:\\SQISoft\\UTV1.0\\execute\\config\\");
	}else{
		_fullpath = path;
	}
	_fullpath += filename;
}
boolean 
scIni::get(const char* name , string& value, const char* defaultValue)
{
	if(!name && strlen(name)==0) {
		return false;
	}
#ifdef WIN32
	char	szValue[1024];
	memset(szValue, '\0', sizeof(szValue));

	string first,second;
	scratchUtil::getInstance()->divide(name,PROPERTY_ITEM_DELIMITER,first,second);

	GetPrivateProfileString(first.c_str(), second.c_str(), defaultValue, 
		szValue, sizeof(szValue), _fullpath.c_str());
	value=szValue;
#endif
	return true;
}
boolean 
scIni::get(const char* section,const char* key, string& value, const char* defaultValue)
{
   	if(!section && strlen(section)==0) {
		return false;
	}
	if(!key && strlen(key)==0) {
		return false;
	}
#ifdef WIN32
	char	szValue[1024];
	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString(section,key,defaultValue,szValue, sizeof(szValue),_fullpath.c_str());
	value=szValue;
#endif
	return true;
}
boolean 
scIni::set(const char* name , const char* value)
{
   	if(!name && strlen(name)==0) {
		return false;
	}
#ifdef WIN32
	string first,second;
	scratchUtil::getInstance()->divide(name,PROPERTY_ITEM_DELIMITER,first,second);
	return WritePrivateProfileString(first.c_str(),second.c_str(),  value, _fullpath.c_str());
#endif
	return true;

}
boolean 
scIni::set(const char* section, const char* key , const char* value)
{
   	if(!section && strlen(section)==0) {
		return false;
	}
	if(!key && strlen(key)==0) {
		return false;
	}
#ifdef WIN32
	return WritePrivateProfileString(section,key,value,_fullpath.c_str());
#endif
	return true;
}