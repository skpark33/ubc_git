#ifndef _windowLog_h_
#define _windowLog_h_

#include <iostream>
#include <tchar.h>
#include <iostream>
#include <fstream>

//#include <afx.h>

using namespace std;

class windowLog {
public:

	static windowLog*	getInstance();
	static void	clearInstance();

	virtual ~windowLog() ;

	int printLog();

	int makeOnOffLog();
	
	int getPowerOnOffTime(unsigned long& onTime, unsigned long& offTime);

	void my_logOpen(const char *filename) ;
	void my_logClose() ;
	void my_printf(const char *pFormat,...); 

	//const char* getDebug(){ return _debugStr.c_str(); }
	//void	clearDebug() { _debugStr = ""; }

protected:
	windowLog();
	LPSTR GetString(EVENTLOGRECORD *pRecord,  LPSTR source);
	BOOL ReadEventSourceInfo(LPCSTR lpszESName, LPSTR lpszEvent);
	LPSTR GetEventMessage( 
	  HMODULE hDll,             /* Handle to the event message file */
	  DWORD   dwEventIndex,     /* Index of the event description message */
	  DWORD   dwLanguageID,     /* Language ID of the message to retrieve */
	  LPTSTR  *lpInserts );



	static windowLog*	_instance;
	ofstream	_log;
	BOOL	_hasLog;

	//string	_debugStr;
	
};



#endif // _windowLog_h_
