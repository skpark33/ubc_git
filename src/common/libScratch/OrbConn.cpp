#include "stdAfx.h"
#include "OrbConn.h"

#define ORBCONN_INI		"UBCConnect.ini"
#define ORBCONN_KEY		"ORB_NAMESERVICE"

COrbConn* COrbConn::m_pOrbCon = NULL;

COrbConn::COrbConn()
{
	char szBuf[MAX_PATH];
	::ZeroMemory(szBuf, MAX_PATH);
	::GetModuleFileName(NULL, szBuf, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH];
	_tsplitpath(szBuf, szDrive, szPath, NULL, NULL);

	sprintf(m_szIniPath, "%s%sdata\\%s", szDrive, szPath, ORBCONN_INI);

	ReloadList();

	memset(&m_CurInfo, 0, sizeof(SOrbConnInfo));
	strncpy(m_CurInfo.key, ORBCONN_KEY, sizeof(m_CurInfo.key));

	::GetPrivateProfileString(ORBCONN_KEY, "NAME", "", szBuf, sizeof(szBuf), m_szIniPath);
	strncpy(m_CurInfo.name, szBuf, sizeof(m_CurInfo.name));

	::GetPrivateProfileString(ORBCONN_KEY, "IP", "", szBuf, sizeof(szBuf), m_szIniPath);
	strncpy(m_CurInfo.ip, szBuf, sizeof(m_CurInfo.ip));

	m_CurInfo.port = ::GetPrivateProfileInt(ORBCONN_KEY, "PORT", 0, m_szIniPath);
	m_CurInfo.use = true;
}

COrbConn::~COrbConn()
{
	RemoveList();
}

COrbConn* COrbConn::GetObject()
{
	if(!m_pOrbCon)
		m_pOrbCon = new COrbConn;
	return m_pOrbCon;

}

void COrbConn::Release()
{
	if(m_pOrbCon)
		delete m_pOrbCon;
	m_pOrbCon = NULL;
}

bool COrbConn::Add(SOrbConnInfo* pSInfo)
{
	if(!pSInfo)	return false;

	int nCnt = 0;
	char szKey[20];
	SOrbConnInfo* pInfo =  NULL;
	
	while(true){
		_snprintf(szKey, sizeof(szKey), "%s%d", ORBCONN_KEY, ++nCnt);
		
		pInfo = Get(szKey);
		if(!pInfo) break;
		
		if(!pInfo->use){
			memcpy(pInfo, pSInfo, sizeof(SOrbConnInfo));
			pInfo->use = true;
			strncpy(pInfo->key, szKey, sizeof(pInfo->key));

			WriteIni(pInfo);
			return true;
		}
	}

	pInfo = new SOrbConnInfo;
	
	memcpy(pInfo, pSInfo, sizeof(SOrbConnInfo));
	pInfo->use = true;
	_snprintf(szKey, sizeof(szKey), "%s%d", ORBCONN_KEY, nCnt);
	strcpy(pInfo->key, szKey);

	WriteIni(pInfo);
	m_InfoList.push_back(pInfo);

	return true;
}

bool COrbConn::Mod(SOrbConnInfo* pSInfo)
{
	if(!pSInfo)	return false;

	OrbConList::iterator itr;
	for(itr = m_InfoList.begin(); itr != m_InfoList.end(); itr++){
		SOrbConnInfo* pInfo =  (*itr);
		if(!pInfo)	continue;

		if(!strncmp(pInfo->key, pSInfo->key, sizeof(pInfo->key))){
			memcpy(pInfo, pSInfo, sizeof(SOrbConnInfo));
			WriteIni(pInfo);
			return true;
		}
	}

	return false;
}

bool COrbConn::Del(SOrbConnInfo* pInfo)
{
	if(!pInfo)	return false;

	pInfo->use = false;
	WriteIni(pInfo);
	
	return true;
}

bool COrbConn::Del(const char * szKey)
{
	OrbConList::iterator itr;
	for(itr = m_InfoList.begin(); itr != m_InfoList.end(); itr++){
		SOrbConnInfo* pInfo =  (*itr);
		if(!pInfo)	continue;

		if(!strncmp(pInfo->key, szKey, sizeof(pInfo->key))){
			pInfo->use = false;
			WriteIni(pInfo);
			return true;
		}
	}

	return false;
}



SOrbConnInfo* COrbConn::Get(const char * szKey)
{
	OrbConList::iterator itr;
	for(itr = m_InfoList.begin(); itr != m_InfoList.end(); itr++){
		SOrbConnInfo* pInfo =  (*itr);
		if(!pInfo)	continue;

		if(!strncmp(pInfo->key, szKey, sizeof(pInfo->key))){
			return pInfo;
		}
	}

	return NULL;
}

SOrbConnInfo* COrbConn::GetCur()
{
	return &m_CurInfo;
}

bool COrbConn::SetCur(const char * szKey)
{
	SOrbConnInfo* pInfo = Get(szKey);
	if(!pInfo)	return false;

	return SetCur(pInfo);
}

bool COrbConn::SetCur(SOrbConnInfo* pInfo)
{
	memcpy(&m_CurInfo, pInfo, sizeof(SOrbConnInfo));
	m_CurInfo.use = true;
	strncpy(m_CurInfo.key, ORBCONN_KEY, sizeof(m_CurInfo.key));
	WriteIni(&m_CurInfo);
	return true;
}

OrbConList* COrbConn::GetList()
{
	return &m_InfoList;
}

void COrbConn::ReloadList()
{
	RemoveList();

	char szBuf[MAX_PATH];
	int nCnt = 0;
	while(true){
		SOrbConnInfo* pInfo = new SOrbConnInfo;
		_snprintf(pInfo->key, sizeof(pInfo->key), "%s%d", ORBCONN_KEY, ++nCnt);

		::GetPrivateProfileString(pInfo->key, "NAME", "", szBuf, sizeof(szBuf), m_szIniPath);
		strncpy(pInfo->name, szBuf, sizeof(pInfo->name));

		::GetPrivateProfileString(pInfo->key, "IP", "", szBuf, sizeof(szBuf), m_szIniPath);
		strncpy(pInfo->ip, szBuf, sizeof(pInfo->ip));
		
		pInfo->port = ::GetPrivateProfileInt(pInfo->key, "PORT", 0, m_szIniPath);
		pInfo->use = ::GetPrivateProfileInt(pInfo->key, "USE", 0, m_szIniPath);

		if(strlen(pInfo->name) == 0 || strlen(pInfo->ip) == 0){
			delete pInfo;
			break;
		}

		m_InfoList.push_back(pInfo);
	}
}

void COrbConn::RemoveList()
{
	OrbConList::iterator itr;
	for(itr = m_InfoList.begin(); itr != m_InfoList.end(); itr++){
		SOrbConnInfo* pInfo =  (*itr);
		if(!pInfo)	continue;
		delete pInfo;
	}
	m_InfoList.clear();
}

void COrbConn::WriteIni(SOrbConnInfo* pInfo)
{
	char szBuf[MAX_PATH];

	::WritePrivateProfileString(pInfo->key, "NAME", pInfo->name, m_szIniPath);
	::WritePrivateProfileString(pInfo->key, "IP", pInfo->ip, m_szIniPath);
	::WritePrivateProfileString(pInfo->key, "PORT", itoa(pInfo->port, szBuf, 10), m_szIniPath);
	::WritePrivateProfileString(pInfo->key, "USE", itoa(pInfo->use, szBuf, 10), m_szIniPath);
}
