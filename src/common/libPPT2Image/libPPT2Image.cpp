// libPPT2Image.cpp : 해당 DLL의 초기화 루틴을 정의합니다.
//

#include "stdafx.h"
#include "libPPT2Image.h"
#include "OfficeApp.h"

#include <io.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CPPT2Image::CPPT2Image()
{
	::ZeroMemory(m_szErrorMessage, sizeof(m_szErrorMessage));

	m_bMakeThumbnail = false;
	m_nThumbnailWidth = -1;
	m_nThumbnailHeight = -1;
}

CPPT2Image::~CPPT2Image()
{
}

BOOL CPPT2Image::SaveToImage(LPCSTR lpszPPTFullpath, LPCSTR ImgExt)
{
	if( lpszPPTFullpath == NULL )
	{
		_stprintf_s(m_szErrorMessage, 1023, _T("FULLPATH is NULL !!! (%s, %s)"), lpszPPTFullpath, ImgExt);
		return FALSE;
	}

	if( _taccess(lpszPPTFullpath, 00) != 0 )
	{
		_stprintf_s(m_szErrorMessage, 1023, _T("NOT EXIST FULLPATH !!! (%s, %s)"), lpszPPTFullpath, ImgExt);
		return FALSE;
	}

	COfficeApp* instance = COfficeApp::getInstance(lpszPPTFullpath, NULL);

	BOOL ret = instance->SaveToImage(lpszPPTFullpath, ImgExt, m_szErrorMessage);

	if( m_bMakeThumbnail )
	{
		ret = instance->SaveToThumbnail(lpszPPTFullpath, m_nThumbnailWidth, m_nThumbnailHeight, ImgExt, m_szErrorMessage);
	}

	COfficeApp::clearInstance();

	return ret;
}

BOOL CPPT2Image::SaveToImage(LPCWSTR lpwszPPTFullpath, LPCWSTR ImgExt)
{
	if( lpwszPPTFullpath == NULL )
	{
		_stprintf_s(m_szErrorMessage, 1023, _T("FULLPATH is NULL !!! (%s, %s)"), lpwszPPTFullpath, ImgExt);
		return FALSE;
	}

	if( _waccess(lpwszPPTFullpath, 00) != 0 )
	{
		_stprintf_s(m_szErrorMessage, 1023, _T("NOT EXIST FULLPATH !!! (%s, %s)"), lpwszPPTFullpath, ImgExt);
		return FALSE;
	}

	wchar_t drv[32],dir[MAX_PATH],fn[MAX_PATH],ext[MAX_PATH];
	_wsplitpath(lpwszPPTFullpath, drv, dir, fn, ext);
	CStringW str_org_ppt_folder_path;
	str_org_ppt_folder_path.Format(L"%s%s%s", drv, dir, fn);

	CStringW str_tmp_ppt_fullpath_unicode;
	CStringW str_tmp_ppt_folder_path_unicode;
	CString str_tmp_ppt_fullpath_ansi;
	CString str_tmp_ppt_folder_path_ansi;
	UUID uuid;
	if(RPC_S_OK != UuidCreate(&uuid))
	{
		srand((unsigned)time(NULL));
		int rnd = rand();

		str_tmp_ppt_fullpath_unicode.Format(L"C:\\SQISoft\\Contents\\ENC\\_tmp_ppt_%d.ppt", rnd);
		str_tmp_ppt_folder_path_unicode.Format(L"C:\\SQISoft\\Contents\\ENC\\_tmp_ppt_%d", rnd);
		str_tmp_ppt_fullpath_ansi.Format("C:\\SQISoft\\Contents\\ENC\\_tmp_ppt_%d.ppt", rnd);
		str_tmp_ppt_folder_path_ansi.Format("C:\\SQISoft\\Contents\\ENC\\_tmp_ppt_%d", rnd);
	}
	else
	{
		char sz_uuid[MAX_PATH+1] = {0};
		wchar_t wsz_uuid[MAX_PATH+1] = {0};

		::StringFromGUID2(uuid, wsz_uuid, MAX_PATH);
		::wcstombs(sz_uuid, wsz_uuid, MAX_PATH);

		str_tmp_ppt_fullpath_unicode.Format(L"C:\\SQISoft\\Contents\\ENC\\_tmp_ppt_%s.ppt", wsz_uuid);
		str_tmp_ppt_folder_path_unicode.Format(L"C:\\SQISoft\\Contents\\ENC\\_tmp_ppt_%s", wsz_uuid);
		str_tmp_ppt_fullpath_ansi.Format("C:\\SQISoft\\Contents\\ENC\\_tmp_ppt_%s.ppt", sz_uuid);
		str_tmp_ppt_folder_path_ansi.Format("C:\\SQISoft\\Contents\\ENC\\_tmp_ppt_%s", sz_uuid);
	}

	if( !::CopyFileW(lpwszPPTFullpath, str_tmp_ppt_fullpath_unicode, FALSE) )
	{
		// error
		// ::GetLastError();
		return FALSE;
	}

	char sz_imgext[MAX_PATH+1] = {0};
	::wcstombs(sz_imgext, ImgExt, MAX_PATH);

	BOOL ret = SaveToImage(str_tmp_ppt_fullpath_ansi, sz_imgext);
	::SetFileAttributes(str_tmp_ppt_fullpath_ansi, FILE_ATTRIBUTE_NORMAL); //읽기전용 해제
	::DeleteFile(str_tmp_ppt_fullpath_ansi);
	if( ret )
	{
		// success ==> move to org
		if( !::MoveFileW(str_tmp_ppt_folder_path_unicode, str_org_ppt_folder_path) )
			ret = FALSE;
	}
	
	if( !ret )
	{
		// fail ==> delete tmp
		char sz_cmd[MAX_PATH+32] = {0};

		sprintf(sz_cmd, "del %s\\thumbs\\*.%s", str_tmp_ppt_folder_path_ansi, sz_imgext);
		::system(sz_cmd);
		::RemoveDirectory(str_tmp_ppt_folder_path_ansi+"\\thumbs");

		sprintf(sz_cmd, "del %s\\*.%s", str_tmp_ppt_folder_path_ansi, sz_imgext);
		::system(sz_cmd);
		::RemoveDirectory(str_tmp_ppt_folder_path_ansi);
	}

	return ret;
}

BOOL CPPT2Image::InitThumbnail(int nFixWidth, int nFixHeight)
{
	if( nFixWidth<0 && nFixHeight<0 )
		return FALSE;

	m_bMakeThumbnail = true;
	m_nThumbnailWidth = nFixWidth;
	m_nThumbnailHeight = nFixHeight;

	return TRUE;
}
