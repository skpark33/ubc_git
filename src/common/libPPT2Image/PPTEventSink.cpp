// PPTEventSink.cpp: implementation of the CPPTEventSink class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PPTEventSink.h"
//#include "../Schedule.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CPPTEventSink::CPPTEventSink(CPPTInvokerInterface* pParent, CString strPresentationName)
:	m_refCount(0)
,	m_pParent(NULL)
,	m_nSlideCount(0)
,	m_nActiveSlideNum(0)
,	m_bIgnoreEvent(false)
,	m_strPresentationName(_T(""))
{	
	m_pParent	= pParent;
	m_strPresentationName = strPresentationName;
}

CPPTEventSink::~CPPTEventSink()
{

}


/******************************************************************************
*   IUnknown Interfaces -- All COM objects must implement, either directly or 
*   indirectly, the IUnknown interface.
******************************************************************************/
STDMETHODIMP CPPTEventSink::QueryInterface(REFIID riid, void ** ppvObj)
{
	if (riid == IID_IUnknown)
	{
		*ppvObj = static_cast<IEApplication*>(this);
	}	
	else if (riid == IID_IEApplication)
	{	
		*ppvObj = static_cast<IEApplication*>(this);
	}
	else if (riid == IID_IDispatch)
	{	
		*ppvObj = static_cast<IDispatch*>(this);
	}	
	else
	{
	    char clsidStr[256];
	    WCHAR wClsidStr[256];
	    char txt[512];

	    StringFromGUID2(riid, (LPOLESTR)&wClsidStr, 256);
	    // Convert down to ANSI
	    WideCharToMultiByte(CP_ACP, 0, wClsidStr, -1, clsidStr, 256, NULL, NULL);
	    sprintf(txt, "riid is : %s: Unsupported Interface", clsidStr);

		*ppvObj = NULL;
		return E_NOINTERFACE;
	}
	
	static_cast<IUnknown*>(*ppvObj)->AddRef();
	return S_OK;
}

STDMETHODIMP_(ULONG) CPPTEventSink::AddRef()
{
	return ++m_refCount;
}

STDMETHODIMP_(ULONG) CPPTEventSink::Release()
{
	if (--m_refCount == 0)
	{
		delete this;
		return 0;
	}
	return m_refCount;
}

/******************************************************************************
*   IDispatch Interface -- This interface allows this class to be used as an
*   automation server, allowing its functions to be called by other COM
*   objects.  NOT USED IN THIS DEMO
******************************************************************************/
STDMETHODIMP CPPTEventSink::GetTypeInfoCount(UINT *iTInfo)
{
   return E_NOTIMPL;
}

STDMETHODIMP CPPTEventSink::GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo)
{
   return E_NOTIMPL;
}

STDMETHODIMP CPPTEventSink::GetIDsOfNames(REFIID riid,  
                                         OLECHAR **rgszNames, 
                                         UINT cNames,  LCID lcid,
                                         DISPID *rgDispId)
{
   HRESULT hr = E_FAIL;
   return hr;
}


STDMETHODIMP CPPTEventSink::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid,
                                  WORD wFlags, DISPPARAMS* pDispParams,
                                  VARIANT* pVarResult, EXCEPINFO* pExcepInfo,
                                  UINT* puArgErr)
{
   if ((riid != IID_NULL))
      return E_INVALIDARG;
   
   HRESULT hr = S_OK;  // Initialize

   if(m_strPresentationName == "")
   {
//	   __DEBUG__("Empty presentation name", _NULL);
	   return S_OK;
   }//if

   if(m_bIgnoreEvent)
   {
//	   __DEBUG__("\tFile name : ", m_strPresentationName);
//	   __DEBUG__("\tIgnore Event : ", m_bIgnoreEvent);
//	   __DEBUG__("Dispid : ", dispIdMember);
	   return S_OK;
   }//if

/*
   __DEBUG__("========================================================", _NULL);
   __DEBUG__("\tFile name : ", m_strPresentationName);
   switch(dispIdMember)
   {
   case 2001	: __DEBUG__("\tWindowSelectionChange", dispIdMember); break;
   case 2002	: __DEBUG__("\tWindowBeforeRightClick", dispIdMember); break;
   case 2003	: __DEBUG__("\tWindowBeforeDubleClick", dispIdMember); break;
   case 2004	: __DEBUG__("\tPresentationClose", dispIdMember); break;
   case 2005	: __DEBUG__("\tPresentationSave", dispIdMember); break;
   case 2006	: __DEBUG__("\tPresentationOpen", dispIdMember); break;
   case 2007	: __DEBUG__("\tNewPresentation", dispIdMember); break;
   case 2008	: __DEBUG__("\tPresentationNewSlide", dispIdMember); break;
   case 2009	: __DEBUG__("\tWindowActivate", dispIdMember); break;
   case 2010	: __DEBUG__("\tWindowDeactivate", dispIdMember); break;
   case 2011	: __DEBUG__("\tSlideShowBegin", dispIdMember); break;
   case 2012	: __DEBUG__("\tSlideShowNextBuild", dispIdMember); break;
   case 2013	: __DEBUG__("\tSlideShowNextSlide", dispIdMember); break;
   case 2014	: __DEBUG__("\tSlideShowEnd", dispIdMember); break;
   case 2015	: __DEBUG__("\tPresentationPrint", dispIdMember); break;
   case 2016	: __DEBUG__("\tSlideSelectionChanged", dispIdMember); break;
   case 2017	: __DEBUG__("\tColorSchemeChanged", dispIdMember); break;
   case 2018	: __DEBUG__("\tPresentationSave", dispIdMember); break;
   case 2019	: __DEBUG__("\tSlideShowNextClick", dispIdMember); break;
   default		: __DEBUG__("\Unknown", dispIdMember); break;
   }//switch
   __DEBUG__("========================================================", _NULL);
 */

   switch (dispIdMember)
   {
   case 2004:  // PresentationClose( IUnknown* Pres)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   PresentationClose( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   PresentationClose( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;

   case 2006:  // PresentationOpen( IUnknown* Pres)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   PresentationOpen( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   PresentationOpen( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;

   case 2011:   //  SlideShowBegin( SlideShowWindow* Wn)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   SlideShowBegin(/*(SlideShowWindow*)*/*(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   SlideShowBegin(/*(SlideShowWindow*)*/pDispParams->rgvarg[0].punkVal );
				   //SlideShowWindow derived from IUnknown, so cast the punkVal -  but not needed
			   }//if
		   }//if
	   }
	   break;

   case 2013:   // SlideShowNextSlide( SlideShowWindow* Wn)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   SlideShowNextSlide( /*(SlideShowWindow*)*/*(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   SlideShowNextSlide( /*(SlideShowWindow*)*/pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;

   case 2014:   // SlideShowEnd(IUnknown* Pres)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   SlideShowEnd( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   SlideShowEnd( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;
   case 2009:	// ESC key
	   {
		   if(pDispParams->cArgs != 2)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   //SlideShowEscape( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   //SlideShowEscape( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;
   }//switch
 
   return hr;
}

/******************************************************************************
*   EApplication Interface 
/*************************************************************************/


STDMETHODIMP CPPTEventSink::PresentationClose(IUnknown* Pres)
{
	TRACE(" PresentationClose   \r\n");
	//::Sleep(500);
	return S_OK; 
}

STDMETHODIMP CPPTEventSink::PresentationOpen(IUnknown* Pres)
{
	//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
	//pSchedule->PresentationOpen();
	m_pParent->PresentationOpen();

	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSink::SlideShowBegin(IUnknown* Wn)
{
    LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;
	Slides pptSlides;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Wn->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptSlideshowWindow.AttachDispatch(lpDispatch);

	// Get Presentation
	pptPresentation.AttachDispatch(pptSlideshowWindow.GetPresentation());
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
//		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if

	// Get Slides
	pptSlides.AttachDispatch(pptPresentation.GetSlides());
		
	// 슬라이드의 전체 수를 구한다.
	m_nSlideCount = pptSlides.GetCount();
	//SlideShowSettings pptSettings = pptPresentation.GetSlideShowSettings();
	//pptSettings.SetLoopUntilStopped(-1);		//msoTrue
	//m_nSlideCount = (int)pptSettings.GetEndingSlide();
	m_nActiveSlideNum = 1;      
	
	pptSlideshowWindow.ReleaseDispatch();
	pptSlides.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();

	//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
	//pSchedule->SlideShowBegin();
	m_pParent->SlideShowBegin();
	
	//m_switch = 0; // be sure one-time switch is set to 0
	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSink::SlideShowEnd(IUnknown* Pres)
{
	LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Pres->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptPresentation.AttachDispatch(lpDispatch);
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
//		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if

	//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
	////pSchedule->SlideShowEnd();
	//pSchedule->Stop();
	m_pParent->StopPlay();

	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSink::SlideShowNextSlide(IUnknown* Wn)
{
	LPDISPATCH lpDispatch;
    SlideShowWindow pptSlideshowWindow;
	SlideShowView pptSlideShowView;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Wn->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptSlideshowWindow.AttachDispatch(lpDispatch);

	// Get SlideShowView	
	pptSlideShowView.AttachDispatch(pptSlideshowWindow.GetView());
	m_nActiveSlideNum = pptSlideShowView.GetCurrentShowPosition();

//	__DEBUG__("Active slide show number : ", m_nActiveSlideNum);
	
	/*pptSlideshowWindow.ReleaseDispatch();
	pptSlideShowView.ReleaseDispatch();*/
	
	if(m_nActiveSlideNum >= m_nSlideCount)
	{
		//m_bIgnoreEvent = true;
		_Slide pptSlide = pptSlideShowView.GetSlide();
		SlideShowTransition pptTransition = pptSlide.GetSlideShowTransition();
		if(pptTransition.GetAdvanceOnTime() == -1/*msoTrue*/)
		{
			float ftTime = pptTransition.GetAdvanceTime();
//			__DEBUG__("Last slide waiting time", ftTime);
			if(ftTime != 0)
			{
				//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
				//pSchedule->SetSlideShowEnd(ftTime);
				m_pParent->SetSlideShowEnd(ftTime);
			}//if
		}//if
		//pSchedule->Stop();
	}//if

	pptSlideshowWindow.ReleaseDispatch();
	pptSlideShowView.ReleaseDispatch();

	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSink::SlideShowEscape(IUnknown* Pres)
{
	/*
	LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Pres->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptPresentation.AttachDispatch(lpDispatch);
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if
	*/

	m_bIgnoreEvent = true;
	//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
	////pSchedule->SlideShowEnd();
	//pSchedule->EscapeBRW();
	m_pParent->EscapeBRW();

	//Sleep(50);
	return S_OK; 
}

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

CPPTEventSinkEx::CPPTEventSinkEx()
:	m_refCount(0)
//,	m_pParent(NULL)
//,	m_nSlideCount(0)
//,	m_nActiveSlideNum(0)
,	m_bIgnoreEvent(false)
//,	m_strPresentationName(_T(""))
{	
//	m_pParent	= pParent;
//	m_strPresentationName = strPresentationName;
}

CPPTEventSinkEx::~CPPTEventSinkEx()
{

}


/******************************************************************************
*   IUnknown Interfaces -- All COM objects must implement, either directly or 
*   indirectly, the IUnknown interface.
******************************************************************************/
STDMETHODIMP CPPTEventSinkEx::QueryInterface(REFIID riid, void ** ppvObj)
{
	if (riid == IID_IUnknown)
	{
		*ppvObj = static_cast<IEApplication*>(this);
	}	
	else if (riid == IID_IEApplication)
	{	
		*ppvObj = static_cast<IEApplication*>(this);
	}
	else if (riid == IID_IDispatch)
	{	
		*ppvObj = static_cast<IDispatch*>(this);
	}
	else
	{
	    char clsidStr[256];
	    WCHAR wClsidStr[256];
	    char txt[512];

	    StringFromGUID2(riid, (LPOLESTR)&wClsidStr, 256);
	    // Convert down to ANSI
	    WideCharToMultiByte(CP_ACP, 0, wClsidStr, -1, clsidStr, 256, NULL, NULL);
	    sprintf(txt, "riid is : %s: Unsupported Interface", clsidStr);

		*ppvObj = NULL;
		return E_NOINTERFACE;
	}
	
	static_cast<IUnknown*>(*ppvObj)->AddRef();
	return S_OK;
}

STDMETHODIMP_(ULONG) CPPTEventSinkEx::AddRef()
{
	return ++m_refCount;
}

STDMETHODIMP_(ULONG) CPPTEventSinkEx::Release()
{
	if (--m_refCount == 0)
	{
		delete this;
		return 0;
	}
	return m_refCount;
}

/******************************************************************************
*   IDispatch Interface -- This interface allows this class to be used as an
*   automation server, allowing its functions to be called by other COM
*   objects.  NOT USED IN THIS DEMO
******************************************************************************/
STDMETHODIMP CPPTEventSinkEx::GetTypeInfoCount(UINT *iTInfo)
{
   return E_NOTIMPL;
}

STDMETHODIMP CPPTEventSinkEx::GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo)
{
   return E_NOTIMPL;
}

STDMETHODIMP CPPTEventSinkEx::GetIDsOfNames(REFIID riid,  
                                         OLECHAR **rgszNames, 
                                         UINT cNames,  LCID lcid,
                                         DISPID *rgDispId)
{
   HRESULT hr = E_FAIL;
   return hr;
}


STDMETHODIMP CPPTEventSinkEx::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid,
                                  WORD wFlags, DISPPARAMS* pDispParams,
                                  VARIANT* pVarResult, EXCEPINFO* pExcepInfo,
                                  UINT* puArgErr)
{
   if ((riid != IID_NULL))
      return E_INVALIDARG;
   
   HRESULT hr = S_OK;  // Initialize

   //if(m_strPresentationName == "")
   //{
	  // __DEBUG__("Empty presentation name", _NULL);
	  // return S_OK;
   //}//if

	if(m_bIgnoreEvent)
	{
//		__DEBUG__("\tFile name : ", m_strPresentationName);
//		__DEBUG__("\tIgnore Event : ", m_bIgnoreEvent);
//		__DEBUG__("Dispid : ", dispIdMember);
		return S_OK;
	}//if

/*
   __DEBUG__("========================================================", _NULL);
   __DEBUG__("\tFile name : ", m_strPresentationName);
   switch(dispIdMember)
   {
   case 2001	: __DEBUG__("\tWindowSelectionChange", dispIdMember); break;
   case 2002	: __DEBUG__("\tWindowBeforeRightClick", dispIdMember); break;
   case 2003	: __DEBUG__("\tWindowBeforeDubleClick", dispIdMember); break;
   case 2004	: __DEBUG__("\tPresentationClose", dispIdMember); break;
   case 2005	: __DEBUG__("\tPresentationSave", dispIdMember); break;
   case 2006	: __DEBUG__("\tPresentationOpen", dispIdMember); break;
   case 2007	: __DEBUG__("\tNewPresentation", dispIdMember); break;
   case 2008	: __DEBUG__("\tPresentationNewSlide", dispIdMember); break;
   case 2009	: __DEBUG__("\tWindowActivate", dispIdMember); break;
   case 2010	: __DEBUG__("\tWindowDeactivate", dispIdMember); break;
   case 2011	: __DEBUG__("\tSlideShowBegin", dispIdMember); break;
   case 2012	: __DEBUG__("\tSlideShowNextBuild", dispIdMember); break;
   case 2013	: __DEBUG__("\tSlideShowNextSlide", dispIdMember); break;
   case 2014	: __DEBUG__("\tSlideShowEnd", dispIdMember); break;
   case 2015	: __DEBUG__("\tPresentationPrint", dispIdMember); break;
   case 2016	: __DEBUG__("\tSlideSelectionChanged", dispIdMember); break;
   case 2017	: __DEBUG__("\tColorSchemeChanged", dispIdMember); break;
   case 2018	: __DEBUG__("\tPresentationSave", dispIdMember); break;
   case 2019	: __DEBUG__("\tSlideShowNextClick", dispIdMember); break;
   default		: __DEBUG__("\Unknown", dispIdMember); break;
   }//switch
   __DEBUG__("========================================================", _NULL);
 */

   switch (dispIdMember)
   {
   case 2004:  // PresentationClose( IUnknown* Pres)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   PresentationClose( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   PresentationClose( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;

   case 2006:  // PresentationOpen( IUnknown* Pres)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   PresentationOpen( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   PresentationOpen( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;

   case 2011:   //  SlideShowBegin( SlideShowWindow* Wn)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   SlideShowBegin(/*(SlideShowWindow*)*/*(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   SlideShowBegin(/*(SlideShowWindow*)*/pDispParams->rgvarg[0].punkVal );
				   //SlideShowWindow derived from IUnknown, so cast the punkVal -  but not needed
			   }//if
		   }//if
	   }
	   break;

   case 2013:   // SlideShowNextSlide( SlideShowWindow* Wn)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   SlideShowNextSlide( /*(SlideShowWindow*)*/*(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   SlideShowNextSlide( /*(SlideShowWindow*)*/pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;

   case 2014:   // SlideShowEnd(IUnknown* Pres)
	   {
		   if(pDispParams->cArgs != 1)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   SlideShowEnd( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   SlideShowEnd( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;
   case 2009:	// ESC key
	   {
		   if(pDispParams->cArgs != 2)
		   {
			   return E_INVALIDARG;
		   }
		   else
		   {
			   if(pDispParams->rgvarg[0].vt & VT_BYREF)
			   {
				   //SlideShowEscape( *(pDispParams->rgvarg[0].ppunkVal) );
			   }
			   else
			   {
				   //SlideShowEscape( pDispParams->rgvarg[0].punkVal );
			   }//if
		   }//if
	   }
	   break;
   }//switch
 
   return hr;
}

/******************************************************************************
*   EApplication Interface 
/*************************************************************************/


STDMETHODIMP CPPTEventSinkEx::PresentationClose(IUnknown* Pres)
{
	TRACE(" PresentationClose   \r\n");
	//::Sleep(500);
	return S_OK; 
}

STDMETHODIMP CPPTEventSinkEx::PresentationOpen(IUnknown* Pres)
{
/*
	//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
	//pSchedule->PresentationOpen();
	m_pParent->PresentationOpen();

	//Sleep(50);
	return S_OK; 
*/

    LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;
	Slides pptSlides;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Pres->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptSlideshowWindow.AttachDispatch(lpDispatch);

	// Get Presentation
	pptPresentation.AttachDispatch(pptSlideshowWindow.GetPresentation());
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
//		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if

	// Get Slides
	pptSlides.AttachDispatch(pptPresentation.GetSlides());

	// 슬라이드의 전체 수를 구한다.
//	m_nSlideCount = pptSlides.GetCount();
	//SlideShowSettings pptSettings = pptPresentation.GetSlideShowSettings();
	//pptSettings.SetLoopUntilStopped(-1);		//msoTrue
	//m_nSlideCount = (int)pptSettings.GetEndingSlide();
//	m_nActiveSlideNum = 1;      

	pptSlideshowWindow.ReleaseDispatch();
	pptSlides.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();

	CPPTInvokerInterface* parent = GetParentSchedule(m_strPresentationName);
	if( parent )
		parent->PresentationOpen();

	//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
	//pSchedule->SlideShowBegin();
//	m_pParent->SlideShowBegin();

	//m_switch = 0; // be sure one-time switch is set to 0
	//Sleep(50);
	return S_OK;
}

STDMETHODIMP CPPTEventSinkEx::SlideShowBegin(IUnknown* Wn)
{
    LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;
	Slides pptSlides;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Wn->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptSlideshowWindow.AttachDispatch(lpDispatch);

	// Get Presentation
	pptPresentation.AttachDispatch(pptSlideshowWindow.GetPresentation());
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
//		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if

	// Get Slides
	pptSlides.AttachDispatch(pptPresentation.GetSlides());
		
	// 슬라이드의 전체 수를 구한다.
	m_nSlideCount = pptSlides.GetCount();
	//SlideShowSettings pptSettings = pptPresentation.GetSlideShowSettings();
	//pptSettings.SetLoopUntilStopped(-1);		//msoTrue
	//m_nSlideCount = (int)pptSettings.GetEndingSlide();
	m_nActiveSlideNum = 1;      

	pptSlideshowWindow.ReleaseDispatch();
	pptSlides.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();

	//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
	//pSchedule->SlideShowBegin();
//	m_pParent->SlideShowBegin();

	CPPTInvokerInterface* parent = GetParentSchedule(m_strPresentationName);
	if( parent )
		parent->SlideShowBegin();

	//m_switch = 0; // be sure one-time switch is set to 0
	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSinkEx::SlideShowEnd(IUnknown* Pres)
{
	LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Pres->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptPresentation.AttachDispatch(lpDispatch);
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
//		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if

	//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
	////pSchedule->SlideShowEnd();
	//pSchedule->Stop();
//	m_pParent->StopPlay();

	CPPTInvokerInterface* parent = GetParentSchedule(m_strPresentationName);
	if( parent )
		parent->StopPlay();

	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSinkEx::SlideShowNextSlide(IUnknown* Wn)
{
	LPDISPATCH lpDispatch;
    SlideShowWindow pptSlideshowWindow;
	SlideShowView pptSlideShowView;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Wn->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptSlideshowWindow.AttachDispatch(lpDispatch);

	// Get SlideShowView	
	pptSlideShowView.AttachDispatch(pptSlideshowWindow.GetView());
	m_nActiveSlideNum = pptSlideShowView.GetCurrentShowPosition();

//	__DEBUG__("Active slide show number : ", m_nActiveSlideNum);
	
	/*pptSlideshowWindow.ReleaseDispatch();
	pptSlideShowView.ReleaseDispatch();*/
	
	if(m_nActiveSlideNum >= m_nSlideCount)
	{
		//m_bIgnoreEvent = true;
		_Slide pptSlide = pptSlideShowView.GetSlide();
		SlideShowTransition pptTransition = pptSlide.GetSlideShowTransition();
		if(pptTransition.GetAdvanceOnTime() == -1/*msoTrue*/)
		{
			float ftTime = pptTransition.GetAdvanceTime();
//			__DEBUG__("Last slide waiting time", ftTime);
			if(ftTime != 0)
			{
				//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
				//pSchedule->SetSlideShowEnd(ftTime);
//				m_pParent->SetSlideShowEnd(ftTime);
				CPPTInvokerInterface* parent = GetParentSchedule(m_strPresentationName);
				if( parent )
					parent->SetSlideShowEnd(ftTime);
			}//if
		}//if
		//pSchedule->Stop();
	}//if

	pptSlideshowWindow.ReleaseDispatch();
	pptSlideShowView.ReleaseDispatch();

	//Sleep(50);
	return S_OK; 
}

STDMETHODIMP CPPTEventSinkEx::SlideShowEscape(IUnknown* Pres)
{
/*
	LPDISPATCH lpDispatch;
	_Presentation pptPresentation;
    SlideShowWindow pptSlideshowWindow;

	// get the IDispatch of the SlideShowWindow object
    HRESULT hr = Pres->QueryInterface(IID_IDispatch, (void**)&lpDispatch);
    pptPresentation.AttachDispatch(lpDispatch);
	//파일이름 확인
	//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
	//대소문자를 구분하지 않고 비교하도록 한다.
	CString strName = pptPresentation.GetFullName();
	if(m_strPresentationName.CompareNoCase(strName) != 0)
	{
		pptSlideshowWindow.ReleaseDispatch();
		pptPresentation.ReleaseDispatch();
		__ERROR__("Fail to get presentation", m_strPresentationName);

		return S_OK;
	}//if
*/

//	m_bIgnoreEvent = true;
	//CPowerPointSchedule* pSchedule = (CPowerPointSchedule*)m_pParent;
	////pSchedule->SlideShowEnd();
	//pSchedule->EscapeBRW();
//	m_pParent->EscapeBRW();

	CPPTInvokerInterface* parent = GetParentSchedule(m_strPresentationName);
	if( parent )
		parent->EscapeBRW();

	//Sleep(50);
	return S_OK; 
}

void CPPTEventSinkEx::AddParentSchedule(LPCTSTR lpszFilename, CPPTInvokerInterface* pParent)
{
	if( lpszFilename==NULL || pParent==NULL ) return;

	CString str_filename = lpszFilename;
	str_filename.MakeLower();

	m_mapSchedules.SetAt(str_filename, (LPVOID)pParent);
}

CPPTInvokerInterface* CPPTEventSinkEx::GetParentSchedule(LPCTSTR lpszFilename)
{
	if( lpszFilename==NULL ) return NULL;

	CString str_filename = lpszFilename;
	str_filename.MakeLower();

	CPPTInvokerInterface* sch = NULL;
	if( m_mapSchedules.Lookup(str_filename, (void*&)sch) )
		return sch;

	return NULL;
}

void CPPTEventSinkEx::SetCurrentPresentationName(LPCTSTR lpszFilename)
{
	m_strPresentationName = lpszFilename;
}
