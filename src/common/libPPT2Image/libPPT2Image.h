// libPPT2Image.h : libPPT2Image DLL의 기본 헤더 파일입니다.
//

#pragma once

//#ifndef __AFXWIN_H__
//	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
//#endif

#include "resource.h"		// 주 기호입니다.

// ClibPPT2ImageApp
// 이 클래스의 구현을 보려면 libPPT2Image.cpp를 참조하십시오.
//


class CPPT2Image
{
public:
	CPPT2Image();
	virtual ~CPPT2Image();

protected:
	char	m_szErrorMessage[1024];
	bool	m_bMakeThumbnail;
	int		m_nThumbnailWidth;
	int		m_nThumbnailHeight;

public:
	LPCSTR	GetErrorMessage() { return m_szErrorMessage; };

	BOOL	SaveToImage(LPCSTR lpszPPTFullpath, LPCSTR ImgExt="PNG");
	BOOL	SaveToImage(LPCWSTR lpwszPPTFullpath, LPCWSTR ImgExt=L"PNG");

	// nFixWidth,nFixHeight ==> -1(auto-size), 둘다 -1값은 허용안됨
	BOOL	InitThumbnail(int nFixWidth, int nFixHeight);
};
