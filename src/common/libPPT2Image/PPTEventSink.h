// PPTEventSink.h: interface for the CPPTEventSink class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PPTEVENTSINK_H__B871B535_7661_4DEA_86DA_20479AB6F90F__INCLUDED_)
#define AFX_PPTEVENTSINK_H__B871B535_7661_4DEA_86DA_20479AB6F90F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "msppt9.h"

// 파워포인트 Type Library의 EApplication 인터페이스 ID
// EApplication : 파워포인트 어플리케이션 이벤트에 대한 인터페이스
static const GUID IID_IEApplication =  
    {0x914934C2,0x5A91,0x11CF, {0x87,0x00,0x00,0xAA,0x00,0x60,0x26,0x3b}};

static BYTE I4_parms[] = VTS_I4;
static BYTE I4I4_parms[] = VTS_I4 VTS_I4;

#define DEFAULT_STARTPAGE_INDEX		1

struct IEApplication : public IDispatch // Pretty much copied from typelib
{
    STDMETHODIMP QueryInterface(REFIID riid, void ** ppvObj) = 0; //HRESULT _stdcall
    STDMETHODIMP_(ULONG) AddRef()  = 0;  // Note the underscore
    STDMETHODIMP_(ULONG) Release() = 0;

    STDMETHODIMP GetTypeInfoCount(UINT *iTInfo) = 0;
    STDMETHODIMP GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo) = 0;
    STDMETHODIMP GetIDsOfNames(REFIID riid, OLECHAR **rgszNames, 
                                  UINT cNames,  LCID lcid, DISPID *rgDispId) = 0;
    STDMETHODIMP Invoke(DISPID dispIdMember, REFIID riid, LCID lcid,
                                  WORD wFlags, DISPPARAMS* pDispParams,
                                  VARIANT* pVarResult, EXCEPINFO* pExcepInfo,
                                  UINT* puArgErr) = 0;
};

interface CPPTInvokerInterface
{
public:
	virtual bool	StopPlay() = 0;;					///<schedule을 정지하는 함수
	virtual void	EscapeBRW(void) = 0;			///<파워포인트에 ESC 키가 눌러져서 BRW를 종료한다.
	virtual	void	PresentationOpen(void) = 0;		///<Presentation이 Open 된 이후 슬라이드 쇼를 시작하도록 한다.
	virtual void	SlideShowBegin(void) = 0;		///<슬라이드 쇼가 시작되었음을 설정
	virtual void	SetSlideShowEnd(float ftTime)=0;///<슬라이드 쇼를 일정한 시간후에 종료하도록 설정
};

//interface CPPTInvokerInterfaceEx
//{
//public:
//	virtual bool	StopPlay(LPCSTR lpszFilename) = 0;;					///<schedule을 정지하는 함수
//	virtual void	EscapeBRW(LPCSTR lpszFilename) = 0;			///<파워포인트에 ESC 키가 눌러져서 BRW를 종료한다.
//	virtual	void	PresentationOpen(LPCSTR lpszFilename) = 0;		///<Presentation이 Open 된 이후 슬라이드 쇼를 시작하도록 한다.
//	virtual void	SlideShowBegin(LPCSTR lpszFilename) = 0;		///<슬라이드 쇼가 시작되었음을 설정
//	virtual void	SetSlideShowEnd(LPCSTR lpszFilename, float ftTime)=0;///<슬라이드 쇼를 일정한 시간후에 종료하도록 설정
//};

class CPPTEventSink : public IEApplication  
{
public:
	CPPTEventSink(CPPTInvokerInterface* pParent, CString strPresentationName);
	virtual ~CPPTEventSink();
public:
	DWORD m_Cookie;
	int m_refCount;

	/***** IUnknown Methods *****/
    STDMETHODIMP QueryInterface(REFIID riid, void ** ppvObj);
    STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();

	/***** IDispatch Methods *****/
	STDMETHODIMP LoadTypeInfo(ITypeInfo** pptinfo, REFCLSID clsid, LCID lcid);
    STDMETHODIMP GetTypeInfoCount(UINT *iTInfo);
    STDMETHODIMP GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo);
    STDMETHODIMP GetIDsOfNames(REFIID riid, OLECHAR **rgszNames, UINT cNames,  LCID lcid, DISPID *rgDispId);
    STDMETHODIMP Invoke(DISPID dispIdMember, 
                    REFIID riid, 
                    LCID lcid,
                    WORD wFlags, DISPPARAMS* pDispParams,
                    VARIANT* pVarResult, EXCEPINFO* pExcepInfo,       
					UINT* puArgErr);

	//***** EApplication Methods ****
	STDMETHODIMP SlideShowBegin(IUnknown* Wn);
	STDMETHODIMP SlideShowEnd(IUnknown* Pres);
	STDMETHODIMP PresentationClose(IUnknown* Pres);
	STDMETHODIMP PresentationOpen(IUnknown* Pres);
	STDMETHODIMP SlideShowEscape(IUnknown* Pres);

	 ///// This event used to check un-shown animations, //
    /////  and to reset the m_shapeCount for the new slide. //
    STDMETHODIMP SlideShowNextSlide(IUnknown* Wn);
	
protected:
	CPPTInvokerInterface*			m_pParent;								///<부모 창
	int				m_nSlideCount;							///<슬라이드 갯수
	int				m_nActiveSlideNum;						///<현재 진행중인 슬라이드 번호
	bool			m_bIgnoreEvent;							///<이벤트를 무시할지 여부를 나타내는 플래그

	CString			m_strPresentationName;					///<이벤트를 처리해야는 Presentation 이름
};

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

class CPPTEventSinkEx : public IEApplication  
{
public:
	CPPTEventSinkEx();
	virtual ~CPPTEventSinkEx();
public:
	DWORD m_Cookie;
	int m_refCount;

	/***** IUnknown Methods *****/
    STDMETHODIMP QueryInterface(REFIID riid, void ** ppvObj);
    STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();

	/***** IDispatch Methods *****/
	STDMETHODIMP LoadTypeInfo(ITypeInfo** pptinfo, REFCLSID clsid, LCID lcid);
    STDMETHODIMP GetTypeInfoCount(UINT *iTInfo);
    STDMETHODIMP GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo);
    STDMETHODIMP GetIDsOfNames(REFIID riid, OLECHAR **rgszNames, UINT cNames,  LCID lcid, DISPID *rgDispId);
    STDMETHODIMP Invoke(DISPID dispIdMember, 
                    REFIID riid, 
                    LCID lcid,
                    WORD wFlags, DISPPARAMS* pDispParams,
                    VARIANT* pVarResult, EXCEPINFO* pExcepInfo,       
					UINT* puArgErr);

	//***** EApplication Methods ****
	STDMETHODIMP SlideShowBegin(IUnknown* Wn);
	STDMETHODIMP SlideShowEnd(IUnknown* Pres);
	STDMETHODIMP PresentationClose(IUnknown* Pres);
	STDMETHODIMP PresentationOpen(IUnknown* Pres);
	STDMETHODIMP SlideShowEscape(IUnknown* Pres);

	///// This event used to check un-shown animations, //
    /////  and to reset the m_shapeCount for the new slide. //
    STDMETHODIMP SlideShowNextSlide(IUnknown* Wn);

	void	AddParentSchedule(LPCSTR lpszFilename, CPPTInvokerInterface* pParent);
	void	SetCurrentPresentationName(LPCSTR lpszFilename);
	void	SetIgnoreEvent(bool bIgnoreEvent) { m_bIgnoreEvent=bIgnoreEvent; };

protected:

	//CPPTInvokerInterface*			m_pParent;								///<부모 창
	int				m_nSlideCount;							///<슬라이드 갯수
	int				m_nActiveSlideNum;						///<현재 진행중인 슬라이드 번호
	bool			m_bIgnoreEvent;							///<이벤트를 무시할지 여부를 나타내는 플래그
	CString			m_strPresentationName;					///<이벤트를 처리해야는 Presentation 이름

	CMapStringToPtr		m_mapSchedules;
	CPPTInvokerInterface*	GetParentSchedule(LPCSTR lpszFilename);
};

#endif // !defined(AFX_PPTEVENTSINK_H__B871B535_7661_4DEA_86DA_20479AB6F90F__INCLUDED_)
