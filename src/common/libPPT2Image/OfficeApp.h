// libPPT2Image.h : libPPT2Image DLL의 기본 헤더 파일입니다.
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include <afxmt.h>
#include "PPTEventSink.h"
#include "resource.h"		// 주 기호입니다.

// ClibPPT2ImageApp
// 이 클래스의 구현을 보려면 libPPT2Image.cpp를 참조하십시오.
//


class COfficeApp : public _Application
{
public:
	static COfficeApp* getInstance();
	static COfficeApp* getInstance(LPCSTR lpszFullpath,	///<Single tone 인스턴스 생성 함수
									//LPCSTR lpszFilename,
									CPPTInvokerInterface* pParent);
	static void clearInstance();							///<Single tone 인스턴스 소멸 함수

	virtual ~COfficeApp();									///<소멸자

	static void SetOpenComplete(CString strFilename, bool bOpen=true);
	static bool IsAllOpen() { return (_refcnt==_mapOpenPPT.GetCount()); };

	static LPCSTR			getTempFolderName();			///<임시파일을 복사할 디렉토리
protected:
	COfficeApp();											///<생성자

	static COfficeApp*		_instance;						///<Single tone 인스턴스
	static CCriticalSection	_lock;							///<동기화 객체
	static int				_refcnt;						///<참조 횟수
	static int				_opencnt;						///<열린 ppt 개수
//	static CString			_tempfolder;					///<임시 디렉토리 이름

	static IConnectionPoint*	_pptEventConnectionPoint;	///<ConnectionPoint 포인터
	static CPPTEventSinkEx*		_pptEventSink;				///<파워포인트 이벤트 싱크 객체

	static bool ConnectPPTEventSink(COfficeApp* instance);						///<파워포인트 이벤트 싱크 설정 함수
	static void DisconnectPPTEventSink();					///<파워포인트 이벤트 싱크 해제 함수

	static CMapStringToString _mapOpenPPT;

public:
	BOOL	Open(LPCSTR lpszFullpath/*, LPCSTR lpszFilename*/);
	BOOL	Play(LPCSTR lpszFullpath);
	BOOL	SlideShowBegin(LPCSTR lpszFullpath);
	//BOOL	SlideShowEnd(LPCSTR lpszFilename);
	BOOL	ClosePresentation(LPCSTR lpszFullpath);

	float				m_fPosX;							///<슬라이드쇼 창이 위치할 X 좌표
	float				m_fPosY;							///<슬라이드쇼 창이 위치할 Y 좌표
	float				m_fWidth;							///<슬라이드쇼 창 넓이
	float				m_fHeight;							///<슬라이드쇼 창 높이

	BOOL	SaveToImage(LPCSTR lpszFullpath, LPCSTR imgExt, LPSTR szErrMsg);
	BOOL	SaveToThumbnail(LPCSTR lpszFullpath, int nWidth, int nHeight, LPCSTR imgExt, LPSTR szErrMsg);
};
