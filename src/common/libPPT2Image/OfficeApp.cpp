// libPPT2Image.cpp : 해당 DLL의 초기화 루틴을 정의합니다.
//

#include "stdafx.h"
#include "OfficeApp.h"

using namespace std;

#include <common/libScratch/scratchUtil.h>
#include <process.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// COfficeApp
COfficeApp* COfficeApp::_instance = NULL;
CCriticalSection COfficeApp::_lock;
int COfficeApp::_refcnt = 0;
int COfficeApp::_opencnt = 0;
IConnectionPoint*	COfficeApp::_pptEventConnectionPoint = NULL;
CPPTEventSinkEx*	COfficeApp::_pptEventSink = NULL;
CMapStringToString COfficeApp::_mapOpenPPT;

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ingle tone 인스턴스 생성 함수 \n
/// @return <형: COfficeApp*> \n
///			<COfficeApp*: Single tone 인스턴스> \n
///			<NULL: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
COfficeApp* COfficeApp::getInstance()
{
	_lock.Lock();

	scratchUtil* aUtil = scratchUtil::getInstance();
	ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
	if( ulPid == 0 )
	{
		// 실행프로세스 없으면 -> 삭제하고 초기화
		DisconnectPPTEventSink();
		delete _instance;
		_instance = NULL;
		_refcnt = 0;
		_opencnt = 0;
		_mapOpenPPT.RemoveAll();
	}

	_lock.Unlock();

	return _instance;
}

COfficeApp* COfficeApp::getInstance(LPCTSTR lpszFullpath, CPPTInvokerInterface* pParent)
{
	_lock.Lock();
	while( _instance != NULL )
	{
		scratchUtil* aUtil = scratchUtil::getInstance();
		ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
		if( ulPid == 0 )
		{
			// 실행프로세스 없으면 -> 삭제하고 초기화
			DisconnectPPTEventSink();
			delete _instance;
			_instance = NULL;
			_refcnt = 0;
			_opencnt = 0;
			_mapOpenPPT.RemoveAll();
		}
		else// if( lpszFilename != NULL && pParent != NULL )
		{
			// 이미 있음 & 새로생성 -> 카운트++
			_refcnt++;
			if( _pptEventSink && pParent != NULL )
				_pptEventSink->AddParentSchedule(lpszFullpath, pParent);

			_instance->Open(lpszFullpath);
		}

		_lock.Unlock();
		return _instance;
	}

	_instance = new COfficeApp();
/*
	////처음 인스턴스 생성시에 이전에 실행중인 파워포인트를 강제 종료시킨다.
	//__DEBUG__("Terminate Powerpoint", _NULL);
	//scratchUtil* aUtil = scratchUtil::getInstance();
	//ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
	//__DEBUG__("scratchUtil : getPid", (ULONGLONG)ulPid);
	//if(ulPid > 0)
	//{
	//	int nRet = aUtil->killProcess(ulPid);
	//	__DEBUG__("scratchUtil : killProcess", nRet);
	//}//if
	//Sleep(500);

	//이미 실행중인 PPT가 있는지 확인
	CLSID clsid;
	CLSIDFromProgID(L"Powerpoint.Application", &clsid);
	IUnknown *pUnk = NULL;
	HRESULT hr = GetActiveObject(clsid, NULL, (IUnknown**)&pUnk);
	if( SUCCEEDED(hr) )
	{
		// 이미 존재 -> 가져옴
		IDispatch *pDisp = NULL;
		hr = pUnk->QueryInterface(IID_IDispatch, (void **)&pDisp);
		_instance->AttachDispatch(pDisp);
	}
	else*/
	{
		// 새로 생성
		if( !_instance->CreateDispatch(_T("Powerpoint.Application")) )
		{
//			__DEBUG__("Powerpoint CreateDispatch fail", _NULL);
			delete _instance;
			_instance = NULL;
		}
	}

	//if( NULL != pUnk ) pUnk->Release();

	if( _instance == NULL )
	{
		_lock.Unlock();
		return _instance;
	}

	//Make the application visible but not minimized
//	_instance->SetVisible((long)TRUE);

	// ppWindowNormal = 1, ppWindowMinimized = 2, ppWindowMaximized = 3
//	_instance->SetWindowState((long)2);	// Start with PPT visible - makes 
//												//  presentation visible, too.

	ConnectPPTEventSink(_instance);

	_refcnt = 1;
	if( _pptEventSink )
		_pptEventSink->AddParentSchedule(lpszFullpath, pParent);

	_instance->Open(lpszFullpath);

	_lock.Unlock();

	return _instance;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Single tone 인스턴스 소멸 함수 \n
/////////////////////////////////////////////////////////////////////////////////
void COfficeApp::clearInstance()
{
	_lock.Lock();
	_refcnt--;

	if( _instance==NULL || _refcnt > 0 )
	{
		// nothing or remain ref-count
		_lock.Unlock();
		return;
	}

	// ignore event
	if( _pptEventSink )
		_pptEventSink->SetIgnoreEvent(true);

	// quit & kill ppt-process
	scratchUtil* aUtil = scratchUtil::getInstance();
	ULONG ulPid = aUtil->getPid("POWERPNT.EXE");
	if( ulPid > 0 )
	{
		DisconnectPPTEventSink();
//		__DEBUG__("Powerpoint Quit", _NULL);
		_instance->Quit();
		_instance->ReleaseDispatch();
	}
	delete _instance;
	_instance = NULL;
	_refcnt = 0;
	_opencnt = 0;
	_mapOpenPPT.RemoveAll();

//	__DEBUG__("Terminate Powerpoint", _NULL);
	// 파워 포인트가 확실하게 죽었는지 5초동안 확인한다
	bool powerpnt_alive = true;
	for(int i=0; i<5 && powerpnt_alive; i++)
	{
		::Sleep(1000);

		ulPid = aUtil->getPid("POWERPNT.EXE");
		if( ulPid == 0 )
		{
			powerpnt_alive = false;						
			break;
		}

//		__DEBUG__("POWERPNT still alive", (unsigned int)ulPid);
		int nRet = aUtil->killProcess(ulPid);
//		__DEBUG__("scratchUtil : kill powerpoint", nRet);
	}

	//실행중인 파워포인트를 강제 종료시킨다.
	if( powerpnt_alive )
	{
		ulPid = aUtil->getPid("POWERPNT.EXE");
//		__DEBUG__("scratchUtil : powerpoint PID", (unsigned int)ulPid);
		if(ulPid > 0)
		{
			int nRet = aUtil->killProcess(ulPid);
//			__DEBUG__("scratchUtil : kill powerpoint", nRet);
		}//if
	}

	_lock.Unlock();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 임시파일을 복사할 디렉토리 \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR COfficeApp::getTempFolderName()
{
	static CString str_tmp_foler_name = _T("");

	if( str_tmp_foler_name.GetLength() == 0 )
	{
		LPTSTR tmp_dir = _tgetenv(_T("Temp"));
		if( tmp_dir == NULL )
			tmp_dir = _tgetenv(_T("Tmp"));

		if( tmp_dir != NULL )
		{
			// get default-temp-dir
			CString str_tmp;
			str_tmp.Format(_T("%s\\UBC_PPT\\"), tmp_dir);

			// remove temp-dir
			CString str_arg;
			// cmd.exe /c rmdir /s /q "%temp%\\UBC_PPT\\"
			//str_arg.Format("cmd.exe /c rmdir /s /q \"%s\"", str_tmp);
			str_arg.Format(_T("rmdir /s /q \"%s\""), str_tmp);

//			__DEBUG__("Delete Powerpoint Temp Directory", str_arg);
			//scratchUtil::getInstance()->createProcess(str_arg, "");
			_tsystem(str_arg);

			// create temp-dir
//			__DEBUG__("Create Powerpoint Temp Directory", str_tmp);
			::CreateDirectory(str_tmp, NULL);

			// get temp-dir
			int pid = getpid();
			str_tmp_foler_name.Format(_T("%s%d\\"), str_tmp, pid);
//			__DEBUG__("Create Powerpoint My Temp Directory", str_tmp_foler_name);
			::CreateDirectory(str_tmp_foler_name, NULL);
		}
	}

	return str_tmp_foler_name;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
COfficeApp::COfficeApp()
{
	m_fPosX = 0.f;
	m_fPosY = 0.f;
	m_fWidth = 0.f;
	m_fHeight = 0.f;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
COfficeApp::~COfficeApp()
{
}

void COfficeApp::SetOpenComplete(CString strFilename, bool bOpen)
{
	strFilename.MakeLower();

	if( bOpen )
	{
		_mapOpenPPT.SetAt(strFilename, strFilename);
	}
	else
	{
		//
		// remove from map(_mapOpenPPT)
		//
	}
}

bool COfficeApp::ConnectPPTEventSink(COfficeApp* instance)
{
	//if(m_pptApp == NULL)
	//{
	//	return false;
	//}//if

	DisconnectPPTEventSink();

	///*********************** Start of code to get connection point **************
  	//  Declare the events we want to catch.
  	//
  	//  Look for the coclass for Application in the typelib, msppt9.olb
  	//  then look for the word "source." The interface - EApplication -
  	//  is the next search target. When you find it you'll see the 
  	//  following guid a the events you'll be able to sink.
  	//  914934C2-5A91-11CF-8700-00AA0060263B
  	//	static const GUID IID_IEApplication =
  	//	{0x914934C2,0x5A91,0x11CF, {0x87,0x00,0x00,0xAA,0x00,0x60,0x26,0x3b}};
  
  	//  Steps for setting up events.
  	// 1. Get server's IConnectionPointContainer interface.
  	// 2. Call IConnectionPointContainerFindConnectionPoint()
  	//    to find the event we want to catch.
  	// 3. Call IConnectionPoint::Advise() with the IUnknown
  	//    interface of our implementation of the events.

	// Get IDispatch interface for Automation...
	IDispatch *pDisp = NULL;
	pDisp = instance->m_lpDispatch;

  	// Get server's (PPT) IConnectionPointContainer interface.
  	IConnectionPointContainer *pConnPtContainer;
  	HRESULT hr = pDisp->QueryInterface(
  							IID_IConnectionPointContainer,
  							(void **)&pConnPtContainer);

  	if( !SUCCEEDED(hr) )
  	{
		CString strError;
		strError.Format(_T("Couldn't get IConnectionPointContainer interface."));
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
 	}//if

  	// Find connection point for events we're interested in.
  	hr = pConnPtContainer->FindConnectionPoint(
  											IID_IEApplication,
  											&_pptEventConnectionPoint
  											);

  	if( !SUCCEEDED(hr) )
  	{
		CString strError;
		strError.Format(_T("Couldn't find connection point via event GUID."));
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
  	}//if

	_pptEventSink = new CPPTEventSinkEx();

	IUnknown *pUnk = NULL;
  	_pptEventSink->QueryInterface(IID_IUnknown, (void**)&pUnk);
  	if( pUnk == NULL )
	{
		CString strError;
		strError.Format(_T("PPTEventSink fail to QueryInterface."));
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
	}//if

  	// Setup advisory connection!
  	hr = _pptEventConnectionPoint->Advise(pUnk, &_pptEventSink->m_Cookie);
  	if( !SUCCEEDED(hr) )
	{
		CString strError;
		strError.Format(_T("PPTEventSink fail to Advise."));
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);

  		return false;
	}//if

  	// Release IConnectionPointContainer interface.
  	pConnPtContainer->Release();

	return true; 
}

void COfficeApp::DisconnectPPTEventSink()
{
	if( _pptEventConnectionPoint )
	{
		_pptEventConnectionPoint->Unadvise(_pptEventSink->m_Cookie); // PPT releases cookie
 		_pptEventConnectionPoint->Release();
		_pptEventConnectionPoint = NULL;
	}

	if( _pptEventSink )
	{
		_pptEventSink->Release();
		_pptEventSink = NULL;
	}
}

BOOL COfficeApp::Open(LPCTSTR lpszFullpath)
{
//	__DEBUG_BEGIN__(lpszFullpath);

	IDispatch *pDisp = NULL;
	Presentations	pptPresentations;
	_Presentation	pptPresentation;

	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
//		__DEBUG__("GetPresentaions fail !!!", lpszFullpath);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);

	//열려있는 presentation중에 같은 파일이 있는지 검사하여
	//이미 열려있다면 닫아주어야 한다.
	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if( strFullName.CompareNoCase(lpszFullpath) == 0 )
		{
			//pptPresentation.Close();
			//pptPresentation.ReleaseDispatch();
			//Sleep(500);
			//break;

			pptPresentation.ReleaseDispatch();
			pptPresentations.ReleaseDispatch();
			return TRUE; // 이미 열려있으면 재활용하는걸로 교체
		}
		pptPresentation.ReleaseDispatch();
	}

	/*
	msoCTrue 1 지원되지 않음 
	msoFalse 0 False 
	msoTriStateMixed -2 지원되지 않음 
	msoTriStateToggle -3 지원되지 않음 
	msoTrue -1 True 
	*/
//	__DEBUG__("Presentation open", lpszFullpath);
	pDisp = pptPresentations.Open(lpszFullpath,      
		(long)0,		//Read-only
		(long)0,		//Untitled
		(long)0			//NoWindow
		);
//	__DEBUG__("Presentation opened", lpszFullpath);

	pptPresentations.ReleaseDispatch();

	return TRUE;
}

BOOL COfficeApp::Play(LPCTSTR lpszFullpath)
{
	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowSettings	pptSlideshow;			///<MS-PowerPoint SlideShow settings

	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
//		__DEBUG__("GetPresentaions fail !!!", lpszFullpath);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(lpszFullpath) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
//		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
//		__ERROR__("Fail to get presentation", lpszFullpath); 
		//Stop();
		pptPresentations.ReleaseDispatch();
		return FALSE;
	}//if

/*
	pDisp = pptPresentation.GetSlideShowSettings();

	pptSlideshow.AttachDispatch(pDisp);
	pptSlideshow.SetLoopUntilStopped(-1);		//msoTrue
	pptSlideshow.Run();

	pptSlideshow.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/
	if( _pptEventSink )
		_pptEventSink->SetCurrentPresentationName(lpszFullpath);

	//
/*
	SlideShowSettings	pptSettings;

	pDisp = pptPresentation.GetSlideShowSettings();

	pptSettings.AttachDispatch(pDisp);
	pptSettings.SetRangeType(1);			//ppShowAll
	pptSettings.SetLoopUntilStopped(-1);	//msoTrue
	pptSettings.SetAdvanceMode(2);			//ppSlideShowUseSlideTimings
	pptSettings.SetShowType(1);				//ppShowTypeSpeaker
	pptSettings.Run();

	pptSettings.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/
	pDisp = pptPresentation.GetSlideShowSettings();

	pptSlideshow.AttachDispatch(pDisp);
	pptSlideshow.SetLoopUntilStopped(-1);		//msoTrue
	pptSlideshow.Run();

	pptSlideshow.ReleaseDispatch();


	//
	SlideShowWindow		pptSlideShowWnd;
	SlideShowView		pptSlideShowView;

	pDisp = pptPresentation.GetSlideShowWindow();
	if( pDisp == NULL ) return FALSE;

	pptSlideShowWnd.AttachDispatch(pDisp);
	pptSlideShowView.AttachDispatch(pptSlideShowWnd.GetView());
	pptSlideShowView.First(); // 첫장으로 이동

	pptSlideShowView.ReleaseDispatch();
	pptSlideShowWnd.ReleaseDispatch();


	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();

	return TRUE;
}

BOOL COfficeApp::SlideShowBegin(LPCTSTR lpszFullpath)
{
	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowWindow		pptSlideShowWnd;

	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
//		__DEBUG__("GetPresentaions fail !!!", lpszFullpath);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(lpszFullpath) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
//		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
//		__ERROR__("Fail to get presentation", lpszFullpath);
		//Stop();
		pptPresentations.ReleaseDispatch();
		return false;
	}//if

	pDisp = pptPresentation.GetSlideShowWindow();
	if( pDisp == NULL ) return FALSE;

	pptSlideShowWnd.AttachDispatch(pDisp);
	// 위치, 크기 지정
	pptSlideShowWnd.SetHeight(m_fHeight);
	pptSlideShowWnd.SetWidth(m_fWidth);
	pptSlideShowWnd.SetTop(m_fPosY);
	pptSlideShowWnd.SetLeft(m_fPosX);

	pptSlideShowWnd.ReleaseDispatch();

	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();

	return true;
}

BOOL COfficeApp::ClosePresentation(LPCSTR lpszFullpath)
{
	IDispatch *pDisp = NULL;
	Presentations		pptPresentations;		///<MS-PowerPoint Presentations
	_Presentation		pptPresentation;
	SlideShowSettings	pptSlideshow;			///<MS-PowerPoint SlideShow settings

	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
//		__DEBUG__("GetPresentaions fail !!!", lpszFullpath);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);

	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(lpszFullpath) == 0)
		{
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
//		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
//		__ERROR__("Fail to get presentation", lpszFullpath); 
		//Stop();
		pptPresentations.ReleaseDispatch();
		return FALSE;
	}//if

/*
	pDisp = pptPresentation.GetSlideShowSettings();

	pptSlideshow.AttachDispatch(pDisp);
	pptSlideshow.SetLoopUntilStopped(-1);		//msoTrue
	pptSlideshow.Run();

	pptSlideshow.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/
	//
/*
	SlideShowSettings	pptSettings;

	pDisp = pptPresentation.GetSlideShowSettings();

	pptSettings.AttachDispatch(pDisp);
	pptSettings.SetRangeType(1);			//ppShowAll
	pptSettings.SetLoopUntilStopped(-1);	//msoTrue
	pptSettings.SetAdvanceMode(2);			//ppSlideShowUseSlideTimings
	pptSettings.SetShowType(1);				//ppShowTypeSpeaker
	pptSettings.Run();

	pptSettings.ReleaseDispatch();
	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();
*/

	//
	SlideShowWindow		pptSlideShowWnd;
	SlideShowView		pptSlideShowView;

	try
	{
		pDisp = pptPresentation.GetSlideShowWindow();
		if( pDisp == NULL ) return FALSE;
	}
	catch(...)
	{
		return FALSE;
	}

	pptSlideShowWnd.AttachDispatch(pDisp);
	pptSlideShowView.AttachDispatch(pptSlideShowWnd.GetView());
	pptSlideShowView.Exit(); // 슬라이드쇼 종료

	pptSlideShowView.ReleaseDispatch();
	pptSlideShowWnd.ReleaseDispatch();

	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();

	return TRUE;
}

BOOL COfficeApp::SaveToImage(LPCSTR lpszFullpath, LPCSTR imgExt, char* szErrMsg)
{
	IDispatch *pDisp = NULL;
	Presentations	pptPresentations;
	_Presentation	pptPresentation;

	// get Presentations
	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
		sprintf_s(szErrMsg, 1023, "NOTHING PRESENTATIONS !!! (%s, %s)", lpszFullpath, imgExt);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);

	// get Presentation
	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(lpszFullpath) == 0)
		{
			// find presentation
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
		// no presentation
//		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
//		__ERROR__("Fail to get presentation", lpszFullpath);
		//Stop();
		sprintf_s(szErrMsg, 1023, "NOTHING PRESENTATION !!! (%s, %s)", lpszFullpath, imgExt);
		pptPresentations.ReleaseDispatch();
		return FALSE;
	}//if

	// get PageSetup
	PageSetup pptPageSetup;
	pDisp = pptPresentation.GetPageSetup();
	if( pDisp == NULL )
	{
		sprintf_s(szErrMsg, 1023, "NOTHING PAGESETUP !!! (%s, %s)", lpszFullpath, imgExt);
		return FALSE;
	}
	pptPageSetup.AttachDispatch(pDisp);

	// calculate max-width or max-height (by ratio)
	float ppt_width = pptPageSetup.GetSlideWidth();
	float ppt_height = pptPageSetup.GetSlideHeight();

	int img_width=0, img_height=0;
	if( ppt_width > ppt_height )
	{
		// landscape
		img_width = 3072; // max-width
		img_height = ppt_height * img_width / ppt_width;
	}
	else
	{
		// portrait
		img_height = 3072; // max-height
		img_width = ppt_width * img_height / ppt_height;
	}

	pptPageSetup.ReleaseDispatch();

	// export to image
	char drv[MAX_PATH]={0},dir[MAX_PATH]={0},fn[MAX_PATH]={0},ext[MAX_PATH]={0};
	_splitpath(lpszFullpath, drv, dir, fn, ext);
	if( strstr(fn,".") == NULL )
		sprintf(ext, "%s%s%s", drv, dir, fn);
	else
		sprintf(ext, "%s%s%s.", drv, dir, fn);
	pptPresentation.Export(ext, imgExt, img_width, img_height);

	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();

	return TRUE;
}

BOOL COfficeApp::SaveToThumbnail(LPCTSTR lpszFullpath, int nWidth, int nHeight, LPCTSTR imgExt, LPTSTR szErrMsg)
{
	IDispatch *pDisp = NULL;
	Presentations	pptPresentations;
	_Presentation	pptPresentation;

	// get Presentations
	pDisp = GetPresentations();
	if( pDisp == NULL )
	{
		_stprintf_s(szErrMsg, 1023, _T("NOTHING PRESENTATIONS !!! (%s, %s)"), lpszFullpath, imgExt);
		return FALSE;
	}
	pptPresentations.AttachDispatch(pDisp);

	// get Presentation
	VARIANT paramVariant;
	paramVariant.vt = VT_I4;
	CString strFullName;
	for(long lCnt=1; lCnt<=pptPresentations.GetCount(); lCnt++)
	{
		paramVariant.lVal = lCnt;
		pptPresentation.AttachDispatch(pptPresentations.Item(paramVariant));
		strFullName = pptPresentation.GetFullName();
		//Office 2003에서는 드라이브 문자가 대문자로 표시되므로
		//대소문자를 구분하지 않고 비교하도록 한다.
		if(strFullName.CompareNoCase(lpszFullpath) == 0)
		{
			// find presentation
			break;
		}//if
		pptPresentation.ReleaseDispatch();
	}//for

	if(pptPresentation.m_lpDispatch == NULL)
	{
		// no presentation
//		__DEBUG__("pptPresentation.m_lpDispatch is NULL", _NULL);
//		__ERROR__("Fail to get presentation", lpszFullpath);
		//Stop();
		_stprintf_s(szErrMsg, 1023, _T("NOTHING PRESENTATION !!! (%s, %s)"), lpszFullpath, imgExt);
		pptPresentations.ReleaseDispatch();
		return FALSE;
	}//if

	// get PageSetup
	PageSetup pptPageSetup;
	pDisp = pptPresentation.GetPageSetup();
	if( pDisp == NULL )
	{
		_stprintf_s(szErrMsg, 1023, _T("NOTHING PAGESETUP !!! (%s, %s)"), lpszFullpath, imgExt);
		return FALSE;
	}
	pptPageSetup.AttachDispatch(pDisp);

	// calculate max-width or max-height (by ratio)
	float ppt_width = pptPageSetup.GetSlideWidth();
	float ppt_height = pptPageSetup.GetSlideHeight();

	int img_width=0, img_height=0;
	if( nWidth<0 )
	{
		// portrait
		img_height = nHeight; // max-height
		img_width = (int)(ppt_width * img_height / ppt_height);
	}
	else
	{
		// landscape
		img_width = nWidth; // max-width
		img_height = (int)(ppt_height * img_width / ppt_width);
	}

	pptPageSetup.ReleaseDispatch();

	// export to image
	TCHAR drv[MAX_PATH]={0},dir[MAX_PATH]={0},fn[MAX_PATH]={0},ext[MAX_PATH]={0};
	_tsplitpath(lpszFullpath, drv, dir, fn, ext);
	//if( strstr(fn,".") == NULL )
		_stprintf(ext, _T("%s%s%s\\thumbs"), drv, dir, fn);
	//else
	//	sprintf(ext, "%s%s%s.", drv, dir, fn);
	pptPresentation.Export(ext, imgExt, img_width, img_height);

	pptPresentation.ReleaseDispatch();
	pptPresentations.ReleaseDispatch();

	return TRUE;
}
