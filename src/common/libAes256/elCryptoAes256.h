/** \class Aes256Crypto
 *  Copyright ⓒ 2016, SQIsoft ELiga. All rights reserved.
 *
 *  \brief 블록암호모드 CBC 모드
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2016/01/11 11:01:00
 */

#ifndef _elCryptoAes256_h_
#define _elCryptoAes256_h_

#include <string>
#include "AES256.h"

class elCryptoAes256
{
public:
    elCryptoAes256(void);
    virtual ~elCryptoAes256(void);

    bool    Init(std::string key32, std::string iv16="QRYC1949OPEI3829", AES256::Chaining_Mode mode=AES256::CBC);
    std::string  Encrypt(std::string plain_text);
    std::string  Decrypt(std::string cipher_text);

protected:
    std::string  key32_;
    std::string  iv16_;
    AES256::Chaining_Mode block_cipher_mode_;

    AES256* aes256_;

};

#endif // _elCryptoAes256_h_