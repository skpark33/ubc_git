#include "stdafx.h"
#ifdef _COP_MSC_

#include "ubcCachedIni.h"
#include <IMAGEHLP.H>
#pragma comment(lib, "imagehlp.lib")

#include "ci/libConfig/ciEnv.h"
#include "ci/libBase/ciStringUtil.h"
#include "ci/libBase/ciStringTokenizer.h"

#include "common/libProfileManager/ProfileManager.h"

#include "util/libEncoding/Encoding.h"

ciSET_DEBUG(10,"ubcCachedIni");


//////////////////////////////////////////////////////////////////////////////////////////////////

CCriticalSection ubcCachedIni::m_csProfileManager;
CMapStringToPtr ubcCachedIni::m_mapProfileManager;

PMANAGER_ITEM* ubcCachedIni::getInstance(const char* fullpath, bool bUTF8name)
{
	m_csProfileManager.Lock();

	char drv[8]={0}, dir[MAX_PATH]={0}, fn[MAX_PATH]={0}, ext[MAX_PATH]={0};
	_splitpath(fullpath, drv, dir, fn, ext);

	CString filename;
	filename.Format("%s%s", fn, ext);
	//filename.MakeLower(); // CInvalidArgException in UTF-8
	_strlwr((LPSTR)(LPCSTR)filename); // MakeLower()

	PMANAGER_ITEM* item = NULL;
	if( m_mapProfileManager.Lookup(filename, (void*&)item) == 0 )
	{
		item = new PMANAGER_ITEM;
		item->count = 0;
		item->available = true;
		item->modified = false;
		item->pManager = new CProfileManager();
		item->pManager->Read(fullpath, bUTF8name);

		m_mapProfileManager.SetAt(filename, item);
	}

	item->count++;

	m_csProfileManager.Unlock();

	return item;
}

void ubcCachedIni::clearInstance(const char* fullpath)
{
	m_csProfileManager.Lock();

	char drv[8]={0}, dir[MAX_PATH]={0}, fn[MAX_PATH]={0}, ext[MAX_PATH]={0};
	_splitpath(fullpath, drv, dir, fn, ext);

	CString filename;
	filename.Format("%s%s", fn, ext);
	filename.MakeLower();

	PMANAGER_ITEM* item = NULL;
	if( m_mapProfileManager.Lookup(filename, (void*&)item) )
	{
		item->count--;

		if(item->available && item->modified)
		{
			item->pManager->Write();
			item->modified = false;
		}

		if(item->count == 0)
		{
			delete item->pManager;
			delete item;
			m_mapProfileManager.RemoveKey(filename);
		}
	}

	m_csProfileManager.Unlock();
}


//////////////////////////////////////////////////////////////////////////////////////////////////

ubcCachedIni::ubcCachedIni(const char* filename, const char* path, const char* sub, bool bUTF8name)
: ubcIni( filename, path, sub )
, m_pManagerItem ( NULL )
, m_bLoaded ( false )
{
	Read(_fullpath.c_str(), bUTF8name);
}

ubcCachedIni::~ubcCachedIni(void)
{
	Clear();
}

boolean 
ubcCachedIni::get(const char* section,const char* key, ciULongLong& value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	ciString str = this->GetProfileString(section, key, "");
	
	value = ciStringUtil::atoull(str.c_str());

	return true;
}
boolean 
ubcCachedIni::get(const char* section,const char* key, ciFloat& value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	ciString str = this->GetProfileString(section, key, "");
	
	value = atof(str.c_str());

	return true;
}


boolean 
ubcCachedIni::get(const char* section,const char* key, ciString& value, const char* defaultValue)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	value = this->GetProfileString(section, key, defaultValue);
	//ciDEBUG(1,("ini value founded---- : [%s]%s=%s", section,key, value.c_str()));
	return true;
}

boolean 
ubcCachedIni::get(const char* section,const char* key, ciBoolean& value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	value = (ciBoolean)GetProfileInt(section, key);
	return ciTrue;
}

boolean 
ubcCachedIni::get(const char* section,const char* key, ciShort& value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	value = (ciShort)GetProfileInt(section, key);
	return ciTrue;
}

boolean 
ubcCachedIni::get(const char* section,const char* key, ciLong& value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	value = (ciLong)GetProfileInt(section, key);
	return ciTrue;
}

boolean 
ubcCachedIni::get(const char* section,const char* key, ciUShort& value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	value = (ciUShort)GetProfileInt(section, key);
	return ciTrue;
}

boolean 
ubcCachedIni::get(const char* section,const char* key, ciULong& value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	value = (ciULong)GetProfileInt(section, key);
	return ciTrue;
}

boolean 
ubcCachedIni::get(const char* section,const char* key, ciTime& value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	value.set(GetProfileString(section, key));
	return true;
}

boolean 
ubcCachedIni::get_sec(const char* section,const char* key, ciTime& value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	value.set(
		time_t(
				strtoul(GetProfileString(section, key), NULL, 10)
		)
	);
	return true;
}

boolean 
ubcCachedIni::set(const char* section, const char* key , ciULongLong value)
{
	char buf[32];
	sprintf(buf, "%u", value);
	return set(section, key, buf);
}
boolean 
ubcCachedIni::set(const char* section, const char* key , ciFloat value)
{
	char buf[32];
	sprintf(buf, "%f", value);
	return set(section, key, buf);
}


boolean 
ubcCachedIni::set(const char* section, const char* key , const char* value)
{
	if( section==NULL || strlen(section)==0 || key==NULL || strlen(key)==0 ) return false;

	ciDEBUG(1,("ini value update : [%s]%s=%s", section, key, value));
	return WriteProfileString(section, key, value);
}

boolean 
ubcCachedIni::set(const char* section, const char* key , ciBoolean value)
{
	char buf[32];
	sprintf(buf, "%d", value);
	return set(section, key, buf);
}

boolean 
ubcCachedIni::set(const char* section, const char* key , ciShort value)
{
	char buf[32];
	sprintf(buf, "%d", value);
	return set(section, key, buf);
}

boolean 
ubcCachedIni::set(const char* section, const char* key , ciUShort value)
{
	char buf[32];
	sprintf(buf, "%d", value);
	return set(section, key, buf);
}

boolean 
ubcCachedIni::set(const char* section, const char* key , ciLong value)
{
	char buf[32];
	sprintf(buf, "%ld", value);
	return set(section, key, buf);
}

boolean 
ubcCachedIni::set(const char* section, const char* key , ciULong value)
{
	char buf[32];
	sprintf(buf, "%u", value);
	return set(section, key, buf);
}

boolean 
ubcCachedIni::set(const char* section, const char* key , ciTime value)
{
	return set(section, key, value.getTimeString());
}

CString ubcCachedIni::toString()
{
	if( m_pManagerItem == NULL || m_pManagerItem->available==false) return "";

	return m_pManagerItem->pManager->toString();
}

void ubcCachedIni::fromString(CString& str)
{
	if( m_pManagerItem == NULL || m_pManagerItem->available==false) return;
	return m_pManagerItem->pManager->fromString(str);
}

void ubcCachedIni::Clear()
{
	clearInstance(m_strFilename);
	m_pManagerItem = NULL;
	m_bLoaded = false;
}

BOOL ubcCachedIni::Read(LPCSTR lpszFilename /*= NULL*/, bool bUTF8name /*= true*/)
{
	if(lpszFilename)
	{
		ciDEBUG(1,("Read(%s)", lpszFilename));
	}
	else
	{
		ciDEBUG(1,("Read(NULL)"));
	}

	if(m_bLoaded) Clear();

	if(lpszFilename && strlen(lpszFilename)>0)
	{
		m_strFilename = lpszFilename;
	}

	m_pManagerItem = getInstance(m_strFilename, bUTF8name);
	if(m_pManagerItem) m_bLoaded = true;

	ciDEBUG(1,("Read(%s) End", m_strFilename));
	return TRUE;
}

BOOL ubcCachedIni::Write()
{
	if(m_pManagerItem == NULL || m_pManagerItem->available==false) return FALSE;

	BOOL ret_val = m_pManagerItem->pManager->Write();
	m_pManagerItem->modified = false;

	return ret_val;
}

BOOL ubcCachedIni::Delete()
{
	if(m_pManagerItem == NULL) return FALSE;

	m_pManagerItem->available = false;

	return TRUE;
}

CString	ubcCachedIni::GetProfileString(CString strTabName, CString strKeyName, CString strDefault /*= _T("")*/)
{
	if(m_pManagerItem == NULL || m_pManagerItem->available==false) return strDefault;

	strTabName.MakeLower();
	strKeyName.MakeLower();

	CLock lock(m_lockValue);
	return m_pManagerItem->pManager->GetProfileString(strTabName, strKeyName, strDefault);
}

DWORD ubcCachedIni::GetProfileString(CString strTabName, CString strKeyName, CString strDefault, CString& strReturnedString)
{
	strReturnedString = GetProfileString(strTabName, strKeyName, strDefault);
	return strReturnedString.GetLength();
}

int ubcCachedIni::GetProfileInt(CString strTabName, CString strKeyName, int nDefault/*=0*/)
{
	CString strDefault;
	strDefault.Format(_T("%d"), nDefault);

	return _ttoi(GetProfileString(strTabName, strKeyName, strDefault));
}

BOOL ubcCachedIni::WriteProfileString(CString strTabName, CString strKeyName, CString strString, BOOL bImmediately/*=FALSE*/)
{
	if(m_pManagerItem == NULL || m_pManagerItem->available==false) return FALSE;

	strTabName.MakeLower();
	strKeyName.MakeLower();

	CLock lock(m_lockValue);
	m_pManagerItem->pManager->WriteProfileString(strTabName, strKeyName, strString);

	if(bImmediately)
	{
		Write();
	}
	else
		m_pManagerItem->modified = true;

	return TRUE;
}

BOOL ubcCachedIni::WriteProfileInt(CString strTabName, CString strKeyName, int nValue, BOOL bImmediately/*=FALSE*/)
{
	CString strString;
	strString.Format(_T("%d"), nValue);

	return WriteProfileString(strTabName, strKeyName, strString, bImmediately);
}

void ubcCachedIni::Lock()
{
	if(m_pManagerItem == NULL || m_pManagerItem->available==false) return;
	m_pManagerItem->pManager->Lock();
}

void ubcCachedIni::Unlock()
{
	if(m_pManagerItem == NULL || m_pManagerItem->available==false) return;
	m_pManagerItem->pManager->Unlock();
}
#endif
