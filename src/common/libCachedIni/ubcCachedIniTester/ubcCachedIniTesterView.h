// ubcCachedIniTesterView.h : interface of the CubcCachedIniTesterView class
//


#pragma once
#include "afxwin.h"


class CubcCachedIniTesterView : public CFormView
{
protected: // create from serialization only
	CubcCachedIniTesterView();
	DECLARE_DYNCREATE(CubcCachedIniTesterView)

public:
	enum{ IDD = IDD_UBCCACHEDINITESTER_FORM };

// Attributes
public:
	CubcCachedIniTesterDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CubcCachedIniTesterView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	CWinThread*	m_pThread[4];

public:
	CString	m_strFilename[4];
	CEdit	m_editFilename[4];
	CEdit	m_editLog;

	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButtonGo();

	LRESULT	OnPostLog(WPARAM wParam, LPARAM lParam);

};

#ifndef _DEBUG  // debug version in ubcCachedIniTesterView.cpp
inline CubcCachedIniTesterDoc* CubcCachedIniTesterView::GetDocument() const
   { return reinterpret_cast<CubcCachedIniTesterDoc*>(m_pDocument); }
#endif

typedef struct {
	HWND wnd;
	CString filename;
	int threadid;
} THREAD_PARAM;

static UINT TestFunc(LPVOID pParam);
