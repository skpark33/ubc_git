// ubcCachedIniTesterDoc.h : interface of the CubcCachedIniTesterDoc class
//


#pragma once


class CubcCachedIniTesterDoc : public CDocument
{
protected: // create from serialization only
	CubcCachedIniTesterDoc();
	DECLARE_DYNCREATE(CubcCachedIniTesterDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CubcCachedIniTesterDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


