// ubcCachedIniTesterDoc.cpp : implementation of the CubcCachedIniTesterDoc class
//

#include "stdafx.h"
#include "ubcCachedIniTester.h"

#include "ubcCachedIniTesterDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CubcCachedIniTesterDoc

IMPLEMENT_DYNCREATE(CubcCachedIniTesterDoc, CDocument)

BEGIN_MESSAGE_MAP(CubcCachedIniTesterDoc, CDocument)
END_MESSAGE_MAP()


// CubcCachedIniTesterDoc construction/destruction

CubcCachedIniTesterDoc::CubcCachedIniTesterDoc()
{
	// TODO: add one-time construction code here

}

CubcCachedIniTesterDoc::~CubcCachedIniTesterDoc()
{
}

BOOL CubcCachedIniTesterDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CubcCachedIniTesterDoc serialization

void CubcCachedIniTesterDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CubcCachedIniTesterDoc diagnostics

#ifdef _DEBUG
void CubcCachedIniTesterDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CubcCachedIniTesterDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CubcCachedIniTesterDoc commands
