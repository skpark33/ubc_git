// ubcCachedIniTesterView.cpp : implementation of the CubcCachedIniTesterView class
//

#include "stdafx.h"
#include "ubcCachedIniTester.h"

#include "ubcCachedIniTesterDoc.h"
#include "ubcCachedIniTesterView.h"

#include "../ubcCachedIni.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define		WM_POST_LOG		(WM_USER + 1025)


// CubcCachedIniTesterView

IMPLEMENT_DYNCREATE(CubcCachedIniTesterView, CFormView)

BEGIN_MESSAGE_MAP(CubcCachedIniTesterView, CFormView)
	ON_BN_CLICKED(IDC_BUTTON1, &CubcCachedIniTesterView::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CubcCachedIniTesterView::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CubcCachedIniTesterView::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CubcCachedIniTesterView::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON_GO, &CubcCachedIniTesterView::OnBnClickedButtonGo)
	ON_MESSAGE(WM_POST_LOG, OnPostLog)
END_MESSAGE_MAP()

// CubcCachedIniTesterView construction/destruction

CubcCachedIniTesterView::CubcCachedIniTesterView()
	: CFormView(CubcCachedIniTesterView::IDD)
{
	// TODO: add construction code here

}

CubcCachedIniTesterView::~CubcCachedIniTesterView()
{
}

void CubcCachedIniTesterView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FILENAME_1, m_editFilename[0]);
	DDX_Control(pDX, IDC_EDIT_FILENAME_2, m_editFilename[1]);
	DDX_Control(pDX, IDC_EDIT_FILENAME_3, m_editFilename[2]);
	DDX_Control(pDX, IDC_EDIT_FILENAME_4, m_editFilename[3]);
	DDX_Control(pDX, IDC_EDIT_LOG, m_editLog);
}

BOOL CubcCachedIniTesterView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CubcCachedIniTesterView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	m_editFilename[0].SetWindowText("C:\\Project\\utv1\\config\\data\\UBCVariables.ini");
	m_editFilename[1].SetWindowText("..\\config\\data\\ubcvariables.ini");
	m_editFilename[2].SetWindowText("C:\\Project\\utv1\\config\\data\\UBCConnect.ini");
	m_editFilename[3].SetWindowText("..\\config\\data\\ubcconnect.ini");
}


// CubcCachedIniTesterView diagnostics

#ifdef _DEBUG
void CubcCachedIniTesterView::AssertValid() const
{
	CFormView::AssertValid();
}

void CubcCachedIniTesterView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CubcCachedIniTesterDoc* CubcCachedIniTesterView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CubcCachedIniTesterDoc)));
	return (CubcCachedIniTesterDoc*)m_pDocument;
}
#endif //_DEBUG


// CubcCachedIniTesterView message handlers

void CubcCachedIniTesterView::OnBnClickedButton1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CubcCachedIniTesterView::OnBnClickedButton2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CubcCachedIniTesterView::OnBnClickedButton3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CubcCachedIniTesterView::OnBnClickedButton4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CubcCachedIniTesterView::OnBnClickedButtonGo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString filename[4];

	for(int i=0; i<4; i++) m_editFilename[i].GetWindowText(filename[i]);

	CFileStatus fs;
	for(int i=0; i<4; i++)
	{
		if( !CFile::GetStatus(filename[i], fs) )
		{
			CString msg;
			msg.Format("File not found : %s", filename[i]);
			::AfxMessageBox(msg, MB_ICONWARNING);
			return;
		}
	}

	for(int i=0; i<4; i++)
	{
		//const char fn[] = {0x54, 0x45, 0x73, 0x74, 0xED, 0x85, 0x8C, 0xEC, 0x8A, 0xA4, 0xED, 0x8A, 0xB8, 0x00 };
		THREAD_PARAM* param = new THREAD_PARAM;
		param->wnd = GetSafeHwnd();
		param->filename = filename[i]; //fn;
		param->threadid = i;
		m_pThread[i] = ::AfxBeginThread(TestFunc, (LPVOID)param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pThread[i]->m_bAutoDelete = TRUE;
	}

	for(int i=0; i<4; i++)
	{
		m_pThread[i]->ResumeThread();
	}
}

LRESULT CubcCachedIniTesterView::OnPostLog(WPARAM wParam, LPARAM lParam)
{
	CString msg;
	switch(wParam)
	{
	case 0:
		msg.Format("Thread[%d] Write[%d]\r\n", wParam, lParam);
		break;

	case 1:
		msg.Format("Thread[%d] Read[%d]\r\n", wParam, lParam);
		break;

	case 2:
		msg.Format("\tThread[%d] Write[%d]\r\n", wParam, lParam);
		break;

	case 3:
		msg.Format("\tThread[%d] Read[%d]\r\n", wParam, lParam);
		break;
	}

	m_editLog.ReplaceSel(msg);

	return 0;
}


UINT TestFunc(LPVOID pParam)
{
	THREAD_PARAM* param = (THREAD_PARAM*)pParam;
	HWND parent_wnd = param->wnd;
	CString filename = param->filename;
	int thread_id = param->threadid;
	delete param;

	srand( (unsigned)time( NULL ) );

	ubcCachedIni ini(filename);

	for(int i=0; i<100; i++)
	{
		long sleep = rand() % 10;
		::Sleep(sleep);

		if(thread_id == 0 || thread_id == 2)
		{
			// WRITE
			ini.set("Test", "Sleep", sleep);
		}
		else
		{
			// READ
			ini.get("Test", "Sleep", sleep);
		}

		::PostMessage(parent_wnd, WM_POST_LOG, thread_id, sleep);
	}

	return 0;
}
