//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ubcCachedIniTester.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_UBCCACHEDINITESTER_FORM     101
#define IDR_MAINFRAME                   128
#define IDR_ubcCachedIniTesTYPE         129
#define IDC_EDIT_FILENAME_1             1000
#define IDC_BUTTON1                     1001
#define IDC_EDIT_FILENAME_2             1002
#define IDC_EDIT_FILENAME_3             1003
#define IDC_EDIT_FILENAME_4             1004
#define IDC_BUTTON2                     1005
#define IDC_BUTTON3                     1006
#define IDC_BUTTON4                     1007
#define IDC_EDIT_LOG                    1008
#define IDC_BUTTON5                     1009
#define IDC_BUTTON_GO                   1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
