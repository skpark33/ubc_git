// ubcCachedIniTester.h : main header file for the ubcCachedIniTester application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CubcCachedIniTesterApp:
// See ubcCachedIniTester.cpp for the implementation of this class
//

class CubcCachedIniTesterApp : public CWinApp
{
public:
	CubcCachedIniTesterApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CubcCachedIniTesterApp theApp;