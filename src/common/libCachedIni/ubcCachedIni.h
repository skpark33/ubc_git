#pragma once
#ifdef _COP_MSC_
#include <afxmt.h>
#include <afxtempl.h>
#include "common/libCommon/ubcIni.h"

class CProfileManager;

typedef struct {
	CProfileManager*	pManager;
	int count;
	bool available;
	bool modified;
} PMANAGER_ITEM;

class ubcCachedIni : public ubcIni
{
protected:
	static	CCriticalSection	m_csProfileManager;
	static	CMapStringToPtr		m_mapProfileManager;	// key:filename -> value:*CProfileManager_pointer
	static	PMANAGER_ITEM*		getInstance(const char* fullpath, bool bUTF8name);
	static	void				clearInstance(const char* fullpath);

public:
	//ubcCachedIni();
	ubcCachedIni(const char* filename, const char* path="", const char* sub="", bool bUTF8name=true);
	//ubcCachedIni(LPCTSTR lpszFilename);
	virtual ~ubcCachedIni(void);

protected:
	CString	m_strFilename;
	PMANAGER_ITEM*	m_pManagerItem;
	bool	m_bLoaded;

	CCriticalSection	m_lockValue;
	class CLock
	{
	public:
		CLock(CCriticalSection& lock) { m_lock=&lock; lock.Lock(); };
		~CLock() { m_lock->Unlock(); };
	protected:
		CCriticalSection* m_lock;
	};

public:
	CString	toString();
	void	fromString(CString& str);

	// from ubcIni
	virtual boolean		get(const char* section, const char* key, ciString& pValue, const char* defaltValue="");
	virtual boolean		get(const char* section, const char* key, ciShort& pValue);
	virtual boolean		get(const char* section, const char* key, ciLong& pValue);
	virtual boolean		get(const char* section, const char* key, ciUShort& pValue);
	virtual boolean		get(const char* section, const char* key, ciULong& pValue);
	virtual boolean		get(const char* section, const char* key, ciTime& pValue);
			boolean		get_sec(const char* section, const char* key, ciTime& pValue);
	virtual boolean		get(const char* section, const char* key, ciBoolean& pValue);
			boolean		get(const char* section, const char* key, ciULongLong& pValue);
			boolean		get(const char* section, const char* key, ciFloat& pValue);

	virtual boolean		set(const char* section, const char* key, const char* pValue);
	virtual boolean		set(const char* section, const char* key, ciShort pValue);
	virtual boolean		set(const char* section, const char* key, ciLong pValue);
	virtual boolean		set(const char* section, const char* key, ciUShort pValue);
	virtual boolean		set(const char* section, const char* key, ciULong pValue);
	virtual boolean		set(const char* section, const char* key, ciTime pValue);
	virtual boolean		set(const char* section, const char* key, ciBoolean pValue);
			boolean		set(const char* section, const char* key, ciULongLong pValue);
			boolean		set(const char* section, const char* key, ciFloat pValue);
	// from ubcIni end

//	void	SetFileName(CString strFilename) { m_strFilename = strFilename; }
	void	Clear();

	BOOL	Read(LPCSTR lpszFilename = NULL, bool bUTF8name = true);
	BOOL	Write();
	BOOL	Delete();

	CString	GetProfileString(CString strTabName, CString strKeyName, CString strDefault = _T(""));
	DWORD	GetProfileString(CString strTabName, CString strKeyName, CString strDefault, CString& strReturnedString);
	int		GetProfileInt(CString lpTabName, CString lpKeyName, int nDefault=0);

	BOOL	WriteProfileString(CString lpTabName, CString lpKeyName, CString strString, BOOL bImmediately=FALSE);
	BOOL	WriteProfileInt(CString lpTabName, CString lpKeyName, int nValue, BOOL bImmediately=FALSE);

	void	Lock();		// 스레드에서 사용전에 반드시 Lock실행
	void	Unlock();	// 스레드에서 사용후에 반드시 Unlock실행
};
#else
#include "ci/libConfig/ciIni.h"
class ubcCachedIni : public ciIni
{
public:
	ubcCachedIni(const char* filename, const char* path="", const char* sub="", bool bUTF8name=true) : ciIni(filename, path,sub) {};
};
#endif
