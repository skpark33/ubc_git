/*! \file PJLink.C
 *  Copyright �� 2007, SQIsoft. All rights reserved.
 *
 *  \brief UTV PJLink
 *  (Environment: VBroker 7.0, Windows 2003 SP2)
 *
 *  \author 
 *  \version 1.0
 *  \date 2007/08/27 11:01:00
*/

#ifndef _PJLink_h_
#define _PJLink_h_

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include "ci/libBase/ciBaseType.h"

//
// COMMON DEFINITION
//

#define UNKNOWN_REASON	"unknown reason"

//
// Authentication
//
#define	RES_AUTH_NO_SECURITY "PJLINK 0\r"

//
// POWER CONTROL
//
#define CON_POWER_ON					"%1POWR 1\r"
#define CON_POWER_OFF					"%1POWR 0\r"
#define GET_POWER_STATE					"%1POWR ?\r"

#define RES_GET_POWER_STATE_OFF			"%1POWR=0\r"
#define RES_GET_POWER_STATE_ON			"%1POWR=1\r"
#define RES_GET_POWER_STATE_COOLING		"%1POWR=2\r"
#define RES_GET_POWER_STATE_WARMUP		"%1POWR=3\r"

#define RES_CON_POWER_OK				"%1POWR=OK\r"

#define RES_POWER_ERR2					"%1POWR=ERR2\r"   // out_of_parameter
#define RES_POWER_ERR3					"%1POWR=ERR3\r"	// Unavailable time
#define RES_POWER_ERR4					"%1POWR=ERR4\r"	// Projector failure

#define RES_POWER_ERR2_STR				"out of parameter"
#define RES_POWER_ERR3_STR				"Unavailable time"
#define RES_POWER_ERR4_STR				"Projector failure"



class PJLink  {
public:
	PJLink(const char* pId, const char* pIp, ciUInt pPort=4352);
	~PJLink();

	ciBoolean	connect();
	void		disconnect();

	ciBoolean	powerOn();
	ciBoolean	powerOff();
	int			getPowerState();

	void		clearErr() {_errMsg = "";}
	const char*	getErr() { return _errMsg.c_str();}
	
protected:

	ciBoolean	_readline(ciString& outVal, int timeout=2);
	ciBoolean	_writeline(ciString& inVal);
	ciBoolean	_writeline(const char* inVal);

	ciBoolean	_connect();
	ciBoolean	_authentication();
	void		_sleep(int sec);
	ciBoolean	_wait(int sec);

	void		_setErr(const char* err) {  _errMsg += err; _errMsg += "\n"; }
	void		_setErrF(const char* pFormat,...);

	ciString 	_ldpId;
	ciString 	_ip;
	ciUInt 		_port;
	SOCKET		_sockfd;
	ciString	_errMsg;
};
		
#endif //_PJLink_h_
