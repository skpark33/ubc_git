// FTPClient_sampleDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"

#include "FTPClient.h"
#include "FTPProtocolOutput.h"
using namespace nsFTP;

#include "afxcmn.h"

typedef struct
{
	tstring	directive; // upload, download
	tstring	filename;
} THREAD_PARAM;
typedef CArray<THREAD_PARAM,THREAD_PARAM&> THREAD_PARAM_LIST;

// CFTPClient_sampleDlg 대화 상자
class CFTPClient_sampleDlg : public CDialog
{
// 생성입니다.
public:
	CFTPClient_sampleDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FTPCLIENT_SAMPLE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	static CFTPClient m_FtpClient;
	CFTPProtocolOutput m_ProtocolOutput;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	static UINT ThreadCallFunc(LPVOID pParam); // 스레드 함수

public:
	CEdit m_ipAddress;
	CEdit m_user;
	CEdit m_password;
	CButton m_connectBtn;
	CButton m_uploadBtn;
	CButton m_downloadBtn;
	BOOL  m_connect;
	CTreeCtrl m_localTree;
	CTreeCtrl m_remoteTree;

	afx_msg void OnConnect();	
	afx_msg void OnBnClickedUpload();
	afx_msg void OnBnClickedDownload();
	afx_msg void OnBnClickedStop();

	afx_msg void OnTvnSelchangedLocalTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTvnSelchangedRemoteTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkLocalTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkRemoteTree(NMHDR *pNMHDR, LRESULT *pResult);
};
