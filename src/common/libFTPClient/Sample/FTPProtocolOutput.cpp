#include "stdafx.h"
#include "FTPProtocolOutput.h"
#include "Definements.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace nsFTP;

BEGIN_MESSAGE_MAP(CFTPProtocolOutput, CRichEditCtrl)
END_MESSAGE_MAP()

CFTPProtocolOutput::CFTPProtocolOutput()
: m_receiveCounter(0)
{
}

CFTPProtocolOutput::~CFTPProtocolOutput()
{
}

DWORD CALLBACK CFTPProtocolOutput::ProtocolStreamInCallback(DWORD_PTR dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb)
{
	CMemFile* pMf = reinterpret_cast<CMemFile*>(dwCookie);
	*pcb = pMf->Read(pbBuff, cb);
	return 0;
}

void CFTPProtocolOutput::OnSendCommand(const tstring& strCommand)
{
	if( strCommand.length()==0 )
		return;

	CString cszLine;
	if( strCommand.length()>4 && strCommand.substr(0, 5)==_T("PASS ") ) {
		cszLine.Format(_T("> PASS **********\r\n"));
	} else {
		cszLine.Format(_T("> %s\r\n"), strCommand.c_str());
	}
	WriteLine(cszLine.GetBuffer(), RGB(0, 0, 255));
}

void CFTPProtocolOutput::OnResponse(const CReply& response)
{
	tstring strResponse = response.Value();
	if( strResponse.length()==0 )
		return;

	COLORREF crText = RGB(0, 150, 0);
	switch( strResponse.at(0) )
	{
	case _T('4'):
		crText = RGB(200, 200, 0);
		break;
	case _T('5'):
		crText = RGB(255, 0, 0);
		break;
	}

	WriteLine(_T("< ") + CString(strResponse.c_str()) + _T("\r\n"), crText);
}

void CFTPProtocolOutput::OnInternalError(const tstring& strErrorMsg, const tstring& strFileName, DWORD dwLineNr)
{
	CString cszMsg;
	cszMsg.Format(_T("%s ==> File \"%s\" (%d)\r\n"), strErrorMsg.c_str(), strFileName.c_str(), dwLineNr);
	WriteLine(cszMsg, RGB(255, 0, 0));
}

void CFTPProtocolOutput::WriteLine(const char* cszLine, COLORREF crText)
{
	if( m_hWnd )
	{
		CHARFORMAT cf;
		cf.dwMask    = CFM_BOLD|CFM_COLOR;
		cf.dwEffects = CFE_BOLD;
		cf.crTextColor = crText;

		SetSelectionCharFormat(cf);
		/*
		CMemFile mf;
		mf.Write(cszLine, strlen(cszLine));
		mf.SeekToBegin();

		EDITSTREAM es;
		es.dwCookie = reinterpret_cast<DWORD_PTR>(&mf);
		es.pfnCallback = CFTPProtocolOutput::ProtocolStreamInCallback; 
		StreamIn(SF_TEXT|SFF_SELECTION, es);
		SetSel(-1, -1);
		LineScroll(1);*/

		int nBegin = GetTextLength();
		SetSel(nBegin, nBegin);
		ReplaceSel(cszLine);
		SetSel(-1, 0);
		nBegin = GetTextLength();
		SetSel(nBegin, nBegin);

		CString message;
		GetWindowText(message);
		printf("%s\n", message.GetBuffer());
	}
}

void CFTPProtocolOutput::OnBytesReceived(const TByteVector& Buffer, long lReceivedBytes)
{
//	if ((m_receiveCounter%100) == 0)
//		WriteLine("#", RGB(0, 0, 0));
//	++m_receiveCounter;
}
