// FTPClient_sampleDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "FTPClient_sample.h"
#include "FTPClient_sampleDlg.h"

#include "FTPDataTypes.h"
#include "Definements.h"

#if defined(_WIN32)
#   include <winsock2.h>
//  WinSock DLL을 사용할 버전
#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0
#   include <time.h>
#else
#   include <sys/socket.h>       /* for socket(), connect(), send(), and recv() */
#   include <arpa/inet.h>        /* for sockaddr_in and inet_addr() */
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CFTPClient_sampleDlg 대화 상자

CFTPClient CFTPClient_sampleDlg::m_FtpClient;

CFTPClient_sampleDlg::CFTPClient_sampleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFTPClient_sampleDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFTPClient_sampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IPADDRESS, m_ipAddress);
	DDX_Control(pDX, IDC_USER, m_user);
	DDX_Control(pDX, IDC_PASSWORD, m_password);
	DDX_Control(pDX, IDC_CONNECT, m_connectBtn);
	DDX_Control(pDX, IDC_UPLOAD, m_uploadBtn);
	DDX_Control(pDX, IDC_DOWNLOAD, m_downloadBtn);
	DDX_Control(pDX, IDC_LOCAL_TREE, m_localTree);
	DDX_Control(pDX, IDC_REMOTE_TREE, m_remoteTree);
	DDX_Control(pDX, IDC_RE_PROTOCOL, m_ProtocolOutput);
}

BEGIN_MESSAGE_MAP(CFTPClient_sampleDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CONNECT, &CFTPClient_sampleDlg::OnConnect)
	ON_BN_CLICKED(IDC_UPLOAD, &CFTPClient_sampleDlg::OnBnClickedUpload)
	ON_BN_CLICKED(IDC_DOWNLOAD, &CFTPClient_sampleDlg::OnBnClickedDownload)
	ON_BN_CLICKED(IDC_STOP, &CFTPClient_sampleDlg::OnBnClickedStop)
	ON_NOTIFY(TVN_SELCHANGED, IDC_LOCAL_TREE, &CFTPClient_sampleDlg::OnTvnSelchangedLocalTree)
	ON_NOTIFY(TVN_SELCHANGED, IDC_REMOTE_TREE, &CFTPClient_sampleDlg::OnTvnSelchangedRemoteTree)
	ON_NOTIFY(NM_DBLCLK, IDC_LOCAL_TREE, &CFTPClient_sampleDlg::OnNMDblclkLocalTree)
	ON_NOTIFY(NM_DBLCLK, IDC_REMOTE_TREE, &CFTPClient_sampleDlg::OnNMDblclkRemoteTree)
END_MESSAGE_MAP()

// CFTPClient_sampleDlg 메시지 처리기

BOOL CFTPClient_sampleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

#ifdef _WIN32
    WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
    WSADATA       WsaData;            // receives data from WSAStartup

    // Load WinSock DLL
    if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {
        return FALSE;
    }
#endif

	m_ipAddress.SetWindowText("IP ADDRESS");
	m_user.SetWindowText("USERID");
	m_password.SetWindowText("PASSWROD");
	m_connect = FALSE;

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CFTPClient_sampleDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CFTPClient_sampleDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CFTPClient_sampleDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CFTPClient_sampleDlg::OnConnect()
{
	if (m_connect) {
		m_FtpClient.Logout();
		m_connect = FALSE;
		m_localTree.DeleteAllItems();
		m_remoteTree.DeleteAllItems();
		m_connectBtn.SetWindowText("CONNECT");
		return;
	} 

	CString ipAddr, user, password;
	m_ipAddress.GetWindowText(ipAddr);
	m_user.GetWindowText(user);
	m_password.GetWindowText(password);

	// 옵저버 설정
	m_FtpClient.AttachObserver(&m_ProtocolOutput);

	// 접속정보 설정
	CLogonInfo loginInfo(tstring(ipAddr.GetBuffer()), 21, 
		tstring(user.GetBuffer()),tstring(password.GetBuffer()), tstring(user.GetBuffer()));

	// 로그인
	if (!m_FtpClient.Login(loginInfo)) {
		AfxMessageBox("로그인하지 못하였습니다.");
		return;
	}
	m_connect = TRUE;
	m_connectBtn.SetWindowText("DISCONNECT");
	
	m_FtpClient.SetResumeMode();
	m_FtpClient.ChangeWorkingDirectory(tstring("."));

	// Remote File List
	TStringVector listVector;
	if (m_FtpClient.List(tstring("."), listVector, TRUE)) {
		m_remoteTree.DeleteAllItems();

		for (int i=0; i<listVector.size(); ++i) {
			m_remoteTree.InsertItem(listVector[i].c_str());
		}
	}

	// Local File List
	{
		m_localTree.DeleteAllItems();

		DWORD           dwRead ;
		HANDLE          hFile, hFind ;
		int             iSize ;
		WIN32_FIND_DATA finddata ;

		hFind = FindFirstFile ("*", &finddata) ;
		if (hFind == INVALID_HANDLE_VALUE)
			return;

		do
		{
			// Open the file and get the size
			hFile = CreateFile (finddata.cFileName, GENERIC_READ, FILE_SHARE_READ,
				NULL, OPEN_EXISTING, 0, NULL) ;

			if (hFile == INVALID_HANDLE_VALUE)
				continue ;

			iSize = GetFileSize (hFile, NULL) ;
			if (iSize == (DWORD) -1)
			{
				CloseHandle (hFile) ;
				continue ;
			}

			// Allocate space and save the filename 
			tstring strItem = finddata.cFileName;
			m_localTree.InsertItem(strItem.c_str());

			CloseHandle (hFile) ;
		}
		while (FindNextFile (hFind, &finddata)) ;

		FindClose (hFind) ;
	}
}

void CFTPClient_sampleDlg::OnBnClickedDownload()
{
	BeginWaitCursor( ); 
	m_downloadBtn.EnableWindow(FALSE);
	THREAD_PARAM_LIST* pThreadParamList = new THREAD_PARAM_LIST;

	THREAD_PARAM thread_param;
	thread_param.directive = "DOWNLOAD";
	thread_param.filename = "Python-2.1.1.tar.gz";

	pThreadParamList->Add(thread_param);

	if(pThreadParamList->GetSize() > 0)
		AfxBeginThread(ThreadCallFunc, pThreadParamList);
	else
		delete pThreadParamList;
/*
	if (m_FtpClient.IsConnected()) {
		m_uploadBtn.EnableWindow(FALSE);
		tstring filename = "Python-2.1.1.tar.gz";
		if (m_FtpClient.DownloadFile(filename, filename, CRepresentation(CType::Image()), TRUE))
		{
			AfxMessageBox("Upload Complete..");
		} else {
			AfxMessageBox("Upload Failed..");
		}
		m_uploadBtn.EnableWindow(TRUE);
	}*/
	m_downloadBtn.EnableWindow(TRUE);
	EndWaitCursor();
}

void CFTPClient_sampleDlg::OnBnClickedUpload()
{
	BeginWaitCursor( ); 
	m_uploadBtn.EnableWindow(FALSE);

	THREAD_PARAM_LIST* pThreadParamList = new THREAD_PARAM_LIST;

	THREAD_PARAM thread_param;
	thread_param.directive = "UPLOAD";
	thread_param.filename = "Python-2.1.1.tar.gz";

	pThreadParamList->Add(thread_param);

	if(pThreadParamList->GetSize() > 0)
		AfxBeginThread(ThreadCallFunc, pThreadParamList);
	else
		delete pThreadParamList;
/*
	if (m_FtpClient.IsConnected()) {

		m_downloadBtn.EnableWindow(FALSE);
		tstring filename = "Python-2.1.1.tar.gz";
		if (m_FtpClient.UploadFile(filename, filename, FALSE, CRepresentation(CType::Image()), TRUE))
		{
			AfxMessageBox("Download Complete..");
		} else {
			AfxMessageBox("Download Failed..");
		}
		m_downloadBtn.EnableWindow(TRUE);
	}*/
	m_uploadBtn.EnableWindow(TRUE);
	EndWaitCursor();
}

void CFTPClient_sampleDlg::OnBnClickedStop()
{
	if (m_FtpClient.IsConnected()) {
		m_FtpClient.Abort();
	}
}

void CFTPClient_sampleDlg::OnTvnSelchangedLocalTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

void CFTPClient_sampleDlg::OnTvnSelchangedRemoteTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

void CFTPClient_sampleDlg::OnNMDblclkLocalTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

void CFTPClient_sampleDlg::OnNMDblclkRemoteTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

UINT CFTPClient_sampleDlg::ThreadCallFunc(LPVOID pParam)
{
	THREAD_PARAM_LIST *pThreadParamList = (THREAD_PARAM_LIST*)pParam;

	int size = pThreadParamList->GetSize();

	if (m_FtpClient.IsConnected()) {

		for(int i=0; i<size; i++)
		{
			THREAD_PARAM& thread_param = pThreadParamList->GetAt(i);
			BOOL result = false;

			// 프로세스 작업

			if (thread_param.directive == "UPLOAD") {				
				if (m_FtpClient.UploadFile(thread_param.filename, thread_param.filename, 
					FALSE, CRepresentation(CType::Image()), TRUE))
				{
					AfxMessageBox("Upload Complete..");
				} else {
					AfxMessageBox("Upload Failed..");
				}
			} else if (thread_param.directive == "DOWNLOAD") {
				tstring filename = "Python-2.1.1.tar.gz";
				if (m_FtpClient.DownloadFile(thread_param.filename, thread_param.filename, 
					CRepresentation(CType::Image()), TRUE))
				{
					AfxMessageBox("Download Complete..");
				} else {
					AfxMessageBox("Download Failed..");
				}
			}
		}
	}
	delete pThreadParamList;
	return 0;
}
