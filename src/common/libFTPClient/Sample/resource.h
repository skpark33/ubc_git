//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FTPClient_sample.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FTPCLIENT_SAMPLE_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDC_IPADDRESS                   1000
#define IDC_USER                        1001
#define IDC_PASSWORD                    1002
#define IDC_CONNECT                     1003
#define IDC_LOCAL_TREE                  1005
#define IDC_REMOTE_TREE                 1006
#define IDC_UPLOAD                      1007
#define IDC_DOWNLOAD                    1008
#define IDC_STOP                        1009
#define IDC_RICHEDIT21                  1012
#define IDC_RE_PROTOCOL                 1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
