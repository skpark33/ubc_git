// ProjectorControlDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "ProjectorControl.h"
#include "ProjectorControlDlg.h"

#include "../ProjectorControl.h"

#include <ci/libDebug/ciDebug.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ciSET_DEBUG(1, "ProjectorControl");


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CProjectorControlDlg 대화 상자




CProjectorControlDlg::CProjectorControlDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProjectorControlDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CProjectorControlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_PROJECTOR_TYPE, m_cbxProjectorType);
	DDX_Control(pDX, IDC_COMBO_COMPORT, m_cbxComPort);
	DDX_Control(pDX, IDC_COMBO_PID, m_cbxPID);
	DDX_Control(pDX, IDC_BUTTON_POWER_ON, m_btnPowerOn);
	DDX_Control(pDX, IDC_BUTTON_POWER_OFF, m_btnPowerOff);
	DDX_Control(pDX, IDC_BUTTON_POWER_STATUS, m_btnPowerStatus);
	DDX_Control(pDX, IDC_BUTTON_LAMP_HOUR, m_btnLampHour);
	DDX_Control(pDX, IDOK, m_btnClose);
	DDX_Control(pDX, IDC_LIST, m_lc);
	DDX_Control(pDX, IDC_STATIC_REMAIN, m_stcRemain);
	DDX_Control(pDX, IDC_EDIT_INTERVAL, m_editInterval);
	DDX_Control(pDX, IDC_BUTTON_PS_START, m_btnStart);
	DDX_Control(pDX, IDC_BUTTON_PS_END, m_btnEnd);
}

BEGIN_MESSAGE_MAP(CProjectorControlDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_POWER_ON, &CProjectorControlDlg::OnBnClickedButtonPowerOn)
	ON_BN_CLICKED(IDC_BUTTON_POWER_OFF, &CProjectorControlDlg::OnBnClickedButtonPowerOff)
	ON_BN_CLICKED(IDC_BUTTON_POWER_STATUS, &CProjectorControlDlg::OnBnClickedButtonPowerStatus)
	ON_BN_CLICKED(IDC_BUTTON_LAMP_HOUR, &CProjectorControlDlg::OnBnClickedButtonLampHour)
	ON_BN_CLICKED(IDC_BUTTON_PS_START, &CProjectorControlDlg::OnBnClickedButtonPsStart)
	ON_BN_CLICKED(IDC_BUTTON_PS_END, &CProjectorControlDlg::OnBnClickedButtonPsEnd)
	ON_CBN_SELCHANGE(IDC_COMBO_PROJECTOR_TYPE, &CProjectorControlDlg::OnCbnSelchangeComboProjectorType)
END_MESSAGE_MAP()


// CProjectorControlDlg 메시지 처리기

BOOL CProjectorControlDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	for(int i=ePrjType_start; i<ePrjType_end; i++)
	{
		CString prj_name = CProjectorControl::ToString((ePrjType)i);
		prj_name.Replace("ePrjType_", "");
		m_cbxProjectorType.AddString(prj_name);
	}
	m_cbxProjectorType.SetCurSel(0);

	m_cbxComPort.AddString("COM1");
	m_cbxComPort.AddString("COM2");
	m_cbxComPort.AddString("COM3");
	m_cbxComPort.AddString("COM4");
	m_cbxComPort.SetCurSel(0);

	m_cbxPID.AddString("All(or 0)");
	m_cbxPID.AddString("1");
	m_cbxPID.AddString("2");
	m_cbxPID.AddString("3");
	m_cbxPID.AddString("4");
	m_cbxPID.AddString("5");
	m_cbxPID.AddString("6");
	m_cbxPID.AddString("7");
	m_cbxPID.AddString("8");
	m_cbxPID.AddString("9");
	m_cbxPID.SetCurSel(1);

	m_lc.InsertColumn(0, "Time", 0, 210);
	m_lc.InsertColumn(1, "PowerStatus Result", 0, 370);

	m_editInterval.SetWindowText("60");

	m_btnEnd.EnableWindow(FALSE);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CProjectorControlDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CProjectorControlDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CProjectorControlDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CProjectorControlDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == 1025)
	{
		CString com_port;
		m_cbxComPort.GetLBText(m_cbxComPort.GetCurSel(), com_port);

		KillTimer(1025);

		m_nRemainInterval--;

		CString str;
		str.Format("%d sec left...", m_nRemainInterval);
		m_stcRemain.SetWindowText(str);

		if(m_nRemainInterval == 0)
		{
			m_nRemainInterval = m_nInterval;

			ePrjRetVal ret_val = ePrjRetVal_UNKNOWN;
			ePrjStatus status = ePrjStatus_UNKNOWN;

			ePrjType cur_sel_prj_type = (ePrjType)(m_cbxProjectorType.GetCurSel() + 1);

			if(ePrjType_start <= cur_sel_prj_type && cur_sel_prj_type < ePrjType_end)
			{
				ciDEBUG(1, ("PowerStatus(%s)", CProjectorControl::ToString(cur_sel_prj_type)) );
				CProjectorControl pc;
				pc.Init(cur_sel_prj_type, com_port);
				pc.SetProjectorID(GetProjectorID());
				ret_val = pc.PowerStatus(status);
			}

			ciDEBUG(1, ("PowerStatus_Result(%s,%s)", CProjectorControl::ToString(ret_val), CProjectorControl::ToString(status)));

			CString result = "";
			if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
				result = CProjectorControl::ToString(status);
			else
				result = CProjectorControl::ToString(ret_val);

			CTime cur_tm = CTime::GetCurrentTime();
			m_lc.InsertItem(0, cur_tm.Format("%Y/%m/%d %H:%M:%S"));
			m_lc.SetItemText(0, 1, result);
		}

		SetTimer(1025, 1000, NULL);
	}
}

void CProjectorControlDlg::OnCbnSelchangeComboProjectorType()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int index = m_cbxProjectorType.GetCurSel();

	if(index == 0 || index == 2)
	{
		m_cbxPID.EnableWindow(FALSE);
	}
	else
	{
		m_cbxPID.EnableWindow(TRUE);
	}
}

void CProjectorControlDlg::OnBnClickedButtonPowerOn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString com_port;
	m_cbxComPort.GetLBText(m_cbxComPort.GetCurSel(), com_port);

	ePrjRetVal ret_val = ePrjRetVal_UNKNOWN;

	ePrjType cur_sel_prj_type = (ePrjType)(m_cbxProjectorType.GetCurSel() + 1);

	if(ePrjType_start <= cur_sel_prj_type && cur_sel_prj_type < ePrjType_end)
	{
		ciDEBUG(1, ("PowerOn(%s)", CProjectorControl::ToString(cur_sel_prj_type)) );
		CProjectorControl pc;
		pc.Init(cur_sel_prj_type, com_port);
		pc.SetProjectorID(GetProjectorID());
		ret_val = pc.PowerOn();
	}

	ciDEBUG(1, ("PowerOn_Result(%s)", CProjectorControl::ToString(ret_val)) );

	AfxMessageBox(CProjectorControl::ToString(ret_val));
}

void CProjectorControlDlg::OnBnClickedButtonPowerOff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString com_port;
	m_cbxComPort.GetLBText(m_cbxComPort.GetCurSel(), com_port);

	ePrjRetVal ret_val = ePrjRetVal_UNKNOWN;

	ePrjType cur_sel_prj_type = (ePrjType)(m_cbxProjectorType.GetCurSel() + 1);

	if(ePrjType_start <= cur_sel_prj_type && cur_sel_prj_type < ePrjType_end)
	{
		ciDEBUG(1, ("PowerOff(%s)", CProjectorControl::ToString(cur_sel_prj_type)) );
		CProjectorControl pc;
		pc.Init(cur_sel_prj_type, com_port);
		pc.SetProjectorID(GetProjectorID());
		ret_val = pc.PowerOff();
	}

	ciDEBUG(1, ("PowerOff_Result(%s)", CProjectorControl::ToString(ret_val)) );

	AfxMessageBox(CProjectorControl::ToString(ret_val));
}

void CProjectorControlDlg::OnBnClickedButtonPowerStatus()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString com_port;
	m_cbxComPort.GetLBText(m_cbxComPort.GetCurSel(), com_port);

	ePrjRetVal ret_val = ePrjRetVal_UNKNOWN;
	ePrjStatus status = ePrjStatus_UNKNOWN;

	ePrjType cur_sel_prj_type = (ePrjType)(m_cbxProjectorType.GetCurSel() + 1);

	if(ePrjType_start <= cur_sel_prj_type && cur_sel_prj_type < ePrjType_end)
	{
		ciDEBUG(1, ("PowerStatus(%s)", CProjectorControl::ToString(cur_sel_prj_type)) );
		CProjectorControl pc;
		pc.Init(cur_sel_prj_type, com_port);
		pc.SetProjectorID(GetProjectorID());
		ret_val = pc.PowerStatus(status);
	}

	ciDEBUG(1, ("PowerStatus_Result(%s,%s)", CProjectorControl::ToString(ret_val), CProjectorControl::ToString(status)) );

	CString result = "";
	if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
		result = CProjectorControl::ToString(status);
	else
		result = CProjectorControl::ToString(ret_val);
	AfxMessageBox(result);
}

void CProjectorControlDlg::OnBnClickedButtonLampHour()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString com_port;
	m_cbxComPort.GetLBText(m_cbxComPort.GetCurSel(), com_port);

	ePrjRetVal ret_val = ePrjRetVal_UNKNOWN;
	int hour = -1;

	ePrjType cur_sel_prj_type = (ePrjType)(m_cbxProjectorType.GetCurSel() + 1);

	if(ePrjType_start <= cur_sel_prj_type && cur_sel_prj_type < ePrjType_end)
	{
		ciDEBUG(1, ("LampHour(%s)", CProjectorControl::ToString(cur_sel_prj_type)) );
		CProjectorControl pc;
		pc.Init(cur_sel_prj_type, com_port);
		pc.SetProjectorID(GetProjectorID());
		ret_val = pc.GetLampHour(hour);
	}

	ciDEBUG(1, ("LampHour_Result(%s,%dh)", CProjectorControl::ToString(ret_val), hour));

	if(ret_val == ePrjRetVal_SUCCESS_TO_RUN_COMMAND)
	{
		CString msg;
		msg.Format("Lamp Hour [ %d h ]", hour);
		AfxMessageBox(msg);
	}
	else
	{
		AfxMessageBox(CProjectorControl::ToString(ret_val));
	}
}

void CProjectorControlDlg::OnBnClickedButtonPsStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_cbxProjectorType.EnableWindow(FALSE);
	m_btnPowerOn.EnableWindow(FALSE);
	m_btnPowerOff.EnableWindow(FALSE);
	m_btnPowerStatus.EnableWindow(FALSE);
	m_btnLampHour.EnableWindow(FALSE);

	m_editInterval.EnableWindow(FALSE);
	m_btnStart.EnableWindow(FALSE);

	m_btnEnd.EnableWindow(TRUE);

	CString str;
	m_editInterval.GetWindowText(str);
	m_nInterval = atoi(str);
	if(m_nInterval == 0) m_nInterval = 60;
	m_nRemainInterval = m_nInterval;

	SetTimer(1025, 1000, NULL);
}

void CProjectorControlDlg::OnBnClickedButtonPsEnd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	KillTimer(1025);

	m_cbxProjectorType.EnableWindow(TRUE);
	m_btnPowerOn.EnableWindow(TRUE);
	m_btnPowerOff.EnableWindow(TRUE);
	m_btnPowerStatus.EnableWindow(TRUE);
	m_btnLampHour.EnableWindow(TRUE);

	m_stcRemain.SetWindowText("");

	m_editInterval.EnableWindow(TRUE);
	m_btnStart.EnableWindow(TRUE);

	m_btnEnd.EnableWindow(FALSE);
}
