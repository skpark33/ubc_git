/*! \class ProjectorThread
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _ProjectorThread_h_
#define _ProjectorThread_h_

#include <ci/libThread/ciThread.h> 

class ProjectorThread :  public  ciThread {
public:

	enum { OFF=0, ON, STATE };

	static ProjectorThread*	getInstance();
	static void	clearInstance();
	virtual ~ProjectorThread() ;
	virtual void run();

	ciBoolean isRun();
	void		setAction(int p) { _action = p; }
	void		setRetry(int p) { _retry = p; }
	void		setType(int p) { _ptype = p; }

protected:
	ProjectorThread();

	static ProjectorThread*	_instance;
	static ciMutex		_instanceLock;

	ciMutex _lock;
	ciBoolean _isRun;

	int _action ;//
	int _retry;
	int _ptype; // projector type

};	

#endif //_ProjectorThread_h_
