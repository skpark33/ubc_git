#pragma once

#include <list>
#include <windows.h>
#include "serialImpl.h"
#include <ci/libThread/ciSyncUtil.h>
#include <ci/libBase/ciTime.h>


///////////////////////////////////
typedef enum
{
	ePrjType_UNKNOWN = 0,
	ePrjType_start = 1,				// DO NOT USE. Just start...
	ePrjType_SHINDORICO = 1,		// 신도리코
	ePrjType_LG,					// LG
	ePrjType_CANNON,				// Cannon
	ePrjType_LG_NEWMODEL,				// LG New Model (BX286)
	ePrjType_NEC,					// NEC
	ePrjType_SAMSUNG,				// Samsung
	ePrjType_EPSON,					// Epson EB-1880

	ePrjType_end,					// DO NOT USE. Just count...
}
ePrjType;


///////////////////////////////////
typedef enum
{
	ePrjRetVal_UNKNOWN = 0,				// 알수없는 에러
	ePrjRetVal_start=1,					// DO NOT USE. Just start...
	ePrjRetVal_SUCCESS_TO_RUN_COMMAND=1,// 명령 성공
	ePrjRetVal_FAIL_TO_INIT,			// 초기화 실패
	ePrjRetVal_FAIL_TO_SEND,			// 명령전송 실패
	ePrjRetVal_FAIL_TO_RECEIVE,			// 명령수신 실패
	ePrjRetVal_FAIL_TO_RUN_COMMAND,		// 명령실행 에러
	ePrjRetVal_INVALID_RETURN_VALUE,	// 알수없는 리턴값
	ePrjRetVal_NOT_SUPPORT,				// 지원 안함

	ePrjRetVal_end,						// DO NOT USE. Just count...
}
ePrjRetVal;


///////////////////////////////////
typedef enum
{
	ePrjStatus_UNKNOWN = 0,
	ePrjStatus_start=1,			// DO NOT USE. Just start...
	ePrjStatus_POWER_ON=1,		// ON
	ePrjStatus_POWER_OFF,		// OFF

	ePrjStatus_end,				// DO NOT USE. Just count...
}
ePrjStatus;


///////////////////////////////////
typedef struct
{
	ePrjType		type;
	ePrjRetVal		ret_value;
} ePrjRetValPair;

typedef std::list<ePrjRetValPair>	ePrjRetValList;

//////////////////////////////////////
typedef	list<serialImpl*>			serialImplList;
typedef	serialImplList::iterator	serialImplListIter;

///////////////////////////////////
class CSerialValue
{
public:
	CSerialValue() { ::ZeroMemory(m_value, sizeof(m_value)); m_size=0; };
	CSerialValue(const BYTE lpData[], const int size=-1) { Set(lpData, size); };
	CSerialValue(const CSerialValue& _val) { *this = _val; };
	~CSerialValue() {};

	BYTE m_value[1024];
	int  m_size;

	CSerialValue& operator=(const CSerialValue& _val)
	{
		Set(_val.m_value, _val.m_size);
		return *this;
	};

	void Set(const BYTE lpData[], const int size=-1)
	{
		::ZeroMemory(m_value, sizeof(m_value));
		m_size = (size<0) ? strlen((char*)lpData) : size;
		if(lpData != NULL) ::memcpy(m_value, lpData, m_size);
	};
};
typedef	list<CSerialValue>			CSerialValueList;
typedef	CSerialValueList::iterator	CSerialValueIter;

///////////////////////////////////
class CProjectorControl
{
public:
	static bool		AllPowerOn(ePrjRetValList* rv_list=NULL);		// 하나라도 성공하면 true리턴. rv_list에 각 타입의 결과값 출력
	static bool		AllPowerOff(ePrjRetValList* rv_list=NULL);		// 하나라도 성공하면 true리턴. rv_list에 각 타입의 결과값 출력
	static bool		AllPowerStatus(ePrjRetValList* rv_list=NULL);	// 하나라도 파워가 켜져있으면 true리턴. rv_list에 각 타입의 결과값 출력
	static ePrjType	SmartPowerOn();
	static ePrjType	SmartPowerOff();

	CProjectorControl();
	virtual ~CProjectorControl();

	static const char*	ToString(ePrjType ePT);
	static const char*	ToString(ePrjRetVal ePT);
	static const char*	ToString(ePrjStatus ePT);

protected:
	ePrjType		m_ePrjType;

	ciStringList	m_serialPortList;
	int				m_nBaudRate;
	int				m_nDataBit;
	int				m_nParityBit;
	int				m_nStopBit;
	char			m_chEOF;
	int				m_nTimeout;

	char			m_szProjectorID[16];
	int 			m_nProjectorID;

	serialImplList	m_serialList;

	ciString		GetBinaryString(LPBYTE szBin, int size=-1);
	bool			IsIn(CSerialValue* sVal, CSerialValueList& sValList);
	ePrjRetVal		ExecuteSerialCommand(CSerialValue& cmd, CSerialValue& outVal);

public:

	void		Init(ePrjType ePT,
					 LPCSTR szDevice= NULL, 
					 int nBaudRate	= CSerial::EBaudUnknown, 
					 int nDataBit	= CSerial::EDataUnknown, 
					 int nParityBit	= CSerial::EParUnknown, 
					 int nStopBit	= CSerial::EStopUnknown );
	void		Close();

	void		SetProjectorID(int id);

	ePrjRetVal	PowerOn();
	ePrjRetVal	PowerOff();
	ePrjRetVal	PowerStatus(ePrjStatus& status);
	ePrjRetVal	GetLampHour(int& hour);	// 램프사용시간. 실패시 -1 리턴.
};

class ProjectorWrapper
{
public:
	static ProjectorWrapper*	getInstance();
	static void	clearInstance();

	virtual ~ProjectorWrapper() ;

	ciBoolean	projectorOn();
	ciBoolean	projectorOff();
	int			getState();
	int			getLampHour();

	int			getLastState();
	time_t		getLastStateTime();
	void		setLastState(int state);

	ciBoolean	projectorOn(int eType);
	ciBoolean	projectorOff(int eType);
	int			getState(int eType);
	int			getLampHour(int eType);
	ciBoolean	getProjectorType();

protected:
	ProjectorWrapper();
	static ProjectorWrapper*	_instance;
	static ciMutex				_instanceLock;

	ciMutex		_lock;
	int			_pType;

	ciMutex		_stateLock;
	ciTime		_lastStateTime;
	int			_lastState;
protected:
};


