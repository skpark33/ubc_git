//	Listener.cpp - Sample application for CSerial
//
//	Copyright (C) 1999-2003 Ramon de Klein (Ramon.de.Klein@ict.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#include "SerialListener.h"

#define MAX_LINE_LENGTH	256

bool 
CSerialListener::open(const char* device, int baudrate, int dataBit, int parityBit, int stopBit, unsigned long eof )
{
   _errCode = ERROR_SUCCESS;
   _errMsg = "";
	
   _eof = eof;

	// Attempt to open the _serial port (COM1)
    _errCode = _serial.Open(_T(device),0,0,false);
	if (_errCode != ERROR_SUCCESS) {
		_errMsg = "Unable to open COM-port";
		printf("code=%d,msg=%s\n",_errCode,_errMsg.c_str());
		return false;
	}

    // Setup the _serial port (9600,8N1, which is the default setting)
    _errCode = _serial.Setup(	(CSerial::EBaudrate)baudrate,
								(CSerial::EDataBits)dataBit,
								(CSerial::EParity)parityBit,
								(CSerial::EStopBits)stopBit		);

	if (_errCode != ERROR_SUCCESS) {
		_errMsg = "Unable to set COM-port setting";
		printf("code=%d,msg=%s\n",_errCode,_errMsg.c_str());
		return false;
	}

    // Register only for the receive event
    _errCode = _serial.SetMask(CSerial::EEventBreak |
								CSerial::EEventCTS   |
								CSerial::EEventDSR   |
								CSerial::EEventError |
								CSerial::EEventRing  |
								CSerial::EEventRLSD  |
								CSerial::EEventRecv);
	
	if (_errCode != ERROR_SUCCESS) {
		_errMsg = "Unable to set COM-port event mask";
		printf("code=%d,msg=%s\n",_errCode,_errMsg.c_str());
		return false;
	}

	
	// Use 'non-blocking' reads, because we don't know how many bytes
	// will be received. This is normally the most convenient mode
	// (and also the default mode for reading data).
    _errCode = _serial.SetupReadTimeouts(CSerial::EReadTimeoutNonblocking);
	if (_errCode != ERROR_SUCCESS) {
		_errMsg = "Unable to set COM-port read timeout.";
		printf("code=%d,msg=%s\n",_errCode,_errMsg.c_str());
		return false;
	}
	

	printf("Serial Port Ready\n");
	return true;
}

LONG
CSerialListener::close()
{
	// Close the port again
	_readWait=false;
	_eventWait=false;
	printf("binSerialListener close\n");
    return _serial.Close();
}

bool 
CSerialListener::svc(const char* msg)
{
	printf("%s", msg);
	printf("(");

	int len = strlen(msg);
	for(int i=0;i<len;i++){
		printf(",0x%02x", msg[i]);
	}
	printf(")\n");
	return true;
}

bool 
CSerialListener::listen()
{
   _errCode = ERROR_SUCCESS;
   _errMsg = "";

   // Keep reading data, until an EOF (CTRL-Z) has been received
	_eventWait = true;

	while (_eventWait)	{
		printf("wait for event...\n");
		// Wait for an event
		
		_errCode = _serial.WaitEvent();
		if (_errCode != ERROR_SUCCESS) {
			_errMsg = "Unable to wait for a COM-port event.";
			printf("code=%d,msg=%s\n",_errCode,_errMsg.c_str());
			return false;
		}

		// Save event
		const CSerial::EEvent eEvent = _serial.GetEventType();

		// Handle break event
		if (eEvent & CSerial::EEventBreak)		{
			printf("\n### BREAK received ###\n");
		}
		// Handle CTS event
		if (eEvent & CSerial::EEventCTS)		{
			printf("\n### Clear to send %s ###\n", _serial.GetCTS()?"on":"off");
		}
		// Handle DSR event
		if (eEvent & CSerial::EEventDSR)		{
			printf("\n### Data set ready %s ###\n", _serial.GetDSR()?"on":"off");
		}

		// Handle error event
		if (eEvent & CSerial::EEventError)		{
			printf("\n### ERROR: ");
			switch (_serial.GetError())	{
				case CSerial::EErrorBreak:		printf("Break condition");			break;
				case CSerial::EErrorFrame:		printf("Framing error");			break;
				case CSerial::EErrorIOE:		printf("IO device error");			break;
				case CSerial::EErrorMode:		printf("Unsupported mode");			break;
				case CSerial::EErrorOverrun:	printf("Buffer overrun");			break;
				case CSerial::EErrorRxOver:		printf("Input buffer overflow");	break;
				case CSerial::EErrorParity:		printf("Input parity error");		break;
				case CSerial::EErrorTxFull:		printf("Output buffer full");		break;
				default:						printf("Unknown");					break;
			}
			printf(" ###\n");
		}

		// Handle ring event
		if (eEvent & CSerial::EEventRing)	{
			printf("\n### RING ###\n");
		}

		// Handle RLSD/CD event
		if (eEvent & CSerial::EEventRLSD)	{
			printf("\n### RLSD/CD %s ###\n", _serial.GetRLSD()?"on":"off");
		}

		// Handle data receive event
		if (eEvent & CSerial::EEventRecv) {
			// Read data, until there is nothing left
			DWORD dwBytesRead = 0;
			char szBuffer[MAX_LINE_LENGTH] = {0};
			int waitCounter=0;
			_readWait = true;
			while(_readWait) {
				// Read data from the COM-port
				_errCode = _serial.Read(szBuffer,sizeof(szBuffer)-1,&dwBytesRead);
				if (_errCode != ERROR_SUCCESS) {
					_errMsg = "Unable to read from COM-port.";
					printf("code=%d,msg=%s\n",_errCode,_errMsg.c_str());
					return false;
				}

				if (dwBytesRead > 0) {
					waitCounter=0;
					// Finalize the data, so it is a valid string
					szBuffer[dwBytesRead] = '\0';
					// Display the data
					svc((const char*)szBuffer);
					// Check if EOF (CTRL+'[') has been specified
					if (strchr(szBuffer,_eof)){
						_eventWait = false;
						break;
					}
					
				}else{
					//printf(".");
					if(waitCounter>30){
						_readWait = false;
					}
					waitCounter++;
					::Sleep(100);
				}
			} //while
		} // if
	} //while
	
	printf("\n");	
	return true;
}

