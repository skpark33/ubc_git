 /*! \file CBinSerialListener.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author skpark
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _binSerialListener_h_
#define _binSerialListener_h_

#include <crtdbg.h>
#include <tchar.h>
#include <windows.h>
#include "Serial.h"

#include <string>
using namespace std;

class CBinSerialListener {
public:
	CBinSerialListener() { _errCode = ERROR_SUCCESS; _readWait=true;_eventWait=true;}
	virtual ~CBinSerialListener() { close(); }
 	
	virtual bool open(	const char* device="COM1", 
							int baudrate=CSerial::EBaud9600, 
							int dataBit=CSerial::EData8, 
							int parityBit=CSerial::EParNone, 
							int stopBit=CSerial::EStop1,
							unsigned long eof=27 ); // Ctrl+'[');
	virtual long  close();
	virtual bool listen();

	const char*		getErrMsg()		{ return _errMsg.c_str(); }
	long			getErrCode()	{ return _errCode; }

protected:	

	virtual bool	svc(const char* msg, int size=-1);

	CSerial				_serial;
	long				_errCode;
	string				_errMsg;
	unsigned long		_eof;
	boolean				_readWait;
	boolean				_eventWait;
};
		
#endif //_binSerialListener_h_
