#ifndef _ubcSite_h_
#define _ubcSite_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>



class ubcSite {
public:
	ubcSite(const char* site);
	virtual ~ubcSite() ;

	int getUserList(ciStringList& userList);
	int getHostList(ciStringList& hostList);
	int getProgramList(ciStringList& programList);

protected:
	ciString _siteId;
};

#endif // _ubcSite_h_
