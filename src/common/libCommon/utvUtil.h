#ifndef _utvUtil_h_
#define _utvUtil_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>

class utvUtil {
public:
	static utvUtil*	getInstance();
	static void	clearInstance();

	virtual ~utvUtil() ;

	void	todayNow(const char* pTime, cciTime& outVal);
	
	ciBoolean isBetweenDate(cciTime& start, cciTime& end);
	ciBoolean isBetweenDate(const char* pDate1, const char* pDate2);
	ciBoolean isBetweenTime(const char* pTime1, const char* pTime2);
	ciBoolean isBetween(const char* pTime1, const char* pTime2);
	ciBoolean isOnTime(const char* pTime1);
	ciBoolean isBetween(ciTime& pnow, const char* pTime1, const char* pTime2);
	ciBoolean isBetweenEndExclusive(ciTime& pnow, const char* pTime1, const char* pTime2);
	ciBoolean isBetweenEndExclusive(ciLong gmt, ciTime& pnow, const char* pTime1, const char* pTime2);
	
	ciBoolean powerOn(const char* mac, ciLong port);
	ciBoolean amtCmd(const char* ip, const char* amtPassword, const char* amtCommand);

	ciLong genId(const char* field);

	int	getRequestId(const char* ipAddress, ciStringList& outValList);
	ciBoolean		setState(const char*	tableName,
							  const char*	keyField,
							  const char*	keyValue,
							  const char*	stateField,			
							  ciBoolean		stateValue);

	ciLong	genTemplateId(const char* siteId);
	ciLong  genFrameId(const char* siteId, ciLong templateId);

	const char*	getHostId();
	const char* getSiteId();


	ciBoolean	defaultEnv();

	ciBoolean	renameFile(const char* from, const char* to, const char* dir);

#ifdef _COP_MSC_
	void		setEnv(const char* regItemName, const char* value);
	ciBoolean socketAgent(const char* ipAddress,
				 int portNo, 
				 const char* directive, 
				 const char* data);

#endif
	ciBoolean monitorState(ciBoolean monitorOff, ciStringList& monitorOffList);
	ciBoolean onOffProcess(ciStringList& monitorOffList);


protected:
	utvUtil();

	ciMutex	_genIdLock;

	static utvUtil*	_instance;
	static ciMutex			_instanceLock;

	ciString _hostId;
	ciString _siteId;
};

#endif // _utvUtil_h_
