#ifndef _ubcPermition_h_
#define _ubcPermition_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>
#include <common/libCommon/ubcFilter.h>

class ubcPermition {
public:
	static ubcPermition*	getInstance();
	static void	clearInstance();

	virtual ~ubcPermition() ;

	void init(const char* userId, ciLong userType, const char* siteList, const char* hostList);
	ubcFilter&	getFilter() { return _filter;}

	ciBoolean	getSiteSet(ciStringSet& outSet);
	ciBoolean	getHostSet(ciStringSet& outSet);

protected:
	ubcPermition();

	ciString _userId;
	ciLong	 _userType;
	ciString _siteList;
	ciString _hostList;

	ubcFilter	_filter;

	static ubcPermition*	_instance;
	static ciMutex			_instanceLock;

};

#endif // _utvUtil_h_
