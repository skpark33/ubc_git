#ifndef _ubcCenter_h_
#define _ubcCenter_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>

class KeyEle {
public:
	ciString hostId;
	ciString macAddress;
	ciString edition;
	ciString description;
	ciString enterpriseKey;
	ciTime lastUpdateTime;
	ciTime authDate;
	ciString licenseType;
	ciString plugInType;
	ciBoolean	serverIpChanged;
	ciBoolean	changeServerIp;
	ciString	ipAddress;
};

typedef map<string, KeyEle>  KEYMAP; 

class ubcCenter {
public:
	ubcCenter(const char* site);
	virtual ~ubcCenter() ;

	static const char*	corbaOption();

	int			listKey(ciBoolean range, ciBoolean listIp=ciFalse);
	int			createKey(const char* edition,const char* enterpriseKey, const char* description, int count=1, int startNo=0);
	int			createKey(const char* key,const char* enterpriseKey, const char* edition, const char* description);
	int			setKey(const char* key,const char* enterpriseKey, const char* edition, const char* description);
	int			deleteKey(const char* key);
	int			resetKey(const char* key,const char* edition, 
						const char* macAddress,const char* ipAddress,
						const char* description, int changeServerIp,const char* licenseType);


	const char*		getEdition(const char* key);

	int			createSite(const char* description);
	int			deleteSite();
	int			listSite(ciBoolean sort=ciFalse);

	int			getMaxKey(int startNo);
	int			getKey(const char* hostId="*");

	void		clear();
	void		printIt(ciBoolean range, ciBoolean listIp=ciFalse);

protected:
	KEYMAP		_keymap;
	ciString	_site;
};

#endif // _ubcCenter_h_
