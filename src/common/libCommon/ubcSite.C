#include <ci/libDebug/ciArgParser.h>
#include <cci/libValue/cciTime.h>
#include <cci/libValue/cciInteger64.h>
#include <cci/libValue/cciBoolean.h>
#include <cci/libValue/cciAny.h>
#include <cci/libObject/cciObject.h>
#include <cci/libWrapper/cciCall.h>
#include <stdlib.h>
#include "ubcUtil.h"
#include "ubcSite.h"

ciSET_DEBUG(10, "ubcSite");


ubcSite::ubcSite(const char* siteId) 
{
    ciDEBUG(7, ("ubcSite(%s)",siteId));
	_siteId = siteId;
}
ubcSite::~ubcSite() 
{
    ciDEBUG(7, ("~ubcSite()"));
}


int	
ubcSite::getUserList(ciStringList& outList)
{
	ciDEBUG(7, ("getUserList()"));

	char entity[256];
	sprintf(entity,"PM=*/Site=%s/User=*", _siteId.c_str());
	return ubcUtil::getInstance()->getChildren((const char*)entity,"userId",outList);
}

int	
ubcSite::getHostList(ciStringList& outList)
{
	ciDEBUG(7, ("getHostList()"));

	char entity[256];
	sprintf(entity,"PM=*/Site=%s/Host=*", _siteId.c_str());
	return ubcUtil::getInstance()->getChildren((const char*)entity,"hostId",outList);
}

int	
ubcSite::getProgramList(ciStringList& outList)
{
	ciDEBUG(7, ("getProgramList()"));

	char entity[256];
	sprintf(entity,"PM=*/Site=%s/Program=*", _siteId.c_str());
	return ubcUtil::getInstance()->getChildren((const char*)entity,"programId",outList);
}