#ifndef _ubcMonitor_h_
#define _ubcMonitor_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>

class ubcMonitor { 
// ubcHostInfo에서는 가져오지 않는 모든 정보를 다룬다.
public:

	class ubcMonitorData {
	public :
		ubcMonitorData() {monitorType=0;	weekShutdownTime = "NONE";  }

		ciString	monitorId;
		ciLong		monitorType;				// [ROOT] HostType
		ciString	monitorName;
		ciString	description;
		ciString	startupTime;
		ciString	shutdownTime;				// [SHUTDOWNTIME]	value
		ciString	holiday;				// [SHUTDOWNTIME]	value   // ["03:00","0","6"] --> 03:00^0^6
		ciString	weekShutdownTime;
		//ciShort		monitorState;

		//ciBoolean	alreadyDone;
	};
	typedef  list<ubcMonitorData*>	UBCMonitorList;
	typedef  map<string, bool>	AlreadyDoneMap;
	typedef  map<string, ciShort>	StateMap;


	virtual ~ubcMonitor();
	ubcMonitor(const char* pSiteId, const char* pHostId);
	
	ciBoolean get();
	ciBoolean writeVariables(const char* pmonitorId=0);
	void clear();
	ciBoolean	doMonitor();

	ciBoolean	power(ubcMonitorData* data, ciBoolean onOff) ;
	ciBoolean	getPowerState(ubcMonitorData* data, int& state) ;

	ciBoolean	sendState(ubcMonitorData* data, ciShort state);

	ciBoolean isAlreadyDone(ubcMonitorData* data);
	void	  setAlreadyDone(ubcMonitorData* data, ciBoolean done);

	ciShort		getState(ubcMonitorData* data);
	void		setState(ubcMonitorData* data, ciShort state);

protected:
	ciBoolean _getSucceed;

public:
	ciString	mgrId;
	ciString	siteId;
	ciString	hostId;
	ciString	monitorEntry;


	UBCMonitorList monitorList;

	static ciString	yesterday;
	static AlreadyDoneMap alreadyDoneMap;
	static StateMap	stateMap;
	
};
#endif // _ubcMonitor_h_
