#ifndef _ubcHost_h_
#define _ubcHost_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>

class SSEle {
public:
	void toString(ciString& outValue);

	ciString	hostId;
	ciString	siteId;
	ciBoolean	adminState;
	ciBoolean	operationalState;
	ciString	currentSchedule1;
	ciString	url;
	ciString	date;
	ciString	time;
	ciString	extra;

};

typedef list<SSEle>	SSList;

class ScreenShotInfo
{
public:
	ScreenShotInfo() {};
	~ScreenShotInfo() { clear();}
	void	toString(int page,int count, ciString& outValue);
	void	push(SSEle& ele);
	void	clear();

	const char*		getSiteId(const char* hostId);
protected:
	ciMutex _listLock;
	SSList	_list;
};

class ubcHostInfo
{
public:
	ubcHostInfo() {
		operationalState=ciFalse;
		displayCounter=0;
		displaySide="A";
	}

	ciString	mgrId;
	ciString	siteId;
	ciString	hostId;
	ciBoolean	operationalState;
	ciShort		displayCounter;
	ciString	autoSchedule1;
	ciString	autoSchedule2;
	ciString	currentSchedule1;
	ciString	currentSchedule2;
	ciString	lastSchedule1;
	ciString	lastSchedule2;
	ciTime		lastScheduleTime1;
	ciTime		lastScheduleTime2;
	ciBoolean	networkUse1;
	ciBoolean	networkUse2;
	ciBoolean	scheduleApplied1;
	ciBoolean	scheduleApplied2;

	ciString	displaySide;
};


typedef list<ubcHostInfo*>	ubcHostInfoList;

class ubcHost {
public:
	ubcHost();
	virtual ~ubcHost() ;

	ciBoolean			init(const char* siteId, const char* hostId) {clear(); return add(siteId,hostId,"A");}
	ciBoolean			init(const char* siteId, const char* hostId, const char* whereClause) 
						{clear(); return add(siteId,hostId,"A",whereClause);}
	ciBoolean			init(const char* programId) {clear(); return add(programId);}
	ubcHostInfoList*	get() { return &_hostList;}

	ciBoolean			hasApplied1(const char* siteId, const char* hostId);
	ciBoolean			hasApplied2(const char* siteId, const char* hostId);

	ciBoolean			add(const char* siteId, const char* hostId, const char* side);
	ciBoolean			add(const char* siteId, const char* hostId, const char* side, const char* whereClause);
	ciBoolean			add(const char* scheduleId);

	void				clear();
	void				printIt();

	static ciBoolean	getScreenShot(const char* siteId, const char* hostId, ciString& fileList,ciShort startHour=0,ciShort endHour=0,ciBoolean usingMinute=ciFalse);
	static ciBoolean	getScreenShot(const char* whereClause, ScreenShotInfo& ssInfo);

	static int			getAppliedHostList(const char* programId, ciStringList& outList);
	static int			getReservedHostList(const char* programId, ciStringList& outList);
	static int			getHostList(const char* programId,const char* whereClause,ciStringList& outList);
	static ciBoolean	setAdminState(const char* siteId,const char* hostId,ciBoolean adminState);
	static ciBoolean	setAttribute(const char* siteId, const char* hostId, const char* attr, cciValue* value);


protected:
	ubcHostInfoList		_hostList;
};







#endif // _ubcHost_h_
