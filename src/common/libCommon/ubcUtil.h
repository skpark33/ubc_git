#ifndef _ubcUtil_h_
#define _ubcUtil_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>

class ubcUtil {
public:
	static ubcUtil*	getInstance();
	static void	clearInstance();

	virtual ~ubcUtil() ;

	ciLong genId(const char* field);
	int	getChildren(const char* pEntity,const char* attrName,ciStringList& outList);
	int	getDataSet(const char* pEntity,const char* attrName,const char* whereClause, ciStringSet& outList);
	int	getHostListUsingThisProgram(const char* programId, ciStringSet& hostList);

	ciBoolean isTest();
	ciBoolean isGlobal();
#ifdef _COP_MSC_
	ciBoolean isGlobalUI();
#endif
	ciShort getGMT();
	ciLong	getGMTGap(ciShort gmt);

	ciBoolean removeObject(cciEntity& entity);
	ciLong		genVncPort();

	ciBoolean removeFaultCall(const char* siteId, 
					const char* hostId,
					const char* faultId,
					 ciLong		severity,
					 const char* cause,
					 const char* additionalText );

protected:
	ubcUtil();
	ciLong		_genVncPort(int minVal, int maxVal, int delta);
	ciLong		_getMinVncPort();

	ciMutex	_genIdLock;

	static ubcUtil*	_instance;
	static ciMutex			_instanceLock;

};

#endif // _utvUtil_h_
