/*
 *  Copyright ⓒ 2002 SQISSoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2005/12/05
 *  File name   : ubcIni.h
 */

#ifndef _ubcIni_h_
#define _ubcIni_h_

//----------------------------------------------------------------------------
//	Include Header File
//----------------------------------------------------------------------------

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libBase/ciTime.h>
#include <ci/libConfig/ciEnv.h>
/* 
  ubcIni : Property 의 데이터 구조 class
*/

#ifndef _COP_MSC_
//class ciSimpleIni;
typedef bool boolean;
#endif
//

class ubcIni
{
public:
	ubcIni(const char* filename, const char* path="", const char* sub="");
	virtual	~ubcIni() {}

	
	virtual boolean		get(const char* section, const char* key, ciString& pValue, const char* defaltValue="");
	virtual boolean		get(const char* section, const char* key, ciShort& pValue);
	boolean		get(const char* section, const char* key, ciShort& pValue, const char* defaultValue);
	virtual boolean		get(const char* section, const char* key, ciLong& pValue);
	boolean		get(const char* section, const char* key, ciLong& pValue, const char* defaultValue);
	virtual boolean		get(const char* section, const char* key, ciUShort& pValue);
	virtual boolean		get(const char* section, const char* key, ciULong& pValue);
	virtual boolean		get(const char* section, const char* key, ciTime& pValue);
	virtual boolean		get(const char* section, const char* key, ciBoolean& pValue);

	virtual boolean		set(const char* section, const char* key, const char* pValue);
	virtual boolean		set(const char* section, const char* key, ciShort pValue);
	virtual boolean		set(const char* section, const char* key, ciLong pValue);
	virtual boolean		set(const char* section, const char* key, ciUShort pValue);
	virtual boolean		set(const char* section, const char* key, ciULong pValue);
	virtual boolean		set(const char* section, const char* key, ciTime pValue);
	virtual boolean		set(const char* section, const char* key, ciBoolean pValue);


protected:
	virtual boolean		_get(const char* section, const char* key, int& pValue);
	boolean		_get(const char* section, const char* key, int& pValue, const char* defaultValue);

	
	string _fullpath;
};


#ifdef _COP_MSC_
class ubcConfig : public ubcIni {
public:
	ubcConfig(const char* filename)	
		: ubcIni(filename,_COP_CD("C:\\SQISoft\\UTV1.0\\execute\\data"),"")
		{}
	virtual ~ubcConfig() {};

	// lvc is low version compatibility
	static boolean lvc_setProperty(const char* name, const char* value);
	static boolean lvc_getProperty(const char* name, ciString& value);
};


class ubcMutexConfig : public ubcConfig {
public:
	ubcMutexConfig(const char* filename, int waitSec=INFINITE)	
		: ubcConfig(filename), _lockName(filename), _hMutex(NULL)
	{ lock(waitSec); }
	virtual ~ubcMutexConfig() { unlock();};

	void lock(int waitSec);
	void unlock();

protected:
	HANDLE	_hMutex;
	ciString	_lockName;

};
#endif
#endif 

