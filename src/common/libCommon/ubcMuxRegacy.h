#ifndef _ubcMuxRegacy_h_
#define _ubcMuxRegacy_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>
#include <common/libCommon/ubcIni.h>
#include <hi/libHttp/ubcMux.h>
#ifdef _COP_MSC_
#include <hi/libHttp/hiHttpMux.h>
#endif
#include <ci/libConfig/ciIni.h>


class ubcMuxRegacyData : public virtual ubcMuxData
{
public:
	ubcMuxRegacyData() { }
	virtual ~ubcMuxRegacyData() { }

	// inherited
	virtual const char* getFTPAddress();
	virtual ciString	getFTPAddress_s() { return getFTPAddress(); }
	virtual ciBoolean	isAvail() { return _isAvail; }
	virtual void		print();
	virtual const char* getIpAddress();
	virtual ciString	getIpAddress_s() { return getIpAddress(); }

protected:
	ciString& _getFTPAddress();
	ciString& _getFTPMirror();
	ciBoolean	_connectTest(const char* ipAddress);
	//ciBoolean 	_isAvail; //inherited
	ciString	_current;
	ciString	_buf;
};

typedef map<ciString,ubcMuxRegacyData*>	ubcMuxRegacyMap;

class ubcMuxRegacy : public virtual  ubcMux {
public:
	ubcMuxRegacy();
	virtual ~ubcMuxRegacy() ;

	// inherited
	virtual ciBoolean	init();
	virtual void		clear();
	virtual void		printMap();
	virtual int			size() { return _siteMap.size(); }
	virtual ubcMuxRegacyData*	getMuxData(const char* siteId);
	virtual ubcMuxRegacyData*	getMuxData();

	// not inherited
	const char* getPmId(const char* siteId);
	int	getHostListUsingThisProgram(const char* programId, ciStringSet& hostList);
	void init_check();

protected:
	ciBoolean	_initPmMap(const char* pmId);
	ciBoolean	_initSiteMap(const char* siteId);

	void		_clearPmMap();
	void		_clearSiteMap();

	//ciBoolean _getMuxServerInfo(ciString& oMuxIp, int& oMuxPort);
	//ciBoolean _getServerInfo(const char* siteId, const char* programId);

	ciMutex		_mapLock;
	ubcMuxRegacyMap	_siteMap;
	ubcMuxRegacyMap	_orphanMap;
	ubcMuxRegacyMap	_pmMap;
	ubcMuxRegacyMap	_programMap;
};

class ubcMuxRegacyFactory : public virtual ubcMuxFactory 
{
public:
	ubcMuxRegacyFactory() {}
	virtual ~ubcMuxRegacyFactory() {}
	virtual ubcMux*   createNewMux();
};

class muxFactorySelector 
{
public:
	static ubcMuxFactory* select();
};

/*  더이상 필요없음.
class ubcMuxRT {
public:
	static ubcMuxRT*	getInstance();
	static void	clearInstance();

	~ubcMuxRT() {clear();}
	void clear();

	ubcMuxRegacyData*	getMuxData(const char* siteId, const char* programId, ciBoolean realTime=ciFalse);
protected:
	ubcMuxRT()  {_muxPort = -1;}

	ciBoolean _getMuxServerInfo(ciString& oMuxIp, int& oMuxPort);
	
	static ubcMuxRT*	_instance;
	static ciMutex		_instanceLock;

	ubcMuxRegacyMap	_map;
	ciMutex		_mapLock;

	ciString	_muxIp;
	int		_muxPort;
};
*/



#endif // _ubcMuxRegacy_h_
