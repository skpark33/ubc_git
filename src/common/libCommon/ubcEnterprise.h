#ifndef _ubcEnterprise_h_
#define _ubcEnterprise_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>
#include <common/libCommon/ubcIni.h>

class ubcEnterprise {
public:
	ubcEnterprise();
	virtual ~ubcEnterprise() ;

	int			listKey(const char* key="*");
	int			createKey(const char* key,const char* password, const char* name, const char* description);
	int			setKey(const char* key,const char* password, const char* name, const char* description);
	int			login(const char* key,const char* password);
	int			deleteKey(const char* key);

	int			getHost(ubcIni& ini, const char* siteId,const char* enterpriseKey,
						const char* hostRange="*");
	ciBoolean	getEnterpriseIp(const char* enterpriseKey, ciString& ip);
	ciBoolean	getEnterpriseKey(const char* siteId,const char* hostId, ciString& key);
	int			createHost(ubcIni& ini, ciString& errMsg, ciLong& errCode, ciString& customer);

	ciBoolean	createUser(const char* mgrId, const char* siteId, const char* userId, long userType)
	{ return _createUser(mgrId,siteId,userId,userType); }

protected:
	ciBoolean _createUser(const char* mgrId, const char* siteId, const char* userId, long userType);

	ciBoolean _isExistHost(const char* mgrId, const char* siteId, const char* hostId);
	ciBoolean _createHost(const char* mgrId, const char* siteId, const char* hostId, const char* hostName, 
						  const char* macAddress, const char* edition, ciTime& authDate,
						  const char* enterprise, ciString& errMsg, ciLong& errCode);
	ciBoolean _isExistSite(const char* siteId,ciString& mgrId);
	ciBoolean _createSite(const char* siteId,const char* enterpriseType, const char* customer, ciString& mgrId);

	ciStringMap	_siteMap;
};

#endif // _ubcEnterprise_h_
