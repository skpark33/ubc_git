/*! \class ubcFilter
 *  Copyright �� 2004, SQIsoft. All rights reserved.
 *
 *  \brief CCI Filter Value
 *  (Environment: VisiBroker 5.2 HP-UX B.11.11)
 *
 *  \author skpark
 *  \version 1.0
 *  \date 2010/10/14 11:01:00
 */
 
#ifndef _ubcFilter_h_
#define _ubcFilter_h_

#include <cci/libValue/cciFilter.h>

/**
 *  struct CCI_Filter {
 *      CCI_FilterUnitList   filter;
 *  };
 */

class ubcFilter : public virtual cciFilter {
public:
	ubcFilter() : cciFilter() {}
	virtual ~ubcFilter() {}

    ciBoolean add(const char* className, const char* attrName, const char* valueList,const char* oper="in");
	ciBoolean getValueSet(const char* className,const char* attrName,ciStringSet& outSet);
protected:
};

#endif // _ubcFilter_h_
