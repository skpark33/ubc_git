#ifndef _ubcPowerOn_h_
#define _ubcPowerOn_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>

class ubcPowerOn { 
// ubcHostInfo에서는 가져오지 않는 모든 정보를 다룬다.
public:

	class ubcPowerOnData {
	public :
		ubcPowerOnData() {powerControl=0;wolPort=0; startupTime = "";  }
		ciString	mgrId;
		ciString	siteId;
		ciString	hostId;
		ciString	macAddress;
		ciLong		powerControl;				// 2=WOL, 3=WOL_REPEATER
		ciLong		wolPort;
		ciString	startupTime;
		ciString	shutdownTime;
		ciString	holiday;
		ciString	vendor;						// 중계기 정보
	};
	typedef  list<ubcPowerOnData*>	UBCPowerOnList;


	virtual ~ubcPowerOn();
	ubcPowerOn(const char* pSiteId, const char* pHostId);
	
	ciBoolean get();
	ciBoolean writeVariables(const char* pmacAddress=0);
	void clear();
	ciBoolean	doPowerOn();

	ciBoolean	powerOn(ubcPowerOnData* data) ;

protected:
	ciBoolean		_getSucceed;
	ciString		_siteId;
	ciString		_hostId;

	ciString		_poweronEntry;
	UBCPowerOnList	_poweronList;

public:
};
#endif // _ubcPowerOn_h_
