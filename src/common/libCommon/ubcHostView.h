#ifndef _ubcHostView_h_
#define _ubcHostView_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>

class ubcHostView { 
// ubcHostInfo에서는 가져오지 않는 모든 정보를 다룬다.
public:
	virtual ~ubcHostView() {}
	ubcHostView(const char* pSiteId, const char* pHostId);
	
	ciBoolean get();
	ciBoolean writeVariables();

protected:
	ciBoolean _getSucceed;
	ciBoolean _useApache;

public:
	ciString mgrId;
	ciString siteId;
	ciString hostId;
	ciLong hostType;				// [ROOT] HostType
	ciString siteName;
	ciString businessCode;
	ciString businessType;
	ciString chainNo;
	ciString phoneNo1;
	//ciString mobileNo;
	ciString hostName;
	ciString domainName;
	ciShort period;					// [ROOT] AgentConnectionPeriod
	ciString description;
	ciShort soundVolume;			//
	ciShort mute;					//
	ciString startupTime;
	ciString shutdownTime;				// [SHUTDOWNTIME]	value
	ciString holiday;				// [SHUTDOWNTIME]	value   // ["03:00","0","6"] --> 03:00^0^6
	ciString edition;
	ciString version;
	ciShort hddThreshold;				// [ROOT] HddThreshold
	ciString zipCode;
	ciString addr1;
	ciString addr2;
	ciString addr3;
	ciString addr4;
	ciShort screenshotPeriod;			// [ROOT] ScreenShotPeriod
	ciTime authDate;
	ciBoolean autoUpdateFlag;			// [ROOT] AutoUpdateFlag
	ciLong renderMode;
	ciShort playLogDayLimit;			// [ROOT] PlayLogDayLimit
	ciString playLogDayCriteria;		// [ROOT] PlayLogDayCriteria
	ciBoolean monitorOff;				//	[ROOT] MonitorOff
	ciString monitorOffList;		//		[ROOT] MonitorOffList
	ciShort soundDisplay;				//
	ciString autoSchedule1;
	ciString autoSchedule2;
	ciString currentSchedule1;
	ciString currentSchedule2;
	ciString lastSchedule1;
	ciString lastSchedule2;
	ciTime lastScheduleTime1;
	ciTime lastScheduleTime2;
	ciBoolean networkUse1;
	ciBoolean networkUse2;
	ciBoolean scheduleApplied1;
	ciBoolean scheduleApplied2;
	ciString baseSchedule1;
	ciString baseSchedule2;
	ciString category;					// [ROOT] Category
	ciShort  monitorType;					// [ROOT] MonitorType
	ciString contentsDownloadTime;		// [ROOT] ContentsDownloadTime
	ciLong download1State;
	ciLong download2State;
	ciString progress1;
	ciString progress2;
	ciString mmsCount;
	ciString winPassword;
	ciString weekShutdownTime;

	// for kolon
	ciString brandId;
};
#endif // _ubcHostView_h_
