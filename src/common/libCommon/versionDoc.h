#ifndef _versionDoc_h_
#define _versionDoc_h_

#include "ci/libThread/ciSyncUtil.h"
#include <map>
#include <list>

// versionDoc 문서입니다.

class VERSION_INFO
{
public:
	VERSION_INFO() { linenum=0;c_time=0; filesize=0;}
	ciString procname;
	ciString keyname;
	ciString version;
	ciString srcname;
	ciString dstname;
	ciString dirname;
	int linenum;
	time_t	c_time;
	ciULong	filesize;
	
	ciString str;
} ;

typedef list<VERSION_INFO> VERSION_LIST;
typedef map<ciString, VERSION_INFO> VERSION_MAP;

class versionDoc
{
public:
	versionDoc();
	virtual ~versionDoc();

	// 도큐먼트 초기화	
	ciBoolean init(const char* home, const char* subpath, const char* file);
	ciBoolean fini();

	ciBoolean	GetVersionInfo(const char* fileName, VERSION_INFO& ver_info);
	ciBoolean	GetVersionInfo(const char* fileName, VERSION_LIST& ver_list);
	ciBoolean	GetVersionInfo(ciStringList& fileNameList, VERSION_LIST& ver_list);
	ciBoolean	GetVersionInfo(VERSION_LIST& ver_list);

	int			diff(VERSION_MAP& target, ciStringMap& outMap);

protected:
	ciBoolean Analysis(const char* home, const char* subpath, const char* file);
	void ParsEnvFileLine(ciString& relpath, const char* envline, VERSION_INFO& verInfo);

	VERSION_MAP	_verMap;
	ciMutex _mutex;
};

#endif //_versionDoc_h_
