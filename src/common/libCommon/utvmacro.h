// ADRequest : requestState
#define REQ_INIT				  0		// 0000,0000 
#define REQ_READY				  3		// 1100,0000 
#define	REQ_COMPLETE			 15		// 1111,0000 
#define REQ_PARTIALLY_FAILED	 31		// 1111,1000
#define REQ_TOTALLY_FAILED		 63		// 1111,1100
#define	REQ_DISCARD				255		// 1111,1111

// Schedule : castingState
#define SCH_INIT				  0		// 0000,0000 
#define SCH_REQUESTED			  1		// 1000,0000 
#define SCH_CONTENTS_VERIFIED	  2		// 0100,0000 
#define SCH_READY				  3		// 1100,0000 
#define	SCH_ON_AIR				  7		// 1110,0000 
#define	SCH_COMPLETE			 15		// 1111,0000 
#define SCH_READY_FAIL			 31		// 1111,1000
#define	SCH_DISCARD				255		// 1111,1111 

// Contents : contentsState
#define CON_INIT				  0		// 0000,0000 
#define CON_ENCODED				  1		// 1000,0000 
#define CON_VERIFIED			  2		// 0100,0000 
#define CON_READY				  3		// 1100,0000 
#define	CON_ON_AIR				  7		// 1110,0000 
#define	CON_COMPLETE			 15		// 1111,0000 
#define CON_READY_FAILED		 31		// 1111,1000
#define CON_COMPLETE_FAILED		 63		// 1111,1100
#define	CON_DISCARD				255		// 1111,1111 

// Contents : contentsType
#define CON_VIDEO					0
#define CON_SMS						1
#define CON_IMAGE					2
#define CON_PROMOTION				3
#define CON_TV						4
#define	CON_TICKER					5
#define	CON_PHONE					6
#define CON_URL						7
#define CON_FLASH					8
#define CON_WEBCAM					9
#define CON_RSS						10
#define CON_CLOCK					11
#define CON_TEXT					12
#define CON_FLASH_TEMPLATE			13
#define CON_DYNAMIC_FLASH			14
#define CON_TYPING					15
#define CON_PPT						16
#define CON_ETC						17
#define CON_WIZARD					18
#define CON_FILES					19
#define CON_CNT						20
#define CON_UNKNOWN					99

// Fault : severity
#define FLT_CLEARED				  0
#define FLT_CRITICAL			  1
#define FLT_MAJOR				  2
#define FLT_MINOR				  3
#define FLT_WARNING				  4
#define	FLT_INDETER				  5


// Fault : ProbableCause(old)
#define SOCKET_BROKEN			"Soket Broken"             
#define PROCESS_DOWN			"Process Down"    
#define RESOURCE_OVERLOAD		"CPU and Memory Overload"   
#define CPU_OVERLOAD			"CPU Overload"  
#define MEMORY_OVERLOAD			"Memory Overload"  
#define RESOURCE_NORMAL			"Normal" 

// Fault : ProbableCause(New)
#define	SERVER_PROCESS_DOWN		"server_process_down"
#define	SERVER_COMMUNICATION_FAIL	"server_communication_fail"
#define	SERVER_VNC_FAIL			"server_vnc_fail"
#define	SERVER_HDD_OVERLOAD		"server_hdd_overload"
#define	SERVER_MEM_OVERLOAD		"server_mem_overload"
#define	SERVER_CPU_OVERLOAD		"server_cpu_overload"
#define	SERVER_NET_OVERLOAD		"server_net_overload"
#define	SERVER_DB_OVERLOAD		"server_db_overload"
#define	HOST_PROCESS_DOWN		"host_process_down"
#define	HOST_COMMUNICATION_FAIL		"host_communication_fail"
#define	HOST_VNC_FAIL			"host_vnc_fail"
#define	HOST_FTP_FAIL			"host_ftp_fail"
#define	HOST_UPDATE_FAIL		"host_update_fail"
#define	HOST_PLAY_CONTENTS_FAIL		"host_play_contents_fail"
#define	HOST_CHANGE_SCHEDULE_FAIL	"host_change_schedule_fail"
#define	HOST_SCREENSHOT_FAIL		"host_screenshot_fail"
#define	HOST_HDD_OVERLOAD		"host_hdd_overload"
#define	HOST_MEM_OVERLOAD		"host_mem_overload"
#define	HOST_CPU_OVERLOAD		"host_cpu_overload"
#define	HOST_NET_OVERLOAD		"host_net_overload"

#define SITE_UNKOWN				0
#define SITE_ADMIN				1
#define SITE_MANAGER			2
#define SITE_USER				3

#define UBC_WM_KEYBOARD_EVENT	1000
#define	UBC_WM_DOWNLOAD_EVENT	2000
#define	UBC_WM_BRW_INFO_EVENT	30000
#define	UBC_WM_TEMPLATE_SHORTCUT	35000
#define	UBC_WM_BRW_GET_DOWNLOAD_STATUS	40000
