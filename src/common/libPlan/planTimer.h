/*! \class planTimer
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _planTimer_h_
#define _planTimer_h_


#include <ci/libTimer/ciTimer.h>
#include <common/libPlan/planManager.h>



class planTimer :  public  ciPollable {
public:
	static planTimer*	getInstance();
	static void	clearInstance();

	planTimer() {}
	virtual ~planTimer() { clear();}

	void clear();
	virtual void processExpired(ciString name, int counter, int interval);
	int	 runPlan(ciTime& now, int startDay=1);


protected:

	ciString	lastTimeStr;

	static planTimer*	_instance;
	static ciMutex			_instanceLock;
};	

#endif //_planTimer_h_
