/*! \class planMonitorTimer
 *  Copyright ⓒ 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _planMonitorTimer_h_
#define _planMonitorTimer_h_


#include <ci/libTimer/ciWinTimer.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <common/libPlan/planManager.h>

class ubcEventLog {
public:
	ubcEventLog() : eventTime(0) {}
	ciString eventType;
	ciString eventKey;
	time_t	eventTime;
	ciString eventBody;
};
typedef map<ciString, ubcEventLog*>	ubcEventLogMap;


class planMonitorTimer :  public  ciWinPollable {
public:

	static planMonitorTimer*	getInstance();
	static void	clearInstance();

	planMonitorTimer() {_ignore = ciFalse; }
	virtual ~planMonitorTimer() { clear();}

	ciBoolean	isAvail() {    return (_ignore ? ciFalse : ciTrue ); }
	void	ignore(ciBoolean p) {  _ignore = p; }
	void	setEventHandler(cciEventHandler* p) { _handler = p; }
	void	setBaseTime(int min, int sec) { _baseMin = min; _baseSec = sec; }
	void clear();

	void			pushEvent(const char* pEventKey,cciEvent& pEvent);  // 수신된 이벤트를 메모리에 집어넣는다.
	ciBoolean		createEventLog(const char* pEventKey, cciEvent& pEvent);  // 발신된 이벤트를 DB 에 집어넣는다.
	
	virtual void processExpired(ciString name, int counter, int interval); // 5분마다 검사한다.

protected:

	void		_removeEvent(ciTime& now); // 현재 시간으로 24시간이 경과한 event 를 메모리에서 삭제한다.
	ciBoolean	_removeEventDB(ciTime& now);  // 현재 시간으로 24시간이 경과한 event 를 DB 에서 삭제한다.
	int			_getEventDB(ciTime& now); // 현재시간으로 부터 5분에서 10분전 event 를 DB 에서 가져와서, 메모리에 넣는다.
	int			_processMissingEvent();	// 두메모리를 비교하여, 잃어버린 이벤틀 찾아내고, 이를 다시 발행한다.


	static planMonitorTimer*	_instance;
	static ciMutex			_instanceLock;

	int _baseMin ;
	int _baseSec ;
	ciBoolean _ignore;
	ciMutex	_postLogLock;
	ubcEventLogMap  _postLogMap;

	ciMutex _receivedLogLock;
	ubcEventLogMap	_receivedLogMap;

	cciEventHandler*	_handler;
	
};	

#endif //_planMonitorTimer_h_
