#ifndef _planThread_h_
#define _planThread_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include <ci/libBase/ciTime.h>

////////////////////
// planThread

class planThread : public virtual ciThread {
public:
	planThread(int counter, ciTime& now);
	virtual ~planThread();

	void run();
	void destroy();
	ciBoolean	isAlive() { return _isAlive; }

protected:

	ciBoolean	_isAlive;
	int _counter;
	ciTime _now;
};
typedef list<planThread*>  planThreadList;

class planThreadManager {
public:
	static int		size();
	static void		clearAll();
	static void		clear();
	static void		push(planThread* pThread);
protected:
	static planThreadList	_list;
	static ciMutex					_listLock;

};


#endif // _planThread_h_
