#ifndef _planManager_h_
#define _planManager_h_
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

#include "common/libPlan/planData.h"
class BPMedia {
public:
	BPMedia() : createTime(0)
				//,downloadState(0) 
				//,downloadRequestTime(0) 
				{}
	ciString siteId;
	ciString bpId;
	ciString bpFile;
	unsigned long createTime;
	//ciShort	downloadState;	// 0 : nothing, 1 : download 를 요청하고 기다리는 상태, 
	                        // 2 : 다운로드 완료된 상태,   -1 : 다운로드 실패한 상태
	//unsigned long downloadRequestTime;  // 다운로드를 요청한 시간.};
};
typedef map<ciString, BPMedia>		BPMediaMap;


#define BPI_PATH		"config"

class planManager  : public cciEventHandler {
public:
	static planManager*	getInstance();
	static void	clearInstance();

	const char*	getMgrId() { return _mgrId.c_str(); }
	virtual		~planManager() ;
	void		clear();
	int			downloadIni(int counter);
	int			updateIni();
	int			loadIni();
	ciBoolean	isServerIni(const char* iniFile);
	ciBoolean	setIni(const char* bpFile, const char* section, const char* name,  ciBoolean value);

	int			loadAll(const char* mgrId);
	int			loadAllFromDB(const char* mgrId); // DB 로 부터 직접 로드하는 함수
	BPData*		load(const char* mgrId, const char* bpId);

	BPData*		getBP(const char* bpId);
	ciBoolean	removeBP(const char* bpId);

	ciBoolean	_createBPLog(const char* actionDirective, const char* actionObject);

/*
	int			createReservation();
	int			removeReservation(ciBoolean modifiedOnly=ciFalse);
	int			removeReservation(const char* bpId);
*/
	int			setState(int pState);

   	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean			addHand();
	//void				setTime(char* hour48, char* hour72) { _startDate = hour48; _endDate = hour72; }
	//void				setWeekDay(int weekday) { _weekday = weekday; }
	void				lock() { _mapLock.lock(); }
	void				unlock() { _mapLock.unlock(); }

	int					getBPList(const char* hostId, SimpleBPList& outBPList);
	int					planExpired(ciTime& now);
	int					planExpiredGMT(ciTime& now);

	void				setHost(const char* hostId) { _hostId = hostId; } // preDownloader 에서 manager 사용시
	ciBoolean			isItHost() { if(_hostId.empty()) return ciFalse; return ciTrue; }
	ciBoolean			downloadContents(ciTime& now);
	void				setEventHandler(cciEventHandler* p, ciBoolean owner=ciFalse) { _eventHandler = p; _eventHandlerOwner=owner;}

	int			pushBP(const char* bpIdList);
	void		clearBP();

	//ciBoolean			setDownloadState(const char* bpId, ciShort downloadState, unsigned long requestTime = 0);
	ciBoolean			download(const char* siteId, const char* programId, ciShort side, ciShort downloadType);
	ciBoolean			stopDownload(const char* siteId, const char* programId, ciShort side, ciShort downloadType);

	ciBoolean			putBRWInfo(const char* brwInfo);
	ciBoolean			isBRWDownloadThis(const char* programId);
	void				deleteOldBrwInfo();
	int					getBrwInfoList(int brwId, BRWInfoList& outList);

	ciBoolean			createApplyLog(cciEntity& targetHost,  // where
										const char* programId, // what
										const char* how	,	// how
										const char* who	,	// who
										ciTime&		applyTime );	// when

	int					programChanged(const char* programId); // 내부의 프로그램이 변경되면, bp 를 set 해준다.

	ciBoolean initSiteMap(const char* mgrId);
	ciBoolean initSiteMapFromDB(const char* mgrId);; // DB 를 직접읽는 함수
	ciBoolean isMySite(const char* targetHost);
	ciBoolean getPmId(const char* siteId, ciString& outVal);
	//ciBoolean getGMT(const char* entity, ciString& mgrId, ciShort& gmt);

	ciBoolean	isIncludeWeekDay(int weekday,ciString& weekInfo);
	ciBoolean	isExceptDay(const char* pStartDay, ciString& exceptDay);

	ciBoolean	changePrevSchedule(const char* hostId, const char* brwId, const char* programId);
	
	ciBoolean	createBPLog(const char* actionDirective, const char* actionObject, const char* additionalInfo);
	ciBoolean	removeBPLog(const char* mgrId);

	int getPlanCounter();
	int loadAll();

	int			getTimeLine(int side, int period, CCI::CCI_StringList& hostIdList, CCI::CCI_StringList& timeLineListList);

protected:
	static planManager*	_instance;
	static ciMutex		_instanceLock;

	planManager() ;
	int			_downloadBP();
	int			_loadBP(const char* bpEntity);
	int			_loadBPFromDB(); // DB 에서 바로 가져오는 함수
	ciBoolean	_addHand(const char* eventName);

	ciBoolean	_bpCreated(ciString& mgrId,ciString& siteId,ciString& bpId);
	ciBoolean	_bpRemoved(ciString& mgrId,ciString& siteId,ciString& bpId);
	ciBoolean	_bpChanged(ciString& mgrId,ciString& siteId,ciString& bpId);

	ciBoolean	_tpCreated(ciString& mgrId,ciString& siteId,ciString& bpId,ciString& tpId);
	ciBoolean	_tpRemoved(ciString& mgrId,ciString& siteId,ciString& bpId,ciString& tpId);
	ciBoolean	_tpChanged(ciString& mgrId,ciString& siteId,ciString& bpId,ciString& tpId);

	ciBoolean	_thCreated(ciString& mgrId,ciString& siteId,ciString& bpId,ciString& thId);
	ciBoolean	_thRemoved(ciString& mgrId,ciString& siteId,ciString& bpId,ciString& thId);

	int			_reservationExpired(ciTime& now, const char* targetHost,ciShort side,
								const char* programId, time_t startTime, ciString& bpFile);
	int			_reloadSchedule(ciTime& now, const char* targetHost,ciShort side,const char* programId, time_t todayStartTime);

	void		_clearLastSchedule();
	void		_clearNullLastSchedule();
	//ciBoolean	_removeScheduleMap(const char* bpId);

	ciStringMap	_siteMap;
	ciMutex		_siteLock;

	BPDataMap	_map;
	ciMutex		_mapLock;

	LastScheduleMap _scheduleMap;

	//ciString	_startDate;
	//ciString	_endDate;
	//int			_weekday;
	ciString	_hostId;
	cciEventHandler*	_eventHandler;
	ciBoolean	_eventHandlerOwner;

	// 방송계획 BP 명령을 소켓을 통해 받았을때, 이를 Map 에 push 한다.
	ciMutex		_bpMapLock;
	BPMediaMap	_bpMap;
	ciBoolean	_bpDirtyFlag;  // <-- Agent 가 bpMap 을 갱신하면 true, Timer 가 BP의 갱신을 인지하면 false 임.
	unsigned long		_bpLastUpdateTime;  // <-- Agent 가 bpMap 을 마지막으로 갱신한 시간. 
	BPMediaMap	_bpDownloadedMap;  // 최신으로, 새롭게 다운로드가  된 List 를 유지한다.

	BRWInfoList	_brwList;
	ciMutex		_brwListLock;

	ciString	_mgrId; // 

	class TimeLine {
	public:
		ciTime startTime;
		ciTime endTime;
		ciString programId;
		ciBoolean adminState;
		TimeLine() : adminState(ciTrue) {}
	};
	typedef map<ciString,TimeLine*> TimeLineMap; // key 는 startTime string 이다.
	typedef list<TimeLine*> TimeLineList; // key 는 startTime string 이다.
};

#endif // _planManager_h_
