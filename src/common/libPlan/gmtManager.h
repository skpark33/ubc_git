#ifndef _gmtManager_h_
#define _gmtManager_h_
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

class gmtManager  : public cciEventHandler {
public:
	typedef map<ciString, int>	GMTMAP;

	static gmtManager*	getInstance();
	static void	clearInstance();

	const char*	getMgrId() { return _mgrId.c_str(); }
	virtual		~gmtManager() ;
	void		clear();

	int			load(const char* mgrId);
	int			find(const char* key);

   	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean			addHand();
	ciBoolean			isGlobal();

protected:
	static gmtManager*	_instance;
	static ciMutex		_instanceLock;

	gmtManager() ;

	GMTMAP		_map;
	ciMutex		_mapLock;

	ciString	_mgrId; // 
};

#endif // _gmtManager_h_
