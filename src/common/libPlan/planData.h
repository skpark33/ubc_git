#ifndef _planData_h_
#define _planData_h_

enum E_TYPE_DOWNLOAD
{
	E_TYPE_PACKAGE_FILE,		//컨텐츠 패키지 파일을 다운로드
	E_TYPE_BPI_FILE,			//방송계획 파일을 다운로드
	E_TYPE_CONTENTS_FILE,	//컨텐츠 패키지 안의 컨텐츠 파일들을 다운로드
	E_TYPE_ALL_FILE			//컨텐츠 패키지와 안의 컨텐츠 파일들을 다운로드
};
/*
#define		WM_CMD_DOWNLOAD_START		2100	//다운로드 시작
#define		WM_CMD_DOWNLOAD_CANCEL	2200	//다운로드 취소
#define		WM_CMD_DOWNLOAD_END		2300	//다운로드 완료

#define		DOWNLOADER_NAME			"UBCDownloader.exe"

//다운로드 명령을 수행하기위한 구조체
typedef struct
{
	char	fileName[256];		//다운로드를 수행할 이름(컨텐츠 패키지 파일, 방송계획 파일, 패키지 이름) 
	char	siteId[256];		//다운로드할 파일의 site Id
	int		typeDownload;	//다운로드하는 파일의 종류(컨텐츠 패키지파일, 방송계획 파일, 컨텐츠 파일)
	int		browserId;		//Player의 Id(0, 1)
	HWND	hWnd;		//다운로드 명령을 주고 받을 윈도우 핸들
	bool	mResult;			//다운로드 결과값  <== 추가
} ST_CmdDownload;



class CmdDownload {
public:
	CmdDownload(const char* fileName, const char* siteId, int typeDownload, int browserId, HWND& hWnd)
	{
		memset((void*)&data,0x00,sizeof(ST_CmdDownload));
		sprintf(data.fileName,"%s", fileName);
		sprintf(data.siteId,"%s", siteId);
		data.typeDownload = typeDownload;
		data.browserId = browserId;
		data.hWnd = hWnd;
		data.mResult = false;
	}
	char*  getData()  { return (char*)&data; }
	void	printIt() ;

	ST_CmdDownload  data;
};
*/

class BRWInfo {
public:
	BRWInfo() : brwState(0), brwId(0), lastUpdateTime(0), pid(0) {}
	int brwState;
	int brwId;  
	ciString programId;
	unsigned long lastUpdateTime;
	// skpark 2012.4.5
	ciString errMsg;
	unsigned long pid;
};
typedef list<BRWInfo*>	BRWInfoList;  
#define BRW_INIT		1
#define BRW_DOWNLOAD	2
#define BRW_RUN			3



class SimpleBP {
public:
	ciString siteId;
	ciString bpId;
	ciString bpFile;
	ciTime	createTime;
};
typedef  list<SimpleBP>	SimpleBPList; 

class THData {
public:
	THData() : side(1),_gmt(9999) {}		
	~THData() {}

	ciString thId;
	ciString targetHost;
	ciShort	 side;

	void setGMT();
	ciShort getGMT() { setGMT(); return _gmt; }

protected:
	ciShort  _gmt;

};
typedef  list<THData*>		THDataList;  

class TPData {
public:
	TPData() : state(0), expiredState(0), downloadState(0), currentRetryCount(0) {}
	~TPData() {}

	ciString		tpId;
	ciString		startTime;
	ciString		endTime;
	ciString		programId;
	ciString		zorder;
	ciShort			state;

	ciBoolean		expiredState;
	ciShort			downloadState;
	ciShort			currentRetryCount;
	ciString		sectionName;

	void printIt();
};
typedef  map<ciString,	TPData*>	TPDataMap;  // key : zorder+tpId
typedef  list<TPData*>		TPDataList;  


class LastSchedule {
public:
	LastSchedule() { side =0; lastSchedule="NULL"; prevSchedule="NULL"; tpData=0; gmt=9999; }
	ciShort side;
	ciShort gmt;
	ciString lastSchedule;
	ciString prevSchedule;
	ciString bpId;
	ciString tpSectionName;
	ciString bpFile;
	time_t todayStartTime;
	TPData*	tpData;
	ciString prevTPId;   // 2012.5.2 prevTPId
};
typedef  map<ciString, LastSchedule*>	LastScheduleMap; 



#define	NO_DOWNLOAD				0
#define	DOWNLOADING				1
#define	DOWNLOAD_COMPLETE		2

class BPData {
public:
	BPData() : state(0), retryCount(0), retryGap(0) {}
	~BPData() { clearTP(); clearTH(); }

	TPData*  findTP(const char* tpKey);
	THData*	 findTH(const char* thKey);

	void clearTP();
	void clearTH();

	int loadTP();
	int loadTPFromDB();
	int loadTH();
	int loadTHFromDB();
	ciBoolean	setIni(const char* section, const char* name,  ciShort value);

	int reservationExpiredGMT(ciTime& now, LastScheduleMap& outmap);
	int reservationExpired(ciTime& now, LastScheduleMap& outmap, ciBoolean exceptionDay, ciString& hostId);
	int createReservation(ciString& hour48);
	int removeReservation();
	int setState(int pState);

	int getTargetHost(ciString& outHostInfo);

	ciBoolean	isTargetHost(const char* hostId);
	int	download(ciTime& now);
	int	stopDownload(const char* programId);

	int _removeReservation(	ciString& rEntity );
	int	_createReservation(	ciString& hour48,
							ciString& tpId, ciString& thId,
							ciString& targetHost,ciShort side,
							ciString& startTime, ciString& endTime, ciString& programId);

	ciBoolean _isAlreadyExist(const char* fromTime,const char* toTime, 
								const char* tpEntity,const char* thEntity);

	int _setBPState(int pState);
	int _setTPState(TPData* pData, int pState);

	ciBoolean	_download(TPData* pTP);


	ciTime			createTime;
	ciString		mgrId;
	ciString		siteId;
	ciString		bpId;
	ciString		bpFile;
	ciTime			startDate;
	ciTime			endDate;
	ciString		weekInfo;
	ciString		exceptDay;
	ciString		zorder;
	ciShort			state;

	ciTime			downloadTime;
	ciShort			retryCount;
	ciShort			retryGap;

	TPDataMap		tpMap;
	THDataList		thList;
	ciMutex			tpLock;
	ciMutex			thLock;
	ciMutex			downloadLock;
};
typedef map<ciString, BPData*>			BPDataMap;  // key : zorder+bpId
typedef list<BPData*>					BPDataList;
typedef map<ciString, BPDataList*>		BPDataListMap;  






#endif // _planData_h_
