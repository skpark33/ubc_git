

        #ifndef ADB_FILELIST_H
        #define ADB_FILELIST_H

       /**
        * adb_FileList.h
        *
        * This file was auto-generated from WSDL
        * by the Apache Axis2/Java version: 1.6.0  Built on : May 17, 2011 (04:21:18 IST)
        */

       /**
        *  adb_FileList class
        */

        

        #include <stdio.h>
        #include <axiom.h>
        #include <axis2_util.h>
        #include <axiom_soap.h>
        #include <axis2_client.h>

        #include "axis2_extension_mapper.h"

        #ifdef __cplusplus
        extern "C"
        {
        #endif

        #define ADB_DEFAULT_DIGIT_LIMIT 1024
        #define ADB_DEFAULT_NAMESPACE_PREFIX_LIMIT 64
        

        typedef struct adb_FileList adb_FileList_t;

        
        

        /******************************* Create and Free functions *********************************/

        /**
         * Constructor for creating adb_FileList_t
         * @param env pointer to environment struct
         * @return newly created adb_FileList_t object
         */
        adb_FileList_t* AXIS2_CALL
        adb_FileList_create(
            const axutil_env_t *env );

        /**
         * Wrapper for the "free" function, will invoke the extension mapper instead
         * @param  _FileList adb_FileList_t object to free
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_free (
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

        /**
         * Free adb_FileList_t object
         * @param  _FileList adb_FileList_t object to free
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_free_obj (
            adb_FileList_t* _FileList,
            const axutil_env_t *env);



        /********************************** Getters and Setters **************************************/
        
        

        /**
         * Getter for id. 
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_FileList_get_id(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

        /**
         * Setter for id.
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @param arg_id axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_set_id(
            adb_FileList_t* _FileList,
            const axutil_env_t *env,
            const axis2_char_t*  arg_id);

        /**
         * Resetter for id
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_reset_id(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

        
        

        /**
         * Getter for pw. 
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_FileList_get_pw(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

        /**
         * Setter for pw.
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @param arg_pw axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_set_pw(
            adb_FileList_t* _FileList,
            const axutil_env_t *env,
            const axis2_char_t*  arg_pw);

        /**
         * Resetter for pw
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_reset_pw(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

        
        

        /**
         * Getter for path. 
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_FileList_get_path(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

        /**
         * Setter for path.
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @param arg_path axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_set_path(
            adb_FileList_t* _FileList,
            const axutil_env_t *env,
            const axis2_char_t*  arg_path);

        /**
         * Resetter for path
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_reset_path(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

        
        

        /**
         * Getter for topDirectoryOnly. 
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return axis2_bool_t
         */
        axis2_bool_t AXIS2_CALL
        adb_FileList_get_topDirectoryOnly(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

        /**
         * Setter for topDirectoryOnly.
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @param arg_topDirectoryOnly axis2_bool_t
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_set_topDirectoryOnly(
            adb_FileList_t* _FileList,
            const axutil_env_t *env,
            axis2_bool_t  arg_topDirectoryOnly);

        /**
         * Resetter for topDirectoryOnly
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_reset_topDirectoryOnly(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

        


        /******************************* Checking and Setting NIL values *********************************/
        

        /**
         * NOTE: set_nil is only available for nillable properties
         */

        

        /**
         * Check whether id is nill
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_FileList_is_id_nil(
                adb_FileList_t* _FileList,
                const axutil_env_t *env);


        

        /**
         * Check whether pw is nill
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_FileList_is_pw_nil(
                adb_FileList_t* _FileList,
                const axutil_env_t *env);


        

        /**
         * Check whether path is nill
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_FileList_is_path_nil(
                adb_FileList_t* _FileList,
                const axutil_env_t *env);


        

        /**
         * Check whether topDirectoryOnly is nill
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_FileList_is_topDirectoryOnly_nil(
                adb_FileList_t* _FileList,
                const axutil_env_t *env);


        

        /**************************** Serialize and Deserialize functions ***************************/
        /*********** These functions are for use only inside the generated code *********************/

        
        /**
         * Wrapper for the deserialization function, will invoke the extension mapper instead
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @param dp_parent double pointer to the parent node to deserialize
         * @param dp_is_early_node_valid double pointer to a flag (is_early_node_valid?)
         * @param dont_care_minoccurs Dont set errors on validating minoccurs, 
         *              (Parent will order this in a case of choice)
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_deserialize(
            adb_FileList_t* _FileList,
            const axutil_env_t *env,
            axiom_node_t** dp_parent,
            axis2_bool_t *dp_is_early_node_valid,
            axis2_bool_t dont_care_minoccurs);

        /**
         * Deserialize an XML to adb objects
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @param dp_parent double pointer to the parent node to deserialize
         * @param dp_is_early_node_valid double pointer to a flag (is_early_node_valid?)
         * @param dont_care_minoccurs Dont set errors on validating minoccurs,
         *              (Parent will order this in a case of choice)
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_FileList_deserialize_obj(
            adb_FileList_t* _FileList,
            const axutil_env_t *env,
            axiom_node_t** dp_parent,
            axis2_bool_t *dp_is_early_node_valid,
            axis2_bool_t dont_care_minoccurs);
                            
            

       /**
         * Declare namespace in the most parent node 
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @param parent_element parent element
         * @param namespaces hash of namespace uri to prefix
         * @param next_ns_index pointer to an int which contain the next namespace index
         */
       void AXIS2_CALL
       adb_FileList_declare_parent_namespaces(
                    adb_FileList_t* _FileList,
                    const axutil_env_t *env, axiom_element_t *parent_element,
                    axutil_hash_t *namespaces, int *next_ns_index);

        

        /**
         * Wrapper for the serialization function, will invoke the extension mapper instead
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @param FileList_om_node node to serialize from
         * @param FileList_om_element parent element to serialize from
         * @param tag_closed whether the parent tag is closed or not
         * @param namespaces hash of namespace uri to prefix
         * @param next_ns_index an int which contain the next namespace index
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axiom_node_t* AXIS2_CALL
        adb_FileList_serialize(
            adb_FileList_t* _FileList,
            const axutil_env_t *env,
            axiom_node_t* FileList_om_node, axiom_element_t *FileList_om_element, int tag_closed, axutil_hash_t *namespaces, int *next_ns_index);

        /**
         * Serialize to an XML from the adb objects
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @param FileList_om_node node to serialize from
         * @param FileList_om_element parent element to serialize from
         * @param tag_closed whether the parent tag is closed or not
         * @param namespaces hash of namespace uri to prefix
         * @param next_ns_index an int which contain the next namespace index
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axiom_node_t* AXIS2_CALL
        adb_FileList_serialize_obj(
            adb_FileList_t* _FileList,
            const axutil_env_t *env,
            axiom_node_t* FileList_om_node, axiom_element_t *FileList_om_element, int tag_closed, axutil_hash_t *namespaces, int *next_ns_index);

        /**
         * Check whether the adb_FileList is a particle class (E.g. group, inner sequence)
         * @return whether this is a particle class.
         */
        axis2_bool_t AXIS2_CALL
        adb_FileList_is_particle();

        /******************************* Alternatives for Create and Free functions *********************************/

        

        /**
         * Constructor for creating adb_FileList_t
         * @param env pointer to environment struct
         * @param _id axis2_char_t*
         * @param _pw axis2_char_t*
         * @param _path axis2_char_t*
         * @param _topDirectoryOnly axis2_bool_t
         * @return newly created adb_FileList_t object
         */
        adb_FileList_t* AXIS2_CALL
        adb_FileList_create_with_values(
            const axutil_env_t *env,
                axis2_char_t* _id,
                axis2_char_t* _pw,
                axis2_char_t* _path,
                axis2_bool_t _topDirectoryOnly);

        


                /**
                 * Free adb_FileList_t object and return the property value.
                 * You can use this to free the adb object as returning the property value. If there are
                 * many properties, it will only return the first property. Other properties will get freed with the adb object.
                 * @param  _FileList adb_FileList_t object to free
                 * @param env pointer to environment struct
                 * @return the property value holded by the ADB object, if there are many properties only returns the first.
                 */
                axis2_char_t* AXIS2_CALL
                adb_FileList_free_popping_value(
                        adb_FileList_t* _FileList,
                        const axutil_env_t *env);
            

        /******************************* get the value by the property number  *********************************/
        /************NOTE: This method is introduced to resolve a problem in unwrapping mode *******************/

        
        

        /**
         * Getter for id by property number (1)
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_FileList_get_property1(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

    
        

        /**
         * Getter for pw by property number (2)
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_FileList_get_property2(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

    
        

        /**
         * Getter for path by property number (3)
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_FileList_get_property3(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

    
        

        /**
         * Getter for topDirectoryOnly by property number (4)
         * @param  _FileList adb_FileList_t object
         * @param env pointer to environment struct
         * @return axis2_bool_t
         */
        axis2_bool_t AXIS2_CALL
        adb_FileList_get_property4(
            adb_FileList_t* _FileList,
            const axutil_env_t *env);

    
     #ifdef __cplusplus
     }
     #endif

     #endif /* ADB_FILELIST_H */
    

