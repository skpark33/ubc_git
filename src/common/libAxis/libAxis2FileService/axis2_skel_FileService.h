

    /**
     * axis2_skel_FileService.h
     *
     * This file was auto-generated from WSDL for "FileService|http://sqisoft.com/" service
     * by the Apache Axis2/C version: 1.6.0  Built on : May 17, 2011 (04:19:43 IST)
     * axis2_skel_FileService Axis2/C skeleton for the axisService- Header file
     */


	#include <axis2_svc_skeleton.h>
	#include <axutil_log_default.h>
	#include <axutil_error_default.h>
    #include <axutil_error.h>
	#include <axiom_text.h>
	#include <axiom_node.h>
	#include <axiom_element.h>
    #include <stdio.h>


   
     #include "adb_MoveFile.h"
    
     #include "adb_MoveFileResponse.h"
    
     #include "adb_FileExist.h"
    
     #include "adb_FileExistResponse.h"
    
     #include "adb_FileList.h"
    
     #include "adb_FileListResponse.h"
    
     #include "adb_Test.h"
    
     #include "adb_TestResponse.h"
    
     #include "adb_FileInfo.h"
    
     #include "adb_FileInfoResponse.h"
    
     #include "adb_CopyFile.h"
    
     #include "adb_CopyFileResponse.h"
    
     #include "adb_Login.h"
    
     #include "adb_LoginResponse.h"
    
     #include "adb_DeleteFile.h"
    
     #include "adb_DeleteFileResponse.h"
    
     #include "adb_GetFile.h"
    
     #include "adb_GetFileResponse.h"
    
     #include "adb_FilesInfo.h"
    
     #include "adb_FilesInfoResponse.h"
    
     #include "adb_PutFile.h"
    
     #include "adb_PutFileResponse.h"
    
     #include "adb_Logout.h"
    
     #include "adb_LogoutResponse.h"
    
     #include "adb_CopyDirectory.h"
    
     #include "adb_CopyDirectoryResponse.h"
    
     #include "adb_DeleteDirectory.h"
    
     #include "adb_DeleteDirectoryResponse.h"
    
     #include "adb_MoveDirectory.h"
    
     #include "adb_MoveDirectoryResponse.h"
    

	#ifdef __cplusplus
	extern "C" {
	#endif

     

		 
        /**
         * auto generated function declaration
         * for "MoveFile|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _moveFile of the adb_MoveFile_t*
         *
         * @return adb_MoveFileResponse_t*
         */
        adb_MoveFileResponse_t* axis2_skel_FileService_MoveFile(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_MoveFile_t* _moveFile);


     

		 
        /**
         * auto generated function declaration
         * for "FileExist|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _fileExist of the adb_FileExist_t*
         *
         * @return adb_FileExistResponse_t*
         */
        adb_FileExistResponse_t* axis2_skel_FileService_FileExist(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_FileExist_t* _fileExist);


     

		 
        /**
         * auto generated function declaration
         * for "FileList|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _fileList of the adb_FileList_t*
         *
         * @return adb_FileListResponse_t*
         */
        adb_FileListResponse_t* axis2_skel_FileService_FileList(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_FileList_t* _fileList);


     

		 
        /**
         * auto generated function declaration
         * for "Test|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _test of the adb_Test_t*
         *
         * @return adb_TestResponse_t*
         */
        adb_TestResponse_t* axis2_skel_FileService_Test(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_Test_t* _test);


     

		 
        /**
         * auto generated function declaration
         * for "FileInfo|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _fileInfo of the adb_FileInfo_t*
         *
         * @return adb_FileInfoResponse_t*
         */
        adb_FileInfoResponse_t* axis2_skel_FileService_FileInfo(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_FileInfo_t* _fileInfo);


     

		 
        /**
         * auto generated function declaration
         * for "CopyFile|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _copyFile of the adb_CopyFile_t*
         *
         * @return adb_CopyFileResponse_t*
         */
        adb_CopyFileResponse_t* axis2_skel_FileService_CopyFile(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_CopyFile_t* _copyFile);


     

		 
        /**
         * auto generated function declaration
         * for "Login|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _login of the adb_Login_t*
         *
         * @return adb_LoginResponse_t*
         */
        adb_LoginResponse_t* axis2_skel_FileService_Login(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_Login_t* _login);


     

		 
        /**
         * auto generated function declaration
         * for "DeleteFile|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _deleteFile of the adb_DeleteFile_t*
         *
         * @return adb_DeleteFileResponse_t*
         */
        adb_DeleteFileResponse_t* axis2_skel_FileService_DeleteFile(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_DeleteFile_t* _deleteFile);


     

		 
        /**
         * auto generated function declaration
         * for "GetFile|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _getFile of the adb_GetFile_t*
         *
         * @return adb_GetFileResponse_t*
         */
        adb_GetFileResponse_t* axis2_skel_FileService_GetFile(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_GetFile_t* _getFile);


     

		 
        /**
         * auto generated function declaration
         * for "FilesInfo|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _filesInfo of the adb_FilesInfo_t*
         *
         * @return adb_FilesInfoResponse_t*
         */
        adb_FilesInfoResponse_t* axis2_skel_FileService_FilesInfo(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_FilesInfo_t* _filesInfo);


     

		 
        /**
         * auto generated function declaration
         * for "PutFile|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _putFile of the adb_PutFile_t*
         *
         * @return adb_PutFileResponse_t*
         */
        adb_PutFileResponse_t* axis2_skel_FileService_PutFile(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_PutFile_t* _putFile);


     

		 
        /**
         * auto generated function declaration
         * for "Logout|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _logout of the adb_Logout_t*
         *
         * @return adb_LogoutResponse_t*
         */
        adb_LogoutResponse_t* axis2_skel_FileService_Logout(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_Logout_t* _logout);


     

		 
        /**
         * auto generated function declaration
         * for "CopyDirectory|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _copyDirectory of the adb_CopyDirectory_t*
         *
         * @return adb_CopyDirectoryResponse_t*
         */
        adb_CopyDirectoryResponse_t* axis2_skel_FileService_CopyDirectory(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_CopyDirectory_t* _copyDirectory);


     

		 
        /**
         * auto generated function declaration
         * for "DeleteDirectory|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _deleteDirectory of the adb_DeleteDirectory_t*
         *
         * @return adb_DeleteDirectoryResponse_t*
         */
        adb_DeleteDirectoryResponse_t* axis2_skel_FileService_DeleteDirectory(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_DeleteDirectory_t* _deleteDirectory);


     

		 
        /**
         * auto generated function declaration
         * for "MoveDirectory|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _moveDirectory of the adb_MoveDirectory_t*
         *
         * @return adb_MoveDirectoryResponse_t*
         */
        adb_MoveDirectoryResponse_t* axis2_skel_FileService_MoveDirectory(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              adb_MoveDirectory_t* _moveDirectory);


     

    /** we have to reserve some error codes for adb and for custom messages */
    #define AXIS2_SKEL_FILESERVICE_ERROR_CODES_START (AXIS2_ERROR_LAST + 2500)

    typedef enum 
    {
        AXIS2_SKEL_FILESERVICE_ERROR_NONE = AXIS2_SKEL_FILESERVICE_ERROR_CODES_START,
        
        AXIS2_SKEL_FILESERVICE_ERROR_LAST
    } axis2_skel_FileService_error_codes;

	#ifdef __cplusplus
	}
	#endif
    

