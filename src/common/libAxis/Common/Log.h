﻿#ifndef __Log_h__
#define __Log_h__

#include <string>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/xmlreader.h>


#define LogWriteLine(level,msg) \
	{\
		Log* Log = Log::getInstance();\
		if(Log->hasLog(level)){\
			Log->timeStamp();\
			Log->lineInfo(level,__FILE__,__LINE__);\
			Log->writeLog msg;\
		}\
	}

namespace UBC_HTTP
{
	enum LOG_LEVEL
	{
		NO = 0,
		ERROR = 2,
		WARNING = 4,
		INFO = 6,
		DEBUG = 8,
		FULL = 9,
	};

	class Log
	{
	public:
		static Log* 	getInstance();
		static void 	clearInstance();
		virtual ~Log();

		void setLevel(LOG_LEVEL p) { _logLevel = p; } 
		bool hasLog(LOG_LEVEL loglevel) { 
			return (_log && _logLevel >= loglevel);
		}
		int	logOpen(const char* logFile);
		void logClose();

		void timeStamp(bool newline=true);
		void lineInfo(LOG_LEVEL loglevel,const char* filename, int lineno);
		void writeLog(const char* pFormat,...);
	protected:
		Log();

		const char* _logLevelStr(LOG_LEVEL ll)
		{
			switch(ll){
				case ERROR		: return "ERROR";
				case WARNING	: return "WARNING";
				case INFO		: return "INFO";
				case DEBUG		: return "DEBUG";
				case FULL		: return "FULL";
				default 		: return "NO";
			}
			return "NO";
		}

		static Log*	_instance;
		FILE* 			_log;
		time_t 			_now;
		LOG_LEVEL		_logLevel;
		unsigned long	_pid;
	};
};
#endif
