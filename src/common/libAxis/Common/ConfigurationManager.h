﻿#ifndef __ConfigurationManager_h__
#define  __ConfigurationManager_h__
#include <string>
#include <map>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/xmlreader.h>



namespace UBC_HTTP
{
	class ConfigurationManager {
	public:
		ConfigurationManager();
		~ConfigurationManager();
		const char* AppSettings(const char* key);
		const char* AppSettings(const std::string& key);
		bool	Init();
		void	Clear();

		static ConfigurationManager* 	getInstance();
		static void 	clearInstance();
		static ConfigurationManager*	_instance;

	protected:
		int		_readXML();
		void 	_parseItem( xmlDocPtr doc 
							,xmlNodePtr a_node
							,const char* nameword
							,const char* keyword
							,const char* valueword
							,STRMAP *map );

		void	_printMap(const char* mapName,STRMAP* map);
		void	_clearMap(STRMAP* map);

		STRMAP _appSettingsMap;
	};


};
#endif
