﻿#include "UBC_HTTP/Common/CommonIncludes.h"
#include "UBC_HTTP/Common/Common.h"
#include "UBC_HTTP/Common/Log.h"
#include <stdio.h>
#include <stdarg.h>

namespace UBC_HTTP
{
	Log*	Log::_instance = 0;

	Log*
	Log::getInstance()
	{
		if (_instance == 0) {
			_instance = new Log();
		}
		return _instance;
	}

	void
	Log::clearInstance()
	{
		if(_instance) {
			delete _instance;
			_instance = NULL;
		}
	}

	Log::Log()
	{
		_log = 0;
		_logLevel = DEBUG;
		_now = 0;
		_pid = getpid();
	}

	Log::~Log()
	{
	}

	void 
	Log::timeStamp(bool newline)
	{
		if(_log ){
			time_t anow = time(NULL);
			if(anow-_now >= 60){
				_now = anow;
				std::string timeStr;
				Common::curTimeStr(_now,timeStr);
				std::string buf = "TIMESTAMP=[";
				buf += timeStr ;
				if(newline){
					buf += "]\n";	
				}
				fprintf(_log, buf.c_str());
			}
		}
	}
	void
	Log::lineInfo(LOG_LEVEL loglevel,const char* filename, int lineno)
	{
		if(_log ){
		   	fprintf(_log,"[%s]:[%u] %s(line:%d)::",
				   _logLevelStr(loglevel),_pid,filename,lineno); 
		   	//fprintf(_log,"[[[%s]]] : %s(line:%d)::",
			//	   _logLevelStr(loglevel),filename,lineno); 
		}
	}

	void 
	Log::writeLog(const char* pFormat,...)
	{
		if(_log ){
			va_list	aVp;
			va_start(aVp, pFormat);
			vfprintf(_log, pFormat, aVp);
			va_end(aVp);
			fprintf(_log, "\n");
			fflush(_log);
		}
	}

	int
	Log::logOpen(const char* logFile)
	{
		if(_log) {
			// aleady opened
			return 2;
		}
		std::string timeStr;
		Common::curTimeStr(timeStr,"_%4d%02d%02d%02d%02d%02d_");

		std::string fullname = logFile;
		fullname += timeStr;
		fullname += Common::itoa(_pid);
		fullname += ".log";
		/*
		if(::access(logFile,R_OK)==0){
			//file exist
			std::string backupFile = fullname;
			backupFile += ".bak";
			::rename(fullname.c_str(),backupFile.c_str());
		}
		*/

		_log = fopen(fullname.c_str(), "a");
		if(_log==0) {
			return 0;
		} 
		timeStamp(false);
		writeLog(" Log Open(pid=%u)",_pid);
		return 1;
	}

	void
	Log::logClose()
	{
		if(_log) {
			timeStamp(false);
			writeLog("Log Close(pid=%u)",_pid);
			fclose(_log);
		}
		_log=0;
		return;
	}
};
