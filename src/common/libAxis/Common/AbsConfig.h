﻿#ifndef __UBCProperties_h__
#define __UBCProperties_h__

#include <UBC_HTTP/Common/CommonIncludes.h>

namespace UBC_HTTP
{
	class AbsConfig
	{
	public:
		AbsConfig();
		virtual ~AbsConfig();

		virtual void SetFilename(std::string& filename)  { _filename = filename; }
		virtual const char* GetValue(const char* key, const char* defaultValue="");
		virtual void PrintMap(const char* mapName);
		virtual int Init() = 0;
	protected:
		int 	_configOpen();
		void	_configClose();


		FILE*	_fd;
		STRMAP _map;
		std::string _filename;
	};

	class UBCProperties : public virtual AbsConfig
	{
	public:
		static UBCProperties* 	getInstance();
		static void 	clearInstance();
		virtual ~UBCProperties() {};
		virtual int Init();
	protected:
		UBCProperties() {};
		static UBCProperties*	_instance;
	};

	class UBCVariables : public virtual AbsConfig
	{
	public:
		static UBCVariables* 	getInstance();
		static void 	clearInstance();
		virtual ~UBCVariables() {};
		virtual int Init();
	protected:
		UBCVariables() {};
		static UBCVariables*	_instance;
	};


};
#endif
