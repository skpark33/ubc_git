#ifndef _CiDirHandler_h_
#define _CiDirHandler_h_

#include <UBC_HTTP/Common/CommonIncludes.h>

namespace UBC_HTTP 
{
class CiDirHandler {
	public:

		static bool	exist(std::string& pName);
		static bool	create(std::string& pName);
		static bool	remove(std::string& pName);
};
};

#endif // _CiDirHandler_h_
