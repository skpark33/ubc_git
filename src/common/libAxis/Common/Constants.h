﻿#ifndef __Constants_h__
#define __Constants_h__

#include <UBC_HTTP/Common/CommonIncludes.h>


namespace UBC_HTTP
{
	class Constants
	{
	public:
		static Constants* 	getInstance();
		static void 	clearInstance();
		static Constants*	_instance;

		bool 		alreadyInit;
		HashTable 	user_path;
		HashTable 	user_pwd;
		HashTable 	nat_map;

		std::string 	DEFAULT_PATH;
		std::string 	CONFIG_ROOT;
		std::string 	PROJECT_HOME;
		std::string 	UBC_VARIABLES;
		std::string 	COP_PROPERTIES;
		std::string 	COP_GET_MODE;
		std::string 	COP_DELIM;
		int 			LOG_LEVEL;
		std::string 	LOG_FILENAME;
		std::string 	LOG_DELIM;
		int 			COP_EVENT_TIMEOUT;
		int 			COP_EVENT_POLLING_INTERVAL;

		void Init();
		Constants();
		~Constants();
		std::string USER_PATH(std::string userId) { return USER_PATH(userId.c_str());}
		std::string USER_PWD(std::string userId) { return USER_PWD(userId.c_str()); }
		std::string USER_PATH(const char* userId);
		std::string USER_PWD(const char* userId);

		const char* COP_SCGW_PORT();
		const char*  DB_IP();
		const char*  DB_UID();
		const char*  DB_PWD();
		const char*  DB_DATABASE();
		std::string DB_CONNECTION_STRING();
		bool IS_GLOBAL();
		const char* NAT_IP(std::string globalIP);
	};

}
#endif
