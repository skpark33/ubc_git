#ifndef _CiMySQL_h_
#define _CiMySQL_h_

#include <mysql.h>
#include <errno.h>

namespace UBC_HTTP
{
	typedef int	ciMutex;

	class CiMySQL 
	{
	public:
		CiMySQL(const char* pDbName, const char* pDbUser, const char* pDbPass, const char* pDbHost="localhost", int pPort = 3306);
		virtual ~CiMySQL();

		virtual bool	connect();
		virtual bool	disconnect();

		virtual bool	execute(const char* pSql);
		virtual bool	execute(std::string &pSql) { return execute(pSql.c_str()); }
		virtual bool	executeGet(std::string &pSql) { return _execute(pSql.c_str()); }

		virtual bool	isConnected()   { return _connected; }
		virtual int			errorCode()	{ if(_mysql) return mysql_errno(_mysql); return 0;  } 
		virtual const char*	errorMsg() 	{ if(_mysql) return mysql_error(_mysql); return ""; };

		virtual bool	next();
		virtual const char*	getValueString(int idx);
		virtual const char*	getValueString(const char* fieldname);
		virtual int			getFieldCount() { return _num_fields; }
		virtual const char* getFieldString(int idx);

	protected:

		virtual bool	_execute(const char* pSql);
		virtual void		_closeSession();
		virtual void		_clearResult();

		std::string	_dbName;
		std::string	_dbUser;
		std::string	_dbPasswd;
		std::string	_dbHost;
		int			_dbPort;

		MYSQL*		_mysql;
		bool	_connected;
		ciMutex		_lock;

		MYSQL_RES*      _res;
		int 			_num_fields;
		MYSQL_FIELD* 	_fields;
		MYSQL_ROW		_row;
	};

};
#endif // _CiMySQL_h_
