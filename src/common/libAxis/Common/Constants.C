﻿#include <UBC_HTTP/Common/CopServiceIncludes.h>

namespace UBC_HTTP
{

	Constants*	Constants::_instance = 0;

	Constants*
	Constants::getInstance()
	{
		if (_instance == 0) {
			_instance = new Constants();
		}
		return _instance;
	}

	void
	Constants::clearInstance()
	{
		if(_instance) {
			delete _instance;
			_instance = NULL;
		}
	}

	Constants::Constants()
    {
		alreadyInit = false;
		DEFAULT_PATH = "";
		CONFIG_ROOT = "";
		PROJECT_HOME = "";
		UBC_VARIABLES = "";
		COP_PROPERTIES = "";
		COP_GET_MODE = "";
		COP_DELIM = "";
		LOG_LEVEL = 4;  // WARNING Level
		LOG_FILENAME = "";
		LOG_DELIM = "";
		COP_EVENT_TIMEOUT = 25;
		COP_EVENT_POLLING_INTERVAL = 100;
	}

	Constants::~Constants()
    {
	}
				
	void Constants::Init()
	{
		if (alreadyInit)
		{
			LogWriteLine(DEBUG,("alreayInit"));
			return;
		}
		LogWriteLine(DEBUG,("Init()"));

		ConfigurationManager::getInstance()->Init();

		CONFIG_ROOT     = Environment::getInstance()->GetEnvironmentVariable("CONFIGROOT");
		if(CONFIG_ROOT.empty()){
			LogWriteLine(DEBUG,("Web.config CONFIGROOT will be used"));
			CONFIG_ROOT    = ConfigurationManager::getInstance()->AppSettings("CONFIGROOT");
		}
		PROJECT_HOME    = Environment::getInstance()->GetEnvironmentVariable("PROJECT_HOME");
		if(PROJECT_HOME.empty()){
			LogWriteLine(DEBUG,("Web.config PROJECT_HOME will be used"));
			PROJECT_HOME    = ConfigurationManager::getInstance()->AppSettings("PROJECT_HOME");
		}

		LogWriteLine(DEBUG,("CONFIGROOT=%s", CONFIG_ROOT.c_str()));
		LogWriteLine(DEBUG,("PROJECT_HOME=%s", PROJECT_HOME.c_str()));

		DEFAULT_PATH    = ConfigurationManager::getInstance()->AppSettings("DEFAULT_PATH");
		UBC_VARIABLES   = ConfigurationManager::getInstance()->AppSettings("UBC_VARIABLES");
		COP_PROPERTIES  = ConfigurationManager::getInstance()->AppSettings("COP_PROPERTIES");
		COP_GET_MODE    = ConfigurationManager::getInstance()->AppSettings("COP_GET_MODE");
		COP_DELIM       = ConfigurationManager::getInstance()->AppSettings("COP_DELIM");
		LOG_FILENAME    = ConfigurationManager::getInstance()->AppSettings("LOG_FILENAME");
		LOG_DELIM       = ConfigurationManager::getInstance()->AppSettings("LOG_DELIM");

		LogWriteLine(DEBUG,("DEFAULT_PATH=%s", DEFAULT_PATH.c_str()));

		LOG_LEVEL		= atoi(ConfigurationManager::getInstance()->AppSettings("LOG_LEVEL"));
		COP_EVENT_TIMEOUT	= atoi(ConfigurationManager::getInstance()->AppSettings("COP_EVENT_TIMEOUT"));
		COP_EVENT_POLLING_INTERVAL	= atoi(ConfigurationManager::getInstance()->AppSettings("COP_EVENT_POLLING_INTERVAL"));

		std::string userId[5];
		userId[0]="ds_ent_center";
		userId[1]="ds_ent_center_server";
		userId[2]="ent_center";
		userId[3]="ent_center_server";
		userId[4]="server";

		for(int i=0;i<5;i++){
			// USER_PATH  
			std::string userPath = ConfigurationManager::getInstance()->AppSettings(userId[i]+"_PATH");
			if (userPath.empty()) userPath = DEFAULT_PATH;
			user_path.Add(userId[i], userPath);

			// USER_PWD          
			std::string userPwd = ConfigurationManager::getInstance()->AppSettings(userId[i]+"_PWD");
			if (userPwd.empty()) userPwd = "";
			user_pwd.Add(userId[i], userPwd);
		}

		// 공용IP --> 사설IP 변경용
		for (int i=1;true;i++)
		{
			std::string nat_ip = ConfigurationManager::getInstance()->AppSettings("NAT_IP_"+Common::itoa((short)i));
			if (nat_ip.empty()) break;

			std::string global_ip,internal_ip;
			Common::Split(nat_ip,'/',&global_ip,&internal_ip);
			nat_map.Add(global_ip, internal_ip);
		}

		UBCProperties::getInstance()->Init();
		UBCProperties::getInstance()->PrintMap(COP_PROPERTIES.c_str());
		UBCVariables::getInstance()->Init();
		UBCVariables::getInstance()->PrintMap(UBC_VARIABLES.c_str());

		alreadyInit = true;
		return;
	}

	std::string Constants::USER_PATH(const char* userId)
	{
		std::string key = userId;
		if (user_path.ContainsKey(key))
		{
			 return user_path[key];
		}
		std::string userPath = (const char*)ConfigurationManager::getInstance()->AppSettings(key+"_PATH");
		if (userPath.empty()) userPath = DEFAULT_PATH;
		user_path.Add(key, userPath);
		return userPath;
		
	}
	std::string Constants::USER_PWD(const char* userId)
	{
		std::string key = userId;
		if (user_pwd.ContainsKey(key))
		{
			 return user_pwd[key];
		}
		std::string userPwd = (const char*)ConfigurationManager::getInstance()->AppSettings(key+"_PWD");
		if (userPwd.empty()) userPwd = "";
		user_pwd.Add(key, userPwd);
		return userPwd;
	}
	//
	// ubc.properties의 값
	const char* Constants::COP_SCGW_PORT()
	{
		return UBCProperties::getInstance()->GetValue("COP.SCGW.PORT","14001");
	}

	//PS.MYSQL_DB.DBNAME=ubc
	//PS.MYSQL_DB.PORT=3306
	//PS.MYSQL_DB.PWD=sqicop
	//PS.MYSQL_DB.SERVER=211.232.57.215
	//PS.MYSQL_DB.USR=ubc
	//PS.MYSQL_DB.DSN=ubc_event
	//PS.MYSQL_DB.DSN_REG=DSN=ubc_event:Network=TCP/IP:Server=211.232.57.215,1433:Database=ubc:Description=ubc:Trusted_connection=No:
	//PS.MYSQL_DB.DSN_DRV=MySQL Connector 5.1
	const char* Constants::DB_IP()
	{
		return UBCProperties::getInstance()->GetValue("PS.MYSQL_DB.SERVER", "localhost");
	}
	const char* Constants::DB_UID()
	{
		return UBCProperties::getInstance()->GetValue("PS.MYSQL_DB.USR", "ubc");
	}
	const char* Constants::DB_PWD()
	{
		return UBCProperties::getInstance()->GetValue("PS.MYSQL_DB.PWD", "");
	}
	const char* Constants::DB_DATABASE()
	{
		return UBCProperties::getInstance()->GetValue("PS.MYSQL_DB.DBNAME", "ubc");
	}
	std::string Constants::DB_CONNECTION_STRING()
	{
		char buf[1024];
		sprintf( buf, "Server={%s};uid={%s};pwd={%s};database={%s};", 
				DB_IP(), DB_UID(), DB_PWD(), DB_DATABASE());
		return buf;
	}
	bool Constants::IS_GLOBAL()
	{
		std::string value = UBCVariables::getInstance()->GetValue("GLOBAL", "0");
		return (value == "1");
	}
	// 공용IP --> 사설IP 변경용
	const char* Constants::NAT_IP(std::string globalIP)
	{
		if (nat_map.ContainsKey(globalIP))
		{
			return nat_map[globalIP];
		}
		return globalIP.c_str();
	}
}
