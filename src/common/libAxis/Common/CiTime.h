/*! \file CiTime.h
 *
 *  Copyright �� 2003 WINCC Inc.
 *  All Rights Reserved.
 *
 *  \brief Time Wrapper Class
 *  (Environment: OSF1 5.1A)
 *
 *  \author 	jhchoi
 *  \version
 *  \date 		2003�� 4�� 28�� 14:00
 */

#ifndef _CiTime_h_
#define _CiTime_h_

#include <string>
#include <list>
#include <ctime>

#define UNIX_DATE_FORMAT_C  "%04d/%02d/%02d %02d:%02d:%02d"
#define MFC_DATE_FORMAT_C  "%FT%T%z"

class CiTime {
public:

	static void timeCheckLog(const char* pMsg);

	CiTime();
	CiTime(const CiTime& p_time);
	CiTime(time_t p_time);
	CiTime(struct tm& p_tm);
	CiTime(const char* p_strTime);
	CiTime(const std::string& p_strTime);

	void	set(); //set current time
	void	set(const CiTime& p_time);
	void	set(time_t p_time);
	void	set(struct tm& p_tm);
	void	set(const char* p_strTime);
	void	set(const std::string& p_strTime);

	void	add(int sec);

	time_t	getTime() const;
	struct tm	getTM();
	std::string	getTimeString();
	std::string	getTimeString(const std::string& p_strForm);
	std::string getTimeString(const std::string& p_strForm, int rpos, const char* insert);
	void		getTimeString_r(std::string& p_strTime);
	void		getTimeString_r(const std::string& p_strForm, std::string& p_strTime);
	
	int		getYear();
	int		getMonth();
	int		getDay();
	int		getHour();
	int		getMinute();
	int		getSecond();
	int		getWday();
	int		getRemainDaySec();
	int		getRemainHourSec();

	int operator - (const CiTime &p_time);
	int operator - (int p_time);
	int operator + (int p_time);
	CiTime& operator = (const CiTime& p_time);
	CiTime& operator = (const char* p_strTime);
	bool operator > (const CiTime& p_time);
	bool operator >= (const CiTime& p_time);
	bool operator == (const CiTime& p_time);
	bool operator < (const CiTime& p_time);
	bool operator <= (const CiTime& p_time);

protected:
	void		_setTM();
	int		_setTimeString(const char* p_strTime);

	time_t	m_time;
	struct tm	m_tm;
};

#endif //_CiTime_h_
