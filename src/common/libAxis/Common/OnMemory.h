﻿#ifndef  __OnMemory_h__
#define  __OnMemory_h__
#include <UBC_HTTP/Common/CommonIncludes.h>

namespace UBC_HTTP
{
	class  MIRInfo {
	public:
		MIRInfo() : _indexAttr(0),_attrTypeMap(0) {}
		MIRInfo(STRARRAY* a, STRMAP* b) : _indexAttr(a),_attrTypeMap(b) {}
		virtual ~MIRInfo() {
			if(_indexAttr) 	delete _indexAttr;
			if(_attrTypeMap) delete _attrTypeMap;
		}
	
		const char* getType(std::string& attrname) {
			if(!_attrTypeMap) return "";
			STRMAP::iterator itr = _attrTypeMap->find(attrname);
			if(itr!=_attrTypeMap->end()){
				return itr->second.c_str();
			}
			return "";
		}

		const char* getIndexAttribute(unsigned int idx) {
			if(_indexAttr==0 || idx >= _indexAttr->size()) {
				return "";
			}
			return (*_indexAttr)[idx].c_str();
		}

		int indexCount() { return ((_indexAttr) ?  _indexAttr->size():  0);}
		int attrCount() { return ((_attrTypeMap)?  _attrTypeMap->size(): 0);}

	protected:
		STRARRAY*	_indexAttr;  // [order]=index attrName
		STRMAP*		_attrTypeMap;   // [attrName]=attrType
	};

	typedef std::map<std::string, MIRInfo*>	MIRMAP; // key = _classname 

	class OnMemory {
	public:
		OnMemory();
		~OnMemory();

		static OnMemory* 	getInstance();
		static void 	clearInstance();

		MIRInfo* GetMirInfo(std::string& pClassName, 
							std::string& pClassScopedName,
							std::string& errMsg);

		std::string CheckId(const char* id, const char* pw);
	protected:
		static OnMemory*	_instance;

		MIRMAP _mirMap;
	};
};

#endif
