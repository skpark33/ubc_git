#ifndef _CopServiceIncludes_h
#define _CopServiceIncludes_h_

#include "UBC_HTTP/Common/CommonIncludes.h"
#include "UBC_HTTP/Common/Common.h"
#include "UBC_HTTP/Common/HashTable.h"
#include "UBC_HTTP/Common/Log.h"
#include "UBC_HTTP/Common/Environment.h"
#include "UBC_HTTP/Common/ConfigurationManager.h"
#include "UBC_HTTP/Common/Constants.h"
#include "UBC_HTTP/Common/AbsConfig.h"

using namespace UBC_HTTP;

#endif
