/*
 *  Copyright �� 200l SQISoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2002/12/05
 *  File name   : CiFuleUtil.h
 */

#ifndef _CiFileUtil_h_
#define _CiFileUtil_h_

#include <string>
#include <list>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <UBC_HTTP/Common/CommonIncludes.h>

namespace UBC_HTTP
{
	typedef struct stat  STAT_T;


	enum FileAttributes
	{
		ReadOnly = 1,
		Hidden = 2,
		System = 4,
		Directory = 16,
		Archive = 32,
		Device = 64,
		Normal = 128,
		Temporary = 256,
		SparseFile = 512,
		ReparsePoint = 1024,
		Compressed = 2048,
		Offline = 4096,
		NotContentIndexed = 8192,
		Encrypted = 16384
	};

	class DirectoryInfo
	{
	public:
		DirectoryInfo() {
			CreationTime=0;
			LastAccessTime=0;
			LastWriteTime=0;
			Attributes = Normal;
		}
		virtual void SetInfo(const char* rel_path, const char* filename, STAT_T& statbuf);
		virtual void ToXML(std::string& outval);

		std::string 	FullName;
		std::string 	Name;
		std::string 	Extension;
		time_t 			CreationTime;
		time_t 			LastAccessTime;
		time_t 			LastWriteTime;
		FileAttributes 	Attributes;
	};
	typedef std::list<DirectoryInfo>	DirectoryInfoList;

	class FileInfo : public virtual DirectoryInfo
	{
	public:
		FileInfo() {
			Length=0;
		}
		virtual void SetInfo(const char* relative_path_filename, STAT_T& statbuf);
		virtual void SetInfo(const char* relative_path,const char* filename, STAT_T& statbuf);
		virtual void ToXML(std::string& outval);

		std::string 	DirectoryName;
		off_t			Length;
	};
	typedef std::list<FileInfo>	FileInfoList;


	class CiFileUtil
	{
	public:
		static FILE* openFile(	const char* pMode,
								const char* pFilename, 
								const char* pSubPath = "", 
								const char* pHomeDir = "CONFIGROOT");

		static bool openFile(FILE*& outFd, 
								const char* pMode,
								const char* pFilename, 
								const char* pSubPath = "", 
								const char* pHomeDir = "CONFIGROOT");

		static void makePath(std::string& outFullPath,
							const char* pFilename, 
							const char* pSubPath, 
							const char* pHomeDir);

		static bool isPathStartFromRoot(const char* pPath);

		static void addPath(std::string& root, const char* added);

		static int isDirectory(const char* pPath);

		static bool copyDir(const char* sourceDir, const char* targetDir, bool overwrite);
		static bool copyDir(const char* sourceDir, const char* targetDir, STRLIST& failList);
		static bool copyDir(const char* sourceDir, const char* targetDir, STRSET& children,STRLIST& failList);
		static bool writeFile(const char* file, off_t offset, unsigned char* context, int context_size, std::string& errMsg);
		static bool readFile(const char* file, off_t offset, int size, unsigned char*& context, int& context_size, std::string& errMsg);
		static bool copyFile(const char* sourceFile, const char* targetFile, bool failIfExist=false);
		static bool moveDir(const char* sourceDir, const char* targetDir );
		static bool moveDir(const char* sourceDir, const char* targetDir, STRLIST& failList);

		static int getFileInfo(std::string& user_path, const char* filename, FileInfo& fileInfo);
		static int listDir(std::string& user_path, const char* sourceDir, bool topDirOnly, 
							DirectoryInfoList& dirInfoList, FileInfoList& fileInfoList);
		static int listDir(const char* sourceDir, const char* ext,STRLIST& outList);
		static int removeDirectory(const char* tarDir);
		static int removeFile(const char* path);
		static bool moveFile(const char* sourceFile, 
								const char* targetFile,
								bool failIfExist=false);
		static	off_t	getFileSize(const char* path);
		static	time_t	getFileLastWriteTime(const char* path);
		static	bool createDirectory(std::string& pName);
	};
};
#endif 

