﻿#ifndef  __Environment_h__
#define  __Environment_h__
#include <string>
#include <map>

namespace UBC_HTTP
{
	class Environment {
	public:
		Environment();
		~Environment();
		std::string GetEnvironmentVariable(const char* key);
		std::string GetUserName();
		std::string GetHostName();

		static Environment* 	getInstance();
		static void 	clearInstance();
		static Environment*	_instance;
	};
};
#endif
