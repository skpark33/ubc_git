/*! \file CiStringTokenizer.h
 *
 *  Copyright ⓒ 2003 WINCC Inc.
 *  All Rights Reserved.
 *
 *  \brief string 을 delimiters 로 Tokenning 한다.
 *  (Environment: OSF1 5.1A)
 *
 *  \author 	jhchoi
 *  \version
 *  \date 		2003년 4월 22일 18:00
 */


#ifndef _CiStringTokenizer_h_
#define _CiStringTokenizer_h_

#include <UBC_HTTP/Common/CommonIncludes.h>

//CiStringTokenizer Class
/**
 * The string tokenizer class allows an application to break a 
 * string into tokens. The tokenization method is simple.
 *
 * @param	str 			: a string to be parsed.
 * @param	delim 		: the char type delimiters.
 * @param	deliList		: the string type delimiters.
 * @param	returnDelims	: flag indicating whether to return the delimiters as tokens.
 * @param	nullToken		: flag indicating whether to return the null string as tokens
 */
class CiStringTokenizer {
public:

	//Default Delimiters \n,\t,\r,\f,space로 문자열을 Tokenning 한다.
	CiStringTokenizer(const std::string& str);
	CiStringTokenizer(const char* str);

 	CiStringTokenizer(const std::string& str, const std::string& delim, bool nullToken = false,
				bool returnDelims = false);
	CiStringTokenizer(const char* str, const char* delim, bool nullToken = false,
				bool returnDelims = false);
	//string delimiters
	CiStringTokenizer(const std::string& str, const STRLIST& deliList, bool nullToken = false,
				bool returnDelims  = false);
	CiStringTokenizer(const char* str, const STRLIST& deliList, bool nullToken = false,
				bool returnDelims = false);
				
	~CiStringTokenizer();

	bool hasMoreTokens();
	int countTokens();	
	
	std::string nextToken();
	std::string nextToken(const std::string& delim);
	std::string nextToken(const char* delim);
	std::string nextToken(const STRLIST& deliList); //string delimiters
	std::string nextToken(int n);
	std::string restTokens();
	
	void nextToken_r(std::string& out);
	void nextToken_r(const std::string& delim, std::string& out);
	void nextToken_r(const char* delim, std::string& out);
	void nextToken_r(const STRLIST& deliList, std::string& out); //string delimiters
	void nextToken_r(int n, std::string& out);
	void restTokens_r(std::string& out);

private:
	void _CiStringTokenizer(const char* str, const char* delim = " \n\t\r\f", 
					bool nullToken = false, bool retDelims = false);
	//string delimiters
	void _CiStringTokenizer(const char* str, const STRLIST& deliList, 
					bool nullToken = false, bool retDelims = false);
	void _setMaxDelimChar();
	void _setMaxDelimString(); //string delimiters
	int _skipDelimiters(int startPos);
	int _scanToken(int startPos);
	
	std::string _str;
	std::string _delimiters;
	STRLIST _delimStrList; //string delimiters
	/**
	* maxDelimChar stores the value of the delimiter character with the
	* highest value. It is used to optimize the detection of delimiter
	* characters.
	*/	
	char _maxDelimChar;
	std::string _maxDelimString; //string delimiters
	bool _retDelims;
	bool _delimsChanged;
	bool _nullToken;
	bool _strDelims;
	int _currentPosition;
	int _newPosition;
	int _maxPosition;
};

#endif //_CiStringTokenizer_h_

