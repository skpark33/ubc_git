﻿#ifndef __Common_h__
#define __Common_h__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

namespace UBC_HTTP
{

#define 	SOCKET			int
#define 	closesocket(fd)	close(fd)	
#define 	WCHAR			wchar_t
#define 	LPWSTR			wchar_t*
#define 	LPSTR			char*

	class Common
	{
	public:
		static void curTimeStr(time_t now,std::string& outval, 
				const char* format= "%4d-%02d-%02d %02d:%02d:%02d");
		static void curTimeStr(std::string& outval,
				const char* format= "%4d-%02d-%02d %02d:%02d:%02d")
				{ curTimeStr(time(NULL),outval,format); }

		static std::string itoa(short no);
		static std::string itoa(unsigned short no);
		static std::string itoa(long no);
		static std::string itoa(unsigned long no);

		static void Split(const std::string& pTarget, const char pDeli,
					 std::string* pFirst, std::string* pOther);

		static void rightTrim(char* str);

		static void SetClientIP(const char* ip) { _peer = ip;}
		static const char* GetClientIP() { return _peer.c_str(); }

		static bool 		is_base64(unsigned char c);
		static std::string 	base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len);
		static std::string	base64_decode(std::string const& encoded_string);;
		static int base64_char_pos(unsigned char& c);
		static int base64_decode(unsigned char* bufplain, const char* bufcoded);

		static bool 		socketAgent(	const char* ipAddress,
											int portNo, 
											const char* directive, 
											const char* data,
											std::string& retval);
		static bool 		socketAgent(	const char* ipAddress,
											int portNo, 
											unsigned char* data,
											int data_len,
					 						const char* header_prefix,
											int header_size,
											std::string& retval,
											std::string& errMsg);
		static bool 		socketAgent(	const char* ipAddress,
											int portNo, 
											unsigned char* data,
											int data_len,
											std::string& retval,
											std::string& errMsg);

		static bool 		socketRecv(	const char* ipAddress,
											int portNo, 
											std::string& retval,
											std::string& errMsg);

		static bool toUnsignedChar(const char* src, 
									int src_size, 
									unsigned char* outval,
									int startIdx=0);

		static bool toUnsignedChar(int src, 
									unsigned char* outval,
									int startIdx=0);

		static int toInt(unsigned char* src, int src_size);

		static std::string& trim(std::string& str);
		static void stringReplace(std::string& target, const char* from, const char* to);
		static void xmlReplace(std::string& target);

		static void toUpper(std::string& str) ;
		static void toUpper(std::string& str, std::string& dest) ;
		static void toLower(std::string& str) ;
		static void toLower(std::string& str, std::string& dest) ;
		//static int	AnsiToUTF8( char* szSrc, char* strDest, int destSize );
		//static int	UTF8ToAnsi( char* szSrc, char* strDest, int destSize );
		//static void	WCharToChar( char* pstrDest, const wchar_t* pwstrSrc );
	public:
		static std::string _peer;
	};
};
#endif
