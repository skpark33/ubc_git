#ifndef _CommonIncludes_h
#define _CommonIncludes_h_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <string>
#include <map>
#include <list>
#include <set>
#include <vector>
#include "axutil_env.h"
#include "axutil_log.h"
#include "axis2_msg_ctx.h"

#define	WEB_CONFIG_FILE 		"/home/ubc/project/ubc/web/UBC_HTTP/UBC_HTTP/Web.config"
#define	COPSERVICE_LOG_FILE		"/home/ubc/project/ubc/log/web/CopService"
#define	FILESERVICE_LOG_FILE		"/home/ubc/project/ubc/log/web/FileService"

typedef std::map<std::string,std::string> STRMAP;
typedef std::vector<std::string> STRARRAY;
typedef std::list<std::string> STRLIST;
typedef std::set<std::string> STRSET;

#endif
