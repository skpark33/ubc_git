﻿#ifndef _HashTable_h_
#define _HashTable_h_

#include <UBC_HTTP/Common/CommonIncludes.h>

namespace UBC_HTTP
{
	class HashTable {
	public:
		HashTable();	
		virtual ~HashTable();
		void Add(std::string& key, std::string& value);
		bool ContainsKey(std::string& key);
		const char* operator [] (std::string& key); 
	protected:
		STRMAP _map;
	};
};

#endif
