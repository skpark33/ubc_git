#ifndef _FileResponse_h_
#define _FileResponse_h_

#include <string>
#include "UBC_HTTP/Common/CopServiceIncludes.h"
#include "adb_FileResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_CopyDirectoryResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_CopyFileResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_DeleteDirectoryResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_DeleteFileResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_FileExistResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_FileInfoResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_FileListResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_FileResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_FilesInfoResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_GetFileResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_LoginResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_LogoutResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_MoveDirectoryResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_MoveFileResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_PutFileResponse.h"
#include "UBC_HTTP/libAxis2FileService/adb_TestResponse.h"

class AbsFileResponse {
public:

	AbsFileResponse()
	{
	   result = false;
	   errorMsg = "";
	   resultType = "";
	   resultData = "";
	   resultBytes = 0;
	   env = 0;
	}

	AbsFileResponse(const axutil_env_t* penv,
			bool inResult, 
			const char* inErrorMsg, 
			const char* inResultType, 
			const char* inResultData,
			unsigned char* inResultBytes,
			int inResultBytesSize)
	{
		Set(penv,inResult, inErrorMsg, inResultType, inResultData, inResultBytes, inResultBytesSize);
	}

	virtual void Set(const axutil_env_t* penv,
			bool inResult, 
			const char* inErrorMsg, 
			const char* inResultType, 
			const char* inResultData,
			unsigned char* inResultBytes,
			int inResultBytesSize)
	{
		result = inResult;
		errorMsg = inErrorMsg;
		resultType = inResultType;
		resultData = inResultData;
		env = penv;

		if(inResultBytes != 0 && inResultBytesSize > 0) {
			resultBytes =  axutil_base64_binary_create_with_plain_binary(
										env, 
										inResultBytes,
										inResultBytesSize);
		}else{
			resultBytes =  0;
		}
		//resultBytes->plain_binary = inResultBytes;
		//resultBytes->plain_binary_len = inResultBytesSize;

	}

	virtual ~AbsFileResponse()  
	{
		if(resultBytes) {
			axutil_base64_binary_free(resultBytes,env);
			resultBytes = 0;
		}
	}


public:

	bool result;
	std::string errorMsg;
	std::string resultType;
	std::string resultData;
	axutil_base64_binary_t* resultBytes;
	const axutil_env_t*  env;
protected:

	adb_FileResponse_t*	_getResponse();
};


class FileResponse : public virtual AbsFileResponse 
{
public :
	adb_FileResponse_t* getResponse()
	{
		return _getResponse();
	}
};


class CopyDirectoryResponse : public virtual AbsFileResponse 
{
public :
	adb_CopyDirectoryResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_CopyDirectoryResponse_t*    response = adb_CopyDirectoryResponse_create(env);
		adb_CopyDirectoryResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class CopyFileResponse : public virtual AbsFileResponse 
{
public :
	adb_CopyFileResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_CopyFileResponse_t*    response = adb_CopyFileResponse_create(env);
		adb_CopyFileResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class DeleteDirectoryResponse : public virtual AbsFileResponse 
{
public :
	adb_DeleteDirectoryResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_DeleteDirectoryResponse_t*    response = adb_DeleteDirectoryResponse_create(env);
		adb_DeleteDirectoryResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class DeleteFileResponse : public virtual AbsFileResponse 
{
public :
	adb_DeleteFileResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_DeleteFileResponse_t*    response = adb_DeleteFileResponse_create(env);
		adb_DeleteFileResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class FileExistResponse : public virtual AbsFileResponse 
{
public :
	adb_FileExistResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_FileExistResponse_t*    response = adb_FileExistResponse_create(env);
		adb_FileExistResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class FileInfoResponse : public virtual AbsFileResponse 
{
public :
	adb_FileInfoResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_FileInfoResponse_t*    response = adb_FileInfoResponse_create(env);
		adb_FileInfoResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class FileListResponse : public virtual AbsFileResponse 
{
public :
	adb_FileListResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_FileListResponse_t*    response = adb_FileListResponse_create(env);
		adb_FileListResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class FilesInfoResponse : public virtual AbsFileResponse 
{
public :
	adb_FilesInfoResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_FilesInfoResponse_t*    response = adb_FilesInfoResponse_create(env);
		adb_FilesInfoResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class GetFileResponse : public virtual AbsFileResponse 
{
public :
	adb_GetFileResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_GetFileResponse_t*    response = adb_GetFileResponse_create(env);
		adb_GetFileResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class LoginResponse : public virtual AbsFileResponse 
{
public :
	adb_LoginResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_LoginResponse_t*    response = adb_LoginResponse_create(env);
		adb_LoginResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class MoveDirectoryResponse : public virtual AbsFileResponse 
{
public :
	adb_MoveDirectoryResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_MoveDirectoryResponse_t*    response = adb_MoveDirectoryResponse_create(env);
		adb_MoveDirectoryResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class MoveFileResponse : public virtual AbsFileResponse 
{
public :
	adb_MoveFileResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_MoveFileResponse_t*    response = adb_MoveFileResponse_create(env);
		adb_MoveFileResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


class PutFileResponse : public virtual AbsFileResponse 
{
public :
	adb_PutFileResponse_t* getResponse()
	{
		adb_FileResponse_t* res_FileResponse = _getResponse();
		adb_PutFileResponse_t*    response = adb_PutFileResponse_create(env);
		adb_PutFileResponse_set_fileResponse(response,env,res_FileResponse);
		return response;
	}
};


#endif
