
#include "CopServiceCallImpl.h"
#include "CopResponse.h"
#include "AgentSocketRcv.h"
#include "TimeSyncRcv.h"
#include "CciCallRcv.h"
#include "CciGetRcv.h"
#include "CciAddHandRcv.h"
#include "CciEventRcv.h"


CopServiceCallImpl::CopServiceCallImpl( const axutil_env_t* penv,
						axis2_msg_ctx_t *pmsg_ctx,
						axis2_char_t* pcommand,
						axis2_char_t* pip,
						int pport,			
						axis2_char_t* pbody,
						axis2_char_t* pquery ) : env(penv)
												,msg_ctx(pmsg_ctx)
{

	command 	= pcommand;
	ip 			= pip;
	body 		= pbody;
	query 		= pquery;
	port 		= pport;


	LogWriteLine(DEBUG,("CopServiceCallImpl(%s, %s,%s,%d,%s,%s)", 
			Common::GetClientIP(),
		   command.c_str(),ip.c_str(),port,body.c_str(),query.c_str()));
}

CopServiceCallImpl::~CopServiceCallImpl() 
{
	LogWriteLine(DEBUG, ("~CopServiceCallImpl()"));
}

adb_CopServiceCallResponse_t*
CopServiceCallImpl::execute()
{
    LogWriteLine(DEBUG,("execute()"));

	CopCommand* cmd = 0;

	if		(command == "AGENT"  ) 
	{
		cmd = new AgentSocketRcv(this);
	}
	else if (command == "TIME"   ) 
	{
		cmd = new TimeSyncRcv(this);
	}
	else if (command == "CALL"   ) 
	{
		cmd = new CciCallRcv(this);
	}
	else if (command == "GET")
	{
		if (Constants::getInstance()->COP_GET_MODE == "CALL")
		{
			cmd = new CciCallRcv(this);
		}
		else
		{
			// use cciCall only !!!
			//cmd = new CciCallRcv(this);
			cmd = new CciGetRcv(this);
		}
	}
	else if (command == "ENCODED_GET")
	{
		//skpark 2013.3.14  ENCODED_GET 은 base64로 decode해야 한다.
		if(!_base64Decode())
		{
			CopResponse aResp(false, "Decoding fail", "", "");
			return aResp.getResponse(env);
		}
		if (Constants::getInstance()->COP_GET_MODE == "CALL")
		{
			cmd = new CciCallRcv(this);
		}
		else
		{
			// use cciCall only !!!
			//cmd = new CciCallRcv(this);
			cmd = new CciGetRcv(this);
		}
	}
	else if (command == "ADDHAND") 
	{
		cmd = new CciAddHandRcv(this);
	}
	else if (command == "EVENT") 
	{
		cmd = new CciEventRcv(this);
	}


	CopResponse* response = 0;
	if(cmd != NULL) {
		response = cmd->Execute();
	}else{
		response =  new CopResponse(false, "Invalid command", "", "");
	}

	LogWriteLine((response->result ? INFO : ERROR), 
					("%s CopService end %d,%s,%s,%s",
					Common::GetClientIP(),  
					response->result, 
					response->errorMsg.c_str(), 
					response->resultType.c_str(), 
					response->resultData.c_str()));

	adb_CopServiceCallResponse_t* aRes = response->getResponse(env);
	delete cmd;
	delete response;
	return aRes;
}


bool
CopServiceCallImpl::_base64Decode()
{
	int bodySize = body.size();
	int querySize = query.size();

	LogWriteLine(DEBUG,("_base64Decode()"));

	body = Common::base64_decode(body);
	query = Common::base64_decode(query);

	if(	_decodeCheck(bodySize,body.size()) 	&& 
		_decodeCheck(querySize,query.size())){
		LogWriteLine(DEBUG,("%s CopService decoded succeed %s,%s,%d,%s,%s", 
			Common::GetClientIP(), 
			command.c_str(), ip.c_str(), port, body.c_str(), query.c_str()));  
		return true;
	}

	LogWriteLine(ERROR, ("%s CopService decoded fail %s,%s,%d,%s,%s", 
		Common::GetClientIP(), 
		command.c_str(), ip.c_str(),port, body.c_str(), query.c_str()));  
	return false;
}

bool
CopServiceCallImpl::_decodeCheck(int before, int after)
{
	LogWriteLine(DEBUG, ("before size=%d, after size=%d",before,after)); 
	if(after == 0) {
		if(before==0) return true;
		return false;
	}
	// after is almost 75% of before
	int rightSize = before*0.75;

	if(rightSize-2 <= after && after <= rightSize+2) {
		return true;
	}
	return false;
}




