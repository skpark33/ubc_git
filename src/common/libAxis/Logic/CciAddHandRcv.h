#ifndef _CciAddHandRcv_h_
#define _CciAddHandRcv_h_

#include "UBC_HTTP/Logic/CopCommand.h"

class CciAddHandRcv : public virtual CopCommand {
public:
	CciAddHandRcv(CopServiceCallImpl*	impl) : CopCommand(impl) {}
	virtual ~CciAddHandRcv() {}
	virtual CopResponse*	Execute();
};

#endif // _AgentScoketRcv_h_
