#ifndef _CopServiceCallImpl_h_
#define _CopServiceCallImpl_h_

#include <string>
#include "UBC_HTTP/Common/CopServiceIncludes.h"
#include "UBC_HTTP/libAxis2CopService/adb_CopServiceCallResponse.h"


class CopServiceCallImpl {
public:
	CopServiceCallImpl( const axutil_env_t* penv,
						axis2_msg_ctx_t *pmsg_ctx,
						axis2_char_t* pcommand,
						axis2_char_t* pip,
						int pport,
						axis2_char_t* pbody,
						axis2_char_t* pquery);
	virtual ~CopServiceCallImpl() ;

    virtual adb_CopServiceCallResponse_t* execute();

public:
	/*
	axis2_char_t*		_command;
	axis2_char_t*		_ip;
	axis2_char_t*		_body;
	axis2_char_t*		_query;
	*/

	std::string		command;
	std::string		ip;
	std::string		body;
	std::string		query;
	int				port;

	const axutil_env_t* env;
	axis2_msg_ctx_t *msg_ctx;

protected:
	bool _base64Decode();
	bool _decodeCheck(int before, int after);
};

#endif // _CopServiceCallImpl_h_
