#ifndef _CopCommand_h_
#define _CopCommand_h_

#include <string>
#include "UBC_HTTP/Common/CopServiceIncludes.h"
#include "UBC_HTTP/Logic/CopResponse.h"
#include "UBC_HTTP/Logic/CopServiceCallImpl.h"

class CopCommand {
public:

	CopCommand(CopServiceCallImpl* impl)
	{
		_impl = impl;
	}
	virtual ~CopCommand()  {}

	virtual CopResponse*	Execute() = 0;
protected:
	CopServiceCallImpl* _impl;
};

#endif // _CopCommand_h_
