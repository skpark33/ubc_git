#ifndef _AgentSocketRcv_h_
#define _AgentSocketRcv_h_

#include "UBC_HTTP/Logic/CopCommand.h"

class AgentSocketRcv : public virtual CopCommand {
public:
	AgentSocketRcv(CopServiceCallImpl*	impl) : CopCommand(impl) {}
	virtual ~AgentSocketRcv() {}
	virtual CopResponse*	Execute();

};

#endif // _AgentScoketRcv_h_
