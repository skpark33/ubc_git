#ifndef _CciCallRcv_h_
#define _CciCallRcv_h_

#include "UBC_HTTP/Logic/CopCommand.h"

class CciCallRcv : public virtual CopCommand {
public:
	CciCallRcv(CopServiceCallImpl*	impl) : CopCommand(impl) {}
	virtual ~CciCallRcv() {}
	virtual CopResponse*	Execute();
};

#endif // _AgentScoketRcv_h_
