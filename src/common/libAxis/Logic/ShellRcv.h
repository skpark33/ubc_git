#ifndef _ShellRcv_h_
#define _ShellRcv_h_

#include "UBC_HTTP/Logic/CopCommand.h"

class ShellRcv : public virtual CopCommand {
public:
	ShellRcv(CopServiceCallImpl*	impl) : CopCommand(impl) {}
	virtual ~ShellRcv() {}
	virtual CopResponse*	Execute();
protected:
	bool _base64Decode(std::string& body);
	bool _decodeCheck(int before, int after);

};

#endif // _AgentScoketRcv_h_
