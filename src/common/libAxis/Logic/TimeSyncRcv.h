#ifndef _TimeSyncRcv_h_
#define _TimeSyncRcv_h_

#include "UBC_HTTP/Logic/CopCommand.h"

class TimeSyncRcv : public virtual CopCommand {
public:
	TimeSyncRcv(CopServiceCallImpl*	impl) : CopCommand(impl) {}
	virtual ~TimeSyncRcv() {}
	virtual CopResponse*	Execute();
};

#endif // _AgentScoketRcv_h_
