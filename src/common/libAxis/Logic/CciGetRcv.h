#ifndef _CciGetRcv_h_
#define _CciGetRcv_h_

#include "UBC_HTTP/Logic/CopCommand.h"
#include "UBC_HTTP/Logic/CciCallRcv.h"
#include "UBC_HTTP/Common/CommonIncludes.h"

class CciGetRcv : public virtual CopCommand {
public:
	CciGetRcv(CopServiceCallImpl*	impl) : CopCommand(impl) {}
	virtual ~CciGetRcv() {}
	virtual CopResponse*	Execute();
protected:
	void _InitClassScopedName(std::string& inBody);

	std::string _classScopedName;
	STRARRAY	_entityList;
	std::string	_className;
};

#endif // _AgentScoketRcv_h_
