#ifndef _CopResponse_h_
#define _CopResponse_h_

#include <string>
#include "UBC_HTTP/Common/CopServiceIncludes.h"
#include "adb_CopServiceCallResponse.h"


class CopResponse {
public:

	CopResponse()
	{
	   result = false;
	   errorMsg = "";
	   resultType = "";
	   resultData = "";
	}

	CopResponse(bool inResult, 
			const char* inErrorMsg, 
			const char* inResultType, 
			const char* inResultData)
	{
		result = inResult;
		errorMsg = inErrorMsg;
		resultType = inResultType;
		resultData = inResultData;
	}

	virtual ~CopResponse()  {}

	adb_CopServiceCallResponse_t*	getResponse(const axutil_env_t *env);

public:

	bool result;
	std::string errorMsg;
	std::string resultType;
	std::string resultData;

protected:
};

#endif // _CopResponse_h_
