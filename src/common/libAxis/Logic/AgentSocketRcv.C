#include "UBC_HTTP/Logic/AgentSocketRcv.h"

CopResponse*	
AgentSocketRcv::Execute()
{
	LogWriteLine(DEBUG,("Execute()"));

	if(Constants::getInstance()->IS_GLOBAL())
	{
		// GLOBAL_CASE NOT IMPLEMENTED!!!
	}

	int bodySize = _impl->body.size();

	LogWriteLine(INFO,("Execute(%s,%d,%d)", _impl->body.c_str(),
											bodySize, sizeof(int)));

	int data_len = bodySize + 1;
	unsigned char* data = new unsigned char[data_len];
	memset(data,0x00,data_len);

	Common::toUnsignedChar(_impl->body.c_str(),bodySize,data,0);
		
	LogWriteLine(DEBUG,("Socket Write(%s)", (char*)data));


	std::string retval, errMsg;
	if(!Common::socketAgent(_impl->ip.c_str(),
							_impl->port,
							data,
							data_len-1,
							retval,
							errMsg)) 
	{
		delete data;
		return new CopResponse(false, errMsg.c_str(), "", "");
	}

	if(Constants::getInstance()->IS_GLOBAL())
	{
		// GLOBAL_CASE NOT IMPLEMENTED!!!
	}

	//retval = retval.substr(8);  // remove header 

	delete data;
	return new CopResponse(true, "", "Text", retval.c_str());
}

