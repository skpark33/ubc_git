#ifndef _CciEventRcv_h_
#define _CciEventRcv_h_

#include "UBC_HTTP/Logic/CopCommand.h"

class CciEventRcv : public virtual CopCommand {
public:
	CciEventRcv(CopServiceCallImpl*	impl) : CopCommand(impl) {}
	virtual ~CciEventRcv() {}
	virtual CopResponse*	Execute();
};

#endif // _AgentScoketRcv_h_
