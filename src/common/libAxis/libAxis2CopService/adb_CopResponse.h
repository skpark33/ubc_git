

        #ifndef ADB_COPRESPONSE_H
        #define ADB_COPRESPONSE_H

       /**
        * adb_CopResponse.h
        *
        * This file was auto-generated from WSDL
        * by the Apache Axis2/Java version: 1.6.0  Built on : May 17, 2011 (04:21:18 IST)
        */

       /**
        *  adb_CopResponse class
        */

        

        #include <stdio.h>
        #include <axiom.h>
        #include <axis2_util.h>
        #include <axiom_soap.h>
        #include <axis2_client.h>

        #include "axis2_extension_mapper.h"

        #ifdef __cplusplus
        extern "C"
        {
        #endif

        #define ADB_DEFAULT_DIGIT_LIMIT 1024
        #define ADB_DEFAULT_NAMESPACE_PREFIX_LIMIT 64
        

        typedef struct adb_CopResponse adb_CopResponse_t;

        
        

        /******************************* Create and Free functions *********************************/

        /**
         * Constructor for creating adb_CopResponse_t
         * @param env pointer to environment struct
         * @return newly created adb_CopResponse_t object
         */
        adb_CopResponse_t* AXIS2_CALL
        adb_CopResponse_create(
            const axutil_env_t *env );

        /**
         * Wrapper for the "free" function, will invoke the extension mapper instead
         * @param  _CopResponse adb_CopResponse_t object to free
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_free (
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

        /**
         * Free adb_CopResponse_t object
         * @param  _CopResponse adb_CopResponse_t object to free
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_free_obj (
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);



        /********************************** Getters and Setters **************************************/
        
        

        /**
         * Getter for result. 
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return axis2_bool_t
         */
        axis2_bool_t AXIS2_CALL
        adb_CopResponse_get_result(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

        /**
         * Setter for result.
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @param arg_result axis2_bool_t
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_set_result(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env,
            axis2_bool_t  arg_result);

        /**
         * Resetter for result
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_reset_result(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

        
        

        /**
         * Getter for errorMsg. 
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopResponse_get_errorMsg(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

        /**
         * Setter for errorMsg.
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @param arg_errorMsg axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_set_errorMsg(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env,
            const axis2_char_t*  arg_errorMsg);

        /**
         * Resetter for errorMsg
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_reset_errorMsg(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

        
        

        /**
         * Getter for resultType. 
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopResponse_get_resultType(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

        /**
         * Setter for resultType.
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @param arg_resultType axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_set_resultType(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env,
            const axis2_char_t*  arg_resultType);

        /**
         * Resetter for resultType
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_reset_resultType(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

        
        

        /**
         * Getter for resultData. 
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopResponse_get_resultData(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

        /**
         * Setter for resultData.
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @param arg_resultData axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_set_resultData(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env,
            const axis2_char_t*  arg_resultData);

        /**
         * Resetter for resultData
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_reset_resultData(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

        


        /******************************* Checking and Setting NIL values *********************************/
        

        /**
         * NOTE: set_nil is only available for nillable properties
         */

        

        /**
         * Check whether result is nill
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_CopResponse_is_result_nil(
                adb_CopResponse_t* _CopResponse,
                const axutil_env_t *env);


        

        /**
         * Check whether errorMsg is nill
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_CopResponse_is_errorMsg_nil(
                adb_CopResponse_t* _CopResponse,
                const axutil_env_t *env);


        
        /**
         * Set errorMsg to nill (currently the same as reset)
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_set_errorMsg_nil(
                adb_CopResponse_t* _CopResponse,
                const axutil_env_t *env);
        

        /**
         * Check whether resultType is nill
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_CopResponse_is_resultType_nil(
                adb_CopResponse_t* _CopResponse,
                const axutil_env_t *env);


        
        /**
         * Set resultType to nill (currently the same as reset)
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_set_resultType_nil(
                adb_CopResponse_t* _CopResponse,
                const axutil_env_t *env);
        

        /**
         * Check whether resultData is nill
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_CopResponse_is_resultData_nil(
                adb_CopResponse_t* _CopResponse,
                const axutil_env_t *env);


        
        /**
         * Set resultData to nill (currently the same as reset)
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_set_resultData_nil(
                adb_CopResponse_t* _CopResponse,
                const axutil_env_t *env);
        

        /**************************** Serialize and Deserialize functions ***************************/
        /*********** These functions are for use only inside the generated code *********************/

        
        /**
         * Wrapper for the deserialization function, will invoke the extension mapper instead
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @param dp_parent double pointer to the parent node to deserialize
         * @param dp_is_early_node_valid double pointer to a flag (is_early_node_valid?)
         * @param dont_care_minoccurs Dont set errors on validating minoccurs, 
         *              (Parent will order this in a case of choice)
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_deserialize(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env,
            axiom_node_t** dp_parent,
            axis2_bool_t *dp_is_early_node_valid,
            axis2_bool_t dont_care_minoccurs);

        /**
         * Deserialize an XML to adb objects
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @param dp_parent double pointer to the parent node to deserialize
         * @param dp_is_early_node_valid double pointer to a flag (is_early_node_valid?)
         * @param dont_care_minoccurs Dont set errors on validating minoccurs,
         *              (Parent will order this in a case of choice)
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopResponse_deserialize_obj(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env,
            axiom_node_t** dp_parent,
            axis2_bool_t *dp_is_early_node_valid,
            axis2_bool_t dont_care_minoccurs);
                            
            

       /**
         * Declare namespace in the most parent node 
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @param parent_element parent element
         * @param namespaces hash of namespace uri to prefix
         * @param next_ns_index pointer to an int which contain the next namespace index
         */
       void AXIS2_CALL
       adb_CopResponse_declare_parent_namespaces(
                    adb_CopResponse_t* _CopResponse,
                    const axutil_env_t *env, axiom_element_t *parent_element,
                    axutil_hash_t *namespaces, int *next_ns_index);

        

        /**
         * Wrapper for the serialization function, will invoke the extension mapper instead
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @param CopResponse_om_node node to serialize from
         * @param CopResponse_om_element parent element to serialize from
         * @param tag_closed whether the parent tag is closed or not
         * @param namespaces hash of namespace uri to prefix
         * @param next_ns_index an int which contain the next namespace index
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axiom_node_t* AXIS2_CALL
        adb_CopResponse_serialize(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env,
            axiom_node_t* CopResponse_om_node, axiom_element_t *CopResponse_om_element, int tag_closed, axutil_hash_t *namespaces, int *next_ns_index);

        /**
         * Serialize to an XML from the adb objects
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @param CopResponse_om_node node to serialize from
         * @param CopResponse_om_element parent element to serialize from
         * @param tag_closed whether the parent tag is closed or not
         * @param namespaces hash of namespace uri to prefix
         * @param next_ns_index an int which contain the next namespace index
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axiom_node_t* AXIS2_CALL
        adb_CopResponse_serialize_obj(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env,
            axiom_node_t* CopResponse_om_node, axiom_element_t *CopResponse_om_element, int tag_closed, axutil_hash_t *namespaces, int *next_ns_index);

        /**
         * Check whether the adb_CopResponse is a particle class (E.g. group, inner sequence)
         * @return whether this is a particle class.
         */
        axis2_bool_t AXIS2_CALL
        adb_CopResponse_is_particle();

        /******************************* Alternatives for Create and Free functions *********************************/

        

        /**
         * Constructor for creating adb_CopResponse_t
         * @param env pointer to environment struct
         * @param _result axis2_bool_t
         * @param _errorMsg axis2_char_t*
         * @param _resultType axis2_char_t*
         * @param _resultData axis2_char_t*
         * @return newly created adb_CopResponse_t object
         */
        adb_CopResponse_t* AXIS2_CALL
        adb_CopResponse_create_with_values(
            const axutil_env_t *env,
                axis2_bool_t _result,
                axis2_char_t* _errorMsg,
                axis2_char_t* _resultType,
                axis2_char_t* _resultData);

        


                /**
                 * Free adb_CopResponse_t object and return the property value.
                 * You can use this to free the adb object as returning the property value. If there are
                 * many properties, it will only return the first property. Other properties will get freed with the adb object.
                 * @param  _CopResponse adb_CopResponse_t object to free
                 * @param env pointer to environment struct
                 * @return the property value holded by the ADB object, if there are many properties only returns the first.
                 */
                axis2_bool_t AXIS2_CALL
                adb_CopResponse_free_popping_value(
                        adb_CopResponse_t* _CopResponse,
                        const axutil_env_t *env);
            

        /******************************* get the value by the property number  *********************************/
        /************NOTE: This method is introduced to resolve a problem in unwrapping mode *******************/

        
        

        /**
         * Getter for result by property number (1)
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return axis2_bool_t
         */
        axis2_bool_t AXIS2_CALL
        adb_CopResponse_get_property1(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

    
        

        /**
         * Getter for errorMsg by property number (2)
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopResponse_get_property2(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

    
        

        /**
         * Getter for resultType by property number (3)
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopResponse_get_property3(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

    
        

        /**
         * Getter for resultData by property number (4)
         * @param  _CopResponse adb_CopResponse_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopResponse_get_property4(
            adb_CopResponse_t* _CopResponse,
            const axutil_env_t *env);

    
     #ifdef __cplusplus
     }
     #endif

     #endif /* ADB_COPRESPONSE_H */
    

