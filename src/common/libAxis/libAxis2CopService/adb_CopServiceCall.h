

        #ifndef ADB_COPSERVICECALL_H
        #define ADB_COPSERVICECALL_H

       /**
        * adb_CopServiceCall.h
        *
        * This file was auto-generated from WSDL
        * by the Apache Axis2/Java version: 1.6.0  Built on : May 17, 2011 (04:21:18 IST)
        */

       /**
        *  adb_CopServiceCall class
        */

        

        #include <stdio.h>
        #include <axiom.h>
        #include <axis2_util.h>
        #include <axiom_soap.h>
        #include <axis2_client.h>

        #include "axis2_extension_mapper.h"

        #ifdef __cplusplus
        extern "C"
        {
        #endif

        #define ADB_DEFAULT_DIGIT_LIMIT 1024
        #define ADB_DEFAULT_NAMESPACE_PREFIX_LIMIT 64
        

        typedef struct adb_CopServiceCall adb_CopServiceCall_t;

        
        

        /******************************* Create and Free functions *********************************/

        /**
         * Constructor for creating adb_CopServiceCall_t
         * @param env pointer to environment struct
         * @return newly created adb_CopServiceCall_t object
         */
        adb_CopServiceCall_t* AXIS2_CALL
        adb_CopServiceCall_create(
            const axutil_env_t *env );

        /**
         * Wrapper for the "free" function, will invoke the extension mapper instead
         * @param  _CopServiceCall adb_CopServiceCall_t object to free
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_free (
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        /**
         * Free adb_CopServiceCall_t object
         * @param  _CopServiceCall adb_CopServiceCall_t object to free
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_free_obj (
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);



        /********************************** Getters and Setters **************************************/
        
        

        /**
         * Getter for command. 
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopServiceCall_get_command(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        /**
         * Setter for command.
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param arg_command axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_set_command(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env,
            const axis2_char_t*  arg_command);

        /**
         * Resetter for command
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_reset_command(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        
        

        /**
         * Getter for ip. 
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopServiceCall_get_ip(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        /**
         * Setter for ip.
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param arg_ip axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_set_ip(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env,
            const axis2_char_t*  arg_ip);

        /**
         * Resetter for ip
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_reset_ip(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        
        

        /**
         * Getter for port. 
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return int
         */
        int AXIS2_CALL
        adb_CopServiceCall_get_port(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        /**
         * Setter for port.
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param arg_port int
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_set_port(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env,
            const int  arg_port);

        /**
         * Resetter for port
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_reset_port(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        
        

        /**
         * Getter for body. 
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopServiceCall_get_body(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        /**
         * Setter for body.
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param arg_body axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_set_body(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env,
            const axis2_char_t*  arg_body);

        /**
         * Resetter for body
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_reset_body(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        
        

        /**
         * Getter for query. 
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopServiceCall_get_query(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        /**
         * Setter for query.
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param arg_query axis2_char_t*
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_set_query(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env,
            const axis2_char_t*  arg_query);

        /**
         * Resetter for query
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_reset_query(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

        


        /******************************* Checking and Setting NIL values *********************************/
        

        /**
         * NOTE: set_nil is only available for nillable properties
         */

        

        /**
         * Check whether command is nill
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_CopServiceCall_is_command_nil(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env);


        

        /**
         * Check whether ip is nill
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_CopServiceCall_is_ip_nil(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env);


        

        /**
         * Check whether port is nill
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_CopServiceCall_is_port_nil(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env);


        

        /**
         * Check whether body is nill
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_CopServiceCall_is_body_nil(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env);


        

        /**
         * Check whether query is nill
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return AXIS2_TRUE if the element is nil or AXIS2_FALSE otherwise
         */
        axis2_bool_t AXIS2_CALL
        adb_CopServiceCall_is_query_nil(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env);


        

        /**************************** Serialize and Deserialize functions ***************************/
        /*********** These functions are for use only inside the generated code *********************/

        
        /**
         * Wrapper for the deserialization function, will invoke the extension mapper instead
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param dp_parent double pointer to the parent node to deserialize
         * @param dp_is_early_node_valid double pointer to a flag (is_early_node_valid?)
         * @param dont_care_minoccurs Dont set errors on validating minoccurs, 
         *              (Parent will order this in a case of choice)
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_deserialize(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env,
            axiom_node_t** dp_parent,
            axis2_bool_t *dp_is_early_node_valid,
            axis2_bool_t dont_care_minoccurs);

        /**
         * Deserialize an XML to adb objects
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param dp_parent double pointer to the parent node to deserialize
         * @param dp_is_early_node_valid double pointer to a flag (is_early_node_valid?)
         * @param dont_care_minoccurs Dont set errors on validating minoccurs,
         *              (Parent will order this in a case of choice)
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_deserialize_obj(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env,
            axiom_node_t** dp_parent,
            axis2_bool_t *dp_is_early_node_valid,
            axis2_bool_t dont_care_minoccurs);
                            
            

       /**
         * Declare namespace in the most parent node 
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param parent_element parent element
         * @param namespaces hash of namespace uri to prefix
         * @param next_ns_index pointer to an int which contain the next namespace index
         */
       void AXIS2_CALL
       adb_CopServiceCall_declare_parent_namespaces(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env, axiom_element_t *parent_element,
                    axutil_hash_t *namespaces, int *next_ns_index);

        

        /**
         * Wrapper for the serialization function, will invoke the extension mapper instead
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param CopServiceCall_om_node node to serialize from
         * @param CopServiceCall_om_element parent element to serialize from
         * @param tag_closed whether the parent tag is closed or not
         * @param namespaces hash of namespace uri to prefix
         * @param next_ns_index an int which contain the next namespace index
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axiom_node_t* AXIS2_CALL
        adb_CopServiceCall_serialize(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env,
            axiom_node_t* CopServiceCall_om_node, axiom_element_t *CopServiceCall_om_element, int tag_closed, axutil_hash_t *namespaces, int *next_ns_index);

        /**
         * Serialize to an XML from the adb objects
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @param CopServiceCall_om_node node to serialize from
         * @param CopServiceCall_om_element parent element to serialize from
         * @param tag_closed whether the parent tag is closed or not
         * @param namespaces hash of namespace uri to prefix
         * @param next_ns_index an int which contain the next namespace index
         * @return AXIS2_SUCCESS on success, else AXIS2_FAILURE
         */
        axiom_node_t* AXIS2_CALL
        adb_CopServiceCall_serialize_obj(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env,
            axiom_node_t* CopServiceCall_om_node, axiom_element_t *CopServiceCall_om_element, int tag_closed, axutil_hash_t *namespaces, int *next_ns_index);

        /**
         * Check whether the adb_CopServiceCall is a particle class (E.g. group, inner sequence)
         * @return whether this is a particle class.
         */
        axis2_bool_t AXIS2_CALL
        adb_CopServiceCall_is_particle();

        /******************************* Alternatives for Create and Free functions *********************************/

        

        /**
         * Constructor for creating adb_CopServiceCall_t
         * @param env pointer to environment struct
         * @param _command axis2_char_t*
         * @param _ip axis2_char_t*
         * @param _port int
         * @param _body axis2_char_t*
         * @param _query axis2_char_t*
         * @return newly created adb_CopServiceCall_t object
         */
        adb_CopServiceCall_t* AXIS2_CALL
        adb_CopServiceCall_create_with_values(
            const axutil_env_t *env,
                axis2_char_t* _command,
                axis2_char_t* _ip,
                int _port,
                axis2_char_t* _body,
                axis2_char_t* _query);

        


                /**
                 * Free adb_CopServiceCall_t object and return the property value.
                 * You can use this to free the adb object as returning the property value. If there are
                 * many properties, it will only return the first property. Other properties will get freed with the adb object.
                 * @param  _CopServiceCall adb_CopServiceCall_t object to free
                 * @param env pointer to environment struct
                 * @return the property value holded by the ADB object, if there are many properties only returns the first.
                 */
                axis2_char_t* AXIS2_CALL
                adb_CopServiceCall_free_popping_value(
                        adb_CopServiceCall_t* _CopServiceCall,
                        const axutil_env_t *env);
            

        /******************************* get the value by the property number  *********************************/
        /************NOTE: This method is introduced to resolve a problem in unwrapping mode *******************/

        
        

        /**
         * Getter for command by property number (1)
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopServiceCall_get_property1(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

    
        

        /**
         * Getter for ip by property number (2)
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopServiceCall_get_property2(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

    
        

        /**
         * Getter for port by property number (3)
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return int
         */
        int AXIS2_CALL
        adb_CopServiceCall_get_property3(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

    
        

        /**
         * Getter for body by property number (4)
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopServiceCall_get_property4(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

    
        

        /**
         * Getter for query by property number (5)
         * @param  _CopServiceCall adb_CopServiceCall_t object
         * @param env pointer to environment struct
         * @return axis2_char_t*
         */
        axis2_char_t* AXIS2_CALL
        adb_CopServiceCall_get_property5(
            adb_CopServiceCall_t* _CopServiceCall,
            const axutil_env_t *env);

    
     #ifdef __cplusplus
     }
     #endif

     #endif /* ADB_COPSERVICECALL_H */
    

