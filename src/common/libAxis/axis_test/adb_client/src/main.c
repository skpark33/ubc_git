#include "axis2_stub_CopService.h"
#include "my_log.h"
int run(int port) ;
int 
main(
    int argc,
    char **argv) 
{
	int iteration = 0;
	int port=80;
	if(argc > 1)
	{
		port = atoi(argv[1]);
	}
	if(argc > 2)
	{
		iteration = atoi(argv[2]);
	}

	do {
		run(port);
	}while(--iteration > 0);

	return 0;
}

int run(int port) 
{
	char uri[256];
	sprintf(uri,"http://localhost:%d/axis2/services/CopService",port);

    axutil_env_t * env = NULL;
    axis2_char_t * operation = NULL;
    axis2_char_t * client_home = NULL;
    axis2_char_t * endpoint_uri = NULL;
    axis2_stub_t * stub = NULL;
    adb_TestResponse_t * Test_res = NULL;
    adb_Test_t * Test_in = NULL;
    axis2_char_t *res_val = NULL;


    endpoint_uri = (axis2_char_t*)uri;
    env = axutil_env_create_all("alltest.log", AXIS2_LOG_LEVEL_TRACE);
    
printf("Test Debug uri=%s\n", endpoint_uri);
    /* Set up deploy folder. */ 
    client_home = AXIS2_GETENV("AXIS2C_HOME");
    if (!client_home) {
		printf("ERROR: ENVVAR AXIS2C_HOME NOT FOUNDED\n");
		return -2;
	}
printf("Test Debug 2\n");

    stub = axis2_stub_create_CopService(env, client_home, endpoint_uri);
    Test_in = adb_Test_create(env);
    //adb_Test_set_arg_0_0(Test_in, env, 10);
    //adb_Test_set_arg_1_0(Test_in, env, 10);
printf("Test Debug 3\n");
    Test_res = axis2_stub_op_CopService_Test(stub, env, Test_in);
    if (!Test_res)
    {
        printf("Error: response NULL\n");
        return -1;
    }
printf("Test Debug 4\n");
    res_val = adb_TestResponse_get_TestResult(Test_res, env);
    printf("Test Result:%s\n", res_val);


printf("CopServcieCall Debug 1\n");

	adb_CopServiceCall_t*	call = adb_CopServiceCall_create(env);

	axis2_char_t* command = (axis2_char_t*)"CALL";
	axis2_char_t* ip = (axis2_char_t*)"100.100.100.100";
	axis2_char_t* body = (axis2_char_t*)"It's body";
	axis2_char_t* query = (axis2_char_t*)"It's query";
	int port2=14161;

	adb_CopServiceCall_set_command(call,env,command);
	adb_CopServiceCall_set_ip(call,env,ip);
	adb_CopServiceCall_set_port(call,env,port2);
	adb_CopServiceCall_set_body(call,env,body);
	adb_CopServiceCall_set_query(call,env,query);

    adb_CopServiceCallResponse_t * response = NULL;
    response = axis2_stub_op_CopService_CopServiceCall(stub, env, call);
    if (!response)
    {
        printf("Error: response NULL\n");
        return -1;
    }
printf("CopServiceCall Debug 2\n");
    adb_CopResponse_t* copResponse = adb_CopServiceCallResponse_get_copResponse(response, env);
	int result = (int)adb_CopResponse_get_result(copResponse,env);
	axis2_char_t* errorMsg = adb_CopResponse_get_errorMsg(copResponse,env);
	axis2_char_t* resultType = adb_CopResponse_get_resultType(copResponse,env);
	axis2_char_t* resultData = adb_CopResponse_get_resultData(copResponse,env);

printf("CopServiceCall Debug result=%d\n", result);
printf("CopServiceCall Debug errorMsg=%s\n", errorMsg);
printf("CopServiceCall Debug resultType=%s\n", resultType);
printf("CopServiceCall Debug resultData=%s\n", resultData);

    return 0;

}


