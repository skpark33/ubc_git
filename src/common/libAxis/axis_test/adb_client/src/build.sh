
gcc -fPIC -g -shared -olibCopServiceStub.so -I $AXIS2C_HOME/include/axis2-1.6.0/  -L$AXIS2C_HOME/lib \
    -laxutil \
    -laxis2_axiom \
    -laxis2_engine \
    -laxis2_parser \
    -lpthread \
    -laxis2_http_sender \
    -laxis2_http_receiver \
    -lguththila \
	adb_CopResponse.c \
	adb_CopResponse.h \
	adb_CopServiceCall.c \
	adb_CopServiceCall.h \
	adb_CopServiceCallResponse.c \
	adb_CopServiceCallResponse.h \
	adb_Test.c \
	adb_Test.h \
	adb_TestResponse.c \
	adb_TestResponse.h \
	axis2_extension_mapper.c \
	axis2_extension_mapper.h \
	axis2_stub_CopService.c \
	axis2_stub_CopService.h

	cp -f libCopServiceStub.so $INSTALL_LIBDIR/.
