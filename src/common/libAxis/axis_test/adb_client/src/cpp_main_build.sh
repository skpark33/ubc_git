
g++ -o CopServiceClient -fPIC -g  \
	-ldl -Wl,--rpath -Wl,$AXIS2C_HOME/lib \
	-I.\
	-I$AXIS2C_HOME/include/axis2-1.6.0/ \
	-L$AXIS2C_HOME/lib \
	-L. \
    -laxutil \
    -laxis2_axiom \
    -laxis2_engine \
    -laxis2_parser \
    -lpthread \
    -laxis2_http_sender \
    -laxis2_http_receiver \
    -lguththila \
	-lCopServiceStub \
	my_log.c \
    main.c 
