

        /**
         * adb_CopServiceCall.c
         *
         * This file was auto-generated from WSDL
         * by the Apache Axis2/C version: SNAPSHOT  Built on : Mar 10, 2008 (08:35:52 GMT+00:00)
         */

        #include "adb_CopServiceCall.h"
        
               /*
                * implmentation of the CopServiceCall|http://sqisoft.com/ element
                */
           


        struct adb_CopServiceCall
        {
            axis2_char_t *property_Type;

            
                axutil_qname_t* qname;
            axis2_char_t* property_command;

                
                axis2_bool_t is_valid_command;
            axis2_char_t* property_ip;

                
                axis2_bool_t is_valid_ip;
            int property_port;

                
                axis2_bool_t is_valid_port;
            axis2_char_t* property_body;

                
                axis2_bool_t is_valid_body;
            axis2_char_t* property_query;

                
                axis2_bool_t is_valid_query;
            
        };


       /************************* Private Function prototypes ********************************/
        

                axis2_status_t AXIS2_CALL
                adb_CopServiceCall_set_command_nil(
                        adb_CopServiceCall_t* _CopServiceCall,
                        const axutil_env_t *env);
            

                axis2_status_t AXIS2_CALL
                adb_CopServiceCall_set_ip_nil(
                        adb_CopServiceCall_t* _CopServiceCall,
                        const axutil_env_t *env);
            

                axis2_status_t AXIS2_CALL
                adb_CopServiceCall_set_port_nil(
                        adb_CopServiceCall_t* _CopServiceCall,
                        const axutil_env_t *env);
            

                axis2_status_t AXIS2_CALL
                adb_CopServiceCall_set_body_nil(
                        adb_CopServiceCall_t* _CopServiceCall,
                        const axutil_env_t *env);
            

                axis2_status_t AXIS2_CALL
                adb_CopServiceCall_set_query_nil(
                        adb_CopServiceCall_t* _CopServiceCall,
                        const axutil_env_t *env);
            


       /************************* Function Implmentations ********************************/
        adb_CopServiceCall_t* AXIS2_CALL
        adb_CopServiceCall_create(
            const axutil_env_t *env)
        {
            adb_CopServiceCall_t *_CopServiceCall = NULL;
            
                axutil_qname_t* qname = NULL;
            
            AXIS2_ENV_CHECK(env, NULL);

            _CopServiceCall = (adb_CopServiceCall_t *) AXIS2_MALLOC(env->
                allocator, sizeof(adb_CopServiceCall_t));

            if(NULL == _CopServiceCall)
            {
                AXIS2_ERROR_SET(env->error, AXIS2_ERROR_NO_MEMORY, AXIS2_FAILURE);
                return NULL;
            }

            memset(_CopServiceCall, 0, sizeof(adb_CopServiceCall_t));

            _CopServiceCall->property_Type = axutil_strdup(env, "adb_CopServiceCall");
            _CopServiceCall->property_command  = NULL;
                  _CopServiceCall->is_valid_command  = AXIS2_FALSE;
            _CopServiceCall->property_ip  = NULL;
                  _CopServiceCall->is_valid_ip  = AXIS2_FALSE;
            _CopServiceCall->is_valid_port  = AXIS2_FALSE;
            _CopServiceCall->property_body  = NULL;
                  _CopServiceCall->is_valid_body  = AXIS2_FALSE;
            _CopServiceCall->property_query  = NULL;
                  _CopServiceCall->is_valid_query  = AXIS2_FALSE;
            
                  qname =  axutil_qname_create (env,
                        "CopServiceCall",
                        "http://sqisoft.com/",
                        NULL);
                _CopServiceCall->qname = qname;
            

            return _CopServiceCall;
        }

        adb_CopServiceCall_t* AXIS2_CALL
        adb_CopServiceCall_create_with_values(
            const axutil_env_t *env,
                axis2_char_t* _command,
                axis2_char_t* _ip,
                int _port,
                axis2_char_t* _body,
                axis2_char_t* _query)
        {
            adb_CopServiceCall_t* adb_obj = NULL;
            axis2_status_t status = AXIS2_SUCCESS;

            adb_obj = adb_CopServiceCall_create(env);

            
              status = adb_CopServiceCall_set_command(
                                     adb_obj,
                                     env,
                                     _command);
              if(status == AXIS2_FAILURE) {
                  adb_CopServiceCall_free (adb_obj, env);
                  return NULL;
              }
            
              status = adb_CopServiceCall_set_ip(
                                     adb_obj,
                                     env,
                                     _ip);
              if(status == AXIS2_FAILURE) {
                  adb_CopServiceCall_free (adb_obj, env);
                  return NULL;
              }
            
              status = adb_CopServiceCall_set_port(
                                     adb_obj,
                                     env,
                                     _port);
              if(status == AXIS2_FAILURE) {
                  adb_CopServiceCall_free (adb_obj, env);
                  return NULL;
              }
            
              status = adb_CopServiceCall_set_body(
                                     adb_obj,
                                     env,
                                     _body);
              if(status == AXIS2_FAILURE) {
                  adb_CopServiceCall_free (adb_obj, env);
                  return NULL;
              }
            
              status = adb_CopServiceCall_set_query(
                                     adb_obj,
                                     env,
                                     _query);
              if(status == AXIS2_FAILURE) {
                  adb_CopServiceCall_free (adb_obj, env);
                  return NULL;
              }
            

            return adb_obj;
        }
      
        axis2_char_t* AXIS2_CALL
                adb_CopServiceCall_free_popping_value(
                        adb_CopServiceCall_t* _CopServiceCall,
                        const axutil_env_t *env)
                {
                    axis2_char_t* value;

                    
                    
                    value = _CopServiceCall->property_command;

                    _CopServiceCall->property_command = (axis2_char_t*)NULL;
                    adb_CopServiceCall_free(_CopServiceCall, env);

                    return value;
                }
            

        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_free(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env)
        {
            
            
            return adb_CopServiceCall_free_obj(
                _CopServiceCall,
                env);
            
        }

        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_free_obj(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env)
        {
            

            AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
            AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);

            if (_CopServiceCall->property_Type != NULL)
            {
              AXIS2_FREE(env->allocator, _CopServiceCall->property_Type);
            }

            adb_CopServiceCall_reset_command(_CopServiceCall, env);
            adb_CopServiceCall_reset_ip(_CopServiceCall, env);
            adb_CopServiceCall_reset_port(_CopServiceCall, env);
            adb_CopServiceCall_reset_body(_CopServiceCall, env);
            adb_CopServiceCall_reset_query(_CopServiceCall, env);
            
              if(_CopServiceCall->qname)
              {
                  axutil_qname_free (_CopServiceCall->qname, env);
                  _CopServiceCall->qname = NULL;
              }
            

            if(_CopServiceCall)
            {
                AXIS2_FREE(env->allocator, _CopServiceCall);
                _CopServiceCall = NULL;
            }

            return AXIS2_SUCCESS;
        }


        

        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_deserialize(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env,
                axiom_node_t **dp_parent,
                axis2_bool_t *dp_is_early_node_valid,
                axis2_bool_t dont_care_minoccurs)
        {
            
            
            return adb_CopServiceCall_deserialize_obj(
                _CopServiceCall,
                env,
                dp_parent,
                dp_is_early_node_valid,
                dont_care_minoccurs);
            
        }

        axis2_status_t AXIS2_CALL
        adb_CopServiceCall_deserialize_obj(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env,
                axiom_node_t **dp_parent,
                axis2_bool_t *dp_is_early_node_valid,
                axis2_bool_t dont_care_minoccurs)
        {
          axiom_node_t *parent = *dp_parent;
          
          axis2_status_t status = AXIS2_SUCCESS;
           
             const axis2_char_t* text_value = NULL;
             axutil_qname_t *qname = NULL;
          
            axutil_qname_t *element_qname = NULL; 
            
               axiom_node_t *first_node = NULL;
               axis2_bool_t is_early_node_valid = AXIS2_TRUE;
               axiom_node_t *current_node = NULL;
               axiom_element_t *current_element = NULL;
            
            AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
            AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);

            
              
              while(parent && axiom_node_get_node_type(parent, env) != AXIOM_ELEMENT)
              {
                  parent = axiom_node_get_next_sibling(parent, env);
              }
              if (NULL == parent)
              {
                /* This should be checked before everything */
                AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, 
                            "Failed in building adb object for CopServiceCall : "
                            "NULL element can not be passed to deserialize");
                return AXIS2_FAILURE;
              }
              

                    current_element = (axiom_element_t *)axiom_node_get_data_element(parent, env);
                    qname = axiom_element_get_qname(current_element, env, parent);
                    if (axutil_qname_equals(qname, env, _CopServiceCall-> qname))
                    {
                        
                          first_node = axiom_node_get_first_child(parent, env);
                          
                    }
                    else
                    {
                        AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, 
                              "Failed in building adb object for CopServiceCall : "
                              "Expected %s but returned %s",
                              axutil_qname_to_string(_CopServiceCall-> qname, env),
                              axutil_qname_to_string(qname, env));
                        
                        return AXIS2_FAILURE;
                    }
                    

                     
                     /*
                      * building command element
                      */
                     
                     
                     
                                   current_node = first_node;
                                   is_early_node_valid = AXIS2_FALSE;
                                   
                                   
                                    while(current_node && axiom_node_get_node_type(current_node, env) != AXIOM_ELEMENT)
                                    {
                                        current_node = axiom_node_get_next_sibling(current_node, env);
                                    }
                                    if(current_node != NULL)
                                    {
                                        current_element = (axiom_element_t *)axiom_node_get_data_element(current_node, env);
                                        qname = axiom_element_get_qname(current_element, env, current_node);
                                    }
                                   
                                 element_qname = axutil_qname_create(env, "command", "http://sqisoft.com/", NULL);
                                 

                           if ( 
                                (current_node   && current_element && (axutil_qname_equals(element_qname, env, qname))))
                           {
                              if( current_node   && current_element && (axutil_qname_equals(element_qname, env, qname)))
                              {
                                is_early_node_valid = AXIS2_TRUE;
                              }
                              
                                 
                                      text_value = axiom_element_get_text(current_element, env, current_node);
                                      if(text_value != NULL)
                                      {
                                            status = adb_CopServiceCall_set_command(_CopServiceCall, env,
                                                               text_value);
                                      }
                                      
                                      else
                                      {
                                            /*
                                             * axis2_qname_t *qname = NULL;
                                             * axiom_attribute_t *the_attri = NULL;
                                             * 
                                             * qname = axutil_qname_create(env, "nil", "http://www.w3.org/2001/XMLSchema-instance", "xsi");
                                             * the_attri = axiom_element_get_attribute(current_element, env, qname);
                                             */
                                            /* currently thereis a bug in the axiom_element_get_attribute, so we have to go to this bad method */

                                            axiom_attribute_t *the_attri = NULL;
                                            axis2_char_t *attrib_text = NULL;
                                            axutil_hash_t *attribute_hash = NULL;

                                            attribute_hash = axiom_element_get_all_attributes(current_element, env);

                                            attrib_text = NULL;
                                            if(attribute_hash)
                                            {
                                                 axutil_hash_index_t *hi;
                                                 void *val;
                                                 const void *key;
                                        
                                                 for (hi = axutil_hash_first(attribute_hash, env); hi; hi = axutil_hash_next(env, hi)) 
                                                 {
                                                     axutil_hash_this(hi, &key, NULL, &val);
                                                     
                                                     if(strstr((axis2_char_t*)key, "nil|http://www.w3.org/2001/XMLSchema-instance"))
                                                     {
                                                         the_attri = (axiom_attribute_t*)val;
                                                         break;
                                                     }
                                                 }
                                            }

                                            if(the_attri)
                                            {
                                                attrib_text = axiom_attribute_get_value(the_attri, env);
                                            }
                                            else
                                            {
                                                /* this is hoping that attribute is stored in "http://www.w3.org/2001/XMLSchema-instance", this happnes when name is in default namespace */
                                                attrib_text = axiom_element_get_attribute_value_by_name(current_element, env, "nil");
                                            }

                                            if(attrib_text && 0 == axutil_strcmp(attrib_text, "1"))
                                            {
                                                AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "NULL value is set to a non nillable element command");
                                                status = AXIS2_FAILURE;
                                            }
                                            else
                                            {
                                                /* after all, we found this is a empty string */
                                                status = adb_CopServiceCall_set_command(_CopServiceCall, env,
                                                                   "");
                                            }
                                      }
                                      
                                 if(AXIS2_FAILURE ==  status)
                                 {
                                     AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "failed in setting the value for command ");
                                     if(element_qname)
                                     {
                                         axutil_qname_free(element_qname, env);
                                     }
                                     return AXIS2_FAILURE;
                                 }
                              }
                           
                  if(element_qname)
                  {
                     axutil_qname_free(element_qname, env);
                     element_qname = NULL;
                  }
                 

                     
                     /*
                      * building ip element
                      */
                     
                     
                     
                                    /*
                                     * because elements are ordered this works fine
                                     */
                                  
                                   
                                   if(current_node != NULL && is_early_node_valid)
                                   {
                                       current_node = axiom_node_get_next_sibling(current_node, env);
                                       
                                       
                                        while(current_node && axiom_node_get_node_type(current_node, env) != AXIOM_ELEMENT)
                                        {
                                            current_node = axiom_node_get_next_sibling(current_node, env);
                                        }
                                        if(current_node != NULL)
                                        {
                                            current_element = (axiom_element_t *)axiom_node_get_data_element(current_node, env);
                                            qname = axiom_element_get_qname(current_element, env, current_node);
                                        }
                                       
                                   }
                                   is_early_node_valid = AXIS2_FALSE;
                                 
                                 element_qname = axutil_qname_create(env, "ip", "http://sqisoft.com/", NULL);
                                 

                           if ( 
                                (current_node   && current_element && (axutil_qname_equals(element_qname, env, qname))))
                           {
                              if( current_node   && current_element && (axutil_qname_equals(element_qname, env, qname)))
                              {
                                is_early_node_valid = AXIS2_TRUE;
                              }
                              
                                 
                                      text_value = axiom_element_get_text(current_element, env, current_node);
                                      if(text_value != NULL)
                                      {
                                            status = adb_CopServiceCall_set_ip(_CopServiceCall, env,
                                                               text_value);
                                      }
                                      
                                      else
                                      {
                                            /*
                                             * axis2_qname_t *qname = NULL;
                                             * axiom_attribute_t *the_attri = NULL;
                                             * 
                                             * qname = axutil_qname_create(env, "nil", "http://www.w3.org/2001/XMLSchema-instance", "xsi");
                                             * the_attri = axiom_element_get_attribute(current_element, env, qname);
                                             */
                                            /* currently thereis a bug in the axiom_element_get_attribute, so we have to go to this bad method */

                                            axiom_attribute_t *the_attri = NULL;
                                            axis2_char_t *attrib_text = NULL;
                                            axutil_hash_t *attribute_hash = NULL;

                                            attribute_hash = axiom_element_get_all_attributes(current_element, env);

                                            attrib_text = NULL;
                                            if(attribute_hash)
                                            {
                                                 axutil_hash_index_t *hi;
                                                 void *val;
                                                 const void *key;
                                        
                                                 for (hi = axutil_hash_first(attribute_hash, env); hi; hi = axutil_hash_next(env, hi)) 
                                                 {
                                                     axutil_hash_this(hi, &key, NULL, &val);
                                                     
                                                     if(strstr((axis2_char_t*)key, "nil|http://www.w3.org/2001/XMLSchema-instance"))
                                                     {
                                                         the_attri = (axiom_attribute_t*)val;
                                                         break;
                                                     }
                                                 }
                                            }

                                            if(the_attri)
                                            {
                                                attrib_text = axiom_attribute_get_value(the_attri, env);
                                            }
                                            else
                                            {
                                                /* this is hoping that attribute is stored in "http://www.w3.org/2001/XMLSchema-instance", this happnes when name is in default namespace */
                                                attrib_text = axiom_element_get_attribute_value_by_name(current_element, env, "nil");
                                            }

                                            if(attrib_text && 0 == axutil_strcmp(attrib_text, "1"))
                                            {
                                                AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "NULL value is set to a non nillable element ip");
                                                status = AXIS2_FAILURE;
                                            }
                                            else
                                            {
                                                /* after all, we found this is a empty string */
                                                status = adb_CopServiceCall_set_ip(_CopServiceCall, env,
                                                                   "");
                                            }
                                      }
                                      
                                 if(AXIS2_FAILURE ==  status)
                                 {
                                     AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "failed in setting the value for ip ");
                                     if(element_qname)
                                     {
                                         axutil_qname_free(element_qname, env);
                                     }
                                     return AXIS2_FAILURE;
                                 }
                              }
                           
                  if(element_qname)
                  {
                     axutil_qname_free(element_qname, env);
                     element_qname = NULL;
                  }
                 

                     
                     /*
                      * building port element
                      */
                     
                     
                     
                                    /*
                                     * because elements are ordered this works fine
                                     */
                                  
                                   
                                   if(current_node != NULL && is_early_node_valid)
                                   {
                                       current_node = axiom_node_get_next_sibling(current_node, env);
                                       
                                       
                                        while(current_node && axiom_node_get_node_type(current_node, env) != AXIOM_ELEMENT)
                                        {
                                            current_node = axiom_node_get_next_sibling(current_node, env);
                                        }
                                        if(current_node != NULL)
                                        {
                                            current_element = (axiom_element_t *)axiom_node_get_data_element(current_node, env);
                                            qname = axiom_element_get_qname(current_element, env, current_node);
                                        }
                                       
                                   }
                                   is_early_node_valid = AXIS2_FALSE;
                                 
                                 element_qname = axutil_qname_create(env, "port", "http://sqisoft.com/", NULL);
                                 

                           if ( 
                                (current_node   && current_element && (axutil_qname_equals(element_qname, env, qname))))
                           {
                              if( current_node   && current_element && (axutil_qname_equals(element_qname, env, qname)))
                              {
                                is_early_node_valid = AXIS2_TRUE;
                              }
                              
                                 
                                      text_value = axiom_element_get_text(current_element, env, current_node);
                                      if(text_value != NULL)
                                      {
                                            status = adb_CopServiceCall_set_port(_CopServiceCall, env,
                                                                   atoi(text_value));
                                      }
                                      
                                      else
                                      {
                                          AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "NULL value is set to a non nillable element port");
                                          status = AXIS2_FAILURE;
                                      }
                                      
                                 if(AXIS2_FAILURE ==  status)
                                 {
                                     AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "failed in setting the value for port ");
                                     if(element_qname)
                                     {
                                         axutil_qname_free(element_qname, env);
                                     }
                                     return AXIS2_FAILURE;
                                 }
                              }
                           
                              else if(!dont_care_minoccurs)
                              {
                                  if(element_qname)
                                  {
                                      axutil_qname_free(element_qname, env);
                                  }
                                  /* this is not a nillable element*/
                                  AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "non nillable or minOuccrs != 0 element port missing");
                                  return AXIS2_FAILURE;
                              }
                           
                  if(element_qname)
                  {
                     axutil_qname_free(element_qname, env);
                     element_qname = NULL;
                  }
                 

                     
                     /*
                      * building body element
                      */
                     
                     
                     
                                    /*
                                     * because elements are ordered this works fine
                                     */
                                  
                                   
                                   if(current_node != NULL && is_early_node_valid)
                                   {
                                       current_node = axiom_node_get_next_sibling(current_node, env);
                                       
                                       
                                        while(current_node && axiom_node_get_node_type(current_node, env) != AXIOM_ELEMENT)
                                        {
                                            current_node = axiom_node_get_next_sibling(current_node, env);
                                        }
                                        if(current_node != NULL)
                                        {
                                            current_element = (axiom_element_t *)axiom_node_get_data_element(current_node, env);
                                            qname = axiom_element_get_qname(current_element, env, current_node);
                                        }
                                       
                                   }
                                   is_early_node_valid = AXIS2_FALSE;
                                 
                                 element_qname = axutil_qname_create(env, "body", "http://sqisoft.com/", NULL);
                                 

                           if ( 
                                (current_node   && current_element && (axutil_qname_equals(element_qname, env, qname))))
                           {
                              if( current_node   && current_element && (axutil_qname_equals(element_qname, env, qname)))
                              {
                                is_early_node_valid = AXIS2_TRUE;
                              }
                              
                                 
                                      text_value = axiom_element_get_text(current_element, env, current_node);
                                      if(text_value != NULL)
                                      {
                                            status = adb_CopServiceCall_set_body(_CopServiceCall, env,
                                                               text_value);
                                      }
                                      
                                      else
                                      {
                                            /*
                                             * axis2_qname_t *qname = NULL;
                                             * axiom_attribute_t *the_attri = NULL;
                                             * 
                                             * qname = axutil_qname_create(env, "nil", "http://www.w3.org/2001/XMLSchema-instance", "xsi");
                                             * the_attri = axiom_element_get_attribute(current_element, env, qname);
                                             */
                                            /* currently thereis a bug in the axiom_element_get_attribute, so we have to go to this bad method */

                                            axiom_attribute_t *the_attri = NULL;
                                            axis2_char_t *attrib_text = NULL;
                                            axutil_hash_t *attribute_hash = NULL;

                                            attribute_hash = axiom_element_get_all_attributes(current_element, env);

                                            attrib_text = NULL;
                                            if(attribute_hash)
                                            {
                                                 axutil_hash_index_t *hi;
                                                 void *val;
                                                 const void *key;
                                        
                                                 for (hi = axutil_hash_first(attribute_hash, env); hi; hi = axutil_hash_next(env, hi)) 
                                                 {
                                                     axutil_hash_this(hi, &key, NULL, &val);
                                                     
                                                     if(strstr((axis2_char_t*)key, "nil|http://www.w3.org/2001/XMLSchema-instance"))
                                                     {
                                                         the_attri = (axiom_attribute_t*)val;
                                                         break;
                                                     }
                                                 }
                                            }

                                            if(the_attri)
                                            {
                                                attrib_text = axiom_attribute_get_value(the_attri, env);
                                            }
                                            else
                                            {
                                                /* this is hoping that attribute is stored in "http://www.w3.org/2001/XMLSchema-instance", this happnes when name is in default namespace */
                                                attrib_text = axiom_element_get_attribute_value_by_name(current_element, env, "nil");
                                            }

                                            if(attrib_text && 0 == axutil_strcmp(attrib_text, "1"))
                                            {
                                                AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "NULL value is set to a non nillable element body");
                                                status = AXIS2_FAILURE;
                                            }
                                            else
                                            {
                                                /* after all, we found this is a empty string */
                                                status = adb_CopServiceCall_set_body(_CopServiceCall, env,
                                                                   "");
                                            }
                                      }
                                      
                                 if(AXIS2_FAILURE ==  status)
                                 {
                                     AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "failed in setting the value for body ");
                                     if(element_qname)
                                     {
                                         axutil_qname_free(element_qname, env);
                                     }
                                     return AXIS2_FAILURE;
                                 }
                              }
                           
                  if(element_qname)
                  {
                     axutil_qname_free(element_qname, env);
                     element_qname = NULL;
                  }
                 

                     
                     /*
                      * building query element
                      */
                     
                     
                     
                                    /*
                                     * because elements are ordered this works fine
                                     */
                                  
                                   
                                   if(current_node != NULL && is_early_node_valid)
                                   {
                                       current_node = axiom_node_get_next_sibling(current_node, env);
                                       
                                       
                                        while(current_node && axiom_node_get_node_type(current_node, env) != AXIOM_ELEMENT)
                                        {
                                            current_node = axiom_node_get_next_sibling(current_node, env);
                                        }
                                        if(current_node != NULL)
                                        {
                                            current_element = (axiom_element_t *)axiom_node_get_data_element(current_node, env);
                                            qname = axiom_element_get_qname(current_element, env, current_node);
                                        }
                                       
                                   }
                                   is_early_node_valid = AXIS2_FALSE;
                                 
                                 element_qname = axutil_qname_create(env, "query", "http://sqisoft.com/", NULL);
                                 

                           if ( 
                                (current_node   && current_element && (axutil_qname_equals(element_qname, env, qname))))
                           {
                              if( current_node   && current_element && (axutil_qname_equals(element_qname, env, qname)))
                              {
                                is_early_node_valid = AXIS2_TRUE;
                              }
                              
                                 
                                      text_value = axiom_element_get_text(current_element, env, current_node);
                                      if(text_value != NULL)
                                      {
                                            status = adb_CopServiceCall_set_query(_CopServiceCall, env,
                                                               text_value);
                                      }
                                      
                                      else
                                      {
                                            /*
                                             * axis2_qname_t *qname = NULL;
                                             * axiom_attribute_t *the_attri = NULL;
                                             * 
                                             * qname = axutil_qname_create(env, "nil", "http://www.w3.org/2001/XMLSchema-instance", "xsi");
                                             * the_attri = axiom_element_get_attribute(current_element, env, qname);
                                             */
                                            /* currently thereis a bug in the axiom_element_get_attribute, so we have to go to this bad method */

                                            axiom_attribute_t *the_attri = NULL;
                                            axis2_char_t *attrib_text = NULL;
                                            axutil_hash_t *attribute_hash = NULL;

                                            attribute_hash = axiom_element_get_all_attributes(current_element, env);

                                            attrib_text = NULL;
                                            if(attribute_hash)
                                            {
                                                 axutil_hash_index_t *hi;
                                                 void *val;
                                                 const void *key;
                                        
                                                 for (hi = axutil_hash_first(attribute_hash, env); hi; hi = axutil_hash_next(env, hi)) 
                                                 {
                                                     axutil_hash_this(hi, &key, NULL, &val);
                                                     
                                                     if(strstr((axis2_char_t*)key, "nil|http://www.w3.org/2001/XMLSchema-instance"))
                                                     {
                                                         the_attri = (axiom_attribute_t*)val;
                                                         break;
                                                     }
                                                 }
                                            }

                                            if(the_attri)
                                            {
                                                attrib_text = axiom_attribute_get_value(the_attri, env);
                                            }
                                            else
                                            {
                                                /* this is hoping that attribute is stored in "http://www.w3.org/2001/XMLSchema-instance", this happnes when name is in default namespace */
                                                attrib_text = axiom_element_get_attribute_value_by_name(current_element, env, "nil");
                                            }

                                            if(attrib_text && 0 == axutil_strcmp(attrib_text, "1"))
                                            {
                                                AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "NULL value is set to a non nillable element query");
                                                status = AXIS2_FAILURE;
                                            }
                                            else
                                            {
                                                /* after all, we found this is a empty string */
                                                status = adb_CopServiceCall_set_query(_CopServiceCall, env,
                                                                   "");
                                            }
                                      }
                                      
                                 if(AXIS2_FAILURE ==  status)
                                 {
                                     AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "failed in setting the value for query ");
                                     if(element_qname)
                                     {
                                         axutil_qname_free(element_qname, env);
                                     }
                                     return AXIS2_FAILURE;
                                 }
                              }
                           
                  if(element_qname)
                  {
                     axutil_qname_free(element_qname, env);
                     element_qname = NULL;
                  }
                 
          return status;
       }

          axis2_bool_t AXIS2_CALL
          adb_CopServiceCall_is_particle()
          {
            
                 return AXIS2_FALSE;
              
          }


          void AXIS2_CALL
          adb_CopServiceCall_declare_parent_namespaces(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env, axiom_element_t *parent_element,
                    axutil_hash_t *namespaces, int *next_ns_index)
          {
            
                  /* Here this is an empty function, Nothing to declare */
                 
          }

        
        
        axiom_node_t* AXIS2_CALL
        adb_CopServiceCall_serialize(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env, axiom_node_t *parent, axiom_element_t *parent_element, int parent_tag_closed, axutil_hash_t *namespaces, int *next_ns_index)
        {
            
            
                return adb_CopServiceCall_serialize_obj(
                    _CopServiceCall, env, parent, parent_element, parent_tag_closed, namespaces, next_ns_index);
            
        }

        axiom_node_t* AXIS2_CALL
        adb_CopServiceCall_serialize_obj(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env, axiom_node_t *parent, axiom_element_t *parent_element, int parent_tag_closed, axutil_hash_t *namespaces, int *next_ns_index)
        {
            
            
         
         axiom_node_t* current_node = NULL;
         int tag_closed = 0;
         
                axiom_namespace_t *ns1 = NULL;

                axis2_char_t *qname_uri = NULL;
                axis2_char_t *qname_prefix = NULL;
                axis2_char_t *p_prefix = NULL;
                axis2_bool_t ns_already_defined;
            
                    axis2_char_t *text_value_1;
                    axis2_char_t *text_value_1_temp;
                    
                    axis2_char_t *text_value_2;
                    axis2_char_t *text_value_2_temp;
                    
                    axis2_char_t text_value_3[ADB_DEFAULT_DIGIT_LIMIT];
                    
                    axis2_char_t *text_value_4;
                    axis2_char_t *text_value_4_temp;
                    
                    axis2_char_t *text_value_5;
                    axis2_char_t *text_value_5_temp;
                    
               axis2_char_t *start_input_str = NULL;
               axis2_char_t *end_input_str = NULL;
               unsigned int start_input_str_len = 0;
               unsigned int end_input_str_len = 0;
            
            
               axiom_data_source_t *data_source = NULL;
               axutil_stream_t *stream = NULL;

             
                int next_ns_index_value = 0;
            

            AXIS2_ENV_CHECK(env, NULL);
            AXIS2_PARAM_CHECK(env->error, _CopServiceCall, NULL);
            
             
                    namespaces = axutil_hash_make(env);
                    next_ns_index = &next_ns_index_value;
                     
                           ns1 = axiom_namespace_create (env,
                                             "http://sqisoft.com/",
                                             "n"); 
                           axutil_hash_set(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING, axutil_strdup(env, "n"));
                       
                     
                    parent_element = axiom_element_create (env, NULL, "CopServiceCall", ns1 , &parent);
                    
                    
                    axiom_element_set_namespace(parent_element, env, ns1, parent);


            
                    data_source = axiom_data_source_create(env, parent, &current_node);
                    stream = axiom_data_source_get_stream(data_source, env);
                  
                       if(!(p_prefix = (axis2_char_t*)axutil_hash_get(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING)))
                       {
                           p_prefix = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof (axis2_char_t) * ADB_DEFAULT_NAMESPACE_PREFIX_LIMIT);
                           sprintf(p_prefix, "n%d", (*next_ns_index)++);
                           axutil_hash_set(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING, p_prefix);
                           
                           axiom_element_declare_namespace_assume_param_ownership(parent_element, env, axiom_namespace_create (env,
                                            "http://sqisoft.com/",
                                            p_prefix));
                       }
                      

                   if (!_CopServiceCall->is_valid_command)
                   {
                      
                           /* no need to complain for minoccurs=0 element */
                            
                          
                   }
                   else
                   {
                     start_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (4 + axutil_strlen(p_prefix) + 
                                  axutil_strlen("command"))); 
                                 
                                 /* axutil_strlen("<:>") + 1 = 4 */
                     end_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (5 + axutil_strlen(p_prefix) + axutil_strlen("command")));
                                  /* axutil_strlen("</:>") + 1 = 5 */
                                  
                     

                   
                   
                     
                     /*
                      * parsing command element
                      */

                    
                    
                            sprintf(start_input_str, "<%s%scommand>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                            
                        start_input_str_len = axutil_strlen(start_input_str);
                        sprintf(end_input_str, "</%s%scommand>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                        end_input_str_len = axutil_strlen(end_input_str);
                    
                           text_value_1 = _CopServiceCall->property_command;
                           
                           axutil_stream_write(stream, env, start_input_str, start_input_str_len);
                           
                            
                           text_value_1_temp = axutil_xml_quote_string(env, text_value_1, AXIS2_TRUE);
                           if (text_value_1_temp)
                           {
                               axutil_stream_write(stream, env, text_value_1_temp, axutil_strlen(text_value_1_temp));
                               AXIS2_FREE(env->allocator, text_value_1_temp);
                           }
                           else
                           {
                               axutil_stream_write(stream, env, text_value_1, axutil_strlen(text_value_1));
                           }
                           
                           axutil_stream_write(stream, env, end_input_str, end_input_str_len);
                           
                     
                     AXIS2_FREE(env->allocator,start_input_str);
                     AXIS2_FREE(env->allocator,end_input_str);
                 } 

                 
                       if(!(p_prefix = (axis2_char_t*)axutil_hash_get(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING)))
                       {
                           p_prefix = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof (axis2_char_t) * ADB_DEFAULT_NAMESPACE_PREFIX_LIMIT);
                           sprintf(p_prefix, "n%d", (*next_ns_index)++);
                           axutil_hash_set(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING, p_prefix);
                           
                           axiom_element_declare_namespace_assume_param_ownership(parent_element, env, axiom_namespace_create (env,
                                            "http://sqisoft.com/",
                                            p_prefix));
                       }
                      

                   if (!_CopServiceCall->is_valid_ip)
                   {
                      
                           /* no need to complain for minoccurs=0 element */
                            
                          
                   }
                   else
                   {
                     start_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (4 + axutil_strlen(p_prefix) + 
                                  axutil_strlen("ip"))); 
                                 
                                 /* axutil_strlen("<:>") + 1 = 4 */
                     end_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (5 + axutil_strlen(p_prefix) + axutil_strlen("ip")));
                                  /* axutil_strlen("</:>") + 1 = 5 */
                                  
                     

                   
                   
                     
                     /*
                      * parsing ip element
                      */

                    
                    
                            sprintf(start_input_str, "<%s%sip>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                            
                        start_input_str_len = axutil_strlen(start_input_str);
                        sprintf(end_input_str, "</%s%sip>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                        end_input_str_len = axutil_strlen(end_input_str);
                    
                           text_value_2 = _CopServiceCall->property_ip;
                           
                           axutil_stream_write(stream, env, start_input_str, start_input_str_len);
                           
                            
                           text_value_2_temp = axutil_xml_quote_string(env, text_value_2, AXIS2_TRUE);
                           if (text_value_2_temp)
                           {
                               axutil_stream_write(stream, env, text_value_2_temp, axutil_strlen(text_value_2_temp));
                               AXIS2_FREE(env->allocator, text_value_2_temp);
                           }
                           else
                           {
                               axutil_stream_write(stream, env, text_value_2, axutil_strlen(text_value_2));
                           }
                           
                           axutil_stream_write(stream, env, end_input_str, end_input_str_len);
                           
                     
                     AXIS2_FREE(env->allocator,start_input_str);
                     AXIS2_FREE(env->allocator,end_input_str);
                 } 

                 
                       if(!(p_prefix = (axis2_char_t*)axutil_hash_get(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING)))
                       {
                           p_prefix = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof (axis2_char_t) * ADB_DEFAULT_NAMESPACE_PREFIX_LIMIT);
                           sprintf(p_prefix, "n%d", (*next_ns_index)++);
                           axutil_hash_set(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING, p_prefix);
                           
                           axiom_element_declare_namespace_assume_param_ownership(parent_element, env, axiom_namespace_create (env,
                                            "http://sqisoft.com/",
                                            p_prefix));
                       }
                      

                   if (!_CopServiceCall->is_valid_port)
                   {
                      
                            
                            AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "Nil value found in non-nillable property port");
                            return NULL;
                          
                   }
                   else
                   {
                     start_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (4 + axutil_strlen(p_prefix) + 
                                  axutil_strlen("port"))); 
                                 
                                 /* axutil_strlen("<:>") + 1 = 4 */
                     end_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (5 + axutil_strlen(p_prefix) + axutil_strlen("port")));
                                  /* axutil_strlen("</:>") + 1 = 5 */
                                  
                     

                   
                   
                     
                     /*
                      * parsing port element
                      */

                    
                    
                            sprintf(start_input_str, "<%s%sport>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                            
                        start_input_str_len = axutil_strlen(start_input_str);
                        sprintf(end_input_str, "</%s%sport>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                        end_input_str_len = axutil_strlen(end_input_str);
                    
                               sprintf (text_value_3, AXIS2_PRINTF_INT32_FORMAT_SPECIFIER, _CopServiceCall->property_port);
                             
                           axutil_stream_write(stream, env, start_input_str, start_input_str_len);
                           
                           axutil_stream_write(stream, env, text_value_3, axutil_strlen(text_value_3));
                           
                           axutil_stream_write(stream, env, end_input_str, end_input_str_len);
                           
                     
                     AXIS2_FREE(env->allocator,start_input_str);
                     AXIS2_FREE(env->allocator,end_input_str);
                 } 

                 
                       if(!(p_prefix = (axis2_char_t*)axutil_hash_get(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING)))
                       {
                           p_prefix = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof (axis2_char_t) * ADB_DEFAULT_NAMESPACE_PREFIX_LIMIT);
                           sprintf(p_prefix, "n%d", (*next_ns_index)++);
                           axutil_hash_set(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING, p_prefix);
                           
                           axiom_element_declare_namespace_assume_param_ownership(parent_element, env, axiom_namespace_create (env,
                                            "http://sqisoft.com/",
                                            p_prefix));
                       }
                      

                   if (!_CopServiceCall->is_valid_body)
                   {
                      
                           /* no need to complain for minoccurs=0 element */
                            
                          
                   }
                   else
                   {
                     start_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (4 + axutil_strlen(p_prefix) + 
                                  axutil_strlen("body"))); 
                                 
                                 /* axutil_strlen("<:>") + 1 = 4 */
                     end_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (5 + axutil_strlen(p_prefix) + axutil_strlen("body")));
                                  /* axutil_strlen("</:>") + 1 = 5 */
                                  
                     

                   
                   
                     
                     /*
                      * parsing body element
                      */

                    
                    
                            sprintf(start_input_str, "<%s%sbody>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                            
                        start_input_str_len = axutil_strlen(start_input_str);
                        sprintf(end_input_str, "</%s%sbody>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                        end_input_str_len = axutil_strlen(end_input_str);
                    
                           text_value_4 = _CopServiceCall->property_body;
                           
                           axutil_stream_write(stream, env, start_input_str, start_input_str_len);
                           
                            
                           text_value_4_temp = axutil_xml_quote_string(env, text_value_4, AXIS2_TRUE);
                           if (text_value_4_temp)
                           {
                               axutil_stream_write(stream, env, text_value_4_temp, axutil_strlen(text_value_4_temp));
                               AXIS2_FREE(env->allocator, text_value_4_temp);
                           }
                           else
                           {
                               axutil_stream_write(stream, env, text_value_4, axutil_strlen(text_value_4));
                           }
                           
                           axutil_stream_write(stream, env, end_input_str, end_input_str_len);
                           
                     
                     AXIS2_FREE(env->allocator,start_input_str);
                     AXIS2_FREE(env->allocator,end_input_str);
                 } 

                 
                       if(!(p_prefix = (axis2_char_t*)axutil_hash_get(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING)))
                       {
                           p_prefix = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof (axis2_char_t) * ADB_DEFAULT_NAMESPACE_PREFIX_LIMIT);
                           sprintf(p_prefix, "n%d", (*next_ns_index)++);
                           axutil_hash_set(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING, p_prefix);
                           
                           axiom_element_declare_namespace_assume_param_ownership(parent_element, env, axiom_namespace_create (env,
                                            "http://sqisoft.com/",
                                            p_prefix));
                       }
                      

                   if (!_CopServiceCall->is_valid_query)
                   {
                      
                           /* no need to complain for minoccurs=0 element */
                            
                          
                   }
                   else
                   {
                     start_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (4 + axutil_strlen(p_prefix) + 
                                  axutil_strlen("query"))); 
                                 
                                 /* axutil_strlen("<:>") + 1 = 4 */
                     end_input_str = (axis2_char_t*)AXIS2_MALLOC(env->allocator, sizeof(axis2_char_t) *
                                 (5 + axutil_strlen(p_prefix) + axutil_strlen("query")));
                                  /* axutil_strlen("</:>") + 1 = 5 */
                                  
                     

                   
                   
                     
                     /*
                      * parsing query element
                      */

                    
                    
                            sprintf(start_input_str, "<%s%squery>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                            
                        start_input_str_len = axutil_strlen(start_input_str);
                        sprintf(end_input_str, "</%s%squery>",
                                 p_prefix?p_prefix:"",
                                 (p_prefix && axutil_strcmp(p_prefix, ""))?":":"");
                        end_input_str_len = axutil_strlen(end_input_str);
                    
                           text_value_5 = _CopServiceCall->property_query;
                           
                           axutil_stream_write(stream, env, start_input_str, start_input_str_len);
                           
                            
                           text_value_5_temp = axutil_xml_quote_string(env, text_value_5, AXIS2_TRUE);
                           if (text_value_5_temp)
                           {
                               axutil_stream_write(stream, env, text_value_5_temp, axutil_strlen(text_value_5_temp));
                               AXIS2_FREE(env->allocator, text_value_5_temp);
                           }
                           else
                           {
                               axutil_stream_write(stream, env, text_value_5, axutil_strlen(text_value_5));
                           }
                           
                           axutil_stream_write(stream, env, end_input_str, end_input_str_len);
                           
                     
                     AXIS2_FREE(env->allocator,start_input_str);
                     AXIS2_FREE(env->allocator,end_input_str);
                 } 

                 
                   if(namespaces)
                   {
                       axutil_hash_index_t *hi;
                       void *val;
                       for (hi = axutil_hash_first(namespaces, env); hi; hi = axutil_hash_next(env, hi)) 
                       {
                           axutil_hash_this(hi, NULL, NULL, &val);
                           AXIS2_FREE(env->allocator, val);
                       }
                       axutil_hash_free(namespaces, env);
                   }
                

            return parent;
        }


        

            /**
             * Getter for command by  Property Number 1
             */
            axis2_char_t* AXIS2_CALL
            adb_CopServiceCall_get_property1(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env)
            {
                return adb_CopServiceCall_get_command(_CopServiceCall,
                                             env);
            }

            /**
             * getter for command.
             */
            axis2_char_t* AXIS2_CALL
            adb_CopServiceCall_get_command(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env)
             {
                
                    AXIS2_ENV_CHECK(env, NULL);
                    AXIS2_PARAM_CHECK(env->error, _CopServiceCall, NULL);
                  

                return _CopServiceCall->property_command;
             }

            /**
             * setter for command
             */
            axis2_status_t AXIS2_CALL
            adb_CopServiceCall_set_command(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env,
                    const axis2_char_t*  arg_command)
             {
                

                AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
                AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
                
                if(_CopServiceCall->is_valid_command &&
                        arg_command == _CopServiceCall->property_command)
                {
                    
                    return AXIS2_SUCCESS; 
                }

                adb_CopServiceCall_reset_command(_CopServiceCall, env);

                
                if(NULL == arg_command)
                {
                    /* We are already done */
                    return AXIS2_SUCCESS;
                }
                _CopServiceCall->property_command = (axis2_char_t *)axutil_strdup(env, arg_command);
                        if(NULL == _CopServiceCall->property_command)
                        {
                            AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "Error allocating memeory for command");
                            return AXIS2_FAILURE;
                        }
                        _CopServiceCall->is_valid_command = AXIS2_TRUE;
                    
                return AXIS2_SUCCESS;
             }

             

           /**
            * resetter for command
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_reset_command(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               int i = 0;
               int count = 0;
               void *element = NULL;

               AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
               

               
            
                
                if(_CopServiceCall->property_command != NULL)
                {
                   
                   
                        AXIS2_FREE(env-> allocator, _CopServiceCall->property_command);
                     _CopServiceCall->property_command = NULL;
                }
            
                
                
                _CopServiceCall->is_valid_command = AXIS2_FALSE; 
               return AXIS2_SUCCESS;
           }

           /**
            * Check whether command is nill
            */
           axis2_bool_t AXIS2_CALL
           adb_CopServiceCall_is_command_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               AXIS2_ENV_CHECK(env, AXIS2_TRUE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_TRUE);
               
               return !_CopServiceCall->is_valid_command;
           }

           /**
            * Set command to nill (currently the same as reset)
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_set_command_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               return adb_CopServiceCall_reset_command(_CopServiceCall, env);
           }

           

            /**
             * Getter for ip by  Property Number 2
             */
            axis2_char_t* AXIS2_CALL
            adb_CopServiceCall_get_property2(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env)
            {
                return adb_CopServiceCall_get_ip(_CopServiceCall,
                                             env);
            }

            /**
             * getter for ip.
             */
            axis2_char_t* AXIS2_CALL
            adb_CopServiceCall_get_ip(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env)
             {
                
                    AXIS2_ENV_CHECK(env, NULL);
                    AXIS2_PARAM_CHECK(env->error, _CopServiceCall, NULL);
                  

                return _CopServiceCall->property_ip;
             }

            /**
             * setter for ip
             */
            axis2_status_t AXIS2_CALL
            adb_CopServiceCall_set_ip(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env,
                    const axis2_char_t*  arg_ip)
             {
                

                AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
                AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
                
                if(_CopServiceCall->is_valid_ip &&
                        arg_ip == _CopServiceCall->property_ip)
                {
                    
                    return AXIS2_SUCCESS; 
                }

                adb_CopServiceCall_reset_ip(_CopServiceCall, env);

                
                if(NULL == arg_ip)
                {
                    /* We are already done */
                    return AXIS2_SUCCESS;
                }
                _CopServiceCall->property_ip = (axis2_char_t *)axutil_strdup(env, arg_ip);
                        if(NULL == _CopServiceCall->property_ip)
                        {
                            AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "Error allocating memeory for ip");
                            return AXIS2_FAILURE;
                        }
                        _CopServiceCall->is_valid_ip = AXIS2_TRUE;
                    
                return AXIS2_SUCCESS;
             }

             

           /**
            * resetter for ip
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_reset_ip(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               int i = 0;
               int count = 0;
               void *element = NULL;

               AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
               

               
            
                
                if(_CopServiceCall->property_ip != NULL)
                {
                   
                   
                        AXIS2_FREE(env-> allocator, _CopServiceCall->property_ip);
                     _CopServiceCall->property_ip = NULL;
                }
            
                
                
                _CopServiceCall->is_valid_ip = AXIS2_FALSE; 
               return AXIS2_SUCCESS;
           }

           /**
            * Check whether ip is nill
            */
           axis2_bool_t AXIS2_CALL
           adb_CopServiceCall_is_ip_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               AXIS2_ENV_CHECK(env, AXIS2_TRUE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_TRUE);
               
               return !_CopServiceCall->is_valid_ip;
           }

           /**
            * Set ip to nill (currently the same as reset)
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_set_ip_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               return adb_CopServiceCall_reset_ip(_CopServiceCall, env);
           }

           

            /**
             * Getter for port by  Property Number 3
             */
            int AXIS2_CALL
            adb_CopServiceCall_get_property3(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env)
            {
                return adb_CopServiceCall_get_port(_CopServiceCall,
                                             env);
            }

            /**
             * getter for port.
             */
            int AXIS2_CALL
            adb_CopServiceCall_get_port(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env)
             {
                
                    AXIS2_ENV_CHECK(env, (int)0);
                    AXIS2_PARAM_CHECK(env->error, _CopServiceCall, (int)0);
                  

                return _CopServiceCall->property_port;
             }

            /**
             * setter for port
             */
            axis2_status_t AXIS2_CALL
            adb_CopServiceCall_set_port(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env,
                    const int  arg_port)
             {
                

                AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
                AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
                
                if(_CopServiceCall->is_valid_port &&
                        arg_port == _CopServiceCall->property_port)
                {
                    
                    return AXIS2_SUCCESS; 
                }

                adb_CopServiceCall_reset_port(_CopServiceCall, env);

                _CopServiceCall->property_port = arg_port;
                        _CopServiceCall->is_valid_port = AXIS2_TRUE;
                    
                return AXIS2_SUCCESS;
             }

             

           /**
            * resetter for port
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_reset_port(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               int i = 0;
               int count = 0;
               void *element = NULL;

               AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
               

               _CopServiceCall->is_valid_port = AXIS2_FALSE; 
               return AXIS2_SUCCESS;
           }

           /**
            * Check whether port is nill
            */
           axis2_bool_t AXIS2_CALL
           adb_CopServiceCall_is_port_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               AXIS2_ENV_CHECK(env, AXIS2_TRUE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_TRUE);
               
               return !_CopServiceCall->is_valid_port;
           }

           /**
            * Set port to nill (currently the same as reset)
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_set_port_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               return adb_CopServiceCall_reset_port(_CopServiceCall, env);
           }

           

            /**
             * Getter for body by  Property Number 4
             */
            axis2_char_t* AXIS2_CALL
            adb_CopServiceCall_get_property4(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env)
            {
                return adb_CopServiceCall_get_body(_CopServiceCall,
                                             env);
            }

            /**
             * getter for body.
             */
            axis2_char_t* AXIS2_CALL
            adb_CopServiceCall_get_body(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env)
             {
                
                    AXIS2_ENV_CHECK(env, NULL);
                    AXIS2_PARAM_CHECK(env->error, _CopServiceCall, NULL);
                  

                return _CopServiceCall->property_body;
             }

            /**
             * setter for body
             */
            axis2_status_t AXIS2_CALL
            adb_CopServiceCall_set_body(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env,
                    const axis2_char_t*  arg_body)
             {
                

                AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
                AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
                
                if(_CopServiceCall->is_valid_body &&
                        arg_body == _CopServiceCall->property_body)
                {
                    
                    return AXIS2_SUCCESS; 
                }

                adb_CopServiceCall_reset_body(_CopServiceCall, env);

                
                if(NULL == arg_body)
                {
                    /* We are already done */
                    return AXIS2_SUCCESS;
                }
                _CopServiceCall->property_body = (axis2_char_t *)axutil_strdup(env, arg_body);
                        if(NULL == _CopServiceCall->property_body)
                        {
                            AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "Error allocating memeory for body");
                            return AXIS2_FAILURE;
                        }
                        _CopServiceCall->is_valid_body = AXIS2_TRUE;
                    
                return AXIS2_SUCCESS;
             }

             

           /**
            * resetter for body
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_reset_body(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               int i = 0;
               int count = 0;
               void *element = NULL;

               AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
               

               
            
                
                if(_CopServiceCall->property_body != NULL)
                {
                   
                   
                        AXIS2_FREE(env-> allocator, _CopServiceCall->property_body);
                     _CopServiceCall->property_body = NULL;
                }
            
                
                
                _CopServiceCall->is_valid_body = AXIS2_FALSE; 
               return AXIS2_SUCCESS;
           }

           /**
            * Check whether body is nill
            */
           axis2_bool_t AXIS2_CALL
           adb_CopServiceCall_is_body_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               AXIS2_ENV_CHECK(env, AXIS2_TRUE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_TRUE);
               
               return !_CopServiceCall->is_valid_body;
           }

           /**
            * Set body to nill (currently the same as reset)
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_set_body_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               return adb_CopServiceCall_reset_body(_CopServiceCall, env);
           }

           

            /**
             * Getter for query by  Property Number 5
             */
            axis2_char_t* AXIS2_CALL
            adb_CopServiceCall_get_property5(
                adb_CopServiceCall_t* _CopServiceCall,
                const axutil_env_t *env)
            {
                return adb_CopServiceCall_get_query(_CopServiceCall,
                                             env);
            }

            /**
             * getter for query.
             */
            axis2_char_t* AXIS2_CALL
            adb_CopServiceCall_get_query(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env)
             {
                
                    AXIS2_ENV_CHECK(env, NULL);
                    AXIS2_PARAM_CHECK(env->error, _CopServiceCall, NULL);
                  

                return _CopServiceCall->property_query;
             }

            /**
             * setter for query
             */
            axis2_status_t AXIS2_CALL
            adb_CopServiceCall_set_query(
                    adb_CopServiceCall_t* _CopServiceCall,
                    const axutil_env_t *env,
                    const axis2_char_t*  arg_query)
             {
                

                AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
                AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
                
                if(_CopServiceCall->is_valid_query &&
                        arg_query == _CopServiceCall->property_query)
                {
                    
                    return AXIS2_SUCCESS; 
                }

                adb_CopServiceCall_reset_query(_CopServiceCall, env);

                
                if(NULL == arg_query)
                {
                    /* We are already done */
                    return AXIS2_SUCCESS;
                }
                _CopServiceCall->property_query = (axis2_char_t *)axutil_strdup(env, arg_query);
                        if(NULL == _CopServiceCall->property_query)
                        {
                            AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "Error allocating memeory for query");
                            return AXIS2_FAILURE;
                        }
                        _CopServiceCall->is_valid_query = AXIS2_TRUE;
                    
                return AXIS2_SUCCESS;
             }

             

           /**
            * resetter for query
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_reset_query(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               int i = 0;
               int count = 0;
               void *element = NULL;

               AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_FAILURE);
               

               
            
                
                if(_CopServiceCall->property_query != NULL)
                {
                   
                   
                        AXIS2_FREE(env-> allocator, _CopServiceCall->property_query);
                     _CopServiceCall->property_query = NULL;
                }
            
                
                
                _CopServiceCall->is_valid_query = AXIS2_FALSE; 
               return AXIS2_SUCCESS;
           }

           /**
            * Check whether query is nill
            */
           axis2_bool_t AXIS2_CALL
           adb_CopServiceCall_is_query_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               AXIS2_ENV_CHECK(env, AXIS2_TRUE);
               AXIS2_PARAM_CHECK(env->error, _CopServiceCall, AXIS2_TRUE);
               
               return !_CopServiceCall->is_valid_query;
           }

           /**
            * Set query to nill (currently the same as reset)
            */
           axis2_status_t AXIS2_CALL
           adb_CopServiceCall_set_query_nil(
                   adb_CopServiceCall_t* _CopServiceCall,
                   const axutil_env_t *env)
           {
               return adb_CopServiceCall_reset_query(_CopServiceCall, env);
           }

           

