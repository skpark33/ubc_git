
#include "my_log.h"


void
mylog_init()
{
	if(log_fp) {
		//fflush(log_fp);
		//fclose(log_fp);
		//log_fp=0 ;	
		return;
	}

	char logpath[256];
	sprintf(logpath, "/opt/axis2c/logs/myclient.log");;
	log_fp = fopen(logpath,"w");
	if(!log_fp){
		printf("log open failed\n");
		return ;
	}
	printf("log opened\n");

	char timestr[100];
	mylog_gettime(timestr,sizeof(timestr));
	fprintf(log_fp,"log_open at %s\n", timestr);
}

void
mylog(const char* text)
{
	mylog_init();
	if(log_fp){
		fprintf(log_fp,"%s\n", text);
		fflush(log_fp);
	}
}

void
mylog_close()
{
	if(log_fp) {
		char timestr[100];
		mylog_gettime(timestr,100);
		fprintf(log_fp,"log_close at %s\n", timestr);
		fflush(log_fp);
		fclose(log_fp);
		log_fp=0 ;	
	}
}

void
mylog_gettime(char* timestr, size_t bufsize)
{
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );

	memset((void*)timestr,(int)0x00,bufsize);
	sprintf ( timestr, "%s", asctime (timeinfo) );
}
