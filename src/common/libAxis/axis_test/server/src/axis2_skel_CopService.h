

    /**
     * axis2_skel_CopService.h
     *
     * This file was auto-generated from WSDL for "CopService|http://sqisoft.com/" service
     * by the Apache Axis2/C version: 1.6.0  Built on : May 17, 2011 (04:19:43 IST)
     * axis2_skel_CopService Axis2/C skeleton for the axisService- Header file
     */


	#include <axis2_svc_skeleton.h>
	#include <axutil_log_default.h>
	#include <axutil_error_default.h>
    #include <axutil_error.h>
	#include <axiom_text.h>
	#include <axiom_node.h>
	#include <axiom_element.h>
    #include <stdio.h>


   

	#ifdef __cplusplus
	extern "C" {
	#endif

     

		 
        /**
         * auto generated function declaration
         * for "Test|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _test of the axiom_node_t*
         *
         * @return axiom_node_t*
         */
        axiom_node_t* axis2_skel_CopService_Test(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              axiom_node_t* _test);


     

		 
        /**
         * auto generated function declaration
         * for "CopServiceCall|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _copServiceCall of the axiom_node_t*
         *
         * @return axiom_node_t*
         */
        axiom_node_t* axis2_skel_CopService_CopServiceCall(const axutil_env_t *env,axis2_msg_ctx_t *msg_ctx,
                                              axiom_node_t* _copServiceCall);


     

    /** we have to reserve some error codes for adb and for custom messages */
    #define AXIS2_SKEL_COPSERVICE_ERROR_CODES_START (AXIS2_ERROR_LAST + 2500)

    typedef enum 
    {
        AXIS2_SKEL_COPSERVICE_ERROR_NONE = AXIS2_SKEL_COPSERVICE_ERROR_CODES_START,
        
        AXIS2_SKEL_COPSERVICE_ERROR_LAST
    } axis2_skel_CopService_error_codes;

	#ifdef __cplusplus
	}
	#endif
    

