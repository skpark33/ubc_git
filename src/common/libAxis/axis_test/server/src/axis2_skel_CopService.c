

    /**
     * axis2_skel_CopService.c
     *
     * This file was auto-generated from WSDL for "CopService|http://sqisoft.com/" service
     * by the Apache Axis2/C version: 1.6.0  Built on : May 17, 2011 (04:19:43 IST)
     * axis2_skel_CopService Axis2/C skeleton for the axisService
     */

     #include "axis2_skel_CopService.h"

     

		 
        /**
         * auto generated function definition signature
         * for "Test|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _test of the axiom_node_t*
         *
         * @return axiom_node_t*
         */
        axiom_node_t* axis2_skel_CopService_Test(const axutil_env_t *env , axis2_msg_ctx_t *msg_ctx,
                                              axiom_node_t* _test )
        {
          /* TODO fill this with the necessary business logic */








          return (axiom_node_t*)NULL;
        }
     

		 
        /**
         * auto generated function definition signature
         * for "CopServiceCall|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _copServiceCall of the axiom_node_t*
         *
         * @return axiom_node_t*
         */
        axiom_node_t* axis2_skel_CopService_CopServiceCall(const axutil_env_t *env , axis2_msg_ctx_t *msg_ctx,
                                              axiom_node_t* _copServiceCall )
        {
          /* TODO fill this with the necessary business logic */
          return (axiom_node_t*)NULL;
        }
     
/*
axiom_node_t *
axis2_CopService_Test(
    const axutil_env_t * env,
    axiom_node_t * node)
{
    axiom_node_t *client_Testing_node = NULL;
    axiom_node_t *return_node = NULL;

    AXIS2_ENV_CHECK(env, NULL);

    if (node)
    {
        client_Testing_node = axiom_node_get_first_child(node, env);
        if (client_Testing_node &&
            axiom_node_get_node_type(client_Testing_node, env) == AXIOM_TEXT)
        {
            axiom_text_t *Testing =
                (axiom_text_t *)
                axiom_node_get_data_element(client_Testing_node, env);
            if (Testing && axiom_text_get_value(Testing, env))
            {
                const axis2_char_t *Testing_str =
                    axiom_text_get_value(Testing, env);
                printf("Client Tested saying \"%s\" \n", Testing_str);
                return_node = build_Testing_response(env, "Hello Client!");
            }
        }
    }
    else
    {
        AXIS2_ERROR_SET(env->error,
                        AXIS2_ERROR_SVC_SKEL_INVALID_XML_FORMAT_IN_REQUEST,
                        AXIS2_FAILURE);
        printf("ERROR: invalid XML in request\n");
        return_node = build_Testing_response(env, "Client! Who are you?");
    }

    return return_node;
}

axiom_node_t *
build_Testing_response(
    const axutil_env_t * env,
    axis2_char_t * Testing)
{
    axiom_node_t *Testing_om_node = NULL;
    axiom_element_t *Testing_om_ele = NULL;

    Testing_om_ele =
        axiom_element_create(env, NULL, "TestResponse", NULL,
                             &Testing_om_node);

    axiom_element_set_text(Testing_om_ele, env, Testing, Testing_om_node);

    return Testing_om_node;
}
*/
