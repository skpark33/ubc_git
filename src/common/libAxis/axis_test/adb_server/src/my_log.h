
#ifndef _my_log_h_
#define _my_log_h_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef __cplusplus
	extern "C" {
#endif
	static FILE* log_fp = 0;

	void mylog_gettime(char* timestr, size_t bufsize);
	void mylog_init();
	void mylog(const char* text);
	void mylog_close();


#ifdef __cplusplus
	}
#endif

#endif
