

        /**
         * adb_Test.c
         *
         * This file was auto-generated from WSDL
         * by the Apache Axis2/C version: SNAPSHOT  Built on : Mar 10, 2008 (08:35:52 GMT+00:00)
         */

        #include "adb_Test.h"
        
               /*
                * implmentation of the Test|http://sqisoft.com/ element
                */
           


        struct adb_Test
        {
            axis2_char_t *property_Type;

            
                axutil_qname_t* qname;
            
        };


       /************************* Private Function prototypes ********************************/
        


       /************************* Function Implmentations ********************************/
        adb_Test_t* AXIS2_CALL
        adb_Test_create(
            const axutil_env_t *env)
        {
            adb_Test_t *_Test = NULL;
            
                axutil_qname_t* qname = NULL;
            
            AXIS2_ENV_CHECK(env, NULL);

            _Test = (adb_Test_t *) AXIS2_MALLOC(env->
                allocator, sizeof(adb_Test_t));

            if(NULL == _Test)
            {
                AXIS2_ERROR_SET(env->error, AXIS2_ERROR_NO_MEMORY, AXIS2_FAILURE);
                return NULL;
            }

            memset(_Test, 0, sizeof(adb_Test_t));

            _Test->property_Type = (axis2_char_t*)axutil_strdup(env, "adb_Test");
            
                  qname =  axutil_qname_create (env,
                        "Test",
                        "http://sqisoft.com/",
                        NULL);
                _Test->qname = qname;
            

            return _Test;
        }

        adb_Test_t* AXIS2_CALL
        adb_Test_create_with_values(
            const axutil_env_t *env)
        {
            adb_Test_t* adb_obj = NULL;
            axis2_status_t status = AXIS2_SUCCESS;

            adb_obj = adb_Test_create(env);

            

            return adb_obj;
        }
      
        
                void* AXIS2_CALL
                adb_Test_free_popping_value(
                        adb_Test_t* _Test,
                        const axutil_env_t *env)
                {
                    adb_Test_free(_Test, env);
                    return NULL;
                }
            

        axis2_status_t AXIS2_CALL
        adb_Test_free(
                adb_Test_t* _Test,
                const axutil_env_t *env)
        {
            
            
            return adb_Test_free_obj(
                _Test,
                env);
            
        }

        axis2_status_t AXIS2_CALL
        adb_Test_free_obj(
                adb_Test_t* _Test,
                const axutil_env_t *env)
        {
            

            AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
            AXIS2_PARAM_CHECK(env->error, _Test, AXIS2_FAILURE);

            if (_Test->property_Type != NULL)
            {
              AXIS2_FREE(env->allocator, _Test->property_Type);
            }

            
              if(_Test->qname)
              {
                  axutil_qname_free (_Test->qname, env);
                  _Test->qname = NULL;
              }
            

            if(_Test)
            {
                AXIS2_FREE(env->allocator, _Test);
                _Test = NULL;
            }

            return AXIS2_SUCCESS;
        }


        

        axis2_status_t AXIS2_CALL
        adb_Test_deserialize(
                adb_Test_t* _Test,
                const axutil_env_t *env,
                axiom_node_t **dp_parent,
                axis2_bool_t *dp_is_early_node_valid,
                axis2_bool_t dont_care_minoccurs)
        {
            
            
            return adb_Test_deserialize_obj(
                _Test,
                env,
                dp_parent,
                dp_is_early_node_valid,
                dont_care_minoccurs);
            
        }

        axis2_status_t AXIS2_CALL
        adb_Test_deserialize_obj(
                adb_Test_t* _Test,
                const axutil_env_t *env,
                axiom_node_t **dp_parent,
                axis2_bool_t *dp_is_early_node_valid,
                axis2_bool_t dont_care_minoccurs)
        {
          axiom_node_t *parent = *dp_parent;
          
          axis2_status_t status = AXIS2_SUCCESS;
          
            axutil_qname_t *element_qname = NULL; 
            
            AXIS2_ENV_CHECK(env, AXIS2_FAILURE);
            AXIS2_PARAM_CHECK(env->error, _Test, AXIS2_FAILURE);

            
          return status;
       }

          axis2_bool_t AXIS2_CALL
          adb_Test_is_particle()
          {
            
                 return AXIS2_FALSE;
              
          }


          void AXIS2_CALL
          adb_Test_declare_parent_namespaces(
                    adb_Test_t* _Test,
                    const axutil_env_t *env, axiom_element_t *parent_element,
                    axutil_hash_t *namespaces, int *next_ns_index)
          {
            
                  /* Here this is an empty function, Nothing to declare */
                 
          }

        
        
        axiom_node_t* AXIS2_CALL
        adb_Test_serialize(
                adb_Test_t* _Test,
                const axutil_env_t *env, axiom_node_t *parent, axiom_element_t *parent_element, int parent_tag_closed, axutil_hash_t *namespaces, int *next_ns_index)
        {
            
            
                return adb_Test_serialize_obj(
                    _Test, env, parent, parent_element, parent_tag_closed, namespaces, next_ns_index);
            
        }

        axiom_node_t* AXIS2_CALL
        adb_Test_serialize_obj(
                adb_Test_t* _Test,
                const axutil_env_t *env, axiom_node_t *parent, axiom_element_t *parent_element, int parent_tag_closed, axutil_hash_t *namespaces, int *next_ns_index)
        {
            
            
         
         axiom_node_t* current_node = NULL;
         int tag_closed = 0;
         
                axiom_namespace_t *ns1 = NULL;

                axis2_char_t *qname_uri = NULL;
                axis2_char_t *qname_prefix = NULL;
                axis2_char_t *p_prefix = NULL;
                axis2_bool_t ns_already_defined;
            
            
               axiom_data_source_t *data_source = NULL;
               axutil_stream_t *stream = NULL;

             
                int next_ns_index_value = 0;
            

            AXIS2_ENV_CHECK(env, NULL);
            AXIS2_PARAM_CHECK(env->error, _Test, NULL);
            
             
                    namespaces = axutil_hash_make(env);
                    next_ns_index = &next_ns_index_value;
                     
                           ns1 = axiom_namespace_create (env,
                                             "http://sqisoft.com/",
                                             "n"); 
                           axutil_hash_set(namespaces, "http://sqisoft.com/", AXIS2_HASH_KEY_STRING, axutil_strdup(env, "n"));
                       
                     
                    parent_element = axiom_element_create (env, NULL, "Test", ns1 , &parent);
                    
                    
                    axiom_element_set_namespace(parent_element, env, ns1, parent);


            

            return parent;
        }


        

