

    /**
     * axis2_skel_CopService.c
     *
     * This file was auto-generated from WSDL for "CopService|http://sqisoft.com/" service
     * by the Apache Axis2/C version: 1.6.0  Built on : May 17, 2011 (04:19:43 IST)
     * axis2_skel_CopService Axis2/C skeleton for the axisService
     */

     #include "axis2_skel_CopService.h"
     #include "my_log.h"

     

		 
        /**
         * auto generated function definition signature
         * for "Test|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _test of the adb_Test_t*
         *
         * @return adb_TestResponse_t*
         */
        adb_TestResponse_t* axis2_skel_CopService_Test(const axutil_env_t *env , axis2_msg_ctx_t *msg_ctx,
                                              adb_Test_t* _test )
        {
          /* TODO fill this with the necessary business logic */

			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "axis2_skel_CopService_Test");


          adb_TestResponse_t* response = NULL;
		  response  = adb_TestResponse_create(env);

		  int aBufLen = strlen("Test Succeed") + 1;
		  char* aBuf = (char*)malloc(aBufLen);
		  memset(aBuf,0x00,aBufLen);
		  sprintf(aBuf,"%s", "Test Succeed");

		  axis2_char_t*  TestResultParam = aBuf;
		  adb_TestResponse_set_TestResult(response , env, TestResultParam);

          return (adb_TestResponse_t*)response ;
        }
     

		 
        /**
         * auto generated function definition signature
         * for "CopServiceCall|http://sqisoft.com/" operation.
         * @param env environment ( mandatory)* @param MessageContext the outmessage context
         * @param _copServiceCall of the adb_CopServiceCall_t*
         *
         * @return adb_CopServiceCallResponse_t*
         */
        adb_CopServiceCallResponse_t* axis2_skel_CopService_CopServiceCall(
				const axutil_env_t *env , 
				axis2_msg_ctx_t *msg_ctx,
                adb_CopServiceCall_t* _copServiceCall )
        {
          /* TODO fill this with the necessary business logic */
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "axis2_skel_CopService_CopServiceCall Invoked");

			axis2_char_t*	command = NULL;
			axis2_char_t*	ip 		= NULL;
			axis2_char_t*	body 	= NULL;
			axis2_char_t*	query 	= NULL;
			int				port 	= 0;

			command = adb_CopServiceCall_get_command(_copServiceCall,env);
			ip 		= adb_CopServiceCall_get_ip(_copServiceCall,env);
			body 	= adb_CopServiceCall_get_body(_copServiceCall,env);
			query 	= adb_CopServiceCall_get_query(_copServiceCall,env);
			port 	= adb_CopServiceCall_get_port(_copServiceCall,env);

			if(!command || !ip || !body || !query || !port){
				AXIS2_LOG_ERROR(env->log, AXIS2_LOG_SI, "input paramenter is null");
				AXIS2_ERROR_SET(env->error, AXIS2_ERROR_SVC_SKEL_INPUT_OM_NODE_NULL,AXIS2_FAILURE);
				return NULL;
			}

			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "command=%s", command);
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "ip=%s", ip);
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "port=%d", port);
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "body=%s", body);
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "query=%s", query);

			//execute action here !!!
			//
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "debug 1");
			adb_CopResponse_t* res_CopResponse = adb_CopResponse_create(env);
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "debug 2");
			adb_CopResponse_set_result(res_CopResponse,env,1);
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "debug 3");
			adb_CopResponse_set_errorMsg(res_CopResponse,env,"no error");
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "debug 4");
			adb_CopResponse_set_resultType(res_CopResponse,env,"test");
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "debug 5");
			adb_CopResponse_set_resultData(res_CopResponse,env,"This is dummy test");
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "debug 6");

			adb_CopServiceCallResponse_t*	response = adb_CopServiceCallResponse_create(env);
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "debug 7");
			adb_CopServiceCallResponse_set_copResponse(response,env,res_CopResponse);	
			AXIS2_LOG_DEBUG(env->log, AXIS2_LOG_SI, "debug 8");
			return (adb_CopServiceCallResponse_t*)response;
        }
     

