// fileServiceTestDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include <libHttpRequest/HttpDownload.h>
#include <FileServiceWrap/FileServiceWrapDll.h>

// CfileServiceTestDlg 대화 상자
class CfileServiceTestDlg : public CDialog
{
// 생성입니다.
public:
	CfileServiceTestDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FILESERVICETEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedStartExecute();
	afx_msg void OnBnClickedStopExecute();

	CEdit m_serverIp;
	CEdit m_port;
	CEdit m_command;
	CEdit m_argument;
	CEdit m_message;
	CButton m_btStop;
	CButton m_btExecute;

	CFileServiceWrap*	m_httpFile;


};
