// fileServiceTestDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "fileServiceTest.h"
#include "fileServiceTestDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CfileServiceTestDlg 대화 상자




CfileServiceTestDlg::CfileServiceTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CfileServiceTestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CfileServiceTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_URL, m_serverIp);
	DDX_Control(pDX, IDC_EDIT_SRCDIR, m_port);
	DDX_Control(pDX, IDC_EDIT_TARGETDIR, m_command);
	DDX_Control(pDX, IDC_FILENAME, m_argument);
	DDX_Control(pDX, IDC_EDIT_MESSAGE, m_message);
	DDX_Control(pDX, ID_STOP_DOWNLOAD, m_btStop);
	DDX_Control(pDX, ID_START_DOWNLOAD, m_btExecute);
}

BEGIN_MESSAGE_MAP(CfileServiceTestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CfileServiceTestDlg::OnBnClickedOk)
	ON_BN_CLICKED(ID_START_DOWNLOAD, &CfileServiceTestDlg::OnBnClickedStartExecute)
	ON_BN_CLICKED(ID_STOP_DOWNLOAD, &CfileServiceTestDlg::OnBnClickedStopExecute)
END_MESSAGE_MAP()


// CfileServiceTestDlg 메시지 처리기

BOOL CfileServiceTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.


	::SetWindowTextA(this->GetSafeHwnd(), "Download Tester");

	// TODO: 여기에 추가 초기화 작업을 추가합니다.


	m_serverIp.SetWindowTextA("211.232.57.176");
	m_port.SetWindowTextA("8080");
	m_command.SetWindowTextA("Login");
	m_argument.SetWindowTextA("");


	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CfileServiceTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CfileServiceTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CfileServiceTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CfileServiceTestDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//HTTPFileDownload();
	OnOK();
}

void CfileServiceTestDlg::OnBnClickedStartExecute()
{
	m_message.SetWindowText("");

	CString serverIp;
	m_serverIp.GetWindowTextA(serverIp);
	CString portStr;
	m_port.GetWindowTextA(portStr);
	int port = atoi(portStr);


	CString msg;
	msg.Format("Connect to %s:%d  ", serverIp, port);
	this->m_message.SetWindowTextA(msg);


	m_httpFile = new CFileServiceWrap(serverIp, port);



	CString command;
	m_command.GetWindowText(command);

	CString argument;
	m_argument.GetWindowText(argument);

	CString errMsg = msg;

	if(command == "Login") {
		if(!m_httpFile->Login("server", "rjtlrl2009")){
			errMsg += " Login Failed : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " Login Succeed";
		}
	}else
	if(command == "Logout") {
		if(!m_httpFile->Logout()){
			errMsg += " Logout Failed : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " Logout Succeed";
		}
	}else
	if(command == "CopyFile") {
		if(!m_httpFile->CopyFile("/Contents/abc/test.txt", "/Contents/def/test1.txt", true)){
			errMsg += " CopyFile Failed : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " CopyFile Succeed";
		}
	}else
	if(command == "MoveFile") {
		if(!m_httpFile->MoveFile("/Contents/abc/test2.txt", "/Contents/def/test2.txt")){
			errMsg += " MoveFile Failed : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " MoveFile Succeed";
		}
	}else
	if(command == "DeleteFile") {
		CString path = "/Contents/abcde/Wildlife.wmv";
		if(!argument.IsEmpty()) path = argument;

		if(!m_httpFile->DeleteFile(path)){
			errMsg += " DeleteFile Failed : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " DeleteFile Succeed";
		}
	}else
	if(command == "FileList") {
		CString path = "/Contents";
		if(!argument.IsEmpty()) path = argument;
		
		CString result;
		if(!m_httpFile->List(path, result, false)){
			errMsg += " FileList Failed : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " FileList Succeed";
			errMsg += result;
		}
		
		/*
		CFileServiceWrap::SFileList list;
		if(!m_httpFile->List(path, list, false)){
			errMsg += " FileList Failed : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " FileList Succeed ";
			for(int i=0; i<list.GetCount() ;i++)
			{
				  errMsg += "	strFullName="      ; 
				  errMsg += list[i].strFullName      ; 
				  errMsg += ",strName="          ; 
				  errMsg += list[i].strName          ; 
				  errMsg += ",strExtension="     ; 
				  errMsg += list[i].strExtension     ; 
				  errMsg += ",strCreationTime="  ; 
				  errMsg += list[i].strCreationTime  ; 
				  errMsg += ",strLastAccessTime="; 
				  errMsg += list[i].strLastAccessTime; 
				  errMsg += ",strLastWriteTime=" ; 
				  errMsg += list[i].strLastWriteTime ; 
				  errMsg += ",strDirectoryName=" ; 
				  errMsg += list[i].strDirectoryName ; 
				  errMsg += ",strLength="        ; 
				  errMsg += list[i].strLength        ; 
				  errMsg += ",strAttributes="    ; 
				  errMsg += list[i].strAttributes    ; 
				if(i>3) break;  // for test....
			}
		}
		*/
	} else	if(command == "FileInfo") {
		CString path = "/Contents/abc/test.txt";
		if(!argument.IsEmpty()) path = argument;
		
		CFileServiceWrap::SFileInfo result;
		if(!m_httpFile->FileInfo(path, result)){
			errMsg += " FileList Failed : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " FileList Succeed";
			  errMsg += "	strFullName="      ; 
			  errMsg += result.strFullName      ; 
			  errMsg += ",strName="          ; 
			  errMsg += result.strName          ; 
			  errMsg += ",strExtension="     ; 
			  errMsg += result.strExtension     ; 
			  errMsg += ",strCreationTime="  ; 
			  errMsg += result.strCreationTime  ; 
			  errMsg += ",strLastAccessTime="; 
			  errMsg += result.strLastAccessTime; 
			  errMsg += ",strLastWriteTime=" ; 
			  errMsg += result.strLastWriteTime ; 
			  errMsg += ",strDirectoryName=" ; 
			  errMsg += result.strDirectoryName ; 
			  errMsg += ",strLength="        ; 
			  errMsg += result.strLength        ; 
			  errMsg += ",strAttributes="    ; 
			  errMsg += result.strAttributes    ; 		
		}
	} else	if(command == "FilesInfo") {
		//CString path = "/Contents/abc/test.txt|/Contents/def/test2.txt|/Contents/abc/noexist.txt";
		//if(!argument.IsEmpty()) path = argument;

		CStringArray strFiles;

		strFiles.Add("/Contents/abc/test.txt");
		strFiles.Add("/Contents/abc/noexist.txt");
		strFiles.Add("/Contents/def/test2.txt");


		CFileServiceWrap::SFileList list;
		if(!m_httpFile->FilesInfo(strFiles, list)){
			errMsg += " FileList Failed : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " FileList Succeed";
			for(int i=0; i<list.GetCount() ;i++)
			{
				  errMsg += "	strFullName="      ; 
				  errMsg += list[i].strFullName      ; 
				  errMsg += ",strName="          ; 
				  errMsg += list[i].strName          ; 
				  errMsg += ",strExtension="     ; 
				  errMsg += list[i].strExtension     ; 
				  errMsg += ",strCreationTime="  ; 
				  errMsg += list[i].strCreationTime  ; 
				  errMsg += ",strLastAccessTime="; 
				  errMsg += list[i].strLastAccessTime; 
				  errMsg += ",strLastWriteTime=" ; 
				  errMsg += list[i].strLastWriteTime ; 
				  errMsg += ",strDirectoryName=" ; 
				  errMsg += list[i].strDirectoryName ; 
				  errMsg += ",strLength="        ; 
				  errMsg += list[i].strLength        ; 
				  errMsg += ",strAttributes="    ; 
				  errMsg += list[i].strAttributes    ; 
				if(i>3) break;  // for test....
			}
		}
	} else	if(command == "FileExist") {
		CString path = "/Contents/abcde/Wildlife.wmv";
		if(!argument.IsEmpty()) path = argument;
		
		if(!m_httpFile->IsExist(path)){
			errMsg += " File does not exist " + path;
		}else{
			errMsg += " File exist " + path;
		}
	} else	if(command == "CopyDirectory") {
		CString src = "/Contents/abc";
		CString dst = "/Contents/123";
		if(!argument.IsEmpty()) dst = argument;
		
		if(!m_httpFile->CopyDirectory(src,dst,false)){
			errMsg += " CopyDrectory error " + src + " to " +  dst + " : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " CopyDrectory succeed " + src + " to " +  dst;
		}

	} else	if(command == "MoveDirectory") {
		CString src = "/Contents/ABC";
		CString dst = "/Contents/abc";
		if(!argument.IsEmpty()) src = argument;
		
		if(!m_httpFile->MoveDirectory(src,dst)){
			errMsg += " MoveDrectory error " + src + " to " +  dst + " : ";
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " MoveDrectory succeed " + src + " to " +  dst;
		}
	} else	if(command == "DeleteDirectory") {
		CString src = "/Contents/abc";
		if(!argument.IsEmpty()) src = argument;
		
		if(!m_httpFile->DeleteDirectory(src)){
			errMsg += " DeleteDrectory error " + src ;
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " DeleteDrectory succeed " + src;
		}
	} else	if(command == "PutFile") {
		CString src = "/Contents/12345/김승우의 승승장구 120313 HDTV x264 720p-光風.avi";
		if(!argument.IsEmpty()) src = argument;
		
		CString local = "C:\\Project\\utv1\\Contents\\12345\\김승우의 승승장구 120313 HDTV x264 720p-光風.avi";
		if(!m_httpFile->PutFile(local,src,0,NULL)){
			errMsg += " PutFile error " + src ;
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " PutFile succeed " + src;
		}
	} else	if(command == "GetFile") {
		CString src = "/Contents/12345/김승우의 승승장구 120313 HDTV x264 720p-光風.avi";
		//CString src = "/Contents/abcde/Wildlife.wmv";
		if(!argument.IsEmpty()) src = argument;
		
		CString local = "C:\\Project\\utv1\\Contents\\12345\\김승우의 승승장구 120313 HDTV x264 720p-光風.avi";
		//CString local = "C:\\Project\\utv1\\Contents\\abcde\\Wildlife.wmv";
		if(!m_httpFile->GetFile(src, local,0,NULL)){
			errMsg += " GetFile error " + src ;
			errMsg +=  m_httpFile->GetErrorMsg();
		}else{
			errMsg += " GetFile succeed " + src;
		}
	}
	
	this->m_message.SetWindowTextA(errMsg);

	//delete m_httpFile;
	//m_httpFile = 0;

	m_btExecute.EnableWindow(true);
	m_btStop.EnableWindow(false);
}

void CfileServiceTestDlg::OnBnClickedStopExecute()
{
	m_btExecute.EnableWindow(true);
	m_btStop.EnableWindow(false);

}


