#pragma once

#include <afxtempl.h>

// 사용시 주의사항!!
//
// LIB_FILEHTTPSERVICE 사용시 static lib 형태로 링크합니다. (libFileHttpService.lib)
// 일반적으로 사용시 전처리문없이 그냥 사용하시며 dll 형태로 링크합니다 (FileServiceWrap.lib)
//

#ifdef LIB_FILEHTTPSERVICE
	#define FILESERVICE_WRAP_API
	#pragma message("------------ LIB_FILEHTTPSERVICE 사용으로 static lib (libFileHttpService.lib)를 링크 ------------")
#else
	#ifdef FILESERVICE_WRAP_EXPORTS
		#define FILESERVICE_WRAP_API __declspec(dllexport)
		#pragma message("---------------------------- FileServiceWrap dll을 export 모드 빌드 ----------------------------")
	#else
		#define FILESERVICE_WRAP_API __declspec(dllimport)
		#pragma message("---------------------------- FileServiceWrap dll을 import 모드 빌드 ----------------------------")
	#endif
#endif

class FILESERVICE_WRAP_API CFileServiceWrap
{
	CString	m_strUrl;
	UINT		m_nBuffSize;
	int		m_nTimeOut;
	int		m_nMaxTryCnt;

	CString m_strUserId;
	CString m_strUserPW;

	BOOL	m_bResult;
	CString m_strErrorMsg;
	static  CString GetSoapErrorMsg(int errorState);
	
	BOOL	m_bIsUserCancel;
	BOOL	IsUserCancel();

	void	CleanUp();
	BOOL	SetErrorMsg(CString strErrorMsg);
public:
	enum FS_PROGRESS_FLAG
	{
		FS_START = 0,
		FS_DOING,
		FS_STOP,
		FS_COMPLETE,
		FS_ERROR
	};

	// 파일 전송 진행 상태 이벤트
	interface IProgressHandler
	{
		virtual void ProgressEvent(FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath) = 0;
	};

	// 파일정보 구조체
	struct SFileInfo
	{
		BOOL	bIsDirectory     ;

        CString strFullName      ; // /temp/test.mp4
        CString strName          ; // test.mp4
        CString strExtension     ; // .mp4
        CString strCreationTime  ; // 2012-01-19T16:56:37.373+09:00
        CString strLastAccessTime; // 2012-01-19T16:56:37.373+09:00
        CString strLastWriteTime ; // 2012-01-26T13:28:46.685+09:00

		// file only
        CString strDirectoryName ; // /temp/
        CString strLength        ; // 574815760
        CString strAttributes    ; // 32(Archive)
	};

	// 파일 리스트용 자료구조
	typedef CArray<SFileInfo, SFileInfo&> SFileList;

// constructor
//	CFileServiceWrap(CString strUrl, int nBuffSize=1024*960, int nTimeOut=30); // 1024*960 보다 큰 경우 서버 오류남.
	CFileServiceWrap(CString strHttpServerIp="127.0.0.1", int nHttpPort=80, UINT nBuffSize=1024*960, int nTimeOut=30, int nMaxTryCnt=3); // 1024*960 보다 큰 경우 서버 오류남.
	~CFileServiceWrap();

// attribute
public:
	// 웹서비스 호출하기 위한 웹서버정보 설정
	CString		GetUrl() { return this->m_strUrl;}
//	void		SetUrl(CString strUrl);
	BOOL		SetHttpServerInfo(CString strHttpServerIp, int nHttpPort=80, int nBuffSize=1024*960, int nTimeOut=0, int nMaxTryCnt=3);

//	void		SetBuffSize(int nBuffSize);

// operation
public:
	// 웹서비스 연결 테스트용
	static		CString Test(CString strUrl);

	// 설정한 IP, PORT에 대하여 연결테스트를 진행한다.
	BOOL		Test();

	// return : TRUE/FALSE
	BOOL		Login(CString strID, CString strPW);
	BOOL		Logout();

	// return : TRUE/FALSE
	BOOL		FileInfo(CString strFile, SFileInfo& info);
	BOOL		FilesInfo(CStringArray& strFiles, SFileList& list);
	BOOL		IsExist(CString strFile);
	BOOL		List(CString strPath, SFileList& list, BOOL bTopDirectoryOnly=TRUE);
	BOOL		List(CString strPath, CString& listStr, BOOL bTopDirectoryOnly=TRUE);

	// return : success="OK" or "Fail" or Error String
	BOOL		MoveFile(CString strSrc, CString strDst);
	BOOL		CopyFile(CString strSrc, CString strDst, BOOL bOverWrite=TRUE);
	BOOL		DeleteFile(CString strFile);

	// return : TRUE/FALSE
	BOOL		PutFile(CString strLocal, CString strServer, ULONGLONG nOffset, IProgressHandler* pProgress);
	BOOL		GetFile(CString strServer, CString strLocal, ULONGLONG nOffset, IProgressHandler* pProgress);
	void		SetUserCancel(); // PutFile과 GetFile 진행도중 중지

	// 서버의 디렉토리에 대하여 copy, move, delete 를 함.
	// return : TRUE/FALSE
	BOOL		CopyDirectory(CString strSrc, CString strDst, BOOL bOverWrite);
	BOOL		MoveDirectory(CString strSrc, CString strDst);
	BOOL		DeleteDirectory(CString strPath);

	// Call 결과 FALSE인 경우 오류메세지
	CString		GetErrorMsg();

protected:
	BSTR		EncryptAlloc(CString& plain_text, bool validCheck=false);
	void		DecryptAlloc(void* res, CString& outval);

};
