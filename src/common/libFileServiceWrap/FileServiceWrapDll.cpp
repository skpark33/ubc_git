#include "StdAfx.h"

#ifndef LIB_FILEHTTPSERVICE
	#define FILESERVICE_WRAP_EXPORTS
#endif

#include "FileServiceWrapDll.h"
#include "WebRef.h"
#include "libXmlParser.h"
#ifndef LIB_FILEHTTPSERVICE
	#include "Dlls/common/TraceLog.h"
	#include "Dlls/common/ubcdefine.h"
#else
	#define TraceLog(_f_) 
	#define UBC_EXECUTE_PATH		_T("SQISoft\\UTV1.0\\execute\\")
	#define UBCVARS_INI				_T("UBCVariables.ini")
#endif

#include <ci/libAes256/ciElCryptoAes256.h>
#include <ci/libConfig/ciIni.h>


#define URL_ADDR_STR		_T("http://%s:%d/UBC_HTTP/FileService.asmx")
#define URL_SSL_ADDR_STR	_T("https://%s:%d/UBC_HTTP/FileService.asmx")

#define URL_AXIS2_ADDR_STR		_T("http://%s:%d/axis2/services/FileService")
#define URL_SSL_AXIS2_ADDR_STR	_T("https://%s:%d/axis2/services/FileService")

//CFileServiceWrap::CFileServiceWrap(CString strUrl, int nBuffSize, int nTimeOut)
//: m_bResult(FALSE)
//, m_bIsUserCancel(FALSE)
//{
//	m_strUrl = strUrl;
//	m_nBuffSize = nBuffSize;
//	m_nTimeOut = (nTimeOut <= 0 ? 30 : nTimeOut);
//}

CFileServiceWrap::CFileServiceWrap(CString strHttpServerIp, int nHttpPort/*=80*/, UINT nBuffSize, int nTimeOut/*=30*/, int nMaxTryCnt/*=3*/)
: m_bResult(FALSE)
, m_bIsUserCancel(FALSE)
, m_nBuffSize(1024*960)
//, m_nBuffSize(1024*960*2)  //skpark 2013.1.23 
, m_nTimeOut(60*3)
//, m_nTimeOut(60*3*2)  // skpark 2013.1.23
, m_nMaxTryCnt(3)
{
	if(!SetHttpServerInfo(strHttpServerIp, nHttpPort, nBuffSize, nTimeOut, nMaxTryCnt))
	{
		m_strErrorMsg = _T("CopHttpService Constructor fails!!");
		throw m_strErrorMsg;
	}
}

CFileServiceWrap::~CFileServiceWrap(void)
{
}

BOOL CFileServiceWrap::SetHttpServerInfo(CString strHttpServerIp, int nHttpPort/*=80*/, int nBuffSize/*=1024*960*/, int nTimeOut/*=0*/, int nMaxTryCnt/*=3*/)
{
	if(strHttpServerIp.IsEmpty()) return FALSE;
	if(nHttpPort <= 0 || nHttpPort > 65536) return FALSE;

	m_nBuffSize = nBuffSize;
	m_nTimeOut = (nTimeOut <= 0 ? (60 * 10): nTimeOut); // Default 10분 설정
	//m_nTimeOut = (nTimeOut <= 0 ? (60 * 30): nTimeOut); // skpark Default 30분 설정 으로 변경

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath_s(szModule, szDrive, MAX_PATH, szPath, MAX_PATH, szFilename, MAX_PATH, szExt, MAX_PATH);

	CString strPath;
	strPath.Format("%s\\%sdata\\%s", szDrive, UBC_EXECUTE_PATH, UBCVARS_INI);

	BOOL bUseSSL = GetPrivateProfileInt(_T("ROOT"), _T("USE_SSL"), 0, strPath);
	BOOL bUseAXIS2 = GetPrivateProfileInt(_T("ROOT"), _T("USE_AXIS2"), 0, strPath);

	char buf[1024] = {0};
	if(bUseSSL && GetPrivateProfileString(_T("SSLDomainNameList"), strHttpServerIp, _T(""), buf, 1024, strPath) > 0)
	{
		if(bUseAXIS2) {
			m_strUrl.Format(URL_SSL_AXIS2_ADDR_STR, buf, nHttpPort);
		}else{
			m_strUrl.Format(URL_SSL_ADDR_STR, buf, nHttpPort);
		}
	}
	else
	{
		if(bUseAXIS2) {
			m_strUrl.Format(URL_AXIS2_ADDR_STR, strHttpServerIp, nHttpPort);
		}else{
			m_strUrl.Format(URL_ADDR_STR, strHttpServerIp, nHttpPort);
		}
	}

	TraceLog((m_strUrl));

	m_nMaxTryCnt = nMaxTryCnt;

	return TRUE;
}

//CString CFileServiceWrap::GetUrl()
//{
//	return m_strUrl;
//}
//
//void CFileServiceWrap::SetUrl(CString strUrl)
//{
//	m_strUrl = strUrl;
//}
//
//void CFileServiceWrap::SetBuffSize(int nBuffSize)
//{
//	m_nBuffSize = nBuffSize;
//}

BOOL CFileServiceWrap::SetErrorMsg(CString strErrorMsg)
{
	m_bResult = FALSE;
	TraceLog((strErrorMsg));
	m_strErrorMsg = strErrorMsg;

	return m_bResult;
}

CString CFileServiceWrap::GetErrorMsg()
{
	return m_strErrorMsg;
}

CString CFileServiceWrap::GetSoapErrorMsg(int errorState)
{
// client error states
//enum SOAPCLIENT_ERROR
//{
//	SOAPCLIENT_SUCCESS=0,           // everything succeeded
//	SOAPCLIENT_INITIALIZE_ERROR,    // initialization failed -- most likely an MSXML installation problem
//	SOAPCLIENT_OUTOFMEMORY,         // out of memory
//	SOAPCLIENT_GENERATE_ERROR,      // failed in generating the response
//	SOAPCLIENT_CONNECT_ERROR,       // failed connecting to server
//	SOAPCLIENT_SEND_ERROR,          // failed in sending message
//	SOAPCLIENT_SERVER_ERROR,        // server error
//	SOAPCLIENT_SOAPFAULT,           // a SOAP Fault was returned by the server
//	SOAPCLIENT_PARSEFAULT_ERROR,    // failed in parsing SOAP fault
//	SOAPCLIENT_READ_ERROR,          // failed in reading response
//	SOAPCLIENT_PARSE_ERROR          // failed in parsing response
//};
	CString strErrorMsg;
	switch(errorState)
	{
	case SOAPCLIENT_SUCCESS          : strErrorMsg.Format(_T("SOAP error : everything succeeded [%d]"), errorState); break;
	case SOAPCLIENT_INITIALIZE_ERROR : strErrorMsg.Format(_T("SOAP error : initialization failed [%d]"), errorState); break;
	case SOAPCLIENT_OUTOFMEMORY      : strErrorMsg.Format(_T("SOAP error : out of memory [%d]"), errorState); break;
	case SOAPCLIENT_GENERATE_ERROR   : strErrorMsg.Format(_T("SOAP error : failed in generating the response [%d]"), errorState); break;
	case SOAPCLIENT_CONNECT_ERROR    : strErrorMsg.Format(_T("SOAP error : failed connecting to server [%d]"), errorState); break;
	case SOAPCLIENT_SEND_ERROR       : strErrorMsg.Format(_T("SOAP error : failed in sending message [%d]"), errorState); break;
	case SOAPCLIENT_SERVER_ERROR     : strErrorMsg.Format(_T("SOAP error : server error [%d]"), errorState); break;
	case SOAPCLIENT_SOAPFAULT        : strErrorMsg.Format(_T("SOAP error : a SOAP Fault was returned by the server [%d]"), errorState); break;
	case SOAPCLIENT_PARSEFAULT_ERROR : strErrorMsg.Format(_T("SOAP error : failed in parsing SOAP fault [%d]"), errorState); break;
	case SOAPCLIENT_READ_ERROR       : strErrorMsg.Format(_T("SOAP error : failed in reading response [%d]"), errorState); break;
	case SOAPCLIENT_PARSE_ERROR      : strErrorMsg.Format(_T("SOAP error : failed in parsing response [%d]"), errorState); break;
	default : strErrorMsg.Format(_T("SOAP error [%d]"), errorState); break;
	}

	return strErrorMsg;
}

void CFileServiceWrap::CleanUp()
{
	m_bResult = FALSE;
	m_strErrorMsg = _T("");
	m_bIsUserCancel = FALSE;
}

BSTR CFileServiceWrap::EncryptAlloc(CString& plain_text, bool validCheck)
{
	if(ciIniUtil::encriptMethod() == 1) {
		CString	cipher_text	= ciAes256Util::Encrypt(ciString(plain_text),validCheck).c_str();
		return cipher_text.AllocSysString();
	}
	return plain_text.AllocSysString();
}

void CFileServiceWrap::DecryptAlloc(void* res, CString& outval)
{
	FileService::FileResponse* response = (FileService::FileResponse*)res;

	if(ciIniUtil::encriptMethod() == 1			&& 
		response->result     == true				&&
		CString(response->resultType) == "ENCRYPTED_XML"	){
		CString	cipher_text(response->resultData);
		if(cipher_text.GetLength() > 0) {
			outval = ciAes256Util::Decrypt(ciString(cipher_text)).c_str();
			TraceLog(("Decryted ResultData=%s", outval));
			return ;
		}
	}
	outval = response->resultData;
	return ;
}


BOOL CFileServiceWrap::Login(CString strID, CString strPW)
{
	TraceLog(("Login (%s,%s,timeout=%d)", strID, strPW, m_nTimeOut));

	CString strID_NOGUID = strID;

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	
	//BSTR bstrID = strID.AllocSysString();
	//BSTR bstrPW = strPW.AllocSysString();
	BSTR bstrID = EncryptAlloc(strID,true);
	BSTR bstrPW = EncryptAlloc(strPW);

	HRESULT hResult = webRef.Login(bstrID, bstrPW, &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);

	if(hResult != NOERROR)
	{
		TraceLog(("Login failed"));
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		return SetErrorMsg(CString(response.errorMsg));
	}

	//CString strPath(_T("/"));
	//BSTR bstr2;
	//BSTR bstrPath = strPath.AllocSysString();
	//hResult = webRef.List(bstrPath, &bstr2);
	//SysFreeString(bstrPath);

	//if(hResult != NOERROR) return 0;

	//CString strList(bstr2);
	//return strList;

	m_strUserId = strID;
	m_strUserPW = strPW;

	return m_bResult;
}

BOOL CFileServiceWrap::Logout()
{
	TraceLog(("Logout"));

	m_strUserId.Empty();
	m_strUserPW.Empty();

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	HRESULT hResult = webRef.Logout();
	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = TRUE;

	return m_bResult;
}

CString CFileServiceWrap::Test(CString strUrl)
{
	TraceLog(("Test"));

	FileService::CFileService webRef;
	webRef.SetUrl(strUrl);

	BSTR bstr;
	HRESULT hResult = webRef.Test(&bstr);

	if(hResult != NOERROR)
	{
		return GetSoapErrorMsg(webRef.GetClientError());
	}

	return CString(bstr);
}

BOOL CFileServiceWrap::Test()
{
	TraceLog(("Test"));

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);

	BSTR bstr;
	HRESULT hResult = webRef.Test(&bstr);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	return TRUE;
}

BOOL CFileServiceWrap::FileInfo(CString strFilePath, SFileInfo& info)
{
	TraceLog(("FileInfo"));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrPath = strFilePath.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrPath = EncryptAlloc(strFilePath);

	try {
		HRESULT hResult = webRef.FileInfo(bstrID, bstrPW, bstrPath, &response);
		SysFreeString(bstrID);
		SysFreeString(bstrPW);
		SysFreeString(bstrPath);

		if(hResult != NOERROR)
		{
			return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
		}

		m_bResult = response.result;
		if(!m_bResult)
		{
			return SetErrorMsg(CString(response.errorMsg));
		}
	} catch (...) {
		return SetErrorMsg("Exception Occurred FileInfo failed");
	}

	//CString strFileInfo(response.resultData);
	CString strFileInfo;
	DecryptAlloc(&response, strFileInfo);

	CXmlParser xmlDoc;
	xmlDoc.LoadXML(strFileInfo);

	if(xmlDoc.SetXPath(_T("//FileInfo")) != XMLPARSER_SUCCESS)
	{
		return SetErrorMsg(xmlDoc.GetLastErrorMsg());
	}

	info.bIsDirectory      = FALSE;
	info.strDirectoryName  = xmlDoc.GetElement(_T("DirectoryName" ));
	info.strFullName       = xmlDoc.GetElement(_T("FullName"      ));
	info.strName           = xmlDoc.GetElement(_T("Name"          ));
	info.strExtension      = xmlDoc.GetElement(_T("Extension"     ));
	info.strLength         = xmlDoc.GetElement(_T("Length"        ));
	info.strCreationTime   = xmlDoc.GetElement(_T("CreationTime"  ));
	info.strLastAccessTime = xmlDoc.GetElement(_T("LastAccessTime"));
	info.strLastWriteTime  = xmlDoc.GetElement(_T("LastWriteTime" ));
	info.strAttributes     = xmlDoc.GetElement(_T("Attributes"    ));

	return m_bResult;
}

BOOL CFileServiceWrap::FilesInfo(CStringArray& strFiles, SFileList& list)
{

	CString strFileList;
	for(int i=0; i<strFiles.GetCount() ; i++)
	{
		strFileList += strFiles[i] + "|";
	}
	TraceLog(("FilesInfo(%s)", strFileList));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrFileList = strFileList.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrFileList = EncryptAlloc(strFileList);

	HRESULT hResult = webRef.FilesInfo(bstrID, bstrPW, bstrFileList, &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrFileList);

	int errState = webRef.GetClientError();
	if(hResult != NOERROR)
	{
		//if(SOAPCLIENT_SUCCESS != errState) { // skpark add 2013.1.21
			char errBuf[1024];
			sprintf(errBuf,"%s, hResult=%d", GetSoapErrorMsg(errState),hResult);

			return SetErrorMsg(errBuf);
		//}
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		//if(SOAPCLIENT_SUCCESS != errState) { // skpark add 2013.1.21
			char errBuf[1024];
			sprintf(errBuf,"%s, hResult2=%d", CString(response.errorMsg),hResult);
			return SetErrorMsg(errBuf);
		//}
	}

	//CString strList(response.resultData);
	CString strList;
	DecryptAlloc(&response, strList);

	CXmlParser xmlDoc;
	xmlDoc.LoadXML(strList);

	if(xmlDoc.SetXPath(_T("//FileInfo")) != XMLPARSER_SUCCESS)
	{
		return SetErrorMsg(xmlDoc.GetLastErrorMsg());
	}

	long lCnt = xmlDoc.GetSelCount();
	for(long i=0; i<lCnt ;i++)
	{
		SFileInfo info;

		info.bIsDirectory      = FALSE;
		info.strFullName       = xmlDoc.GetElement(_T("FullName"      ), i);
		info.strName           = xmlDoc.GetElement(_T("Name"          ), i);
		info.strExtension      = xmlDoc.GetElement(_T("Extension"     ), i);
		info.strCreationTime   = xmlDoc.GetElement(_T("CreationTime"  ), i);
		info.strLastAccessTime = xmlDoc.GetElement(_T("LastAccessTime"), i);
		info.strLastWriteTime  = xmlDoc.GetElement(_T("LastWriteTime" ), i);
		info.strDirectoryName  = xmlDoc.GetElement(_T("DirectoryName" ), i);
		info.strLength         = xmlDoc.GetElement(_T("Length"        ), i);
		info.strAttributes     = xmlDoc.GetElement(_T("Attributes"    ), i);

		list.Add(info);
	}

	m_bResult = TRUE;

	return m_bResult;
}

BOOL CFileServiceWrap::IsExist(CString strFilePath)
{
	TraceLog(("IsExist(%s)", strFilePath));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrPath = strFilePath.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrPath = EncryptAlloc(strFilePath);

	HRESULT hResult = webRef.FileExist(bstrID, bstrPW, bstrPath, &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrPath);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;

	TraceLog(("IsExist(%s=%d)", strFilePath, m_bResult));

	return m_bResult;
}

BOOL CFileServiceWrap::List(CString strPath, SFileList& list, BOOL bTopDirectoryOnly/*=TRUE*/)
{
	TraceLog(("List"));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrPath = strPath.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrPath = EncryptAlloc(strPath);


	HRESULT hResult = webRef.FileList(bstrID, bstrPW, bstrPath, (bTopDirectoryOnly ? true : false), &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrPath);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		return SetErrorMsg(CString(response.errorMsg));
	}

	//CString strList(response.resultData);
	CString strList;
	DecryptAlloc(&response, strList);

	CXmlParser xmlDoc;
	xmlDoc.LoadXML(strList);

	if(xmlDoc.SetXPath(_T("//DirectoryInfo")) != XMLPARSER_SUCCESS)
	{
		return SetErrorMsg(xmlDoc.GetLastErrorMsg());
	}

	LONG lCnt = xmlDoc.GetSelCount();
	for(long i=0; i<lCnt ;i++)
	{
		SFileInfo info;

		info.bIsDirectory      = TRUE;
		info.strFullName       = xmlDoc.GetElement(_T("FullName"      ), i);
		info.strName           = xmlDoc.GetElement(_T("Name"          ), i);
		info.strExtension      = xmlDoc.GetElement(_T("Extension"     ), i);
		info.strCreationTime   = xmlDoc.GetElement(_T("CreationTime"  ), i);
		info.strLastAccessTime = xmlDoc.GetElement(_T("LastAccessTime"), i);
		info.strLastWriteTime  = xmlDoc.GetElement(_T("LastWriteTime" ), i);
		//info.strDirectoryName  = xmlDoc.GetElement(_T("DirectoryName" ));
		//info.strLength         = xmlDoc.GetElement(_T("Length"        ));
		//info.strAttributes     = xmlDoc.GetElement(_T("Attributes"    ));

		list.Add(info);
	}

	if(xmlDoc.SetXPath(_T("//FileInfo")) != XMLPARSER_SUCCESS)
	{
		return SetErrorMsg(xmlDoc.GetLastErrorMsg());
	}

	lCnt = xmlDoc.GetSelCount();
	for(long i=0; i<lCnt ;i++)
	{
		SFileInfo info;

		info.bIsDirectory      = FALSE;
		info.strFullName       = xmlDoc.GetElement(_T("FullName"      ), i);
		info.strName           = xmlDoc.GetElement(_T("Name"          ), i);
		info.strExtension      = xmlDoc.GetElement(_T("Extension"     ), i);
		info.strCreationTime   = xmlDoc.GetElement(_T("CreationTime"  ), i);
		info.strLastAccessTime = xmlDoc.GetElement(_T("LastAccessTime"), i);
		info.strLastWriteTime  = xmlDoc.GetElement(_T("LastWriteTime" ), i);
		info.strDirectoryName  = xmlDoc.GetElement(_T("DirectoryName" ), i);
		info.strLength         = xmlDoc.GetElement(_T("Length"        ), i);
		info.strAttributes     = xmlDoc.GetElement(_T("Attributes"    ), i);

		list.Add(info);
	}

	m_bResult = TRUE;

	return m_bResult;
}

BOOL CFileServiceWrap::List(CString strPath, CString& listStr, BOOL bTopDirectoryOnly/*=TRUE*/)
{
	TraceLog(("List"));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrPath = strPath.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrPath = EncryptAlloc(strPath);

	HRESULT hResult = webRef.FileList(bstrID, bstrPW, bstrPath, (bTopDirectoryOnly ? true : false), &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrPath);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		return SetErrorMsg(CString(response.errorMsg));
	}

	//listStr = response.resultData;
	DecryptAlloc(&response, listStr);

	m_bResult = TRUE;

	return m_bResult;
}


void CFileServiceWrap::SetUserCancel()
{
	TraceLog(("SetUserCancel"));
	m_bIsUserCancel = TRUE;
}

BOOL CFileServiceWrap::IsUserCancel()
{
	return m_bIsUserCancel;
}

BOOL CFileServiceWrap::PutFile(CString strLocal, CString strServer, ULONGLONG nOffset, IProgressHandler* pProgress)
{
	TraceLog(("PutFile_ver3(%s,%ld)", strServer,nOffset));

	// 파일을 처음 올리는 경우 먼저 삭제부터 한다.
	if(nOffset == 0)
	{
		if(IsExist(strServer))
		{
			if(!DeleteFile(strServer)) return FALSE;
		}
	}

	CleanUp();

	TCHAR szErrorMsg[1024] = {0};

	if(!PathFileExists(strLocal))
	{
		return SetErrorMsg(_T("The file does not exist in the path"));
	}

	CFile f;
	CFileException err;

	if(!f.Open(strLocal, CFile::modeRead | CFile::typeBinary, &err))
	{
		err.GetErrorMessage(szErrorMsg, 1024);
		return SetErrorMsg(szErrorMsg);
	}

	ULONGLONG nFileLength = f.GetLength();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	BYTE* buf = new BYTE[m_nBuffSize];

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrID_without_GUID = EncryptAlloc(m_strUserId,false);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrServer = EncryptAlloc(strServer);

	m_bResult = TRUE;

	if(pProgress) pProgress->ProgressEvent(FS_START, nOffset, nFileLength, strLocal);

	int nTryCnt = 0;

	try
	{
		//ULONGLONG nOffset = 0;
		UINT nByteRead = 0;

		if(nOffset > 0)
		{
			f.Seek(nOffset, CFile::begin);
		}

		//while( (nByteRead = f.Read(buf, m_nBuffSize)) > 0 && !IsUserCancel())
		while(!IsUserCancel())
		{
			nByteRead = f.Read(buf, m_nBuffSize);

			ATLSOAP_BLOB context;
			context.size = nByteRead;
			context.data = buf;

			nTryCnt = 0;

			while(1)
			{
				FileService::FileResponse response;
				TraceLog(("before PutFile(%s,%ld)", strServer, nOffset));
				
				BSTR bstrID_ptr = (nOffset==0) ? bstrID : bstrID_without_GUID;
				HRESULT hResult = webRef.PutFile(bstrID_ptr, bstrPW, bstrServer, nOffset, nByteRead, context, &response);
				TraceLog(("after PutFile(result=%d)", hResult));
				if(hResult != NOERROR)
				{
					if( nTryCnt < m_nMaxTryCnt &&
					   (webRef.GetClientError() == SOAPCLIENT_SERVER_ERROR     ||
						webRef.GetClientError() == SOAPCLIENT_SOAPFAULT        ||
						webRef.GetClientError() == SOAPCLIENT_PARSEFAULT_ERROR ||
						webRef.GetClientError() == SOAPCLIENT_READ_ERROR       ||
						webRef.GetClientError() == SOAPCLIENT_PARSE_ERROR      ))
					{
						nTryCnt++;
						continue;
					}
					else
					{
						TraceLog(("Throw SOAP ERROR"));
						m_strErrorMsg = GetSoapErrorMsg(webRef.GetClientError());
						throw m_strErrorMsg;
					}
				}

				if(!response.result)
				{
					TraceLog(("Throw SERVER ERROR (errMsg=%s)", response.errorMsg));
					m_strErrorMsg = response.errorMsg;
					throw m_strErrorMsg;
				}

				break;
			}

			// init
			nOffset += nByteRead;
			nByteRead = 0;
			memset(buf, 0x00, m_nBuffSize);

			if(pProgress) pProgress->ProgressEvent(FS_DOING, nOffset, nFileLength, strLocal);

			if(nOffset >= nFileLength) break;
		}
	}
	catch(CFileException err)
	{
		err.GetErrorMessage(szErrorMsg, 1024);
		SetErrorMsg(szErrorMsg);
	}
	catch(CString err)
	{
		SetErrorMsg(err);
	}

	if(buf) delete buf;
	f.Close();
	SysFreeString(bstrID);
	SysFreeString(bstrID_without_GUID);
	SysFreeString(bstrPW);
	SysFreeString(bstrServer);

	if(IsUserCancel())
	{
		if(pProgress) pProgress->ProgressEvent(FS_STOP, nOffset, nFileLength, strLocal);
		return SetErrorMsg(_T("Stop by the user"));
	}

	if(pProgress) pProgress->ProgressEvent((m_bResult ? FS_COMPLETE:FS_ERROR ), nOffset, nFileLength, strLocal);

	return m_bResult;
}

BOOL CFileServiceWrap::GetFile(CString strServer, CString strLocal, ULONGLONG nOffset, IProgressHandler* pProgress)
{
	TraceLog(("GetFile"));

	SFileInfo info;
	if(!FileInfo(strServer, info)) return FALSE;

	CleanUp();
	TCHAR szErrorMsg[1024] = {0};

	CFile f;
	CFileException err;

	CString strLocalPath = strLocal;
	strLocalPath.Replace(_T("/"), _T("\\"));
	strLocalPath = strLocalPath.Left(strLocalPath.ReverseFind('\\')+1);
	::MakeSureDirectoryPathExists(strLocalPath);

	if(PathFileExists(strLocal) && nOffset > 0)
	{
		if(!f.Open(strLocal, CFile::modeWrite | CFile::typeBinary | CFile::shareDenyNone, &err))
		{
			err.GetErrorMessage(szErrorMsg, 1024);
			return SetErrorMsg(szErrorMsg);
		}
	}
	else
	{
		if(!f.Open(strLocal, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary | CFile::shareDenyNone, &err))
		{
			err.GetErrorMessage(szErrorMsg, 1024);
			return SetErrorMsg(szErrorMsg);
		}
	}

	ULONGLONG nFileLength = (ULONGLONG)_ttoi64(info.strLength);

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrID_without_GUID = EncryptAlloc(m_strUserId,false);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrServer = EncryptAlloc(strServer);

	m_bResult = TRUE;

	if(pProgress) pProgress->ProgressEvent(FS_START, nOffset, nFileLength, strLocal);

	int nTryCnt = 0;

	try
	{
		if(nOffset > 0)
		{
			f.Seek(nOffset, CFile::begin);
		}

		while( nOffset < nFileLength && !IsUserCancel())
		{
			FileService::FileResponse response;
			BSTR bstrID_ptr = (nOffset==0) ? bstrID : bstrID_without_GUID;
			HRESULT hResult = webRef.GetFile(bstrID_ptr, bstrPW, bstrServer, nOffset, m_nBuffSize, &response);
			if(hResult != NOERROR)
			{
				if( nTryCnt < m_nMaxTryCnt &&
				   (webRef.GetClientError() == SOAPCLIENT_SERVER_ERROR     ||
					webRef.GetClientError() == SOAPCLIENT_SOAPFAULT        ||
					webRef.GetClientError() == SOAPCLIENT_PARSEFAULT_ERROR ||
					webRef.GetClientError() == SOAPCLIENT_READ_ERROR       ||
					webRef.GetClientError() == SOAPCLIENT_PARSE_ERROR      ))
				{
					nTryCnt++;
					continue;
				}
				else
				{
					m_strErrorMsg = GetSoapErrorMsg(webRef.GetClientError());
					throw m_strErrorMsg;
				}
			}

			nTryCnt = 0;

			if(!response.result)
			{
				m_strErrorMsg = response.errorMsg;
				throw m_strErrorMsg;
			}

			if(response.resultBytes.size <= 0) break;

			f.Write(response.resultBytes.data, response.resultBytes.size);

			if(response.resultBytes.size > 0 && response.resultBytes.data) free(response.resultBytes.data);

			nOffset += response.resultBytes.size;
			if(pProgress) pProgress->ProgressEvent(FS_DOING, nOffset, nFileLength, strLocal);
		}
	}
	catch(CFileException err)
	{
		err.GetErrorMessage(szErrorMsg, 1024);
		SetErrorMsg(szErrorMsg);
	}
	catch(CString err)
	{
		SetErrorMsg(err);
	}

	f.Close();
	SysFreeString(bstrID);
	SysFreeString(bstrID_without_GUID);
	SysFreeString(bstrPW);
	SysFreeString(bstrServer);

	if(IsUserCancel())
	{
		if(pProgress) pProgress->ProgressEvent(FS_STOP, nOffset, nFileLength, strLocal);
		return SetErrorMsg(_T("Stop by the user"));
	}

	if(pProgress) pProgress->ProgressEvent((m_bResult ? FS_COMPLETE:FS_ERROR ), nOffset, nFileLength, strLocal);

	return m_bResult;
}

BOOL CFileServiceWrap::CopyFile(CString strSrc, CString strDst, BOOL bOverWrite)
{
	TraceLog(("CopyFile"));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);


	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrSrc = strSrc.AllocSysString();
	//BSTR bstrDst = strDst.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrSrc = EncryptAlloc(strSrc);
	BSTR bstrDst = EncryptAlloc(strDst);


	HRESULT hResult = webRef.CopyFile(bstrID, bstrPW, bstrSrc, bstrDst, (bool)bOverWrite, &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrSrc);
	SysFreeString(bstrDst);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		return SetErrorMsg(CString(response.errorMsg));
	}

	return m_bResult;
}

BOOL CFileServiceWrap::MoveFile(CString strSrc, CString strDst)
{
	TraceLog(("MoveFile"));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrSrc = strSrc.AllocSysString();
	//BSTR bstrDst = strDst.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrSrc = EncryptAlloc(strSrc);
	BSTR bstrDst = EncryptAlloc(strDst);


	HRESULT hResult = webRef.MoveFile(bstrID, bstrPW, bstrSrc, bstrDst, &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrSrc);
	SysFreeString(bstrDst);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		return SetErrorMsg(CString(response.errorMsg));
	}

	return m_bResult;
}

BOOL CFileServiceWrap::DeleteFile(CString strFile)
{
	TraceLog(("DeleteFile"));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrFile = strFile.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrFile = EncryptAlloc(strFile);

	HRESULT hResult = webRef.DeleteFile(bstrID, bstrPW, bstrFile, &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrFile);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		return SetErrorMsg(CString(response.errorMsg));
	}

	return m_bResult;
}

BOOL CFileServiceWrap::CopyDirectory(CString strSrc, CString strDst, BOOL bOverWrite)
{
	TraceLog(("CopyDirectory"));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrSrc = strSrc.AllocSysString();
	//BSTR bstrDst = strDst.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrSrc = EncryptAlloc(strSrc);
	BSTR bstrDst = EncryptAlloc(strDst);

	HRESULT hResult = webRef.CopyDirectory(bstrID, bstrPW, bstrSrc, bstrDst, (bool)bOverWrite, &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrSrc);
	SysFreeString(bstrDst);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		return SetErrorMsg(CString(response.errorMsg));
	}

	return m_bResult;
}

BOOL CFileServiceWrap::MoveDirectory(CString strSrc, CString strDst)
{
	TraceLog(("MoveDirectory"));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrSrc = strSrc.AllocSysString();
	//BSTR bstrDst = strDst.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrSrc = EncryptAlloc(strSrc);
	BSTR bstrDst = EncryptAlloc(strDst);

	HRESULT hResult = webRef.MoveDirectory(bstrID, bstrPW, bstrSrc, bstrDst, &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrSrc);
	SysFreeString(bstrDst);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		return SetErrorMsg(CString(response.errorMsg));
	}

	return m_bResult;
}

BOOL CFileServiceWrap::DeleteDirectory(CString strFile)
{
	TraceLog(("DeleteDirectory"));

	CleanUp();

	FileService::CFileService webRef;
	webRef.SetUrl(m_strUrl);
	webRef.SetTimeout(m_nTimeOut * 1000);

	FileService::FileResponse response;
	//BSTR bstrID = m_strUserId.AllocSysString();
	//BSTR bstrPW = m_strUserPW.AllocSysString();
	//BSTR bstrFile = strFile.AllocSysString();

	BSTR bstrID = EncryptAlloc(m_strUserId,true);
	BSTR bstrPW = EncryptAlloc(m_strUserPW);
	BSTR bstrFile = EncryptAlloc(strFile);

	HRESULT hResult = webRef.DeleteDirectory(bstrID, bstrPW, bstrFile, &response);
	SysFreeString(bstrID);
	SysFreeString(bstrPW);
	SysFreeString(bstrFile);

	if(hResult != NOERROR)
	{
		return SetErrorMsg(GetSoapErrorMsg(webRef.GetClientError()));
	}

	m_bResult = response.result;
	if(!m_bResult)
	{
		return SetErrorMsg(CString(response.errorMsg));
	}

	return m_bResult;
}
