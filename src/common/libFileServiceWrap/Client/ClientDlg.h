// ClientDlg.h : 헤더 파일
//

#pragma once


#include "FileServiceWrap/FileServiceWrapDll.h"
//#pragma comment(lib, "FileServiceWrap.lib")

#include "afxcmn.h"

// CClientDlg 대화 상자
class CClientDlg : public CDialog, public CFileServiceWrap::IProgressHandler
{
// 생성입니다.
public:
	CClientDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CLIENT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	virtual void ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath);

	static UINT UploadProc(LPVOID param);
	static UINT DownloadProc(LPVOID param);

	SYSTEMTIME m_tmStart;
	DWORD      m_dwStartTick;
	DWORD      m_dwCurTick;
	ULONGLONG  m_uSendBytes;

	CFileServiceWrap m_WebRef;

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	CProgressCtrl m_Progress;
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton9();
};
