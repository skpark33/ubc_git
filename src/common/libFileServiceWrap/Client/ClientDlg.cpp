// ClientDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "Client.h"
#include "ClientDlg.h"

#ifdef _DEBUG
	#ifdef LIB_FILEHTTPSERVICE
		#pragma comment(lib, "libFileHttpService_d.lib")
	#else
		#pragma comment(lib, "FileServiceWrap_d.lib")
	#endif
#else
	#ifdef LIB_FILEHTTPSERVICE
		#pragma comment(lib, "libFileHttpService.lib")
	#else
		#pragma comment(lib, "FileServiceWrap.lib")
	#endif
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CClientDlg 대화 상자




CClientDlg::CClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CClientDlg::IDD, pParent)
	//, m_WebRef(_T("ubcdev2.sqisoft.com"), 8080, FALSE)
	//, m_WebRef(_T("211.232.57.215"), 8080)
	, m_WebRef(_T("58.87.34.245"), 8080)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
}

BEGIN_MESSAGE_MAP(CClientDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CClientDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CClientDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON2, &CClientDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CClientDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CClientDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CClientDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CClientDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CClientDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CClientDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CClientDlg::OnBnClickedButton9)
END_MESSAGE_MAP()


// CClientDlg 메시지 처리기

BOOL CClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	SetDlgItemText(IDC_EDIT1, "C:\\Colorful.2011.BRRip.AC2.XViD.FAR.mkv");
	SetDlgItemText(IDC_EDIT2, "/contents/{b9b9fac1-d423-4787-ad21-0100c0f78c89}/Colorful.2011.BRRip.AC2.XViD.FAR.mkv");
	SetDlgItemText(IDC_EDIT3, "0");

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CClientDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CClientDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CClientDlg::OnBnClickedButton2()
{
	AfxMessageBox(m_WebRef.Test(_T("http://58.87.34.245:8080/UBC_HTTP/FileService.asmx")));
	//AfxMessageBox(m_WebRef.Test(_T("https://ubcdev2.sqisoft.com:8080/UBC_HTTP/FileService.asmx")));
}

void CClientDlg::OnBnClickedButton3()
{
	CString strServer;
	GetDlgItemText(IDC_EDIT2, strServer);

	CFileServiceWrap::SFileInfo info;
	if(m_WebRef.FileInfo(strServer, info))
	{
		AfxMessageBox(info.strFullName);
	}
	else
	{
		AfxMessageBox(m_WebRef.GetErrorMsg());
	}
}

void CClientDlg::OnBnClickedButton9()
{
	CStringArray arr;
	arr.Add("/contents/{0c26bf1f-42d2-4107-8e53-c508b1fcf4e8}/14.jpg");
	arr.Add("/contents/{0c754cc9-ade9-48bd-9e35-877cf82268dc}/Chrysanthemum.jpg");
	arr.Add("/contents/{0caeba49-f0a9-4846-87c8-d58f71f7dc07}/18.jpg");

	CString strResult;

	CFileServiceWrap::SFileList list;
	if(m_WebRef.FilesInfo(arr, list))
	{
		for(int i=0; i<list.GetCount() ;i++)
		{
			CFileServiceWrap::SFileInfo& info = list.GetAt(i);
			strResult += (info.bIsDirectory ? _T("+") : _T(" ")) + info.strName + _T("\n");
		}
	}
	else
	{
		strResult = m_WebRef.GetErrorMsg();
	}

	AfxMessageBox(strResult);
}

void CClientDlg::OnBnClickedButton4()
{
	CString strServer;
	GetDlgItemText(IDC_EDIT2, strServer);

	CString strResult;

	CFileServiceWrap::SFileList list;
	if(m_WebRef.List(strServer, list, FALSE))
	{
		for(int i=0; i<list.GetCount() ;i++)
		{
			CFileServiceWrap::SFileInfo& info = list.GetAt(i);

			strResult += (info.bIsDirectory ? _T("+") : _T(" ")) + info.strName + _T("\n");
		}
	}
	else
	{
		strResult = m_WebRef.GetErrorMsg();
	}

	AfxMessageBox(strResult);
}

void CClientDlg::ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath)
{
	m_Progress.SetPos( (uSendBytes*1000)/uTotalBytes );

	CString strText;
	strText.Format(_T("FileSize %I64d    %.1f%%    Bytes %I64d"), uTotalBytes, (uSendBytes*100.)/uTotalBytes, uSendBytes);

	SetDlgItemText(IDC_TEXT, strText);

	DWORD dwTickCount = ::GetTickCount();
	DWORD dwDiff = dwTickCount - m_dwCurTick;
	ULONGLONG nDiffByte = (uSendBytes - m_uSendBytes);
	m_uSendBytes = uSendBytes;

	CString strText2;
	strText2.Format("Start : %04d/%02d/%02d %02d:%02d:%02d %03ld    %.2f(MB/s)    Elapsed (%.1f sec)    Remaining (%.1f sec)"
					, m_tmStart.wYear
					, m_tmStart.wMonth
					, m_tmStart.wDay
					, m_tmStart.wHour
					, m_tmStart.wMinute
					, m_tmStart.wSecond
					, m_tmStart.wMilliseconds
					, (nDiffByte * 1000. / dwDiff) / (1024. * 1024.)
					, (dwTickCount - m_dwStartTick) / 1000.
					, (uTotalBytes - uSendBytes) / (nDiffByte * 1000. / dwDiff)
					);

	SetDlgItemText(IDC_TEXT2, strText2);

	m_dwCurTick = dwTickCount;
}

void CClientDlg::OnBnClickedButton5()
{
	SetDlgItemText(IDC_TEXT, _T(""));

	m_Progress.SetRange(0, 1000);
	m_Progress.SetPos(0);

	m_uSendBytes = 0;
	m_dwStartTick = ::GetTickCount();
	m_dwCurTick = m_dwStartTick;

	GetLocalTime(&m_tmStart); 
	CString strText2;

	strText2.Format("Start : %04d/%02d/%02d %02d:%02d:%02d %03ld"
					, m_tmStart.wYear
					, m_tmStart.wMonth
					, m_tmStart.wDay
					, m_tmStart.wHour
					, m_tmStart.wMinute
					, m_tmStart.wSecond
					, m_tmStart.wMilliseconds
					);

	SetDlgItemText(IDC_TEXT2, strText2);

	AfxBeginThread(UploadProc, (LPVOID)(this));
}

UINT CClientDlg::UploadProc(LPVOID param)
{
	CClientDlg* pDlg = (CClientDlg*)param;

	CString strLocal;
	pDlg->GetDlgItemText(IDC_EDIT1, strLocal);

	CString strServer;
	pDlg->GetDlgItemText(IDC_EDIT2, strServer);

	if(!pDlg->m_WebRef.PutFile(strLocal, strServer, 0, NULL))//(IProgressHandler*)pDlg))
	{
		AfxMessageBox(pDlg->m_WebRef.GetErrorMsg());
	}
	else
	{
		AfxMessageBox("Complete");
	}

	//AfxEndThread(0);
	return 0;
}

void CClientDlg::OnBnClickedButton6()
{	
	SetDlgItemText(IDC_TEXT, _T(""));

	m_Progress.SetRange(0, 1000);
	m_Progress.SetPos(0);

	m_uSendBytes = 0;
	m_dwStartTick = ::GetTickCount();
	m_dwCurTick = m_dwStartTick;

	GetLocalTime(&m_tmStart); 
	CString strText2;

	strText2.Format("Start : %04d/%02d/%02d %02d:%02d:%02d %03ld"
					, m_tmStart.wYear
					, m_tmStart.wMonth
					, m_tmStart.wDay
					, m_tmStart.wHour
					, m_tmStart.wMinute
					, m_tmStart.wSecond
					, m_tmStart.wMilliseconds
					);

	SetDlgItemText(IDC_TEXT2, strText2);

	AfxBeginThread(DownloadProc, (LPVOID)(this));
}

UINT CClientDlg::DownloadProc(LPVOID param)
{
	CClientDlg* pDlg = (CClientDlg*)param;

	CString strLocal;
	pDlg->GetDlgItemText(IDC_EDIT1, strLocal);

	CString strServer;
	pDlg->GetDlgItemText(IDC_EDIT2, strServer);

	CString strOffset;
	pDlg->GetDlgItemText(IDC_EDIT3, strOffset);

	//if(!pDlg->m_WebRef.GetFile(strServer, strLocal, 2147942400, (IProgressHandler*)pDlg))
	if(!pDlg->m_WebRef.GetFile(strServer, strLocal, (ULONGLONG)_ttoi64(strOffset), (IProgressHandler*)pDlg))
	{
		AfxMessageBox(pDlg->m_WebRef.GetErrorMsg());
	}
	else
	{
		AfxMessageBox("Complete");
	}

	//AfxEndThread(0);
	return 0;
}

void CClientDlg::OnBnClickedButton7()
{
	if(!m_WebRef.Login("server","rjtlrl2009"))
	{
		AfxMessageBox(m_WebRef.GetErrorMsg());
	}
}

void CClientDlg::OnBnClickedButton8()
{
	m_WebRef.SetUserCancel();
}
