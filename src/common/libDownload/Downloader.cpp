/************************************************************************************/
/*! @file Downloader.cpp
	@brief 파일을 다운로드하는 클래스 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2011/04/13\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2011/04/13:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "StdAfx.h"
#include "Downloader.h"
#include "Dbghelp.h"
//#include "CMN/libCommon/ubcIniMux.h"
#include "common/libCommon/ubcMuxRegacy.h"
#include "ci/libDebug/ciDebug.h"
//#include "ProfileManager.h"

#define		READ_PACKAGE			(mngProfile.GetProfileString)
#define		LOCAL_CONTNETS			(_T("/local_contents/"))

using namespace libDownload;
ciSET_DEBUG(9,"libDownloader");

typedef struct _ST_DOWNLOAD_THREAD_PARAM
{
	void*				pParentWnd;				///<부모 포인터
	CString				strPackageSiteId;		///<다운로드할 패키지 ini Site Id(+Site)
	CString				strPackageId;			///<다운로드할 패키지 ini Id(+Host)
	int					nBrowserId;				///<다운로드할 패키지를 방송할 Browser Id
} ST_DOWNLOAD_THREAD_PARAM;

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (bool) bUseReport : (in) 다운로드결과 보고기능 사용 여부
/// @param (int) nCutLevel : (in) location 값을 잘라내는 level
/////////////////////////////////////////////////////////////////////////////////
CDownloader::CDownloader(bool bUseReport, int nCutLevel, bool bCheckFileTime)
: m_strPackageFile(_T(""))
, m_strIP(_T(""))
, m_strId(_T(""))
, m_strPwd(_T(""))
, m_nPort(21)
, m_pFtpConnection(NULL)
, m_pDownResult(NULL)
, m_hWndNotify(NULL)
, m_ulTotalSize(0)
, m_paryDownloadFile(NULL)
, m_bAsync(true)
, m_bCancel(false)
, m_bUseReport(bUseReport)
, m_nCutLevel(nCutLevel)
, m_nFailCount(0)
, m_nTotalCount(0)
, m_nLocalExistCount(0)
, m_bHasFail(false)
, m_bUseHttp(false)
, m_nCurrentIndex(0)
, m_dwStartTick(0)
//, m_dwEndTick(0)
, m_ulCompleteDownloadSize(0)
, m_ulPreDownloadSize(0)
, m_pWebRef(NULL)
, m_bCheckFileTime(bCheckFileTime) //skpark same_size_file_problem
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, cDrive, cPath, cFilename, cExt);

	//기본 경로
	CString strPath;
	strPath.Format(_T("%s%s..\\..\\Contents\\ENC\\"), cDrive, cPath);
	CFileStatus status;
	CFile::GetStatus(strPath, status );
	m_strContentsPath = status.m_szFullName;
	
	strPath.Format(_T("%s%s..\\..\\Contents\\Temp\\"), cDrive, cPath);
	CFile::GetStatus(strPath, status );
	m_strTmpPath = status.m_szFullName;
	strPath.Format(_T("%s%s\\Config\\"), cDrive, cPath);

	CFile::GetStatus(strPath, status);
	m_strPackagePath = status.m_szFullName;

	//Http 사용 여부
	strPath.Format(_T("%s%s\\data\\UBCVariables.ini"), cDrive, cPath);
	//CProfileManager mngData;
	//mngData.Read(strPath);
	//CString strBool = mngData.GetProfileString(_T("ROOT"), _T("HTTP_ONLY"), _T(""));
	CString strBool = GetINIValue(_T("UBCVariables.ini"), _T("ROOT"), _T("HTTP_ONLY"));
	if(strBool.CompareNoCase("CLIENT") == 0)
	{
		m_bUseHttp = true;
		ciDEBUG(1, ("Http mod"));
	}
	else
	{
		m_bUseHttp = false;
		ciDEBUG(1, ("FTP mod"));
	}//if

	CString strCacheServer = GetINIValue(_T("UBCVariables.ini"), _T("ROOT"), _T("ContentsCacheServer"));
	if(!strCacheServer.IsEmpty() && strCacheServer.CompareNoCase("localhost") != 0)
	{
		m_strCacheServer = strCacheServer;
	}else{
		m_strCacheServer = "";
	}
	ciDEBUG(1,("Cache Server = %s", m_strCacheServer));
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CDownloader::~CDownloader()
{
	//m_ssInternet.Close();
	ClearDownloadResult();
	ClearDownloadArray();
	//ubcMux::clearInstance();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 콘텐츠 파일의 경로를 설정한다. \n
/// @param (CString) strContentsPath : (in) 컨텐츠 파일의 경로
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::SetContentsPath(CString strContentsPath)
{
	ciDEBUG(1,("SetContentsPath(%s)", strContentsPath));
	m_strContentsPath = strContentsPath;
	if(m_strContentsPath[m_strContentsPath.GetLength()-1] != _T('\\'))
	{
		m_strContentsPath += _T("\\");
	}//if

	CFileStatus status;
	CFile::GetStatus(m_strContentsPath, status);
	m_strContentsPath = status.m_szFullName;
	ciDEBUG(1,("m_strContentsPath(%s)", m_strContentsPath));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 콘텐츠 파일의 경로를 반환한다. \n
/// @return <형: CString> \n
///			설정된 컨텐츠 파일의 경로 \n
/////////////////////////////////////////////////////////////////////////////////
CString CDownloader::GetContentsPath()
{
	return m_strContentsPath;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 콘텐츠 파일의 임시 경로를 설정한다. \n
/// @param (CString) strTmpPath : (in) 컨텐츠 파일의 임시 경로
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::SetTmpPath(CString strTmpPath)
{
	ciDEBUG(1,("SetTmpPath(%s)", strTmpPath));
	m_strTmpPath = strTmpPath;
	if(m_strTmpPath[m_strTmpPath.GetLength()-1] != _T('\\'))
	{
		m_strTmpPath += _T("\\");
	}//if

	CFileStatus status;
	CFile::GetStatus(m_strTmpPath, status);
	m_strTmpPath = status.m_szFullName;
	ciDEBUG(1,("SetTmpPath(%s)", m_strTmpPath));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 콘텐츠 파일의 임시 경로를 반환한다. \n
/// @return <형: CString> \n
///			콘텐츠 파일의 임시 경로 \n
/////////////////////////////////////////////////////////////////////////////////
CString CDownloader::GetTmpPath()
{
	return m_strTmpPath;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 패키지 파일의 경로를 설정한다. \n
/// @param (CString) strPackagePath : (in) 패키지 파일의 경로
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::SetPackagePath(CString strPackagePath)
{
	ciDEBUG(1,("SetPackagePath(%s)", strPackagePath));
	m_strPackagePath = strPackagePath;
	if(m_strPackagePath[m_strPackagePath.GetLength()-1] != _T('\\'))
	{
		m_strPackagePath += _T("\\");
	}//if

	CFileStatus status;
	CFile::GetStatus(m_strPackagePath, status);
	m_strPackagePath = status.m_szFullName;
	ciDEBUG(1,("m_strPackagePath(%s)", m_strPackagePath));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 패키지 파일의 경로를 반환한다. \n
/// @return <형: CString> \n
///			패키지 파일의 경로 \n
/////////////////////////////////////////////////////////////////////////////////
CString CDownloader::GetPackagePath()
{
	return m_strPackagePath;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 진행상황을 받을 윈도우를 등록 \n
/// @param (HWND) hWnd : (in) 다운로드 진행상황을 받을 윈도우 핸들
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::SetProgressNotifyWnd(HWND hWnd)
{
	m_hWndNotify = hWnd;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드결과 보고기능을 사용할지 여부를 설정 \n
/// @param (bool) bUseReport : (in) 다운로드결과 보고기능을 사용할지 여부
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::UseDownloadReport(bool bUseReport)
{
	m_bUseReport = bUseReport;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠 파일의 location필드값을 잘라내는 level을 설정한다. \n
/// @param (int) nLevel : (in) 잘라내는 level
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::SetCutLocationLevel(int nLevel)
{
	m_nCutLevel = nLevel;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
///컨텐츠 파일의 location필드값을 잘라내는 level을 반환한다. \n
/// @return <형: int> \n
///			<잘라내는 level> \n
/////////////////////////////////////////////////////////////////////////////////
int CDownloader::GetCutLocationLevel()
{
	return m_nCutLevel;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 접속이 가능한 FTP 주소를 얻어온다. \n
/// @param (CString) strPackageSiteId : (in) 패키지 파일의 siteId
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::GetFtpAddress(CString strPackageSiteId, CString strPackageName)
{
	//인터넷 세션 초기화
	if(!m_bUseHttp)
	{
		if(m_pFtpConnection)
		{
			m_pFtpConnection->Close();
			delete m_pFtpConnection;
			m_pFtpConnection = NULL;
		}//if
		//m_ssInternet.Close();
	}//if

	//접속할 FTP 정보를 얻어온다.
	ubcMuxData* pFtpInfo = 0;

	if(strPackageName.IsEmpty() || strPackageName == "*")
	{ 
		pFtpInfo = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(strPackageSiteId);
	}
	else
	{
		pFtpInfo = ubcMux::getInstance(muxFactorySelector::select())->getMuxData();
	}
	ciDEBUG(1,("getMuxData(%s,%s)", strPackageSiteId, strPackageName));

	if(!pFtpInfo)
	{
		ciERROR(("Fail to get ftp server information"));
		//ubcMux::clearInstance();

		return false;
	}//if

	m_strIP = pFtpInfo->getFTPAddress();
	m_nPort = pFtpInfo->ftpPort;
	m_strId = pFtpInfo->ftpId.c_str();
	m_strPwd = pFtpInfo->ftpPasswd.c_str();
	ciDEBUG(1,("IP = %s, Port = %d, Id = %s, Pwd = %s", m_strIP, m_nPort, m_strId, m_strPwd));

	if(!pFtpInfo->isAvail())
	{
		ciERROR(("None ftp server available"));
		//ubcMux::clearInstance();
		
		return false;
	}//if

	ciDEBUG(1,("getMuxData(%s,%s) succeed", strPackageSiteId, strPackageName));
	pFtpInfo->print();
	//ubcMux::clearInstance();
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 접속이 가능한 Cache FTP 주소를 얻어온다. \n
/// @param (CString) strPackageSiteId : (in) 패키지 파일의 siteId
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::IsViaCache()
{
	ciDEBUG(1,("IsViaCache()"));
	// m_strCacheServer 는 포트번호를 포함한 형식이다.
	// ex) 111.111.111.111:8080
	if(m_strCacheServer.IsEmpty()){
		ciDEBUG(1,("No Cache Server"));
		return false;
	}
	ciWARN(("Cache Server is=%s", m_strCacheServer));
	// IP형식에 맞는지 확인해야 한다.
	// 점이 3개가 있는지 확인한다.
	ciStringTokenizer aTokens(m_strCacheServer,".");
	if(aTokens.countTokens() != 4){
		ciWARN(("Invalid Cache Server !!! "));
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 패키지 ini 파일을 다운로드 한다. \n
/// @param (CString) strPackageName : (in) 다운로드할 패키지 ini 파일의 이름
/// @param (CString) strPackageSiteId : (in) 다운로드할 패키지 ini Site Id(+Site)
/// @param (int) nBrowserId : (in) 다운로드할 패키지를 방송할 Browser Id
/// @param (bool) bUseReport : (in) 다운로드 결과를 기록할지 여부
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::DownloadPackageFile(CString strPackageName, CString strPackageSiteId, int nBrowserId, bool bUseReport)
{
	CString packageFile = strPackageName;
	if(strPackageName.GetLength() < 4 || strPackageName.Right(4).CompareNoCase(_T(".ini")) != 0)
	{
		packageFile.Append(_T(".ini"));
	}

	ciDEBUG(1, ("Begin download package file"));

	/* ini 와 bpi 는 Cache 로 부터 받지 않는다. 
	//skpark Cache Server Start [
	if(IsViaCache())
	{
		if(DnPkgFileViaCache(packageFile))
		{
			ciDEBUG(1, ("Success download contents package file from Cache Server"));
			return true;
		}
		ciWARN(("Fail to download contents package file from Cache Server"));
	}
	//skpark Cache Server End ]
	*/

	if(GetFtpAddress(strPackageSiteId, strPackageName))  // skpark Cache 처리 완료
	{
		if(DnPkgFile(packageFile))
		{
			ciDEBUG(1, ("Success download contents package file"));
			return true;
		}
		ciERROR(("Fail to download contents package file"));
	}
	else
	{
		ciERROR(("Fail to get ftp address"));
	}//if

	//다운로드가 실패하면 download result에 기록한다.
	m_bUseReport = bUseReport;
	if(m_bUseReport)
	{
		ClearDownloadResult();
		CDownloadFile* pDown = NULL;
		m_paryDownloadFile = new CDownloadFileArray();

		strPackageName.Replace(".ini", "");
		m_pDownResult = new CDownloadResult(m_paryDownloadFile, strPackageName, nBrowserId, this);
		m_pDownResult->BeginDownload(false);	
		m_pDownResult->FailDownloadPackage();
	}//if
	m_bUseReport = true;

	return false;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 패키지 ini 파일을 다운로드 하며, 받은파일을 새로이 덮어쓸지 여부를 선택한다. \n
/// @param (CString) strPackageName : (in) 다운로드할 패키지 ini 파일의 이름
/// @param (CString) strPackageSiteId : (in) 다운로드할 패키지 ini Site Id(+Site)
/// @param (bool) bReplace : (in) 다운로드한 파일을 덮어쓸지 여부
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::DownloadPackageFileEx(CString strPackageName, CString strPackageSiteId, bool bReplace)
{
	if(!GetFtpAddress(strPackageSiteId)) // skpark Cache 처리 불필요
	{
		TRACE(_T("Fail to get ftp address\r\n"));
		return false;
	}//if

	CString strExt = strPackageName.Right(4);
	if(strExt.CompareNoCase(_T(".ini")) != 0)
	{
		strPackageName.Append(_T(".ini"));
	}//if

	return DnPkgFile(strPackageName, bReplace);
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 방송계획(*.bpi) 파일을 다운로드 한다. \n
/// @param (CString) strPackageName : (in) 다운로드할 방송계획 파일의 이름
/// @param (CString) strBPISiteId : (in) 다운로드할 방송계획 Site Id(+Site)
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::DownloadBPIFile(CString strBPIName, CString strBPISiteId)
{
	ciDEBUG(1, ("Begin download bpi file"));
	CString BPIFile = strBPIName;
	if(strBPIName.GetLength() < 4 || strBPIName.Right(4).CompareNoCase(_T(".bpi")) != 0)
	{
		BPIFile.Append(_T(".bpi"));
	}//if


	/* ini 와 bpi 는 Cache 로 부터 받지 않는다. 
	//skpark Cache Server Start [
	if(IsViaCache())
	{
		if(DnPkgFileViaCache(BPIFile))
		{
			ciDEBUG(1, ("Success download bpi file from Cache Server"));
			return true;
		}
		ciWARN(("Fail to download bpi file from Cache Server"));
	}
	//skpark Cache Server End ]
	*/

	if(GetFtpAddress(strBPISiteId,_T("*")))  // skpark Cahce 처리 완료
	{

		if(DnPkgFile(BPIFile))
		{
			ciDEBUG(1, ("Success download bpi file"));
			return true;
		}
		ciERROR(("Fail to download bpi file"));
	}
	else
	{
		ciERROR(("Fail to get ftp address"));
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 중인 작업을 중지 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::CancelDownload()
{
	ciDEBUG(1, ("Set user canceled"));
	if(m_bUseHttp && m_pWebRef)
	{
		m_pWebRef->SetUserCancel();
	}//if

	m_bCancel = true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 패키지 파일을 FTP로부터 다운로드한다.(방송계획 파일 포함) \n
/// @param (CString) strPackageName : (in) 다운로드 패키지(방송계획) 이름
/// @param (bool) bReplace : (in) 다운로드한 패키지 파일을 로컬에 있는 파일에 덮어쓸지 여부
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::DnPkgFile(CString strPackageName, bool bReplace)
{
	CString strRemotePath, strLocalPath;
	strRemotePath = _T("/Config/");
	strRemotePath += strPackageName;

	strLocalPath = m_strPackagePath + strPackageName;
	strLocalPath += _T(".tmp");

	//로컬 경로에 지정된 폴더가 존재하도록...
	MakeSurePath(strLocalPath);

	if(!m_bUseHttp)
	{
		//Http 사용 안함
		ciDEBUG(1, ("FTP mod contentspackage file download"));
		try
		{
			m_pFtpConnection = m_ssInternet.GetFtpConnection(m_strIP, m_strId, m_strPwd, m_nPort, TRUE);
			if(m_pFtpConnection == NULL)
			{
				ciDEBUG(1,("FTP connect fai"));
				return false;
			}//if

			// download
			CFile* fileServer = m_pFtpConnection->OpenFile(strRemotePath);
			if(fileServer)
			{
				CFile fileLocal;
				if(fileLocal.Open(strLocalPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
				{
					BYTE* pBuf = (BYTE*)malloc(sizeof(BYTE)*BUF_SIZE);
					memset(pBuf, 0x00, BUF_SIZE);
					// read & write
					try
					{
						int nRead;
						while((nRead = fileServer->Read(pBuf, BUF_SIZE)) != 0)
						{
							fileLocal.Write(pBuf, nRead);
						}//while
						free(pBuf);
						fileLocal.Close();
						fileServer->Close();
						delete fileServer;
						m_pFtpConnection->Close();
						delete m_pFtpConnection;
						m_pFtpConnection = NULL;
						//m_ssInternet.Close();

						//if(bReplace)	//받은 파일을 로컬에 덮어쓰기
						//{
						ciDEBUG(1,("Replace package file = %s", strLocalPath));
						CString strTarget = strLocalPath;
						strTarget.Delete(strTarget.GetLength()-4, 4);
						if(!MoveFileEx(strLocalPath, strTarget, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
						{
							ciDEBUG(1,("Move file faile = %s", strTarget));
							return false;
						}//if
						//}//if

						return true;
					}
					catch(CFileException* ex)
					{
						TCHAR szCause[1024];
						ex->GetErrorMessage(szCause, 1024);
						ciDEBUG(1,("File I/O Exception = %s", szCause));
						ex->Delete();
					}//try
					free(pBuf);
					fileLocal.Close();
				}
				else
				{
					ciDEBUG(1,("Can not create path file = %s", strLocalPath));
				}//if

				fileServer->Close();
				delete fileServer;
			}
			else
			{
				ciDEBUG(1,("Can not open FTP file = %s", strRemotePath));
			}//if
		}
		catch(CInternetException* ex)
		{
			CString msg;
			TCHAR szCause[1024];
			ex->GetErrorMessage(szCause, 1024);
			msg.Format("(%d) %s", ex->m_dwError, szCause);
			ex->Delete();
			ciDEBUG(1,("FTP exception = %s", msg));
		}//try	

		if(m_pFtpConnection)
		{
			m_pFtpConnection->Close();
			delete m_pFtpConnection;
			m_pFtpConnection = NULL;
			//m_ssInternet.Close();
		}//if
	}
	else
	{
		//Http 사용
		ciDEBUG(1, ("HTTP mod contentspackage file download"));
		m_pWebRef = new CFileServiceWrap(m_strIP, m_nPort);
		m_pWebRef->Login(m_strId, m_strPwd);
		if(m_pWebRef->GetFile(strRemotePath, strLocalPath, 0, NULL))
		{
			ciDEBUG(1,("Replace package file = %s", strLocalPath));
			CString strTarget = strLocalPath;
			strTarget.Delete(strTarget.GetLength()-4, 4);
			if(MoveFileEx(strLocalPath, strTarget, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
			{
				delete m_pWebRef;
				m_pWebRef = NULL;
				return true;
			}//if

			ciDEBUG(1,("Move file faile = %s", strTarget));
		}//if
		
		ciERROR(("Fail to HTTP get file = %s", strRemotePath));
		delete m_pWebRef;
		m_pWebRef = NULL;
	}//if

	return false;
}

//skpark Cache Server Start [
bool CDownloader::DnPkgFileViaCache(CString strPackageName)
{
	//로컬 경로에 지정된 폴더가 존재하도록...
	MakeSurePath(m_strPackagePath);

	//Cache Server 사용
	ciDEBUG(1, ("Cache Server mod contentspackage file download %s", strPackageName ));

	CHttpDownload aObj(this);

	if(!aObj.InternetOpen()){
		ciERROR(("Fail to connect to internet"));
		return false;
	}

	CString url;
	url.Format("HTTP://%s", this->m_strCacheServer);

	aObj.SetURL(url);
	aObj.SetSourceDir(_T("UTV1.0/execute/config/"));
	aObj.SetTargetDir(m_strPackagePath);

	CString tmpName = (strPackageName+_T(".tmp"));
	int downloadResult  = aObj.DownloadFile(tmpName);
	if(downloadResult < 0){
		ciERROR(("Fail to Cache Server get file = %s, ERROR MSG=%s", tmpName, aObj.getErrMsg()));
		aObj.InternetClose();
		return false;
	}else if(downloadResult == 0){
		ciDEBUG(1, ("Already Exist = %s, MSG=%s", tmpName, aObj.getErrMsg()));
		aObj.InternetClose();
		return true;
	}
	CString lastPath = m_strPackagePath + strPackageName;
	CString tmpPath		= m_strPackagePath + tmpName;
	if(!MoveFileEx(tmpPath, lastPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
	{
		ciDEBUG(1,("Move file faile = %s", lastPath));
		aObj.InternetClose();
		return false;
	}//if

	ciDEBUG(1,("Replace package file = %s", lastPath));
	aObj.InternetClose();
	return true;

}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 패키지의 PlayContents 파일을 다운로드 한다. \n
/// @param (CString) strPackageSiteId : (in) 다운로드할 패키지 ini Site Id(+Site)
/// @param (CString) strPackageId : (in) 다운로드할 패키지 ini Id(+Host)
/// @param (int) nBrowserId : (in) 다운로드할 패키지를 방송할 Browser Id
/// @param (bool) bAsync : (in) Sync, Async 모드의 구분
/// @return <형: int> \n
///			<E_RETURN_DOWNLOAD enum 값> \n
/////////////////////////////////////////////////////////////////////////////////
int CDownloader::DownloadPlayContents(CString strPackageSiteId, CString strPackageId, int nBrowserId, bool bAsync)
{
	ciDEBUG(1, ("Begin download playcontents"));

	m_bCancel = false;
	m_bHasFail = false;
	m_strPackageFile = m_strPackagePath + strPackageId;
	CString strExt = m_strPackageFile.Right(4);
	if(strExt.CompareNoCase(_T(".ini")) != 0)
	{
		m_strPackageFile.Append(_T(".ini"));
	}//if
	
	m_bAsync = bAsync;
	CFileStatus status;
	if(CFile::GetStatus(m_strPackageFile, status ) == FALSE)
	{
		ciERROR(("Can't find package file = %s", m_strPackageFile));
		return E_DNRET_FAIL_MISSING_FILE;	//해당 파일이 없다.
	}//if

	ClearDownload();

	//비동기
	if(m_bAsync)
	{
		ciDEBUG(1, ("Async mod"));
		ST_DOWNLOAD_THREAD_PARAM* pParam;
		pParam = new ST_DOWNLOAD_THREAD_PARAM;
		pParam->pParentWnd = this;
		pParam->strPackageSiteId = strPackageSiteId;
		pParam->strPackageId = strPackageId;
		pParam->nBrowserId = nBrowserId;

		CWinThread* pThread = ::AfxBeginThread(CDownloader::ThreadDownloadPlayContents, pParam, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		pThread->m_bAutoDelete = TRUE;
		pThread->ResumeThread();
		return E_DNRET_SUCCESS;
	}//if

	//콘텐츠 패키지 분석
	if(!MakePlayContentsList())
	{
		ciERROR(("Fail to MakePlayContentsList"));
		return E_DNRET_FAIL_EMPTY_LIST;
	}//if

	//로컬에 콘텐츠 파일들이 있는지 검사
	if(!CheckLocalFileSize(strPackageId, nBrowserId))
	{
		ciDEBUG(1,("Download size is 0"));
		return E_DNRET_SUCCESS;
	}//if

	//skpark Cache Server Start [
	//접속정보 얻어오기 Cache Server
	bool bDownloadedByCache=false;  // skpark DownloadServer
	if(IsViaCache())
	{
		if(DownloadFilesViaCache(strPackageId))
		{
			ciDEBUG(1, ("Success download playcontents from Cache Server"));
			bDownloadedByCache = true;
		}
		else
		{
			ciWARN(("Fail to download playcontents from Cache Server"));
		}
	}
	//skpark Cache Server End ]

	//접속정보 얻어오기
	if(!GetFtpAddress(strPackageSiteId,strPackageId)) // skpark Cache 처리 완료
	{		
		ciERROR(("Fail to get ftp address"));
		if(bDownloadedByCache) {
			if(this->SetEndByCache()){
				return E_DNRET_SUCCESS;
			}
		}
		return E_DNRET_FAIL_FTP_ADDR;
	}//if

	//서버접속
	if(!ConnectFtp())
	{
		ciERROR(("FTP Connect fail"));
		if(bDownloadedByCache) {
			if(this->SetEndByCache()){
				return E_DNRET_SUCCESS;
			}
		}
		return E_DNRET_FAIL_FTP_CONNECT;
	}//if

	//서버에 콘텐츠 파일들이 있는지 검사
	if(!CheckRemoteFileSize(strPackageId, nBrowserId))	//사이즈 비교
	{
		ClearDownload();
		
		if(m_bHasFail)
		{
			ciERROR(("Fail to GetFileSize"));
			return E_DNRET_FAIL;
		}
		else
		{
			ciDEBUG(1,("Download size is 0"));
			return E_DNRET_SUCCESS;
		}//if
	}//if

	//파일 다운로드
	if(!DownloadFiles())
	{
		ClearDownload();
		ciDEBUG(1,("Download canceled"));
		return E_DNRET_CANCEL;
	}//if
	
	ClearDownload();

	if(m_bHasFail)
	{
		ciERROR(("Fail to DownloadFiles"));
		return E_DNRET_FAIL;
	}//if

	ciDEBUG(1, ("Success download playcontents"));
	return E_DNRET_SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 패키지의 모든 콘텐츠 파일을 다운로드 한다. \n
/// @param (CString) strPackageSiteId : (in) 다운로드할 패키지 ini Site Id(+Site)
/// @param (CString) strPackageId : (in) 다운로드할 패키지 ini Id(+Host)
/// @param (int) nBrowserId : (in) 다운로드할 패키지를 방송할 Browser Id
/// @param (bool) bAsync : (in) Sync, Async 모드의 구분
/// @return <형: int> \n
///			<E_RETURN_DOWNLOAD enum 값> \n
/////////////////////////////////////////////////////////////////////////////////
int CDownloader::DownloadAllContents(CString strPackageSiteId, CString strPackageId, int nBrowserId, bool bAsync)
{
	m_bCancel = false;
	m_bHasFail = false;
	m_strPackageFile = m_strPackagePath + strPackageId;
	CString strExt = m_strPackageFile.Right(4);
	if(strExt.CompareNoCase(_T(".ini")) != 0)
	{
		m_strPackageFile.Append(_T(".ini"));
	}//if

	m_bAsync = bAsync;
	CFileStatus status;
	if(CFile::GetStatus(m_strPackageFile, status ) == FALSE)
	{
		ciERROR(("Can't find package file = %s", m_strPackageFile));
		return E_DNRET_FAIL_MISSING_FILE;	//해당 파일이 없다.
	}//if

	ClearDownload();

	//비동기
	if(m_bAsync)
	{
		ST_DOWNLOAD_THREAD_PARAM* pParam;
		pParam = new ST_DOWNLOAD_THREAD_PARAM;
		pParam->pParentWnd = this;
		pParam->strPackageSiteId = strPackageSiteId;
		pParam->strPackageId = strPackageId;
		pParam->nBrowserId = nBrowserId;

		CWinThread* pThread = ::AfxBeginThread(CDownloader::ThreadDownloadAllContents, pParam, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		pThread->m_bAutoDelete = TRUE;
		pThread->ResumeThread();
		return E_DNRET_SUCCESS;
	}//if

	//콘텐츠 패키지 분석
	if(!MakeContentsList())
	{
		ciERROR(("Fail to MakePlayContentsList"));
		return E_DNRET_FAIL_EMPTY_LIST;
	}//if

	//로컬에 콘텐츠 파일들이 있는지 검사
	if(!CheckLocalFileSize(strPackageId, nBrowserId))
	{
		ciDEBUG(1,("Download size is 0"));
		return E_DNRET_SUCCESS;
	}//if

	//skpark Cache Server Start [
	//접속정보 얻어오기 Cache Server
	bool bDownloadedByCache=false;  // skpark DownloadServer
	if(IsViaCache())
	{
		if(DownloadFilesViaCache(strPackageId))
		{
			ciDEBUG(1, ("Success download playcontents from Cache Server"));
			bDownloadedByCache = true;
		}
		else
		{
			ciWARN(("Fail to download playcontents from Cache Server"));
		}
	}
	//skpark Cache Server end ]

	//접속정보 얻어오기
	if(!GetFtpAddress(strPackageSiteId,strPackageId)) // skpark Cache 처리 완료
	{		
		ciERROR(("Fail to get ftp address"));
		if(bDownloadedByCache) {
			if(this->SetEndByCache()){
				return E_DNRET_SUCCESS;
			}
		}
		return E_DNRET_FAIL_FTP_ADDR;
	}//if

	//서버접속
	if(!ConnectFtp())
	{
		if(bDownloadedByCache) {
			if(this->SetEndByCache()){
				return E_DNRET_SUCCESS;
			}
		}
		ciERROR(("FTP Connect fail"));
		return E_DNRET_FAIL_FTP_CONNECT;
	}//if

	//서버에 콘텐츠 파일들이 있는지 검사
	if(!CheckRemoteFileSize(strPackageId, nBrowserId))	//사이즈 비교
	{
		ClearDownload();
		
		if(m_bHasFail)
		{
			ciERROR(("Fail to GetFileSize"));
			return E_DNRET_FAIL;
		}
		else
		{
			ciDEBUG(1,("Download size is 0"));
			return E_DNRET_SUCCESS;
		}//if
	}//if

	//파일 다운로드
	if(!DownloadFiles())
	{
		ClearDownload();
		ciDEBUG(1,("Download canceled"));
		return E_DNRET_CANCEL;
	}//if
	
	ClearDownload();

	if(m_bHasFail)
	{
		ciERROR(("Fail to DownloadFiles"));
		return E_DNRET_FAIL;
	}//if

	ciDEBUG(1, ("Success download playcontents"));
	return E_DNRET_SUCCESS;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 해당 콘텐츠패키지의 플레이콘텐츠가 모두 다운로드 되었는지 여부를 반환 \n
/// @param (CString) strPackageId : (in) 다운로드여부를 확인할 패키지 id
/// @return <형: int> \n
///			<1: 이미 다운로드 되었음> \n
///			<0: 다운로드할 플레이컨텐츠가 존재함> \n
///			<-1: 콘텐츠패키지 파일 오류> \n
/////////////////////////////////////////////////////////////////////////////////
int CDownloader::IsAlreadyDownloaded(CString strPackageId)
{
	m_bHasFail = false;
	m_strPackageFile = m_strPackagePath + strPackageId;
	CString strExt = m_strPackageFile.Right(4);
	if(strExt.CompareNoCase(_T(".ini")) != 0)
	{
		m_strPackageFile.Append(_T(".ini"));
	}//if
	
	CFileStatus status;
	if(CFile::GetStatus(m_strPackageFile, status ) == FALSE)
	{
		ciERROR(("Can't find package file = %s", m_strPackageFile));
		return -1;	//해당 파일이 없다.
	}//if

	ClearDownload();

	if(!MakePlayContentsList())
	{
		ciERROR(("Fail to MakePlayContentsList"));
		return -1;		
	}//if

	//로컬에 콘텐츠 파일들이 있는지 검사
	m_bUseReport = false;						//DownloadResult를 기록하지 않도록 함
	if(!CheckLocalFileSize(strPackageId))
	{
		m_bUseReport = true;
		ciDEBUG(1,("Download size is 0"));
		return 1;
	}//if
	m_bUseReport = true;

	//다운로드해야하는 파일이 존재함
	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 패키지 ini와 컨텐츠를 다운로드하는 모든과정을 일괄 처리한다. \n
/// @param (CString) strPackageName : (in) 다운로드할 패키지 ini 파일의 이름
/// @param (CString) strPackageSiteId : (in) 다운로드할 패키지 ini Site Id(+Site)
/// @param (int) nBrowserId : (in) 다운로드할 패키지를 방송할 Browser Id
/// @param (bool) bUseReport : (in) 다운로드 결과를 기록할지 여부
/// @return <형: int> \n
///			<E_RETURN_DOWNLOAD enum 값> \n
/////////////////////////////////////////////////////////////////////////////////
int CDownloader::ProcessDownloadPackage(CString strPackageName, CString strPackageSiteId, int nBrowserId, bool bUseReport)
{
	//기존에 패키지파일 다운로드, 콘텐츠 다운로드 여부확인, 콘텐츠 다운로드 함수가 각각 분리되어 있던것을 일괄 처리한다.
	// 1) 로컬에 콘텐츠패키지파일(ini)파일이 있는지 확인
	// 2) 콘텐츠패키지파일 다운로드(로컬에 존재여부와 관계없이)
	// 3) 콘텐츠 파일을 다운로드 해야하는지 확인
	// 4) 콘텐츠 파일 다운로드

	ciDEBUG(1, ("Begin ProcessDownloadPackage"));

	bool bExistLocal = false;
	m_bHasFail = false;
	m_strPackageFile = m_strPackagePath + strPackageName;
	CString strExt = m_strPackageFile.Right(4);
	if(strExt.CompareNoCase(_T(".ini")) != 0)
	{
		m_strPackageFile.Append(_T(".ini"));
	}//if
	
	// 1) 로컬에 콘텐츠패키지 파일이 있는지 확인
	CFileStatus status;
	if(CFile::GetStatus(m_strPackageFile, status ) == FALSE)
	{
		ciERROR(("Can't find package file = %s", m_strPackageFile));
		bExistLocal = false;
	}
	else
	{
		bExistLocal = true;
	}//if

	// 2) 콘텐츠패키지파일 다운로드
	if(!DownloadPackageFile(strPackageName, strPackageSiteId, nBrowserId, bUseReport))
	{
		ciERROR(("Fail to download packagefile : %s", strPackageName));
		return E_DNRET_FAIL_PACKAGE_DOWNLOAD;
	}//if

	// 3) 콘텐츠 파일을 다운로드 해야하는지 확인
	// 콘텐츠패키지파일이 로컬에 있는경우에만 확인
	// 콘텐츠패키지파일이 로컬에 없었다면 download result 파일을 기록하기 위하여 그냥 콘텐츠파일 다운로드 시도...
	if(bExistLocal)
	{
		int nRet = IsAlreadyDownloaded(strPackageName);
		switch(nRet)
		{
		case 1:		//모든콘텐츠 파일이 로컬에 다운로드 되었음
			{
				ciDEBUG(1, ("Already all contents file downloaded"));
				return E_DNRET_ALREADY_EXIST;
			}
			break;
		case 0:		//다운로드해야하는 콘텐츠 파일이 있음
			{
				ciDEBUG(1, ("Need Contents file download"));
			}
			break;
		case -1:	//콘텐츠패키지파일 오류
			{
				ciERROR(("Read fail in contentspackage file"));
				return E_DNRET_FAIL_READ_FILE;
			}
			break;
		default:	//알수없는 오류
			{
				ciERROR(("Unknown error in IsAlreadyDownloaded"));
				return E_DNRET_ERROR_UNKNOWN;
			}
		}//switch
	}//if

	// 4) 콘텐츠 파일 다운로드
	int nRet = DownloadPlayContents(strPackageSiteId, strPackageName, nBrowserId, false);
	ciDEBUG(1, ("End ProcessDownloadPackage - ret code : %d", nRet));
	return nRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드할 PlayContents 리스트를 만든다. \n
/// @return <형: bool> \n
///			<true: 다운로드할 파일이 있음> \n
///			<false: 다운로드할 파일이 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::MakePlayContentsList()
{
	CString strTemplateList		= _T("");
	CString strTemplatePlayList	= _T("");
	CString strFrameList		= _T("");
	CString strPlayContentsList = _T("");
	CString strETCList			= _T("");
	CString strContentsList		= _T("");
	CDownloadFile* pDown		= NULL;

	CProfileManager mngProfile;
	if(!mngProfile.Read(m_strPackageFile))
	{
		ciERROR(("Fail to load contentspackage file = %s", m_strPackageFile));
		return false;		//파일 읽기 오류
	}//if

	strTemplatePlayList = READ_PACKAGE(_T("Host"), _T("templatePlayList"));

	//템플릿 플레이 리스트에 있는것만 가져온다.
	int nPos = 0;
	int nIdx = 0;
	int nLen = 0;
	CString strTemplate;
	CString token = strTemplatePlayList.Tokenize(",", nPos);
	while(token != _T(""))
	{
		token.Trim();
		nIdx = token.Find(_T("/"));
		if(nIdx != -1)
		{
			strTemplate = _T("Template");
			strTemplate += token.Left(nIdx);
			strTemplateList += strTemplate;
			strTemplateList += _T(",");
		}//if

		token = strTemplatePlayList.Tokenize(_T(","), nPos);
	}//while

	strTemplateList.TrimRight(_T(","));
	if(strTemplateList.GetLength() == 0)
	{
		ciERROR(("Empty template play list"));
		return false;
	}//if

	//FrameList
	nPos = 0;
	token = strTemplateList.Tokenize(_T(","), nPos);
	while(token != _T(""))
	{
		token.Trim();
		strFrameList += READ_PACKAGE(token, _T("FrameList"));
		strFrameList += _T(",");

		token = strTemplateList.Tokenize(_T(","), nPos);
	}//while

	strFrameList.TrimRight(_T(","));
	if(strFrameList.GetLength() == 0)
	{
		ciERROR(("Empty frame list"));
		return false;
	}//if

	//PlayContentsList
	nPos = 0;
	token = strFrameList.Tokenize(_T(","), nPos);
	while(token != "")
	{
		token.Trim();
		strPlayContentsList += READ_PACKAGE(token, _T("DefaultScheduleList"));
		strPlayContentsList += _T(",");

		strPlayContentsList += READ_PACKAGE(token, _T("ScheduleList"));
		strPlayContentsList += _T(",");

		token = strFrameList.Tokenize(_T(","), nPos);
	}//while

	strPlayContentsList.TrimRight(_T(","));
	if(strPlayContentsList.GetLength() == 0)
	{
		ciERROR(("Empty playcontents list"));
		return false;
	}//if

	ClearDownloadArray();
	m_paryDownloadFile = new CDownloadFileArray();

	nPos = 0;
	CString strName;
	ULONGLONG ulVolume = 0;
	token = strPlayContentsList.Tokenize(_T(","), nPos);
	bool dontDownload = false;
	while(token != _T(""))
	{
		token.Trim();

		strName = READ_PACKAGE(token, _T("filename"));
		ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));
		dontDownload = mngProfile.GetProfileInt(token,_T("bDontDownload"),0);

		if(strName.GetLength() != 0 && ulVolume != 0 && dontDownload == false)
		{
			pDown = new CDownloadFile();
			pDown->m_strFileName = strName;
			pDown->m_ulFileSize = ulVolume;

			pDown->m_nContentType = atoi(READ_PACKAGE(token, _T("contentsType")));
			pDown->m_strServerPath = READ_PACKAGE(token, _T("location"));
			pDown->m_strContentsId = READ_PACKAGE(token, _T("contentsId"));
			pDown->m_strContentsName = READ_PACKAGE(token, _T("contentsName"));
			pDown->m_strLastModifiedTime = READ_PACKAGE(token, _T("lastModifiedTime"));//skpark same_size_file_problem
			ciDEBUG(1,("lastModifiedTime of %s is %s", token,pDown->m_strLastModifiedTime));

			//경로 처음에는 "/contents/"문자열이 와야한다.
			nLen = pDown->m_strServerPath.GetLength();
			pDown->m_strContentsRoot = pDown->m_strServerPath.Left(10);											//컨텐츠 root 경로
			pDown->m_strContentsPath = pDown->m_strServerPath.Right(pDown->m_strServerPath.GetLength() - 10);	//컨텐츠 경로

			m_paryDownloadFile->Add(pDown);
		}//if

		token = strPlayContentsList.Tokenize(_T(","), nPos);
	}//while

	//플래쉬 부속파일
	strContentsList = READ_PACKAGE(_T("Host"), _T("ContentsList"));
	nPos = 0;
	int nType;
	token = strContentsList.Tokenize(_T(","), nPos);
	while(token != _T(""))
	{
		token.Trim();
		strName = READ_PACKAGE(token, _T("filename"));
		ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));
		nType = atoi(READ_PACKAGE(token, _T("contentsType")));

		if(strName.GetLength() != 0 && ulVolume != 0 && nType == CONTENTS_FILE)
		{
			pDown = new CDownloadFile();
			pDown->m_strFileName = strName;
			pDown->m_ulFileSize = ulVolume;
			pDown->m_nContentType = nType;

			pDown->m_strServerPath = READ_PACKAGE(token, _T("location"));
			pDown->m_strContentsId = READ_PACKAGE(token, _T("contentsId"));
			pDown->m_strContentsName = READ_PACKAGE(token, _T("contentsName"));
			pDown->m_strLastModifiedTime = READ_PACKAGE(token, _T("lastModifiedTime")); //skpark same_size_file_problem
			ciDEBUG(1,("lastModifiedTime of %s is %s", pDown->m_strFileName,pDown->m_strLastModifiedTime));
			//경로 처음에는 "/contents/"문자열이 와야한다.
			nLen = pDown->m_strServerPath.GetLength();
			pDown->m_strContentsRoot = pDown->m_strServerPath.Left(10);											//컨텐츠 root 경로
			pDown->m_strContentsPath = pDown->m_strServerPath.Right(pDown->m_strServerPath.GetLength() - 10);	//컨텐츠 경로

			m_paryDownloadFile->Add(pDown);
		}//if

		token = strContentsList.Tokenize(_T(","), nPos);
	}//while

	//ETCList
	strETCList = READ_PACKAGE(_T("Host"), _T("EtcList"));
	nPos = 0;
	token = strETCList.Tokenize(_T(","), nPos);
	while(token != _T(""))
	{
		token.Trim();
		strName = READ_PACKAGE(token, _T("filename"));
		ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));

		if(strName.GetLength() != 0 && ulVolume != 0)
		{
			pDown = new CDownloadFile();
			pDown->m_strFileName = strName;
			pDown->m_ulFileSize = ulVolume;

			pDown->m_nContentType = atoi(READ_PACKAGE(token, _T("contentsType")));
			pDown->m_strServerPath = READ_PACKAGE(token, _T("location"));
			pDown->m_strContentsId = READ_PACKAGE(token, _T("contentsId"));
			pDown->m_strContentsName = READ_PACKAGE(token, _T("contentsName"));
			pDown->m_strLastModifiedTime = READ_PACKAGE(token, _T("lastModifiedTime")); //skpark same_size_file_problem
			ciDEBUG(1,("lastModifiedTime of %s is %s", pDown->m_strFileName,pDown->m_strLastModifiedTime));

			//경로 처음에는 "/contents/"문자열이 와야한다.
			nLen = pDown->m_strServerPath.GetLength();
			pDown->m_strContentsRoot = pDown->m_strServerPath.Left(10);											//컨텐츠 root 경로
			pDown->m_strContentsPath = pDown->m_strServerPath.Right(pDown->m_strServerPath.GetLength() - 10);	//컨텐츠 경로

			m_paryDownloadFile->Add(pDown);
		}//if

		token = strETCList.Tokenize(_T(","), nPos);
	}//while

	if(m_paryDownloadFile->GetCount() == 0)
	{
		ciERROR(("Nothing to download"));
		return false;
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드할 Contents 리스트를 만든다. \n
/// @return <형: bool> \n
///			<true: 다운로드할 파일이 있음> \n
///			<false: 다운로드할 파일이 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::MakeContentsList()
{
	CString strContentsList, strETCList;
	CDownloadFile* pDown = NULL;

	CProfileManager mngProfile;
	if(!mngProfile.Read(m_strPackageFile))
	{
		ciERROR(("Fail to load contentspackage file = %s", m_strPackageFile));
		return false;		//파일 읽기 오류
	}//if

	strContentsList = READ_PACKAGE(_T("Host"), _T("ContentsList"));

	ClearDownloadArray();
	m_paryDownloadFile = new CDownloadFileArray();

	int nPos = 0;
	int nLen = 0;
	CString strName;
	ULONGLONG ulVolume = 0;
	CString token = strContentsList.Tokenize(_T(","), nPos);
	while(token != _T(""))
	{
		if(m_bCancel)
		{
			return false;
		}//if

		token.Trim();

		strName = READ_PACKAGE(token, _T("filename"));
		ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));

		if(strName.GetLength() != 0 && ulVolume != 0)
		{
			pDown = new CDownloadFile();
			pDown->m_strFileName = strName;
			pDown->m_ulFileSize = ulVolume;

			pDown->m_nContentType = atoi(READ_PACKAGE(token, _T("contentsType")));
			pDown->m_strServerPath = READ_PACKAGE(token, _T("location"));
			pDown->m_strContentsId = READ_PACKAGE(token, _T("contentsId"));
			pDown->m_strContentsName = READ_PACKAGE(token, _T("contentsName"));
			pDown->m_strLastModifiedTime = READ_PACKAGE(token, _T("lastModifiedTime")); //skpark same_size_file_problem
			ciDEBUG(1,("lastModifiedTime of %s is %s", pDown->m_strFileName,pDown->m_strLastModifiedTime));

			//경로 처음에는 "/contents/"문자열이 와야한다.
			nLen = pDown->m_strServerPath.GetLength();
			pDown->m_strContentsRoot = pDown->m_strServerPath.Left(10);											//컨텐츠 root 경로
			pDown->m_strContentsPath = pDown->m_strServerPath.Right(pDown->m_strServerPath.GetLength() - 10);	//컨텐츠 경로

			m_paryDownloadFile->Add(pDown);
		}//if

		token = strContentsList.Tokenize(_T(","), nPos);
	}//while

	//ETCList
	strETCList = READ_PACKAGE(_T("Host"), _T("EtcList"));
	nPos = 0;
	token = strETCList.Tokenize(_T(","), nPos);
	while(token != _T(""))
	{
		if(m_bCancel)
		{
			return false;
		}//if

		token.Trim();

		strName = READ_PACKAGE(token, _T("filename"));
		ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));

		if(strName.GetLength() != 0 && ulVolume != 0)
		{
			pDown = new CDownloadFile();
			pDown->m_strFileName = strName;
			pDown->m_ulFileSize = ulVolume;

			pDown->m_nContentType = atoi(READ_PACKAGE(token, _T("contentsType")));
			pDown->m_strServerPath = READ_PACKAGE(token, _T("location"));
			pDown->m_strContentsId = READ_PACKAGE(token, _T("contentsId"));
			pDown->m_strContentsName = READ_PACKAGE(token, _T("contentsName"));
			pDown->m_strLastModifiedTime = READ_PACKAGE(token, _T("lastModifiedTime")); //skpark same_size_file_problem
			ciDEBUG(1,("lastModifiedTime of %s is %s", pDown->m_strFileName,pDown->m_strLastModifiedTime));

			//경로 처음에는 "/contents/"문자열이 와야한다.
			nLen = pDown->m_strServerPath.GetLength();
			pDown->m_strContentsRoot = pDown->m_strServerPath.Left(10);											//컨텐츠 root 경로
			pDown->m_strContentsPath = pDown->m_strServerPath.Right(pDown->m_strServerPath.GetLength() - 10);	//컨텐츠 경로

			m_paryDownloadFile->Add(pDown);
		}//if

		token = strETCList.Tokenize(_T(","), nPos);
	}//while

	if(m_paryDownloadFile->GetCount() == 0)
	{
		ciERROR(("nothing to download"));
		return false;
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FTP 연결을 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::ConnectFtp()
{
	if(!m_bUseHttp)
	{
		//Http 사용 안함
		ciDEBUG(1, ("Trying connect FTP : %s, %s, %s, %d", m_strIP, m_strId, m_strPwd, m_nPort));
		try
		{
			m_pFtpConnection = m_ssInternet.GetFtpConnection(m_strIP, m_strId, m_strPwd, m_nPort, TRUE);
			if(m_pFtpConnection != NULL)
			{
				ciDEBUG(1, ("FTP connected"));
				return true;
			}//if
		}
		catch(CInternetException* ex)
		{
			CString msg;
			TCHAR szCause[1024];
			ex->GetErrorMessage(szCause, 1024);
			msg.Format(_T("FTP exception : (%d) %s\r\n"), ex->m_dwError, szCause);
			ex->Delete();
			ciERROR(("%s", msg));
		}//try

	}
	else
	{
		//Http 사용
		ciDEBUG(1, ("Trying connect HTTP : %s, %s, %s, %d", m_strIP, m_strId, m_strPwd, m_nPort));
		m_pWebRef = new CFileServiceWrap(m_strIP, m_nPort);
		m_pWebRef->Login(m_strId, m_strPwd);
		ciDEBUG(1, ("HTTP connected"));
		return true;
	}//if

	ciERROR(("Network connect fail"));
	return false;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬의 파일사이즈를 비교한다. \n
/// @param (CString) strPackageId : (in) 다운로드할 패키지 ini Id(+Host)
/// @param (int) nBrowserId : (in) 다운로드할 패키지를 방송할 Browser Id
/// @return <형: bool> \n
///			<true: 다운할 파일이 있음> \n
///			<false: 다운로드할 파일이 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::CheckLocalFileSize(CString strPackageId, int nBrowserId)
{
	CDownloadFile* pInfo = NULL;
	bool bNoneExistLocal = false;
	m_nTotalCount = m_paryDownloadFile->GetCount();

	ciDEBUG(1,("Check file array count = %d", m_nTotalCount));
	this->m_nLocalExistCount = 0;

	for(int i=0; i<m_nTotalCount; i++)
	{
		pInfo = m_paryDownloadFile->GetAt(i);

		//Compare local size
		if(CompareLocalContents(pInfo))
		{
			//로컬에 파일이 있으므로 다운로드 하지 않는다.
			ciDEBUG(10,("Local exist = %s", pInfo->m_strFileName));
			this->m_nLocalExistCount++;
			continue;	
		}//if

		bNoneExistLocal = true;
	}//for

	if(bNoneExistLocal)
	{
		return true;
	}//if

	ciDEBUG(1, ("All contents file exist in local"));
	if(m_bUseReport)
	{
		ClearDownloadResult();
		m_pDownResult = new CDownloadResult(m_paryDownloadFile, strPackageId, nBrowserId, this);
		m_pDownResult->BeginDownload(false);	
		m_pDownResult->EndDownload();
	}//if
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Remote의 파일사이즈를 비교한다. \n
/// @param (CString) strPackageId : (in) 다운로드할 패키지 ini Id(+Host)
/// @param (int) nBrowserId : (in) 다운로드할 패키지를 방송할 Browser Id
/// @return <형: bool> \n
///			<true: 다운할 파일이 있음> \n
///			<false: 다운로드할 파일이 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::CheckRemoteFileSize(CString strPackageId, int nBrowserId)
{
	CDownloadFile* pInfo = NULL;
	m_ulTotalSize = 0;
	m_nTotalCount = m_paryDownloadFile->GetCount();

	ciDEBUG(1,("Check file array count = %d", m_nTotalCount));
	if(!m_bUseHttp)
	{
		for(int i=0; i<m_nTotalCount; i++)
		{
			pInfo = m_paryDownloadFile->GetAt(i);
			if(pInfo->m_nDownloadState == E_STATE_COMPLETE_FAIL
				|| pInfo->m_nDownloadState == E_STATE_COMPLETE_SUCCESS)
			{
				//로컬에 있거나 오류인 경우
				continue;
			}//if

			//Compare remote size
			if(!CompareRemoteContents(pInfo))
			{
				m_bHasFail = true;
				ciDEBUG(1,("Can't found file from server : %s, %s", pInfo->m_strFileName, pInfo->m_strContentsId));
			}//if
		}//for
	}
	else
	{
		//Http를 사용하는 경우에는 배열정보를 넘겨서 한꺼번에 파일사이즈를 비교한다.
		if(!CompareRemoteContentsViaHttp())
		{
			ciERROR(("Fail to find contents file from HTTP server"));
			return false;
		}//if
	}//if

	if(m_ulTotalSize > 0)
	{
		ciDEBUG(1, ("Contents file will be download from server : %I64u (bytes)", m_ulTotalSize));
		if(m_bUseReport)
		{
			ClearDownloadResult();
			m_pDownResult = new CDownloadResult(m_paryDownloadFile, strPackageId, nBrowserId, this);
			m_pDownResult->BeginDownload();	
		}//if
		return true;
	}//if

	ciDEBUG(1, ("Download size is 0"));
	if(m_bUseReport)
	{
		ClearDownloadResult();
		m_pDownResult = new CDownloadResult(m_paryDownloadFile, strPackageId, nBrowserId, this);
		m_pDownResult->BeginDownload(false);	
		m_pDownResult->EndDownload();
	}//if
	return false;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 해당 파일이 로컬에 있는지 비교한다. \n
/// @param (CDownloadFile*) pInfo : (in/out) 컨텐츠 파일의 정보
/// @return <형: bool> \n
///			<true: 로컬에 같은 파일이 있음> \n
///			<false: 로컬에 같은 파일이 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::CompareLocalContents(CDownloadFile* pInfo)
{
	CFileStatus fs;
	CString strLocalPath, strTmpPath;
	//struct _stat64 stStat64;
	//memset(&stStat64, 0x00, sizeof(stStat64));

	if(IsFontFile(pInfo->m_strFileName))
	{
		char szWinDir[MAX_PATH] = { 0x00 };
		if(GetWindowsDirectory(szWinDir, MAX_PATH) == 0)
		{
			ciWARN(("Fail to get system winows dir"));
			pInfo->SetEnd(E_REASON_EXCEPTION_FILE, E_STATE_COMPLETE_FAIL, 0, false);
			return false;
		}//if

		CString strFontDir;
		strFontDir.Format(_T("%s\\fonts\\%s"), szWinDir, pInfo->m_strFileName);

		//if(_stati64(strFontDir, &stStat64) == 0)
		if(CFile::GetStatus(strFontDir, fs) == TRUE)
		{
			//if(stStat64.st_size == pInfo->m_ulFileSize)
			if(fs.m_size == pInfo->m_ulFileSize)
			{
				pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
				return true;
			}
			else
			{
				//패키지파일과 로컬에 존재하는 파일의 사이즈가 틀린경우
				//ciDEBUG(1, ("Different local [file size ini size = %I64u, sys size = %I64u]", pInfo->m_ulFileSize, stStat64.st_size));
				ciDEBUG(1, ("Different local file(%s) size [ini size = %I64u, sys size = %I64u]", strFontDir, pInfo->m_ulFileSize, fs.m_size));
				//pInfo->m_ulLocalFileSize = stStat64.st_size;
				pInfo->m_ulLocalFileSize = fs.m_size;
				return false;
			}//if
		}
		else
		{
			return false;
		}//if
	}
	else
	{
		//Tmp 와 ENC 폴더 양쪽을 비교해교하여야 한다.
		GetLocalPath(strLocalPath, strTmpPath, pInfo->m_strServerPath, pInfo->m_strFileName);
		//if(_stati64(strLocalPath, &stStat64) == 0)
		if(CFile::GetStatus(strLocalPath, fs) == TRUE)
		{
			//if(stStat64.st_size == pInfo->m_ulFileSize)
			if(fs.m_size == pInfo->m_ulFileSize 
				&& false == markNewFile(strLocalPath, fs.m_atime,pInfo)) // skpark same_size_file_problem
			{
				pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
				return true;
			}
			else
			{
				//패키지파일과 로컬에 존재하는 파일의 사이즈가 틀린경우
				//ciDEBUG(1, ("Different local [file size ini size = %I64u, sys size = %I64u]", pInfo->m_ulFileSize, stStat64.st_size));
				ciDEBUG(1, ("Different local file(%s) size [ini size = %I64u, sys size = %I64u] or filetime", strLocalPath, pInfo->m_ulFileSize, fs.m_size));
				//pInfo->m_ulLocalFileSize = stStat64.st_size;
				pInfo->m_ulLocalFileSize = fs.m_size;
				return false;
			}//if
		}
		//else if(_stati64(strTmpPath, &stStat64) == 0)
		else if(CFile::GetStatus(strTmpPath, fs) == TRUE)
		{
			//if(stStat64.st_size == pInfo->m_ulFileSize)
			if(fs.m_size == pInfo->m_ulFileSize
				&& false == markNewFile(strTmpPath, fs.m_atime,pInfo)) // skpark same_size_file_problem
			{

				pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);

				//Tmp폴더에 있는 파일은 ENC 폴더로 옮겨준다.
				MakeSurePath(strLocalPath);
				
				if(!MoveFileEx(strTmpPath, strLocalPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
				{
					ciERROR(("Contents file replace fail = %s", strLocalPath));
				}//if
				return true;
			}
			else
			{
				//패키지파일과 로컬에 존재하는 파일의 사이즈가 틀린경우
				//ciDEBUG(1, ("Different local [file size ini size = %I64u, sys size = %I64u]", pInfo->m_ulFileSize, stStat64.st_size));
				ciDEBUG(1, ("Different local file(%s) size [ini size = %I64u, sys size = %I64u] or fileTime", strTmpPath, pInfo->m_ulFileSize, fs.m_size));
				//pInfo->m_ulLocalFileSize = stStat64.st_size;
				pInfo->m_ulLocalFileSize = fs.m_size;
				return false;
			}//if
		}
		else if(IsTarFile(pInfo->m_strFileName))
		{
			//Tar 파일이면 ENC 폴더내의 Tar파일 이름의 폴더에서
			//Tar_Info.ini 파일의 size 값과 파일 사이즈를 비교하여 틀리다면 받는다.
			char cBuffer[BUF_SIZE] = { 0x00 };
			ULONGLONG ulTarSize = 0;
			int nIdx = strLocalPath.ReverseFind(_T('.'));
			CString strInfo = strLocalPath.Left(nIdx);
			strInfo += _T("\\Tar_Info.ini");
			GetPrivateProfileString(_T("Tar"), _T("size"), _T(""), cBuffer, BUF_SIZE, strInfo);
			ulTarSize = (ULONGLONG)_atoi64(cBuffer);
			if(ulTarSize != pInfo->m_ulFileSize)
			{
				//패키지파일과 로컬에 존재하는 파일의 사이즈가 틀린경우
				ciDEBUG(1, ("Different local file(%s) size [ini size = %I64u, sys size = %I64u]", strLocalPath, pInfo->m_ulFileSize, ulTarSize));
				pInfo->m_ulLocalFileSize = ulTarSize;
				return false;
			}
			else
			{
				// skpark same_size_file_problem을 Tar 파일에는 적용하지 않는다.
				pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
				return true;
			}//if
		}
		else
		{
			return false;
		}//if
	}//if
}

ULONGLONG CDownloader::GetLocalFileSize(CString& strLocalPath)
{
	CFileStatus fs;
	if(CFile::GetStatus(strLocalPath, fs) == TRUE)
	{
		return fs.m_size;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 해당 파일이 서버에 있는지 비교한다. \n
/// @param (CDownloadFile*) pInfo : (in/out) 컨텐츠 파일의 정보
/// @return <형: bool> \n
///			<true: 서버에 같은 파일이 있음> \n
///			<false: 서버에 같은 파일이 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::CompareRemoteContents(CDownloadFile* pInfo)
{
	CString strFindPath, strLocalFindPath;		//여기서 local은 로컬 단말이 아니라, 서브컨텐츠서버와 메인컨텐츠 서버의 의미...
	bool bExist = false;

	strLocalFindPath = LOCAL_CONTNETS + pInfo->m_strContentsPath + pInfo->m_strFileName;			//로컬의 컨텐츠 경로
	strFindPath = pInfo->m_strContentsRoot + pInfo->m_strContentsPath + pInfo->m_strFileName;		//메인의 컨텐츠 경로

	///////////////////////////////////////////////////////////////
	//FTP를 이용하여 서버에서 파일을 찾아서 파일의 사이즈를 비교한다.
	//"local_contents"에서 파일을 먼저 찾고 없다면 contents에서 찾는다.
	CFtpFileFind findFile(m_pFtpConnection);
	bExist = findFile.FindFile(strLocalFindPath);
	if(bExist)
	{
		findFile.FindNextFile();
		if(pInfo->m_ulFileSize == findFile.GetLength())
		{
			//"local_contents"에 파일이 있음...
			ciDEBUG(1,("[FTP] Found file = %s", strLocalFindPath));
			pInfo->m_strContentsRoot = LOCAL_CONTNETS;
			m_ulTotalSize += pInfo->m_ulFileSize;
			findFile.Close();
			return true;
		}//if

		bExist = false;
		ciDEBUG(1,("Different file size Package(%I64u) / Server(%I64u)", pInfo->m_ulFileSize, findFile.GetLength()));
	}//if

	ciERROR(("[FTP] Can not found file = %s", strLocalFindPath));
	
	//Find from NAS
	if(!bExist)
	{
		if(findFile.FindFile(strFindPath))
		{
			findFile.FindNextFile();

			ciDEBUG(1,("File size Package(%I64u) / NAS(%I64u)", pInfo->m_ulFileSize, findFile.GetLength()));

			if(pInfo->m_ulLocalFileSize == findFile.GetLength())
			{
				//NAS에 파일이 있고 로컬단말의 사이즈와 같은 경우에는 다운로드하지 않는다.
				ciWARN(("File size is different with package file ==> Local(%I64u) / Server(%I64u)", pInfo->m_ulLocalFileSize, findFile.GetLength()));
				pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
			}
			else
			{
				//NAS에 파일이 있고 로컬단말의 사이즈와 다른경우에만 다운로드를 한다.
				ciDEBUG(1,("[FTP] Found file = %s", strFindPath));
				m_ulTotalSize += pInfo->m_ulFileSize;
			}//if
			findFile.Close();
			return true;
		}//if
	}//if

	//remote 경로에 파일이 없음.
	ciERROR(("[FTP] Can not found file = %s", strFindPath));
	pInfo->SetEnd(E_REASON_FILE_NOTFOUND, E_STATE_COMPLETE_FAIL, 0, false);		
	findFile.Close();
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드할 파일들을 웹서버를 통하여 비교한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::CompareRemoteContentsViaHttp()
{
	ciDEBUG(1,("CompareRemoteContentsViaHttp(start)"));

	if(!m_pWebRef)
	{
		ciERROR(("Http file service not initialized"));
		m_bHasFail = true;
		return false;
	}//if

	CString strFullPath;
	CStringArray aryFilePath;
	CFileServiceWrap::SFileList listFileInfo;
	CDownloadFile* pInfo = NULL;


	// 1. get FilesInfo from local_contents
	aryFilePath.RemoveAll();
	listFileInfo.RemoveAll();
	for(int i=0; i<m_nTotalCount; i++)
	{
		pInfo = m_paryDownloadFile->GetAt(i);
		if(pInfo->m_nDownloadState != E_STATE_INIT)
		{
			continue;
		}//if

		// local_contents경로만 추가
		strFullPath = LOCAL_CONTNETS;
		strFullPath += pInfo->m_strContentsPath;
		strFullPath += pInfo->m_strFileName;
		aryFilePath.Add(strFullPath);
	}//for
	ciDEBUG(1,("get FilesInfo from local_contents (%d file)", aryFilePath.GetCount()));

	//웹서버를 통해서 파일들의 정보를 얻어온다.
	if(!m_pWebRef->FilesInfo(aryFilePath, listFileInfo))
	{
		ciERROR(("Fail to get HTTP FilesInfo : %s", m_pWebRef->GetErrorMsg()));
		m_bHasFail = true;
		return false;
	}//if

	//서버로부터 얻어온 정보를 맵에 저장한다.
	CMapStringToString mapString;
	CFileServiceWrap::SFileInfo infoFile;
	for(int i=0; i<listFileInfo.GetCount(); i++)
	{
		infoFile = listFileInfo.GetAt(i);
		mapString.SetAt(infoFile.strFullName, infoFile.strLength);
	}//if

	//다시 다운로드할 파일들의 사이즈를 맵에있는 사이즈와 비교한다.
	CString strLength;
	for(int i=0; i<m_nTotalCount; i++)
	{
		pInfo = m_paryDownloadFile->GetAt(i);
		if(pInfo->m_nDownloadState != E_STATE_INIT)
		{
			continue;
		}//if

		//local_contents 경로
		strFullPath = LOCAL_CONTNETS;
		strFullPath += pInfo->m_strContentsPath;
		strFullPath += pInfo->m_strFileName;
		if(mapString.Lookup(strFullPath, strLength))
		{
			if(pInfo->m_ulFileSize == _atoi64(strLength))
			{
				//"local_contents"에 파일이 있음...
				ciDEBUG(1,("[HTTP] Found file = %s", strFullPath));
				pInfo->m_strContentsRoot = LOCAL_CONTNETS;
				m_ulTotalSize += pInfo->m_ulFileSize;
				continue;
			}//if
		}//if

		//local_contents 경로에 파일이 없음.
		ciDEBUG(1,("[HTTP] Can not found file (from local_contents) = %s", strFullPath));
	}//for


	// 2. get FilesInfo from contents
	aryFilePath.RemoveAll();
	listFileInfo.RemoveAll();
	for(int i=0; i<m_nTotalCount; i++)
	{
		pInfo = m_paryDownloadFile->GetAt(i);
		if(pInfo->m_nDownloadState != E_STATE_INIT)
		{
			continue;
		}//if

		// root가 local_contents인 경우 => 이미 local_contents에서 찾았으므로 패스
		if( pInfo->m_strContentsRoot == LOCAL_CONTNETS )
			continue;

		// contents 경로를 추가
		strFullPath = pInfo->m_strContentsRoot;
		strFullPath += pInfo->m_strContentsPath;
		strFullPath += pInfo->m_strFileName;
		aryFilePath.Add(strFullPath);
	}//for

	//
	if( aryFilePath.GetCount() == 0 )
	{
		ciDEBUG(1,("All files is in local_contents"));
		ciDEBUG(1,("~CompareRemoteContentsViaHttp(end)"));
		return true;
	}
	ciDEBUG(1,("get FilesInfo from contents (%d file)", aryFilePath.GetCount()));

	//웹서버를 통해서 파일들의 정보를 얻어온다.
	if(!m_pWebRef->FilesInfo(aryFilePath, listFileInfo))
	{
		ciERROR(("Fail to get HTTP FilesInfo : %s", m_pWebRef->GetErrorMsg()));
		m_bHasFail = true;
		return false;
	}//if

	//서버로부터 얻어온 정보를 맵에 저장한다.
	mapString.RemoveAll();
	//CFileServiceWrap::SFileInfo infoFile;
	for(int i=0; i<listFileInfo.GetCount(); i++)
	{
		infoFile = listFileInfo.GetAt(i);
		mapString.SetAt(infoFile.strFullName, infoFile.strLength);
	}//if

	//다시 다운로드할 파일들의 사이즈를 맵에있는 사이즈와 비교한다.
	for(int i=0; i<m_nTotalCount; i++)
	{
		pInfo = m_paryDownloadFile->GetAt(i);
		if(pInfo->m_nDownloadState != E_STATE_INIT)
		{
			continue;
		}//if

		// root가 local_contents인 경우 => 이미 local_contents에서 찾았으므로 패스
		if( pInfo->m_strContentsRoot == LOCAL_CONTNETS )
			continue;

		//contents 경로
		strFullPath = pInfo->m_strContentsRoot;
		strFullPath += pInfo->m_strContentsPath;
		strFullPath += pInfo->m_strFileName;
		if(mapString.Lookup(strFullPath, strLength))
		{

			ciDEBUG(1,("File size Package(%I64u) / NAS(%s) / Local(%I64u)", 
				pInfo->m_ulFileSize, strLength, pInfo->m_ulLocalFileSize));

			if(pInfo->m_ulLocalFileSize == _atoi64(strLength))
			{
				//NAS에 파일이 있고 로컬단말의 사이즈와 같은 경우에는 다운로드하지 않는다.
				ciDEBUG(1, ("local File size is same as server file size  ==> Local(%I64u) / Server(%I64u)", pInfo->m_ulLocalFileSize, _atoi64(strLength)));
				pInfo->SetEnd(E_REASON_LOCAL_EXIST, E_STATE_COMPLETE_SUCCESS, 0, true);
			}
			else
			{
				//NAS에 파일이 있고 로컬단말의 사이즈와 다른경우에만 다운로드를 한다.
				//"contents"에 파일이 있음...
				ciDEBUG(1,("[HTTP] Found file = %s", strFullPath));
				m_ulTotalSize += pInfo->m_ulFileSize;
			}//if
			continue;
		}//if

		//local_contents 와 contents 경로에 파일이 없음.
		ciERROR(("[HTTP] Can not found file (from contents) = %s", strFullPath));
		pInfo->SetEnd(E_REASON_FILE_NOTFOUND, E_STATE_COMPLETE_FAIL, 0, false);
		m_bHasFail = true;
	}//for

	ciDEBUG(1,("~CompareRemoteContentsViaHttp(end)"));
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 배열의 파일들을 FTP로부터 다운로드한다. \n
/// @param (CFileServiceWrap::IProgressHandler*) pVoid : (in) Http 라이브러리에서 사용하기 위한 인터페이스 포인터
/// @return <형: bool> \n
///			<true: 다운로드 완료> \n
///			<false: 다운로드 취소> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::DownloadFiles(CFileServiceWrap::IProgressHandler* pVoid)
{
	if(m_bUseHttp)
	{
		return DownloadFilesViaHttp(pVoid);
	}//if

	m_nFailCount = 0;
	m_ulCompleteDownloadSize = 0;
	ULONGLONG ulCurrentDownloadSize = 0;
	ULONGLONG ulCurrentFileSize = 0;

	CString strStatus, strServerPath, strLocalFullPath, strLocalTempFullPath, strContentsRoot;
	CDownloadFile* pInfo = NULL;
	CFile* fileServer = NULL;
	CFile fileLocal;
	int nCountDownload = 0;
	int nRead = 0;
	int nRemainSecTotal, nPercentTotal;
	BYTE* pbtBuf = new BYTE[SIZE_FILE_BUF];

	//다운로드 상태를 알리는 주기
	CString strPeriod = GetINIValue(_T("UBCVariables.ini"), _T("ROOT"), _T("AgentConnectionPeriod"));
	DWORD dwPeriod = atoi(strPeriod)*1000;
	if(dwPeriod == 0)
	{
		dwPeriod = 60*1000;
	}//if
	DWORD dwEventTick = ::GetTickCount();
	DWORD dwEndTick;

	if(m_hWndNotify && m_bAsync)
	{
		strStatus.Format(_T("00%% (00:00:00)"));
		::SendMessage(m_hWndNotify, WM_PROGRESS_DOWNLOAD, 0, (LPARAM)(LPCSTR)strStatus);
	}//if

	for(m_nCurrentIndex=0; m_nCurrentIndex<m_nTotalCount; m_nCurrentIndex++)
	{
		if(m_bCancel)
		{
			delete[]pbtBuf;
			return false;
		}//if

		pInfo = m_paryDownloadFile->GetAt(m_nCurrentIndex);
		if(pInfo->m_nDownloadState != E_STATE_INIT)
		{
			continue;
		}//if

		//다운로드 시작
		pInfo->SetStart();

		m_ulCompleteDownloadSize += ulCurrentFileSize;
		ulCurrentFileSize = pInfo->m_ulFileSize;
		ulCurrentDownloadSize = 0;

		m_dwStartTick = 0;
		try
		{
			if(pInfo->m_strContentsRoot.GetLength() != 0)
			{
				strServerPath = pInfo->m_strContentsRoot;
				strServerPath.Append(pInfo->m_strContentsPath);
			}
			else
			{
				strServerPath = pInfo->m_strServerPath;
			}//if
			strServerPath.Append(pInfo->m_strFileName);

			GetLocalPath(strLocalFullPath, strLocalTempFullPath, pInfo->m_strServerPath, pInfo->m_strFileName);

			MakeSurePath(strLocalFullPath);
			MakeSurePath(strLocalTempFullPath);

		// download
			fileServer = m_pFtpConnection->OpenFile(strServerPath);
			if(!fileServer)
			{
				m_bHasFail = true;
				ciERROR(("Can't connect to ServerPath : %s", strServerPath));

				pInfo->SetEnd(E_REASON_FILE_NOTFOUND, E_STATE_COMPLETE_FAIL, 0, false);
				continue;
			}//if
		
			if(!fileLocal.Open(strLocalTempFullPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
			{
				m_bHasFail = true;
				ciERROR(("Can't create to LocalTempFullPath : %s", strLocalTempFullPath));

				pInfo->SetEnd(E_REASON_EXCEPTION_FILE, E_STATE_COMPLETE_FAIL, 0, false);
				fileServer->Close();
				delete fileServer;
				continue;
			}//if

			ciDEBUG(1, ("Begin download file : %s", strServerPath));
			m_dwStartTick = ::GetTickCount();
			memset(pbtBuf, 0x00, SIZE_FILE_BUF);

			// read & write
			try
			{
				nRead = 0;
				while((nRead = fileServer->Read(pbtBuf, SIZE_FILE_BUF)) != 0)
				{
					if(m_bCancel)
					{
						fileServer->Close();
						delete fileServer;
						fileLocal.Close();
						delete[]pbtBuf;
						return false;
					}//if

					fileLocal.Write(pbtBuf, nRead);
					ulCurrentDownloadSize += nRead;
					dwEndTick = ::GetTickCount();

					nRemainSecTotal = (dwEndTick-m_dwStartTick) * (m_ulTotalSize - m_ulCompleteDownloadSize - ulCurrentDownloadSize) / ulCurrentDownloadSize / 1000;
					CTimeSpan tmspRemainTotal(nRemainSecTotal);
					nPercentTotal = (m_ulCompleteDownloadSize+ulCurrentDownloadSize)*100/m_ulTotalSize;

					strStatus.Format(_T("Current download file : %s (%d/%d)   %02d%% (%s)"),
									pInfo->m_strFileName, m_nCurrentIndex+1, m_nTotalCount,
									nPercentTotal, tmspRemainTotal.Format(_T("%H:%M:%S")));
					if(m_hWndNotify && m_bAsync)
					{
						TRACE(_T("*****************************************************\r\n"));
						TRACE(_T("Percdent = %d\r\n"), nPercentTotal);
						TRACE(_T("Status = %s\r\n"), strStatus);
						TRACE(_T("*****************************************************\r\n"));
						::SendMessage(m_hWndNotify, WM_PROGRESS_DOWNLOAD, nPercentTotal*10, (LPARAM)(LPCSTR)strStatus);
					}//if
/*
					if(m_pDownResult && m_bUseReport)
					{
						m_pDownResult->m_nSuccessCount = nCountDownload;
						//다운로드 상태 알림
						if((dwEndTick - dwEventTick) >= dwPeriod)
						{
							m_pDownResult->SendUpdateEvent();
							dwEventTick = dwEndTick;
						}//if
					}//if
*/
				}//while
			}
			catch(CFileException* ex)
			{
				CString msg;
				TCHAR szCause[1024];
				ex->GetErrorMessage(szCause, 1024);
				msg.Format(_T("(%d) (%s) %s"), ex->m_cause, ex->m_strFileName, szCause);
				ex->Delete();

				ciERROR(("File R/W Exception : %s", msg));

				m_bHasFail = true;
				pInfo->SetEnd(E_REASON_EXCEPTION_FILE, E_STATE_COMPLETE_FAIL, 0, false);
				fileServer->Close();
				delete fileServer;
				fileLocal.Close();
				continue;
			}//try

			ciDEBUG(1, ("End download file : %s", strServerPath));
			fileLocal.Close();

			//Rename
			//폰트파일은 Windows/fonts 폴더에 복사해 준다.
			if(IsFontFile(strLocalTempFullPath))
			{
				if(!RegisterFont(pInfo->m_strFileName, strLocalTempFullPath))
				{
					m_bHasFail = true;
					ciERROR(("Font register fail = %s", pInfo->m_strFileName));
					pInfo->SetEnd(E_REASON_COPY_FAIL, E_STATE_COMPLETE_FAIL, 0, false);
				}
				else
				{
					pInfo->SetEnd(E_REASON_DOWNLOAD_SUCCESS, E_STATE_COMPLETE_SUCCESS, ulCurrentDownloadSize, true);
					ciDEBUG(1,("Font register success = %s", pInfo->m_strFileName));
				}//if
			}
			else if(IsPptFile(strLocalFullPath))
			{
				//PPT 파일은 읽기전용 속성을 해제하여 덮어써야한다.
				//읽기 전용속성을 주지 않으면 파일 Open과정에 파일의 크기가 변경되는 경우가 있다...
				SetFileAttributes(strLocalFullPath, FILE_ATTRIBUTE_NORMAL);		//읽기전용 해제

				if(!MoveFileEx(strLocalTempFullPath, strLocalFullPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
				{
					m_bHasFail = true;
					ciERROR(("Contents file replace fail : %s", strLocalFullPath));
					pInfo->SetEnd(E_REASON_COPY_FAIL, E_STATE_COMPLETE_FAIL, 0, false);
				}
				else
				{
					pInfo->SetEnd(E_REASON_DOWNLOAD_SUCCESS, E_STATE_COMPLETE_SUCCESS, ulCurrentDownloadSize, true);
				}//if

				SetFileAttributes(strLocalFullPath, FILE_ATTRIBUTE_READONLY);	//읽기전용 설정
			}
			else
			{
				if(!MoveFileEx(strLocalTempFullPath, strLocalFullPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
				{
					m_bHasFail = true;
					ciERROR(("Contents file replace fail : %s", strLocalFullPath));
					pInfo->SetEnd(E_REASON_COPY_FAIL, E_STATE_COMPLETE_FAIL, 0, false);
				}
				else
				{
					pInfo->SetEnd(E_REASON_DOWNLOAD_SUCCESS, E_STATE_COMPLETE_SUCCESS, ulCurrentDownloadSize, true);
				}//if
			}//if

			fileServer->Close();
			delete fileServer;
			nCountDownload++;

			if(m_pDownResult && m_bUseReport)
			{
				m_pDownResult->m_nSuccessCount = nCountDownload;
				//다운로드 상태 알림
				if((dwEndTick - dwEventTick) >= dwPeriod)
				{
					m_pDownResult->SendUpdateEvent();
					dwEventTick = dwEndTick;
				}//if
			}//if				

		}
		catch(CInternetException* ex)
		{
			CString msg;
			TCHAR szCause[1024];
			ex->GetErrorMessage(szCause, 1024);
			msg.Format(_T("(%d) %s"), ex->m_dwError, szCause);
			ex->Delete();

			ciERROR(("Internet Exception : %s", msg));

			m_bHasFail = true;
			pInfo->SetEnd(E_REASON_EXCEPTION_FTP, E_STATE_COMPLETE_FAIL, 0, false);
			/*fileServer->Close();
			delete fileServer;
			fileLocal.Close();*/
			//nCountProgress++;
		}//try
	}//for

	if(m_pDownResult && m_bUseReport)
	{
		m_pDownResult->EndDownload();
	}//if

	delete[]pbtBuf;
	return true;
}

bool CDownloader::DownloadFilesViaCache(CString strPackageId)
{
	ciDEBUG(1,("DownloadFilesViaCache(%s)", strPackageId));

	CString strServerPath, strLocalFullPath, strLocalTempFullPath, strContentsRoot;
	CDownloadFile* pInfo = NULL;

	CHttpDownload aObj(this);

	if(!aObj.InternetOpen()){
		ciERROR(("Fail to connect to internet"));
		return false;
	}

	CString url;
	url.Format("HTTP://%s", this->m_strCacheServer);
	aObj.SetURL(url);
	if(!aObj.ConnectTest()){
		ciERROR(("Fail to connect to %s", url));
		return false;
	}


	bool retval = true;
	for(m_nCurrentIndex=0; m_nCurrentIndex<m_nTotalCount; m_nCurrentIndex++)
	{
		if(m_bCancel)
		{
			return false;
		}//if

		pInfo = m_paryDownloadFile->GetAt(m_nCurrentIndex);
		ciDEBUG(1,("%s(%d/%d) file is ready to download", pInfo->m_strFileName, m_nCurrentIndex,m_nTotalCount));
		if(pInfo->m_nDownloadState != E_STATE_INIT)
		{
			ciDEBUG(1,("%s 's state is not init", pInfo->m_strFileName));
			continue;
		}//if

		if(pInfo->m_strContentsRoot.GetLength() != 0)
		{
			strServerPath = pInfo->m_strContentsRoot;
			strServerPath.Append(pInfo->m_strContentsPath);
		}
		else
		{
			strServerPath = pInfo->m_strServerPath;
		}//if
		
		CString subDir = "";
		ciStringTokenizer tokens(strServerPath,"/");
		if(tokens.countTokens() > 2){
			// 이경우 sub-directory 가 있다는 뜻이다.
			tokens.nextToken();
			tokens.nextToken();
			while(tokens.hasMoreTokens()){
				subDir.Append("/");
				subDir.Append(tokens.nextToken().c_str());
			}
		}
		CString cacheServerPath = "Contents/Enc";
		if(!subDir.IsEmpty()){
			cacheServerPath.Append(subDir);
		}

		strServerPath.Append(pInfo->m_strFileName);
		GetLocalPath(strLocalFullPath, strLocalTempFullPath, pInfo->m_strServerPath, pInfo->m_strFileName);

		MakeSurePath(strLocalFullPath);
		MakeSurePath(strLocalTempFullPath);

		//폰트파일은 Cache 에서는 취급하지 않는다.
		if(IsFontFile(strLocalTempFullPath))
		{
			continue;
		}
		//PPT파일은 Cache 에서는 취급하지 않는다.
		if(IsPptFile(strLocalFullPath))
		{
			continue;
		}

		
		CString cacheLocalPath = "";
		ciStringTokenizer dirTokens(strLocalTempFullPath,"\\");
		int len = dirTokens.countTokens();
		for(int i=1;i<len;i++){ // 한번 덜돌아서, 제일 마지막 파일명을 떨궈낸다.
			cacheLocalPath.Append(dirTokens.nextToken().c_str());
			if(i<len-1){
				cacheLocalPath.Append("\\");
			}
		}
		

		aObj.SetSourceDir(cacheServerPath);
		aObj.SetTargetDir(cacheLocalPath);

		// download
		//ciDEBUG2(1,("strLocalTempFullPath path = %s", strLocalTempFullPath));
		//ciDEBUG2(1,("cacheLocalPath path = %s", cacheLocalPath));
		//ciDEBUG2(1,("cacheServerPath = %s", cacheServerPath));
		//ciDEBUG2(1,("FileName = %s", pInfo->m_strFileName));

		//다운로드 시작
		pInfo->SetStartByCache();

		int downloadResult = aObj.DownloadFile(pInfo->m_strFileName, pInfo->m_ulFileSize);
		if(downloadResult < 0)
		{
			retval = false;
			ciERROR(("Cache Server GetFile Fail: %s, %s", aObj.getErrMsg(), strServerPath));
			continue;
		}else if(downloadResult == 0){
			retval = true;
			ciDEBUG(1, ("File Already Exist : %s, %s", aObj.getErrMsg(), strServerPath));
			continue;
		}//if
		ciDEBUG2(1,("download succeed [%s/%s] from Cache Server", cacheServerPath, pInfo->m_strFileName));

		if(!MoveFileEx(strLocalTempFullPath, strLocalFullPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
		{
			ciERROR(("Contents file replace fail : %s,%s",strLocalTempFullPath, strLocalFullPath));
			retval = false;
			continue;
		}

		// Local File Size 값을 다시 셋팅해 놓는다.
		ULONGLONG newLocalSize = GetLocalFileSize(strLocalFullPath);
		if(newLocalSize > 0){
			ciDEBUG2(1,("Contents file replace succeed : %s(Size=%I64u)", strLocalFullPath, newLocalSize));
			pInfo->m_ulLocalFileSize = newLocalSize;
			//skpark DownloadServer 2013.12.18  
			pInfo->m_strDownloadServer = "Cache://";
			pInfo->m_strDownloadServer += this->m_strCacheServer;
		}else{
			ciERROR(("Something Wrong !!! Contents file replace succeed : %s BUT Size is 0", strLocalFullPath));
		}
	}//for
	return retval;
}

bool CDownloader::SetEndByCache()
{
	ciDEBUG(1,("SetEndByCache()"));

	CDownloadFile* pInfo = NULL;
	int nTotalCount = m_paryDownloadFile->GetSize();
	int counter=0;
	for(int i=0; i<nTotalCount; i++)
	{
		if(m_bCancel)
		{
			return false;
		}//if
		pInfo = m_paryDownloadFile->GetAt(i);
		if(pInfo->m_nDownloadState != E_STATE_INIT)
		{
			continue;
		}//if
		if(pInfo->m_bStart ) {
			if(pInfo->m_strDownloadServer == this->m_strCacheServer) 
			{
				// strDownloadServer 값이 CacheServer 값과 같다는 얘기는
				// cache 서버로 부터 다운로드가 성공했다는 얘기가 된다.
				pInfo->SetEnd(E_REASON_DOWNLOAD_SUCCESS, E_STATE_COMPLETE_SUCCESS, pInfo->m_ulFileSize, true);
				counter++;
			}else{
				pInfo->SetEnd(E_REASON_CONNECTION_FAIL, E_STATE_COMPLETE_FAIL, 0, false);
			}
		}
	}//for
	if(counter == nTotalCount){
		ciDEBUG(1,("SetEndByCache(All files download succeed from Cache(%d files))", counter));
		return true;
	}
	ciDEBUG(1,("SetEndByCache(Part of files download failed from Cache(%d/%d files fail))", counter, nTotalCount));
	return false;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 배열의 파일들을 HTTP로 다운로드한다. \n
/// @param (CFileServiceWrap::IProgressHandler*) pVoid : (in) Http 라이브러리에서 사용하기 위한 인터페이스 포인터
/// @return <형: bool> \n
///			<true: 다운로드 완료> \n
///			<false: 다운로드 취소> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloader::DownloadFilesViaHttp(CFileServiceWrap::IProgressHandler* pVoid)
{
	if(!m_pWebRef)
	{
		ciERROR(("Http file service not initialized"));
		return false;
	}//if

	ciDEBUG(1,("DownloadFilesViaHttp()"));

	m_nFailCount = 0;
	m_ulCompleteDownloadSize = 0;

	CString strServerPath, strLocalFullPath, strLocalTempFullPath, strContentsRoot;
	CDownloadFile* pInfo = NULL;
	int nCountDownload = 0;

	//다운로드 상태를 알리는 주기
	CString strPeriod = GetINIValue(_T("UBCVariables.ini"), _T("ROOT"), _T("AgentConnectionPeriod"));
	DWORD dwPeriod = atoi(strPeriod)*1000;
	if(dwPeriod == 0)
	{
		dwPeriod = 60*1000;
	}//if
	DWORD dwEventTick = ::GetTickCount();
	DWORD dwEndTick;

	if(m_hWndNotify && m_bAsync)
	{
		CString strStatus;
		strStatus.Format(_T("00%% (00:00:00)"));
		::SendMessage(m_hWndNotify, WM_PROGRESS_DOWNLOAD, 0, (LPARAM)(LPCSTR)strStatus);
	}//if

	for(m_nCurrentIndex=0; m_nCurrentIndex<m_nTotalCount; m_nCurrentIndex++)
	{
		if(m_bCancel)
		{
			return false;
		}//if

		pInfo = m_paryDownloadFile->GetAt(m_nCurrentIndex);
		if(pInfo->m_nDownloadState != E_STATE_INIT)
		{
			continue;
		}//if

		//skpark DownloadServer 2013.12.18  
		CString serverBuf = m_pWebRef->GetUrl();
		//format = http://ip:port/UBC_HTTP/FileService.asmx
		if(serverBuf.GetLength() > strlen("http://.../UBC_HTTP/FileService.asmx")){
			if(serverBuf.Mid(serverBuf.GetLength()-strlen("/UBC_HTTP/FileService.asmx")) == "/UBC_HTTP/FileService.asmx"){
				serverBuf = serverBuf.Mid(0, serverBuf.GetLength()-strlen("/UBC_HTTP/FileService.asmx"));
			}
		}
		pInfo->m_strDownloadServer = serverBuf;


		//다운로드 시작
		pInfo->SetStart();
		m_ulPreDownloadSize = 0;

		if(pInfo->m_strContentsRoot.GetLength() != 0)
		{
			strServerPath = pInfo->m_strContentsRoot;
			strServerPath.Append(pInfo->m_strContentsPath);
		}
		else
		{
			strServerPath = pInfo->m_strServerPath;
		}//if
		strServerPath.Append(pInfo->m_strFileName);

		GetLocalPath(strLocalFullPath, strLocalTempFullPath, pInfo->m_strServerPath, pInfo->m_strFileName);

		MakeSurePath(strLocalFullPath);
		MakeSurePath(strLocalTempFullPath);

		// download

		//ciDEBUG2(1,("download strServerPath = %s", strServerPath));

		try {  // GetFile 은 맘대로 throw 하고...try catch 는 없고..
			if(!m_pWebRef->GetFile(strServerPath, strLocalTempFullPath, 0, pVoid))
			{
				m_bHasFail = true;
				ciERROR(("HTTP GetFile : %s, %s", m_pWebRef->GetErrorMsg(), strServerPath));

				pInfo->SetEnd(E_REASON_FILE_NOTFOUND, E_STATE_COMPLETE_FAIL, 0, false);
				continue;
			}//if
		}catch (CString err) {
			ciERROR(("HTTP GetFile : %s, %s", err, strServerPath));
			pInfo->SetEnd(E_REASON_FILE_NOTFOUND, E_STATE_COMPLETE_FAIL, 0, false);
			continue;
		}
		ciDEBUG2(1,("download success %s", strServerPath));

		//skpark same_size_file_problem
		//다운로드된 파일은, 파일의 시간을 모두 lastModifiedTime 으로 바꾼다. (그럴필요가 없다...)
		//if(pInfo->m_strLastModifiedTime.IsEmpty()){
		//}

		//Rename
		//폰트파일은 Windows/fonts 폴더에 복사해 준다.
		if(IsFontFile(strLocalTempFullPath))
		{
			if(!RegisterFont(pInfo->m_strFileName, strLocalTempFullPath))
			{
				m_bHasFail = true;
				ciERROR(("Font register fail = %s", pInfo->m_strFileName));
				pInfo->SetEnd(E_REASON_COPY_FAIL, E_STATE_COMPLETE_FAIL, 0, false);
			}
			else
			{
				pInfo->SetEnd(E_REASON_DOWNLOAD_SUCCESS, E_STATE_COMPLETE_SUCCESS, pInfo->m_ulFileSize, true);
				ciDEBUG(1,("Font register success = %s", pInfo->m_strFileName));
			}//if
		}
		else if(IsPptFile(strLocalFullPath))
		{
			//PPT 파일은 읽기전용 속성을 해제하여 덮어써야한다.
			//읽기 전용속성을 주지 않으면 파일 Open과정에 파일의 크기가 변경되는 경우가 있다...
			SetFileAttributes(strLocalFullPath, FILE_ATTRIBUTE_NORMAL);		//읽기전용 해제

			if(!MoveFileEx(strLocalTempFullPath, strLocalFullPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
			{
				m_bHasFail = true;
				ciERROR(("Contents file replace fail : %s", strLocalFullPath));
				pInfo->SetEnd(E_REASON_COPY_FAIL, E_STATE_COMPLETE_FAIL, 0, false);
			}
			else
			{
				pInfo->SetEnd(E_REASON_DOWNLOAD_SUCCESS, E_STATE_COMPLETE_SUCCESS, pInfo->m_ulFileSize, true);
			}//if

			SetFileAttributes(strLocalFullPath, FILE_ATTRIBUTE_READONLY);	//읽기전용 설정
		}
		else
		{
			if(!MoveFileEx(strLocalTempFullPath, strLocalFullPath, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
			{
				m_bHasFail = true;
				ciERROR(("Contents file replace fail : %s", strLocalFullPath));
				pInfo->SetEnd(E_REASON_COPY_FAIL, E_STATE_COMPLETE_FAIL, 0, false);
			}
			else
			{
				pInfo->SetEnd(E_REASON_DOWNLOAD_SUCCESS, E_STATE_COMPLETE_SUCCESS, pInfo->m_ulFileSize, true);
			}//if
		}//if


		nCountDownload++;
		dwEndTick = ::GetTickCount();

		if(m_pDownResult && m_bUseReport)
		{
			m_pDownResult->m_nSuccessCount = nCountDownload;
			//다운로드 상태 알림
			if((dwEndTick - dwEventTick) >= dwPeriod)
			{
				m_pDownResult->SendUpdateEvent();
				dwEventTick = dwEndTick;
			}//if
		}//if				
	}//for

	if(m_pDownResult && m_bUseReport)
	{
		m_pDownResult->EndDownload();
	}//if

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// HTTP file service의 업로드/다운로드 상태처리 \n
/// @param (FS_PROGRESS_FLAG) nFlag : (in) 상태처리 결과
/// @param (ULONGLONG) uCurrentBytes : (in) 처리중인 byte
/// @param (ULONGLONG) uTotalBytes : (in) 처리해야하는 전체 byte
/// @param (CString) strLocalPath : (in) 다운로드중인 파일의 경로
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uCurrentBytes, ULONGLONG uTotalBytes, CString strLocalPath)
{
	if(nFlag != CFileServiceWrap::FS_PROGRESS_FLAG::FS_DOING)
	{
		return;
	}//if


	TCHAR drive[MAX_PATH], dir[MAX_PATH], fname[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(strLocalPath, drive, dir, fname, ext);
	CString strFileName, strStatus;
	strFileName.Format(_T("%s%s"), fname, ext);

	DWORD dwEndTick = ::GetTickCount();

	ULONGLONG ulGapSize = uCurrentBytes - m_ulPreDownloadSize;
	m_ulPreDownloadSize = uCurrentBytes;
	m_ulCompleteDownloadSize += ulGapSize;


	DWORD dwElapse = dwEndTick-m_dwStartTick;
	if(dwElapse == 0)
	{
		dwElapse = 1;
	}//if

	int nBps = (ulGapSize*1000 / dwElapse);
	if(nBps == 0)
	{
		nBps = 1;	// 0으로 나누어지는것 방지
	}//if

	int nRemainSecTotal = (m_ulTotalSize-m_ulCompleteDownloadSize) / nBps;

	CTimeSpan tmspRemainTotal(nRemainSecTotal);
	int nPercentTotal = (m_ulCompleteDownloadSize)*100/m_ulTotalSize;
	strStatus.Format(_T("Current download file : %s (%d/%d)   %02d%% (%s)"),
					strFileName, m_nCurrentIndex+1, m_nTotalCount,
					nPercentTotal, tmspRemainTotal.Format(_T("%H:%M:%S")));
	if(m_hWndNotify && m_bAsync)
	{
		TRACE(_T("*****************************************************\r\n"));
		TRACE(_T("Percent = %d\r\n"), nPercentTotal);
		TRACE(_T("Status = %s\r\n"), strStatus);
		TRACE(_T("*****************************************************\r\n"));
		::SendMessage(m_hWndNotify, WM_PROGRESS_DOWNLOAD, nPercentTotal*10, (LPARAM)(LPCSTR)strStatus);
	}//if

	m_dwStartTick = dwEndTick;
}

// skpark DownloadServer : Cache 로 부터 download 받을때만 호출된다.
void CDownloader::OnConnectError(int errCode, CString errMsg)  
{
	ciERROR(("OnConnectError(%d, %s)", errCode, errMsg));
	::SendMessage(m_hWndNotify, WM_PROGRESS_DOWNLOAD, 0, (LPARAM)(LPCSTR)errMsg);
}
void CDownloader::OnBeforeDownload(CString filename, unsigned long srcFileSize) 
{
	CString strStatus;
	int nPercentTotal = 0;
	strStatus.Format(_T("Download From Cache :  download start : %s (%d/%d)   %02d%%"),
					filename, m_nCurrentIndex+1, m_nTotalCount,	nPercentTotal);

	ciDEBUG(1,("OnBeforeDownload(%s,%d)", strStatus, nPercentTotal));
	::SendMessage(m_hWndNotify, WM_PROGRESS_DOWNLOAD, nPercentTotal*10, (LPARAM)(LPCSTR)strStatus);
}
void CDownloader::OnDownload(CString filename, unsigned long srcFileSize, unsigned long recvFileSize) 
{
	static int prev_percent =  -1;
	CString strStatus;
	int nPercentTotal = int((float(recvFileSize)/float(srcFileSize))*100.0);
	if(nPercentTotal != 100 && nPercentTotal == prev_percent){
		return; 
	}
	prev_percent = nPercentTotal;
	strStatus.Format(_T("Download From Cache : downloading ... : %s (%d/%d)   %02d%%"),
					filename, m_nCurrentIndex+1, m_nTotalCount,	nPercentTotal);

	//ciDEBUG(1,("OnDownload(%s,%d)", strStatus, nPercentTotal));
	::SendMessage(m_hWndNotify, WM_PROGRESS_DOWNLOAD, nPercentTotal*10, (LPARAM)(LPCSTR)strStatus);
}
void CDownloader::OnAfterDownload(int errCode, CString errMsg, CString filename, unsigned long srcFileSize, unsigned long recvFileSize) 
{
	CString strStatus;
	int nPercentTotal = int((float(recvFileSize)/float(srcFileSize))*100.0);

	if(errCode == 2) { // Already Exist;
		strStatus = errMsg;
		nPercentTotal = 100;
	}else if(errCode == 1){ // Succeed
		strStatus.Format(_T("Download From Cache : download succeed : %s (%d/%d)   %02d%%"),
						filename, m_nCurrentIndex+1, m_nTotalCount,	nPercentTotal);
	}else{
		strStatus = errMsg;
	}
	ciDEBUG(1,("OnAfterDownload(%s,%d)", strStatus, nPercentTotal));
	::SendMessage(m_hWndNotify, WM_PROGRESS_DOWNLOAD, nPercentTotal*10, (LPARAM)(LPCSTR)strStatus);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 배열을 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::ClearDownloadArray()
{
	if(m_paryDownloadFile)
	{
		delete m_paryDownloadFile;
		m_paryDownloadFile = NULL;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 결과를 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::ClearDownloadResult()
{
	if(m_pDownResult)
	{
		delete m_pDownResult;
		m_pDownResult = NULL;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 작업을 정리한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::ClearDownload()
{
	if(!m_bUseHttp)
	{
		if(m_pFtpConnection)
		{
			m_pFtpConnection->Close();
			delete m_pFtpConnection;
			m_pFtpConnection = NULL;
		}//if
		//m_ssInternet.Close();
	}
	else
	{
		if(m_pWebRef)
		{
			delete m_pWebRef;
			m_pWebRef = NULL;
		}//if
	}//if

	ClearDownloadArray();
	ClearDownloadResult();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 전송상태 메시지를 전송한다. \n
/// @param (int) nState : (in) 다운로드 상태 값
/// @param (int) nRet : (in) 종료결과 값
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::SendStateMsg(int nState, int nRet)
{
	if(m_hWndNotify && m_bAsync)
	{
		::PostMessage(m_hWndNotify, WM_STATE_DOWNLOAD, (WPARAM)nState, (LPARAM)nRet);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// PlayContetns 다운로드 thread \n
/// @param (LPVOID) param : (in) ST_DOWNLOAD_THREAD_PARAM
/// @return <형: UINT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CDownloader::ThreadDownloadPlayContents(LPVOID param)
{
	ciDEBUG(1, ("Begin download playcontents thread"));

	ST_DOWNLOAD_THREAD_PARAM* pParam = (ST_DOWNLOAD_THREAD_PARAM*)param;
	CDownloader* pParent = (CDownloader*)pParam->pParentWnd;

	if(pParent->m_bUseHttp)
	{
		CoInitializeEx(NULL, COINIT_MULTITHREADED);
	}//if

	//콘텐츠 패키지 분석
	pParent->SendStateMsg(E_DNSTATE_CHECK_FILES, 0);
	if(!pParent->MakePlayContentsList())
	{
		ciERROR(("Fail to MakePlayContentsList"));
		pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_FAIL_EMPTY_LIST);
		
		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		ciDEBUG(1, ("End download playcontents thread"));
		return 0;
	}//if

	//로컬에 콘텐츠 파일들이 있는지 검사
	if(!pParent->CheckLocalFileSize(pParam->strPackageId, pParam->nBrowserId))
	{
		ciDEBUG(1,("Download size is 0"));
		pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);	//다운로드 받을것이 없다.

		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		ciDEBUG(1, ("End download playcontents thread"));
		return 0;
	}//if

	//skpark Cache Server Start [
	//접속정보 얻어오기
	bool bDownloadedByCache=false;  // skpark DownloadServer
	if(pParent->IsViaCache())
	{
		if(pParent->DownloadFilesViaCache(pParam->strPackageId))
		{
			ciDEBUG(1, ("Success download playcontents from Cache Server"));
			bDownloadedByCache = true;
		}
		else
		{
			ciWARN(("Fail to download playcontents from Cache Server"));
		}
	}
	//skpark Cache Server End ]


	//접속정보 얻어오기
	pParent->SendStateMsg(E_DNSTATE_GET_ADDR, 0);
	if(!pParent->GetFtpAddress(pParam->strPackageSiteId, pParam->strPackageId)) // skpark Cache 처리 완료
	{
		ciERROR(("Fail to get ftp address"));
		if(bDownloadedByCache && pParent->SetEndByCache()) 
		{
			pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);	// Cache 에 의해 이미 받았음.
		}
		else
		{
			pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_FAIL_FTP_ADDR);
		}

		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		ciDEBUG(1, ("End download playcontents thread"));
		return 0;
	}//if

	//서버접속
	pParent->SendStateMsg(E_DNSTATE_CONNECTING, 0);
	if(!pParent->ConnectFtp())
	{
		ciERROR(("FTP Connect fail"));
		if(bDownloadedByCache && pParent->SetEndByCache()) 
		{
			pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);	// Cache 에 의해 이미 받았음.
		}
		else
		{
			pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_FAIL_FTP_CONNECT);
		}
		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		ciDEBUG(1, ("End download playcontents thread"));
		return 0;
	}//if

	//서버에 콘텐츠 파일들이 있는지 검사
	pParent->SendStateMsg(E_DNSTATE_CHECK_FILESIZE, 0);
	if(!pParent->CheckRemoteFileSize(pParam->strPackageId, pParam->nBrowserId))	//사이즈 비교
	{
		ciDEBUG(1,("Download size is 0"));
		pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);	//다운로드 받을것이 없다.
		pParent->ClearDownload();
		
		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		ciDEBUG(1, ("End download playcontents thread"));
		return 0;
	}//if

	//파일 다운로드
	pParent->SendStateMsg(E_DNSTATE_DOWNLOADING, 0);
	if(!pParent->DownloadFiles((CFileServiceWrap::IProgressHandler*)pParent))	//파일 다운로드
	{
		ciDEBUG(1,("Download canceled"));
		pParent->SendStateMsg(E_DNSTATE_CANCEL_DOWNLOAD, E_DNRET_CANCEL);
		pParent->ClearDownload();

		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		ciDEBUG(1, ("End download playcontents thread"));
		return 0;
	}//if

	pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);
	pParent->ClearDownload();

	ciDEBUG(1, ("Success download playcontents"));

	if(pParent->m_bUseHttp)
	{
		CoUninitialize();
	}//if
	delete pParam;
	ciDEBUG(1, ("End download playcontents thread"));
	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Contetns 다운로드 thread \n
/// @param (LPVOID) param : (in) ST_DOWNLOAD_THREAD_PARAM
/// @return <형: UINT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CDownloader::ThreadDownloadAllContents(LPVOID param)
{
	ST_DOWNLOAD_THREAD_PARAM* pParam = (ST_DOWNLOAD_THREAD_PARAM*)param;
	CDownloader* pParent = (CDownloader*)pParam->pParentWnd;

	if(pParent->m_bUseHttp)
	{
		CoInitializeEx(NULL, COINIT_MULTITHREADED);
	}//if

	//콘텐츠 패키지 분석
	pParent->SendStateMsg(E_DNSTATE_CHECK_FILES, 0);
	if(!pParent->MakeContentsList())
	{
		ciERROR(("Fail to MakePlayContentsList"));
		pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_FAIL_EMPTY_LIST);
		
		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		return 0;
	}//if

	//로컬에 콘텐츠 파일들이 있는지 검사
	if(!pParent->CheckLocalFileSize(pParam->strPackageId, pParam->nBrowserId))
	{
		ciDEBUG(1,("Download size is 0"));
		pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);	//다운로드 받을것이 없다.

		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		return 0;
	}//if

	//skpark Cache Server Start [
	//접속정보 얻어오기
	bool bDownloadedByCache=false;  // skpark DownloadServer
	if(pParent->IsViaCache())
	{
		if(pParent->DownloadFilesViaCache(pParam->strPackageId))
		{
			bDownloadedByCache = true;
			ciDEBUG(1, ("Success download playcontents from Cache Server"));
		}
		else
		{
			ciWARN(("Fail to download playcontents from Cache Server"));
		}
	}
	//skpark Cache Server End ]

	//접속정보 얻어오기
	pParent->SendStateMsg(E_DNSTATE_GET_ADDR, 0);
	if(!pParent->GetFtpAddress(pParam->strPackageSiteId, pParam->strPackageId)) //skpark Cache 처리 완료
	{
		ciERROR(("Fail to get ftp address"));
		if(bDownloadedByCache && pParent->SetEndByCache()) 
		{
			pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);	// Cache 에 의해 이미 받았음.
		}
		else
		{
			pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_FAIL_FTP_ADDR);
		}
		
		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		return 0;
	}//if

	//서버접속
	pParent->SendStateMsg(E_DNSTATE_CONNECTING, 0);
	if(!pParent->ConnectFtp())
	{
		ciERROR(("FTP Connect fail"));
		if(bDownloadedByCache && pParent->SetEndByCache()) 
		{
			pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);	// Cache 에 의해 이미 받았음.
		}
		else
		{
			pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_FAIL_FTP_CONNECT);
		}	
		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		return 0;
	}//if

	//서버에 콘텐츠 파일들이 있는지 검사
	pParent->SendStateMsg(E_DNSTATE_CHECK_FILESIZE, 0);
	if(!pParent->CheckRemoteFileSize(pParam->strPackageId, pParam->nBrowserId))	//사이즈 비교
	{
		ciDEBUG(1,("Download size is 0"));
		pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);	//다운로드 받을것이 없다.
		pParent->ClearDownload();
		
		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		return 0;
	}//if

	//파일 다운로드
	pParent->SendStateMsg(E_DNSTATE_DOWNLOADING, 0);
	if(!pParent->DownloadFiles((CFileServiceWrap::IProgressHandler*)pParent))	//파일 다운로드
	{
		ciDEBUG(1,("Download canceled"));
		pParent->SendStateMsg(E_DNSTATE_CANCEL_DOWNLOAD, E_DNRET_CANCEL);
		pParent->ClearDownload();

		if(pParent->m_bUseHttp)
		{
			CoUninitialize();
		}//if
		return 0;
	}//if

	pParent->SendStateMsg(E_DNSTATE_COMPLETE_DOWNLOAD, E_DNRET_SUCCESS);
	pParent->ClearDownload();

	ciDEBUG(1, ("Success download playcontents"));

	if(pParent->m_bUseHttp)
	{
		CoUninitialize();
	}//if
	delete pParam;
	return 0;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일의 경로를 ENC와 Tmp경로로 변경하여 준다. \n
/// @param (CString&) strLocalPath : (out) ENC 경로로 변환한 파일의 전체 경로
/// @param (CString&) strLocalTempPath : (out) Tmp 경로로 변환한 파일의 전체 경로
/// @param (LPCSTR) lpszLocation : (in) 파일의 location 필드 값
/// @param (LPCSTR) lpszFilename : (in) 파일의 이름
/// @return <형: bool> \n
///			<값: 설명> \n
///			<값: 설명> \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::GetLocalPath(CString& strLocalPath, CString& strLocalTempPath, LPCSTR lpszLocation, LPCSTR lpszFilename)
{
	//일반 컨텐츠의 location 값은 /contents/<패키지명|컨텐츠 guid>/...
	//즉, 상위 2단계까지가 서버의 경로를 나타내며 그 이후의 경로는
	//플래쉬 서브폴더 등과 같은 컨텐츠에 종속된 하위 폴더경로이다.
	//로컬에서는 상위2단계 밑의 하위 경로만 취급한다.
	//==> m_nCutLevel값에 따라서 자르는 단계를 조정하도록 수정
	
	int nPos = 0;
	int nLevel = 0;
	CString strLocation = lpszLocation;
	if(strLocation.GetLength() != 0 && m_nCutLevel > 0)
	{
		while(nLevel < m_nCutLevel)
		{
			nPos = strLocation.Find(_T("/"), nPos+1);
			if(nPos == -1)
			{
				break;
			}//if

			nLevel++;
		}//while
	}//if
	strLocation.Delete(0, nPos+1);	

	strLocalPath = m_strContentsPath;
	if(strLocation.GetLength() != 0)
	{
		strLocalPath += strLocation;
		if(strLocalPath[strLocalPath.GetLength()-1] != _T('\\'))
		{
			strLocalPath += _T("\\");
		}//if
	}//if
	strLocalPath += lpszFilename;

	strLocalTempPath = m_strTmpPath;
	//실행 요청된 프로세서에 따라서 temp 폴더의 경로를 나누어준다.
	//BRW1, BRW2, FirmwareView
	CString strAppName = ::AfxGetAppName();
	if(strAppName.GetLength() != 0)
	{
		strLocalTempPath += strAppName;
		strLocalTempPath += _T("\\");
	}//if
	if(strLocation.GetLength() != 0)
	{
		strLocalTempPath += strLocation;
		if(strLocalTempPath[strLocalTempPath.GetLength()-1] != _T('\\'))
		{
			strLocalTempPath += _T("\\");
		}//if
	}//if
	strLocalTempPath += lpszFilename;

	//LocalFullPath를 실제 파일이 존재하는지 확인하여
	//존재하는 파일의 경로라면 중간에 ".."등과 같은 상대경로 문자를 제거하고
	//정확한 경로로 만들어 준다.
	CFileStatus status;
	CFile::GetStatus(strLocalPath, status);
	strLocalPath = status.m_szFullName;

	CFile::GetStatus(strLocalTempPath, status);
	strLocalTempPath = status.m_szFullName;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드결과 실패한 파일의 개수를 설정한다. \n
/// @param (int) nFailCount : (in) 실패한 개수
/////////////////////////////////////////////////////////////////////////////////
void CDownloader::SetFailCount(int nFailCount)
{
	m_nFailCount  = nFailCount;
	if(m_hWndNotify && m_bAsync)
	{
		::PostMessage(m_hWndNotify, WM_FAILCOUNT_DOWNLOAD, (WPARAM)nFailCount, (LPARAM)m_nTotalCount);
	}//if
}


bool CDownloader::GetFile(LPCTSTR szFile, LPCTSTR szRemote, LPCTSTR szLocal, LPCTSTR szSite)
{
	bool bRet = false;

	ubcMuxData* aData = NULL;
	if(szSite){
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(szSite);
	}
	if(!aData){
		return bRet;
	}

	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	if(m_bUseHttp)
	{
		CFileServiceWrap objFileSvcClient(aData->getFTPAddress_s().c_str(), aData->ftpPort);

		if(!objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str()))
		{
			ciERROR(("GetFile ERROR : %s", objFileSvcClient.GetErrorMsg()));
			return bRet;
		}

		bRet = objFileSvcClient.GetFile(CString(szRemote)+CString(szFile), CString(szLocal)+CString(szFile), 0, NULL);
		if(!bRet)
		{
			ciERROR(("GetFile ERROR : %s", objFileSvcClient.GetErrorMsg()));
			return bRet;
		}

		objFileSvcClient.Logout();
	}
	else
	{
		/*
		nsFTP::CFTPClient FtpClient;

		nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
		if (!FtpClient.Login(loginInfo)) {
			return bRet;
		}

		FtpClient.SetResumeMode(false);
		FtpClient.ChangeWorkingDirectory("/");

		bRet = FtpClient.DownloadFile(
					tstring(szRemote)+tstring(szFile), 
					tstring(szLocal)+tstring(szFile),
					nsFTP::CRepresentation(nsFTP::CType::Image()), true);

		FtpClient.Logout();
		*/
		ciERROR(("It's FTP version,  GetFile does not support FTP"));
		return ciFalse;
	}

	// 파일 다운로드가 안됐지만 파일이 생성된경우 파일을 삭제해야 함.
	if(!bRet){
		CString szBuf = szLocal;
		szBuf += szFile;

		CFileFind ff;
		if(ff.FindFile(szBuf)){
			::DeleteFile(szBuf);
		}
		ff.Close();
	}

	return bRet;
}


bool CDownloader::GetFile(LPCTSTR ip, ULONG port, LPCTSTR szFile, LPCTSTR szRemote, LPCTSTR szLocal, LPCTSTR szSite)
{
	bool bRet = false;

	ubcMuxData* aData = NULL;
	if(szSite){
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(szSite);
	}
	if(!aData){
		return bRet;
	}

	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	if(m_bUseHttp)
	{
		CFileServiceWrap objFileSvcClient(ip, port);

		if(!objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str()))
		{
			ciERROR(("GetFile ERROR : %s", objFileSvcClient.GetErrorMsg()));
			return bRet;
		}

		bRet = objFileSvcClient.GetFile(CString(szRemote)+CString(szFile), CString(szLocal)+CString(szFile), 0, NULL);
		if(!bRet)
		{
			ciERROR(("GetFile ERROR : %s", objFileSvcClient.GetErrorMsg()));
			return bRet;
		}

		objFileSvcClient.Logout();
	}
	else
	{
		/*
		nsFTP::CFTPClient FtpClient;

		nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
		if (!FtpClient.Login(loginInfo)) {
			return bRet;
		}

		FtpClient.SetResumeMode(false);
		FtpClient.ChangeWorkingDirectory("/");

		bRet = FtpClient.DownloadFile(
					tstring(szRemote)+tstring(szFile), 
					tstring(szLocal)+tstring(szFile),
					nsFTP::CRepresentation(nsFTP::CType::Image()), true);

		FtpClient.Logout();
		*/
		ciERROR(("It's FTP version,  GetFile does not support FTP"));
		return ciFalse;
	}

	// 파일 다운로드가 안됐지만 파일이 생성된경우 파일을 삭제해야 함.
	if(!bRet){
		CString szBuf = szLocal;
		szBuf += szFile;

		CFileFind ff;
		if(ff.FindFile(szBuf)){
			::DeleteFile(szBuf);
		}
		ff.Close();
	}

	return bRet;
}


bool			
CDownloader::GetFile(LPCTSTR file, ULONGLONG fileSize, LPCTSTR annoId, LPCTSTR siteId, LPCTSTR subdir)
{
	ciDEBUG(1,("GetFile(%s)", file));

	char	szBuf[_MAX_PATH] = { 0x00 };
	::GetModuleFileName(NULL, szBuf, _MAX_PATH);

	char drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
	_splitpath(szBuf, drive, path, filename, ext);

	CString remote1, remote2;
	CString local, temp, localFile, tempFile;

	remote1.Format("\\local_Contents\\%s\\%s\\", subdir, annoId);
	remote2.Format("\\Contents\\%s\\%s\\", subdir, annoId);
	local.Format("%s\\SQISoft\\Contents\\Enc\\%s\\", drive,subdir);
	temp.Format("%s\\SQISoft\\Contents\\Temp\\%s\\", drive,subdir);
	localFile.Format("%s%s", local, file);
	tempFile.Format("%s%s", temp, file);

	ciDEBUG(1, ("GetFile(%s,%s,%s,%s)", file,remote1,local,siteId));

	CFileStatus fs;
	if(CFile::GetStatus(localFile, fs))
	{
		// 로컬에 파일이 있고, 사이즈가 같다.
		if(fileSize > 0 && fs.m_size == fileSize)
		{
			ciDEBUG(1,("%s already exist", localFile));
			return true;
		}
	}
	else if(CFile::GetStatus(tempFile, fs) == TRUE) 
	{
		if(fs.m_size == fileSize)
		{
			//Tmp폴더에 있는 파일은 ENC 폴더로 옮겨준다.
			MakeSurePath(localFile);
			
			if(!MoveFileEx(tempFile, localFile, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
			{
				ciERROR(("Contents file replace fail = %s", localFile));
				return false;
			}
			ciDEBUG(1,("%s move succeed", localFile));
			return true;
		}
	}
	//CString waitMsg;
	//waitMsg.Format(LoadStringById(IDS_ANNOUNCE_WAIT001), file);
	//CWaitMessageBox wait(waitMsg);

	// local_Contents 에서 1차 시도한다.
	if(this->GetFile(file, remote1, temp, siteId)){
		ciDEBUG(1, ("GetFile(%s,%s,%s,%s) succeed", file,remote1,temp,siteId));

		//Tmp폴더에 있는 파일은 ENC 폴더로 옮겨준다.
		MakeSurePath(localFile);
		if(!MoveFileEx(tempFile, localFile, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
		{
			ciERROR(("Contents file replace fail = %s", localFile));
			return false;
		}
		ciDEBUG(1,("%s move succeed", localFile));
		return true;
	}
	ciERROR(("Download %s%s  fail", remote1,file));
	// 실패하면 Contents 에서 2차 시도한다.
	if(this->GetFile(file, remote2, temp, siteId)){
		ciDEBUG(1, ("GetFile(%s,%s,%s,%s) succeed", file,remote2,temp,siteId));

		//Tmp폴더에 있는 파일은 ENC 폴더로 옮겨준다.
		MakeSurePath(localFile);
		if(!MoveFileEx(tempFile, localFile, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
		{
			ciERROR(("Contents file replace fail = %s", localFile));
			return false;
		}
		ciDEBUG(1,("%s move succeed", localFile));
		return true;
	}
	ciERROR(("Download %s%s  fail", remote2,file));
	return false;
}

bool			
CDownloader::GetFile(LPCTSTR ip1, LPCTSTR ip2, ULONG port,
					 LPCTSTR file, ULONGLONG fileSize, LPCTSTR annoId, LPCTSTR siteId, LPCTSTR subdir)
{
	ciDEBUG(1,("GetFile(%s,%s,%ld, %s)", ip1,ip2,port,file));

	char	szBuf[_MAX_PATH] = { 0x00 };
	::GetModuleFileName(NULL, szBuf, _MAX_PATH);

	char drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
	_splitpath(szBuf, drive, path, filename, ext);

	CString remote;
	CString local, temp, localFile, tempFile;

	remote.Format("\\Contents\\%s\\%s\\", subdir, annoId);
	local.Format("%s\\SQISoft\\Contents\\Enc\\%s\\", drive,subdir);
	temp.Format("%s\\SQISoft\\Contents\\Temp\\%s\\", drive,subdir);
	localFile.Format("%s%s", local, file);
	tempFile.Format("%s%s", temp, file);

	ciDEBUG(1, ("GetFile(%s,%s,%s,%s)", file,remote,local,siteId));

	CFileStatus fs;
	if(CFile::GetStatus(localFile, fs))
	{
		// 로컬에 파일이 있고, 사이즈가 같다.
		if(fileSize > 0 && fs.m_size == fileSize)
		{
			ciDEBUG(1,("%s already exist", localFile));
			return true;
		}
	}
	else if(CFile::GetStatus(tempFile, fs) == TRUE) 
	{
		if(fs.m_size == fileSize)
		{
			//Tmp폴더에 있는 파일은 ENC 폴더로 옮겨준다.
			MakeSurePath(localFile);
			
			if(!MoveFileEx(tempFile, localFile, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
			{
				ciERROR(("Contents file replace fail = %s", localFile));
				return false;
			}
			ciDEBUG(1,("%s move succeed", localFile));
			return true;
		}
	}
	//CString waitMsg;
	//waitMsg.Format(LoadStringById(IDS_ANNOUNCE_WAIT001), file);
	//CWaitMessageBox wait(waitMsg);

	// ip1 에서 1차 시도한다.
	if(this->GetFile(ip1,port,file, remote, temp, siteId)){
		ciDEBUG(1, ("GetFile(%s,%ld,%s,%s,%s,%s) succeed", ip1,port,file,remote,temp,siteId));

		//Tmp폴더에 있는 파일은 ENC 폴더로 옮겨준다.
		MakeSurePath(localFile);
		if(!MoveFileEx(tempFile, localFile, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
		{
			ciERROR(("Contents file replace fail = %s", localFile));
			return false;
		}
		ciDEBUG(1,("%s move succeed", localFile));
		return true;
	}
	ciERROR(("Download %s%s  fail", remote,file));
	// 실패하면 ip2 에서 2차 시도한다.
	if(this->GetFile(ip2,port,file, remote, temp, siteId)){
		ciDEBUG(1, ("GetFile(%s,%ld,%s,%s,%s,%s) succeed", ip2,port,file,remote,temp,siteId));

		//Tmp폴더에 있는 파일은 ENC 폴더로 옮겨준다.
		MakeSurePath(localFile);
		if(!MoveFileEx(tempFile, localFile, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
		{
			ciERROR(("Contents file replace fail = %s", localFile));
			return false;
		}
		ciDEBUG(1,("%s move succeed", localFile));
		return true;
	}
	ciERROR(("Download %s%s  fail", remote,file));
	return false;
}

bool		
CDownloader::markNewFile(CString& filePath, CTime& localFileAccessTime,  CDownloadFile* pInfo)
{
	if(m_bCheckFileTime == false) return false;  // 이경우 timeCheck 은 실시하지 않는다.

	// skpark same_size_file_problem
	// 사이즈가 같은 경우에, 로컬화일의 시각과 Ini 의 lastModifiedTime 을 한번더 비교하여
	// lastModifiedTime 이 더 새것이면 파일이 다른것으로 본다.

	CString iniLastModifiedTime = pInfo->m_strLastModifiedTime;

	if(iniLastModifiedTime.IsEmpty() || iniLastModifiedTime.GetLength() != 19) {
		ciDEBUG(3,("%s lastModifiedTime is null", filePath));
		return false;
	}

	struct tm	c_tm;
	memset(&c_tm, 0x00, sizeof(struct tm));

	sscanf(iniLastModifiedTime.GetBuffer(), "%04d/%02d/%02d %02d:%02d:%02d",
		&c_tm.tm_year, &c_tm.tm_mon, &c_tm.tm_mday,
		&c_tm.tm_hour, &c_tm.tm_min, &c_tm.tm_sec);

	c_tm.tm_year -= 1900;
	c_tm.tm_mon -= 1;
	
	time_t now1;
	time(&now1);
	struct tm* localTime = localtime(&now1);
	c_tm.tm_isdst = localTime->tm_isdst;

	time_t ini_time = mktime(&c_tm);

	if(ini_time <= 0) {
		ciERROR(("lastModifiedTime format error %s  fail", iniLastModifiedTime));
		return false;
	}

	if(localFileAccessTime.GetTime() < ini_time){
		ciDEBUG(1,("%s fileTime is older than ini lastModifiedTime (%s)", filePath, iniLastModifiedTime));
		// 이 경우 다운로드후, 화일시간을 바꾸어야 함!!!!!!!!!!!!!!!!! 그러기 위해 마크를 해둠
		//pInfo->m_bModified = true;  // 그럴필요가 없어졌음.
		return true;
	}
	ciDEBUG(1,("%s fileTime is NOT older than ini lastModifiedTime (%s)", filePath, iniLastModifiedTime));
	return false;
}
