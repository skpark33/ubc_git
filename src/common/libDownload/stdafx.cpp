// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// libDownload.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"
#include <io.h>
#include "Dbghelp.h"

namespace libDownload
{

// TODO: 필요한 추가 헤더는
// 이 파일이 아닌 STDAFX.H에서 참조합니다.

LPCTSTR GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}

LPCTSTR	GetSiteID()
{
	static CString strSiteID = _T("");

	if(strSiteID.GetLength() == 0)
	{
		strSiteID = GetINIValue(_T("UBCVariables.ini"), _T("ROOT"), _T("SiteId"));
	}//if

	return strSiteID;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 ini 파일의 값을 반환한다. \n
/// @param (CString) strFileName : (in) ini파일 이름
/// @param (CString) strSection : (in) ini파일의 section 
/// @param (CString) strKey : (in) 구하려는 값의 key값
/// @return <형: CString> \n
///			<key값의 value> \n
/////////////////////////////////////////////////////////////////////////////////
CString GetINIValue(CString strFileName, CString strSection, CString strKey)
{
	TCHAR cBuffer[BUF_SIZE] = { 0x00 };
	CString strPath = GetAppPath();
	strPath += _T("data\\");
	strPath += strFileName;
	GetPrivateProfileString(strSection, strKey, _T(""), cBuffer, BUF_SIZE, strPath);

	return CString(cBuffer);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 ini 파일의 key에 값을 쓴다. \n
/// @param (CString) strFileName : (in) ini파일 이름
/// @param (CString) strSection : (in) ini파일의 section
/// @param (CString) strKey : (in) 기록하려는 값의 key값
/// @param (CString) strValue : (in) 기록하려는 값
/////////////////////////////////////////////////////////////////////////////////
void WriteINIValue(CString strFileName, CString strSection, CString strKey, CString strValue)
{
	CString strPath = GetAppPath();
	strPath += _T("data\\");
	strPath += strFileName;
	WritePrivateProfileString(strSection, strKey, strValue, strPath);
}

CString ToString(int nValue)
{
	CString str;
	str.Format(_T("%d"), nValue);

	return str;
}

CString ToString(double fValue)
{
	CString str;
	str.Format(_T("%.3f"), fValue);

	return str;
}

CString ToString(ULONG ulvalue)
{
	CString str;
	str.Format(_T("%ld"), ulvalue);
	return str;
}

CString ToString(ULONGLONG ulvalue)
{
	CString str;
	str.Format(_T("%I64u"), ulvalue);
	return str;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 폰트파일인지 구분하여 준다. \n
/// @param (ciString) strFileName : (in) 파일 이름
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsFontFile(CString strFileName)
{
	CString strExt = _T("");
	int nlen = strFileName.GetLength();
	int i;
	for(i = nlen-1; i >= 0; i--)
	{
		if(strFileName[i] == _T('.'))
		{
			strExt = strFileName.Mid(i+1);
			break;
		}//if
	}//for

	if(strExt.CompareNoCase(_T("fon")) == 0 || strExt.CompareNoCase(_T("ttc")) == 0 || strExt.CompareNoCase(_T("ttf")) == 0)
	{
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ppt파일인지 구분하여 준다. \n
/// @param (ciString) strFileName : (in) 파일 이름
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsPptFile(CString strFileName)
{
	CString strExt = _T("");
	int nlen = strFileName.GetLength();
	int i;
	for(i = nlen-1; i >= 0; i--)
	{
		if(strFileName[i] == _T('.'))
		{
			strExt = strFileName.Mid(i+1);
			break;
		}//if
	}//for

	if(strExt.CompareNoCase(_T("ppt")) == 0 || strExt.CompareNoCase(_T("pptx")) == 0 || strExt.CompareNoCase(_T("pps")) == 0 || strExt.CompareNoCase(_T("ppsx")) == 0)
	{
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 폰트를 시스템이 등록한다. \n
/// @param (CString) strFontName : (in) 등록하려는 폰트이름
/// @param (CString) strFontPath : (in) 등록하려는 폰트파일의 경로
/// @return <boo: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool RegisterFont(CString strFontName, CString strFontPath)
{
	char szWinDir[MAX_PATH] = { 0x00 };
	if(GetWindowsDirectory(szWinDir, MAX_PATH) == 0)
	{
		TRACE(_T("Fail to get win dir\r\n"));
		return false;
	}//if
	
	CString strFontDir;
	strFontDir.Format(_T("%s\\fonts\\%s"), szWinDir, strFontName);
	
	//폰트파일이 없다면 이동...
	if(_access(strFontDir, 0) != 0)
	{
		if(!MoveFileEx(strFontPath, strFontDir, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
		{
			TRACE(_T("Fail to move font file = %s\r\n"), strFontPath);
			return false;
		}//if
	}//if

	//레지스트리를 검사하여 폰트가 등록이 안되어 있다면 등록해 준다.
	HKEY hKey = NULL;
	CString strRooyKey = _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Fonts\\");
	CString strOpenKey = strRooyKey;
	LONG lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE , strOpenKey, 0, KEY_ALL_ACCESS, &hKey);
	if(lResult != ERROR_SUCCESS)
	{
		TRACE(_T("Fail to get font registry\r\n"));
		return false;
	}//if

    TCHAR    achClass[MAX_PATH] = TEXT("");		// buffer for class name 
    DWORD    cchClassName = MAX_PATH;			// size of class string 
    DWORD    cSubKeys=0;						// number of subkeys 
    DWORD    cbMaxSubKey;						// longest subkey size 
    DWORD    cchMaxClass;						// longest class string 
    DWORD    cValues;							// number of values for key 
    DWORD    cchMaxValue;						// longest value name 
    DWORD    cbMaxValueData;					// longest value data 
    DWORD    cbSecurityDescriptor;				// size of security descriptor 
    FILETIME ftLastWriteTime;					// last write time 
 
    DWORD i; 
 
    TCHAR  achValue[MAX_VALUE_NAME]; 
    DWORD cchValue = MAX_VALUE_NAME; 
 
    // Get the class name and the value count. 
    lResult = RegQueryInfoKey(
        hKey,                    // key handle 
        achClass,                // buffer for class name 
        &cchClassName,           // size of class string 
        NULL,                    // reserved 
        &cSubKeys,               // number of subkeys 
        &cbMaxSubKey,            // longest subkey size 
        &cchMaxClass,            // longest class string 
        &cValues,                // number of values for this key 
        &cchMaxValue,            // longest value name 
        &cbMaxValueData,         // longest value data 
        &cbSecurityDescriptor,   // security descriptor 
        &ftLastWriteTime);       // last write time

	if(lResult != ERROR_SUCCESS)
	{
		TRACE(_T("Failt to get font key information\r\n"));
		return false;
	}//if
 
	CString strLog;
	bool bRegister = false;
	TCHAR buffer[256];
	DWORD dwSize;
    // Enumerate the key values. 
    if(cValues) 
    {
		//strLog.Format("\nNumber of values: %d\n", cValues);
        //TRACE(strLog);

        for(i=0, lResult=ERROR_SUCCESS; i<cValues; i++) 
        { 
            cchValue = MAX_VALUE_NAME; 
            achValue[0] = _T('\0'); 
            lResult = RegEnumValue(hKey, i, achValue, &cchValue, NULL, NULL,NULL,NULL);
            if(lResult == ERROR_SUCCESS) 
            { 
				//strLog.Format("(%d) %s\n", i+1, achValue);
                //TRACE(strLog);

				//Value의 값을 읽어서 추가하려는 폰트 파일이 등록되어 있는지 확인...
				memset(buffer, 0x00, 256);
				dwSize = 256;
				lResult = RegQueryValueEx(hKey, achValue, 0, NULL, (LPBYTE)buffer, &dwSize);
				if(lResult == ERROR_SUCCESS)
				{
					if(strFontName.CompareNoCase(CString(buffer)) == 0)
					{
						bRegister = true;
						break;
					}//if
				}//if
            }//if 
        }//for
    }//if

	if(bRegister)
	{
		return true;
	}//if

	//Key가 등록되어 있지 않으므로 등록해 준다.
	lResult = RegSetValueEx(hKey, strFontName, 0, REG_SZ, (LPBYTE)strFontName.GetBuffer(), strFontName.GetLength()+1);
	if(lResult != ERROR_SUCCESS)
	{
		TRACE(_T("Fail to create font value = %s\r\n"), strFontName);
		return false;
	}//if
	strFontName.ReleaseBuffer();
	RegCloseKey(hKey);

	//리소스로 폰트 추가
	if(AddFontResource(strFontDir) == 0)
	{
		TRACE(_T("Fail to add font resource = %s\r\n"), strFontDir);
		return false;
	}//if
	
	//폰트가 새로 등록되었음을 알림....
	::SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
	
	return true;
} 


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// tar파일인지 구분하여 준다. \n
/// @param (ciString) strFileName : (in) 파일 이름
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsTarFile(CString strFileName)
{
	CString strExt = _T("");
	int nlen = strFileName.GetLength();
	int i;
	for(i = nlen-1; i >= 0; i--)
	{
		if(strFileName[i] == _T('.'))
		{
			strExt = strFileName.Mid(i+1);
			break;
		}//if
	}//for

	if(strExt.CompareNoCase(_T("tar")) == 0)
	{
		return true;
	}//if

	return false;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 경로를 확인하고 만들어준다. \n
/// @param (CString) strPath : (in) 존재하여야 하는 경로
/////////////////////////////////////////////////////////////////////////////////
void MakeSurePath(CString strPath)
{
	//경로에 "/"를 "\\"로 바꿔야 한다.
	strPath.Replace(_T("/"), _T("\\"));
	TCHAR drive[MAX_PATH], dir[MAX_PATH], fname[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(strPath, drive, dir, fname, ext);

	CString str;
	str.Format(_T("%s%s%s"), drive, dir, fname);
	::MakeSureDirectoryPathExists(str);
}


}//namespace
