/************************************************************************************/
/*! @file DownloadResult.cpp
	@brief 컨텐츠 파일을 다운로드하는 로그를 기록하는 클래스
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/11/22\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2010/11/22:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "StdAfx.h"
#include "DownloadResult.h"
#include "Downloader.h"

using namespace libDownload;

#define		WRITE_RESULT			(m_mngProfile.WriteProfileString)
typedef		CMap<CString,LPCSTR,int,int>	CMapStringToInt;

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (CDownloadFileArray*) paryFile : (in) 다운로드하려는 컨텐츠 파일들의 정보 배열
/// @param (CString) strProgramId : (in) 다운로드하려는 패키지의 Id(+host)
/// @param (int) nBrowserId : (in) 패키지를 방송하려는 Browser Id
/// @param (LPVOID) pParent : (in) 부모 포인터
/////////////////////////////////////////////////////////////////////////////////
CDownloadResult::CDownloadResult(CDownloadFileArray* paryFile,
								CString strProgramId,
								int nBrowserId,
								LPVOID pParent)
:m_strProgramId(strProgramId)		
,m_strContentsList(_T(""))	
,m_strSuccessList(_T(""))	
,m_strFailList(_T(""))		
,m_strLoclaExistList(_T(""))
,m_strFileName(_T(""))		
,m_nSuccessCount(0)
,m_nFailCount(0)		
,m_nLocalExistCount(0)	
,m_nTotalCount(0)		
,m_nState(E_STATE_INIT)
,m_strCurrentProgress(_T("0/0"))
,m_pParent(pParent)
//,m_paryFile(paryFile)
{
	m_paryFile = paryFile;
	string strHost, strMac, strEdit, strVer;
	scratchUtil* aUtil = scratchUtil::getInstance();
	if(aUtil->readAuthFile(strHost, strMac, strEdit))
	{
		strcpy(m_stDownloadFileInfo.hostId, strHost.c_str());	//단말의 Id
	}//if
	aUtil->clearInstance();
	strcpy(m_stDownloadFileInfo.siteId, GetSiteID());			//단말의 site Id
	strcpy(m_stDownloadFileInfo.programId, strProgramId);		//package Id(+host)	
	m_stDownloadFileInfo.browserId = nBrowserId;				//Browser Id(1면, 2면)

	CTime tmNow = CTime::GetCurrentTime();
	m_strFileName.Format(_T("UBCDownloadResult_%s_%s.ini"), ::AfxGetAppName(), tmNow.Format("%Y%m%d%H%M%S"));
	strcpy(m_stDownloadFileInfo.fileName, m_strFileName);

	CDownloader* aParent = (CDownloader*)pParent;
	if(aParent) {
		this->m_nLocalExistCount = aParent->GetLocalExistCount();
	}

	InitResult();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CDownloadResult::~CDownloadResult()
{

}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 보존기간이 지난 파일을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::DeleteOldFile()
{
	//UBCVariables에서 보존기간을 읽어와서 그 기간보다 오래된 Result 파일은 삭제한다.
	//UBCVariables에 보존기간 값이 없다면 기본 15일로 값을 설정한다.
	CString strVariablesPath = GetAppPath();
	strVariablesPath += _T("data\\");
	strVariablesPath += _T("UBCVariables.ini");
	char cBuffer[BUF_SIZE] = { 0x00 };	
	//GetPrivateProfileString(_T("ROOT"), _T("PreservePeriod"), _T(""), cBuffer, BUF_SIZE, strVariablesPath);
	GetPrivateProfileString(_T("ROOT"), _T("PlayLogDayLimit"), _T(""), cBuffer, BUF_SIZE, strVariablesPath);
	int nPeriod = atoi(cBuffer);
	if(nPeriod == 0)
	{
		nPeriod = 15;
		//WritePrivateProfileString(_T("ROOT"), _T("PreservePeriod"), _T("15"), strVariablesPath);
		WritePrivateProfileString(_T("ROOT"), _T("PlayLogDayLimit"), _T("15"), strVariablesPath);
	}//if

	//현재시간부터 보존기간을 뺀 시간...
	CTime tmCurrent = CTime::GetCurrentTime();
	CTimeSpan tmPeriod(nPeriod, 0, 0, 0);
	CTime tmPreserve = tmCurrent - tmPeriod;

	CFileFind findResult;
	CString strFindPath = GetAppPath();
	//strFindPath += _T("data\\");
	strFindPath += _T("log\\");
	strFindPath += _T("UBCDownloadResult*.ini");
	bool bWorking = findResult.FindFile(strFindPath);
	CTime tmLastWrite;
	while(bWorking)
	{
		bWorking = findResult.FindNextFile();
		if(findResult.IsDots())
		{
			continue;
		}//if

		if(findResult.IsDirectory())
		{
			continue;
		}//if

		TRACE(_T("%s\r\n"), findResult.GetFileName());
		findResult.GetLastWriteTime(tmLastWrite);
		TRACE(_T("tmPreserve = %s\r\n"), tmPreserve.Format(_T("%Y/%m/%d %H:%M:%S")));
		TRACE(_T("tmLastWrite = %s\r\n"), tmLastWrite.Format(_T("%Y/%m/%d %H:%M:%S")));
		if(tmLastWrite < tmPreserve)
		{
			TRACE(_T("Delete file : %s\r\n"), findResult.GetFilePath());

			if(!DeleteFile(findResult.GetFilePath()))
			{
				LPVOID lpMsgBuf;
				FormatMessage( 
					FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					FORMAT_MESSAGE_FROM_SYSTEM | 
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					GetLastError(),
					0, // Default language
					(LPTSTR) &lpMsgBuf,
					0,
					NULL 
					);

				TRACE(_T("Delete file fail : %s\r\n"), (LPTSTR)lpMsgBuf);
			}//if

		}//if		
	}//while


	//구버젼의 파일들 삭제
	strFindPath = GetAppPath();
	strFindPath += _T("data\\");
	strFindPath += _T("UBCDownloadResult*.ini");
	bWorking = findResult.FindFile(strFindPath);
	while(bWorking)
	{
		bWorking = findResult.FindNextFile();
		if(findResult.IsDots())
		{
			continue;
		}//if

		if(findResult.IsDirectory())
		{
			continue;
		}//if
		
		TRACE(_T("Delete file : %s\r\n"), findResult.GetFilePath());

		if(!DeleteFile(findResult.GetFilePath()))
		{
			LPVOID lpMsgBuf;
			FormatMessage( 
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				GetLastError(),
				0, // Default language
				(LPTSTR) &lpMsgBuf,
				0,
				NULL 
				);

			TRACE(_T("Delete file fail : %s\r\n"), (LPTSTR)lpMsgBuf);
		}//if
	}//while
	findResult.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 기록을 초기화 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::InitResult()
{
	CString strPath = GetAppPath();
	strPath += _T("log\\");
	strPath += m_strFileName;
	//DeleteFile(strPath);		//이전의 파일을 삭제한다.
	m_mngProfile.SetFileName(strPath);

	//보존기간이 지난 파일은 삭제한다.
	DeleteOldFile();

	//totalCount
	m_nTotalCount = m_paryFile->GetCount();

	//Progress
	m_strCurrentProgress.Format(_T("%d/%d"), m_nSuccessCount+m_nLocalExistCount, m_nTotalCount);

	//ContentsList
	m_strContentsList = _T("");
	CString strTmpId;
	for(int i=0; i<m_nTotalCount; i++)
	{
		strTmpId = m_paryFile->GetAt(i)->m_strContentsId;
		m_strContentsList += _T("Contents_");
		m_strContentsList += strTmpId.Trim();
		m_strContentsList += _T(",");
	}//for
	m_strContentsList.TrimRight(_T(","));

	//초기값으로 셋팅한다.
	//programId
	WRITE_RESULT(_T("ROOT"), _T("programId"), m_strProgramId);

	//totalCount
	WRITE_RESULT(_T("ROOT"), _T("totalCount"), ::ToString(m_nTotalCount));

	//currentProgress
	WRITE_RESULT(_T("ROOT"), _T("currentProgress"), m_strCurrentProgress);

	//successCount
	WRITE_RESULT(_T("ROOT"), _T("successCount"), ::ToString(m_nSuccessCount));

	//failCount
	WRITE_RESULT(_T("ROOT"), _T("failCount"), ::ToString(m_nFailCount));

	//localExistCount
	WRITE_RESULT(_T("ROOT"), _T("localExistCount"), ::ToString(m_nLocalExistCount));

	//contentsList
	//WRITE_RESULT(_T("ROOT"), _T("contentsList"), m_strContentsList);

	//successList
	//WRITE_RESULT(_T("ROOT"), _T("successList"), m_strSuccessList);

	//failList
	WRITE_RESULT(_T("ROOT"), _T("failList"), m_strFailList);

	//localExistList
	//WRITE_RESULT(_T("ROOT"), _T("localExistList"), m_strLoclaExistList);

	//state
	WRITE_RESULT(_T("ROOT"), _T("state"), ::ToString(m_nState));

	//startTime
	WRITE_RESULT(_T("ROOT"), _T("startTime"), _T(""));

	//endTime
	WRITE_RESULT(_T("ROOT"), _T("endTime"), _T(""));

/*	실패한것만 기록하도록 수정
	CDownloadFile* pFile;
	CString strContentsKey;
	for(int i=0; i<m_nTotalCount; i++)
	{
		pFile = m_paryFile->GetAt(i);
		if(pFile->m_nDownloadState != E_STATE_INIT)
		{
			continue;
		}//if

		//pFile->SetInit();
		strContentsKey.Format(_T("Contents_%s"), pFile->m_strContentsId);

		//state
		WRITE_RESULT(strContentsKey, _T("state"),::ToString(pFile->m_nDownloadState));	//초기상태

		//contentsId
		WRITE_RESULT(strContentsKey, _T("contentsId"), pFile->m_strContentsId);

		//contentsName
		WRITE_RESULT(strContentsKey, _T("contentsName"), pFile->m_strContentsName);

		//contentsType
		WRITE_RESULT(strContentsKey, _T("contentsType"), ::ToString(pFile->m_nContentType));

		//fileName
		WRITE_RESULT(strContentsKey, _T("fileName"), pFile->m_strFileName);

		//location
		WRITE_RESULT(strContentsKey, _T("location"), pFile->m_strServerPath);

		//volume
		WRITE_RESULT(strContentsKey, _T("volume"), ::ToString(pFile->m_ulFileSize));

		//downloadVolume
		WRITE_RESULT(strContentsKey, _T("downloadVolume"), ::ToString(pFile->m_ullDownloadSize));

		//startTime
		WRITE_RESULT(strContentsKey, _T("startTime"), _T(""));

		//endTime
		WRITE_RESULT(strContentsKey, _T("endTime"), _T(""));

		//reason
		WRITE_RESULT(strContentsKey, _T("reason"), ::ToString(pFile->m_nDownloadReason));

		//result
		WRITE_RESULT(strContentsKey, _T("result"), ::ToString(pFile->m_bDownloadResult));
	}//for
*/
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 시작을 기록한다. \n
/// @param (bool) bSend : (in) Event를 보낼지 여부
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::BeginDownload(bool bSend)
{
	//state
	m_nState = E_STATE_DOWNLOADING;		//다운로드 시작
	WRITE_RESULT(_T("ROOT"), _T("state"), ::ToString(m_nState));

	//startTime
	m_tmStartTime = CTime::GetCurrentTime();
	WRITE_RESULT(_T("ROOT"), _T("startTime"), ::ToString((ULONG)m_tmStartTime.GetTime()));

	if(bSend)
	{
		//다운로드 시작을 알림
		SendUpdateEvent();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 종료를 기록한다. \n
/// @param (bool) bSend : (in) Event를 보낼지 여부
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::EndDownload(bool bSend)
{
	//endTime
	m_tmEndTime = CTime::GetCurrentTime();
	WRITE_RESULT(_T("ROOT"), _T("endTime"), ::ToString((ULONG)m_tmEndTime.GetTime()));

	//다운로드 결과를 계산, 정리하여 기록...
	CString strContentsKey;
	CDownloadFile* pFile = NULL;
	CString strLocalPath, strTmpPath;
	CFileStatus fs;
	CDownloader* pParent = (CDownloader*)m_pParent;

	CMapStringToInt		mapServer;  // skpark DownloadServer

	m_nSuccessCount			= 0;
	m_nFailCount			= 0;
	m_nLocalExistCount		= 0;
	m_strSuccessList		= _T("");
	m_strFailList			= _T("");
	m_strLoclaExistList		= _T("");

	for(int i=0; i<m_nTotalCount; i++)
	{
		pFile = m_paryFile->GetAt(i);
		strContentsKey.Format(_T("Contents_%s"), pFile->m_strContentsId);

		switch(pFile->m_nDownloadReason)
		{
		case E_REASON_CONNECTION_FAIL:
		case E_REASON_FILE_NOTFOUND:
		case E_REASON_EXCEPTION_FTP:
		case E_REASON_EXCEPTION_FILE:
			{
				//로컬에서 검사하여 존재여부와 사이즈를 비교하여 상태와 결과를 바꾸어 준다.
				pParent->GetLocalPath(strLocalPath, strTmpPath, pFile->m_strServerPath, pFile->m_strFileName);
				if(CFile::GetStatus(strLocalPath, fs) == TRUE)
				{
					if(fs.m_size == pFile->m_ulFileSize)
					{
						//로컬에 파일이 존재하고 사이즈가 같다면 성공으로 처리...
						pFile->m_nDownloadState = E_STATE_COMPLETE_SUCCESS;
						pFile->m_nDownloadReason = E_REASON_LOCAL_EXIST;
						pFile->m_bDownloadResult = true;
						pFile->m_tmStartTime = pFile->m_tmEndTime;
						pFile->m_ullDownloadSize = pFile->m_ulFileSize;

						m_nLocalExistCount++;
						m_strLoclaExistList += strContentsKey;
						m_strLoclaExistList += _T(",");

						m_nSuccessCount++;
						m_strSuccessList += strContentsKey;
						m_strSuccessList += _T(",");
					}
					else
					{
						//로컬에 존재하고 파일의 사이즈만 틀리다면 실패...
						m_nFailCount++;
						m_strFailList += strContentsKey;
						m_strFailList += _T(",");
					}//if
				}
				else
				{
					//로컬에도 없다면 실패
					m_nFailCount++;
					m_strFailList += strContentsKey;
					m_strFailList += _T(",");
				}//if
			}
			break;
		case E_REASON_LOCAL_EXIST:
			{
				pFile->m_tmStartTime = pFile->m_tmEndTime;
				pFile->m_ullDownloadSize = pFile->m_ulFileSize;

				m_nLocalExistCount++;
				m_strLoclaExistList += strContentsKey;
				m_strLoclaExistList += _T(",");

				m_nSuccessCount++;
				m_strSuccessList += strContentsKey;
				m_strSuccessList += _T(",");
			}
			break;
		case E_REASON_DOWNLOAD_SUCCESS:
			{
				m_nSuccessCount++;
				m_strSuccessList += strContentsKey;
				m_strSuccessList += _T(",");
			}
			break;
		case E_REASON_UNKNOWN:
			{
				m_nFailCount++;
				m_strFailList += strContentsKey;
				m_strFailList += _T(",");
			}
			break;
		}//switch
		// skpark DownloadServer
		if(	pFile->m_nDownloadReason ==  E_REASON_DOWNLOAD_SUCCESS	||
			pFile->m_nDownloadReason ==  E_REASON_LOCAL_EXIST		){
			if(!pFile->m_strDownloadServer.IsEmpty()){
				int currentSuccessCounter = 0;
				mapServer.Lookup(pFile->m_strDownloadServer, currentSuccessCounter);
				currentSuccessCounter++;
				mapServer.SetAt(pFile->m_strDownloadServer, currentSuccessCounter);
			}
		}
		if(pFile->m_nDownloadState != E_STATE_COMPLETE_SUCCESS)	//실패한것만 기록하도록 수정.
		{
			//contentsId
			WRITE_RESULT(strContentsKey, _T("contentsId"), pFile->m_strContentsId);

			//contentsName
			WRITE_RESULT(strContentsKey, _T("contentsName"), pFile->m_strContentsName);

			//contentsType
			WRITE_RESULT(strContentsKey, _T("contentsType"), ::ToString(pFile->m_nContentType));

			//fileName
			WRITE_RESULT(strContentsKey, _T("fileName"), pFile->m_strFileName);

			//location
			WRITE_RESULT(strContentsKey, _T("location"), pFile->m_strServerPath);

			//volume
			WRITE_RESULT(strContentsKey, _T("volume"), ::ToString(pFile->m_ulFileSize));

			//downloadVolume
			WRITE_RESULT(strContentsKey, _T("downloadVolume"), ::ToString(pFile->m_ullDownloadSize));

			//startTime
			WRITE_RESULT(strContentsKey, _T("startTime"), ::ToString((ULONG)pFile->m_tmStartTime.GetTime()));

			//endTime
			WRITE_RESULT(strContentsKey, _T("endTime"), ::ToString((ULONG)pFile->m_tmEndTime.GetTime()));

			//state
			WRITE_RESULT(strContentsKey, _T("state"),::ToString(pFile->m_nDownloadState));

			//reason
			WRITE_RESULT(strContentsKey, _T("reason"), ::ToString(pFile->m_nDownloadReason));

			//result
			WRITE_RESULT(strContentsKey, _T("result"), ::ToString(pFile->m_bDownloadResult));
		}//if
	}//for

	m_strSuccessList.TrimRight(_T(","));
	m_strFailList.TrimRight(_T(","));
	m_strLoclaExistList.TrimRight(_T(","));

	// skpark DownloadServer
	bool hasCache = false;
	int downloadFromCacheCount = 0;
	int downloadFromServerCount = 0;
	if(mapServer.GetSize()>0){
		CString strServer;
		POSITION pos = mapServer.GetStartPosition();
		while(pos){
			CString url;		
			int successCount=0;
			mapServer.GetNextAssoc(pos, url, successCount);
			CString token;
			token.Format("%s:%d",url,successCount);
			if(url.Mid(0,strlen("Cache://")) == "Cache://"){
				downloadFromCacheCount = successCount;
				hasCache=true;
			}else{
				downloadFromServerCount = successCount;
			}
			if(!strServer.IsEmpty()){
				strServer.Append(",");
			}
			strServer.Append(token);
		}
		WRITE_RESULT(_T("ROOT"), _T("DownloadServer"), strServer);
	}

	if(hasCache){
		m_strCurrentProgress.Format(_T("%d/%d|%d"), m_nSuccessCount, m_nTotalCount,downloadFromCacheCount );
		//WRITE_RESULT(_T("ROOT"), _T("currentProgress2"), m_strCurrentProgress);
	}else{
		m_strCurrentProgress.Format(_T("%d/%d"), m_nSuccessCount, m_nTotalCount );
	}

	WRITE_RESULT(_T("ROOT"), _T("currentProgress"), m_strCurrentProgress);
	WRITE_RESULT(_T("ROOT"), _T("successCount"), ::ToString(m_nSuccessCount));
	WRITE_RESULT(_T("ROOT"), _T("failCount"), ::ToString(m_nFailCount));
	WRITE_RESULT(_T("ROOT"), _T("localExistCount"), ::ToString(m_nLocalExistCount-downloadFromCacheCount));
	//WRITE_RESULT(_T("ROOT"), _T("successList"), m_strSuccessList);
	WRITE_RESULT(_T("ROOT"), _T("failList"), m_strFailList);
	//WRITE_RESULT(_T("ROOT"), _T("localExistList"), m_strLoclaExistList);

	if(m_nTotalCount == m_nLocalExistCount)
	{
		m_nState = E_STATE_COMPLETE_LOCAL_EXIST;	//전체 컨텐츠파일이 로컬에 있었음
	}
	else if(m_nFailCount == 0)
	{
		m_nState = E_STATE_COMPLETE_SUCCESS;		//전체 또는 일부파일 다운로드 성공
	}
	else if(m_nFailCount == m_nTotalCount)
	{
		m_nState = E_STATE_COMPLETE_FAIL;			//전체 실패
	}
	else
	{
		m_nState = E_STATE_PARTIAL_FAIL;			//일부 실패
	}//if

	//state
	WRITE_RESULT(_T("ROOT"), _T("state"), ::ToString(m_nState));

	//다운로드에 실패한 개수를 설정한다.
	pParent->SetFailCount(m_nFailCount);




	if(bSend)
	{
		//다운로드 종료를 알림
		SendUpdateEvent();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠패키지파일의 다운로드가 실패하였음을 기록한다. \n
/// @param (bool) bSend : (in) Event를 보낼지 여부
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::FailDownloadPackage(bool bSend)
{
	//endTime
	m_tmEndTime = CTime::GetCurrentTime();
	WRITE_RESULT(_T("ROOT"), _T("endTime"), ::ToString((ULONG)m_tmEndTime.GetTime()));

	//다운로드 결과를 계산, 정리하여 기록...
	CString strContentsKey;
	CDownloadFile* pFile = NULL;
	CString strLocalPath, strTmpPath;
	CFileStatus fs;
	CDownloader* pParent = (CDownloader*)m_pParent;

	m_nSuccessCount			= 0;
	m_nFailCount			= 1;
	m_nLocalExistCount		= 0;
	m_strSuccessList		= _T("");
	m_strFailList			= m_strProgramId + ".ini";
	m_strLoclaExistList		= _T("");
	m_nTotalCount			= 1;

	m_strCurrentProgress.Format(_T("%d/%d"), m_nSuccessCount+m_nLocalExistCount, 1);

	WRITE_RESULT(_T("ROOT"), _T("currentProgress"), m_strCurrentProgress);
	WRITE_RESULT(_T("ROOT"), _T("successCount"), ::ToString(m_nSuccessCount));
	WRITE_RESULT(_T("ROOT"), _T("failCount"), ::ToString(m_nFailCount));
	WRITE_RESULT(_T("ROOT"), _T("localExistCount"), ::ToString(m_nLocalExistCount));
	//WRITE_RESULT(_T("ROOT"), _T("successList"), m_strSuccessList);
	WRITE_RESULT(_T("ROOT"), _T("failList"), m_strFailList);
	//WRITE_RESULT(_T("ROOT"), _T("localExistList"), m_strLoclaExistList);
	WRITE_RESULT(_T("ROOT"), _T("totalcount"), ::ToString(m_nTotalCount));

	m_nState = E_STATE_COMPLETE_FAIL;			//전체 실패

	//state
	WRITE_RESULT(_T("ROOT"), _T("state"), ::ToString(m_nState));

	//다운로드에 실패한 개수를 설정한다.
	pParent->SetFailCount(m_nFailCount);

	if(bSend)
	{
		//다운로드 종료를 알림
		SendUpdateEvent();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 다운로드 상태를 알린다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDownloadResult::SendUpdateEvent()
{
	if(strlen(m_stDownloadFileInfo.hostId) == 0)
	{
		TRACE("Empty HostId\r\n");
	}//if

	if(m_nState==E_STATE_DOWNLOADING){ //skpark 2013.12.23 다운로드 종료시에는 아래 항목을 업데이트할 이유가 없다.
		m_strCurrentProgress.Format(_T("%d/%d"), m_nSuccessCount+m_nLocalExistCount, m_nTotalCount);
		WRITE_RESULT(_T("ROOT"), _T("successCount"), ::ToString(m_nSuccessCount));
		WRITE_RESULT(_T("ROOT"), _T("currentProgress"), m_strCurrentProgress);
	}
	TRACE(_T("Send DownloadResult event\r\n"));
	TRACE1(_T("State = %d\r\n"), m_nState);
	TRACE1(_T("Current progress = %s\r\n"), m_strCurrentProgress);

	m_stDownloadFileInfo.state = m_nState;
	sprintf(m_stDownloadFileInfo.progress, _T("%s"), m_strCurrentProgress);
	m_stDownloadFileInfo.alreadyExist = m_nLocalExistCount;
	//다운로드가 완료되었다면 메모리의 내용을 파일에 써준다(펌웨어뷰에서 읽기 때문...)
	if(m_nState > E_STATE_DOWNLOADING)
	{
		m_mngProfile.Write();
	}//if

	COPYDATASTRUCT  CDS;
	// Set CDS
	CDS.dwData = UBC_WM_DOWNLOAD_EVENT;
	CDS.cbData = sizeof(ST_DownloadFileInfo);
	CDS.lpData = (PVOID)&m_stDownloadFileInfo;

	TRACE1(_T("Program Id = %s\r\n"), m_stDownloadFileInfo.programId);
	TRACE1(_T("Host Id = %s\r\n"), m_stDownloadFileInfo.hostId);
	TRACE1(_T("Site Id = %s\r\n"), m_stDownloadFileInfo.siteId);
	TRACE1(_T("Progress = %s\r\n"), m_stDownloadFileInfo.progress);
	TRACE1(_T("State = %d\r\n"), m_stDownloadFileInfo.state);
	TRACE1(_T("Browser Id = %d\r\n"), m_stDownloadFileInfo.browserId);
	TRACE1(_T("File Name = %s\r\n"), m_stDownloadFileInfo.fileName);
	
	//Firmware View에 보낸다.
	scratchUtil* aUtil = scratchUtil::getInstance();
	ULONG ulPid = aUtil->getPid(_T("UBCFirmwareView.exe"));
	//ULONG ulPid = aUtil->getPid("DownloadResultRecv.exe");
	TRACE1(_T("FirmwareView Pid = %d\r\n"), (int)ulPid);

	HWND hWnd = aUtil->getWHandle(ulPid);
	CString strHandle;
	strHandle.Format(_T("FirmwareView Handle = %d\r\n"), hWnd);
	TRACE(strHandle);

	aUtil->clearInstance();
	::SendMessage(hWnd , WM_COPYDATA , 0, (LPARAM)&CDS);
}


/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ini 파일의 lock 상태가 unlock상태가 될때까지 대기한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDownloadResult::WaitForLock()
{
	CString strValue;
	//먼저 lock 값(1: lock, 0: Unlock)을 읽어서 lock 상태이면 잠시후에 다시 시도한다. 
	strValue = GetINIValue(m_strFileName, "ROOT", "lockState");
	if(strValue == "")
	{
		__DEBUG__("Can't found lockState value", _NULL);
		return true;
	}//if

	__DEBUG__("File lockState", strValue);

	if(strValue == "1")
	{
		HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		DWORD dwRet;
		int nCount = 0;

		while(1)
		{
			dwRet = WaitForSingleObject(hEvent, 500);
			if(dwRet == WAIT_TIMEOUT)
			{
				strValue = GetINIValue(m_strFileName, "ROOT", "lockState");
				if(strValue == "1")
				{
					nCount++;
					if(nCount >= 5)	//3초간 반복 대기하여도 잠금 상태이면 실패
					{
						__DEBUG__("Fail to read : lock state", _NULL);
						CloseHandle(hEvent);
						return false;
					}//if

					continue;
				}
				else if(strValue == "0")
				{
					//잠금 상태가 풀렸으면 값을 읽는다.
					break;
				}//if
			}//if
		}//while
	}//if

	return true;
}
*/