// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define WIN32_LEAN_AND_MEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include <afx.h>
#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.




// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
using namespace std;

#define		BUF_SIZE				(1024*100)
#define		SIZE_FILE_BUF			(1024*1024)
#define		MAX_KEY_LENGTH			255
#define		MAX_VALUE_NAME			16383

enum  CONTENTS_TYPE {
	CONTENTS_NOT_DEFINE = -1,
	CONTENTS_VIDEO = 0,
	CONTENTS_SMS,			//1. Horizontal Ticker
	CONTENTS_IMAGE,
	CONTENTS_PROMOTION,
	CONTENTS_TV,
	CONTENTS_TICKER,		//5. Vertical Ticker
	CONTENTS_PHONE,
	CONTENTS_WEBBROWSER,
	CONTENTS_FLASH,
	CONTENTS_WEBCAM,
	CONTENTS_RSS,			// 10.
	CONTENTS_CLOCK,
	CONTENTS_TEXT,			// 12. 이전 SMS
	CONTENTS_FLASH_TEMPLATE,
	CONTENTS_DYNAMIC_FLASH,
	CONTENTS_TYPING,
	CONTENTS_PPT,			//16
	CONTENTS_ETC,		// 2010.10.12 장광수 기타컨텐츠추가
	CONTENTS_WIZARD,	// 2010.10.21 정운형 마법사컨텐츠추가
	CONTENTS_FILE		//플래쉬 부속파일
	//CONTENTS_HWP,
	//CONTENTS_EXCEL
};

#include "common/libScratch/scratchUtil.h"

namespace libDownload
{
LPCTSTR		GetAppPath();
LPCTSTR		GetSiteID();
CString		ToString(int nValue);
CString		ToString(double fValue);
CString		ToString(ULONG ulValue);
CString		ToString(ULONGLONG ulvalue);
bool		IsFontFile(CString strFileName);				///<폰트파일인지 구분하여 준다.
bool		IsPptFile(CString strFileName);					///<PPT파일인지 구분하여 준다.
bool		IsTarFile(CString strFileName);					///<tar파일인지 구분하여 준다.
CString		GetINIValue(CString strFileName, CString strSection, CString strKey);						///<지정된 ini 파일의 값을 반환한다.
void		WriteINIValue(CString strFileName, CString strSection, CString strKey, CString strValue);	///<지정된 ini 파일의 key에 값을 쓴다.
bool		RegisterFont(CString strFontName, CString strFontPath);										///<폰트를 시스템이 등록한다.
void		MakeSurePath(CString strPath);																///<주어진 경로를 확인하고 만들어준다.
}