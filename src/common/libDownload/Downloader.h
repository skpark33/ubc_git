/************************************************************************************/
/*! @file Downloader.h
	@brief 파일을 다운로드하는 클래스 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2011/04/08\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2011/04/08:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

#include "DownloadResult.h"
#include "common/libFileServiceWrap/FileServiceWrapDll.h"
#include "common/libHttpRequest/HttpDownload.h"
#include "common/libCommon/utvMacro.h"
#include <afxinet.h>


enum E_RETURN_DOWNLOAD	//컨텐츠 다운로드 리턴 값
{
	E_DNRET_ERROR_UNKNOWN = -3,			//Unknown error
	E_DNRET_FAIL_PACKAGE_DOWNLOAD,		//컨텐츠패키지파일 다운로드 실패
	E_DNRET_FAIL,						//다운로드하는 컨텐츠 파일중에 실패가 있음
	E_DNRET_SUCCESS = 0,				//다운로드 성공
	E_DNRET_FAIL_MISSING_FILE,			//해당하는 패키지 파일이 없음
	E_DNRET_FAIL_READ_FILE,				//패키지 파일 읽기 오류
	E_DNRET_FAIL_FTP_ADDR,				//접속 가능한 FTP 주소가 없음
	E_DNRET_FAIL_FTP_CONNECT,			//FTP 접속에 실패함
	E_DNRET_FAIL_EMPTY_LIST,			//패키지 파일에 다운로드할 수 있는 파일이 없음
	E_DNRET_CANCEL,						//다운로드 취소
	E_DNRET_ALREADY_EXIST				//모든 콘텐츠파일이 로컬에 존재함
};

enum E_STATE_DOWNLOAD	//컨텐츠 다운로드 상태 값
{
	E_DNSTATE_CHECK_FILES = 0,			//다운로드 할 파일을 분석중
	E_DNSTATE_GET_ADDR,					//접속할 FTP주소를 받아오는중
	E_DNSTATE_CONNECTING,				//FTP에 접속중
	E_DNSTATE_CHECK_FILESIZE,			//다운로드할 파일 사이즈 계산중
	E_DNSTATE_DOWNLOADING,				//다운로드 중
	E_DNSTATE_COMPLETE_DOWNLOAD,		//다운로드 완료
	E_DNSTATE_WAITING_DOWNLOAD,			//다운로드 대기중
	E_DNSTATE_CANCEL_DOWNLOAD			//다운로드 취소
};

/*
//결과 메시지
static TCHAR* pszRet[] = { _T("Download success"),
							_T("Fail to load package file"),
							_T("Fail to get file server address"),
							_T("Fail to get connect file server"),
							_T("None dowload file list in package"),
							_T("Cancl dowloading package")
							 };
*/
//상태 메시지
static TCHAR* pszState[] = { _T("Checking files to download..."),
							_T("Getting file server address..."),
							_T("Connecting to file server..."),
							_T("Checking file size to download..."),
							_T("Downloading files..."),
							_T("Complete download..."),
							_T("Waiting download..."),
							_T("Cancel download...")
							 };

#define		WM_PROGRESS_DOWNLOAD	(WM_USER+500)		//다운로드 진행상태 알림 메시지
#define		WM_STATE_DOWNLOAD		(WM_USER+501)		//다운로드 완료 알림 메시지
#define		WM_FAILCOUNT_DOWNLOAD	(WM_USER+502)		//다운로드 실패개수 알림 메시지



//! 파일을 다운로드하는 클래스
/*!
*/
class CDownloader : public CFileServiceWrap::IProgressHandler, public IHttpDownloadUI
{
public:
	CDownloader(bool bUseReport = true,
				int nCutLevel = 2,
				bool bCheckFileTime = false );					///<생성자  skpark same_size_file_problem
	virtual ~CDownloader(void);									///<소멸자

	void		SetProgressNotifyWnd(HWND hWnd);				///<다운로드 진행상황을 받을 윈도우를 등록

	void		SetContentsPath(CString strContentsPath);		///<콘텐츠 파일의 경로를 설정한다.
	CString		GetContentsPath(void);							///<설정된 콘텐츠 파일의 경로를 반환한다.

	void		SetTmpPath(CString strTmpPath);					///<콘텐츠 파일의 임시 경로를 설정한다.
	CString		GetTmpPath(void);								///<설정된 콘텐츠 파일의 임시 경로를 반환한다.

	void		SetPackagePath(CString strPackagePath);			///<패키지 파일의 경로를 설정한다.
	CString		GetPackagePath(void);							///<설정된 패키지 파일의 경로를 반환한다.

	bool		DownloadPackageFile(CString strPackageName,
									CString strPackageSiteId,
									int nBrowserId,
									bool bUseReport = true);	///<패키지 ini 파일을 다운로드 한다.
	//bool		DownloadPackageFileEx(CString strPackageName,
	//								CString strPackageSiteId,
	//								bool bReplace);				///<패키지 ini 파일을 다운로드 하며, 받은파일을 새로이 덮어쓸지 여부를 선택한다.
	bool		DownloadBPIFile(CString strBPIName,
								CString strBPISiteId);			///<방송계획(*.bpi) 파일을 다운로드 한다.
	int			DownloadPlayContents(CString strPackageSiteId,
									CString strPackageId,
									int nBrowserId,
									bool bAsync = true);		///<패키지의 PlayContents 파일을 다운로드 한다.
	int			DownloadAllContents(CString strPackageSiteId,
									CString strPackageId,
									int nBrowserId,
									bool bAsync = true);		///<패키지의 모든 콘텐츠 파일을 다운로드 한다.

	int			IsAlreadyDownloaded(CString strPackageId);		///<해당 콘텐츠패키지의 플레이콘텐츠가 모두 다운로드 되었는지 여부를 반환
	int			ProcessDownloadPackage(CString strPackageName,
									CString strPackageSiteId,
									int nBrowserId,
									bool bUseReport = true);	///<패키지 ini와 컨텐츠를 다운로드하는 모든과정을 일괄 처리한다.

	void		CancelDownload(void);							///<다운로드 중인 작업을 중지 한다.
	void		UseDownloadReport(bool bUseReport);				///<다운로드결과 보고기능을 사용할지 여부를 설정
	void		SetCutLocationLevel(int nLevel);				///<컨텐츠 파일의 location필드값을 잘라내는 level을 설정한다.
	int			GetCutLocationLevel(void);						///<컨텐츠 파일의 location필드값을 잘라내는 level을 반환한다.
	void		GetLocalPath(CString& strLocalPath,
							CString& strLocalTempPath,
							LPCSTR lpszLocation,
							LPCSTR lpszFilename);				///<파일의 경로를 ENC와 Tmp경로로 변경하여 준다.
	void		SetFailCount(int nFailCount);					///<다운로드결과 실패한 파일의 개수를 설정한다.

	void		SetCheckFileTime(bool b) { m_bCheckFileTime = b; } // skpark same_size_file_problem

private:
	bool		MakePlayContentsList(void);						///<다운로드할 PlayContents 리스트를 만든다.
	bool		MakeContentsList(void);							///<다운로드할 Contents 리스트를 만든다.

	bool		GetFtpAddress(CString strPackageSiteId,CString strPackageName);		///<접속이 가능한 FTP 주소를 얻어온다.

	bool		DnPkgFile(CString strPackageName,
							bool bReplace = true);				///<패키지 파일을 FTP로부터 다운로드한다.(방송계획 파일 포함)
	bool		ConnectFtp(void);								///<FTP 연결을 한다.
	bool		CheckLocalFileSize(CString strPackageId,
									int nBrowserId = 0);		///<로컬의 파일사이즈를 비교한다.
	bool		CheckRemoteFileSize(CString strPackageId,
									int nBrowserId);			///<Remote의 파일사이즈를 비교한다.
	bool		CompareLocalContents(CDownloadFile* pInfo);		///<해당 파일이 로컬에 있는지 비교한다.
	bool		CompareRemoteContents(CDownloadFile* pInfo);	///<해당 파일이 서버에 있는지 비교한다.
	bool		CompareRemoteContentsViaHttp(void);				///<다운로드할 파일들을 웹서버를 통하여 비교한다.
	bool		DownloadFiles(CFileServiceWrap::IProgressHandler* pVoid = NULL);		///<다운로드 배열의 파일들을 FTP로부터 다운로드한다.

	void		ClearDownloadArray(void);						///<다운로드 배열을 정리한다.
	void		ClearDownloadResult(void);						///<다운로드 결과를 정리한다.
	void		ClearDownload(void);							///<다운로드 작업을 정리한다.

	void		SendStateMsg(int nState, int nRet);				///<완료메시지를 전송한다.

	bool		IsViaCache();									/// 캐쉬서버가 셋팅되어 있는지 여부
	bool		DnPkgFileViaCache(CString strPackageName);		///<패키지 파일을 캐쉬로부터 다운로드한다.(방송계획 파일 포함)
	bool		DownloadFilesViaCache(CString strPackageId);	///<다운로드 배열의 파일들을 캐쉬로부터 다운로드한다.
	bool		SetEndByCache();  // skpark DownloadServer
	
	ULONGLONG	GetLocalFileSize(CString& strLocalPath);		// 로컬 파일 사이즈를 가져온다.

	CString				m_strContentsPath;						///<콘텐츠 파일을 다운로드할 폴더
	CString				m_strTmpPath;							///<콘텐츠 파일을 임시로 다운로드할 폴더
	CString				m_strPackagePath;						///<패키지 파일을 다운로드할 폴더
	CString				m_strPackageFile;						///<현재 작업중인 패키지파일
	CString				m_strIP;								///<FTP 접속 Ip 주소
	CString				m_strId;								///<FTP 사용자 Id
	CString				m_strPwd;								///<FTP 비밀번호
	int					m_nPort;								///<FTP 접속 포트번호
	int					m_nCutLevel;							///<location값을 잘라내는 level
	ULONGLONG			m_ulTotalSize;							///<다운로드할 전체 사이즈
	HWND				m_hWndNotify;							///<다운로드 진생상황을 받을 윈도우 핸들
	bool				m_bAsync;								///<비동기 여부
	bool				m_bCancel;								///<다운로드를 중지 하는지 여부
	bool				m_bHasFail;								///<컨텐츠파일 다운로드과정중에 실패가 있었는지 여부
	bool				m_bUseReport;							///<다운로드결과 보고기능을 사용하는지 여부
	int					m_nFailCount;							///<다운로드에 실패한 개수
	int					m_nTotalCount;							///<다운로드 전체 개수
	int					m_nLocalExistCount;						///<로컬 존재수>
	int					m_nCurrentIndex;						///<현재 다운로드중인 배열의 인덱스
	DWORD				m_dwStartTick;							///<현재 파일을 다운로드 시작한 시간
	//DWORD				m_dwEndTick;							///<현재 파일이 다운로드 완료된 시간
	ULONGLONG			m_ulCompleteDownloadSize;				///<전체 다운로드된 사이즈
	ULONGLONG			m_ulPreDownloadSize;					///<이전에 다운로드한 사이즈
	CString				m_strCacheServer;								///<FTP 비밀번호

	CInternetSession	m_ssInternet;							///<인터넷 세션
	CFtpConnection*		m_pFtpConnection;						///<FTP 세션
	CDownloadResult*	m_pDownResult;							///<Download result를 기록
	CDownloadFileArray*	m_paryDownloadFile;						///<다운로드 파일(컨텐츠) 배열

	static UINT	ThreadDownloadPlayContents(LPVOID param);		///<PlayContetns 다운로드 thread
	static UINT	ThreadDownloadAllContents(LPVOID param);		///<Contetns 다운로드 thread

	////////////////////////////////////////////////////////////////////////////////////////////////

	bool				m_bUseHttp;								///<Http 기반으로 동작하는지 여부
	CFileServiceWrap*	m_pWebRef;								///<Http file service wrapper

	bool		DownloadFilesViaHttp(CFileServiceWrap::IProgressHandler* pVoid = NULL);///<다운로드 배열의 파일들을 HTTP로 다운로드한다.			
	bool		markNewFile(CString& filePath,CTime& localFileAccessTime, CDownloadFile* pInfo); // skpark same_size_file_problem
	bool		m_bCheckFileTime;  // skpark same_size_file_problem

protected:

public:
	int			GetLocalExistCount() { return this->m_nLocalExistCount; }

	virtual void ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag,
								ULONGLONG uCurrentBytes,
								ULONGLONG uTotalBytes,
								CString strLocalPath);			///<HTTP file service의 업로드/다운로드 상태처리


	bool GetFile(LPCTSTR szFile, LPCTSTR szRemote, LPCTSTR szLocal, LPCTSTR szSite); // 단일파일 가져오기
	bool GetFile(LPCTSTR file, ULONGLONG fileSize, LPCTSTR annoId, LPCTSTR siteId, LPCTSTR subdir); // 긴급공지용 단일파일 가져오기
	// ip port 지정형 GetFile
	bool GetFile(LPCTSTR ip, ULONG port, LPCTSTR szFile, LPCTSTR szRemote, LPCTSTR szLocal, LPCTSTR szSite);
	// first,second ip  지정형 GetFile
	bool GetFile(LPCTSTR ip1, LPCTSTR ip2, ULONG port,
		LPCTSTR file, ULONGLONG fileSize, LPCTSTR annoId, LPCTSTR siteId, LPCTSTR subdir); // CMS용 단일파일 가져오기

// IHttpDownloadUI Interface
	virtual void OnConnectError(int errCode, CString errMsg) ;
	virtual void OnBeforeDownload(CString filename, unsigned long srcFileSize);
	virtual void OnDownload(CString filename, 
						unsigned long srcFileSize, unsigned long recvFileSize);
	virtual void OnAfterDownload(int errCode, CString errMsg, CString filename, 
						unsigned long srcFileSize, unsigned long recvFileSize);

};
