// 컴퓨터에서 형식 라이브러리 마법사의 [클래스 추가]를 사용하여 생성한 IDispatch 래퍼 클래스입니다.

#import "D:\\project\\utv1\\bin8\\SMSDll.dll" no_namespace
// CcSendSMS 래퍼 클래스

class CcSendSMS : public COleDispatchDriver
{
public:
	CcSendSMS(){} // COleDispatchDriver 기본 생성자를 호출합니다.
	CcSendSMS(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CcSendSMS(const CcSendSMS& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// 특성
public:

	// 작업
public:


	// _cSendSMS 메서드
public:
	VARIANT SendSMS(VARIANT * biz_id, VARIANT * password, VARIANT * DNSName, VARIANT * send_number, VARIANT * receive_number, VARIANT * sms_contents, VARIANT * merge_name, VARIANT * reserved_date, VARIANT * userData1, VARIANT * userData2, VARIANT * userData3)
	{
		VARIANT result;
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x60030000, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms, biz_id, password, DNSName, send_number, receive_number, sms_contents, merge_name, reserved_date, userData1, userData2, userData3);
		return result;
	}
	VARIANT UpdateSMS(VARIANT * biz_id, VARIANT * password, VARIANT * DNSName, VARIANT * unique_num, VARIANT * sms_contents, VARIANT * reserved_date, VARIANT * userData1, VARIANT * userData2, VARIANT * userData3)
	{
		VARIANT result;
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x60030001, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms, biz_id, password, DNSName, unique_num, sms_contents, reserved_date, userData1, userData2, userData3);
		return result;
	}
	VARIANT DeleteSMS(VARIANT * biz_id, VARIANT * password, VARIANT * DNSName, VARIANT * unique_num, VARIANT * userData1, VARIANT * userData2, VARIANT * userData3)
	{
		VARIANT result;
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x60030002, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms, biz_id, password, DNSName, unique_num, userData1, userData2, userData3);
		return result;
	}

	// _cSendSMS 속성
public:

};
