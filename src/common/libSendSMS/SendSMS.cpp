/************************************************************************************/
/*! @file SendSMS.cpp
	@brief SMSDll.dll을 이용하여 쏜다넷 서비스의 SMS전송을 하는 클래스
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/10/11\n
	
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2010/10/11:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "StdAfx.h"
#include "SendSMS.h"
#include "common/libHttpRequest/HttpRequest.h"
#include "common/libCommon/SecurityIni.h"

#include "ci/libDebug/ciDebug.h"

ciSET_DEBUG(10,"CSendSMS");


#define BUF_SIZE			(1024*100)

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (std::string) strConfigPath : (in) 설정파일의 경로
/////////////////////////////////////////////////////////////////////////////////
CSendSMS::CSendSMS(std::string strConfigPath, std::string strCompany)
: m_strConfigPath(strConfigPath)
, m_strBizID("")
, m_strPWD("")
, m_strReqURL("")
, m_strReqParam("")
, m_strRecvURL("")
, m_strSendNum("")
, m_strReceiver("")
, m_strMsg("")
{
	ciDEBUG(10, ("CSendSMS(%s,%s)", strConfigPath.c_str(), strCompany.c_str()) );

	CoInitialize(NULL);

	CSecurityIni iniSec;
	std::string strSection;
	if(strCompany.length() != 0)
	{
		strSection = "SMS_";
		strSection += strCompany;
	}
	else
	{
		strSection = "SMS";
	}//if

	m_bUseHttp = atoi(iniSec.ReadValue(strSection.c_str(), "USE_HTTP", m_strConfigPath.c_str()));

	if(!m_bUseHttp)
	{
		//BIZ ID
		m_strBizID = iniSec.ReadValue(strSection.c_str(), "BIZ_ID", m_strConfigPath.c_str());

		//PWD
		m_strPWD = iniSec.ReadValue(strSection.c_str(), "PWD", m_strConfigPath.c_str());

		//Request URL
		m_strReqURL = iniSec.ReadValue(strSection.c_str(), "REQUEST_URL", m_strConfigPath.c_str());

		//Request Param
		m_strReqParam = iniSec.ReadValue(strSection.c_str(), "REQUEST_PARAM", m_strConfigPath.c_str());

		//Receive URL
		m_strRecvURL = iniSec.ReadValue(strSection.c_str(), "RECEIVE_URL", m_strConfigPath.c_str());
	}
	else
	{
		//Http URL
		m_strHttpUrl = iniSec.ReadValue(strSection.c_str(), "HTTP_URL", m_strConfigPath.c_str());
	}//if

	//Send number
	m_strSendNum = iniSec.ReadValue(strSection.c_str(), "SEND_NUM", m_strConfigPath.c_str());
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CSendSMS::~CSendSMS(void)
{
	//CoUninitialize();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 스트링을 VARINT형으로 변환한다. \n
/// @param (std::string*) pString : (in) 원본 문자열
/// @param (VARIANT*) pVar : (in/out) 변환된 VARIANT 값
/////////////////////////////////////////////////////////////////////////////////
void CSendSMS::StringToVariant(std::string* pString, VARIANT* pVar)
{
	WCHAR wszBuf[BUF_SIZE];

	ZeroMemory(wszBuf, sizeof(BUF_SIZE));
	MultiByteToWideChar(GetACP(), 0, (char*)pString->c_str(), -1, wszBuf, sizeof(wszBuf));

	VariantInit(pVar);
	pVar->vt = VT_BSTR;
	pVar->bstrVal = SysAllocString(wszBuf);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VARIANT형을 스트링으로 변환한다. \n
/// @param (VARIANT) var : (in) 원본 VARIANT 값
/// @param (char) *pChar : (out) 변환된 문자열
/////////////////////////////////////////////////////////////////////////////////
void CSendSMS::VariantToChar(VARIANT var, char *pChar)
{
	INT cbCount = 0;

	cbCount = WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, var.bstrVal, -1, pChar, cbCount, NULL, NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Soap을 초기화 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CSendSMS::Initialize()
{
	HRESULT hr;
	hr = m_pSoapClient.CreateInstance(__uuidof(SoapClient30));
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot create SoapClient."), hr);
		return false;
	}//if

	try
	{
		CString WsdlFile= _T("http://aspdll.xonda.net/SMSWS/webservice/xSMSWebService.wsdl");
		m_pSoapClient->MSSoapInit2(_variant_t(WsdlFile), _T(""), _T(""), _T(""), _T(""));
	}
	catch(_com_error err)
	{
		DisplayFault(_T("Cannot initialize SoapClient. "));
		return false;
	}//try

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// SMS 전송 함수 \n
/// @param (std::string) strReceiver : (in) 수신자 번호 리스트(동보시에 ','로 구분)
/// @param (std::string) strMsg : (in) 전송하려는 메시지(80자 이내)
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CSendSMS::SendSMS(std::string strReceiver, std::string strMsg)
{
	if(!m_bUseHttp)
	{
		if(m_strSendNum == "" || m_strBizID == "" || m_strPWD == "")
		{
			ciDEBUG(1, ("Empty some input value"));
			return false;
		}//if

		if(!Initialize())
		{
			ciDEBUG(1, ("Fail to initialize"));
			return false;
		}//if
	}//if

	m_strReceiver = strReceiver;
	//80자 까지만 전송...
	if(strMsg.length() > 80)
	{
		m_strMsg = strMsg.substr(0, 79);
	}
	else
	{
		m_strMsg = strMsg;
	}//if

	if(!m_bUseHttp)
	{
		return Execute(L"SendSMS");
	}//if

	std::string strTmp, strSendMsg;
	strSendMsg = "send_phone=";
	strSendMsg += m_strSendNum.c_str();
	strSendMsg += "&recive_phone=";
	strSendMsg += m_strReceiver.c_str();
	strSendMsg += "&contents=";
	strSendMsg += m_strMsg.c_str();

	//UTF-8 로 변환
	std::string strUtf8 = AnsiToUTF8(strSendMsg);

	CString strRet;
	CHttpRequest http_request;
	if(!http_request.RequestPost(m_strHttpUrl.c_str(), strUtf8.c_str(), strRet))
	{
		ciDEBUG(1, ("Fail to send SMS : %s", strRet));
		return false;
	}//if

	ciDEBUG(1, ("Success send SMS : %s", strRet));
	
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Ansi 문자열을 UTF-8로 변환한다. \n
/// @param (CString) strAnsi : (in) Ansi 문자열
/// @return <형: string> \n
///			변환된 UTF-8 문자열 \n
/////////////////////////////////////////////////////////////////////////////////
std::string CSendSMS::AnsiToUTF8(std::string& strAnsi)
{
	WCHAR unicode[1500]; 
    char utf8[1500]; 
 
    memset(unicode, 0, sizeof(unicode)); 
    memset(utf8, 0, sizeof(utf8)); 
 
    ::MultiByteToWideChar(CP_ACP, 0, strAnsi.c_str(), -1, unicode, sizeof(unicode)); 
    ::WideCharToMultiByte(CP_UTF8, 0, unicode, -1, utf8, sizeof(utf8), NULL, NULL); 
 
	return std::string(utf8); 
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 메소드를 invoke한다. \n
/// @param (OLECHAR*) pMethodName : (in) invoke하려는 메소드 명
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CSendSMS::Execute(OLECHAR* pMethodName)
{
	HRESULT hr;
	DISPID dispid;
	DISPPARAMS dispparams;
	VARIANTARG params[11];
	VARIANT result;
	CString ParamText;
	EXCEPINFO ExceptInfo;

	// Get dispatch ID corrisponding to method name.
	hr = m_pSoapClient->GetIDsOfNames(IID_NULL, &pMethodName, 1, LOCALE_SYSTEM_DEFAULT, &dispid);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot get dispatch id of SendSMS method."), hr);
		return false;
	}

	// Set B parameter.
	VariantInit(&params[0]);
	params[0].vt = VT_BSTR;
	ParamText = "";
	params[0].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[0], &params[0], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert userData3 to a string."), hr);
		return false;
	}//if

	VariantInit(&params[1]);
	params[1].vt = VT_BSTR;
	ParamText = "";
	params[1].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[1], &params[1], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert userData2 to a string."), hr);
		return false;
	}//if

	VariantInit(&params[2]);
	params[2].vt = VT_BSTR;
	ParamText = "";
	params[2].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[2], &params[2], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert userData1 to a string."), hr);
		return false;
	}//if

	VariantInit(&params[3]);
	params[3].vt = VT_BSTR;
	ParamText = "";
	params[3].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[3], &params[3], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert reserved_date to a string."), hr);
		return false;
	}//if

	VariantInit(&params[4]);
	params[4].vt = VT_BSTR;
	ParamText = "";
	params[4].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[4], &params[4], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert merge_name to a string."), hr);
		return false;
	}//if

	VariantInit(&params[5]);
	params[5].vt = VT_BSTR;
	ParamText = m_strMsg.c_str();
	params[5].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[5], &params[5], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert sms_contents to a string."), hr);
		return false;
	}//if

	VariantInit(&params[6]);
	params[6].vt = VT_BSTR;
	ParamText = m_strReceiver.c_str();
	params[6].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[6], &params[6], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert receive_number to a string."), hr);
		return false;
	}//if

	VariantInit(&params[7]);
	params[7].vt = VT_BSTR;
	ParamText = m_strSendNum.c_str();
	params[7].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[7], &params[7], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert send_number to a string."), hr);
		return false;
	}//if

	VariantInit(&params[8]);
	params[8].vt = VT_BSTR;
	ParamText = m_strRecvURL.c_str();
	params[8].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[8], &params[8], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert DNSName to a string."), hr);
		return false;
	}//if

	VariantInit(&params[9]);
	params[9].vt = VT_BSTR;
	ParamText = m_strPWD.c_str();
	params[9].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[9], &params[9], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert password to a string."), hr);
		return false;
	}//if

	VariantInit(&params[10]);
	params[10].vt = VT_BSTR;
	ParamText = m_strBizID.c_str();
	params[10].bstrVal = ParamText.AllocSysString();
	hr = VariantChangeType(&params[10], &params[10], 0, VT_BSTR);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot convert biz_id to a string."), hr);
		return false;
	}//if	

	// Initialize DISPPARAMS structure.
	dispparams.cArgs = 11;
	dispparams.cNamedArgs = 0;
	dispparams.rgdispidNamedArgs = NULL;
	dispparams.rgvarg = params;

	// Prepare result variant.
	VariantInit(&result);	

	bool bRet = false;
	// Invoke the specified method.
	hr = m_pSoapClient->Invoke(dispid, IID_NULL, LOCALE_SYSTEM_DEFAULT, DISPATCH_METHOD, &dispparams, &result, &ExceptInfo, NULL);
	if(FAILED(hr))
	{
		DisplayFault(_T("Invoke of SendSMS method failed."));
	}
	else
	{
		bRet = true;
		// Convert result to a string.
		VariantChangeType(&result, &result, 0, VT_BSTR);

		//char* pChar = NULL;
		//VariantToChar(result, pChar);
	}//if

	// Clean up variants.
	VariantClear(&result);
	for(int i = 0 ;i<16;i++)
	{
		VariantClear(&params[i]);
	}//for

	return bRet;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// HRESULT 결과를 출력한다. \n
/// @param (LPCTSTR) pMessage : (in) 오류 메시지
/// @param (HRESULT) hr : (in) HRESULT 값
/////////////////////////////////////////////////////////////////////////////////
void CSendSMS::DisplayHResult(LPCTSTR pMessage, HRESULT hr)
{
	if(hr != 0)
	{
		LPVOID lpMsgBuf;
		FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			hr,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
		);
		CString Msg = pMessage;
		Msg += " ";
		Msg += (LPCTSTR)lpMsgBuf;
		TRACE(Msg);
		LocalFree(lpMsgBuf);
	}
	else
	{
		//MessageBox(pMessage, "Error", MB_OK | MB_ICONEXCLAMATION);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Display fault information in SoapClient object. \n
/// @param (LPCTSTR) pMessage : (in) 오류메시지
/////////////////////////////////////////////////////////////////////////////////
void CSendSMS::DisplayFault(LPCTSTR pMessage)
{

	HRESULT hr;

	BSTR FaultString;
	hr = m_pSoapClient->get_FaultString(&FaultString);
	if(FAILED(hr))
	{
		DisplayHResult(_T("Cannot get FaultString."), hr);
		return;
	}//if

	CString Msg = pMessage;
	Msg += " ";
	Msg += FaultString;

	SysFreeString(FaultString);

	TRACE(Msg);
}


void CSendSMS::RaiseError(LPOLESTR pMessage, HRESULT hr) 
{
	ICreateErrorInfoPtr pCreateErrorInfo;
	IErrorInfoPtr pErrorInfo;

	if(SUCCEEDED(CreateErrorInfo(&pCreateErrorInfo)))
	{
		pCreateErrorInfo->SetSource(L"comsup");
		pCreateErrorInfo->SetDescription(pMessage);

		pCreateErrorInfo.QueryInterface(__uuidof(IErrorInfo), &pErrorInfo);

		_com_raise_error(hr, pErrorInfo.Detach());

	}
	else
	{
		_com_raise_error(hr, NULL);
	}//if
}


/////////////////////////////////////////////////////////////////////////////
void CSendSMS::RaiseError(LPOLESTR pMessage, HRESULT hr, EXCEPINFO& excepinfo, DISPPARAMS& params, unsigned int uArgErr)
{

	_bstr_t Msg = pMessage;
	wchar_t szArgErr[34];

	switch(hr)
	{
	case DISP_E_EXCEPTION:
		Msg += " ";
		Msg += excepinfo.bstrDescription;
		SysFreeString(excepinfo.bstrSource);
		SysFreeString(excepinfo.bstrDescription);
		SysFreeString(excepinfo.bstrHelpFile);
		hr = excepinfo.scode;
		break;

	case DISP_E_TYPEMISMATCH:
		if(uArgErr != -1)
		{
			Msg += " Error in parameter ";
			Msg += _ultow(params.cArgs - uArgErr, szArgErr, 10);
		}
		break;

	}//switch

	RaiseError(Msg, hr);
}


