/************************************************************************************/
/*! @file SendSMS.h
	@brief SMSDll.dll을 이용하여 쏜다넷 서비스의 SMS전송을 하는 클래스
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/10/11\n
	▶ 참고사항:
		- http://www.smsket.com/main.asp

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2010/10/11:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

#include "objbase.h"
//#include "Msxml2.h"
#import "msxml4.dll" 
//using namespace MSXML2;

#import "C:\Program Files (x86)\Common Files\MSSoap\Binaries\mssoap30.dll" exclude(\
"IErrorInfo","IStream", "ISequentialStream", "_LARGE_INTEGER", "_ULARGE_INTEGER",\
"tagSTATSTG", "_FILETIME")
using namespace MSSOAPLib30;

//#include <afxdisp.h>
#include <string>


//! SMS를 전송하는 클래스
/*!
	쏜다넷의 SMSDll.dll을 사용한다. \n
	http://www.smsket.com/main.asp \n
*/
class CSendSMS
{
public:
	CSendSMS(std::string strConfigPath, std::string strCompany = "");	///<생성자	
	virtual ~CSendSMS(void);				///<소멸자

	bool	SendSMS(std::string strReceiver, std::string strMsg);				///<SMS 전송 함수

protected:
	std::string			m_strConfigPath;	///<환경설정파일의 경로
	std::string			m_strBizID;			///<기업회원 아이디
	std::string			m_strPWD;			///<비밀번호
	std::string			m_strReqURL;		///<SMS를 요청하는 URL
	std::string			m_strReqParam;		///<SMS를 요청하는 URL의 인자
	std::string			m_strRecvURL;		///<SMS 요청의 응답을 받는 페이지 URL
	std::string			m_strSendNum;		///<발신자 번호
	std::string			m_strReceiver;		///<수신자 번호
	std::string			m_strMsg;			///<메시지
	std::string			m_strHttpUrl;		///<Http로 전송하는 주소
	bool				m_bUseHttp;			///<Http를 이용하는지 여부

	ISoapClientPtr		m_pSoapClient;		///<Soap client

	bool	Initialize(void);													///<Soap을 초기화 한다.
	bool	Execute(OLECHAR* pMethodName);										///<지정된 메소드를 invoke한다.
	void	StringToVariant(std::string* pString, VARIANT* pVar);				///<스트링을 VARINT형으로 변환한다.
	void	VariantToChar(VARIANT var, char *pChar);							///<VARIANT형을 스트링으로 변환한다.
	void	DisplayHResult(LPCTSTR pMessage, HRESULT hr);						///<HRESULT 결과를 출력한다.
	void	DisplayFault(LPCTSTR pMessage);										///<Display fault information in SoapClient object.

	std::string  AnsiToUTF8(std::string& strAnsi);								///<Ansi 문자열을 UTF-8로 변환한다.

private:
	static void RaiseError(LPOLESTR pMessage, HRESULT hr); 
	static void RaiseError(LPOLESTR pMessage, HRESULT hr, EXCEPINFO& excepinfo, DISPPARAMS& params, unsigned int uArgErr);

};
