// SampleSMS.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "../SendSMS.h"

int _tmain(int argc, _TCHAR* argv[])
{
	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);

	std::string strConfigPath;
	strConfigPath = cDrive;
	strConfigPath += cPath;
	strConfigPath += "UBCTT.ini";

	CSendSMS sendSMS(strConfigPath, "KIA");
	std::string strReceiver;
	strReceiver = "01025987839";
	//strReceiver += ",";
	//strReceiver += "0177184877";

	bool bRet = sendSMS.SendSMS(strReceiver, "SMS 메시지 전송 테스트 입니다!!!");
	if(!bRet)
	{
		//fail to sending
	}
	else
	{
		//success
	}//if

	return 0;
}

