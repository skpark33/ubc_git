#ifndef _SHA256ENCODE_H_
#define _SHA256ENCODE_H_
 
#include "stdafx.h"
 
#include <stdlib.h>  

#ifdef _COP_MSC_
#define rotlFixed(x, n) _rotl((x), (n))  
#define rotrFixed(x, n) _rotr((x), (n))  
#else
#define rotlFixed(x, n) (((x) << (n)) | ((x) >> (32-(n))))
#define rotrFixed(x, n) (((x) >> (n)) | ((32-(n))) << (x))
#endif
  
#define S0(x) (rotrFixed(x, 2) ^ rotrFixed(x,13) ^ rotrFixed(x, 22))  
#define S1(x) (rotrFixed(x, 6) ^ rotrFixed(x,11) ^ rotrFixed(x, 25))  
#define s0(x) (rotrFixed(x, 7) ^ rotrFixed(x,18) ^ (x >> 3))  
#define s1(x) (rotrFixed(x,17) ^ rotrFixed(x,19) ^ (x >> 10))  

#define Ch(x,y,z) (z^(x&(y^z)))  
#define Maj(x,y,z) ((x&y)|(z&(x|y))) 

#ifndef IN
 #define IN
#endif
 
#ifndef OUT
 #define OUT
#endif
 

typedef int Int32;  
typedef unsigned int UInt32;  

#ifdef _COP_MSC_
typedef __int64 Int64;  
typedef unsigned __int64 UInt64;  
#else
typedef long long Int64;  
typedef unsigned long long UInt64;  
#endif

typedef unsigned char Byte;  

extern const UInt32 g_HashKey[64];
 
//typedef struct  
class CSha256Encode
{  
private:
   
 UInt64 m_nCount;  
 Byte buffer[64];
 
 UInt32 m_nTranseAr[8];
 UInt32 m_nHashDataAr[16];
 
 // 최종 SHA256 해쉬값이 들어갈 배열
 UInt32 m_nHashCodeAr[8];
 
 // 최종 값이 들어갈 버퍼가 외부버퍼일때, 그 버퍼의 포인터
 UInt32* m_pHashCode;
 
 const UInt32* const m_pHashKey;
 

public:
 CSha256Encode():m_pHashKey(g_HashKey), m_pHashCode(NULL) {}
 virtual ~CSha256Encode() {}
 
private:
 CSha256Encode(const CSha256Encode &c):m_pHashKey(g_HashKey), m_pHashCode(NULL) {}
 CSha256Encode& operator=(const CSha256Encode &c) { return *this; }
 

public:
 void Init();
 void Sha256_Init(UInt32* pHashCode = NULL);
 void Sha256_Update(const Byte *data, size_t size);  
 void Sha256_Final();  

 void Sha256_Transform( const UInt32 *data);
 void Sha256_WriteByteBlock();
 
 void RunHashing(int i, int j, const UInt32 *data);
 
 UInt32 GetHashCode(int nIndex) { return nIndex < 8 ? m_nHashCodeAr[nIndex] : 0; }
 
 UInt32 GetTransData(int nCnt, int nIndex)
 {
  if((nCnt >= 0) && (nCnt <= 7))
  {
   return m_nTranseAr[(nCnt - nIndex) & 7];
  }
 
  return 0;
 }
 
 void EncodeSHA256(IN char* szSrcStr, IN int nLen, OUT char* szEncStr, int outSize);
 UInt32* EncodeSHA256(IN char* szSrcStr, IN int nLen); 
} ;  
 

#endif  // _SHA256ENCODE_H_
