// EventGatewayView.cpp : implementation of the CEventGatewayView class
//

#include "stdafx.h"
#include "EventGateway.h"

#include "EventGatewayDoc.h"
#include "EventGatewayView.h"

#include "EGEventHandler.h"
#include "UBCEventSet.h"
#include "UBCMirSet.h"

#include "AddhandListDialog.h"

#include "caf/util/libEncoding/Encoding.h"
#include "common/libCommon/ubcUtil.h"

#include "cci/libMir/cciMirExtUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ciSET_DEBUG(10, "CEventGatewayView");


#define		SYSTEM_LOG_HEIGHT		(120)

#define		TIMER_LOG_FILE_CHANGE				(1021)
#define		TIMER_LOG_FILE_CHANGE_TIME			(60*1000)	// every 1min

#define		TIMER_CHECK_TIMEOUT_EVENT_ID		(1022)		// delete old event
#define		TIMER_CHECK_TIMEOUT_EVENT_TIME		(60*1000)	// every 1min

#define		TIMER_RESET_EVENT_HANDLER_ID		(1023)		// reset event handler
#define		TIMER_RESET_EVENT_HANDLER_TIME		(2*60*1000)	// every 2min

#define		TIMER_IMPORT_MIR_ID					(1024)		// reset event handler
#define		TIMER_IMPORT_MIR_TIME				(60*1000)	// every 1min

// CEventGatewayView

IMPLEMENT_DYNCREATE(CEventGatewayView, CFormView)

BEGIN_MESSAGE_MAP(CEventGatewayView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_MESSAGE(WM_POST_EVENT, OnPostEvent)
	ON_MESSAGE(WM_POST_SYSTEM_LOG, OnPostSystemLog)
	ON_COMMAND(ID_VIEW_ADDHAND_LIST, &CEventGatewayView::OnViewAddhandList)
END_MESSAGE_MAP()

// CEventGatewayView construction/destruction

CEventGatewayView::CEventGatewayView()
	: CFormView(CEventGatewayView::IDD)
{
	// TODO: add construction code here

	m_pEventHandler = NULL;
}

CEventGatewayView::~CEventGatewayView()
{
	if(m_pEventHandler)
	{
		m_pEventHandler->removeHand();
		ciDEBUG(10, ("Delete EventHandler") );
		//delete m_pEventHandler; // not delete -> error msg-box during deleting
		//m_pEventHandler = NULL;
	}

	ciDEBUG(10, ("Close Database") );
	CUBCEventSet::clearInstance();
}

void CEventGatewayView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_lcLog);
	DDX_Control(pDX, IDC_LIST_SYSTEM_LOG, m_lcSystemLog);
}

BOOL CEventGatewayView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CEventGatewayView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//
	m_lcLog.SetExtendedStyle(m_lcLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcLog.InsertColumn(COL_COUNT,		"Count",	LVCFMT_RIGHT, 0);
	m_lcLog.InsertColumn(COL_EVENTTIME,	"Time",		0, 120);
	m_lcLog.InsertColumn(COL_EVENTTYPE,	"Type",		0, 240);
	m_lcLog.InsertColumn(COL_ENTITY,	"Entity",	0, 120);
	m_lcLog.InsertColumn(COL_EVENT_BODY,"EventBody",0, 200);

	//
	m_lcSystemLog.SetExtendedStyle(m_lcSystemLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcSystemLog.InsertColumn(SYS_COL_TIME, "Time", 0, 120);
	m_lcSystemLog.InsertColumn(SYS_COL_STRING, "System Log", 0, 400);

	//
	if(CUBCEventSet::RegisterODBC(GetSafeHwnd()) == FALSE)
	{
		ciWARN( ("Fail to RegisterODBC() !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"WARNING : Fail to RegisterODBC()" );
	}

	//
	if(CUBCEventSet::getInstance() == NULL)
	{
		ciERROR( ("Fail to open Database !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to open Database" );
	}

	//
	ResetEventHandler();

	// timeout-event delete timer
	if( SetTimer(TIMER_CHECK_TIMEOUT_EVENT_ID, TIMER_CHECK_TIMEOUT_EVENT_TIME, NULL) )
	{
		ciDEBUG(10, ("Success to SetTimer(CHECK_TIMEOUT_EVENT)") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"SUCCESS : Success to SetTimer(CHECK_TIMEOUT_EVENT)" );
	}
	else
	{
		ciERROR( ("Fail to SetTimer(CHECK_TIMEOUT_EVENT) !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to SetTimer(CHECK_TIMEOUT_EVENT)" );
	}

	//
	SetTimer(TIMER_IMPORT_MIR_ID, 1000, NULL);
	SetTimer(TIMER_LOG_FILE_CHANGE, TIMER_LOG_FILE_CHANGE_TIME, NULL);
}


// CEventGatewayView diagnostics

#ifdef _DEBUG
void CEventGatewayView::AssertValid() const
{
	CFormView::AssertValid();
}

void CEventGatewayView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CEventGatewayDoc* CEventGatewayView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CEventGatewayDoc)));
	return (CEventGatewayDoc*)m_pDocument;
}
#endif //_DEBUG


// CEventGatewayView message handlers

void CEventGatewayView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(GetSafeHwnd())
	{
		if(m_lcLog.GetSafeHwnd())
			m_lcLog.MoveWindow(0, 0, cx, cy-SYSTEM_LOG_HEIGHT);
		if(m_lcSystemLog.GetSafeHwnd())
			m_lcSystemLog.MoveWindow(0, cy-SYSTEM_LOG_HEIGHT, cx, SYSTEM_LOG_HEIGHT);
	}
}

void CEventGatewayView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CFormView::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case TIMER_CHECK_TIMEOUT_EVENT_ID:
		KillTimer(TIMER_CHECK_TIMEOUT_EVENT_ID);
		CheckTimeoutEvent();
		break;
	case TIMER_RESET_EVENT_HANDLER_ID:
		KillTimer(TIMER_RESET_EVENT_HANDLER_ID);
		ResetEventHandler();
		break;
	case TIMER_IMPORT_MIR_ID:
		KillTimer(TIMER_IMPORT_MIR_ID);
		if(UpdateMirTable() == false)
			SetTimer(TIMER_IMPORT_MIR_ID, TIMER_IMPORT_MIR_TIME, NULL);
		break;

	case TIMER_LOG_FILE_CHANGE:
		KillTimer(TIMER_LOG_FILE_CHANGE);

		CEventGatewayApp::CheckLogFileDate();

		SetTimer(TIMER_LOG_FILE_CHANGE, TIMER_LOG_FILE_CHANGE_TIME, NULL);
		break;
	}
}

void CEventGatewayView::OnViewAddhandList()
{
	CAddhandListDialog dlg(m_pEventHandler);
	dlg.DoModal();
}

LRESULT CEventGatewayView::OnPostEvent(WPARAM wParam, LPARAM lParam)
{
	ciDEBUG(10, ("OnPostEvent(0x%08X,0x%08X)", wParam, lParam) );

	// reset event-handler timer
	if( !SetTimer(TIMER_RESET_EVENT_HANDLER_ID, TIMER_RESET_EVENT_HANDLER_TIME, NULL) )
	{
		ciERROR( ("Fail to SetTimer(RESET_EVENT_HANDLER) !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to SetTimer(RESET_EVENT_HANDLER)" );
	}

	//
	EVENT_ITEM* item = (EVENT_ITEM*)wParam;
	if(item == NULL)
	{
		ciWARN( ("EventItem is NULL !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"WARNING : EventItem is NULL" );
		return 0;
	}

	ciDEBUG(10, ("eventType=%s", item->eventType) );
	ciDEBUG(10, ("domain=%s", item->domain) );
	ciDEBUG(10, ("entity=%s", item->entity) );
	ciDEBUG(10, ("eventTime=%s", TIME_TO_STRING(item->eventTime)) );
	ciDEBUG(10, ("toString=%s", item->toString) );

	//
	CUBCEventSet* event_set = CUBCEventSet::getInstance();
	if(event_set == NULL)
	{
		ciERROR( ("Fail to open Database !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to open Database" );
		return 0;
	}

	try
	{
		event_set->AddNew();

		if( ubcUtil::getInstance()->isGlobal() )
		{
			ciDEBUG(10, ("Global(Unicode) mode") );
			// unicode mode
			event_set->m_eventTypeW	= GetUfromUTF8(item->eventType); // unicode
			event_set->m_domainW	= GetUfromUTF8(item->domain); // unicode
			event_set->m_entityW	= GetUfromUTF8(item->entity); // unicode
			event_set->m_eventTime	= item->eventTime;
			event_set->m_eventBodyW	= GetUfromUTF8(item->toString); // unicode
		}
		else
		{
			ciDEBUG(10, ("Not GLobal(Ansi) mode") );
			// ansi mode
			event_set->m_eventTypeA	= item->eventType; // ansi
			event_set->m_domainA	= item->domain; // ansi
			event_set->m_entityA	= item->entity; // ansi
			event_set->m_eventTime	= item->eventTime;
			event_set->m_eventBodyA	= item->toString; // ansi
		}

		if(event_set->Update() == FALSE)
		{
			ciERROR( ("Fail to CUBCEventSet::Update() !!!") );
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							"ERROR : Fail to CUBCEventSet::Update()" );
		}
	}
	catch(CDBException* ex)
	{
		char szCause[255] = {0};
		ex->GetErrorMessage(szCause, 254);

		ciERROR( ("Fail to CUBCEventSet::AddNew() by CDBException(%s) !!!", szCause) );

		CString msg;
		msg.Format("ERROR : Fail to CUBCEventSet::AddNew() by CDBException(%s)", szCause);
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						msg );

		delete ex;
	}
	catch(CException* ex)
	{
		char szCause[255] = {0};
		ex->GetErrorMessage(szCause, 254);

		ciERROR( ("Fail to CUBCEventSet::AddNew() by CException(%s) !!!", szCause) );

		CString msg;
		msg.Format("ERROR : Fail to CUBCEventSet::AddNew() by CException(%s)", szCause);
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						msg );

		delete ex;
	}
	catch(...)
	{
		ciERROR( ("Fail to CUBCEventSet::AddNew() by Unknown-Exception !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to CUBCEventSet::AddNew() by Unknown-Exception" );
	}
/*
	// directly sql-insert (but no return-value)
	if(event_set->m_pDatabase)
	{
		CString sql;
		sql.Format("insert into ubc_event (\"eventType\",\"domain\",\"entity\",\"eventTime\",\"eventBody\") values ('%s','%s','%s','%s','%s');"
			, item->eventType.c_str()	// EventType
			, ""						// Domain
			, item->entity.c_str()		// Entity
			, CURRENT_TIME_STRING		// EventTime
			, item->toString.c_str()	// EventBody
		);
		event_set->m_pDatabase->ExecuteSQL(sql);
	}
*/

	// check log-count
	int idx = m_lcLog.GetItemCount();
	if(idx > 100000)
	{
		idx--;
		for(int i=0; i<1000; i++, idx--)
			m_lcLog.DeleteItem(idx);
	}

	//
	CTime cur_tm = CTime::GetCurrentTime();

	static ULONGLONG count = 0;
	char buf[32];
	sprintf(buf, "%I64u", count++);
	m_lcLog.InsertItem(0, buf);
	m_lcLog.SetItemText(0, COL_EVENTTIME, TIME_TO_STRING(item->eventTime));
	m_lcLog.SetItemText(0, COL_EVENTTYPE, GetAfromUTF8(item->eventType));
	m_lcLog.SetItemText(0, COL_ENTITY, GetAfromUTF8(item->entity));
	m_lcLog.SetItemText(0, COL_EVENT_BODY, GetAfromUTF8(item->toString));

	delete item;

	return 1;
}

LRESULT CEventGatewayView::OnPostSystemLog(WPARAM wParam, LPARAM lParam)
{
	ciDEBUG(10, ("OnPostSystemLog()") );

	CString* tm_str = (CString*)wParam;
	CString* err_str = (CString*)lParam;
	if(tm_str == NULL || err_str == NULL)
	{
		if(tm_str == NULL)
		{
			ciWARN( ("TimeString is NULL !!!") );
		}
		else
			delete tm_str;

		if(err_str == NULL)
		{
			ciWARN( ("Errortring is NULL !!!") );
		}
		else
			delete err_str;

		return 0;
	}

	ciDEBUG(10, ("SystemLog(Time=%s,SysLog=%s)", (LPCSTR)*tm_str, (LPCSTR)*err_str) );

	// check log-count
	if(m_lcSystemLog.GetItemCount() > 10000)
	{
		for(int i=0; i<100; i++)
			m_lcSystemLog.DeleteItem(0);
	}

	// new log
	int idx = m_lcSystemLog.GetItemCount();
	m_lcSystemLog.InsertItem(idx, "" );
	m_lcSystemLog.SetItemText(idx, SYS_COL_TIME,		(LPCSTR)*tm_str );
	m_lcSystemLog.SetItemText(idx, SYS_COL_STRING,	(LPCSTR)*err_str );
	m_lcSystemLog.EnsureVisible(idx, FALSE);

	delete tm_str;
	delete err_str;

	return 1;
}

void CEventGatewayView::ResetEventHandler()
{
	ciDEBUG(10, ("ResetEventHandler()"));

	//
	if(m_pEventHandler)
	{
		if( !m_pEventHandler->removeHand() )
		{
			ciWARN( ("Fail to cciEventManager::removeAllHandler() !!!") );
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							"WARNING : Fail to cciEventManager::removeAllHandler()" );
		}
		//delete m_pEventHandler; // not delete -> error msg-box during deleting
		//m_pEventHandler = NULL;
	}

	//
	if(m_pEventHandler == NULL)
		m_pEventHandler = new EGEventHandler(GetSafeHwnd());
	if(m_pEventHandler == NULL)
	{
		ciERROR( ("Fail to create Event-Handler object !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to create Event-Handler object" );
	}
	else
	{
		// success to make event-handler object
		m_pEventHandler->addHand();
	}

	//
	if( !SetTimer(TIMER_RESET_EVENT_HANDLER_ID, TIMER_RESET_EVENT_HANDLER_TIME, NULL) )
	{
		ciERROR( ("Fail to SetTimer(RESET_EVENT_HANDLER) !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to SetTimer(RESET_EVENT_HANDLER)" );
	}
}

void CEventGatewayView::CheckTimeoutEvent()
{
	ciDEBUG(10, ("CheckTimeoutEvent()"));

	CUBCEventSet* event_set = CUBCEventSet::getInstance();
	if(event_set && event_set->m_pDatabase)
	{
		try
		{
			event_set->CheckTimeoutEvent();
		}
		catch(CDBException* ex)
		{
			char szCause[255] = {0};
			ex->GetErrorMessage(szCause, 254);

			ciWARN( ("Fail to CUBCEventSet::ExecuteSQL() by CDBException(%s) !!!", szCause) );

			CString msg;
			msg.Format("WARNING : Fail to CUBCEventSet::ExecuteSQL() by CDBException(%s)", szCause);
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							msg );

			delete ex;
		}
		catch(CException* ex)
		{
			char szCause[255] = {0};
			ex->GetErrorMessage(szCause, 254);

			ciWARN( ("Fail to CUBCEventSet::ExecuteSQL() by CException(%s) !!!", szCause) );

			CString msg;
			msg.Format("WARNING : Fail to CUBCEventSet::ExecuteSQL() by CException(%s)", szCause);
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							msg );

			delete ex;
		}
		catch(...)
		{
			ciWARN( ("Fail to CUBCEventSet::ExecuteSQL() by Unknown-Exception !!!") );
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							"WARNING : Fail to CUBCEventSet::ExecuteSQL() by Unknown-Exception" );
		}
	}
	else
	{
		ciERROR( ("CUBCEventSet is NULL !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : CUBCEventSet is NULL" );
	}

	if( !SetTimer(TIMER_CHECK_TIMEOUT_EVENT_ID, TIMER_CHECK_TIMEOUT_EVENT_TIME, NULL) )
	{
		ciERROR( ("Fail to SetTimer(CHECK_TIMEOUT_EVENT) !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to SetTimer(CHECK_TIMEOUT_EVENT)" );
	}
}

bool CEventGatewayView::ClearMirTable()
{
	ciDEBUG(10, ("ClearMirTable()"));

	CUBCMirSet* mir_set = CUBCMirSet::getInstance();
	if(mir_set && mir_set->m_pDatabase)
	{
		try
		{
			mir_set->m_pDatabase->ExecuteSQL("delete from ubc_mir;"); // delete all
			return true;
		}
		catch(CDBException* ex)
		{
			char szCause[255] = {0};
			ex->GetErrorMessage(szCause, 254);

			ciWARN( ("Fail to CUBCMirSet::ExecuteSQL() by CDBException(%s) !!!", szCause) );

			CString msg;
			msg.Format("WARNING : Fail to CUBCMirSet::ExecuteSQL() by CDBException(%s)", szCause);
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							msg );

			delete ex;
		}
		catch(CException* ex)
		{
			char szCause[255] = {0};
			ex->GetErrorMessage(szCause, 254);

			ciWARN( ("Fail to CUBCMirSet::ExecuteSQL() by CException(%s) !!!", szCause) );

			CString msg;
			msg.Format("WARNING : Fail to CUBCMirSet::ExecuteSQL() by CException(%s)", szCause);
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							msg );

			delete ex;
		}
		catch(...)
		{
			ciWARN( ("Fail to CUBCMirSet::ExecuteSQL() by Unknown-Exception !!!") );
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							"WARNING : Fail to CUBCMirSet::ExecuteSQL() by Unknown-Exception" );
		}
	}
	else
	{
		ciERROR( ("CUBCMirSet is NULL !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : CUBCMirSet is NULL" );
	}

	return false;
}

bool CEventGatewayView::UpdateMirTable()
{
	// 0. MIR 연결을 초기화한다.
	cciMirExtUtil*  mir_util = cciMirExtUtil::getInstance();
	// 반드시 널체크를 해준다.
	if( mir_util==NULL )
	{
		ciERROR(("Mir initialize failed"));  
		return false;
	}

	// 1. model.rule 로 부터 모든 class 정보를 load 하여 메모리를 구축한다.
	int count = mir_util->loadAllClass("SVR"); // SVR 은 UBC 의 Server Module Name 이다.
	if(count == 0)
	{
		ciERROR(("No model rule founded"));
		return false;
	}

	//2. 모든 클래스 정보를 outList 에 담아온다.
	cciClassInfoList outList;
	count = mir_util->getAllClassInfo(outList);
	if(count == 0)
	{
		ciERROR(("No class founded"));
		return false;
	}
	ciDEBUG(1,("%d class founded", count));

	//2.5 테이블 클리어
	if(ClearMirTable() == false)
	{
		ciWARN(("Fail to clear UBC_MIR table !!!"));
		return false;
	}

	//3. 클래스 정보를 프린트한다.
	CUBCMirSet* mir_set = CUBCMirSet::getInstance();
	cciClassInfoList::iterator itr = outList.begin();
	cciClassInfoList::iterator end = outList.end();
	for( ; itr!=end; itr++)
	{
		cciClassInfo& aInfo = (*itr);
		ciDEBUG(1, ("Class=%s", aInfo.classScopedName.c_str()) );

		cciAttrInfoMap::iterator jtr = aInfo.attrInfoMap.begin();
		cciAttrInfoMap::iterator end2 = aInfo.attrInfoMap.end();
		for( ; jtr!=end2; jtr++)
		{
			ciDEBUG(1,("AttrName=%s, AttrType=%s, AttrClass=%s, IndexOrder=%d", 
				jtr->first.c_str(), 
				jtr->second.attrType.c_str(),
				jtr->second.className.c_str(),
				jtr->second.indexOrder
				));

			try
			{
				mir_set->AddNew();
				mir_set->m_classScopedName = aInfo.classScopedName.c_str();
				mir_set->m_attrName = jtr->first.c_str();
				mir_set->m_attrType = jtr->second.attrType.c_str();
				mir_set->m_className = jtr->second.className.c_str();
				mir_set->m_indexOrder = (jtr->second.indexOrder == 0) ? 99 : jtr->second.indexOrder ;

				if(mir_set->Update() == FALSE)
				{
					ciERROR( ("Fail to CUBCMirSet::Update() !!!") );
					POST_SYSTEM_LOG(GetSafeHwnd(), 
									CURRENT_TIME_STRING, 
									"ERROR : Fail to CUBCMirSet::Update()" );
				}
			}
			catch(CDBException* ex)
			{
				char szCause[255] = {0};
				ex->GetErrorMessage(szCause, 254);

				ciERROR( ("Fail to CUBCMirSet::AddNew() by CDBException(%s) !!!", szCause) );

				CString msg;
				msg.Format("ERROR : Fail to CUBCMirSet::AddNew() by CDBException(%s)", szCause);
				POST_SYSTEM_LOG(GetSafeHwnd(), 
								CURRENT_TIME_STRING, 
								msg );

				delete ex;
			}
			catch(CException* ex)
			{
				char szCause[255] = {0};
				ex->GetErrorMessage(szCause, 254);

				ciERROR( ("Fail to CUBCMirSet::AddNew() by CException(%s) !!!", szCause) );

				CString msg;
				msg.Format("ERROR : Fail to CUBCMirSet::AddNew() by CException(%s)", szCause);
				POST_SYSTEM_LOG(GetSafeHwnd(), 
								CURRENT_TIME_STRING, 
								msg );

				delete ex;
			}
			catch(...)
			{
				ciERROR( ("Fail to CUBCMirSet::AddNew() by Unknown-Exception !!!") );
				POST_SYSTEM_LOG(GetSafeHwnd(), 
								CURRENT_TIME_STRING, 
								"ERROR : Fail to CUBCMirSet::AddNew() by Unknown-Exception" );
			}
		}
	}

	return true;
}
