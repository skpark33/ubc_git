// EventGatewayView.h : interface of the CEventGatewayView class
//


#pragma once

#include "afxcmn.h"


class EGEventHandler;


class CEventGatewayView : public CFormView
{
protected: // create from serialization only
	CEventGatewayView();
	DECLARE_DYNCREATE(CEventGatewayView)

public:
	enum{ IDD = IDD_EVENTGATEWAY_FORM };

	enum {
		COL_COUNT,
		COL_EVENTTIME,
		COL_EVENTTYPE,
		COL_ENTITY,
		COL_EVENT_BODY,
	};

	enum {
		SYS_COL_TIME,
		SYS_COL_STRING,
	};

// Attributes
public:
	CEventGatewayDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CEventGatewayView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnViewAddhandList();

	CListCtrl	m_lcLog;
	CListCtrl	m_lcSystemLog;

protected:
	afx_msg LRESULT OnPostEvent(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPostSystemLog(WPARAM wParam, LPARAM lParam);

	EGEventHandler*		m_pEventHandler;

	void	ResetEventHandler();
	void	CheckTimeoutEvent();

	bool	ClearMirTable();
	bool	UpdateMirTable();
};

#ifndef _DEBUG  // debug version in EventGatewayView.cpp
inline CEventGatewayDoc* CEventGatewayView::GetDocument() const
   { return reinterpret_cast<CEventGatewayDoc*>(m_pDocument); }
#endif

