#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "ReposControl2.h"
#include "EGEventHandler.h"

// CAddhandListDialog 대화 상자입니다.

class CAddhandListDialog : public CDialog
{
	DECLARE_DYNAMIC(CAddhandListDialog)

public:
	CAddhandListDialog(EGEventHandler* pEventHandler, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAddhandListDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ADDHAND_LIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CReposControl2	m_reposControl;
	EGEventHandler*	m_pEventHandler;

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);

	CListCtrl	m_lc;
	CButton		m_btnOK;
};
