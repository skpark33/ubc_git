#include "stdafx.h"
/*! \file EGWorld.C
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
*/

//#include "stdafx.h"
//#include "MsgBox.h"
#include "EGWorld.h"

#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciEnv.h>
#include <common/libInstall/installUtil.h>
#include <common/libCommon/ubcIni.h>


ciSET_DEBUG(10, "EGWorld");

EGWorld::EGWorld() {
	ciDEBUG(1,("EGWorld()"));
}

EGWorld::~EGWorld() {
	ciDEBUG(1,("~EGWorld()"));
}

ciBoolean 
EGWorld::init(int& p_argc, char** &p_argv) 
{
	ciDEBUG(1,("EGWorld::init(%d)", p_argc));	
	
	if(cciORBWorld::init(p_argc,p_argv) == ciFalse) {
		ciERROR(("ciWorld::init() is FAILED"));
		return ciFalse;
	}
	//const char*  ignoreDebugEnv = getenv("IGNORE_COP_DEBUG");
	//if(ignoreDebugEnv==0 || ignoreDebugEnv[0] != '1'){
	//	ciDebug::setDebugOn();
	//	ciWorld::ylogOn();
	//}
	//ciEnv::defaultEnv(true,"ubc");
	if(ciWorld::hasWatcher() == ciTrue) {}

	return ciTrue;
}


ciBoolean
EGWorld::fini(long p_finiCode)
{
	ciDEBUG(1,("fini()"));
	ciWorld::ylogOff();
	return ciWorld::fini(p_finiCode);
}
