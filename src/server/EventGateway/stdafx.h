// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes


 // Here, minimal DB support is requested.  No view is chosen.

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxdb.h>        // ODBC



#define		WM_POST_EVENT					(WM_USER+1025)
#define		WM_POST_SYSTEM_LOG				(WM_USER+1026)

#define		CURRENT_TIME_STRING				(CTime::GetCurrentTime().Format("%Y/%m/%d %H:%M:%S"))
#define		TIME_TO_STRING(TM)				(TM.Format("%Y/%m/%d %H:%M:%S"))

typedef struct
{
	CString eventType;
	CString domain;
	CString entity;
	CTime	eventTime;
	CString toString;
} EVENT_ITEM;

#define		POST_EVENT(HPWND,ET,DM,EN,ETM,ToStr)	\
					{ \
						EVENT_ITEM* item = new EVENT_ITEM; \
						if(item) { \
							item->eventType = ET; \
							item->domain = DM; \
							item->entity = EN; \
							item->eventTime = ETM; \
							item->toString = ToStr; \
						} \
						::PostMessage(HPWND, WM_POST_EVENT, (WPARAM)item, NULL); \
					}

#define		POST_SYSTEM_LOG(HPWND,TM,SL) \
					{ \
						CString* TMP_TM = new CString(TM); \
						CString* TMP_SL = new CString(SL); \
						::PostMessage(HPWND, WM_POST_SYSTEM_LOG, (WPARAM)TMP_TM, (LPARAM)TMP_SL); \
					}

//#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
//#endif
