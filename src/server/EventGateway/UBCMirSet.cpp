// UBCMirSet.h : CUBCMirSet 클래스의 구현입니다.

// CUBCMirSet 구현입니다.

// 코드 생성 위치 2012년 1월 27일 금요일, 오후 3:04

#include "stdafx.h"
#include "UBCMirSet.h"
#include <ODBCINST.H>

#include "UBCEventSet.h"

#include "common/libCommon/ubcUtil.h"

ciSET_DEBUG(10, "CUBCMirSet");


CUBCMirSet*		CUBCMirSet::_instance = NULL;
CCriticalSection	CUBCMirSet::_cs;


IMPLEMENT_DYNAMIC(CUBCMirSet, CRecordset)


CUBCMirSet::CUBCMirSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_classScopedName = "";
	m_attrName = "";
	m_attrType = "";
	m_className = "";
	m_indexOrder = -1;

	m_nFields = 5;
	m_nDefaultType = snapshot;
}

CUBCMirSet* CUBCMirSet::getInstance()
{
	_cs.Lock();
	if(_instance == 0)
	{
		_instance = new CUBCMirSet();
		if(_instance->OpenDatabase() == false)
		{
			delete _instance;
			_instance = NULL;
		}
	}
	_cs.Unlock();
	return _instance;
}

void CUBCMirSet::clearInstance()
{
	_cs.Lock();
	if(_instance)
	{
		_instance->CloseDatabase();
		delete _instance;
		_instance = NULL;
	}
	_cs.Unlock();
}

bool CUBCMirSet::OpenDatabase()
{
	if( !IsOpen() )
		if( !Open() )
		{
			//::AfxMessageBox(_T("데이버테이스 열기 실패 !!!"), MB_ICONSTOP);
			return false;
		}

	return true;
}

void CUBCMirSet::CloseDatabase()
{
	if( IsOpen() )
	{
		Close();
	}
}

CString CUBCMirSet::GetDefaultConnect()
{
	return CUBCEventSet::getInstance()->GetDefaultConnect();
}

CString CUBCMirSet::GetDefaultSQL()
{
	return _T("[dbo].[ubc_mir]");
}

void CUBCMirSet::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// RFX_Text() 및 RFX_Int() 같은 매크로는 데이터베이스의 필드
// 형식이 아니라 멤버 변수의 형식에 따라 달라집니다.
// ODBC에서는 자동으로 열 값을 요청된 형식으로 변환하려고 합니다

	RFX_Text  (pFX, _T("[classScopedName]"),  m_classScopedName);
	RFX_Text  (pFX, _T("[attrName]"),     m_attrName);
	RFX_Text  (pFX, _T("[attrType]"),     m_attrType);
	RFX_Text  (pFX, _T("[className]"),     m_className);
	RFX_Int (pFX, _T("[indexOrder]"),     m_indexOrder);
}
/////////////////////////////////////////////////////////////////////////////
// CUBCMirSet 진단

#ifdef _DEBUG
void CUBCMirSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CUBCMirSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


