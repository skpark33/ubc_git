// UBCEventSet.h : CUBCEventSet의 선언입니다.

#pragma once

#include <afxmt.h>


// 코드 생성 위치 2012년 1월 27일 금요일, 오후 3:04

class CUBCEventSet : public CRecordset
{
protected:
	CUBCEventSet(CDatabase* pDatabase = NULL);
	static CUBCEventSet*	_instance;

	static CCriticalSection	_cs;

	static CString		m_strDBType;
	static CString		m_strDSN;
	static CString		m_strID;
	static CString		m_strPWD;

	static bool				getPropertiesValue(HWND hParentWnd, LPCSTR lpszKey, CString& value);
	static bool				getCopDsnReg(HWND hParentWnd, LPCSTR lpszDBKey, CString& value);
	static bool				getCopDsnDrv(HWND hParentWnd, LPCSTR lpszDBKey, CString& value);
	static bool				getCopDsn(HWND hParentWnd, LPCSTR lpszDBKey, CString& value);
	static bool				getCopUsr(HWND hParentWnd, LPCSTR lpszDBKey, CString& value);
	static bool				getCopPwd(HWND hParentWnd, LPCSTR lpszDBKey, CString& value);

public:
	static bool				RegisterODBC(HWND hParentWnd);
	static CUBCEventSet*	getInstance();
	static void				clearInstance();

	bool	OpenDatabase();
	void	CloseDatabase();

	DECLARE_DYNAMIC(CUBCEventSet)

// 필드/매개 변수 데이터

// 아래의 문자열 형식(있을 경우)은 데이터베이스 필드의 실제 데이터 형식을
// 나타냅니다(CStringA: ANSI 데이터 형식, CStringW: 유니코드 데이터 형식).
// 이것은 ODBC 드라이버에서 불필요한 변환을 수행할 수 없도록 합니다.
// 원할 경우 이들 멤버를 CString 형식으로 변환할 수 있으며
// 그럴 경우 ODBC 드라이버에서 모든 필요한 변환을 수행합니다.
// (참고: 유니코드와 이들 변환을 모두 지원하려면  ODBC 드라이버
// 버전 3.5 이상을 사용해야 합니다).

	CStringW	m_eventTypeW;
	LONGLONG	m_eventIndex;
	CStringW	m_domainW;
	CStringW	m_entityW;
	CTime		m_eventTime;
	CStringW	m_eventBodyW;

	CStringA	m_eventTypeA;
	CStringA	m_domainA;
	CStringA	m_entityA;
	CStringA	m_eventBodyA;

// 재정의
	// 마법사에서 생성한 가상 함수 재정의
	public:
	virtual CString GetDefaultConnect();	// 기본 연결 문자열

	virtual CString GetDefaultSQL(); 	// 레코드 집합의 기본 SQL
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX 지원

// 구현
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void	CheckTimeoutEvent();
};


