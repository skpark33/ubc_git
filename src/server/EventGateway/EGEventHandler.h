#ifndef _EGEventHandler_h_
#define _EGEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

#include <afxmt.h>

//
typedef struct {
	CString	eventType;
	CString	entity;
} ADDHAND_ITEM;

typedef		CArray<ADDHAND_ITEM, ADDHAND_ITEM&>		ADDHAND_ITEM_LIST;

//
class EGEventHandler : public cciEventHandler
{
public:
	EGEventHandler(HWND hParentWnd);
	virtual ~EGEventHandler();

   	virtual void 	processEvent(cciEvent& pEvent);
	ciBoolean		addHand();
	ciBoolean		removeHand();

	ADDHAND_ITEM_LIST*	GetAddhandList() { return &m_arrAddhandList; };

protected:
	HWND	m_hParentWnd;
	CString	m_strAddhandFilename;

	ADDHAND_ITEM_LIST	m_arrAddhandList;

	void	ReadDefaultInfo();
	bool	ReadInfoFromFile(LPCSTR lpszAddhandFilename);
};

#endif // _EGEventHandler_h_
