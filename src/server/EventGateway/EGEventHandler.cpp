#include "stdafx.h"

//#include <ci/libConfig/ciPath.h>
//#include <cci/libWrapper/cciEvent.h>
//#include "common/libInstall/installUtil.h"
//#include "common/libScratch/scratchUtil.h"
//#include "common/libCommon/ubcIni.h"

#include "EGEventHandler.h"

#include "Lib.h"

ciSET_DEBUG(10, "EGEventHandler");


EGEventHandler::EGEventHandler(HWND hParentWnd)
: m_hParentWnd (hParentWnd)
, m_strAddhandFilename ("")
{
	m_strAddhandFilename = GetMainPath();
	m_strAddhandFilename += GetAppName();
	m_strAddhandFilename += ".txt";
}

EGEventHandler::~EGEventHandler()
{
}

ciBoolean EGEventHandler::addHand()
{
	ciDEBUG(10, ("addHand()") );

	// check addhand-info file
	CFileStatus fs;
	if( CFile::GetStatus(m_strAddhandFilename, fs) )
	{
		// addhand-info file exist -> read from file.
		if(ReadInfoFromFile(m_strAddhandFilename) && m_arrAddhandList.GetCount()>0)
		{
			// success to read from file
			ciDEBUG(10, ("Success to load Addhand-Info file(%s)", m_strAddhandFilename) );

			CString msg;
			msg.Format("SUCCESS : Load Addhand-Info file(%s)", m_strAddhandFilename);
			POST_SYSTEM_LOG(m_hParentWnd, CURRENT_TIME_STRING, msg);
		}
		else
		{
			// fail to read from file. -> read from pre-defined info.
			ciWARN( ("Fail to load Addhand-Info file(%s) !!!", m_strAddhandFilename) );

			CString msg;
			msg.Format("WARNING : Fail to load Addhand-Info file(%s)", m_strAddhandFilename);
			POST_SYSTEM_LOG(m_hParentWnd, CURRENT_TIME_STRING, msg);

			ReadDefaultInfo();
		}
	}
	else
	{
		// not exist -> read from pre-defined info.
		ReadDefaultInfo();
	}

	// get cciEventmanger
	cciEventManager* manager = cciEventManager::getInstance();
	if(manager == NULL)
	{
		// fail to get cciEventmanger -> somethiong wrong
		ciERROR( ("cciEventManager is NULL !!!") );
		POST_SYSTEM_LOG(m_hParentWnd, CURRENT_TIME_STRING, "ERROR : cciEventManager is NULL");
		return ciFalse;
	}

	//
	if( m_arrAddhandList.GetCount() > 0 )
	{
		for(int i=0; i<m_arrAddhandList.GetCount(); i++)
		{
			//if(i != 0) this->increase();

			ADDHAND_ITEM& item = m_arrAddhandList.GetAt(i);
			ciString eventType	= item.eventType;		// eventtype. (ex:heartbeat)
			ciString entity		= item.entity;			// entity (ex:*)

			//
			cciEntity aEntity (entity.c_str());
			if(manager->addHandler(aEntity, eventType, this) < 0)
			{
				// fail to addhand
				ciERROR( ("Fail to cciEventManager::addHandler(%s,%s) !!!", eventType.c_str(), entity.c_str()) );

				CString msg;
				msg.Format("ERROR : Fail to cciEventManager::addHandler(%s,%s)", eventType.c_str(), entity.c_str() );

				POST_SYSTEM_LOG(m_hParentWnd, CURRENT_TIME_STRING, msg);
			}
			else
			{
				// success to addhand
				ciDEBUG(10, ("Success to cciEventManager::addHandler (%s,%s)", eventType.c_str(), entity.c_str()) );
			}
		}
	}
	else
	{
		// no item
		ciERROR( ("No Addhand-Info !!!") );
		POST_SYSTEM_LOG(m_hParentWnd, CURRENT_TIME_STRING, "ERROR : No Addhand-Info");
		return ciFalse;
	}

	return ciTrue;
}

ciBoolean EGEventHandler::removeHand()
{
	ciDEBUG(10, ("removeHand()") );

	cciEventManager* manager = cciEventManager::getInstance();
	if(manager == NULL) return ciFalse;

	ciBoolean ret_val = ciFalse;

	try
	{
		ret_val = manager->removeAllHandler();
	}
	catch(CORBA::Exception& ex)
	{
	}
	catch(...)
	{
	}

	ciDEBUG(10, ("~removeHand()") );

	return ret_val;
}

void EGEventHandler::processEvent(cciEvent& aEvent)
{
	ciDEBUG(10, ("processEvent(%s)", aEvent.toString()) );

	cciEntity aEntity(aEvent.getEntity());

	POST_EVENT(
		m_hParentWnd, 
		aEvent.getEventType(), 
		"", 
		aEntity.toString(), 
		CTime::GetCurrentTime(), 
		aEvent.toStringWithType() //aEvent.toString()
	);

	return;
}

void EGEventHandler::ReadDefaultInfo()
{
	ciDEBUG(10, ("Load Default Addhand-Info") );

	POST_SYSTEM_LOG(m_hParentWnd, CURRENT_TIME_STRING, "SUCCESS : Load Default Addhand-Info");

	const char* pre_def_addhand[] = {
		"alarmSummary","*",							// 1
		"dsUpdate","*",								// 2
		"heartbeat","*",							// 3
		"hostScheduleUpdated","*",					// 4
		"reservationExpired","*",					// 5
		"reservationRemoved","*",					// 6
		"reservationChanged","*",					// 7
		"scheduleReserved","*",						// 8
		"updateAlarm","FM=*",						// 9
		"faultAlarm","FM=*/Site=*/Host=*",			// 10
		"announceCreated","PM=*/Site=*/Announce=*",	// 11
		"announceChanged","PM=*/Site=*/Announce=*",	// 12
		"announceRemoved","PM=*/Site=*/Announce=*",	// 13
		"announceExpired","PM=*/Site=*/Announce=*",	// 14
		"bpCreated","PM=*/Site=*/BP=*",				// 15
		"bpChanged","PM=*/Site=*/BP=*",				// 16
		"bpRemoved","PM=*/Site=*/BP=*",				// 17
		"adminStateChanged","PM=*/Site=*/Host=*",	// 18
		"downloadStateChanged","PM=*/Site=*/Host=*",// 19
		"monitorStateChanged","PM=*/Site=*/Host=*",	// 20
		"operationalStateChanged","PM=*/Site=*/Host=*",// 21
		"processStateChanged","PM=*/Site=*/Host=*",	// 22
		"reloadSchedule","PM=*/Site=*/Host=*",		// 23
		"scheduleChanged","PM=*/Site=*/Host=*",		// 24
		"vncStateChanged","PM=*/Site=*/Host=*"	,	// 25
		"deviceOperationalStateChanged","PM=*/Site=*/Host=*"		// 26
	};

	//
	m_arrAddhandList.RemoveAll();

	//
	for(int i=0; i<26; i++)	// -> check upper list count !!!
	{
		ADDHAND_ITEM item;

		item.eventType = pre_def_addhand[i*2];
		item.entity = pre_def_addhand[i*2+1];

		m_arrAddhandList.Add(item);
	}
}

bool EGEventHandler::ReadInfoFromFile(LPCSTR lpszAddhandFilename)
{
	//
	m_arrAddhandList.RemoveAll();

	//
	CStdioFile file;
	if( file.Open(lpszAddhandFilename, CFile::modeRead | CFile::typeText) )
	{
		CString str_line;

		while(file.ReadString(str_line))
		{
			str_line.Trim(" \r\n"); // trim - space,CR,LF

			if(str_line.Find(",") <= 0)
			{
				// invalid line
				continue;
			}

			//
			ADDHAND_ITEM item;
			int pos = 0;
			item.eventType = (LPCSTR)str_line.Tokenize(",", pos);	// ex:heartbeat
			item.entity = (LPCSTR)str_line.Tokenize(",", pos);		// ex:*

			m_arrAddhandList.Add(item);
		}

		file.Close();

		return true;
	}
	else
	{
		return false;
	}
}
