//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by EventGateway.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_EVENTGATEWAY_FORM           101
#define IDR_MAINFRAME                   128
#define IDR_EventGatewayTYPE            129
#define IDD_ADDHAND_LIST                130
#define IDC_LIST_CTTRL                  1000
#define IDC_LIST_LOG                    1000
#define IDC_LIST_SYSTEM_LOG             1001
#define IDC_LIST                        1002
#define ID_VIEW_VIEWADDHANDLIST         32772
#define ID_VIEW_ADDHAND_LIST            32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
