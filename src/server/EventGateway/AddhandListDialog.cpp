// AddhandListDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "EventGateway.h"

#include "AddhandListDialog.h"


// CAddhandListDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAddhandListDialog, CDialog)

CAddhandListDialog::CAddhandListDialog(EGEventHandler* pEventHandler, CWnd* pParent /*=NULL*/)
	: CDialog(CAddhandListDialog::IDD, pParent)
	, m_pEventHandler ( pEventHandler )
{
	
}

CAddhandListDialog::~CAddhandListDialog()
{
}

void CAddhandListDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_LIST, m_lc);
}


BEGIN_MESSAGE_MAP(CAddhandListDialog, CDialog)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CAddhandListDialog 메시지 처리기입니다.

BOOL CAddhandListDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_reposControl.SetParent(this);
	m_reposControl.AddControl((CWnd*)&m_lc, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnOK, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	m_lc.InsertColumn(0, "EventType", 0, 140);
	m_lc.InsertColumn(1, "Entity", 0, 150);

	if(m_pEventHandler)
	{
		ADDHAND_ITEM_LIST* addhand_list = m_pEventHandler->GetAddhandList();
		for(int i=0; i<addhand_list->GetCount(); i++)
		{
			ADDHAND_ITEM& item = addhand_list->GetAt(i);
			m_lc.InsertItem(i, item.eventType);
			m_lc.SetItemText(i, 1, item.entity);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAddhandListDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(GetSafeHwnd() && m_lc.GetSafeHwnd())
	{
		m_reposControl.MoveControl();
	}
}
