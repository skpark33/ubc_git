// UBCEventSet.h : CUBCEventSet 클래스의 구현입니다.

// CUBCEventSet 구현입니다.

// 코드 생성 위치 2012년 1월 27일 금요일, 오후 3:04

#include "stdafx.h"
#include "UBCEventSet.h"
#include <ODBCINST.H>

#include "EGEventHandler.h"

#include "lib.h"

#include "ci/libConfig/ciXProperties.h"
#include "common/libCommon/ubcUtil.h"


ciSET_DEBUG(10, "CUBCEventSet");


CUBCEventSet*		CUBCEventSet::_instance = NULL;
CCriticalSection	CUBCEventSet::_cs;

CString				CUBCEventSet::m_strDBType = "";

CString				CUBCEventSet::m_strDSN = "";
CString				CUBCEventSet::m_strID = "";
CString				CUBCEventSet::m_strPWD = "";

IMPLEMENT_DYNAMIC(CUBCEventSet, CRecordset)


CUBCEventSet::CUBCEventSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_eventTypeW = "";
	m_eventIndex = 0L;
	m_domainW = "";
	m_entityW = "";
	m_eventTime;
	m_eventBodyW = "";

	m_eventTypeA = "";
	m_domainA = "";
	m_entityA = "";
	m_eventBodyA = "";

	m_nFields = 6;
	m_nDefaultType = snapshot;
}

#define		DEFAULT_DSN_REG		("DSN=ubc_event:Network=TCP/IP:Server=127.0.0.1,1433:Database=ubc:Description=ubc:Trusted_connection=No:")
#define		DEFAULT_DSN_DRV		("SQL Server")
#define		DEFAULT_DSN			("SQL Server")


bool CUBCEventSet::getPropertiesValue(HWND hParentWnd, LPCSTR lpszKey, CString& value)
{
	ciXProperties* prop = ciXProperties::getInstance();
	if(prop == NULL) return false;

	ciString buf = "";
	if(prop->get(lpszKey, buf) && buf.length() > 0)
	{
		value = buf.c_str();
		value.Trim(" \r\n");

		ciDEBUG(10, ("%s=%s", lpszKey, (LPCSTR)value) );
		return true;
	}

	CString msg;
	msg.Format("%s is NULL !!!", lpszKey);
	ciWARN( ((LPCSTR)msg) );

	msg.Insert(0, "WARNING : ");
	POST_SYSTEM_LOG(hParentWnd, 
					CURRENT_TIME_STRING, 
					(LPCSTR)msg );
	return false;
}

bool CUBCEventSet::getCopDsnReg(HWND hParentWnd, LPCSTR lpszDBKey, CString& value)
{
	CString str_key;
	str_key.Format("PS.%s.COP_DSN_REG", lpszDBKey);

	return getPropertiesValue(hParentWnd, str_key, value);
}

bool CUBCEventSet::getCopDsnDrv(HWND hParentWnd, LPCSTR lpszDBKey, CString& value)
{
	CString str_key;
	str_key.Format("PS.%s.COP_DSN_DRV", lpszDBKey);

	return getPropertiesValue(hParentWnd, str_key, value);
}

bool CUBCEventSet::getCopDsn(HWND hParentWnd, LPCSTR lpszDBKey, CString& value)
{
	CString str_key;
	str_key.Format("PS.%s.COP_DSN", lpszDBKey);

	return getPropertiesValue(hParentWnd, str_key, value);
}

bool CUBCEventSet::getCopUsr(HWND hParentWnd, LPCSTR lpszDBKey, CString& value)
{
	CString str_key;
	str_key.Format("PS.%s.COP_USR", lpszDBKey);

	if( !getPropertiesValue(hParentWnd, str_key, value) )
	{
		str_key.Format("PS.%s.USR", lpszDBKey);
		return getPropertiesValue(hParentWnd, str_key, value);
	}
	return true;
}

bool CUBCEventSet::getCopPwd(HWND hParentWnd, LPCSTR lpszDBKey, CString& value)
{
	CString str_key;
	str_key.Format("PS.%s.COP_PWD", lpszDBKey);

	if( !getPropertiesValue(hParentWnd, str_key, value) )
	{
		str_key.Format("PS.%s.PWD", lpszDBKey);
		return getPropertiesValue(hParentWnd, str_key, value);
	}
	return true;
}

bool CUBCEventSet::RegisterODBC(HWND hParentWnd)
{
	CString str_dsn_reg = "";
	CString str_dsn_drv = "";

	//
	CString str_buf = "", str_db_key = "";
	getPropertiesValue(hParentWnd, "PS.PS_OBJECT_MAP.UBC_EVENT", str_buf);
	if(str_buf.Find(',') < 0) // MSDB,MS_DB or MYSQL,MYSQL_DB => find ","
	{
		CString msg;
		msg.Format("PS.PS_OBJECT_MAP.UBC_EVENT is INVALID !!! (%s)", str_buf);
		ciWARN( ((LPCSTR)msg) );

		msg.Insert(0, "WARNING : ");
		POST_SYSTEM_LOG(hParentWnd, 
						CURRENT_TIME_STRING, 
						(LPCSTR)msg );
		return false;
	}
	else
	{
		int pos = 0;
		m_strDBType = str_buf.Tokenize(",", pos);
		str_db_key = str_buf.Tokenize(",", pos); str_db_key.Trim(" ");

		getCopDsnReg(hParentWnd, str_db_key, str_dsn_reg);
		getCopDsnDrv(hParentWnd, str_db_key, str_dsn_drv);
		getCopDsn(hParentWnd, str_db_key, m_strDSN);

		if(!getCopUsr(hParentWnd, str_db_key, m_strID) || m_strID.GetLength()==0)
		{
			CString msg;
			msg.Format("PS.%s.COP_USR is INVALID !!!", str_db_key);
			ciWARN( ((LPCSTR)msg) );

			msg.Insert(0, "WARNING : ");
			POST_SYSTEM_LOG(hParentWnd, 
							CURRENT_TIME_STRING, 
							(LPCSTR)msg );
			return false;
		}

		if(!getCopPwd(hParentWnd, str_db_key, m_strPWD) || m_strPWD.GetLength()==0)
		{
			CString msg;
			msg.Format("PS.%s.COP_PWD is INVALID !!!", str_db_key);
			ciWARN( ((LPCSTR)msg) );

			msg.Insert(0, "WARNING : ");
			POST_SYSTEM_LOG(hParentWnd, 
							CURRENT_TIME_STRING, 
							(LPCSTR)msg );
			return false;
		}
	}

	//
	if( str_dsn_reg.GetLength() == 0 )
	{
		str_dsn_reg = DEFAULT_DSN_REG;
		ciWARN( ("PS.%s.COP_DSN_REG=%s", str_db_key, str_dsn_reg) );
	}
	if( str_dsn_drv.GetLength() == 0 )
	{
		str_dsn_drv = DEFAULT_DSN_DRV;
		ciWARN( ("PS.%s.COP_DSN_DRV=%s", str_db_key, str_dsn_drv) );
	}
	if( m_strDSN.GetLength() == 0 )
	{
		m_strDSN = DEFAULT_DSN;
		ciWARN( ("PS.%s.COP_DSN=%s", str_db_key, m_strDSN) );
	}

	//
	char* sz_dsn_reg = new char[str_dsn_reg.GetLength()+1];
	::ZeroMemory(sz_dsn_reg, str_dsn_reg.GetLength()+1);
	sprintf(sz_dsn_reg, "%s", (LPCSTR)str_dsn_reg);
	if(m_strDBType == "MSDB")
	{
		int len = str_dsn_reg.GetLength();
		for(int i=0; i<len; i++)
		{
			if(sz_dsn_reg[i] == ':') sz_dsn_reg[i] = '\0';
		}
	}
	else if(m_strDBType == "MYSQL")
	{
		int len = str_dsn_reg.GetLength();
		for(int i=0; i<len; i++)
		{
			if(sz_dsn_reg[i] == ':') sz_dsn_reg[i] = ';';
		}
	}

	//
	if (SQLConfigDataSource(NULL, ODBC_REMOVE_DSN, str_dsn_drv, sz_dsn_reg) == FALSE)
	{
/*
		WORD	iError = 1;
		DWORD	pfErrorCode;
		char	lpszErrorMsg[301];
		WORD	cbErrorMsgMax = 300;
		RETCODE	rc;

		while (SQL_NO_DATA != (rc = SQLInstallerError(
			iError ++,
			& pfErrorCode,
			lpszErrorMsg,
			cbErrorMsgMax,
			NULL )))
		{
			if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
			{
				AfxMessageBox(lpszErrorMsg);
			}
		}
*/
		ciWARN( ("Fail to SQLConfigDataSource(ODBC_REMOVE_DSN) !!!") );
	}

	//
	if (SQLConfigDataSource(NULL, ODBC_ADD_DSN, str_dsn_drv, sz_dsn_reg) == FALSE)
	{
/*
		WORD	iError = 1;
		DWORD	pfErrorCode;
		char	lpszErrorMsg[301];
		WORD	cbErrorMsgMax = 300;
		RETCODE	rc;

		while (SQL_NO_DATA != (rc = SQLInstallerError(
			iError ++,
			& pfErrorCode,
			lpszErrorMsg,
			cbErrorMsgMax,
			NULL )))
		{
			if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
			{
				AfxMessageBox(lpszErrorMsg);
			}
		}
*/
		ciWARN( ("Fail to SQLConfigDataSource(ODBC_ADD_DSN) !!!") );
		POST_SYSTEM_LOG(hParentWnd, 
						CURRENT_TIME_STRING, 
						"WARNING : Fail to SQLConfigDataSource(ODBC_ADD_DSN)" );

		delete[]sz_dsn_reg;
		return false;
	}
	delete[]sz_dsn_reg;
	return true;
}

CUBCEventSet* CUBCEventSet::getInstance()
{
	_cs.Lock();
	if(_instance == 0)
	{
		_instance = new CUBCEventSet();
		if(_instance->OpenDatabase() == false)
		{
			delete _instance;
			_instance = NULL;
		}
	}
	_cs.Unlock();
	return _instance;
}

void CUBCEventSet::clearInstance()
{
	_cs.Lock();
	if(_instance)
	{
		_instance->CloseDatabase();
		delete _instance;
		_instance = NULL;
	}
	_cs.Unlock();
}

bool CUBCEventSet::OpenDatabase()
{
	if( !IsOpen() )
	{
		if( !Open() )
		{
			//::AfxMessageBox(_T("데이버테이스 열기 실패 !!!"), MB_ICONSTOP);
			return false;
		}
	}
	return true;
}

void CUBCEventSet::CloseDatabase()
{
	if( IsOpen() )
	{
		Close();
	}
}

CString CUBCEventSet::GetDefaultConnect()
{
	//return "DSN=ubc_event;UID=ubc;PWD=sqicop;";//DATABASE=dbo;PORT=3306;Charset=sjis;";
	//return _T("DSN=ubc_event;UID=ubc;PWD=-507263a;APP=Microsoft\x00ae Visual Studio\x00ae 2005;WSID=COMTEC-12704");
	//return _T("DSN=ubc_event;UID=ubc;PWD=-507263a");
	static CString str_def_connect = "";
	if(str_def_connect.GetLength() == 0)
	{
		str_def_connect.Format("DSN=%s;UID=%s;PWD=%s", m_strDSN, m_strID, m_strPWD);
		ciDEBUG(10, ("DSNDefaultConnect : %s", str_def_connect) );
	}
	return str_def_connect;
}

CString CUBCEventSet::GetDefaultSQL()
{
	return _T("[dbo].[ubc_event]");
}

void CUBCEventSet::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// RFX_Text() 및 RFX_Int() 같은 매크로는 데이터베이스의 필드
// 형식이 아니라 멤버 변수의 형식에 따라 달라집니다.
// ODBC에서는 자동으로 열 값을 요청된 형식으로 변환하려고 합니다

	if( ubcUtil::getInstance()->isGlobal() )
	{
		ciDEBUG(10, ("Global(Unicode) mode") );

		// unicode mode
		RFX_Text  (pFX, _T("[eventType]"),  m_eventTypeW);
		RFX_BigInt(pFX, _T("[eventIndex]"), m_eventIndex);
		RFX_Text  (pFX, _T("[domain]"),     m_domainW);
		RFX_Text  (pFX, _T("[entity]"),     m_entityW);
		RFX_Date  (pFX, _T("[eventTime]"),  m_eventTime);
		RFX_Text  (pFX, _T("[eventBody]"),  m_eventBodyW, 1<<16);		// 2^16=64K
	}
	else
	{
		ciDEBUG(10, ("Not Global(ANSI) mode") );

		// ansi mode
		RFX_Text  (pFX, _T("[eventType]"),  m_eventTypeA);
		RFX_BigInt(pFX, _T("[eventIndex]"), m_eventIndex);
		RFX_Text  (pFX, _T("[domain]"),     m_domainA);
		RFX_Text  (pFX, _T("[entity]"),     m_entityA);
		RFX_Date  (pFX, _T("[eventTime]"),  m_eventTime);
		RFX_Text  (pFX, _T("[eventBody]"),  m_eventBodyA, 1<<16);		// 2^16=64K
	}
}
/////////////////////////////////////////////////////////////////////////////
// CUBCEventSet 진단

#ifdef _DEBUG
void CUBCEventSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CUBCEventSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


void CUBCEventSet::CheckTimeoutEvent()
{
	if(m_pDatabase)
	{
		if(m_strDBType == "MSDB")
		{
			ciDEBUG(10, ("CheckTimeoutEvent(MSDB)") );
			m_pDatabase->ExecuteSQL("set dateformat ymd delete from ubc_event where eventTime < convert(varchar(19),dateadd(n,-2,getdate()),120);"); // delete before 2min-event
		}
		else if(m_strDBType == "MYSQL")
		{
			ciDEBUG(10, ("CheckTimeoutEvent(MYSQL)") );
			m_pDatabase->ExecuteSQL("delete from dbo.ubc_event where eventTime < DATE_ADD(now(), INTERVAL -2 minute);"); // delete before 2min-event
		}
	}
	else
	{
		ciWARN( ("CheckTimeoutEvent(Fail : m_pDatabase is NULL)") );
	}
}
