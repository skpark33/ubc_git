// EventGateway.h : main header file for the EventGateway application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols

class EGWorldThread;

// CEventGatewayApp:
// See EventGateway.cpp for the implementation of this class
//

class CEventGatewayApp : public CWinApp
{
public:
	CEventGatewayApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();

	DECLARE_MESSAGE_MAP()

	virtual int ExitInstance();

protected:

	EGWorldThread*		m_worldThread;

public:
	static	void	CheckLogFileDate();

};

extern CEventGatewayApp theApp;