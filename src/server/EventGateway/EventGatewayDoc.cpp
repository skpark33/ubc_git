// EventGatewayDoc.cpp : implementation of the CEventGatewayDoc class
//

#include "stdafx.h"
#include "EventGateway.h"

#include "EventGatewayDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CEventGatewayDoc

IMPLEMENT_DYNCREATE(CEventGatewayDoc, CDocument)

BEGIN_MESSAGE_MAP(CEventGatewayDoc, CDocument)
END_MESSAGE_MAP()


// CEventGatewayDoc construction/destruction

CEventGatewayDoc::CEventGatewayDoc()
{
	// TODO: add one-time construction code here

}

CEventGatewayDoc::~CEventGatewayDoc()
{
}

BOOL CEventGatewayDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CEventGatewayDoc serialization

void CEventGatewayDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CEventGatewayDoc diagnostics

#ifdef _DEBUG
void CEventGatewayDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CEventGatewayDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CEventGatewayDoc commands
