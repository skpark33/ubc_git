// EventGatewayDoc.h : interface of the CEventGatewayDoc class
//


#pragma once


class CEventGatewayDoc : public CDocument
{
protected: // create from serialization only
	CEventGatewayDoc();
	DECLARE_DYNCREATE(CEventGatewayDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CEventGatewayDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


