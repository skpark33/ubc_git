#ifndef _customerReplyImpl_h_
#define _customerReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class customerReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	customerReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~customerReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _create();
    virtual ciBoolean _remove();
    virtual ciBoolean _set();
    virtual ciBoolean _initObject();

	ciBoolean _createODData(ciString& customerId);
	ciBoolean _createRootSite(ciString& customerId, ciLong& errCode, ciString& errMsg);
	ciBoolean _removeODData(ciString& customerId);
	ciBoolean _removeRootSite(ciString& customerId);
	ciBoolean _loadURL();
	ciBoolean _removeODS(ciString& customerId, const char* className);
	ciBoolean	_insertOneByOne(cciPS* pPS, ciString& tableName, ciString& dataFile);

};

#endif // _customerReplyImpl_h_
