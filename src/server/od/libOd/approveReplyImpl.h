#ifndef _approveReplyImpl_h_
#define _approveReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class approveReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	approveReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~approveReplyImpl() ;

	enum { aproved_init=0, approve_requested, approve_approved, approve_rejected, approve_removed };

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

    virtual ciBoolean _create();
    virtual ciBoolean _remove();
    virtual ciBoolean	_set();
	virtual ciBoolean	_postEvent(const char* directive);

	ciBoolean _genId(int idx, const char* idName);


};



#endif // _approveReplyImpl_h_
