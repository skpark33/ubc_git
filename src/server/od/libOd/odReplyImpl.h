#ifndef _odReplyImpl_h_
#define _odReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciScopeInfo.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciException.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class odReplyImpl : public virtual aciReply {
public:
	odReplyImpl();
	virtual ~odReplyImpl() {};

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

    virtual ciBoolean _get();
    
	ciBoolean _createSite();
	ciBoolean _getUnusedProgram();
	ciBoolean _getCategory();
	ciBoolean _getPmId(ciString& pmId);

	ciBoolean _getSiteLevel(const char* siteId, ciShort& siteLevel);

protected:
	ciString			_mgrId;
};


#endif // _odReplyImpl_h_
