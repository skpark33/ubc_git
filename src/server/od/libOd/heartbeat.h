/*! \class heartbeat
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _heartbeat_h_
#define _heartbeat_h_


#include <ci/libTimer/ciWinTimer.h>


class heartbeat :  public  ciWinPollable {

public:
	static heartbeat*	getInstance();
	static void	clearInstance();
	virtual ~heartbeat() ;
	virtual void processExpired(ciString name, int counter, int interval);

protected:
	heartbeat();
	ciBoolean _postEvent();
	ciBoolean _heartbeatLog(const char* succeed, const char* nowStr);

	static heartbeat*	_instance;
	static ciMutex			_instanceLock;


};	

#endif //_heartbeat_h_
