
#include "pmoReplyImpl.h"
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciCall.h>

ciSET_DEBUG(10, "pmoReplyImpl");


pmoReplyImpl::~pmoReplyImpl() 
{
    ciDEBUG(7, ("~pmoReplyImpl()"));
}

ciBoolean
pmoReplyImpl::_initObject()
{
    ciDEBUG(7, ("~pmoReplyImpl()"));

	_object->addItemAsKey("mgrId", new cciString());
    _object->addItemAsKey("pmId",new cciString());
 
    _object->addItem("ipAddress", new cciString());
    _object->addItem("ipAddress1", new cciString());
    _object->addItem("ipAddress2", new cciString());
    _object->addItem("ftpAddress", new cciString());
    _object->addItem("ftpAddress1", new cciString());
    _object->addItem("ftpAddress2", new cciString());
    _object->addItem("ftpMirror", new cciString());
    _object->addItem("ftpMirror1", new cciString());
    _object->addItem("ftpMirror2", new cciString());
    _object->addItem("ftpId", new cciString());
    _object->addItem("ftpPasswd", new cciString());
    _object->addItem("ftpPort", new cciInteger32());
    _object->addItem("socketPort", new cciInteger32());
    _object->addItem("currentConnectionCount", new cciInteger32());
    _object->addItem("lastUpdateTime", new cciTime());
    _object->addItem("adminState", new cciBoolean());
	
    _fromEntity(0,"mgrId");
    _fromEntity(1,"pmId");

    return ciTrue;
}

ciBoolean
pmoReplyImpl::_execute()
{
	return ciFalse;
}

