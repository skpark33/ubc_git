#ifndef _programLocationReplyImpl_h_
#define _programLocationReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class programLocationReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	programLocationReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~programLocationReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

	virtual ciBoolean	_create();
	virtual ciBoolean	_remove();

};

#endif // _programLocationReplyImpl_h_
