 /*! \file odWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _odWorld_h_
#define _odWorld_h_

#include <aci/libCall/aciCallWorld.h> 
#include "common/libAGTMux/AGTMuxSession.h"

class odWorld : public aciCallWorld {
public:
	odWorld();
	~odWorld();
 	
	//[
	//	From aciCallWorld
	//
	virtual aciCall*    newCallImpl(const char* pname);
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);

	static int createPMO(char* arg);

	//
	//]

protected:	
	AGTMuxSession* _muxSession;
	
};
		
#endif //_odWorld_h_
