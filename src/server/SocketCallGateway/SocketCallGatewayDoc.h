// SocketCallGatewayDoc.h : interface of the CSocketCallGatewayDoc class
//


#pragma once


class CSocketCallGatewayDoc : public CDocument
{
protected: // create from serialization only
	CSocketCallGatewayDoc();
	DECLARE_DYNCREATE(CSocketCallGatewayDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CSocketCallGatewayDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


