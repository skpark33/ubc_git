// SocketCallGateway.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "SocketCallGateway.h"
#include "MainFrm.h"

#include "SocketCallGatewayDoc.h"
#include "SocketCallGatewayView.h"

#include "SCGWorld.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


ciSET_DEBUG(10, "CSocketCallGatewayApp");


// CSocketCallGatewayApp

BEGIN_MESSAGE_MAP(CSocketCallGatewayApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CSocketCallGatewayApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()


// CSocketCallGatewayApp construction

CSocketCallGatewayApp::CSocketCallGatewayApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CSocketCallGatewayApp object

CSocketCallGatewayApp theApp;


// CSocketCallGatewayApp initialization

BOOL CSocketCallGatewayApp::InitInstance()
{
	ciDebug::setDebugOn();
	//ciDebug::logOpen(".\\log\\CSocketCallGateway.log");
	CheckLogFileDate();

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CSocketCallGatewayDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CSocketCallGatewayView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	//
	m_worldThread = new SCGWorldThread();
	ciDEBUG(10, ("SCGWorldThread init"));
	m_worldThread->init();
	ciDEBUG(10, ("SCGWorldThread before start"));
	m_worldThread->start();
	ciDEBUG(10, ("SCGWorldThread after start"));

	ciDebug::setDebugOn();
	ciWorld::ylogOn();

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	cmdInfo.m_nShellCommand = CCommandLineInfo::FileNew;	// <-- arguments 무시

	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand

	return TRUE;
}

int CSocketCallGatewayApp::ExitInstance()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	ciDEBUG(10, ("SCGWorldThread before stop"));
	if(m_worldThread)
	{
		m_worldThread->stop();
		delete m_worldThread;
	}
	ciDEBUG(10, ("SCGWorldThread end stop"));

	return CWinApp::ExitInstance();
}


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CSocketCallGatewayApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CSocketCallGatewayApp message handlers



#define		DEFAULT_LOG_FILE_PREFIX			"SocketCallGateway"
#define		DEFAULT_LOG_FILE_TERM			(7)		// days

void CSocketCallGatewayApp::CheckLogFileDate()
{
	// new logfile open
	static int log_month = -1;
	static int log_day = -1;

	CTime cur_tm = CTime::GetCurrentTime();
	int cur_month = cur_tm.GetMonth();
	int cur_day = cur_tm.GetDay();

	// check date
	if(log_month == cur_month && log_day == cur_day) return;

	// new logfile open
	CString log_path;
	log_path.Format(".\\log\\%s_%02d%02d.log", DEFAULT_LOG_FILE_PREFIX, cur_month, cur_day);

	ciDebug::logClose();
	ciDebug::logOpen(log_path);
	log_month = cur_month;
	log_day = cur_day;

	// delete old logfile
	CMapStringToString map_logfiles_in_terms;
	for(int i=0; i<DEFAULT_LOG_FILE_TERM; i++)
	{
		CTimeSpan tm_span(i, 0, 0, 0);

		CTime old_tm = cur_tm - tm_span;
		CString log_file;
		log_file.Format("%s_%02d%02d.log", DEFAULT_LOG_FILE_PREFIX, old_tm.GetMonth(), old_tm.GetDay());

		map_logfiles_in_terms.SetAt(log_file.MakeLower(), log_file.MakeLower());
	}

	CString filter;
	filter.Format(".\\log\\%s_*.log", DEFAULT_LOG_FILE_PREFIX);

	CFileFind ff;
	BOOL bWorking = ff.FindFile(filter);
	while (bWorking)
	{
		bWorking = ff.FindNextFile();

		if (ff.IsDots()) continue;
		if (ff.IsDirectory()) continue;

		CString file_name = ff.GetFileName();
		CString value;
		if( map_logfiles_in_terms.Lookup(file_name.MakeLower(), value) ) continue;

		::DeleteFile(ff.GetFilePath());
	}
	ff.Close();
}
