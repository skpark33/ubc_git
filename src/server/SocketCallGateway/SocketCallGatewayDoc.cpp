// SocketCallGatewayDoc.cpp : implementation of the CSocketCallGatewayDoc class
//

#include "stdafx.h"
#include "SocketCallGateway.h"

#include "SocketCallGatewayDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSocketCallGatewayDoc

IMPLEMENT_DYNCREATE(CSocketCallGatewayDoc, CDocument)

BEGIN_MESSAGE_MAP(CSocketCallGatewayDoc, CDocument)
END_MESSAGE_MAP()


// CSocketCallGatewayDoc construction/destruction

CSocketCallGatewayDoc::CSocketCallGatewayDoc()
{
	// TODO: add one-time construction code here

}

CSocketCallGatewayDoc::~CSocketCallGatewayDoc()
{
}

BOOL CSocketCallGatewayDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CSocketCallGatewayDoc serialization

void CSocketCallGatewayDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CSocketCallGatewayDoc diagnostics

#ifdef _DEBUG
void CSocketCallGatewayDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSocketCallGatewayDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CSocketCallGatewayDoc commands
