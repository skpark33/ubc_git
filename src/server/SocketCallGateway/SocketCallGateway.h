// SocketCallGateway.h : main header file for the SocketCallGateway application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols

class SCGWorldThread;

// CSocketCallGatewayApp:
// See SocketCallGateway.cpp for the implementation of this class
//

class CSocketCallGatewayApp : public CWinApp
{
public:
	CSocketCallGatewayApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();

	DECLARE_MESSAGE_MAP()

	virtual int ExitInstance();

protected:

	SCGWorldThread*		m_worldThread;

public:
	static	void	CheckLogFileDate();

};

extern CSocketCallGatewayApp theApp;