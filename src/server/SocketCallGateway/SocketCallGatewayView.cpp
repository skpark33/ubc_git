// SocketCallGatewayView.cpp : implementation of the CSocketCallGatewayView class
//

#include "stdafx.h"
#include "SocketCallGateway.h"

#include "SocketCallGatewayDoc.h"
#include "SocketCallGatewayView.h"

#include "CallSocket.h"

#include "ci/libConfig/ciXProperties.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define		TIMER_LOG_FILE_CHANGE			1024
#define		TIMER_LOG_FILE_CHANGE_TIME		(1000 * 60) // every 1 min


#define		DEFAULT_LISTEN_PORT		(1400)
#define		SYSTEM_LOG_HEIGHT		(120)

#define		LOG_ITEM_COUNT_LIMIT	(10000)
#define		LOG_ITEM_CLEAN_COUNT	(100)


ciSET_DEBUG(10, "CSocketCallGatewayView");


// CSocketCallGatewayView

IMPLEMENT_DYNCREATE(CSocketCallGatewayView, CFormView)

BEGIN_MESSAGE_MAP(CSocketCallGatewayView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_MESSAGE(WM_POST_CALL_LOG, OnPostCallLog)
	ON_MESSAGE(WM_POST_SYSTEM_LOG, OnPostSystemLog)
	ON_MESSAGE(WM_RESET_LISTEN_SOCKET, OnResetListenSocket)
	ON_MESSAGE(WM_REPLY_FROM_CALL_THREAD, OnReplyFromCallThread)
	ON_MESSAGE(WM_CLOSE_CALL_SOCKET, OnCloseCallSocket)
END_MESSAGE_MAP()

// CSocketCallGatewayView construction/destruction

CSocketCallGatewayView::CSocketCallGatewayView()
	: CFormView(CSocketCallGatewayView::IDD)
{
	// TODO: add construction code here
	ciDEBUG(10, ("CSocketCallGatewayView()") );
}

CSocketCallGatewayView::~CSocketCallGatewayView()
{
	ciDEBUG(10, ("~CSocketCallGatewayView()") );
	m_sockListen.StopListen();
}

void CSocketCallGatewayView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_lcLog);
	DDX_Control(pDX, IDC_LIST_SYSTEM_LOG, m_lcSystemLog);
}

BOOL CSocketCallGatewayView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CSocketCallGatewayView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	ciDEBUG(10, ("OnInitialUpdate") );

	// setting Log-ListCtrl
	m_lcLog.SetExtendedStyle(m_lcLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcLog.InsertColumn(COL_COUNT, "Count", LVCFMT_RIGHT, 0);
	m_lcLog.InsertColumn(COL_CONNECT_STATUS, "Connect", 0, 25);
	m_lcLog.InsertColumn(COL_CALL_TIME, "Call Time", 0, 120);
	m_lcLog.InsertColumn(COL_CALL_STRING, "Call String", 0, 240);
	m_lcLog.InsertColumn(COL_REPLY_TIME, "Reply Time", 0, 120);
	m_lcLog.InsertColumn(COL_REPLY_STRING, "Reply String", 0, 240);

	// setting SystemLog-ListCtrl
	m_lcSystemLog.SetExtendedStyle(m_lcSystemLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcSystemLog.InsertColumn(SYS_COL_TIME, "Time", 0, 120);
	m_lcSystemLog.InsertColumn(SYS_COL_STRING, "System Log", 0, 400);

	// check receive-buffer-size of socket
	ciDEBUG(10, ("Check Socket Receive Buffer Size") );
	GetSocketReceiveBufferSize();
	ciDEBUG(10, ("Socket Receive Buffer Size=%d", CCallSocket::m_nReadBufferSize) );
	CCallSocket::m_hParentWnd = GetSafeHwnd();

	// get listen-socket-port
	ciXProperties* prop = ciXProperties::getInstance();
	ciLong port = DEFAULT_LISTEN_PORT;
	prop->get("COP.SCGW.PORT", port);
	ciDEBUG(10, ("Liten Socket Port=%d", port) );
	m_sockListen.Init(GetSafeHwnd(), port);

	// start listen
	ciDEBUG(10, ("Open Liten Socket") );
	PostMessage(WM_RESET_LISTEN_SOCKET, NULL, NULL);

	// set timer of checking timeout-thread
	if( SetTimer(TIMER_TIMEOUT_CALL_THREAD_CHECK_ID, TIMER_TIMEOUT_CALL_THREAD_CHECK_TIME, NULL) )
	{
		ciDEBUG(10, ("Success to SetTimer(CALL_THREAD_CHECK)") );
	}
	else
	{
		// fail to timer -> something wrong
		ciERROR( ("Fail to SetTimer(CALL_THREAD_CHECK) !!!") );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to SetTimer(CALL_THREAD_CHECK)" );
	}

	SetTimer(TIMER_LOG_FILE_CHANGE, TIMER_LOG_FILE_CHANGE_TIME, NULL);
}


// CSocketCallGatewayView diagnostics

#ifdef _DEBUG
void CSocketCallGatewayView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSocketCallGatewayView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CSocketCallGatewayDoc* CSocketCallGatewayView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSocketCallGatewayDoc)));
	return (CSocketCallGatewayDoc*)m_pDocument;
}
#endif //_DEBUG


// CSocketCallGatewayView message handlers


void CSocketCallGatewayView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(GetSafeHwnd())
	{
		if(m_lcLog.GetSafeHwnd())
			m_lcLog.MoveWindow(0, 0, cx, cy-SYSTEM_LOG_HEIGHT);
		if(m_lcSystemLog.GetSafeHwnd())
			m_lcSystemLog.MoveWindow(0, cy-SYSTEM_LOG_HEIGHT, cx, SYSTEM_LOG_HEIGHT);
	}
}

void CSocketCallGatewayView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CFormView::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case TIMER_LOG_FILE_CHANGE:
		KillTimer(TIMER_LOG_FILE_CHANGE);

		CSocketCallGatewayApp::CheckLogFileDate();

		SetTimer(TIMER_LOG_FILE_CHANGE, TIMER_LOG_FILE_CHANGE_TIME, NULL);
		break;

	case TIMER_TIMEOUT_CALL_THREAD_CHECK_ID:
		// check timeout-thread
		KillTimer(TIMER_TIMEOUT_CALL_THREAD_CHECK_ID);

		//
		CheckTimeoutCallThread();

		//
		if( SetTimer(TIMER_TIMEOUT_CALL_THREAD_CHECK_ID, TIMER_TIMEOUT_CALL_THREAD_CHECK_TIME, NULL) )
		{
			// success
		}
		else
		{
			// fail to timer -> something wrong
			ciERROR( ("Fail to SetTimer(TIMEOUT_CALL_THREAD_CHECK) !!!") );
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							"ERROR : Fail to SetTimer(TIMEOUT_CALL_THREAD_CHECK)" );
		}
		break;

	case TIMER_RESET_LISTEN_SOCKET_ID:
		// reset listen socket
		KillTimer(TIMER_RESET_LISTEN_SOCKET_ID);

		ciDEBUG(10, ("Reset Listen Socket") );
		PostMessage(WM_RESET_LISTEN_SOCKET, NULL, NULL);
		break;
	}
}

LRESULT CSocketCallGatewayView::OnPostCallLog(WPARAM wParam, LPARAM lParam)
{
	ciDEBUG(10, ("OnPostCallLog(0x%08X,0x%08X)", wParam, lParam) );

	CALL_LOG_ITEM* item = (CALL_LOG_ITEM*)wParam;
	if(item == NULL)
	{
		// log item is NULL
		ciWARN( ("CALL_LOG_ITEM is NULL !!!") );
		return 0;
	}

	int idx = -1;

	if(item->callSocket)
	{
		LVFINDINFO fi;
		fi.flags = LVFI_PARAM;
		fi.lParam = (DWORD_PTR)item->callSocket;

		idx = m_lcLog.FindItem(&fi);
	}

	if(idx < 0)
	{
		// not exist -> add new
		ciDEBUG(10, ("CallLog_NEW(socket=0x%08X,Status=%s,cTime=%s,cString=%s)", 
			item->callSocket, 
			item->connect, 
			item->callTime, 
			item->callString ) );

		// just count
		static ULONGLONG count = 0;
		char buf[32];
		sprintf(buf, "%I64u", count++);

		// add new
		idx = m_lcLog.GetItemCount();
		m_lcLog.InsertItem(idx, buf );
		m_lcLog.SetItemText(idx, COL_CONNECT_STATUS,	item->connect );
		m_lcLog.SetItemText(idx, COL_CALL_TIME,		item->callTime );
		m_lcLog.SetItemText(idx, COL_CALL_STRING,		item->callString );

		m_lcLog.SetItemData(idx, (DWORD_PTR)item->callSocket);

		// check item-count of listctrl
		CheckListCtrlLimit();
	}
	else
	{
		// exist -> overwrite
		ciDEBUG(10, ("CallLog_Update(idx=%d,socket=0x%08X,Status=%s,cTime=%s,cString=%s)", 
			idx, 
			item->callSocket, 
			item->connect, 
			item->callTime, 
			item->callString ) );

		m_lcLog.SetItemText(idx, COL_CONNECT_STATUS,	item->connect );
		m_lcLog.SetItemText(idx, COL_CALL_TIME,			item->callTime );
		if(item->callString.GetLength() > 0)
			m_lcLog.SetItemText(idx, COL_CALL_STRING,		item->callString );
	}

	delete item;

	return 1;
}

LRESULT CSocketCallGatewayView::OnPostSystemLog(WPARAM wParam, LPARAM lParam)
{
	ciDEBUG(10, ("OnPostSystemLog()") );

	CString* tm_str = (CString*)wParam;
	CString* err_str = (CString*)lParam;
	if(tm_str == NULL || err_str == NULL)
	{
		// null item exist -> something wrong
		if(tm_str == NULL)
		{
			ciWARN( ("TimeString is NULL !!!") );
		}
		else
			delete tm_str;

		if(err_str == NULL)
		{
			ciWARN( ("Errortring is NULL !!!") );
		}
		else
			delete err_str;

		return 0;
	}

	ciDEBUG(10, ("SystemLog(Time=%s,SysLog=%s)", (LPCSTR)*tm_str, (LPCSTR)*err_str) );

	// check log-count
	if(m_lcSystemLog.GetItemCount() > 10000)
	{
		for(int i=0; i<100; i++)
			m_lcSystemLog.DeleteItem(0);
	}

	// add new log
	int idx = m_lcSystemLog.GetItemCount();
	m_lcSystemLog.InsertItem(idx, "" );
	m_lcSystemLog.SetItemText(idx, SYS_COL_TIME,		(LPCSTR)*tm_str );
	m_lcSystemLog.SetItemText(idx, SYS_COL_STRING,	(LPCSTR)*err_str );
	m_lcSystemLog.EnsureVisible(idx, FALSE);

	delete tm_str;
	delete err_str;

	return 1;
}

LRESULT CSocketCallGatewayView::OnResetListenSocket(WPARAM wParam, LPARAM lParam)
{
	ciDEBUG(10, ("OnResetListenSocket()") );

	m_sockListen.Close();

	if(m_sockListen.StartListen())
	{
		// success to StartListen
		ciDEBUG(10, ("Success to open ListenSocket") );
		CString msg;
		msg.Format("SUCCESS : Success to open ListenSocket (Port:%d)", m_sockListen.m_nPort);
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						(LPCSTR)msg );
	}
	else
	{
		// fail to StartListen -> retry
		DWORD err_code = GetLastError();

		ciWARN( ("Fail to open ListenSocket !!! (%s)", GetErrorMessageString(err_code)) );

		CString err_str;
		err_str.Format("ERROR : Fail to open ListenSocket (%s)", GetErrorMessageString(err_code) );
		POST_SYSTEM_LOG(GetSafeHwnd(), 
						CURRENT_TIME_STRING, 
						err_str );

		// retry
		if( SetTimer(TIMER_RESET_LISTEN_SOCKET_ID, TIMER_RESET_LISTEN_SOCKET_TIME, NULL) )
		{
			// success
		}
		else
		{
			// fail to settimer -> something wrong
			ciERROR( ("Fail to SetTimer(RESET_LISTEN_SOCKET) !!!") );
			POST_SYSTEM_LOG(GetSafeHwnd(), 
							CURRENT_TIME_STRING, 
							"ERROR : Fail to SetTimer(RESET_LISTEN_SOCKET)" );
		}
	}

	return 1;
}

LRESULT CSocketCallGatewayView::OnReplyFromCallThread(WPARAM wParam, LPARAM lParam)
{
	ciDEBUG(10, ("OnReplyFromCallThread(0x%08X,0x%08X)", wParam, lParam) );

	CCallSocket* call_socket = (CCallSocket*)wParam;
	CString* reply_string = (CString*)lParam;

	if(call_socket==NULL)
	{
		// call-socket is NULL -> something wrong
		ciWARN( ("Reply from NULL-CallSocket !!!"));
		if(reply_string) delete reply_string;
		return 0;
	}
	if(reply_string==NULL)
	{
		// reply-string is NULL -> something wrong
		ciWARN( ("NULL Reply from CallThread !!! (0x%08X)", call_socket));
		call_socket->SetDeleteThread();
		call_socket->Close();
		PostMessage(WM_CLOSE_CALL_SOCKET, (WPARAM)call_socket, NULL);
		return 0;
	}

	//
	LVFINDINFO fi;
	fi.flags = LVFI_PARAM;
	fi.lParam = (DWORD_PTR)call_socket;

	int idx = m_lcLog.FindItem(&fi);
	if(idx < 0)
	{
		// reply from timeout-callthread -> deleted by timeout-thread timer
		ciWARN( ("Reply from Timeout-CallThread !!! (0x%08X) ReplyString=%s", call_socket, *reply_string));
		delete reply_string;
		return 0;
	}

	// show reply-string
	m_lcLog.SetItemText(idx, COL_REPLY_TIME, CURRENT_TIME_STRING);
	m_lcLog.SetItemText(idx, COL_REPLY_STRING, *reply_string);

	// send to connector
	if(call_socket->SendPacket(*reply_string))
	{
		// well done
		ciDEBUG(10, ("Success to reply (0x%08X) [%s:%d]", call_socket, call_socket->m_strPeerName, call_socket->m_nPeerPort) );
	}
	else
	{
		// fail to send
		ciWARN(("Fail to send ReplyPacket !!! (0x%08X) ReplyString=%s", call_socket, *reply_string));
		m_lcLog.SetItemText(idx, COL_REPLY_TIME, CURRENT_TIME_STRING);
		m_lcLog.SetItemText(idx, COL_REPLY_STRING, *reply_string);
	}

	delete reply_string;

	// check (normaly) terminated-thread flag & close
	call_socket->SetDeleteThread();
	call_socket->Close();
	PostMessage(WM_CLOSE_CALL_SOCKET, (WPARAM)call_socket, NULL);

	return 1;
}

LRESULT CSocketCallGatewayView::OnCloseCallSocket(WPARAM wParam, LPARAM lParam)
{
	ciDEBUG(10, ("OnCloseCallSocket(0x%08X,0x%08X)", wParam, lParam) );

	CCallSocket* call_socket = (CCallSocket*)wParam;
	if(call_socket == NULL) return 0;

	LVFINDINFO fi;
	fi.flags = LVFI_PARAM;
	fi.lParam = (DWORD_PTR)call_socket;

	int idx = m_lcLog.FindItem(&fi);
	if(idx < 0)
	{
		// not found list
		ciDEBUG(10, ("Not Exist CallSocket in ListCtrl (0x%08X)", call_socket) );
		return 0;
	}

	// show to disconnected
	m_lcLog.SetItemText(idx, COL_CONNECT_STATUS, "X");
	m_lcLog.SetItemData(idx, NULL);

	// if exist err_msg, show
	if(call_socket->m_strErrMsg.GetLength() > 0)
	{
		m_lcLog.SetItemText(idx, COL_REPLY_TIME, CURRENT_TIME_STRING);
		m_lcLog.SetItemText(idx, COL_REPLY_STRING, call_socket->m_strErrMsg);
	}

	// delete socket
	delete call_socket;

	ciDEBUG(10, ("CallSocket Deleted from ListCtrl (idx:%d,0x%08X)", idx, call_socket) );

	return 1;
}

void CSocketCallGatewayView::GetSocketReceiveBufferSize()
{
	SOCKET sock;
	int snd_buf_size, rcv_buf_size;
	int state;
	socklen_t len;

	//
	sock = socket(PF_INET, SOCK_STREAM, 0);
	if(INVALID_SOCKET == sock)
	{
		// socket create error
		return;
	}

	//
	len = sizeof(snd_buf_size);
	state = getsockopt(sock, SOL_SOCKET, SO_SNDBUF, (char*)&snd_buf_size, &len);
	if(-1 == state)
	{
		// getsockopt error
		::closesocket(sock);
		return;
	}

	//
	len = sizeof(rcv_buf_size);
	state = getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char*)&rcv_buf_size, &len);
	if(-1 == state)
	{
		// getsockopt error
		::closesocket(sock);
		return;
	}

	//printf("데이터 입력받기 위한 소켓의 버퍼 크기 : %d \n", rcv_buf);
	//printf("데이터 출력하기 위한 소켓의 버퍼 크기 : %d \n", snd_buf);
	CCallSocket::m_nReadBufferSize = rcv_buf_size;

	::closesocket(sock);
}

void CSocketCallGatewayView::CheckTimeoutCallThread()
{
	// check timeout-thread
	ciDEBUG(10, ("CheckTimeoutCallThread()") );

	CCallSocket::m_csTimeoutCallThread.Lock();

	// get current-time & span-time
	CTime cur_tm = CTime::GetCurrentTime();
	CTimeSpan span_tm(0, TIMEOUT_CALL_HOUR, TIMEOUT_CALL_MINUTE, 0);

	POSITION pos = CCallSocket::m_listTimeoutCallThread.GetHeadPosition();
	POSITION old_pos;
	for (int i=0; i<CCallSocket::m_listTimeoutCallThread.GetCount(); i++)
	{
		old_pos = pos;
		TIMEOUT_CALL_THREAD_ITEM& item = CCallSocket::m_listTimeoutCallThread.GetNext(pos);

		try
		{
			DWORD dwExitCode = 0;
			::GetExitCodeThread(item.callThread->m_hThread, &dwExitCode);

			if(dwExitCode != STILL_ACTIVE)
			{
				// inactive thread

				// delete thread & remove from list
				delete item.callThread;
				CCallSocket::m_listTimeoutCallThread.RemoveAt(old_pos);
			}
			else if(dwExitCode == STILL_ACTIVE && (item.callTime+span_tm) < cur_tm )
			{
				// active & timeout

				// terminate thread & delete thread & remove from list
				::TerminateThread(item.callThread->m_hThread, 1);
				delete item.callThread;
				CCallSocket::m_listTimeoutCallThread.RemoveAt(old_pos);
			}
			else
			{
				// active but not timeout -> do nothing
			}
		}
		catch(...)
		{
		}
	}

	CCallSocket::m_csTimeoutCallThread.Unlock();

	ciDEBUG(10, ("Remain TimeoutCallThread List Size=%d", CCallSocket::m_listTimeoutCallThread.GetCount()) );
}

void CSocketCallGatewayView::CheckListCtrlLimit()
{
	int count = m_lcLog.GetItemCount();
	if(count < LOG_ITEM_COUNT_LIMIT) return;

	for(int i=LOG_ITEM_CLEAN_COUNT-1; i>=0; i--)
	{
		CCallSocket* sock = (CCallSocket*)m_lcLog.GetItemData(i);
		if(sock)
		{
			m_lcLog.SetItemData(i, NULL);
			delete sock;
		}

		m_lcLog.DeleteItem(i);
	}
}
