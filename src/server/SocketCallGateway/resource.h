//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SocketCallGateway.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_SOCKETCALLGATEWAY_FORM      101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_SocketCallGatewTYPE         129
#define IDC_LIST_LOG                    1000
#define IDC_LIST_SYSTEM_LOG             1001

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
