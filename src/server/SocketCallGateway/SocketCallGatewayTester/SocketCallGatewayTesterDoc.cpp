// SocketCallGatewayTesterDoc.cpp : implementation of the CSocketCallGatewayTesterDoc class
//

#include "stdafx.h"
#include "SocketCallGatewayTester.h"

#include "SocketCallGatewayTesterDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSocketCallGatewayTesterDoc

IMPLEMENT_DYNCREATE(CSocketCallGatewayTesterDoc, CDocument)

BEGIN_MESSAGE_MAP(CSocketCallGatewayTesterDoc, CDocument)
END_MESSAGE_MAP()


// CSocketCallGatewayTesterDoc construction/destruction

CSocketCallGatewayTesterDoc::CSocketCallGatewayTesterDoc()
{
	// TODO: add one-time construction code here

}

CSocketCallGatewayTesterDoc::~CSocketCallGatewayTesterDoc()
{
}

BOOL CSocketCallGatewayTesterDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CSocketCallGatewayTesterDoc serialization

void CSocketCallGatewayTesterDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CSocketCallGatewayTesterDoc diagnostics

#ifdef _DEBUG
void CSocketCallGatewayTesterDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSocketCallGatewayTesterDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CSocketCallGatewayTesterDoc commands
