#pragma once

// CSCGWSocket 명령 대상입니다.

class CCallSocketBuffer
{
public:
	CCallSocketBuffer() { m_pBuffer=NULL; m_nBufferSize=0; };
	virtual ~CCallSocketBuffer()
	{
		if(m_pBuffer) delete[]m_pBuffer;
	};

protected:
	LPBYTE		m_pBuffer;
	int			m_nBufferSize;

public:
	bool	Append(LPBYTE pAppend, int nSize)
	{
		int new_size = m_nBufferSize + nSize;
		LPBYTE new_buff = new BYTE[new_size+1];
		if(new_buff == NULL) return false;
		::ZeroMemory(new_buff, new_size+1);
		if(m_pBuffer)
		{
			memcpy(new_buff, m_pBuffer, m_nBufferSize);
		}
		memcpy(new_buff+m_nBufferSize, pAppend, nSize);
		if(m_pBuffer) delete[]m_pBuffer;
		m_pBuffer = new_buff;
		m_nBufferSize = new_size;
		return true;
	};

	LPBYTE	GetBuffer() { return m_pBuffer; };
	int		GetBufferSize() { return m_nBufferSize; };
};

class CSCGWSocket : public CAsyncSocket
{
public:
	CSCGWSocket();
	virtual ~CSCGWSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnClose(int nErrorCode);

	HWND	m_hParentHwnd;

	//int		m_nReceiveSize;
	//BYTE	m_buf[16384];
	CCallSocketBuffer	m_buf;

	void	SendPacket(CString& msg);
};


