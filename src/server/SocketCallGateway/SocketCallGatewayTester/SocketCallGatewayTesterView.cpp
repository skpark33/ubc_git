// SocketCallGatewayTesterView.cpp : implementation of the CSocketCallGatewayTesterView class
//

#include "stdafx.h"
#include "SocketCallGatewayTester.h"

#include "SocketCallGatewayTesterDoc.h"
#include "SocketCallGatewayTesterView.h"

#include "SCGWSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSocketCallGatewayTesterView

IMPLEMENT_DYNCREATE(CSocketCallGatewayTesterView, CFormView)

BEGIN_MESSAGE_MAP(CSocketCallGatewayTesterView, CFormView)
	ON_BN_CLICKED(IDC_BUTTON_RUN_TEST, &CSocketCallGatewayTesterView::OnBnClickedButtonRunTest)
	ON_MESSAGE(WM_CONNECT_SOCKET, OnConnectSocket)
	ON_MESSAGE(WM_CLOSE_SOCKET, OnCloseSocket)
	ON_MESSAGE(WM_ADD_TEST_CALL, OnAddTestCall)
END_MESSAGE_MAP()

// CSocketCallGatewayTesterView construction/destruction

CSocketCallGatewayTesterView::CSocketCallGatewayTesterView()
	: CFormView(CSocketCallGatewayTesterView::IDD)
{
	// TODO: add construction code here

}

CSocketCallGatewayTesterView::~CSocketCallGatewayTesterView()
{
}

void CSocketCallGatewayTesterView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_lcLog);
	DDX_Control(pDX, IDC_EDIT_TEST_COUNT, m_editTestCount);
	DDX_Control(pDX, IDC_EDIT_SERVER_PORT, m_editServerPort);
}

BOOL CSocketCallGatewayTesterView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CSocketCallGatewayTesterView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	m_lcLog.InsertColumn(0, "Count", 0, 80);
	m_lcLog.InsertColumn(1, "Call String", 0, 300);
	m_lcLog.InsertColumn(2, "Reply String", 0, 300);

	m_editServerPort.SetWindowText("14001");
}


// CSocketCallGatewayTesterView diagnostics

#ifdef _DEBUG
void CSocketCallGatewayTesterView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSocketCallGatewayTesterView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CSocketCallGatewayTesterDoc* CSocketCallGatewayTesterView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSocketCallGatewayTesterDoc)));
	return (CSocketCallGatewayTesterDoc*)m_pDocument;
}
#endif //_DEBUG


// CSocketCallGatewayTesterView message handlers

void CSocketCallGatewayTesterView::OnBnClickedButtonRunTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString str_test_count;
	m_editTestCount.GetWindowText(str_test_count);

	CString str_server_port;
	m_editServerPort.GetWindowText(str_server_port);

	TEST_THREAD_PARAM* param = new TEST_THREAD_PARAM;
	param->hParentHwnd	= GetSafeHwnd();
	param->nCount		= atoi(str_test_count);
	param->nServerPort	= atoi(str_server_port);

	CWinThread* pCallThread = ::AfxBeginThread(TestThreadFunc, (LPVOID)param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
	pCallThread->m_bAutoDelete = TRUE;
	pCallThread->ResumeThread();
}

LRESULT CSocketCallGatewayTesterView::OnConnectSocket(WPARAM wParam, LPARAM lParam)
{
	CSCGWSocket* sock = (CSCGWSocket*)wParam;

	LVFINDINFO fi;
	fi.flags = LVFI_PARAM;
	fi.lParam = (DWORD_PTR)sock;

	int idx = m_lcLog.FindItem(&fi);
	if(idx < 0) return 0;

	CString msg = m_lcLog.GetItemText(idx, 1);
	sock->SendPacket(msg);

	return 1;
}

LRESULT CSocketCallGatewayTesterView::OnCloseSocket(WPARAM wParam, LPARAM lParam)
{
	CSCGWSocket* sock = (CSCGWSocket*)wParam;

	LVFINDINFO fi;
	fi.flags = LVFI_PARAM;
	fi.lParam = (DWORD_PTR)sock;

	int idx = m_lcLog.FindItem(&fi);
	if(idx < 0) return 0;

	int buf_size = sock->m_buf.GetBufferSize();
	LPBYTE p_buf = sock->m_buf.GetBuffer();

	if(p_buf && buf_size>0)
	{
		BYTE header[5] = {0};
		memcpy(header, p_buf, 4);

		UINT packet_size;
		memcpy(&packet_size, p_buf+4, 4);

	//	LPBYTE body = new BYTE[buf_size-8 + 1];
	//	::ZeroMemory(body, buf_size-8 + 1);
	//	memcpy(body, p_buf+8, buf_size-8);

		CString msg;
		msg.Format("%s,%d,%s", header, packet_size, p_buf+8);

		m_lcLog.SetItemText(idx, 2, msg);
	}

	delete sock;
//	delete[]body;

	m_lcLog.SetItemData(idx, NULL);

	return 1;
}

LRESULT CSocketCallGatewayTesterView::OnAddTestCall(WPARAM wParam, LPARAM lParam)
{
	int count = (int)wParam;
	UINT server_port = (UINT)lParam;

	CString str;
	switch(count%7)
	{
	default:
	case 0:		str.Format("get PM=*"); break;
	case 1:		str.Format("get OD=*/PMO=*"); break;
	case 2:		str.Format("get AGTServer=*"); break;
	case 3:		str.Format("get PM=*/Site=*/User=*"); break;
	case 4:		str.Format("get PM=*/Site=*"); break;
	case 5:		str.Format("get PM=*/Site=*/Announce=*"); break;
	case 6:		str.Format("get PM=*/Site=*/Host=*"); break;
	}

	char buf[32] = {0};
	sprintf(buf, "%d", count);
	m_lcLog.InsertItem(0, buf);
	m_lcLog.SetItemText(0, 1, str);

	CSCGWSocket* sock = new CSCGWSocket();
	sock->m_hParentHwnd = GetSafeHwnd();

	m_lcLog.SetItemData(0, (DWORD)sock);

	sock->Create();
	sock->Connect("127.0.0.1", server_port);

	//
	int cnt = m_lcLog.GetItemCount();
	if(cnt > 10000)
	{
		for(int i=cnt-1; i>9000; i--)
		{
			m_lcLog.DeleteItem(i);
		}
	}

	return 1;
}

UINT TestThreadFunc(LPVOID pParam)
{
	TEST_THREAD_PARAM* param = (TEST_THREAD_PARAM*)pParam;
	HWND	hParentHwnd = param->hParentHwnd;
	int		nCount = param->nCount;
	int		nServerPort = param->nServerPort;
	delete param;

	for(int i=0; i<nCount; i++)
	{
		::PostMessage(hParentHwnd, WM_ADD_TEST_CALL, i, nServerPort);
		::Sleep(200);
	}

	return 0;
}
