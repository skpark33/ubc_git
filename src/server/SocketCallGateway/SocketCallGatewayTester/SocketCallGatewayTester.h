// SocketCallGatewayTester.h : main header file for the SocketCallGatewayTester application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CSocketCallGatewayTesterApp:
// See SocketCallGatewayTester.cpp for the implementation of this class
//

class CSocketCallGatewayTesterApp : public CWinApp
{
public:
	CSocketCallGatewayTesterApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CSocketCallGatewayTesterApp theApp;