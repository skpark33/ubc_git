// SocketCallGatewayTesterDoc.h : interface of the CSocketCallGatewayTesterDoc class
//


#pragma once


class CSocketCallGatewayTesterDoc : public CDocument
{
protected: // create from serialization only
	CSocketCallGatewayTesterDoc();
	DECLARE_DYNCREATE(CSocketCallGatewayTesterDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CSocketCallGatewayTesterDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


