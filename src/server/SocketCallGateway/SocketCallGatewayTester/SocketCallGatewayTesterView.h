// SocketCallGatewayTesterView.h : interface of the CSocketCallGatewayTesterView class
//


#pragma once
#include "afxcmn.h"
#include "afxwin.h"


class CSocketCallGatewayTesterView : public CFormView
{
protected: // create from serialization only
	CSocketCallGatewayTesterView();
	DECLARE_DYNCREATE(CSocketCallGatewayTesterView)

public:
	enum{ IDD = IDD_SOCKETCALLGATEWAYTESTER_FORM };

// Attributes
public:
	CSocketCallGatewayTesterDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CSocketCallGatewayTesterView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonRunTest();
	afx_msg LRESULT OnConnectSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAddTestCall(WPARAM wParam, LPARAM lParam);

	CListCtrl m_lcLog;
	CEdit m_editTestCount;
	CEdit m_editServerPort;
};


#ifndef _DEBUG  // debug version in SocketCallGatewayTesterView.cpp
inline CSocketCallGatewayTesterDoc* CSocketCallGatewayTesterView::GetDocument() const
   { return reinterpret_cast<CSocketCallGatewayTesterDoc*>(m_pDocument); }
#endif


typedef struct
{
	HWND	hParentHwnd;
	int		nCount;
	int		nServerPort;
} TEST_THREAD_PARAM;

static UINT TestThreadFunc(LPVOID pParam);
