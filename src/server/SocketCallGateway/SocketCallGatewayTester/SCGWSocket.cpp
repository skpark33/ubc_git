// SCGWSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "SocketCallGatewayTester.h"
#include "SCGWSocket.h"


// CSCGWSocket

CSCGWSocket::CSCGWSocket()
{
//	::ZeroMemory(m_buf, 1024);
//	m_nReceiveSize = 0;
}

CSCGWSocket::~CSCGWSocket()
{
}


// CSCGWSocket 멤버 함수

void CSCGWSocket::OnConnect(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	PostMessage(m_hParentHwnd, WM_CONNECT_SOCKET, (WPARAM)this, NULL);

	CAsyncSocket::OnConnect(nErrorCode);
}

void CSCGWSocket::OnReceive(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	BYTE buf[32768];

	int nRead = Receive(buf, 16384); 

	switch (nRead)
	{
	case 0:
		//PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
		Close();
		break;

	case SOCKET_ERROR:
		if (GetLastError() != WSAEWOULDBLOCK) 
		{
			// error occurred
			//PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			Close();
		}
		break;

	default:
		//memcpy(m_buf+m_nReceiveSize, buf, nRead);
		//m_nReceiveSize += nRead;
		m_buf.Append(buf, nRead);
		break;
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

void CSCGWSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	PostMessage(m_hParentHwnd, WM_CLOSE_SOCKET, (WPARAM)this, NULL);

	CAsyncSocket::OnClose(nErrorCode);
}

void CSCGWSocket::SendPacket(CString& msg)
{
	int length = msg.GetLength();

	BYTE len[4] = {0};
	//int temp_len = length;
	//for(int i=3; i>=0; i--)
	//{
	//	len[i] = temp_len % 256;
	//	temp_len >>= 8;
	//}

	Send("SCGW", 4);
	Send(&length, 4);
	Send((LPCSTR)msg, length);
}
