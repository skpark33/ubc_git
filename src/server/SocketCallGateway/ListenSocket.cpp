// ListenSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "SocketCallGateway.h"
#include "ListenSocket.h"

#include "CallSocket.h"


ciSET_DEBUG(10, "CListenSocket");


// CListenSocket

CListenSocket::CListenSocket()
:	m_bOpen (false)
,	m_bRetryConnect (true)
{
}

CListenSocket::~CListenSocket()
{
}

// CListenSocket 멤버 함수

void CListenSocket::OnAccept(int nErrorCode)
{
	if(nErrorCode)
	{
		ciWARN( ("OnAccept ERROR !!! [ErrCode:%s]", GetErrorMessageString(nErrorCode)) );
	}
	else
	{
		ciDEBUG(10, ("OnAccept()") );
	}

	// add to listctrl
	CCallSocket* call_socket = new CCallSocket();
	if(call_socket)
	{
		// success to create call-socket obj
		if( Accept(*call_socket) )
		{
			// success to accept
			call_socket->GetPeerName(call_socket->m_strPeerName, call_socket->m_nPeerPort);
			ciDEBUG(10, ("Accept (0x%08X) %s:%u", call_socket, call_socket->m_strPeerName, call_socket->m_nPeerPort) );

			POST_CALL_LOG(	m_hParentWnd, 
							call_socket, 
							"O", 
							CURRENT_TIME_STRING, 
							"" );
		}
		else
		{
			// fail to accept
			DWORD nErrorCode = GetLastError();

			call_socket->GetPeerName(call_socket->m_strPeerName, call_socket->m_nPeerPort);
			ciWARN( ("Fail to accept !!! (0x%08X) [ErrCode:%s] %s:%u", call_socket, GetErrorMessageString(nErrorCode), call_socket->m_strPeerName, call_socket->m_nPeerPort) );

			call_socket->m_strErrMsg.Format("ERROR : Fail to accept [ErrCode:%s]", GetErrorMessageString(nErrorCode) );
			delete call_socket;
			Close();
			::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)call_socket, NULL);
		}
	}
	else
	{
		// fail to create call-socket obj
		ciERROR(("Fail to create CallSocket object !!!"));
		POST_SYSTEM_LOG(m_hParentWnd, 
						CURRENT_TIME_STRING, 
						"ERROR : Fail to create CallSocket object" );
	}

	CAsyncSocket::OnAccept(nErrorCode);
}

void CListenSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	m_bOpen = false;

	if(nErrorCode)
	{
		ciWARN( ("OnClose ERROR !!! [ErrCode:%s]", GetErrorMessageString(nErrorCode)) );
	}
	else
	{
		ciDEBUG(10, ("OnClose()") );
	}

	POST_SYSTEM_LOG(m_hParentWnd, 
					CURRENT_TIME_STRING, 
					"WARNING : Listen Socket Closed" );

	if(m_bRetryConnect)
	{
		// not StopListen -> retry StartListen
		ciWARN( ("Retry Connect Listen Socket !!!") );
		::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, NULL, NULL);
	}

	CAsyncSocket::OnClose(nErrorCode);
}

void CListenSocket::Init(HWND hParentWnd, UINT nPort)
{
	ciDEBUG(10, ("Init(Port:%u)", nPort) );

	m_hParentWnd = hParentWnd;
	m_nPort = nPort;
}

BOOL CListenSocket::StartListen()
{
	ciDEBUG(10, ("StartListen(Port:%u)", m_nPort) );

	m_bRetryConnect = true;
	if(Create(m_nPort))
	{
		// success to create
		BOOL ret_val = Listen();
		if(ret_val)
		{
			// success to listen
			m_bOpen = true;
		}
		else
		{
			// fail to listen -> retry StartListen
			ciERROR( ("Fail to listen ListenSocket !!! [ErrCode:%s]", GetErrorMessageString(GetLastError()) ) );
			Close();
			::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, NULL, NULL);
		}
		return ret_val;
	}
	else
	{
		// fail to create -> retry StartListen
		ciERROR( ("Fail to open ListenSocket !!! [ErrCode:%s]", GetErrorMessageString(GetLastError()) ) );
		Close();
		::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, NULL, NULL);
		return NULL;
	}
}

void CListenSocket::StopListen()
{
	ciDEBUG(10, ("StopListen()") );

	m_bRetryConnect = false;
	m_bOpen = false;
	Close();
}
