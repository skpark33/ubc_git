#pragma once

#include <afxmt.h>


#define		HEADER_PREDEFINED_STRING		"SCGW"
#define		HEADER_PREDEFINED_STRING_SIZE	4


//
typedef struct
{
	BYTE head[HEADER_PREDEFINED_STRING_SIZE];
	UINT length;
} PACKET_HEADER;

#define		PACKET_HEADER_SIZE				(sizeof(PACKET_HEADER))

//
typedef struct
{
	CTime			callTime;
	CWinThread*		callThread;
} TIMEOUT_CALL_THREAD_ITEM;
typedef		CList<TIMEOUT_CALL_THREAD_ITEM, TIMEOUT_CALL_THREAD_ITEM&>		TIMEOUT_CALL_THREAD_LIST;

//
class CCallSocket;
typedef struct
{
	CString			callString;
	CCallSocket*	callSocket;
	CWinThread*		callThread;
} CALL_THREAD_PARAM;


class CCallSocketBuffer
{
public:
	CCallSocketBuffer() { m_pBuffer=NULL; m_nBufferSize=0; };
	virtual ~CCallSocketBuffer()
	{
		Init();
	};

protected:
	LPBYTE		m_pBuffer;
	int			m_nBufferSize;

public:
	void	Init()
	{
		if(m_pBuffer) delete[]m_pBuffer;
		m_pBuffer=NULL;
		m_nBufferSize=0;
	};

	bool	Append(LPBYTE pAppend, int nSize)
	{
		int new_size = m_nBufferSize + nSize;
		LPBYTE new_buff = new BYTE[new_size+1];
		if(new_buff == NULL) return false;
		::ZeroMemory(new_buff, new_size+1);
		if(m_pBuffer)
		{
			memcpy(new_buff, m_pBuffer, m_nBufferSize);
		}
		memcpy(new_buff+m_nBufferSize, pAppend, nSize);
		if(m_pBuffer) delete[]m_pBuffer;
		m_pBuffer = new_buff;
		m_nBufferSize = new_size;
		return true;
	};

	LPBYTE	GetBuffer() { return m_pBuffer; };
	int		GetBufferSize() { return m_nBufferSize; };
};

// CCallSocket 명령 대상입니다.

class CCallSocket : public CAsyncSocket
{
public:
	static	int		m_nReadBufferSize;
	static	HWND	m_hParentWnd;

	static	CCriticalSection			m_csTimeoutCallThread;
	static	TIMEOUT_CALL_THREAD_LIST	m_listTimeoutCallThread;

	static	CCriticalSection	m_csRunningCallSocket;
	static	CMapPtrToPtr		m_listRunningCallSocket;

	static	CString		BufferToString(LPBYTE pBuff, int size);

	//
	CCallSocket();
	virtual ~CCallSocket();
	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

protected:
	CCallSocketBuffer	m_Buffer;
	LPBYTE			m_pReadBuffer;
	CWinThread*		m_pCallThread;
	bool			m_bDeleteThread;

	void	DeleteThread(bool bShowErrMsg=true);
	void	PacketAnalysis();

public:
	CString			m_strPeerName;
	UINT			m_nPeerPort;

	CString			m_strErrMsg;

	void	SetDeleteThread(bool bDeleteThread=true) { m_bDeleteThread=bDeleteThread; };
	BOOL	SendPacket(CString& strReply);
};

static UINT CallThreadFunc(LPVOID pParam);
