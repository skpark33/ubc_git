// SocketCallGatewayView.h : interface of the CSocketCallGatewayView class
//


#pragma once
#include "afxcmn.h"

#include "ListenSocket.h"


class CSocketCallGatewayView : public CFormView
{
protected: // create from serialization only
	CSocketCallGatewayView();
	DECLARE_DYNCREATE(CSocketCallGatewayView)

public:
	enum{ IDD = IDD_SOCKETCALLGATEWAY_FORM };

	enum {
		COL_COUNT,
		COL_CONNECT_STATUS,
		COL_CALL_TIME,
		COL_CALL_STRING,
		COL_REPLY_TIME,
		COL_REPLY_STRING,
	};

	enum {
		SYS_COL_TIME,
		SYS_COL_STRING,
	};

// Attributes
public:
	CSocketCallGatewayDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CSocketCallGatewayView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	CListenSocket	m_sockListen;

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnPostCallLog(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPostSystemLog(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResetListenSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReplyFromCallThread(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseCallSocket(WPARAM wParam, LPARAM lParam);

	CListCtrl	m_lcLog;
	CListCtrl	m_lcSystemLog;

protected:

	void	GetSocketReceiveBufferSize();
	void	CheckTimeoutCallThread();
	void	CheckListCtrlLimit();
};

#ifndef _DEBUG  // debug version in SocketCallGatewayView.cpp
inline CSocketCallGatewayDoc* CSocketCallGatewayView::GetDocument() const
   { return reinterpret_cast<CSocketCallGatewayDoc*>(m_pDocument); }
#endif

