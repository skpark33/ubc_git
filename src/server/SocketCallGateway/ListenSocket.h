#pragma once

// CListenSocket 명령 대상입니다.

class CListenSocket : public CAsyncSocket
{
public:
	CListenSocket();
	virtual ~CListenSocket();

	virtual void OnAccept(int nErrorCode);
	virtual void OnClose(int nErrorCode);

	UINT	m_nPort;
protected:
	HWND	m_hParentWnd;
	bool	m_bOpen;
	bool	m_bRetryConnect;

public:
	void	Init(HWND hParentWnd, UINT nPort);
	BOOL	StartListen();
	void	StopListen();
};
