// CallSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "SocketCallGateway.h"
#include "CallSocket.h"

#include <cci/libWrapper/cciCall.h>

ciSET_DEBUG(10, "CCallSocket");


// CCallSocket

int CCallSocket::m_nReadBufferSize = (16*1024);	// 64k
HWND CCallSocket::m_hParentWnd = NULL;

CCriticalSection			CCallSocket::m_csTimeoutCallThread;
TIMEOUT_CALL_THREAD_LIST	CCallSocket::m_listTimeoutCallThread;

CCriticalSection	CCallSocket::m_csRunningCallSocket;
CMapPtrToPtr		CCallSocket::m_listRunningCallSocket;


CCallSocket::CCallSocket()
:	m_pReadBuffer (NULL)
,	m_pCallThread (NULL)
,	m_bDeleteThread (false)
,	m_strPeerName ("")
,	m_nPeerPort (0)
{
	// insert this to running-list
	ciDEBUG(10, ("Create CallSocket object (0x%08X)", this) );
	m_csRunningCallSocket.Lock();
	m_listRunningCallSocket.SetAt(this, this);
	m_csRunningCallSocket.Unlock();

	// make receive-buffer
	m_pReadBuffer = new BYTE[m_nReadBufferSize];
	if(m_pReadBuffer == NULL)
	{
		// fail to create receive-buffer -> not enough memory
		ciWARN( ("Fail to create ReadBuffer !!! (0x%08X)", this) );
	}

	// add to listctrl
	POST_CALL_LOG(	m_hParentWnd, 
					this, 
					"X", 
					CURRENT_TIME_STRING, 
					"" );
}

CCallSocket::~CCallSocket()
{
	// delete this from running-list
	ciDEBUG(10, ("Delete CallSocket object (0x%08X)", this) );
	m_csRunningCallSocket.Lock();
	if( !m_listRunningCallSocket.RemoveKey(this) )
	{
		// not exist this in running-list -> something wrong
		ciWARN( ("Delete Invalid CallSocket object !!! (0x%08X)", this) );
	}
	m_csRunningCallSocket.Unlock();

	// delete receive-buffer
	if(m_pReadBuffer)
	{
		delete[]m_pReadBuffer;
		m_pReadBuffer = NULL;
	}
	else
	{
		// receive-buffer is null -> not enough memory
		ciWARN( ("ReadBuffer is NULL !!! (0x%08X)", this) );
	}

	// delete call-thread
	DeleteThread();
}

// CCallSocket 멤버 함수

void CCallSocket::OnConnect(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(nErrorCode)
	{
		ciWARN( ("OnConnect ERROR !!! (0x%08X) [ErrCode:%s] %s:%d", this, GetErrorMessageString(nErrorCode), m_strPeerName, m_nPeerPort) );
	}
	else
	{
		ciDEBUG(10, ("OnConnect (0x%08X) %s:%d", this, m_strPeerName, m_nPeerPort) );
	}

	CAsyncSocket::OnConnect(nErrorCode);
}

void CCallSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(nErrorCode)
	{
		ciWARN( ("OnClose ERROR !!! (0x%08X) [ErrCode:%s] %s:%d", this, GetErrorMessageString(nErrorCode), m_strPeerName, m_nPeerPort) );
	}
	else
	{
		ciDEBUG(10, ("OnClose (0x%08X) %s:%d", this, m_strPeerName, m_nPeerPort) );
	}

	::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);

	CAsyncSocket::OnClose(nErrorCode);
}

void CCallSocket::OnReceive(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(nErrorCode)
	{
		ciWARN( ("OnReceive ERROR !!! (0x%08X) [ErrCode:%s] %s:%d", this, GetErrorMessageString(nErrorCode), m_strPeerName, m_nPeerPort) );
	}
	else
	{
		//ciDEBUG(10, ("OnReceive (0x%08X) %s:%d", this, m_strPeerName, m_nPeerPort) );
	}

	if(m_pReadBuffer)
	{
		int nRead = Receive(m_pReadBuffer, m_nReadBufferSize); 
		ciDEBUG(10, ("Receive (0x%08X) ReadSize=%d", this, nRead) );

		switch (nRead)
		{
		case 0:
			// read nothing -> socket error
			ciWARN( ("Fail to receive !!! (0x%08X) Read Nothing", this) );
			m_strErrMsg = "WARNING : Fail to receive (Read Nothing)";
			Close();
			::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			break;

		case SOCKET_ERROR:
			if (GetLastError() != WSAEWOULDBLOCK) 
			{
				// socket error occurred
				ciWARN( ("Fail to receive !!! (0x%08X) SOCKET_ERROR", this) );
				m_strErrMsg = "WARNING : Fail to receive (SOCKET_ERROR)";
				Close();
				::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			}
			break;

		default:
			if( m_Buffer.Append(m_pReadBuffer, nRead) )
			{
				PacketAnalysis();
			}
			else
			{
				// fail to append received-buffer -> not enough memory
				ciWARN( ("Fail to append Buffer !!! (0x%08X)", this) );
				m_strErrMsg = "WARNING : Fail to append Buffer";
				Close();
				::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			}
			break;
		}
	}
	else
	{
		// receive-buffer is null -> not enough memory
		ciWARN( ("ReadBuffer is NULL !!! (0x%08X)", this) );
		m_strErrMsg = "WARNING : ReadBuffer is NULL";
		Close();
		::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

void CCallSocket::DeleteThread(bool bShowErrMsg)
{
	if(m_pCallThread)
	{
		try
		{
			DWORD dwExitCode = 0;
			::GetExitCodeThread(m_pCallThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				// still alive thread
				DWORD timeout = WAIT_TIMEOUT;

				if(m_bDeleteThread)
				{
					// wait - already checked end-flag
					timeout = ::WaitForSingleObject(m_pCallThread->m_hThread, 1000); // wait 1sec
				}

				if(timeout == WAIT_TIMEOUT)
				{
					// still alive -> insert to timeout-list
					if(bShowErrMsg)
					{
						ciDEBUG(10, ("Still alive CallThread (0x%08X->0x%08X)", this, m_pCallThread) );
					}

					m_csTimeoutCallThread.Lock();

					// add to timeout-callthread list
					TIMEOUT_CALL_THREAD_ITEM tcti;
					tcti.callTime = CTime::GetCurrentTime();
					tcti.callThread = m_pCallThread;
					m_listTimeoutCallThread.AddTail(tcti);
					int count = m_listTimeoutCallThread.GetCount();

					m_csTimeoutCallThread.Unlock();

					if(bShowErrMsg)
					{
						ciDEBUG(10, ("TimeoutCallThread Size=%d", count) );
					}
				}
				else
				{
					// inactive thread -> delete thread
					if(bShowErrMsg)
					{
						ciDEBUG(10, ("Delete CallThread object (0x%08X->0x%08X)", this, m_pCallThread) );
					}
					delete m_pCallThread;
				}
			}
			else
			{
				// inactive thread -> delete thread
				if(bShowErrMsg)
				{
					ciDEBUG(10, ("Delete CallThread object (0x%08X->0x%08X)", this, m_pCallThread) );
				}
				delete m_pCallThread;
			}
		}
		catch(...)
		{
			// error occured during delete thread
			if(bShowErrMsg)
			{
				ciWARN( (" Fail to delete CallThread object by Exception!!! (0x%08X->0x%08X)", this, m_pCallThread) );
			}
		}

		m_pCallThread = NULL;
	}
	else
	{
		// no thread objet
		if(bShowErrMsg)
		{
			ciWARN( ("CallThread object is NULL !!! (0x%08X)", this) );
		}
	}
}

void CCallSocket::PacketAnalysis()
{
	// get buffer info.
	LPBYTE p_buf = m_Buffer.GetBuffer();
	int buf_size = m_Buffer.GetBufferSize();
	if(buf_size < PACKET_HEADER_SIZE || p_buf == NULL) return;

	// get header
	PACKET_HEADER header;
	LPBYTE p_header = (LPBYTE)&header;
	memcpy(p_header, p_buf, PACKET_HEADER_SIZE);

	// check HEADER ('SCGW')
	if(memcmp(header.head, HEADER_PREDEFINED_STRING, HEADER_PREDEFINED_STRING_SIZE) != 0)
	{
		// invalid packet
		ciWARN( ("Invalid Packet Header !!! (0x%08X) Packet=%s", this, BufferToString(p_buf,buf_size)) );
		m_strErrMsg = "WARNING : Invalid Packet Header";
		Close();
		::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
		return;
	}

	// get packet(body) size
	UINT packet_length = header.length;

	if(buf_size-PACKET_HEADER_SIZE < packet_length)
	{
		// not completely receive
		//ciDEBUG(10, ("NotCompleteReceive (0x%08X) PacketLength=%d,PacketSize=%d,Packet=%s", this, packet_length, buf_size-PACKET_HEADER_SIZE, BufferToString(p_buf,buf_size)) );
		return;
	}
	else if(buf_size-PACKET_HEADER_SIZE > packet_length)
	{
		// invalid packet -> over valid packet size
		ciWARN( ("Invalid Packet Length !!! (0x%08X) Packet=%s", this, BufferToString(p_buf,buf_size)) );
		m_strErrMsg = "WARNING : Invalid Packet Length";
		Close();
		::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
		return;
	}
	else
	{
		// valid packet
		ciDEBUG(10, ("Valid Packet (0x%08X) Packet=%s", this, BufferToString(p_buf,buf_size)) );

		//
		POST_CALL_LOG(	m_hParentWnd, 
						this, 
						"O", 
						CURRENT_TIME_STRING, 
						(char*)(p_buf+PACKET_HEADER_SIZE) );

		// create & run thread
		CALL_THREAD_PARAM* param = new CALL_THREAD_PARAM;
		if(param)
		{
			param->callString = (char*)(p_buf+PACKET_HEADER_SIZE);
			param->callSocket = this;
			m_pCallThread = ::AfxBeginThread(CallThreadFunc, (LPVOID)param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
			param->callThread = m_pCallThread;
			if(m_pCallThread)
			{
				// success to create call-thread
				ciDEBUG(10, ("Create CallThread object (0x%08X->0x%08X)", this, m_pCallThread) );
				m_pCallThread->m_bAutoDelete = FALSE;
				m_pCallThread->ResumeThread();
			}
			else
			{
				// fail to create call-thread -> not enough memory or resources
				ciWARN( ("Fail to create CallThread object !!! (0x%08X)", this) );
				m_strErrMsg = "WARNING : Fail to create CallThread object";
				Close();
				::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			}
		}
		else
		{
			// fail to thread-parameter -> not enough memory
			ciWARN( ("Fail to create CallThread-Param !!! (0x%08X)", this) );
			m_strErrMsg = "WARNING : Fail to create CallThread-Param";
			Close();
			::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
		}
	}
}

BOOL CCallSocket::SendPacket(CString& strReply)
{
	ciDEBUG(10, ("SendPacket (0x%08X) ReplyString=%s", this, strReply) );

	CString error_null_str = "";

	// check ERROR-msg (ex. ERROR : Fail to etc.......) -> if ERROR, send Empty-String("")
	bool is_error = (::strnicmp((LPCSTR)strReply, "ERROR", 5) == 0) ? true : false;
	CString& str_reply = (is_error ? error_null_str : strReply);

	BOOL ret_val = TRUE;

	UINT length = str_reply.GetLength();
	LPBYTE send_buf = new BYTE[length + PACKET_HEADER_SIZE];

	if(send_buf)
	{
		// make header
		PACKET_HEADER header;
		memcpy(header.head, HEADER_PREDEFINED_STRING, HEADER_PREDEFINED_STRING_SIZE);
		header.length = length;

		// make packet
		memcpy(send_buf, &header, PACKET_HEADER_SIZE); // header
		memcpy(send_buf+PACKET_HEADER_SIZE, (LPCSTR)str_reply, length); // body
		int total_length = length + PACKET_HEADER_SIZE;

		// send to call-socket
		int total_send_size = 0;
		while(total_send_size < total_length) // send until total_length
		{
			int send_size = Send(send_buf+total_send_size, total_length-total_send_size);
			if(send_size == SOCKET_ERROR)
			{
				// fail to send to call-socket
				ciWARN( ("ReplyPacket Send Error !!! (0x%08X) [ErrCode:%s] ReplyPacket=%s", this, ::GetErrorMessageString(GetLastError()), BufferToString(send_buf,total_length)) );
				CString err_str;
				err_str.Format("WARNING : Fail to send ReplyPacket (ErrCode:%s) ReplyPacket=%s", ::GetErrorMessageString(GetLastError()), BufferToString(send_buf,total_length));
				strReply = err_str;
				ret_val = FALSE;
				break;
			}
			total_send_size += send_size;
		}

		delete[]send_buf;
	}
	else
	{
		// fail to create send_buffer -> not enough memory
		ciWARN( ("Fail to create Send-Buffer !!! (0x%08X) ReplyString=%s", this, strReply) );
		CString err_str;
		err_str.Format("WARNING : Fail to create Send-Buffer () ReplyString=%s", strReply);
		strReply = err_str;
		ret_val = FALSE;
	}

	return ret_val;
}

CString CCallSocket::BufferToString(LPBYTE pBuff, int size)
{
	CString ret_val;
	for(int i=0; i<size; i++)
	{
		if(pBuff[i] < 32 || pBuff[i] > 126)
		{
			// not ascii -> (0x00)
			char buf[12] = {0};
			sprintf(buf, "(0x%02X)", pBuff[i]);
			ret_val.Append(buf);
		}
		else
		{
			// ascii
			ret_val.AppendChar(pBuff[i]);
		}
	}
	return ret_val;
}

UINT CallThreadFunc(LPVOID pParam)
{
	CALL_THREAD_PARAM* param = (CALL_THREAD_PARAM*)pParam;
	CString call_string = param->callString;
	CCallSocket* call_socket = param->callSocket;
	CWinThread* call_thread = param->callThread;
	delete param;

	CString* reply_string = new CString();

	if(reply_string)
	{
/*
		// debug - make dummy packet
		srand(::GetTickCount());
		int tm = rand() % 10000;
		::Sleep(tm);

		reply_string->Format("%d,%s", tm, call_string);
*/
		cciCall aCall;
		aCall.fromString(call_string);

		try 
		{
			if(!aCall.call())
			{
				// fail to call
				ciWARN( ("Fail to call !!! (0x%08X->0x%08X) CallString=%s", call_socket, call_thread, call_string) );
				reply_string->Format("WARNING : Fail to call (%s)", aCall.toString());
			}

			while(aCall.hasMore())
			{
				cciReply reply;

				if ( !aCall.getReply(reply) )
				{
					// hasMore, but fail to getReply
					ciWARN( ("Fail to getReply !!! (0x%08X->0x%08X) CallString=%s", call_socket, call_thread, call_string) );
					reply_string->Format("WARNING : Fail to getReply (%s)", aCall.toString());
				}

				// add delimeter
				if(reply_string->GetLength() > 0)
					reply_string->Append("@#$%");

				reply_string->Append(reply.toStringWithType()); //reply.toString();
			}
		}
		catch(CORBA::Exception& ex)
		{
			// error occured
			ciWARN( ("Fail to call by CORBA::Exception !!! (0x%08X->0x%08X) CallString=%s", call_socket, call_thread, call_string) );
			reply_string->Format("WARNING : Fail to call by CORBA::Exception (%s)", aCall.toString());
		}
		catch(...)
		{
			// error occured
			ciWARN( ("Fail to call by Exception !!! (0x%08X->0x%08X) CallString=%s", call_socket, call_thread, call_string) );
			reply_string->Format("WARNING : Fail to call by Exception (%s)", aCall.toString());
		}
/**/
	}
	else
	{
		// fail to create reply_string -> not enough memory
		ciWARN( ("Fail to create Reply-String !!! (0x%08X->0x%08X) CallString=%s", call_socket, call_thread, call_string) );
	}

	// send reply_string
	::PostMessage(CCallSocket::m_hParentWnd, WM_REPLY_FROM_CALL_THREAD, (WPARAM)call_socket, (LPARAM)reply_string);

	return 0;
}
