/*! \class selfTester
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _selfTester_h_
#define _selfTester_h_


#include <ci/libTimer/ciTimer.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>


class aciWorld;

class selfTester :  public  ciPollable {
public:

	class heartbeatEventHandler : public cciEventHandler {
	public:
		virtual void 		processEvent(cciEvent& pEvent);
	};


	static selfTester*	getInstance();
	static void	clearInstance();
	virtual ~selfTester() ;
	virtual void processExpired(ciString name, int counter, int interval);

	void setWorld(aciWorld* p);
	void finish(ciBoolean fromMonitor=ciFalse) ;

	ciBoolean dbSetTest();
	ciBoolean dbGetTest();

	ciBoolean heartbeatMonitor();

	void setCleanTime(const char* cleanTime, const char* cleanWeekDay, ciShort cleanMinGap);
	ciBoolean itsCleanTime(ciTime& now);
	void setDBTestFlag(ciBoolean p) { _dbTestFlag = p; }
	void setEventTestFlag(ciBoolean p) { _eventTestFlag = p; }

	ciBoolean isEveryDayClean() { if(_cleanHour < 0 || _cleanWeekDay.size() < 7) return ciFalse; return ciTrue;}

	ciBoolean createServerLog(const char* actionKind, const char* actionDirective, const char* actionObject, const char* additionalInfo);

protected:
	selfTester();

	static selfTester*	_instance;
	static ciMutex		_instanceLock;

	aciWorld*	_world;
	ciShort		_cleanHour;
	ciShort		_cleanMin;
	ciShort		_cleanMinGap;
	ciIntSet	_cleanWeekDay;
	time_t		_processStartTime;
	ciBoolean	_dbTestFlag;
	ciBoolean	_eventTestFlag;
};	

#endif //_selfTester_h_
