 /*! \file pmWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _pmWorld_h_
#define _pmWorld_h_

#include <aci/libCall/aciCallWorld.h> 

class pmWorld : public aciCallWorld {
public:
	pmWorld();
	~pmWorld();
 	
	//[
	//	From aciCallWorld
	//
	virtual aciCall*    newCallImpl(const char* pname);
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode=0);

	//
	//]

protected:	


	
};
		
#endif //_pmWorld_h_
