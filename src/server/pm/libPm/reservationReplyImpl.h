#ifndef _reservationReplyImpl_h_
#define _reservationReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>
#include "server/pm/libPm/reservationTimer.h"

#define PROGRAM_DELI '_'

class reservationReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	reservationReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~reservationReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();
    virtual ciBoolean _create();
    virtual ciBoolean _set();
    virtual ciBoolean _remove();
    //virtual ciBoolean _removeFromTimer();

	ciBoolean	_postEvent(const char* eventType, reservEle* ele);
	ciBoolean	_getReservationId(const char* mgrId, const char* siteId, const char* hostId,
									ciStringList& outList);
};

#endif // _reservationReplyImpl_h_
