#ifndef _hostViewReplyImpl_h_
#define _hostViewReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <cci/libWrapper/cciCall.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>


class hostViewReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	hostViewReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~hostViewReplyImpl() ;

protected:
	virtual ciBoolean _execute();
	virtual ciBoolean _initObject();

};

#endif // _hostViewReplyImpl_h_
