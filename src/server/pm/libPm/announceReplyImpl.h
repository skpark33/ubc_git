#ifndef _announceReplyImpl_h_
#define _announceReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>


class announceReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	announceReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~announceReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

	ciBoolean	_set();
	ciBoolean	_create();
	ciBoolean	_remove();

	ciBoolean	_postEvent(const char* eventType);

};

#endif // _announceReplyImpl_h_
