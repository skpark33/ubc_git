#include "announceReplyImpl.h"
#include "announceTimer.h"
#include <cci/libWrapper/cciEvent.h>
#include <cci/libValue/cciEnum.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libBase/ciStringUtil.h>
#include <common/libCommon/ubcUtil.h>
#include <common/libScratch/scratchUtil.h>
#ifdef _COP_MSC_
#else
#include <ci/libFile/ciFileUtil.h>
#endif

#include <cci/libWrapper/cciEventQManager.h>

ciSET_DEBUG(10, "announceReplyImpl");


announceReplyImpl::~announceReplyImpl() 
{
    ciDEBUG(7, ("~announceReplyImpl()"));
}

ciBoolean
announceReplyImpl::_initObject()
{
    ciDEBUG(7, ("~announceReplyImpl()"));
 
	_object->addItemAsKey("mgrId", new cciString());
	_object->addItemAsKey("siteId", new cciString());
	_object->addItemAsKey("announceId",new cciString());

	_object->addItem("serialNo", new cciInteger32());
	_object->addItem("title", new cciString());
	_object->addItem("creator", new cciString());
	_object->addItem("createTime", new cciTime());
	_object->addItem("hostIdList", new cciStringList());
	_object->addItem("startTime", new cciTime(ciFalse));
	_object->addItem("endTime", new cciTime(ciFalse));
	_object->addItem("position", new cciEnum());		// USTB
	_object->addItem("height", new cciInteger16());

	_object->addItem("comment1", new cciString());
	_object->addItem("comment2", new cciString());
	_object->addItem("comment3", new cciString());
	_object->addItem("comment4", new cciString());
	_object->addItem("comment5", new cciString());
	_object->addItem("comment6", new cciString());
	_object->addItem("comment7", new cciString());
	_object->addItem("comment8", new cciString());
	_object->addItem("comment9", new cciString());
	_object->addItem("comment10", new cciString());

	_object->addItem("font", new cciString());
	_object->addItem("fontSize", new cciInteger16());
	_object->addItem("bgColor", new cciString());
	_object->addItem("fgColor", new cciString());
	_object->addItem("playSpeed", new cciInteger16());
	_object->addItem("bgLocation", new cciString());
	_object->addItem("bgFile", new cciString());
	_object->addItem("alpha", new cciInteger16());

	_object->addItem("hostNameList", new cciStringList());
	_object->addItem("contentsType", new cciInteger16());
	_object->addItem("lrMargin", new cciInteger16());
	_object->addItem("udMargin", new cciInteger16());
	_object->addItem("sourceSize", new cciInteger16());
	_object->addItem("soundVolume", new cciInteger16());
	_object->addItem("fileSize", new cciUnsigned64());
	_object->addItem("fileMD5", new cciString());
	_object->addItem("width", new cciInteger16());
	_object->addItem("posX", new cciInteger16());
	_object->addItem("posY", new cciInteger16());
	_object->addItem("heightRatio", new cciFloat());


    _fromEntity(0,"mgrId");
    _fromEntity(1,"siteId");
    _fromEntity(2,"announceId");

    return ciTrue;
}

ciBoolean
announceReplyImpl::_execute()
{
	if(!_isMyCall("mgrId","siteId","UBC_Site")){
		ciWARN(("It's not my call"));
		return ciTrue;
	}

	return ciFalse;
}

ciBoolean
announceReplyImpl::_set()
{
	ciDEBUG(5,("_set()"));
	if(!inherited::_set()){
		return ciFalse;
	}

	ciBoolean needToAdd = ciFalse;
	ciBoolean needToTimeCheck = ciFalse;
	ciString announceId = _entity.getClassValue(2);
	annEle* ele = announceTimer::getInstance()->find(announceId.c_str());
	if(!ele) {
		ele = new annEle();
		ele->mgrId = _entity.getClassValue(0);
		ele->siteId = _entity.getClassValue(1);
		ele->announceId = announceId;
		needToAdd = ciTrue;
	}else{
		// 아직 살아있는 공지의 경우, 만약 시간이 현재 시간 사이에 있지 안도록 수정되었다면
		// 공지를 해지해야 한다.
		needToTimeCheck = ciTrue;
	}
	ciShort contentsType=2;
	if(_reqList.getItem("contentsType", contentsType)){
		ele->contentsType = contentsType;
	}
	ciTime startTime;
	if(_reqList.getItem("startTime", startTime)){
		ele->startTime = startTime;
	}
	ciTime endTime;
	if(_reqList.getItem("endTime", endTime)){
		ele->endTime = endTime;
	}
	CCI::CCI_StringList hostIdList;
	if(_reqList.getItem("hostIdList", hostIdList)){
		ele->hostIdList = hostIdList;
	}

	ciTime now;
	/*
	if(ubcUtil::getInstance()->isGlobal()){
		if(needToAdd){
			announceTimer::getInstance()->add(ele);
			needToAdd = ciFalse;
		}
		now = now + ubcUtil::getInstance()->getGMTGap(ele->gmt);
		ciDEBUG(1, ("gmt now= %s", now.getTimeString().c_str()));
				ciDEBUG(1, ("gmt now= %s, startTime=%s, endTime=%s", 
					now.getTimeString().c_str(), 
					ele->startTime.getTimeString().c_str(),
					ele->endTime.getTimeString().c_str()));
	}
	*/

	if(startTime < now && now < endTime){
		ele->hasEnd = ciFalse;  
		if(needToAdd){
			announceTimer::getInstance()->add(ele);
		}
		announceTimer::getInstance()->postAnnounceExpired(ciTrue,announceId.c_str());
	}else{
		if(needToTimeCheck){
			announceTimer::getInstance()->postAnnounceExpired(ciFalse,announceId.c_str());
			if(now < endTime){
				ele->hasStart = ciFalse; // 아직 끝나지 않도록 되어 있다면, 시작시간이 미래로 가버린 것이다.
				ele->hasEnd = ciFalse; // 아직 끝나지 않도록 되어 있다면, 시작시간이 미래로 가버린 것이다.
			}
		}
		if(needToAdd){
			if(now < endTime){
				announceTimer::getInstance()->add(ele);
			}else{
				delete ele;
			}
		}
	}

	announceTimer::getInstance()->postAnnounceExpired(ciTrue,announceId.c_str());

	_postEvent("announceChanged");
	return ciTrue;
}

ciBoolean
announceReplyImpl::_remove()
{
	ciDEBUG(5,("_remove()"));

	if(!inherited::_moveToTrashCan("announceId","title")) {
		ciERROR(("_moveToTrashCan failed"));
		return ciFalse;
	}
	if(!inherited::_remove()){
		return ciFalse;
	}
	ciString announceId = _entity.getClassValue(2);
	announceTimer::getInstance()->erase(announceId.c_str());
	announceTimer::getInstance()->eraseRemoved(announceId.c_str());

	_postEvent("announceRemoved");

	const char* configRoot = getenv("CONFIGROOT");

	if(!configRoot){
		ciERROR(("ConfigRoot is not set"));
		return ciTrue;
	}
	ciString filename = configRoot;
	filename += "\\UBCAnnounce_";
	filename += announceId;
	filename += ".ini";

#ifdef _COP_MSC_
	if(::DeleteFile(filename.c_str())) {
#else
	if(ciFileUtil::removeFile(filename.c_str())) {
#endif
		ciDEBUG(1,("%s file deleted", filename.c_str()));
	}else{
		ciWARN(("%s file delete failed", filename.c_str()));
	}

	const char* project_home = getenv("PROJECT_HOME");

	if(!project_home){
		ciERROR(("project_home is not set"));
		return ciTrue;
	}
	ciString bgFolder = project_home;
	bgFolder += "\\Contents\\announce\\";
	bgFolder += announceId;

	if(scratchUtil::getInstance()->removeDirectory(bgFolder.c_str())) {
		ciDEBUG(1,("%s folder deleted", bgFolder.c_str()));
	}else{
		ciWARN(("%s folder delete failed", bgFolder.c_str()));
	}

	return ciTrue;
}

ciBoolean
announceReplyImpl::_create()
{
	ciDEBUG(5,("_create()"));

	char buf[256];

	ciString rId = _entity.getClassValue(2);
	if(rId == "unspecified" || rId == "*" || rId == "?" ) {
		ciDEBUG(1,("announceId unspecified,  id will be set by system"));
		//ciULong key = ubcUtil::getInstance()->genId("announceId");
		//sprintf(buf,"%010ld", key);
		scratchUtil::getInstance()->CreateGUID(buf,sizeof(buf));
		_entity.setClassValue(2,buf);	
		if(_reqList.itemExist("announceId")){
			_reqList.removeItem("announceId");
		}
		_object->setItem("announceId", buf);
	}else{
		sprintf(buf,"%s",rId.c_str());
	}

	ciString announceId = buf;

	if(!inherited::_create()){
		return ciFalse;
	}

	ciShort contentsType=2;
	_reqList.getItem("contentsType", contentsType);
	ciTime startTime;
	_reqList.getItem("startTime", startTime);
	ciTime endTime;
	_reqList.getItem("endTime", endTime);
	CCI::CCI_StringList hostIdList;
	_reqList.getItem("hostIdList", hostIdList);

	annEle* ele = new annEle();
	ele->mgrId = _entity.getClassValue(0);
	ele->siteId = _entity.getClassValue(1);
	ele->announceId = announceId;
	ele->hostIdList = hostIdList;

	ele->contentsType = contentsType;
	ele->startTime = startTime;
	ele->endTime = endTime;

	announceTimer::getInstance()->add(ele);
	ciTime now;
	/*
	if(ubcUtil::getInstance()->isGlobal()){
		now = now + ubcUtil::getInstance()->getGMTGap(ele->gmt);
		ciDEBUG(1, ("gmt now= %s, startTime=%s, endTime=%s", 
			now.getTimeString().c_str(), 
			ele->startTime.getTimeString().c_str(),
			ele->endTime.getTimeString().c_str()));
	}
	*/
	//if(startTime <= now && now < endTime){
		announceTimer::getInstance()->postAnnounceExpired(ciTrue,announceId.c_str());
	//}

	_postEvent("announceCreated");
	return ciTrue;
}
ciBoolean
announceReplyImpl::_postEvent(const char* eventType)
{
	ciDEBUG(5,("_postEvent(%s)", eventType));

	ciString siteId = _entity.getClassValue(1);
	ciString announceId = _entity.getClassValue(2);

	ciDEBUG(5,("_postEvent(%s,%s,%s)", 
		eventType,siteId.c_str(), announceId.c_str()));


	cciEvent aEvent;

	aEvent.setDomain(ciArgParser::getInstance()->getServerDomain());
	aEvent.setEntity("PM=%s/Site=%s/Announce=%s", 
			ciArgParser::getInstance()->getServerName(),
			siteId.c_str(),
			announceId.c_str());
	
	cciTime now;
	aEvent.setEventTime(now);

	aEvent.setEventType(eventType);
	aEvent.addItem("announceId", announceId.c_str());

	if(strcmp(eventType,"announceChanged")==0){
		aEvent.addItem("attributes", _reqList);
	}else if(strcmp(eventType,"announceCreated")==0){
		cciAttributeList aList;
		_object->toAttributeList(aList);
		aEvent.addItem("attributes", aList);
	}

	if(cciEventQManager::getInstance()->push(aEvent)) {
		ciDEBUG(1,("%s(%s/%s) Event posted", eventType , siteId.c_str(), announceId.c_str()));
		return ciTrue;
	}
	ciERROR(("%s(%s/%s) Event posted failed", eventType, siteId.c_str(), announceId.c_str()));
	return ciFalse;
}

