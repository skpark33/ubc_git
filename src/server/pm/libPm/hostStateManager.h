#ifndef _hostStateManager_h_
#define _hostStateManager_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>
#include <ci/libTimer/ciTimer.h>

#define _DEFAULT_60_SEC_		60

class HostState {
public:
	ciULong		lastUpdateTime;
	ciBoolean	operationalState;
	ciBoolean	vncState;
	ciBoolean	hddOverload;
	ciBoolean	cpuOverload;
	ciBoolean	memOverload;

	ciShort		gmt;
	ciShort		monitorState;
	//ciBoolean	starterState;
	//ciBoolean	browserState;
	//ciBoolean	browser1State;
	//ciBoolean	firmwareViewState;
	//ciBoolean	preDownloaderState;

	ciShort		period;
	ciBoolean	isInit;
	ciString	startupTime;
	ciString	shutdownTime;
	ciFloat		disk1Avail;
	ciFloat		disk2Avail;
	time_t		bootUpTime;
	time_t		bootDownTime;


	HostState() : lastUpdateTime(0),operationalState(0),
		vncState(0),
		hddOverload(0),
		cpuOverload(0),
		memOverload(0),
		gmt(9999),
		monitorState(0),
		//starterState(0),
		//browserState(0),
		//browser1State(0),
		//firmwareViewState(0),
		//preDownloaderState(0),
		bootUpTime(0), bootDownTime(0),
		disk1Avail(-2.0), disk2Avail(-2.0),
		period(_DEFAULT_60_SEC_),isInit(ciTrue) {} //
};

typedef map<string,HostState*> OPMAP;

class hostStateManager : public  ciPollable {
public:
	static hostStateManager*	getInstance();
	static void	clearInstance();

	virtual ~hostStateManager() ;
/*
	void push(const char* entity, ciBoolean state);
	void erase(const char* entity);
	ciBoolean getState(const char* entity,ciBoolean& outVal);
*/
	int		load();
	void	clear();
	virtual	void processExpired(ciString name, int counter, int interval);
	ciBoolean	setOperationalState(const char* entity, ciBoolean newState);
	ciBoolean	postEvent(const char* eventType, cciEntity& entity, int newState,
							const char* startupTime=0, const char* shutdownTime=0, ciFloat diskAvail=-1.0);
	ciBoolean	autoStartUp(const char* entity, const char* startupTime,time_t now,int period);
	ciBoolean	hddOverload(cciStringList& fileSystem, ciShort hddThreshold, ciBoolean& isOver,
							ciFloat& disk1Avail, ciFloat& disk2Avail);

	ciBoolean	_gethddInfo(const char* fileSystem, ciString& driveName, ciShort& diskUsed, ciFloat& diskAvail);
	ciBoolean	_isOverloadTimeException(cciTime& now);


	void set(const char* entity, ciShort period);
	void set(const char* entity, time_t lastUpdateTime);
	void set(const char* entity, const char* startupTime, const char* shutdownTime);
	ciBoolean set(const char* entity, ciBoolean operationalStateChanged);
	ciBoolean set(const char* entity, ciBoolean vncState, ciBoolean& operationalState);
	ciBoolean set(const char* entity, cciStringList& fileSystem, ciShort hddThreshold,ciFloat& disk1Avail, ciFloat& disk2Avail);
	//ciBoolean setOffProcessState(const char* entity);
	ciBoolean setMem(const char* entity, ciLong realMemoryUsed, ciLong realMemoryTotal);
	ciBoolean setCPU(const char* entity, ciFloat cpuUsed);

	ciBoolean isGmtChanged(const char* entity, ciShort state);
	ciBoolean isMonitorStateChanged(const char* entity, ciShort state);
	//ciBoolean isStarterStateChanged(const char* entity, ciBoolean state);
	//ciBoolean isBrowserStateChanged(const char* entity, ciBoolean state);
	//ciBoolean isBrowser1StateChanged(const char* entity, ciBoolean state);
	//ciBoolean isFirmwareViewStateChanged(const char* entity, ciBoolean state);
	//ciBoolean isPreDownloaderStateChanged(const char* entity, ciBoolean state);
	ciBoolean isBootTimeChanged(const char* entity, ciTime& bootUpTime, ciTime& bootDownTime);

	//void printProcessState(const char* entity);
	//ciBoolean postProcessStateChangedEvent(	cciEntity& entity,
	//										ciBoolean	starterState,
	//										ciBoolean	browserState,
	//										ciBoolean	browser1State,
	//										ciBoolean	firmwareViewState,
	//										ciBoolean	preDownloaderState);

	ciBoolean postMonitorStateChangedEvent(	cciEntity& entity,ciShort	monitorState );
	ciBoolean postGmtChangedEvent(	cciEntity& entity,ciShort	gmt );

	void push(const char* entity);
	void erase(const char* entity);

protected:
	hostStateManager();

	ciMutex	_mapLock;
	OPMAP	_map;

	static hostStateManager*	_instance;
	static ciMutex			_instanceLock;

};

#endif // _utvUtil_h_
