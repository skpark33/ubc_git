#ifndef _templateCache_h_
#define _templateCache_h_

#include <server/pm/libPm/pmCache.h>

class templateData : public  pmCacheData {
public:
	ciString		mgrId;
	ciString		siteId;
	ciString		programId;
	ciString		templateId;
	ciShort			width;
	ciShort			height;
	ciString		bgColor;
	ciString		bgImage;
	ciString		description;
	ciString		shortCut;

	void printIt() {
		cout << "Template Data" << endl;
		cout << mgrId.c_str() << "," << siteId.c_str() << "," << programId.c_str() << "," ;
		cout << templateId.c_str() << "," << width << "," << height << "," << bgColor.c_str() << ",";
		cout << bgImage.c_str() << "," << description.c_str() << "," << shortCut.c_str() << endl;
	}
};


class templateCache : public virtual pmCache {
public:
	templateCache() ;
	virtual ~templateCache() ;
	virtual int init(cciEntity& pEntity);
	virtual	int	compare(pmCache* old);
	virtual ciBoolean	attrCompare(pmCacheData* newData, pmCacheData* oldData);

protected:

};

#endif // _templateCache_h_
