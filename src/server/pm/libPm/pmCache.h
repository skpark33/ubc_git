#ifndef _pmCache_h_
#define _pmCache_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libValue/cciEnum.h>
#include <cci/libObject/cciObject.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <common/libCommon/utvMacro.h>

class pmCacheData {
public:
};
typedef map<ciString,pmCacheData*>	pmCacheMap;

class pmCache {
public:
	typedef pmCache inherited;

	pmCache() ;
	virtual ~pmCache() ;

	virtual void	clear();

	virtual	int		init(cciEntity& pEntity)=0;
	virtual	int		compare(pmCache* old)=0;

	pmCacheMap	_map;
	ciMutex		_mapLock;

protected:


};




#endif // _pmCache_h_
