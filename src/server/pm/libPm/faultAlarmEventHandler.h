#ifndef _faultAlarmEventHandler_h_
#define _faultAlarmEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

#include <common/libCommon/utvUtil.h>

class userInfo {
public:
	ciString mgrId;
	ciString siteId;
	ciString userId;
	ciString phoneNo;
	ciString email;
	ciLong userType;
	ciStringList siteList;
	ciStringList hostList;
	ciStringList probableCauseList;
	ciBoolean	useEmail;
	ciBoolean	useSms;


};
typedef map<ciString, userInfo*>	userInfoMap;

class faultAlarmEventHandler : public cciEventHandler {
public:
	static faultAlarmEventHandler*	getInstance();
	static void	clearInstance();

	virtual ~faultAlarmEventHandler() ;

   	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean			addHand();
	ciBoolean			init();
	void				clearMap();

	ciBoolean			insertUserInfo(const char* key, cciAttributeList& reqList);
	ciBoolean			updateUserInfo(const char* key, cciAttributeList& reqList);
	ciBoolean			removeUserInfo(const char* key, cciAttributeList& reqList);

	ciBoolean			sendTT(const char* kind, const char* receiver,
								const char* title,  ciString& text, ciString& enterprise);
									


protected:
	faultAlarmEventHandler();

	ciBoolean _filterIn(userInfo* Info, cciEntity& source, const char* probableCause);


	long	_handId;

	static faultAlarmEventHandler*	_instance;
	static ciMutex			_instanceLock;

	userInfoMap		_userMap;
	ciMutex			_userMapLock;

};


#endif // _faultAlarmEventHandler_h_
