#ifndef _bpReplyImpl_h_
#define _bpReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>
//#include <common/libCommon/ubcIni.h>

class bpReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	bpReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~bpReplyImpl() ;

protected:
	virtual ciBoolean _execute();
	virtual ciBoolean _initObject();
	virtual ciBoolean _create();
	virtual ciBoolean _remove();
	virtual ciBoolean _restore();
	virtual ciBoolean _set();

	virtual ciBoolean _setZorder();

	void		_fillByte(int from, int count, char fillbyte, ciString& outValue);
	ciBoolean	_getMinMaxZorder(char* query, ciString& minValue, ciString& maxValue);
	ciBoolean	_setBiggerValue(ciString& orgValue, ciString& outValue);
	ciBoolean	_setSmallerValue(ciString& orgValue, ciString& outValue);
	void		_setMidValue(ciString& minValue, ciString& maxValue, ciString& outValue);

	ciBoolean _genId(int idx, const char* idName);
	ciBoolean _postEvent(const char* eventType);

	ciBoolean	_createBPLog(const char* action, const char* who, const char* bpName);
	ciBoolean	_getRegisterId(ciString& who, ciString& bpName);

};

#endif // _bpReplyImpl_h_
