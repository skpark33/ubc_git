#ifndef _userReplyImpl_h_
#define _userReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class userReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	userReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~userReplyImpl() ;

	int getChildren(cciStringList* inVal, ciStringSet& outVal);
	int getChildren(const char* pWhereClause, ciStringSet& outVal);
	ciBoolean hasValue(cciStringList* list, const char* value);

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _create();
    virtual ciBoolean _set();
    virtual ciBoolean _remove();
    virtual ciBoolean _restore();
    virtual ciBoolean _initObject();
	ciBoolean _login();

	ciBoolean _createUserLog(const char* siteId, const char* via, const char* result);
	ciBoolean _setLoginTime(const char* siteId, const char* userId);

	ciBoolean 	_getCustomer(const char* siteId, ciString& customer,ciShort& gmt, ciString& language);
	ciBoolean 	_getRootSite(const char* siteId, ciString& root);
	ciBoolean 	_isCustomerExist(const char* siteId, ciShort& gmt, ciString& language);
	ciBoolean 	_getParentSite(const char* siteId, ciString& parentSite);

	ciBoolean	_strongPasswordCheck(ciString& password);
	ciBoolean	_between(const char* src, const char start, const char end);
	ciBoolean	_notbetween(const char* src, 
							const char start1, const char end1,
							const char start2, const char end2,
							const char start3, const char end3 );

	ciBoolean _createAuthChangeLog(const char* changeType);
	ciBoolean _isSuperAlreadyExist();

};

#endif // _userReplyImpl_h_
