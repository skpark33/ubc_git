#ifndef _thReplyImpl_h_
#define _thReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>
//#include <common/libCommon/ubcIni.h>

class thReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	thReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~thReplyImpl() ;

protected:
	virtual ciBoolean _execute();
	virtual ciBoolean _initObject();
	virtual ciBoolean _create();
	virtual ciBoolean _remove();
	virtual ciBoolean _set();

	void _genId(int idx, const char* idName);
	ciBoolean _postEvent(const char* eventType);


};

#endif // _thReplyImpl_h_
