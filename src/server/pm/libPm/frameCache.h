#ifndef _frameCache_h_
#define _frameCache_h_

#include <server/pm/libPm/pmCache.h>

class frameData : public pmCacheData {
public:
	ciString		mgrId;
	ciString		siteId;
	ciString		programId;
	ciString		templateId;
	ciString		frameId;

	ciShort			grade;
	ciShort			width;
	ciShort			height;
	ciShort			x;
	ciShort			y;
	ciBoolean		isPIP;
	ciString		borderStyle;
	ciShort			borderThickness;
	ciString		borderColor;
	ciShort			cornerRadius;
	ciString		description;
	ciString		comment1;
	ciString		comment2;
	ciString		comment3;	
};


class frameCache : public virtual pmCache {
public:
	frameCache() ;
	virtual ~frameCache() ;
	virtual int init(cciEntity& pEntity);
	virtual	int	compare(pmCache* old);
	virtual ciBoolean	attrCompare(pmCacheData* newData, pmCacheData* oldData);

protected:


};

#endif // _frameCache_h_
