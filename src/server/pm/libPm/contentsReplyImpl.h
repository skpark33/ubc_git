#ifndef _contentsReplyImpl_h_
#define _contentsReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class contentsReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	class FileInfo {
	public:
		ciString filename;
		ciString asciifilename;
		ciString location;
	};
	typedef list<FileInfo>	FileInfoList;

	contentsReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~contentsReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _create();
    virtual ciBoolean _set();
    virtual ciBoolean _remove();
    virtual ciBoolean _initObject();

	ciBoolean _isUsed(const char* filename, const char* location);
	ciBoolean _deleteFile(const char* filename, const char* location);
	ciBoolean _deleteFolder(const char* location);

	int _deleteFileList(FileInfoList& fList);
	ciBoolean _getFileList(const char* mgrId,	
								const char* siteId,	
								const char* programId,
								const char* contentsId, FileInfoList& fList);

};

#endif // _contentsReplyImpl_h_
