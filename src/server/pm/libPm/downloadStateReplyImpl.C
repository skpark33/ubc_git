
#include "downloadStateReplyImpl.h"
#include <cci/libWrapper/cciEvent.h>
#include <cci/libValue/cciEnum.h>
#include <ci/libDebug/ciArgParser.h>
#include <common/libCommon/ubcUtil.h>
#include <common/libScratch/scratchUtil.h>


ciSET_DEBUG(10, "downloadStateReplyImpl");


downloadStateReplyImpl::~downloadStateReplyImpl() 
{
    ciDEBUG(7, ("~downloadStateReplyImpl()"));
}

ciBoolean
downloadStateReplyImpl::_initObject()
{
    ciDEBUG(7, ("~downloadStateReplyImpl()"));
 
	_object->addItemAsKey("mgrId", new cciString());
	_object->addItemAsKey("siteId", new cciString());
    _object->addItemAsKey("hostId",new cciString());
    _object->addItemAsKey("downloadId",new cciString());

    _object->addItem("brwId",new cciInteger16());
    _object->addItem("programId",new cciString());
	_object->addItem("progress", new cciString());
	_object->addItem("programState", new cciEnum());
	_object->addItem("programStartTime", new cciTime());
	_object->addItem("programEndTime", new cciTime());

	_object->addItem("contentsId",new cciString());
	_object->addItem("contentsName", new cciString());
	_object->addItem("contentsType", new cciEnum());
	_object->addItem("location", new cciString());
	_object->addItem("filename", new cciString());
    _object->addItem("volume", new cciUnsigned64());

	_object->addItem("startTime", new cciTime());
	_object->addItem("endTime", new cciTime());
    _object->addItem("currentVolume", new cciUnsigned64());

	_object->addItem("result", new cciBoolean());
	_object->addItem("reason", new cciEnum());
	_object->addItem("downState", new cciEnum());

    _fromEntity(0,"mgrId");
    _fromEntity(1,"siteId");
    _fromEntity(2,"hostId");
    _fromEntity(3,"downloadId");

    return ciTrue;
}

ciBoolean
downloadStateReplyImpl::_execute()
{
	if(!_isMyCall("mgrId","siteId","UBC_Site")){
		ciWARN(("It's not my call"));
		return ciTrue;
	}
	return ciFalse;
}

ciBoolean
downloadStateReplyImpl::_create()
{

	ciDEBUG(1,("_create()"));

	if(!_genId(3,"downloadId")){
		this->_setErrReply(-1,"key generation failed");
		return ciFalse;
	}
	
	return inherited::_create();
}
ciBoolean
downloadStateReplyImpl::_set()
{
	return inherited::_set();
}

ciBoolean
downloadStateReplyImpl::_genId(int idx, const char* idName)
{
	
	ciString rId = _entity.getClassValue(idx);
	if(rId == "unspecified" || rId == "*" || rId == "?" ) {
		ciDEBUG(1,("id unspecified,  id will be set by system"));
		//ciULong key = ubcUtil::getInstance()->genId(idName);
		//ciDEBUG(1,("_genId = %ld", key));
		//if(key<=0) {
		//	ciERROR(("Key gen failed"));
		//	return ciFalse;
		//}
		char buf[256];
		//sprintf(buf,"%010ld", key);
		scratchUtil::getInstance()->CreateGUID(buf,sizeof(buf));
		_entity.setClassValue(idx,buf);	
		if(_reqList.itemExist(idName)){
			_reqList.removeItem(idName);
		}
		_object->setItem(idName, buf);
	}
	return ciTrue;
}
ciBoolean
downloadStateReplyImpl::_remove()
{
	ciDEBUG(7,("_remove()"));

	if(!inherited::_moveToTrashCan("downloadId","programId")) {
		ciERROR(("_moveToTrashCan failed"));
		return ciFalse;
	}
	if(!inherited::_remove()) {
		ciERROR(("Host remove failed"));
		return ciFalse;
	}

	return ciTrue;
}
