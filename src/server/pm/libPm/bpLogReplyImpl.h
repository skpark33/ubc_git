#ifndef _bpLogReplyImpl_h_
#define _bpLogReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>
//#include <common/libCommon/ubcIni.h>

class bpLogReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	bpLogReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~bpLogReplyImpl() ;

protected:
	virtual ciBoolean _execute();
	virtual ciBoolean _remove();
	virtual ciBoolean _initObject();
};

#endif // _bpLogReplyImpl_h_
