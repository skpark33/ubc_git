
#include "userReplyImpl.h"
#include "faultAlarmEventHandler.h"
#include "maxLimit.h"
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciEnum.h>
#include "common/libCommon/ubcUtil.h"
#include <ci/libDebug/ciArgParser.h>
#include "common/libScratch/scratchUtil.h"


ciSET_DEBUG(10, "userReplyImpl");


userReplyImpl::~userReplyImpl() 
{
    ciDEBUG(7, ("~userReplyImpl()"));
}

ciBoolean
userReplyImpl::_initObject()
{
    ciDEBUG(7, ("_initObject()"));

	_object->addItemAsKey("mgrId", new cciString());
 	_object->addItemAsKey("siteId", new cciString());
 	_object->addItemAsKey("userId", new cciString());

	_object->addItem("roleId", new cciString());			// KIA
	_object->addItem("validationDate", new cciTime(ciFalse));		// KIA
	
	if(_directive == "get" || _directive == "bulkget" ){
		if(maxLimit::getInstance()->isOnewayPassword()){
			_object->addItem("password", new cciString());
		}else{
			_object->addItem("password", new cciString(), ciTrue, "dbo.password_decode(password)");
		}
		_object->addItem("mobileNo", new cciString(), ciTrue, "dbo.password_decode(mobileNo)");
		_object->addItem("phoneNo", new cciString(), ciTrue, "dbo.password_decode(phoneNo)");
	
	}else if(_directive == "set" || _directive == "bulkset"   ){
		_request.unwrap();

		ciString pwd;
		_request.getItem("password", pwd);
		if(pwd.empty() || maxLimit::getInstance()->isOnewayPassword() ){
			_object->addItem("password", new cciString());
		}else{
			ciString proc = "dbo.password_encode('" + pwd + "')";	
			_object->addItem("password", new cciString(), ciTrue, proc.c_str());
		}

		ciString mobileNo;
		_request.getItem("mobileNo", mobileNo);
		if(mobileNo.empty()){
			_object->addItem("mobileNo", new cciString());
		}else{
			ciString proc = "dbo.password_encode('" + mobileNo + "')";	
			_object->addItem("mobileNo", new cciString(), ciTrue, proc.c_str());
		}

		ciString phoneNo;
		_request.getItem("phoneNo", phoneNo);
		if(phoneNo.empty()){
			_object->addItem("phoneNo", new cciString());
		}else{
			ciString proc = "dbo.password_encode('" + phoneNo + "')";	
			_object->addItem("phoneNo", new cciString(), ciTrue, proc.c_str());
		}
	}else{
		_object->addItem("password", new cciString());
		_object->addItem("mobileNo", new cciString());
		_object->addItem("phoneNo", new cciString());
	}
	
	//_object->addItem("password", new cciString());


	_object->addItem("userName", new cciString());
	_object->addItem("sid", new cciString());
	_object->addItem("zipCode", new cciString());
	_object->addItem("addr1", new cciString());
	_object->addItem("addr2", new cciString());
	_object->addItem("email", new cciString());
	_object->addItem("registerTime", new cciTime());
	_object->addItem("userType", new cciEnum());
	_object->addItem("siteList", new cciStringList());
	_object->addItem("hostList", new cciStringList());
	_object->addItem("probableCauseList", new cciStringList());
	_object->addItem("useEmail", new cciBoolean());
	_object->addItem("useSms", new cciBoolean());
	_object->addItem("lastLoginTime", new cciTime());
	_object->addItem("lastPWChangeTime", new cciTime());
	_object->addItem("loginFailCount", new cciInteger16());
	_object->addItem("adminState", new cciBoolean());
	
 	_object->addItem("trashId_", new cciString());			
	
	_fromEntity(0,"mgrId");
    _fromEntity(1,"siteId");
    _fromEntity(2,"userId");

    return ciTrue;
}

ciBoolean
userReplyImpl::_execute()
{
	if(!_isMyCall("mgrId","siteId","UBC_Site")){
		ciWARN(("It's not my call"));
		return ciTrue;
	}
	if(_directive=="login") return _login();
	return ciFalse;
}

ciBoolean
userReplyImpl::_create()
{
	ciDEBUG(5,("_create()"));

	if(maxLimit::getInstance()->isOverUser()){
		_setErrReply(-1,"User License overflow");
		return ciTrue;
	}

	ciString password;
	_reqList.getItem("password", password);
	if(!password.empty()){
		if(maxLimit::getInstance()->isStrongPassword()){
			if(!_strongPasswordCheck(password)){
				_setErrReply(-5,"STRONG PASSWORD ERROR");
				return ciTrue;	
			}
		}
		_reqList.removeItem("password");
		ciString proc = "CALL(dbo.password_encode('" + password + "'))";	
		if(maxLimit::getInstance()->isOnewayPassword()){
			maxLimit::getInstance()->encodeSha256(password, proc);
		}
		_reqList.addItem("password", proc.c_str());
	}

	ciString mobileNo;
	_reqList.getItem("mobileNo", mobileNo);
	if(!mobileNo.empty()){
		ciString proc = "CALL(dbo.password_encode('" + mobileNo + "'))";	
		_reqList.removeItem("mobileNo");
		_reqList.addItem("mobileNo", proc.c_str());
	}

	ciString phoneNo;
	_reqList.getItem("phoneNo", phoneNo);
	if(!phoneNo.empty()){
		ciString proc = "CALL(dbo.password_encode('" + phoneNo + "'))";	
		_reqList.removeItem("phoneNo");
		_reqList.addItem("phoneNo", proc.c_str());
	}

	ciString userId = _entity.getClassValue(2);
	if(userId == "super"){
		// super id 는 전시스템을 걸쳐서 하나만 있어야 한다.
		// 따라서 이미 있는지 테스트 한다.
		if(_isSuperAlreadyExist()){
			char result[512];
			sprintf(result, "invalid userId(%s), keyword 'super' cannot be used as user id", userId.c_str());
			ciERROR((result));
			this->_setErrReply(-1,result);
			return ciTrue;
		}
	}


	if(!inherited::_create()){
		return ciFalse;
	}
	const char* key = _entity.toString();
	faultAlarmEventHandler* handler = faultAlarmEventHandler::getInstance();
	handler->insertUserInfo(key,_reqList);

	if(maxLimit::getInstance()->isStrongLog()){
		_createAuthChangeLog("C");
	}


	return ciTrue;
}

ciBoolean
userReplyImpl::_isSuperAlreadyExist()
{
	ciDEBUG(1,("isSuperAlreadyExist"));

	cciObject aObj("UBC_USER");

	aObj.addItemAsKey("userId","super");

	if(!aObj.get()){
		return ciFalse;
	}
	if(!aObj.next()){
		return ciFalse;
	}

	ciWARN(("SuperAlreadyExist"));
	return ciTrue;
}

ciBoolean	
userReplyImpl::_between(const char* src, const char start, const char end)
{
	ciDEBUG(1,("between(%s,%c,%c)", src,start,end));
	const char* ptr = src;
	while(*ptr){
		if(start <= *ptr && *ptr <= end){
			return ciTrue;
		}
		ptr++;
	}
	return ciFalse;
}
ciBoolean	
userReplyImpl::_notbetween(const char* src, 
						const char start1, const char end1,
						const char start2, const char end2,
						const char start3, const char end3 )
{
	ciDEBUG(1,("notbetween(%s,%c,%c,%c,%c,%c,%c)", src,start1,end1,start2,end2,start3,end3));
	const char* ptr = src;
	while(*ptr){
		if( !((start1 <= *ptr && *ptr <= end1) || (start2 <= *ptr && *ptr <= end2) || (start3 <= *ptr && *ptr <= end3))){
			return ciTrue;
		}
		ptr++;
	}
	return ciFalse;
}

ciBoolean
userReplyImpl::_strongPasswordCheck(ciString& password)
{
	ciDEBUG(5,("_strongPasswordCheck(%s)", password.c_str()));

	// 길이 8자 이상
	if(password.length() < 8){
		return ciFalse;
	}
	// 숫자포함
	if(!_between(password.c_str(), '0','9')){
		return ciFalse;
	}
	// 특수문자포함
	if(!_notbetween(password.c_str(),'0','9','a','z','A','Z')){
		return ciFalse;
	}
	// 소문자포함
	if(!_between(password.c_str(),'a','z')){
		return ciFalse;
	}
	// 대문자포함
	if(!_between(password.c_str(),'A','Z')){
		return ciFalse;
	}
	return ciTrue;
}

ciBoolean
userReplyImpl::_set()
{
	ciDEBUG(5,("_set()"));

	ciString password;
	if(_reqList.getItem("password",password)) {
		ciDEBUG(1,("password is changed"));
		if(maxLimit::getInstance()->isStrongPassword()){
			if(!_strongPasswordCheck(password)){
				_setErrReply(-5,"STRONG PASSWORD ERROR");
				return ciTrue;	
			}
		}

		if(maxLimit::getInstance()->isOnewayPassword()){
			_reqList.removeItem("password");
			ciString proc;
			maxLimit::getInstance()->encodeSha256(password, proc);
			_reqList.addItem("password", proc.c_str());
		}

		cciTime now;
		_reqList.addItem("lastPWChangeTime", now);
		_reqList.addItem("loginFailCount", ciShort(0));
		//_reqList.addItem("trashTime_", ciTime(0));
	}
	/*
	ciBoolean adminState;
	if(_reqList.getItem("adminState",adminState)) {
		ciDEBUG(1,("adminState is changed"));
		cciTime now;
		_reqList.addItem("lastLoginTime", now);
	}
	*/
	if(!inherited::_set()){
		return ciFalse;
	}
	
	const char* key = _entity.toString();
	faultAlarmEventHandler* handler = faultAlarmEventHandler::getInstance();
	handler->updateUserInfo(key,_reqList);

	if(maxLimit::getInstance()->isStrongLog()){
		_createAuthChangeLog("U");
	}

	return ciTrue;
}
ciBoolean
userReplyImpl::_restore()
{
	ciDEBUG(5,("_restore()"));

	if(!inherited::_restore()){
		return ciFalse;
	}

	const char* key = _entity.toString();

	if(_object->get()){
		while(_object->next()) {
			_reqList.clear();
			_object->toAttributeList(_reqList);
			_object->modifyEntity(_entity);
			faultAlarmEventHandler* handler = faultAlarmEventHandler::getInstance();
			handler->insertUserInfo(key,_reqList);
			if(maxLimit::getInstance()->isStrongLog()){
				_createAuthChangeLog("C");
			}
		}
	}
	return ciTrue;
}


ciBoolean
userReplyImpl::_remove()
{
	ciDEBUG(5,("_remove()"));
	if(!inherited::_moveToTrashCan("userId", "userName")) {
		ciERROR(("_moveToTrashCan failed"));
		return ciFalse;
	}
	if(!inherited::_remove()){
		return ciFalse;
	}

	const char* key = _entity.toString();
	faultAlarmEventHandler* handler = faultAlarmEventHandler::getInstance();
	handler->removeUserInfo(key,_reqList);

	if(maxLimit::getInstance()->isStrongLog()){
		_createAuthChangeLog("D");
	}

	return ciTrue;
}

ciBoolean
userReplyImpl::_login()
{
	ciDEBUG(1,("login()"));

	ciString lPwd;
	_reqList.getItem("password",lPwd);
	ciString via="UNKNOWN";
	_reqList.getItem("via",via);
	
	ciString mgrId = _entity.getClassValue(0);
	ciString siteId = _entity.getClassValue(1);
	ciString userId = _entity.getClassValue(2);


	cciObject aObj("UBC_User",0,ciFalse);

	//aObj.addItemAsKey("mgrId", new cciString(mgrId.c_str()));
	//aObj.addItemAsKey("siteId", new cciString(siteId.c_str()));
	aObj.addItemAsKey("userId", new cciString(userId.c_str()));

	cciString* siteIdVal = (cciString*)aObj.addItemAsKey("siteId", new cciString());
	
	cciString* password ;
	if(maxLimit::getInstance()->isOnewayPassword()){
		password = (cciString*)aObj.addItem("password", new cciString());
	}else{
		password = (cciString*)aObj.addItem("password", new cciString(), ciTrue,"dbo.password_decode(password)");
	}
	//cciString* password = (cciString*)aObj.addItem("password", new cciString());
	cciEnum* userType = (cciEnum*)aObj.addItem("userType", new cciEnum());
	cciStringList* siteList = (cciStringList*)aObj.addItem("siteList", new cciStringList());
	cciStringList* hostList = (cciStringList*)aObj.addItem("hostList", new cciStringList());
	cciTime* validationDate = (cciTime*)aObj.addItem("validationDate", new cciTime());
	cciBoolean* adminState = (cciBoolean*)aObj.addItem("adminState", new cciTime());
	cciTime* lastLoginTime = (cciTime*)aObj.addItem("lastLoginTime", new cciTime());
	cciTime* lastPWChangeTime = (cciTime*)aObj.addItem("lastPWChangeTime", new cciTime());

	if(!aObj.get()){
		ciERROR(("%s,%s login failed", siteId.c_str(), userId.c_str()));
		char result[512];
		sprintf(result, "invalid userId(%s)", userId.c_str());
		if(siteId=="*") siteId = "UNKNOWN";
		this->_createUserLog(siteId.c_str(),via.c_str(),result);
		this->_setErrReply(-1,result);
		return ciTrue;
	}
	if(!aObj.next()){
		ciERROR(("%s,%s login failed", siteId.c_str(), userId.c_str()));
		char result[512];
		sprintf(result, "invalid userId(%s)", userId.c_str());
		if(siteId=="*") siteId = "UNKNOWN";
		this->_createUserLog(siteId.c_str(),via.c_str(),result);
		this->_setErrReply(-1,result);
		return ciTrue;
	}
	if(siteIdVal->get()){
		siteId = siteIdVal->get();
	}else{
		siteId = "";
	}
	if(siteId.empty()){
		ciWARN(("%s,%s login failed", siteId.c_str(), userId.c_str()));
		char result[512];
		sprintf(result, "invalid userId(%s)", userId.c_str());
		this->_createUserLog(siteId.c_str(),via.c_str(),result);
		this->_setErrReply(-1,result);
		return ciTrue;	
	}
	
	time_t valTime = validationDate->get();
	if(valTime){
		time_t now = time(0);
		if(valTime < now){
			char result[512];
			sprintf(result, "userId(%s) expired(%s)", userId.c_str(),validationDate->toString());
			this->_createUserLog(siteId.c_str(),via.c_str(),result);
			ciWARN((result));
			this->_setErrReply(-2,result);
			return ciTrue;	
		}
	}
	/*
	if(adminState->get() == ciFalse ){
		char result[512];
		sprintf(result, "userId(%s) locked()", userId.c_str());
		this->_createUserLog(siteId.c_str(),via.c_str(),result);
		ciWARN((result));
		this->_setErrReply(-4,result);
		return ciTrue;	
	}
	*/
	if(password && password->get()){
		ciString rPwd = password->get();
		if(maxLimit::getInstance()->isSameSha256(lPwd,rPwd)){
			ciDEBUG(1,("%s,%s login succeed", siteId.c_str(), userId.c_str()));
			cciAttributeList attrList;
			attrList.addItem("validationDate", *validationDate);
			attrList.addItem("siteId", siteId.c_str());
			attrList.addItem("userType", *userType);
			if(userType->get() != 1 && siteId != "*" && !siteId.empty()){
				if(!hasValue(siteList,siteId.c_str())){
					siteList->addItem(siteId.c_str());
				}
			}

			ciStringSet siteSet;	
			if(siteList->length()){
				getChildren(siteList,siteSet);
			}
			if(siteSet.size()){
				ciString bufList = "[";
				ciStringSet::iterator itr;
				for(itr=siteSet.begin();itr!=siteSet.end();itr++) {
					if(bufList.size() > 2){
						bufList += ",";
					}
					bufList += "\"";
					bufList += itr->c_str();
					bufList += "\"";
				}
				bufList += "]";
				attrList.addItem("siteList", bufList);
			}else{
				attrList.addItem("siteList", "");
			}

			if(hostList->length()){
				attrList.addItem("hostList", hostList->toString());
			}else{
				attrList.addItem("hostList", "");
			}

			// Customer 추가 !!!
			ciString customer,language;
			ciShort gmt;
			this->_getCustomer(siteId.c_str(), customer, gmt, language);
			ciDEBUG(1,("customer for site(%s) = %s", siteId.c_str(), customer.c_str()));
			
			if(maxLimit::getInstance()->isStrongPassword()){
				ciTime now;
				now = now - (90*24*60*60);
				if(userType->get() != 1) {
					if(lastLoginTime->getYear() > 2000 && lastLoginTime->get() < now.getTime() ){
						ciERROR(("%s,%s login failed", siteId.c_str(), userId.c_str()));
						char result[512];
						sprintf(result, "userId(%s) locked because Never have been login in last 90 days", userId.c_str());
						if(siteId=="*") siteId = "UNKNOWN";
						this->_createUserLog(siteId.c_str(),via.c_str(),result);
						this->_setErrReply(-5,result);
						return ciTrue;
					}
				}
				if(lastPWChangeTime->getYear() > 2000 && lastPWChangeTime->get() < now.getTime() ){
					ciERROR(("%s,%s login failed", siteId.c_str(), userId.c_str()));
					char result[512];
					sprintf(result, "userId(%s) locked because Never have been password changed in last 90 days", userId.c_str());
					//if(siteId=="*") siteId = "UNKNOWN";
					//this->_createUserLog(siteId.c_str(),via.c_str(),result);
					//this->_setErrReply(-6,result);
					//return ciTrue;
					attrList.addItem("ERROR_CODE", ciLong(-6));
					attrList.addItem("ERROR_MSG", result);

				}
				//attrList.addItem("lastPWChangeTime", lastPWChangeTime);
			}
			
			attrList.addItem("customer", customer);
			attrList.addItem("gmt", gmt);
			attrList.addItem("language", language);

			ciString version;
			scratchUtil::getInstance()->getVersion(version, true);
			attrList.addItem("version", version.c_str());
			//attrList.addItem("version", "5.9");

			if(maxLimit::getInstance()->getApproveId() == userId){
				attrList.addItem("specialRole", "APPROVE");
				ciDEBUG(1,("User %s has APPROVE specialRule", userId.c_str()));
			}

			char result[512];
			sprintf(result, "login succeed(%s)", userId.c_str());

			//attrList.addItem("ERROR_CODE", ciLong(0));
			//attrList.addItem("ERROR_MSG", result);
			_setReply(attrList);
			//this->_setErrReply(0,result);
			this->_createUserLog(siteId.c_str(),via.c_str(),result);
			this->_setLoginTime(siteId.c_str(),userId.c_str());
			return ciTrue;
		}
	}

	ciWARN(("%s,%s login failed", siteId.c_str(), userId.c_str()));
	char result[512];
	sprintf(result, "password mismatch");
	this->_setErrReply(-3,result);
	this->_createUserLog(siteId.c_str(),via.c_str(),result);
	return ciTrue;
}

ciBoolean 
userReplyImpl::_getCustomer(const char* siteId, ciString& customer, ciShort& gmt, ciString& language)
{
	ciDEBUG(1,("_getCustomer(%s)", siteId));

	customer="UNKNOWN";
	ciString rootSite;
	if(!_getRootSite(siteId, rootSite)){
		ciERROR(("_getRootSite(%s) failed", siteId));
		return ciFalse;
	}
	
	if(!_isCustomerExist(rootSite.c_str(),gmt,language)){
		ciERROR(("%s customer does not exist(siteId=%s)", rootSite.c_str(), siteId));
		return ciFalse;
	}
	customer = rootSite;
	ciDEBUG(1,("customer %s(gmt=%d,language=%s) founded for site %s", customer.c_str(), gmt, language.c_str(), siteId));
	return ciTrue;
}

ciBoolean 
userReplyImpl::_getRootSite(const char* siteId, ciString& root)
{
	ciDEBUG(1,("_getRootSite(%s)", siteId));

	ciString parentSite;
	ciString searchSite = siteId;
	while(1) {
		if(_getParentSite(searchSite.c_str(),parentSite) == ciFalse){
			break;
		}
		searchSite = parentSite;
		parentSite = "";
	} 

	if(searchSite.empty()){
		ciWARN(("%s Top Site does not founded", siteId));
		return ciFalse;
	}
	root = searchSite;
	ciDEBUG(1,("%s customer founded for site %s", root.c_str(), siteId));
	return ciTrue;
}
ciBoolean 
userReplyImpl::_isCustomerExist(const char* siteId, ciShort& gmt, ciString& language)
{
	ciDEBUG(1,("_isCustomerExist(%s)", siteId));

	cciObject aObj("UBC_Customer");

	aObj.addItemAsKey("customerId", new cciString(siteId));
	cciInteger16* a_gmt = (cciInteger16*)aObj.addItem("gmt", new cciInteger16());
	cciString* a_language = (cciString*)aObj.addItem("language", new cciString());

	if(!aObj.get()){
		ciERROR(("get %s customer failed", siteId));
		return ciFalse;
	}
	if(!aObj.next()){
		ciERROR(("no customer(%s) founded", siteId));
		return ciFalse;
	}

	gmt = a_gmt->get();
	language = a_language->get();

	ciDEBUG(1,("%s customer founded", siteId));
	return ciTrue;	
}


ciBoolean 
userReplyImpl::_getParentSite(const char* siteId, ciString& parentSite)
{
	ciDEBUG(1,("_getParentSite(%s)", siteId));

	cciObject aObj("UBC_Site");

	aObj.addItemAsKey("siteId", new cciString(siteId));
	cciString* parentId = (cciString*)aObj.addItem("parentId", new cciString());

	if(!aObj.get()){
		ciERROR(("get site parentId failed"));
		return ciFalse;
	}
	if(!aObj.next()){
		ciERROR(("no site parentId founded"));
		return ciFalse;
	}
	
	parentSite = parentId->get();
	
	if(parentSite.empty() || parentSite == "NULL"){
		ciDEBUG(1,("end of tree(%s)", siteId));
		return ciFalse;
	}

	ciDEBUG(1,("%s parent found for site (%s)", parentSite.c_str(), siteId));
	return ciTrue;
}

ciBoolean 
userReplyImpl::_createUserLog(const char* siteId, const char* via, const char* result)
{
	ciDEBUG(1,("_createUserLog(%s,%s,%s)", siteId,via, result));
	ciTime now;

	cciCall call("create");
	cciEntity aEntity(_entity);
	aEntity.addClass("UserLog","1");
	aEntity.setClassValue(1,siteId);
	call.setEntity(aEntity);

	call.addItem("via",via);
	call.addItem("via",via);
	call.addItem("result",result);
	call.addItem("loginTime",now);
	call.wrap();

	ciString callString=call.toString();
	ciDEBUG(5,(callString.c_str()));

	ciString errString="";
	try {
		if(!call.call()){
			errString = callString + " failed";
			ciERROR((errString.c_str()));
			return 0;
		}
	}catch(CORBA::Exception& ex){
		errString = callString + " failed(";
		errString += ex._name();
		errString += ")";
		ciERROR((errString.c_str()));
		return 0;
	}

	errString = callString + " succeed";
	ciDEBUG(5,(errString.c_str()));
	return ciTrue;
}

ciBoolean 
userReplyImpl::_setLoginTime(const char* siteId, const char* userId)
{
	ciDEBUG(1,("_setloginTime(%s,%s,%s)", siteId,userId));
	ciTime now;

	cciObject aObj("UBC_USER");

	aObj.addItemAsKey("siteId",siteId);
	aObj.addItemAsKey("userId",userId);
	aObj.addItem("lastLoginTime",new cciTime());

	if(!aObj.set()){
		ciERROR(("_setloginTime(%s,%s,%s) failed", siteId,userId));
		return ciFalse;
	}
	return ciTrue;
}

ciBoolean
userReplyImpl::hasValue(cciStringList* list, const char* value)
{
	ciDEBUG(1,("hasValue(%s)",value));

	CORBA::ULong len = list->length();
	for(CORBA::ULong i = 0;i<len;i++){
		ciString buf = (*list)[i];
		if(buf == value){
			return ciTrue;
		}
	}
	return ciFalse;	
}

int
userReplyImpl::getChildren(cciStringList* inVal, ciStringSet& outVal)
{
	ciDEBUG(1,("getChidren(...)"));

	//ciString whereClause = "parentId in (";
	CORBA::ULong len = inVal->length();
	for(CORBA::ULong i = 0;i<len;i++){
		ciString parentId = (*inVal)[i];
		outVal.insert(ciStringSet::value_type(parentId));
		/*
		if(i>0){
			whereClause += ",";
		}
		whereClause += "'";
		whereClause += parentId;
		whereClause += "'";
		*/
	}
	/*
	whereClause += ")";
	if(len>0){
		getChildren(whereClause.c_str(), outVal);
	}
	*/
	return outVal.size();	
}

int
userReplyImpl::getChildren(const char* pWhereClause, ciStringSet& outVal)
{
	ciDEBUG(1,("getChidren(%s)",pWhereClause));

	ciStringSet bufSet;
	int len = ubcUtil::getInstance()->getDataSet("PM=*/Site=*", "siteId", pWhereClause, bufSet);
	if(len == 0) { 
		return 0; 
	}

	int i=0;
	ciString whereClause = "parentId in (";
	ciStringSet::iterator itr;
	for(itr=bufSet.begin();itr!=bufSet.end();itr++,i++) {
		outVal.insert(ciStringSet::value_type(itr->c_str()));
		if(i>0){
			whereClause += ",";
		}
		whereClause += "'";
		whereClause += (*itr);
		whereClause += "'";
	}
	whereClause += ")";

	if(i>0){
		len += getChildren(whereClause.c_str(), outVal);
	}
	return len;	
}


ciBoolean
userReplyImpl::_createAuthChangeLog(const char* changeType)
{
	//changeType "D" : Remove,  "C" : Create, "U" : Set

	if(_entity.length() < 3){
		ciERROR(("_createAuthChangeLog failed : Invalid Entity"));
		return ciFalse;
	}

	ciTime now;

	char logDate[11];
	memset(logDate,0x00,11);
	sprintf(logDate,"%04d/%02d/%02d", now.getYear(), now.getMonth(), now.getDay());

	char logTime[11];
	memset(logTime,0x00,11);
	sprintf(logTime,"%02d%02d%02d", now.getHour(), now.getMinute(), now.getSecond());

	ciString logId = _entity.getClassValue(2);

	cciObject aObj("UBC_AuthChangeLog");

	aObj.addItem("logDate", new cciString(logDate));
	aObj.addItem("logTime", new cciString(logTime));
	aObj.addItem("logId", new cciString(logId.c_str()));
	aObj.addItem("changeType", new cciString(changeType));

	if(changeType[0] != 'D'){
		ciString changeString = _reqList.toString();
		if(changeString.length() > 254) {
			changeString = changeString.substr(0,254);
		}
		aObj.addItem("changeString", new cciString(changeString.c_str()));
	}

	if(!aObj.create()){
		ciERROR(("create  UBC_AuthChangeLog failed"));
		return ciFalse;
	}
	ciDEBUG(1,("create  UBC_AuthChangeLog succeed"));
	return ciTrue;

}