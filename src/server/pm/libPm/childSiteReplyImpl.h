#ifndef _childSiteReplyImpl_h_
#define _childSiteReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class childSiteReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	childSiteReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) { _myCall = ciFalse;}
	virtual ~childSiteReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

	ciBoolean _myCall;

};

#endif // _childSiteReplyImpl_h_
