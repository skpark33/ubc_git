#ifndef _trashCanReplyImpl_h_
#define _trashCanReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class trashCanReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	class FileInfo {
	public:
		ciString filename;
		ciString location;
	};
	typedef list<FileInfo>	FileInfoList;

	trashCanReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~trashCanReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

    virtual ciBoolean _create();
    virtual ciBoolean _restore();
    virtual ciBoolean _remove();

	ciBoolean _genId(int idx, const char* idName);

	ciBoolean _createObject(cciEntity* pEntity,cciAttributeList& attrList);
	int _restoreChildren(cciEntity* pEntity, const char* className);
	ciBoolean _restore(cciEntity& pEntity);

	ciBoolean _setBPState(cciEntity* pEntity, short nState);

	int _deleteIniFile(ciString& programId);

	ciBoolean _isUsed(const char* filename, const char* location);
	ciBoolean _deleteFolder(const char* location);
	int _deleteFile(ciString& filename, ciString& location);

	int	_removeChildren(const char* mgrId, const char* siteId, 
									const char* childClass, const char* parentEntity);
	ciBoolean _removeChild(cciEntity& targetEntity);

};



#endif // _trashCanReplyImpl_h_
