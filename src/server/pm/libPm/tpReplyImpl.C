
#include "tpReplyImpl.h"
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciCall.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libThread/ciThread.h>
#include <common/libCommon/ubcUtil.h>
#include <common/libScratch/scratchUtil.h>
#include <cci/libWrapper/cciEventQManager.h>


ciSET_DEBUG(10, "tpReplyImpl");


tpReplyImpl::~tpReplyImpl() 
{
    ciDEBUG(7, ("~tpReplyImpl()"));
}

ciBoolean
tpReplyImpl::_initObject()
{
    ciDEBUG(7, ("~tpReplyImpl()"));

	_object->addItemAsKey("mgrId", new cciString());
    _object->addItemAsKey("siteId",new cciString());
    _object->addItemAsKey("bpId",new cciString());
    _object->addItemAsKey("tpId",new cciString());
 
    _object->addItem("createTime", new cciTime());
    _object->addItem("startTime", new cciString());
    _object->addItem("endTime", new cciString());
    _object->addItem("programId", new cciString());
    _object->addItem("zorder", new cciString());

    _fromEntity(0,"mgrId");
    _fromEntity(1,"siteId");
    _fromEntity(2,"bpId");
    _fromEntity(3,"tpId");

    return ciTrue;
}

ciBoolean
tpReplyImpl::_execute()
{
	if(!_isMyCall("mgrId","siteId","UBC_Site")){
		ciWARN(("It's not my call"));
		return ciTrue;
	}

	return ciFalse;
}

ciBoolean
tpReplyImpl::_create()
{
	ciDEBUG(7,("_create()"));

	_genId(3,"tpId");
	
	if(!inherited::_create()) {
		ciERROR(("TP create failed"));
		return ciFalse;
	}
	_postEvent("tpCreated");
	return ciTrue;
}

ciBoolean
tpReplyImpl::_set()
{
	ciDEBUG(7,("_set()"));
	
	if(!inherited::_set()) {
		ciERROR(("TP set failed"));
		return ciFalse;
	}
	_postEvent("tpChanged");
	return ciTrue;
}

ciBoolean
tpReplyImpl::_remove()
{
	ciDEBUG(7,("_remove()"));
	if(!inherited::_moveToTrashCan("tpId","programId")) {
		ciERROR(("_moveToTrashCan failed"));
		return ciFalse;
	}

	if(!inherited::_remove()) {
		ciERROR(("TP remove failed"));
		return ciFalse;
	}
		
	_postEvent("tpRemoved");
	return ciTrue;
}

ciBoolean
tpReplyImpl::_restore()
{
	ciDEBUG(7,("_restore()"));
	
	if(!inherited::_restore()) {
		ciERROR(("TP restore failed"));
		return ciFalse;
	}
	if(_object->get()) {
		while(_object->next()) {
			_object->modifyEntity(_entity);
			_postEvent("tpCreated");
		}
	}
	return ciTrue;
}

void
tpReplyImpl::_genId(int idx, const char* idName)
{
	ciString rId = _entity.getClassValue(idx);
	if(rId == "unspecified" || rId == "*" || rId == "?" ) {
		ciDEBUG(1,("id unspecified,  id will be set by system"));
//		ciULong key = ubcUtil::getInstance()->genId(idName);
		char buf[256];
//		sprintf(buf,"%010ld", key);
		scratchUtil::getInstance()->CreateGUID(buf,sizeof(buf));
		_entity.setClassValue(idx,buf);	
		if(_reqList.itemExist(idName)){
			_reqList.removeItem(idName);
		}
		_object->setItem(idName, buf);
	}
}

ciBoolean
tpReplyImpl::_postEvent(const char* eventType)
{
	ciDEBUG(5,("_postEvent(%s)", eventType));

	ciString mgrId = _entity.getClassValue(0);
	ciString siteId = _entity.getClassValue(1);
	ciString bpId = _entity.getClassValue(2);
	ciString tpId = _entity.getClassValue(3);

	ciDEBUG(5,("_postEvent(%s,%s,%s,%s)", 
		eventType,siteId.c_str(), bpId.c_str(), tpId.c_str()));

	cciEvent aEvent;

	aEvent.setDomain(ciArgParser::getInstance()->getServerDomain());
	aEvent.setEntity(_entity);
	
	cciTime now;
	aEvent.setEventTime(now);

	aEvent.setEventType(eventType);
	aEvent.addItem("mgrId", mgrId.c_str());
	aEvent.addItem("siteId", siteId.c_str());
	aEvent.addItem("bpId", bpId.c_str());
	aEvent.addItem("tpId", tpId.c_str());

	if(cciEventQManager::getInstance()->push(aEvent)) {
		ciDEBUG(1,("%s(%s/%s) Event posted", eventType , siteId.c_str(), tpId.c_str()));
		return ciTrue;
	}
	ciERROR(("%s(%s/%s) Event posted failed", eventType, siteId.c_str(), tpId.c_str()));
	return ciFalse;

}
