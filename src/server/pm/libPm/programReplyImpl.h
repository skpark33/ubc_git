#ifndef _programReplyImpl_h_
#define _programReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>
//#include <common/libCommon/ubcIni.h>

class ubcCachedIni;

class programReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	programReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~programReplyImpl() ;

protected:
	virtual ciBoolean _execute();
	virtual ciBoolean _initObject();
	virtual ciBoolean _remove();
	virtual ciBoolean _restore();
	
	ciBoolean _removeLastSchedule();
	ciBoolean _setLastScheduleEmpty(const char* mgrId,const char* siteId,const char* hostId, int side);

	ciBoolean	_apply();

	ciBoolean	_getAppliedHostList(const char* programId, ciStringMap& outMap);
	int			_postReloadEvent(ciBoolean isNew, cciEntity& target, ciStringMap& applyHostList, ciTime& eventKey, ciString& errString);
	
	//ciBoolean	_updateProgram(ubcIni& ini, ciString& errString, ciBoolean& isNew);
	//ciBoolean	_updateContents(ubcIni& ini, ciString& errString);
	//ciBoolean	_updateTemplate(ubcIni& ini, ciString& errString);
	//ciBoolean	_updateFrame(ubcIni& ini, ciString& errString, const char* section, cciEntity& target);
	//ciBoolean	_updateDefaultSchedule(ubcIni& ini, ciString& errString, const char* section, cciEntity& target);
	//ciBoolean	_updateSchedule(ubcIni& ini, ciString& errString, const char* section, cciEntity& target);

	ciBoolean	_updateProgram(ubcCachedIni* ini, ciString& errString, ciBoolean& isNew);
	ciBoolean	_updateContents(ubcCachedIni* ini, ciString& errString);

	ciBoolean	_garbageCollection(const char* tablename, cciEntity& target, ciStringList& idList);
	int			_getGarbageIdList(const char* tablename,cciEntity& target,
										ciStringList& idList,ciStringList& outList);

	ciBoolean	_updateDB(aciReply* impl, cciEntity& target, cciRequest& pRequest, ciString& errString);
	ciBoolean	_updateDB(aciReply* impl, cciEntity& target, cciRequest& pRequest, ciString& errString,ciBoolean& isNew);
	ciBoolean	_updateProgramDB(aciReply* impl, cciEntity& target, cciRequest& pRequest, 
								   ciString& errString, ciBoolean& isNew, 
								   ciString& oldTemplatePlayList, ciTime& lastUpdateTime);
	const char*	_getIdName(cciEntity& entity);

	int			_createReservation(const char* programId,ciStringMap& hostIdList, ciString& startTime, ciString& endTime);
	int			_createReservation(const char* programId,const char* host, const char* side, const char* startTime, const char* endTime);

	ciBoolean	_createProgramLocation(const char* programId, const char* mgrId=0);
	ciBoolean	_removeProgramLocation(const char* programId);
	ciBoolean	_removeTP(const char* programId);
	ciBoolean	_getBraceValue(ciString& inval, ciString& outval);

};

#endif // _programReplyImpl_h_
