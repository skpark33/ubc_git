#ifndef _hostReplyImpl_h_
#define _hostReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <cci/libWrapper/cciCall.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#ifdef _COP_LINUX_
#include <ci/libMSCCompat/ciMSCCompatTypes.h>
#endif
#include <aci/libCall/aciReply.h>

//#ifdef WIN32
//#include <stdafx.h>
//#endif //WIN32


class hostReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	hostReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~hostReplyImpl() ;

protected:
	virtual ciBoolean _execute();
	virtual ciBoolean _initObject();

	virtual ciBoolean _set();
	virtual ciBoolean _create();
	virtual ciBoolean _remove();


	virtual ciBoolean _getScreenShot();

	virtual ciBoolean _startup();
	virtual ciBoolean _amtCmd(const char* amtCommand);
	virtual ciBoolean _wolStartup();

	virtual ciBoolean _shutdown();
	virtual ciBoolean _reboot();  
	
	virtual ciBoolean _killdown();
	virtual ciBoolean _reset();  

	virtual ciBoolean _setVolume();
	virtual ciBoolean _getVolume();
	virtual ciBoolean _muteOn();
	virtual ciBoolean _muteOff();
	
	virtual ciBoolean _ipconfig();
	virtual ciBoolean _ping();

	virtual ciBoolean _initSMS();

	virtual ciBoolean _getProperties();
	virtual ciBoolean _setProperties();
	virtual ciBoolean _getProperty();
	virtual ciBoolean _setProperty();
	ciBoolean	_setProperty(const char* name, const char*value);

	virtual ciBoolean _delProperty();

	virtual ciBoolean _changeDefaultTemplate();
	virtual ciBoolean _autoUpdate();
	virtual ciBoolean _restartProcess();
	virtual ciBoolean _cleanContents();
	virtual ciBoolean _cleanAllContents();
	virtual ciBoolean _diffVersion();
	virtual ciBoolean _register();
	virtual ciBoolean _setLastSchedules();
	virtual ciBoolean _monitorPower();

	ciBoolean _wakeup();
	ciBoolean _restore();
	ciBoolean	_simpleAgentCommand(const char* directive);
	ciBoolean	_simpleAgentCommand(cciCall& call);

	ciBoolean _registerReturn(const char* edition, ciBoolean errCode, const char* pFormat,...);
	ciBoolean _lookupSMSContents(const char* tablename, 
								  ciStringSet& contentsIdSet);
	ciBoolean _initComments(const char* contentsId);

	ciBoolean	_postDefaultTemplateChanged(const char* siteId,
										   const char* hostId,
										   const char* templateId);
	ciBoolean	_removeProcess();
	ciBoolean	_updateProcess(ciBoolean adminState);

	ciBoolean	_removeSchedule();
	ciBoolean	_postEvent(const char* eventType,ciBoolean flag=ciFalse, ciTime* bootUpTime=0, ciTime* bootDownTime=0);


	void	_powerStateLog(ciTime& bootUpTime, ciTime& bootDownTime);
	ciBoolean	_createApplyLog(cciEntity& targetHost,  // where
										const char* programId, // what
										const char* how,		// how
										const char* who,		// who
										ciTime&		applyTime );	// when

	ciBoolean	_bulkSetLastSchedule(ciString& hostList, 
									ciString& lastSchedule1, 
									ciTime& lastScheduleTime1, 
									ciBoolean scheduleApplied1,
									ciString& lastSchedule2, 
									ciTime& lastScheduleTime2, 
									ciBoolean scheduleApplied2);

	ciBoolean	_postBulkHostScheduleUpdated(ciStringList& entityList, 
									ciString& lastSchedule1, 
									ciString& lastSchedule2 );

	int			_getMonitorIdList(ciStringSet& monitorIdList);

	class	_CurrentValue {
	public:
		_CurrentValue() : mute(ciFalse), vncState(ciFalse),
		gmt(9999),
		monitorState(0),
		starterState(ciFalse),
		browserState(ciFalse),
		browser1State(ciFalse),
		firmwareViewState(ciFalse),
		preDownloaderState(ciFalse),
		period(0), screenshotPeriod(0),hddThreshold(0),  
		soundVolume(0),playLogDayLimit(0),autoUpdateFlag(ciFalse),renderMode(1),
		//monitorType(0),
		monitorOff(ciFalse) {}
		
		//ciString shutdownTime;
		ciBoolean mute;
		ciBoolean vncState;

		ciShort		gmt;
		ciShort		monitorState;
		ciBoolean starterState;
		ciBoolean browserState;
		ciBoolean browser1State;
		ciBoolean firmwareViewState;
		ciBoolean preDownloaderState;

		ciShort period;
		ciShort screenshotPeriod;
		ciString edition;
		ciString holiday;
		ciShort soundVolume;
		ciShort playLogDayLimit;

		ciString playLogDayCriteria;
		ciBoolean	monitorOff;
		ciString	monitorOffList;

		ciString category;
		//ciShort monitorType;
		ciString contentsDownloadTime;
		ciString winPassword;
		ciString weekShutdownTime;
		ciShort hddThreshold;
		ciBoolean autoUpdateFlag;
		ciLong	renderMode;

	};

	ciBoolean	_getCurrentValue(_CurrentValue& currentValue);
	ciBoolean	_getLiveProgramList(ciStringSet& programList);
	ciBoolean	_getReservedProgramList(ciStringSet& programList);
	ciBoolean	_getProgramList(ciString& programList, const char* siteId = "*");

	class _TemplateTime {
	public:
		ciString templateId;
		ciInt	 min;
	};
	typedef list<_TemplateTime>  _TemplateTimeList; 


private:
	UINT __hexStrToInt(ciString& hexStr);

};

#endif // _hostReplyImpl_h_
