#ifndef _defaultScheduleCache_h_
#define _defaultScheduleCache_h_

#include <server/pm/libPm/pmCache.h>

class defaultScheduleData : public pmCacheData {
public:
	ciString		mgrId;
	ciString		siteId;
	ciString		programId;
	ciString		templateId;
	ciString		frameId;
	ciString		scheduleId;

	ciString		requestId;		
	ciString		contentsId;		
	ciTime			startDate;		
	ciTime			endDate;		
	ciShort			playOrder;		
	ciBoolean		openningFlag;	
	ciLong			timeScope;		
	ciShort			priority;		
	ciLong			castingState;	
	ciTime			castingStateTime;	
	ciString		castingStateHistory;
	ciString		result;			
	ciBoolean		operationalState;
	ciBoolean		adminState;	
	ciString		phoneNumber;
	ciString		comment1; 		
	ciString		comment2; 		
	ciString		comment3; 		
	ciString		comment4; 		
	ciString		comment5; 		
	ciString		comment6; 		
	ciString		comment7; 		
	ciString		comment8; 		
	ciString		comment9; 		
	ciString		comment10; 		
	ciString		filename;		
	ciString		location;		
	ciString 		contentsType;	
	ciLong 			contentsState;	
	ciShort			volume;			
	ciLong			runningTime;	
	ciString		bgColor;		
	ciString		fgColor;		
	ciString		font;			
	ciShort			fontSize;		
	ciShort			playSpeed;		
	ciShort			soundVolume;	
	//ciString		promotionValueList;	
	ciString		parentScheduleId;	
	ciString		contentsName;		
};


class defaultScheduleCache : public virtual pmCache {
public:
	defaultScheduleCache() ;
	virtual ~defaultScheduleCache() ;
	virtual int init(cciEntity& pEntity);
	virtual	int	compare(pmCache* old);
	virtual ciBoolean	attrCompare(pmCacheData* newData, pmCacheData* oldData);
	virtual	int	postEvent(cciEntity& target, ciStringMap& applyHostList, ciTime& eventKey);


protected:

	CCI::CCI_StringList _entityList;
	CCI::CCI_EnumList	_stateList;

};

#endif // _defaultScheduleCache_h_
