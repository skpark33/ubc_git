#ifndef _planoReplyImpl_h_
#define _planoReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>
//#include <common/libCommon/ubcIni.h>

class planoReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	planoReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~planoReplyImpl() ;

protected:
	virtual ciBoolean _execute();
	virtual ciBoolean _initObject();
	virtual ciBoolean _create();
	virtual ciBoolean _remove();
	//virtual ciBoolean _restore();
	virtual ciBoolean _set();


};

#endif // _planoReplyImpl_h_
