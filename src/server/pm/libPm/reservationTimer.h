/*! \class reservationTimer
 *  Copyright ⓒ 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _reservationTimer_h_
#define _reservationTimer_h_


#include <ci/libTimer/ciTimer.h>


class reservEle {
public:
	reservEle() { fromTime.set((time_t)0); toTime.set((time_t)0); hasStart=ciFalse,hasEnd=ciFalse; side=1;}
	reservEle(reservEle* ele) { 
		mgrId=ele->mgrId;
		siteId=ele->siteId;
		hostId=ele->hostId;
		reservationId=ele->reservationId;
		
		programId=ele->programId;
		side=ele->side;
		fromTime=ele->fromTime;
		toTime=ele->toTime;

		hasStart=ele->hasStart;
		hasEnd=ele->hasEnd;
	}

	ciString	mgrId;
	ciString	siteId;
	ciString	hostId;
	ciString	reservationId;

	ciString	programId;
	ciShort		side;
	ciTime		fromTime;
	ciTime		toTime;

	ciBoolean	hasStart;
	ciBoolean	hasEnd;
};

typedef map<ciString, reservEle*>		RESERV_MAP;
typedef map<ciString, ciStringSet*>		HOST_RESERV_MAP;


class reservationTimer :  public  ciPollable {
public:
	static reservationTimer*	getInstance();
	static void	clearInstance();

	reservationTimer() {_readyTime=30;}
	virtual ~reservationTimer() { clear();}
	
	virtual void processExpired(ciString name, int counter, int interval);

	void		load(int readyTime=30);
	void		load(cciEntity& entity);
	void		clear();

	void		add(reservEle* aEle);
	ciBoolean		erase(const char* reservationId, ciTime& now);
	ciBoolean		eraseRemoved(const char* reservationId);

	ciBoolean		changeFromTime(const char* reservationId, ciTime& fromTime);
	ciBoolean		changeToTime(const char* reservationId, ciTime& toTime);

	reservEle*	find(const char* reservationId);
	reservEle*	findRemoved(const char* reservationId);

	void		printIt();

	void		resetReservedScheduleList(reservEle* ele, 	
									CCI::CCI_StringList& reservedScheduleList1,
									CCI::CCI_StringList& reservedScheduleList2,
									ciBoolean isRemoved);
	

protected:
	void			_genReservedScheduleList(const char* hostId, const char* removedReservation=0);
	int				_getReservedScheduleList(const char* hostId,int pSide, CCI::CCI_StringList& outList);
	ciBoolean		_setReservedScheduleList(const char* siteId, const char* hostId);
	ciBoolean		_setReservedScheduleList(const char* siteId, const char* hostId,
												CCI::CCI_StringList& inList1,
												CCI::CCI_StringList& inList2);

	void			_postReservationExpired(ciBoolean isStart, reservEle* ele, const char* removedProgramId="");
	ciBoolean		_setLastSchedule(reservEle* ele);
	ciBoolean		_getBaseSchedule(reservEle* ele, 
										ciString& baseSchedule1, ciString& baseSchedule2);
	ciBoolean		_postReloadEvent(reservEle* ele);

	static reservationTimer*	_instance;
	static ciMutex			_instanceLock;

	ciMutex		_mapLock;
	RESERV_MAP	_map;
	RESERV_MAP	_removed_map;

	ciMutex			_host_mapLock;
	HOST_RESERV_MAP	_host_map;

	int			_readyTime;  // 예약을 미리 실행시키는 정도 초

};	

#endif //_reservationTimer_h_
