#ifndef _downloadStateReplyImpl_h_
#define _downloadStateReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class downloadStateReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	downloadStateReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~downloadStateReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _create();
    virtual ciBoolean _set();
    virtual ciBoolean _remove();
    virtual ciBoolean _initObject();

	ciBoolean	_genId(int idx, const char* idName);

};

#endif // _downloadStateReplyImpl_h_
