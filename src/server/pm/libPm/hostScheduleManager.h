#ifndef _hostScheduleManager_h_
#define _hostScheduleManager_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include <common/libCommon/utvMacro.h>
#include <ci/libTimer/ciTimer.h>

class HostSchedule {
public:
	ciString		currentSchedule1;
	ciString		currentSchedule2;
	ciString		hostMsg1;
	ciString		hostMsg2;
	ciString		autoSchedule1;
	ciString		autoSchedule2;
	ciString		lastSchedule1;
	ciString		lastSchedule2;
	ciShort			networkUse1;
	ciShort			networkUse2;
	HostSchedule() 
		: networkUse1(-1),networkUse2(-1)
		  ,currentSchedule1("-"), currentSchedule2("-") 
		  ,lastSchedule1("-"), lastSchedule2("-") 
		  ,autoSchedule1("-"), autoSchedule2("-") {}

	HostSchedule(HostSchedule* p){
		currentSchedule1=p->currentSchedule1;
		currentSchedule2=p->currentSchedule2;
		hostMsg1=p->hostMsg1;
		hostMsg2=p->hostMsg2;
		autoSchedule1=p->autoSchedule1;
		autoSchedule2=p->autoSchedule2;
		lastSchedule1=p->lastSchedule1;
		lastSchedule2=p->lastSchedule2;
		networkUse1=p->networkUse1;
		networkUse2=p->networkUse2;
	}

	void printIt();
};

typedef map<string,HostSchedule*> SCHMAP;

class hostScheduleManager {
public:
	static hostScheduleManager*	getInstance();
	static void	clearInstance();

	virtual ~hostScheduleManager() ;
	int		load();
	void	clear();

	ciBoolean	postEvent(const char* entity, HostSchedule* pEle);
	ciBoolean	postReloadEvent(const char* entity, HostSchedule* pEle, ciShort side);

	void set(const char* entity, HostSchedule* pEle, ciBoolean owner=ciTrue);
	ciBoolean set(cciEntity& entity, ciString& lastSchedule1,ciString& lastSchedule2);

	void push(const char* entity);
	void erase(const char* entity);

protected:
	hostScheduleManager();

	ciMutex	_mapLock;
	SCHMAP	_map;

	static hostScheduleManager*	_instance;
	static ciMutex			_instanceLock;

};

#endif 
