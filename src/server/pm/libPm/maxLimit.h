/*! \class maxLimit
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _maxLimit_h_
#define _maxLimit_h_


#include <ci/libTimer/ciTimer.h>
#include "cci/libValue/cciStringList.h"


class maxLimit  {
public:
	enum APPROVE_STATUS { eStatusNoState=0, 
					  eStatusRequested, eStatusAccepted, eStatusRejected, eStatusRemoved, eStatusCount, };

	static maxLimit*	getInstance();
	static void	clearInstance();

	maxLimit() ;
	virtual ~maxLimit() { }

	void	load();
	int		getRowCount(const char* tableName);

	ciBoolean	isOverHost();
	ciBoolean	isOverUser();
	ciBoolean	isOverSite();

	ciBoolean	isStrongPassword() { return _isStrongPassword; }
	ciBoolean	isOnewayPassword() { return _isOnewayPassword; }
	ciBoolean	isStrongLog() { return _isStrongLog; }

	void	encodeSha256(ciString& input, ciString& output);
	ciBoolean	isSameSha256(ciString& lValue, ciString& rValue);

	int		getApproveState(const char* action, cciEntity& target, 
							cciAttributeList& attributeList, const char* requestedId);
	ciBoolean   approveRequest(const char* action, cciEntity& target, 
							cciAttributeList& attributeList, const char* requestedId);
	ciBoolean   setApproveStateRemove(const char* action, cciEntity& target);


	const char*	getApproveId() { return _managerId.c_str(); }

protected:
	static maxLimit*	_instance;
	static ciMutex			_instanceLock;

	int _host;
	int _site;
	int _user;
	ciBoolean	_isStrongPassword;
	ciBoolean	_isStrongLog;
	ciBoolean	_isOnewayPassword;

	ciString	_managerId;
};	

#endif //_maxLimit_h_
