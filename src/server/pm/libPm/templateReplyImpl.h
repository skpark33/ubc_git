#ifndef _templateReplyImpl_h_
#define _templateReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class templateReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	templateReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~templateReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();
	virtual ciBoolean _create();
	virtual ciBoolean _set();
	virtual ciBoolean _remove();
	//virtual ciBoolean _restore();
	ciBoolean	_createBGContents(ciString& bgImage, ciULong& outSerialNo);
	ciBoolean	_getBGContents(ciString& mgrId, ciString& siteId, ciString& programId,
								ciString& contentsId, ciString& pLocation, ciString& pFilename,
								ciULong& outSerialNo);
	ciBoolean	_createBGContents(ciString& mgrId, ciString& siteId, ciString& programId,
								ciString& contentsId, ciString& pLocation, ciString& pFilename);

};

#endif // _templateReplyImpl_h_
