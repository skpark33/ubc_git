#ifndef _siteReplyImpl_h_
#define _siteReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class siteReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	siteReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~siteReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

	virtual ciBoolean _remove();
	virtual ciBoolean _restore();
	virtual ciBoolean _create();
	virtual ciBoolean _hostAutoUpdate();
	virtual ciBoolean _changeSite();

	int _updateAddionalData(ciStringMap& srcAndTarget);
	int _updateBP(ciString& src, ciString& target);
	ciBoolean	_postBPChanged(cciEntity& bpEntity);
	ciBoolean	_changeBPIFile(cciEntity& bpEntity, ciString& src, ciString& dest);

	ciBoolean	_isExist(const char* siteId);
	ciBoolean	_isExist_sitename(const char* siteName);
};

#endif // _siteReplyImpl_h_
