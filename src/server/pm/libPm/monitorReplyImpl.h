#ifndef _monitorReplyImpl_h_
#define _monitorReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>
//#include <common/libCommon/ubcIni.h>

class monitorReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	monitorReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~monitorReplyImpl() ;

protected:
	virtual ciBoolean _execute();
	virtual ciBoolean _initObject();
	virtual ciBoolean _create();
	virtual ciBoolean _remove();
	virtual ciBoolean _set();

	ciBoolean	_startup();
	ciBoolean	_shutdown();

	ciBoolean   _getBeamIp(ciString& beamIp);

	ciBoolean	_postEvent(const char* eventType, ciBoolean flag);

};

#endif // _monitorReplyImpl_h_
