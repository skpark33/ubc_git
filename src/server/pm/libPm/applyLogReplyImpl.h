#ifndef _applyLogReplyImpl_h_
#define _applyLogReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

#define PROGRAM_DELI '_'

class applyLogReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	applyLogReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~applyLogReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();
    virtual ciBoolean _create();
    virtual ciBoolean _remove();

	void _getPackageSize(const char* packageName, ciString& volume);

};

#endif // _applyLogReplyImpl_h_
