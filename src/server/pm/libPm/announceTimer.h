/*! \class announceTimer
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _announceTimer_h_
#define _announceTimer_h_


#include <ci/libTimer/ciTimer.h>
#include "cci/libValue/cciStringList.h"


class annEle {
public:
	annEle() { startTime.set((time_t)0); endTime.set((time_t)0); hasStart=ciFalse;hasEnd=ciFalse;gmt=9999;createTime=time(NULL);contentsType=2;}
	ciString	mgrId;
	ciString	siteId;
	ciString	announceId;
	ciShort		contentsType;

	ciTime		startTime;
	ciTime		endTime;
	ciBoolean	hasStart;
	ciBoolean	hasEnd;
	CCI::CCI_StringList	hostIdList;
	ciShort		gmt;
	time_t		createTime;
};

typedef map<ciString, annEle*>		ANN_MAP;


class announceTimer :  public  ciPollable {
public:
	static announceTimer*	getInstance();
	static void	clearInstance();

	announceTimer() { };
	virtual ~announceTimer() { clear();}

    virtual void processExpired(ciString name, int counter, int interval);

	void	load();
	void	clear();

	void	add(annEle* aEle);
	ciBoolean	erase(const char* announceId);
	ciBoolean	eraseRemoved(const char* announceId);

	annEle*	find(const char* announceId);
	annEle*	findRemoved(const char* announceId);

	void	printIt();
	//ciBoolean removeAnnounce(annEle* ele);
	void	postAnnounceExpired(ciBoolean isStart, const char* announceId);


protected:

	ciBoolean _hostIsExist(CCI::CCI_StringList& lList, CCI::CCI_StringList& rList);
	void	_postAnnounceExpired(ciBoolean isStart, annEle* ele);

	void	_getGMT(annEle* aEle);
	ciShort	_getGMT(const char* hostId);

	static announceTimer*	_instance;
	static ciMutex			_instanceLock;

	ciMutex		_mapLock;
	ANN_MAP	_map;
	ANN_MAP	_removed_map;

};	

#endif //_announceTimer_h_
