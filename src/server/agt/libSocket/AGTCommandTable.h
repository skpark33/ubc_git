#ifndef _AGTCommandTable_h_
#define _AGTCommandTable_h_

#include "ci/libBase/ciBaseType.h"
#include "ci/libBase/ciListType.h"
#include "ci/libThread/ciSyncUtil.h"

class AGTCommand;
class AGTCommandTable {
public:	
	typedef list<AGTCommand*> CMD_LIST;
	typedef map<ciString, CMD_LIST*> CMD_MAP;

	virtual ~AGTCommandTable();

	static AGTCommandTable* getInstance();
	static void clearInstance();

	void register_agent(const char* hostId);
	ciStringVector* get_agent_list();

	void push_back(const char* hostId, AGTCommand* command);
	AGTCommand* pop_front(const char* hostId);
	CMD_LIST* getCmdList(const char* hostId);

protected:
	AGTCommandTable();
	static AGTCommandTable* _instance;
	static ciMutex _sLock;

	ciMutex _tableLock;
	CMD_MAP _commandTable;

};

#endif // _AGTCommandTable_h_
