#ifndef _AGTServerSession_h_
#define _AGTServerSession_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libBase/ciTime.h>
#include "AGTCommandTable.h"
#include "AGTMessageQue.h"

////////////////////
// AGTServerSession

class AGTServerSession : public ciThread {
public:
	AGTServerSession(int port);
	virtual ~AGTServerSession();

	ciBoolean init();
	void fini();

	void run();
	void destroy();

	ciBoolean isHalt(int timeout);  // skpark 2010.11.
protected:
	ciBoolean _isAlive;
	int _serverFd;
	int _serverPort;

	ciMutex	_counterLock;
	ciTime	_lastTime;  // 마지막으로 데이터를 받은 시각
	ciULong	_counter;   // 데이터 받은 횟수(누적)

	void _checkReport(AGTData* recvData, AGTCommandTable::CMD_LIST* cmdList);
	ciBoolean _processHostInfo(int client_fd,const char* received, const char* classCode, ciBoolean interactive);
	ciBoolean _processDownloadState(int client_fd,const char* received, const char* classCode, ciBoolean interactive);


	//AGTMessageQue* _msgQue;
};

#endif // _AGTServerSession_h_
