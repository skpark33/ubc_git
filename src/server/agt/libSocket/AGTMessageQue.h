#ifndef _AGTMessageQue_h_
#define _AGTMessageQue_h_

#include <queue>
#include <ci/libThread/ciSyncUtil.h>
#include <ci/libThread/ciCondition.h>
#include "common/libAGTData/AGTData.h"

class AGTThreadPool;

////////////////////////
//	AGTMessageQue

class AGTMessageQue {
public:
	AGTMessageQue();
	virtual ~AGTMessageQue();

	void add(AGTData* message);
	AGTData* get();
protected:

	queue<AGTData*> _msgQueue;
	ciCondition* _msgCond;
	ciMutex _msgLock;

};

#endif // _AGTMessageQue_h_
