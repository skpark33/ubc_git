#ifndef _AGTThread_h_
#define _AGTThread_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include <ci/libThread/ciCondition.h>
#include "common/libAGTData/AGTData.h"
#include "AGTThreadPool.h"
#include "AGTMessageQue.h"

////////////////////
// AGTThread

class AGTThread : public virtual ciThread {
private:
	bool _isAlive;
	AGTThreadPool* _ownerPool;
protected:
	int	_getSiteId(const char* hostId, ciString& siteId);

	ciBoolean _pmCall(AGTData* message);
	AGTMessageQue*	_msgQue;
public:
	bool isWorking;
	int	hostCounter;

	AGTThread(AGTThreadPool* ownerPool);
	virtual ~AGTThread();

	void run();
	void destroy();
	void add(AGTData* message) { _msgQue->add(message); }
};

#endif // _AGTThread_h_
