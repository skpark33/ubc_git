#ifndef _AGTReload_h_
#define _AGTReload_h_

#include "ci/libBase/ciBaseType.h"
#include "ci/libBase/ciListType.h"
#include "ci/libThread/ciSyncUtil.h"

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>


class AGTReload  : public cciEventHandler {
public:	
	virtual ~AGTReload();

	static AGTReload* getInstance();
	static void clearInstance();

	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean		addHand(const char* serverId);


protected:
	AGTReload();
	static AGTReload* _instance;
	static ciMutex _sLock;

};

#endif // _AGTReload_h_
