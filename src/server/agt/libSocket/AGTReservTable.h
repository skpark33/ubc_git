#ifndef _AGTReservTable_h_
#define _AGTReservTable_h_

#include "ci/libBase/ciBaseType.h"
#include "ci/libBase/ciListType.h"
#include "ci/libThread/ciSyncUtil.h"

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>
#include "common/libAGTData/AGTCommand.h"

class AGTReservInfo
{
public:
	AGTReservInfo() : directive("reserved"), isStart(ciTrue), side(0) {}
	ciString	siteId;
	ciString	reservationId;
	ciString	programId;
	ciShort		side;		// 1 : �ո�, 2: �޸�, 3:���
	ciTime		fromTime;
	ciTime		toTime;
	ciString	directive;
	ciString	past_directive;
	ciBoolean	isStart;
	ciString	removedProgramId;
};
typedef map<ciString, AGTReservInfo*>	RESERV_MAP;


class AGTReservTable  : public cciEventHandler {
public:	
	typedef map<ciString,RESERV_MAP*>	RESERV_HOST_MAP;

	virtual ~AGTReservTable();

	static AGTReservTable* getInstance();
	static void clearInstance();

	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean		addHand(const char* serverId);

	int init(const char* serverId);
	void clear();
	int getReservInfo(const char* hostId, AGTCommandList& commandList);
	void	update(const char* directive,
				   const char* hostId,const char* siteId, const char* reservationId,
				   const char* programId,ciShort side,ciTime& fromTime,ciTime& toTime,
				   ciBoolean isStart=ciTrue,  const char* removedProgramId="");
	ciBoolean	remove(const char* hostId);
	ciBoolean	remove(const char* hostId, const char* reservationId);

	void	printIt();
protected:
	AGTReservTable();
	static AGTReservTable* _instance;
	static ciMutex _sLock;

	ciMutex _mapLock;
	RESERV_HOST_MAP _map;

};

#endif // _AGTReservTable_h_
