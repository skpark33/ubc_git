#ifndef _AGTDownloadResultServer_h_
#define _AGTDownloadResultServer_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include <ci/libThread/ciCondition.h>
#include "common/libAGTData/AGTData.h"
#include "common/libAGTData/AGTDownloadResult.h"

////////////////////
// AGTDownloadResultServer
class AGTServerDR;

class AGTDownloadResultServer  {
public:
	static AGTDownloadResultServer*	getInstance();
	static void	clearInstance();

	virtual ~AGTDownloadResultServer();
	//void clear();
	//void push(AGTDownloadResult* ele);
	//ciBoolean hasMore() ;
	//AGTDownloadResult*	pop();
	//int run();
	ciBoolean run(AGTDownloadResult* pResult);

protected:
	AGTDownloadResultServer();

	ciBoolean _isExistSameKey(AGTServerDR* pResult, int& oldState);
	ciBoolean _isExistSameKeyInDB(AGTServerDR* pResult, int& oldState);

	static AGTDownloadResultServer*	_instance;
	static ciMutex _instanceLock;

	//ciMutex					_listLock;
	//AGTDownloadResultList	_list;

	class resultEle {
	public:
		ciULong  updateTime;
		int		 state;	  
	};

	ciMutex		_resultKeyMapLock;
	map<ciString,resultEle*>	_resultKeyMap;  // key,time
};


class AGTServerDR : public virtual AGTDownloadResult
{
public:
	AGTServerDR(const char* siteId,
				 const char* hostId, 
				 ciShort brwId, 
				 const char* programId, 
				 const char* progress, 
				 ciLong state); 
	AGTServerDR() {}
	AGTServerDR(AGTDownloadResult* target);
	virtual ~AGTServerDR() {}

	ciBoolean	get(const char* entity, const char* whereClause);
	ciBoolean	isEmpty() { if(_downloadIdList.size()) return ciFalse; return ciTrue;}
	
	ciBoolean	create(); 
	ciBoolean	createLog(); 
	
	ciBoolean	createSummary(); 
	ciBoolean	updateSummary(const char* downloadId, ciBoolean stateOnly=ciFalse); 
	ciBoolean	getSummary(ciString& summaryId, int& state); 
	ciBoolean	removeOldSummary(); 

	ciBoolean	update(AGTServerDR& old) ;
	ciBoolean	updateHost() ;
	ciBoolean	postEvent() ;
	ciBoolean	removeOldData();

	const char*	getKey() { return _key.c_str(); }
protected:
	const char*	_findDownloadId(const char* contentsId, AGTServerDR& old) ;

	ciStringList			_downloadIdList;
	ciString _key;

};


#endif // _AGTDownloadResultServer_h_
