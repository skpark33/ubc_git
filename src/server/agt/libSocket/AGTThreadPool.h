#ifndef _AGTThreadPool_h_
#define _AGTThreadPool_h_

#include <ci/libThread/ciSyncUtil.h>
#include <ci/libThread/ciCondition.h>

class AGTMessageQue;
class AGTThread;

////////////////////
// AGTThreadPool

const unsigned int DefaultThreadSize = 3;

class AGTThreadPool {
public:
	static AGTThreadPool* getInstance();
	static void clearInstance();

	virtual ~AGTThreadPool();
	void add(const char* hostId, AGTData* message);

protected:

	AGTThreadPool();

	void _init();
	void _fini();

	vector<AGTThread*> _pool;
	ciMutex _poolLock;

	map<ciString, AGTThread*>	_map;
	ciMutex _mapLock;

	unsigned int _poolSize;

	static ciMutex _sLock;
	static AGTThreadPool* _instance;
};

#endif // _AGTThreadPool_h_
