#ifndef _AGTHostTable_h_
#define _AGTHostTable_h_

#include "ci/libBase/ciBaseType.h"
#include "ci/libBase/ciListType.h"
#include "ci/libThread/ciSyncUtil.h"

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

class AGTScheduleInfo
{
public:
	AGTScheduleInfo() : reload1(ciFalse), reload2(ciFalse),
		lastScheduleTime1(0),lastScheduleTime2(0) {}
	ciString lastSchedule1;
	ciString lastSchedule2;
	ciBoolean reload1;
	ciBoolean reload2;
	time_t lastScheduleTime1;
	time_t lastScheduleTime2;
};

class AGTHostTable  : public cciEventHandler {
public:	
	typedef map<ciString, AGTScheduleInfo*> HOST_MAP;

	virtual ~AGTHostTable();

	static AGTHostTable* getInstance();
	static void clearInstance();

	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean		addHand(const char* serverId);

	int init(const char* serverId);
	void clear();
	const char* 	getLastSchedule1(const char* hostId);
	const char* 	getLastSchedule2(const char* hostId);
	ciBoolean	getLastSchedule(const char* hostId, 
								ciString& lastSchedule1, ciString& lastSchedule2,
								time_t& lastScheduleTime1, time_t& lastSchedule2Time,
								ciBoolean& reload1, ciBoolean& reload2);
	void	update(const char* hostId, const char* lastSchedule1, const char* lastSchedule2);
	void	updateReload(const char* hostId, ciBoolean reload1,ciBoolean reload2,
						const char* lastSchedule1=0, const char* lastSchedule2=0);
	ciBoolean	remove(const char* hostId);
	ciBoolean	isExist(const char* hostId);

protected:
	AGTHostTable();
	static AGTHostTable* _instance;
	static ciMutex _sLock;

	ciMutex _mapLock;
	HOST_MAP _map;
};

#endif // _AGTHostTable_h_
