#ifndef _AGTAnnounceEventHandler_h_
#define _AGTAnnounceEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

class AGTAnnounceObject {
public:
	AGTAnnounceObject() : 
	  m_isStart(ciFalse), m_ulSerialNo(0),m_lPosition(0),m_sHeight(100),m_sFontSize(24),m_sPlaySpeed(1),m_sAlpha(0) 
	  ,m_ContentsType(2)
	,m_LRMargin(0)
	,m_UDMargin(0)
	,m_SourceSize(1)
	,m_SoundVolume(0)
	,m_FileSize(0)
	,m_FileMD5("")
	,m_HeightRatio(9.0/16.0)
	,m_Width(0)
	,m_PosX(0)
	,m_PosY(0)
	{}

	~AGTAnnounceObject() {}
	ciBoolean		m_isStart;				// 스타트 여부
	ciString		m_strMgrId;				///<프로세스ID
	ciString		m_strSiteId;			///<SITE ID
	ciString		m_strAnnounceId;		///<긴급공지 ID
	ciString		m_strTitle;				///<제목
	ciULong			m_ulSerialNo;			///<Key 번호
	ciString		m_strCreator;			///<생성자 ID
	ciTime			m_tmCreateTime;			///<생성 시간
	cciStringList	m_listHostIdList;		///<적용될 호스트 리스트
	ciTime			m_tmStartTime;			///<시작 시간, 0 이면 즉시
	ciTime			m_tmEndTime;			///<끝 시간, 0 이면 계속
	ciLong			m_lPosition;			///<티커 위치(0 : 바닥  1: 최상단. 2; 중앙)
	ciShort			m_sHeight;				///<티커의 두께
	ciString		m_strComment[10];		///<공지글 내용 최대 10줄
	ciString		m_strFont;
	ciShort			m_sFontSize;
	ciString		m_strFgColor;
	ciString		m_strBgColor;
	ciString		m_strBgLocation;		///<백그라운드 파일 경로
	ciString		m_strBgFile;			///<백그라운드 파일 명
	ciShort			m_sPlaySpeed;			///<속도는 1~5 까지의 값을 갖고 1이 제일 느린 속도이다
	ciShort			m_sAlpha;				///<0-100 투명도. 클수록 투명하다 . 추후 구현

	//cciStringList m_HostNameList;
	ciShort	m_ContentsType;
	ciShort m_LRMargin;
	ciShort m_UDMargin;
	ciShort m_SourceSize;
	ciShort m_SoundVolume;
	ciULongLong m_FileSize;
	ciString m_FileMD5;
	//ciBoolean	m_IsFileChanged;// DB 에는 기록되지 않는 값이다.
	ciFloat	m_HeightRatio; // 가로대 세로비 가로를 1로 했을때, 세로의 값이다. DB 에는 기록되지 않는다.
	ciShort		m_Width;
	ciShort   m_PosX;
	ciShort   m_PosY;

};

class AGTAnnounceEventHandler : public cciEventHandler {
public:
	static AGTAnnounceEventHandler* getInstance();
	static void clearInstance();

	virtual ~AGTAnnounceEventHandler() {}
   	virtual void 	processEvent(cciEvent& pEvent);
	ciBoolean		addHand();

protected:
	AGTAnnounceEventHandler() 	{}

	int			getAGTAnnounceObject(const char* announceId, AGTAnnounceObject& objList);
	ciBoolean	announce(const char* announceId, ciBoolean isStart, ciStringList& myHostList, ciShort contentsType);
	ciBoolean	sendAnnounce(const char* hostId, AGTAnnounceObject& pAnnounce);

	static AGTAnnounceEventHandler* _instance;
	static ciMutex _sLock;

};


#endif // _AGTAnnounceEventHandler_h_
