 /*! \file AGTServerWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _AGTServerWorld_h_
#define _AGTServerWorld_h_

#include <aci/libCall/aciCallWorld.h> 
#include "server/agt/agtServer/AGTSessionMonitor.h"
#ifdef _MUX_TEST_
#	include "SVR/libAGTMux/AGTMuxSession.h"
#endif

class AGTServerWorld : public aciCallWorld {
public:
	AGTServerWorld();
	virtual ~AGTServerWorld();
 	
	//[
	//	From aciCallWorld
	//
	virtual aciCall*    newCallImpl(const char* pname);
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);
	//
	//]

	static int callTest(char* p_param);
	ciBoolean	isMain();
	ciBoolean	timeSync(const char* timeHost, int timePort);

protected:	
	ciBoolean _initProgram();
	ciBoolean _initBP();


	int _getSocketPort(const char* pId);
	
	AGTServerSession* _session;
	AGTSessionMonitor* _monitor;
#ifdef _MUX_TEST_
	AGTMuxSession* _muxSession;
#endif
	int	_isMain;
};
		
#endif //_AGTServerWorld_h_
