#ifndef _AGTSessionMonitor_h_
#define _AGTSessionMonitor_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include <ci/libThread/ciCondition.h>
#include "common/libAGTData/AGTData.h"
#include "server/agt/libSocket/AGTServerSession.h"

class AGTServerWorld;

////////////////////
// AGTSessionMonitor

class AGTSessionMonitor : public virtual ciThread {
public:
	AGTSessionMonitor(AGTServerSession* session,
						AGTServerWorld* world, int timeout) 
		: _session(session),_world(world),_isAlive(ciTrue),_timeout(timeout) {}
	virtual ~AGTSessionMonitor();


	void run();
	void destroy();
protected:
	AGTServerSession* _session;
	AGTServerWorld* _world;

	int _timeout;
	ciBoolean _isAlive;
};

#endif // _AGTSessionMonitor_h_
