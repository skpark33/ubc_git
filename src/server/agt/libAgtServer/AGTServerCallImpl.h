/*! \class AGTServerCallImpl
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief UTV_Mgr Implement
 *  (Environment: Visibroker 7.0, Windows XP)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _AGTServerCallImpl_h_
#define _AGTServerCallImpl_h_


#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciAttribute.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciScopeInfo.h>

#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libBase/ciBaseType.h>
#include <cci/libUtil/cciPOAUtil.h>

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include "aci/libCall/aciCall.h"

class AGTServerCallImpl : virtual public  aciCall {
public:
	AGTServerCallImpl(const char* pname) : _id(pname), aciCall(pname) {}
	virtual ~AGTServerCallImpl() {} ;
	virtual aciReply*	newCallReply(cciEntity& pEntity, const char* directive);
protected:
	ciString _id;
};	

#endif //_AGTServerCallImpl_h_
