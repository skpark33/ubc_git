#ifndef _AGTClientReplyImpl_h_
#define _AGTClientReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciScopeInfo.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciException.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class AGTClientReplyImpl : public virtual aciReply {
public:
	AGTClientReplyImpl();
	virtual ~AGTClientReplyImpl() {};

	virtual ciBoolean execute();
	CORBA::Boolean get(CCI::CCI_Reply_out pReply);

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();
	virtual int _getIndex();

    virtual ciBoolean _get();
	virtual ciBoolean _set();
	virtual ciBoolean _shutdown();
	virtual ciBoolean _startup();
	virtual ciBoolean _reboot();
    virtual ciBoolean _wakeup();
	virtual ciBoolean _restartProcess();
	virtual ciBoolean _cleanContents();
	virtual ciBoolean _cleanAllContents();
	virtual ciBoolean _monitorPower();
	virtual ciBoolean _autoUpdate();
    
protected:
	ciString _mgrId;
	ciString _hostId;

    int _indexCounter;
    ciStringVector    _indexValues;
};

#endif // _AGTClientReplyImpl_h_
