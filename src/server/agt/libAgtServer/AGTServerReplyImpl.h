#ifndef _AGTServerReplyImpl_h_
#define _AGTServerReplyImpl_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <aci/libCall/aciReply.h>

class AGTServerReplyImpl : public virtual aciReply {
public:
	AGTServerReplyImpl();
	virtual ~AGTServerReplyImpl() {};

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

    virtual ciBoolean _get();
    virtual ciBoolean _getTimeLine();

protected:
	ciString _mgrId;
};


#endif // _AGTServerReplyImpl_h_
