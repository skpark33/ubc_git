#pragma once


////////////////////////////////////////////////////////////
// CVNCSocket 명령 대상입니다.

class CVNCSocket : public CAsyncSocket
{
public:

	static CCriticalSection m_csDestroySocket;
	static CMapStringToPtr m_mapDestroySocket;
	static BOOL* m_pRunThread;

	static BOOL DestroyDeleteSocket(CVNCSocket** sock, bool bDeleteNow=false);
	static UINT CheckDestroySocketThread(LPVOID pParam);
	static void StartCheckThread();
	static void CloseCheckThread();

public:
	CVNCSocket(SOCKET_TYPE eType, HWND hParentWnd);
	virtual ~CVNCSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

	int 		GetID() { return m_nID; };
	SOCKET_TYPE	GetType() { return m_eSocketType; };

	void		SetLinkSocket(CVNCSocket* pSock);

	BOOL		SendEx(LPBYTE pData, int len);
	BOOL		FlushBuffer();

	void		SetTimeStamp() { m_dwTimeStamp=::GetTickCount(); };
	DWORD		GetTimeStamp() { return m_dwTimeStamp; };

	void		ExtractPeerInfo() { GetPeerName(m_strPeerIP, m_nPeerPort); m_tmConnectTime=CTime::GetCurrentTime(); };
	void		GetPeerInfo(CString& strIP, UINT& nPort) { strIP=m_strPeerIP; nPort=m_nPeerPort; };
	void		GetPeerInfo(CString& strIP, UINT& nPort, CTime& tmConnect) { strIP=m_strPeerIP; nPort=m_nPeerPort; tmConnect=m_tmConnectTime; };

	LPCTSTR		GetSocketInfo();
	LPCTSTR		GetSocketInfo(DWORD dwErrCode);

	bool		m_bInitialized;

protected:
	SOCKET_TYPE	m_eSocketType;
	HWND		m_hParentWnd;
	int			m_nID;

	LPBYTE		m_pBuffer;
	int			m_nBufferLen;
	void		PacketAnalysis();

	CCriticalSection	m_cs;
	CVNCSocket*			m_pLinkSocket;

	DWORD		m_dwTimeStamp;

	CString		m_strPeerIP;
	UINT		m_nPeerPort;
	CTime		m_tmConnectTime;

	CString		m_strInfo;
	CString		m_strInfoEx;
};

