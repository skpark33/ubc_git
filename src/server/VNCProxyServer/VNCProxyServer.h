// VNCProxyServer.h : main header file for the VNCProxyServer application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CVNCProxyServerApp:
// See VNCProxyServer.cpp for the implementation of this class
//

class CVNCProxyServerApp : public CWinApp
{
public:
	CVNCProxyServerApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CVNCProxyServerApp theApp;