#pragma once

#include <tlhelp32.h>
#include <shlwapi.h>
#include <winternl.h>

// CListenSocket 명령 대상입니다.


class CListenSocket : public CAsyncSocket
{
public:
	CListenSocket();
	virtual ~CListenSocket();

	virtual void OnAccept(int nErrorCode);
	virtual void OnClose(int nErrorCode);

	UINT	m_nPort;
	SOCKET_TYPE	m_eListenType;

protected:
	HWND	m_hParentWnd;
	BOOL	m_bOpen;
	BOOL	m_bRetryConnect;

public:
	void	Init(HWND hParentWnd, UINT nPort, SOCKET_TYPE eType);

	BOOL	IsOpened() { return m_bOpen; };

	BOOL	StartListen();
	void	StopListen();
};


////////////////////////////////////////////////////////////
class CSocketBuffer
{
public:
	CSocketBuffer() { m_pBuffer=NULL; m_nBufferSize=0; };
	virtual ~CSocketBuffer()
	{
		Init();
	};

protected:
	LPBYTE		m_pBuffer;
	int			m_nBufferSize;
	CTime		m_tmLastUpdate;

public:
	void	Init()
	{
		if(m_pBuffer) delete[]m_pBuffer;
		m_pBuffer=NULL;
		m_nBufferSize=0;
	};

	bool	Append(LPBYTE pAppend, int nSize)
	{
		m_tmLastUpdate = CTime::GetCurrentTime();

		int new_size = m_nBufferSize + nSize;
		LPBYTE new_buff = new BYTE[new_size+1];
		if(new_buff == NULL) return false;
		::ZeroMemory(new_buff, new_size+1);
		if(m_pBuffer)
		{
			memcpy(new_buff, m_pBuffer, m_nBufferSize);
		}
		memcpy(new_buff+m_nBufferSize, pAppend, nSize);
		if(m_pBuffer) delete[]m_pBuffer;
		m_pBuffer = new_buff;
		m_nBufferSize = new_size;
		return true;
	};

	bool	Pop(int nSize, LPBYTE& pData)
	{
		if(nSize == m_nBufferSize)
		{
			pData = m_pBuffer;
			m_pBuffer = NULL;
			m_nBufferSize = 0;
			return true;
		}

		int new_size = m_nBufferSize - nSize;
		LPBYTE new_buff = new BYTE[new_size+1];
		if(new_buff == NULL) return false;
		::ZeroMemory(new_buff, new_size+1);
		if(m_pBuffer)
		{
			memcpy(new_buff, m_pBuffer+nSize, m_nBufferSize-nSize);
		}
		pData = m_pBuffer;
		m_pBuffer = new_buff;
		m_nBufferSize -= nSize;
		return true;
	}

	LPBYTE	GetBuffer() { return m_pBuffer; };
	int		GetBufferSize() { return m_nBufferSize; };
	void	CheckAndClear(int nLimitSec)
	{
		CTime cur_tm = CTime::GetCurrentTime();
		if( m_nBufferSize > 0 &&
			m_tmLastUpdate.GetTime()+nLimitSec < cur_tm.GetTime() )
		{
			// nLimitSec초 이후 초기화
			Init();
		}
	};
};

////////////////////////////////////////////////
class CProxySocket : public CAsyncSocket
{
protected:
	static DWORD	m_dwLastScreenChanged;

public:
	CProxySocket() { m_pReadBuffer = new BYTE[1024]; };
	virtual ~CProxySocket() { if(m_pReadBuffer) delete[]m_pReadBuffer; };

	HWND		m_hParentWnd;
	CString		m_strPeerName;
	UINT		m_nPeerPort;
	UINT		m_nAcceptPort;

protected:
	CSocketBuffer	m_Buffer;
	LPBYTE		m_pReadBuffer;

	CString		BufferToString(LPBYTE pBuff, int size);

	unsigned long getPid(const char* exename);
	HWND getWHandle(unsigned long pid);
};

