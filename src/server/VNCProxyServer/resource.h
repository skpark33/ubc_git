//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VNCProxyServer.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_VNCPROXYSERVER_FORM         101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_VNCProxyServerTYPE          129
#define IDR_TRAY_MENU                   131
#define IDD_SYSTEM_LOG                  132
#define IDC_LIST_ITEMS                  1000
#define IDC_LIST1                       1001
#define IDC_LIST_SYSTEM_LOG             1001
#define IDS_TOTAL_SERVER                1025
#define IDS_SERVER_ONLY                 1026
#define IDS_VIEWER_ONLY                 1027
#define ID_VIEW_SYSTEMLOG               32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
