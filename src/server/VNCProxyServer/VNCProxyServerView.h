// VNCProxyServerView.h : interface of the CVNCProxyServerView class
//


#pragma once
#include "afxcmn.h"

#include "SortListCtrl.h"
#include "SystemLogDlg.h"


class CVNCProxyServerView : public CFormView
{
protected: // create from serialization only
	CVNCProxyServerView();
	DECLARE_DYNCREATE(CVNCProxyServerView)

public:
	enum{ IDD = IDD_VNCPROXYSERVER_FORM };

	enum { COL_ID=0, COL_SERVER_IP, COL_SERVER_CONN_TIME, COL_SERVER_CONN, COL_VIEWER_IP, COL_VIEWER_CONN_TIME, COL_VIEWER_CONN, COLUMN_COUNT };

// Attributes
public:
	CVNCProxyServerDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CVNCProxyServerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	CSystemLogDlg	m_dlgSystemLog;

public:
	//CListCtrl m_lcItems;
	CSortListCtrl	m_lcItems;

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg LRESULT OnAddSystemLog(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResetListenSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAddStandbySocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAddReadySocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateView(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnShutdownCloseSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDeleteSocket(WPARAM wParam, LPARAM lParam);

	afx_msg void OnUpdateViewSystemlog(CCmdUI *pCmdUI);
	afx_msg void OnViewSystemlog();
};

#ifndef _DEBUG  // debug version in VNCProxyServerView.cpp
inline CVNCProxyServerDoc* CVNCProxyServerView::GetDocument() const
   { return reinterpret_cast<CVNCProxyServerDoc*>(m_pDocument); }
#endif

