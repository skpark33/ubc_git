// ListenSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VNCProxyServer.h"

#include "ListenSocket.h"
#include "VNCSocket.h"


// CListenSocket

CListenSocket::CListenSocket()
:	m_bOpen ( FALSE )
,	m_bRetryConnect ( TRUE )
{
}

CListenSocket::~CListenSocket()
{
}

// CListenSocket 멤버 함수

void CListenSocket::OnAccept(int nErrorCode)
{
	__FUNC_BEGIN__;

	if( nErrorCode )
	{
		CString* msg = new CString();
		msg->Format("OnAccept ERROR !!! (ErrCode=%s)", ::GetLastErrorString(nErrorCode));

		__ERROR__( ((LPCTSTR)*msg) );
		m_bOpen = FALSE;
		Close();

		::PostMessage(m_hParentWnd, WM_ADD_SYSTEM_LOG, (WPARAM)msg, NULL);
		::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, m_nPort, NULL);

		return;
	}
	else
	{
		//ciDEBUG(10, ("OnAccept()") );
	}

	CVNCSocket* p_sock = new CVNCSocket(m_eListenType, m_hParentWnd);

	if( Accept(*p_sock) )
	{
		p_sock->ExtractPeerInfo();

		::PostMessage(m_hParentWnd, WM_ADD_STANDBY_SOCKET, (WPARAM)p_sock, NULL);

		if( m_eListenType == eViewerSocket )
		{
			__DEBUG__( ("Accept Viewer-Socket (%s)", p_sock->GetSocketInfo()) );
			p_sock->SendEx((LPBYTE)"RFB 000.000\0", 12); // send init-packet
		}
		else
		{
			__DEBUG__( ("Accept Server-Socket (%s)", p_sock->GetSocketInfo()) );
		}
	}
	else
	{
		CString* msg = new CString();
		msg->Format("Accept ERROR !!! (ErrCode=%s)", ::GetLastErrorString(nErrorCode));

		__ERROR__( ((LPCTSTR)*msg) );
		m_bOpen = FALSE;
		Close();

		::PostMessage(m_hParentWnd, WM_ADD_SYSTEM_LOG, (WPARAM)msg, NULL);
		::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, m_nPort, NULL);
	}

	CAsyncSocket::OnAccept(nErrorCode);

	__FUNC_END__;
}

void CListenSocket::OnClose(int nErrorCode)
{
	__FUNC_BEGIN__;

	m_bOpen = FALSE;

	if( nErrorCode )
	{
		CString* msg = new CString();
		msg->Format("OnClose ERROR !!! (ErrCode=%s)", ::GetLastErrorString(nErrorCode));

		__ERROR__( ((LPCTSTR)*msg) );

		::PostMessage(m_hParentWnd, WM_ADD_SYSTEM_LOG, (WPARAM)msg, NULL);
	}
	else
	{
		//ciDEBUG(10, ("OnClose()") );
	}

	if( m_bRetryConnect )
	{
		__DEBUG__( ("Retry Listen") );

		// not StopListen -> retry StartListen
		::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, m_nPort, NULL);
	}

	CAsyncSocket::OnClose(nErrorCode);

	__FUNC_END__;
}

void CListenSocket::Init(HWND hParentWnd, UINT nPort, SOCKET_TYPE eType)
{
	__FUNC_BEGIN__;

	if( eType == eViewerSocket )
	{
		__DEBUG__( ("Init Viewer-Listen-Socket(Port=%d)", nPort) );
	}
	else
	{
		__DEBUG__( ("Init Server-Listen-Socket(Port=%d)", nPort) );
	}

	m_hParentWnd = hParentWnd;
	m_nPort = nPort;
	m_eListenType = eType;

	__FUNC_END__;
}

BOOL CListenSocket::StartListen()
{
	__FUNC_BEGIN__;

	if(m_bOpen)
	{
		__DEBUG__( ("Already Listen(Port=%d)", m_nPort) );
		return TRUE;
	}

	if( m_eListenType == eViewerSocket )
	{
		__DEBUG__( ("Create Viewer-Listen-Socket(Port=%d)", m_nPort) );
	}
	else
	{
		__DEBUG__( ("Create Server-Listen-Socket(Port=%d)", m_nPort) );
	}

	m_bRetryConnect = TRUE;

	BOOL ret_val = Create(m_nPort);
	if( ret_val )
	{
		// success to create
		BOOL ret_val = Listen();
		if(ret_val)
		{
			// success to listen
			m_bOpen = TRUE;
		}
		else
		{
			// fail to listen -> retry StartListen
			CString* msg = new CString();
			msg->Format("Listen ERROR !!! (Port=%d, ErrCode=%s)", m_nPort, ::GetLastErrorString(::GetLastError()));

			__ERROR__( ((LPCTSTR)*msg) );
			Close();

			::PostMessage(m_hParentWnd, WM_ADD_SYSTEM_LOG, (WPARAM)msg, NULL);
			::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, NULL, NULL);
		}
	}
	else
	{
		// fail to create -> retry StartListen
		CString* msg = new CString();
		msg->Format("Create ERROR !!! (Port=%d, ErrCode=%s)", m_nPort, ::GetLastErrorString(::GetLastError()));

		__ERROR__( ((LPCTSTR)*msg) );

		::PostMessage(m_hParentWnd, WM_ADD_SYSTEM_LOG, (WPARAM)msg, NULL);
		::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, NULL, NULL);
	}

	__FUNC_END__;

	return ret_val;
}

void CListenSocket::StopListen()
{
	__FUNC_BEGIN__;

	m_bRetryConnect = FALSE;
	m_bOpen = FALSE;
	Close();

	__FUNC_END__;
}
