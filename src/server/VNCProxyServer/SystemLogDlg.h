#pragma once
#include "afxcmn.h"


// CSystemLogDlg 대화 상자입니다.

class CSystemLogDlg : public CDialog
{
	DECLARE_DYNAMIC(CSystemLogDlg)

public:
	CSystemLogDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSystemLogDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SYSTEM_LOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CSize	m_sizeInit;
	int		m_nCount;

public:

	afx_msg void OnSize(UINT nType, int cx, int cy);

	CListCtrl	m_lcSystemLog;

	void	AddSystemLog(LPCTSTR lpszLog);

};
