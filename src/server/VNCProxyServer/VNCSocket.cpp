// VNCSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"

#include "VNCProxyServer.h"
#include "VNCSocket.h"

#include "MainFrm.h"


#define		MAX_BUFFER_SIZE		(128*1024)	// 128kbyte

// CVNCSocket

CCriticalSection CVNCSocket::m_csDestroySocket;
CMapStringToPtr CVNCSocket::m_mapDestroySocket;
BOOL* CVNCSocket::m_pRunThread = NULL;


CVNCSocket::CVNCSocket(SOCKET_TYPE eType, HWND hParentWnd)
:	m_eSocketType ( eType )
,	m_hParentWnd ( hParentWnd )
,	m_nID ( MAXLONG )
,	m_bInitialized ( false )
,	m_nBufferLen ( 0 )
,	m_pLinkSocket ( NULL )
,	m_dwTimeStamp ( 0 )
,	m_strPeerIP ( "" )
,	m_nPeerPort ( 0 )
,	m_strInfo ( "" )
{
	m_pBuffer = new BYTE[MAX_BUFFER_SIZE];
	::ZeroMemory(m_pBuffer, MAX_BUFFER_SIZE);
}

CVNCSocket::~CVNCSocket()
{
	if(m_pBuffer) delete[]m_pBuffer;
}


// CVNCSocket 멤버 함수

void CVNCSocket::OnConnect(int nErrorCode)
{
	CAsyncSocket::OnConnect(nErrorCode);
}

void CVNCSocket::OnClose(int nErrorCode)
{
	__DEBUG__( ("Close(%s)", GetSocketInfo(nErrorCode)) );

	::PostMessage(m_hParentWnd, WM_CLOSE_SOCKET, (WPARAM)this, NULL);

	CAsyncSocket::OnClose(nErrorCode);
}

CString ByteArrayToString(LPBYTE pData, int len)
{
	if(pData == NULL || len == 0)
		return "No Data";

	CString ret_val = "";
	CString str_tmp;

	for(int i=0; i<len; i++)
	{
		BYTE byte = pData[i];
		if( 32 <= byte || byte < 127 )
			ret_val.AppendChar((char)byte);
		else
			ret_val += " ";

		str_tmp.Format("(%02X) ", byte);
		ret_val += str_tmp;
	}

	return ret_val;
}

void CVNCSocket::OnReceive(int nErrorCode)
{
	//BYTE buf[1024];
	int nRead = Receive(m_pBuffer+m_nBufferLen, MAX_BUFFER_SIZE-m_nBufferLen);
	//ciDEBUG(10, ("Receive (0x%08X) ReadSize=%d", this, nRead) );

	//TRACE("%d,", nRead);

	//if(m_eSocketType == eServerSocket)// && (nRead==4 || nRead==274) )
	//	TRACE("Server Read(Size:%d) : %s\r\n", m_nBufferLen+nRead, ByteArrayToString(m_pBuffer, m_nBufferLen+nRead));
	//else
	//	TRACE("Client Read(Size:%d) : %s\r\n", m_nBufferLen+nRead, ByteArrayToString(m_pBuffer, m_nBufferLen+nRead));

	switch (nRead)
	{
	case 0:
		// read nothing -> socket is closed
		{
			__WARN__( ("Socket Closed !!! (%s)", GetSocketInfo()) );
			::PostMessage(m_hParentWnd, WM_CLOSE_SOCKET, (WPARAM)this, NULL);
		}
		break;

	case SOCKET_ERROR:
		{
			DWORD err_code = GetLastError();
			if ( err_code != WSAEWOULDBLOCK )
			{
				// socket error occurred --> close socket
				__WARN__( ("Receive Error !!! (%s)", GetSocketInfo(err_code)) );
				::PostMessage(m_hParentWnd, WM_CLOSE_SOCKET, (WPARAM)this, NULL);
			}
		}
		break;

	default:
		m_nBufferLen += nRead;
		if( m_bInitialized == false )
		{
			PacketAnalysis();

			if(m_bInitialized) FlushBuffer();
		}
		else
		{
			LOCK(m_cs);
			if(m_pLinkSocket)
			{
				m_pLinkSocket->SendEx(m_pBuffer, m_nBufferLen);
			}
			m_nBufferLen = 0;
		}
		break;
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

void CVNCSocket::PacketAnalysis()
{
	if( m_nBufferLen < 4 ) return;

	if( m_nID == MAXLONG )
	{
		if( ::memcmp(m_pBuffer, "ID:", 3) != 0 )
		{
			__WARN__( ("Wrong Packet !!! (%s, Packet=%s)", GetSocketInfo(), ByteArrayToString(m_pBuffer,3)) );
			::PostMessage(m_hParentWnd, WM_CLOSE_SOCKET, (WPARAM)this, NULL);
			return;
		}

		bool find_end_of_id = false;
		int i=3;
		for( ; i<m_nBufferLen; i++ ) // ID:xxxxx\0RFB 000.000\n (len=21) , break at i=8
		{
			if( m_pBuffer[i] < '0' || '9' < m_pBuffer[i] )
			{
				find_end_of_id = true;
				break;
			}
		}

		if( find_end_of_id )
		{
			m_nID = atoi((LPCSTR)m_pBuffer+3);
			m_strInfo.Format("0x%08X, Peer=%s:%d, ID=%d", this, m_strPeerIP, m_nPeerPort, m_nID);

			if( m_eSocketType == eViewerSocket )
			{
				//m_bInitialized = true;
				::PostMessage(m_hParentWnd, WM_ADD_READY_SOCKET, (WPARAM)this, NULL);
			}
		}
	}

	if( m_nID != MAXLONG && m_nBufferLen >= 12 )
	{
		LPBYTE pdata = m_pBuffer+m_nBufferLen-12;

		if( ::memcmp(m_pBuffer+m_nBufferLen-12, "RFB ", 4) == 0 )
		{
			m_bInitialized = true;

			LPBYTE buf = new BYTE[MAX_BUFFER_SIZE];
			::ZeroMemory(buf, MAX_BUFFER_SIZE);

			memcpy(buf, m_pBuffer+m_nBufferLen-12, 12);
			m_nBufferLen = 12;
			memcpy(m_pBuffer, buf, m_nBufferLen);

			delete[]buf;
		
			::PostMessage(m_hParentWnd, WM_ADD_READY_SOCKET, (WPARAM)this, NULL);
		}
	}
}

void CVNCSocket::SetLinkSocket(CVNCSocket* pSock)
{
	LOCK(m_cs);

	m_pLinkSocket = pSock;
}

BOOL CVNCSocket::SendEx(LPBYTE pData, int len)
{
	//if(m_eSocketType == eServerSocket)
	//	TRACE("Server Send:%s\r\n", ByteArrayToString(pData, len));
	//else
	//	TRACE("Client Send:%s\r\n", ByteArrayToString(pData, len));

	DWORD start_tick = ::GetTickCount();
	DWORD latest_tick = start_tick;
	while(len > 0 && GetTickDiff(start_tick, latest_tick) < 1000 )
	{
		latest_tick = ::GetTickCount();
		int send_len = Send(pData, len);
		if( send_len == SOCKET_ERROR )
		{
			DWORD err_code = ::GetLastError();
			if( err_code != WSAEWOULDBLOCK )
			{
				__WARN__( ("Send Error !!! (%s)", GetSocketInfo(err_code)) );
				::PostMessage(m_hParentWnd, WM_CLOSE_SOCKET, (WPARAM)this, NULL);
				return FALSE;
			}

			::Sleep(0);
			continue;
		}
		pData += send_len;
		len -= send_len;
	}

	if(GetTickDiff(start_tick, latest_tick) >= 1000)
	{
		__WARN__( ("Send Timeout !!! (%s)", GetSocketInfo()) );
		::PostMessage(m_hParentWnd, WM_CLOSE_SOCKET, (WPARAM)this, NULL);
	}

	return TRUE;
}

BOOL CVNCSocket::FlushBuffer()
{
	LOCK(m_cs);

	if( m_bInitialized && m_pLinkSocket!=NULL && m_nBufferLen>0 )
	{
		m_pLinkSocket->SendEx(m_pBuffer, m_nBufferLen);
		m_nBufferLen = 0;
		return TRUE;
	}

	return FALSE;
}

LPCTSTR CVNCSocket::GetSocketInfo()
{
	if( m_strInfo.GetLength() == 0 )
		m_strInfo.Format("0x%08X, Peer=%s:%d, ID=NoID", this, m_strPeerIP, m_nPeerPort);
	return m_strInfo;
}

LPCTSTR CVNCSocket::GetSocketInfo(DWORD dwErrCode)
{
	m_strInfoEx.Format("%s, ErrCode=%s", GetSocketInfo(), ::GetLastErrorString(dwErrCode));
	return m_strInfoEx;
}

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

BOOL CVNCSocket::DestroyDeleteSocket(CVNCSocket** sock, bool bDeleteNow)
{
	__FUNC_BEGIN__;

	if( (*sock) == NULL ) return FALSE;

	CString str_sock_info = (*sock)->GetSocketInfo();
	__DEBUG__( ("DestroySocket(%s)", str_sock_info) );

	LOCK(m_csDestroySocket);

	BOOL ret_val = TRUE;

	if( bDeleteNow )
	{
		try
		{
			(*sock)->ShutDown();
			(*sock)->Close();
		}
		catch(...)
		{
			__WARN__( ("Socket Shutdown/Close Error !!! (%s)", str_sock_info) );
		}

		try
		{
			delete (*sock);
		}
		catch(...)
		{
			__WARN__( ("Socket Delete Error !!! (%s)", str_sock_info) );
		}
	}
	else
	{
		((CMainFrame*)::AfxGetMainWnd())->GetActiveView()->PostMessage(WM_SHUTDOWN_CLOSE_SOCKET, (WPARAM)*sock);

		CString str_id;
		str_id.Format("%08X", (*sock));

		(*sock)->SetTimeStamp();

		CVNCSocket* tmp_sock;
		if( m_mapDestroySocket.Lookup(str_id, (void*&)tmp_sock) == FALSE )
		{
			m_mapDestroySocket.SetAt(str_id, *sock);
		}
		else
		{
			ret_val = FALSE;
		}
	}

	(*sock) = NULL;

	__FUNC_END__;

	return ret_val;
}

UINT CVNCSocket::CheckDestroySocketThread(LPVOID pParam)
{
	BOOL* pRunThread = (BOOL*)pParam;

	UINT count = 0;
	while(*pRunThread)
	{
		//
		::Sleep(10);
		count++;
		if( count % 100 != 0 ) continue; // every 1-sec

		//
		LOCK(m_csDestroySocket);

		DWORD cur_tick = ::GetTickCount();
		POSITION pos = m_mapDestroySocket.GetStartPosition();
		CString key;
		while(pos)
		{
			CVNCSocket* vnc_socket = NULL;
			m_mapDestroySocket.GetNextAssoc( pos, key, (void*&)vnc_socket );
			if(vnc_socket == NULL)
			{
				m_mapDestroySocket.RemoveKey(key);
				continue;
			}

			DWORD diff_time = ::GetTickDiff(vnc_socket->GetTimeStamp(), cur_tick);
			if(diff_time > 10*1000) // delete atfer 10-sec.
			{
				CString str_sock_info = vnc_socket->GetSocketInfo();
				try
				{
					//delete vnc_socket;
					((CMainFrame*)::AfxGetMainWnd())->PostMessage(WM_DELETE_SOCKET, (WPARAM)vnc_socket);
				}
				catch(...)
				{
					__WARN__( ("Socket Delete Error !!! (%s)", str_sock_info) );
				}

				m_mapDestroySocket.RemoveKey(key);
			}
		}
	}

	delete pRunThread;

	return 0;
}

void CVNCSocket::StartCheckThread()
{
	if( m_pRunThread != NULL ) return;

	m_pRunThread = new BOOL;
	*m_pRunThread = TRUE;

	CWinThread* pThread = ::AfxBeginThread(CVNCSocket::CheckDestroySocketThread, (LPVOID)m_pRunThread, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;
	pThread->ResumeThread();
}

void CVNCSocket::CloseCheckThread()
{
	if(m_pRunThread == NULL) return;

	*m_pRunThread = FALSE;
	m_pRunThread = NULL;

	::Sleep(20);
}
