#pragma once


class CSortListCtrl : public CListCtrl
{
public:
	CSortListCtrl();
	virtual ~CSortListCtrl();

	typedef struct
	{
		HWND	hWnd;			// 리스트컨트롤 핸들
		int		nCol;			// 정렬기준 컬럼
		bool	bAscend;		// true=내림차순, false=오름차순
	} SORT_PARAM;

protected:
	DECLARE_MESSAGE_MAP()

	COLORREF	m_rgbDisconTextColor;
	COLORREF	m_rgbDisconBackColor;

	COLORREF	m_rgbServerTextColor;
	COLORREF	m_rgbServerBackColor;

	COLORREF	m_rgbViewerTextColor;
	COLORREF	m_rgbViewerBackColor;

	COLORREF	m_rgbConnTextColor;
	COLORREF	m_rgbConnBackColor;

	int			m_nSortCol;
	bool		m_bAscend;

	static int CALLBACK SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam);

public:
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult);

	BOOL	Sort(int col=-1, bool ascend=true);

};
