// VNCProxyServerDoc.h : interface of the CVNCProxyServerDoc class
//


#pragma once


class CListenSocket;
class CVNCSocket;


class CVNCProxyServerDoc : public CDocument
{
protected: // create from serialization only
	CVNCProxyServerDoc();
	DECLARE_DYNCREATE(CVNCProxyServerDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CVNCProxyServerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:

	static CCriticalSection	m_csCount;
	static int	m_nTotalCount;
	static int	m_nServerConnectCount;
	static int	m_nViewerConnectCount;

protected:
	// Generated message map functions

	DECLARE_MESSAGE_MAP()

	CListenSocket*		m_pServerListenSocket;
	CListenSocket*		m_pViewerListenSocket;

	static CCriticalSection	m_csStandbySocket;
	static CMapStringToPtr	m_mapStandbySocket;	// obj-address <--> socket-obj

	static CCriticalSection	m_csVNCItem;
	static CMapStringToPtr	m_mapVNCItem;	// ID <--> VNC_ITEM

	void		SendUpdateMessage(VNC_ITEM* pItem);

	BOOL*		m_pRunStandbyThread;
	BOOL*		m_pRunReadyThread;
	void		StartCheckThread();
	void		CloseCheckThread();
	static UINT CheckStandbySocketThread(LPVOID pParam);
	static UINT CheckReadySocketThread(LPVOID pParam);

public:

	BOOL		StartListenSocket(HWND hParentWnd);
	BOOL		StopListenSocket();

	BOOL		AddStandbySocket(CVNCSocket* pNewSocket);
	BOOL		AddReadySocket(CVNCSocket* pNewSocket);
	BOOL		CloseSocket(CVNCSocket* pNewSocket);

};


