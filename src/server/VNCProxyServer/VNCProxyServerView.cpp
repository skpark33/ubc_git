// VNCProxyServerView.cpp : implementation of the CVNCProxyServerView class
//

#include "stdafx.h"
#include "VNCProxyServer.h"

#include "MainFrm.h"
#include "VNCProxyServerDoc.h"
#include "VNCProxyServerView.h"

#include "VNCSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



#define		TIMER_CHECK_LOG_FILE_ID				(1025)
#define		TIMER_CHECK_LOG_FILE_TIME			(60*1000)		// 1-min

#define		TIMER_RESET_LISTEN_SOCKET_ID		(1026)
#define		TIMER_RESET_LISTEN_SOCKET_TIME		(5*1000)		// 5-sec


// CVNCProxyServerView

IMPLEMENT_DYNCREATE(CVNCProxyServerView, CFormView)

BEGIN_MESSAGE_MAP(CVNCProxyServerView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_MESSAGE(WM_ADD_SYSTEM_LOG, OnAddSystemLog)
	ON_MESSAGE(WM_RESET_LISTEN_SOCKET, OnResetListenSocket)
	ON_MESSAGE(WM_ADD_STANDBY_SOCKET, OnAddStandbySocket)
	ON_MESSAGE(WM_ADD_READY_SOCKET, OnAddReadySocket)
	ON_MESSAGE(WM_CLOSE_SOCKET, OnCloseSocket)
	ON_MESSAGE(WM_UPDATE_VIEW, OnUpdateView)
	ON_MESSAGE(WM_SHUTDOWN_CLOSE_SOCKET, OnShutdownCloseSocket)
	ON_MESSAGE(WM_DELETE_SOCKET, OnDeleteSocket)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SYSTEMLOG, &CVNCProxyServerView::OnUpdateViewSystemlog)
	ON_COMMAND(ID_VIEW_SYSTEMLOG, &CVNCProxyServerView::OnViewSystemlog)
END_MESSAGE_MAP()


// CVNCProxyServerView construction/destruction

CVNCProxyServerView::CVNCProxyServerView()
	: CFormView(CVNCProxyServerView::IDD)
{
	__FUNC_BEGIN__;
	__FUNC_END__;
}

CVNCProxyServerView::~CVNCProxyServerView()
{
	__FUNC_BEGIN__;
	__FUNC_END__;
}

void CVNCProxyServerView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_ITEMS, m_lcItems);
}

BOOL CVNCProxyServerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CVNCProxyServerView::OnInitialUpdate()
{
	__FUNC_BEGIN__;

	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	SetScrollSizes(MM_TEXT, CSize(1,1));

	m_dlgSystemLog.Create(IDD_SYSTEM_LOG, this);
	m_dlgSystemLog.ShowWindow(SW_HIDE);

	m_lcItems.SetExtendedStyle(m_lcItems.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcItems.InsertColumn(COL_ID,				"ID", 0, 60);
	m_lcItems.InsertColumn(COL_SERVER_IP,		"VNC Server IP", 0, 115);
	m_lcItems.InsertColumn(COL_SERVER_CONN_TIME,"Server Connect Time", 0, 145);
	m_lcItems.InsertColumn(COL_SERVER_CONN,		"Connect", LVCFMT_CENTER, 85);
	m_lcItems.InsertColumn(COL_VIEWER_IP,		"VNC Viewer IP", 0, 115);
	m_lcItems.InsertColumn(COL_VIEWER_CONN_TIME,"Viewer Connect Time", 0, 145);
	m_lcItems.InsertColumn(COL_VIEWER_CONN,		"Connect", LVCFMT_CENTER, 85);

	GetDocument()->StartListenSocket(GetSafeHwnd());

	SetTimer(TIMER_CHECK_LOG_FILE_ID, TIMER_CHECK_LOG_FILE_TIME, NULL);

	__FUNC_END__;
}


// CVNCProxyServerView diagnostics

#ifdef _DEBUG
void CVNCProxyServerView::AssertValid() const
{
	CFormView::AssertValid();
}

void CVNCProxyServerView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CVNCProxyServerDoc* CVNCProxyServerView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CVNCProxyServerDoc)));
	return (CVNCProxyServerDoc*)m_pDocument;
}
#endif //_DEBUG


// CVNCProxyServerView message handlers

void CVNCProxyServerView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if( GetSafeHwnd() && m_lcItems.GetSafeHwnd() )
	{
		m_lcItems.MoveWindow(0, 0, cx, cy);
	}
}

void CVNCProxyServerView::OnTimer(UINT_PTR nIDEvent)
{
	__FUNC_BEGIN__;
	__DEBUG__( ("nIDEvent(%d)", nIDEvent) );

	switch( nIDEvent )
	{
	case TIMER_CHECK_LOG_FILE_ID:
		KillTimer(1025);
		__DAILY_LOG_OPEN__("log\\VNCProxyServer");
		SetTimer(1025, 60*1000, NULL);
		break;

	case TIMER_RESET_LISTEN_SOCKET_ID:
		KillTimer(TIMER_RESET_LISTEN_SOCKET_ID);
		GetDocument()->StartListenSocket(GetSafeHwnd());
		break;
	}

	CFormView::OnTimer(nIDEvent);

	__FUNC_END__;
}

LRESULT CVNCProxyServerView::OnAddSystemLog(WPARAM wParam, LPARAM lParam)
{
	CString* msg = (CString*)wParam;

	m_dlgSystemLog.AddSystemLog(*msg);
	m_dlgSystemLog.ShowWindow(SW_SHOW);

	delete msg;

	return 0;
}

LRESULT CVNCProxyServerView::OnResetListenSocket(WPARAM wParam, LPARAM lParam)
{
	SetTimer(TIMER_RESET_LISTEN_SOCKET_ID, TIMER_RESET_LISTEN_SOCKET_TIME, NULL);
	return 0;
}

LRESULT CVNCProxyServerView::OnAddStandbySocket(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__( ("StandbySocket(0x%08X)", wParam) );

	return GetDocument()->AddStandbySocket((CVNCSocket*)wParam);
}

LRESULT CVNCProxyServerView::OnAddReadySocket(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__( ("ReadySocket(0x%08X)", wParam) );

	return GetDocument()->AddReadySocket((CVNCSocket*)wParam);
}

LRESULT CVNCProxyServerView::OnCloseSocket(WPARAM wParam, LPARAM lParam)
{
	__DEBUG__( ("CloseSocket(0x%08X)", wParam) );

	return GetDocument()->CloseSocket((CVNCSocket*)wParam);
}

LRESULT CVNCProxyServerView::OnUpdateView(WPARAM wParam, LPARAM lParam)
{
//	__FUNC_BEGIN__;

	VNC_ITEM* vnc_item = (VNC_ITEM*)wParam;
	if( vnc_item == NULL )
	{
		__WARN__( ("VNC_ITEM is NULL !!!") );
		return 0;
	}

	CString str_id;
	str_id.Format("%d", vnc_item->nID);

	LVFINDINFO lvfi;
	lvfi.flags = LVFI_STRING;
	lvfi.psz = str_id;

	int idx = m_lcItems.FindItem(&lvfi);
	if( idx < 0 )
	{
		idx = m_lcItems.GetItemCount();
		m_lcItems.InsertItem(idx, str_id);
		m_lcItems.SetItemData(idx, (DWORD)vnc_item);
	}

//	__DEBUG__( ("ID=%s", str_id) );

	CString str_ip;
	UINT n_port;
	CTime tm_conn;
	if( vnc_item->pServerSocket != NULL )
	{
		vnc_item->pServerSocket->GetPeerInfo(str_ip, n_port, tm_conn);
//		__DEBUG__( ("ServerSocket(%s, ConnTime=%s)", vnc_item->pServerSocket->GetSocketInfo(), tm_conn.Format("%Y-%m-%d %H:%M:%S")) );
		m_lcItems.SetItemText(idx, COL_SERVER_IP, str_ip);
		m_lcItems.SetItemText(idx, COL_SERVER_CONN_TIME, tm_conn.Format("%Y-%m-%d %H:%M:%S"));
		m_lcItems.SetItemText(idx, COL_SERVER_CONN, "O");
	}
	else
	{
//		__DEBUG__( ("ServerSocket(NULL)") );
		m_lcItems.SetItemText(idx, COL_SERVER_CONN, "X");
	}

	if( vnc_item->pViewerSocket != NULL )
	{
		vnc_item->pViewerSocket->GetPeerInfo(str_ip, n_port, tm_conn);
//		__DEBUG__( ("ViewerSocket(%s, ConnTime=%s)", vnc_item->pViewerSocket->GetSocketInfo(), tm_conn.Format("%Y-%m-%d %H:%M:%S")) );
		m_lcItems.SetItemText(idx, COL_VIEWER_IP, str_ip);
		m_lcItems.SetItemText(idx, COL_VIEWER_CONN_TIME, tm_conn.Format("%Y-%m-%d %H:%M:%S"));
		m_lcItems.SetItemText(idx, COL_VIEWER_CONN, "O");
	}
	else
	{
//		__DEBUG__( ("ViewerSocket=NULL") );
		m_lcItems.SetItemText(idx, COL_VIEWER_CONN, "X");
	}

	m_lcItems.RedrawItems(idx, idx);

	CCriticalSection& _cs = CVNCProxyServerDoc::m_csCount;

	LOCK(_cs);
	((CMainFrame*)::AfxGetMainWnd())->SetStatusText(
		CVNCProxyServerDoc::m_nTotalCount, 
		CVNCProxyServerDoc::m_nServerConnectCount, 
		CVNCProxyServerDoc::m_nViewerConnectCount
	);

//	__FUNC_END__;

	return 0;
}

LRESULT CVNCProxyServerView::OnShutdownCloseSocket(WPARAM wParam, LPARAM lParam)
{
	__FUNC_BEGIN__;

	CVNCSocket* vnc_sock = (CVNCSocket*)wParam;
	CString str_sock_info = vnc_sock->GetSocketInfo();
	__DEBUG__( ("DestroySocket(%s)", str_sock_info) );

	try
	{
		vnc_sock->ShutDown();
		vnc_sock->Close();
	}
	catch(...)
	{
		__WARN__( ("Socket Shutdown/Close Error !!! (%s)", str_sock_info) );
	}

	__FUNC_END__;

	return 0;
}

LRESULT CVNCProxyServerView::OnDeleteSocket(WPARAM wParam, LPARAM lParam)
{
	__FUNC_BEGIN__;

	CVNCSocket* vnc_sock = (CVNCSocket*)wParam;
	CString str_sock_info = vnc_sock->GetSocketInfo();
	__DEBUG__( ("DeleteSocket(%s)", str_sock_info) );

	try
	{
		delete vnc_sock;
	}
	catch(...)
	{
		__WARN__( ("Socket Delete Error !!! (%s)", str_sock_info) );
	}

	__FUNC_END__;

	return 0;
}

void CVNCProxyServerView::OnUpdateViewSystemlog(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_dlgSystemLog.IsWindowVisible());
}

void CVNCProxyServerView::OnViewSystemlog()
{
	if( m_dlgSystemLog.IsWindowVisible() )
		m_dlgSystemLog.ShowWindow(SW_HIDE);
	else
		m_dlgSystemLog.ShowWindow(SW_SHOW);

}
