// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "VNCProxyServer.h"

#include "MainFrm.h"
#include "VNCProxyServerDoc.h"
#include "VNCProxyServerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_CLOSE_SOCKET, OnCloseSocket)
	ON_MESSAGE(WM_DELETE_SOCKET, OnDeleteSocket)
	ON_MESSAGE(ID_ICON_NOTIFY, OnTrayNotification)
	ON_WM_SYSCOMMAND()
	ON_WM_SIZE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	IDS_TOTAL_SERVER,
	IDS_SERVER_ONLY,
	IDS_VIEWER_ONLY,
	//ID_INDICATOR_CAPS,
	//ID_INDICATOR_NUM,
	//ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | CBRS_TOP //| WS_VISIBLE
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	//m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	//DockControlBar(&m_wndToolBar);

	// Make Tray Icon
	// 투명배경색은 무조건 White(255,255,255)
	static CBitmap tray_bmp;
	static HICON tray_icon = NULL;
	if(tray_bmp.LoadBitmap(IDR_MAINFRAME))
	{
		tray_icon = ::LoadIcon(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME) );
	}

	CString title;
	title.LoadString(AFX_IDS_APP_TITLE);
	m_TrayIcon.Create(this, ID_ICON_NOTIFY, title, tray_icon, IDR_TRAY_MENU);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.style &= (~(FWS_ADDTOTITLE | WS_MAXIMIZEBOX | WS_VISIBLE));

	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers


void CMainFrame::OnClose()
{
	((CVNCProxyServerDoc*)GetActiveView()->GetDocument())->StopListenSocket();

	CFrameWnd::OnClose();
}

LRESULT CMainFrame::OnCloseSocket(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->PostMessage(WM_CLOSE_SOCKET, wParam, lParam);
}

LRESULT CMainFrame::OnDeleteSocket(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->PostMessage(WM_DELETE_SOCKET, wParam, lParam);
}

LRESULT CMainFrame::OnTrayNotification(WPARAM wParam, LPARAM lParam)
{
	//TRACE(_T("%d, %d\r\n"), wParam, lParam);
	if(m_TrayIcon.Enabled())
        return m_TrayIcon.OnTrayNotification(wParam, lParam);

	return 0;
}

void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
	if(nID == SC_CLOSE)
	{
		ShowWindow(SW_HIDE);
		return;
	}

	CFrameWnd::OnSysCommand(nID, lParam);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize(nType, cx, cy);

	if(nType == SIZE_MINIMIZED)
	{
		ShowWindow(SW_HIDE);
		return;
	}
}

void CMainFrame::SetStatusText(int nTotalServer, int nServerOnly, int nViewerOnly)
{
	CString str;

	str.Format("TotalServer:%d", nTotalServer);
	m_wndStatusBar.SetPaneText(1, str);

	str.Format("Server:%d", nServerOnly);
	m_wndStatusBar.SetPaneText(2, str);

	str.Format("Viewer:%d", nViewerOnly);
	m_wndStatusBar.SetPaneText(3, str);
}
