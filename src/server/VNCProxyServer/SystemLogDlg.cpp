// SystemLogDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VNCProxyServer.h"
#include "SystemLogDlg.h"


// CSystemLogDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSystemLogDlg, CDialog)

CSystemLogDlg::CSystemLogDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSystemLogDlg::IDD, pParent)
	, m_sizeInit ( 0, 0 )
	, m_nCount ( 0 )
{
}

CSystemLogDlg::~CSystemLogDlg()
{
}

void CSystemLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SYSTEM_LOG, m_lcSystemLog);
}


BEGIN_MESSAGE_MAP(CSystemLogDlg, CDialog)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CSystemLogDlg 메시지 처리기입니다.

BOOL CSystemLogDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_lcSystemLog.MoveWindow(0, 0, m_sizeInit.cx, m_sizeInit.cy);

	m_lcSystemLog.SetExtendedStyle(m_lcSystemLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcSystemLog.InsertColumn(0, "Count", 0, 0);
	m_lcSystemLog.InsertColumn(1, "Time", 0, 125);
	m_lcSystemLog.InsertColumn(2, "System Log", 0, 375);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSystemLogDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	m_sizeInit.SetSize(cx, cy);

	if( GetSafeHwnd() && m_lcSystemLog.GetSafeHwnd() )
	{
		m_lcSystemLog.MoveWindow(0, 0, cx, cy);
	}
}

void CSystemLogDlg::OnOK()
{
	//CDialog::OnOK();
	ShowWindow(SW_HIDE);
}

void CSystemLogDlg::OnCancel()
{
	//CDialog::OnCancel();
	ShowWindow(SW_HIDE);
}

void CSystemLogDlg::AddSystemLog(LPCTSTR lpszLog)
{
	m_nCount++;

	m_lcSystemLog.SetRedraw(FALSE);

	TCHAR buf[64];
	_stprintf(buf, "%d", m_nCount);
	m_lcSystemLog.InsertItem(0, buf);

	m_lcSystemLog.SetItemText(0, 1, CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S"));
	m_lcSystemLog.SetItemText(0, 2, lpszLog);

	int count = m_lcSystemLog.GetItemCount();
	if( count > 10000 )
	{
		for(int i=count-1; i>9000; i--)
		{
			m_lcSystemLog.DeleteItem(i);
		}
	}

	m_lcSystemLog.SetRedraw(TRUE);
}
