// VNCProxyServerDoc.cpp : implementation of the CVNCProxyServerDoc class
//

#include "stdafx.h"
#include "VNCProxyServer.h"

#include "VNCProxyServerDoc.h"

#include "ListenSocket.h"
#include "VNCSocket.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CCriticalSection CVNCProxyServerDoc::m_csStandbySocket;
CMapStringToPtr CVNCProxyServerDoc::m_mapStandbySocket;

CCriticalSection CVNCProxyServerDoc::m_csVNCItem;
CMapStringToPtr CVNCProxyServerDoc::m_mapVNCItem;

CCriticalSection CVNCProxyServerDoc::m_csCount;
int	CVNCProxyServerDoc::m_nTotalCount = 0;
int	CVNCProxyServerDoc::m_nServerConnectCount = 0;
int	CVNCProxyServerDoc::m_nViewerConnectCount = 0;


// CVNCProxyServerDoc

IMPLEMENT_DYNCREATE(CVNCProxyServerDoc, CDocument)

BEGIN_MESSAGE_MAP(CVNCProxyServerDoc, CDocument)
END_MESSAGE_MAP()


// CVNCProxyServerDoc construction/destruction

CVNCProxyServerDoc::CVNCProxyServerDoc()
:	m_pServerListenSocket ( NULL )
,	m_pViewerListenSocket ( NULL )
,	m_pRunStandbyThread ( NULL )
,	m_pRunReadyThread ( NULL )
{
	__FUNC_BEGIN__;
	__FUNC_END__;
}

CVNCProxyServerDoc::~CVNCProxyServerDoc()
{
	__FUNC_BEGIN__;

	CloseCheckThread();
	CVNCSocket::CloseCheckThread();

	LOCK(m_csStandbySocket);
	LOCK(m_csVNCItem);

	//
	__DEBUG__( ("Clear Standby-Socket") );
	POSITION pos = m_mapStandbySocket.GetStartPosition();
	CString key;
	while( pos )
	{
		CVNCSocket* vnc_socket = NULL;
		m_mapStandbySocket.GetNextAssoc( pos, key, (void*&)vnc_socket );

		if( vnc_socket == NULL ) continue;

		CVNCSocket::DestroyDeleteSocket(&vnc_socket, true);
	}
	m_mapStandbySocket.RemoveAll();

	//
	__DEBUG__( ("Clear Ready-Socket") );
	pos = m_mapVNCItem.GetStartPosition();
	while( pos )
	{
		VNC_ITEM* item = NULL;
		m_mapVNCItem.GetNextAssoc( pos, key, (void*&)item );

		if( item == NULL ) continue;

		if( item->pServerSocket != NULL )
		{
			CVNCSocket::DestroyDeleteSocket(&(item->pServerSocket), true);
		}
		if( item->pViewerSocket != NULL )
		{
			CVNCSocket::DestroyDeleteSocket(&(item->pViewerSocket), true);
		}
		delete item;
	}
	m_mapVNCItem.RemoveAll();

	__FUNC_END__;
}

BOOL CVNCProxyServerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


// CVNCProxyServerDoc serialization

void CVNCProxyServerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CVNCProxyServerDoc diagnostics

#ifdef _DEBUG
void CVNCProxyServerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CVNCProxyServerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CVNCProxyServerDoc commands

BOOL CVNCProxyServerDoc::StartListenSocket(HWND hParentWnd)
{
	__FUNC_BEGIN__;

	StartCheckThread();
	CVNCSocket::StartCheckThread();

	//
	BOOL ret_val1 = TRUE;
	if( m_pServerListenSocket == NULL )
	{
		__DEBUG__( ("Create Listen Socket (Server)") );
		m_pServerListenSocket = new CListenSocket();
		m_pServerListenSocket->Init(hParentWnd, 5500, eServerSocket);
	}

	if( m_pServerListenSocket->IsOpened() )
	{
		__WARN__( ("Already exist Listen Socket (Server) !!!") );
	}
	else
		ret_val1 = m_pServerListenSocket->StartListen();

	//
	BOOL ret_val2 = TRUE;
	if( m_pViewerListenSocket == NULL )
	{
		__DEBUG__( ("Create Listen Socket (Viewer)") );
		m_pViewerListenSocket = new CListenSocket();
		m_pViewerListenSocket->Init(hParentWnd, 5901, eViewerSocket);
	}

	if( m_pViewerListenSocket->IsOpened() )
	{
		__WARN__( ("Already exist Listen Socket (Viewer) !!!") );
	}
	else
		ret_val1 = m_pViewerListenSocket->StartListen();

	//
	__FUNC_END__;

	if( ret_val1 && ret_val2 ) return TRUE;

	return FALSE;
}

BOOL CVNCProxyServerDoc::StopListenSocket()
{
	__FUNC_BEGIN__;

	if( m_pServerListenSocket != NULL )
	{
		__DEBUG__( ("Close Listen Socket (Server)") );
		m_pServerListenSocket->ShutDown();
		m_pServerListenSocket->Close();
		delete m_pServerListenSocket;
		m_pServerListenSocket = NULL;
	}
	else
	{
		__WARN__( ("Listen Socket (Server) is NULL !!!") );
	}

	if( m_pViewerListenSocket != NULL )
	{
		__DEBUG__( ("Close Listen Socket (Viewer)") );
		m_pViewerListenSocket->ShutDown();
		m_pViewerListenSocket->Close();
		delete m_pViewerListenSocket;
		m_pViewerListenSocket = NULL;
	}
	else
	{
		__WARN__( ("Listen Socket (Viewer) is NULL !!!") );
	}

	__FUNC_END__;

	return TRUE;
}

BOOL CVNCProxyServerDoc::AddStandbySocket(CVNCSocket* pNewSocket)
{
	__FUNC_BEGIN__;

	if( pNewSocket == NULL ) return FALSE;

	__DEBUG__( ("Standby-Socket(%s)", pNewSocket->GetSocketInfo()) );

	LOCK(m_csStandbySocket);

	SOCKET_TYPE type = pNewSocket->GetType();

	CString str_addr;
	str_addr.Format("%08X", pNewSocket); // obj-address

	CVNCSocket* tmp_sock = NULL;
	if( m_mapStandbySocket.Lookup(str_addr, (void*&)tmp_sock) )
	{
		__WARN__( ("Already exist Standby-Socket !!! (%s)", pNewSocket->GetSocketInfo()) );

		// already exist same-address-object  ==>  WTH  ==>  delete
		if( CVNCSocket::DestroyDeleteSocket(&pNewSocket) )
		{
			LOCK(m_csCount);
			if( type == eServerSocket ) m_nServerConnectCount--;
			else /*if( type == eViewerSocket )*/ m_nViewerConnectCount--;
		}
		m_mapStandbySocket.RemoveKey(str_addr);

		return FALSE;
	}

	// not exist in map
	m_mapStandbySocket.SetAt(str_addr, pNewSocket);
	pNewSocket->SetTimeStamp();

	LOCK(m_csCount);
	if( type == eServerSocket ) m_nServerConnectCount++;
	else /*if( type == eViewerSocket )*/ m_nViewerConnectCount++;

	__FUNC_END__;

	return TRUE;
}

BOOL CVNCProxyServerDoc::AddReadySocket(CVNCSocket* pNewSocket)
{
	__FUNC_BEGIN__;

	if( pNewSocket == NULL ) return FALSE;

	//
	__DEBUG__( ("Ready-Socket(%s)", pNewSocket->GetSocketInfo()) );

	//
	LOCK(m_csStandbySocket);
	LOCK(m_csVNCItem);

	//
	CString str_addr;
	str_addr.Format("%08X", pNewSocket); // obj-address

	CVNCSocket* tmp_sock = NULL;
	if( m_mapStandbySocket.Lookup(str_addr, (void*&)tmp_sock) == FALSE )
	{
		__WARN__( ("Not exist in Standby-Socket-Map !!! (%s)", pNewSocket->GetSocketInfo()) );

		// not exist object  ==>  WTH
		return FALSE;
	}

	// delete object from standby-map
	m_mapStandbySocket.RemoveKey(str_addr);

	//
	CString str_id;
	str_id.Format("%010d", pNewSocket->GetID());
	VNC_ITEM* vnc_item = NULL;
	if( m_mapVNCItem.Lookup(str_id, (void*&)vnc_item) )
	{
		__DEBUG__( ("Already created ID (%s)", pNewSocket->GetSocketInfo()) );

		// already exist ready-object  ==>  close all

		if( (pNewSocket->GetType() == eServerSocket && vnc_item->pServerSocket != NULL && vnc_item->pServerSocket != pNewSocket ) ||
			(pNewSocket->GetType() == eViewerSocket && vnc_item->pViewerSocket != NULL && vnc_item->pViewerSocket != pNewSocket ) )
		{
			if( vnc_item->pServerSocket != NULL )
			{
				__DEBUG__( ("Already Server-Socket connected (%s)", pNewSocket->GetSocketInfo()) );
				if(vnc_item->pViewerSocket) vnc_item->pViewerSocket->SetLinkSocket(NULL);
				if( CVNCSocket::DestroyDeleteSocket(&(vnc_item->pServerSocket)) )
				{
					LOCK(m_csCount);
					m_nServerConnectCount--;
				}
			}
			if( vnc_item->pViewerSocket != NULL )
			{
				__DEBUG__( ("Already Viewer-Socket connected (%s)", pNewSocket->GetSocketInfo()) );
				if(vnc_item->pServerSocket) vnc_item->pServerSocket->SetLinkSocket(NULL);
				if( CVNCSocket::DestroyDeleteSocket(&(vnc_item->pViewerSocket)) )
				{
					LOCK(m_csCount);
					m_nViewerConnectCount--;
				}
			}
		}
	}
	else
	{
		__DEBUG__( ("Create New ID (%s)", pNewSocket->GetSocketInfo()) );

		// not exist  ==>  create new
		vnc_item = new VNC_ITEM;
		::ZeroMemory(vnc_item, sizeof(VNC_ITEM));
		vnc_item->nID = pNewSocket->GetID();

		m_mapVNCItem.SetAt(str_id, vnc_item);

		LOCK(m_csCount);
		m_nTotalCount++;
	}

	//
	if( pNewSocket->GetType() == eServerSocket )
	{
		__DEBUG__( ("New Server-Socket connect (%s)", pNewSocket->GetSocketInfo()) );

		if(vnc_item->pViewerSocket) vnc_item->pViewerSocket->SetLinkSocket(pNewSocket);
		pNewSocket->SetLinkSocket(vnc_item->pViewerSocket);
		vnc_item->pServerSocket = pNewSocket;
	}
	else //if( pNewSocket->GetType() == eViewerSocket )
	{
		__DEBUG__( ("New Viewer-Socket connect (%s)", pNewSocket->GetSocketInfo()) );

		if(vnc_item->pServerSocket) vnc_item->pServerSocket->SetLinkSocket(pNewSocket);
		pNewSocket->SetLinkSocket(vnc_item->pServerSocket);
		vnc_item->pViewerSocket = pNewSocket;
	}
	vnc_item->dwLastUpdate = ::GetTickCount();

	//
	if( vnc_item->pViewerSocket != NULL && vnc_item->pServerSocket != NULL )
	{
		__DEBUG__( ("Server/Client connect each other (%s)", pNewSocket->GetSocketInfo()) );

		vnc_item->pServerSocket->FlushBuffer();
	}

	// sendmessage update-msg to view
	SendUpdateMessage(vnc_item);

	__FUNC_END__;

	return TRUE;
}

BOOL CVNCProxyServerDoc::CloseSocket(CVNCSocket* pNewSocket)
{
	__FUNC_BEGIN__;

	if( pNewSocket == NULL ) return FALSE;

	__DEBUG__( ("Close-Socket(%s)", pNewSocket->GetSocketInfo()) );

	LOCK(m_csStandbySocket);
	LOCK(m_csVNCItem);

	// find from standby-map  ==>  delete
	CString str_addr;
	str_addr.Format("%08X", pNewSocket); // obj-address

	CVNCSocket* tmp_sock = NULL;
	if( m_mapStandbySocket.Lookup(str_addr, (void*&)tmp_sock) || pNewSocket->GetID() == MAXLONG )
	{
		__DEBUG__( ("Find in Standy-Socket-Map (%s)", pNewSocket->GetSocketInfo()) );

		// find or not init  ==>  delete
		SOCKET_TYPE type = pNewSocket->GetType();
		if( CVNCSocket::DestroyDeleteSocket(&tmp_sock) )
		{
			LOCK(m_csCount);
			if( type == eServerSocket ) m_nServerConnectCount--;
			else /*if( type == eViewerSocket )*/ m_nViewerConnectCount--;
		}
		m_mapStandbySocket.RemoveKey(str_addr);
		return TRUE;
	}

	//
	CString str_id;
	str_id.Format("%010d", pNewSocket->GetID());
	VNC_ITEM* vnc_item = NULL;
	if( m_mapVNCItem.Lookup(str_id, (void*&)vnc_item) )
	{
		__DEBUG__( ("Find in Ready-Socket-Map (%s)", pNewSocket->GetSocketInfo()) );

		// find  ==>  delete all
		if( vnc_item->pServerSocket != NULL )
		{
			__DEBUG__( ("Exist connected Server-Socket (%s)", vnc_item->pServerSocket->GetSocketInfo()) );
			if(vnc_item->pViewerSocket) vnc_item->pViewerSocket->SetLinkSocket(NULL);
			if( CVNCSocket::DestroyDeleteSocket(&(vnc_item->pServerSocket)) )
			{
				LOCK(m_csCount);
				m_nServerConnectCount--;
			}
		}
		if( vnc_item->pViewerSocket != NULL )
		{
			__DEBUG__( ("Exist connected Viewer-Socket (%s)", vnc_item->pViewerSocket->GetSocketInfo()) );
			if(vnc_item->pServerSocket) vnc_item->pServerSocket->SetLinkSocket(NULL);
			if( CVNCSocket::DestroyDeleteSocket(&(vnc_item->pViewerSocket)) )
			{
				LOCK(m_csCount);
				m_nViewerConnectCount--;
			}
		}

		// sendmessage update-msg to view
		SendUpdateMessage(vnc_item);

		return TRUE;
	}

	__WARN__( ("Not exist socket in Standby/Ready-Socket-map !!!") );

	return FALSE;
}

void CVNCProxyServerDoc::SendUpdateMessage(VNC_ITEM* pItem)
{
	POSITION pos = GetFirstViewPosition();
	while( pos != NULL )
	{
		CView* pView = GetNextView(pos);
		pView->PostMessage(WM_UPDATE_VIEW, (WPARAM)pItem, NULL);
	}   
}

void CVNCProxyServerDoc::StartCheckThread()
{
	__FUNC_BEGIN__;

	if( m_pRunStandbyThread == NULL )
	{
		__DEBUG__( ("Create new CheckStandbySocketThread") );

		m_pRunStandbyThread = new BOOL;
		*m_pRunStandbyThread = TRUE;

		CWinThread* pThread = ::AfxBeginThread(CVNCProxyServerDoc::CheckStandbySocketThread, (LPVOID)m_pRunStandbyThread, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		pThread->m_bAutoDelete = TRUE;
		pThread->ResumeThread();
	}
	else
	{
		__WARN__( ("Already exist CheckStandbySocketThread !!!") );
	}

	if( m_pRunReadyThread == NULL )
	{
		__DEBUG__( ("Create new CheckReadySocketThread") );

		m_pRunReadyThread = new BOOL;
		*m_pRunReadyThread = TRUE;

		CWinThread* pThread = ::AfxBeginThread(CVNCProxyServerDoc::CheckReadySocketThread, (LPVOID)m_pRunReadyThread, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		pThread->m_bAutoDelete = TRUE;
		pThread->ResumeThread();
	}
	else
	{
		__WARN__( ("Already exist CheckReadySocketThread !!!") );
	}

	__FUNC_END__;
}

void CVNCProxyServerDoc::CloseCheckThread()
{
	__FUNC_BEGIN__;

	if( m_pRunStandbyThread != NULL )
	{
		__DEBUG__( ("Close CheckStandbySocketThread") );

		*m_pRunStandbyThread = FALSE;
		m_pRunStandbyThread = NULL;
	}
	else
	{
		__WARN__( ("Not exist CheckStandbySocketThread !!!") );
	}

	if( m_pRunReadyThread != NULL )
	{
		__DEBUG__(("Close CheckReadySocketThread"));

		*m_pRunReadyThread = FALSE;
		m_pRunReadyThread = NULL;
	}
	else
	{
		__WARN__( ("Not exist CheckReadySocketThread !!!") );
	}

	::Sleep(20);

	__FUNC_END__;
}

UINT CVNCProxyServerDoc::CheckStandbySocketThread(LPVOID pParam)
{
	__FUNC_BEGIN__;

	BOOL* pRunThread = (BOOL*)pParam;

	UINT count = 0;
	while( *pRunThread )
	{
		//
		::Sleep(10);
		count++;
		if( count % 500 != 0 ) continue; // check every 5-sec

		__DEBUG__( ("Start Check Standby-Socket-Map") );

		//
		CCriticalSection& cs = CVNCProxyServerDoc::m_csStandbySocket;
		LOCK(cs);

		DWORD cur_tick = ::GetTickCount();
		POSITION pos = CVNCProxyServerDoc::m_mapStandbySocket.GetStartPosition();
		CString key;
		while( pos )
		{
			CVNCSocket* vnc_socket = NULL;
			CVNCProxyServerDoc::m_mapStandbySocket.GetNextAssoc( pos, key, (void*&)vnc_socket );
			if( vnc_socket == NULL )
			{
				CVNCProxyServerDoc::m_mapStandbySocket.RemoveKey(key);
				continue;
			}

			DWORD diff_time = ::GetTickDiff(vnc_socket->GetTimeStamp(), cur_tick);
			if( diff_time > 2*60*1000 ) // delete atfer 2-min by force.
			{
				SOCKET_TYPE type = vnc_socket->GetType();
				if( CVNCSocket::DestroyDeleteSocket(&vnc_socket) )
				{
					LOCK(m_csCount);
					if( type == eServerSocket ) m_nServerConnectCount--;
					else /*if( type == eViewerSocket )*/ m_nViewerConnectCount--;
				}
				CVNCProxyServerDoc::m_mapStandbySocket.RemoveKey(key);
			}
		}

		__DEBUG__( ("End Check Standby-Socket-Map") );
	}

	delete pRunThread;

	__FUNC_END__;

	return 0;
}

UINT CVNCProxyServerDoc::CheckReadySocketThread(LPVOID pParam)
{
	__FUNC_BEGIN__;

	BOOL* pRunThread = (BOOL*)pParam;

	UINT count = 0;
	while( *pRunThread )
	{
		//
		::Sleep(10);
		count++;
		if( count % 1000 != 0 ) continue; // check every 10-sec

		__DEBUG__( ("Start Check Ready-Socket-Map") );

		//
		CCriticalSection& cs = CVNCProxyServerDoc::m_csVNCItem;
		LOCK(cs);

		DWORD cur_tick = ::GetTickCount();
		POSITION pos = CVNCProxyServerDoc::m_mapVNCItem.GetStartPosition();
		CString key;
		while( pos )
		{
			VNC_ITEM* vnc_item = NULL;
			CVNCProxyServerDoc::m_mapVNCItem.GetNextAssoc( pos, key, (void*&)vnc_item );
			if( vnc_item == NULL )
			{
				CVNCProxyServerDoc::m_mapVNCItem.RemoveKey(key);
				continue;
			}

			DWORD diff_time = ::GetTickDiff(vnc_item->dwLastUpdate, cur_tick);
			if( ( diff_time > 2*60*1000 ) && 
				( vnc_item->pServerSocket==NULL || vnc_item->pViewerSocket==NULL) ) // delete atfer 2-min.
			{
				//
				if( vnc_item->pServerSocket != NULL )
				{
					__DEBUG__( ("Close Server-Socket (%s)", vnc_item->pServerSocket->GetSocketInfo()) );
					::AfxGetMainWnd()->PostMessage(WM_CLOSE_SOCKET, (WPARAM)vnc_item->pServerSocket, NULL);
				}
				if( vnc_item->pViewerSocket != NULL )
				{
					__DEBUG__( ("Close Viewer-Socket (%s)", vnc_item->pViewerSocket->GetSocketInfo()) );
					::AfxGetMainWnd()->PostMessage(WM_CLOSE_SOCKET, (WPARAM)vnc_item->pViewerSocket, NULL);
				}
			}
		}

		__DEBUG__( ("End Check Ready-Socket-Map") );
	}

	delete pRunThread;

	__FUNC_END__;

	return 0;
}
