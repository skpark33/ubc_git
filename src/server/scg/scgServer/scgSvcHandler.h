#ifndef _scgSvcHandler_h_
#define _scgSvcHandler_h_

#include "ci/libBase/ciListType.h"
#include "ci/libSocket/ciSockSvcHandler.h"
#include "cci/libWrapper/cciEventHandler.h"

class scgSvcHandler : public ciSockSvcHandler
{
public:
	scgSvcHandler();
	scgSvcHandler(const char* pName);
	virtual ~scgSvcHandler();

	virtual int open(void*);
	virtual int close(u_long p_val);
	virtual int svc();
	virtual int handle_close(ACE_HANDLE pHandle = ACE_INVALID_HANDLE,
								ACE_Reactor_Mask pMask = ACE_Event_Handler::ALL_EVENTS_MASK);

protected:
	typedef ciSockSvcHandler inherited;
};

#endif // _spsSvcHandler_h_
