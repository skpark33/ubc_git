
#include <ci/libBase/ciAceType.h>
#include <ace/Profile_Timer.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciXProperties.h>

#include <cci/libUtil/cciPOAUtil.h>
#include <cci/libUtil/cciCorbaCommon.h>
#include <cci/libUtil/cciCorbaMacro.h>
#include <cci/libValue/cciTime.h>
#include <cci/libValue/cciAny.h>
#include <cci/libValue/cciUid.h>
#include <cci/libValue/cciUidList.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciReplyList.h>
#include <cci/libValue/cciEnum.h>
#include <cci/libValue/cciEnumList.h>
#include <cci/libValue/cciValueFactory.h>
#include <cci/libMir/cciClassDefWrapper.h>
#include <cci/libMoUtil/cciMOUtil.h>
#include <cci/libPfUtil/cciDSUtil.h>
#include <cci/libPS/libPSFactory/cciPSFactory.h>
#include <cci/libObject/cciObject.h>

#include "scgWorld.h"
#include "scgAdapter.h"


ciSET_DEBUG(10, "scgWorld");


scgWorld::scgWorld()
{
	ciDEBUG(3, ("scgWorld()"));
}

scgWorld::~scgWorld() {
	ciDEBUG(3, ("~scgWorld()"));
}

ciBoolean scgWorld::init(int& p_argc, char** &p_argv) 
{
	ciDEBUG(5, ("scgWorld::init(%d)", p_argc));    

	if(cciORBWorld::init(p_argc,p_argv) == ciFalse) {
		ciERROR(("cciORBWorld::init() is FAILED"));
		return ciFalse;
	}

	// +w is Default Option
	if ( !ciArgParser::getInstance()->isSet("+w") ) {
		_watcher = ciWatcher::getInstance(_procName.c_str(), stdout);
		_watcher->init();
		_console = new ciConsoleHandler();

		if(ciWorld::hasWatcher() == ciTrue) {
			_watcher->addWatcherCmd("exit", ciWorld::terminate, "Terminate Program");
			_watcher->addWatcherCmd("proc", ciWorld::proc, "Process Infomations");
		}
	}

	//
	ciString ipaddr = "0.0.0.0";
	ciXProperties* prop = ciXProperties::getInstance();
	ciLong port = 0;
	prop->get("COP.SCGW.PORT", port);
	if( port == 0 ) port = 14001;
	scgAdapter::getInstance()->open(ipaddr, port);

	//
	if(_watcher) _watcher->setPrompt("scgServer");

	return ciTrue;
}

ciBoolean scgWorld::fini(long p_finiCode)
{
	ciDEBUG(5, ("scgWorld::fini(%ld)", p_finiCode));

	if ( _watcher != NULL ) {
		delete _watcher;
		_watcher = NULL;
	}

	return cciORBWorld::fini(p_finiCode);
}
