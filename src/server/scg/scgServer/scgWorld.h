#ifndef _SCGWORLD_H_
#define _SCGWORLD_H_

#include <ci/libBase/ciListType.h>
#ifdef _COP_USE_PROPERTIES_
#     include <ci/libConfig/ciProperties.h> 
#else
#     include <ci/libConfig/ciConfig.h> 
#endif
#include <cci/libWrapper/cciEventManager.h>
#include <cci/libWorld/cciORBWorld.h>


// scgWorld class
class scgWorld : public cciORBWorld {
public:

	scgWorld();
	~scgWorld();	

	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode = 0);

};

#endif //_SCGWORLD_H_
