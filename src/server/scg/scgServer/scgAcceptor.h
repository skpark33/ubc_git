#ifndef _scgAcceptor_h_
#define _scgAcceptor_h_

#include <ci/libSocket/ciSockAcceptor.h>

class scgAcceptor : public ciSockAcceptor
{
	public:
		scgAcceptor();
		virtual ~scgAcceptor();
		virtual int make_svc_handler(ciSockSvcHandler*& pSvcHandler);
		//virtual int activate_svc_handler(ciSockSvcHandler* pSvcHandler);
};

#endif // _scgAcceptor_h_
