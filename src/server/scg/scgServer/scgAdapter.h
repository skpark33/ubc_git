#ifndef _scgAdapter_h_
#define _scgAdapter_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciMutex.h>
#include "scgAcceptor.h"

class scgAdapter
{
	public:
		static scgAdapter* getInstance();
		virtual ~scgAdapter();

		ciBoolean	open(ciString& pIpAddress, ciInt pPort);
		void		close();

	protected:
		scgAdapter();

		static scgAdapter *	_instance;
		static ciMutex		_mutex;

		scgAcceptor *		_acceptor;
};

#endif // _scgAdapter_h_
