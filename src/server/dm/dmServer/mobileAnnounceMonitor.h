/*! \class mobileAnnounceMonitor
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _mobileAnnounceMonitor_h_
#define _mobileAnnounceMonitor_h_


#include <ci/libTimer/ciTimer.h>
#include "cci/libValue/cciStringList.h"
#include <list>

class MobileContents {
public:
	MobileContents() { fileSize=0; heightRatio=1;}
	ciString	siteId;
	ciString	userId;
	ciString	contentsName;
	ciString	filename;
	ciString	contentsType;
	ciString	location;
	ciTime		registerTime;
	ciString	fileMD5;
	ciULong		fileSize;
	ciFloat		heightRatio;
	ciStringList	hostList;
};
typedef list<MobileContents*>	MobileContentsList;

class mobileAnnounceMonitor :  public  ciPollable {
public:
	static mobileAnnounceMonitor*	getInstance();
	static void	clearInstance();

	mobileAnnounceMonitor();
	virtual ~mobileAnnounceMonitor() { _clear();}

    virtual void processExpired(ciString name, int counter, int interval);

protected:
	int			_getEvent(ciTime& now);
	int			_clearAnnounce(ciTime& now); 
	int			_getHost();
	bool		_checkIsDone(MobileContents* pInfo);

	bool		_runCLI(ciTime& now, MobileContents* pInfo);
	void		_clear();

	MobileContentsList	_contentsList;

	static mobileAnnounceMonitor* 	_instance; 
	static ciMutex 				_instanceLock;


protected:


};	

#endif //_mobileAnnounceMonitor_h_
