/*! \class smsMon
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _smsMon_h_
#define _smsMon_h_

#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libTimer/ciTimer.h>

class smsMon :  public  ciPollable {
public:
	static smsMon*	getInstance();
	static void	clearInstance();

	smsMon();
	virtual ~smsMon();

	virtual void processExpired(ciString name, int counter, int interval);

	class smsInfo {
	public:
		smsInfo(smsMon* p) : _parent(p) { state = 0;eventType=0;}
		ciBoolean sendIt();
		ciBoolean update();

		unsigned long 	eventIndex;
		int 		eventType;
		ciString 	telno;
		ciString 	message;
		ciString 	eventTime;
		int	 		state;

	protected:
		smsMon* _parent;
	};

	typedef list<smsInfo*>	smsInfoList;

protected:
	static smsMon*	_instance;
	static ciMutex			_instanceLock;

public:
	ciString _home;
	ciString _server;
	ciString _port;
	ciString _db;
	ciString _user;
	ciString _password;

};	

#endif //_smsMon_h_
