/*! \class mgwMon
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _mgwMon_h_
#define _mgwMon_h_

#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libDebug/ciArgParser.h>

#include <cci/libUtil/cciCorbaMacro.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciBoolean.h>
#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciEnum.h>
#include <cci/libValue/cciAny.h>

#include <cci/libObject/cciObject.h>
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciCall.h>

#include <common/libCommon/utvUtil.h>
#include <ci/libTimer/ciTimer.h>

class mgwMon :  public  ciPollable {
public:
	typedef map<ciString,ciStringSet>	SSHOTMAP;

	static mgwMon*	getInstance();
	static void	clearInstance();

	mgwMon();
	virtual ~mgwMon();

	virtual void processExpired(ciString name, int counter, int interval);

	void	initURL();
protected:
	static mgwMon*	_instance;
	static ciMutex			_instanceLock;

	class _SCHEDULE_INFO {
	public:
		ciString			scheduleType;
		ciString			siteId;
		ciString			programId;
		CCI::CCI_StringList	entityList;
		CCI::CCI_EnumList	stateList;
		ciStringList		hostList;
	};
	typedef map<ciString,_SCHEDULE_INFO*>	_PROGRAM_MAP;

	int			_updateComment(ciString& sCode, ciString& sData, ciString& pNo);
	void		_getLastComment(cciReply& reply, ciString& lastComment,ciShort& currentComment);
	ciBoolean	_updateIsProcessed(const char* id);

	ciBoolean	_setComment(const char* mgrId,const char* siteId,const char* programId,	const char* contentsId,	
							const char* sData,const char* lastComment,ciShort currentComment);

	int			_getSchedule(const char* scheduleType,const char* mgrId,const char* siteId,
							const char* programId,const char* contentsId);
	int			_getHostIdList(const char* programId, _SCHEDULE_INFO* ele);

	int			_postEvent();
	void		_clearMap();
	void		_processSMS();

	ciBoolean _insertScreenShotDB();
	ciBoolean _insertScreenShotDB(const char* hostId, const char* createTime, const char* filename);
	ciBoolean _insertLastScreenShotDB(const char* hostId, const char* createTime, const char* filename);
	ciBoolean _deleteScreenShotDB();
	int _getScreenShotFile(SSHOTMAP& fileMap);
	int _getScreenShotFile(const char* siteId, ciStringSet& fileList);

	ciBoolean	doQuery(const char* query);

	cciObject*	_smsObj;
	_PROGRAM_MAP	_map;
	ciStringSet		_contentsIdSet;
	ciString		_url;

};	

#endif //_mgwMon_h_
