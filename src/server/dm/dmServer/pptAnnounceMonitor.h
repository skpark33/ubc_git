/*! \class pptAnnounceMonitor
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _pptAnnounceMonitor_h_
#define _pptAnnounceMonitor_h_


#include <ci/libTimer/ciTimer.h>
#include "cci/libValue/cciStringList.h"
#include <list>

class PPTAnnounce {
public:
	PPTAnnounce() { fileSize=0; _directive = "remove";}
	ciString mgrId;
	ciString siteId;
	ciString announceId;
	ciString creator;
	CCI::CCI_StringList hostIdList;
	ciTime createTime;
	ciTime startTime;
	ciTime endTime;
	ciString comment1;
	ciString comment2;
	ciString bgFile;
	ciULongLong  fileSize;
	ciString fileMD5;

	ciString	_directive;
};
typedef map<ciString, PPTAnnounce*>	PPTAnnounceMap;

class PPTContents {
public:
	PPTContents() { fileSize=0; }
	ciString	announceId;
	ciString	userId;
	ciString	filename;
	ciTime		registerTime;
	ciString	fileMD5;
	ciULong		fileSize;
	ciTime		startTime;
	ciTime		endTime;
};
typedef map<ciString, PPTContents*>	PPTContentsMap;


class pptAnnounceMonitor :  public  ciPollable {
public:
	static pptAnnounceMonitor*	getInstance();
	static void	clearInstance();

	pptAnnounceMonitor();
	virtual ~pptAnnounceMonitor() { _clearEvent(); _clearAnnounce(); }

    virtual void processExpired(ciString name, int counter, int interval);

protected:
	void		_clearEvent();
	void		_clearAnnounce();

	bool		_getHost();
	int			_getCurrentPPTAnnounce(ciTime& now);
	int			_getEvent(ciTime& now);

	bool		_eventToAnnounce(PPTContents* pContents, PPTAnnounce* pAnnounce);
	bool		_runCLI(PPTAnnounce* pInfo);

	bool		_removeAnnounce(ciString& pAnnounceId);
protected:

	PPTContentsMap _contentsMap;
	PPTAnnounceMap _announceMap;
	ciStringList	_hostIdList;
	ciString		_siteId;

	static pptAnnounceMonitor* 	_instance; 
	static ciMutex 				_instanceLock;


};	

#endif //_pptAnnounceMonitor_h_
