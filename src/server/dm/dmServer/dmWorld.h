 /*! \file dmWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _dmWorld_h_
#define _dmWorld_h_

#include <cci/libWorld/cciORBWorld.h> 

class dmWorld : public cciORBWorld {
public:
	dmWorld();
	~dmWorld();
 	
	//[
	//	From aciCallWorld
	//
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);
	//
	//]

protected:	
	
};
		
#endif //_dmWorld_h_
