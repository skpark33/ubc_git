/*! \class externalAnnounceMonitor
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _externalAnnounceMonitor_h_
#define _externalAnnounceMonitor_h_


#include <ci/libTimer/ciTimer.h>
#include <ci/libTimer/ciWinTimer.h>
#include "cci/libValue/cciStringList.h"


class HostInfo {
public:
	HostInfo() : dirtyFlag(false)  {}
	ciString hostId;
	ciString type;
	ciString hostName;
	ciString addr1;
	ciString model;
	ciString description;
	ciString startTime;
	ciString endTime;
	ciString eId;
	ciBoolean	dirtyFlag;
};
typedef map<ciString, HostInfo*>	HostInfoMap;


class externalAnnounceMonitor :  public  ciWinPollable {
public:
	static externalAnnounceMonitor*	getInstance();
	static void	clearInstance();

	externalAnnounceMonitor();
	virtual ~externalAnnounceMonitor() { }

    virtual void processExpired(ciString name, int counter, int interval);

	void setDeathTime(int h,int m) { _deathHour = h; _deathMin=m; }
protected:
	int			_getAllHostType();
	ciString 	_findHostType(ciString& hostId);

	int			_get_new_HostListFrom_WEB_EVENT_TABLE(ciTime& now, ciTime& lastTime, HostInfoMap* outMap, ciStringSet* outSet);
	//int			_get_valid_HostListFrom_WEB_EVENT_TABLE(ciTime& now, ciStringSet* outSet);

	int			_clearAnnounce(ciTime& now, ciStringSet* inHostSet); 

	int			_getHostInfo(HostInfoMap* inHostInfo);
	int			_getHostInfo(ciString& query, const char* type, HostInfoMap* inoutMap);
	bool		_getDirective(const char* key, HostInfo* pInfo, ciStringList& outDirectiveList);

	bool		_runCLI(const char* directive, const char* keyStr, ciTime& now, HostInfo* inHostInfo);
	ciString	_createKey(ciString& hostId,  ciString& type, ciString& eId);
	//bool		_calcTime(ciString& timeStr, int hour,int min, int& outMin);

	void		_getSiteInfo();
	bool		_updateTouchTime(ciString& eId, ciTime& nextTime);
	void		_clear();

	HostInfoMap	_prevMap;

	static externalAnnounceMonitor* 	_instance; 
	static ciMutex 				_instanceLock;


protected:

	ciStringMap		_hostTypeMap;

	ciString	_siteId;
	ciString	_url;
	ciShort			_room_width;	
	ciShort			_room_height;	
	ciShort			_hd_width;	
	ciShort			_hd_height;	
	ciShort			_sd_width;	
	ciShort			_sd_height;	

	ciShort			_deathHour;
	ciShort			_deathMin;

};	

#endif //_externalAnnounceMonitor_h_
