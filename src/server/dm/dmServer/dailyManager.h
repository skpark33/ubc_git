/*! \class dailyManager
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _dailyManager_h_
#define _dailyManager_h_


#include <ci/libTimer/ciTimer.h>
class cciPS;

class dailyManager :  public  ciPollable {


public:
	static dailyManager*	getInstance();
	static void	clearInstance();
	virtual ~dailyManager() ;
	virtual void processExpired(ciString name, int counter, int interval);

protected:
	dailyManager();

	static dailyManager*	_instance;
	static ciMutex			_instanceLock;

	ciBoolean	_deleteOldReservation(ciShort duration);
	ciBoolean	_removeReservation(const char* mgrId, const char* siteId, 
										   const char* hostId, const char* reservationId);
	ciBoolean	_deleteOldAnnounce(ciShort duration);
	ciBoolean	_removeAnnounce(const char* mgrId, const char* siteId,const char* reservationId);

	ciBoolean _deleteOldScreenShotFile(ciShort duration);
	ciBoolean _deleteOldScreenShotFile(const char* subDir,ciShort duration);
	
	ciBoolean	_deleteOldlog(ciShort duration, const char* subDir);
	int			_deleteSubDir(ciShort duration, const char* dirPath);

	ciBoolean	_deleteOldFaultlog(ciShort duration);

	ciBoolean	_deleteTrashCan(ciShort duration);


	ciBoolean _sqlLoadPlayLog(const char* tableName, const char* dirPrefix, ciString& formatfile,const char* url);
#ifdef _COP_MSC_
	ciBoolean _killRepeater();
	ciBoolean _killPmServer();
#endif
	ciBoolean _doHostLog();

	int			_insertCSV(cciPS* aPS,ciString dataFile, const char* tableName, ciString columns,int columnCount);
	ciBoolean	_bulkInsertCSV(cciPS* aPS, ciString dataFile, const char* tableName);


	ciBoolean	_dailyConnectionStat();
	ciBoolean	_getAdditionalValue(int idx, ciString& inval, int order, ciString& outval);


	ciShort			_isTest;

	ciBoolean _updateFailFile( const char* dirPrefix );
	int _charCount(const char* buf, const char deli);
	ciBoolean	_insertLoginLog(ciTime& now);


	ciBoolean _do_failed;
	ciBoolean _do_insert;
	ciBoolean _do_watchsec;
	ciBoolean _do_autoever;
	ciBoolean _do_ubgolf;
	ciString _filter;

	ciString _month;
	ciString _lastMonth;



};	

#endif //_dailyManager_h_
