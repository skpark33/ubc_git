/*! \class mobileAnnounceMonitor
 *  Copyright ⓒ 2002, COP. All rights reserved.
 *
 *  \brief Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/14 19:01:00
 */

#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libBase/ciTime.h>
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciTime.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciFloat.h>
#include <cci/libObject/cciObject.h>

#include <ci/libDebug/ciArgParser.h>
#include <common/libCommon/ubcIni.h>

#if defined (_COP_MSC_)
     #include <windows.h>
     #include <atltime.h>
#endif

#include "mobileAnnounceMonitor.h"

#define  TABLE_NAME		"ubc_android_filecontents"

ciSET_DEBUG(6, "mobileAnnounceMonitor");

mobileAnnounceMonitor* 	mobileAnnounceMonitor::_instance = 0; 
ciMutex 	mobileAnnounceMonitor::_instanceLock;


mobileAnnounceMonitor*	
mobileAnnounceMonitor::getInstance() {
	if(!_instance) {
		ciGuard aGuard(_instanceLock);
		if(!_instance) {
			_instance = new mobileAnnounceMonitor;
		}
	}
	return _instance;
}

void	
mobileAnnounceMonitor::clearInstance() {
	if(_instance) {
		ciGuard aGuard(_instanceLock);
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}

mobileAnnounceMonitor::mobileAnnounceMonitor()
{
}

void
mobileAnnounceMonitor::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG2(5,("processExpired(%s,%d,%d)", name.c_str(), counter, interval));
	
	ciTime now;
	
	// 이벤트 가져오기
	if(0==_getEvent(now)){
		ciERROR(("No Event founded"));
		_clearAnnounce(now); // 하루지난걸 지운다.
		return ;
	}

	//해당 호스트 가져오기
	if(0==_getHost()){
		ciERROR(("No host founded"));
		_clearAnnounce(now); // 하루지난걸 지운다.
		return ;
	}


	// 이벤트를 하나씩 돌면서, cli 를 날린다. (create);
	MobileContentsList::iterator itr;
	for(itr=_contentsList.begin();itr!=_contentsList.end();itr++){
		MobileContents* aInfo = (*itr);
		if(aInfo == 0) continue;
		if(_runCLI(now, aInfo)){
			_checkIsDone(aInfo);
		}
	}
	_clear();
	_clearAnnounce(now); // 하루지난걸 지운다.
	return ;
}

void
mobileAnnounceMonitor::_clear()
{
	// 메모리를 해제한다.
	MobileContentsList::iterator itr;
	for(itr=_contentsList.begin();itr!=_contentsList.end();itr++){
		MobileContents* aInfo = (*itr);
		delete aInfo;
	}
	_contentsList.clear();
}

int
mobileAnnounceMonitor::_getEvent(ciTime& now)
{
	_clear();

	char from[24];
	memset(from,0x00,24);
	sprintf(from,"%04d-%02d-%02d %02d:%02d:00", 
		now.getYear(),now.getMonth(),now.getDay(), 
		now.getHour(),now.getMinute());

	// 최근 1분 데이터중 isDone 값이 check 되지 않은 값을 가져온다. 
	ciTime oneMinuteLater = now + (60);

	char to[24];
	memset(to,0x00,24);
	sprintf(to,"%04d-%02d-%02d %02d:%02d:00", 
		oneMinuteLater.getYear(),oneMinuteLater.getMonth(),oneMinuteLater.getDay(), 
		oneMinuteLater.getHour(),oneMinuteLater.getMinute());


	cciObject aObj(TABLE_NAME);

	cciString* siteId =			(cciString*)aObj.addItem("siteId", new cciString());
	cciString* userId =			(cciString*)aObj.addItem("userId", new cciString());
	cciString* contentsName =			(cciString*)aObj.addItem("contentsName", new cciString());
	cciTime*   registerTime =		(cciTime*)aObj.addItem("registerTime", new cciTime());
	cciString* filename =	(cciString*)aObj.addItem("filename", new cciString());
	cciString* contentsType =	(cciString*)aObj.addItem("contentsType", new cciString());
	cciString* location =	(cciString*)aObj.addItem("location", new cciString());
	cciString* fileMD5 =	(cciString*)aObj.addItem("fileMD5", new cciString());
	cciUnsigned32* fileSize =	(cciUnsigned32*)aObj.addItem("fileSize", new cciUnsigned32());
	cciFloat* heightRatio =	(cciFloat*)aObj.addItem("heightRatio", new cciFloat());

	char query[256];
	sprintf(query,"isDone=0 and registerTime >= CAST('%s' as dateTime) and registerTime < CAST('%s' as dateTime)", from, to);

	ciDEBUG2(1,("_getEvent(%s)",query));

	if(!aObj.get(query)){
		ciERROR(("failed (query=%s)", query));
		return 0;
	}

	int counter=0;
	while(aObj.next()){
		
		MobileContents* aContents = new MobileContents();

		aContents->siteId =  siteId->get();
		aContents->userId =  userId->get();
		aContents->contentsName =  contentsName->get();
		aContents->registerTime =  registerTime->get();
		aContents->filename =  filename->get();
		aContents->contentsType =  contentsType->get();
		aContents->location =  location->get();
		aContents->fileMD5 = fileMD5->get();
		aContents->fileSize = fileSize->get();
		aContents->heightRatio = heightRatio->get();

		_contentsList.push_back(aContents);
		counter++;

	}
	ciDEBUG(1,("%d event founded", counter));
	return counter;
}


bool
mobileAnnounceMonitor::_checkIsDone(MobileContents* pInfo)
{
	cciObject aObj(TABLE_NAME);

	aObj.addItemAsKey("filename", new cciString(pInfo->filename.c_str()));
	aObj.addItem("isDone", new cciInteger16(1));

	ciDEBUG2(1,("_checkIsDone(%s)",pInfo->filename.c_str()));

	if(!aObj.set()){
		ciERROR(("_checkIsDone failed(%s)",pInfo->filename.c_str()));
		return false;
	}
	return true;
}

/*
void
mobileAnnounceMonitor::_getSiteInfo()
{
	// UBCVariables.ini 의 hotel_event 란에서 announce에 필요한 정보를 가져온다.

	const char* prj_home = getenv("PROJECT_HOME");
	if(!prj_home){
		prj_home = _COP_CD("C:\\Project\\ubc");
	}
	ciString file = prj_home;
	file +=  "\\config\\data";
	
	ubcIni aIni("UBCVariables",file.c_str(),"");

	ciString	siteId;
	ciString	url;
	ciShort			room_width=0;	
	ciShort			room_height=0;	
	ciShort			hd_width=0;	
	ciShort			hd_height=0;	
	ciShort			sd_width=0;	
	ciShort			sd_height=0;	

	aIni.get("HOTEL_EVENT","siteId",siteId);
	aIni.get("HOTEL_EVENT","url",url);
	aIni.get("HOTEL_EVENT","room_width",room_width);
	aIni.get("HOTEL_EVENT","room_height",room_height);
	aIni.get("HOTEL_EVENT","hd_width",hd_width);
	aIni.get("HOTEL_EVENT","hd_height",hd_height);
	aIni.get("HOTEL_EVENT","sd_width",sd_width);
	aIni.get("HOTEL_EVENT","sd_height",sd_height);

	if(!siteId.empty())		_siteId   = siteId;
	if(!url.empty())		_url = url;
	if(!room_width)			_room_width = room_width;
	if(!room_height)		_room_height = room_height;	
	if(!hd_width)			_hd_width = hd_width;	
	if(!hd_height)			_hd_height = hd_height;	
	if(!sd_width)			_sd_width = sd_width;	
	if(!sd_height)			_sd_height = sd_height;	

}
*/

int			
mobileAnnounceMonitor::_clearAnnounce(ciTime& now)
{
	// endTime 이 24시간 이상 지난것을 지운다.

	ciTime yesterday = now - 60*60*24;

	cciObject  aObj("UBC_Announce");

	cciString* announceId = (cciString*)aObj.addItem("announceId", new cciString());

	char query[256];
	sprintf(query, "title = 'MobileEvent' and endTime < CAST('%04d-%02d-%02d %02d:%02d:%02d' as datetime)", yesterday.getYear(), yesterday.getMonth(),
		yesterday.getDay(), yesterday.getHour(), yesterday.getMinute(), yesterday.getSecond());

	if(!aObj.get(query)){
		ciERROR(("UBC_Announce get failed"));
		return 0;
	}
	
	ciStringList removeIdList;

	while(aObj.next()){
		const char* aAnnounceId = announceId->get();
		removeIdList.push_back(aAnnounceId);
	}

	int len = removeIdList.size();
	ciDEBUG(1,("%d th remove target founded", len));

	len = 0;
	ciStringList::iterator itr;
	for(itr=removeIdList.begin();itr!=removeIdList.end();itr++){

		ciString aAnnounceId = (*itr);

		cciCall aCall("remove");
		cciEntity aEntity("PM=*/Site=*/Announce=%s", aAnnounceId.c_str());
		aCall.setEntity(aEntity);

		try
		{
			if(!aCall.call()) {
				ciERROR(("delete %s fail", aEntity.toString()));
				continue;
			}
		}
		catch(CORBA::Exception& ex)
		{
			ciERROR((ex._name()));
			continue;
		}

		len++;
	}

	ciDEBUG(1,("%d th remove target removed", len));
	return len;
}

int			
mobileAnnounceMonitor::_getHost()
{
	int counter=0;
	MobileContentsList::iterator itr;
	for(itr=_contentsList.begin();itr!=_contentsList.end();itr++){
		MobileContents* aInfo = (*itr);

		cciObject aObj("UBC_USER");
		aObj.addItemAsKey("siteId", new cciString(aInfo->siteId.c_str()));
		aObj.addItemAsKey("userId", new cciString(aInfo->userId.c_str()));

		cciStringList* hostList = (cciStringList*)aObj.addItem("hostList", new cciStringList());
	
		if(!aObj.get()){
			ciERROR(("get User(%s) HostList failed ", aInfo->userId.c_str()));
			continue;
		}

		aInfo->hostList.clear();

		while(aObj.next()){
			if(hostList && hostList->length() > 0){
				aInfo->hostList = hostList->getStringList();
				counter++;
				ciDEBUG(1,("%s host founded for user %s", hostList->toString(), aInfo->userId.c_str()));
				break;
			}else{
				ciWARN(("%s user does not have host", aInfo->userId.c_str()));
			}
		}
	}
	ciDEBUG(1,("%d th user data founded", counter));
	return counter;
}


bool		
mobileAnnounceMonitor::_runCLI(ciTime& now, MobileContents* pInfo)
{
	char nowStr[24];
	memset(nowStr,0x00,24);
	sprintf(nowStr,"%04d/%02d/%02d %02d:%02d:%02d", now.getYear(),now.getMonth(),now.getDay(), 
													now.getHour(),now.getMinute(),now.getSecond());
	ciTime from = now;
	ciTime to  = now + (60*3);  // 3분간 방송


	ciDEBUG(1,("runCLI(%s,%s,%s)", nowStr,
									pInfo->userId.c_str(), 
									pInfo->filename.c_str()));

	cciCall aCall("create");

	cciEntity aEntity("PM=*/Site=%s/Announce=%s",pInfo->siteId.c_str(), pInfo->contentsName.c_str());
	aCall.setEntity(aEntity);

	aCall.addItem("title", "MobileEvent");
	aCall.addItem("creator", pInfo->userId.c_str());

	CCI::CCI_StringList aHostList;
	ciStringList::iterator itr;
	int i=0;
	for(itr=pInfo->hostList.begin();itr!=pInfo->hostList.end();itr++){
		aHostList.length(i+1);
		aHostList[i] = CORBA::string_dup(itr->c_str());
		i++;
	}
	aCall.addItem("hostIdList", aHostList);

	aCall.addItem("startTime",	from);
	aCall.addItem("endTime",	to);
	

	// 상수들
	aCall.addItem("bgFile", pInfo->filename.c_str());
	aCall.addItem("bgLocation", pInfo->location.c_str());
	aCall.addItem("contentsType", ciShort(1)); // Image
	aCall.addItem("sourceSize", ciShort(1)); // 원본비율
	aCall.addItem("height", (ciShort)60);				// 백분율
	aCall.addItem("width", (ciShort)100);				// 백분율
	aCall.addItem("heightRatio", pInfo->heightRatio);
	aCall.addItem("fileSize", pInfo->fileSize);
	aCall.addItem("fileMD5", pInfo->fileMD5);

	aCall.addItem("posX", ciShort(0));
	aCall.addItem("posY", ciShort(0));
	aCall.addItem("position", ciLong(0));

	aCall.wrap();


	ciDEBUG(1,("Call: %s", aCall.toString()));
	try
	{
		if(!aCall.call()) {
			ciERROR(("%s fail", aCall.toString()));
			return false;
		}
	}
	catch(CORBA::Exception& ex)
	{
		ciERROR((ex._name()));
		return false;
	}
	return true;
}
