/*! \class updateMon
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _updateMon_h_
#define _updateMon_h_

#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libTimer/ciTimer.h>

class updateMon :  public  ciPollable {
public:
	static updateMon*	getInstance();
	static void	clearInstance();

	updateMon();
	virtual ~updateMon();

	virtual void processExpired(ciString name, int counter, int interval);

protected:
	static updateMon*	_instance;
	static ciMutex			_instanceLock;

	ciBoolean _getVersionInfo(ciString& versionFileName, 
								ciString& out_fileprefix,
								ciString& out_version);
	ciBoolean _release(ciString& prefix, ciString& workPath, ciString& oldVersion,
						ciBoolean old_version_exist);

	ciString _serverPath;
	ciString _clientPath;
	ciString _serverVersionFile;
	ciString _clientVersionFile;
	ciString _apknameStr;
	ciString _apkversionStr;
};	

#endif //_updateMon_h_
