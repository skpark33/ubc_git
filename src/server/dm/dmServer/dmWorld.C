/*! \file dmWorld.C
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
*/


#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciConfig.h>
#include <cci/libWrapper/cciEventManager.h>

#include "dailyManager.h"
//#include "serverManager.h"
#include "mgwMon.h"
#include "dmWorld.h"
#include "externalAnnounceMonitor.h"
#include "mobileAnnounceMonitor.h"
#include "pptAnnounceMonitor.h"



ciSET_DEBUG(10, "dmWorld");

dmWorld::dmWorld() {
	ciDEBUG(3, ("dmWorld()"));
}

dmWorld::~dmWorld() {
	ciDEBUG(3, ("~dmWorld()"));
	dailyManager::getInstance()->stopTimer();
}

ciBoolean 
dmWorld::init(int& p_argc, char** &p_argv) 
{
	ciDEBUG(5, ("dmWorld::init(%d)", p_argc));	
	
	if(cciORBWorld::init(p_argc,p_argv) == ciFalse) {
		ciERROR(("cciORBWorld::init() is FAILED"));
		return ciFalse;
	}

	dailyManager* dTimer = dailyManager::getInstance();
	dTimer->initTimer("dailyManager",60*60);
	dTimer->startTimer();

	if(ciArgParser::getInstance()->isSet("+announce")){
		externalAnnounceMonitor* eTimer = externalAnnounceMonitor::getInstance();

		ciString deathTime;
		if(ciArgParser::getInstance()->getArgValue("+deathTime", deathTime)){
			if(deathTime.size() >= 5){
				int deathHour = atoi(deathTime.substr(0,2).c_str());
				int deathMin = atoi(deathTime.substr(3,2).c_str());
				ciWARN(("+deathTime=%d:%d", deathHour,deathMin));
				eTimer->setDeathTime(deathHour,deathMin);
			}
		}

		eTimer->initTimer("externalAnnounceMonitor",15);
		eTimer->startTimer();
	}
	if(ciArgParser::getInstance()->isSet("+mobile_announce")){
		mobileAnnounceMonitor* eTimer = mobileAnnounceMonitor::getInstance();
		eTimer->initTimer("mobileAnnounceMonitor",15);
		eTimer->startTimer();
	}

	if(ciArgParser::getInstance()->isSet("+ppt_announce")){
		pptAnnounceMonitor* eTimer = pptAnnounceMonitor::getInstance();
		eTimer->initTimer("pptAnnounceMonitor",15);
		eTimer->startTimer();
	}

	if(ciArgParser::getInstance()->isSet("+mgw")){
		mgwMon* mTimer = mgwMon::getInstance();
		mTimer->initTimer("mgwMon",60);
		mTimer->startTimer();
	}


	/*
	ciString periodStr;
	if(ciArgParser::getInstance()->getArgValue("+monitor", periodStr)){
		int period = atoi(periodStr.c_str());
		if(period < 5) period = 5;
		ciDEBUG(1,("serverManager run by %d minutes", period));
		serverManager* sTimer = serverManager::getInstance();
		sTimer->initTimer("serverManager",60*period);
		sTimer->startTimer();
	}
	*/
	return ciTrue;
}


ciBoolean
dmWorld::fini(long p_finiCode)
{
	ciDEBUG(3,("fini()"));
	dailyManager::clearInstance();

	return ciWorld::fini(p_finiCode);
}


