// ContentsDistributorView.cpp : CContentsDistributorView 클래스의 구현
//

#include "stdafx.h"
#include "PublicContentsInfo.h"

#ifdef _CONTENTS_DISTRIBUTOR_
	#include "Lib.h"
#else
	#include "Control/Lib.h"
#endif


#define		LOCK_INTERVAL		1000


CPublicContentsInfo::CPublicContentsInfo()
{
}

CPublicContentsInfo::~CPublicContentsInfo()
{
}

BOOL CPublicContentsInfo::Load(LPCSTR lpszPath)
{
	CString ini_path = (lpszPath==NULL) ? ::GetINIPath() : lpszPath;

	//
	m_mutex.Lock();

	//
	char* buf = new char[64000];
	::ZeroMemory(buf, sizeof(char)*64000);

	::GetPrivateProfileString("PublicContents", "IncompleteItems", "", buf, 64000, ini_path);
	CString str_path = buf;
	str_path.TrimLeft(" ");
	str_path.TrimRight(" ");

	delete[]buf;

	if(str_path.GetLength() > 0)
	{
		int pos = 0;
		CString str_item = str_path.Tokenize("|", pos);

		while(str_item != "")
		{
			if(str_item.Find("/") > 0)
			{
				int pos2 = 0;
				PUBLICCONTENTS_INFO info;

				info.source_path = str_item.Tokenize("/", pos2);
				info.target_path = str_item.Tokenize("/", pos2);

				m_list.Add(info);
			}

			str_item = str_path.Tokenize("|", pos);
		}
	}

	//
	m_mutex.Unlock();

	return TRUE;
}

BOOL CPublicContentsInfo::Save(LPCSTR lpszPath)
{
	CString ini_path = (lpszPath==NULL) ? ::GetINIPath() : lpszPath;

	//
	m_mutex.Lock();

	//
	CString value = "";
	int count = m_list.GetCount();
	for(int i=0; i<count; i++)
	{
		PUBLICCONTENTS_INFO& info = m_list.GetAt(i);

		if(value.GetLength() > 0) value+="|";
		value += info.source_path;
		value += "/";
		value += info.target_path;
	}

	//
	::WritePrivateProfileString("PublicContents", "IncompleteItems", (LPCSTR)value, ini_path);

	//
	m_mutex.Unlock();

	return TRUE;
}

void CPublicContentsInfo::PushBack(LPCSTR lpszSource, LPCSTR lpszTarget, bool bSave)
{
	//
	m_mutex.Lock();

	//
	PUBLICCONTENTS_INFO info;

	info.source_path = lpszSource;
	info.target_path = lpszTarget;

	m_list.Add(info);

	//
	m_mutex.Unlock();

	//
	if(bSave) Save();
}

bool CPublicContentsInfo::PopFront()
{
	//
	m_mutex.Lock();

	//
	if(m_list.GetCount() > 0)
	{
		m_list.RemoveAt(0);
		m_mutex.Unlock();
		return true;
	}

	//
	m_mutex.Unlock();

	//
	Save();

	return false;
}

bool CPublicContentsInfo::GetFront(CString& strSource, CString& strTarget)
{
	m_mutex.Lock();

	if(m_list.GetCount() <= 0)
	{
		m_mutex.Unlock();
		return false;
	}

	PUBLICCONTENTS_INFO& info = m_list.GetAt(0);

	strSource = info.source_path;
	strTarget = info.target_path;

	m_mutex.Unlock();
	return true;
}

BOOL CPublicContentsInfo::LoadAndClear(LPCSTR lpszPath)
{
	//
	m_mutex.Lock();

	int cnt = 0;
	CFile file;
	while( !file.Open(lpszPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
	{
		::Sleep(LOCK_INTERVAL);
		if(cnt++ > 10*(1000/LOCK_INTERVAL)) return FALSE;	// 10-sec => time-out
	}

	CString ini_path = lpszPath;
	ini_path.Replace(".tmp", ".ini");

	//
	char* buf = new char[64000];
	::ZeroMemory(buf, sizeof(char)*64000);

	::GetPrivateProfileString("PublicContents", "IncompleteItems", "", buf, 64000, ini_path);
	CString str_path = buf;
	str_path.TrimLeft(" ");
	str_path.TrimRight(" ");

	delete[]buf;

	if(str_path.GetLength() > 0)
	{
		int pos = 0;
		CString str_item = str_path.Tokenize("|", pos);

		if(str_item.Find("/") > 0)
		{
			int pos2 = 0;
			PUBLICCONTENTS_INFO info;

			info.source_path = str_item.Tokenize("/", pos2);
			info.target_path = str_item.Tokenize("/", pos2);

			m_list.Add(info);
		}
	}

	::WritePrivateProfileString("PublicContents", "IncompleteItems", "", ini_path);

	//
	m_mutex.Unlock();

	return TRUE;
}

BOOL CPublicContentsInfo::LockPushBack(LPCSTR lpszPath, LPCSTR lpszSource, LPCSTR lpszTarget)
{
	CString ini_path = lpszPath;
	ini_path.Replace(".tmp", ".ini");

	//
	int cnt = 0;
	CFile file;
	while( !file.Open(lpszPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
	{
		::Sleep(LOCK_INTERVAL);
		if(cnt++ > 10*(1000/LOCK_INTERVAL)) return FALSE;	// 10-sec => time-out
	}

	//
	Load(ini_path);

	PushBack(lpszSource, lpszTarget, false);

	return Save(ini_path);
}
