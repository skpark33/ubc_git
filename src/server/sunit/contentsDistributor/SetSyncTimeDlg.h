#pragma once
#include "afxwin.h"
#include "afxdtctl.h"


// CSetSyncTimeDlg 대화 상자입니다.

class CSetSyncTimeDlg : public CDialog
{
	DECLARE_DYNAMIC(CSetSyncTimeDlg)

public:
	CSetSyncTimeDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSetSyncTimeDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SET_SYNC_TIME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

public:
	CButton			m_chkSetSyncTime;
	CDateTimeCtrl	m_dtcSyncTime;

	afx_msg void OnBnClickedCheckSetSyncTime();
};
