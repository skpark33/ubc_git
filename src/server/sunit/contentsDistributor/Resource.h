//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ContentsDistributor.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_CONTENTSDISTRIBUTOR_FORM    101
#define IDR_MAINFRAME                   128
#define IDR_ContentsDistribTYPE         129
#define IDB_PNG_PLAY                    130
#define IDB_PNG_STOP                    131
#define IDD_SET_SYNC_TIME               132
#define IDD_CLEAN_EMPTY_FOLDERS         133
#define IDD_CLEAN_EMPTY_FOLDER          133
#define IDC_LIST_LOG                    1000
#define IDC_EDIT_SOURCE_PATH            1001
#define IDC_EDIT_TARGET_PATH            1002
#define IDC_BUTTON_CHANGE_SOURCE_PATH   1003
#define IDC_BUTTON_CHANGE_TARGET_PATH   1004
#define IDC_STATIC_STATUS               1005
#define IDC_TIMEPICKER                  1006
#define IDC_CHECK1                      1007
#define IDC_CHECK_SET_SYNC_TIME         1007
#define IDC_LIST_EMPTY_FOLDERS          1008
#define IDC_BUTTON_RESCAN               1009
#define ID_32771                        32771
#define ID_SYNC_CONTENTS_NOW            32772
#define ID_SET_SYNC_TIME                32773
#define ID_32774                        32774
#define ID_SETTING_LOCKFOLDER           32775
#define ID_32776                        32776
#define ID_CLEAN_EMPTY_FOLDER           32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
