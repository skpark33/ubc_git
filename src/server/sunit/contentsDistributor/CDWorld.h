 /*! \file CDWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _CDWorld_h_
#define _CDWorld_h_

#include <cci/libWorld/cciORBWorld.h> 
#include "ci/libThread/ciThread.h"
#include <FWV/tcpSocketAcceptor.h>
#include <CMN/libScratch/scratchUtil.h>
//#include <FWV/tcpSocketServer.h>

class CDWorld : public cciORBWorld {
public:
	CDWorld();
	~CDWorld();
 	
	//[
	//	From ciWorld
	//
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);

protected:
};
	
class CDWorldThread : public virtual ciThread {
public:
	void init() { 
		world = new CDWorld();
		if(!world) {
			 exit(1);
		}
		if(world->init(__argc, __argv) == ciFalse) {
			 world->fini(2);
			 exit(1);
		}
	}
	void run(){
		world->run();
	}
	void destroy(){
		world->fini(0);
	}
	
protected:
	CDWorld* world;
};
#endif //_CDWorld_h_
