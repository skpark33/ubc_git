// ContentsDistributorView.h : CContentsDistributorView 클래스의 인터페이스
//


#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "atlimage.h"
#include "afxmt.h"
#include "afxtempl.h"


#define		DEFAULT_LOCK_FILE				"Lock.tmp"


////////////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	CString source_path;
	CString target_path;
} PUBLICCONTENTS_INFO;
typedef		CArray<PUBLICCONTENTS_INFO, PUBLICCONTENTS_INFO&>	PUBLICCONTENTS_INFOLIST;


////////////////////////////////////////////////////////////////////////////////////////
class CPublicContentsInfo
{
public:
	CPublicContentsInfo();
	virtual ~CPublicContentsInfo();

protected:
	PUBLICCONTENTS_INFOLIST		m_list;
	CMutex		m_mutex;

public:
	BOOL		Load(LPCSTR lpszPath=NULL);
	BOOL		Save(LPCSTR lpszPath=NULL);

	void		PushBack(LPCSTR lpszSource, LPCSTR lpszTarget, bool bSave=true);
	bool		PopFront();
	bool		GetFront(CString& strSource, CString& strTarget);

	/////////////////////////////////////////////////////////////////////////////////////////
	BOOL		LoadAndClear(LPCSTR lpszPath);	// do not save-func. after run this func. !!!
	BOOL		LockPushBack(LPCSTR lpszPath, LPCSTR lpszSource, LPCSTR lpszTarget);
};
