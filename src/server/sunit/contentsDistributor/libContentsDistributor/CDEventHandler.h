#ifndef _CDEventHandler_h_
#define _CDEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

#include <afxmt.h>
#include <afxtempl.h>

// <-- add by seventhstone (2011.07.11)
#define		WM_ADD_CONTENTS					(WM_USER + 1025)	// wParam=total_copy_count, lParam=id name(CString*, delete after)
#define		WM_CHANGE_CONTENTS_STATUS		(WM_USER + 1026)	// wParam=cur_copy_count, lParam=status(0:no_err, etc:error_msg(CString*, delete after) )
#define		WM_START_SYNC_CONTENTS			(WM_USER + 1027)	// wParam=1
#define		WM_STOP_SYNC_CONTENTS			(WM_USER + 1028)	// wParam=0

#define		WM_CONTENTS_CHANGE				(WM_USER + 1029)	// change-flag of file(s) in specified folder.

#define		WM_CONTENTS_MONITOR_FAULT		(WM_USER + 1030)	// Contents_Monitor_Thread is fault ==> re-create
// -->


class CDEventHandler : public cciEventHandler {
public:
	CDEventHandler() ;
	virtual ~CDEventHandler() {}
   	virtual void 	processEvent(cciEvent& pEvent);
	ciBoolean		addHand();

	const char* getProgramPath() { return _programPath.c_str(); }
	const char* getContentsPath() { return _contentsPath.c_str(); }
	const char* getTempPath() { return _tempPath.c_str(); }

protected:
   	void 		_processReloadSchedule(cciEvent& pEvent);
   	void 		_processProgramRemoved(cciEvent& pEvent);

	ciULong _eventKey;
	ciMutex	_eventKeyLock;

	ciString	_contentsPath;
	ciString	_programPath;
	ciString	_tempPath;

	// <-- add by seventhstone (2011.07.11)
public:
	static void 			_processSyncContents(cciEvent& pEvent);
	static void				_deleteThread(CWinThread* pThread);

	static	void			_initContentsSyncThread();		// 동기화 스레드 생성
	static	void			_clearContentsSyncThread();		// 동기화 스레드 삭제

	static	ciString		_contentsSourcePath;			// 컨텐츠 원본 디렉토리
	static	ciString		_announceSourcePath;			// 긴급공지 컨텐츠 원본 디렉토리

	static	CWinThread*		_contentsSyncThread;			// 컨텐츠 동기화 스레드 핸들
	static	bool 			_runContentsSyncThread;			// 스레드 실행 flag

	static	bool 			_refreshContentsSync;			// 스레드 갱싱 flag
	static	CMutex			_contentsSyncLock;				// 스레드 동기화
	static	HWND	 		_hwndMainWnd;					// 메인화면 핸들

	// add by seventhstone (2011.11.22)
	static	void			_initContentsMonitorThread();	// 컨텐츠 모니터 스레드 생성
	static	void			_clearContentsMonitorThread();	// 컨텐츠 모니터 스레드 삭제

	static	CWinThread*		_contentsMonitorThread[2];			// 컨텐츠 모니터 스레드 핸들
	static	bool 			_runContentsMonitorThread;		// 스레드 실행 flag

	// add by seventhstone (2012.03.23)
	static	void			_initCachingContentsThread();	// 컨텐츠제외 스레드 생성
	static	void			_clearCachingContentsThread();	// 컨텐츠제외 스레드 삭제

	static	CWinThread*		_cachingContentsThread;			// 컨텐츠제외 스레드 핸들
	static	bool 			_runCachingContentsThread;		// 스레드 실행 flag
	static	bool 			_isGetCachingContentsFromServer;// 컨텐츠제외 값을 서버에서 얻어왔는지 Flag

	//static	CMapStringToString	_mapNoCachingContents;		// 복사제외리스트, 삭제할리스트
	static	CMapStringToString	_mapCachingContents;		// 복사제외리스트, 삭제할리스트
	static	CMutex			_cachingContentsLock;			// 복사제외리스트 동기화
	static	void			InitCachingContentsList(CStringArray& listCachingContents); // 복사제외리스트 초기화
	static	bool			IsCachingContents(CString contentsId); // 복사제외 확인
	// -->

};

// <-- add by seventhstone (2011.07.11)
static UINT ContentsSyncThreadFunc(LPVOID pParam);	//	동기화 메인 스레드
static UINT SyncContents_Root(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot);	// 컨텐츠 루트 검색
static UINT SyncContents_Sub(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot, LPCSTR lpszSubPath, CStringArray& listCopyContents);	// 컨텐츠 서브 검색
static UINT CopyContentsList(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot, CStringArray& listCopyContents); // 컨텐츠 복사
static bool	CreatePathDirectory(LPCTSTR lpszPath); // 컨텐츠 디렉토리 생성

// add by seventhstone (2011.11.22)
static UINT ContentsMonitorThreadFunc(LPVOID pParam);	//	동기화 메인 스레드

// <-- add by seventhstone (2012.03.23)
static UINT DeleteContents_Root(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot);	// 컨텐츠 루트 검색
static UINT DeleteContents_Sub(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot, LPCSTR lpszSubPath, bool bForceDelete, CStringArray& listCopyContents);	// 컨텐츠 서브 검색
static UINT DeleteContentsList(LPCSTR lpszTgtRoot, CStringArray& listDeleteContents); // 컨텐츠 삭제

static UINT UpdateCachingContentsThreadFunc(LPVOID pParam);	//	동기화제외 업데이트 메인 스레드
// -->


class CMutexGuard
{
public:
	CMutexGuard(CMutex& mutex) { m_mutex = &mutex; mutex.Lock(); };
	virtual ~CMutexGuard() { m_mutex->Unlock(); };

protected:
	CMutex*		m_mutex;

};

#endif // _CDEventHandler_h_
