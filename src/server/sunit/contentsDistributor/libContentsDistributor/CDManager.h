#ifndef _CDManager_h_
#define _CDManager_h_

#include "libContentsDistributor/CDEventHandler.h"

class CDManager  {
public:
	static CDManager*	getInstance();
	static void	clearInstance();

	virtual		~CDManager() ;
	ciBoolean	init(const char* mgrId);

	ciBoolean	createProgramLocation(const char* programId);
	ciBoolean	removeProgramLocation(const char* programId);

	//const char*	toString();
	//ciBoolean	isMine(const char* mgrId);



protected:
	static CDManager*	_instance;
	static ciMutex		_instanceLock;

	CDManager() ;

	int _initProgramLocation() ;

	//ciStringList	_mgrIdList; //
	ciString	_mgrId; //
	CDEventHandler*	_eventHandler;
	//ciString		_bufStr;

	//ciStringMap	_plmap;
	//ciMutex		_plmapLock;
};

#endif // _CDManager_h_
