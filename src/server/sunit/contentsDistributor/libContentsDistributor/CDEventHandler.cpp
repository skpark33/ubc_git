#include "stdafx.h"
//#include "libDownload/ProfileManager.h"
#include "libDownload/Downloader.h"

#include <ci/libConfig/ciPath.h>
#include <cci/libWrapper/cciEvent.h>
#include "CMN/libInstall/installUtil.h"
#include "CMN/libScratch/scratchUtil.h"
#include "CMN/libCommon/ubcIni.h"
#include "libHttpRequest/HttpRequest.h"

#include "CDManager.h"
#include "CDEventHandler.h"
#include "CDDownloadThread.h"

#include <shlwapi.h>

ciSET_DEBUG(10, "CDEventHandler");


// <-- add by seventhstone (2011.07.11)
ciString		CDEventHandler::_contentsSourcePath = "";
ciString		CDEventHandler::_announceSourcePath = "";

CWinThread*		CDEventHandler::_contentsSyncThread = NULL;
bool			CDEventHandler::_runContentsSyncThread = true;
bool 			CDEventHandler::_refreshContentsSync = false;
CMutex			CDEventHandler::_contentsSyncLock;
HWND	 		CDEventHandler::_hwndMainWnd = NULL;

// add by seventhstone (2011.11.22)
CWinThread*		CDEventHandler::_contentsMonitorThread[2] = { NULL, NULL };
bool 			CDEventHandler::_runContentsMonitorThread = false;

// add by seventhstone (2012.03.23)
//CMapStringToString	CDEventHandler::_mapNoCachingContents;			// 복사제외리스트, 삭제할리스트
CMapStringToString	CDEventHandler::_mapCachingContents;			// 복사리스트, 보존할리스트
CMutex			CDEventHandler::_cachingContentsLock;				// 복사제외리스트 동기화

CWinThread*		CDEventHandler::_cachingContentsThread = NULL;		// 컨텐츠제외 스레드 핸들
bool 			CDEventHandler::_runCachingContentsThread = false;	// 스레드 실행 flag
bool 			CDEventHandler::_isGetCachingContentsFromServer = false;

#define			GET_CACHING_CONTENTS_CYCLE				(60*60)	// 1시간	<=	(24*60*60) // 24시간
// -->

#define			CONTENTS_UPDATE_FLAG_FILE			"ContentsDistributor.update"

CDEventHandler::CDEventHandler()
{
	ciString buf = ciPath::getInstance()->getPath("");
	_programPath = buf + "..\\config";
	_contentsPath = buf + "..\\Contents";
	_tempPath = buf + "..\\Tmp";
}

ciBoolean
CDEventHandler::addHand()
{
	ciDEBUG(5,("addHand()"));

	// <-- add by seventhstone (2011.07.11)
	_initContentsSyncThread();
	_initContentsMonitorThread();
	_initCachingContentsThread();
	// -->

	cciEventManager* manager = cciEventManager::getInstance();
	cciEntity aEntity ("PM=*");

	ciString eventName = "applySchedule";
	if(manager->addHandler(aEntity, eventName, this)<0){
		ciERROR(("addHandler(*,%s) fail", eventName.c_str()));
		return ciFalse;
	}
	ciDEBUG(1,("addHandler(*,%s) succeed\n", eventName.c_str()));

	this->increase();

	eventName = "programRemoved";
	if(manager->addHandler(aEntity, eventName, this)<0){
		ciERROR(("addHandler(*,%s) fail", eventName.c_str()));
		return ciFalse;
	}
	ciDEBUG(1,("addHandler(*,%s) succeed\n", eventName.c_str()));
	return ciTrue;
}

void
CDEventHandler::processEvent(cciEvent& aEvent)
{
	ciDEBUG(5,("processEvent()"));
 
	ciString eventName = aEvent.getEventType();
	ciDEBUG(1,("%s event Received", eventName.c_str()));
	if(eventName == "applySchedule") {
		// <-- add by seventhstone (2011.07.11)
		//_processReloadSchedule(aEvent);

		// <-- wait until refresh caching-contents-list
		if(CDEventHandler::_isGetCachingContentsFromServer)
			CDEventHandler::_isGetCachingContentsFromServer = false;
		while( !CDEventHandler::_isGetCachingContentsFromServer )
			Sleep(100);
		// -->

		_processSyncContents(aEvent);
		// -->
		return;
	}
	if(eventName == "programRemoved") {
		_processProgramRemoved(aEvent);
		return;
	}

	ciERROR(("Invalid eventType"));
	return;

}

void
CDEventHandler::_processReloadSchedule(cciEvent& aEvent)
{
	ciDEBUG(5,("_processReloadSchedule()"));
	ciString eventName = aEvent.getEventType();

	ciString siteId;
	if(!aEvent.getItem("siteId", siteId)) {
		ciERROR(("%s Event does not have siteId", eventName.c_str()));
		return ;
	}
	ciString programId;
	if(!aEvent.getItem("programId", programId)) {
		ciERROR(("%s Event does not have programId", eventName.c_str()));
		return ;
	}
	ciShort side;
	if(!aEvent.getItem("side", side)) {
		ciERROR(("%s Event does not have side", eventName.c_str()));
		return ;
	}
	ciTime eventKey;
	if(!aEvent.getItem("eventKey", eventKey)) {
		ciERROR(("%s Event does not have eventKey", eventName.c_str()));
		return ;
	}

	{	
		ciGuard aGuard(_eventKeyLock);
		if(eventKey.getTime() == _eventKey){
			ciWARN(("Same Event skipped"));
			return ;
		}
		_eventKey = eventKey.getTime();
	}
	ciDEBUG(1,("%s event Received", eventName.c_str()));
	ciDEBUG(1,("eventKey=%s ", eventKey.getTimeString().c_str()));
	ciDEBUG(1,("siteId=%s ", siteId.c_str()));
	ciDEBUG(1,("programId=%s ", programId.c_str()));
	ciDEBUG(1,("side=%d ", side));


	// check wheather event from my server.
	// check local ini file exist.
	// if exist , return

	ciString fullpath = ciPath::getInstance()->getPath("..\\config\\");
	fullpath += programId;
	fullpath += ".ini";

	ciDEBUG(1,("program file is %s", fullpath.c_str()));

	CFile file;
	if(file.Open(fullpath.c_str(), CFile::modeRead ))
	{
		ciWARN(("%s file already exist", fullpath.c_str()));
		//Do this in PM Server
		//CDManager::getInstance()->createProgramLocation(programId.c_str());  // Insert DB. It'doen't matter whether It's already exist or not
		return;
	}

	CDDownloadThreadManager::clear();  // 종료된 Thread 를 삭제한다.
	CDDownloadThread* aThread = new CDDownloadThread(siteId.c_str(),programId.c_str(),this);
	aThread->start();
	CDDownloadThreadManager::push(aThread);

	return ;
}

void
CDEventHandler::_processProgramRemoved(cciEvent& aEvent)
{
	ciDEBUG(5,("_processProgramRemoved()"));
	ciString eventName = aEvent.getEventType();

	ciString mgrId;
	if(!aEvent.getItem("mgrId", mgrId)) {
		ciERROR(("%s Event does not have mgrId", eventName.c_str()));
		return ;
	}
	ciString siteId;
	if(!aEvent.getItem("siteId", siteId)) {
		ciERROR(("%s Event does not have siteId", eventName.c_str()));
		return ;
	}
	ciString programId;
	if(!aEvent.getItem("programId", programId)) {
		ciERROR(("%s Event does not have programId", eventName.c_str()));
		return ;
	}
	ciDEBUG(1,("%s event Received", eventName.c_str()));
	ciDEBUG(1,("siteId=%s ", siteId.c_str()));
	ciDEBUG(1,("programId=%s ", programId.c_str()));

	// Delete Ini file

	ciString fullpath = ciPath::getInstance()->getPath("..\\config");
	fullpath += programId;
	fullpath += ".ini";

	if(::DeleteFile(fullpath.c_str()))
	{
		ciWARN(("%s file already exist", fullpath.c_str()));
	}

	// Delete Contents File !!!! Someday in future

	// Do this in PM Server 
	//CDManager::getInstance()->removeProgramLocation(programId.c_str());
	return ;
}


// <-- add by seventhstone (2011.07.11)
void CDEventHandler::_processSyncContents(cciEvent& pEvent)
{
	CMutexGuard aGuard(_contentsSyncLock);
	// 스레드의 재검색플래그 갱신
	_refreshContentsSync = true;
	ciDEBUG(5,("_refreshContentsSync=%d", _refreshContentsSync));
}

void CDEventHandler::_deleteThread(CWinThread* pThread)
{
	if(pThread)
	{
		try
		{
			DWORD dwExitCode = 0;
			::GetExitCodeThread(pThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				ciDEBUG(5,("waiting until end of thread..."));
				DWORD timeout = ::WaitForSingleObject(pThread->m_hThread, INFINITE);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}
			ciDEBUG(5,("delete thread handle"));
			delete pThread;
		}
		catch(...)
		{
		}

		pThread = NULL;
	}
}

void CDEventHandler::_initContentsSyncThread()
{
	CMutexGuard aGuard(_contentsSyncLock);

	if(_contentsSyncThread == NULL)
	{
		ciDEBUG(5,("create _contentsSyncThread"));
		_runContentsSyncThread = true;
		_refreshContentsSync = false;
		_contentsSyncThread = ::AfxBeginThread(ContentsSyncThreadFunc, NULL, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		_contentsSyncThread->m_bAutoDelete = FALSE;
		_contentsSyncThread->ResumeThread();
		ciDEBUG(5,("run _contentsSyncThread"));
	}
	else
	{
		ciDEBUG(5,("already exist _contentsSyncThread"));
	}
}

void CDEventHandler::_clearContentsSyncThread()
{
	ciDEBUG(5,("_clearContentsSyncThread(handle=%d)", _contentsSyncThread ));
	{
		CMutexGuard aGuard(_contentsSyncLock);
		_refreshContentsSync = false;
		_runContentsSyncThread = false;
	}

	//
	{
//		ciGuard aGuard(_contentsSyncLock);
		ciDEBUG(5,("delete _contentsSyncThread(%d)", _contentsSyncThread ));

		_deleteThread(_contentsSyncThread);
		_contentsSyncThread = NULL;
	}
}

UINT ContentsSyncThreadFunc(LPVOID pParam)
{
	ciDEBUG(5,("ContentsSyncThreadFunc()"));

	//int run_contents_sync_thread_num = (int) pParam;

	char src_root[1024/*MAX_PATH*/+1] = {0}; // "Z:\\Contents";
	char tgt_root[1024/*MAX_PATH*/+1] = {0}; // "E:\\Project\\ubc\\contents";

	CString app;
	app.LoadString(AFX_IDS_APP_TITLE);

	ciString init_path = ciPath::getInstance()->getPath("");
	init_path += app;
	init_path += ".ini";

	ciDEBUG(5,("ini_path=%s", init_path.c_str() ));

	::GetPrivateProfileString("Settings", "SourcePath", "", src_root, 1024/*MAX_PATH*/, init_path.c_str() );
	::GetPrivateProfileString("Settings", "TargetPath", "", tgt_root, 1024/*MAX_PATH*/, init_path.c_str() );

	ciDEBUG(5,("SourcePath=%s", src_root ));
	ciDEBUG(5,("TargetPath=%s", tgt_root ));

	while(1)
	{
		{
			CMutexGuard aGuard(CDEventHandler::_contentsSyncLock);

			static int cnt = 0;
			TRACE("%d(%d)\r\n", cnt++, CDEventHandler::_runContentsSyncThread);

			// 스레드 종료 체크
			if(CDEventHandler::_runContentsSyncThread == false) break;

			// 스레드 실행 체크
			if(CDEventHandler::_refreshContentsSync)
			{
				CDEventHandler::_refreshContentsSync = false;
			}
			else
			{
				::Sleep(1000);
				continue;
			}
		}

		// 컨텐츠 싱크 실행
		SyncContents_Root(src_root, tgt_root);
		DeleteContents_Root(src_root, tgt_root);
	}

	ciDEBUG(5,("~ContentsSyncThreadFunc()"));

	return 0;
}

UINT SyncContents_Root(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot)
{
	CTime cur_tm = CTime::GetCurrentTime();
	ciDEBUG(5,("SyncContents_Root(%s,%s,%s)", lpszSrcRoot, lpszTgtRoot,cur_tm.Format("%Y/%m/%d %H:%M:%S") ));

	if(CDEventHandler::_hwndMainWnd)
		::PostMessage(CDEventHandler::_hwndMainWnd, WM_START_SYNC_CONTENTS, 1, 0 );

	if(stricmp(lpszSrcRoot, lpszTgtRoot) == 0)
	{
		// if lpszSrcRoot=lpszTgtRoot => no-copy
		ciDEBUG(5,("Source() equal to Target()... No COPY", lpszSrcRoot, lpszTgtRoot) );
		return 0;
	}

	// 컨텐츠 디렉토리의 루트 검색
	CString filter = lpszSrcRoot;
	filter += "\\*.*";

	CFileFind ff;
	BOOL bWorking = ff.FindFile(filter);

	while (bWorking && CDEventHandler::_runContentsSyncThread)
	{
		bWorking = ff.FindNextFile();

		if (ff.IsDots()) continue;

		if (ff.IsDirectory())
		{
			CStringArray copy_contents_list;

			//
			CString folder_name = ff.GetFileName();
			CString sub_path = "\\";
			sub_path += folder_name;

			bool is_caching_contents = CDEventHandler::IsCachingContents(folder_name);
			if( !is_caching_contents )
			{
				ciDEBUG(5,("NoCachingContents(%s)", folder_name ));
				continue;	// 컨텐츠제외일 경우 skip
			}

			//
			ciDEBUG(5,("SyncContents_Sub(%s)", sub_path ));
			SyncContents_Sub(lpszSrcRoot, lpszTgtRoot, sub_path, copy_contents_list);
			ciDEBUG(5,("CopyContentsListCount(%d)", copy_contents_list.GetCount() ));

			//
			if(copy_contents_list.GetCount() > 0)
			{
				ciDEBUG(5,("Add to mainwnd(%s)", ff.GetFileName() ));

				if(CDEventHandler::_hwndMainWnd)
				{
					CString* fn = new CString(ff.GetFileName());
					::PostMessage(CDEventHandler::_hwndMainWnd, WM_ADD_CONTENTS, copy_contents_list.GetCount(), (LPARAM)fn );
				}

				CopyContentsList(lpszSrcRoot, lpszTgtRoot, copy_contents_list);
			}
		}
	}
	ff.Close();

	ciDEBUG(5,("~SyncContents_Root()" ));
//	if(CDEventHandler::_hwndMainWnd)
//		::PostMessage(CDEventHandler::_hwndMainWnd, WM_STOP_SYNC_CONTENTS, 0, 0 );

	return 0;
}

UINT SyncContents_Sub(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot, LPCSTR lpszSubPath, CStringArray& listCopyContents)
{
	// 컨텐츠 디렉토리의 하위검색(리컬시브)
	CString filter;
	filter.Format("%s%s\\*.*", lpszSrcRoot, lpszSubPath);

	CFileFind ff;
	BOOL bWorking = ff.FindFile(filter);

	while (bWorking && CDEventHandler::_runContentsSyncThread)
	{
		bWorking = ff.FindNextFile();

		if (ff.IsDots()) continue;

		if (ff.IsDirectory())
		{
			//
			CString sub_path;
			sub_path.Format("%s\\%s", lpszSubPath, ff.GetFileName());

			//
			SyncContents_Sub(lpszSrcRoot, lpszTgtRoot, sub_path, listCopyContents);
		}
		else
		{
			// file
			CString sub_path;
			sub_path.Format("%s\\%s", lpszSubPath, ff.GetFileName());

			CString src_fullpath;
			src_fullpath.Format("%s%s", lpszSrcRoot, sub_path);

			CString tgt_fullpath;
			tgt_fullpath.Format("%s%s", lpszTgtRoot, sub_path);

			//
			CTime src_time;
			ff.GetLastWriteTime(src_time);

			ULONGLONG src_size = ff.GetLength();

			//
			struct _stati64 statbuf;

			CFileStatus tgt_fs;
			if(CFile::GetStatus(tgt_fullpath, tgt_fs)==FALSE || _stati64(tgt_fullpath, &statbuf))
			{
				// 파일정보 실패시 복사리스트에 추가
				ciDEBUG(5,("add contents : file not exist(%s)", sub_path ));
				listCopyContents.Add(sub_path);
			}
			else if(src_time != tgt_fs.m_mtime)
			{
				// 시간 다르면 복사리스트에 추가
				ciDEBUG(5,("add contents : different time(%s<-->%s,%s)", src_time.Format("%Y-%m-%d %H:%M:%S"), tgt_fs.m_mtime.Format("%Y-%m-%d %H:%M:%S"), sub_path ));
				listCopyContents.Add(sub_path);
			}
			else if(src_size != statbuf.st_size)//tgt_fs.m_size)
			{
				// 크기 다르면 복사리스트에 추가
				ciDEBUG(5,("add contents : different size(%I64u<-->%I64u,%s)", src_size, statbuf.st_size/*tgt_fs.m_size*/, sub_path ));
				listCopyContents.Add(sub_path);
			}
		}
	}
	ff.Close();

	return 0;
}

UINT CopyContentsList(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot, CStringArray& listCopyContents)
{
	for(int i=0; i<listCopyContents.GetCount() && CDEventHandler::_runContentsSyncThread; i++)
	{
		const CString& contents_path = listCopyContents.GetAt(i);

		CString src_fullpath;
		src_fullpath.Format("%s%s", lpszSrcRoot, contents_path);

		CString tgt_fullpath;
		tgt_fullpath.Format("%s%s", lpszTgtRoot, contents_path);

		ciDEBUG(5,("CopyFile(%s -> %s)", src_fullpath, tgt_fullpath ));

		if( CreatePathDirectory(tgt_fullpath)==false || ::CopyFile(src_fullpath, tgt_fullpath, FALSE)==0 )
		{
			ciERROR(("CopyFile FAIL !!!" ));

			// 디렉토리 생성실패 or 복사실패 : err
			if(CDEventHandler::_hwndMainWnd)
			{
				CString* path = new CString((LPCSTR)contents_path);
				::PostMessage(CDEventHandler::_hwndMainWnd, WM_CHANGE_CONTENTS_STATUS, i+1, (LPARAM)path );
			}
		}
		else
		{
			// no_err
			if(CDEventHandler::_hwndMainWnd)
				::PostMessage(CDEventHandler::_hwndMainWnd, WM_CHANGE_CONTENTS_STATUS, i+1, 0 );
		}

		// create update file( /{...}/ContentsDistributor.update )
		int pos = contents_path.Find('\\', 1);
		if(pos > 0)
		{
			tgt_fullpath.Format("%s%s\\%s", lpszTgtRoot, contents_path.Left(pos), CONTENTS_UPDATE_FLAG_FILE);
			ciDEBUG(5, ("Update Flag (%s)", tgt_fullpath ));

			CFile file;
			if( file.Open(tgt_fullpath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
			{
				CString msg = CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S");
				file.Write((LPCSTR)msg, msg.GetLength());
				file.Close();
			}
			else
			{
				ciWARN(("Can't create CD-Update-File (%s)", tgt_fullpath ));
			}
		}
		else
		{
			ciWARN(("contents_path do not include '\\' (%s)", contents_path ));
		}
	}
	return 0;
}

bool CreatePathDirectory(LPCTSTR lpszPath) // 파일까지 fullpath
{
    TCHAR szPathBuffer[1024/*MAX_PATH*/];

    size_t len = _tcslen(lpszPath);

    for(size_t i = 0; i < len; i++ )
    {
        szPathBuffer[i] = *(lpszPath + i);
        if(szPathBuffer[i] == _T('\\') || szPathBuffer[i] == _T('/'))
        {
            szPathBuffer[i + 1] = NULL;
            if(!PathFileExists(szPathBuffer))
            {
                if(!::CreateDirectory(szPathBuffer, NULL))
                {
                    if(GetLastError() != ERROR_ALREADY_EXISTS)
					{
						ciERROR(("CreatePathDirectory FAIL (%s)", lpszPath ));
                        return false;
					}//if
                }//if
            }//if
        }//if
    }//for

    return true;
}

// add by seventhstone (2011.11.22)
void CDEventHandler::_initContentsMonitorThread()
{
	if(_contentsMonitorThread[0] == NULL)
	{
		ciDEBUG(5,("create _contentsMonitorThread"));

		// get contents_path from ini-file.
		CString app;
		app.LoadString(AFX_IDS_APP_TITLE);

		ciString init_path = ciPath::getInstance()->getPath("");
		init_path += app;
		init_path += ".ini";

		ciDEBUG(5,("ini_path=%s", init_path.c_str() ));

		char src_root[1024/*MAX_PATH*/+1] = {0}; // "Z:\\Contents";
		::GetPrivateProfileString("Settings", "SourcePath", "", src_root, 1024/*MAX_PATH*/, init_path.c_str() );

		_contentsSourcePath = src_root;
		ciDEBUG(5,("ContentsSourcePath=%s", (LPCSTR)_contentsSourcePath.c_str() ));

		_announceSourcePath = src_root;
		_announceSourcePath += "\\announce";
		ciDEBUG(5,("AnnounceSourcePath=%s", (LPCSTR)_announceSourcePath.c_str() ));
		::CreateDirectory(_announceSourcePath.c_str(), NULL);

		//
		_runContentsMonitorThread = true;
		_contentsMonitorThread[0] = ::AfxBeginThread(ContentsMonitorThreadFunc, (LPVOID)_contentsSourcePath.c_str(), THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		_contentsMonitorThread[0]->m_bAutoDelete = FALSE;
		_contentsMonitorThread[0]->ResumeThread();
		ciDEBUG(5,("run _contentsMonitorThread[0]"));

		//
		_contentsMonitorThread[1] = ::AfxBeginThread(ContentsMonitorThreadFunc, (LPVOID)_announceSourcePath.c_str(), THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		_contentsMonitorThread[1]->m_bAutoDelete = FALSE;
		_contentsMonitorThread[1]->ResumeThread();
		ciDEBUG(5,("run _contentsMonitorThread[1]"));
	}
	else
	{
		ciDEBUG(5,("already exist _contentsMonitorThread"));
	}
}

void CDEventHandler::_clearContentsMonitorThread()
{
	ciDEBUG(5,("_clearContentsThread()"));
	{
		_runContentsMonitorThread = false;
	}

	//
	{
		ciDEBUG(5,("delete _contentsMonitorThread1(%d)", _contentsMonitorThread[0] ));
		_deleteThread(_contentsMonitorThread[0]);
		_contentsMonitorThread[0] = NULL;

		ciDEBUG(5,("delete _contentsMonitorThread2(%d)", _contentsMonitorThread[1] ));
		_deleteThread(_contentsMonitorThread[1]);
		_contentsMonitorThread[1] = NULL;
	}
}

UINT ContentsMonitorThreadFunc(LPVOID pParam)
{
	ciString path = (LPCSTR)pParam;

	ciDEBUG(5,("ContentsMonitorThreadFunc=%s", path.c_str() ));

	DWORD dwWaitStatus;
	HANDLE dwChangeHandle;

	// Watch the directory for file creation and deletion. 
	dwChangeHandle = FindFirstChangeNotification( 
		path.c_str(),                 // directory to watch 
		TRUE,                         // watch subtree 
		FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME | FILE_NOTIFY_CHANGE_SIZE | FILE_NOTIFY_CHANGE_LAST_WRITE ); // watch all file&folder changes

	if (dwChangeHandle == INVALID_HANDLE_VALUE || dwChangeHandle == NULL)
	{
		ciERROR(("invalid handle of ContentsMonitorThread !!!"));
		return 0;
	}

	// Change notification is set. Now wait on both notification
	// handles and refresh accordingly.
	bool run = true;
	while(run)
	{
		if(CDEventHandler::_runContentsMonitorThread == false) break;

		// Wait for notification.
		dwWaitStatus = WaitForSingleObject(dwChangeHandle, 1000);

		if(CDEventHandler::_runContentsMonitorThread == false) break;

		switch (dwWaitStatus)
		{
		case WAIT_OBJECT_0:
			// A file was created, renamed, or deleted in the directory.
			// Refresh this directory and restart the notification.

			ciDEBUG(5,("FindFirstChangeNotification" ));
			if(CDEventHandler::_hwndMainWnd)
				::PostMessage(CDEventHandler::_hwndMainWnd, WM_CONTENTS_CHANGE, NULL, NULL );
			if(FindNextChangeNotification(dwChangeHandle) == FALSE)
			{
				ciERROR(("FindNextChangeNotification function failed !!!"));
				if(CDEventHandler::_hwndMainWnd)
					::PostMessage(CDEventHandler::_hwndMainWnd, WM_CONTENTS_MONITOR_FAULT, NULL, NULL );
				run = false;
			}
			break;

		case WAIT_TIMEOUT:
			break;

		default:
			ciERROR(("Unhandled dwWaitStatus(%d) !!!", dwWaitStatus));
			run = false;
			break;
		}
	}

	return 0;
}

// <-- add by seventhstone (2012.03.23)
void CDEventHandler::_initCachingContentsThread()
{
	if(_cachingContentsThread == NULL)
	{
		ciDEBUG(5,("create _cachingContentsThread"));

		//
		_runCachingContentsThread = true;
		_cachingContentsThread = ::AfxBeginThread(UpdateCachingContentsThreadFunc, NULL, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		_cachingContentsThread->m_bAutoDelete = FALSE;
		_cachingContentsThread->ResumeThread();
		ciDEBUG(5,("run _cachingContentsThread"));
	}
	else
	{
		ciDEBUG(5,("already exist _cachingContentsThread"));
	}
}

void CDEventHandler::_clearCachingContentsThread()
{
	ciDEBUG(5,("_clearContentsThread(handle=%d)", _cachingContentsThread ));
	{
		_runCachingContentsThread = false;
	}

	//
	{
		ciDEBUG(5,("delete _cachingContentsThread(%d)", _cachingContentsThread ));

		_deleteThread(_cachingContentsThread);
		_cachingContentsThread = NULL;
	}
}

void CDEventHandler::InitCachingContentsList(CStringArray& listCachingContents)
{
	_cachingContentsLock.Lock();
	//_mapNoCachingContents.RemoveAll();
	_mapCachingContents.RemoveAll();

	for(int i=0; i<listCachingContents.GetCount(); i++)
	{
		CString contents = listCachingContents.GetAt(i);

		//ciDEBUG(5,("Add to NoCachingContents(%s)", (LPCSTR)contents.MakeUpper() ));
		ciDEBUG(5,("Add to CachingContents(%s)", (LPCSTR)contents.MakeUpper() ));

		//_mapNoCachingContents.SetAt((LPCSTR)contents.MakeUpper(), contents);
		_mapCachingContents.SetAt((LPCSTR)contents.MakeUpper(), contents);
	}

	_cachingContentsLock.Unlock();

#ifdef DEBUG
	// print all-items of caching-contents
	POSITION pos = _mapCachingContents.GetStartPosition();
	CString key, value;
	while(pos != NULL)
	{
		_mapCachingContents.GetNextAssoc( pos, key, value );
		ciDEBUG(5,("Inserted NoCachingContents(%s)", (LPCSTR)key ));
	}
#endif
}

bool CDEventHandler::IsCachingContents(CString contents)
{
	_cachingContentsLock.Lock();

	if(_mapCachingContents.GetCount() == 0)
	{
		// prevent (no-list -> delete-all)
		return true;
	}

	CString ccc = contents.MakeUpper();
	CString value;
	//if( _mapNoCachingContents.Lookup((LPCSTR)ccc, value) )
	if( _mapCachingContents.Lookup((LPCSTR)ccc, value) )
	{
		//ciDEBUG(5,("IsCachingContents(%s)", ccc ));
		_cachingContentsLock.Unlock();
		//return false;
		return true;
	}

	//ciDEBUG(5,("NoCachingContents(%s)", ccc ));
	_cachingContentsLock.Unlock();
	//return true;
	return false;
}

UINT DeleteContents_Root(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot)
{
	CTime cur_tm = CTime::GetCurrentTime();
	ciDEBUG(5,("DeleteContents_Root(%s,%s,%s)", lpszSrcRoot, lpszTgtRoot,cur_tm.Format("%Y/%m/%d %H:%M:%S") ));

//	if(CDEventHandler::_hwndMainWnd)
//		::PostMessage(CDEventHandler::_hwndMainWnd, WM_START_SYNC_CONTENTS, 1, 0 );

	if(stricmp(lpszSrcRoot, lpszTgtRoot) == 0)
	{
		// if lpszSrcRoot=lpszTgtRoot => no-delete
		ciDEBUG(5,("Source() equal to Target()... No Delete", lpszSrcRoot, lpszTgtRoot) );

		if(CDEventHandler::_hwndMainWnd)
			::PostMessage(CDEventHandler::_hwndMainWnd, WM_STOP_SYNC_CONTENTS, 0, 0 );

		return 0;
	}

	// 컨텐츠 디렉토리의 루트 검색
	CString filter = lpszTgtRoot;
	filter += "\\*.*";

	CFileFind ff;
	BOOL bWorking = ff.FindFile(filter);

	while (bWorking && CDEventHandler::_runContentsSyncThread)
	{
		bWorking = ff.FindNextFile();

		if (ff.IsDots()) continue;

		if (ff.IsDirectory())
		{
			CStringArray delete_contents_list;

			//
			CString folder_name = ff.GetFileName();
			CString sub_path = "\\";
			sub_path += folder_name;

			//
			bool is_caching_contents = CDEventHandler::IsCachingContents(folder_name);

			//
			ciDEBUG(5,("DeleteContents_Sub(%s)", sub_path ));
			int f_cnt = DeleteContents_Sub(lpszSrcRoot, lpszTgtRoot, sub_path, !is_caching_contents, delete_contents_list);
			if(f_cnt == 0)
			{
				ciDEBUG(5,("delete contents : file not exist in folder(%s)", sub_path ));
				delete_contents_list.Add(sub_path); // delete folder
			}
			ciDEBUG(5,("DeleteContentsListCount(%d)", delete_contents_list.GetCount() ));

			//
			if(delete_contents_list.GetCount() > 0)
			{
				DeleteContentsList(lpszTgtRoot, delete_contents_list);
			}
		}
	}
	ff.Close();

	ciDEBUG(5,("~DeleteContents_Root()" ));
	if(CDEventHandler::_hwndMainWnd)
		::PostMessage(CDEventHandler::_hwndMainWnd, WM_STOP_SYNC_CONTENTS, 0, 0 );

	return 0;
}

UINT DeleteContents_Sub(LPCSTR lpszSrcRoot, LPCSTR lpszTgtRoot, LPCSTR lpszSubPath, bool bForceDelete, CStringArray& listDeleteContents)
{
	// 컨텐츠 디렉토리의 하위검색(리컬시브)
	CString filter;
	filter.Format("%s%s\\*.*", lpszTgtRoot, lpszSubPath);

	int file_count = 0;

	CFileFind ff;
	BOOL bWorking = ff.FindFile(filter);

	while (bWorking && CDEventHandler::_runContentsSyncThread)
	{
		bWorking = ff.FindNextFile();

		if (ff.IsDots()) continue;

		if (ff.IsDirectory())
		{
			//
			CString sub_path;
			sub_path.Format("%s\\%s", lpszSubPath, ff.GetFileName());

			//
			int f_cnt = DeleteContents_Sub(lpszSrcRoot, lpszTgtRoot, sub_path, bForceDelete, listDeleteContents);
			if(f_cnt == 0)
			{
				ciDEBUG(5,("delete contents : file not exist in folder(%s)", sub_path ));
				listDeleteContents.Add(sub_path); // delete folder
			}
		}
		else
		{
			// file
			file_count++;

			CString sub_path;
			sub_path.Format("%s\\%s", lpszSubPath, ff.GetFileName());

			CString src_fullpath;
			src_fullpath.Format("%s%s", lpszSrcRoot, sub_path);

			if( ::_access(src_fullpath, 0) != 0  || bForceDelete )
			{
				// 파일 존재하지 않을시
				if(bForceDelete)
				{
					ciDEBUG(5,("delete contents : no caching contents(%s)", sub_path ));
				}
				else
				{
					ciDEBUG(5,("delete contents : file not exist(%s)", sub_path ));
				}
				listDeleteContents.Add(sub_path);
			}
		}
	}
	ff.Close();
	return file_count;
}

UINT DeleteContentsList(LPCSTR lpszTgtRoot, CStringArray& listDeleteContents)
{
	for(int i=0; i<listDeleteContents.GetCount() && CDEventHandler::_runContentsSyncThread; i++)
	{
		const CString& contents_path = listDeleteContents.GetAt(i);

		CString src_fullpath;
		src_fullpath.Format("%s%s", lpszTgtRoot, contents_path);

		ciDEBUG(5,("DeleteFile(%s)", src_fullpath ));

		if( ::DeleteFile(src_fullpath)==0 )
		{
			// 파일삭제실패
			if( ::RemoveDirectory(src_fullpath)==0 )
				ciERROR(("DeleteFile and RemoveDirectory FAIL !!! (%s)", src_fullpath ));
		}
		else
		{
			// no_err
		}
	}
	return 0;
}

UINT UpdateCachingContentsThreadFunc(LPVOID pParam)
{
	ciDEBUG(5,("UpdateCachingContentsThreadFunc"));

	int sec = 0;
	while(CDEventHandler::_runCachingContentsThread)
	{
		if(++sec % GET_CACHING_CONTENTS_CYCLE != 0 && CDEventHandler::_isGetCachingContentsFromServer)
		{
			::Sleep(1000);
			continue;
		}
		else
		{
			sec = 0;
			::Sleep(1000);
		}

		CHttpRequest http_request;
		http_request.Init(CHttpRequest::CONNECTION_SERVER);

		CStringArray response_list;
		//BOOL ret_val = http_request.RequestGet("UBC_Server/get_no_caching_contents_list.asp", response_list);
		BOOL ret_val = http_request.RequestGet("UBC_Server/get_caching_contents_list.asp", response_list);

		// debug <--
		CString ip, url;
		UINT port;
		http_request.GetServerInfo(ip, port);
		ciDEBUG(5,("Connect_to(%s:%d,%s)", ip, port, http_request.GetRequestURL()));
		//

		if(ret_val == FALSE)
		{
			ciDEBUG(5,("GetErrorMsg(%s)", http_request.GetErrorMsg() ));
		}
		// -->

		int count = response_list.GetCount();
		if(count > 0)
		{
			const CString& line = response_list.GetAt(0);
			if(line == "Exist" || line == "Nothing")
			{
				CDEventHandler::_isGetCachingContentsFromServer = true;

				CStringArray contents_list;
				for(int i=0; i<count; i++)
				{
					const CString& line = response_list.GetAt(i);
					if(strnicmp(line, "contentsId=", 11) == 0)
					{
						int pos = 0;
						CString contents_id = line.Tokenize("=", pos);
						contents_id = line.Tokenize("=", pos);
						contents_list.Add(contents_id);
					}
				}

				CDEventHandler::InitCachingContentsList(contents_list);
				ciDEBUG(5,("CachingContentsList Count=%d", contents_list.GetCount() ));
			}
		}
	}

	return 0;
}
// -->
