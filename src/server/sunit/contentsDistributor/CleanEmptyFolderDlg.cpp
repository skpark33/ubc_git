// CleanEmptyFolderDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ContentsDistributor.h"
#include "CleanEmptyFolderDlg.h"


#define		WM_SCAN_COMPLETE_EMPTY_FOLDER		(WM_USER + 2049)


ciSET_DEBUG(10,"CCleanEmptyFolderDlg");


// CCleanEmptyFolderDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCleanEmptyFolderDlg, CDialog)

CCleanEmptyFolderDlg::CCleanEmptyFolderDlg(LPCTSTR lpszContentsPath, CWnd* pParent /*=NULL*/)
:	m_strContentsPath ( lpszContentsPath )
,	m_nThreadID ( 0 )
,	m_pRunThread ( NULL )
,	m_pThread ( NULL )
,	CDialog(CCleanEmptyFolderDlg::IDD, pParent)
{
}

CCleanEmptyFolderDlg::~CCleanEmptyFolderDlg()
{
	DeleteThread();
}

void CCleanEmptyFolderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_EMPTY_FOLDERS, m_lcEmptyFolder);
	DDX_Control(pDX, IDC_BUTTON_RESCAN, m_btnRescan);
	DDX_Control(pDX, IDOK, m_btnDeleteFolder);
	DDX_Control(pDX, IDCANCEL, m_btnClose);
}


BEGIN_MESSAGE_MAP(CCleanEmptyFolderDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_RESCAN, &CCleanEmptyFolderDlg::OnBnClickedButtonRescan)
	ON_MESSAGE(WM_SCAN_COMPLETE_EMPTY_FOLDER, OnScanCompleteEmptyFolder)
END_MESSAGE_MAP()


// CCleanEmptyFolderDlg 메시지 처리기입니다.

BOOL CCleanEmptyFolderDlg::OnInitDialog()
{
	ciDEBUG(1,("OnInitDialog()"));

	CDialog::OnInitDialog();

	//
	m_lcEmptyFolder.SetExtendedStyle(m_lcEmptyFolder.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_CHECKBOXES | LVS_EX_GRIDLINES );
	m_lcEmptyFolder.InsertColumn(0, "", 0, 26);
	m_lcEmptyFolder.InsertColumn(1, "Folder Name", 0, 280);
	m_lcEmptyFolder.InsertColumn(2, "Status", 0, 120);

	// start thread
	OnBnClickedButtonRescan();

	ciDEBUG(1,("~OnInitDialog()"));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCleanEmptyFolderDlg::OnOK()
{
	int cnt = m_lcEmptyFolder.GetItemCount();
	for(int i=0; i<cnt; i++)
	{
		if( m_lcEmptyFolder.GetCheck(i) )
		{
			CString str_path = m_strContentsPath;
			if( str_path.GetAt(str_path.GetLength()-1) != '\\' )
				str_path += "\\";
			str_path += m_lcEmptyFolder.GetItemText(i, 1);

			if( ::RemoveDirectory(str_path) )
			{
				ciDEBUG(1,("RemoveDirectory(%s)", str_path));
				m_lcEmptyFolder.SetCheck(i, 0);
				m_lcEmptyFolder.SetItemText(i, 2, "Delete");
			}
			else
			{
				CString msg;
				msg.Format("Fail to RemoveDirectory !!!\r\n\r\n%s", str_path);

				ciERROR(((LPCSTR)msg));

				m_lcEmptyFolder.SetItemText(i, 2, "Deleting ERROR !!!");
			}
		}
	}

	//CDialog::OnOK();
}

void CCleanEmptyFolderDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnCancel();
}

void CCleanEmptyFolderDlg::OnBnClickedButtonRescan()
{
	ciDEBUG(1,("OnBnClickedButtonRescan()"));

	//
	m_btnRescan.EnableWindow(FALSE);
	m_btnDeleteFolder.EnableWindow(FALSE);

	//
	DeleteThread();

	//
	ClearList();

	m_lcEmptyFolder.InsertItem(0, "");
	m_lcEmptyFolder.SetItemText(0, 1, "Scanning...");

	//
	m_pRunThread = new bool;
	*m_pRunThread = true;

	EMPTY_CONTENTS_SCAN_PARAM* param = new EMPTY_CONTENTS_SCAN_PARAM;
	param->hParentWnd = GetSafeHwnd();
	param->nThreadID = m_nThreadID;
	param->pRunThread = m_pRunThread;
	param->strContentsPath = m_strContentsPath;
	param->pEmptyFolderList = new CStringArray;

	m_pThread = ::AfxBeginThread(EmptyContentsScanThread, (LPVOID)param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
	m_pThread->m_bAutoDelete = true;
	m_pThread->ResumeThread();

	ciDEBUG(1,("~OnBnClickedButtonRescan()"));
}

void CCleanEmptyFolderDlg::DeleteThread()
{
	ciDEBUG(1,("DeleteThread()"));

	m_nThreadID++;

	if( m_pRunThread ) *m_pRunThread = false;

	m_pRunThread = NULL;
	m_pThread = NULL;

	ciDEBUG(1,("~DeleteThread()"));
}

void CCleanEmptyFolderDlg::ClearList()
{
	m_lcEmptyFolder.DeleteAllItems();
}

LRESULT CCleanEmptyFolderDlg::OnScanCompleteEmptyFolder(WPARAM wParam, LPARAM lParam)
{
	UINT thread_id = (UINT)wParam;
	CStringArray* empty_folder_list = (CStringArray*)lParam;

	ciDEBUG(1,("OnScanCompleteEmptyFolder(%d)", thread_id));

	if( thread_id == m_nThreadID )
	{
		int cnt = empty_folder_list->GetCount();
		if( cnt == 0 )
		{
			m_lcEmptyFolder.SetItemText(0, 1, "No Empty Folder");
		}
		else
		{
			ClearList();
			m_btnRescan.EnableWindow();
			m_btnDeleteFolder.EnableWindow();

			for(int i=0; i<cnt; i++)
			{
				m_lcEmptyFolder.InsertItem(i, "");
				m_lcEmptyFolder.SetItemText(i, 1, empty_folder_list->GetAt(i));
				m_lcEmptyFolder.SetCheck(i);
			}
		}
	}
	else
	{
		ciWARN(("Not equal Thread-ID(%d, %d)", thread_id, m_nThreadID));
	}

	if( empty_folder_list )
		delete empty_folder_list;

	return 1;
}

bool GetFolderFileList(LPCTSTR lpszPath, CStringArray& listFolderFile, bool bIncludeFolder, bool bIncludeFile)
{
	CString str_full_path = lpszPath;
	listFolderFile.RemoveAll();

	if( str_full_path.GetAt(str_full_path.GetLength()-1) != '\\' )
		str_full_path += "\\";
	str_full_path += "*.*";

	CFileFind ff;
	BOOL bWorking = ff.FindFile(str_full_path);

	while (bWorking)
	{
		bWorking = ff.FindNextFile();

		if (ff.IsDots()) continue;
		if (!bIncludeFolder && ff.IsDirectory()) continue;
		if (!bIncludeFile && !ff.IsDirectory()) continue;

		listFolderFile.Add(ff.GetFileName());
	}
	ff.Close();

	return true;
}

UINT EmptyContentsScanThread(LPVOID pParam)
{
	//
	EMPTY_CONTENTS_SCAN_PARAM* param = (EMPTY_CONTENTS_SCAN_PARAM*)pParam;

	HWND	hParentWnd	= param->hParentWnd;
	UINT	nThreadID	= param->nThreadID;
	bool*	pRunThread	= param->pRunThread;
	CString	strContentsPath = param->strContentsPath;
	CStringArray* pEmptyFolderList = param->pEmptyFolderList;

	delete param;

	//
	CStringArray folder_list;
	GetFolderFileList(strContentsPath, folder_list, true, false);

	int cnt = folder_list.GetCount();
	for(int i=0; i<cnt; i++)
	{
		CString str_path = strContentsPath;
		if( str_path.GetAt(str_path.GetLength()-1) != '\\' )
			str_path += "\\";
		str_path += folder_list.GetAt(i);

		CStringArray sub_folder_file_list;
		GetFolderFileList(str_path, sub_folder_file_list, true, true);

		ciDEBUG(1,("Scan(%s=%d)", str_path, sub_folder_file_list.GetCount()));

		if( sub_folder_file_list.GetCount() == 0 )
		{
			ciDEBUG(1,("Find Empty Folder(%s)", folder_list.GetAt(i)));
			pEmptyFolderList->Add(folder_list.GetAt(i));
		}
	}

	//
	if( ::IsWindow(hParentWnd) )
	{
		ciDEBUG(1,("Scan Complete(%d)", nThreadID));
		::PostMessage(hParentWnd, WM_SCAN_COMPLETE_EMPTY_FOLDER, (WPARAM)nThreadID, (LPARAM)pEmptyFolderList);
	}
	else
	{
		ciWARN(("No Parent Wnd !!!"));
	}

	delete pRunThread;

	return 0;
}
