// SetSyncTimeDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ContentsDistributor.h"
#include "SetSyncTimeDlg.h"

#include "Lib.h"


// CSetSyncTimeDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSetSyncTimeDlg, CDialog)

CSetSyncTimeDlg::CSetSyncTimeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSetSyncTimeDlg::IDD, pParent)
{

}

CSetSyncTimeDlg::~CSetSyncTimeDlg()
{
}

void CSetSyncTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_SET_SYNC_TIME, m_chkSetSyncTime);
	DDX_Control(pDX, IDC_TIMEPICKER, m_dtcSyncTime);
}


BEGIN_MESSAGE_MAP(CSetSyncTimeDlg, CDialog)
	ON_BN_CLICKED(IDC_CHECK_SET_SYNC_TIME, &CSetSyncTimeDlg::OnBnClickedCheckSetSyncTime)
END_MESSAGE_MAP()


// CSetSyncTimeDlg 메시지 처리기입니다.

BOOL CSetSyncTimeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//
	m_dtcSyncTime.SetFormat("HH:mm");

	//
	TCHAR buf[255] = {0};
	::GetPrivateProfileString("Settings", "UsingSyncTime", "1", buf, 255, ::GetINIPath());
	m_chkSetSyncTime.SetCheck(_ttoi(buf));
	m_dtcSyncTime.EnableWindow(_ttoi(buf));

	//
	::ZeroMemory(buf, 0);
	::GetPrivateProfileString("Settings", "SyncTime", DEFAULT_SYNC_TIME, buf, 255, ::GetINIPath());

	CString str;
	if(_tcslen(buf) != 5 || buf[2] != _T(':') ||
		buf[0] < _T('0') || buf[0] > _T('9') ||
		buf[1] < _T('0') || buf[1] > _T('9') ||
		buf[3] < _T('0') || buf[3] > _T('9') ||
		buf[4] < _T('0') || buf[4] > _T('9') )
			str = DEFAULT_SYNC_TIME;
	else
			str = buf;

	int pos = 0;
	int hour = _ttoi(str.Tokenize(_T(":"), pos));
	int minute = _ttoi(str.Tokenize(_T(":"), pos));

	CTime cur_time = CTime::GetCurrentTime();
	CTime sync_time(cur_time.GetYear(),cur_time.GetMonth(),cur_time.GetDay(),hour,minute,0);

	m_dtcSyncTime.SetTime(&sync_time);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSetSyncTimeDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	TCHAR buf[255] = {0};

	int check = m_chkSetSyncTime.GetCheck();
	_stprintf(buf, _T("%d"), check);
	::WritePrivateProfileString("Settings", "UsingSyncTime", buf, ::GetINIPath());

	CTime sync_time;
	m_dtcSyncTime.GetTime(sync_time);
	_stprintf(buf, _T("%02d:%02d"), sync_time.GetHour(), sync_time.GetMinute());
	::WritePrivateProfileString("Settings", "SyncTime", buf, ::GetINIPath());

	CDialog::OnOK();
}

void CSetSyncTimeDlg::OnBnClickedCheckSetSyncTime()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_dtcSyncTime.EnableWindow(m_chkSetSyncTime.GetCheck());
}
