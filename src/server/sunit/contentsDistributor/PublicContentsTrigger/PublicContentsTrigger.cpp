// PublicContentsTrigger.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "PublicContentsTrigger.h"
#include "PublicContentsTriggerDlg.h"

#include "../PublicContentsInfo.h"
#include "../Control/Lib.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPublicContentsTriggerApp

BEGIN_MESSAGE_MAP(CPublicContentsTriggerApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CPublicContentsTriggerApp construction

CPublicContentsTriggerApp::CPublicContentsTriggerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CPublicContentsTriggerApp object

CPublicContentsTriggerApp theApp;


// CPublicContentsTriggerApp initialization

BOOL CPublicContentsTriggerApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	if(__argc < 3)
	{
		::AfxMessageBox("PublicContentsTrigger [source_path] [target_path]", MB_ICONWARNING);
		return FALSE;
	}
	else
	{
		PUBLIC_CONTENTS_PATH pcp = {0};

		strcpy(pcp.source_path, __argv[1]);
		strcpy(pcp.target_path, __argv[2]);

		TRACE("s:%s\r\n", pcp.source_path);
		TRACE("t:%s\r\n", pcp.target_path);

		CString lock_file = GetMainPath();
		lock_file += DEFAULT_LOCK_FILE;

		CPublicContentsInfo info;
		BOOL ret_val = info.LockPushBack(lock_file, pcp.source_path, pcp.target_path);

		if(ret_val)	::exit(1);
		else		::exit(0);
	}

	CPublicContentsTriggerDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
