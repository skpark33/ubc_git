#include "stdafx.h"
/*! \file CDWorld.C
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
*/

//#include "stdafx.h"
//#include "MsgBox.h"
#include "CDWorld.h"

#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciEnv.h>
#include <CMN/libInstall/installUtil.h>
#include <CMN/libCommon/ubcIni.h>
#include "libContentsDistributor/CDManager.h"


ciSET_DEBUG(10, "CDWorld");

CDWorld::CDWorld() {
	ciDEBUG(1,("CDWorld()"));
}

CDWorld::~CDWorld() {
	ciDEBUG(1,("~CDWorld()"));
}

ciBoolean 
CDWorld::init(int& p_argc, char** &p_argv) 
{
	ciDEBUG(1,("CDWorld::init(%d)", p_argc));	
	
	if(cciORBWorld::init(p_argc,p_argv) == ciFalse) {
		ciERROR(("ciWorld::init() is FAILED"));
		return ciFalse;
	}
	//const char*  ignoreDebugEnv = getenv("IGNORE_COP_DEBUG");
	//if(ignoreDebugEnv==0 || ignoreDebugEnv[0] != '1'){
	//	ciDebug::setDebugOn();
	//	ciWorld::ylogOn();
	//}
	//ciEnv::defaultEnv(true,"ubc");
	if(ciWorld::hasWatcher() == ciTrue) {}

	ciString mgrId;
	ciArgParser::getInstance()->getArgValue("+id", mgrId);
	if(mgrId.empty()){
		ciERROR(("+id is null"));
	}
	CDManager::getInstance()->init(mgrId.c_str());

	return ciTrue;
}


ciBoolean
CDWorld::fini(long p_finiCode)
{
	ciDEBUG(1,("fini()"));
	ciWorld::ylogOff();
	return ciWorld::fini(p_finiCode);
}



