#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CCleanEmptyFolderDlg 대화 상자입니다.

class CCleanEmptyFolderDlg : public CDialog
{
	DECLARE_DYNAMIC(CCleanEmptyFolderDlg)

public:
	CCleanEmptyFolderDlg(LPCTSTR lpszContentsPath, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCleanEmptyFolderDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CLEAN_EMPTY_FOLDER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CString		m_strContentsPath;

	UINT		m_nThreadID;
	bool*		m_pRunThread;
	CWinThread*	m_pThread;
	void		DeleteThread();

	void		ClearList();

public:
	afx_msg void OnBnClickedButtonRescan();
	afx_msg LRESULT OnScanCompleteEmptyFolder(WPARAM wParam, LPARAM lParam);

	CListCtrl	m_lcEmptyFolder;
	CButton		m_btnRescan;
	CButton		m_btnDeleteFolder;
	CButton		m_btnClose;
};

static bool GetFolderFileList(LPCTSTR lpszPath, CStringArray& listFolderFile, bool bIncludeFolder, bool bIncludeFile);
static UINT EmptyContentsScanThread(LPVOID pParam);

typedef struct {
	HWND	hParentWnd;
	UINT	nThreadID;
	bool*	pRunThread;
	CString	strContentsPath;
	CStringArray* pEmptyFolderList;
} EMPTY_CONTENTS_SCAN_PARAM;
