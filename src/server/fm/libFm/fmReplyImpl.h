#ifndef _fmReplyImpl_h_
#define _fmReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciScopeInfo.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciException.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class fmReplyImpl : public virtual aciReply {
public:
	fmReplyImpl();
	virtual ~fmReplyImpl() {};

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

    virtual ciBoolean _get();
    
	ciBoolean _requestAlarmSummary();

protected:
	ciString			_mgrId;
};


#endif // _fmReplyImpl_h_
