#ifndef _faultReplyImpl_h_
#define _faultReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <cci/libUtil/cciStringPropMap.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class faultReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	faultReplyImpl( const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciFalse,
            int memsize=100 )
    : aciReply(className,psType,psName,psOwner,memsize) {}
	virtual ~faultReplyImpl() ;

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

    virtual ciBoolean _create();
    virtual ciBoolean _remove();
    virtual ciBoolean _removeFault();
    virtual ciBoolean _set();

	//ciBoolean _moveMatchUpAlarm(cciEntity& source, ciString& probableCause);

};

#endif // _faultReplyImpl_h_
