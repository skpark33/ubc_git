#ifndef _processStateEventHandler_h_
#define _processStateEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

#include <common/libCommon/utvUtil.h>


class processStateEventHandler : public cciEventHandler {
public:
	static processStateEventHandler*	getInstance();
	static void	clearInstance();

	virtual ~processStateEventHandler() ;

   	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean			addHand();


protected:
	processStateEventHandler();

	long	_handId;

	static processStateEventHandler*	_instance;
	static ciMutex			_instanceLock;

};


#endif // _processStateEventHandler_h_
