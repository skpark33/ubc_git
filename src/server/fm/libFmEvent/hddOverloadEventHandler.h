#ifndef _hddOverloadEventHandler_h_
#define _hddOverloadEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

#include <common/libCommon/utvUtil.h>


class hddOverloadEventHandler : public cciEventHandler {
public:
	static hddOverloadEventHandler*	getInstance();
	static void	clearInstance();

	virtual ~hddOverloadEventHandler() ;

   	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean			addHand();


protected:
	hddOverloadEventHandler();

	long	_handId;
	long	_handId1;
	long	_handId2;

	static hddOverloadEventHandler*	_instance;
	static ciMutex			_instanceLock;

};


#endif // _hddOverloadEventHandler_h_
