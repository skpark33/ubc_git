#ifndef _fmUtil_h_
#define _fmUtil_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciTime.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

class fmoInfo {
public:
	fmoInfo() : duration(7), dirty(ciFalse) {}
	ciLong duration;
	ciStringList probableCauseList;
	ciStringList excludeList;
	ciStringList includeList;
	ciBoolean	dirty;
	ciMutex	 lock;

	ciBoolean getFMO(ciLong& pduration,
			ciStringList& pprobableCauseList, 
			ciStringList& pexcludeList, 
			ciStringList& pincludeList);
	void setFMO(ciLong pduration,
			ciStringList& pprobableCauseList, 
			ciStringList& pexcludeList, 
			ciStringList& pincludeList);
};

class fmHostInfo {
public:
	ciString	siteId;
	ciString	hostId;
	ciBoolean	adminState;
	ciBoolean	operationalState;
	ciString	startupTime;
	ciString	shutdownTime;
//	ciTime		lastInsertTime;
//	ciString	siteOpenTime;
//	ciString	siteCloseTime;
};
typedef map<string,fmHostInfo*>	fmHostInfoMap;

class simpleFault {
public:
	simpleFault() { counter=1;}
	ciString siteId;
	ciString hostId;
	ciString faultId;
	ciLong counter;
	ciTime updateTime;
};
typedef list<simpleFault>	simpleFaultList;

class complexFault : public simpleFault {
public:
	complexFault() { counter=1;}
	ciLong severity;
	ciString cause;
	ciString additionalText;
};
typedef list<complexFault>	complexFaultList;

class fmUtil {
public:
	static fmUtil*	getInstance();
	static void	clearInstance();

	virtual ~fmUtil() ;

	ciBoolean		isFault(const char* siteId, 
							const char* hostId, 
							const char* cause );	

	int				lookupAlarm(const char* siteId, 
								const char* hostId, 
								ciLong		severity,
								const char* cause,
								simpleFaultList& outList);	

	int				lookupAlarm(const char* siteId, 
								const char* hostId, 
								const char*	faultId,
								simpleFaultList& outList);

	int				lookupAlarm(const char* siteId, 
								const char* hostId, 
								complexFaultList& outList);	

	ciBoolean 		updateFault(const char* siteId, const char* hostId,const char* faultId,ciLong counter); 
	int 			updateFault(simpleFaultList& inList);

	ciBoolean		updateHostInfo(const char* siteId, const char* hostId, 
								const char* attrName, ciBoolean newBool, const char* newStr);

	ciBoolean		removeFault(const char* siteId, const char* hostId,const char* faultId); 

	ciBoolean		removeFaultCall(const char* siteId, 
								const char* hostId, 
								const char* faultId,
								ciLong		severity,
								const char* cause,
								const char* additionalText="");
	int				removeFault(const char* siteId, 
								const char* hostId, 
								const char* faultId,
								ciLong		severity,
								const char* cause,
								const char* additionalText="");
	int				removeFault(const char* siteId, 
								const char* hostId, 
								ciLong		severity,
								const char* cause,
								const char* additionalText="");
	int				removeFault(const char* siteId, const char* hostId);

	ciBoolean		createFault(const char* siteId, 
								const char* hostId, 
								ciLong		severity,
								const char* cause,
								const char* additionalText="");


	ciBoolean		isFMOExist();
	int				getFMO(ciLong& pduration,
						ciStringList& pprobableCauseList, 
						ciStringList& pexcludeList, 
						ciStringList& pincludeList);
	int				setFMO(ciLong pduration,
						ciStringList& pprobableCauseList, 
						ciStringList& pexcludeList, 
						ciStringList& pincludeList);

	ciBoolean		filterIn(const char* cause,const char* hostId);
	fmHostInfo*		getHostInfo(const char* siteId, const char* hostId);

	ciBoolean		postFaultAlarm(const char* faultId,
							const char* siteId, 
							const char* hostId,
							ciLong		severity,
							const char* cause,
							const char* additionalText,
							ciLong counter,
							ciTime& updateTime);
	ciBoolean		createFaultLog(const char* faultId,
							const char* siteId, 
							const char* hostId,
							ciLong		severity,
							const char* cause,
							const char* additionalText,
							ciLong counter,
							ciTime& updateTime);


protected:
	fmUtil();

	static fmUtil*	_instance;
	static ciMutex			_instanceLock;

	fmoInfo  _fmoInfo;
	fmHostInfoMap	_hostmap;
	ciMutex			_hostmapLock;

};

#endif // _fmUtil_h_
