#ifndef _alarmManager_h_
#define _alarmManager_h_
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

typedef map<ciString, int>		SummaryMap;

class alarmManager   {
public:
	static alarmManager*	getInstance();
	static void	clearInstance();

	virtual		~alarmManager() ;
	void		clear();
	int			load();
	ciBoolean			update(const char* probableCause, int severity);
	ciBoolean	postAlarmSummary();
	const char* toString();

protected:
	static alarmManager*	_instance;
	static ciMutex		_instanceLock;

	alarmManager() ;
	int			_update(const char* probableCause, int severity);

	SummaryMap	_map;
	ciMutex		_mapLock;

	ciString	_strBuf;

};

#endif // _alarmManager_h_
