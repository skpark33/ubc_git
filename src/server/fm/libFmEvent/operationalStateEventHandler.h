#ifndef _operationalStateEventHandler_h_
#define _operationalStateEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

#include <common/libCommon/utvUtil.h>


class operationalStateEventHandler : public cciEventHandler {
public:
	static operationalStateEventHandler*	getInstance();
	static void	clearInstance();

	virtual ~operationalStateEventHandler() ;

   	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean			addHand();


protected:
	operationalStateEventHandler();

	long	_handId;

	static operationalStateEventHandler*	_instance;
	static ciMutex			_instanceLock;

};


#endif // _operationalStateEventHandler_h_
