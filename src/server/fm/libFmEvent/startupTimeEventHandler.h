#ifndef _startupTimeEventHandler_h_
#define _startupTimeEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

#include <common/libCommon/utvUtil.h>


class startupTimeEventHandler : public cciEventHandler {
public:
	static startupTimeEventHandler*	getInstance();
	static void	clearInstance();

	virtual ~startupTimeEventHandler() ;

   	virtual void 		processEvent(cciEvent& pEvent);
	ciBoolean			addHand();


protected:
	startupTimeEventHandler();

	long	_handId;

	static startupTimeEventHandler*	_instance;
	static ciMutex			_instanceLock;

};


#endif // _startupTimeEventHandler_h_
