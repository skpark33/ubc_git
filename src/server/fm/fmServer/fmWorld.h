 /*! \file fmWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _fmWorld_h_
#define _fmWorld_h_

#include <aci/libCall/aciCallWorld.h> 
//#include <server/fm/libFmEvent/heartbeatMon.h>


class fmWorld : public aciCallWorld {
public:
	fmWorld();
	~fmWorld();
 	
	//[
	//	From aciCallWorld
	//
	virtual aciCall*    newCallImpl(const char* pname);
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);
	//
	//]

protected:	

	//ciBoolean	_startMonitor(int period);
	//heartbeatMon*		_heartbeatMon;
	

};
		
#endif //_fmWorld_h_
