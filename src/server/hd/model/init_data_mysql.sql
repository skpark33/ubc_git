
--sm_hd
insert into `sm_hd`(`hdId`) values ('1');




--sm_application


--naming
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','naming',null,null,null,'INIT','${ACE_ROOT}/bin/Naming_Service','${PROJECT_HOME}/log/naming','BG','-m 1 -o ns.ior -ORBEndPoint iiop://${NAMESERVER_HOST}:${NAMESERVER_PORT}',1,1,1,3);

--irserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','irserver',null,null,null,'naming','${ACE_ROOT}/bin/IFR_Service','${PROJECT_HOME}/log/irserver','BG','-o ir.ior -ORBEndPoint iiop://${NAMESERVER_HOST}:${IR_PORT} -ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService',1,1,1,3);

--putirall
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','putirall',null,null,null,'irserver','${PROJECT_HOME}/util/script/putirall','${PROJECT_HOME}/log/putirall','FG',null,0,1,1,3);

--nbgen
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','nbgen',null,null,null,'putirall','${COP_HOME}/bin/${PLATFORM}/nbgen','${PROJECT_HOME}/log/nbgen','FG','+noidl -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository +Debug all',0,1,1,3);

--notiinfogen
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','notiInfoGen',null,null,null,'nbgen','${COP_HOME}/bin/${PLATFORM}/notiInfoGen','${PROJECT_HOME}/log/notiInfoGen','FG',null,0,1,1,3);

--dummysm
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','dummySm',null,null,null,'notiInfoGen','${COP_HOME}/bin/${PLATFORM}/dummySm','${PROJECT_HOME}/log/dummySm','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +Debug all',1,1,1,3);

--xmirserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','xMirServer','MIR','MIR','GW','dummySm','${COP_HOME}/bin/${PLATFORM}/xMirServer','${PROJECT_HOME}/log/xMirServer','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +recache +module ALL +mirId ManagedElement +Debug all ',1,1,1,3);

--notimgr1
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','notiMgr1','NotiManager','MGR01','FW','xMirServer','${COP_HOME}/bin/${PLATFORM}/notifymanager','${PROJECT_HOME}/log/notiMgr1','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBEndpoint iiop://${NAMESERVER_HOST}:${NOTI_MGR1_PORT} +name FW=FW/Manager=M01 +Mgr M01 +prefilter +noDS +logPolicy NOTIFY_DEBUG_POLICY +Debug all',1,1,1,3);

--spmanager
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','spManager','SPM','M01',null,'notiMgr1','${COP_HOME}/bin/${PLATFORM}/COP_spManager','${PROJECT_HOME}/log/spManager','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +noDS +pretend +Debug all',1,1,1,3);

--spserver1
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','spServer1','SP','SP01',null,'spManager','${COP_HOME}/bin/${PLATFORM}/COP_spServer','${PROJECT_HOME}/log/spServer1','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository +spip ${NAMESERVER_HOST} +spport 14151 +noDS +noES +Debug all',1,1,1,3);

--spserver2
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','spServer2','SP','SP02',null,'spServer1','${COP_HOME}/bin/${PLATFORM}/COP_spServer','${PROJECT_HOME}/log/spServer2','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository +spip ${NAMESERVER_HOST} +spport 14152 +noDS +noES +Debug all',1,1,1,3);

--spserver3
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','spServer3','SP','SP03',null,'spServer2','${COP_HOME}/bin/${PLATFORM}/COP_spServer','${PROJECT_HOME}/log/spServer3','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository +spip ${NAMESERVER_HOST} +spport 14153 +noDS +noES +Debug all',1,1,1,3);

--timeserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','timeServer',null,null,null,'spServer3','${COP_HOME}/bin/${PLATFORM}/COP_timeServer','${PROJECT_HOME}/log/timeServer','BG','14106',1,1,1,3);

--pfserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','pfServer','PF','PF','FW','timeServer','${COP_HOME}/bin/${PLATFORM}/pfServer','${PROJECT_HOME}/log/pfServer','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29',1,1,1,3);

--odserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','odServer','OD','OD','OD','pfServer','${PROJECT_HOME}/bin/${PLATFORM}/odServer','${PROJECT_HOME}/log/odServer','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +appId OD +noES +Debug all',1,1,1,3);

--fmserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','fmServer','FM','1','FM','odServer','${PROJECT_HOME}/bin/${PLATFORM}/fmServer','${PROJECT_HOME}/log/fmServer','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +appId FM  +noES +Debug all',1,1,1,3);

--dmserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','dmServer','DM','1','DM','fmServer','${PROJECT_HOME}/bin/${PLATFORM}/dmServer','${PROJECT_HOME}/log/dmServer','BG','+appId DM +noES +insert +Debug all',1,1,1,3);

--egserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','egServer',null,null,null,'dmServer','${PROJECT_HOME}/bin/${PLATFORM}/egServer','${PROJECT_HOME}/log/egServer','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository +Debug ciMySQL',1,1,1,3);

--scgserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','scgServer',null,null,null,'egServer','${PROJECT_HOME}/bin/${PLATFORM}/scgServer','${PROJECT_HOME}/log/scgServer','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository',1,1,1,3);

--pmserver1
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','pmServer1','PM','1','PM','scgServer','${PROJECT_HOME}/bin/${PLATFORM}/pmServer','${PROJECT_HOME}/log/pmServer1','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +appId PM +noES +Debug all',1,0,1,3);

--pmserver2
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','pmServer2','PM','2','PM','pmServer1','${PROJECT_HOME}/bin/${PLATFORM}/pmServer','${PROJECT_HOME}/log/pmServer2','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +appId PM +noES +Debug all',1,0,1,3);

--agtserver1
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','agtServer1','AGTServer','1','AGENT','pmServer2','${PROJECT_HOME}/bin/${PLATFORM}/agtServer','${PROJECT_HOME}/log/agtServer1','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +appId AGENTSERVER +noES +dontDown +Debug all',1,0,1,3);

--agtserver2
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','agtServer2','AGTServer','2','AGENT','agtServer1','${PROJECT_HOME}/bin/${PLATFORM}/agtServer','${PROJECT_HOME}/log/agtServer2','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +appId AGENTSERVER +noES +dontDown +Debug all',1,0,1,3);

--lmserver1
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','lmServer1','LM','1','LM','agtServer2','${PROJECT_HOME}/bin/${PLATFORM}/lmServer','${PROJECT_HOME}/log/lmServer1','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +appId LM +noES +Debug all',1,0,1,3);

--lmserver2
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','lmServer2','LM','2','LM','lmServer1','${PROJECT_HOME}/bin/${PLATFORM}/lmServer','${PROJECT_HOME}/log/lmServer2','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +appId LM +noES +Debug all',1,0,1,3);

--hdserver
insert into `sm_application`(`hdId`,`appId`,`appKind`,`appIrId`,`appDomain`,`preRunningEntity`,`binaryName`,`runningDir`,`runningType`,`arguments`,`automaticStartup`,`isPrimitive`,`monitored`,`initSleepSecond`) values ('1','hdServer','HD','1','SM','hdServer','${PROJECT_HOME}/bin/${PLATFORM}/hdServer','${PROJECT_HOME}/log/hdServer','BG','-ORBInitRef NameService=corbaloc:iiop:${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:${NAMESERVER_HOST}:${IR_PORT}/InterfaceRepository -ORBEndpoint iiop://${NAMESERVER_HOST}:14120/portspan=29 +noES +Debug all',1,0,1,3);

