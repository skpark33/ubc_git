
CREATE TABLE dbo.sm_application (
   hdId VARCHAR(255) NOT NULL,
   appId VARCHAR(255) NOT NULL,
   appKind VARCHAR(255) DEFAULT NULL,
   appIrId VARCHAR(255) DEFAULT NULL,
   appDomain VARCHAR(255) DEFAULT NULL,
   preRunningEntity VARCHAR(255) DEFAULT NULL,
   binaryName VARCHAR(255) DEFAULT NULL,
   runningDir VARCHAR(255) DEFAULT NULL,
   runningType VARCHAR(255) DEFAULT 'BG',
   arguments VARCHAR(1024) DEFAULT NULL,
   automaticStartup DECIMAL(1) DEFAULT '1',
   isPrimitive DECIMAL(1) DEFAULT '0',
   monitored DECIMAL(1) DEFAULT '1',
   hasAgent DECIMAL(1) DEFAULT '0',
   appStatus VARCHAR(255) DEFAULT 'INACTIVE',
   adminState DECIMAL(1) DEFAULT '0',
   pid DECIMAL(10) DEFAULT NULL,
   appIOR VARCHAR(255) DEFAULT NULL,
   startTime DATETIME DEFAULT NULL,
   initSleepSecond DECIMAL(5) DEFAULT '3',
   cpuUsed DECIMAL(5,2) DEFAULT NULL,
   realMemoryUsed DECIMAL(10) DEFAULT NULL,
   virtualMemoryUsed DECIMAL(10) DEFAULT NULL,
   realMemoryThreshold DECIMAL(10) DEFAULT NULL,
   virtualMemoryThreshold DECIMAL(10) DEFAULT NULL,
   thresholdRestart DECIMAL(1) DEFAULT '0',
   remarks VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (hdId, appId),
  UNIQUE KEY `UQ__sm_application` (`hdId`,`appId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE dbo.sm_dbms (
   dbmsId VARCHAR(255) NOT NULL,
   serviceName VARCHAR(255) DEFAULT NULL,
   remarks VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (dbmsId),
  UNIQUE KEY `UQ__sm_dbms` (`dbmsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE dbo.sm_filesystem (
   hdId VARCHAR(255) NOT NULL,
   filesystemId VARCHAR(255) NOT NULL,
   filesystemName VARCHAR(255) DEFAULT NULL,
   filesystemBlocks DECIMAL(10,0) DEFAULT NULL,
   filesystemUsed DECIMAL(10,0) DEFAULT NULL,
   filesystemAvailable DECIMAL(10,0) DEFAULT NULL,
   filesystemCapacity DECIMAL(5,2) DEFAULT NULL,
   filesystemMountedOn VARCHAR(255) DEFAULT NULL,
   filesystemMonitored DECIMAL(1,0) DEFAULT '1',
   remarks VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (hdId, filesystemId),
  UNIQUE KEY `UQ__sm_filesystem` (`hdId`,`filesystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE dbo.sm_hd (
   hdId VARCHAR(255) NOT NULL,
   pid VARCHAR(255) DEFAULT NULL,
   startTime DATETIME DEFAULT NULL,
   directorStatus VARCHAR(255) DEFAULT NULL,
   remarks VARCHAR(255) DEFAULT NULL,
   hostName VARCHAR(255) DEFAULT NULL,
   hostStatus VARCHAR(255) DEFAULT NULL,
   hostIp VARCHAR(255) DEFAULT NULL,
   os VARCHAR(255) DEFAULT NULL,
   cpuCount DECIMAL(5,0) DEFAULT NULL,
   appMonitoringPeriod DECIMAL(5,0) DEFAULT '60',
   hostMonitoringPeriod DECIMAL(5,0) DEFAULT '60',
   appMonitoringFlag DECIMAL(1,0) DEFAULT '1',
   hostMonitoringFlag DECIMAL(1,0) DEFAULT '1',
   cpuThreshold DECIMAL(5,2) DEFAULT '0',
   memoryThreshold DECIMAL(5,2) DEFAULT '0',
   filesystemThreshold DECIMAL(5,2) DEFAULT '0',
   cpuUsed DECIMAL(5,2) DEFAULT NULL,
   realMemoryUsed DECIMAL(10,0) DEFAULT NULL,
   realMemoryTotal DECIMAL(10,0) DEFAULT NULL,
   virtualMemoryUsed DECIMAL(10,0) DEFAULT NULL,
   virtualMemoryTotal DECIMAL(10,0) DEFAULT NULL,
   swapInUsed DECIMAL(10,0) DEFAULT NULL,
   lastUpdateTime DATETIME DEFAULT NULL,
  PRIMARY KEY (hdId),
  UNIQUE KEY `UQ__sm_hd` (`hdId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
