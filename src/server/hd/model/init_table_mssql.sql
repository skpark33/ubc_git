CREATE TABLE [dbo].[sm_application](
	[hdId] [varchar](255) NOT NULL,
	[appId] [varchar](255) NOT NULL,
	[appKind] [varchar](255) NULL,
	[appIrId] [varchar](255) NULL,
	[appDomain] [varchar](255) NULL,
	[preRunningEntity] [varchar](255) NULL,
	[binaryName] [varchar](255) NULL,
	[runningDir] [varchar](255) NULL,
	[runningType] [varchar](255) NULL,
	[arguments] [varchar](1024) NULL,
	[automaticStartup] [decimal](1, 0) NULL,
	[isPrimitive] [decimal](1, 0) NULL,
	[monitored] [decimal](1, 0) NULL,
	[hasAgent] [decimal](1, 0) NULL,
	[appStatus] [varchar](255) NULL,
	[adminState] [decimal](1, 0) NULL,
	[pid] [decimal](10, 0) NULL,
	[appIOR] [varchar](255) NULL,
	[startTime] [datetime] NULL,
	[initSleepSecond] [decimal](5, 0) NULL,
	[cpuUsed] [decimal](5, 2) NULL,
	[realMemoryUsed] [decimal](10, 0) NULL,
	[virtualMemoryUsed] [decimal](10, 0) NULL,
	[realMemoryThreshold] [decimal](10, 0) NULL,
	[virtualMemoryThreshold] [decimal](10, 0) NULL,
	[thresholdRestart] [decimal](1, 0) NULL,
	[remarks] [varchar](255) NULL,
 CONSTRAINT [PK_ubc_application] PRIMARY KEY CLUSTERED 
(
	[hdId] ASC,
	[appId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_ubc_application] UNIQUE NONCLUSTERED 
(
	[hdId] ASC,
	[appId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[sm_application] ADD  CONSTRAINT [DF_sm_application_runningType]  DEFAULT ('BG') FOR [runningType]
GO

ALTER TABLE [dbo].[sm_application] ADD  CONSTRAINT [DF_sm_application_automaticStartup]  DEFAULT ((1)) FOR [automaticStartup]
GO

ALTER TABLE [dbo].[sm_application] ADD  CONSTRAINT [DF_sm_application_isPrimitive]  DEFAULT ((0)) FOR [isPrimitive]
GO

ALTER TABLE [dbo].[sm_application] ADD  CONSTRAINT [DF_sm_application_monitored]  DEFAULT ((1)) FOR [monitored]
GO

ALTER TABLE [dbo].[sm_application] ADD  CONSTRAINT [DF_sm_application_hasAgent]  DEFAULT ((0)) FOR [hasAgent]
GO

ALTER TABLE [dbo].[sm_application] ADD  CONSTRAINT [DF_sm_application_appStatus]  DEFAULT ('INACTIVE') FOR [appStatus]
GO

ALTER TABLE [dbo].[sm_application] ADD  CONSTRAINT [DF_sm_application_adminState]  DEFAULT ((0)) FOR [adminState]
GO

ALTER TABLE [dbo].[sm_application] ADD  CONSTRAINT [DF_sm_application_initSleepSecond]  DEFAULT ((3)) FOR [initSleepSecond]
GO

ALTER TABLE [dbo].[sm_application] ADD  CONSTRAINT [DF_sm_application_thresholdRestart]  DEFAULT ((0)) FOR [thresholdRestart]
GO



CREATE TABLE [dbo].[sm_dbms](
	[dbmsId] [varchar](255) NOT NULL,
	[serviceName] [varchar](255) NULL,
	[remarks] [varchar](255) NULL,
 CONSTRAINT [PK_ubc_dbms] PRIMARY KEY CLUSTERED 
(
	[dbmsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



CREATE TABLE [dbo].[sm_filesystem](
	[hdId] [varchar](255) NOT NULL,
	[filesystemId] [varchar](255) NOT NULL,
	[filesystemName] [varchar](255) NULL,
	[filesystemBlocks] [decimal](10, 0) NULL,
	[filesystemUsed] [decimal](10, 0) NULL,
	[filesystemAvailable] [decimal](10, 0) NULL,
	[filesystemCapacity] [decimal](5, 2) NULL,
	[filesystemMountedOn] [varchar](255) NULL,
	[filesystemMonitored] [decimal](1, 0) NULL,
	[remarks] [varchar](255) NULL,
 CONSTRAINT [PK_ubc_filesystem] PRIMARY KEY CLUSTERED 
(
	[hdId] ASC,
	[filesystemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[sm_filesystem] ADD  CONSTRAINT [DF_sm_filesystem_filesystemMonitored]  DEFAULT ((1)) FOR [filesystemMonitored]
GO



CREATE TABLE [dbo].[sm_hd](
	[hdId] [varchar](255) NOT NULL,
	[pid] [varchar](255) NULL,
	[startTime] [datetime] NULL,
	[directorStatus] [varchar](255) NULL,
	[remarks] [varchar](255) NULL,
	[hostName] [varchar](255) NULL,
	[hostStatus] [varchar](255) NULL,
	[hostIp] [varchar](255) NULL,
	[os] [varchar](255) NULL,
	[cpuCount] [decimal](5, 0) NULL,
	[appMonitoringPeriod] [decimal](5, 0) NULL,
	[hostMonitoringPeriod] [decimal](5, 0) NULL,
	[appMonitoringFlag] [decimal](1, 0) NULL,
	[hostMonitoringFlag] [decimal](1, 0) NULL,
	[cpuThreshold] [decimal](5, 2) NULL,
	[memoryThreshold] [decimal](5, 2) NULL,
	[filesystemThreshold] [decimal](5, 2) NULL,
	[cpuUsed] [decimal](5, 2) NULL,
	[realMemoryUsed] [decimal](10, 0) NULL,
	[realMemoryTotal] [decimal](10, 0) NULL,
	[virtualMemoryUsed] [decimal](10, 0) NULL,
	[virtualMemoryTotal] [decimal](10, 0) NULL,
	[swapInUsed] [decimal](10, 0) NULL,
	[lastUpdateTime] [datetime] NULL,
 CONSTRAINT [PK_ubc_hd] PRIMARY KEY CLUSTERED 
(
	[hdId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_ubc_hd] UNIQUE NONCLUSTERED 
(
	[hdId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[sm_hd] ADD  CONSTRAINT [DF_sm_hd_appMonitoringPeriod]  DEFAULT ((60)) FOR [appMonitoringPeriod]
GO

ALTER TABLE [dbo].[sm_hd] ADD  CONSTRAINT [DF_sm_hd_hostMonitoringPeriod]  DEFAULT ((60)) FOR [hostMonitoringPeriod]
GO

ALTER TABLE [dbo].[sm_hd] ADD  CONSTRAINT [DF_sm_hd_appMonitoringFlag]  DEFAULT ((1)) FOR [appMonitoringFlag]
GO

ALTER TABLE [dbo].[sm_hd] ADD  CONSTRAINT [DF_sm_hd_hostMonitoringFlag]  DEFAULT ((1)) FOR [hostMonitoringFlag]
GO

ALTER TABLE [dbo].[sm_hd] ADD  CONSTRAINT [DF_sm_hd_cpuThreshold]  DEFAULT ((0)) FOR [cpuThreshold]
GO

ALTER TABLE [dbo].[sm_hd] ADD  CONSTRAINT [DF_sm_hd_memoryThreshold]  DEFAULT ((0)) FOR [memoryThreshold]
GO

ALTER TABLE [dbo].[sm_hd] ADD  CONSTRAINT [DF_sm_hd_filesystemThreshold]  DEFAULT ((0)) FOR [filesystemThreshold]
GO


