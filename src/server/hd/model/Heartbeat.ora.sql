
--- Heartbeat TABLE ---

DROP TABLE SM_Heartbeat ;
CREATE TABLE SM_Heartbeat (
	HeartbeatMgtId			VARCHAR2(64) not null, 
	HeartbeatId			VARCHAR2(64) not null,
	period				NUMBER(5),
	termOfValidity			NUMBER(5),
	termOfThreshold			NUMBER(5),
	activeState			NUMBER(1),
	remarks				VARCHAR2(1024)
) ;
CREATE UNIQUE INDEX SM_Heartbeat_IDX ON SM_Heartbeat (HeartbeatMgtId,HeartbeatId) ;


