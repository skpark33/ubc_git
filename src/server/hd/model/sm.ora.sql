
--- HD TABLE ---

DROP TABLE SM_HD ;
CREATE TABLE SM_HD (
	hdId					VARCHAR2(64) not null,
	pid						VARCHAR2(64),
	startTime				DATE,
	lastUpdateTime			DATE,
	directorStatus			VARCHAR2(64),
	remarks					VARCHAR2(64),
	hostName				VARCHAR2(64),
	hostStatus				VARCHAR2(64),
	hostIp					VARCHAR2(64),
	os						VARCHAR2(128),
	cpuCount				NUMBER(5),
	appMonitoringPeriod		NUMBER(5),
	hostMonitoringPeriod	NUMBER(5),
	appMonitoringFlag		NUMBER(1),
	hostMonitoringFlag		NUMBER(1),
	cpuThreshold			NUMBER(5,2),
	memoryThreshold			NUMBER(5,2),
	filesystemThreshold		NUMBER(5,2),
	cpuUsed					NUMBER(5,2),
	realMemoryUsed			NUMBER(10),
	realMemoryTotal			NUMBER(10),
	virtualMemoryUsed		NUMBER(10),
	virtualMemoryTotal		NUMBER(10),
	swapInUsed	 NUMBER(10)
) ;
CREATE UNIQUE INDEX SM_HD_IDX ON SM_HD (hdId) ;

DROP TABLE SM_Application ;
CREATE TABLE SM_Application (
	hdId					VARCHAR2(64) not null,
	appId					VARCHAR2(64) not null,
	appKind					VARCHAR2(64),
	appDomain				VARCHAR2(64),
	preRunningEntity		VARCHAR2(64),
	binaryName				VARCHAR2(64),
	runningDir				VARCHAR2(64),
	runningType				VARCHAR2(64),
	arguments				VARCHAR2(1024),
	automaticStartup		NUMBER(1),
	isPrimitive				NUMBER(1),
	monitored				NUMBER(1),
	hasAgent				NUMBER(1),
	appStatus				VARCHAR2(64),
	adminState				NUMBER(1),
	pid						NUMBER(10),
	appIOR					VARCHAR2(1024),
	startTime				DATE,
	initSleepSecond			NUMBER(5),
	cpuUsed					NUMBER(5,2),
	realMemoryUsed			NUMBER(10),
	virtualMemoryUsed		NUMBER(10),
	realMemoryThreshold		NUMBER(10),
	virtualMemoryThreshold	NUMBER(10),
	thresholdRestart		NUMBER(1),
	remarks	 VARCHAR2(64)
) ;
CREATE UNIQUE INDEX SM_Application_IDX ON SM_Application (hdId,appId) ;

DROP TABLE SM_Filesystem ;
CREATE TABLE SM_Filesystem (
	hdId				VARCHAR2(64) not null,
	filesystemId		VARCHAR2(64) not null,
	filesystemName		VARCHAR2(64),
	filesystemBlocks	NUMBER(10),
	filesystemUsed		NUMBER(10),
	filesystemAvailable	NUMBER(10),
	filesystemCapacity	NUMBER(5,2),
	filesystemMountedOn	VARCHAR2(64),
	filesystemMonitored	NUMBER(1),
	remarks				VARCHAR2(64)
) ;
CREATE UNIQUE INDEX SM_Filesystem_IDX ON SM_Filesystem (hdId,filesystemId) ;

DROP TABLE SM_Application_Debug ;
CREATE TABLE SM_Application_Debug (
	hdId					VARCHAR2(64) not null,
	appId					VARCHAR2(64) not null,
	appKind					VARCHAR2(64) not null,
	debugpackage			VARCHAR2(64) not null,
	debuglevel				NUMBER(10),
	isdebuged				NUMBER(1)
) ;
CREATE UNIQUE INDEX SM_Application_Debug_IDX ON SM_Application_Debug (hdId,appId,appKind,debugpackage) ;

DROP TABLE SM_Dbms;
CREATE TABLE SM_Dbms (
	hdId							VARCHAR2(64) not null,
	dbmsId							VARCHAR2(64) not null,
	serviceName						VARCHAR2(64),
	remarks							VARCHAR2(64)
) ;
CREATE UNIQUE INDEX SM_Dbms_IDX ON SM_Dbms (dbmsId) ;

DROP TABLE SM_Network ;
CREATE TABLE SM_Network (
    hdId                VARCHAR2(64) not null,
    lanId        		VARCHAR2(64) not null,
    ipAddress      		VARCHAR2(64),
    inPacket    		NUMBER(10),
    outPacket      		NUMBER(10),
    inErrPacket 		NUMBER(10),
    outErrPacket  		NUMBER(10)
) ;
CREATE UNIQUE INDEX SM_Network_IDX ON SM_Network (hdId,lanId) ;

-- DATA DUMP ---
set lines 2000
set pages 1000
echo off
spool SM_Application.txt
SELECT
	hdId||'|'||appId||'|'||appKind||'|'||appDomain||'|'||preRunningEntity||'|'||
	binaryName||'|'||runningDir||'|'||runningType||'|'||arguments||'|'||automaticStartup||'|'||
	isPrimitive||'|'||monitored||'|'||hasAgent||'|'||appStatus||'|'||adminState||'|'||pid||'|'||
	appIOR||'|'||startTime||'|'||initSleepSecond||'|'||realMemoryUsed||'|'||virtualMemoryUsed||'|'||
	realMemoryThreshold||'|'||virtualMemoryThreshold||'|'||thresholdRestart||'|'||remarks||'|'
FROM
	Sm_Application;
spool off
-- DATA DUMP end --


--- SD TABLE ---


DROP TABLE SM_PROCESSHISTORY ;
CREATE TABLE SM_PROCESSHISTORY (
		processId		VARCHAR2(64) not null,
		hostName		VARCHAR2(64),
		processTime		DATE,
		status			VARCHAR2(10)
) ;
CREATE INDEX SM_PROCESSHISTORY_IDX1 ON SM_PROCESSHISTORY (processId, processTime);
CREATE UNIQUE INDEX SM_PROCESSHISTORY_IDX2 ON SM_PROCESSHISTORY (processId, processTime, status) ;

DROP TABLE SM_RESOURCEHISTORY ;
CREATE TABLE SM_RESOURCEHISTORY (
		processId		VARCHAR2(64) not null,
		hostName		VARCHAR2(64),
		processTime		DATE,
		status			VARCHAR2(10),
		cpu				NUMBER(5,2),
		realMemory		NUMBER(10),
		virtualMemory	NUMBER(10)
) ;
CREATE INDEX SM_RESOURCEHISTORY_IDX1 ON SM_RESOURCEHISTORY (processId,processTime) ;
CREATE UNIQUE INDEX SM_RESOURCEHISTORY_IDX2 ON SM_RESOURCEHISTORY (processId, processTime, status) ;


DROP TABLE SM_HOSTHISTORY ;
CREATE TABLE SM_HOSTHISTORY (
		hostName 						VARCHAR2(64) not null,
		processTime 					DATE,
		hostIp 							VARCHAR2(32),
		hostStatus 						VARCHAR2(10),
		cpuUsed						NUMBER(5,2),
		realMemoryUsed					NUMBER(10),
		realMemoryTotal					NUMBER(10),
		virtualMemoryUsed				NUMBER(10),
		virtualMemoryTotal				NUMBER(10)
) ;
CREATE INDEX SM_HOSTHISTORY_IDX1 ON SM_HOSTHISTORY (processTime) ;
CREATE INDEX SM_HOSTHISTORY_IDX2 ON SM_HOSTHISTORY (hostName,processTime) ;

DROP TABLE SM_TableManager ;
CREATE TABLE SM_TableManager (
		managerId 						VARCHAR2(64) not null,
		directory 						VARCHAR2(128),
		backupMethod 					VARCHAR2(10) default 'export',
		backupPeriod					NUMBER(5) default 1,
		backupPeriodHour					NUMBER(5) default 0,
		deleteDuration					NUMBER(5) default 7
) ;
CREATE INDEX SM_TableManager_IDX ON SM_TableManager (managerId) ;

DROP TABLE SM_ManagedTable ;
CREATE TABLE SM_ManagedTable (
		managerId 						VARCHAR2(64) not null,
		tableName 						VARCHAR2(64) not null,
		dbName 							VARCHAR2(32),
		autoBackup						NUMBER(1) default 0,
		class1							VARCHAR2(64),
		class2							VARCHAR2(64),
		class3							VARCHAR2(64)
) ;
CREATE INDEX SM_ManagedTable_IDX ON SM_ManagedTable (tableName) ;

DROP TABLE SM_ClientUtilize ;
CREATE TABLE SM_ClientUtilize (
		eventTime						DATE,
		clientType						VARCHAR2(10),  -- 'GUI' or 'WEB'
		class1							VARCHAR2(256),
		class2							VARCHAR2(256),
		class3							VARCHAR2(256),
		counter							NUMBER(10) default 0
) ;
CREATE INDEX SM_ClientUtilize_Idx ON SM_ClientUtilize (eventTime) ;

DROP TABLE SM_ClientUtilize_RAW ;
CREATE TABLE SM_ClientUtilize_RAW (
		eventTime						DATE,
		clientType						VARCHAR2(10),  -- 'GUI' or 'WEB'
		class1							VARCHAR2(256),
		class2							VARCHAR2(256),
		class3							VARCHAR2(256),
		counter							NUMBER(10)
) ;
CREATE INDEX SM_ClientUtilize_RAW_IDX ON SM_ClientUtilize_RAW (eventTime) ;

DROP TABLE SM_Dbms;
CREATE TABLE SM_Dbms (
		hdId							VARCHAR2(64) not null,
		dbmsId							VARCHAR2(64) not null,
		serviceName						VARCHAR2(64),
		remarks							VARCHAR2(64)
) ;
CREATE UNIQUE INDEX SM_Dbms_IDX ON SM_Dbms (dbmsId) ;

DROP TABLE SM_Dbms_BackupHistory;
CREATE TABLE SM_Dbms_BackupHistory (
		startTime						DATE not null,
		endTime							DATE not null,
		kind							VARCHAR2(64),
		result							VARCHAR2(128),
		tableList						VARCHAR2(1024)
) ;
CREATE UNIQUE INDEX SM_Dbms_BackupHistory_IDX ON SM_Dbms_BackupHistory (startTime) ;
