INSERT INTO SM_HD ( HDID, PID, appMonitoringPeriod, hostMonitoringPeriod, appMonitoringFlag, hostMonitoringFlag
, cpuThreshold, memoryThreshold, filesystemThreshold ) VALUES ( 
'cops', '0', '60', '60', '1', '1', '0', '0', '0'); 


INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, BINARYNAME, RUNNINGDIR
, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT, APPSTATUS, ADMINSTATE
, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD, VIRTUALMEMORYTHRESHOLD
, THRESHOLDRESTART ) VALUES ( 
'cops', 'osagent', 'osagent', '', 'osagent', '${LOGROOT}', '-a ${OSAGENT_HOST} -p ${OSAGENT_PORT}'
, 0, 1, 0, 0, 'Deactive', 0, 0, 0, 0, 0, 0, 0, 0); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, RUNNINGDIR, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT
, APPSTATUS, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD
, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART ) VALUES ( 
'cops', 'naming', 'naming', '', 'HD=cops/Application=osagent', 'nameserv'
, '${LOGROOT}\naming', '-VBJprop vbroker.se.iiop_tp.scm.iiop_tp.listener.port=${NAMESERVER_PORT} -VBJprop vbroker.se.default.local.listener.shm=false'
, 0, 1, 0, 0, 'Deactive', 0, 0, 1, 0, 0, 0, 0, 0); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT, APPSTATUS, ADMINSTATE
, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD, VIRTUALMEMORYTHRESHOLD
, THRESHOLDRESTART, RUNNINGTYPE ) VALUES ( 
'cops', 'fwActive', 'fwActive', '', 'HD=cops/Application=naming', 'naming'
, 0, 1, 0, 0, 'Deactive', 0, 0, 1, 0, 0, 0, 0, 0, 'fwActive'); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, RUNNINGDIR, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT
, APPSTATUS, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD
, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART ) VALUES ( 
'cops', 'NotiManager:MGR01', 'NotiManager', 'FW', 'HD=cops/Application=fwActive', 'notifymanager'
, '${LOGROOT}\NOTIFY\NOTI_MGR01', '+prop ${CONFIGROOT}\vbroker\fs.properties +name FW=FW/Manager=M01 +Mgr M01 +prefilter -Dvbroker.se.iiop_tp.scm.iiop_tp.listener.port=${NOTI_MGR1_PORT} +logPolicy NOTIFY_DEBUG_POLICY'
, 0, 1, 0, 1, 'Deactive', 0, 0, 1, 0, 0, 0, 0, 0); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT, APPSTATUS
, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD
, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART, RUNNINGTYPE ) VALUES ( 
'cops', 'rmirrepo', 'rmirrepo', '', 'HD=cops/Application=NotiManager:MGR01', 'del', '""${HOMEPATH}\${IR_FILE}*.*""'
, 0, 1, 0, 0, 'Deactive', 0, 0, 1, 0, 0, 0, 0, 0, 'fg'); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, RUNNINGDIR, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT
, APPSTATUS, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD
, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART ) VALUES ( 
'cops', 'irep', 'irep', '', 'HD=cops/Application=rmirrepo', 'irep', '${LOGROOT}\irserver'
, '${IR_FILE} -VBJprop vbroker.se.iiop_tp.host=${NAMESERVER_HOST}'
, 0, 1, 0, 0, 'Deactive', 0, 0, 1, 0, 0, 0, 0, 0); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT, APPSTATUS
, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD
, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART, RUNNINGTYPE ) VALUES ( 
'cops', 'ior2Naming', 'ior2Naming', '', 'HD=cops/Application=irep', 'ior2Naming'
, '-Dvbroker.orb.initRef=NameService=corbaloc::${NAMESERVER_HOST}:${NAMESERVER_PORT}/NameService +name IR=Repository +ir'
, 0, 1, 0, 0, 'Deactive', 0, 0, 1, 0, 0, 0, 0, 0, 'fg'); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, RUNNINGDIR, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT
, APPSTATUS, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD
, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART, RUNNINGTYPE ) VALUES ( 
'cops', 'putirall', 'putirall', '', 'HD=cops/Application=ior2Naming', 'putirall'
, '${LOGROOT}\putirall', '${COP_IDLROOT}/caf ${COP_IDLROOT}/cas', 0, 1, 0, 0, 'Deactive'
, 0, 0, 1, 0, 0, 0, 0, 0, 'fg'); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, RUNNINGDIR, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT
, APPSTATUS, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD
, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART, RUNNINGTYPE ) VALUES ( 
'cops', 'notiInfoGen', 'notiInfoGen', '', 'HD=cops/Application=putirall'
, 'notiInfoGen', '${LOGROOT}', '+id NOTIINFOGEN', 0, 1, 0, 0, 'Deactive', 0, 0
, 1, 0, 0, 0, 0, 0, 'fg'); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT, APPSTATUS
, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD
, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART, RUNNINGTYPE ) VALUES ( 
'cops', 'nbgen', 'nbgen', '', 'HD=cops/Application=notiInfoGen', 'nbgen'
, '+noidl +id NBGEN', 0, 1, 0, 0, 'Deactive', 0, 0, 1, 0, 0, 0, 0, 0, 'fg'); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, PRERUNNINGENTITY
, BINARYNAME, RUNNINGDIR, ARGUMENTS, AUTOMATICSTARTUP, ISPRIMITIVE, MONITORED, HASAGENT
, APPSTATUS, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD
, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART ) VALUES ( 
'cops', 'MIR:MIR', 'MIR', 'GW', 'HD=cops/Application=nbgen', 'xMirServer'
, '${LOGROOT}\xMirServer', '+recache +module ALL +mirId ManagedElement'
, 0, 1, 0, 1, 'Deactive', 0, 0, 1, 0, 0, 0, 0, 0); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, BINARYNAME, AUTOMATICSTARTUP
, ISPRIMITIVE, MONITORED, HASAGENT, APPSTATUS, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED
, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART
 ) VALUES ( 
'cops', 'AM:AM', 'AM', 'SM', 'amServer', 0, 0, 1, 1, 'Deactive', 0, 0, 0, 0, 0
, 0, 0, 0); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, BINARYNAME, AUTOMATICSTARTUP
, ISPRIMITIVE, MONITORED, HASAGENT, APPSTATUS, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED
, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART
 ) VALUES ( 
'cops', 'SD:SD', 'SD', 'SM', 'sdServer', 0, 0, 1, 1, 'Deactive', 0, 0, 0, 0, 0
, 0, 0, 0); 

INSERT INTO SM_APPLICATION ( HDID, APPID, APPKIND, APPDOMAIN, BINARYNAME, AUTOMATICSTARTUP
, ISPRIMITIVE, MONITORED, HASAGENT, APPSTATUS, ADMINSTATE, PID, INITSLEEPSECOND, REALMEMORYUSED
, VIRTUALMEMORYUSED, REALMEMORYTHRESHOLD, VIRTUALMEMORYTHRESHOLD, THRESHOLDRESTART
 ) VALUES ( 
'cops', 'HD:cops', 'HD', 'SM', 'hdServer', 0, 0, 1, 1, 'Deactive', 0, 0, 0, 0, 0
, 0, 0, 0); 

commit;
