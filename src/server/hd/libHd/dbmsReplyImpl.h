#ifndef _dbmsReplyImpl_h_
#define _dbmsReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciScopeInfo.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciException.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class dbmsReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	dbmsReplyImpl(const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciTrue,
            int memsize=100) ;
	virtual ~dbmsReplyImpl() {};

protected:
	virtual ciBoolean _initObject();
	virtual ciBoolean _execute();

protected:
	ciString			_dbmsId;

	ciBoolean _getDbVersion();
	ciBoolean _getTsInfo();
	ciBoolean _getTsDf();
	ciBoolean _getErrorInfo();
	ciBoolean _getTraceInfo();
	ciBoolean _getParaInfo();

	// for oracle only
	ciBoolean _export();
};

#endif // _dbmsReplyImpl_h_
