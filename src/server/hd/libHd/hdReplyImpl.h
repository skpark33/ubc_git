#ifndef _hdReplyImpl_h_
#define _hdReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciScopeInfo.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciException.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class hdReplyImpl : public virtual aciReply {
public:
	hdReplyImpl(const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciTrue,
            int memsize=100) ;
	virtual ~hdReplyImpl() {};

protected:
	ciString			_hdId;

	virtual ciBoolean _initObject();
	virtual ciBoolean _execute();
	virtual ciBoolean _set();
};


#endif // _hdReplyImpl_h_
