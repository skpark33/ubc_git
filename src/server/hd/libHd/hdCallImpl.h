/*! \class hdCallImpl
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief
 *  \author
 *  \version
 *  \date
 */

#ifndef _hdCallImpl_h_
#define _hdCallImpl_h_


#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciAttribute.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciScopeInfo.h>

#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libBase/ciBaseType.h>
#include <cci/libUtil/cciPOAUtil.h>

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include "aci/libCall/aciCall.h"

class hdCallImpl : virtual public  aciCall {
public:
	hdCallImpl(const char* pname) : aciCall(pname) { init(); } ;
	virtual ~hdCallImpl() {} ;
	virtual aciReply*	newCallReply(cciEntity& pEntity, const char* directive);

	void init();

protected:
};

#endif //_hdCallImpl_h_
