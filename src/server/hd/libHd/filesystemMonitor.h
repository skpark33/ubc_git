/*! \class filesystemMonitor
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _filesystemMonitor_h_
#define _filesystemMonitor_h_


#include <ci/libTimer/ciTimer.h>

class filesystemMonitor :  public  ciPollable {
public:
    virtual void processExpired(ciString name, int counter, int interval);
protected:
};

#endif //_filesystemMonitor_h_
