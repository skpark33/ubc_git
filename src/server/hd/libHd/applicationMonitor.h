/*! \class applicationMonitor
 *  Copyright �� 2002, SQI soft. All rights reserved.
 *
 *  \brief
 *  \author
 *  \version
 *  \date
 */

#ifndef _applicationMonitor_h_
#define _applicationMonitor_h_

#include <ci/libTimer/ciTimer.h>
#include <ci/libThread/ciMutex.h>

class applicationMonitor :  public  ciPollable
{
public:
	static applicationMonitor*	getInstance();
	static void clearInstance();

	virtual ~applicationMonitor();

	ciBoolean init(const char *phdId, int period=0);

protected:
	applicationMonitor();
	static ciMutex _mutex;
	static applicationMonitor* _instance;

	ciString _hdId;

public:
    virtual void processExpired(ciString name, int counter, int interval);
};

#endif //_applicationMonitor_h_

