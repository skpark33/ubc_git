#include "hdReplyImpl.h"
#include "hdCallImpl.h"
#include "ci/libBase/ciTime.h"
#include "cci/libWorld/cciORBWorld.h"
#include <cci/libWrapper/cciEvent.h>

#include "cci/libValue/cciBoolean.h"
#include "cci/libValue/cciTime.h"
#include "cci/libValue/cciString.h"
#include "cci/libValue/cciFloat.h"
#include "cci/libValue/cciInteger16.h"
#include "cci/libValue/cciInteger32.h"

#include "ci/libBase/ciBaseType.h"

#include "applicationMonitor.h"
#include "hostMonitor.h"

ciSET_DEBUG(10, "hdReplyImpl");

hdReplyImpl::hdReplyImpl(const char* className,
            const char* psType,
            const char* psName,
            ciBoolean   psOwner,
            int memsize) : aciReply(className,psType,psName,psOwner,memsize)
{
	ciDEBUG(7, ("filesystemReplyImpl()"));
	_hdId = "";
}

ciBoolean
hdReplyImpl::_initObject()
{
    ciDEBUG(7, ("_initObject()"));

	_hdId = _entity.getClassValue(0);

    _object->addItemAsKey("hdId",            new cciString(_hdId.c_str()));

    _object->addItem("pid",                  new cciString());
    _object->addItem("startTime",            new cciTime(ciFalse));
    _object->addItem("directorStatus",       new cciString()); 
    _object->addItem("remarks",              new cciString());
    _object->addItem("hostName",             new cciString());
    _object->addItem("hostStatus",           new cciString());
    _object->addItem("hostIp",               new cciString());
    _object->addItem("os",                   new cciString());
    _object->addItem("cpuCount",             new cciInteger16());
    _object->addItem("appMonitoringPeriod",  new cciInteger16());
    _object->addItem("hostMonitoringPeriod", new cciInteger16());
    _object->addItem("appMonitoringFlag",    new cciBoolean());
    _object->addItem("hostMonitoringFlag",   new cciBoolean());
    _object->addItem("cpuThreshold",         new cciFloat());
    _object->addItem("memoryThreshold",      new cciFloat());
    _object->addItem("filesystemThreshold",  new cciFloat());
    _object->addItem("cpuUsed",              new cciFloat());
    _object->addItem("realMemoryUsed",       new cciUnsigned32());
    _object->addItem("realMemoryTotal",      new cciUnsigned32());
    _object->addItem("virtualMemoryUsed",    new cciUnsigned32());
    _object->addItem("virtualMemoryTotal",   new cciUnsigned32());
    _object->addItem("swapInUsed",           new cciUnsigned32());
    _object->addItem("lastUpdateTime",       new cciTime(ciFalse));

    _fromEntity(0,"hdId");

	return ciTrue;
}

ciBoolean
hdReplyImpl::_execute()
{
    ciDEBUG(7, ("_execute()"));
	return ciFalse;
}

// set ������ (MonitorPeriod, Flag)
ciBoolean
hdReplyImpl::_set()
{
    ciDEBUG(5, ("_set()"));

	//
	ciString _hdId  = _entity.getClassValue(0);

	ciDEBUG(5, ("_set : hdId(%s)", _hdId.c_str()) );

	cciBoolean*   a_appMonitoringFlag    = new cciBoolean();
	cciInteger16* a_appMonitoringPeriod  = new cciInteger16();
	cciBoolean*   a_hostMonitoringFlag   = new cciBoolean();
	cciInteger16* a_hostMonitoringPeriod = new cciInteger16();

	cciObject aObj("SM_HD");
	aObj.addItemAsKey("hdId",  new cciString(_hdId.c_str()) );
	aObj.addItem("appMonitoringFlag",    a_appMonitoringFlag);
	aObj.addItem("appMonitoringPeriod",  a_appMonitoringPeriod);
	aObj.addItem("hostMonitoringFlag",   a_hostMonitoringFlag);
	aObj.addItem("hostMonitoringPeriod", a_hostMonitoringPeriod);

	if( !aObj.get() )
	{
		ciERROR( ("_set : fail to get !!!") );
		return aciReply::_set();
	}
	if( !aObj.next() )
	{
		ciERROR( ("_set : fail to next !!!") );
		return aciReply::_set();
	}

	ciBoolean appMonitoringFlag    = a_appMonitoringFlag->get();
	ciInt     appMonitoringPeriod  = a_appMonitoringPeriod->get();
	ciBoolean hostMonitoringFlag   = a_hostMonitoringFlag->get();
	ciInt     hostMonitoringPeriod = a_hostMonitoringPeriod->get();

	ciDEBUG(5, ("_set : hdId(%s), appMonitoringFlag(%d), appMonitoringPeriod(%d)"
				", hostMonitoringFlag(%d), hostMonitoringPeriod(%d)"
					, _hdId.c_str()
					, appMonitoringFlag
					, appMonitoringPeriod
					, hostMonitoringFlag
					, hostMonitoringPeriod) );

	//
	// appMonitoring
	//
	ciBoolean find_appmon_upate = ciFalse;

	ciBoolean app_flag;
	if( _reqList.getValueBool("appMonitoringFlag", app_flag) )
	{
		ciDEBUG(7, ("_set : appMonitoringFlag (%d)", app_flag));
		if( appMonitoringFlag != app_flag )
		{
			find_appmon_upate = ciTrue;
			appMonitoringFlag = app_flag;
			ciDEBUG(7, ("_set : new appMonitoringFlag (%d)", appMonitoringFlag));
		}
	}

	CORBA::Any appMonitoringPeriodAny;
	if( _reqList.getValueAny("appMonitoringPeriod", appMonitoringPeriodAny) )
	{
		CCI::CCI_Integer16 period;
		appMonitoringPeriodAny >>= period;
		ciInt tmp_period = period;

		ciDEBUG(7, ("_set : appMonitoringPeriod (%d)", tmp_period));

		if( appMonitoringPeriod != tmp_period )
		{
			find_appmon_upate = ciTrue;
			appMonitoringPeriod = tmp_period;
			ciDEBUG(7, ("_set : new appMonitoringPeriod (%d)", appMonitoringPeriod));
		}
	}

	//
	applicationMonitor *app_monitor = applicationMonitor::getInstance();

	if( find_appmon_upate )
	{
		ciDEBUG(7, ("_set : applicationMonitor->stopTimer()"));
		app_monitor->stopTimer();
		ciDEBUG(7, ("_set : applicationMonitor->setInterval(%d)", appMonitoringPeriod));
		app_monitor->setInterval(appMonitoringPeriod);

		if( appMonitoringFlag )
		{
			ciDEBUG(7, ("_set : applicationMonitor->startTimer()"));
			app_monitor->startTimer();
		}
	}

	//
	// hostMonitoring
	//
	ciBoolean find_hostmon_upate = ciFalse;

	ciBoolean host_flag;
	if( _reqList.getValueBool("hostMonitoringFlag", host_flag) )
	{
		ciDEBUG(7, ("_set : hostMonitoringFlag (%d)", host_flag));
		if( hostMonitoringFlag != host_flag )
		{
			find_hostmon_upate = ciTrue;
			hostMonitoringFlag = host_flag;
			ciDEBUG(7, ("_set : new hostMonitoringFlag (%d)", hostMonitoringFlag));
		}
	}

	CORBA::Any hostMonitoringPeriodAny;
	if( _reqList.getValueAny("hostMonitoringPeriod", hostMonitoringPeriodAny) )
	{
		CCI::CCI_Integer16 period;
		hostMonitoringPeriodAny >>= period;
		ciInt tmp_period = period;

		ciDEBUG(7, ("_set : hostMonitoringPeriod (%d)", tmp_period));

		if( hostMonitoringPeriod != tmp_period )
		{
			find_hostmon_upate = ciTrue;
			hostMonitoringPeriod = tmp_period;
			ciDEBUG(7, ("_set : new hostMonitoringPeriod (%d)", hostMonitoringPeriod));
		}
	}

	//
	hostMonitor *host_monitor = hostMonitor::getInstance();

	if( find_hostmon_upate )
	{
		ciDEBUG(7, ("_set : hostMonitor->stopTimer()"));
		host_monitor->stopTimer();
		ciDEBUG(7, ("_set : hostMonitor->setInterval(%d)", hostMonitoringPeriod));
		host_monitor->setInterval(hostMonitoringPeriod);

		if( hostMonitoringFlag )
		{
			ciDEBUG(7, ("_set : hostMonitor->startTimer()"));
			host_monitor->startTimer();
		}
	}


	return aciReply::_set();
}
