#ifndef _applicationReplyImpl_h_
#define _applicationReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>
#include <ci/libThread/ciMutex.h>

class applicationReplyImpl : public virtual aciReply {
public:
	typedef aciReply inherited;

	applicationReplyImpl(const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciTrue,
            int memsize=100) ;
	virtual ~applicationReplyImpl() ;

	static ciBoolean call(cciEntity& entity, const char *directive, cciReply* reply = 0);

protected:
	static ciMutex _mutex;

	ciBoolean _execute();
	ciBoolean _initObject();

//	ciBoolean _create();
//	ciBoolean _remove();
	ciBoolean _start();
	ciBoolean _stop();
	ciBoolean _watch();
	ciBoolean _getDebug();
	ciBoolean _setDebug();
	ciBoolean _saveDebug();

typedef map<ciString, ciString>				CMD_MAP;
typedef map<ciString, ciString>::iterator	CMD_MAP_ITR;
typedef map<ciString, ciString>::value_type	CMD_MAP_VALUE;

	static ciBoolean	getRunCommandMap(const char* cmd, CMD_MAP& cmdMap);

public:
	static ciString		getRunCommand(const char* binaryName, const char* id, const char* kind, const char* domain, const char* arguments);
	static ciBoolean	isEqualRunCommand(const char* cmd1, const char* cmd2);

	static ciBoolean	start(cciEntity &entity, ciBoolean postEvent=ciTrue); // start & update status only
	static ciBoolean	watch(cciEntity &entity, ciUInt& newPid, ciString& newAppStatus, 
								ciFloat& cpu, ciUInt& rmem, ciUInt& vmem); // watch & update status only
	static ciBoolean	stop(cciEntity &entity, ciBoolean postEvent=ciTrue); // stop & update status only

	static ciBoolean	fg(const char* command, const char* appId);
	static ciUInt		bg(const char *command, const char *binaryName, const char* appId);
	//static void			killProcess(int pid, const char* binaryName, ciBoolean immediate=ciTrue);
	static ciBoolean	killProcess(const char *binaryName, ciUInt pid);


#if defined (_COP_MSC_)
	static ciBoolean	watch(ciString& pid, ciFloat& cpu, ciUInt& rmem, ciUInt& vmem); // get cpu,rmem,vmem for MSC
#endif
};

#endif // _applicationReplyImpl_h_

