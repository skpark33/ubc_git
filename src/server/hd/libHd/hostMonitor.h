/*! \class hostMonitor
 *  Copyright ⓒ 2002, COP. All rights reserved.
 *
 *  \brief
 *  \author
 *  \version
 *  \date
 */

#ifndef _hostMonitor_h_
#define _hostMonitor_h_

#include "ci/libTimer/ciTimer.h"
//#include "sm/libHostInfo/hostinfo.h"
#include <list>


/////////////////////////////////////////////
typedef struct
{
	ciString  id;
	ciString  name;
	ciUInt    blocks;
	ciUInt    used;
	ciUInt    avail;
	ciFloat   capacity;
	ciString  mount;
	ciBoolean monitored;
	ciString  remarks;

	ciBoolean is_new;
} fileSystemData;

typedef list<fileSystemData>	fileSystemList;	// 파일시스템 저장 데이터
typedef fileSystemList::iterator	fileSystemListItr;	// 파일시스템 저장 데이터


/////////////////////////////////////////////
typedef struct
{
	//char    host_name[255];
	//int     hostStatus;     // 0:"DirectorFail", 1:"Normal", 2:"Warning"
	//int     cpuThrChk;
	//int     memThrChk;
	float   cpu_user;
	float   cpu_nice;
	float   cpu_sys;
	float   cpu_idle;
	long    mem_real_total;
	long    mem_real_used;
	long    mem_virtual_total;
	long    mem_virtual_used;
	long    mem_swap_total;
	long    mem_swap_used;
	//long    t_numApp;
	//long    runApp;
	//long    waitApp;
	//long    sleepApp;
	//long    zombieApp;
	//long    idleApp;
} hostInfo;


/////////////////////////////////////////////
class hostMonitor : public ciPollable {
public:
	static hostMonitor*	getInstance();
	static void clearInstance();

	virtual ~hostMonitor() ;

protected:
	hostMonitor();
	static ciMutex _mutex;
	static hostMonitor* _instance;

	ciString	_hdId;

public:
	ciBoolean	init(const char *phdId, int period=0);
	virtual void processExpired(ciString name, int counter, int interval);

protected:
	// 파일시스템 모니터
	ciBoolean	_getExistFilesystem(fileSystemList& dataList);
	ciString	_getNewFilesystemID(fileSystemList& dataList);
	void		_updateFilesystem(fileSystemList& dataList);
	void		_filesystemMonitor(ciFloat filesystemThreshold, ciBoolean& filesystemWarning);

	// CPU,메모리 모니터
	ciBoolean	_getHostInfo(hostInfo* info);
	void		_systemMonitor(ciFloat cpuThreshold, ciFloat memoryThreshold,
								ciString& hostStatus, ciString& remarks );

	// network monitor
	//void _networkMonitor();
	//nettraffic_info* _lastNetTrafficInfo;
};

#endif //_hostMonitor_h_

