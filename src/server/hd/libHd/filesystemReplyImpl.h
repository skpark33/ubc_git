#ifndef _filesystemReplyImpl_h_
#define _filesystemReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciValueInclude.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class filesystemReplyImpl : public virtual aciReply {
public:
	filesystemReplyImpl(const char* className,
            const char* psType=PS_AUTO,
            const char* psName="",
            ciBoolean   psOwner = ciTrue,
            int memsize=100) ;
	virtual ~filesystemReplyImpl() ;

protected:
	ciBoolean _execute();
	ciBoolean _initObject();

};


#endif // _filesystemReplyImpl_h_
