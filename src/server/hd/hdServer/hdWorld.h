 /*! \file hdWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief
 *
 *  \author
 *  \version
 *  \date
 */

#ifndef _hdWorld_h_
#define _hdWorld_h_

#include <aci/libCall/aciCallWorld.h>
#include <cci/libWrapper/cciEventControl.h>
#include <cci/libXLog/cciXLogManager.h>
#include <cci/libXLog/cciXLog.h>

#include "../libHd/applicationMonitor.h"
#include "../libHd/hostMonitor.h"
#include "aci/libWrapper/aciManagerImpl.h"
//#include "sm/hdServer/hdProxy.h"

class hdWorld : public aciCallWorld
{
public:
	hdWorld();
	~hdWorld();

	//[
	//	From aciCallWorld
	//
	virtual aciCall*    newCallImpl(const char* pname);
	ciBoolean init(int& p_argc, char** &p_argv);
	//
	//]
	virtual ciBoolean	fini(long p_finiCode = 0);

	//
	static ciString		__hdId;
	static ciBoolean	__isAliveNamingService;
	static void			__initArguments(int& p_argc, char** &p_argv, ciString& aId);
	static ciBoolean	__checkNamingService();
	static ciBoolean	__runPrimitive(ciBoolean runPrimitive=ciTrue);

protected:
	//
	static int		__argc;
	static char**	__argv;

	//
	applicationMonitor*	_appMonitor;
	hostMonitor*		_hostMonitor;

	ciBoolean	_startMonitor();
	ciBoolean	_stopMonitor();

	ciString	_getOSVersion();
	int			_getCPUCount();

};

#endif //_hdWorld_h_
