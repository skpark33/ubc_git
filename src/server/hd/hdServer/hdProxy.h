 /*! \file hdProxy.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief
 *
 *  \author
 *  \version
 *  \date
 */

#ifndef _hdProxy_h_
#define _hdProxy_h_

#include <cci/libWrapper/cciEventControl.h>

#include "sm/libHd/applicationMonitor.h"
#include "sm/libHd/hostMonitor.h"
#include "aci/libWrapper/aciManagerImpl.h"

class hdProxy
{
public:
	static hdProxy *getInstance();
	static void clearInstance();

	~hdProxy();

	ciBoolean	init(const char* hdId, ciBoolean aAloneStartup);
	ciBoolean	run();

protected:
	hdProxy();

	cciEntity	_entity;

	static ciMutex	_mutex;
	static hdProxy*		_instance;
	aciManagerImpl*		_aciManager;

	applicationMonitor*	_appMonitor;
	hostMonitor*		_hostMonitor;

	ciBoolean	_nameBindingManager(const char* pId);
	ciBoolean	_runPrimitive();
	ciBoolean	_startMonitor();
};

#endif //_hdProxy_h_

