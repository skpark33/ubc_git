#include "stdafx.h"

#include "UBCAgentNotiSvc.h"

#include <windows.h>
#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>
#include <conio.h>

#include <tlhelp32.h>
//#include <iostream>
//#include <string>
#include "psapi.h"
#pragma comment(lib, "Psapi.lib")

#include <Userenv.h>
#pragma comment(lib, "Userenv.lib")

//#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")

#include "ServiceUtil.h"
#include "ServiceMgr.h"

#include "HttpRequest.h"

BOOL RunCmd(LPCTSTR szCmd);
DWORD WINAPI service_handler(DWORD fdwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
{
	CServiceManager manager;

	switch (fdwControl) 
	{ 
	case SERVICE_CONTROL_PAUSE:
		manager.SetServiceRunStatus(SERVICE_PAUSE_PENDING);
		// 서비스를 일시 중지 시킨다.
		manager.SetServiceRunStatus(SERVICE_PAUSED);
		break;

	case SERVICE_CONTROL_CONTINUE:
		manager.SetServiceRunStatus(SERVICE_CONTINUE_PENDING);
		// 일시 중지 시킨 서비스를 재개한다.
		manager.SetServiceRunStatus(SERVICE_RUNNING);
		break;

	case SERVICE_CONTROL_STOP:
		manager.SetServiceRunStatus(SERVICE_STOP_PENDING);
		// 서비스를 멈춘다 (즉, 종료와 같은 의미)
		// 서비스를 종료하면, service_main 는 절대로 리턴하지 않는다.
		// 그러므로 해제하려면 작업이 있으면 모든것을 이곳에서 처리한다.
		manager.SetServiceRunStatus(SERVICE_STOPPED);
		break;

	default:
		break;
	}

	return NO_ERROR;
}

CString g_strExePath = "";
CString g_strINIPath = "";

CString g_strCenterIP = _T("");
UINT g_center_port = 0;

CString g_strNotiIP = _T("");
UINT g_noti_port = 0;

UBCPROCESS_INFOMAP	g_map_agent;
CCriticalSection	g_lock_agent;

int service_main(INT ARGC, LPSTR* ARGV)
{
	TCHAR str[MAX_PATH];
	::ZeroMemory(str, MAX_PATH);
	::GetModuleFileName(NULL, str, MAX_PATH);
	int length = _tcslen(str) - 1;
	while( (length > 0) && (str[length] != _T('\\')) )
		str[length--] = 0;
	g_strExePath = str;
	g_strINIPath = g_strExePath + "..\\config\\data\\UBCAgentNotiSvc.ini";

	GetPrivateProfileString(_T("UBCAgentNotiSvc"), _T("IP"),	g_strCenterIP,	g_strINIPath);
	CString str_port;
	GetPrivateProfileString(_T("UBCAgentNotiSvc"), _T("PORT"),	str_port,		g_strINIPath);
	g_noti_port = _ttoi(str_port);

	GetPrivateProfileString(_T("UBCAgentNotiSvc"), _T("NOTI_IP"),	g_strNotiIP,	g_strINIPath);
	GetPrivateProfileString(_T("UBCAgentNotiSvc"), _T("NOTI_PORT"),	str_port,		g_strINIPath);
	g_noti_port = _ttoi(str_port);
	if(g_noti_port == 0) g_noti_port=8088;

	//
	CServiceManager manager;
	CServiceUtility utility;
	if(utility.IsServiceMode() == TRUE)
	{
		manager.SetServiceHandler(RegisterServiceCtrlHandlerEx(CServiceManager::m_InstallParam.lpServiceName, service_handler, NULL));
		if (manager.GetServiceHandler() == NULL) 
			return 0;
	}
	else
	{
		printf("run debug mode\npress any key close...\n");
	}

	manager.SetServiceRunStatus(SERVICE_START_PENDING);

	// bla bla initialize...
	Sleep(1000);

	// start ok, i'm ready receive event
	manager.SetServiceRunStatus(SERVICE_RUNNING);

	/////////////////////////////////////////////////////////////////////
	// test code
	/////////////////////////////////////////////////////////////////////
	//char szCmd[4096] = {0};
	//sprintf(szCmd, "cmd /c %s", "notepad");
	//if(!RunCmd(szCmd))
	//{
	//	::WinExec(szCmd, SW_SHOW);
	//}

	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	int count = 0;
	while(manager.GetServiceRunStatus() != SERVICE_STOPPED)
	{
		if(manager.GetServiceRunStatus() == SERVICE_PAUSED)
		{
			printf("SERVICE_PAUSED\n");
			Sleep(1000);
			continue;
		}

		if(utility.IsServiceMode() == FALSE && _kbhit())
		{
			printf("stop debug mode\n");
			break;
		}

		printf("while(%d)", count++);
		if(count < 60)
		{
			Sleep(1000);
			continue;
		}
		count = 0;

		//::MessageBeep(0xFFFFFFFF);
		//printf("ting...\n");
		//Sleep(1000);

		CWinThread* pThread = ::AfxBeginThread(CheckStatusThread, NULL, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		if(pThread)
		{
			pThread->m_bAutoDelete = TRUE;
			pThread->ResumeThread();
		}
	}

	CoUninitialize();

	return 0;
}

int main(int argc, char** argv)
{
#if 0
	/////////////////////////////////////////////////////////////////////
	// test code
	/////////////////////////////////////////////////////////////////////

	CServiceUtility u;
	u.ServiceUserBlankPassword(FALSE);

	u.UserPrivileges("Administrtor", L"SeServiceLogonRight");
	u.ProcessPrivileges(GetCurrentProcess(), SE_SHUTDOWN_NAME, TRUE);

	PWTS_PROCESS_INFO sinfo = NULL;
	DWORD count = 0;
	u.WTSEnumProcesses(sinfo, &count);
	u.WTSFree(sinfo);

	PWTS_SESSION_INFO info = NULL;
	count = 0;
	u.WTSEnumSessions(info, &count);
	u.WTSFree(info);

	CServiceUtility::CWTSSession ws(WTS_CURRENT_SERVER_HANDLE, 0);

	char buffer[512] = {0};
	if(u.GetOSDisplayString(buffer))
		printf("%s\n", buffer);

	CServiceUtility su;
	STARTUPINFO si = {0};
	PROCESS_INFORMATION pi = {0};
	u.CreateProcessToDesktop("C:\\Windows\\System32\\cmd.exe", NULL, si, pi, 0);
	WaitForSingleObject(pi.hProcess, INFINITE);

#endif

	CServiceManager manager;
	CServiceUtility utility;

	/* 설정하지 않으면, 기본 파일값으로 
	파일 이름을 잘라다가 자동으로 설정한다. */
	manager.ConfigServiceName("UBCAgentNotiSvc");
	manager.ConfigServiceDisp("UBC Agent Notification Service");
	manager.ConfigServiceDesc("UBC Agent Notification Service");

	if(utility.IsServiceMode() == FALSE)
	{
		char ch = 0;
		if(argc != 2)
		{
			printf("UBCAgentNotiSvc.exe [option: a, i, u, s, t, p, c]\n");
			printf("  a - install and start\n");
			printf("  i - install\n");
			printf("  u - uninstall\n");
			printf("  s - start\n");
			printf("  t - stop\n");
			printf("  p - pause\n");
			printf("  c - continue\n\n");

			printf("input command: ");
			ch = getch();
		}
		else
			ch = argv[1][0];

		switch(ch)
		{
		case 'a':
			manager.Install();
			manager.Start();
			return 0;
		case 'i':
			manager.Install();
			return 0;
		case 'u':
			manager.Uninstall();
			return 0;
		case 's':
			manager.Start();
			return 0;
		case 't':
			manager.Stop();
			return 0;
		case 'p':
			manager.Pause();
			return 0;
		case 'c':
			manager.Continue();
			return 0;

		default:
			return service_main(argc, argv);
		}
	}

	SERVICE_TABLE_ENTRY STE[] =
	{
		{(char*)manager.m_InstallParam.lpServiceName, (LPSERVICE_MAIN_FUNCTION)service_main},
		{NULL,NULL}
	};

	if(StartServiceCtrlDispatcher(STE) == FALSE)
		return -1;

	return 0;
}

void GetLineList(CString& str_lines, CStringArray& line_list)
{
	// divide by line
	int pos = 0;
	CString line = str_lines.Tokenize(_T("\r\n"), pos);
	while(line != _T(""))
	{
		line_list.Add(line);
		line = str_lines.Tokenize(_T("\r\n"), pos);
	}
}

BOOL GetAppList(CStringArray& line_list, PUBCPROCESS_INFOLIST& list_app)
{
	BOOL ret_val = TRUE;
	UBCPROCESS_INFO* info = NULL;

	//
	for(int i=1; i<line_list.GetCount(); i++)
	{
		const CString& line = line_list.GetAt(i);
		if(line.Find(_T('=')) <= 0) return FALSE;

		int pos = 0;
		CString key = line.Tokenize(_T("="), pos);
		CString value = line.Tokenize(_T("="),pos);

		if(key == _T("customer"))
		{
			info = new UBCPROCESS_INFO;
			info->customer = value;

			list_app.Add(info);
		}
		else if(key == _T("serverId") && info)
		{
			info->serverId = value;
		}
		else if(key == _T("serverIp") && info)
		{
			info->serverIp = value;
		}
		else if(key == _T("lastUpdateTime") && info)
		{
			info->lastUpdateTime = value;
		}
		else if(key == _T("appId") && info)
		{
			info->appId = value;
		}
		else if(key == _T("binaryName"))
		{
			info->binaryName = value;
		}
		else if(key == _T("argument"))
		{
			info->argument = value;
		}
		else if(key == _T("properties"))
		{
			info->properties = value;
		}
		else if(key == _T("debug"))
		{
			info->debug = value;
		}
		else if(key == _T("status"))
		{
			info->status = value;
		}
		else if(key == _T("startUpTime"))
		{
			info->startUpTime = value;
		}
		else if(key == _T("command"))
		{
			info->command = value;
		}
		else if(key == _T("commandTime"))
		{
			info->commandTime = value;
		}
		else if(key == _T("executeTime"))
		{
			info->executeTime = value;
		}
	}

	return TRUE;
}

UINT CheckStatusThread(LPVOID pParam)
{
	CString str_email_list;
	GetPrivateProfileString(_T("UBCAgentNotiSvc"), _T("EMAIL"),	str_email_list,	g_strINIPath);
	if(str_email_list.GetLength() == 0) return FALSE;

	// make url
	CString url = _T("UBC_Server/get_agent_list.asp");

	// make send-message
	CString send_msg = _T("");

	//
	CString out_msg;
	CHttpRequest http_request(g_strCenterIP, g_center_port);
	if( !http_request.RequestPost(url, send_msg, out_msg) )
	{
		return FALSE;
	}

	// get line-list ( divide by line )
	CStringArray line_list;
	GetLineList(out_msg, line_list);

	//
	if(line_list.GetCount() > 0 && line_list.GetAt(0) == _T("Fail"))
	{
		return FALSE;
	}
	else if(line_list.GetCount() == 0 || line_list.GetAt(0) != _T("OK"))
	{
		return FALSE;
	}

	//
	PUBCPROCESS_INFOLIST list_app;
	if( GetAppList(line_list, list_app) == FALSE )
	{
		return FALSE;
	}

	//
	PUBCPROCESS_INFOLIST noti_list;
	CString str_noti_server_id_list = _T("");
	{
		CLockObject _lock(g_lock_agent);
		for(int i=0; i<list_app.GetCount(); i++)
		{
			UBCPROCESS_INFO* new_info = list_app.GetAt(i);
			if(new_info==NULL) continue;

			CString key;
			key.Format(_T("%s-%s-%s"), new_info->customer, new_info->serverId, new_info->serverIp);

			UBCPROCESS_INFO* old_info = NULL;
			if(g_map_agent.Lookup(key, (void*&)old_info))
			{
				if( old_info->lastUpdateTime < new_info->lastUpdateTime &&
					old_info->status != new_info->status &&
					new_info->status != UBCSERVER_STATUS_NORMAL &&
					old_info->properties != _T("NoNoti") )
				{
					noti_list.Add(old_info);
				}
				*old_info = *new_info;
				delete new_info;
			}
			else
			{
				g_map_agent.SetAt(key, new_info);
			}
		}

		//
		if(noti_list.GetCount() > 0)
		{
			for(int i=0; i<noti_list.GetCount(); i++)
			{
				UBCPROCESS_INFO* info = noti_list.GetAt(i);
				if(info==NULL) continue;

				CString str_id;
				str_id.Format(_T("%s : %s : %s : %s"), info->customer, info->serverId, info->serverIp, info->status);

				if(str_noti_server_id_list.GetLength() > 0)
					str_noti_server_id_list += _T("|");
				str_noti_server_id_list += str_id;
			}
		}
	}

	if(str_noti_server_id_list.GetLength() > 0)
	{
		url = _T("agentmail.aspx");
		send_msg.Format(_T("email=%s&server_list=%s"), 
			CHttpRequest::ToEncodingString(str_email_list), 
			CHttpRequest::ToEncodingString(str_noti_server_id_list) );
		out_msg = _T("");

		CHttpRequest http_request(g_strNotiIP, g_noti_port);
		if( !http_request.RequestPost(url, send_msg, out_msg) )
		{
			return FALSE;
		}
	}

	return TRUE;
}
