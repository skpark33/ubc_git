#ifndef _EGWORLD_H_
#define _EGWORLD_H_

#include <ci/libBase/ciListType.h>
#ifdef _COP_USE_PROPERTIES_
#     include <ci/libConfig/ciProperties.h> 
#else
#     include <ci/libConfig/ciConfig.h> 
#endif
#include <cci/libWrapper/cciEventManager.h>
#include <cci/libWorld/cciORBWorld.h>


// egWorld class
class egWorld : public cciORBWorld {
public:

	egWorld();
	~egWorld();	

	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode = 0);

protected:
	static cciEventManager* _manager;

};

class egEventHandler : public cciEventHandler {
public:
    virtual void processEvent(cciEvent& event);
};

#endif //_EGWORLD_H_
