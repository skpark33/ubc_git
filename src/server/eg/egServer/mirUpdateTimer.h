
#ifndef _mirUpdateTimer_h_
#define _mirUpdateTimer_h_


#include <ci/libTimer/ciTimer.h>
#include <ci/libThread/ciMutex.h>


class mirUpdateTimer :  public  ciPollable {
public:
	static mirUpdateTimer*	getInstance();
	static void	clearInstance();

	mirUpdateTimer() { };
	virtual ~mirUpdateTimer() { }

    virtual void processExpired(ciString name, int counter, int interval);

protected:
	static mirUpdateTimer*	_instance;
	static ciMutex			_instanceLock;

};	

#endif //_mirUpdateTimer_h_
