
#ifndef _eventCleanTimer_h_
#define _eventCleanTimer_h_


#include <ci/libTimer/ciTimer.h>
#include <ci/libThread/ciMutex.h>


class eventCleanTimer :  public  ciPollable {
public:
	static eventCleanTimer*	getInstance();
	static void	clearInstance();

	eventCleanTimer() { };
	virtual ~eventCleanTimer() { }

    virtual void processExpired(ciString name, int counter, int interval);

protected:
	static eventCleanTimer*	_instance;
	static ciMutex			_instanceLock;

};	

#endif //_eventCleanTimer_h_
