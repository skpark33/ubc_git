#ifndef _pmReplyImpl_h_
#define _pmReplyImpl_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciScopeInfo.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciException.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class lmReplyImpl : public virtual aciReply {
public:
	lmReplyImpl();
	virtual ~lmReplyImpl() {};

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

    virtual ciBoolean _get();

protected:
	ciString			_mgrId;
};


#endif // _pmReplyImpl_h_
