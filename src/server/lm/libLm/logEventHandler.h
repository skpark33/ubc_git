#ifndef _logEventHandler_h_
#define _logEventHandler_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciEntity.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>


class logEventManager  {
public:
	static logEventManager*	getInstance();
	static void	clearInstance();

	logEventManager() { }
	virtual ~logEventManager() {}
	ciBoolean		addHand();

protected:
	static logEventManager*	_instance;
	static ciMutex			_instanceLock;
};



class logEventHandler : public cciEventHandler {
public:
	logEventHandler(const char* serverId) { _serverId = serverId; }
	virtual ~logEventHandler() {}
   	virtual void 		processEvent(cciEvent& pEvent);


protected:
	ciString	_serverId;
   	void 		_scheduleStateLog(cciEvent& pEvent);
   	void 		_connectionStateLog(cciEvent& pEvent);
   	void 		_powerStateLog(cciEvent& pEvent);

	ciBoolean	_postEvent(const char* eventType, const char* siteId, const char* hostId, ciTime& eventTime,
						  ciTime* bootUpTime=0,
						  ciTime* bootDownTime=0,
						  ciBoolean operationalState=ciFalse,
						  const char* currentSchedule1=0,
						  const char* currentSchedule2=0,
						  const char* autoSchedule1=0,
						  const char* autoSchedule2=0,
						  const char* lastSchedule1=0,
						  const char* lastSchedule2=0);

};


#endif // _logEventHandler_h_
