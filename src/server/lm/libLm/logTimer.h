/*! \class logTimer
 *  Copyright ⓒ 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _announceTimer_h_
#define _announceTimer_h_


#include <ci/libTimer/ciTimer.h>

class LMO {
public:
	LMO() { duration=7;adminState=ciTrue;logType=0;}
	//ciString lmId;
	ciString		description;    // 설명
	ciString		tableName;      // dbTable 명 or file 접두어
	ciString		timeFieldName;      // dbTable 명 or file 접두어
	ciBoolean		adminState;		// on/off
	ciShort			logType;      	// 0 : DB table, 1: File
	ciLong			duration;      	// 로그보관주기
};
typedef map<ciString, LMO*>		LMOMap;

#define LOGTYPE_DB		0
#define LOGTYPE_FILE	1


class logTimer :  public  ciPollable {
public:
	static logTimer*	getInstance();
	static void	clearInstance();

	logTimer() { _isMainServer=ciFalse; };
	virtual ~logTimer() { }

    virtual void processExpired(ciString name, int counter, int interval);

	ciBoolean	load(const char* id="*");
	void	remove(const char* id);
	void	clear();

	ciBoolean	getState(const char* id) ;
	ciBoolean	getPowerState()		{ return getState("PowerStateLog");}
	ciBoolean	getNetworkState()	{ return getState("ConnectionStateLog");}
	ciBoolean	getScheduleState()	{ return getState("ScheduleStateLog");}

	ciBoolean	isMainServer() { return _isMainServer;}
	void		isMainServer(ciBoolean p) { _isMainServer = p; }

protected:

	ciBoolean	_deleteOldLog(const char* tableName, const char* timeFieldName, ciShort duration);
	ciBoolean	_deleteOldScreenShotFile(ciShort duration);
	ciBoolean	_deleteOldScreenShotFile(const char* subDir, ciShort duration);

	static logTimer*	_instance;
	static ciMutex			_instanceLock;

	LMOMap	_map;
	ciMutex _mapLock;
	ciBoolean	_isMainServer;
	/*
	ciLong		_duration;
	ciBoolean	_powerState;
	ciBoolean	_connectionState;
	ciBoolean	_scheduleState;
	*/
};	

#endif //_announceTimer_h_
