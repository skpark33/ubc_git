 /*! \file lmWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _lmWorld_h_
#define _lmWorld_h_

#include <aci/libCall/aciCallWorld.h> 

class lmWorld : public aciCallWorld {
public:
	lmWorld();
	~lmWorld();
 	
	//[
	//	From aciCallWorld
	//
	virtual aciCall*    newCallImpl(const char* pname);
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);

	//
	//]

protected:	


	
};
		
#endif //_lmWorld_h_
