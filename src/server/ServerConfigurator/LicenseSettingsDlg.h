#pragma once

#include "SubDlg.h"
#include "libServerAuth/ServerAuth.h"
#include "afxwin.h"


// CLicenseSettingsDlg 대화 상자입니다.

class CLicenseSettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(CLicenseSettingsDlg)

public:
	CLicenseSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLicenseSettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LICENSE_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CServerAuth		m_serverAuth;

	CString	m_strEnterpriseKey;
	CString	m_strPassword;

	bool	m_bIsAuthenticated;

	bool	IsSameInIni(LPCSTR section, LPCSTR name, LPCSTR value);
	bool	IsSameInProperties(LPCSTR name, LPCSTR value);

	bool	IsEnterpriseKeyAuthorized();

	int		m_nLButtonPush;
	bool	m_bOnceTimeForceAuth;

public:
	CEdit			m_editIPAddr;
	CEdit			m_editMACAddr;
	CEdit			m_editEnterKey;
	CEdit			m_editPwd;
	CButton			m_btnBypassAuth;
	CButton_String	m_btnAuth;
	CStatic			m_stcInfo;
	CStatic			m_stcSettings;

	afx_msg void OnBnClickedButtonAuth();
	afx_msg void OnEnChangeEditKeyPassword();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonAuthBypass();

	virtual void	UpdateControls();
	virtual bool	RunInit();
	virtual int		CheckInstallComplete();

};
