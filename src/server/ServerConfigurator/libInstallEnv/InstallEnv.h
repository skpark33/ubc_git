/************************************************************************************/
/*! @file InstallEnv.h
	@brief 윈도우즈 환경변수를 추가하는 클래스 선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2012/07/23\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2012/07/23:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#pragma once
using namespace std;

#include "../Installable/installable.h"

#ifndef _ERR_CODDE
	#define _ERR_CODDE
	#define ERR_TEST_FAIL			-1			//테스트 과정 실패
	#define ERR_UNINSTALLED			-2			//테스트 결과 설치가 되어있지 않음
	#define ERR_FAIL_INSTALL		-3			//설치 실패
	#define ERR_FAIL_UNINSTALL		-4			//uninstall fail
	#define ERR_ETC					-10			//기타 실패
#endif


//! 윈도우즈 환경변수를 추가하는 클래스
/*!
*/
class CInstallEnv : public installable
{
public:
	CInstallEnv(void);								///<기본 생성자
	CInstallEnv(CMapStringToString& mapEnv);		///<생성자
	virtual ~CInstallEnv(void);						///<소멸자

	virtual int prerequisite()	{ return TRUE; };
	virtual int test();								///<환경변수의 리스트가 정상적으로 추가되었는지 확인한다.
	virtual int	install();							///<환경변수들을 등록한다.

	virtual int	uninstall(CMapStringToString& mapEnv);	///<환경변수들을 삭제한다.

	int			get(CMapStringToString& mapEnv);	///<환경변수들 리스트를 반환한다.
	int			set(CMapStringToString& mapEnv);	///<환경변수들 리스트를 설정한다.

	int			get(LPCTSTR lpszKey, CString& strValue);	///<환경변수를 반환한다.

private:
	bool		ReadEnvList(void);					///<ServerConfigurator.ini로 부터 환경변수 리스트를 읽는다.
	void		SaveEnvList(void);					///<ServerConfigurator.ini에 환경변수 리스트를 쓴다.

	HWND		GetEnvVarWindow();					// 윈도우 환경변수창의 핸들 얻기

	CMapStringToString	m_mapEnv;					///<환경변수들 리스트
	CString				m_strConfigPath;			///<ServerConfigurator.ini의 경로
};

