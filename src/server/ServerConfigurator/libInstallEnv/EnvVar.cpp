////////////////////////////////////////////////////////////////////////////////
//
//  editenv - Environment Variable Editor C++ Class Implementation
//  Copyright (c) 2009 Dan Moulding
//	revision: 2010-10-18 Seungwoo Oh.
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//  See COPYING.txt for the full terms of the GNU Lesser General Public License.
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EnvVar.h"

// Global Constants
static UINT const	broadcastTimeout = 5000; // in milliseconds
static LPCTSTR		userEnvSubKey    = _T("Environment");
static LPCTSTR		systemEnvSubKey  = _T("System\\CurrentControlSet\\Control\\Session Manager\\Environment");

CEnvVar::CEnvVar (env_scope scope, LPCTSTR lpszName) : scope_(es_invalid), type_(REG_NONE)
{
    PBYTE       data;
    HKEY        key;
    DWORD       size;
    LONG        status;
    HKEY        subKey;
    LPCTSTR		subKeyName;

    switch (scope) {
    case es_system:
        key = HKEY_LOCAL_MACHINE;
        subKeyName = systemEnvSubKey;
        break;

    case es_user:
        key = HKEY_CURRENT_USER;
        subKeyName = userEnvSubKey;
        break;

    default:
//       assert(false);
//       return;
		key = HKEY_LOCAL_MACHINE;
        subKeyName = systemEnvSubKey;
    }

    scope_ = scope;
	name_ = lpszName;

    RegOpenKeyEx(key, subKeyName, 0, KEY_QUERY_VALUE | KEY_SET_VALUE, &subKey);
    status = RegQueryValueEx(subKey, lpszName, 0, NULL, NULL, &size);
    if (ERROR_SUCCESS != status) {
        // This environment variable doesn't exist. Assign this EnvVar object's
        // value the empty string.
        value_ = _T("");
    } else {
        // This environment variable already exists. Assign its value to this
        // EnvVar object.
        data = new BYTE [size];
        RegQueryValueEx(subKey, lpszName, 0, &type_, data, &size);
        value_ = (LPCTSTR)data;
        delete [] data;
    }
    RegCloseKey(key);
}

CEnvVar::CEnvVar (CEnvVar const &other)
{
    copy_(other);
}

CEnvVar::~CEnvVar ()
{
    destroy_();
}

CEnvVar& CEnvVar::operator = (CEnvVar const &other)
{
    if (this != &other) {
        destroy_();
        copy_(other);
    }

    return *this;
}

unsigned int CEnvVar::cut (const CString& text)
{
    unsigned int count = 0;
    HKEY key;
    HKEY subKey;
    LPCTSTR	subKeyName;

    if (es_invalid == scope_) {
        return 0;
    }

    switch (scope_) {
    case es_system:
        key = HKEY_LOCAL_MACHINE;
        subKeyName = systemEnvSubKey;
        break;

    case es_user:
        key = HKEY_CURRENT_USER;
        subKeyName = userEnvSubKey;
        break;
    }

    // Replace every instance of text with the empty string.
    int pos = value_.Find(text);
    int length = text.GetLength();
    while (pos != -1) {
        ++count;
        value_.Delete(pos, length);
        pos = value_.Find(text, pos);
    }

	if(type_ == 0)
	{
		if(value_.Find("%") < 0)
			type_ = REG_SZ;
		else
			type_ = REG_EXPAND_SZ;
	}

    // Write the new value to the registry.
    RegOpenKeyEx(key, subKeyName, 0, KEY_QUERY_VALUE | KEY_SET_VALUE, &subKey);
    RegSetValueEx(subKey,
                  name_,
                  0,
                  type_,
                  (const BYTE*)(LPCTSTR)value_,
                  value_.GetLength()*sizeof(TCHAR));
    RegCloseKey(key);

    // Notify everyone of the change.
    broadcastChange_();

    return count;
}

void CEnvVar::paste (const CString& text)
{
    HKEY key;
    HKEY subKey;
    LPCTSTR subKeyName;

    if (es_invalid == scope_) {
        return;
    }

    switch (scope_) {
    case es_system:
        key = HKEY_LOCAL_MACHINE;
        subKeyName = systemEnvSubKey;
        break;

    case es_user:
        key = HKEY_CURRENT_USER;
        subKeyName = userEnvSubKey;
        break;
    }

    // Append text to the current value.
    value_ += text;

	if(type_ == 0)
	{
		if(value_.Find("%") < 0)
			type_ = REG_SZ;
		else
			type_ = REG_EXPAND_SZ;
	}

    // Write the new value to the registry.
    RegOpenKeyEx(key, subKeyName, 0, KEY_QUERY_VALUE | KEY_SET_VALUE, &subKey);
    RegSetValueEx(subKey,
                  name_,
                  0,
                  type_,
                  (const BYTE*)(LPCTSTR)value_,
                  value_.GetLength()*sizeof(TCHAR));
    RegCloseKey(key);

    // Notify everyone of the change.
    broadcastChange_();
}

void CEnvVar::set (const CString& text)
{
    HKEY key;
    HKEY subKey;
    LPCTSTR subKeyName;

    if (es_invalid == scope_) {
        return;
    }

    switch (scope_) {
    case es_system:
        key = HKEY_LOCAL_MACHINE;
        subKeyName = systemEnvSubKey;
        break;

    case es_user:
        key = HKEY_CURRENT_USER;
        subKeyName = userEnvSubKey;
        break;
    }

    // Assign the new value.
    value_ = text;

	if(type_ == 0)
	{
		if(value_.Find("%") < 0)
			type_ = REG_SZ;
		else
			type_ = REG_EXPAND_SZ;
	}

    // Write the new value to the registry.
    RegOpenKeyEx(key, subKeyName, 0, KEY_SET_VALUE, &subKey);
    RegSetValueEx(subKey,
                  name_,
                  0,
                  type_,
                  (const BYTE*)(LPCTSTR)value_,
                  value_.GetLength()*sizeof(TCHAR));
    RegCloseKey(key);

    // Notify everyone of the change.
    broadcastChange_();
}

void CEnvVar::unset ()
{
	HKEY key;
	HKEY subKey;
	LPCTSTR subKeyName;

    if (es_invalid == scope_) {
        return;
    }

    switch (scope_) {
    case es_system:
        key = HKEY_LOCAL_MACHINE;
        subKeyName = systemEnvSubKey;
        break;

    case es_user:
        key = HKEY_CURRENT_USER;
        subKeyName = userEnvSubKey;
        break;
    }

    // Assign the empty string for the EnvVar object's value.
    value_ = "";

    // Delete the value from the registry.
    RegOpenKeyEx(key, subKeyName, 0, KEY_SET_VALUE, &subKey);
    RegDeleteValue(subKey, name_);
    RegCloseKey(key);

    // Notify everyone of the change.
    broadcastChange_();
}

CString CEnvVar::value () const
{
    return value_;
}

int CEnvVar::broadcastChange_ ()
{
    DWORD_PTR result;

    // Broadcast WM_SETTINGCHANGE
    return SendMessageTimeout(HWND_BROADCAST,
                       WM_SETTINGCHANGE,
                       NULL,
                       (LPARAM)_T("Environment"),
                       SMTO_ABORTIFHUNG,
                       broadcastTimeout,
                       &result);
}

void CEnvVar::copy_ (CEnvVar const &other)
{
    name_  = other.name_;
    scope_ = other.scope_;
    value_ = other.value_;
}

void CEnvVar::destroy_ ()
{
}
