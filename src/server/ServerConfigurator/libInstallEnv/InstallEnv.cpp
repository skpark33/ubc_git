#include "StdAfx.h"
#include "InstallEnv.h"
#include "EnvVar.h"
#include <ci/libDebug/ciDebug.h>

#define BUF_SIZE		(1024*100)

ciSET_DEBUG(10, "libInstallEnv");



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기본생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallEnv::CInstallEnv()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (CMapStringToString&) mapEnv : (in) 환경변수 map
/////////////////////////////////////////////////////////////////////////////////
CInstallEnv::CInstallEnv(CMapStringToString& mapEnv)
{
	set(mapEnv);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallEnv::~CInstallEnv()
{
	m_mapEnv.RemoveAll();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 환경변수의 리스트가 정상적으로 추가되었는지 확인한다. \n
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallEnv::test()
{
	ciDEBUG(1, ("begin windows environment variavles test"));
	clearError();
	
	if( !ReadEnvList() ) return FALSE;

	bool bHasNotAdded = false;
	POSITION pos;
	CString strKey, strValue;
	CMapStringToString mapNotAdded;
	for( pos = m_mapEnv.GetStartPosition(); pos != NULL; )
	{
		m_mapEnv.GetNextAssoc(pos, strKey, strValue);
		//System 환경변수만 해당된다.
		CEnvVar env(CEnvVar::env_scope::es_system, strKey);
		if( strKey.CompareNoCase("PATH") == 0 )
		{
			//PATH 환경 변수는 System의 환경 변수값에 추가되어 있는지 ;를 기준으로 잘라서 하나씩 검사
			CString strAddedPath = env.value();
			CString strTok, strNotAddedPath = "";
			int nPos = 0;
			bool bHasNotAddedPath = false;
			//';'를 기준으로 분리
			strTok = strValue.Tokenize(";", nPos);
			while( strTok != "" )
			{
				strTok.Trim();
				if( strAddedPath.Find(strTok, 0) == -1 )
				{
					//빠져있는 path를 조합하고 나중에 빠진 전체를 map에 추가
					bHasNotAddedPath = true;
					strNotAddedPath += strTok;
					strNotAddedPath += ";";
				}//if

				strTok = strValue.Tokenize(";", nPos);		
			}//while

			if( bHasNotAddedPath )
			{
				//전체 빠져있는 path를 최종 map에 추가...
				bHasNotAdded = true;
				mapNotAdded.SetAt(strKey, strNotAddedPath);
			}//if
		}
		else
		{
			if( strValue.CompareNoCase(env.value()) != 0 )
			{
				bHasNotAdded = true;
				mapNotAdded.SetAt(strKey, strValue);
			}//if
		}//if
	}//for

	if(bHasNotAdded)
	{
		POSITION pos;
		CString strKey, strValue, strError;
		strError = "Not added env values exist : ";
		for( pos = mapNotAdded.GetStartPosition(); pos != NULL; )
		{
			mapNotAdded.GetNextAssoc(pos, strKey, strValue);
			strError += strKey;
			strError += "$";
			strError += strValue;
			strError += " | ";
		}//for

		ciDEBUG(1, ("%s", strError));
		setError(ERR_UNINSTALLED, (LPSTR)(LPCSTR)strError);
		return FALSE;
	}//if

	if( m_mapEnv.GetCount()==0 )
	{
		ciWARN(("No Environment !!!")); 
		return FALSE;
	}//if

	ciDEBUG(1, ("All Env items installed")); 
	ciDEBUG(1, ("end windows environment variavles test"));
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 환경변수들을 등록한다. \n
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallEnv::install()
{
	ciDEBUG(1, ("begin windows environment variavles install"));
	clearError();

	POSITION pos;
	CString strKey, strValue;
	// PATH제외하고 전부 처리
	for( pos = m_mapEnv.GetStartPosition(); pos != NULL; )
	{
		m_mapEnv.GetNextAssoc(pos, strKey, strValue);
		//반드시 System 환경변수로 등록해야 한다.
		CEnvVar env(CEnvVar::env_scope::es_system, strKey);
		if( strKey.CompareNoCase("PATH") == 0 )
		{
			//PATH 환경 변수는 기존의 환경 변수값에 추가 해야 한다.
			CString strAddedPath = env.value();

			int pos = 0;
			CString strAddingPath = "";
			CString strSubPath = strValue.Tokenize(";", pos); // ;단위로 분리
			while( strSubPath != "" )
			{
				if( strAddedPath.Find(strSubPath) < 0 )
				{
					// 기존PATH에 없으면 추가
					if( strAddingPath.GetLength() > 0 )
						strAddingPath += ";";
					strAddingPath += strSubPath;
				}

				strSubPath = strValue.Tokenize(";", pos);
			}

			if( strAddedPath.GetLength() > 0 && strAddingPath.GetLength() > 0 )
				strAddingPath.Insert(0, ";");//env.paste(";");
			env.paste(strAddingPath);
		}
		else
		{
			// PATH가 아닌 경우, 단순히set
			env.set(strValue);

			CEnvVar del_env(CEnvVar::env_scope::es_user, strKey);
			del_env.unset();
		}//if
	}//for

	// CEnvVar::broadcastChange_() 적용안되는 문제 있음 (윈도우OS 자체버그)
	// 따라서 수동으로 환경변수 창을 열고 Enter키 전송하여 강제적용

	// open Environment-Variables dialog
	::ShellExecute(NULL, "Open", "rundll32.exe", "sysdm.cpl,EditEnvironmentVariables", NULL, SW_NORMAL);

	// find dialog during 5-sec
	HWND hwnd = NULL;
	int cnt = 0;
	while( cnt < 1000 && hwnd == NULL )
	{
		hwnd = GetEnvVarWindow();
		cnt++;
		::Sleep(5);
	}

	if( hwnd != NULL )
	{
		cnt = 0;
		while( cnt < 1000 && hwnd != NULL )
		{
			::SetForegroundWindow(hwnd);

			// ENTER key down
			keybd_event(VK_RETURN, 0x9C, 0, 0);
			// ENTER key up
			keybd_event(VK_RETURN, 0x9C, KEYEVENTF_KEYUP, 0);

			::Sleep(5);

			cnt++;
			hwnd = GetEnvVarWindow();
		}
	}

	SaveEnvList();

	ciDEBUG(1, ("Success windows environment variavles install"));
	ciDEBUG(1, ("end windows environment variavles install"));
	return TRUE;
}

int CInstallEnv::uninstall(CMapStringToString& mapEnv)
{
	ciDEBUG(1, ("begin windows environment variavles uninstall"));
	clearError();

	POSITION pos;
	CString strKey, strValue;
	for( pos=mapEnv.GetStartPosition(); pos != NULL; )
	{
		mapEnv.GetNextAssoc(pos, strKey, strValue);
		//반드시 System 환경변수로 등록해야 한다.
		CEnvVar env(CEnvVar::env_scope::es_system, strKey);
		env.unset();
	}//for

	ciDEBUG(1, ("end windows environment variavles uninstall"));
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
///환경변수들 리스트를 반환한다. \n
/// @param (CMapStringToString*) pmapEnv : (out) 환경변수 map
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallEnv::get(CMapStringToString& mapEnv)
{
	POSITION pos;
	CString strKey, strValue;
	for( pos = m_mapEnv.GetStartPosition(); pos != NULL; )
	{
		m_mapEnv.GetNextAssoc(pos, strKey, strValue);
		mapEnv.SetAt(strKey, strValue);
	}//for

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 환경변수들 리스트를 설정한다. \n
/// @param (CStringArray&) mapEnv : (in) 환경변수 map
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallEnv::set(CMapStringToString& mapEnv)
{
	m_mapEnv.RemoveAll();

	POSITION pos;
	CString strKey, strValue;
	for( pos = mapEnv.GetStartPosition(); pos != NULL; )
	{
		mapEnv.GetNextAssoc(pos, strKey, strValue);
		m_mapEnv.SetAt(strKey, strValue);
	}//for

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 환경변수를 반환한다. \n
/// @param (LPCTSTR) lpszKey : (in) 환경변수 키
///        (CString&) strValue : (out) 환경변수 값
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallEnv::get(LPCTSTR lpszKey, CString& strValue)
{
	if( m_mapEnv.Lookup(lpszKey, strValue) )
	{
		// success
		return TRUE;
	}
	// fail
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ServerConfigurator.ini로 부터 환경변수 리스트를 읽는다. \n
/////////////////////////////////////////////////////////////////////////////////
bool CInstallEnv::ReadEnvList()
{
	clearError();

	//ServerConfigurator.ini의 경로
	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);

	CString strPath;
	strPath.Format("%s%s%s.ini", cDrive, cPath, cFilename);

	CFileStatus fs;
	if( CFile::GetStatus(strPath, fs) != TRUE )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to access file(%s)", strPath);
		ciDEBUG(1, (szBuf));
		setError(ERR_ETC, szBuf);
		return false;
	}//if

	m_strConfigPath = fs.m_szFullName;

	//ServerConfigurator.ini로 부터 환경변수 리스트를 읽어온다.
	m_mapEnv.RemoveAll();
	char cBuf[BUF_SIZE] = {0};
	CString strList, strTok, strKey, strValue;
	int nPos = 0, nIdx = 0;
	GetPrivateProfileString("EnvList", "List", "", cBuf, BUF_SIZE, m_strConfigPath);
	strList = cBuf;

	ciDEBUG(1, ("[EnvList] List=%s", strList));

	//'|'를 기준으로 분리
	strTok = strList.Tokenize("|", nPos);
	while( strTok != "" )
	{
		strTok.Trim();
		//$를 기준으로 Key%Value 형식이다...
		nIdx = strTok.Find('$', 0);
		if( nIdx != -1 )
		{
			strKey = strTok.Left(nIdx);
			strValue = strTok.Right(strTok.GetLength() - (nIdx+1));
			m_mapEnv.SetAt(strKey, strValue);
			ciDEBUG(1, ("%s=%s", strKey, strValue));
		}//if

		strTok = strList.Tokenize("|", nPos);		
	}//while

	return true;
}

void CInstallEnv::SaveEnvList(void)
{
	clearError();

	if( m_strConfigPath.GetLength() == 0 )
	{
		//ServerConfigurator.ini의 경로
		char cModule[MAX_PATH];
		::ZeroMemory(cModule, MAX_PATH);
		::GetModuleFileName(NULL, cModule, MAX_PATH);

		char cDrive[MAX_PATH]={0}, cPath[MAX_PATH]={0}, cFilename[MAX_PATH]={0}, cExt[MAX_PATH]={0};
		_splitpath(cModule, cDrive, cPath, cFilename, cExt);

		m_strConfigPath.Format("%s%s%s.ini", cDrive, cPath, cFilename);
	}

	//ServerConfigurator.ini로 부터 환경변수 리스트를 읽어온다.
	CString str_list = "";
	POSITION pos;
	CString strKey, strValue;
	for( pos = m_mapEnv.GetStartPosition(); pos != NULL; )
	{
		m_mapEnv.GetNextAssoc(pos, strKey, strValue);

		if( str_list.GetLength() ) str_list+="|";

		str_list += strKey;
		str_list += "$";
		str_list += strValue;
	}//for

	WritePrivateProfileString("EnvList", "List", str_list, m_strConfigPath);
}

HWND CInstallEnv::GetEnvVarWindow()
{
	const char* sz_window_name[] = {
		"Environment Variables", // eng
		"\x00c8\x00af\x00b0\x00e6 \x00ba\x00af\x00bc\x00f6", // "환경 변수" - kor
		"\x008A\x00C2\x008B\x00AB\x0095\x00CF\x0090\x0094", // "環境變數" - jpn (진짜값은 일본용 유니코드문자)
	};

	for(int i=0; i<3; i++)
	{
		HWND hwnd = ::FindWindow(NULL, sz_window_name[i]);
		if( hwnd != NULL ) return hwnd;
	}

	return NULL;
}
