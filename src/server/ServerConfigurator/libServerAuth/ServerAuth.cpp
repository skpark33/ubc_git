#include "StdAfx.h"
#include "ServerAuth.h"
#include "resource.h"

#include "ci/libBase/ciStringTokenizer.h"
#include "ci/libDebug/ciDebug.h"
//#include "ci/libConfig/ciXProperties.h"
#include "common/libScratch/scratchUtil.h"
#include "common/libCommon/ubcIni.h"

#include "common/libHttpRequest/HttpRequest.h"
//#include "common/libInstall/installUtil.h"
#include "common/libInstallWeb/installWebUtil.h"


ciSET_DEBUG(10, "CServerAuth");


CServerAuth::CServerAuth()
{
	m_bOnceTimeForceAuth = false;
}

CServerAuth::~CServerAuth()
{
}

int CServerAuth::get(LPCSTR key, CString& value)
{
	value = "";

	if( m_mapKeyValues.Lookup(key, value) ) return TRUE;

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);

	return FALSE;
}

int CServerAuth::set(LPCSTR key, LPCSTR value)
{
	m_mapKeyValues.SetAt(key, value);
	return TRUE;
}

CString CServerAuth::get(LPCSTR key)
{
	CString value="";
	m_mapKeyValues.Lookup(key, value);
	return value;
}

int CServerAuth::test()
{
	ciDEBUG(1, ("test()"));

	clearError();

	bool retval = true;

	//
	ciString current_ip;
	scratchUtil::getInstance()->getIpAddress(current_ip);

	//
	if( current_ip.empty() )
	{
		setError(-1, "Fail to get current ip address");
		ciERROR((_errString.c_str()));
		set(KEY_CURRENT_IP_ADDRESS, _errString.c_str());
		return FALSE;
	}
	else
	{
		ciDEBUG(1, ("%s=%s", KEY_CURRENT_IP_ADDRESS, current_ip.c_str()));
		set(KEY_CURRENT_IP_ADDRESS, current_ip.c_str());
	}

	//
	ciString real_mac_addr = scratchUtil::getInstance()->GetMacaddr();
	if( real_mac_addr.empty() )
	{
		setError(-1, "Fail to get mac address");
		ciERROR((_errString.c_str()));
		set(KEY_CURRENT_MAC_ADDRESS, _errString.c_str());
		return FALSE;
	}
	else
	{
		ciDEBUG(1, ("%s=%s", KEY_CURRENT_MAC_ADDRESS, real_mac_addr.c_str()));
		set(KEY_CURRENT_MAC_ADDRESS, real_mac_addr.c_str());
	}

	//
	ciString enterprise_key="", pwd="", file_mac_addr="";
	scratchUtil::getInstance()->readAuthFile(enterprise_key, file_mac_addr, pwd);

	//
	if( enterprise_key.empty() ) retval = false;
	ciDEBUG(1,("%s=%s", KEY_ENTERPRISE_KEY, enterprise_key.c_str()));
	set(KEY_ENTERPRISE_KEY, enterprise_key.c_str());

	//
	if( pwd.empty() ) retval = false;
	ciDEBUG(1,("%s=%s", KEY_PASSWORD, pwd.c_str()));
	set(KEY_PASSWORD, pwd.c_str());

	//
	if( file_mac_addr.empty() || file_mac_addr != real_mac_addr )
	{
		retval = false;
		ciWARN(("different MAC Address from real-mac to auth-file-mac"));
	}
	ciDEBUG(1,("%s=%s from auth-file", KEY_CURRENT_MAC_ADDRESS, file_mac_addr.c_str()));

	//
	ciDEBUG(1, ("test(%d,%s) end", retval, _errString.c_str()) );
	return retval;
}

int	CServerAuth::install()
{
	ciDEBUG(1,("install()"));

	clearError();

	//
	CString str_ent_key, str_pwd, str_ip, str_mac;
	get(KEY_CURRENT_IP_ADDRESS,		str_ip);
	get(KEY_CURRENT_MAC_ADDRESS,	str_mac);
	get(KEY_ENTERPRISE_KEY,			str_ent_key);
	get(KEY_PASSWORD,				str_pwd);
	ciDEBUG(1,("%s=%s", KEY_CURRENT_IP_ADDRESS,	str_ip));
	ciDEBUG(1,("%s=%s", KEY_ENTERPRISE_KEY,		str_ent_key));
	ciDEBUG(1,("%s=%s", KEY_PASSWORD,			str_pwd));

	string limit_max ="0,0,0";

	CStringArray response_list;

	if( m_bOnceTimeForceAuth )
	{
		response_list.Add("Authenticate");
		response_list.Add("");
	}
	else
	{
		// 일반 인증 (강제인증 아님)

		//
		CHttpRequest http_request;
		http_request.Init(CHttpRequest::CONNECTION_CENTER);

		//
		CString send_msg;
		send_msg.Format("entKey=%s&pwd=%s&ipAddr=%s", str_ent_key, str_pwd, str_ip);
		ciDEBUG(1,("SendMessage=%s", send_msg));

		//
		BOOL ret_http = http_request.RequestPost("/UBC_Registration/authorize_server.asp", send_msg, response_list);
		ciDEBUG(1, ("URL=%s", http_request.GetRequestURL()) );
		for( int i=0; i<response_list.GetSize(); i++ )
		{
			ciDEBUG(1, ("Line(%d)=%s", i, response_list.GetAt(i)) );
		}
		if( ret_http == FALSE || response_list.GetSize()==0 )
		{
			// 통신에러
			setError(1, "Fail to connect UBC Center Server");
			ciWARN((_errString.c_str()));
			return FALSE;
		}

		const CString& result = response_list.GetAt(0);
		if( result == "IpIsNull" )
		{
			// ipAddress-parameter is null
			setError(2, "IP address is NULL");
			ciWARN((_errString.c_str()));
			return FALSE;
		}
		else if( result == "NotExistKey" )
		{
			// not exist key from UBC Center server

			setError(3, "Not exist enterprise-key on UBC Center server");
			ciWARN((_errString.c_str()));
			return FALSE;
		}
		else if( result == "InvalidPassword" )
		{
			// not match password from UBC Center server
			setError(4, "Invalid password");
			ciWARN((_errString.c_str()));
			return FALSE;
		}
		else if( result != "Authenticate" /*|| response_list.GetSize() < 2*/ )
		{
			// unknown error
			setError(5, "Unknown ERROR");
			ciWARN((_errString.c_str()));
			return FALSE;
		}
		else if( response_list.GetSize() < 2 )
		{
			response_list.Add("");
		}
	}

	limit_max = (LPCSTR)response_list.GetAt(1);
	ciDEBUG(1,("limit_max=%s", limit_max.c_str()));

	// 인증
	ciString mac = (LPCSTR)str_mac;
	installWebUtil::getInstance()->SetOnceTimeForceAuth(m_bOnceTimeForceAuth);
	int ret_val = installWebUtil::getInstance()->auth("SERVER", str_ent_key, str_pwd, _errString, mac);
	if( ret_val <= 0 )
	{
		_errCode = 6;
		ciERROR((_errString.c_str()));
		return FALSE;
	}

	// 단말수, 사이트수, 유저수를 다른 인증서에 적는다.
	ciStringTokenizer aTokens(limit_max.c_str(),",");
	ciString max_host = "0";
	ciString max_site = "0";
	ciString max_user = "0";

	if( aTokens.hasMoreTokens() ) max_host = aTokens.nextToken();
	if( aTokens.hasMoreTokens() ) max_site = aTokens.nextToken();
	if( aTokens.hasMoreTokens() ) max_user = aTokens.nextToken();
	scratchUtil::getInstance()->writeLimitFile(max_host.c_str(),max_site.c_str(),max_user.c_str());

	ciDEBUG(1, (_errString.c_str()));
	ciDEBUG(1, ("install() - Authorized"));

	return TRUE;
}
