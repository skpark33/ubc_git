#pragma once

using namespace std;

//#include "../ServerConfigurator/Installable/installable.h"
#include "../Installable/installable.h"

#define		KEY_CURRENT_IP_ADDRESS		("CurrentIPAddress")
#define		KEY_CURRENT_MAC_ADDRESS		("CurrentMACAddress")
#define		KEY_ENTERPRISE_KEY			("EnterpriseKey")
#define		KEY_PASSWORD				("Password")

class CServerAuth : public installable
{
public:
	CServerAuth(void);
	~CServerAuth(void);

	virtual int get(LPCSTR key, CString& value);
	virtual int set(LPCSTR key, LPCSTR value);
	virtual CString get(LPCSTR key);

	virtual int prerequisite() { return 0; };
	virtual int test();
	virtual int	install();

	void	SetOnceTimeForceAuth(bool bForce) { m_bOnceTimeForceAuth=bForce; };

protected:
	CMapStringToString		m_mapKeyValues;

	bool	 m_bOnceTimeForceAuth;

};
