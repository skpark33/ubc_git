//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by libServerAuth.rc
//
#define IDS_ERR_FAIL_TO_GET_IP_ADDR     5000
#define IDS_ERR_FAIL_TO_GET_MAC_ADDR    5001
#define IDS_ERR_NOT_CONFIRM_NAMESERVER_HOST 5002
#define IDS_ERR_NOT_CONFIRM_PROPERTIES  5003
#define IDS_ERR_NOT_CONFIRM_INI         5004
#define IDS_AUTHENTICATE                5005
#define IDS_ALREADY_AUTHENTICATED       5006
#define IDS_ERR_FAIL_TO_CONNECT_CENTER  5007
#define IDS_ERR_IP_IS_NULL              5008
#define IDS_ERR_NOT_EXIST_KEY           5009
#define IDS_ERR_INVALID_PASSWORD        5010
#define IDS_ERR_UNKNOWN_ERROR           5011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        5014
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
