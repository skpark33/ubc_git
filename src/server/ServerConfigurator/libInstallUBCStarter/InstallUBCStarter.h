/************************************************************************************/
/*! @file InstallUBCStarter.h
	@brief UBCStarter를 시작프로그램에 등록하는 클래스 선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2012/07/23\n
	▶ 참고사항:
		
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2012/07/23:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#pragma once
using namespace std;

#include "../Installable/installable.h"


#ifndef _ERR_CODDE
	#define _ERR_CODDE
	#define ERR_TEST_FAIL			-1			//테스트 과정 실패
	#define ERR_UNINSTALLED			-2			//테스트 결과 설치가 되어있지 않음
	#define ERR_FAIL_INSTALL		-3			//설치 실패
	#define ERR_FAIL_UNINSTALL		-4			//uninstall fail
	#define ERR_ETC					-10			//기타 실패
#endif


//! 윈도우즈 자동업데이트를 못하도록 설정하는 클래스
/*!
*/
class CInstallUBCStarter : public installable
{
public:
	CInstallUBCStarter(void);				///<기본 생성자
	virtual ~CInstallUBCStarter(void);		///<소멸자

	virtual int prerequisite()	{ setError(-1,"NO Service"); return 0; };
	virtual int test();								///<UBCStarter가 시작프로그램에 등록되어 있는지 검사한다.
	virtual int	install();							///<UBCStarter를 시작프로그램에 등록한다.

protected:

	typedef struct {
		CString strFullPath;		// 대상 전체경로
		CString strWorkingPath;		// 대상 경로
		CString strArguments;		// 파라미터
		CString strIconPath;		// 아이콘 전체경로
		int		nIconIndex;			// 아이콘 인덱스
		CString strTitle;			// 바로가기 이름
		CString strDescription;		// 설명
		DWORD	fMakeLocationFlag;	// 바로가기 위치
	} SHORTCUT_ITEM;
				// fMakeLocationFlag => can use OR-Flag
				// 0x0001 = Desktop
				// 0x0002 = Startup
				// 0x0004 = QuickLaunch

	BOOL	CreateShortcut( SHORTCUT_ITEM& item );

};
