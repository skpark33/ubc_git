#include "StdAfx.h"
#include "InstallUBCStarter.h"
#include "shlobj.h"
#include <ci/libDebug/ciDebug.h>


ciSET_DEBUG(10, "libInstallUBCStarter");


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기본생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallUBCStarter::CInstallUBCStarter()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallUBCStarter::~CInstallUBCStarter()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UBCStarter가 시작프로그램에 등록되어 있는지 검사한다. \n
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallUBCStarter::test()
{
	ciDEBUG(1, ("begin UBCStarter test"));
	clearError();

	CString strTarget;
	char szTargetPath[MAX_PATH+1]={0};
	::SHGetSpecialFolderPath(NULL, szTargetPath, CSIDL_STARTUP, FALSE);
	strTarget = szTargetPath;
	strTarget += _T("\\UBC Start.lnk");
	if( !PathFileExists(strTarget) )
	{
		char szBuf[1024]={0};
		sprintf(szBuf, "Not installed UBC Starter.lnk");
		ciDEBUG(1, (szBuf));
		setError(ERR_UNINSTALLED, szBuf);
		return FALSE;
	}//if

	ciDEBUG(1, ("Already installed UBC Starter.lnk"));
	ciDEBUG(1, ("end UBCStarter test"));
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UBCStarter를 시작프로그램에 등록한다. \n
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallUBCStarter::install()
{
	ciDEBUG(1, ("begin UBCStarter install"));

	SHORTCUT_ITEM item;
	item.strFullPath    = "C:\\Project\\ubc\\bin8\\SelfAutoUpdate.exe";
	item.strWorkingPath = "C:\\Project\\ubc\\bin8";
	item.strArguments   = "+server +log";
	item.strIconPath    = "C:\\Project\\ubc\\3rdparty\\icon\\UBCPlayerStart.ico";
	item.nIconIndex     = 0;
	item.strTitle       = "UBC Start";
	item.strDescription = "Start UBC Server Processes";
	item.fMakeLocationFlag= 0x0002;

	char str_drive[MAX_PATH+1] = {0};
	::GetModuleFileName(NULL, str_drive, MAX_PATH);

	item.strFullPath.SetAt(0, str_drive[0]);
	item.strWorkingPath.SetAt(0, str_drive[0]);
	item.strIconPath.SetAt(0, str_drive[0]);

	if( !CreateShortcut(item) ) return FALSE;

/*
	clearError();

	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);

	CString strPath;
	strPath.Format("%s%sMenuRegister.exe /install /server", cDrive, cPath);

	//MenuRegister 실행
	STARTUPINFO         sINFO = {0};
	PROCESS_INFORMATION pINFO = {0};

	sINFO.cb = sizeof( sINFO );

	//if( ::system(strPath) )
	if(!CreateProcess(NULL, (LPSTR)(LPCSTR)strPath, NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &sINFO, &pINFO ) ) // CREATE_NO_WINDOW = 윈도우없이 실행
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to run MenuRegister fille : [%s]", strPath);
		ciDEBUG(1, (szBuf));
		setError(ERR_FAIL_INSTALL, szBuf);
		return FALSE;
	}//if

	//실행종료 대기
	WaitForSingleObject(pINFO.hProcess, INFINITE);

	CloseHandle(pINFO.hProcess);
	CloseHandle(pINFO.hThread);
*/

	ciDEBUG(1, ("Success UBCStarter install"));
	ciDEBUG(1, ("end UBCStarter install"));
	return TRUE;
}

BOOL CInstallUBCStarter::CreateShortcut( SHORTCUT_ITEM& item )
{
	ciDEBUG(1, ("CreateShortcut() start"));
	ciDEBUG(1, ("strFullPath       = %s", item.strFullPath));
	ciDEBUG(1, ("strWorkingPath    = %s", item.strWorkingPath));
	ciDEBUG(1, ("strArguments      = %s", item.strArguments));
	ciDEBUG(1, ("strIconPath       = %s", item.strIconPath));
	ciDEBUG(1, ("nIconPath         = %d", item.nIconIndex));
	ciDEBUG(1, ("strTitle          = %s", item.strTitle));
	ciDEBUG(1, ("strDescription    = %s", item.strDescription));
	ciDEBUG(1, ("fMakeLocationFlag = %d", item.fMakeLocationFlag));

	if( item.fMakeLocationFlag == 0 )
	{
		setError(-1, "MakeLocationFlag is NULL");
		ciWARN(("MakeLocationFlag is NULL"));
		return FALSE;
	}

	//  실제 대상파일이 존재하는지 체크하는 루틴추가.
	if( _taccess( item.strFullPath, 0 ) )
	{
		char errBuf[1024]={0};
		sprintf(errBuf, "File not found (%s)", item.strFullPath);
		setError(errno, errBuf);
		ciWARN((errBuf));
		return FALSE;
	}
	if( item.strWorkingPath.GetLength() != 0 && _taccess( item.strWorkingPath, 0 ) )
	{
		char errBuf[1024]={0};
		sprintf(errBuf, "Working path not found (%s)", item.strWorkingPath);
		setError(errno, errBuf);
		ciWARN((errBuf));
		return FALSE;
	}

	//
	if( item.strWorkingPath.GetLength() == 0 || item.strTitle.GetLength() == 0 )
	{
		TCHAR drv[MAX_PATH], dir[MAX_PATH], fn[MAX_PATH], ext[MAX_PATH];
		::_tsplitpath(item.strFullPath, drv, dir, fn, ext);

		if( item.strWorkingPath.GetLength() == 0 )
		{
			item.strWorkingPath.Format("%s%s", drv, dir);
			ciDEBUG(1, ("strWorkingPath=%d", item.strWorkingPath));
		}
		if( item.strTitle.GetLength() == 0 )
		{
			item.strTitle = fn;
			ciDEBUG(1, ("strTitle=%d", item.strTitle));
		}
	}

	//
	CStringArray listShortCutFullPath;
	if( item.fMakeLocationFlag & 0x0001 ) // Desktop
	{
		// 바탕화면 아이콘이 저장될 경로 설정
		TCHAR szDesktopPath[MAX_PATH] = {0};

		if( FAILED(::SHGetFolderPath(NULL, CSIDL_DESKTOP , NULL, 0, szDesktopPath)) )
		{
			setError(-1, "Fail to get Desktop path");
			ciWARN(("Fail to get Desktop path"));
			return FALSE;
		}

		_tcscat(szDesktopPath, "\\");
		_tcscat(szDesktopPath, item.strTitle);
		_tcscat(szDesktopPath, ".lnk");

		listShortCutFullPath.Add(szDesktopPath);
		ciDEBUG(1, ("ShortCutFullPath=%s", szDesktopPath));
	}
	if( item.fMakeLocationFlag & 0x0002 ) // Startup
	{
		// 시작프로그램 아이콘이 저장될 경로 설정
		TCHAR szStartupPath[MAX_PATH] = {0};

		if( FAILED(::SHGetFolderPath(NULL, CSIDL_STARTUP, NULL, 0, szStartupPath)) )
		{
			setError(-1, "Fail to get Startup path");
			ciWARN(("Fail to get Startup path"));
			return FALSE;
		}

		_tcscat(szStartupPath, "\\");
		_tcscat(szStartupPath, item.strTitle);
		_tcscat(szStartupPath, ".lnk");

		listShortCutFullPath.Add(szStartupPath);
		ciDEBUG(1, ("ShortCutFullPath=%s", szStartupPath));
	}
	if( item.fMakeLocationFlag & 0x0004 ) // QuickLaunch
	{
		// 빠른실행 아이콘이 저장될 경로 설정
		TCHAR szQuickLaunchPath[MAX_PATH] = {0};

		if( FAILED(::SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szQuickLaunchPath)) )
		{
			setError(-1, "Fail to get Quick-launch path");
			ciWARN(("Fail to get Quick-launch path"));
			return FALSE;
		}

		_tcscat(szQuickLaunchPath, "\\Microsoft\\Internet Explorer\\Quick Launch\\");
		_tcscat(szQuickLaunchPath, item.strTitle);
		_tcscat(szQuickLaunchPath, ".lnk");

		listShortCutFullPath.Add(szQuickLaunchPath);
		ciDEBUG(1, ("ShortCutFullPath=%s", szQuickLaunchPath));
	}

	// COM Object 초기화
	CoInitialize(NULL);

	BOOL bResult = TRUE;
	IShellLink* pShellLink;
	IPersistFile* pPersistFile;

	for(int i=0; i<listShortCutFullPath.GetCount(); i++)
	{
		const CString& strShortCutFullPath = listShortCutFullPath.GetAt(i);

		// IShellLink 객체를 생성하고 포인터를 구함
		HRESULT hResult = CoCreateInstance(
											CLSID_ShellLink,
											NULL,
											CLSCTX_INPROC_SERVER,
											IID_IShellLink,
											(LPVOID*) &pShellLink
										);

		if( SUCCEEDED(hResult) )
		{
			// 단축아이콘의 대상체와 설명을 설정한다.
			pShellLink->SetPath(             item.strFullPath );
			pShellLink->SetDescription(      item.strDescription );
			pShellLink->SetArguments(        item.strArguments );
			pShellLink->SetWorkingDirectory( item.strWorkingPath );
			if(item.strIconPath.GetLength())
				pShellLink->SetIconLocation( item.strIconPath, item.nIconIndex );

			// 저장하기 위해 IPersistFile 객체 생성
			hResult = pShellLink->QueryInterface( IID_IPersistFile, (LPVOID *) &pPersistFile );

			// 유니코드로 파일 패스를 변경한 후 저장
			if( SUCCEEDED(hResult) )
			{
				WORD wszFullPath[MAX_PATH] = {0};
				MultiByteToWideChar( 
					CP_ACP, 
					MB_PRECOMPOSED, 
					strShortCutFullPath, 
					-1, 
					LPWSTR(wszFullPath),
					MAX_PATH );
				ciDEBUG(1, ("strShortCutFullPath=%s", strShortCutFullPath));

				if( pPersistFile->Save( LPWSTR(wszFullPath), TRUE ) != S_OK )
				{
					setError(-1, "Fail to save shortcut");
					ciWARN(("Fail to save shortcut"));
					bResult = FALSE;
				}
				else
				{
					ciDEBUG(1, ("Success (%s)", strShortCutFullPath));
				}

				pPersistFile->Release();

				// 아이콘 재설정
				::SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST | SHCNF_FLUSH, NULL, NULL);
			}
			else
			{
				setError(-1, "Fail to QueryInterface shortcut");
				ciWARN(("Fail to QueryInterface shortcut"));
				bResult = FALSE;
			}
			pShellLink->Release();
		}
		else
		{
			setError(-1, "Fail to create shortcut object");
			ciWARN(("Fail to create shortcut object"));
			bResult = FALSE;
		}
	}

	CoUninitialize();
	ciDEBUG(1, ("CreateShortcut() end"));
	return bResult;
}
