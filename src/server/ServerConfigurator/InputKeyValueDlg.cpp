// InputKeyValueDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "InputKeyValueDlg.h"


// CInputKeyValueDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CInputKeyValueDlg, CDialog)

CInputKeyValueDlg::CInputKeyValueDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputKeyValueDlg::IDD, pParent)
	, m_reposControl ( this )
{
	m_bNumberValue = false;
	m_nDialogHeight = -1;
}

CInputKeyValueDlg::~CInputKeyValueDlg()
{
}

void CInputKeyValueDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_GROUP, m_stcGroup);
	DDX_Control(pDX, IDC_STATIC_KEY, m_stcKey);
	DDX_Control(pDX, IDC_STATIC_VALUE, m_stcValue);
	DDX_Control(pDX, IDC_EDIT_KEY, m_editKey);
	DDX_Control(pDX, IDC_EDIT_VALUE, m_editValue);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CInputKeyValueDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CInputKeyValueDlg 메시지 처리기입니다.

BOOL CInputKeyValueDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	if(m_bNumberValue)
		m_editValue.ModifyStyle(NULL, ES_NUMBER);

	//
	CDC* dc = GetDC();
	CFont font;
	font.CreatePointFont(8*10, "Tahoma");

	CFont* old_font = dc->SelectObject(&font);
	CSize key_size = dc->GetOutputTextExtent(m_strKeyName);
	CSize value_size = dc->GetOutputTextExtent(m_strValueName);

	CRect key_rect, value_rect;
	m_stcKey.GetWindowRect(key_rect);
	m_stcValue.GetWindowRect(value_rect);

	if(key_rect.Width() < key_size.cx || value_rect.Width() < value_size.cx)
	{
		int diff = max(key_size.cx-key_rect.Width(), value_size.cx-value_rect.Width());
		key_rect.right += diff;
		value_rect.right += diff;
		ScreenToClient(key_rect);
		ScreenToClient(value_rect);
		m_stcKey.MoveWindow(key_rect);
		m_stcValue.MoveWindow(value_rect);

		CRect edit_rect;
		m_editKey.GetWindowRect(edit_rect);
		edit_rect.left += diff;
		ScreenToClient(edit_rect);
		m_editKey.MoveWindow(edit_rect);

		m_editValue.GetWindowRect(edit_rect);
		edit_rect.left += diff;
		ScreenToClient(edit_rect);
		m_editValue.MoveWindow(edit_rect);
	}
	dc->SelectObject(old_font);
	ReleaseDC(dc);

	//
	m_reposControl.AddControl((CWnd*)&m_stcGroup, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl((CWnd*)&m_editKey, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl((CWnd*)&m_editValue, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl((CWnd*)&m_btnOK, REPOS_RESIZE_DIV2, REPOS_FIX, REPOS_RESIZE_DIV2, REPOS_FIX, 2);
	m_reposControl.AddControl((CWnd*)&m_btnCancel, REPOS_RESIZE_DIV2, REPOS_FIX, REPOS_RESIZE_DIV2, REPOS_FIX, 2);

	CRect client_rect;
	GetWindowRect(client_rect);
	m_nDialogHeight = client_rect.Height();

	//
	SetWindowText(m_strTitle);
	m_stcKey.SetWindowText(m_strKeyName);
	m_editKey.SetWindowText(m_strKey);
	m_stcValue.SetWindowText(m_strValueName);
	m_editValue.SetWindowText(m_strValue);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CInputKeyValueDlg::OnOK()
{
	m_editKey.GetWindowText(m_strKey);
	m_editValue.GetWindowText(m_strValue);

	if(m_strKey.GetLength() == 0)
	{
		CString msg;
		msg.Format(IDS_MSG_ITEM_NAME_IS_NULL, m_strKeyName);

		::AfxMessageBox(msg, MB_ICONWARNING);
		return;
	}

	CDialog::OnOK();
}

void CInputKeyValueDlg::SetDialogInfo(LPCSTR lpszTitle, LPCSTR lpszKeyName, LPCSTR lpszKey, LPCSTR lpszValueName, LPCSTR lpszValue)
{
	if(lpszTitle)		m_strTitle		= lpszTitle;
	if(lpszKeyName)		m_strKeyName	= lpszKeyName;
	if(lpszKey)			m_strKey		= lpszKey;
	if(lpszValueName)	m_strValueName	= lpszValueName;
	if(lpszValue)		m_strValue		= lpszValue;
}

void CInputKeyValueDlg::SetDialogInfo(UINT nTitleID, UINT nKeyNameID, LPCSTR lpszKey, UINT nValueNameID, LPCSTR lpszValue)
{
	m_strTitle.LoadString(nTitleID);
	m_strKeyName.LoadString(nKeyNameID);
	if(lpszKey)			m_strKey		= lpszKey;
	m_strValueName.LoadString(nValueNameID);
	if(lpszValue)		m_strValue		= lpszValue;
}

void CInputKeyValueDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	m_reposControl.MoveControl(TRUE);
}

void CInputKeyValueDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	if(m_nDialogHeight > 0)
	{
		lpMMI->ptMinTrackSize.y = m_nDialogHeight;
		lpMMI->ptMaxTrackSize.y = m_nDialogHeight;
	}
	lpMMI->ptMinTrackSize.x = 200;

	CDialog::OnGetMinMaxInfo(lpMMI);
}
