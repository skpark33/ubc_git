#pragma once
#include "afxwin.h"

#include "ReposControl2.h"

// CInputKeyValueDlg 대화 상자입니다.

class CInputKeyValueDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputKeyValueDlg)

public:
	CInputKeyValueDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CInputKeyValueDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_INPUT_KEY_VALUE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CReposControl2	m_reposControl;
	int		m_nDialogHeight;

	CString	m_strTitle;
	CString	m_strKeyName;
	CString	m_strValueName;
	CString	m_strKey;
	CString	m_strValue;

	bool	m_bNumberValue;

public:
	CStatic m_stcGroup;
	CStatic	m_stcKey;
	CStatic	m_stcValue;
	CEdit	m_editKey;
	CEdit	m_editValue;

	CButton m_btnOK;
	CButton m_btnCancel;

	void	SetDialogInfo(LPCSTR lpszTitle, LPCSTR lpszKeyName, LPCSTR lpszKey, LPCSTR lpszValueName, LPCSTR lpszValue);
	void	SetDialogInfo(UINT nTitleID, UINT nKeyNameID, LPCSTR lpszKey, UINT nValueNameID, LPCSTR lpszValue);
	void	SetNumberValue(bool isNumber=true) { m_bNumberValue=isNumber; };

	void	GetValue(CString& key, int& value) { key=m_strKey; value=atoi(m_strValue); };
	void	GetValue(CString& key, CString& value) { key=m_strKey; value=m_strValue; };
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
};
