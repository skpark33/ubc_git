
#include "StdAfx.h"
#include "ReposControl2.h"


CReposControl2::CReposControl2(CWnd*	pParent)
:	m_pParent (pParent)
,	m_bInit(false)
{
}

CReposControl2::~CReposControl2()
{
}

void CReposControl2::Clear()
{
	m_bInit = false;
	m_ReposControlInfoList.RemoveAll();
}

void CReposControl2::AddControl(CWnd* pControl, int nLeft, int nTop, int nRight, int nBottom, int nHorzCount, int nVertCount, CWnd* pParentWnd)
{
	if(m_bInit == false)
	{
		m_bInit = true;
		if(m_pParent && m_pParent->GetSafeHwnd())
			m_pParent->GetClientRect(m_rectParent);
	}

	if(pControl != NULL && pControl->GetSafeHwnd())
	{
		// Modified by 정운형 2008-11-17 오후 3:39
		// 변경내역 :  Help dialog control위치 버그수정
		//GetWindowRect로 얻어오는 rect값이 프로그램이 전체화면모드일때와
		//window 화면 모드일때 다르게 나옴
		/*int caption_height	= ::GetSystemMetrics(SM_CYSIZE);
		int frame_width		= ::GetSystemMetrics(SM_CXFRAME);
		int frame_height	= ::GetSystemMetrics(SM_CYFRAME);*/

		//
		REPOS_CONTROL_INFO2 info;

		info.pWnd		= pControl;
		info.nLeft		= nLeft;
		info.nTop		= nTop;
		info.nRight		= nRight;
		info.nBottom	= nBottom;
		info.nHorzCount	= nHorzCount;
		info.nVertCount	= nVertCount;
		info.pParentWnd	= pParentWnd;

		if(pParentWnd == NULL)
			info.rectParent = m_rectParent;
		else
			pParentWnd->GetClientRect(info.rectParent);

		//pControl->GetWindowRect(info.rect);
		WINDOWPLACEMENT stWndPlc;
		::ZeroMemory(&stWndPlc, sizeof(WINDOWPLACEMENT));
		GetWindowPlacement(pControl->GetSafeHwnd(), &stWndPlc);
		info.rect = stWndPlc.rcNormalPosition;

		//info.rect.OffsetRect(-frame_width, -(caption_height+frame_height));
		// Modified by 정운형 2008-11-17 오후 3:39
		// 변경내역 :  Help dialog control위치 버그수정		

		m_ReposControlInfoList.Add(info);
	}
}

void CReposControl2::MoveControl(BOOL bRepaint)
{
	if(m_pParent && m_pParent->GetSafeHwnd())
	{
		//
		CRect parent_rect;
		m_pParent->GetClientRect(parent_rect);

		int width_diff = parent_rect.Width() - m_rectParent.Width();
		int height_diff = parent_rect.Height() - m_rectParent.Height();

		//
		int count = m_ReposControlInfoList.GetCount();

		for(int i=0; i<count; i++)
		{
			REPOS_CONTROL_INFO2& info = m_ReposControlInfoList.GetAt(i);

			if(info.pWnd->GetSafeHwnd())
			{
				CRect rect = info.rect;

				if(info.pParentWnd == NULL)
				{
					if(info.nLeft   == REPOS_MOVE) rect.left   += width_diff;	else if(info.nLeft   != REPOS_FIX) rect.left   += (width_diff  * info.nLeft   / info.nHorzCount);
					if(info.nRight  == REPOS_MOVE) rect.right  += width_diff;	else if(info.nRight  != REPOS_FIX) rect.right  += (width_diff  * info.nRight  / info.nHorzCount);
					if(info.nTop    == REPOS_MOVE) rect.top    += height_diff;	else if(info.nTop    != REPOS_FIX) rect.top    += (height_diff * info.nTop    / info.nVertCount);
					if(info.nBottom == REPOS_MOVE) rect.bottom += height_diff;	else if(info.nBottom != REPOS_FIX) rect.bottom += (height_diff * info.nBottom / info.nVertCount);
				}
				else
				{
					CRect parent_rect;
					info.pParentWnd->GetClientRect(parent_rect);

					int width_diff = parent_rect.Width() - info.rectParent.Width();
					int height_diff = parent_rect.Height() - info.rectParent.Height();

					if(info.nLeft   == REPOS_MOVE) rect.left   += width_diff;	else if(info.nLeft   != REPOS_FIX) rect.left   += (width_diff  * info.nLeft   / info.nHorzCount);
					if(info.nRight  == REPOS_MOVE) rect.right  += width_diff;	else if(info.nRight  != REPOS_FIX) rect.right  += (width_diff  * info.nRight  / info.nHorzCount);
					if(info.nTop    == REPOS_MOVE) rect.top    += height_diff;	else if(info.nTop    != REPOS_FIX) rect.top    += (height_diff * info.nTop    / info.nVertCount);
					if(info.nBottom == REPOS_MOVE) rect.bottom += height_diff;	else if(info.nBottom != REPOS_FIX) rect.bottom += (height_diff * info.nBottom / info.nVertCount);
				}

				info.pWnd->MoveWindow(rect, bRepaint);
			}
		}

		m_pParent->Invalidate(FALSE);
	}
}

void CReposControl2::Move(CWnd* pWnd, int left, int top, int right, int bottom)
{
	int count = m_ReposControlInfoList.GetCount();

	for(int i=0; i<count; i++)
	{
		REPOS_CONTROL_INFO2& info = m_ReposControlInfoList.GetAt(i);

		if(pWnd == info.pWnd)
		{
			info.rect.left += left;
			info.rect.top += top;
			info.rect.right+= right;
			info.rect.bottom+= bottom;
			break;
		}
	}
}

/*
	// Vertical		4-resize
	m_pos.AddControl(&m_static1, REPOS_FIX, REPOS_FIX,			REPOS_MOVE, REPOS_RESIZE_DIV2,	1, 4);
	m_pos.AddControl(&m_static2, REPOS_FIX, REPOS_RESIZE_DIV2,	REPOS_MOVE, REPOS_RESIZE_DIV3,	1, 4);
	m_pos.AddControl(&m_static3, REPOS_FIX, REPOS_RESIZE_DIV3,	REPOS_MOVE, REPOS_RESIZE_DIV4,	1, 4);
	m_pos.AddControl(&m_static4, REPOS_FIX, REPOS_RESIZE_DIV4,	REPOS_MOVE, REPOS_MOVE,			1, 4);

	// Horizontal	1-fix,2-resize,1-fix
	m_pos.AddControl(&m_static1, REPOS_FIX,			REPOS_FIX, REPOS_FIX,			REPOS_MOVE, 2, 1);
	m_pos.AddControl(&m_static2, REPOS_FIX	,		REPOS_FIX, REPOS_RESIZE_DIV2,	REPOS_MOVE, 2, 1);
	m_pos.AddControl(&m_static3, REPOS_RESIZE_DIV2,	REPOS_FIX, REPOS_MOVE,			REPOS_MOVE, 2, 1);
	m_pos.AddControl(&m_static4, REPOS_MOVE,		REPOS_FIX, REPOS_MOVE,			REPOS_MOVE, 2, 1);
*/
