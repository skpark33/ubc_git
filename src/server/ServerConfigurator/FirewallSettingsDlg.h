#pragma once


#include "SubDlg.h"
#include "libInstallFireWall/InstallFireWall.h"

#include "ReposControl2.h"


// CFirewallSettingsDlg 대화 상자입니다.

class CFirewallSettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(CFirewallSettingsDlg)

public:
	CFirewallSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFirewallSettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FIREWALL_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CReposControl2		m_reposControl;

	bool				m_bValueChange;

	CString				m_strProgramType;
	CString				m_strPortType;

	CInstallFireWall	m_FireWall;
	CStringArray		m_listSavedPrograms;
	CStringArray		m_listSavedPorts;

	bool		AddProgram(LPCSTR lpszProgram);
	bool		AddPort(int nPort);
	void		ImportDefaultValues();

	bool		DeleteStringInList(LPCSTR lpszStr, CStringArray& listStr);

public:
	CStatic			m_stcFirewallList;
	CListCtrl		m_lcProgramsPorts;
	CButton			m_btnImportDefaultValues;
	CButton			m_btnAddProgram;
	CButton			m_btnAddPort;
	CButton			m_btnDeleteSelectedItems;
	CButton_Bool	m_btnInstallFirewall;

	afx_msg void OnBnClickedButtonAddProgram();
	afx_msg void OnBnClickedButtonAddPort();
	afx_msg void OnBnClickedButtonDeleteSelectedItems();
	afx_msg void OnBnClickedButtonInstallFirewall();
	afx_msg void OnBnClickedButtonImportDefaultValues();

	virtual void	UpdateControls();
	virtual bool	RunInit();
	virtual int		CheckInstallComplete();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
