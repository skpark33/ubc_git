#include "StdAfx.h"
#include "CenterInfo.h"
#include "resource.h"

#include "ci/libDebug/ciDebug.h"
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchUtil.h"

ciSET_DEBUG(10, "CCenterInfo");


CCenterInfo::CCenterInfo(void)
{
	m_mapKeyValues.SetAt(KEY_UBC_CURRENT_VERSION, "");
	m_mapKeyValues.SetAt(KEY_UBC_CENTER_IP, "");
	m_mapKeyValues.SetAt(KEY_UBC_CENTER_HTTP_PORT, "");
	m_mapKeyValues.SetAt(KEY_UBC_CENTER_FTP_IP, "");
	m_mapKeyValues.SetAt(KEY_UBC_CENTER_FTP_PORT, "");
	m_mapKeyValues.SetAt(KEY_UBC_CENTER_FTP_ID, "");
	m_mapKeyValues.SetAt(KEY_UBC_CENTER_FTP_PWD, "");
}

CCenterInfo::~CCenterInfo(void)
{
}

int CCenterInfo::get(LPCSTR key, CString& value)
{
	if( m_mapKeyValues.Lookup(key, value) )
	{
		return TRUE;
	}

	// invalid key
	value = "";

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);
	ciWARN((msg));

	return FALSE;
}

int CCenterInfo::set(LPCSTR key, LPCSTR value)
{
	CString tmp_val="";
	if( !m_mapKeyValues.Lookup(key, tmp_val) )
	{
		// invalid key
		CString msg;
		msg.Format("Invalid Key name (%s)", key);
		setError(-1, msg);
		ciWARN((msg));
		return FALSE;
	}

	m_mapKeyValues.SetAt(key, value);
	return TRUE;
}

CString CCenterInfo::get(LPCSTR key)
{
	CString value="";
	if( m_mapKeyValues.Lookup(key, value) )
	{
		return value;
	}

	// invalid key
	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);
	ciWARN((msg));
	return "";
}

int CCenterInfo::test()
{
	ciDEBUG(1,("test()"));

	clearError();

	string current_version	= "";
	string center_ip		= "";
	string center_http_port	= "";
	string center_ftp_ip	= "";
	string center_ftp_port	= "";
	string center_ftp_id	= "";
	string center_ftp_pwd	= "";

	ubcIni aIni("UBCConnect", _COP_CD("C:\\Project\\ubc\\config"), "data");
	if( !scratchUtil::getInstance()->getVersion(current_version, true) )
	{
		setError(-1, "File not found release.txt");
		return FALSE;
	}

	aIni.get("UBCCENTER", "IP",			center_ip		);
	aIni.get("UBCCENTER", "HTTP_PORT",	center_http_port);
	aIni.get("UBCCENTER", "FTP_IP",		center_ftp_ip	);
	aIni.get("UBCCENTER", "FTPPORT",	center_ftp_port	);
	aIni.get("UBCCENTER", "FTPID",		center_ftp_id	);
	aIni.get("UBCCENTER", "FTPPASSWD",	center_ftp_pwd	);

	set(KEY_UBC_CURRENT_VERSION,	current_version.c_str()	);
	set(KEY_UBC_CENTER_IP,			center_ip.c_str()		);
	set(KEY_UBC_CENTER_HTTP_PORT,	center_http_port.c_str());
	set(KEY_UBC_CENTER_FTP_IP,		center_ftp_ip.c_str()	);
	set(KEY_UBC_CENTER_FTP_PORT,	center_ftp_port.c_str()	);
	set(KEY_UBC_CENTER_FTP_ID,		center_ftp_id.c_str()	);
	set(KEY_UBC_CENTER_FTP_PWD,		center_ftp_pwd.c_str()	);

	ciDEBUG(1,("test() end"));

	return TRUE;
}

int	CCenterInfo::install()
{
	ciDEBUG(1,("install()"));

	clearError();

	// set values
	CString center_ip		= "",
			center_http_port= "",
			center_ftp_ip	= "",
			center_ftp_port	= "",
			center_ftp_id	= "",
			center_ftp_pwd	= "";

	get(KEY_UBC_CENTER_IP,			center_ip		);
	get(KEY_UBC_CENTER_HTTP_PORT,	center_http_port);
	get(KEY_UBC_CENTER_FTP_IP,		center_ftp_ip	);
	get(KEY_UBC_CENTER_FTP_PORT,	center_ftp_port	);
	get(KEY_UBC_CENTER_FTP_ID,		center_ftp_id	);
	get(KEY_UBC_CENTER_FTP_PWD,		center_ftp_pwd	);
	ciDEBUG(1, ("%s=%s", KEY_UBC_CENTER_IP,			center_ip));
	ciDEBUG(1, ("%s=%s", KEY_UBC_CENTER_HTTP_PORT,	center_http_port));
	ciDEBUG(1, ("%s=%s", KEY_UBC_CENTER_FTP_IP,		center_ftp_ip));
	ciDEBUG(1, ("%s=%s", KEY_UBC_CENTER_FTP_PORT,	center_ftp_port));
	ciDEBUG(1, ("%s=%s", KEY_UBC_CENTER_FTP_ID,		center_ftp_id));
	ciDEBUG(1, ("%s=%s", KEY_UBC_CENTER_FTP_PWD,	center_ftp_pwd));

	ubcIni aIni("UBCConnect", _COP_CD("C:\\Project\\ubc\\config"), "data");

	aIni.set("UBCCENTER", "IP",			center_ip		);
	aIni.set("UBCCENTER", "HTTP_PORT",	center_http_port);
	aIni.set("UBCCENTER", "FTP_IP",		center_ftp_ip	);
	aIni.set("UBCCENTER", "FTPPORT",	center_ftp_port	);
	aIni.set("UBCCENTER", "FTPID",		center_ftp_id	);
	aIni.set("UBCCENTER", "FTPPASSWD",	center_ftp_pwd	);

	// re-check values ( is values changed really? )
	if( !test() ) // get changed values
	{
		setError(-1, "Fail to get changing values");
		return FALSE;
	}

	CString new_center_ip		= "",
			new_center_http_port= "",
			new_center_ftp_ip	= "",
			new_center_ftp_port	= "",
			new_center_ftp_id	= "",
			new_center_ftp_pwd	= "";

	get(KEY_UBC_CENTER_IP,			new_center_ip		);
	get(KEY_UBC_CENTER_HTTP_PORT,	new_center_http_port);
	get(KEY_UBC_CENTER_FTP_IP,		new_center_ftp_ip	);
	get(KEY_UBC_CENTER_FTP_PORT,	new_center_ftp_port	);
	get(KEY_UBC_CENTER_FTP_ID,		new_center_ftp_id	);
	get(KEY_UBC_CENTER_FTP_PWD,		new_center_ftp_pwd	);
	ciDEBUG(1, ("NewValue %s=%s", KEY_UBC_CENTER_IP,		new_center_ip));
	ciDEBUG(1, ("NewValue %s=%s", KEY_UBC_CENTER_HTTP_PORT,	new_center_http_port));
	ciDEBUG(1, ("NewValue %s=%s", KEY_UBC_CENTER_FTP_IP,	new_center_ftp_ip));
	ciDEBUG(1, ("NewValue %s=%s", KEY_UBC_CENTER_FTP_PORT,	new_center_ftp_port));
	ciDEBUG(1, ("NewValue %s=%s", KEY_UBC_CENTER_FTP_ID,	new_center_ftp_id));
	ciDEBUG(1, ("NewValue %s=%s", KEY_UBC_CENTER_FTP_PWD,	new_center_ftp_pwd));

	if( center_ip != new_center_ip )
	{
		// not change ubc_center_ip
		_errString += "Fail to apply UBC Center IP.\r\n\r\n";

		set(KEY_UBC_CENTER_IP, center_ip);
	}
	if( center_http_port != new_center_http_port )
	{
		// not change ubc_center_http_port
		_errString += "Fail to apply UBC Center HTTP Port.\r\n\r\n";

		set(KEY_UBC_CENTER_HTTP_PORT, center_http_port);
	}
	if( center_ftp_ip != new_center_ftp_ip )
	{
		// not change ubc_center_ftp_ip
		_errString += "Fail to apply UBC Center FTP IP.\r\n\r\n";

		set(KEY_UBC_CENTER_FTP_IP, center_ftp_ip);
	}
	if( center_ftp_port != new_center_ftp_port )
	{
		// not change ubc_center_ftp_port
		_errString += "Fail to apply UBC Center FTP Port.\r\n\r\n";

		set(KEY_UBC_CENTER_FTP_PORT, center_ftp_port);
	}
	if( center_ftp_id != new_center_ftp_id )
	{
		// not change ubc_center_ftp_id
		_errString += "Fail to apply UBC Center FTP ID.\r\n\r\n";

		set(KEY_UBC_CENTER_FTP_ID, center_ftp_id);
	}
	if( center_ftp_pwd != new_center_ftp_pwd )
	{
		// not change ubc_center_ftp_pwd
		_errString += "Fail to apply UBC Center FTP Password.\r\n\r\n";

		set(KEY_UBC_CENTER_FTP_PWD,	center_ftp_pwd);
	}

	// exist error
	if( _errString.length() > 0 )
	{
		_errCode = -1;
		ciERROR(("ErrorMsg=%s", _errString.c_str()));
		return FALSE;
	}

	ciDEBUG(1,("install() end"));
	return TRUE;
}
