#pragma once

using namespace std;

#include "../Installable/installable.h"

#define		KEY_UBC_CURRENT_VERSION		("UBCCurrentVersion")
#define		KEY_UBC_CENTER_IP			("UBCCenterIP")
#define		KEY_UBC_CENTER_HTTP_PORT	("UBCCenterHTTPPort")
#define		KEY_UBC_CENTER_FTP_IP		("UBCCenterFTPIP")
#define		KEY_UBC_CENTER_FTP_PORT		("UBCCenterFTPPort")
#define		KEY_UBC_CENTER_FTP_ID		("UBCCenterFTPID")
#define		KEY_UBC_CENTER_FTP_PWD		("UBCCenterFTPPwd")

class CCenterInfo : public installable
{
public:
	CCenterInfo(void);
	~CCenterInfo(void);

	virtual int prerequisite() { return 0; };
	virtual int test();
	virtual int	install();

	virtual int get(LPCSTR key, CString& value);
	virtual int set(LPCSTR key, LPCSTR value);
	virtual CString get(LPCSTR key);

protected:
	CMapStringToString		m_mapKeyValues;

};
