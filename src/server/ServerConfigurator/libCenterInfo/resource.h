//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by libCenterInfo.rc
//
#define IDS_ERR_NOT_APPLY_UBC_CENTER_IP 4000
#define IDS_ERR_NOT_APPLY_UBC_CENTER_HTTP_PORT 4001
#define IDS_ERR_NOT_APPLY_UBC_CENTER_FTP_IP 4002
#define IDS_ERR_NOT_APPLY_UBC_CENTER_FTP_PORT 4003
#define IDS_ERR_NOT_APPLY_UBC_CENTER_FTP_ID 4004
#define IDS_ERR_NOT_APPLY_UBC_CENTER_FTP_PWD 4005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         3000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
