// ServerConfiguratorDoc.h : interface of the CServerConfiguratorDoc class
//


#pragma once

#include "SubDlg.h"

class CSettingsDlg;
class CSummaryDlg;

typedef struct {
	CString		strTitle;
	CSubDlg*	pDialog;
	UINT		nIDTemplate;
} SUBDLG_ITEM;

typedef CArray<SUBDLG_ITEM, SUBDLG_ITEM&>	SUBDLG_ITEM_LIST;


class CServerConfiguratorDoc : public CDocument
{
protected: // create from serialization only
	CServerConfiguratorDoc();
	DECLARE_DYNCREATE(CServerConfiguratorDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CServerConfiguratorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	bool		m_bSubDLgAllInit;
	SUBDLG_ITEM_LIST	m_SubDlgItemList;

public:
	void		SetSubDLgAllInit(bool bInit) { m_bSubDLgAllInit=bInit; };
	bool		IsSubDLgAllInit() { return m_bSubDLgAllInit; };

	void		AddDialog(LPCSTR lpszTitle, CSubDlg* pDialog, UINT nIDTemplate);

	int					GetSubDlgCount() { return m_SubDlgItemList.GetCount(); };
	CSubDlg*			GetSubDlg(int idx) { return m_SubDlgItemList.GetAt(idx).pDialog; };
	const SUBDLG_ITEM&	GetSubDlgItem(int idx) { return m_SubDlgItemList.GetAt(idx); };
};
