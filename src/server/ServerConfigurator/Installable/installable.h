#ifndef _installable_h_
#define _installable_h_

#include <string>
#include <list>

class installable {
public:
	installable() { _errCode = 0; }
	virtual ~installable() {}

	virtual int prerequisite() = 0;
	virtual int test() = 0;
	virtual int	install() = 0;

	virtual int get(const char* key, string& value)			{setError(-1,"NO Service"); return 0;}
	virtual int get(const char* key, int& value)			{setError(-1,"NO Service"); return 0;}
	virtual int get(const char* key, unsigned long& value)	{setError(-1,"NO Service"); return 0;}
	virtual int get(const char* key, list<string>& value)	{setError(-1,"NO Service"); return 0;}

	virtual int set(const char* key, const char* value)		{setError(-1,"NO Service"); return 0;}
	virtual int set(const char* key, int value)				{setError(-1,"NO Service"); return 0;}
	virtual int set(const char* key, unsigned long value)	{setError(-1,"NO Service"); return 0;}
	virtual int set(const char* key, list<string>& value)	{setError(-1,"NO Service"); return 0;}
	
	const char*	getErrorString()							{ return _errString.c_str(); }
	int	getErrorCode()										{ return _errCode; }
	void clearError()										{ _errCode=0; _errString=""; }
	void setError(int eCode, const char* eStr)				{ _errCode=eCode; _errString=eStr; }

protected:

	string	_errString;
	int		_errCode;

};
#endif