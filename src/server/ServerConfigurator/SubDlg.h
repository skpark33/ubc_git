// ServerConfigurator.h : main header file for the ServerConfigurator application
//
#pragma once


class CServerConfiguratorDoc;

////////////////////////////////////////////////////////////////////////////////////////////

class CValuePairControl
{
public:
	CValuePairControl() {};
	virtual ~CValuePairControl() {};

	virtual void Init(LPVOID pValue, LPVOID pLinkWnd) {};
	virtual void UpdateControl() {};
};


////////////////////////////////////////////////////////////////////////////////////////////
// CButton_Bool

class CButton_Bool : public CButton, CValuePairControl
{
	DECLARE_DYNAMIC(CButton_Bool)

public:
	CButton_Bool() { m_pInitValue = NULL; m_pValueChange = NULL; };
	virtual ~CButton_Bool() {};

protected:
	DECLARE_MESSAGE_MAP()

	bool*		m_pInitValue;
	bool*		m_pValueChange;

public:
	virtual void Init(LPVOID pValue, LPVOID pValueChange) {
														m_pInitValue = (bool*)pValue;
														m_pValueChange = (bool*)pValueChange;
													};

	virtual void UpdateControl();
};


////////////////////////////////////////////////////////////////////////////////////////////
// CButton_Check

class CButton_Check : public CButton, CValuePairControl
{
	DECLARE_DYNAMIC(CButton_Check)

public:
	CButton_Check() { m_pInitValue = NULL; m_pLinkButton = NULL; };
	virtual ~CButton_Check() {};

protected:
	DECLARE_MESSAGE_MAP()

	bool*		m_pInitValue;
	CButton*	m_pLinkButton;

public:
	virtual void Init(LPVOID pValue, LPVOID pLinkWnd) {
														m_pInitValue = (bool*)pValue;
														m_pLinkButton = (CButton*)pLinkWnd;
													};

	virtual void UpdateControl();
};

////////////////////////////////////////////////////////////////////////////////////////////
// CButton_Radio

class CButton_Radio : public CButton, CValuePairControl
{
	DECLARE_DYNAMIC(CButton_Radio)

public:
	CButton_Radio() {
		m_pInitValue = NULL; 
		for(int i=0; i<10; i++) m_pLinkButton[i] = NULL;
	};
	virtual ~CButton_Radio() {};

protected:
	DECLARE_MESSAGE_MAP()

	int*		m_pInitValue;
	CButton*	m_pLinkButton[10];

public:

	virtual void Init(LPVOID pValue, LPVOID pLinkWnd0, LPVOID pLinkWnd1)
													{
														m_pInitValue = (int*)pValue;
														m_pLinkButton[0] = (CButton*)pLinkWnd0;
														m_pLinkButton[1] = (CButton*)pLinkWnd1;
													};

	virtual void Init(LPVOID pValue, LPVOID pLinkWnd0, LPVOID pLinkWnd1, LPVOID pLinkWnd2)
													{
														m_pInitValue = (int*)pValue;
														m_pLinkButton[0] = (CButton*)pLinkWnd0;
														m_pLinkButton[1] = (CButton*)pLinkWnd1;
														m_pLinkButton[2] = (CButton*)pLinkWnd2;
													};

	virtual void Init(LPVOID pValue, LPVOID pLinkWnd0, LPVOID pLinkWnd1, LPVOID pLinkWnd2, LPVOID pLinkWnd3)
													{
														m_pInitValue = (int*)pValue;
														m_pLinkButton[0] = (CButton*)pLinkWnd0;
														m_pLinkButton[1] = (CButton*)pLinkWnd1;
														m_pLinkButton[2] = (CButton*)pLinkWnd2;
														m_pLinkButton[3] = (CButton*)pLinkWnd3;
													};
	virtual void UpdateControl();
};

////////////////////////////////////////////////////////////////////////////////////////////
// CButton_String

class CButton_String : public CButton, CValuePairControl
{
	DECLARE_DYNAMIC(CButton_String)

public:
	CButton_String() {};
	virtual ~CButton_String() {};

protected:
	DECLARE_MESSAGE_MAP()

	CArray<CString*, CString*>	m_arrInitValue;
	CArray<CEdit*, CEdit*>		m_arrLinkValue;
	CUIntArray					m_arrAllowEmpty;

public:
	virtual void Init(LPVOID pValue, LPVOID pLinkWnd) {
												m_arrInitValue.Add((CString*)pValue);
												m_arrLinkValue.Add((CEdit*)pLinkWnd);
												m_arrAllowEmpty.Add(0);
											};

	virtual void Init(LPVOID pValue, LPVOID pLinkWnd, bool bCheckEmpty) {
												Init(pValue, pLinkWnd);
												m_arrAllowEmpty.SetAt(m_arrAllowEmpty.GetCount()-1, bCheckEmpty);
											};

	virtual void UpdateControl();
};

////////////////////////////////////////////////////////////////////////////////////////////

class CSubDlg : public CDialog
{
	DECLARE_DYNAMIC(CSubDlg)

public:
	CSubDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubDlg();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SERVERCONFIGURATOR_FORM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

protected:

	CServerConfiguratorDoc*		m_pDoc;

	bool	m_bInit;
	bool	m_bInstallComplete;

	CPtrMap		m_mapColorIgnoreWnd;
	bool		IsColorIgnoreWnd(HWND hWnd);	// 지정된 배경색 무시

	CUIntArray	m_arrAllControls;
	void		AddControl(HWND hwnd) { m_arrAllControls.Add((UINT)hwnd); };
	void		AddColorIgnoreWnd(HWND hWnd) { m_mapColorIgnoreWnd.SetAt(hWnd, hWnd); };

public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	void	EnableDialog(BOOL bEnable); // enable/disable all controls & dialog(itself)

	bool	IsInit() { return m_bInit; };
	bool	IsInstallComplete() { return m_bInstallComplete; };

	virtual void	UpdateControls() {}; // control(button) update

	virtual bool	RunInitThread(); // run InitThread
	virtual bool	RunInit() { return false; }; // run module-run-test
	virtual int		CheckInstallComplete() { return 0; }; // check to install complete (m_bInstallComplete)

	//int		GetDialogID() { return m_nDialogID; };
	//virtual void UpdateButtonControls() {};

protected:
	//virtual void OnOK();
	//virtual void OnCancel();

	//static int	m_nSubDialogCount;
	//int			m_nDialogID;

	CWinThread*		m_pInitThread;
	static UINT		InitThread(LPVOID pParam); // run RunInit

	static CSubDlg*	m_pSummaryDlg;
};
