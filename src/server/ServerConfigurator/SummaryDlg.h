#pragma once


#include "SubDlg.h"
#include "ReposControl2.h"


#define		SHORTCUT_BUTTON_COUNT		(10)
#define		SHORTCUT_BUTTON_ID			(10000)

class CServerConfiguratorDoc;


////////////////////////////////////////////////////////////////////////////////////////////////////////////
class CSummaryButton : public CButton
{
	DECLARE_DYNAMIC(CSummaryButton)

public:
	CSummaryButton() { m_nID = 0; };
	virtual ~CSummaryButton() {};

protected:
	DECLARE_MESSAGE_MAP()

	UINT	m_nID;

public:
	afx_msg void OnBnClicked();

	void	SetID(UINT nID) { m_nID = nID; };
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSummaryDlg 대화 상자입니다.

class CSummaryDlg : public CSubDlg//CDialog
{
	DECLARE_DYNAMIC(CSummaryDlg)

public:
	CSummaryDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSummaryDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SUMMARY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
 
	CFont			m_fontButton;
	CSummaryButton	m_btnShortCut[SHORTCUT_BUTTON_COUNT];
	CUIntArray		m_arDialogResult;

	CReposControl2	m_reposControl;

	CServerConfiguratorDoc*		m_pDoc;

public:

	CListCtrl	m_lcSummary;

	void		InitControl(CServerConfiguratorDoc* pDoc);
	virtual void	UpdateControls();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
