// SysAdminLoginDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"

#include "SysAdminLoginDlg.h"


#define		SYS_ADMIN_LOGIN_PASSWD		("-507263a")


// CSysAdminLoginDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSysAdminLoginDlg, CDialog)

CSysAdminLoginDlg::CSysAdminLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSysAdminLoginDlg::IDD, pParent)
{

}

CSysAdminLoginDlg::~CSysAdminLoginDlg()
{
}

void CSysAdminLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_ID, m_editID);
	DDX_Control(pDX, IDC_EDIT_PASSWD, m_editPasswd);
}


BEGIN_MESSAGE_MAP(CSysAdminLoginDlg, CDialog)
END_MESSAGE_MAP()


// CSysAdminLoginDlg 메시지 처리기입니다.

void CSysAdminLoginDlg::OnOK()
{
	CString str_id;
	m_editID.GetWindowText(str_id);

	CString str_passwd;
	m_editPasswd.GetWindowText(str_passwd);

	if( str_id.GetLength() > 0 || str_passwd != SYS_ADMIN_LOGIN_PASSWD )
	{
		::AfxMessageBox(IDS_SYSADMINLOGIN_FAIL_TO_LOGIN, MB_ICONSTOP);
		return;
	}

	CDialog::OnOK();
}
