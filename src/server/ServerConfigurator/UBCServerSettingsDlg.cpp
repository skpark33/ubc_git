// UBCServerSettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "UBCServerSettingsDlg.h"

#include "ErrorMessageDlg.h"


// CUBCServerSettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CUBCServerSettingsDlg, CSubDlg)

CUBCServerSettingsDlg::CUBCServerSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*CUBCServerSettingsDlg::IDD,*/ pParent)
{
	m_bValueChange = true;
}

CUBCServerSettingsDlg::~CUBCServerSettingsDlg()
{
}

void CUBCServerSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SERVER, m_lcServer);
	DDX_Control(pDX, IDC_EDIT_MAIN_SERVER_IP, m_editMainServer);
	DDX_Control(pDX, IDC_BUTTON_SET_SERVER, m_btnSetSeverSettings);
}


BEGIN_MESSAGE_MAP(CUBCServerSettingsDlg, CSubDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_SET_SERVER, &CUBCServerSettingsDlg::OnBnClickedButtonSetServer)
	ON_NOTIFY(NM_CLICK, IDC_LIST_SERVER, &CUBCServerSettingsDlg::OnNMClickServer)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_SERVER, &CUBCServerSettingsDlg::OnLvnKeydownServer)
	ON_EN_CHANGE(IDC_EDIT_MAIN_SERVER_IP, &CUBCServerSettingsDlg::OnEnChangeEditMainServer)
END_MESSAGE_MAP()


// CUBCServerSettingsDlg 메시지 처리기입니다.


BOOL CUBCServerSettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	AddControl( m_lcServer.GetSafeHwnd() );
	AddControl( m_editMainServer.GetSafeHwnd() );
	AddControl( m_btnSetSeverSettings.GetSafeHwnd() );

	m_btnSetSeverSettings.Init(&m_bInstallComplete, &m_bValueChange);

	//
	m_lcServer.SetExtendedStyle(m_lcServer.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT );
	m_lcServer.InsertColumn(0, ::LoadString(IDS_COLUMN_SERVER_ID), 0, 40);
	m_lcServer.InsertColumn(1, ::LoadString(IDS_COLUMN_SERVER_NAME), 0, 200);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CUBCServerSettingsDlg::OnTimer(UINT_PTR nIDEvent)
{
	if( nIDEvent == 1025 )
	{
		KillTimer(1025);

		CString cur_server_id = GetSelectServerID();
		if( m_strServerID == cur_server_id )
			m_bValueChange = false;
		else
			m_bValueChange = true;

		m_btnSetSeverSettings.UpdateControl();
	}

	CSubDlg::OnTimer(nIDEvent);
}

void CUBCServerSettingsDlg::OnBnClickedButtonSetServer()
{
	// get NAMESERVER_HOST
	// if MainServer, must current-ip=nameserver_host
	CString nameserver_host;
	m_editMainServer.GetWindowText(nameserver_host);
	nameserver_host.Trim(" \t");

	if( m_lcServer.GetCheck(0) )
	{
		CString current_ip = m_serverSettings.get(KEY_CURRENT_IP_ADDRESS);
		if(current_ip != nameserver_host)
		{
			int ret_val = ::AfxMessageBox(IDS_MSG_NAMESERVER_HOST_DIFF, MB_ICONWARNING | MB_YESNO);
			if(ret_val == IDNO) return;
		}
	}

	m_serverSettings.set(KEY_NAMESERVER_HOST, (LPCSTR)nameserver_host );

	CString server_id = GetSelectServerID();
	m_serverSettings.set(KEY_SERVER_ID, (LPCSTR)server_id );

	//
	BeginWaitCursor();

	int ret = m_serverSettings.install();

	EndWaitCursor();

	if( ret )
	{
		m_bInstallComplete = true;
		m_bValueChange = false;

		m_strServerID = server_id;
		m_strServerIP = nameserver_host;
	}
	else
	{
		// fail to install
		m_bInstallComplete = false;
	}

	CheckInstallComplete();
	UpdateControls();

	if( m_bInstallComplete )
	{
		::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
	}
	else
	{
		CErrorMessageDlg dlg("Install Error !!!", m_serverSettings.getErrorCode(), m_serverSettings.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
	}
}

CString CUBCServerSettingsDlg::GetSelectServerID()
{
	CString server_id;

	int count = m_lcServer.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if( m_lcServer.GetCheck(i) )
		{
			if( server_id.GetLength() > 0 ) server_id += "_";
			server_id += m_lcServer.GetItemText(i, 0);
		}
	}
	return server_id;
}

void CUBCServerSettingsDlg::OnNMClickServer(NMHDR *pNMHDR, LRESULT *pResult)
{
	SetTimer(1025, 100, NULL);
	*pResult = 0;
}

void CUBCServerSettingsDlg::OnLvnKeydownServer(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	SetTimer(1025, 100, NULL);
	*pResult = 0;
}

void CUBCServerSettingsDlg::OnEnChangeEditMainServer()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CSubDlg::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	// 연관된 CButton_String 컨트롤
	CString cur_server_ip;
	m_editMainServer.GetWindowText(cur_server_ip);
	if( m_strServerIP == cur_server_ip )
		m_bValueChange = false;
	else
		m_bValueChange = true;

	m_btnSetSeverSettings.UpdateControl();
}

bool CUBCServerSettingsDlg::RunInit()
{
	m_bInstallComplete = true;

	//
	if( !m_serverSettings.test() )
	{
		m_bInstallComplete = false;
	}

	//
	m_strServerID = m_serverSettings.get(KEY_SERVER_ID);
	m_strServerID.Trim(" \t");

	int all_slave_count = 0;
	if( m_strServerID.GetLength() > 0 )
	{
		int pos = 0;
		CString server_id = m_strServerID.Tokenize("_", pos);
		while( server_id != "" )
		{
			all_slave_count++;
			server_id = m_strServerID.Tokenize("_", pos);
		}
	}
	else
	{
		m_bInstallComplete = false;
	}

	//
	CString slave_id_list = m_strServerID;
	slave_id_list.Insert(0, "_");
	slave_id_list+= "_";

	//
	CStringArray server_list;
	m_serverSettings.GetAllSlaveIDList(server_list);
	int check_count = 0;
	for(int i=0; i<server_list.GetCount(); i++)
	{
		CString server_id = server_list.GetAt(i);

		m_lcServer.InsertItem(i, server_id);

		CString server_name_format;
		server_name_format.LoadString( (i==0) ? IDS_TEXT_MAIN_SERVER : IDS_TEXT_SUB_SERVER );

		CString server_name;
		server_name.Format(server_name_format, server_id);

		m_lcServer.SetItemText(i, 1, server_name);

		//
		server_id.Insert(0, "_");
		server_id += "_";

		if( slave_id_list.Find(server_id) >= 0 )
		{
			m_lcServer.SetCheck(i);
			check_count++;
		}
	}
	m_lcServer.SetColumnWidth(1, LVSCW_AUTOSIZE_USEHEADER);
	if( check_count != all_slave_count ) m_bInstallComplete = false;

	//
	m_strServerIP = m_serverSettings.get(KEY_NAMESERVER_HOST);
	m_editMainServer.SetWindowText(m_strServerIP);
	if( m_strServerIP.GetLength() == 0 ) m_bInstallComplete = false;

	return m_bInstallComplete;
}

void CUBCServerSettingsDlg::UpdateControls()
{
	m_btnSetSeverSettings.UpdateControl();
}

int CUBCServerSettingsDlg::CheckInstallComplete()
{
	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}
