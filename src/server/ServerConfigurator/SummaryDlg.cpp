// SummaryDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "SummaryDlg.h"

#include "ServerConfiguratorDoc.h"


// CSummaryDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSummaryDlg, CSubDlg)

CSummaryDlg::CSummaryDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*CSummaryDlg::IDD,*/ pParent)
{
	m_fontButton.CreatePointFont(8*10, "Tahoma");

	m_pDoc = NULL;
	m_pSummaryDlg = this;
}

CSummaryDlg::~CSummaryDlg()
{
}

void CSummaryDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SUMMARY, m_lcSummary);
}


BEGIN_MESSAGE_MAP(CSummaryDlg, CSubDlg)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CSummaryDlg 메시지 처리기입니다.

BOOL CSummaryDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	m_reposControl.SetParent(this);
	m_reposControl.AddControl((CWnd*)&m_lcSummary, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSummaryDlg::InitControl(CServerConfiguratorDoc* pDoc)
{
	m_bInit = true;
	m_bInstallComplete = true;

	CImageList gapImage;
	gapImage.Create(1,24,ILC_COLORDDB,1,0); //2번째 파라미터로 높이조절.....
	m_lcSummary.SetImageList(&gapImage,LVSIL_SMALL);

	m_lcSummary.SetExtendedStyle(m_lcSummary.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT );

	m_lcSummary.InsertColumn(0, ::LoadString(IDS_COLUMN_ITEM), 0, 200 );
	m_lcSummary.InsertColumn(1, ::LoadString(IDS_COLUMN_STATUS), 0, 100 );
	m_lcSummary.InsertColumn(2, "", 0, 100 );

	m_pDoc = pDoc;
	int count = m_pDoc->GetSubDlgCount();
	for(int i=1; i<count; i++)
	{
		const SUBDLG_ITEM& item = m_pDoc->GetSubDlgItem(i);

		int id = i-1;

		m_lcSummary.InsertItem(id, item.strTitle);
		m_lcSummary.SetItemText(id, 1, ::LoadString(IDS_STATUS_CHECKING));

		CRect rect;
		m_lcSummary.GetSubItemRect(id, 2, LVIR_LABEL, rect);
		rect.DeflateRect(1,1);

		//
		m_btnShortCut[id].Create(::LoadString(IDS_MSG_MOVE_THIS_TAB), 
								WS_CHILD | /*WS_VISIBLE |*/ BS_PUSHBUTTON | BS_MULTILINE, 
								rect, 
								&m_lcSummary, 
								SHORTCUT_BUTTON_ID+id );
		m_btnShortCut[id].SetFont(&m_fontButton);
		m_btnShortCut[id].SetID(i);
		m_btnShortCut[id].EnableWindow(FALSE);
	}
}

void CSummaryDlg::UpdateControls()
{
	bool all_init = true;

	int count = m_pDoc->GetSubDlgCount();
	for(int i=1; i<count; i++)
	{
		CSubDlg* subdlg = m_pDoc->GetSubDlg(i);

		int id = i-1;

		if( subdlg->IsInit() )
		{
			if( subdlg->IsInstallComplete() )
			{
				m_lcSummary.SetItemText(id, 1, ::LoadString(IDS_STATUS_COMPLETE));
				m_btnShortCut[id].ShowWindow(SW_HIDE);
			}
			else
			{
				m_lcSummary.SetItemText(id, 1, ::LoadString(IDS_STATUS_INCOMPLETE));
				m_btnShortCut[id].ShowWindow(SW_SHOW);
			}
		}
		else
		{
			all_init = false;

			m_lcSummary.SetItemText(id, 1, ::LoadString(IDS_STATUS_CHECKING));
			m_btnShortCut[id].ShowWindow(SW_HIDE);
		}
	}

	if( all_init )
	{
		for(int i=1; i<count; i++)
		{
			m_btnShortCut[i-1].EnableWindow(TRUE);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
// CButtonEx

IMPLEMENT_DYNAMIC(CSummaryButton, CButton)

BEGIN_MESSAGE_MAP(CSummaryButton, CButton)
	ON_CONTROL_REFLECT(BN_CLICKED, OnBnClicked)
END_MESSAGE_MAP()

// CButtonEx 메시지 처리기입니다.

void CSummaryButton::OnBnClicked()
{
	::AfxGetMainWnd()->PostMessage(SHORTCUT_BUTTON_ID, m_nID);
}

void CSummaryDlg::OnSize(UINT nType, int cx, int cy)
{
	CSubDlg::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )
	{
		m_reposControl.MoveControl(TRUE);
	}
}
