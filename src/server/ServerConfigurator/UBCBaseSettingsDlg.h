#pragma once

#include "SubDlg.h"

#include "libCenterInfo/CenterInfo.h"
#include "libGlobalOption/GlobalOption.h"


// CUBCBaseSettingsDlg 대화 상자입니다.

class CUBCBaseSettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(CUBCBaseSettingsDlg)

public:
	CUBCBaseSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CUBCBaseSettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBC_BASE_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CCenterInfo		m_ubcCenterInfo;
	CGlobalOption	m_GlobalOption;

	bool	m_bUBCCenterInfo;
	int 	m_nGlobalOption;

	CString	m_strUBCCenterIP;			// required
	CString	m_strUBCCenterHTTPPort;
	CString	m_strUBCCenterFTPIP;
	CString	m_strUBCCenterFTPPort;
	CString	m_strUBCCenterFTPID;		// required
	CString	m_strUBCCenterFTPPwd;		// required

public:
	CEdit	m_editCurrentUBCVersion;
	CEdit	m_editUBCCenterIP;
	CEdit	m_editUBCCenterHTTPPort;
	CEdit	m_editUBCCenterFTPIP;
	CEdit	m_editUBCCenterFTPPort;
	CEdit	m_editUBCCenterFTPID;
	CEdit	m_editUBCCenterFTPPwd;
	CButton_String	m_btnSetUBCCenterInfo;

	CButton	m_btnDisuseGlobal;
	CButton	m_btnUseGlobal;
	CButton_Radio	m_btnSetGlobal;

	afx_msg void OnBnClickedButtonSetUbcCenterInfo();
	afx_msg void OnBnClickedRadioGlobal();
	afx_msg void OnBnClickedButtonSetGlobal();
	afx_msg void OnEnChangeEditUbcCenter();

	virtual void	UpdateControls();
	virtual bool	RunInit();
	virtual int		CheckInstallComplete();
};
