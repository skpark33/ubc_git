#include "StdAfx.h"
#include "InstallFireWall.h"
#include "common/libScratch/scratchUtil.h"
#include <ci/libDebug/ciDebug.h>

#define BUF_SIZE		(1024*100)

ciSET_DEBUG(10, "libInstallFireWall");

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기본생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallFireWall::CInstallFireWall()
{
	ReadFireWallList();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallFireWall::~CInstallFireWall()
{
	m_listProgram.RemoveAll();
	m_listPort.RemoveAll();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (CStringArray&) listProgram : (in) 방화벽 예외 프로그램 리스트
/// @param (CStringArray&) listPort : (in) 방화벽 예외 포트 리스트
/////////////////////////////////////////////////////////////////////////////////
CInstallFireWall::CInstallFireWall(CStringArray& listProgram, CStringArray& listPort)
{
	set(listProgram, listPort);
}
	

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기존의 방화벽 예외 리스트가 정상적으로 등록되어 있는지 확인한다. \n
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallFireWall::test()
{
	ciDEBUG(1, ("begin firewall test"));
	clearError();

	if( m_listProgram.GetCount() == 0 && m_listPort.GetCount() == 0 )
	{
		ciDEBUG(1, ("Firewall program and port list count is zero"));
		setError(ERR_TEST_FAIL, "Firewall program and port list count is zero");
		return FALSE;
	}//if

	//방화벽 예외 프로그램들이 등록되어 있는지 확인한다.
	scratchUtil* pUtil = scratchUtil::getInstance();
	CString strItem;
	CStringArray listNotAdded;
	bool bHasNotAdded = false;
	for( int i=0; i<m_listProgram.GetCount(); i++ )
	{
		strItem = m_listProgram.GetAt(i);
		if( !pUtil->IsAppExceptionAdded((LPSTR)(LPCSTR)strItem) )
		{
			//추가되지 않은 항목들은 에러 로그로 기록
			ciDEBUG(1, ("Not added : %s", strItem));
			bHasNotAdded = true;
			listNotAdded.Add(strItem);
		}//if
	}//for


	//방화벽 예외 포트들이 등록되어 있는지 확인한다.
	for( int i=0; i<m_listPort.GetCount(); i++ )
	{
		//TCP와 UDP포트 
		strItem = m_listPort.GetAt(i);
		if( !pUtil->IsPortAdded(atol(strItem)) )
		{
			//추가되지 않은 항목들은 에러 로그로 기록
			ciDEBUG(1, ("Not added : %s", strItem));
			bHasNotAdded = true;
			listNotAdded.Add(strItem);
		}//if
	}//for

	//추가되지 않은 항목이 있다면...
	if( bHasNotAdded )
	{
		CString strError, strItem;
		strError = "Not added item exist : ";
		for( int i = 0; i<listNotAdded.GetCount(); i++ )
		{
			strItem = listNotAdded.GetAt(i);
			strError += strItem;
			strError += " | ";
		}//if

		ciDEBUG(1, ("%s", strError));
		setError(ERR_UNINSTALLED, (LPSTR)(LPCSTR)strError);
		return FALSE;
	}//if

	ciDEBUG(1, ("All Firewall items installed"));
	ciDEBUG(1, ("end Firewall test"));
	return TRUE;
}

ciULong _createProcess(const char *exe, const char* dir, BOOL minFlag)
{
	ciDEBUG2(5, ("_createProcess(%s, %s)", exe, dir) );

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	::ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESHOWWINDOW;
	if (minFlag)	si.wShowWindow = SW_MINIMIZE;
	else			si.wShowWindow = SW_NORMAL;
	//si.lpDesktop = "Winsta0\\Default";

	//if(!dir || !strlen(dir)){
	//	dir = UBC_HOME;
	//}

	BOOL bRun = CreateProcess(
		NULL, 
		(LPSTR)exe, 
		NULL, 
		NULL, 
		FALSE, 
		NORMAL_PRIORITY_CLASS, 
		NULL, 
		dir, 
		&si, 
		&pi
		);

	CloseHandle( pi.hThread );
	CloseHandle( pi.hProcess );

	if (bRun)
	{
		//myDEBUG("createProcess : pid(%d)\n", pi.dwProcessId);
		return pi.dwProcessId;
	}
	//myDEBUG("createProcess : fail to CreateProcess\n");
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 방화벽 예외 리스트를 등록한다. \n
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallFireWall::install()
{
	ciDEBUG(1, ("begin firewall install"));
	clearError();

	if( m_listProgram.GetCount() == 0 && m_listPort.GetCount() == 0 )
	{
		ciDEBUG(1, ("Firewall program and port list count is zero"));
		setError(ERR_FAIL_INSTALL, "Firewall program and port list count is zero");
		ciDEBUG(1, ("end firewall install"));
		return FALSE;
	}//if

	// ini 파일에 저장
	CString str_prog_list;
	for( int i=0; i<m_listProgram.GetCount(); i++ )
	{
		if(str_prog_list.GetLength() > 0) str_prog_list += "|";
		str_prog_list += m_listProgram.GetAt(i);
	}
	WritePrivateProfileString("FirewallProgramList", "List", (LPCSTR)str_prog_list, GetINIPath());

	CString str_port_list;
	for( int i=0; i<m_listPort.GetCount(); i++ )
	{
		if(str_port_list.GetLength() > 0) str_port_list += "|";
		str_port_list += m_listPort.GetAt(i);
	}
	WritePrivateProfileString("FirewallPortList", "List", (LPCSTR)str_port_list, GetINIPath());

	//방화벽 예외 프로그램들을 등록한다.
	scratchUtil* pUtil = scratchUtil::getInstance();
	CString strItem;
	CStringArray listNotAdded;
	bool bHasNotAdded = false;
	for( int i=0; i<m_listProgram.GetCount(); i++ )
	{
		strItem = m_listProgram.GetAt(i);
		if( !pUtil->AddToExceptionList((LPSTR)(LPCSTR)strItem) )
		{
			//추가되지 않은 항목들은 에러 로그로 기록
			ciDEBUG(1, ("Add fail : %s", strItem));
			listNotAdded.Add(strItem);
		}//if

		char drv[MAX_PATH]={0}, dir[MAX_PATH]={0}, fn[MAX_PATH]={0}, ext[MAX_PATH]={0};
		::_splitpath(strItem, drv, dir, fn, ext);

		CString str_cmd;
		str_cmd.Format("netsh.exe advfirewall firewall set rule name=\"%s%s\" new profile=\"any\"", fn, ext);
		_createProcess(str_cmd, "C:\\Windows\\System32", TRUE);
	}//for


	//방화벽 예외 포트들을 등록한다.
	for( int i=0; i<m_listPort.GetCount(); i++ )
	{
		//TCP 와 UDP 포트 
		strItem = m_listPort.GetAt(i);
		if( !pUtil->AddPort(atol(strItem)) )
		{
			//추가되지 않은 항목들은 에러 로그로 기록
			ciDEBUG(1, ("Add fail : %s", strItem));
			strItem.Insert(0, "Port : ");
			listNotAdded.Add(strItem);
		}//if
	}//for

	//추가되지 않은 항목이 있다면...
	if( listNotAdded.GetCount() > 0 )
	{
		CString strError, strItem;
		strError = "Fail to add item\r\n\r\n";
		for( int i=0; i<listNotAdded.GetCount(); i++ )
		{
			strItem = listNotAdded.GetAt(i);
			strError += strItem;
			strError += "\r\n";
		}//if

		ciDEBUG(1, ("%s", strError));
		setError(ERR_FAIL_INSTALL, (LPSTR)(LPCSTR)strError);
		return FALSE;
	}//if

	CString str_cmd;
	str_cmd = "netsh.exe advfirewall firewall set rule name=\"ubc_tcp_port\" new profile=\"any\"";
	_createProcess(str_cmd, "C:\\Windows\\System32", TRUE);
	str_cmd = "netsh.exe advfirewall firewall set rule name=\"ubc_udp_port\" new profile=\"any\"";
	_createProcess(str_cmd, "C:\\Windows\\System32", TRUE);

	ciDEBUG(1, ("Succes firewall install"));
	ciDEBUG(1, ("end firewall install"));
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 리스트를 방화벽 예외 리스트에서 삭제한다. \n
/// @param (CStringArray&) listProgram : (in) 방화벽 예외 리스트에서 삭제하려는 프로그램 리스트
/// @param (CStringArray&) listPort : (in) 방화벽 예외 리스트에서 삭제하려는 포트 리스트
/// @return <형: int> \n
///			<FALSE: 실패> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallFireWall::uninstall(CStringArray& listProgram, CStringArray& listPort)
{
	ciDEBUG(1, ("begin firewall uninstall"));
	clearError();

	if( listProgram.GetCount() == 0 && listPort.GetCount() == 0 )
	{
		ciDEBUG(1, ("Firewall uninstall program and port list count is zero"));
		ciDEBUG(1, ("end firewall uninstall"));
		return TRUE;
	}//if


	//방화벽 예외에서 프로그램들을 삭제한다.
	scratchUtil* pUtil = scratchUtil::getInstance();
	CString strItem;
	CStringArray listFail;
	bool bHasFail = false;
	for( int i=0; i<listProgram.GetCount(); i++ )
	{
		strItem = listProgram.GetAt(i);
		if( !pUtil->RemoveFromExceptionList((LPSTR)(LPCSTR)strItem) )
		{
			//삭제되지 않은 항목들은 에러 로그로 기록
			ciDEBUG(1, ("Remove fail : %s", strItem));
			bHasFail = true;
			listFail.Add(strItem);

			strItem += " fail !!!";
			::AfxMessageBox(strItem);
		}//if
	}//for

	//방화벽 예외에서 포트들을 삭제한다.
	for( int i=0; i<listPort.GetCount(); i++ )
	{
		//TCP 와 UDP 포트 
		strItem = listPort.GetAt(i);
		::AfxMessageBox(strItem);
		if( !pUtil->RemovePort(atol(strItem)) )
		{
			//삭제되지 않은 항목들은 에러 로그로 기록
			ciDEBUG(1, ("Remove fail : %s", strItem));
			bHasFail = true;
			listFail.Add(strItem);
		}//if
	}//for

	//삭제에 실패한 항목이 있다면...
	if( bHasFail )
	{
		CString strError, strItem;
		strError = "Fail to remove item : ";
		for( int i=0; i<listFail.GetCount(); i++ )
		{
			strItem = listFail.GetAt(i);
			strError += strItem;
			strError += " | ";
		}//if

		ciDEBUG(1, ("%s", strError));
		setError(ERR_FAIL_UNINSTALL, (LPSTR)(LPCSTR)strError);
		ciDEBUG(1, ("end firewall uninstall"));
		return FALSE;
	}//if

	ciDEBUG(1, ("Success firewall uninstall"));
	ciDEBUG(1, ("end firewall uninstall"));
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 방화벽 예외 리스트를 반환한다. \n
/// @param (CStringArray*) plistProgram : (out) 방화벽 예외 프로그램 리스트
/// @param (CStringArray*) plistPort : (out) 방화벽 예외 포트 리스트
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallFireWall::get(CStringArray* plistProgram, CStringArray* plistPort)
{
	if( plistProgram==NULL || plistPort==NULL )
	{
		ciDEBUG(1, ("NULL [out] parameter"));
		setError(ERR_ETC, "NULL [out] parameter");
		return FALSE;
	}//if

	plistProgram->Copy(m_listProgram);
	plistPort->Copy(m_listPort);

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 방화벽 예외 리스트를 설정한다. \n
/// @param (CStringArray&) listProgram : (in) 방화벽 예외 프로그램 리스트
/// @param (CStringArray&) listPort : (in) 방화벽 예외 포트 리스트
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallFireWall::set(CStringArray& listProgram, CStringArray& listPort)
{
	m_listProgram.RemoveAll();
	m_listPort.RemoveAll();
	m_listProgram.Copy(listProgram);
	m_listPort.Copy(listPort);

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ServerConfigurator.ini로 부터 방화벽 예외 리스트를 읽는다. \n
/////////////////////////////////////////////////////////////////////////////////
void CInstallFireWall::ReadFireWallList()
{
	clearError();

	////ServerConfigurator.ini의 경로
	//char cModule[MAX_PATH];
	//::ZeroMemory(cModule, MAX_PATH);
	//::GetModuleFileName(NULL, cModule, MAX_PATH);

	//char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	//_splitpath(cModule, cDrive, cPath, cFilename, cExt);

	//CString strPath;
	//strPath.Format("%s%s%s.ini", cDrive, cPath, cFilename);

	CFileStatus fs;
	if( CFile::GetStatus(GetINIPath(), fs) != TRUE )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to access file(%s)", GetINIPath());
		ciDEBUG(1, (szBuf));
		setError(ERR_ETC, szBuf);
		return;
	}//if

	//m_strConfigPath = fs.m_szFullName;

	//ServerConfigurator.ini로 부터 Program 리스트를 읽어온다.
	m_listProgram.RemoveAll();
	char cBuf[BUF_SIZE] = {0};
	CString strList, strTok;
	int nPos = 0;
	GetPrivateProfileString("FirewallProgramList", "List", "", cBuf, BUF_SIZE, GetINIPath());
	strList = cBuf;

	//'|'를 기준으로 분리
	strTok = strList.Tokenize("|", nPos);
	while( strTok != "" )
	{
		strTok.Trim();
		m_listProgram.Add(strTok);

		strTok = strList.Tokenize("|", nPos);		
	}//while


	//ServerConfigurator.ini로 부터 Port 리스트를 읽어온다.
	m_listPort.RemoveAll();
	nPos = 0;
	GetPrivateProfileString("FirewallPortList", "List", "", cBuf, BUF_SIZE, GetINIPath());
	strList = cBuf;
	
	//'|'를 기준으로 분리
	strTok = strList.Tokenize("|", nPos);
	while( strTok != "" )
	{
		strTok.Trim();
		m_listPort.Add(strTok);

		strTok = strList.Tokenize("|", nPos);		
	}//while
}

LPCSTR CInstallFireWall::GetINIPath()
{
	static CString str_path = "";

	if( str_path.GetLength() == 0 )
	{
		char cModule[MAX_PATH];
		::ZeroMemory(cModule, MAX_PATH);
		::GetModuleFileName(NULL, cModule, MAX_PATH);

		char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
		_splitpath(cModule, cDrive, cPath, cFilename, cExt);

		str_path.Format("%s%s%s.ini", cDrive, cPath, cFilename);
	}

	return (LPCSTR)str_path;
}
