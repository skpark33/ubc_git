/************************************************************************************/
/*! @file InstallFireWall.h
	@brief 윈도우즈 방화벽에 예외항목을 추가하는 클래스 선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2012/07/23\n
	▶ 참고사항:
		- libScratch의 WinFireWall 클래스를 사용함

  @warning ※ 사용하는 프로그램에서 libScratch를 링크하여야 함\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2012/07/23:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#pragma once
using namespace std;

#include "../Installable/installable.h"

#ifndef _ERR_CODDE
	#define _ERR_CODDE
	#define ERR_TEST_FAIL			-1			//테스트 과정 실패
	#define ERR_UNINSTALLED			-2			//테스트 결과 설치가 되어있지 않음
	#define ERR_FAIL_INSTALL		-3			//설치 실패
	#define ERR_FAIL_UNINSTALL		-4			//uninstall fail
	#define ERR_ETC					-10			//기타 실패
#endif


//! 윈도우즈 방화벽에 예외항목을 추가하는 클래스
/*!
*/
class CInstallFireWall : public installable
{
public:
	CInstallFireWall(void);														///<기본 생성자
	CInstallFireWall(CStringArray& listProgram, CStringArray& listPort);		///<생성자
	virtual ~CInstallFireWall(void);											///<소멸자

	virtual int prerequisite()	{ setError(-1,"NO Service"); return 0; };
	virtual int test();															///<기존의 방화벽 예외 리스트가 정상적으로 등록되어 있는지 확인한다.
	virtual int	install();														///<방화벽 예외 리스트를 등록한다.

	int			uninstall(CStringArray& listProgram, CStringArray& listPort);	///<주어진 리스트를 방화벽 예외 리스트에서 삭제한다.

	int			get(CStringArray* plistProgram, CStringArray* plistPort);		///<방화벽 예외 리스트를 반환한다.
	int			set(CStringArray& listProgram, CStringArray& listPort);			///<방화벽 예외 리스트를 설정한다.

private:
	void		ReadFireWallList(void);											///<ServerConfigurator.ini로 부터 방화벽 예외 리스트를 읽는다.

	CStringArray	m_listProgram;												///<방화벽 예외 프로그램 리스트(전체 경로)
	CStringArray	m_listPort;													///<방화벽 예외 포트 리스트
	//CString			m_strConfigPath;											///<ServerConfigurator.ini의 경로

protected:
	LPCSTR	GetINIPath();
};
