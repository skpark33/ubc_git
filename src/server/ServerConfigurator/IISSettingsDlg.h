#pragma once

#include "SubDlg.h"
#include "libIisSetting/IisSetting.h"
#include "afxwin.h"


// CIISSettingsDlg 대화 상자입니다.

class CIISSettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(CIISSettingsDlg)

public:
	CIISSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CIISSettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_IIS_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CIisSetting	m_HttpSetting;
	CIisSetting	m_UBCDefaultSetting;
	CIisSetting	m_UBCFtpSetting;
	CIisSetting	m_UBCWebSetting;

	bool	m_bHttpCompleted;
	bool	m_bUBCDefaultCompleted;
	bool	m_bUBCFtpCompleted;
	bool	m_bUBCWebCompleted;

public:
	CButton			m_chkUseHttp;
	CEdit			m_editHttpPort;
	CButton_Check	m_btnUseHttp;

	CButton			m_chkUseUBCDefault;
	CEdit			m_editUBCDefaultPort;
	CButton_Check	m_btnUseUBCDefault;

	CButton			m_chkUseUBCWeb;
	CEdit			m_editUBCWebPort;
	CButton_Check	m_btnUseUBCWeb;

	CButton			m_chkUseIISFTP;
	CEdit			m_editIISFTPPort;
	CEdit			m_editFTPID;
	CEdit			m_editFTPPassword;
	CEdit			m_editFTPPassiveLowPort;
	CEdit			m_editFTPPassiveHighPort;
	CButton_Check	m_btnUseIISFTP;

	afx_msg void OnBnClickedCheckUseHttp();
	afx_msg void OnBnClickedButtonUseHttp();

	afx_msg void OnBnClickedCheckUseUBCDefault();
	afx_msg void OnBnClickedButtonUseUBCDefault();

	afx_msg void OnBnClickedCheckUseUbcWeb();
	afx_msg void OnBnClickedButtonUseUbcWeb();

	afx_msg void OnBnClickedCheckUseIisFtp();
	afx_msg void OnBnClickedButtonUseIisFtp();

	virtual void	UpdateControls();
	virtual bool	RunInit();
	virtual int		CheckInstallComplete();
};
