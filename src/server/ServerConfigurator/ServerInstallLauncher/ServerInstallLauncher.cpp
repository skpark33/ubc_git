// ServerInstallLauncher.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "ServerInstallLauncher.h"
#include "ServerInstallLauncherDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CServerInstallLauncherApp

BEGIN_MESSAGE_MAP(CServerInstallLauncherApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CServerInstallLauncherApp 생성

CServerInstallLauncherApp::CServerInstallLauncherApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CServerInstallLauncherApp 개체입니다.

CServerInstallLauncherApp theApp;


// CServerInstallLauncherApp 초기화

BOOL CServerInstallLauncherApp::InitInstance()
{
	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("SQISoft"));

	CheckEnv();
	LaunchServerInstall();

/*
	CServerInstallLauncherDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
*/

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

#define		DEFAULT_ENV_COUNT		(12)
char* szDefaultEnv[ DEFAULT_ENV_COUNT*2 ] = {
	"PROJECT_HOME", 
	"C:\\Project\\ubc", 

	"TRDPARTYROOT", 
	"C:\\Project\\3rdparty",

	"ACE_ROOT", 
	"%TRDPARTYROOT%\\win32\\ACE_wrappers", 

	"CONFIGROOT", 
	"%PROJECT_HOME%\\config", 

	"COP_HOME", 
	"%PROJECT_HOME%\\cop", 

	"COP_VCVER", 
	"8", 

	"MY_IP", 
	"127.0.0.1", 

	"MYSQL_HOME", 
	"C:\\Program Files (x86)\\MySQL\\MySQL Server 5.1", 

	"NAMESERVER_HOST", 
	"127.0.0.1", 

	"PATH", 
	"%PATH%;C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE;C:\\Program Files\\Microsoft Visual Studio 8\\VC\\bin;C:\\Program Files (x86)\\Microsoft Visual Studio 8\\Common7\\IDE;C:\\Program Files (x86)\\Microsoft Visual Studio 8\\VC\\bin;%MYSQL_HOME%\\bin;%PROJECT_HOME%\\bin\\win32;%PROJECT_HOME%\\bin8;%PROJECT_HOME%\\util\\win32;%COP_HOME%\\bin8;%COP_HOME%\\util\\win32;%TRDPARTYROOT%\\win32\\ACE_wrappers\\bin;%TRDPARTYROOT%\\win32\\ACE_wrappers\\lib;%TRDPARTYROOT%\\win32\\bin;%TRDPARTYROOT%\\win32\\UnxUtils\\bin;%TRDPARTYROOT%\\win32\\UnxUtils\\usr\\local\\wbin", 

	"PROJECT_CODE", 
	"ubc", 

	"TAO_ROOT", 
	"%TRDPARTYROOT%\\win32\\ACE_wrappers\\TAO", 
};

void CServerInstallLauncherApp::CheckEnv()
{
	// get install-drive letter
	char str_replace[MAX_PATH+1] = {0};
	::GetModuleFileName(NULL, str_replace, MAX_PATH);
	if('a' <= str_replace[0] && str_replace[0] <= 'z')
		str_replace[0] = str_replace[0] - 'a' + 'A';
	str_replace[3] = NULL; // ex)"E:\"

	for(int i=0; i<DEFAULT_ENV_COUNT*2; i+=2)
	{
		char* variables = szDefaultEnv[i];
		CString values = szDefaultEnv[i+1];

		// change default-drive "C:\" => install-drive "E:\"
		if(str_replace[0] != 'C' && str_replace[0] != 'c' && stricmp(variables, "PATH") && stricmp(variables, "MYSQL_HOME"))
			values.Replace("C:\\", str_replace);

		// getenv
		if( getenv(variables) == NULL || stricmp(variables,"PATH")==0 )
		{
			// not exist => _putenv
			CString str;
			str.Format("%s=%s", variables, values);

			// expand %ENVVAR%
			char buf[10240] = {0};
			ExpandEnvironmentStrings(str, buf, 10240);

			// set env
			if( _putenv(buf) )
			{
				CString fmt;
				fmt.LoadString(IDS_CANT_APPLY_ENVIRONMENT_VARIABLES);

				CString msg;
				msg.Format(fmt, buf);

				::AfxMessageBox(msg);
			}
		}
	}
}

bool CServerInstallLauncherApp::LaunchServerInstall()
{
	TCHAR str[MAX_PATH] = {0};
	::GetModuleFileName(NULL, str, MAX_PATH);
	int length = strlen(str) - 1;
	while( (length > 0) && (str[length] != '\\') )
		str[length--] = 0;

	CString str_cmd = str;
	str_cmd += "ServerInstall.exe";

	PROCESS_INFORMATION pi = {0};
	STARTUPINFO         si = {0};
	si.cb = sizeof( si );

	if( !CreateProcess(NULL, (LPSTR)(LPCSTR)str_cmd, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi ) )
	{
		CString str;
		str.LoadString(IDS_CANT_EXECUTE_SERVER_INSTALL);
		::MessageBox(NULL, str, "ServerInstall Launcher", MB_ICONSTOP);
		return false;
	}

	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);

	return true;
}
