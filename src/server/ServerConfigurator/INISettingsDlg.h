#pragma once

#include "SubDlg.h"
#include "PropertyGrid/PropertyGrid.h"

#include "ReposControl2.h"


// CINISettingsDlg 대화 상자입니다.

class ITEM_DATA;

class CINISettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(CINISettingsDlg)

public:
	CINISettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CINISettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_INI_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CReposControl2	m_reposControl;

	bool	m_bValueChange;

	ITEM_DATA*	m_pCurrentItemData;

	bool	AddFile(LPCSTR lpszFullPath);
	bool	InitGrid(ITEM_DATA* data);
	bool	UpdateData();

public:
	CStatic			m_stcFileList;
	CListCtrl		m_lcINIFile;
	CButton			m_btnAddFile;
	CButton			m_btnRemoveFileFromList;

	CStatic			m_stcFile;
	CPropertyGrid	m_grid;
	CButton			m_btnAddSection;
	CButton			m_btnAddItem;
	CButton			m_btnDeleteSelectedItem;
	CButton_Bool	m_btnSaveSettings;

	afx_msg void OnBnClickedButtonAddFile();
	afx_msg void OnBnClickedButtonRemoveFileFromList();
	afx_msg void OnBnClickedButtonAddSection();
	afx_msg void OnBnClickedButtonAddItem();
	afx_msg void OnBnClickedButtonDeleteSelectedItem();
	afx_msg void OnBnClickedButtonSaveSettings();
	afx_msg void OnLvnItemchangedListIni(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnPGItemChanged(WPARAM wParam, LPARAM lParam);

	virtual void	UpdateControls();
	virtual bool	RunInit();
	virtual int		CheckInstallComplete();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};

class CProfileManager;
class ciXProperties;

class ITEM_DATA
{
public:
	ITEM_DATA(CProfileManager* ini, LPCSTR lpszFilename)
		: m_pINI(ini)
		, m_pProperties(NULL)
		, m_strFilename(lpszFilename)
		{ m_bChanged = false; };

	ITEM_DATA(ciXProperties* prop, LPCSTR lpszFilename)
		: m_pINI(NULL)
		, m_pProperties(prop)
		, m_strFilename(lpszFilename)
		{ m_bChanged = false; };

	virtual ~ITEM_DATA() {};

protected:
	CString				m_strFilename;
	CProfileManager*	m_pINI;
	ciXProperties*		m_pProperties;
	bool				m_bChanged;

public:
	LPCSTR				GetFilename() { return m_strFilename; };
	CProfileManager*	GetINI() { return m_pINI; };
	ciXProperties*		GetProperties() { return m_pProperties; };

	void				SetChange(bool bChange=true) { m_bChanged=bChange; };
	bool				IsChanged() { return m_bChanged; };
};
