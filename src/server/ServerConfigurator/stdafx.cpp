// stdafx.cpp : source file that includes just the standard includes
// ServerConfigurator.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#include "resource.h"


void GetIPAddress(CString strIP, BYTE ip[])
{
	for(int i=0; i<4; i++) ip[i] = 0;

	int pos = 0;
	CString str_ip = strIP.Tokenize(".", pos);
	for(int i=0; i<4 && str_ip!=""; i++)
	{
		ip[i] = atoi(str_ip);
		str_ip = strIP.Tokenize(".", pos);
	}
}

void GetIPAddress(BYTE ip[], CString& strIP)
{
	strIP.Format("%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
}
//
//void UpdateButtonControl(bool bFlag, CButton* pCheck, CButton* pButton)
//{
//	CString msg;
//
//	if(pCheck->GetCheck())
//	{
//		// install
//		msg.LoadString(bFlag ? IDS_MSG_REINSTALL : IDS_MSG_INSTALL);
//		pButton->EnableWindow(TRUE);
//	}
//	else
//	{
//		// not install
//		msg.LoadString(bFlag ? IDS_MSG_COMPLETED : IDS_MSG_INCOMPLETED);
//		pButton->EnableWindow(FALSE);
//	}
//	pButton->SetWindowText(msg);
//}

CString	LoadString(UINT nID)
{
	static CMapStringToString _map;

	CString str_id;
	str_id.Format("%d", nID);

	CString str_ret;
	if( _map.Lookup(str_id, str_ret) ) return str_ret;

	str_ret.LoadString(nID);
	_map.SetAt(str_id, str_ret);
	return str_ret;
}

CString ToString(int nVal)
{
	char buf[64]={0};
	sprintf(buf, "%d", nVal);
	return CString(buf);
}
