#pragma once

#include "SubDlg.h"
#include "libTrdPartyInstall/TrdPartyInstall.h"

// CTrdpartySettingsDlg 대화 상자입니다.

class CTrdpartySettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(CTrdpartySettingsDlg)

public:
	CTrdpartySettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTrdpartySettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TRDPARTY_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CTrdPartyInstall	m_TrdPartyInstall;

	bool	m_bInstallNetFramework4;
	bool	m_bInstallVC2005Express;
	bool	m_bInstallVC2010Redist_x86;
	bool	m_bInstallVC2010Redist_x64;
	bool	m_bInstallMySQLServer;
	bool	m_bInstallMySQLODBC_x86;
	bool	m_bInstallMySQLODBC_x64;

public:
	CButton			m_chkInstallVC2005Express;
	CButton_Check	m_btnInstallVC2005Express;

	CButton			m_chkInstallNetFramework4;
	CButton_Check	m_btnInstallNetFramework4;

	CButton			m_chkInstallVC2010Redist_x86;
	CButton_Check	m_btnInstallVC2010Redist_x86;

	CButton			m_chkInstallVC2010Redist_x64;
	CButton_Check	m_btnInstallVC2010Redist_x64;

	CButton			m_chkInstallMySQLServer;
	CButton_Check	m_btnInstallMySQLServer;

	CButton			m_chkInstallMySQLODBC_x86;
	CButton_Check	m_btnInstallMySQLODBC_x86;

	CButton			m_chkInstallMySQLODBC_x64;
	CButton_Check	m_btnInstallMySQLODBC_x64;

	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnBnClickedCheckInstallNetFramework4();
	afx_msg void OnBnClickedButtonInstallNetFramework4();
	afx_msg void OnBnClickedCheckInstallVc2005Express();
	afx_msg void OnBnClickedButtonInstallVc2005Express();
	afx_msg void OnBnClickedCheckInstallVC2010Redist_x86();
	afx_msg void OnBnClickedButtonInstallVC2010Redist_x86();
	afx_msg void OnBnClickedCheckInstallVC2010Redist_x64();
	afx_msg void OnBnClickedButtonInstallVC2010Redist_x64();
	afx_msg void OnBnClickedCheckInstallMySQLServer();
	afx_msg void OnBnClickedButtonInstallMySQLServer();
	afx_msg void OnBnClickedCheckInstallMySQLODBC_x86();
	afx_msg void OnBnClickedButtonInstallMySQLODBC_x86();
	afx_msg void OnBnClickedCheckInstallMySQLODBC_x64();
	afx_msg void OnBnClickedButtonInstallMySQLODBC_x64();

	virtual void	UpdateControls();
	virtual bool	RunInit();
	virtual int		CheckInstallComplete();
};
