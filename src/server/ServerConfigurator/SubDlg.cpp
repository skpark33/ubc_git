// ServerConfigurator.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "SubDlg.h"

#include "MainFrm.h"
#include "ServerConfiguratorDoc.h"
#include "ServerConfiguratorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSubDlg 대화 상자입니다.

//int CSubDlg::m_nSubDialogCount = 0;
CSubDlg* CSubDlg::m_pSummaryDlg = NULL;


IMPLEMENT_DYNAMIC(CSubDlg, CDialog)

CSubDlg::CSubDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CDialog(CSubDlg::IDD, pParent)
{
	m_pInitThread = NULL;
//	m_nDialogID = m_nSubDialogCount++;
	m_pDoc = pDoc;
	m_bInit = false;
	m_bInstallComplete = false;
}

CSubDlg::~CSubDlg()
{
}

void CSubDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSubDlg, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

// CSubDlg 메시지 처리기입니다.

BOOL CSubDlg::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if( pMsg->wParam == VK_ESCAPE )
		{
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CSubDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	EnableDialog(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

HBRUSH CSubDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if( !IsWindowEnabled() )
		return hbr;

	if( nCtlColor != CTLCOLOR_EDIT && 
		nCtlColor != CTLCOLOR_LISTBOX &&
		IsColorIgnoreWnd(pWnd->GetSafeHwnd()) == false )
	{
		static CBrush s_brush(::GetSysColor(COLOR_BTNHIGHLIGHT));
		pDC->SetTextColor(RGB(0, 0, 0));  //글자색(일괄 변경)
		//pDC->SetBkColor(RGB(0, 0, 0));  //배경색
		pDC->SetBkMode(TRANSPARENT);
		return s_brush;
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

//void CSubDlg::OnOK()
//{
//	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
//
//	//CDialog::OnOK();
//}
//
//void CSubDlg::OnCancel()
//{
//	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
//
//	//CDialog::OnCancel();
//}

bool CSubDlg::IsColorIgnoreWnd(HWND hWnd)
{
	HWND wnd;
	if( m_mapColorIgnoreWnd.Lookup(hWnd, (void*&)wnd) )
	{
		return true;
	}
	return false;
}

bool CSubDlg::RunInitThread()
{
	m_pInitThread = ::AfxBeginThread(CSubDlg::InitThread, this, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
	m_pInitThread->m_bAutoDelete = TRUE;
	m_pInitThread->ResumeThread();

	return true;
}

UINT CSubDlg::InitThread(LPVOID pParam)
{
	CSubDlg* pDlg = (CSubDlg*)pParam;

	pDlg->RunInit();
	pDlg->m_bInit = true;

	pDlg->CheckInstallComplete();
	pDlg->UpdateControls();
	//pDlg->Invalidate();

	::AfxGetMainWnd()->PostMessage(WM_COMPLETE_DIALOG_INIT);

	return 0;
}

void CSubDlg::EnableDialog(BOOL bEnable)
{
	EnableWindow(bEnable);

	int count = m_arrAllControls.GetCount();
	for(int i=0; i<count; i++)
	{
		HWND hwnd = (HWND)m_arrAllControls.GetAt(i);
		::EnableWindow(hwnd, bEnable);
	}
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNAMIC(CButton_Bool, CButton)

BEGIN_MESSAGE_MAP(CButton_Bool, CButton)
END_MESSAGE_MAP()

void CButton_Bool::UpdateControl()
{
	if( m_pInitValue && m_pValueChange )
	{
		CString msg;

		if( *m_pInitValue && *m_pValueChange == false )
		{
			// install
			msg.LoadString(IDS_MSG_COMPLETED);
			EnableWindow(FALSE);
		}
		else
		{
			// install or change
			msg.LoadString(IDS_MSG_INSTALL);
			EnableWindow(TRUE);
		}
		SetWindowText(msg);
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNAMIC(CButton_Check, CButton)

BEGIN_MESSAGE_MAP(CButton_Check, CButton)
END_MESSAGE_MAP()

void CButton_Check::UpdateControl()
{
	CString msg;

	if( m_pInitValue && m_pLinkButton )
	{
		if( m_pLinkButton->GetCheck() )
		{
			// install
			msg.LoadString(*m_pInitValue ? IDS_MSG_REINSTALL : IDS_MSG_INSTALL);
			EnableWindow(TRUE);
		}
		else
		{
			// not install
			msg.LoadString(*m_pInitValue? IDS_MSG_COMPLETED : IDS_MSG_INCOMPLETE);
			EnableWindow(FALSE);
		}
		SetWindowText(msg);
	}
	else if( m_pInitValue )
	{
		if( *m_pInitValue )
		{
			EnableWindow(FALSE);
			msg.LoadString(IDS_MSG_COMPLETED);
		}
		else
		{
			EnableWindow(TRUE);
			msg.LoadString(IDS_MSG_INSTALL);
		}
		SetWindowText(msg);
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNAMIC(CButton_Radio, CButton)

BEGIN_MESSAGE_MAP(CButton_Radio, CButton)
END_MESSAGE_MAP()

void CButton_Radio::UpdateControl()
{
	if( m_pInitValue )
	{
		int idx = -1;

		for(int i=0; i<10; i++)
		{
			if( m_pLinkButton[i] != NULL )
			{
				if( m_pLinkButton[i]->GetCheck() )
				{
					idx = i;
					break;
				}
			}
		}

		CString msg;
		if( *m_pInitValue < 0 || *m_pInitValue != idx )
		{
			// install
			msg.LoadString(IDS_MSG_INSTALL);
			EnableWindow(TRUE);
		}
		else
		{
			// not install
			msg.LoadString(IDS_MSG_COMPLETED);
			EnableWindow(FALSE);
		}
		SetWindowText(msg);
	}
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNAMIC(CButton_String, CButton)

BEGIN_MESSAGE_MAP(CButton_String, CButton)
END_MESSAGE_MAP()

void CButton_String::UpdateControl()
{
	if( m_arrInitValue.GetCount() > 0 )
	{
		//
		bool diff = false;
		for(int i=0; i<m_arrInitValue.GetCount(); i++)
		{
			const CString* str = (CString*)m_arrInitValue.GetAt(i);
			const CEdit* edit = (CEdit*)m_arrLinkValue.GetAt(i);
			int allow_empty = m_arrAllowEmpty.GetAt(i);

			CString cur_str;
			edit->GetWindowText(cur_str);

			if( (allow_empty == 0 && *str == "") || *str != cur_str )
			{
				diff = true;
				break;
			}
		}

		//
		CString msg;

		if( diff )
		{
			// install
			msg.LoadString(IDS_MSG_INSTALL);
			EnableWindow(TRUE);
		}
		else
		{
			// not install
			msg.LoadString(IDS_MSG_COMPLETED);
			EnableWindow(FALSE);
		}
		SetWindowText(msg);
	}
}
