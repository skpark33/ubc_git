#pragma once

#include "SubDlg.h"

#include "libDBInstall/dbInstall.h"
#include "afxwin.h"


// CDBSettingsDlg 대화 상자입니다.

class CDBSettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(CDBSettingsDlg)

public:
	CDBSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDBSettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DB_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	bool		m_bCreateInfo;

	bool		m_bCreateUserCompleted;
	bool		m_bCreateInstanceCompleted;
	bool		m_bCreateTableCompleted;
	bool		m_bCreateInitDataCompleted;

	CString		m_strDBType;
	CString		m_strServerIP;
	CString		m_strServerPort;
	CString		m_strInstanceName;
	CString		m_strUserID;
	CString		m_strPassword;

	CString		m_strLastSelectedDBType;
	CString		GetSelectDBType();

	void		UpdateInstalledStatus();

	void		SetDBField(dbServer* pDBServer, bool bSetToDefaultValue=false);

public:

	CButton		m_radioMySQL;
	CButton		m_radioMSSQL;
	CButton		m_radioMARIA;

	CEdit		m_editDBType;
	CEdit		m_editServerIP;
	CEdit		m_editServerPort;
	CEdit		m_editRootInstanceName;
	CEdit		m_editRootUserID;
	CEdit		m_editRootPassword;
	CEdit		m_editInstanceName;
	CEdit		m_editUserID;
	CEdit		m_editPassword;
	CButton_String	m_btnCreateInfo;

	CButton			m_chkCreateUser;
	CButton_Check	m_btnCreateUser;

	CButton			m_chkCreateInstance;
	CButton_Check	m_btnCreateInstance;

	CButton			m_chkCreateTable;
	CButton_Check	m_btnCreateTable;

	CButton			m_chkCreateInitData;
	CButton_Check	m_btnCreateInitData;

	CButton			m_btnExecuteAlterTable;

	afx_msg void OnBnClickedRadioDBType();

	afx_msg void OnEnChangeEditInfo();
	afx_msg void OnBnClickedButtonCreateInfo();

	afx_msg void OnBnClickedCheckCreateUser();
	afx_msg void OnBnClickedButtonCreateUser();

	afx_msg void OnBnClickedCheckCreateInstance();
	afx_msg void OnBnClickedButtonCreateInstance();

	afx_msg void OnBnClickedCheckCreateTable();
	afx_msg void OnBnClickedButtonCreateTable();

	afx_msg void OnBnClickedCheckInitData();
	afx_msg void OnBnClickedButtonInitData();

	virtual bool	RunInit();
	virtual void	UpdateControls();
	virtual int		CheckInstallComplete();
	afx_msg void OnBnClickedButtonExecuteAltertable();
};
