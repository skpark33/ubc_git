#pragma once

#include "SubDlg.h"
#include "libNasInstall/nasInstall.h"


// CNASSettingsDlg 대화 상자입니다.

class CNASSettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(CNASSettingsDlg)

public:
	CNASSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNASSettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NAS_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	nasInstall	m_NasInstall;
	int 	m_nNASOption;
	bool	m_bValueChange;

	CString	m_strNASLocation;
	CString	m_strNASID;
	CString	m_strNASPWD;
	CString	m_strDriveLetter;

	DECLARE_MESSAGE_MAP()

	bool	CheckValueChange();

public:

	CButton		m_radioDisuseNAS;
	CButton		m_radioUseNAS;

	CEdit		m_editNASLocation;
	CEdit		m_editNASID;
	CEdit		m_editNASPassword;
	CComboBox	m_cbxDriveLetter;

	CButton_Bool	m_btnNASConnect;

	afx_msg void OnBnClickedRadioNas();
	afx_msg void OnBnClickedButtonNasConnect();

	virtual void	UpdateControls();
	virtual bool	RunInit();
	virtual int		CheckInstallComplete();
};
