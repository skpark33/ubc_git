#pragma once

#include "SubDlg.h"
#include "libServerSettings/ServerSettings.h"

// CUBCServerSettingsDlg 대화 상자입니다.

class CUBCServerSettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(CUBCServerSettingsDlg)

public:
	CUBCServerSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CUBCServerSettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBC_SERVER_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	bool		m_bValueChange;

	CServerSettings		m_serverSettings;

	CString		m_strServerID;
	CString		m_strServerIP;

	CString		GetSelectServerID();

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonSetServer();
	afx_msg void OnNMClickServer(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownServer(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeEditMainServer();

	CListCtrl		m_lcServer;
	CEdit			m_editMainServer;
	CButton_Bool	m_btnSetSeverSettings;

	//void	RefreshAppList();
	//void	GetCurrentAppList(CStringArray& appList);

	virtual void	UpdateControls();
	virtual bool	RunInit();
	virtual int		CheckInstallComplete();
};
