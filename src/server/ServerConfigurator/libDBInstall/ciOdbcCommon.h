#ifndef _ciOdbcCommon_h_
#define _ciOdbcCommon_h_


#include <sql.h> 
#include <sqlucode.h>  //skpark 2011.11.02  for Unicode
#include <sqlext.h>
#include <odbcinst.h>

#define IS_SQL_ERR !IS_SQL_OK
//skpark modify 2007.8.20
#define IS_SQL_EXEC_OK(res) (res==SQL_SUCCESS_WITH_INFO || res==SQL_SUCCESS || res==SQL_NO_DATA)
#define IS_SQL_OK(res) (res==SQL_SUCCESS_WITH_INFO || res==SQL_SUCCESS)
#define SAFE_STR(str) ((str==NULL) ? "" : str)
	//--
	enum sqlDataTypes
	{
		sqlDataTypeUnknown=SQL_UNKNOWN_TYPE,
		sqlDataTypeChar=SQL_CHAR,
		sqlDataTypeNumeric=SQL_NUMERIC,
		sqlDataTypeDecimal=SQL_DECIMAL,
		sqlDataTypeInteger=SQL_INTEGER,
		sqlDataTypeSmallInt=SQL_SMALLINT,
		sqlDataTypeFloat=SQL_FLOAT,
		sqlDataTypeReal=SQL_REAL,
		sqlDataTypeDouble=SQL_DOUBLE,
#if (ODBCVER >= 0x0300)
		sqlDataTypeDateTime=SQL_DATETIME,
#endif
		sqlDataTypeVarChar=SQL_VARCHAR
	};

class  ODBCTool
{
public:
	static WCHAR* _new_Char2WChar( char* szSrc, size_t& wlen )
	{
		if( szSrc == 0 )	{
			return 0;
		}
		int nlen = strlen(szSrc) + 1;
		WCHAR* pwchRetVal = new WCHAR[nlen*sizeof(WCHAR)];
		wlen = mbstowcs(pwchRetVal, szSrc, nlen);
		return pwchRetVal;
	}
};

class  ODBCConnection
{
public:
	bool Connect(const char * svSource)
	{
		int nConnect = SQLAllocHandle( SQL_HANDLE_ENV, SQL_NULL_HANDLE, &m_hEnv );
		if( nConnect == SQL_SUCCESS || nConnect == SQL_SUCCESS_WITH_INFO ) {
			nConnect = SQLSetEnvAttr( m_hEnv, 
				SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0 );
			if( nConnect == SQL_SUCCESS || nConnect == SQL_SUCCESS_WITH_INFO ) {
				nConnect = SQLAllocHandle( SQL_HANDLE_DBC, m_hEnv, &m_hDBC );
				if( nConnect == SQL_SUCCESS || nConnect == SQL_SUCCESS_WITH_INFO ) {
					SQLSetConnectOption( m_hDBC,SQL_LOGIN_TIMEOUT,5 );                
					short shortResult = 0;
					SQLCHAR szOutConnectString[ 1024 ];
						nConnect = SQLDriverConnect( m_hDBC,                // Connection Handle
					NULL,                           // Window Handle
						(SQLCHAR*)svSource,  // InConnectionString
						strlen(svSource),             // StringLength1
						szOutConnectString,             // OutConnectionString
						sizeof( szOutConnectString ),   // Buffer length
						&shortResult,                   // StringLength2Ptr
						SQL_DRIVER_NOPROMPT );          // no User prompt
					return IS_SQL_OK(nConnect);
				}
			}
		}
		if( m_hDBC != NULL ) {
			m_nReturn = SQLDisconnect( m_hDBC );
			m_nReturn = SQLFreeHandle( SQL_HANDLE_DBC,  m_hDBC );
		}
		if( m_hEnv!=NULL )
			m_nReturn = SQLFreeHandle( SQL_HANDLE_ENV, m_hEnv );
		m_hDBC              = NULL;
		m_hEnv              = NULL;
		m_nReturn           = SQL_ERROR;
		return( IS_SQL_OK(nConnect) );
	}

	bool ConnectW(SQLWCHAR * svSource, size_t wlen)
	{
		int nConnect = SQLAllocHandle( SQL_HANDLE_ENV, SQL_NULL_HANDLE, &m_hEnv );
		if( nConnect == SQL_SUCCESS || nConnect == SQL_SUCCESS_WITH_INFO ) {
			nConnect = SQLSetEnvAttr( m_hEnv, 
				SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0 );
			if( nConnect == SQL_SUCCESS || nConnect == SQL_SUCCESS_WITH_INFO ) {
				nConnect = SQLAllocHandle( SQL_HANDLE_DBC, m_hEnv, &m_hDBC );
				if( nConnect == SQL_SUCCESS || nConnect == SQL_SUCCESS_WITH_INFO ) {
					SQLSetConnectOption( m_hDBC,SQL_LOGIN_TIMEOUT,5 );                
					short shortResult = 0;
					SQLWCHAR szOutConnectString[ 1024 ];
						nConnect = SQLDriverConnectW( m_hDBC,                // Connection Handle
					NULL,                           // Window Handle
						svSource,					// InConnectionString
						wlen,						// StringLength1
						szOutConnectString,             // OutConnectionString
						sizeof( szOutConnectString ),   // Buffer length
						&shortResult,                   // StringLength2Ptr
						SQL_DRIVER_NOPROMPT );          // no User prompt
					return IS_SQL_OK(nConnect);
				}
			}
		}
		if( m_hDBC != NULL ) {
			m_nReturn = SQLDisconnect( m_hDBC );
			m_nReturn = SQLFreeHandle( SQL_HANDLE_DBC,  m_hDBC );
		}
		if( m_hEnv!=NULL )
			m_nReturn = SQLFreeHandle( SQL_HANDLE_ENV, m_hEnv );
		m_hDBC              = NULL;
		m_hEnv              = NULL;
		m_nReturn           = SQL_ERROR;
		return( IS_SQL_OK(nConnect) );
	}

	ODBCConnection()
	{
		m_hDBC              = NULL;
		m_hEnv              = NULL;
		m_nReturn           = SQL_ERROR;
	}
	virtual ~ODBCConnection()
	{
		if( m_hDBC != NULL ) {
			m_nReturn = SQLFreeHandle( SQL_HANDLE_DBC,  m_hDBC );
	}
	if( m_hEnv!=NULL )
		m_nReturn = SQLFreeHandle( SQL_HANDLE_ENV, m_hEnv );
	}

	bool disconnect()
	{
		if( m_hDBC != NULL )
		{
			m_nReturn = SQLDisconnect( m_hDBC );
			if(!IS_SQL_OK(m_nReturn))
			{	
				return false;
			}			
		}
		// skpark 2012.4.3 FreeHandle 하지 않고, 그냥 NULL 처리한 것은 수상하다
		//m_hDBC = NULL;

		return true;
	}

public:
	operator HDBC()
	{
		return m_hDBC;
	}
protected :
	SQLRETURN       m_nReturn;      // Internal SQL Error code
	HENV            m_hEnv;         // Handle to environment
	HDBC            m_hDBC;         // Handle to database connection
};
	//--
class  MSSQLConnection : public ODBCConnection
{
public:
	enum enumProtocols
	{
		protoNamedPipes,
		protoWinSock,
		protoIPX,
		protoBanyan,
		protoRPC
	};
public:
	MSSQLConnection (){};
	virtual ~MSSQLConnection (){};
	BOOL Connect(const char* User="",const char * Pass="",	const char * Host="local" ,BOOL Trusted=1, enumProtocols Proto=protoWinSock)
	{
		char str[512]="";
		//DRIVER={sql server};SERVER=211.232.57.154;""UID=rdveda;PWD=optahera;
		//sprintf(str, "Driver={SQL Server};Server=%s;Uid=%s;Pwd=%s;Trusted_Connection=%s;Network=%s;" ,Host,User,Pass, (Trusted ? "Yes" : "No"));
		sprintf(str, "Driver={SQL Server};Server=%s;Uid=%s;Pwd=%s;", Host, User, Pass);

		/*switch(Proto)
		{
		case protoNamedPipes:
			strcat(str,"dbnmpntw");
			break;
		case protoWinSock:
			strcat(str,"dbmssocn");
			break;
		case protoIPX:
			strcat(str,"dbmsspxn");
			break;
		case protoBanyan:
			strcat(str,"dbmsvinn");
			break;
		case protoRPC:
			strcat(str, "dbmsrpcn");
			break;
		default:
			strcat(str, "dbmssocn");
			break;
		}
			strcat(str,";");*/
			return ODBCConnection::Connect(str);
	};

	BOOL ConnectW(const char* User="",const char * Pass="",	const char * Host="local" ,BOOL Trusted=1, enumProtocols Proto=protoWinSock)
	{
		char str[512]="";
		sprintf(str, "Driver={SQL Server};AutoTranslate=yes;Server=%s;Uid=%s;Pwd=%s;", Host, User, Pass);
		size_t len = 0;
		WCHAR* wstr =  ODBCTool::_new_Char2WChar(str,len);
		if(wstr == 0 || len == 0){
			printf("char 2 wchar encoding failed !!!!!!!!!!!!!!!!!!!\n");
			return false;
		}
		bool retval =  ODBCConnection::ConnectW(wstr, len);
		//printf("ODBCConnection::ConnectW !!!!!!!!!!!!!!!!!!!\n");
		delete [] wstr;
		return retval;
	};

	bool disconnect() { return ODBCConnection::disconnect(); }
};

 class  MDBConnection : public ODBCConnection
 {
 public:
	 MDBConnection(){};
	 virtual ~MDBConnection(){};
	 int Connect(const char * MDBPath,const char * User="", const char * Pass="",BOOL Exclusive=0)
	 {
		 char str[512]="";
		 sprintf("Driver={Microsoft Access Driver (*.mdb)};Dbq=%s;Uid=%s;Pwd=%s;Exclusive=%s;", MDBPath,User, Pass,(Exclusive ? "yes" : "no"));
		 return ODBCConnection::Connect(str);
	 };

	 bool disconnect() { return ODBCConnection::disconnect(); }
 };
 
 class  ODBCStmt
 {
public:
	 HSTMT m_hStmt;
	 operator HSTMT()
	 {
		 return m_hStmt;
	 }
	 ODBCStmt(HDBC hDBCLink)
	 {
		 SQLRETURN m_nReturn;
		 m_nReturn = SQLAllocHandle( SQL_HANDLE_STMT, hDBCLink, &m_hStmt );
		 SQLSetStmtAttr(m_hStmt, SQL_ATTR_CONCURRENCY, (SQLPOINTER) SQL_CONCUR_ROWVER, 0);
		 SQLSetStmtAttr(m_hStmt, SQL_ATTR_CURSOR_TYPE, (SQLPOINTER) SQL_CURSOR_KEYSET_DRIVEN, 0);
		 if(!IS_SQL_OK(m_nReturn))
			 m_hStmt=INVALID_HANDLE_VALUE;
	 }
	 virtual ~ODBCStmt()
	 {
		 if(m_hStmt!=INVALID_HANDLE_VALUE)
			 SQLFreeHandle(SQL_HANDLE_STMT,m_hStmt);
	 }
	 BOOL IsValid()
	 {
		 return m_hStmt!=INVALID_HANDLE_VALUE;
	 }
	 USHORT GetColumnCount()
	 {
		 short nCols=0;
		 if(!IS_SQL_OK(SQLNumResultCols(m_hStmt,&nCols)))
			 return 0;
		 return nCols;
	 }
	 DWORD GetChangedRowCount(void)
	 {
		 long nRows=0;
		 if(!IS_SQL_OK(SQLRowCount(m_hStmt,&nRows)))
			 return 0;
		 return nRows;
	 }
	 BOOL execute( const char * strSQL)
	 {
		 SQLRETURN nRet=SQLExecDirect( m_hStmt, (SQLCHAR *)strSQL, SQL_NTS );
		 return IS_SQL_EXEC_OK( nRet );
	 }
	 BOOL executeW(WCHAR* strSQL)  //skpark 2011.11.02  for Unicode
	 {
		 SQLRETURN nRet=SQLExecDirectW( m_hStmt, (SQLWCHAR *)strSQL, SQL_NTS );
		 return IS_SQL_EXEC_OK( nRet );
	 }
	 BOOL Fetch()
	 {
		 SQLRETURN nRet=SQLFetch(m_hStmt);
		 return IS_SQL_OK( nRet );
	 }
	 BOOL FecthRow(UINT nRow)
	 {
		 return IS_SQL_OK(SQLSetPos(m_hStmt, nRow, SQL_POSITION, SQL_LOCK_NO_CHANGE));
	 }
	 BOOL FetchPrevious()
	 {
		 SQLRETURN nRet=SQLFetchScroll(m_hStmt,SQL_FETCH_PRIOR,0);
		 return IS_SQL_OK(nRet);
	 }
	 BOOL FecthNext()
	 {
		 SQLRETURN nRet=SQLFetchScroll(m_hStmt,SQL_FETCH_NEXT,0);
		 return IS_SQL_OK(nRet);
	 }
	 BOOL FetchRow(ULONG nRow,BOOL Absolute=1)
	 {
		 SQLRETURN nRet=SQLFetchScroll(m_hStmt,
			 (Absolute ? SQL_FETCH_ABSOLUTE : SQL_FETCH_RELATIVE),nRow);
		 return IS_SQL_OK(nRet);
	 }
	 BOOL FetchFirst()
	 {
		 SQLRETURN nRet=SQLFetchScroll(m_hStmt,SQL_FETCH_FIRST,0);
		 return IS_SQL_OK(nRet);
	 }
	 BOOL FetchLast()
	 {
		 SQLRETURN nRet=SQLFetchScroll(m_hStmt,SQL_FETCH_LAST,0);
		 return IS_SQL_OK(nRet);
	 }
	 BOOL Cancel()
	 {
		 SQLRETURN nRet=SQLCancel(m_hStmt);
		 return IS_SQL_OK(nRet);
	 }
 };
 //--
 class  ODBCRecord
 {
	 HSTMT m_hStmt;
 public:
	 ODBCRecord(HSTMT hStmt){m_hStmt=hStmt;};
	 ~ODBCRecord(){};
	 USHORT GetColumnCount()
	 {
		 short nCols=0;
		 if(!IS_SQL_OK(SQLNumResultCols(m_hStmt,&nCols)))
			 return 0;
		 return nCols;
	 }
	 BOOL BindColumn(USHORT Column,LPVOID pBuffer,
		 ULONG pBufferSize,LONG * pReturnedBufferSize=NULL,
		 USHORT nType=SQL_C_CHAR)
	 {
		 LONG pReturnedSize=0;
		 SQLRETURN Ret=SQLBindCol(m_hStmt,Column,nType,
			 pBuffer,pBufferSize,&pReturnedSize);
		 if(*pReturnedBufferSize)
			 *pReturnedBufferSize=pReturnedSize;
		 return IS_SQL_OK(Ret);
	 }
	 USHORT GetColumnByName(const char * Column)
	 {
		 SHORT nCols=GetColumnCount();
		 for(USHORT i=1;i<(nCols+1);i++)
		 {
			 CHAR Name[256]="";
			 GetColumnName(i,Name,sizeof(Name));
			 if(!_stricmp(Name,Column))
				 return i;
		 }
		 return 0;
	 }
	 BOOL GetData(USHORT Column, LPVOID pBuffer, 
		 ULONG pBufLen, LONG * dataLen=NULL, int Type=SQL_C_DEFAULT)
	 {
		 SQLINTEGER od=0;
		 int Err=SQLGetData(m_hStmt,Column,Type,pBuffer,pBufLen,&od);
		 if(IS_SQL_ERR(Err))
		 { 
			 return 0;
		 } 
		 if(dataLen)
			 *dataLen=od;
		 return 1;
	 }
	 SQLRETURN GetLongData(USHORT Column, LPVOID pBuffer, ULONG pBufLen, 
		 SQLINTEGER& od, LONG * dataLen=NULL, int Type=SQL_C_DEFAULT)
	 {
		 SQLRETURN ret = SQL_NTS;
		 //ret = SQLGetData(m_hStmt, Column, SQL_C_DEFAULT,pBuffer ,pBufLen, &od);
		 ret = SQLGetData(m_hStmt, Column, Type,pBuffer ,pBufLen, &od);
		 return ret ;
	 }

	 int GetColumnType( USHORT Column )
	 {
		 int nType=SQL_C_DEFAULT;
		 SQLCHAR svColName[ 256 ]="";
		 SWORD swCol=0,swType=0,swScale=0,swNull=0;
		 UDWORD pcbColDef;
		 SQLDescribeCol( m_hStmt,            // Statement handle
			 Column,             // ColumnNumber
			 svColName,          // ColumnName
			 sizeof( svColName), // BufferLength
			 &swCol,             // NameLengthPtr
			 &swType,            // DataTypePtr
			 &pcbColDef,         // ColumnSizePtr
			 &swScale,           // DecimalDigitsPtr
			 &swNull );          // NullablePtr
		 nType=(int)swType;
		 return( nType );
	 }

	 DWORD GetColumnSize( USHORT Column )
	 {
		 int nType=SQL_C_DEFAULT;
		 SQLCHAR svColName[ 256 ]="";
		 SWORD swCol=0,swType=0,swScale=0,swNull=0;
		 DWORD pcbColDef=0;
		 SQLDescribeCol( m_hStmt,            // Statement handle
			 Column,             // ColumnNumber
			 svColName,          // ColumnName
			 sizeof( svColName), // BufferLength
			 &swCol,             // NameLengthPtr
			 &swType,            // DataTypePtr
			 &pcbColDef,         // ColumnSizePtr
			 &swScale,           // DecimalDigitsPtr
			 &swNull );          // NullablePtr
		 return pcbColDef;
	 }
	 DWORD GetColumnScale( USHORT Column )
	 {
		 int nType=SQL_C_DEFAULT;
		 SQLCHAR svColName[ 256 ]="";
		 SWORD swCol=0,swType=0,swScale=0,swNull=0;
		 DWORD pcbColDef=0;
		 SQLDescribeCol( m_hStmt,            // Statement handle
			 Column,             // ColumnNumber
			 svColName,          // ColumnName
			 sizeof( svColName), // BufferLength
			 &swCol,             // NameLengthPtr
			 &swType,            // DataTypePtr
			 &pcbColDef,         // ColumnSizePtr
			 &swScale,           // DecimalDigitsPtr
			 &swNull );          // NullablePtr
		 return swScale;
	 }

	 BOOL GetColumnName( USHORT Column, LPTSTR Name, SHORT NameLen )
	 {
		 int nType=SQL_C_DEFAULT;
		 SWORD swCol=0,swType=0,swScale=0,swNull=0;
		 DWORD pcbColDef=0;
		 SQLRETURN Ret=
			 SQLDescribeCol( m_hStmt,            // Statement handle
			 Column,               // ColumnNumber
			 (SQLCHAR*)Name,     // ColumnName
			 NameLen,    // BufferLength
			 &swCol,             // NameLengthPtr
			 &swType,            // DataTypePtr
			 &pcbColDef,         // ColumnSizePtr
			 &swScale,           // DecimalDigitsPtr
			 &swNull );          // NullablePtr
		 if(IS_SQL_ERR(Ret))
			 return 0;
		 return 1;
	 }
	 BOOL IsColumnNullable( USHORT Column )
	 {
		 int nType=SQL_C_DEFAULT;
		 SQLCHAR svColName[ 256 ]="";
		 SWORD swCol=0,swType=0,swScale=0,swNull=0;
		 UDWORD pcbColDef;
		 SQLDescribeCol( m_hStmt,            // Statement handle
			 Column,             // ColumnNumber
			 svColName,          // ColumnName
			 sizeof( svColName), // BufferLength
			 &swCol,             // NameLengthPtr
			 &swType,            // DataTypePtr
			 &pcbColDef,         // ColumnSizePtr
			 &swScale,           // DecimalDigitsPtr
			 &swNull );          // NullablePtr
		 return (swNull==SQL_NULLABLE);
	 }
 };
//};

#endif

