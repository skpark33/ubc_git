#ifndef _dbServer_h_
#define _dbServer_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include "ServerConfigurator/Installable/installable.h"

#define DB_KIND_MYSQL	"MYSQL"
#define DB_KIND_MSSQL	"MSDB"
#define DB_KIND_MARIA	"MARIA"

#define DB_KIND				"KIND"
#define DB_DBNAME			"DBNAME"
#define DB_PORT				"PORT"
#define DB_SERVER			"SERVER"
#define DB_USR				"USR"
#define DB_PWD				"PWD"
#define DB_ROOT_USR			"ROOT_USR"
#define DB_ROOT_PWD			"ROOT_PWD"
#define DB_ROOT_DBNAME		"ROOT_DBNAME"

class dbServer : public installable {
public:
	dbServer(const char* dbKind=NULL)	{ _getDBInfo(dbKind); printInfo(); };
	dbServer(const char* dbKind, const char* serverIp, const char* dbInst, const char* dbUser, const char* dbPasswd,int dbPort); 
	virtual ~dbServer() {}

	// void setDBKind(const char* dbKind);

	virtual int prerequisite();
	virtual int test();
	virtual int	install();

	virtual int get(const char* key, string& value);
	virtual int get(const char* key, int& value);

	virtual int set(const char* key, const char* value);
	virtual int set(const char* key, int value);

	//virtual void printError();
	virtual void  printInfo();
protected:

	virtual int _mysql_prerequisite();
	virtual int _mysql_test(ciBoolean isRootTest);
	virtual int	_mysql_install();

	virtual int _mssql_prerequisite();
	virtual int _mssql_test();
	virtual int	_mssql_install();

	virtual int	_validTest();
	virtual int _getDBInfo(const char* dbKind=NULL);
	virtual int _runSQLScript(const char* scriptName);
	virtual int _mysql_runSQLScript(const char* scriptName);
	virtual int _mssql_runSQLScript(const char* scriptName);
	virtual int _runSQLScriptUsingSystem(const char* scriptName);

	int	_web_config();
	int	_read_web_config(const char* path, ciString& replaceStr1, ciString& replaceStr2, ciString& readValue);
	int	_writeFile(const char* path, ciString& writeValue);

	int	_localSettings_asp();
	int	_read_localSettings_asp(const char* path, const char* findStr, const char* replaceStr, ciString& readValue);

	int	_mysql_script();
	int	_mysql_script_user();
	int	_mysql_script_inst();
	int	_mysql_script_table();
	int	_mysql_script_data();

	int	_mssql_script();
	int	_mssql_script_user();
	int	_mssql_script_inst();
	int	_mssql_script_table();
	int	_mssql_script_data();

	ciBoolean	_isGlobal();

	string	_dbKind;
	string	_dbHost;
	string	_dbInst;
	string	_dbUser;
	string	_dbPasswd;
	long	_dbPort;
	string	_dbKind2;

	string	_rootUser;
	string	_rootPasswd;
	string	_rootInst;

};


class createUser :  public dbServer {
public:
	createUser() {};
	virtual ~createUser() {};
	virtual int test();

protected:

	virtual int _mysql_prerequisite();
	virtual int _mysql_test();
	virtual int	_mysql_install();

	virtual int _mssql_prerequisite();
	virtual int _mssql_test();
	virtual int	_mssql_install();

};

class createDB :  public dbServer {
public:
	createDB() {};
	virtual ~createDB() {};
	virtual int test();
	
protected:
	virtual int _mysql_prerequisite();
	virtual int _mysql_test();
	virtual int	_mysql_install();

	virtual int _mssql_prerequisite() ;
	virtual int _mssql_test();
	virtual int	_mssql_install();

};

class createTable :  public dbServer {
public:
	createTable() {};
	virtual ~createTable() {};
	
	virtual int test();

protected:
	virtual int _mysql_prerequisite();
	virtual int _mysql_test();
	virtual int	_mysql_install();

	virtual int _mssql_prerequisite();
	virtual int _mssql_test();
	virtual int	_mssql_install();

};


class createData :  public dbServer {
public:
	createData() {};
	virtual ~createData() {};
	
	virtual int test();

protected:
	virtual int _mysql_prerequisite();
	virtual int _mysql_test();
	virtual int	_mysql_install();

	virtual int _mssql_prerequisite();
	virtual int _mssql_test();
	virtual int	_mssql_install();

};

#endif // _dbServer_h_
