#ifndef _ciMsdbSession_h_
#define _ciMsdbSession_h_


#include <ci/libBase/ciBaseType.h>
#include <cstdio>
#include <ci/libBase/ciListType.h>
#include "ciOdbcCommon.h"

typedef vector<ciInt> ParamType;
typedef ParamType::iterator paramIter;


class  ciMsdbSession : public MSSQLConnection{
	protected:

		ciBoolean	_executeUnicode(char* pSql);

		ciBoolean	_execute(char *pSql);
		ciBoolean	_execute_get(char *pSql);
		void		_Error(short int HandleType, SQLHANDLE* oHandle, ciString& Error, const char* Query);

		ciString	_name;
		ciString	_odbcName;
		ciString	_dbName;
		ciString	_dbUser;
		ciString	_dbPasswd;
		

		static int	_gId;
		int			_id;

		ODBCStmt*	_oStmt;

	public:
		ciMsdbSession();
		ciMsdbSession(const char * pOdbcName, const char* pDbName, const char* pDbUser, const char* pDbPass);

		~ciMsdbSession();

		ciBoolean	connect();
		ciBoolean	disconnect();

		ciBoolean	execute(const char* pSql);
		ciBoolean	execute(ciString &pSql);
		ciBoolean	executeGet(ciString &pSql);

		ciBoolean	transCommit();

////////////////////////////////////////////////////////////////////////////
		ciBoolean	isEof();
		ciBoolean	next();
		void		clear(ciBoolean hasCursor);

		ciBoolean	isGlobal();

		int			waitNo;
	//	time_t		acquiredTime;
};

#endif // _ciMsdbSession_h_
