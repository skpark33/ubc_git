#include "StdAfx.h"
#include "ServerSettings.h"
#include "resource.h"

#include "ci/libConfig/ciIni.h"
#include "ci/libConfig/ciXProperties.h"
#include "ci/libDebug/ciDebug.h"

#include "cci/libValue/cciTime.h"
#include "cci/libValue/cciInteger64.h"
#include "cci/libValue/cciBoolean.h"
#include "cci/libObject/cciObject.h"
#include "cci/libWrapper/cciCall.h"
#include "cci/libPS/libPSFactory/cciPSFactory.h"

#include "common/libCommon/ubcIni.h"
#include "common/libCommon/ubcUtil.h"
#include "common/libScratch/scratchUtil.h"
#include "common/libScratch/scEnv.h"

#include "../libInstallEnv/EnvVar.h"


ciSET_DEBUG(10, "CServerSettings");


CServerSettings::CServerSettings(void)
{
	m_strAppListName = "";
}

CServerSettings::~CServerSettings(void)
{
}

int CServerSettings::get(LPCSTR key, CString& value)
{
	value = "";

	if( m_mapKeyValues.Lookup(key, value) ) return TRUE;

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);

	return FALSE;
}

int CServerSettings::set(LPCSTR key, LPCSTR value)
{
	m_mapKeyValues.SetAt(key, value);
	return TRUE;
}

CString CServerSettings::get(LPCSTR key)
{
	CString value="";
	m_mapKeyValues.Lookup(key, value);
	return value;
}

bool CServerSettings::GetInfo()
{
	bool ret_val = true;

	// get current ip
	ciString current_ip;
	scratchUtil::getInstance()->getIpAddress(current_ip);
	if( current_ip.empty() )
	{
		setError(-1, "Fail to get IP address");
		ciERROR((_errString.c_str()));
		set(KEY_CURRENT_IP_ADDRESS, _errString.c_str());
		ret_val = false;
	}
	set(KEY_CURRENT_IP_ADDRESS, current_ip.c_str());
	ciDEBUG(1,("%s=%s", KEY_CURRENT_IP_ADDRESS, current_ip.c_str()));

	// get app-list for each server
	m_arSlaveServerAppList.RemoveAll();
	m_arSlaveServerAppList.Add("Dummy");
	ubcIni aIni("UBCStarter", _COP_CD("C:\\Project\\ubc\\config"), "data");
	int i = 1;
	while(1)
	{
		char key[128] = {0};
		sprintf(key, "APP_LIST_%d", i++);

		ciString buf = "";
		aIni.get("ROOT", key, buf);

		if( buf.length() == 0 ) break;

		m_arSlaveServerAppList.Add(buf.c_str());
	}
	if( m_arSlaveServerAppList.GetCount() <= 1 ) ret_val = false;

	// get slave server name
	CString slave_name = "";
	GetSlaveName(slave_name);
	set(KEY_SERVER_ID, (LPCSTR)slave_name );
	if( slave_name == "" ) ret_val = false;

	// get NAMESERVER_HOST
	ubcIni bIni("UBCConnect", _COP_CD("C:\\Project\\ubc\\config"), "data");
	ciString nameserver_host;
	bIni.get("ORB_NAMESERVICE", "IP", nameserver_host);
	set(KEY_NAMESERVER_HOST, nameserver_host.c_str() );
	if( nameserver_host == "" ) ret_val = false;

	return ret_val;
}

int CServerSettings::test()
{
	ciDEBUG(1, ("test()") );

	int ret_val = TRUE;

	//
	if( !GetInfo() ) ret_val=FALSE;

	if( !IsIPConfirmed() ) ret_val=FALSE;

	ciDEBUG(1, ("test() end") );
	return ret_val;
}

int CServerSettings::install()
{
	CString slave_name = get(KEY_SERVER_ID);

	// call changeServerIP
	// (call changePMO)
	CString nameserver_host = get(KEY_NAMESERVER_HOST);
	if( !changeServerIP(nameserver_host, true) ) return FALSE;

	// change fs.properties
	if( !changeFsProperties(nameserver_host) ) return FALSE;

	// get slave list
	CUIntArray server_list;
	GetServerIDList(server_list);
	if( server_list.GetCount() == 0 )
	{
		setError(-1, "Server ID list is NULL !!!");
		ciERROR((_errString.c_str()));
		return FALSE;
	}

	// set to UBCServer.ini
	{
		ubcIni aIni("UBCServer", _COP_CD("C:\\Project\\ubc"));
		aIni.set("ROOT", "SLAVE_NAME", (LPCSTR)slave_name);
	}

	// get app list (merging app-list)
	CStringArray app_list;
	if( server_list.GetCount() > 1 ) // only 1 => not need to parsing
	{
		for( int i=0; i<server_list.GetCount(); i++ )
		{
			int server_id = server_list.GetAt(i);
			const CString& slave_app_list = m_arSlaveServerAppList.GetAt(server_id);

			int pos = 0;
			while(1)
			{
				CString token = slave_app_list.Tokenize(" ,", pos);
				if( token == "" ) break;

				app_list.Add(token);
			}
		}
	}

	// delete duplicate(CLI, CONTENTSDISTRIBUTOR, SOCKETCALLGATEWAY, VNCManager, UBCServerAgent)
	bool find_cli=false, find_cd=false, find_scg=false, find_vnc=false, find_usa=false, find_svc=false;
	for( int i=app_list.GetCount()-1; i>=0; i-- )
	{
		const CString& app_id = app_list.GetAt(i);

		bool is_dup = false;

		if( strnicmp(app_id, "CLI", strlen("CLI")) == 0 )
		{
			if( find_cli ) is_dup = true;
			find_cli = true;
		}
		else if( strnicmp(app_id, "CONTENTSDISTRIBUTOR", strlen("CONTENTSDISTRIBUTOR")) == 0 )
		{
			if( find_cd ) is_dup = true;
			find_cd = true;
		}
		else if( strnicmp(app_id, "SOCKETCALLGATEWAY", strlen("SOCKETCALLGATEWAY")) == 0 )
		{
			if( find_scg ) is_dup = true;
			find_scg = true;
		}
		else if( strnicmp(app_id, "VNCManager", strlen("VNCManager")) == 0 )
		{
			if( find_vnc ) is_dup = true;
			find_vnc = true;
		}
		else if( strnicmp(app_id, "UBCServerAgent", strlen("UBCServerAgent")) == 0 )
		{
			if( find_usa ) is_dup = true;
			find_usa = true;
		}
		else if( strnicmp(app_id, "UBCSERVICE", strlen("UBCSERVICE")) == 0 )
		{
			if( find_svc ) is_dup = true;
			find_svc = true;
		}

		if(is_dup) app_list.RemoveAt(i);
	}

	// convert app-list(merging map) to string
	CString str_app_list = "";
	for( int i=0; i<app_list.GetCount(); i++ )
	{
		if( str_app_list.GetLength() != 0 ) str_app_list+=",";
		str_app_list += app_list.GetAt(i);
	}

	// create new APP_LIST_X_Y_Z key-value
	if( str_app_list.GetLength() > 0 )
	{
		CString app_name = "APP_LIST_" + slave_name;

		ubcIni aIni("UBCStarter", _COP_CD("C:\\Project\\ubc\\config"), "data");
		aIni.set("ROOT", app_name, (LPCSTR)str_app_list);
	}

	return TRUE;
}

void CServerSettings::GetAllSlaveIDList(CStringArray& server_list)
{
	for( int i=1; i<m_arSlaveServerAppList.GetCount(); i++ )
	{
		CString id;
		id.Format("%d", i);

		server_list.Add(id);
	}
}

void CServerSettings::GetSlaveName(CString& slaveName)
{
	ciDEBUG(1, ("GetSlaveName()"));

	string slave_name = "";

	ubcIni aIni("UBCServer", _COP_CD("C:\\Project\\ubc"));
	aIni.get("ROOT", "SLAVE_NAME", slave_name);

	slaveName = slave_name.c_str();

	ciDEBUG(1, ("getSlaveName(%s) end", slaveName) );
	return;
}

bool CServerSettings::IsIPConfirmed()
{
	ciDEBUG(1, ("IsIPConfirmed()"));

	_errString = "";

	//
	//CString current_ip;
	//get(KEY_CURRENT_IP_ADDRESS, current_ip);
	CString nameserver_host = get(KEY_NAMESERVER_HOST);

	//
	const char* env_nameserver_host = getenv("NAMESERVER_HOST");

	if( env_nameserver_host == NULL || nameserver_host != env_nameserver_host )
	{
		setError(-1, "Not confimed NAMESERVER_HOST");
		ciWARN((_errString.c_str()));
		return false;
	}

	//
	bool retval = true;
	retval &= IsSameInProperties("COP.SERVER_SP01.IPADDRESS",	nameserver_host);
	retval &= IsSameInProperties("COP.SERVER_SP02.IPADDRESS",	nameserver_host);
	retval &= IsSameInProperties("COP.SERVER_SP03.IPADDRESS",	nameserver_host);
	retval &= IsSameInProperties("COP.SOCKET_PROXY_SERVER.IP",	nameserver_host);
//	retval &= IsSameInProperties("PS.CM_DB.SERVER",				nameserver_host);
//	retval &= IsSameInProperties("PS.MYSQL_DB.SERVER",			nameserver_host);
//	retval &= IsSameInProperties("PS.MS_DB.COP_ODBC",			nameserver_host);
	retval &= IsSameInProperties("AGT.SERVER.MUX",				nameserver_host);

	//retval &= IsSameInProperties("AGT.SERVER.1",				nameserver_host);
	//retval &= IsSameInProperties("AGT.SERVER.2",				nameserver_host);
	CUIntArray server_id_list;
	GetServerIDList(server_id_list);
	for( int i=0; i<server_id_list.GetCount(); i++ )
	{
		CString name;
		name.Format("AGT.SERVER.%d", server_id_list.GetAt(i));
		retval &= IsSameInProperties(name, nameserver_host);
	}

	//
	if( retval == false )
	{
		setError(-1, "Not confimed ubc.properties");
		ciWARN((_errString.c_str()));
		return false;
	}

	//
	retval &= IsSameInIni("ORB_NAMESERVICE","IP", nameserver_host);
	retval &= IsSameInIni("AGENTMUX",		"IP", nameserver_host);
	retval &= IsSameInIni("TIMESYNCER",		"IP", nameserver_host);
	retval &= IsSameInIni("VNCMANAGER",		"IP", nameserver_host);

	ciString temp_url = "http://" + nameserver_host;
	ciString url;
	ciIniUtil::HttpToHttps(temp_url.c_str(), url);
	retval &= IsSameInIni("DEVICEWIZARD",	"TestURL", url.c_str());
	if( retval == false )
	{
		setError(-1, "Not confimed UBCConnect.ini");
		ciWARN((_errString.c_str()));
		return false;
	}

	//
	ciString fs_path = _COP_CD("C:\\Project\\ubc\\config");
			 fs_path += "\\tao\\fs.properties";

	FILE* fp = fopen(fs_path.c_str(), "r");
	if( fp == NULL )
	{
		setError(errno, "File not found (fs.properties)");
		ciWARN((_errString.c_str()));
		return false;
	}

	char buf[1024]={0};
	fgets(buf, 1023, fp);
	fclose(fp);

	char* find_str = strstr(buf, "iiop:");
	if( find_str == NULL )
	{
		setError(-1, "Invalid fs.properties file");
		ciWARN((_errString.c_str()));
		return false;
	}
	if( strncmp(find_str+5, nameserver_host, nameserver_host.GetLength()) != 0 )
	{
		setError(-1, "Diff server ip address in fs.properties file");
		ciWARN((_errString.c_str()));
		return false;
	}

	ciDEBUG(1,("IsIPConfirmed() end"));
	//
	return true;
}

bool CServerSettings::IsSameInProperties(LPCSTR name, LPCSTR value)
{
	ciXProperties* aProp = ciXProperties::getInstance();
	if( aProp == NULL ) return false;

	ciString buf;
	aProp->get(name, buf);

	if( strncmp(value,buf.c_str(),strlen(value)) == 0 )
	{
		ciDEBUG(10, ("%s=%s is same with %s", name, buf.c_str(), value));
		return true;
	}
	ciDEBUG(10, ("%s=%s is not same with %s", name, buf.c_str(), value));
	return false;
}

bool CServerSettings::IsSameInIni(LPCSTR section, LPCSTR name, LPCSTR value)
{
	ubcIni aIni("UBCConnect", _COP_CD("C:\\Project\\ubc\\config"), "data");

	ciString buf;
	aIni.get(section,name,buf);

	if( strncmp(value,buf.c_str(),strlen(value)) == 0 )
	{
		ciDEBUG(10, ("%s,%s=%s is same with %s", section, name, buf.c_str(), value));
		return true;
	}
	ciDEBUG(10, ("%s,%s=%s is not same with %s", section, name, buf.c_str(), value));
	return false;
}

void CServerSettings::GetServerIDList(CUIntArray& server_id_list)
{
	server_id_list.RemoveAll();

	CString str_server_id;
	get(KEY_SERVER_ID, str_server_id);

	int pos = 0;
	CString token = str_server_id.Tokenize("_", pos);
	while( token != "" )
	{
		server_id_list.Add(atoi(token));
		token = str_server_id.Tokenize("_", pos);
	}
}

bool CServerSettings::changeServerIP(LPCSTR ip, bool isServer)
{
	ciDEBUG(1, ("changeServerIP(%s)", ip));

	ciXProperties* aProp = ciXProperties::getInstance();

	aProp->set("COP.SERVER_SP01.IPADDRESS", ip);
	aProp->set("COP.SERVER_SP02.IPADDRESS", ip);
	aProp->set("COP.SERVER_SP03.IPADDRESS", ip);
	aProp->set("COP.SOCKET_PROXY_SERVER.IP", ip);
//	aProp->set("PS.CM_DB.SERVER", ip);
//	aProp->set("PS.MS_DB.COP_ODBC", ip);
//	aProp->set("PS.MYSQL_DB.SERVER", ip);

	ciString mux = ip;
	mux += ":14160";
	aProp->set("AGT.SERVER.MUX", mux.c_str());

	CUIntArray server_id_list;
	GetServerIDList(server_id_list);
	for( int i=0; i<server_id_list.GetCount(); i++ )
	{
		int server_id = server_id_list.GetAt(i);

		CString agt;
		agt.Format("%s:%d", ip, 14160 + server_id );

		CString name;
		name.Format("AGT.SERVER.%d", server_id );

		aProp->set(name, (LPCSTR)agt);
	}

	if( !aProp->write() )
	{
		setError(-1, "Fail to write ubc.properties file");
		ciERROR((_errString.c_str()));
		return false;
	}
	else
	{
		ciDEBUG(1,("properties file write succeed"));
	}

	ubcIni aIni("UBCConnect", _COP_CD("C:\\Project\\ubc\\config"), "data");

	aIni.set("ORB_NAMESERVICE","IP",	ip);
	aIni.set("ORB_NAMESERVICE","NAME",	"default");

	aIni.set("ORB_NAMESERVICE1","IP",	ip);
	aIni.set("ORB_NAMESERVICE1","NAME",	"default");
	aIni.set("ORB_NAMESERVICE1","PORT",	(ciLong)14109);
	aIni.set("ORB_NAMESERVICE1","USE",	(ciShort)1);

	aIni.set("AGENTMUX",	"IP", ip);
	aIni.set("TIMESYNCER",	"IP", ip);
	aIni.set("VNCMANAGER",	"IP", ip);

	ciString testUrlBuf = "http://";
			 testUrlBuf += ip;
			 testUrlBuf += ":";
			 testUrlBuf += ciIniUtil::getHttpPortStr();
	//		 testUrlBuf += ":8080/ubchome/NetworkTest.htm";
			 testUrlBuf += "/ubchome/NetworkTest.htm";

	ciString testUrl;
	ciIniUtil::HttpToHttps(testUrlBuf.c_str(), testUrl);

	aIni.set("DEVICEWIZARD", "TestURL", testUrl.c_str());

//	scEnv* env = scEnv::getInstance();
//	env->setUserEnv("NAMESERVER_HOST", ip);
	CEnvVar env(CEnvVar::env_scope::es_system, "NAMESERVER_HOST");
	env.set(ip);

	//createAnnounce.bat 파일을 새로 만듬
	ciString announceBat = _COP_CD("C:\\Project\\ubc\\util\\win32\\createAnnounce.bat");
	ciString announceSrc = _COP_CD("C:\\Project\\ubc\\util\\win32\\createAnnounce.src");
	ciString announceIp  = _COP_CD("C:\\Project\\ubc\\util\\win32\\createAnnounce.ip");
	ofstream out(announceIp.c_str(), ios_base::out);
	if( out.fail() )
	{	
		ciDEBUG(1,("%s open failed.\n", announceIp.c_str()));
	}
	else
	{
		out << "set NAMESERVER_HOST=" << ip << endl;
		out.close();
		ciString command = "cat " + announceIp + " " + announceSrc + " > " + announceBat;
		::system(command.c_str());
	}

	if( isServer )
	{
		cciPSFactory::clearInstance();  // DB 연결정보를 초기화해야 한다.
		return changePMO(true, "OD");  // 마지막으로 PMO 데이터를 교체한다.
	}

	return true;
}

bool CServerSettings::changePMO(bool force, LPCSTR mgrId)
{
	ciDEBUG(1, ("changePMO(%d,%s)",force,mgrId));

	CUIntArray server_id_list;
	GetServerIDList(server_id_list);

	// 0.  properties 화일에 올바르게 AGT.SERVER 셋팅이 있는지 먼저 살핀다.
	ciXProperties* aProp = ciXProperties::getInstance();

	ciXPropertyMap aMap;
	aProp->gets("AGT.SERVER.*", aMap, ciTrue);

	if( aMap.size() == 0 )
	{
		setError(-1, "No AGT.SERVER setting exist in ubc.properties file");
		ciERROR((_errString.c_str()));
		return false;
	}

	//1. PMO 객체가 있는지 본다.
	for( int i=0; i<server_id_list.GetCount(); i++ )
	{
		//
		CString pm_id;
		pm_id.Format("%d", server_id_list.GetAt(i));

		try
		{
			//
			cciObject aObj("UBC_PMO", 0, ciFalse);
			aObj.addItemAsKey("mgrId", new cciString(mgrId));
			aObj.addItemAsKey("pmId", new cciString((LPCSTR)pm_id));

			ciBoolean ret_val = aObj.get();
			//if(aObj.getPS()->isConnected())
			//{
			//	::AfxMessageBox("not conn");
			//}
			if( ret_val ) ret_val &= aObj.next();

			if( ret_val )
			{
				// 있다.
				ciDEBUG(1, ("PMO object already exist"));
				if(force)
				{
					// 지운다. (OD=OD/PMO=%d)
					if( aObj.remove() )
					{
						ciDEBUG(1, ("old PMO deleted"));
					}
					else
					{
						setError(-1, "Fail to delete old PMO object");
						ciERROR((_errString.c_str()));
						return false;
					}
				}
				else
				{
					ciDEBUG(1, ("PMO object already exist, don't need to create new one"));
					return true;
				}
			}
		}
		catch(...)
		{
			setError(-1, "Fail to delete old PMO object, because of Exception");
			ciERROR((_errString.c_str()));
			return false;
		}
	}

	// 없거나, 지워진 상태다.  새로 만든다.

	bool ret_val = true;

	int counter=0;
	ciXPropertyIterator itr;
	for( itr=aMap.begin(); itr!=aMap.end(); itr++ )
	{
		const char* pmId = itr->first.c_str();
		if( strcmp(pmId,"MUX") == 0 ) continue; // AGT.SERVER.MUX는 패스

		bool find = false;
		for( int i=0; i<server_id_list.GetCount(); i++ )
		{
			//
			CString pm_id;
			pm_id.Format("%d", server_id_list.GetAt(i));

			if( pm_id == pmId )
			{
				find = true;
				break;
			}
		}
		if( find == false ) continue;

		ciString ip,port;
		ciStringUtil::divide(itr->second, ':', &ip, &port);
		if( ip.empty() || port.empty() )
		{
			ciERROR(("%s is invalid AGT.SERVER data", itr->second.c_str()));
			continue;
		}

		try
		{
			cciObject aObj("UBC_PMO", 0, ciFalse);
			aObj.addItemAsKey("mgrId",	new cciString(mgrId));
			aObj.addItemAsKey("pmId",	new cciString(pmId));
			aObj.addItem("ipAddress",	new cciString(ip.c_str()));
			aObj.addItem("socketPort",	new cciString(port.c_str()));
			aObj.addItem("ftpId",		new cciString("server"));
			aObj.addItem("ftpPasswd",	new cciString("rjtlrl2009"));
			aObj.addItem("ftpPort",		new cciInteger16(21));

			if( !aObj.create() )
			{
				ret_val = false;

				char errBuf[1024]={0};
				sprintf(errBuf, "Fail to create PMO object(%s)", itr->second.c_str());
				setError(-1, errBuf);
				ciERROR((errBuf));
				continue;
			}
		}
		catch(...)
		{
			setError(-1, "Fail to create PMO object, because of Exception");
			ciERROR((_errString.c_str()));
			return false;
		}

		ciDEBUG(1, ("%s=%s pmo create succeed", itr->first.c_str(), itr->second.c_str()));
		counter++;
	}

	ciDEBUG(1, ("%d pmo created ", counter));
	return ret_val;
}

bool CServerSettings::changeFsProperties(LPCSTR lpszNameHostIP)
{
	// fs.properties
	// -ORBInitRef NameService=corbaloc:iiop:211.232.57.202:14109/NameService

	ciString config_path = _COP_CD("C:\\Project\\ubc\\config");
			 config_path += "\\tao\\fs.properties";

	FILE* fp = fopen(config_path.c_str(), "w");
	if( fp == NULL )
	{
		char errBuf[1024]={0};
		sprintf(errBuf, "Fail to open file(%s)", config_path.c_str());
		setError(errno, errBuf);
		ciERROR((_errString.c_str()));
		return false;
	}

	fprintf(fp, "-ORBInitRef NameService=corbaloc:iiop:%s:14109/NameService", lpszNameHostIP);
	fclose(fp);

	return true;
}
