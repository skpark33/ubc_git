//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by libServerSettings.rc
//
#define IDS_ERR_FAIL_TO_GET_IP_ADDR     6000
#define IDS_ERR_NOT_CONFIRM_NAMESERVER_HOST 6001
#define IDS_ERR_NOT_CONFIRM_PROPERTIES  6002
#define IDS_ERR_NOT_CONFIRM_INI         6003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
