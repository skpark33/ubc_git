#pragma once

using namespace std;

#include "../Installable/installable.h"


#define		KEY_CURRENT_IP_ADDRESS		("CurrentIPAddress")
//#define		KEY_IS_GLOBAL				("IsGlobal")
//#define		KEY_IS_MAIN_SERVER			("IsMainServer")
#define		KEY_SERVER_ID				("ServerID")
#define		KEY_NAMESERVER_HOST			("NameServerHost")


class CServerSettings : public installable
{
public:
	CServerSettings(void);
	~CServerSettings(void);

	virtual int get(LPCSTR key, CString& value);
	virtual int set(LPCSTR key, LPCSTR value);
	virtual CString get(LPCSTR key);

	virtual int prerequisite() { return 0; };
	virtual int test();
	virtual int	install();

	//void	RefreshAppList();
	//void	GetAllAppList(CStringArray& allAppList);
	//void	GetCurrentAppList(CStringArray& appList);
	//void	SetCurrentAppList(CStringArray& appList);

	void	GetAllSlaveIDList(CStringArray& server_list);

protected:
	CMapStringToString	m_mapKeyValues;
	//CStringArray		m_arAllAppList;
	//CStringArray		m_arCurrentAppList;
	CStringArray		m_arSlaveServerAppList;

	CString	m_strAppListName;
	LPCSTR	GetAppListName();
	void	GetSlaveName(CString& slaveName);
	bool	GetInfo();

	bool	IsIPConfirmed();
	bool	IsSameInProperties(LPCSTR name, LPCSTR value);
	bool	IsSameInIni(LPCSTR section, LPCSTR name, LPCSTR value);

	void	GetServerIDList(CUIntArray& server_id_list);
	bool	changeServerIP(LPCSTR ip, bool isServer);
	bool	changePMO(bool force, LPCSTR mgrId);

	bool	changeFsProperties(LPCSTR lpszNameHostIP);
};
