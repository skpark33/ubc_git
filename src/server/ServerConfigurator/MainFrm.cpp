// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "ServerConfigurator.h"

#include "MainFrm.h"

#include "SummaryDlg.h"

#include "common/libScratch/scratchUtil.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_MESSAGE(WM_COMPLETE_DIALOG_INIT, OnCompleteDialogInit)
	ON_MESSAGE(SHORTCUT_BUTTON_ID, OnShortcutButton)
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD /*| WS_VISIBLE*/ | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	SetMenu(NULL); // 메뉴 제거
	m_wndStatusBar.ShowWindow(SW_HIDE); // 상태바표시 않함

	SetWindowText(_T("Server Configurator"));

	char fullpath[MAX_PATH] = {0};
	::GetModuleFileName(NULL, fullpath, MAX_PATH);
	scratchUtil::getInstance()->AddToExceptionList(fullpath);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style &= (~(FWS_ADDTOTITLE | /*WS_MAXIMIZEBOX |*/ WS_VISIBLE));

	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers


void CMainFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = 640;
	lpMMI->ptMinTrackSize.y = 480;

	CFrameWnd::OnGetMinMaxInfo(lpMMI);
}

LRESULT CMainFrame::OnCompleteDialogInit(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->PostMessage(WM_COMPLETE_DIALOG_INIT, wParam, lParam);
}

LRESULT CMainFrame::OnShortcutButton(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->PostMessage(SHORTCUT_BUTTON_ID, wParam, lParam);
}

