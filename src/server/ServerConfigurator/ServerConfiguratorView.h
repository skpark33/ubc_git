// ServerConfiguratorView.h : interface of the CServerConfiguratorView class
//


#pragma once
//#include "afxcmn.h"
//#include "afxwin.h"

#include "TabCtrlEx.h"
#include "ReposControl2.h"


class CServerConfiguratorView : public CFormView
{
protected: // create from serialization only
	CServerConfiguratorView();
	DECLARE_DYNCREATE(CServerConfiguratorView)

public:
	enum{ IDD = IDD_SERVERCONFIGURATOR_FORM };

// Attributes
public:
	CServerConfiguratorDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CServerConfiguratorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	bool			m_bSubDlgAllInit;
	CBrush			m_brushBG;

	CSummaryDlg*	m_pSummaryDlg;

	CReposControl2	m_reposControl;

	void	MoveSubDialog();

public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnTcnSelchangeTabStep(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonBackStep();
	afx_msg void OnBnClickedButtonNextStep();
	afx_msg void OnBnClickedButtonExit();
	afx_msg LRESULT OnCompleteDialogInit(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnShortcutButton(WPARAM wParam, LPARAM lParam);

	CTabCtrlEx	m_tcStep;

	CButton		m_btnBackStep;
	CButton		m_btnNextStep;
	CButton		m_btnExit;

	void		UpdateControls();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
};

#ifndef _DEBUG  // debug version in ServerConfiguratorView.cpp
inline CServerConfiguratorDoc* CServerConfiguratorView::GetDocument() const
   { return reinterpret_cast<CServerConfiguratorDoc*>(m_pDocument); }
#endif


#define		ADD_DIALOG(TITLE_ID, DLG_CLASS, DLG_ID)												\
											GetDocument()->AddDialog(							\
												::LoadString(TITLE_ID),							\
												new DLG_CLASS(GetDocument(), (CWnd*)&m_tcStep),	\
												DLG_ID );										\
											m_tcStep.InsertItem(tab_idx++, ::LoadString(TITLE_ID));
//											item.strTitle = TITLE;								\
//											item.pDialog = new DLG_CLASS((CWnd*)&m_tcStep);		\
//											item.nIDTemplate = DLG_ID;							\
//											m_tcStep.InsertItem(tab_idx++, item.strTitle);		\
//											m_arDlgItem.Add(item);
