// TrdpartySettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "TrdpartySettingsDlg.h"


#define		TEST_NET_FRAMEWORK_4	(10000)
#define		TEST_VC2005_EXPRESS		(10001)
#define		TEST_VC2010_REDIST_X86	(10002)
#define		TEST_VC2010_REDIST_X64	(10003)
#define		TEST_MYSQL_SERVER		(10004)
#define		TEST_MYSQL_ODBC_X86		(10005)
#define		TEST_MYSQL_ODBC_X64		(10006)


// CTrdpartySettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTrdpartySettingsDlg, CSubDlg)

CTrdpartySettingsDlg::CTrdpartySettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*CTrdpartySettingsDlg::IDD,*/ pParent)
{
	m_bInstallNetFramework4		= false;
	m_bInstallVC2005Express		= false;
	m_bInstallVC2010Redist_x86	= false;
	m_bInstallVC2010Redist_x64	= false;
	m_bInstallMySQLServer		= false;
	m_bInstallMySQLODBC_x86		= false;
	m_bInstallMySQLODBC_x64		= false;
}

CTrdpartySettingsDlg::~CTrdpartySettingsDlg()
{
}

void CTrdpartySettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_INSTALL_NET_FRAMEWORK_4, m_chkInstallNetFramework4);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_NET_FRAMEWORK_4, m_btnInstallNetFramework4);
	DDX_Control(pDX, IDC_CHECK_INSTALL_VC2005_EXPRESS, m_chkInstallVC2005Express);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_VC2005_EXPRESS, m_btnInstallVC2005Express);
	DDX_Control(pDX, IDC_CHECK_INSTALL_VC2010_REDIST_X86, m_chkInstallVC2010Redist_x86);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_VC2010_REDIST_X86, m_btnInstallVC2010Redist_x86);
	DDX_Control(pDX, IDC_CHECK_INSTALL_VC2010_REDIST_X64, m_chkInstallVC2010Redist_x64);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_VC2010_REDIST_X64, m_btnInstallVC2010Redist_x64);
	DDX_Control(pDX, IDC_CHECK_INSTALL_MYSQL_SERVER, m_chkInstallMySQLServer);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_MYSQL_SERVER, m_btnInstallMySQLServer);
	DDX_Control(pDX, IDC_CHECK_INSTALL_MYSQL_ODBC_X86, m_chkInstallMySQLODBC_x86);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_MYSQL_ODBC_X86, m_btnInstallMySQLODBC_x86);
	DDX_Control(pDX, IDC_CHECK_INSTALL_MYSQL_ODBC_X64, m_chkInstallMySQLODBC_x64);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_MYSQL_ODBC_X64, m_btnInstallMySQLODBC_x64);
}


BEGIN_MESSAGE_MAP(CTrdpartySettingsDlg, CSubDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_INSTALL_NET_FRAMEWORK_4, &CTrdpartySettingsDlg::OnBnClickedCheckInstallNetFramework4)
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_NET_FRAMEWORK_4, &CTrdpartySettingsDlg::OnBnClickedButtonInstallNetFramework4)
	ON_BN_CLICKED(IDC_CHECK_INSTALL_VC2005_EXPRESS, &CTrdpartySettingsDlg::OnBnClickedCheckInstallVc2005Express)
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_VC2005_EXPRESS, &CTrdpartySettingsDlg::OnBnClickedButtonInstallVc2005Express)
	ON_BN_CLICKED(IDC_CHECK_INSTALL_VC2010_REDIST_X86, &CTrdpartySettingsDlg::OnBnClickedCheckInstallVC2010Redist_x86)
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_VC2010_REDIST_X86, &CTrdpartySettingsDlg::OnBnClickedButtonInstallVC2010Redist_x86)
	ON_BN_CLICKED(IDC_CHECK_INSTALL_VC2010_REDIST_X64, &CTrdpartySettingsDlg::OnBnClickedCheckInstallVC2010Redist_x64)
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_VC2010_REDIST_X64, &CTrdpartySettingsDlg::OnBnClickedButtonInstallVC2010Redist_x64)
	ON_BN_CLICKED(IDC_CHECK_INSTALL_MYSQL_SERVER, &CTrdpartySettingsDlg::OnBnClickedCheckInstallMySQLServer)
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_MYSQL_SERVER, &CTrdpartySettingsDlg::OnBnClickedButtonInstallMySQLServer)
	ON_BN_CLICKED(IDC_CHECK_INSTALL_MYSQL_ODBC_X86, &CTrdpartySettingsDlg::OnBnClickedCheckInstallMySQLODBC_x86)
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_MYSQL_ODBC_X86, &CTrdpartySettingsDlg::OnBnClickedButtonInstallMySQLODBC_x86)
	ON_BN_CLICKED(IDC_CHECK_INSTALL_MYSQL_ODBC_X64, &CTrdpartySettingsDlg::OnBnClickedCheckInstallMySQLODBC_x64)
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_MYSQL_ODBC_X64, &CTrdpartySettingsDlg::OnBnClickedButtonInstallMySQLODBC_x64)
END_MESSAGE_MAP()


// CTrdpartySettingsDlg 메시지 처리기입니다.

BOOL CTrdpartySettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	AddControl( m_chkInstallNetFramework4.GetSafeHwnd() );
	AddControl( m_btnInstallNetFramework4.GetSafeHwnd() );
	AddControl( m_chkInstallVC2005Express.GetSafeHwnd() );
	AddControl( m_btnInstallVC2005Express.GetSafeHwnd() );
	AddControl( m_chkInstallVC2010Redist_x86.GetSafeHwnd() );
	AddControl( m_btnInstallVC2010Redist_x86.GetSafeHwnd() );
	AddControl( m_chkInstallVC2010Redist_x64.GetSafeHwnd() );
	AddControl( m_btnInstallVC2010Redist_x64.GetSafeHwnd() );
	AddControl( m_chkInstallMySQLServer.GetSafeHwnd() );
	AddControl( m_btnInstallMySQLServer.GetSafeHwnd() );
	AddControl( m_chkInstallMySQLODBC_x86.GetSafeHwnd() );
	AddControl( m_btnInstallMySQLODBC_x86.GetSafeHwnd() );
	AddControl( m_chkInstallMySQLODBC_x64.GetSafeHwnd() );
	AddControl( m_btnInstallMySQLODBC_x64.GetSafeHwnd() );

	m_btnInstallNetFramework4.Init( &m_bInstallNetFramework4, &m_chkInstallNetFramework4);
	m_btnInstallVC2005Express.Init( &m_bInstallVC2005Express, &m_chkInstallVC2005Express );
	m_btnInstallVC2010Redist_x86.Init( &m_bInstallVC2010Redist_x86, &m_chkInstallVC2010Redist_x86);
	m_btnInstallVC2010Redist_x64.Init( &m_bInstallVC2010Redist_x64, &m_chkInstallVC2010Redist_x64);
	m_btnInstallMySQLServer.Init( &m_bInstallMySQLServer, &m_chkInstallMySQLServer);
	m_btnInstallMySQLODBC_x86.Init( &m_bInstallMySQLODBC_x86, &m_chkInstallMySQLODBC_x86);
	m_btnInstallMySQLODBC_x64.Init( &m_bInstallMySQLODBC_x64, &m_chkInstallMySQLODBC_x64);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CTrdpartySettingsDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case TEST_NET_FRAMEWORK_4:
		if( m_TrdPartyInstall.test(KEY_TRDPARTY_NET_FRAMEWORK_4) )
		{
			KillTimer(TEST_NET_FRAMEWORK_4);
			m_bInstallNetFramework4 = true;
			m_chkInstallNetFramework4.SetCheck(BST_UNCHECKED);
			CheckInstallComplete();
			UpdateControls();
		}
		break;
	case TEST_VC2005_EXPRESS:
		if( m_TrdPartyInstall.test(KEY_TRDPARTY_VC2005_EXPRESS) )
		{
			KillTimer(TEST_VC2005_EXPRESS);
			m_bInstallVC2005Express = true;
			m_chkInstallVC2005Express.SetCheck(BST_UNCHECKED);
			CheckInstallComplete();
			UpdateControls();
		}
		break;
	case TEST_VC2010_REDIST_X86:
		if( m_TrdPartyInstall.test(KEY_TRDPARTY_VC2010_REDIST_X86) )
		{
			KillTimer(TEST_VC2010_REDIST_X86);
			m_bInstallVC2010Redist_x86 = true;
			m_chkInstallVC2010Redist_x86.SetCheck(BST_UNCHECKED);
			CheckInstallComplete();
			UpdateControls();
		}
		break;
	case TEST_VC2010_REDIST_X64:
		if( m_TrdPartyInstall.test(KEY_TRDPARTY_VC2010_REDIST_X64) )
		{
			KillTimer(TEST_VC2010_REDIST_X64);
			m_bInstallVC2010Redist_x64 = true;
			m_chkInstallVC2010Redist_x64.SetCheck(BST_UNCHECKED);
			CheckInstallComplete();
			UpdateControls();
		}
		break;
	case TEST_MYSQL_SERVER:
		if( m_TrdPartyInstall.test(KEY_TRDPARTY_MYSQL_SERVER) )
		{
			KillTimer(TEST_MYSQL_SERVER);
			m_bInstallMySQLServer = true;
			m_chkInstallMySQLServer.SetCheck(BST_UNCHECKED);
			CheckInstallComplete();
			UpdateControls();
		}
		break;
	case TEST_MYSQL_ODBC_X86:
		if( m_TrdPartyInstall.test(KEY_TRDPARTY_MYSQL_ODBC_X86) )
		{
			KillTimer(TEST_MYSQL_ODBC_X86);
			m_bInstallMySQLODBC_x86 = true;
			m_chkInstallMySQLODBC_x86.SetCheck(BST_UNCHECKED);
			CheckInstallComplete();
			UpdateControls();
		}
		break;
	case TEST_MYSQL_ODBC_X64:
		if( m_TrdPartyInstall.test(KEY_TRDPARTY_MYSQL_ODBC_X64) )
		{
			KillTimer(TEST_MYSQL_ODBC_X64);
			m_bInstallMySQLODBC_x64 = true;
			m_chkInstallMySQLODBC_x64.SetCheck(BST_UNCHECKED);
			CheckInstallComplete();
			UpdateControls();
		}
		break;
	}

	CSubDlg::OnTimer(nIDEvent);
}

void CTrdpartySettingsDlg::OnBnClickedCheckInstallNetFramework4()
{
	UpdateControls();
}

void CTrdpartySettingsDlg::OnBnClickedButtonInstallNetFramework4()
{
	BeginWaitCursor();

	m_TrdPartyInstall.install(KEY_TRDPARTY_NET_FRAMEWORK_4);

	EndWaitCursor();

	SetTimer(TEST_NET_FRAMEWORK_4, 1000, NULL);
}

void CTrdpartySettingsDlg::OnBnClickedCheckInstallVc2005Express()
{
	UpdateControls();
}

void CTrdpartySettingsDlg::OnBnClickedButtonInstallVc2005Express()
{
	BeginWaitCursor();

	m_TrdPartyInstall.install(KEY_TRDPARTY_VC2005_EXPRESS);

	EndWaitCursor();

	SetTimer(TEST_VC2005_EXPRESS, 1000, NULL);
}

void CTrdpartySettingsDlg::OnBnClickedCheckInstallVC2010Redist_x86()
{
	UpdateControls();
}

void CTrdpartySettingsDlg::OnBnClickedButtonInstallVC2010Redist_x86()
{
	BeginWaitCursor();

	m_TrdPartyInstall.install(KEY_TRDPARTY_VC2010_REDIST_X86);

	EndWaitCursor();

	SetTimer(TEST_VC2010_REDIST_X86, 1000, NULL);
}

void CTrdpartySettingsDlg::OnBnClickedCheckInstallVC2010Redist_x64()
{
	UpdateControls();
}

void CTrdpartySettingsDlg::OnBnClickedButtonInstallVC2010Redist_x64()
{
	BeginWaitCursor();

	m_TrdPartyInstall.install(KEY_TRDPARTY_VC2010_REDIST_X64);

	EndWaitCursor();

	SetTimer(TEST_VC2010_REDIST_X64, 1000, NULL);
}

void CTrdpartySettingsDlg::OnBnClickedCheckInstallMySQLServer()
{
	UpdateControls();
}

void CTrdpartySettingsDlg::OnBnClickedButtonInstallMySQLServer()
{
	BeginWaitCursor();

	m_TrdPartyInstall.install(KEY_TRDPARTY_MYSQL_SERVER);

	EndWaitCursor();

	SetTimer(TEST_MYSQL_SERVER, 1000, NULL);
}

void CTrdpartySettingsDlg::OnBnClickedCheckInstallMySQLODBC_x86()
{
	UpdateControls();
}

void CTrdpartySettingsDlg::OnBnClickedButtonInstallMySQLODBC_x86()
{
	BeginWaitCursor();

	m_TrdPartyInstall.install(KEY_TRDPARTY_MYSQL_ODBC_X86);

	EndWaitCursor();

	SetTimer(TEST_MYSQL_ODBC_X86, 1000, NULL);
}

void CTrdpartySettingsDlg::OnBnClickedCheckInstallMySQLODBC_x64()
{
	UpdateControls();
}

void CTrdpartySettingsDlg::OnBnClickedButtonInstallMySQLODBC_x64()
{
	BeginWaitCursor();

	m_TrdPartyInstall.install(KEY_TRDPARTY_MYSQL_ODBC_X64);

	EndWaitCursor();

	SetTimer(TEST_MYSQL_ODBC_X64, 1000, NULL);
}

bool CTrdpartySettingsDlg::RunInit()
{
	bool ret_value = true;

	//
	if( m_TrdPartyInstall.test(KEY_TRDPARTY_NET_FRAMEWORK_4) )
	{
		m_bInstallNetFramework4 = true;
	}
	else
	{
		m_chkInstallNetFramework4.SetCheck(BST_CHECKED);
	}

	//
	if( m_TrdPartyInstall.test(KEY_TRDPARTY_VC2005_EXPRESS) )
	{
		m_bInstallVC2005Express = true;
	}
	else
	{
		m_chkInstallVC2005Express.SetCheck(BST_CHECKED);
	}

	//
	if( m_TrdPartyInstall.test(KEY_TRDPARTY_VC2010_REDIST_X86) )
	{
		m_bInstallVC2010Redist_x86 = true;
	}
	else
	{
		m_chkInstallVC2010Redist_x86.SetCheck(BST_CHECKED);
	}

	//
	if( m_TrdPartyInstall.test(KEY_TRDPARTY_VC2010_REDIST_X64) )
	{
		m_bInstallVC2010Redist_x64 = true;
	}
	else
	{
		m_chkInstallVC2010Redist_x64.SetCheck(BST_CHECKED);
	}

	//
	if( m_TrdPartyInstall.test(KEY_TRDPARTY_MYSQL_SERVER) )
	{
		m_bInstallMySQLServer = true;
	}
	else
	{
		m_chkInstallMySQLServer.SetCheck(BST_CHECKED);
	}

	//
	if( m_TrdPartyInstall.test(KEY_TRDPARTY_MYSQL_ODBC_X86) )
	{
		m_bInstallMySQLODBC_x86 = true;
	}
	else
	{
		m_chkInstallMySQLODBC_x86.SetCheck(BST_CHECKED);
	}

	//
	if( m_TrdPartyInstall.test(KEY_TRDPARTY_MYSQL_ODBC_X64) )
	{
		m_bInstallMySQLODBC_x64 = true;
	}
	else
	{
		m_chkInstallMySQLODBC_x64.SetCheck(BST_CHECKED);
	}

	return ret_value;
}

void CTrdpartySettingsDlg::UpdateControls()
{
	m_btnInstallNetFramework4.UpdateControl();
	m_btnInstallVC2005Express.UpdateControl();
	m_btnInstallVC2010Redist_x86.UpdateControl();
	m_btnInstallVC2010Redist_x64.UpdateControl();
	m_btnInstallMySQLServer.UpdateControl();
	m_btnInstallMySQLODBC_x86.UpdateControl();
	m_btnInstallMySQLODBC_x64.UpdateControl();
}

int CTrdpartySettingsDlg::CheckInstallComplete()
{
	m_bInstallComplete = true;

	if( m_bInstallNetFramework4 == false || 
		m_bInstallVC2005Express == false || 
		m_bInstallVC2010Redist_x86 == false || 
		m_bInstallVC2010Redist_x64 == false /*|| 
		m_bInstallMySQLServer == false || 
		m_bInstallMySQLODBC_x86 == false ||
		m_bInstallMySQLODBC_x64 == false*/ )
	{
		m_bInstallComplete = false;
	}

	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}
