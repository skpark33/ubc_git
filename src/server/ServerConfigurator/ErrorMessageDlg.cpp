// ErrorMessageDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "ErrorMessageDlg.h"


// CErrorMessageDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CErrorMessageDlg, CDialog)

CErrorMessageDlg::CErrorMessageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CErrorMessageDlg::IDD, pParent)
{
	m_hIco = NULL;
	m_nType = 0;
}

CErrorMessageDlg::CErrorMessageDlg(LPCTSTR lpszMessage, int nErrorCode, LPCTSTR lpszErrMsg, UINT nType)
:	CDialog(CErrorMessageDlg::IDD)
,	m_hIco ( NULL )
,	m_strMessage ( lpszMessage )
,	m_nErrorCode ( nErrorCode )
,	m_strErrorMessage ( lpszErrMsg )
,	m_nType ( nType )
{
	CString lines = lpszMessage;
	lines.Replace("\r", "");

	CStringArray msg_list;
	int pos = 0;
	CString line;
	do
	{
		line = lines.Tokenize("\n", pos);
		if( line == "" ) break;
		msg_list.Add(line);
	} while(1);

	if( msg_list.GetCount() == 1 )
	{
		m_strMessage.Insert(0, "\r\n");
	}
	else if( msg_list.GetCount() == 2 )
	{
		m_strMessage.Format("%s\r\n\r\n%s", msg_list[0], msg_list[1]);
	}
	else if( msg_list.GetCount() == 3 )
	{
		m_strMessage.Format("%s\r\n%s\r\n%s", msg_list[0], msg_list[1], msg_list[2]);
	}
}

CErrorMessageDlg::CErrorMessageDlg(UINT nMsgID, int nErrorCode, LPCTSTR lpszErrMsg, UINT nType)
:	CDialog(CErrorMessageDlg::IDD)
,	m_hIco ( NULL )
,	m_nErrorCode ( nErrorCode )
,	m_strErrorMessage ( lpszErrMsg )
,	m_nType ( nType )
{
	m_strMessage.LoadString(nMsgID);

	CErrorMessageDlg(m_strMessage, nErrorCode, lpszErrMsg, nType);
}

CErrorMessageDlg::~CErrorMessageDlg()
{
}

void CErrorMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ICON, m_ico);
	DDX_Control(pDX, IDC_STATIC_MESSAGE, m_stcMessage);
	DDX_Control(pDX, IDC_EDIT_ERROR_CODE, m_editErrorCode);
	DDX_Control(pDX, IDC_EDIT_ERROR_MESSAGE, m_editErrorMessage);
	DDX_Control(pDX, IDCANCEL, m_btnClose);
}


BEGIN_MESSAGE_MAP(CErrorMessageDlg, CDialog)
	ON_WM_GETMINMAXINFO()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CErrorMessageDlg 메시지 처리기입니다.

BOOL CErrorMessageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_repos.SetParent(this);
	m_repos.AddControl((CWnd*)&m_stcMessage, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editErrorCode, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editErrorMessage, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnClose, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	switch( m_nType & MB_ICONMASK )
	{
	case MB_ICONSTOP:		m_hIco=::LoadIcon(NULL, IDI_ERROR);	break;
	case MB_ICONWARNING:	m_hIco=::LoadIcon(NULL, IDI_WARNING); break;
	case MB_ICONQUESTION:	m_hIco=::LoadIcon(NULL, IDI_QUESTION); break;
	default:				m_hIco=::LoadIcon(NULL, IDI_INFORMATION); break;
	}

	if( m_hIco ) m_ico.SetIcon(m_hIco);
	m_stcMessage.SetWindowText(m_strMessage);
	m_editErrorCode.SetWindowText(::ToString(m_nErrorCode));
	m_editErrorMessage.SetWindowText(m_strErrorMessage);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CErrorMessageDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = 300;
	lpMMI->ptMinTrackSize.y = 200;

	CDialog::OnGetMinMaxInfo(lpMMI);
}

void CErrorMessageDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )
	{
		m_repos.MoveControl(TRUE);
	}
}
