#pragma once

using namespace std;

#include "../Installable/installable.h"

#define		KEY_IS_GLOBAL				("IsGlobal")

class CGlobalOption : public installable
{
public:
	CGlobalOption(void);
	~CGlobalOption(void);

	virtual int prerequisite() { return 0; };
	virtual int test();
	virtual int	install();

	virtual int get(LPCSTR key, CString& value);
	virtual int set(LPCSTR key, LPCSTR value);
	virtual CString get(LPCSTR key);

protected:
	CMapStringToString		m_mapKeyValues;

};
