#include "StdAfx.h"
#include "GlobalOption.h"
//#include "resource.h"

#include "ci/libDebug/ciDebug.h"
#include "common/libCommon/ubcIni.h"
//#include "common/libScratch/scratchUtil.h"
#include "common/libCommon/ubcUtil.h"

ciSET_DEBUG(10, "CGlobalOption");


CGlobalOption::CGlobalOption(void)
{
}

CGlobalOption::~CGlobalOption(void)
{
}

int CGlobalOption::get(LPCSTR key, CString& value)
{
	value = "";

	if( m_mapKeyValues.Lookup(key, value) )
	{
		return TRUE;
	}

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);
	return FALSE;
}

int CGlobalOption::set(LPCSTR key, LPCSTR value)
{
	m_mapKeyValues.SetAt(key, value);
	return TRUE;
}

CString CGlobalOption::get(LPCSTR key)
{
	CString value="";
	m_mapKeyValues.Lookup(key, value);
	return value;
}

int CGlobalOption::test()
{
	ciDEBUG(1,("test()"));

	clearError();

	// IsGlobal üũ - UBCVariables.ini-ROOT-GLOBAL

	ubcIni aIni("UBCVariables", _COP_CD("C:\\Project\\ubc\\config"), "data");

	ciString is_global = "";
	aIni.get("ROOT","GLOBAL", is_global);
	ciDEBUG(1, ("GLOBAL=%s", is_global.c_str()) );

	set(KEY_IS_GLOBAL, is_global.c_str());

	ciDEBUG(1,("test() end"));

	if( is_global.length() == 0 )
	{
		setError(-1, "no value [ROOT]GLOBAL");
		return FALSE;
	}

	return TRUE;
}

int	CGlobalOption::install()
{
	ciDEBUG(1,("install()"));

	clearError();

	// set is_global
	ubcIni aIni("UBCVariables", _COP_CD("C:\\Project\\ubc\\config"), "data");

	aIni.set("ROOT", "GLOBAL", get(KEY_IS_GLOBAL));
	ciDEBUG(1, ("GLOBAL=%s", get(KEY_IS_GLOBAL)) );

	ciDEBUG(1, ("install() end"));
	return TRUE;
}
