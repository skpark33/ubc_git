// TabCtrlEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "TabCtrlEx.h"


// CTabCtrlEx

IMPLEMENT_DYNAMIC(CTabCtrlEx, CTabCtrl)

CTabCtrlEx::CTabCtrlEx()
{
	m_font.CreatePointFont(9*10, "MS Shell Dlg");
}

CTabCtrlEx::~CTabCtrlEx()
{
}


BEGIN_MESSAGE_MAP(CTabCtrlEx, CTabCtrl)
END_MESSAGE_MAP()



// CTabCtrlEx 메시지 처리기입니다.



void CTabCtrlEx::PreSubclassWindow()
{
	HKEY hKey;
	char szVal[255]={0};
	DWORD dwType = REG_SZ;
	DWORD dwSize=255;
	CString strRegKey = "Software\\Microsoft\\Windows\\CurrentVersion\\ThemeManager";

	LONG lRet = RegOpenKeyEx(HKEY_CURRENT_USER, strRegKey, 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey); // 32bit OS일경우 KEY_WOW64_32KEY
	if( lRet == ERROR_SUCCESS )
	{
		//
		lRet = RegQueryValueEx(hKey, "ThemeActive", NULL, &dwType, (BYTE*)szVal, &dwSize);  
		if( atoi(szVal)==0 )
		{
			ModifyStyle(0, TCS_OWNERDRAWFIXED);
		}

		RegCloseKey(hKey);
	}

	CTabCtrl::PreSubclassWindow();
}

void CTabCtrlEx::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	COLORREF select_color = ::GetSysColor(COLOR_BTNHIGHLIGHT);
	COLORREF deselect_color = ::GetSysColor(COLOR_BTNFACE);

	CRect rect = lpDrawItemStruct->rcItem; 
	rect.DeflateRect(0,2,3,0);

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	if (!pDC) return;

	int nSavedDC = pDC->SaveDC();

	pDC->SelectObject(&m_font);
	pDC->SetBkMode(TRANSPARENT);
	if( lpDrawItemStruct->itemID == GetCurSel() )
		pDC->FillSolidRect(rect, select_color);
	else
		pDC->FillSolidRect(rect, deselect_color);

	char buf[1024];
	TCITEM item = {0};
	item.mask = TCIF_TEXT;
	item.pszText = buf;
	item.cchTextMax = 1024;
	GetItem(lpDrawItemStruct->itemID, &item);

	pDC->DrawText(buf, strlen(buf), rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER );

	pDC->RestoreDC(nSavedDC);
}
