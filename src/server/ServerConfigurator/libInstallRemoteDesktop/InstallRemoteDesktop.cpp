#include "StdAfx.h"
#include "InstallRemoteDesktop.h"
#include <ci/libDebug/ciDebug.h>


ciSET_DEBUG(10, "libInstallRemoteDesktop");


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기본생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallRemoteDesktop::CInstallRemoteDesktop()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallRemoteDesktop::~CInstallRemoteDesktop()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우즈 원격데스크톱이 사용하도록 설정되었는지 확인한다. \n
/// @return <형: int> \n
///			<값: 설명> \n
///			<값: 설명> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallRemoteDesktop::test()
{
	ciDEBUG(1, ("begin Remote desktop test"));
	clearError();

	LONG lRet;
	HKEY hKey;
	DWORD dwVal;
	DWORD dwType = REG_DWORD;
	DWORD dwSize = sizeof(DWORD);
	CString strRegKey = "SYSTEM\\CurrentControlSet\\Control\\Terminal Server";

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, strRegKey, 0, KEY_ALL_ACCESS, &hKey);
	if( lRet != ERROR_SUCCESS )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to open reg key(%s)", strRegKey);
		ciDEBUG(1, (szBuf));
		setError(lRet, szBuf);
		ciDEBUG(1, ("Fail to Remote desktop test"));
		return FALSE;
	}//if

	lRet = RegQueryValueEx(hKey, "fDenyTSConnections", NULL, &dwType, (BYTE*)&dwVal, &dwSize);  
	RegCloseKey(hKey);
	if( lRet != ERROR_SUCCESS )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to query reg value(fDenyTSConnections)");
		ciDEBUG(1, (szBuf));
		setError(lRet, szBuf);
		ciDEBUG(1, ("Fail to Remote desktop test"));
		return FALSE;
	}//if

	if( dwVal != 0 )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Not set terminal service");
		ciDEBUG(1, (szBuf));
		setError(ERR_UNINSTALLED, szBuf);
		return FALSE;
	}//if

	ciDEBUG(1, ("Already set tetminal service"));
	ciDEBUG(1, ("end Remote desktop test"));
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우즈 원격데스크톱을 사용하도록 설정한다. \n
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallRemoteDesktop::install()
{
	ciDEBUG(1, ("begin Remote desktop install"));
	clearError();

	HKEY hKey;
	LONG lRet;
	DWORD dwVal;
	CString strRegKey = "SYSTEM\\CurrentControlSet\\Control\\Terminal Server";

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, strRegKey, 0, KEY_WRITE, &hKey);
	if( lRet != ERROR_SUCCESS )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to open reg key(%s)", strRegKey);
		ciDEBUG(1, (szBuf));
		setError(lRet, szBuf);
		ciDEBUG(1, ("Fail to Remote desktop install"));
		return FALSE;
	}//if

	//fDenyTSConnections = 1(허용안함), 0(허용함)
	dwVal = 0;
	lRet = RegSetValueEx(hKey, "fDenyTSConnections" ,0, REG_DWORD, (BYTE*)&dwVal, sizeof(dwVal));  
	if( lRet != ERROR_SUCCESS )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to set reg value(fDenyTSConnections)");
		ciDEBUG(1, (szBuf));
		setError(lRet, szBuf);
		ciDEBUG(1, ("Fail to Remote desktop install"));
		return FALSE;
	}//if
	::system("netsh firewall set service type = remotedesktop mode = enable");

/*
	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);

	CString strPath;
	strPath.Format("%s%s\\Remotedesktop.bat", cDrive, cPath);
	//배치파일 생성
	if(!CreateBatchFile(strPath))
	{
		char szBuf[1024] = { 0x00 };
		sprintf(szBuf, "Fail to create batch fille : [%s]", strPath);
		ciDEBUG(1, (szBuf));
		setError(ERR_CREATE_BAT, szBuf);
		ciDEBUG(1, ("end Remote desktop install"));
		return FALSE;
	}//if

	//배치파일 실행
	STARTUPINFO         sINFO = {0};
	PROCESS_INFORMATION pINFO = {0};

	sINFO.cb = sizeof( sINFO );

	if(!CreateProcess(NULL, (LPSTR)(LPCSTR)strPath, NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &sINFO, &pINFO ) ) // CREATE_NO_WINDOW = 윈도우없이 실행
	{
		char szBuf[1024] = { 0x00 };
		sprintf(szBuf, "Fail to run batch fille : [%s]", strPath);
		ciDEBUG(1, (szBuf));
		setError(ERR_RUN_BAT, szBuf);
		ciDEBUG(1, ("end Remote desktop install"));
		return FALSE;
	}//if

	//실행종료 대기
	WaitForSingleObject(pINFO.hProcess, INFINITE);

	CloseHandle(pINFO.hProcess);
	CloseHandle(pINFO.hThread);
*/
	ciDEBUG(1, ("Success install Remote desktop"));
	ciDEBUG(1, ("end Remote desktop install"));
	return TRUE;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우즈 원격데스크톱을 사용하도록하는 레지스트리를 수정하는 배치파일을 만든다. \n
/// @param (CString) strPath : (in) 배치파일을 생성할 경로
/// @return <형: bool> \n
///			<false: 성공> \n
///			<true: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CInstallRemoteDesktop::CreateBatchFile(CString strPath)
{
	ciDEBUG(1, ("begin Create batch file"));

	CFileStatus fs;
	if(CFile::GetStatus(strPath, fs) == TRUE)
	{
		ciDEBUG(1, ("Aleady exist Remotedesktop.bat"));
		return true;
	}//if

	CFile fileBat;
	if(!fileBat.Open(strPath, CFile::modeCreate | CFile::modeWrite))
	{
		ciDEBUG(1, ("Fail to create batch file"));
		ciDEBUG(1, ("end Create batch file"));
		return false;
	}//if

	
	CString strText = "@echo off\r\nREM Enable Remotedesktop service\r\n";
	strText += "REG ADD \"HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Terminal Server\" /v fDenyTSConnections /t REG_DWORD /d 0x00 /f";
	fileBat.Write(strText, strlen(strText));
	fileBat.Close();

	ciDEBUG(1, ("end Create batch file"));
	return true;
}
*/