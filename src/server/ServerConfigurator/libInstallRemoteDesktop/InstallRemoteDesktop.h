/************************************************************************************/
/*! @file InstallRemoteDesktop.h
	@brief 윈도우즈 원격데스크톱을 사용하도록 설정하는 클래스 선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2012/07/23\n
	▶ 참고사항:
		
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2012/07/23:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#pragma once
using namespace std;

#include "../Installable/installable.h"



//#define ERR_CREATE_BAT		-1			//bat 파일 생성 오류
//#define ERR_RUN_BAT			-2			//bat 파일 실행 오류
#ifndef _ERR_CODDE
	#define _ERR_CODDE
	#define ERR_TEST_FAIL			-1			//테스트 과정 실패
	#define ERR_UNINSTALLED			-2			//테스트 결과 설치가 되어있지 않음
	#define ERR_FAIL_INSTALL		-3			//설치 실패
	#define ERR_FAIL_UNINSTALL		-4			//uninstall fail
	#define ERR_ETC					-10			//기타 실패
#endif


//! 윈도우즈 원격데스크톱을 사용하도록 설정하는 클래스
/*!
*/
class CInstallRemoteDesktop : public installable
{
public:
	CInstallRemoteDesktop(void);				///<기본 생성자
	virtual ~CInstallRemoteDesktop(void);		///<소멸자

	virtual int prerequisite()	{ setError(-1,"NO Service"); return 0; };
	virtual int test();								///<윈도우즈 원격데스크톱이 사용하도록 설정되었는지 확인한다.
	virtual int	install();							///<윈도우즈 원격데스크톱을 사용하도록 설정한다.

private:
	//bool		CreateBatchFile(CString strPath);	///<윈도우즈 원격데스크톱을 사용하도록하는 레지스트리를 수정하는 배치파일을 만든다.
};

