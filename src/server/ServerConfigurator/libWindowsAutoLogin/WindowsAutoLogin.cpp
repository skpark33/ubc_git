#include "StdAfx.h"
#include "WindowsAutoLogin.h"
//#include "resource.h"

#include "ci/libDebug/ciDebug.h"

ciSET_DEBUG(10, "CWindowsAutoLogin");

CWindowsAutoLogin::CWindowsAutoLogin(void)
{
}

CWindowsAutoLogin::~CWindowsAutoLogin(void)
{
}

int CWindowsAutoLogin::get(LPCSTR key, CString& value)
{
	value = "";

	if( m_mapKeyValues.Lookup(key, value) ) return TRUE;

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);

	return FALSE;
}

CString CWindowsAutoLogin::get(LPCSTR key)
{
	CString value="";
	m_mapKeyValues.Lookup(key, value);
	return value;
}

int CWindowsAutoLogin::set(LPCSTR key, LPCSTR value)
{
	m_mapKeyValues.SetAt(key, value);
	return TRUE;
}

int CWindowsAutoLogin::test()
{
	ciDEBUG(1, ("test()"));

	int ret_val = TRUE;

	// get autologin-info from registry
	HKEY hKey;
	char szVal[255]={0};
	DWORD dwType = REG_SZ;
	DWORD dwSize;
	CString strRegKey = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon";

	LONG lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, strRegKey, 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey); // 32bit OS일경우 KEY_WOW64_32KEY
	if( lRet == ERROR_SUCCESS )
	{
		//
		dwSize = 255;
		::ZeroMemory(szVal, sizeof(szVal));
		lRet = RegQueryValueEx(hKey, KEY_WINDOWS_AUTO_LOGIN_USE, NULL, &dwType, (BYTE*)szVal, &dwSize);  
		if( lRet == ERROR_SUCCESS && dwType == REG_SZ )
		{
			set(KEY_WINDOWS_AUTO_LOGIN_USE, szVal);
			ciDEBUG(1, ("%s=%s", KEY_WINDOWS_AUTO_LOGIN_USE, szVal));

			if( strcmp(szVal, "1") != 0 )
			{
				char errBuf[1024]={0};
				sprintf(errBuf, "AutoLogon not use");
				setError(-1, errBuf);
				ciDEBUG(1, (errBuf));
				ret_val = FALSE;
			}
		}
		else
		{
			char errBuf[1024]={0};
			sprintf(errBuf, "Fail to RegQueryValueEx (HKLM\\%s\\%s) (ErrCode:%d)", strRegKey, KEY_WINDOWS_AUTO_LOGIN_USE, lRet);
			setError(lRet, errBuf);
			ciWARN((errBuf));
			ret_val = FALSE;
		}

		//
		dwSize = 255;
		::ZeroMemory(szVal, sizeof(szVal));
		lRet = RegQueryValueEx(hKey, KEY_WINDOWS_AUTO_LOGIN_ID, NULL, &dwType, (BYTE*)szVal, &dwSize);  
		if( lRet == ERROR_SUCCESS && dwType == REG_SZ )
		{
			set(KEY_WINDOWS_AUTO_LOGIN_ID, szVal);
			ciDEBUG(1, ("%s=%s", KEY_WINDOWS_AUTO_LOGIN_ID, szVal));
		}
		else
		{
			char errBuf[1024]={0};
			sprintf(errBuf, "Fail to RegQueryValueEx (HKLM\\%s\\%s) (ErrCode:%d)", strRegKey, KEY_WINDOWS_AUTO_LOGIN_ID, lRet);
			setError(lRet, errBuf);
			ciWARN((errBuf));
			ret_val = FALSE;
		}

		//
		dwSize = 255;
		::ZeroMemory(szVal, sizeof(szVal));
		lRet = RegQueryValueEx(hKey, KEY_WINDOWS_AUTO_LOGIN_PASSWORD, NULL, &dwType, (BYTE*)szVal, &dwSize);  
		if( lRet == ERROR_SUCCESS && dwType == REG_SZ )
		{
			set(KEY_WINDOWS_AUTO_LOGIN_PASSWORD, szVal);
			ciDEBUG(1, ("%s=%s", KEY_WINDOWS_AUTO_LOGIN_PASSWORD, szVal));
		}
		else
		{
			char errBuf[1024]={0};
			sprintf(errBuf, "Fail to RegQueryValueEx (HKLM\\%s\\%s) (ErrCode:%d)", strRegKey, KEY_WINDOWS_AUTO_LOGIN_PASSWORD, lRet);
			setError(lRet, errBuf);
			ciWARN((errBuf));
			ret_val = FALSE;
		}

		//
		RegCloseKey(hKey);
	}
	else
	{
		ciWARN(("Fail to RegOpenKeyEx(HLM\\%s) !!!", strRegKey));
		ret_val = FALSE;
	}//if

	ciDEBUG(1, ("test() end"));
	return ret_val;
}

int	CWindowsAutoLogin::install()
{
	ciDEBUG(1, ("install()"));

	_errString = "";

	// set autologin-info to registry
	HKEY hKey;
	CString strRegKey = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon";

	LONG lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, strRegKey, 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey); // 32bit OS일경우 KEY_WOW64_32KEY
	if( lRet == ERROR_SUCCESS )
	{
		CString value;
		//
		lRet = RegSetValueEx(
				hKey,  // RegCreateKeyEx에서 얻은 핸들값
				KEY_WINDOWS_AUTO_LOGIN_USE, // 값 이름
				NULL, // 반드시 0
				REG_SZ, //문자열 데이타 타입
				(LPBYTE)"1", // 값 데이터
				2 //값의 타입이(REG_SZ, REG_EXPAND_SZ, REG_MULTI_SZ) 일 경우 문자열의 크기
			);
		if( lRet == ERROR_SUCCESS )  // 성공시 ERROR_SUCCESS, 실패시 0이 아닌값이 리턴됨
		{
			ciDEBUG(1, ("%s=1", KEY_WINDOWS_AUTO_LOGIN_USE));
		}
		else
		{
			char errBuf[1024]={0};
			sprintf(errBuf, "Fail to RegSetValueEx (HKLM\\%s\\%s) (ErrCode:%d)", strRegKey, KEY_WINDOWS_AUTO_LOGIN_USE, lRet);
			setError(lRet, errBuf);
			ciERROR((errBuf));
			return FALSE;
		}

		//
		value = "";
		get(KEY_WINDOWS_AUTO_LOGIN_ID, value);
		lRet = RegSetValueEx(
				hKey,  // RegCreateKeyEx에서 얻은 핸들값
				KEY_WINDOWS_AUTO_LOGIN_ID, // 값 이름
				NULL, // 반드시 0
				REG_SZ, //문자열 데이타 타입
				(LPBYTE)(LPCSTR)value, // 값 데이터
				value.GetLength()+1 //값의 타입이(REG_SZ, REG_EXPAND_SZ, REG_MULTI_SZ) 일 경우 문자열의 크기
			);
		if( lRet == ERROR_SUCCESS )  // 성공시 ERROR_SUCCESS, 실패시 0이 아닌값이 리턴됨
		{
			ciDEBUG(1, ("%s=%s", KEY_WINDOWS_AUTO_LOGIN_ID, value));
		}
		else
		{
			char errBuf[1024]={0};
			sprintf(errBuf, "Fail to RegSetValueEx (HKLM\\%s\\%s) (ErrCode:%d)", strRegKey, KEY_WINDOWS_AUTO_LOGIN_ID, lRet);
			setError(lRet, errBuf);
			ciERROR((errBuf));
			return FALSE;
		}

		//
		value = "";
		get(KEY_WINDOWS_AUTO_LOGIN_PASSWORD, value);
		lRet = RegSetValueEx(
				hKey,  // RegCreateKeyEx에서 얻은 핸들값
				KEY_WINDOWS_AUTO_LOGIN_PASSWORD, // 값 이름
				NULL, // 반드시 0
				REG_SZ, //문자열 데이타 타입
				(LPBYTE)(LPCSTR)value, // 값 데이터
				value.GetLength()+1 //값의 타입이(REG_SZ, REG_EXPAND_SZ, REG_MULTI_SZ) 일 경우 문자열의 크기
			);
		if( lRet == ERROR_SUCCESS )  // 성공시 ERROR_SUCCESS, 실패시 0이 아닌값이 리턴됨
		{
			ciDEBUG(1, ("%s=%s", KEY_WINDOWS_AUTO_LOGIN_PASSWORD, value));
		}
		else
		{
			char errBuf[1024]={0};
			sprintf(errBuf, "Fail to RegSetValueEx (HKLM\\%s\\%s) (ErrCode:%d)", strRegKey, KEY_WINDOWS_AUTO_LOGIN_PASSWORD, lRet);
			setError(lRet, errBuf);
			ciERROR((errBuf));
			return FALSE;
		}

		//
		RegCloseKey(hKey);
	}
	else
	{
		char errBuf[1024]={0};
		sprintf(errBuf, "Fail to RegOpenKeyEx (HKLM\\%s) (ErrCode:%d)", strRegKey, lRet);
		setError(lRet, errBuf);
		ciERROR((errBuf));
		return FALSE;
	}//if

	ciDEBUG(1,("install() end"));
	return TRUE;
}
