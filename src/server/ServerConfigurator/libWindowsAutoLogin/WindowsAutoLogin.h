#pragma once

using namespace std;

#include "../Installable/installable.h"

#define		KEY_WINDOWS_AUTO_LOGIN_ID			("DefaultUserName")
#define		KEY_WINDOWS_AUTO_LOGIN_PASSWORD		("DefaultPassword")
#define		KEY_WINDOWS_AUTO_LOGIN_USE			("AutoAdminLogon")

class CWindowsAutoLogin : public installable
{
public:
	CWindowsAutoLogin(void);
	virtual ~CWindowsAutoLogin(void);

	virtual int prerequisite() { return 0; };
	virtual int test();
	virtual int	install();

	virtual int get(LPCSTR key, CString& value);
	virtual int set(LPCSTR key, LPCSTR value);
	virtual CString get(LPCSTR key);

protected:
	CMapStringToString		m_mapKeyValues;

};
