#ifndef _nasInstall_h_
#define _nasInstall_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

#include "ServerConfigurator/Installable/installable.h"

#define		NAS_USE					"NAS_USE"
#define		NAS_NETWORK_PATH		"NAS_NETWORK_PATH"
#define		NAS_ID					"NAS_ID"
#define		NAS_PASSWORD			"NAS_PASSWORD"
#define		NAS_DRIVE				"NAS_DRIVE"


class nasInstall : public installable {
public:
	nasInstall();
	nasInstall(int userNas, const char* networkPath, const char* id, const char* passwd, const char* drive);
	virtual ~nasInstall() {};

	// void setDBKind(const char* dbKind);

	virtual int prerequisite();
	virtual int test();
	virtual int	install();


	virtual int get(const char* key, string& value);
	virtual int get(const char* key, int& value);
	
	virtual int set(const char* key, const char* value);
	virtual int set(const char* key, int value);

	virtual void printError();
	virtual void  printInfo();

protected:

	virtual int	_uninstall();

	virtual int	_validTest();
	virtual int _getInfo();
	virtual int _setInfo();
	ciBoolean	_isExistDrive(const char* drive);
	ciBoolean	_isExistPath(const char* path);
	ciBoolean	_moveDir(const char* fromDir, const char* toDir);
	ciBoolean	_copyDir(const char* fromDir, const char* toDir);
	ciBoolean	_removeDir(const char* dir);

	int			_useNas;
	string		_networkPath;
	string		_id;
	string		_passwd;
	string		_drive;

	ciBoolean	_driveExist;

	ciBoolean	_addWindowsCredentials(const char* serverIP, const char* username, const char* password);

	ciBoolean	_isSymbolicLink(const char* path);
};


#endif