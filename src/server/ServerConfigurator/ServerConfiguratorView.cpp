// ServerConfiguratorView.cpp : implementation of the CServerConfiguratorView class
//

#include "stdafx.h"
#include "ServerConfigurator.h"

#include "ServerConfiguratorDoc.h"
#include "ServerConfiguratorView.h"

#include "SummaryDlg.h"
#include "OSSettingsDlg.h"
#include "LicenseSettingsDlg.h"
#include "UBCBaseSettingsDlg.h"
#include "DBSettingsDlg.h"
#include "IISSettingsDlg.h"
#include "UBCServerSettingsDlg.h"
#include "NASSettingsDlg.h"
#include "FirewallSettingsDlg.h"
#include "INISettingsDlg.h"
#include "TrdpartySettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		DLG_LEFT_GAP		2
#define		DLG_RIGHT_GAP		3
#define		DLG_TOP_GAP(n)		(4 + 18*n)//22
#define		DLG_BOTTOM_GAP		3


// CServerConfiguratorView

IMPLEMENT_DYNCREATE(CServerConfiguratorView, CFormView)

BEGIN_MESSAGE_MAP(CServerConfiguratorView, CFormView)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_STEP, &CServerConfiguratorView::OnTcnSelchangeTabStep)
	ON_BN_CLICKED(IDC_BUTTON_BACK_STEP, &CServerConfiguratorView::OnBnClickedButtonBackStep)
	ON_BN_CLICKED(IDC_BUTTON_NEXT_STEP, &CServerConfiguratorView::OnBnClickedButtonNextStep)
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CServerConfiguratorView::OnBnClickedButtonExit)
	ON_MESSAGE(WM_COMPLETE_DIALOG_INIT, OnCompleteDialogInit)
	ON_MESSAGE(SHORTCUT_BUTTON_ID, OnShortcutButton)
	ON_WM_SETCURSOR()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CServerConfiguratorView construction/destruction

CServerConfiguratorView::CServerConfiguratorView()
	: CFormView(CServerConfiguratorView::IDD)
{
	m_brushBG.CreateSolidBrush(::GetSysColor(COLOR_BTNHIGHLIGHT));

	m_pSummaryDlg = NULL;
}

CServerConfiguratorView::~CServerConfiguratorView()
{
}

void CServerConfiguratorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_STEP, m_tcStep);
	DDX_Control(pDX, IDC_BUTTON_BACK_STEP, m_btnBackStep);
	DDX_Control(pDX, IDC_BUTTON_NEXT_STEP, m_btnNextStep);
	DDX_Control(pDX, IDC_BUTTON_EXIT, m_btnExit);
}

BOOL CServerConfiguratorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CServerConfiguratorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	SetScrollSizes(MM_TEXT, CSize(1,1));

	m_reposControl.SetParent(this);
	m_reposControl.AddControl((CWnd*)&m_tcStep, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnBackStep, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnNextStep, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnExit, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	//
	//CRect tab_rect;
	//m_tcStep.GetWindowRect(tab_rect);

	//
	int tab_idx = 0;
	ADD_DIALOG(IDS_DIALOG_SUMMARY,		CSummaryDlg,			IDD_SUMMARY);
	ADD_DIALOG(IDS_DIALOG_TRDPARTY,		CTrdpartySettingsDlg,	IDD_TRDPARTY_SETTINGS);
	ADD_DIALOG(IDS_DIALOG_OS,			COSSettingsDlg,			IDD_OS_SETTINGS);
	ADD_DIALOG(IDS_DIALOG_UBC_BASE,		CUBCBaseSettingsDlg,	IDD_UBC_BASE_SETTINGS);
	ADD_DIALOG(IDS_DIALOG_LICENSE,		CLicenseSettingsDlg,	IDD_LICENSE_SETTINGS);
	ADD_DIALOG(IDS_DIALOG_DB,			CDBSettingsDlg,			IDD_DB_SETTINGS);
	ADD_DIALOG(IDS_DIALOG_UBC_SERVER,	CUBCServerSettingsDlg,	IDD_UBC_SERVER_SETTINGS);
	ADD_DIALOG(IDS_DIALOG_IIS,			CIISSettingsDlg,		IDD_IIS_SETTINGS);
	ADD_DIALOG(IDS_DIALOG_NAS,			CNASSettingsDlg,		IDD_NAS_SETTINGS);
	ADD_DIALOG(IDS_DIALOG_FIREWALL,		CFirewallSettingsDlg,	IDD_FIREWALL_SETTINGS);
	ADD_DIALOG(IDS_DIALOG_INI,			CINISettingsDlg,		IDD_INI_SETTINGS);

	m_pSummaryDlg = (CSummaryDlg*)GetDocument()->GetSubDlg(0);

	//
	int count = GetDocument()->GetSubDlgCount();
	for(int i=0; i<count; i++)
	{
		const SUBDLG_ITEM& subdlg_item = GetDocument()->GetSubDlgItem(i);

		subdlg_item.pDialog->Create(subdlg_item.nIDTemplate, (CWnd*)&m_tcStep);
		subdlg_item.pDialog->ShowWindow(SW_HIDE);
		//subdlg_item.pDialog->MoveWindow(DLG_LEFT_GAP,
		//								DLG_TOP_GAP,
		//								tab_rect.Width() - (DLG_LEFT_GAP + DLG_RIGHT_GAP),
		//								tab_rect.Height() - (DLG_TOP_GAP + DLG_BOTTOM_GAP) );
		subdlg_item.pDialog->EnableDialog(TRUE);///////////////////////////////////////////////////////////////
	}
	m_pSummaryDlg->InitControl(GetDocument());
	m_pSummaryDlg->UpdateControls();
	m_pSummaryDlg->EnableDialog(TRUE);
	MoveSubDialog();

	//
	LRESULT result;
	OnTcnSelchangeTabStep(NULL, &result);

	//
	SetTimer(1025, 100, NULL); // run init thread
}


// CServerConfiguratorView diagnostics

#ifdef _DEBUG
void CServerConfiguratorView::AssertValid() const
{
	CFormView::AssertValid();
}

void CServerConfiguratorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CServerConfiguratorDoc* CServerConfiguratorView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CServerConfiguratorDoc)));
	return (CServerConfiguratorDoc*)m_pDocument;
}
#endif //_DEBUG


// CServerConfiguratorView message handlers

HBRUSH CServerConfiguratorView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CServerConfiguratorView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )
	{
		m_reposControl.MoveControl(TRUE);
		MoveSubDialog();
	}
}

void CServerConfiguratorView::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 1025)
	{
		KillTimer(1025);

		bool bSubDlgAllInit = true;
		for(int i=1; i<GetDocument()->GetSubDlgCount(); i++)
		{
			CSubDlg* subdlg = GetDocument()->GetSubDlg(i);
			if( subdlg->IsInit() == false )
			{
				bSubDlgAllInit = false;
				subdlg->RunInitThread();
				break;
			}
		}

		GetDocument()->SetSubDLgAllInit(bSubDlgAllInit);
		m_pSummaryDlg->UpdateControls();
		UpdateControls();
	}

	CFormView::OnTimer(nIDEvent);
}

void CServerConfiguratorView::OnTcnSelchangeTabStep(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	int idx = m_tcStep.GetCurSel();
	if(idx < 0) return;

	for(int i=0; i<GetDocument()->GetSubDlgCount(); i++)
	{
		CSubDlg* subdlg = GetDocument()->GetSubDlg(i);

		if(i == idx)
		{
			subdlg->ShowWindow(SW_SHOW);
			subdlg->SetFocus();
		}
		else
		{
			subdlg->ShowWindow(SW_HIDE);
		}
	}

	UpdateControls();
}

void CServerConfiguratorView::OnBnClickedButtonBackStep()
{
	int idx = m_tcStep.GetCurSel();
	if(idx > 0)
	{
		idx--;
		m_tcStep.SetCurSel(idx);
	}

	LRESULT result;
	OnTcnSelchangeTabStep(NULL, &result);

	UpdateControls();
}

void CServerConfiguratorView::OnBnClickedButtonNextStep()
{
	int idx = m_tcStep.GetCurSel();
	if(idx < m_tcStep.GetItemCount()-1)
	{
		idx++;
		m_tcStep.SetCurSel(idx);
	}

	LRESULT result;
	OnTcnSelchangeTabStep(NULL, &result);

	UpdateControls();
}

void CServerConfiguratorView::OnBnClickedButtonExit()
{
	//
	// exit 전 처리
	//

	::AfxGetMainWnd()->PostMessage(WM_CLOSE, 0, 0);
}

void CServerConfiguratorView::MoveSubDialog()
{
	CRect tab_rect;
	m_tcStep.GetWindowRect(tab_rect);

	int tab_row = m_tcStep.GetRowCount();

	int count = GetDocument()->GetSubDlgCount();
	for(int i=0; i<count; i++)
	{
		const SUBDLG_ITEM& subdlg_item = GetDocument()->GetSubDlgItem(i);

		subdlg_item.pDialog->MoveWindow(DLG_LEFT_GAP,
										DLG_TOP_GAP(tab_row),
										tab_rect.Width() - (DLG_LEFT_GAP + DLG_RIGHT_GAP),
										tab_rect.Height() - (DLG_TOP_GAP(tab_row) + DLG_BOTTOM_GAP) );
	}
}

void CServerConfiguratorView::UpdateControls()
{
	if( GetDocument()->IsSubDLgAllInit() )
	{
		m_tcStep.EnableWindow(TRUE);

		int idx = m_tcStep.GetCurSel();
		if(idx == 0)
		{
			m_btnBackStep.EnableWindow(FALSE);
			m_btnNextStep.EnableWindow(TRUE);
		}
		else if(idx == m_tcStep.GetItemCount()-1)
		{
			m_btnBackStep.EnableWindow(TRUE);
			m_btnNextStep.EnableWindow(FALSE);
		}
		else
		{
			m_btnBackStep.EnableWindow(TRUE);
			m_btnNextStep.EnableWindow(TRUE);
		}
	}
	else
	{
		m_tcStep.EnableWindow(FALSE);
		m_btnBackStep.EnableWindow(FALSE);
		m_btnNextStep.EnableWindow(FALSE);
	}
}

LRESULT CServerConfiguratorView::OnCompleteDialogInit(WPARAM wParam, LPARAM lParam)
{
	SetTimer(1025, 100, NULL); // init next subdlg

	return 0;
}

LRESULT CServerConfiguratorView::OnShortcutButton(WPARAM wParam, LPARAM lParam)
{
	m_tcStep.SetCurSel(wParam);

	LRESULT result;
	OnTcnSelchangeTabStep(NULL, &result);

	UpdateControls();

	return 0;
}

BOOL CServerConfiguratorView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if( !GetDocument()->IsSubDLgAllInit() && pWnd && pWnd->GetSafeHwnd()!=m_btnExit.GetSafeHwnd() )
	{
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		return TRUE;
	}

	return CFormView::OnSetCursor(pWnd, nHitTest, message);
}
