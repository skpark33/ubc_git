#pragma once

#include "SubDlg.h"

#include "libInstallAutoUpdateDisable/InstallAutoUpdateDisable.h"
#include "libInstallRemoteDesktop/InstallRemoteDesktop.h"
#include "libInstallUBCStarter/InstallUBCStarter.h"
#include "libInstallEnv/InstallEnv.h"
#include "libWindowsAutoLogin/WindowsAutoLogin.h"

#include "ReposControl2.h"


// COSSettingsDlg 대화 상자입니다.

class COSSettingsDlg : public CSubDlg
{
	DECLARE_DYNAMIC(COSSettingsDlg)

public:
	COSSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COSSettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OS_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CReposControl2		m_reposControl;

	// init(saved) values
	bool		m_bDisableWindowsUpdate;
	bool		m_bEnableRDP;
	bool		m_bAddToStartupPrograms;
	bool		m_bAutoLogin;
	CString		m_strAutoLoginID;
	CString		m_strAutoLoginPwd;
	bool		m_bEnvCompleted;

	CInstallAutoUpdateDisable	m_AutoUpdateDisable;
	CInstallRemoteDesktop		m_RemoteDesktop;
	CInstallUBCStarter			m_UBCStarter;
	CWindowsAutoLogin			m_WindowsAutoLogin;
	CInstallEnv					m_Env;

public:
	CButton			m_chkDisableWindowsUpdate;
	CButton_Check	m_btnDisableWindowsUpdate;

	CButton			m_chkEnableRDP;
	CButton_Check	m_btnEnableRDP;

	CButton			m_chkAddToStartupPrograms;
	CButton_Check	m_btnAddToStartupPrograms;

	CEdit			m_editAutoLoginID;
	CEdit			m_editAutoLoginPassword;
	CButton_String	m_btnAutoLogin;

	CStatic			m_stcEnvVar;
	CListCtrl		m_lcEnv;
	CButton			m_btnImportDefaultEnv;
	CButton			m_btnAddEnv;
	CButton			m_btnModifyEnv;
	CButton			m_btnDeleteEnv;
	CButton_Check	m_btnSaveEnv;

	afx_msg void OnBnClickedCheckDisableWindowsUpdate();
	afx_msg void OnBnClickedButtonDisableWindowsUpdate();

	afx_msg void OnBnClickedCheckEnableRdp();
	afx_msg void OnBnClickedButtonEnableRdp();

	afx_msg void OnBnClickedCheckAddToStartupPrograms();
	afx_msg void OnBnClickedButtonAddToStartupPrograms();

	afx_msg void OnEnChangeEditAutologin();
	afx_msg void OnBnClickedButtonAutologin();

	afx_msg void OnNMDblclkListEnv(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonImportDefaultEnv();
	afx_msg void OnBnClickedButtonAddEnv();
	afx_msg void OnBnClickedButtonModifyEnv();
	afx_msg void OnBnClickedButtonDeleteEnv();
	afx_msg void OnBnClickedButtonSaveEnv();

	virtual void	UpdateControls();
	virtual bool	RunInit();
	virtual int		CheckInstallComplete();

protected:

	CMapStringToString	m_mapDeleteEnv;

	CString	GetLocalIP();
	CString	GetMySQLPath();
	CString	GetMainServerIP();
	int		FindItem(LPCSTR lpszVar);
	void	ModifyItem(int idx);
	void	InsertEnvItem(LPCSTR lpszVar, LPCSTR lpszVal);

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
