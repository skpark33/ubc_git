// ServerConfiguratorDoc.cpp : implementation of the CServerConfiguratorDoc class
//

#include "stdafx.h"
#include "ServerConfigurator.h"

#include "ServerConfiguratorDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CServerConfiguratorDoc

IMPLEMENT_DYNCREATE(CServerConfiguratorDoc, CDocument)

BEGIN_MESSAGE_MAP(CServerConfiguratorDoc, CDocument)
END_MESSAGE_MAP()


// CServerConfiguratorDoc construction/destruction

CServerConfiguratorDoc::CServerConfiguratorDoc()
{
	m_bSubDLgAllInit = false;
}

CServerConfiguratorDoc::~CServerConfiguratorDoc()
{
	for(int i=0; i<m_SubDlgItemList.GetCount(); i++)
	{
		CSubDlg* subdlg = m_SubDlgItemList.GetAt(i).pDialog;
		subdlg->DestroyWindow();
		delete subdlg;
	}
}

BOOL CServerConfiguratorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CServerConfiguratorDoc serialization

void CServerConfiguratorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CServerConfiguratorDoc diagnostics

#ifdef _DEBUG
void CServerConfiguratorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CServerConfiguratorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CServerConfiguratorDoc commands

void CServerConfiguratorDoc::AddDialog(LPCSTR lpszTitle, CSubDlg* pDialog, UINT nIDTemplate)
{
	SUBDLG_ITEM item;

	item.strTitle = lpszTitle;
	item.pDialog = pDialog;
	item.nIDTemplate = nIDTemplate;

	m_SubDlgItemList.Add(item);
}
