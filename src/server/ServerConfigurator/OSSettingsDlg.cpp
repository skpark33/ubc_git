// OSSettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "OSSettingsDlg.h"

#include "InputKeyValueDlg.h"
#include "ErrorMessageDlg.h"

#include "common/libCommon/ubcIni.h"
#include "common/libScratch/scratchUtil.h"


// COSSettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(COSSettingsDlg, CSubDlg)

COSSettingsDlg::COSSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*COSSettingsDlg::IDD,*/ pParent)
{
	m_bDisableWindowsUpdate = false;
	m_bEnableRDP            = false;
	m_bAddToStartupPrograms = false;
	m_bAutoLogin            = false;
	m_bEnvCompleted         = false;
}

COSSettingsDlg::~COSSettingsDlg()
{
}

void COSSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_DISABLE_WINDOWS_UPDATE, m_chkDisableWindowsUpdate);
	DDX_Control(pDX, IDC_BUTTON_DISABLE_WINDOWS_UPDATE,	m_btnDisableWindowsUpdate);
	DDX_Control(pDX, IDC_CHECK_ENABLE_RDP, m_chkEnableRDP);
	DDX_Control(pDX, IDC_BUTTON_ENABLE_RDP, m_btnEnableRDP);
	DDX_Control(pDX, IDC_CHECK_ADD_TO_STARTUP_PROGRAMS, m_chkAddToStartupPrograms);
	DDX_Control(pDX, IDC_BUTTON_ADD_TO_STARTUP_PROGRAMS, m_btnAddToStartupPrograms);
	DDX_Control(pDX, IDC_EDIT_AUTOLOGIN_ID, m_editAutoLoginID);
	DDX_Control(pDX, IDC_EDIT_AUTOLOGIN_PASSWORD, m_editAutoLoginPassword);
	DDX_Control(pDX, IDC_BUTTON_AUTOLOGIN, m_btnAutoLogin);
	DDX_Control(pDX, IDC_STATIC_ENV_VAR, m_stcEnvVar);
	DDX_Control(pDX, IDC_LIST_ENV, m_lcEnv);
	DDX_Control(pDX, IDC_BUTTON_IMPORT_DEFAULT_ENV, m_btnImportDefaultEnv);
	DDX_Control(pDX, IDC_BUTTON_ADD_ENV, m_btnAddEnv);
	DDX_Control(pDX, IDC_BUTTON_MODIFY_ENV, m_btnModifyEnv);
	DDX_Control(pDX, IDC_BUTTON_DELETE_ENV, m_btnDeleteEnv);
	DDX_Control(pDX, IDC_BUTTON_SAVE_ENV, m_btnSaveEnv);
}

BEGIN_MESSAGE_MAP(COSSettingsDlg, CSubDlg)
	ON_BN_CLICKED(IDC_CHECK_DISABLE_WINDOWS_UPDATE, &COSSettingsDlg::OnBnClickedCheckDisableWindowsUpdate)
	ON_BN_CLICKED(IDC_BUTTON_DISABLE_WINDOWS_UPDATE, &COSSettingsDlg::OnBnClickedButtonDisableWindowsUpdate)
	ON_BN_CLICKED(IDC_CHECK_ENABLE_RDP, &COSSettingsDlg::OnBnClickedCheckEnableRdp)
	ON_BN_CLICKED(IDC_BUTTON_ENABLE_RDP, &COSSettingsDlg::OnBnClickedButtonEnableRdp)
	ON_BN_CLICKED(IDC_CHECK_ADD_TO_STARTUP_PROGRAMS, &COSSettingsDlg::OnBnClickedCheckAddToStartupPrograms)
	ON_BN_CLICKED(IDC_BUTTON_ADD_TO_STARTUP_PROGRAMS, &COSSettingsDlg::OnBnClickedButtonAddToStartupPrograms)
	ON_EN_CHANGE(IDC_EDIT_AUTOLOGIN_ID, &COSSettingsDlg::OnEnChangeEditAutologin)
	ON_EN_CHANGE(IDC_EDIT_AUTOLOGIN_PASSWORD, &COSSettingsDlg::OnEnChangeEditAutologin)
	ON_BN_CLICKED(IDC_BUTTON_AUTOLOGIN, &COSSettingsDlg::OnBnClickedButtonAutologin)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_ENV, &COSSettingsDlg::OnNMDblclkListEnv)
	ON_BN_CLICKED(IDC_BUTTON_IMPORT_DEFAULT_ENV, &COSSettingsDlg::OnBnClickedButtonImportDefaultEnv)
	ON_BN_CLICKED(IDC_BUTTON_ADD_ENV, &COSSettingsDlg::OnBnClickedButtonAddEnv)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY_ENV, &COSSettingsDlg::OnBnClickedButtonModifyEnv)
	ON_BN_CLICKED(IDC_BUTTON_DELETE_ENV, &COSSettingsDlg::OnBnClickedButtonDeleteEnv)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_ENV, &COSSettingsDlg::OnBnClickedButtonSaveEnv)
	ON_WM_SIZE()
END_MESSAGE_MAP()

// COSSettingsDlg 메시지 처리기입니다.

BOOL COSSettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	m_reposControl.SetParent(this);

	m_reposControl.AddControl((CWnd*)&m_stcEnvVar, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_lcEnv, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnImportDefaultEnv, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnAddEnv, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnModifyEnv, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnDeleteEnv, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnSaveEnv, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	AddControl( m_chkDisableWindowsUpdate.GetSafeHwnd() );
	AddControl( m_btnDisableWindowsUpdate.GetSafeHwnd() );
	AddControl( m_chkEnableRDP.GetSafeHwnd() );
	AddControl( m_btnEnableRDP.GetSafeHwnd() );
	AddControl( m_chkAddToStartupPrograms.GetSafeHwnd() );
	AddControl( m_btnAddToStartupPrograms.GetSafeHwnd() );
	AddControl( m_editAutoLoginID.GetSafeHwnd() );
	AddControl( m_editAutoLoginPassword.GetSafeHwnd() );
	AddControl( m_btnAutoLogin.GetSafeHwnd() );
	AddControl( m_lcEnv.GetSafeHwnd() );
	AddControl( m_btnImportDefaultEnv.GetSafeHwnd() );
	AddControl( m_btnAddEnv.GetSafeHwnd() );
	AddControl( m_btnModifyEnv.GetSafeHwnd() );
	AddControl( m_btnDeleteEnv.GetSafeHwnd() );
	AddControl( m_btnSaveEnv.GetSafeHwnd() );

	m_btnDisableWindowsUpdate.Init( &m_bDisableWindowsUpdate, &m_chkDisableWindowsUpdate );
	m_btnEnableRDP.Init( &m_bEnableRDP, &m_chkEnableRDP );
	m_btnAddToStartupPrograms.Init( &m_bAddToStartupPrograms, &m_chkAddToStartupPrograms );
	m_btnAutoLogin.Init( &m_strAutoLoginID, &m_editAutoLoginID );
	m_btnAutoLogin.Init( &m_strAutoLoginPwd, &m_editAutoLoginPassword );
	m_btnSaveEnv.Init( &m_bEnvCompleted, NULL );

	m_lcEnv.SetExtendedStyle( m_lcEnv.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );
	m_lcEnv.InsertColumn(0, ::LoadString(IDS_COLUMN_VARIABLE), 0, 120);
	m_lcEnv.InsertColumn(1, ::LoadString(IDS_COLUMN_VALUE), 0, 450);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void COSSettingsDlg::OnBnClickedCheckDisableWindowsUpdate()
{
	UpdateControls();
}

void COSSettingsDlg::OnBnClickedButtonDisableWindowsUpdate()
{
	BeginWaitCursor();

	int ret = m_AutoUpdateDisable.install();

	EndWaitCursor();

	if( !ret )
	{
		CErrorMessageDlg dlg("Install Error !!!", m_AutoUpdateDisable.getErrorCode(), m_AutoUpdateDisable.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();

		return;
	}

	m_bDisableWindowsUpdate = true;
	m_chkDisableWindowsUpdate.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void COSSettingsDlg::OnBnClickedCheckEnableRdp()
{
	UpdateControls();
}

void COSSettingsDlg::OnBnClickedButtonEnableRdp()
{
	BeginWaitCursor();

	int ret = m_RemoteDesktop.install();

	EndWaitCursor();

	if( !ret )
	{
		CErrorMessageDlg dlg("Install Error !!!", m_RemoteDesktop.getErrorCode(), m_RemoteDesktop.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();

		return;
	}

	m_bEnableRDP = true;
	m_chkEnableRDP.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void COSSettingsDlg::OnBnClickedCheckAddToStartupPrograms()
{
	UpdateControls();
}

void COSSettingsDlg::OnBnClickedButtonAddToStartupPrograms()
{
	BeginWaitCursor();

	int ret = m_UBCStarter.install();

	EndWaitCursor();

	if( !ret )
	{
		CErrorMessageDlg dlg("Install Error !!!", m_UBCStarter.getErrorCode(), m_UBCStarter.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();

		return;
	}

	m_bAddToStartupPrograms = true;
	m_chkAddToStartupPrograms.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

#define		DEFAULT_ENV_COUNT		(11)
char* szDefaultEnv[ DEFAULT_ENV_COUNT*2 ] = {
	"ACE_ROOT", 
	"%TRDPARTYROOT%\\win32\\ACE_wrappers", 

	"CONFIGROOT", 
	"%PROJECT_HOME%\\config", 

	"COP_HOME", 
	"%PROJECT_HOME%\\cop", 

	"COP_VCVER", 
	"8", 

	"MY_IP", 
	"(LOCAL_IP)", 

	"MYSQL_HOME", 
	"(MYSQL_PATH)", 
/*
	// move to UBC-Server-Settings
	"NAMESERVER_HOST", 
	"(MAIN_SERVER_IP)", 
*/
	"PATH", 
	"C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE;C:\\Program Files\\Microsoft Visual Studio 8\\VC\\bin;C:\\Program Files (x86)\\Microsoft Visual Studio 8\\Common7\\IDE;C:\\Program Files (x86)\\Microsoft Visual Studio 8\\VC\\bin;%MYSQL_HOME%\\bin;%PROJECT_HOME%\\bin\\win32;%PROJECT_HOME%\\bin8;%PROJECT_HOME%\\util\\win32;%COP_HOME%\\bin8;%COP_HOME%\\util\\win32;%TRDPARTYROOT%\\win32\\ACE_wrappers\\bin;%TRDPARTYROOT%\\win32\\ACE_wrappers\\lib;%TRDPARTYROOT%\\win32\\bin;%TRDPARTYROOT%\\win32\\UnxUtils\\bin;%TRDPARTYROOT%\\win32\\UnxUtils\\usr\\local\\wbin", 

	"PROJECT_CODE", 
	"ubc", 

	"PROJECT_HOME", 
	"C:\\Project\\ubc", 

	"TAO_ROOT", 
	"%TRDPARTYROOT%\\win32\\ACE_wrappers\\TAO", 

	"TRDPARTYROOT", 
	"C:\\Project\\3rdparty",
};

void COSSettingsDlg::OnBnClickedButtonImportDefaultEnv()
{
	// get install-drive letter
	char str_replace[MAX_PATH+1] = {0};
	::GetModuleFileName(NULL, str_replace, MAX_PATH);
	if('a' <= str_replace[0] && str_replace[0] <= 'z')
		str_replace[0] = str_replace[0] - 'a' + 'A';
	str_replace[3] = NULL; // ex="E:\"

	CString local_ip = GetLocalIP();
	CString mysql_path = GetMySQLPath();
	CString main_server_ip = GetMainServerIP();

	for(int i=0; i<DEFAULT_ENV_COUNT*2; i+=2)
	{
		char* variables = szDefaultEnv[i];
		CString values = szDefaultEnv[i+1];

		// change default-drive "C:\" => install-drive "E:\"
		if(str_replace[0] != 'C' && str_replace[0] != 'c' && stricmp(variables, "PATH"))
			values.Replace("C:\\", str_replace);

		if(stricmp(values, "(LOCAL_IP)") == 0)
		{
			values = (LPSTR)(LPCSTR)local_ip;
		}
		else if(stricmp(values, "(MYSQL_PATH)") == 0)
		{
			values = (LPSTR)(LPCSTR)mysql_path;
		}
		else if(stricmp(values, "(MAIN_SERVER_IP)") == 0)
		{
			//values = (LPSTR)(LPCSTR)main_server_ip;
			values = (LPSTR)(LPCSTR)local_ip;
		}

		InsertEnvItem(variables, values);
	}

	m_bEnvCompleted = false;
	CheckInstallComplete();
	UpdateControls();
}

void COSSettingsDlg::OnBnClickedButtonAddEnv()
{
	CInputKeyValueDlg dlg;
	dlg.SetDialogInfo(IDS_TITLE_EDIT_SYSTEM_VAR, IDS_TITLE_VARIABLE_NAME, "", IDS_TITLE_VARIABLE_VALUE, "");
	if(dlg.DoModal() != IDOK) return;

	CString new_variables, new_values;
	dlg.GetValue(new_variables, new_values);

	//
	if( stricmp(new_variables, "NAMESERVER_HOST")==0 )
	{
		::AfxMessageBox(IDS_INFO_CANT_SET_NAMESERVER_HOST_IN_THIS_TAB, MB_ICONWARNING );
		return;
	}

	// check already exist var
	for(int i=0; i<m_lcEnv.GetItemCount(); i++)
	{
		CString str_var = m_lcEnv.GetItemText(i, 0);
		if( stricmp(new_variables, str_var)==0 )
		{
			// already exist key, replace new value ?
			int ret_val = ::AfxMessageBox(IDS_WARN_ALREADY_EXIST_REPLACE_NEW_VALUE, MB_ICONWARNING | MB_YESNO);
			if( ret_val==IDNO ) return;
		}
	}

	InsertEnvItem(new_variables, new_values);

	m_bEnvCompleted = false;
	CheckInstallComplete();
	UpdateControls();
}

void COSSettingsDlg::OnBnClickedButtonModifyEnv()
{
	int count = m_lcEnv.GetItemCount();
	for(int i=0; i<count; i++)
	{
		UINT state = m_lcEnv.GetItemState(i, LVIS_SELECTED);
		if(state)
		{
			ModifyItem(i);
			return;
		}
	}
}

void COSSettingsDlg::OnBnClickedButtonDeleteEnv()
{
	int count = m_lcEnv.GetItemCount();
	for(int i=0; i<count; i++)
	{
		UINT state = m_lcEnv.GetItemState(i, LVIS_SELECTED);
		if(state)
		{
			CString key = m_lcEnv.GetItemText(i, 0);
			CString value = m_lcEnv.GetItemText(i, 1);

			m_mapDeleteEnv.SetAt(key, value);

			m_lcEnv.DeleteItem(i);

			m_bEnvCompleted = false;
			CheckInstallComplete();
			UpdateControls();

			return;
		}
	}
}

void COSSettingsDlg::OnBnClickedButtonSaveEnv()
{
	BeginWaitCursor();

	//
	m_Env.uninstall(m_mapDeleteEnv);
	m_mapDeleteEnv.RemoveAll();

	//
	CMapStringToString map_env;
	int count = m_lcEnv.GetItemCount();
	for(int i=0; i<count; i++)
	{
		CString str_name = m_lcEnv.GetItemText(i, 0);
		CString str_value = m_lcEnv.GetItemText(i, 1);

		map_env.SetAt(str_name, str_value);
	}
	m_Env.set(map_env);

	int ret = m_Env.install();

	EndWaitCursor();

	if( !ret )
	{
		CErrorMessageDlg dlg("Install Error !!!", m_Env.getErrorCode(), m_Env.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();

		return;
	}

	m_bEnvCompleted = true;

	CheckInstallComplete();
	UpdateControls();

	int ret_val = ::AfxMessageBox(IDS_MSG_YOU_NEED_TO_RERUN, MB_YESNO | MB_ICONWARNING);
	if( ret_val == IDYES )
	{
		::AfxGetMainWnd()->PostMessage(WM_CLOSE, 0, 0);
	}
}

void COSSettingsDlg::UpdateControls()
{
	m_btnDisableWindowsUpdate.UpdateControl();
	m_btnEnableRDP.UpdateControl();
	m_btnAddToStartupPrograms.UpdateControl();
	m_btnAutoLogin.UpdateControl();
	m_btnSaveEnv.UpdateControl();
}

CString	COSSettingsDlg::GetLocalIP()
{
	string current_ip;
	if( scratchUtil::getInstance()->getIpAddress(current_ip) ) return current_ip.c_str();
	return "0.0.0.0";
}

CString	COSSettingsDlg::GetMySQLPath()
{
	char* szFindPath[] = {
		"C:\\Program Files\\MySQL\\MySQL Server*.*", 
		"C:\\Program Files (x86)\\MySQL\\MySQL Server*.*", 
		"C:\\Program Files\\MariaDB*.*", 
		"C:\\Program Files (x86)\\MariaDB*.*", 
	};

	for(int i=0; i<4; i++)
	{
		CFileFind finder;
		BOOL bWorking = finder.FindFile(szFindPath[i]);

		while (bWorking)
		{
			bWorking = finder.FindNextFile();
			if(finder.IsDirectory())
			{
				return finder.GetFilePath();
			}
		}
		finder.Close();
	}
	return "C:\\Program Files (x86)\\MySQL\\MySQL Server 5.1";
}

CString	COSSettingsDlg::GetMainServerIP()
{
	ubcIni aIni("UBCConnect", _COP_CD("C:\\Project\\ubc\\config"), "data");

	ciString nameserver_host="";
	aIni.get("ORB_NAMESERVICE", "IP", nameserver_host);

	return nameserver_host.c_str();
}

int COSSettingsDlg::FindItem(LPCSTR lpszVar)
{
/*
	LVFINDINFO info;
	info.flags = LVFI_STRING;
	info.psz = lpszVar;

	return m_lcEnv.FindItem(&info);
*/

	int count = m_lcEnv.GetItemCount();
	for(int i=0; i<count; i++)
	{
		CString str = m_lcEnv.GetItemText(i, 0);
		if( stricmp(lpszVar, str) == 0 )
			return i;
	}
	return -1;
}

void COSSettingsDlg::ModifyItem(int idx)
{
	CString variables = m_lcEnv.GetItemText(idx, 0);
	CString values = m_lcEnv.GetItemText(idx, 1);

	CInputKeyValueDlg dlg;
	dlg.SetDialogInfo(IDS_TITLE_EDIT_SYSTEM_VAR, IDS_TITLE_VARIABLE_NAME, variables, IDS_TITLE_VARIABLE_VALUE, values);
	if(dlg.DoModal() != IDOK) return;

	CString new_variables, new_values;
	dlg.GetValue(new_variables, new_values);

	if(stricmp(variables, new_variables))
		m_lcEnv.DeleteItem(idx);

	InsertEnvItem(new_variables, new_values);

	m_bEnvCompleted = false;
	CheckInstallComplete();
	UpdateControls();
}

void COSSettingsDlg::InsertEnvItem(LPCSTR lpszVar, LPCSTR lpszVal)
{
	int idx = FindItem(lpszVar);
	if(idx < 0)
	{
		idx = m_lcEnv.GetItemCount();
		m_lcEnv.InsertItem(idx, lpszVar);
	}

	m_lcEnv.SetItemText(idx, 1, lpszVal);
}

void COSSettingsDlg::OnNMDblclkListEnv(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pNMLV->iItem < 0) return;

	ModifyItem(pNMLV->iItem);
}

bool COSSettingsDlg::RunInit()
{
	bool ret_val = true;

	// windows autoupdate disable
	if( m_AutoUpdateDisable.test() )
	{
		m_bDisableWindowsUpdate = true;
	}
	else
	{
		ret_val = false;
		m_bDisableWindowsUpdate = false;
		m_chkDisableWindowsUpdate.SetCheck(BST_CHECKED);
	}

	// remote desktop enable
	if( m_RemoteDesktop.test() )
	{
		m_bEnableRDP = true;
	}
	else
	{
		ret_val = false;
		m_bEnableRDP = false;
		m_chkEnableRDP.SetCheck(BST_CHECKED);
	}

	// add to startup group
	if( m_UBCStarter.test() )
	{
		m_bAddToStartupPrograms = true;
	}
	else
	{
		ret_val = false;
		m_bAddToStartupPrograms = false;
		m_chkAddToStartupPrograms.SetCheck(BST_CHECKED);
	}

	// auto-login
	if( m_WindowsAutoLogin.test() )
	{
		m_bAutoLogin = true;
		m_strAutoLoginID = "id";
		m_strAutoLoginPwd = "pwd";
		m_WindowsAutoLogin.get(KEY_WINDOWS_AUTO_LOGIN_ID, m_strAutoLoginID);
		m_WindowsAutoLogin.get(KEY_WINDOWS_AUTO_LOGIN_PASSWORD, m_strAutoLoginPwd);
		m_editAutoLoginID.SetWindowText(m_strAutoLoginID);
		m_editAutoLoginPassword.SetWindowText(m_strAutoLoginPwd);
	}
	else
	{
		ret_val = false;
		m_bAutoLogin = false;
	}

	// environment
	if( m_Env.test() )
	{
		m_bEnvCompleted = true;
	}
	else
	{
		ret_val = FALSE;
		m_bEnvCompleted = false;
	}

	CMapStringToString map_env;
	m_Env.get(map_env);

	m_lcEnv.DeleteAllItems();

	POSITION pos = map_env.GetStartPosition();
	CString strKey, strValue;
	while( pos != NULL )
	{
		map_env.GetNextAssoc(pos, strKey, strValue);
		InsertEnvItem(strKey, strValue);
	}

	for(int i=0; i<DEFAULT_ENV_COUNT*2 && ret_val; i+=2)
	{
		char* variables = szDefaultEnv[i];
		if( !map_env.Lookup(variables, strValue) )
			ret_val = FALSE;
	}

	return ret_val;
}

int COSSettingsDlg::CheckInstallComplete()
{
	m_bInstallComplete = true;

	if( m_bDisableWindowsUpdate == false || 
		m_bEnableRDP == false || 
		m_bAddToStartupPrograms == false || 
		m_bAutoLogin == false || 
		m_bEnvCompleted == false)
	{
		m_bInstallComplete = false;
	}

	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}

void COSSettingsDlg::OnBnClickedButtonAutologin()
{
	CString str_new_id, str_new_pwd;
	m_editAutoLoginID.GetWindowText(str_new_id);
	m_editAutoLoginPassword.GetWindowText(str_new_pwd);

	m_WindowsAutoLogin.set(KEY_WINDOWS_AUTO_LOGIN_ID, str_new_id);
	m_WindowsAutoLogin.set(KEY_WINDOWS_AUTO_LOGIN_PASSWORD, str_new_pwd);

	BeginWaitCursor();

	int ret = m_WindowsAutoLogin.install();

	EndWaitCursor();

	if( ret )
	{
		m_bAutoLogin = true;

		m_strAutoLoginID = str_new_id;
		m_strAutoLoginPwd = str_new_pwd;
	}
	else
	{
		m_bAutoLogin = false;
	}

	CheckInstallComplete();
	UpdateControls();

	if( m_bAutoLogin )
	{
		::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
	}
	else
	{
		CErrorMessageDlg dlg("Install Error !!!", m_WindowsAutoLogin.getErrorCode(), m_WindowsAutoLogin.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
	}
}

void COSSettingsDlg::OnEnChangeEditAutologin()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CSettingsDlg::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	m_btnAutoLogin.UpdateControl();
}

void COSSettingsDlg::OnSize(UINT nType, int cx, int cy)
{
	CSubDlg::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )
	{
		m_reposControl.MoveControl(TRUE);
	}
}
