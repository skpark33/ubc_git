// IISSettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "IISSettingsDlg.h"

#include "InputKeyValueDlg.h"
#include "ErrorMessageDlg.h"


// CIISSettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CIISSettingsDlg, CSubDlg)

CIISSettingsDlg::CIISSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
:	CSubDlg(pDoc, /*CIISSettingsDlg::IDD,*/ pParent)
,	m_UBCDefaultSetting (IIS_UBC_DEFAULT_NAME, 8080)
,	m_HttpSetting (IIS_UBC_HTTP_NAME, 8080)
,	m_UBCWebSetting (IIS_UBC_WEB_NAME, 80)
,	m_UBCFtpSetting (IIS_UBC_FTP_NAME, 21)
{
	m_bUBCDefaultCompleted = false;
	m_bHttpCompleted       = false;
	m_bUBCWebCompleted     = false;
	m_bUBCFtpCompleted     = false;
}

CIISSettingsDlg::~CIISSettingsDlg()
{
}

void CIISSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_USE_UBC_DEFAULT, m_chkUseUBCDefault);
	DDX_Control(pDX, IDC_EDIT_LEGACY_WEB_PORT, m_editUBCDefaultPort);
	DDX_Control(pDX, IDC_BUTTON_USE_UBC_DEFAULT, m_btnUseUBCDefault);
	DDX_Control(pDX, IDC_CHECK_USE_HTTP, m_chkUseHttp);
	DDX_Control(pDX, IDC_EDIT_HTTP_PORT, m_editHttpPort);
	DDX_Control(pDX, IDC_BUTTON_USE_HTTP, m_btnUseHttp);
	DDX_Control(pDX, IDC_CHECK_USE_UBC_WEB, m_chkUseUBCWeb);
	DDX_Control(pDX, IDC_EDIT_UBC_WEB_PORT, m_editUBCWebPort);
	DDX_Control(pDX, IDC_BUTTON_USE_UBC_WEB, m_btnUseUBCWeb);
	DDX_Control(pDX, IDC_CHECK_USE_IIS_FTP, m_chkUseIISFTP);
	DDX_Control(pDX, IDC_EDIT_IIS_FTP_PORT, m_editIISFTPPort);
	DDX_Control(pDX, IDC_EDIT_FTP_ID, m_editFTPID);
	DDX_Control(pDX, IDC_EDIT_FTP_PASSWORD, m_editFTPPassword);
	DDX_Control(pDX, IDC_EDIT_FTP_PASSIVE_LOW_PORT, m_editFTPPassiveLowPort);
	DDX_Control(pDX, IDC_EDIT_FTP_PASSIVE_HIGH_PORT, m_editFTPPassiveHighPort);
	DDX_Control(pDX, IDC_BUTTON_USE_IIS_FTP, m_btnUseIISFTP);
}


BEGIN_MESSAGE_MAP(CIISSettingsDlg, CSubDlg)
	ON_BN_CLICKED(IDC_CHECK_USE_UBC_DEFAULT, &CIISSettingsDlg::OnBnClickedCheckUseUBCDefault)
	ON_BN_CLICKED(IDC_BUTTON_USE_UBC_DEFAULT, &CIISSettingsDlg::OnBnClickedButtonUseUBCDefault)
	ON_BN_CLICKED(IDC_CHECK_USE_HTTP, &CIISSettingsDlg::OnBnClickedCheckUseHttp)
	ON_BN_CLICKED(IDC_BUTTON_USE_HTTP, &CIISSettingsDlg::OnBnClickedButtonUseHttp)
	ON_BN_CLICKED(IDC_CHECK_USE_UBC_WEB, &CIISSettingsDlg::OnBnClickedCheckUseUbcWeb)
	ON_BN_CLICKED(IDC_BUTTON_USE_UBC_WEB, &CIISSettingsDlg::OnBnClickedButtonUseUbcWeb)
	ON_BN_CLICKED(IDC_CHECK_USE_IIS_FTP, &CIISSettingsDlg::OnBnClickedCheckUseIisFtp)
	ON_BN_CLICKED(IDC_BUTTON_USE_IIS_FTP, &CIISSettingsDlg::OnBnClickedButtonUseIisFtp)
END_MESSAGE_MAP()


// CIISSettingsDlg 메시지 처리기입니다.

BOOL CIISSettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	AddControl( m_chkUseUBCDefault.GetSafeHwnd() );
	AddControl( m_editUBCDefaultPort.GetSafeHwnd() );
	AddControl( m_btnUseUBCDefault.GetSafeHwnd() );
	AddControl( m_chkUseHttp.GetSafeHwnd() );
	AddControl( m_editHttpPort.GetSafeHwnd() );
	AddControl( m_btnUseHttp.GetSafeHwnd() );
	AddControl( m_chkUseUBCWeb.GetSafeHwnd() );
	AddControl( m_editUBCWebPort.GetSafeHwnd() );
	AddControl( m_btnUseUBCWeb.GetSafeHwnd() );
	AddControl( m_chkUseIISFTP.GetSafeHwnd() );
	AddControl( m_editIISFTPPort.GetSafeHwnd() );
	AddControl( m_editFTPID.GetSafeHwnd() );
	AddControl( m_editFTPPassword.GetSafeHwnd() );
	AddControl( m_editFTPPassiveLowPort.GetSafeHwnd() );
	AddControl( m_editFTPPassiveHighPort.GetSafeHwnd() );
	AddControl( m_btnUseIISFTP.GetSafeHwnd() );

	AddColorIgnoreWnd( m_editUBCDefaultPort.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editHttpPort.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editUBCWebPort.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editIISFTPPort.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editFTPID.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editFTPPassword.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editFTPPassiveLowPort.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editFTPPassiveHighPort.GetSafeHwnd() );

	m_btnUseUBCDefault.Init( &m_bUBCDefaultCompleted, &m_chkUseUBCDefault );
	m_btnUseHttp.Init( &m_bHttpCompleted, &m_chkUseHttp );
	m_btnUseUBCWeb.Init( &m_bUBCWebCompleted, &m_chkUseUBCWeb );
	m_btnUseIISFTP.Init( &m_bUBCFtpCompleted, &m_chkUseIISFTP );

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CIISSettingsDlg::OnBnClickedCheckUseUBCDefault()
{
	UpdateControls();
}

void CIISSettingsDlg::OnBnClickedButtonUseUBCDefault()
{
	CString str_port;
	m_editUBCDefaultPort.GetWindowText(str_port);
	int iis_port = atoi(str_port);

	if( iis_port == 0 )
	{
		::AfxMessageBox(IDS_WARN_INPUT_PORT_NUMBER, MB_ICONINFORMATION);
		return;
	}

	BeginWaitCursor();

	m_UBCDefaultSetting.set(IIS_PORT, iis_port);
	int ret = m_UBCDefaultSetting.install();

	EndWaitCursor();

	if( !ret )
	{
		CErrorMessageDlg dlg("Install Error !!!", m_UBCDefaultSetting.getErrorCode(), m_UBCDefaultSetting.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();

		return;
	}

	m_bUBCDefaultCompleted= true;
	m_chkUseUBCDefault.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void CIISSettingsDlg::OnBnClickedCheckUseHttp()
{
	UpdateControls();
}

void CIISSettingsDlg::OnBnClickedButtonUseHttp()
{
	CString str_port;
	m_editHttpPort.GetWindowText(str_port);
	int iis_port = atoi(str_port);

	if( iis_port == 0 )
	{
		::AfxMessageBox(IDS_WARN_INPUT_PORT_NUMBER, MB_ICONINFORMATION);
		return;
	}

	BeginWaitCursor();

	m_HttpSetting.set(IIS_PORT, iis_port);
	int ret = m_HttpSetting.install();

	EndWaitCursor();

	if( !ret )
	{
		CErrorMessageDlg dlg("Install Error !!!", m_HttpSetting.getErrorCode(), m_HttpSetting.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();

		return;
	}

	m_bHttpCompleted = true;
	m_chkUseHttp.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void CIISSettingsDlg::OnBnClickedCheckUseUbcWeb()
{
	UpdateControls();
}

void CIISSettingsDlg::OnBnClickedButtonUseUbcWeb()
{
	CString str_port;
	m_editUBCWebPort.GetWindowText(str_port);
	int iis_port = atoi(str_port);

	if( iis_port == 0 )
	{
		::AfxMessageBox(IDS_WARN_INPUT_PORT_NUMBER, MB_ICONINFORMATION);
		return;
	}

	BeginWaitCursor();

	m_UBCWebSetting.set(IIS_PORT, iis_port);
	int ret = m_UBCWebSetting.install();

	EndWaitCursor();

	if( !ret )
	{
		CErrorMessageDlg dlg("Install Error !!!", m_UBCWebSetting.getErrorCode(), m_UBCWebSetting.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();

		return;
	}

	m_bUBCWebCompleted= true;
	m_chkUseUBCWeb.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void CIISSettingsDlg::OnBnClickedCheckUseIisFtp()
{
	UpdateControls();
}

void CIISSettingsDlg::OnBnClickedButtonUseIisFtp()
{
	//
	CString str_port;
	m_editIISFTPPort.GetWindowText(str_port);
	int iis_port = atoi(str_port);

	if( iis_port == 0 )
	{
		::AfxMessageBox(IDS_WARN_INPUT_PORT_NUMBER, MB_ICONWARNING);
		return;
	}

	//
	CString str_ftp_id, str_ftp_password;
	m_editFTPID.GetWindowText(str_ftp_id);
	m_editFTPPassword.GetWindowText(str_ftp_password);
	str_ftp_id.Trim(" \t");
	str_ftp_password.Trim(" \t");

	if( str_ftp_id.GetLength() == 0  || str_ftp_password.GetLength() == 0)
	{
		::AfxMessageBox(IDS_WARN_INPUT_ID_PASSWORD, MB_ICONWARNING);
		return;
	}

	//
	CString str_ftp_passive_low_port, str_ftp_passive_high_port;
	m_editFTPPassiveLowPort.GetWindowText(str_ftp_passive_low_port);
	m_editFTPPassiveHighPort.GetWindowText(str_ftp_passive_high_port);
	str_ftp_id.Trim(" \t");
	str_ftp_password.Trim(" \t");
	int low_port = atoi(str_ftp_passive_low_port);
	int high_port = atoi(str_ftp_passive_high_port);
	if( low_port == 0  || high_port == 0)
	{
		::AfxMessageBox(IDS_WARN_INPUT_FTP_PORT, MB_ICONWARNING);
		return;
	}

	BeginWaitCursor();

	//
	m_UBCFtpSetting.set(IIS_PORT, iis_port);
	m_UBCFtpSetting.set(IIS_ACCOUNT, str_ftp_id);
	m_UBCFtpSetting.set(IIS_PASSWORD, str_ftp_password);
	m_UBCFtpSetting.set(IIS_LOW_PORT, low_port);
	m_UBCFtpSetting.set(IIS_HIGH_PORT, high_port);

	int ret = m_UBCFtpSetting.install();

	EndWaitCursor();

	//
	if( !ret )
	{
		CErrorMessageDlg dlg("Install Error !!!", m_UBCFtpSetting.getErrorCode(), m_UBCFtpSetting.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();

		return;
	}

	m_bUBCFtpCompleted= true;
	m_chkUseIISFTP.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void CIISSettingsDlg::UpdateControls()
{
	m_btnUseHttp.UpdateControl();
	m_btnUseUBCDefault.UpdateControl();
	m_btnUseUBCWeb.UpdateControl();
	m_btnUseIISFTP.UpdateControl();

	m_editUBCDefaultPort.EnableWindow( m_chkUseUBCDefault.GetCheck() );
	m_editHttpPort.EnableWindow( m_chkUseHttp.GetCheck() );
	m_editUBCWebPort.EnableWindow( m_chkUseUBCWeb.GetCheck() );
	m_editIISFTPPort.EnableWindow( m_chkUseIISFTP.GetCheck() );
	m_editFTPID.EnableWindow( m_chkUseIISFTP.GetCheck() );
	m_editFTPPassword.EnableWindow( m_chkUseIISFTP.GetCheck() );
	m_editFTPPassiveLowPort.EnableWindow( m_chkUseIISFTP.GetCheck() );
	m_editFTPPassiveHighPort.EnableWindow( m_chkUseIISFTP.GetCheck() );
}

bool CIISSettingsDlg::RunInit()
{
	int port = 8080;
	char sz_port[255];

	//
	if( m_HttpSetting.test() )
	{
		m_bHttpCompleted = true;
	}
	else
	{
		m_chkUseHttp.SetCheck(BST_CHECKED);
	}
	port = 8080;
	m_HttpSetting.get(IIS_PORT, port);
	sprintf(sz_port, "%d", port);
	m_editHttpPort.SetWindowText(sz_port);

	//
	if( m_UBCDefaultSetting.test() )
	{
		m_bUBCDefaultCompleted = true;
	}
	else
	{
		m_chkUseUBCDefault.SetCheck(BST_CHECKED);
	}
	port = 8080;
	m_UBCDefaultSetting.get(IIS_PORT, port);
	sprintf(sz_port, "%d", port);
	m_editUBCDefaultPort.SetWindowText(sz_port);

	//
	if( m_UBCFtpSetting.test() )
	{
		m_bUBCFtpCompleted = true;
	}
	else
	{
		// default value = unchecked
		//m_chkUseIISFTP.SetCheck(BST_CHECKED);
		m_chkUseIISFTP.SetCheck(BST_UNCHECKED);
	}
	//
	port = 21;
	m_UBCFtpSetting.get(IIS_PORT, port);
	sprintf(sz_port, "%d", port);
	m_editIISFTPPort.SetWindowText(sz_port);
	//
	port = 14300;
	m_UBCFtpSetting.get(IIS_LOW_PORT, port);
	sprintf(sz_port, "%d", port);
	m_editFTPPassiveLowPort.SetWindowText(sz_port);
	//
	port = 14500;
	m_UBCFtpSetting.get(IIS_HIGH_PORT, port);
	sprintf(sz_port, "%d", port);
	m_editFTPPassiveHighPort.SetWindowText(sz_port);

	//
	if( m_UBCWebSetting.test() )
	{
		m_bUBCWebCompleted = true;
	}
	else
	{
		// default value = unchecked
		//m_chkUseUBCWeb.SetCheck(BST_CHECKED);
		m_chkUseUBCWeb.SetCheck(BST_UNCHECKED);
	}
	port = 80;
	m_UBCWebSetting.get(IIS_PORT, port);
	sprintf(sz_port, "%d", port);
	m_editUBCWebPort.SetWindowText(sz_port);

	return true;
}

int CIISSettingsDlg::CheckInstallComplete()
{
	m_bInstallComplete = true;

	if( m_bUBCDefaultCompleted == false /*|| 
		m_bHttpCompleted == false || 
		m_bUBCFtpCompleted == false || 
		m_bUBCWebCompleted == false */)
	{
		m_bInstallComplete = false;
	}

	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}
