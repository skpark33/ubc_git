// UBCBaseSettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "UBCBaseSettingsDlg.h"

//#include "ci/libDebug/ciDebug.h"
#include "ErrorMessageDlg.h"


//ciSET_DEBUG(10, "CLicenseSettingsDlg");


// CUBCBaseSettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CUBCBaseSettingsDlg, CSubDlg)

CUBCBaseSettingsDlg::CUBCBaseSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*CUBCBaseSettingsDlg::IDD,*/ pParent)
{
	m_nGlobalOption = -1;
	//m_bIsSetGlobalOption = false;
}

CUBCBaseSettingsDlg::~CUBCBaseSettingsDlg()
{
}

void CUBCBaseSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CURRENT_UBC_VERSION, m_editCurrentUBCVersion);
	DDX_Control(pDX, IDC_EDIT_UBC_CENTER_IP, m_editUBCCenterIP);
	DDX_Control(pDX, IDC_EDIT_UBC_CENTER_WEBPORT, m_editUBCCenterHTTPPort);
	DDX_Control(pDX, IDC_EDIT_UBC_CENTER_FTP_IP, m_editUBCCenterFTPIP);
	DDX_Control(pDX, IDC_EDIT_UBC_CENTER_FTP_PORT, m_editUBCCenterFTPPort);
	DDX_Control(pDX, IDC_EDIT_UBC_CENTER_FTP_ID, m_editUBCCenterFTPID);
	DDX_Control(pDX, IDC_EDIT_UBC_CENTER_FTP_PASSWORD, m_editUBCCenterFTPPwd);
	DDX_Control(pDX, IDC_BUTTON_SET_UBC_CENTER_INFO, m_btnSetUBCCenterInfo);
	DDX_Control(pDX, IDC_RADIO_DISUSE_GLOBAL, m_btnDisuseGlobal);
	DDX_Control(pDX, IDC_RADIO_USE_GLOBAL, m_btnUseGlobal);
	DDX_Control(pDX, IDC_BUTTON_SET_GLOBAL, m_btnSetGlobal);
}


BEGIN_MESSAGE_MAP(CUBCBaseSettingsDlg, CSubDlg)
	ON_BN_CLICKED(IDC_BUTTON_SET_UBC_CENTER_INFO, &CUBCBaseSettingsDlg::OnBnClickedButtonSetUbcCenterInfo)
	ON_BN_CLICKED(IDC_RADIO_DISUSE_GLOBAL, &CUBCBaseSettingsDlg::OnBnClickedRadioGlobal)
	ON_BN_CLICKED(IDC_RADIO_USE_GLOBAL, &CUBCBaseSettingsDlg::OnBnClickedRadioGlobal)
	ON_BN_CLICKED(IDC_BUTTON_SET_GLOBAL, &CUBCBaseSettingsDlg::OnBnClickedButtonSetGlobal)
	ON_EN_CHANGE(IDC_EDIT_UBC_CENTER_IP, &CUBCBaseSettingsDlg::OnEnChangeEditUbcCenter)
	ON_EN_CHANGE(IDC_EDIT_UBC_CENTER_WEBPORT, &CUBCBaseSettingsDlg::OnEnChangeEditUbcCenter)
	ON_EN_CHANGE(IDC_EDIT_UBC_CENTER_FTP_IP, &CUBCBaseSettingsDlg::OnEnChangeEditUbcCenter)
	ON_EN_CHANGE(IDC_EDIT_UBC_CENTER_FTP_PORT, &CUBCBaseSettingsDlg::OnEnChangeEditUbcCenter)
	ON_EN_CHANGE(IDC_EDIT_UBC_CENTER_FTP_ID, &CUBCBaseSettingsDlg::OnEnChangeEditUbcCenter)
	ON_EN_CHANGE(IDC_EDIT_UBC_CENTER_FTP_PASSWORD, &CUBCBaseSettingsDlg::OnEnChangeEditUbcCenter)
END_MESSAGE_MAP()


// CUBCBaseSettingsDlg 메시지 처리기입니다.


BOOL CUBCBaseSettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	AddColorIgnoreWnd( m_editCurrentUBCVersion.GetSafeHwnd() );

	AddControl( m_editCurrentUBCVersion.GetSafeHwnd() );
	AddControl( m_editUBCCenterIP.GetSafeHwnd() );
	AddControl( m_editUBCCenterHTTPPort.GetSafeHwnd() );
	AddControl( m_editUBCCenterFTPIP.GetSafeHwnd() );
	AddControl( m_editUBCCenterFTPPort.GetSafeHwnd() );
	AddControl( m_editUBCCenterFTPID.GetSafeHwnd() );
	AddControl( m_editUBCCenterFTPPwd.GetSafeHwnd() );
	AddControl( m_btnSetUBCCenterInfo.GetSafeHwnd() );
	AddControl( m_btnDisuseGlobal.GetSafeHwnd() );
	AddControl( m_btnUseGlobal.GetSafeHwnd() );
	AddControl( m_btnSetGlobal.GetSafeHwnd() );

	m_btnSetUBCCenterInfo.Init(&m_strUBCCenterIP, &m_editUBCCenterIP);
	m_btnSetUBCCenterInfo.Init(&m_strUBCCenterHTTPPort, &m_editUBCCenterHTTPPort, true); // allow empty
	m_btnSetUBCCenterInfo.Init(&m_strUBCCenterFTPIP, &m_editUBCCenterFTPIP, true); // allow empty
	m_btnSetUBCCenterInfo.Init(&m_strUBCCenterFTPPort, &m_editUBCCenterFTPPort, true); // allow empty
	m_btnSetUBCCenterInfo.Init(&m_strUBCCenterFTPID, &m_editUBCCenterFTPID);
	m_btnSetUBCCenterInfo.Init(&m_strUBCCenterFTPPwd, &m_editUBCCenterFTPPwd);
	m_btnSetGlobal.Init(&m_nGlobalOption, &m_btnDisuseGlobal, &m_btnUseGlobal );

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CUBCBaseSettingsDlg::OnBnClickedButtonSetUbcCenterInfo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString //str_current_version,
			str_center_ip,
			str_center_http_port,
			str_center_ftp_ip,
			str_center_ftp_port,
			str_center_ftp_id,
			str_center_ftp_pwd;

	//m_editCurrentUBCVersion.GetWindowText(str_current_version);
	m_editUBCCenterIP.GetWindowText(str_center_ip);
	m_editUBCCenterHTTPPort.GetWindowText(str_center_http_port);
	m_editUBCCenterFTPIP.GetWindowText(str_center_ftp_ip);
	m_editUBCCenterFTPPort.GetWindowText(str_center_ftp_port);
	m_editUBCCenterFTPID.GetWindowText(str_center_ftp_id);
	m_editUBCCenterFTPPwd.GetWindowText(str_center_ftp_pwd);

	str_center_ip.Trim(" \t");
	str_center_ftp_id.Trim(" \t");
	str_center_ftp_pwd.Trim(" \t");

	////ciDEBUG(10, ("%s=%s", KEY_UBC_CURRENT_VERSION,	str_current_version));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_IP,		str_center_ip));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_HTTP_PORT,	str_center_http_port));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_FTP_IP,	str_center_ftp_ip));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_FTP_PORT,	str_center_ftp_port));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_FTP_ID,	str_center_ftp_id));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_FTP_PWD,	str_center_ftp_pwd));

	// check required-fields
	if( str_center_ip == "" )
	{
		::AfxMessageBox(IDS_MSG_CENTER_IP_IS_EMPTY, MB_ICONWARNING);
		return;
	}
	if( str_center_ftp_id == "" )
	{
		::AfxMessageBox(IDS_MSG_CENTER_FTP_ID_IS_EMPTY, MB_ICONWARNING);
		return;
	}
	if( str_center_ftp_pwd == "" )
	{
		::AfxMessageBox(IDS_MSG_CENTER_FTP_PWD_IS_EMPTY, MB_ICONWARNING);
		return;
	}

	//m_ubcCenterInfo.set(KEY_UBC_CURRENT_VERSION,	str_current_version);
	m_ubcCenterInfo.set(KEY_UBC_CENTER_IP,			str_center_ip);
	m_ubcCenterInfo.set(KEY_UBC_CENTER_HTTP_PORT,	str_center_http_port);
	m_ubcCenterInfo.set(KEY_UBC_CENTER_FTP_IP,		str_center_ftp_ip);
	m_ubcCenterInfo.set(KEY_UBC_CENTER_FTP_PORT,	str_center_ftp_port);
	m_ubcCenterInfo.set(KEY_UBC_CENTER_FTP_ID,		str_center_ftp_id);
	m_ubcCenterInfo.set(KEY_UBC_CENTER_FTP_PWD,		str_center_ftp_pwd);

	BeginWaitCursor();

	int ret = m_ubcCenterInfo.install();

	EndWaitCursor();

	if( ret )
	{
		// success
		//ciDEBUG(10, ("Success to apply Center Info"));
		m_bUBCCenterInfo = true;

		m_strUBCCenterIP       = str_center_ip;
		m_strUBCCenterHTTPPort = str_center_http_port;
		m_strUBCCenterFTPIP    = str_center_ftp_ip;
		m_strUBCCenterFTPPort  = str_center_ftp_port;
		m_strUBCCenterFTPID    = str_center_ftp_id;
		m_strUBCCenterFTPPwd   = str_center_ftp_pwd;
	}
	else
	{
		// fail
		//ciDEBUG(10, ("Fail to apply Center Info"));
		m_bUBCCenterInfo = false;
	}


	CheckInstallComplete();
	UpdateControls();


	if( m_bUBCCenterInfo )
	{
		::AfxMessageBox(IDS_MSG_SUCCESS_ALL, MB_ICONINFORMATION);
	}
	else
	{
		CString msg;
		msg.LoadString(IDS_MSG_FAIL_TO_APPLY);
		msg += "\r\n\r\n";
		msg += m_ubcCenterInfo.getErrorString();

		CErrorMessageDlg dlg("Install Error !!!", m_ubcCenterInfo.getErrorCode(), msg, MB_ICONSTOP);
		dlg.DoModal();
	}
}

bool CUBCBaseSettingsDlg::RunInit()
{
	bool ret_val = true;

	//
	m_ubcCenterInfo.test();

	CString str_current_version;

	m_ubcCenterInfo.get(KEY_UBC_CURRENT_VERSION,	str_current_version);
	m_ubcCenterInfo.get(KEY_UBC_CENTER_IP,			m_strUBCCenterIP);
	m_ubcCenterInfo.get(KEY_UBC_CENTER_HTTP_PORT,	m_strUBCCenterHTTPPort);
	m_ubcCenterInfo.get(KEY_UBC_CENTER_FTP_IP,		m_strUBCCenterFTPIP);
	m_ubcCenterInfo.get(KEY_UBC_CENTER_FTP_PORT,	m_strUBCCenterFTPPort);
	m_ubcCenterInfo.get(KEY_UBC_CENTER_FTP_ID,		m_strUBCCenterFTPID);
	m_ubcCenterInfo.get(KEY_UBC_CENTER_FTP_PWD,		m_strUBCCenterFTPPwd);

	//ciDEBUG(10, ("%s=%s", KEY_UBC_CURRENT_VERSION,	str_current_version));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_IP,		m_strOldUBCCenterIP));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_HTTP_PORT,	m_strOldUBCCenterHTTPPort));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_FTP_IP,	m_strOldUBCCenterFTPIP));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_FTP_PORT,	m_strOldUBCCenterFTPPort));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_FTP_ID,	m_strOldUBCCenterFTPID));
	//ciDEBUG(10, ("%s=%s", KEY_UBC_CENTER_FTP_PWD,	m_strOldUBCCenterFTPPwd));

	m_editCurrentUBCVersion.SetWindowText(	str_current_version );
	m_editUBCCenterIP.SetWindowText(		m_strUBCCenterIP );
	m_editUBCCenterHTTPPort.SetWindowText(	m_strUBCCenterHTTPPort );
	m_editUBCCenterFTPIP.SetWindowText(		m_strUBCCenterFTPIP );
	m_editUBCCenterFTPPort.SetWindowText(	m_strUBCCenterFTPPort );
	m_editUBCCenterFTPID.SetWindowText(		m_strUBCCenterFTPID );
	m_editUBCCenterFTPPwd.SetWindowText(	m_strUBCCenterFTPPwd );

	if(	m_strUBCCenterIP == "" || 
		//m_strUBCCenterHTTPPort == "" ||
		//m_strUBCCenterFTPPort == "" || 
		//m_strUBCCenterFTPIP == "" || 
		m_strUBCCenterFTPID == "" || 
		m_strUBCCenterFTPPwd == "" )
	{
		ret_val = false;
		m_bUBCCenterInfo = false;
	}
	else
	{
		m_bUBCCenterInfo = true;
	}

	//
	m_GlobalOption.test();

	CString str_global = m_GlobalOption.get(KEY_IS_GLOBAL);
	//ciDEBUG(10, ("GLOBAL=%s", str_global));
	if(str_global == "1")
	{
		ret_val = true;
		m_nGlobalOption = 1;
		m_btnUseGlobal.SetCheck(BST_CHECKED);
	}
	else if(str_global == "0")
	{
		ret_val = true;
		m_nGlobalOption = 0;
		m_btnDisuseGlobal.SetCheck(BST_CHECKED);
	}
	else
	{
		// default value
		ret_val = false;
		m_btnDisuseGlobal.SetCheck(BST_CHECKED);
	}

	return ret_val;
}

void CUBCBaseSettingsDlg::OnBnClickedRadioGlobal()
{
	m_btnSetGlobal.UpdateControl();
}

void CUBCBaseSettingsDlg::OnBnClickedButtonSetGlobal()
{
	// get IsGlobal
	m_GlobalOption.set(KEY_IS_GLOBAL, m_btnUseGlobal.GetCheck() ? "1" : "0" );

	if( m_GlobalOption.install() )
	{
		m_nGlobalOption = m_btnUseGlobal.GetCheck() ? 1 : 0;
		::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
	}
	else
	{
		::AfxMessageBox(m_GlobalOption.getErrorString(), MB_ICONSTOP);
	}

	CheckInstallComplete();
	UpdateControls();
}

void CUBCBaseSettingsDlg::OnEnChangeEditUbcCenter()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CSubDlg::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	// 연관된 CButton_String 컨트롤
	m_btnSetUBCCenterInfo.UpdateControl();
}

void CUBCBaseSettingsDlg::UpdateControls()
{
	// CButton_XXXX 버튼리스트 나열
	m_btnSetUBCCenterInfo.UpdateControl();
	m_btnSetGlobal.UpdateControl();
}

int CUBCBaseSettingsDlg::CheckInstallComplete()
{
	m_bInstallComplete = true;

	if( m_bUBCCenterInfo == false ||
		m_nGlobalOption < 0 )
	{
		m_bInstallComplete = false;
	}

	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}
