#include "StdAfx.h"
#include "InstallAutoUpdateDisable.h"
#include "ServerConfigurator/libIisSetting/Redir/CommandRedir.h"
#include "Winsvc.h"
#include <ci/libDebug/ciDebug.h>


ciSET_DEBUG(10, "libInstallAutoUpdateDisable");


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기본생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallAutoUpdateDisable::CInstallAutoUpdateDisable()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CInstallAutoUpdateDisable::~CInstallAutoUpdateDisable()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우즈 자동업데이트를 못하도록 설정이 되어있는지 확인한다. \n
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallAutoUpdateDisable::test()
{
	ciDEBUG(1, ("begin AutoUpdateDisable test"));

	clearError();

	//레지스트리 쿼리는 프로그램으로 안되서 서비스상태를 구해오는것으로 수정함.

	SC_HANDLE hScm = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
	if( hScm == NULL )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to open SCManager");
		ciDEBUG(1, (szBuf));
		setError(ERR_TEST_FAIL, szBuf);
		return FALSE;
	}//if

	SC_HANDLE hService = OpenService(hScm, "wuauserv", SC_MANAGER_ALL_ACCESS);
	if( hService == NULL )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to open service [wuauserv]");
		ciDEBUG(1, (szBuf));
		setError(::GetLastError(), szBuf);
		return FALSE;
	}//if

    LPQUERY_SERVICE_CONFIG lpQrService;
	lpQrService = (LPQUERY_SERVICE_CONFIG)LocalAlloc(LPTR, 4096); 

	DWORD dwNeed;
	if( !QueryServiceConfig(hService, lpQrService, 4096, &dwNeed) )
	{
		LocalFree(lpQrService);
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to query service config");
		ciDEBUG(1, (szBuf));
		setError(::GetLastError(), szBuf); 
		return FALSE;
	}//if
/*
	switch(lpQrService->dwStartType)
	{
	case SERVICE_BOOT_START:
		::AfxMessageBox("SERVICE_BOOT_START");
		break;
	case SERVICE_SYSTEM_START:
		::AfxMessageBox("SERVICE_SYSTEM_START");
		break;
	case SERVICE_AUTO_START:
		::AfxMessageBox("SERVICE_AUTO_START");
		break;
	case SERVICE_DEMAND_START:
		::AfxMessageBox("SERVICE_DEMAND_START");
		break;
	case SERVICE_DISABLED:
		::AfxMessageBox("SERVICE_DISABLED");
		break;
	}
*/
	if( lpQrService->dwStartType != SERVICE_DISABLED )
	{
		LocalFree(lpQrService);
		char szBuf[1024] = { 0x00 };
		sprintf(szBuf, "Uninstalled AutoUpdate disabled : service start type is not disabled");
		ciDEBUG(1, (szBuf));
		setError(ERR_UNINSTALLED, szBuf);
		return FALSE;
	}//if
	LocalFree(lpQrService);

	SERVICE_STATUS ss;
	QueryServiceStatus(hService, &ss);
	if( ss.dwCurrentState != SERVICE_STOPPED )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Uninstalled AutoUpdate disabled : service state is not stop");
		ciDEBUG(1, (szBuf));
		setError(ERR_UNINSTALLED, szBuf);
		return FALSE;
	}//if
 
    CloseServiceHandle(hService);
    CloseServiceHandle(hScm);
/*
	CCommandRedirector redir;
	CString strCmd = "REG QUERY \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsUpdate\\Auto Update\" /v AUOptions /t REG_DWORD";
	if(!redir.Open(strCmd))
	{
		//레지스트리 쿼리 실패
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to query registry");
		ciDEBUG(1, (szBuf));
		setError(ERR_TEST_FAIL, szBuf);
		redir.Close();
		return FALSE;
	}
	redir.Close();

	//레지스트리 쿼리가 성공하면 아래와 같이 출력되므로...
	//HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\AutoUpdate
    // AUOptions    REG_DWORD    0x3
	int nIdx = 0;
	nIdx = redir.m_strResult.Find("0x", 0);
	if(nIdx == -1)
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Can't found registry value (\"AutoUpdate Query\") from query result ==> %s", redir.m_strResult);
		ciDEBUG(1, (szBuf));
		setError(ERR_TEST_FAIL, szBuf);
		redir.Close();
		return FALSE;
	}//if
	CString strResult = redir.m_strResult;
	strResult.Delete(0, nIdx+3);

	// 4 : 업데이트 자동 설치(권장)
	// 3 : 업데이트를 다운로드하지만 설치 여부는 직접 선택
	// 2 : 업데이트를 확인하지만 다운로드 및 설치 여부는 직접 선택
	// 1 : 업데이트를 확인하지 않음(권장하지 않음)
	int nVal = atoi(strResult);
	if(nVal != 1)
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Not set AutoUpdate value to 1 (The value is set to %d)", nVal);
		ciDEBUG(1, (szBuf));
		setError(ERR_UNINSTALLED, szBuf);
		redir.Close();
		return FALSE;
	}//if
*/
	ciDEBUG(1, ("Success test AutoUpdateDisable"));
	ciDEBUG(1, ("end AutoUpdateDisable test"));
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우즈 자동업데이트를 못하도록 설정한다. \n
/// @return <형: int> \n
///			<FALSE: 오류> \n
///			<TRUE: 성공> \n
/////////////////////////////////////////////////////////////////////////////////
int CInstallAutoUpdateDisable::install()
{
	ciDEBUG(1, ("begin AutoUpdateDisable install"));
	clearError();

	//레지스트리 수정은 프로그램으로 안되서 서비스를 제어하도록 변경함.

	SC_HANDLE hScm = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
	if( hScm == NULL )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to open SCManager");
		ciDEBUG(1, (szBuf));
		setError(::GetLastError(), szBuf);
		return FALSE;
	}//if

	SC_HANDLE hService = OpenService(hScm, "wuauserv", SC_MANAGER_ALL_ACCESS);
	if( hService == NULL )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to open service [wuauserv]");
		ciDEBUG(1, (szBuf));
		setError(::GetLastError(), szBuf);
		return FALSE;
	}//if

	//서비스를 사용안함으로 수정
    if( !ChangeServiceConfig(hService, SERVICE_NO_CHANGE, SERVICE_DISABLED, SERVICE_NO_CHANGE, NULL, NULL, NULL, NULL, NULL, NULL, NULL) )
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to change service config");
		ciDEBUG(1, (szBuf));
		setError(::GetLastError(), szBuf);
		return FALSE;
	}//if

	//서비스를 중지시킴.
	SERVICE_STATUS ss;
	QueryServiceStatus(hService, &ss);
	if( ss.dwCurrentState != SERVICE_STOPPED )
	{
		if( !ControlService(hService, SERVICE_CONTROL_STOP, &ss) )
		{
			char szBuf[1024] = {0};
			sprintf(szBuf, "Fail to stop service [wuauserv]");
			ciDEBUG(1, (szBuf));
			setError(::GetLastError(), szBuf);
			return FALSE;
		}//if
	}//if
 
    CloseServiceHandle(hService);
    CloseServiceHandle(hScm);
/*
	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);

	CString strPath;
	strPath.Format("%s%s\\AutoUpdateDisable.bat", cDrive, cPath);
	//배치파일 생성
	if(!CreateBatchFile(strPath))
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to create batch fille : [%s]", strPath);
		ciDEBUG(1, (szBuf));
		setError(ERR_FAIL_INSTALL, szBuf);
		ciDEBUG(1, ("Fail to AutoUpdateDisable install"));
		return FALSE;
	}//if

	//배치파일 실행
	STARTUPINFO         sINFO = {0};
	PROCESS_INFORMATION pINFO = {0};

	sINFO.cb = sizeof( sINFO );

	if(!CreateProcess(NULL, (LPSTR)(LPCSTR)strPath, NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &sINFO, &pINFO ) ) // CREATE_NO_WINDOW = 윈도우없이 실행
	{
		char szBuf[1024] = {0};
		sprintf(szBuf, "Fail to run batch fille : [%s]", strPath);
		ciDEBUG(1, (szBuf));
		setError(ERR_FAIL_INSTALL, szBuf);
		ciDEBUG(1, ("Fail to AutoUpdateDisable install"));
		return FALSE;
	}//if

	//실행종료 대기
	WaitForSingleObject(pINFO.hProcess, INFINITE);

	CloseHandle(pINFO.hProcess);
	CloseHandle(pINFO.hThread);
*/
	ciDEBUG(1, ("Success install AutoUpdateDisable"));
	ciDEBUG(1, ("end AutoUpdateDisable install"));
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우즈 자동업데이트를 못하도록하는 레지스트리를 수정하는 배치파일을 만든다. \n
/// @param (CString) strPath : (in) 배치파일을 생성할 경로
/// @return <형: bool> \n
///			<false: 성공> \n
///			<true: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CInstallAutoUpdateDisable::CreateBatchFile(CString strPath)
{
	ciDEBUG(1, ("begin Create batch file"));

	CFileStatus fs;
	if( CFile::GetStatus(strPath, fs) == TRUE )
	{
		ciDEBUG(1, ("Aleady exist AutoUpdateDisable.bat"));
		return true;
	}//if

	CFile fileBat;
	if( !fileBat.Open(strPath, CFile::modeCreate | CFile::modeWrite) )
	{
		ciDEBUG(1, ("Fail to create batch file"));
		return false;
	}//if

	// 4 : 업데이트 자동 설치(권장)
	// 3 : 업데이트를 다운로드하지만 설치 여부는 직접 선택
	// 2 : 업데이트를 확인하지만 다운로드 및 설치 여부는 직접 선택
	// 1 : 업데이트를 확인하지 않음(권장하지 않음)
	// ........AUOptions /t REG_DWORD /d 0x01 <== 이 값을 수정하여 option 조정
	CString strText = "@echo off\r\nREM Disable Windows Autoupdate\r\n";
	strText += "REG ADD \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsUpdate\\Auto Update\" /v AUOptions /t REG_DWORD /d 0x01 /f";
	fileBat.Write(strText, strlen(strText));
	fileBat.Close();

	ciDEBUG(1, ("end Create batch file"));
	return true;
}