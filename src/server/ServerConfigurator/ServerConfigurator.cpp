// ServerConfigurator.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "MainFrm.h"

#include "ServerConfiguratorDoc.h"
#include "ServerConfiguratorView.h"


#include <ci/libDebug/ciArgParser.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CServerConfiguratorApp

BEGIN_MESSAGE_MAP(CServerConfiguratorApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CServerConfiguratorApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()


// CServerConfiguratorApp construction

CServerConfiguratorApp::CServerConfiguratorApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CServerConfiguratorApp object

CServerConfiguratorApp theApp;


// CServerConfiguratorApp initialization

BOOL SetThreadLocaleEx(LCID locale)
{
	USES_CONVERSION;
	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if(::GetVersionEx(&osv) == FALSE) //get Version Failed 
		return ::SetThreadLocale(locale);

	switch(osv.dwMajorVersion)
	{
	case 6:  //WINDOWS VISTA
		{
			unsigned (__stdcall* SetThreadUILanguage)(LANGID);
			CStringA strThreaUILanguage = T2A(_T("SetThreadUILanguage"));

			HINSTANCE hIns = ::LoadLibrary(_T("kernel32.dll"));  
			if(hIns == NULL) //no Instance
				return ::SetThreadLocale(locale);

			SetThreadUILanguage = (unsigned (__stdcall* )(LANGID))::GetProcAddress(hIns,strThreaUILanguage);   
			if(SetThreadUILanguage == NULL) //procAddress Getfailed 
				return ::SetThreadLocale(locale);

			LANGID lang = SetThreadUILanguage(locale);
			return (lang==LOWORD(locale))?TRUE:FALSE;
		} 
	default: //WINDOWS 2000, XP
		return ::SetThreadLocale(locale);
	}

}

BOOL CServerConfiguratorApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CServerConfiguratorDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CServerConfiguratorView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);


	// arguments - language
	LANGID lang_id = PRIMARYLANGID(GetSystemDefaultLangID());
	ciArgParser::initialize(__argc,__argv);
	if(ciArgParser::getInstance()->isSet("+eng") || ciArgParser::getInstance()->isSet("+ENG"))
		lang_id = LANG_ENGLISH;
	else if(ciArgParser::getInstance()->isSet("+jpn") || ciArgParser::getInstance()->isSet("+JPN"))
		lang_id = LANG_JAPANESE;
	else if(ciArgParser::getInstance()->isSet("+kor") || ciArgParser::getInstance()->isSet("+KOR"))
		lang_id = LANG_KOREAN;

	switch(lang_id)
	{
	case LANG_KOREAN:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_KOREAN, SUBLANG_KOREAN) , SORT_DEFAULT));
		break;
	case LANG_JAPANESE:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_JAPANESE, SORT_JAPANESE_UNICODE) , SORT_DEFAULT));
		break;
	default:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
		break;
	}

	// pre-load string from string-table-resource
	::LoadString(IDS_STATUS_CHECKING);
	::LoadString(IDS_STATUS_COMPLETE);
	::LoadString(IDS_STATUS_INCOMPLETE);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	cmdInfo.m_nShellCommand = CCommandLineInfo::FileNew;


	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	return TRUE;
}



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CServerConfiguratorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CServerConfiguratorApp message handlers

