// NASSettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "NASSettingsDlg.h"

#include "ErrorMessageDlg.h"

#include "libWindowsAutoLogin/WindowsAutoLogin.h"


// CNASSettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNASSettingsDlg, CSubDlg)

CNASSettingsDlg::CNASSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*CNASSettingsDlg::IDD,*/ pParent)
{
	m_nNASOption = -1;
}

CNASSettingsDlg::~CNASSettingsDlg()
{
}

void CNASSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RADIO_DISUSE_NAS, m_radioDisuseNAS);
	DDX_Control(pDX, IDC_RADIO_USE_NAS, m_radioUseNAS);
	DDX_Control(pDX, IDC_EDIT_NAS_LOCATION, m_editNASLocation);
	DDX_Control(pDX, IDC_EDIT_NAS_ID, m_editNASID);
	DDX_Control(pDX, IDC_EDIT_NAS_PASSWORD, m_editNASPassword);
	DDX_Control(pDX, IDC_COMBO_DRIVE_LETTER, m_cbxDriveLetter);
	DDX_Control(pDX, IDC_BUTTON_NAS_CONNECT, m_btnNASConnect);
}


BEGIN_MESSAGE_MAP(CNASSettingsDlg, CSubDlg)
	ON_BN_CLICKED(IDC_RADIO_DISUSE_NAS, &CNASSettingsDlg::OnBnClickedRadioNas)
	ON_BN_CLICKED(IDC_RADIO_USE_NAS, &CNASSettingsDlg::OnBnClickedRadioNas)
	ON_BN_CLICKED(IDC_BUTTON_NAS_CONNECT, &CNASSettingsDlg::OnBnClickedButtonNasConnect)
END_MESSAGE_MAP()


// CNASSettingsDlg 메시지 처리기입니다.

BOOL CNASSettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	AddColorIgnoreWnd(m_editNASLocation.GetSafeHwnd());
	AddColorIgnoreWnd(m_editNASID.GetSafeHwnd());
	AddColorIgnoreWnd(m_editNASPassword.GetSafeHwnd());
	AddColorIgnoreWnd(m_cbxDriveLetter.GetSafeHwnd());

	AddControl( m_radioDisuseNAS.GetSafeHwnd() );
	AddControl( m_radioUseNAS.GetSafeHwnd() );
	AddControl( m_editNASLocation.GetSafeHwnd() );
	AddControl( m_editNASID.GetSafeHwnd() );
	AddControl( m_editNASPassword.GetSafeHwnd() );
	AddControl( m_cbxDriveLetter.GetSafeHwnd() );
	AddControl( m_btnNASConnect.GetSafeHwnd() );

	m_btnNASConnect.Init(&m_bInstallComplete, &m_bValueChange);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

bool CNASSettingsDlg::CheckValueChange()
{
	//
	m_bValueChange = false;

	//
	int nas_opt = 0;
	if( m_radioUseNAS.GetCheck() )
		nas_opt = 0;
	else if( m_radioDisuseNAS.GetCheck() )
		nas_opt = 1;

	if( m_nNASOption != nas_opt ) m_bValueChange = true;

	//
	CString str_nas_location;
	m_editNASLocation.GetWindowText(str_nas_location);

	CString str_nas_id;
	m_editNASID.GetWindowText(str_nas_id);

	CString str_nas_pwd;
	m_editNASPassword.GetWindowText(str_nas_pwd);

	CString str_drive_letter;
	int drv_sel = m_cbxDriveLetter.GetCurSel();
	if( drv_sel >= 0 )
		m_cbxDriveLetter.GetLBText(drv_sel, str_drive_letter);

	if( str_nas_location != m_strNASLocation ||
		str_nas_id != m_strNASID ||
		str_nas_pwd != m_strNASPWD ||
		str_drive_letter != m_strDriveLetter )
	{
		m_bValueChange = true;
	}

	return m_bValueChange;
}

void CNASSettingsDlg::OnBnClickedRadioNas()
{
	if( m_radioDisuseNAS.GetCheck() )
	{
		m_editNASLocation.EnableWindow(FALSE);
		m_editNASID.EnableWindow(FALSE);
		m_editNASPassword.EnableWindow(FALSE);
		m_cbxDriveLetter.EnableWindow(FALSE);
	}
	else
	{
		m_editNASLocation.EnableWindow(TRUE);
		m_editNASID.EnableWindow(TRUE);
		m_editNASPassword.EnableWindow(TRUE);
		m_cbxDriveLetter.EnableWindow(TRUE);
	}

	CheckValueChange();
	CheckInstallComplete();
	UpdateControls();
}

void CNASSettingsDlg::OnBnClickedButtonNasConnect()
{
	CWindowsAutoLogin win_auto_login;
	if( !win_auto_login.test() )
	{
		// fail -> you must complete to windows-auto-login
		::AfxMessageBox(IDS_MSG_NOT_COMPLETE_WINDOWS_AUTO_LOGIN, MB_ICONWARNING);
		return;
	}

	CString str_auto_login_id, str_auto_login_pwd;
	win_auto_login.get(KEY_WINDOWS_AUTO_LOGIN_ID, str_auto_login_id);
	win_auto_login.get(KEY_WINDOWS_AUTO_LOGIN_PASSWORD, str_auto_login_pwd);

	CString str_nas_location;
	m_editNASLocation.GetWindowText(str_nas_location);
	m_NasInstall.set(NAS_NETWORK_PATH, str_nas_location);

	CString str_nas_id;
	m_editNASID.GetWindowText(str_nas_id);
	m_NasInstall.set(NAS_ID, str_nas_id);

	CString str_nas_pwd;
	m_editNASPassword.GetWindowText(str_nas_pwd);
	m_NasInstall.set(NAS_PASSWORD, str_nas_pwd);

	CString str_drive_letter;
	if(m_cbxDriveLetter.GetCurSel() < 0)
	{
		//
		// msg -> please, select drive letter
		//
		return;
	}
	m_cbxDriveLetter.GetLBText(m_cbxDriveLetter.GetCurSel(), str_drive_letter);
	m_NasInstall.set(NAS_DRIVE, str_drive_letter);

	if( m_radioUseNAS.GetCheck() )
	{
		m_NasInstall.set(NAS_USE, 1);

		// check auto-login-id == nas-id ?
		// if equal, must auto-login-pwd == nas-pwd
		if( str_auto_login_id == str_nas_id )
		{
			if( str_auto_login_pwd != str_nas_pwd )
			{
				::AfxMessageBox(IDS_MSG_EQUAL_ID_EQUAL_PWD, MB_ICONWARNING);
				return;
			}
		}
	}
	else
	{
		m_NasInstall.set(NAS_USE, 0);
	}

	BeginWaitCursor();

	int ret = m_NasInstall.install();

	EndWaitCursor();

	if( !ret )
	{
		// install or uninstall fail
		CErrorMessageDlg dlg("Install Error !!!", m_NasInstall.getErrorCode(), m_NasInstall.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();

		return;
	}

	// success
	m_bInstallComplete = true;
	m_bValueChange = false;

	CheckInstallComplete();
	UpdateControls();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

bool CNASSettingsDlg::RunInit()
{
	bool ret_val = true;

	//
	int use_nas = 0;
	m_NasInstall.get(NAS_USE, use_nas);
	if(use_nas)
	{
		m_radioUseNAS.SetCheck(BST_CHECKED);

		m_editNASLocation.EnableWindow(TRUE);
		m_editNASID.EnableWindow(TRUE);
		m_editNASPassword.EnableWindow(TRUE);
		m_cbxDriveLetter.EnableWindow(TRUE);
	}
	else
	{
		m_radioDisuseNAS.SetCheck(BST_CHECKED);

		m_editNASLocation.EnableWindow(FALSE);
		m_editNASID.EnableWindow(FALSE);
		m_editNASPassword.EnableWindow(FALSE);
		m_cbxDriveLetter.EnableWindow(FALSE);
	}

	//
	string nas_location;
	m_NasInstall.get(NAS_NETWORK_PATH, nas_location);
	m_editNASLocation.SetWindowText(nas_location.c_str());

	//
	string nas_id;
	m_NasInstall.get(NAS_ID, nas_id);
	m_editNASID.SetWindowText(nas_id.c_str());

	//
	string nas_pwd;
	m_NasInstall.get(NAS_PASSWORD, nas_pwd);
	m_editNASPassword.SetWindowText(nas_pwd.c_str());

	//
	DWORD uDriveMask = GetLogicalDrives();
	TCHAR szDrive[] = _T("A:");
	if(uDriveMask)
	{
		uDriveMask = ~uDriveMask;
		for(char ch='a'; ch<='z' && uDriveMask; ch++)
		{
			if(uDriveMask & 1)
			{
				m_cbxDriveLetter.AddString(szDrive);
			}
			++szDrive[0];
			uDriveMask >>= 1;
		}

		if(m_cbxDriveLetter.GetCount() > 0)
		{
			m_cbxDriveLetter.SetCurSel(m_cbxDriveLetter.GetCount()-1);
		}
	}

	string nas_drive;
	m_NasInstall.get(NAS_DRIVE, nas_drive);
	if(nas_drive.length() > 0)
	{
		m_cbxDriveLetter.AddString(nas_drive.c_str());
		for(int i=0; i<m_cbxDriveLetter.GetCount(); i++)
		{
			CString str;
			m_cbxDriveLetter.GetLBText(i, str);
			if( stricmp(str, nas_drive.c_str()) == 0 )
			{
				m_cbxDriveLetter.SetCurSel(i);
				break;
			}
		}
	}

	//test
	if( m_NasInstall.test() )
	{
		m_bInstallComplete = true;
	}

	return TRUE;
}

void CNASSettingsDlg::UpdateControls()
{
	m_btnNASConnect.UpdateControl();
}

int CNASSettingsDlg::CheckInstallComplete()
{
	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}
