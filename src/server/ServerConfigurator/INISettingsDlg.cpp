// INISettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "INISettingsDlg.h"

#include "InputSingleDlg.h"
#include "InputKeyValueDlg.h"
#include "ErrorMessageDlg.h"

#include "caf/ci/libConfig/ciXProperties.h"
#include "common/libCommon/ubcIni.h"
#include "common/libProfileManager/ProfileManager.h"


// CINISettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CINISettingsDlg, CSubDlg)

CINISettingsDlg::CINISettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*CINISettingsDlg::IDD,*/ pParent)
{
	m_pCurrentItemData = NULL;
	m_bValueChange = true;
}

CINISettingsDlg::~CINISettingsDlg()
{
}

void CINISettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_STATIC_FILE_LIST, m_stcFileList);
	DDX_Control(pDX, IDC_LIST_INI_FILE, m_lcINIFile);
	DDX_Control(pDX, IDC_BUTTON_ADD_FILE, m_btnAddFile);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_FILE_FROM_LIST, m_btnRemoveFileFromList);
	DDX_Control(pDX, IDC_STATIC_FILE, m_stcFile);
	DDX_Control(pDX, IDC_STATIC_GRID, m_grid);
	DDX_Control(pDX, IDC_BUTTON_ADD_SECTION, m_btnAddSection);
	DDX_Control(pDX, IDC_BUTTON_ADD_ITEM, m_btnAddItem);
	DDX_Control(pDX, IDC_BUTTON_DELETE_SELECTED_ITEM, m_btnDeleteSelectedItem);
	DDX_Control(pDX, IDC_BUTTON_SAVE_SETTINGS, m_btnSaveSettings);
}


BEGIN_MESSAGE_MAP(CINISettingsDlg, CSubDlg)
	ON_BN_CLICKED(IDC_BUTTON_ADD_FILE, &CINISettingsDlg::OnBnClickedButtonAddFile)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_FILE_FROM_LIST, &CINISettingsDlg::OnBnClickedButtonRemoveFileFromList)
	ON_BN_CLICKED(IDC_BUTTON_ADD_SECTION, &CINISettingsDlg::OnBnClickedButtonAddSection)
	ON_BN_CLICKED(IDC_BUTTON_ADD_ITEM, &CINISettingsDlg::OnBnClickedButtonAddItem)
	ON_BN_CLICKED(IDC_BUTTON_DELETE_SELECTED_ITEM, &CINISettingsDlg::OnBnClickedButtonDeleteSelectedItem)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_SETTINGS, &CINISettingsDlg::OnBnClickedButtonSaveSettings)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_INI_FILE, &CINISettingsDlg::OnLvnItemchangedListIni)
	ON_WM_SIZE()
	ON_MESSAGE(WM_PG_ITEMCHANGED, OnPGItemChanged)
END_MESSAGE_MAP()


// CINISettingsDlg 메시지 처리기입니다.

char* szDefaultINIList[] = {
	"D:\\Project\\ubc\\config\\data\\UBCConnect.ini",
	"D:\\Project\\ubc\\config\\data\\UBCServerMonitor2.ini",
	"D:\\Project\\ubc\\config\\data\\UBCVariables.ini",
	"D:\\Project\\ubc\\config\\ubc.properties",
};

BOOL CINISettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	m_reposControl.SetParent(this);

	m_reposControl.AddControl((CWnd*)&m_stcFileList, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_lcINIFile, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnAddFile, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnRemoveFileFromList, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);

	m_reposControl.AddControl((CWnd*)&m_stcFile, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_grid, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnAddSection, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnAddItem, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnDeleteSelectedItem, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnSaveSettings, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	AddControl( m_lcINIFile.GetSafeHwnd() );
	AddControl( m_btnAddFile.GetSafeHwnd() );
	AddControl( m_btnRemoveFileFromList.GetSafeHwnd() );

	AddControl( m_grid.GetSafeHwnd() );
	AddControl( m_btnAddSection.GetSafeHwnd() );
	AddControl( m_btnAddItem.GetSafeHwnd() );
	AddControl( m_btnDeleteSelectedItem.GetSafeHwnd() );
	AddControl( m_btnSaveSettings.GetSafeHwnd() );

	m_lcINIFile.SetExtendedStyle( m_lcINIFile.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcINIFile.InsertColumn(0, ::LoadString(IDS_COLUMN_FULL_PATH), 0, 0);
	m_lcINIFile.InsertColumn(1, ::LoadString(IDS_COLUMN_INI_FILE_LIST), 0, 100);
	m_lcINIFile.SetColumnWidth(1, LVSCW_AUTOSIZE_USEHEADER);

	m_btnSaveSettings.Init(&m_bInstallComplete, &m_bValueChange);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

bool CINISettingsDlg::AddFile(LPCSTR lpszFullPath)
{
	int idx = m_lcINIFile.GetItemCount();

	// check duplicate
	for(int i=0; i<idx; i++)
	{
		CString fullpath = m_lcINIFile.GetItemText(i, 0);
		if( stricmp(fullpath, lpszFullPath) == 0 )
		{
			//
			// messagebox - already added
			//
			return false;
		}
	}

	//
	char dir[MAX_PATH]={0}, path[MAX_PATH]={0}, filename[MAX_PATH]={0}, ext[MAX_PATH]={0};
	_splitpath(lpszFullPath, dir, path, filename, ext);

	strcat(filename, ext);

	m_lcINIFile.InsertItem(idx, lpszFullPath);
	m_lcINIFile.SetItemText(idx, 1, filename);
	m_lcINIFile.SetItemData(idx, 0);

	return true;
}

void CINISettingsDlg::OnBnClickedButtonAddFile()
{
	CFileDialog dlg(TRUE, NULL, NULL, OFN_ALLOWMULTISELECT | OFN_FILEMUSTEXIST, "INI,Properties Files (*.ini, *.properties)|*.ini;*.properties|All Files (*.*)|*.*||", this);
	if(dlg.DoModal() == IDOK)
	{
		int idx = m_lcINIFile.GetItemCount();
		for(POSITION pos=dlg.GetStartPosition(); pos!=NULL; idx++)
		{
			if( AddFile(dlg.GetNextPathName(pos)) )
			{
				m_bValueChange = true;
			}
		}

		CheckInstallComplete();
		UpdateControls();
	}
}

void CINISettingsDlg::OnBnClickedButtonRemoveFileFromList()
{
	int count = m_lcINIFile.GetItemCount();
	for(int i=count-1; i>=0; i--)
	{
		if( m_lcINIFile.GetItemState(i, LVIS_SELECTED) )
		{
			m_bValueChange = true;

			ITEM_DATA* item = (ITEM_DATA*)m_lcINIFile.GetItemData(i);
			m_lcINIFile.DeleteItem(i);

			if(item) delete item;

			m_pCurrentItemData = NULL;
		}
	}

	InitGrid(m_pCurrentItemData);

	CheckInstallComplete();
	UpdateControls();
}

void CINISettingsDlg::OnBnClickedButtonAddSection()
{
	CInputSingleDlg dlg;
	dlg.SetDialogInfo(IDS_MSG_ADD_SECTION, IDS_MSG_SECTION_NAME);

	if(dlg.DoModal() == IDOK)
	{
		CString section_name;
		dlg.GetValue(section_name);

		if(section_name.GetLength() > 0)
		{
			m_bValueChange = true;

			m_grid.AddSection((LPCSTR)section_name);
			m_grid.Invalidate();

			CheckInstallComplete();
			UpdateControls();
		}
	}
}

void CINISettingsDlg::OnBnClickedButtonAddItem()
{
	if(m_pCurrentItemData==NULL) return;

	CPropertyGrid::CSection* section = m_grid.GetFocusedSection();
	CPropertyGrid::CSection* section_ex = m_grid.GetFocusedSectionOfItem();
	if(section == NULL && section_ex == NULL)
	{
		::AfxMessageBox(IDS_MSG_SELECT_SECTION, MB_ICONWARNING);
		return;
	}
	if(section == NULL)
		section = section_ex;

	CInputKeyValueDlg dlg;
	dlg.SetDialogInfo(IDS_MSG_ADD_ITEM_NAME_AND_VALUE, IDS_MSG_ITEM_NAME, "", IDS_MSG_ITEM_VALUE, "");

	if(dlg.DoModal() == IDOK)
	{
		CString item_name;
		CString item_value;
		dlg.GetValue(item_name, item_value);

		if(item_name.GetLength() > 0)
		{
			m_bValueChange = true;

			if(m_pCurrentItemData->GetINI() != NULL)
			{
				m_pCurrentItemData->GetINI()->WriteProfileString(section->m_title.c_str(), item_name, item_value);
			}
			else if(m_pCurrentItemData->GetProperties() != NULL)
			{
				m_pCurrentItemData->GetProperties()->set(item_name, item_value);
			}

			m_grid.AddStringItem(section->m_id, (LPCSTR)item_name, (LPCSTR)item_value );
			m_grid.Invalidate();

			CheckInstallComplete();
			UpdateControls();
		}
	}
}

void CINISettingsDlg::OnBnClickedButtonDeleteSelectedItem()
{
	const CPropertyGrid::CSection* section = m_grid.GetFocusedSection();
	if(section)
	{
		CString msg;
		msg.LoadString(IDS_MSG_DELETE_SECTION);
		msg.Replace("%s", section->m_title.c_str());

		if(::AfxMessageBox(msg, MB_ICONWARNING | MB_YESNO) == IDNO) return;

		if(m_grid.RemoveSection(section->m_id) == false)
		{
			::AfxMessageBox(IDS_ERROR_NOT_EXIST_SECTION, MB_ICONSTOP);
		}
		else
			m_bValueChange = true;

		m_grid.Invalidate();

		CheckInstallComplete();
		UpdateControls();

		return;
	}

	const CPropertyGrid::CItem* item = m_grid.GetFocusedItem();
	if(item)
	{
		CString msg;
		msg.LoadString(IDS_MSG_DELETE_ITEM);
		msg.Replace("%s", item->m_name.c_str());

		if(::AfxMessageBox(msg, MB_ICONWARNING | MB_YESNO) == IDNO) return;

		if(m_grid.RemoveItem(item->m_id) == false)
		{
			::AfxMessageBox(IDS_ERROR_NOT_EXIST_ITEM, MB_ICONSTOP);
		}
		else
			m_bValueChange = true;

		m_grid.Invalidate();

		CheckInstallComplete();
		UpdateControls();

		return;
	}
}

void CINISettingsDlg::OnBnClickedButtonSaveSettings()
{
	UpdateData();

	CString file_list = "";
	int count = m_lcINIFile.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if(file_list.GetLength() > 0)
			file_list += "|";
		file_list += m_lcINIFile.GetItemText(i, 0);
	}

	ubcIni aIni("ServerConfigurator", _COP_CD("C:\\Project\\ubc\\bin8"), "");
	aIni.set("INISettings", "FileList", file_list);

	for(int i=0; i<count; i++)
	{
		ITEM_DATA* item_data = (ITEM_DATA*)m_lcINIFile.GetItemData(i);
		if(item_data == NULL) continue;
		if(!item_data->IsChanged()) continue;

		if(item_data->GetINI() != NULL)
		{
			item_data->GetINI()->Write();
		}
		else if(item_data->GetProperties() != NULL)
		{
			item_data->GetProperties()->write();
		}
	}

	m_bInstallComplete = true;
	m_bValueChange = false;

	CheckInstallComplete();
	UpdateControls();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void CINISettingsDlg::OnLvnItemchangedListIni(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int idx = pNMLV->iItem;

	if(idx < 0)
	{
		InitGrid(NULL);
		return;
	}

	ITEM_DATA* item_data = (ITEM_DATA*)m_lcINIFile.GetItemData(idx);
	if(item_data != NULL)
	{
		InitGrid(item_data);
		return;
	}

	CString filename = m_lcINIFile.GetItemText(idx, 1);
	filename.MakeLower();

	if(filename.Find(".ini") > 0)
	{
		// ini type
		CString fullpath = m_lcINIFile.GetItemText(idx, 0);

		item_data = new ITEM_DATA(new CProfileManager(fullpath), m_lcINIFile.GetItemText(idx, 1));

		m_lcINIFile.SetItemData(idx, (DWORD)item_data);

		InitGrid(item_data);
		return;
	}
	if(filename.Find(".properties") > 0)
	{
		// properties type
		CString fullpath = m_lcINIFile.GetItemText(idx, 0);

		item_data = new ITEM_DATA(new ciXProperties(fullpath, "", ""), m_lcINIFile.GetItemText(idx, 1));

		m_lcINIFile.SetItemData(idx, (DWORD)item_data);

		InitGrid(item_data);
		return;
	}
}

bool CINISettingsDlg::InitGrid(ITEM_DATA* item_data)
{
	m_grid.DeleteEditControl();
	UpdateData();

	m_pCurrentItemData = item_data;

	m_grid.ResetContents();
	m_grid.Invalidate();

	if(item_data == NULL)
	{
		m_stcFile.SetWindowText("");
		m_btnAddSection.EnableWindow(FALSE);
		m_btnAddItem.EnableWindow(FALSE);
		m_btnDeleteSelectedItem.EnableWindow(FALSE);
		return false;
	}

	CString str;
	str.Format(::LoadString(IDS_TEXT_INI_FILE_CONTENTS), item_data->GetFilename() );
	m_stcFile.SetWindowText(str);

	if(item_data->GetINI() != NULL)
	{
		CString tab_name;
		CMapStringToString* group;
		for(POSITION pos = item_data->GetINI()->GetStartPosition(); pos != NULL; )
		{
			item_data->GetINI()->GetNextAssoc( pos, tab_name, (void*&)group );

			HSECTION hs = m_grid.AddSection( (LPCSTR)tab_name );

			//
			POSITION pos_sub;
			CString key;
			CString value;

			for( pos_sub = group->GetStartPosition(); pos_sub != NULL; )
			{
				group->GetNextAssoc( pos_sub, key, value );

				m_grid.AddStringItem(hs, (LPCSTR)key, (LPCSTR)value );
			}
		}

		m_btnAddSection.EnableWindow();
		m_btnAddItem.EnableWindow();
		m_btnDeleteSelectedItem.EnableWindow();

		return true;
	}
	else if(item_data->GetProperties() != NULL)
	{
		HSECTION hs = m_grid.AddSection( "" );

		ciXPropertyIterator itr = item_data->GetProperties()->begin();
		ciXPropertyIterator end = item_data->GetProperties()->end();
		for( ; itr!=end; itr++)
		{
			m_grid.AddStringItem(hs, itr->first.c_str(), itr->second.c_str() );
		}

		m_btnAddSection.EnableWindow(FALSE);
		m_btnAddItem.EnableWindow();
		m_btnDeleteSelectedItem.EnableWindow();
		return true;
	}

	return false;
}

bool CINISettingsDlg::UpdateData()
{
	if(m_pCurrentItemData == NULL)
		return false;

	vector<CPropertyGrid::CSection>::iterator itr = m_grid.GetBeginOfSections();
	vector<CPropertyGrid::CSection>::iterator end = m_grid.GetEndOfSections();

	for( ; itr!=end; itr++)
	{
		vector<CPropertyGrid::CItem>::iterator itr_sub = itr->m_items.begin();
		vector<CPropertyGrid::CItem>::iterator end_sub = itr->m_items.end();

		for( ; itr_sub!=end_sub; itr_sub++)
		{
			if(m_pCurrentItemData->GetINI() != NULL)
			{
				m_pCurrentItemData->GetINI()->WriteProfileString(itr->m_title.c_str(), itr_sub->m_name.c_str(), itr_sub->m_strValue.c_str());
			}
			else if(m_pCurrentItemData->GetProperties() != NULL)
			{
				m_pCurrentItemData->GetProperties()->set(itr_sub->m_name.c_str(), itr_sub->m_strValue.c_str());
			}
		}
	}

	return true;
}

bool CINISettingsDlg::RunInit()
{
	// make def-value(ini-file-list)
	string def_file_list="";
	for(int i=0; i<4; i++)
	{
		if(def_file_list.length() > 0)
			def_file_list += "|";
		def_file_list += _COP_CD(szDefaultINIList[i]);
	}

	//
	ubcIni aIni("ServerConfigurator", _COP_CD("C:\\Project\\ubc\\bin8"), "");

	string file_list;
	aIni.get("INISettings", "FileList", file_list, def_file_list.c_str());

	CString str_file_list = file_list.c_str();
	int pos = 0;
	CString file_path = str_file_list.Tokenize("|", pos);
	while(file_path != "")
	{
		AddFile(file_path);

		file_path = str_file_list.Tokenize("|", pos);
	}

	InitGrid(NULL);

	m_bInstallComplete = true;
	m_bValueChange = false;

	return TRUE;
}

void CINISettingsDlg::UpdateControls()
{
	m_btnSaveSettings.UpdateControl();
}

int CINISettingsDlg::CheckInstallComplete()
{
	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}

void CINISettingsDlg::OnSize(UINT nType, int cx, int cy)
{
	CSubDlg::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )
	{
		m_reposControl.MoveControl(TRUE);
	}
}

LRESULT CINISettingsDlg::OnPGItemChanged(WPARAM wParam, LPARAM lParam)
{
	if(m_pCurrentItemData==NULL) return FALSE;

	m_pCurrentItemData->SetChange();

	m_bInstallComplete = false;
	UpdateControls();
	return FALSE;
}
