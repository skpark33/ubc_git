// FirewallSettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "FirewallSettingsDlg.h"

//#include "FirewallPortDlg.h"
#include "InputSingleDlg.h"
#include "ErrorMessageDlg.h"

//#include "common/libCommon/ubcIni.h"


// CFirewallSettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFirewallSettingsDlg, CSubDlg)

CFirewallSettingsDlg::CFirewallSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*CFirewallSettingsDlg::IDD,*/ pParent)
{
	m_bValueChange = true;

	m_strProgramType = ::LoadString(IDS_TYPE_PROGRAM);
	m_strPortType = ::LoadString(IDS_TYPE_PORT);
}

CFirewallSettingsDlg::~CFirewallSettingsDlg()
{
}

void CFirewallSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_FIREWALL_LIST, m_stcFirewallList);
	DDX_Control(pDX, IDC_LIST_PROGRAMS_PORTS, m_lcProgramsPorts);
	DDX_Control(pDX, IDC_BUTTON_IMPORT_DEFAULT_VALUES, m_btnImportDefaultValues);
	DDX_Control(pDX, IDC_BUTTON_ADD_PROGRAM, m_btnAddProgram);
	DDX_Control(pDX, IDC_BUTTON_ADD_PORT, m_btnAddPort);
	DDX_Control(pDX, IDC_BUTTON_DELETE_SELECTED_ITEMS, m_btnDeleteSelectedItems);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_FIREWALL, m_btnInstallFirewall);
}


BEGIN_MESSAGE_MAP(CFirewallSettingsDlg, CSubDlg)
	ON_BN_CLICKED(IDC_BUTTON_ADD_PROGRAM, &CFirewallSettingsDlg::OnBnClickedButtonAddProgram)
	ON_BN_CLICKED(IDC_BUTTON_ADD_PORT, &CFirewallSettingsDlg::OnBnClickedButtonAddPort)
	ON_BN_CLICKED(IDC_BUTTON_DELETE_SELECTED_ITEMS, &CFirewallSettingsDlg::OnBnClickedButtonDeleteSelectedItems)
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_FIREWALL, &CFirewallSettingsDlg::OnBnClickedButtonInstallFirewall)
	ON_BN_CLICKED(IDC_BUTTON_IMPORT_DEFAULT_VALUES, &CFirewallSettingsDlg::OnBnClickedButtonImportDefaultValues)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CFirewallSettingsDlg 메시지 처리기입니다.

BOOL CFirewallSettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	m_reposControl.SetParent(this);

	m_reposControl.AddControl((CWnd*)&m_stcFirewallList, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_lcProgramsPorts, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnImportDefaultValues, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnAddProgram, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnAddPort, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnDeleteSelectedItems, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl((CWnd*)&m_btnInstallFirewall, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	AddControl( m_lcProgramsPorts.GetSafeHwnd() );
	AddControl( m_btnImportDefaultValues.GetSafeHwnd() );
	AddControl( m_btnAddProgram.GetSafeHwnd() );
	AddControl( m_btnAddPort.GetSafeHwnd() );
	AddControl( m_btnDeleteSelectedItems.GetSafeHwnd() );
	AddControl( m_btnInstallFirewall.GetSafeHwnd() );

	m_lcProgramsPorts.SetExtendedStyle( m_lcProgramsPorts.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcProgramsPorts.InsertColumn(0, ::LoadString(IDS_COLUMN_TYPE), 0, 75);
	m_lcProgramsPorts.InsertColumn(1, ::LoadString(IDS_COLUMN_VALUE), 0, 350);

	m_btnInstallFirewall.Init(&m_bInstallComplete, &m_bValueChange);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFirewallSettingsDlg::OnBnClickedButtonAddProgram()
{
	CFileDialog dlg(TRUE, NULL, NULL, OFN_ALLOWMULTISELECT | OFN_FILEMUSTEXIST, "All Files (*.*)|*.*||", this);
	if( dlg.DoModal() == IDOK )
	{
		POSITION pos = dlg.GetStartPosition();
		while( pos != NULL )
		{
			AddProgram(dlg.GetNextPathName(pos));
		}

		m_bValueChange = true;

		CheckInstallComplete();
		UpdateControls();
	}
}

void CFirewallSettingsDlg::OnBnClickedButtonAddPort()
{
	CInputSingleDlg dlg;
	dlg.SetDialogInfo( IDS_MSG_ADD_FIREWALL_PORT, IDS_MSG_FIREWALL_PORT );
	dlg.SetNumberValue();
	if( dlg.DoModal() == IDOK )
	{
		int value;
		dlg.GetValue(value);
		AddPort(value);

		m_bValueChange = true;

		CheckInstallComplete();
		UpdateControls();
	}
}

void CFirewallSettingsDlg::OnBnClickedButtonDeleteSelectedItems()
{
	int count = m_lcProgramsPorts.GetItemCount();
	for( int i=count-1; i>=0; i-- )
	{
		UINT state = m_lcProgramsPorts.GetItemState(i, LVIS_SELECTED);
		if(state == LVIS_SELECTED)
		{
			m_lcProgramsPorts.DeleteItem(i);

			m_bValueChange = true;
		}
	}

	CheckInstallComplete();
	UpdateControls();
}

void CFirewallSettingsDlg::OnBnClickedButtonInstallFirewall()
{
	BeginWaitCursor();

	//
	CStringArray list_uninstall_programs, list_uninstall_ports;

	list_uninstall_programs.Copy(m_listSavedPrograms);
	list_uninstall_ports.Copy(m_listSavedPorts);

	//
	CStringArray list_install_programs, list_install_ports;

	int count = m_lcProgramsPorts.GetItemCount();
	for(int i=0; i<count; i++)
	{
		CString str_type = m_lcProgramsPorts.GetItemText(i, 0);
		if(str_type == m_strProgramType)
		{
			CString str_program = m_lcProgramsPorts.GetItemText(i, 1);
			list_install_programs.Add(str_program);
			DeleteStringInList(str_program, list_uninstall_programs);
		}
		else if(str_type == m_strPortType)
		{
			CString str_port = m_lcProgramsPorts.GetItemText(i, 1);
			list_install_ports.Add(str_port);
			DeleteStringInList(str_port, list_uninstall_ports);
		}
	}

	//
	m_FireWall.set(list_install_programs, list_install_ports);

	//
	if( !m_FireWall.install() )
	{
		EndWaitCursor();

		CErrorMessageDlg dlg("Install(Register) Error !!!", m_FireWall.getErrorCode(), m_FireWall.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}

	m_listSavedPrograms.Copy(list_install_programs);
	m_listSavedPorts.Copy(list_install_ports);

	if( !m_FireWall.uninstall(list_uninstall_programs, list_uninstall_ports) )
	{
		EndWaitCursor();

		CErrorMessageDlg dlg("Install(Unregister) Error !!!", m_FireWall.getErrorCode(), m_FireWall.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}

	m_bInstallComplete = true;
	m_bValueChange = false;

	CheckInstallComplete();
	UpdateControls();

	EndWaitCursor();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

bool CFirewallSettingsDlg::AddProgram(LPCSTR lpszProgram)
{
	int count = m_lcProgramsPorts.GetItemCount();
	for(int i=0; i<count; i++)
	{
		CString str_type = m_lcProgramsPorts.GetItemText(i, 0);
		if(str_type != m_strProgramType)
			continue;

		CString str_program = m_lcProgramsPorts.GetItemText(i, 1);
		if(stricmp(str_program, lpszProgram) == 0)
			return false;
	}

	m_lcProgramsPorts.InsertItem(count, m_strProgramType);
	m_lcProgramsPorts.SetItemText(count, 1, lpszProgram);
	return true;
}

bool CFirewallSettingsDlg::AddPort(int nPort)
{
	char sz_buf[32];
	sprintf(sz_buf, "%d", nPort);

	int count = m_lcProgramsPorts.GetItemCount();
	for(int i=0; i<count; i++)
	{
		CString str_type = m_lcProgramsPorts.GetItemText(i, 0);
		if(str_type != m_strPortType)
			continue;

		CString str_port = m_lcProgramsPorts.GetItemText(i, 1);
		if(str_port == sz_buf)
			return false;
	}

	m_lcProgramsPorts.InsertItem(count, m_strPortType);
	m_lcProgramsPorts.SetItemText(count, 1, sz_buf);
	return true;
}

// ServerInstall.ini
//
// [FirewallProgramList]
// List=D:\Project\ubc\bin8\odServer.exe|D:\Project\ubc\bin8\pmServer.exe|...
// [FirewallPortList]
// List=100|200-300|...

const char* szDefaultProgramValues[] = {
	"C:\\Project\\3rdparty\\win32\\ace_wrappers\\bin\\ifr_service.exe",
	"C:\\Project\\3rdparty\\win32\\ace_wrappers\\bin\\naming_service.exe",
	"C:\\Project\\ubc\\cop\\bin8\\clinvoker.exe",
	"C:\\Project\\ubc\\cop\\bin8\\cop_spManager.exe",
	"C:\\Project\\ubc\\cop\\bin8\\cop_spServer.exe",
	"C:\\Project\\ubc\\cop\\bin8\\cop_timeServer.exe",
	"C:\\Project\\ubc\\cop\\bin8\\dummysm.exe",
	"C:\\Project\\ubc\\cop\\bin8\\notifyManager.exe",
	"C:\\Project\\ubc\\cop\\bin8\\notifyServer.exe",
	"C:\\Project\\ubc\\cop\\bin8\\pfServer.exe",
	"C:\\Project\\ubc\\cop\\bin8\\xmirServer.exe",
	"C:\\Project\\ubc\\bin8\\ContentsDistributor.exe",
	"C:\\Project\\ubc\\bin8\\EventGateway.exe",
	"C:\\Project\\ubc\\bin8\\repeater.exe",
	"C:\\Project\\ubc\\bin8\\SelfAutoUpdate.exe",
	"C:\\Project\\ubc\\bin8\\SocketCallGateway.exe",
	"C:\\Project\\ubc\\bin8\\ubc_dmServer.exe",
	"C:\\Project\\ubc\\bin8\\ubc_fmServer.exe",
	"C:\\Project\\ubc\\bin8\\ubc_lmServer.exe",
	"C:\\Project\\ubc\\bin8\\ubc_mmServer.exe",
	"C:\\Project\\ubc\\bin8\\ubc_odServer.exe",
	"C:\\Project\\ubc\\bin8\\ubc_pmServer.exe",
	"C:\\Project\\ubc\\bin8\\ubcagtServer.exe",
	"C:\\Project\\ubc\\bin8\\UBCServerAgent.exe",
	"C:\\Project\\ubc\\bin8\\UBCServerAgentSvc.exe",
	"C:\\Project\\ubc\\bin8\\UTV_starter.exe",
	"C:\\Project\\ubc\\bin8\\utv_wol.exe",
	"",
};

int szDefaultPortValues[] = {
	80, 
	8080, 
	5500, 
	5901,
	-1,
};

void CFirewallSettingsDlg::ImportDefaultValues()
{
	char str_drive[MAX_PATH+1] = {0};
	::GetModuleFileName(NULL, str_drive, MAX_PATH);

	for(int i=0; strlen(szDefaultProgramValues[i]) > 0; i++)
	{
		CString path = szDefaultProgramValues[i];
		path.SetAt(0, str_drive[0]);
		AddProgram(path);
	}

	for(int i=0; szDefaultPortValues[i] > 0; i++)
	{
		AddPort(szDefaultPortValues[i]);
	}
}

void CFirewallSettingsDlg::OnBnClickedButtonImportDefaultValues()
{
	ImportDefaultValues();

	m_bValueChange = true;

	CheckInstallComplete();
	UpdateControls();
}

bool CFirewallSettingsDlg::DeleteStringInList(LPCSTR lpszStr, CStringArray& listStr)
{
	int count = listStr.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str = listStr.GetAt(i);
		if(stricmp(str, lpszStr) == 0)
		{
			listStr.RemoveAt(i);
			return true;
		}
	}
	return false;
}

bool CFirewallSettingsDlg::RunInit()
{
	bool ret_value = true;

	//
	m_FireWall.get(&m_listSavedPrograms, &m_listSavedPorts);

	for(int i=0; i<m_listSavedPrograms.GetCount(); i++)
	{
		const CString& str_program = m_listSavedPrograms.GetAt(i);
		AddProgram(str_program);
	}
	for(int i=0; i<m_listSavedPorts.GetCount(); i++)
	{
		const CString& str_port = m_listSavedPorts.GetAt(i);
		AddPort(_ttoi(str_port));
	}

	//
	if( m_FireWall.test() )
	{
		ret_value = true;
		m_bInstallComplete = true;
	}
	else
	{
		ret_value = false;
		m_bInstallComplete = false;
	}

	m_bValueChange = false;

	return ret_value;
}

void CFirewallSettingsDlg::UpdateControls()
{
	m_btnInstallFirewall.UpdateControl();
}

int CFirewallSettingsDlg::CheckInstallComplete()
{
	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}

void CFirewallSettingsDlg::OnSize(UINT nType, int cx, int cy)
{
	CSubDlg::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )
	{
		m_reposControl.MoveControl(TRUE);
	}
}
