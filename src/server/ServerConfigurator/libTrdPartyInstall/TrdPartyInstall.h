#pragma once

using namespace std;

#include "../Installable/installable.h"

#define		KEY_TRDPARTY_NET_FRAMEWORK_4		("NETFRAMEWORK4")
#define		KEY_TRDPARTY_VC2005_EXPRESS			("VC2005Express")
#define		KEY_TRDPARTY_VC2010_REDIST_X86		("VC2010REDIST_X86")
#define		KEY_TRDPARTY_VC2010_REDIST_X64		("VC2010REDIST_X64")
#define		KEY_TRDPARTY_MYSQL_SERVER			("MySQLServer")
#define		KEY_TRDPARTY_MYSQL_ODBC_X86			("MYSQLODBC_X86")
#define		KEY_TRDPARTY_MYSQL_ODBC_X64			("MYSQLODBC_X64")


class CTrdPartyInstall : public installable
{
public:
	CTrdPartyInstall(void);
	virtual ~CTrdPartyInstall(void);

	virtual int prerequisite() { return 0; };
	virtual int test();
	int test(CString strProgramName);
	virtual int	install() { return 0; };
	int	install(CString strProgramName);

	virtual int get(LPCSTR key, CString& value);
	virtual int set(LPCSTR key, LPCSTR value) { return 0; };
	virtual CString get(LPCSTR key);

protected:
	CMapStringToString		m_mapKeyValues;

	int		test_NetFramework4();
	int		test_VC2005Express();
	int		test_VC2010Redist_x86();
	int		test_VC2010Redist_x64();
	int		test_MySQLServer();
	int		test_MySQLODBC_x86();
	int		test_MySQLODBC_x64();

	int		install_NetFramework4();
	int		install_VC2005Express();
	int		install_VC2010Redist_x86();
	int		install_VC2010Redist_x64();
	int		install_MySQLServer();
	int		install_MySQLODBC_x86();
	int		install_MySQLODBC_x64();

	BOOL	IsExistFileFolder(const char* path);
};
