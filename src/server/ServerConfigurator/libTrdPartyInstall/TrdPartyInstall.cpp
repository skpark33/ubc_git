#include "StdAfx.h"
#include "TrdPartyInstall.h"
//#include "resource.h"

#include "ci/libDebug/ciDebug.h"

ciSET_DEBUG(10, "CTrdPartyInstall");

CTrdPartyInstall::CTrdPartyInstall(void)
{
	m_mapKeyValues.SetAt(KEY_TRDPARTY_VC2005_EXPRESS, "0");
	m_mapKeyValues.SetAt(KEY_TRDPARTY_MYSQL_SERVER, "0");
}

CTrdPartyInstall::~CTrdPartyInstall(void)
{
}

int CTrdPartyInstall::get(LPCSTR key, CString& value)
{
	value = "";

	if( m_mapKeyValues.Lookup(key, value) ) return TRUE;

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);

	return FALSE;
}

CString CTrdPartyInstall::get(LPCSTR key)
{
	CString value="";
	m_mapKeyValues.Lookup(key, value);
	return value;
}

int CTrdPartyInstall::test()
{
	ciDEBUG(1, ("test()"));

	POSITION pos = m_mapKeyValues.GetStartPosition();
	CString key, value;

	BOOL ret_val = TRUE;

	while( pos != NULL )
	{
		m_mapKeyValues.GetNextAssoc( pos, key, value );

		if( !test(key) ) ret_val = FALSE;
	}

	ciDEBUG(1, ("test() end"));
	return ret_val;
}

int CTrdPartyInstall::test(CString strProgramName)
{
	ciDEBUG(1, ("test()"));

	_errString = "";

	if		( strProgramName == KEY_TRDPARTY_NET_FRAMEWORK_4 )		return test_NetFramework4();
	else if ( strProgramName == KEY_TRDPARTY_VC2005_EXPRESS )		return test_VC2005Express();
	else if ( strProgramName == KEY_TRDPARTY_VC2010_REDIST_X86 )	return test_VC2010Redist_x86();
	else if ( strProgramName == KEY_TRDPARTY_VC2010_REDIST_X64 )	return test_VC2010Redist_x64();
	else if ( strProgramName == KEY_TRDPARTY_MYSQL_SERVER )			return test_MySQLServer();
	else if ( strProgramName == KEY_TRDPARTY_MYSQL_ODBC_X86 )		return test_MySQLODBC_x86();
	else if ( strProgramName == KEY_TRDPARTY_MYSQL_ODBC_X64 )		return test_MySQLODBC_x64();

	ciDEBUG(1, ("test() end"));
	return FALSE;
}

int	CTrdPartyInstall::install(CString strProgramName)
{
	ciDEBUG(1, ("install()"));

	_errString = "";

	if		( strProgramName == KEY_TRDPARTY_NET_FRAMEWORK_4 )		return install_NetFramework4();
	else if ( strProgramName == KEY_TRDPARTY_VC2005_EXPRESS )		return install_VC2005Express();
	else if ( strProgramName == KEY_TRDPARTY_VC2010_REDIST_X86 )	return install_VC2010Redist_x86();
	else if ( strProgramName == KEY_TRDPARTY_VC2010_REDIST_X64 )	return install_VC2010Redist_x64();
	else if ( strProgramName == KEY_TRDPARTY_MYSQL_SERVER )			return install_MySQLServer();
	else if ( strProgramName == KEY_TRDPARTY_MYSQL_ODBC_X86 )		return install_MySQLODBC_x86();
	else if ( strProgramName == KEY_TRDPARTY_MYSQL_ODBC_X64 )		return install_MySQLODBC_x64();

	ciDEBUG(1, ("install() end"));
	return FALSE;
}

int CTrdPartyInstall::test_NetFramework4()
{
	char program_path[255] = {0};
	char* system_env_path = getenv("SystemRoot");
	::strcpy(program_path, (system_env_path==NULL) ? "C:\\Windows" : system_env_path );
	::strcat(program_path, "\\Microsoft.Net\\Framework\\v4*");

	m_mapKeyValues.SetAt(KEY_TRDPARTY_NET_FRAMEWORK_4, "0");
	if( IsExistFileFolder(program_path) )
	{
		m_mapKeyValues.SetAt(KEY_TRDPARTY_NET_FRAMEWORK_4, "1");
		return TRUE;
	}

	char errBuf[1024]={0};
	sprintf(errBuf, "Not installed Net Framework 4 (%s)", program_path);
	setError(-1, errBuf);
	return FALSE;
}

int CTrdPartyInstall::test_VC2005Express()
{
	char* program_path[] = {
		"C:\\Program Files (x86)\\Microsoft Visual Studio 8\\VC\\bin\\cl.exe",
			  "C:\\Program Files\\Microsoft Visual Studio 8\\VC\\bin\\cl.exe",
	};

	char* program_env_path = getenv("ProgramFiles");
	if( program_env_path )
	{
		program_path[0][0] = program_env_path[0];
		program_path[1][0] = program_env_path[0];
	}

	m_mapKeyValues.SetAt(KEY_TRDPARTY_VC2005_EXPRESS, "0");
	for( int i=0; i<2; i++ )
	{
		if( IsExistFileFolder(program_path[i]) )
		{
			m_mapKeyValues.SetAt(KEY_TRDPARTY_VC2005_EXPRESS, "1");
			return TRUE;
		}
	}

	char errBuf[1024]={0};
	sprintf(errBuf, "Not installed VC2005 (%s)", program_path[0]);
	setError(-1, errBuf);

	return FALSE;
}

int CTrdPartyInstall::test_VC2010Redist_x86()
{
	m_mapKeyValues.SetAt(KEY_TRDPARTY_VC2010_REDIST_X86, "0");

	// check C:\ Windows \ SysWOW64 \ msvcr100.dll (must larger than 100KB.. over 700KB)
	// not C:\ Windows \ System32 \ msvcr100.dll

	PVOID OldValue;
    BOOL ret_val = Wow64DisableWow64FsRedirection (&OldValue);

	CString str_path = getenv("SystemRoot");
	str_path += "\\SysWOW64\\msvcr100.dll";

	CFileStatus fs;
	if( CFile::GetStatus(str_path, fs) )
	{
		if( fs.m_size > 700*1024 )
		{
			if( ret_val==TRUE ) Wow64RevertWow64FsRedirection (OldValue);

			m_mapKeyValues.SetAt(KEY_TRDPARTY_VC2010_REDIST_X86, "1");
			return TRUE;
		}
	}

	char errBuf[1024]={0};
	sprintf(errBuf, "Not installed VC2010 Redistributable x86 (%s)", str_path);
	setError(-1, errBuf);

	if( ret_val==TRUE ) Wow64RevertWow64FsRedirection (OldValue);

	return FALSE;
}

int CTrdPartyInstall::test_VC2010Redist_x64()
{
	m_mapKeyValues.SetAt(KEY_TRDPARTY_VC2010_REDIST_X64, "0");

	// check C:\ Windows \ System32 \ msvcr100.dll (must larger than 100KB.. over 700KB)
	// not C:\ Windows \ SysWOW64 \ msvcr100.dll

	PVOID OldValue;
    BOOL ret_val = Wow64DisableWow64FsRedirection (&OldValue);

	CString str_path = getenv("SystemRoot");
	str_path += "\\System32\\msvcr100.dll";

	CFileStatus fs;
	if( CFile::GetStatus(str_path, fs) )
	{
		if( fs.m_size > 700*1024 )
		{
			if( ret_val==TRUE ) Wow64RevertWow64FsRedirection (OldValue);

			m_mapKeyValues.SetAt(KEY_TRDPARTY_VC2010_REDIST_X64, "1");
			return TRUE;
		}
	}

	char errBuf[1024]={0};
	sprintf(errBuf, "Not installed VC2010 Redistributable x64 (%s)", str_path);
	setError(-1, errBuf);

	if( ret_val==TRUE ) Wow64RevertWow64FsRedirection (OldValue);

	return FALSE;
}

int CTrdPartyInstall::test_MySQLServer()
{
	char* program_path[] = {
		"C:\\Program Files (x86)\\MySQL\\MySQL Server*",
			  "C:\\Program Files\\MySQL\\MySQL Server*",
	};

	char* program_env_path = getenv("ProgramFiles");
	if( program_env_path )
	{
		program_path[0][0] = program_env_path[0];
		program_path[1][0] = program_env_path[0];
	}

	m_mapKeyValues.SetAt(KEY_TRDPARTY_MYSQL_SERVER, "0");
	for( int i=0; i<2; i++ )
	{
		if( IsExistFileFolder(program_path[i]) )
		{
			m_mapKeyValues.SetAt(KEY_TRDPARTY_MYSQL_SERVER, "1");
			return TRUE;
		}
	}

	char errBuf[1024]={0};
	sprintf(errBuf, "Not installed MySQL Server (%s)", program_path[1]);
	setError(-1, errBuf);

	return FALSE;
}

int CTrdPartyInstall::test_MySQLODBC_x86()
{
// 32bit
//[HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\ODBC\ODBCINST.INI\ODBC Drivers]
//"MySQL ODBC 5.1 Driver"="Installed"

	m_mapKeyValues.SetAt(KEY_TRDPARTY_MYSQL_ODBC_X86, "0");

	const char* reg_key = "SOFTWARE\\Wow6432Node\\ODBC\\ODBCINST.INI\\ODBC Drivers";

	HKEY hKey;
	LONG lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, reg_key, 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey);
	if( lRet == ERROR_SUCCESS )
	{
		char szVal[1024] = {0};
		DWORD dwType = REG_SZ;
		DWORD dwSize = sizeof(szVal);
		lRet = RegQueryValueEx(hKey, "MySQL ODBC 5.1 Driver", NULL, &dwType, (BYTE*)szVal, &dwSize);  
		RegCloseKey(hKey);

		if( lRet == ERROR_SUCCESS )
		{
			if( stricmp(szVal, "Installed") == NULL )
			{
				m_mapKeyValues.SetAt(KEY_TRDPARTY_MYSQL_ODBC_X86, "1");
				return TRUE;
			}
		}
	}

	char errBuf[1024]={0};
	sprintf(errBuf, "Not installed MySQL ODBC Connector (%s)", reg_key);
	setError(-1, errBuf);

	return FALSE;
}


int CTrdPartyInstall::test_MySQLODBC_x64()
{
// 64bit
//[HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers]
//"MySQL ODBC 5.1 Driver"="Installed"

	m_mapKeyValues.SetAt(KEY_TRDPARTY_MYSQL_ODBC_X64, "0");

	const char* reg_key = "SOFTWARE\\ODBC\\ODBCINST.INI\\ODBC Drivers";

	HKEY hKey;
	LONG lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, reg_key, 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey);
	if( lRet == ERROR_SUCCESS )
	{
		char szVal[1024] = {0};
		DWORD dwType = REG_SZ;
		DWORD dwSize = sizeof(szVal);
		lRet = RegQueryValueEx(hKey, "MySQL ODBC 5.1 Driver", NULL, &dwType, (BYTE*)szVal, &dwSize);  
		RegCloseKey(hKey);

		if( lRet == ERROR_SUCCESS )
		{
			if( stricmp(szVal, "Installed") == NULL )
			{
				m_mapKeyValues.SetAt(KEY_TRDPARTY_MYSQL_ODBC_X64, "1");
				return TRUE;
			}
		}
	}

	char errBuf[1024]={0};
	sprintf(errBuf, "Not installed MySQL ODBC Connector (%s)", reg_key);
	setError(-1, errBuf);

	return FALSE;
}

int CTrdPartyInstall::install_NetFramework4()
{
// Microsoft .NET Framework 4
// http://www.microsoft.com/en-us/download/details.aspx?id=17851
// http://download.microsoft.com/download/1/B/E/1BE39E79-7E39-46A3-96FF-047F95396215/dotNetFx40_Full_setup.exe

	ciDEBUG(1, ("install()"));

	ShellExecute(NULL, "open", "iexplore.exe", "http://www.microsoft.com/en-us/download/details.aspx?id=17851", NULL, SW_SHOWNORMAL);
	ShellExecute(NULL, "open", "iexplore.exe", "http://download.microsoft.com/download/1/B/E/1BE39E79-7E39-46A3-96FF-047F95396215/dotNetFx40_Full_setup.exe", NULL, SW_SHOWNORMAL);

	ciDEBUG(1, ("install() end"));
	return TRUE;
}

int CTrdPartyInstall::install_VC2005Express()
{
/*
http://www.microsoft.com/korea/msdn/vstudio/express/visualc/download/
http://msdn.microsoft.com/en-in/express/aa975050

http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0x409	English
http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0x404	Chinese Traditional
http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0x407	German
http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0x40C	French
http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0x410	Italian
http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0x411	Japanese
http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0x412	Korean
http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0x804	Chinese Simplified
http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0xC0A	Spanish
*/
	ciDEBUG(1, ("install()"));

	ShellExecute(NULL, "open", "iexplore.exe", "http://msdn.microsoft.com/en-in/express/aa975050", NULL, SW_SHOWNORMAL);
	ShellExecute(NULL, "open", "iexplore.exe", "http://go.microsoft.com/fwlink/?LinkId=51410&clcid=0x412", NULL, SW_SHOWNORMAL);

	ciDEBUG(1, ("install() end"));
	return TRUE;
}

int CTrdPartyInstall::install_VC2010Redist_x86()
{
// Microsoft Visual C++ 2010 Redistributable Package (x86)
// http://www.microsoft.com/en-us/download/details.aspx?id=5555
// http://download.microsoft.com/download/5/B/C/5BC5DBB3-652D-4DCE-B14A-475AB85EEF6E/vcredist_x86.exe


// Microsoft Visual C++ 2010 SP1 Redistributable Package (x86)
// http://www.microsoft.com/en-US/download/details.aspx?id=8328
// http://download.microsoft.com/download/C/6/D/C6D0FD4E-9E53-4897-9B91-836EBA2AACD3/vcredist_x86.exe

	ciDEBUG(1, ("install()"));

	ShellExecute(NULL, "open", "iexplore.exe", "http://www.microsoft.com/en-US/download/details.aspx?id=8328", NULL, SW_SHOWNORMAL);
	ShellExecute(NULL, "open", "iexplore.exe", "http://download.microsoft.com/download/C/6/D/C6D0FD4E-9E53-4897-9B91-836EBA2AACD3/vcredist_x86.exe", NULL, SW_SHOWNORMAL);

	ciDEBUG(1, ("install() end"));
	return TRUE;
}

int CTrdPartyInstall::install_VC2010Redist_x64()
{
// Microsoft Visual C++ 2010 SP1 Redistributable Package (x64)
// http://www.microsoft.com/en-US/download/details.aspx?id=13523
// http://download.microsoft.com/download/A/8/0/A80747C3-41BD-45DF-B505-E9710D2744E0/vcredist_x64.exe

	ciDEBUG(1, ("install()"));

	ShellExecute(NULL, "open", "iexplore.exe", "http://www.microsoft.com/en-US/download/details.aspx?id=13523", NULL, SW_SHOWNORMAL);
	ShellExecute(NULL, "open", "iexplore.exe", "http://download.microsoft.com/download/A/8/0/A80747C3-41BD-45DF-B505-E9710D2744E0/vcredist_x64.exe", NULL, SW_SHOWNORMAL);

	ciDEBUG(1, ("install() end"));
	return TRUE;
}

int CTrdPartyInstall::install_MySQLServer()
{
	ciDEBUG(1, ("install()"));

	ShellExecute(NULL, "open", "iexplore.exe", "http://dev.mysql.com/downloads/windows/installer/5.5.html", NULL, SW_SHOWNORMAL);
	ShellExecute(NULL, "open", "iexplore.exe", "http://dev.mysql.com/get/Downloads/MySQLInstaller/mysql-installer-community-5.5.45.0.msi", NULL, SW_SHOWNORMAL);

	ciDEBUG(1, ("install() end"));
	return TRUE;
}

int CTrdPartyInstall::install_MySQLODBC_x86()
{
// MySQL Connector/ODBC x86
// http://dev.mysql.com/downloads/connector/odbc/5.1.html
// http://dev.mysql.com/get/Downloads/Connector-ODBC/5.1/mysql-connector-odbc-5.1.13-win32.msi

	ciDEBUG(1, ("install()"));

	ShellExecute(NULL, "open", "iexplore.exe", "http://dev.mysql.com/downloads/connector/odbc/5.1.html", NULL, SW_SHOWNORMAL);
	ShellExecute(NULL, "open", "iexplore.exe", "http://dev.mysql.com/get/Downloads/Connector-ODBC/5.1/mysql-connector-odbc-5.1.13-win32.msi", NULL, SW_SHOWNORMAL);

	ciDEBUG(1, ("install() end"));
	return TRUE;
}

int CTrdPartyInstall::install_MySQLODBC_x64()
{
// MySQL Connector/ODBC x64
// http://dev.mysql.com/downloads/connector/odbc/5.1.html
// http://dev.mysql.com/get/Downloads/Connector-ODBC/5.1/mysql-connector-odbc-5.1.13-winx64.msi

	ciDEBUG(1, ("install()"));

	ShellExecute(NULL, "open", "iexplore.exe", "http://dev.mysql.com/downloads/connector/odbc/5.1.html", NULL, SW_SHOWNORMAL);
	ShellExecute(NULL, "open", "iexplore.exe", "http://dev.mysql.com/get/Downloads/Connector-ODBC/5.1/mysql-connector-odbc-5.1.13-winx64.msi", NULL, SW_SHOWNORMAL);

	ciDEBUG(1, ("install() end"));
	return TRUE;
}

BOOL CTrdPartyInstall::IsExistFileFolder(const char* path)
{
	WIN32_FIND_DATA ffd = {0};
	HANDLE hFind = FindFirstFile(path, &ffd);

	if( hFind == INVALID_HANDLE_VALUE ) return false;

	FindClose(hFind);
	return true;
}
