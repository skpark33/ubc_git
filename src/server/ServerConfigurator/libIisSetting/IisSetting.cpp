#include "StdAfx.h"
#include "IisSetting.h"
#include "Redir/CommandRedir.h"
#include "shlwapi.h"
#include <IMAGEHLP.H>

#define IIS_DEFAULT_PATH	_T("UBC_DEFAULT")
#define IIS_UBC_HTTP_PATH	_T("UBC_HTTP")
#define IIS_UBC_WEB_PATH	_T("UBC_WEB")

//#define IIS_APPCMD			_T("%systemroot%\\system32\\inetsrv\\appcmd.exe")
#define IIS_APPCMD			_T("%s\\system32\\inetsrv\\appcmd.exe")

//SITE "UBC_DEFAULT" (id:3,bindings:http/*:8080:,https/*:8888:,state:Started)
//SITE "UBC_WEB" (id:4,bindings:http/*:80:,https/*:443:,state:Started)
#define IIS_LIST_SITE		_T("list site")
#define IIS_ADD_SITE		_T("add site /name:\"%s\" /bindings:http://*:%d /physicalPath:\"%s\"")
#define IIS_DEL_SITE		_T("delete site /site.name:\"%s\"")
#define IIS_ADD_VDIR		_T("add vdir /app.name:\"%s\"/ /path:\"/%s\" /physicalPath:\"%s\"")
#define IIS_SET_DIRECTORYBROWSE		_T("set config \"%s\" /section:directoryBrowse /enabled:true")

//APPPOOL "UBC_DEFAULT" (MgdVersion:v2.0,MgdMode:Integrated,state:Started)
//APPPOOL "UBC_HTTP" (MgdVersion:v2.0,MgdMode:Integrated,state:Started)
//APPPOOL "UBC_WEB" (MgdVersion:v2.0,MgdMode:Integrated,state:Started)
#define IIS_LIST_POOL		_T("list apppool")
#define IIS_ADD_POOL		_T("add apppool /name:\"%s\" /processModel.identityType:LocalSystem /enable32BitAppOnWin64:true")
#define IIS_DEL_POOL		_T("delete apppool /apppool.name:\"%s\"")

//APP "UBC_DEFAULT/" (applicationPool:DefaultAppPool)
//APP "UBC_DEFAULT/UBC_HTTP" (applicationPool:UBC_HTTP)
//APP "UBC_WEB/" (applicationPool:UBC_WEB)
#define IIS_LIST_APP		_T("list app")
#define IIS_ADD_APP			_T("add app /site.name:\"%s\" /path:\"/%s\" /physicalPath:\"%s\" /applicationPool:\"%s\"")
#define IIS_DEL_APP			_T("delete app /app.name:\"%s\"")


#define IIS_ADD_FTP_SITE		_T("add site /name:\"%s\" /bindings:ftp://*:%d /physicalPath:\"%s\"")	// name, port, path
#define IIS_SET_FTP_SITE_1		_T("set config \"%s\" /section:system.ftpserver/security/authorization /+[accessType='Allow',permissions='Read,Write',roles='',users='%s'] /commit:apphost") // name, account
#define IIS_SET_FTP_SITE_2		_T("set config /section:system.applicationHost/sites /[name='%s'].ftpServer.serverAutoStart:\"true\" /[name='%s'].ftpServer.security.ssl.controlChannelPolicy:\"SslAllow\" /[name='%s'].ftpServer.security.ssl.dataChannelPolicy:\"SslAllow\" /[name='%s'].ftpServer.security.authentication.basicAuthentication.enabled:\"True\" /[name='%s'].ftpServer.directoryBrowse.showFlags:DisplayVirtualDirectories /commit:apphost") // name, name, name, name, name
#define IIS_SET_FTP_PASSIVE_PORT	_T("set config -section:system.ftpServer/firewallSupport /lowDataChannelPort:\"%d\" /highDataChannelPort:\"%d\" /commit:apphost")
#define IIS_GET_FTP_PASSIVE_PORT	_T("list config -section:system.ftpServer/firewallSupport")

#define IIS_GET_ACCOUNT			_T("list config \"%s\" /section:system.ftpserver/security/authorization")	// name
#define IIS_LIST_ACCOUNT		_T("net user")
#define IIS_ADD_ACCOUNT			_T("net user /add \"%s\" \"%s\"")	// account, password
#define IIS_CHANGE_PWD_ACCOUNT	_T("net user \"%s\" \"%s\"")	// account, password
#define IIS_SET_ACCOUNT_1		_T("net user \"%s\" /active:yes /expires:never /passwordchg:yes /passwordreq:no /comment:\"ftp account\"")	// account
#define IIS_SET_ACCOUNT_2		_T("wmic path Win32_userAccount where Name='%s' set PasswordExpires=false")	// account
#define IIS_SET_ACCOUNT_3		_T("net localgroup \"administrators\" \"%s\" /delete")	// account
#define IIS_SET_ACCOUNT_4		_T("net localgroup \"administrators\" \"%s\" /add")	// account



CIisSetting::CIisSetting(CString strWebSiteName, int nPort)
: installable()
, m_strWebSiteName (strWebSiteName)
, m_nPort (nPort)
, m_strAccount (_T(""))
, m_strPassword (_T(""))
, m_nPassiveLowPort (0)
, m_nPassiveHighPort (0)
{
	m_strAppCmd.Format(IIS_APPCMD, getenv("SystemRoot"));
}

CIisSetting::~CIisSetting(void)
{
}

// 선행작업 여부 체크
int CIisSetting::prerequisite()
{
	// IIS 설치 여부 확인
	if( !::PathFileExists(m_strAppCmd) )
	{
		setError(-1, _T("IIS is not installed"));
		return FALSE;
	}

	CString strHomePath(getenv("PROJECT_HOME"));
	if( strHomePath.IsEmpty() )
	{
		setError(-1, _T("PROJECT_HOME environment variable is not set."));
		return FALSE;
	}

	// UBC_DEFAULT
	if( m_strWebSiteName == IIS_UBC_DEFAULT_NAME )
	{
	}
	// UBC_HTTP
	else if( m_strWebSiteName == IIS_UBC_HTTP_NAME )
	{
		if( GetSiteInfo(IIS_UBC_DEFAULT_NAME).IsEmpty() )
		{
			setError(-1, _T("[UBC_DEFAULT] is not intalled"));
			return FALSE;
		}
	}
	// UBC_WEB
	else if( m_strWebSiteName == IIS_UBC_WEB_NAME )
	{
	}
	// UBC_FTP
	else if( m_strWebSiteName == IIS_UBC_FTP_NAME )
	{
	}

	return TRUE;
}

// 이미 되어있는지 체크
int CIisSetting::test()
{
	clearError();

	// UBC_DEFAULT
	if( m_strWebSiteName == IIS_UBC_DEFAULT_NAME )
	{
		CString strInfo = GetSiteInfo(m_strWebSiteName);
		if( strInfo.IsEmpty() )
		{
			setError(-1, m_strWebSiteName + _T(" is not installed"));
			return FALSE;
		}

		int nPos1 = strInfo.Find(_T("bindings:http"));
		if( nPos1 > 0 )
		{
			nPos1 += 13;
			int nPos2 = strInfo.Find(_T(":"), nPos1);
			if( nPos2 > 0 )
			{
				nPos2 += 1;
				int nPos3 = strInfo.Find(_T(":"), nPos2);
				if( nPos3 > 0 )
				{
					CString strPort = strInfo.Mid(nPos2, nPos3-nPos2);
					m_nPort = _ttoi(strPort);
				}
			}
		}

		//CString strFind;
		//strFind.Format(_T("http/*:%d:"), m_nPort);
		//if(strInfo.Find(strFind) < 0)
		//{
		//	setError(-1, _T("Other ports on the website has been set."));
		//	return FALSE;
		//}
	}
	// UBC_HTTP
	else if( m_strWebSiteName == IIS_UBC_HTTP_NAME )
	{
		CString strFind;
		strFind.Format(_T("%s/%s"), IIS_UBC_DEFAULT_NAME, IIS_UBC_HTTP_NAME);

		CString strInfo = GetAppInfo(strFind);
		if( strInfo.IsEmpty() )
		{
			setError(-1, m_strWebSiteName + _T(" is not installed"));
			return FALSE;
		}
	}
	// UBC_WEB
	else if( m_strWebSiteName == IIS_UBC_WEB_NAME )
	{
		CString strInfo = GetSiteInfo(m_strWebSiteName);
		if( strInfo.IsEmpty() )
		{
			setError(-1, m_strWebSiteName + _T(" is not installed"));
			return FALSE;
		}

		int nPos1 = strInfo.Find(_T("bindings:http"));
		if( nPos1 > 0 )
		{
			nPos1 += 13;
			int nPos2 = strInfo.Find(_T(":"), nPos1);
			if( nPos2 > 0 )
			{
				nPos2 += 1;
				int nPos3 = strInfo.Find(_T(":"), nPos2);
				if( nPos3 > 0 )
				{
					CString strPort = strInfo.Mid(nPos2, nPos3-nPos2);
					m_nPort = _ttoi(strPort);
				}
			}
		}

		//CString strFind;
		//strFind.Format(_T("http/*:%d:"), m_nPort);
		//if(strInfo.Find(strFind) < 0)
		//{
		//	setError(-1, _T("Other ports on the website has been set."));
		//	return FALSE;
		//}
	}
	// UBC_FTP
	else if( m_strWebSiteName == IIS_UBC_FTP_NAME )
	{
		CString strInfo = GetSiteInfo(m_strWebSiteName);
		if( strInfo.IsEmpty() )
		{
			setError(-1, m_strWebSiteName + _T(" is not installed"));
			return FALSE;
		}

		m_strAccount = GetAccountInfo(m_strWebSiteName);

		//
		int nPos1 = strInfo.Find(_T("bindings:ftp"));
		if( nPos1 > 0 )
		{
			nPos1 += 12;
			int nPos2 = strInfo.Find(_T(":"), nPos1);
			if( nPos2 > 0 )
			{
				nPos2 += 1;
				int nPos3 = strInfo.Find(_T(":"), nPos2);
				if( nPos3 > 0 )
				{
					CString strPort = strInfo.Mid(nPos2, nPos3-nPos2);
					m_nPort = _ttoi(strPort);
				}
			}
		}

		//
		GetFTPRangePort(m_nPassiveLowPort, m_nPassiveHighPort);
	}
	else
	{
		setError(-1, m_strWebSiteName + _T(" is invalid service"));
		return FALSE;
	}

	return TRUE;
}

int	CIisSetting::install()
{
	clearError();

	if( prerequisite() < 0 ) return FALSE;

	// C:\\Project\ubc
	CString strHomePath(getenv("PROJECT_HOME"));

	// UBC_DEFAULT
	if( m_strWebSiteName == IIS_UBC_DEFAULT_NAME )
	{
		// Add Site
		if( !AddSite(m_strWebSiteName, m_nPort, strHomePath + _T("\\web\\") + IIS_DEFAULT_PATH) ) return FALSE;

		// Vdir (PublicContents)
		if( !AddVdir(m_strWebSiteName, _T("PublicContents"), strHomePath + _T("\\PublicContents")) ) return FALSE;

		// Vdir (ScreenShot)
		if( !AddVdir(m_strWebSiteName, _T("ScreenShot"), strHomePath + _T("\\ScreenShot")) ) return FALSE;
		if( !SetVdirDirectoryBrowse(m_strWebSiteName+"/ScreenShot") ) return FALSE;
	}
	// UBC_HTTP
	else if( m_strWebSiteName == IIS_UBC_HTTP_NAME )
	{
		// Add AppPool
		if( !AddAppPool(m_strWebSiteName) ) return FALSE;

		// Add App
		if( !AddApp(IIS_UBC_DEFAULT_NAME, m_strWebSiteName, strHomePath + _T("\\web\\") + IIS_UBC_HTTP_PATH) ) return FALSE;
	}
	// UBC_WEB
	else if( m_strWebSiteName == IIS_UBC_WEB_NAME )
	{
		// Add AppPool
		if( !AddAppPool(m_strWebSiteName) ) return FALSE;

		// Add Site
		if( !AddSite(m_strWebSiteName, m_nPort, strHomePath + _T("\\web\\") + IIS_UBC_WEB_PATH) ) return FALSE;
	}
	// UBC_FTP
	else if( m_strWebSiteName == IIS_UBC_FTP_NAME )
	{
		// create account
		if( !CreateAccount(m_strAccount, m_strPassword) ) return FALSE;

		// Add Site
		if( !AddFtpSite(m_strWebSiteName, m_nPort, strHomePath) ) return FALSE;

		// set site
		if( !SetFtpSite(m_strWebSiteName, m_strAccount, m_nPassiveLowPort, m_nPassiveHighPort) ) return FALSE;
	}
	else
		return FALSE;

	return TRUE;
}

int CIisSetting::get(const char* key, string& value)
{
	if( _tcscmp(key, IIS_NAME) == 0 )
	{
		value = m_strWebSiteName;
		return TRUE;
	}
	if( _tcscmp(key, IIS_ACCOUNT) == 0 )
	{
		value = m_strAccount;
		return TRUE;
	}
	if( _tcscmp(key, IIS_PASSWORD) == 0 )
	{
		value = m_strPassword;
		return TRUE;
	}

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);
	return FALSE;
}

int CIisSetting::get(const char* key, int& value)
{
	if( _tcscmp(key, IIS_PORT) == 0 )
	{
		value = m_nPort;
		return TRUE;
	}

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);
	return FALSE;
}

int CIisSetting::set(const char* key, const char* value)
{
	if( _tcscmp(key, IIS_NAME) == 0 )
	{
		m_strWebSiteName = value;
		return TRUE;
	}
	if( _tcscmp(key, IIS_ACCOUNT) == 0 )
	{
		m_strAccount = value;
		return TRUE;
	}
	if( _tcscmp(key, IIS_PASSWORD) == 0 )
	{
		m_strPassword = value;
		return TRUE;
	}

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);
	return FALSE;
}

int CIisSetting::set(const char* key, int value)
{
	if( _tcscmp(key, IIS_PORT) == 0 )
	{
		m_nPort = value;
		return TRUE;
	}

	CString msg;
	msg.Format("Invalid Key name (%s)", key);
	setError(-1, msg);
	return FALSE;
}

CString CIisSetting::GetSiteInfo(CString strName)
{
	CString strCmd;
	strCmd.Format(_T("%s %s"), m_strAppCmd, IIS_LIST_SITE);

	CCommandRedirector redir;
	if( !redir.Open(strCmd) )
	{
		redir.Close();
		CString msg;
		msg.Format("Fail to execute command(%s)", strCmd);
		setError(-1, msg);
		return _T("");
	}
	redir.Close();

	strName.MakeUpper();
	CString strFind;
	strFind.Format(_T("SITE \"%s\" "), strName);

	int nPos = 0;
	CString strRow;
	while( !(strRow = redir.m_strResult.Tokenize("\n", nPos)).IsEmpty() )
	{
		CString strResult = strRow;

		strRow.MakeUpper();
		if( strRow.Find(strFind) == 0 ) return strResult;
	}

	return _T("");
}

CString CIisSetting::GetPoolInfo(CString strName)
{
	CString strCmd;
	strCmd.Format(_T("%s %s"), m_strAppCmd, IIS_LIST_POOL);

	CCommandRedirector redir;
	if( !redir.Open(strCmd) )
	{
		redir.Close();
		CString msg;
		msg.Format("Fail to execute command(%s)", strCmd);
		setError(-1, msg);
		return _T("");
	}
	redir.Close();

	strName.MakeUpper();
	CString strFind;
	strFind.Format(_T("APPPOOL \"%s\" "), strName);

	int nPos = 0;
	CString strRow;
	while( !(strRow = redir.m_strResult.Tokenize("\n", nPos)).IsEmpty() )
	{
		CString strResult = strRow;

		strRow.MakeUpper();
		if( strRow.Find(strFind) == 0 ) return strResult;
	}

	return _T("");
}

CString CIisSetting::GetAppInfo(CString strName)
{
	CString strCmd;
	strCmd.Format(_T("%s %s"), m_strAppCmd, IIS_LIST_APP);

	CCommandRedirector redir;
	if( !redir.Open(strCmd) )
	{
		redir.Close();
		CString msg;
		msg.Format("Fail to execute command(%s)", strCmd);
		setError(-1, msg);
		return _T("");
	}
	redir.Close();

	strName.MakeUpper();
	CString strFind;
	strFind.Format(_T("APP \"%s\" "), strName);

	int nPos = 0;
	CString strRow;
	while( !(strRow = redir.m_strResult.Tokenize("\n", nPos)).IsEmpty() )
	{
		CString strResult = strRow;

		strRow.MakeUpper();
		if(strRow.Find(strFind) == 0) return strResult;
	}

	return _T("");
}

bool CIisSetting::ExecuteAppCmd(CString strCmd)
{
	CCommandRedirector redir;
	if( !redir.Open(m_strAppCmd + _T(" ") + strCmd) )
	{
		redir.Close();
		CString msg;
		msg.Format("Fail to execute command(%s)", strCmd);
		setError(-1, msg);
		return false;
	}
	redir.Close();

	if( redir.m_strResult.Find("ERROR") != 0 ) return true;

	CString msg;
	msg.Format("Fail to execute command(%s)\r\n\r\n%s", strCmd, redir.m_strResult);
	setError(-1, msg);
	return false;
}

bool CIisSetting::AddSite(CString strSiteName, int nPort, CString strPath, bool bRenew)
{
	CString strCmd;

	if( bRenew )
	{
		strCmd.Format(IIS_DEL_SITE, strSiteName);
		ExecuteAppCmd(strCmd);
	}

	strCmd.Format(IIS_ADD_SITE, strSiteName, nPort, strPath);

	return ExecuteAppCmd(strCmd);
}

bool CIisSetting::AddVdir(CString strSiteName, CString strVdirName, CString strPath)
{
	if( !MakeSureDirectoryPathExists(strPath+"\\") )
	{
		DWORD dwErrNo = GetLastError();

		LPVOID lpMsgBuf = NULL;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
					| FORMAT_MESSAGE_IGNORE_INSERTS 
					| FORMAT_MESSAGE_FROM_SYSTEM     
					,	NULL
					, dwErrNo
					, 0
					, (LPTSTR)&lpMsgBuf
					, 0
					, NULL);

		CString strErrMsg;
		if( !lpMsgBuf )
		{
			strErrMsg.Format(_T("Unknow Error - CreateProcess [%d]"), dwErrNo);
		}
		else
		{
			strErrMsg.Format(_T("%s [%d]"), (LPCTSTR)lpMsgBuf, dwErrNo);
			strErrMsg.Replace("\r", "");
			strErrMsg.Replace("\n", "");
		}//if

		LocalFree(lpMsgBuf);

		CString msg;
		msg.Format("Fail to MakeSureDirectoryPathExists(%s)", strErrMsg);
		setError(dwErrNo, msg);
		return false;
	}

	CString strCmd;
	strCmd.Format(IIS_ADD_VDIR, strSiteName, strVdirName, strPath);
	return ExecuteAppCmd(strCmd);
}

bool CIisSetting::AddAppPool(CString strAppPoolName)
{
	CString strCmd;

	strCmd.Format(IIS_DEL_POOL, strAppPoolName);
	ExecuteAppCmd(strCmd);

	strCmd.Format(IIS_ADD_POOL, strAppPoolName);

	return ExecuteAppCmd(strCmd);
}

bool CIisSetting::SetVdirDirectoryBrowse(CString strSiteName)
{
	CString strCmd;

	strCmd.Format(IIS_SET_DIRECTORYBROWSE, strSiteName);
	return ExecuteAppCmd(strCmd);
}

bool CIisSetting::AddApp(CString strSiteName, CString strAppName, CString strPath)
{
	CString strCmd;

	strCmd.Format(IIS_DEL_APP, strAppName);
	ExecuteAppCmd(strCmd);

	strCmd.Format(IIS_ADD_APP, strSiteName, strAppName, strPath, strAppName);

	return ExecuteAppCmd(strCmd);
}




/*
ftp add

appcmd.exe add site /name:"UBC_FTP" /bindings:ftp://*:21 /physicalPath:"C:\Project"
appcmd.exe set config "UBC_FTP" /section:system.ftpserver/security/authorization /+[accessType='Allow',permissions='Read,Write',roles='',users='server'] /commit:apphost
appcmd.exe set config /section:system.applicationHost/sites /[name='UBC_FTP'].ftpServer.serverAutoStart:"false" /[name='UBC_FTP'].ftpServer.security.ssl.controlChannelPolicy:"SslAllow" /[name='UBC_FTP'].ftpServer.security.ssl.dataChannelPolicy:"SslAllow" /[name='UBC_FTP'].ftpServer.security.authentication.basicAuthentication.enabled:"True" /[name='UBC_FTP'].ftpServer.directoryBrowse.showFlags:DisplayVirtualDirectories /commit:apphost

net user /add "server" "rjtlrl2009"
net user "server" /active:yes /expires:never /passwordchg:yes /passwordreq:no /comment:"ftp account"
wmic path Win32_userAccount where Name='server' set PasswordExpires=false
net localgroup "administrators" "server" /add
error -> return_val=2
can't run -> return_val=1
run -> return_val=0

*/

bool CIisSetting::AddFtpSite(CString strSiteName, int nPort, CString strPath, bool bRenew)
{
	CString strCmd;

	if( bRenew )
	{
		strCmd.Format(IIS_DEL_SITE, strSiteName);
		ExecuteAppCmd(strCmd);
	}

	strCmd.Format(IIS_ADD_FTP_SITE, strSiteName, nPort, strPath);

	return ExecuteAppCmd(strCmd);
}

bool CIisSetting::SetFtpSite(CString strSiteName, CString strAccount, int nLowPort, int nHighPort)
{
	CString strCmd1, strCmd2, strCmd3;

	strCmd1.Format(IIS_SET_FTP_SITE_1, strSiteName, strAccount);
	strCmd2.Format(IIS_SET_FTP_SITE_2, strSiteName, strSiteName, strSiteName, strSiteName, strSiteName);
	strCmd3.Format(IIS_SET_FTP_PASSIVE_PORT, nLowPort, nHighPort);

	if( !ExecuteAppCmd(strCmd1) ) return false;
	if( !ExecuteAppCmd(strCmd2) ) return false;
	if( !ExecuteAppCmd(strCmd3) ) return false;

	return true;
}

CString	CIisSetting::GetAccountInfo(CString strName)
{
	CString strParam;
	strParam.Format(IIS_GET_ACCOUNT, strName);

	CString strCmd;
	strCmd.Format(_T("%s %s"), m_strAppCmd, strParam);

	CCommandRedirector redir;
	if( !redir.Open(strCmd) )
	{
		redir.Close();
		CString msg;
		msg.Format("Fail to execute command(%s)", strCmd);
		setError(-1, msg);
		return _T("");
	}
	redir.Close();

	CString strAccount = _T("");

	int nPos = 0;
	while(1)
	{
		CString strRow = redir.m_strResult.Tokenize("\n", nPos);
		if( strRow.IsEmpty() ) break;

		int pos_users = strRow.Find(_T("users=\""));
		if( pos_users >= 0 )
		{
			int pos_end = strRow.Find("\"", pos_users+8);
			if( pos_end >= 0 )
			{
				strAccount = strRow.Mid(pos_users+8, pos_end-(pos_users+8));
				break;
			}
		}
	}

	return strAccount;
}

bool CIisSetting::GetFTPRangePort(int& nLowPort, int& nHighPort)
{
	CString strCmd;
	strCmd.Format(_T("%s %s"), m_strAppCmd, IIS_GET_FTP_PASSIVE_PORT);

	CCommandRedirector redir;
	if( !redir.Open(strCmd) )
	{
		redir.Close();
		CString msg;
		msg.Format("Fail to execute command(%s)", strCmd);
		setError(-1, msg);
		return false;
	}
	redir.Close();

	// <system.ftpServer>
	//		<firewallSupport lowDataChannelPort="14300" highDataChannelPort="14500" />
	// </system.ftpServer>

#define		LOW_DATA_CHANNEL_PORT		"lowDataChannelPort=\""
#define		HIGH_DATA_CHANNEL_PORT		"highDataChannelPort=\""

	LPCSTR str_result = (LPCSTR)redir.m_strResult;
	const char* sz_low_port  = strstr(str_result, LOW_DATA_CHANNEL_PORT);
	const char* sz_high_port = strstr(str_result, HIGH_DATA_CHANNEL_PORT);

	if( sz_low_port==NULL || sz_high_port==NULL ) return false;

	nLowPort  = atoi(sz_low_port);
	nHighPort = atoi(sz_high_port);

	return true;
}

bool CIisSetting::CreateAccount(CString strAccount, CString strPassword)
{
	// create disable.inf
	TRY
	{
		CFile file;
		CFileException fex;
		if( !file.Open("C:\\UBC_SrvConf_PwdComplex_Disable.inf", CFile::modeCreate | CFile::modeWrite | CFile::typeBinary, &fex) )
		{
			// create error
			setError(fex.m_cause, _T("Fail to create disable.inf"));
			return false;
		}

		const char buf_inf[] = "[System Access]\r\nPasswordComplexity=0\r\n[Version]\r\nsignature=\"$CHICAGO$\"\r\nRevision=1";
		file.Write(buf_inf, strlen(buf_inf));
		file.Close();
	}
	CATCH(CFileException, ex)
	{
		::DeleteFile("C:\\UBC_SrvConf_PwdComplex_Disable.inf");
		setError(ex->m_cause, _T("Fail to write disable.inf"));
		return false;
	}
	END_CATCH

	// run secedit (disable password complexity)
	int ret = ::system("secedit /configure /db C:\\UBC_SrvConf_PwdComplex_Disable.db /cfg C:\\UBC_SrvConf_PwdComplex_Disable.inf");
	::DeleteFile("C:\\UBC_SrvConf_PwdComplex_Disable.db");
	::DeleteFile("C:\\UBC_SrvConf_PwdComplex_Disable.inf");
	switch( ret )
	{
	case 0: // success -> pass
		break;
	case 1: // can't run command
		setError(errno, _T("Can't run secedit.exe while disable-password-complexity"));
		return false;
		break;
	case 2: // error return
		setError(errno, _T("Can't update disable-password-complexity"));
		return false;
		break;
	}

	CString str_cmd;
	//
	str_cmd.Format(IsExistAccountInfo(m_strAccount) ? IIS_CHANGE_PWD_ACCOUNT : IIS_ADD_ACCOUNT, m_strAccount, m_strPassword);
	ret = ::system(str_cmd);
	switch( ret )
	{
	case 0: // success -> pass
		break;
	case 1: // can't run command
		setError(errno, _T("Can't run net.exe (1)"));
		return false;
		break;
	case 2: // error return
		setError(errno, _T("Can't create account"));
		return false;
		break;
	}

	//
	str_cmd.Format(IIS_SET_ACCOUNT_1, m_strAccount);
	ret = ::system(str_cmd);
	switch( ret )
	{
	case 0: // success -> pass
		break;
	case 1: // can't run command
		setError(errno, _T("Can't run net.exe (2)"));
		return false;
		break;
	case 2: // error return
		setError(errno, _T("Can't apply attributes of account"));
		return false;
		break;
	}

	//
	str_cmd.Format(IIS_SET_ACCOUNT_2, m_strAccount);
	ret = ::system(str_cmd);
	switch( ret )
	{
	case 0: // success -> pass
		break;
	case 1: // can't run command
		setError(errno, _T("Can't run net.exe (3)"));
		return false;
		break;
	case 2: // error return
		setError(errno, _T("Can't apply disable-password-expire of account"));
		return false;
		break;
	}

	//
	str_cmd.Format(IIS_SET_ACCOUNT_3, m_strAccount);
	::system(str_cmd);
	str_cmd.Format(IIS_SET_ACCOUNT_4, m_strAccount);
	ret = ::system(str_cmd);
	switch( ret )
	{
	case 0: // success -> pass
		break;
	case 1: // can't run command
		setError(errno, _T("Can't run net.exe (4)"));
		return false;
		break;
	case 2: // error return
		setError(errno, _T("Can't apply account-group"));
		return false;
		break;
	}

	// create enable.inf
	TRY
	{
		CFile file;
		CFileException fex;
		if( !file.Open("C:\\UBC_SrvConf_PwdComplex_Enable.inf", CFile::modeCreate | CFile::modeWrite | CFile::typeBinary, &fex) )
		{
			// create error
			setError(fex.m_cause, _T("Fail to create enable.inf"));
			return false;
		}

		const char buf_inf[] = "[System Access]\r\nPasswordComplexity=1\r\n[Version]\r\nsignature=\"$CHICAGO$\"\r\nRevision=1";
		file.Write(buf_inf, strlen(buf_inf));
		file.Close();
	}
	CATCH(CFileException, ex)
	{
		::DeleteFile("C:\\UBC_SrvConf_PwdComplex_Enable.inf");
		setError(ex->m_cause, _T("Fail to write enable.inf"));
		return false;
	}
	END_CATCH

	// run secedit (disable password complexity)
	ret = ::system("secedit /configure /db C:\\UBC_SrvConf_PwdComplex_Enable.db /cfg C:\\UBC_SrvConf_PwdComplex_Enable.inf");
	::DeleteFile("C:\\UBC_SrvConf_PwdComplex_Enable.db");
	::DeleteFile("C:\\UBC_SrvConf_PwdComplex_Enable.inf");
	switch( ret )
	{
	case 0: // success -> pass
		break;
	case 1: // can't run command
		setError(errno, _T("Can't run secedit.exe while enable-password-complexity"));
		return false;
		break;
	case 2: // error return
		setError(errno, _T("Can't update enable-password-complexity"));
		return false;
		break;
	}

	return true;
}

bool CIisSetting::IsExistAccountInfo(CString strAccount)
{
	CString strCmd = IIS_LIST_ACCOUNT;
	CCommandRedirector redir;
	if( !redir.Open(strCmd) )
	{
		redir.Close();
		CString msg;
		msg.Format("Fail to execute command(%s)", strCmd);
		setError(-1, msg);
		return _T("");
	}
	redir.Close();

	bool find_account = false;
	bool find_start = false;
	bool find_end = false;
	int nPos = 0;
	while( !find_account )
	{
		CString strRow = redir.m_strResult.Tokenize("\n", nPos);
		if( strRow.IsEmpty() ) break;

		if( strRow.Find(_T("---")) >= 0 ) find_start = true;
		if( strRow.Find(_T(".")) >= 0 ) find_end = true;

		if( find_start && !find_end )
		{
			int pos = 0;
			while( !find_account )
			{
				CString str_account = redir.m_strResult.Tokenize(" \t", pos);
				if( str_account.IsEmpty() ) break;

				if( stricmp(strAccount, str_account) == 0 ) find_account=true;
			}
		}
	}

	return find_account;
}

