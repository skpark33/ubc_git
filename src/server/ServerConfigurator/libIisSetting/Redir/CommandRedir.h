#pragma once
#include "redir.h"

class CCommandRedirector :
	public CRedirector
{
public:
	CCommandRedirector(void);
	~CCommandRedirector(void);

public:
	CString m_strResult;

protected:
	// overrides:
	virtual void WriteStdOut(LPCTSTR pszOutput);
	virtual void WriteStdError(LPCTSTR pszError);
};
