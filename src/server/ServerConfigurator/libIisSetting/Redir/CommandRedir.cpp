#include "StdAfx.h"
#include "CommandRedir.h"

CCommandRedirector::CCommandRedirector(void)
:	m_strResult ("")
{
}

CCommandRedirector::~CCommandRedirector(void)
{
}

void CCommandRedirector::WriteStdOut(LPCTSTR pszOutput)
{
	m_strResult.Append(pszOutput);
}

void CCommandRedirector::WriteStdError(LPCTSTR pszError)
{
	m_strResult.Append(pszError);
}
