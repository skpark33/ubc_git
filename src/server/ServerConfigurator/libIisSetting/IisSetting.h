#pragma once

using namespace std;

#include "ServerConfigurator/Installable/installable.h"

#define IIS_UBC_DEFAULT_NAME	_T("UBC_DEFAULT")
#define IIS_UBC_HTTP_NAME		_T("UBC_HTTP")
#define IIS_UBC_WEB_NAME		_T("UBC_WEB")
#define IIS_UBC_FTP_NAME		_T("UBC_FTP")

#define IIS_NAME			_T("NAME")
#define IIS_PORT			_T("PORT")
#define IIS_ACCOUNT			_T("ACCOUNT")
#define IIS_PASSWORD		_T("PASSWORD")
#define IIS_LOW_PORT		_T("LOWPORT")
#define IIS_HIGH_PORT		_T("HOGHPORT")

class CIisSetting : public installable
{
public:
	CIisSetting(CString strWebSiteName, int nPort);
	virtual ~CIisSetting(void);

	int prerequisite();
	int test();
	int	install();

	int get(const char* key, string& value);
	int get(const char* key, int& value);

	int set(const char* key, const char* value);
	int set(const char* key, int value);

private:
	CString m_strWebSiteName;
	int		m_nPort;
	CString m_strAppCmd;
	CString m_strAccount;
	CString m_strPassword;
	int		m_nPassiveLowPort;
	int		m_nPassiveHighPort;

	CString GetSiteInfo(CString strName);
	CString GetPoolInfo(CString strName);
	CString GetAppInfo(CString strName);

	bool	ExecuteAppCmd(CString strCmd);
	bool	AddSite(CString strSiteName, int nPort, CString strPath, bool bRenew=TRUE);
	bool	AddVdir(CString strSiteName, CString strVdirName, CString strPath);
	bool	AddApp(CString strSiteName, CString strAppName, CString strPath);
	bool	AddAppPool(CString strAppPoolName);
	bool	SetVdirDirectoryBrowse(CString strSiteName);

	bool	AddFtpSite(CString strSiteName, int nPort, CString strPath, bool bRenew=TRUE);
	bool	SetFtpSite(CString strSiteName, CString strAccount, int nLowPort, int nHighPort);
	bool	GetFTPRangePort(int& nLowPort, int& nHighPort);
	bool	CreateAccount(CString strAccount, CString strPassword);

	CString	GetAccountInfo(CString strName);
	bool	IsExistAccountInfo(CString strAccount);
};
