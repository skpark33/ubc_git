// DBSettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "DBSettingsDlg.h"

#include "ErrorMessageDlg.h"


#define		DEFAULT_DEFAULT_SERVER_IP			"127.0.0.1"
#define		DEFAULT_DEFAULT_SERVER_PORT_MYSQL	(3306)
#define		DEFAULT_DEFAULT_SERVER_PORT_MSSQL	(1433)
#define		DEFAULT_DEFAULT_SERVER_PORT_MARIA	DEFAULT_DEFAULT_SERVER_PORT_MYSQL
#define		DEFAULT_DEFAULT_INSTANCE_NAME		"dbo"
#define		DEFAULT_DEFAULT_USER_ID				"ubc"
#define		DEFAULT_DEFAULT_PASSWORD			"-507263a"

#define		DEFAULT_MYSQL_ROOT_DBNAME			"mysql"
#define		DEFAULT_MYSQL_ROOT_ID				"root"
#define		DEFAULT_MYSQL_ROOT_PWD				"sqicop"


// CDBSettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDBSettingsDlg, CSubDlg)

CDBSettingsDlg::CDBSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*COSSettingsDlg::IDD,*/ pParent)
{
	m_bCreateInfo = false;

	m_bCreateUserCompleted = false;
	m_bCreateInstanceCompleted = false;
	m_bCreateTableCompleted = false;
	m_bCreateInitDataCompleted = false;
}

CDBSettingsDlg::~CDBSettingsDlg()
{
}

void CDBSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RADIO_MYSQL, m_radioMySQL);
	DDX_Control(pDX, IDC_RADIO_MSSQL, m_radioMSSQL);
	DDX_Control(pDX, IDC_RADIO_MARIA, m_radioMARIA);
	DDX_Control(pDX, IDC_EDIT_DB_TYPE, m_editDBType);
	DDX_Control(pDX, IDC_EDIT_SERVER_IP, m_editServerIP);
	DDX_Control(pDX, IDC_EDIT_SERVER_PORT, m_editServerPort);
	DDX_Control(pDX, IDC_EDIT_ROOT_INSTANCE_NAME, m_editRootInstanceName);
	DDX_Control(pDX, IDC_EDIT_ROOT_USER_ID, m_editRootUserID);
	DDX_Control(pDX, IDC_EDIT_ROOT_PASSWORD, m_editRootPassword);
	DDX_Control(pDX, IDC_EDIT_INSTANCE_NAME, m_editInstanceName);
	DDX_Control(pDX, IDC_EDIT_USER_ID, m_editUserID);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_editPassword);
	DDX_Control(pDX, IDC_BUTTON_CREATE_INFO, m_btnCreateInfo);
	DDX_Control(pDX, IDC_CHECK_CREATE_USER, m_chkCreateUser);
	DDX_Control(pDX, IDC_BUTTON_CREATE_USER, m_btnCreateUser);
	DDX_Control(pDX, IDC_CHECK_CREATE_INSTANCE, m_chkCreateInstance);
	DDX_Control(pDX, IDC_BUTTON_CREATE_INSTANCE, m_btnCreateInstance);
	DDX_Control(pDX, IDC_CHECK_CREATE_TABLE, m_chkCreateTable);
	DDX_Control(pDX, IDC_BUTTON_CREATE_TABLE, m_btnCreateTable);
	DDX_Control(pDX, IDC_CHECK_CREATE_INIT_DATA, m_chkCreateInitData);
	DDX_Control(pDX, IDC_BUTTON_CREATE_INIT_DATA, m_btnCreateInitData);
	DDX_Control(pDX, IDC_BUTTON_EXECUTE_ALTERTABLE, m_btnExecuteAlterTable);
}


BEGIN_MESSAGE_MAP(CDBSettingsDlg, CSubDlg)
	ON_BN_CLICKED(IDC_RADIO_MYSQL, &CDBSettingsDlg::OnBnClickedRadioDBType)
	ON_BN_CLICKED(IDC_RADIO_MSSQL, &CDBSettingsDlg::OnBnClickedRadioDBType)
	ON_BN_CLICKED(IDC_RADIO_MARIA, &CDBSettingsDlg::OnBnClickedRadioDBType)
	ON_EN_CHANGE(IDC_EDIT_SERVER_IP, &CDBSettingsDlg::OnEnChangeEditInfo)
	ON_EN_CHANGE(IDC_EDIT_SERVER_PORT, &CDBSettingsDlg::OnEnChangeEditInfo)
	ON_EN_CHANGE(IDC_EDIT_INSTANCE_NAME, &CDBSettingsDlg::OnEnChangeEditInfo)
	ON_EN_CHANGE(IDC_EDIT_USER_ID, &CDBSettingsDlg::OnEnChangeEditInfo)
	ON_EN_CHANGE(IDC_EDIT_PASSWORD, &CDBSettingsDlg::OnEnChangeEditInfo)
	ON_BN_CLICKED(IDC_BUTTON_CREATE_INFO, &CDBSettingsDlg::OnBnClickedButtonCreateInfo)
	ON_BN_CLICKED(IDC_CHECK_CREATE_USER, &CDBSettingsDlg::OnBnClickedCheckCreateUser)
	ON_BN_CLICKED(IDC_BUTTON_CREATE_USER, &CDBSettingsDlg::OnBnClickedButtonCreateUser)
	ON_BN_CLICKED(IDC_CHECK_CREATE_INSTANCE, &CDBSettingsDlg::OnBnClickedCheckCreateInstance)
	ON_BN_CLICKED(IDC_BUTTON_CREATE_INSTANCE, &CDBSettingsDlg::OnBnClickedButtonCreateInstance)
	ON_BN_CLICKED(IDC_CHECK_CREATE_TABLE, &CDBSettingsDlg::OnBnClickedCheckCreateTable)
	ON_BN_CLICKED(IDC_BUTTON_CREATE_TABLE, &CDBSettingsDlg::OnBnClickedButtonCreateTable)
	ON_BN_CLICKED(IDC_CHECK_INIT_DATA, &CDBSettingsDlg::OnBnClickedCheckInitData)
	ON_BN_CLICKED(IDC_BUTTON_INIT_DATA, &CDBSettingsDlg::OnBnClickedButtonInitData)
	ON_BN_CLICKED(IDC_BUTTON_EXECUTE_ALTERTABLE, &CDBSettingsDlg::OnBnClickedButtonExecuteAltertable)
END_MESSAGE_MAP()


// CDBSettingsDlg 메시지 처리기입니다.

BOOL CDBSettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	AddColorIgnoreWnd( m_editRootInstanceName.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editRootUserID.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editRootPassword.GetSafeHwnd() );

	AddControl( m_radioMySQL.GetSafeHwnd() );
	AddControl( m_radioMSSQL.GetSafeHwnd() );
	AddControl( m_radioMARIA.GetSafeHwnd() );
	AddControl( m_editServerIP.GetSafeHwnd() );
	AddControl( m_editServerPort.GetSafeHwnd() );
	AddControl( m_editRootInstanceName.GetSafeHwnd() );
	AddControl( m_editRootUserID.GetSafeHwnd() );
	AddControl( m_editRootPassword.GetSafeHwnd() );
	AddControl( m_editInstanceName.GetSafeHwnd() );
	AddControl( m_editUserID.GetSafeHwnd() );
	AddControl( m_editPassword.GetSafeHwnd() );
	AddControl( m_btnCreateInfo.GetSafeHwnd() );
	AddControl( m_chkCreateUser.GetSafeHwnd() );
	AddControl( m_btnCreateUser.GetSafeHwnd() );
	AddControl( m_chkCreateInstance.GetSafeHwnd() );
	AddControl( m_btnCreateInstance.GetSafeHwnd() );
	AddControl( m_chkCreateTable.GetSafeHwnd() );
	AddControl( m_btnCreateTable.GetSafeHwnd() );
	AddControl( m_chkCreateInitData.GetSafeHwnd() );
	AddControl( m_btnCreateInitData.GetSafeHwnd() );

	m_btnCreateInfo.Init(&m_strDBType, &m_editDBType);
	m_btnCreateInfo.Init(&m_strServerIP, &m_editServerIP);
	m_btnCreateInfo.Init(&m_strServerPort, &m_editServerPort);
	m_btnCreateInfo.Init(&m_strInstanceName, &m_editInstanceName);
	m_btnCreateInfo.Init(&m_strUserID, &m_editUserID);
	m_btnCreateInfo.Init(&m_strPassword, &m_editPassword);

	m_btnCreateUser.Init(&m_bCreateUserCompleted, &m_chkCreateUser);
	m_btnCreateInstance.Init(&m_bCreateInstanceCompleted, &m_chkCreateInstance);
	m_btnCreateTable.Init(&m_bCreateTableCompleted, &m_chkCreateTable);
	m_btnCreateInitData.Init(&m_bCreateInitDataCompleted, &m_chkCreateInitData);

	m_editRootInstanceName.SetWindowText(DEFAULT_MYSQL_ROOT_DBNAME);
	m_editRootUserID.SetWindowText(DEFAULT_MYSQL_ROOT_ID);
	m_editRootPassword.SetWindowText(DEFAULT_MYSQL_ROOT_PWD);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDBSettingsDlg::OnBnClickedRadioDBType()
{
	CString str_cur_db = GetSelectDBType();

	if( m_strLastSelectedDBType != str_cur_db )
	{
		m_strLastSelectedDBType = str_cur_db;

		m_editDBType.SetWindowText(str_cur_db);

		dbServer db_server(str_cur_db);
		SetDBField(&db_server);

		UpdateControls();
		UpdateInstalledStatus();
	}
}

void CDBSettingsDlg::OnEnChangeEditInfo()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CSubDlg::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	// 연관된 CButton_String 컨트롤
	m_btnCreateInfo.UpdateControl();
}

void CDBSettingsDlg::OnBnClickedButtonCreateInfo()
{
	//
	CString str_db_type = GetSelectDBType();

	// ip
	CString str_db_server_ip;
	m_editServerIP.GetWindowText(str_db_server_ip);

	// port
	CString str_db_port;
	m_editServerPort.GetWindowText(str_db_port);

	// instance
	CString str_instance_name;
	m_editInstanceName.GetWindowText(str_instance_name);

	// user id
	CString str_db_user_id;
	m_editUserID.GetWindowText(str_db_user_id);

	// password
	CString str_db_pwd;
	m_editPassword.GetWindowText(str_db_pwd);

	BeginWaitCursor();

	//
	dbServer db_server(str_db_type, str_db_server_ip, str_instance_name, str_db_user_id, str_db_pwd, atoi(str_db_port));
	if(str_db_type == DB_KIND_MYSQL)
	{
		// root instance
		CString str_root_instance_name;
		m_editRootInstanceName.GetWindowText(str_root_instance_name);

		// root user id
		CString str_root_user_id;
		m_editRootUserID.GetWindowText(str_root_user_id);

		// root password
		CString str_root_pwd;
		m_editRootPassword.GetWindowText(str_root_pwd);

		//
		db_server.set(DB_ROOT_DBNAME, str_root_instance_name);
		db_server.set(DB_ROOT_USR, str_root_user_id);
		db_server.set(DB_ROOT_PWD, str_root_pwd);
	}
	else if(str_db_type == DB_KIND_MSSQL)
	{
		// nothing to set
	}
	else if(str_db_type == DB_KIND_MARIA)
	{
		//
		// set special-attributes
		//
	}

	//
	if( !db_server.prerequisite() )
	{
		EndWaitCursor();

		CErrorMessageDlg dlg("Prerequisite Error !!!", db_server.getErrorCode(), db_server.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}
	if( !db_server.test() )
	{
		EndWaitCursor();

		CErrorMessageDlg dlg("Connection Error !!!", db_server.getErrorCode(), db_server.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}
	if( !db_server.install() )
	{
		EndWaitCursor();

		CErrorMessageDlg dlg("Install Error !!!", db_server.getErrorCode(), db_server.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}

	m_bCreateInfo = true;

	// all success
	m_strDBType       = str_db_type;
	m_strServerIP     = str_db_server_ip;
	m_strServerPort   = str_db_port;
	m_strInstanceName = str_instance_name;
	m_strUserID       = str_db_user_id;
	m_strPassword     = str_db_pwd;

	CheckInstallComplete();
	UpdateControls();
	UpdateInstalledStatus();

	EndWaitCursor();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void CDBSettingsDlg::OnBnClickedCheckCreateUser()
{
	UpdateControls();
}

void CDBSettingsDlg::OnBnClickedButtonCreateUser()
{
	//
	if( !m_bCreateInfo )
	{
		::AfxMessageBox(IDS_MSG_NOT_INSTALLED_CANT_INSTALL, MB_ICONWARNING);
		return;
	}

	BeginWaitCursor();

	//
	CString db_type = GetSelectDBType();

	createUser create_user;
	if(db_type == DB_KIND_MYSQL)
	{
		// root instance
		CString str_root_instance_name;
		m_editRootInstanceName.GetWindowText(str_root_instance_name);

		// root user id
		CString str_root_user_id;
		m_editRootUserID.GetWindowText(str_root_user_id);

		// root password
		CString str_root_pwd;
		m_editRootPassword.GetWindowText(str_root_pwd);

		//
		create_user.set(DB_ROOT_DBNAME, str_root_instance_name);
		create_user.set(DB_ROOT_USR, str_root_user_id);
		create_user.set(DB_ROOT_PWD, str_root_pwd);
	}
	else if(db_type == DB_KIND_MSSQL)
	{
		// mssql not support. nothing to set
	}
	else if(db_type == DB_KIND_MARIA)
	{
		//
		// set special-attributes
		//
	}

	// prerequisite
	if( !create_user.prerequisite() )
	{
		EndWaitCursor();

		CErrorMessageDlg dlg("Prerequisite Error !!!", create_user.getErrorCode(), create_user.getErrorString(), MB_ICONWARNING);
		dlg.DoModal();
		return;
	}
	// test
	if( db_type != DB_KIND_MSSQL && create_user.test() ) // mssql -> just connect-test
	{
		EndWaitCursor();

		int ret_val = ::AfxMessageBox(IDS_MSG_ALREADY_INSTALLED_CONTINUE_TO_REINSTALL, MB_ICONWARNING | MB_YESNO);
		if( ret_val == IDNO ) return;
	}
	// install
	if( !create_user.install() )
	{
		EndWaitCursor();

		// fail
		CErrorMessageDlg dlg("Install Error !!!", create_user.getErrorCode(), create_user.getErrorString(), MB_ICONWARNING);
		dlg.DoModal();

		return;
	}

	// success
	m_bCreateUserCompleted = true;
	m_chkCreateUser.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	EndWaitCursor();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void CDBSettingsDlg::OnBnClickedCheckCreateInstance()
{
	UpdateControls();
}

void CDBSettingsDlg::OnBnClickedButtonCreateInstance()
{
	//
	if( !m_bCreateInfo )
	{
		::AfxMessageBox(IDS_MSG_NOT_INSTALLED_CANT_INSTALL, MB_ICONWARNING);
		return;
	}

	BeginWaitCursor();

	//
	// check m_bCreateUserCompleted
	// if false, can't continue
	//

	//
	CString db_type = GetSelectDBType();

	createDB create_instance;
	if(db_type == DB_KIND_MYSQL)
	{
		// root instance
		CString str_root_instance_name;
		m_editRootInstanceName.GetWindowText(str_root_instance_name);

		// root user id
		CString str_root_user_id;
		m_editRootUserID.GetWindowText(str_root_user_id);

		// root password
		CString str_root_pwd;
		m_editRootPassword.GetWindowText(str_root_pwd);

		//
		create_instance.set(DB_ROOT_DBNAME, str_root_instance_name);
		create_instance.set(DB_ROOT_USR, str_root_user_id);
		create_instance.set(DB_ROOT_PWD, str_root_pwd);
	}
	else if(db_type == DB_KIND_MSSQL)
	{
		// mssql not support
	}
	else if(db_type == DB_KIND_MARIA)
	{
		//
		// set special-attributes
		//
	}

	//
	if( !create_instance.prerequisite() )
	{
		EndWaitCursor();

		CErrorMessageDlg dlg("Prerequisite Error !!!", create_instance.getErrorCode(), create_instance.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}

	// install
	if( !create_instance.install() )
	{
		// fail
		EndWaitCursor();

		CErrorMessageDlg dlg("Install Error !!!", create_instance.getErrorCode(), create_instance.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}

	// success
	m_bCreateInstanceCompleted = true;
	m_chkCreateInstance.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	EndWaitCursor();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void CDBSettingsDlg::OnBnClickedCheckCreateTable()
{
	UpdateControls();
}

void CDBSettingsDlg::OnBnClickedButtonCreateTable()
{
	//
	if( !m_bCreateInfo )
	{
		::AfxMessageBox(IDS_MSG_NOT_INSTALLED_CANT_INSTALL, MB_ICONWARNING);
		return;
	}

	BeginWaitCursor();

	//
	// check m_bCreateUserCompleted, m_bCreateInstanceCompleted
	// if false, can't continue
	//

	//
	createTable create_table;

	if(!create_table.prerequisite())
	{
		EndWaitCursor();

		CErrorMessageDlg dlg("Prerequisite Error !!!", create_table.getErrorCode(), create_table.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}

	// test
	CString str_err_code;
	int err_code = 0;
	if( create_table.test() )
	{
		EndWaitCursor();

		int ret_val = ::AfxMessageBox(IDS_MSG_ALREADY_INSTALLED_CONTINUE_TO_REINSTALL, MB_ICONWARNING | MB_YESNO);
		if( ret_val == IDNO ) return;
	}

	// install
	if( !create_table.install() )
	{
		EndWaitCursor();

		// fail
		CErrorMessageDlg dlg("Install Error !!!", create_table.getErrorCode(), create_table.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}

	// success
	m_bCreateTableCompleted = true;
	m_chkCreateTable.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	EndWaitCursor();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

void CDBSettingsDlg::OnBnClickedCheckInitData()
{
	UpdateControls();
}

void CDBSettingsDlg::OnBnClickedButtonInitData()
{
	//
	if( !m_bCreateInfo )
	{
		::AfxMessageBox(IDS_MSG_NOT_INSTALLED_CANT_INSTALL, MB_ICONWARNING);
		return;
	}

	BeginWaitCursor();

	//
	// check m_bCreateUserCompleted, m_bCreateInstanceCompleted, m_bCreateUserCompleted
	// if false, can't continue
	//

	//
	CString db_type = GetSelectDBType();

	createData create_data;
	if(db_type == DB_KIND_MYSQL)
	{
		// root instance
		CString str_root_instance_name;
		m_editRootInstanceName.GetWindowText(str_root_instance_name);

		// root user id
		CString str_root_user_id;
		m_editRootUserID.GetWindowText(str_root_user_id);

		// root password
		CString str_root_pwd;
		m_editRootPassword.GetWindowText(str_root_pwd);

		//
		create_data.set(DB_ROOT_DBNAME, str_root_instance_name);
		create_data.set(DB_ROOT_USR, str_root_user_id);
		create_data.set(DB_ROOT_PWD, str_root_pwd);
	}
	else if(db_type == DB_KIND_MSSQL)
	{
		// mssql not support
	}
	else if(db_type == DB_KIND_MARIA)
	{
		//
		// set special-attributes
		//
	}

	//
	if(!create_data.prerequisite())
	{
		EndWaitCursor();

		CErrorMessageDlg dlg("Prerequisite Error !!!", create_data.getErrorCode(), create_data.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}

	// test
	if( create_data.test() )
	{
		int ret_val = ::AfxMessageBox(IDS_MSG_ALREADY_INSTALLED_CONTINUE_TO_REINSTALL, MB_ICONWARNING | MB_YESNO);
		if( ret_val == IDNO ) return;
	}

	// install
	if( !create_data.install() )
	{
		EndWaitCursor();

		// fail
		CErrorMessageDlg dlg("Install Error !!!", create_data.getErrorCode(), create_data.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
		return;
	}

	// success
	m_bCreateInitDataCompleted = true;
	m_chkCreateInitData.SetCheck(BST_UNCHECKED);

	CheckInstallComplete();
	UpdateControls();

	EndWaitCursor();

	::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
}

CString CDBSettingsDlg::GetSelectDBType()
{
	if(m_radioMySQL.GetCheck())
	{
		return DB_KIND_MYSQL;
	}
	else if(m_radioMSSQL.GetCheck())
	{
		return DB_KIND_MSSQL;
	}
	else if(m_radioMARIA.GetCheck())
	{
		return DB_KIND_MARIA;
	}
	return "";
}

void CDBSettingsDlg::UpdateInstalledStatus()
{
	if( !m_bCreateInfo ) return;

	// db type
	CString str_db_type = GetSelectDBType();

	// ip
	CString str_server_ip;
	m_editServerIP.GetWindowText(str_server_ip);

	// port
	CString str_server_port;
	m_editServerPort.GetWindowText(str_server_port);

	// root instance
	CString str_root_instance;
	m_editRootInstanceName.GetWindowText(str_root_instance);

	// root id
	CString str_root_id;
	m_editRootUserID.GetWindowText(str_root_id);

	// root pwd
	CString str_root_pwd;
	m_editRootPassword.GetWindowText(str_root_pwd);

	// instance
	CString str_instance;
	m_editInstanceName.GetWindowText(str_instance);

	// id
	CString str_id;
	m_editUserID.GetWindowText(str_id);

	// pwd
	CString str_pwd;
	m_editPassword.GetWindowText(str_pwd);

	//
	createUser	create_user;
	createDB	create_db;
	createTable	create_table;
	createData	create_data;

	//
	create_user.set (DB_KIND, str_db_type);
	create_db.set   (DB_KIND, str_db_type);
	create_table.set(DB_KIND, str_db_type);
	create_data.set (DB_KIND, str_db_type);

	create_user.set (DB_SERVER, str_server_ip);
	create_db.set   (DB_SERVER, str_server_ip);
	create_table.set(DB_SERVER, str_server_ip);
	create_data.set (DB_SERVER, str_server_ip);

	create_user.set (DB_PORT, str_server_port);
	create_db.set   (DB_PORT, str_server_port);
	create_table.set(DB_PORT, str_server_port);
	create_data.set (DB_PORT, str_server_port);

	create_user.set (DB_ROOT_DBNAME, str_root_instance);
	create_db.set   (DB_ROOT_DBNAME, str_root_instance);
	create_table.set(DB_ROOT_DBNAME, str_root_instance);
	create_data.set (DB_ROOT_DBNAME, str_root_instance);

	create_user.set (DB_ROOT_USR, str_root_id);
	create_db.set   (DB_ROOT_USR, str_root_id);
	create_table.set(DB_ROOT_USR, str_root_id);
	create_data.set (DB_ROOT_USR, str_root_id);

	create_user.set (DB_ROOT_PWD, str_root_pwd);
	create_db.set   (DB_ROOT_PWD, str_root_pwd);
	create_table.set(DB_ROOT_PWD, str_root_pwd);
	create_data.set (DB_ROOT_PWD, str_root_pwd);

	create_user.set (DB_DBNAME, str_instance);
	create_db.set   (DB_DBNAME, str_instance);
	create_table.set(DB_DBNAME, str_instance);
	create_data.set (DB_DBNAME, str_instance);

	create_user.set (DB_USR, str_id);
	create_db.set   (DB_USR, str_id);
	create_table.set(DB_USR, str_id);
	create_data.set (DB_USR, str_id);

	create_user.set (DB_PWD, str_pwd);
	create_db.set   (DB_PWD, str_pwd);
	create_table.set(DB_PWD, str_pwd);
	create_data.set (DB_PWD, str_pwd);

	//
	if(create_user.test())
	{
		m_bCreateUserCompleted = true;
		m_chkCreateUser.SetCheck(BST_UNCHECKED);
	}
	else
	{
		m_bCreateUserCompleted = false;
		m_chkCreateUser.SetCheck(BST_CHECKED);
	}

	//
	if(create_db.test())
	{
		m_bCreateInstanceCompleted = true;
		m_chkCreateInstance.SetCheck(BST_UNCHECKED);
	}
	else
	{
		m_bCreateInstanceCompleted = false;
		m_chkCreateInstance.SetCheck(BST_CHECKED);
	}

	//
	if(create_table.test())
	{
		m_bCreateTableCompleted = true;
		m_chkCreateTable.SetCheck(BST_UNCHECKED);
	}
	else
	{
		m_bCreateTableCompleted = false;
		m_chkCreateTable.SetCheck(BST_CHECKED);
	}

	//
	if(create_data.test())
	{
		m_bCreateInitDataCompleted = true;
		m_chkCreateInitData.SetCheck(BST_UNCHECKED);
	}
	else
	{
		m_bCreateInitDataCompleted = false;
		m_chkCreateInitData.SetCheck(BST_CHECKED);
	}

	m_btnCreateUser.UpdateControl();
	m_btnCreateInstance.UpdateControl();
	m_btnCreateTable.UpdateControl();
	m_btnCreateInitData.UpdateControl();
}

void CDBSettingsDlg::UpdateControls()
{
	m_btnCreateInfo.UpdateControl();

	//
	if( m_btnCreateInfo.GetStyle() & WS_DISABLED )
	{
		m_bCreateInfo = true;

		m_chkCreateUser.EnableWindow(TRUE);
		m_btnCreateUser.EnableWindow(TRUE);
		m_chkCreateInstance.EnableWindow(TRUE);
		m_btnCreateInstance.EnableWindow(TRUE);
		m_chkCreateTable.EnableWindow(TRUE);
		m_btnCreateTable.EnableWindow(TRUE);
		m_chkCreateInitData.EnableWindow(TRUE);
		m_btnCreateInitData.EnableWindow(TRUE);
	}
	else
	{
		m_bCreateInfo = false;
	}

	m_btnCreateUser.UpdateControl();
	m_btnCreateInstance.UpdateControl();
	m_btnCreateTable.UpdateControl();
	m_btnCreateInitData.UpdateControl();

	//
	BOOL enable_win = (GetSelectDBType()==DB_KIND_MSSQL ? FALSE : TRUE);
	m_editRootInstanceName.EnableWindow(enable_win);
	m_editRootUserID.EnableWindow(enable_win);
	m_editRootPassword.EnableWindow(enable_win);

	//
	if( !m_bCreateInfo )
	{
		m_chkCreateUser.EnableWindow(FALSE);
		m_btnCreateUser.EnableWindow(FALSE);
		m_chkCreateInstance.EnableWindow(FALSE);
		m_btnCreateInstance.EnableWindow(FALSE);
		m_chkCreateTable.EnableWindow(FALSE);
		m_btnCreateTable.EnableWindow(FALSE);
		m_chkCreateInitData.EnableWindow(FALSE);
		m_btnCreateInitData.EnableWindow(FALSE);
	}

	//
	m_btnExecuteAlterTable.EnableWindow(m_bInstallComplete);
}

void CDBSettingsDlg::SetDBField(dbServer* pDBServer, bool bSetToDefaultValue)
{
	if( pDBServer==NULL ) return;

	//
	string db_type;
	pDBServer->get(DB_KIND, db_type);
	m_editDBType.SetWindowText(db_type.c_str());

	//
	string db_ip;
	pDBServer->get(DB_SERVER, db_ip);
	if( db_ip.length()==0 ) db_ip=DEFAULT_DEFAULT_SERVER_IP;
	m_editServerIP.SetWindowText(db_ip.c_str());

	//
	int db_port=0;
	pDBServer->get(DB_PORT, db_port);
	if( db_port==0 )
	{
		if( db_type==DB_KIND_MYSQL ) db_port=DEFAULT_DEFAULT_SERVER_PORT_MYSQL;
		else if( db_type==DB_KIND_MSSQL ) db_port=DEFAULT_DEFAULT_SERVER_PORT_MSSQL;
		else if( db_type==DB_KIND_MARIA ) db_port=DEFAULT_DEFAULT_SERVER_PORT_MARIA;
	}
	m_editServerPort.SetWindowText(::ToString(db_port));

	//
	string root_instance_name;
	pDBServer->get(DB_ROOT_DBNAME, root_instance_name);
	if( root_instance_name.length()==0 ) root_instance_name  = DEFAULT_MYSQL_ROOT_DBNAME;
	m_editRootInstanceName.SetWindowText(root_instance_name.c_str());

	//
	string root_user_id;
	pDBServer->get(DB_ROOT_USR, root_user_id);
	if( root_user_id.length()==0 ) root_user_id  = DEFAULT_MYSQL_ROOT_ID;
	m_editRootUserID.SetWindowText(root_user_id.c_str());

	//
	string root_password;
	pDBServer->get(DB_ROOT_PWD, root_password);
	if( root_password.length()==0 ) root_password  = DEFAULT_MYSQL_ROOT_PWD;
	m_editRootPassword.SetWindowText(root_password.c_str());

	//
	string instance_name;
	pDBServer->get(DB_DBNAME, instance_name);
	if( instance_name.length()==0 ) instance_name=DEFAULT_DEFAULT_INSTANCE_NAME;
	m_editInstanceName.SetWindowText(instance_name.c_str());

	//
	string db_user;
	pDBServer->get(DB_USR, db_user);
	if( db_user.length()==0 ) db_user=DEFAULT_DEFAULT_USER_ID;
	m_editUserID.SetWindowText(db_user.c_str());

	//
	string db_pwd;
	pDBServer->get(DB_PWD, db_pwd);
	if( db_pwd.length()==0 ) db_pwd=DEFAULT_DEFAULT_PASSWORD;
	m_editPassword.SetWindowText(db_pwd.c_str());

	// test
	if(db_type == DB_KIND_MYSQL)
	{
		pDBServer->set(DB_ROOT_DBNAME, root_instance_name.c_str());
		pDBServer->set(DB_ROOT_USR, root_user_id.c_str());
		pDBServer->set(DB_ROOT_PWD, root_password.c_str());
	}
	else if(db_type == DB_KIND_MSSQL)
	{
		// nothing to set
	}
	else if(db_type == DB_KIND_MARIA)
	{
		//
		// set special-attributes
		//
	}

	//
	if( bSetToDefaultValue )
	{
		m_strDBType = db_type.c_str();
		m_strServerIP = db_ip.c_str();
		m_strServerPort = ::ToString(db_port);
		m_strInstanceName = instance_name.c_str();
		m_strUserID = db_user.c_str();
		m_strPassword = db_pwd.c_str();
	}
}

bool CDBSettingsDlg::RunInit()
{
	//
	dbServer default_db_server;

	//
	string db_type;
	default_db_server.get(DB_KIND, db_type);
	if(db_type == DB_KIND_MYSQL)
	{
		// mysql
		m_radioMySQL.SetCheck(BST_CHECKED);
		m_strDBType = DB_KIND_MYSQL;
	}
	else if(db_type == DB_KIND_MSSQL)
	{
		// mssql
		m_radioMSSQL.SetCheck(BST_CHECKED);
		m_strDBType = DB_KIND_MSSQL;
	}
	else if(db_type == DB_KIND_MARIA)
	{
		// maria
		m_radioMySQL.SetCheck(BST_CHECKED);
		m_strDBType = DB_KIND_MARIA;
	}
	m_strLastSelectedDBType = m_strDBType;

	//
	SetDBField(&default_db_server);

	//
	if( !default_db_server.prerequisite() )
	{
		UpdateInstalledStatus();

		CString msg;
		msg.Format("prerequisite ERROR\r\n\r\nERR_CODE=%d\r\nERR_STRING=%s", default_db_server.getErrorCode(), default_db_server.getErrorString());
		//::AfxMessageBox(msg, MB_ICONSTOP);
		return FALSE;
	}

	//
	if( !default_db_server.test() )
	{
		UpdateInstalledStatus();

		CString msg;
		msg.Format("test ERROR\r\n\r\nERR_CODE=%d\r\nERR_STRING=%s", default_db_server.getErrorCode(), default_db_server.getErrorString());
		//::AfxMessageBox(msg, MB_ICONSTOP);
		return FALSE;
	}

	m_bCreateInfo = true;
	SetDBField(&default_db_server, true);

	UpdateInstalledStatus();

	return TRUE;
}

int CDBSettingsDlg::CheckInstallComplete()
{
	m_bInstallComplete = true;

	if( m_bCreateInfo == false || 
		m_bCreateUserCompleted == false || 
		m_bCreateInstanceCompleted == false || 
		m_bCreateTableCompleted == false || 
		m_bCreateInitDataCompleted == false )
	{
		m_bInstallComplete = false;
	}

	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}

void CDBSettingsDlg::OnBnClickedButtonExecuteAltertable()
{
	CString command = "UTV_sample.exe +server +ubc +noES +noxlog +util +alterTable";
	if( system(command) !=0 )
	{
		//utvERROR(("%s failed", command.c_str()));
		//CErrorMessageDlg dlg("AlterTable Error !!!", 0, command, MB_ICONSTOP);
		//dlg.DoModal();
		::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
	}
	else
	{
		//utvDEBUG(("%s succeed", command.c_str()));
		::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
	}
}
