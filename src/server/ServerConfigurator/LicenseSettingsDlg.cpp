// LicenseSettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ServerConfigurator.h"
#include "LicenseSettingsDlg.h"

//#include "ci/libDebug/ciDebug.h"
#include "ErrorMessageDlg.h"
#include "SysAdminLoginDlg.h"


//ciSET_DEBUG(10, "CLicenseSettingsDlg");


// CLicenseSettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLicenseSettingsDlg, CSubDlg)

CLicenseSettingsDlg::CLicenseSettingsDlg(CServerConfiguratorDoc* pDoc, CWnd* pParent /*=NULL*/)
	: CSubDlg(pDoc, /*CLicenseSettingsDlg::IDD,*/ pParent)
	, m_bIsAuthenticated ( false )
	, m_nLButtonPush ( 0 )
	, m_bOnceTimeForceAuth ( false )
{
}

CLicenseSettingsDlg::~CLicenseSettingsDlg()
{
}

void CLicenseSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubDlg::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_IP_ADDR, m_editIPAddr);
	DDX_Control(pDX, IDC_EDIT_MAC_ADDR, m_editMACAddr);
	DDX_Control(pDX, IDC_EDIT_ENTERPRISE_KEY, m_editEnterKey);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, m_editPwd);
	DDX_Control(pDX, IDC_BUTTON_AUTH_BYPASS, m_btnBypassAuth);
	DDX_Control(pDX, IDC_BUTTON_AUTH, m_btnAuth);
	DDX_Control(pDX, IDC_STATIC_INFO, m_stcInfo);
	DDX_Control(pDX, IDC_STATIC_SETTINGS, m_stcSettings);
}


BEGIN_MESSAGE_MAP(CLicenseSettingsDlg, CSubDlg)
	ON_BN_CLICKED(IDC_BUTTON_AUTH, &CLicenseSettingsDlg::OnBnClickedButtonAuth)
	ON_EN_CHANGE(IDC_EDIT_ENTERPRISE_KEY, &CLicenseSettingsDlg::OnEnChangeEditKeyPassword)
	ON_EN_CHANGE(IDC_EDIT_PASSWORD, &CLicenseSettingsDlg::OnEnChangeEditKeyPassword)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_BUTTON_AUTH_BYPASS, &CLicenseSettingsDlg::OnBnClickedButtonAuthBypass)
END_MESSAGE_MAP()


// CLicenseSettingsDlg 메시지 처리기입니다.

BOOL CLicenseSettingsDlg::OnInitDialog()
{
	CSubDlg::OnInitDialog();

	AddColorIgnoreWnd( m_editIPAddr.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editMACAddr.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editEnterKey.GetSafeHwnd() );
	AddColorIgnoreWnd( m_editPwd.GetSafeHwnd() );

	AddControl( m_editIPAddr.GetSafeHwnd() );
	AddControl( m_editMACAddr.GetSafeHwnd() );
	AddControl( m_editEnterKey.GetSafeHwnd() );
	AddControl( m_editPwd.GetSafeHwnd() );
	AddControl( m_btnAuth.GetSafeHwnd() );

	m_btnAuth.Init( &m_strEnterpriseKey, &m_editEnterKey );
	m_btnAuth.Init( &m_strPassword, &m_editPwd );

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CLicenseSettingsDlg::OnBnClickedButtonAuth()
{
	//ciDEBUG(10, ("OnBnClickedButtonAuth()"));
/*
	if( !m_serverAuth.IsIPConfirmed() )
	{
		ciWARN((m_serverAuth.getErrorString()));
		AfxMessageBox(m_serverAuth.getErrorString(), MB_ICONWARNING);
		return;
	}
	m_editIPAddr.SetWindowText(m_serverAuth.get("CurrentIPAddress"));
	m_editMACAddr.SetWindowText(m_serverAuth.get("CurrentMACAddress"));
*/

	//
	CString str_ip, str_mac_addr, str_ent_key, str_pwd;
	m_editIPAddr.GetWindowText(str_ip);
	m_editMACAddr.GetWindowText(str_mac_addr);
	m_editEnterKey.GetWindowText(str_ent_key);
	m_editPwd.GetWindowText(str_pwd);

	//ciDEBUG(10, ("CurrentIPAddress=%s", str_ip));
	//ciDEBUG(10, ("EnterpriseKey=%s", str_ent_key));
	//ciDEBUG(10, ("Password=%s", str_pwd));

	//
	if(str_ent_key.GetLength() == 0)
	{
		//ciWARN(("Enterprise Key is empty"));
		AfxMessageBox(IDS_WARN_INPUT_ENTERPRISE_KEY, MB_ICONWARNING);
		return;
	}
	if(str_pwd.GetLength() == 0)
	{
		//ciWARN(("Password is empty"));
		AfxMessageBox(IDS_WARN_INPUT_PASSWORD, MB_ICONWARNING);
		return;
	}

	BeginWaitCursor();

	m_serverAuth.set(KEY_CURRENT_IP_ADDRESS, (LPCSTR)str_ip);
	m_serverAuth.set(KEY_CURRENT_MAC_ADDRESS, str_mac_addr);
	m_serverAuth.set(KEY_ENTERPRISE_KEY, (LPCSTR)str_ent_key);
	m_serverAuth.set(KEY_PASSWORD, (LPCSTR)str_pwd);

	m_serverAuth.SetOnceTimeForceAuth(m_bOnceTimeForceAuth);
	int ret = m_serverAuth.install();

	EndWaitCursor();

	if( ret )
	{
	//	ciDEBUG(10, ("EnterpriseKey is Authorized"));
		m_bIsAuthenticated = true;

		m_strEnterpriseKey = str_ent_key;
		m_strPassword = str_pwd;
	}
	else
	{
	//	ciDEBUG(10, ("EnterpriseKey NOT Authorized"));
		m_bIsAuthenticated = false;
	}

	CheckInstallComplete();
	UpdateControls();

	if( ret )
	{
		::AfxMessageBox(IDS_MSG_SUCCESS, MB_ICONINFORMATION);
	}
	else
	{
		CErrorMessageDlg dlg("Install Error !!!", m_serverAuth.getErrorCode(), m_serverAuth.getErrorString(), MB_ICONSTOP);
		dlg.DoModal();
	}
}

void CLicenseSettingsDlg::OnEnChangeEditKeyPassword()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CSubDlg::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	m_btnAuth.UpdateControl();
}

bool CLicenseSettingsDlg::RunInit()
{
	//
	if( m_serverAuth.test() )
	{
		//ciDEBUG(10, ("Already EnterpriseKey Authorized"));
		m_bIsAuthenticated = true;
	}
	else
	{
		//ciDEBUG(10, ("EnterpriseKey NOT Authorized"));
		m_bIsAuthenticated = false;
	}

	CString str_ip_addr, str_mac_addr;

	m_strEnterpriseKey = "test";

	m_serverAuth.get(KEY_CURRENT_IP_ADDRESS, str_ip_addr);
	m_serverAuth.get(KEY_CURRENT_MAC_ADDRESS, str_mac_addr);
	m_serverAuth.get(KEY_ENTERPRISE_KEY, m_strEnterpriseKey);
	//m_serverAuth.get("password", m_strpassword);

	if(m_strEnterpriseKey.GetLength() > 0)
		m_strPassword = "sqisoft";

//	ciDEBUG(10, ("CurrentIPAddress=%s", str_ip_addr));
//	ciDEBUG(10, ("CurrentMACAddress=%s", str_mac_addr));
//	ciDEBUG(10, ("EnterpriseKey=%s", m_strEnterpriseKey));
//	//ciDEBUG(10, ("Password=%s", m_strPassword));

	m_editIPAddr.SetWindowText(str_ip_addr);
	m_editMACAddr.SetWindowText(str_mac_addr);
	m_editEnterKey.SetWindowText(m_strEnterpriseKey);
	m_editPwd.SetWindowText(m_strPassword);

	return m_bIsAuthenticated;
}

void CLicenseSettingsDlg::UpdateControls()
{
	m_btnAuth.UpdateControl();
}

int CLicenseSettingsDlg::CheckInstallComplete()
{
	m_bInstallComplete = true;

	if( m_bIsAuthenticated == false )
	{
		m_bInstallComplete = false;
	}

	m_pSummaryDlg->UpdateControls();
	return m_bInstallComplete;
}

void CLicenseSettingsDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	static CPoint prev_pt;
	if( prev_pt != point )
	{
		m_nLButtonPush = 0;
		prev_pt = point;
	}

	CSubDlg::OnMouseMove(nFlags, point);
}

void CLicenseSettingsDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_editIPAddr.SetReadOnly(TRUE);
	m_editMACAddr.SetReadOnly(TRUE);
	m_btnBypassAuth.ShowWindow(SW_HIDE);

	if( ++m_nLButtonPush >= 5 )
	{
		CRect rect_info;
		CRect rect_settings;
		m_stcInfo.GetWindowRect(rect_info);
		m_stcSettings.GetWindowRect(rect_settings);
		ScreenToClient(rect_info);
		ScreenToClient(rect_settings);

		if( rect_info.PtInRect(point) )
		{
			m_editIPAddr.SetReadOnly(FALSE);
			m_editMACAddr.SetReadOnly(FALSE);
		}
		else if( rect_settings.PtInRect(point) )
		{
			m_btnBypassAuth.ShowWindow(SW_SHOW);
		}
	}

	CSubDlg::OnLButtonUp(nFlags, point);
}

void CLicenseSettingsDlg::OnBnClickedButtonAuthBypass()
{
	//ciDEBUG(10, ("OnBnClickedButtonAuthBypass()"));

	CSysAdminLoginDlg login_dlg;
	if( login_dlg.DoModal() != IDOK ) return;

	m_bOnceTimeForceAuth = true;
	OnBnClickedButtonAuth();
	m_bOnceTimeForceAuth = false;
}
