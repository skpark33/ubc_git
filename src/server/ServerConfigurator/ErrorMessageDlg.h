#pragma once
#include "afxwin.h"


#include "ReposControl2.h"


// CErrorMessageDlg 대화 상자입니다.

class CErrorMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CErrorMessageDlg)

public:
	CErrorMessageDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	CErrorMessageDlg(LPCTSTR lpszMessage, int nErrorCode, LPCTSTR lpszErrMsg, UINT nType);
	CErrorMessageDlg(UINT nMsgID, int nErrorCode, LPCTSTR lpszErrMsg, UINT nType);
	virtual ~CErrorMessageDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ERROR_MSG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CReposControl2	m_repos;

	HICON		m_hIco;
	UINT		m_nType;
	CString		m_strMessage;
	int			m_nErrorCode;
	CString		m_strErrorMessage;

public:
	CStatic	m_ico;
	CStatic	m_stcMessage;
	CEdit	m_editErrorCode;
	CEdit	m_editErrorMessage;
	CButton	m_btnClose;

	virtual BOOL OnInitDialog();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
