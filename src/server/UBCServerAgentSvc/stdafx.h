// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#include <stdio.h>
#include <tchar.h>
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include <afx.h>
#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.
#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <iostream>


#include <afxsock.h>		// MFC socket extensions
#include <afxtempl.h>
#include <afxmt.h>
#include <afxinet.h>
#include <afxtempl.h>


BOOL GetPrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, CString& strReturnedString, LPCTSTR lpFileName);
BOOL GetPrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, CString& strReturnedString, LPCTSTR lpDefault, LPCTSTR lpFileName);


#define		UBCPROCESS_COMMAND_INIT			_T("INIT")
#define		UBCPROCESS_COMMAND_STARTING		_T("Starting")
#define		UBCPROCESS_COMMAND_STARTED		_T("Started")
#define		UBCPROCESS_COMMAND_STOPPING		_T("Stopping")
#define		UBCPROCESS_COMMAND_STOPPED		_T("Stopped")

#define		UBCPROCESS_STATUS_ACTIVE		_T("Active")
#define		UBCPROCESS_STATUS_INACTIVE		_T("Inactive")
#define		UBCPROCESS_STATUS_OVER_MEM		_T("Over Mem.")
#define		UBCPROCESS_STATUS_OVER_HANDLE	_T("Over Handle")

#define		UBCSERVER_STATUS_NORMAL			_T("Normal")
#define		UBCSERVER_STATUS_NO_STARTER		_T("NoStarter")
#define		UBCSERVER_STATUS_NO_AGENT		_T("NoAgent")
#define		UBCSERVER_STATUS_NO_SESSION		_T("NoSession")

class CLockObject
{
public:
	CLockObject(CCriticalSection& lock) : m_cs(&lock), m_mutex(NULL), m_bAlreadyUnlock(false) { m_cs->Lock(); };
	CLockObject(CMutex& lock) : m_cs(NULL), m_mutex(&lock), m_bAlreadyUnlock(false) { m_mutex->Lock(); };

	~CLockObject() { Unlock(); };

	void Unlock()
	{
		if(!m_bAlreadyUnlock)
		{
			if(m_mutex) m_mutex->Unlock();
			if(m_cs) m_cs->Unlock();
		}
		m_bAlreadyUnlock = true;
	};

protected:
	CMutex*				m_mutex;
	CCriticalSection*	m_cs;
	bool				m_bAlreadyUnlock;
};

#define		LOCK(x)		CLockObject __lock__(x);
