#include "stdafx.h"

#include "UBCServerAgentSvc.h"

#include <windows.h>
#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>
#include <conio.h>

#include <tlhelp32.h>
//#include <iostream>
//#include <string>
#include "psapi.h"
#pragma comment(lib, "Psapi.lib")

#include <Userenv.h>
#pragma comment(lib, "Userenv.lib")

//#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")

#include "ServiceUtil.h"
#include "ServiceMgr.h"

#include "HttpRequest.h"

BOOL RunCmd(LPCTSTR szCmd);
DWORD WINAPI service_handler(DWORD fdwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
{
	CServiceManager manager;

	switch (fdwControl) 
	{ 
	case SERVICE_CONTROL_PAUSE:
		manager.SetServiceRunStatus(SERVICE_PAUSE_PENDING);
		// 서비스를 일시 중지 시킨다.
		manager.SetServiceRunStatus(SERVICE_PAUSED);
		break;

	case SERVICE_CONTROL_CONTINUE:
		manager.SetServiceRunStatus(SERVICE_CONTINUE_PENDING);
		// 일시 중지 시킨 서비스를 재개한다.
		manager.SetServiceRunStatus(SERVICE_RUNNING);
		break;

	case SERVICE_CONTROL_STOP:
		manager.SetServiceRunStatus(SERVICE_STOP_PENDING);
		// 서비스를 멈춘다 (즉, 종료와 같은 의미)
		// 서비스를 종료하면, service_main 는 절대로 리턴하지 않는다.
		// 그러므로 해제하려면 작업이 있으면 모든것을 이곳에서 처리한다.
		manager.SetServiceRunStatus(SERVICE_STOPPED);
		break;

	default:
		break;
	}

	return NO_ERROR;
}

CString g_strExePath = "";
CString g_strINIPath = "";

int service_main(INT ARGC, LPSTR* ARGV)
{
	TCHAR str[MAX_PATH];
	::ZeroMemory(str, MAX_PATH);
	::GetModuleFileName(NULL, str, MAX_PATH);
	int length = _tcslen(str) - 1;
	while( (length > 0) && (str[length] != _T('\\')) )
		str[length--] = 0;
	g_strExePath = str;
	g_strINIPath = g_strExePath + ".\\UBCServerAgent.ini";

	CServiceManager manager;
	CServiceUtility utility;
	if(utility.IsServiceMode() == TRUE)
	{
		manager.SetServiceHandler(RegisterServiceCtrlHandlerEx(CServiceManager::m_InstallParam.lpServiceName, service_handler, NULL));
		if (manager.GetServiceHandler() == NULL) 
			return 0;
	}
	else
	{
		printf("run debug mode\npress any key close...\n");
	}

	manager.SetServiceRunStatus(SERVICE_START_PENDING);

	// bla bla initialize...
	Sleep(1000);

	// start ok, i'm ready receive event
	manager.SetServiceRunStatus(SERVICE_RUNNING);

	/////////////////////////////////////////////////////////////////////
	// test code
	/////////////////////////////////////////////////////////////////////
	//char szCmd[4096] = {0};
	//sprintf(szCmd, "cmd /c %s", "notepad");
	//if(!RunCmd(szCmd))
	//{
	//	::WinExec(szCmd, SW_SHOW);
	//}

	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	int count = 0;
	while(manager.GetServiceRunStatus() != SERVICE_STOPPED)
	{
		if(manager.GetServiceRunStatus() == SERVICE_PAUSED)
		{
			printf("SERVICE_PAUSED\n");
			Sleep(1000);
			continue;
		}

		if(utility.IsServiceMode() == FALSE && _kbhit())
		{
			printf("stop debug mode\n");
			break;
		}

		printf("while(%d)", count++);
		if(count < 60)
		{
			Sleep(1000);
			continue;
		}
		count = 0;

		//::MessageBeep(0xFFFFFFFF);
		//printf("ting...\n");
		//Sleep(1000);

		CWinThread* pThread = ::AfxBeginThread(SendStatusThread, NULL, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		if(pThread)
		{
			pThread->m_bAutoDelete = TRUE;
			pThread->ResumeThread();
		}
	}

	CoUninitialize();

	return 0;
}

int main(int argc, char** argv)
{
#if 0
	/////////////////////////////////////////////////////////////////////
	// test code
	/////////////////////////////////////////////////////////////////////

	CServiceUtility u;
	u.ServiceUserBlankPassword(FALSE);

	u.UserPrivileges("Administrtor", L"SeServiceLogonRight");
	u.ProcessPrivileges(GetCurrentProcess(), SE_SHUTDOWN_NAME, TRUE);

	PWTS_PROCESS_INFO sinfo = NULL;
	DWORD count = 0;
	u.WTSEnumProcesses(sinfo, &count);
	u.WTSFree(sinfo);

	PWTS_SESSION_INFO info = NULL;
	count = 0;
	u.WTSEnumSessions(info, &count);
	u.WTSFree(info);

	CServiceUtility::CWTSSession ws(WTS_CURRENT_SERVER_HANDLE, 0);

	char buffer[512] = {0};
	if(u.GetOSDisplayString(buffer))
		printf("%s\n", buffer);

	CServiceUtility su;
	STARTUPINFO si = {0};
	PROCESS_INFORMATION pi = {0};
	u.CreateProcessToDesktop("C:\\Windows\\System32\\cmd.exe", NULL, si, pi, 0);
	WaitForSingleObject(pi.hProcess, INFINITE);

#endif

	CServiceManager manager;
	CServiceUtility utility;

	/* 설정하지 않으면, 기본 파일값으로 
	파일 이름을 잘라다가 자동으로 설정한다. */
	manager.ConfigServiceName("UBCServerAgentSvc");
	manager.ConfigServiceDisp("UBC Server Agent Service");
	manager.ConfigServiceDesc("UBC Server Agent Service");

	if(utility.IsServiceMode() == FALSE)
	{
		char ch = 0;
		if(argc != 2)
		{
			printf("UBCServerAgentSvc.exe [option: a, i, u, s, t, p, c]\n");
			printf("  a - install and start\n");
			printf("  i - install\n");
			printf("  u - uninstall\n");
			printf("  s - start\n");
			printf("  t - stop\n");
			printf("  p - pause\n");
			printf("  c - continue\n\n");

			printf("input command: ");
			ch = getch();
		}
		else
			ch = argv[1][0];

		switch(ch)
		{
		case 'a':
			manager.Install();
			manager.Start();
			return 0;
		case 'i':
			manager.Install();
			return 0;
		case 'u':
			manager.Uninstall();
			return 0;
		case 's':
			manager.Start();
			return 0;
		case 't':
			manager.Stop();
			return 0;
		case 'p':
			manager.Pause();
			return 0;
		case 'c':
			manager.Continue();
			return 0;

		default:
			return service_main(argc, argv);
		}
	}

	SERVICE_TABLE_ENTRY STE[] =
	{
		{(char*)manager.m_InstallParam.lpServiceName, (LPSERVICE_MAIN_FUNCTION)service_main},
		{NULL,NULL}
	};

	if(StartServiceCtrlDispatcher(STE) == FALSE)
		return -1;

	return 0;
}

BOOL GetVersion(CString& version)
{
	CString release_file = g_strExePath + "..\\ReleaseInfo\\release.txt";

	FILE* fVer = fopen(release_file, "r");
	if (!fVer) {
		// version.txt is not found
		return false;
	}
	if(feof(fVer)) {
		// version.txt is empty
		fclose(fVer);
		return false;
	}
	char buffer[1024] = {0};
	fgets(buffer, 1023, fVer);
	buffer[5]=0; //version 은 4자를 넘지 않는다. x.xx
	version = buffer+1;
	fclose(fVer);
	return true;
}

#define		CONSOLE_OUTPUT_BUFFER		(1024 * 1024)		// cmd-output buffer size = 1MB

UINT SendStatusThread(LPVOID pParam)
{
	// create pipe
	HANDLE hRead=NULL, hWrite=NULL;
	if( !CreatePipe(&hRead, &hWrite, 0, CONSOLE_OUTPUT_BUFFER) )
	{
		if(hRead) CloseHandle(hRead);
		if(hWrite) CloseHandle(hWrite);
		return FALSE;
	}
	if( !SetHandleInformation(hWrite, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT) )
	{
		if(hRead) CloseHandle(hRead);
		if(hWrite) CloseHandle(hWrite);
		return FALSE; // 쓰기 핸들은 상속 가능
	}

	// 
	PROCESS_INFORMATION pi = {0};
	STARTUPINFO si = {0};
	si.cb = sizeof(si);
	si.hStdOutput = hWrite;
	si.dwFlags = STARTF_USESTDHANDLES;

	TCHAR wmic_cmd[] = _T("cmd /c wmic process get caption, creationdate, WorkingSetSize, HandleCount");
	if( !CreateProcess(0, wmic_cmd, 0, 0, TRUE, CREATE_NO_WINDOW|CREATE_SUSPENDED, 0, 0, &si, &pi) )
	{
		if(hRead) CloseHandle(hRead);
		if(hWrite) CloseHandle(hWrite);
		if(pi.hProcess) CloseHandle(pi.hProcess);
		if(pi.hThread) CloseHandle(pi.hThread);
		return FALSE;
	}
	::ResumeThread(pi.hThread);

	// wait until end of process, and 5-seconds
	int wait_count = 0;
	DWORD exit_code = STILL_ACTIVE;
	while(exit_code == STILL_ACTIVE && wait_count<5000)
	{
		::GetExitCodeProcess(pi.hProcess, &exit_code);
		::Sleep(1);
		wait_count++;
	}

	// get output
	DWORD read = 0;
	TCHAR* buf = new TCHAR[CONSOLE_OUTPUT_BUFFER];
	::ZeroMemory(buf, sizeof(TCHAR)*CONSOLE_OUTPUT_BUFFER);

	ReadFile(hRead, buf, CONSOLE_OUTPUT_BUFFER-1, &read, NULL);
	buf[read] = 0;
	CString str_buffer = buf;
	delete[]buf;
	if(hRead) CloseHandle(hRead);
	if(hWrite) CloseHandle(hWrite);
	if(pi.hProcess) CloseHandle(pi.hProcess);
	if(pi.hThread) CloseHandle(pi.hThread);

	//HWND hwnd_starter = ::FindWindow(NULL, _T("UTV Starter"));
	//HWND hwnd_agent   = ::FindWindow(NULL, _T("UBCServerAgent"));
	HWND hwnd_starter = str_buffer.Find(_T("UTV_starter.exe")) < 0 ? NULL : (HWND)1;
	HWND hwnd_agent   = str_buffer.Find(_T("UBCServerAgent.exe")) < 0 ? NULL : (HWND)1;

	//
	CString str_status = "";
	if(hwnd_starter==NULL && hwnd_agent==NULL)
	{
		// No starter & No agent ==> NoSession
		str_status = UBCSERVER_STATUS_NO_SESSION;
	}
	else if(hwnd_starter!=NULL && hwnd_agent==NULL)
	{
		// No agent ==> NoAgent
		str_status = UBCSERVER_STATUS_NO_AGENT;
	}
	else if(hwnd_starter==NULL && hwnd_agent!=NULL)
	{
		// No starter ==> NoStarter
		str_status = UBCSERVER_STATUS_NO_STARTER;
	}
	else //if(hwnd_starter!=NULL && hwnd_agent!=NULL)
	{
		// normal
		str_status = UBCSERVER_STATUS_NORMAL;
	}

	// read Customer & ServerId & ServerIp
	CString str_customer = "", str_serverid = "", str_serverip = "";
	GetPrivateProfileString(_T("UBCServerAgent"), _T("Customer"),	str_customer,	g_strINIPath);//_T("UBCServerAgent.ini"));
	GetPrivateProfileString(_T("UBCServerAgent"), _T("ServerId"),	str_serverid,	g_strINIPath);//_T("UBCServerAgent.ini"));
	GetPrivateProfileString(_T("UBCServerAgent"), _T("ServerIp"),	str_serverip,	g_strINIPath);//_T("UBCServerAgent.ini"));

	// read startup-time of UBCServerAgent
	CString str_startuptime = "1970-01-01 00:00:00";
	GetPrivateProfileString(_T("UBCServerAgent"), _T("START_TIME"),	str_startuptime, g_strINIPath);//_T("UBCServerAgent.ini"));

	// get version
	CString str_ver = "";
	GetVersion(str_ver);

	// make url
	CString url = _T("UBC_Server/set_server_process_status.asp");

	// make send-message
	CString send_msg;
	send_msg.Format(_T("customer=%s&serverId=%s&appId=%s&serverIp=%s&binaryName=%s&argument=%s&properties=%s&debug=%s&status=%s&startUpTime=%s"), 
			CHttpRequest::ToEncodingString( str_customer ),
			CHttpRequest::ToEncodingString( str_serverid ),
			"UBCServerAgent",
			CHttpRequest::ToEncodingString( str_serverip ),
			"UBCServerAgent.exe",
			CHttpRequest::ToEncodingString( str_ver ),
			"",
			"",
			CHttpRequest::ToEncodingString( str_status ),
			CHttpRequest::ToEncodingString( str_startuptime )
		);

	//
	CString out_msg;
	CHttpRequest http_request;
	if( !http_request.RequestPost(url, send_msg, out_msg) )
	{
		//__WARN__( (_T("Connect fail (thread_id:%d) URL=%s"), thread_id, http_request.GetRequestURL()) );
		//__WARN__( (_T("Connect fail (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );
		//__WARN__( (_T("Connect fail (thread_id:%d) ErrMsg=%s"), thread_id, http_request.GetErrorMsg()) );

		//CString detail_info;
		//detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s\r\nError Message : %s"),
		//					thread_id, http_request.GetRequestURL(), send_msg, http_request.GetErrorMsg());

		//POST_SYSTEM_LOG(parent_wnd, _T("Warn : Fail to init status"), detail_info);
		CString str_ip;
		UINT n_port;
		http_request.GetServerInfo(str_ip, n_port);
		printf("FAIL=(%s:%d,%s,%s)", str_ip, n_port, http_request.GetRequestURL(), http_request.GetErrorMsg());
		return FALSE;
	}

	if(out_msg != _T("OK"))
	{
		printf("FAIL=%s.", out_msg);
		//__WARN__( (_T("Invalid result (thread_id:%d) URL=%s"), thread_id, http_request.GetRequestURL()) );
		//__WARN__( (_T("Invalid result (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );
		//__WARN__( (_T("Invalid result (thread_id:%d) out_msg=%s"), thread_id, out_msg) );

		//// invalid html
		//POST_COMPLETE_INIT_UBC_PROCESS_STATUS(parent_wnd, thread_id);

		//CString detail_info;
		//detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s\r\nResult : \r\n"),
		//					thread_id, http_request.GetRequestURL(), send_msg );
		//detail_info += out_msg;

		//POST_SYSTEM_LOG(parent_wnd, _T("Warn : Invalid result in init status"), detail_info);
		return FALSE;
	}

	CString str_ip;
	UINT n_port;
	http_request.GetServerInfo(str_ip, n_port);
	printf("SUCCESS=(%s:%d,%s,%s,%s)", str_ip, n_port, http_request.GetRequestURL(), http_request.GetErrorMsg(), send_msg);
	printf("SUCCESS=%s", out_msg);

	return TRUE;
}
