#ifndef _hsrAdapter_h_
#define _hsrAdapter_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciMutex.h>
#include "hsrAcceptor.h"

class hsrAdapter
{
	public:
		static hsrAdapter* getInstance();
		virtual ~hsrAdapter();

		ciBoolean	open(ciString& pIpAddress, ciInt pPort);
		void		close();

	protected:
		hsrAdapter();

		static hsrAdapter *	_instance;
		static ciMutex		_mutex;

		hsrAcceptor *		_acceptor;
};

#endif // _hsrAdapter_h_
