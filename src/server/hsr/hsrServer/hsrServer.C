#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>

#ifdef _COP_UTV_
#	include "ci/libConfig/ciEnv.h"
#	include "ci/libDebug/ciArgParser.h"
#endif

#include "hsrWorld.h"


ciSET_DEBUG(10, "hsrServer");


int main(int argc, char* argv[])
{
	//ciDebug::setDebugOn(0);
#ifdef _COP_UTV_
	ciArgParser::initialize(argc, argv);
	ciEnv::defaultEnv();
#endif

	hsrWorld* world = new hsrWorld();

    if(!world) {
        cerr<<"Unable to create ciWorld"<<endl;
        exit(1);
    }

    if(world->init(argc, argv) == ciFalse) {
        cerr<<"Init world error"<<endl;
        world->fini(2);
    }

    world->run();

    world->fini(0);
    return 0;
}
