#ifndef _hsrSvcHandler_h_
#define _hsrSvcHandler_h_

#include "ci/libBase/ciListType.h"
#include "ci/libSocket/ciSockSvcHandler.h"

class hsrSvcHandler : public ciSockSvcHandler
{
public:
	hsrSvcHandler();
	hsrSvcHandler(const char* pName);
	virtual ~hsrSvcHandler();

	virtual int open(void*);
	virtual int close(u_long p_val);
	virtual int svc();
	virtual int handle_close(ACE_HANDLE pHandle = ACE_INVALID_HANDLE,
								ACE_Reactor_Mask pMask = ACE_Event_Handler::ALL_EVENTS_MASK);

protected:
	ciBoolean _rsh(ciString& buf_cmd, ciString& strOutMsg);
	typedef ciSockSvcHandler inherited;
};

#endif // _spsSvcHandler_h_
