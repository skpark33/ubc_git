#ifndef _hsrAcceptor_h_
#define _hsrAcceptor_h_

#include <ci/libSocket/ciSockAcceptor.h>

class hsrAcceptor : public ciSockAcceptor
{
	public:
		hsrAcceptor();
		virtual ~hsrAcceptor();
		virtual int make_svc_handler(ciSockSvcHandler*& pSvcHandler);
		//virtual int activate_svc_handler(ciSockSvcHandler* pSvcHandler);
};

#endif // _hsrAcceptor_h_
