/*! \class hsrTimer
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _hsrTimer_h_
#define _hsrTimer_h_

#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libBase/ciTime.h>
#include <ci/libTimer/ciTimer.h>

class hsrTimer :  public  ciPollable {
public:
	static hsrTimer*	getInstance();
	static void	clearInstance();

	hsrTimer();
	virtual ~hsrTimer();

	virtual void processExpired(ciString name, int counter, int interval);

protected:
	static hsrTimer*	_instance;
	static ciMutex			_instanceLock;

    void setCleanTime(const char* cleanTime, const char* cleanWeekDay, ciShort cleanMinGap);
    ciBoolean itsCleanTime(ciTime& now);

	ciBoolean	_isMonitor;
    ciShort     _cleanHour;
    ciShort     _cleanMin;
    ciShort     _cleanMinGap;
    ciIntSet    _cleanWeekDay;

};	

#endif //_hsrTimer_h_
