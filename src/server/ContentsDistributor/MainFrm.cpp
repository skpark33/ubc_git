// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "ContentsDistributor.h"

#include "MainFrm.h"
#include "ContentsDistributorDoc.h"
#include "ContentsDistributorView.h"

#include "../libContentsDistributor/CDEventHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWndEx)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWndEx)
	ON_WM_CREATE()
	ON_WM_GETMINMAXINFO()
	ON_WM_SYSCOMMAND()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_MESSAGE(WM_ADD_CONTENTS, OnAddContents)
	ON_MESSAGE(WM_CHANGE_CONTENTS_STATUS, OnChangeContentsStatus)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
{
	// TODO: 여기에 멤버 초기화 코드를 추가합니다.
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | /*WS_VISIBLE |*/ CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("도구 모음을 만들지 못했습니다.\n");
		return -1;      // 만들지 못했습니다.
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("상태 표시줄을 만들지 못했습니다.\n");
		return -1;      // 만들지 못했습니다.
	}

	// TODO: 도구 모음을 도킹할 수 없게 하려면 이 세 줄을 삭제하십시오.
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	CreateTrayIcon(this, _T("Contents Distributor"), IDR_MAINFRAME, IDR_MAINFRAME/*IDB_TRAY_ICON*/);

	SetUseMagnetEffect();

	SetWindowText(_T("Contents Distributor"));

#ifndef DEBUG
	SetTimer(1026, 1, NULL);
#endif

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWndEx::PreCreateWindow(cs) )
		return FALSE;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	cs.style &= (~(FWS_ADDTOTITLE | WS_MAXIMIZEBOX | WS_VISIBLE));

	return TRUE;
}


// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWndEx::Dump(dc);
}

#endif //_DEBUG


// CMainFrame 메시지 처리기


void CMainFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CFrameWndEx::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMinTrackSize.x = 520;
	lpMMI->ptMinTrackSize.y = 240;
}

void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(nID == SC_CLOSE)
	{
		ShowWindow(SW_HIDE);
		//int ret_val = AfxMessageBox("Exit Program ?", MB_ICONWARNING | MB_YESNO);
		//if(ret_val == IDNO)
			return;
	}

	CFrameWndEx::OnSysCommand(nID, lParam);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CFrameWndEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(nType == SIZE_MINIMIZED)
	{
		ShowWindow(SW_HIDE);
		return;
	}
}

void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CFrameWndEx::OnTimer(nIDEvent);

	if(nIDEvent == 1026)
	{
		KillTimer(1026);
		ShowWindow(SW_HIDE);
	}
}

LRESULT CMainFrame::OnAddContents(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->PostMessage(WM_ADD_CONTENTS, wParam, lParam);
}

LRESULT CMainFrame::OnChangeContentsStatus(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->PostMessage(WM_CHANGE_CONTENTS_STATUS, wParam, lParam);
}
