// PublicContentsTrigger.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CPublicContentsTriggerApp:
// See PublicContentsTrigger.cpp for the implementation of this class
//

class CPublicContentsTriggerApp : public CWinApp
{
public:
	CPublicContentsTriggerApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CPublicContentsTriggerApp theApp;


typedef struct
{
	char source_path[1024];
	char target_path[1024];
} PUBLIC_CONTENTS_PATH;
