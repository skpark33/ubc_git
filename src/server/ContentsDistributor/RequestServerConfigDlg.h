#pragma once
#include "afxwin.h"


// CRequestServerConfigDlg 대화 상자입니다.

class CRequestServerConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CRequestServerConfigDlg)

public:
	CRequestServerConfigDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRequestServerConfigDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_REQUEST_SERVER_CONFIG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CString m_strIniPath;

public:
	CEdit m_editIP;
	CEdit m_editPort;
};
