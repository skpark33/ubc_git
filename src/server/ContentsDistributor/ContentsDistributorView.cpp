// ContentsDistributorView.cpp : CContentsDistributorView 클래스의 구현
//

#include "stdafx.h"
#include "ContentsDistributor.h"

#include "ContentsDistributorDoc.h"
#include "ContentsDistributorView.h"

#include "SetSyncTimeDlg.h"
#include "CleanEmptyFolderDlg.h"
#include "RequestServerConfigDlg.h"
#include "BrowseForFolder.h"
#include "Lib.h"

#include "../libContentsDistributor/CDEventHandler.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifndef FOF_NO_UI
	#define	FOF_NO_UI		(FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_NOCONFIRMMKDIR)
#endif

#define		TIMER_LOG_FILE_CHANGE			1024
#define		TIMER_LOG_FILE_CHANGE_TIME		(1000 * 60) // every 1 min


#define		TIMER_CONTENTS_CHANGE			1025
#define		TIMER_CONTENTS_CHANGE_TIME		(1000 * 60 * 30) // 30 min

#define		TIMER_CONTENTS_TERM_CHECK		1026
#define		TIMER_CONTENTS_TERM_CHECK_TIME	1000	// every 1 sec

#define		TIMER_PUBLICCONTENTS_TERM_CHECK			1027
#define		TIMER_PUBLICCONTENTS_TERM_CHECK_TIME	1000	// every 1 sec


#define		WM_START_PUBLICCONTENTS_COPY	(WM_USER + 2025)	// wParam=source(CString*, delete after), lParam=target(CString*, delete after)
#define		WM_STOP_PUBLICCONTENTS_COPY		(WM_USER + 2026)	// wParam=status(0:no_err, etc:error), lParam=nothing


#define		PUBLICCONTENTS_COPY_NOERROR				0
#define		PUBLICCONTENTS_COPY_NOT_EXIST_SOURCE	1
#define		PUBLICCONTENTS_COPY_COPY_ERROR			2


ciSET_DEBUG(10, "CContentsDistributorView");


CPublicContentsInfo CContentsDistributorView::m_PublicContentsInfo;

CWinThread* CContentsDistributorView::m_pPublicContentsThread = NULL;
bool CContentsDistributorView::m_bPublicContentsThread = false;


// CContentsDistributorView

IMPLEMENT_DYNCREATE(CContentsDistributorView, CFormView)

BEGIN_MESSAGE_MAP(CContentsDistributorView, CFormView)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_COMMAND(ID_SYNC_CONTENTS_NOW, &CContentsDistributorView::OnSyncContentsNow)
	ON_COMMAND(ID_SET_SYNC_TIME, &CContentsDistributorView::OnSetSyncTime)
	ON_BN_CLICKED(IDC_BUTTON_CHANGE_SOURCE_PATH, OnBnClickedButtonChangeSourcePath)
	ON_BN_CLICKED(IDC_BUTTON_CHANGE_TARGET_PATH, OnBnClickedButtonChangeTargetPath)
	ON_COMMAND(ID_SETTING_LOCKFOLDER, &CContentsDistributorView::OnSettingLockfolder)
	ON_MESSAGE(WM_ADD_CONTENTS, OnAddContents)
	ON_MESSAGE(WM_CHANGE_CONTENTS_STATUS, OnChangeContentsStatus)
	ON_MESSAGE(WM_START_SYNC_CONTENTS, OnStartStopSyncContents)
	ON_MESSAGE(WM_STOP_SYNC_CONTENTS, OnStartStopSyncContents)
	ON_MESSAGE(WM_CONTENTS_CHANGE, OnContentsChange)
	ON_MESSAGE(WM_CONTENTS_MONITOR_FAULT, OnContentsMonitorFault)
	ON_MESSAGE(WM_START_PUBLICCONTENTS_COPY, OnStartPublicContentsCopy)
	ON_MESSAGE(WM_STOP_PUBLICCONTENTS_COPY, OnStopPublicContentsCopy)
	ON_COMMAND(ID_CLEAN_EMPTY_FOLDER, &CContentsDistributorView::OnCleanEmptyFolder)
	ON_COMMAND(ID_REQUEST_SERVER_CONFIG, &CContentsDistributorView::OnRequestServerConfig)
END_MESSAGE_MAP()

// CContentsDistributorView 생성/소멸

CContentsDistributorView::CContentsDistributorView()
	: CFormView(CContentsDistributorView::IDD)
{
	// TODO: 여기에 생성 코드를 추가합니다.

	m_bDistributing = false;
	m_nLineCount = 0;
	m_bTodaySync = false;
}

CContentsDistributorView::~CContentsDistributorView()
{
}

void CContentsDistributorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_lcLog);
	DDX_Control(pDX, IDC_EDIT_SOURCE_PATH, m_editSourcePath);
	DDX_Control(pDX, IDC_EDIT_TARGET_PATH, m_editTargetPath);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_stcStatus);
}

BOOL CContentsDistributorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void WriteImageFileFromResource(DWORD nResID, LPCTSTR lpszFilename)
{
	HRSRC pRes = FindResource(NULL,(LPTSTR)(nResID),_T("PNG"));
	if(pRes == NULL) return;
    DWORD image_size = SizeofResource(NULL, pRes);
	if(image_size == NULL) return;
	HANDLE hRes = LoadResource(NULL,(struct HRSRC__ *)pRes);
	if(hRes == NULL) return;
	LPSTR lpRes = (char *)LockResource(hRes);
	if(lpRes == NULL) return;

	CFile file;
	if(file.Open(lpszFilename, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
	{
		LPBYTE pBuf = new BYTE[image_size];
		memcpy(pBuf, lpRes, image_size);

		file.Write(pBuf, image_size);
		file.Close();

		delete[]pBuf;
	}

	UnlockResource(hRes);
	FreeResource(hRes);
}

void CContentsDistributorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	// write image file from resource
	if(::_taccess("Play.png", 00) != 0) WriteImageFileFromResource(IDB_PNG_PLAY, "Play.png");
	if(::_taccess("Stop.png", 00) != 0) WriteImageFileFromResource(IDB_PNG_STOP, "Stop.png");

	// loading image file
	m_imgPlay.Load("Play.png");
	m_imgStop.Load("Stop.png");

	//
	TCHAR buf[MAX_PATH+1] = {0};
	::GetPrivateProfileString("Settings", "SourcePath", "", buf, MAX_PATH, ::GetINIPath());
	m_editSourcePath.SetWindowText(buf);

	::GetPrivateProfileString("Settings", "TargetPath", "", buf, MAX_PATH, ::GetINIPath());
	m_editTargetPath.SetWindowText(buf);

	TCHAR sz_def_folder[MAX_PATH+1] = "C:\\Project\\ubc\\web\\UBC_DEFAULT\\UBC_PublicContents\\";
	LPCSTR path = ciPath::getInstance()->getPath("");
	sz_def_folder[0] = path[0];

	::GetPrivateProfileString("Settings", "LockFolder", sz_def_folder, buf, MAX_PATH, ::GetINIPath());
	m_strPublicContentsLockFolder = buf;
	if(::_access(m_strPublicContentsLockFolder, 0) != 0)
	{
		OnSettingLockfolder();
	}

	//
	ResetSyncTIme();

	//
	m_lcLog.SetExtendedStyle(m_lcLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcLog.InsertColumn(COL_COUNT, "Count", 0, 0);
	m_lcLog.InsertColumn(COL_TIME, "Time", 0, 120);
	m_lcLog.InsertColumn(COL_CONTENTS_ID, "Contents ID", 0, 240);
	m_lcLog.InsertColumn(COL_STATUS, "Status",LVCFMT_CENTER , 120);
	m_lcLog.InsertColumn(COL_RESULT, "Result List", 0, 200);

	//
	InitPublicContentsThread();

	//
	CMutexGuard aGuard(CDEventHandler::_contentsSyncLock);
	CDEventHandler::_hwndMainWnd = GetSafeHwnd();

	//
	SetTimer(TIMER_LOG_FILE_CHANGE, TIMER_LOG_FILE_CHANGE_TIME, NULL);
	SetTimer(TIMER_CONTENTS_TERM_CHECK, TIMER_CONTENTS_TERM_CHECK_TIME, NULL);
	SetTimer(TIMER_PUBLICCONTENTS_TERM_CHECK, TIMER_PUBLICCONTENTS_TERM_CHECK_TIME, NULL);

#ifdef DEBUG
	//debug test
	CDEventHandler::_initContentsSyncThread();
	CDEventHandler::_initContentsMonitorThread();
	CDEventHandler::_initCachingContentsThread();
#endif

}

// CContentsDistributorView 진단

#ifdef _DEBUG
void CContentsDistributorView::AssertValid() const
{
	CFormView::AssertValid();
}

void CContentsDistributorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CContentsDistributorDoc* CContentsDistributorView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CContentsDistributorDoc)));
	return (CContentsDistributorDoc*)m_pDocument;
}
#endif //_DEBUG


// CContentsDistributorView 메시지 처리기

void CContentsDistributorView::OnDestroy()
{
	CDEventHandler::_clearContentsSyncThread();
	CDEventHandler::_clearContentsMonitorThread();
	CDEventHandler::_clearCachingContentsThread();
	DeletePublicContentsThread();

	CFormView::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CContentsDistributorView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CFormView::OnPaint()을(를) 호출하지 마십시오.

	if(!m_imgPlay.IsNull())
	{
		CRect rect;
		m_stcStatus.GetWindowRect(rect);
		ScreenToClient(rect);

		if(m_bDistributing)
			m_imgPlay.Draw(dc.GetSafeHdc(), rect.left+2, rect.top+2, m_imgPlay.GetWidth(), m_imgPlay.GetHeight());
		else
			m_imgStop.Draw(dc.GetSafeHdc(), rect.left+2, rect.top+2, m_imgPlay.GetWidth(), m_imgPlay.GetHeight());
	}
}

void CContentsDistributorView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(GetSafeHwnd() && m_lcLog.GetSafeHwnd())
	{
		m_lcLog.MoveWindow(0, 83, cx, cy-83);
	}
}

void CContentsDistributorView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CFormView::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case TIMER_LOG_FILE_CHANGE:
		KillTimer(TIMER_LOG_FILE_CHANGE);

		CContentsDistributorApp::CheckLogFileDate();

		SetTimer(TIMER_LOG_FILE_CHANGE, TIMER_LOG_FILE_CHANGE_TIME, NULL);
		break;

	case TIMER_CONTENTS_CHANGE:
		{
			// contents-file-update but no event (1-hour)
			KillTimer(TIMER_CONTENTS_CHANGE);

			cciEvent null_event;
			CDEventHandler::_processSyncContents(null_event);
		}
		break;

	case TIMER_CONTENTS_TERM_CHECK:
		{
			// term-check contents
			KillTimer(TIMER_CONTENTS_TERM_CHECK);

			CTime cur_time = CTime::GetCurrentTime();

			if(m_bTodaySync == false && 
				m_nSetSyncTime && 
				cur_time.GetHour() == m_nSyncHour && 
				cur_time.GetMinute() == m_nSyncMinute )
			{
				m_bTodaySync = true;
				cciEvent null_event;
				CDEventHandler::_processSyncContents(null_event);
			}
			else
			{
				if(cur_time.GetHour() != m_nSyncHour || cur_time.GetMinute() != m_nSyncMinute )
					m_bTodaySync = false;

				SetTimer(TIMER_CONTENTS_TERM_CHECK, TIMER_CONTENTS_TERM_CHECK_TIME, NULL);
			}
		}
		break;

	case TIMER_PUBLICCONTENTS_TERM_CHECK:
		CheckPublicContents();
		break;
	}
}

void CContentsDistributorView::CheckPublicContents()
{
	CString lock_path = m_strPublicContentsLockFolder;
	if(lock_path.Right(1) != '\\') lock_path += '\\';
	lock_path += DEFAULT_LOCK_FILE;

	CPublicContentsInfo info;
	info.LoadAndClear(lock_path);

	CString source, target;
	while(info.GetFront(source, target))
	{
		ciDEBUG(5,("source=%s", source));
		ciDEBUG(5,("target=%s", target));

		CContentsDistributorView::m_PublicContentsInfo.PushBack(source, target, false);
		info.PopFront();
	}
	//CContentsDistributorView::m_PublicContentsInfo.Save();
}

void CContentsDistributorView::OnSyncContentsNow()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CDEventHandler::_refreshContentsSync = true;
}

void CContentsDistributorView::OnSetSyncTime()
{
	CSetSyncTimeDlg dlg;
	dlg.DoModal();

	ResetSyncTIme();
}

void CContentsDistributorView::OnBnClickedButtonChangeSourcePath()
{
	CString source_path;
	m_editSourcePath.GetWindowText(source_path);

	CBrowseForFolder bff(GetSafeHwnd(), "Select Source Path", source_path);
	if(bff.DoModal())
	{
		::WritePrivateProfileString("Settings", "SourcePath", bff.GetSelectFolder(), ::GetINIPath());
		m_editSourcePath.SetWindowText(bff.GetSelectFolder());

		::AfxMessageBox("Please exit and run this program for applying the change.", MB_ICONWARNING);
	}
}

void CContentsDistributorView::OnBnClickedButtonChangeTargetPath()
{
	CString target_path;
	m_editTargetPath.GetWindowText(target_path);

	CBrowseForFolder bff(GetSafeHwnd(), "Select Target Path", target_path);
	if(bff.DoModal())
	{
		::WritePrivateProfileString("Settings", "TargetPath", bff.GetSelectFolder(), ::GetINIPath());
		m_editTargetPath.SetWindowText(bff.GetSelectFolder());

		::AfxMessageBox("Please exit and run this program for applying the change.", MB_ICONWARNING);
	}
}

int CALLBACK MyBrowseCallbackProc2(
	HWND hwnd,	
	UINT uMsg,	
	LPARAM lParam,	
	LPARAM lpData	
	)
{
	switch (uMsg) {
	case BFFM_INITIALIZED:
		::SendMessage( hwnd, BFFM_SETSELECTION, TRUE, lpData );
		break;
		}
	return 0;
}

void CContentsDistributorView::OnSettingLockfolder()
{
	TCHAR current_path[MAX_PATH+1];
	TCHAR new_path[MAX_PATH+1];

	_tcscpy(current_path, m_strPublicContentsLockFolder);

	BROWSEINFO browse;
	browse.hwndOwner = m_hWnd;
	browse.pidlRoot = NULL;
	browse.pszDisplayName = current_path;
	browse.lpszTitle = _T("공용컨텐츠 정보 폴더 설정");
	browse.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
	browse.lpfn = MyBrowseCallbackProc2;
	browse.lParam = (LPARAM)current_path;

	LPITEMIDLIST pidl = SHBrowseForFolder(&browse);

	if(NULL != pidl && SHGetPathFromIDList( pidl, new_path ))
	{
		m_strPublicContentsLockFolder = new_path;
		::WritePrivateProfileString("Settings", "LockFolder", m_strPublicContentsLockFolder, ::GetINIPath());
	}
}

LRESULT CContentsDistributorView::OnAddContents(WPARAM wParam, LPARAM lParam) // wParam=total_copy_count, lParam=id name(CString*, delete after)
{
	int total_count = (int)wParam;

	CString* str_id = (CString*)lParam;
	if(str_id == NULL) str_id = new CString();

	CTime cur_tm = CTime::GetCurrentTime();

	CString msg;
	msg.Format("Copying(0/%d)", total_count);

	CString str_count;
	str_count.Format("%d", m_nLineCount++ );

	m_lcLog.InsertItem(0, "");
	m_lcLog.SetItemText(0, COL_COUNT, str_count);
	m_lcLog.SetItemText(0, COL_TIME, cur_tm.Format("%Y/%m/%d %H:%M:%S"));
	m_lcLog.SetItemText(0, COL_CONTENTS_ID, (LPCSTR)*str_id);
	m_lcLog.SetItemText(0, COL_STATUS, msg);
	m_lcLog.SetItemData(0, total_count);

	delete str_id;

	// check item-count-limit
	int count = m_lcLog.GetItemCount();
	if(count > 10000)
	{
		int idx = count-1;
		for(int i=0; i<100; i++, idx--)
			m_lcLog.DeleteItem(idx);
	}

	return 0;
}

LRESULT CContentsDistributorView::OnChangeContentsStatus(WPARAM wParam, LPARAM lParam) // wParam=cur_copy_count, lParam=status(0:no_err, etc:err_msg(CString*, delete after) )
{
	int total_count = m_lcLog.GetItemData(0);
	int cur_count = (int)wParam;

	// err_msg
	if(lParam != 0)
	{
		CString* err_msg = (CString*)lParam;
		if(err_msg == NULL) err_msg = new CString();

		CString result = m_lcLog.GetItemText(0, COL_RESULT);
		result += (LPCSTR)*err_msg;
		result += ", ";

		m_lcLog.SetItemText(0, COL_RESULT, result);

		delete err_msg;
	}

	// field update
	CString msg;
	if(total_count != cur_count)
	{
		// copying...
		msg.Format("Copying(%d/%d)", cur_count, total_count);
	}
	else
	{
		// complete
		CString err_list = m_lcLog.GetItemText(0, COL_RESULT);
		if(err_list.GetLength() == 0)
		{
			// no err
			msg = "Complete";
		}
		else
		{
			// something error
			msg = "Error";
		}
	}
	m_lcLog.SetItemText(0, COL_STATUS, msg);

	return 0;
}

LRESULT CContentsDistributorView::OnStartStopSyncContents(WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.

	m_bDistributing = wParam ? true : false;

	if(m_bDistributing)
	{
		// sync-start

		// kill contents-file-change timer
		KillTimer(TIMER_CONTENTS_CHANGE);
		// kill contents-term-check timer
		KillTimer(TIMER_CONTENTS_TERM_CHECK);
	}
	else
	{
		// sync-stop

		// set contents-term-check timer
		SetTimer(TIMER_CONTENTS_TERM_CHECK, TIMER_CONTENTS_TERM_CHECK_TIME, NULL);
	}

	CString str_count;
	str_count.Format("%d", m_nLineCount++ );

	m_lcLog.InsertItem(0, "");
	m_lcLog.SetItemText(0, COL_COUNT, str_count);
	m_lcLog.SetItemText(0, COL_TIME, CTime::GetCurrentTime().Format("%Y/%m/%d %H:%M:%S"));
	m_lcLog.SetItemText(0, COL_CONTENTS_ID, m_bDistributing ? "Start Sync" : "Stop Sync");

	Invalidate();

	return 1;
}

LRESULT CContentsDistributorView::OnContentsChange(WPARAM wParam, LPARAM lParam)
{
	SetTimer(TIMER_CONTENTS_CHANGE, TIMER_CONTENTS_CHANGE_TIME, NULL);

	return 1;
}

LRESULT CContentsDistributorView::OnContentsMonitorFault(WPARAM wParam, LPARAM lParam)
{
	CDEventHandler::_clearContentsMonitorThread();
	CDEventHandler::_initContentsMonitorThread();

	return 1;
}

LRESULT CContentsDistributorView::OnStartPublicContentsCopy(WPARAM wParam, LPARAM lParam)	// wParam=source(CString*, delete after), lParam=target(CString*, delete after)
{
	//
	CString* source_path = (CString*)wParam;
	if(source_path == NULL) source_path = new CString();
	source_path->Insert(0, "From:");

	CString* target_path = (CString*)lParam;
	if(target_path == NULL) target_path = new CString();
	target_path->Insert(0, "To:");

	//
	CTime cur_tm = CTime::GetCurrentTime();

	//
	CString str_count;
	str_count.Format("%d", m_nLineCount++ );

	int idx = m_bDistributing ? 1 : 0;

	m_lcLog.InsertItem(idx, "");
	m_lcLog.SetItemText(idx, COL_COUNT, str_count);
	m_lcLog.SetItemText(idx, COL_TIME, cur_tm.Format("%Y/%m/%d %H:%M:%S"));
	m_lcLog.SetItemText(idx, COL_CONTENTS_ID, (LPCSTR)*source_path);
	m_lcLog.SetItemText(idx, COL_STATUS, "Public:Copying...");
	m_lcLog.SetItemText(idx, COL_RESULT, (LPCSTR)*target_path);
	m_lcLog.SetItemData(idx, MAXDWORD);

	delete source_path;
	delete target_path;

	if(m_lcLog.GetItemCount() > 1000)
	{
		for(int i=m_lcLog.GetItemCount()-1; i>900; i--)
			m_lcLog.DeleteItem(i);
	}

	return 0;
}

LRESULT CContentsDistributorView::OnStopPublicContentsCopy(WPARAM wParam, LPARAM lParam)	// wParam=status(0:no_err, etc:error), lParam=nothing
{
	// err_msg
	CString result_msg;
	switch(wParam)
	{
	case PUBLICCONTENTS_COPY_NOERROR:
		result_msg = "Public:Complete";
		break;
	case PUBLICCONTENTS_COPY_NOT_EXIST_SOURCE:
		result_msg = "Public:Source Not Exist";
		break;
	case PUBLICCONTENTS_COPY_COPY_ERROR:
		result_msg = "Public:Copy ERROR";
		break;
	default:
		result_msg = "Public:Unknown";
		break;
	}

	// find idx
	LVFINDINFO info;
	info.flags = LVFI_PARAM;
	info.lParam = MAXDWORD;

	int idx = m_lcLog.FindItem(&info);

	// field update
	m_lcLog.SetItemText(idx, COL_STATUS, result_msg);

	return 0;
}

void CContentsDistributorView::ResetSyncTIme()
{
	//
	TCHAR buf[255] = {0};
	::GetPrivateProfileString("Settings", "UsingSyncTime", "1", buf, 255, ::GetINIPath());
	m_nSetSyncTime = _ttoi(buf);

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("Settings", "SyncTime", DEFAULT_SYNC_TIME, buf, 255, ::GetINIPath());

	CString str;
	if(_tcslen(buf) != 5 || buf[2] != _T(':') ||
		buf[0] < _T('0') || buf[0] > _T('9') ||
		buf[1] < _T('0') || buf[1] > _T('9') ||
		buf[3] < _T('0') || buf[3] > _T('9') ||
		buf[4] < _T('0') || buf[4] > _T('9') )
			str = DEFAULT_SYNC_TIME;
	else
			str = buf;

	int pos = 0;
	int hour = _ttoi(str.Tokenize(_T(":"), pos));
	int minute = _ttoi(str.Tokenize(_T(":"), pos));

	if(hour != m_nSyncHour || minute != m_nSyncMinute) m_bTodaySync = false;

	m_nSyncHour = hour;
	m_nSyncMinute = minute;
}

void CContentsDistributorView::InitPublicContentsThread()
{
	if(m_pPublicContentsThread == NULL)
	{
		m_bPublicContentsThread = true;
		m_pPublicContentsThread = ::AfxBeginThread(PublicContentsThreadFunc, (LPVOID)GetSafeHwnd(), THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		m_pPublicContentsThread->m_bAutoDelete = FALSE;
		m_pPublicContentsThread->ResumeThread();
	}
	else
	{
		// already exist
	}
}

void CContentsDistributorView::DeletePublicContentsThread()
{
	m_bPublicContentsThread = false;

	if(m_pPublicContentsThread)
	{
		try
		{
			DWORD dwExitCode = 0;
			::GetExitCodeThread(m_pPublicContentsThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pPublicContentsThread->m_hThread, INFINITE);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}
			delete m_pPublicContentsThread;
		}
		catch(...)
		{
		}

		m_pPublicContentsThread = NULL;
	}
}

UINT PublicContentsThreadFunc(LPVOID pParam)
{
	//
	HWND parent_hwnd = (HWND)pParam;

	//
	TCHAR buf[MAX_PATH+1] = {0};
	::GetPrivateProfileString("Settings", "SourcePath", "", buf, MAX_PATH, ::GetINIPath());

	CString source_root = buf;

	//
	while(CContentsDistributorView::m_bPublicContentsThread)
	{
		CString source_item, target_item;
		if(CContentsDistributorView::m_PublicContentsInfo.GetFront(source_item, target_item))
		{
			char source_path[MAX_PATH] = {0};
			sprintf_s(source_path, MAX_PATH, "%s\\%s", source_root, source_item);

			char target_path[MAX_PATH] = {0};
			sprintf_s(target_path, MAX_PATH, "%s\\%s", source_root, target_item);

			ciDEBUG(5,("source_path=%s", source_path));
			ciDEBUG(5,("target_path=%s", target_path));

			// send start
			CString* src_path = new CString((LPCSTR)source_item);
			CString* tgt_path = new CString((LPCSTR)target_item);
			::PostMessage(parent_hwnd, WM_START_PUBLICCONTENTS_COPY, (WPARAM)src_path, (LPARAM)tgt_path);

			// get folder status
			int result = 0;
			CFileStatus fs;
			if(CFile::GetStatus(source_path, fs))
			{
				// folder copy
				SHFILEOPSTRUCT	shfo;
				::ZeroMemory(&shfo, sizeof shfo);

				WORD			wfunc	= FO_COPY;
				TCHAR*			pszFrom	= source_path;
				TCHAR*			pszTo	= target_path;
				TCHAR*			pszTitle= _T("복사");

				shfo.hwnd = parent_hwnd;
				shfo.wFunc = wfunc;
				shfo.fFlags = FOF_NO_UI;
				shfo.lpszProgressTitle=pszTitle;
				shfo.pTo=pszTo;
				shfo.pFrom=pszFrom;

				int ret_val = ::SHFileOperation(&shfo);
				if(ret_val)
				{
					result = PUBLICCONTENTS_COPY_COPY_ERROR;
				}
				else
				{
					result = PUBLICCONTENTS_COPY_NOERROR;
				}
			}
			else
			{
				result = PUBLICCONTENTS_COPY_NOT_EXIST_SOURCE;
			}

			//
			CContentsDistributorView::m_PublicContentsInfo.PopFront(true);
			if(result == PUBLICCONTENTS_COPY_COPY_ERROR)
			{
				CContentsDistributorView::m_PublicContentsInfo.PushBack(source_item, target_item);
			}

			// send stop
			::PostMessage(parent_hwnd, WM_STOP_PUBLICCONTENTS_COPY, (WPARAM)result, NULL);
		}

		::Sleep(1000);
	}

	return 0;
}

void CContentsDistributorView::OnCleanEmptyFolder()
{
	TCHAR buf[MAX_PATH+1] = {0};
	::GetPrivateProfileString("Settings", "SourcePath", "", buf, MAX_PATH, ::GetINIPath());

	CCleanEmptyFolderDlg dlg(buf);
	dlg.DoModal();
}

void CContentsDistributorView::OnRequestServerConfig()
{
	CRequestServerConfigDlg dlg;
	dlg.DoModal();
}
