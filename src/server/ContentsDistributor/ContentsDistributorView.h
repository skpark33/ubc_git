// ContentsDistributorView.h : CContentsDistributorView 클래스의 인터페이스
//


#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "atlimage.h"
#include "afxmt.h"
#include "afxtempl.h"

#include "PublicContentsInfo.h"


class CContentsDistributorView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CContentsDistributorView();
	DECLARE_DYNCREATE(CContentsDistributorView)

public:
	enum{ IDD = IDD_CONTENTSDISTRIBUTOR_FORM };

	enum {
		COL_COUNT = 0,
		COL_TIME,
		COL_CONTENTS_ID,
		COL_STATUS,
		COL_RESULT,
	};

// 특성입니다.
public:
	CContentsDistributorDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CContentsDistributorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSyncContentsNow();
	afx_msg void OnSetSyncTime();
	afx_msg void OnBnClickedButtonChangeSourcePath();
	afx_msg void OnBnClickedButtonChangeTargetPath();
	afx_msg void OnSettingLockfolder();
	LRESULT OnAddContents(WPARAM wParam, LPARAM lParam);
	LRESULT OnChangeContentsStatus(WPARAM wParam, LPARAM lParam);
	LRESULT OnStartStopSyncContents(WPARAM wParam, LPARAM lParam);
	LRESULT OnContentsChange(WPARAM wParam, LPARAM lParam);
	LRESULT OnContentsMonitorFault(WPARAM wParam, LPARAM lParam);
	LRESULT OnStartPublicContentsCopy(WPARAM wParam, LPARAM lParam);
	LRESULT OnStopPublicContentsCopy(WPARAM wParam, LPARAM lParam);

	bool	m_bDistributing;
	CImage	m_imgPlay;
	CImage	m_imgStop;
	int		m_nLineCount;

	int		m_nSetSyncTime;
	int		m_nSyncHour;
	int		m_nSyncMinute;
	bool	m_bTodaySync;

	CString	m_strPublicContentsLockFolder;

public:
	afx_msg void OnDestroy();

	CListCtrl	m_lcLog;
	CEdit	m_editSourcePath;
	CEdit	m_editTargetPath;
	CStatic	m_stcStatus;

	void	ResetSyncTIme();

	void	CheckPublicContents();

	// for public contents
	static CPublicContentsInfo	m_PublicContentsInfo;

	void	InitPublicContentsThread();
	void	DeletePublicContentsThread();

	static CWinThread*		m_pPublicContentsThread;			// 공용컨텐츠 스레드 핸들
	static bool 			m_bPublicContentsThread;			// 스레드 실행 flag
	afx_msg void OnCleanEmptyFolder();
	afx_msg void OnRequestServerConfig();
};

#ifndef _DEBUG  // ContentsDistributorView.cpp의 디버그 버전
inline CContentsDistributorDoc* CContentsDistributorView::GetDocument() const
   { return reinterpret_cast<CContentsDistributorDoc*>(m_pDocument); }
#endif


////////////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	char source_path[1024];
	char target_path[1024];
} PUBLIC_CONTENTS_PATH;

static UINT PublicContentsThreadFunc(LPVOID pParam);
