#include <stdafx.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libConfig/ciPath.h>

#include <ci/libDebug/ciArgParser.h>
#include <ci/libBase/ciStringTokenizer.h>

#include <cci/libUtil/cciCorbaMacro.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciBoolean.h>
#include <cci/libValue/cciValueInclude.h>

#include <cci/libObject/cciObject.h>
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciCall.h>

#include <common/libCommon/ubcIni.h>
#include "CDManager.h"

ciSET_DEBUG(10, "CDManager");

CDManager* 	CDManager::_instance = 0; 
ciMutex 	CDManager::_instanceLock;


CDManager*	
CDManager::getInstance() {
	if(!_instance) {
		ciGuard aGuard(_instanceLock);
		if(!_instance) {
			_instance = new CDManager;
		}
	}
	return _instance;
}
void	
CDManager::clearInstance() {
	if(_instance) {
		ciGuard aGuard(_instanceLock);
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}


CDManager::CDManager() 
{
    ciDEBUG(7, ("CDManager...()"));
	_mgrId = "";
	_eventHandler = 0;
}

CDManager::~CDManager() 
{
    ciDEBUG(7, ("~CDManager()"));
	if(_eventHandler){
		delete _eventHandler;
	}
}

ciBoolean
CDManager::init(const char* mgrId) 
{
    ciDEBUG(7, ("init(...)"));
	_mgrId = mgrId ;
	// ProgramLocation initialize

	if(ciArgParser::getInstance()->isSet("+withCD")){
		_initProgramLocation();
	}
	
	_eventHandler = new CDEventHandler();
	_eventHandler->addHand();

	return ciTrue;
}

int
CDManager::_initProgramLocation() 
{
    ciDEBUG(7, ("_initProgramLocation()"));

	ciString iniFiles = ciPath::getInstance()->getPath("..\\config");
	iniFiles += "\\*.ini";

	WIN32_FIND_DATA fileData;
	HANDLE hFind = FindFirstFileA(iniFiles.c_str(),&fileData);
	if(hFind == INVALID_HANDLE_VALUE){
		ciERROR(("%s is INVALID HANDLE PATH", iniFiles.c_str()));
		return 0;
	}
	ciDEBUG(1,("Find %s directory", iniFiles.c_str()));

	int counter=0;
	do {
		ciString buf = fileData.cFileName;
		if(buf.substr(buf.length()-4) != ".ini"){
			ciDEBUG(9,("ext is wrong %s skipped", fileData.cFileName));
			continue;
		}
		ciDEBUG(9,("%s founded", fileData.cFileName));

		ciString filename = fileData.cFileName;
		ciString programId = filename.substr(0, filename.length()-4);
	
		counter += (int) createProgramLocation(programId.c_str());

	} while(FindNextFile(hFind, &fileData));
	FindClose(hFind);                  // close the file handle

	return counter;
}


ciBoolean
CDManager::createProgramLocation(const char* programId)
{
	ciDEBUG(1,("createProgramLocation(%s)", programId));

	cciCall aCall("create");
	cciEntity aEntity("OD=OD/ProgramLocation=%s_%s",_mgrId.c_str(), programId);
	aCall.setEntity(aEntity);

	aCall.addItem("programId", programId);
	aCall.addItem("pmId", 	_mgrId.c_str());

	ciString callString = aCall.toString();
	ciString errString;
	try {
		if(!aCall.call()){
			errString = callString + " failed";
			ciERROR((errString.c_str()));
			return 0;
		}
	}catch(CORBA::Exception& ex){
		errString = callString + " failed(";
		errString += ex._name();
		errString += ")";
		ciERROR((errString.c_str()));
		return 0;
	}

	return ciTrue;
}

ciBoolean
CDManager::removeProgramLocation(const char* programId)
{
	ciDEBUG(1,("removeProgramLocation(%s)", programId));

	cciCall aCall("remove");
	cciEntity aEntity("OD=OD/ProgramLocation=*_%s",programId);
	aCall.setEntity(aEntity);

	ciString callString = aCall.toString();
	ciString errString;
	try {
		if(!aCall.call()){
			errString = callString + " failed";
			ciERROR((errString.c_str()));
			return 0;
		}
	}catch(CORBA::Exception& ex){
		errString = callString + " failed(";
		errString += ex._name();
		errString += ")";
		ciERROR((errString.c_str()));
		return 0;
	}

	return ciTrue;
}
/*
const char* 
CDManager::toString()
{
	_bufStr.clear();
	ciStringList::iterator itr;
	for(itr!=_mgrIdList.begin();itr!=_mgrIdList.end();itr++){
		if(!_bufStr.empty()) {
			_bufStr += "+";
		}
		_bufStr += (*itr);
	}
}

ciBoolean
CDManager::isMine(const char* mgrId)
{
	ciStringList::iterator itr;
	for(itr!=_mgrIdList.begin();itr!=_mgrIdList.end();itr++){
		if((*itr) == mgrId){
			ciDEBUG(1,("%s is mine", mgrId));
			return ciTrue;
		}
	}
	ciDEBUG(1,("%s is not mine", mgrId));
	return ciFalse;
}
*/