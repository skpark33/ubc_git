#ifndef _CDDownloadThread_h_
#define _CDDownloadThread_h_

#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>

////////////////////
// CDDownloadThread
class CDEventHandler;

class CDDownloadThread : public virtual ciThread {
public:
	CDDownloadThread(const char* siteId, const char* programId, CDEventHandler* handler);
	virtual ~CDDownloadThread();

	void run();
	void destroy();
	ciBoolean	isAlive() { return _isAlive; }

protected:

	ciBoolean		_isAlive;
	ciString		_siteId;
	ciString		_programId;
	CDEventHandler* _handler;
};
typedef list<CDDownloadThread*>  CDDownloadThreadList;

class CDDownloadThreadManager {
public:
	static int		size();
	static void		clearAll();
	static void		clear();
	static void		push(CDDownloadThread* pThread);
protected:
	static CDDownloadThreadList	_list;
	static ciMutex					_listLock;

};


#endif // _CDDownloadThread_h_
