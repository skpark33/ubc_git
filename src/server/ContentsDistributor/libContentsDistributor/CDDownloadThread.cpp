#include "stdafx.h"
//#include "libDownload/ProfileManager.h"
#include "libDownload/Downloader.h"

#include "ci/libDebug/ciDebug.h"
#include "common/libCommon/ubcIni.h"
#include "common/libScratch/ScratchUtil.h"

#include "CDDownloadThread.h"
#include "CDEventHandler.h"
#include "CDManager.h"

ciSET_DEBUG(10, "CDDownloadThread");

////////////////////
// CDDownloadThread

CDDownloadThread::CDDownloadThread(const char* siteId, const char* programId, CDEventHandler* handler)
:_isAlive(ciTrue), _siteId(siteId), _programId(programId), _handler(handler)
{
	ciDEBUG(2, ("CDDownloadThread()"));
}

CDDownloadThread::~CDDownloadThread() 
{
	ciDEBUG(2, ("~CDDownloadThread()"));
}

void 
CDDownloadThread::run() 
{
	ciDEBUG(5, ("run()"));
/*
	// CDownloader 
	CDownloader	libDownloader(false,1);
	libDownloader.SetPackagePath(_handler->getProgramPath());
	libDownloader.SetTmpPath(_handler->getTempPath());

	ciDEBUG(1,("start download program ini(%s)", _programId.c_str()));
	if(!libDownloader.DownloadPackageFile(_programId.c_str(), _siteId.c_str())){
		ciERROR(("Fail to download package file : %s", _programId.c_str()));
		return ;
	}
	ciDEBUG(1,("start download contents(%s,%s)",_siteId.c_str(),_programId.c_str()));
	libDownloader.SetContentsPath(_handler->getContentsPath());
	int nRet = libDownloader.DownloadPlayContents(_siteId.c_str(), _programId.c_str(), 0,ciFalse);
	if(nRet != E_DNRET_SUCCESS)	{
		ciERROR(("Downlaod %s failed : %s", _programId.c_str(), pszRet[nRet])); 
		return ;
	}

	ciDEBUG(1,("Succeed to download : %s", _programId.c_str()));
	
	// succeed to download, so this server has this program
	CDManager::getInstance()->createProgramLocation(_programId.c_str());
*/
	_isAlive = ciFalse;
	return;
}


void 
CDDownloadThread::destroy() {
	ciDEBUG(5, ("destroy()"));
	_isAlive = ciFalse;
}

CDDownloadThreadList		CDDownloadThreadManager::_list;
ciMutex						CDDownloadThreadManager::_listLock;

int 
CDDownloadThreadManager::size() 
{
	ciGuard aGuard(_listLock);
	return _list.size();
}

void 
CDDownloadThreadManager::push(CDDownloadThread* pThread) 
{
	ciGuard aGuard(_listLock);
	_list.push_back(pThread);
}

void 
CDDownloadThreadManager::clear() 
{
	ciGuard aGuard(_listLock);

	CDDownloadThreadList::iterator itr;
	for(itr=_list.begin();itr!=_list.end();){
		CDDownloadThread* aThread = (*itr);
		if(!aThread->isAlive()){
			ciDEBUG(5,("Thread deleted"));
			delete aThread;
			_list.erase(itr++);
		}else{
			++itr;
		}
	}
}

void 
CDDownloadThreadManager::clearAll() 
{
	ciGuard aGuard(_listLock);

	CDDownloadThreadList::iterator itr;
	for(itr=_list.begin();itr!=_list.end();itr++){
		CDDownloadThread* aThread = (*itr);
		while(aThread->isAlive()){
			::Sleep(1000);
		}
		delete aThread;
	}
	ciDEBUG(5,("Thread deleted"));
	_list.clear();
}