#pragma once

class CBrowseForFolder
{
public:
	CBrowseForFolder(HWND hParentWnd, LPCTSTR lpszTitle, LPCTSTR lpszInitFolderPath=_T(""));
	virtual ~CBrowseForFolder();

	BOOL		DoModal();
	LPCTSTR		GetSelectFolder() { return (LPCTSTR)m_strSelectFolder; };

private:
	HWND		m_hParentWnd;
	CString		m_strTitle;
	CString		m_strSelectFolder;
};
