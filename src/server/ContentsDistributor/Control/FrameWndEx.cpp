// FrameWndEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
//#include "ControlLib.h"
#include "FrameWndEx.h"

#include "Lib.h"
//#include ".\framewndex.h"
#include "afxadv.h"

#define		POSITION_KEY		_T("MainFramePosition")
#define		POSITION_FORMAT		_T("%u,%u,%d,%d,%d,%d,%d,%d,%d,%d")

// CFrameWndEx

IMPLEMENT_DYNCREATE(CFrameWndEx, CFrameWnd)

CFrameWndEx::CFrameWndEx()
: m_nFirstX (0)
, m_nFirstY (0)
, m_nFirstWidth(640)
, m_nFirstHeight(480)
, m_hTrayIcon(NULL)
, m_bMagnetEffect(false)
{
}

CFrameWndEx::~CFrameWndEx()
{
}


BEGIN_MESSAGE_MAP(CFrameWndEx, CFrameWnd)
	ON_WM_SHOWWINDOW()
	ON_WM_WINDOWPOSCHANGING()
	ON_MESSAGE(ID_ICON_NOTIFY, OnTrayNotification)
END_MESSAGE_MAP()


// CFrameWndEx 메시지 처리기입니다.

BOOL CFrameWndEx::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CString ini_path = ::GetINIPath();

	WINDOWPLACEMENT wp;
	GetWindowPlacement(&wp);

	CString str;
	str.Format(
		POSITION_FORMAT,
		wp.flags,
		wp.showCmd,
		wp.ptMinPosition.x,
		wp.ptMinPosition.y,
		wp.ptMaxPosition.x,
		wp.ptMaxPosition.y,
		wp.rcNormalPosition.left,
		wp.rcNormalPosition.top,
		wp.rcNormalPosition.right,
		wp.rcNormalPosition.bottom
	);

	::WritePrivateProfileString(_T("Settings"), POSITION_KEY, str, ini_path);

	if(m_hTrayIcon) ::DestroyIcon(m_hTrayIcon);

	return CFrameWnd::DestroyWindow();
}

void CFrameWndEx::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CFrameWnd::OnShowWindow(bShow, nStatus);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	static bool bOnce = true;

	if( bOnce &&
		bShow &&
		!IsWindowVisible() )
	{
		bOnce = false;

		CString ini_path = ::GetINIPath();

		WINDOWPLACEMENT wp;
		TCHAR position[1024];

		if(::GetPrivateProfileString(_T("Settings"), POSITION_KEY, _T(""), position, 1024, ini_path) > 0)
		{
			int read = _stscanf(
				position,
				POSITION_FORMAT,
				&wp.flags,
				&wp.showCmd,
				&wp.ptMinPosition.x,
				&wp.ptMinPosition.y,
				&wp.ptMaxPosition.x,
				&wp.ptMaxPosition.y,
				&wp.rcNormalPosition.left,
				&wp.rcNormalPosition.top,
				&wp.rcNormalPosition.right,
				&wp.rcNormalPosition.bottom
			);

			if ( read == 10 )
				if(SetWindowPlacement(&wp) == 0)
				{
					::AfxMessageBox(_T("Error"));
				}
		}
		else
		{
			wp.flags					= 0;
			wp.showCmd					= 1;
			wp.ptMinPosition.x			= -1;
			wp.ptMinPosition.y			= -1;
			wp.ptMaxPosition.x			= -1;
			wp.ptMaxPosition.y			= -1;
			wp.rcNormalPosition.left	= m_nFirstX;
			wp.rcNormalPosition.top		= m_nFirstY;
			wp.rcNormalPosition.right	= m_nFirstWidth;
			wp.rcNormalPosition.bottom	= m_nFirstHeight;

			SetWindowPlacement(&wp);
		}
	}
}

LRESULT CFrameWndEx::OnTrayNotification(WPARAM wParam, LPARAM lParam)
{
	if(m_TrayIcon.Enabled())
        return m_TrayIcon.OnTrayNotification(wParam, lParam);

	return 0;
}

void CFrameWndEx::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CFrameWnd::OnWindowPosChanging(lpwndpos);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(m_bMagnetEffect)
	{ // 자석 기능 모듈
		CRect desktop_rect;
		CRect taskbar_rect;
		CRect display_rect;
		HWND hTaskBar; 

		GetDesktopWindow()->GetClientRect(desktop_rect);

		//태스크바의 크기를 가져온다.
		hTaskBar = ::FindWindow(_T("Shell_TrayWnd"), NULL);
		if(hTaskBar)
		{
			::GetWindowRect(hTaskBar, &taskbar_rect);
			display_rect.SubtractRect(desktop_rect, taskbar_rect);
		}
		else
		{
			display_rect = desktop_rect;
		}

		// display 공간 자석 기능
		if( ((lpwndpos->x > display_rect.left) && (lpwndpos->x < display_rect.left + m_nMagnetGap)) ||
			((lpwndpos->x < display_rect.left) && (lpwndpos->x > display_rect.left - m_nMagnetGap)) )
		{
			lpwndpos->x = display_rect.left;
		}
		else if(((lpwndpos->x + lpwndpos->cx < display_rect.right) && (lpwndpos->x + lpwndpos->cx > display_rect.right - m_nMagnetGap)) ||
				((lpwndpos->x + lpwndpos->cx > display_rect.right) && (lpwndpos->x + lpwndpos->cx < display_rect.right + m_nMagnetGap)) )
		{
			lpwndpos->x = display_rect.right - lpwndpos->cx;
		}

		if( ((lpwndpos->y > display_rect.top) && (lpwndpos->y < display_rect.top + m_nMagnetGap)) ||
			((lpwndpos->y < display_rect.top) && (lpwndpos->y > display_rect.top - m_nMagnetGap)) )
		{
			lpwndpos->y = display_rect.top;
		}
		else if(((lpwndpos->y + lpwndpos->cy < display_rect.bottom) && (lpwndpos->y + lpwndpos->cy > display_rect.bottom - m_nMagnetGap)) ||
				((lpwndpos->y + lpwndpos->cy > display_rect.bottom) && (lpwndpos->y + lpwndpos->cy < display_rect.bottom + m_nMagnetGap)) )
		{
			lpwndpos->y = display_rect.bottom - lpwndpos->cy;
		}

		// desktop 공간 자석 기능
		if( ((lpwndpos->x > desktop_rect.left) && (lpwndpos->x < desktop_rect.left + m_nMagnetGap)) ||
			((lpwndpos->x < desktop_rect.left) && (lpwndpos->x > desktop_rect.left - m_nMagnetGap)) )
		{
			lpwndpos->x = desktop_rect.left;
		}
		else if(((lpwndpos->x + lpwndpos->cx < desktop_rect.right) && (lpwndpos->x + lpwndpos->cx > desktop_rect.right - m_nMagnetGap)) ||
				((lpwndpos->x + lpwndpos->cx > desktop_rect.right) && (lpwndpos->x + lpwndpos->cx < desktop_rect.right + m_nMagnetGap)) )
		{
			lpwndpos->x = desktop_rect.right - lpwndpos->cx;
		}

		if( ((lpwndpos->y > desktop_rect.top) && (lpwndpos->y < desktop_rect.top + m_nMagnetGap)) ||
			((lpwndpos->y < desktop_rect.top) && (lpwndpos->y > desktop_rect.top - m_nMagnetGap)) )
		{
			lpwndpos->y = desktop_rect.top;
		}
		else if(((lpwndpos->y + lpwndpos->cy < desktop_rect.bottom) && (lpwndpos->y + lpwndpos->cy > desktop_rect.bottom - m_nMagnetGap)) ||
				((lpwndpos->y + lpwndpos->cy > desktop_rect.bottom) && (lpwndpos->y + lpwndpos->cy < desktop_rect.bottom + m_nMagnetGap)) )
		{
			lpwndpos->y = desktop_rect.bottom - lpwndpos->cy;
		}
	}
}

BOOL CFrameWndEx::CreateTrayIcon(CWnd* pWnd, LPCTSTR lpszTitle, UINT nMenuID, UINT nBmpIcon)
{
	// Make Tray Icon
	// 투명배경색은 무조건 White(255,255,255)
	if(m_BMP24bit.LoadBitmap(nBmpIcon))
	{
		//ICONINFO icInfo;
		//icInfo.fIcon = TRUE;
		//icInfo.hbmMask = (HBITMAP) m_BMP24bit;
		//icInfo.xHotspot = 0;
		//icInfo.yHotspot = 0;
		//icInfo.hbmColor = (HBITMAP) m_BMP24bit;
		//m_hTrayIcon = CreateIconIndirect(&icInfo);
		m_hTrayIcon = ::LoadIcon(::AfxGetInstanceHandle(), MAKEINTRESOURCE(nBmpIcon) );
	}

	CString title;
	title.LoadString(AFX_IDS_APP_TITLE);
	//title.Append(_T(" "));
	//title.Append(::GetAppVersion());

	return m_TrayIcon.Create(pWnd, ID_ICON_NOTIFY, lpszTitle, m_hTrayIcon, nMenuID);
}

bool CFrameWndEx::GetUseMagnetEffect()
{
	return m_bMagnetEffect;
}

void CFrameWndEx::SetUseMagnetEffect(bool bEffect, int nGap)
{
	m_bMagnetEffect = bEffect;
	m_nMagnetGap = nGap;
}

void CFrameWndEx::SetAlwaysOnTop(bool bTop)
{
	if(bTop)
	{ // always top ON
		SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	}
	else
	{ // always top OFF
		SetWindowPos(&wndNoTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	}
}

void CFrameWndEx::CreateHighColorToolbar(CToolBar* pToolbar, int cx, int cy, int nGrow, UINT nEnableID, UINT nDisableID, COLORREF bgRGB)
{
	// hi-color toolbar
	CBitmap		bitmap1;
	bitmap1.LoadBitmap(nEnableID);

	m_imglstToolbarEnable.Create(cx, cy, ILC_COLORDDB|ILC_MASK, 0, nGrow);
	m_imglstToolbarEnable.Add(&bitmap1, bgRGB); //바탕의 분홍색이 마스크

	pToolbar->SendMessage(TB_SETIMAGELIST, 0, (LPARAM)m_imglstToolbarEnable.m_hImageList); // 활성이미지

	//
	CBitmap		bitmap2;
	bitmap2.LoadBitmap(nDisableID);

	m_imglstToolbarDisable.Create(cx, cy, ILC_COLORDDB|ILC_MASK, 0, nGrow);
	m_imglstToolbarDisable.Add(&bitmap2, bgRGB); 

	pToolbar->SendMessage(TB_SETDISABLEDIMAGELIST, 0, (LPARAM)m_imglstToolbarDisable.m_hImageList); // 비활성이미지.. 
}
