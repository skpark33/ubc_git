// RequestServerConfigDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ContentsDistributor.h"
#include "RequestServerConfigDlg.h"

#include <ci/libConfig/ciPath.h>

// CRequestServerConfigDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CRequestServerConfigDlg, CDialog)

CRequestServerConfigDlg::CRequestServerConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRequestServerConfigDlg::IDD, pParent)
{
	//
	CString app;
	app.LoadString(AFX_IDS_APP_TITLE);

	m_strIniPath = ciPath::getInstance()->getPath("");
	m_strIniPath += app;
	m_strIniPath += ".ini";
}

CRequestServerConfigDlg::~CRequestServerConfigDlg()
{
}

void CRequestServerConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_IP, m_editIP);
	DDX_Control(pDX, IDC_EDIT_PORT, m_editPort);
}


BEGIN_MESSAGE_MAP(CRequestServerConfigDlg, CDialog)
END_MESSAGE_MAP()


// CRequestServerConfigDlg 메시지 처리기입니다.

BOOL CRequestServerConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	char sz_ip[255], sz_port[255];
	::GetPrivateProfileString("Settings", "IP", "",   sz_ip,   254, m_strIniPath );
	::GetPrivateProfileString("Settings", "Port", "", sz_port, 254, m_strIniPath );

	//
	m_editIP.SetWindowText(sz_ip);
	m_editPort.SetWindowText(sz_port);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CRequestServerConfigDlg::OnOK()
{
	//
	CString str_ip;
	m_editIP.GetWindowText(str_ip);

	CString str_port;
	m_editPort.GetWindowText(str_port);

	//
	::WritePrivateProfileString("Settings", "IP", str_ip, m_strIniPath );
	::WritePrivateProfileString("Settings", "Port", str_port, m_strIniPath );

	CDialog::OnOK();
}
