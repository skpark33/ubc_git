#include "StdAfx.h"
#include "BrowseForFolder.h"


int CALLBACK BrowseForFolder_CallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	if(uMsg == BFFM_INITIALIZED)
		SendMessage(hwnd, BFFM_SETSELECTION, (WPARAM)TRUE, (LPARAM)lpData);
	return 0;
}

CBrowseForFolder::CBrowseForFolder(HWND hParentWnd, LPCTSTR lpszTitle, LPCTSTR lpszInitFolderPath)
{
	m_hParentWnd = hParentWnd;
	m_strTitle = lpszTitle;
	m_strSelectFolder = lpszInitFolderPath;
}

CBrowseForFolder::~CBrowseForFolder(void)
{
}

BOOL CBrowseForFolder::DoModal()
{
	ITEMIDLIST *pildBrowse;
	TCHAR pszPathname[MAX_PATH] = {0};

	BROWSEINFO bInfo;
	memset(&bInfo, 0, sizeof(bInfo));
	bInfo.hwndOwner			= m_hParentWnd;
	bInfo.pidlRoot			= NULL;
	bInfo.pszDisplayName	= pszPathname;
	bInfo.lpszTitle			= m_strTitle;
	bInfo.ulFlags			= BIF_RETURNONLYFSDIRS;
	bInfo.lpfn				= BrowseForFolder_CallbackProc;
	bInfo.lParam			= (LPARAM)(LPCTSTR)m_strSelectFolder; // 초기로 지정할 디렉토리 경로가 저장된 변수

	pildBrowse = ::SHBrowseForFolder(&bInfo);

	if( pildBrowse != NULL )
	{
		SHGetPathFromIDList(pildBrowse, pszPathname);

		m_strSelectFolder = pszPathname;

		return TRUE;
	}

	return FALSE;
}
