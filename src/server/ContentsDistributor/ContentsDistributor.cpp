// ContentsDistributor.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "ContentsDistributor.h"
#include "MainFrm.h"

#include "ContentsDistributorDoc.h"
#include "ContentsDistributorView.h"

#include "libContentsDistributor/CDEventHandler.h"
#include "libContentsDistributor/CDManager.h"

#include "ci/libDebug/ciDebug.h"
#include "ci/libDebug/ciArgParser.h"
#include "ci/libWorld/ciWorld.h"
#include "CDWorld.h"

#include "Lib.h"


ciSET_DEBUG(10,"CContentsDistributorApp");


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		DEFAULT_SOURCE_CONTENTS_FOLDER_NAME			"contents"
#define		DEFAULT_TARGET_CONTENTS_FOLDER_NAME			"local_contents"


// CContentsDistributorApp

BEGIN_MESSAGE_MAP(CContentsDistributorApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CContentsDistributorApp::OnAppAbout)
	// 표준 파일을 기초로 하는 문서 명령입니다.
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()


// CContentsDistributorApp 생성

CContentsDistributorApp::CContentsDistributorApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
	m_worldThread = NULL;
}


// 유일한 CContentsDistributorApp 개체입니다.

CContentsDistributorApp theApp;


// CContentsDistributorApp 초기화

BOOL CContentsDistributorApp::InitInstance()
{
	ciDebug::setDebugOn();
	//ciDebug::logOpen(".\\log\\ContentsDistributor.log");
	CheckLogFileDate();

	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다. 
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// OLE 라이브러리를 초기화합니다.
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("ContentsDistributor"));
	//LoadStdProfileSettings(4);  // MRU를 포함하여 표준 INI 파일 옵션을 로드합니다.
	// 응용 프로그램의 문서 템플릿을 등록합니다. 문서 템플릿은
	//  문서, 프레임 창 및 뷰 사이의 연결 역할을 합니다.
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CContentsDistributorDoc),
		RUNTIME_CLASS(CMainFrame),       // 주 SDI 프레임 창입니다.
		RUNTIME_CLASS(CContentsDistributorView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	// find source path
	TCHAR source_path[MAX_PATH+1] = {0};
	::GetPrivateProfileString("Settings", "SourcePath", "", source_path, MAX_PATH, ::GetINIPath());
	if(strlen(source_path) == 0)
	{
		::ZeroMemory(source_path, sizeof(source_path));
		::GetModuleFileName(NULL, source_path, MAX_PATH);

		// find path
		int length = _tcslen(source_path) - 1;
		while( (length >= 0) && (source_path[length] != '\\') )
			source_path[length--] = 0;

		// find contents path
		length = _tcslen(source_path) - 1;
		if(length >= 0 && source_path[length] == '\\' ) source_path[length--] = 0;
		while( (length >= 0) && (source_path[length] != '\\') )
			source_path[length--] = 0;
		strcat(source_path, DEFAULT_SOURCE_CONTENTS_FOLDER_NAME);
	}
	::WritePrivateProfileString("Settings", "SourcePath", source_path, ::GetINIPath());

	// find target path
	TCHAR target_path[MAX_PATH+1] = {0};
	::GetPrivateProfileString("Settings", "TargetPath", "", target_path, MAX_PATH, ::GetINIPath());
	if(strlen(target_path) == 0)
	{
		::ZeroMemory(target_path, sizeof(target_path));
		::GetModuleFileName(NULL, target_path, MAX_PATH);

		// find path
		int length = _tcslen(target_path) - 1;
		while( (length >= 0) && (target_path[length] != '\\') )
			target_path[length--] = 0;

		// find contents path
		length = _tcslen(target_path) - 1;
		if(length >= 0 && target_path[length] == '\\' ) target_path[length--] = 0;
		while( (length >= 0) && (target_path[length] != '\\') )
			target_path[length--] = 0;
		strcat(target_path, DEFAULT_TARGET_CONTENTS_FOLDER_NAME);
	}
	::WritePrivateProfileString("Settings", "TargetPath", target_path, ::GetINIPath());

#ifndef DEBUG
	//
	m_worldThread = new CDWorldThread();
	m_worldThread->init();
	ciDEBUG(1,("CDWorldThread before start"));
	m_worldThread->start();
	ciDEBUG(1,("CDWorldThread after start"));
	//
#endif

	// 표준 셸 명령, DDE, 파일 열기에 대한 명령줄을 구문 분석합니다.
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	cmdInfo.m_nShellCommand = CCommandLineInfo::FileNew;	// <-- arguments 무시

	// 명령줄에 지정된 명령을 디스패치합니다.
	// 응용 프로그램이 /RegServer, /Register, /Unregserver 또는 /Unregister로 시작된 경우 FALSE를 반환합니다.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// 창 하나만 초기화되었으므로 이를 표시하고 업데이트합니다.
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// 접미사가 있을 경우에만 DragAcceptFiles를 호출합니다.
	//  SDI 응용 프로그램에서는 ProcessShellCommand 후에 이러한 호출이 발생해야 합니다.
	return TRUE;
}



// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// 대화 상자를 실행하기 위한 응용 프로그램 명령입니다.
void CContentsDistributorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CContentsDistributorApp 메시지 처리기


int CContentsDistributorApp::ExitInstance()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(m_worldThread)
	{
		m_worldThread->stop();
		delete m_worldThread;
	}
	//CDManager::clearInstance();

	return CWinApp::ExitInstance();
}


#define		DEFAULT_LOG_FILE_PREFIX			"ContentsDistributor"
#define		DEFAULT_LOG_FILE_TERM			(7)		// days

void CContentsDistributorApp::CheckLogFileDate()
{
	// new logfile open
	static int log_month = -1;
	static int log_day = -1;

	CTime cur_tm = CTime::GetCurrentTime();
	int cur_month = cur_tm.GetMonth();
	int cur_day = cur_tm.GetDay();

	// check date
	if(log_month == cur_month && log_day == cur_day) return;

	// new logfile open
	CString log_path;
	log_path.Format(".\\log\\%s_%02d%02d.log", DEFAULT_LOG_FILE_PREFIX, cur_month, cur_day);

	ciDebug::logClose();
	ciDebug::logOpen(log_path);
	log_month = cur_month;
	log_day = cur_day;

	// delete old logfile
	CMapStringToString map_logfiles_in_terms;
	for(int i=0; i<DEFAULT_LOG_FILE_TERM; i++)
	{
		CTimeSpan tm_span(i, 0, 0, 0);

		CTime old_tm = cur_tm - tm_span;
		CString log_file;
		log_file.Format("%s_%02d%02d.log", DEFAULT_LOG_FILE_PREFIX, old_tm.GetMonth(), old_tm.GetDay());

		map_logfiles_in_terms.SetAt(log_file.MakeLower(), log_file.MakeLower());
	}

	CString filter;
	filter.Format(".\\log\\%s_*.log", DEFAULT_LOG_FILE_PREFIX);

	CFileFind ff;
	BOOL bWorking = ff.FindFile(filter);
	while (bWorking)
	{
		bWorking = ff.FindNextFile();

		if (ff.IsDots()) continue;
		if (ff.IsDirectory()) continue;

		CString file_name = ff.GetFileName();
		CString value;
		if( map_logfiles_in_terms.Lookup(file_name.MakeLower(), value) ) continue;

		::DeleteFile(ff.GetFilePath());
	}
	ff.Close();
}
