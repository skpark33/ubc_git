#pragma once

bool	__LOG_OPEN__(LPCTSTR lpszLogFilename=NULL);
void	__LOG_CLOSE__();
bool	__IS_LOG_OPEN__();
LPCTSTR	__GET_LOG_NAME__();

#define	__LOG__(msg)	\
							__log_header__(_T("log  "), __FUNCTION__, __LINE__); \
							__log_body__ msg; \
							__log_tail__();

#define	__DEBUG__(msg)	\
							__log_header__(_T("debug"), __FUNCTION__, __LINE__); \
							__log_body__ msg; \
							__log_tail__();

#define	__WARN__(msg)	\
							__log_header__(_T("WARN "), __FUNCTION__, __LINE__); \
							__log_body__ msg; \
							__log_tail__();

#define	__ERROR__(msg)	\
							__log_header__(_T("ERROR"), __FUNCTION__, __LINE__); \
							__log_body__ msg; \
							__log_tail__();

#define	__FUNC_BEGIN__		__DEBUG__( (_T("begin...")) );
#define	__FUNC_END__		__DEBUG__( (_T("end.")) );


void	__log_header__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine);
void	__log_body__(LPCTSTR pFormat,...);
void	__log_tail__();
