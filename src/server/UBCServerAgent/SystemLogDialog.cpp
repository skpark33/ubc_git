// SystemLogDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCServerAgent.h"
#include "SystemLogDialog.h"


// CSystemLogDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSystemLogDialog, CDialog)

CSystemLogDialog::CSystemLogDialog(SYSTEMLOG_INFO* pLog, CWnd* pParent /*=NULL*/)
	: CDialog(CSystemLogDialog::IDD, pParent)
	, m_pSystemInfo ( pLog )
{
}

CSystemLogDialog::~CSystemLogDialog()
{
}

void CSystemLogDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_TIME, m_editTime);
	DDX_Control(pDX, IDC_EDIT_LOG, m_editLog);
	DDX_Control(pDX, IDC_EDIT_DETAIL_INFO, m_editDetailInfo);
}


BEGIN_MESSAGE_MAP(CSystemLogDialog, CDialog)
END_MESSAGE_MAP()


// CSystemLogDialog 메시지 처리기입니다.

BOOL CSystemLogDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	if(m_pSystemInfo)
	{
		m_editTime.SetWindowText(m_pSystemInfo->strTime);
		m_editLog.SetWindowText(m_pSystemInfo->strLog);
		m_editDetailInfo.SetWindowText(m_pSystemInfo->strDetailInfo);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
