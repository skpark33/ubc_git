// UBCServerAgentDoc.h : interface of the CUBCServerAgentDoc class
//


#pragma once


class CUBCServerAgentDoc : public CDocument
{
protected: // create from serialization only
	CUBCServerAgentDoc();
	DECLARE_DYNCREATE(CUBCServerAgentDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CUBCServerAgentDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	CString	m_strCustomer;
	CString	m_strServerID;
	CString	m_strServerIP;
	CString	m_strServerName;

	UBCPROCESS_INFOMAP		m_mapUBCProcess;
	PUBCPROCESS_INFOLIST	m_listUBCProcess;
	ALLPROCESS_INFOMAP		m_mapAllProcess;

	int		m_nMemoryThreshold;
	int		m_nHandleThreshold;

	void	ClearUBCProcessList();
	void	ClearAllProcessList();

	BOOL	GetUBCProcessList();	// load from UBCStarter.ini
	BOOL	GetAllProcessList();	// get current process-list -- wmic process get caption, creationdate, sessionid

	BOOL	GetIPAddress(CString& strAddr);

public:
	BOOL	GetUBCProcessList(UBCPROCESS_INFOLIST& info_list);
	BOOL	GetServerProcessList(PUBCPROCESS_INFOLIST& info_list); // must delete item-objects in info_list

	BOOL	WriteStartUpTime();	// export start-up-time itself

	void	GetServerInfo(CString& strCustomer, CString& strServerID, CString& strServerIP)
			{ strCustomer=m_strCustomer; strServerID=m_strServerID; strServerIP=m_strServerIP; };
};

