#pragma once
#include "afxwin.h"

#include "CxStatic.h"

// CAutoCloseMessageDlg 대화 상자입니다.

class CAutoCloseMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CAutoCloseMessageDlg)

public:
	CAutoCloseMessageDlg(LPTSTR iconID, LPCTSTR lpszMessage, int nViewSec, CWnd* pParent = NULL);   // 표준 생성자입니다.
			// iconID = IDI_WARNING, IDI_ERROR, IDI_INFORMATION, IDI_QUESTION

	virtual ~CAutoCloseMessageDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_AUTOCLOSE_MESSAGE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	LPTSTR		m_iconID;
	CString		m_strMessage;
	int			m_nViewSec;

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CStatic		m_icon;
	//CStatic		m_stcMessage;
	CxStatic		m_stcMessage;
};
