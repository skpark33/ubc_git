
#include "stdafx.h"

#include "Log.h"

FILE*				__log_file__ = NULL;
TCHAR				__log_name__[1024+1] = {0};
CCriticalSection	__log_lock__;

bool __LOG_OPEN__(LPCTSTR lpszLogFilename)
{
	if(lpszLogFilename==NULL || _tcslen(lpszLogFilename)==0) return false;

	_tcsncpy(__log_name__, lpszLogFilename, 1024);

	// make .bak.bak & .bak
	TCHAR log_bak[1024+1] = {0};
	TCHAR log_bak_bak[1024+1] = {0};

	_tcsncpy(log_bak, lpszLogFilename, 1024); _tcscat(log_bak, _T(".bak"));
	_tcsncpy(log_bak_bak, log_bak, 1024); _tcscat(log_bak_bak, _T(".bak"));

	::DeleteFile(log_bak_bak);
	::MoveFile(log_bak, log_bak_bak);
	::MoveFile(lpszLogFilename, log_bak);

	__log_lock__.Lock();

	if(__log_file__) fclose(__log_file__);
	__log_file__ = _tfopen(lpszLogFilename, "w");

	__log_lock__.Unlock();

	return (__log_file__ != NULL);
}

void __LOG_CLOSE__()
{
	__log_lock__.Lock();

	if(__log_file__) fclose(__log_file__);
	__log_file__ = NULL;

	__log_lock__.Unlock();
}

bool __IS_LOG_OPEN__()
{
	return (__log_file__ != NULL);
}

LPCTSTR __GET_LOG_NAME__()
{
	return __log_name__;
}

void	__log_header__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine)
{
	__log_lock__.Lock();

	if( __log_file__ == NULL ) return;

#ifdef _UNICODE
	CString strFuncName;
	ANSItoUNICODE(lpszFuncName, strFuncName);
	_ftprintf(__log_file__, _T("[%s %s] [%s] %s (line:%d)"), datebuf, timebuf, lpszType, lpszFuncName, nLine);
#else
	_ftprintf(__log_file__, 
				_T("[%s] [%s] %s (line:%d) "), 
				CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S"), 
				lpszType, 
				lpszFuncName, 
				nLine
			);
#endif
}

void	__log_body__(LPCTSTR pFormat,...)
{
	if( __log_file__ == NULL ) return;

	va_list	aVp;
	va_start(aVp, pFormat);
	_vftprintf(__log_file__, pFormat, aVp);
	va_end(aVp);
}

void	__log_tail__()
{
	if( __log_file__ != NULL )
	{
		_ftprintf(__log_file__, _T("\n") );
		fflush(__log_file__);
	}

	__log_lock__.Unlock();
}
