// UBCServerAgentView.h : interface of the CUBCServerAgentView class
//


#pragma once
#include "afxcmn.h"

#include "SortListCtrl.h"
#include "afxwin.h"

class CUBCServerAgentView : public CFormView
{
protected: // create from serialization only
	CUBCServerAgentView();
	DECLARE_DYNCREATE(CUBCServerAgentView)

public:
	enum{ IDD = IDD_UBCSERVERAGENT_FORM };

// Attributes
public:
	CUBCServerAgentDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CUBCServerAgentView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	enum { COL_APPID=0, COL_BINARY_NAME, COL_STATUS, COL_START_TIME, COL_ARGUMENT, COL_PROPERTIES, COL_DEBUG, COL_COUNT };
	enum { SYS_COL_TIME=0, SYS_COL_STRING, SYS_COL_COUNT };

	int		m_nCurrentStatusThreadID;

	BOOL	m_bInitStatusResult;
	BOOL	m_bSendStatusResult;

	void	ClearProcessList();
	void	ClearSystemLogList(int nDeleteCount=0);

public:
	static CCriticalSection	m_csCheckThread;
	static int m_nCurrentCheckThreadID;
//	static BOOL	m_bCheckStatusResult;

	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnNMDblclkListSystemLog(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT	OnPostSystemLog(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCompleteInitUBCProcessStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCompleteSendUBCProcessStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCompleteCheckUBCProcessCommand(WPARAM wParam, LPARAM lParam);

	CEdit	m_editCustomer;
	CEdit	m_editServerID;
	CEdit	m_editServerIP;

	CSortListCtrl	m_lcProcess;
	CSortListCtrl	m_lcSystemLog;
};

#ifndef _DEBUG  // debug version in UBCServerAgentView.cpp
inline CUBCServerAgentDoc* CUBCServerAgentView::GetDocument() const
   { return reinterpret_cast<CUBCServerAgentDoc*>(m_pDocument); }
#endif


typedef struct
{
	HWND	hParentHwnd;
	int		nThreadID;
	CString	strCustomer;
	CString	strServerId;
	CString	strServerIp;
	//PUBCPROCESS_INFOLIST* pSPList;
	UBCPROCESS_INFOLIST* pSPList;
} THREAD_PARAM;

static UINT InitServerProcessStatusThread(LPVOID pParam);
static UINT SendServerProcessStatusThread(LPVOID pParam);
static UINT CheckServerProcessStatusThread(LPVOID pParam);
