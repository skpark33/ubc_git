//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCServerAgent.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_UBCSERVERAGENT_FORM         101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_UBCServerAgentTYPE          129
#define IDR_TRAY_MENU                   130
#define IDD_SYSTEM_LOG                  131
#define IDD_AUTOCLOSE_MESSAGE_DIALOG    132
#define IDC_LIST_PROCESS                1000
#define IDC_LIST_SYSTEM_LOG             1001
#define IDC_EDIT_TIME                   1002
#define IDC_EDIT_LOG                    1003
#define IDC_EDIT_DETAIL_INFO            1004
#define IDI_ICON                        1005
#define IDC_STATIC_MESSAGE              1006
#define IDS_ALREADY_EXIST_PROCESS       1007
#define IDC_EDIT_CUSTOMER               1007
#define IDC_EDIT_SERVER_ID              1008
#define IDC_EDIT3                       1009
#define IDC_EDIT_SERVER_IP              1009
#define ID__OPEN                        32771
#define ID__EXIT                        32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
