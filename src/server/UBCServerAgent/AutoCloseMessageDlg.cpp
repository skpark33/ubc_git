// AutoCloseMessageDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCServerAgent.h"
#include "AutoCloseMessageDlg.h"


#define		AUTO_CLOSE_TIMER_ID		1026

// CAutoCloseMessageDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAutoCloseMessageDlg, CDialog)

CAutoCloseMessageDlg::CAutoCloseMessageDlg(LPTSTR iconID, LPCTSTR lpszMessage, int nViewSec, CWnd* pParent /*=NULL*/)
	: CDialog(CAutoCloseMessageDlg::IDD, pParent)
	, m_iconID ( iconID )
	, m_strMessage ( lpszMessage )
	, m_nViewSec ( nViewSec )
{

}

CAutoCloseMessageDlg::~CAutoCloseMessageDlg()
{
}

void CAutoCloseMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDI_ICON, m_icon);
	DDX_Control(pDX, IDC_STATIC_MESSAGE, m_stcMessage);
}


BEGIN_MESSAGE_MAP(CAutoCloseMessageDlg, CDialog)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CAutoCloseMessageDlg 메시지 처리기입니다.

BOOL CAutoCloseMessageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	HICON ico = ::LoadIcon(NULL, m_iconID);
	m_icon.SetIcon(ico);
	::DeleteObject(ico);

	m_stcMessage.SetWindowText(m_strMessage);
	SetTimer(AUTO_CLOSE_TIMER_ID, m_nViewSec*1000, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAutoCloseMessageDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	switch(nIDEvent)
	{
	case AUTO_CLOSE_TIMER_ID:
		KillTimer(AUTO_CLOSE_TIMER_ID);
		OnOK();
		break;
	}

	CDialog::OnTimer(nIDEvent);
}
