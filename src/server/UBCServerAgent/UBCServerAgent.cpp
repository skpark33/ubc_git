// UBCServerAgent.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "UBCServerAgent.h"
#include "MainFrm.h"

#include "UBCServerAgentDoc.h"
#include "UBCServerAgentView.h"

#include "AutoCloseMessageDlg.h"
#include "Log.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUBCServerAgentApp

BEGIN_MESSAGE_MAP(CUBCServerAgentApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CUBCServerAgentApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()

HANDLE CUBCServerAgentApp::m_hSingletonMutex = NULL;

// CUBCServerAgentApp construction

CUBCServerAgentApp::CUBCServerAgentApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CUBCServerAgentApp object

CUBCServerAgentApp theApp;


// CUBCServerAgentApp initialization

BOOL CUBCServerAgentApp::InitInstance()
{
	//__LOG_OPEN__("log\\UBCServerAgent.log");
	CheckLogFileDate();

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CUBCServerAgentDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CUBCServerAgentView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);


	CheckSingletonProcess();


	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);


	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	return TRUE;
}

int CUBCServerAgentApp::ExitInstance()
{
	if(m_hSingletonMutex)
	{
		::ReleaseMutex(m_hSingletonMutex);
		::CloseHandle(m_hSingletonMutex);
	}

	return CWinApp::ExitInstance();
}


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CUBCServerAgentApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CUBCServerAgentApp message handlers

BOOL CUBCServerAgentApp::CheckSingletonProcess()
{
	m_hSingletonMutex = ::CreateMutex(NULL, FALSE, "Global\\UBCServerAgent");
	DWORD err_code = ::GetLastError();
	if(!m_hSingletonMutex || err_code == ERROR_ALREADY_EXISTS)
	{
		// already exist UBCServerAgent
		CString msg;
		msg.LoadString(IDS_ALREADY_EXIST_PROCESS);

		CAutoCloseMessageDlg dlg(IDI_WARNING, msg, 5);
		dlg.DoModal();

		exit(1);

		return TRUE;
	}
	else
	{
		// fail to create mutex -> maybe exist multiple-process of UBCServerAgent
	}

	return FALSE;
}

#define		DEFAULT_LOG_FILE_PREFIX		_T("UBCServerAgent")
#define		DEFAULT_LOG_FILE_TERM		(7)

void CUBCServerAgentApp::CheckLogFileDate()
{
	// new logfile open
	static int log_month = -1;
	static int log_day = -1;

	CTime cur_tm = CTime::GetCurrentTime();
	int cur_month = cur_tm.GetMonth();
	int cur_day = cur_tm.GetDay();

	// check date
	if(log_month == cur_month && log_day == cur_day) return;

	// new logfile open
	CString log_path;
	log_path.Format(".\\log\\%s_%02d%02d.log", DEFAULT_LOG_FILE_PREFIX, cur_month, cur_day);

	__LOG_CLOSE__();
	__LOG_OPEN__(log_path);
	log_month = cur_month;
	log_day = cur_day;

	// delete old logfile
	CMapStringToString map_logfiles_in_terms;
	for(int i=0; i<DEFAULT_LOG_FILE_TERM; i++)
	{
		CTimeSpan tm_span(i, 0, 0, 0);

		CTime old_tm = cur_tm - tm_span;
		CString log_file;
		log_file.Format("%s_%02d%02d.log", DEFAULT_LOG_FILE_PREFIX, old_tm.GetMonth(), old_tm.GetDay());

		map_logfiles_in_terms.SetAt(log_file.MakeLower(), log_file.MakeLower());
	}

	CString filter;
	filter.Format(".\\log\\%s_*.log", DEFAULT_LOG_FILE_PREFIX);

	CFileFind ff;
	BOOL bWorking = ff.FindFile(filter);
	while (bWorking)
	{
		bWorking = ff.FindNextFile();

		if (ff.IsDots()) continue;
		if (ff.IsDirectory()) continue;

		CString file_name = ff.GetFileName();
		CString value;
		if( map_logfiles_in_terms.Lookup(file_name.MakeLower(), value) ) continue;

		bool find = false;
		POSITION pos = map_logfiles_in_terms.GetStartPosition();
		while(pos)
		{
			CString key, value;
			map_logfiles_in_terms.GetNextAssoc(pos, key, value);

			if( file_name.GetLength() != 0 && 
				value.GetLength() != 0 && 
				_tcsnicmp(file_name, value, value.GetLength() ) == 0)
			{
				find = true;
				break;
			}
		}
		if(find) continue;

		::DeleteFile(ff.GetFilePath());
	}
	ff.Close();
}
