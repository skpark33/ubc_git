#pragma once
#include "afxwin.h"


// CSystemLogDialog 대화 상자입니다.

class CSystemLogDialog : public CDialog
{
	DECLARE_DYNAMIC(CSystemLogDialog)

public:
	CSystemLogDialog(SYSTEMLOG_INFO* pLog, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSystemLogDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SYSTEM_LOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	SYSTEMLOG_INFO*		m_pSystemInfo;

public:

	CEdit m_editTime;
	CEdit m_editLog;
	CEdit m_editDetailInfo;
};
