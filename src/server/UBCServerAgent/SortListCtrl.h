#pragma once


class CSortListCtrl : public CListCtrl
{
public:
	CSortListCtrl();
	virtual ~CSortListCtrl();

	typedef struct
	{
		HWND	hWnd;			// 리스트컨트롤 핸들
		int		nCol;			// 정렬기준 컬럼
		bool	bAscend;		// true=내림차순, false=오름차순
	} SORT_PARAM;

protected:
	DECLARE_MESSAGE_MAP()

	bool		m_bUseFilter;
	CString		m_strFilter;

	bool		m_bSortable;
	int			m_nSortCol;
	bool		m_bAscend;

	CFont		m_font;
	COLORREF	m_rgbTextColor;
	COLORREF	m_rgbBackColor;
	COLORREF	m_rgbSelectTextColor;
	COLORREF	m_rgbSelectBackColor;

	bool		m_bDisplayLongLength;
	bool		m_bDisplayLongLengthValues[1024];

	static int CALLBACK SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam);

public:
	afx_msg void OnPaint();
	afx_msg BOOL OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);

	BOOL		Sort(int col=-1, bool ascend=true);
	void		SetSortable(bool bSortable=true) { m_bSortable=bSortable; };

	bool		GetUseFilter() { return m_bUseFilter; };
	void		SetUseFilter(bool bUseFilter=true) { m_bUseFilter=bUseFilter; };
	LPCTSTR		GetFilter() { return m_strFilter; };
	void		SetFilter(LPCTSTR lpszFilter=NULL);

	void		SetFont(CFont& font);

	bool		SetDisplayLongLengthColumn(int nCol, bool bDisplay=true);
	afx_msg void OnHdnDividerdblclickListJob(NMHDR *pNMHDR, LRESULT *pResult);
};
