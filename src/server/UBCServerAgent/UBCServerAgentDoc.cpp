// UBCServerAgentDoc.cpp : implementation of the CUBCServerAgentDoc class
//

#include "stdafx.h"
#include "UBCServerAgent.h"

#include "UBCServerAgentDoc.h"

#include "Log.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define		CONSOLE_OUTPUT_BUFFER		(1024 * 1024)		// cmd-output buffer size = 1MB

// CUBCServerAgentDoc

IMPLEMENT_DYNCREATE(CUBCServerAgentDoc, CDocument)

BEGIN_MESSAGE_MAP(CUBCServerAgentDoc, CDocument)
END_MESSAGE_MAP()


// CUBCServerAgentDoc construction/destruction

CUBCServerAgentDoc::CUBCServerAgentDoc()
:	m_strCustomer ( _T("") )
,	m_strServerID ( _T("") )
,	m_strServerIP ( _T("") )
,	m_nMemoryThreshold ( 1024*1024 )	// max memory  = 1 GByte
,	m_nHandleThreshold ( 1000 )			// handle count = 1000
{
	__FUNC_BEGIN__;

	// get customer
	GetPrivateProfileString(_T("ROOT"), _T("ENTERPRISE"), m_strCustomer, _T("..\\config\\data\\UBCServerMonitor2.ini"));
	if(m_strCustomer.GetLength() == 0)
	{
		__WARN__( (_T("[ROOT] ENTERPRISE=NULL")) );
		GetPrivateProfileString(_T("CUSTOMER_INFO"), _T("NAME"), m_strCustomer, _T("..\\config\\data\\UBCVariables.ini"));
		if(m_strCustomer.GetLength() == 0)
		{
			__WARN__( (_T("[CUSTOMER_INFO] NAME=NULL -> UNKNOWN")) );
			m_strCustomer = _T("UNKNOWN");
		}
	}
	__DEBUG__( (_T("Customer=%s"), m_strCustomer) );
	WritePrivateProfileString(_T("UBCServerAgent"), _T("Customer"),	m_strCustomer,	_T(".\\UBCServerAgent.ini"));

	// get server_id
	TCHAR ac[MAX_PATH+1] = {0};
	DWORD len = MAX_PATH;
	if( GetComputerName(ac, &len) == FALSE )
	{
		__WARN__( (_T("GetComputerName fail")) );
		LPCTSTR computer_name = _tgetenv("COMPUTERNAME");
		m_strServerID = ( computer_name!=NULL ? computer_name : _T("UNKNOWN") );
	}
	else
	{
		m_strServerID = ac;
	}
	__DEBUG__( (_T("ServerId=%s"), m_strServerID) );
	WritePrivateProfileString(_T("UBCServerAgent"), _T("ServerId"),	m_strServerID,	_T(".\\UBCServerAgent.ini"));

	// get server_ip
	if( !GetIPAddress(m_strServerIP) )
	{
		__WARN__( (_T("GetIPAddress fail")) );
		LPCTSTR my_ip = _tgetenv("MY_IP");
		m_strServerIP = ( my_ip!=NULL ? my_ip : _T("UNKNOWN") );
	}
	__DEBUG__( (_T("ServerIp=%s"), m_strServerIP) );
	WritePrivateProfileString(_T("UBCServerAgent"), _T("ServerIp"),	m_strServerIP,	_T(".\\UBCServerAgent.ini"));

	// get SLAVE_NAME from UBCServer.ini (NULL or 1,12,34,123...)
	GetPrivateProfileString(_T("ROOT"), _T("SLAVE_NAME"), m_strServerName, _T("..\\UBCServer.ini"));

	__FUNC_END__;
}

CUBCServerAgentDoc::~CUBCServerAgentDoc()
{
	ClearUBCProcessList();
	ClearAllProcessList();
}

BOOL CUBCServerAgentDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CUBCServerAgentDoc serialization

void CUBCServerAgentDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CUBCServerAgentDoc diagnostics

#ifdef _DEBUG
void CUBCServerAgentDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CUBCServerAgentDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CUBCServerAgentDoc commands

void CUBCServerAgentDoc::ClearUBCProcessList()
{
	__FUNC_BEGIN__;
	POSITION pos = m_mapUBCProcess.GetStartPosition();
	CString key;
	while(pos)
	{
		UBCPROCESS_INFO* info = NULL;
		m_mapUBCProcess.GetNextAssoc( pos, key, (void*&)info );

		if(info) delete info;
	}
	m_mapUBCProcess.RemoveAll();
	m_listUBCProcess.RemoveAll();
	__FUNC_END__;
}

void CUBCServerAgentDoc::ClearAllProcessList()
{
	__FUNC_BEGIN__;
	POSITION pos = m_mapAllProcess.GetStartPosition();
	CString key;
	while(pos)
	{
		ALLPROCESS_INFO* info = NULL;
		m_mapAllProcess.GetNextAssoc( pos, key, (void*&)info );

		if(info) delete info;
	}
	m_mapAllProcess.RemoveAll();
	__FUNC_END__;
}

BOOL CUBCServerAgentDoc::GetUBCProcessList()
{
	__FUNC_BEGIN__;

	// APP_LIST, APP_LIST_12, APP_LIST_34 ...
	TCHAR applist_name[1024] = {0};
	if(m_strServerName.GetLength() > 0)
		_stprintf(applist_name, _T("APP_LIST_%s"), m_strServerName);
	else
		_stprintf(applist_name, _T("APP_LIST"));

	// get APP_LIST from UBCStarter.ini (NAMESERV,IREP,IDL2IR,NBGEN...)
	CString str_applist;
	GetPrivateProfileString(_T("ROOT"), applist_name, str_applist, _T("..\\config\\data\\UBCStarter.ini"));

	//
	int pos = 0;
	CString str_app_id = str_applist.Tokenize(",", pos);
	while(str_app_id != "")
	{
		str_app_id.Trim(" ");

		UBCPROCESS_INFO* info = new UBCPROCESS_INFO;
		info->appId = str_app_id;

		//
		GetPrivateProfileString(str_app_id, _T("BINARY_NAME"),	info->binaryName,	_T("..\\config\\data\\UBCStarter.ini"));
		GetPrivateProfileString(str_app_id, _T("ARGUMENT"),		info->argument,		_T("..\\config\\data\\UBCStarter.ini"));
		GetPrivateProfileString(str_app_id, _T("PROPERTIES"),	info->properties,	_T("..\\config\\data\\UBCStarter.ini"));
		GetPrivateProfileString(str_app_id, _T("DEBUG"),		info->debug,		_T("..\\config\\data\\UBCStarter.ini"));
		GetPrivateProfileString(str_app_id, _T("STATUS"),		info->status,		_T("..\\config\\data\\UBCStarter.ini"));
		GetPrivateProfileString(str_app_id, _T("START_TIME"),	info->startUpTime,	_T("..\\config\\data\\UBCStarter.ini")); info->startUpTime.Replace(_T("/"), _T("-"));

		// set 'nameserv' -> object-pointer
		str_app_id.MakeLower();
		m_mapUBCProcess.SetAt(str_app_id, info);
		m_listUBCProcess.Add(info);

		// find next
		str_app_id = str_applist.Tokenize(_T(","), pos);
	}

	// add 'UBCServerAgent' & 'Starter'
	{
		CString str_app_id;
		UBCPROCESS_INFO* info = NULL;

		//
		//str_app_id = _T("UBCServerAgent");

		//info = new UBCPROCESS_INFO;
		//info->appId = str_app_id;

		//info->binaryName = _T("UBCServerAgent.exe");

		//// set 'nameserv' -> object-pointer
		//str_app_id.MakeLower();
		//m_mapUBCProcess.SetAt(str_app_id, info);

		//
		str_app_id = _T("UBCStarter");

		info = new UBCPROCESS_INFO;
		info->appId = str_app_id;

		info->binaryName = _T("UTV_Starter.exe");
		GetPrivateProfileString(_T("UTVStarter"), _T("START_TIME"),	info->startUpTime, _T("1970-01-01 00:00:00"), _T(".\\UBCServerAgent.ini"));

		// set 'nameserv' -> object-pointer
		str_app_id.MakeLower();
		m_mapUBCProcess.SetAt(str_app_id, info);
		m_listUBCProcess.Add(info);
	}

	__FUNC_END__;

	return FALSE;
}

BOOL CUBCServerAgentDoc::GetAllProcessList()
{
	__FUNC_BEGIN__;

	ClearAllProcessList();

	// create pipe
	HANDLE hRead=NULL, hWrite=NULL;
	__DEBUG__(("CreatePipe"));
	if( !CreatePipe(&hRead, &hWrite, 0, CONSOLE_OUTPUT_BUFFER) )
	{
		if(hRead) CloseHandle(hRead);
		if(hWrite) CloseHandle(hWrite);
		return FALSE;
	}
	__DEBUG__(("SetHandleInformation"));
	if( !SetHandleInformation(hWrite, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT) )
	{
		if(hRead) CloseHandle(hRead);
		if(hWrite) CloseHandle(hWrite);
		return FALSE; // 쓰기 핸들은 상속 가능
	}

	// 
	PROCESS_INFORMATION pi = {0};
	STARTUPINFO si = {0};
	si.cb = sizeof(si);
	si.hStdOutput = hWrite;
	si.dwFlags = STARTF_USESTDHANDLES;

	TCHAR wmic_cmd[] = _T("cmd /c wmic process get caption, creationdate, WorkingSetSize, HandleCount");
	__DEBUG__(("CreateProcess(%s)", wmic_cmd));
	if( !CreateProcess(0, wmic_cmd, 0, 0, TRUE, CREATE_NO_WINDOW|CREATE_SUSPENDED, 0, 0, &si, &pi) )
	{
		if(hRead) CloseHandle(hRead);
		if(hWrite) CloseHandle(hWrite);
		if(pi.hProcess) CloseHandle(pi.hProcess);
		if(pi.hThread) CloseHandle(pi.hThread);
		return FALSE;
	}
	::ResumeThread(pi.hThread);

	// wait until end of process, and 5-seconds
	__DEBUG__(("GetExitCodeProcess"));
	int wait_count = 0;
	DWORD exit_code = STILL_ACTIVE;
	while(exit_code == STILL_ACTIVE && wait_count<5000)
	{
		::GetExitCodeProcess(pi.hProcess, &exit_code);
		::Sleep(1);
		wait_count++;
	}

	// get output
	DWORD read = 0;
	TCHAR* buf = new TCHAR[CONSOLE_OUTPUT_BUFFER];
	::ZeroMemory(buf, sizeof(TCHAR)*CONSOLE_OUTPUT_BUFFER);

	__DEBUG__(("ReadFile"));
	ReadFile(hRead, buf, CONSOLE_OUTPUT_BUFFER-1, &read, NULL);
	buf[read] = 0;
	CString str_buffer = buf;
	delete[]buf;
	if(hRead) CloseHandle(hRead);
	if(hWrite) CloseHandle(hWrite);
	if(pi.hProcess) CloseHandle(pi.hProcess);
	if(pi.hThread) CloseHandle(pi.hThread);

	// divide by line
	CStringArray line_list;
	int pos = 0;
	CString line = str_buffer.Tokenize(_T("\r\n"), pos);
	while(line != _T(""))
	{
		line_list.Add(line);
		line = str_buffer.Tokenize(_T("\r\n"), pos);
	}
	if(line_list.GetCount() == 0) return FALSE;

	// get Caption,CreationDate position from first-line.
	const CString& str_header = line_list.GetAt(0);
	int pos_values[4] = {0};
	pos_values[0] = str_header.Find(_T("Caption"));			// binaryname
	pos_values[1] = str_header.Find(_T("CreationDate"));	// start-up time
	pos_values[2] = str_header.Find(_T("WorkingSetSize"));	// memory size
	pos_values[3] = str_header.Find(_T("HandleCount"));		// handle count
	for(int i=0; i<4; i++)
		if(pos_values[i]<0) return FALSE;

	for(int i=1; i<line_list.GetCount(); i++)
	{
		const CString& str_line = line_list.GetAt(i);

		// get binary-name, create-time
		CString str_bin_name	= str_line.Mid(pos_values[0], pos_values[1]-pos_values[0]);
		CString str_create_time	= str_line.Mid(pos_values[1], pos_values[2]-pos_values[1]);
		CString str_mem_size	= str_line.Mid(pos_values[2], pos_values[3]-pos_values[2]);
		CString str_handle_count = str_line.Mid(pos_values[3], str_line.GetLength()-pos_values[3]);

		str_bin_name.Trim(_T(" "));
		str_create_time.Trim(_T(" "));
		str_mem_size.Trim(_T(" "));
		str_handle_count.Trim(_T(" "));

		TCHAR buf_tm[] = "YYYY-MM-DD HH:MM:SS";
		_tcsncpy(buf_tm, (LPCTSTR)str_create_time, 4);
		_tcsncpy(buf_tm+5, (LPCTSTR)str_create_time+4, 2);
		_tcsncpy(buf_tm+8, (LPCTSTR)str_create_time+6, 2);
		_tcsncpy(buf_tm+11, (LPCTSTR)str_create_time+8, 2);
		_tcsncpy(buf_tm+14, (LPCTSTR)str_create_time+10, 2);
		_tcsncpy(buf_tm+17, (LPCTSTR)str_create_time+12, 2);

		ALLPROCESS_INFO* info = NULL;
		if( !m_mapAllProcess.Lookup(str_bin_name.MakeLower(), (void*&)info) )
		{
			// not exist binary-name
			ALLPROCESS_INFO* info =  new ALLPROCESS_INFO;
			info->startUpTime = buf_tm;
			info->memSize = _ttoi(str_mem_size) / 1024;	// KByte
			info->handleCount = _ttoi(str_handle_count);

			m_mapAllProcess.SetAt(str_bin_name, info);
		}
	}

	__DEBUG__(("WriteStartUpTime"));
	WriteStartUpTime();

	__FUNC_END__;

	return TRUE;
}

BOOL CUBCServerAgentDoc::GetUBCProcessList(UBCPROCESS_INFOLIST& info_list)
{
	__FUNC_BEGIN__;

	if( m_mapUBCProcess.GetCount() == 0) GetUBCProcessList();

	int count = m_listUBCProcess.GetCount();
	for(int i=0; i<count; i++)
	{
		UBCPROCESS_INFO* info = m_listUBCProcess.GetAt(i);
		info_list.Add(*info);
	}

	__FUNC_END__;

	return TRUE;
}

BOOL CUBCServerAgentDoc::GetServerProcessList(PUBCPROCESS_INFOLIST& info_list)
{
	__FUNC_BEGIN__;

	info_list.RemoveAll();

	ClearUBCProcessList();
	GetUBCProcessList();
	GetAllProcessList();

	// update startup-time of UBC-process-info from All-process-info
	POSITION pos = m_mapUBCProcess.GetStartPosition();
	CString key;
	while(pos)
	{
		UBCPROCESS_INFO* ubc_info = NULL;
		m_mapUBCProcess.GetNextAssoc( pos, key, (void*&)ubc_info );

		if(ubc_info==NULL) continue;

		CString str_bin_name = ubc_info->binaryName;
		str_bin_name.MakeLower();

		ALLPROCESS_INFO* all_info = NULL;
		// find ubc-process-info from All-process-info
		if( m_mapAllProcess.Lookup(str_bin_name, (void*&)all_info) )
		{
			// find -> update startup-time
			ubc_info->startUpTime = all_info->startUpTime;

			if(all_info->memSize > m_nMemoryThreshold)
				ubc_info->status = UBCPROCESS_STATUS_OVER_MEM;
			else if(all_info->handleCount > m_nHandleThreshold)
				ubc_info->status = UBCPROCESS_STATUS_OVER_HANDLE;
			else
				ubc_info->status = UBCPROCESS_STATUS_ACTIVE;
		}
		else
		{
			// not find -> change status to 'Inactive'
			ubc_info->status = UBCPROCESS_STATUS_INACTIVE;
		}

		// make new-duplicate-object
		UBCPROCESS_INFO* info = new UBCPROCESS_INFO;
		*info = *ubc_info;

		info_list.Add(info);
	}

	__FUNC_END__;

	return TRUE;
}

BOOL CUBCServerAgentDoc::GetIPAddress(CString& strAddr)
{
	__FUNC_BEGIN__;

	// get host ip address

    strAddr = _T("");
	char ac[128] = {0};

    if(gethostname(ac, sizeof(ac)) == -1)
	{
		__WARN__( (_T("gethostname fail")) );
		return FALSE;
	}

    struct hostent* phe = gethostbyname(ac);
    if(phe == 0)
	{
		__WARN__( (_T("gethostbyname fail")) );
		return FALSE;
	}

    if(phe->h_addr_list[0] != 0)
	{
        struct in_addr addr;
        memcpy(&addr, phe->h_addr_list[0], sizeof(struct in_addr));
        strAddr = inet_ntoa(addr);
    }

	__FUNC_END__;

    return TRUE;
}

BOOL CUBCServerAgentDoc::WriteStartUpTime()
{
	BOOL ret_val = TRUE;

	// get start-up-time of UBCStarter
	CString str_bin_name = _T("UTV_starter.exe");
	ALLPROCESS_INFO* info = NULL;
	if( m_mapAllProcess.Lookup(str_bin_name.MakeLower(), (void*&)info) && info != NULL)
	{
		WritePrivateProfileString(_T("UTVStarter"), _T("START_TIME"),	info->startUpTime,	_T(".\\UBCServerAgent.ini"));
	}
	else
		ret_val = FALSE;

	// get start-up-time of UBCServerAgent
	str_bin_name = _T("UBCServerAgent.exe");
	info = NULL;
	if( m_mapAllProcess.Lookup(str_bin_name.MakeLower(), (void*&)info) && info != NULL)
	{
		WritePrivateProfileString(_T("UBCServerAgent"), _T("START_TIME"),	info->startUpTime,	_T(".\\UBCServerAgent.ini"));
	}
	else
		ret_val = FALSE;

	return ret_val;
}
