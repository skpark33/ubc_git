// CheckListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "SortListCtrl.h"

CSortListCtrl::CSortListCtrl()
:	m_bSortable ( true )
,	m_nSortCol ( 0 )
,	m_bAscend ( false )
,	m_bUseFilter ( false )
,	m_strFilter ( _T("") )
{
	::ZeroMemory(m_bDisplayLongLengthValues, sizeof(m_bDisplayLongLengthValues));

	m_rgbTextColor = ::GetSysColor(COLOR_WINDOWTEXT);//RGB(0,0,0);
	m_rgbBackColor = ::GetSysColor(COLOR_WINDOW);//RGB(255,255,255);

	m_rgbSelectTextColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT);
	m_rgbSelectBackColor = ::GetSysColor(COLOR_HIGHLIGHT);
}

CSortListCtrl::~CSortListCtrl()
{
}


BEGIN_MESSAGE_MAP(CSortListCtrl, CListCtrl)
	ON_WM_PAINT()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnNMCustomdraw)
	ON_NOTIFY_REFLECT_EX(LVN_COLUMNCLICK, OnLvnColumnclick)
	ON_NOTIFY(HDN_DIVIDERDBLCLICK, 0, OnHdnDividerdblclickListJob)
END_MESSAGE_MAP()


void CSortListCtrl::OnPaint()
{
//	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CListCtrl::OnPaint()을(를) 호출하지 마십시오.

	const MSG *msg = GetCurrentMessage();
	DefWindowProc( msg->message, msg->wParam, msg->lParam );

	// Draw the lines only for LVS_REPORT mode
	if( (GetStyle() & LVS_TYPEMASK) == LVS_REPORT)
	{
		// Get the number of columns
		CClientDC dc(this);
		//CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
		int nColumnCount = GetHeaderCtrl()->GetItemCount();

		CRect header_rect;
		GetHeaderCtrl()->GetItemRect(0, header_rect);

		CFont* old_font = dc.SelectObject(&m_font);

		CSize size_pre = dc.GetOutputTextExtent(_T("0"), 1);
		CSize size_dot = dc.GetOutputTextExtent(_T("..."), 3);

		for( int col=0; col<nColumnCount && col<1024; col++ )
		{
			if( !m_bDisplayLongLengthValues[col] ) continue;

			int count = GetItemCount();
			for( int idx=0; idx<count; idx++ )
			{
				CString str = GetItemText(idx, col);
				if(str.GetLength() < 260) continue;
				UINT state = GetItemState(idx, LVIS_SELECTED);

				CRect rect;
				if( GetSubItemRect(idx, col, LVIR_LABEL, rect) )
				{
					if(rect.top < header_rect.bottom) rect.top = header_rect.bottom;
					if(rect.bottom < header_rect.bottom) rect.bottom = header_rect.bottom;

					rect.DeflateRect(1, 0);
					if(state & LVIS_SELECTED)
					{
						//dc.FillSolidRect(rect, m_rgbSelectBackColor);
						dc.SetTextColor(m_rgbSelectTextColor);
						dc.SetBkColor(m_rgbSelectBackColor);
					}
					else
					{
						//dc.FillSolidRect(rect, m_rgbBackColor);
						dc.SetTextColor(m_rgbTextColor);
						dc.SetBkColor(m_rgbBackColor);
					}
					rect.left += size_pre.cx-1;
					dc.DrawText(str, rect, DT_MODIFYSTRING | DT_LEFT | DT_VCENTER | DT_SINGLELINE );

					CSize size_str = dc.GetOutputTextExtent(str);
					if(rect.Width() < size_str.cx)
					{
						rect.left = rect.right - size_dot.cx;
						dc.DrawText(_T("..."), 3, rect, DT_MODIFYSTRING | DT_LEFT | DT_VCENTER | DT_SINGLELINE );
					}
				}
			}
		}

		dc.SelectObject(old_font);
	}
}

void CSortListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
	int idx = static_cast<int>( pLVCD->nmcd.dwItemSpec );

	switch(pLVCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		break;

	case CDDS_ITEMPREPAINT:
		if(m_bUseFilter)
		{
			CString str = GetItemText(idx, 0);
			str.MakeLower();

			if(str.Find(m_strFilter) >= 0)
			{
				pLVCD->clrText   = RGB(0,0,0);
				pLVCD->clrTextBk = RGB(255,255,0);

				*pResult = CDRF_NOTIFYITEMDRAW;
			}
			else
				*pResult = CDRF_DODEFAULT;
		}
		else
		{
			*pResult = CDRF_DODEFAULT;
		}
		break;

	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		//if(m_bDisplayLongLength && m_bDisplayLongLengthValues[pLVCD->iSubItem])
		//{
		//	pLVCD->clrText   = m_rgbSelectTextColor;
		//	pLVCD->clrTextBk = m_rgbSelectBackColor;
		//	*pResult = CDRF_NEWFONT;
		//}
		//else
			*pResult = CDRF_NEWFONT;
		break;

	default:
		*pResult = CDRF_DODEFAULT;
		break;
	}
}


BOOL CSortListCtrl::OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int	nSortCol = pNMLV->iSubItem;

	return Sort(nSortCol, (m_nSortCol != nSortCol) ? true : !m_bAscend);
}

int CALLBACK CSortListCtrl::SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam)
{
	SORT_PARAM*	ptrSortParam = (SORT_PARAM *)lSortParam;
	CListCtrl*	pListCtrl = (CListCtrl *)CWnd::FromHandle(ptrSortParam->hWnd);

	LVFINDINFO	lvFind;
	lvFind.flags = LVFI_PARAM;
	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	CString strItem1 = pListCtrl->GetItemText(nIndex1, ptrSortParam->nCol);
	CString	strItem2 = pListCtrl->GetItemText(nIndex2, ptrSortParam->nCol);

	if (ptrSortParam->bAscend == true)
		return strItem1.CompareNoCase(strItem2);
	else
		return strItem2.CompareNoCase(strItem1);
}

BOOL CSortListCtrl::Sort(int col, bool ascend)
{
	if( !m_bSortable ) return TRUE;

	/*
	if(col<0)
	{
	col = m_nSortCol;
	ascend = m_bAscend;
	}
	else
	{
	m_nSortCol = col;
	m_bAscend = ascend;
	}

	//
	SORT_PARAM	sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = col;
	sort_param.bAscend = ascend;

	return SortItems(&SortColumn, (LPARAM)&sort_param);
	*/

	CHeaderCtrl* header = GetHeaderCtrl();

	// 정렬표시 해제
	HD_ITEM hditem;
	hditem.mask = HDI_FORMAT;
	header->GetItem( m_nSortCol, &hditem );
	hditem.fmt &= ~(HDF_SORTDOWN | HDF_SORTUP);
	header->SetItem( m_nSortCol, &hditem );

	// 정렬한다.
	SORT_PARAM sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = col;
	sort_param.bAscend = ascend;

	// 현재 정렬기준 컬럼과 정렬모드를 저장한다.
	m_nSortCol = col;
	m_bAscend = sort_param.bAscend;

	BOOL ret_val = SortItems(&SortColumn, (LPARAM)&sort_param);

	// 정렬표시 설정
	header->GetItem( m_nSortCol, &hditem );

	hditem.fmt &= ~(HDF_SORTDOWN | HDF_SORTUP);
	hditem.fmt |= (m_bAscend ? HDF_SORTUP : HDF_SORTDOWN );

	header->SetItem( m_nSortCol, &hditem );

	return ret_val;
}

void CSortListCtrl::SetFilter(LPCTSTR lpszFilter)
{
	if(lpszFilter == NULL || _tcslen(lpszFilter) == 0)
	{
		m_bUseFilter = false;
		m_strFilter = _T("");
	}

	m_bUseFilter = true;
	m_strFilter = lpszFilter;
}

void CSortListCtrl::SetFont(CFont& font)
{
	LOGFONT log_font = {0};
	font.GetLogFont(&log_font);

	//m_font.CreatePointFont(9*10, _T("Gulimche"));
	BOOL ret = m_font.CreateFontIndirect(&log_font);
	CListCtrl::SetFont(&m_font);
}

bool CSortListCtrl::SetDisplayLongLengthColumn(int nCol, bool bDisplay)
{
	if(nCol >= 1024) return false;
	m_bDisplayLongLengthValues[nCol] = bDisplay;

	m_bDisplayLongLength = false;
	for(int i=0; i<1024; i++)
		if(m_bDisplayLongLengthValues[i])
		{
			m_bDisplayLongLength = true;
			break;
		}

	return true;
}

void CSortListCtrl::OnHdnDividerdblclickListJob(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);

	if(m_bDisplayLongLength && m_bDisplayLongLengthValues[phdr->iItem])
	{
		CPaintDC dc(this); // device context for painting
		CFont* old_font = dc.SelectObject(&m_font);

		int width = 0;

		int count = GetItemCount();
		for(int i=0; i<count; i++)
		{
			CString str = GetItemText(i, phdr->iItem);

			CSize size_str = dc.GetOutputTextExtent(str);
			if(size_str.cx > width)
				width = size_str.cx;
		}
		SetColumnWidth(phdr->iItem, width+6+2);

		dc.SelectObject(&old_font);
	}
	else
		SetColumnWidth(phdr->iItem, LVSCW_AUTOSIZE);

	*pResult = 0;
}
