// UBCServerAgent.h : main header file for the UBCServerAgent application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CUBCServerAgentApp:
// See UBCServerAgent.cpp for the implementation of this class
//

class CUBCServerAgentApp : public CWinApp
{
public:
	CUBCServerAgentApp();

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	afx_msg void OnAppAbout();

	DECLARE_MESSAGE_MAP()

	BOOL	CheckSingletonProcess();

public:
	static HANDLE	m_hSingletonMutex;

	static void	CheckLogFileDate();
};

extern CUBCServerAgentApp theApp;