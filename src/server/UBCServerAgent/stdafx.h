// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <afxsock.h>		// MFC socket extensions
#include <afxtempl.h>
#include <afxmt.h>



//#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
//#endif


BOOL	GetPrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, CString& strReturnedString, LPCTSTR lpFileName);
BOOL	GetPrivateProfileString(LPCTSTR lpAppName, LPCTSTR lpKeyName, CString& strReturnedString, LPCTSTR lpDefault, LPCTSTR lpFileName);


#define		UBCPROCESS_COMMAND_INIT			_T("INIT")
#define		UBCPROCESS_COMMAND_STARTING		_T("Starting")
#define		UBCPROCESS_COMMAND_STARTED		_T("Started")
#define		UBCPROCESS_COMMAND_STOPPING		_T("Stopping")
#define		UBCPROCESS_COMMAND_STOPPED		_T("Stopped")

#define		UBCPROCESS_STATUS_ACTIVE		_T("Active")
#define		UBCPROCESS_STATUS_INACTIVE		_T("Inactive")
#define		UBCPROCESS_STATUS_OVER_MEM		_T("Over Mem.")
#define		UBCPROCESS_STATUS_OVER_HANDLE	_T("Over Handle")

#define		UBCSERVER_STATUS_NORMAL			_T("Normal")
#define		UBCSERVER_STATUS_NO_STARTER		_T("NoStarter")
#define		UBCSERVER_STATUS_NO_AGENT		_T("NoAgent")
#define		UBCSERVER_STATUS_NO_SESSION		_T("NoSession")

//
typedef struct {
	CString		appId;			// 어플리케이션ID
	CString		binaryName;		// 실행 파일 이름
	CString		argument;		// 입력 인수
	CString		properties;		// CORBA 인수
	CString		debug;			// 디버깅 인수
	CString		status;			// 어플리케이션 상태
	CString		startUpTime;	// 기동 시각
	CString		command;		// UBCPROCESS_INFO_STARTING / UBCPROCESS_INFO_STOPPING
} UBCPROCESS_INFO;

typedef		CArray<UBCPROCESS_INFO*, UBCPROCESS_INFO*>		PUBCPROCESS_INFOLIST;
typedef		CArray<UBCPROCESS_INFO, UBCPROCESS_INFO&>		UBCPROCESS_INFOLIST;
typedef		CMapStringToPtr		UBCPROCESS_INFOMAP;

void	DuplicateProcessList(PUBCPROCESS_INFOLIST& src_list, UBCPROCESS_INFOLIST& tgt_list);
void	DuplicateProcessList(UBCPROCESS_INFOLIST& src_list, UBCPROCESS_INFOLIST& tgt_list);

//
typedef struct {
	CString		startUpTime;
	int			memSize;		// KByte
	int			handleCount;
} ALLPROCESS_INFO;

typedef		CArray<ALLPROCESS_INFO*, ALLPROCESS_INFO*>		ALLPROCESS_INFOLIST;
typedef		CMapStringToPtr		ALLPROCESS_INFOMAP;


#define		ID_ICON_NOTIFY							(9999)
#define		ID_ICON_NOTIFY_CLICK					(9998)

#define		WM_POST_SYSTEM_LOG						(9997)
#define		WM_COMPLETE_INIT_UBC_PROCESS_STATUS		(9996)
#define		WM_COMPLETE_SEND_UBC_PROCESS_STATUS		(9995)
#define		WM_COMPLETE_CHECK_UBC_PROCESS_COMMAND	(9994)

#define		CURRENT_TIME_STRING				(CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S"))

#define		POST_SYSTEM_LOG(HPWND,LOG,DETAIL) \
					{ \
						CString* TMP_LOG = new CString(LOG); \
						CString* TMP_DETAIL = new CString(DETAIL); \
						::PostMessage(HPWND, WM_POST_SYSTEM_LOG, (WPARAM)TMP_LOG, (LPARAM)TMP_DETAIL); \
					}

#define		POST_COMPLETE_INIT_UBC_PROCESS_STATUS(HPWND,ID) \
					::PostMessage(HPWND, WM_COMPLETE_INIT_UBC_PROCESS_STATUS, (WPARAM)ID, (LPARAM)1);

#define		POST_COMPLETE_SEND_UBC_PROCESS_STATUS(HPWND,ID) \
					::PostMessage(HPWND, WM_COMPLETE_SEND_UBC_PROCESS_STATUS, (WPARAM)ID, (LPARAM)1);

#define		POST_COMPLETE_CHECK_UBC_PROCESS_COMMAND(HPWND,ID) \
					::PostMessage(HPWND, WM_COMPLETE_CHECK_UBC_PROCESS_COMMAND, (WPARAM)ID, (LPARAM)1);


class CLockObject
{
public:
	CLockObject(CCriticalSection& lock) : m_cs(&lock), m_mutex(NULL), m_bAlreadyUnlock(false) { m_cs->Lock(); };
	CLockObject(CMutex& lock) : m_cs(NULL), m_mutex(&lock), m_bAlreadyUnlock(false) { m_mutex->Lock(); };

	~CLockObject() { Unlock(); };

	void Unlock()
	{
		if(!m_bAlreadyUnlock)
		{
			if(m_mutex) m_mutex->Unlock();
			if(m_cs) m_cs->Unlock();
		}
		m_bAlreadyUnlock = true;
	};

protected:
	CMutex*				m_mutex;
	CCriticalSection*	m_cs;
	bool				m_bAlreadyUnlock;
};

#define		LOCK(x)		CLockObject __lock__(x);

DWORD GetTickDiff(DWORD dwStart, DWORD dwEnd);


typedef struct
{
	CString strTime;
	CString strLog;
	CString strDetailInfo;
} SYSTEMLOG_INFO;


LPCTSTR GetLastErrorString(DWORD err_code);
