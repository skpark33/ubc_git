// UBCServerAgentView.cpp : implementation of the CUBCServerAgentView class
//

#include "stdafx.h"
#include "UBCServerAgent.h"

#include "UBCServerAgentDoc.h"
#include "UBCServerAgentView.h"

#include "HttpRequest.h"

#include "SystemLogDialog.h"

#include "Log.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		MAX_LOG_COUNT							(10000)


#define		TIMER_LOG_FILE_CHANGE_ID				(1024)
#define		TIMER_LOG_FILE_CHANGE_TIME				(60*1000)	// 1-min

#define		INIT_UBC_PROCESS_STATUS_TIMER_ID		(1025)
#define		INIT_UBC_PROCESS_STATUS_TIMER_TIME		(60*1000)	// 1-min

#define		SEND_UBC_PROCESS_STATUS_TIMER_ID		(1026)
#define		SEND_UBC_PROCESS_STATUS_TIMER_TIME		(60*1000)	// 1-min

#define		CHECK_UBC_PROCESS_STATUS_TIMER_ID		(1027)
#define		CHECK_UBC_PROCESS_STATUS_TIMER_TIME		(60*1000)	// 1-min


CCriticalSection CUBCServerAgentView::m_csCheckThread;
int CUBCServerAgentView::m_nCurrentCheckThreadID = -1;
//BOOL CUBCServerAgentView::m_bCheckStatusResult = FALSE;


// CUBCServerAgentView

IMPLEMENT_DYNCREATE(CUBCServerAgentView, CFormView)

BEGIN_MESSAGE_MAP(CUBCServerAgentView, CFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SYSTEM_LOG, &CUBCServerAgentView::OnNMDblclkListSystemLog)
	ON_MESSAGE(WM_POST_SYSTEM_LOG, OnPostSystemLog)
	ON_MESSAGE(WM_COMPLETE_INIT_UBC_PROCESS_STATUS, OnCompleteInitUBCProcessStatus)
	ON_MESSAGE(WM_COMPLETE_SEND_UBC_PROCESS_STATUS, OnCompleteSendUBCProcessStatus)
	ON_MESSAGE(WM_COMPLETE_CHECK_UBC_PROCESS_COMMAND, OnCompleteCheckUBCProcessCommand)
END_MESSAGE_MAP()

// CUBCServerAgentView construction/destruction

CUBCServerAgentView::CUBCServerAgentView()
	: CFormView(CUBCServerAgentView::IDD)
{
	m_nCurrentStatusThreadID = -1;
	m_bInitStatusResult = FALSE;
	m_bSendStatusResult = TRUE;
}

CUBCServerAgentView::~CUBCServerAgentView()
{
}

void CUBCServerAgentView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PROCESS, m_lcProcess);
	DDX_Control(pDX, IDC_LIST_SYSTEM_LOG, m_lcSystemLog);
	DDX_Control(pDX, IDC_EDIT_CUSTOMER, m_editCustomer);
	DDX_Control(pDX, IDC_EDIT_SERVER_ID, m_editServerID);
	DDX_Control(pDX, IDC_EDIT_SERVER_IP, m_editServerIP);
}

BOOL CUBCServerAgentView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CUBCServerAgentView::OnInitialUpdate()
{
	__FUNC_BEGIN__;

	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
	SetScrollSizes(MM_TEXT, CSize(1,1));

	__DEBUG__( (_T("Init ListCtrl")) );
	// init process-listctrl
	m_lcProcess.SetExtendedStyle(m_lcProcess.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_lcProcess.InsertColumn(COL_APPID,			_T("App ID"), 0, 150);
	m_lcProcess.InsertColumn(COL_BINARY_NAME,	_T("Binary Name"), 0, 150);
	m_lcProcess.InsertColumn(COL_STATUS,		_T("Status"), 0, 60);
	m_lcProcess.InsertColumn(COL_START_TIME,	_T("Startup Time"), 0, 120);
	m_lcProcess.InsertColumn(COL_ARGUMENT,		_T("argument"), 0, 150);
	m_lcProcess.InsertColumn(COL_PROPERTIES,	_T("properties"), 0, 150);
	m_lcProcess.InsertColumn(COL_DEBUG,			_T("debug"), 0, 150);

	// init systemlog-listctrl
	m_lcSystemLog.SetExtendedStyle(m_lcSystemLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );
	m_lcSystemLog.InsertColumn(SYS_COL_TIME, "Time", 0, 120);
	m_lcSystemLog.InsertColumn(SYS_COL_STRING, "System Log", 0, 400);

	// get customer, server_id, server_ip
	CString str_customer, str_server_id, str_server_ip;
	GetDocument()->GetServerInfo(str_customer, str_server_id, str_server_ip);
	__DEBUG__( (_T("Customer : %s"), str_customer) );
	__DEBUG__( (_T("ServerId : %s"), str_server_id) );
	__DEBUG__( (_T("ServerIp : %s"), str_server_ip) );

	m_editCustomer.SetWindowText(str_customer);
	m_editServerID.SetWindowText(str_server_id);
	m_editServerIP.SetWindowText(str_server_ip);

	// set title
	CString title;
	title.LoadString(AFX_IDS_APP_TITLE);
	::AfxGetMainWnd()->SetWindowText(title);

	//
	CString msg;
	msg.Format(_T("Customer : %s\r\nServer ID : %s\r\nServer IP : %s"), str_customer, str_server_id, str_server_ip);
	POST_SYSTEM_LOG(GetSafeHwnd(), _T("Info : Success to get default info."), msg );

	// init timer & log & singleton
	__DEBUG__( (_T("Init Timer")) );
	SetTimer(INIT_UBC_PROCESS_STATUS_TIMER_ID, 500, NULL);
	SetTimer(TIMER_LOG_FILE_CHANGE_ID, TIMER_LOG_FILE_CHANGE_TIME, NULL);

	if( !__IS_LOG_OPEN__() )
	{
		CString detail_info = _T("Log filename : ");
		detail_info += __GET_LOG_NAME__();

		POST_SYSTEM_LOG(GetSafeHwnd(), _T("Warn : Fail to create LOG-file."), detail_info );
	}

	if( CUBCServerAgentApp::m_hSingletonMutex == NULL )
	{
		POST_SYSTEM_LOG(GetSafeHwnd(), _T("Warn : Fail to create Singleton-Process-Safeguard"), _T("CreateMutex Fail") );
	}

	__FUNC_END__;
}


// CUBCServerAgentView diagnostics

#ifdef _DEBUG
void CUBCServerAgentView::AssertValid() const
{
	CFormView::AssertValid();
}

void CUBCServerAgentView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CUBCServerAgentDoc* CUBCServerAgentView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCServerAgentDoc)));
	return (CUBCServerAgentDoc*)m_pDocument;
}
#endif //_DEBUG


// CUBCServerAgentView message handlers

void CUBCServerAgentView::OnDestroy()
{
	__FUNC_BEGIN__;

	CFormView::OnDestroy();

	__DEBUG__( (_T("Destroy ProcessList")) );
	ClearProcessList();

	__DEBUG__( (_T("Destroy SystemLog")) );
	ClearSystemLogList();

	__FUNC_END__;
}

void CUBCServerAgentView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	int system_log_height = cy/4;

	if(GetSafeHwnd() && m_lcProcess.GetSafeHwnd() && m_lcSystemLog.GetSafeHwnd())
	{
		m_lcProcess.MoveWindow(0, 20, cx, cy-20-system_log_height, FALSE);
		m_lcSystemLog.MoveWindow(0, cy-system_log_height, cx, system_log_height, FALSE);
	}
}

void CUBCServerAgentView::OnTimer(UINT_PTR nIDEvent)
{
	__FUNC_BEGIN__;
	__DEBUG__( (_T("Timer ID=%d"), nIDEvent) );

	switch(nIDEvent)
	{
	case TIMER_LOG_FILE_CHANGE_ID:
		KillTimer(TIMER_LOG_FILE_CHANGE_ID);

		CUBCServerAgentApp::CheckLogFileDate();

		SetTimer(TIMER_LOG_FILE_CHANGE_ID, TIMER_LOG_FILE_CHANGE_TIME, NULL);
		break;


	case INIT_UBC_PROCESS_STATUS_TIMER_ID:
		__DEBUG__( (_T("INIT_UBC_PROCESS_STATUS timer")) );

		KillTimer(INIT_UBC_PROCESS_STATUS_TIMER_ID);

		if(m_bInitStatusResult == TRUE)
		{
			// success to init --> send status
			__DEBUG__( (_T("success to init -> set timer SEND_UBC_PROCESS_STATUS")) );
			SetTimer(SEND_UBC_PROCESS_STATUS_TIMER_ID, 100, NULL);
			SetTimer(CHECK_UBC_PROCESS_STATUS_TIMER_ID, CHECK_UBC_PROCESS_STATUS_TIMER_TIME, NULL);
			break;
		}

		if(m_nCurrentStatusThreadID >= 0)
		{
			__WARN__( (_T("delayed to init status (current_thread_id=%d)"), m_nCurrentStatusThreadID) );

			TCHAR buf[32];
			_stprintf(buf, _T("Thread ID : %d"), m_nCurrentStatusThreadID);

			POST_SYSTEM_LOG(GetSafeHwnd(), _T("Warn : Being delayed to init status"), buf);
		}

		// create init-status-thread
		__DEBUG__( (_T("create init-status-thread")) );
		{
			THREAD_PARAM* param = new THREAD_PARAM;
			param->hParentHwnd = GetSafeHwnd();
			param->nThreadID = ++m_nCurrentStatusThreadID;
			GetDocument()->GetServerInfo(param->strCustomer, param->strServerId, param->strServerIp);
			param->pSPList = NULL;

			CWinThread* pThread = ::AfxBeginThread(InitServerProcessStatusThread, (LPVOID)param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
			pThread->m_bAutoDelete = TRUE;
			pThread->ResumeThread();
		}

		__DEBUG__( (_T("reset INIT_UBC_PROCESS_STATUS timer")) );
		SetTimer(INIT_UBC_PROCESS_STATUS_TIMER_ID, INIT_UBC_PROCESS_STATUS_TIMER_TIME, NULL);
		break;


	case SEND_UBC_PROCESS_STATUS_TIMER_ID:
		__DEBUG__( (_T("SEND_UBC_PROCESS_STATUS timer")) );

		KillTimer(SEND_UBC_PROCESS_STATUS_TIMER_ID);

		if(m_bSendStatusResult == FALSE)
		{
			__WARN__( (_T("fail to send status (current_thread_id=%d)"), m_nCurrentStatusThreadID) );

			// send-status-result is FALSE --> fail to send status
			TCHAR buf[32];
			_stprintf(buf, _T("Thread ID : %d"), m_nCurrentStatusThreadID);

			POST_SYSTEM_LOG(GetSafeHwnd(), _T("Warn : Fail to send status"), buf);
		}

		{
			CUBCServerAgentDoc* pDoc = GetDocument();
			// update ubc-process-list
			__DEBUG__( (_T("GetServerProcessList")) );

			PUBCPROCESS_INFOLIST process_list;
			pDoc->GetServerProcessList( process_list );

			// listup ubc-process-list
			m_lcProcess.SetRedraw(FALSE);
			ClearProcessList();
			for(int i=0; i<process_list.GetCount(); i++)
			{
				UBCPROCESS_INFO* info = process_list.GetAt(i);

				m_lcProcess.InsertItem(i, _T(""));
				m_lcProcess.SetItemText(i, COL_APPID, info->appId);
				m_lcProcess.SetItemText(i, COL_BINARY_NAME, info->binaryName);
				m_lcProcess.SetItemText(i, COL_STATUS, info->status);
				m_lcProcess.SetItemText(i, COL_START_TIME, info->startUpTime);
				m_lcProcess.SetItemText(i, COL_ARGUMENT, info->argument);
				m_lcProcess.SetItemText(i, COL_PROPERTIES, info->properties);
				m_lcProcess.SetItemText(i, COL_DEBUG, info->debug);

				m_lcProcess.SetItemData(i, (DWORD)info);
			}
			m_lcProcess.Sort(0);
			m_lcProcess.SetRedraw(TRUE);

			// create send-status-thread
			__DEBUG__( (_T("create send-status-thread")) );

			UBCPROCESS_INFOLIST* duplicate_list = new UBCPROCESS_INFOLIST;
			DuplicateProcessList(process_list, *duplicate_list);

			THREAD_PARAM* param = new THREAD_PARAM;
			param->hParentHwnd = GetSafeHwnd();
			param->nThreadID = ++m_nCurrentStatusThreadID;
			pDoc->GetServerInfo(param->strCustomer, param->strServerId, param->strServerIp);
			param->pSPList = duplicate_list;

			m_bSendStatusResult = FALSE;

			CWinThread* pThread = ::AfxBeginThread(SendServerProcessStatusThread, (LPVOID)param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
			pThread->m_bAutoDelete = TRUE;
			pThread->ResumeThread();
		}

		__DEBUG__( (_T("reset SEND_UBC_PROCESS_STATUS timer")) );
		SetTimer(SEND_UBC_PROCESS_STATUS_TIMER_ID, SEND_UBC_PROCESS_STATUS_TIMER_TIME, NULL);
		break;


	case CHECK_UBC_PROCESS_STATUS_TIMER_ID:
		__DEBUG__( (_T("CHECK_UBC_PROCESS_STATUS timer")) );
		{
			CUBCServerAgentDoc* pDoc = GetDocument();

			CLockObject _lock(m_csCheckThread);

			KillTimer(CHECK_UBC_PROCESS_STATUS_TIMER_ID);

			// create check-status-thread
			__DEBUG__( (_T("create check-status-thread")) );

			UBCPROCESS_INFOLIST* process_list = new UBCPROCESS_INFOLIST;
			pDoc->GetUBCProcessList(*process_list);

			THREAD_PARAM* param = new THREAD_PARAM;
			param->hParentHwnd = GetSafeHwnd();
			param->nThreadID = ++m_nCurrentCheckThreadID;
			pDoc->GetServerInfo(param->strCustomer, param->strServerId, param->strServerIp);
			param->pSPList = process_list;

			CWinThread* pThread = ::AfxBeginThread(CheckServerProcessStatusThread, (LPVOID)param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
			pThread->m_bAutoDelete = TRUE;
			pThread->ResumeThread();

			__DEBUG__( (_T("reset CHECK_UBC_PROCESS_STATUS timer")) );
			SetTimer(CHECK_UBC_PROCESS_STATUS_TIMER_ID, CHECK_UBC_PROCESS_STATUS_TIMER_TIME, NULL);
		}
		break;
	}

	CFormView::OnTimer(nIDEvent);

	__FUNC_END__;
}

void CUBCServerAgentView::OnNMDblclkListSystemLog(NMHDR *pNMHDR, LRESULT *pResult)
{
	__FUNC_BEGIN__;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	int idx = pNMLV->iItem;
	if( idx >= 0 )
	{
		__DEBUG__( (_T("System Detail Log : idx=%d"), idx) );

		SYSTEMLOG_INFO* info = (SYSTEMLOG_INFO*)m_lcSystemLog.GetItemData(idx);
		CSystemLogDialog dlg(info);
		dlg.DoModal();
	}

	*pResult = 0;

	__FUNC_END__;
}

LRESULT CUBCServerAgentView::OnPostSystemLog(WPARAM wParam, LPARAM lParam)
{
	__FUNC_BEGIN__;

	CString* tm_log = (CString*)wParam;
	CString* tm_detailinfo = (CString*)lParam;
	if(tm_log == NULL || tm_detailinfo == NULL)
	{
		__WARN__( (_T("WPARAM or LPARAM is NULL (WPARAM=0x%X, LPARAM=0x%X)"), wParam, lParam) );
		if(tm_log) delete tm_log;
		if(tm_detailinfo) delete tm_detailinfo;
		return 0;
	}

	__DEBUG__( (_T("SystemLog Title : %s"), *tm_log) );
	__DEBUG__( (_T("SystemLog Detail : %s"), *tm_detailinfo) );

	SYSTEMLOG_INFO* new_info = new SYSTEMLOG_INFO;
	new_info->strTime		= CURRENT_TIME_STRING;
	new_info->strLog		= *tm_log;
	new_info->strDetailInfo	= *tm_detailinfo;

	if(tm_log) delete tm_log;
	if(tm_detailinfo) delete tm_detailinfo;

	// check log-count
	m_lcSystemLog.SetRedraw(FALSE);
	int count = m_lcSystemLog.GetItemCount();
	if(count > MAX_LOG_COUNT)
	{
		__DEBUG__( (_T("SystemLog count-limit-over : %d"), count) );
		ClearSystemLogList(MAX_LOG_COUNT/10); // delete all-log-of-10%
	}

	// add new log
	__DEBUG__( (_T("Add new SystemLog")) );
	m_lcSystemLog.InsertItem(0, "" );
	m_lcSystemLog.SetItemText(0, SYS_COL_TIME,		new_info->strTime );
	m_lcSystemLog.SetItemText(0, SYS_COL_STRING,	new_info->strLog );
	m_lcSystemLog.SetItemData(0, (DWORD)new_info );

	m_lcSystemLog.SetRedraw(TRUE);

	__FUNC_END__;

	return 0;
}

LRESULT	CUBCServerAgentView::OnCompleteInitUBCProcessStatus(WPARAM wParam, LPARAM lParam)
{
	int thread_id = wParam;

	__DEBUG__( (_T("return_thread_id=%d, current_thread_id=%d"), thread_id, m_nCurrentStatusThreadID) );

	if(thread_id == m_nCurrentStatusThreadID) m_bInitStatusResult = lParam;

	return 0;
}

LRESULT	CUBCServerAgentView::OnCompleteSendUBCProcessStatus(WPARAM wParam, LPARAM lParam)
{
	int thread_id = wParam;

	__DEBUG__( (_T("return_thread_id=%d, current_thread_id=%d"), thread_id, m_nCurrentStatusThreadID) );

	if(thread_id == m_nCurrentStatusThreadID) m_bSendStatusResult = lParam;

	return 0;
}

LRESULT	CUBCServerAgentView::OnCompleteCheckUBCProcessCommand(WPARAM wParam, LPARAM lParam)
{
	int thread_id = wParam;

	__DEBUG__( (_T("return_thread_id=%d, current_thread_id=%d"), thread_id, m_nCurrentStatusThreadID) );

	if(thread_id == m_nCurrentCheckThreadID) m_bSendStatusResult = lParam;

	return 0;
}

void CUBCServerAgentView::ClearProcessList()
{
	int count = m_lcProcess.GetItemCount();
	for(int i=0; i<count; i++)
	{
		UBCPROCESS_INFO* info = (UBCPROCESS_INFO*)m_lcProcess.GetItemData(i);
		if(info) delete info;
	}
	m_lcProcess.DeleteAllItems();
}

void CUBCServerAgentView::ClearSystemLogList(int nDeleteCount)
{
	bool all_delete = (nDeleteCount==0);

	int count = m_lcSystemLog.GetItemCount();

	int last_id = ( all_delete ? 0 : count-nDeleteCount );
	if(last_id < 0) last_id = 0;

	for(int i=count-1; i>=last_id; i--)
	{
		SYSTEMLOG_INFO* del_info = (SYSTEMLOG_INFO*)m_lcSystemLog.GetItemData(i);
		if( del_info ) delete del_info;
		if( !all_delete ) m_lcSystemLog.DeleteItem(i);
	}
	if( all_delete ) m_lcSystemLog.DeleteAllItems();
}

/*
// TEST URL
http://ubccenter.sqisoft.com:8080/UBC_Server/set_server_process_status.asp?customer=UNKNOWN&serverId=MISEOKJEONG-PC&appId=UBCServerAgent&serverIp=192.168.100.105&binaryName=UTV_Starter.exe&argument=&properties=&debug=&status=Inactive&startUpTime=
http://ubccenter.sqisoft.com:8080/UBC_Server/init_server_process_command_list.asp?customer=UNKNOWN&serverId=MISEOKJEONG-PC
http://ubccenter.sqisoft.com:8080/UBC_Server/get_server_process_command_list.asp?customer=UNKNOWN&serverId=MISEOKJEONG-PC
http://ubccenter.sqisoft.com:8080/UBC_Server/set_server_process_command.asp?customer=UNKNOWN&serverId=MISEOKJEONG-PC&command=Started&appIdCount=2&appIdList=SerialGW|UBCServerAgent
*/

BOOL RequestPost(LPCTSTR url, LPCTSTR send_msg, CString& out_msg, CString& detail_info, CString& request_url, HWND parent_wnd, int thread_id)
{
	__FUNC_BEGIN__;

	CHttpRequest http_request;
	if( !http_request.RequestPost(url, send_msg, out_msg) )
	{
		request_url = http_request.GetRequestURL();

		__WARN__( (_T("Connect fail (thread_id:%d) URL=%s"), thread_id, request_url) );
		__WARN__( (_T("Connect fail (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );
		__WARN__( (_T("Connect fail (thread_id:%d) ErrMsg=%s"), thread_id, http_request.GetErrorMsg()) );

		detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s\r\nError Message : %s"),
							thread_id, request_url, send_msg, http_request.GetErrorMsg());

		return FALSE;
	}

	request_url = http_request.GetRequestURL();

	__FUNC_END__;

	return TRUE;
}

UINT InitServerProcessStatusThread(LPVOID pParam)
{
	__FUNC_BEGIN__;

	THREAD_PARAM* param = (THREAD_PARAM*)pParam;

	//
	HWND	parent_wnd    = param->hParentHwnd;
	int		thread_id     = param->nThreadID;
	CString str_customer  = param->strCustomer;
	CString str_server_id = param->strServerId;
	CString str_server_ip = param->strServerIp;

	delete param;

	//
	if(str_customer.GetLength() == 0 || str_server_id.GetLength() == 0)
	{
		__ERROR__( (_T("Customer or ServerId is NULL (Customer:%s,ServerId:%s)"), str_customer, str_server_id) );

		//POST_COMPLETE_INIT_UBC_PROCESS_STATUS(parent_wnd, thread_id, 0);

		TCHAR buf[32];
		_stprintf(buf, _T("Thread ID : %d"), thread_id);

		POST_SYSTEM_LOG(parent_wnd, _T("Error : CUSTOMER or SERVER_ID is NULL"), buf);
		return FALSE;
	}

	// make url
	CString url = _T("UBC_Server/init_server_process_command_list.asp");

	// make send-message
	CString send_msg;
	send_msg.Format(_T("customer=%s&serverId=%s"), 
		CHttpRequest::ToEncodingString(str_customer),
		CHttpRequest::ToEncodingString(str_server_id) );

	// connect to ubc-center
	CString out_msg = _T("");
	CString detail_info = _T("");
	CString request_url = _T("");
	if( RequestPost(url, send_msg, out_msg, detail_info, request_url, parent_wnd, thread_id) == FALSE)
	{
		//
		//POST_COMPLETE_INIT_UBC_PROCESS_STATUS(parent_wnd, thread_id, 0);

		//
		POST_SYSTEM_LOG(parent_wnd, _T("Warn : Fail to init status"), detail_info);
		return FALSE;
	}

	if(out_msg != _T("OK") && out_msg != _T("Fail"))
	{
		__WARN__( (_T("Invalid result (thread_id:%d) URL=%s"), thread_id, request_url) );
		__WARN__( (_T("Invalid result (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );
		__WARN__( (_T("Invalid result (thread_id:%d) out_msg=%s"), thread_id, out_msg) );

		// invalid html
		//POST_COMPLETE_INIT_UBC_PROCESS_STATUS(parent_wnd, thread_id, 0);

		CString detail_info;
		detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s\r\nResult : \r\n"),
							thread_id, request_url, send_msg );
		detail_info += out_msg;

		POST_SYSTEM_LOG(parent_wnd, _T("Warn : Invalid result in init status"), detail_info);
		return FALSE;
	}

	POST_COMPLETE_INIT_UBC_PROCESS_STATUS(parent_wnd, thread_id);

	POST_SYSTEM_LOG(parent_wnd, _T("Info : Success to init status"), detail_info);

	__FUNC_END__;

	return TRUE;
}

UINT SendServerProcessStatusThread(LPVOID pParam)
{
	__FUNC_BEGIN__;

	THREAD_PARAM* param = (THREAD_PARAM*)pParam;

	//
	HWND	parent_wnd    = param->hParentHwnd;
	int		thread_id     = param->nThreadID;
	CString str_customer  = param->strCustomer;
	CString str_server_id = param->strServerId;
	CString str_server_ip = param->strServerIp;
	UBCPROCESS_INFOLIST* sp_List = param->pSPList;

	// make copy-object
	UBCPROCESS_INFOLIST info_list;
	DuplicateProcessList(*sp_List, info_list);

	delete sp_List;
	delete param;

	// make url
	CString url = _T("UBC_Server/set_server_process_status.asp");

	//
	DWORD start_tick = ::GetTickCount();
	CString out_msg = "";
	CString request_url = _T("");

	for(int i=0; i<info_list.GetCount(); i++)
	{
		UBCPROCESS_INFO& info = info_list.GetAt(i);
		if( info.appId == _T("UBCServerAgent") ) continue;

		// make send-message
		CString send_msg;
		send_msg.Format(_T("customer=%s&serverId=%s&appId=%s&serverIp=%s&binaryName=%s&argument=%s&properties=%s&debug=%s&status=%s&startUpTime=%s"), 
				CHttpRequest::ToEncodingString( str_customer ),
				CHttpRequest::ToEncodingString( str_server_id ),
				CHttpRequest::ToEncodingString( info.appId ),
				CHttpRequest::ToEncodingString( str_server_ip ),
				CHttpRequest::ToEncodingString( info.binaryName ),
				CHttpRequest::ToEncodingString( info.argument ),
				CHttpRequest::ToEncodingString( info.properties ),
				CHttpRequest::ToEncodingString( info.debug ),
				CHttpRequest::ToEncodingString( info.status ),
				CHttpRequest::ToEncodingString( info.startUpTime )
			);

		// check time-out
		DWORD diff_tick = ::GetTickDiff(start_tick, ::GetTickCount());
		if(diff_tick > (60*1000))
		{
			__WARN__( (_T("Timeout (thread_id:%d) URL=%s"), thread_id, url) );
			__WARN__( (_T("Timeout (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );

			CString detail_info;
			detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s"),
								thread_id, request_url, send_msg );

			POST_SYSTEM_LOG(parent_wnd, _T("Warn : Overtime to send status"), detail_info);
			break; // exit after 1-min
		}

		// connect to ubc-center
		CString detail_info = _T("");
		if( RequestPost(url, send_msg, out_msg, detail_info, request_url, parent_wnd, thread_id) == FALSE )
		{
			POST_SYSTEM_LOG(parent_wnd, _T("Warn : Fail to send status"), detail_info);
		}

		if(out_msg != _T("OK"))
		{
			__WARN__( (_T("Invalid result (thread_id:%d) URL=%s"), thread_id, request_url) );
			__WARN__( (_T("Invalid result (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );
			__WARN__( (_T("Invalid result (thread_id:%d) out_msg=%s"), thread_id, out_msg) );

			// invalid html
			CString detail_info;
			detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s\r\nResult : \r\n"),
								thread_id, request_url, send_msg );
			detail_info += out_msg;

			POST_SYSTEM_LOG(parent_wnd, _T("Warn : Invalid result in send status"), detail_info);
		}
	}

	POST_COMPLETE_SEND_UBC_PROCESS_STATUS(parent_wnd, thread_id);

	__FUNC_END__;

	return TRUE;
}

void GetLineList(CString& str_lines, CStringArray& line_list)
{
	// divide by line
	int pos = 0;
	CString line = str_lines.Tokenize(_T("\r\n"), pos);
	while(line != _T(""))
	{
		line_list.Add(line);
		line = str_lines.Tokenize(_T("\r\n"), pos);
	}
}

BOOL GetAppList(CStringArray& line_list, UBCPROCESS_INFOLIST& list_app, HWND parent_wnd, int thread_id)
{
	BOOL ret_val = TRUE;

	//
	CString last_app_id = _T("");
	for(int i=1; i<line_list.GetCount(); i++)
	{
		const CString& line = line_list.GetAt(i);
		if(line.Find(_T('=')) <= 0) continue;

		int pos = 0;
		CString key = line.Tokenize(_T("="), pos);
		CString value = line.Tokenize(_T("="),pos);

		if(key == _T("appId"))
		{
			last_app_id = value;
		}
		else if(key == _T("command"))
		{
			if(last_app_id.GetLength() == 0) continue;

			if(value == UBCPROCESS_COMMAND_STARTING)
			{
				__DEBUG__( (_T("New command (thread_id:%d) %s -> %s"), thread_id, last_app_id, UBCPROCESS_COMMAND_STARTED) );

				UBCPROCESS_INFO info;
				info.appId = last_app_id;
				info.command = UBCPROCESS_COMMAND_STARTED;

				list_app.Add(info);
			}
			else if(value == UBCPROCESS_COMMAND_STOPPING)
			{
				__DEBUG__( (_T("New command (thread_id:%d) %s -> %s"), thread_id, last_app_id, UBCPROCESS_COMMAND_STOPPED) );

				UBCPROCESS_INFO info;
				info.appId = last_app_id;
				info.command = UBCPROCESS_COMMAND_STOPPED;

				list_app.Add(info);
			}
			else
			{
				ret_val = FALSE;
			}
			last_app_id = _T("");
		}
	}

	return ret_val;
}

BOOL ExecuteUBCProcessCommand(HWND parent_wnd, int thread_id, UBCPROCESS_INFO& info)
{
	__FUNC_BEGIN__;

	// execute UBC Process command (start/stop)
	BOOL ret_val = TRUE;

	// normal command to ubc-process
	// set command ok -> run command
	HWND hwnd_starter = ::FindWindow(NULL, _T("UTV Starter"));
	if(hwnd_starter == NULL)
	{
		__WARN__( (_T("Not exist UBC-Starter (thread_id:%d) AppId=%s"), thread_id, info.appId) );
		__WARN__( (_T("Not exist UBC-Starter (thread_id:%d) NewCommand=%s"), thread_id, info.command) );

		// no starter
		CString detail_info;
		detail_info.Format(_T("Thread ID : %d\r\nAppID : %s\r\nNew Command : %s"),
							thread_id, info.appId, info.command );

		POST_SYSTEM_LOG(parent_wnd, _T("Warn : Not exist UBC-Starter"), detail_info);
	}
	else
	{
		TCHAR buf_app_id[1024] = {0};
		::_tcscpy(buf_app_id, info.appId);

		COPYDATASTRUCT cds = {0};
		cds.dwData = (info.command == UBCPROCESS_COMMAND_STARTED ? 40002 : 40003); 
		cds.cbData = (_tcslen(buf_app_id) + 1) * sizeof(TCHAR);
		cds.lpData = (LPVOID)buf_app_id;

		DWORD dwRet = 0;
		if( !::SendMessageTimeout(hwnd_starter, WM_COPYDATA, 0, (LPARAM)&cds, SMTO_ABORTIFHUNG|SMTO_NORMAL, 1000, &dwRet) )
		{
			DWORD err_code = ::GetLastError();

			__WARN__( (_T("SendMessage Error (thread_id:%d) AppId=%s"), thread_id, info.appId) );
			__WARN__( (_T("SendMessage Error (thread_id:%d) NewCommand=%s"), thread_id, info.command) );
			__WARN__( (_T("SendMessage Error (thread_id:%d) ErrorCode=%d(%s)"), thread_id, err_code, GetLastErrorString(err_code)) );

			// sendmessage error
			CString detail_info;
			detail_info.Format(_T("Thread ID : %d\r\nAppID : %s\r\nNew Command : %s\r\nError Code : %d\r\nError String : %s"),
								thread_id, info.appId, info.command, err_code, GetLastErrorString(err_code) );

			POST_SYSTEM_LOG(parent_wnd, _T("Warn : No UBC-Starter"), detail_info);
		}
	}

	__FUNC_END__;

	return ret_val;
}

BOOL ExecuteUBCServerCommand(HWND parent_wnd, int thread_id, UBCPROCESS_INFO& info, UBCPROCESS_INFOLIST& info_list)
{
	// execute UBC Server command (UBC Start/Stop)
	__FUNC_BEGIN__;

	BOOL ret_val = TRUE;

	// UBC Start/Stop
	PROCESS_INFORMATION pi = {0};
	STARTUPINFO si = {0};
	si.cb = sizeof(STARTUPINFO);
	si.wShowWindow = SW_HIDE;			// 콘솔창이 보이지 않도록 한다
	si.dwFlags = STARTF_USESHOWWINDOW;	// 콘솔창이 보이지 않도록 한다

	CString str_execute=_T(""), str_argument=_T("");
	if( info.command == UBCPROCESS_COMMAND_STARTED )
	{
		//str_execute = _T("SelfAutoUpdate.exe");
		//str_argument = _T("+server +log");
		str_argument = _T("SelfAutoUpdate.exe +server +log");
	}
	else if( info.command == UBCPROCESS_COMMAND_STOPPED )
	{
		// all process start(stop)
		for(int i=0; i<info_list.GetCount(); i++)
		{
			UBCPROCESS_INFO& ubc_info = info_list.GetAt(i);

			ubc_info.command = info.command;
			ExecuteUBCProcessCommand(parent_wnd, thread_id, ubc_info);

			// reset timer
			__DEBUG__( (_T("reset CHECK_UBC_PROCESS_STATUS timer(thread_id:%d)"), thread_id) );
			::SetTimer(parent_wnd, CHECK_UBC_PROCESS_STATUS_TIMER_ID, CHECK_UBC_PROCESS_STATUS_TIMER_TIME, NULL);
		}

		//str_execute = _T("C:\\Project\\ubc\\util\\win32\\copRun.bat");
		//str_argument = _T("kill");
		str_argument = _T("C:\\Project\\ubc\\util\\win32\\copRun.bat kill");
	}

	if( !CreateProcess(NULL//(LPTSTR)(LPCTSTR)str_execute
					, (LPTSTR)(LPCTSTR)str_argument
					, NULL
					, NULL
					, FALSE
					, 0
					, NULL
					, (LPTSTR)_T("C:\\Project\\ubc\\bin8")
					, &si
					, &pi ) )
	{
		DWORD err_code = GetLastError();

		__WARN__( (_T("CreateProcess Error (thread_id:%d) Command=%s"), thread_id, str_execute) );
		__WARN__( (_T("CreateProcess Error (thread_id:%d) Argument=%s"), thread_id, str_argument) );
		__WARN__( (_T("CreateProcess Error (thread_id:%d) ErrorCode=%d(%s)"), thread_id, err_code, GetLastErrorString(err_code)) );

		// sendmessage error
		CString detail_info;
		detail_info.Format(_T("Thread ID : %d\r\nCommand : %s\r\nArgument : %s\r\nError Code : %d\r\nError String : %s"),
							thread_id, str_execute, str_argument, err_code, GetLastErrorString(err_code) );

		POST_SYSTEM_LOG(parent_wnd, _T("Warn : UBC Start/Stop Error"), detail_info);
	}

	if(pi.hProcess) CloseHandle(pi.hProcess);
	if(pi.hThread) CloseHandle(pi.hThread);

	__FUNC_END__;
	return ret_val;
}

UINT CheckServerProcessStatusThread(LPVOID pParam)
{
	__FUNC_BEGIN__;

	THREAD_PARAM* param = (THREAD_PARAM*)pParam;

	//
	HWND	parent_wnd    = param->hParentHwnd;
	int		thread_id     = param->nThreadID;
	CString str_customer  = param->strCustomer;
	CString str_server_id = param->strServerId;
	CString str_server_ip = param->strServerIp;
	UBCPROCESS_INFOLIST* sp_List = param->pSPList;

	// make copy-object
	UBCPROCESS_INFOLIST info_list;
	DuplicateProcessList(*sp_List, info_list);

	delete sp_List;
	delete param;

	// make url
	CString url = _T("UBC_Server/get_server_process_command_list.asp");

	// make send-message
	CString send_msg;
	send_msg.Format(_T("customer=%s&serverId=%s&onlyGet=1"), 
		CHttpRequest::ToEncodingString(str_customer),
		CHttpRequest::ToEncodingString(str_server_id) );

	// connect to ubc-center
	CString out_msg = _T("");
	CString detail_info = _T("");
	CString request_url = _T("");
	if( RequestPost(url, send_msg, out_msg, detail_info, request_url, parent_wnd, thread_id) == FALSE )
	{
		//
		POST_COMPLETE_CHECK_UBC_PROCESS_COMMAND(parent_wnd, thread_id);

		//
		POST_SYSTEM_LOG(parent_wnd, _T("Warn : Fail to check status"), detail_info);
		return FALSE;
	}

	// get line-list ( divide by line )
	CStringArray line_list;
	GetLineList(out_msg, line_list);

	//
	if(line_list.GetCount() > 0 && line_list.GetAt(0) == _T("Fail"))
	{
		// not exist new command
		__DEBUG__( (_T("Not exist new command (thread_id:%d)"), thread_id) );

		// check time-out
		CLockObject _lock(CUBCServerAgentView::m_csCheckThread);
		if(CUBCServerAgentView::m_nCurrentCheckThreadID != thread_id)
		{
			__WARN__( (_T("Timeout (thread_id:%d) URL=%s"), thread_id, url) );
			__WARN__( (_T("Timeout (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );

			POST_COMPLETE_CHECK_UBC_PROCESS_COMMAND(parent_wnd, thread_id);

			TCHAR buf[32];
			_stprintf(buf, _T("Thread ID : %d"), thread_id);

			POST_SYSTEM_LOG(parent_wnd, _T("Warn : Being delayed to check status"), buf);
		}
		return FALSE;
	}
	else if(line_list.GetCount() == 0 || line_list.GetAt(0) != _T("OK"))
	{
		__WARN__( (_T("Invalid result (thread_id:%d) URL=%s"), thread_id, request_url) );
		__WARN__( (_T("Invalid result (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );
		__WARN__( (_T("Invalid result (thread_id:%d) out_msg=%s"), thread_id, out_msg) );

		POST_COMPLETE_CHECK_UBC_PROCESS_COMMAND(parent_wnd, thread_id);

		// invalid html
		CString detail_info;
		detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s\r\nResult : \r\n"),
							thread_id, request_url, send_msg );
		detail_info += out_msg;

		POST_SYSTEM_LOG(parent_wnd, _T("Warn : Invalid result in check status"), detail_info);
		return FALSE;
	}

	// reset timer
	::SetTimer(parent_wnd, CHECK_UBC_PROCESS_STATUS_TIMER_ID, CHECK_UBC_PROCESS_STATUS_TIMER_TIME, NULL);

	// parsing result (app_id <--> command)
	UBCPROCESS_INFOLIST list_app;
	if(line_list.GetCount() > 0 && line_list.GetAt(0) == _T("OK"))
	{
		if( GetAppList(line_list, list_app, parent_wnd, thread_id) == FALSE )
		{
			__WARN__( (_T("Invalid command (thread_id:%d) URL=%s"), thread_id, request_url) );
			__WARN__( (_T("Invalid command (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );
			__WARN__( (_T("Invalid command (thread_id:%d) OutMsg=%s"), thread_id, out_msg) );

			// invalid command
			CString detail_info;
			detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s\r\nResult : \r\n"),
								thread_id, request_url, send_msg );
			detail_info += out_msg;

			POST_SYSTEM_LOG(parent_wnd, _T("Warn : Invalid command in check status"), detail_info);
		}
	}

	// set(update) command to server & run command
	for(int i=0; i<list_app.GetCount(); i++)
	{
		UBCPROCESS_INFO& info = list_app.GetAt(i);

		__DEBUG__( (_T("New Command (thread_id:%d) AppId=%s -> %s"), thread_id, info.appId, info.command) );

		// check time-out
		{
			CLockObject _lock(CUBCServerAgentView::m_csCheckThread);
			if(CUBCServerAgentView::m_nCurrentCheckThreadID != thread_id)
			{
				__WARN__( (_T("Timeout (thread_id:%d) AppId=%s"), thread_id, info.appId) );
				__WARN__( (_T("Timeout (thread_id:%d) NewCommand=%s"), thread_id, info.command) );

				CString detail_info;
				detail_info.Format(_T("Thread ID : %d\r\nAppID : %s\r\nNew Command : %s"),
									thread_id, info.appId, info.command );

				POST_SYSTEM_LOG(parent_wnd, _T("Warn : Being delayed to check status"), detail_info);
				break;
			}
		}

		// make url
		CString url = _T("UBC_Server/set_server_process_command.asp");

		// make send-message
		CString send_msg;
		send_msg.Format(_T("customer=%s&serverId=%s&command=%s&appIdCount=1&appIdList=%s"), 
				CHttpRequest::ToEncodingString(str_customer),
				CHttpRequest::ToEncodingString(str_server_id),
				CHttpRequest::ToEncodingString(info.command),
				CHttpRequest::ToEncodingString(info.appId)
			);

		// connect to ubc-center
		CString out_msg = _T("");
		CString detail_info = _T("");
		CString request_url = _T("");
		if( RequestPost(url, send_msg, out_msg, detail_info, request_url, parent_wnd, thread_id) == FALSE )
		{
			//
			POST_COMPLETE_CHECK_UBC_PROCESS_COMMAND(parent_wnd, thread_id);

			//
			POST_SYSTEM_LOG(parent_wnd, _T("Warn : Fail to set command"), detail_info);

			// reset timer
			::SetTimer(parent_wnd, CHECK_UBC_PROCESS_STATUS_TIMER_ID, CHECK_UBC_PROCESS_STATUS_TIMER_TIME, NULL);
			continue;
		}

		if(out_msg == _T("Fail"))
		{
			__WARN__( (_T("Not exist AppId (thread_id:%d) URL=%s"), thread_id, request_url) );
			__WARN__( (_T("Not exist AppId (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );
			__WARN__( (_T("Not exist AppId (thread_id:%d) out_msg=%s"), thread_id, out_msg) );

			// something wrong (not exist app_id)
			CString detail_info;
			detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s\r\nResult : \r\n"),
								thread_id, request_url, send_msg );
			detail_info += out_msg;

			POST_SYSTEM_LOG(parent_wnd, _T("Warn : Not exist AppId"), detail_info);
		}
		else if(out_msg != _T("OK"))
		{
			__WARN__( (_T("Invalid result (thread_id:%d) URL=%s"), thread_id, request_url) );
			__WARN__( (_T("Invalid result (thread_id:%d) SendMsg=%s"), thread_id, send_msg) );
			__WARN__( (_T("Invalid result (thread_id:%d) out_msg=%s"), thread_id, out_msg) );

			// invalid html
			CString detail_info;
			detail_info.Format(_T("Thread ID : %d\r\nURL : %s?%s\r\nResult : \r\n"),
								thread_id, request_url, send_msg );
			detail_info += out_msg;

			POST_SYSTEM_LOG(parent_wnd, _T("Warn : Invalid result in set command"), detail_info);
		}
		else
		{
			if(info.appId == _T("UBCServerAgent") )
			{
				ExecuteUBCServerCommand(parent_wnd, thread_id, info, info_list);
			}
			else
			{
				ExecuteUBCProcessCommand(parent_wnd, thread_id, info);
			}
		}

		// reset timer
		__DEBUG__( (_T("reset CHECK_UBC_PROCESS_STATUS timer(thread_id:%d)"), thread_id) );
		::SetTimer(parent_wnd, CHECK_UBC_PROCESS_STATUS_TIMER_ID, CHECK_UBC_PROCESS_STATUS_TIMER_TIME, NULL);
	}

	POST_COMPLETE_CHECK_UBC_PROCESS_COMMAND(parent_wnd, thread_id);

	__FUNC_END__;

	return TRUE;
}
