/*! \class serviceMonitor
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _serviceMonitor_h_
#define _serviceMonitor_h_


#include <ci/libTimer/ciTimer.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>

class ubcIni;
class dummyEventHandler : public cciEventHandler {
public:
	virtual void 		processEvent(cciEvent& pEvent) {};
};


class testResult {
public:
	testResult() : startTime(0), endTime(0), result(ciFalse), alreadySend(ciFalse) {}
	ciULong		startTime;
	ciULong		endTime;
	ciBoolean	result;
	ciString	debugString;
	ciBoolean   alreadySend;
	
	const char* toString();
protected:
	ciString _toStringBuf;
};
typedef map<ciString, testResult*>		testResultMap;


class serviceMonitor :  public  ciPollable {
public:
	static serviceMonitor*	getInstance();
	static void	clearInstance();
	virtual ~serviceMonitor() ;
	virtual void processExpired(ciString name, int counter, int interval);

	void	doTest(ciString& name);

	ciBoolean	serverTest(ciString& debugString, const char* entity);

	ciBoolean	notiTest(ciString& debugString);
	ciBoolean	timeServerTest(ciString& debugString);
	ciBoolean	pmTest(ciString& debugString, int pmId);
	ciBoolean	agtTest(ciString& debugString, int pmId);
	ciBoolean	odTest(ciString& debugString);
	ciBoolean	dbTest(ciString& debugString);

	int			getError(ciTime& now, int period, testResultMap* outMap);
	ciBoolean	connectTest(int timeout);

	void		setMaxPm(int p) { _maxPm = p; }
	void		setIni(ubcIni* p) { _ini = p; }


	ciBoolean 	killServer(const char* binary);
	void		clearAlreadySend();

protected:
	serviceMonitor();


	static serviceMonitor*	_instance;
	static ciMutex			_instanceLock;


	ciMutex			_lock;
	testResultMap	_map;	

	ubcIni*	_ini;
	int _maxPm;

};	

#endif //_serviceMonitor_h_
