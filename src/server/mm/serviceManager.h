/*! \class serviceManager
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _serviceManager_h_
#define _serviceManager_h_


#include <ci/libTimer/ciTimer.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>
#include <server/mm/serviceMonitor.h>
#include "common/libCommon/ubcIni.h"



class ubcIni;
class cciORBWorld;

class commandResult 
{
public:
	commandResult() { result = -2;}
	ciString hostId;
	ciString hostName;
	ciString ipAddress;
	ciString category;
	int result;

};
typedef list<commandResult>	commandResultList;

class serviceManager :  public  ciPollable {
public:
	//static serviceManager*	getInstance();
	//static void	clearInstance();
	
	serviceManager(ubcIni*	pIni);
	virtual ~serviceManager() ;
	virtual void processExpired(ciString name, int counter, int interval);

	ciBoolean	killServer(const char* binary);
	ciBoolean	filesystemMonitor(const char* strDrv, ciULong& freeK, ciULong& totalK, ciFloat& capacity);

	void	setWorld(cciORBWorld* world) { _world = world; }
	void	setPeriod(int p) { _period = p; }

	ciBoolean	sendTT(const char* kind, const char* receiver,
						const char* title,  ciString& text, ciString& enterprise);

	ubcIni*		getIni() { return _ini;}
	const char*  getEnterprise() { return _enterprise.c_str(); }
	ciStringList*	getMailList() { return &_mailList; }
	
	int			getMaxPm() { return _maxPm; }
	int			getBPMonHour() { return _bpMonHour; }
	

protected:
	void _do_planMonitor(ciString name, int counter, int interval);
	void _do_monitorManager(ciString name, int counter, int interval);
	void _do_eventMonitor(ciString name, int counter, int interval);
	void _do_notiBlockMonitor(ciString name, int counter, int interval);//
	void _do_commander(ciString name, int counter, int interval, ciStringList& timeList, ciString& command, ciString& whereClause);
	void _do_HDDMonitor();

	void	_getLastHeartBeatSendTime(ciString& outValue);
	int	_getServiceCode(ciString& name);

	int	_getCommandData(commandResultList& outList, ciString& whereClause);
	ciBoolean	_isStilSelected(const char* pHostId, ciString& whereClause);
	int _getDeadCount(int& succeed);
	int _getStartFailCount();
	int _getEndFailCount();
	ciBoolean _sendTT(const char* text);
	void	_setZorder();



	//static serviceManager*	_instance;
	//static ciMutex			_instanceLock;
	cciORBWorld*	_world;

	ciShort _period; // minitue
	ciString _enterprise;
	ciShort _deadLimit;
	ciShort _failLimit;
	ciString _testProgram;
	ciShort _maxPm;
	ciShort	_bpMonHour;
	ubcIni*	 _ini;


	ciStringList _mailList;
	ciStringList _telList;
	ciStringList _timeList;
	ciStringList _driveList;
	
	class BATCH_JOB {
	public:
		ciStringList commandTimeList;
		ciString command;
		ciString whereClause;
	};
	typedef list<BATCH_JOB>	BATCH_JOB_LIST;

	BATCH_JOB_LIST	_batchJobList;

	testResultMap* _errMap;
};	



#endif //_serviceManager_h_
