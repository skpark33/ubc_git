#ifndef _serverManager_h_
#define _serverManager_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>

class processInfo {
public:
	processInfo() : state(ciFalse) {}
	ciString id;
	ciString binary;
	ciTime	startTime;
	ciBoolean	state;
};
typedef map<ciString, processInfo*>	ProcessInfoMap;
typedef map<ciString, int>	HDDInfoMap;  // drive, capacity

class serverManager {
public:
	static serverManager*	getInstance();
	static void	clearInstance();

	virtual ~serverManager() ;

	ciBoolean	isMain();
	const char*  getEnterprise() { return _enterprise.c_str(); }
	void		setEnterprise(const char* p) { _enterprise = p; }
	
	const char*  getIpAddress();
	const char*  getHostName();

	int			getCPUUsage();
	int			getMEMUsage();
	const char*	getHDDUsage();
	const char*	getProcessInfo();

	void	clearProcessInfo();

	ProcessInfoMap*		getProcessMap() { return &_map;}
	HDDInfoMap*			getHDDMap()		{ return &_hddmap; }


	ciBoolean sendFault(int probableCause, const char* systemName, const char* serverName,
						const char* title,  const char* text );

	ciBoolean sendHeartBeat( const char* systemName, const char* serverName, ciBoolean isMain,
						const char* hddUsage, const char* processInfo, int cpuUsage,
						int memUsage,  const char* ipAddress );


protected:
	serverManager();

	static serverManager*	_instance;
	static ciMutex			_instanceLock;

	ciBoolean _isProcessAlive(const char* binaryName);

	ciMutex			_lock;
	ProcessInfoMap	_map;
	HDDInfoMap		_hddmap;

	int			_isMain;
	ciString	_enterprise; // system name
	ciString	_ipAddress;
	ciString	_hostName;
	ciString	_hddUsed;
	ciString	_slaveName;
	ciString	_processInfoStr;

};

#endif // _utvUtil_h_
