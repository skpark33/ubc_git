/*! \class serverCommander
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _serverMonitor_h_
#define _serverMonitor_h_


#include <ci/libTimer/ciTimer.h>

#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>


class commandResult {
public:
	commandResult() : result(-1) {}
	int			result;
	ciString	hostId;
	ciString	ipAddress;
	
	const char* toString();
protected:
	ciString _toStringBuf;
};
typedef lsit<commandResult*>		commandResultList;


class serverCommander :  public  ciPollable {
public:
	static serverCommander*	getInstance();
	static void	clearInstance();
	virtual ~serverCommander() ;
	virtual void processExpired(ciString name, int counter, int interval);

	void	doTest(ciString& name);
	void	setIni(ubcIni* p) { _ini = p; }

protected:
	serverCommander();


	static serverCommander*	_instance;
	static ciMutex			_instanceLock;

	ubcIni*	_ini;

};	

#endif //_serverMonitor_h_
