/*! \class serverMonitor
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _serverMonitor_h_
#define _serverMonitor_h_

#include <ci/libTimer/ciTimer.h>

class ubcIni;

class serverMonitor :  public  ciPollable {
public:
	static serverMonitor*	getInstance();
	static void	clearInstance();
	virtual ~serverMonitor() ;
	virtual void processExpired(ciString name, int counter, int interval);

	void	init(ubcIni* ini);
	void	checkThreshold(int cpuUsage, int memUsage);

protected:
	serverMonitor();
	

	static serverMonitor*	_instance;
	static ciMutex			_instanceLock;


	ubcIni*	_ini;


	ciShort			_cpuThresh;
	ciShort			_memThresh;
	ciShort			_hddThresh;


};	

#endif //_serverMonitor_h_
