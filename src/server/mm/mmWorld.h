 /*! \file mmWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _mmWorld_h_
#define _mmWorld_h_

#include <aci/libCall/aciCallWorld.h> 

class serviceManager;
class mmWorld : public aciCallWorld {
public:
	mmWorld();
	virtual ~mmWorld();
 	
	//[
	//	From aciCallWorld
	//
	virtual aciCall*    newCallImpl(const char* pname);
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode=0);
	//
	//]
	ciBoolean isMain();

protected:	
	serviceManager* _mTimer;
	serviceManager* _sTimer;
	serviceManager* _cTimer;
	serviceManager* _eTimer;

	ciString _slaveName;
	
};
		
#endif //_mmWorld_h_
