/*! \class bpMon
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _bpMon_h_
#define _bpMon_h_

#include <ci/libTimer/ciWinTimer.h>
#include "common/libCommon/ubcIni.h"

class bpMonData {
public:
	bpMonData() {
		reportTime = 0;
		reportWay = 0;
		downloadFailLimit = 0;
		testStartTime = 0;
		testEndTime = 0;
	}
	~bpMonData() {}

	ciString bpId;
	ciString programId;
	time_t reportTime;
	ciStringList phone;
	ciStringList email;
	ciShort reportWay;
	ciShort downloadFailLimit;
	ciString testHostId;
	time_t testStartTime;
	time_t testEndTime;
};
typedef map<ciString, bpMonData*>	bpMonDataMap;	

class bpMon :  public  ciWinPollable {
public:
	static bpMon*	getInstance();
	static void	clearInstance();
	virtual ~bpMon() ;
	virtual void processExpired(ciString name, int counter, int interval);

	int	init();
    void clear();
	void setEnterprise(const char* p) { _enterprise = p; }

	bpMonData*  find(const char* id);
	ciBoolean  insert(const char* id, bpMonData* pEle);
	ciBoolean  remove(const char* id);


	ciBoolean	sendTT(const char* kind, const char* receiver,
						const char* title,  ciString& text, ciString& enterprise);

	ciBoolean	createTestBP(bpMonData* pEle);
	ciBoolean	removeTestBP(bpMonData* pEle);

	void		setMaxPm(int p) { _maxPm = p; }
	void		setBPMonHour(int p) { _bpMonHour = p; }

	void		setIni(ubcIni* pIni) { _ini = pIni;}

protected:
	bpMon();
	
	void	_deleteOld();
	void	_clearOld(time_t baseTime);

	ciBoolean	_check_downloadResultSummary(bpMonData* pEle, ciString& smsMsg, ciString& mailMsg);
	ciBoolean	_check_testHostScheduleLog(bpMonData* pEle, ciString& smsMsg, ciString& mailMsg);
	int			_sendTT(bpMonData* pEle, ciString& smsMsg, ciString& mailMsg);
	int			_sendTT(const char* smsMsg, ciString& mailMsg);

	int		_getTH(const char* bpId, ciStringList& thList);
	int		_check_downloadResultSummary(const char* programId, ciStringList& thList, ciStringList& failHostList);

	int			_compare_pm_and_agent();
	int			_get_pm_bp(ciStringSet&	bpMap);
	int			_get_agt_bp(const char* serverId, ciStringSet&	bpMap);

	static bpMon*	_instance;
	static ciMutex			_instanceLock;

	ciMutex			_mapLock;
	bpMonDataMap  _map;
	ciString	_enterprise;

	int			_maxPm;
	int			_bpMonHour;

	ubcIni*	 _ini;

};	

#endif //_bpMon_h_
