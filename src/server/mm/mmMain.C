
#include "mmWorld.h"

int main(int argc, char** argv)
{
    mmWorld* world = new mmWorld();

    if(!world) {
        cerr<<"Unable to create mmWorld"<<endl;
        exit(1);
    }

    if(world->init(argc, argv) == ciFalse) {
        cerr<<"Init world error"<<endl;
        world->fini(2);
    }
    world->run();

    world->fini(0);
	return 0;
}
