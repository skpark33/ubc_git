/*! \class serviceMonitor
 *  Copyright ⓒ 2002, COP. All rights reserved.
 *
 *  \brief Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/14 19:01:00
 */

#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciXProperties.h>

#include <cci/libUtil/cciCorbaMacro.h>
#include <cci/libValue/cciException.h>
#include <cci/libValue/cciInteger16.h>
#include <cci/libValue/cciBoolean.h>
#include <cci/libValue/cciAny.h>

#include <cci/libObject/cciObject.h>
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciCall.h>
#include "common/libCommon/ubcIni.h"
#include <cci/libWrapper/cciEventManager.h>
#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ace/sock_connector.h>




#if defined (_COP_MSC_)
     #include <windows.h>
     #include <atltime.h>
#endif#

#include "serviceMonitor.h"
#include "common/libScratch/scratchUtil.h"


ciSET_DEBUG(10, "serviceMonitor");

serviceMonitor* 	serviceMonitor::_instance = 0; 
ciMutex 	serviceMonitor::_instanceLock;


const char*
testResult::toString()
{
	ciTime from(this->startTime);
	ciTime to(this->endTime);

	_toStringBuf =  "\nStartTime : " + from.getTimeString();
	_toStringBuf += "\nEndTime : " + to.getTimeString();
	_toStringBuf += "\nResult : " + ciString(( this->result ? "Succeed" : "Failed"));
	_toStringBuf += "\nDebugString : " + this->debugString;

	return _toStringBuf.c_str();

}

serviceMonitor*	
serviceMonitor::getInstance() {
	if(!_instance) {
		ciGuard aGuard(_instanceLock);
		if(!_instance) {
			_instance = new serviceMonitor;
		}
	}
	return _instance;
}

void	
serviceMonitor::clearInstance() {
	if(_instance) {
		ciGuard aGuard(_instanceLock);
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}


serviceMonitor::serviceMonitor() 
{
    ciDEBUG(7, ("serviceMonitor()"));
}

serviceMonitor::~serviceMonitor() 
{
    ciDEBUG(7, ("~serviceMonitor()"));

	ciGuard aGuard(_lock);

	testResultMap::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		testResult* ele = itr->second;
		delete itr->second;

	}
	_map.clear();
}
void
serviceMonitor::clearAlreadySend() 
{
    ciDEBUG(7, ("clearAlreadySend()"));

	ciGuard aGuard(_lock);

	testResultMap::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		testResult* ele = itr->second;
		itr->second->alreadySend = ciFalse;

	}
}

void
serviceMonitor::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(7,("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	ciTime now;
	ciDEBUG(1,("now=%s", now.getTimeString().c_str()));

	static int today = 0;
	
	if(now.getDay() != today){
		today = now.getDay();  // 날짜가 바뀌었다.
		//alreadySend 값을 모두 초기화 시켜준다.
		clearAlreadySend();
	}

	//doTest(ciString("NOTI"));
	doTest(ciString("PM"));
	doTest(ciString("OD"));
	doTest(ciString("AGT"));
	doTest(ciString("TIMESERVER"));
	//doTest(ciString("DB"));

	return;
}

void 
serviceMonitor::doTest(ciString& name)
{
	ciTime s_now;
	ciDEBUG(1,("%s STARTTIME=%s", name.c_str(), s_now.getTimeString().c_str()));
	testResult* ele = 0;

	{
		ciGuard aGuard(_lock);
		testResultMap::iterator itr = _map.find(name);	
		if(itr!=_map.end()){
			ele = itr->second;
		}else{
			ele = new testResult();
			_map.insert(testResultMap::value_type(name,ele));
		}
	
		ele->startTime = s_now.getTime();
		_ini->set(name.c_str(), "STARTTIME", s_now.getTimeString().c_str());
	}

	ciBoolean result = ciFalse;
	ciString debugString;

	if(name == "NOTI"){
		result = notiTest(debugString);
	}else if(name == "PM"){
		for(int i=1;i<=_maxPm;i++){
			result = pmTest(debugString,i);
			if(result ==  ciFalse){
				break;
			}
		}
	}else if(name == "TIMESERVER"){
		result = timeServerTest(debugString);
		if(result==ciFalse){
			// 타임서버가 정상이 아니면 리부트한다.
			if(this->killServer("COP_timeServer.exe")){
				ciDEBUG(1,("COP_timeServer.exe kill succeed"));
			}else{
				ciDEBUG(1,("COP_timeServer.exe kill failed"));
			}
		}
	}else if(name == "AGT"){
		for(int i=1;i<=_maxPm;i++){
			result = agtTest(debugString,i);
			if(result ==  ciFalse){
				break;
			}
		}
	}else if(name == "OD"){
		result = odTest(debugString);
	}else if(name == "DB"){
		result = dbTest(debugString);
	}

	{
		ciGuard aGuard(_lock);
	
		ele->result = result;
		ele->debugString = debugString;

		ciTime e_now;
		ele->endTime = e_now.getTime();

		ciDEBUG(1,("%s ENDTIME=%s", name.c_str(), e_now.getTimeString().c_str()));
		ciDEBUG(1,("%s RESULT=%d, %s", name.c_str(), ele->result, ele->debugString.c_str()));
		_ini->set(name.c_str(), "ENDTIME", e_now.getTimeString().c_str());
		_ini->set(name.c_str(), "RESULT", ele->result);
		_ini->set(name.c_str(), "DEBUG", ele->debugString.c_str());
	}
}

ciBoolean
serviceMonitor::serverTest(ciString& debugString, const char* entity)
{
	ciDEBUG(1,("serverTest(%s)", entity));

	debugString = entity ;
	debugString += " Test start";
	ciDEBUG(5,(debugString.c_str()));

	cciCall aCall("get");

	cciEntity aEntity(entity);
	aCall.setEntity(aEntity);
	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);
	aCall.wrap();
	ciBoolean resultExist = ciFalse;

	debugString = aCall.toString();
	ciDEBUG(5,(debugString.c_str()));
	try {
		if(!aCall.call()) {
			debugString += " failed";
			ciERROR((debugString.c_str()));
			return ciFalse;
		}
		cciReply reply;
		while(aCall.hasMore()){ // skpark add aCall.hasMore()!
		while(aCall.getReply(reply))  // skpark add aCall.hasMore()!
		{
			if(!reply.unwrap()){
				ciWARN(("Invalid data"));
				continue;
			}
			resultExist = ciTrue;		
		}}
	}catch(CORBA::Exception& ex) {
		debugString += ex._name();
		ciERROR((debugString.c_str()));
		return ciFalse;
	}
	
	if(!resultExist){
		debugString += " failed";;
		ciERROR((debugString.c_str()));
		return resultExist;
	}

	debugString += " succeed";
	ciDEBUG(1,(debugString.c_str()));
	return resultExist;
}


ciBoolean
serviceMonitor::odTest(ciString& debugString)
{
	ciDEBUG(1,("odTest()"));
	ciBoolean retval = serverTest(debugString, "OD=OD");
	return retval;
}


ciBoolean
serviceMonitor::agtTest(ciString& debugString, int pmId)
{
	ciDEBUG(1,("agtTest()"));
	char entity[256];
	sprintf(entity,"AGTServer=%d", pmId);
	
	ciBoolean retval = serverTest(debugString, (const char*)entity);
	return retval;

}

ciBoolean
serviceMonitor::pmTest(ciString& debugString, int pmId)
{
	ciDEBUG(1,("pmTest()"));
	char entity[256];
	sprintf(entity,"PM=%d", pmId);
	
	ciBoolean retval = serverTest(debugString, (const char*)entity);
	return retval;

}

ciBoolean
serviceMonitor::timeServerTest(ciString& debugString)
{
	ciDEBUG(1,("timeServerTest()"));

	debugString = "connectTest(5)";
	ciDEBUG(5,(debugString.c_str()));
	
	if(!this->connectTest(5)){
		debugString += " failed";
		ciERROR((debugString.c_str()));
		return ciFalse;
	}
	debugString += " succeed";
	ciDEBUG(1, (debugString.c_str()));
	return ciTrue;
}

ciBoolean
serviceMonitor::notiTest(ciString& debugString)
{
	ciDEBUG(1,("notiTest()"));

	cciEventManager* manager = cciEventManager::getInstance();
	ciString eventName = "bpChanged";
	cciEntity aEntity = "*";
	
	debugString = "addHand * bpChanged";
	ciDEBUG(5,(debugString.c_str()));

	ciLong handId = manager->addHandler(aEntity, eventName, new dummyEventHandler());
	if(handId<0) {
		debugString = "addHand * bpChanged failed";
		ciWARN((debugString.c_str()));
		return ciFalse;
	}

	debugString = "addHand * bpChanged succeed";
	ciDEBUG(5,(debugString.c_str()));

	debugString = "removeHand * bpChanged";
	if(!manager->removeAllHandler()){
		debugString = "removeHand fail";
		ciWARN((debugString.c_str()));
		return ciFalse;
	}

	debugString = "removeHand succeed";
	ciDEBUG(5,(debugString.c_str()));
	return ciTrue;
}


ciBoolean
serviceMonitor::dbTest(ciString& debugString)
{
	ciDEBUG(1,("dbTest()"));
	
	cciObject aObj("UBC_PMO");

	cciAny nullAny;
	aObj.addItemAsKey("mgrId", new cciString("OD"));
	aObj.addItemAsKey("pmId", new cciString("1"));
	aObj.addItem("lastUpdateTime", new cciTime());

	if(!aObj.set("")){
		debugString = "dbset failed = ";
		debugString += aObj.getErrMsg();
		ciERROR((debugString.c_str()));
		return ciFalse;
	}

	debugString = "dbset succeed";
	ciDEBUG(1,(debugString.c_str()));
	return ciTrue;

}

int	
serviceMonitor::getError(ciTime& now, int period, testResultMap* outMap)
{
	ciDEBUG(1,("getError()"));

	ciGuard aGuard(_lock);

	testResultMap::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		ciULong nowSec = now.getTime();
		ciULong gap = nowSec - itr->second->endTime;
		if(gap > period) {
			char buf[256];
			sprintf(buf,"%s : gap(%u) bigger than period(%u)", itr->first.c_str(), gap,period);
			ciERROR((buf));
			itr->second->result = ciFalse;
			itr->second->debugString = buf;
		}else if(itr->second->result == ciFalse ){
			ciERROR(("%s : Test Result is false", itr->first.c_str(), gap,period));
		}

		if(itr->second->result == ciFalse) {
			testResultMap::iterator jtr = outMap->find(itr->first);
				testResult* ele = 0;
				if(jtr==outMap->end()){
					ele = new testResult;
					outMap->insert(testResultMap::value_type(itr->first, ele));
				}else{
					ele = jtr->second;
				}
				ele->debugString = itr->second->debugString;
				ele->endTime = itr->second->endTime;
				ele->startTime = itr->second->startTime;
				ele->result = itr->second->result;
		}
	}
	return outMap->size();
}


ciBoolean 
serviceMonitor::connectTest(int timeout)
{
    ACE_INET_Addr server_addr;
	int result = server_addr.set (14106, "127.0.0.1");

	if (result == -1) {
		ciERROR(("invalid address. 127.0.0.1 14106"));
		return ciFalse;
	}

    ACE_SOCK_Connector connector;
    ACE_SOCK_Stream peer;
	ACE_Time_Value aTimeval;
	aTimeval.set(timeout,0);

    if (connector.connect (peer , server_addr, &aTimeval) < 0) {
		ciERROR(("connect() error. 127.0.0.1 14106"));
		return ciFalse;
	}
	peer.close();
	ciDEBUG(1,("connect succeed : 127.0.0.1 14106"));
	return ciTrue;
}


ciBoolean 
serviceMonitor::killServer(const char* binary)
{
	ciDEBUG(1,("_killServer(%s)", binary));
	unsigned  long pid = scratchUtil::getInstance()->getPid(binary);
	if(!pid){
		ciWARN(("%s doesn't run", binary));
		return ciFalse;
	}
	scratchUtil::getInstance()->killProcess(pid);
	return ciTrue;
}