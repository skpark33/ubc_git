/*! \class mmCallImpl
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief UTV_Mgr Implement
 *  (Environment: Visibroker 7.0, Windows XP)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _mmCallImpl_h_
#define _mmCallImpl_h_


#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciAttribute.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciScopeInfo.h>

#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libBase/ciBaseType.h>
#include <cci/libUtil/cciPOAUtil.h>

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include "aci/libCall/aciCall.h"

class mmCallImpl : virtual public  aciCall {
public:
	mmCallImpl(const char* pname) : aciCall(pname) {} ;
	virtual ~mmCallImpl() {} ;
	virtual aciReply*	newCallReply(cciEntity& pEntity, const char* directive);
protected:
};	

#endif //_mmCallImpl_h_
