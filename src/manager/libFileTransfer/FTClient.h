#pragma once

#include "FTMsgSocket.h"
#include "FTDefine.h"

enum EFT_STATUS{
	E_ST_SUCCESS,
	E_ST_TIMEOUT,
	E_ST_UNKONWN
};

#ifdef _FT_LIBLINK
class CFTClient
#else
class AFX_EXT_CLASS CFTClient
#endif
{
public:
	CFTClient();
	~CFTClient();
	void SetParent(CWnd*);
	bool Connect(LPCSTR szIP, int nPort=FT_PORT);
	void Close();
	void Write(const char* buf, unsigned int nSize);
	void ClearFileList();
	bool SetINI(LPCSTR, LPCSTR =FT_CONFIG_PATH);
	bool AddFile(LPCSTR, LPCSTR =FT_CONTENTS_PATH);
	bool RemoveFile(LPCSTR);
	bool CreateNexFile(LPCSTR =NULL);
	bool SendFiles();
	void Stop();

	void SetInfoCtrl(LP_PROGRESS file, LP_PROGRESS total, CWnd* info){
		m_Socket.SetInfoCtrl(file, total, info);
	}
	CMapStringToPtr* GetFiles(){
		return (CMapStringToPtr*)m_Socket.GetFiles();
	}
	BYTE GetStatus(){
		return m_Status;
	}
	const CFTMsgSocket* GetSocket(){
		return &m_Socket;
	}
protected:
public:
protected:
	CWnd* m_pParent;
	BYTE m_Status;
	bool m_bThreadStop;
	CFTMsgSocket m_Socket;
	CWinThread* m_pRecvThread;
	static unsigned int RecvThread(LPVOID);
};