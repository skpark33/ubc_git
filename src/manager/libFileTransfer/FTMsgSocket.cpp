#include "stdafx.h"
#include "FTMsgSocket.h"
#include "FileCopy.h"
#include <IMAGEHLP.H>
#pragma comment(lib, "imagehlp.lib")

#ifdef __ciDEBUG__
#include <ci/libDebug/ciDebug.h>
#endif


#ifdef __ciDEBUG__
ciSET_DEBUG(10,"libFileTrasfer");
#endif

CFTMsgSocket::CFTMsgSocket(const SOCKET s, const int af) : CFTSocket(s, af)
{
	m_bServer = true;
	m_pParent = NULL;
	m_szINIFile = _T("");
	m_szCurFile = _T("");
	m_szDownloadPath = _T("");
	m_szTemporaryPath = _T("");

	m_pStatus = NULL;
	m_TotalSize = 0;
	m_pFileProg = NULL;
	m_pTotalProg = NULL;

	m_mapFiles.RemoveAll();
	m_file.m_hFile = INVALID_HANDLE_VALUE;

//	SetBuffer();
}

CFTMsgSocket::~CFTMsgSocket()
{
	ClearFileList();
}

void CFTMsgSocket::SetParent(CWnd* pWnd)
{
	m_pParent = pWnd;
}

void CFTMsgSocket::Post(UINT Msg, WPARAM wParam, LPARAM lParam)
{
	if(!m_pParent)	return;
	m_pParent->PostMessage(Msg, wParam, lParam);
}

void CFTMsgSocket::SetDonloadPath(LPCSTR path)
{
	if(!path)	return;
	m_szDownloadPath = path;
#ifdef __ciDEBUG__
	ciDEBUG(1, ("SetDonloadPath(%s)", path));
#else	
	TRACE(">>> SetDonloadPath(%s)\n", path);
#endif
}

void CFTMsgSocket::SetTemporaryPath(LPCSTR path)
{
	if(!path)	return;
	m_szTemporaryPath = path;
#ifdef __ciDEBUG__
	ciDEBUG(1, ("SetTemporaryPath(%s)", path));
#else	
	TRACE(">>> SetTemporaryPath(%s)\n", path);
#endif
}

void CFTMsgSocket::ClearFileList()
{
	m_TotalSize = 0;

	if(INVALID_HANDLE_VALUE != m_file.m_hFile){
		m_file.Close();
		m_file.m_hFile = INVALID_HANDLE_VALUE;
	}

	POSITION pos = m_mapFiles.GetStartPosition();
	while(pos){
		CString szFile;
		SFileInfo* pInfo = NULL;
		m_mapFiles.GetNextAssoc(pos, szFile, (void*&)pInfo);
		if(!pInfo)	continue;
		delete pInfo;
	}
	m_mapFiles.RemoveAll();
}

bool CFTMsgSocket::SetINI(LPCSTR szPath, LPCSTR szRemotePath)
{
	m_szINIFile = _T("");
	if(!szPath)	return false;

	CFileFind ff;
	if(!ff.FindFile(szPath))
		return false;

	ff.FindNextFile();

	SFileInfo* pInfo = NULL;
	CString szFile = ff.GetFileName();
	ULONGLONG nSize = ff.GetLength();
	ff.Close();

	if(m_mapFiles.Lookup(szFile, (void*&)pInfo))
		return false;

	pInfo = new SFileInfo;
	pInfo->size = nSize;
	pInfo->type = EFT_FT_CONFIG;
	strncpy(pInfo->szFile, szFile, sizeof(pInfo->szLocalPath)-1);
	strncpy(pInfo->szLocalPath, szPath, sizeof(pInfo->szLocalPath)-1);
	strncpy(pInfo->szRemotePath, szRemotePath, sizeof(pInfo->szRemotePath)-1);

	m_szINIFile = szFile;
	m_mapFiles.SetAt(szFile, pInfo);
	return true;
}

bool CFTMsgSocket::AddFile(LPCSTR szPath, LPCSTR szRemotePath)
{
	if(!szPath)	return false;

	CFileFind ff;
	if(!ff.FindFile(szPath))
		return false;

	ff.FindNextFile();

	SFileInfo* pInfo = NULL;
	CString szFile = ff.GetFileName();
	ULONGLONG nSize = ff.GetLength();
	ff.Close();

	if(m_mapFiles.Lookup(szFile, (void*&)pInfo))
		return false;

	m_TotalSize += nSize;

	pInfo = new SFileInfo;
	pInfo->size = nSize;
	strncpy(pInfo->szFile, szFile, sizeof(pInfo->szFile)-1);
	strncpy(pInfo->szLocalPath, szPath, sizeof(pInfo->szLocalPath)-1);
	strncpy(pInfo->szRemotePath, szRemotePath, sizeof(pInfo->szRemotePath)-1);

	m_mapFiles.SetAt(szFile, pInfo);
	return true;
}

bool CFTMsgSocket::RemoveFile(LPCSTR szPath)
{
	if(!szPath)	return false;

	POSITION pos = m_mapFiles.GetStartPosition();
	while(pos){
		CString szFile;
		SFileInfo* pInfo = NULL;
		m_mapFiles.GetNextAssoc(pos, szFile, (void*&)pInfo);

		if(strncmp(pInfo->szLocalPath, szPath, sizeof(pInfo->szLocalPath)-1))
			continue;

		m_TotalSize -= pInfo->size;
		if(m_TotalSize < 0)
			m_TotalSize = 0;

		delete pInfo;
		m_mapFiles.RemoveKey(szFile);
		return true;
	}
	return false;
}

bool CFTMsgSocket::CreateNexFile(LPCSTR szSFile)
{
	CString szFile;
	SFileInfo* pInfo = NULL;

	if(szSFile && m_szCurFile != szSFile){
		m_szCurFile = szSFile;
	}

	POSITION pos = m_mapFiles.GetStartPosition();
	if(!pos){
		return false;
	}

	if(szSFile){
		while(pos){
			m_mapFiles.GetNextAssoc(pos, szFile, (void*&)pInfo);
			if(strncmp(pInfo->szFile, szSFile, sizeof(pInfo->szFile)-1)==0)
				break;
		}

		if(!pos){
			return false;
		}
	}else{
		m_TotalDown = 0;
		m_szCurFile = _T("");

		if(m_pTotalProg){
			m_pTotalProg->SetRange(0, 100);
			m_pTotalProg->SetPos(0);
		}
	}
	
	m_mapFiles.GetNextAssoc(pos, szFile, (void*&)pInfo);
	if(!pInfo){
		return false;
	}
	m_szCurFile = szFile;

	CFileFind ff;
	if(!ff.FindFile(pInfo->szLocalPath)){
		ff.Close();
		pInfo->flag = EFT_FF_OPENERR;
		return CreateNexFile(szFile);
	}
	ff.Close();

	SFTM_MESSAGE msg;
	InitMessage(&msg);
	msg.header.id = htons(FTM_CREATEFILE_REQ);
	msg.header.len = htons(sizeof(SFTM_CREATEFILEREQ));

	msg.msg.msg0001.type = pInfo->type;
	msg.msg.msg0001.size = htonl((ULONG)(pInfo->size/1000));
	msg.msg.msg0001.realsize = htonll((ULONGLONG)(pInfo->size));
	strncpy(msg.msg.msg0001.file, pInfo->szFile, sizeof(msg.msg.msg0001.file)-1);
	strncpy(msg.msg.msg0001.path, pInfo->szRemotePath, sizeof(msg.msg.msg0001.path)-1);

//	Sleep(100);
	Write((char*)&msg, sizeof(SFTM_MESSAGE));
#ifdef __ciDEBUG__
	ciDEBUG(1, ("CreateNexFile(%s)", m_szCurFile));
#else	
	TRACE(">>> CreateNexFile(%s)\n", m_szCurFile);
#endif
	return true;
}

bool CFTMsgSocket::SendFileData(LP_SFTM_SENDFILEREQ pMsg0003, bool bFileRet)
{
	if(INVALID_HANDLE_VALUE == m_file.m_hFile){
		return bFileRet;
	}

	CString szTemp;
	char buf[FT_MAX_DATA];
	ZeroMemory(&buf, sizeof(buf));

	UINT nRet = m_file.Read(&buf, sizeof(buf));
	if(nRet == 0){
		m_file.Close();
		m_file.m_hFile = INVALID_HANDLE_VALUE;

		SFileInfo* pInfo = NULL;
		m_mapFiles.Lookup(pMsg0003->file, (void*&)pInfo);
		if(pInfo)	pInfo->flag = EFT_FF_SUCCESS;

		return false;
	}

//	Sleep(10);
	if(nRet < sizeof(buf))
		Write((char*)&buf, nRet);
	else
		Write((char*)&buf, sizeof(buf));

	m_SendCnt++;
	m_FileDown += nRet;
	m_TotalDown += nRet;
	SetCurPos();
	SetTotPos();
#ifndef __ciDEBUG__
	TRACE(">>> SendFileData(%s : %d)\n", pMsg0003->file, nRet);
#endif
	return true;
}

void CFTMsgSocket::RecvData(const char* pBuf, int size)
{
	if(FT_MAX_DATA < size)
		size = FT_MAX_DATA;

	SFTM_MESSAGE msg;
	InitMessage(&msg);
	msg.header.id = htons(FTM_SENDFILE_RES);
	msg.header.len = htons(sizeof(SFTM_SENDFILERES));

	if(INVALID_HANDLE_VALUE != m_file.m_hFile){
		msg.msg.msg1003.flag = EFT_FF_SENDDATA;
		m_file.Write(pBuf, size);
	}else{
		msg.msg.msg1003.flag = EFT_FF_CREAERR;
	}
	
	msg.msg.msg1003.count = 0;
	msg.msg.msg1003.size = 0;
	ZeroMemory(msg.msg.msg1003.file, sizeof(msg.msg.msg1003.file));
	ZeroMemory(msg.msg.msg1003.path, sizeof(msg.msg.msg1003.path));

//	Sleep(10);
	Write((char*)&msg, sizeof(SFTM_MESSAGE));
#ifndef __ciDEBUG__
	TRACE(">>> CFTMsgSocket::RecvData(%d)\n", size);
#endif
}

bool CFTMsgSocket::FindFileInServer(LPCSTR szPath, ULONGLONG nSize)
{
	CFileFind ff;
	CString szFile;
	ULONGLONG nLen;

	szFile = m_szDownloadPath + szPath;
	if(ff.FindFile(szFile)){
		ff.FindNextFile();
		nLen = ff.GetLength();
		ff.Close();

		if(nLen == nSize)
			return true;
	}

	szFile = m_szTemporaryPath + szPath;
	if(ff.FindFile(szFile)){
		ff.FindNextFile();
		nLen = ff.GetLength();
		ff.Close();

		if(nLen == nSize)
			return true;
	}

	return false;
}

void CFTMsgSocket::CompleteSock()
{
	SFTM_MESSAGE msg;
	InitMessage(&msg);
	msg.header.id = htons(FTM_COMPLETESOCK_REQ);
	msg.header.len = htons(sizeof(SFTM_COMPLETESOCKREQ));
	
	msg.msg.msg0004.flag = EFT_FF_SUCCESS;//pInfo->flag;
	strncpy(msg.msg.msg0004.file, m_szINIFile, sizeof(msg.msg.msg0004.file)-1);
//	strncpy(msg.msg.msg0004.path, pInfo->szRemotePath, sizeof(msg.msg.msg0004.path)-1);

	Write((char*)&msg, sizeof(SFTM_MESSAGE));
#ifdef __ciDEBUG__
	ciDEBUG(1, ("CompleteSock()"));
#else	
	TRACE(">>> CompleteSock()\n");
#endif
}

void CFTMsgSocket::SetInfoCtrl(LP_PROGRESS file, LP_PROGRESS total, CWnd* pInfo)
{
	m_pStatus = pInfo;
	m_pFileProg = file;
	m_pTotalProg = total;
}

void CFTMsgSocket::SetCurPos()
{
	if(!m_pFileProg) return;

	if(m_FileDown < 0) m_FileDown = 0;
	if(m_FileSize < 0) m_FileSize = 0;

	int nPos = 100;
	if(m_FileSize != 0) nPos = int(m_FileDown*100/m_FileSize);
	if(nPos > 100) nPos = 100;

	int nDown = int(m_FileDown/1000);
	int nFile = int(m_FileSize/1000);

	CString szText;
	szText.Format("%d %% [%d Kbyte / %d Kbyte]", nPos, nDown, nFile);
	m_pFileProg->SetText(szText);
	m_pFileProg->SetPos(nPos);
}

void CFTMsgSocket::SetTotPos()
{
	if(!m_pTotalProg) return;

	int nPos = 100;

	if(m_TotalSize < 0) m_TotalSize = 0;
	if(m_TotalDown < 0) m_TotalDown = 0;

	if(m_TotalSize != 0) nPos = int(m_TotalDown*100/m_TotalSize);
	if(nPos > 100) nPos = 100;

	int nDown = int(m_TotalDown/1000);
	int nFile = int(m_TotalSize/1000);

	CString szText;
	szText.Format("%d %% [%d Kbyte / %d Kbyte]", nPos, nDown, nFile);
	m_pTotalProg->SetText(szText);
	m_pTotalProg->SetPos(nPos);
}

int CFTMsgSocket::SetBuffer()
{
	int nRet;
	nRet = SetOption(SO_SNDBUF, (const char*)&m_SendBuff, sizeof(m_SendBuff));
	nRet = SetOption(SO_RCVBUF, (const char*)&m_RecvBuff, sizeof(m_RecvBuff));
	return nRet;
}

void CFTMsgSocket::Message(const char* buf, int size)
{
	LP_SFTM_HEADER pHeader = (LP_SFTM_HEADER)buf;
	BOOL bCheck = !strncmp(pHeader->check, FT_HEADER_CHECK, sizeof(pHeader->check));

	LP_SFTM_MESSAGE pMsg = (LP_SFTM_MESSAGE)buf;

	if(!m_bServer || bCheck)
	{
		switch(ntohs(pHeader->id))
		{
		case FTM_CREATEFILE_REQ:	CREATEFILE_REQ(pMsg);		break;
		case FTM_CREATEFILE_RES:	CREATEFILE_RES(pMsg);		break;
//		case FTM_COMPLETEFILE_REQ:	COMPLETEFILE_REQ(pMsg);		break;
//		case FTM_COMPLETEFILE_RES:	COMPLETEFILE_RES(pMsg);		break;
//		case FTM_SENDFILE_REQ:		SENDFILE_REQ(pMsg);			break;
		case FTM_SENDFILE_RES:		SENDFILE_RES(pMsg);			break;
		case FTM_COMPLETESOCK_REQ:	COMPLETESOCK_REQ(pMsg);		break;
		case FTM_COMPLETESOCK_RES:	COMPLETESOCK_RES(pMsg);		break;
		case FTM_ACK_REQ:			ACK_REQ(pMsg);				break;
		case FTM_ACK_RES:			ACK_RES(pMsg);				break;
		case FTM_FILECOPY_NOTI:		FILECOPY_NOTI(pMsg);		break;
		default:
#ifndef __ciDEBUG__
	TRACE(">>> SOCKET:%d Recv Msg(0x%04X)\n", m_hSocket, ntohs(pMsg->header.id));
#endif
			break;
		}
	}
	else if(m_bServer)
	{
		RecvData(buf, size);
	}
#ifndef __ciDEBUG__
	TRACE("size:%dByte-----------------------\n", size);
#endif
}

void CFTMsgSocket::CREATEFILE_REQ(LP_SFTM_MESSAGE pMsg)
{

	CString szFile = _T("");
	szFile += pMsg->msg.msg0001.path;
	szFile += pMsg->msg.msg0001.file;

	//ciDEBUG(1, ("skpark CREATEFILE_REQ %s 0", pMsg->msg.msg0001.file));

	SFTM_MESSAGE msg;
	InitMessage(&msg);
	msg.header.id = htons(FTM_CREATEFILE_RES);
	msg.header.len = htons(sizeof(SFTM_CREATEFILERES));

	strncpy(msg.msg.msg1001.file, pMsg->msg.msg0001.file, sizeof(pMsg->msg.msg0001.file)-1);
	strncpy(msg.msg.msg1001.path, pMsg->msg.msg0001.path, sizeof(pMsg->msg.msg0001.path)-1);
	
	ULARGE_INTEGER tempfs, downfs, tb, tfb;
	GetDiskFreeSpaceEx(m_szTemporaryPath, &tempfs, &tb, &tfb);
	GetDiskFreeSpaceEx(m_szDownloadPath, &downfs, &tb, &tfb);
	
	//ciDEBUG(1, ("skpark CREATEFILE_REQ %s 1", pMsg->msg.msg0001.file));

	// free space 를 1KByte 정도 크게 조회한다.
	ULONGLONG nfsByte = ntohll((ULONGLONG)pMsg->msg.msg0001.realsize);
	ULONGLONG fs = nfsByte + 1000;

	EFileType eType = (EFileType)pMsg->msg.msg0001.type;
	//ciDEBUG(1, ("skpark CREATEFILE_REQ %s 2", pMsg->msg.msg0001.file));

	if(EFT_FT_CONFIG != eType && FindFileInServer(szFile, nfsByte)){
		//ciDEBUG(1, ("skpark CREATEFILE_REQ %s 3 alreay exist", pMsg->msg.msg0001.file));
		msg.msg.msg1001.flag = EFT_FF_EXIST;
	}
	else if(tempfs.QuadPart <= fs || downfs.QuadPart <= fs){
		msg.msg.msg1001.flag = EFT_FF_DSERR;
#ifdef __ciDEBUG__
		ciDEBUG(1, ("file : %d Byte", nfsByte));
		ciDEBUG(1, ("temp : %I64d / target : %I64d / file: %I64d", tempfs.QuadPart, downfs.QuadPart, fs));
#else	
	TRACE("temp : %I64d / target : %I64d / file: %I64d", tempfs.QuadPart, downfs.QuadPart, fs);
#endif
	}
	else{
		msg.msg.msg1001.flag = EFT_FF_CREATE;
		//ciDEBUG(1, ("skpark CREATEFILE_REQ %s 3 does not exist", pMsg->msg.msg0001.file));
	}

	szFile = m_szTemporaryPath;
	szFile += pMsg->msg.msg0001.path;
	szFile += pMsg->msg.msg0001.file;

	// 부속파일처리 관련 수정
	CString strMakePath = m_szTemporaryPath + pMsg->msg.msg0001.path;
	strMakePath.Replace(_T("/"), _T("\\"));
	MakeSureDirectoryPathExists(strMakePath);

	if(INVALID_HANDLE_VALUE != m_file.m_hFile){
		m_file.Close();
		m_file.m_hFile = INVALID_HANDLE_VALUE;
	}
	//ciDEBUG(1, ("skpark CREATEFILE_REQ %s 4", pMsg->msg.msg0001.file));

	if(EFT_FF_CREATE == msg.msg.msg1001.flag)
	{
		/*
		// 0000552: 이름이 소문자로 되어있는 스케줄을 대문자로 다시저장하여 단말에 보내는 경우 대문자로 변경해야하므로 먼저 삭제한다
		if(EFT_FT_CONFIG == eType)
		{
			ciDEBUG(1, ("skpark CREATEFILE_REQ %s 5", pMsg->msg.msg0001.file));
			CFile::Remove(szFile);
		}
		*/
		//ciDEBUG(1, ("skpark CREATEFILE_REQ %s 5.1", pMsg->msg.msg0001.file));

		if(!m_file.Open(szFile, CFile::modeCreate|CFile::modeWrite))
		{
			//ciDEBUG(1, ("skpark CREATEFILE_REQ %s 6", pMsg->msg.msg0001.file));
			msg.msg.msg1001.flag = EFT_FF_CREAERR;
		}
	}
	//ciDEBUG(1, ("skpark CREATEFILE_REQ %s 7", pMsg->msg.msg0001.file));

	Write((char*)&msg, sizeof(SFTM_MESSAGE));
#ifdef __ciDEBUG__
	ciDEBUG(1, ("CREATEFILE_REQ[0x%04X] %s [%d]", FTM_CREATEFILE_REQ, pMsg->msg.msg0001.file, msg.msg.msg1001.flag));
#else	
	TRACE(">>> CREATEFILE_REQ[0x%04X] %s [%d]\n", FTM_CREATEFILE_REQ, pMsg->msg.msg0001.file, msg.msg.msg1001.flag);
#endif
}

void CFTMsgSocket::CREATEFILE_RES(LP_SFTM_MESSAGE pMsg)
{
	CString szFile;
	CString szTemp;
	SFileInfo* pInfo = NULL;
#ifdef __ciDEBUG__
	ciDEBUG(1, ("CREATEFILE_RES[0x%04X] %s [%d]", FTM_CREATEFILE_RES, pMsg->msg.msg1001.file, pMsg->msg.msg1001.flag));
#else	
	TRACE(">>> CREATEFILE_RES[0x%04X] %s [%d]\n", FTM_CREATEFILE_RES, pMsg->msg.msg1001.file, pMsg->msg.msg1001.flag);
#endif

	m_mapFiles.Lookup(pMsg->msg.msg1001.file, (void*&)pInfo);
	if(!pInfo){
		if(!CreateNexFile(m_szCurFile)){
			CompleteSock();
		}
		return;
	}

	if(INVALID_HANDLE_VALUE != m_file.m_hFile){
		m_file.Close();
		m_file.m_hFile = INVALID_HANDLE_VALUE;
	}

	if(m_pFileProg){
		m_pFileProg->SetRange(0, 100);
		m_pFileProg->SetPos(0);
	}

	pInfo->flag = pMsg->msg.msg1001.flag;

	if(pMsg->msg.msg1001.flag != EFT_FF_CREATE){
		m_TotalDown += pInfo->size;
		SetTotPos();
		if(!CreateNexFile(pInfo->szFile)){
			CompleteSock();
		}
		return;
	}

	if(!m_file.Open(pInfo->szLocalPath, CFile::modeRead|CFile::shareDenyWrite)){
		pInfo->flag = EFT_FF_OPENERR;
		m_TotalDown += pInfo->size;
		SetTotPos();

		if(!CreateNexFile(pInfo->szFile)){
			CompleteSock();
		}
		return;
	}

	SFTM_SENDFILEREQ msg0003;
	ZeroMemory(&msg0003, sizeof(SFTM_SENDFILEREQ));

	msg0003.count = 0;
	msg0003.size = pInfo->size;
	strncpy(msg0003.file, pMsg->msg.msg1001.file, sizeof(msg0003.file)-1);
	strncpy(msg0003.path, pMsg->msg.msg1001.path, sizeof(msg0003.path)-1);

	m_SendCnt = 0;
	m_FileDown = 0;
	m_FileSize = m_file.GetLength();
	SetCurPos();
	SetTotPos();
	
	m_file.SeekToBegin();

	if(m_pStatus){
		szTemp.Format("Uploading %s", m_szCurFile);
		m_pStatus->SetWindowText(szTemp);
	}

	if(!SendFileData(&msg0003)){
		Sleep(3000); // 파일간 3초 지연
		if(!CreateNexFile(m_szCurFile)){
			CompleteSock();
		}
	}
}

void CFTMsgSocket::COMPLETEFILE_REQ(LP_SFTM_MESSAGE pMsg)
{
#ifndef __ciDEBUG__
	TRACE(">>> CFTMsgSocket::COMPLETEFILE_REQ()\n");
#endif
}

void CFTMsgSocket::COMPLETEFILE_RES(LP_SFTM_MESSAGE pMsg)
{
#ifndef __ciDEBUG__
	TRACE(">>> CFTMsgSocket::COMPLETEFILE_RES()\n");
#endif
}

void CFTMsgSocket::SENDFILE_REQ(LP_SFTM_MESSAGE pMsg)
{
#ifndef __ciDEBUG__
	TRACE(">>> CFTMsgSocket::SENDFILE_REQ()\n");
#endif
}

void CFTMsgSocket::SENDFILE_RES(LP_SFTM_MESSAGE pMsg)
{
	CString szTemp;
	SFTM_SENDFILEREQ msg0003;
	ZeroMemory(&msg0003, sizeof(SFTM_SENDFILEREQ));

	strncpy(msg0003.file, m_szCurFile, sizeof(msg0003.file)-1);

	if(m_pStatus){
		szTemp.Format("Uploading %s", m_szCurFile);
		m_pStatus->SetWindowText(szTemp);
	}

	if(!SendFileData(&msg0003, true)){
		Sleep(3000); // 파일간 3초 지연
		if(!CreateNexFile(m_szCurFile)){
			CompleteSock();
		}
	}
#ifdef __ciDEBUG__
	ciDEBUG(1, ("SENDFILE_RES"));
#else	
	TRACE(">>> SENDFILE_RES()\n");
#endif
}

void CFTMsgSocket::COMPLETESOCK_REQ(LP_SFTM_MESSAGE pMsg)
{
//	SFTM_MESSAGE msg;
//	InitMessage(&msg);
//	msg.header.id = htons(FTM_COMPLETESOCK_RES);
//	msg.header.len = htons(sizeof(SFTM_COMPLETESOCKRES));
	
//	msg.msg.msg1004.flag = pMsg->msg.msg0004.flag;
//	strncpy(msg.msg.msg1004.file, pMsg->msg.msg0004.file, sizeof(msg.msg.msg1004.file)-1);
//	strncpy(msg.msg.msg1004.path, pMsg->msg.msg0004.path, sizeof(msg.msg.msg1004.path)-1);

//	Write((char*)&msg, sizeof(SFTM_MESSAGE));

	if(INVALID_HANDLE_VALUE != m_file.m_hFile){
		m_file.Close();
		m_file.m_hFile = INVALID_HANDLE_VALUE;
	}

	CFileCopy::Create()->Add(m_hSocket, &pMsg->msg.msg0004);

#ifdef __ciDEBUG__
	ciDEBUG(1, ("COMPLETESOCK_REQ(%s)", pMsg->msg.msg0004.file));
#else	
	TRACE(">>> COMPLETESOCK_REQ(%s)\n", pMsg->msg.msg0004.file);
#endif
}

void CFTMsgSocket::COMPLETESOCK_RES(LP_SFTM_MESSAGE pMsg)
{
	Close();

	SFileInfo* pInfo = NULL;
	if(m_mapFiles.Lookup(pMsg->msg.msg1004.file, (void*&)pInfo)){
		if(pInfo){
			pInfo->flag = pMsg->msg.msg1004.flag;
		}
	}

#ifdef __ciDEBUG__
	ciDEBUG(1, ("COMPLETESOCK_RES(%s)", pMsg->msg.msg1004.file));
#else	
	TRACE(">>> CFTMsgSocket::COMPLETESOCK_RES(%s)\n", pMsg->msg.msg1004.file);
#endif
}

void CFTMsgSocket::SendAckMsg()
{
	SFTM_MESSAGE msg;
	InitMessage(&msg);
	msg.header.id = htons(FTM_ACK_REQ);
	msg.header.len = 0;

	Write((char*)&msg, sizeof(SFTM_MESSAGE));

#ifdef __ciDEBUG__
	ciDEBUG(1, ("SendAckMsg"));
#else
	DWORD dwTick = CheckTickCount();
	TRACE(">>> CFTMsgSocket::SendAckMsg(%d)\n", dwTick);
#endif
}

void CFTMsgSocket::ACK_REQ(LP_SFTM_MESSAGE pMsg)
{
	SFTM_MESSAGE msg;
	InitMessage(&msg);
	msg.header.id = htons(FTM_ACK_RES);
	msg.header.len = 0;

	Write((char*)&msg, sizeof(SFTM_MESSAGE));

#ifndef __ciDEBUG__
	TRACE(">>> CFTMsgSocket::ACK_REQ()\n");
#endif
}

void CFTMsgSocket::ACK_RES(LP_SFTM_MESSAGE pMsg)
{
#ifndef __ciDEBUG__
	TRACE(">>> CFTMsgSocket::ACK_RES()\n");
#endif
}

void CFTMsgSocket::FILECOPY_NOTI(LP_SFTM_MESSAGE pMsg)
{
	SFileInfo* pInfo = NULL;
	if(m_mapFiles.Lookup(pMsg->msg.msg2001.file, (void*&)pInfo));
	if(pInfo){
		pInfo->flag = pMsg->msg.msg2001.flag;
	}

#ifndef __ciDEBUG__
	TRACE(">>> CFTMsgSocket::FILECOPY_NOTI()\n");
#endif
}