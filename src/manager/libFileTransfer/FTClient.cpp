#include "stdafx.h"
#include "FTClient.h"
#include "FileCopy.h"

CFTClient::CFTClient()
{
	m_pParent = NULL;
	m_pRecvThread = NULL;
	m_bThreadStop = true;
	m_Status = E_ST_SUCCESS;
	m_Socket.SetServer(false);
}

CFTClient::~CFTClient()
{
	CFileCopy::Release();

	m_bThreadStop = true;
	m_Socket.Close();

	if(m_pRecvThread){
		::WaitForSingleObject(m_pRecvThread->m_hThread, 5000);
		m_pRecvThread = NULL;
	}
}

void CFTClient::SetParent(CWnd* pWnd)
{
	m_pParent = pWnd;
	m_Socket.SetParent(pWnd);
}

bool CFTClient::Connect(LPCSTR szIP, int nPort)
{
	if(m_Socket.Open() == INVALID_SOCKET) return false;

	if(m_Socket.Connect(szIP, nPort) == SOCKET_ERROR) return false;

//	m_Socket.SetBuffer();
//	m_Socket.EnableNonBlockSock(false);

//	struct timeval tv_timeo = { 30, 0 };
//	m_Socket.SetOption(SO_SNDTIMEO, (const char*)&tv_timeo, sizeof(tv_timeo));
//	m_Socket.SetOption(SO_RCVTIMEO, (const char*)&tv_timeo, sizeof(tv_timeo));

	if(m_bThreadStop)
	{
		m_bThreadStop = false;
		m_pRecvThread = AfxBeginThread(RecvThread, (LPVOID)this);
	}

	return true;
}

void CFTClient::Close()
{
	m_bThreadStop = true;
	m_Socket.Close();
}

void CFTClient::Write(const char* buf, unsigned int nSize)
{
	m_Socket.Write(buf, nSize);
}

void CFTClient::ClearFileList()
{
	m_Socket.ClearFileList();
}

bool CFTClient::SetINI(LPCSTR szPath, LPCSTR szRemote)
{
	return m_Socket.SetINI(szPath, szRemote);
}

bool CFTClient::AddFile(LPCSTR szPathFile, LPCSTR szRemote)
{
	return m_Socket.AddFile(szPathFile, szRemote);
}

bool CFTClient::RemoveFile(LPCSTR szPathFile)
{
	return m_Socket.RemoveFile(szPathFile);
}

bool CFTClient::SendFiles()
{
	if(!m_Socket.CreateNexFile(NULL))
		return false;

	while(!m_bThreadStop){
		Sleep(1000);
	}
	return true;
}

void CFTClient::Stop()
{
	m_bThreadStop = true;
}

bool CFTClient::CreateNexFile(LPCSTR szPathFile)
{
	return m_Socket.CreateNexFile(szPathFile);
}

unsigned int CFTClient::RecvThread(LPVOID pVoid)
{
	CFTClient* pClient = (CFTClient*)pVoid;
	if(!pClient)
	{
		AfxEndThread(0);
		return 0;
	}

	SOCKET s = pClient->m_Socket.GetHandle();
	if(s == INVALID_SOCKET)
	{
		AfxEndThread(0);
		return 0;
	}

	fd_set readset, rset;
	struct timeval tv;
//	char buf[FT_BUFFER_SIZE+10];
	char buf[FT_BUFFER_SIZE];

	FD_ZERO(&readset);
	FD_SET(s, &readset);

	while(1)
	{
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		rset = readset;

		int nSel = pClient->m_Socket.Select(&rset, NULL, NULL, &tv);

		if(pClient->m_bThreadStop)
		{
			AfxEndThread(0);
			return 0;
		}

		// time out
		if(nSel == 0)
		{
			if(FT_TIMEOUT < pClient->m_Socket.CheckTickCount())
			{
				pClient->m_Status = E_ST_TIMEOUT;
				pClient->m_bThreadStop = true;
				pClient->Close();
				TRACE(">>> CFTClient::RecvThread is terminated : time out\n");
				AfxEndThread(0);
				return 0;
			}
//			else if(10000 < pClient->m_Socket.CheckTickCount()){
//				pClient->m_Socket.SendAckMsg();
//			}
			continue;
		}

		if(nSel == SOCKET_ERROR)
		{
			pClient->m_Status = E_ST_UNKONWN;
			pClient->m_bThreadStop = true;
			TRACE(">>> CFTClient::RecvThread is terminated : socket error\n");
			AfxEndThread(0);
			return 0;
		}

		if (FD_ISSET(s, &rset))
		{
			for(int i = 0; i < nSel; i++)
			{
				memset(buf, 0, sizeof(buf));
				int rv = pClient->m_Socket.Read(buf, sizeof(buf), 0);

				if(rv == SOCKET_ERROR) continue;
				if(rv == -1) continue;

				if(rv == 0)
				{
					DWORD dwSize; 
					ioctlsocket(s, FIONREAD, &dwSize); 
					if(dwSize != 0) continue;

					pClient->m_bThreadStop = true;
					TRACE(">>> CFTClient::RecvThread is terminated : socket close\n");
					AfxEndThread(0);
					return 0;
				}
				else
				{
					pClient->m_Socket.Message(buf, rv);
				}
			}
		}
	}

	AfxEndThread(0);
	return 0;
}