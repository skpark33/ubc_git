#pragma once

#define FT_PORT				14005
#define FT_MAX_DATA			(1024*64)
#define FT_TIMEOUT			30000

#define FT_HEADER_CHECK		"oxoxoxoxox"
#define	FT_ROOT_PATH		_T("SQISoft")
#define	FT_CONFIG_PATH		_T("SQISoft\\UTV1.0\\execute\\config\\")
#define	FT_CONTENTS_PATH	_T("SQISoft\\Contents\\ENC\\")
#define	FT_CONTENTS_ROOT	_T("SQISoft\\Contents\\")

#define FTM_CREATEFILE_REQ		0x0001
#define FTM_CREATEFILE_RES		0x1001

#define FTM_COMPLETEFILE_REQ	0x0002
#define FTM_COMPLETEFILE_RES	0x1002

#define FTM_SENDFILE_REQ		0x0003
#define FTM_SENDFILE_RES		0x1003

#define FTM_COMPLETESOCK_REQ	0x0004
#define FTM_COMPLETESOCK_RES	0x1004

#define FTM_ACK_REQ				0x0005
#define FTM_ACK_RES				0x1005

#define FTM_FILECOPY_NOTI		0x2001

enum EFileFlag{
	EFT_FF_SUCCESS, // success
	EFT_FF_INIT,
	EFT_FF_CREATE,
	EFT_FF_SENDDATA,
	EFT_FF_EXIST,	// file exist to a network
	EFT_FF_DSERR,	// drive space error
	EFT_FF_OPENERR,	// file is not opened on a local
	EFT_FF_CREAERR, // file is not created on a network
	EFT_FF_COPYERR,
	EFT_FF_UNKNOWN
};

enum EFileType{
	EFT_FT_CONFIG, // ini config file
	EFT_FT_NORMAL,
	EFT_FT_UNKONWN
};


#define htonll(A)  ((((ULONGLONG)(A) & 0xff00000000000000) >> 56) | \
                    (((ULONGLONG)(A) & 0x00ff000000000000) >> 40) | \
                    (((ULONGLONG)(A) & 0x0000ff0000000000) >> 24) | \
                    (((ULONGLONG)(A) & 0x000000ff00000000) >> 8)  | \
                    (((ULONGLONG)(A) & 0x00000000ff000000) << 8)  | \
                    (((ULONGLONG)(A) & 0x0000000000ff0000) << 24) | \
                    (((ULONGLONG)(A) & 0x000000000000ff00) << 40) | \
                    (((ULONGLONG)(A) & 0x00000000000000ff) << 56))

#define ntohll  htonll

// Duplex Messages
typedef struct SFtm_CreateFileReq{
	BYTE	type;// EFileType
	ULONG	size;// KByte 단위
	char	file[MAX_PATH];
	char	path[MAX_PATH];
	ULONGLONG realsize; // Byte 단위
}SFTM_CREATEFILEREQ, *LP_SFTM_CREATEFILEREQ;

typedef struct SFtm_CreateFileRes{
	BYTE flag;// EFileFlag
	char file[MAX_PATH];
	char path[MAX_PATH];
}SFTM_CREATEFILERES, *LP_SFTM_CREATEFILERES;

typedef struct SFtm_CompleteFileReq{
	char	file[MAX_PATH];
}SFTM_COMPLETEFILEREQ, *LP_SFTM_COMPLETEFILEREQ;

typedef struct SFtm_CompleteFileRes{
	BYTE flag;// EFileFlag
	char file[MAX_PATH];
}SFTM_COMPLETEFILERES, *LP_SFTM_COMPLETEFILERES;

typedef struct SFtm_SendFileReq{
	ULONG	count;
	ULONG	size;// KByte 단위
	char	file[MAX_PATH];
	char	path[MAX_PATH];
}SFTM_SENDFILEREQ, *LP_SFTM_SENDFILEREQ;

typedef struct SFtm_SendFileRes{
	BYTE	flag;// EFileFlag
	ULONG	count;
	ULONG	size;// KByte 단위
	char	file[MAX_PATH];
	char	path[MAX_PATH];
}SFTM_SENDFILERES, *LP_SFTM_SENDFILERES;

typedef struct SFtm_CompleteSockReq{
	BYTE	flag;// EFileFlag
	char	file[MAX_PATH];
	char	path[MAX_PATH];
}SFTM_COMPLETESOCKREQ, *LP_SFTM_COMPLETESOCKREQ;

typedef struct SFtm_CompleteSockRes{
	BYTE	flag;// EFileFlag
	char	file[MAX_PATH];
	char	path[MAX_PATH];
}SFTM_COMPLETESOCKRES, *LP_SFTM_COMPLETESOCKRES;


// Noti Messages
typedef struct SFtm_FileCopyNoti{
	BYTE	flag;// EFileFlag
	char	file[MAX_PATH];
}SFTM_FILECOPYNOTI, *LP_SFTM_FILECOPYNOTI;


// Message...
typedef struct SFtm_Header{
	char	dummy[10];
	char    check[10];
	USHORT	id;
	USHORT	len;
}SFTM_HEADER, *LP_SFTM_HEADER;


// All Message Structure
typedef struct SFtm_Message{
	SFTM_HEADER	header;
	union {
		SFTM_CREATEFILEREQ		msg0001;
		SFTM_CREATEFILERES		msg1001;
		SFTM_COMPLETEFILEREQ	msg0002;
		SFTM_COMPLETEFILERES	msg1002;
		SFTM_SENDFILEREQ		msg0003;
		SFTM_SENDFILERES		msg1003;
		SFTM_COMPLETESOCKREQ	msg0004;
		SFTM_COMPLETESOCKRES	msg1004;

		SFTM_FILECOPYNOTI		msg2001;
	} msg;
}SFTM_MESSAGE, *LP_SFTM_MESSAGE;

#define FT_BUFFER_SIZE		(FT_MAX_DATA>sizeof(SFTM_MESSAGE)?FT_MAX_DATA:sizeof(SFTM_MESSAGE))

extern void InitMessage(LP_SFTM_MESSAGE pMsg);