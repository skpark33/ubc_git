// UTBListCtrlEx.cpp : implementation file
//

#include "stdafx.h"
#include "UTBListCtrlEx.h"
#include "commctrl.h"

#include "MemDC.h"

namespace {
	LRESULT EnableWindowTheme(HWND hwnd, LPCWSTR classList, LPCWSTR subApp, LPCWSTR idlist)
	{
		LRESULT lResult = S_FALSE;
		HMODULE hinstDll;
		BOOL (WINAPI *pIsThemeActive)();
		HRESULT (WINAPI *pSetWindowTheme)(HWND hwnd, LPCWSTR pszSubAppName, LPCWSTR pszSubIdList);
		HANDLE (WINAPI *pOpenThemeData)(HWND hwnd, LPCWSTR pszClassList);
		HRESULT (WINAPI *pCloseThemeData)(HANDLE hTheme);

		// Check if running on Windows XP or newer
		hinstDll = ::LoadLibrary(_T("UxTheme.dll"));
		if (hinstDll)
		{
			// Check if theme service is running
			(FARPROC&)pIsThemeActive = ::GetProcAddress( hinstDll, "IsThemeActive" );
			if( pIsThemeActive && pIsThemeActive() )
			{
				(FARPROC&)pOpenThemeData = ::GetProcAddress(hinstDll, "OpenThemeData");
				(FARPROC&)pCloseThemeData = ::GetProcAddress(hinstDll, "CloseThemeData");
				(FARPROC&)pSetWindowTheme = ::GetProcAddress(hinstDll, "SetWindowTheme");
				if (pSetWindowTheme && pOpenThemeData && pCloseThemeData)			
				{
					// Check is themes is available for the application
					HANDLE hTheme = pOpenThemeData(hwnd,classList);
					if (hTheme!=NULL)
					{
						VERIFY(pCloseThemeData(hTheme)==S_OK);
						// Enable Windows Theme Style
						lResult = pSetWindowTheme(hwnd, subApp, idlist);
					}
				}
			}
			::FreeLibrary(hinstDll);
		}
		return lResult;
	}
}

// CUTBListCtrlEx

static CString ToString(int nValue)
{
	CString strValue;
	strValue.Format(_T("%d"), nValue);
	return strValue;
}

IMPLEMENT_DYNAMIC(CUTBListCtrlEx, CListCtrl)

CUTBListCtrlEx::CUTBListCtrlEx()
{
	m_bSortEnable = true;
	m_nCurSort = -1;
	m_bAscend = true;

	// 초기화
	////////////////////////
	m_clrText   = ::GetSysColor(COLOR_WINDOWTEXT);
	m_clrHText  = ::GetSysColor(COLOR_HIGHLIGHTTEXT);
	m_clrBkgnd  = ::GetSysColor(COLOR_WINDOW);
	m_clrHBkgnd = ::GetSysColor(COLOR_HIGHLIGHT);

	m_dwMode    = LS_NONE;

	m_clrBarFore  = ::GetSysColor(COLOR_ACTIVECAPTION);
	m_clrBarBack  = ::GetSysColor(COLOR_BTNFACE);
	m_clrTextFore = ::GetSysColor(COLOR_WINDOWTEXT);

	m_clrCalendar = ::GetSysColor(COLOR_BTNSHADOW);
//	m_clrCalendar = RGB(0x77, 0x33, 0x11);

	m_nHideScrollBar = -1;

	m_bDrawGridLine = true; // LVS_EX_GRIDLINES
}

CUTBListCtrlEx::~CUTBListCtrlEx()
{
}

BOOL CUTBListCtrlEx::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
	// TODO: Add your specialized code here and/or call the base class

	return CListCtrl::Create(dwStyle, rect, pParentWnd, nID);
}

BEGIN_MESSAGE_MAP(CUTBListCtrlEx, CListCtrl)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_NOTIFY_REFLECT_EX(LVN_ENDSCROLL, &CUTBListCtrlEx::OnLvnEndScroll)
	ON_NOTIFY_REFLECT_EX(NM_CLICK, &CUTBListCtrlEx::OnNMClick)
	ON_NOTIFY_REFLECT_EX(LVN_COLUMNCLICK, &CUTBListCtrlEx::OnLvnColumnclick)
	ON_MESSAGE(WM_SETFONT, OnSetFont)
	ON_WM_MEASUREITEM_REFLECT()
	ON_WM_NCCALCSIZE()
	ON_NOTIFY_REFLECT_EX(LVN_GETDISPINFO, &CUTBListCtrlEx::OnLvnGetdispinfo)
	ON_NOTIFY(HDN_BEGINTRACKA, 0, &CUTBListCtrlEx::OnHdnBegintrack)
	ON_NOTIFY(HDN_BEGINTRACKW, 0, &CUTBListCtrlEx::OnHdnBegintrack)
	ON_NOTIFY_EX(TTN_NEEDTEXTA, 0, OnToolNeedText)	// tooltip
	ON_NOTIFY_EX(TTN_NEEDTEXTW, 0, OnToolNeedText)	// tooltip
END_MESSAGE_MAP()

// CUTBListCtrlEx message handlers
int CUTBListCtrlEx::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CUTBListCtrlEx::OnDestroy()
{
	DeleteAllItemData();

	CListCtrl::OnDestroy();
}

void CUTBListCtrlEx::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CListCtrl::OnClose();
}

void CUTBListCtrlEx::PreSubclassWindow()
{
	if(GetParent())
	{
		SetFont(GetParent()->GetFont());
	}

	VERIFY( m_ctrHeader.SubclassWindow( GetHeaderCtrl()->GetSafeHwnd() ) );

	CListCtrl::PreSubclassWindow();

	// Focus retangle is not painted properly without double-buffering
#if (_WIN32_WINNT >= 0x501)
	SetExtendedStyle(LVS_EX_DOUBLEBUFFER | GetExtendedStyle());
#endif
//	SetExtendedStyle(GetExtendedStyle() | LVS_EX_FULLROWSELECT);
//	SetExtendedStyle(GetExtendedStyle() | LVS_EX_HEADERDRAGDROP);
//	SetExtendedStyle(GetExtendedStyle() | LVS_EX_GRIDLINES);

	// Enable Vista-look if possible
	EnableWindowTheme(m_hWnd, L"ListView", L"Explorer", NULL);


	// Disable the CToolTipCtrl of CListCtrl so it won't disturb the CWnd tooltip
	GetToolTips()->Activate(FALSE);

	// Activates the standard CWnd tooltip functionality
	VERIFY( EnableToolTips(TRUE) );

	//// Disable the CToolTipCtrl of CListCtrl so it won't disturb our own tooltip-ctrl
	//GetToolTips()->Activate(FALSE);

	//// Enable our own tooltip-ctrl and make it show tooltip even if not having focus
	//VERIFY( m_OwnToolTipCtrl.Create(this, TTS_ALWAYSTIP) );
	//m_OwnToolTipCtrl.Activate(TRUE);
}

void CUTBListCtrlEx::InitHeader(int nBitmapID, UINT dwStyle)
{
	if(GetParent())
	{
		SetFont(GetParent()->GetFont());
	}

	m_dwMode = dwStyle;

	CBitmap bmp;
	bmp.LoadBitmap(nBitmapID);
	m_ilHeader.Create(16, 16, ILC_COLORDDB | ILC_MASK, 0, 1);
	m_ilHeader.Add(&bmp, RGB(255, 255, 255));
	m_ctrHeader.SetImageList(&m_ilHeader);
/*
	HDITEM hdItem;
	ZeroMemory(&hdItem, sizeof(HDITEM));

	hdItem.mask = HDI_IMAGE|HDI_FORMAT;
	VERIFY( m_ctrHeader.GetItem(0, &hdItem) );

	hdItem.iImage = -1;
	hdItem.fmt |= HDF_IMAGE;

	VERIFY( m_ctrHeader.SetItem(0, &hdItem) );
*/
	if( m_dwMode == LS_CHECKBOX )//|| m_dwMode == LS_RADIOBOX )
	{
		SetExtendedStyle(GetExtendedStyle() | LVS_EX_CHECKBOXES);
	}
	if( m_dwMode == LS_RADIOBOX )
	{
		SetExtendedStyle(GetExtendedStyle() | LVS_SINGLESEL);
	}

	// 리스트에 체크박스가 있는 경우 헤더에 체크여부를 판단하는 기능 추가
	if(m_dwMode == LS_CHECKBOX)
	{
		HDITEM itemCheck = {0};
		itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
		m_ctrHeader.GetItem(0, &itemCheck);

		itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
		itemCheck.iImage = (itemCheck.iImage == CUTBHeaderCtrl::LC_CHK_UNSEL ? CUTBHeaderCtrl::LC_CHK_SEL : CUTBHeaderCtrl::LC_CHK_UNSEL);
		itemCheck.fmt |= HDF_IMAGE | HDF_LEFT;
		m_ctrHeader.SetItem(0, &itemCheck);

		m_ctrHeader.SetBitmapMargin(0);
	}

	m_bDrawGridLine = false;
	if(GetExtendedStyle() & LVS_EX_GRIDLINES)
	{
		SetExtendedStyle(GetExtendedStyle() ^ LVS_EX_GRIDLINES);
		m_bDrawGridLine = true;
	}
}

void CUTBListCtrlEx::SetSortCol(const int iColumn, bool bAsc)
{
	m_nCurSort = iColumn;
	m_bAscend = bAsc;
}

int CUTBListCtrlEx::Compare(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	return ((CUTBListCtrlEx*)lParam)->Compare(lParam1, lParam2);
}

int CUTBListCtrlEx::Compare(LPARAM lParam1, LPARAM lParam2)
{
	LVFINDINFO lvFind = {0};
	lvFind.flags = LVFI_PARAM;

	lvFind.lParam = lParam1;
	int nIndex1 = FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = FindItem(&lvFind);

	int nCol = GetCurSortCol();

	CString strItem1 = GetItemText(nIndex1, nCol);
	CString strItem2 = GetItemText(nIndex2, nCol);

	if(nCol >= 0 && m_ctrHeader.GetColType(nCol) == CUTBHeaderCtrl::COL_PROGRESS)
	{
		float fRate;
		BOOL bGradientFill;
		COLORREF clrBarFore;
		COLORREF clrBarBack;
		COLORREF clrTextFore;
		int nAlign;

		GetProgressBarText(nIndex1, nCol, fRate, nAlign, strItem1, bGradientFill, clrBarFore, clrBarBack, clrTextFore);
		GetProgressBarText(nIndex2, nCol, fRate, nAlign, strItem2, bGradientFill, clrBarFore, clrBarBack, clrTextFore);
	}

	LVCOLUMN lvcol = {0};
	lvcol.mask = LVCF_FMT;

	GetColumn(nCol, &lvcol);
	if(lvcol.fmt & LVCFMT_RIGHT)
	{
		if(strItem1.GetLength() > strItem2.GetLength())
		{
			CString strPad(_T(' '), strItem1.GetLength() - strItem2.GetLength());
			strItem2 = strPad + strItem2;
		}
		else if(strItem1.GetLength() < strItem2.GetLength())
		{
			CString strPad(_T(' '), strItem2.GetLength() - strItem1.GetLength());
			strItem1 = strPad + strItem1;
		}
	}

	strItem1.MakeLower();
	strItem2.MakeLower();

	if(IsAscend()) return strItem1.Compare(strItem2);
	else           return strItem2.Compare(strItem1);
}

BOOL CUTBListCtrlEx::SortList(const int iColumn, PFNLVCOMPARE pfnCompare, DWORD_PTR dwData)
{
	SetSortHeader(iColumn);
	return SortItems(pfnCompare, dwData);
}

BOOL CUTBListCtrlEx::SortList(const int iColumn, bool bAsc, PFNLVCOMPARE pfnCompare, DWORD_PTR dwData)
{
	SetSortHeader(iColumn, bAsc);
	return SortItems(pfnCompare, dwData);
}

void CUTBListCtrlEx::SetSortHeader(const int iColumn)
{
	if(m_nCurSort >= 0)
	{
		if(m_nCurSort != iColumn)
		{
			SetSortHeader(iColumn, true);
		}
		else
		{
			SetSortHeader(iColumn, !m_bAscend);
		}
	}
	else if(m_bSortEnable)
	{
		SetSortHeader(iColumn, true);
	}
}

void CUTBListCtrlEx::SetSortHeader(const int iColumn, bool bAsc)
{
	m_bAscend = bAsc;

	if(m_nCurSort >= 0)
	{
		if(m_nCurSort != iColumn)
		{
			HDITEM itemOld = {0};
			itemOld.mask = HDI_FORMAT;
			m_ctrHeader.GetItem(m_nCurSort, &itemOld);

			itemOld.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);
			m_ctrHeader.SetItem(m_nCurSort, &itemOld);
		}
	}

	HDITEM itemNew = {0};
	itemNew.mask = HDI_FORMAT | HDI_IMAGE;
	m_ctrHeader.GetItem(iColumn, &itemNew);

	itemNew.mask = HDI_FORMAT | HDI_IMAGE;
	//itemNew.iImage = (m_bAscend == true) ? LC_ASC : LC_DES;
	itemNew.iImage = 0;
	itemNew.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);
	itemNew.fmt |= (m_bAscend ? HDF_SORTUP : HDF_SORTDOWN);
	m_ctrHeader.SetItem(iColumn, &itemNew);

	// invalidate the header control so it gets redrawn
	m_nCurSort = iColumn;
	Invalidate();
}

int CUTBListCtrlEx::GetColPos(LPCTSTR szCol)
{
	LPVOID obj = NULL;
	if( m_mapColToPos.Lookup(szCol, (void*&)obj) )
	{
		return (int)obj;
	}

	TCHAR szText[256] = {0};
	LVCOLUMN lvcol = {0};

	lvcol.mask = LVCF_TEXT | LVCF_WIDTH;
	lvcol.pszText = szText;
	lvcol.cchTextMax = 256;
	lvcol.cx = 0;

	int nCol = 0;
	while(GetColumn(nCol, &lvcol)) {
		CString szBuf = lvcol.pszText;
		if(szBuf == szCol)
		{
			m_mapColToPos.SetAt(szCol, (void*)nCol);
			return nCol;
		}
		nCol++;
	}

	return -1;
}

CString CUTBListCtrlEx::GetColText(int nCol)
{
	TCHAR szText[256] = {0};
	LVCOLUMN lvcol = {0};

	lvcol.mask = LVCF_TEXT | LVCF_WIDTH;
	lvcol.pszText = szText;
	lvcol.cchTextMax = 256;
	lvcol.cx = 0;

	GetColumn(nCol, &lvcol);
	return lvcol.pszText;
}

BOOL CUTBListCtrlEx::SetText(int nRow, LPCTSTR szCol, LPCTSTR szData)
{
	return SetItemText(nRow, GetColPos(szCol), szData);
}

CString CUTBListCtrlEx::GetText(int nRow, LPCTSTR szCol)
{
	return GetItemText(nRow, GetColPos(szCol));
}

BOOL CUTBListCtrlEx::OnLvnEndScroll(NMHDR *pNMHDR, LRESULT *pResult)
{
	TRACE("\nOnLvnEndScroll");
	LPNMLVSCROLL pStateChanged = reinterpret_cast<LPNMLVSCROLL>(pNMHDR);

	if(m_ctrHeader.GetFixedCol() >= 0)
		Invalidate();

	*pResult = 0;

	//return 값이 FALSE 이면 부모에게 통지를 하고 TRUE 이면 부모에게 통지를 하지 않는다.
	return FALSE;
}

void CUTBListCtrlEx::SetCheckHdrState(bool bCheck)
{
	HDITEM itemCheck = {0};
	itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
	m_ctrHeader.GetItem(0, &itemCheck);

	itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
	itemCheck.iImage = (bCheck ? CUTBHeaderCtrl::LC_CHK_SEL : CUTBHeaderCtrl::LC_CHK_UNSEL);
	itemCheck.fmt = itemCheck.fmt | HDF_IMAGE ;//| HDF_BITMAP;
	m_ctrHeader.SetItem(0, &itemCheck);
}

bool CUTBListCtrlEx::GetCheckHdrState()
{
	HDITEM itemCheck = {0};
	itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
	m_ctrHeader.GetItem(0, &itemCheck);

	return (itemCheck.iImage == CUTBHeaderCtrl::LC_CHK_SEL);
}

void CUTBListCtrlEx::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	int nSavedDC = pDC->SaveDC();                // Save DC state

	DrawCtrl(pDC, lpDrawItemStruct->itemID);

	pDC->RestoreDC(nSavedDC);
}

void CUTBListCtrlEx::DrawCtrl(CDC* pDC, int nItem)
{
	LVITEM lvi = {0};
	lvi.mask      = LVIF_IMAGE | LVIF_STATE | LVIF_INDENT;
	lvi.iItem     = nItem;
	lvi.iSubItem  = 0;
	lvi.stateMask = 0xFFFF;                // get all state flags

	GetItem(&lvi);

	bool bHighlight = ( lvi.state & LVIS_DROPHILITED || 
					  ( lvi.state & LVIS_SELECTED && (GetFocus() == this || GetStyle() & LVS_SHOWSELALWAYS)));

	CRect rcBound;
	GetItemRect(nItem, rcBound, LVIR_BOUNDS);

	P_ITEM_DATA pKey = (P_ITEM_DATA)CListCtrl::GetItemData(nItem);
	P_ITEM_DATA pItemData = NULL;

	if( m_mapItemData.Lookup(pKey, (void*&)pItemData) &&
		pItemData != NULL                             )
	{
		m_clrRowBkgnd = pItemData->clrBack;
		m_clrRowText = pItemData->clrText;
	}
	else
	{
		m_clrRowBkgnd = m_clrBkgnd;
		m_clrRowText = m_clrText;
	}

	// 배경 그리기
	rcBound.DeflateRect(0,0,0,1);
	pDC->FillSolidRect(&rcBound, (bHighlight)? m_clrHBkgnd : m_clrRowBkgnd);

//	CPen pen;
//	pen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNFACE));
	static CPen pen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNFACE));
	CPen* pOldPen = pDC->SelectObject(&pen);

	if(m_bDrawGridLine)
	{
		pDC->MoveTo(rcBound.left, rcBound.bottom);
		pDC->LineTo(rcBound.right, rcBound.bottom);
	}

	rcBound.InflateRect(0,0,0,1);

	// Draw labels for remaining columns
	LV_COLUMN lvc = {0};
	lvc.mask = LVCF_FMT | LVCF_WIDTH;

	int nFixedColRight = m_ctrHeader.GetFixedColRight();

	CRect rect;
	GetClientRect(rect);

	for (int nColumn = 0; GetColumn(nColumn, &lvc); nColumn++)
	{
		CRect rcItemBound;
		GetSubItemRect(nItem, nColumn, LVIR_BOUNDS, rcItemBound);

		if(m_ctrHeader.GetFixedCol() < 0)
		{
			if( rcItemBound.right < rect.left ) continue;
			if( rect.right < rcItemBound.left ) continue;
		}

		rcItemBound.right = rcItemBound.left + GetColumnWidth(nColumn);

		CMemDC *pMDC = NULL;
		if(m_ctrHeader.GetFixedCol() >= 0)
		{
			if( nColumn > m_ctrHeader.GetFixedCol() )
			{
				if( rcItemBound.right <= nFixedColRight+rcBound.left )
				{
					continue;
				}
				else if( rcItemBound.left <= nFixedColRight )
				{
					//CRgn rgn;
					//rgn.CreateRectRgn(nFixedColRight+rcBound.left, rcItemBound.top, rcItemBound.right, rcItemBound.bottom);
					//pDC->SelectClipRgn(&rgn);
					//rgn.DeleteObject();
					CRect rect(nFixedColRight+rcBound.left, rcItemBound.top, rcItemBound.right, rcItemBound.bottom);
					pMDC = new CMemDC(pDC, rect);
					pMDC->SelectObject(&pen);

					rcBound.DeflateRect(0,0,0,1);
					pMDC->FillSolidRect(&rcBound, (bHighlight)? m_clrHBkgnd : m_clrRowBkgnd);

					if(m_bDrawGridLine)
					{
						pMDC->MoveTo(rcBound.left, rcBound.bottom);
						pMDC->LineTo(rcBound.right, rcBound.bottom);
					}

					rcBound.InflateRect(0,0,0,1);
				}
			}
		}

		CDC* dc = pMDC ? pMDC : pDC;

		if(m_bDrawGridLine)
		{
			dc->MoveTo(rcItemBound.right, rcBound.top);
			dc->LineTo(rcItemBound.right, rcBound.bottom);
		}

		if(GetColumnWidth(nColumn) > 0)
		{
			switch(m_ctrHeader.GetColType(nColumn))
			{
			case CUTBHeaderCtrl::COL_PROGRESS:
				DrawProgressBar(dc, nItem, nColumn);
				break;

			case CUTBHeaderCtrl::COL_CALENDAR:
				DrawCalendarBar(dc, nItem, nColumn, bHighlight);
				break;

			case CUTBHeaderCtrl::COL_INDICATOR_CALENDAR:
				DrawCalendarBar(dc, nItem, nColumn, bHighlight, true);
				break;

			default:
				DrawCheckBox(dc, nItem, nColumn);
				DrawRadioBox(dc, nItem, nColumn);
				DrawImage(dc, nItem, nColumn);
				DrawText(dc, nItem, nColumn, bHighlight);
				break;
			}
		}

		//pDC->SelectClipRgn(NULL);
		if(pMDC) delete pMDC;
	}

	pDC->SelectObject(pOldPen);
}

void CUTBListCtrlEx::DrawProgressBar(CDC* pDC, int nItem, int nColumn)
{
	CRect rcClip;
	pDC->GetClipBox(&rcClip);

	CRect rcItem;
	GetSubItemRect(nItem, nColumn, LVIR_BOUNDS, rcItem);

	if(rcItem.Width() > GetColumnWidth(nColumn))
	{
		rcItem.right = rcItem.left + GetColumnWidth(nColumn);
	}

	float fRate;
	BOOL bGradientFill;
	COLORREF clrBarFore;
	COLORREF clrBarBack;
	COLORREF clrTextFore;
	int nAlign;
	CString strText;
	GetProgressBarText(nItem, nColumn, fRate, nAlign, strText, bGradientFill, clrBarFore, clrBarBack, clrTextFore);
//	CString sItem = GetItemText(nItem, nColumn);
//	float fRate = _tstof(sItem);

	rcItem.DeflateRect(2,1,1,2);

	/* 사용자가 지정한 위치 값(m_nPos)을 기준으로 */
	/* m_nMin과 m_nMax 범위에서 해당되는 퍼센트를 구한다 */
	double Fraction = fRate / (double)100.;

	CRect rcClient, rcLeft, rcRight;
	rcLeft = rcRight = rcClient = rcItem;

	/* Progress 진행바를 그리기 위한 범위 값을 구한다 */
	rcLeft.right = rcLeft.left + (int)((rcLeft.right - rcLeft.left) * Fraction);

	/* rcLeft : 진행바 영역     */
	/* rcRight : 진행바를 제외한 영역 */
	rcLeft.right = rcLeft.left + (int)((rcClient.right - rcClient.left) * Fraction);
	rcRight.left = rcLeft.right;

	// CRgn 대신 MemDC생성
	CMemDC memdc_left(pDC, rcLeft);
	CMemDC memdc_right(pDC, rcRight);

	CDC* pDC_Left = &memdc_left;
	CDC* pDC_Right = &memdc_right;

	pDC_Left->SelectObject(GetFont());
	pDC_Right->SelectObject(GetFont());

	if(bGradientFill)
	{
		COLOR16 red   = (GetRValue(clrBarFore) << 8);
		COLOR16 green = (GetGValue(clrBarFore) << 8);
		COLOR16 blue  = (GetBValue(clrBarFore) << 8);

		/* 1. 진행바를 를 그린다 */
		TRIVERTEX vert[4] =
		{   //                             Red     Green   Blue   Alpha
			{ rcLeft.left , rcLeft.top   ,  red*2, green*2, blue*2, 0x0000 },
			{ rcLeft.right, rcLeft.bottom,  red  , green  , blue  , 0x0000 }
		};
		GRADIENT_RECT gRect = { 0, 1 };

		pDC_Left->GradientFill(vert,2,&gRect,1,GRADIENT_FILL_RECT_H);
	}
	else
	{
		pDC_Left->FillSolidRect(rcLeft, clrBarFore);	/* 푸른색 */
	}

	/* 2. 진행바를 제외한 나머지 부분을 그린다 */
	pDC_Right->FillSolidRect(rcRight, clrBarBack);

//	CString strText;
//	strText.Format(_T("%.1f%%"), fRate);

	//int nOldBkMode = pDC->SetBkMode(TRANSPARENT); // TRANSPARENT
	pDC_Left->SetBkMode(TRANSPARENT); // TRANSPARENT
	pDC_Right->SetBkMode(TRANSPARENT); // TRANSPARENT

	rcLeft.IntersectRect(rcClip, rcLeft);

	/* CRgn class를 사용하는 이유 ? */
	/*  : 문자열의 특정 범위까지만 색상에 변화를 주어야 하기 때문에 */
	/* 1. 진행바 영역에 없을때 문자열 그리기 */
//	CRgn rgn;	// <-- LVS_EX_DOUBLEBUFFER 설정시 MFC버그로인해 CRgn 적용안됨.
//	rgn.CreateRectRgn(rcLeft.left, rcLeft.top, rcLeft.right, rcLeft.bottom);
//	pDC->SelectClipRgn(&rgn);
//	rgn.DeleteObject();
	CFont* old_font = NULL;
	if(m_fontText.GetSafeHandle() != NULL)
		old_font = (CFont*)pDC_Left->SelectObject(&m_fontText);
    pDC_Left->SetTextColor(clrBarBack);
    pDC_Left->DrawText(strText, rcClient, nAlign | DT_VCENTER | DT_SINGLELINE);
	if(old_font)
		pDC_Left->SelectObject(old_font);

	/* 2. 진행바 영역에 있을때 문자열 그리기 */
	rcRight.IntersectRect(rcClip, rcRight);
//	rgn.CreateRectRgn(rcRight.left, rcRight.top, rcRight.right, rcRight.bottom);
//	pDC->SelectClipRgn(&rgn);
//	rgn.DeleteObject();
	old_font = NULL;
	if(m_fontText.GetSafeHandle() != NULL)
		old_font = (CFont*)pDC_Right->SelectObject(&m_fontText);
    pDC_Right->SetTextColor(clrTextFore);
    pDC_Right->DrawText(strText, rcClient, nAlign | DT_VCENTER | DT_SINGLELINE);
	if(old_font)
		pDC_Right->SelectObject(old_font);

//	rgn.CreateRectRgn(rcClip.left, rcClip.top, rcClip.right, rcClip.bottom);
//	pDC->SelectClipRgn(&rgn);
//	rgn.DeleteObject();

	//pDC->SetBkMode(nOldBkMode);
}

void CUTBListCtrlEx::DrawCheckBox(CDC* pDC, int nItem, int nColumn)
{
	if(m_dwMode != LS_CHECKBOX || nColumn != 0)
	{
		return;
	}

	BOOL bCheck = GetCheck(nItem);

	CRect rcBound;
	GetSubItemRect(nItem, nColumn, LVIR_BOUNDS, rcBound);
	CPoint pt = rcBound.TopLeft();
	pt += CPoint(3,1);

	if(bCheck) m_ctrHeader.GetImageList()->DrawEx(pDC, CUTBHeaderCtrl::LC_CHK_SEL  , pt, CSize(0,0), CLR_NONE, CLR_NONE, ILD_NORMAL);
	else       m_ctrHeader.GetImageList()->DrawEx(pDC, CUTBHeaderCtrl::LC_CHK_UNSEL, pt, CSize(0,0), CLR_NONE, CLR_NONE, ILD_NORMAL);
}

void CUTBListCtrlEx::DrawRadioBox(CDC* pDC, int nItem, int nColumn)
{
	if(m_dwMode != LS_RADIOBOX || nColumn != 0)
	{
		return;
	}

	//BOOL bCheck = GetCheck(nItem);
	BOOL bCheck = (GetItemState(nItem, LVIS_FOCUSED) == LVIS_FOCUSED || GetItemState(nItem, LVIS_SELECTED) == LVIS_SELECTED);

	CRect rcBound;
	GetSubItemRect(nItem, nColumn, LVIR_BOUNDS, rcBound);
	CPoint pt = rcBound.TopLeft();
	pt += CPoint(3,1);

	if(bCheck) m_ctrHeader.GetImageList()->DrawEx(pDC, CUTBHeaderCtrl::LC_RADIO_SEL  , pt, CSize(0,0), CLR_NONE, CLR_NONE, ILD_NORMAL);
	else       m_ctrHeader.GetImageList()->DrawEx(pDC, CUTBHeaderCtrl::LC_RADIO_UNSEL, pt, CSize(0,0), CLR_NONE, CLR_NONE, ILD_NORMAL);
}

void CUTBListCtrlEx::DrawImage(CDC* pDC, int nItem, int nColumn)
{
	if((GetExtendedStyle() & LVS_EX_SUBITEMIMAGES) != LVS_EX_SUBITEMIMAGES && nColumn > 0)
	{
		return;
	}

	LVITEM lvi = {0};
	lvi.mask     = LVIF_IMAGE;
	lvi.iItem    = nItem;
	lvi.iSubItem = nColumn;
	GetItem(&lvi);

	if(lvi.iImage < 0) return;

	CImageList* pImageList = GetImageList(LVSIL_SMALL);
	if( pImageList == NULL ) return;

	if(lvi.iImage >= pImageList->GetImageCount()) return;

	CRect rcIcon;
	GetSubItemRect(nItem, nColumn, LVIR_ICON, rcIcon);

	CPoint pt = rcIcon.TopLeft();
	pt += CPoint(1,0);

	pImageList->DrawEx(pDC, lvi.iImage, pt, CSize(0,0), CLR_NONE, CLR_NONE, ILD_NORMAL);
}

void CUTBListCtrlEx::DrawText(CDC* pDC, int nItem, int nColumn, bool bHighlight)
{
	CRect rcIcon, rcLabel;
	GetSubItemRect(nItem, nColumn, LVIR_LABEL, rcLabel);

	if( GetExtendedStyle() & LVS_EX_SUBITEMIMAGES )
	{
		CImageList* pImageList = GetImageList(LVSIL_SMALL);

		if( pImageList )
		{
			GetSubItemRect(nItem, nColumn, LVIR_ICON , rcIcon);

			LVITEM lvi = {0};
			lvi.mask     = LVIF_IMAGE;
			lvi.iItem    = nItem;
			lvi.iSubItem = nColumn;
			GetItem(&lvi);

			CImageList* pImageList = GetImageList(LVSIL_SMALL);
			if( lvi.iImage >= 0 &&
				pImageList      && 
				lvi.iImage < pImageList->GetImageCount() )
			{
				rcLabel.left = rcIcon.right;
			}
		}
	}

	LV_COLUMN lvc = {0};
	lvc.mask = LVCF_FMT | LVCF_WIDTH;
	if( GetColumn(nColumn, &lvc) == NULL ) return;

	UINT nHorzAlign = DT_LEFT;
	if     ((lvc.fmt & LVCFMT_RIGHT ) == LVCFMT_RIGHT ) nHorzAlign = DT_RIGHT ;
	else if((lvc.fmt & LVCFMT_CENTER) == LVCFMT_CENTER) nHorzAlign = DT_CENTER;
	//else if((lvc.fmt & LVCFMT_LEFT  ) == LVCFMT_LEFT  ) nHorzAlign = DT_LEFT  ;

	pDC->SetTextColor((bHighlight) ? m_clrHText : m_clrRowText);

	rcLabel.DeflateRect(2, 2);

	CFont* old_font = NULL;
	if(m_fontText.GetSafeHandle() != NULL)
		old_font = (CFont*)pDC->SelectObject(&m_fontText);

	TCHAR str_text[4096] = {0};
	GetItemText(nItem, nColumn, str_text, 4095);
	pDC->DrawText(str_text, _tcslen(str_text), rcLabel, nHorzAlign | DT_VCENTER | DT_END_ELLIPSIS);

	if(old_font)
		pDC->SelectObject(old_font);
}

void CUTBListCtrlEx::DrawCalendarBar(CDC* pDC, int nItem, int nColumn, bool bHighlight, bool bShowIndicator)
{
	CRect rcClip;
	pDC->GetClipBox(&rcClip);

	CUTBHeaderCtrl::P_HDR_COL_INFO pHdrColInfo = m_ctrHeader.GetColInfo(nColumn);

	CRect rcBound;
	GetSubItemRect(nItem, nColumn, LVIR_BOUNDS, rcBound);

	CRect rcInd(0, rcBound.top, DAY_PIX, rcBound.bottom);
	rcInd.MoveToX(rcBound.left);
	rcInd.DeflateRect(11,0,11,0);

	rcBound.DeflateRect(1,0,0,1);
	pDC->FillSolidRect(rcBound, m_clrRowBkgnd);

	if(m_bDrawGridLine)
	{
		// 구분선을 그린다
		for(int nPos=rcBound.left+DAY_PIX-1 ; nPos < rcBound.right ; nPos+=DAY_PIX)
		{
			pDC->MoveTo(nPos, rcBound.top);
			pDC->LineTo(nPos, rcBound.bottom);
		}
	}

	rcBound.DeflateRect(0,1,0,0);

	// 여기에 새로 만든 DATETIME_INFO 이걸로 그리기 다시 할것.
	// 바 그리고, weekday 및 예외 날짜 그리는 기능도 넣을것.

	// DateList=%s~%s|WeekInfo=%s|ExceptDay=%s
	DATETIME_INFO stDatetimeInfo(GetItemText(nItem, nColumn));

	for(int i=0; i<stDatetimeInfo.arrDateStart.GetCount() ;i++)
	{
		CRect rcItem = rcBound;

		CTime tmStart = stDatetimeInfo.arrDateStart[i];
		CTime tmEnd   = stDatetimeInfo.arrDateEnd  [i];

		// 시작시간이 기준시간보다 작으면 기준시간의 시작시간으로 설정
		if(tmStart < pHdrColInfo->m_tmStartDate)
		{
			tmStart = pHdrColInfo->m_tmStartDate;
		}

		// 종료시간이 기준시간보다 크면 기준시간의 종료시간으로 설정
		if(tmEnd > pHdrColInfo->m_tmEndDate)
		{
			tmEnd = pHdrColInfo->m_tmEndDate;
		}

		// 시작시간과 종료시간이 1시간 간격도 되지 않으면 표시되지 않으므로
		if((tmEnd-tmStart).GetTotalHours() <= 0)
		{
			tmEnd = tmEnd + CTimeSpan(0,1,0,0);
		}

		rcItem.left += (tmStart - pHdrColInfo->m_tmStartDate).GetTotalHours();
		rcItem.right = rcItem.left + (tmEnd - tmStart).GetTotalHours();

		COLOR16 red   = (GetRValue(m_clrCalendar) << 8);
		COLOR16 green = (GetGValue(m_clrCalendar) << 8);
		COLOR16 blue  = (GetBValue(m_clrCalendar) << 8);
		COLOR16 max = (( red > green ? red : green) > blue ? ( red > green ? red : green) : blue);

		COLOR16 red1   = (max > 0x7F00 ? red  /2 :  red *2);
		COLOR16 green1 = (max > 0x7F00 ? green/2 : green*2);
		COLOR16 blue1  = (max > 0x7F00 ? blue /2 : blue *2);

		int nCenterY = rcItem.CenterPoint().y;
		TRIVERTEX vert[4] =
		{   //                              Red     Green   Blue   Alpha
			//{ rcItem.left , rcItem.top   , 0xF000, 0xFF00, 0xFF00, 0x0000 },
			//{ rcItem.right, nCenterY     ,   red1, green1,  blue1, 0x0000 },
			//{ rcItem.left , nCenterY     ,   red1, green1,  blue1, 0x0000 },
			//{ rcItem.right, rcItem.bottom,   red , green ,  blue , 0x0000 }
			{ rcItem.left , rcItem.top   ,   red , green ,  blue , 0x0000 },
			{ rcItem.right, nCenterY     ,   0xF000, 0xFF00, 0xFF00, 0x0000 },
			{ rcItem.left , nCenterY     ,   0xF000, 0xFF00, 0xFF00, 0x0000 },
			{ rcItem.right, rcItem.bottom,   red , green ,  blue , 0x0000 }
		};
		GRADIENT_RECT gRect1 = { 0, 1 };
		GRADIENT_RECT gRect2 = { 2, 3 };

		//CRgn rgn;
		//if((tmEnd-tmStart).GetTotalHours() > 1)
		//{
			rcItem.IntersectRect(rcClip, rcItem);
		//	rgn.CreateRoundRectRgn(rcItem.left, rcItem.top, rcItem.right, rcItem.bottom, 3, 3);
		//	pDC->SelectClipRgn(&rgn);
		//	rgn.DeleteObject();
		//}

		//pDC->GradientFill(vert,4,&gRect1,1,GRADIENT_FILL_RECT_V);
		//pDC->GradientFill(vert,4,&gRect2,1,GRADIENT_FILL_RECT_V);
		pDC->FillSolidRect(rcItem, m_clrCalendar);

		COLORREF clrOldColor = pDC->SetBkColor(RGB( red>>8, green>>8, blue>>8 ));
		CRect rcDay = rcItem;

		int nDayCnt = (tmEnd - tmStart).GetDays();
		CTime tmBase = CTime(tmStart.GetYear(), tmStart.GetMonth(), tmStart.GetDay(), 0, 0, 0);

		for(int nDay=0; nDay<=nDayCnt ; nDay++)
		{
			bool bSkip = false;

			CTime tmTmp = tmBase + CTimeSpan(nDay,0,0,0);

			bool bWeekCheck = false;
			for(int j=0; j<stDatetimeInfo.arrWeekInfo.GetCount() ;j++)
			{
				if(stDatetimeInfo.arrWeekInfo[j] == (tmTmp.GetDayOfWeek()-1))
				{
					bWeekCheck = true;
					break;
				}
			}

			if(!bWeekCheck)
			{
				bSkip = true;
			}

			if(!bSkip)
			{
				for(int j=0; j<stDatetimeInfo.arrExceptDay.GetCount() ;j++)
				{
					if(stDatetimeInfo.arrExceptDay[j].Format(_T("%m%d")) == tmTmp.Format(_T("%m%d")))
					{
						bSkip = true;
						break;
					}
				}
			}

			if(bSkip)
			{
				rcDay.left = rcBound.left + (tmTmp - pHdrColInfo->m_tmStartDate).GetTotalHours();
				rcDay.right = rcDay.left + 24; // 24시간

				pDC->FillSolidRect(rcDay, m_clrRowBkgnd);//m_clrCalendar);//(bHighlight)? m_clrHBkgnd : m_clrRowBkgnd);

				CPen pen(PS_SOLID, 1, RGB(0,0,0));
				CPen* pOldPen = pDC->SelectObject(&pen);
				pDC->MoveTo(rcDay.left , rcDay.top + rcDay.Height()/2-1);
				pDC->LineTo(rcDay.right, rcDay.top + rcDay.Height()/2-1);
				pDC->SelectObject(pOldPen);
			}
		}

		pDC->SetBkColor(clrOldColor);

		//if((tmEnd-tmStart).GetTotalHours() > 1)
		//{
		//	rgn.CreateRectRgn(rcClip.left, rcClip.top, rcClip.right, rcClip.bottom);
		//	pDC->SelectClipRgn(&rgn);
		//	rgn.DeleteObject();
		//}
	}

	if(bShowIndicator)
	{
		rcInd.MoveToX(rcInd.left + (pHdrColInfo->m_tmSelDate - pHdrColInfo->m_tmStartDate).GetTotalHours());
		pDC->FillSolidRect(rcInd, RGB(255,0,0));
	}
}

BOOL CUTBListCtrlEx::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	TRACE("\nOnNMClick");
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	if(	pNMItemActivate->iItem >= 0 &&
		pNMItemActivate->iItem < GetItemCount() )
	{
		if( m_dwMode == LS_CHECKBOX && pNMItemActivate->iSubItem == 0)
		{
			SetCheck(pNMItemActivate->iItem, !GetCheck(pNMItemActivate->iItem));
		}
	}

	*pResult = 0;

	//return 값이 FALSE 이면 부모에게 통지를 하고 TRUE 이면 부모에게 통지를 하지 않는다.
	return FALSE;
}

BOOL CUTBListCtrlEx::OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	TRACE("\nOnLvnColumnclick");
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	if(m_dwMode == LS_CHECKBOX && pNMLV->iSubItem == 0)
	{
		bool bCheck = !GetCheckHdrState();

		if(GetSelectedCount() >= 2)
		{
			POSITION pos = GetFirstSelectedItemPosition();
			while(pos != NULL)
			{
				int nRow = GetNextSelectedItem(pos);
				SetCheck(nRow, bCheck);
			}
		}
		else
		{
			for(int nRow=0; nRow<GetItemCount() ;nRow++)
			{
				SetCheck(nRow, bCheck);
			}
		}

		SetCheckHdrState(bCheck);

		Invalidate();
	}
	else
	{
		SortList(pNMLV->iSubItem, PFNLVCOMPARE(Compare), (DWORD_PTR)this);
	}

	CPoint ptCurPos;
	if(::GetCursorPos(&ptCurPos))
	{
		ScreenToClient(&ptCurPos);

		CUTBHeaderCtrl::ST_HDR_COL_INFO stHdrColInfo;
		if(GetHdrHitInfo(ptCurPos, stHdrColInfo))
		{
			CUTBHeaderCtrl::P_HDR_COL_INFO pHdrColInfo = m_ctrHeader.GetColInfo(pNMLV->iSubItem);
			pHdrColInfo->m_tmSelDate = stHdrColInfo.m_tmSelDate;
		}
	}

	*pResult = 0;

	//return 값이 FALSE 이면 부모에게 통지를 하고 TRUE 이면 부모에게 통지를 하지 않는다.
	return FALSE;
}

void CUTBListCtrlEx::SetProgressBarCol(int nColumn)
{
	m_ctrHeader.SetColType(nColumn, CUTBHeaderCtrl::COL_PROGRESS);
}

void CUTBListCtrlEx::SetProgressBarCol(int nColumn, COLORREF clrBarFore, COLORREF clrBarBack, COLORREF clrTextFore)
{	
	m_ctrHeader.SetColType(nColumn, CUTBHeaderCtrl::COL_PROGRESS);
	m_clrBarFore  = clrBarFore;
	m_clrBarBack  = clrBarBack;
	m_clrTextFore = clrTextFore;
}

void CUTBListCtrlEx::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	TEXTMETRIC tm = {0};
	HDC hDC = ::GetDC(NULL);
	CFont* pFont = GetFont();
	HFONT hFontOld = (HFONT)SelectObject(hDC, pFont->GetSafeHandle());
	GetTextMetrics(hDC, &tm);
	lpMeasureItemStruct->itemHeight = tm.tmHeight + tm.tmExternalLeading + 4; 
	SelectObject(hDC, hFontOld); 
	::ReleaseDC(NULL, hDC);

	/*LOGFONT lf;
	GetFont()->GetLogFont( &lf );

	if( lf.lfHeight < 0 )
		lpMeasureItemStruct->itemHeight = -lf.lfHeight; 
	else
		lpMeasureItemStruct->itemHeight = lf.lfHeight; 

	if(lpMeasureItemStruct->itemHeight > 0)
		lpMeasureItemStruct->itemHeight += 10;*/
}

LRESULT CUTBListCtrlEx::OnSetFont(WPARAM wParam, LPARAM)
{
	LRESULT res =  Default();

	CRect rc;
	GetWindowRect( &rc );

	WINDOWPOS wp = {0};
	wp.hwnd = m_hWnd;
	wp.cx = rc.Width();
	wp.cy = rc.Height();
	wp.flags = SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER;
	SendMessage( WM_WINDOWPOSCHANGED, 0, (LPARAM)&wp );

	return res;
}

void CUTBListCtrlEx::SetRowColor(int nItem, COLORREF clrBack, COLORREF clrText)
{
	P_ITEM_DATA pKey = (P_ITEM_DATA)CListCtrl::GetItemData(nItem);
	P_ITEM_DATA pItemData = NULL;

	if(!m_mapItemData.Lookup(pKey, (void*&)pItemData))
	{
		pItemData = new ST_ITEM_DATA;
		m_mapItemData[pItemData] = pItemData;
	}

	pItemData->clrBack = clrBack;
	pItemData->clrText = clrText;

	CListCtrl::SetItemData(nItem, (DWORD_PTR)pItemData);
}

BOOL CUTBListCtrlEx::GetRowColor(int nItem, COLORREF& clrBack, COLORREF& clrText)
{
	P_ITEM_DATA pKey = (P_ITEM_DATA)CListCtrl::GetItemData(nItem);
	P_ITEM_DATA pItemData = NULL;

	if(m_mapItemData.Lookup(pKey, (void*&)pItemData))
	{
		clrBack = pItemData->clrBack;
		clrText = pItemData->clrText;
		return TRUE;
	}

	return FALSE;
}

void CUTBListCtrlEx::DeleteAllItemData()
{
	POSITION pos = m_mapItemData.GetStartPosition();
	while( pos )
	{
		P_ITEM_DATA pKey = NULL;
		P_ITEM_DATA pValue = NULL;
		m_mapItemData.GetNextAssoc(pos, (void*&)pKey, (void*&)pValue);
		if(pValue)
		{
			delete pValue;
		}
	}

	m_mapItemData.RemoveAll();
}

void CUTBListCtrlEx::HideScrollBars(int nScrollBar)
{
	m_nHideScrollBar = nScrollBar; //Selects which scrollbars to get hidden.
}

void CUTBListCtrlEx::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{
	switch(m_nHideScrollBar)
	{
	case SB_HORZ:
		ModifyStyle(WS_HSCROLL,0,0); //Just by modifing the style we remove the scrollbar
		break;
	case SB_VERT:
		ModifyStyle(WS_VSCROLL,0,0);
		break;
	case SB_BOTH:
		ModifyStyle(WS_HSCROLL | WS_VSCROLL,0,0);
		break;
	default:
		break;
	}

	CListCtrl::OnNcCalcSize(bCalcValidRects, lpncsp);
}

LRESULT CUTBListCtrlEx::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
	case LVM_INSERTITEMA:
	case LVM_INSERTITEMW:
		{
			LV_ITEM *pItem = (LV_ITEM *)(lParam);

			P_ITEM_DATA pItemData = new ST_ITEM_DATA;
			m_mapItemData[pItemData] = pItemData;

			pItemData->clrBack = m_clrBkgnd;
			pItemData->clrText = m_clrText;

			pItem->mask |= LVIF_PARAM;
			pItem->lParam = (LPARAM)pItemData;
		}
		break;

	case LVM_DELETEITEM:
		{
			P_ITEM_DATA pKey = (P_ITEM_DATA)CListCtrl::GetItemData((int)wParam);
			P_ITEM_DATA pItemData = NULL;

			if( m_mapItemData.Lookup(pKey, (void*&)pItemData) &&
				pItemData != NULL                             )
			{
				m_mapItemData.RemoveKey(pKey);
				delete pItemData;
			}
		}
		break;

	case LVM_DELETEALLITEMS:
		DeleteAllItemData();
		break;

	default:
		break;
	}

	return CListCtrl::DefWindowProc(message, wParam, lParam);
}

BOOL CUTBListCtrlEx::OnLvnGetdispinfo(NMHDR *pNMHDR, LRESULT *pResult)
{
//	TRACE("\nOnLvnGetdispinfo");
	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	//return 값이 FALSE 이면 부모에게 통지를 하고 TRUE 이면 부모에게 통지를 하지 않는다.
	return FALSE;
}

BOOL CUTBListCtrlEx::GetSubItemRect(int iItem, int iSubItem, int nArea, CRect& ref)
{
	if(iSubItem == 0 && LVIR_BOUNDS == nArea && GetFixedCol() >= 0)
	{
		// Draw labels for remaining columns
		LV_COLUMN lvc = {0};
		lvc.mask = LVCF_FMT | LVCF_WIDTH;

		if( GetColumn(iSubItem, &lvc) == NULL ) return FALSE;

		CRect rcItemBound;
		CListCtrl::GetSubItemRect(iItem, iSubItem, LVIR_BOUNDS, rcItemBound);

		if(iSubItem <= 2)
		{
			rcItemBound.left += GetScrollPos(SB_HORZ);
			rcItemBound.right = rcItemBound.left+lvc.cx;
		}

		ref = rcItemBound;
	}
	else
	{
		return CListCtrl::GetSubItemRect(iItem, iSubItem, nArea, ref);
	}

	return TRUE;
}

void CUTBListCtrlEx::OnHdnBegintrack(NMHDR *pNMHDR, LRESULT *pResult)
{
	TRACE("\nOnHdnBegintrack");
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);

	// 컬럼의 크기를 조정하지 못하도록 아무것도 안하기
	CUTBHeaderCtrl::HDR_COL_TYPE col_type = m_ctrHeader.GetColType(phdr->iItem);
	if( col_type == CUTBHeaderCtrl::COL_CALENDAR ||
		col_type == CUTBHeaderCtrl::COL_INDICATOR_CALENDAR )
	{
		*pResult = TRUE;
		return;
	}

	*pResult = 0;
}

CTime CUTBListCtrlEx::ToDateTime(CString strValue)
{
	// %Y/%m/%d %H:%M:%S
	COleDateTime tm;
	tm.ParseDateTime(strValue);

	return CTime( tm.GetYear()
				, tm.GetMonth()
				, tm.GetDay()
				, tm.GetHour()
				, tm.GetMinute()
				, tm.GetSecond()
				);
}

void CUTBListCtrlEx::SaveColumnState(CString strListName, CString strIniPath)
{
	// 컬럼순서 저장
	CHeaderCtrl* pHdr = GetHeaderCtrl();
	int nCount = pHdr->GetItemCount();
	
	LPINT nColumnOrders = (LPINT)new int[nCount];
	memset(nColumnOrders, 0x00, sizeof(int)*nCount);

    pHdr->GetOrderArray(nColumnOrders, nCount);

	CString strColumnsOrder;
	for(int i=0; i<nCount ;i++)
	{
		strColumnsOrder += ToString(nColumnOrders[i]);
		strColumnsOrder += ";";
	}

	// 컬럼너비 저장
	CString strColumnsWidth;
	for(int i=0; i<nCount ;i++)
	{
		strColumnsWidth += ToString(GetColumnWidth(i));
		strColumnsWidth += ";";
	}

	WritePrivateProfileString(strListName, "ColumnCount", ToString(nCount), strIniPath);
	WritePrivateProfileString(strListName, "OrderList", strColumnsOrder, strIniPath);
	WritePrivateProfileString(strListName, "WidthList", strColumnsWidth, strIniPath);
	WritePrivateProfileString(strListName, "SortColumn", ToString(GetCurSortCol()), strIniPath);
	WritePrivateProfileString(strListName, "IsAscend", ToString(IsAscend()), strIniPath);

	delete nColumnOrders;
}

void CUTBListCtrlEx::LoadColumnState(CString strListName, CString strIniPath, int nDefaultSortCol/*=-1*/)
{
	CHeaderCtrl* pHdr = GetHeaderCtrl();
	int nCount = pHdr->GetItemCount();

	char buf[1024] = {0};

	GetPrivateProfileString(strListName, "ColumnCount", "0", buf, 1024, strIniPath);
	int nSavedCount = _ttoi(buf);
	GetPrivateProfileString(strListName, "OrderList", "", buf, 1024, strIniPath);
	CString strOrderList = buf;
	GetPrivateProfileString(strListName, "WidthList", "", buf, 1024, strIniPath);
	CString strWidthList = buf;

	GetPrivateProfileString(strListName, "SortColumn", "", buf, 1024, strIniPath);
	if(_tcslen(buf) > 0 || nDefaultSortCol >= 0)
	{
		int nSortCol = (_tcslen(buf) > 0 ? _ttoi(buf) : nDefaultSortCol);

		GetPrivateProfileString(strListName, "IsAscend", "1", buf, 1024, strIniPath);
		bool bIsAscend = (_ttoi(buf) != 0);

		if(nSortCol >= nCount)
		{
			nSortCol = nDefaultSortCol;
			bIsAscend = true;
		}

		//SetSortCol(nSortCol, bIsAscend);
		SortList(nSortCol, bIsAscend, (PFNLVCOMPARE)CUTBListCtrlEx::Compare, (DWORD_PTR)this);
	}

	if(nSavedCount != nCount) return;

	if(!strOrderList.IsEmpty() && (GetExtendedStyle() & LVS_EX_HEADERDRAGDROP))
	{
		LPINT nColumnOrders = (LPINT)new int[nCount];
		memset(nColumnOrders, 0x00, sizeof(int)*nCount);

		int nIndex = 0;
		int nPos = 0;
		CString strOrder = strOrderList.Tokenize(";", nPos);
		while(strOrder != "")
		{
			nColumnOrders[nIndex] = _ttoi(strOrder);
			strOrder = strOrderList.Tokenize(";", nPos);
			nIndex++;
		}

		pHdr->SetOrderArray(nCount, nColumnOrders);

		delete nColumnOrders;
	}

	if(!strWidthList.IsEmpty())
	{
		int nIndex = 0;
		int nPos = 0;
		CString strWidth = strWidthList.Tokenize(";", nPos);
		while(strWidth != "")
		{
			SetColumnWidth(nIndex, _ttoi(strWidth));
			strWidth = strWidthList.Tokenize(";", nPos);
			nIndex++;
		}
	}
}

// Sets the data (lParam) associated with a particular item.
BOOL CUTBListCtrlEx::SetItemData(int nItem, DWORD_PTR dwData)
{
	P_ITEM_DATA pKey = (P_ITEM_DATA)CListCtrl::GetItemData(nItem);
	P_ITEM_DATA pItemData = NULL;

	if( m_mapItemData.Lookup(pKey, (void*&)pItemData) == NULL )
	{
		pItemData = new ST_ITEM_DATA;
		m_mapItemData[pItemData] = pItemData;
	}

	pItemData->dwData = dwData;

	return CListCtrl::SetItemData(nItem, (DWORD_PTR)pItemData);
}

// Retrieves the data (lParam) associated with a particular item.
DWORD_PTR CUTBListCtrlEx::GetItemData(int nItem)
{
	if(nItem < 0) return NULL;

	P_ITEM_DATA pItemData = (P_ITEM_DATA)CListCtrl::GetItemData(nItem);
	if( pItemData == NULL ) return NULL;

	return pItemData->dwData;
}

BOOL CUTBListCtrlEx::SetProgressBarText(int nRow, int nCol, float fProgress, int nAlign, LPCTSTR szText, BOOL bGradientFill, COLORREF clrBarFore, COLORREF clrBarBack, COLORREF clrTextFore)
{
	CString strData;
	strData.Format("%f`%d`%d`%d`%d`%d`%s"
				, fProgress
				, bGradientFill
				, clrBarFore, clrBarBack, clrTextFore
				, nAlign
				, szText
				);

	return SetItemText(nRow, nCol, strData);
}

void CUTBListCtrlEx::GetProgressBarText(int nRow, int nCol, float& fProgress, int& nAlign, CString& strText, BOOL& bGradientFill, COLORREF& clrBarFore, COLORREF& clrBarBack, COLORREF& clrTextFore)
{
	fProgress = 0.;
	bGradientFill = TRUE;
	clrBarFore = m_clrBarFore;
	clrBarBack = m_clrBarBack;
	clrTextFore = m_clrTextFore;
	nAlign = DT_CENTER;
	strText = "";

	CString strData = GetItemText(nRow, nCol);

	if(strData.IsEmpty()) return;

	int nStart = 0;
	int nPos = strData.Find("`", nStart);
	if( nPos < 0)
	{
		fProgress = _tstof(strData.Mid(nStart));
		strText = strData;
		return;
	}
	fProgress = _tstof(strData.Mid(nStart, nPos-nStart));
	nStart = nPos + 1;

	nPos = strData.Find("`", nStart);
	if( nPos < 0 ) return;
	bGradientFill = (_ttoi(strData.Mid(nStart, nPos-nStart)) == TRUE);
	nStart = nPos + 1;

	nPos = strData.Find("`", nStart);
	if( nPos < 0 ) return;
	clrBarFore = (DWORD)_ttoi(strData.Mid(nStart, nPos-nStart));
	nStart = nPos + 1;

	nPos = strData.Find("`", nStart);
	if( nPos < 0 ) return;
	clrBarBack = (DWORD)_ttoi(strData.Mid(nStart, nPos-nStart));
	nStart = nPos + 1;

	nPos = strData.Find("`", nStart);
	if( nPos < 0 ) return;
	clrTextFore = (DWORD)_ttoi(strData.Mid(nStart, nPos-nStart));
	nStart = nPos + 1;

	nPos = strData.Find("`", nStart);
	if( nPos < 0 ) return;
	nAlign = _ttoi(strData.Mid(nStart, nPos-nStart));
	nStart = nPos + 1;

	strText = strData.Mid(nStart);
}

int CUTBListCtrlEx::GetRadioSelectedIndex()
{
	POSITION pos = GetFirstSelectedItemPosition();
	if( pos == NULL ) return -1;
	return GetNextSelectedItem(pos);
}

BOOL CUTBListCtrlEx::GetHdrHitInfo(CPoint point, CUTBHeaderCtrl::ST_HDR_COL_INFO& stHdrColInfo)
{
    CRect rcList;
    GetWindowRect(&rcList);
    ScreenToClient(reinterpret_cast<LPPOINT>(&rcList));

    LVHITTESTINFO HitInfo = {0};
    HitInfo.pt.x = point.x - rcList.left;
    HitInfo.pt.y = point.y - rcList.top;
 
	//HitTest(&HitInfo);
	SubItemHitTest(&HitInfo);

	if(HitInfo.iSubItem < 0) return FALSE;

	CUTBHeaderCtrl::P_HDR_COL_INFO pHdrColInfo = m_ctrHeader.GetColInfo(HitInfo.iSubItem);
	if( pHdrColInfo == NULL ) return FALSE;

	stHdrColInfo = *pHdrColInfo;
	if(pHdrColInfo->m_nColType != CUTBHeaderCtrl::COL_INDICATOR_CALENDAR) return FALSE;

	CRect rcBound;
	GetSubItemRect(-1, HitInfo.iSubItem, LVIR_BOUNDS, rcBound);

	if( (rcBound.top + (rcBound.Height() / 4)) > point.y ) return FALSE;

	TRACE(pHdrColInfo->m_tmStartDate.Format("\n%Y/%m/%d"));
	stHdrColInfo.m_tmSelDate = pHdrColInfo->m_tmStartDate + CTimeSpan(((HitInfo.pt.x - rcBound.left) / DAY_PIX),0,0,0);
	TRACE(stHdrColInfo.m_tmSelDate.Format("\n%Y/%m/%d"));

	return TRUE;
}

void CUTBListCtrlEx::CellHitTest(const CPoint& pt, int& nRow, int& nCol) const
{
	nRow = -1;
	nCol = -1;

	LVHITTESTINFO lvhti = {0};
	lvhti.pt = pt;
	nRow = ListView_SubItemHitTest(m_hWnd, &lvhti);	// SubItemHitTest is non-const
	nCol = lvhti.iSubItem;
	//if (!(lvhti.flags & LVHT_ONITEMLABEL))
	if (!(lvhti.flags & LVHT_ONITEM))
		nRow = -1;
}

BOOL CUTBListCtrlEx::OnToolNeedText(UINT id, NMHDR* pNMHDR, LRESULT* pResult)
{
	CPoint pt(GetMessagePos());
	ScreenToClient(&pt);

	int nRow, nCol;
	CellHitTest(pt, nRow, nCol);

	CString tooltip = GetToolTipText(nRow, nCol);
	if (tooltip.IsEmpty())
		return FALSE;

	// Non-unicode applications can receive requests for tooltip-text in unicode
	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
#ifndef _UNICODE
	if (pNMHDR->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, static_cast<LPCTSTR>(tooltip), sizeof(pTTTA->szText));
	else
		_mbstowcsz(pTTTW->szText, static_cast<LPCTSTR>(tooltip), sizeof(pTTTW->szText)/sizeof(WCHAR));
#else
	if (pNMHDR->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, static_cast<LPCTSTR>(tooltip), sizeof(pTTTA->szText));
	else
		lstrcpyn(pTTTW->szText, static_cast<LPCTSTR>(tooltip), sizeof(pTTTW->szText)/sizeof(WCHAR));
#endif
	// If wanting to display a tooltip which is longer than 80 characters,
	// then one must allocate the needed text-buffer instead of using szText,
	// and point the TOOLTIPTEXT::lpszText to this text-buffer.
	// When doing this, then one is required to release this text-buffer again
	return TRUE;
}

bool CUTBListCtrlEx::ShowToolTip(const CPoint& pt) const
{
	// Lookup up the cell
	int nRow, nCol;
	CellHitTest(pt, nRow, nCol);

	if (nRow!=-1 && nCol!=-1)
		return true;
	else
		return false;
}

CString CUTBListCtrlEx::GetToolTipText(int nRow, int nCol)
{
	if (nRow < 0 || nCol < 0) return CString("");

	CString strCellText = GetItemText(nRow, nCol);
	if(GetColType(nCol) == CUTBHeaderCtrl::COL_PROGRESS && strCellText.ReverseFind(_T('`')) >= 0)
	{
		strCellText = strCellText.Mid(strCellText.ReverseFind(_T('`'))+1);
	}

	return strCellText;	// Cell-ToolTip
}

INT_PTR CUTBListCtrlEx::OnToolHitTest(CPoint point, TOOLINFO * pTI) const
{
	CPoint pt(GetMessagePos());
	ScreenToClient(&pt);
	if (!ShowToolTip(pt))
		return -1;

	int nRow, nCol;
	CellHitTest(pt, nRow, nCol);

	//Get the client (area occupied by this control
	RECT rcClient = {0};
	GetClientRect( &rcClient );

	//Fill in the TOOLINFO structure
	pTI->hwnd = m_hWnd;
	pTI->uId = (UINT) (nRow * 1000 + nCol);
	pTI->lpszText = LPSTR_TEXTCALLBACK;	// Send TTN_NEEDTEXT when tooltip should be shown
	pTI->rect = rcClient;

	return pTI->uId; // Must return a unique value for each cell (Marks a new tooltip)
}

void CUTBListCtrlEx::SetTextFont(LPTSTR lpszFontName, int nFontSize)
{
	if(m_fontText.GetSafeHandle() != NULL)
		m_fontText.DeleteObject();

	m_fontText.CreatePointFont(nFontSize*10, lpszFontName);

	SetFont(&m_fontText);
}
