#pragma once

//typedef unsigned long int UINT4;
typedef unsigned int UINT4;

typedef struct tagMD5_CTX
{
	UINT4 i[2];
	UINT4 buf[4];
	unsigned char in[64];
	unsigned char digest[16];
} MD5_CTX, *P_MD5_CTX;

class CMD5Util
{
private:
	static CMD5Util* _instance;
	unsigned char PADDING[64];

public:
	CMD5Util(void);
	~CMD5Util(void);

	static	CMD5Util* GetInstance();

	bool MDString(char *inString, char *outString);
	bool MDFile(char *filename, char *outString);

	void MDPrint (MD5_CTX *mdContext);
	void ToString (MD5_CTX *mdContext, char *outString);

private:
	void MD5Init(MD5_CTX *mdContext);
	void MD5Update(MD5_CTX *mdContext, unsigned char *inBuf, unsigned int inLen);
	void MD5Final(MD5_CTX *mdContext);
	void Transform (UINT4 *buf, UINT4 *in);
};
