// libXmlParser.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "common/libXmlParser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// 생성자
CXmlParser::CXmlParser()
{
	ClearErrMsg();
	m_pXMLDoc = NULL;
	m_pNodeList = NULL;

//	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	try
	{
		CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER, IID_IXMLDOMDocument, (void**)&m_pXMLDoc);
		m_pXMLDoc->put_async(FALSE);
		m_pXMLDoc->put_preserveWhiteSpace(TRUE);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("XML-DOM Init error"));
		m_pXMLDoc = NULL;
	}
}

// 소멸자
CXmlParser::~CXmlParser()
{
//	CoUninitialize();
}

void CXmlParser::GetErrMessage(int nErrCode, CString strDefaultMsg /*= ""*/)
{
	m_nErrCode = nErrCode;
	CString strErrMsg;

	LPVOID lpMsgBuf = NULL;

	if(nErrCode != 0)
	{
		FormatMessage(  FORMAT_MESSAGE_ALLOCATE_BUFFER
					  | FORMAT_MESSAGE_IGNORE_INSERTS 
					  | FORMAT_MESSAGE_FROM_SYSTEM     
					  ,	NULL
					  , nErrCode
					  , 0
					  , (LPTSTR)&lpMsgBuf
					  , 0
					  , NULL );
	}

	if(!lpMsgBuf)
	{
		strErrMsg.Format(_T("%s [%d]"), strDefaultMsg, nErrCode);
	}
	else
	{
		strErrMsg.Format(_T("%s [%d]"), (LPCTSTR)lpMsgBuf, nErrCode);
	}

	LocalFree( lpMsgBuf );

	m_strErrMsg = strErrMsg;
}

CString CXmlParser::GetLastErrorMsg(void)
{
	return m_strErrMsg;
}

int	CXmlParser::GetErrCode()
{
	return m_nErrCode;
}

void CXmlParser::ClearErrMsg()
{
	m_nErrCode = XMLPARSER_SUCCESS;
	m_strErrMsg = _T("");
}

// 화일로부터 XML을 로드하여 파싱
int CXmlParser::Load(CString strXmlFile)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}

	m_strXmlFile = _T("");

	try
	{
		CComVariant varXmlFile(strXmlFile);
		VARIANT_BOOL bIsSuccess = S_OK;

		m_pXMLDoc->load(varXmlFile, &bIsSuccess);

		if(!bIsSuccess)
		{
			GetErrMessage(GetLastError(), _T("Xml file load error"));
			return XMLPARSER_APPERR;
		}
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("Xml file load error"));
		return XMLPARSER_SYSTEMERR;
	}

	m_strXmlFile = strXmlFile;

	return XMLPARSER_SUCCESS;
}

// String으로부터 XML을 로드하여 파싱
int CXmlParser::LoadXML(CString strXmlString)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}

	m_strXmlFile = _T("");

	try
	{
		CComBSTR bstrXmlString(strXmlString);
		VARIANT_BOOL bIsSuccess = S_OK;

		m_pXMLDoc->loadXML(bstrXmlString, &bIsSuccess);

		if(!bIsSuccess)
		{
			GetErrMessage(GetLastError(), _T("Xml string load error"));
			return XMLPARSER_APPERR;
		}
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("Xml string load error"));
		return XMLPARSER_SYSTEMERR;
	}

	return XMLPARSER_SUCCESS;
}

// 파일로 XML Document를 저장
int CXmlParser::Save(CString strXmlFile /*= ""*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}

	if(strXmlFile.IsEmpty()) strXmlFile = m_strXmlFile;
	if(strXmlFile.IsEmpty())
	{
		GetErrMessage(0, _T("XmlFile is not exist to save"));
		return XMLPARSER_APPERR;
	}

	CComVariant varXmlFile(strXmlFile);

	HRESULT hResult = XMLPARSER_SUCCESS;
	try
	{
		hResult = m_pXMLDoc->save(varXmlFile);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("XmlFile Save error"));
		m_pNodeList = NULL;
		return XMLPARSER_SYSTEMERR;
	}

	return (hResult == S_OK ? XMLPARSER_SUCCESS : XMLPARSER_APPERR);
}

// 마지막 실행한 XPath를 리턴
CString CXmlParser::GetXPath()
{
	ClearErrMsg();

	return m_strXPath;
}

// XPath로 Document에서 요소들을 추출한다.
int CXmlParser::SetXPath(CString strXPath)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}

	try
	{
		CComBSTR bstrXPath(strXPath);
		m_pXMLDoc->selectNodes(bstrXPath, &m_pNodeList);
		m_strXPath = strXPath;
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("Set XPath error"));
		m_pNodeList = NULL;
		return XMLPARSER_SYSTEMERR;
	}

	return XMLPARSER_SUCCESS;
}

// XPath로 추출한 요소들의 수
long CXmlParser::GetSelCount()
{
	ClearErrMsg();

	if(!m_pNodeList) return -1;

	long lCnt = 0;

	try
	{
		m_pNodeList->get_length(&lCnt);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("Select count error"));
		return -1;
	}

	return lCnt;
}

// XPath로 추출한 요소들을 XML String으로 리턴한다.
CString CXmlParser::GetXML(CString strXPath /*= ""*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return _T("");
	}

	if(!strXPath.IsEmpty()) SetXPath(strXPath);

	CString strXML;

	try
	{
		long lCnt = GetSelCount();
		for(long i=0; i<lCnt ;i++)
		{
			IXMLDOMNode * pSelXDN;
			m_pNodeList->get_item(i, &pSelXDN);
			if(pSelXDN == NULL) return _T("");

			CComBSTR bstrTmp;
			pSelXDN->get_xml(&bstrTmp);

			strXML += CString(bstrTmp) + _T("\n");
		}
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("GetXML error"));
		return _T("");
	}

	return strXML;
}

// XPath로 추출한 요소들의 값을 String으로 리턴한다.
CString CXmlParser::GetText(CString strXPath /*= ""*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return _T("");
	}

	if(!strXPath.IsEmpty()) SetXPath(strXPath);

	CString strText;

	try
	{
		long lCnt = GetSelCount();
		for(long i=0; i<lCnt ;i++)
		{
			IXMLDOMNode * pSelXDN;
			m_pNodeList->get_item(i, &pSelXDN);
			if(pSelXDN == NULL) return _T("");

			CComBSTR bstrTmp;
			pSelXDN->get_text(&bstrTmp);

			strText += CString(bstrTmp) + _T("\n");
		}
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("GetText error"));
		return _T("");
	}

	return strText;
}

// XPath로 추출한 요소들의 값을 String으로 리턴한다.
CString CXmlParser::GetData(long lIndex /*= 0*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return _T("");
	}

	if(!m_pNodeList || lIndex < 0)
	{
		GetErrMessage(0, _T("Element is not exist"));
		return _T("");
	}

	CString strText;

	try
	{
		long lCnt = GetSelCount();
		for(long i=0; i<lCnt ;i++)
		{
			if(lIndex != i) continue;

			IXMLDOMNode * pSelXDN;
			m_pNodeList->get_item(i, &pSelXDN);
			if(pSelXDN == NULL) return _T("");

			CComBSTR bstrTmp;
			pSelXDN->get_text(&bstrTmp);

			strText = CString(bstrTmp);
		}
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("GetText error"));
		return _T("");
	}

	return strText;
}

// Element의 값을 리턴한다.
CString CXmlParser::GetElement(CString strName, long lIndex /*= 0*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return _T("");
	}
	
	if(!m_pNodeList || lIndex < 0)
	{
		GetErrMessage(0, _T("Element is not exist"));
		return _T("");
	}

	try
	{
		IXMLDOMNode * pSelXDN;
		IXMLDOMNode * pResultXDN;

		m_pNodeList->get_item(lIndex, &pSelXDN);
		if(pSelXDN == NULL) return _T("");

		CComBSTR bstrXPath(strName);
		pSelXDN->selectSingleNode(bstrXPath, &pResultXDN);
		if(pResultXDN == NULL) return _T("");

		CComBSTR bstrResult;
		pResultXDN->get_text(&bstrResult);

		return CString(bstrResult);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("GetElement error"));
		return _T("");
	}
}

// Element에 값을 설정한다.
int CXmlParser::SetElement(CString strName, CString strValue, long lIndex /*= 0*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}
	
	if(!m_pNodeList || lIndex < 0)
	{
		GetErrMessage(0, _T("Element is not exist"));
		return XMLPARSER_APPERR;
	}

	try
	{
		IXMLDOMNode * pSelXDN;
		IXMLDOMNode * pResultXDN;

		m_pNodeList->get_item(lIndex, &pSelXDN);
		if(pSelXDN == NULL) return XMLPARSER_APPERR;

		CComBSTR bstrXPath(strName);
		pSelXDN->selectSingleNode(bstrXPath, &pResultXDN);
		if(pResultXDN == NULL) return XMLPARSER_APPERR;

		CComBSTR bstrValue(strValue);
		pResultXDN->put_text(bstrValue);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("SetElement error"));
		return XMLPARSER_SYSTEMERR;
	}

	return XMLPARSER_SUCCESS;
}

// 새로운 Element를 생성한다.
int CXmlParser::NewElement(CString strName, long lIndex /*= 0*/, CString strValue /*= ""*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}
	
	if(!m_pNodeList || lIndex < 0)
	{
		GetErrMessage(0, _T("Element is not exist"));
		return XMLPARSER_APPERR;
	}

	try
	{
		IXMLDOMNode * pSelXDN;
		IXMLDOMNode * pNewXDN;
		IXMLDOMNode * pResultXDN;

		m_pNodeList->get_item(lIndex, &pSelXDN);
		if(pSelXDN == NULL) return XMLPARSER_APPERR;

		CComVariant varType(NODE_ELEMENT);
		CComBSTR bstrName(strName);
		CComBSTR bstrUri;
		pSelXDN->get_namespaceURI(&bstrUri);

		m_pXMLDoc->createNode(varType, bstrName, bstrUri, &pNewXDN);

		pSelXDN->appendChild(pNewXDN, &pResultXDN);
		if(pResultXDN == NULL) return XMLPARSER_APPERR;

		CComBSTR bstrValue(strValue);
		pResultXDN->put_text(bstrValue);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("NewElement error"));
		return XMLPARSER_SYSTEMERR;
	}

	return XMLPARSER_SUCCESS;
}

// Element를 삭제한다.
int CXmlParser::DeleteElement(CString strName, long lIndex /*= 0*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}
	
	if(!m_pNodeList || lIndex < 0)
	{
		GetErrMessage(0, _T("Element is not exist"));
		return XMLPARSER_APPERR;
	}

	try
	{
		IXMLDOMNode * pSelXDN;
		IXMLDOMNode * pResultXDN;

		m_pNodeList->get_item(lIndex, &pSelXDN);
		if(pSelXDN == NULL) return XMLPARSER_APPERR;

		CComBSTR bstrXPath(strName);
		pSelXDN->selectSingleNode(bstrXPath, &pResultXDN);
		if(pResultXDN == NULL) return XMLPARSER_APPERR;

		pSelXDN->removeChild(pResultXDN, NULL);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("DeleteElement error"));
		return XMLPARSER_SYSTEMERR;
	}

	return XMLPARSER_SUCCESS;
}

// Attribute의 값을 리턴한다.
CString CXmlParser::GetAttribute(CString strName, long lIndex /*= 0*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return _T("");
	}
	
	if(!m_pNodeList || lIndex < 0)
	{
		GetErrMessage(0, _T("Attribute is not exist"));
		return _T("");
	}

	try
	{
		IXMLDOMNode * pSelXDN;
		IXMLDOMNode * pResultXDN;
		IXMLDOMNamedNodeMap * pNodeAttr;

		m_pNodeList->get_item(lIndex, &pSelXDN);
		if(pSelXDN == NULL) return _T("");

		pSelXDN->get_attributes(&pNodeAttr);
		if(pNodeAttr == NULL) return _T("");

		CComBSTR bstrXPath(strName);
		pNodeAttr->getNamedItem(bstrXPath, &pResultXDN);
		if(pResultXDN == NULL) return _T("");

		CComBSTR bstrResult;
		pResultXDN->get_text(&bstrResult);

		return CString(bstrResult);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("GetAttribute error"));
		return _T("");
	}
}

// Attribute에 값을 설정한다.
int CXmlParser::SetAttribute(CString strName, CString strValue, long lIndex /*= 0*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}

	if(!m_pNodeList || lIndex < 0)
	{
		GetErrMessage(0, _T("Attribute is not exist"));
		return XMLPARSER_APPERR;
	}

	try
	{
		IXMLDOMNode * pSelXDN;
		IXMLDOMNode * pResultXDN;
		IXMLDOMNamedNodeMap * pNodeAttr;

		m_pNodeList->get_item(lIndex, &pSelXDN);
		if(pSelXDN == NULL) return XMLPARSER_APPERR;

		pSelXDN->get_attributes(&pNodeAttr);
		if(pNodeAttr == NULL) return XMLPARSER_APPERR;

		CComBSTR bstrXPath(strName);
		pNodeAttr->getNamedItem(bstrXPath, &pResultXDN);
		if(pResultXDN == NULL) return XMLPARSER_APPERR;

		CComBSTR bstrValue(strValue);
		pResultXDN->put_text(bstrValue);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("SetAttribute error"));
		return XMLPARSER_SYSTEMERR;
	}

	return XMLPARSER_SUCCESS;
}

// 새로운 Attribute를 생성한다.
int CXmlParser::NewAttribute(CString strName, long lIndex /*= 0*/, CString strValue /*= ""*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}
	
	if(!m_pNodeList || lIndex < 0)
	{
		GetErrMessage(0, _T("Attribute is not exist"));
		return XMLPARSER_APPERR;
	}

	try
	{
		IXMLDOMNode * pSelXDN;
		IXMLDOMNamedNodeMap * pNodeAttr;
		IXMLDOMNode * pNewXDN;
		IXMLDOMNode * pResultXDN;

		m_pNodeList->get_item(lIndex, &pSelXDN);
		if(pSelXDN == NULL) return XMLPARSER_APPERR;

		CComVariant varType(NODE_ATTRIBUTE);
		CComBSTR bstrName(strName);
		CComBSTR bstrUri;
		pSelXDN->get_namespaceURI(&bstrUri);

		m_pXMLDoc->createNode(varType, bstrName, bstrUri, &pNewXDN);

		pSelXDN->get_attributes(&pNodeAttr);
		if(pNodeAttr == NULL) return XMLPARSER_APPERR;

		pNodeAttr->setNamedItem(pNewXDN, &pResultXDN);
		if(pResultXDN == NULL) return XMLPARSER_APPERR;

		CComBSTR bstrValue(strValue);
		pResultXDN->put_text(bstrValue);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("NewAttribute error"));
		return XMLPARSER_SYSTEMERR;
	}

	return XMLPARSER_SUCCESS;
}

// Attribute를 삭제한다.
int CXmlParser::DeleteAttribute(CString strName, long lIndex /*= 0*/)
{
	ClearErrMsg();

	if(!m_pXMLDoc)
	{
		GetErrMessage(0, _T("It is not running CXmlParser Lib"));
		return XMLPARSER_APPERR;
	}
	
	if(!m_pNodeList || lIndex < 0)
	{
		GetErrMessage(0, _T("Attribute is not exist"));
		return XMLPARSER_APPERR;
	}

	try
	{
		IXMLDOMNode * pSelXDN;
		IXMLDOMNamedNodeMap * pNodeAttr;

		m_pNodeList->get_item(lIndex, &pSelXDN);
		if(pSelXDN == NULL) return XMLPARSER_APPERR;

		pSelXDN->get_attributes(&pNodeAttr);
		if(pNodeAttr == NULL) return XMLPARSER_APPERR;

		CComBSTR bstrXPath(strName);
		pNodeAttr->removeNamedItem(bstrXPath, NULL);
	}
	catch(...)
	{
		GetErrMessage(GetLastError(), _T("DeleteAttribute error"));
		return XMLPARSER_SYSTEMERR;
	}

	return XMLPARSER_SUCCESS;
}
