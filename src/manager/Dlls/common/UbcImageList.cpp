#include "StdAfx.h"
#include "resource.h"
#include "UbcImageList.h"

CUbcImageList* CUbcImageList::_instance;
CCriticalSection CUbcImageList::_cs;
CUbcImageList* CUbcImageList::GetInstance()
{
	_cs.Lock();
	if(_instance == NULL)
	{
		_instance = new CUbcImageList();
	}
	_cs.Unlock();

	return _instance;
}

void CUbcImageList::ClearInstance()
{
	_cs.Lock();
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
	_cs.Unlock();
}

CUbcImageList::CUbcImageList(void)
{
	SHFILEINFO sfi;

	HIMAGELIST hImageList = (HIMAGELIST)SHGetFileInfo(_T("C:\\")
													, 0
													, &sfi
													, sizeof(SHFILEINFO)
													, SHGFI_SYSICONINDEX | SHGFI_SMALLICON);
	if(hImageList)
	{
		m_ilContents.Attach(hImageList);
	}
	else
	{
		m_ilContents.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1);	// add an images list with appropriate background (transparent) color
	}

	CBitmap bmpCpontentsState;
	bmpCpontentsState.LoadBitmap(IDB_OVERLAP_CONTENTS_STATE);

	int nOverlayIndex = m_ilContents.GetImageCount();
	m_ilContents.Add(&bmpCpontentsState, RGB(255,255,255));

	m_ilContents.SetOverlayImage(nOverlayIndex  , SERVER_IMG_IDX);
	m_ilContents.SetOverlayImage(nOverlayIndex+1, NIX_IMG_IDX);
}

CUbcImageList::~CUbcImageList(void)
{
	m_ilContents.Detach();
}

int CUbcImageList::GetFileIconIndex(CString strExt)
{
	SHFILEINFO sfi;
	SHGetFileInfo(strExt
				, 0
				, &sfi
				, sizeof(SHFILEINFO)
				,SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_SMALLICON
				);

	return sfi.iIcon;
}

int CUbcImageList::GetFolderIconIndex(CString strPath, BOOL bOpen)
{
	SHFILEINFO sfi;
	SHGetFileInfo(strPath
				, FILE_ATTRIBUTE_DIRECTORY
				, &sfi
				, sizeof(SHFILEINFO)
				, SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_SMALLICON | (bOpen ? SHGFI_OPENICON : 0)
				);

	return sfi.iIcon;
}
