//////////////////////////////////////////////////////////////////////////
//
// 콘텐츠 명과 화일명으로 사용할수 없는 특수문자는 다음과 같다.
// 1) 원래 MS 윈도우즈에서 파일명으로 사용할 수 없는 문자
// <>\/*|":
// 2) 추가적으로 사용할 수 없는 문자
// [],!%`' 
//
// [],<> 는 괄호로 치환하고, 나머지는 모두 언더스코어로 치환한다.
//
//////////////////////////////////////////////////////////////////////////
#pragma once

class CPreventChar
{
private:
	static CPreventChar* _instance;

	CMapStringToString m_mapPreventChar[3];		// eFileName
//	CStringArray m_astrPreventChar;
//	CStringArray m_astrConvertChar;

public:
	CPreventChar(void);
	~CPreventChar(void);

	typedef enum { eFileName, ePackageName, eId } PREVENT_TYPE; 

	static	CPreventChar* GetInstance();

	CString	GetPreventChar(PREVENT_TYPE nType=eFileName);

	bool	IsPreventChar(CString strValue, PREVENT_TYPE nType=eFileName);
	bool	IsPreventChar(TCHAR szValue, PREVENT_TYPE nType=eFileName);

	bool	HavePreventChar(CString strValue, PREVENT_TYPE nType=eFileName);
	CString	ConvertToAvailableChar(CString strValue, PREVENT_TYPE nType=eFileName);

	void	AddPreventChar(CString strPreventChar, CString strConvertChar, PREVENT_TYPE nType=eFileName);
	void	AddPreventChar(TCHAR szPreventChar, TCHAR szConvertChar, PREVENT_TYPE nType=eFileName);
	void	DelPreventChar(CString strPreventChar, PREVENT_TYPE nType=eFileName);
	void	DelPreventChar(TCHAR szPreventChar, PREVENT_TYPE nType=eFileName);
};
