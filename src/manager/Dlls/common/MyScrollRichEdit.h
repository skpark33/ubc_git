#pragma once


// CMyScrollRichEdit

class CMyScrollRichEdit : public CRichEditCtrl

{
	DECLARE_DYNAMIC(CMyScrollRichEdit)

public:
	CMyScrollRichEdit();
	virtual ~CMyScrollRichEdit();
	void	WriteEditHighLightBox(char* Msg, COLORREF TextColor, COLORREF BkColor);
	void 	WriteEditBox(CString* Msg, BOOL bVScr=TRUE);
	void 	WriteEditBox(char* Msg, BOOL bVScr=TRUE);
	void 	SetMaxLine(int MaxLine = 3000, int LineMaxChar = 256);
	void 	Clear();

protected:
	long	m_StartPos, m_EndPos;
	int		m_MaxLine;

	afx_msg LRESULT Message(WPARAM id, LPARAM msg);
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMenuCopy();
};


