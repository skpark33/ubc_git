#pragma once

class CUbcGUID
{
private:
	static CUbcGUID* _instance;

	CMapStringToString	m_mapContentsIdToGUID;
	CString CreateGUID();

public:
	CUbcGUID(void) {};
	~CUbcGUID(void) {};

	static	CUbcGUID* GetInstance();


	CString	ToGUID(CString strId);
	CString	GetGUID(CString strId);

	BOOL	IsGUID(CString strGUID);

	void	Clear();
	CMapStringToString* GetMapTable();
};
