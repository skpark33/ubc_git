#pragma once


// CUTBHeaderCtrl

#define DAY_PIX 24

class CUTBHeaderCtrl : public CHeaderCtrl
{
	DECLARE_DYNAMIC(CUTBHeaderCtrl)

public:
	enum tagHDR_IMAGE_IDX { LC_EMPTY, LC_CHK_UNSEL, LC_CHK_SEL, LC_RADIO_UNSEL, LC_RADIO_SEL, LC_SORTUP, LC_SORTDOWN };

	typedef enum tagHDR_COL_TYPE { COL_NORMAL, COL_PROGRESS, COL_CALENDAR, COL_INDICATOR_CALENDAR } HDR_COL_TYPE;
	typedef	struct tagHDR_COL_INFO
	{
		HDR_COL_TYPE m_nColType   ;
		CTime		 m_tmStartDate;
		CTime	     m_tmEndDate  ;
		CTime        m_tmSelDate  ;

		tagHDR_COL_INFO()
		{
			m_nColType    = COL_NORMAL;

			CTime tmCurrentTime = CTime::GetCurrentTime();
			m_tmStartDate = CTime(tmCurrentTime.GetYear(), tmCurrentTime.GetMonth(), tmCurrentTime.GetDay(), 0, 0, 0);
			m_tmEndDate   = CTime(tmCurrentTime.GetYear(), tmCurrentTime.GetMonth(), tmCurrentTime.GetDay(), 0, 0, 0);
			m_tmSelDate   = CTime(tmCurrentTime.GetYear(), tmCurrentTime.GetMonth(), tmCurrentTime.GetDay(), 0, 0, 0);
		}
	} ST_HDR_COL_INFO, *P_HDR_COL_INFO;

	CUTBHeaderCtrl();
	virtual ~CUTBHeaderCtrl();

	int				GetFixedCol() { return m_nFixedCol; }
	void			SetFixedCol(int nCol) { m_nFixedCol = nCol; }
	int				GetFixedColRight() { return m_nFixedColRight; }

	HDR_COL_TYPE	GetColType(int nCol);
	void			SetColType(int nCol, HDR_COL_TYPE nColType);
	void			SetColType(int nCol, HDR_COL_TYPE nColType, CTime tmStartDate, int nAddMonth);
	void			SetColType(int nCol, HDR_COL_TYPE nColType, CTime tmStartDate, CTime tmEndDate);
	void			SetColType(int nCol, HDR_COL_TYPE nColType, CTime tmStartDate, int nAddMonth, CTime tmSelDate);
	void			SetColType(int nCol, HDR_COL_TYPE nColType, CTime tmStartDate, CTime tmEndDate, CTime tmSelDate);

	CUTBHeaderCtrl::P_HDR_COL_INFO	GetColInfo(int nCol);
protected:
	DECLARE_MESSAGE_MAP()

	CArray<P_HDR_COL_INFO, P_HDR_COL_INFO>	m_HeaderInfoList;

private:
	CTime	 m_tmStartDate;
	CTime	 m_tmEndDate;

	int      m_nFixedCol;
	int      m_nFixedColRight;
	int      m_nFontHeight;

	int      m_iSpacing;
	SIZE     m_sizeImage;
	COLORREF m_cr3DHighLight;
	COLORREF m_cr3DShadow;
	COLORREF m_cr3DFace;
	COLORREF m_cr3DDarkFace;
	COLORREF m_crBtnText;

	void	 DrawCtrl    (CDC* pDC);
	int		 DrawImage   (CDC* pDC, int nCol, CRect rect, LPHDITEM hdi, BOOL bRight);
	int		 DrawSortIcon(CDC* pDC, int nCol, CRect rect, LPHDITEM hdi);
	int		 DrawBitmap  (CDC* pDC, int nCol, CRect rect, LPHDITEM hdi, CBitmap* pBitmap, BITMAP* pBitmapInfo, BOOL bRight);
	int		 DrawText    (CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi);
	int      DrawCalendar(CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi);
	int      DrawIndCalendar(CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi);

	void	 CalcFontHeight();
	void	 CalcFixedColRight();

	int		 GetMonthDays(int nYear, int nMonth);
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual void DrawItem(LPDRAWITEMSTRUCT);
	virtual void DrawItem(CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
};


