#pragma once

#if defined(AFX_TARG_KOR) || defined(_ML_KOR_)
	#define UBCDEFAULT_FONT					_T("����")
	#define UBCSTUDIOEXE_STR				_T("UBCStudioPE_kr.exe")
	#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE_kr.exe")
	#define UBCMANAGEREXE_STR				_T("UBCManager_kr.exe")
#elif defined(AFX_TARG_JPN) || defined(_ML_JAP_)
	#define UBCDEFAULT_FONT					_T("MS �����ë�")
	#define UBCSTUDIOEXE_STR				_T("UBCStudioPE_jp.exe")
	#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE_jp.exe")
	#define UBCMANAGEREXE_STR				_T("UBCManager_jp.exe")
#elif defined(AFX_TARG_ENG) || defined(_ML_ENG_)
	#define UBCDEFAULT_FONT					_T("Arial")
	#define UBCSTUDIOEXE_STR				_T("UBCStudioPE_en.exe")
	#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE_en.exe")
	#define UBCMANAGEREXE_STR				_T("UBCManager_en.exe")
#else
	#define UBCDEFAULT_FONT					_T("System")
	#define UBCSTUDIOEXE_STR				_T("UBCStudioPE.exe")
	#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE.exe")
	#define UBCMANAGEREXE_STR				_T("UBCManager.exe")
#endif

#define UBC_ROOT_PATH			_T("SQISoft")
#define UBC_EXECUTE_PATH		_T("SQISoft\\UTV1.0\\execute\\")
#define UBC_CONFIG_PATH			_T("SQISoft\\UTV1.0\\execute\\config\\")
#define UBC_CONTENTS_PATH		_T("SQISoft\\Contents\\ENC\\")
#define UBC_CONTENTS_ROOT		_T("SQISoft\\Contents\\")

#define LOGIN_DAT				_T("UBCLogin.dat")
#define ORBCONN_INI				_T("UBCConnect.ini")
#define UBCVARS_INI				_T("UBCVariables.ini")
#define UBCURL_INI				_T("UBCURL.ini")
#define UBCCODE_INI				_T("UBCCode.ini")

#define _UBCSTUDIO_EE_ENV_INI	_T("UBCStudioEE.ini")
#define _UBCSTUDIO_PE_ENV_INI	_T("UBCStudioPE.ini")

#ifdef _UBCMANAGER_
	#define ENVIROMENT_INI		_T("UBCManager.ini")
#elif _UBCSTUDIO_EE_
	#define ENVIROMENT_INI		_UBCSTUDIO_EE_ENV_INI
#elif _UBCSTUDIO_PE_
	#define ENVIROMENT_INI		_UBCSTUDIO_PE_ENV_INI
#endif

#define STR_DEF_TIME			_T("%Y/%m/%d %H:%M:%S")
#define STR_DEF_EMPTY_TIME		_T("1970/01/01 09:00:00")

#define EH_SEPARATOR			_T("^")

#define IsSame(x,y)				(!_tcscmp(x,y))

enum ECopyData	{ eCDSetPackage, eCDNewPackage };
enum EDisSide	{ eDispA, eDispB };
enum ESrcApp	{ eSAUnknwon, eSAStudio, eSAManager };
enum EScheRev	{ eSRCancel, eSRNormal, eSRSaveAs };
enum EServerERROR { eNORMAL, eServerConnectionOverload, };