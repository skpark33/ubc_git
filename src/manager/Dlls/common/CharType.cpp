//////////////////////////////////////////////////////////////////////////
// 
// 영문 반자
// A1A1h ~ A2E5h: 기호와 약자 몇가지 
// A3A1h ~ A3FEh: 전각 문자 
// A4A1h ~ A4FEh: 한글자모 
// A5A1h ~ A5AAh: 소문자 로마숫자 (i~x) 
// A5B0h ~ A5B9h: 대문자 로마숫자 (I ~ X) 
// A5C1h ~ A5D8h: 대문자 그리스 문자 
// A5E1h ~ A5F8h: 소문자 그리스 문자 
// A6A1h ~ A6E4h: 라인 문자 
// A7A1h ~ A7EFh: 단위 
// A8A1h ~ A8B0h: 대문자 발음기호 
// A8B1h ~ A8BEh: 원문자 한글 자음 (ㄱ~ㅎ) 
// A8BFh ~ A8CCh: 원문자 가나다 (가~하) 
// A8CDh ~ A8E6h: 원문자 영문 소문자 (a~z) 
// A8E7h ~ A8F5h: 원문자 숫자 (1~15) 
// A8B1h ~ A8BEh: 분수 (1/2, 1/3, 2/3, 1/4, 3/4, 1/8, 3/8, 5/8, 7/8)
// A9A1h ~ A9B0h: 소문자 발음기호 
// A9B1h ~ A9BEh: 괄호문자 한글 자음 (ㄱ~ㅎ) 
// A9BFh ~ A9CCh: 괄호문자 가나다 (가~하) 
// A9CDh ~ A9E6h: 괄호문자 영문 소문자 (a~z) 
// A9E7h ~ A9F5h: 괄호문자 숫자 (1~15) 
// A9B1h ~ A9BEh: 위, 아래첨자 (1, 2, 3, 4, n, 1, 2, 3, 4) 
// ABA1h ~ ABF3h: 가타카나 
// ABA1h ~ ABF6h: 히라가나 
// ACA1h ~ ACC1h: 키릴 문자 대문자 
// ACD1h ~ ACF1h: 키릴 문자 소문자 
// B0A1h ~ C8FEh: 완성형 한글 영역
// CAA1h ~ FDFEh: 완성형 한자 영역
// 
//////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "common/CharType.h"

CCharType* CCharType::_instance;
CCharType* CCharType::GetInstance()
{
	if(_instance == NULL) _instance = new CCharType();
	return _instance;
}

CCharType::CHAR_TYPE CCharType::GetCharType(LPCTSTR szChar)
{
	const char* buffer = szChar;
	int len = strlen(buffer);
	int x = 0;

	if(len <= 0) return CT_UNKNOWN;
	if(len == 1)
	{
		// 영어
		if( (((unsigned char)buffer[x] >= 0x41) && ((unsigned char)buffer[x] <= 0x5A)) ||
			(((unsigned char)buffer[x] >= 0x61) && ((unsigned char)buffer[x] <= 0x7A)) )
		{
			return CT_ASCII_ENG;
		}
		// 숫자
		else if ( ( buffer[x]>= '0' ) && ( buffer[x] <= '9' ) )
		{
			return CT_ASCII_NUM;
		}
		// 특수문자
		else
		{
			return CT_ASCII_SYM;
		}
	}
	else
	{
		// 한글
		if( ((unsigned char)buffer[x]   >= 0xB0) &&
			((unsigned char)buffer[x]   <= 0xC8) &&
			((unsigned char)buffer[x+1] >= 0xA1) &&
			((unsigned char)buffer[x+1] <= 0xFE) )
		{
			return CT_HANGUL;
		}
		// 한글 자모
		else if( ((unsigned char)buffer[x]   == 0xA4) &&
				 ((unsigned char)buffer[x+1] >= 0xA1) &&
				 ((unsigned char)buffer[x+1] <= 0xFE) )
		{
			return CT_HAN_JAMO;
		}
		// 한자
		else if( ((unsigned char)buffer[x]   >= 0xCA) &&
				 ((unsigned char)buffer[x]   <= 0xFD) &&
				 ((unsigned char)buffer[x+1] >= 0xA1) &&
				 ((unsigned char)buffer[x+1] <= 0xFE) )
		{
			return CT_HANJA;
		}
		// 가타카나
		else if( ((unsigned char)buffer[x]   == 0xAB) &&
				 ((unsigned char)buffer[x+1] >= 0xA1) &&
				 ((unsigned char)buffer[x+1] <= 0xF3) )
		{
			return CT_GATAKANA;
		}
		// 히라가나
		else if( ((unsigned char)buffer[x]   == 0xAB) &&
				 ((unsigned char)buffer[x+1] >= 0xA1) &&
				 ((unsigned char)buffer[x+1] <= 0xF6) )
		{
			return CT_HIRAKANA;
		}
		// 키릴 문자 대문자 
		else if( ((unsigned char)buffer[x]   == 0xAC) &&
				 ((unsigned char)buffer[x+1] >= 0xA1) &&
				 ((unsigned char)buffer[x+1] <= 0xC1) )
		{
			return CT_KIRILL_CAP;
		}
		// 키릴 문자 소문자
		else if( ((unsigned char)buffer[x]   == 0xAC) &&
				 ((unsigned char)buffer[x+1] >= 0xD1) &&
				 ((unsigned char)buffer[x+1] <= 0xFE) )
		{
			return CT_KIRILL_SAMALL;
		}
		// 영문
		else if( (((unsigned char)buffer[x] >= 0x41) && ((unsigned char)buffer[x] <= 0x5A)) ||
				 (((unsigned char)buffer[x] >= 0x61) && ((unsigned char)buffer[x] <= 0x7A)) )
		{
			return CT_ASCII_ENG;
		}
		// 숫자
		else if ( ( buffer[x]>= '0' ) && ( buffer[x] <= '9' ) )
		{
			return CT_ASCII_NUM;
		}
		else if( ((unsigned char)buffer[x]>= 0xA1) && ((unsigned char)buffer[x+1] >= 0xA1) )
		{
			return CT_FULL_WIDTH;
		}
		else
		{
			return CT_ASCII_SYM;
		}
	}

	return CT_UNKNOWN;
}

bool CCharType::IsIncludeType(LPCTSTR szString, CHAR_TYPE in_type)
{
	int len = strlen(szString);

	for(int x = 0; x < len; x++)
	{
		CHAR_TYPE out_type = GetCharType(szString+x);
		if(in_type == out_type) return true;

		if( out_type != CT_UNKNOWN   &&
			out_type != CT_ASCII_ENG &&
			out_type != CT_ASCII_NUM &&
			out_type != CT_ASCII_SYM )
		{
			x++;
		}
	}

	return false;
}
