#pragma once
//#include "resource.h"

class CUTBListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CUTBListCtrl)
public:
	enum { 
		LC_EMPTY, LC_EMPTY2, 
		LC_ASC, LC_DES, 
		LC_RDOU, LC_RDOS,
		LC_CHKU, LC_CHKS,
	};
public:
	CUTBListCtrl();
	virtual ~CUTBListCtrl();
	virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);

	void InitHeader(int nBitmapID);
	void SetSortHeader(const int iColumn);
	void SetSortHeader(const int iColumn, bool bAsc);
	int  GetCurSortCol();
	bool IsAscend();
	void SetSortCol(const int iColumn, bool bAsc=true);
	int  Compare(LPARAM lParam1, LPARAM lParam2);
	BOOL SortList(const int iColumn, PFNLVCOMPARE pfnCompare, DWORD_PTR dwData);
	BOOL SortList(const int iColumn, bool bAsc, PFNLVCOMPARE pfnCompare, DWORD_PTR dwData);

	void SetCheckHdrState(bool bCheck);
	bool GetCheckHdrState();

	int  GetColPos(LPCTSTR);
	CString GetColText(int);
	BOOL SetText(int, LPCTSTR, LPCTSTR);
	CString GetText(int, LPCTSTR);

	void SaveColumnState(CString strListName, CString strIniPath);
	void LoadColumnState(CString strListName, CString strIniPath, int nSortCol=-1);
protected:
	bool m_bAscend;
	int  m_nCurSort;
	CHeaderCtrl m_ctrHeader;
	CImageList	m_ilHeader;

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnLvnEndScroll(NMHDR *pNMHDR, LRESULT *pResult);
};


