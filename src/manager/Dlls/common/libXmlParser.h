// libXmlParser.h: interface for the CXmlParser
//
//////////////////////////////////////////////////////////////////////

#pragma once

// Err MACRO
#define XMLPARSER_SUCCESS          0
#define XMLPARSER_SYSTEMERR		-100
#define XMLPARSER_APPERR		-200

#include "msxml2.h"
#pragma comment(lib, "msxml2.lib")
#include <atlbase.h>

class CXmlParser
{
public:
	CXmlParser();
	virtual ~CXmlParser();

	int Load(CString strXmlFile);
	int LoadXML(CString strXmlString);

	int Save(CString strXmlFile = _T(""));

	CString GetXPath();
	int SetXPath(CString strXPath);

	CString GetXML(CString strXPath = _T(""));
	CString GetText(CString strXPath = _T(""));
	CString GetData(long lIndex = 0);

	CString GetElement(CString strName, long lIndex = 0);
	int SetElement(CString strName, CString strValue, long lIndex = 0);
	int NewElement(CString strName, long lIndex = 0, CString strValue = _T(""));
	int DeleteElement(CString strName, long lIndex = 0);

	CString GetAttribute(CString strName, long lIndex = 0);
	int SetAttribute(CString strName, CString strValue, long lIndex = 0);
	int NewAttribute(CString strName, long lIndex = 0, CString strValue = _T(""));
	int DeleteAttribute(CString strName, long lIndex = 0);

	LONG GetSelCount();

	int	GetErrCode();
	CString GetLastErrorMsg();

protected:
	IXMLDOMDocument * m_pXMLDoc;
	IXMLDOMNodeList * m_pNodeList;

	CString m_strXPath;
	CString m_strXmlFile;

	CString m_strErrMsg;
	int		m_nErrCode;
	void	GetErrMessage(int nErrCode, CString strDefaultMsg = _T(""));

	void	ClearErrMsg();
};
