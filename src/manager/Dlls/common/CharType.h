//////////////////////////////////////////////////////////////////////////
//
// 콘텐츠 명과 화일명으로 사용할수 없는 특수문자는 다음과 같다.
// 1) 원래 MS 윈도우즈에서 파일명으로 사용할 수 없는 문자
// <>\/*|":
// 2) 추가적으로 사용할 수 없는 문자
// [],!%`' 
//
// [],<> 는 괄호로 치환하고, 나머지는 모두 언더스코어로 치환한다.
//
//////////////////////////////////////////////////////////////////////////
#pragma once

class CCharType
{
private:
	static CCharType* _instance;

public:
	enum CHAR_TYPE { CT_UNKNOWN		// 알수없음
		,  CT_ASCII_ENG		// 아스키 영문
		,  CT_ASCII_NUM		// 아스키 숫자
		,  CT_ASCII_SYM		// 아스키 기호
//		,  CT_SYMBOLS		// A1A1h ~ A2E5h: 기호와 약자 몇가지 
		,  CT_FULL_WIDTH	// A3A1h ~ A3FEh: 전각 문자 
		,  CT_HAN_JAMO		// A4A1h ~ A4FEh: 한글자모 
//		,  CT_ROMA_SMALL	// A5A1h ~ A5AAh: 소문자 로마숫자 (i~x) 
//		,  CT_ROAM_CAP		// A5B0h ~ A5B9h: 대문자 로마숫자 (I ~ X) 
//		,  CT_GREEC_SMALL	// A5C1h ~ A5D8h: 대문자 그리스 문자 
//		,  CT_GREEC_CAP		// A5E1h ~ A5F8h: 소문자 그리스 문자 
//		,  CT_LINE			// A6A1h ~ A6E4h: 라인 문자 
//		,  CT_UNIT			// A7A1h ~ A7EFh: 단위 
//		,  CT_PRONUN_SYM	// A8A1h ~ A8B0h: 대문자 발음기호 
//		,  CT_CIRCLE_HAN1	// A8B1h ~ A8BEh: 원문자 한글 자음 (ㄱ~ㅎ) 
//		,  CT_CIRCLE_HAN2	// A8BFh ~ A8CCh: 원문자 가나다 (가~하) 
//		,  CT_CLRCLE_SMALL	// A8CDh ~ A8E6h: 원문자 영문 소문자 (a~z) 
//		,  CT_CIRCLE_NUM	// A8E7h ~ A8F5h: 원문자 숫자 (1~15) 
//		,  CT_FRACTION		// A8B1h ~ A8BEh: 분수 (1/2, 1/3, 2/3, 1/4, 3/4, 1/8, 3/8, 5/8, 7/8)
//		,  CT_PRONUN_SMALL	// A9A1h ~ A9B0h: 소문자 발음기호 
//		,  CT_BRACKET_HAN1	// A9B1h ~ A9BEh: 괄호문자 한글 자음 (ㄱ~ㅎ) 
//		,  CT_BRACKET_HAN2	// A9BFh ~ A9CCh: 괄호문자 가나다 (가~하) 
//		,  CT_BRACKET_SMALL	// A9CDh ~ A9E6h: 괄호문자 영문 소문자 (a~z) 
//		,  CT_BRACKET_NUM	// A9E7h ~ A9F5h: 괄호문자 숫자 (1~15) 
//		,  CT_SUBSCRIPT		// A9B1h ~ A9BEh: 위, 아래첨자 (1, 2, 3, 4, n, 1, 2, 3, 4) 
		,  CT_GATAKANA		// ABA1h ~ ABF3h: 가타카나 
		,  CT_HIRAKANA		// ABA1h ~ ABF6h: 히라가나 
		,  CT_KIRILL_CAP	// ACA1h ~ ACC1h: 키릴 문자 대문자 
		,  CT_KIRILL_SAMALL	// ACD1h ~ ACF1h: 키릴 문자 소문자 
		,  CT_HANGUL		// B0A1h ~ C8FEh: 완성형 한글 영역
		,  CT_HANJA			// CAA1h ~ FDFEh: 완성형 한자 영역
	};

public:
	CCharType(void) {};
	~CCharType(void) {};

	static	CCharType* GetInstance();

	CHAR_TYPE GetCharType(LPCTSTR);
	bool	  IsIncludeType(LPCTSTR, CHAR_TYPE);
};
