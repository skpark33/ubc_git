#pragma once


// CUBCTabCtrl

class CUBCTabCtrl : public CTabCtrl
{
	DECLARE_DYNAMIC(CUBCTabCtrl)

public:
	CUBCTabCtrl();
	virtual ~CUBCTabCtrl();

	void	InitImageList(UINT nBitmapID);
	void	InitImageListHighColor(UINT nBitmapID, COLORREF crMask);

	LPARAM	GetItemParam(int nIndex);
	void	SetItemParam(int nIndex, LPARAM lParam);

protected:
	CImageList		m_ilTabs;

	DECLARE_MESSAGE_MAP()
};


