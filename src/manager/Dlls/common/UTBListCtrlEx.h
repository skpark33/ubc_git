#pragma once
//#include "resource.h"
#include "common/UTBHeaderCtrl.h"

class CUTBListCtrlEx : public CListCtrl
{
	DECLARE_DYNAMIC(CUTBListCtrlEx)
public:
	enum tagLIST_MODE { LS_NONE, LS_CHECKBOX, LS_RADIOBOX };

public:
	CUTBListCtrlEx();
	virtual ~CUTBListCtrlEx();
	virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);

	void	InitHeader(int nBitmapID, UINT dwStyle);
	bool	GetSortEnable() { return m_bSortEnable; }
	void	SetSortEnable(bool bEnable) { m_bSortEnable = bEnable; }
	void	SetSortHeader(const int iColumn);
	void	SetSortHeader(const int iColumn, bool bAsc);
	int		GetCurSortCol() { return m_nCurSort; };
	bool	IsAscend() { return m_bAscend; };
	void	SetSortCol(const int iColumn, bool bAsc=true);
	BOOL	SortList(const int iColumn, PFNLVCOMPARE pfnCompare, DWORD_PTR dwData);
	BOOL	SortList(const int iColumn, bool bAsc, PFNLVCOMPARE pfnCompare, DWORD_PTR dwData);

	int		Compare(LPARAM lParam1, LPARAM lParam2);
	static int CALLBACK Compare(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

	void	SetCheckHdrState(bool bCheck);
	bool	GetCheckHdrState();

	int		GetColPos(LPCTSTR);
	CString GetColText(int);
	BOOL	SetText(int, LPCTSTR, LPCTSTR);
	CString GetText(int, LPCTSTR);

	int		GetRadioSelectedIndex();

	void	SaveColumnState(CString strListName, CString strIniPath);
	void	LoadColumnState(CString strListName, CString strIniPath, int nSortCol=-1);

	void	SetProgressBarCol(int nColumn);
	void	SetProgressBarCol(int nColumn, COLORREF clrBarFore, COLORREF clrBarBack, COLORREF clrTextFore);
	BOOL	SetProgressBarText(int nRow, int nCol, float fProgress, int nAlign, LPCTSTR szText, BOOL bGradientFill, COLORREF clrBarFore, COLORREF clrBarBack, COLORREF clrTextFore);
	void	GetProgressBarText(int nRow, int nCol, float& fProgress, int& nAlign, CString& strText, BOOL& bGradientFill, COLORREF& clrBarFore, COLORREF& clrBarBack, COLORREF& clrTextFore);

	void	SetBackColor(COLORREF clrColor) { m_clrBkgnd = clrColor; }
	void	SetTextColor(COLORREF clrColor) { m_clrText = clrColor; }
	void	SetSelBackColor(COLORREF clrColor) { m_clrHBkgnd = clrColor; }
	void	SetSelTextColor(COLORREF clrColor) { m_clrHText = clrColor; }

	COLORREF GetBackColor() { return m_clrBkgnd; }
	COLORREF GetTextColor() { return m_clrText ; }
	COLORREF GetSelBackColor() { return m_clrHBkgnd; }
	COLORREF GetSelTextColor() { return m_clrHText ; }

	void	SetRowColor(int nItem, COLORREF clrBack, COLORREF clrText);
	BOOL	GetRowColor(int nItem, COLORREF& clrBack, COLORREF& clrText);

	// Sets the data (lParam) associated with a particular item.
	virtual BOOL SetItemData(int nItem, DWORD_PTR dwData);

	// Retrieves the data (lParam) associated with a particular item.
	virtual DWORD_PTR GetItemData(int nItem);

	// 스크롤바 삭제
	void			HideScrollBars(int nScrollBar=SB_BOTH);

	// 특정컬럼 이하 스크롤 과府
	int				GetFixedCol() { return m_ctrHeader.GetFixedCol(); }
	void			SetFixedCol(int nCol) { m_ctrHeader.SetFixedCol(nCol); }

	CUTBHeaderCtrl::HDR_COL_TYPE GetColType(int nCol) { return m_ctrHeader.GetColType(nCol); };
	void			SetColType(int nCol, CUTBHeaderCtrl::HDR_COL_TYPE nColType)                                     { m_ctrHeader.SetColType(nCol, nColType)                        ; }
	void			SetColType(int nCol, CUTBHeaderCtrl::HDR_COL_TYPE nColType, CTime tmStartDate, int nAddMonth)   { m_ctrHeader.SetColType(nCol, nColType, tmStartDate, nAddMonth); }
	void			SetColType(int nCol, CUTBHeaderCtrl::HDR_COL_TYPE nColType, CTime tmStartDate, CTime tmEndDate) { m_ctrHeader.SetColType(nCol, nColType, tmStartDate, tmEndDate); }
	void			SetCalendarColor(COLORREF clrColor) { m_clrCalendar = clrColor; }

	CUTBHeaderCtrl::P_HDR_COL_INFO GetColInfo(int nCol) { return m_ctrHeader.GetColInfo(nCol); }
	// 그리탛E라인 설정
	bool			GetDrawGridLine() { return m_bDrawGridLine; }
	void			SetDrawGridLine(bool bDrawGridLine) { m_bDrawGridLine = bDrawGridLine; }

	BOOL			GetHdrHitInfo(CPoint point, CUTBHeaderCtrl::ST_HDR_COL_INFO& stHdrColInfo);
protected:
	// 아이템에 특별한짓을 하컖E싶을 경퓖E여기에...
	typedef struct tagITEM_DATA
	{
		COLORREF clrBack;
		COLORREF clrText;
		DWORD_PTR dwData;	// SetItemData or GetItemData 퓖E

		tagITEM_DATA()
		{
			clrBack = ::GetSysColor(COLOR_WINDOW);
			clrText = ::GetSysColor(COLOR_WINDOWTEXT);
			dwData = NULL;
		}
	} ST_ITEM_DATA, *P_ITEM_DATA;

	CMapPtrToPtr m_mapItemData;
	void		DeleteAllItemData();

	UINT        m_dwMode;
	bool		m_bSortEnable;
	bool		m_bAscend;
	int			m_nCurSort;
	int			m_nHideScrollBar;
	bool		m_bDrawGridLine;

	CUTBHeaderCtrl m_ctrHeader;
	CImageList	m_ilHeader;

	// tooltip
	void CellHitTest(const CPoint& pt, int& nRow, int& nCol) const;
	bool ShowToolTip(const CPoint& pt) const;
	CString GetToolTipText(int nRow, int nCol);

	void DrawCtrl       (CDC* pDC, int nItem);
	void DrawCheckBox	(CDC* pDC, int nItem, int nColumn);
	void DrawRadioBox	(CDC* pDC, int nItem, int nColumn);
	void DrawImage		(CDC* pDC, int nItem, int nColumn);
	void DrawText		(CDC* pDC, int nItem, int nColumn, bool bHighlight = false);
	void DrawProgressBar(CDC* pDC, int nItem, int nColumn);
	void DrawCalendarBar(CDC* pDC, int nItem, int nColumn, bool bHighlight = false, bool bShowIndicator = false);

	BOOL GetSubItemRect(int iItem, int iSubItem, int nArea, CRect& ref);

	// 변펯E선푳E
	////////////////////////
	COLORREF    m_clrText;
	COLORREF    m_clrHText;
	COLORREF    m_clrBkgnd;
	COLORREF    m_clrHBkgnd;

	COLORREF    m_clrRowBkgnd;
	COLORREF    m_clrRowText;

	COLORREF	m_clrBarFore;
	COLORREF	m_clrBarBack;
	COLORREF	m_clrTextFore;

	COLORREF	m_clrCalendar;

typedef class CDatetimeInfo
{
public:
	CArray<CTime> arrDateStart;
	CArray<CTime> arrDateEnd  ;
	CArray<int>   arrWeekInfo ;
	CArray<CTime> arrExceptDay;

	CDatetimeInfo() {};
	CDatetimeInfo(CString strDateInfo)
	{
		int nPipePos=0;
		CString strPipeData;
		while( !(strPipeData = strDateInfo.Tokenize(_T("|"), nPipePos)).IsEmpty() )
		{
			int nStartPos       = strPipeData.Find(_T("="));
			CString strToken    = strPipeData.Left(nStartPos);
			CString strDataList = strPipeData.Mid(nStartPos+1);

			if( strToken.Find(_T("DateList")) == 0)
			{
				int nLoopPos=0;
				CString strData;
				while( !(strData = strDataList.Tokenize(_T(","), nLoopPos)).IsEmpty() )
				{
					int pos = strData.Find(_T("~"));
					if(pos < 0) continue;

					CString strStartDate = strData.Left(pos);
					CString strEndDate   = strData.Mid(pos+1);

					if(strStartDate > strEndDate) continue;

					arrDateStart.Add(CUTBListCtrlEx::ToDateTime(strStartDate));
					arrDateEnd  .Add(CUTBListCtrlEx::ToDateTime(strEndDate  ));
				}
			}
			else if( strToken.Find(_T("WeekInfo")) == 0)
			{
				int nLoopPos=0;
				CString strData;
				while( !(strData = strDataList.Tokenize(_T(","), nLoopPos)).IsEmpty() )
				{
					arrWeekInfo.Add(_ttoi(strData));
				}
			}
			else if( strToken.Find(_T("ExceptDay")) == 0)
			{
				int nLoopPos=0;
				CString strData;
				while( !(strData = strDataList.Tokenize(_T(","), nLoopPos)).IsEmpty() )
				{
					strData.Replace(_T("."), _T("/"));
					CString strNormalizeDate;
					strNormalizeDate.Format(_T("%d/%s 00:00:00")
										  , CTime::GetCurrentTime().GetYear()
										  , strData
										  );
					arrExceptDay.Add(CUTBListCtrlEx::ToDateTime(strNormalizeDate));
				}
			}
		}
	}

	CString ToString()
	{
		CString strDateList;
		for(int i=0; i<arrDateStart.GetCount() ;i++)
		{
			strDateList += arrDateStart[i].Format(_T("%Y/%m/%d %H:%M:%S"))
						+ _T("~")
						+ arrDateEnd[i].Format(_T(""))
						+ _T(",") ;
		}
		
		CString strWeekInfo;
		for(int i=0; i<arrWeekInfo.GetCount() ;i++)
		{
			CString strTmp;
			strTmp.Format(_T("%d"), arrWeekInfo[i]);
			strWeekInfo += (strWeekInfo.IsEmpty() ? _T(""):_T(","))
						+ strTmp;
		}
		
		CString strExceptDay;
		for(int i=0; i<arrExceptDay.GetCount() ;i++)
		{
			strExceptDay += (strExceptDay.IsEmpty() ? _T(""):_T(","))
						 + arrExceptDay[i].Format(_T("%m.%d"));
		}

		CString strResult;
		strResult.Format(_T("DateList=%s|WeekInfo=%s|ExceptDay=%s|")
						, strDateList
						, strWeekInfo
						, strExceptDay
						);
		return strResult;
	}
} DATETIME_INFO, *P_DATETIME_INFO;

	static CTime ToDateTime(CString strValue);

protected:

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg BOOL OnLvnEndScroll(NMHDR *pNMHDR, LRESULT *pResult);
	virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg LRESULT OnSetFont(WPARAM wParam, LPARAM);
	afx_msg BOOL OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual void PreSubclassWindow();
public:
	afx_msg BOOL OnLvnGetdispinfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnHdnBegintrack(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnToolNeedText(UINT id, NMHDR* pNMHDR, LRESULT* pResult); // tooltip
	virtual INT_PTR OnToolHitTest(CPoint point, TOOLINFO * pTI) const; // tooltip

protected:
	CMapStringToPtr	m_mapColToPos;

protected:
	CFont	m_fontText;
public:
	void	SetTextFont(LPTSTR lpszFontName, int nFontSize); // ex) "Batang", 9
};
