#pragma once

#define SERVER_IMG_IDX	1
#define NIX_IMG_IDX		2

class CUbcImageList
{
private:
	static CUbcImageList* _instance;
	static CCriticalSection	_cs;

	CImageList		m_ilContents;

public:
	CUbcImageList(void);
	~CUbcImageList(void);

	static CUbcImageList*	GetInstance();
	static void				ClearInstance();

	CImageList*				GetImageList() { return &m_ilContents; }

	int						GetFileIconIndex(CString strExt);
	int						GetFolderIconIndex(CString strPath, BOOL bOpen);
};
