#pragma once

#include <afxmt.h>

#ifdef _UBCMANAGER_
	#include "UBCCopCommon\CopModule.h"
#elif defined (_UBCSTUDIO_EE_)
	#include "CopModule.h"
#elif defined (_UBCSTUDIO_PE_)
	#include "Enviroment.h"
#elif defined (_UBCCOPCOMMON_)
	#include "UBCCopCommon\CopModule.h"
#endif

#include "common/UTBListCtrlEx.h"

class CUbcCode
{
private:
	static CUbcCode* _instance;
	static CCriticalSection	_cs;

	CodeItemList    m_listCode;
	CMapStringToPtr m_mapCode;

	CString			m_strDrive;
	CString			m_strPath ;
	CString			m_strIniPath;

	CString			m_strCustomer;

public:
	CUbcCode(LPCTSTR szCustomer=NULL);
	~CUbcCode(void);

	static CUbcCode* GetInstance(LPCTSTR szCustomer=NULL);
	static void		 ClearInstance();

	CString   GetCodeName(CString strCategoryName, int nEnumNumber);
	SCodeItem GetCodeInfo(CString strCategoryName, int nEnumNumber);
	int		  GetCategoryInfo(CString strCategoryName, CodeItemList& list);

	void      FillListCtrl(CUTBListCtrlEx& lcList, CString strCategoryName, BOOL bHasEmpty=FALSE, CString strHasEmptyName=_T(""));
	int       FindListCtrl(CUTBListCtrlEx& lcList, int nEnumNumber);

	void      FillComboBoxCtrl(CComboBox& cbCombo, CString strCategoryName, BOOL bHasEmpty=FALSE, CString strHasEmptyName=_T(""));
	int       FindComboBoxCtrl(CComboBox& cbCombo, int nEnumNumber);
};
