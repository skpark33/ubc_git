// Control\EditEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"

#include "EditEx.h"

// CEditEx

IMPLEMENT_DYNAMIC(CEditEx, CEdit)
CEditEx::CEditEx()
{
}

CEditEx::~CEditEx()
{
}


BEGIN_MESSAGE_MAP(CEditEx, CEdit)
END_MESSAGE_MAP()



// CEditEx 메시지 처리기입니다.

int CEditEx::GetValueInt()
{
	CEdit::GetWindowText(m_str);

	m_str.Replace(_T(","), _T(""));

	return _ttoi(m_str);
}

LPCTSTR CEditEx::GetValueIntMoneyTypeString()
{
	CEdit::GetWindowText(m_str);

	return ToMoneyTypeString(m_str);
}

LPCTSTR CEditEx::GetValueString()
{
	CEdit::GetWindowText(m_str);

	return m_str;
}

void CEditEx::SetValue(int value)
{
	SetWindowText(ToString(value));
}

void CEditEx::SetValue(LPCTSTR value)
{
	SetWindowText(value);
}

void CEditEx::SetValueToMoneyType(int value)
{
	SetWindowText(ToMoneyTypeString(value));
}

CString CEditEx::ToMoneyTypeString(LPCTSTR lpszStr)
{
	CString str = lpszStr;

	int nLength = str.Find(_T("."));
	if(nLength<0)
        nLength = str.GetLength();

	while( nLength > 3 )
	{
		nLength = nLength - 3;
		str.Insert( nLength, ',' );
	}

	return str;
}

CString CEditEx::ToMoneyTypeString(int value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%d"), value);
	return ToMoneyTypeString(buf);
}

CString CEditEx::ToMoneyTypeString(ULONG ulvalue)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%ld"), ulvalue);
	return ToMoneyTypeString(buf);
}

CString CEditEx::ToMoneyTypeString(ULONGLONG ulvalue)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%I64d"), ulvalue);
	return ToMoneyTypeString(buf);
}
