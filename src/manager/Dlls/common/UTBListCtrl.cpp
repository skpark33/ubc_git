// UTBListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "UTBListCtrl.h"

// CUTBListCtrl

static CString ToString(int nValue)
{
	CString strValue;
	strValue.Format(_T("%d"), nValue);
	return strValue;
}

IMPLEMENT_DYNAMIC(CUTBListCtrl, CListCtrl)

CUTBListCtrl::CUTBListCtrl()
{
	m_nCurSort = -1;
	m_bAscend = true;
}

CUTBListCtrl::~CUTBListCtrl()
{
}

BOOL CUTBListCtrl::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
	// TODO: Add your specialized code here and/or call the base class

	return CListCtrl::Create(dwStyle, rect, pParentWnd, nID);
}

BEGIN_MESSAGE_MAP(CUTBListCtrl, CListCtrl)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_NOTIFY_REFLECT(LVN_ENDSCROLL, &CUTBListCtrl::OnLvnEndScroll)
END_MESSAGE_MAP()

// CUTBListCtrl message handlers
int CUTBListCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	return 0;
}

void CUTBListCtrl::OnDestroy()
{
	CListCtrl::OnDestroy();

	// TODO: Add your message handler code here
}

void CUTBListCtrl::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CListCtrl::OnClose();
}

void CUTBListCtrl::InitHeader(int nBitmapID)
{
	VERIFY( m_ctrHeader.SubclassWindow( GetHeaderCtrl()->GetSafeHwnd() ) );

	CBitmap bmp;
	bmp.LoadBitmap(nBitmapID);
	m_ilHeader.Create(16, 16, ILC_COLORDDB | ILC_MASK, 0, 1);
	m_ilHeader.Add(&bmp, RGB(255, 255, 255));
	m_ctrHeader.SetImageList(&m_ilHeader);

	HDITEM hdItem;
	ZeroMemory(&hdItem, sizeof(HDITEM));

	hdItem.mask = HDI_IMAGE|HDI_FORMAT;
	VERIFY( m_ctrHeader.GetItem(0, &hdItem) );

	hdItem.iImage = 0;
	hdItem.fmt |= HDF_IMAGE;

	VERIFY( m_ctrHeader.SetItem(0, &hdItem) );

	// 리스트에 체크박스가 있는 경우 헤더에 체크여부를 판단하는 기능 추가
	if(GetExtendedStyle() & LVS_EX_CHECKBOXES)
	{
		HDITEM itemCheck;
		itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
		m_ctrHeader.GetItem(0, &itemCheck);

		itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
		itemCheck.iImage = (itemCheck.iImage == LC_CHKU ? LC_CHKS : LC_CHKU);
		itemCheck.fmt = HDF_IMAGE | HDF_LEFT;
		m_ctrHeader.SetItem(0, &itemCheck);

		m_ctrHeader.SetBitmapMargin(2);
	}
}

int CUTBListCtrl::GetCurSortCol()
{
	return m_nCurSort;
}

bool CUTBListCtrl::IsAscend()
{
	return m_bAscend;
}

void CUTBListCtrl::SetSortCol(const int iColumn, bool bAsc)
{
	m_nCurSort = iColumn;
	m_bAscend = bAsc;
}

int CUTBListCtrl::Compare(LPARAM lParam1, LPARAM lParam2)
{
	LVFINDINFO lvFind;
	lvFind.flags = LVFI_PARAM;

	lvFind.lParam = lParam1;
	int nIndex1 = FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = FindItem(&lvFind);

	int nCol = GetCurSortCol();

	CString strItem1 = GetItemText(nIndex1, nCol);
	CString strItem2 = GetItemText(nIndex2, nCol);

	LVCOLUMN lvcol;
	ZeroMemory(&lvcol, sizeof(LVCOLUMN));
	lvcol.mask=LVCF_FMT;

	GetColumn(nCol, &lvcol);
	if(lvcol.fmt & LVCFMT_RIGHT)
	{
		if(strItem1.GetLength() > strItem2.GetLength())
		{
			CString strPad(_T(' '), strItem1.GetLength() - strItem2.GetLength());
			strItem2 = strPad + strItem2;
		}
		else if(strItem1.GetLength() < strItem2.GetLength())
		{
			CString strPad(_T(' '), strItem2.GetLength() - strItem1.GetLength());
			strItem1 = strPad + strItem1;
		}
	}

	strItem1.MakeLower();
	strItem2.MakeLower();

	if(IsAscend()) return strItem1.Compare(strItem2);
	else           return strItem2.Compare(strItem1);
}

BOOL CUTBListCtrl::SortList(const int iColumn, PFNLVCOMPARE pfnCompare, DWORD_PTR dwData)
{
	SetSortHeader(iColumn);
	return SortItems(pfnCompare, dwData);
}

BOOL CUTBListCtrl::SortList(const int iColumn, bool bAsc, PFNLVCOMPARE pfnCompare, DWORD_PTR dwData)
{
	SetSortHeader(iColumn, bAsc);
	return SortItems(pfnCompare, dwData);
}

void CUTBListCtrl::SetSortHeader(const int iColumn)
{
	if(m_nCurSort >= 0)
	{
		if(m_nCurSort != iColumn)
		{
			SetSortHeader(iColumn, true);
		}
		else
		{
			SetSortHeader(iColumn, !m_bAscend);
		}
	}
	else
	{
		SetSortHeader(iColumn, true);
	}
}

void CUTBListCtrl::SetSortHeader(const int iColumn, bool bAsc)
{
	m_bAscend = bAsc;

	if(m_nCurSort >= 0)
	{
		if(m_nCurSort != iColumn)
		{
			HDITEM itemOld;
			itemOld.mask = HDI_FORMAT;
			m_ctrHeader.GetItem(m_nCurSort, &itemOld);

			itemOld.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);
			m_ctrHeader.SetItem(m_nCurSort, &itemOld);
		}
	}

#define USE_BITMAP_HEADER
#ifndef USE_BITMAP_HEADER
	// change the item to owner drawn.
	HD_ITEM hditem;
	hditem.mask = HDI_FORMAT;
//	VERIFY( m_ctrHeader.GetItem( iColumn, &hditem ) );
	m_ctrHeader.GetItem( iColumn, &hditem );

	hditem.fmt |= HDF_OWNERDRAW;
//	VERIFY( m_ctrHeader.SetItem( iColumn, &hditem ) );
	m_ctrHeader.SetItem( iColumn, &hditem );

#else
	HDITEM itemNew;
	itemNew.mask = HDI_FORMAT | HDI_IMAGE;
	m_ctrHeader.GetItem(iColumn, &itemNew);

	itemNew.mask = HDI_FORMAT | HDI_IMAGE;
	//itemNew.iImage = (m_bAscend == true) ? LC_ASC : LC_DES;
	itemNew.iImage = 0;
	itemNew.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);
	itemNew.fmt |= (m_bAscend ? HDF_SORTUP : HDF_SORTDOWN);
	m_ctrHeader.SetItem(iColumn, &itemNew);

#endif
	// invalidate the header control so it gets redrawn
	m_nCurSort = iColumn;
	Invalidate();
}

int CUTBListCtrl::GetColPos(LPCTSTR szCol)
{
	char str[256];
	ZeroMemory(str, sizeof(str));

	LVCOLUMN lvcol;
	ZeroMemory(&lvcol, sizeof(LVCOLUMN));

	lvcol.mask=LVCF_TEXT|LVCF_WIDTH;
	lvcol.pszText=str;
	lvcol.cchTextMax=256;
	lvcol.cx = 0;

	int nCol = 0;
	while(GetColumn(nCol, &lvcol)){
		CString szBuf = lvcol.pszText;
		if(szBuf == szCol)
			return nCol;
		nCol++;
	}

	return -1;
}

CString CUTBListCtrl::GetColText(int nCol)
{
	char str[256];
	ZeroMemory(str, sizeof(str));

	LVCOLUMN lvcol;
	ZeroMemory(&lvcol, sizeof(LVCOLUMN));

	lvcol.mask=LVCF_TEXT|LVCF_WIDTH;
	lvcol.pszText=str;
	lvcol.cchTextMax=256;
	lvcol.cx = 0;

	GetColumn(nCol, &lvcol);
	CString szBuf = lvcol.pszText;
	return szBuf;
}

BOOL CUTBListCtrl::SetText(int nRow, LPCTSTR szCol, LPCTSTR szData)
{
	return SetItemText(nRow, GetColPos(szCol), szData);
}

CString CUTBListCtrl::GetText(int nRow, LPCTSTR szCol)
{
	return GetItemText(nRow, GetColPos(szCol));
}

void CUTBListCtrl::OnLvnEndScroll(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVSCROLL pStateChanged = reinterpret_cast<LPNMLVSCROLL>(pNMHDR);

	Invalidate();

	*pResult = 0;
}

void CUTBListCtrl::SetCheckHdrState(bool bCheck)
{
	HDITEM itemCheck;
	itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
	m_ctrHeader.GetItem(0, &itemCheck);

	itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
	itemCheck.iImage = (bCheck ? LC_CHKS : LC_CHKU);
	itemCheck.fmt = itemCheck.fmt | HDF_IMAGE | HDF_BITMAP;
	m_ctrHeader.SetItem(0, &itemCheck);
}

bool CUTBListCtrl::GetCheckHdrState()
{
	HDITEM itemCheck;
	itemCheck.mask = HDI_FORMAT | HDI_IMAGE;
	m_ctrHeader.GetItem(0, &itemCheck);

	return (itemCheck.iImage == LC_CHKS);
}

void CUTBListCtrl::SaveColumnState(CString strListName, CString strIniPath)
{
	// 컬럼순서 저장
	CHeaderCtrl* pHdr = GetHeaderCtrl();
	int nCount = pHdr->GetItemCount();
	
	LPINT nColumnOrders = (LPINT)new int[nCount];
	memset(nColumnOrders, 0x00, sizeof(int)*nCount);

    pHdr->GetOrderArray(nColumnOrders, nCount);

	CString strColumnsOrder;
	for(int i=0; i<nCount ;i++)
	{
		strColumnsOrder += ToString(nColumnOrders[i]);
		strColumnsOrder += ";";
	}

	// 컬럼너비 저장
	CString strColumnsWidth;
	for(int i=0; i<nCount ;i++)
	{
		strColumnsWidth += ToString(GetColumnWidth(i));
		strColumnsWidth += ";";
	}

	WritePrivateProfileString(strListName, "ColumnCount", ToString(nCount), strIniPath);
	WritePrivateProfileString(strListName, "OrderList", strColumnsOrder, strIniPath);
	WritePrivateProfileString(strListName, "WidthList", strColumnsWidth, strIniPath);
	WritePrivateProfileString(strListName, "SortColumn", ToString(GetCurSortCol()), strIniPath);
	WritePrivateProfileString(strListName, "IsAscend", ToString(IsAscend()), strIniPath);

	delete nColumnOrders;
}

void CUTBListCtrl::LoadColumnState(CString strListName, CString strIniPath, int nDefaultSortCol/*=-1*/)
{
	CHeaderCtrl* pHdr = GetHeaderCtrl();
	int nCount = pHdr->GetItemCount();

	char buf[1024];

	GetPrivateProfileString(strListName, "ColumnCount", "0", buf, 1024, strIniPath);
	int nSavedCount = _ttoi(buf);
	GetPrivateProfileString(strListName, "OrderList", "", buf, 1024, strIniPath);
	CString strOrderList = buf;
	GetPrivateProfileString(strListName, "WidthList", "", buf, 1024, strIniPath);
	CString strWidthList = buf;

	GetPrivateProfileString(strListName, "SortColumn", "", buf, 1024, strIniPath);
	if(_tcslen(buf) > 0 || nDefaultSortCol >= 0)
	{
		int nSortCol = (_tcslen(buf) > 0 ? _ttoi(buf) : nDefaultSortCol);

		GetPrivateProfileString(strListName, "IsAscend", "1", buf, 1024, strIniPath);
		bool bIsAscend = (_ttoi(buf) != 0);

		if(nSortCol >= nCount)
		{
			nSortCol = nDefaultSortCol;
			bIsAscend = true;
		}

		SetSortCol(nSortCol, bIsAscend);
	}

	if(nSavedCount != nCount) return;

	if(!strOrderList.IsEmpty() && (GetExtendedStyle() & LVS_EX_HEADERDRAGDROP))
	{
		LPINT nColumnOrders = (LPINT)new int[nCount];
		memset(nColumnOrders, 0x00, sizeof(int)*nCount);

		int nIndex = 0;
		int nPos = 0;
		CString strOrder = strOrderList.Tokenize(";", nPos);
		while(strOrder != "")
		{
			nColumnOrders[nIndex] = _ttoi(strOrder);
			strOrder = strOrderList.Tokenize(";", nPos);
			nIndex++;
		}

		pHdr->SetOrderArray(nCount, nColumnOrders);

		delete nColumnOrders;
	}

	if(!strWidthList.IsEmpty())
	{
		int nIndex = 0;
		int nPos = 0;
		CString strWidth = strWidthList.Tokenize(";", nPos);
		while(strWidth != "")
		{
			SetColumnWidth(nIndex, _ttoi(strWidth));
			strWidth = strWidthList.Tokenize(";", nPos);
			nIndex++;
		}
	}
}