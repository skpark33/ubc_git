// UBCTabCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "UBCTabCtrl.h"

// CUBCTabCtrl
IMPLEMENT_DYNAMIC(CUBCTabCtrl, CTabCtrl)

CUBCTabCtrl::CUBCTabCtrl()
{

}

CUBCTabCtrl::~CUBCTabCtrl()
{
}

BEGIN_MESSAGE_MAP(CUBCTabCtrl, CTabCtrl)
END_MESSAGE_MAP()

// CUBCTabCtrl message handlers
void CUBCTabCtrl::InitImageList(UINT nBitmapID)
{
	if(!::IsWindow(GetSafeHwnd()) || m_ilTabs.operator HIMAGELIST())
		{ ASSERT(FALSE); return; }				// tab control has to be created first and image list can be created only once
	if(m_ilTabs.Create(nBitmapID, 16, 1, RGB(0xFF,0,0xFF)))	// add an images list with appropriate background (transparent) color
		SetImageList(&m_ilTabs);
}

void CUBCTabCtrl::InitImageListHighColor(UINT nBitmapID, COLORREF crMask)
{
	if(!::IsWindow(GetSafeHwnd()) || m_ilTabs.operator HIMAGELIST())
		{ ASSERT(FALSE); return; }				// tab control has to be created first and image list can be created only once

	CBitmap bmp;
	bmp.LoadBitmap(nBitmapID);

	if(m_ilTabs.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilTabs.Add(&bmp, crMask); //바탕의 회색이 마스크
		SetImageList(&m_ilTabs);
	}
}

LPARAM CUBCTabCtrl::GetItemParam(int nIndex)
{
	TCITEM item;
	memset(&item, 0x00, sizeof(TCITEM));
	item.mask = TCIF_PARAM;

	if(!GetItem(nIndex, &item)) return NULL;

	return item.lParam;
}

void CUBCTabCtrl::SetItemParam(int nIndex, LPARAM lParam)
{
	TCITEM item;
	memset(&item, 0x00, sizeof(TCITEM));
	item.mask = TCIF_PARAM;
	item.lParam = lParam;

	SetItem(nIndex, &item);
}