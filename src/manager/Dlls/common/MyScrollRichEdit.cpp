// MyScrollEdit.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MyScrollRichEdit.h"

// CMyScrollRichEdit

#define		STR_COPY			"Copy"
#define		ID_MENU_COPY	20010

IMPLEMENT_DYNAMIC(CMyScrollRichEdit, CRichEditCtrl)
CMyScrollRichEdit::CMyScrollRichEdit()
{
	m_MaxLine = 3000;
	m_StartPos = 0;
	m_EndPos = 0;
}

CMyScrollRichEdit::~CMyScrollRichEdit()
{
}

BEGIN_MESSAGE_MAP(CMyScrollRichEdit, CRichEditCtrl)
	ON_MESSAGE(WM_USER + 1002, Message)
	ON_WM_CTLCOLOR()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_MENU_COPY, OnMenuCopy)
END_MESSAGE_MAP()

// CMyScrollRichEdit 메시지 처리기입니다.

void CMyScrollRichEdit::SetMaxLine(int MaxLine, int LineMaxChar)
{
	m_MaxLine = MaxLine;
}

void CMyScrollRichEdit::WriteEditHighLightBox(char* Msg, COLORREF TextColor, COLORREF BkColor)
{
	WriteEditBox(Msg);
	bstr_t strBuf = Msg;

	m_StartPos;
//	SetSel(m_StartPos-strlen(Msg), m_StartPos);
	SetSel(m_StartPos-strBuf.length(), m_StartPos);

	CHARFORMAT2 cf;
	cf.cbSize = sizeof(CHARFORMAT2);
	cf.dwMask		= CFM_COLOR | CFM_BACKCOLOR | CFM_BOLD;
	cf.dwEffects	= CFE_BOLD;//(unsigned long)~( CFE_AUTOCOLOR | CFE_UNDERLINE | CFE_BOLD);
	cf.crTextColor = TextColor;//RGB(0,0,0);
	cf.crBackColor = BkColor;//RGB(0xCC,0x99,0x99);
	SetSelectionCharFormat(cf);
	HideSelection(TRUE, FALSE);
}

void CMyScrollRichEdit::WriteEditBox(CString* Msg, BOOL bVScr)
{
	WriteEditBox(Msg->GetBuffer(), bVScr);
}

void CMyScrollRichEdit::WriteEditBox(char* Msg, BOOL bVScr)
{
	int nLine = GetLineCount();
	if(nLine > m_MaxLine ) {
		nLine =- m_MaxLine;
		SetSel(0, LineIndex(nLine));
		ReplaceSel("");
	}

	int nPos;
	SetSel(m_StartPos, m_EndPos);
	nPos = m_EndPos;
	ReplaceSel(Msg);
	GetSel(m_StartPos, m_EndPos);

	int nMsgLine = GetLineCount() - nLine;
	if(bVScr){
		SendMessage(WM_VSCROLL, SB_BOTTOM, 0);
	}else if(nLine > 15){
		LineScroll((nMsgLine<15)?nMsgLine:15);
		SetSel(nPos, nPos);
	}else{
		SetSel(nPos, nPos);
	}
}

void CMyScrollRichEdit::Clear()
{
	m_StartPos = 0;
	m_EndPos = 0;
	SetWindowText("");
}

LRESULT CMyScrollRichEdit::Message(WPARAM id, LPARAM msg)
{
	if(id == 0) {
		int nLine = GetLineCount();
		if(nLine > m_MaxLine ) {
			SetSel(0, LineIndex(nLine - m_MaxLine));
			ReplaceSel("");
		}
		SetSel(m_StartPos, m_EndPos);
	} else {
		GetSel(m_StartPos, m_EndPos);
	}
	return 0;
}

BOOL CMyScrollRichEdit::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->wParam)
	{
		case VK_DELETE :
			Clear();
		default:
			;
	}
	return CRichEditCtrl::PreTranslateMessage(pMsg);
}

void CMyScrollRichEdit::OnRButtonDown(UINT nFlags, CPoint point)
{
	CPoint popuppos;
	GetCursorPos(&popuppos);

	CMenu popup;
	VERIFY(popup.CreatePopupMenu());
	popup.InsertMenu(-1, MF_BYPOSITION, ID_MENU_COPY, STR_COPY);

	popup.TrackPopupMenu(TPM_LEFTALIGN, popuppos.x, popuppos.y, this);

	CRichEditCtrl::OnRButtonDown(nFlags, point);
}

void CMyScrollRichEdit::OnMenuCopy()
{
	Copy();
}