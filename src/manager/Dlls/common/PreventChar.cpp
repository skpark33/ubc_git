//////////////////////////////////////////////////////////////////////////
//
// 콘텐츠 명과 화일명으로 사용할수 없는 특수문자는 다음과 같다.
// 1) 원래 MS 윈도우즈에서 파일명으로 사용할 수 없는 문자
// <>\/*|":
// 2) 추가적으로 사용할 수 없는 문자
// [],!%`' 
//
// [],<> 는 괄호로 치환하고, 나머지는 모두 언더스코어로 치환한다.
//
//////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "common/PreventChar.h"

CPreventChar::CPreventChar(void)
{
	//m_mapPreventChar[eFileName][_T("[" )] = _T("(");
	//m_mapPreventChar[eFileName][_T("]" )] = _T(")");
	//m_mapPreventChar[eFileName][_T("," )] = _T("_");
	//m_mapPreventChar[eFileName][_T("!" )] = _T("_");
	//m_mapPreventChar[eFileName][_T("%" )] = _T("_");
	//m_mapPreventChar[eFileName][_T("`" )] = _T("_");
	//m_mapPreventChar[eFileName][_T("'" )] = _T("_");
	m_mapPreventChar[eFileName][_T("~" )] = _T("_");
	m_mapPreventChar[eFileName][_T("<" )] = _T("(");	// OS에서 파일명으로 사용하지 못함
	m_mapPreventChar[eFileName][_T(">" )] = _T(")");	// ..
	m_mapPreventChar[eFileName][_T("\\")] = _T("_");	// ..
	m_mapPreventChar[eFileName][_T("/" )] = _T("_");	// ..
	m_mapPreventChar[eFileName][_T("*" )] = _T("_");	// ..
	m_mapPreventChar[eFileName][_T("|" )] = _T("_");	// ..
	m_mapPreventChar[eFileName][_T("\"")] = _T("_");	// ..
	m_mapPreventChar[eFileName][_T(":" )] = _T("_");	// ..
	m_mapPreventChar[eFileName][_T("?" )] = _T("_");	// ..

	m_mapPreventChar[ePackageName][_T("[" )] = _T("(");
	m_mapPreventChar[ePackageName][_T("]" )] = _T(")");
	m_mapPreventChar[ePackageName][_T("<" )] = _T("(");
	m_mapPreventChar[ePackageName][_T(">" )] = _T(")");
	m_mapPreventChar[ePackageName][_T("," )] = _T("_");
	m_mapPreventChar[ePackageName][_T("!" )] = _T("_");
	m_mapPreventChar[ePackageName][_T("%" )] = _T("_");
	m_mapPreventChar[ePackageName][_T("`" )] = _T("_");
	m_mapPreventChar[ePackageName][_T("'" )] = _T("_");
	m_mapPreventChar[ePackageName][_T("\\")] = _T("_");
	m_mapPreventChar[ePackageName][_T("/" )] = _T("_");
	m_mapPreventChar[ePackageName][_T("*" )] = _T("_");
	m_mapPreventChar[ePackageName][_T("|" )] = _T("_");
	m_mapPreventChar[ePackageName][_T("\"")] = _T("_");
	m_mapPreventChar[ePackageName][_T(":" )] = _T("_");
	m_mapPreventChar[ePackageName][_T("~")] = _T("_");  
	m_mapPreventChar[ePackageName][_T("?")] = _T("_");  

	m_mapPreventChar[eId][_T("[" )] = _T("_");
	m_mapPreventChar[eId][_T("]" )] = _T("_");
	m_mapPreventChar[eId][_T("<" )] = _T("_");
	m_mapPreventChar[eId][_T(">" )] = _T("_");
	m_mapPreventChar[eId][_T("," )] = _T("_");
	m_mapPreventChar[eId][_T("!" )] = _T("_");
	m_mapPreventChar[eId][_T("%" )] = _T("_");
	m_mapPreventChar[eId][_T("`" )] = _T("_");
	m_mapPreventChar[eId][_T("'" )] = _T("_");
	m_mapPreventChar[eId][_T("\\")] = _T("_");
	m_mapPreventChar[eId][_T("/" )] = _T("_");
	m_mapPreventChar[eId][_T("*" )] = _T("_");
	m_mapPreventChar[eId][_T("|" )] = _T("_");
	m_mapPreventChar[eId][_T("\"")] = _T("_");
	m_mapPreventChar[eId][_T(":" )] = _T("_");
	m_mapPreventChar[eId][_T("~")] = _T("_");  
	m_mapPreventChar[eId][_T("?")] = _T("_");  

	m_mapPreventChar[eId][_T(" ")] = _T("_");  
	m_mapPreventChar[eId][_T("#")] = _T("_");  
	m_mapPreventChar[eId][_T("@")] = _T("_");  
	m_mapPreventChar[eId][_T("$")] = _T("_");  
	m_mapPreventChar[eId][_T("^")] = _T("_");  
	m_mapPreventChar[eId][_T("&")] = _T("_");  
	m_mapPreventChar[eId][_T("-")] = _T("_"); 
	m_mapPreventChar[eId][_T("+")] = _T("_");  
	m_mapPreventChar[eId][_T("=")] = _T("_");  
	m_mapPreventChar[eId][_T("(")] = _T("_");  
	m_mapPreventChar[eId][_T(")")] = _T("_");  
	m_mapPreventChar[eId][_T("{")] = _T("_");  
	m_mapPreventChar[eId][_T("}")] = _T("_");  
	m_mapPreventChar[eId][_T(".")] = _T("_");  
	m_mapPreventChar[eId][_T(";")] = _T("_");  


}

CPreventChar::~CPreventChar(void)
{
}

CPreventChar* CPreventChar::_instance;
CPreventChar* CPreventChar::GetInstance()
{
	if(_instance == NULL) _instance = new CPreventChar();
	return _instance;
}

CString CPreventChar::GetPreventChar(PREVENT_TYPE nType)
{
	CString strResult;

	POSITION pos = m_mapPreventChar[nType].GetStartPosition();
	while(pos)
	{
		CString strKey;
		CString strKeyValue;
		m_mapPreventChar[nType].GetNextAssoc(pos, strKey, strKeyValue);
		
		if(!strResult.IsEmpty()) strResult += " ";
		strResult += strKey;
	}

	return strResult;
}

bool CPreventChar::IsPreventChar(CString strValue, PREVENT_TYPE nType)
{
	return (m_mapPreventChar[nType].PLookup(strValue) != NULL);
}

bool CPreventChar::IsPreventChar(TCHAR szValue, PREVENT_TYPE nType)
{
	return IsPreventChar(CString(szValue), nType);
}

bool CPreventChar::HavePreventChar(CString strValue, PREVENT_TYPE nType)
{
	//for(int i=0; i<strValue.GetLength() ;i++)
	//{
	//	if(IsPreventChar(strValue[i])) return true;
	//}
	//return false;

	// 0000792: 콘텐츠명에 사용하지 못하는 문자를 체크할때 UNICODE로 변환 후 체크할것.
	WCHAR szWide[MAX_PATH] = {0};
	MultiByteToWideChar(CP_ACP, 0, strValue, strValue.GetLength(), szWide, MAX_PATH);

	for(int i=0; i<strValue.GetLength() ;i++)
	{
		if(IsPreventChar(CString(szWide[i]), nType)) return true;
	}

	return false;
}

CString CPreventChar::ConvertToAvailableChar(CString strValue, PREVENT_TYPE nType)
{
	if(!HavePreventChar(strValue, nType)) return strValue;

	CString strResult = strValue;

	POSITION pos = m_mapPreventChar[nType].GetStartPosition();
	while(pos)
	{
		CString strKey;
		CString strKeyValue;
		m_mapPreventChar[nType].GetNextAssoc(pos, strKey, strKeyValue);
		strResult.Replace(strKey, strKeyValue);
	}

	return strResult;
}

void CPreventChar::AddPreventChar(CString strPreventChar, CString strConvertChar, PREVENT_TYPE nType)
{
	m_mapPreventChar[nType][strPreventChar] = strConvertChar;
}

void CPreventChar::AddPreventChar(TCHAR szPreventChar, TCHAR szConvertChar, PREVENT_TYPE nType)
{
	AddPreventChar(CString(szPreventChar), CString(szConvertChar), nType);
}

void CPreventChar::DelPreventChar(CString strPreventChar, PREVENT_TYPE nType)
{
	m_mapPreventChar[nType].RemoveKey(strPreventChar);
}

void CPreventChar::DelPreventChar(TCHAR szPreventChar, PREVENT_TYPE nType)
{
	DelPreventChar(CString(szPreventChar), nType);
}