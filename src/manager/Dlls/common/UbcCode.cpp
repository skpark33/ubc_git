#include "StdAfx.h"
#include "UbcCode.h"
#include "TraceLog.h"
#include "common/libProfileManager/ProfileManager.h"

#ifndef _UBCCOPCOMMON_
	#include "Enviroment.h"
#endif

CUbcCode* CUbcCode::_instance;
CCriticalSection CUbcCode::_cs;
CUbcCode* CUbcCode::GetInstance(LPCTSTR szCustomer)
{
	_cs.Lock();
	if(_instance == NULL)
	{
		_instance = new CUbcCode(szCustomer);

	}
	_cs.Unlock();

	return _instance;
}

void CUbcCode::ClearInstance()
{
	_cs.Lock();
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
	_cs.Unlock();
}

CUbcCode::CUbcCode(LPCTSTR szCustomer)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	m_strDrive = szDrive;
	m_strPath = szPath;
	m_strIniPath.Format("%s%sdata\\%s", m_strDrive, m_strPath, UBCCODE_INI);

#ifdef _UBCMANAGER_
	CCopModule::GetObject()->GetCodeList(m_listCode, GetEnvPtr()->m_strCustomer);
#elif defined (_UBCSTUDIO_EE_)
	getCodeList(m_listCode, GetEnvPtr()->m_strCustomer);
#elif defined (_UBCCOPCOMMON_)
	CCopModule::GetObject()->GetCodeList(m_listCode, szCustomer);
#endif

	// 서버에서 가져오기가 실패하는 경우 ini에서 로드
	if(m_listCode.GetCount() <= 0)
	{
		CProfileManager objIniManager(m_strIniPath);
		int nCodeCnt = objIniManager.GetProfileInt(_T("HEADER"), _T("CODE_COUNT"));
		for(int i=0; i<nCodeCnt ;i++)
		{
			CString strEntry;
			strEntry.Format(_T("CODE_%d"), i);
			TCHAR buf[1024]={0};

			SCodeItem info;

			info.strMgrId        = objIniManager.GetProfileString(strEntry, _T("MgrId"       ));
			info.strCodeId       = objIniManager.GetProfileString(strEntry, _T("CodeId"      ));
			info.strCategoryName = objIniManager.GetProfileString(strEntry, _T("CategoryName"));
			info.strEnumString   = objIniManager.GetProfileString(strEntry, _T("EnumString"  ));
			info.nEnumNumber     = objIniManager.GetProfileInt   (strEntry, _T("EnumNumber"  ));
			info.bVisible        = objIniManager.GetProfileInt   (strEntry, _T("Visible"     ), 1);
			info.nDisplayOrder   = objIniManager.GetProfileInt   (strEntry, _T("DisplayOrder"));

			m_listCode.AddTail(info);
		}
	}

	POSITION pos = m_listCode.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SCodeItem info = m_listCode.GetNext(pos);

		CString strKey;
		strKey.Format(_T("%s`%d"), info.strCategoryName, info.nEnumNumber);

		m_mapCode[strKey] = posOld;
	}
}

CUbcCode::~CUbcCode(void)
{
	if(m_listCode.GetCount() <= 0) return;

	CProfileManager objIniManager;

	// ini에 write
	objIniManager.WriteProfileInt(_T("HEADER"), _T("CODE_COUNT"), m_listCode.GetCount());

	int nCodeCnt=0;
	POSITION pos = m_listCode.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SCodeItem info = m_listCode.GetNext(pos);

		CString strEntry;
		strEntry.Format(_T("CODE_%d"), nCodeCnt);

		objIniManager.WriteProfileString(strEntry, _T("MgrId"       ), info.strMgrId          );
		objIniManager.WriteProfileString(strEntry, _T("CodeId"      ), info.strCodeId         );
		objIniManager.WriteProfileString(strEntry, _T("CategoryName"), info.strCategoryName   );
		objIniManager.WriteProfileString(strEntry, _T("EnumString"  ), info.strEnumString     );
		objIniManager.WriteProfileInt   (strEntry, _T("EnumNumber"  ), info.nEnumNumber       );
		objIniManager.WriteProfileInt   (strEntry, _T("Visible"     ), (info.bVisible ? 1 : 0));
		objIniManager.WriteProfileInt   (strEntry, _T("DisplayOrder"), (int)info.nDisplayOrder);

		nCodeCnt++;
	}

	objIniManager.Write(m_strIniPath);
}

CString CUbcCode::GetCodeName(CString strCategoryName, int nEnumNumber)
{
	CString retval = GetCodeInfo(strCategoryName, nEnumNumber).strEnumString;
	if(retval.IsEmpty()){
		return ToString(nEnumNumber);
	}
	return retval;
}

SCodeItem CUbcCode::GetCodeInfo(CString strCategoryName, int nEnumNumber)
{
	CString strKey;
	strKey.Format(_T("%s`%d"), strCategoryName, nEnumNumber);

	SCodeItem item;

	POSITION pos = (POSITION)m_mapCode[strKey];
	if(pos)
	{
		item = (SCodeItem)m_listCode.GetAt(pos);
	}

	return item;
}

int CUbcCode::GetCategoryInfo(CString strCategoryName, CodeItemList& list)
{
	TraceLog(("GetCategoryInfo(%s)", strCategoryName));

	int nCount = 0;
	POSITION pos = m_listCode.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = m_listCode.GetNext(pos);
		if(strCategoryName == info.strCategoryName)
		{
			list.AddTail(info);
			nCount++;
		}
	}

	return nCount;
}

void CUbcCode::FillListCtrl(CUTBListCtrlEx& lcList, CString strCategoryName, BOOL bHasEmpty/*=FALSE*/, CString strHasEmptyName/*=_T("")*/)
{
	TraceLog(("FillListCtrl(%s)", strCategoryName));
	lcList.DeleteAllItems();
	int nRowIndex = 0;

	if(bHasEmpty)
	{
		lcList.InsertItem(nRowIndex, strHasEmptyName);
		lcList.SetItemData(nRowIndex, -1);

		nRowIndex++;
		TraceLog(("FillListCtrl(%s) is empty", strCategoryName));
	}

	CodeItemList list;
	GetCategoryInfo(strCategoryName, list);
	POSITION pos = list.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = list.GetNext(pos);

		lcList.InsertItem(nRowIndex, info.strEnumString);
		lcList.SetItemData(nRowIndex, info.nEnumNumber);

		nRowIndex++;
	}
	TraceLog(("FillListCtrl(%s) is %d", strCategoryName, nRowIndex));

}

int CUbcCode::FindListCtrl(CUTBListCtrlEx& lcList, int nEnumNumber)
{
	for(int i=0; i<lcList.GetItemCount() ;i++)
	{
		if((long)lcList.GetItemData(i) == nEnumNumber)
		{
			return i;
		}
	}

	return -1;
}

void CUbcCode::FillComboBoxCtrl(CComboBox& cbCombo, CString strCategoryName, BOOL bHasEmpty/*=FALSE*/, CString strHasEmptyName/*=_T("")*/)
{
	TraceLog(("FillComboBoxCtrl(%s)", strCategoryName));

	cbCombo.ResetContent();

	if(bHasEmpty)
	{
		int nIndex = cbCombo.AddString(strHasEmptyName);
		cbCombo.SetItemData(nIndex, -1);
	}

	CodeItemList list;
	GetCategoryInfo(strCategoryName, list);

	POSITION pos = list.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = list.GetNext(pos);
		int nIndex = cbCombo.AddString(info.strEnumString);
		TraceLog(("strEnumSTring=%s", info.strEnumString));
		cbCombo.SetItemData(nIndex, info.nEnumNumber);
	}
}

int CUbcCode::FindComboBoxCtrl(CComboBox& cbCombo, int nEnumNumber)
{
	for(int i=0; i<cbCombo.GetCount() ;i++)
	{
		if((long)cbCombo.GetItemData(i) == nEnumNumber)
		{
			return i;
		}
	}

	return -1;
}