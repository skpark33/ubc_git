#pragma once

typedef enum tagITEM_FMT
{
	FMT_TEXT       = 0x0001
,	FMT_START_DATE = 0x0002
,	FMT_END_DATE   = 0x0004
,	FMT_LPARAM     = 0x0008
} ITEM_FMT;

typedef struct tagPLAN_ITEM
{
	CString		strText    ;
	CTime		tmStartTime;
	CTime		tmEndTime  ;
	LPARAM		lParam     ;

	bool		bGradient  ;
	COLORREF	clrColor   ;

	tagPLAN_ITEM()
	{
		lParam    = NULL;
		bGradient = true;
		clrColor  = -1  ;
	}
} ST_PLAN_ITEM, *P_PLAN_ITEM;
