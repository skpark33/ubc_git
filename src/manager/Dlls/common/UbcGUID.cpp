#include "StdAfx.h"
#include "common/UbcGUID.h"

CUbcGUID* CUbcGUID::_instance;
CUbcGUID* CUbcGUID::GetInstance()
{
	if(_instance == NULL) _instance = new CUbcGUID();
	return _instance;
}

CMapStringToString* CUbcGUID::GetMapTable()
{
	return &m_mapContentsIdToGUID;
}

void CUbcGUID::Clear()
{
	m_mapContentsIdToGUID.RemoveAll();
}

CString CUbcGUID::CreateGUID()
{
	GUID guid;
	WCHAR szPackID[40];

	if(CoCreateGuid(&guid) != S_OK)
	{
		//_tprintf(_T("GUID 생성 실패"));
		return _T("");
	}

	if(!StringFromGUID2(guid, szPackID, 39))
	{
		//_tprintf(_T("GUID 변환 실패"));
		return _T("");
	}

	//_tprintf(_T("%s"), szPackID);

	CString strGUID;
	strGUID = szPackID;
	//strGUID.Remove('-');
	//strGUID.Remove('{');
	//strGUID.Remove('}');
	//strGUID = "G_" + sGUID_plane;

	// {392DD706-570B-43A7-819C-F7250D5B6265}
	return strGUID;
}

CString CUbcGUID::ToGUID(CString strId)
{
	CString strGUID;
	m_mapContentsIdToGUID.Lookup(strId, strGUID);

	if(strId.IsEmpty())
	{
		strGUID = CreateGUID();
	}
	else if(IsGUID(strId))
	{
		return strId;
	}
	else if(IsGUID(strGUID))
	{
		return strGUID;
	}
	else
	{
		strGUID = CreateGUID();
	}

	m_mapContentsIdToGUID[strId] = strGUID;

	return strGUID;
}

CString CUbcGUID::GetGUID(CString strId)
{
	if(strId.IsEmpty()) return _T("");

	if(IsGUID(strId)) return strId;

	CString strGUID;
	m_mapContentsIdToGUID.Lookup(strId, strGUID);

	if(IsGUID(strGUID)) return strGUID;

	return strId;
}

BOOL CUbcGUID::IsGUID(CString strGUID)
{
	// {392DD706-570B-43A7-819C-F7250D5B6265}
	if(strGUID.GetLength() != 38) return FALSE;

	if( strGUID[0]  != '{' || strGUID[37] != '}' ||
		strGUID[9]  != '-' || strGUID[14] != '-' || 
		strGUID[19] != '-' || strGUID[24] != '-' )
	{
		return FALSE;
	}

	return TRUE;
}
