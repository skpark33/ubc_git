// UTBHeaderCtrl.cpp : 구?E파일입니다.
//

#include "stdafx.h"
#include "UTBHeaderCtrl.h"
#include "MemDC.h"

// CUTBHeaderCtrl

IMPLEMENT_DYNAMIC(CUTBHeaderCtrl, CHeaderCtrl)

CUTBHeaderCtrl::CUTBHeaderCtrl()
{
	m_nFixedCol      = -1;
	m_nFixedColRight = 0;

	m_nFontHeight    = 16;

	m_iSpacing       = 3;
	m_cr3DHighLight  = ::GetSysColor(COLOR_3DHIGHLIGHT);
	m_cr3DShadow     = ::GetSysColor(COLOR_3DSHADOW);
	m_cr3DFace       = ::GetSysColor(COLOR_3DFACE);
	m_cr3DDarkFace   = ::GetSysColor(COLOR_ACTIVEBORDER);
	m_crBtnText      = ::GetSysColor(COLOR_BTNTEXT);
}

CUTBHeaderCtrl::~CUTBHeaderCtrl()
{
	for(int i=0; i<m_HeaderInfoList.GetCount(); i++)
	{
		P_HDR_COL_INFO info = m_HeaderInfoList.GetAt(i);
		delete info;
	}
	m_HeaderInfoList.RemoveAll();
}


BEGIN_MESSAGE_MAP(CUTBHeaderCtrl, CHeaderCtrl)
	ON_WM_SIZE()
	ON_WM_PAINT()
END_MESSAGE_MAP()

// CUTBHeaderCtrl 메시햨E처리기입니다.

void CUTBHeaderCtrl::OnSize(UINT nType, int cx, int cy)
{
	CHeaderCtrl::OnSize(nType, cx, cy);

	CalcFontHeight();
}

void CUTBHeaderCtrl::CalcFontHeight()
{
	if (IsWindow(m_hWnd))
	{
		CDC* pDC=GetDC();
		int nOldDC=pDC->SaveDC();

		m_nFontHeight = pDC->GetTextExtent(_T(" ")).cy;

		pDC->RestoreDC(nOldDC);
		ReleaseDC(pDC);
	}
}

LRESULT CUTBHeaderCtrl::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch( message )
	{
	case HDM_INSERTITEMA:
	case HDM_INSERTITEMW:
		{
			int nIndex = (int)wParam;
			//if(GetColType(nIndex) == COL_CALENDAR)
			//{
			//	HD_ITEM* phdi = (HD_ITEM*)lParam;
			//	CTimeSpan tmDiff = GetColInfo(nIndex)->m_tmEndDate - GetColInfo(nIndex)->m_tmStartDate;
			//	phdi->cxy = (tmDiff.GetDays() + 1) * DAY_PIX;
			//}
		}
		break;

	case HDM_DELETEITEM:
		{
			int nIndex = (int)wParam;

			P_HDR_COL_INFO pHdrConInfo = GetColInfo(nIndex);
			if(pHdrConInfo)
			{
				for(int i=0; i<m_HeaderInfoList.GetCount(); i++)
				{
					P_HDR_COL_INFO info = m_HeaderInfoList.GetAt(i);
					if(info == pHdrConInfo)
					{
						m_HeaderInfoList.RemoveAt(i);
						break;
					}
				}
				delete pHdrConInfo;
			}
		}
		break;

	case HDM_LAYOUT:
		{
			HD_LAYOUT *layout	=(HD_LAYOUT*)lParam;
			RECT *rect			=layout->prc;
			WINDOWPOS *pos		=layout->pwpos;
			LRESULT lRes		=CWnd::DefWindowProc(message,wParam,lParam);

			int nHeight = 0;

			CalcFontHeight();

			int nColHeightScale = 1;
			for(int i=0; i<GetItemCount() && nColHeightScale==1; i++)
			{
				switch( GetColType(OrderToIndex(i)) )
				{
				case COL_CALENDAR:
					nColHeightScale = 3;
					break;

				case COL_INDICATOR_CALENDAR:
					nColHeightScale = 4;
					break;
				}
			}

			nHeight = m_nFontHeight*nColHeightScale+4;

			// ?E叢동?
			pos->cy	= nHeight;

			// Data영역
			rect->top = nHeight;

			return lRes;
		}
		break;

	case HDM_GETITEMRECT:
		{
			int nIndex = (int)wParam;
			LPRECT lprc = (LPRECT)lParam;
			LRESULT lRes = CHeaderCtrl::DefWindowProc(message, wParam, lParam);

			CUTBHeaderCtrl::HDR_COL_TYPE col_type = GetColType(nIndex);
			if( col_type == COL_CALENDAR ||
				col_type == COL_INDICATOR_CALENDAR )
			{
				CTimeSpan tmDiff = GetColInfo(nIndex)->m_tmEndDate - GetColInfo(nIndex)->m_tmStartDate;
				lprc->right = lprc->left + (tmDiff.GetDays() + 1) * DAY_PIX;
			}

			int nSbPos = GetParent()->GetScrollPos(SB_HORZ);
			if(nIndex <= m_nFixedCol)
			{
				lprc->left += nSbPos;
				lprc->right += nSbPos;
			}

			return lRes;
		}
		break;

	default:
		break;
	}

	return CHeaderCtrl::DefWindowProc(message, wParam, lParam);
}

void CUTBHeaderCtrl::OnPaint()
{
	CPaintDC dc(this);

	if(m_nFixedCol < 0)
	{
		DrawCtrl(&dc);
	}
	else
	{
		CMemDC MemDC(&dc);
		DrawCtrl(&MemDC);
	}
}

void CUTBHeaderCtrl::CalcFixedColRight()
{
	m_nFixedColRight = 0;

	if(m_nFixedCol < 0) return;
	int nSbPos = GetParent()->GetScrollPos(SB_HORZ);
	if(nSbPos <= 0) return;

	CRect rectItem;
	GetItemRect(m_nFixedCol, rectItem);

	m_nFixedColRight = rectItem.right;
}

///////////////////////////////////////////////////////////////////////////////
// DrawCtrl
void CUTBHeaderCtrl::DrawCtrl(CDC* pDC)
{
	CRect rectClip;
	if (pDC->GetClipBox(&rectClip) == ERROR)
		return;

	pDC->FillSolidRect(rectClip, RGB(255,255,255));
//	pDC->FillSolidRect(rectClip, m_cr3DFace);
//	pDC->Draw3dRect(rectClip, m_cr3DHighLight, m_cr3DShadow);

	int iItems = GetItemCount();
	ASSERT(iItems >= 0);

	CalcFixedColRight();

	CPen* pPen = pDC->GetCurrentPen();

	//CFont* pOldFont = pDC->SelectObject(m_pTextFont);
	static CFont font;
	if( (HFONT)font == NULL ) font.CreatePointFont(8*10, "Tahoma");
	//CFont* pNewFont = GetParent()->GetFont();
	//if( pNewFont == NULL ) pNewFont=&font;
	CFont* pOldFont = pDC->SelectObject(/*pNewFont*/ &font);

	pDC->SetBkColor(m_cr3DFace);
	pDC->SetTextColor(m_crBtnText);

	for (int i = 0; i < iItems; i++)
	{
		CRect rectItem;
		int iItem = OrderToIndex(i);

		TCHAR szText[1024] = {0};

		HDITEM hditem = {0};
		hditem.mask = HDI_WIDTH|HDI_FORMAT|HDI_TEXT|HDI_IMAGE|HDI_BITMAP;
		hditem.pszText = szText;
		hditem.cchTextMax = sizeof(szText);
		VERIFY(GetItem(iItem, &hditem));
		VERIFY(GetItemRect(iItem, rectItem));

		CRgn rgn;
		if(m_nFixedCol >= 0)
		{
			if( iItem > m_nFixedCol )
			{
				if( rectItem.right <= m_nFixedColRight )
				{
					continue;
				}
				else if( rectItem.left <= m_nFixedColRight )
				{
					rgn.CreateRectRgn(m_nFixedColRight-rectClip.left, rectItem.top, rectItem.right, rectItem.bottom);
					pDC->SelectClipRgn(&rgn);
					rgn.DeleteObject();
				}
			}
		}

		if ( rectItem.right >= rectClip.left || rectItem.left <= rectClip.right )
		{
			if (hditem.fmt & HDF_OWNERDRAW)
			{
				DRAWITEMSTRUCT disItem = {0};
				disItem.CtlType = ODT_BUTTON;
				disItem.CtlID = GetDlgCtrlID();
				disItem.itemID = iItem;
				disItem.itemAction = ODA_DRAWENTIRE;
				disItem.itemState = 0;
				disItem.hwndItem = m_hWnd;
				disItem.hDC = pDC->m_hDC;
				disItem.rcItem = rectItem;
				disItem.itemData = 0;

				DrawItem(&disItem);
			}
			else
			{
				pDC->FillSolidRect(rectItem, (iItem <= m_nFixedCol ? m_cr3DDarkFace : m_cr3DFace));

				switch( GetColType(i) )
				{
				case COL_CALENDAR:
					DrawCalendar(pDC, iItem, rectItem, &hditem);
					break;

				case COL_INDICATOR_CALENDAR:
					DrawIndCalendar(pDC, iItem, rectItem, &hditem);
					break;

				default:
					rectItem.DeflateRect(m_iSpacing, 0);
					DrawItem(pDC, iItem, rectItem, &hditem);
					rectItem.InflateRect(m_iSpacing, 0);
					break;
				}
				pDC->Draw3dRect(rectItem, m_cr3DHighLight, m_cr3DShadow);
			}
		}

		pDC->SelectClipRgn(NULL);
	}

	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pPen);
}

///////////////////////////////////////////////////////////////////////////////
// DrawItem
void CUTBHeaderCtrl::DrawItem(LPDRAWITEMSTRUCT)
{
	ASSERT(FALSE);  // must override for self draw header controls
}

///////////////////////////////////////////////////////////////////////////////
// DrawItem
void CUTBHeaderCtrl::DrawItem(CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi)
{
	ASSERT(lphdi->mask & HDI_FORMAT);

	int iWidth = 0;

	CBitmap* pBitmap = NULL;
	BITMAP BitmapInfo = {0};

	if (lphdi->fmt & HDF_BITMAP)
	{
		ASSERT(lphdi->mask & HDI_BITMAP);
		ASSERT(lphdi->hbm);

		pBitmap = CBitmap::FromHandle(lphdi->hbm);
		if (pBitmap)
			VERIFY(pBitmap->GetObject(sizeof(BITMAP), &BitmapInfo));
	}

	rect.left  += ((iWidth = DrawImage   (pDC, nCol, rect, lphdi, FALSE                     )) != 0) ? iWidth + m_iSpacing : 0;
	rect.right -= ((iWidth = DrawBitmap  (pDC, nCol, rect, lphdi, pBitmap, &BitmapInfo, TRUE)) != 0) ? iWidth + m_iSpacing : 0;
	rect.left  += ((iWidth = DrawText    (pDC, nCol, rect, lphdi                            )) != 0) ? ((rect.CenterPoint().x + iWidth/2) - rect.left) + m_iSpacing : 0;
	rect.right -= ((iWidth = DrawSortIcon(pDC, nCol, rect, lphdi                            )) != 0) ? iWidth + m_iSpacing : 0;
}

///////////////////////////////////////////////////////////////////////////////
// DrawImage
int CUTBHeaderCtrl::DrawImage(CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi, BOOL bRight)
{
	CImageList* pImageList = GetImageList();
	int iWidth = 0;

	if (lphdi->iImage == LC_EMPTY) return iWidth;
	if (!pImageList) return iWidth;
	if (rect.Width() <= 0) return iWidth;

	IMAGEINFO imageinfo = {0};
	if(!pImageList->GetImageInfo(lphdi->iImage, &imageinfo)) return iWidth;

	CRect rcImage = imageinfo.rcImage;

	POINT point;

	point.y = rect.CenterPoint().y - (rcImage.Height() >> 1);

	if (bRight)
		point.x = rect.right - rcImage.Width();
	else
		point.x = rect.left;

	// save image list background color
//	COLORREF rgb = pImageList->GetBkColor();

	// set image list background color to same as header control
//	pImageList->SetBkColor(pDC->GetBkColor());
	pImageList->DrawEx(pDC, lphdi->iImage, point, CSize(0,0), CLR_NONE, CLR_NONE, ILD_NORMAL);
//	pImageList->DrawIndirect(pDC, lphdi->iImage, point, size, CPoint(0, 0));
//	pImageList->SetBkColor(rgb);

	iWidth = rcImage.Width();

	return iWidth;
}

///////////////////////////////////////////////////////////////////////////////
// Draw Sort Icon
int CUTBHeaderCtrl::DrawSortIcon(CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi)
{
	CImageList* pImageList = GetImageList();
	int iWidth = 0;

	int nImageIdx = LC_EMPTY;

	if(lphdi->fmt & HDF_SORTUP  ) nImageIdx = LC_SORTUP;
	if(lphdi->fmt & HDF_SORTDOWN) nImageIdx = LC_SORTDOWN;

	if(nImageIdx == LC_EMPTY) return iWidth;
	if (!pImageList) return iWidth;
	if (rect.Width() <= 0) return iWidth;

	IMAGEINFO imageinfo = {0};
	if(!pImageList->GetImageInfo(lphdi->iImage, &imageinfo)) return iWidth;

	CRect rcImage = imageinfo.rcImage;

	POINT point = {0};
	point.y = rect.CenterPoint().y - (rcImage.Height() >> 1);
	point.x = rect.left;

	pImageList->DrawEx(pDC, nImageIdx, point, CSize(0,0), CLR_NONE, CLR_NONE, ILD_NORMAL);

	iWidth = rcImage.Width();

	return iWidth;
}

///////////////////////////////////////////////////////////////////////////////
// DrawBitmap
int CUTBHeaderCtrl::DrawBitmap(CDC* pDC, 
								int nCol, CRect rect, 
								LPHDITEM lphdi, 
								CBitmap* pBitmap, 
								BITMAP* pBitmapInfo, 
								BOOL bRight)
{
	UNUSED_ALWAYS(lphdi);

	int iWidth = 0;

	if (pBitmap)
	{
		iWidth = pBitmapInfo->bmWidth;
		if (iWidth <= rect.Width() && rect.Width() > 0)
		{
			POINT point = {0};

			// JCW_U 2008.04.12 START
			//point.y = rect.CenterPoint().y - (pBitmapInfo->bmHeight >> 1);
			point.y = rect.bottom - pBitmapInfo->bmHeight - 10;
			// JCW_U 2008.04.12 END

			if (bRight)
				point.x = rect.right - iWidth;
			else
				point.x = rect.left;

			CDC dc;
			if (dc.CreateCompatibleDC(pDC) == TRUE) 
			{
				VERIFY(dc.SelectObject(pBitmap));
				iWidth = pDC->BitBlt(
					point.x, point.y, 
					pBitmapInfo->bmWidth, pBitmapInfo->bmHeight, 
					&dc, 
					0, 0, 
					SRCCOPY
				) ? iWidth:0;
			}
			else 
				iWidth = 0;
		}
		else
			iWidth = 0;
	}

	return iWidth;
}

///////////////////////////////////////////////////////////////////////////////
// DrawText
int CUTBHeaderCtrl::DrawText(CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi)
{
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(m_crBtnText);

	CSize size;
	if (rect.Width() > 0 && lphdi->mask & HDI_TEXT && lphdi->fmt & HDF_STRING)
	{
		size = pDC->GetTextExtent(lphdi->pszText);

		// always center column headers
		pDC->DrawText(lphdi->pszText, -1, rect
					, DT_CENTER
					| DT_END_ELLIPSIS
					| DT_VCENTER
					| DT_SINGLELINE
					);
	}

	size.cx = rect.Width()>size.cx ? size.cx:rect.Width();
	return size.cx>0 ? size.cx:0;
}

int CUTBHeaderCtrl::DrawCalendar(CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi)
{
	CTime tmDrawDate = GetColInfo(nCol)->m_tmStartDate;

	CRect rcWeekDay(0, m_nFontHeight  +1, DAY_PIX, m_nFontHeight*2+3);
	CRect rcDay    (0, m_nFontHeight*2+2, DAY_PIX, m_nFontHeight*3+4);

	rcWeekDay.MoveToX(rect.left);
	rcDay    .MoveToX(rect.left);

	while(tmDrawDate <= GetColInfo(nCol)->m_tmEndDate)
	{
		CString strWeekDay = tmDrawDate.Format(_T("%a"));
		CString strDay     = tmDrawDate.Format(_T("%#d"));

		if(tmDrawDate.GetDayOfWeek() == 1)
		{
			pDC->SetTextColor(RGB(255,50,50));
		}
		else if(tmDrawDate.GetDayOfWeek() == 7)
		{
			pDC->SetTextColor(RGB(50,50,255));
		}
		else
		{
			pDC->SetTextColor(RGB(104,104,104));
		}

		pDC->DrawText(strDay, -1, rcDay, DT_CENTER | DT_END_ELLIPSIS | DT_VCENTER | DT_SINGLELINE);
		pDC->Draw3dRect(rcDay, m_cr3DHighLight, m_cr3DShadow);

		pDC->DrawText(strWeekDay, -1, rcWeekDay, DT_CENTER | DT_END_ELLIPSIS | DT_VCENTER | DT_SINGLELINE);
		pDC->Draw3dRect(rcWeekDay, m_cr3DHighLight, m_cr3DShadow);

		rcWeekDay.MoveToX(rcWeekDay.left + DAY_PIX);
		rcDay    .MoveToX(rcDay    .left + DAY_PIX);

		tmDrawDate += CTimeSpan(1, 0, 0, 0);
	}

	pDC->SetTextColor(m_crBtnText);

	int nLeft = rect.left;
	tmDrawDate = GetColInfo(nCol)->m_tmStartDate;
	while(tmDrawDate <= GetColInfo(nCol)->m_tmEndDate)
	{
		int nMonthDays = GetMonthDays(tmDrawDate.GetYear(), tmDrawDate.GetMonth());
		int nStartDay  = (tmDrawDate.GetMonth() == GetColInfo(nCol)->m_tmStartDate.GetMonth() ? GetColInfo(nCol)->m_tmStartDate.GetDay() : 1);
		int nEndDay    = (tmDrawDate.GetMonth() == GetColInfo(nCol)->m_tmEndDate.GetMonth() ? GetColInfo(nCol)->m_tmEndDate.GetDay() : nMonthDays);
		int nDays      = (nEndDay-nStartDay+1);

		CRect rcMonth = CRect(0, 0, DAY_PIX*nDays, m_nFontHeight+2);
		rcMonth.MoveToX(nLeft);

		CString strMonthName = tmDrawDate.Format(_T("%#m"));
		if(pDC->GetTextExtent(_T("%Y/%#m (%B)")).cx+m_iSpacing < rcMonth.Width())
		{
			strMonthName = tmDrawDate.Format(_T("%Y/%#m (%B)"));
		}
		else if(pDC->GetTextExtent(_T("%#m (%B)")).cx+m_iSpacing < rcMonth.Width())
		{
			strMonthName = tmDrawDate.Format(_T("%#m (%B)"));
		}

		pDC->DrawText(strMonthName, -1, rcMonth, DT_CENTER | DT_END_ELLIPSIS | DT_VCENTER | DT_SINGLELINE);
		pDC->Draw3dRect(rcMonth, m_cr3DHighLight, m_cr3DShadow);

		nLeft = rcMonth.right;

		tmDrawDate += CTimeSpan(nMonthDays - tmDrawDate.GetDay() + 1
								, 0, 0, 0);
	}

	return 0;
}

int CUTBHeaderCtrl::DrawIndCalendar(CDC* pDC, int nCol, CRect rect, LPHDITEM lphdi)
{
	DrawCalendar(pDC, nCol, rect, lphdi);

	CTime tmDrawDate = GetColInfo(nCol)->m_tmStartDate;

	CRect rcInd(0, m_nFontHeight*3+3, DAY_PIX, m_nFontHeight*4+5);
	rcInd.MoveToX(rect.left);

	while(tmDrawDate <= GetColInfo(nCol)->m_tmEndDate)
	{
		pDC->FillSolidRect(rcInd, RGB(255,255,255));
		pDC->Draw3dRect(rcInd, m_cr3DShadow, m_cr3DHighLight);
		if(GetColInfo(nCol)->m_tmSelDate == tmDrawDate)
		{
			CRect rcBar = rcInd;
			rcBar.top -= 2;
			rcBar.DeflateRect(10, 4, 10, 0);
			pDC->FillSolidRect(rcBar, RGB(255,0,0));
		}
		rcInd.MoveToX(rcInd.left + DAY_PIX);
		tmDrawDate += CTimeSpan(1, 0, 0, 0);
	}

	return 0;
}

int	CUTBHeaderCtrl::GetMonthDays(int nYear, int nMonth)
{
	CTime tmBase(nYear, nMonth, 1, 12, 0, 0);
	CTime tmNextMonth = tmBase + CTimeSpan(31, 0, 0, 0);

	CString str1 = tmBase.Format(_T("%Y/%m/%d %H:%M:%S"));
	CString str2 = tmNextMonth.Format(_T("%Y/%m/%d %H:%M:%S"));

	int nDiff = tmNextMonth.GetDay() - tmBase.GetDay();
	return 31 - nDiff;
}

CUTBHeaderCtrl::HDR_COL_TYPE CUTBHeaderCtrl::GetColType(int nCol)
{
	P_HDR_COL_INFO pHdrColInfo = GetColInfo(nCol);
	if( pHdrColInfo == NULL ) return COL_NORMAL;

	return pHdrColInfo->m_nColType;
}

void CUTBHeaderCtrl::SetColType(int nCol, HDR_COL_TYPE nColType)
{
	CTime tmCurrentTime = CTime::GetCurrentTime();
	tmCurrentTime = CTime(tmCurrentTime.GetYear(), tmCurrentTime.GetMonth(), tmCurrentTime.GetDay(), 0, 0, 0);
	SetColType(nCol, nColType, tmCurrentTime, 3, tmCurrentTime);
}

void CUTBHeaderCtrl::SetColType(int nCol, HDR_COL_TYPE nColType, CTime tmStartDate, int nAddMonth)
{
	SetColType(nCol, nColType, tmStartDate, nAddMonth, tmStartDate);
}

void CUTBHeaderCtrl::SetColType(int nCol, HDR_COL_TYPE nColType, CTime tmStartDate, CTime tmEndDate)
{
	SetColType(nCol, nColType, tmStartDate, tmEndDate, tmStartDate);
}

void CUTBHeaderCtrl::SetColType(int nCol, HDR_COL_TYPE nColType, CTime tmStartDate, int nAddMonth, CTime tmSelDate)
{
	CTime tmCur = CTime::GetCurrentTime();

	int nTotalMonth = tmStartDate.GetYear() * 12 + tmStartDate.GetMonth() + nAddMonth;

	CTime tmEndDate  = CTime( nTotalMonth/12 - ((nTotalMonth%12) == 0 ? 1 : 0)
							, ((nTotalMonth%12) == 0 ? 12 : (nTotalMonth%12))
							, tmStartDate.GetDay()
							, tmStartDate.GetHour()
							, tmStartDate.GetMinute()
							, tmStartDate.GetSecond()
							);
	tmEndDate -= CTimeSpan(1, 0, 0, 0);

	SetColType(nCol, nColType, tmStartDate, tmEndDate, tmSelDate);
}

void CUTBHeaderCtrl::SetColType(int nCol, HDR_COL_TYPE nColType, CTime tmStartDate, CTime tmEndDate, CTime tmSelDate)
{
	TCHAR szText[1024] = {0};

	HDITEM hditem = {0};
	hditem.mask = HDI_WIDTH | HDI_LPARAM | HDI_TEXT;
	hditem.pszText = szText;
	hditem.cchTextMax = sizeof(szText);
	VERIFY(GetItem(nCol, &hditem));

	P_HDR_COL_INFO pHdrColInfo = (P_HDR_COL_INFO)hditem.lParam;
	if( pHdrColInfo == NULL )
	{
		pHdrColInfo = new ST_HDR_COL_INFO;
		hditem.lParam = (LPARAM)pHdrColInfo;
	}

	switch( nColType )
	{
	case COL_CALENDAR:
		{
			pHdrColInfo->m_nColType = nColType;
			if(tmStartDate > tmEndDate)
			{
				pHdrColInfo->m_tmStartDate = tmEndDate;
				pHdrColInfo->m_tmEndDate   = tmStartDate;
			}
			else
			{
				pHdrColInfo->m_tmStartDate = tmStartDate;
				pHdrColInfo->m_tmEndDate   = tmEndDate;
			}

			CTimeSpan tmDiff = pHdrColInfo->m_tmEndDate - pHdrColInfo->m_tmStartDate;
			hditem.cxy = (tmDiff.GetDays() + 1) * DAY_PIX;
		}
		break;

	case COL_INDICATOR_CALENDAR:
		{
			pHdrColInfo->m_nColType = nColType;
			if(tmStartDate > tmEndDate)
			{
				pHdrColInfo->m_tmStartDate = tmEndDate;
				pHdrColInfo->m_tmEndDate   = tmStartDate;
			}
			else
			{
				pHdrColInfo->m_tmStartDate = tmStartDate;
				pHdrColInfo->m_tmEndDate   = tmEndDate;
			}

			pHdrColInfo->m_tmSelDate = tmSelDate;

			CTimeSpan tmDiff = pHdrColInfo->m_tmEndDate - pHdrColInfo->m_tmStartDate;
			hditem.cxy = (tmDiff.GetDays() + 1) * DAY_PIX;
		}
		break;

	default:
		{
			if( nColType != pHdrColInfo->m_nColType     &&
			   (pHdrColInfo->m_nColType == COL_CALENDAR || pHdrColInfo->m_nColType == COL_INDICATOR_CALENDAR))
			{
				CDC* pDC=GetDC();
				int nOldDC=pDC->SaveDC();

				hditem.cxy = pDC->GetTextExtent(hditem.pszText).cx + m_iSpacing*2;

				pDC->RestoreDC(nOldDC);
				ReleaseDC(pDC);
			}

			pHdrColInfo->m_nColType = nColType;
		}
		break;
	}

	SetItem(nCol, &hditem);
}


CUTBHeaderCtrl::P_HDR_COL_INFO CUTBHeaderCtrl::GetColInfo(int nCol)
{
//	TCHAR szText[1024] = {0};

	HDITEM hditem = {0};
	hditem.mask = HDI_LPARAM;
//	hditem.pszText = szText;
//	hditem.cchTextMax = sizeof(szText);
	VERIFY(GetItem(nCol, &hditem));

	P_HDR_COL_INFO pHdrColInfo = (P_HDR_COL_INFO)hditem.lParam;
	if( pHdrColInfo == NULL )
	{
		pHdrColInfo = new ST_HDR_COL_INFO;
		hditem.lParam = (LPARAM)pHdrColInfo;
		SetItem(nCol, &hditem);
	}

	return pHdrColInfo;
}
