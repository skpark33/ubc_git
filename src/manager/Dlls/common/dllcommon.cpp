#include "stdafx.h"
#include "dllcommon.h"

bool hasDoubleByte(const char* ptr)
{
	while(*ptr){
		if(IsDBCSLeadByte(*ptr)){
			return true;
		}
		ptr++;
	}
	return false;
}