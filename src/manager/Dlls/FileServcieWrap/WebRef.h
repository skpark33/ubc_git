//
// sproxy.exe generated file
// do not modify this file
//
// Created: 06/12/2015@22:25:31
//

#pragma once


#if !defined(_WIN32_WINDOWS) && !defined(_WIN32_WINNT) && !defined(_WIN32_WCE)
#pragma message("warning: defining _WIN32_WINDOWS = 0x0410")
#define _WIN32_WINDOWS 0x0410
#endif

#include <atlsoap.h>

namespace FileService
{

struct FileResponse
{
	bool result;
	BSTR errorMsg;
	BSTR resultType;
	BSTR resultData;
	ATLSOAP_BLOB resultBytes;
};

template <typename TClient = CSoapSocketClientT<> >
class CFileServiceT : 
	public TClient, 
	public CSoapRootHandler
{
protected:

	const _soapmap ** GetFunctionMap();
	const _soapmap ** GetHeaderMap();
	void * GetHeaderValue();
	const wchar_t * GetNamespaceUri();
	const char * GetServiceName();
	const char * GetNamespaceUriA();
	HRESULT CallFunction(
		void *pvParam, 
		const wchar_t *wszLocalName, int cchLocalName,
		size_t nItem);
	HRESULT GetClientReader(ISAXXMLReader **ppReader);

public:

	HRESULT __stdcall QueryInterface(REFIID riid, void **ppv)
	{
		if (ppv == NULL)
		{
			return E_POINTER;
		}

		*ppv = NULL;

		if (InlineIsEqualGUID(riid, IID_IUnknown) ||
			InlineIsEqualGUID(riid, IID_ISAXContentHandler))
		{
			*ppv = static_cast<ISAXContentHandler *>(this);
			return S_OK;
		}

		return E_NOINTERFACE;
	}

	ULONG __stdcall AddRef()
	{
		return 1;
	}

	ULONG __stdcall Release()
	{
		return 1;
	}

	CFileServiceT(ISAXXMLReader *pReader = NULL)
		:TClient(_T("https://ubcdev2.sqisoft.com:8080/UBC_HTTP/FileService.asmx"))
	{
		SetClient(true);
		SetReader(pReader);
	}
	
	~CFileServiceT()
	{
		Uninitialize();
	}
	
	void Uninitialize()
	{
		UninitializeSOAP();
	}	

	HRESULT FileExist(
		BSTR id, 
		BSTR pw, 
		BSTR fileName, 
		FileResponse* fileResponse
	);

	HRESULT DeleteDirectory(
		BSTR id, 
		BSTR pw, 
		BSTR srcPath, 
		FileResponse* fileResponse
	);

	HRESULT Test(
		BSTR* TestResult
	);

	HRESULT FilesInfo(
		BSTR id, 
		BSTR pw, 
		BSTR fileList, 
		FileResponse* fileResponse
	);

	HRESULT CopyDirectory(
		BSTR id, 
		BSTR pw, 
		BSTR srcPath, 
		BSTR dstPath, 
		bool overwrite, 
		FileResponse* fileResponse
	);

	HRESULT Login(
		BSTR id, 
		BSTR pw, 
		FileResponse* fileResponse
	);

	HRESULT GetFile(
		BSTR id, 
		BSTR pw, 
		BSTR file, 
		__int64 offset, 
		int size, 
		FileResponse* fileResponse
	);

	HRESULT Logout(
	);

	HRESULT FileInfo(
		BSTR id, 
		BSTR pw, 
		BSTR fileName, 
		FileResponse* fileResponse
	);

	HRESULT FileList(
		BSTR id, 
		BSTR pw, 
		BSTR path, 
		bool topDirectoryOnly, 
		FileResponse* fileResponse
	);

	HRESULT CopyFile(
		BSTR id, 
		BSTR pw, 
		BSTR srcFile, 
		BSTR dstFile, 
		bool overwrite, 
		FileResponse* fileResponse
	);

	HRESULT MoveFile(
		BSTR id, 
		BSTR pw, 
		BSTR srcFile, 
		BSTR dstFile, 
		FileResponse* fileResponse
	);

	HRESULT PutFile(
		BSTR id, 
		BSTR pw, 
		BSTR file, 
		__int64 offset, 
		int size, 
		ATLSOAP_BLOB context, 
		FileResponse* fileResponse
	);

	HRESULT MoveDirectory(
		BSTR id, 
		BSTR pw, 
		BSTR srcPath, 
		BSTR dstPath, 
		FileResponse* fileResponse
	);

	HRESULT DeleteFile(
		BSTR id, 
		BSTR pw, 
		BSTR srcFile, 
		FileResponse* fileResponse
	);
};

typedef CFileServiceT<> CFileService;

__if_not_exists(__FileResponse_entries)
{
extern __declspec(selectany) const _soapmapentry __FileResponse_entries[] =
{
	{ 
		0x1150D23F, 
		"result", 
		L"result", 
		sizeof("result")-1, 
		SOAPTYPE_BOOLEAN, 
		SOAPFLAG_FIELD, 
		offsetof(FileResponse, result),
		NULL, 
		NULL, 
		-1 
	},
	{ 
		0x8F6C9C51, 
		"errorMsg", 
		L"errorMsg", 
		sizeof("errorMsg")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_FIELD | SOAPFLAG_NULLABLE, 
		offsetof(FileResponse, errorMsg),
		NULL, 
		NULL, 
		-1 
	},
	{ 
		0x160DFB21, 
		"resultType", 
		L"resultType", 
		sizeof("resultType")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_FIELD | SOAPFLAG_NULLABLE, 
		offsetof(FileResponse, resultType),
		NULL, 
		NULL, 
		-1 
	},
	{ 
		0x1604CF79, 
		"resultData", 
		L"resultData", 
		sizeof("resultData")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_FIELD | SOAPFLAG_NULLABLE, 
		offsetof(FileResponse, resultData),
		NULL, 
		NULL, 
		-1 
	},
	{ 
		0xD687B7A6, 
		"resultBytes", 
		L"resultBytes", 
		sizeof("resultBytes")-1, 
		SOAPTYPE_BASE64BINARY, 
		SOAPFLAG_FIELD | SOAPFLAG_NULLABLE, 
		offsetof(FileResponse, resultBytes),
		NULL, 
		NULL, 
		-1 
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __FileResponse_map =
{
	0xDF5A91AF,
	"FileResponse",
	L"FileResponse",
	sizeof("FileResponse")-1,
	sizeof("FileResponse")-1,
	SOAPMAP_STRUCT,
	__FileResponse_entries,
	sizeof(FileResponse),
	5,
	-1,
	SOAPFLAG_NONE,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};
}

struct __CFileService_FileExist_struct
{
	BSTR id;
	BSTR pw;
	BSTR fileName;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_FileExist_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FileExist_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FileExist_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xB81AFC41, 
		"fileName", 
		L"fileName", 
		sizeof("fileName")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FileExist_struct, fileName),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_FileExist_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_FileExist_map =
{
	0x2EEE54DC,
	"FileExist",
	L"FileExistResponse",
	sizeof("FileExist")-1,
	sizeof("FileExistResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_FileExist_entries,
	sizeof(__CFileService_FileExist_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_DeleteDirectory_struct
{
	BSTR id;
	BSTR pw;
	BSTR srcPath;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_DeleteDirectory_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_DeleteDirectory_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_DeleteDirectory_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xA5826695, 
		"srcPath", 
		L"srcPath", 
		sizeof("srcPath")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_DeleteDirectory_struct, srcPath),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_DeleteDirectory_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_DeleteDirectory_map =
{
	0x03A51257,
	"DeleteDirectory",
	L"DeleteDirectoryResponse",
	sizeof("DeleteDirectory")-1,
	sizeof("DeleteDirectoryResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_DeleteDirectory_entries,
	sizeof(__CFileService_DeleteDirectory_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_Test_struct
{
	BSTR TestResult;
};

extern __declspec(selectany) const _soapmapentry __CFileService_Test_entries[] =
{

	{
		0x12321ADF, 
		"TestResult", 
		L"TestResult", 
		sizeof("TestResult")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_Test_struct, TestResult),
		NULL,
		NULL,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_Test_map =
{
	0x66CB6EEF,
	"Test",
	L"TestResponse",
	sizeof("Test")-1,
	sizeof("TestResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_Test_entries,
	sizeof(__CFileService_Test_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_FilesInfo_struct
{
	BSTR id;
	BSTR pw;
	BSTR fileList;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_FilesInfo_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FilesInfo_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FilesInfo_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xB81A065C, 
		"fileList", 
		L"fileList", 
		sizeof("fileList")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FilesInfo_struct, fileList),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_FilesInfo_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_FilesInfo_map =
{
	0x952BDBCE,
	"FilesInfo",
	L"FilesInfoResponse",
	sizeof("FilesInfo")-1,
	sizeof("FilesInfoResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_FilesInfo_entries,
	sizeof(__CFileService_FilesInfo_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_CopyDirectory_struct
{
	BSTR id;
	BSTR pw;
	BSTR srcPath;
	BSTR dstPath;
	bool overwrite;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_CopyDirectory_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_CopyDirectory_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_CopyDirectory_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xA5826695, 
		"srcPath", 
		L"srcPath", 
		sizeof("srcPath")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_CopyDirectory_struct, srcPath),
		NULL,
		NULL,
		-1,
	},
	{
		0x2661A078, 
		"dstPath", 
		L"dstPath", 
		sizeof("dstPath")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_CopyDirectory_struct, dstPath),
		NULL,
		NULL,
		-1,
	},
	{
		0xD99CE427, 
		"overwrite", 
		L"overwrite", 
		sizeof("overwrite")-1, 
		SOAPTYPE_BOOLEAN, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_CopyDirectory_struct, overwrite),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_CopyDirectory_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_CopyDirectory_map =
{
	0x6C32383F,
	"CopyDirectory",
	L"CopyDirectoryResponse",
	sizeof("CopyDirectory")-1,
	sizeof("CopyDirectoryResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_CopyDirectory_entries,
	sizeof(__CFileService_CopyDirectory_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_Login_struct
{
	BSTR id;
	BSTR pw;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_Login_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_Login_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_Login_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_Login_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_Login_map =
{
	0x481233A8,
	"Login",
	L"LoginResponse",
	sizeof("Login")-1,
	sizeof("LoginResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_Login_entries,
	sizeof(__CFileService_Login_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_GetFile_struct
{
	BSTR id;
	BSTR pw;
	BSTR file;
	__int64 offset;
	int size;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_GetFile_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_GetFile_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_GetFile_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0x0039BBA0, 
		"file", 
		L"file", 
		sizeof("file")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_GetFile_struct, file),
		NULL,
		NULL,
		-1,
	},
	{
		0x0A5C4687, 
		"offset", 
		L"offset", 
		sizeof("offset")-1, 
		SOAPTYPE_LONG, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_GetFile_struct, offset),
		NULL,
		NULL,
		-1,
	},
	{
		0x0040DE5B, 
		"size", 
		L"size", 
		sizeof("size")-1, 
		SOAPTYPE_INT, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_GetFile_struct, size),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_GetFile_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_GetFile_map =
{
	0x7F910D2F,
	"GetFile",
	L"GetFileResponse",
	sizeof("GetFile")-1,
	sizeof("GetFileResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_GetFile_entries,
	sizeof(__CFileService_GetFile_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_Logout_struct
{
};

extern __declspec(selectany) const _soapmapentry __CFileService_Logout_entries[] =
{

	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_Logout_map =
{
	0x47D5CEA9,
	"Logout",
	L"LogoutResponse",
	sizeof("Logout")-1,
	sizeof("LogoutResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_Logout_entries,
	sizeof(__CFileService_Logout_struct),
	0,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_FileInfo_struct
{
	BSTR id;
	BSTR pw;
	BSTR fileName;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_FileInfo_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FileInfo_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FileInfo_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xB81AFC41, 
		"fileName", 
		L"fileName", 
		sizeof("fileName")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FileInfo_struct, fileName),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_FileInfo_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_FileInfo_map =
{
	0x200CA6DB,
	"FileInfo",
	L"FileInfoResponse",
	sizeof("FileInfo")-1,
	sizeof("FileInfoResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_FileInfo_entries,
	sizeof(__CFileService_FileInfo_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_FileList_struct
{
	BSTR id;
	BSTR pw;
	BSTR path;
	bool topDirectoryOnly;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_FileList_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FileList_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FileList_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0x003F166D, 
		"path", 
		L"path", 
		sizeof("path")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_FileList_struct, path),
		NULL,
		NULL,
		-1,
	},
	{
		0xF1C6250A, 
		"topDirectoryOnly", 
		L"topDirectoryOnly", 
		sizeof("topDirectoryOnly")-1, 
		SOAPTYPE_BOOLEAN, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_FileList_struct, topDirectoryOnly),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_FileList_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_FileList_map =
{
	0x930EC86B,
	"FileList",
	L"FileListResponse",
	sizeof("FileList")-1,
	sizeof("FileListResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_FileList_entries,
	sizeof(__CFileService_FileList_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_CopyFile_struct
{
	BSTR id;
	BSTR pw;
	BSTR srcFile;
	BSTR dstFile;
	bool overwrite;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_CopyFile_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_CopyFile_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_CopyFile_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xA57D0BC8, 
		"srcFile", 
		L"srcFile", 
		sizeof("srcFile")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_CopyFile_struct, srcFile),
		NULL,
		NULL,
		-1,
	},
	{
		0x265C45AB, 
		"dstFile", 
		L"dstFile", 
		sizeof("dstFile")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_CopyFile_struct, dstFile),
		NULL,
		NULL,
		-1,
	},
	{
		0xD99CE427, 
		"overwrite", 
		L"overwrite", 
		sizeof("overwrite")-1, 
		SOAPTYPE_BOOLEAN, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_CopyFile_struct, overwrite),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_CopyFile_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_CopyFile_map =
{
	0x4D40C6AA,
	"CopyFile",
	L"CopyFileResponse",
	sizeof("CopyFile")-1,
	sizeof("CopyFileResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_CopyFile_entries,
	sizeof(__CFileService_CopyFile_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_MoveFile_struct
{
	BSTR id;
	BSTR pw;
	BSTR srcFile;
	BSTR dstFile;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_MoveFile_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_MoveFile_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_MoveFile_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xA57D0BC8, 
		"srcFile", 
		L"srcFile", 
		sizeof("srcFile")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_MoveFile_struct, srcFile),
		NULL,
		NULL,
		-1,
	},
	{
		0x265C45AB, 
		"dstFile", 
		L"dstFile", 
		sizeof("dstFile")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_MoveFile_struct, dstFile),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_MoveFile_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_MoveFile_map =
{
	0xF768DD26,
	"MoveFile",
	L"MoveFileResponse",
	sizeof("MoveFile")-1,
	sizeof("MoveFileResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_MoveFile_entries,
	sizeof(__CFileService_MoveFile_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_PutFile_struct
{
	BSTR id;
	BSTR pw;
	BSTR file;
	__int64 offset;
	int size;
	ATLSOAP_BLOB context;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_PutFile_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_PutFile_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_PutFile_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0x0039BBA0, 
		"file", 
		L"file", 
		sizeof("file")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_PutFile_struct, file),
		NULL,
		NULL,
		-1,
	},
	{
		0x0A5C4687, 
		"offset", 
		L"offset", 
		sizeof("offset")-1, 
		SOAPTYPE_LONG, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_PutFile_struct, offset),
		NULL,
		NULL,
		-1,
	},
	{
		0x0040DE5B, 
		"size", 
		L"size", 
		sizeof("size")-1, 
		SOAPTYPE_INT, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_PutFile_struct, size),
		NULL,
		NULL,
		-1,
	},
	{
		0xCFB9FD65, 
		"context", 
		L"context", 
		sizeof("context")-1, 
		SOAPTYPE_BASE64BINARY, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_PutFile_struct, context),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_PutFile_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_PutFile_map =
{
	0xF1178308,
	"PutFile",
	L"PutFileResponse",
	sizeof("PutFile")-1,
	sizeof("PutFileResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_PutFile_entries,
	sizeof(__CFileService_PutFile_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_MoveDirectory_struct
{
	BSTR id;
	BSTR pw;
	BSTR srcPath;
	BSTR dstPath;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_MoveDirectory_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_MoveDirectory_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_MoveDirectory_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xA5826695, 
		"srcPath", 
		L"srcPath", 
		sizeof("srcPath")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_MoveDirectory_struct, srcPath),
		NULL,
		NULL,
		-1,
	},
	{
		0x2661A078, 
		"dstPath", 
		L"dstPath", 
		sizeof("dstPath")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_MoveDirectory_struct, dstPath),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_MoveDirectory_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_MoveDirectory_map =
{
	0x2217BC3B,
	"MoveDirectory",
	L"MoveDirectoryResponse",
	sizeof("MoveDirectory")-1,
	sizeof("MoveDirectoryResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_MoveDirectory_entries,
	sizeof(__CFileService_MoveDirectory_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};


struct __CFileService_DeleteFile_struct
{
	BSTR id;
	BSTR pw;
	BSTR srcFile;
	FileResponse fileResponse;
};

extern __declspec(selectany) const _soapmapentry __CFileService_DeleteFile_entries[] =
{

	{
		0x00000DED, 
		"id", 
		L"id", 
		sizeof("id")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_DeleteFile_struct, id),
		NULL,
		NULL,
		-1,
	},
	{
		0x00000EE7, 
		"pw", 
		L"pw", 
		sizeof("pw")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_DeleteFile_struct, pw),
		NULL,
		NULL,
		-1,
	},
	{
		0xA57D0BC8, 
		"srcFile", 
		L"srcFile", 
		sizeof("srcFile")-1, 
		SOAPTYPE_STRING, 
		SOAPFLAG_NONE | SOAPFLAG_IN | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL | SOAPFLAG_NULLABLE,
		offsetof(__CFileService_DeleteFile_struct, srcFile),
		NULL,
		NULL,
		-1,
	},
	{
		0xFDC63DCF, 
		"fileResponse", 
		L"fileResponse", 
		sizeof("fileResponse")-1, 
		SOAPTYPE_UNK, 
		SOAPFLAG_NONE | SOAPFLAG_OUT | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		offsetof(__CFileService_DeleteFile_struct, fileResponse),
		NULL,
		&__FileResponse_map,
		-1,
	},
	{ 0x00000000 }
};

extern __declspec(selectany) const _soapmap __CFileService_DeleteFile_map =
{
	0xC864F1C2,
	"DeleteFile",
	L"DeleteFileResponse",
	sizeof("DeleteFile")-1,
	sizeof("DeleteFileResponse")-1,
	SOAPMAP_FUNC,
	__CFileService_DeleteFile_entries,
	sizeof(__CFileService_DeleteFile_struct),
	1,
	-1,
	SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
	0x13F1EDFD,
	"http://sqisoft.com/",
	L"http://sqisoft.com/",
	sizeof("http://sqisoft.com/")-1
};

extern __declspec(selectany) const _soapmap * __CFileService_funcs[] =
{
	&__CFileService_FileExist_map,
	&__CFileService_DeleteDirectory_map,
	&__CFileService_Test_map,
	&__CFileService_FilesInfo_map,
	&__CFileService_CopyDirectory_map,
	&__CFileService_Login_map,
	&__CFileService_GetFile_map,
	&__CFileService_Logout_map,
	&__CFileService_FileInfo_map,
	&__CFileService_FileList_map,
	&__CFileService_CopyFile_map,
	&__CFileService_MoveFile_map,
	&__CFileService_PutFile_map,
	&__CFileService_MoveDirectory_map,
	&__CFileService_DeleteFile_map,
	NULL
};

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::FileExist(
		BSTR id, 
		BSTR pw, 
		BSTR fileName, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_FileExist_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.fileName = fileName;

	__atlsoap_hr = SetClientStruct(&__params, 0);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/FileExist\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::DeleteDirectory(
		BSTR id, 
		BSTR pw, 
		BSTR srcPath, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_DeleteDirectory_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.srcPath = srcPath;

	__atlsoap_hr = SetClientStruct(&__params, 1);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/DeleteDirectory\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::Test(
		BSTR* TestResult
	)
{
    if ( TestResult == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_Test_struct __params;
	memset(&__params, 0x00, sizeof(__params));

	__atlsoap_hr = SetClientStruct(&__params, 2);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/Test\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*TestResult = __params.TestResult;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::FilesInfo(
		BSTR id, 
		BSTR pw, 
		BSTR fileList, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_FilesInfo_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.fileList = fileList;

	__atlsoap_hr = SetClientStruct(&__params, 3);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/FilesInfo\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::CopyDirectory(
		BSTR id, 
		BSTR pw, 
		BSTR srcPath, 
		BSTR dstPath, 
		bool overwrite, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_CopyDirectory_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.srcPath = srcPath;
	__params.dstPath = dstPath;
	__params.overwrite = overwrite;

	__atlsoap_hr = SetClientStruct(&__params, 4);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/CopyDirectory\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::Login(
		BSTR id, 
		BSTR pw, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_Login_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;

	__atlsoap_hr = SetClientStruct(&__params, 5);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/Login\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::GetFile(
		BSTR id, 
		BSTR pw, 
		BSTR file, 
		__int64 offset, 
		int size, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_GetFile_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.file = file;
	__params.offset = offset;
	__params.size = size;

	__atlsoap_hr = SetClientStruct(&__params, 6);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/GetFile\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::Logout(
	)
{

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_Logout_struct __params;
	memset(&__params, 0x00, sizeof(__params));

	__atlsoap_hr = SetClientStruct(&__params, 7);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/Logout\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::FileInfo(
		BSTR id, 
		BSTR pw, 
		BSTR fileName, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_FileInfo_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.fileName = fileName;

	__atlsoap_hr = SetClientStruct(&__params, 8);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/FileInfo\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::FileList(
		BSTR id, 
		BSTR pw, 
		BSTR path, 
		bool topDirectoryOnly, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_FileList_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.path = path;
	__params.topDirectoryOnly = topDirectoryOnly;

	__atlsoap_hr = SetClientStruct(&__params, 9);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/FileList\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::CopyFile(
		BSTR id, 
		BSTR pw, 
		BSTR srcFile, 
		BSTR dstFile, 
		bool overwrite, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_CopyFile_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.srcFile = srcFile;
	__params.dstFile = dstFile;
	__params.overwrite = overwrite;

	__atlsoap_hr = SetClientStruct(&__params, 10);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/CopyFile\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::MoveFile(
		BSTR id, 
		BSTR pw, 
		BSTR srcFile, 
		BSTR dstFile, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_MoveFile_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.srcFile = srcFile;
	__params.dstFile = dstFile;

	__atlsoap_hr = SetClientStruct(&__params, 11);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/MoveFile\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::PutFile(
		BSTR id, 
		BSTR pw, 
		BSTR file, 
		__int64 offset, 
		int size, 
		ATLSOAP_BLOB context, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_PutFile_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.file = file;
	__params.offset = offset;
	__params.size = size;
	__params.context = context;

	__atlsoap_hr = SetClientStruct(&__params, 12);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/PutFile\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::MoveDirectory(
		BSTR id, 
		BSTR pw, 
		BSTR srcPath, 
		BSTR dstPath, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_MoveDirectory_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.srcPath = srcPath;
	__params.dstPath = dstPath;

	__atlsoap_hr = SetClientStruct(&__params, 13);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/MoveDirectory\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
inline HRESULT CFileServiceT<TClient>::DeleteFile(
		BSTR id, 
		BSTR pw, 
		BSTR srcFile, 
		FileResponse* fileResponse
	)
{
    if ( fileResponse == NULL )
		return E_POINTER;

	HRESULT __atlsoap_hr = InitializeSOAP(NULL);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_INITIALIZE_ERROR);
		return __atlsoap_hr;
	}
	
	CleanupClient();

	CComPtr<IStream> __atlsoap_spReadStream;
	__CFileService_DeleteFile_struct __params;
	memset(&__params, 0x00, sizeof(__params));
	__params.id = id;
	__params.pw = pw;
	__params.srcFile = srcFile;

	__atlsoap_hr = SetClientStruct(&__params, 14);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_OUTOFMEMORY);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = GenerateResponse(GetWriteStream());
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_GENERATE_ERROR);
		goto __skip_cleanup;
	}
	
	__atlsoap_hr = SendRequest(_T("SOAPAction: \"http://sqisoft.com/DeleteFile\"\r\n"));
	if (FAILED(__atlsoap_hr))
	{
		goto __skip_cleanup;
	}
	__atlsoap_hr = GetReadStream(&__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_READ_ERROR);
		goto __skip_cleanup;
	}
	
	// cleanup any in/out-params and out-headers from previous calls
	Cleanup();
	__atlsoap_hr = BeginParse(__atlsoap_spReadStream);
	if (FAILED(__atlsoap_hr))
	{
		SetClientError(SOAPCLIENT_PARSE_ERROR);
		goto __cleanup;
	}

	*fileResponse = __params.fileResponse;
	goto __skip_cleanup;
	
__cleanup:
	Cleanup();
__skip_cleanup:
	ResetClientState(true);
	memset(&__params, 0x00, sizeof(__params));
	return __atlsoap_hr;
}

template <typename TClient>
ATL_NOINLINE inline const _soapmap ** CFileServiceT<TClient>::GetFunctionMap()
{
	return __CFileService_funcs;
}

template <typename TClient>
ATL_NOINLINE inline const _soapmap ** CFileServiceT<TClient>::GetHeaderMap()
{
	static const _soapmapentry __CFileService_FileExist_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_FileExist_atlsoapheader_map = 
	{
		0x2EEE54DC,
		"FileExist",
		L"FileExistResponse",
		sizeof("FileExist")-1,
		sizeof("FileExistResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_FileExist_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_DeleteDirectory_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_DeleteDirectory_atlsoapheader_map = 
	{
		0x03A51257,
		"DeleteDirectory",
		L"DeleteDirectoryResponse",
		sizeof("DeleteDirectory")-1,
		sizeof("DeleteDirectoryResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_DeleteDirectory_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_Test_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_Test_atlsoapheader_map = 
	{
		0x66CB6EEF,
		"Test",
		L"TestResponse",
		sizeof("Test")-1,
		sizeof("TestResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_Test_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_FilesInfo_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_FilesInfo_atlsoapheader_map = 
	{
		0x952BDBCE,
		"FilesInfo",
		L"FilesInfoResponse",
		sizeof("FilesInfo")-1,
		sizeof("FilesInfoResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_FilesInfo_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_CopyDirectory_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_CopyDirectory_atlsoapheader_map = 
	{
		0x6C32383F,
		"CopyDirectory",
		L"CopyDirectoryResponse",
		sizeof("CopyDirectory")-1,
		sizeof("CopyDirectoryResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_CopyDirectory_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_Login_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_Login_atlsoapheader_map = 
	{
		0x481233A8,
		"Login",
		L"LoginResponse",
		sizeof("Login")-1,
		sizeof("LoginResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_Login_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_GetFile_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_GetFile_atlsoapheader_map = 
	{
		0x7F910D2F,
		"GetFile",
		L"GetFileResponse",
		sizeof("GetFile")-1,
		sizeof("GetFileResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_GetFile_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_Logout_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_Logout_atlsoapheader_map = 
	{
		0x47D5CEA9,
		"Logout",
		L"LogoutResponse",
		sizeof("Logout")-1,
		sizeof("LogoutResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_Logout_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_FileInfo_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_FileInfo_atlsoapheader_map = 
	{
		0x200CA6DB,
		"FileInfo",
		L"FileInfoResponse",
		sizeof("FileInfo")-1,
		sizeof("FileInfoResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_FileInfo_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_FileList_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_FileList_atlsoapheader_map = 
	{
		0x930EC86B,
		"FileList",
		L"FileListResponse",
		sizeof("FileList")-1,
		sizeof("FileListResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_FileList_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_CopyFile_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_CopyFile_atlsoapheader_map = 
	{
		0x4D40C6AA,
		"CopyFile",
		L"CopyFileResponse",
		sizeof("CopyFile")-1,
		sizeof("CopyFileResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_CopyFile_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_MoveFile_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_MoveFile_atlsoapheader_map = 
	{
		0xF768DD26,
		"MoveFile",
		L"MoveFileResponse",
		sizeof("MoveFile")-1,
		sizeof("MoveFileResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_MoveFile_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_PutFile_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_PutFile_atlsoapheader_map = 
	{
		0xF1178308,
		"PutFile",
		L"PutFileResponse",
		sizeof("PutFile")-1,
		sizeof("PutFileResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_PutFile_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_MoveDirectory_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_MoveDirectory_atlsoapheader_map = 
	{
		0x2217BC3B,
		"MoveDirectory",
		L"MoveDirectoryResponse",
		sizeof("MoveDirectory")-1,
		sizeof("MoveDirectoryResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_MoveDirectory_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};

	static const _soapmapentry __CFileService_DeleteFile_atlsoapheader_entries[] =
	{
		{ 0x00000000 }
	};

	static const _soapmap __CFileService_DeleteFile_atlsoapheader_map = 
	{
		0xC864F1C2,
		"DeleteFile",
		L"DeleteFileResponse",
		sizeof("DeleteFile")-1,
		sizeof("DeleteFileResponse")-1,
		SOAPMAP_HEADER,
		__CFileService_DeleteFile_atlsoapheader_entries,
		0,
		0,
		-1,
		SOAPFLAG_NONE | SOAPFLAG_PID | SOAPFLAG_DOCUMENT | SOAPFLAG_LITERAL,
		0x13F1EDFD,
		"http://sqisoft.com/",
		L"http://sqisoft.com/",
		sizeof("http://sqisoft.com/")-1
	};


	static const _soapmap * __CFileService_headers[] =
	{
		&__CFileService_FileExist_atlsoapheader_map,
		&__CFileService_DeleteDirectory_atlsoapheader_map,
		&__CFileService_Test_atlsoapheader_map,
		&__CFileService_FilesInfo_atlsoapheader_map,
		&__CFileService_CopyDirectory_atlsoapheader_map,
		&__CFileService_Login_atlsoapheader_map,
		&__CFileService_GetFile_atlsoapheader_map,
		&__CFileService_Logout_atlsoapheader_map,
		&__CFileService_FileInfo_atlsoapheader_map,
		&__CFileService_FileList_atlsoapheader_map,
		&__CFileService_CopyFile_atlsoapheader_map,
		&__CFileService_MoveFile_atlsoapheader_map,
		&__CFileService_PutFile_atlsoapheader_map,
		&__CFileService_MoveDirectory_atlsoapheader_map,
		&__CFileService_DeleteFile_atlsoapheader_map,
		NULL
	};
	
	return __CFileService_headers;
}

template <typename TClient>
ATL_NOINLINE inline void * CFileServiceT<TClient>::GetHeaderValue()
{
	return this;
}

template <typename TClient>
ATL_NOINLINE inline const wchar_t * CFileServiceT<TClient>::GetNamespaceUri()
{
	return L"http://sqisoft.com/";
}

template <typename TClient>
ATL_NOINLINE inline const char * CFileServiceT<TClient>::GetServiceName()
{
	return NULL;
}

template <typename TClient>
ATL_NOINLINE inline const char * CFileServiceT<TClient>::GetNamespaceUriA()
{
	return "http://sqisoft.com/";
}

template <typename TClient>
ATL_NOINLINE inline HRESULT CFileServiceT<TClient>::CallFunction(
	void *, 
	const wchar_t *, int,
	size_t)
{
	return E_NOTIMPL;
}

template <typename TClient>
ATL_NOINLINE inline HRESULT CFileServiceT<TClient>::GetClientReader(ISAXXMLReader **ppReader)
{
	if (ppReader == NULL)
	{
		return E_INVALIDARG;
	}
	
	CComPtr<ISAXXMLReader> spReader = GetReader();
	if (spReader.p != NULL)
	{
		*ppReader = spReader.Detach();
		return S_OK;
	}
	return TClient::GetClientReader(ppReader);
}

} // namespace FileService

template<>
inline HRESULT AtlCleanupValue<FileService::FileResponse>(FileService::FileResponse *pVal)
{
	pVal;
	AtlCleanupValue(&pVal->result);
	AtlCleanupValue(&pVal->errorMsg);
	AtlCleanupValue(&pVal->resultType);
	AtlCleanupValue(&pVal->resultData);
	AtlCleanupValue(&pVal->resultBytes);
	return S_OK;
}

template<>
inline HRESULT AtlCleanupValueEx<FileService::FileResponse>(FileService::FileResponse *pVal, IAtlMemMgr *pMemMgr)
{
	pVal;
	pMemMgr;
	
	AtlCleanupValueEx(&pVal->result, pMemMgr);
	AtlCleanupValueEx(&pVal->errorMsg, pMemMgr);
	AtlCleanupValueEx(&pVal->resultType, pMemMgr);
	AtlCleanupValueEx(&pVal->resultData, pMemMgr);
	AtlCleanupValueEx(&pVal->resultBytes, pMemMgr);
	return S_OK;
}
