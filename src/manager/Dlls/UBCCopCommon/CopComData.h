#pragma once

#include "copmodule.h"

class CCopComData
{
public:
	static CCopComData* GetObject();
	static void Release();

	HostRevInfoList m_lsHosRev;
	PackageRevInfoList m_lsPackageRev;
private:
	CCopComData();
	~CCopComData();

	static CCopComData* m_pObject;
};