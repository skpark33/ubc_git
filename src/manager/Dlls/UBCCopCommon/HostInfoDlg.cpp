// CHostInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "HostInfoDlg.h"

// CHostInfoDlg dialog
IMPLEMENT_DYNAMIC(CHostInfoDlg, CDialog)

CHostInfoDlg::CHostInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHostInfoDlg::IDD, pParent)
{
	m_szPM.Empty();
	m_szSite.Empty();
	m_szHost.Empty();
	m_szIP.Empty();
	m_szMac.Empty();
	m_szOpState.Empty();
	m_szVendor.Empty();
	m_szModel.Empty();
	m_szOS.Empty();
	m_szSerialNo.Empty();
	m_szPeriod.Empty();
	m_szEdition.Empty();
	m_szDescription.Empty();
	m_szAuto1.Empty();
	m_szAuto2.Empty();
	m_szCurrent1.Empty();
	m_szCurrent2.Empty();
	m_szLast1.Empty();
	m_szLast2.Empty();
	m_szNetwork1.Empty();
	m_szNetwork2.Empty();
}

CHostInfoDlg::~CHostInfoDlg()
{
}

void CHostInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PMID, m_szPM);
	DDX_Text(pDX, IDC_EDIT_SITEID, m_szSite);
	DDX_Text(pDX, IDC_EDIT_HOST, m_szHost);
	DDX_Text(pDX, IDC_EDIT_IP, m_szIP);
	DDX_Text(pDX, IDC_EDIT_MAC, m_szMac);
	DDX_Text(pDX, IDC_EDIT_OPSTATE, m_szOpState);

	DDX_Control(pDX, IDC_COMBO_ADMINSTATE, m_cbAdState);
	DDX_Control(pDX, IDC_TIME_AUTHDATE, m_dtcAuthDate);
	DDX_Control(pDX, IDC_TIME_UPDATEDATE, m_dtcUpDate);
	DDX_Control(pDX, IDC_COMBO_DISPLAYCNT, m_cbDisplayCnt);
	
	DDX_Text(pDX, IDC_EDIT_VENDOR, m_szVendor);
	DDX_Text(pDX, IDC_EDIT_MODEL, m_szModel);
	DDX_Text(pDX, IDC_EDIT_OS, m_szOS);
	DDX_Text(pDX, IDC_EDIT_SERIALNO, m_szSerialNo);
	DDX_Text(pDX, IDC_EDIT_PERIOD, m_szPeriod);
	DDX_Text(pDX, IDC_EDIT_EDITION, m_szEdition);
	DDX_Text(pDX, IDC_EDIT_DESCRIPT, m_szDescription);
	DDX_Text(pDX, IDC_EDIT_AUTO1, m_szAuto1);
	DDX_Text(pDX, IDC_EDIT_AUTO2, m_szAuto2);
	DDX_Text(pDX, IDC_EDIT_CURRENT1, m_szCurrent1);
	DDX_Text(pDX, IDC_EDIT_CURRENT2, m_szCurrent2);
	DDX_Text(pDX, IDC_EDIT_LAST1, m_szLast1);
	DDX_Text(pDX, IDC_EDIT_LAST2, m_szLast2);
	DDX_Text(pDX, IDC_EDIT_NETWORK1, m_szNetwork1);
	DDX_Text(pDX, IDC_EDIT_NETWORK2, m_szNetwork2);
}

BEGIN_MESSAGE_MAP(CHostInfoDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CHostInfoDlg::OnBnClickedOk)
END_MESSAGE_MAP()

// CHostInfoDlg message handlers
BOOL CHostInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_szPM = m_HostInfo.mgrId;
	m_szSite = m_HostInfo.siteId;
	m_szHost = m_HostInfo.hostId;
	m_szIP = m_HostInfo.ipAddress;
	m_szMac = m_HostInfo.macAddress;
	
	m_szOpState = m_HostInfo.operationalState?STR_OPERA_ON:STR_OPERA_OFF;
	
	m_dtcAuthDate.SetFormat("yyyy/MM/dd HH:mm:ss");
	m_dtcUpDate.SetFormat("yyyy/MM/dd HH:mm:ss");

	if(!m_HostInfo.hostId.IsEmpty()){
		m_dtcAuthDate.SetTime(&m_HostInfo.authDate);
		m_dtcUpDate.SetTime(&m_HostInfo.lastUpdateTime);
	}

	m_cbAdState.AddString(STR_ADMIN_OFF);
	m_cbAdState.AddString(STR_ADMIN_ON);

	m_cbDisplayCnt.AddString("1");
	m_cbDisplayCnt.AddString("2");
	m_cbDisplayCnt.SetCurSel(m_HostInfo.displayCounter-1);
	
	m_szVendor = m_HostInfo.vendor;
	m_szMonitorType = m_HostInfo.monitorType;
	m_szModel = m_HostInfo.model;
	m_szOS = m_HostInfo.os;
	m_szSerialNo.Format(_T("%d"), m_HostInfo.serialNo);
	m_szPeriod = ToString(m_HostInfo.period);

	m_cbAdState.SetCurSel(m_HostInfo.adminState?1:0);
	
	m_szEdition = m_HostInfo.edition;
	m_szDescription = m_HostInfo.description;

	m_szAuto1 = m_HostInfo.autoPackage1;
	m_szAuto2 = m_HostInfo.autoPackage2;
	m_szCurrent1 = m_HostInfo.currentPackage1;
	m_szCurrent2 = m_HostInfo.currentPackage2;
	m_szLast1 = m_HostInfo.lastPackage1;
	m_szLast2 = m_HostInfo.lastPackage2;
	m_szNetwork1 = m_HostInfo.networkUse1?STR_NET_TRUE:STR_NET_FALSE;
	m_szNetwork2 = m_HostInfo.networkUse2?STR_NET_TRUE:STR_NET_FALSE;

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CHostInfoDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CHostInfoDlg::SetInfo(const SHostInfo& info)
{
	m_HostInfo = info;
}

void CHostInfoDlg::GetInfo(SHostInfo& info)
{
	info = m_HostInfo;
}
void CHostInfoDlg::OnBnClickedOk()
{
	UpdateData();

	m_HostInfo.mgrId = m_szPM;
	m_HostInfo.siteId = m_szSite;
	m_HostInfo.hostId = m_szHost;
	m_HostInfo.ipAddress = m_szIP;
	m_HostInfo.macAddress = m_szMac;
//	m_HostInfo.operationalState = m_szOpState;
	
//	m_dtcAuthDate.GetTime(m_HostInfo.authDate);
//	m_dtcUpDate.GetTime(m_HostInfo.lastUpdateDate);

	m_HostInfo.vendor = m_szVendor;
	m_HostInfo.monitorType = m_szMonitorType;
	m_HostInfo.model = m_szModel;
	m_HostInfo.os = m_szOS;
	m_HostInfo.serialNo = _ttoi(m_szSerialNo);
	m_HostInfo.period = m_szPeriod.IsEmpty()?0:atoi(m_szPeriod);
	m_HostInfo.adminState = m_cbAdState.GetCurSel();
	m_HostInfo.edition = m_szEdition;

	m_HostInfo.displayCounter = m_cbDisplayCnt.GetCurSel()+1;

	m_HostInfo.description = m_szDescription;
	m_HostInfo.autoPackage1 = m_szAuto1;
	m_HostInfo.autoPackage2 = m_szAuto2;
	m_HostInfo.currentPackage1 = m_szCurrent1;
	m_HostInfo.currentPackage2 = m_szCurrent2;
	m_HostInfo.lastPackage1 = m_szLast1;
	m_HostInfo.lastPackage2 = m_szLast2;
//	m_HostInfo.networkUse1 = m_szNetwork1;
//	m_HostInfo.networkUse2 = m_szNetwork2;

	OnOK();
}
