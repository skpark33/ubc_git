// PackageSelectDlg.cpp : 구현 파일입니다.
//
#include "stdafx.h"
#include "CopModule.h"
#include "resource.h"
#include "PackageSelectDlg.h"

#include "common\UbcCode.h"
#include "ContentsSelectDlg.h"
#include "SelectUsers.h"
#include "common/TraceLog.h"


// CPackageSelectDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPackageSelectDlg, CDialog)

CPackageSelectDlg::CPackageSelectDlg(LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CPackageSelectDlg::IDD, pParent)
	, m_strCustomer ( szCustomer )
	, m_bSingleSelect ( false )
	, m_repos ( this )
	, m_nMovingSplitType ( 0 )
	, m_bInitialize ( false )
	, m_downloadOnly (0)
{
	//스플릿바가 마우스 클릭을 하면 위아래로 300pt 가 움직이도록 한다.
	m_stcSplit.SetMouseAction(CSplitStatic::eMOUSE_CLICK); //skpark 2013.3.28
	m_stcSplit.SetVerticalEnd(130); //skpark 2013.3.28
}

CPackageSelectDlg::~CPackageSelectDlg()
{
}

void CPackageSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_stcGroup);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_btnRefresh);
	DDX_Control(pDX, IDC_EDIT_PROGRAM_ID, m_editProgramId);
	DDX_Control(pDX, IDC_EDIT_DESCRIPTION, m_editDescription);
	DDX_Control(pDX, IDC_EDIT_LAST_UPDATE_ID, m_editLastUpdateId);
	DDX_Control(pDX, IDC_COMBO_INCLUDED_CONTENTS, m_cbxIncludedContents);
	DDX_Control(pDX, IDC_DATE_START, m_dtcStart);
	DDX_Control(pDX, IDC_DATE_END, m_dtcEnd);
	DDX_Control(pDX, IDC_LIST_PUBLIC, m_lcPublic);
	DDX_Control(pDX, IDC_LIST_VERIFY, m_lcVerify);
	DDX_Control(pDX, IDC_LIST_CONTENTS_CATEGORY, m_lcContentsCategory);
	DDX_Control(pDX, IDC_LIST_PURPOSE, m_lcPurpose);
	DDX_Control(pDX, IDC_LIST_MODEL, m_lcModel);
	DDX_Control(pDX, IDC_LIST_DIRECTION, m_lcDirection);
	DDX_Control(pDX, IDC_LIST_RESOLUTION, m_lcResolution);
	DDX_Control(pDX, IDC_STATIC_SPLIT, m_stcSplit);
	DDX_Control(pDX, IDC_LIST_PACKAGE, m_lcPackage);
	DDX_Control(pDX, IDOK, m_btnSelect);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BN_USER, m_btnUser);
	DDX_Control(pDX, IDC_EDIT_SELECTED_PACKAGE, m_editSelectedPackage);
	DDX_Control(pDX, IDC_LIST_HOST, m_lscHost);
	DDX_Control(pDX, IDC_STATIC_HOST, m_stcHost);
	DDX_Control(pDX, IDC_STATIC_SELECTED_PACKAGE, m_stcSelected);
	DDX_Control(pDX, IDC_CHECK_DOWNLOADONLY, m_checkDownloadOnly);
	DDX_Control(pDX, IDC_UNCHECK_DOWNLOADONLY, m_uncheckDownloadOnly);
}


BEGIN_MESSAGE_MAP(CPackageSelectDlg, CDialog)
	ON_WM_GETMINMAXINFO()
	ON_WM_SETCURSOR()
	ON_WM_SIZE()
	ON_NOTIFY(NM_CLICK, IDC_LIST_PUBLIC, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PUBLIC, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_PUBLIC, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_PUBLIC, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_VERIFY, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_VERIFY, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_VERIFY, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_VERIFY, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_CONTENTS_CATEGORY, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONTENTS_CATEGORY, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CONTENTS_CATEGORY, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_CONTENTS_CATEGORY, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_PURPOSE, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PURPOSE, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_PURPOSE, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_PURPOSE, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_MODEL, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_MODEL, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_MODEL, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_MODEL, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_DIRECTION, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_DIRECTION, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_DIRECTION, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_DIRECTION, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_RESOLUTION, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RESOLUTION, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_RESOLUTION, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_RESOLUTION, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_PACKAGE, OnNMClickPackageList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PACKAGE, OnNMClickPackageList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_PACKAGE, OnNMRClickPackageList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_PACKAGE, OnNMRClickPackageList)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnBnClickedButtonRefresh)
	ON_CBN_SELCHANGE(IDC_COMBO_INCLUDED_CONTENTS, OnCbnSelchangeComboIncludedContents)
	ON_MESSAGE(WM_MOVE_SPLITER, OnMoveSpliter)
	ON_MESSAGE(WM_LBTDOWN_SPLITER, OnLButtonDownSpliter) //skpark 2013.3.27
	ON_BN_CLICKED(IDOK, &CPackageSelectDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPackageSelectDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_USER, &CPackageSelectDlg::OnBnClickedBnUser)
	ON_BN_CLICKED(IDC_CHECK_DOWNLOADONLY, &CPackageSelectDlg::OnBnClickedCheckDownloadonly)
	ON_BN_CLICKED(IDC_UNCHECK_DOWNLOADONLY, &CPackageSelectDlg::OnBnClickedUncheckDownloadonly)
END_MESSAGE_MAP()


// CPackageSelectDlg 메시지 처리기입니다.


#define		FILTER_HEIGHT				150
#define		PACKAGE_LIST_HEIGHT			65
#define		EXT_HEIGHT			140


BOOL CPackageSelectDlg::OnInitDialog()
{
	CWaitMessageBox wait;

	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	GetWindowRect(m_rectSizeMin);

	//
	InitControl();

	// WIA 에서만 보여준다.
	if(m_strCustomer == "WIA"){
		m_checkDownloadOnly.ShowWindow(SW_SHOW);
		m_uncheckDownloadOnly.ShowWindow(SW_SHOW);
	}
	
	//skpark 2013.3.29
	LoadFilterData();
	if(isHostMerged()){
		InitHostList();
		InsertHostData();
	}
	//skpark end

	InitData();
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

//skpark 2013.3.29
bool CPackageSelectDlg::GetSelectedPackage(SPackageInfo& info)
{
	POSITION pos = m_listSelectedPackage.GetHeadPosition();
	if(pos)
	{
		info =  m_listSelectedPackage.GetNext(pos);
		return true;
	}
	return false;
}

void CPackageSelectDlg::OnOK()
{
	m_listSelectedPackage.RemoveAll();

	int count = m_lcPackage.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if(m_lcPackage.GetCheck(i))
		{
			SPackageInfo* info = (SPackageInfo*)m_lcPackage.GetItemData(i);
			if(info)
			{
				m_listSelectedPackage.AddTail(*info);
			}
		}
	}

	if(m_listSelectedPackage.GetCount() <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_PACKAGESELELCTDLG_MSG001));
		return;
	}








	//skpark 2013.3.29
	if(isHostMerged()){
		SPackageInfo stPackageInfo;
		if(!this->GetSelectedPackage(stPackageInfo)){
			UbcMessageBox(LoadStringById(IDS_PACKAGESELELCTDLG_MSG001));
			return;
		}

		if(this->m_strCustomer == "KIA" || this->m_strCustomer == "HYUNDAI" || this->m_strCustomer == "HYUNDAIDP") {
			if(!isTested(stPackageInfo)) {
				return ;
			}
		}

		std::list<std::string> lsHost;
		for(int nRow = 0; nRow < this->m_arHostList.GetCount(); nRow++)
		{
			SHostInfo HostInfo = this->m_arHostList.GetAt(nRow);

			std::string strHost;
			strHost = HostInfo.hostId;
			strHost+= HostInfo.displayCounter==1?":A":":B";

			if(this->m_downloadOnly > 0){
				CCopModule::GetObject()->DownloadOnlyProperty((LPCTSTR)HostInfo.siteId, (LPCTSTR)HostInfo.hostId, m_downloadOnly);
			}

			lsHost.push_back(strHost);
		}

		CWaitMessageBox wait;
		CCopModule::GetObject()->ApplyPackage(	(LPCTSTR)m_loginId, 
												(LPCTSTR)stPackageInfo.szSiteID, 
												(LPCTSTR)stPackageInfo.szPackage, _T(""), _T(""), lsHost);
	}
	// skpark end

	ClearData();
	CDialog::OnOK();
}

void CPackageSelectDlg::OnCancel()
{
	m_listSelectedPackage.RemoveAll();

	ClearData();

	CDialog::OnCancel();
}

void CPackageSelectDlg::InitControl()
{
	//skpark 2013.3.29 
	if(isHostMerged()){
		this->SetWindowTextA(LoadStringById(IDC_PACKAGE_SELECT_TITLE));

		m_btnSelect.LoadBitmap(IDB_BUTTON_APPLY, RGB(255,255,255));
		m_btnCancel.LoadBitmap(IDB_BUTTON_CANCEL, RGB(255,255,255));

		m_editSelectedPackage.ShowWindow(true);
		m_stcHost.ShowWindow(true);
		m_stcSelected.ShowWindow(true);
		m_lscHost.ShowWindow(true);

	}else{
		m_btnSelect.LoadBitmap(IDB_BUTTON_SELECT, RGB(255,255,255));
		m_btnCancel.LoadBitmap(IDB_BUTTON_CANCEL, RGB(255,255,255));

		CRect rect;
		this->GetWindowRect(rect);
		this->SetWindowPos(NULL,0,0,rect.Width(),rect.Height()-EXT_HEIGHT,SWP_NOMOVE | SWP_NOZORDER);

		m_editSelectedPackage.ShowWindow(false);
		m_stcHost.ShowWindow(false);
		m_stcSelected.ShowWindow(false);
		m_lscHost.ShowWindow(false);

	}
	// skpark end



	m_repos.AddControl((CWnd*)&m_stcGroup, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_btnRefresh, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editProgramId, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editDescription, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editLastUpdateId, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_btnUser, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_cbxIncludedContents, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_dtcStart, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_dtcEnd, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcPublic, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcVerify, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcContentsCategory, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcPurpose, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcModel, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcDirection, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcResolution, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_stcSplit, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	//skpark
	if(isHostMerged()){
		m_repos.AddControl((CWnd*)&m_lcPackage, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
		m_repos.AddControl((CWnd*)&m_btnSelect, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
		m_repos.AddControl((CWnd*)&m_btnCancel, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
		m_repos.AddControl((CWnd*)&m_lscHost, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
		m_repos.AddControl((CWnd*)&m_stcHost, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
		m_repos.AddControl((CWnd*)&m_stcSelected, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
		m_repos.AddControl((CWnd*)&m_editSelectedPackage, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	}else{
		m_repos.AddControl((CWnd*)&m_lcPackage, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
		m_repos.AddControl((CWnd*)&m_btnSelect, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
		m_repos.AddControl((CWnd*)&m_btnCancel, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
		m_repos.AddControl((CWnd*)&m_lscHost, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	}

	m_btnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));

	m_cbxIncludedContents.InsertString(0, LoadStringById(IDS_COPCOMMON_STR001));
	m_cbxIncludedContents.InsertString(1, LoadStringById(IDS_COPCOMMON_STR002));
	m_cbxIncludedContents.SetCurSel(0);

	CTime start_time(2011, 4, 1, 0, 0, 0);
	m_dtcStart.SetTime(&start_time);

	CRect rect;
	m_lcPublic.GetClientRect(rect);
	m_lcPublic.SetExtendedStyle(m_lcPublic.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcPublic.SetSelTextColor(RGB(0,0,0));
	m_lcPublic.SetSelBackColor(RGB(220,220,220));
	m_lcPublic.InsertColumn(0, LoadStringById(IDS_CONTENTSSELECTDLG_LST011), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcPublic.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcVerify.GetClientRect(rect);
	m_lcVerify.SetExtendedStyle(m_lcVerify.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcVerify.SetSelTextColor(RGB(0,0,0));
	m_lcVerify.SetSelBackColor(RGB(220,220,220));
	m_lcVerify.InsertColumn(0, LoadStringById(IDS_CONTENTSSELECTDLG_LST012), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcVerify.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcContentsCategory.GetClientRect(rect);
	m_lcContentsCategory.SetExtendedStyle(m_lcContentsCategory.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcContentsCategory.SetSelTextColor(RGB(0,0,0));
	m_lcContentsCategory.SetSelBackColor(RGB(220,220,220));
	m_lcContentsCategory.InsertColumn(0, LoadStringById(IDS_CONTENTSSELECTDLG_LST013), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcContentsCategory.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcPurpose.GetClientRect(rect);
	m_lcPurpose.SetExtendedStyle(m_lcPurpose.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcPurpose.SetSelTextColor(RGB(0,0,0));
	m_lcPurpose.SetSelBackColor(RGB(220,220,220));
	m_lcPurpose.InsertColumn(0, LoadStringById(IDS_CONTENTSSELECTDLG_LST014), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcPurpose.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcModel.GetClientRect(rect);
	m_lcModel.SetExtendedStyle(m_lcModel.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcModel.SetSelTextColor(RGB(0,0,0));
	m_lcModel.SetSelBackColor(RGB(220,220,220));
	m_lcModel.InsertColumn(0, LoadStringById(IDS_CONTENTSSELECTDLG_LST015), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcModel.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcDirection.GetClientRect(rect);
	m_lcDirection.SetExtendedStyle(m_lcDirection.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcDirection.SetSelTextColor(RGB(0,0,0));
	m_lcDirection.SetSelBackColor(RGB(220,220,220));
	m_lcDirection.InsertColumn(0, LoadStringById(IDS_CONTENTSSELECTDLG_LST016), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcDirection.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcResolution.GetClientRect(rect);
	m_lcResolution.SetExtendedStyle(m_lcResolution.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcResolution.SetSelTextColor(RGB(0,0,0));
	m_lcResolution.SetSelBackColor(RGB(220,220,220));
	m_lcResolution.InsertColumn(0, LoadStringById(IDS_CONTENTSSELECTDLG_LST017), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcResolution.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcPackage.GetClientRect(rect);
	m_lcPackage.SetExtendedStyle(m_lcPackage.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcPackage.SetSelTextColor(RGB(0,0,0));
	m_lcPackage.SetSelBackColor(RGB(220,220,220));
	m_lcPackage.InsertColumn(eCheck       , "", LVCFMT_LEFT, 22);
	m_lcPackage.InsertColumn(eSite        , LoadStringById(IDS_CONTENTSSELECTDLG_LST001), LVCFMT_LEFT, 100);
	m_lcPackage.InsertColumn(ePackage     , LoadStringById(IDS_CONTENTSSELECTDLG_LST002), LVCFMT_LEFT, 140);
	m_lcPackage.InsertColumn(eUser        , LoadStringById(IDS_CONTENTSSELECTDLG_LST019), LVCFMT_LEFT, 80);
	m_lcPackage.InsertColumn(eTime        , LoadStringById(IDS_CONTENTSSELECTDLG_LST006), LVCFMT_LEFT, 120);
	m_lcPackage.InsertColumn(eVolume      , LoadStringById(IDS_CONTENTSSELECTDLG_LST020), LVCFMT_RIGHT, 70);
	m_lcPackage.InsertColumn(ePublic      , LoadStringById(IDS_CONTENTSSELECTDLG_LST011), LVCFMT_LEFT, 60);
	m_lcPackage.InsertColumn(eVerify      , LoadStringById(IDS_CONTENTSSELECTDLG_LST012), LVCFMT_LEFT, 60);
	m_lcPackage.InsertColumn(eCategory    , LoadStringById(IDS_CONTENTSSELECTDLG_LST013), LVCFMT_LEFT, 100);
	m_lcPackage.InsertColumn(ePurpose     , LoadStringById(IDS_CONTENTSSELECTDLG_LST014), LVCFMT_LEFT, 100);
	m_lcPackage.InsertColumn(eHostType    , LoadStringById(IDS_CONTENTSSELECTDLG_LST015), LVCFMT_LEFT, 100);
	m_lcPackage.InsertColumn(eVertical    , LoadStringById(IDS_CONTENTSSELECTDLG_LST016), LVCFMT_LEFT, 100);
	m_lcPackage.InsertColumn(eResolution  , LoadStringById(IDS_CONTENTSSELECTDLG_LST017), LVCFMT_LEFT, 100);
	m_lcPackage.InsertColumn(eDesc        , LoadStringById(IDS_CONTENTSSELECTDLG_LST018), LVCFMT_LEFT, 120);
	m_lcPackage.InsertColumn(eValidateDate, LoadStringById(IDS_CONTENTSSELECTDLG_LST021), LVCFMT_LEFT, 120);
	m_lcPackage.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);


	//skpark 2013.3.29 
	m_stcSplit.MovePosition(); //skpark 2013.3.28 처음 시작할때 닫은채로 시작한다.

}

//skpark 2013.3.29
void CPackageSelectDlg::InitHostList()
{
	m_lscHost.SetExtendedStyle(m_lscHost.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lscHost.InsertColumn(eHostCheck,		_T(""),										 LVCFMT_LEFT, 22);
	m_lscHost.InsertColumn(eHostSiteID,		LoadStringById(IDS_PACKAGECHANGEDLG_LST001), LVCFMT_LEFT, 100);
	m_lscHost.InsertColumn(eHostHostID,		LoadStringById(IDS_PACKAGECHANGEDLG_LST006), LVCFMT_LEFT, 100);
	m_lscHost.InsertColumn(eHostDisplayNo,	LoadStringById(IDS_PACKAGECHANGEDLG_LST007), LVCFMT_LEFT, 100);
	m_lscHost.InsertColumn(eHostAutoSche,	LoadStringById(IDS_PACKAGECHANGEDLG_LST008), LVCFMT_LEFT, 180);
	m_lscHost.InsertColumn(eHostCurSche,	LoadStringById(IDS_PACKAGECHANGEDLG_LST009), LVCFMT_LEFT, 180);
	//m_lscHost.InsertColumn(eHostCreator,	LoadStringById(IDS_PACKAGECHANGEDLG_LST010), LVCFMT_LEFT, 150);
	m_lscHost.InsertColumn(eHostLastSche,	LoadStringById(IDS_PACKAGECHANGEDLG_LST011), LVCFMT_LEFT, 180);
	
	//m_lscHost.SetColumnWidth(m_lscHost.GetColPos(m_szColHostTitle[eCheck]), 22); // 2010.02.17 by gwangsoo
	//m_lscHost.SetColumnWidth(eSiteID, 100);
	m_lscHost.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

//skpark 2013.3.29
void CPackageSelectDlg::InsertHostData()
{
	m_lscHost.DeleteAllItems();

	for(int nRow = 0; nRow < m_arHostList.GetCount(); nRow++)
	{
		SHostInfo Info = m_arHostList.GetAt(nRow);

		m_lscHost.InsertItem(nRow, "");
		m_lscHost.SetItemText(nRow, eHostSiteID, Info.siteId);
		m_lscHost.SetItemText(nRow, eHostHostID, Info.hostId);

		if(Info.displayCounter==1)
		{
			m_lscHost.SetItemText(nRow, eHostDisplayNo, LoadStringById(IDS_PACKAGECHANGEDLG_STR001));
			m_lscHost.SetItemText(nRow, eHostAutoSche , Info.autoPackage1);
			m_lscHost.SetItemText(nRow, eHostCurSche  , Info.currentPackage1);
			m_lscHost.SetItemText(nRow, eHostLastSche , Info.lastPackage1);
		}
		else
		{
			m_lscHost.SetItemText(nRow, eHostDisplayNo, LoadStringById(IDS_PACKAGECHANGEDLG_STR002));
			m_lscHost.SetItemText(nRow, eHostAutoSche , Info.autoPackage2);
			m_lscHost.SetItemText(nRow, eHostCurSche  , Info.currentPackage2);
			m_lscHost.SetItemText(nRow, eHostLastSche , Info.lastPackage2);
		}

		m_lscHost.SetItemData(nRow, (DWORD_PTR)nRow);
		m_lscHost.SetCheck(nRow);
	}
	m_lscHost.SetCheckHdrState(true);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	//CString szPath;
	//szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	//m_lscHost.LoadColumnState("PACKAGE-CHNG-LIST", szPath);
}



void CPackageSelectDlg::InitData()
{
	//CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcPublic, _T(""));
	m_lcPublic.InsertItem(0, LoadStringById(IDS_CONTENTSSELECTDLG_STR001));
	m_lcPublic.InsertItem(1, LoadStringById(IDS_CONTENTSSELECTDLG_STR002));

	//CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcVerify, _T(""));
	m_lcVerify.InsertItem(0, LoadStringById(IDS_CONTENTSSELECTDLG_STR003));
	m_lcVerify.InsertItem(1, LoadStringById(IDS_CONTENTSSELECTDLG_STR004));

	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcContentsCategory, _T("Kind"));
	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcPurpose, _T("Purpose"));
	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcModel, _T("HostType"));
	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcDirection, _T("Direction"));
	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcResolution, _T("Resolution"));

	CheckScrollBarVisible(m_lcPublic);
	CheckScrollBarVisible(m_lcVerify);
	CheckScrollBarVisible(m_lcContentsCategory);
	CheckScrollBarVisible(m_lcPurpose);
	CheckScrollBarVisible(m_lcModel);
	CheckScrollBarVisible(m_lcDirection);
	CheckScrollBarVisible(m_lcResolution);
}

void CPackageSelectDlg::ClearData()
{
	int count = m_lcPackage.GetItemCount();
	for(int i=0; i<count; i++)
	{
		SPackageInfo* info = (SPackageInfo*)m_lcPackage.GetItemData(i);
		if(info) delete info;
	}
	m_lcPackage.DeleteAllItems();
}

void CPackageSelectDlg::CheckScrollBarVisible(CUTBListCtrlEx& lc)
{
	lc.SetColumnWidth(0, LVSCW_AUTOSIZE);
	int auto_width = lc.GetColumnWidth(0);

	CRect rect;
	lc.GetClientRect(rect);

	CScrollBar* pBar =  lc.GetScrollBarCtrl(SB_VERT);
	int width = (pBar && pBar->IsWindowVisible()) ? rect.Width()-::GetSystemMetrics(SM_CXVSCROLL) : rect.Width();

	if(width < auto_width) width = auto_width;

	lc.SetColumnWidth(0, width);
}

void CPackageSelectDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CDialog::OnGetMinMaxInfo(lpMMI);

	if(m_bInitialize)
	{
		lpMMI->ptMinTrackSize.x = m_rectSizeMin.Width();
		if(isHostMerged()){ //skpark 2013.3.29
			lpMMI->ptMinTrackSize.y = m_rectSizeMin.Height();
		}else{
			lpMMI->ptMinTrackSize.y = m_rectSizeMin.Height() - EXT_HEIGHT;
		}
	}
	else
	{
		m_bInitialize = true;
	}
}

BOOL CPackageSelectDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(pWnd->GetSafeHwnd() == m_stcSplit.GetSafeHwnd())
	{
		m_nMovingSplitType = 1;
		SetCursor(::LoadCursor(NULL, IDC_SIZENS));
		return TRUE;
	}
	else
	{
		m_nMovingSplitType = 0;
		SetCursor(::LoadCursor(NULL, IDC_ARROW));
	}

	BOOL retval = CDialog::OnSetCursor(pWnd, nHitTest, message);
	return retval;
}

void CPackageSelectDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if(GetSafeHwnd())
	{
		m_repos.MoveControl(TRUE);
		Invalidate(TRUE);
	}

	if(m_lcPackage.GetSafeHwnd())
	{
		int move = 0;
		CRect rect;
		m_lcPackage.GetWindowRect(rect);
		if(rect.Height() < PACKAGE_LIST_HEIGHT)
			move = rect.Height() - PACKAGE_LIST_HEIGHT;

		if(move != 0)
		{
			m_repos.Move((CWnd*)&m_stcGroup, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcPublic, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcVerify, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcContentsCategory, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcPurpose, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcModel, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcDirection, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcResolution, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_stcSplit, 0, move, 0, move);
			m_repos.Move((CWnd*)&m_lcPackage, 0, move, 0, 0);

			m_repos.MoveControl(TRUE);

			CheckScrollBarVisible(m_lcPublic);
			CheckScrollBarVisible(m_lcVerify);
			CheckScrollBarVisible(m_lcContentsCategory);
			CheckScrollBarVisible(m_lcPurpose);
			CheckScrollBarVisible(m_lcModel);
			CheckScrollBarVisible(m_lcDirection);
			CheckScrollBarVisible(m_lcResolution);
		}
	}

}

void CPackageSelectDlg::OnBnClickedButtonRefresh()
{
	CWaitMessageBox wait;

	//
	PackageInfoList		package_list;
	CString filter;
	GetFilterString(filter);
	CCopModule::GetObject()->GetPackage(
			"*" /*(GetEnvPtr()->m_Authority==CCopModule::eSiteAdmin)?"*":GetEnvPtr()->m_szSite*/, 
			package_list,
			filter
		);

	ClearData();

	int count = package_list.GetCount();
	POSITION pos = package_list.GetHeadPosition();
	for(int i=0; i<count; i++)
	{
		SPackageInfo& info = package_list.GetNext(pos);

		m_lcPackage.InsertItem(i, "");
		m_lcPackage.SetItemText(i, eSite, info.szSiteID);
		m_lcPackage.SetItemText(i, ePackage, info.szPackage);
		m_lcPackage.SetItemText(i, eUser, info.szUpdateUser);

		CTime tm(info.tmUpdateTime);
		m_lcPackage.SetItemText(i, eTime, tm.Format("%Y/%m/%d %H:%M:%S"));

		m_lcPackage.SetItemText(i, eVolume, (info.volume < 0 ? "" : ToFileSize((ULONGLONG)info.volume, 'G')));

		m_lcPackage.SetItemText(i, ePublic, info.bIsPublic ? "O" : "X");
		m_lcPackage.SetItemText(i, eVerify, info.bIsVerify ? "O" : "X");

		if(info.tmValidationDate > 0)
		{
			CTime tmValidationDate(info.tmValidationDate);
			m_lcPackage.SetItemText(i, eValidateDate, tmValidationDate.Format(STR_DEF_TIME));
		}

		SCodeItem item;

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("Kind", info.nContentsCategory);
		m_lcPackage.SetItemText(i, eCategory, item.strEnumString);

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("Purpose", info.nPurpose);
		m_lcPackage.SetItemText(i, ePurpose, item.strEnumString);

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("HostType", info.nHostType);
		m_lcPackage.SetItemText(i, eHostType, item.strEnumString);

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("Direction", info.nVertical);
		m_lcPackage.SetItemText(i, eVertical, item.strEnumString);

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("Resolution", info.nResolution);
		m_lcPackage.SetItemText(i, eResolution, item.strEnumString);

		m_lcPackage.SetItemText(i, eDesc, info.szDescript);

		SPackageInfo* new_info = new SPackageInfo;
		*new_info = info;
		m_lcPackage.SetItemData(i, (DWORD)new_info);
	}

	SaveFilterData();
}

void CPackageSelectDlg::OnCbnSelchangeComboIncludedContents()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbxIncludedContents.GetCurSel();
	int count = m_cbxIncludedContents.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxIncludedContents.DeleteString(1);
			m_listSelectedContentsId.RemoveAll();
		}
	}
	else if(idx == count-1 )
	{
		CContentsSelectDlg dlg(m_strCustomer);
		dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_listSelectedContentsId.RemoveAll();
				m_cbxIncludedContents.DeleteString(1);
				m_cbxIncludedContents.SetCurSel(0);
			}

			CONTENTS_INFO_EX_LIST& c_ls = dlg.GetSelectedContentsIdList();

			CString str = "";
			int cnt = c_ls.GetCount();
			POSITION pos = c_ls.GetHeadPosition();
			for(int i=0; i<cnt; i++)
			{
				CONTENTS_INFO_EX& info = c_ls.GetNext(pos);

				m_listSelectedContentsId.Add(info.strContentsId);

				if(str.GetLength() > 0) str += ",";
				str += info.strContentsName;
			}

			if(str.GetLength() > 0)
			{
				m_cbxIncludedContents.InsertString(1, str);
				m_cbxIncludedContents.SetCurSel(1);
			}
			else
			{
				m_cbxIncludedContents.SetCurSel(0);
			}
		}
		else
		{
			m_cbxIncludedContents.SetCurSel(count-2);
		}
	}
}

void CPackageSelectDlg::OnNMClickFilterList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;

	int index = pNMItemActivate->iItem;
	if(index < 0) return;

	CWnd* wnd = CWnd::FromHandle(pNMHDR->hwndFrom);
	if(wnd)
	{
		CUTBListCtrlEx* plc = (CUTBListCtrlEx*)wnd;

		//LVHITTESTINFO oInfo ;
		//oInfo.pt = pNMItemActivate->ptAction ;
		//plc->HitTest(&oInfo);

		// Check Box Icon Click
		//if( oInfo.flags == LVHT_ONITEMSTATEICON ) 
		{
			bool all_check = true;
			for(int i=0; i<plc->GetItemCount(); i++)
			{
				//if((i==index && plc->GetCheck(i) != 0) || (i!=index && plc->GetCheck(i) == 0))
				if(plc->GetCheck(i) == 0)
				{
					all_check = false;
					break;
				}
			}
			plc->SetCheckHdrState(all_check ? TRUE : FALSE);

			//*pResult = 1; // 무시하고 싶으면 셋팅
		}
	}
}

void CPackageSelectDlg::OnNMRClickFilterList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 1;
}

void CPackageSelectDlg::OnNMClickPackageList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;

	int index = pNMItemActivate->iItem;
	if(index < 0) return;

	if(m_bSingleSelect)
	{
		CWnd* wnd = CWnd::FromHandle(pNMHDR->hwndFrom);
		if(wnd)
		{
			CUTBListCtrlEx* plc = (CUTBListCtrlEx*)wnd;

			LVHITTESTINFO oInfo ;
			oInfo.pt = pNMItemActivate->ptAction ;
			plc->HitTest(&oInfo);

			// Check Box Icon Click
			//if( oInfo.flags == LVHT_ONITEMSTATEICON ) //skpark 2013.3.29 comment out
			//{
				int count = m_lcPackage.GetItemCount();
				for(int i=0; i<count; i++)
				{
					if(index == i)
					{
						m_lcPackage.SetItemState(i, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
						//skpark 2013.3.29
						m_lcPackage.SetCheck(i, TRUE); //skpark 2013.3.29 add
						if(isHostMerged()){
							SPackageInfo* info = (SPackageInfo*)m_lcPackage.GetItemData(i);
							if(info)
							{
								m_editSelectedPackage.SetWindowText(info->szPackage);
							}
						}
						//skpark end
					}
					else
					{
						m_lcPackage.SetCheck(i, FALSE);
						m_lcPackage.SetItemState(i, NULL, LVIS_SELECTED | LVIS_FOCUSED);
					}
				}
			//}

		}
	}
}

void CPackageSelectDlg::OnNMRClickPackageList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 1;
}

LRESULT CPackageSelectDlg::OnMoveSpliter(WPARAM wParam, LPARAM lParam)
{
	CString msg;

	CRect rect1, rect2;
	m_stcGroup.GetWindowRect(rect1);
	m_lcPackage.GetWindowRect(rect2);

	if(rect1.Height() + lParam < FILTER_HEIGHT)
		lParam = FILTER_HEIGHT - rect1.Height();
	else if(rect2.Height() - lParam < PACKAGE_LIST_HEIGHT)
		lParam = rect2.Height() - PACKAGE_LIST_HEIGHT;

	if(lParam != 0)
	{
		m_repos.Move((CWnd*)&m_stcGroup, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcPublic, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcVerify, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcContentsCategory, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcPurpose, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcModel, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcDirection, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcResolution, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_stcSplit, 0, lParam, 0, lParam);
		m_repos.Move((CWnd*)&m_lcPackage, 0, lParam, 0, 0);

		CheckScrollBarVisible(m_lcPublic);
		CheckScrollBarVisible(m_lcVerify);
		CheckScrollBarVisible(m_lcContentsCategory);
		CheckScrollBarVisible(m_lcPurpose);
		CheckScrollBarVisible(m_lcModel);
		CheckScrollBarVisible(m_lcDirection);
		CheckScrollBarVisible(m_lcResolution);

		m_repos.MoveControl(TRUE);
	}
	return 0;
}

//skpark 2013.3.27
LRESULT CPackageSelectDlg::OnLButtonDownSpliter(WPARAM wParam, LPARAM lParam)
{
	CString msg;

	/*
	CRect rect1, rect2;
	m_stcGroup.GetWindowRect(rect1);
	m_lcPackage.GetWindowRect(rect2);

	if(rect1.Height() + lParam < FILTER_HEIGHT)
		lParam = FILTER_HEIGHT - rect1.Height();
	else if(rect2.Height() - lParam < PACKAGE_LIST_HEIGHT)
		lParam = rect2.Height() - PACKAGE_LIST_HEIGHT;
	*/
	if(lParam != 0)
	{
		m_lastMove = lParam;  // 마지막 이동값을 저장해둔다.

		m_repos.Move((CWnd*)&m_stcGroup, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcPublic, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcVerify, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcContentsCategory, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcPurpose, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcModel, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcDirection, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcResolution, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_stcSplit, 0, lParam, 0, lParam);
		m_repos.Move((CWnd*)&m_lcPackage, 0, lParam, 0, 0);

		CheckScrollBarVisible(m_lcPublic);
		CheckScrollBarVisible(m_lcVerify);
		CheckScrollBarVisible(m_lcContentsCategory);
		CheckScrollBarVisible(m_lcPurpose);
		CheckScrollBarVisible(m_lcModel);
		CheckScrollBarVisible(m_lcDirection);
		CheckScrollBarVisible(m_lcResolution);

		m_repos.MoveControl(TRUE);
	}

	return 0;
}

void CPackageSelectDlg::GetFilterString(CString& strFilter)
{
	CStringArray filter_list;
	CString filter;

	//
	CString programId;
	m_editProgramId.GetWindowText(programId);
	if(programId.GetLength() > 0)
	{
		filter.Format("lower(programId) like ('%%%s%%')", programId.MakeLower());
		filter_list.Add(filter);
	}

	CString description;
	m_editDescription.GetWindowText(description);
	if(description.GetLength() > 0)
	{
		filter.Format("description like ('%%%s%%')", description);
		int pos = 0;
		CString strTag;
		while((strTag = description.Tokenize("/ ", pos)) != _T(""))
		{
			//filter.Format(_T("concat('/', lower(description), '/') like '%%/%s/%%'")
			filter.Format(_T("description like '%%%s%%'")
						 , strTag.MakeLower()
						 );
			filter_list.Add(filter);
		}
	}

	CString lastUpdateId;
	m_editLastUpdateId.GetWindowText(lastUpdateId);
	if(lastUpdateId.GetLength() > 0)
	{
		filter.Format("lastUpdateId like ('%%%s%%')", lastUpdateId);
		filter_list.Add(filter);
	}

	CTimeSpan span(1, 0, 0, 0);
	CTime start_time;
	m_dtcStart.GetTime(start_time);
	CString startTime = start_time.Format("%Y-%m-%d");
	CTime end_time;
	m_dtcEnd.GetTime(end_time);
	end_time += span;
	CString endTime = end_time.Format("%Y-%m-%d");
	{
		filter.Format("lastUpdateTime > CAST('%s' as datetime) and lastUpdateTime < CAST('%s' as datetime)", startTime, endTime);
		filter_list.Add(filter);
	}

	if(!m_lcPublic.GetCheckHdrState())
	{
		// not all
		if(m_lcPublic.GetCheck(0))
		{
			// 공개
			filter_list.Add("isPublic = '1'");
		}
		else if(m_lcPublic.GetCheck(1))
		{
			// 비공개
			filter_list.Add("isPublic = '0'");
		}
	}

	if(!m_lcVerify.GetCheckHdrState())
	{
		// not all
		if(m_lcVerify.GetCheck(0))
		{
			// 공개
			filter_list.Add("isVerify = '1'");
		}
		else if(m_lcVerify.GetCheck(1))
		{
			// 비공개
			filter_list.Add("isVerify = '0'");
		}
	}

	if(!m_lcContentsCategory.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcContentsCategory, "contentsCategory", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	if(!m_lcPurpose.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcPurpose, "purpose", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	if(!m_lcModel.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcModel, "hostType", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	if(!m_lcDirection.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcDirection, "vertical", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	if(!m_lcResolution.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcResolution, "resolution", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	//
	//m_cbxIncludedContents
	if(m_listSelectedContentsId.GetCount() > 0)
	{
		CStringArray program_id_list;
		int count = m_listSelectedContentsId.GetCount();
		for(int i=0; i<count; i++)
		{
			const CString& id = m_listSelectedContentsId.GetAt(i);
			CCopModule::GetObject()->GetProgramIdListIncludeContents( id, program_id_list );
		}

		if(program_id_list.GetCount() > 0)
		{
			GetSelectItemsStringFromList(program_id_list, "programId", filter);
			if(filter.GetLength() > 0) filter_list.Add(filter);
		}
	}

	//
	int count = filter_list.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str = filter_list.GetAt(i);

		if(strFilter.GetLength() > 0)
		{
			strFilter += " and ";
		}

		strFilter += "( ";
		strFilter += str;
		strFilter += ") ";
	}

	//UbcMessageBox(strFilter);
}

void CPackageSelectDlg::GetSelectItemsStringFromList(CUTBListCtrlEx& lc, LPCTSTR lpszField , CString& filter)
{
	filter = "";

	int count = lc.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if(lc.GetCheck(i))
		{
			if(filter.GetLength() > 0) filter += ",";

			CString tmp;
			tmp.Format("'%d'", lc.GetItemData(i));

			filter += tmp;
		}
	}
	if(filter.GetLength() > 0)
	{
		CString tmp;
		tmp.Format("%s in (", lpszField);

		filter.Insert(0, tmp);
		filter += ")";
	}
}

void CPackageSelectDlg::GetSelectItemsStringFromList(CStringArray& ar, LPCTSTR lpszField , CString& filter)
{
	filter = "";

	int count = ar.GetCount();
	for(int i=0; i<count; i++)
	{
		if(filter.GetLength() > 0) filter += ",";

		CString tmp;
		tmp.Format("'%s'", ar.GetAt(i));

		filter += tmp;
	}
	if(filter.GetLength() > 0)
	{
		CString tmp;
		tmp.Format("%s in (", lpszField);

		filter.Insert(0, tmp);
		filter += ")";
	}
}

void CPackageSelectDlg::OnBnClickedOk()
{
	//skpark 2013.3.29
	if(isHostMerged()){
		// 체크가 해지된 host 를 m_arHostList 에서 제거한다.

		CArray<SHostInfo> tempArray;
		tempArray.Copy(m_arHostList);
		m_arHostList.RemoveAll();

		for(int nHostRow=0; nHostRow<m_lscHost.GetItemCount() ;nHostRow++)
		{
			if(!m_lscHost.GetCheck(nHostRow)) {
				continue;
			}
			int nHostIdx = (int)m_lscHost.GetItemData(nHostRow);
			//char buf[10]; sprintf(buf,"%d",nHostIdx);
			//UbcMessageBox(buf);
			if(nHostIdx < 0 || nHostIdx >= tempArray.GetCount()) {
				continue;
			}
			m_arHostList.Add(tempArray.GetAt(nHostIdx));
		}
	}
	// skpark end; 
	OnOK();
}

void CPackageSelectDlg::OnBnClickedCancel()
{
	OnCancel();
}

void CPackageSelectDlg::OnBnClickedBnUser()
{
	CSelectUsers dlg;
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		m_editLastUpdateId.SetWindowText(_T(""));
		return;
	}

	UserInfoList& list = dlg.GetSelectedList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SUserInfo info = list.GetNext(pos);
		m_editLastUpdateId.SetWindowText(info.userId);
	}
}

//skpark 2013.3.29  필터값을 저장한다.
void CPackageSelectDlg::SaveFilterData()
{
	if(m_envIni.IsEmpty()){
		return;
	}
	CString strPackageId    ;
	CString strCategory     ;
	CString strExtFilter	= "0";

	m_editProgramId.GetWindowText(strPackageId);
	m_editDescription.GetWindowText(strCategory);
	if(m_lastMove > 0){
		strExtFilter	= "1";  // 상세필터가 열려있다.
	}

	CTime start_time;
	m_dtcStart.GetTime(start_time);

	CTime end_time;
	m_dtcEnd.GetTime(end_time);


	WritePrivateProfileString("PACKAGE_SELECT-FILTER", "PackageID" , strPackageId    , m_envIni);
	WritePrivateProfileString("PACKAGE_SELECT-FILTER", "Category"  , strCategory     , m_envIni);
	WritePrivateProfileString("PACKAGE_SELECT-FILTER", "ExtendedFilter"  , strExtFilter     , m_envIni);

	WritePrivateProfileString("PACKAGE_SELECT-FILTER", "StartTime_Year"  , ::ToString(start_time.GetYear())   , m_envIni);
	WritePrivateProfileString("PACKAGE_SELECT-FILTER", "StartTime_Month"  , ::ToString(start_time.GetMonth())   , m_envIni);
	WritePrivateProfileString("PACKAGE_SELECT-FILTER", "StartTime_Day"  , ::ToString(start_time.GetDay())   , m_envIni);

	WritePrivateProfileString("PACKAGE_SELECT-FILTER", "EndTime_Year"  , ::ToString(end_time.GetYear())   , m_envIni);
	WritePrivateProfileString("PACKAGE_SELECT-FILTER", "EndTime_Month"  , ::ToString(end_time.GetMonth())   , m_envIni);
	WritePrivateProfileString("PACKAGE_SELECT-FILTER", "EndTime_Day"  , ::ToString(end_time.GetDay())   , m_envIni);

}

void CPackageSelectDlg::LoadFilterData()
{
	if(m_envIni.IsEmpty()){
		return;
	}

	CString strPackageId    ;
	CString strCategory     ;
	CString strExtFilter	= "0";
	CString startTime_year	= "";
	CString startTime_month	= "";
	CString startTime_day	= "";
	CString endTime_year	= "";
	CString endTime_month	= "";
	CString endTime_day	= "";

	char buf[1025]; 
	memset(buf,0x00,1025);
	GetPrivateProfileString("PACKAGE_SELECT-FILTER", "PackageID" , "", buf, 1024, m_envIni); strPackageId    = buf;
	memset(buf,0x00,1025);
	GetPrivateProfileString("PACKAGE_SELECT-FILTER", "Category"         , "", buf, 1024, m_envIni); strCategory     = buf;
	memset(buf,0x00,1025);
	GetPrivateProfileString("PACKAGE_SELECT-FILTER", "ExtendedFilter"   , "", buf, 1024, m_envIni); strExtFilter = buf;

	memset(buf,0x00,1025);
	GetPrivateProfileString("PACKAGE_SELECT-FILTER", "StartTime_Year"   , "", buf, 1024, m_envIni); startTime_year = buf;
	memset(buf,0x00,1025);
	GetPrivateProfileString("PACKAGE_SELECT-FILTER", "StartTime_Month"   , "", buf, 1024, m_envIni); startTime_month = buf;
	memset(buf,0x00,1025);
	GetPrivateProfileString("PACKAGE_SELECT-FILTER", "StartTime_Day"   , "", buf, 1024, m_envIni); startTime_day = buf;

	memset(buf,0x00,1025);
	GetPrivateProfileString("PACKAGE_SELECT-FILTER", "EndTime_Year"   , "", buf, 1024, m_envIni); endTime_year = buf;
	memset(buf,0x00,1025);
	GetPrivateProfileString("PACKAGE_SELECT-FILTER", "EndTime_Month"   , "", buf, 1024, m_envIni); endTime_month = buf;
	memset(buf,0x00,1025);
	GetPrivateProfileString("PACKAGE_SELECT-FILTER", "EndTime_Day"   , "", buf, 1024, m_envIni); endTime_day = buf;

	m_editProgramId.SetWindowText(strPackageId);
	m_editDescription.SetWindowText(strCategory);
	if(strExtFilter	== "1"){  // 상세필터가 열려있다.
		this->m_stcSplit.MovePosition();
	}

	if(!startTime_year.IsEmpty() && !startTime_month.IsEmpty() && !startTime_day.IsEmpty()){
		CTime start_time(atoi(startTime_year), atoi(startTime_month),atoi(startTime_day),0,0,0);
		m_dtcStart.SetTime(&start_time);
	}
// skpark 2014.02.18 EndTime 은 기억하지 않고, 항상 오늘날짜를 넣는다.
//	if(!endTime_year.IsEmpty() && !endTime_month.IsEmpty() && !endTime_day.IsEmpty()){
//		CTime end_time(atoi(endTime_year), atoi(endTime_month),atoi(endTime_day),0,0,0);
//		m_dtcEnd.SetTime(&end_time);
//	}
	time_t now = ::time(NULL);
	CTime end_time(now);
	
	m_dtcEnd.SetTime(&end_time);

}

void CPackageSelectDlg::OnBnClickedCheckDownloadonly()
{
	int check = m_checkDownloadOnly.GetCheck();
	if(check) {
		m_uncheckDownloadOnly.SetCheck(false);
		m_downloadOnly = 1;
	}else{
		int uncheck = m_uncheckDownloadOnly.GetCheck();
		if(!uncheck){
			m_downloadOnly = 0;
		}
	}
}

void CPackageSelectDlg::OnBnClickedUncheckDownloadonly()
{
	int uncheck = m_uncheckDownloadOnly.GetCheck();
	if(uncheck) {
		m_checkDownloadOnly.SetCheck(false);
		m_downloadOnly = 2;
	}else{
		int check = m_checkDownloadOnly.GetCheck();
		if(!check){
			m_downloadOnly = 0;
		}
	}
}

bool CPackageSelectDlg::isTested(SPackageInfo& stPackageInfo)
{
 // 테스트 단말에 테스트 여부 체크 
	time_t now = time(NULL);

	CString strPackage = stPackageInfo.szPackage;

	CString szPath = m_envIni;
	//szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, "UBCManager.ini");
	int targetHostCount = this->m_arHostList.GetCount();

	if(targetHostCount >  1) 
	{
		char lastPackage[256];			memset(lastPackage,0x00,256);
		char lastLoginId[256];			memset(lastLoginId,0x00,256);
		char lastTargetHostCount[11];	memset(lastTargetHostCount,0x00,11);
		char lastExecutionTime[11];		memset(lastExecutionTime,0x00,11);

		GetPrivateProfileString("PACKAGECHG_DLG", "Package", "", lastPackage, 256, szPath);
		GetPrivateProfileString("PACKAGECHG_DLG", "loginId", "", lastLoginId, 256, szPath);
		GetPrivateProfileString("PACKAGECHG_DLG", "TargetHostCount", "", lastTargetHostCount, 11, szPath);
		GetPrivateProfileString("PACKAGECHG_DLG", "ExecutionTime", "", lastExecutionTime, 11,szPath);
	
		if(	atoi(lastTargetHostCount) != 1		||
			((!strPackage.IsEmpty()) && strPackage != lastPackage)	||
			m_loginId != lastLoginId				){

				TraceLog(("lastPackage=%s,%s", lastPackage, strPackage));
				TraceLog(("lastTargetHostCount=%d,%d", atoi(lastTargetHostCount), targetHostCount));
				TraceLog(("lastLoginId=%s,%s", lastLoginId, m_loginId));
				TraceLog(("Customer=%s", m_strCustomer));
				// test 부터 먼저 할것!!!	
				UbcMessageBox(m_testWarningMsg);
				return false;	
		
		}else{  // 다똑같이 일치하고, 1개에 테스트를 해봤다면, 그 때가 언제인지 본다.
			time_t  lastNow = (time_t)strtoul(lastExecutionTime,0,10);
			TraceLog(("lastExecutionTime=%I64d,%I64d, %d", lastNow, now, now-lastNow));
			if(now - lastNow > 60*60*12) {
				// test 부터 먼저 할것!!!	
				UbcMessageBox(m_testWarningMsg);
				return false;	
			}
		}
	}


	WritePrivateProfileString("PACKAGECHG_DLG", "Package", strPackage, szPath);
	WritePrivateProfileString("PACKAGECHG_DLG", "loginId", m_loginId, szPath);
	WritePrivateProfileString("PACKAGECHG_DLG", "TargetHostCount", ToString(targetHostCount), szPath);
	WritePrivateProfileString("PACKAGECHG_DLG", "ExecutionTime", ToString((ULONG)now), szPath);
	return true;

}

