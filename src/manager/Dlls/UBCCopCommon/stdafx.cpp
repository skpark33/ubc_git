// stdafx.cpp : source file that includes just the standard includes
// UBCCopCommon.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

CString ToString(int nVal)
{
	CString szBuf;
	szBuf.Format("%d", nVal);
	return szBuf;
}

CString ToString(unsigned long lVal)
{
	CString szBuf;
	szBuf.Format("%ld", lVal);
	return szBuf;
}

CString ToString(double dVal, int nDegree)
{
	CString strFormat;
	strFormat.Format(_T("%%.%df"), nDegree);

	CString szBuf;
	szBuf.Format(strFormat, dVal);
	return szBuf;
}

CString ToString(ULONGLONG lVal)
{
	CString szBuf;
	szBuf.Format("%I64d", lVal);
	return szBuf;
}

CString ToFileSize(ULONGLONG lVal, char chUnit, bool bIncUnit /*= true*/)
{
	CString strVal;
	CString strUnit;

	if(chUnit == 'A' || chUnit == 'a')
	{
		if     (lVal / 1024 == 0) chUnit = 'B';
		else if(lVal / (1024*1024) == 0) chUnit = 'K';
		else if(lVal / (1024*1024*1024) == 0) chUnit = 'M';
		else chUnit = 'G';
	}

	if(chUnit == 'B' || chUnit == 'b')
	{
		strVal = ToString(lVal);
		if(bIncUnit) strUnit = _T("Byte");
	}
	else if(chUnit == 'K' || chUnit == 'k')
	{
		strVal = ToString(lVal/1024, 1);
		if(bIncUnit) strUnit = _T("KByte");
	}
	else if(chUnit == 'M' || chUnit == 'm')
	{
		strVal = ToString(lVal/(1024*1024.), 1);
		if(bIncUnit) strUnit = _T("MByte");
	}
	else if(chUnit == 'G' || chUnit == 'g')
	{
		strVal = ToString(lVal/(1024*1024*1024.), 1);
		if(bIncUnit) strUnit = _T("GByte");
	}

	return strVal + strUnit;
}

CString MakeCurrency(ULONGLONG lVar)
{
	CString szDigit;
	CString szBuff = _T("");
	ULONGLONG v = lVar;
	
	int i = 0;
	do{
		if(i && !(i%3))
			szBuff = _T(",") + szBuff;
		szDigit.Format("%c", (v%10) + '0');
		szBuff = szDigit + szBuff;
		v /= 10;
		i++;
	}while(v);
	return szBuff;
}

CString LoadStringById(UINT nID)
{
	CString strValue;
	if(!strValue.LoadString(nID)) return _T("");
	return strValue;
}

CTime NormalizeTime(CTime tmValue)
{
	return NormalizeTime(tmValue.GetHour(), tmValue.GetMinute());
}

CTime NormalizeTime(int nHour, int nMinute)
{
	CTime tmCurTime = CTime::GetCurrentTime();
	return CTime( tmCurTime.GetYear()
				, tmCurTime.GetMonth()
				, tmCurTime.GetDay()
				, 0, 0, 0 )
				+ CTimeSpan(0, nHour, nMinute, 0);
}

void InitDateRange(CDateTimeCtrl& dtControl)
{
	CTime tmFrom = CTime(2000,1,1,0,0,0);
	CTime tmTo = CTime(2037,12,31,23,59,59);
	dtControl.SetRange(&tmFrom, &tmTo);
}