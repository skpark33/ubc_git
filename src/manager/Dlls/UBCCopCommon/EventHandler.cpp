#include "stdafx.h"
#include "eventhandler.h"
#include "common\TraceLog.h"
#include "common\ubcdefine.h"
#include "common\libCommon\utvmacro.h"

#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <idl/CCI/CCITypes.h>
#include <cci/libWrapper/cciXCallManager.h>
#include <cci/libValue/cciAny.h>
#include <cci/libWrapper/cciEventManager.h>
#include <cci/libWrapper/cciEventHandler.h>

bool CEventManager::m_bServerStatus = false;

///////////////////////////////////////////////////////////////////////////////////////////////////
// CEventManager
///////////////////////////////////////////////////////////////////////////////////////////////////
CEventManager::CEventManager(IEventHandler* pIf)
{
	m_pInterface = pIf;
}

CEventManager::~CEventManager()
{
	m_pInterface = NULL;
}

//int CEventManager::SetStatChangeEvent(LPCTSTR szSite)
//{
//	try{
//		cciEventManager* pEventManager = cciEventManager::getInstance();
//		if(NULL == pEventManager){
//			TraceLog(("Event Manager의 핸들을 등록할 수 없습니다"));
//			return -1;
//		}
//
//		cciEntity aEntity("PM=*/Site=%s/Host=*", szSite);
//
//		int handelId = pEventManager->addHandler(aEntity, "operationalStateChanged", this);
//
//		if(0 > handelId){
//			TraceLog(("Add StatChangeEvent failed"));
//			return -1;
//		}
//
//		TraceLog(("Add StatChangeEvent successed"));
//		return handelId;
//
//	}catch(CCI::CCI_Exception& ke){
//		TraceLog(("Catch CCI::CCI_Exception[%d] : %s", ke.code, ke.reason.in()));
//		return -1;
//	}catch(CORBA::Exception& ce){
//		TraceLog(("Catch CORBA::Exception : %s", ce._name()));
//		return -1;
//	}
//
//	return -1;
//}

int CEventManager::AddEvent(LPCTSTR szParam)
{
	ciStringTokenizer token(szParam);
	
	if(token.countTokens() != 2)
		return -1;

	ciString entity_str;
	if(token.hasMoreTokens())
		entity_str = token.nextToken();

	ciString id;
	if(token.hasMoreTokens())
		id = token.nextToken();

	cciEntity entity;
	entity.fromString(entity_str.c_str());

	try{
		cciEventManager* pEventManager = cciEventManager::getInstance();
		if(NULL == pEventManager){
			TraceLog(("To create cciEventManager is failed"));
			return -1;
		}

		CEventHandler* pEventHandler = new CEventHandler(&m_pInterface);
		if(NULL == pEventHandler){
			TraceLog(("To create CEventHandler is failed"));
			return -1;
		}

		TraceLog(("Add Event : %s, %s", entity.toString(), id.c_str()));
		ciLong handleId = pEventManager->addHandler(entity, id, pEventHandler);
		TraceLog(("handleId : %d", handleId));

		m_lsEventID.AddTail(handleId);

		return handleId;
	}catch (CCI::CCI_Exception& ke){
		TraceLog(("Catch CCI::CCI_Exception[%d] : %s", ke.code, ke.reason.in()));
		return -1;
	}catch (CORBA::Exception& ce){
		TraceLog(("Catch CORBA::Exception : %s", ce._name()));
		return -1;
	}

	return -1;
}

BOOL CEventManager::RemoveEvent(int handelId)
{
	if(handelId < 0) return FALSE;

	// 0000078: 서버가 죽은 상태에서, Manager 를 죽이면 Manager가 Hang걸리지 않도록 수정
	if(!GetServerStatus()) return TRUE;

	try
	{
		cciEventManager* pEventManager = cciEventManager::getInstance();

		if(NULL == pEventManager)
		{
			TraceLog(("Event Manager의 핸들을 삭제할 수 없습니다"));
			return FALSE;
		}

		if(!pEventManager->removeHandler(handelId)){
			TraceLog(("removeHandler(%d) is failed", handelId));
			return FALSE;
		}

		POSITION pos = m_lsEventID.Find(handelId);
		if(pos)
		{
			m_lsEventID.RemoveAt(pos);
		}
	}
	catch(CCI::CCI_Exception& ke)
	{
		TraceLog(("Catch CCI::CCI_Exception[%d] : %s", ke.code, ke.reason.in()));
		return FALSE;
	}
	catch(CORBA::Exception& ce)
	{
		TraceLog(("Catch CORBA::Exception : %s", ce._name()));
		return FALSE;
	}

	TraceLog(("removeHandler(%d) is successed", handelId));

	return TRUE;
}

BOOL CEventManager::RemoveAllEvent()
{
	SetInterfacePtr(NULL);

	CList<int> list;

	POSITION pos = m_lsEventID.GetHeadPosition();
	while(pos)
	{
		list.AddTail(m_lsEventID.GetNext(pos));
	}

	pos = list.GetHeadPosition();
	while(pos)
	{
		int evId = list.GetNext(pos);
		RemoveEvent(evId);
	}

	m_lsEventID.RemoveAll();

	return TRUE;
}

void CEventManager::ClearEvent()
{
	if(GetServerStatus())
	{
		cciEventManager::clearInstance();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// CEventHandler
///////////////////////////////////////////////////////////////////////////////////////////////////
int CEventHandler::m_nRefCnt = 0;
bool CEventHandler::m_bIgnoreEvent = false;
bool CEventHandler::CloseWaitAllEvent(HWND hWnd)
{
	TraceLog(("CloseWaitAllEvent [%d]", m_nRefCnt));

	m_bIgnoreEvent = true;
	if(m_nRefCnt <= 0) return true;

	TraceLog(("CloseWaitAllEventThread create"));

	AfxBeginThread(CEventHandler::CloseWaitAllEventThread, hWnd);

	return false;
}

UINT CEventHandler::CloseWaitAllEventThread(LPVOID pParam)
{
	TraceLog(("CloseWaitAllEventThread begin"));

	HWND hWnd = (HWND)pParam;
	while(m_nRefCnt > 0)
	{
		Sleep(100);
	}

	TraceLog(("::SendMessage(hWnd, WM_CLOSE) begin"));

	if(hWnd && ::IsWindow(hWnd)) ::SendMessage(hWnd, WM_CLOSE, 0, 0);

	TraceLog(("CloseWaitAllEventThread end"));

	return 0;
}

void CEventHandler::processEvent(cciEvent& ev)
{
	TraceLog(("m_bIgnoreEvent == [%s]", (m_bIgnoreEvent ? "TRUE" : "FALSE")));
	if(m_bIgnoreEvent) return;

	TraceLog(("m_ppInterface == [%s]", (m_ppInterface == NULL ? "NULL" : "NOT")));
	if(m_ppInterface == NULL) return;

	TraceLog(("pInterface == [%s]", (*m_ppInterface == NULL ? "NULL" : "NOT")));
	// 0001414: 단말목록등 서버로부터 이벤트를 받는 화면이 닫힐때 응답없음으로 빠지는 문제.
	IEventHandler* pInterface = *m_ppInterface;
	if(pInterface == NULL) return;

	m_nRefCnt++;

//	cciEntity aEntity;
//	aEntity.set(ev.getEntity());
	ciString strType = ev.getEventType();

	CCI::CCI_AttributeList& tempList = ev.getAttributeList();
	cciAttributeList attList(tempList);

	TraceLog(("Invoke event : %s", strType.c_str()));
	if(strType == "operationalStateChanged")
	{
		ciString strSite, strHost;
		ciBoolean bStat;

		for(int i = 0; i < (int)attList.length(); i++)
		{
			cciAttribute& attr = attList[i];

			if(IsSame("siteId",attr.getName()))
			{
				strSite = "";
				attr.getValue(strSite);
			}
			else if(IsSame("hostId",attr.getName()))
			{
				strHost = "";
				attr.getValue(strHost);
			}
			else if(IsSame("operationalState",attr.getName()))
			{
				bStat = false;
				attr.getValue(bStat);
			}
		}

		SOpStat data;
		strncpy(data.siteId, strSite.c_str(), sizeof(data.siteId));
		strncpy(data.hostId, strHost.c_str(), sizeof(data.hostId));
		data.operationalState = bStat;

		pInterface->InvokeEvent(IEH_OPSTAT, (LPARAM)&data);
	}
	else if(strType == "deviceOperationalStateChanged")
	{
		ciString strSite, strHost,strObjectClass, strObjectId;
		ciBoolean bStat;

		for(int i = 0; i < (int)attList.length(); i++)
		{
			cciAttribute& attr = attList[i];

			if(IsSame("siteId",attr.getName()))
			{
				strSite = "";
				attr.getValue(strSite);
			}
			else if(IsSame("hostId",attr.getName()))
			{
				strHost = "";
				attr.getValue(strHost);
			}
			else if(IsSame("objectClass",attr.getName()))
			{
				strObjectClass = "";
				attr.getValue(strObjectClass);
			}
			else if(IsSame("objectId",attr.getName()))
			{
				strObjectId = "";
				attr.getValue(strObjectId);
			}
			else if(IsSame("operationalState",attr.getName()))
			{
				bStat = false;
				attr.getValue(bStat);
			}
		}

		SDeviceOpStat data;
		strncpy(data.siteId, strSite.c_str(), sizeof(data.siteId));
		strncpy(data.hostId, strHost.c_str(), sizeof(data.hostId));
		strncpy(data.objectClass, strObjectClass.c_str(), sizeof(data.objectClass));
		strncpy(data.objectId, strObjectId.c_str(), sizeof(data.objectId));
		data.operationalState = bStat;

		pInterface->InvokeEvent(IEH_DEVICE_OPSTAT, (LPARAM)&data);
	}
	else if(strType == "adminStateChanged")
	{
		ciString strSite, strHost;
		ciBoolean bStat;

		for(int i = 0; i < (int)attList.length(); i++)
		{
			cciAttribute& attr = attList[i];

			if(IsSame("siteId",attr.getName()))
			{
				strSite = "";
				attr.getValue(strSite);
			}
			else if(IsSame("hostId",attr.getName()))
			{
				strHost = "";
				attr.getValue(strHost);
			}
			else if(IsSame("adminState",attr.getName()))
			{
				bStat = false;
				attr.getValue(bStat);
			}
		}

		SAdStat data;
		strncpy(data.siteId, strSite.c_str(), sizeof(data.siteId));
		strncpy(data.hostId, strHost.c_str(), sizeof(data.hostId));
		data.adminState = bStat;

		pInterface->InvokeEvent(IEH_ADSTAT, (LPARAM)&data);
	}
	// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	else if(strType == "monitorStateChanged")
	{
		ciString strSite, strHost;
		ciShort nStat;

		for(int i = 0; i < (int)attList.length(); i++)
		{
			cciAttribute& attr = attList[i];

			if(IsSame("siteId",attr.getName()))
			{
				strSite = "";
				attr.getValue(strSite);
			}
			else if(IsSame("hostId",attr.getName()))
			{
				strHost = "";
				attr.getValue(strHost);
			}
			else if(IsSame("monitorState",attr.getName()))
			{
				nStat = -1;
				attr.getValue(nStat);
			}
		}

		SMonitorStat data;
		strncpy(data.siteId, strSite.c_str(), sizeof(data.siteId));
		strncpy(data.hostId, strHost.c_str(), sizeof(data.hostId));
		data.monitorState = nStat;

		pInterface->InvokeEvent(IEH_MOSTAT, (LPARAM)&data);
	}
	else if(strType == "hostScheduleUpdated")
	{
		SUpPackage data;

		for(int i = 0; i < (int)attList.length(); i++)
		{
			ciString str = "";
			ciShort nUse = 0;
			cciAttribute& attr = attList[i];

			if(IsSame(attr.getName(),"siteId"))
			{
				attr.getValue(str);
				data.siteId = str.c_str();
			}
			else if(IsSame(attr.getName(),"hostId"))
			{
				attr.getValue(str);
				data.hostId = str.c_str();
			}
			else if(IsSame(attr.getName(),"currentSchedule1"))
			{
				attr.getValue(str);
				data.currentPackage1 = str.c_str();
			}
			else if(IsSame(attr.getName(),"currentSchedule2"))
			{
				attr.getValue(str);
				data.currentPackage2 = str.c_str();
			}
			else if(IsSame(attr.getName(),"autoSchedule1"))
			{
				attr.getValue(str);
				data.autoPackage1 = str.c_str();
			}
			else if(IsSame(attr.getName(),"autoSchedule2"))
			{
				attr.getValue(str);
				data.autoPackage2 = str.c_str();
			}
			else if(IsSame(attr.getName(),"lastSchedule1"))
			{
				attr.getValue(str);
				data.lastPackage1 = str.c_str();
			}
			else if(IsSame(attr.getName(),"lastSchedule2"))
			{
				attr.getValue(str);
				data.lastPackage2 = str.c_str();
			}
			else if(IsSame(attr.getName(),"networkUse1"))
			{
				attr.getValue(nUse);
				data.networkUse1 = nUse;
			}
			else if(IsSame(attr.getName(),"networkUse2"))
			{
				attr.getValue(nUse);
				data.networkUse2 = nUse;
			}
			else if(IsSame(attr.getName(),"hostMsg1"))
			{
				attr.getValue(str);
				data.hostMsg1 = str.c_str();
			}
			else if(IsSame(attr.getName(),"hostMsg2"))
			{
				attr.getValue(str);
				data.hostMsg2 = str.c_str();
			}

			if(strlen(attr.getName()) > 0)
			{
				if(str.empty())
				{
					TraceLog(("Attribute Name : %s / Value : %d",attr.getName(),nUse));
				}
				else
				{
					TraceLog(("Attribute Name : %s / Value : %s",attr.getName(),str.c_str()));
				}
			}
		}

		pInterface->InvokeEvent(IEH_UPPACKAGE, (LPARAM)&data);
	}
	else if(strType == "scheduleReserved")
	{
		pInterface->InvokeEvent(IEH_SCHEREV, (LPARAM)&ev);
	}
	else if(strType == "reservationRemoved")
	{
		pInterface->InvokeEvent(IEH_REMVREV, (LPARAM)&ev);
	}
	else if(strType == "reservationExpired")
	{
		pInterface->InvokeEvent(IEH_EXPIREV, (LPARAM)&ev);
	}
	else if(strType == "reservationChanged")
	{
		pInterface->InvokeEvent(IEH_CHNGREV, (LPARAM)&ev);
	}
	else if(strType == "announceCreated")
	{
		pInterface->InvokeEvent(IEH_CREANNO, (LPARAM)&ev);
	}
	else if(strType == "announceChanged")
	{
		pInterface->InvokeEvent(IEH_CHGANNO, (LPARAM)&ev);
	}
	else if(strType == "announceRemoved")
	{
		pInterface->InvokeEvent(IEH_RMVANNO, (LPARAM)&ev);
	}
	else if(strType == "announceExpired")
	{
		pInterface->InvokeEvent(IEH_EXPANNO, (LPARAM)&ev);
	}
	else if(strType == "vncStateChanged")
	{
		TraceLog(("pInterface->InvokeEvent begin"));
		pInterface->InvokeEvent(IEH_CHNGVNC, (LPARAM)&ev);
		TraceLog(("pInterface->InvokeEvent end"));
	}
	else if(strType == "bpCreated")
	{
		pInterface->InvokeEvent(IEH_CREBPID, (LPARAM)&ev);
	}
	else if(strType == "bpChanged")
	{
		pInterface->InvokeEvent(IEH_CHGBPID, (LPARAM)&ev);
	}
	else if(strType == "bpRemoved")
	{
		pInterface->InvokeEvent(IEH_CREBPID, (LPARAM)&ev);
	}
	else if(strType == "scheduleStateLogCreated")
	{
		SPackageStateLog data;

		ciTime   eventTime       ;
		ciString siteId          ;
		ciString hostId          ;
		ciString autoPackage1   ;
		ciString autoPackage2   ;
		ciString currentPackage1;
		ciString currentPackage2;
		ciString lastPackage1   ;
		ciString lastPackage2   ;

		attList.getItem("eventTime"       , eventTime       ); data.eventTime        = eventTime       .getTime();
		attList.getItem("siteId"          , siteId          ); data.siteId           = siteId          .c_str();
		attList.getItem("hostId"          , hostId          ); data.hostId           = hostId          .c_str();
		attList.getItem("autoSchedule1"   , autoPackage1   ); data.autoPackage1    = autoPackage1   .c_str();
		attList.getItem("autoSchedule2"   , autoPackage2   ); data.autoPackage2    = autoPackage2   .c_str();
		attList.getItem("currentSchedule1", currentPackage1); data.currentPackage1 = currentPackage1.c_str();
		attList.getItem("currentSchedule2", currentPackage2); data.currentPackage2 = currentPackage2.c_str();
		attList.getItem("lastSchedule1"   , lastPackage1   ); data.lastPackage1    = lastPackage1   .c_str();
		attList.getItem("lastSchedule2"   , lastPackage2   ); data.lastPackage2    = lastPackage2   .c_str();

		pInterface->InvokeEvent(IEH_CRESLOG, (LPARAM)&data);
	}
	else if(strType == "powerStateLogCreated")
	{
		SPowerStateLog data;

		ciTime   eventTime   ;
		ciString siteId      ;
		ciString hostId      ;
		ciTime   bootUpTime  ;
		ciTime   bootDownTime;

		attList.getItem("eventTime"   , eventTime   ); data.eventTime    = eventTime   .getTime();
		attList.getItem("siteId"      , siteId      ); data.siteId       = siteId      .c_str();
		attList.getItem("hostId"      , hostId      ); data.hostId       = hostId      .c_str();
		attList.getItem("bootUpTime"  , bootUpTime  ); data.bootUpTime   = bootUpTime  .getTime();
		attList.getItem("bootDownTime", bootDownTime); data.bootDownTime = bootDownTime.getTime();

		pInterface->InvokeEvent(IEH_CRESLOG, (LPARAM)&data);
	}
	else if(strType == "connectionStateLogCreated")
	{
		SConnectionStateLog data;

		ciTime    eventTime       ;
		ciString  siteId          ;
		ciString  hostId          ;
		ciBoolean operationalState;

		attList.getItem("eventTime"       , eventTime       ); data.eventTime        = eventTime       .getTime();
		attList.getItem("siteId"          , siteId          ); data.siteId           = siteId          .c_str();
		attList.getItem("hostId"          , hostId          ); data.hostId           = hostId          .c_str();
		attList.getItem("operationalState", operationalState); data.operationalState = operationalState;

		pInterface->InvokeEvent(IEH_CRESLOG, (LPARAM)&data);
	}
	else if(strType == "faultAlarm")
	{
		SFault data;

		ciString faultId       ;
		ciString siteId        ;
		ciString hostId        ;
		ciLong   counter       ;
		ciLong   severity      ;
		ciString probableCause ;
		ciTime   eventTime     ;
		ciTime   updateTime    ;
		ciString additionalText;

		attList.getItem("faultId"       , faultId       ); data.faultId        = faultId       .c_str();
		attList.getItem("siteId"        , siteId        ); data.siteId         = siteId        .c_str();
		attList.getItem("hostId"        , hostId        ); data.hostId         = hostId        .c_str();
		attList.getItem("counter"       , counter       ); data.counter        = counter       ;
		attList.getItem("severity"      , severity      ); data.severity       = severity      ;
		attList.getItem("probableCause" , probableCause ); data.probableCause  = probableCause .c_str();
		attList.getItem("eventTime"     , eventTime     ); data.eventTime      = eventTime     .getTime();
		attList.getItem("updateTime"    , updateTime    ); data.updateTime     = updateTime    .getTime();
		attList.getItem("additionalText", additionalText); data.additionalText = additionalText.c_str();

		pInterface->InvokeEvent(IEH_FLTALAM, (LPARAM)&data);
	}
	else if(strType == "updateAlarm")
	{
		SFaultUpdate data;

		for(int i = 0; i < (int)attList.length(); i++)
		{
			ciString strValue = "";
			ciTime tmTime = "";
			ciLong lValue = 0;
			cciAttribute& attr = attList[i];

			if(IsSame(attr.getName(),"faultId"))
			{
				attr.getValue(strValue);
				data.faultId = strValue.c_str();
			}
			else if(IsSame(attr.getName(),"counter"))
			{
				attr.getValue(lValue);
				data.counter = lValue;
				data.bCounter = true;
			}
			else if(IsSame(attr.getName(),"updateTime"))
			{
				attr.getValue(tmTime);
				data.updateTime = tmTime.getTime();
				data.bUpdateTime = true;
			}
			else if(IsSame(attr.getName(),"additionalText"))
			{
				attr.getValue(strValue);
				data.additionalText = strValue.c_str();
				data.bAdditionalText = true;
			}
		}

		pInterface->InvokeEvent(IEH_UPDALAM, (LPARAM)&data);
	}
	else if(strType == "processStateChanged")
	{
		SProcessStateChange data;

		ciString  siteId;
		ciString  hostId;
		ciBoolean starterState;
		ciBoolean browserState;
		ciBoolean browser1State;
		ciBoolean firmwareViewState;
		ciBoolean preDownloaderState;

		attList.getItem("siteId"            , siteId            ); data.siteId             = siteId    .c_str();
		attList.getItem("hostId"            , hostId            ); data.hostId             = hostId    .c_str();
		attList.getItem("starterState"      , starterState      ); data.starterState       = starterState      ;
		attList.getItem("browserState"      , browserState      ); data.browserState       = browserState      ;
		attList.getItem("browser1State"     , browser1State     ); data.browser1State      = browser1State     ;
		attList.getItem("firmwareViewState" , firmwareViewState ); data.firmwareViewState  = firmwareViewState ;
		attList.getItem("preDownloaderState", preDownloaderState); data.preDownloaderState = preDownloaderState;

		pInterface->InvokeEvent(IEH_CHNGPRS, (LPARAM)&data);
	}
	// 0000707: 콘텐츠 다운로드 상태 조회 기능
	else if(strType == "downloadStateChanged")
	{
		SDownloadStateChange data;

		ciString siteId;
		ciString hostId;
		ciShort  brwId;				// 0 or 1
		ciString programId;
		ciString progress;
		ciLong	 programState;		// 0 : init, 1: start, 2:partially failed, 3:failed,4:succeed
		ciTime   programStartTime;
		ciTime   programEndTime;

		attList.getItem("siteId"          , siteId          ); data.siteId           = siteId    .c_str();
		attList.getItem("hostId"          , hostId          ); data.hostId           = hostId    .c_str();
		attList.getItem("brwId"           , brwId           ); data.brwId            = brwId             ;
		attList.getItem("programId"       , programId       ); data.programId        = programId .c_str();
		attList.getItem("progress"        , progress        ); data.progress         = progress  .c_str();
		attList.getItem("programState"    , programState    ); data.programState     = programState      ;
		attList.getItem("programStartTime", programStartTime); data.programStartTime = programStartTime.getTime();
		attList.getItem("programEndTime"  , programEndTime  ); data.programEndTime   = programEndTime.getTime();

		TraceLog(("siteId       [%s]", data.siteId      ));
		TraceLog(("hostId       [%s]", data.hostId      ));
		TraceLog(("brwId        [%d]", data.brwId       ));
		TraceLog(("programId    [%s]", data.programId   ));
		TraceLog(("progress     [%s]", data.progress    ));
		TraceLog(("programState [%d]", data.programState));

		pInterface->InvokeEvent(IEH_CHNGDOWN, (LPARAM)&data);
	}
	else if(strType == "alarmSummary")
	{
		SAlarmSummary data;

		ciString summary;
		ciTime   updateTime;

		attList.getItem("summary", summary); data.summary = summary.c_str();
		attList.getItem("updateTime", updateTime); data.updateTime = updateTime.getTime();

		CString strSummary = data.summary;
		int nEtcAlarm = 0;
		int pos=0;
		CString strToken;
		while(!(strToken = strSummary.Tokenize(_T(","), pos)).IsEmpty())
		{
			int nDelim = strToken.Find(_T("="));
			if(nDelim < 0) continue;

			CString strKey = strToken.Left(nDelim);
			int nValue = _ttoi(strToken.Mid(nDelim+1));

			if     (strKey == HOST_PROCESS_DOWN      ) data.nProcessDown = nValue;
			else if(strKey == HOST_COMMUNICATION_FAIL) data.nCommunicationFail = nValue;
			else if(strKey == HOST_HDD_OVERLOAD      ) data.nHddOverload = nValue;
			else if(strKey == HOST_MEM_OVERLOAD      ) data.nMemOverload = nValue;
			else if(strKey == HOST_CPU_OVERLOAD      ) data.nCpuOverload = nValue;
			else									   nEtcAlarm += nValue;
		}

		data.nEtcAlarm = nEtcAlarm;

		pInterface->InvokeEvent(IEH_SUMALAM, (LPARAM)&data);
	}
	else if(strType == "diskAvailChanged")
	{
		SDiskAvailChanged data;

		ciString siteId;
		ciString hostId;
		ciShort  diskId;
		ciFloat	 diskAvail;

		attList.getItem("siteId"   , siteId   ); data.siteId    = siteId.c_str();
		attList.getItem("hostId"   , hostId   ); data.hostId    = hostId.c_str();
		attList.getItem("diskId"   , diskId   ); data.diskId    = diskId        ;
		attList.getItem("diskAvail", diskAvail); data.diskAvail = diskAvail     ;

		TraceLog(("siteId   [%s]", data.siteId   ));
		TraceLog(("hostId   [%s]", data.hostId   ));
		TraceLog(("diskId   [%d]", data.diskId   ));
		TraceLog(("diskAvail[%f]", data.diskAvail));

		pInterface->InvokeEvent(IEH_HDDCHNG, (LPARAM)&data);
	}
	else
	{
		pInterface->InvokeEvent(IEH_UNKOWN, (LPARAM)&ev);
	}

	m_nRefCnt--;
}