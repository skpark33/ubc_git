#pragma once

#ifndef IDD_SELECT_USER_DLG
#define IDD_SELECT_USER_DLG	25294
#endif//IDD_SELECT_USER_DLG

#include "common/HoverButton.h"
#include "common/UTBListCtrlEx.h"

// CSelectUsers 대화 상자입니다.

class AFX_EXT_CLASS CSelectUsers : public CDialog
{
	DECLARE_DYNAMIC(CSelectUsers)

public:
	CSelectUsers(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSelectUsers();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SELECT_USER_DLG };
	//enum { eUserSite, eUserID, eUserName, eUserPerm, eUserNo, eUserTel, eUserMail, eUserEnd };
	enum { eUserID, eUserName, eUserEnd };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	bool				m_bSingleSelect;
	UserInfoList		m_listUsers;

	UserInfoList		m_listSelectUser;

	void				InitList();
	void				RefreshList(CString strFilter=_T(""));

public:
	void				SetSingleSelection(bool bSingleSelect) { m_bSingleSelect=bSingleSelect; };
	UserInfoList&		GetSelectedList() { return m_listSelectUser; };

public:
	CUTBListCtrlEx		m_lcList;
	CHoverButton		m_btnSelect;
	CHoverButton		m_btnCancel;

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnEnChangeEbSearch();
};
