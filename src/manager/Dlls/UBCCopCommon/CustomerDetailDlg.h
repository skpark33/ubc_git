#pragma once
#include "afxwin.h"
#include "resource.h"
#include "CopModule.h"

// CCustomerDetailDlg 대화 상자입니다.

class CCustomerDetailDlg : public CDialog
{
	DECLARE_DYNAMIC(CCustomerDetailDlg)

public:
	CCustomerDetailDlg(int btMode, SCustomerInfo& info, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCustomerDetailDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_CUSTOMER_DETAIL };
	enum { eCreate, eSave, eDelete };

	void	GetCustomerInfo(SCustomerInfo& info) { info = m_info; }
	int		GetMode() { return m_btMode; }
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	SCustomerInfo m_info;
	int			m_btMode;
	int			m_gmtVal[48];

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedDelete();
	afx_msg void OnBnClickedCancel();
	CButton m_btDel;
	CButton m_btOK;
	CButton m_btCancel;
	CEdit m_editCustomerId;
	CComboBox m_cbLanguage;
	CComboBox m_cbTimeZone;
	CEdit m_editDescription;
	CEdit m_editURL;
	virtual BOOL OnInitDialog();

	bool	SetData();
};
