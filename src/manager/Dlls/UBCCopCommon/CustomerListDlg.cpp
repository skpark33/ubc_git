// CustomerListDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CustomerListDlg.h"
#include "CustomerDetailDlg.h"
#include "PasswordDlg.h"


// CCustomerListDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCustomerListDlg, CDialog)

CCustomerListDlg::CCustomerListDlg(CString strCustomerInfo,CString user, int auth, CWnd* pParent /*=NULL*/)
	: CDialog(CCustomerListDlg::IDD, pParent)
	, m_strCustomerName(strCustomerInfo)
	, m_szLoginID(user)
	, m_Authority(auth)
{

}

CCustomerListDlg::~CCustomerListDlg()
{
}

void CCustomerListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_btAdd);
	DDX_Control(pDX, IDC_LIST_CUSTOMER, m_lscCustomer);
	DDX_Control(pDX, IDC_BUTTON_REF, m_btRef);
	DDX_Control(pDX, IDC_EDIT_CUSTOMER, m_editCustomer);
}


BEGIN_MESSAGE_MAP(CCustomerListDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCustomerListDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCustomerListDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_REF, &CCustomerListDlg::OnBnClickedButtonRef)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CCustomerListDlg::OnBnClickedButtonAdd)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CUSTOMER, &CCustomerListDlg::OnNMDblclkListCustomer)
	ON_NOTIFY(NM_CLICK, IDC_LIST_CUSTOMER, &CCustomerListDlg::OnNMClickListCustomer)
END_MESSAGE_MAP()


// CCustomerListDlg 메시지 처리기입니다.

void CCustomerListDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strCustomerName.IsEmpty() || m_strCustomerName == "UNKNOWN"){
		m_editCustomer.GetWindowTextA(m_strCustomerName);
	}
	if(m_strCustomerName.IsEmpty() || m_strCustomerName == "UNKNOWN"){
		UbcMessageBox(LoadStringById(IDS_CUSTOMERINFO_MSG001));
		return;
	}

	m_nCustomerTimezone = 9999;
	m_strCustomerLanguage = _T("");

	// 0001408: Customer 정보 입력을 받는다.
	POSITION pos = m_listCustomerInfo.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SCustomerInfo info = m_listCustomerInfo.GetNext(pos);
		if(m_strCustomerName != info.customerId) continue;

		m_nCustomerTimezone = info.gmt;
		m_strCustomerLanguage = info.language;

		TCHAR szModule[MAX_PATH];
		::ZeroMemory(szModule, MAX_PATH);
		::GetModuleFileName(NULL, szModule, MAX_PATH);

		TCHAR szDrive[MAX_PATH], szPath[MAX_PATH];
		_tsplitpath(szModule, szDrive, szPath, NULL, NULL);

		CString szIniPath;
		szIniPath.Format(_T("%s%sdata\\%s"), szDrive, szPath, UBCVARS_INI);
		::WritePrivateProfileString("CUSTOMER_INFO", "NAME", m_strCustomerName, szIniPath);
		::WritePrivateProfileString("CUSTOMER_INFO", "GMT", ToString(info.gmt), szIniPath);
		::WritePrivateProfileString("CUSTOMER_INFO", "LANGUAGE", info.language, szIniPath);

		// URL.INI를 새로 작성한다.
		szIniPath.Format("%s%sdata\\%s", szDrive, szPath, UBCURL_INI);
		CFile file;
		if(file.Open(szIniPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
		{
			file.Write((LPCTSTR)info.url, info.url.GetLength()*sizeof(TCHAR));
			file.Close();
		}
	}

	OnOK();
}

void CCustomerListDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	return;
	//OnCancel();
}

void CCustomerListDlg::OnBnClickedButtonRef()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SetList();
	UbcMessageBox(LoadStringById(IDS_REFRASHLIST_MSG), MB_ICONINFORMATION);
}

void CCustomerListDlg::OnBnClickedButtonAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_szLoginID != "super" || m_Authority != CCopModule::eSiteAdmin) {	
		return; // super가 아니면 추가가 동작하지 않는다.
	}

	SCustomerInfo info;
	CCustomerDetailDlg aDlg(eCreate,info);

	if(aDlg.DoModal() != IDOK){
		return;
	}

	aDlg.GetCustomerInfo(info);
	if(!CCopModule::GetObject()->CreateCustomer(info)){
		UbcMessageBox("Customer Create failed", MB_ICONINFORMATION);
		return ;
	}
	UbcMessageBox(LoadStringById(IDS_CUSTOMER_STR004), MB_ICONINFORMATION);
	SetList();
}

void CCustomerListDlg::OnNMDblclkListCustomer(NMHDR *pNMHDR, LRESULT *pResult)
{
	//OnBnClickedOk();
	
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscCustomer.GetItemData(nRow);
	SCustomerInfo info = m_listCustomerInfo.GetAt(pos);

	m_editCustomer.SetWindowText(info.customerId);

	if(m_szLoginID != "super" || m_Authority != CCopModule::eSiteAdmin) {	
		return; // super가 아니면 더블클릭이 동작하지 않는다.
	}

	CCustomerDetailDlg aDlg(eSave,info);
	int retcode = aDlg.DoModal();
	if(retcode == IDOK){
		aDlg.GetCustomerInfo(info);
		if(aDlg.GetMode() == eSave) {
			if(!CCopModule::GetObject()->SetCustomer(info)){
				UbcMessageBox("Customer Set failed", MB_ICONINFORMATION);
				return ;
			}
			UbcMessageBox(LoadStringById(IDS_CUSTOMER_STR005), MB_ICONINFORMATION);
		} else {
			if(UbcMessageBox(LoadStringById(IDS_CUSTOMER_DETAIL_MSG02), MB_YESNO)==IDYES){
				aDlg.GetCustomerInfo(info);

				CPasswordDlg aPasswordDlg;
				if(aPasswordDlg.DoModal()!=IDOK){
					return ;
				}
				CString hardCodedPassword = info.customerId;
				hardCodedPassword.Append("_sqisoft");
				if(aPasswordDlg.GetPassword() != hardCodedPassword){
					UbcMessageBox("Password mis-matched", MB_ICONINFORMATION);
					return ;
				}

				CString myIp = CCopModule::GetObjectA()->GetMyIp();
				ciTime now;
				info.description=info.customerId;
				info.description.Append(" Deleted by ");
				info.description.Append(myIp);
				info.description.Append(" at ");
				info.description.Append(now.getTimeString().c_str());
				if(!CCopModule::GetObject()->SetCustomer(info)){
					UbcMessageBox("Customer Delete failed (Delete Log failed)", MB_ICONINFORMATION);
					return ;
				}
				if(!CCopModule::GetObject()->DelCustomer(info)){
					UbcMessageBox("Customer Delete failed", MB_ICONINFORMATION);
					return ;
				}
				UbcMessageBox(LoadStringById(IDS_CUSTOMER_STR006), MB_ICONINFORMATION);
			}
		}
		SetList();
		return;
	}
	
}

void CCustomerListDlg::OnNMClickListCustomer(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscCustomer.GetItemData(nRow);
	SCustomerInfo info = m_listCustomerInfo.GetAt(pos);

	m_editCustomer.SetWindowText(info.customerId);

}

BOOL CCustomerListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btAdd.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btRef.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));

	m_btAdd.SetToolTipText(LoadStringById(IDS_CUSTOMER_STR002));
	m_btRef.SetToolTipText(LoadStringById(IDS_CUSTOMER_STR003));


	InitList();
	SetList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCustomerListDlg::InitList()
{
	// m_cb_category 를 초기화

	if(m_szLoginID != "super" || m_Authority != CCopModule::eSiteAdmin) {	
		m_btAdd.EnableWindow(false);
	}

	// eCheck, eCustomer, eTimeZone, eLanguage, eDescription, eURL, eEnd
	//m_ColumTitle[eCheck     ] = _T("");
	m_ColumTitle[eCustomer     ] =		LoadStringById(IDS_CUSTOMER_LST001);
	m_ColumTitle[eLanguage ] =			LoadStringById(IDS_CUSTOMER_LST003);
	m_ColumTitle[eTimeZone   ] =		LoadStringById(IDS_CUSTOMER_LST002);
	//m_ColumTitle[eDescription   ] =		LoadStringById(IDS_CUSTOMER_LST004);
	//m_ColumTitle[eURL   ] =		LoadStringById(IDS_CUSTOMER_LST005);

	//m_lscCustomer.SetExtendedStyle(m_lscCustomer.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lscCustomer.SetExtendedStyle(m_lscCustomer.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	//m_lscCustomer.InsertColumn(eCheck,			m_ColumTitle[eCheck], LVCFMT_LEFT, 22);
	m_lscCustomer.InsertColumn(eCustomer,		m_ColumTitle[eCustomer], LVCFMT_LEFT, 80);
	m_lscCustomer.InsertColumn(eLanguage,		m_ColumTitle[eLanguage], LVCFMT_LEFT, 70);
	m_lscCustomer.InsertColumn(eTimeZone,		m_ColumTitle[eTimeZone], LVCFMT_LEFT, 100);
	//m_lscCustomer.InsertColumn(eLanguage,		m_ColumTitle[eDescription], LVCFMT_LEFT, 150);
	//m_lscCustomer.InsertColumn(eLanguage,		m_ColumTitle[eURL], LVCFMT_LEFT, 150);

	//m_lscCustomer.InitHeader(IDB_LIST_HEADER);

}

void CCustomerListDlg::SetList()
{

	m_listCustomerInfo.RemoveAll();
	if(!CCopModule::GetObject()->GetCustomerList(m_listCustomerInfo)){
		UbcMessageBox("Get Customer failed");
		return;
	}

	if(!m_strCustomerName.IsEmpty() && m_strCustomerName != "UNKNOWN" 
		&& this->m_szLoginID != "super" ){
		// m_strCustomerName 는 오직 OK 를 눌렀을때만 값이 채워지므로 SetList에서 m_strCustomerName 가 아닌 경우는
		// 오직 한가지 경우,  DLG 로 값이 넘어온 경우 뿐이다.
		OnBnClickedOk();
		return ;
	}


	m_lscCustomer.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_listCustomerInfo.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SCustomerInfo info = m_listCustomerInfo.GetNext(pos);
		// eCheck, eCustomer, eTimeZone, eLanguage, eDescription, eURL, eEnd
		
		m_lscCustomer.InsertItem(nRow, "");
		m_lscCustomer.SetItemText(nRow, eCustomer, info.customerId);
		m_lscCustomer.SetItemText(nRow, eLanguage    , info.language);
		if(info.gmt != 9999){
			char buf[20];
			if(info.gmt<0){	
				sprintf(buf,"GMT%d min", (info.gmt));
			}else{
				sprintf(buf,"GMT+%d min", (info.gmt));
			}
			m_lscCustomer.SetItemText(nRow, eTimeZone    , buf);
		}else{
			m_lscCustomer.SetItemText(nRow, eTimeZone    , _T(""));
		}
		//m_lscCustomer.SetItemText(nRow, eDescription    , info.description);
		//m_lscCustomer.SetItemText(nRow, eURL    , info.url);

		m_lscCustomer.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
	m_lscCustomer.SetRedraw(TRUE);

}


