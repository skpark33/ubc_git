#pragma once

//#include "resource.h"
#ifndef IDD_AUTHCHECK
#define IDD_AUTHCHECK	25003
#endif//IDD_AUTHCHECK

// CAuthCheckDlg dialog
class AFX_EXT_CLASS CAuthCheckDlg : public CDialog
{
	DECLARE_DYNAMIC(CAuthCheckDlg)

	CString m_szCustomerId;
public:
	CAuthCheckDlg(CString strCustomerId, CWnd* pParent = NULL);   // standard constructor
	virtual ~CAuthCheckDlg();

// Dialog Data
	enum { IDD = IDD_AUTHCHECK };

	CString m_szKey;
	CString m_szPasswd;
	CString m_szHostId;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedOk();
};
