#include "stdafx.h"
#include "atlenc.h"

#include "resource.h"
#include "copmodule.h"
#include "copcomdata.h"
#include "ubccopcommon.h"
//#include "CustomerInfoDlg.h"
#include "CustomerListDlg.h"

#include "cci/libWorld/cciGUIORBThread.h"
#include "common/TraceLog.h"

#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libWorld/ciWorld.h>
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciAny.h>
#include <cci/libValue/cciBoolean.h>
#include <cci/libPfUtil/cciDSUtil.h>
//#include <UTIL/InternalIPClient/InternalIPClient.h>
#include <common/libCommon/ubcPermition.h>
#include "common/libCommon/ubcIni.h"

ciSET_DEBUG(1, "UBCStudioEE");

/////////////////////////////////////////
// CDirtyFlag class
bool CDirtyFlag::IsDirty(const char* name) 
{ 
	DIRTY_MAP::iterator itr = _map.find(name); 
	if(itr != _map.end()) {
		return bool(itr->second);
	}
	return false;
}
void CDirtyFlag::SetDirty(const char* name, bool flag) 
{ 
	_map.insert(DIRTY_MAP::value_type(name,flag)); 
}

/////////////////////////////////////////
// functions
int CCopModule_SetValue(std::list<std::string>& sList, CCI::CCI_StringList& tList){
	cciStringList ccistr;
	ccistr.set(tList);
	sList = ccistr.getStringList();

	return sList.size();
}

int CCopModule_SetValue(CStringList& sList, CCI::CCI_StringList& tList){
	ciStringList cistr;
	cciStringList ccistr;

	sList.RemoveAll();
	ccistr.set(tList);
	cistr = ccistr.getStringList();

	int iRet = 0;
	for(ciStringList::iterator iter = cistr.begin(); iter != cistr.end(); iter++, iRet++){
		sList.AddTail((*iter).c_str());
	}

	return iRet;
}

static const string inKey = 
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789";
static const string outKey1 = 
             "0123456789"
             "abcdefghijklmnopqrstuvwxyz"
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
static const string outKey2 = 
             "9876543210"
             "zyxwvutsrqponmlkjihgfedcba"
             "ZYXWVUTSRQPONMLKJIHGFEDCBA";

string simple_encode(string const& inval) 
{
	int in_len = inval.size();
	string ret;

	for(int i=0;i<in_len;i++)
	{
		const char ele = inval[i];
		int idx = int(inKey.find(ele));
		if(idx>=0){
			ret += outKey2[idx];
			ret += outKey1[idx];
		}else{
			ret += ele;
		}
	}
	return ret;
}

string simple_decode(string const& inval) 
{
	int in_len = inval.size();
	string ret;

	for(int i=0;i<in_len;i=i+2)
	{
		const char ele = inval[i];
		int idx = int(outKey2.find(ele));
		if(idx>=0){
			ret += inKey[idx];
		}else{
			ret += ele;
		}
	}
	return ret;
}


int GetLoginFailCount(CString& id)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	char buf1[1024];
	CString strPath, szTemp;
	strPath.Format("%s%sdata\\%s", szDrive, szPath, UBCVARS_INI);

	ciString enc_id = 	simple_encode(ciString(id));

	::GetPrivateProfileString(_T("ENTRY1"), enc_id.c_str(), _T("0"), buf1, 1024, strPath);

	return atoi(buf1);
}

unsigned long GetLastLoginFailTime(CString& id)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	char buf1[1024];
	CString strPath, szTemp;
	strPath.Format("%s%sdata\\%s", szDrive, szPath, UBCVARS_INI);

	ciString enc_id = 	simple_encode(ciString(id));

	::GetPrivateProfileString(_T("ENTRY2"), enc_id.c_str(), _T("0"), buf1, 1024, strPath);

	return strtoul(buf1,NULL,10);
}

void SetLoginFailCount(CString& id, int counter)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString strPath, szTemp;
	strPath.Format("%s%sdata\\%s", szDrive, szPath, UBCVARS_INI);

	char buf1[1024];
	sprintf(buf1,"%u", counter);
	
	time_t now = time(NULL);

	char buf2[1024];
	if(counter > 0){
		sprintf(buf2,"%u", now);
	}else{
		sprintf(buf2,"0");
	}

	ciString enc_id = 	simple_encode(ciString(id));

	::WritePrivateProfileString(_T("ENTRY1"), enc_id.c_str(), buf1,strPath);
	::WritePrivateProfileString(_T("ENTRY2"), enc_id.c_str(), buf2, strPath);

}
/////////////////////////////////////////
// class CCopModule
CCopModule* CCopModule::m_pModule = NULL;

CCopModule::CCopModule()
{
	InitModule();
}

CCopModule::~CCopModule()
{
	ReleaseModule();
}

CCopModule* CCopModule::GetObject()
{
	if(!m_pModule){
		m_pModule = new CCopModule;
	}
	return m_pModule;
}

void CCopModule::Release()
{
	if(m_pModule){
		delete m_pModule;
	}
	m_pModule = NULL;
}

void CCopModule::InitModule()
{
	m_bIsInitORB = FALSE;
	m_pOrbThread = NULL;
	m_Authority = eSiteUnkown;
}

void CCopModule::ReleaseModule()
{
	CCopComData::Release();
	DestroyORB();
}

BOOL CCopModule::CopErrCheck(cciReply& reply, bool bShowMsgBox/*=true*/)
{
	ciLong errCode=0;
	reply.getItem("ERROR_CODE", errCode);
	ciString errMsg;
	reply.getItem("ERROR_MSG", errMsg);

	if(errCode < 0 )
	{
		TraceLog(("ERROR_CODE [%d]", errCode));
		TraceLog(("ERROR_MSG [%s]", errMsg.c_str()));

		// Display this message  :      skpark 20100621
		CString strMsg;
		strMsg.Format(_T("%s[code:%d]"), errMsg.c_str(), errCode);
		if(bShowMsgBox) UbcMessageBox(strMsg, MB_ICONERROR);
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::InitORB(LPCTSTR szIP, int nPort)
{
	if(m_bIsInitORB) return TRUE;

	m_bIsInitORB = FALSE;

	if(m_pOrbThread){
		m_pOrbThread->destroy();
		delete m_pOrbThread;
		m_pOrbThread = NULL;
	}
/*
	InternalIPClient ipClient(szIP, nPort);
	ciBoolean bRet = ipClient.init(__argc, __argv);

	if (bRet == ciFalse){
		return FALSE;
	}
*/
	// ACE init
	int nRet = ACE::init();

	if (!m_pOrbThread){
		m_pOrbThread = new cciGUIORBThread(__argc,__argv);
		if(m_pOrbThread != NULL)
			m_pOrbThread->start();
	}

	// ciWorld Init
	ciWorld* pWorld = ciWorld::getWorld();

	if(pWorld->init(__argc, __argv) == ciFalse) {
		TraceLog(("cciORBWorld::init() is failed"));
		return FALSE;
	}

	ciDebug::setDebugOn();
	ciWorld::ylogOn();
	ciEnv::defaultEnv(true,"utv1");

	cciDSUtil::onDS();
	cciDSUtil* aDsUtil = cciDSUtil::getInstance();
	aDsUtil->init();

	if(ciWorld::hasWatcher() == ciTrue) {
		TraceLog(("ciWorld::hasWatcher() is true"));
		return FALSE;
	}

	for(int i=0; i<30 ; i++)
	{
		Sleep(1000);
		if(CheckORB())
		{
			m_bIsInitORB = TRUE;
			break;
		}
	}

	TraceLog(("InitORB() is successed"));
	return TRUE;
}

void CCopModule::DestroyORB()
{
	if(m_pOrbThread)
	{
		//m_pOrbThread->stop();
		//m_pOrbThread->join();
		m_pOrbThread->destroy();
		delete m_pOrbThread;
		m_pOrbThread = NULL;
	}
	TraceLog(("DestroyORB() is successed"));

	m_bIsInitORB = FALSE;
}

BOOL CCopModule::CheckORB()
{
	cciCall aCall("get");
	cciEntity aEntity("PM=*");
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}
const char* CCopModule::GetMyIp()
{
	static string s_myIp = "";

	if(!s_myIp.empty() && s_myIp != "0.0.0.0"){
		return s_myIp.c_str();
	}

	char a_hostname[128];
	char a_hostip[32];
	::memset(a_hostname, 0, sizeof(a_hostname));
	::memset(a_hostip, 0, sizeof(a_hostip));

	BOOL retval = true;
	if(gethostname(a_hostname, 127) != 0)
	{
		strcpy(a_hostip, "0.0.0.0");
		retval = false;
	}
	else
	{
		hostent *p;
		p = gethostbyname(a_hostname);
		if(p == NULL)
		{
			strcpy(a_hostip, "0.0.0.0");
			retval = false;
		}
		else
		{
			strcpy(a_hostip, inet_ntoa(*(struct in_addr *) p->h_addr));
		}
	}
	
	s_myIp = a_hostip;
	return s_myIp.c_str();
}

int CCopModule::Login(CString& strSite, CString strID, CString strPW, 
					  CString& strValidationDate, CString& strCustomer, CString& strSpecialRole)
{
//	UbcMessageBox("1 CCopModule::Login : " + strSite);
	int loginFailCount = GetLoginFailCount(strID);
	time_t now = time(NULL);
	unsigned long lastLoginFailTime = GetLastLoginFailTime(strID);
	
	if(loginFailCount >= 5 && (lastLoginFailTime && now - lastLoginFailTime < 60*30)){
		CString err;
		err.Format(LoadStringById(IDS_LOGINDLG_MSG013),  (60*30)-(now-lastLoginFailTime));
		UbcMessageBox(err, MB_ICONERROR);
		return -1;
	}
	if(lastLoginFailTime && now - lastLoginFailTime >= 60*30){
		SetLoginFailCount(strID,0);
		loginFailCount = 0;
		lastLoginFailTime = 0;
	}


	cciCall aCall("login");
	cciEntity aEntity("PM=1/Site=%s/User=%s", strSite, strID);
	aCall.setEntity(aEntity);

	ciString strPasswd = strPW;
	aCall.addItem("password", strPasswd);
	//aCall.addItem("via", _T("MANAGER")); // 로그남기기용

	CString via = GetMyIp();
	aCall.addItem("via", via); // 로그남기기용 자신의 IP 가 들어감

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call())
		{
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return -1;
		}

		int nResult = eSiteUnkown;

		//if(aCall.hasMore()) {
		while(aCall.hasMore())
		{
			cciReply reply;

			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				return eSiteUnkown;
			}

			ciLong errCode=0;
			reply.getItem("ERROR_CODE", errCode);
			if(errCode== -3 ) {
				SetLoginFailCount(strID,++loginFailCount);
				if(loginFailCount >= 5){
					CString err;
					err.Format(LoadStringById(IDS_LOGINDLG_MSG013), 60*10);
					UbcMessageBox(err, MB_ICONERROR);
					return -1;
				}
			}else{
				SetLoginFailCount(strID,0);
			}

			if(errCode== -5){
				UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG012), MB_ICONERROR);
				return -1;
			}
			
			if(!CopErrCheck(reply)){
				return -1;
			}

			reply.unwrap();

			// 패스워드가 오랜되었다는 경고
			reply.getItem("ERROR_CODE", errCode);
			if(errCode== -6){
				UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG011), MB_ICONWARNING);
			}			

			ciLong userType;
			if(!reply.getItem("userType", userType)) continue;
			nResult = userType;

			ciTime validationDate;
			reply.getItem("validationDate", validationDate);
			
			ciString version;
			reply.getItem("version", version);
			m_strVersion = version.c_str();
			TraceLog(("Server Version = [%s]", m_strVersion));

			ciString specialRole;
			reply.getItem("specialRole", specialRole);
			strSpecialRole = specialRole.c_str();
			TraceLog(("Server Version = [%s]", strSpecialRole));

			//ciTime lastPWChangeTime;
			//reply.getItem("lastPWChangeTime", lastPWChangeTime);

			CTime tmValidationDate(validationDate.getTime());
			strValidationDate = tmValidationDate.Format(_T("%Y/%m/%d %H:%M:%S"));

			//CTime tmLastPWChangeTime(lastPWChangeTime.getTime());
			//strLastPWChangeTime = tmLastPWChangeTime.Format(_T("%Y/%m/%d %H:%M:%S"));

			TraceLog(("User ValidationDate [%s]", strValidationDate));

			ciString siteId;
			reply.getItem("siteId", siteId);
			TraceLog(("%s", siteId.c_str()));
			strSite = siteId.c_str();

			// 0001408: Customer 정보 입력을 받는다.
			ciString customer;
			reply.getItem("customer", customer);
			TraceLog(("%s", customer.c_str()));
			strCustomer = customer.c_str();

			// Customer가 없는 경우 선택하는 다이얼로그를 팝업한다.
			// skpark 2013.3.25  Customer관리화면을 변경함
			//CCustomerInfoDlg dlg(strCustomer);
			CCustomerListDlg dlg(strCustomer, strID, userType);
			if(dlg.DoModal() == IDOK)
			{
				strCustomer = dlg.GetSelectCustomerName();
				int nUserTimezoneMinute = dlg.GetSelectCustomerTimezone();

				if(nUserTimezoneMinute >= -24*60 && nUserTimezoneMinute <= 24*60)
				{
					CString strTimeZoneName;
					LONG nMinute = 9999;
					if( GetTimeZoneMinute(strTimeZoneName, nMinute) &&
						nUserTimezoneMinute != nMinute  )
					{
						int nOsTimezone = nMinute * (nMinute>=0 ? 1 : -1);
						CString strOsTimezone;
						strOsTimezone.Format(_T("(UTC%c%02d:%02d)")
										, (nMinute>=0 ? '-' : '+')
										, nOsTimezone/60
										, nOsTimezone-(nOsTimezone/60)*60
										);

						int nUserTimezone = nUserTimezoneMinute * (nUserTimezoneMinute>=0 ? 1 : -1);
						CString strUserTimezone;
						strUserTimezone.Format(_T("(UTC%c%02d:%02d)")
										, (nUserTimezoneMinute>=0 ? '-' : '+')
										, nUserTimezone/60
										, nUserTimezone-(nUserTimezone/60)*60
										);

						CString strMsg;
						strMsg.Format(LoadStringById(IDS_COPCOMMON_STR003)
									, strOsTimezone
									, strUserTimezone
									);

						UbcMessageBox(strMsg);
						return -3;
					}
				}

				if(!dlg.GetSelectCustomerLanguage().IsEmpty())
				{
					CString strOsLocaleName = GetLocaleName();

					TraceLog(("strOsLocaleName : %s", strOsLocaleName));
					TraceLog(("GetSelectCustomerLanguage() : %s", dlg.GetSelectCustomerLanguage()));
					TraceLog(("CompareNoCase : %d", strOsLocaleName.CompareNoCase(dlg.GetSelectCustomerLanguage())));

					if(!strOsLocaleName.IsEmpty() && strOsLocaleName.CompareNoCase(dlg.GetSelectCustomerLanguage()) != 0)
					{
						CString strMsg;
						strMsg.Format(LoadStringById(IDS_COPCOMMON_STR004)
									, strOsLocaleName
									, dlg.GetSelectCustomerLanguage()
									);

						UbcMessageBox(strMsg);
						return -3;
					}
				}
			}

			ciString siteList;	// 2010.10.14 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
			ciString hostList;

			if(strSite != _T("Default") || nResult != 1)
			{
				reply.getItem("siteList", siteList);
				reply.getItem("hostList", hostList);

				TraceLog(("siteList : %s", siteList.c_str()));
				TraceLog(("hostList : %s", hostList.c_str()));

				cciStringList childSitelist;

				// 사용자 계정의 경우 해당사이트만 볼수있게한다. 하위사이트에 대한 권한이 없음
				if(eSiteUser == userType) childSitelist.addItem(siteId.c_str());
				else                      GetChildSite(strCustomer, siteId.c_str(), childSitelist);

				cciStringList cciSiteList(siteList.c_str());

				for(int i=0; i<cciSiteList.length(); i++)
				{
					// 사용자 계정의 경우 해당사이트만 볼수있게한다. 하위사이트에 대한 권한이 없음
					if(eSiteUser == userType) childSitelist.addItem(cciSiteList[i].c_str());
					else                      GetChildSite(strCustomer, cciSiteList[i].c_str(), childSitelist);
				}

				TraceLog(("siteList : %s", childSitelist.toString()));
				siteList = childSitelist.toString();
			}

			// 2010.10.14 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
			ubcPermition::getInstance()->init(strID, userType, siteList.c_str(), hostList.c_str());
			
			m_strLoginId = strID;
			m_strSiteId = strSite;
			m_Authority = userType;

			return nResult;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return -2;
	}

	return eSiteUnkown;
}

BOOL CCopModule::GetUser(SUserInfo& info, UserInfoList& list, bool bFilter/*=TRUE*/)
{

	CString szSite, szUser;

	if(info.siteId.IsEmpty())
		szSite = "*";
	else
		szSite = info.siteId;

	if(info.userId.IsEmpty())
		szUser = "*";
	else
		szUser = info.userId;

	TraceLog(("GetUser : %s", szUser));

	cciCall aCall("get");
	cciEntity aEntity("PM=*/Site=%s/User=%s", szSite, szUser);
	if(bFilter) aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);
	aCall.addItem("siteId", nullAny);
	aCall.addItem("userId", nullAny);
	aCall.addItem("password", nullAny);
	aCall.addItem("userName", nullAny);
	aCall.addItem("sid", nullAny);
	aCall.addItem("userType", nullAny);
	aCall.addItem("email", nullAny);
	aCall.addItem("phoneNo", nullAny);
	aCall.addItem("useSms", nullAny);
	aCall.addItem("useEmail", nullAny);
	aCall.addItem("probableCauseList", nullAny);
	aCall.addItem("siteList", nullAny);
	aCall.addItem("hostList", nullAny);
	aCall.addItem("roleId", nullAny);
	aCall.addItem("validationDate", nullAny);
	aCall.addItem("lastLoginTime", nullAny);
	aCall.addItem("lastPWChangeTime", nullAny);
	aCall.wrap();

	//if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString mgrId, siteId, userId, password, userName, sid, email, phoneNo;
			ciLong userType;
			ciBoolean useSms, useEmail;
			cciStringList probableCauseList, siteList, hostList;
			ciString roleId;
			ciTime validationDate;
			ciTime lastLoginTime;
			ciTime lastPWChangeTime;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			reply.getItem("userId", userId);
			reply.getItem("password", password);
			reply.getItem("userName", userName);
			reply.getItem("sid", sid);
			reply.getItem("userType", userType);
			reply.getItem("email", email);
			reply.getItem("phoneNo", phoneNo);
			reply.getItem("useSms", useSms);
			reply.getItem("useEmail", useEmail);
			ciBoolean bResult3 = reply.getItem("probableCauseList", probableCauseList);
			ciBoolean bResult1 = reply.getItem("siteList", siteList);
			ciBoolean bResult2 = reply.getItem("hostList", hostList);
			reply.getItem("roleId", roleId);
			reply.getItem("validationDate", validationDate);
			reply.getItem("lastLoginTime", lastLoginTime);
			reply.getItem("lastPWChangeTime", lastPWChangeTime);

			if(userId.length() == 0)
				continue;

			SUserInfo uiTemp;
			uiTemp.mgrId = mgrId.c_str();
			uiTemp.siteId = siteId.c_str();
			uiTemp.userId = userId.c_str();
			uiTemp.password = password.c_str();
			uiTemp.userName = userName.c_str();
			uiTemp.sid = sid.c_str();
			uiTemp.userType = userType;
			uiTemp.email = email.c_str();
			uiTemp.phoneNo = phoneNo.c_str();
			uiTemp.useSms = useSms;
			uiTemp.useEmail = useEmail;
			uiTemp.probableCauseList = probableCauseList.toString();
			uiTemp.siteList = siteList.toString();
			uiTemp.hostList = hostList.toString();
			uiTemp.roleId = roleId.c_str();
			uiTemp.validationDate = validationDate.getTime();
			uiTemp.lastLoginTime = lastLoginTime.getTime();
			uiTemp.lastPWChangeTime = lastPWChangeTime.getTime();

			if(eSiteUser == m_Authority)
			{
				uiTemp.bReadOnly = (m_strSiteId != siteId.c_str() || m_strLoginId != userId.c_str());
			}

			if(!uiTemp.bReadOnly && userType < m_Authority)
			{
				uiTemp.bReadOnly = true;
			}
			else if(!uiTemp.bReadOnly && userId != "super")
			{
				ciStringSet sitePermitionList;
				uiTemp.bReadOnly = true;
				if(ubcPermition::getInstance()->getSiteSet(sitePermitionList))
				{
					ciStringSet::iterator itr = sitePermitionList.find(siteId);
					if(itr != sitePermitionList.end())
					{
						ciString tmp = (*itr);
						uiTemp.bReadOnly = tmp.empty();
					}
				}
			}

			list.AddTail(uiTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::AddUser(SUserInfo& info)
{
	cciCall aCall("create");
	cciEntity aEntity("PM=*/Site=%s/User=%s", info.siteId, info.userId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId", info.mgrId);
//	aCall.addItem("siteId", info.siteId);
//	aCall.addItem("userId", info.userId);
	aCall.addItem("password", info.password);
	aCall.addItem("userName", info.userName);
	aCall.addItem("sid", info.sid);
	aCall.addItem("userType", (long)info.userType);
	aCall.addItem("email", info.email);
	aCall.addItem("phoneNo", info.phoneNo);
	aCall.addItem("useSms", info.useSms);
	aCall.addItem("useEmail", info.useEmail);
	aCall.addItem("probableCauseList", info.probableCauseList);
	aCall.addItem("siteList", info.siteList);
	aCall.addItem("hostList", info.hostList);
	aCall.addItem("roleId", info.roleId);
	aCall.addItem("validationDate", ciTime(info.validationDate.GetTime()));
	aCall.addItem("lastLoginTime", ciTime());

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			ciString err = aCall.getError();
			if(!err.empty())
			{
				UbcMessageBox(err.c_str());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		if(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				return FALSE;
			}
			ciLong errCode=0;
			reply.getItem("ERROR_CODE", errCode);
			ciString errMsg;
			reply.getItem("ERROR_MSG", errMsg);
			if(errCode < 0 )
			{
				if(errMsg == "STRONG PASSWORD ERROR"){
					UbcMessageBox(IDS_LOGINDLG_MSG010);
				}else{
					UbcMessageBox(errMsg.c_str());
				}
				return FALSE;
			}
			//if(!CopErrCheck(reply)) return FALSE;

			CString szName;
			ciString cisTemp;
			cciEntity& aEntity = reply.getEntity();
			TraceLog(("aEntity=%s\n", aEntity.toString()));

			for(int i = 0; i < aEntity.length(); i++){
				szName = aEntity.getClassName(i);
				
				if(szName == "PM"){
					cisTemp = aEntity.getClassValue(i);
					info.mgrId = cisTemp.c_str();
				}
				else if(szName == "Site"){
					cisTemp = aEntity.getClassValue(i);
					info.siteId = cisTemp.c_str();
				}
				else if(szName == "User"){
					cisTemp = aEntity.getClassValue(i);
					info.userId = cisTemp.c_str();
				}
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return SetUser(info);
}

BOOL CCopModule::DelUser(LPCTSTR szSiteID, LPCTSTR szUserID)
{
	if( szSiteID == NULL || _tcslen(szSiteID) == 0 ||
		szUserID == NULL || _tcslen(szUserID) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove");  // remove user OK
	cciEntity aEntity("PM=*/Site=%s/User=%s", szSiteID, szUserID);
	aCall.setEntity(aEntity);

	if(atof(m_strVersion) >= 5.9) {
		aCall.addItem("trashId_", m_strLoginId);
		aCall.wrap();
	}

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetUser(SUserInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/User=%s", info.siteId, info.userId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId", info.mgrId);
//	aCall.addItem("siteId", info.siteId);
//	aCall.addItem("userId", info.userId);
	if(info.passwordChanged){
		aCall.addItem("password", info.password);
		info.passwordChanged = false;
	}
	aCall.addItem("userName", info.userName);
	aCall.addItem("sid", info.sid);
	aCall.addItem("userType", (ciLong)info.userType);
	aCall.addItem("email", info.email);
	aCall.addItem("phoneNo", info.phoneNo);
	aCall.addItem("useSms", info.useSms);
	aCall.addItem("useEmail", info.useEmail);

	cciStringList siteListBuf, hostListBuf, causeListBuf;

	causeListBuf.fromString(info.probableCauseList);
	siteListBuf.fromString(info.siteList);
	hostListBuf.fromString(info.hostList);

	//aCall.addItem("probableCauseList", info.probableCauseList);
	//aCall.addItem("siteList", info.siteList);   
	//aCall.addItem("hostList", info.hostList);

	aCall.addItem("probableCauseList", causeListBuf.get());
	aCall.addItem("siteList", siteListBuf.get());   
	aCall.addItem("hostList", hostListBuf.get());

	aCall.addItem("roleId", info.roleId);
	aCall.addItem("validationDate", ciTime(info.validationDate.GetTime()));
	aCall.addItem("lastLoginTime", ciTime(info.lastLoginTime.GetTime()));
	aCall.addItem("lastPWChangeTime", ciTime(info.lastPWChangeTime.GetTime()));

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			ciString err = aCall.getError();
			if(!err.empty()) {
				UbcMessageBox(err.c_str());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
		if(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				UbcMessageBox("Failed : cciCall::getReply()");
				return FALSE;
			}
			ciLong errCode=0;
			reply.getItem("ERROR_CODE", errCode);
			ciString errMsg;
			reply.getItem("ERROR_MSG", errMsg);
			if(errCode < 0 )
			{
				if(errMsg == "STRONG PASSWORD ERROR"){
					UbcMessageBox(IDS_LOGINDLG_MSG010);
				}else{
					UbcMessageBox(errMsg.c_str());
				}
				return FALSE;
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

CString CCopModule::UserType(int nType)
{
	switch(nType)
	{
	case eSiteAdmin:	return _T("Super admin");
	case eSiteManager:	return _T("Site manager");
	case eSiteUser:		return _T("Site user");
	}
	return _T("Unkown");
}

BOOL CCopModule::GetMenuAuthList(CString strUserId, CString strCustomer, MenuAuthInfoList& list)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("OD=*/MenuAuth=*");
	aCall.setEntity(aEntity);

	CString strWhere;
	strWhere.Format(_T("menuId in (select menuId from ubc_rolemap where roleId = (select roleId from ubc_user where userId = '%s') and customer = '%s') and customer = '%s' and menuType = 'S'")
				  , strUserId
				  , strCustomer
				  , strCustomer
				  );
	AddItem(aCall, "whereClause", strWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString		mgrId;			// manager process id
			ciString		menuId;			// menuId 
			ciString		menuName;		// 메뉴명
			ciString		menuType;		// S:솔루션, W: 웹
			ciBoolean		cAuth;			// create	
			ciBoolean		rAuth;			// read
			ciBoolean		uAuth;			// update	
			ciBoolean		dAuth;			// delete
			ciBoolean		aAuth;			// approve

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("menuId", menuId);
			reply.getItem("menuName", menuName);
			reply.getItem("menuType", menuType);
			reply.getItem("cAuth", cAuth);
			reply.getItem("rAuth", rAuth);
			reply.getItem("uAuth", uAuth);
			reply.getItem("dAuth", dAuth);
			reply.getItem("aAuth", aAuth);

			SMenuAuthInfo temp;
			temp.mgrId = mgrId.c_str();
			temp.menuId = menuId.c_str();
			temp.menuName = menuName.c_str();
			temp.menuType = menuType.c_str();
			temp.cAuth = cAuth;
			temp.rAuth = rAuth;
			temp.uAuth = uAuth;
			temp.dAuth = dAuth;
			temp.aAuth = aAuth;

			list.AddTail(temp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetSite(SSiteInfo& info, SiteInfoList& list, LPCTSTR szWhere /*= NULL*/, bool bFilter/*=TRUE*/)
{
	CString szSite;
	if(info.siteId.IsEmpty())
		szSite = "*";
	else
		szSite = info.siteId;

	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s", szSite);
	if(bFilter) aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);
	aCall.addItem("siteId", nullAny);
	aCall.addItem("siteName", nullAny);
	aCall.addItem("parentId", nullAny);
	aCall.addItem("comment1", nullAny);
	aCall.addItem("phoneNo1", nullAny);
	aCall.addItem("ceoName", nullAny);
	aCall.addItem("phoneNo2", nullAny);
	aCall.addItem("comment2", nullAny);
	aCall.addItem("businessType", nullAny);
	aCall.addItem("businessCode", nullAny);
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
		
		ciStringList ciTemp;
		cciStringList cciTemp;

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString mgrId, siteId, siteName, parentId;
			ciString businessType, businessCode;
			//ciString chainNo, businessType, businessCode, shopOpenTime, shopCloseTime;
			//CCI::CCI_StringList holiday;
			ciString comment1;	// 거점 주소
			ciString phoneNo1;	// 거점 전화번호
			ciString ceoName;	// 담당자
			ciString phoneNo2;	// 담당자 연락처
			ciString comment2;	// 기타

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			reply.getItem("siteName", siteName);
			//reply.getItem("chainNo", chainNo);
			reply.getItem("businessType", businessType);
			reply.getItem("businessCode", businessCode);
			//reply.getItem("shopOpenTime", shopOpenTime);
			//reply.getItem("shopCloseTime", shopCloseTime);
			//reply.getItem("holiday", holiday);
			reply.getItem("parentId", parentId);
			reply.getItem("comment1", comment1);
			reply.getItem("phoneNo1", phoneNo1);
			reply.getItem("ceoName", ceoName);
			reply.getItem("phoneNo2", phoneNo2);
			reply.getItem("comment2", comment2);

			if(siteId.length() == 0)
				continue;

//			cciTemp.set(holiday);
//			ciTemp = cciTemp.getStringList();

			SSiteInfo siTemp;
			siTemp.mgrId = mgrId.c_str();
			siTemp.siteId = siteId.c_str();
			siTemp.siteName = siteName.c_str();
			//siTemp.chainNo = chainNo.c_str();
			siTemp.businessType = businessType.c_str();
			siTemp.businessCode = businessCode.c_str();
			//siTemp.shopOpenTime = shopOpenTime.c_str();
			//siTemp.shopCloseTime = shopCloseTime.c_str();
			//siTemp.holiday = ciTemp;
			siTemp.parentId = parentId.c_str();
			siTemp.comment1 = comment1.c_str();
			siTemp.phoneNo1 = phoneNo1.c_str();
			siTemp.ceoName = ceoName.c_str();
			siTemp.phoneNo2 = phoneNo2.c_str();
			siTemp.comment2 = comment2.c_str();
			cciTemp.clear();

			if(eSiteUser == m_Authority)
			{
				siTemp.bReadOnly = true;
			}
			else
			{
				ciStringSet sitePermitionList;
				siTemp.bReadOnly = (m_strSiteId != siteId.c_str() && this->m_strLoginId != "super");
				if(ubcPermition::getInstance()->getSiteSet(sitePermitionList))
				{
					ciStringSet::iterator itr = sitePermitionList.find(siteId);
					if(itr != sitePermitionList.end())
					{
						ciString tmp = (*itr);
						siTemp.bReadOnly = tmp.empty();
					}
				}
			}

			list.AddTail(siTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::AddSite(CString strCustomer, SSiteInfo& info)
{
	cciCall aCall("createSite");
	cciEntity aEntity("OD=OD");
	aCall.setEntity(aEntity);

	//AddItem(aCall, "mgrId", info.mgrId);
	aCall.addItem("pmId", info.mgrId);
	aCall.addItem("siteId", info.siteId);
	aCall.addItem("siteName", info.siteName);
	//aCall.addItem("chainNo", info.chainNo);
	aCall.addItem("businessType", info.businessType);
	aCall.addItem("businessCode", info.businessCode);
	//aCall.addItem("shopOpenTime", info.shopOpenTime);
	//aCall.addItem("shopCloseTime", info.shopCloseTime);
	aCall.addItem("parentId", info.parentId);
	aCall.addItem("comment1", info.comment1);
	aCall.addItem("phoneNo1", info.phoneNo1);
	aCall.addItem("ceoName", info.ceoName);
	aCall.addItem("phoneNo2", info.phoneNo2);
	aCall.addItem("comment2", info.comment2);
	aCall.addItem("franchizeType", strCustomer);

	//if(!info.holiday.empty()){
	//	AddItem(aCall, "holiday", info.holiday);
	//}else{
	//	info.holiday.push_back("NOSET");
	//	AddItem(aCall, "holiday", info.holiday);
	//}

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		if(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				return FALSE;
			}

			if(!CopErrCheck(reply)) return FALSE;

			CString szName;
			ciString cisTemp;
			cciEntity& aEntity = reply.getEntity();
			TraceLog(("aEntity=%s\n", aEntity.toString()));

			for(int i = 0; i < aEntity.length(); i++){
				szName = aEntity.getClassName(i);
				
				if(szName == "PM"){
					cisTemp = aEntity.getClassValue(i);
					info.mgrId = cisTemp.c_str();
				}
				else if(szName == "Site"){
					cisTemp = aEntity.getClassValue(i);
					info.siteId = cisTemp.c_str();
				}
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::DelSite(LPCTSTR szSiteID)
{
	if( szSiteID == NULL || _tcslen(szSiteID) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove");   // remove site OK
	cciEntity aEntity("PM=*/Site=%s", szSiteID);
	aCall.setEntity(aEntity);

	if(atof(m_strVersion) >= 5.9) {
		aCall.addItem("trashId_", m_strLoginId);
		aCall.wrap();
	}

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		if(!_ApproveErrorCheck(aCall)) return FALSE;


	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}
	const char* errMsg = aCall.getErrorFromReply();
	if(errMsg) {
		TraceLog(("ERROR : %s", errMsg));
	}else{
		TraceLog(("SUCCEED"));
	}
	

	return TRUE;
}

BOOL CCopModule::SetSite(SSiteInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s", info.siteId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId", info.mgrId);
//	aCall.addItem("siteId", info.siteId);
	aCall.addItem("siteName", info.siteName);
	//aCall.addItem("chainNo", info.chainNo);
	aCall.addItem("businessType", info.businessType);
	aCall.addItem("businessCode", info.businessCode);
	//aCall.addItem("shopOpenTime", info.shopOpenTime);
	//aCall.addItem("shopCloseTime", info.shopCloseTime);
	aCall.addItem("parentId", info.parentId);
	aCall.addItem("comment1", info.comment1);
	aCall.addItem("phoneNo1", info.phoneNo1);
	aCall.addItem("ceoName", info.ceoName);
	aCall.addItem("phoneNo2", info.phoneNo2);
	aCall.addItem("comment2", info.comment2);

	//if(!info.holiday.empty()){
	//	AddItem(aCall, "holiday", info.holiday);
	//}else{
	//	info.holiday.push_back("NOSET");
	//	AddItem(aCall, "holiday", info.holiday);
	//}

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}
	TraceLog(("%s succeed", aCall.toString()));
	return TRUE;
}

BOOL CCopModule::DummyCall(LPCTSTR szCall, LPCTSTR szEntity, LPCTSTR szWhere)
{
	cciCall aCall(szCall);
	cciEntity aEntity(szEntity);
	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try
	{
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call())
		{
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}
	catch(CORBA::Exception& ex)
	{
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetHostForAll(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere, bool bFilter/*=TRUE*/)
{
	if(!pCall) return FALSE;

	//cciCall aCall("bulkget");
	pCall->setDirective("bulkget");
	//cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	cciEntity aEntity("PM=*/Site=%s/HostView=%s", szSite, szHost);
	if(bFilter) pCall->setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	pCall->setEntity(aEntity);

	cciAny nullAny;
	pCall->addItem("mgrId", nullAny);
	pCall->addItem("siteId", nullAny);
	pCall->addItem("siteName", nullAny);
	pCall->addItem("hostId", nullAny);
	pCall->addItem("hostName", nullAny);
	pCall->addItem("domainName", nullAny); //skpark
	pCall->addItem("hostType", nullAny);
	pCall->addItem("vendor", nullAny);
	pCall->addItem("monitorType", nullAny);
	pCall->addItem("model", nullAny);
	pCall->addItem("os", nullAny);
	pCall->addItem("serialNo", nullAny);
	pCall->addItem("version", nullAny);
	pCall->addItem("period", nullAny);
	pCall->addItem("screenshotPeriod", nullAny);
	pCall->addItem("description", nullAny);
	pCall->addItem("ipAddress", nullAny);
	pCall->addItem("adminState", nullAny);
	pCall->addItem("operationalState", nullAny);
	pCall->addItem("macAddress", nullAny);
	pCall->addItem("edition", nullAny);
	pCall->addItem("authDate", nullAny);
	pCall->addItem("createTime", nullAny);
	pCall->addItem("displayCounter", nullAny);
	pCall->addItem("autoSchedule1", nullAny);
	pCall->addItem("autoSchedule2", nullAny);
	pCall->addItem("currentSchedule1", nullAny);
	pCall->addItem("currentSchedule2", nullAny);
	pCall->addItem("lastSchedule1", nullAny);
	pCall->addItem("lastSchedule2", nullAny);
	pCall->addItem("lastScheduleTime1", nullAny);
	pCall->addItem("lastScheduleTime2", nullAny);
	pCall->addItem("bootUpTime", nullAny);
	pCall->addItem("networkUse1", nullAny);
	pCall->addItem("networkUse2", nullAny);
	pCall->addItem("lastUpdateTime", nullAny);
	pCall->addItem("vncPort", nullAny);
	pCall->addItem("vncState", nullAny);
	pCall->addItem("addr1", nullAny);
	pCall->addItem("startupTime", nullAny);
	pCall->addItem("shutdownTime", nullAny);
	pCall->addItem("powerControl", nullAny);
	pCall->addItem("soundVolume", nullAny);
	pCall->addItem("mute", nullAny);
	pCall->addItem("soundDisplay", nullAny);
	pCall->addItem("cpuUsed", nullAny);
	pCall->addItem("realMemoryUsed", nullAny);
	pCall->addItem("realMemoryTotal", nullAny);
	pCall->addItem("virtualMemoryUsed", nullAny);
	pCall->addItem("virtualMemoryTotal", nullAny);
	pCall->addItem("cpuTemp", nullAny);
	pCall->addItem("autoUpdateFlag", nullAny);
	pCall->addItem("playLogDayLimit", nullAny);
	pCall->addItem("hddThreshold", nullAny);
	pCall->addItem("playLogDayCriteria", nullAny);
	pCall->addItem("category", nullAny);
	pCall->addItem("contentsDownloadTime", nullAny);
	pCall->addItem("holiday", nullAny);	
	pCall->addItem("fileSystem", nullAny);
	pCall->addItem("renderMode", nullAny);
	pCall->addItem("starterState", nullAny);
	pCall->addItem("browserState", nullAny);
	pCall->addItem("browser1State", nullAny);
	pCall->addItem("preDownloaderState", nullAny);
	pCall->addItem("firmwareViewState", nullAny);
	pCall->addItem("download1State", nullAny);
	pCall->addItem("progress1", nullAny);
	pCall->addItem("download2State", nullAny);
	pCall->addItem("progress2", nullAny);
	pCall->addItem("monitorOff", nullAny);
	pCall->addItem("monitorOffList", nullAny);
	pCall->addItem("monitorState", nullAny);
	pCall->addItem("gmt", nullAny);
	pCall->addItem("monitorUseTime", nullAny);
	pCall->addItem("weekShutdownTime", nullAny);
	pCall->addItem("hostMsg1", nullAny);
	pCall->addItem("hostMsg2", nullAny);
	pCall->addItem("disk1Avail", nullAny);
	pCall->addItem("disk2Avail", nullAny);
	pCall->addItem("protocolType", nullAny);
	pCall->addItem("sosId", nullAny);
	pCall->wrap();

	if(szWhere && _tcsclen(szWhere) > 0) pCall->addItem("whereClause", szWhere);

	DWORD dwStart = GetTickCount();

	try {
		TraceLog(("%s", pCall->toString()));
		if(!pCall->call()){
			if(pCall->getError() && _tcslen(pCall->getError()) > 0)
			{
				UbcMessageBox(pCall->getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));

	return TRUE;
}

BOOL CCopModule::GetHostForList(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere, bool bFilter/*=TRUE*/)
{
	if(!pCall) return FALSE;

	pCall->setDirective("bulkget");
	cciEntity aEntity("PM=*/Site=%s/HostView=%s", szSite, szHost);
	if(bFilter) pCall->setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	pCall->setEntity(aEntity);

	cciAny nullAny;
	pCall->addItem("mgrId", nullAny);
	pCall->addItem("siteId", nullAny);
	pCall->addItem("siteName", nullAny);
	pCall->addItem("hostId", nullAny);
	pCall->addItem("hostName", nullAny);
	pCall->addItem("domainName", nullAny); //skpark
	pCall->addItem("hostType", nullAny);
	pCall->addItem("vendor", nullAny);
	pCall->addItem("monitorType", nullAny);
	pCall->addItem("model", nullAny);
	pCall->addItem("os", nullAny);
	pCall->addItem("serialNo", nullAny);
	pCall->addItem("version", nullAny);
	pCall->addItem("period", nullAny);
	pCall->addItem("screenshotPeriod", nullAny);
	pCall->addItem("description", nullAny);
	pCall->addItem("ipAddress", nullAny);
	pCall->addItem("adminState", nullAny);
	pCall->addItem("operationalState", nullAny);
	pCall->addItem("macAddress", nullAny);
	pCall->addItem("edition", nullAny);
//	pCall->addItem("authDate", nullAny);
//	pCall->addItem("createTime", nullAny);
	pCall->addItem("displayCounter", nullAny);
	pCall->addItem("autoSchedule1", nullAny);
	pCall->addItem("autoSchedule2", nullAny);
	pCall->addItem("currentSchedule1", nullAny);
	pCall->addItem("currentSchedule2", nullAny);
	pCall->addItem("lastSchedule1", nullAny);
	pCall->addItem("lastSchedule2", nullAny);
	pCall->addItem("lastScheduleTime1", nullAny);
	pCall->addItem("lastScheduleTime2", nullAny);
	pCall->addItem("bootUpTime", nullAny);
	pCall->addItem("networkUse1", nullAny);
	pCall->addItem("networkUse2", nullAny);
	pCall->addItem("lastUpdateTime", nullAny);
	pCall->addItem("vncPort", nullAny);
	pCall->addItem("vncState", nullAny);
	pCall->addItem("addr1", nullAny);
	pCall->addItem("startupTime", nullAny);
	pCall->addItem("shutdownTime", nullAny);
//	pCall->addItem("powerControl", nullAny);
//	pCall->addItem("soundVolume", nullAny);
//	pCall->addItem("mute", nullAny);
//	pCall->addItem("soundDisplay", nullAny);
//	pCall->addItem("cpuUsed", nullAny);
//	pCall->addItem("realMemoryUsed", nullAny);
//	pCall->addItem("realMemoryTotal", nullAny);
//	pCall->addItem("virtualMemoryUsed", nullAny);
//	pCall->addItem("virtualMemoryTotal", nullAny);
//	pCall->addItem("cpuTemp", nullAny);
//	pCall->addItem("autoUpdateFlag", nullAny);
//	pCall->addItem("playLogDayLimit", nullAny);
//	pCall->addItem("hddThreshold", nullAny);
//	pCall->addItem("playLogDayCriteria", nullAny);
	pCall->addItem("category", nullAny);
	pCall->addItem("contentsDownloadTime", nullAny);
	pCall->addItem("holiday", nullAny);	
	pCall->addItem("fileSystem", nullAny);
//	pCall->addItem("renderMode", nullAny);
//	pCall->addItem("starterState", nullAny);
//	pCall->addItem("browserState", nullAny);
//	pCall->addItem("browser1State", nullAny);
//	pCall->addItem("preDownloaderState", nullAny);
//	pCall->addItem("firmwareViewState", nullAny);
	pCall->addItem("download1State", nullAny);
	pCall->addItem("progress1", nullAny);
	pCall->addItem("download2State", nullAny);
	pCall->addItem("progress2", nullAny);
	pCall->addItem("monitorOff", nullAny);
	pCall->addItem("monitorOffList", nullAny);
	pCall->addItem("monitorState", nullAny);
	pCall->addItem("gmt", nullAny);
	pCall->addItem("monitorUseTime", nullAny);
	pCall->addItem("weekShutdownTime", nullAny);
	pCall->addItem("hostMsg1", nullAny);
	pCall->addItem("hostMsg2", nullAny);
	pCall->addItem("disk1Avail", nullAny);
	pCall->addItem("disk2Avail", nullAny);
	pCall->addItem("sosId", nullAny);
	pCall->wrap();

	if(szWhere && _tcsclen(szWhere) > 0) pCall->addItem("whereClause", szWhere);

	DWORD dwStart = GetTickCount();

	try {
		TraceLog(("%s", pCall->toString()));
		if(!pCall->call()){
			if(pCall->getError() && _tcslen(pCall->getError()) > 0)
			{
				UbcMessageBox(pCall->getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));

	return TRUE;
}

BOOL CCopModule::GetHostForCenter(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere, bool bFilter/*=TRUE*/)
{
	if(!pCall) return FALSE;

	pCall->setDirective("bulkget");
	cciEntity aEntity("PM=*/Site=%s/HostView=%s", szSite, szHost);
	if(bFilter) pCall->setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	pCall->setEntity(aEntity);

	cciAny nullAny;
	pCall->addItem("mgrId", nullAny);
	pCall->addItem("siteId", nullAny);
	pCall->addItem("siteName", nullAny);
	pCall->addItem("hostId", nullAny);
	pCall->addItem("hostName", nullAny);
	pCall->addItem("hostType", nullAny);
	pCall->addItem("adminState", nullAny);
	pCall->addItem("operationalState", nullAny);
	pCall->addItem("vncPort", nullAny);
	pCall->addItem("vncState", nullAny);
	pCall->addItem("displayCounter", nullAny);

	pCall->addItem("version", nullAny);
	pCall->addItem("autoUpdateFlag", nullAny);
	pCall->addItem("contentsDownloadTime", nullAny);
	pCall->addItem("description", nullAny);
	pCall->addItem("ipAddress", nullAny);
	pCall->addItem("macAddress", nullAny);
	pCall->addItem("edition", nullAny);
	pCall->addItem("bootUpTime", nullAny);
	pCall->addItem("lastUpdateTime", nullAny);

	pCall->addItem("category", nullAny);

	pCall->addItem("monitorOff", nullAny);
	pCall->addItem("monitorOffList", nullAny);
	pCall->addItem("monitorState", nullAny);

	pCall->addItem("disk1Avail", nullAny);
	pCall->addItem("disk2Avail", nullAny);

	pCall->addItem("protocolType", nullAny);

	pCall->wrap();

	if(szWhere && _tcsclen(szWhere) > 0) pCall->addItem("whereClause", szWhere);

	DWORD dwStart = GetTickCount();

	try {
		TraceLog(("%s", pCall->toString()));
		if(!pCall->call()){
			if(pCall->getError() && _tcslen(pCall->getError()) > 0)
			{
				UbcMessageBox(pCall->getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));

	return TRUE;
}

BOOL CCopModule::GetHostForProcess(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere, bool bFilter/*=TRUE*/)
{
	if(!pCall) return FALSE;

	pCall->setDirective("bulkget");
	cciEntity aEntity("PM=*/Site=%s/HostView=%s", szSite, szHost);
	if(bFilter) pCall->setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	pCall->setEntity(aEntity);

	cciAny nullAny;
	pCall->addItem("mgrId", nullAny);
	pCall->addItem("siteId", nullAny);
	pCall->addItem("siteName", nullAny);
	pCall->addItem("hostId", nullAny);
	pCall->addItem("hostName", nullAny);
	pCall->addItem("hostType", nullAny);
	pCall->addItem("ipAddress", nullAny);
	pCall->addItem("adminState", nullAny);
	pCall->addItem("operationalState", nullAny);
	pCall->addItem("vncPort", nullAny);
	pCall->addItem("vncState", nullAny);
	pCall->addItem("displayCounter", nullAny);

	pCall->addItem("starterState", nullAny);
	pCall->addItem("browserState", nullAny);
	pCall->addItem("browser1State", nullAny);
	pCall->addItem("preDownloaderState", nullAny);
	pCall->addItem("firmwareViewState", nullAny);

	pCall->addItem("category", nullAny);

	pCall->addItem("monitorOff", nullAny);
	pCall->addItem("monitorOffList", nullAny);
	pCall->addItem("monitorState", nullAny);

	pCall->wrap();

	if(szWhere && _tcsclen(szWhere) > 0) pCall->addItem("whereClause", szWhere);

	DWORD dwStart = GetTickCount();

	try {
		TraceLog(("%s", pCall->toString()));
		if(!pCall->call()){
			if(pCall->getError() && _tcslen(pCall->getError()) > 0)
			{
				UbcMessageBox(pCall->getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));

	return TRUE;
}

BOOL CCopModule::GetHostForSelect(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere, bool bFilter/*=TRUE*/)
{
	if(!pCall) return FALSE;

	pCall->setDirective("bulkget");
	cciEntity aEntity("PM=*/Site=%s/HostView=%s", szSite, szHost);
	if(bFilter) pCall->setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	pCall->setEntity(aEntity);

	cciAny nullAny;
	pCall->addItem("mgrId", nullAny);
	pCall->addItem("siteId", nullAny);
	pCall->addItem("siteName", nullAny);
	pCall->addItem("hostId", nullAny);
	pCall->addItem("hostName", nullAny);
	pCall->addItem("hostType", nullAny);
	pCall->addItem("adminState", nullAny);
	pCall->addItem("operationalState", nullAny);
	pCall->addItem("displayCounter", nullAny);
	pCall->addItem("category", nullAny);
	pCall->addItem("disk1Avail", nullAny);
	pCall->addItem("disk2Avail", nullAny);
	pCall->addItem("cpuTemp", nullAny);
	//pCall->addItem("ipAddress", nullAny);
	pCall->wrap();

	if(szWhere && _tcsclen(szWhere) > 0) pCall->addItem("whereClause", szWhere);

	DWORD dwStart = GetTickCount();

	try {
		TraceLog(("%s", pCall->toString()));
		if(!pCall->call()){
			if(pCall->getError() && _tcslen(pCall->getError()) > 0)
			{
				UbcMessageBox(pCall->getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));

	return TRUE;
}

BOOL CCopModule::GetHostData(cciCall* pCall, HostInfoList& list, int nMaxCount/*=100*/)
{
	if(!pCall) return FALSE;

	DWORD dwStart = GetTickCount();

	int nGetCount = 0;

	try {
		while(pCall->hasMore()) {
			cciReply reply;
			if ( ! pCall->getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			ciString  mgrId, siteId, siteName, hostId, vendor, model, os, description, ipAddress;
			ciShort	monitorType=0;
			ciULong serialNo = 0;
			ciString  macAddress, edition, hostName, version, location;
			ciString  autoPackage1, autoPackage2, currentPackage1, currentPackage2, lastPackage1, lastPackage2;
			ciBoolean adminState = false;
			ciBoolean operationalState = false;
			ciBoolean networkUse1 = false;
			ciBoolean networkUse2 = false;
			ciBoolean mute = false;
			ciBoolean vncState = false;
			ciBoolean autoUpdateFlag = false;
			CCI::CCI_StringList lsBuf;
			CCI::CCI_StringList lsFileSystem;
			CCI::CCI_StringList lsMonitorOffList;

			ciShort period = 0;
			ciShort screenshotPeriod = 0;
			ciShort displayCounter = 0;
			ciShort soundDisplay = 0;
			ciShort soundVolume = 0;
			ciShort cpuTemp = 0;
			ciLong  vncport = 0;
			ciLong  powerControl = 0;
			ciLong  realMemoryUsed, realMemoryTotal, virtualMemoryUsed, virtualMemoryTotal;
			ciFloat cpuUsed = 0;
			ciShort playLogDayLimit = 15;
			ciShort hddThreshold = 100;

			time_t tmInit = 0;
			ciTime authDate(tmInit), lastUpdateTime(tmInit), lastPackageTime1(tmInit), lastPackageTime2(tmInit);
			ciTime bootUpTime(tmInit), createTime(tmInit);
			//ciTime startupTime(tmInit), shutdownTime(tmInit); // 2010.11.08 박선견 time 아니고 string 이라고 함
			ciString startupTime, shutdownTime;
			ciString sosId;
			ciString playLogDayCriteria;
			ciLong renderMode = 1;
			ciString category;
			ciString contentsDownloadTime;
			ciBoolean starterState = false;
			ciBoolean browserState = false;
			ciBoolean browser1State = false;
			ciBoolean preDownloaderState = false;
			ciBoolean firmwareViewState = false;
			ciString progress1, progress2;	// 0000707: 콘텐츠 다운로드 상태 조회 기능
			ciLong download1State = 0;
			ciLong download2State = 0;
			ciBoolean monitorOff = false;
			ciLong hostType = 0;
			ciShort monitorState = -1;
			ciShort gmt = 9999;
			ciLong monitorUseTime = -1;
			ciString weekShutdownTime;
			ciString hostMsg1;
			ciString hostMsg2;
			ciFloat disk1Avail = -1;
			ciFloat disk2Avail = -1;
			ciString protocolType;
			ciString domainName; //skpark

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			reply.getItem("siteName", siteName);
			reply.getItem("hostId", hostId);
			reply.getItem("hostName", hostName);
			reply.getItem("domainName", domainName); //skpark
			reply.getItem("hostType", hostType);
			reply.getItem("vendor", vendor);
			reply.getItem("monitorType", monitorType);
			reply.getItem("model", model);
			reply.getItem("os", os);
			reply.getItem("serialNo", serialNo);
			reply.getItem("version", version);
			reply.getItem("period", period);
			reply.getItem("screenshotPeriod", screenshotPeriod);
			reply.getItem("description", description);
			reply.getItem("ipAddress", ipAddress);
			reply.getItem("adminState", adminState);
			reply.getItem("operationalState", operationalState);
			reply.getItem("macAddress", macAddress);
			reply.getItem("edition", edition);
			reply.getItem("authDate", authDate);
			reply.getItem("createTime", createTime);
			reply.getItem("displayCounter", displayCounter);
			reply.getItem("autoSchedule1", autoPackage1);
			reply.getItem("autoSchedule2", autoPackage2);
			reply.getItem("currentSchedule1", currentPackage1);
			reply.getItem("currentSchedule2", currentPackage2);
			reply.getItem("lastSchedule1", lastPackage1);
			reply.getItem("lastSchedule2", lastPackage2);
			reply.getItem("lastScheduleTime1", lastPackageTime1);
			reply.getItem("lastScheduleTime2", lastPackageTime2);
			reply.getItem("bootUpTime", bootUpTime);
			reply.getItem("networkUse1", networkUse1);
			reply.getItem("networkUse2", networkUse2);
			reply.getItem("lastUpdateTime", lastUpdateTime);
			reply.getItem("vncPort", vncport);
			reply.getItem("vncState", vncState);
			reply.getItem("addr1", location);
			reply.getItem("startupTime", startupTime);
			reply.getItem("shutdownTime", shutdownTime);
			reply.getItem("powerControl", powerControl);
			reply.getItem("soundVolume", soundVolume);
			reply.getItem("mute", mute);
			reply.getItem("soundDisplay", soundDisplay);
			reply.getItem("cpuUsed", cpuUsed);
			reply.getItem("realMemoryUsed", realMemoryUsed);
			reply.getItem("realMemoryTotal", realMemoryTotal);
			reply.getItem("virtualMemoryUsed", virtualMemoryUsed);
			reply.getItem("virtualMemoryTotal", virtualMemoryTotal);
			reply.getItem("cpuTemp", cpuTemp);
			reply.getItem("autoUpdateFlag", autoUpdateFlag);
			reply.getItem("playLogDayLimit", playLogDayLimit);
			reply.getItem("hddThreshold", hddThreshold);
			reply.getItem("playLogDayCriteria", playLogDayCriteria);
			reply.getItem("category", category);
			reply.getItem("contentsDownloadTime", contentsDownloadTime);

			reply.getItem("holiday", lsBuf);
			cciStringList holiday(lsBuf);
			
			reply.getItem("fileSystem", lsFileSystem);
			cciStringList fileSystem(lsFileSystem);

			reply.getItem("renderMode", renderMode);

			reply.getItem("starterState", starterState);
			reply.getItem("browserState", browserState);
			reply.getItem("browser1State", browser1State);
			reply.getItem("preDownloaderState", preDownloaderState);
			reply.getItem("firmwareViewState", firmwareViewState);

			// 0000707: 콘텐츠 다운로드 상태 조회 기능
			reply.getItem("download1State", download1State);
			reply.getItem("progress1", progress1);
			reply.getItem("download2State", download2State);
			reply.getItem("progress2", progress2);

			reply.getItem("monitorOff", monitorOff);
			reply.getItem("monitorOffList", lsMonitorOffList);
			cciStringList monitorOffList(lsMonitorOffList);

			reply.getItem("monitorState", monitorState);
			reply.getItem("gmt", gmt);
			reply.getItem("monitorUseTime", monitorUseTime);
			reply.getItem("weekShutdownTime", weekShutdownTime);

			reply.getItem("hostMsg1", hostMsg1);
			reply.getItem("hostMsg2", hostMsg2);

			reply.getItem("disk1Avail", disk1Avail);
			reply.getItem("disk2Avail", disk2Avail);
			
			reply.getItem("protocolType", protocolType);
			reply.getItem("sosId", sosId);

			SHostInfo hiTemp;

			hiTemp.mgrId = mgrId.c_str();
			hiTemp.siteId = siteId.c_str();
			hiTemp.siteName = siteName.c_str();
			hiTemp.hostId = hostId.c_str();
			hiTemp.hostName = hostName.c_str();
			hiTemp.domainName = domainName.c_str();//skpark
			hiTemp.hostType = hostType;
			hiTemp.vendor = vendor.c_str();
			hiTemp.monitorType = monitorType;
			hiTemp.model = model.c_str();
			hiTemp.os = os.c_str();
			hiTemp.serialNo = serialNo;
			hiTemp.version = version.c_str();
			hiTemp.period = period;
			hiTemp.screenshotPeriod = screenshotPeriod;
			hiTemp.description = description.c_str();
			hiTemp.ipAddress = ipAddress.c_str();
			hiTemp.adminState = adminState;
			hiTemp.operationalState = operationalState;
			hiTemp.macAddress = macAddress.c_str();
			hiTemp.edition = edition.c_str();
			hiTemp.authDate = authDate.getTime();
			hiTemp.createTime = createTime.getTime();
			hiTemp.displayCounter = displayCounter;
			hiTemp.autoPackage1 = autoPackage1.c_str();;
			hiTemp.autoPackage2 = autoPackage2.c_str();;
			hiTemp.currentPackage1 = currentPackage1.c_str();
			hiTemp.currentPackage2 = currentPackage2.c_str();
			hiTemp.lastPackage1 = lastPackage1.c_str();
			hiTemp.lastPackage2 = lastPackage2.c_str();
			hiTemp.lastPackageTime1 = lastPackageTime1.getTime();
			hiTemp.lastPackageTime2 = lastPackageTime2.getTime();
			hiTemp.networkUse1 = networkUse1;
			hiTemp.networkUse2 = networkUse2;
			hiTemp.lastUpdateTime = lastUpdateTime.getTime();
			hiTemp.vncPort = vncport;
			hiTemp.vncState = vncState;
			hiTemp.location = location.c_str();
			hiTemp.startupTime = startupTime.c_str();
			hiTemp.shutdownTime = shutdownTime.c_str();
			hiTemp.powerControl = powerControl;
			hiTemp.soundVolume = soundVolume;
			hiTemp.mute = mute;
			hiTemp.soundDisplay = soundDisplay;
			hiTemp.cpuTemp = cpuTemp;
			hiTemp.autoUpdateFlag = autoUpdateFlag;
			hiTemp.playLogDayLimit = playLogDayLimit;
			hiTemp.hddThreshold = hddThreshold;
			hiTemp.playLogDayCriteria = playLogDayCriteria.c_str();
			hiTemp.category = category.c_str();
			hiTemp.contentsDownloadTime = contentsDownloadTime.c_str();

			hiTemp.holiday = "";
			ciStringList lsHoliday = holiday.getStringList();
			for(ciStringList::iterator Iter = lsHoliday.begin(); Iter != lsHoliday.end(); Iter++){
				hiTemp.holiday += (*Iter).c_str();
				hiTemp.holiday += EH_SEPARATOR;
			}

			hiTemp.bootUpTime = bootUpTime.getTime();
			hiTemp.realMemoryUsed = realMemoryUsed;
			hiTemp.realMemoryTotal = realMemoryTotal;
			hiTemp.virtualMemoryUsed = virtualMemoryUsed;
			hiTemp.virtualMemoryTotal = virtualMemoryTotal;
			hiTemp.cpuUsed = cpuUsed;

			for(int i=0; i<fileSystem.length() && i<2; i++)
			{
				// "{filesystemName,OS},{filesystemBlocks,153605464},{filesystemUsed,20077600},{filesystemAvailable,133527864},{filesystemCapacity,13.07089},{filesystemMountedOn,C:}"
				CString szBuf = fileSystem[i].c_str();
				if(szBuf.Find(",") < 0)
				{
					hiTemp.filesystemCapacity[i] = atof(fileSystem[i].c_str());
				}
				else
				{
					int nValueIndex = -1;
					CString strValue[6];

					int pos = 0;
					CString strToken;
					while((strToken = szBuf.Tokenize(",", pos)) != _T(""))
					{
						strToken.Replace(_T("{"), _T(""));
						strToken.Replace(_T("}"), _T(""));

						if     (strToken == _T("filesystemName")) nValueIndex = 0;
						else if(strToken == _T("filesystemBlocks")) nValueIndex = 1;
						else if(strToken == _T("filesystemUsed")) nValueIndex = 2;
						else if(strToken == _T("filesystemAvailable")) nValueIndex = 3;
						else if(strToken == _T("filesystemCapacity")) nValueIndex = 4;
						else if(strToken == _T("filesystemMountedOn")) nValueIndex = 5;
						else if(nValueIndex >= 0) strValue[nValueIndex] = strToken;
						else continue;
					}

					if(strValue[5].IsEmpty()) continue;
					if(strValue[5].GetAt(0) != 'C' && strValue[5].GetAt(0) != 'D') continue;

					int nIndex = strValue[5].GetAt(0) - 'C';

					hiTemp.filesystemCapacity[nIndex] = atof(strValue[4]);

					TraceLog(("filesystemBlocks=%s,filesystemAvailable=%s", strValue[1],strValue[3]));
					if(atoi(strValue[1])==0 && atoi(strValue[3]) == 0) {  
						// filesystemBlock == 0 <-- skpark 2015.12.03 
						// This means no drive
						if(nIndex == 0) {
							disk1Avail = -2;//No C Drive
						}else if(nIndex == 1) {
							disk2Avail = -2;//No D Drive
						}
						TraceLog(("disk2Avail=%d", int(disk2Avail)));
						hiTemp.filesystemInfo[nIndex].Format(_T("%s No Drive"), strValue[5].Left(2));
					}else{
						hiTemp.filesystemInfo[nIndex].Format(_T("%s %s/%s")
														, strValue[5].Left(2)
														//, ToFileSize(_ttoi64(strValue[1]), 'M', false)
														, ToFileSize(_ttoi64(strValue[2]), 'M', false)
														, ToFileSize(_ttoi64(strValue[3]), 'M', false)
														);
					}
				}
			}

			hiTemp.renderMode = renderMode;

			hiTemp.starterState = starterState;
			hiTemp.browserState = browserState;
			hiTemp.browser1State = browser1State;
			hiTemp.preDownloaderState = preDownloaderState;
			hiTemp.firmwareViewState = firmwareViewState;
			
			// 0000707: 콘텐츠 다운로드 상태 조회 기능
			hiTemp.download1State = download1State;
			hiTemp.progress1 = progress1.c_str();
			hiTemp.download2State = download2State;
			hiTemp.progress2 = progress2.c_str();

			if( hiTemp.download1State < 0 ||
				hiTemp.download1State > 4 )
			{
				hiTemp.download1State = 0;
				hiTemp.progress1.Empty();
			}

			if( hiTemp.download2State < 0 ||
				hiTemp.download2State > 4 )
			{
				hiTemp.download2State = 0;
				hiTemp.progress2.Empty();
			}

			hiTemp.monitorOff = monitorOff;
			hiTemp.monitorOffList = monitorOffList.getStringList();
			hiTemp.monitorState = monitorState;
			//{
			//	// log file
			//	CFile file;
			//	file.Open("c:\\monitorofflist.txt", CFile::modeCreate | CFile::modeWrite | CFile::typeBinary);
			//	file.Write((LPCSTR)hiTemp.hostId, hiTemp.hostId.GetLength());
			//	file.Write("\r\n", 2);
			//	CString str = reply.toString();
			//	file.Write((LPCSTR)str, str.GetLength());
			//	file.Close();
			//}
			hiTemp.gmt = gmt;
			hiTemp.monitorUseTime = monitorUseTime;

			hiTemp.weekShutdownTime = weekShutdownTime.c_str();

			hiTemp.hostMsg1 = hostMsg1.c_str();
			hiTemp.hostMsg2 = hostMsg2.c_str();

			hiTemp.disk1Avail = disk1Avail;
			hiTemp.disk2Avail = disk2Avail;

			hiTemp.protocolType = protocolType.c_str();
			hiTemp.sosId = sosId.c_str();

			list.AddTail(hiTemp);

			nGetCount++;

			if(nMaxCount > 0 && nMaxCount <= nGetCount) break;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));

	return TRUE;
}

//skpark 2013.4.24 WOL 기능을 위한 정보 수집
BOOL 
CCopModule::GetRelayHostInfo(RelayHostInfoMap& pmap, ICallProgressHandler* pHandler/*=0*/)
{
	// 같은 사이트에 있는 단말중에 operationalstate 가 1 인 것을 찾는데, 가급적 hostType 이 2(kiosk) 인 것이 걸리도록 hostType 으로 order by 한다.
	
	int len = pmap.size();
	int counter = 0;
	for(RelayHostInfoMap::iterator itr=pmap.begin();itr!=pmap.end();itr++){

		if(pHandler) pHandler->ProgressEvent(len, ++counter);

		ciString site = itr->first;
		ciString  hostList;
		SRelayHostInfo* aInfo = itr->second;
		if(aInfo){
			for(ciStringSet::iterator jtr=aInfo->target_hostId_set.begin();jtr!=aInfo->target_hostId_set.end();jtr++){
				if(!hostList.empty()){
					hostList += ",";
				}
				hostList += "'";
				hostList += (*jtr);
				hostList += "'";
			}
		}
		ciString whereClause = "hostId not in (" + hostList + ") and operationalState=1 order by hostType";

		cciCall aCall("bulkget");
		cciEntity aEntity("PM=*/Site=%s/Host=*", site.c_str());
		aCall.setEntity(aEntity);

		cciAny anyVal;
		aCall.addItem("ipAddress", anyVal);
		aCall.addItem("hostId", anyVal);
		aCall.addItem("hostName", anyVal);
		aCall.addItem("hostType", anyVal);
		aCall.wrap();
		aCall.addItem("whereClause" , whereClause.c_str());

		try {
			TraceLog(("%s", aCall.toString()));
			if(!aCall.call()){
				if(aCall.getError() && _tcslen(aCall.getError()) > 0)
				{
					UbcMessageBox(aCall.getError());
				}
				TraceLog(("Failed : cciCall::call()"));
				continue;
			}
			while(aCall.hasMore()) {
				cciReply reply;
				if ( ! aCall.getReply(reply)) {
					TraceLog(("Failed : cciCall::getReply()"));
					continue;
				}

				if(!CopErrCheck(reply)) {
					TraceLog(("Failed : cciCall::getReply()"));
					if(pHandler) pHandler->ResultComplete(FALSE);
					return FALSE;
				}
				if(reply.length() == 0) {
					TraceLog(("Failed : cciCall::getReply()"));
					continue;
				}

				reply.unwrap();
	
				ciString relay_ipAddress, relay_hostId, relay_hostName;
				ciShort relay_hostType;
				reply.getItem("ipAddress",	relay_ipAddress);
				reply.getItem("hostId",		relay_hostId);
				reply.getItem("hostName",	relay_hostName);
				reply.getItem("hostType",	relay_hostType);

				if(relay_ipAddress.empty()) {
					TraceLog(("Failed : ipAddress is null"));
					continue;
				}

				aInfo->relay_ipAddress = relay_ipAddress.c_str();
				aInfo->relay_hostId = relay_hostId.c_str();
				aInfo->relay_hostName = relay_hostName.c_str();
				aInfo->relay_hostType = relay_hostType;

				TraceLog(("Succeed : cciCall::getReply([%s],[%s],[%s])", relay_ipAddress.c_str(),relay_hostId.c_str(),relay_hostName.c_str()));

				break;
			}

		}catch(CORBA::Exception& ex){
			TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
			if(pHandler) pHandler->ResultComplete(FALSE);
			return FALSE;
		}
	}
	if(pHandler) pHandler->ResultComplete(TRUE);
	return TRUE;
}


BOOL CCopModule::DelHost(LPCTSTR szSite, LPCTSTR szHost)
{
	if( szSite == NULL || _tcslen(szSite) == 0 ||
		szHost == NULL || _tcslen(szHost) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove");  // remove Host OK
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	if(atof(m_strVersion) >= 5.9) {
		aCall.addItem("trashId_", m_strLoginId);
		aCall.wrap();
	}

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
		if(!_ApproveErrorCheck(aCall)) return FALSE;

	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}
BOOL CCopModule::SetHost(SHostInfo& info, LPCTSTR szUserID, CDirtyFlag* df)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", info.siteId, info.hostId);
	aCall.setEntity(aEntity);

//	if(df && df->IsDirty()) aCall.addItem("mgrId", info.mgrId);
//	if(df && df->IsDirty()) aCall.addItem("siteId", info.siteId);
//	if(df && df->IsDirty()) aCall.addItem("hostId", info.hostId);
	if(df && df->IsDirty("hostName")) aCall.addItem("hostName", info.hostName);
	if(df && df->IsDirty("domainName")) aCall.addItem("domainName", info.domainName); //skpark
	if(df && df->IsDirty("hostType")) aCall.addItem("hostType", info.hostType);
	if(df && df->IsDirty("vendor")) aCall.addItem("vendor", info.vendor);
	if(df && df->IsDirty("monitorType")) aCall.addItem("monitorType", info.monitorType);
	if(df && df->IsDirty("model")) aCall.addItem("model", info.model);
//	if(df && df->IsDirty()) aCall.addItem("os", info.os);
//	if(df && df->IsDirty()) aCall.addItem("serialNo", info.serialNo);
//	if(df && df->IsDirty()) aCall.addItem("version", info.version);
	if(df && df->IsDirty("period")) aCall.addItem("period", info.period);
	if(df && df->IsDirty("screenshotPeriod")) aCall.addItem("screenshotPeriod", info.screenshotPeriod);
	if(df && df->IsDirty("description")) aCall.addItem("description", info.description);
//	if(df && df->IsDirty()) aCall.addItem("ipAddress", info.ipAddress);
	if(df && df->IsDirty("adminState")) aCall.addItem("adminState", info.adminState);
//	if(df && df->IsDirty()) aCall.addItem("operationalState", info.operationalState);
//	if(df && df->IsDirty()) aCall.addItem("macAddress", info.macAddress);
//	if(df && df->IsDirty()) aCall.addItem("edition", info.edition);
//	if(df && df->IsDirty()) aCall.addItem("authDate", info.authDate);
//	if(df && df->IsDirty()) aCall.addItem("displayCounter", info.displayCounter);
//	if(df && df->IsDirty()) aCall.addItem("autoSchedule1", info.autoPackage1);
//	if(df && df->IsDirty()) aCall.addItem("autoSchedule2", info.autoPackage2);
//	if(df && df->IsDirty()) aCall.addItem("currentSchedule1", info.currentPackage1);
//	if(df && df->IsDirty()) aCall.addItem("currentSchedule2", info.currentPackage2);
//	if(df && df->IsDirty()) aCall.addItem("lastSchedule1", info.lastPackage1);
//	if(df && df->IsDirty()) aCall.addItem("lastSchedule2", info.lastPackage2);
//	if(df && df->IsDirty()) aCall.addItem("lastScheduleTime1", info.lastPackageTime1);
//	if(df && df->IsDirty()) aCall.addItem("lastScheduleTime2", info.lastPackageTime2);
//	if(df && df->IsDirty()) aCall.addItem("bootUpTime", info.bootUpTime);
//	if(df && df->IsDirty()) aCall.addItem("networkUse1", info.networkUse1);
//	if(df && df->IsDirty()) aCall.addItem("networkUse2", info.networkUse2);
//	if(df && df->IsDirty()) aCall.addItem("lastUpdateTime", info.lastUpdateTime);
	if(df && df->IsDirty("vncPort")) aCall.addItem("vncPort", info.vncPort);
//	if(df && df->IsDirty()) aCall.addItem("vncState", info.vncState);
	if(df && df->IsDirty("addr1")) aCall.addItem("addr1", info.location);
	if(df && df->IsDirty("autoUpdateFlag")) aCall.addItem("autoUpdateFlag", info.autoUpdateFlag);
	if(df && df->IsDirty("playLogDayLimit")) aCall.addItem("playLogDayLimit", info.playLogDayLimit);
	if(df && df->IsDirty("hddThreshold")) aCall.addItem("hddThreshold", info.hddThreshold);
	if(df && df->IsDirty("playLogDayCriteria")) aCall.addItem("playLogDayCriteria", info.playLogDayCriteria);
	if(df && df->IsDirty("renderMode")) aCall.addItem("renderMode", info.renderMode);
	if(df && df->IsDirty("category")) aCall.addItem("category", info.category);
	if(df && df->IsDirty("contentsDownloadTime")) aCall.addItem("contentsDownloadTime", info.contentsDownloadTime);

//	ciTime tmBuf;
//	tmBuf.set(info.startupTime.GetTime());
	if(df && df->IsDirty("startupTime")) aCall.addItem("startupTime", info.startupTime);
//	tmBuf.set(info.shutdownTime.GetTime());
	if(df && df->IsDirty("shutdownTime")) aCall.addItem("shutdownTime", info.shutdownTime);

	if(df && df->IsDirty("powerControl")) aCall.addItem("powerControl", info.powerControl);
	if(df && df->IsDirty("soundVolume")) aCall.addItem("soundVolume", info.soundVolume);
	if(df && df->IsDirty("mute")) aCall.addItem("mute", info.mute);
	if(df && df->IsDirty("soundDisplay")) aCall.addItem("soundDisplay", info.soundDisplay);
//	if(df && df->IsDirty()) aCall.addItem("cpuUsed", info.cpuUsed);
//	if(df && df->IsDirty()) aCall.addItem("realMemoryUsed", info.realMemoryUsed);
//	if(df && df->IsDirty()) aCall.addItem("realMemoryTotal", info.realMemoryTotal);
//	if(df && df->IsDirty()) aCall.addItem("virtualMemoryUsed", info.virtualMemoryUsed);
//	if(df && df->IsDirty()) aCall.addItem("virtualMemoryTotal", info.virtualMemoryTotal);

	if(df && df->IsDirty("monitorOff")) aCall.addItem("monitorOff", info.monitorOff);
	cciStringList ccislBuf(info.monitorOffList);
	if(df && df->IsDirty("monitorOffList")) aCall.addItem("monitorOffList", ccislBuf.get());

	if(df && df->IsDirty("weekShutdownTime")) aCall.addItem("weekShutdownTime", info.weekShutdownTime);

	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	ciStringList lsHoliday;
	int nPos = 0;
	
	if(info.holiday.IsEmpty()){
		info.holiday = "NOSET";
	}else if(info.holiday.Find("NOSET") < 0 && info.holiday.Find(":") < 0){
		CString szTemp = info.holiday;
		info.holiday = "NOSET";
		info.holiday += EH_SEPARATOR;
		info.holiday += szTemp;
	}

	CString szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);

	while(!szToken.IsEmpty()){
		lsHoliday.push_back(ciString(szToken));
		szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);
	}

	cciStringList holiday(lsHoliday);
	CCI::CCI_StringList lsBuf = holiday.get();
	if(df && df->IsDirty("holiday")) aCall.addItem("holiday", lsBuf);


	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetHost(SHostInfo& info, LPCTSTR szUserID)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", info.siteId, info.hostId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId", info.mgrId);
//	aCall.addItem("siteId", info.siteId);
//	aCall.addItem("hostId", info.hostId);
	aCall.addItem("hostName", info.hostName);
	aCall.addItem("domainName", info.domainName); //skpark
	aCall.addItem("hostType", info.hostType);
	aCall.addItem("vendor", info.vendor);
	aCall.addItem("monitorType", info.monitorType);
	aCall.addItem("model", info.model);
//	aCall.addItem("os", info.os);
//	aCall.addItem("serialNo", info.serialNo);
//	aCall.addItem("version", info.version);
	aCall.addItem("period", info.period);
	aCall.addItem("screenshotPeriod", info.screenshotPeriod);
	aCall.addItem("description", info.description);
//	aCall.addItem("ipAddress", info.ipAddress);
	aCall.addItem("adminState", info.adminState);
//	aCall.addItem("operationalState", info.operationalState);
//	aCall.addItem("macAddress", info.macAddress);
//	aCall.addItem("edition", info.edition);
//	aCall.addItem("authDate", info.authDate);
//	aCall.addItem("displayCounter", info.displayCounter);
//	aCall.addItem("autoSchedule1", info.autoPackage1);
//	aCall.addItem("autoSchedule2", info.autoPackage2);
//	aCall.addItem("currentSchedule1", info.currentPackage1);
//	aCall.addItem("currentSchedule2", info.currentPackage2);
//	aCall.addItem("lastSchedule1", info.lastPackage1);
//	aCall.addItem("lastSchedule2", info.lastPackage2);
//	aCall.addItem("lastScheduleTime1", info.lastPackageTime1);
//	aCall.addItem("lastScheduleTime2", info.lastPackageTime2);
//	aCall.addItem("bootUpTime", info.bootUpTime);
//	aCall.addItem("networkUse1", info.networkUse1);
//	aCall.addItem("networkUse2", info.networkUse2);
//	aCall.addItem("lastUpdateTime", info.lastUpdateTime);
	aCall.addItem("vncPort", info.vncPort);
//	aCall.addItem("vncState", info.vncState);
	aCall.addItem("addr1", info.location);
	aCall.addItem("autoUpdateFlag", info.autoUpdateFlag);
	aCall.addItem("playLogDayLimit", info.playLogDayLimit);
	aCall.addItem("hddThreshold", info.hddThreshold);
	aCall.addItem("playLogDayCriteria", info.playLogDayCriteria);
	aCall.addItem("renderMode", info.renderMode);
	aCall.addItem("category", info.category);
	aCall.addItem("contentsDownloadTime", info.contentsDownloadTime);

//	ciTime tmBuf;
//	tmBuf.set(info.startupTime.GetTime());
	aCall.addItem("startupTime", info.startupTime);
//	tmBuf.set(info.shutdownTime.GetTime());
	aCall.addItem("shutdownTime", info.shutdownTime);

	aCall.addItem("powerControl", info.powerControl);
	aCall.addItem("soundVolume", info.soundVolume);
	aCall.addItem("mute", info.mute);
	aCall.addItem("soundDisplay", info.soundDisplay);
//	aCall.addItem("cpuUsed", info.cpuUsed);
//	aCall.addItem("realMemoryUsed", info.realMemoryUsed);
//	aCall.addItem("realMemoryTotal", info.realMemoryTotal);
//	aCall.addItem("virtualMemoryUsed", info.virtualMemoryUsed);
//	aCall.addItem("virtualMemoryTotal", info.virtualMemoryTotal);

	aCall.addItem("monitorOff", info.monitorOff);
	cciStringList ccislBuf(info.monitorOffList);
	aCall.addItem("monitorOffList", ccislBuf.get());

	aCall.addItem("weekShutdownTime", info.weekShutdownTime);

	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	ciStringList lsHoliday;
	int nPos = 0;
	
	if(info.holiday.IsEmpty()){
		info.holiday = "NOSET";
	}else if(info.holiday.Find("NOSET") < 0 && info.holiday.Find(":") < 0){
		CString szTemp = info.holiday;
		info.holiday = "NOSET";
		info.holiday += EH_SEPARATOR;
		info.holiday += szTemp;
	}

	CString szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);

	while(!szToken.IsEmpty()){
		lsHoliday.push_back(ciString(szToken));
		szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);
	}

	cciStringList holiday(lsHoliday);
	CCI::CCI_StringList lsBuf = holiday.get();
	aCall.addItem("holiday", lsBuf);

//	aCall.addItem("fileSystem", );

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

int CCopModule::UpdateHost(LPCTSTR szSite, LPCTSTR szKey, LPCTSTR szPasswd, LPCTSTR szHostId, LPCTSTR szCustomer)
{
	int nRet = 0;
	cciCall aCall("hostAutoUpdate");

	// skpark 2009/11/05
	ciString pmId="*";  
	if(szSite[0]=='*'){  //pmId 와 siteId 가 모두 '*' 이면, 좋지않으므로 특수처리함
		pmId = "1";
	}

	cciEntity aEntity("PM=%s/Site=%s", pmId.c_str(), szSite);
	aCall.setEntity(aEntity);

	AddItem(aCall, "enterpriseKey", szKey);
	AddItem(aCall, "password", szPasswd);
	AddItem(aCall, "hostId", (szHostId && strlen(szHostId) > 0 ? szHostId : "*"));
	AddItem(aCall, "customer", szCustomer); // 0001408: Customer 정보 입력을 받는다.

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return -1;
		}

//		while(aCall.hasMore()) {
		if(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				return -1;
//				continue;
			}

			ciLong ERROR_CODE;
			ciString ERROR_MSG;

			reply.unwrap();
			reply.getItem("ERROR_CODE", ERROR_CODE);
			reply.getItem("ERROR_MSG", ERROR_MSG);
			nRet = ERROR_CODE;

			TraceLog(("hostAutoUpdate code : %d\n Msg : %s", ERROR_CODE, ERROR_MSG.c_str() ));
			if(ERROR_CODE == -2)
			{
				UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG001), MB_ICONERROR);
				return -1;
			}

			if(!CopErrCheck(reply)) return -1;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		UbcMessageBox("Failed ", MB_ICONERROR);
		return -1;
	}

	return nRet;
}

void CCopModule::ChangeSite(LPCTSTR szSite, CStringList& lsHost, bool bReboot)
{
	cciCall aCall("changeSite");

	cciEntity aEntity("PM=*/Site=%s", szSite);
	aCall.setEntity(aEntity);

	AddItem(aCall, "hostIdList", lsHost);
	aCall.addItem("autoReboot", bReboot);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}
}

// 0001389: [RFP] 단말 OS 패스워드 변경 기능
BOOL CCopModule::SetWinPassword(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szUserID, LPCTSTR szPassword)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("winPassword", szPassword);

	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

void CCopModule::GetHostRev(LPCTSTR szSite, HostRevInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	list.RemoveAll();

	cciCall aCall("bulkget");
	//cciEntity aEntity("PM=*/Site=%s/Host=*", szSite);
	cciEntity aEntity("PM=*/Site=%s/HostView=*", szSite);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	//AddItem(aCall, "siteId");
	//AddItem(aCall, "hostId");
	//AddItem(aCall, "reservedScheduleList1");
	//AddItem(aCall, "reservedScheduleList2");
	//AddItem(aCall, "lastSchedule1");
	//AddItem(aCall, "lastScehdule2");
	//AddItem(aCall, "currentSchedule1");
	//AddItem(aCall, "currentSchedule2");
	//AddItem(aCall, "autoSchedule1");
	//AddItem(aCall, "autoSchedule2");
	//AddItem(aCall, "displayCounter");
	//aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}

		cciReply reply;
		SHostRevInfo info;
		short displayCounter;
		ciString siteId, siteName, hostId, hostName, lastPackage1, lastPackage2, currentPackage1, currentPackage2, autoPackage1, autoPackage2;
		CCI::CCI_StringList reservedPackageList1, reservedPackageList2;

		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) return;

			ciString szBuf = reply.toString();
			if(szBuf.length() < 10) continue;
			reply.unwrap();
			
			reply.getItem("siteId", siteId);
			reply.getItem("siteName", siteName);
			reply.getItem("hostId", hostId);
			reply.getItem("hostName", hostName);
			reply.getItem("lastSchedule1", lastPackage1);
			reply.getItem("lastSchedule2", lastPackage2);
			reply.getItem("currentSchedule1", currentPackage1);
			reply.getItem("currentSchedule2", currentPackage2);
			reply.getItem("autoSchedule1", autoPackage1);
			reply.getItem("autoSchedule2", autoPackage2);
			reply.getItem("reservedScheduleList1", reservedPackageList1);
			reply.getItem("reservedScheduleList2", reservedPackageList2);
			reply.getItem("displayCounter", displayCounter);

			if(siteId.empty() || hostId.empty() || displayCounter == 0)
				continue;

			info.disSide = eDispA;
			info.siteId = siteId.c_str();
			info.siteName = siteName.c_str();
			info.hostId = hostId.c_str();
			info.hostName = hostName.c_str();

			CCopModule_SetValue(info.reservedPackageList, reservedPackageList1);
			info.lastPackage = lastPackage1.c_str();
			info.currentPackage = currentPackage1.c_str();
			info.autoPackage = autoPackage1.c_str();

			list.AddTail(info);

			if(displayCounter == 1)
				continue;

			info.disSide = eDispB;
			info.siteId = siteId.c_str();
			info.siteName = siteName.c_str();
			info.hostId = hostId.c_str();
			info.hostName = hostName.c_str();

			CCopModule_SetValue(info.reservedPackageList, reservedPackageList2);
			info.lastPackage = lastPackage2.c_str();
			info.currentPackage = currentPackage2.c_str();
			info.autoPackage = autoPackage2.c_str();

			list.AddTail(info);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}

	return;
}

void CCopModule::GetPackageRev(LPCTSTR szSite, LPCTSTR szHost, int nSide, PackageRevInfoList& list)
{
	list.RemoveAll();

	cciCall aCall("get");
	cciEntity aEntity("PM=*/Site=%s/Host=%s/Reservation=*/", szSite, szHost);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	AddItem(aCall, "reservationId");
	AddItem(aCall, "siteId");
	AddItem(aCall, "hostId");
	AddItem(aCall, "programId");
	AddItem(aCall, "side");
	AddItem(aCall, "fromTime");
	AddItem(aCall, "toTime");
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}

		cciReply reply;
		SPackageRevInfo info;
		ciString reservationId, siteId, hostId, programId;
		ciShort side;
		ciTime fromTime, toTime;

		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) return;

			ciString szBuf = reply.toString();
			if(szBuf.length() < 10) continue;
			reply.unwrap();
			
			reply.getItem("reservationId", reservationId);
			reply.getItem("siteId", siteId);
			reply.getItem("hostId", hostId);
			reply.getItem("programId", programId);
			reply.getItem("side", side);
			reply.getItem("fromTime", fromTime);
			reply.getItem("toTime", toTime);

			if(nSide!=2 && (nSide+1) != side && side != 3)
				continue;

			info.reservationId = reservationId.c_str();
			info.siteId = siteId.c_str();
			info.hostId = hostId.c_str();
			info.programId = programId.c_str();
			info.side = side;
			info.fromTime = fromTime.getTime();
			info.toTime = toTime.getTime();

			list.AddTail(info);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}

	return;
}

void CCopModule::GetPackage(LPCTSTR szSite, PackageInfoList& lsPackage, LPCTSTR szWhere /*= NULL*/)
{
	lsPackage.RemoveAll();

	cciCall aCall( (szWhere && _tcsclen(szWhere) > 0 ? "bulkget" : "get") );
	cciEntity aEntity("PM=*/Site=%s/Program=*", szSite);

	TraceLog(("ubcPermition = %s", ubcPermition::getInstance()->getFilter().toString()));
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}

		cciReply reply;
		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) return;

			ciString szBuf = reply.toString();
			if(szBuf.length() < 10) continue;

			reply.unwrap();

			SPackageInfo info;
			ciString szProcID, szSiteID, szPackage, szDescript, szUpdateUser, templatePlayList;
			ciTime tmUpdateDate;
			ciULongLong volume = 0;

			// 패키지객체 속성추가
			ciLong		contentsCategory;	// 종류 (모닝,..)
			ciLong		purpose         ;	// 용도별 (교육용,..)
			ciLong		hostType        ;	// 단말타입 (키오스크..)
			ciLong		vertical        ;	// 가로/세로 방향 (가로,..)
			ciLong		resolution      ;	// 해상도 (1920x1080,..)
			ciBoolean	isPublic        ;	// 공개여부
			ciTime	 	validationDate  ;	// 패키지유효기간
			ciBoolean	isVerify        ;	// 승인여부

			reply.getItem("mgrId", szProcID);
			info.szProcID = szProcID.c_str();

			reply.getItem("siteId", szSiteID);
			info.szSiteID = szSiteID.c_str();
			
			reply.getItem("programId", szPackage);
			info.szPackage = szPackage.c_str();
			
			reply.getItem("description", szDescript);
			info.szDescript = szDescript.c_str();
			
			reply.getItem("lastUpdateId", szUpdateUser);
			info.szUpdateUser = szUpdateUser.c_str();

			reply.getItem("lastUpdateTime", tmUpdateDate);
			info.tmUpdateTime = tmUpdateDate.getTime();

			reply.getItem("templatePlayList", templatePlayList);

			// 패키지객체 속성추가
			reply.getItem("contentsCategory", contentsCategory); info.nContentsCategory = contentsCategory;
			reply.getItem("purpose"         , purpose         ); info.nPurpose          = purpose         ;
			reply.getItem("hostType"        , hostType        ); info.nHostType         = hostType        ;
			reply.getItem("vertical"        , vertical        ); info.nVertical         = vertical        ;
			reply.getItem("resolution"      , resolution      ); info.nResolution       = resolution      ;
			reply.getItem("isPublic"        , isPublic        ); info.bIsPublic         = isPublic        ;
			reply.getItem("validationDate"  , validationDate  ); info.tmValidationDate  = validationDate.getTime();
			reply.getItem("isVerify"        , isVerify        ); info.bIsVerify         = isVerify        ;

			reply.getItem("volume", volume); info.volume = volume;

			lsPackage.AddTail(info);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}
}

// 2010.08.06 by gwangsoo
BOOL CCopModule::IsExistPackage(LPCTSTR szPackage, BOOL &bExist)
{
	cciCall aCall("get");
	cciEntity aEntity("PM=*/Site=*/Program=%s", szPackage);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	bExist = FALSE;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call())
		{
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore())
		{
			cciReply reply;
			if ( ! aCall.getReply(reply))
			{
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			bExist = TRUE;
			break;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

void CCopModule::ApplyPackage(std::string strUserId, std::string strSite, std::string strPackage, std::string strSTime, std::string strETime, std::list<std::string> lsHost)
{
	cciCall aCall("apply");
	cciEntity aEntity("PM=*/Site=%s/Program=%s", strSite.c_str(), strPackage.c_str());
	aCall.setEntity(aEntity);

	cciStringList strTemp(lsHost);
	aCall.addItem("hostIdList", strTemp.get());
	aCall.addItem("applyTime", strSTime);
	aCall.addItem("endTime", strETime);

	// skpark 2013.2.15  ipAddress 도 남김...
	strUserId += ("//");
	strUserId += (this->GetMyIp());
	aCall.addItem("userId", strUserId);	// 로그에 남기기 위해 추가함

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}

		cciReply reply;

		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) {
				UbcMessageBox("Apply Fail (Check your server)");
				return;
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		UbcMessageBox("Apply Fail (Unknown Exception)");
		return;
	}
	UbcMessageBox(LoadStringById(IDS_APPLY_SUCCED_MSG));
}

void CCopModule::SetPackage(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szProg, LPCTSTR szRevId, LPCTSTR szSTime, LPCTSTR szETime)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s/Reservation=%s", szSite, szHost, szRevId);
	aCall.setEntity(aEntity);

	ciTime tmSTime(szSTime);
	ciTime tmETime(szETime);

	cciStringList strTemp;
	strTemp.addItem(szHost);

	//aCall.addItem("hostIdList", strTemp.get());
	aCall.addItem("fromTime", tmSTime);
	aCall.addItem("toTime", tmETime);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}
}

BOOL CCopModule::RemovePackage(LPCTSTR szSite, LPCTSTR szPackage)
{
	if( szSite == NULL || _tcslen(szSite) == 0 ||
		szPackage == NULL || _tcslen(szPackage) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove");  // remove Program OK
	cciEntity aEntity("PM=*/Site=%s/Program=%s", szSite, szPackage);
	aCall.setEntity(aEntity);

	if(atof(m_strVersion) >= 5.9) {
		aCall.addItem("trashId_", m_strLoginId);
		aCall.wrap();
	}

/*  이거 넣으면 fromString 이 안됨..	
	CString strUserId = m_strLoginId;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId);	// skpark 2013.02.15 로그에 남기기 위해 추가함
*/
	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
		if(!_ApproveErrorCheck(aCall)) return FALSE;

	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

void CCopModule::RemovePackageRev(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szRevId)
{
	if( szSite == NULL || _tcslen(szSite) == 0 ||
		szHost == NULL || _tcslen(szHost) == 0 ||
		szRevId == NULL || _tcslen(szRevId) == 0 )
	{
		return;
	}
  
	cciCall aCall("remove"); // remove reservation
	cciEntity aEntity("PM=*/Site=%s/Host=%s/Reservation=%s", szSite, szHost, szRevId);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}
}

// 2010.02.17 by gwangsoo
// nTargetDisplay : 0=Front, 1=Back, 2=All
BOOL CCopModule::CancelPackage(LPCTSTR szSite, LPCTSTR szHost, int nTargetDisplay, LPCTSTR szUserID)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	if(nTargetDisplay == eFront || nTargetDisplay == eAll) aCall.addItem("lastSchedule1", "NULL");
	if(nTargetDisplay == eBack  || nTargetDisplay == eAll) aCall.addItem("lastSchedule2", "NULL");

	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	try
	{
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call())
		{
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}
	catch(CORBA::Exception& ex)
	{
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetUnusedPackage(short nUnusedDays, CStringArray& astrPackageList)
{
	cciCall aCall("getUnusedProgram");
	cciEntity aEntity("OD=OD");
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	AddItem(aCall, "unusedDay", nUnusedDays);

	CString strProgramList;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString programList;

			reply.unwrap();
			reply.getItem("programList", programList);
			strProgramList = programList.c_str();

			TraceLog((strProgramList));
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	int pos = 0;
	CString strPackage;
	while( !(strPackage = strProgramList.Tokenize(_T(","), pos)).IsEmpty() )
	{
		astrPackageList.Add(strPackage);
	}

	return TRUE;
}

void CCopModule::GetAnnounce(LPCTSTR szSite, AnnounceInfoList& list)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/Announce=*", szSite);
	if(strlen(szSite) != 0 && szSite[0] != '*'){
		aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	}
	aCall.setEntity(aEntity);
	ciString whereClause = "order by startTime, endTime";
	aCall.addItem("whereClause", whereClause);
	list.RemoveAll();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}

		cciReply reply;

		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) return;

			ciString szBuf = reply.toString();
			if(szBuf.length() < 10) continue;

			ciString	mgrId;
			ciString	siteId;
			ciString	announceId;
			ciString	title;
			ciLong		serialNo=0;
			ciString	creator;
			ciTime		createTime;
			ciTime		startTime;
			ciTime		endTime;
			ciLong		position=0;
			ciShort		height=0;
			ciString	font;
			ciShort		fontSize=0;
			ciString	fgColor;
			ciString	bgColor;
			ciString	bgLocation;
			ciString	bgFile;
			ciShort		playSpeed=0;
			ciShort		alpha=0;

			ciString	comment1;
			ciString	comment2;
			ciString	comment3;
			ciString	comment4;
			ciString	comment5;
			ciString	comment6;
			ciString	comment7;
			ciString	comment8;
			ciString	comment9;
			ciString	comment10;

			cciStringList ccislBuf;
			CCI::CCI_StringList hostIdList;

			reply.unwrap();
			SAnnounceInfo info;
/*
{PM=1/Site=Default/Announce=0000000008,[
{mgrId,1},{siteId,Default},{announceId,0000000008},{serialNo,9},{title,test1},{creator,super},
{createTime,2009/09/17 17:05:14},{hostIdList,["SQI-06171"]},{startTime,2009/09/17 17:04:59},
{endTime,2009/09/17 17:04:59},{position,0},{height,100},
{comment1,},{comment2,},{comment3,},{comment4,},{comment5,},{comment6,},{comment7,},{comment8,},{comment9,},{comment10,},
{font,굴림},{fontSize,24},{bgColor,#000000},{fgColor,#FFFFFF},{playSpeed,1000},{bgLocation,},{bgFile,},{alpha,0}]}
*/
			reply.getItem("mgrId"     , mgrId     );	info.MgrId = mgrId.c_str();
			reply.getItem("siteId"    , siteId    );	info.SiteId = siteId.c_str();
			reply.getItem("announceId", announceId);	info.AnnoId = announceId.c_str();
			reply.getItem("title"     , title     );	info.Title = title.c_str();
			reply.getItem("serialNo"  , serialNo  );	info.SerialNo = serialNo;
			reply.getItem("creator"   , creator   );	info.Creator = creator.c_str();
			reply.getItem("createTime", createTime);	info.CreateTime = createTime.getTime();

			reply.getItem("hostIdList", hostIdList);
			ccislBuf.set(hostIdList);
			info.HostIdList = ccislBuf.getStringList();

			reply.getItem("startTime" , startTime );	info.StartTime = startTime.getTime();
			reply.getItem("endTime"   , endTime   );	info.EndTime = endTime.getTime();
			reply.getItem("position"  , position  );	info.Position = position;
			reply.getItem("height"    , height    );	info.Height = height;
			reply.getItem("font"      , font      );	info.Font = font.c_str();
			reply.getItem("fontSize"  , fontSize  );	info.FontSize = fontSize;
			reply.getItem("fgColor"   , fgColor   );	info.FgColor = fgColor.c_str();
			reply.getItem("bgColor"   , bgColor   );	info.BgColor = bgColor.c_str();
			reply.getItem("bgLocation", bgLocation);	info.BgLocation = bgLocation.c_str();
			reply.getItem("bgFile"    , bgFile    );	info.BgFile = bgFile.c_str();
			reply.getItem("playSpeed" , playSpeed );	info.Speed = playSpeed;
			reply.getItem("alpha"     , alpha     );	info.Alpha = alpha;

			reply.getItem("comment1"  , comment1  );	info.Comment[0] = comment1.c_str();
			reply.getItem("comment2"  , comment2  );	info.Comment[1] = comment2.c_str();
			reply.getItem("comment3"  , comment3  );	info.Comment[2] = comment3.c_str();
			reply.getItem("comment4"  , comment4  );	info.Comment[3] = comment4.c_str();
			reply.getItem("comment5"  , comment5  );	info.Comment[4] = comment5.c_str();
			reply.getItem("comment6"  , comment6  );	info.Comment[5] = comment6.c_str();
			reply.getItem("comment7"  , comment7  );	info.Comment[6] = comment7.c_str();
			reply.getItem("comment8"  , comment8  );	info.Comment[7] = comment8.c_str();
			reply.getItem("comment9"  , comment9  );	info.Comment[8] = comment9.c_str();
			reply.getItem("comment10" , comment10 );	info.Comment[9] = comment10.c_str();

			cciStringList ccislBuf2;
			CCI::CCI_StringList hostNameList;
			ciShort contentsType=2;
			ciShort lrMargin=0;
			ciShort udMargin=0;
			ciShort sourceSize=1;
			ciShort soundVolume=100;
			ciULongLong fileSize=0;
			ciString	fileMD5;
			ciShort	width=0;
			ciShort posX=0;
			ciShort posY=0;
			ciFloat heightRatio=0;

			if(!reply.getItem("hostNameList",	hostNameList)){
				TraceLog(("WARN : hostNameList get failed"));
			}else{
				ccislBuf2.set(hostNameList); 
				TraceLog(("hostNameList=%s", ccislBuf2.toString()));
				info.HostNameList=ccislBuf2.getStringList();
			}
			
			reply.getItem("contentsType",	contentsType);	info.ContentsType = contentsType;
			reply.getItem("lrMargin",		lrMargin);		info.LRMargin = lrMargin;
			reply.getItem("udMargin",		udMargin);		info.UDMargin = udMargin;
			reply.getItem("sourceSize",		sourceSize);	info.SourceSize = sourceSize;
			reply.getItem("soundVolume",	soundVolume);	info.SoundVolume = soundVolume;
			reply.getItem("fileSize",		fileSize);		info.FileSize = fileSize;
			reply.getItem("fileMD5",		fileMD5);		info.FileMD5 = fileMD5.c_str();
			reply.getItem("width",		width);				info.Width = width;
			reply.getItem("posX",		posX);				info.PosX = posX;
			reply.getItem("posY",		posY);				info.PosY = posY;
			reply.getItem("heightRatio",		heightRatio);				info.HeightRatio = heightRatio;


			list.AddTail(info);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}
}

//skpark lmo_solution
BOOL CCopModule::GetLMOSolution(LMOSolutionInfoList& list)
{
	cciCall aCall("get");
	cciEntity aEntity("LM=*/LMO_SOLUTION=*");
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	list.RemoveAll();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}

		cciReply reply;

		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) return FALSE;

			ciString szBuf = reply.toString();
			//UbcMessageBox(szBuf.c_str());
			if(szBuf.length() < 10) continue;

			reply.unwrap();
			SLMOSolutionInfo info;

			ciString mgrId;
			ciString lmId;
			ciString description;
			ciString tableName;
			ciString timeFieldName;

			reply.getItem("mgrId"     , mgrId     );	info.mgrId = mgrId.c_str();
			reply.getItem("lmId"    , lmId    );	info.lmId = lmId.c_str();
			reply.getItem("description", description);	info.description = description.c_str();
			reply.getItem("tableName"     , tableName     );	info.tableName = tableName.c_str();
			reply.getItem("timeFieldName"  , timeFieldName  );	info.timeFieldName = timeFieldName.c_str();
			reply.getItem("logType"   , info.logType   );	
			reply.getItem("duration"   , info.duration   );	
			reply.getItem("adminState", info.adminState);	


			list.AddTail(info);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;

	}
	return TRUE;
}

//skpark lmo_solution
BOOL CCopModule::SetLMOSolution(SLMOSolutionInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("LM=*/LMO_SOLUTION=%s", info.lmId);
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	

	aCall.addItem("description", info.description);
	aCall.addItem("tableName", info.tableName);
	aCall.addItem("timeFieldName", info.timeFieldName);
	aCall.addItem("adminState", info.adminState);
	aCall.addItem("logType", info.logType);
	aCall.addItem("duration", info.duration);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}
	return TRUE;
}
//skpark lmo_solution
BOOL CCopModule::CreateLMOSolution(SLMOSolutionInfo& info)
{
	cciCall aCall("create");
	cciEntity aEntity("LM=1/LMO_SOLUTION=%s", info.lmId);
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	

	aCall.addItem("description", info.description);
	aCall.addItem("tableName", info.tableName);
	aCall.addItem("timeFieldName", info.timeFieldName);
	aCall.addItem("adminState", info.adminState);
	aCall.addItem("logType", info.logType);
	aCall.addItem("duration", info.duration);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}
	return TRUE;
}
//skpark lmo_solution
BOOL CCopModule::DelLMOSolution(SLMOSolutionInfo& info)
{
	cciCall aCall("remove");  // remove solution
	cciEntity aEntity("LM=1/LMO_SOLUTION=%s", info.lmId);
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}
	return TRUE;
}



bool CCopModule::DelAnnounce(LPCTSTR szSite, LPCTSTR szAnnoId)
{
	if( szSite == NULL || _tcslen(szSite) == 0 ||
		szAnnoId == NULL || _tcslen(szAnnoId) == 0 )
	{
		return false;
	}

	cciCall aCall("remove");  // remove announce
	cciEntity aEntity("PM=*/Site=%s/Announce=%s", szSite, szAnnoId);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	return true;
}

bool CCopModule::SetAnnounce(LPCTSTR szSite, LPCTSTR szAnnoId, SAnnounceInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Announce=%s", szSite, szAnnoId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId", info.ProcId);
//	aCall.addItem("siteId", info.SiteId);
//	aCall.addItem("announceId", info.AnnoId);
	aCall.addItem("title", info.Title);
//	aCall.addItem("serialNo", info.SerialNo);
//	aCall.addItem("creator", info.Creator);
//	aCall.addItem("createTime", info.CreateTime);

	cciStringList ccislBuf = info.HostIdList;
	CCI::CCI_StringList slBuf = ccislBuf.get();
	aCall.addItem("hostIdList", slBuf);
	
	ciTime tmBuf = info.StartTime.GetTime();
	aCall.addItem("startTime", tmBuf);

	tmBuf = info.EndTime.GetTime();
	aCall.addItem("endTime", tmBuf);
	
	aCall.addItem("position", (ciLong)info.Position);
	aCall.addItem("height", (ciShort)info.Height);
	aCall.addItem("font", info.Font);
	aCall.addItem("fontSize", (ciShort)info.FontSize);
	aCall.addItem("fgColor", info.FgColor);
	aCall.addItem("bgColor", info.BgColor);
	aCall.addItem("bgLocation", info.BgLocation);
	aCall.addItem("bgFile", info.BgFile);
	aCall.addItem("playSpeed", (ciShort)info.Speed);
	aCall.addItem("alpha", (ciShort)info.Alpha);

	aCall.addItem("comment1", info.Comment[0]);
	aCall.addItem("comment2", info.Comment[1]);
	aCall.addItem("comment3", info.Comment[2]);
	aCall.addItem("comment4", info.Comment[3]);
	aCall.addItem("comment5", info.Comment[4]);
	aCall.addItem("comment6", info.Comment[5]);
	aCall.addItem("comment7", info.Comment[6]);
	aCall.addItem("comment8", info.Comment[7]);
	aCall.addItem("comment9", info.Comment[8]);
	aCall.addItem("comment10", info.Comment[9]);

	cciStringList ccislBuf2 = info.HostNameList;
	CCI::CCI_StringList slBuf2 = ccislBuf2.get();
	aCall.addItem("hostNameList",	slBuf2);
	aCall.addItem("contentsType",	(ciShort)info.ContentsType);
	aCall.addItem("lrMargin",		(ciShort)info.LRMargin);
	aCall.addItem("udMargin",		(ciShort)info.UDMargin);
	aCall.addItem("sourceSize",		(ciShort)info.SourceSize);
	aCall.addItem("soundVolume",	(ciShort)info.SoundVolume);
	if(!info.BgFile.IsEmpty()){
		if(info.FileSize > 0) {
			aCall.addItem("fileSize",		(ciULongLong)info.FileSize);
		}
	}else{
		aCall.addItem("fileSize",		(ciULongLong)0);
	}
	aCall.addItem("fileMD5",		info.FileMD5);
	aCall.addItem("width",			(ciShort)info.Width);
	aCall.addItem("posX",			(ciShort)info.PosX);
	aCall.addItem("posY",			(ciShort)info.PosY);
	aCall.addItem("heightRatio",	info.HeightRatio);

	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	return true;
}

bool CCopModule::CreAnnounce(LPCTSTR szSite, LPCTSTR szUserId, SAnnounceInfo& info)
{
	cciCall aCall("create");
	cciEntity aEntity("PM=*/Site=%s/Announce=%s", szSite, info.AnnoId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId", info.ProcId);
	aCall.addItem("siteId", szSite);
	aCall.addItem("announceId", info.AnnoId);
	aCall.addItem("title", info.Title);
//	aCall.addItem("serialNo", info.SerialNo);
	aCall.addItem("creator", szUserId);
//	aCall.addItem("createTime", info.CreateTime);

	cciStringList ccislBuf = info.HostIdList;
	CCI::CCI_StringList slBuf = ccislBuf.get();
	aCall.addItem("hostIdList", slBuf);
	
	ciTime tmBuf = info.StartTime.GetTime();
	aCall.addItem("startTime", tmBuf);

	tmBuf = info.EndTime.GetTime();
	aCall.addItem("endTime", tmBuf);
	
	aCall.addItem("position", (ciLong)info.Position);
	aCall.addItem("height", (ciShort)info.Height);
	aCall.addItem("font", info.Font);
	aCall.addItem("fontSize", (ciShort)info.FontSize);
	aCall.addItem("fgColor", info.FgColor);
	aCall.addItem("bgColor", info.BgColor);
	aCall.addItem("bgLocation", info.BgLocation);
	aCall.addItem("bgFile", info.BgFile);
	aCall.addItem("playSpeed", (ciShort)info.Speed);
	aCall.addItem("alpha", (ciShort)info.Alpha);

	aCall.addItem("comment1", info.Comment[0]);
	aCall.addItem("comment2", info.Comment[1]);
	aCall.addItem("comment3", info.Comment[2]);
	aCall.addItem("comment4", info.Comment[3]);
	aCall.addItem("comment5", info.Comment[4]);
	aCall.addItem("comment6", info.Comment[5]);
	aCall.addItem("comment7", info.Comment[6]);
	aCall.addItem("comment8", info.Comment[7]);
	aCall.addItem("comment9", info.Comment[8]);
	aCall.addItem("comment10", info.Comment[9]);

	cciStringList ccislBuf2 = info.HostNameList;
	CCI::CCI_StringList slBuf2 = ccislBuf2.get();
	aCall.addItem("hostNameList",	slBuf2);
	aCall.addItem("contentsType",	(ciShort)info.ContentsType);
	aCall.addItem("lrMargin",		(ciShort)info.LRMargin);
	aCall.addItem("udMargin",		(ciShort)info.UDMargin);
	aCall.addItem("sourceSize",		(ciShort)info.SourceSize);
	aCall.addItem("soundVolume",	(ciShort)info.SoundVolume);
	aCall.addItem("fileSize",		(ciULongLong)info.FileSize);
	aCall.addItem("fileMD5",		info.FileMD5);

	aCall.addItem("width",			(ciShort)info.Width);
	aCall.addItem("posX",			(ciShort)info.PosX);
	aCall.addItem("posY",			(ciShort)info.PosY);
	aCall.addItem("heightRatio",	info.HeightRatio);

	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	return true;
}


BOOL CCopModule::GetPackageStateLog(LPCTSTR szSite, PackageStateLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("LM=*/Site=%s/Host=*/ScheduleStateLog=*", szSite);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SPackageStateLogInfo stTemp;

			ciTime   eventTime       ;
			ciString siteId          ;
			ciString hostId          ;
			ciString autoPackage1   ;
			ciString autoPackage2   ;
			ciString currentPackage1;
			ciString currentPackage2;
			ciString lastPackage1   ;
			ciString lastPackage2   ;

			reply.unwrap();
			reply.getItem("eventTime"       , eventTime       ); stTemp.eventTime        = eventTime       .getTime();
			reply.getItem("siteId"          , siteId          ); stTemp.siteId           = siteId          .c_str();
			reply.getItem("hostId"          , hostId          ); stTemp.hostId           = hostId          .c_str();
			reply.getItem("autoSchedule1"   , autoPackage1   ); stTemp.autoPackage1    = autoPackage1   .c_str();
			reply.getItem("autoSchedule2"   , autoPackage2   ); stTemp.autoPackage2    = autoPackage2   .c_str();
			reply.getItem("currentSchedule1", currentPackage1); stTemp.currentPackage1 = currentPackage1.c_str();
			reply.getItem("currentSchedule2", currentPackage2); stTemp.currentPackage2 = currentPackage2.c_str();
			reply.getItem("lastSchedule1"   , lastPackage1   ); stTemp.lastPackage1    = lastPackage1   .c_str();
			reply.getItem("lastSchedule2"   , lastPackage2   ); stTemp.lastPackage2    = lastPackage2   .c_str();

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetPowerStateLog(LPCTSTR szSite, PowerStateLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciEntity aEntity("LM=*/Site=%s/Host=*/PowerStateLog=*", szSite);
	return _GetPowerStateLog(aEntity,list,szWhere);
}

BOOL CCopModule::GetConnectionStateLog(LPCTSTR szSite, ConnectionStateLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciEntity aEntity("LM=*/Site=%s/Host=*/ConnectionStateLog=*", szSite);
	return _GetConnectionStateLog(aEntity,list,szWhere);
}
BOOL CCopModule::GetPowerStateLogByHost(LPCTSTR szHost, PowerStateLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciEntity aEntity("LM=*/Site=*/Host=%s/PowerStateLog=*", szHost);
	return _GetPowerStateLog(aEntity,list,szWhere);
}

BOOL CCopModule::GetConnectionStateLogByHost(LPCTSTR szHost, ConnectionStateLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciEntity aEntity("LM=*/Site=*/Host=%s/ConnectionStateLog=*", szHost);
	return _GetConnectionStateLog(aEntity,list,szWhere);
}

BOOL CCopModule::_GetPowerStateLog(cciEntity& aEntity, PowerStateLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	//cciEntity aEntity(szEntity);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SPowerStateLogInfo stTemp;

			ciTime   eventTime   ;
			ciString siteId      ;
			ciString hostId      ;
			ciTime   bootUpTime  ;
			ciTime   bootDownTime;

			reply.unwrap();
			reply.getItem("eventTime"   , eventTime   ); stTemp.eventTime    = eventTime   .getTime();
			reply.getItem("siteId"      , siteId      ); stTemp.siteId       = siteId      .c_str();
			reply.getItem("hostId"      , hostId      ); stTemp.hostId       = hostId      .c_str();
			reply.getItem("bootUpTime"  , bootUpTime  ); stTemp.bootUpTime   = bootUpTime  .getTime();
			reply.getItem("bootDownTime", bootDownTime); stTemp.bootDownTime = bootDownTime.getTime();

			//TraceLog(("eventTime [%s]", eventTime.getTimeString().c_str()));
			//TraceLog(("bootUpTime [%s]", bootUpTime.getTimeString().c_str()));
			//TraceLog(("bootDownTime [%s]", bootDownTime.getTimeString().c_str()));

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::_GetConnectionStateLog(cciEntity& aEntity, ConnectionStateLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	//cciEntity aEntity(szEntity);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SConnectionStateLogInfo stTemp;

			ciTime    eventTime       ;
			ciString  siteId          ;
			ciString  hostId          ;
			ciBoolean operationalState;

			reply.unwrap();
			reply.getItem("eventTime"       , eventTime       ); stTemp.eventTime        = eventTime       .getTime();
			reply.getItem("siteId"          , siteId          ); stTemp.siteId           = siteId          .c_str();
			reply.getItem("hostId"          , hostId          ); stTemp.hostId           = hostId          .c_str();
			reply.getItem("operationalState", operationalState); stTemp.operationalState = operationalState;

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetLogConfigInfo(SLogConfigInfo& info)
{
	cciCall aCall("get");
	cciEntity aEntity("LM=1/LMO=1");
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		if(aCall.hasMore())
		{
			cciReply reply;
			if (!aCall.getReply(reply))
			{
				TraceLog(("Failed : cciCall::getReply()"));
				return FALSE;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
			{
				return FALSE;
			}

			ciBoolean packageState   ;
			ciBoolean powerState     ;
			ciBoolean connectionState;
			ciLong    duration       ;

			reply.unwrap();
			reply.getItem("scheduleState"  , packageState   ); info.packageState    = packageState   ;
			reply.getItem("powerState"     , powerState     ); info.powerState      = powerState     ;
			reply.getItem("connectionState", connectionState); info.connectionState = connectionState;
			reply.getItem("duration"       , duration       ); info.duration        = duration       ;

			info.bIsExist = true;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetLogConfigInfo(SLogConfigInfo& info)
{
	cciCall aCall( (info.bIsExist ? "set":"create") );
	cciEntity aEntity("LM=1/LMO=1");
	aCall.setEntity(aEntity);

	aCall.addItem("scheduleState"  , info.packageState  );
	aCall.addItem("powerState"     , info.powerState     );
	aCall.addItem("connectionState", info.connectionState);
	aCall.addItem("duration"       , info.duration       );

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	info.bIsExist = true;

	return TRUE;
}

void CCopModule::ChangeOpStat(LPCTSTR szPm, LPCTSTR szSite, LPCTSTR szHost, bool bOpStat)
{
	cciCall aCall("post");
	cciEntity aEntity("PM=%s/Site=%s/Host=%s", szPm, szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("hostId", szHost);
	aCall.addItem("operationalState", bOpStat?"true":"false");

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}
}
BOOL CCopModule::SetBright(LPCTSTR szSite, LPCTSTR szHost,  LPCTSTR szObjectClass, LPCTSTR szObjectId,LPCTSTR szObjectType, ciLong brightness)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s/%s=%s", szSite, szHost, szObjectClass, szObjectId);
	aCall.setEntity(aEntity);

	aCall.addItem("lightType", (const char*)szObjectType);
	aCall.addItem("brightness", brightness);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::DevicePowerOn(LPCTSTR szSite, LPCTSTR szHost,  LPCTSTR szObjectClass, LPCTSTR szObjectId, LPCTSTR szObjectType)
{
	cciCall aCall("startup");
	cciEntity aEntity("PM=*/Site=%s/Host=%s/%s=%s", szSite, szHost, szObjectClass, szObjectId);
	aCall.setEntity(aEntity);

	aCall.addItem("objectType", (const char*)szObjectType);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}


BOOL CCopModule::DevicePowerOff(LPCTSTR szSite, LPCTSTR szHost,  LPCTSTR szObjectClass, LPCTSTR szObjectId,LPCTSTR szObjectType)
{
	cciCall aCall("shutdown");
	cciEntity aEntity("PM=*/Site=%s/Host=%s/%s=%s", szSite, szHost, szObjectClass, szObjectId);
	aCall.setEntity(aEntity);

	aCall.addItem("objectType", (const char*)szObjectType);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}


BOOL CCopModule::PowerOn(LPCTSTR szSite, LPCTSTR szHost)
{
	cciCall aCall("startup");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::Shutdown(LPCTSTR szSite, LPCTSTR szHost)
{
	cciCall aCall("shutdown");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}
	return TRUE;

}

BOOL CCopModule::Reboot(LPCTSTR szSite, LPCTSTR szHost)
{
	cciCall aCall("reboot");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	return true;
}
BOOL CCopModule::MuteOn(LPCTSTR szSite, LPCTSTR szHost)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);
	aCall.addItem("mute", ciBoolean(true));
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	return true;
}
BOOL CCopModule::MuteOff(LPCTSTR szSite, LPCTSTR szHost)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("mute", ciBoolean(false));
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	return true;
}
BOOL CCopModule::SetVolume(LPCTSTR szSite, LPCTSTR szHost, ciUShort volume)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("soundVolume", volume);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	return true;
}

void CCopModule::Restart(LPCTSTR szSite, LPCTSTR szHost)
{
	cciCall aCall("restartProcess");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}
}

// 유효기간이 지난 패키지 삭제
BOOL CCopModule::CleanContents1(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId)
{
	cciCall aCall("cleanContents");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("guid", szGuid);
	
	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserId;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
		if(!_ApproveErrorCheck(aCall)) return FALSE;

	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

// 다음 패키지를 제외한 나머지 패키지 모두 삭제
BOOL CCopModule::CleanContents2(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId, CStringArray& aExcludeList)
{
	cciCall aCall("cleanContents");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("guid", szGuid);
	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserId;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	cciStringList excludeProgramList;
	for(int i=0; i<aExcludeList.GetCount() ;i++)
	{
		excludeProgramList.addItem(aExcludeList[i]);
	}

	aCall.addItem("excludeProgramList", excludeProgramList);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
		if(!_ApproveErrorCheck(aCall)) return FALSE;

	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

// 다음 패키지를  모두 삭제
BOOL CCopModule::CleanContents3(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId, CStringArray& aIncludeList)
{
	cciCall aCall("cleanContents");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("guid", szGuid);
	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserId;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	cciStringList deleteProgramList;
	for(int i=0; i<aIncludeList.GetCount() ;i++)
	{
		deleteProgramList.addItem(aIncludeList[i]);
	}

	aCall.addItem("deleteProgramList", deleteProgramList);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
		if(!_ApproveErrorCheck(aCall)) return FALSE;

	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

// 다음 패키지를  모두 삭제
BOOL CCopModule::CleanContents4(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId, CStringArray& aFilepathList)
{
	cciCall aCall("cleanContents");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("guid", szGuid);
	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserId;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	cciStringList deleteFileList;
	for(int i=0; i<aFilepathList.GetCount() ;i++)
	{
		deleteFileList.addItem(aFilepathList[i]);
	}

	aCall.addItem("deleteFileList", deleteFileList);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
		if(!_ApproveErrorCheck(aCall)) return FALSE;

	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

// 모든 컨텐츠 삭제
BOOL CCopModule::CleanAllContents(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId)
{
	cciCall aCall("cleanContents");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("guid", szGuid);
	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserId;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

void CCopModule::Trace(const char* szPath, int nLine, const char* szMsg)
{
	if(__debuger__.isDebugOn(1)) { 
		char szFile[MAX_PATH], szExe[MAX_PATH];
		_tsplitpath(szPath, NULL, NULL, szFile, szExe);

		unsigned long tid = PTHREAD_SELF(); 
		ciInt aSize = printf("TRACE-[%u]%s%s(line:%d)::",tid, szFile, szExe, nLine); 
		aSize += printf(szMsg); 

		ciDebug::setSize(aSize);
		if(ciDebug::hasLog()){
			ciDebug::log("TRACE-[%u]%s%s(line:%d)::",tid, szFile, szExe, nLine); 
			ciDebug::log(szMsg);
			ciDebug::logLineEnd();
		}
	}
}

bool CCopModule::AddItem(cciCall& cal, const char* szKey, const char* szVal)
{
	if(!szKey)	return false;

	cciAny nullAny;

	if(szVal && strlen(szVal))
		return cal.addItem(szKey, szVal);
	else
		return cal.addItem(szKey, nullAny);
}

bool CCopModule::AddItem(cciCall& cal, const char* szKey, const long lVal)
{
	if(!szKey)	return false;

	return cal.addItem(szKey, (ciLong)lVal);
}

bool CCopModule::AddItem(cciCall& cal, const char* szKey, CTime& tVal)
{
	if(!szKey)	return false;

	ciTime cit(tVal.GetTime());

	return cal.addItem(szKey, cit);
}

bool CCopModule::AddItem(cciCall& cal, const char* szKey, CStringList& lsVal)
{
	cciStringList cciTemp;
	CCI::CCI_StringList cci_temp;

	POSITION pos = lsVal.GetHeadPosition();
	while(pos){
		CString szItem = lsVal.GetNext(pos);
		cciTemp.addItem(szItem);
	}

	cci_temp = cciTemp.get();
	return cal.addItem(szKey, cci_temp);
}

bool CCopModule::AddItem(cciCall& cal, const char* szKey, std::list<std::string>& lsVal)
{
	cciStringList cciTemp(lsVal);
	CCI::CCI_StringList cci_temp;

	cci_temp = cciTemp.get();
	return cal.addItem(szKey, cci_temp);
}

BOOL CCopModule::GetBroadcastingPlanList(LPCTSTR szSite, BroadcastingPlanInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/BP=*", szSite);
	// 0001374: [RFP] 지점에서 상위조직에서 설정한 방송계획을 볼 수 있고, 우선순위를 조정할 수 있는 기능
//	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			ciString mgrId       ;
			ciString siteId      ;
			ciString bpId        ;
			ciString bpName      ;
			ciTime   createTime  ;
			ciTime   startDate   ;
			ciTime   endDate     ;
			ciString weekInfo    ;
			ciString exceptDay   ;
			ciString description ;
			ciString zorder      ;
			ciString registerId  ;
			ciTime   downloadTime;
			ciShort  retryCount  ;
			ciShort  retryGap    ;

			reply.unwrap();
			reply.getItem("mgrId"       , mgrId       );
			reply.getItem("siteId"      , siteId      );
			reply.getItem("bpId"        , bpId        );
			reply.getItem("bpName"      , bpName      );
			reply.getItem("createTime"  , createTime  );
			reply.getItem("startDate"   , startDate   );
			reply.getItem("endDate"     , endDate     );
			reply.getItem("weekInfo"    , weekInfo    );
			reply.getItem("exceptDay"   , exceptDay   );
			reply.getItem("description" , description );
			reply.getItem("zorder"      , zorder      );
			reply.getItem("registerId"  , registerId  );
			reply.getItem("downloadTime", downloadTime);
			reply.getItem("retryCount"  , retryCount  );
			reply.getItem("retryGap"    , retryGap    );

			SBroadcastingPlanInfo stTemp;

			stTemp.strMgrId       = mgrId      .c_str()  ;
			stTemp.strSiteId      = siteId     .c_str()  ;
			stTemp.strBpId        = bpId       .c_str()  ;
			stTemp.strBpName      = bpName     .c_str()  ;
			stTemp.tmCreateTime   = createTime .getTime();
			stTemp.tmStartDate    = startDate  .getTime();
			stTemp.tmEndDate      = endDate    .getTime();
			stTemp.strWeekInfo    = weekInfo   .c_str()  ;
			stTemp.strExceptDay   = exceptDay  .c_str()  ;
			stTemp.strDescription = description.c_str()  ;
			stTemp.strZOrder      = zorder     .c_str()  ;
			stTemp.strRegisterId  = registerId .c_str()  ;
			stTemp.tmDownloadTime = downloadTime.getTime();
			stTemp.nRetryCount    = retryCount           ;
			stTemp.nRetryGap      = retryGap             ;

			if(stTemp.nRetryCount < 0) stTemp.nRetryCount = 0;
			if(stTemp.nRetryGap   < 0) stTemp.nRetryGap   = 0;

			// 0001374: [RFP] 지점에서 상위조직에서 설정한 방송계획을 볼 수 있고, 우선순위를 조정할 수 있는 기능
			ciStringSet siteList;
			stTemp.bReadOnly = (m_strSiteId != siteId.c_str() && this->m_Authority != CCopModule::eSiteAdmin);
			if(ubcPermition::getInstance()->getSiteSet(siteList))
			{
				ciStringSet::iterator itr = siteList.find(siteId);
				if(itr != siteList.end())
				{
					ciString tmp = (*itr);
					stTemp.bReadOnly = tmp.empty();
				}
			}

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetBroadcastingPlan(LPCTSTR szSite, LPCTSTR szBpId, SBroadcastingPlanInfo& info)
{
	cciCall aCall("get");
	cciEntity aEntity("PM=*/Site=%s/BP=%s", szSite, szBpId);
//	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

//	AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			ciString mgrId       ;
			ciString siteId      ;
			ciString bpId        ;
			ciString bpName      ;
			ciTime   createTime  ;
			ciTime   startDate   ;
			ciTime   endDate     ;
			ciString weekInfo    ;
			ciString exceptDay   ;
			ciString description ;
			ciString zorder      ;
			ciString registerId  ;
			ciTime   downloadTime;
			ciShort  retryCount  ;
			ciShort  retryGap    ;

			reply.unwrap();
			reply.getItem("mgrId"       , mgrId       );
			reply.getItem("siteId"      , siteId      );
			reply.getItem("bpId"        , bpId        );
			reply.getItem("bpName"      , bpName      );
			reply.getItem("createTime"  , createTime  );
			reply.getItem("startDate"   , startDate   );
			reply.getItem("endDate"     , endDate     );
			reply.getItem("weekInfo"    , weekInfo    );
			reply.getItem("exceptDay"   , exceptDay   );
			reply.getItem("description" , description );
			reply.getItem("zorder"      , zorder      );
			reply.getItem("registerId"  , registerId  );
			reply.getItem("downloadTime", downloadTime);
			reply.getItem("retryCount"  , retryCount  );
			reply.getItem("retryGap"    , retryGap    );

			info.strMgrId       = mgrId      .c_str()  ;
			info.strSiteId      = siteId     .c_str()  ;
			info.strBpId        = bpId       .c_str()  ;
			info.strBpName      = bpName     .c_str()  ;
			info.tmCreateTime   = createTime .getTime();
			info.tmStartDate    = startDate  .getTime();
			info.tmEndDate      = endDate    .getTime();
			info.strWeekInfo    = weekInfo   .c_str()  ;
			info.strExceptDay   = exceptDay  .c_str()  ;
			info.strDescription = description.c_str()  ;
			info.strZOrder      = zorder     .c_str()  ;
			info.strRegisterId  = registerId .c_str()  ;
			info.tmDownloadTime = downloadTime.getTime();
			info.nRetryCount    = retryCount           ;
			info.nRetryGap      = retryGap             ;

			if(info.nRetryCount < 0) info.nRetryCount = 0;
			if(info.nRetryGap   < 0) info.nRetryGap   = 0;

			// 0001374: [RFP] 지점에서 상위조직에서 설정한 방송계획을 볼 수 있고, 우선순위를 조정할 수 있는 기능
			ciStringSet siteList;
			info.bReadOnly = (m_strSiteId != siteId.c_str() && this->m_Authority != CCopModule::eSiteAdmin);
			if(ubcPermition::getInstance()->getSiteSet(siteList))
			{
				ciStringSet::iterator itr = siteList.find(siteId);
				if(itr != siteList.end())
				{
					ciString tmp = (*itr);
					info.bReadOnly = tmp.empty();
				}
			}

			break;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::DelBroadcastingPlan(LPCTSTR szSite, LPCTSTR szBpId)
{
	if( szSite == NULL || _tcslen(szSite) == 0 ||
		szBpId == NULL || _tcslen(szBpId) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove");  // remove BP OK
	cciEntity aEntity("PM=*/Site=%s/BP=%s", szSite, szBpId);
	aCall.setEntity(aEntity);

	if(atof(m_strVersion) >= 5.9) {
		aCall.addItem("trashId_", m_strLoginId);
		aCall.wrap();
	}


	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
		if(!_ApproveErrorCheck(aCall)) return FALSE;

	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetBroadcastingPlan(SBroadcastingPlanInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/BP=%s", info.strSiteId, info.strBpId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId"      , info.strMgrId      );
//	aCall.addItem("siteId"     , info.strSiteId     );
//	aCall.addItem("bpId"       , info.strBpId       );
	aCall.addItem("bpName"     , info.strBpName     );
//	aCall.addItem("createTime" , ciTime(info.tmCreateTime.GetTime()));
	aCall.addItem("startDate"  , ciTime(info.tmStartDate .GetTime()));
	aCall.addItem("endDate"    , ciTime(info.tmEndDate   .GetTime()));
	aCall.addItem("weekInfo"   , info.strWeekInfo   );
	aCall.addItem("exceptDay"  , info.strExceptDay  );
	aCall.addItem("description", info.strDescription);
//	aCall.addItem("zorder"     , info.strZOrder     );
	aCall.addItem("registerId"  , info.strRegisterId );
	aCall.addItem("downloadTime", ciTime(info.tmDownloadTime.GetTime()));
	aCall.addItem("retryCount"  , (ciShort)info.nRetryCount);
	aCall.addItem("retryGap"    , (ciShort)info.nRetryGap  );

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::CreBroadcastingPlan(SBroadcastingPlanInfo& info)
{
	cciCall aCall("create");
	cciEntity aEntity("PM=*/Site=%s/BP=*", info.strSiteId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId"       , info.strMgrId      );
//	aCall.addItem("siteId"      , info.strSiteId     );
//	aCall.addItem("bpId"        , info.strBpId       );
	aCall.addItem("bpName"      , info.strBpName     );
//	aCall.addItem("createTime"  , ciTime(info.tmCreateTime.GetTime()));
	aCall.addItem("startDate"   , ciTime(info.tmStartDate .GetTime()));
	aCall.addItem("endDate"     , ciTime(info.tmEndDate   .GetTime()));
	aCall.addItem("weekInfo"    , info.strWeekInfo   );
	aCall.addItem("exceptDay"   , info.strExceptDay  );
	aCall.addItem("description" , info.strDescription);
//	aCall.addItem("zorder"      , info.strZOrder     );
	aCall.addItem("registerId"  , info.strRegisterId );
	aCall.addItem("downloadTime", ciTime(info.tmDownloadTime.GetTime()));
	aCall.addItem("retryCount"  , (ciShort)info.nRetryCount);
	aCall.addItem("retryGap"    , (ciShort)info.nRetryGap  );

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		if(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				return FALSE;
			}

			if(!CopErrCheck(reply)) return FALSE;

			CString szName;
			ciString cisTemp;
			cciEntity& aEntity = reply.getEntity();
			TraceLog(("aEntity=%s\n", aEntity.toString()));

			for(int i = 0; i < aEntity.length(); i++)
			{
				szName = aEntity.getClassName(i);
				
				if(szName == "PM")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strMgrId = cisTemp.c_str();
				}
				else if(szName == "Site")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strSiteId = cisTemp.c_str();
				}
				else if(szName == "BP")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strBpId = cisTemp.c_str();
				}
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetBroadcastingPlanZorder(LPCTSTR szSite, LPCTSTR szBpId, LPCTSTR szPrevZOrder, LPCTSTR szNextZOrder)
{
	cciCall aCall("setZorder");
	cciEntity aEntity("PM=*/Site=%s/BP=%s", szSite, szBpId);
	aCall.setEntity(aEntity);

	aCall.addItem("prevZorder", (szPrevZOrder ? szPrevZOrder : ""));
	aCall.addItem("nextZorder", (szNextZOrder ? szNextZOrder : ""));

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetBroadcastingPlanState(LPCTSTR szSite, LPCTSTR szBpId, short nState)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/BP=%s", szSite, szBpId);
	aCall.setEntity(aEntity);

	aCall.addItem("state", nState);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetTimePlan(LPCTSTR szSite, LPCTSTR szBpId, TimePlanInfoList& list)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/BP=%s/TP=*", szSite, szBpId);
//	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	AddItem(aCall, "whereClause", "order by CAST(zorder AS decimal)");

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call())
		{
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			ciString mgrId     ;
			ciString siteId    ;
			ciString bpId      ;
			ciString tpId      ;
			ciTime   createTime;
			ciString startTime ;
			ciString endTime   ;
			ciString programId ;
			ciString zorder    ;

			reply.unwrap();
			reply.getItem("mgrId"     , mgrId     );
			reply.getItem("siteId"    , siteId    );
			reply.getItem("bpId"      , bpId      );
			reply.getItem("tpId"      , tpId      );
			reply.getItem("createTime", createTime);
			reply.getItem("startTime" , startTime );
			reply.getItem("endTime"   , endTime   );
			reply.getItem("programId" , programId );
			reply.getItem("zorder"    , zorder    );

			STimePlanInfo stTemp;

			stTemp.strMgrId     = mgrId     .c_str()  ;
			stTemp.strSiteId    = siteId    .c_str()  ;
			stTemp.strBpId      = bpId      .c_str()  ;
			stTemp.strTpId      = tpId      .c_str()  ;
			stTemp.tmCreateTime = createTime.getTime();
			stTemp.strStartTime = startTime .c_str()  ;
			stTemp.strEndTime   = endTime   .c_str()  ;
			stTemp.strProgramId = programId .c_str()  ;
			stTemp.strZOrder    = zorder    .c_str()  ;

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::DelTimePlan(LPCTSTR szSite, LPCTSTR szBpId, LPCTSTR szTpId)
{
	if( szSite == NULL || _tcslen(szSite) == 0 || 
		szBpId == NULL || _tcslen(szBpId) == 0 || 
		szTpId == NULL || _tcslen(szTpId) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove"); // remove TP
	cciEntity aEntity("PM=*/Site=%s/BP=%s/TP=%s", szSite, szBpId, szTpId);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetTimePlan(STimePlanInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/BP=%s/TP=%s", info.strSiteId, info.strBpId, info.strTpId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId"     , info.strMgrId    );
//	aCall.addItem("siteId"    , info.strSiteId   );
//	aCall.addItem("bpId"      , info.strBpId     );
//	aCall.addItem("tpId"      , info.strTpId     );
//	aCall.addItem("createTime", ciTime(info.tmCreateTime.GetTime()));
	aCall.addItem("startTime" , info.strStartTime);
	aCall.addItem("endTime"   , info.strEndTime  );
	aCall.addItem("programId" , info.strProgramId);
	aCall.addItem("zorder"    , info.strZOrder   );

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::CreTimePlan(STimePlanInfo& info)
{
	cciCall aCall("create");
	cciEntity aEntity("PM=*/Site=%s/BP=%s/TP=*", info.strSiteId, info.strBpId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId"     , info.strMgrId    );
//	aCall.addItem("siteId"    , info.strSiteId   );
//	aCall.addItem("bpId"      , info.strBpId     );
//	aCall.addItem("tpId"      , info.strTpId     );
//	aCall.addItem("createTime", ciTime(info.tmCreateTime.GetTime()));
	aCall.addItem("startTime" , info.strStartTime);
	aCall.addItem("endTime"   , info.strEndTime  );
	aCall.addItem("programId" , info.strProgramId);
	aCall.addItem("zorder"    , info.strZOrder   );

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		if(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				return FALSE;
			}

			if(!CopErrCheck(reply)) return FALSE;

			CString szName;
			ciString cisTemp;
			cciEntity& aEntity = reply.getEntity();
			TraceLog(("aEntity=%s\n", aEntity.toString()));

			for(int i = 0; i < aEntity.length(); i++)
			{
				szName = aEntity.getClassName(i);
				
				if(szName == "PM")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strMgrId = cisTemp.c_str();
				}
				else if(szName == "Site")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strSiteId = cisTemp.c_str();
				}
				else if(szName == "BP")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strBpId = cisTemp.c_str();
				}
				else if(szName == "TP")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strTpId = cisTemp.c_str();
				}
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetTargetHost(LPCTSTR szSite, LPCTSTR szBpId, TargetHostInfoList& list)
{
	cciCall aCall("get");
	cciEntity aEntity("PM=*/Site=%s/BP=%s/TH=*", szSite, szBpId);
//	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId"     , nullAny);
	aCall.addItem("siteId"    , nullAny);
	aCall.addItem("bpId"      , nullAny);
	aCall.addItem("thId"      , nullAny);
	aCall.addItem("targetHost", nullAny);
	aCall.addItem("side"      , nullAny);
	aCall.addItem("hostName"  , nullAny);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			ciString mgrId     ;
			ciString siteId    ;
			ciString bpId      ;
			ciString thId      ;
			ciString targetHost;
			ciShort  side      ;
			ciString hostName  ;

			reply.unwrap();
			reply.getItem("mgrId"     , mgrId     );
			reply.getItem("siteId"    , siteId    );
			reply.getItem("bpId"      , bpId      );
			reply.getItem("thId"      , thId      );
			reply.getItem("targetHost", targetHost);
			reply.getItem("side"      , side      );
			reply.getItem("hostName"  , hostName  );

			STargetHostInfo stTemp;

			stTemp.strMgrId      = mgrId     .c_str();
			stTemp.strSiteId     = siteId    .c_str();
			stTemp.strBpId       = bpId      .c_str();
			stTemp.strThId       = thId      .c_str();
			stTemp.strTargetHost = targetHost.c_str();
			stTemp.nSide         = side             ;
			stTemp.strHostName   = hostName  .c_str();

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::DelTargetHost(LPCTSTR szSite, LPCTSTR szBpId, LPCTSTR szThId)
{
	if( szSite == NULL || _tcslen(szSite) == 0 || 
		szBpId == NULL || _tcslen(szBpId) == 0 || 
		szThId == NULL || _tcslen(szThId) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove");  // remove TH
	cciEntity aEntity("PM=*/Site=%s/BP=%s/TH=%s", szSite, szBpId, szThId);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetTargetHost(STargetHostInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/BP=%s/TH=%s", info.strSiteId, info.strBpId, info.strThId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId"     , info.strMgrId      );
//	aCall.addItem("siteId"    , info.strSiteId     );
//	aCall.addItem("bpId"      , info.strBpId       );
//	aCall.addItem("thId"      , info.strThId       );
	aCall.addItem("targetHost", info.strTargetHost );
	aCall.addItem("side"      , (ciShort)info.nSide);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::CreTargetHost(STargetHostInfo& info)
{
	cciCall aCall("create");
	cciEntity aEntity("PM=*/Site=%s/BP=%s/TH=%s", info.strSiteId, info.strBpId, info.strThId);
	aCall.setEntity(aEntity);

//	aCall.addItem("mgrId"     , info.strMgrId      );
//	aCall.addItem("siteId"    , info.strSiteId     );
//	aCall.addItem("bpId"      , info.strBpId       );
//	aCall.addItem("thId"      , info.strThId       );
	aCall.addItem("targetHost", info.strTargetHost );
	aCall.addItem("side"      , (ciShort)info.nSide);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		if(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				return FALSE;
			}

			if(!CopErrCheck(reply)) return FALSE;

			CString szName;
			ciString cisTemp;
			cciEntity& aEntity = reply.getEntity();
			TraceLog(("aEntity=%s\n", aEntity.toString()));

			for(int i = 0; i < aEntity.length(); i++)
			{
				szName = aEntity.getClassName(i);
				
				if(szName == "PM")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strMgrId = cisTemp.c_str();
				}
				else if(szName == "Site")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strSiteId = cisTemp.c_str();
				}
				else if(szName == "BP")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strBpId = cisTemp.c_str();
				}
				else if(szName == "TH")
				{
					cisTemp = aEntity.getClassValue(i);
					info.strThId = cisTemp.c_str();
				}
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}


BOOL CCopModule::IsExistBroadcastingPlan(LPCTSTR szSite, LPCTSTR szBpName)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/BP=*", szSite);
//	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	CString strWhere;
	strWhere.Format(_T("bpName='%s'"), szBpName);
	AddItem(aCall, "whereClause", strWhere);

	BOOL bResult = FALSE;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return TRUE;

			if(reply.length() == 0)
				continue;

			bResult = TRUE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));

		// 결과를 알 수 없으므로 객체가 있는것으로 간주한다.
		bResult = TRUE;
	}

	return bResult;
}

//+ 장애 관리
BOOL CCopModule::GetFault(LPCTSTR szSite, FaultInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("FM=*/Site=%s/Host=*/Fault=*", szSite);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SFaultInfo stTemp;

			ciString faultId       ;
			ciString siteId        ;
			ciString hostId        ;
			ciLong   counter       ;
			ciLong   severity      ;
			ciString probableCause ;
			ciTime   eventTime     ;
			ciTime   updateTime    ;
			ciString additionalText;

			reply.unwrap();
			reply.getItem("faultId"       , faultId       ); stTemp.faultId        = faultId       .c_str();
			reply.getItem("siteId"        , siteId        ); stTemp.siteId         = siteId        .c_str();
			reply.getItem("hostId"        , hostId        ); stTemp.hostId         = hostId        .c_str();
			reply.getItem("counter"       , counter       ); stTemp.counter        = counter       ;
			reply.getItem("severity"      , severity      ); stTemp.severity       = severity      ;
			reply.getItem("probableCause" , probableCause ); stTemp.probableCause  = probableCause .c_str();
			reply.getItem("eventTime"     , eventTime     ); stTemp.eventTime      = eventTime     .getTime();
			reply.getItem("updateTime"    , updateTime    ); stTemp.updateTime     = updateTime    .getTime();
			reply.getItem("additionalText", additionalText); stTemp.additionalText = additionalText.c_str();

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetFault(SFaultInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("FM=*/Site=%s/Host=%s/Fault=%s", info.siteId, info.hostId, info.faultId);
	aCall.setEntity(aEntity);

	aCall.addItem("additionalText", info.additionalText);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetFaultLog(LPCTSTR szSite, FaultLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("FM=*/Site=%s/Host=*/FaultLog=*", szSite);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SFaultInfo stTemp;

			ciString faultId       ;
			ciString siteId        ;
			ciString hostId        ;
			ciShort  counter       =0;
			ciLong   severity      =0;
			ciString probableCause ;
			ciTime   eventTime     ;
			ciTime   updateTime    ;
			ciString additionalText;

			reply.unwrap();
			reply.getItem("faultId"       , faultId       ); stTemp.faultId        = faultId       .c_str();
			reply.getItem("siteId"        , siteId        ); stTemp.siteId         = siteId        .c_str();
			reply.getItem("hostId"        , hostId        ); stTemp.hostId         = hostId        .c_str();
			reply.getItem("counter"       , counter       ); stTemp.counter        = counter       ;
			reply.getItem("severity"      , severity      ); stTemp.severity       = severity      ;
			reply.getItem("probableCause" , probableCause ); stTemp.probableCause  = probableCause .c_str();
			reply.getItem("eventTime"     , eventTime     ); stTemp.eventTime      = eventTime     .getTime();
			reply.getItem("updateTime"    , updateTime    ); stTemp.updateTime     = updateTime    .getTime();
			reply.getItem("additionalText", additionalText); stTemp.additionalText = additionalText.c_str();

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetFaultLog(LPCTSTR szSite, LPCTSTR szHost,FaultLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("FM=*/Site=%s/Host=%s/FaultLog=*", szSite,szHost);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SFaultInfo stTemp;

			ciString faultId       ;
			ciString siteId        ;
			ciString hostId        ;
			ciShort   counter      =0;
			ciLong   severity     =0;
			ciString probableCause ;
			ciTime   eventTime     ;
			ciTime   updateTime    ;
			ciString additionalText;

			reply.unwrap();
			reply.getItem("faultId"       , faultId       ); stTemp.faultId        = faultId       .c_str();
			reply.getItem("siteId"        , siteId        ); stTemp.siteId         = siteId        .c_str();
			reply.getItem("hostId"        , hostId        ); stTemp.hostId         = hostId        .c_str();
			reply.getItem("counter"       , counter       ); stTemp.counter        = counter       ;
			reply.getItem("severity"      , severity      ); stTemp.severity       = severity      ;
			reply.getItem("probableCause" , probableCause ); stTemp.probableCause  = probableCause .c_str();
			reply.getItem("eventTime"     , eventTime     ); stTemp.eventTime      = eventTime     .getTime();
			reply.getItem("updateTime"    , updateTime    ); stTemp.updateTime     = updateTime    .getTime();
			reply.getItem("additionalText", additionalText); stTemp.additionalText = additionalText.c_str();

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

//- 장애 관리

// 0000751: 콘텐츠 다운로드 상태 조회 기능
BOOL CCopModule::GetDownloadState(LPCTSTR szSite, LPCTSTR szHost, DownloadStateInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/Host=%s/DownloadState=*/", szSite, szHost);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SDownloadStateInfo stTemp;

			ciString	mgrId           ;
			ciString	siteId          ;
			ciString	hostId          ;
			ciShort		brwId           ;
			ciString	programId       ;
			ciLong		programState    ;
			ciTime		programStartTime;
			ciTime		programEndTime  ;
			ciString	progress        ;

			ciString	downloadId      ;
			ciString	contentsId      ;
			ciString	contentsName    ;
			ciLong		contentsType    ;
			ciString	location        ;
			ciString	filename        ;
			ciULongLong	volume          ;
			ciULongLong currentVolume   ;
			ciTime		startTime       ;
			ciTime		endTime         ;
			ciBoolean	result          ;
			ciLong		reason          ;
			ciLong		downState       ;

			reply.unwrap();
			reply.getItem("mgrId"           , mgrId           ); stTemp.mgrId            = mgrId           .c_str();
			reply.getItem("siteId"          , siteId          ); stTemp.siteId           = siteId          .c_str();
			reply.getItem("hostId"          , hostId          ); stTemp.hostId           = hostId          .c_str();
			reply.getItem("brwId"           , brwId           ); stTemp.brwId            = brwId           ;
			reply.getItem("programId"       , programId       ); stTemp.programId        = programId       .c_str();
			reply.getItem("programState"    , programState    ); stTemp.programState     = programState    ;
			reply.getItem("programStartTime", programStartTime); stTemp.programStartTime = programStartTime.getTime();
			reply.getItem("programEndTime"  , programEndTime  ); stTemp.programEndTime   = programEndTime  .getTime();
			reply.getItem("progress"        , progress        ); stTemp.progress         = progress        .c_str();

			reply.getItem("downloadId"      , downloadId      ); stTemp.downloadId       = downloadId      .c_str();
			reply.getItem("contentsId"      , contentsId      ); stTemp.contentsId       = contentsId      .c_str();
			reply.getItem("contentsName"    , contentsName    ); stTemp.contentsName     = contentsName    .c_str();
			reply.getItem("contentsType"    , contentsType    ); stTemp.contentsType     = contentsType    ;
			reply.getItem("location"        , location        ); stTemp.location         = location        .c_str();
			reply.getItem("filename"        , filename        ); stTemp.filename         = filename        .c_str();
			reply.getItem("volume"          , volume          ); stTemp.volume           = volume          ;
			reply.getItem("currentVolume"   , currentVolume   ); stTemp.currentVolume    = currentVolume   ;
			reply.getItem("startTime"       , startTime       ); stTemp.startTime        = startTime       .getTime();
			reply.getItem("endTime"         , endTime         ); stTemp.endTime          = endTime         .getTime();
			reply.getItem("result"          , result          ); stTemp.result           = result          ;
			reply.getItem("reason"          , reason          ); stTemp.reason           = reason          ;
			reply.getItem("downState"       , downState       ); stTemp.downState        = downState       ;

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetDownloadSummary(LPCTSTR szSite, LPCTSTR szHost, DownloadSummaryInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/Host=%s/DownloadStateSummary=*/", szSite, szHost);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId"           , nullAny);
	aCall.addItem("siteId"          , nullAny);
	aCall.addItem("hostId"          , nullAny);
	aCall.addItem("downloadId"      , nullAny);
	aCall.addItem("brwId"           , nullAny);
	aCall.addItem("programId"       , nullAny);
	aCall.addItem("programState"    , nullAny);
	aCall.addItem("programStartTime", nullAny);
	aCall.addItem("programEndTime"  , nullAny);
	aCall.addItem("progress"        , nullAny);
	aCall.addItem("siteName"        , nullAny);
	aCall.addItem("hostName"        , nullAny);
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SDownloadSummaryInfo stTemp;

			ciString	mgrId           ;
			ciString	siteId          ;
			ciString	hostId          ;
			ciString	downloadId      ;
			ciShort		brwId           ;
			ciString	programId       ;
			ciLong		programState    ;
			ciTime		programStartTime;
			ciTime		programEndTime  ;
			ciString	progress        ;
			ciString	siteName        ;	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다
			ciString	hostName        ;	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다

			reply.unwrap();
			reply.getItem("mgrId"           , mgrId           ); stTemp.mgrId            = mgrId           .c_str();
			reply.getItem("siteId"          , siteId          ); stTemp.siteId           = siteId          .c_str();
			reply.getItem("hostId"          , hostId          ); stTemp.hostId           = hostId          .c_str();
			reply.getItem("downloadId"      , downloadId      ); stTemp.downloadId       = downloadId      .c_str();
			reply.getItem("brwId"           , brwId           ); stTemp.brwId            = brwId           ;
			reply.getItem("programId"       , programId       ); stTemp.programId        = programId       .c_str();
			reply.getItem("programState"    , programState    ); stTemp.programState     = programState    ;
			reply.getItem("programStartTime", programStartTime); stTemp.programStartTime = programStartTime.getTime();
			reply.getItem("programEndTime"  , programEndTime  ); stTemp.programEndTime   = programEndTime  .getTime();
			reply.getItem("progress"        , progress        ); stTemp.progress         = progress        .c_str();
			reply.getItem("siteName"        , siteName        ); stTemp.siteName         = siteName        .c_str();	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다
			reply.getItem("hostName"        , hostName        ); stTemp.hostName         = hostName        .c_str();	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다

			//TraceLog(("mgrId[%s],siteId[%s],hostId[%s],programId[%s],", mgrId.c_str(), siteId.c_str(), hostId.c_str(), programId.c_str()));

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

int CCopModule::GetOperationalStates(CMapStringToString& hostMap, CMapStringToString& outMap)
{
	POSITION posHost = hostMap.GetStartPosition();
	while(posHost != NULL) {
		CMapStringToString bufMap;
		for(int i=0;i<80 && posHost != NULL;i++){
			CString strKey;
			CString strHostName;
			hostMap.GetNextAssoc( posHost, strKey, strHostName );
			int nPos = 0;
			CString strPackage = strKey.Tokenize("/", nPos);
			CString strHostId = strKey.Tokenize("/", nPos);

			bufMap[strHostId]=_T("");
		}
		_GetOperationalStates(bufMap);
		POSITION posBuf = bufMap.GetStartPosition();
		while(posBuf != NULL) {
			CString strHostId;
			CString operationalState;
			bufMap.GetNextAssoc( posBuf, strHostId, operationalState );
			outMap[strHostId]=operationalState;
		}
	}
	return outMap.GetSize();
}

BOOL CCopModule::_GetOperationalStates(CMapStringToString& hostMap)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=*/Host=*");
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("hostId"          , nullAny);
	aCall.addItem("operationalState"      , nullAny);
	aCall.wrap();

	CString hostList;
	POSITION posHost = hostMap.GetStartPosition();
	while(posHost != NULL)
	{
		CString strHostId;
		CString strHostName;
		hostMap.GetNextAssoc( posHost, strHostId, strHostName );
		
		if(!hostList.IsEmpty()){
			hostList += ",";
		}
		hostList += "'";
		hostList += strHostId;
		hostList += "'";
	}
	CString szWhere = "hostId in (" + hostList + ")";

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;


			ciString	hostId          ;
			ciBoolean	operationalState           ;

			reply.unwrap();
			reply.getItem("hostId"          , hostId          ); 
			reply.getItem("operationalState"           , operationalState           ); 

			if(operationalState) {
				hostMap[hostId.c_str()] = "[on]";
			}else{
				hostMap[hostId.c_str()] = "[off]";
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}



BOOL CCopModule::GetUserLog(LPCTSTR szSite,UserLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/User=*/UserLog=*/", szSite);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId"           , nullAny);
	aCall.addItem("siteId"          , nullAny);
	aCall.addItem("userId"          , nullAny);
	aCall.addItem("loginTime"		, nullAny);
	aCall.addItem("via"				, nullAny);
	aCall.addItem("result"			, nullAny);
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SUserLogInfo stTemp;

			ciString	mgrId           ;
			ciString	siteId          ;
			ciString	userId          ;
			ciTime		loginTime		;
			ciString	via				;
			ciString	result			;

			reply.unwrap();
			reply.getItem("mgrId"           , mgrId           ); stTemp.mgrId            = mgrId           .c_str();
			reply.getItem("siteId"          , siteId          ); stTemp.siteId           = siteId          .c_str();
			reply.getItem("userId"          , userId          ); stTemp.userId           = userId          .c_str();
			reply.getItem("loginTime"		, loginTime		  ); stTemp.loginTime		 = loginTime.getTime();
			reply.getItem("via"				, via			  ); stTemp.via				 = via        .c_str();
			reply.getItem("result"			, result		  ); stTemp.result			 = result        .c_str();

			//TraceLog(("mgrId[%s],siteId[%s],hostId[%s],programId[%s],", mgrId.c_str(), siteId.c_str(), hostId.c_str(), programId.c_str()));

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetBPLog(LPCTSTR szSite,BPLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/BP=*/BPLog=*/", szSite);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId"           , nullAny);
	aCall.addItem("siteId"          , nullAny);
	aCall.addItem("bpName"          , nullAny);
	aCall.addItem("touchTime"		, nullAny);
	aCall.addItem("who"				, nullAny);
	aCall.addItem("action"			, nullAny);
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SBPLogInfo stTemp;

			ciString	mgrId           ;
			ciString	siteId          ;
			ciString	bpName          ;
			ciTime		touchTime		;
			ciString	who				;
			ciString	action			;

			reply.unwrap();
			reply.getItem("mgrId"           , mgrId           ); stTemp.mgrId            = mgrId           .c_str();
			reply.getItem("siteId"          , siteId          ); stTemp.siteId           = siteId          .c_str();
			reply.getItem("bpName"          , bpName          ); stTemp.bpName           = bpName          .c_str();
			reply.getItem("touchTime"		, touchTime		  ); stTemp.touchTime		 = touchTime.getTime();
			reply.getItem("who"				, who			  ); stTemp.who				 = who        .c_str();
			reply.getItem("action"			, action		  ); stTemp.action			 = action        .c_str();

			//TraceLog(("mgrId[%s],siteId[%s],hostId[%s],programId[%s],", mgrId.c_str(), siteId.c_str(), hostId.c_str(), programId.c_str()));

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetPlayLog(LPCTSTR szSite,LPCTSTR szHost,PlayLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/Host=%s/PlayLog=*", szSite,szHost);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("siteId"          , nullAny);
	aCall.addItem("hostId"          , nullAny);
	aCall.addItem("playDate"        , nullAny);
	aCall.addItem("programId"		, nullAny);
	aCall.addItem("contentsName"	, nullAny);
	aCall.addItem("filename"		, nullAny);
	aCall.addItem("playCount"		, nullAny);
	aCall.addItem("playTime"		, nullAny);
	aCall.addItem("failCount"		, nullAny);
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			SPlayLogInfo stTemp;

			ciString	siteId          ;
			ciString	hostId          ;
			ciTime		playDate		;
			ciString	programId       ;
			ciString	contentsName	;
			ciString	filename		;
			unsigned long	playCount=0	;
			unsigned long	playTime=0	;
			unsigned long	failCount=0	;

			reply.unwrap();
			reply.getItem("siteId"          , siteId          ); stTemp.siteId           = siteId          .c_str();
			reply.getItem("hostId"          , hostId          ); stTemp.hostId           = hostId          .c_str();
			reply.getItem("playDate"		, playDate		  ); stTemp.playDate		 = playDate.getTime();
			reply.getItem("programId"       , programId       ); stTemp.programId        = programId           .c_str();
			reply.getItem("contentsName"	, contentsName	  ); stTemp.contentsName	 = contentsName        .c_str();
			reply.getItem("filename"		, filename		  ); stTemp.filename		 = filename        .c_str();
			reply.getItem("playCount"		, playCount		  ); stTemp.playCount		 = playCount;
			reply.getItem("playTime"		, playTime		  ); stTemp.playTime		 = playTime;
			reply.getItem("failCount"		, failCount		  ); stTemp.failCount		 = failCount;

			//TraceLog(("mgrId[%s],siteId[%s],hostId[%s],programId[%s],", mgrId.c_str(), siteId.c_str(), hostId.c_str(), programId.c_str()));

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

//skpark code
BOOL CCopModule::GetCodeCategory(CodeItemList& list, CString strCustomer)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("OD=OD/Code=*");
	aCall.setEntity(aEntity);

	cciAny anyVal;
	aCall.addItem("categoryName", anyVal);
	aCall.wrap();

	CString strWhere;
	strWhere.Format(_T("customer='%s' order by categoryName"), strCustomer);
	AddItem(aCall, "whereClause", strWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		ciStringSet resultSet;
		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;
			reply.unwrap();

			ciString  categoryName;
			reply.getItem("categoryName", categoryName);
			if(categoryName.empty()){
				continue;
			}
			ciStringSet::iterator itr = resultSet.find(categoryName);
			if(itr != resultSet.end()){ // 이미 있다.
				continue;
			}
			resultSet.insert(ciStringSet::value_type(categoryName));

			SCodeItem item;
			item.strCategoryName = categoryName.c_str();

			list.AddTail(item);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}


//skpark code
BOOL CCopModule::GetCodeItem(CodeItemList& list, CString strCustomer, CString category)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("OD=OD/Code=*");
	aCall.setEntity(aEntity);

	CString strWhere;
	strWhere.Format(_T("customer='%s' and categoryName='%s' order by categoryName, dorder, enumNumber"), strCustomer, category);
	AddItem(aCall, "whereClause", strWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString mgrId, codeId, categoryName, enumString;
			ciLong enumNumber = 0;
			ciBoolean visible = false;
			ciShort dorder = 0;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("codeId", codeId);
			reply.getItem("categoryName", categoryName);
			reply.getItem("enumString", enumString);
			reply.getItem("enumNumber", enumNumber);
			reply.getItem("visible", visible);
			reply.getItem("dorder", dorder);

			SCodeItem item;
			item.strMgrId = mgrId.c_str();
			item.strCodeId = codeId.c_str();
			item.strCategoryName = categoryName.c_str();
			item.strEnumString = enumString.c_str();
			item.nEnumNumber = enumNumber;
			item.bVisible = visible;
			item.nDisplayOrder = dorder;

			list.AddTail(item);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

//skpark code
BOOL CCopModule::SetCodeItem(SCodeItem& info, CString customer)
{
	cciCall aCall("bulkset");
	cciEntity aEntity("OD=OD/Code=%s", info.strCodeId);
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	
	//aCall.addItem("customer", customer);
	aCall.addItem("categoryName", info.strCategoryName);
	aCall.addItem("enumString", info.strEnumString);
	aCall.addItem("enumNumber", (ciLong)info.nEnumNumber);
	aCall.addItem("visible", info.bVisible);
	aCall.addItem("dorder", info.nDisplayOrder);

	aCall.wrap();
	
	ciString query = "customer='";
	query += customer;
	query += "'";
	
	aCall.addItem("whereClause", query.c_str());

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}
	return TRUE;
}

//skpark code
BOOL CCopModule::CreateCodeItem(SCodeItem& info, CString customer)
{
	cciCall aCall("create");
	cciEntity aEntity("OD=OD/Code=%s", info.strCodeId);
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	
	aCall.addItem("customer", customer);
	aCall.addItem("categoryName", info.strCategoryName);
	aCall.addItem("enumString", info.strEnumString);
	aCall.addItem("enumNumber", (ciLong)info.nEnumNumber);
	aCall.addItem("visible", info.bVisible);
	aCall.addItem("dorder", info.nDisplayOrder);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		UbcMessageBox(ex._name());
		return FALSE;
	}
	return TRUE;
}

//skpark code
BOOL CCopModule::DelCodeItem(SCodeItem& info, CString customer)
{
	cciCall aCall("bulkremove");
	cciEntity aEntity("OD=OD/Code=%s", info.strCodeId);
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	
	char whereClause[500];
	sprintf(whereClause,"customer='%s'", customer);
	aCall.addItem("whereClause", whereClause);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}
	return TRUE;
}


BOOL CCopModule::GetCodeList(CodeItemList& list, CString strCustomer)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("OD=OD/Code=*");
	aCall.setEntity(aEntity);

	CString strWhere;
	strWhere.Format(_T("customer='%s' order by categoryName, dorder, enumNumber"), strCustomer);
	AddItem(aCall, "whereClause", strWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString mgrId, codeId, categoryName, enumString;
			ciLong enumNumber = 0;
			ciBoolean visible = false;
			ciShort dorder = 0;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("codeId", codeId);
			reply.getItem("categoryName", categoryName);
			reply.getItem("enumString", enumString);
			reply.getItem("enumNumber", enumNumber);
			reply.getItem("visible", visible);
			reply.getItem("dorder", dorder);

			SCodeItem item;
			item.strMgrId = mgrId.c_str();
			item.strCodeId = codeId.c_str();
			item.strCategoryName = categoryName.c_str();
			item.strEnumString = enumString.c_str();
			item.nEnumNumber = enumNumber;
			item.bVisible = visible;
			item.nDisplayOrder = dorder;

			list.AddTail(item);
			TraceLog(("%s:%s=%d",  categoryName.c_str(),enumString.c_str(),enumNumber));
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

//skpark code
BOOL CCopModule::SetCustomer(SCustomerInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("OD=OD/Customer=%s", info.customerId);
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	
	aCall.addItem("description", info.description);
	aCall.addItem("url", info.url);
	aCall.addItem("gmt", (ciShort)info.gmt);
	aCall.addItem("language", info.language);

	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}
	return TRUE;
}

//skpark code
BOOL CCopModule::CreateCustomer(SCustomerInfo& info)
{
	cciCall aCall("create");
	cciEntity aEntity("OD=OD/Customer=%s", info.customerId);
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	
	aCall.addItem("description", info.description);
	aCall.addItem("url", info.url);
	aCall.addItem("gmt", (ciShort)info.gmt);
	aCall.addItem("language", info.language);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		UbcMessageBox(ex._name());
		return FALSE;
	}

	cciReply reply;
	while(aCall.getReply(reply))
	{
	
		ciString errMsg;
		ciLong errCode = 0;
		reply.getItem("ERROR_CODE", errCode);
		reply.getItem("ERROR_MSG", errMsg);

		if(!errMsg.empty() && errCode < 0){
			UbcMessageBox(errMsg.c_str());
			return FALSE;
		}
	}

	return TRUE;
}

//skpark code
BOOL CCopModule::DelCustomer(SCustomerInfo& info)
{
	cciCall aCall("remove");  //remove customer
	cciEntity aEntity("OD=OD/Customer=%s", info.customerId);
	//aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	
	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}
	return TRUE;
}




BOOL CCopModule::GetProgramIdListIncludeContents(LPCTSTR lpszContentsId, CStringArray& listProgramId)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=*/Program=*/Contents=%s", lpszContentsId);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CCopModule::GetObject()->CopErrCheck(reply)) return FALSE;

			ciString programId;

			reply.unwrap();
			reply.getItem("programId", programId);

			listProgramId.Add(programId.c_str());
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetContents(LPCTSTR szSite, CONTENTS_INFO_EX_LIST& lsContents, LPCTSTR szWhere /*= NULL*/)
{
	lsContents.RemoveAll();

	cciCall aCall( (szWhere && _tcsclen(szWhere) > 0 ? "bulkget" : "get") );
	cciEntity aEntity("PM=*/Site=%s/Program=*/Contents=*", szSite);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}

		cciReply reply;
		while(aCall.getReply(reply))
		{
			if(!CCopModule::GetObject()->CopErrCheck(reply)) return FALSE;

			ciString szBuf = reply.toString();
			if(szBuf.length() < 10) continue;

			reply.unwrap();

			CONTENTS_INFO_EX info;
			ciString	strMgrId;
			ciString	strSiteId;
			ciString	strProgramId;
			ciString	strContentsId;
			ciString	strCategory;
			ciString	strContentsName;
			ciString	strParentsId;
			ciString	strRegisterId;
			ciString	strLocalLocation;
			ciString	strSeverLocation;
			ciString	strFilename;
			ciString	strVerifier;
			ciString	strDescription;
			ciString	strComment[10];
			ciString	strBgColor;
			ciString	strFgColor;
			ciString	strFont;
			ciString	strRequester;
			ciString	strVerifyMembers;
			ciString	strRegisterType;
			ciString	strWizardXML;
			ciString	strWizardFiles;

			ciLong 		nContentsType;
			ciLong 		nRunningTime;
			ciLong		nFontSize;
			ciLong		nPlaySpeed;
			ciLong 		nSoundVolume;
			ciLong		nAlign;
			ciLong		nWidth;
			ciLong		nHeight;
			ciLong		nCurrentComment;
			ciLong		nFilesize;
			ciLong		nContentsState;
			ciLong		nContentsCategory;
			ciLong		nPurpose;
			ciLong		nHostType;
			ciLong		nVertical;
			ciLong		nResolution;

			ciBoolean	bIsPublic;
			ciBoolean	bLocalFileExist;
			ciBoolean	bServerFileExist;

			ciTime		tmRegisterTime;
			ciTime		tmValidationDate;
			ciTime		tmVerifyTime;

			//
			reply.getItem("mgrId", strMgrId);
			info.strMgrId = strMgrId.c_str();

			reply.getItem("siteId", strSiteId);
			info.strSiteId = strSiteId.c_str();

			reply.getItem("programId", strProgramId);
			info.strProgramId = strProgramId.c_str();

			reply.getItem("contentsId", strContentsId);
			info.strContentsId = strContentsId.c_str();

			reply.getItem("category", strCategory);
			info.strCategory = strCategory.c_str();

			reply.getItem("contentsName", strContentsName);
			info.strContentsName = strContentsName.c_str();

			reply.getItem("parentsId", strParentsId);
			info.strParentsId = strParentsId.c_str();

			reply.getItem("registerId", strRegisterId);
			info.strRegisterId = strRegisterId.c_str();

			reply.getItem("location", strSeverLocation);
			info.strSeverLocation = strSeverLocation.c_str();

			reply.getItem("filename", strFilename);
			info.strFilename = strFilename.c_str();

			reply.getItem("verifier", strVerifier);
			info.strVerifier = strVerifier.c_str();

			reply.getItem("description", strDescription);
			info.strDescription = strDescription.c_str();

			for(int i=0; i<10; i++)
			{
				CString str_item; str_item.Format("comment%d", i);
				reply.getItem(str_item, strComment[i]);
				info.strComment[i] = strComment[i].c_str();
			}

			reply.getItem("bgColor", strBgColor);
			info.strBgColor = strBgColor.c_str();

			reply.getItem("fgColor", strFgColor);
			info.strFgColor = strFgColor.c_str();

			reply.getItem("font", strFont);
			info.strFont = strFont.c_str();

			reply.getItem("requester", strRequester);
			info.strRequester = strRequester.c_str();

			reply.getItem("verifyMembers", strVerifyMembers);
			info.strVerifyMembers = strVerifyMembers.c_str();

			reply.getItem("registerType", strRegisterType);
			info.strRegisterType = strRegisterType.c_str();

			reply.getItem("wizardXML", strWizardXML);
			info.strWizardXML = strWizardXML.c_str();

			reply.getItem("wizardFiles", strWizardFiles);
			info.strWizardFiles = strWizardFiles.c_str();

			reply.getItem("contentsType", nContentsType);
			info.nContentsType = nContentsType;

			reply.getItem("runningTime", nRunningTime);
			info.nRunningTime = nRunningTime;

			reply.getItem("fontSize", nFontSize);
			info.nFontSize = nFontSize;

			reply.getItem("playSpeed", nPlaySpeed);
			info.nPlaySpeed = nPlaySpeed;

			reply.getItem("soundVolume", nSoundVolume);
			info.nSoundVolume = nSoundVolume;

			reply.getItem("align", nAlign);
			info.nAlign = nAlign;

			reply.getItem("width", nWidth);
			info.nWidth = nWidth;

			reply.getItem("height", nHeight);
			info.nHeight = nHeight;

			reply.getItem("currentComment", nCurrentComment);
			info.nCurrentComment = nCurrentComment;

			reply.getItem("filesize", nFilesize);
			info.nFilesize = nFilesize;

			reply.getItem("contentsState", nContentsState);
			info.nContentsState = nContentsState;

			reply.getItem("contentsCategory", nContentsCategory);
			info.nContentsCategory = nContentsCategory;

			reply.getItem("purpose", nPurpose);
			info.nPurpose = nPurpose;

			reply.getItem("hostType", nHostType);
			info.nHostType = nHostType;

			reply.getItem("vertical", nVertical);
			info.nVertical = nVertical;

			reply.getItem("resolution", nResolution);
			info.nResolution = nResolution;

			reply.getItem("isPublic", bIsPublic);
			info.bIsPublic = bIsPublic;

			reply.getItem("registerTime", tmRegisterTime);
			info.tmRegisterTime = tmRegisterTime.getTime();

			reply.getItem("validationDate", tmValidationDate);
			info.tmValidationDate = tmValidationDate.getTime();

			reply.getItem("verifyTime", tmVerifyTime);
			info.tmVerifyTime = tmVerifyTime.getTime();

			lsContents.AddTail(info);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}
	return TRUE;
}

BOOL CCopModule::RequestAlarmSummary()
{
	cciCall aCall("requestAlarmSummary");
	cciEntity aEntity("FM=*");
	aCall.setEntity(aEntity);

	try
	{
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call())
		{
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}
	catch(CORBA::Exception& ex)
	{
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::AutoUpdate(LPCTSTR szSite, LPCTSTR szHost)
{
	cciCall aCall("autoUpdate");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::AutoUpdateConfig(LPCTSTR szSite, LPCTSTR szHost, BOOL bIsAutoUpdateFlag, CString strAutoUpdateTime)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	if(bIsAutoUpdateFlag != BST_INDETERMINATE)
	{
		aCall.addItem("autoUpdateFlag", (bool)bIsAutoUpdateFlag);
	}

	if(!strAutoUpdateTime.IsEmpty())
	{
		aCall.addItem("contentsDownloadTime", strAutoUpdateTime);
	}

//	aCall.addItem("userId", szUserID); // 로그작성용

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::MonitorPower(LPCTSTR szSite, LPCTSTR szHost, bool bOnOffValue)
{
	cciCall aCall("monitorPower");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", szSite, szHost);
	aCall.setEntity(aEntity);

	aCall.addItem("onOff", (bool)bOnOffValue);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetChildSite(CString strCustomer, CString strSiteId, ChildSiteInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	if(strCustomer.IsEmpty()) strCustomer = _T("*");
	if(strSiteId.IsEmpty()) strSiteId = _T("*");

	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Franchize=%s/ChildSite=%s", strCustomer, strSiteId);
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);
	aCall.addItem("siteId", nullAny);
	aCall.addItem("siteName", nullAny);
	aCall.addItem("parentId", nullAny);
	aCall.addItem("lvl", nullAny);
	aCall.addItem("name_path", nullAny);
	aCall.addItem("id_path", nullAny);
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString mgrId, siteName, parentId, siteId, name_path, id_path;
			ciShort lvl;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			reply.getItem("siteName", siteName);
			reply.getItem("parentId", parentId);
			reply.getItem("lvl", lvl);
			reply.getItem("name_path", name_path);
			reply.getItem("id_path", id_path);

			SChildSiteInfo temp;
			temp.mgrId = mgrId.c_str();
			temp.siteId = siteId.c_str();
			temp.siteName = siteName.c_str();
			temp.parentId = parentId.c_str();
			temp.lvl = lvl;
			temp.name_path = name_path.c_str();
			temp.id_path = id_path.c_str();

			temp.changing_parentId = "";
			temp.is_parent_changed = false;

			list.AddTail(temp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetChildSite(CString strCustomer, CString strSiteId, cciStringList& list, LPCTSTR szWhere /*= NULL*/)
{
	if(strCustomer.IsEmpty()) strCustomer = _T("*");
	if(strSiteId.IsEmpty()) strSiteId = _T("*");

	cciCall aCall("get");
	cciEntity aEntity("PM=*/Franchize=%s/ChildSite=%s", strCustomer, strSiteId);
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("siteId", nullAny);
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString siteId;

			reply.unwrap();
			reply.getItem("siteId", siteId);

			list.addItem(siteId.c_str());
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetScreenShotInfo(CString strWhere, ScreenShotInfo& ssInfo)
{
	TraceLog(("GetScreenShotInfo"));
	TraceLog((ubcPermition::getInstance()->getFilter().toString()));
	return ubcHost::getScreenShot(strWhere, ssInfo);
}

BOOL CCopModule::GetApplyInfoList(CString strHostId, ApplyLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=*/Host=%s/ApplyLog=*/", strHostId);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0) continue;

			SApplyLogInfo stTemp;

			ciString	mgrId    ;
			ciString	siteId   ;
			ciString	hostId   ;
			ciString	applyId  ;
			ciString	programId;
			ciTime		applyTime;
			ciString	how      ;
			ciString	who      ;
			ciString	why      ;

			reply.unwrap();
			reply.getItem("mgrId"     , mgrId    ); stTemp.mgrId     = mgrId    .c_str();
			reply.getItem("siteId"    , siteId   ); stTemp.siteId    = siteId   .c_str();
			reply.getItem("hostId"    , hostId   ); stTemp.hostId    = hostId   .c_str();
			reply.getItem("applyId"   , applyId  ); stTemp.applyId   = applyId  .c_str();
			reply.getItem("programId" , programId); stTemp.programId = programId.c_str();
			reply.getItem("applyTime" , applyTime); stTemp.applyTime = applyTime.getTime();
			reply.getItem("how"       , how      ); stTemp.how       = how      .c_str();
			reply.getItem("who"       , who      ); stTemp.who       = who      .c_str();
			reply.getItem("why"       , why      ); stTemp.why       = why      .c_str();

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetScheduleStateInfoList(CString strHostId, ScheduleStateLogInfoList& list, LPCTSTR szWhere /*= NULL*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("LM=*/Site=*/Host=%s/ScheduleStateLog=*/", strHostId);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	if(szWhere && _tcsclen(szWhere) > 0) AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0) continue;

			SScheduleStateLogInfo stTemp;

			ciString	mgrId    ;
			ciString	siteId   ;
			ciString	hostId   ;
			ciString	logId  ;
			ciTime		eventTime;
			ciString	lastSchedule1      ;
			ciString	autoSchedule1      ;
			ciString	currentSchedule1      ;

			reply.unwrap();
			reply.getItem("mgrId"     , mgrId    ); stTemp.mgrId     = mgrId    .c_str();
			reply.getItem("siteId"    , siteId   ); stTemp.siteId    = siteId   .c_str();
			reply.getItem("hostId"    , hostId   ); stTemp.hostId    = hostId   .c_str();
			reply.getItem("logId"   , logId  ); stTemp.logId   = logId  .c_str();
			reply.getItem("eventTime" , eventTime); stTemp.eventTime = eventTime.getTime();
			reply.getItem("lastSchedule1"       , lastSchedule1      ); stTemp.lastSchedule1       = lastSchedule1      .c_str();
			reply.getItem("autoSchedule1"       , autoSchedule1      ); stTemp.autoSchedule1       = autoSchedule1      .c_str();
			reply.getItem("currentSchedule1"       , currentSchedule1      ); stTemp.currentSchedule1       = currentSchedule1      .c_str();

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}


BOOL CCopModule::GetRoleList(RoleInfoList& list)
{
	cciCall aCall("get");
	cciEntity aEntity("OD=OD/Role=*");
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString mgrId;
			ciString roleId;
			ciString roleName;
			ciString roleDesc;
			ciString customer;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("roleId", roleId);
			reply.getItem("roleName", roleName);
			reply.getItem("roleDesc", roleDesc);
			reply.getItem("customer", customer);

			SRoleInfo item;
			item.mgrId = mgrId.c_str();
			item.roleId = roleId.c_str();
			item.roleName = roleName.c_str();
			item.roleDesc = roleDesc.c_str();
			item.customer = customer.c_str();

			list.AddTail(item);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetCustomerList(CustomerInfoList& list)
{
	cciCall aCall("get");
	cciEntity aEntity("OD=OD/Customer=*");
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString mgrId, customerId, url, description, language;
			ciShort gmt=9999;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("customerId", customerId);
			reply.getItem("url", url);
			reply.getItem("description", description);
			reply.getItem("gmt", gmt);
			reply.getItem("language", language);

			SCustomerInfo item;
			item.mgrId = mgrId.c_str();
			item.customerId = customerId.c_str();
			item.url = url.c_str();
			item.description = description.c_str();
			item.gmt = gmt;
			item.language = language.c_str();

			item.url.Replace("\r\n","\n");
			item.url.Replace("\n","\r\n");

			list.AddTail(item);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::GetAllCategory(CStringArray& aCategoryList)
{
	cciCall aCall("getCategory");
	cciEntity aEntity("OD=OD");
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	CString strProgramList;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			cciStringList categoryList;

			reply.unwrap();
			reply.getItem("categoryList", categoryList);

			for(int i=0; i<categoryList.length() ;i++)
			{
				aCategoryList.Add(categoryList[i].c_str());
			}
			ciString buf = categoryList.toString();
			TraceLog(("category=%s", buf.c_str()));
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetCategory(SHostInfo& info, LPCTSTR szUserID, CString strCategory)
{
	//CString strLowCategory1 = "/" + info.category + "/";
	//strLowCategory1.MakeLower();

	//CString strLowCategory2 = "/" + strCategory + "/";
	//strLowCategory2.MakeLower();

	//if(strLowCategory1.Find(strLowCategory2) >= 0) return TRUE;

	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", info.siteId, info.hostId);
	aCall.setEntity(aEntity);

	aCall.addItem("category", strCategory);
	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::SetPowerMng(SHostInfo& info, LPCTSTR szUserID)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", info.siteId, info.hostId);
	aCall.setEntity(aEntity);

	aCall.addItem("shutdownTime", info.shutdownTime);
	aCall.addItem("startupTime", info.startupTime);
	aCall.addItem("monitorOff", info.monitorOff);
	cciStringList ccislBuf(info.monitorOffList);
	aCall.addItem("monitorOffList", ccislBuf.get());
	aCall.addItem("weekShutdownTime", info.weekShutdownTime);

	ciStringList lsHoliday;
	int nPos = 0;
	
	if(info.holiday.IsEmpty()){
		info.holiday = "NOSET";
	}else if(info.holiday.Find("NOSET") < 0 && info.holiday.Find(":") < 0){
		CString szTemp = info.holiday;
		info.holiday = "NOSET";
		info.holiday += EH_SEPARATOR;
		info.holiday += szTemp;
	}

	CString szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);

	while(!szToken.IsEmpty()){
		lsHoliday.push_back(ciString(szToken));
		szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);
	}

	cciStringList holiday(lsHoliday);
	CCI::CCI_StringList lsBuf = holiday.get();
	aCall.addItem("holiday", lsBuf);

	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}
BOOL CCopModule::SetPowerMng(SNodeViewInfo* info,LPCTSTR szUserID)
{
	cciCall aCall("set");
	if(info->objectClass == "Host"){
		cciEntity aEntity("PM=*/Site=%s/Host=%s",info->hostSiteId,info->hostId);
		aCall.setEntity(aEntity);
	}else{
		cciEntity aEntity("PM=*/Site=%s/Host=%s/%s=%s",	info->hostSiteId,info->hostId, 
														info->objectClass, info->objectId);
		aCall.setEntity(aEntity);
	}

	aCall.addItem("shutdownTime", info->shutdownTime);
	aCall.addItem("startupTime", info->startupTime);
	aCall.addItem("weekShutdownTime", info->weekShutdownTime);

	ciStringList lsHoliday;
	int nPos = 0;
	
	if(info->holiday.IsEmpty()){
		info->holiday = "NOSET";
	}else if(info->holiday.Find("NOSET") < 0 && info->holiday.Find(":") < 0){
		CString szTemp = info->holiday;
		info->holiday = "NOSET";
		info->holiday += EH_SEPARATOR;
		info->holiday += szTemp;
	}

	CString szToken = info->holiday.Tokenize(EH_SEPARATOR, nPos);

	while(!szToken.IsEmpty()){
		lsHoliday.push_back(ciString(szToken));
		szToken = info->holiday.Tokenize(EH_SEPARATOR, nPos);
	}

	cciStringList holiday(lsHoliday);
	CCI::CCI_StringList lsBuf = holiday.get();
	aCall.addItem("holiday", lsBuf);

	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::IsMySite(LPCTSTR szSiteID)
{
	BOOL bResult = FALSE;

	ciStringSet siteList;
	if(ubcPermition::getInstance()->getSiteSet(siteList))
	{
		ciStringSet::iterator itr = siteList.find(szSiteID);
		if(itr != siteList.end())
		{
			ciString tmp = (*itr);
			bResult = !tmp.empty();
		}
	}

	return bResult;
}

// skpark 2012.11.12
// site 가 추가되거나 삭제될때, ubcPermition 의 filter 객체를 다시 계산해주어야 한다.
int  
CCopModule::RefreshPermitionSite(CString strCustomer)
{
	TraceLog(("RefreshSite : Customer=[%s],Site=[%s],User=[%s],UserType=[%d]", 
		strCustomer,m_strSiteId,m_strLoginId,m_Authority));

	ciString siteId = m_strSiteId;
	ciString userId = m_strLoginId;
	int userType = m_Authority;

	ciString siteList;
	ciString hostList;

	if(m_strSiteId != _T("Default") || userType != CCopModule::eSiteAdmin)
	{
		GetUserSiteList(siteList,hostList);

		TraceLog(("siteList : %s", siteList.c_str()));
		TraceLog(("hostList : %s", hostList.c_str()));

		cciStringList childSitelist;
		// 사용자 계정의 경우 해당사이트만 볼수있게한다. 하위사이트에 대한 권한이 없음
		if(CCopModule::eSiteUser == userType) childSitelist.addItem(siteId.c_str());
		else                                   GetChildSite(strCustomer, siteId.c_str(), childSitelist);

		cciStringList cciSiteList(siteList.c_str());

		for(int i=0; i<cciSiteList.length(); i++)
		{
			// 사용자 계정의 경우 해당사이트만 볼수있게한다. 하위사이트에 대한 권한이 없음
			if(CCopModule::eSiteUser == userType) childSitelist.addItem(cciSiteList[i].c_str());
			else                                   GetChildSite(strCustomer, cciSiteList[i].c_str(), childSitelist);
		}

		TraceLog(("siteList : %s", childSitelist.toString()));

		siteList = childSitelist.toString();
	}

	ubcPermition::getInstance()->init(siteId.c_str(), userType, siteList.c_str(), hostList.c_str());
	return userType;
}

BOOL 	
CCopModule::GetUserSiteList(ciString& siteList,ciString& hostList)
{
	TraceLog(("GetUserSiteList : Site=[%s],User=[%s]", m_strSiteId,m_strLoginId));

	cciCall aCall("get");
	cciEntity aEntity("PM=*/Site=%s/User=%s", m_strSiteId, m_strLoginId);
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);
	aCall.addItem("siteId", nullAny);
	aCall.addItem("userId", nullAny);
	aCall.addItem("siteList", nullAny);
	aCall.addItem("hostList", nullAny);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}


		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString mgrId, siteId, userId;
	
			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			reply.getItem("userId", userId);
			reply.getItem("siteList", siteList);
			reply.getItem("hostList", hostList);

			if(userId.length() == 0)
				continue;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}
	return TRUE;
}

BOOL
CCopModule::HasOption(CString option)
{
	return ciArgParser::getInstance()->isSet(option);
}

BOOL
CCopModule::GetTimeLine(SHostInfo& info,int period/*=5*/,int side/*=1*/)
{
	DWORD dwStart = GetTickCount();

	if(info.siteId.IsEmpty() || info.siteId == "*" || info.hostId.IsEmpty() || info.hostId == "*"){
		return false;
	}
	info.timeLineInfo.clear();

	CCI::CCI_StringList aHostList;
	aHostList.length(1);
	aHostList[0] = info.hostId;

	_HostInfoMap aHostInfoMap;
	aHostInfoMap.insert(_HostInfoMap::value_type((const char*)info.hostId,&info));

	bool	retval = _GetTimeLineNextSchedule(info.mgrId, &aHostList, &aHostInfoMap, period, side);

	TraceLog(("Total Time=[%u msec]", GetTickCount()-dwStart));
	return retval;
}

BOOL
CCopModule::GetTimeLine(SHostInfo& hostInfo, CList<CString>& timeLineList,int period/*=5*/,int side/*=1*/)
{
	CCI::CCI_StringList cci_TimeLineList;
	bool retval = _GetTimeLine(hostInfo.mgrId, hostInfo.hostId,cci_TimeLineList,period,side);
	int hLen = cci_TimeLineList.length();
	for(int i=0;i<hLen;i++){
		CString aTimeLine = (const char*)cci_TimeLineList[i].in();
		aTimeLine.Replace("|"," ");
		timeLineList.AddTail(aTimeLine);
	}
	return retval;
}

BOOL
CCopModule::_GetTimeLine(const char* mgrId,const char* hostId,CCI::CCI_StringList& timeLineList ,int period,int side)
{
	TraceLog(("_GetTimeLine(%s,%s) start", mgrId, hostId));
	DWORD dwStart = GetTickCount();

	cciCall aCall("getTimeLine");
	cciEntity aEntity("AGTServer=%s",mgrId);
	aCall.setEntity(aEntity);

	aCall.addItem("side", ciShort(side));
	aCall.addItem("period", ciShort(period));
	cciAttributeList attrs;

	CCI::CCI_StringList cci_HostList;
	cci_HostList.length(1);
	cci_HostList[0] = CORBA::string_dup(hostId);

	attrs.addItem("hostList", cci_HostList);

	timeLineList.length(1);
	timeLineList[0] = CORBA::string_dup("GET_ALL_TIMELINE");  // IDL 을 고치지 않기 위한 꼼수이다.
	                                                          // 이 경우에는 하나의 host 에대해서 timeline 전체를 가져오게 된다.
	attrs.addItem("timeLineListList", timeLineList);
	aCall.addItem("attributes", attrs);

	int counter=0;

	try {
		//TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
		TraceLog(("call Time1 =[%u msec]", GetTickCount()-dwStart));

		ciTime now;
		cciReply reply;
		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) return false;
			reply.unwrap();
			reply.getItem("timeLineListList",	timeLineList);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	
	TraceLog(("_GetTimeLine(%s) Lap Time =[%u msec], counter = %d", hostId, GetTickCount()-dwStart, timeLineList.length()));
	return true;
}

BOOL
CCopModule::GetTimeLine(HostInfoList& lsHost,int period/*=5*/,int side/*=1*/)
{
	DWORD dwStart = GetTickCount();

	typedef map<ciString,_HostInfoMap*>			_HostInfoMapMap;		//key=mgrId
	typedef map<ciString,CCI::CCI_StringList*>	_HostListMap;	//key=mgrId

	_HostInfoMapMap _aHostInfoMapMap;
	_HostListMap	_aHostListMap;

	int counter=0;
	POSITION pos = lsHost.GetHeadPosition();
	while(pos){
		SHostInfo& info = lsHost.GetAt(pos);
		if(info.siteId.IsEmpty() || info.siteId == "*" || info.hostId.IsEmpty() || info.hostId == "*"){
			lsHost.GetNext(pos);
			continue;
		}

		info.timeLineInfo.clear();

		_HostInfoMap* aHostInfoMap=0;
		_HostInfoMapMap::iterator itr = _aHostInfoMapMap.find((const char*)info.mgrId);
		if(itr==_aHostInfoMapMap.end()){
			aHostInfoMap = new _HostInfoMap();
			_aHostInfoMapMap.insert(_HostInfoMapMap::value_type((const char*)info.mgrId,aHostInfoMap));
		}else{
			aHostInfoMap = itr->second;
		}

		CCI::CCI_StringList* aHostList=0;
		_HostListMap::iterator jtr = _aHostListMap.find((const char*)info.mgrId);
		if(jtr==_aHostListMap.end()){
			aHostList = new CCI::CCI_StringList();
			_aHostListMap.insert(_HostListMap::value_type((const char*)info.mgrId,aHostList));
		}else{
			aHostList = jtr->second;
		}

		int aHostListIdx = aHostList->length();
		aHostList->length(aHostListIdx+1);
		(*aHostList)[aHostListIdx] = info.hostId;
		aHostInfoMap->insert(map<ciString,SHostInfo*>::value_type((const char*)info.hostId,&info));

		lsHost.GetNext(pos);
		counter++;
	}
	TraceLog(("Lap Time0 =[%u msec], hostCount=%d", GetTickCount()-dwStart, counter));

	BOOL retval = false;
	_HostInfoMapMap::iterator itr = _aHostInfoMapMap.begin();
	_HostListMap::iterator jtr = _aHostListMap.begin();
	for(;itr!=_aHostInfoMapMap.end() && jtr!=_aHostListMap.end();itr++,jtr++){
		CCI::CCI_StringList* aHostList = jtr->second;
		_HostInfoMap*		aHostInfoMap = itr->second;

		if(aHostList && aHostInfoMap) {
			retval |= _GetTimeLineNextSchedule(itr->first.c_str(),aHostList,aHostInfoMap,period,side);
		}
		if(aHostList) delete aHostList;
		if(aHostInfoMap) delete aHostInfoMap;
	}

	TraceLog(("Total Time=[%u msec]", GetTickCount()-dwStart));
	return retval;
}


BOOL
CCopModule::_GetTimeLineNextSchedule(const char* mgrId,CCI::CCI_StringList* pHostList, _HostInfoMap* pHostInfoMap ,int period,int side)
{
	TraceLog(("_GetTimeLineNextSchedule(%s) start", mgrId));
	DWORD dwStart = GetTickCount();

	cciCall aCall("getTimeLine");
	cciEntity aEntity("AGTServer=%s",mgrId);
	aCall.setEntity(aEntity);

	aCall.addItem("side", ciShort(side));
	aCall.addItem("period", ciShort(period));
	cciAttributeList attrs;
	attrs.addItem("hostList", *pHostList);
	aCall.addItem("attributes", attrs);

	int counter=0;

	try {
		//TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
		TraceLog(("call Time1 =[%u msec]", GetTickCount()-dwStart));

		ciTime now;
		cciReply reply;
		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) return false;

			CCI::CCI_StringList hostList;
			CCI::CCI_StringList timeLineListList;

			reply.unwrap();
	
			reply.getItem("hostList",			hostList);	
			reply.getItem("timeLineListList",	timeLineListList);
			
			int hLen = hostList.length();
			for(int i=0;i<hLen;i++){
				const char* aHost = (const char*)hostList[i].in();
				_HostInfoMap::iterator itr = pHostInfoMap->find(aHost);
				if(itr == pHostInfoMap->end()){
					TraceLog(("ERROR: %s host is not founded",aHost));
					continue;
				}
				SHostInfo* info = itr->second;

				CString nextSchedule = timeLineListList[i].in();
				nextSchedule.Replace("|"," ");
				TraceLog(("%s Each List=%s", aHost, nextSchedule)); 
				
				if(side==1){
					info->timeLineInfo.nextSchedule1 = nextSchedule;
				}else{
					info->timeLineInfo.nextSchedule2 = nextSchedule;
				}
				counter++;
			}// hostList for
		}// reply while
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	
	TraceLog(("_GetTimeLineNextSchedule(%s) Lap Time =[%u msec], counter = %d", mgrId, GetTickCount()-dwStart, counter));
	return true;
}

BOOL CCopModule::GetPlanoList(LPCTSTR planoId, LPCTSTR szWhere, PlanoInfoList& aPlanoList, bool bUpdate/*=false*/)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=*/Plano=%s", planoId);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);


	if(szWhere && _tcsclen(szWhere) > 0) aCall.addItem("whereClause", szWhere);

	CString strProgramList;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			ciString mgrId,siteId, planoId, backgroundFile, description, query;
			ciBoolean	expand;
			unsigned short planoType;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			reply.getItem("planoId", planoId);
			reply.getItem("backgroundFile", backgroundFile);
			reply.getItem("description", description);
			reply.getItem("query", query);
			reply.getItem("expand", expand);
			reply.getItem("planoType", planoType);

			ciBoolean matched = false;
			if(bUpdate){
				POSITION pos = aPlanoList.GetHeadPosition();
				while(pos)
				{
					SPlanoInfo* oldInfo = aPlanoList.GetNext(pos);
					if(oldInfo->planoId == planoId.c_str()){
												
						oldInfo->mgrId =	mgrId.c_str();
						oldInfo->siteId =	siteId.c_str();
						oldInfo->backgroundFile =	backgroundFile.c_str();
						oldInfo->planoId =			planoId.c_str();
						oldInfo->description =		description.c_str();
						oldInfo->query =			query.c_str();
						oldInfo->expand =			expand;
						oldInfo->planoType =			planoType;
						matched = true;
						break;
					}
				}

			}

			if(!bUpdate || !matched){
				SPlanoInfo* aInfo = new SPlanoInfo;
				aInfo->mgrId =	mgrId.c_str();
				aInfo->siteId =	siteId.c_str();
				aInfo->backgroundFile =	backgroundFile.c_str();
				aInfo->planoId =			planoId.c_str();
				aInfo->description =		description.c_str();
				aInfo->query =			query.c_str();
				aInfo->expand =			expand;
				aInfo->planoType =			planoType;

				aPlanoList.AddTail(aInfo);
			}
			
			TraceLog(("planoId=%s founded", planoId.c_str()));
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::CreatePlano(SPlanoInfo* info)
{
	TraceLog(("CreatePlano(%s,%s)", info->planoId, info->backgroundFile));

	cciCall aCall("create");
	cciEntity aEntity("PM=*/Site=%s/Plano=%s", info->siteId,info->planoId);
	aCall.setEntity(aEntity);

	aCall.addItem("backgroundFile", info->backgroundFile);
	aCall.addItem("description", info->description);
	aCall.addItem("query", info->query);
	aCall.addItem("expand", (ciBoolean) info->expand);
	aCall.addItem("planoType", info->planoType);

	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			return false;
		}
	}catch(CORBA::Exception& ex){
		CString errMsg;
		errMsg.Format("Create Error =%s", ex._name());
		UbcMessageBox(errMsg);
		return false;
	}
	TraceLog(("%s plano created", info->planoId));
	return true;
}


BOOL CCopModule::SetPlano(SPlanoInfo* info)
{
	TraceLog(("SetPlano(%s)", info->planoId));

	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Plano=%s", info->siteId,info->planoId);
	aCall.setEntity(aEntity);

	aCall.addItem("backgroundFile", info->backgroundFile);
	aCall.addItem("description", info->description);
	aCall.addItem("query", info->query);
	aCall.addItem("expand", (ciBoolean)info->expand);
	aCall.addItem("planoType", info->planoType);

	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			return false;
		}
	}catch(CORBA::Exception& ex){
		CString errMsg;
		errMsg.Format("Set Error =%s", ex._name());
		UbcMessageBox(errMsg);
		return false;
	}
	TraceLog(("%s plano set", info->planoId));
	return true;
}

BOOL CCopModule::DeletePlano(SPlanoInfo* info)
{
	TraceLog(("DeletePlano(%s)", info->planoId));

	cciCall aCall("remove"); //remove plano
	cciEntity aEntity("PM=*/Site=%s/Plano=%s", info->siteId,info->planoId);
	aCall.setEntity(aEntity);

	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			return false;
		}
	}catch(CORBA::Exception& ex){
		CString errMsg;
		errMsg.Format("Delete Error =%s", ex._name());
		UbcMessageBox(errMsg);
		return false;
	}
	TraceLog(("%s plano deleted", info->planoId));
	return true;
}



int CCopModule::GetNodeList(CString& mgrId, CString& siteId, CString& planoId, CString& nodeId, int oper, int admin, NodeViewInfoList& nodeInfoList)
{
	TraceLog(("GetNodeList(%s,%s,%s,%s)", mgrId,siteId,planoId,nodeId));

	cciCall aCall("bulkget");
	cciEntity aEntity("PM=%s/Site=%s/Plano=%s/NodeView=%s", mgrId,siteId,planoId,nodeId);
	aCall.setEntity(aEntity);

	cciAny nullAny;

	ciString whereClause;

	if(oper>=0){
		char buf[100];
		sprintf(buf, "operationalState=%d", oper);	
		whereClause += buf;
	}
	if(admin>=0){
		if(!whereClause.empty()) {
			whereClause += " and ";
		}
		char buf[100];
		sprintf(buf, "adminState=%d", admin);	
		whereClause += buf;
	}
	aCall.addItem("objectClass", nullAny);
	aCall.addItem("objectId", nullAny);
	aCall.addItem("objectType", nullAny);
	aCall.addItem("x", nullAny);
	aCall.addItem("y", nullAny);
	aCall.addItem("targetName", nullAny);	
	aCall.addItem("description", nullAny);	
	aCall.addItem("nodeValue", nullAny);	
	aCall.addItem("startupTime", nullAny);
	aCall.addItem("shutdownTime", nullAny);
	aCall.addItem("hostId", nullAny);
	aCall.addItem("hostSiteId", nullAny);
	aCall.addItem("weekShutdownTime", nullAny);
	aCall.addItem("holiday", nullAny);
	aCall.addItem("operationalState", nullAny);
	aCall.addItem("adminState", nullAny);
	//aCall.addItem("ipAddress", nullAny);
	//aCall.addItem("vncPort", nullAny);

	aCall.wrap();
	aCall.addItem("whereClause",whereClause);

	int counter=0;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return 0;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return 0;

			ciString		nodeViewId;
			ciString		objectClass;	
			ciString		objectId;
			ciString		objectType;
			unsigned short	x=100;
			unsigned short	y=100;
			ciString		targetName;	
			ciString		description;	
			ciBoolean		operationalState=false;	
			ciBoolean		adminState=true;	
			long			nodeValue=0;	
			ciString		startupTime;
			ciString		shutdownTime;
			ciString		hostId;
			ciString		hostSiteId;
			CCI::CCI_StringList		holiday;
			ciString		weekShutdownTime;

			reply.unwrap();
			reply.getItem("nodeViewId", nodeViewId);
			reply.getItem("objectClass", objectClass);
			reply.getItem("objectId", objectId);
			reply.getItem("objectType", objectType);
			reply.getItem("x", x);
			reply.getItem("y", y);
			reply.getItem("targetName", targetName);	
			reply.getItem("description", description);	
			reply.getItem("operationalState", operationalState);	
			reply.getItem("adminState", adminState);	
			reply.getItem("nodeValue", nodeValue);	
			reply.getItem("startupTime", startupTime);
			reply.getItem("shutdownTime", shutdownTime);
			reply.getItem("hostId", hostId);
			reply.getItem("hostSiteId", hostSiteId);
			reply.getItem("weekShutdownTime", weekShutdownTime);
			reply.getItem("holiday", holiday);

			SNodeViewInfo aInfo;

			aInfo.mgrId =	mgrId;
			aInfo.siteId =	siteId;
			aInfo.planoId =	planoId;
			aInfo.nodeId = nodeViewId.c_str();
			aInfo.objectClass=objectClass.c_str();
			aInfo.objectId=objectId.c_str();
			aInfo.objectType=objectType.c_str();
			aInfo.x=x;
			aInfo.y=y;
			aInfo.targetName=targetName.c_str();	
			aInfo.description=description.c_str();	
			aInfo.operationalState=operationalState;	
			aInfo.adminState=adminState;	
			aInfo.nodeValue=nodeValue;	
			aInfo.startupTime=startupTime.c_str();
			aInfo.shutdownTime=shutdownTime.c_str();
			aInfo.hostId=hostId.c_str();
			aInfo.hostSiteId=hostSiteId.c_str();
			aInfo.weekShutdownTime=weekShutdownTime.c_str();
			cciStringList listBuf(holiday);
			aInfo.holiday=listBuf.toString();

			nodeInfoList.AddTail(aInfo);
			counter++;
			TraceLog(("nodeId=%s founded, holiday=%s, weekShutdownTime=%s", nodeViewId.c_str(), aInfo.holiday, aInfo.weekShutdownTime));
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return 0;
	}
	TraceLog(("%d node founded", counter));
	return counter;
}

bool CCopModule::GetNodeObjectInfo(SNodeViewInfo* nodeInfo)
{
	TraceLog(("GetNodeObjectInfo(%s)", nodeInfo->nodeId));

	cciCall aCall("get");
	cciEntity aEntity;

	ciString typeFieldName;
	ciString valueFieldName;
	ciString nameFieldName;

	if(nodeInfo->objectClass == "Host") {
		aEntity.set("PM=*/Site=%s/%s=%s", nodeInfo->hostSiteId, nodeInfo->objectClass, nodeInfo->objectId);
		typeFieldName = "hostType";
		valueFieldName = "soundVolume";
		nameFieldName = "hostName";
	}else if(nodeInfo->objectClass == "Light"){
		aEntity.set("PM=*/Site=%s/Host=%s/%s=%s", nodeInfo->hostSiteId,nodeInfo->hostId, nodeInfo->objectClass, nodeInfo->objectId);
		typeFieldName = "lightType";
		valueFieldName = "brightness";
		nameFieldName = "lightName";
	}else if(nodeInfo->objectClass == "Monitor"){
		aEntity.set("PM=*/Site=%s/Host=%s/%s=%s", nodeInfo->hostSiteId,nodeInfo->hostId, nodeInfo->objectClass, nodeInfo->objectId);
		typeFieldName = "monitorType";
		valueFieldName = "monitorUseTime";
		nameFieldName = "monitorName";
	}else{
		TraceLog(("Unknown Object Class (%s)", nodeInfo->objectClass));
		return false;
	}

	cciAny nullAny;
	aCall.addItem(typeFieldName.c_str(), nullAny);
	aCall.addItem(nameFieldName.c_str(), nullAny);	
	aCall.addItem(valueFieldName.c_str(), nullAny);	
	aCall.addItem("operationalState", nullAny);	
	aCall.addItem("adminState", nullAny);	
	aCall.addItem("hostId", nullAny);
	aCall.addItem("siteId", nullAny);
	aCall.addItem("holiday", nullAny);
	aCall.addItem("weekShutdownTime", nullAny);
	aCall.addItem("startupTime", nullAny);
	aCall.addItem("shutdownTime", nullAny);
	if(nodeInfo->objectClass == "Host") {
		aCall.addItem("ipAddress", nullAny);
		aCall.addItem("vncPort", nullAny);
	}

	aCall.setEntity(aEntity);
	aCall.wrap();

	int counter=0;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return 0;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return 0;

			ciLong			objectType;
			ciString		targetName;	
			ciBoolean		operationalState=false;	
			ciBoolean		adminState=true;	
			long			nodeValue=0;	
			ciString		hostId;
			ciString		siteId;
			ciString		shutdownTime;
			ciString		startupTime;
			ciString		holiday;
			ciString		weekShutdownTime;
			ciString		ipAddress;
			long			vncPort=0;

			reply.unwrap();
			reply.getItem(typeFieldName.c_str(), objectType);
			reply.getItem(nameFieldName.c_str(), targetName);	
			reply.getItem("operationalState", operationalState);	
			reply.getItem("adminState", adminState);	
			reply.getItem(valueFieldName.c_str(), nodeValue);	
			reply.getItem("hostId", hostId);
			reply.getItem("siteId", siteId);
			reply.getItem("shutdownTime", shutdownTime);
			reply.getItem("startupTime", startupTime);
			reply.getItem("holiday", holiday);
			reply.getItem("weekShutdownTime", weekShutdownTime);
			if(nodeInfo->objectClass == "Host") {
				reply.getItem("ipAddress", ipAddress);
				reply.getItem("vncPort", vncPort);
			}

			nodeInfo->objectType=::ToString(objectType);
			nodeInfo->targetName=targetName.c_str();	
			nodeInfo->operationalState=operationalState;	
			nodeInfo->adminState=adminState;	
			nodeInfo->nodeValue=nodeValue;	
			nodeInfo->hostId=hostId.c_str();
			nodeInfo->hostSiteId=siteId.c_str();
			nodeInfo->shutdownTime=shutdownTime.c_str();
			nodeInfo->startupTime=startupTime.c_str();
			nodeInfo->holiday=holiday.c_str();
			nodeInfo->weekShutdownTime=weekShutdownTime.c_str();
			nodeInfo->ipAddress = ipAddress.c_str();
			nodeInfo->vncPort = vncPort;
			TraceLog(("nodeId=%s founded, (holiday=%s)", nodeInfo->nodeId, nodeInfo->holiday));
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return false;
	}
	TraceLog(("%d node founded", counter));
	return true;
}


int CCopModule::CreateNode(SNodeViewInfo* nodeInfo)
{
	nodeInfo->nodeId =  nodeInfo->objectClass + "::" + nodeInfo->objectId;
	TraceLog(("CreateNode(%s)", nodeInfo->nodeId));

	NodeViewInfoList nodeInfoList;
/*
	if(0 < GetNodeList(nodeInfo->mgrId,nodeInfo->siteId,nodeInfo->planoId,nodeInfo->nodeId,nodeInfoList)){
		CString errMsg;
		errMsg.Format("%s node already exist", nodeInfo->nodeId);
		TraceLog((errMsg));
		return 0;
	}
*/
	cciCall aCall("create");
	cciEntity aEntity("PM=%s/Site=%s/Plano=%s/Node=%s", 
		nodeInfo->mgrId,
		nodeInfo->siteId,
		nodeInfo->planoId,
		nodeInfo->nodeId
	);
	aCall.setEntity(aEntity);

	aCall.addItem("objectClass", nodeInfo->objectClass);
	aCall.addItem("objectId", nodeInfo->objectId);
	aCall.addItem("x", nodeInfo->x);
	aCall.addItem("y", nodeInfo->y);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			return -1;
		}
	}catch(CORBA::Exception& ex){
		CString errMsg;
		errMsg.Format("Create Error =%s", ex._name());
		UbcMessageBox(errMsg);
		return -2;
	}
	TraceLog(("%s node created", nodeInfo->nodeId));

	nodeInfoList.RemoveAll();
	GetNodeList(nodeInfo->mgrId,nodeInfo->siteId,nodeInfo->planoId,nodeInfo->nodeId,-1,-1,nodeInfoList);
	if(0 == nodeInfoList.GetSize()){
		CString errMsg;
		errMsg.Format("%s node create failed", nodeInfo->nodeId);
		TraceLog((errMsg));
		UbcMessageBox(errMsg);
		return -3;
	}

	SNodeViewInfo createdNode =  nodeInfoList.GetHead();

	nodeInfo->adminState = createdNode.adminState;	
	nodeInfo->description = createdNode.description;	
	nodeInfo->nodeValue = createdNode.nodeValue;	
	nodeInfo->objectType = createdNode.objectType;	
	nodeInfo->operationalState = createdNode.operationalState;	
	nodeInfo->shutdownTime = createdNode.shutdownTime;	
	nodeInfo->startupTime = createdNode.startupTime;	
	nodeInfo->targetName = createdNode.targetName;	


	CString msg;
	msg.Format("node object(%s) create end (objectTye=%s)", nodeInfo->objectId, nodeInfo->objectType );
	TraceLog((msg));
	return 1;	
}

int CCopModule::SetNode(SNodeViewInfo* nodeInfo)
{
	nodeInfo->nodeId =  nodeInfo->objectClass + "::" + nodeInfo->objectId;
	TraceLog(("SetNode(%s)", nodeInfo->nodeId));

	cciCall aCall("set");
	cciEntity aEntity("PM=%s/Site=%s/Plano=%s/Node=%s", 
		nodeInfo->mgrId,
		nodeInfo->siteId,
		nodeInfo->planoId,
		nodeInfo->nodeId
	);
	aCall.setEntity(aEntity);

	aCall.addItem("x", nodeInfo->x);
	aCall.addItem("y", nodeInfo->y);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			return -1;
		}
	}catch(CORBA::Exception& ex){
		CString errMsg;
		errMsg.Format("Set Error =%s", ex._name());
		UbcMessageBox(errMsg);
		return -2;
	}
	CString msg;
	msg.Format("node object(%s) set end (objectTye=%s)", nodeInfo->objectId, nodeInfo->objectType );
	TraceLog((msg));
	return 1;	
}

int CCopModule::DeleteNode(SNodeViewInfo* nodeInfo)
{
	nodeInfo->nodeId =  nodeInfo->objectClass + "::" + nodeInfo->objectId;
	TraceLog(("DeleteNode(%s)", nodeInfo->nodeId));

	cciCall aCall("remove");  // remove node
	cciEntity aEntity("PM=%s/Site=%s/Plano=%s/Node=%s", 
		nodeInfo->mgrId,
		nodeInfo->siteId,
		nodeInfo->planoId,
		nodeInfo->nodeId
	);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			return -1;
		}
	}catch(CORBA::Exception& ex){
		CString errMsg;
		errMsg.Format("Create Error =%s", ex._name());
		UbcMessageBox(errMsg);
		return -2;
	}
	TraceLog(("%s node deleted", nodeInfo->nodeId));
	return 1;	
}

BOOL CCopModule::GetLightList(LPCTSTR szSite, LPCTSTR szLight,LPCTSTR szWhere, bool bFilter, bool bSmallColumn, LightInfoList& list)
{
	cciCall aCall;

	aCall.setDirective("bulkget");
	cciEntity aEntity("PM=*/Site=%s/Host=*/Light=%s", szSite,szLight);
	if(bFilter) aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);
	aCall.addItem("siteId", nullAny);
	aCall.addItem("hostId", nullAny);
	aCall.addItem("lightId", nullAny);
	aCall.addItem("lightName", nullAny);
	if(!bSmallColumn) {
		aCall.addItem("lightType", nullAny);
		aCall.addItem("description", nullAny);
		aCall.addItem("adminState", nullAny);
		aCall.addItem("operationalState", nullAny);
		aCall.addItem("startupTime", nullAny);
		aCall.addItem("shutdownTime", nullAny);
		aCall.addItem("holiday", nullAny);	
		aCall.addItem("brightness", nullAny);
		aCall.addItem("weekShutdownTime", nullAny);
	}
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) aCall.addItem("whereClause", szWhere);

	DWORD dwStart = GetTickCount();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));

	int nGetCount = 0;

	try {
		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			ciString  mgrId, siteId, hostId, lightId, description;
			ciString  lightName;
			ciBoolean adminState = false;
			ciBoolean operationalState = false;
			ciString startupTime, shutdownTime;
			ciLong lightType = 0;
			ciLong brightness = -1;
			ciString weekShutdownTime;
			cciStringList holiday;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			reply.getItem("hostId", hostId);
			reply.getItem("lightId", lightId);
			reply.getItem("lightName", lightName);
			if(!bSmallColumn) {
				reply.getItem("lightType", lightType);
				reply.getItem("description", description);
				reply.getItem("adminState", adminState);
				reply.getItem("operationalState", operationalState);
				reply.getItem("startupTime", startupTime);
				reply.getItem("shutdownTime", shutdownTime);

				CCI::CCI_StringList lsBuf;
				reply.getItem("holiday", lsBuf);
				holiday.set(lsBuf);
				reply.getItem("brightness", brightness);
				reply.getItem("weekShutdownTime", weekShutdownTime);
			}

			SLightInfo hiTemp;

			hiTemp.mgrId = mgrId.c_str();
			hiTemp.siteId = siteId.c_str();
			hiTemp.hostId = hostId.c_str();
			hiTemp.lightId = lightId.c_str();
			hiTemp.lightName = lightName.c_str();
			if(!bSmallColumn) {
				hiTemp.lightType = lightType;
				hiTemp.description = description.c_str();
				hiTemp.adminState = adminState;
				hiTemp.operationalState = operationalState;
				hiTemp.startupTime = startupTime.c_str();
				hiTemp.shutdownTime = shutdownTime.c_str();
				hiTemp.holiday = "";
				ciStringList lsHoliday = holiday.getStringList();
				for(ciStringList::iterator Iter = lsHoliday.begin(); Iter != lsHoliday.end(); Iter++){
					hiTemp.holiday += (*Iter).c_str();
					hiTemp.holiday += EH_SEPARATOR;
				}
				hiTemp.brightness = brightness;
				hiTemp.weekShutdownTime = weekShutdownTime.c_str();
			}
			list.AddTail(hiTemp);
			nGetCount++;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));
	return TRUE;
}
BOOL CCopModule::SetLight(SLightInfo& info, LPCTSTR szUserID)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=*/Light=%s", info.siteId, info.lightId);
	aCall.setEntity(aEntity);

	aCall.addItem("lightName", info.lightName);
	aCall.addItem("lightType", info.lightType);
	aCall.addItem("description", info.description);
	aCall.addItem("startupTime", info.startupTime);
	aCall.addItem("shutdownTime", info.shutdownTime);
	aCall.addItem("brightness", info.brightness);
	aCall.addItem("weekShutdownTime", info.weekShutdownTime);
	aCall.addItem("adminState", (ciBoolean)info.adminState);

	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용

	ciStringList lsHoliday;
	int nPos = 0;
	
	if(info.holiday.IsEmpty()){
		info.holiday = "NOSET";
	}else if(info.holiday.Find("NOSET") < 0 && info.holiday.Find(":") < 0){
		CString szTemp = info.holiday;
		info.holiday = "NOSET";
		info.holiday += EH_SEPARATOR;
		info.holiday += szTemp;
	}

	CString szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);

	while(!szToken.IsEmpty()){
		lsHoliday.push_back(ciString(szToken));
		szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);
	}

	cciStringList holiday(lsHoliday);
	CCI::CCI_StringList lsBuf = holiday.get();
	aCall.addItem("holiday", lsBuf);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}
BOOL CCopModule::CreateLight(SLightInfo& info, LPCTSTR szUserID)
{
	cciCall aCall("create");
	cciEntity aEntity("PM=*/Site=%s/Host=*/Light=%s", info.siteId, info.lightId);
	aCall.setEntity(aEntity);

	aCall.addItem("lightName", info.lightName);
	aCall.addItem("lightType", info.lightType);
	aCall.addItem("description", info.description);
	aCall.addItem("startupTime", info.startupTime);
	aCall.addItem("shutdownTime", info.shutdownTime);
	aCall.addItem("brightness", info.brightness);
	aCall.addItem("weekShutdownTime", info.weekShutdownTime);
	aCall.addItem("adminState", (ciBoolean)info.adminState);
/*
	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용
*/
	ciStringList lsHoliday;
	int nPos = 0;
	
	if(info.holiday.IsEmpty()){
		info.holiday = "NOSET";
	}else if(info.holiday.Find("NOSET") < 0 && info.holiday.Find(":") < 0){
		CString szTemp = info.holiday;
		info.holiday = "NOSET";
		info.holiday += EH_SEPARATOR;
		info.holiday += szTemp;
	}

	CString szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);

	while(!szToken.IsEmpty()){
		lsHoliday.push_back(ciString(szToken));
		szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);
	}

	cciStringList holiday(lsHoliday);
	CCI::CCI_StringList lsBuf = holiday.get();
	aCall.addItem("holiday", lsBuf);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::DelLight(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szLight)
{
	if( szSite == NULL || _tcslen(szSite) == 0 ||
		szLight == NULL || _tcslen(szLight) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove"); // remove light
	cciEntity aEntity("PM=*/Site=%s/Host=%s/Light=%s", szSite, szHost, szLight);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}


BOOL CCopModule::GetMonitorList(LPCTSTR szSite, LPCTSTR szMonitor,LPCTSTR szWhere, bool bFilter, bool bSmallColumn, MonitorInfoList& list)
{
	cciCall aCall;

	aCall.setDirective("bulkget");
	cciEntity aEntity("PM=*/Site=%s/Host=*/Monitor=%s", szSite,szMonitor);
	if(bFilter) aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);
	aCall.addItem("siteId", nullAny);
	aCall.addItem("hostId", nullAny);
	aCall.addItem("monitorId", nullAny);
	aCall.addItem("monitorName", nullAny);
	if(!bSmallColumn) {
		aCall.addItem("monitorType", nullAny);
		aCall.addItem("description", nullAny);
		aCall.addItem("adminState", nullAny);
		aCall.addItem("operationalState", nullAny);
		aCall.addItem("startupTime", nullAny);
		aCall.addItem("shutdownTime", nullAny);
		aCall.addItem("holiday", nullAny);	
		aCall.addItem("monitorUseTime", nullAny);
		aCall.addItem("weekShutdownTime", nullAny);
	}
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) aCall.addItem("whereClause", szWhere);

	DWORD dwStart = GetTickCount();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));

	int nGetCount = 0;

	try {
		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			ciString  mgrId, siteId, hostId, monitorId, description;
			ciString  monitorName;
			ciBoolean adminState = false;
			ciBoolean operationalState = false;
			ciString startupTime, shutdownTime;
			ciLong monitorType = 0;
			ciLong monitorUseTime = -1;
			ciString weekShutdownTime;
			cciStringList holiday;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			reply.getItem("hostId", hostId);
			reply.getItem("monitorId", monitorId);
			reply.getItem("monitorName", monitorName);
			if(!bSmallColumn){
				reply.getItem("monitorType", monitorType);
				reply.getItem("description", description);
				reply.getItem("adminState", adminState);
				reply.getItem("operationalState", operationalState);
				reply.getItem("startupTime", startupTime);
				reply.getItem("shutdownTime", shutdownTime);

				CCI::CCI_StringList lsBuf;
				reply.getItem("holiday", lsBuf);
				holiday.set(lsBuf);
				reply.getItem("monitorUseTime", monitorUseTime);
				reply.getItem("weekShutdownTime", weekShutdownTime);
			}

			SMonitorInfo hiTemp;

			hiTemp.mgrId = mgrId.c_str();
			hiTemp.siteId = siteId.c_str();
			hiTemp.hostId = hostId.c_str();
			hiTemp.monitorId = monitorId.c_str();
			hiTemp.monitorName = monitorName.c_str();
			if(!bSmallColumn){
				hiTemp.monitorType = monitorType;
				hiTemp.description = description.c_str();
				hiTemp.adminState = adminState;
				hiTemp.operationalState = operationalState;
				hiTemp.startupTime = startupTime.c_str();
				hiTemp.shutdownTime = shutdownTime.c_str();
				hiTemp.holiday = "";
				ciStringList lsHoliday = holiday.getStringList();
				for(ciStringList::iterator Iter = lsHoliday.begin(); Iter != lsHoliday.end(); Iter++){
					hiTemp.holiday += (*Iter).c_str();
					hiTemp.holiday += EH_SEPARATOR;
				}
				hiTemp.monitorUseTime = monitorUseTime;
				hiTemp.weekShutdownTime = weekShutdownTime.c_str();
			}
			list.AddTail(hiTemp);
			nGetCount++;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));
	return TRUE;
}
BOOL CCopModule::SetMonitor(SMonitorInfo& info, LPCTSTR szUserID)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s/Host=*/Monitor=%s", info.siteId, info.monitorId);
	aCall.setEntity(aEntity);

	aCall.addItem("monitorName", info.monitorName);
	aCall.addItem("monitorType", info.monitorType);
	aCall.addItem("description", info.description);
	aCall.addItem("startupTime", info.startupTime);
	aCall.addItem("shutdownTime", info.shutdownTime);
	aCall.addItem("monitorUseTime", info.monitorUseTime);
	aCall.addItem("weekShutdownTime", info.weekShutdownTime);
	aCall.addItem("adminState", (ciBoolean)info.adminState);

	/*
	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용
	*/
	ciStringList lsHoliday;
	int nPos = 0;
	
	if(info.holiday.IsEmpty()){
		info.holiday = "NOSET";
	}else if(info.holiday.Find("NOSET") < 0 && info.holiday.Find(":") < 0){
		CString szTemp = info.holiday;
		info.holiday = "NOSET";
		info.holiday += EH_SEPARATOR;
		info.holiday += szTemp;
	}

	CString szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);

	while(!szToken.IsEmpty()){
		lsHoliday.push_back(ciString(szToken));
		szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);
	}

	cciStringList holiday(lsHoliday);
	CCI::CCI_StringList lsBuf = holiday.get();
	aCall.addItem("holiday", lsBuf);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}
BOOL CCopModule::CreateMonitor(SMonitorInfo& info)
{
	cciCall aCall("create");
	cciEntity aEntity("PM=*/Site=%s/Host=%s/Monitor=%s", info.siteId, info.hostId, info.monitorId);
	aCall.setEntity(aEntity);

	aCall.addItem("monitorName", info.monitorName);
	aCall.addItem("monitorType", info.monitorType);
	aCall.addItem("description", info.description);
	aCall.addItem("startupTime", info.startupTime);
	aCall.addItem("shutdownTime", info.shutdownTime);
	aCall.addItem("monitorUseTime", info.monitorUseTime);
	aCall.addItem("weekShutdownTime", info.weekShutdownTime);
	aCall.addItem("adminState", (ciBoolean)info.adminState);

	/*
	// skpark 2015.2.15 IpAddress 까지 남김
	CString strUserId = szUserID;
	strUserId.Append("//");
	strUserId.Append(this->GetMyIp());
	aCall.addItem("userId", strUserId); // 로그작성용
	*/

	ciStringList lsHoliday;
	int nPos = 0;
	
	if(info.holiday.IsEmpty()){
		info.holiday = "NOSET";
	}else if(info.holiday.Find("NOSET") < 0 && info.holiday.Find(":") < 0){
		CString szTemp = info.holiday;
		info.holiday = "NOSET";
		info.holiday += EH_SEPARATOR;
		info.holiday += szTemp;
	}

	CString szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);

	while(!szToken.IsEmpty()){
		lsHoliday.push_back(ciString(szToken));
		szToken = info.holiday.Tokenize(EH_SEPARATOR, nPos);
	}

	cciStringList holiday(lsHoliday);
	CCI::CCI_StringList lsBuf = holiday.get();
	aCall.addItem("holiday", lsBuf);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::DelMonitor(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szMonitor)
{
	if( szSite == NULL || _tcslen(szSite) == 0 ||
		szMonitor == NULL || _tcslen(szMonitor) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove");  // remove monitor
	cciEntity aEntity("PM=*/Site=%s/Host=%s/Monitor=%s", szSite, szHost, szMonitor);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}


BOOL CCopModule::GetIconList(LPCTSTR szNodeId, LPCTSTR szWhere, NodeTypeInfoList& list)
{
	cciCall aCall;

	aCall.setDirective("bulkget");
	cciEntity aEntity("OD=OD/NodeType=%s", szNodeId);
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);
	aCall.addItem("nodeTypeId", nullAny);
	aCall.addItem("objectClass", nullAny);
	aCall.addItem("objectType", nullAny);
	aCall.addItem("objectState", nullAny);
	aCall.addItem("ext", nullAny);
	aCall.wrap();

	if(szWhere && _tcsclen(szWhere) > 0) aCall.addItem("whereClause", szWhere);

	DWORD dwStart = GetTickCount();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));

	int nGetCount = 0;

	try {
		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			ciString  mgrId, nodeTypeId, objectClass, ext;
			ciBoolean objectState = false;
			ciLong objectType = 0;

			reply.unwrap();
			reply.getItem("mgrId",			mgrId);
			reply.getItem("nodeTypeId",		nodeTypeId);
			reply.getItem("objectClass",	objectClass);
			reply.getItem("objectType",		objectType);
			reply.getItem("objectState",	objectState);
			reply.getItem("ext", ext);

			SNodeTypeInfo hiTemp;

			hiTemp.mgrId = mgrId.c_str();
			hiTemp.nodeTypeId = nodeTypeId.c_str();
			hiTemp.objectClass = objectClass.c_str();
			hiTemp.objectType = objectType;
			hiTemp.objectState = objectState;
			hiTemp.ext = ext.c_str();

			list.AddTail(hiTemp);
			nGetCount++;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	TraceLog(("Reply complete time : %d (msec)", GetTickCount()-dwStart));
	return TRUE;
}

BOOL CCopModule::SetIcon(SNodeTypeInfo& info)
{
	cciCall aCall("set");
	cciEntity aEntity("OD=OD/NodeType=%s", info.nodeTypeId);
	aCall.setEntity(aEntity);

	aCall.addItem("objectClass", info.objectClass);
	aCall.addItem("objectType", info.objectType);
	aCall.addItem("objectState",(ciBoolean) info.objectState);
	aCall.addItem("ext", info.ext);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::CreateIcon(SNodeTypeInfo& info)
{
	cciCall aCall("create");
	cciEntity aEntity("OD=OD/NodeType=%s", info.nodeTypeId);
	aCall.setEntity(aEntity);

	aCall.addItem("objectClass", info.objectClass);
	aCall.addItem("objectType", info.objectType);
	aCall.addItem("objectState",(ciBoolean) info.objectState);
	aCall.addItem("ext", info.ext);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}
	try {
		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				return FALSE;
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}
	return TRUE;
}

BOOL CCopModule::DelIcon(LPCTSTR szNodeId)
{
	if( _tcslen(szNodeId) == 0 )
	{
		return FALSE;
	}

	cciCall aCall("remove");  // remove nodeType
	cciEntity aEntity("OD=OD/NodeType=%s", szNodeId);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::IsExist(LPCTSTR szEntity, BOOL &bExist)
{
	cciCall aCall("get");
	cciEntity aEntity(szEntity);
	aCall.setEntity(aEntity);

	bExist = FALSE;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call())
		{
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
		while(aCall.hasMore())
		{
			cciReply reply;
			if ( ! aCall.getReply(reply))
			{
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			bExist = TRUE;
			break;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::ChangeParent(LPCTSTR szSiteId, LPCTSTR szNewParentId)
{
	cciCall aCall("set");
	cciEntity aEntity("PM=*/Site=%s", szSiteId);
	aCall.setEntity(aEntity);

	aCall.addItem("parentId", szNewParentId);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		UbcMessageBox("Save ERROR !!!");
		return FALSE;
	}
	TraceLog(("%s succeed", aCall.toString()));
	return TRUE;	
}


BOOL CCopModule::SetProperty(std::string strSiteId,std::string strHostId,std::string strName,std::string strValue)
{
	cciCall aCall("setProperty");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", strSiteId.c_str(), strHostId.c_str());
	aCall.setEntity(aEntity);

	aCall.addItem("name", strName);
	aCall.addItem("value", strValue);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}

		cciReply reply;

		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) {
				UbcMessageBox("setProperty Fail (Check your server)");
				return false;
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		UbcMessageBox("setProperty Fail (Unknown Exception)");
		return false;
	}
	return true;
}
BOOL CCopModule::GetProperty(std::string strSiteId,std::string strHostId,std::string strName)
{
	cciCall aCall("getProperty");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", strSiteId.c_str(), strHostId.c_str());
	aCall.setEntity(aEntity);

	aCall.addItem("name", strName);
	aCall.addItem("value", "");

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}

		cciReply reply;

		while(aCall.getReply(reply))
		{
			if(!CopErrCheck(reply)) {
				UbcMessageBox("getProperty Fail (Check your server)");
				return false;
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		UbcMessageBox("getProperty Fail (Unknown Exception)");
		return false;
	}
	return true;
}

BOOL CCopModule::PingTest(std::string strSiteId,std::string strHostId)
{
	cciCall aCall("ping");
	cciEntity aEntity("PM=*/Site=%s/Host=%s", strSiteId.c_str(), strHostId.c_str());
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				//UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return false;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		UbcMessageBox("Ping test fail (Unknown Exception)");
		return false;
	}
	return true;
}

BOOL
CCopModule::DownloadOnlyProperty(std::string strSiteId,std::string strHostId,int downloadOnly)
{
	TraceLog(("DownloadOnlyProperty(PM=*/Site=%s/Host=%s %d)", strSiteId.c_str(), strHostId.c_str(), downloadOnly));

	bool retval = false;

	if(downloadOnly == 1){
		retval &= SetProperty(strSiteId, strHostId,"PLUS.UBCStarter.BROWSER.PROPERTIES", "+download_only");
		retval &= SetProperty(strSiteId, strHostId,"SET.UBCStarter.3RDPARTY_PLAYER.AUTO_STARTUP", "true");
	}

	if(downloadOnly == 2){
		retval &= SetProperty(strSiteId, strHostId,"MINUS.UBCStarter.BROWSER.PROPERTIES", "+download_only");
		retval &= SetProperty(strSiteId, strHostId,"SET.UBCStarter.3RDPARTY_PLAYER.AUTO_STARTUP", "false");
	}
	return retval;
}

BOOL
CCopModule::CreateHost(LPCSTR lpszIniPath)
{
	ubcConfig ini(lpszIniPath);

	//
	ciString hostIdList;
	ini.get("ROOT", "AddHostIdList", hostIdList);
	ciDEBUG(1, ("AddHostIdList(%s)", hostIdList.c_str()) );

	if( hostIdList.empty() )
	{
		ciERROR( ("No Host founded") );
		return 0;
	}

	ciString errMsg;
	ciLong errCode;//,ciString& customer
	int counter=0;
	ciStringTokenizer aTokens(hostIdList.c_str(), ",");

	while( aTokens.hasMoreTokens() )
	{
		ciString hostId = aTokens.nextToken();

		ciString mgrId,siteId,hostName,macAddress,edition,enterpriseKey;
		ciTime authDate;

		mgrId = "1"; //ini.get(hostId.c_str(), "mgrId",         mgrId);
		ini.get(hostId.c_str(), "siteId",        siteId);
		//ini.get(hostId.c_str(),"hostId",         hostId);
		ini.get(hostId.c_str(), "hostName",      hostName);
		ini.get(hostId.c_str(), "macAddress",    macAddress);
		ini.get(hostId.c_str(), "edition",       edition);
		ini.get(hostId.c_str(), "authDate",      authDate);
		ini.get(hostId.c_str(), "enterpriseKey", enterpriseKey);

		if( createHost(mgrId.c_str(), siteId.c_str(), hostId.c_str(), hostName.c_str(), 
			macAddress.c_str(), edition.c_str(), authDate, enterpriseKey.c_str(), errMsg, errCode) )
		{
			ciDEBUG(1, ("%s host created", hostId.c_str()) );
			counter++;
		}
		else
		{
			ciERROR( ("%s host create failed", hostId.c_str()) );
		}
	}
	return counter;
}

BOOL
CCopModule::CreateHost(LPCSTR mgrId, LPCSTR siteId, LPCSTR hostId)
{
	ciTime authDate;
	ciString errMsg;
	ciLong errCode;
	return createHost(mgrId, siteId, hostId, hostId, "", "ENT", authDate, "", errMsg, errCode);
}

BOOL
CCopModule::RemoveHost(LPCSTR lpszIniPath)
{
	ubcConfig ini(lpszIniPath);

	//
	ciString hostIdList;
	ini.get("ROOT", "DeleteHostIdList", hostIdList);
	ciDEBUG(1, ("DeleteHostIdList(%s)", hostIdList.c_str()) );

	if( hostIdList.empty() )
	{
		ciERROR( ("No Host founded") );
		return 0;
	}

	ciString errMsg;
	ciLong errCode;//,ciString& customer
	int counter=0;
	ciStringTokenizer aTokens(hostIdList.c_str(), ",");

	while( aTokens.hasMoreTokens() )
	{
		ciString hostId = aTokens.nextToken();

		if( removeHost(hostId.c_str(), errMsg, errCode) )
		{
			ciDEBUG(1, ("%s host deleted", hostId.c_str()) );
			counter++;
		}
		else
		{
			ciERROR( ("%s host delete failed", hostId.c_str()) );
		}
	}
	return counter;
}

BOOL 
CCopModule::createHost(LPCSTR mgrId, LPCSTR siteId, LPCSTR hostId, LPCSTR hostName, 
					  LPCSTR macAddress, LPCSTR edition, ciTime& authDate, LPCSTR enterprise, 
					  ciString& errMsg, ciLong& errCode)
{
	ciDEBUG(1, ("createHost(%s,%s,%s)", mgrId, siteId, hostId) );

	cciCall aCall("create");

	cciEntity aEntity("PM=%s/Site=%s/Host=%s", mgrId, siteId, hostId);
	aCall.setEntity(aEntity);

	aCall.addItem("hostName",         hostName);
	aCall.addItem("macAddress",       macAddress);
	aCall.addItem("edition",          edition);
	//aCall.addItem("authDate",         authDate);
	//aCall.addItem("enterpriseKey",    enterprise);
	aCall.addItem("adminState",       ciTrue);
	aCall.addItem("operationalState", ciFalse);
	//ciTime now;
	//aCall.addItem("createTime",       now);
	aCall.wrap();

	int counter = 0;
	try
	{
		if(!aCall.call())
		{
			ciERROR(("create %s fail", aEntity.toString()));
			return FALSE;
		}
		ciDEBUG(1, ("host(%s) created", hostId) );
	}
	catch(CORBA::Exception& ex)
	{
		ciERROR( (ex._name()) );
		return FALSE;
	}

	cciReply reply;
	while(aCall.getReply(reply))
	{
		ciDEBUG(9, ("create %s succeed", aEntity.toString()) );

		reply.getItem("ERROR_CODE", errCode);
		reply.getItem("ERROR_MSG", errMsg);
		break;
	}

	return TRUE;
}

BOOL
CCopModule::removeHost(LPCSTR hostId, ciString& errMsg, ciLong& errCode)
{
	ciDEBUG(1, ("removeHost(%s)", hostId) );

	cciCall aCall("remove"); // remove host OK

	cciEntity aEntity("PM=*/Site=*/Host=%s", hostId);
	aCall.setEntity(aEntity);

	if(atof(m_strVersion) >= 5.9) {
		aCall.addItem("trashId_", m_strLoginId);
		aCall.wrap();
	}


	int counter = 0;
	try
	{
		if(!aCall.call())
		{
			ciERROR(("remove %s fail", aEntity.toString()));
			return FALSE;
		}
		//if(!_ApproveErrorCheck(aCall)) return FALSE;

		ciDEBUG(1, ("host(%s) removed", hostId) );

	}
	catch(CORBA::Exception& ex)
	{
		ciERROR( (ex._name()) );
		return FALSE;
	}

	cciReply reply;
	while(aCall.getReply(reply))
	{
		ciDEBUG(9, ("remove %s succeed", aEntity.toString()) );

		reply.getItem("ERROR_CODE", errCode);
		reply.getItem("ERROR_MSG", errMsg);
		break;
	}

	return TRUE;
}

BOOL CCopModule::GetCanInfoList(CString& strClassName, CString& strName, CString& strSite, CString& strUserId, CString& szWhere , TrashCanInfoList& list)
{
	cciCall aCall("bulkget");

	CString entityStr = "PM=*/Site=";
	if(strClassName != "Site") {  // Site 의 경우, 지워진 사이트는 자식사이트 목록에 나올수가 없으므로....필터를 적용할 수 없다.
		entityStr += strSite;
	}else{
		entityStr += "*";
	}
	entityStr += "/TrashCan=";
	/*
	CString additionalWhereClause;
	if(!strSite.IsEmpty() && (nameAttrName != "siteName" || strName.IsEmpty()) ){
		CStringList children;
		if(FindRecursiveChildren(strSite,children,"")){
			POSITION pos = children.GetHeadPosition();
			CString childrenStr;
			while(pos)
			{
				CString child = children.GetNext(pos);
				if(!childrenStr.IsEmpty()){
					childrenStr += ",";
				}
				childrenStr += "'";
				childrenStr += child;
				childrenStr += "'";
			}
			if(!childrenStr.IsEmpty()){
				additionalWhereClause.Format("(siteId in (%s))", childrenStr);
			}
		}
	}
	*/
	CString nameWhere = "(entityClass = '";
	nameWhere += strClassName;
	nameWhere += "')";
	if(!strName.IsEmpty()) {
		nameWhere += " AND (nameValue like '%%";
		nameWhere +=  strName;
		nameWhere += "%%')";
	}

	if(szWhere.IsEmpty()) {
		szWhere = nameWhere;
	}else{
		CString buf = szWhere;
		szWhere = nameWhere;
		szWhere += " AND " ;
		szWhere += buf;
	}
	/*
	if(!additionalWhereClause.IsEmpty()) {
		if(!szWhere.IsEmpty()) {
			szWhere += " AND " ;
			szWhere += additionalWhereClause;
		}else{
			szWhere = additionalWhereClause;
		}
	}
	*/
	if(!szWhere.IsEmpty()) {
		szWhere += _T(" order by eventTime desc");
	}else{
		szWhere = _T(" order by eventTime desc");
	}

	cciEntity aEntity(entityStr);
	if(strClassName != "Site") {  // Site 의 경우, 지워진 사이트는 자식사이트 목록에 나올수가 없으므로....필터를 적용할 수 없다.
		aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	}
	aCall.setEntity(aEntity);

	cciAny nullAny;

	aCall.addItem("mgrId", nullAny);
	aCall.addItem("trashCanId", nullAny);
	aCall.addItem("siteId", nullAny);
	aCall.addItem("idValue", nullAny);
	aCall.addItem("nameValue", nullAny);
	aCall.addItem("entity", nullAny);
	aCall.addItem("attrList", nullAny);
	aCall.addItem("eventTime", nullAny);
	aCall.addItem("userId", nullAny);
	aCall.wrap();

	TraceLog(("bulkget entity=%s, whereClause=%s",entityStr, szWhere));

	if(!szWhere.IsEmpty()) {
		AddItem(aCall, "whereClause", szWhere);
	}

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			if(reply.length() == 0) continue;

			STrashCanInfo stTemp;

			ciString	mgrId    ;
			ciString	trashCanId;
			ciString	siteId   ;
			ciString	idValue   ;
			ciString	nameValue  ;
			CCI::CCI_Entity	entityBuf;
			ciString	attrList;
			ciTime		eventTime;
			ciString	userId;

			reply.unwrap();
			reply.getItem("mgrId"		, mgrId		);	stTemp.mgrId		= mgrId.c_str();
			reply.getItem("trashCanId"	, trashCanId);	stTemp.trashCanId	= trashCanId.c_str();
			reply.getItem("siteId"		, siteId	);	stTemp.siteId		= siteId.c_str();
			reply.getItem("idValue"		, idValue	);	stTemp.idValue		= idValue.c_str();
			reply.getItem("nameValue"	, nameValue	);	stTemp.nameValue	= nameValue.c_str();
			reply.getItem("attrList"	, attrList	);	stTemp.attrList		= attrList.c_str();
			reply.getItem("eventTime"	, eventTime	);	stTemp.eventTime	= eventTime.getTime();
			reply.getItem("userId"		, userId	);	stTemp.userId		= userId.c_str();
			if(reply.getItem("entity"		, entityBuf	)){
				cciEntity aEntity(entityBuf);			
				stTemp.entity		= aEntity.toString();
			}else{
				TraceLog(("Entity get failed"));
			}

			list.AddTail(stTemp);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CCopModule::CanRecovery(CString& strClassName, CString& strSite, CString& strId)
{
	cciCall aCall("restore");

	CString entityStr = "PM=*/Site=";
	entityStr += strSite;
	entityStr += "/TrashCan=";
	entityStr += strId;

	cciEntity aEntity(entityStr);
	if(strClassName != "Site") {
		aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	}
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}
BOOL CCopModule::CanEliminate(CString& strClassName, CString& strSite, CString& strId)
{
	TraceLog(("CanEliminate(%s,%s)", strClassName, strId));

	cciCall aCall("remove");  // remove TrashCan

	CString entityStr = "PM=*/Site=";
	entityStr += strSite;
	entityStr += "/TrashCan=";
	entityStr += strId;

	cciEntity aEntity(entityStr);
	if(strClassName != "Site"){
		aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	}
	aCall.setEntity(aEntity);


	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL 
CCopModule::_ApproveErrorCheck(cciCall& call)
{
	if(call.hasMore())
	{
		cciReply reply;
		if (!call.getReply(reply)) {
			TraceLog(("Failed : cciCall::getReply()"));
		}
		ciLong errCode=0;
		reply.getItem("ERROR_CODE", errCode);
		ciString errMsg="";
		reply.getItem("ERROR_MSG", errMsg);
		TraceLog(("ERROR : %s:%d", errMsg.c_str(), errCode));
		if(errCode == -99){
			UbcMessageBox(LoadStringById(IDS_ERRORMSG001));
			return FALSE;
		}
	}
	return TRUE;
}

BOOL
CCopModule::getApproveInfoList(LPCSTR lpszWhere, APPROVE_INFO_LIST& ApproveInfoList)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("OD=*/Approve=*");
	aCall.setEntity(aEntity);

	if( lpszWhere!=NULL && strlen(lpszWhere)>0 )
	{
		AddItem(aCall, "whereClause", lpszWhere);
	}

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return FALSE;

			//
			ciString	mgrId;
			ciString	approveId;
			ciLong		approveState = 0;
			ciString	action;
			cciEntity	target;//ciString	target;
			ciString	details;
			ciString	requestedId;
			ciTime		requestedTime;
			ciString	approvedId;
			ciTime		approvedTime;
			ciString	description1;
			ciString	description2;

			//
			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("approveId", approveId);
			reply.getItem("approveState", approveState);
			reply.getItem("action", action);
			reply.getItem("target", target);
			reply.getItem("details", details);
			reply.getItem("requestedId", requestedId);
			reply.getItem("requestedTime", requestedTime);
			reply.getItem("approvedId", approvedId);
			reply.getItem("approvedTime", approvedTime);
			reply.getItem("description1", description1);
			reply.getItem("description2", description2);

			//
			APPROVE_INFO* info = new APPROVE_INFO;
			info->strMgrId			= mgrId.c_str();
			info->strApproveId		= approveId.c_str();
			info->eApproveState		= (APPROVE_INFO::APPROVE_STATUS)approveState;
			info->strAction			= action.c_str();
			info->strTarget			= target.toString();//target.c_str();
			info->strDetails		= details.c_str();
			info->strRequestedId	= requestedId.c_str();
			//info->strRequestedTime	= requestedTime;
			info->strApprovedId		= approvedId.c_str();
			//info->strApprovedTime	= approvedTime;
			info->strDescription1	= description1.c_str();
			info->strDescription2	= description2.c_str();

			CTime tmRequestedTime(requestedTime.getTime());
			info->strRequestedTime = tmRequestedTime.Format(_T("%Y/%m/%d %H:%M:%S"));

			CTime tmApprovedTime(approvedTime.getTime());
			info->strApprovedTime = tmApprovedTime.Format(_T("%Y/%m/%d %H:%M:%S"));

			//
			if( info->strTarget.Find("Host=") > 0 )			info->eApproveType = APPROVE_INFO::eTypeHost;
			else if( info->strTarget.Find("Program=") > 0 )	info->eApproveType = APPROVE_INFO::eTypeProgram;
			else if( info->strTarget.Find("BP=") > 0 )		info->eApproveType = APPROVE_INFO::eTypeBP;
			else if( info->strTarget.Find("Site=") > 0 )	info->eApproveType = APPROVE_INFO::eTypeSite;

			ApproveInfoList.Add(info);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL
CCopModule::setApprove(LPCSTR lpszMgrId, LPCSTR lpszApproveId, APPROVE_INFO::APPROVE_STATUS eNewApproveState)
{
	cciCall aCall("set");
	cciEntity aEntity("OD=%s/Approve=%s", lpszMgrId, lpszApproveId);
	aCall.setEntity(aEntity);

	aCall.addItem("approveState", (ciLong)eNewApproveState);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}
	TraceLog(("%s succeed", aCall.toString()));
	return TRUE;
}

BOOL
CCopModule::removeApprove(LPCSTR lpszMgrId, LPCSTR lpszApproveId)
{
	cciCall aCall("remove");
	cciEntity aEntity("OD=%s/Approve=%s", lpszMgrId, lpszApproveId);
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}
	TraceLog(("%s succeed", aCall.toString()));
	return TRUE;
}

int
CCopModule::FindRecursiveChildren(LPCSTR parentId,CStringList& children, const char* additionalWhereClause)
{
	TraceLog(("FindRecursiveChildren(%s, where=%s)", parentId, additionalWhereClause));
	if(eSiteUser == m_Authority)
	{
		return 0;
	}

	CStringList siteList;
	if(FindChildren(parentId, siteList, additionalWhereClause) > 0) {
		POSITION pos = siteList.GetHeadPosition();
		while(pos)
		{
			CString site = siteList.GetNext(pos);
			children.AddTail(site);
			FindRecursiveChildren(site, children, additionalWhereClause);
		}	
	
	}
	return children.GetSize();
}


int
CCopModule::FindChildren(LPCSTR parentSite, CStringList& children, const char* additionalWhereClause)
{
	TraceLog(("FindChildren(%s, where=%s)", parentSite, additionalWhereClause));
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=*");
	aCall.setEntity(aEntity);

	cciAny nullAny;
	aCall.addItem("mgrId", nullAny);
	aCall.addItem("siteId", nullAny);
	aCall.wrap();

	CString szWhere ;
	if(additionalWhereClause && strlen(additionalWhereClause)) {
		szWhere.Format("parentId='%s'and (%s)", parentSite, additionalWhereClause);
	}else{
		szWhere.Format("parentId='%s'", parentSite);
	}
	AddItem(aCall, "whereClause", szWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return 0;
		}
		
		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(reply)) return 0;
			reply.unwrap();

			ciString siteId;
			reply.getItem("siteId", siteId);

			if(siteId.length() == 0)	continue;

			children.AddTail(siteId.c_str());
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return 0;
	}
	return children.GetSize();

}