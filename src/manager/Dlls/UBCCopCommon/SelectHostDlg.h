#pragma once

#ifndef IDD_SELECT_HOST
#define IDD_SELECT_HOST	25062
#endif//IDD_SELECT_HOST

#include "afxwin.h"
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\CheckComboBox.h"

// CSelectHostDlg 대화 상자입니다.

class AFX_EXT_CLASS CSelectHostDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectHostDlg)

public:
	CSelectHostDlg(LPCTSTR szCustomer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSelectHostDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SELECT_HOST };

	enum { eCheck, eHostName, eHostId, eGroup, eMaxCol };

	// Output
	CString				m_strHostName;
	CString				m_strHostId;

	int					m_nAuthority;
	CString				m_strSiteId;
	CString				m_strIniPath;

protected:
	CString				m_strCustomer;

	CString				m_szColum[eMaxCol];
	HostInfoList		m_lsInfoList;

	CEdit				m_editFilterHostName;
	CEdit				m_editFilterHost;
	//CComboBox			m_cbxHostType;
	CCheckComboBox		m_cbxHostType;
	CComboBox			m_cbxSite;
	CComboBox			m_cbxIncludeContents;
	CComboBox			m_cbxPlayingPackage;
	CComboBox			m_cbOnOff;
	CComboBox			m_cbOperation;
//	CEdit				m_editFilterDescription;
	CHoverButton		m_bnRefresh;
	CUTBListCtrlEx		m_lcList;
	CHoverButton		m_bnOK;
	CHoverButton		m_bnCancel;

	CString				m_strSite;
	CString				m_strIncludeContentsID;
	CString				m_strPlayingPackageID;

	void				InitList();
	void				RefreshList();

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonPlanRefresh();
	afx_msg void OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnCbnSelchangeCbSite();
	afx_msg void OnCbnSelchangeCbIncludecontents();
	afx_msg void OnCbnSelchangeCbPlayingPackage();
	CComboBox m_cbFilterTag;
};
