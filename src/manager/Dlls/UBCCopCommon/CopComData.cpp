#include "stdafx.h"
#include "CopComData.h"

CCopComData* CCopComData::m_pObject = NULL;

CCopComData::CCopComData()
{
}

CCopComData::~CCopComData()
{
}

CCopComData* CCopComData::GetObject()
{
	if(!m_pObject){
		m_pObject = new CCopComData;
	}
	return m_pObject;
}

void CCopComData::Release()
{
	if(m_pObject){
		delete m_pObject;
		m_pObject = NULL;
	}
}