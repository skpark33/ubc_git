#pragma once

#include "UBCCopCommon\CopModule.h"
#include "Control\ReposControl2.h"
#include "common\HoverButton.h"

#ifndef IDD_SITE_SELECT_DLG
#define IDD_SITE_SELECT_DLG	25168
#endif//IDD_SITE_SELECT_DLG

// CKeyIgnoreTreeCtrl

class CKeyIgnoreTreeCtrl : public CTreeCtrl
{
	DECLARE_DYNAMIC(CKeyIgnoreTreeCtrl)

public:
	CKeyIgnoreTreeCtrl() {};
	virtual ~CKeyIgnoreTreeCtrl() {};

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
};



// CSiteSelectDlg 대화 상자입니다.

class AFX_EXT_CLASS CSiteSelectDlg : public CDialog
{
	DECLARE_DYNAMIC(CSiteSelectDlg)

public:
	CSiteSelectDlg(LPCTSTR szCustomer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSiteSelectDlg();
// Dialog Data
	enum { IDD = IDD_SITE_SELECT_DLG };

	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	//enum { IDD = IDD_SITE_SELECT_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CString				m_strCustomer;

	static CMapStringToPtr m_mapChildSiteInfo;

	SiteInfoList		m_listSelectSiteInfo;

	bool				m_bSingleSelect;
	CReposControl2		m_repos;

	void				_DeleteAllItems(HTREEITEM item);
	void				DeleteAllItems();

	void				_SetAllSelection(HTREEITEM itemCurrent, HTREEITEM item, BOOL bCheck);
	void				SetAllSelection(HTREEITEM itemCurrent, BOOL bCheck);

	HTREEITEM			_GetParentItem(HTREEITEM item, LPCSTR lpszParentId);
	HTREEITEM			GetParentItem(LPCSTR lpszParentId);

	void				_GetAllSelection(SiteInfoList& siteInfoList, HTREEITEM item);
	void				GetAllSelection(SiteInfoList& siteInfoList);

	void				InitControl();
	void				RefreshTree();
	HTREEITEM			InsertRootItem();
	int					InsertTreeItem(HTREEITEM itemParent, CString strParentId, int nCallDepth);
	SiteInfoList*		GetChildSiteInfo(CString strParentId, BOOL bCallServer = TRUE);

	static CWinThread*	m_pQueryThread;
	static HWND			m_hReplyWnd;
	static UINT			QueryThread(LPVOID param);
public:
	static void			PrepareSiteList();

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);

	CKeyIgnoreTreeCtrl	m_tcSite;
	CHoverButton		m_btnSelect;
	CHoverButton		m_btnCancel;
	CHoverButton		m_btnRefreshAll;

	void				SetSingleSelection(bool bSingleSelect) { m_bSingleSelect=bSingleSelect; };

	bool				GetSelectSiteInfo(SSiteInfo& siteInfo);
	bool				GetSelectSiteInfoList(SiteInfoList& siteInfoList);

	afx_msg void OnNMDblclkTreeSite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickTreeSite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBnRefreshAll();
	afx_msg void OnTvnItemexpandedTreeSite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT	OnRefreshList(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedOk();
};
