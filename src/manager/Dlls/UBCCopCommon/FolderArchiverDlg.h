#pragma once
#include "afxcmn.h"
#include "common\progressctrlex.h"


// CFolderArchiverDlg 대화 상자입니다.

class CFolderArchiverDlg : public CDialog
{
	DECLARE_DYNAMIC(CFolderArchiverDlg)

public:
	CFolderArchiverDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFolderArchiverDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FOLDER_ARCHIVER };

	// Input param
	CString			m_strInFolderPath;
	CString			m_strTargetFileName;

protected:
	CString			m_strTotalSize;
	CString			m_strCurSize;
	CProgressCtrlEx	m_ctrlProgress;

	ULONGLONG		GetFolderSize(CString strInPath);
	BOOL			FolderArchiving(CString strInPath);

	BOOL		m_bRunning;
	CWinThread* m_pThread;
	static UINT	ThreadProc(LPVOID param);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnCompleteArchive(WPARAM, LPARAM);
};
