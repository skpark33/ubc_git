// SplitStatic.cpp : 구현 파일입니다.
//

#include "stdafx.h"
//#include "FrameManager.h"
#include "SplitStatic.h"


// CSplitStatic

IMPLEMENT_DYNAMIC(CSplitStatic, CStatic)

CSplitStatic::CSplitStatic()
{
	m_bCapture = false;

	//skpark 2013.3.28
	m_mouse_action = eMOUSE_MOVE;
	m_cx = 0;
	m_cy = 0;
	m_verticalEnd=0;
	m_horizontalEnd=0;
	//skpark end

}

CSplitStatic::~CSplitStatic()
{
}


BEGIN_MESSAGE_MAP(CSplitStatic, CStatic)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()



// CSplitStatic 메시지 처리기입니다.



void CSplitStatic::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CStatic::OnPaint()을(를) 호출하지 마십시오.

//	CWnd::DefWindowProc(WM_PAINT, (WPARAM)dc.GetSafeHdc(), 0);

	CRect client_rect;
	GetClientRect(client_rect);

	for(int x=client_rect.left+client_rect.Width()/3; x<client_rect.right-client_rect.Width()/3; x+=5)
	{
		dc.FillSolidRect(x+1,client_rect.top+1,2,2, RGB(255,255,255));
		dc.FillSolidRect(x,client_rect.top,2,2, RGB(160,160,160));

		dc.FillSolidRect(x+1,client_rect.top+1+3,2,2, RGB(255,255,255));
		dc.FillSolidRect(x,client_rect.top+3,2,2, RGB(160,160,160));
	}
}

void CSplitStatic::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CStatic::OnLButtonDown(nFlags, point);

	m_bCapture = true;
	SetCapture();
	m_pointStart = point;

	// skpark 2013.3.27  스플리터를 마우스로 누른 경우에 스플리터가 열리고 닫힘 동작을 정의할 수 있도록
	// 윈도우 메시지를 추가하였다.  
	//현재 위치가 처음 그려진 위치라면 VerticalEnd 만큼 이동하고
	//아니면 -1*VerticalEnd 만큼 이동한다.
	MovePosition();
}

void CSplitStatic::MovePosition()
{
	if(m_mouse_action == eMOUSE_CLICK) {
		if( m_cy >=0 && m_cx >=0 ){
			// 클릭한 위치가 처음 그려진 위치에 있다.  
			m_cy = (-1) * m_verticalEnd;
			m_cx = (-1) * m_horizontalEnd;
		}else{
			m_cy =  m_verticalEnd;
			m_cx =  m_horizontalEnd;
		}
		GetParent()->SendMessage(WM_LBTDOWN_SPLITER, m_cx, m_cy);
	}
	//skpark end
}

void CSplitStatic::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CStatic::OnLButtonUp(nFlags, point);

	m_bCapture = false;
	ReleaseCapture();
}

void CSplitStatic::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CStatic::OnMouseMove(nFlags, point);
	if(m_mouse_action == eMOUSE_MOVE)  //skpark 2013.3.28 
	{
		if(m_bCapture)
		{
			int cx = point.x - m_pointStart.x;
			int cy = point.y - m_pointStart.y;

			GetParent()->SendMessage(WM_MOVE_SPLITER, cx, cy);
		}
	}
}
