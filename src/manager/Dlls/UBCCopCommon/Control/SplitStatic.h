#pragma once

#define		WM_MOVE_SPLITER		(WM_USER+900)
#define		WM_LBTDOWN_SPLITER		(WM_USER+901)  //skpark 2013.3.28
// CSplitStatic

class CSplitStatic : public CStatic
{
	DECLARE_DYNAMIC(CSplitStatic)

public:
	CSplitStatic();
	virtual ~CSplitStatic();

	//skpark 2013.3.28
	enum { eMOUSE_MOVE, eMOUSE_CLICK };

protected:
	DECLARE_MESSAGE_MAP()

	bool	m_bCapture;
	CPoint	m_pointStart;

	//skpark 2013.3.28
	int m_mouse_action;
	int m_verticalEnd;
	int m_horizontalEnd;
	int m_cx;
	int m_cy;
	//skpark end

public:
	//skpark 2013.3.28
	void SetVerticalEnd(int d) { m_verticalEnd = d;}
	void SetHorizontalEnd(int d) { m_horizontalEnd = d;}
	void SetMouseAction(int move_or_click) { m_mouse_action = move_or_click; }
	void GoLowerPosition() { m_cx=0; m_cy=0; } // 다시 누를 경우, 올라간다 혹은 우측으로 간다.
	void GoHigherPosition() { m_cx=-1;m_cy=-1; } // 다시 누를 경우, 내려간다 혹은 좌측으로 간다.
	void MovePosition();
	// skpark end

	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};


