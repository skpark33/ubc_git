// SelectPlanDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "copmodule.h"
#include "SelectPlanDlg.h"
#include "common\ubcdefine.h"
#include "SelectHostDlg.h"
#include "PackageSelectDlg.h"
#include "SelectUsers.h"


// CSelectPlanDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSelectPlanDlg, CDialog)

CSelectPlanDlg::CSelectPlanDlg(LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CSelectPlanDlg::IDD, pParent)
	, m_bSingleSelect ( false )
	, m_strCustomer ( szCustomer )
{

}

CSelectPlanDlg::~CSelectPlanDlg()
{
	POSITION pos = m_mapTimePlanInfo.GetStartPosition();
	while(pos)
	{
		CString strKey;
		TimePlanInfoList* pTimePlanInfoList = NULL;
		m_mapTimePlanInfo.GetNextAssoc(pos, strKey, (void*&)pTimePlanInfoList);
		if(pTimePlanInfoList) delete pTimePlanInfoList;
	}
	m_mapTimePlanInfo.RemoveAll();
}

void CSelectPlanDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PLAN, m_lcList);
	DDX_Control(pDX, IDC_BUTTON_PLAN_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDOK, m_bnSelect);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
	DDX_Control(pDX, IDC_DATE_FILTER_START, m_dtFilterStart);
	DDX_Control(pDX, IDC_DATE_FILTER_END, m_dtFilterEnd);
	DDX_Control(pDX, IDC_DP_DAILY_PLAN, m_dpDailyPlan);
}


BEGIN_MESSAGE_MAP(CSelectPlanDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSelectPlanDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSelectPlanDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_HOST, &CSelectPlanDlg::OnBnClickedButtonFilterHost)
	ON_BN_CLICKED(IDC_BUTTON_PLAN_REFRESH, &CSelectPlanDlg::OnBnClickedButtonPlanRefresh)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_REGISTER, &CSelectPlanDlg::OnBnClickedButtonFilterRegister)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PACKAGE, &CSelectPlanDlg::OnBnClickedButtonFilterPackage)
	ON_NOTIFY(NM_CLICK, IDC_LIST_PLAN, &CSelectPlanDlg::OnNMClickListPlan)
END_MESSAGE_MAP()


// CSelectPlanDlg 메시지 처리기입니다.

void CSelectPlanDlg::OnBnClickedOk()
{
	m_listSelectBpInfo.RemoveAll();

	if(m_bSingleSelect)
	{
		int nIndex = m_lcList.GetRadioSelectedIndex();

		if(nIndex >= 0)
		{
			POSITION pos = (POSITION)m_lcList.GetItemData(nIndex);
			if(pos)
			{
				SBroadcastingPlanInfo& info = m_lsInfoList.GetAt(pos);
				m_listSelectBpInfo.AddTail(info);
			}
		}
	}
	else
	{
		for(int i=0; i<m_lcList.GetItemCount(); i++)
		{
			if(!m_lcList.GetCheck(i)) continue;

			POSITION pos = (POSITION)m_lcList.GetItemData(i);
			if(pos)
			{
				SBroadcastingPlanInfo& info = m_lsInfoList.GetAt(pos);
				m_listSelectBpInfo.AddTail(info);
			}
		}
	}

	if(m_listSelectBpInfo.GetCount() <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_SELECTPLAN_MSG002));
		return;
	}

	OnOK();
}

void CSelectPlanDlg::OnBnClickedCancel()
{
	OnCancel();
}

BOOL CSelectPlanDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CTime tmCur = CTime::GetCurrentTime();
	m_dtFilterEnd.SetTime(&tmCur);
	InitDateRange(m_dtFilterEnd);

	int nAddMonth = -1;
	int nTotalMonth = tmCur.GetYear() * 12 + tmCur.GetMonth() + nAddMonth;

	CTime tmStartDate  = CTime( nTotalMonth/12 - ((nTotalMonth%12) == 0 ? 1 : 0)
							, ((nTotalMonth%12) == 0 ? 12 : (nTotalMonth%12))
							, tmCur.GetDay()
							, tmCur.GetHour()
							, tmCur.GetMinute()
							, tmCur.GetSecond()
							);
	//tmStartDate -= CTimeSpan(1, 0, 0, 0);
	m_dtFilterStart.SetTime(&tmStartDate);
	InitDateRange(m_dtFilterStart);

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnSelect .LoadBitmap(IDB_BUTTON_SELECT , RGB(255,255,255));
	m_bnCancel .LoadBitmap(IDB_BUTTON_CANCEL , RGB(255,255,255));

	m_bnRefresh.SetToolTipText(LoadStringById(IDS_SELECTPLAN_BTN001));

	InitList();

	m_dtFilterStart.GetTime(m_stListFilter.tmStartDate);
	m_dtFilterEnd.GetTime(m_stListFilter.tmEndDate);

	m_dpDailyPlan.SetDefaultViewTime(0);
	m_dpDailyPlan.SetViewRange(24);
	m_dpDailyPlan.SetEditable(FALSE);

	RefreshList();

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSelectPlanDlg::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eSiteName    ] = LoadStringById(IDS_SELECTPLAN_LIST001);
	m_szColum[ePlan        ] = LoadStringById(IDS_SELECTPLAN_LIST002);
	m_szColum[eDownloadTime] = LoadStringById(IDS_SELECTPLAN_LIST003);
	m_szColum[ePeriod      ] = LoadStringById(IDS_SELECTPLAN_LIST004);
	m_szColum[eRegDate     ] = LoadStringById(IDS_SELECTPLAN_LIST005);
	m_szColum[eRegUser     ] = LoadStringById(IDS_SELECTPLAN_LIST006);
	m_szColum[eDesc        ] = LoadStringById(IDS_SELECTPLAN_LIST007);

	m_lcList.InsertColumn(eSiteName    , m_szColum[eSiteName    ], LVCFMT_CENTER, 80);
	m_lcList.InsertColumn(ePlan        , m_szColum[ePlan        ], LVCFMT_LEFT  , 90);
	m_lcList.InsertColumn(eDownloadTime, m_szColum[eDownloadTime], LVCFMT_CENTER,110);
	m_lcList.InsertColumn(ePeriod      , m_szColum[ePeriod      ], LVCFMT_LEFT  ,150);
	m_lcList.InsertColumn(eRegDate     , m_szColum[eRegDate     ], LVCFMT_CENTER,110);
	m_lcList.InsertColumn(eRegUser     , m_szColum[eRegUser     ], LVCFMT_LEFT  , 60);
	m_lcList.InsertColumn(eDesc        , m_szColum[eDesc        ], LVCFMT_LEFT  ,100);

	// 제일 마지막에 할것...
	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, (m_bSingleSelect ? CUTBListCtrlEx::LS_RADIOBOX : CUTBListCtrlEx::LS_CHECKBOX));
}

void CSelectPlanDlg::RefreshList()
{
	m_lcList.DeleteAllItems();
	m_lsInfoList.RemoveAll();

	CString strWhere;
	CString strTmp;

	// 방송계획명
	if(!m_stListFilter.strBpName.IsEmpty())
	{
		strTmp.Format(_T("bpName like '%%%s%%'"), m_stListFilter.strBpName.MakeLower());
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	// 태그,키워드 조건 추가
	m_stListFilter.strTag.Trim();
	if(!m_stListFilter.strTag.IsEmpty())
	{
		int pos = 0;
		CString strTag = m_stListFilter.strTag.Tokenize("/", pos);
		while(strTag != _T(""))
		{
			//strWhere.AppendFormat(_T(" %s concat('/', description, '/') like '%%/%s/%%'")
			strWhere.AppendFormat(_T(" %s description like '%%%s%%'")
								, (strWhere.IsEmpty() ? _T("") : _T("and"))
								, strTag.MakeLower()
								);

			strTag = m_stListFilter.strTag.Tokenize("/", pos);
		}
	}

	// 작성자
	if(!m_stListFilter.strRegister.IsEmpty())
	{
		strTmp.Format(_T("registerId = '%s'"), m_stListFilter.strRegister);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	// 항상 현재일을 기준으로 보이도록 한다
	//CString strStartDate = m_stListFilter.tmStartDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CTime tmCur = CTime::GetCurrentTime();
	CString strBaseDate = tmCur.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	//strTmp.Format(_T("startDate >= '%s' and endDate > '%s'"), strStartDate, strStartDate);
	strTmp.Format(_T("endDate > CAST('%s' as datetime)"), strBaseDate);
	strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;

	// 작성일자
	CString strStartDate = m_stListFilter.tmStartDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_stListFilter.tmEndDate.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");
	strTmp.Format(_T("createTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);
	strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;

	// 포함패키지
	if(!m_stListFilter.strPackageId.IsEmpty())
	{
		strTmp.Format(_T("bpId in (select bpId from UBC_TP where lower(programId) like lower('%%%s%%'))"), m_stListFilter.strPackageId);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	// 포함단말
	if(!m_stListFilter.strHostId.IsEmpty())
	{
		strTmp.Format(_T("bpId in (select bpId from UBC_TH where targetHost like '%%=%s')"), m_stListFilter.strHostId);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	strWhere += _T(" order by zorder");
	strWhere.Trim();

#ifdef _DEBUG
//	UbcMessageBox(strWhere);
#endif

	if(!CCopModule::GetObject()->GetBroadcastingPlanList( (CCopModule::eSiteAdmin == m_nAuthority ? _T("*") : m_strSiteId)
														, m_lsInfoList
														, strWhere ))
	{
		return;
	}

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SBroadcastingPlanInfo& info = m_lsInfoList.GetNext(pos);

		CString strPeriod;
		strPeriod.Format(_T("%s~%s")
						, info.tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S"))
						, info.tmEndDate.Format(_T("%Y/%m/%d %H:%M:%S"))
						);

		CString strDownloadTime;
#ifdef _ML_KOR_
		strDownloadTime.Format(_T("%s 재배포(%d회/%d분)")
							, info.tmDownloadTime.Format(_T("%Y/%m/%d %H:%M:%S"))
							, info.nRetryCount
							, info.nRetryGap
							);
#else
		strDownloadTime.Format(_T("%s Redistribution(%dtimes/%dmin)")
							, info.tmDownloadTime.Format(_T("%Y/%m/%d %H:%M:%S"))
							, info.nRetryCount
							, info.nRetryGap
							);
#endif

		m_lcList.InsertItem(nRow, "");
		m_lcList.SetItemText(nRow, eSiteName    , info.strSiteId);
		m_lcList.SetItemText(nRow, ePlan        , info.strBpName);
		m_lcList.SetItemText(nRow, eDownloadTime, strDownloadTime);
		m_lcList.SetItemText(nRow, ePeriod      , strPeriod);
		m_lcList.SetItemText(nRow, eRegDate     , info.tmCreateTime.Format(_T("%Y/%m/%d %H:%M:%S")));
		m_lcList.SetItemText(nRow, eRegUser     , info.strRegisterId);
		m_lcList.SetItemText(nRow, eDesc        , info.strDescription);
		m_lcList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	RefreshDailyPlan();
}

BOOL CSelectPlanDlg::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_PLAN_NAME)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_TAG      )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_REGISTER )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_PACKAGE  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_DATE_FILTER_START    )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_DATE_FILTER_END      )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOST     )->GetSafeHwnd() )
		{
			OnBnClickedButtonPlanRefresh();
			return TRUE;
		}
	}

	return __super::PreTranslateMessage(pMsg);
}

void CSelectPlanDlg::OnBnClickedButtonFilterHost()
{
	CSelectHostDlg dlg(m_strCustomer);
	dlg.m_nAuthority = m_nAuthority;
	dlg.m_strSiteId = m_strSiteId;
	dlg.m_strIniPath = m_strIniPath;

	m_strHostId = _T("");
	SetDlgItemText(IDC_EDIT_FILTER_HOST, _T(""));

	if(dlg.DoModal() == IDOK)
	{
		m_strHostId = dlg.m_strHostId;
		SetDlgItemText(IDC_EDIT_FILTER_HOST, dlg.m_strHostName);
	}
}

void CSelectPlanDlg::OnBnClickedButtonFilterRegister()
{
	CSelectUsers dlg;
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_REGISTER, _T(""));
		return;
	}

	UserInfoList& list = dlg.GetSelectedList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SUserInfo info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_REGISTER, info.userId);
	}
}

void CSelectPlanDlg::OnBnClickedButtonFilterPackage()
{
	CPackageSelectDlg dlg(m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, info.szPackage);
	}
}

void CSelectPlanDlg::OnBnClickedButtonPlanRefresh()
{
	GetDlgItemText(IDC_EDIT_FILTER_PLAN_NAME, m_stListFilter.strBpName  );
	GetDlgItemText(IDC_EDIT_FILTER_TAG      , m_stListFilter.strTag     );
	GetDlgItemText(IDC_EDIT_FILTER_REGISTER , m_stListFilter.strRegister);
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , m_stListFilter.strPackageId);
	GetDlgItemText(IDC_EDIT_FILTER_HOST     , m_stListFilter.strHostName);
	m_stListFilter.strHostId = m_strHostId;
	m_dtFilterStart.GetTime(m_stListFilter.tmStartDate);
	m_dtFilterEnd.GetTime(m_stListFilter.tmEndDate);

	RefreshList();
}

void CSelectPlanDlg::RefreshDailyPlan()
{
	m_dpDailyPlan.DeleteAllItem();

	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		if(!m_lcList.GetItemState(i, LVIS_SELECTED)) continue;

		POSITION posBP = (POSITION)m_lcList.GetItemData(i);
		if(!posBP) return;

		SBroadcastingPlanInfo& infoBP = m_lsInfoList.GetAt(posBP);

		TimePlanInfoList* pTimePlanInfoList = GetTimePlanInfo(infoBP.strBpId, FALSE);
		if(!pTimePlanInfoList) continue;

		POSITION posTP = pTimePlanInfoList->GetHeadPosition();
		while(posTP)
		{
			POSITION posOld = posTP;
			STimePlanInfo& infoTP = pTimePlanInfoList->GetNext(posTP);

			if(infoTP.nProcType == PROC_DELETE) continue;

			m_dpDailyPlan.AddItem( infoTP.strProgramId
								 , NormalizeTime(_ttoi(infoTP.strStartTime.Left(2)), _ttoi(infoTP.strStartTime.Mid(3,2))).GetTime()
								 , NormalizeTime(_ttoi(infoTP.strEndTime.Left(2)), _ttoi(infoTP.strEndTime.Mid(3,2))).GetTime()
								 , (LONG*)posOld
								 );
		}
	}

	m_dpDailyPlan.SetSelectedIndex(m_dpDailyPlan.GetItemCount()-1);
}

void CSelectPlanDlg::OnNMClickListPlan(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	RefreshDailyPlan();
}

TimePlanInfoList* CSelectPlanDlg::GetTimePlanInfo(CString strBpId, BOOL bForceCall)
{
	TimePlanInfoList* pTimePlanInfoList = NULL;

	CString strKey = strBpId;
	if(strKey.IsEmpty()) return NULL;

	if(bForceCall || !m_mapTimePlanInfo.Lookup(strKey, (void*&)pTimePlanInfoList))
	{
		if(bForceCall && pTimePlanInfoList)
		{
			m_mapTimePlanInfo.RemoveKey(strKey);
			delete pTimePlanInfoList;
		}

		pTimePlanInfoList = new TimePlanInfoList;
		BOOL bResult = CCopModule::GetObject()->GetTimePlan( (CCopModule::eSiteAdmin == m_nAuthority ? _T("*") : m_strSiteId)
															, strBpId
															, *pTimePlanInfoList
															);
		if(!bResult)
		{
			delete pTimePlanInfoList;
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_SELECTPLAN_MSG001), strBpId);
			UbcMessageBox(szMsg, MB_ICONERROR);
			return NULL;
		}

		m_mapTimePlanInfo[strKey] = pTimePlanInfoList;
	}

	return pTimePlanInfoList;
}
