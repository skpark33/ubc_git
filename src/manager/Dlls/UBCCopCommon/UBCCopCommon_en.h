#pragma once

#define UBCDEFAULT_FONT					_T("Arial")
#define UBCSTUDIOEXE_STR				_T("UBCStudio_en.exe")
#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE_en.exe")
#define UBCMANAGEREXE_STR				_T("UBCManager_en.exe")

//CFtpMultiSite
#define FTPMULTISITE_STR001			_T("Connecting to the site %s")
#define FTPMULTISITE_STR002			_T("Fail")
#define FTPMULTISITE_STR003			_T("Error")
#define FTPMULTISITE_STR004			_T("Abort")
#define FTPMULTISITE_STR005			_T("Success")
#define FTPMULTISITE_STR006			_T("Wait")
#define FTPMULTISITE_STR007			_T("Uploading")
#define FTPMULTISITE_STR008			_T("Downloading")
#define FTPMULTISITE_STR009			_T("Skip")
#define FTPMULTISITE_STR010			_T("Local")
#define FTPMULTISITE_STR011			_T("Server")
#define FTPMULTISITE_STR012			_T("Unknown")
#define FTPMULTISITE_STR013			_T("Both")

#define FTPMULTISITE_LST001			_T("Site")
#define FTPMULTISITE_LST002			_T("FileName")
#define FTPMULTISITE_LST003			_T("Location")
#define FTPMULTISITE_LST004			_T("Progress")
#define FTPMULTISITE_LST005			_T("Local Size")
#define FTPMULTISITE_LST006			_T("Server Size")
#define FTPMULTISITE_LST007			_T("Status")
#define FTPMULTISITE_LST008			_T("Message")
#define FTPMULTISITE_LST009			_T("Server Path")

#define FTPMULTISITE_MSG001			_T(">> Connection to site (%s) failed\n")
#define FTPMULTISITE_MSG002			_T(">> Site(%s) : %s\n")
#define FTPMULTISITE_MSG003			_T("> Site(%s) : %s\n")
#define FTPMULTISITE_MSG004			_T("Site : %s / File : %s")
#define FTPMULTISITE_MSG005			_T("File upload is complete")
#define FTPMULTISITE_MSG006			_T(">> Connection to site (%s) failed\n")
#define FTPMULTISITE_MSG007			_T("File download is complete")
#define FTPMULTISITE_MSG008			_T("Abort by the user")
#define FTPMULTISITE_MSG009			_T("File sizes vary. Server [%s], Local [%s]")
#define FTPMULTISITE_MSG010			_T("Since there does not download the file")
#define FTPMULTISITE_MSG011			_T("File download failed")
#define FTPMULTISITE_MSG012			_T("File upload failed")

//CPackageMngDlg
#define PACKAGEMNGDLG_LST001		_T("site")
#define PACKAGEMNGDLG_LST002		_T("package")
#define PACKAGEMNGDLG_LST003		_T("user")
#define PACKAGEMNGDLG_LST004		_T("time")
#define PACKAGEMNGDLG_LST005		_T("description")

//CPackageRevAddDlg
#define PACKAGEREVADDDLG_STR001	_T("Reserve package")
#define PACKAGEREVADDDLG_STR002	_T("Applied terminal(s)")
#define PACKAGEREVADDDLG_STR003	_T("Reserved terminal(s)")

#define PACKAGEREVADDDLG_MSG001	_T("Please enter the start time.")
#define PACKAGEREVADDDLG_MSG002	_T("Start time must be more than 5 minutes later than the current time.")
#define PACKAGEREVADDDLG_MSG003	_T("End time since before the start time should be set at least 5 minutes.")
#define PACKAGEREVADDDLG_MSG004	_T("The package has been modified.\nIf you submit the command, it will change the package file on the server.\nIf you don't want to overwrite the existing file, the package should be saved under a new name.\nDo you wish to save the package under a new name?")
#define PACKAGEREVADDDLG_MSG005	_T("Invalid start time.\nStart time must be later than the end time of other terminals.")

//CPackageRevDlg
#define PACKAGEREVDLG_LST001		_T("Site")
#define PACKAGEREVDLG_LST002		_T("Host")
#define PACKAGEREVDLG_LST003		_T("Display")
#define PACKAGEREVDLG_LST004		_T("Reserved Package")
#define PACKAGEREVDLG_LST005		_T("Last Package")
#define PACKAGEREVDLG_LST006		_T("Current Package")
#define PACKAGEREVDLG_LST007		_T("Auto Package")
#define PACKAGEREVDLG_LST008		_T("Package")
#define PACKAGEREVDLG_LST009		_T("Start time")
#define PACKAGEREVDLG_LST010		_T("End time")

#define PACKAGEREVDLG_STR001		_T("Reserve package")
#define PACKAGEREVDLG_STR002		_T("Add Reservation")
#define PACKAGEREVDLG_STR003		_T("Modify Reservation")
#define PACKAGEREVDLG_STR004		_T("Remove package")

#define PACKAGEREVDLG_MSG001		_T("No terminals were selected.\nPlease select a terminal(s) and try again.")
#define PACKAGEREVDLG_MSG002		_T("No package were selected.\nnPlease select a package(s) and try again.")
#define PACKAGEREVDLG_MSG003		_T("No package were selected.\nnPlease select a package(s) and try again.")
#define PACKAGEREVDLG_MSG004		_T("Are you sure you want to delete the package?")
#define PACKAGEREVDLG_MSG005		_T("%s group is not registered. Please, Try again after register %s group")

//CSiteInfoDlg
#define SITEINFODLG_STR001			_T("Holiday date")

#define SITEINFODLG_MSG001			_T("Please enter the site ID.")
#define SITEINFODLG_MSG002			_T("Please use alphabet letters and numbers only")

//CFlashView
#define FLASHVIEW_MSG001			_T("Flash is not installed or inappropriate version.\nDo you want to download Flash player now?")

//CAuthCheckDlg
#define AUTHCHECK_MSG001			_T("Authentication failed.\nPlease check the enterprise key and password and try again.")

//CSelectHostDlg
#define SELECTHOST_STR001				_T("in-use")
#define SELECTHOST_STR002				_T("not-in-use")
#define SELECTHOST_STR003				_T("Connected")
#define SELECTHOST_STR004				_T("Disconnected")

#define SELECTHOST_LIST001				_T("Host")
#define SELECTHOST_LIST002				_T("Group")

#define SELECTHOST_BTN001				_T("Refresh Host list")

#define SELECTHOST_MSG001				_T("Please select the terminal")

//CMultiSelectHost
#define MULTISELECTHOST_BTN001			_T("Refresh Host list")
		
#define MULTISELECTHOST_STR001			_T("in-use")
#define MULTISELECTHOST_STR002			_T("not-in-use")
#define MULTISELECTHOST_STR003			_T("Connected")
#define MULTISELECTHOST_STR004			_T("Disconnected")
#define MULTISELECTHOST_STR005			_T("All")
		
#define MULTISELECTHOST_LIST001			_T("Host")
#define MULTISELECTHOST_LIST002			_T("Group")
		
#define MULTISELECTHOST_MSG001			_T("Please select the terminal")
