#pragma once

#include "afxwin.h"
#include "afxdtctl.h"
#include "afxcmn.h"

//#include "resource.h"
#ifndef IDD_SITEINFO
#define IDD_SITEINFO	25000
#endif//IDD_SITEINFO

#include "copmodule.h"
#include "common/HoverButton.h"
#include "common/utblistctrl.h"

// CSiteInfoDlg dialog
class AFX_EXT_CLASS CSiteInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CSiteInfoDlg)
public:
	void SetSite(const SSiteInfo& info);
	SSiteInfo& GetSite();
	void	SetParentSite(const char* parentId);
private:
	SSiteInfo m_SiteInfo;

public:
	CSiteInfoDlg(CString& strCustomer,CWnd* pParent = NULL);   // standard constructor
	virtual ~CSiteInfoDlg();

// Dialog Data
	enum { IDD = IDD_SITEINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CComboBox m_combo_businessType;
	CComboBox m_combo_businessCode;

	CString m_strCustomer;
};
