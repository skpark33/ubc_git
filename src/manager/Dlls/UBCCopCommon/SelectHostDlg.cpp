// SelectHostDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "copmodule.h"
#include "SelectHostDlg.h"

#include "ContentsSelectDlg.h"
#include "PackageSelectDlg.h"
#include "SiteSelectDlg.h"

#include "common\UbcCode.h"
#include "common\TraceLog.h"


// CSelectHostDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSelectHostDlg, CDialog)

CSelectHostDlg::CSelectHostDlg(LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CSelectHostDlg::IDD, pParent)
	, m_strCustomer ( szCustomer )
{

}

CSelectHostDlg::~CSelectHostDlg()
{
}

void CSelectHostDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FILTER_HOSTNAME, m_editFilterHostName);
	DDX_Control(pDX, IDC_EDIT_FILTER_HOST, m_editFilterHost);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOSTTYPE, m_cbxHostType);
	DDX_Control(pDX, IDC_CB_SITE, m_cbxSite);
	DDX_Control(pDX, IDC_CB_INCLUDECONTENTS, m_cbxIncludeContents);
	DDX_Control(pDX, IDC_CB_PLAYING_PACKAGE, m_cbxPlayingPackage);
	DDX_Control(pDX, IDC_CB_ONOFF, m_cbOnOff);
	DDX_Control(pDX, IDC_CB_OPERATION, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_PLAN_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcList);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
	DDX_Control(pDX, IDC_COMBO_FILTER_TAG, m_cbFilterTag);
}


BEGIN_MESSAGE_MAP(CSelectHostDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSelectHostDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSelectHostDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_PLAN_REFRESH, &CSelectHostDlg::OnBnClickedButtonPlanRefresh)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOST, &CSelectHostDlg::OnNMDblclkListHost)
	ON_CBN_SELCHANGE(IDC_CB_SITE, &CSelectHostDlg::OnCbnSelchangeCbSite)
	ON_CBN_SELCHANGE(IDC_CB_INCLUDECONTENTS, &CSelectHostDlg::OnCbnSelchangeCbIncludecontents)
	ON_CBN_SELCHANGE(IDC_CB_PLAYING_PACKAGE, &CSelectHostDlg::OnCbnSelchangeCbPlayingPackage)
END_MESSAGE_MAP()


// CSelectHostDlg 메시지 처리기입니다.

BOOL CSelectHostDlg::OnInitDialog()
{
	CWaitMessageBox wait;

	CDialog::OnInitDialog();

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnOK     .LoadBitmap(IDB_BUTTON_SELECT , RGB(255,255,255));
	m_bnCancel .LoadBitmap(IDB_BUTTON_CANCEL , RGB(255,255,255));

	m_bnRefresh.SetToolTipText(LoadStringById(IDS_SELECTHOST_BTN001));

	CodeItemList listHostType;
	CUbcCode::GetInstance(m_strCustomer)->GetCategoryInfo(_T("HostType"), listHostType);

	POSITION pos = listHostType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listHostType.GetNext(pos);
		m_cbxHostType.AddString(info.strEnumString, info.nEnumNumber);
	}

	m_cbOnOff.AddString("All");
	m_cbOnOff.AddString(LoadStringById(IDS_SELECTHOST_STR001));
	m_cbOnOff.AddString(LoadStringById(IDS_SELECTHOST_STR002));
	m_cbOnOff.SetCurSel(0);

	m_cbOperation.AddString("All");
	m_cbOperation.AddString(LoadStringById(IDS_SELECTHOST_STR003));
	m_cbOperation.AddString(LoadStringById(IDS_SELECTHOST_STR004));
	m_cbOperation.SetCurSel(0);

	CStringArray aTagList;
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	char buf[1024];
//	CString szPath;
//	szPath.Format("%s%sdata\\UBCManager.ini", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	GetPrivateProfileString("HOST-FILTER", "HostName", "", buf, 1024, m_strIniPath);
	m_editFilterHostName.SetWindowText(buf);
	GetPrivateProfileString("HOST-FILTER", "HostID", "", buf, 1024, m_strIniPath);
	m_editFilterHost.SetWindowText(buf);

	GetPrivateProfileString("HOST-FILTER", "HostType", "0", buf, 1024, m_strIniPath);
	CString strHostType = buf;
	if(strHostType.IsEmpty())
	{
		m_cbxHostType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strHostType.Tokenize(",", pos)) != _T(""))
		{
			m_cbxHostType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}

	GetPrivateProfileString("HOST-FILTER", "SiteName", "", buf, 1024, m_strIniPath);
	CString strSiteName = buf;
	m_cbxSite.AddString(LoadStringById(IDS_COPCOMMON_STR001));
	m_cbxSite.AddString(LoadStringById(IDS_COPCOMMON_STR002));
	m_cbxSite.SetCurSel(0);
	if(CCopModule::eSiteAdmin != m_nAuthority && CCopModule::eSiteManager != m_nAuthority)
	{
		m_cbxSite.InsertString(1, m_strSiteId);
		m_cbxSite.SetCurSel(1);
		m_cbxSite.EnableWindow(FALSE);

		m_strSite = m_strSiteId;
	}
	else
	{
		GetPrivateProfileString("HOST-FILTER", "SiteID", "", buf, 1024, m_strIniPath);
		m_strSite = buf;

		if(m_strSite.GetLength() > 0)
		{
			m_cbxSite.InsertString(1, strSiteName);
			m_cbxSite.SetCurSel(1);
		}
	}

	GetPrivateProfileString("HOST-FILTER", "IncludeContents", "", buf, 1024, m_strIniPath);
	m_cbxIncludeContents.AddString(LoadStringById(IDS_COPCOMMON_STR001));
	m_cbxIncludeContents.AddString(LoadStringById(IDS_COPCOMMON_STR002));
	m_cbxIncludeContents.SetCurSel(0);
	if(buf[0] != 0)
	{
		m_cbxIncludeContents.InsertString(1, buf);
		m_cbxIncludeContents.SetCurSel(1);
	}

	GetPrivateProfileString("HOST-FILTER", "IncludeContentsID", "", buf, 1024, m_strIniPath);
	m_strIncludeContentsID = buf;

	GetPrivateProfileString("HOST-FILTER", "PlayingPackage", "", buf, 1024, m_strIniPath);
	m_cbxPlayingPackage.AddString(LoadStringById(IDS_COPCOMMON_STR001));
	m_cbxPlayingPackage.AddString(LoadStringById(IDS_COPCOMMON_STR002));
	m_cbxPlayingPackage.SetCurSel(0);
	if(buf[0] != 0)
	{
		m_cbxPlayingPackage.InsertString(1, buf);
		m_cbxPlayingPackage.SetCurSel(1);
	}

	GetPrivateProfileString("HOST-FILTER", "PlayingPackageID", "", buf, 1024, m_strIniPath);
	m_strPlayingPackageID = buf;

	GetPrivateProfileString("HOST-FILTER", "AdminStat", "0", buf, 1024, m_strIniPath);
	m_cbOnOff.SetCurSel(atoi(buf));
	GetPrivateProfileString("HOST-FILTER", "OperaStat", "0", buf, 1024, m_strIniPath);
	m_cbOperation.SetCurSel(atoi(buf));
	GetPrivateProfileString("HOST-FILTER", "Category", "", buf, 1024, m_strIniPath);
	m_cbFilterTag.SetWindowText(buf);

	InitList();

	
	// skpark comment out
	// 화면을 여는 것과 동시에 검색이 이루어 지므로, 화면이 너무 늦게 나오게 되고,
	// 원하지 않는 검색결과를 오래 기다려야 하는 경우가 많다.
	// Refresh 버튼을 누른 경우 데이터가 나오도록 수정한다.
	// RefreshList();
	TraceLog(("skpark........"));
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSelectHostDlg::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck ] = _T("");
	m_szColum[eHostName] = LoadStringById(IDS_SELECTHOST_LIST001);
	m_szColum[eHostId] = LoadStringById(IDS_SELECTHOST_LIST003);
	m_szColum[eGroup ] = LoadStringById(IDS_SELECTHOST_LIST002);

	m_lcList.InsertColumn(eCheck , m_szColum[eCheck ], LVCFMT_CENTER, 22);
	m_lcList.InsertColumn(eHostName, m_szColum[eHostName], LVCFMT_LEFT  , 100);
	m_lcList.InsertColumn(eHostId, m_szColum[eHostId], LVCFMT_LEFT  , 100);
	m_lcList.InsertColumn(eGroup , m_szColum[eGroup ], LVCFMT_LEFT  , 100);

	// 제일 마지막에 할것...
	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_RADIOBOX);

//	m_lcList.SetSortEnable(false);
//	m_lcList.SetFixedCol(eCheck);
}

void CSelectHostDlg::RefreshList()
{
	CWaitMessageBox wait;

	CStringArray filter_list;
	CString strTmp;

	//
	CString strHostName;
	m_editFilterHostName.GetWindowText(strHostName);
	if(!strHostName.IsEmpty())
	{
		strTmp.Format(_T("hostName like '%%%s%%'"), strHostName);
		filter_list.Add(strTmp);
	}

	//
	CString strHost;
	m_editFilterHost.GetWindowText(strHost);
	if(!strHost.IsEmpty())
	{
		strTmp.Format(_T("hostId like '%%%s%%'"), strHost);
		filter_list.Add(strTmp);
	}

	//
	CString strHostType = m_cbxHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	if(!strHostType.IsEmpty())
	{
		strTmp.Format(_T("hostType in ('%s')")
					, strHostType
					);
		strTmp.Replace(",", "','");
		filter_list.Add(strTmp);
	}

	//
	CString strSiteName;
	m_cbxSite.GetWindowText(strSiteName);

	bool has_where_siteId_alreay = false;
	if(m_strSite.IsEmpty())
	{
		if(CCopModule::eSiteUser == m_nAuthority)
		{
			strTmp.Format(_T("siteId like '%%%s%%'"), m_strSiteId);
			filter_list.Add(strTmp);
			has_where_siteId_alreay = true;
		}
	}
	else
	{
		// skpark 2013.4.23 multiselect 가 가능하도록
		//strTmp.Format(_T("siteId like '%%%s%%'"), m_strSite);
		//filter_list.Add(strTmp);
		CString strSiteId = m_strSite;
		if(!strSiteId.IsEmpty() && strSiteId != "*" ){
			strSiteId.Replace(_T(","), _T("','"));
			strTmp.Format(_T("siteId in ('%s')"), strSiteId);
			filter_list.Add(strTmp);
			has_where_siteId_alreay = true;
		}
	}

	//
	CString strIncludeContents = "";
	if(m_cbxIncludeContents.GetCurSel() > 0) m_cbxIncludeContents.GetWindowText(strIncludeContents);
	if(!m_strIncludeContentsID.IsEmpty())
	{
		CStringArray program_id_list;
		CCopModule::GetObject()->GetProgramIdListIncludeContents(m_strIncludeContentsID, program_id_list);

		strTmp = "";
		int count = program_id_list.GetCount();
		for(int i=0; i<count; i++)
		{
			if(strTmp.GetLength() > 0) strTmp += ",";

			CString tmp;
			tmp.Format("'%s'", program_id_list.GetAt(i));

			strTmp += tmp;
		}
		if(strTmp.GetLength() > 0)
		{
			strTmp.Insert(0, "lastSchedule1 in (");
			strTmp += ")";
		}
		filter_list.Add(strTmp);
	}

	//
	CString strPlayingPackage = "";
	if(m_cbxPlayingPackage.GetCurSel() > 0) m_cbxPlayingPackage.GetWindowText(strPlayingPackage);
	if(!m_strPlayingPackageID.IsEmpty())
	{
		strTmp.Format(_T("lastSchedule1 = '%s'"), m_strPlayingPackageID);
		filter_list.Add(strTmp);
	}

	//
	if(m_cbOnOff.GetCurSel())
	{
		strTmp.Format(_T("adminState = %d"), (m_cbOnOff.GetCurSel()==1)? 1:0);
		filter_list.Add(strTmp);
	}

	if(m_cbOperation.GetCurSel())
	{
		strTmp.Format(_T("operationalState = %d"), (m_cbOperation.GetCurSel()==1)? 1:0);
		filter_list.Add(strTmp);
	}

	//
	CString strDescription;
	m_cbFilterTag.GetWindowText(strDescription);
	if(!strDescription.IsEmpty())
	{
		strTmp.Format(_T("category like '%%%s%%'"), strDescription);
		filter_list.Add(strTmp);
	}

	//
	CString strWhere = "";
	int count = filter_list.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str = filter_list.GetAt(i);

		if(strWhere.GetLength() > 0)
		{
			strWhere += " and ";
		}

		strWhere += "( ";
		strWhere += str;
		strWhere += ") ";
	}

#ifdef _DEBUG
//	UbcMessageBox(strWhere);
#endif

	WritePrivateProfileString("HOST-FILTER", "HostName", strHostName, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "HostID", strHost, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "HostType", strHostType, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "SiteName", strSiteName, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "SiteID", m_strSite, m_strIniPath);

	WritePrivateProfileString("HOST-FILTER", "IncludeContents", strIncludeContents, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContentsID", m_strIncludeContentsID, m_strIniPath);

	WritePrivateProfileString("HOST-FILTER", "PlayingPackage", strPlayingPackage, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackageID", m_strPlayingPackageID, m_strIniPath);

	WritePrivateProfileString("HOST-FILTER", "AdminStat", ToString(m_cbOnOff.GetCurSel()), m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "OperaStat", ToString(m_cbOperation.GetCurSel()), m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "Category", strDescription, m_strIniPath);

	m_lsInfoList.RemoveAll();
	cciCall aCall;
	CCopModule::GetObject()->GetHostForSelect(&aCall
											, ((CCopModule::eSiteAdmin == m_nAuthority||has_where_siteId_alreay==true) ? _T("*") : m_strSiteId)
											, _T("*")
											, strWhere
											);
	CCopModule::GetObject()->GetHostData(&aCall
										, m_lsInfoList
										, 0
										);

	m_lcList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsInfoList.GetNext(pos);

		m_lcList.InsertItem(nRow, "");
		m_lcList.SetItemText(nRow, eHostName, info.hostName);
		m_lcList.SetItemText(nRow, eHostId, info.hostId);
		m_lcList.SetItemText(nRow, eGroup , info.siteId);
		m_lcList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
}

void CSelectHostDlg::OnBnClickedOk()
{
	m_strHostId.Empty();

	int nRow = m_lcList.GetRadioSelectedIndex();
	if(nRow >= 0)
	{
		POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
		SHostInfo info = m_lsInfoList.GetAt(pos);

		m_strHostId = info.hostId;
		m_strHostName = info.hostName;
	}

	if(m_strHostId.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_SELECTHOST_MSG001));
		return;
	}

	OnOK();
}

void CSelectHostDlg::OnBnClickedCancel()
{
	m_strHostId.Empty();
	m_strHostName.Empty();

	OnCancel();
}

void CSelectHostDlg::OnBnClickedButtonPlanRefresh()
{
	RefreshList();
}

void CSelectHostDlg::OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	OnBnClickedOk();
}

BOOL CSelectHostDlg::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOSTNAME   )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOST       )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_SITE                )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_INCLUDECONTENTS     )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_PLAYING_PACKAGE     )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_DESCRIPTION)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_ONOFF               )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_OPERATION           )->GetSafeHwnd() )
		{
			OnBnClickedButtonPlanRefresh();
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CSelectHostDlg::OnCbnSelchangeCbSite()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbxSite.GetCurSel();
	int count = m_cbxSite.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxSite.DeleteString(1);
			m_strSite = "";
		}
	}
	else if(idx == count-1 )
	{
		CSiteSelectDlg dlg(m_strCustomer);
		//skpark 2013.4.23 multiselect 수정
		//dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_strSite = "";
				m_cbxSite.DeleteString(1);
				m_cbxSite.SetCurSel(0);
			}
			//skpark 2013.4.23 multiselect start[
			CString strSiteName,strSiteId;
			SiteInfoList siteInfoList;
			if(dlg.GetSelectSiteInfoList(siteInfoList))
			{
				POSITION pos = siteInfoList.GetHeadPosition();
				while(pos)
				{
					SSiteInfo info = siteInfoList.GetNext(pos);
					strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
					strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
				}
			}
			if(!strSiteName.IsEmpty()){
				m_cbxSite.InsertString(1, strSiteName);
				m_cbxSite.SetCurSel(1);
				m_strSite = strSiteId;
			}else{
				m_cbxSite.SetCurSel(0);
				m_strSite = "";
			
			}
			//SSiteInfo info;
			//dlg.GetSelectSiteInfo(info);

			//if(info.siteName.GetLength() > 0)
			//{
			//	m_cbxSite.InsertString(1, info.siteName);
			//	m_cbxSite.SetCurSel(1);
			//	m_strSite = info.siteId;
			//}
			//else
			//{
			//	m_cbxSite.SetCurSel(0);
			//	m_strSite = "";
			//}

			//skpark 2013.4.23 multiselect end]



		}
		else
		{
			m_cbxSite.SetCurSel(count-2);
		}
	}else{
		// skpark 2013.4.23 multiselect 
		// 콤보박스의 글자가 14자가 넘어가면 팝업으로 보여준다.
		CString strSiteName;
		m_cbxSite.GetWindowText(strSiteName);
		if(strSiteName.GetLength() > 14){
			CString msgStr = "Selected Group is\n";
			msgStr.Append(strSiteName);
			UbcMessageBox(msgStr);
		}
	}
}

void CSelectHostDlg::OnCbnSelchangeCbIncludecontents()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbxIncludeContents.GetCurSel();
	int count = m_cbxIncludeContents.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxIncludeContents.DeleteString(1);
			m_strIncludeContentsID = "";
		}
	}
	else if(idx == count-1 )
	{
		CContentsSelectDlg dlg(m_strCustomer);
		dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_strIncludeContentsID = "";
				m_cbxIncludeContents.DeleteString(1);
				m_cbxIncludeContents.SetCurSel(0);
			}

			CONTENTS_INFO_EX_LIST& info_list = dlg.GetSelectedContentsIdList();

			if(info_list.GetCount() > 0)
			{
				POSITION pos = info_list.GetHeadPosition();
				CONTENTS_INFO_EX& info = info_list.GetNext(pos);

				m_cbxIncludeContents.InsertString(1, info.strContentsName);
				m_cbxIncludeContents.SetCurSel(1);
				m_strIncludeContentsID = info.strContentsId;
			}
			else
			{
				m_cbxIncludeContents.SetCurSel(0);
				m_strIncludeContentsID = "";
			}
		}
		else
		{
			m_cbxIncludeContents.SetCurSel(count-2);
		}
	}
}

void CSelectHostDlg::OnCbnSelchangeCbPlayingPackage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbxPlayingPackage.GetCurSel();
	int count = m_cbxPlayingPackage.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxPlayingPackage.DeleteString(1);
			m_strPlayingPackageID = "";
		}
	}
	else if(idx == count-1 )
	{
		CPackageSelectDlg dlg(m_strCustomer);
		dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_strPlayingPackageID = "";
				m_cbxPlayingPackage.DeleteString(1);
				m_cbxPlayingPackage.SetCurSel(0);
			}

			PackageInfoList& info_list = dlg.GetSelectedPackageList();

			if(info_list.GetCount() > 0)
			{
				POSITION pos = info_list.GetHeadPosition();
				SPackageInfo& info = info_list.GetNext(pos);

				m_cbxPlayingPackage.InsertString(1, info.szPackage);
				m_cbxPlayingPackage.SetCurSel(1);
				m_strPlayingPackageID = info.szPackage;
			}
			else
			{
				m_cbxPlayingPackage.SetCurSel(0);
				m_strPlayingPackageID = "";
			}
		}
		else
		{
			m_cbxPlayingPackage.SetCurSel(count-2);
		}
	}
}


