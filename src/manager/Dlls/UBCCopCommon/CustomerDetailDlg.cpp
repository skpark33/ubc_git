// CustomerDetailDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CustomerDetailDlg.h"
#include "UBCCopCommon.h"


// CCustomerDetailDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCustomerDetailDlg, CDialog)

CCustomerDetailDlg::CCustomerDetailDlg(int btMode, SCustomerInfo& info, CWnd* pParent /*=NULL*/)
	: CDialog(CCustomerDetailDlg::IDD, pParent)
	, m_btMode(btMode)
	, m_info(info)
{

}

CCustomerDetailDlg::~CCustomerDetailDlg()
{
}

void CCustomerDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDDELETE, m_btDel);
	DDX_Control(pDX, IDOK, m_btOK);
	DDX_Control(pDX, IDCANCEL, m_btCancel);
	DDX_Control(pDX, IDC_EDIT_CUSTOMERID, m_editCustomerId);
	DDX_Control(pDX, IDC_CB_LANGUAGE, m_cbLanguage);
	DDX_Control(pDX, IDC_CB_TIMEZONE, m_cbTimeZone);
	DDX_Control(pDX, IDC_EDIT_DESCRIPTION, m_editDescription);
	DDX_Control(pDX, IDC_EDIT_URL, m_editURL);
}


BEGIN_MESSAGE_MAP(CCustomerDetailDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCustomerDetailDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDDELETE, &CCustomerDetailDlg::OnBnClickedDelete)
	ON_BN_CLICKED(IDCANCEL, &CCustomerDetailDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CCustomerDetailDlg 메시지 처리기입니다.
bool CCustomerDetailDlg::SetData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString customerId;
	this->m_editCustomerId.GetWindowText(customerId);
	if(customerId.IsEmpty()){
		UbcMessageBox(LoadStringById(IDS_CUSTOMER_DETAIL_MSG01), MB_ICONINFORMATION);
		return false;
	}
	this->m_info.customerId = customerId;

	CString description;
	this->m_editDescription.GetWindowText(description);
	//if(description.IsEmpty()){
	//	UbcMessageBox(LoadStringById(IDS_CUSTOMER_DETAIL_MSG03), MB_ICONINFORMATION);
	//	return false;
	//}
	this->m_info.description = description;

	this->m_editURL.GetWindowText(this->m_info.url);
	this->m_cbLanguage.GetWindowText(this->m_info.language);

	int gmt = 9999;
	int idx = this->m_cbTimeZone.GetCurSel();
	if(0 <= idx && idx < 24) {
		gmt = (-1)*idx*30;
	}else if( 24 <= idx && idx < 48){
		gmt = (idx-24)*30;
	}
	this->m_info.gmt = gmt;

	return true;
}


void CCustomerDetailDlg::OnBnClickedOk()
{
	if(SetData()){
		OnOK();
	}
}

void CCustomerDetailDlg::OnBnClickedDelete()
{
	m_btMode = eDelete;
	if(SetData()){
		OnOK();
	}
}

void CCustomerDetailDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

BOOL CCustomerDetailDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// langauage 초기화
	const char* languages[] = {
			"",			//0  osLang 을 넣기 위해 index 값을 기억하기 위해 빈자리를 하나 둠.
			"Arabic",   //1
			"Bulgarian",
			"Chinese",
			"Croatian",
			"Czech",
			"Danish",
			"Dutch",
			"English",  // 8
			"Estonian",
			"Finnish",
			"French", //11
			"German",
			"Greek",
			"Hebrew",
			"Hindi",
			"Hungarian",
			"Indonesian",
			"Italian",
			"Japanese",
			"Korean",//20
			"Latvian",//21
			"Lithuanian",
			"Norwegian",
			"Polish",
			"Portuguese",
			"Romanian",
			"Russian",
			"Serbian",
			"Slovak",
			"Slovenian",
			"Spanish",  //31
			"Swedish",
			"Thai",
			"Turkish",
			"Ukrainian", //35
			"END"
	};
	
	CString osLang = GetLocaleName();
	this->m_cbLanguage.AddString(osLang);  // osLang 이 항상 처음에 온다.

	int i=0;
	while(languages[++i] != "END") {  // i는 1부터 시작함에 유의
		this->m_cbLanguage.AddString(languages[i]);
	};
	

	for(int hour=0;hour<24;hour++){
		char buf[20];
		sprintf(buf,"GMT-%03d min", hour*30);
		this->m_cbTimeZone.AddString(buf);
		this->m_gmtVal[hour] = hour*30*(-1);
	}
	for(int hour=0;hour<24;hour++){
		char buf[20];
		sprintf(buf,"GMT+%03d min", hour*30);
		this->m_cbTimeZone.AddString(buf);
		this->m_gmtVal[hour+24] = hour*30;
	}
	
	if(CCopModule::GetObject()->HasOption("+sqisoft_only")==false){  // Manager 를 띠울때 +sqisoft_only 옵션을 주고 띠운경우에 한해서, 동작한다.
		this->m_btDel.EnableWindow(false); // Customer 삭제기능은 현재로서는 열어놓아서는 안된다.
	} 

	if(this->m_btMode == eCreate){
		this->m_btDel.EnableWindow(false);

		CString strTimeZoneName;
		LONG nMinute=0;
		GetTimeZoneMinute(strTimeZoneName,nMinute);
		int osTimeZone =  ((nMinute >=0) ? int(24+nMinute/30) : int((-1)*nMinute/30));

		this->m_cbLanguage.SetCurSel(0);
		this->m_cbTimeZone.SetCurSel(osTimeZone);

	}else{

		this->m_editCustomerId.EnableWindow(false);
		this->m_editCustomerId.SetWindowText(m_info.customerId);
		this->m_editDescription.SetWindowText(m_info.description);
		this->m_editURL.SetWindowText(m_info.url);

		// language 값 셋팅
		if(m_info.language.IsEmpty()){
			//this->m_cbLanguage.SetCurSel(0);  아무것도 없어야 한다.
		}else{
			if(m_info.language.CompareNoCase(osLang) != 0){  // 다르면
				int idx=-1;
				int i=0;
				while(languages[++i] != "END") {  // i는 1부터 시작함에 유의
					if(m_info.language.CompareNoCase(languages[i]) == 0){
						idx=i;
						break;
					}
				};
				if(idx<0){
					// 해당 언어가 콤보박스에 없다..제일 마지막에 추가한다.
					this->m_cbLanguage.AddString(m_info.language);
					this->m_cbLanguage.SetCurSel(i);
				}else{
					this->m_cbLanguage.SetCurSel(idx);
				}
			}else{
				this->m_cbLanguage.SetCurSel(0);
			}
		}

		// gmt 값 셋팅
		if(-720 <= m_info.gmt && m_info.gmt <= 720){
			int nMinute = m_info.gmt;
			int curTimeZone =  ((nMinute >=0) ? int(24+nMinute/30) : int((-1)*nMinute/30));
			this->m_cbTimeZone.SetCurSel(curTimeZone);
		}else{
			// 아무것도 없어야 한다.
		}
	}

	this->UpdateData(false);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
