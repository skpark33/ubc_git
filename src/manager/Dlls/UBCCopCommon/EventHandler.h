#pragma once

#include <cci/libWrapper/cciEventHandler.h>

#define IEH_UNKOWN		0x0000
#define IEH_OPSTAT		0x0001
#define IEH_UPPACKAGE	0x0002
#define IEH_ADSTAT		0x0003
#define IEH_SCHEREV		0x0004
#define IEH_REMVREV		0x0005
#define IEH_EXPIREV		0x0006
#define IEH_CHNGREV		0x0007
#define IEH_CREANNO		0x0008
#define IEH_CHGANNO		0x0009
#define IEH_RMVANNO		0x000A
#define IEH_EXPANNO		0x000B
#define IEH_CHNGVNC		0x000C
#define IEH_CREBPID		0x000D
#define IEH_CHGBPID		0x000E
#define IEH_RMVBPID		0x000F
#define IEH_CRESLOG		0x0010	// Package Log Created
#define IEH_CREPLOG		0x0011	// Power on/off Log Created
#define IEH_CRECLOG		0x0012	// Connection Log Created
#define IEH_FLTALAM     0x0013	// Fault Alarm
#define IEH_UPDALAM     0x0014	// Update Alarm
#define IEH_CHNGPRS     0x0015	// Process State Changed
#define IEH_CHNGDOWN    0x0016	// Download State Changed // 0000707: 콘텐츠 다운로드 상태 조회 기능
#define IEH_SUMALAM     0x0017	// 장애요약 alarmSummary 이벤트
#define IEH_MOSTAT		0x0018	// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
#define IEH_HDDCHNG		0x0019	// disk 잔량 이벤트
#define IEH_DEVICE_OPSTAT 0x0020 // deviceOperationalStateChanged
const UINT WM_INVOKE_EVENT  = RegisterWindowMessage(_T("WM_INVOKE_EVENT"));

interface IEventHandler
{
	virtual int InvokeEvent(WPARAM, LPARAM) = 0;
};

struct SOpStat{
	SOpStat() {
		memset(siteId,0x00,100);
		memset(hostId,0x00,100);
		operationalState=false;
	}
	char siteId[100];
	char hostId[100];
	bool operationalState;
};

struct SDeviceOpStat{
	SDeviceOpStat() {
		memset(siteId,0x00,100);
		memset(hostId,0x00,100);
		memset(objectClass,0x00,100);
		memset(objectId,0x00,100);
		operationalState=false;
	}
	char siteId[100];
	char hostId[100];
	char objectClass[100];
	char objectId[100];
	bool operationalState;
};
struct SAdStat{
	SAdStat() {
		memset(siteId,0x00,100);
		memset(hostId,0x00,100);
		adminState=false;
	}
	char siteId[100];
	char hostId[100];
	bool adminState;
};

// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
struct SMonitorStat{
	SMonitorStat() {
		memset(siteId,0x00,100);
		memset(hostId,0x00,100);
		monitorState=0;
	}
	char siteId[100];
	char hostId[100];
	int	 monitorState;
};

struct SUpPackage{
	CString siteId;
	CString hostId;
	CString currentPackage1;
	CString currentPackage2;
	CString autoPackage1;
	CString autoPackage2;
	CString lastPackage1;
	CString lastPackage2;
	short networkUse1;
	short networkUse2;
	CString hostMsg1;
	CString hostMsg2;
};

struct SPackageStateLog
{
	CString siteId;
	CString hostId;
	CTime	eventTime;
	CString currentPackage1;
	CString currentPackage2;
	CString autoPackage1;
	CString autoPackage2;
	CString lastPackage1;
	CString lastPackage2;
};

struct SPowerStateLog
{
	CString siteId;
	CString hostId;
	CTime	eventTime;
	CTime	bootUpTime;
	CTime	bootDownTime;
};

struct SConnectionStateLog
{
	CString siteId;
	CString hostId;
	CTime	eventTime;
	bool	operationalState;
};

struct SFault
{
	CString faultId;
	CString siteId;
	CString hostId;
	long	counter;
	long	severity;
	CString probableCause;
	CTime	eventTime;
	CTime	updateTime;
	CString additionalText;
};

struct SFaultUpdate
{
	CString faultId;

	bool    bCounter;
	long	counter;
	
	bool    bUpdateTime;
	CTime	updateTime;
	
	bool    bAdditionalText;
	CString additionalText;

	SFaultUpdate()
	{
		bCounter = false;
		bUpdateTime = false;
		bAdditionalText = false;
	}
};

struct SProcessStateChange
{
	CString siteId;
	CString hostId;
	bool	starterState;
	bool	browserState;	// 앞면
	bool	browser1State;	// 뒷면
	bool	firmwareViewState;
	bool	preDownloaderState;

	SProcessStateChange()
	{
		starterState = false;
		browserState = false;
		browser1State = false;
		firmwareViewState = false;
		preDownloaderState = false;
	}
};

// 0000707: 콘텐츠 다운로드 상태 조회 기능
struct SDownloadStateChange
{
	CString siteId;
	CString hostId;
	int		brwId;
	CString programId;
	CString	progress;
	int		programState;	// 0 : init, 1: start, 2:partially failed, 3:failed,4:succeed
	CTime   programStartTime;
	CTime   programEndTime;

	SDownloadStateChange()
	{
		brwId = 0;
		programState = 0;
	}
};

struct SAlarmSummary
{
	CString summary;
	CTime   updateTime;

	// summary 에서 문자열을 파싱한 결과임
	int		nProcessDown;
	int		nCommunicationFail;
	int		nHddOverload;
	int		nMemOverload;
	int		nCpuOverload;
	int		nEtcAlarm;

	SAlarmSummary()
	{
		nProcessDown = 0;
		nCommunicationFail = 0;
		nHddOverload = 0;
		nMemOverload = 0;
		nCpuOverload = 0;
		nEtcAlarm = 0;
	}
};

struct SDiskAvailChanged
{
	CString siteId;
	CString hostId;
	int		diskId;
	float	diskAvail;

	SDiskAvailChanged()
	{
		diskId = 0;
		diskAvail = -1;
	}
};

class AFX_EXT_CLASS CEventHandler : public cciEventHandler
{
	static int m_nRefCnt;
	static bool m_bIgnoreEvent;
	static UINT CloseWaitAllEventThread(LPVOID pParam);

public:
	CEventHandler(IEventHandler** pIEH)
	{
		m_ppInterface = pIEH;
	}
	virtual ~CEventHandler()
	{
		m_ppInterface = NULL;
	}
	virtual void processEvent(cciEvent& ev);

	static bool CloseWaitAllEvent(HWND hWnd);
	static void SetIgnoreEvent(bool bIgnoreEvent) { m_bIgnoreEvent = bIgnoreEvent; }

protected:
	IEventHandler** m_ppInterface;
};

class AFX_EXT_CLASS CEventManager
{
public:
	CEventManager(IEventHandler*);
	virtual ~CEventManager();

//	int SetStatChangeEvent(LPCTSTR szSite);
	int AddEvent(LPCTSTR szParam);
	BOOL RemoveEvent(int nID);
	BOOL RemoveAllEvent();

	static void ClearEvent();

protected:
public:
protected:
	CList<int> m_lsEventID;
	IEventHandler* m_pInterface;

	// 0000078: 서버가 죽은 상태에서, Manager 를 죽이면 Manager가 Hang걸리지 않도록 수정
	static bool m_bServerStatus;

public:
	static void SetServerStatus(bool bServerStatus) { CEventManager::m_bServerStatus = bServerStatus; }
	static bool GetServerStatus() { return CEventManager::m_bServerStatus; }

	// 0001414: 단말목록등 서버로부터 이벤트를 받는 화면이 닫힐때 응답없음으로 빠지는 문제.
	void SetInterfacePtr(IEventHandler* pInterface) { m_pInterface = pInterface; }
	IEventHandler* GetInterfacePtr() { return m_pInterface; }
};
