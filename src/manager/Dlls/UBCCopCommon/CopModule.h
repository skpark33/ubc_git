#pragma once

#include <list>
#include <map>
#include "common\ubcdefine.h"
#include <cci/libWrapper/cciCall.h>
#include <ci/libBase/ciTime.h>
#include "common\libcommon\ubchost.h"

class cciGUIORBThread;

// 진행 상태 이벤트
interface ICallProgressHandler
{
	virtual void ProgressEvent(int max, int current) =0;
	virtual void ResultComplete(bool retval) = 0;
};

//skpark 2013.4.24 WOL 을 위한 정보 수집 구조체
struct SRelayHostInfo {
	std::set<std::string> target_hostId_set;
	CString relay_ipAddress;
	CString relay_hostId;
	CString relay_hostName;
	int		relay_hostType;
};
typedef map<CString,SRelayHostInfo*>	RelayHostInfoMap;  // siteId , SRelayHostInfo map


struct SUserInfo{
	CString mgrId;
	CString siteId;
	CString siteName;
	CString userId;
	CString password;
	CString userName;
	CString sid;
	BYTE	userType;
	CString email;
	CString phoneNo;

	CString siteList;	// 관리대상 사이트 예) ["SQI","DEMO"]
	CString hostList;	// 관리대상 호스트

	// 고장 알람 수신여부 및 대상 장애 유형
	bool	useSms;
	bool	useEmail;
	CString probableCauseList;

	CString roleId;
	CTime	validationDate;
	CTime	lastLoginTime;
	CTime	lastPWChangeTime;
	bool	bReadOnly; // 조회권한만있는 경우.
	bool	passwordChanged;

	SUserInfo(){
		mgrId.Empty();
		siteId.Empty();
		siteName.Empty();
		userId.Empty();
		password.Empty();
		userName.Empty();
		sid.Empty();
		userType = 0;
		email.Empty();
		phoneNo.Empty();

		siteList.Empty();
		hostList.Empty();

		useSms = false;
		useEmail = false;
		probableCauseList.Empty();

		roleId.Empty();
		bReadOnly = false;
		passwordChanged = false;
	}
};

struct SSiteInfo{
	CString mgrId;
	CString siteId;
	CString siteName;
	//CString chainNo;
	CString businessType;
	CString businessCode;
	//CString shopOpenTime;
	//CString shopCloseTime;
	//std::list<std::string> holiday;
	CString parentId;
	CString comment1;	// 거점 주소
	CString phoneNo1;	// 거점 전화번호
	CString ceoName;	// 담당자
	CString phoneNo2;	// 담당자 연락처
	CString comment2;	// 기타
	bool	bReadOnly; // 조회권한만있는 경우.

	SSiteInfo(){
		mgrId.Empty();
		siteId.Empty();
		siteName.Empty();
		//chainNo.Empty();
		businessType.Empty();
		businessCode.Empty();
		//shopOpenTime.Empty();
		//shopCloseTime.Empty();
		//holiday.clear();
		parentId.Empty();
		comment1.Empty();
		phoneNo1.Empty();
		ceoName.Empty();
		phoneNo2.Empty();
		comment2.Empty();
		bReadOnly = false;
	}

	friend bool operator==( __in const SSiteInfo& val1, __in const SSiteInfo& val2 ) throw()
	{
		return( val1.siteId == val2.siteId );
	}
};

class STimeLine {
public:
	ciTime startTime;
	ciTime endTime;
	CString programId;

	STimeLine& operator= (const STimeLine& info){
		startTime = info.startTime;
		endTime = info.endTime;
		programId = info.programId;
	}
};
typedef list<STimeLine> TimeLineList; // key 는 startTime string 이다.

class STimeLineInfo {
public:
	CString			nextSchedule1;
	CString			nextSchedule2;
	//CString			entity;
	TimeLineList	timeLineList1;
	TimeLineList	timeLineList2;

	STimeLineInfo() { clear(); }
	~STimeLineInfo() { clear(); }

	STimeLineInfo& operator= (const STimeLineInfo& info)
	{
		nextSchedule1 = info.nextSchedule1;
		nextSchedule2 = info.nextSchedule2;

		STimeLineInfo* info_ptr = (STimeLineInfo*)&info; //Const type 이라서 동작을 제대로 안하기 때문에 포인터 사용

		TimeLineList::iterator otr;
		for(otr=info_ptr->timeLineList1.begin();otr!=info_ptr->timeLineList1.end();otr++){
			timeLineList1.push_back(*otr);
		}
		for(otr=info_ptr->timeLineList2.begin();otr!=info_ptr->timeLineList2.end();otr++){
			timeLineList2.push_back(*otr);
		}
		return *this;
	}

	void clear() {
		timeLineList1.clear();
		timeLineList2.clear();
		nextSchedule1 = "";
		nextSchedule2 = "";
		//entity = "";
	}
};

struct SHostInfo{
	CString mgrId;
	CString siteId;
	CString siteName;
	CString hostId;
	CString hostName;
	CString domainName;//skpark
	long	hostType;
	CString vendor;
	short	monitorType;
	CString model;
	CString os;
	ULONG serialNo;
	CString version;
	short   period;
	short	screenshotPeriod;
	CString description;
	CString ipAddress;
	bool    adminState;
	bool    operationalState;
	CString macAddress;
	CString edition;
	CTime   authDate;
	CTime	createTime;
	short   displayCounter;
	CString autoPackage1;
	CString autoPackage2;
	CString currentPackage1;
	CString currentPackage2;
	CString lastPackage1;
	CString lastPackage2;
	CTime   lastPackageTime1;
	CTime   lastPackageTime2;
	CTime	bootUpTime;
	bool    networkUse1;
	bool    networkUse2;
	CTime   lastUpdateTime;
	long	vncPort;
	bool	vncState;
	CString location;
//	CTime	startupTime;	// 2010.11.08 박선견 time 아니고 string 이라고 함
//	CTime	shutdownTime;
	CString startupTime;
	CString shutdownTime;
	short	powerControl;
	short	soundVolume;
	bool	mute;
	short	soundDisplay;
	float	cpuUsed;
	CString filesystemInfo[2];
	float	filesystemCapacity[2];
	long	realMemoryUsed;
	long	realMemoryTotal;
	long	virtualMemoryUsed;
	long	virtualMemoryTotal;
	CString	holiday;
	short	cpuTemp;
	bool	autoUpdateFlag;
	short	playLogDayLimit;
	short   hddThreshold;
	CString playLogDayCriteria;
	long	renderMode;
	CString category;
	CString contentsDownloadTime; // 10자리 예) 00:00
	bool	starterState;
	bool	browserState;	// 앞면
	bool	browser1State;	// 뒷면
	bool	preDownloaderState;
	bool	firmwareViewState;	
	int		download1State;	// 0000707: 콘텐츠 다운로드 상태 조회 기능
	CString	progress1;		// ..
	int		download2State;	// ..
	CString	progress2;		// ..
	bool	monitorOff;	// 모니터 절전 기능 관련
	std::list<std::string> monitorOffList; // 모니터 절전 기능 관련
	int		monitorState; // 1:on, 0:off, -1:unknown
	short	gmt;
	long	monitorUseTime;
	CString weekShutdownTime;
	CString hostMsg1;
	CString hostMsg2;
	float	disk1Avail;	// hdd disk 남은 용량 GByte 단위
	float	disk2Avail;
	CString protocolType;
	CString sosId;

	STimeLineInfo	timeLineInfo;
	
	SHostInfo(){
		mgrId.Empty();
		siteId.Empty();
		siteName.Empty();
		hostId.Empty();
		hostName.Empty();
		hostType = -1;
		vendor.Empty();
		monitorType=0;
		model.Empty();
		os.Empty();
		serialNo = 0;
		version.Empty();
		period = 0;
		screenshotPeriod = 0;
		description.Empty();
		ipAddress.Empty();
		adminState = false;
		operationalState = false;
		macAddress.Empty();
		edition.Empty();
//		authDate;
		displayCounter = 0;
		autoPackage1.Empty();
		autoPackage2.Empty();
		currentPackage1.Empty();
		currentPackage2.Empty();
		lastPackage1.Empty();
		lastPackage2.Empty();
		networkUse1 = false;
		networkUse2 = false;
//		lastUpdateDate;
		vncPort = 0;
		vncState = false;
		location.Empty();
		startupTime.Empty();
		shutdownTime.Empty();
		powerControl = 0;
		soundVolume = 0;
		mute = false;
		soundDisplay = 1;
		cpuUsed = 0.0f;
		filesystemCapacity[0] = 0.0f;
		filesystemCapacity[1] = 0.0f;
		realMemoryUsed = 0;
		realMemoryTotal = 0;
		virtualMemoryUsed = 0;
		virtualMemoryTotal = 0;
		holiday.Empty();
		cpuTemp = 0;
		autoUpdateFlag = false;
		playLogDayLimit = 15;
		hddThreshold = 95;
		playLogDayCriteria = "09:00";
		renderMode = 1;
		category.Empty();
		contentsDownloadTime.Empty();
		starterState = false;
		browserState = false;	// 앞면
		browser1State = false;	// 뒷면
		preDownloaderState = false;
		firmwareViewState = false;
		download1State = 0;	// 0000707: 콘텐츠 다운로드 상태 조회 기능
		progress1.Empty();
		download2State = 0;
		progress2.Empty();
		monitorOff = false;
		monitorOffList.clear();
		monitorState = -1;
		gmt = 9999;
		monitorUseTime = -1;
		weekShutdownTime.Empty();
		hostMsg1.Empty();
		hostMsg2.Empty();
		disk1Avail = -1;
		disk2Avail = -1;
		protocolType.Empty();
	}
};

// 0000751: 콘텐츠 다운로드 상태 조회 기능
struct SDownloadStateInfo
{
	CString		mgrId;
	CString		siteId;
	CString		hostId;
	int			brwId;
	CString		programId;
	int			programState;		// 전체 다운로드 정보
	time_t		programStartTime;
	time_t		programEndTime;
	CString		progress;

	CString		downloadId;			// 콘텐츠 개별 정보
	CString		contentsId;
	CString		contentsName;
	int			contentsType;
	CString		location;
	CString		filename;
	ULONGLONG	volume;
	time_t		startTime;
	time_t		endTime;
	ULONGLONG	currentVolume;
	bool		result;
	int			reason;
	int			downState;

	SDownloadStateInfo(){
		mgrId.Empty();
		siteId.Empty();
		hostId.Empty();
		brwId = 0;
		programId.Empty();
		programState=0;
		programStartTime=0;
		programEndTime=0;
		progress.Empty();

		downloadId.Empty();
		contentsId.Empty();
		contentsName.Empty();
		contentsType=0;
		location.Empty();
		filename.Empty();
		volume=0;
		startTime=0;
		endTime=0;
		currentVolume=0;
		result=true;
		reason=0;
		downState=0;
	}
};

struct SPackageInfo{
	time_t  tmUpdateTime;
	CString	szProcID;
	CString	szSiteID;
	CString	szPackage;
	CString	szDescript;
	CString	szUpdateUser;
	CString templatePlayList;
	CString serialNo;

	// 패키지객체 속성추가
	LONG	nContentsCategory;	// 종류 (모닝,..)
	LONG	nPurpose         ;	// 용도별 (교육용,..)
	LONG	nHostType        ;	// 단말타입 (키오스크..)
	LONG	nVertical        ;	// 가로/세로 방향 (가로,..)
	LONG	nResolution      ;	// 해상도 (1920x1080,..)
	bool	bIsPublic        ;	// 공개여부
	time_t	tmValidationDate ;	// 패키지유효기간
	bool	bIsVerify        ;	// 승인여부

	ULONGLONG volume;

	SPackageInfo(){
		tmUpdateTime = 0;
		szProcID.Empty();
		szSiteID.Empty();
		szPackage.Empty();
		szDescript.Empty();
		szUpdateUser.Empty();
		templatePlayList.Empty();
		serialNo.Empty();
		
		nContentsCategory = -1;
		nPurpose          = -1;
		nHostType         = -1;
		nVertical         = -1;
		nResolution       = -1;
		bIsPublic         = false;
		tmValidationDate  = 0;
		bIsVerify         = false;

		volume = -1;
	}
};

struct SHostRevInfo{
	BYTE disSide;
	CString siteId;
	CString siteName;
	CString hostId;
	CString hostName;
	std::list<std::string> reservedPackageList;
	CString lastPackage;
	CString currentPackage;
	CString autoPackage;

	SHostRevInfo(){
		disSide = 0;
		siteId.Empty();
		siteName.Empty();
		hostId.Empty();
		hostName.Empty();
		reservedPackageList.clear();
		lastPackage.Empty();
		currentPackage.Empty();
		autoPackage.Empty();
	}
};

struct SPackageRevInfo{
	BYTE side;
	time_t fromTime;
	time_t toTime;
	CString reservationId;
	CString siteId;
	CString hostId;
	CString programId;

	SPackageRevInfo(){
		side = 0;
		fromTime = 0;
		toTime = 0;
		reservationId.Empty();
		siteId.Empty();
		hostId.Empty();
		programId.Empty();
	}
};

enum { e_VIDEO, e_IMAGE, e_TIKER, e_FLASH, e_WEB, e_PPT, e_TYPEEND };

struct SAnnounceInfo{
	CString MgrId;
	CString SiteId;
	CString AnnoId;
	CString Title;
	LONG SerialNo;
	CString Creator;
	CTime CreateTime;
	CTime StartTime;
	CTime EndTime;
	BYTE Position;
	short Height;
	CString Font;
	short FontSize;
	CString FgColor;
	CString BgColor;
	CString BgLocation;
	CString BgFile;
	short Speed;
	short Alpha;
	CString Comment[10];
	std::list<std::string> HostIdList;
	
	std::list<std::string> HostNameList;
	short	ContentsType;
	short LRMargin;
	short UDMargin;
	short SourceSize;
	int SoundVolume;
	ULONGLONG FileSize;
	CString FileMD5;
	bool	IsFileChanged;// DB 에는 기록되지 않는 값이다.
	float	HeightRatio; // 가로대 세로비 가로를 1로 했을때, 세로의 값이다. DB 에는 기록되지 않는다.
	short	Width;
	short   PosX;
	short   PosY;

	SAnnounceInfo(){
		MgrId.Empty();
		SiteId.Empty();
		AnnoId.Empty();
		Title.Empty();
		SerialNo = 0;
		Creator.Empty();
		//CreateTime;
		//StartTime;
		//EndTime;
		Position = 0;
		Height = 100;
		//Font = UBCDEFAULT_FONT;
		Font = _T("System"); //
		FontSize = 24;
		FgColor = "#FFFFFF";
		BgColor = "#000000";
		BgLocation = "/Contents/announce/";
		BgFile.Empty();
		Speed = 1;
		Alpha = 0;

		for(int i = 0; i < 10; i++)
			Comment[i].Empty();

		ContentsType = e_TIKER;
		LRMargin=0;
		UDMargin=0;
		SourceSize=1;
		SoundVolume=100;
		FileSize=0;
		IsFileChanged=false;
		HeightRatio = (9.0/16.0);//16:9 가 기본임
		Width=100;
		PosX=0;
		PosY=0;
		
	}
};
//skpark lmo_solution
struct SLMOSolutionInfo {
	CString mgrId;
	CString lmId;
	CString description;
	CString tableName;
	CString timeFieldName;
	short	logType;
	long	duration;
	bool	adminState;

	SLMOSolutionInfo(){
		logType = 0;
		duration = 0;
		adminState=true;
	}
};

struct SPackageStateLogInfo
{
	CString siteId;
	CString hostId;
	CTime	eventTime;
	CString currentPackage1;
	CString currentPackage2;
	CString autoPackage1;
	CString autoPackage2;
	CString lastPackage1;
	CString lastPackage2;
};

struct SPowerStateLogInfo
{
	CString siteId;
	CString hostId;
	CTime	eventTime;
	CTime	bootUpTime;
	CTime	bootDownTime;
};

struct SConnectionStateLogInfo
{
	CString siteId;
	CString hostId;
	CTime	eventTime;
	bool	operationalState;

	SConnectionStateLogInfo()
	{
		operationalState = false;
	}
};

struct SLogConfigInfo
{
	bool bIsExist       ;
	bool packageState  ;
	bool powerState     ;
	bool connectionState;
	LONG duration       ;

	SLogConfigInfo()
	{
		bIsExist        = false;
		packageState   = true;
		powerState      = true;
		connectionState = true;
		duration        = 3;
	}
};

struct SFaultInfo
{
	CString faultId;
	CString siteId;
	CString hostId;
	long	counter;
	long	severity;
	CString probableCause;
	CTime	eventTime;
	CTime	updateTime;
	CString additionalText;
};

struct SFaultUpdateInfo
{
	CString faultId;

	bool    bCounter;
	long	counter;
	
	bool    bUpdateTime;
	CTime	updateTime;
	
	bool    bAdditionalText;
	CString additionalText;

	SFaultUpdateInfo()
	{
		bCounter = false;
		bUpdateTime = false;
		bAdditionalText = false;
	}
};

// 방송계획 조회시 조회필터에 포함할 단말의 객체
struct SFilterHostInfo
{
	CString strHostId;	// Host ID
	CString strSiteId;	// SITE ID
};

typedef enum PROC_TYPE { PROC_UNKNOWN, PROC_NEW, PROC_UPDATE, PROC_DELETE };

// 방송계획(BroadCasting Plan) 객체 (시간 계획의 집합)
struct SBroadcastingPlanInfo
{
	PROC_TYPE	nProcType	  ;	// 0:Unknown, 1:New, 2:Update, 3:Delete
	CString		strMgrId      ;	// 프로세스ID
	CString 	strSiteId     ;	// SITE ID
	CString 	strBpId       ;	// BroadCasting Plan ID
	CString 	strBpName     ;	// 예약명
	CTime   	tmCreateTime  ;	// 생성 시간
	CTime   	tmStartDate   ;	// 시작날짜 "YYYY/mm/dd HH:MM:ss"
	CTime   	tmEndDate     ;	// 끝 날짜  "YYYY/mm/dd HH:MM:ss"
	CString 	strWeekInfo   ;	// "0,1,2,3,4,5,6" 표시된 요일만 방송하게 된다.  디폴트는 전체가 모두 선택된 것이다.
	CString 	strExceptDay  ;	// Ex)  "12.25,1.1" 해당 일자는 방송을 하지 않는다.
	CString 	strDescription;	// 설명
	CString 	strZOrder     ;	// 겹칠 경우 방송 우선순위. 1000 단위로 증가한다.  숫자가 클수록 우선 순위가 높다
	CString		strRegisterId ; // 등록자
	CTime   	tmDownloadTime;	// 배포시간 "YYYY/mm/dd HH:MM:ss"
	int			nRetryCount   ; // 배포 재시도 횟수
	int			nRetryGap     ; // 배포 재시도 간격
	bool		bReadOnly     ; // 조회권한만있는 경우. 0001374: [RFP] 지점에서 상위조직에서 설정한 방송계획을 볼 수 있고, 우선순위를 조정할 수 있는 기능

	SBroadcastingPlanInfo()
	{
		nProcType    = PROC_UNKNOWN;
		tmCreateTime = CTime(2099,1,1,0,0,0);
		strWeekInfo  = _T("0,1,2,3,4,5,6");
		tmDownloadTime = CTime::GetCurrentTime();
		nRetryCount  = 0;
		nRetryGap    = 0;
		bReadOnly    = false;
	}

	SBroadcastingPlanInfo Clone()
	{
		SBroadcastingPlanInfo info;
//		info.strMgrId       = strMgrId      ;
//		info.strSiteId      = strSiteId     ;
//		info.strBpId        = strBpId       ;
//		info.strBpName      = strBpName     ;
//		info.tmCreateTime   = tmCreateTime  ;
		info.tmStartDate    = tmStartDate   ;
		info.tmEndDate      = tmEndDate     ;
		info.strWeekInfo    = strWeekInfo   ;
		info.strExceptDay   = strExceptDay  ;
		info.strDescription = strDescription;
//		info.strZOrder      = strZOrder     ;

		info.strRegisterId  = strRegisterId ;
		info.tmDownloadTime = tmDownloadTime;
		info.nRetryCount    = nRetryCount   ;
		info.nRetryGap      = nRetryGap     ;

		info.bReadOnly      = bReadOnly     ;

		return info;
	}

	bool operator== (const SBroadcastingPlanInfo& info)
	{
		if(info.strMgrId       != strMgrId      ) return false;
		if(info.strSiteId      != strSiteId     ) return false;
		if(info.strBpId        != strBpId       ) return false;
		if(info.strBpName      != strBpName     ) return false;
		if(info.tmCreateTime   != tmCreateTime  ) return false;
		if(info.tmStartDate    != tmStartDate   ) return false;
		if(info.tmEndDate      != tmEndDate     ) return false;
		if(info.strWeekInfo    != strWeekInfo   ) return false;
		if(info.strExceptDay   != strExceptDay  ) return false;
		if(info.strDescription != strDescription) return false;
		if(info.strZOrder      != strZOrder     ) return false;
		if(info.strRegisterId  != strRegisterId ) return false;
		if(info.tmDownloadTime != tmDownloadTime) return false;
		if(info.nRetryCount    != nRetryCount   ) return false;
		if(info.nRetryGap      != nRetryGap     ) return false;

		return true;
	}
};

// 시간계획(Time Plan) 객체 (하나의 프로그램이  몇시부터 몇시까지 방송될 것인가의 정보)
struct STimePlanInfo
{
	PROC_TYPE	nProcType	;	// 0:Unknown, 1:New, 2:Update, 3:Delete
	CString 	strMgrId    ;	// 프로세스ID
	CString 	strSiteId   ;	// SITE ID
	CString 	strBpId     ;	// BroadCasting Plan ID
	CString 	strTpId     ;	// Key 번호
	CTime		tmCreateTime;	// 생성 시간
	CString 	strStartTime;	// 시작시간 “00:00:00”
	CString 	strEndTime  ;	// 끝 시간  “00:00:00”
	CString 	strProgramId;	// 방송될 프로그램 ID
	CString 	strZOrder   ;	// 겹칠 경우 방송 우선순위. 1000 단위로 증가한다.  숫자가 클수록 우선 순위가 높다

	STimePlanInfo()
	{
		nProcType    = PROC_UNKNOWN;
		tmCreateTime = CTime(2099,1,1,0,0,0);
	}

	STimePlanInfo Clone()
	{
		STimePlanInfo info;

//		info.strMgrId     = strMgrId    ;
//		info.strSiteId    = strSiteId   ;
//		info.strBpId      = strBpId     ;
//		info.strTpId      = strTpId     ;
//		info.tmCreateTime = tmCreateTime;
		info.strStartTime = strStartTime;
		info.strEndTime   = strEndTime  ;
		info.strProgramId = strProgramId;
		info.strZOrder    = strZOrder   ;

		return info;
	}

	bool operator== (const STimePlanInfo& info)
	{
		if(info.strMgrId     != strMgrId    ) return false;
		if(info.strSiteId    != strSiteId   ) return false;
		if(info.strBpId      != strBpId     ) return false;
		if(info.strTpId      != strTpId     ) return false;
		if(info.tmCreateTime != tmCreateTime) return false;
		if(info.strStartTime != strStartTime) return false;
		if(info.strEndTime   != strEndTime  ) return false;
		if(info.strProgramId != strProgramId) return false;
		if(info.strZOrder    != strZOrder   ) return false;

		return true;
	}
};

// 대상 단말(Target Host) 객체 (하나의 방송계획이 적용되는 단말 객체)
struct STargetHostInfo
{
	PROC_TYPE	nProcType    ;	// 0:Unknown, 1:New, 2:Update, 3:Delete
	CString 	strMgrId     ;	// 프로세스ID
	CString 	strSiteId    ;	// SITE ID
	CString 	strBpId      ;	// BroadCasting Plan ID
	CString 	strThId      ;	// hostId
	CString		strTargetHost;  // targetHost
	int			nSide        ;	// 앞면 : 1 뒷면 :2  양면 : 3
	CString		strHostName  ;  // targetHostName

	STargetHostInfo()
	{
		nProcType = PROC_UNKNOWN;
		nSide = 1;
	}

	STargetHostInfo Clone()
	{
		STargetHostInfo info;

//		info.strMgrId      = strMgrId     ;
//		info.strSiteId     = strSiteId    ;
//		info.strBpId       = strBpId      ;
		info.strThId       = strThId      ;
		info.strTargetHost = strTargetHost;
		info.nSide         = nSide        ;
		info.strHostName   = strHostName  ;

		return info;
	}

	bool operator== (const STargetHostInfo& info)
	{
		if(info.strMgrId      != strMgrId     ) return false;
		if(info.strSiteId     != strSiteId    ) return false;
		if(info.strBpId       != strBpId      ) return false;
		if(info.strThId       != strThId      ) return false;
		if(info.strTargetHost != strTargetHost) return false;
		if(info.nSide         != nSide        ) return false;

		return true;
	}
};

struct SCodeItem
{
	CString strMgrId;
	CString strCodeId;
	CString strCategoryName;
	CString strEnumString;
	int		nEnumNumber;
	bool	bVisible;
	short	nDisplayOrder;

	SCodeItem()
	{
		nEnumNumber = 0;
		bVisible = 0;
		nDisplayOrder = 0;
	}
};

struct SMenuAuthInfo
{
	CString		mgrId;			// manager process id
	CString		menuId;			// menuId 

	CString		menuName;		// 메뉴명
	CString		menuType;		// S:솔루션, W: 웹
	bool		cAuth;			// create	
	bool		rAuth;			// read
	bool		uAuth;			// update	
	bool		dAuth;			// delete
	bool		aAuth;			// approve

	SMenuAuthInfo()
	{
		mgrId.Empty();			// manager process id
		menuId.Empty();			// menuId 

		menuName.Empty();		// 메뉴명
		menuType.Empty();		// S:솔루션, W: 웹
		cAuth = false;			// create	
		rAuth = false;			// read
		uAuth = false;			// update	
		dAuth = false;			// delete
		aAuth = false;			// approve
	}
};

struct SDownloadSummaryInfo
{
	CString		mgrId;
	CString		siteId;
	CString		hostId;
	CString		downloadId;			// 콘텐츠 개별 정보
	int			brwId;
	CString		programId;
	int			programState;		// 전체 다운로드 정보
	time_t		programStartTime;
	time_t		programEndTime;
	CString		progress;
	CString		siteName;	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다
	CString		hostName;	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다

	SDownloadSummaryInfo()
	{
		mgrId.Empty();
		siteId.Empty();
		hostId.Empty();
		downloadId.Empty();
		brwId = 0;
		programId.Empty();
		programState=0;
		programStartTime=0;
		programEndTime=0;
		progress.Empty();
	}
};

struct SChildSiteInfo
{
	CString		mgrId;
	CString		siteId;
	CString		siteName;
	CString		parentId;
	int			lvl;
	CString		name_path;
	CString		id_path;

	CString		changing_parentId;
	bool		is_parent_changed;

	SChildSiteInfo()
	{
		lvl = 0;
	}
};
// ApplyLog : 방송명령로그
struct SApplyLogInfo
{
	CString		mgrId;
	CString		siteId;
	CString		hostId;		// where
	CString		applyId;
	CString		programId;	// what
	time_t		applyTime;	// when
	CString		how;		// BP or USER
	CString		who;		// userId or BPId
	CString		why;		// comment

	SApplyLogInfo()
	{
		mgrId.Empty();
		siteId.Empty();
		hostId.Empty();
		applyId.Empty();
		programId.Empty();
		applyTime=0;
		how.Empty();
		who.Empty();
		why.Empty();
	}
};

//ScheduleStateLog : 방송명령결과로그
struct SScheduleStateLogInfo
{
	CString		mgrId;
	CString		siteId;
	CString		hostId;		// where
	CString		logId;
	time_t		eventTime;	// when
	CString		lastSchedule1;
	CString		autoSchedule1;
	CString		currentSchedule1;

	SScheduleStateLogInfo()
	{
		mgrId.Empty();
		siteId.Empty();
		hostId.Empty();
		logId.Empty();
		eventTime=0;
		lastSchedule1.Empty();
		autoSchedule1.Empty();
		currentSchedule1.Empty();
	}
};

//UserLog : 로그인로그
struct SUserLogInfo
{
	CString		mgrId;
	CString		siteId;
	CString		userId;
	time_t		loginTime;
	CString		via;
	CString		result;

	SUserLogInfo()
	{
		mgrId.Empty();
		siteId.Empty();
		userId.Empty();
		loginTime=0;
		via.Empty();
		result.Empty();
	}
};
//BPLog : 로그인로그
struct SBPLogInfo
{
	CString		mgrId;
	CString		siteId;
	CString		bpName;
	time_t		touchTime;
	CString		who;
	CString		action;

	SBPLogInfo()
	{
		mgrId.Empty();
		siteId.Empty();
		bpName.Empty();
		touchTime=0;
		who.Empty();
		action.Empty();
	}
};

//PlayLog : 플레이로그
struct SPlayLogInfo
{
	CString		siteId;
	CString		hostId;
	time_t		playDate;
	CString		programId;
	CString		contentsName;
	CString		filename;
	unsigned long			playCount;
	unsigned long			playTime;
	unsigned long			failCount;

	SPlayLogInfo()
	{
		playCount = 0;
		playTime = 0;
		failCount = 0;
		playDate=0;
	}
};

// 0001408: Customer 정보 입력을 받는다.
struct SCustomerInfo
{
	CString mgrId;
	CString customerId;
	CString url;
	CString description;
	int		gmt;
	CString language;
	
	SCustomerInfo()
	{
		mgrId.Empty();
		customerId.Empty();
		url.Empty();
		description.Empty();
		gmt = 9999;
		language.Empty();
	}
};

// Role
struct SRoleInfo
{
	CString mgrId;
	CString roleId;
	CString roleName;
	CString roleDesc;
	CString customer;

	SRoleInfo()
	{
		mgrId.Empty();
		roleId.Empty();
		roleName.Empty();
		roleDesc.Empty();
		customer.Empty();
	}
};


struct SPlanoInfo
{
	CString	mgrId;	
	CString	siteId;	
	CString	planoId;	
	CString	backgroundFile;	
	CString	description;	
	CString	query; 
	BOOL	expand; 
	unsigned short	planoType; 

	SPlanoInfo() {
		expand=false;
		planoType=0;
	}
	SPlanoInfo(SPlanoInfo* pInfo) {
		set(pInfo);
	}
	void set(SPlanoInfo* pInfo) {
		expand=pInfo->expand;
		planoType=pInfo->planoType;
		mgrId = pInfo->mgrId;	
		siteId = pInfo->siteId;	
		planoId = pInfo->planoId;	
		backgroundFile = pInfo->backgroundFile;	
		description = pInfo->description;	
		query = pInfo->query; 
	}
};

struct SNodeViewInfo
{
	SNodeViewInfo() {
		x = 100;
		y = 100;
		nodeValue = 0;
		operationalState = false;
		adminState = true;
		vncPort=0;
		ipAddress="";
	}
	SNodeViewInfo(SNodeViewInfo& p) {
		mgrId			= p.mgrId; 
		siteId			= p.siteId;
		planoId			= p.planoId;
		nodeId			= p.nodeId;
		objectClass		= p.objectClass;
		objectId		= p.objectId;
		objectType		= p.objectType;
		x				= p.x;
		y				= p.y;
		targetName		= p.targetName;
		description		= p.description;
		operationalState= p.operationalState;
		adminState		= p.adminState;
		nodeValue		= p.nodeValue;
		startupTime		= p.startupTime;
		shutdownTime	= p.shutdownTime;
		hostId			= p.hostId;
		hostSiteId		= p.hostSiteId;
		weekShutdownTime= p.weekShutdownTime;
		holiday			= p.holiday;
		vncPort			= p.vncPort;
		ipAddress		= p.ipAddress;
	}

	CString			mgrId; 
	CString			siteId;	
	CString			planoId;	
	CString			nodeId;	

	CString			objectClass;		// classname
	CString			objectId;			// id
	CString			objectType;			// classname::type
	unsigned short	x;	
	unsigned short	y;	
	CString			targetName;	
	CString			description;	
	BOOL			operationalState;	// on/off 
	BOOL			adminState;	 
	long			nodeValue;			// soundVolume, brightness 등
	CString			startupTime;		// auto startup time
	CString			shutdownTime;		// auto shutdown time
	CString			hostId;
	CString			hostSiteId;
	CString			weekShutdownTime;
	CString			holiday;
	CString			ipAddress;
	long			vncPort;
};

struct SLightInfo
{
	SLightInfo() {
		adminState = false;
		operationalState = false;
		lightType = 0;
		brightness = -1;
	}
	CString  mgrId, siteId, hostId, lightId, description;
	CString  lightName;
	BOOL adminState;
	BOOL operationalState;
	CString startupTime, shutdownTime;
	long lightType;
	long brightness;
	CString holiday, weekShutdownTime;
};

struct SMonitorInfo
{
	SMonitorInfo() {
		adminState = false;
		operationalState = false;
		monitorType = 0;
		monitorUseTime = -1;
	}
	CString  mgrId, siteId, hostId, monitorId, description;
	CString  monitorName;
	BOOL adminState;
	BOOL operationalState;
	CString startupTime, shutdownTime;
	long monitorType;
	long monitorUseTime;
	CString holiday, weekShutdownTime;
};

struct SNodeTypeInfo
{
	SNodeTypeInfo() {
		objectState = false;
		objectType = 0;
	}
	CString  mgrId, nodeTypeId, objectClass, ext;
	BOOL objectState;
	long objectType;
};


class STrashCanInfo 
{
public:
	CString mgrId;
	CString trashCanId;
	CString siteId;
	CString	idValue;
	CString	nameValue;
	CString	entity;
	CString	attrList;
	time_t	eventTime;
	CString	userId;

	STrashCanInfo() : eventTime(0) {}
};
typedef CList<STrashCanInfo>			TrashCanInfoList;

typedef CList<SNodeTypeInfo>			NodeTypeInfoList;
typedef CList<SMonitorInfo>				MonitorInfoList;
typedef CList<SLightInfo>				LightInfoList;
typedef CList<SNodeViewInfo>			NodeViewInfoList;
typedef CList<SPlanoInfo*>				PlanoInfoList;
typedef CList<SUserInfo>				UserInfoList;
typedef CList<SSiteInfo>				SiteInfoList;
typedef CList<SHostInfo>				HostInfoList;
typedef CList<SHostRevInfo>				HostRevInfoList;
typedef CList<SPackageInfo>				PackageInfoList;
typedef CList<SPackageRevInfo>			PackageRevInfoList;
typedef CList<SAnnounceInfo>			AnnounceInfoList;
typedef CList<SLMOSolutionInfo>			LMOSolutionInfoList;  //skpark lmo_solution
typedef CList<SPackageStateLogInfo>		PackageStateLogInfoList;
typedef CList<SPowerStateLogInfo>		PowerStateLogInfoList;
typedef CList<SConnectionStateLogInfo>	ConnectionStateLogInfoList;
typedef CList<SFaultInfo>				FaultInfoList;
typedef CList<SFaultInfo>				FaultLogInfoList;
typedef CList<SBroadcastingPlanInfo>	BroadcastingPlanInfoList;
typedef CList<STimePlanInfo>			TimePlanInfoList;
typedef CList<STargetHostInfo>			TargetHostInfoList;
typedef CList<SDownloadStateInfo>		DownloadStateInfoList;	// 0000751: 콘텐츠 다운로드 상태 조회 기능
typedef CList<SCodeItem>				CodeItemList;
typedef CList<SDownloadSummaryInfo>		DownloadSummaryInfoList;	// 0000751: 콘텐츠 다운로드 상태 조회 기능
typedef CList<SMenuAuthInfo>			MenuAuthInfoList;
typedef CList<SChildSiteInfo>			ChildSiteInfoList;
typedef CList<SApplyLogInfo>			ApplyLogInfoList;	// 0001328: 단말 목록 화면에서 [컨텐츠 패키지 변경명령 이력] 을 볼수 있도록 한다.
typedef CList<SScheduleStateLogInfo>	ScheduleStateLogInfoList;	// 0001328: 단말 목록 화면에서 [컨텐츠 패키지 변경결과 이력] 을 볼수 있도록 한다.
typedef CList<SCustomerInfo>			CustomerInfoList;	// 0001408: Customer 정보 입력을 받는다.
typedef CList<SRoleInfo>				RoleInfoList;
typedef CList<SUserLogInfo>				UserLogInfoList;
typedef CList<SBPLogInfo>				BPLogInfoList;
typedef CList<SPlayLogInfo>				PlayLogInfoList;

///////////////////////////////////////////////////////////////////////////////
class CONTENTS_INFO_EX
{
public:
	// attributes
	CString		strMgrId;
	CString		strSiteId;
	CString		strProgramId;
	CString		strContentsId;
	CString		strCategory;
	CString		strContentsName;
	int 		nContentsType;
	CString		strParentsId;
	CString		strRegisterId;
	CTime		tmRegisterTime;
	CString		strLocalLocation          ;	// local contents location
	CString		strSeverLocation     ;	// server contents location
	CString		strFilename          ;
	ULONG 		nRunningTime         ;
	CString		strVerifier;	// server contents location
	CString		strDescription;
	CString		strComment[10]       ;
	CString		strBgColor           ;	// background color
	CString		strFgColor           ;	// foreground color
	CString		strFont              ;	// font
	int			nFontSize            ;	// font size
	int			nPlaySpeed           ;	// tiker 등이 플레이 되는 속도
	int 		nSoundVolume         ;
	bool		bIsPublic;
	int			nAlign               ;	// 정렬방향 1~9

	bool		bLocalFileExist      ;	///<콘텐츠의 경로에 실제 파일이 존재하는지 여부
	bool		bServerFileExist     ;

	int			nWidth               ;
	int			nHeight              ;
	int			nCurrentComment      ;

	//
	ULONGLONG	nFilesize            ;	// volume
	int			nContentsState       ;

	int			nContentsCategory;
	int			nPurpose;
	int			nHostType;
	int			nVertical;
	int			nResolution;
	CTime		tmValidationDate;
	CString		strRequester;
	CString		strVerifyMembers;
	CTime		tmVerifyTime;
	CString		strRegisterType;

	// 0000611: 플레쉬 콘텐츠 마법사
	CString		strWizardXML         ;  // wizard 용 XML string
	CString		strWizardFiles       ;  // wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

	CONTENTS_INFO_EX()
	: nContentsType (-1)
	, nRunningTime (0)
	, nFontSize (36)
	, nPlaySpeed (0)
	, nSoundVolume (0)
	, bIsPublic (false)
	, nAlign (5)
	, bLocalFileExist (false)
	, bServerFileExist (false)
	, nWidth (0)
	, nHeight (0)
	, nCurrentComment (0)
	, nFilesize (0)
	, nContentsState (3) // CON_READY
	, nContentsCategory (99)
	, nPurpose (99)
	, nHostType (99)
	, nVertical (99)
	, nResolution (99)
	{};

	virtual ~CONTENTS_INFO_EX() {};

	CONTENTS_INFO_EX& operator= (const CONTENTS_INFO_EX& info)
	{
		strMgrId = info.strMgrId;
		strSiteId = info.strSiteId;
		strProgramId = info.strProgramId;
		strContentsId = info.strContentsId;
		strCategory = info.strCategory;
		strContentsName = info.strContentsName;
		nContentsType = info.nContentsType;
		strParentsId = info.strParentsId;
		strRegisterId = info.strRegisterId;
		tmRegisterTime = info.tmRegisterTime;
		strLocalLocation = info.strLocalLocation;
		strSeverLocation = info.strSeverLocation;
		strFilename = info.strFilename;
		nRunningTime = info.nRunningTime;
		strVerifier = info.strVerifier;
		strDescription = info.strDescription;
		strBgColor = info.strBgColor;
		strFgColor = info.strFgColor;
		strFont = info.strFont;
		nFontSize = info.nFontSize;
		nPlaySpeed = info.nPlaySpeed;
		nSoundVolume = info.nSoundVolume;
		bIsPublic = info.bIsPublic;
		nAlign = info.nAlign;
		bLocalFileExist = info.bLocalFileExist;
		bServerFileExist = info.bServerFileExist;
		nWidth = info.nWidth;
		nHeight = info.nHeight;
		nCurrentComment = info.nCurrentComment;
		nFilesize = info.nFilesize;
		nContentsState = info.nContentsState;
		nContentsCategory = info.nContentsCategory;
		nPurpose = info.nPurpose;
		nHostType = info.nHostType;
		nVertical = info.nVertical;
		nResolution = info.nResolution;
		tmValidationDate = info.tmValidationDate;
		strRequester = info.strRequester;
		strVerifyMembers = info.strVerifyMembers;
		tmVerifyTime = info.tmVerifyTime;
		strRegisterType = info.strRegisterType;
		strWizardXML = info.strWizardXML;
		strWizardFiles = info.strWizardFiles;

		for(int i=0; i<10; i++) strComment[i] = info.strComment[i];

		return *this;
	}

	bool operator== (const CONTENTS_INFO_EX & info)
	{
		if(strMgrId				!= info.strMgrId) return false;
		if(strSiteId			!= info.strSiteId) return false;
		if(strProgramId			!= info.strProgramId) return false;
		if(strContentsId		!= info.strContentsId) return false;
		if(strCategory			!= info.strCategory) return false;
		if(strContentsName		!= info.strContentsName) return false;
		if(nContentsType		!= info.nContentsType) return false;
		if(strParentsId			!= info.strParentsId) return false;
		if(strRegisterId		!= info.strRegisterId) return false;
		if(tmRegisterTime		!= info.tmRegisterTime) return false;
		if(strLocalLocation		!= info.strLocalLocation) return false;
		if(strSeverLocation		!= info.strSeverLocation) return false;
		if(strFilename			!= info.strFilename) return false;
		if(nRunningTime			!= info.nRunningTime) return false;
		if(strVerifier			!= info.strVerifier) return false;
		if(strDescription		!= info.strDescription) return false;
		if(strBgColor			!= info.strBgColor) return false;
		if(strFgColor			!= info.strFgColor) return false;
		if(strFont				!= info.strFont) return false;
		if(nFontSize			!= info.nFontSize) return false;
		if(nPlaySpeed			!= info.nPlaySpeed) return false;
		if(nSoundVolume			!= info.nSoundVolume) return false;
		if(bIsPublic			!= info.bIsPublic) return false;
		if(nAlign				!= info.nAlign) return false;
		if(bLocalFileExist		!= info.bLocalFileExist) return false;
		if(bServerFileExist		!= info.bServerFileExist) return false;
		if(nWidth				!= info.nWidth) return false;
		if(nHeight				!= info.nHeight) return false;
		if(nCurrentComment		!= info.nCurrentComment) return false;
		if(nFilesize			!= info.nFilesize) return false;
		if(nContentsState		!= info.nContentsState) return false;
		if(nContentsCategory	!= info.nContentsCategory) return false;
		if(nPurpose				!= info.nPurpose) return false;
		if(nHostType			!= info.nHostType) return false;
		if(nVertical			!= info.nVertical) return false;
		if(nResolution			!= info.nResolution) return false;
		if(tmValidationDate		!= info.tmValidationDate) return false;
		if(strRequester			!= info.strRequester) return false;
		if(strVerifyMembers		!= info.strVerifyMembers) return false;
		if(tmVerifyTime			!= info.tmVerifyTime) return false;
		if(strRegisterType		!= info.strRegisterType) return false;
		if(strWizardXML			!= info.strWizardXML) return false;
		if(strWizardFiles		!= info.strWizardFiles) return false;

		for(int i=0; i<10; i++) if(strComment[i] != info.strComment[i]) return false;

		return true;
	} 
};

typedef CMapStringToPtr CONTENTS_INFO_MAP;
//typedef	CArray<CONTENTS_INFO_EX*, CONTENTS_INFO_EX*>	CONTENTS_INFO_EX_LIST;
typedef	CList<CONTENTS_INFO_EX>		CONTENTS_INFO_EX_LIST;

//skpark 2012.12.21 추가
// attribute 가 변경되었는지를 알린다.
// thread safe 하지 않다.
class AFX_EXT_CLASS CDirtyFlag
{
public:
	typedef map<ciString,ciBoolean>		DIRTY_MAP;  
	CDirtyFlag() {}
	~CDirtyFlag() { _map.clear();}

	bool IsDirty(const char* name);
	void SetDirty(const char* name, bool flag);
protected:
	DIRTY_MAP _map;
};

// <-- seventhstone 2015.05.13 추가
// 승인(Approve) 데이터
class APPROVE_INFO
{
public:
	APPROVE_INFO() { eApproveType=eTypeNoType; eApproveState=eStatusNoState; };
	~APPROVE_INFO() {};

	enum APPROVE_TYPE { eTypeNoType=0, 
						eTypeSite, eTypeHost, eTypeProgram, eTypeBP, eTypeCount, };

	enum APPROVE_STATUS { eStatusNoState=0, 
						  eStatusRequested, eStatusAccepted, eStatusRejected, eStatusRemoved, eStatusCount, };

	//
	APPROVE_TYPE	eApproveType;

	//
	CString			strMgrId;		// OD process id "OD"
	CString			strApproveId;	// GUID of approve

	APPROVE_STATUS	eApproveState;	// 0 : no_state 1 : requested 2: approved 3: rejected 4:remove
	CString			strAction;		// cciCall directive name : ex) remove
	CString			strTarget;		// cciCall entity name : ex) PM=1/Site=SQI/Program=axxxx
	CString			strDetails;		// cciCall attributes : ex) [{trashId_,super}]
	CString			strRequestedId;
	CString			strRequestedTime;
	CString			strApprovedId;
	CString			strApprovedTime;
	CString			strDescription1;
	CString			strDescription2;
};
typedef CArray<APPROVE_INFO*, APPROVE_INFO*>		APPROVE_INFO_LIST;
// -->

class cciCall;

class AFX_EXT_CLASS CCopModule
{
public:
	enum { eSiteUnkown, eSiteAdmin, eSiteManager, eSiteUser };
	enum { eFront, eBack, eAll }; // TargetDisplay

	static CCopModule* GetObject();
	static void Release();

	BOOL CopErrCheck(cciReply& reply, bool bShowMsgBox=true);

	BOOL IsInitORB() { return m_bIsInitORB; }
	BOOL InitORB(LPCTSTR szIP, int nPort);
	void DestroyORB();
	BOOL CheckORB();
	int  Login(CString& strSite, CString strID, CString strPW, CString& strValidationDate, CString& strCustomer,CString& strSpecialRole);
	BOOL GetChildSite(CString strCustomer, CString strSiteId, ChildSiteInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetChildSite(CString strCustomer, CString strSiteId, cciStringList& list, LPCTSTR szWhere = NULL);

	BOOL GetUser(SUserInfo& info, UserInfoList& list, bool bFilter=TRUE);
	BOOL AddUser(SUserInfo& info);
	BOOL DelUser(LPCTSTR szSiteID, LPCTSTR szUserID);
	BOOL SetUser(SUserInfo& info);
	CString UserType(int);

	BOOL GetSite(SSiteInfo& info, SiteInfoList& list, LPCTSTR szWhere = NULL, bool bFilter=TRUE);
	BOOL AddSite(CString strCustomer, SSiteInfo& info);
	BOOL DelSite(LPCTSTR szSiteID);
	BOOL SetSite(SSiteInfo& info);
	BOOL IsMySite(LPCTSTR szSiteID);

	BOOL GetHostForAll(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere = NULL, bool bFilter=TRUE);
	BOOL GetHostForList(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere = NULL, bool bFilter=TRUE);
	BOOL GetHostForCenter(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere = NULL, bool bFilter=TRUE);
	BOOL GetHostForProcess(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere = NULL, bool bFilter=TRUE);
	BOOL GetHostForSelect(cciCall* pCall, LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szWhere = NULL, bool bFilter=TRUE);
	BOOL GetHostData(cciCall* pCall, HostInfoList& list, int nMaxCount=0);

	BOOL GetRelayHostInfo(RelayHostInfoMap& pmap, ICallProgressHandler* pHandler=0); //skpark 2013.4.24 WOL 기능을 위한 정보 수집

	BOOL DelHost(LPCTSTR szSite, LPCTSTR szHost);
	BOOL SetHost(SHostInfo& info, LPCTSTR szUserID);
	BOOL SetHost(SHostInfo& info, LPCTSTR szUserID, CDirtyFlag* df);  //skpark 2012.12.21 추가
	int  UpdateHost(LPCTSTR szSite, LPCTSTR szKey, LPCTSTR szPasswd, LPCTSTR szHostId, LPCTSTR szCustomer);
	void ChangeSite(LPCTSTR szSite, CStringList& lsHost, bool bReboot);
	BOOL DummyCall(LPCTSTR szCall, LPCTSTR szEntity, LPCTSTR szWhere = NULL);
	BOOL SetWinPassword(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szUserID, LPCTSTR szPassword);	// 0001389: [RFP] 단말 OS 패스워드 변경 기능
	BOOL GetAllCategory(CStringArray& aCategoryList);
	BOOL SetCategory(SHostInfo& info, LPCTSTR szUserID, CString strCategory);
	BOOL SetPowerMng(SHostInfo& info, LPCTSTR szUserID);

	void GetHostRev(LPCTSTR szSite, HostRevInfoList& list, LPCTSTR szWhere = NULL);
	void GetPackageRev(LPCTSTR szSite, LPCTSTR szHost, int nSide, PackageRevInfoList& list);

	void GetPackage(LPCTSTR szSite, PackageInfoList& lsPackage, LPCTSTR szWhere = NULL);
	void ApplyPackage(std::string strUserId, std::string strSite, std::string strPackage, std::string strSTime, std::string strETime, std::list<std::string> lsHost);
	void SetPackage(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szProg, LPCTSTR szRevId, LPCTSTR szSTime, LPCTSTR szETime);
	BOOL RemovePackage(LPCTSTR szSite, LPCTSTR szPackage);
	void RemovePackageRev(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szRevId);
	BOOL CancelPackage(LPCTSTR szSite, LPCTSTR szHost, int nTargetDisplay, LPCTSTR szUserID); // 2010.02.17 by gwangsoo
	BOOL IsExistPackage(LPCTSTR szPackage, BOOL &bExist);	// 2010.08.06 by gwangsoo
	BOOL GetUnusedPackage(short nUnusedDays, CStringArray& astrPackageList);

	BOOL GetLMOSolution(LMOSolutionInfoList& list); //skpark lmo_solution
	BOOL SetLMOSolution(SLMOSolutionInfo& info); //skpark lmo_solution
	BOOL CreateLMOSolution(SLMOSolutionInfo& info); //skpark lmo_solution
	BOOL DelLMOSolution(SLMOSolutionInfo& info); //skpark lmo_solution

	void GetAnnounce(LPCTSTR szSite, AnnounceInfoList& list);
	bool DelAnnounce(LPCTSTR szSite, LPCTSTR szAnnoId);
	bool SetAnnounce(LPCTSTR szSite, LPCTSTR szAnnoId, SAnnounceInfo& info);
	bool CreAnnounce(LPCTSTR szSite, LPCTSTR szUserId, SAnnounceInfo& info);

	BOOL GetPackageStateLog(LPCTSTR szSite, PackageStateLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetPowerStateLog(LPCTSTR szSite, PowerStateLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetConnectionStateLog(LPCTSTR szSite, ConnectionStateLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetPowerStateLogByHost(LPCTSTR szHost, PowerStateLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetConnectionStateLogByHost(LPCTSTR szHost, ConnectionStateLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL _GetPowerStateLog(cciEntity& szEntity, PowerStateLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL _GetConnectionStateLog(cciEntity& szEntity, ConnectionStateLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetUserLog(LPCTSTR szSite, UserLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetBPLog(LPCTSTR szSite, BPLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetPlayLog(LPCTSTR szSite,LPCTSTR szHost, PlayLogInfoList& list, LPCTSTR szWhere = NULL);

	BOOL GetLogConfigInfo(SLogConfigInfo& info);
	BOOL SetLogConfigInfo(SLogConfigInfo& info);

	// skpark 2012.11.12  site가 추가/삭제되면 permition 을 refresh 해야 한다.
	int  RefreshPermitionSite(CString strCustomer);
	BOOL GetUserSiteList(ciString& siteList,ciString& hostList);

	//+ 장애 관리
	BOOL GetFault(LPCTSTR szSite, FaultInfoList& list, LPCTSTR szWhere = NULL);
	BOOL SetFault(SFaultInfo& info);
	BOOL GetFaultLog(LPCTSTR szSite, FaultLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetFaultLog(LPCTSTR szSite, LPCTSTR szHost, FaultLogInfoList& list, LPCTSTR szWhere = NULL);
	//- 장애 관리

	void ChangeOpStat(LPCTSTR szPm, LPCTSTR szSite, LPCTSTR szHost, bool bOpStat);

	BOOL SetBright(LPCTSTR szSite, LPCTSTR szHost,  LPCTSTR szObjectClass, LPCTSTR szObjectId,LPCTSTR szObjectType, ciLong brightness);
	BOOL DevicePowerOn(LPCTSTR szSite, LPCTSTR szHost,  LPCTSTR szObjectClass, LPCTSTR szObjectId,LPCTSTR szObjectType);
	BOOL DevicePowerOff(LPCTSTR szSite, LPCTSTR szHost,  LPCTSTR szObjectClass, LPCTSTR szObjectId,LPCTSTR szObjectType);
	BOOL MuteOn(LPCTSTR szSite, LPCTSTR szHost);
	BOOL MuteOff(LPCTSTR szSite, LPCTSTR szHost);
	BOOL SetVolume(LPCTSTR szSite, LPCTSTR szHost, ciUShort volume);

	BOOL PowerOn(LPCTSTR szSite, LPCTSTR szHost);
	BOOL Shutdown(LPCTSTR szSite, LPCTSTR szHost);
	BOOL Reboot(LPCTSTR szSite, LPCTSTR szHost);
	void Restart(LPCTSTR szSite, LPCTSTR szHost);
	BOOL AutoUpdate(LPCTSTR szSite, LPCTSTR szHost);
	BOOL AutoUpdateConfig(LPCTSTR szSite, LPCTSTR szHost, BOOL bIsAutoUpdateFlag, CString strAutoUpdateTime);
	BOOL MonitorPower(LPCTSTR szSite, LPCTSTR szHost, bool bOnOffValue);
	BOOL SetPowerMng(SNodeViewInfo* info, LPCTSTR szUserID);

	BOOL GetBroadcastingPlanList(LPCTSTR szSite, BroadcastingPlanInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetBroadcastingPlan(LPCTSTR szSite, LPCTSTR szBpId, SBroadcastingPlanInfo& info);
	BOOL DelBroadcastingPlan(LPCTSTR szSite, LPCTSTR szBpId);
	BOOL SetBroadcastingPlan(SBroadcastingPlanInfo& info);
	BOOL CreBroadcastingPlan(SBroadcastingPlanInfo& info);
	BOOL SetBroadcastingPlanZorder(LPCTSTR szSite, LPCTSTR szBpId, LPCTSTR szPrevZOrder, LPCTSTR szNextZOrder);
	BOOL SetBroadcastingPlanState(LPCTSTR szSite, LPCTSTR szBpId, short nState);

	BOOL GetTimePlan(LPCTSTR szSite, LPCTSTR szBpId, TimePlanInfoList& list);
	BOOL DelTimePlan(LPCTSTR szSite, LPCTSTR szBpId, LPCTSTR szTpId);
	BOOL SetTimePlan(STimePlanInfo& info);
	BOOL CreTimePlan(STimePlanInfo& info);

	BOOL GetTargetHost(LPCTSTR szSite, LPCTSTR szBpId, TargetHostInfoList& list);
	BOOL DelTargetHost(LPCTSTR szSite, LPCTSTR szBpId, LPCTSTR szTpId);
	BOOL SetTargetHost(STargetHostInfo& info);
	BOOL CreTargetHost(STargetHostInfo& info);

	BOOL IsExistBroadcastingPlan(LPCTSTR szSite, LPCTSTR szBpName);

	// 0000751: 콘텐츠 다운로드 상태 조회 기능
	BOOL GetDownloadState(LPCTSTR szSite, LPCTSTR szHost, DownloadStateInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetDownloadSummary(LPCTSTR szSite, LPCTSTR szHost, DownloadSummaryInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetApplyInfoList(CString strHostId, ApplyLogInfoList& list, LPCTSTR szWhere = NULL);
	BOOL GetScheduleStateInfoList(CString strHostId, ScheduleStateLogInfoList& list, LPCTSTR szWhere = NULL);

	// skpark
	BOOL GetCodeCategory(CodeItemList& list, CString strCustomer); //skpark code
	BOOL GetCodeItem(CodeItemList& list, CString strCustomer, CString category); //skpark code
	BOOL SetCodeItem(SCodeItem& info, CString strCustomer); //skpark code
	BOOL CreateCodeItem(SCodeItem& info, CString strCustomer); //skpark code
	BOOL DelCodeItem(SCodeItem& info, CString strCustomer); //skpark code

	BOOL SetCustomer(SCustomerInfo& info); //skpark code
	BOOL CreateCustomer(SCustomerInfo& info); //skpark code
	BOOL DelCustomer(SCustomerInfo& info); //skpark code

	BOOL	HasOption(CString option); //skpark ciArgParser::isSet


	BOOL GetCodeList(CodeItemList& list, CString strCustomer);
	BOOL GetMenuAuthList(CString strUserId, CString strCustomer, MenuAuthInfoList& list);
	BOOL GetCustomerList(CustomerInfoList& list);
	BOOL GetRoleList(RoleInfoList& list);

	BOOL RequestAlarmSummary(); // 알람이벤트를 강제로 발생하도록 한다

	BOOL GetScreenShotInfo(CString strWhere, ScreenShotInfo& ssInfo);

	// 단말청소
	BOOL CleanContents1(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId);								// 유효기간이 지난 패키지 삭제
	BOOL CleanContents2(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId, CStringArray& aExcludeList);	// 다음 패키지를 제외한 나머지 패키지 모두 삭제
	BOOL CleanContents3(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId, CStringArray& aIncludeList);	// 다음 패키지를  모두 삭제
	BOOL CleanContents4(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId, CStringArray& aFilepathList);	// 다음 컨텐츠/폴더를  모두 삭제
	BOOL CleanAllContents(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szGuid, LPCTSTR szUserId);							// 모든 컨텐츠 삭제

	// Plano
	BOOL GetPlanoList(LPCTSTR planoId, LPCTSTR where, PlanoInfoList& aPlanoList, bool bUpdate=false);
	BOOL CreatePlano(SPlanoInfo* info);
	BOOL DeletePlano(SPlanoInfo* info);
	BOOL SetPlano(SPlanoInfo* info);
	int GetNodeList(CString& mgrId, CString& siteId, CString& planoId, CString& nodeId, int oper, int admin, NodeViewInfoList& nodeInfoList);
	bool GetNodeObjectInfo(SNodeViewInfo* nodeInfo);
	int CreateNode(SNodeViewInfo* nodeInfo);
	int DeleteNode(SNodeViewInfo* nodeInfo);
	int SetNode(SNodeViewInfo* nodeInfo);
	BOOL GetLightList(LPCTSTR szSite, LPCTSTR szLight,LPCTSTR szWhere, bool bFilter, bool bSmallColumn, LightInfoList& list);
	BOOL SetLight(SLightInfo& info, LPCTSTR szUserID);
	BOOL CreateLight(SLightInfo& info, LPCTSTR szUserID);
	BOOL DelLight(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szLight);
	BOOL GetMonitorList(LPCTSTR szSite, LPCTSTR szLight,LPCTSTR szWhere, bool bFilter, bool bSmallColumn, MonitorInfoList& list);
	BOOL SetMonitor(SMonitorInfo& info, LPCTSTR szUserID);
	BOOL CreateMonitor(SMonitorInfo& info);
	BOOL DelMonitor(LPCTSTR szSite, LPCTSTR szHost, LPCTSTR szLight);
	BOOL GetIconList(LPCTSTR szNodeId,LPCTSTR szWhere, NodeTypeInfoList& list);
	BOOL SetIcon(SNodeTypeInfo& info);
	BOOL CreateIcon(SNodeTypeInfo& info);
	BOOL DelIcon(LPCTSTR szNodeId);
	BOOL IsExist(LPCTSTR szEntity, BOOL &bExist);

	// 자기 IP
	const char* GetMyIp();

	//Server version
	const char* GetVersion() { return m_strVersion; }

	static void Trace(const char* szPath, int nLine, const char* szMsg);

	typedef map<ciString,SHostInfo*>	_HostInfoMap;			//key=entity

	BOOL	GetTimeLine(HostInfoList& pHostInfoList,int period=5,int side=1);
	BOOL	GetTimeLine(SHostInfo& info,int period=5,int side=1);
	BOOL	GetTimeLine(SHostInfo& hostInfo, CList<CString>& timeLineList,int period=5,int side=1);

	BOOL	_GetTimeLine(const char* mgrId,const char* hostId,CCI::CCI_StringList& timeLineList ,int period,int side);
	BOOL	_GetTimeLineNextSchedule(const char* mgrId,CCI::CCI_StringList* pHostList, _HostInfoMap* pHostInfoMap ,int period,int side);

	int GetOperationalStates(CMapStringToString& hostMap,  CMapStringToString& outMap);
	BOOL _GetOperationalStates(CMapStringToString& hostMap);

	int FindRecursiveChildren(LPCSTR parentSite,CStringList& children, const char* additionalWhereClause="");
	int FindChildren(LPCSTR parentSite, CStringList& children, const char* additionalWhereClause="");


private:
	CCopModule();
	~CCopModule();

	void InitModule();
	void ReleaseModule();

	bool AddItem(cciCall& cal, const char* szKey, const char* szVal = NULL);
	bool AddItem(cciCall& cal, const char* szKey, const long lVal);
	bool AddItem(cciCall& cal, const char* szKey, CTime& tVal);
	bool AddItem(cciCall& cal, const char* szKey, CStringList& lsVal);
	bool AddItem(cciCall& cal, const char* szKey, std::list<std::string>& lsVal);

	BOOL _ApproveErrorCheck(cciCall& call);

public:
private:
	BOOL m_bIsInitORB;
	cciGUIORBThread* m_pOrbThread;
	static CCopModule* m_pModule;

	CString m_strLoginId;
	CString m_strVersion;
	CString m_strSiteId;
	int m_Authority;

public:
	BOOL	GetProgramIdListIncludeContents(LPCTSTR lpszContentsId, CStringArray& listProgramId);
	BOOL	GetContents(LPCTSTR szSite, CONTENTS_INFO_EX_LIST& lsContents, LPCTSTR szWhere = NULL);

	BOOL	ChangeParent(LPCTSTR szSiteId, LPCTSTR szNewParentId);
	BOOL	SetProperty(std::string strSiteId,std::string strHostId,std::string strName,std::string strValue);
	BOOL	GetProperty(std::string strSiteId,std::string strHostId,std::string strName);
	BOOL	PingTest(std::string strSiteId,std::string strHostId);
	BOOL	DownloadOnlyProperty(std::string strSiteId,std::string strHostId,int downloadOnly);

	//
	BOOL	CreateHost(LPCSTR lpszIniPath);
	BOOL	CreateHost(LPCSTR mgrId, LPCSTR siteId, LPCSTR hostId);
	BOOL	RemoveHost(LPCSTR lpszIniPath);

	BOOL	GetCanInfoList(CString& strClassName, CString& strName, CString& strSite, CString& strUserId,CString& szWhere , TrashCanInfoList& list);
	BOOL	CanRecovery(CString& strClassName, CString& strSite, CString& strId);
	BOOL	CanEliminate(CString& strClassName, CString& strSite, CString& strId);

protected:
	BOOL	createHost(LPCSTR mgrId, LPCSTR siteId, LPCSTR hostId, LPCSTR hostName, 
						  LPCSTR macAddress, LPCSTR edition, ciTime& authDate, LPCSTR enterprise, 
						  ciString& errMsg, ciLong& errCode );
	BOOL	removeHost(LPCSTR hostId, ciString& errMsg, ciLong& errCode);


	// <-- seventhstone 2015.05.13 추가
	// 승인(Approve) 관련
public:
	BOOL	getApproveInfoList(LPCSTR lpszWhere, APPROVE_INFO_LIST& ApproveInfoList);
	BOOL	setApprove(LPCSTR lpszMgrId, LPCSTR lpszApproveId, APPROVE_INFO::APPROVE_STATUS eNewApproveState=APPROVE_INFO::eStatusAccepted);
	BOOL	removeApprove(LPCSTR lpszMgrId, LPCSTR lpszApproveId);
	// -->
};
