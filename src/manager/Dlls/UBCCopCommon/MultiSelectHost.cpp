// MultiSelectHost.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "copmodule.h"
#include "UBCCopCommon.h"
#include "MultiSelectHost.h"

#include "ContentsSelectDlg.h"
#include "PackageSelectDlg.h"
#include "SiteSelectDlg.h"

#include "common\UbcCode.h"
#include "common\TraceLog.h"

// CMultiSelectHost 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMultiSelectHost, CDialog)

CMultiSelectHost::CMultiSelectHost(LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CMultiSelectHost::IDD, pParent)
	, m_strCustomer ( szCustomer )
{

}

CMultiSelectHost::~CMultiSelectHost()
{
}

void CMultiSelectHost::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FILTER_HOSTNAME, m_editFilterHostName);
	DDX_Control(pDX, IDC_EDIT_FILTER_HOST, m_editFilterHost);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOSTTYPE, m_cbxHostType);
	DDX_Control(pDX, IDC_CB_SITE, m_cbxSite);
	DDX_Control(pDX, IDC_CB_INCLUDECONTENTS, m_cbxIncludeContents);
	DDX_Control(pDX, IDC_CB_PLAYING_PACKAGE, m_cbxPlayingPackage);
	DDX_Control(pDX, IDC_CB_ONOFF, m_cbOnOff);
	DDX_Control(pDX, IDC_CB_OPERATION, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_HOST_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcHostList);
	DDX_Control(pDX, IDC_LIST_SEL_HOST, m_lcSelHostList);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
	DDX_Control(pDX, IDC_COMBO_FILTER_TAG, m_cbFilterTag);
	DDX_Control(pDX, IDC_COMBO_FILTER_CACHE, m_cbFilterCache);
}

BEGIN_MESSAGE_MAP(CMultiSelectHost, CDialog)
	ON_BN_CLICKED(IDOK, &CMultiSelectHost::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMultiSelectHost::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_ADD, &CMultiSelectHost::OnBnClickedBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, &CMultiSelectHost::OnBnClickedBnDel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOST, &CMultiSelectHost::OnNMDblclkListHost)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SEL_HOST, &CMultiSelectHost::OnNMDblclkListSelHost)
	ON_BN_CLICKED(IDC_BUTTON_HOST_REFRESH, &CMultiSelectHost::OnBnClickedButtonHostRefresh)
	ON_CBN_SELCHANGE(IDC_CB_SITE, &CMultiSelectHost::OnCbnSelchangeCbSite)
	ON_CBN_SELCHANGE(IDC_CB_INCLUDECONTENTS, &CMultiSelectHost::OnCbnSelchangeCbIncludecontents)
	ON_CBN_SELCHANGE(IDC_CB_PLAYING_PACKAGE, &CMultiSelectHost::OnCbnSelchangeCbPlayingPackage)
	ON_EN_CHANGE(IDC_EDIT_FILTER_DESCRIPTION, &CMultiSelectHost::OnEnChangeEditFilterDescription)
END_MESSAGE_MAP()


// CMultiSelectHost 메시지 처리기입니다.

BOOL CMultiSelectHost::OnInitDialog()
{
	CWaitMessageBox wait;

	CDialog::OnInitDialog();

	if(!m_title.IsEmpty()){
		this->SetWindowText(m_title);
	}

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnAdd    .LoadBitmap(IDB_BUTTON_NEXT2  , RGB(255,255,255));
	m_bnDel    .LoadBitmap(IDB_BUTTON_PREV2  , RGB(255,255,255));
	m_bnOK     .LoadBitmap(IDB_BUTTON_SELECT , RGB(255,255,255));
	m_bnCancel .LoadBitmap(IDB_BUTTON_CANCEL , RGB(255,255,255));

	m_bnRefresh.SetToolTipText(LoadStringById(IDS_MULTISELECTHOST_BTN001));

	CodeItemList listHostType;
	CUbcCode::GetInstance(m_strCustomer)->GetCategoryInfo(_T("HostType"), listHostType);

	POSITION pos = listHostType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listHostType.GetNext(pos);
		m_cbxHostType.AddString(info.strEnumString, info.nEnumNumber);
	}

	m_cbOnOff.AddString(LoadStringById(IDS_MULTISELECTHOST_STR005));
	m_cbOnOff.AddString(LoadStringById(IDS_MULTISELECTHOST_STR001));
	m_cbOnOff.AddString(LoadStringById(IDS_MULTISELECTHOST_STR002));
	m_cbOnOff.SetCurSel(0);

	m_cbOperation.AddString(LoadStringById(IDS_MULTISELECTHOST_STR005));
	m_cbOperation.AddString(LoadStringById(IDS_MULTISELECTHOST_STR003));
	m_cbOperation.AddString(LoadStringById(IDS_MULTISELECTHOST_STR004));
	m_cbOperation.SetCurSel(0);

	CStringArray aTagList;
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_cbFilterCache.AddString(LoadStringById(IDS_MULTISELECTHOST_STR005));
	m_cbFilterCache.AddString(LoadStringById(IDS_MULTISELECTHOST_STR006));
	m_cbFilterCache.AddString(LoadStringById(IDS_MULTISELECTHOST_STR007));
	m_cbFilterCache.SetCurSel(0);

	char buf[1024];
//	CString szPath;
//	szPath.Format("%s%sdata\\UBCManager.ini", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	GetPrivateProfileString("HOST-FILTER", "HostName", "", buf, 1024, m_strIniPath);
	m_editFilterHostName.SetWindowText(buf);
	GetPrivateProfileString("HOST-FILTER", "HostID", "", buf, 1024, m_strIniPath);
	m_editFilterHost.SetWindowText(buf);

	GetPrivateProfileString("HOST-FILTER", "HostType", "0", buf, 1024, m_strIniPath);
	CString strHostType = buf;
	if(strHostType.IsEmpty())
	{
		m_cbxHostType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strHostType.Tokenize(",", pos)) != _T(""))
		{
			m_cbxHostType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}

	GetPrivateProfileString("HOST-FILTER", "SiteName", "", buf, 1024, m_strIniPath);
	CString strSiteName = buf;
	m_cbxSite.AddString(LoadStringById(IDS_COPCOMMON_STR001));
	m_cbxSite.AddString(LoadStringById(IDS_COPCOMMON_STR002));
	m_cbxSite.SetCurSel(0);
	//if(CCopModule::eSiteAdmin != m_nAuthority)
	if(CCopModule::eSiteUser == m_nAuthority) // skpark 2012.11.1 siteUser 만 못고친다.
		                                      // siteManager 는 하위 사이트선택이 가능해야 한다.
	{
		m_cbxSite.InsertString(1, m_strSiteId);
		m_cbxSite.SetCurSel(1);
		m_cbxSite.EnableWindow(FALSE);

		m_strSite = m_strSiteId;
	}
	else
	{
		GetPrivateProfileString("HOST-FILTER", "SiteID", "", buf, 1024, m_strIniPath);
		m_strSite = buf;

		if(m_strSite.GetLength() > 0)
		{
			m_cbxSite.InsertString(1, strSiteName);
			m_cbxSite.SetCurSel(1);
		}
	}

	GetPrivateProfileString("HOST-FILTER", "IncludeContents", "", buf, 1024, m_strIniPath);
	m_cbxIncludeContents.AddString(LoadStringById(IDS_COPCOMMON_STR001));
	m_cbxIncludeContents.AddString(LoadStringById(IDS_COPCOMMON_STR002));
	m_cbxIncludeContents.SetCurSel(0);
	if(buf[0] != 0)
	{
		m_cbxIncludeContents.InsertString(1, buf);
		m_cbxIncludeContents.SetCurSel(1);
	}

	GetPrivateProfileString("HOST-FILTER", "IncludeContentsID", "", buf, 1024, m_strIniPath);
	m_strIncludeContentsID = buf;

	GetPrivateProfileString("HOST-FILTER", "PlayingPackage", "", buf, 1024, m_strIniPath);
	m_cbxPlayingPackage.AddString(LoadStringById(IDS_COPCOMMON_STR001));
	m_cbxPlayingPackage.AddString(LoadStringById(IDS_COPCOMMON_STR002));
	m_cbxPlayingPackage.SetCurSel(0);
	if(buf[0] != 0)
	{
		m_cbxPlayingPackage.InsertString(1, buf);
		m_cbxPlayingPackage.SetCurSel(1);
	}

	GetPrivateProfileString("HOST-FILTER", "PlayingPackageID", "", buf, 1024, m_strIniPath);
	m_strPlayingPackageID = buf;

	GetPrivateProfileString("HOST-FILTER", "AdminStat", "0", buf, 1024, m_strIniPath);
	m_cbOnOff.SetCurSel(atoi(buf));
	GetPrivateProfileString("HOST-FILTER", "OperaStat", "0", buf, 1024, m_strIniPath);
	m_cbOperation.SetCurSel(atoi(buf));
	GetPrivateProfileString("HOST-FILTER", "Category", "", buf, 1024, m_strIniPath);
	m_cbFilterTag.SetWindowText(buf);

	InitHostList();
	RefreshHostList();

	InitSelHostList();

	RefreshSelHostList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMultiSelectHost::OnBnClickedOk()
{
	m_astrSelHostList.RemoveAll();
	m_astrSelHostNameList.RemoveAll();
	m_astrSelSiteIdList.RemoveAll();

	for(int i=0; i<m_lcSelHostList.GetItemCount() ;i++)
	{
		m_astrSelHostList.Add(m_lcSelHostList.GetItemText(i, eHostID));
		m_astrSelHostNameList.Add(m_lcSelHostList.GetItemText(i, eHostName));
		m_astrSelSiteIdList.Add(m_lcSelHostList.GetItemText(i, eGroup));
	}
	TraceLog(("%d row selected", m_astrSelHostNameList.GetSize()));

	OnOK();
}

void CMultiSelectHost::OnBnClickedCancel()
{
	OnCancel();
}

void CMultiSelectHost::InitHostList()
{
	m_lcHostList.SetExtendedStyle(m_lcHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck ] = _T("");
	m_szColum[eHostName] = LoadStringById(IDS_MULTISELECTHOST_LIST001);
	m_szColum[eHostID] = LoadStringById(IDS_MULTISELECTHOST_LIST003);
	m_szColum[eGroup ] = LoadStringById(IDS_MULTISELECTHOST_LIST002);

	m_lcHostList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcHostList.InsertColumn(eHostName , m_szColum[eHostName ], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eHostID , m_szColum[eHostID ], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eGroup  , m_szColum[eGroup  ], LVCFMT_LEFT  ,  85);

	// 제일 마지막에 할것...
	m_lcHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcHostList.SetSortEnable(false);
//	m_lcHostList.SetFixedCol(eCheck);
}

void CMultiSelectHost::RefreshHostList()
{
	CWaitMessageBox wait;

	CStringArray filter_list;
	CString strTmp;

	if(!m_exWhere.IsEmpty()){
		filter_list.Add(m_exWhere);
	}

	//
	CString strHostName;
	m_editFilterHostName.GetWindowText(strHostName);
	if(!strHostName.IsEmpty())
	{
		strTmp.Format(_T("hostName like '%%%s%%'"), strHostName);
		filter_list.Add(strTmp);
	}

	//
	CString strHost;
	m_editFilterHost.GetWindowText(strHost);
	if(!strHost.IsEmpty())
	{
		strTmp.Format(_T("hostId like '%%%s%%'"), strHost);
		filter_list.Add(strTmp);
	}

	//
	CString strHostType = m_cbxHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	if(!strHostType.IsEmpty())
	{
		strTmp.Format(_T("hostType in ('%s')")
					, strHostType
					);
		strTmp.Replace(",", "','");
		filter_list.Add(strTmp);
	}

	//
	CString strSiteName;
	m_cbxSite.GetWindowText(strSiteName);

	bool has_where_siteId_alreay = false;

	if(m_strSite.IsEmpty())
	{
		//if(CCopModule::eSiteAdmin != m_nAuthority)
		if(CCopModule::eSiteUser == m_nAuthority)
		{
			strTmp.Format(_T("siteId like '%%%s%%'"), m_strSiteId);
			has_where_siteId_alreay = true;
			filter_list.Add(strTmp);
		}
	}
	else
	{
		// skpark 2013.4.23 multiselect 가 가능하도록
		//strTmp.Format(_T("siteId like '%%%s%%'"), m_strSite);
		//filter_list.Add(strTmp);
		CString strSiteId = m_strSite;
		if(!strSiteId.IsEmpty() && strSiteId != "*" ){
			strSiteId.Replace(_T(","), _T("','"));
			strTmp.Format(_T("siteId in ('%s')"), strSiteId);
			has_where_siteId_alreay = true;
			filter_list.Add(strTmp);
		}
	}

	//
	CString strIncludeContents = "";
	if(m_cbxIncludeContents.GetCurSel() > 0) m_cbxIncludeContents.GetWindowText(strIncludeContents);
	if(!m_strIncludeContentsID.IsEmpty())
	{
		CStringArray program_id_list;
		CCopModule::GetObject()->GetProgramIdListIncludeContents(m_strIncludeContentsID, program_id_list);

		strTmp = "";
		int count = program_id_list.GetCount();
		for(int i=0; i<count; i++)
		{
			if(strTmp.GetLength() > 0) strTmp += ",";

			CString tmp;
			tmp.Format("'%s'", program_id_list.GetAt(i));

			strTmp += tmp;
		}
		if(strTmp.GetLength() > 0)
		{
			strTmp.Insert(0, "lastSchedule1 in (");
			strTmp += ")";
		}
		filter_list.Add(strTmp);
	}

	//
	CString strPlayingPackage = "";
	if(m_cbxPlayingPackage.GetCurSel() > 0) m_cbxPlayingPackage.GetWindowText(strPlayingPackage);
	if(!m_strPlayingPackageID.IsEmpty())
	{
		strTmp.Format(_T("lastSchedule1 = '%s'"), m_strPlayingPackageID);
		filter_list.Add(strTmp);
	}

	//
	if(m_cbOnOff.GetCurSel())
	{
		strTmp.Format(_T("adminState = %d"), (m_cbOnOff.GetCurSel()==1)? 1:0);
		filter_list.Add(strTmp);
	}

	if(m_cbOperation.GetCurSel())
	{
		strTmp.Format(_T("operationalState = %d"), (m_cbOperation.GetCurSel()==1)? 1:0);
		filter_list.Add(strTmp);
	}

	if(m_cbFilterCache.GetCurSel()==1) // localhost
	{
		strTmp=_T("domainName = 'localhost'");
		filter_list.Add(strTmp);
	}else if(m_cbFilterCache.GetCurSel()==2) {
		strTmp=_T("(domainName != 'localhost' or domainName is null)");
		filter_list.Add(strTmp);
	}

	//
	CString strDescription;
	m_cbFilterTag.GetWindowText(strDescription);
	if(!strDescription.IsEmpty())
	{
		strTmp.Format(_T("category like '%%%s%%'"), strDescription);
		filter_list.Add(strTmp);
	}

	//
	CString strWhere = "";
	int count = filter_list.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str = filter_list.GetAt(i);

		if(strWhere.GetLength() > 0)
		{
			strWhere += " and ";
		}

		strWhere += "( ";
		strWhere += str;
		strWhere += ") ";
	}

#ifdef _DEBUG
//	UbcMessageBox(strWhere);
#endif

	WritePrivateProfileString("HOST-FILTER", "HostName", strHostName, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "HostID", strHost, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "HostType", strHostType, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "SiteName", strSiteName, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "SiteID", m_strSite, m_strIniPath);

	WritePrivateProfileString("HOST-FILTER", "IncludeContents", strIncludeContents, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContentsID", m_strIncludeContentsID, m_strIniPath);

	WritePrivateProfileString("HOST-FILTER", "PlayingPackage", strPlayingPackage, m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackageID", m_strPlayingPackageID, m_strIniPath);

	WritePrivateProfileString("HOST-FILTER", "AdminStat", ToString(m_cbOnOff.GetCurSel()), m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "OperaStat", ToString(m_cbOperation.GetCurSel()), m_strIniPath);
	WritePrivateProfileString("HOST-FILTER", "Category", strDescription, m_strIniPath);

	m_lsInfoList.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetHostForSelect(&aCall
														, ((CCopModule::eSiteAdmin == m_nAuthority || has_where_siteId_alreay == true) ? _T("*") : m_strSiteId)
														, _T("*")
														, strWhere
														);

	if(bRet)
	{
		CCopModule::GetObject()->GetHostData(&aCall
											, m_lsInfoList
											, 0
											);
	}

	m_lcHostList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsInfoList.GetNext(pos);

		m_lcHostList.InsertItem(nRow, "");
		m_lcHostList.SetItemText(nRow, eHostName, info.hostName);
		m_lcHostList.SetItemText(nRow, eHostID, info.hostId);
		m_lcHostList.SetItemText(nRow, eGroup , info.siteId);
		m_lcHostList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	
}

void CMultiSelectHost::OnBnClickedButtonHostRefresh()
{
	RefreshHostList();
}

void CMultiSelectHost::InitSelHostList()
{
	m_lcSelHostList.SetExtendedStyle(m_lcSelHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcSelHostList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcSelHostList.InsertColumn(eHostName , m_szColum[eHostName ], LVCFMT_LEFT  , 100);
	m_lcSelHostList.InsertColumn(eHostID , m_szColum[eHostID ], LVCFMT_LEFT  , 100);
	m_lcSelHostList.InsertColumn(eGroup , m_szColum[eGroup ], LVCFMT_LEFT  , 100);

	// 제일 마지막에 할것...
	m_lcSelHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcSelHostList.SetSortEnable(false);
//	m_lcSelHostList.SetFixedCol(eCheck);
}

void CMultiSelectHost::RefreshSelHostList()
{
	m_lcSelHostList.DeleteAllItems();

	for(int i=0; i<m_astrSelHostList.GetCount() ;i++)
	{
		m_lcSelHostList.InsertItem(i, "");
		m_lcSelHostList.SetItemText(i, eHostID, m_astrSelHostList[i]);
	}
	for(int i=0; i<m_astrSelHostNameList.GetCount() ;i++)
	{
		//m_lcSelHostList.InsertItem(i, "");
		m_lcSelHostList.SetItemText(i, eHostName, m_astrSelHostNameList[i]);
	}
	for(int i=0; i<m_astrSelSiteIdList.GetCount() ;i++)
	{
		//m_lcSelHostList.InsertItem(i, "");
		m_lcSelHostList.SetItemText(i, eGroup, m_astrSelSiteIdList[i]);
	}
}

void CMultiSelectHost::OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	AddSelHost(pNMLV->iItem);
}

void CMultiSelectHost::OnNMDblclkListSelHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	DelSelHost(pNMLV->iItem);
}

void CMultiSelectHost::OnBnClickedBnAdd()
{
	bool bChecked = false;
	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(m_lcHostList.GetCheck(i))
		{
			bChecked = true;
			break;
		}
	}

	if(!bChecked)
	{
		UbcMessageBox(LoadStringById(IDS_MULTISELECTHOST_MSG001));
		return;
	}

	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(!m_lcHostList.GetCheck(i)) continue;

		AddSelHost(i);

		m_lcHostList.SetCheck(i, FALSE);
	}

	m_lcHostList.SetCheckHdrState(FALSE);
}

void CMultiSelectHost::OnBnClickedBnDel()
{
	for(int i=m_lcSelHostList.GetItemCount()-1; i>=0 ;i--)
	{
		if(!m_lcSelHostList.GetCheck(i)) continue;
		DelSelHost(i);
	}

	m_lcSelHostList.SetCheckHdrState(FALSE);
}

void CMultiSelectHost::AddSelHost(int nHostIndex)
{
	if(nHostIndex < 0 || nHostIndex >= m_lcHostList.GetItemCount()) return;

	CString strHostId = m_lcHostList.GetItemText(nHostIndex, eHostID);
	CString strHostName = m_lcHostList.GetItemText(nHostIndex, eHostName);
	CString strSiteId = m_lcHostList.GetItemText(nHostIndex, eGroup);

	// 이미 추가된 단말인지 체크
	for(int i=0; i<m_lcSelHostList.GetItemCount() ;i++)
	{
		CString strSelHostId = m_lcSelHostList.GetItemText(i, eHostID);
		if(strSelHostId == strHostId) return;
	}

	int nRow = m_lcSelHostList.GetItemCount();
	m_lcSelHostList.InsertItem(nRow, "");
	m_lcSelHostList.SetItemText(nRow, eHostID, strHostId);
	m_lcSelHostList.SetItemText(nRow, eHostName, strHostName);
	m_lcSelHostList.SetItemText(nRow, eGroup, strSiteId);
}

void CMultiSelectHost::DelSelHost(int nSelHostIndex)
{
	if(nSelHostIndex < 0 || nSelHostIndex >= m_lcSelHostList.GetItemCount()) return;

	m_lcSelHostList.DeleteItem(nSelHostIndex);
}

BOOL CMultiSelectHost::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_SITEID)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOST  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_ONOFF          )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_OPERATION      )->GetSafeHwnd() )
		{
			OnBnClickedButtonHostRefresh();
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CMultiSelectHost::OnCbnSelchangeCbSite()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbxSite.GetCurSel();
	int count = m_cbxSite.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxSite.DeleteString(1);
			m_strSite = "";
		}
	}
	else if(idx == count-1 )
	{
		CSiteSelectDlg dlg(m_strCustomer);
		//skpark 2013.4.23 multiselect 수정
		//dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_strSite = "";
				m_cbxSite.DeleteString(1);
				m_cbxSite.SetCurSel(0);
			}
			//skpark 2013.4.23 multiselect start[
			CString strSiteName,strSiteId;
			SiteInfoList siteInfoList;
			if(dlg.GetSelectSiteInfoList(siteInfoList))
			{
				POSITION pos = siteInfoList.GetHeadPosition();
				while(pos)
				{
					SSiteInfo info = siteInfoList.GetNext(pos);
					strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
					strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
				}
			}
			if(!strSiteName.IsEmpty()){
				m_cbxSite.InsertString(1, strSiteName);
				m_cbxSite.SetCurSel(1);
				m_strSite = strSiteId;
			}else{
				m_cbxSite.SetCurSel(0);
				m_strSite = "";
			
			}

			//SSiteInfo info;
			//dlg.GetSelectSiteInfo(info);

			//if(info.siteName.GetLength() > 0)
			//{
			//	m_cbxSite.InsertString(1, info.siteName);
			//	m_cbxSite.SetCurSel(1);
			//	m_strSite = info.siteId;
			//}
			//else
			//{
			//	m_cbxSite.SetCurSel(0);
			//	m_strSite = "";
			//}
			//skpark 2013.4.23 multiselect end]

		}
		else
		{
			m_cbxSite.SetCurSel(count-2);
		}
	}else{
		// skpark 2013.4.23 multiselect 
		// 콤보박스의 글자가 14자가 넘어가면 팝업으로 보여준다.
		CString strSiteName;
		m_cbxSite.GetWindowText(strSiteName);
		if(strSiteName.GetLength() > 14){
			CString msgStr = "Selected Group is\n";
			msgStr.Append(strSiteName);
			UbcMessageBox(msgStr);
		}
	}
}

void CMultiSelectHost::OnCbnSelchangeCbIncludecontents()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbxIncludeContents.GetCurSel();
	int count = m_cbxIncludeContents.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxIncludeContents.DeleteString(1);
			m_strIncludeContentsID = "";
		}
	}
	else if(idx == count-1 )
	{
		CContentsSelectDlg dlg(m_strCustomer);
		dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_strIncludeContentsID = "";
				m_cbxIncludeContents.DeleteString(1);
				m_cbxIncludeContents.SetCurSel(0);
			}

			CONTENTS_INFO_EX_LIST& info_list = dlg.GetSelectedContentsIdList();

			if(info_list.GetCount() > 0)
			{
				POSITION pos = info_list.GetHeadPosition();
				CONTENTS_INFO_EX& info = info_list.GetNext(pos);

				m_cbxIncludeContents.InsertString(1, info.strContentsName);
				m_cbxIncludeContents.SetCurSel(1);
				m_strIncludeContentsID = info.strContentsId;
			}
			else
			{
				m_cbxIncludeContents.SetCurSel(0);
				m_strIncludeContentsID = "";
			}
		}
		else
		{
			m_cbxIncludeContents.SetCurSel(count-2);
		}
	}
}

void CMultiSelectHost::OnCbnSelchangeCbPlayingPackage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbxPlayingPackage.GetCurSel();
	int count = m_cbxPlayingPackage.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxPlayingPackage.DeleteString(1);
			m_strPlayingPackageID = "";
		}
	}
	else if(idx == count-1 )
	{
		CPackageSelectDlg dlg(m_strCustomer);
		dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_strPlayingPackageID = "";
				m_cbxPlayingPackage.DeleteString(1);
				m_cbxPlayingPackage.SetCurSel(0);
			}

			PackageInfoList& info_list = dlg.GetSelectedPackageList();

			if(info_list.GetCount() > 0)
			{
				POSITION pos = info_list.GetHeadPosition();
				SPackageInfo& info = info_list.GetNext(pos);

				m_cbxPlayingPackage.InsertString(1, info.szPackage);
				m_cbxPlayingPackage.SetCurSel(1);
				m_strPlayingPackageID = info.szPackage;
			}
			else
			{
				m_cbxPlayingPackage.SetCurSel(0);
				m_strPlayingPackageID = "";
			}
		}
		else
		{
			m_cbxPlayingPackage.SetCurSel(count-2);
		}
	}
}

void CMultiSelectHost::OnEnChangeEditFilterDescription()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
