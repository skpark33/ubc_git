// SiteInfoDlg.cpp : implementation file
//
#include "stdafx.h"
#include "resource.h"
#include "SiteInfoDlg.h"
#include "common\TraceLog.h"
#include "common\dllcommon.h"
#include "common\PreventChar.h"
#include "common\CharType.h"
#include "common\UbcCode.h"


// CSiteInfoDlg dialog
IMPLEMENT_DYNAMIC(CSiteInfoDlg, CDialog)

CSiteInfoDlg::CSiteInfoDlg(CString& strCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CSiteInfoDlg::IDD, pParent)
{
	m_strCustomer = strCustomer;
}

CSiteInfoDlg::~CSiteInfoDlg()
{
}

void CSiteInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_BUSINESSTYPE, m_combo_businessType);
	DDX_Control(pDX, IDC_COMBO_BUSINESSCODE, m_combo_businessCode);
}

BEGIN_MESSAGE_MAP(CSiteInfoDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CSiteInfoDlg::OnBnClickedOk)
END_MESSAGE_MAP()

// CSiteInfoDlg message handlers
BOOL CSiteInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if(!m_SiteInfo.siteId.IsEmpty())
	{
		((CEdit*)GetDlgItem(IDC_EB_SITE_MGRID))->SetReadOnly();
		((CEdit*)GetDlgItem(IDC_EB_SITE_ID))->SetReadOnly();
	}

	SetDlgItemText(IDC_EB_SITE_PARENTID  , m_SiteInfo.parentId);
	SetDlgItemText(IDC_EB_SITE_MGRID  , m_SiteInfo.mgrId);
	SetDlgItemText(IDC_EB_SITE_ID     , m_SiteInfo.siteId);
	SetDlgItemText(IDC_EB_SITE_NAME   , m_SiteInfo.siteName);
	SetDlgItemText(IDC_EB_SITE_TEL    , m_SiteInfo.phoneNo1);
	SetDlgItemText(IDC_EB_SITE_ADDR   , m_SiteInfo.comment1);
	SetDlgItemText(IDC_EB_SITE_MAN    , m_SiteInfo.ceoName);
	SetDlgItemText(IDC_EB_SITE_MAN_TEL, m_SiteInfo.phoneNo2);
	SetDlgItemText(IDC_EB_REMARK      , m_SiteInfo.comment2);

	CUbcCode::GetInstance(m_strCustomer)->FillComboBoxCtrl(m_combo_businessType, _T("businessType"));
	m_combo_businessType.SetCurSel(CUbcCode::GetInstance()->FindComboBoxCtrl(m_combo_businessType, ::atoi(m_SiteInfo.businessType)));
	CUbcCode::GetInstance(m_strCustomer)->FillComboBoxCtrl(m_combo_businessCode, _T("businessCode"));
	m_combo_businessCode.SetCurSel(CUbcCode::GetInstance()->FindComboBoxCtrl(m_combo_businessCode, ::atoi(m_SiteInfo.businessCode)));

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSiteInfoDlg::SetSite(const SSiteInfo& info)
{
	m_SiteInfo = info;
}
void CSiteInfoDlg::SetParentSite(const char* parentId)
{
	m_SiteInfo.parentId = parentId;

}

SSiteInfo& CSiteInfoDlg::GetSite()
{
	return m_SiteInfo;
}

void CSiteInfoDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	GetDlgItemText(IDC_EB_SITE_MGRID  , m_SiteInfo.mgrId);
	GetDlgItemText(IDC_EB_SITE_ID     , m_SiteInfo.siteId);
	GetDlgItemText(IDC_EB_SITE_NAME   , m_SiteInfo.siteName);
	GetDlgItemText(IDC_EB_SITE_TEL    , m_SiteInfo.phoneNo1);
	GetDlgItemText(IDC_EB_SITE_ADDR   , m_SiteInfo.comment1);
	GetDlgItemText(IDC_EB_SITE_MAN    , m_SiteInfo.ceoName);
	GetDlgItemText(IDC_EB_SITE_MAN_TEL, m_SiteInfo.phoneNo2);
	GetDlgItemText(IDC_EB_REMARK      , m_SiteInfo.comment2);

	if(m_SiteInfo.siteId.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_SITEINFODLG_MSG001));
		((CEdit*)GetDlgItem(IDC_EB_SITE_ID))->SetFocus();
		return;
	}

	if(hasDoubleByte(m_SiteInfo.siteId))
	{
		UbcMessageBox(LoadStringById(IDS_SITEINFODLG_MSG002));
		((CEdit*)GetDlgItem(IDC_EB_SITE_ID))->SetFocus();
		return;
	}

	m_SiteInfo.businessType = ::ToString((int)m_combo_businessType.GetItemData(m_combo_businessType.GetCurSel()));
	m_SiteInfo.businessCode = ::ToString((int)m_combo_businessCode.GetItemData(m_combo_businessCode.GetCurSel()));

	m_SiteInfo.siteName = (m_SiteInfo.siteName.IsEmpty() ? m_SiteInfo.siteId : m_SiteInfo.siteName);

	OnOK();
}

BOOL CSiteInfoDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == ((CEdit*)GetDlgItem(IDC_EB_SITE_ID))->GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam, CPreventChar::eId))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}
