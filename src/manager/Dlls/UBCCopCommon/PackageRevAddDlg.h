#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "afxdtctl.h"

#include "resource.h"
#include "copmodule.h"

#include "common\ubcdefine.h"
#include "common\HoverButton.h"
#include "common\utblistctrl.h"

// CPackageRevAddDlg dialog
class CPackageRevAddDlg : public CDialog
{
	DECLARE_DYNAMIC(CPackageRevAddDlg)
public:
	void Set(ESrcApp, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, BOOL, PackageRevInfoList&, BOOL bModRev=FALSE);
	void SetTime(time_t, time_t);
	EScheRev GetRet(CString& szSite, CString& szPackage);
	void SetTitle(LPCTSTR);
	void SetCustomer(LPCTSTR);

private:
	void InitListCur();
	void InitListRev();
	void SetDataListCur();
	void SetDataListRev();
	void SetDateTime(SYSTEMTIME& st1, SYSTEMTIME& st2);
public:
	BOOL m_bStart;
	BOOL m_bEnd;

	CTime m_tmStart;
	CTime m_tmEnd;

	CString m_strCustomer;
private:
	BOOL m_bSTimeInit;
	BOOL m_bETimeInit;

	CString m_szMsg;
	CString m_szTitle;
	CString m_szUsrSite;
	CString m_szSchSite;
	CString m_szPackage;
	CString m_RevId;

	EScheRev m_RetMsg;

	CUTBListCtrl m_lscPackageCur;
	CUTBListCtrl m_lscPackageRev;
	CButton m_chkStart;
	CButton m_chkEnd;
	CDateTimeCtrl m_dtcStart;
	CDateTimeCtrl m_dtcStart2;
	CDateTimeCtrl m_dtcEnd;
	CDateTimeCtrl m_dtcEnd2;

	ESrcApp m_SrcApp;
	BOOL m_bModifiedPackage;
	BOOL m_bModifyReservation;
//	PackageInfoList m_lsPackageInfo;
	PackageRevInfoList* m_pScheRevInfo;

public:
	CPackageRevAddDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPackageRevAddDlg();

// Dialog Data
	enum { IDD = IDD_RESERVE_ADD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedCheckStart();
	afx_msg void OnBnClickedCheckEnd();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBnSelectPackage();
};
