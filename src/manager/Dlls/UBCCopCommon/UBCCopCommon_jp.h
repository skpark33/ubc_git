#pragma once

#define UBCDEFAULT_FONT					_T("MS ゴシック")
#define UBCSTUDIOEXE_STR				_T("UBCStudio_jp.exe")
#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE_jp.exe")
#define UBCMANAGEREXE_STR				_T("UBCManager_jp.exe")

//CFtpMultiSite
#define FTPMULTISITE_STR001			_T("グループ %s に接続中..")
#define FTPMULTISITE_STR002			_T("失敗")
#define FTPMULTISITE_STR003			_T("エラー")
#define FTPMULTISITE_STR004			_T("キャンセル")
#define FTPMULTISITE_STR005			_T("成功")
#define FTPMULTISITE_STR006			_T("スタンバイ")
#define FTPMULTISITE_STR007			_T("アップロード中")
#define FTPMULTISITE_STR008			_T("ダウンロード中")
#define FTPMULTISITE_STR009			_T("スキップ")
#define FTPMULTISITE_STR010			_T("ローカル")
#define FTPMULTISITE_STR011			_T("サーバー")
#define FTPMULTISITE_STR012			_T("不明")
#define FTPMULTISITE_STR013			_T("すべて")

#define FTPMULTISITE_LST001			_T("サイト")
#define FTPMULTISITE_LST002			_T("ファイル名")
#define FTPMULTISITE_LST003			_T("場所")
#define FTPMULTISITE_LST004			_T("進捗状況")
#define FTPMULTISITE_LST005			_T("ローカルのサイズ")
#define FTPMULTISITE_LST006			_T("サーバーのサイズ")
#define FTPMULTISITE_LST007			_T("状態")
#define FTPMULTISITE_LST008			_T("メッセージ")
#define FTPMULTISITE_LST009			_T("サーバーのパス")

#define FTPMULTISITE_MSG001			_T(">>グループ(%s) の接続に失敗しました。\n")
#define FTPMULTISITE_MSG002			_T(">> グループ(%s) : %s\n")
#define FTPMULTISITE_MSG003			_T("> グループ(%s) : %s\n")
#define FTPMULTISITE_MSG004			_T("グループ : %s / ファイル : %s")
#define FTPMULTISITE_MSG005			_T("ファイルのアップロードが完了しました")
#define FTPMULTISITE_MSG006			_T(">> グループ(%s) の接続に失敗しました。\n")
#define FTPMULTISITE_MSG007			_T("ファイルのダウンロードが完了しました。")
#define FTPMULTISITE_MSG008			_T("ユーザーによる取り消し")
#define FTPMULTISITE_MSG009			_T("ファイルのサイズが違います サーバー [%s], ローカル [%s]")
#define FTPMULTISITE_MSG010			_T("既にファイルがあるので、ダウンロードされていません")
#define FTPMULTISITE_MSG011			_T("ファイルのダウンロードが失敗しました")
#define FTPMULTISITE_MSG012			_T("ファイルのアップロードに失敗しました")

//CPackageMngDlg
#define PACKAGEMNGDLG_LST001		_T("グループ")
#define PACKAGEMNGDLG_LST002		_T("スケジュール")
#define PACKAGEMNGDLG_LST003		_T("ユーザー")
#define PACKAGEMNGDLG_LST004		_T("時間")
#define PACKAGEMNGDLG_LST005		_T("説明")

//CPackageRevAddDlg
#define PACKAGEREVADDDLG_STR001	_T("スケジュール予約")
#define PACKAGEREVADDDLG_STR002	_T("適用された端末")
#define PACKAGEREVADDDLG_STR003	_T("予約された端末")

#define PACKAGEREVADDDLG_MSG001	_T("開始時間を入力してください。")
#define PACKAGEREVADDDLG_MSG002	_T("開始時間は現在の時間より５分遅れて設定します。")
#define PACKAGEREVADDDLG_MSG003	_T("終了時刻は開始時刻よりも少なくとも5分後に設定する必要があります")
#define PACKAGEREVADDDLG_MSG004	_T("スケジュールが変更された状態です。\nスケジュールを適用すればサーバーにあるスケジュールが変更されます。\nサーバーにあるスケジュールを変更しない際には別名で保存してください。\n名前を付けて保存しますか。")
#define PACKAGEREVADDDLG_MSG005	_T("開始時間が間違っています。\n開始時間を他のターミナルに適用した時間より遅く設定します。")

//CPackageRevDlg
#define PACKAGEREVDLG_LST001		_T("グループ")
#define PACKAGEREVDLG_LST002		_T("端末")
#define PACKAGEREVDLG_LST003		_T("ディスプレー")
#define PACKAGEREVDLG_LST004		_T("予約されたスケジュール")
#define PACKAGEREVDLG_LST005		_T("最近のスケジュール")
#define PACKAGEREVDLG_LST006		_T("現在のスケジュール")
#define PACKAGEREVDLG_LST007		_T("自動設定のスケジュール")
#define PACKAGEREVDLG_LST008		_T("スケジュール")
#define PACKAGEREVDLG_LST009		_T("開始時間")
#define PACKAGEREVDLG_LST010		_T("終了時間")

#define PACKAGEREVDLG_STR001		_T("スケジュール予約")
#define PACKAGEREVDLG_STR002		_T("スケジュール追加")
#define PACKAGEREVDLG_STR003		_T("スケジュール編集")
#define PACKAGEREVDLG_STR004		_T("スケジュール削除")

#define PACKAGEREVDLG_MSG001		_T("端末が選択されていません。\n端末を選択してからもう一度やり直してください。")
#define PACKAGEREVDLG_MSG002		_T("スケジュールが選択されていません。\nスケジュールを選択してからもう一度やり直してください。")
#define PACKAGEREVDLG_MSG003		_T("スケジュールが選択されていません。\nスケジュールを選択してからもう一度やり直してください。")
#define PACKAGEREVDLG_MSG004		_T("選択したスケジュールを削除しますか?")
#define PACKAGEREVDLG_MSG005		_T("グループ %s が登録されていません。グループ %s を登録した後やり直してください。")

//CSiteInfoDlg
#define SITEINFODLG_STR001			_T("休日・祝日")

#define SITEINFODLG_MSG001			_T("グループを入力してください。")
#define SITEINFODLG_MSG002			_T("グループIDは英語と数字だけ入力できます。")

//CFlashView
#define FLASHVIEW_MSG001			_T("フラッシュコントロールが設置されなかったかサポートされないバージョンです。\nフラッシュダウンロードページに移動しますか。")

//CAuthCheckDlg
#define AUTHCHECK_MSG001			_T("認証に失敗しました。\nエンタープライズキーとエンタープライズパスワードをもう一度確認してください。")

//CSelectHostDlg
#define SELECTHOST_STR001				_T("使用")
#define SELECTHOST_STR002				_T("不使用")
#define SELECTHOST_STR003				_T("接続")
#define SELECTHOST_STR004				_T("未接続")

#define SELECTHOST_LIST001				_T("端末名")
#define SELECTHOST_LIST002				_T("グループ名")

#define SELECTHOST_BTN001				_T("端末のリストを更新")

#define SELECTHOST_MSG001				_T("適用する端末を選択してください")

//CMultiSelectHost
#define MULTISELECTHOST_BTN001			_T("端末のリストを更新")
		
#define MULTISELECTHOST_STR001			_T("使用")
#define MULTISELECTHOST_STR002			_T("不使用")
#define MULTISELECTHOST_STR003			_T("接続")
#define MULTISELECTHOST_STR004			_T("未接続")
#define MULTISELECTHOST_STR005			_T("All")
		
#define MULTISELECTHOST_LIST001			_T("端末名")
#define MULTISELECTHOST_LIST002			_T("グループ名")
		
#define MULTISELECTHOST_MSG001			_T("適用する端末を選択してください")
