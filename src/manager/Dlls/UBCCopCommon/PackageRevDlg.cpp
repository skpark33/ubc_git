// PackageRevDlg.cpp : implementation file
//
#include "stdafx.h"
#include "common/ubcdefine.h"
#include "copmodule.h"
#include "PackageRevDlg.h"
#include "CopComData.h"
#include "PackageRevAddDlg.h"
#include "common\TraceLog.h"
#include "common\libCommon\ubcMuxRegacy.h"
#include "FtpMultiSite.h"
#include "MultiSelectHost.h"

#define STR_TERM_SITE		LoadStringById(IDS_PACKAGEREVDLG_LST001)
#define STR_TERM_HOSTID		LoadStringById(IDS_PACKAGEREVDLG_LST002)
#define STR_TERM_HOSTNAME	LoadStringById(IDS_PACKAGEREVDLG_LST011)
#define STR_TERM_DISP		LoadStringById(IDS_PACKAGEREVDLG_LST003)
#define STR_TERM_REVS		LoadStringById(IDS_PACKAGEREVDLG_LST004)
#define STR_TERM_LASS		LoadStringById(IDS_PACKAGEREVDLG_LST005)
#define STR_TERM_CURS		LoadStringById(IDS_PACKAGEREVDLG_LST006)
#define STR_TERM_AUTS		LoadStringById(IDS_PACKAGEREVDLG_LST007)

#define STR_SCHE_NAME		LoadStringById(IDS_PACKAGEREVDLG_LST008)
#define STR_SCHE_START		LoadStringById(IDS_PACKAGEREVDLG_LST009)
#define STR_SCHE_END		LoadStringById(IDS_PACKAGEREVDLG_LST010)

// CPackageRevDlg dialog
IMPLEMENT_DYNAMIC(CPackageRevDlg, CDialog)

CPackageRevDlg::CPackageRevDlg(LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CPackageRevDlg::IDD, pParent)
, m_strCustomer ( szCustomer )
,	m_EventManager(this)
{
	m_szUsrSite = _T("*");
	m_szPackage.Empty();
	m_szScheSite.Empty();

	m_SrcApp = eSAUnknwon;
	m_bModify = false;

	m_RetMsg = eSRNormal;
	m_nCurSide = 0;
	m_szCurSite.Empty();
	m_szCurHost.Empty();
	m_szDrive.Empty();

	m_szTitle = LoadStringById(IDS_PACKAGEREVDLG_STR001);

	m_bCanAdd = true;
}

CPackageRevDlg::~CPackageRevDlg()
{
}

void CPackageRevDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_TERMINAL, m_lscTerminal);
	DDX_Control(pDX, IDC_LIST_PACKAGE, m_lscPackage);
}

BEGIN_MESSAGE_MAP(CPackageRevDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CPackageRevDlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_MOD, &CPackageRevDlg::OnBnClickedButtonMod)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CPackageRevDlg::OnBnClickedButtonDel)

	ON_NOTIFY(NM_CLICK, IDC_LIST_TERMINAL, OnNMClickListHost)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_TERMINAL, &CPackageRevDlg::OnLvnItemchangedListTerminal)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_HOST, &CPackageRevDlg::OnBnClickedButtonSelectHost)
	ON_EN_CHANGE(IDC_EB_SEARCH, &CPackageRevDlg::OnEnChangeEbSearch)
END_MESSAGE_MAP()

// CPackageRevDlg message handlers
BOOL CPackageRevDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_szTermTitle[eTermSite] = STR_TERM_SITE;
	m_szTermTitle[eTermHostName] = STR_TERM_HOSTNAME;
	m_szTermTitle[eTermHostId] = STR_TERM_HOSTID;
	m_szTermTitle[eTermDisp] = STR_TERM_DISP;
	m_szTermTitle[eRevPackage] = STR_TERM_REVS;
	m_szTermTitle[eLastPackage] = STR_TERM_LASS;
	m_szTermTitle[eCurPackage] = STR_TERM_CURS;
	m_szTermTitle[eAutoPackage] = STR_TERM_AUTS;

	m_szScheTitle[eScheName] = STR_SCHE_NAME;
	m_szScheTitle[eScheSTime] = STR_SCHE_START;
	m_szScheTitle[eScheETime] = STR_SCHE_END;

	InitTermList();
	InitScheList();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString strPath;
	strPath.Format("%s\\%sdata\\%s", szDrive, UBC_EXECUTE_PATH, _T("UBCManager.ini"));
	m_lscTerminal.LoadColumnState("PACKAGE_REV_HOST-LIST", strPath);
	m_lscPackage.LoadColumnState("PACKAGE_REV_PACKAGE-LIST", strPath);

	//CCopModule::GetObject()->GetHostRev(m_szUsrSite, CCopComData::GetObject()->m_lsHosRev);
	SetDataTermList(NULL);

	// 이벤트생성시 오래걸리는 현상이 발생되어 스레드로 뺀다.
	AfxBeginThread(AddEventThread, this);

	GetDlgItem(IDC_BUTTON_SELECT_HOST)->EnableWindow(m_szHost.IsEmpty());
	GetDlgItem(IDC_BUTTON_ADD)->EnableWindow(m_bCanAdd);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

UINT CPackageRevDlg::AddEventThread(LPVOID pParam)
{
	CPackageRevDlg* pView = (CPackageRevDlg*)pParam;
	if(!pView) return 0;
	HWND hWnd = pView->GetSafeHwnd();

	TraceLog(("CPackageRevDlg::AddEventThread - Start"));

	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	if(::IsWindow(hWnd)) pView->m_EventManager.AddEvent("* scheduleReserved");
	if(::IsWindow(hWnd)) pView->m_EventManager.AddEvent("* reservationRemoved");
	if(::IsWindow(hWnd)) pView->m_EventManager.AddEvent("* reservationExpired");
	if(::IsWindow(hWnd)) pView->m_EventManager.AddEvent("* reservationChanged");

	::CoUninitialize();

	TraceLog(("CPackageRevDlg::AddEventThread - End"));

	return 0;
}

void CPackageRevDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString strPath;
	strPath.Format("%s\\%sdata\\%s", szDrive, UBC_EXECUTE_PATH, _T("UBCManager.ini"));
	m_lscTerminal.SaveColumnState("PACKAGE_REV_HOST-LIST", strPath);
	m_lscPackage.SaveColumnState("PACKAGE_REV_PACKAGE-LIST", strPath);

	// 0001414: 단말목록등 서버로부터 이벤트를 받는 화면이 닫힐때 응답없음으로 빠지는 문제.
	// 더이상 이벤트가 날아와도 처리하지 않도록 한다
	m_EventManager.RemoveAllEvent();
}

void CPackageRevDlg::Set(ESrcApp app, LPCTSTR szUsrSite, LPCTSTR szDrive, LPCTSTR szHost, LPCTSTR szPackage, LPCTSTR szScheSite, int nAuthority, LPCTSTR strUserId, LPCTSTR strSiteId, LPCTSTR strIniPath, bool& bMod, bool bCanAdd)
{
	m_SrcApp = app;
	m_bModify = bMod;
	m_pModify = &bMod;
	m_szUsrSite = szUsrSite?szUsrSite:"*";
	m_szDrive = szDrive?szDrive:"";
	m_szHost = szHost?szHost:"";
	m_szPackage = szPackage?szPackage:"";
	m_szScheSite = szScheSite?szScheSite:"";

	m_nAuthority = nAuthority;	// 권한
	m_strUserId = strUserId; // 로그에 남기기 위해 추가함
	m_strSiteId = strSiteId?strSiteId:"";	// 로그인site
	m_strIniPath = strIniPath?strIniPath:"";	// ini 경로

	m_bCanAdd = bCanAdd;
}

EScheRev CPackageRevDlg::GetRet(CString& szPackage)
{
	szPackage = m_szPackage;
	return m_RetMsg;
}

void CPackageRevDlg::InitTermList()
{
	m_lscTerminal.SetExtendedStyle(m_lscTerminal.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	for(int nCol = 0; nCol < eTermEnd; nCol++)
	{
		m_lscTerminal.InsertColumn(nCol, m_szTermTitle[nCol], LVCFMT_LEFT, 100);
	}

//	m_lscTerminal.SetColumnWidth(eRevPackage, 300);
	m_lscTerminal.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CPackageRevDlg::InitScheList()
{
//	m_lscPackage.SetExtendedStyle(m_lscPackage.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lscPackage.SetExtendedStyle(m_lscPackage.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_GRIDLINES);

	for(int nCol = 0; nCol < eScheEnd; nCol++)
	{
		m_lscPackage.InsertColumn(nCol, m_szScheTitle[nCol], LVCFMT_LEFT, 100);
	}

	m_lscPackage.SetColumnWidth(eScheCheck, 22);
	m_lscPackage.SetColumnWidth(eScheSTime, 200);
	m_lscPackage.SetColumnWidth(eScheETime, 200);

	//m_lscPackage.InitHeader(IDB_UBCLISTCTR_HEAD);
	m_lscPackage.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CPackageRevDlg::SetDataTermList(CStringArray* pHostIdList)
{
	m_lscTerminal.SetRedraw(FALSE);
	m_lscTerminal.EnableWindow(FALSE);

	m_lscTerminal.DeleteAllItems();

	CString strWhere;
	CString strHostList;

	if(pHostIdList)
	{
		for(int i=0; i<pHostIdList->GetCount() ;i++)
		{
			CString strHostId;
			strHostId.Format(_T(",'%s'"), pHostIdList->GetAt(i));
			strHostList += strHostId;
		}
	}

	if(!m_szHost.IsEmpty())
	{
		strHostList = _T("'") + m_szHost + _T("'") + strHostList;
	}

	strHostList.Trim(_T(","));
	strWhere.Format(_T("hostId in (%s) order by hostName"), strHostList);
	CWaitMessageBox wait;
	CCopModule::GetObject()->GetHostRev(m_szUsrSite, CCopComData::GetObject()->m_lsHosRev, strWhere);

	int nRow = 0;
	POSITION pos = CCopComData::GetObject()->m_lsHosRev.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostRevInfo info = CCopComData::GetObject()->m_lsHosRev.GetNext(pos);

		m_lscTerminal.InsertItem(nRow, info.siteName);
		m_lscTerminal.SetItemText(nRow, eTermHostName, info.hostName);
		m_lscTerminal.SetItemText(nRow, eTermHostId, info.hostId);
		m_lscTerminal.SetItemText(nRow, eTermDisp, (info.disSide==eDispA)? LoadStringById(IDS_PACKAGEREVDLG_STR005):LoadStringById(IDS_PACKAGEREVDLG_STR006));

		CString szBuf;
		for(std::list<std::string>::iterator Iter = info.reservedPackageList.begin(); Iter != info.reservedPackageList.end(); Iter++)
		{
			szBuf += (*Iter).c_str();
			szBuf += ", ";
		}
		m_lscTerminal.SetItemText(nRow, eRevPackage, szBuf);

		m_lscTerminal.SetItemText(nRow, eLastPackage, info.lastPackage);
		m_lscTerminal.SetItemText(nRow, eCurPackage, info.currentPackage);
		m_lscTerminal.SetItemText(nRow, eAutoPackage, info.autoPackage);

		m_lscTerminal.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	m_lscTerminal.EnableWindow(TRUE);
	m_lscTerminal.SetRedraw(TRUE);

	if(m_lscTerminal.GetItemCount() > 0)
	{
		m_lscTerminal.SetSelectionMark(0);
		m_lscTerminal.SetItemState(0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);
		SelectHostList(0);
	}
}

void CPackageRevDlg::SetDataScheList(LPCTSTR szSite, LPCTSTR szHost, int nSide)
{
	m_nCurSide = nSide;
	m_szCurSite = szSite?szSite:"";
	m_szCurHost = szHost?szHost:"";

	CWaitMessageBox wait;
	CCopModule::GetObject()->GetPackageRev(szSite, szHost, nSide, CCopComData::GetObject()->m_lsPackageRev);

	CString strFilter;
	GetDlgItemText(IDC_EB_SEARCH, strFilter);
	RefreshPackageList(strFilter);
}

void CPackageRevDlg::RefreshPackageList(CString strFilter)
{
	strFilter.MakeLower();
	m_lscPackage.DeleteAllItems();

	int nRow = 0;
	SPackageRevInfo info;
	POSITION pos = CCopComData::GetObject()->m_lsPackageRev.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SPackageRevInfo info = CCopComData::GetObject()->m_lsPackageRev.GetNext(pos);

		if(!strFilter.IsEmpty())
		{
			CString strPackage = info.programId;
			strPackage.MakeLower();

			if(strPackage.Find(strFilter) < 0)
			{
				continue;
			}
		}

		m_lscPackage.InsertItem(nRow, "");
		m_lscPackage.SetItemText(nRow, eScheName, info.programId);

		CTime tmFrom(info.fromTime);
		if(tmFrom.Format(STR_DEF_TIME) == STR_DEF_EMPTY_TIME)
		{
			m_lscPackage.SetItemText(nRow, eScheSTime, "");
		}
		else
		{
			m_lscPackage.SetItemText(nRow, eScheSTime, tmFrom.Format(STR_DEF_TIME));
		}

		CTime tmTo(info.toTime);
		if(tmTo.Format(STR_DEF_TIME) == STR_DEF_EMPTY_TIME)
		{
			m_lscPackage.SetItemText(nRow, eScheETime, "");
		}
		else
		{
			m_lscPackage.SetItemText(nRow, eScheETime, tmTo.Format(STR_DEF_TIME));
		}

		m_lscPackage.SetItemData(nRow, (DWORD_PTR)posOld);
		nRow++;
	}

	m_lscPackage.SortList(eScheName, false, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscPackage);
}

void CPackageRevDlg::OnNMClickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;
	
	if(nRow < 0 || nCol < 0)
		return;

	if(m_lscTerminal.IsWindowEnabled())
	{
		SelectHostList(nRow);
	}
}

void CPackageRevDlg::OnLvnItemchangedListTerminal(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(m_lscTerminal.IsWindowEnabled())
	{
		OnNMClickListHost(pNMHDR, pResult);
	}
}

void CPackageRevDlg::SelectHostList(int nRow)
{
	if(m_lscTerminal.GetItemCount() == 0)
		return;

	POSITION pos = (POSITION)m_lscTerminal.GetItemData(nRow);
	if(!pos) return;

	SHostRevInfo info = CCopComData::GetObject()->m_lsHosRev.GetNext(pos);
	SetDataScheList(info.siteId, info.hostId, info.disSide);

	CString szBuf;
	szBuf.Format(LoadStringById(IDS_PACKAGEREVDLG_STR007), info.hostName, info.disSide+1);
	GetDlgItem(IDC_STATIC_HOST)->SetWindowText(szBuf);
}

int CPackageRevDlg::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	cciEvent* ev = (cciEvent*)lParam;
	cciEntity aEntity;
	aEntity.set(ev->getEntity());
	ciString strType = ev->getEventType();

	CCI::CCI_AttributeList& tempList = ev->getAttributeList();
	cciAttributeList attList(tempList);

	switch(wParam)
	{
	case IEH_SCHEREV:
		TraceLog(("scheduleReserved"));
		return ieReserved(&aEntity, &attList);
	case IEH_REMVREV:
		TraceLog(("reservationRemoved"));
		return ieRemoved(&aEntity, &attList);
	case IEH_EXPIREV:
		TraceLog(("reservationExpired"));
		return ieExpired(&aEntity, &attList);
	case IEH_CHNGREV:
		TraceLog(("reservationChanged"));
		return ieChanged(&aEntity, &attList);
	case IEH_UNKOWN:
		TraceLog(("InvokeEvent : Unkown"));
		//cciAttributeList* pList = (cciAttributeList*)lParam;

		//for(int i = 0; i < (int)pList->length(); i++){
		//	cciAttribute& attr = (*pList)[i];

		//	if(strcmp(attr.getName(),"siteId") == 0){
		//		attr.getValue(strSite);
		//	}
		//}
		break;
	}

	return 0;
}

int CPackageRevDlg::ieReserved(cciEntity* pEnt, cciAttributeList* pList)
{
	ciTime fromTime, toTime;
	ciShort side;
	ciString reservationId, siteId, hostId, programId;
	CCI::CCI_StringList reservedPackageList1, reservedPackageList2;

	for(int i = 0; i < pEnt->length(); i++){
		ciString szName = pEnt->getClassName(i);
		
		if(szName == "Site"){
			siteId = pEnt->getClassValue(i);
		}
		else if(szName == "Host"){
			hostId = pEnt->getClassValue(i);
		}
	}

	for(int i = 0; i < (int)pList->length(); i++){
		cciAttribute& attr = (*pList)[i];

		if(strcmp(attr.getName(),"reservationId") == 0){
			attr.getValue(reservationId);
		}
		else if(strcmp(attr.getName(),"siteId") == 0){
			attr.getValue(siteId);
		}
		else if(strcmp(attr.getName(),"hostId") == 0){
			attr.getValue(hostId);
		}
		else if(strcmp(attr.getName(),"programId") == 0){
			attr.getValue(programId);
		}
		else if(strcmp(attr.getName(),"side") == 0){
			attr.getValue(side);
		}
		else if(strcmp(attr.getName(),"fromTime") == 0){
			attr.getValue(fromTime);
		}
		else if(strcmp(attr.getName(),"toTime") == 0){
			attr.getValue(toTime);
		}
		else if(strcmp(attr.getName(),"reservedScheduleList1") == 0){
			attr.getValueStringList(reservedPackageList1);
		}
		else if(strcmp(attr.getName(),"reservedScheduleList2") == 0){
			attr.getValueStringList(reservedPackageList2);
		}
	}

	TraceLog(("siteId:%s hostId:%s disSide:%d", siteId.c_str(),hostId.c_str(),side));
	SPackageRevInfo infoRev;

	infoRev.reservationId = reservationId.c_str();
	infoRev.siteId = siteId.c_str();
	infoRev.hostId = hostId.c_str();
	infoRev.programId = programId.c_str();
	infoRev.side = side;
	infoRev.fromTime = fromTime.getTime();
	infoRev.toTime = toTime.getTime();

	for(int nRow = 0; nRow < m_lscTerminal.GetItemCount(); nRow++){
		POSITION pos = (POSITION)m_lscTerminal.GetItemData(nRow);
		SHostRevInfo Info = CCopComData::GetObject()->m_lsHosRev.GetAt(pos);
		
		if(Info.hostId != infoRev.hostId)
			continue;
		if(infoRev.side != (Info.disSide+1) && infoRev.side != 3)
			continue;

		if(Info.disSide == eDispA){
			cciStringList ccistr;
			ccistr.set(reservedPackageList1);
			Info.reservedPackageList = ccistr.getStringList();
		}else{
			cciStringList ccistr;
			ccistr.set(reservedPackageList2);
			Info.reservedPackageList = ccistr.getStringList();
		}

		CString szBuf = "";
		for(std::list<std::string>::iterator Iter = Info.reservedPackageList.begin(); Iter != Info.reservedPackageList.end(); Iter++){
			szBuf += (*Iter).c_str();
			szBuf += ", ";
		}
		m_lscTerminal.SetItemText(nRow, eRevPackage, szBuf);
	}

	if(m_szCurHost != hostId.c_str())
		return 0;
	if((m_nCurSide+1) != side && side != 3)
		return 0;

	SetDataScheList(m_szCurSite, m_szCurHost, m_nCurSide);

	return 0;
}

int CPackageRevDlg::ieRemoved(cciEntity* pEnt, cciAttributeList* pList)
{
	ciShort  side;
	ciTime fromTime, toTime;
	ciString reservationId, siteId, hostId, programId;
	CCI::CCI_StringList reservedPackageList1, reservedPackageList2;

	for(int i = 0; i < pEnt->length(); i++){
		ciString szName = pEnt->getClassName(i);
		
		if(szName == "Site"){
			siteId = pEnt->getClassValue(i);
		}
		else if(szName == "Host"){
			hostId = pEnt->getClassValue(i);
		}
	}

	for(int i = 0; i < (int)pList->length(); i++){
		cciAttribute& attr = (*pList)[i];

		if(strcmp(attr.getName(),"reservationId") == 0){
			attr.getValue(reservationId);
		}
		else if(strcmp(attr.getName(),"siteId") == 0){
			attr.getValue(siteId);
		}
		else if(strcmp(attr.getName(),"hostId") == 0){
			attr.getValue(hostId);
		}
		else if(strcmp(attr.getName(),"programId") == 0){
			attr.getValue(programId);
		}
		else if(strcmp(attr.getName(),"side") == 0){
			attr.getValue(side);
		}
		else if(strcmp(attr.getName(),"fromTime") == 0){
			attr.getValue(fromTime);
		}
		else if(strcmp(attr.getName(),"toTime") == 0){
			attr.getValue(toTime);
		}
		else if(strcmp(attr.getName(),"reservedScheduleList1") == 0){
			attr.getValueStringList(reservedPackageList1);
		}
		else if(strcmp(attr.getName(),"reservedScheduleList2") == 0){
			attr.getValueStringList(reservedPackageList2);
		}
	}

	TraceLog(("siteId:%s hostId:%s disSide:%d", siteId.c_str(),hostId.c_str(),side));
	SPackageRevInfo infoRev;

	infoRev.reservationId = reservationId.c_str();
	infoRev.siteId = siteId.c_str();
	infoRev.hostId = hostId.c_str();
	infoRev.programId = programId.c_str();
	infoRev.side = side;
	infoRev.fromTime = fromTime.getTime();
	infoRev.toTime = toTime.getTime();

	for(int nRow = 0; nRow < m_lscTerminal.GetItemCount(); nRow++){
		POSITION pos = (POSITION)m_lscTerminal.GetItemData(nRow);
		SHostRevInfo Info = CCopComData::GetObject()->m_lsHosRev.GetAt(pos);
		
		if(Info.hostId != infoRev.hostId)
			continue;
		if(infoRev.side != (Info.disSide+1) && infoRev.side != 3)
			continue;

		if(Info.disSide == eDispA){
			cciStringList ccistr;
			ccistr.set(reservedPackageList1);
			Info.reservedPackageList = ccistr.getStringList();
		}else{
			cciStringList ccistr;
			ccistr.set(reservedPackageList2);
			Info.reservedPackageList = ccistr.getStringList();
		}

		CString szBuf = "";
		for(std::list<std::string>::iterator Iter = Info.reservedPackageList.begin(); Iter != Info.reservedPackageList.end(); Iter++){
			szBuf += (*Iter).c_str();
			szBuf += ", ";
		}
		m_lscTerminal.SetItemText(nRow, eRevPackage, szBuf);
	}

	//if(m_szCurHost != hostId.c_str())
	//	return 0;
	//if((m_nCurSide+1) != side && side != 3)
	//	return 0;

	//m_lscPackage.DeleteAllItems();

	return 0;
}

int CPackageRevDlg::ieExpired(cciEntity* pEnt, cciAttributeList* pList)
{
	return 0;
}

int CPackageRevDlg::ieChanged(cciEntity* pEnt, cciAttributeList* pList)
{
	ciShort  side;
	ciTime fromTime, toTime;
	ciString reservationId, siteId, hostId, programId;
	CCI::CCI_StringList reservedPackageList1, reservedPackageList2;

	for(int i = 0; i < pEnt->length(); i++){
		ciString szName = pEnt->getClassName(i);
		
		if(szName == "Site"){
			siteId = pEnt->getClassValue(i);
		}
		else if(szName == "Host"){
			hostId = pEnt->getClassValue(i);
		}
	}

	for(int i = 0; i < (int)pList->length(); i++){
		cciAttribute& attr = (*pList)[i];

		if(strcmp(attr.getName(),"reservationId") == 0){
			attr.getValue(reservationId);
		}
		else if(strcmp(attr.getName(),"siteId") == 0){
			attr.getValue(siteId);
		}
		else if(strcmp(attr.getName(),"hostId") == 0){
			attr.getValue(hostId);
		}
		else if(strcmp(attr.getName(),"programId") == 0){
			attr.getValue(programId);
		}
		else if(strcmp(attr.getName(),"side") == 0){
			attr.getValue(side);
		}
		else if(strcmp(attr.getName(),"fromTime") == 0){
			attr.getValue(fromTime);
		}
		else if(strcmp(attr.getName(),"toTime") == 0){
			attr.getValue(toTime);
		}
		else if(strcmp(attr.getName(),"reservedScheduleList1") == 0){
			attr.getValueStringList(reservedPackageList1);
		}
		else if(strcmp(attr.getName(),"reservedScheduleList2") == 0){
			attr.getValueStringList(reservedPackageList2);
		}
	}

	TraceLog(("siteId:%s hostId:%s disSide:%d", siteId.c_str(),hostId.c_str(),side));
	SPackageRevInfo infoRev;

	infoRev.reservationId = reservationId.c_str();
	infoRev.siteId = siteId.c_str();
	infoRev.hostId = hostId.c_str();
	infoRev.programId = programId.c_str();
	infoRev.side = side;
	infoRev.fromTime = fromTime.getTime();
	infoRev.toTime = toTime.getTime();

	for(int nRow = 0; nRow < m_lscTerminal.GetItemCount(); nRow++){
		POSITION pos = (POSITION)m_lscTerminal.GetItemData(nRow);
		SHostRevInfo Info = CCopComData::GetObject()->m_lsHosRev.GetAt(pos);
		
		if(Info.hostId != infoRev.hostId)
			continue;
		if(infoRev.side != (Info.disSide+1) && infoRev.side != 3)
			continue;

		if(Info.disSide == eDispA){
			cciStringList ccistr;
			ccistr.set(reservedPackageList1);
			Info.reservedPackageList = ccistr.getStringList();
		}else{
			cciStringList ccistr;
			ccistr.set(reservedPackageList2);
			Info.reservedPackageList = ccistr.getStringList();
		}

		CString szBuf = "";
		for(std::list<std::string>::iterator Iter = Info.reservedPackageList.begin(); Iter != Info.reservedPackageList.end(); Iter++){
			szBuf += (*Iter).c_str();
			szBuf += ", ";
		}
		m_lscTerminal.SetItemText(nRow, eRevPackage, szBuf);
	}

	for(int nRow = 0; nRow < m_lscPackage.GetItemCount(); nRow++){
		POSITION pos = (POSITION)m_lscPackage.GetItemData(nRow);
		SPackageRevInfo Info = CCopComData::GetObject()->m_lsPackageRev.GetAt(pos);

		if(Info.reservationId != infoRev.reservationId)
			continue;

		CTime tmFrom(infoRev.fromTime);
		if(tmFrom.Format(STR_DEF_TIME) == STR_DEF_EMPTY_TIME){
			m_lscPackage.SetItemText(nRow, eScheSTime, "");
		}else{
			m_lscPackage.SetItemText(nRow, eScheSTime, tmFrom.Format(STR_DEF_TIME));
		}

		CTime tmTo(infoRev.toTime);
		if(tmTo.Format(STR_DEF_TIME) == STR_DEF_EMPTY_TIME){
			m_lscPackage.SetItemText(nRow, eScheETime, "");
		}else{
			m_lscPackage.SetItemText(nRow, eScheETime, tmTo.Format(STR_DEF_TIME));
		}
	}

	m_lscPackage.SortList(eScheName, false, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscPackage);

	return 0;
}

void CPackageRevDlg::OnBnClickedButtonAdd()
{
//	HostRevInfoList listHostRev;
	PackageRevInfoList listScheRev;
	std::list<std::string> listHost;

	for(int nRow = 0; nRow < m_lscTerminal.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscTerminal.GetItemData(nRow);
		SHostRevInfo Info = CCopComData::GetObject()->m_lsHosRev.GetAt(pos);
//		listHostRev.AddTail(Info);

		std::string strHost = Info.hostId;
		if(Info.disSide == eDispA)
			strHost += ":1";
		else
			strHost += ":2";

		listHost.push_back(strHost);

		CWaitMessageBox wait;
		CCopModule::GetObject()->GetPackageRev(Info.siteId, Info.hostId, Info.disSide, listScheRev);
	}

//	if(listHostRev.GetCount() == 0){
	if(listHost.size() == 0)
	{
		MessageBox(LoadStringById(IDS_PACKAGEREVDLG_MSG001), LoadStringById(IDS_PACKAGEREVDLG_STR002), MB_ICONINFORMATION);
		return;
	}

	CPackageRevAddDlg dlgAdd;
	dlgAdd.Set(m_SrcApp, m_szUsrSite, m_szScheSite, m_szPackage, "", m_bModify, listScheRev);
	dlgAdd.SetTitle(LoadStringById(IDS_PACKAGEREVDLG_STR002));
	dlgAdd.SetCustomer(m_strCustomer);

	TraceLog(("Add Reservation"));
	if(dlgAdd.DoModal() != IDOK){
		return;
	}

	CString szRevSite, szRevPackage;
	m_RetMsg = dlgAdd.GetRet(szRevSite, szRevPackage);

	if(m_RetMsg == eSRSaveAs){
		OnOK();
		return;
	}else{
		m_bModify = false;
		(*m_pModify) = false;
	}

	TraceLog(("getMuxData"));
	ubcMuxData * pData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(m_szScheSite);
	if(pData)
	{
		CString strPackageIni;
		strPackageIni.Format(_T("%s%s%s.ini")
							, m_szDrive
							, UBC_CONFIG_PATH
							, m_szPackage
							);

		//TraceLog(("Ftp %s:%d",pData->ipAddress.c_str(),pData->ftpPort));
		//CComFtpDlg dlgFtp;
		//dlgFtp.Set(pData->ipAddress.c_str()
		//		 , pData->ftpId.c_str()
		//		 , pData->ftpPasswd.c_str()
		//		 , pData->ftpPort
		//		 , strPackageIni
		//		 , CComFtpDlg::eUpload
		//		 );

		//if(dlgFtp.DoModal() == IDCANCEL) return;

		// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
		TCHAR szModule[MAX_PATH];
		::ZeroMemory(szModule, MAX_PATH);
		::GetModuleFileName(NULL, szModule, MAX_PATH);

		TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
		_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

		char buf[1024];
		CString strPath, szTemp;
		strPath.Format("%s%sdata\\%s", szDrive, szPath, UBCVARS_INI);

		::GetPrivateProfileString(_T("ROOT"), _T("HTTP_ONLY"), _T(""), buf, 1024, strPath);
		BOOL bIsHttpOnly = (CString(buf).CompareNoCase("CLIENT") == 0);

		CFtpMultiSite dlgMultiFtp(bIsHttpOnly);
		if(!dlgMultiFtp.AddSite(m_szScheSite, strPackageIni))
		{
			CString strMsg;
			strMsg.Format(LoadStringById(IDS_PACKAGEREVDLG_MSG005), m_szScheSite, m_szScheSite);
			UbcMessageBox(strMsg, MB_ICONERROR);
			return;
		}

		if(!dlgMultiFtp.RunFtp(CFtpMultiSite::eUpload)) return;
	}

	TraceLog(("ApplyPackage"));
	CWaitMessageBox wait;
	CCopModule::GetObject()->ApplyPackage( std::string(m_strUserId)
										 , std::string(szRevSite)
										 , std::string(szRevPackage)
										 , std::string(dlgAdd.m_bStart?dlgAdd.m_tmStart.Format(STR_DEF_TIME):"")
										 , std::string(dlgAdd.m_bEnd?dlgAdd.m_tmEnd.Format(STR_DEF_TIME):"")
										 , listHost
										 );
	//OnOK();
}

void CPackageRevDlg::OnBnClickedButtonMod()
{
	PackageRevInfoList listScheRev;
	std::list<std::string> listHost;

	for(int nRow = 0; nRow < m_lscPackage.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscPackage.GetItemData(nRow);
		SPackageRevInfo Info = CCopComData::GetObject()->m_lsPackageRev.GetAt(pos);
		listScheRev.AddTail(Info);
	}
	
	int nCnt = 0;
	for(int nRow = 0; nRow < m_lscPackage.GetItemCount(); nRow++){
		if(!m_lscPackage.GetCheck(nRow))
			continue;

		nCnt++;
		POSITION pos = (POSITION)m_lscPackage.GetItemData(nRow);
		SPackageRevInfo Info = CCopComData::GetObject()->m_lsPackageRev.GetAt(pos);

		CPackageRevAddDlg dlgMod;
		dlgMod.Set(m_SrcApp, m_szUsrSite, Info.siteId, Info.programId, Info.reservationId, false, listScheRev, true);
		dlgMod.SetTitle(LoadStringById(IDS_PACKAGEREVDLG_STR003));
		dlgMod.SetTime(Info.fromTime, Info.toTime);
		dlgMod.SetCustomer(m_strCustomer);

		if(dlgMod.DoModal() != IDOK){
			m_lscPackage.SetCheck(nRow, false);
			continue;
		}
		m_lscPackage.SetCheck(nRow, false);

		CWaitMessageBox wait;
		CCopModule::GetObject()->SetPackage(Info.siteId, Info.hostId, Info.programId, Info.reservationId,
								dlgMod.m_bStart?dlgMod.m_tmStart.Format(STR_DEF_TIME):"",
								dlgMod.m_bEnd?dlgMod.m_tmEnd.Format(STR_DEF_TIME):"");
	}

	if(nCnt == 0)
	{
		MessageBox(LoadStringById(IDS_PACKAGEREVDLG_MSG002), LoadStringById(IDS_PACKAGEREVDLG_STR003), MB_ICONINFORMATION);
		return;
	}
}

void CPackageRevDlg::OnBnClickedButtonDel()
{
	bool bIsDel = false;
	PackageRevInfoList list;
	for(int nRow = m_lscPackage.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscPackage.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lscPackage.GetItemData(nRow);
		SPackageRevInfo Info = CCopComData::GetObject()->m_lsPackageRev.GetAt(pos);
		list.AddTail(Info);

		if(!bIsDel)
		{
			if(MessageBox(LoadStringById(IDS_PACKAGEREVDLG_MSG004), LoadStringById(IDS_PACKAGEREVDLG_STR004), MB_ICONINFORMATION|MB_YESNO) != IDYES)
				return;
			bIsDel = true;
		}

		if( Info.siteId.GetLength()==0 || 
			Info.hostId.GetLength()==0 || 
			Info.reservationId.GetLength()==0 ) continue;

		CWaitMessageBox wait;
		CCopModule::GetObject()->RemovePackageRev(Info.siteId, Info.hostId, Info.reservationId);
		m_lscPackage.DeleteItem(nRow);
	}

	if(list.GetCount() == 0)
	{
		MessageBox(LoadStringById(IDS_PACKAGEREVDLG_MSG003),  LoadStringById(IDS_PACKAGEREVDLG_STR004), MB_ICONINFORMATION);
		return;
	}
}

void CPackageRevDlg::OnBnClickedButtonSelectHost()
{
	CMultiSelectHost dlg(m_strCustomer);

	dlg.m_nAuthority = m_nAuthority;
	dlg.m_strSiteId = m_strSiteId;
	dlg.m_strIniPath = m_strIniPath;

	for(int i=0; i<m_lscTerminal.GetItemCount() ;i++)
	{
		dlg.m_astrSelHostList.Add( m_lscTerminal.GetItemText(i, eTermHostId) );
	}

	if(dlg.DoModal() != IDOK) return;

	SetDataTermList(&dlg.m_astrSelHostList);
}

void CPackageRevDlg::OnEnChangeEbSearch()
{
	CString strFilter;
	GetDlgItemText(IDC_EB_SEARCH, strFilter);
	RefreshPackageList(strFilter);
}
