// AuthCheckDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "AuthCheckDlg.h"

// CAuthCheckDlg dialog
IMPLEMENT_DYNAMIC(CAuthCheckDlg, CDialog)

CAuthCheckDlg::CAuthCheckDlg(CString strCustomerId, CWnd* pParent /*=NULL*/)
	: CDialog(CAuthCheckDlg::IDD, pParent)
	, m_szCustomerId(strCustomerId)
{
	m_szKey.Empty();
	m_szPasswd.Empty();
	m_szHostId.Empty();
}

CAuthCheckDlg::~CAuthCheckDlg()
{
}

void CAuthCheckDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_KEY, m_szKey);
	DDX_Text(pDX, IDC_EDIT_CCPASSWD, m_szPasswd);
	DDX_Text(pDX, IDC_EDIT_HOST_ID, m_szHostId);
}

BEGIN_MESSAGE_MAP(CAuthCheckDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CAuthCheckDlg::OnBnClickedOk)
END_MESSAGE_MAP()

// CAuthCheckDlg message handlers
BOOL CAuthCheckDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetDlgItemText(IDC_TXT_CUSTOMER, m_szCustomerId);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CAuthCheckDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CAuthCheckDlg::OnBnClickedOk()
{
	UpdateData();
	OnOK();
}
