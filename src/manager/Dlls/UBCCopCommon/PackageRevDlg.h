#pragma once
#include "afxcmn.h"
#include "resource.h"
#include "eventhandler.h"

#include "common\ubcdefine.h"
#include "common\HoverButton.h"
//#include "common\utblistctrl.h"
#include "common\utblistctrlex.h"

// CPackageRevDlg dialog
class CPackageRevDlg : public CDialog, public IEventHandler
{
	DECLARE_DYNAMIC(CPackageRevDlg)
public:
	enum { eTermSite, eTermHostName, eTermHostId, eTermDisp, eRevPackage, eLastPackage, eCurPackage, eAutoPackage, eTermEnd };
	enum { eScheCheck, eScheName, eScheSTime, eScheETime, eScheEnd };

	void Set( ESrcApp app, LPCTSTR szSite, LPCTSTR szDrive, LPCTSTR szHost, LPCTSTR szPackage, LPCTSTR szScheSite
			, int nAuthority, LPCTSTR strUserId, LPCTSTR strSiteId, LPCTSTR strIniPath, bool& bMod, bool bCanAdd);
	EScheRev GetRet(CString&);
private:
	void InitTermList();
	void InitScheList();
	void SetDataTermList(CStringArray* pHostIdList);
	void SetDataScheList(LPCTSTR szSite, LPCTSTR szHost, int nSide);
	void RefreshPackageList(CString strFilter);
	void SelectHostList(int);

	int ieReserved(cciEntity* pEnt, cciAttributeList* pList);
	int ieRemoved(cciEntity* pEnt, cciAttributeList* pList);
	int ieExpired(cciEntity* pEnt, cciAttributeList* pList);
	int ieChanged(cciEntity* pEnt, cciAttributeList* pList);
public:
private:
	CString m_strCustomer;

	CString m_szUsrSite;
	CString m_szDrive;
	CString m_szScheSite;
	CString m_szHost;
	CString m_szPackage;
	CString m_szTitle;
	CString m_szTermTitle[eTermEnd];
	CString m_szScheTitle[eScheEnd];

	int		m_nAuthority;	// 권한
	CString m_strUserId;	
	CString	m_strSiteId;	// 로그인site
	CString	m_strIniPath;	// ini 경로

	CUTBListCtrlEx m_lscTerminal;
	CUTBListCtrlEx m_lscPackage;

//	CList<int> m_lsEvID;
	CEventManager m_EventManager;
	static UINT AddEventThread(LPVOID pParam);

	int m_nCurSide;
	CString m_szCurSite;
	CString m_szCurHost;

	ESrcApp m_SrcApp;
	BOOL m_bModify;
	bool* m_pModify;
	EScheRev m_RetMsg;

	bool m_bCanAdd;

public:
	CPackageRevDlg(LPCTSTR szCustomer, CWnd* pParent = NULL);   // standard constructor
	virtual ~CPackageRevDlg();
	virtual int InvokeEvent(WPARAM, LPARAM);

// Dialog Data
	enum { IDD = IDD_RESERVE_PACKAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonMod();
	afx_msg void OnBnClickedButtonDel();

	afx_msg void OnNMClickListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedListTerminal(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonSelectHost();
	afx_msg void OnEnChangeEbSearch();
};
