// PackageMngDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "UBCCopCommon.h"
#include "PackageMngDlg.h"
#include "copmodule.h"

#include <tlhelp32.h>
#include <shlwapi.h>
#include <winternl.h>

#define STR_SITE		LoadStringById(IDS_PACKAGEMNGDLG_LST001)
#define STR_PACKAGE		LoadStringById(IDS_PACKAGEMNGDLG_LST002)
#define STR_USER		LoadStringById(IDS_PACKAGEMNGDLG_LST003)
#define STR_TIME		LoadStringById(IDS_PACKAGEMNGDLG_LST004)
#define STR_DESCRIPT	LoadStringById(IDS_PACKAGEMNGDLG_LST005)

// CPackageMngDlg dialog
IMPLEMENT_DYNAMIC(CPackageMngDlg, CDialog)

CPackageMngDlg::CPackageMngDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPackageMngDlg::IDD, pParent)
{
	m_szSite = "";
}

CPackageMngDlg::~CPackageMngDlg()
{
}

void CPackageMngDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PACKAGE, m_lscPackage);
}

BEGIN_MESSAGE_MAP(CPackageMngDlg, CDialog)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDOK, &CPackageMngDlg::OnBnClickedOk)

	ON_COMMAND(ID_POPUP01_DELETE, OnPackageDelete)
	ON_COMMAND(ID_POPUP01_EDIT, OnPackageEdit)
	ON_COMMAND(ID_POPUP01_PLAY, OnPackagePlay)

	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_PACKAGE, OnLvnColumnclickList)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_PACKAGE, OnNMCustomdrawList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_PACKAGE, OnNMClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PACKAGE, OnNMDblclkList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_PACKAGE, OnNMRclickList)
END_MESSAGE_MAP()

// CPackageMngDlg message handlers
BOOL CPackageMngDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitPackageList();
	SetPackageData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPackageMngDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CPackageMngDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnClose();
}

void CPackageMngDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
//	PostMessage(WM_CLOSE);
	OnOK();
}

void CPackageMngDlg::InitPackageList()
{
	m_szColName[eSite] = STR_SITE;
	m_szColName[ePackage] = STR_PACKAGE;
	m_szColName[eUser] = STR_USER;
	m_szColName[eTime] = STR_TIME;
	m_szColName[eDesc] = STR_DESCRIPT;

	m_lscPackage.SetExtendedStyle(m_lscPackage.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	for(int nCol = 0; nCol < eEnd; nCol++){
		m_lscPackage.InsertColumn(nCol, m_szColName[nCol], LVCFMT_LEFT, 100);
	}

	m_lscPackage.SetColumnWidth(eSite, 60);
	m_lscPackage.SetColumnWidth(ePackage, 140);
	m_lscPackage.SetColumnWidth(eTime, 120);
	m_lscPackage.SetColumnWidth(eDesc, 120);

	m_lscPackage.InitHeader(IDB_UBCLISTCTR_HEAD);
}

void CPackageMngDlg::SetPackageData()
{
	PackageInfoList lsPackage;
	CCopModule::GetObject()->GetPackage(m_szSite, lsPackage);

	int nRow = 0;
	POSITION pos = lsPackage.GetHeadPosition();
	while(pos){
		POSITION posOld = pos;
		SPackageInfo info = lsPackage.GetNext(pos);

		m_lscPackage.InsertItem(nRow, info.szSiteID, eSite);

		m_lscPackage.SetItemText(nRow, ePackage, info.szPackage);
		m_lscPackage.SetItemText(nRow, eUser, info.szUpdateUser);
		m_lscPackage.SetItemText(nRow, eDesc, info.szDescript);

		CTime tm(info.tmUpdateTime);
		m_lscPackage.SetItemText(nRow, eTime, tm.Format(STR_DEF_TIME));

		m_lscPackage.SetItemData(nRow, (DWORD_PTR)posOld);
		nRow++;
	}
}

void CPackageMngDlg::Set(LPCTSTR szSite, LPCTSTR szUser, LPCTSTR szPasswd)
{
	m_szSite = szSite?szSite:"";
	m_szUser = szUser?szUser:"";
	m_szPasswd = szPasswd?szPasswd:"";
}

void CPackageMngDlg::OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

//	int nRow = pNMLV->iItem;
	int	nColumn = pNMLV->iSubItem;
	m_lscPackage.SetSortHeader(nColumn);
	m_lscPackage.SortItems(CompareList, (DWORD_PTR)this);
}

void CPackageMngDlg::OnNMCustomdrawList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
}

void CPackageMngDlg::OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
}

void CPackageMngDlg::OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
//	int	nColumn = pNMLV->iSubItem;
}

void CPackageMngDlg::OnNMRclickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_PACKAGEMNG));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);

	pPopup = menu.GetSubMenu(0);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

int CPackageMngDlg::CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	CPackageMngDlg* pDlg = (CPackageMngDlg*)lParam;
	CUTBListCtrl* pListCtrl = &pDlg->m_lscPackage;
	return pListCtrl->Compare(lParam1, lParam2);
}

void CPackageMngDlg::OnPackageDelete()
{
}

void CPackageMngDlg::OnPackageEdit()
{
	CString szPackage = m_lscPackage.GetItemText(m_ptSelList.y, ePackage);

	unsigned long lPid = getPid(UBCSTUDIOEEEXE_STR);
	HWND hWnd = getWHandle(lPid);

	if(hWnd){
		CWnd* pWnd = CWnd::FromHandle(hWnd);
		pWnd->ActivateTopParent();

		if(pWnd->IsIconic()){
			pWnd->ShowWindow(SW_RESTORE);
		}

		COPYDATASTRUCT cds;
		cds.dwData = (ULONG_PTR)eCDSetPackage;
		cds.cbData = szPackage.GetLength();
		cds.lpData = (PVOID)(LPCTSTR)szPackage;

		::SendMessage(hWnd, WM_COPYDATA, (WPARAM)GetSafeHwnd(), (LPARAM)&cds);

		return;
	}

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szLocation[MAX_PATH];
	_tsplitpath(szModule, szDrive, szLocation, NULL, NULL);

	CString szPath = "";
	szPath.Format("\"%s%s\"", szDrive, szLocation);

	CString szParam = "";
	szParam += " +siteid=";
	szParam += m_szSite;
	szParam += " +userid=";
	szParam += m_szUser;
	szParam += " +passwd=";
	szParam += m_szPasswd;
	szParam += " +package=";
	szParam += szPackage;

	HINSTANCE nRet = ShellExecute(NULL, NULL, UBCSTUDIOEEEXE_STR, szParam, szPath, SW_SHOWNORMAL);
}

void CPackageMngDlg::OnPackagePlay()
{
}

unsigned long CPackageMngDlg::getPid(const char* exename)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE ) {
		return 0;
	}
	PROCESSENTRY32 pe32 ;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	char strProcessName[512];

	if ( !Process32First ( hSnapshot, &pe32 ) )	{
		return 0;
	}
	

	do 	{
		//memset(strProcessName, 0, sizeof(strProcessName));
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if( stricmp(pe32.szExeFile, exename) == 0 )		{		
			return pe32.th32ProcessID;
		}
		
	} while (Process32Next( hSnapshot, &pe32 ));

	return 0;
}

HWND CPackageMngDlg::getWHandle(const char* exename)
{
	return getWHandle(getPid(exename));
}

BOOL CPackageMngDlg::find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if (!::IsWindowVisible(hwnd)) return TRUE;
	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);
	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;
	if (pe->pid != pid) return TRUE;
	pe->hwnd = hwnd;
	return FALSE;
}

HWND CPackageMngDlg::getWHandle(unsigned long pid)
{
	if(pid>0){
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);
		
		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1){
			parent =  ::GetParent(child);
			if(!parent){
				return child;
			}
			child = parent;
		}
		return e.hwnd;
	}
	return (HWND)0;
}