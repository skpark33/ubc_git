#pragma once

#include "common\HoverButton.h"
#include "Control\ReposControl2.h"
#include "Control\SplitStatic.h"

#include "common/UTBListCtrlEx.h"
#include "afxwin.h"

#ifndef IDD_PACKAGE_SELECT_DLG
#define IDD_PACKAGE_SELECT_DLG 25170
#endif


// CPackageSelectDlg 

class AFX_EXT_CLASS CPackageSelectDlg : public CDialog
{
	DECLARE_DYNAMIC(CPackageSelectDlg)

public:
	CPackageSelectDlg(LPCTSTR szCustomer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPackageSelectDlg();
	virtual BOOL OnInitDialog();

// 
	enum { IDD = IDD_PACKAGE_SELECT_DLG };

	enum { eCheck // 2010.02.17 by gwangsoo
		 , eSite, ePackage, eUser, eTime, eVolume, ePublic, eVerify
		 , eCategory, ePurpose, eHostType, eVertical, eResolution, eDesc
		 , eValidateDate
		 , eEnd };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()
 
	
	CString				m_strCustomer;
	CString					m_testWarningMsg;

	bool				m_bSingleSelect;
	CReposControl2		m_repos;
	int					m_nMovingSplitType;
	CRect				m_rectSizeMin;
	bool				m_bInitialize;

	PackageInfoList		m_listSelectedPackage;

	CStringArray		m_listSelectedContentsId;

	void				InitControl();
	void				InitData();
	void				ClearData();
	void				CheckScrollBarVisible(CUTBListCtrlEx& lc);
	void				GetFilterString(CString& strFilter);
	void				GetSelectItemsStringFromList(CUTBListCtrlEx& lc, LPCTSTR lpszField , CString& filter);
	void				GetSelectItemsStringFromList(CStringArray& ar, LPCTSTR lpszField , CString& filter);

	BOOL				GetProgramIdListIncludeContents(LPCTSTR lpszContentsId, CStringArray& listProgramId);
	BOOL				GetVerifyProgramIdList(bool bVerify, CStringArray& listProgramId);

public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnCbnSelchangeComboIncludedContents();
	afx_msg void OnNMClickFilterList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClickFilterList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickPackageList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClickPackageList(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg LRESULT OnMoveSpliter(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLButtonDownSpliter(WPARAM wParam, LPARAM lParam); //skpark 2013.3.27

	CHoverButton		m_btnRefresh;
	CEdit				m_editProgramId;
	CEdit				m_editDescription;
	CEdit				m_editLastUpdateId;
	CComboBox			m_cbxIncludedContents;
	CDateTimeCtrl		m_dtcStart;
	CDateTimeCtrl		m_dtcEnd;
	CUTBListCtrlEx	m_lcPublic;
	CUTBListCtrlEx	m_lcVerify;
	CUTBListCtrlEx	m_lcContentsCategory;
	CUTBListCtrlEx	m_lcPurpose;
	CUTBListCtrlEx	m_lcModel;
	CUTBListCtrlEx	m_lcDirection;
	CUTBListCtrlEx	m_lcResolution;
	CStatic				m_stcGroup;
	CSplitStatic		m_stcSplit;
	CUTBListCtrlEx		m_lcPackage;
	CHoverButton		m_btnSelect;
	CHoverButton		m_btnCancel;
	CButton				m_btnUser;

	void				SetSingleSelection(bool bSingleSelect) { m_bSingleSelect=bSingleSelect; };
	PackageInfoList&	GetSelectedPackageList() { return m_listSelectedPackage; };
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedBnUser();

	enum { eHostCheck, eHostSiteID, eHostHostID, eHostDisplayNo, eHostAutoSche, eHostCurSche /*,eHostCreator*/,eHostLastSche, /*eScheTime,*/ eHostExpEnd };

	CEdit m_editSelectedPackage;
	CUTBListCtrlEx m_lscHost;
	CStatic m_stcHost;
	CStatic m_stcSelected;
	CString m_loginId;
	CString m_envIni;
	int		m_lastMove;

public:
	CArray<SHostInfo> m_arHostList;
	bool isHostMerged() {return (m_arHostList.GetSize() > 0 ? true : false); }
	bool isTested(SPackageInfo& stPackageInfo);
	void				SetTestWarningMsg(CString& msg) { m_testWarningMsg = msg; }
	bool GetSelectedPackage(SPackageInfo& info);
	void SetEnvValue(CString user, CString ini) { m_loginId = user; m_envIni = ini;}

	void	InitHostList();
	void	InsertHostData();
	void	SaveFilterData();
	void	LoadFilterData();
	//skpark end
	afx_msg void OnBnClickedCheckDownloadonly();
	afx_msg void OnBnClickedUncheckDownloadonly();
	CButton m_checkDownloadOnly;
	CButton m_uncheckDownloadOnly;
	int		m_downloadOnly;

	int downloadOnlyCheck() { return m_downloadOnly; }
};
