#pragma once

#define UBCDEFAULT_FONT					_T("굴림")
#define UBCSTUDIOEXE_STR				_T("UBCStudio_kr.exe")
#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE_kr.exe")
#define UBCMANAGEREXE_STR				_T("UBCManager_kr.exe")

//CFtpMultiSite
#define FTPMULTISITE_STR001			_T("그룹 %s 에 접속중..")
#define FTPMULTISITE_STR002			_T("실패")
#define FTPMULTISITE_STR003			_T("오류")
#define FTPMULTISITE_STR004			_T("취소")
#define FTPMULTISITE_STR005			_T("성공")
#define FTPMULTISITE_STR006			_T("대기")
#define FTPMULTISITE_STR007			_T("업로드 중")
#define FTPMULTISITE_STR008			_T("다운로드 중")
#define FTPMULTISITE_STR009			_T("건너뜀")
#define FTPMULTISITE_STR010			_T("로컬")
#define FTPMULTISITE_STR011			_T("서버")
#define FTPMULTISITE_STR012			_T("알수없음")
#define FTPMULTISITE_STR013			_T("모두")

#define FTPMULTISITE_LST001			_T("소속그룹")
#define FTPMULTISITE_LST002			_T("파일명")
#define FTPMULTISITE_LST003			_T("파일위치")
#define FTPMULTISITE_LST004			_T("진행상태")
#define FTPMULTISITE_LST005			_T("로컬 파일크기")
#define FTPMULTISITE_LST006			_T("서버 파일크기")
#define FTPMULTISITE_LST007			_T("상태")
#define FTPMULTISITE_LST008			_T("메세지")
#define FTPMULTISITE_LST009			_T("서버 경로")

#define FTPMULTISITE_MSG001			_T(">> 그룹(%s) 에 접속이 실패하였습니다\n")
#define FTPMULTISITE_MSG002			_T(">> 그룹(%s) : %s\n")
#define FTPMULTISITE_MSG003			_T("> 그룹(%s) : %s\n")
#define FTPMULTISITE_MSG004			_T("그룹 : %s / 파일 : %s")
#define FTPMULTISITE_MSG005			_T("파일 업로드가 완료되었습니다")
#define FTPMULTISITE_MSG006			_T(">> 그룹(%s) 에 접속이 실패하였습니다\n")
#define FTPMULTISITE_MSG007			_T("파일 다운로드가 완료되었습니다")
#define FTPMULTISITE_MSG008			_T("사용자에 의한 취소")
#define FTPMULTISITE_MSG009			_T("파일 사이즈가 다릅니다 Server [%s], Local [%s]")
#define FTPMULTISITE_MSG010			_T("이미 파일이 있으므로 다운로드하지 않습니다")
#define FTPMULTISITE_MSG011			_T("파일 다운로드가 실패하였습니다")
#define FTPMULTISITE_MSG012			_T("파일 업로드가 실패하였습니다")

//CPackageMngDlg
#define PACKAGEMNGDLG_LST001		_T("그룹")
#define PACKAGEMNGDLG_LST002		_T("콘텐츠 패키지")
#define PACKAGEMNGDLG_LST003		_T("사용자")
#define PACKAGEMNGDLG_LST004		_T("시간")
#define PACKAGEMNGDLG_LST005		_T("설명")

//CPackageRevAddDlg
#define PACKAGEREVADDDLG_STR001	_T("콘텐츠 패키지 예약")
#define PACKAGEREVADDDLG_STR002	_T("적용된 단말")
#define PACKAGEREVADDDLG_STR003	_T("예약된 단말")

#define PACKAGEREVADDDLG_MSG001	_T("시작시간을 입력하세요.")
#define PACKAGEREVADDDLG_MSG002	_T("시작시간은 현재시간보다 5분 늦게 설정해야 합니다.")
#define PACKAGEREVADDDLG_MSG003	_T("종료시간은 시작시간보다 적어도 5분 후로 설정해야 합니다.")
#define PACKAGEREVADDDLG_MSG004	_T("콘텐츠 패키지가 변경된 상태입니다.\n콘텐츠 패키지를 적용하면 서버에 있는 콘텐츠 패키지가 변경됩니다\n서버에 있는 콘텐츠 패키지를 변경하지 않으려면 새이름으로 저장 하시기 바랍니다\n새이름으로 저장 하시겠습니까?")
#define PACKAGEREVADDDLG_MSG005	_T("잘못된 시작시간 입니다.\n시작시간을 다른 터미널에 적용된 시간보다 늦게 설정해야 합니다")

//CPackageRevDlg
#define PACKAGEREVDLG_LST001		_T("그룹")
#define PACKAGEREVDLG_LST002		_T("단말")
#define PACKAGEREVDLG_LST003		_T("디스플레이")
#define PACKAGEREVDLG_LST004		_T("예약된 콘텐츠 패키지")
#define PACKAGEREVDLG_LST005		_T("최근 콘텐츠 패키지")
#define PACKAGEREVDLG_LST006		_T("현재 콘텐츠 패키지")
#define PACKAGEREVDLG_LST007		_T("자동설정 콘텐츠 패키지")
#define PACKAGEREVDLG_LST008		_T("콘텐츠 패키지")
#define PACKAGEREVDLG_LST009		_T("시작시간")
#define PACKAGEREVDLG_LST010		_T("종료시간")

#define PACKAGEREVDLG_STR001		_T("콘텐츠 패키지 예약")
#define PACKAGEREVDLG_STR002		_T("콘텐츠 패키지 추가")
#define PACKAGEREVDLG_STR003		_T("콘텐츠 패키지 편집")
#define PACKAGEREVDLG_STR004		_T("콘텐츠 패키지 삭제")

#define PACKAGEREVDLG_MSG001		_T("단말을 선택하지 않았습니다.\n단말 선택후 다시 시도하여주십시요")
#define PACKAGEREVDLG_MSG002		_T("콘텐츠 패키지를 선택하지 않았습니다.\n콘텐츠 패키지 선택후 다시 시도하여주십시요")
#define PACKAGEREVDLG_MSG003		_T("콘텐츠 패키지를 선택하지 않았습니다.\n콘텐츠 패키지 선택후 다시 시도하여주십시요")
#define PACKAGEREVDLG_MSG004		_T("선택된 콘텐츠 패키지를 삭제하시겠습니까?")
#define PACKAGEREVDLG_MSG005		_T("그룹 %s 는 등록 되어 있지 않습니다. 그룹 %s 를 등록후 다시 시도하세요.")

//CSiteInfoDlg
#define SITEINFODLG_STR001			_T("공휴일")

#define SITEINFODLG_MSG001			_T("그룹을 입력하십시오")
#define SITEINFODLG_MSG002			_T("그룹ID 에는 영문과 숫자만 입력가능합니다")

//CFlashView
#define FLASHVIEW_MSG001			_T("플래쉬 콘트롤이 설치되지 않았거나 지원하지 않는 버젼입니다\n플래쉬 다운로드 페이지로 이동 하시겠습니까?")

//CAuthCheckDlg
#define AUTHCHECK_MSG001			_T("인증에 실패하였습니다.\nenterprise key 와 enterprise password 를 다시 한번 확인해 주시기 바랍니다")

//CSelectHostDlg
#define SELECTHOST_STR001				_T("사용")
#define SELECTHOST_STR002				_T("미사용")
#define SELECTHOST_STR003				_T("접속")
#define SELECTHOST_STR004				_T("미접속")

#define SELECTHOST_LIST001				_T("단말이름")
#define SELECTHOST_LIST002				_T("그룹")

#define SELECTHOST_BTN001				_T("단말 리스트 새로고침")

#define SELECTHOST_MSG001				_T("적용 단말을 선택하세요")

//CMultiSelectHost
#define MULTISELECTHOST_BTN001			_T("단말 리스트 새로고침")

#define MULTISELECTHOST_STR001			_T("사용")
#define MULTISELECTHOST_STR002			_T("미사용")
#define MULTISELECTHOST_STR003			_T("접속")
#define MULTISELECTHOST_STR004			_T("미접속")
#define MULTISELECTHOST_STR005			_T("All")

#define MULTISELECTHOST_LIST001			_T("단말이름")
#define MULTISELECTHOST_LIST002			_T("그룹")

#define MULTISELECTHOST_MSG001			_T("적용 단말을 선택하세요")
