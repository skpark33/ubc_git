#pragma once

//#include "resource.h"
#ifndef IDD_HOSTINFO
#define IDD_HOSTINFO	25002
#endif//IDD_HOSTINFO

#include "copmodule.h"
#include "common/HoverButton.h"
#include "common/utblistctrl.h"

#define STR_ADMIN_ON		_T("Use")
#define STR_ADMIN_OFF		_T("Not use")
#define STR_OPERA_ON		_T("Connected")
#define STR_OPERA_OFF		_T("Disconnected")
#define STR_NET_TRUE		_T("Server")
#define STR_NET_FALSE		_T("Local")

// CHostInfoDlg dialog
class AFX_EXT_CLASS CHostInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CHostInfoDlg)
public:
	void SetInfo(const SHostInfo&);
	void GetInfo(SHostInfo&);
private:
public:
private:
	CString m_szPM;
	CString m_szSite;
	CString m_szHost;
	CString m_szIP;
	CString m_szMac;
	CString m_szOpState;
	CComboBox m_cbAdState;
	CDateTimeCtrl m_dtcAuthDate;
	CDateTimeCtrl m_dtcUpDate;
	CString m_szVendor;
	short m_szMonitorType;
	CString m_szModel;
	CString m_szOS;
	CString m_szSerialNo;
	CString m_szPeriod;
	CString m_szEdition;
	CComboBox m_cbDisplayCnt;
	CString m_szDescription;
	CString m_szAuto1;
	CString m_szAuto2;
	CString m_szCurrent1;
	CString m_szCurrent2;
	CString m_szLast1;
	CString m_szLast2;
	CString m_szNetwork1;
	CString m_szNetwork2;

	SHostInfo m_HostInfo;

public:
	CHostInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CHostInfoDlg();

// Dialog Data
	enum { IDD = IDD_HOSTINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedOk();
};
