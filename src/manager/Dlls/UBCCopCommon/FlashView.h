#pragma once

#include "resource.h"
#include "common\flashctrl.h"

// CFlashView dialog
class CFlashView : public CDialog
{
	DECLARE_DYNAMIC(CFlashView)

public:
	CFlashView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFlashView();

// Dialog Data
	enum { IDD = IDD_FLASHVIEW };

	void Set(LPCTSTR, LPCTSTR);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	CString m_szSiteId;
	CString m_szHostId;
	CString m_szIP;
	CString m_Files;
	CFlashCtrl m_FlashView;

	void getScreenShot1(CString args);

//	bool FindShowSameWindow(CString strText);

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
//	afx_msg void OnDestroy();
//	afx_msg void OnClose();
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	DECLARE_EVENTSINK_MAP()
	void FSCommandFlashCtrl(LPCTSTR command, LPCTSTR args);
};
