#pragma once
#include "afxwin.h"
#include "resource.h"
#include "CopModule.h"

// CCustomerInfoDlg 대화 상자입니다.

class CCustomerInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CCustomerInfoDlg)

	CustomerInfoList m_listCustumerInfo;
	CString m_strCustumerName;
	int		m_nCustumerTimezone;
	CString m_strCustumerLanguage;

public:
	CCustomerInfoDlg(CString strCustomerInfo, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCustomerInfoDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CUSTOMER_INFO_DLG };

	CString GetSelectCustumerName() { return m_strCustumerName; }
	int		GetSelectCustumerTimezone() { return m_nCustumerTimezone; }
	CString GetSelectCustumerLanguage() { return m_strCustumerLanguage; }

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CComboBox m_cbCustomerInfo;
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeCbCustomerInfo();
};
