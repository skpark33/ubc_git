#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "common\HoverButton.h"
//#include "common\utblistctrl.h"
#include "resource.h"
#include "CopModule.h"

// CCustomerListDlg 대화 상자입니다.

class CCustomerListDlg : public CDialog
{
	DECLARE_DYNAMIC(CCustomerListDlg)

public:
	CCustomerListDlg(CString strCustomerInfo,CString user, int auth, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCustomerListDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CUSTOMER_LIST_DLG };
	enum { eCreate, eSave, eDelete };


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonRef();
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnNMDblclkListCustomer(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListCustomer(NMHDR *pNMHDR, LRESULT *pResult);

	CString GetSelectCustomerName() { return m_strCustomerName; }
	int		GetSelectCustomerTimezone() { return m_nCustomerTimezone; }
	CString GetSelectCustomerLanguage() { return m_strCustomerLanguage; }

	void SetUser(CString user, int auth) { m_szLoginID=user; m_Authority=auth;}
	void InitList();
	void SetList();

private:
	enum {/* eCheck,*/ eCustomer,eLanguage,eTimeZone, /*eDescription, eURL, */ eEnd };

	CHoverButton m_btAdd;
	CHoverButton m_btRef;

	CEdit m_editCustomer;
	CListCtrl m_lscCustomer;
	CString m_strCustomerName;
	int		m_nCustomerTimezone;
	CString m_strCustomerLanguage;

	CustomerInfoList m_listCustomerInfo;
	CString m_ColumTitle[eEnd];

	CString m_szLoginID;
	int m_Authority;

public:
};
