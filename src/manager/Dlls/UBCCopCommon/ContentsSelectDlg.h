#pragma once

#include "common\HoverButton.h"
#include "Control\ReposControl2.h"
#include "Control\SplitStatic.h"

#include "common/UTBListCtrlEx.h"
#include "afxcmn.h"

#ifndef IDD_CONTENTS_SELECT_DLG
#define IDD_CONTENTS_SELECT_DLG 25186
#endif


///////////////////////////////////////////////////////////////////////////////
// CContentsSelectDlg 대화 상자입니다.

class AFX_EXT_CLASS CContentsSelectDlg : public CDialog
{
	DECLARE_DYNAMIC(CContentsSelectDlg)

public:
	CContentsSelectDlg(LPCTSTR szCustomer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CContentsSelectDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CONTENTS_SELECT_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CString				m_strCustomer;

	bool				m_bSingleSelect;
	CReposControl2		m_repos;
	int					m_nMovingSplitType;
	CRect				m_rectSizeMin;
	bool				m_bInitialize;

	CONTENTS_INFO_EX_LIST		m_listSelectedContents;

	void				InitControl();
	void				InitData();
	void				ClearData();
	void				CheckScrollBarVisible(CUTBListCtrlEx& lc);
	void				GetFilterString(CString& strFilter);
	void				GetSelectItemsStringFromList(CUTBListCtrlEx& lc, LPCTSTR lpszField , CString& filter);
	void				GetSelectItemsStringFromList(CStringArray& ar, LPCTSTR lpszField , CString& filter);

	void				GetContents(LPCTSTR szSite, CONTENTS_INFO_EX_LIST& lsContents, LPCTSTR szWhere = NULL);

public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnNMClickFilterList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClickFilterList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickContentsList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClickContentsList(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg LRESULT OnMoveSpliter(WPARAM wParam, LPARAM lParam);

	CHoverButton		m_btnRefresh;
	CEdit				m_editContentsName;
	CEdit				m_editCategory;
	CEdit				m_editRegister;
	CEdit				m_editRequester;
	CEdit				m_editFilename;
	CDateTimeCtrl		m_dtcStart;
	CDateTimeCtrl		m_dtcEnd;
	CUTBListCtrlEx		m_lcContentsType;
	CUTBListCtrlEx		m_lcPublic;
	CUTBListCtrlEx		m_lcVerify;
	CUTBListCtrlEx		m_lcContentsCategory;
	CUTBListCtrlEx		m_lcPurpose;
	CUTBListCtrlEx		m_lcModel;
	CUTBListCtrlEx		m_lcDirection;
	CUTBListCtrlEx		m_lcResolution;

	CStatic				m_stcGroup;
	CSplitStatic		m_stcSplit;
	CUTBListCtrlEx		m_lcContents;
	CHoverButton		m_btnSelect;
	CHoverButton		m_btnCancel;

	void				SetSingleSelection(bool bSingleSelect) { m_bSingleSelect=bSingleSelect; };
	CONTENTS_INFO_EX_LIST&	GetSelectedContentsIdList() { return m_listSelectedContents; };
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
