// SiteSelectDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "SiteSelectDlg.h"

#define WM_REFRESH_LIST WM_USER+100


// CKeyIgnoreTreeCtrl

IMPLEMENT_DYNAMIC(CKeyIgnoreTreeCtrl, CTreeCtrl)

BEGIN_MESSAGE_MAP(CKeyIgnoreTreeCtrl, CTreeCtrl)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
END_MESSAGE_MAP()

// CKeyIgnoreTreeCtrl 메시지 처리기입니다.

void CKeyIgnoreTreeCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(nChar == VK_SPACE) return;
	CTreeCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CKeyIgnoreTreeCtrl::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(nChar == VK_SPACE) return;
	CTreeCtrl::OnKeyUp(nChar, nRepCnt, nFlags);
}



// CSiteSelectDlg 대화 상자입니다.

CMapStringToPtr CSiteSelectDlg::m_mapChildSiteInfo;

CWinThread*	CSiteSelectDlg::m_pQueryThread = NULL;
HWND		CSiteSelectDlg::m_hReplyWnd = NULL;

IMPLEMENT_DYNAMIC(CSiteSelectDlg, CDialog)

CSiteSelectDlg::CSiteSelectDlg(LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CSiteSelectDlg::IDD, pParent)
	, m_bSingleSelect ( false )
	, m_repos ( this )
	, m_strCustomer ( szCustomer )
{

}

CSiteSelectDlg::~CSiteSelectDlg()
{
}

void CSiteSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_SITE, m_tcSite);
	DDX_Control(pDX, IDOK, m_btnSelect);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BN_REFRESH_ALL, m_btnRefreshAll);
}

BEGIN_MESSAGE_MAP(CSiteSelectDlg, CDialog)
	ON_WM_SIZE()
	ON_NOTIFY(NM_CLICK, IDC_TREE_SITE, OnNMClickTreeSite)
	ON_NOTIFY(NM_DBLCLK, IDC_TREE_SITE, OnNMDblclkTreeSite)
	ON_BN_CLICKED(IDC_BN_REFRESH_ALL, &CSiteSelectDlg::OnBnClickedBnRefreshAll)
	ON_NOTIFY(TVN_ITEMEXPANDED, IDC_TREE_SITE, &CSiteSelectDlg::OnTvnItemexpandedTreeSite)
	ON_MESSAGE(WM_REFRESH_LIST, OnRefreshList)
	ON_BN_CLICKED(IDOK, &CSiteSelectDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CSiteSelectDlg 메시지 처리기입니다.

BOOL CSiteSelectDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitControl();
	RefreshTree();

	PrepareSiteList();

	m_hReplyWnd = GetSafeHwnd();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSiteSelectDlg::RefreshTree()
{
	m_tcSite.DeleteAllItems();

	m_tcSite.SetRedraw(FALSE);

	HTREEITEM hRootItem = NULL;
	//if(m_strCustomer == _T("HYUNDAI"))
	//{
	//	hRootItem = InsertRootItem();
	//	InsertTreeItem(m_tcSite.InsertItem(_T("지역본부"), hRootItem), m_strCustomer, _T("지역본부"), 1);
	//	InsertTreeItem(m_tcSite.InsertItem(_T("서비스센터"), hRootItem), m_strCustomer, _T("서비스센터"), 1);
	//	InsertTreeItem(m_tcSite.InsertItem(_T("출고센터"), hRootItem), m_strCustomer, _T("출고센터"), 1);
	//}
	//else if(m_strCustomer == _T("KIA"))
	//{
	//	hRootItem = InsertRootItem();
	//	InsertTreeItem(hRootItem, m_strCustomer, _T(""), 1);
	//}
	//else
	//{
	//	InsertTreeItem(NULL, _T(""), _T(""), 1);
	//}

	if(!m_strCustomer.IsEmpty())
	{
		hRootItem = InsertRootItem();
	}

	InsertTreeItem(hRootItem, m_strCustomer, 1);

	m_tcSite.Expand(hRootItem, TVE_EXPAND);

	m_tcSite.SetRedraw(TRUE);
}

HTREEITEM CSiteSelectDlg::InsertRootItem()
{
	SiteInfoList* pSiteInfoList = GetChildSiteInfo(_T(""));
	if(!pSiteInfoList) return NULL;

	POSITION pos = pSiteInfoList->GetHeadPosition();
	for (int i = 0; i < pSiteInfoList->GetCount(); i++)
	{
		SSiteInfo& info = pSiteInfoList->GetNext(pos);
		if(info.siteId == m_strCustomer)
		{
			HTREEITEM itemNew = m_tcSite.InsertItem(info.siteName);
			m_tcSite.SetItemData(itemNew, (DWORD_PTR)&info);
			return itemNew;
		}
	}

	return NULL;
}

void CSiteSelectDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CSiteSelectDlg::OnOK()
{
	m_listSelectSiteInfo.RemoveAll();
	GetAllSelection(m_listSelectSiteInfo);

	SSiteInfo info;
	GetSelectSiteInfo(info);

	CDialog::OnOK();
//	DeleteAllItems();
}

void CSiteSelectDlg::OnCancel()
{
	CDialog::OnCancel();
//	DeleteAllItems();
}

void CSiteSelectDlg::_DeleteAllItems(HTREEITEM item)
{
	while(item)
	{
		//
		SSiteInfo* info = (SSiteInfo*)m_tcSite.GetItemData(item);
		if(info) delete info;

		//
		if(m_tcSite.ItemHasChildren(item))
		{
			HTREEITEM child_item = m_tcSite.GetChildItem(item);
			_DeleteAllItems(child_item);
		}

		//
		item = m_tcSite.GetNextSiblingItem(item);
	}
}

void CSiteSelectDlg::DeleteAllItems()
{
	HTREEITEM item = m_tcSite.GetRootItem();

	_DeleteAllItems(item);

	m_tcSite.DeleteAllItems();
}

void CSiteSelectDlg::_SetAllSelection(HTREEITEM itemCurrent, HTREEITEM item, BOOL bCheck)
{
	while(item)
	{
		//
		if(itemCurrent != item)
		{
			m_tcSite.SetCheck(item, bCheck);
			if(bCheck) m_tcSite.Expand(item, TVE_EXPAND);
			//m_tcSite.SetItemState(item, NULL, TVIS_SELECTED);
		}

		//
		if(m_tcSite.ItemHasChildren(item))
		{
			HTREEITEM child_item = m_tcSite.GetChildItem(item);
			m_tcSite.Expand(item, TVE_EXPAND);
			_SetAllSelection(itemCurrent, child_item, bCheck);
		}

		//
		item = m_tcSite.GetNextSiblingItem(item);
	}
}

void CSiteSelectDlg::SetAllSelection(HTREEITEM itemCurrent, BOOL bCheck)
{
	HTREEITEM item = m_tcSite.GetRootItem();

	_SetAllSelection(itemCurrent, item, bCheck);
}

HTREEITEM CSiteSelectDlg::_GetParentItem(HTREEITEM item, LPCSTR lpszParentId)
{
	while(item)
	{
		//
		SSiteInfo* info = (SSiteInfo*)m_tcSite.GetItemData(item);
		if(info->siteId == lpszParentId)
			return item;

		//
		if(m_tcSite.ItemHasChildren(item))
		{
			HTREEITEM child_item = m_tcSite.GetChildItem(item);
			HTREEITEM ret_item = _GetParentItem(child_item, lpszParentId);
			if(ret_item) return ret_item;
		}

		//
		item = m_tcSite.GetNextSiblingItem(item);
	}
	return NULL;
}

HTREEITEM CSiteSelectDlg::GetParentItem(LPCSTR lpszParentId)
{
	HTREEITEM item = m_tcSite.GetRootItem();

	return _GetParentItem(item, lpszParentId);
}

void CSiteSelectDlg::_GetAllSelection(SiteInfoList& siteInfoList, HTREEITEM item)
{
	while(item)
	{
		//
		SSiteInfo* info = (SSiteInfo*)m_tcSite.GetItemData(item);
		if(m_tcSite.GetCheck(item) && info)
		{
			siteInfoList.AddTail(*info);
		}

		//
		if(m_tcSite.ItemHasChildren(item))
		{
			HTREEITEM child_item = m_tcSite.GetChildItem(item);
			_GetAllSelection(siteInfoList, child_item);
		}

		//
		item = m_tcSite.GetNextSiblingItem(item);
	}
}

void CSiteSelectDlg::GetAllSelection(SiteInfoList& siteInfoList)
{
	HTREEITEM item = m_tcSite.GetRootItem();

	return _GetAllSelection(siteInfoList, item);
}

void CSiteSelectDlg::InitControl()
{
	GetDlgItem(IDC_KB_INCLUDE_CHILD)->EnableWindow(!m_bSingleSelect);

	m_repos.AddControl((CWnd*)&m_tcSite, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnRefreshAll, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnSelect, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnCancel, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	m_btnSelect.LoadBitmap(IDB_BUTTON_SELECT, RGB(255,255,255));
	m_btnCancel.LoadBitmap(IDB_BUTTON_CANCEL, RGB(255,255,255));
	m_btnRefreshAll.LoadBitmap(IDB_BUTTON_REFRESH2, RGB(255,255,255));
}

SiteInfoList* CSiteSelectDlg::GetChildSiteInfo(CString strParentId, BOOL bCallServer)
{
	SiteInfoList* pSiteInfoList = NULL;

	CString strKey = strParentId;
	if(strKey.IsEmpty()) strKey = _T("NULL");

	if(!m_mapChildSiteInfo.Lookup(strKey, (void*&)pSiteInfoList))
	{
		if(bCallServer)
		{
			CString strWhere;
			if(strParentId.IsEmpty()) strWhere = _T("parentId is NULL");
			else                      strWhere.Format(_T("parentId = '%s'"), strParentId);

			strWhere += _T(" order by siteName");
			strWhere.Trim();

			SSiteInfo site;
			site.siteId = _T("*");

			pSiteInfoList = new SiteInfoList;
			if(CCopModule::GetObject()->GetSite(site, *pSiteInfoList, strWhere, FALSE))
			{
				m_mapChildSiteInfo[strKey] = pSiteInfoList;
			}
			else
			{
				delete pSiteInfoList;
				pSiteInfoList = NULL;
			}
		}
	}

	return pSiteInfoList;
}

int CSiteSelectDlg::InsertTreeItem(HTREEITEM itemParent, CString strParentId, int nCallDepth)
{
	while(m_tcSite.ItemHasChildren(itemParent))
	{
		m_tcSite.DeleteItem(m_tcSite.GetChildItem(itemParent));
	}

	SiteInfoList* pSiteInfoList = GetChildSiteInfo(strParentId, (nCallDepth > 0));
	if(!pSiteInfoList) return 0;

	int nResult = 0;

	POSITION pos = pSiteInfoList->GetHeadPosition();
	for (int i = 0; i < pSiteInfoList->GetCount(); i++)
	{
		SSiteInfo& info = pSiteInfoList->GetNext(pos);

//		if(!strBusinessType.IsEmpty() && strBusinessType != info.businessType) continue;

		HTREEITEM itemNew = m_tcSite.InsertItem(info.siteName, itemParent, TVI_SORT);
		m_tcSite.SetItemData(itemNew, (DWORD_PTR)&info);

		nResult++;

		int nChildCnt = InsertTreeItem(itemNew, info.siteId, nCallDepth-1);
		//if(nChildCnt <= 0)
		//{
		//	m_tcSite.InsertItem(_T(""), itemNew, TVI_SORT);
		//}
	}

	m_tcSite.RedrawWindow();

	return nResult;
}

void CSiteSelectDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if(GetSafeHwnd())
	{
		m_repos.MoveControl(TRUE);
		Invalidate(TRUE);
	}
}

bool CSiteSelectDlg::GetSelectSiteInfo(SSiteInfo& siteInfo)
{
	if(m_listSelectSiteInfo.GetCount() > 0)
	{
		POSITION pos = m_listSelectSiteInfo.GetHeadPosition();
		siteInfo = m_listSelectSiteInfo.GetNext(pos);
		return true;
	}
	return false;
}

bool CSiteSelectDlg::GetSelectSiteInfoList(SiteInfoList& siteInfoList)
{
	if(m_listSelectSiteInfo.GetCount() > 0)
	{
		POSITION pos = m_listSelectSiteInfo.GetHeadPosition();
		for (int i=0; i<m_listSelectSiteInfo.GetCount(); i++)
		{
			siteInfoList.AddTail(m_listSelectSiteInfo.GetNext(pos));
		}
		return true;
	}
	return false;
}

void CSiteSelectDlg::OnNMClickTreeSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	//*pResult = 1; // 무시하고 싶으면 셋팅
	*pResult = 0;

	CTreeCtrl* pTreeCtrl = (CTreeCtrl*)CWnd::FromHandle(pNMHDR->hwndFrom);
	if(!pTreeCtrl) return;

	UINT flags;
	DWORD pos = GetMessagePos();
	CPoint point(LOWORD(pos), HIWORD(pos));
	pTreeCtrl->ScreenToClient(&point);
	HTREEITEM hItem = pTreeCtrl->HitTest(point, &flags);

	if(!hItem) return;

	if(hItem && (flags & TVHT_ONITEMSTATEICON))
	{
		int nIncludeChild = ((CButton*)GetDlgItem(IDC_KB_INCLUDE_CHILD))->GetCheck();

		if(m_bSingleSelect)
		{
			SetAllSelection(hItem, FALSE);
			//m_tcSite.SetItemState(hItem, TVIS_SELECTED, TVIS_SELECTED);
		}
		// 왼쪽 컨트롤키가 눌린상태에서 체크박스에 클릭하는 경우 하위조직 모두 체크
		else if(0x8000 == (::GetKeyState(VK_LCONTROL) & 0x8000) || nIncludeChild)
		{
			BOOL bCheck = !m_tcSite.GetCheck(hItem);
			m_tcSite.SetCheck(hItem, bCheck);
			m_tcSite.Expand(hItem, TVE_EXPAND);
			_SetAllSelection(NULL, m_tcSite.GetChildItem(hItem), bCheck);
			*pResult = 1;
		}
	}
}

void CSiteSelectDlg::OnNMDblclkTreeSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	*pResult = 0;

	CTreeCtrl* pTreeCtrl = (CTreeCtrl*)CWnd::FromHandle(pNMHDR->hwndFrom);
	if(!pTreeCtrl) return;

	UINT flags;
	DWORD pos = GetMessagePos();
	CPoint point(LOWORD(pos), HIWORD(pos));
	pTreeCtrl->ScreenToClient(&point);
	HTREEITEM hItem = pTreeCtrl->HitTest(point, &flags);
	if(!hItem) return;

	m_tcSite.SetRedraw(FALSE);

	if(hItem && (flags & TVHT_ONITEMSTATEICON))
	{
		if(m_bSingleSelect)
		{
			SetAllSelection(hItem, FALSE);
			//m_tcSite.SetItemState(hItem, TVIS_SELECTED, TVIS_SELECTED);
		}
		else
		{
			BOOL bCheck = m_tcSite.GetCheck(hItem);
			m_tcSite.SetCheck(hItem, bCheck);
			m_tcSite.Expand(hItem, TVE_EXPAND);
			_SetAllSelection(NULL, m_tcSite.GetChildItem(hItem), bCheck);
			*pResult = 1;
		}
	}
	else
	{
		SSiteInfo* pInfo = (SSiteInfo*)m_tcSite.GetItemData(hItem);
		if(pInfo && !m_tcSite.GetChildItem(hItem))
		{
			CWaitMessageBox wait;

			InsertTreeItem(hItem, pInfo->siteId, 1);
		}
		*pResult = 1;
	}

	m_tcSite.SetRedraw(TRUE);
}

void CSiteSelectDlg::OnTvnItemexpandedTreeSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	*pResult = 0;

	HTREEITEM hItem = pNMTreeView->itemNew.hItem;
	if(!hItem) return;

	CWaitMessageBox wait;

	SSiteInfo* pInfo = (SSiteInfo*)m_tcSite.GetItemData(hItem);
	//if(pInfo && !m_tcSite.GetChildItem(hItem))
	if(pInfo)
	{
		InsertTreeItem(hItem, pInfo->siteId, 1);
	}
}

void CSiteSelectDlg::OnBnClickedBnRefreshAll()
{
	if(UbcMessageBox(LoadStringById(IDS_SITESELECTDLG_MSG001), MB_YESNO) != IDYES) return;

	CWaitMessageBox wait;

	POSITION pos = m_mapChildSiteInfo.GetStartPosition();
	while(pos)
	{
		CString strKey;
		SiteInfoList* pSiteInfoList = NULL;
		m_mapChildSiteInfo.GetNextAssoc(pos, strKey, (void*&)pSiteInfoList);
		if(pSiteInfoList) delete pSiteInfoList;
	}
	m_mapChildSiteInfo.RemoveAll();

	SSiteInfo site;
	site.siteId = _T("*");
	SiteInfoList listAll;
	if(CCopModule::GetObject()->GetSite(site, listAll, _T(""), FALSE))
	{
		POSITION pos = listAll.GetHeadPosition();
		for (int i = 0; i < listAll.GetCount(); i++)
		{
			SSiteInfo& info = listAll.GetNext(pos);

			CString strKey = info.parentId;
			if(strKey.IsEmpty()) strKey = _T("NULL");
			SiteInfoList* pSiteInfoList = NULL;

			if(!m_mapChildSiteInfo.Lookup(strKey, (void*&)pSiteInfoList))
			{
				pSiteInfoList = new SiteInfoList;
				m_mapChildSiteInfo[strKey] = pSiteInfoList;
			}

			pSiteInfoList->AddTail(info);
		}
	}

	RefreshTree();
}

void CSiteSelectDlg::PrepareSiteList()
{
	if(!m_pQueryThread)
	{
		m_pQueryThread = ::AfxBeginThread(CSiteSelectDlg::QueryThread, NULL, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pQueryThread->m_bAutoDelete = TRUE;
		m_pQueryThread->ResumeThread();
	}
}

UINT CSiteSelectDlg::QueryThread(LPVOID pParam)
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	SSiteInfo site;
	site.siteId = _T("*");
	SiteInfoList listAll;
	if(CCopModule::GetObject()->GetSite(site, listAll, _T(""), FALSE))
	{
		POSITION pos = listAll.GetHeadPosition();
		for (int i = 0; i < listAll.GetCount(); i++)
		{
			SSiteInfo& info = listAll.GetNext(pos);

			CString strKey = info.parentId;
			if(strKey.IsEmpty()) strKey = _T("NULL");
			SiteInfoList* pSiteInfoList = NULL;

			if(!CSiteSelectDlg::m_mapChildSiteInfo.Lookup(strKey, (void*&)pSiteInfoList))
			{
				pSiteInfoList = new SiteInfoList;
				CSiteSelectDlg::m_mapChildSiteInfo[strKey] = pSiteInfoList;
			}
			else if(pSiteInfoList && pSiteInfoList->Find(info))
			{
				continue;
			}

			pSiteInfoList->AddTail(info);
		}
	}

	if(::IsWindow(m_hReplyWnd)) ::PostMessage(m_hReplyWnd, WM_REFRESH_LIST, 0, 0);

	::CoUninitialize();

	return 0;
}

LRESULT	CSiteSelectDlg::OnRefreshList(WPARAM wParam, LPARAM lParam)
{
	RefreshTree();
	return 0;
}
