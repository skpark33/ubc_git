// FlashView.cpp : implementation file
//

#include "stdafx.h"
#include "UBCCopCommon.h"
#include "FlashView.h"
#include "CopModule.h"
#include "common/TraceLog.h"
#include "ubccommon\ubccommon.h"
#include "common\libCommon\ubcMuxRegacy.h"
#include "common\libcommon\ubchost.h"
#include "common/libCommon/ubcIni.h"

#define TM_CLOSEDLG			1

// CFlashView dialog
IMPLEMENT_DYNAMIC(CFlashView, CDialog)

CFlashView::CFlashView(CWnd* pParent /*=NULL*/)
	: CDialog(CFlashView::IDD, pParent)
{
}

CFlashView::~CFlashView()
{
}

void CFlashView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FLASH_CTRL, m_FlashView);
}

BEGIN_MESSAGE_MAP(CFlashView, CDialog)
//	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_TIMER()
//	ON_WM_CLOSE()
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()

// CFlashView message handlers
BOOL CFlashView::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString strWindowText;
	GetWindowText(strWindowText);
	strWindowText += " " + m_szHostId;

	//if(FindShowSameWindow(strWindowText))
	//{
	//	EndDialog(IDOK);
	//	return FALSE;
	//}

	SetWindowText(strWindowText);

	// TODO:  Add extra initialization here
	CString szBuf;

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, NULL, NULL);

	long lver = m_FlashView.FlashVersion();
	short maj = HIWORD(lver);
	short min = LOWORD(lver);
	TraceLog(("Flash version %d.%d",maj,min));

	//if(maj < 10)
	//{
	//	if(UbcMessageBox(LoadStringById(IDS_FLASHVIEW_MSG001), MB_ICONWARNING|MB_YESNO)==IDYES)
	//	{
	//		OpenIExplorer(_T("http://www.adobe.com/go/getflash"), 860);
	//	}
	//	SetTimer(TM_CLOSEDLG, 100, NULL);
	//}
	//else
	{
		ubcConfig aIni("UBCVariables");
		ciLong val = 0;
		aIni.get("ROOT", "ENABLE_SHORT_SCREENSHOT_PERIOD", val);

		szBuf.Format("%s%s", szDrive, szPath);
		//szBuf += "flash\\screenshot1.swf";
		szBuf += (val==0 ? "flash\\screenshot1.swf" : "flash\\screenshot1_short.swf");

		m_FlashView.LoadMovie(0, szBuf);
	}

	m_szIP.Empty();
	ubcMuxData* pData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(m_szSiteId);
	if(pData)
	{
		m_szIP = pData->ipAddress.c_str();
	}

	CRect rc;
	GetClientRect(&rc);
	m_FlashView.MoveWindow(&rc);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

//void CFlashView::OnDestroy()
//{
////	m_FlashView.DestroyWindow();
//	CDialog::OnDestroy();
//
//	// TODO: Add your message handler code here
//	ubcMux::clearInstance();
//}

//void CFlashView::OnClose()
//{
//	m_FlashView.DestroyWindow();
//	// TODO: Add your message handler code here and/or call default
//	ubcMux::clearInstance();
//
//	CDialog::OnClose();
//}

void CFlashView::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CDialog::OnWindowPosChanging(lpwndpos);

	// TODO: Add your message handler code here
}

void CFlashView::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	if(!IsWindow(m_hWnd))	return;

	CRect rc;
	GetClientRect(&rc);

	if(IsWindow(m_FlashView.m_hWnd))
		m_FlashView.MoveWindow(&rc);
}

void CFlashView::Set(LPCTSTR siteId, LPCTSTR hostId)
{
	m_szSiteId = siteId;
	m_szHostId = hostId;
}

void CFlashView::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(TM_CLOSEDLG);
	OnCancel();
}

BEGIN_EVENTSINK_MAP(CFlashView, CDialog)
	ON_EVENT(CFlashView, IDC_FLASH_CTRL, 150, CFlashView::FSCommandFlashCtrl, VTS_BSTR VTS_BSTR)
END_EVENTSINK_MAP()

void CFlashView::FSCommandFlashCtrl(LPCTSTR command, LPCTSTR args)
{
	if(IsSame(command, "screenshotRequest")){
		getScreenShot1(args);
	}
}

void CFlashView::getScreenShot1(CString args)
{
	int nPos = 0;
	short sHour = 0;
	short eHour = 0;
	CString szToken;

	szToken = args.Tokenize(",", nPos);
	if(!szToken.IsEmpty()){
		sHour = atoi(szToken);

		szToken = args.Tokenize(",", nPos);
		if(!szToken.IsEmpty()){
			eHour = atoi(szToken);
		}
	}

	ubcConfig aIni("UBCVariables");
	ciLong val = 0;
	aIni.get("ROOT", "ENABLE_SHORT_SCREENSHOT_PERIOD", val);

	ciString fileList;
	ubcHost::getScreenShot(m_szSiteId, m_szHostId, fileList, sHour, eHour, (val>0) );
	m_Files = fileList.c_str();

	ciString url;
	ciString homepage="http://"+m_szIP;
	ciIniUtil::HttpToHttps(homepage.c_str(), url);

	CString szBuf = "";
	szBuf += "<invoke name='getScreenShot1'><arguments>";
	szBuf += "<string>"+m_szHostId+"</string>";
	szBuf += "<string>"+m_Files+"</string>";
	//szBuf += "<string>http://"+m_szIP+":8080/ScreenShot/</string>";
	//szBuf += "<string>http://"+m_szIP+":";
	szBuf += "<string>";
	szBuf += url.c_str();
	szBuf += ":";
	szBuf += ciIniUtil::getHttpPortStr().c_str();
	szBuf += "/ScreenShot/</string>";
	szBuf += "</arguments></invoke>";

	m_FlashView.CallFunction(szBuf);
}

//bool CFlashView::FindShowSameWindow(CString strText)
//{
//	CWnd* pWnd = CWnd::FromHandle(::FindWindowEx(NULL, NULL, NULL, strText));
//	if(!pWnd) return FALSE;
//
//	if( pWnd->IsWindowVisible() )
//	{
//		if(pWnd->IsIconic()) pWnd->ShowWindow(SW_RESTORE);
//		pWnd->SetForegroundWindow();
//		return TRUE;
//	}
//
//	delete pWnd;
////	::DestroyWindow(hWnd);
////	::CloseWindow(hWnd);
////	CWnd* pWnd = CWnd::FromHandle(hWnd);
////	if(pWnd) delete pWnd;
//
//	return FALSE;
//}