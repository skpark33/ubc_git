#pragma once

#include "common\utblistctrl.h"

#ifndef IDD_PACKAGEMANAGER
#define IDD_PACKAGEMANAGER 25009
#endif//IDD_PACKAGEMANAGER

// CPackageMngDlg dialog
class AFX_EXT_CLASS CPackageMngDlg : public CDialog
{
	DECLARE_DYNAMIC(CPackageMngDlg)
public:
	enum { eSite, ePackage, eUser, eTime, eDesc, eEnd };

	typedef struct {
		DWORD pid;
		HWND hwnd;
	} find_hwnd_from_pid_t;


	void Set(LPCTSTR, LPCTSTR, LPCTSTR);
private:
	void InitPackageList();
	void SetPackageData();

	unsigned long getPid(const char* exename);
	HWND getWHandle(const char* exename);
	HWND getWHandle(unsigned long pid);
public:
private:
	CString m_szSite;
	CString m_szUser;
	CString m_szPasswd;
	CString m_szColName[eEnd];
	CUTBListCtrl m_lscPackage;

	CPoint m_ptSelList;

	static int CALLBACK CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
	static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam);

public:
	CPackageMngDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPackageMngDlg();

// Dialog Data
	enum { IDD = IDD_PACKAGEMANAGER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnBnClickedOk();

	afx_msg void OnPackageDelete();
	afx_msg void OnPackageEdit();
	afx_msg void OnPackagePlay();

	afx_msg void OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRclickList(NMHDR *pNMHDR, LRESULT *pResult);
};
