#pragma once

#ifndef IDD_MULTI_SELECT_HOST
#define IDD_MULTI_SELECT_HOST	25063
#endif//IDD_MULTI_SELECT_HOST

#include "afxwin.h"
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\CheckComboBox.h"


// CMultiSelectHost 대화 상자입니다.

class AFX_EXT_CLASS CMultiSelectHost : public CDialog
{
	DECLARE_DYNAMIC(CMultiSelectHost)

public:
	CMultiSelectHost(LPCTSTR szCustomer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMultiSelectHost();

	void SetTitle(CString& p) { m_title = p; }

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MULTI_SELECT_HOST };
	enum { eCheck, eHostName, eHostID, eGroup, eMaxCol };

	// In/Out Parameter
	CStringArray		m_astrSelHostList;    //hostIdList
	CStringArray		m_astrSelHostNameList;  //hostNameList
	CStringArray		m_astrSelSiteIdList;  //siteIdList

	int					m_nAuthority;
	CString				m_strSiteId;
	CString				m_strIniPath;
	CString				m_exWhere;

protected:
	CString				m_title;
	CString				m_strCustomer;

	CString				m_szColum[eMaxCol];
	HostInfoList		m_lsInfoList;

	CEdit				m_editFilterHostName;
	CEdit				m_editFilterHost;
	//CComboBox			m_cbxHostType;
	CCheckComboBox		m_cbxHostType;
	CComboBox			m_cbxSite;
	CComboBox			m_cbxIncludeContents;
	CComboBox			m_cbxPlayingPackage;
	CComboBox			m_cbOnOff;
	CComboBox			m_cbOperation;
//	CEdit				m_editFilterDescription;
	CHoverButton		m_bnRefresh;
	CUTBListCtrlEx		m_lcHostList;
	CUTBListCtrlEx		m_lcSelHostList;
	CHoverButton		m_bnAdd;
	CHoverButton		m_bnDel;
	CHoverButton		m_bnOK;
	CHoverButton		m_bnCancel;

	CString				m_strSite;
	CString				m_strIncludeContentsID;
	CString				m_strPlayingPackageID;

	void				InitHostList();
	void				RefreshHostList();

	void				InitSelHostList();
	void				RefreshSelHostList();

	void				AddSelHost(int nHostIndex);
	void				DelSelHost(int nSelHostIndex);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonHostRefresh();
	afx_msg void OnBnClickedBnAdd();
	afx_msg void OnBnClickedBnDel();
	afx_msg void OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListSelHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeCbSite();
	afx_msg void OnCbnSelchangeCbIncludecontents();
	afx_msg void OnCbnSelchangeCbPlayingPackage();
	afx_msg void OnEnChangeEditFilterDescription();
	CComboBox m_cbFilterTag;
	CComboBox m_cbFilterCache;
};
