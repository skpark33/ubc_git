// UBCCopCommon.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <afxdllx.h>
#include "ubccopcommon.h"
#include "copmodule.h"
#include "packagerevdlg.h"
#include "FlashView.h"
#include "common\libCommon\ubcMuxRegacy.h"
#include "FolderArchiverDlg.h"
#include "SelectUsers.h"
#include "ContentsSelectDlg.h"
#include "SiteSelectDlg.h"
#include "common/TraceLog.h"

#define SE_TIME_ZONE_NAME _T("SeTimeZonePrivilege")

#ifdef _MANAGED
#error Please read instructions in UBCCopCommon.cpp to compile with /clr
// If you want to add /clr to your project you must do the following:
//	1. Remove the above include for afxdllx.h
//	2. Add a .cpp file to your project that does not have /clr thrown and has
//	   Precompiled headers disabled, with the following text:
//			#include <afxwin.h>
//			#include <afxdllx.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static AFX_EXTENSION_MODULE UBCCopCommonDLL = { NULL, NULL };

#ifdef _MANAGED
#pragma managed(push, off)
#endif

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UBCCopCommon.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UBCCopCommonDLL, hInstance))
			return 0;

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		new CDynLinkLibrary(UBCCopCommonDLL);

	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UBCCopCommon.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UBCCopCommonDLL);
	}
	return 1;   // ok
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

void CopComRelease()
{
	CCopModule::Release();
}

int RunPackageRevDlg(ESrcApp app, LPCTSTR szUsrSite, LPCTSTR szHost, LPCTSTR szPackage, LPCTSTR szSchSite
					, int nAuthority, LPCTSTR strUserId, LPCTSTR strSiteId, LPCTSTR strIniPath, bool& bMod, LPCTSTR szDrive, LPCTSTR szCustomer, bool bCanAdd/*=true*/)
{
	CPackageRevDlg dlg(szCustomer);
	dlg.Set(app, szUsrSite, szDrive, szHost, szPackage, szSchSite, nAuthority, strUserId, strSiteId, strIniPath, bMod, bCanAdd);
	if(dlg.DoModal() == IDCANCEL)
		return eSRCancel;

	CString szBuf;
	int nRet =  dlg.GetRet(szBuf);

	if(eSRSaveAs == nRet){
		return nRet;
	}

	return nRet;
}

void RunFlashView(LPCTSTR szSite, LPCTSTR szHost)
{
	//CFlashView* pDlg = new CFlashView;
	//pDlg->Set(szSite, szHost);
	//pDlg->Create(CFlashView::IDD);
	//if(::IsWindow(pDlg->GetSafeHwnd()))
	//{
	//	pDlg->ShowWindow(SW_SHOW);
	//}

	CFlashView dlg;
	dlg.Set(szSite, szHost);
	dlg.DoModal();
}

BOOL RunFolderArchiver(CString strInFolderPath, CString strTargetFileName)
{
	CFolderArchiverDlg dlg;
	dlg.m_strInFolderPath = strInFolderPath;
	dlg.m_strTargetFileName = strTargetFileName;
	if(dlg.DoModal() == IDOK)
	{
		return TRUE;
	}

	return FALSE;
}

BOOL GetSelectUsers(CString& strSelUserId, CString& strSelUserName)
{
	CSelectUsers dlg;
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK) return FALSE;

	UserInfoList& list = dlg.GetSelectedList();
	POSITION pos = list.GetHeadPosition();
	if(!pos) return FALSE;

	SUserInfo info = list.GetNext(pos);
	strSelUserId = info.userId;
	strSelUserName = info.userName;
	
	return TRUE;
}

BOOL GetSelectContents(CString strCustomer, CString& strSelContentsId, CString& strSelContentsName)
{
	CContentsSelectDlg dlg(strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK) return FALSE;

	CONTENTS_INFO_EX_LIST& list = dlg.GetSelectedContentsIdList();
	POSITION pos = list.GetHeadPosition();
	if(!pos) return FALSE;

	CONTENTS_INFO_EX& info = list.GetNext(pos);
	strSelContentsName = info.strContentsName;
	strSelContentsId = info.strContentsId;

	return TRUE;
}

BOOL GetSelectSite(CString strCustomer, CString& strSelSiteId, CString& strSelSiteName)
{
	CSiteSelectDlg dlg(strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK) return FALSE;

	SSiteInfo info;
	dlg.GetSelectSiteInfo(info);

	strSelSiteId = info.siteId;
	strSelSiteName = info.siteName;

	return TRUE;
}

void PrepareSiteList()
{
	CSiteSelectDlg::PrepareSiteList();
}

BOOL GetTimeZoneMinute(CString& strTimeZoneName, LONG& nMinute)
{
	TIME_ZONE_INFORMATION tzi;
	memset(&tzi, 0x00, sizeof(TIME_ZONE_INFORMATION));

	//HANDLE hToken;
	//TOKEN_PRIVILEGES tkp;

	//OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES|TOKEN_QUERY, &hToken);
	//LookupPrivilegeValue(NULL, SE_TIME_ZONE_NAME, &tkp.Privileges[0].Luid);
	//tkp.PrivilegeCount = 1;
	//tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	//AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);

	DWORD dwRet = GetTimeZoneInformation(&tzi);

	//tkp.Privileges[0].Attributes = 0; 
	//AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES) NULL, 0); 

	if(dwRet == TIME_ZONE_ID_STANDARD || dwRet == TIME_ZONE_ID_UNKNOWN)    
		strTimeZoneName = tzi.StandardName;
	else if( dwRet == TIME_ZONE_ID_DAYLIGHT )
		strTimeZoneName = tzi.DaylightName;
	else
	{
		TraceLog(("GTZI failed (%d)", GetLastError()));
		return FALSE;
	}

	nMinute = tzi.Bias;
	TraceLog(("TimeZoneInfo [%s] [%d]", strTimeZoneName, nMinute));

	return TRUE;
}

CString GetLocaleName()
{
	TCHAR szLocale[MAX_PATH] = {0};
	//GetLocaleInfo( LOCALE_SYSTEM_DEFAULT, LOCALE_SENGLANGUAGE, szLocale, MAX_PATH);
	//TraceLog(("1 [%s]", szLocale));
	//GetLocaleInfo( LOCALE_SYSTEM_DEFAULT, LOCALE_SENGCOUNTRY, szLocale, MAX_PATH);
	//TraceLog(("2 [%s]", szLocale));

	//::GetSystem
	//GetLocaleInfo( LOCALE_NEUTRAL, LOCALE_SENGLANGUAGE, szLocale, MAX_PATH);
	//TraceLog(("3 [%s]", szLocale));
	//GetLocaleInfo( LOCALE_NEUTRAL, LOCALE_SENGCOUNTRY, szLocale, MAX_PATH);
	//TraceLog(("4 [%s]", szLocale));

	//GetLocaleInfo( LOCALE_USER_DEFAULT, LOCALE_SENGLANGUAGE, szLocale, MAX_PATH);
	//TraceLog(("5 [%s]", szLocale));
	//GetLocaleInfo( LOCALE_USER_DEFAULT, LOCALE_SENGCOUNTRY, szLocale, MAX_PATH);
	//TraceLog(("6 [%s]", szLocale));

	GetLocaleInfo( LOCALE_SYSTEM_DEFAULT, LOCALE_SENGLANGUAGE, szLocale, MAX_PATH);
	TraceLog(("LocaleName [%s]", szLocale));
//	_tsetlocale(LC_ALL, szLocale);
	return CString(szLocale);
}