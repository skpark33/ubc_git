// CPackageRevAddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UBCCopCommon.h"
#include "PackageRevAddDlg.h"
#include "copmodule.h"
#include "common\libcommon\ubchost.h"
#include "PackageSelectDlg.h"

// CPackageRevAddDlg dialog
IMPLEMENT_DYNAMIC(CPackageRevAddDlg, CDialog)

CPackageRevAddDlg::CPackageRevAddDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPackageRevAddDlg::IDD, pParent)
{
	m_szUsrSite = "*";
	m_szSchSite = "";
	m_szTitle = LoadStringById(IDS_PACKAGEREVADDDLG_STR001);
	m_szPackage.Empty();
	m_RevId.Empty();

	m_RetMsg = eSRNormal;
	m_SrcApp = eSAUnknwon;

	m_bModifiedPackage = false;
	m_bModifyReservation = false;
	m_bSTimeInit = false;
	m_bETimeInit = false;
}

CPackageRevAddDlg::~CPackageRevAddDlg()
{
}

void CPackageRevAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PACKAGE_CUR, m_lscPackageCur);
	DDX_Control(pDX, IDC_LIST_PACKAGE_REV, m_lscPackageRev);
	DDX_Control(pDX, IDC_CHECK_START, m_chkStart);
	DDX_Control(pDX, IDC_CHECK_END, m_chkEnd);
	DDX_Control(pDX, IDC_DATETIMEPICKER_START, m_dtcStart);
	DDX_Control(pDX, IDC_DATETIMEPICKER_START2, m_dtcStart2);
	DDX_Control(pDX, IDC_DATETIMEPICKER_END, m_dtcEnd);
	DDX_Control(pDX, IDC_DATETIMEPICKER_END2, m_dtcEnd2);
}

BEGIN_MESSAGE_MAP(CPackageRevAddDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK_START, &CPackageRevAddDlg::OnBnClickedCheckStart)
	ON_BN_CLICKED(IDC_CHECK_END, &CPackageRevAddDlg::OnBnClickedCheckEnd)
	ON_BN_CLICKED(IDOK, &CPackageRevAddDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BN_SELECT_PACKAGE, &CPackageRevAddDlg::OnBnClickedBnSelectPackage)
END_MESSAGE_MAP()

// CPackageRevAddDlg message handlers
BOOL CPackageRevAddDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	SetWindowText(m_szTitle);
	m_dtcStart.SetFormat("yyyy/MM/dd");
	m_dtcStart2.SetFormat("tt HH:mm");
	m_dtcEnd.SetFormat("yyyy/MM/dd");
	m_dtcEnd2.SetFormat("tt HH:mm");

	SYSTEMTIME stm;
	GetLocalTime(&stm);
	
	CTime tmCurS(stm);
	CTimeSpan tsStart(0,0,6,0);
	tmCurS += tsStart;
	m_dtcStart.SetTime(&tmCurS);
	m_dtcStart2.SetTime(&tmCurS);

	InitDateRange(m_dtcStart);
	InitDateRange(m_dtcStart2);

	CTime tmCurE(stm);
	CTimeSpan tsEnd(0,1,6,0);
	tmCurE += tsEnd;
	m_dtcEnd.SetTime(&tmCurE);
	m_dtcEnd2.SetTime(&tmCurE);

	InitDateRange(m_dtcEnd);
	InitDateRange(m_dtcEnd2);

	PackageInfoList lsPackageInfo;

	if(!m_szPackage.IsEmpty())
	{
		CString strWhere;
		strWhere.Format(_T("programId = '%s'"), m_szPackage);
		CWaitMessageBox wait;
		CCopModule::GetObject()->GetPackage(m_szUsrSite, lsPackageInfo, strWhere);

		POSITION pos = lsPackageInfo.GetHeadPosition();
		if(pos)
		{
			SPackageInfo info = lsPackageInfo.GetNext(pos);
			m_szPackage = info.szPackage;
			m_szSchSite = info.szSiteID;
		}
	}

	SetDlgItemText(IDC_EB_SELECT_PACKAGE, m_szPackage);

	if(m_bSTimeInit)
	{
		m_dtcStart.SetTime(&m_tmStart);
		m_dtcStart2.SetTime(&m_tmStart);
		m_chkStart.SetCheck(true);
	}
	else
	{
		m_chkStart.SetCheck(true);
	}

	if(m_bETimeInit){
		m_dtcEnd.SetTime(&m_tmEnd);
		m_dtcEnd2.SetTime(&m_tmEnd);
		m_chkEnd.SetCheck(true);
	}else{
		m_chkEnd.SetCheck(false);
		m_dtcEnd.EnableWindow(false);
		m_dtcEnd2.EnableWindow(false);
	}

	InitListCur();
	InitListRev();
	SetDataListCur();
	SetDataListRev();

	UpdateData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPackageRevAddDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CPackageRevAddDlg::Set(ESrcApp app, LPCTSTR szSite, LPCTSTR szScheSite, LPCTSTR szPackage, LPCTSTR resevId, BOOL bModSche, PackageRevInfoList& list, BOOL bModRev)
{
	m_SrcApp = app;
	m_bModifiedPackage = bModSche;
	m_bModifyReservation = bModRev;
	m_RevId = resevId;
	m_szUsrSite = szSite?szSite:"*";
	m_szSchSite = szScheSite?szScheSite:"";
	m_szPackage = szPackage?szPackage:"";
	m_pScheRevInfo = &list;
}

void CPackageRevAddDlg::SetTime(time_t tStart, time_t tEnd)
{
	if(tStart){
		m_tmStart = CTime(tStart);
		m_bSTimeInit = true;
	}else{
		m_bSTimeInit = false;
	}

	if(tEnd){
		m_tmEnd = CTime(tEnd);
		m_bETimeInit = true;
	}else{
		m_bETimeInit = false;
	}
}

void CPackageRevAddDlg::SetTitle(LPCTSTR szTitle)
{
	m_szTitle = szTitle;
}

void CPackageRevAddDlg::SetCustomer(LPCTSTR szCustomer)
{
	m_strCustomer = szCustomer;
}

EScheRev CPackageRevAddDlg::GetRet(CString& szSite, CString& szPackage)
{
	szSite = m_szSchSite;
	szPackage = m_szPackage;
	return m_RetMsg;
}

void CPackageRevAddDlg::InitListCur()
{
//	m_lscPackageCur.SetExtendedStyle(m_lscPackageCur.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lscPackageCur.SetExtendedStyle(m_lscPackageCur.GetExtendedStyle() | LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT);
	m_lscPackageCur.InsertColumn(0, LoadStringById(IDS_PACKAGEREVADDDLG_STR002), LVCFMT_LEFT, 160);
	m_lscPackageCur.InitHeader(IDB_UBCLISTCTR_HEAD);
}

void CPackageRevAddDlg::InitListRev()
{
//	m_lscPackageRev.SetExtendedStyle(m_lscPackageRev.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lscPackageRev.SetExtendedStyle(m_lscPackageRev.GetExtendedStyle() | LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT);
	m_lscPackageRev.InsertColumn(0, LoadStringById(IDS_PACKAGEREVADDDLG_STR003), LVCFMT_LEFT, 160);
	m_lscPackageRev.InitHeader(IDB_UBCLISTCTR_HEAD);
}

void CPackageRevAddDlg::SetDataListCur()
{
	m_lscPackageCur.DeleteAllItems();

	if(m_szPackage.IsEmpty()) return;

	CWaitMessageBox wait;

	ciStringList list;
	ubcHost::getAppliedHostList(m_szPackage, list);

	int nRow = 0;
	for(ciStringList::iterator Iter = list.begin(); Iter != list.end(); Iter++){
		m_lscPackageCur.InsertItem(nRow++, (*Iter).c_str());
	}
}

void CPackageRevAddDlg::SetDataListRev()
{
	m_lscPackageRev.DeleteAllItems();

	if(m_szPackage.IsEmpty()) return;

	CWaitMessageBox wait;

	ciStringList list;
	ubcHost::getReservedHostList(m_szPackage, list);

	int nRow = 0;
	for(ciStringList::iterator Iter = list.begin(); Iter != list.end(); Iter++){
		m_lscPackageRev.InsertItem(nRow++, (*Iter).c_str());
	}
}

void CPackageRevAddDlg::OnBnClickedCheckStart()
{
	m_chkStart.SetCheck(true);
	m_szMsg = LoadStringById(IDS_PACKAGEREVADDDLG_MSG001);
	MessageBox(m_szMsg, m_szTitle, MB_ICONINFORMATION);
}

void CPackageRevAddDlg::OnBnClickedCheckEnd()
{
	UpdateData();

	m_dtcEnd.EnableWindow(m_chkEnd.GetCheck());
	m_dtcEnd2.EnableWindow(m_chkEnd.GetCheck());
}

void CPackageRevAddDlg::OnBnClickedOk()
{
	m_bStart = m_chkStart.GetCheck();
	m_bEnd = m_chkEnd.GetCheck();

	SYSTEMTIME sysTime1, sysTime2;

	m_dtcStart.GetTime(&sysTime1);
	m_dtcStart2.GetTime(&sysTime2);
	SetDateTime(sysTime1, sysTime2);
	m_dtcStart.SetTime(&sysTime1);
	m_dtcStart2.SetTime(&sysTime2);

	m_dtcEnd.GetTime(&sysTime1);
	m_dtcEnd2.GetTime(&sysTime2);
	SetDateTime(sysTime1, sysTime2);
	m_dtcEnd.SetTime(&sysTime1);
	m_dtcEnd2.SetTime(&sysTime2);

	m_dtcStart.GetTime(m_tmStart);
	m_dtcEnd.GetTime(m_tmEnd);

	SYSTEMTIME stm;
	GetLocalTime(&stm);
	CTime tmCur(stm);

	CTimeSpan tSpan = m_tmStart - tmCur;
	if(!m_bModifyReservation && tSpan.GetTotalMinutes() < 5){
		m_szMsg = LoadStringById(IDS_PACKAGEREVADDDLG_MSG002);
		MessageBox(m_szMsg, m_szTitle, MB_ICONINFORMATION);
		return;
	}

	if(m_bEnd){
		tSpan = m_tmEnd - m_tmStart;
	
		// 0000797: 콘텐츠 패키지 예약시 실행시간은 적어도 5분이상으로 설정되도록 한다
		if(tSpan.GetTotalMinutes() < 5)
		{
			m_szMsg = LoadStringById(IDS_PACKAGEREVADDDLG_MSG003);
			MessageBox(m_szMsg, m_szTitle, MB_ICONINFORMATION);
			return;
		}
	}

	m_RetMsg = eSRNormal; // Normal
	
	if(m_bModifiedPackage && m_lscPackageCur.GetItemCount() > 0){
		m_szMsg = LoadStringById(IDS_PACKAGEREVADDDLG_MSG004);
		if(MessageBox(m_szMsg, m_szTitle, MB_ICONINFORMATION|MB_YESNO) == IDYES){
			m_RetMsg = eSRSaveAs; // SaveAs
		}
	}

	//POSITION pos = m_pScheRevInfo->GetHeadPosition();
	//while(pos){
	//	SPackageRevInfo info = m_pScheRevInfo->GetNext(pos);
	//	if(info.reservationId == m_RevId)
	//		continue;

	//	CTime fromTime(info.fromTime);
	//	CTime toTime(info.toTime);

	//	if(fromTime.Format(STR_DEF_TIME) == STR_DEF_EMPTY_TIME)
	//		continue;

	//	//if(toTime.Format(STR_DEF_TIME) == STR_DEF_EMPTY_TIME){
	//	//	if(m_tmStart <= fromTime && fromTime <= m_tmEnd ){
	//	//		m_szMsg.Format(LoadStringById(IDS_PACKAGEREVADDDLG_MSG005));
	//	//		MessageBox(m_szMsg, m_szTitle, MB_ICONINFORMATION);
	//	//		return;
	//	//	}
	//	//}else{
	//	//	if(m_tmStart <= toTime && toTime <= m_tmEnd){
	//	//		m_szMsg.Format(LoadStringById(IDS_PACKAGEREVADDDLG_MSG005));
	//	//		MessageBox(m_szMsg, m_szTitle, MB_ICONINFORMATION);
	//	//		return;
	//	//	}
	//	//}
	//}

	//if("*" == m_szUsrSite)
	//{
	//	m_szPackage = "";
	//	SetDlgItemText(IDC_EB_SELECT_PACKAGE, m_szPackage);
	//	m_szSchSite = m_stPackageInfo.szSiteID;
	//}

	OnOK();
}

void CPackageRevAddDlg::SetDateTime(SYSTEMTIME& date, SYSTEMTIME& time)
{
	time.wYear = date.wYear;
	time.wMonth = date.wMonth;
	time.wDay = date.wDay;
	time.wDayOfWeek = date.wDayOfWeek;
	time.wSecond = 0;
	time.wMilliseconds = 0;

    date.wHour = time.wHour;
    date.wMinute = time.wMinute;
    date.wSecond = 0;
    date.wMilliseconds = 0;
}

void CPackageRevAddDlg::OnBnClickedBnSelectPackage()
{
	CPackageSelectDlg dlg(m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK) return;

	PackageInfoList& list = dlg.GetSelectedPackageList();
	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo info = list.GetNext(pos);
		m_szPackage = info.szPackage;
		m_szSchSite = info.szSiteID;
	}

	SetDlgItemText(IDC_EB_SELECT_PACKAGE, m_szPackage);

	m_bModifiedPackage = false;
	SetDataListCur();
	SetDataListRev();
}
