// SelectUsers.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "CopModule.h"
#include "SelectUsers.h"


// CSelectUsers 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSelectUsers, CDialog)

CSelectUsers::CSelectUsers(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectUsers::IDD, pParent)
	, m_bSingleSelect ( true )
{

}

CSelectUsers::~CSelectUsers()
{
}

void CSelectUsers::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_btnSelect);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_LIST_USERS, m_lcList);
}


BEGIN_MESSAGE_MAP(CSelectUsers, CDialog)
	ON_BN_CLICKED(IDOK, &CSelectUsers::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSelectUsers::OnBnClickedCancel)
	ON_EN_CHANGE(IDC_EB_SEARCH, &CSelectUsers::OnEnChangeEbSearch)
END_MESSAGE_MAP()


// CSelectUsers 메시지 처리기입니다.

void CSelectUsers::OnBnClickedOk()
{
	m_listSelectUser.RemoveAll();

	if(m_bSingleSelect)
	{
		POSITION pos = (POSITION)m_lcList.GetItemData(m_lcList.GetRadioSelectedIndex());
		if(pos)
		{
			SUserInfo info = m_listUsers.GetAt(pos);
			m_listSelectUser.AddTail(info);
		}
	}
	else
	{
		int count = m_lcList.GetItemCount();
		for(int i=0; i<count; i++)
		{
			if(!m_lcList.GetCheck(i)) continue;

			POSITION pos = (POSITION)m_lcList.GetItemData(i);
			if(pos)
			{
				SUserInfo info = m_listUsers.GetAt(pos);
				m_listSelectUser.AddTail(info);
			}
		}
	}

	if(m_listSelectUser.GetCount() <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_SELECTUSER_MSG001));
		return;
	}

	OnOK();
}

void CSelectUsers::OnBnClickedCancel()
{
	OnCancel();
}

BOOL CSelectUsers::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_btnSelect.LoadBitmap(IDB_BUTTON_SELECT, RGB(255,255,255));
	m_btnCancel.LoadBitmap(IDB_BUTTON_CANCEL, RGB(255,255,255));

	InitList();

	SUserInfo info;
	CCopModule::GetObject()->GetUser(info, m_listUsers);
	RefreshList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSelectUsers::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lcList.InsertColumn(eUserID  , LoadStringById(IDS_SELECTUSER_STR001), LVCFMT_LEFT, 150);
	m_lcList.InsertColumn(eUserName, LoadStringById(IDS_SELECTUSER_STR002), LVCFMT_LEFT, 150);

	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, (m_bSingleSelect ? CUTBListCtrlEx::LS_RADIOBOX : CUTBListCtrlEx::LS_CHECKBOX));
}

void CSelectUsers::RefreshList(CString strFilter)
{
	strFilter.MakeLower();

	m_lcList.SetRedraw(FALSE);
	m_lcList.DeleteAllItems();

	POSITION pos = m_listUsers.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SUserInfo info = m_listUsers.GetNext(pos);

		if(!strFilter.IsEmpty())
		{
			CString strUserId = info.userId;
			CString strUserName = info.userName;

			strUserId.MakeLower();
			strUserName.MakeLower();

			if(strUserId.Find(strFilter) < 0 && strUserName.Find(strFilter) < 0)
			{
				continue;
			}
		}

		int nRow = m_lcList.GetItemCount();

		m_lcList.InsertItem(nRow, info.userId);
		m_lcList.SetItemText(nRow, eUserName, info.userName);
		m_lcList.SetItemData(nRow, (DWORD_PTR)posOld);
	}

	m_lcList.SortList(0, false, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);
	m_lcList.SetRedraw(TRUE);
}

void CSelectUsers::OnEnChangeEbSearch()
{
	CString strFilter;
	GetDlgItemText(IDC_EB_SEARCH, strFilter);
	RefreshList(strFilter);
}
