// FtpMultiSite.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "UBCCopCommon.h"
#include "FtpMultiSite.h"
#include "ubccommon\ftpmodule.h"
#include "common\libCommon\ubcMuxRegacy.h"
#include "common/TraceLog.h"
#include "common/libProfileManager/ProfileManager.h"
#include <IMAGEHLP.H>
#pragma comment(lib, "imagehlp.lib")
#include "common/MD5Util.h"

#define WM_COMPLETE_FTP (WM_USER+1)

// CFtpMultiSite dialog
IMPLEMENT_DYNAMIC(CFtpMultiSite, CDialog)

CFtpMultiSite::CFtpMultiSite(BOOL bIsHttpOnly, CWnd* pParent /*=NULL*/)
	: CDialog(CFtpMultiSite::IDD, pParent)
//	, m_objFileSvcClient("211.232.57.205", 8080)
{
	m_bRet = FALSE;
	m_bOnlyAddFile = FALSE;
	m_pThread = NULL;
	m_FtpStat = eUnkown;
	m_lsPackage.clear();
	m_lsDownFiles.RemoveAll();
	m_nCurProcItem = -1;
	m_bUserAbort = false;
	m_bFtpEventUse = true;
	m_CurBytes = 0;

	// 2012.03.02 ftp대신 http로 파일전송을 할 수 있도록 한다.
	m_bIsHttpOnly = bIsHttpOnly;
#ifdef _DEBUG
//	m_bIsHttpOnly = true;
#endif

	m_FtpCallback = 0;  	// skpark same_size_file_problem 


}

CFtpMultiSite::~CFtpMultiSite()
{
}

void CFtpMultiSite::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TITLE, m_sttTitle);
	DDX_Control(pDX, IDC_LC_LIST, m_lcList);
}

BEGIN_MESSAGE_MAP(CFtpMultiSite, CDialog)
	ON_WM_DESTROY()
	ON_MESSAGE(WM_COMPLETE_FTP, OnCompleteFTP)
	ON_BN_CLICKED(IDOK, &CFtpMultiSite::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CFtpMultiSite::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_RETRY, &CFtpMultiSite::OnBnClickedBnRetry)
END_MESSAGE_MAP()

// CFtpMultiSite message handlers
BOOL CFtpMultiSite::OnInitDialog()
{
	TraceLog(("CFtpMultiSite::OnInitDialog"));

	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	if(m_FtpStat == eUnkown) return TRUE;

	GetDlgItem(IDC_KB_AUTO_CLOSE)->ShowWindow(m_FtpStat == eUpload);

	// 업로드할 파일의 목록과 용량을 단말과 서버에서 확인하고 있습니다.
	CWaitMessageBox wait(LoadStringById(IDS_FTPMULTISITE_MSG013));

	InitList();
	FillList();

	m_pThread = AfxBeginThread(ThreadProc, (LPVOID)this);

	GetDlgItem(IDOK    )->EnableWindow(FALSE);
	GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
	GetDlgItem(IDC_BN_RETRY)->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CFtpMultiSite::OnDestroy()
{
	TraceLog(("CFtpMultiSite::OnDestroy"));

	CDialog::OnDestroy();

	if(m_pThread)
	{
		DWORD dwRet = WaitForSingleObject(m_pThread->m_hThread, 5000);
	}

	m_pThread = NULL;
}

BOOL CFtpMultiSite::RunFtp(int stat)
{
	TraceLog(("CFtpMultiSite::RunFtp"));

	m_FtpStat = stat;
	DoModal();
	m_FtpStat = eUnkown;
	return m_bRet;
}

BOOL CFtpMultiSite::AddSite(LPCTSTR szSite, LPCTSTR szFile)
{
	TraceLog(("CFtpMultiSite::AddSite..."));

	TraceLog(("AddSite(%s, %s)",szSite?szSite:"NULL", szFile?szFile:"NULL"));
	if(!szSite || !szFile){
		return FALSE;
	}

	ST_PACKAGE_INFO stPackageInfo;
	stPackageInfo.fullpath = szFile;
	stPackageInfo.siteId = szSite;

	TCHAR szDrive[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_splitpath(szFile, szDrive, NULL, szFilename, szExt);

	stPackageInfo.drive = szDrive;
	stPackageInfo.file = szFilename;
	stPackageInfo.file += szExt;
	stPackageInfo.packageName = szFilename;

	ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(stPackageInfo.siteId.c_str());
	if(!aData){
		TraceLog(("getMuxData Failed"));
		return FALSE;
	}

#ifdef _DEBUG
//	aData->ftpPort = 8080;
#endif

	stPackageInfo.ip = aData->getFTPAddress();
	stPackageInfo.userId = aData->ftpId;
	stPackageInfo.passwd = aData->ftpPasswd;
	stPackageInfo.port = aData->ftpPort;
	TraceLog(("IP:%s / PORT:%d / ID:%s /PW:%s"
			, stPackageInfo.ip.c_str()
			, stPackageInfo.port
			, stPackageInfo.userId.c_str()
			, stPackageInfo.passwd.c_str()));

	m_lsPackage.push_back(stPackageInfo);

	return TRUE;
}

void CFtpMultiSite::OnlyAddFile(BOOL bOnly)
{
	TraceLog(("CFtpMultiSite::OnlyAddFile"));

	m_bOnlyAddFile = bOnly;
}

BOOL CFtpMultiSite::AddDownloadFile(LPCTSTR szFile)
{
	TraceLog(("CFtpMultiSite::AddDownloadFile"));

	POSITION pos = m_lsDownFiles.Find(szFile);
	if(pos) return TRUE;
	m_lsDownFiles.AddTail(szFile);
	return TRUE;
}

UINT CFtpMultiSite::ThreadProc(LPVOID param)
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	TraceLog(("CFtpMultiSite::ThreadProc"));

	CFtpMultiSite* pDlg = (CFtpMultiSite*)param;
	if(pDlg)
	{
		if(pDlg->m_FtpStat == eUpload)
			pDlg->m_bRet = pDlg->UploadFile();
		else if(pDlg->m_FtpStat == eDownload)
			pDlg->m_bRet = pDlg->DownloadFile();

		pDlg->PostMessage(WM_COMPLETE_FTP);
	}

	::CoUninitialize();

//	AfxEndThread(0);

	return 0;
}

BOOL CFtpMultiSite::UploadFile()
{
	TraceLog(("CFtpMultiSite::UploadFile"));

	m_bUserAbort = false;
	m_nCurProcItem = 0;
	BOOL bResult = TRUE;

	CString szBuf;

	if(!m_bIsHttpOnly) m_FtpClient.AttachObserver(this);

	for(std::list<ST_PACKAGE_INFO>::iterator it = m_lsPackage.begin(); it != m_lsPackage.end(); it++)
	{
		ST_PACKAGE_INFO stPackageInfo = *it;

		m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_STR001));

		BOOL bLoginResult = FALSE;

		if(!m_bIsHttpOnly)
		{
			CLogonInfo loginInfo(stPackageInfo.ip, stPackageInfo.port, stPackageInfo.userId, stPackageInfo.passwd);
			bLoginResult = m_FtpClient.Login(loginInfo);
		}
		// 2012.03.02 ftp대신 http로 파일전송을 할 수 있도록 한다.
		else
		{
			m_objFileSvcClient.SetHttpServerInfo(stPackageInfo.ip.c_str(), stPackageInfo.port);
			bLoginResult = m_objFileSvcClient.Login(stPackageInfo.userId.c_str(), stPackageInfo.passwd.c_str());
		}

		if(!bLoginResult)
		{
			bResult = FALSE;

			TraceLog(("IP:%s / PORT:%d / ID:%s /PW:%s",stPackageInfo.ip.c_str(),stPackageInfo.port,stPackageInfo.userId.c_str(),stPackageInfo.passwd.c_str()));

			// 동일서버에 해당되는 항목을 모두 실패처리한다
			for(; m_nCurProcItem<m_lcList.GetItemCount() ; m_nCurProcItem++)
			{
				CString strSiteId = m_lcList.GetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eSite]));
				if(strSiteId != stPackageInfo.siteId.c_str()) break;

				SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR002), CFtpModule::MakeErrorMsg(FTP_ERROR_CONNECT_FAIL));
			}

			continue;
		}

		if(!m_bIsHttpOnly)
		{
			m_FtpClient.SetResumeMode(false);
			if(m_FtpClient.ChangeWorkingDirectory("/") == FTP_ERROR)
			{
				bResult = FALSE;

				// 동일서버에 해당되는 항목을 모두 실패처리한다
				for(; m_nCurProcItem<m_lcList.GetItemCount() ; m_nCurProcItem++)
				{
					CString strSiteId = m_lcList.GetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eSite]));
					if(strSiteId != stPackageInfo.siteId.c_str()) break;

					SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR002), CFtpModule::MakeErrorMsg(FTP_ERROR_CHANGE_DIRECTORY, (LPARAM)_T("/")));
				}

				m_FtpClient.Logout();
				continue;
			}
		}

		m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_STR007));

		PackageFileCheck(stPackageInfo.fullpath.c_str());

		for(IterFileInfo Iter = m_mapLocal.begin(); Iter != m_mapLocal.end(); Iter++)
		{
			if(m_bUserAbort)
			{
				if(!m_bIsHttpOnly) 
				{
					m_FtpClient.Logout();
				}
				else  
				{
					m_objFileSvcClient.Logout();
				}
				return FALSE;
			}

			std::string sFileKey = Iter->first;
			P_FILE_INFO pInfo = (P_FILE_INFO)&(Iter->second);

			if(sFileKey.empty() || !pInfo) continue;

			std::string sRemoteFileKey = pInfo->location + pInfo->name;

			IterFileInfo IRe = m_mapRemote.find(sRemoteFileKey);

			TraceLog(("Check file :RemoteKey=%s", sRemoteFileKey.c_str()));

			bool bSkip = false;
			bool bIsSameButModified = false; // skpark same_size_file_problem
			CString   strLocation = LoadStringById(IDS_FTPMULTISITE_STR010);
			ULONGLONG lLocalSize  = (pInfo->exist ? pInfo->size : 0);
			ULONGLONG lServerSize = (m_mapRemote.end() != IRe ? (IRe->second).size : 0);
			CString   strServerPath = pInfo->location.c_str();
			CString   strProgress = _T("0%");
			CString   strStatus   = LoadStringById(IDS_FTPMULTISITE_STR006);
			CString   strMessage;

			// 공용컨텐츠의 경우 업로드하지 않는다
			if(pInfo->isPublic)
			{
				strLocation = LoadStringById(IDS_FTPMULTISITE_STR011);
				strProgress = _T("100%");
				strStatus   = LoadStringById(IDS_FTPMULTISITE_STR009);
				strMessage  = CFtpModule::MakeErrorMsg(FTP_ERROR_REMOTE_FILE_PUBLIC, (LPARAM)pInfo->name.c_str());
				bSkip = true;
			}
			// 파일이 로컬에는 없고 서버에는 있는경우 이름만 비교
			else if(!pInfo->exist)
			{
				if(m_mapRemote.end() != IRe)
				{
					strLocation = LoadStringById(IDS_FTPMULTISITE_STR011);
					strProgress = _T("100%");
					strStatus   = LoadStringById(IDS_FTPMULTISITE_STR009);
					strMessage  = CFtpModule::MakeErrorMsg(FTP_ERROR_LOCAL_FILE_EXIST, (LPARAM)pInfo->name.c_str());
					bSkip = true;
				}
				else
				{
					strLocation = LoadStringById(IDS_FTPMULTISITE_STR012);
					strStatus   = LoadStringById(IDS_FTPMULTISITE_STR003);
					strMessage  = CFtpModule::MakeErrorMsg(FTP_ERROR_LOCAL_FILE_OPEN, (LPARAM)pInfo->name.c_str());
					bSkip = true;
					bResult = FALSE;
				}
			}
			// INI의 파일사이즈와 실제 파일사이즈가 다른 경우에는 실제파일이 원본이 아니라고 보고 업로드하지 않는다.
			else if(pInfo->size != pInfo->sizeINI)
			{
				strLocation = LoadStringById(IDS_FTPMULTISITE_STR013);
				strProgress = _T("100%");
				strStatus   = LoadStringById(IDS_FTPMULTISITE_STR009);
				strMessage  = CFtpModule::MakeErrorMsg(FTP_ERROR_LOCAL_FILE_EXIST, (LPARAM)pInfo->name.c_str());
				bSkip = true;
			}
			// 파일이 로컬에는 있고 서버에도 있는경우 이름과 파일 사이즈 비교
			else if(m_mapRemote.end() != IRe)
			{
				// file 진위여부를 체크하기 위해 MD5를 사용한다.
				char szMd5[16*2+1] = {0};
				// 50 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
				if(pInfo->size < 50000000 && strlen(pInfo->md5.c_str()) > 0)
				{
					CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)(pInfo->localpath + pInfo->name).c_str(), szMd5);
				}

				if((IRe->second).size == pInfo->size)
				{
					// 진짜 파일이 아닌경우 업로드하지 않는다.
					if(strcmp(pInfo->md5.c_str(), szMd5) != 0)
					{
						strLocation = LoadStringById(IDS_FTPMULTISITE_STR013);
						strProgress = _T("100%");
						strStatus   = LoadStringById(IDS_FTPMULTISITE_STR009);
						strMessage  = CFtpModule::MakeErrorMsg(FTP_ERROR_DIRTY_LOCAL_FILE, (LPARAM)pInfo->name.c_str());
						bSkip = true;
					}
					else
					{
						// 여기는 size 가 같고,  ini 의 md5와 로컬의 md5 가 같은 경우이다.
						//
						// skpark same_size_file_problem 2014.06.12 사실은 여기서, 사이즈가 같더라도, 변경된 파일은 Upload 해야한다.!!!!					
						if(	m_FtpCallback ) {
							bIsSameButModified = m_FtpCallback->isModified(pInfo->contentsId.c_str());
							bSkip =  !bIsSameButModified;
						}else{
							bSkip = true;
						}
						if(bSkip){
							strLocation = LoadStringById(IDS_FTPMULTISITE_STR013);
							strProgress = _T("100%");
							strStatus   = LoadStringById(IDS_FTPMULTISITE_STR009);
							strMessage  = CFtpModule::MakeErrorMsg(FTP_ERROR_LOCAL_FILE_EXIST, (LPARAM)pInfo->name.c_str());
						}else{
							TraceLog(("RemoteFileSize(%ld) and localFileSize(%ld) is same but Local(%s) is modified", lServerSize ,lLocalSize , pInfo->name.c_str()));
						
						}

					}
				}
				else
				{
					TraceLog(("File(%s) RemoteFileSize(%ld) and localFileSize(%ld) is different", pInfo->name.c_str(), lServerSize , lLocalSize));
					strLocation = LoadStringById(IDS_FTPMULTISITE_STR013);
					strMessage.Format(LoadStringById(IDS_FTPMULTISITE_MSG009)
									, ToFileSize(lServerSize, 'B')
									, ToFileSize(lLocalSize, 'B')
									);
				}
			}else{
					TraceLog(("RemoteFile does not exist in RemoteMap (%s)",sRemoteFileKey.c_str()));

			}

			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eLocation  ]), strLocation           );
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eProgress  ]), strProgress           );
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eLocalSize ]), ToFileSize(lLocalSize , 'A'));
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eServerSize]), ToFileSize(lServerSize, 'A'));
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eServerPath]), strServerPath         );
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eStatus    ]), strStatus             );
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eMessage   ]), strMessage            );

			if(bSkip)
			{
				SetListItemStatus(m_nCurProcItem, strStatus, strMessage);
				m_nCurProcItem++;
				continue;
			}

			m_CurBytes = 0;
			m_FileBytes = pInfo->size;

			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eStatus]), LoadStringById(IDS_FTPMULTISITE_STR007));

			szBuf.Format(LoadStringById(IDS_FTPMULTISITE_MSG004), stPackageInfo.siteId.c_str(), pInfo->name.c_str());
			m_sttTitle.SetWindowText(szBuf);

			if(!m_bIsHttpOnly)
			{
				//if(m_FtpClient.MakeDirectory(pInfo->location) == FTP_ERROR)
				if(!MakeServerDir(pInfo->location.c_str()))
				{
					bResult = FALSE;

					SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR002), CFtpModule::MakeErrorMsg(FTP_ERROR_MAKE_DIRECTORY, (LPARAM)pInfo->location.c_str()));
					m_nCurProcItem++;
					continue;
				}
			}

			//skpark same_size_file_problem 2014.06.12 여기서 modified 된 파일의 시간을 변경해야 한다.
			//if(	m_FtpCallback && bIsSameButModified) {
			if(	m_FtpCallback ) {
				m_FtpCallback->ChangeFileTime(pInfo->contentsId.c_str());
			}

			BOOL bUploadResult = FALSE;
			if(!m_bIsHttpOnly) 
			{
				bUploadResult = m_FtpClient.UploadFile(pInfo->localpath + pInfo->name, pInfo->location + pInfo->name, false, CRepresentation(CType::Image()), true);
			}
			else
			{
				try {
					bUploadResult = m_objFileSvcClient.PutFile((pInfo->localpath + pInfo->name).c_str(), (pInfo->location + pInfo->name).c_str(), 0, (IProgressHandler*)this);
				}catch (CString& e) {
					bUploadResult = false;
				}
			}
			
			if(!bUploadResult)
			{
				TraceLog(("PutFile (%s) failed",sRemoteFileKey.c_str()));
				bResult = FALSE;

				SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR002), CFtpModule::MakeErrorMsg(FTP_ERROR_UPLOADING, (LPARAM)pInfo->name.c_str()));
				m_nCurProcItem++;
				continue;
			}



			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eProgress]), _T("100%"));
			SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR005));
			m_nCurProcItem++;
		}

		m_CurBytes = 0;
		m_FileBytes = 0;

		CFileFind ff;
		if(ff.FindFile(stPackageInfo.fullpath.c_str()) && ff.FindNextFile())
		{
			m_FileBytes = ff.GetLength();
		}
		ff.Close();

		m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eStatus]), LoadStringById(IDS_FTPMULTISITE_STR007));

		if(m_bUserAbort)
		{
			if(!m_bIsHttpOnly) 
			{
				m_FtpClient.Logout();
			}
			else
			{
				m_objFileSvcClient.Logout();
			}
			return FALSE;
		}

		//skpark same_size_file_problem 2014.06.12 여기서 ini 를 업로드 하기 전에 modified 된 파일의 modified 값은 false 로 돌려놔야 한다. (ini 까지!!!)
		if(	m_FtpCallback ) {
			m_FtpCallback->ChangeModifiedFlag(false);
		}
		
		BOOL bUploadResult = FALSE;
		if(!m_bIsHttpOnly) 
		{
			bUploadResult = m_FtpClient.UploadFile(stPackageInfo.fullpath, tstring("/config/") + stPackageInfo.file, false, CRepresentation(CType::Image()), true);
		}
		else
		{
			try {
				bUploadResult = m_objFileSvcClient.PutFile(stPackageInfo.fullpath.c_str(), (tstring("/config/") + stPackageInfo.file).c_str(), 0, (IProgressHandler*)this);
			}catch (CString& e) {
				bUploadResult = false;
			}
		}

		if(!bUploadResult)
		{
			bResult = FALSE;
			SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR002), CFtpModule::MakeErrorMsg(FTP_ERROR_UPLOADING, (LPARAM)stPackageInfo.file.c_str()));
		}
		else
		{
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eProgress]), _T("100%"));
			SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR005));
		}

		if(!m_bIsHttpOnly) 
		{
			m_FtpClient.Logout();
		}
		else 
		{
			m_objFileSvcClient.Logout();
		}

		m_nCurProcItem++;
	}

	if(!m_bIsHttpOnly) m_FtpClient.DetachObserver(this);

	return bResult;
}

BOOL CFtpMultiSite::DownloadFile()
{
	TraceLog(("CFtpMultiSite::DownloadFile"));

	m_bUserAbort = false;
	m_nCurProcItem = 0;
	bool bResult = true;

	CString szBuf;

	if(!m_bIsHttpOnly) m_FtpClient.AttachObserver(this);

	for(std::list<ST_PACKAGE_INFO>::iterator it = m_lsPackage.begin(); it != m_lsPackage.end(); it++)
	{
		ST_PACKAGE_INFO stPackageInfo = *it;

		m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_STR001));

		if(!m_bIsHttpOnly)
		{
			CLogonInfo loginInfo(stPackageInfo.ip, stPackageInfo.port, stPackageInfo.userId, stPackageInfo.passwd);
			bResult = m_FtpClient.Login(loginInfo);
		}
		// 2012.03.02 ftp대신 http로 파일전송을 할 수 있도록 한다.
		else
		{
			m_objFileSvcClient.SetHttpServerInfo(stPackageInfo.ip.c_str(), stPackageInfo.port);
			bResult = m_objFileSvcClient.Login(stPackageInfo.userId.c_str(), stPackageInfo.passwd.c_str());
		}

		if(!bResult)
		{
			TraceLog(("IP:%s / PORT:%d / ID:%s /PW:%s",stPackageInfo.ip.c_str(),stPackageInfo.port,stPackageInfo.userId.c_str(),stPackageInfo.passwd.c_str()));

			// 동일서버에 해당되는 항목을 모두 실패처리한다
			for(; m_nCurProcItem<m_lcList.GetItemCount() ; m_nCurProcItem++)
			{
				CString strSiteId = m_lcList.GetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eSite]));
				if(strSiteId != stPackageInfo.siteId.c_str()) break;

				SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR002), CFtpModule::MakeErrorMsg(FTP_ERROR_CONNECT_FAIL));
			}

			continue;
		}

		if(!m_bIsHttpOnly)
		{
			m_FtpClient.SetResumeMode(false);
			if(m_FtpClient.ChangeWorkingDirectory("/") == FTP_ERROR)
			{
				bResult = false;

				// 동일서버에 해당되는 항목을 모두 실패처리한다
				for(; m_nCurProcItem<m_lcList.GetItemCount() ; m_nCurProcItem++)
				{
					CString strSiteId = m_lcList.GetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eSite]));
					if(strSiteId != stPackageInfo.siteId.c_str()) break;

					SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR002), CFtpModule::MakeErrorMsg(FTP_ERROR_CHANGE_DIRECTORY, (LPARAM)_T("/")));
				}

				m_FtpClient.Logout();
				continue;
			}
		}

		m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_STR008));

		PackageFileCheck(stPackageInfo.fullpath.c_str());

		TraceLog(("CFtpMultiSite::DownloadFile downloading start"));

		for(IterFileInfo Iter = m_mapLocal.begin(); Iter != m_mapLocal.end(); Iter++)
		{
			std::string sFileKey = Iter->first;
			TraceLog((_T("%s"), sFileKey.c_str()));

			P_FILE_INFO pInfo = (P_FILE_INFO)&(Iter->second);
			TraceLog(("pInfo [0x%X], %s, %s", pInfo, pInfo->location.c_str(), pInfo->name.c_str()));

			if(sFileKey.empty() || !pInfo) continue;

			TraceLog(("m_bOnlyAddFile [%d]", m_bOnlyAddFile));
			TraceLog(("pInfo->location.c_str() [%s]", pInfo->location.c_str()));
			TraceLog(("pInfo->name.c_str() [%s]", pInfo->name.c_str()));
			TraceLog(("m_lsDownFiles.Find() [%d]", m_lsDownFiles.Find((pInfo->location + pInfo->name).c_str())));

			if(m_bOnlyAddFile && !m_lsDownFiles.Find((pInfo->location + pInfo->name).c_str())) {
				TraceLog(("Already Exist"));			
				continue;
			}

			IterFileInfo IRe = m_mapRemote.find(pInfo->location + pInfo->name);
			TraceLog(("m_mapRemote.find"));

			bool bSkip = false;
			CString   strLocation = LoadStringById(IDS_FTPMULTISITE_STR011);
			ULONGLONG lLocalSize  = (pInfo->exist ? pInfo->size : 0);
			ULONGLONG lServerSize = (m_mapRemote.end() != IRe ? (IRe->second).size : 0);
			CString   strServerPath = pInfo->location.c_str();
			CString   strProgress = _T("0%");
			CString   strStatus   = LoadStringById(IDS_FTPMULTISITE_STR006);
			CString   strMessage;

			// 다운로드할 파일이 서버에 없는 경우
			if(m_mapRemote.end() == IRe)
			{
				strStatus   = LoadStringById(IDS_FTPMULTISITE_STR003);
				strMessage  = CFtpModule::MakeErrorMsg(FTP_ERROR_REMOTE_FILE_OPEN, (LPARAM)pInfo->name.c_str());
				bSkip = true;
				bResult = false;
			}

			bool bIsSameButModified = false; //skpark same_size_file_problem

			// 다운로드할 파일이 이미 로컬에 있는경우
			if(pInfo->exist /*&& (IRe->second).size == pInfo->size*/)
			{
				//skpark same_size_file_problem
				if(	m_FtpCallback ) {
					bIsSameButModified = this->isNewFile(pInfo);
					bSkip = !bIsSameButModified;
				}else{
					bSkip = true;
				}
				
				strLocation = LoadStringById(IDS_FTPMULTISITE_STR013);
				strProgress = _T("100%");
				strStatus   = LoadStringById(IDS_FTPMULTISITE_STR009);
				strMessage  = LoadStringById(IDS_FTPMULTISITE_MSG010);
			}

			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eLocation  ]), strLocation           );
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eProgress  ]), strProgress           );
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eLocalSize ]), ToFileSize(lLocalSize , 'A'));
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eServerSize]), ToFileSize(lServerSize, 'A'));
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eServerPath]), strServerPath         );
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eStatus    ]), strStatus             );
			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eMessage   ]), strMessage            );

			if(bSkip)
			{
				SetListItemStatus(m_nCurProcItem, strStatus, strMessage);
				m_nCurProcItem++;
				TraceLog(("CFtpMultiSite::DownloadFile download skip"));
				continue;
			}

			m_CurBytes = 0;
			m_FileBytes = (IRe->second).size;

			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eStatus]), LoadStringById(IDS_FTPMULTISITE_STR008));

			szBuf.Format(LoadStringById(IDS_FTPMULTISITE_MSG004), stPackageInfo.siteId.c_str(), pInfo->name.c_str());
			m_sttTitle.SetWindowText(szBuf);

			TraceLog(("CFtpMultiSite::DownloadFile download start"));

			// 부속파일처리 관련 수정
			CString strMakePath = pInfo->localpath.c_str();
			strMakePath.Replace(_T("/"), _T("\\"));
			MakeSureDirectoryPathExists(strMakePath);

			if(!m_bIsHttpOnly) 
			{
				bResult = m_FtpClient.DownloadFile(pInfo->location + pInfo->name, pInfo->localpath + pInfo->name, CRepresentation(CType::Image()), true);
			}
			else
			{
				try {
					bResult = m_objFileSvcClient.GetFile((pInfo->location + pInfo->name).c_str(), (pInfo->localpath + pInfo->name).c_str(), 0, (IProgressHandler*)this);
				}catch (CString& e){
					bResult = false;
				}

			}
			if(!bResult)
			{
				// 0000829: 다운로드가 실패하거나 중지하여 받다가 끊어진 파일은 로컬에서 자동으로 지워져야 한다.
				::DeleteFile((pInfo->localpath + pInfo->name).c_str());

				if(m_bUserAbort)
				{
					if(!m_bIsHttpOnly) 
					{
						m_FtpClient.Logout();
					}
					else 
					{
						m_objFileSvcClient.Logout();
					}
					return FALSE;
				}

				SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR002), CFtpModule::MakeErrorMsg(FTP_ERROR_DOWNLOADING, (LPARAM)pInfo->name.c_str()));
				m_nCurProcItem++;
				continue;
			}

			//skpark same_size_file_problem
			if(bIsSameButModified){
				ChangeFileTime(pInfo);
			}

			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eProgress]), _T("100%"));
			SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR005));
			m_nCurProcItem++;
		}

		TraceLog(("Logout()"));

		if(!m_bIsHttpOnly) 
		{
			m_FtpClient.Logout();
		}
		else
		{
			m_objFileSvcClient.Logout();
		}
	}

	TraceLog(("CFtpMultiSite::DownloadFile download complete"));

	if(!m_bIsHttpOnly) m_FtpClient.DetachObserver(this);

	if(bResult)
	{
		m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_MSG007));
	}
	else
	{
		m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_MSG011));
	}

	return bResult;
}

void CFtpMultiSite::PackageFileCheck(LPCTSTR szPackagePath)
{
	TraceLog(("CFtpMultiSite::PackageFileCheck"));

	m_mapLocal.clear();
	m_mapRemote.clear();

	TCHAR szDrive[MAX_PATH], szFile[MAX_PATH];
	_splitpath(szPackagePath, szDrive, NULL, szFile, NULL);

	CProfileManager objIniManager(szPackagePath);

	// 부속파일처리 관련 수정
	//CString szDefLoc = "/contents/";
	//szDefLoc.Format("/contents/%s/", szFile);

	CStringList lsPath;
	lsPath.AddTail("/contents/");

	CStringArray arrFileList;

	// 2011.05.02 템플릿의 배경이미지기능은 사용하지 않으며, 컨텐츠단위의 GUID구조에서 적용할 수 없음.
	//GetPrivateProfileString("HOST", "TEMPLATELIST", "", szTemp, sizeof(szTemp), szPackagePath);
	//CString szBuf = szTemp;

	//TraceLog(("CFtpMultiSite::PackageFileCheck local info start"));

	//int nPos = 0;
	//CString szToken = szBuf.Tokenize(",", nPos);
	//while(!szToken.IsEmpty())
	//{
	//	GetPrivateProfileString(szToken, "BGIMAGE", "", szTemp, sizeof(szTemp), szPackagePath);

	//	if(strlen(szTemp) > 0)
	//	{
	//		fileInfo.attrib = "";
	//		fileInfo.name = szTemp;

	//		// 로컬경로 설정
	//		fileInfo.localpath = szDrive;
	//		fileInfo.localpath += "\\";
	//		fileInfo.localpath += UBC_CONTENTS_PATH;

	//		// 서버경로설정
	//		fileInfo.location = "/contents/" + ;

	//		// (로컬경로 + 파일명 + 서버경로)를 가지고 키를 조합한다.
	//		std::string sFileKey = fileInfo.localpath;
	//		sFileKey += fileInfo.name;
	//		sFileKey += ";";
	//		sFileKey += fileInfo.location;

	//		// 이미 추가된 파일인지 체크
	//		if(m_mapLocal.find(sFileKey) == m_mapLocal.end())
	//		{
	//			CFileFind ff;
	//			if(ff.FindFile((fileInfo.localpath + fileInfo.name).c_str()))
	//			{
	//				ff.FindNextFile();
	//				fileInfo.exist = true;
	//				fileInfo.size = ff.GetLength();
	//			}
	//			else
	//			{
	//				fileInfo.exist = false;
	//				fileInfo.size = 0;
	//			}
	//			ff.Close();

	//			m_mapLocal.insert(PairFileInfo(sFileKey, fileInfo));
	//		}
	//	}

	//	szToken = szBuf.Tokenize(",", nPos);
	//}

	CString strContentsList = objIniManager.GetProfileString("HOST", "CONTENTSLIST");
	CString strEtcList      = objIniManager.GetProfileString("HOST", "ETCLIST"     );
	strContentsList += (!strContentsList.IsEmpty() && !strEtcList.IsEmpty() ? "," : "") + strEtcList;

	int nPos = 0;
	CString szToken;
	while(!(szToken = strContentsList.Tokenize(",", nPos)).IsEmpty())
	{
		ST_FILE_INFO fileInfo;

		// 부속파일처리 관련 수정
		CString strContentsId =         objIniManager.GetProfileString(szToken, "contentsId");
		CString strParentId   =         objIniManager.GetProfileString(szToken, "parentId"  );
		CString strFileName   =         objIniManager.GetProfileString(szToken, "FILENAME"  );
		BOOL    bIsPublic     =         objIniManager.GetProfileInt   (szToken, "isPublic"  );
		CString strFileMD5    =         objIniManager.GetProfileString(szToken, "MD5"       );
		fileInfo.sizeINI      = _atoi64(objIniManager.GetProfileString(szToken, "volume"    ));

		fileInfo.contentsId      = strContentsId;  //skpark same_size_file_problem
		fileInfo.lastModifiedTime = objIniManager.GetProfileString(szToken, "LastModifiedTime"       ); //skpark same_size_file_problem

		if(strFileName.IsEmpty()) continue;

		fileInfo.attrib = "";
		fileInfo.name = strFileName;

		CString strServerLocation = objIniManager.GetProfileString(szToken, "LOCATION");

		// 서버경로설정
		if(!strServerLocation.IsEmpty())
		{
			fileInfo.location = strServerLocation;

			if(!lsPath.Find(strServerLocation))
			{
				lsPath.AddTail(strServerLocation);
			}
		}
		else
		{
			fileInfo.location = "/contents/"
							  + (!strParentId.IsEmpty() ? strParentId : strContentsId)
							  + "/";
		}

		// 로컬경로 설정
		fileInfo.localpath = szDrive;
		fileInfo.localpath += "\\";
		fileInfo.localpath += UBC_CONTENTS_PATH;
		if(!strParentId.IsEmpty())	// 부속파일처리 관련 수정
		{
			CString strCompareStr = "/contents/" + strParentId + "/";
			CString strLocalPath = strServerLocation;
			if(strLocalPath.MakeLower().Find(strCompareStr.MakeLower()) == 0)
			{
				strLocalPath = strLocalPath.Mid(strCompareStr.GetLength());
				fileInfo.localpath += strLocalPath;
			}
		}

		// (로컬경로 + 파일명 + 서버경로)를 가지고 키를 조합한다.
		std::string sFileKey = fileInfo.localpath;
		sFileKey += fileInfo.name;
		sFileKey += ";";
		sFileKey += fileInfo.location;

		// 이미 추가된 파일인지 체크
		IterFileInfo Iter = m_mapLocal.find(sFileKey);
		if(m_mapLocal.end() == Iter)
		{
			fileInfo.exist = false;
			fileInfo.size = 0;

			CFileFind ff;
			BOOL bFind = ff.FindFile((fileInfo.localpath + fileInfo.name).c_str());
			if( bFind )
			{
				bFind = ff.FindNextFile();
				if( ff.GetLength() == fileInfo.sizeINI )
				{
					//TraceLog(("%s%s, ff.GetLength()=%d , fileInfo.sizeIni=%d", 
					//	fileInfo.localpath.c_str(),fileInfo.name.c_str(),(int)ff.GetLength(), (int)fileInfo.sizeINI));

					fileInfo.exist = true;
					fileInfo.size = ff.GetLength();
				}
			}
			ff.Close();

			fileInfo.isPublic = bIsPublic;
			fileInfo.md5 = strFileMD5;
			m_mapLocal.insert(PairFileInfo(sFileKey, fileInfo));

			arrFileList.Add((fileInfo.location + fileInfo.name).c_str());
		}
	}

	TraceLog(("CFtpMultiSite::PackageFileCheck local info complete"));

	TraceLog(("CFtpMultiSite::PackageFileCheck server info start"));

	m_bFtpEventUse = false;

	if(!m_bIsHttpOnly)
	{
		POSITION pos = lsPath.GetHeadPosition();
		while(pos)
		{
			CString szLoc = lsPath.GetNext(pos);
			if(szLoc.IsEmpty())	continue;

			TSpFTPFileStatusVector vecRemoteFile;
			if(!m_FtpClient.List(tstring(szLoc), vecRemoteFile, TRUE)) continue;

			for (int i=0; i<vecRemoteFile.size(); ++i)
			{
				TSpFTPFileStatus fileStat = vecRemoteFile[i];

				ST_FILE_INFO fileInfo;

				fileInfo.exist = true;
				fileInfo.attrib = fileStat->Attributes();
				fileInfo.location = szLoc;
				fileInfo.name = fileStat->Name();
				fileInfo.size = fileStat->Size();

				if(fileInfo.attrib[0] == 'd') continue;

				std::string sFileKey = szLoc;
				sFileKey += fileInfo.name;

				TraceLog(("sFileKey [%s]", sFileKey.c_str()));

				m_mapRemote.insert(PairFileInfo(sFileKey, fileInfo));
			}
		}
	}
	else
	{
		CFileServiceWrap::SFileList listRemoteFile;
		//if(!m_objFileSvcClient.List(szLoc, listRemoteFile)) continue;

		while(1) {
			int arrLen = arrFileList.GetSize();
			if(arrLen == 0) {
				break;
			}
			int old_remLen = listRemoteFile.GetSize();

			if(!m_objFileSvcClient.FilesInfo(arrFileList, listRemoteFile))
			{
				TraceLog(("ERROR : FilesInfo failed"));
				break;
			}
			int remLen = listRemoteFile.GetSize() - old_remLen;
			TraceLog(("FilesInfo End [arrFileList=%d], [listRemoteFile=%d]",arrLen, remLen));
			if(remLen >= arrLen || remLen == 0 ) {
				break;
			}

			// FilesInfo 의 결과가 요청한 파일의 갯수보다 작다.
			// listRemoteFile 에서 발견된 파일을 뺀 나머지 파일을 가지고, 새로운 arrFileList 를 만든다.
			// 이 새로운 arrFileList 의 크기를 저장해 둔다.
			// 다시 FilesInfo 를 시도한다.
			// 만약 또 작다면, 다시
			// listRemoteFile 에서 발견된 파일을 뺀 나머지 파일을 가지고, 새로운 arrFileList 를 만든다.
			// 이 새로운 arrFileList의 크기가 직전의 arrFileList 의 크기와 같다면 while문을 멈춘다.
			
			//CArray<int,int> removeIndexList;
			for(int j=arrLen-1; j>=0 ;j--) // 뒤에것부터 해야 지울수 있다.
			{
				std::string sFileKey2 = arrFileList[j];
				for(int i=old_remLen; i<remLen+old_remLen ;i++)
				{
					std::string sFileKey = listRemoteFile[i].strFullName;
					if(sFileKey == sFileKey2){
						TraceLog(("FilesInfo [%s],%d founded", sFileKey.c_str(),j));
						// 이 경우 arrFileList 에서 제거한다.
						//removeIndexList.Add(j);
						arrFileList.RemoveAt(j);
					}
				}
			}
			
			//for(int k=0; k<removeIndexList.GetCount() ;k++) {
			//	arrFileList.RemoveAt(removeIndexList[k]);
			//}

			int new_arrLen = arrFileList.GetSize();
			TraceLog(("%d th file not founded (previous=%d)", new_arrLen, arrLen));
			if(new_arrLen == 0 || arrLen == new_arrLen) {
				break;
			}
		}

		for(int i=0; i<listRemoteFile.GetCount() ;i++)
		{
			if(listRemoteFile[i].bIsDirectory) continue;

			ST_FILE_INFO fileInfo;

			fileInfo.exist = true;
			fileInfo.attrib = listRemoteFile[i].strAttributes;
			fileInfo.location = listRemoteFile[i].strDirectoryName;
			fileInfo.name = listRemoteFile[i].strName;
			fileInfo.size = (ULONGLONG)_ttoi64(listRemoteFile[i].strLength);

			std::string sFileKey = listRemoteFile[i].strFullName;

			TraceLog(("sFileKey [%s]", sFileKey.c_str()));

			m_mapRemote.insert(PairFileInfo(sFileKey, fileInfo));

		}
	}

	m_bFtpEventUse = true;

	TraceLog(("CFtpMultiSite::PackageFileCheck server info complete"));
}

void CFtpMultiSite::ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath)
{
	TraceLog(("CFtpMultiSite::OnBytesReceived start"));

	if(nFlag == CFileServiceWrap::FS_DOING)
	{
		m_CurBytes = uSendBytes;

		static DWORD nTick = GetTickCount();
		if(GetTickCount() - nTick > 300)
		{
			nTick = GetTickCount();

			TraceLog(("m_nCurProcItem [%d], m_CurBytes[%I64d], m_FileBytes[%I64d]", m_nCurProcItem, m_CurBytes, m_FileBytes));

			long lBytes = (m_FileBytes > 0 ? (m_CurBytes*1000)/m_FileBytes : 100);

			CString strBuf;
			strBuf.Format("%2.1f%%", float(lBytes)/10.0f);

			m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eProgress]), strBuf);
		}
	}

	TraceLog(("CFtpMultiSite::OnBytesReceived end"));
}

void CFtpMultiSite::OnBytesReceived(const TByteVector& vBuffer, long lReceivedBytes)
{
	if(!m_bFtpEventUse) return;

	TraceLog(("CFtpMultiSite::OnBytesReceived start"));

	m_CurBytes += lReceivedBytes;

	static DWORD nTick = GetTickCount();
	if(GetTickCount() - nTick > 300)
	{
		nTick = GetTickCount();

		TraceLog(("m_nCurProcItem [%d], m_CurBytes[%I64d], m_FileBytes[%I64d]", m_nCurProcItem, m_CurBytes, m_FileBytes));

		long lBytes = (m_FileBytes > 0 ? (m_CurBytes*1000)/m_FileBytes : 100);

		CString strBuf;
		strBuf.Format("%2.1f%%", float(lBytes)/10.0f);

		m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eProgress]), strBuf);
	}

	TraceLog(("CFtpMultiSite::OnBytesReceived end"));
}

void CFtpMultiSite::OnBytesSent(const TByteVector& vBuffer, long lSentBytes)
{
	TraceLog(("CFtpMultiSite::OnBytesSent"));

	m_CurBytes += lSentBytes;

	static DWORD nTick = GetTickCount();
	if(GetTickCount() - nTick > 300)
	{
		nTick = GetTickCount();

		long lBytes = (m_FileBytes > 0 ? (m_CurBytes*1000)/m_FileBytes : 100);

		CString strBuf;
		strBuf.Format("%2.1f%%", float(lBytes)/10.0f);

		m_lcList.SetItemText(m_nCurProcItem, m_lcList.GetColPos(m_szColum[eProgress]), strBuf);
	}
}

LRESULT CFtpMultiSite::OnCompleteFTP(WPARAM, LPARAM)
{
	TraceLog(("CFtpMultiSite::OnCompleteFTP"));

	if(m_pThread) WaitForSingleObject(m_pThread->m_hThread, 5000);
	m_pThread = NULL;

	GetDlgItem(IDOK    )->EnableWindow(TRUE);
	GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
	GetDlgItem(IDC_BN_RETRY)->EnableWindow(!m_bRet);

	// 성공했을 경우 자동 종료하도록 함.
	if(m_bRet)
	{
		if(m_bUserAbort)
		{
			m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_STR014));
		}
		else if(m_FtpStat == eUpload)
		{
			m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_MSG005));
			CButton* pAutoClose = (CButton*)GetDlgItem(IDC_KB_AUTO_CLOSE);
			if(pAutoClose && pAutoClose->GetCheck() == TRUE)
			{
				OnBnClickedOk();
			}
		}
		else if(m_FtpStat == eDownload)
		{
			m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_STR007));
			OnBnClickedOk();
		}
	}
	else
	{
		if(m_bUserAbort)
		{
			m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_STR014));
		}
		else if(m_FtpStat == eUpload  )
		{
			UbcMessageBox(LoadStringById(IDS_FTPMULTISITE_MSG012));
			m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_MSG012));
		}
		else if(m_FtpStat == eDownload)
		{
			UbcMessageBox(LoadStringById(IDS_FTPMULTISITE_MSG011));
			m_sttTitle.SetWindowText(LoadStringById(IDS_FTPMULTISITE_MSG011));
		}
	}

	return S_OK;
}

void CFtpMultiSite::InitList()
{
	TraceLog(("CFtpMultiSite::InitList"));

	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eSite         ] = LoadStringById(IDS_FTPMULTISITE_LST001);
	m_szColum[ePackage      ] = LoadStringById(IDS_FTPMULTISITE_LST010);
	m_szColum[eFileName     ] = LoadStringById(IDS_FTPMULTISITE_LST002);
	m_szColum[eLocation     ] = LoadStringById(IDS_FTPMULTISITE_LST003);
	m_szColum[eProgress     ] = LoadStringById(IDS_FTPMULTISITE_LST004);
	m_szColum[eLocalSize    ] = LoadStringById(IDS_FTPMULTISITE_LST005);
	m_szColum[eServerSize   ] = LoadStringById(IDS_FTPMULTISITE_LST006);
	m_szColum[eServerPath   ] = LoadStringById(IDS_FTPMULTISITE_LST009);
	m_szColum[eStatus       ] = LoadStringById(IDS_FTPMULTISITE_LST007);
	m_szColum[eMessage      ] = LoadStringById(IDS_FTPMULTISITE_LST008);

	m_lcList.InsertColumn(eSite      , m_szColum[eSite      ], LVCFMT_LEFT  ,  60);
	m_lcList.InsertColumn(ePackage   , m_szColum[ePackage   ], LVCFMT_LEFT  , 100);
	m_lcList.InsertColumn(eFileName  , m_szColum[eFileName  ], LVCFMT_LEFT  , 200);
	m_lcList.InsertColumn(eLocation  , m_szColum[eLocation  ], LVCFMT_CENTER,  60);
	m_lcList.InsertColumn(eProgress  , m_szColum[eProgress  ], LVCFMT_LEFT  ,  60);
	m_lcList.InsertColumn(eLocalSize , m_szColum[eLocalSize ], LVCFMT_RIGHT ,  80);
	m_lcList.InsertColumn(eServerSize, m_szColum[eServerSize], LVCFMT_RIGHT ,  80);
	m_lcList.InsertColumn(eServerPath, m_szColum[eServerPath], LVCFMT_LEFT  , 200);
	m_lcList.InsertColumn(eStatus    , m_szColum[eStatus    ], LVCFMT_CENTER,  60);
	m_lcList.InsertColumn(eMessage   , m_szColum[eMessage   ], LVCFMT_LEFT  , 400);

	// 제일 마지막에 할것...
	m_lcList.SetProgressBarCol(eProgress);
	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcList.SetSortEnable(false);
}

void CFtpMultiSite::FillList()
{
	TraceLog(("CFtpMultiSite::FillList"));

	for(std::list<ST_PACKAGE_INFO>::iterator it = m_lsPackage.begin(); it != m_lsPackage.end(); it++)
	{
		ST_PACKAGE_INFO stPackageInfo = *it;

		PackageFileCheck(stPackageInfo.fullpath.c_str());

		int nItemIdx = m_lcList.GetItemCount();

		for(IterFileInfo Iter = m_mapLocal.begin(); Iter != m_mapLocal.end(); Iter++)
		{
			std::string sFileKey = Iter->first;
			P_FILE_INFO pInfo = (P_FILE_INFO)&(Iter->second);

			if(sFileKey.empty() || !pInfo) continue;

			if( m_FtpStat == eDownload )
			{
				if(m_bOnlyAddFile && !m_lsDownFiles.Find((pInfo->location + pInfo->name).c_str())) continue;
			}

//			IterFileInfo IRe = m_mapRemote.find(pInfo->location + pInfo->name);

			CString strLocalLocation = pInfo->localpath.c_str();
			CString strChildPath;
			CString strContentsPath = UBC_CONTENTS_PATH;
			int nFindPos = strLocalLocation.MakeLower().Find(strContentsPath.MakeLower());
			if(nFindPos >= 0)
			{
				strLocalLocation = pInfo->localpath.c_str();
				strChildPath = strLocalLocation.Mid(nFindPos + strContentsPath.GetLength());
			}

			m_lcList.InsertItem(nItemIdx, stPackageInfo.siteId.c_str());
			m_lcList.SetItemText(nItemIdx, m_lcList.GetColPos(m_szColum[ePackage  ]), stPackageInfo.packageName.c_str());
			m_lcList.SetItemText(nItemIdx, m_lcList.GetColPos(m_szColum[eFileName ]), strChildPath + pInfo->name.c_str());
			m_lcList.SetItemText(nItemIdx, m_lcList.GetColPos(m_szColum[eProgress ]), _T("0%")                   );
			m_lcList.SetItemText(nItemIdx, m_lcList.GetColPos(m_szColum[eLocalSize]), ToFileSize(pInfo->size, 'A'));
			m_lcList.SetItemText(nItemIdx, m_lcList.GetColPos(m_szColum[eStatus   ]), LoadStringById(IDS_FTPMULTISITE_STR006));
			m_lcList.SetItemData(nItemIdx, (DWORD_PTR)pInfo);

			nItemIdx++;
		}

		if( m_FtpStat == eUpload )
		{
			ULONGLONG lFileSize = 0;
			CFileFind ff;
			if(ff.FindFile(stPackageInfo.fullpath.c_str()))
			{
				ff.FindNextFile();
				lFileSize = ff.GetLength();
			}
			ff.Close();

			m_lcList.InsertItem(nItemIdx, stPackageInfo.siteId.c_str());
			m_lcList.SetItemText(nItemIdx, m_lcList.GetColPos(m_szColum[ePackage  ]), stPackageInfo.packageName.c_str());
			m_lcList.SetItemText(nItemIdx, m_lcList.GetColPos(m_szColum[eFileName ]), stPackageInfo.file.c_str());
			m_lcList.SetItemText(nItemIdx, m_lcList.GetColPos(m_szColum[eProgress ]), _T("0%")                  );
			m_lcList.SetItemText(nItemIdx, m_lcList.GetColPos(m_szColum[eLocalSize]), ToFileSize(lFileSize, 'A') );
			m_lcList.SetItemData(nItemIdx, (DWORD_PTR)&stPackageInfo);
		}
	}
}

void CFtpMultiSite::OnOK() {}
void CFtpMultiSite::OnCancel() {}

void CFtpMultiSite::OnBnClickedOk()
{
	CDialog::OnOK();
}

void CFtpMultiSite::OnBnClickedCancel()
{
	TraceLog(("CFtpMultiSite::OnBnClickedCancel"));

	if(!m_bIsHttpOnly) 
	{
		m_FtpClient.Abort();
	}
	else
	{
		m_objFileSvcClient.SetUserCancel();
	}
	m_bUserAbort = true;

	SetListItemStatus(m_nCurProcItem, LoadStringById(IDS_FTPMULTISITE_STR004), LoadStringById(IDS_FTPMULTISITE_MSG008));
	GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
}

void CFtpMultiSite::SetListItemStatus(int nItem, CString strStatus, CString strMessage)
{
	TraceLog(("CFtpMultiSite::SetListItemStatus"));

	if(strStatus == LoadStringById(IDS_FTPMULTISITE_STR002) || strStatus == LoadStringById(IDS_FTPMULTISITE_STR003))
	{
		m_lcList.SetRowColor(m_nCurProcItem, RGB(215,215,255), RGB(255,0,0));
	}
	else
	{
		m_lcList.SetRowColor(m_nCurProcItem, RGB(215,215,255), m_lcList.GetTextColor());
	}

	m_lcList.SetItemText(nItem, m_lcList.GetColPos(m_szColum[eStatus]), strStatus);
	m_lcList.SetItemText(nItem, m_lcList.GetColPos(m_szColum[eMessage]), strMessage);

	m_lcList.EnsureVisible(nItem+10, FALSE);
	m_lcList.RedrawItems(nItem, nItem);
}

BOOL CFtpMultiSite::MakeServerDir(CString strLocation)
{
	if(m_bIsHttpOnly) return TRUE;

	int nPos = 0;
	CString strToken;
	CString strPath = _T("/");
	while(!(strToken = strLocation.Tokenize("/", nPos)).IsEmpty())
	{
		strPath += strToken + _T("/");
		if(m_FtpClient.MakeDirectory((LPCTSTR)strPath) == FTP_ERROR)
		{
			return FALSE;
		}
	}

	return TRUE;
}

void CFtpMultiSite::OnBnClickedBnRetry()
{
	m_bRet = FALSE;
	m_bUserAbort = FALSE;

	// 업로드할 파일의 목록과 용량을 단말과 서버에서 확인하고 있습니다.
	CWaitMessageBox wait(LoadStringById(IDS_FTPMULTISITE_MSG013));

	m_lcList.DeleteAllItems();
	FillList();

	m_pThread = AfxBeginThread(ThreadProc, (LPVOID)this);

	GetDlgItem(IDOK    )->EnableWindow(FALSE);
	GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
	GetDlgItem(IDC_BN_RETRY)->EnableWindow(FALSE);
}

//skpark same_size_file_problem 2014.06.12
bool	
CFtpMultiSite::isNewFile(CFtpMultiSite::ST_FILE_INFO* pInfo)
{
	CString target = (pInfo->localpath + pInfo->name).c_str();
	TraceLog(("isNewFile?=%s", target));

	if(pInfo->lastModifiedTime.length()==0){
		return false;
	}

	CString modifiedTime = pInfo->lastModifiedTime.c_str();
	
	HANDLE hFile = ::CreateFileA(target, 
								GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile==INVALID_HANDLE_VALUE) {
		TraceLog(("%s file open error", target));
		return false;		
	}
	FILETIME cTime, aTime, wTime, ft;
		
	if(TimeStringToFileTime(modifiedTime,&ft)){
		if(::GetFileTime(hFile, &cTime, &aTime, &wTime)){
			TraceLog(("%s file time get file time succeed", target));
			if(aTime.dwHighDateTime < ft.dwHighDateTime || 
				(aTime.dwHighDateTime == ft.dwHighDateTime && aTime.dwLowDateTime < ft.dwLowDateTime) ) {
				TraceLog(("Yes %s file is new (%s)", target, modifiedTime));
				CloseHandle(hFile);
				return true;			
			}
			TraceLog(("No, %s file is not new (%s)", target, modifiedTime));
			CloseHandle(hFile);
			return false;
		}
		TraceLog(("%s file get file time failed (ErrorCode=%d)",target, ::GetLastError()));
	}else{
		TraceLog(("%s TimeStringToFileTime", modifiedTime));
	}
	CloseHandle(hFile);
	return false;
}

//skpark same_size_file_problem 2014.06.12
bool	
CFtpMultiSite::ChangeFileTime(CFtpMultiSite::ST_FILE_INFO* pInfo)
{ 
	// skpark same_size_file_problem 해당 contents 의 file 시간을 LastModifiedTime 과 일치시킨다.

	CString target = (pInfo->localpath + pInfo->name).c_str();
	CString modifiedTime = pInfo->lastModifiedTime.c_str();

	TraceLog(("ChangeFileTime target = %s", target));
	TraceLog(("ChangeFileTime modifiedTime = %s", modifiedTime));

	HANDLE hFile = ::CreateFileA(target, 
								GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile==INVALID_HANDLE_VALUE) {
		TraceLog(("%s file open error", target));
		return false;		
	}
	FILETIME ft;
	if(TimeStringToFileTime(modifiedTime,&ft)){
		if(::SetFileTime(hFile,&ft,&ft,&ft)){
			TraceLog(("%s file set file time succeed", target));
			CloseHandle(hFile);
			return true;
		}else{
			TraceLog(("%s file set file time failed (ErrorCode=%d)",target, ::GetLastError()));
		
		}
	}
	CloseHandle(hFile);
	TraceLog(("%s file set file time failed", target));
	return false;
}


bool CFtpMultiSite::TimeStringToFileTime(CString& timeStr,  LPFILETIME pft)
{
	TraceLog(("TimeStringToFileTime=%s", timeStr));

	if(timeStr.IsEmpty() || timeStr.GetLength() != 19) {
		return false;
	}

	struct tm	c_tm;
	memset(&c_tm, 0x00, sizeof(struct tm));

	sscanf(timeStr.GetBuffer(), "%04d/%02d/%02d %02d:%02d:%02d",
		&c_tm.tm_year, &c_tm.tm_mon, &c_tm.tm_mday,
		&c_tm.tm_hour, &c_tm.tm_min, &c_tm.tm_sec);

	c_tm.tm_year -= 1900;
	c_tm.tm_mon -= 1;
	
	time_t now1;
	time(&now1);
	struct tm* localTime = localtime(&now1);
	c_tm.tm_isdst = localTime->tm_isdst;

	time_t a_time = mktime(&c_tm);

	if(a_time <= 0) {
		TraceLog(("Time change error"));
		return false;
	}

	CTime debugTime(a_time);
	TraceLog(("Set TimeStringToFileTime=%04d/%02d/%02d %02d:%02d:%02d"
						, debugTime.GetYear()
						, debugTime.GetMonth()
						, debugTime.GetDay()
						, debugTime.GetHour()
						, debugTime.GetMinute()
						, debugTime.GetSecond()));
	
	 // Note that LONGLONG is a 64-bit value
	 LONGLONG ll;
	 ll = Int32x32To64(a_time, 10000000) + 116444736000000000;
	 pft->dwLowDateTime = (DWORD)ll;
	 pft->dwHighDateTime = ll >> 32;

	 return true;
}

