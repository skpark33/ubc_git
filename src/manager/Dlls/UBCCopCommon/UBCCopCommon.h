#pragma once

#include "common\ubcdefine.h"

AFX_EXT_API void CopComRelease();
AFX_EXT_API int RunPackageRevDlg(ESrcApp, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, int nAuthority, LPCTSTR strUserId, LPCTSTR strSiteId, LPCTSTR strIniPath, bool&, LPCTSTR, LPCTSTR, bool bCanAdd=true);
AFX_EXT_API void RunFlashView(LPCTSTR, LPCTSTR);
AFX_EXT_API BOOL RunFolderArchiver(CString strInFolderPath, CString strTargetFileName);
AFX_EXT_API BOOL GetSelectUsers(CString& strSelUserId, CString& strSelUserName);
AFX_EXT_API BOOL GetSelectContents(CString strCustomer, CString& strSelContentsId, CString& strSelContentsName);
AFX_EXT_API BOOL GetSelectSite(CString strCustomer, CString& strSelSiteId, CString& strSelSiteName);
AFX_EXT_API void PrepareSiteList();
AFX_EXT_API BOOL GetTimeZoneMinute(CString& strTimeZoneName, LONG& nMinute);
AFX_EXT_API CString GetLocaleName();