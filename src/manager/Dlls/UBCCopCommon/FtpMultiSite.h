#pragma once

#ifndef IDD_FTP_MUTISITE
#define IDD_FTP_MUTISITE	25008
#endif//IDD_FTP_MUTISITE

#include <map>
#include <list>
#include <string>
#include "common\ProgressCtrlEx.h"
#include "common\libFTPClient\FTPClient.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "common\UTBListCtrlEx.h"
#include "common\libFileServiceWrap/FileServiceWrapDll.h"
#ifdef _DEBUG
	#pragma comment(lib, "FileServiceWrap_d.lib")
#else
	#pragma comment(lib, "FileServiceWrap.lib")
#endif

using namespace nsFTP;


// CFtpMultiSite dialog
class AFX_EXT_CLASS CFtpMultiSite
	: public CDialog
	, public nsFTP::CFTPClient::CNotification
	, public CFileServiceWrap::IProgressHandler // 2012.03.02 ftp대신 http로 파일전송을 할 수 있도록 한다.
{
	DECLARE_DYNAMIC(CFtpMultiSite)
public:
	enum { eUnkown, eUpload, eDownload };


typedef	struct tagPACKAGE_INFO
	{
		std::string packageName;
		int port;
		std::string fullpath;
		std::string drive;
		std::string file;
		std::string siteId;
		std::string ip;
		std::string userId;
		std::string passwd;
	} ST_PACKAGE_INFO, *P_PACKAGE_INFO;

typedef	struct tagFILE_INFO
	{
		bool exist;
		ULONGLONG size;
		std::string name;
		std::string attrib;
		std::string location;	// 서버경로
		std::string localpath;	// 로컬경로
		bool isPublic;
		std::string md5;
		ULONGLONG sizeINI;
		std::string contentsId; //skpark same_size_file_problem
		std::string lastModifiedTime; //skpark same_size_file_problem

		tagFILE_INFO()
		{
			exist = false;
			size = 0;
			isPublic = false;
			sizeINI = 0;
		}
	} ST_FILE_INFO, *P_FILE_INFO;

typedef std::map<std::string, ST_FILE_INFO>		MapFileInfo;
typedef std::pair<std::string, ST_FILE_INFO>	PairFileInfo;
typedef MapFileInfo::const_iterator				IterFileInfo;


	interface IFtpCallback {  //skpark same_size_file_problem 2014.06.12
		virtual bool	isModified(const char* contentsId) = 0;
		virtual bool	ChangeFileTime(const char* contentsId) = 0;
		virtual bool	ChangeModifiedFlag(bool flag) = 0;
	};


	void OnlyAddFile(BOOL =TRUE);
	BOOL AddSite(LPCTSTR, LPCTSTR);
	BOOL AddDownloadFile(LPCTSTR);
	BOOL RunFtp(int stat);

private:
	bool m_bFtpEventUse;
	void PackageFileCheck(LPCTSTR);
	BOOL MakeServerDir(CString strLocation);

	BOOL UploadFile();
	BOOL DownloadFile();

	static UINT	ThreadProc(LPVOID param);

	// 2012.03.02 ftp대신 http로 파일전송을 할 수 있도록 한다.
	virtual void ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath);
	virtual void OnBytesReceived(const TByteVector& /*vBuffer*/, long /*lReceivedBytes*/);
	virtual void OnBytesSent(const TByteVector& /*vBuffer*/, long /*lSentBytes*/);
public:
private:
	BOOL m_bRet;
	BOOL m_bOnlyAddFile;
	int m_FtpStat;
	std::list<ST_PACKAGE_INFO> m_lsPackage;
	CStringList m_lsDownFiles;

	// 2012.03.02 ftp대신 http로 파일전송을 할 수 있도록 한다.
	BOOL m_bIsHttpOnly;
	CFileServiceWrap m_objFileSvcClient;
	CFTPClient m_FtpClient;

	MapFileInfo m_mapLocal;
	MapFileInfo m_mapRemote;

	CWinThread* m_pThread;

	ULONGLONG m_CurBytes;
	ULONGLONG m_FileBytes;

	CStatic m_sttTitle;

	enum { eSite, ePackage, eFileName, eLocation, eProgress, eLocalSize, eServerSize, eServerPath, eStatus, eMessage, eMaxCol };
	int m_nCurProcItem;
	CUTBListCtrlEx m_lcList;
	CString m_szColum[eMaxCol];
	void InitList();
	void FillList();
	void SetListItemStatus(int nItem, CString strStatus, CString strMessage=_T(""));

	bool m_bUserAbort;
	IFtpCallback*	m_FtpCallback;  //skpark same_size_file_problem 2014.06.12

public:
	CFtpMultiSite(BOOL bIsHttpOnly, CWnd* pParent = NULL);   // standard constructor
	virtual ~CFtpMultiSite();

// Dialog Data
	enum { IDD = IDD_FTP_MUTISITE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnCompleteFTP(WPARAM, LPARAM);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedBnRetry();

	void	SetFTPCallback(IFtpCallback* p) { m_FtpCallback = p; } //skpark same_size_file_problem 2014.06.12
	bool	isNewFile(CFtpMultiSite::ST_FILE_INFO* pInfo); //skpark same_size_file_problem 2014.06.12
	bool	ChangeFileTime(CFtpMultiSite::ST_FILE_INFO* pInfo); //skpark same_size_file_problem 2014.06.12
	bool	TimeStringToFileTime(CString& timeStr,  LPFILETIME pft); //skpark same_size_file_problem 2014.06.12


};
