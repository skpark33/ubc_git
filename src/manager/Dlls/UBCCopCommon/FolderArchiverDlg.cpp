// FolderArchiverDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "UBCCopCommon.h"
#include "FolderArchiverDlg.h"

#define WM_COMPLETE_ARCHIVE (WM_USER+1)

// CFolderArchiverDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFolderArchiverDlg, CDialog)

CFolderArchiverDlg::CFolderArchiverDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFolderArchiverDlg::IDD, pParent)
	, m_strInFolderPath(_T(""))
	, m_strTotalSize(_T(" 0 Byte"))
	, m_strCurSize(_T(" 0 Byte"))
{
	m_bRunning = FALSE;
	m_pThread = NULL;
}

CFolderArchiverDlg::~CFolderArchiverDlg()
{
}

void CFolderArchiverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_PATH, m_strInFolderPath);
	DDX_Text(pDX, IDC_TXT_TOTAL_SIZE, m_strTotalSize);
	DDX_Text(pDX, IDC_TXT_CUR_SIZE, m_strCurSize);
	DDX_Control(pDX, IDC_PROG_ARCHIVE, m_ctrlProgress);
}


BEGIN_MESSAGE_MAP(CFolderArchiverDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CFolderArchiverDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CFolderArchiverDlg::OnBnClickedCancel)
	ON_WM_DESTROY()
	ON_MESSAGE(WM_COMPLETE_ARCHIVE, OnCompleteArchive)
END_MESSAGE_MAP()


// CFolderArchiverDlg 메시지 처리기입니다.

void CFolderArchiverDlg::OnBnClickedOk()
{
//	OnOK();
}

void CFolderArchiverDlg::OnBnClickedCancel()
{
	m_bRunning = FALSE;
	//OnCancel();
}

BOOL CFolderArchiverDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_strInFolderPath.Trim(_T("\\"));
	m_strInFolderPath.ReverseFind(_T('\\'));

	m_ctrlProgress.SetStyle(PROGRESS_TEXT);
	m_ctrlProgress.SetRange(0, 100);
	m_ctrlProgress.SetText(_T("0%%"));
	m_ctrlProgress.SetPos(0);

	m_pThread = AfxBeginThread(ThreadProc, (LPVOID)this);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

UINT CFolderArchiverDlg::ThreadProc(LPVOID param)
{
	CFolderArchiverDlg* pDlg = (CFolderArchiverDlg*)param;
	if(!pDlg)
	{
		AfxEndThread(0);
		return 0;
	}

	pDlg->m_bRunning = TRUE;
	if(pDlg->FolderArchiving(pDlg->m_strInFolderPath))
	{
		pDlg->PostMessage(WM_COMPLETE_ARCHIVE, 1);
	}
	else
	{
		pDlg->PostMessage(WM_COMPLETE_ARCHIVE, 0);
	}

	AfxEndThread(0);

	return 0;
}

BOOL CFolderArchiverDlg::FolderArchiving(CString strInPath)
{
	// 전체 용량을 구한다.
	ULONGLONG ulFolderSize = GetFolderSize(strInPath);
	m_strTotalSize = _T(" ") + ::MakeCurrency(ulFolderSize) + _T(" Byte");
	SetDlgItemText(IDC_TXT_TOTAL_SIZE, m_strTotalSize);

	TCHAR drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(strInPath, drive, path, filename, ext);
	CString strSrcPath;
	strSrcPath.Format(_T("%s%s"), drive, path);
	strSrcPath.Trim(_T('\\'));

	CString strFolderName;
	strFolderName.Format(_T("%s%s"), filename, ext);

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);
	_tsplitpath(szModule, drive, path, filename, ext);

	CString strTarExePath;
	strTarExePath.Format(_T("%s%star.exe"), drive, path);

	CString strWorkingPath;
	strWorkingPath.Format(_T("%s\\%s"), drive, UBC_CONTENTS_PATH);

	if(m_strTargetFileName.IsEmpty())
	{
		m_strTargetFileName = strFolderName + _T(".tar");
	}

	CString strCmdLine;
	strCmdLine.Format(_T(" -C \"%s\" -cvf \"%s\" \"%s\"")
					, strSrcPath
					, m_strTargetFileName
					, strFolderName
					);

	STARTUPINFO si = {0};
	si.cb = sizeof(STARTUPINFO);
	si.wShowWindow = SW_HIDE;
	si.dwFlags = STARTF_USESHOWWINDOW;	// 콘솔창이 보이지 않도록 한다

	PROCESS_INFORMATION pi;

	if(!CreateProcess((LPSTR)(LPCTSTR)strTarExePath //"C:\\Documents and Settings\\Administrator\\바탕 화면\\win32_tar_gzip\\tar.exe"
					, (LPSTR)(LPCTSTR)strCmdLine //" -cvf 임시폴더.tar 임시폴더"
					, NULL
					, NULL
					, FALSE
					, 0
					, NULL
					, (LPSTR)(LPCTSTR)strWorkingPath
					, &si
					, &pi
					) )
	{
		DWORD dwErrNo = GetLastError();

		LPVOID lpMsgBuf = NULL;
		FormatMessage(  FORMAT_MESSAGE_ALLOCATE_BUFFER
					  | FORMAT_MESSAGE_IGNORE_INSERTS 
					  | FORMAT_MESSAGE_FROM_SYSTEM     
					  ,	NULL
					  , dwErrNo
					  , 0
					  , (LPTSTR)&lpMsgBuf
					  , 0
					  , NULL );

		CString strErrMsg;
		if(!lpMsgBuf)
		{
			strErrMsg.Format(_T("Unknow Error - CreateProcess [%d]"), dwErrNo);
		}
		else
		{
			strErrMsg.Format(_T("%s [%d]"), (LPCTSTR)lpMsgBuf, dwErrNo);
		}

		LocalFree( lpMsgBuf );

		UbcMessageBox(strErrMsg);

		return FALSE;
	}

	ULONGLONG ulTarSize = 0;
	m_strCurSize = _T(" 0 Byte");
	SetDlgItemText(IDC_TXT_CUR_SIZE, m_strCurSize);

	m_ctrlProgress.SetRange32(0, 100);
	m_ctrlProgress.SetPos(0);

	while(::WaitForSingleObject( pi.hProcess, 0) == WAIT_TIMEOUT)
	{
		if(!m_bRunning)
		{
			TerminateProcess(pi.hProcess, 0);
			::WaitForSingleObject( pi.hProcess, 5000);
			
			CFileFind ff;
			if(ff.FindFile(strWorkingPath + m_strTargetFileName))
			{
				DeleteFile(strWorkingPath + m_strTargetFileName);
			}

			return FALSE;
		}

		if(ulFolderSize <= 0) continue;

		CFileFind ff;
		if(ff.FindFile(strWorkingPath + m_strTargetFileName))
		{
			ff.FindNextFile();
			ulTarSize = ff.GetLength();
			m_strCurSize = _T(" ") + ::MakeCurrency(ulTarSize) + _T(" Byte");
			SetDlgItemText(IDC_TXT_CUR_SIZE, m_strCurSize);
		}
		ff.Close();

		int nRate = (ulTarSize * 100) / ulFolderSize;

		m_ctrlProgress.SetText(::ToString(nRate) + _T("%"));
		m_ctrlProgress.SetPos(nRate);

		Sleep(500);
	}

	m_ctrlProgress.SetText(::ToString(100) + _T("%"));
	m_ctrlProgress.SetPos(100);

	return TRUE;
}

ULONGLONG CFolderArchiverDlg::GetFolderSize(CString strInPath)
{
	if(strInPath.IsEmpty()) return 0;

	ULONGLONG ulTotalSize = 0;

	CFileFind ff;
	BOOL bFindIt = ff.FindFile(strInPath + "\\*.*");
	while(bFindIt)
	{
		bFindIt = ff.FindNextFile();

		if(ff.IsDots()) continue;
		if(ff.IsDirectory())
		{
			ulTotalSize += GetFolderSize(ff.GetFilePath());
		}
		else
		{
			ulTotalSize += ff.GetLength();
		}
	}

	ff.Close();

	return ulTotalSize;
}

void CFolderArchiverDlg::OnDestroy()
{
	CDialog::OnDestroy();

	if(m_pThread)
	{
		DWORD dwRet = WaitForSingleObject(m_pThread->m_hThread, 5000);
	}

	m_pThread = NULL;
}

LRESULT CFolderArchiverDlg::OnCompleteArchive(WPARAM wParam, LPARAM lParam)
{
	if(m_pThread) WaitForSingleObject(m_pThread->m_hThread, 5000);
	m_pThread = NULL;

	// 성공했을 경우 자동 종료하도록 함.
	if(wParam)
	{
		EndDialog(IDOK);
	}
	else
	{
		EndDialog(IDCANCEL);
	}

	return S_OK;
}
