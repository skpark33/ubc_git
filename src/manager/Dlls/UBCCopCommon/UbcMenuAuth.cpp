#include "StdAfx.h"
#include "UBCCopCommon\CopModule.h"
#include "common/TraceLog.h"
#include "UbcMenuAuth.h"

CUbcMenuAuth* CUbcMenuAuth::_instance;
CCriticalSection CUbcMenuAuth::_cs;
CUbcMenuAuth* CUbcMenuAuth::GetInstance()
{
	_cs.Lock();
	if(_instance == NULL)
	{
		_instance = new CUbcMenuAuth();
	}
	_cs.Unlock();

	return _instance;
}

void CUbcMenuAuth::ClearInstance()
{
	_cs.Lock();
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
	_cs.Unlock();
}

CUbcMenuAuth::CUbcMenuAuth()
{
	m_bInitialized = FALSE;
}

CUbcMenuAuth::~CUbcMenuAuth(void)
{
}

void CUbcMenuAuth::InitAuthData(CString strUserId, CString strCustomer)
{
	m_strUserId = strUserId;
	m_strCustomer = strCustomer;

	m_bInitialized = TRUE;
	m_mapMenuAuth.RemoveAll();

	MenuAuthInfoList listMenuAuth;

	CCopModule::GetObject()->GetMenuAuthList(strUserId, strCustomer, listMenuAuth);

#pragma message("CUbcMenuAuth 생성자 : todo 서버에서 가져오기가 실패하는 경우 Default 권한 설정") 
	// 서버에서 가져오기가 실패하는 경우 Default 권한 설정
	if(listMenuAuth.GetCount() <= 0)
	{
		//CProfileManager objIniManager(m_strIniPath);
		//int nCodeCnt = objIniManager.GetProfileInt(_T("HEADER"), _T("CODE_COUNT"));
		//for(int i=0; i<nCodeCnt ;i++)
		//{
		//	CString strEntry;
		//	strEntry.Format(_T("CODE_%d"), i);
		//	TCHAR buf[1024]={0};

		//	SMenuAuthInfo info;

		//	info.strMgrId        = objIniManager.GetProfileString(strEntry, _T("MgrId"       ));
		//	info.strCodeId       = objIniManager.GetProfileString(strEntry, _T("CodeId"      ));
		//	info.strCategoryName = objIniManager.GetProfileString(strEntry, _T("CategoryName"));
		//	info.strEnumString   = objIniManager.GetProfileString(strEntry, _T("EnumString"  ));
		//	info.nEnumNumber     = objIniManager.GetProfileInt   (strEntry, _T("EnumNumber"  ));
		//	info.bVisible        = objIniManager.GetProfileInt   (strEntry, _T("Visible"     ), 1);
		//	info.nDisplayOrder   = objIniManager.GetProfileInt   (strEntry, _T("DisplayOrder"));

		//	m_listMenuAuth.AddTail(info);
		//}
	}

	POSITION pos = listMenuAuth.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SMenuAuthInfo info = listMenuAuth.GetNext(pos);

		//CString strMenuAuth;
		//if(info.cAuth) strMenuAuth += _T("C");
		//if(info.rAuth) strMenuAuth += _T("R");
		//if(info.uAuth) strMenuAuth += _T("U");
		//if(info.dAuth) strMenuAuth += _T("D");
		//if(info.aAuth) strMenuAuth += _T("A");
		//m_mapMenuAuth[info.menuName] = strMenuAuth;
		m_mapMenuAuth[info.menuId] = info.menuName;
	}
}

BOOL CUbcMenuAuth::GetEnableValue(CString strMenuId)
{
//	TraceLog(("GetEnableValue [%s]", strMenuId));

	// 초기화가 안된경우 무조건 true 리턴
	if(!m_bInitialized) return TRUE;

	// 수퍼계정은 무조건 전체 권한을 갖도록 한다.
	if(m_strUserId == "super") return TRUE;

	CString strMenuName = m_mapMenuAuth[strMenuId];
//	TraceLog(("MenuName [%s]", strMenuName));

	return !(strMenuName.IsEmpty());

	// 권한이 설정되어 있지 않은 경우
	//if(strMenuAuth.IsEmpty()) return bDefault;
	//return (strMenuAuth.FindOneOf(strAuth) >= 0);
}
