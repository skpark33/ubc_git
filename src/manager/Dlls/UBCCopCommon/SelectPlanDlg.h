#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "afxdtctl.h"
#include "dp_daily_plan_table.h"

#ifndef IDD_SELECT_PLAN_DLG
#define IDD_SELECT_PLAN_DLG 25296
#endif

// CSelectPlanDlg 대화 상자입니다.

class AFX_EXT_CLASS CSelectPlanDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectPlanDlg)

typedef struct tagLIST_FILTER
{
	CString strBpName;
	CString strTag;
	CString strRegister;
	CString strPackageId;
	CTime   tmStartDate;
	CTime   tmEndDate;
	CString strHostId;
	CString strHostName;
} ST_LIST_FILTER, *P_LIST_FILTER;

public:
	CSelectPlanDlg(LPCTSTR szCustomer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSelectPlanDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SELECT_PLAN_DLG };
	enum { eSiteName, ePlan, eDownloadTime, ePeriod, eRegDate, eRegUser, eDesc, eMaxCol };

	CString						m_strCustomer;
	int							m_nAuthority;
	CString						m_strSiteId;
	CString						m_strIniPath;

	void						SetSingleSelection(bool bSingleSelect) { m_bSingleSelect=bSingleSelect; };
	BroadcastingPlanInfoList&	GetSelectedList() { return m_listSelectBpInfo; };

protected:
	bool						m_bSingleSelect;
	BroadcastingPlanInfoList	m_listSelectBpInfo;

	CDateTimeCtrl				m_dtFilterStart;
	CDateTimeCtrl				m_dtFilterEnd;
	CUTBListCtrlEx				m_lcList;
	CHoverButton				m_bnRefresh;
	CHoverButton				m_bnSelect;
	CHoverButton				m_bnCancel;

	CString						m_szColum[eMaxCol];
	BroadcastingPlanInfoList	m_lsInfoList;

	void						InitList();
	ST_LIST_FILTER				m_stListFilter;
	CString						m_strHostId;
	void						RefreshList();
	CDp_daily_plan_table		m_dpDailyPlan;
	void						RefreshDailyPlan();
	CMapStringToPtr				m_mapTimePlanInfo;
	TimePlanInfoList*			GetTimePlanInfo(CString strBpId, BOOL bForceCall);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonFilterHost();
	afx_msg void OnBnClickedButtonPlanRefresh();
	afx_msg void OnBnClickedButtonFilterRegister();
	afx_msg void OnBnClickedButtonFilterPackage();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnNMClickListPlan(NMHDR *pNMHDR, LRESULT *pResult);
};
