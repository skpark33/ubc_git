#pragma once

#include <afxmt.h>

#define InitAuth(x,y) CUbcMenuAuth::GetInstance()->InitAuthData(x,y)
#define IsAuth(x) CUbcMenuAuth::GetInstance()->GetEnableValue(x)

class AFX_EXT_CLASS CUbcMenuAuth
{
private:
	static CUbcMenuAuth* _instance;
	static CCriticalSection	_cs;

	CMapStringToString	m_mapMenuAuth;

	CString				m_strUserId;
	CString				m_strCustomer;

	BOOL				m_bInitialized;

public:
	CUbcMenuAuth();
	~CUbcMenuAuth(void);

	static CUbcMenuAuth* GetInstance();
	static void		     ClearInstance();

	void				 InitAuthData(CString strUserId, CString strCustomer=_T(""));
	BOOL				 GetEnableValue(CString strMenuId);
};
