#include "StdAfx.h"
#include "WaitMessageBox.h"
#include "WaitMessageBoxCore.h"

int UbcMessageBox(LPCTSTR lpszText, UINT nType/* = MB_OK*/, UINT nIDHelp/* = 0*/)
{
	CWaitMessageBoxCore::GetInstance()->Hide();
	return AfxMessageBox(lpszText, nType, nIDHelp);
}

int UbcMessageBox(UINT nIDPrompt, UINT nType /*= MB_OK*/, UINT nIDHelp /*= (UINT)-1*/)
{
	CWaitMessageBoxCore::GetInstance()->Hide();
	return AfxMessageBox(nIDPrompt, nType, nIDHelp);
}

CWaitMessageBox::CWaitMessageBox(BOOL bInit/*=TRUE*/)
{
	CString strMsg;
	strMsg.LoadString(IDS_WAITMSGBOX_MSG001);
	Show(strMsg, bInit);
}

CWaitMessageBox::CWaitMessageBox(CString strMsg, BOOL bInit/*=TRUE*/)
{
	Show(strMsg, bInit);
}

CWaitMessageBox::~CWaitMessageBox(void)
{
	Hide();
}

void CWaitMessageBox::Show(CString strMsg, BOOL bInit/*=FALSE*/)
{
	CWaitMessageBoxCore::GetInstance()->Show(strMsg, bInit);
}

void CWaitMessageBox::Hide()
{
	CWaitMessageBoxCore::GetInstance()->Hide();
}
