// WaitMsgBoxDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "WaitMsgBoxDlg.h"

#define INTERVAL 100

// CWaitMsgBoxDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CWaitMsgBoxDlg, CDialog)

CWaitMsgBoxDlg::CWaitMsgBoxDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWaitMsgBoxDlg::IDD, pParent)
	, m_dwStartTime(0)
	, m_bInitComplete(FALSE)
{
	m_hBlackBrush = CreateSolidBrush( RGB(0, 0, 0) );
	m_hDarkGrayBrush = CreateSolidBrush(RGB(32, 32, 32));
}

CWaitMsgBoxDlg::~CWaitMsgBoxDlg()
{
	if(m_hBlackBrush) DeleteObject(m_hBlackBrush);
	if(m_hDarkGrayBrush) DeleteObject(m_hDarkGrayBrush);
}

void CWaitMsgBoxDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TXT_START_TIME, m_txtStartTime);
	DDX_Control(pDX, IDC_TXT_PROGRESS_TIME, m_txtProgressTime);
	DDX_Control(pDX, IDC_TXT_MSG, m_txtMsg);
	DDX_Control(pDX, IDC_PROGRESS, m_Progress);
}

BEGIN_MESSAGE_MAP(CWaitMsgBoxDlg, CDialog)
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

// CWaitMsgBoxDlg 메시지 처리기입니다.

void CWaitMsgBoxDlg::OnOK() {}
void CWaitMsgBoxDlg::OnCancel() {}

BOOL CWaitMsgBoxDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ModifyStyle(WS_CAPTION, 0, SWP_FRAMECHANGED);

	Init();
	Hide();

	SetTimer(0, 100, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CWaitMsgBoxDlg::Init()
{
	m_Progress.SetRange(0, 99);
	m_Progress.SetPos(0);

	CString strName, strValue, strText;
	strName.LoadString(IDS_WAITMSGBOX_STR001);
	strValue = CTime::GetCurrentTime().Format(_T("%Y/%m/%d %H:%M:%S"));
	strText.Format(_T("%s %s"), strName, strValue);
	m_txtStartTime.SetWindowText(strText);

	strName.LoadString(IDS_WAITMSGBOX_STR002);
	strText.Format(_T("%s 00:00:00"), strName);
	m_txtProgressTime.SetWindowText(strText);

	m_dwStartTime = ::GetTickCount();

	m_txtMsg.SetWindowText(_T(""));
}

void CWaitMsgBoxDlg::Show(CString strMsg, BOOL bInit/*=FALSE*/)
{
	if(bInit)
	{
		Init();
	}

	if(strMsg.Find("\n") > 0)
	{
		m_txtMsg.ModifyStyle(SS_CENTERIMAGE | SS_WORDELLIPSIS, 0);
	}
	else
	{
		m_txtMsg.ModifyStyle(0, SS_CENTERIMAGE | SS_WORDELLIPSIS);
	}

	m_txtMsg.SetWindowText(strMsg);

	SetWindowPos(&CWnd::wndTop, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
}

void CWaitMsgBoxDlg::Hide()
{
	this->ShowWindow(SW_HIDE);
}

void CWaitMsgBoxDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 0)
	{
		KillTimer(0);
		m_bInitComplete = TRUE;
		SetTimer(1, INTERVAL, NULL);
	}
	else if(nIDEvent == 1)
	{
		DWORD dwElapse = ::GetTickCount() - m_dwStartTime;
		DWORD dwElapseSec = dwElapse / 1000;

		CString strName, strText;
		strName.LoadString(IDS_WAITMSGBOX_STR002);
		strText.Format(_T("%s %02d:%02d:%02d")
					, strName
					, dwElapseSec / 3600
					, (dwElapseSec % 3600) / 60
					, (dwElapseSec % 60)
					);

		m_txtProgressTime.SetWindowText(strText);

		m_Progress.SetPos((dwElapse/INTERVAL) % 100);
	}

	CDialog::OnTimer(nIDEvent);
}

HBRUSH CWaitMsgBoxDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	pDC->SetBkMode(TRANSPARENT); // 글자색과 배경색이 일치하게 만듬
	pDC->SetTextColor(RGB(255, 255, 255));

	if(nCtlColor==CTLCOLOR_DLG)
	{
		return m_hBlackBrush;
	}

	// 정적 스태틱
	if(nCtlColor == CTLCOLOR_STATIC)
	{
		return m_hDarkGrayBrush;
	}

	return hbr;
}
