// UBCCommon.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "ubccommon.h"
#include <afxdllx.h>
#include "WaitMessageBox.h"

#ifdef _MANAGED
#error Please read instructions in UBCCommon.cpp to compile with /clr
// If you want to add /clr to your project you must do the following:
//	1. Remove the above include for afxdllx.h
//	2. Add a .cpp file to your project that does not have /clr thrown and has
//	   Precompiled headers disabled, with the following text:
//			#include <afxwin.h>
//			#include <afxdllx.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static AFX_EXTENSION_MODULE UBCCommonDLL = { NULL, NULL };

#ifdef _MANAGED
#pragma managed(push, off)
#endif

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UBCCommon.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UBCCommonDLL, hInstance))
			return 0;

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		new CDynLinkLibrary(UBCCommonDLL);

	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UBCCommon.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UBCCommonDLL);
	}
	return 1;   // ok
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

void OpenIExplorer(CString strParam, int cx/*=1024*/, int cy/*=768*/)
{
	HRESULT hr;
	IWebBrowser2* pWebBrowser = NULL;

	if(NULL != pWebBrowser)
	{
		pWebBrowser->Quit();
		pWebBrowser->Release();
		pWebBrowser = NULL;
	}

	if(FAILED(hr = ::CoCreateInstance(CLSID_InternetExplorer, NULL, CLSCTX_LOCAL_SERVER, IID_IWebBrowser2, (void**)&pWebBrowser)))
	{
//		CoUninitialize();
	}

	CString strHeader, strTarget, strUrl;
	strHeader = "Content-Type: application/x-www-form-urlencoded\r\n";
	strTarget = "_top"; // ���ο� â���� ���
	strUrl = strParam;

	VARIANT vtHeader = {0}, vtTarget= {0}, vtEmpty= {0}, vtPostData = {0};

	vtHeader.vt = VT_BSTR; 
	vtHeader.bstrVal = strHeader.AllocSysString();

	vtTarget.vt = VT_BSTR; 
	vtTarget.bstrVal = strTarget.AllocSysString(); 

	::VariantInit(&vtEmpty);

	pWebBrowser->put_ToolBar(VARIANT_FALSE);		// �ͽ��÷ξ� ���� ����
	pWebBrowser->put_MenuBar(VARIANT_FALSE);		// �޴��� ����
	pWebBrowser->put_AddressBar(VARIANT_FALSE);		// �ּ�â ����
	pWebBrowser->put_StatusBar(VARIANT_FALSE);		// ���¹� ����
	pWebBrowser->put_Visible(VARIANT_TRUE);
	pWebBrowser->put_Top(0);
	pWebBrowser->put_Left(0);
	pWebBrowser->put_Width(cx);
	pWebBrowser->put_Height(cy);

	pWebBrowser->put_Visible(VARIANT_TRUE);     
	hr = pWebBrowser->Navigate(strUrl.AllocSysString(), &vtEmpty, &vtTarget, &vtPostData, &vtHeader);

	long IEHwnd;
	pWebBrowser->get_HWND(&IEHwnd);

	SetForegroundWindow((HWND)IEHwnd);

	if(!SUCCEEDED(hr))
	{
		CString msg = "HyperLink Error";
		if(E_INVALIDARG == hr)
			msg += ": Invalid Parameters.";
		else if(E_OUTOFMEMORY == hr) 
			msg += ": Out of memory.";

		MessageBeep(MB_ICONEXCLAMATION);
		UbcMessageBox(msg, MB_ICONEXCLAMATION | MB_OK);
	}

	SysFreeString(vtHeader.bstrVal);
	SysFreeString(vtTarget.bstrVal);
	pWebBrowser->Release();
}

BOOL ShellCopyFile(CString strFrom, CString strTo, BOOL bFailIfExists)
{
	strFrom.Replace(_T("/"), _T("\\"));
	strTo.Replace(_T("/"), _T("\\"));
	strFrom.Trim(_T("\\"));
	strTo.Trim(_T("\\"));
	strFrom.MakeLower();
	strTo.MakeLower();

	if(strFrom.Find(strTo) == 0 || strTo.Find(strFrom) == 0) return TRUE;

	SHFILEOPSTRUCT fileop;
	memset(&fileop, 0x00, sizeof(fileop));

	TCHAR szFrom[MAX_PATH+1] = {0};
	strcpy(szFrom, strFrom);

	TCHAR szTo[MAX_PATH+1] = {0};
	strcpy(szTo, strTo);

	HWND hWnd = NULL;
	CWnd* pMainWnd = AfxGetMainWnd();
	if(pMainWnd) hWnd = pMainWnd->GetSafeHwnd();

	fileop.hwnd = hWnd;
	fileop.wFunc = FO_COPY;
	fileop.pFrom = szFrom;
	fileop.pTo = szTo;
	//fileop.fFlags = FOF_NOCONFIRMATION | FOF_FILESONLY;
	fileop.fFlags = FOF_NOCONFIRMATION;
//	fileop.fAnyOperationsAborted = TRUE;
//	fileop.hNameMappings = NULL;
//	fileop.lpszProgressTitle = NULL; // only used if FOF_SIMPLEPROGRESS

	int nResult = SHFileOperation(&fileop);

	return (nResult == 0 && fileop.fAnyOperationsAborted == false);
}