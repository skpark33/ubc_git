#pragma once

#include "afxcmn.h"
#include "common\MyScrollRichEdit.h"

#ifndef IDD_FTP_REPORT
#define IDD_FTP_REPORT 20002
#endif//IDD_FTP_REPORT

// CFtpReportDlg dialog
class AFX_EXT_CLASS CFtpReportDlg : public CDialog
{
	DECLARE_DYNAMIC(CFtpReportDlg)

public:
	CFtpReportDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFtpReportDlg();

// Dialog Data
	enum { IDD = IDD_FTP_REPORT };

	void	WriteEditHighLightBox(char* Msg, COLORREF TextColor, COLORREF BkColor);
	void 	WriteEditBox(CString* Msg, BOOL bVScr=TRUE);
	void 	WriteEditBox(char* Msg, BOOL bVScr=TRUE);
	void 	SetMaxLine(int MaxLine = 3000, int LineMaxChar = 256);
	void 	Clear();

//	CRichEditCtrl m_recReport;
	CMyScrollRichEdit m_recReport;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnEnChangeRicheditReport();
	afx_msg void OnBnClickedOk();
	afx_msg void OnClose();
};
