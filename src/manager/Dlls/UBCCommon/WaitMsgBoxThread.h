#pragma once

#include "WaitMsgBoxDlg.h"

// CWaitMsgBoxThread

class CWaitMsgBoxThread : public CWinThread
{
	DECLARE_DYNCREATE(CWaitMsgBoxThread)

protected:
	CWaitMsgBoxThread();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CWaitMsgBoxThread();

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	CWaitMsgBoxDlg* m_pMsgBoxDlg;

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual int Run();
};
