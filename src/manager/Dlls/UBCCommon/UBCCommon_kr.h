#pragma once

#define UBCDEFAULT_FONT					_T("굴림")
#define UBCSTUDIOEXE_STR				_T("UBCStudio_kr.exe")
#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE_kr.exe")
#define UBCMANAGEREXE_STR				_T("UBCManager_kr.exe")

//CFtpModule
#define FTPMODULE_MSG001		_T("업로드할 콘텐츠 파일이 존재하지 않습니다.")
#define FTPMODULE_MSG002		_T("업로드가 취소되었습니다.")
#define FTPMODULE_MSG003		_T("원격접속이 실패하였습니다.\r\n네트워크 또는 방화벽을 확인후 다시 시도해주십시요.")
#define FTPMODULE_MSG004		_T("콘텐츠 파일(%s) 이 존재하지 않거나 다른 응용프로그램에서 사용중입니다.\r\n응용프로그램을 종료후 다시 시도하여주십시요.")
#define FTPMODULE_MSG005		_T("콘텐츠 파일(%s)이 원격 호스트에 이미 존재합니다.")
#define FTPMODULE_MSG006		_T("콘텐츠 파일(%s)을 원격 호스트에서 열수가 없습니다.\r\n원격 호스트 상태를 확인하여 주십시요.")
#define FTPMODULE_MSG007		_T("콘텐츠 파일(%s)이 이미 존재합니다.")
#define FTPMODULE_MSG008		_T("콘텐츠 파일(%s)이 존재하지 않습니다.")
#define FTPMODULE_MSG009		_T("디스크가 유효하지 않습니다\r\n디스크 공간을 확인하여 주십시요.")
#define FTPMODULE_MSG010		_T("콘텐츠 패키지[%s] 갱신 공지가 발송되지 않았습니다.\r\n네크워크나 방화벽상태를 확인하여 주십시요.")
#define FTPMODULE_MSG011		_T("알수없는 예외가 발생했습니다 !!!")
#define FTPMODULE_MSG012		_T("알수없는 예외가 발생했습니다 !!!")
#define FTPMODULE_MSG013		_T("원격 디렉토리 변경이 실패하였습니다 (%s)")
#define FTPMODULE_MSG014		_T("원격 디렉토리 생성이 실패하였습니다 (%s).\r\n네크워크나 방화벽상태를 확인하여 주십시요.")
#define FTPMODULE_MSG015		_T("알수없는 이유로 파일(%s) 업로드가 실패하였습니다.\r\n네크워크나 방화벽상태를 확인하여 주십시요.")
#define FTPMODULE_MSG016		_T("알수없는 이유로 파일(%s) 다운로드가 실패하였습니다.\r\n네크워크나 방화벽상태를 확인하여 주십시요.")