#include "stdafx.h"
#include "resource.h"
#include "ftpmodule.h"

CFtpModule::CFtpModule()
{
}

CFtpModule::~CFtpModule()
{
}

CString CFtpModule::MakeErrorMsg(int nErrorCode, LPARAM lParam)
{
	CString strMsg;
	switch(nErrorCode)
	{
	case FTP_ERROR_OK:					//all ok
		//strMsg = "Upload completed successfully.";
		strMsg.Empty();
		break;
	case FTP_ERROR_EMPTY_CONTENTS:		// Empty contents
		strMsg = LoadStringById(IDS_FTPMODULE_MSG001);
		break;
	case FTP_ERROR_USER_CANCEL:			//user cancel	
		strMsg = LoadStringById(IDS_FTPMODULE_MSG002);
		break;
	case FTP_ERROR_CONNECT_FAIL:		//connect fail
		strMsg = LoadStringById(IDS_FTPMODULE_MSG003); 
		break;
	case FTP_ERROR_LOCAL_FILE_OPEN:		//local file open error
		{
		LPSTR lpszFileName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG004), lpszFileName);
		}
		break;
	case FTP_ERROR_LOCAL_FILE_EXIST:
		{
		LPSTR lpszFileName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG005), lpszFileName);
		}
		break;
	case FTP_ERROR_REMOTE_FILE_OPEN:	//remote file open error
		{
		LPSTR lpszFileName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG006), lpszFileName);
//		strMsg.Format("Host contents file(%s) create fail.\r\nPlease check disk free space.", lpszFileName);
		}
		break;
	case FTR_ERROR_REMOTE_FILE_EXIST:
		{
		LPSTR lpszFileName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG007), lpszFileName);
		}
		break;
	case FTR_ERROR_REMOTE_FILE_NOTEXIST:
		{
		LPSTR lpszFileName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG008), lpszFileName);
		}
		break;
	case FTR_ERROR_REMOTE_SPACE:
		strMsg = LoadStringById(IDS_FTPMODULE_MSG009);
		break;
	case FTP_ERROR_NOTIFY_UPDATE:
		{
		LPSTR lpszPackageName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG010), lpszPackageName);
		}
		break;
	case FTP_ERROR_CHANGE_DIRECTORY:
		{
		LPSTR lpszDrectoryName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG013), lpszDrectoryName);
		}
		break;
	case FTP_ERROR_MAKE_DIRECTORY:
		{
		LPSTR lpszDrectoryName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG014), lpszDrectoryName);
		}
		break;
	case FTP_ERROR_UPLOADING:
		{
		LPSTR lpszFileName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG015), lpszFileName);
		}
		break;
	case FTP_ERROR_DOWNLOADING:
		{
		LPSTR lpszFileName = (LPSTR)lParam;
		strMsg.Format(LoadStringById(IDS_FTPMODULE_MSG016), lpszFileName);
		}
		break;
	case FTP_ERROR_UNKNOWN:				//Unknown file exception
		strMsg = LoadStringById(IDS_FTPMODULE_MSG011);
		//strMsg = (LPSTR)lParam;
		break;
	case FTP_ERROR_REMOTE_FILE_PUBLIC:
		strMsg = LoadStringById(IDS_FTPMODULE_MSG017);
		//strMsg = (LPSTR)lParam;
		break;
	case FTP_ERROR_DIRTY_LOCAL_FILE:
		strMsg = LoadStringById(IDS_FTPMODULE_MSG018);
		//strMsg = (LPSTR)lParam;
		break;
	default:
		strMsg = LoadStringById(IDS_FTPMODULE_MSG012);
		break;
	}//switch

	return strMsg;
}