#pragma once

#define UBCDEFAULT_FONT					_T("Arial")
#define UBCSTUDIOEXE_STR				_T("UBCStudio_en.exe")
#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE_en.exe")
#define UBCMANAGEREXE_STR				_T("UBCManager_en.exe")

//CFtpModule
#define FTPMODULE_MSG001		_T("No content files to upload.")
#define FTPMODULE_MSG002		_T("Content files upload canceled by user.")
#define FTPMODULE_MSG003		_T("Connection to remote host failed.\r\nPlease check your network or firewall settings.")
#define FTPMODULE_MSG004		_T("Local content file (%s) does not exist or another application has it open.\r\nPlease close that application and try again.")
#define FTPMODULE_MSG005		_T("Content file (%s) already exists on the server.")
#define FTPMODULE_MSG006		_T("Content file (%s) cannot be opened from server.\r\nPlease check the server and try again.")
#define FTPMODULE_MSG007		_T("Content file (%s) already exists on the server.")
#define FTPMODULE_MSG008		_T("Content file (%s) does not exist.")
#define FTPMODULE_MSG009		_T("Host disk space is not valid\r\nPlease check the disk space and try again.")
#define FTPMODULE_MSG010		_T("Schedule [%s] update notice failed.\r\nPlease check your network or firewall settings.")
#define FTPMODULE_MSG011		_T("Unknown file exception occured !!!\r\nPlease try again after closing all unnecessary applications")
#define FTPMODULE_MSG012		_T("Unknown file exception occured !!!\r\nPlease try again after closing all unnecessary applications")
#define FTPMODULE_MSG013		_T("Failed to change the remote directory (%s)\r\nPlease check your network or firewall settings.")
#define FTPMODULE_MSG014		_T("Failed to make the remote directory (%s)\r\nPlease check your network or firewall settings.")
#define FTPMODULE_MSG015		_T("Upload a file(%s) for an unknown reason failed\r\nPlease check your network or firewall settings.")
#define FTPMODULE_MSG016		_T("Download a file(%s) for an unknown reason failed\r\nPlease check your network or firewall settings.")