#pragma once

class AFX_EXT_CLASS CWaitMessageBox
{
public:
	CWaitMessageBox(BOOL bInit=TRUE);
	CWaitMessageBox(CString strMsg, BOOL bInit=TRUE);
	virtual ~CWaitMessageBox(void);

	static void Show(CString strMsg, BOOL bInit=FALSE);
	static void Hide();
};

int AFX_EXT_API UbcMessageBox(LPCTSTR lpszText, UINT nType = MB_OK, UINT nIDHelp = 0);
int AFX_EXT_API UbcMessageBox(UINT nIDPrompt, UINT nType = MB_OK, UINT nIDHelp = (UINT)-1);