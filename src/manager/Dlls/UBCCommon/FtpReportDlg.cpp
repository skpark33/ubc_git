// FtpReport.cpp : implementation file
//
#include "stdafx.h"
#include "resource.h"
#include "FtpReportDlg.h"

// CFtpReportDlg dialog
IMPLEMENT_DYNAMIC(CFtpReportDlg, CDialog)

CFtpReportDlg::CFtpReportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFtpReportDlg::IDD, pParent)
{
}

CFtpReportDlg::~CFtpReportDlg()
{
}

void CFtpReportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RICHEDIT_REPORT, m_recReport);
}

BEGIN_MESSAGE_MAP(CFtpReportDlg, CDialog)
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_RICHEDIT_REPORT, &CFtpReportDlg::OnEnChangeRicheditReport)
	ON_BN_CLICKED(IDOK, &CFtpReportDlg::OnBnClickedOk)
	ON_WM_CLOSE()
END_MESSAGE_MAP()

// CFtpReportDlg message handlers
BOOL CFtpReportDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CFtpReportDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CFtpReportDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnClose();
}

void CFtpReportDlg::OnEnChangeRicheditReport()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}

void CFtpReportDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
	PostMessage(WM_DESTROY, 0, 0);
}

void CFtpReportDlg::WriteEditHighLightBox(char* Msg, COLORREF TextColor, COLORREF BkColor)
{
	m_recReport.WriteEditHighLightBox(Msg, TextColor, BkColor);
}

void CFtpReportDlg::WriteEditBox(CString* Msg, BOOL bVScr/*=TRUE*/)
{
	m_recReport.WriteEditBox(Msg, bVScr);
}

void CFtpReportDlg::WriteEditBox(char* Msg, BOOL bVScr/*=TRUE*/)
{
	m_recReport.WriteEditBox(Msg, bVScr);
}

void CFtpReportDlg::SetMaxLine(int MaxLine/*=3000*/, int LineMaxChar/*=256*/)
{
	m_recReport.SetMaxLine(MaxLine, LineMaxChar);
}

void CFtpReportDlg::Clear()
{
	m_recReport.Clear();
}