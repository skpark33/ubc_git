//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCCommon_jp.rc
//
#define IDB_UBCLISTCTR_HEAD             20000
#define IDC_PROGRESS_FILE               20000
#define IDD_UBCFTP                      20001
#define IDC_PROGRESS_TOTAL              20001
#define IDC_STATIC_FILE                 20002
#define IDD_FTP_REPORT                  20002
#define IDC_RICHEDIT21                  20003
#define IDC_RICHEDIT_REPORT             20003
#define IDS_FTPMODULE_MSG001            20003
#define IDS_FTPMODULE_MSG002            20004
#define IDD_WAIT_MSGBOX                 20004
#define IDC_PROGRESS1                   20004
#define IDC_PROGRESS                    20004
#define IDS_FTPMODULE_MSG003            20005
#define IDC_TXT_MSG                     20005
#define IDS_FTPMODULE_MSG004            20006
#define IDC_TXT_START_TIME              20006
#define IDS_FTPMODULE_MSG005            20007
#define IDC_TXT_PROGRESS_TIME           20007
#define IDS_FTPMODULE_MSG006            20008
#define IDS_FTPMODULE_MSG007            20009
#define IDS_FTPMODULE_MSG008            20010
#define IDS_FTPMODULE_MSG009            20011
#define IDS_FTPMODULE_MSG010            20012
#define IDS_FTPMODULE_MSG011            20013
#define IDS_FTPMODULE_MSG012            20014
#define IDS_FTPMODULE_MSG013            20015
#define IDS_FTPMODULE_MSG014            20016
#define IDS_FTPMODULE_MSG015            20017
#define IDS_FTPMODULE_MSG016            20018
#define IDS_FTPMODULE_MSG017            20019
#define IDS_FTPMODULE_MSG018            20020
#define IDS_WAITMSGBOX_STR001           20021
#define IDS_WAITMSGBOX_STR002           20022
#define IDS_WAITMSGBOX_MSG001           20023
#define IDS_LOGINDLG_MSG010             20024

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        20005
#define _APS_NEXT_COMMAND_VALUE         20000
#define _APS_NEXT_CONTROL_VALUE         20008
#define _APS_NEXT_SYMED_VALUE           20000
#endif
#endif
