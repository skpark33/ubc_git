#pragma once

AFX_EXT_API void OpenIExplorer(CString strParam, int cx= 1024, int cy= 768);
AFX_EXT_API BOOL ShellCopyFile(CString strFrom, CString strTo, BOOL bFailIfExists);