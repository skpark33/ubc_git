#pragma once

#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"

// CWaitMsgBoxDlg 대화 상자입니다.

class CWaitMsgBoxDlg : public CDialog
{
	DECLARE_DYNAMIC(CWaitMsgBoxDlg)

public:
	CWaitMsgBoxDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CWaitMsgBoxDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_WAIT_MSGBOX };

	void Init();
	void Show(CString strMsg, BOOL bInit=FALSE);
	void Hide();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	HBRUSH	m_hBlackBrush;
	HBRUSH	m_hDarkGrayBrush;

	DECLARE_MESSAGE_MAP()
public:
	BOOL	m_bInitComplete;
	CStatic m_txtStartTime;
	CStatic m_txtProgressTime;
	CStatic m_txtMsg;
	CProgressCtrl m_Progress;
	DWORD m_dwStartTime;

	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
	virtual void OnCancel();
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
