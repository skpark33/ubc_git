#include "StdAfx.h"
#include "WaitMessageBoxCore.h"

CWaitMessageBoxCore* CWaitMessageBoxCore::m_pInstance = NULL;
CCriticalSection CWaitMessageBoxCore::m_csLock;

CWaitMessageBoxCore::CWaitMessageBoxCore(void)
: m_pMsgBoxThread(NULL)
{
	if(m_pMsgBoxThread)
	{
		if( m_pMsgBoxThread->m_pMsgBoxDlg && 
			m_pMsgBoxThread->m_pMsgBoxDlg->GetSafeHwnd() &&
			::IsWindow(m_pMsgBoxThread->m_pMsgBoxDlg->GetSafeHwnd()))
		{
			m_pMsgBoxThread->m_pMsgBoxDlg->CloseWindow();
			WaitForSingleObject(m_pMsgBoxThread->m_hThread, 3000);
		}

		m_pMsgBoxThread = NULL;
	}

	m_pMsgBoxThread = (CWaitMsgBoxThread*)AfxBeginThread(RUNTIME_CLASS(CWaitMsgBoxThread), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
	m_pMsgBoxThread->m_bAutoDelete = TRUE;
	m_pMsgBoxThread->ResumeThread();

	while(1)
	{
		Sleep(100);
		if( m_pMsgBoxThread->m_pMsgBoxDlg &&
			m_pMsgBoxThread->m_pMsgBoxDlg->m_bInitComplete )
		{
			break;
		}
	}
}

CWaitMessageBoxCore::~CWaitMessageBoxCore(void)
{
	if(m_pMsgBoxThread)
	{
		if( m_pMsgBoxThread->m_pMsgBoxDlg && 
			m_pMsgBoxThread->m_pMsgBoxDlg->GetSafeHwnd() &&
			::IsWindow(m_pMsgBoxThread->m_pMsgBoxDlg->GetSafeHwnd()))
		{
			m_pMsgBoxThread->m_pMsgBoxDlg->CloseWindow();
			WaitForSingleObject(m_pMsgBoxThread->m_hThread, 3000);
		}

		m_pMsgBoxThread = NULL;
	}
}

CWaitMessageBoxCore* CWaitMessageBoxCore::GetInstance()
{
	m_csLock.Lock();

	if(!m_pInstance)
	{
		m_pInstance = new CWaitMessageBoxCore;
	}

	m_csLock.Unlock();

	return m_pInstance;
}

void CWaitMessageBoxCore::Release()
{
	m_csLock.Lock();

	if(m_pInstance)
	{
		delete m_pInstance;
		m_pInstance = NULL;
	}

	m_csLock.Unlock();
}

void CWaitMessageBoxCore::Show(CString strMsg, BOOL bInit/*=FALSE*/)
{
	if(m_pMsgBoxThread && m_pMsgBoxThread->m_pMsgBoxDlg)
	{
		m_pMsgBoxThread->m_pMsgBoxDlg->Show(strMsg, bInit);
	}
}

void CWaitMessageBoxCore::Hide()
{
	if(m_pMsgBoxThread && m_pMsgBoxThread->m_pMsgBoxDlg)
	{
		m_pMsgBoxThread->m_pMsgBoxDlg->Hide();
	}
}
