// WaitMsgBoxThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "WaitMsgBoxThread.h"
#include "WaitMsgBoxDlg.h"


// CWaitMsgBoxThread

IMPLEMENT_DYNCREATE(CWaitMsgBoxThread, CWinThread)

CWaitMsgBoxThread::CWaitMsgBoxThread()
: m_pMsgBoxDlg(NULL)
{
}

CWaitMsgBoxThread::~CWaitMsgBoxThread()
{
}

BOOL CWaitMsgBoxThread::InitInstance()
{
	m_pMsgBoxDlg = new CWaitMsgBoxDlg();
	m_pMainWnd = (CWnd*)m_pMsgBoxDlg;
	int nRet = m_pMsgBoxDlg->DoModal();

	delete m_pMsgBoxDlg;
	m_pMsgBoxDlg = NULL;
	m_pMainWnd = NULL;

	return TRUE;
}

int CWaitMsgBoxThread::ExitInstance()
{
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CWaitMsgBoxThread, CWinThread)
END_MESSAGE_MAP()


// CWaitMsgBoxThread 메시지 처리기입니다.

int CWaitMsgBoxThread::Run()
{
	return CWinThread::Run();
}
