// stdafx.cpp : source file that includes just the standard includes
// UBCCommon.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

CString LoadStringById(UINT nID)
{
	CString strValue;
	if(!strValue.LoadString(nID)) return _T("");
	return strValue;
}