#pragma once

#define UBCDEFAULT_FONT					_T("MS ゴシック")
#define UBCSTUDIOEXE_STR				_T("UBCStudio_kr.exe")
#define UBCSTUDIOEEEXE_STR				_T("UBCStudioEE_kr.exe")
#define UBCMANAGEREXE_STR				_T("UBCManager_kr.exe")

//CFtpModule
#define FTPMODULE_MSG001		_T("アップロードするコンテンツファイルが存在しません。")
#define FTPMODULE_MSG002		_T("アップロードがキャンセルとなりました。")
#define FTPMODULE_MSG003		_T("遠隔接続に失敗しました。\r\nネットワークまたはファイアウォールを確認し、もう一度やり直してください。")
#define FTPMODULE_MSG004		_T("コンテンツファイル(%s)が存在しないか他のアプリケーションで使用中です。\r\nアプリケーションを終了し、もう一度やり直してください。")
#define FTPMODULE_MSG005		_T("コンテンツファイル(%s)がすでに遠隔ホストに存在します。")
#define FTPMODULE_MSG006		_T("コンテンツファイル(%s)を遠隔ホストで開けません。\r\n遠隔ホストの状態を確認してください。")
#define FTPMODULE_MSG007		_T("コンテンツファイル(%s)がすでに存在します。")
#define FTPMODULE_MSG008		_T("コンテンツファイル(%s)が存在しません。")
#define FTPMODULE_MSG009		_T("ディスクが有効ではありません。\r\nディスクの空き容量を確認してください。")
#define FTPMODULE_MSG010		_T("スケジュール[%s]更新の知らせが転送されていません。\r\nネットワークまたはファイアウォールの状態を確認してください。")
#define FTPMODULE_MSG011		_T("不明な例外が発生しました。")
#define FTPMODULE_MSG012		_T("不明な例外が発生しました。")
#define FTPMODULE_MSG013		_T("リモートディレクトリを変更できませんでした (%s)\r\nネットワークまたはファイアウォールの状態を確認してください。")
#define FTPMODULE_MSG014		_T("リモートディレクトリを作成に失敗しました。 (%s)\r\nネットワークまたはファイアウォールの状態を確認してください。")
#define FTPMODULE_MSG015		_T("不明な理由で、ファイル(%s)のアップロードに失敗しました\r\nネットワークまたはファイアウォールの状態を確認してください。")
#define FTPMODULE_MSG016		_T("不明な理由で、ファイル(%s)のダウンロードが失敗しました\r\nネットワークまたはファイアウォールの状態を確認してください。")
