#pragma once

#include <afxmt.h>
#include "WaitMsgBoxThread.h"

class CWaitMessageBoxCore
{
public:
	static CWaitMessageBoxCore* GetInstance();
	static void Release();

	void Show(CString strMsg, BOOL bInit=FALSE);
	void Hide();

private:
	CWaitMessageBoxCore(void);
	virtual ~CWaitMessageBoxCore(void);

	static CWaitMessageBoxCore* m_pInstance;
	static CCriticalSection m_csLock;

	CWaitMsgBoxThread* m_pMsgBoxThread;
};
