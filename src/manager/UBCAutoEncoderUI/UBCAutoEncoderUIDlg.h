// UBCAutoEncoderUIDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "util/UBCAutoEncoder/MediaInfoDLL.h"

// CUBCAutoEncoderUIDlg 대화 상자
class CUBCAutoEncoderUIDlg : public CDialog
{
// 생성입니다.
public:
	CUBCAutoEncoderUIDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCAUTOENCODERUI_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	void					SetPathOpenFile(LPCTSTR);
	bool					IsValidOpenFolder();
	bool					LoadContents(LPCTSTR lpszFullPath);
	unsigned long			GetBitRate(LPCTSTR fullpath, CString& video_format,CString& video_codec, CString& audio_format, CString& errStr);
	bool					getTargetInfo();
	bool					IsSame();

	CString					m_currentPath;
	CString					m_szPathOpenFile;
	CString					m_strSource;
	CString					m_strTarget;
	CString					m_strBatch;
	BOOL					m_bIsChangedFile;	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	CString					m_strBitrate;
	CString					m_strTargetExt;

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CButton m_btnBrowserContentsFile;
	CEdit m_editContentsFileName;
	afx_msg void OnBnClickedButtonBrowserContentsFile();

	CEdit m_src_videoCodec;
	CEdit m_src_video;
	CEdit m_src_audio;
	CEdit m_src_bitrate;

	CButton m_btnBrowserBatch;
	CEdit m_editBatchFile;

	CEdit m_editTargetName;
	CEdit m_tgt_videoCodec;
	CEdit m_tgt_video;
	CEdit m_tgt_audio;
	CEdit m_tgt_bitrate;

	afx_msg void OnBnClickedButtonBrowserBatch();
	afx_msg void OnBnClickedOk();
};
