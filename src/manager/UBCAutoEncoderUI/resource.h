//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCAutoEncoderUI.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_UBCAUTOENCODERUI_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_SOURCE                 138
#define IDC_BUTTON_BROWSER_CONTENTS_FILE 139
#define IDC_EDIT_TARGET                 140
#define IDC_EDIT_BATCH                  142
#define IDC_BUTTON_BROWSER_BATCH        143
#define IDC_SRC_VIDEO                   144
#define IDC_SRC_AUDIO                   145
#define IDC_SRC_BITRATE                 146
#define IDC_TGT_VIDEO                   147
#define IDC_SRC_VIDEO_CODEC             148
#define IDC_TGT_VIDEO_CODEC             149
#define IDC_EDIT_ENCODER                150
#define IDC_TGT_AUDIO                   155
#define IDC_TGT_BITRATE                 156

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
