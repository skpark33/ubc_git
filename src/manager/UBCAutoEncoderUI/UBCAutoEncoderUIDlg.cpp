// UBCAutoEncoderUIDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCAutoEncoderUI.h"
#include "UBCAutoEncoderUIDlg.h"
#include "io.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace MediaInfoDLL;

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CUBCAutoEncoderUIDlg 대화 상자




CUBCAutoEncoderUIDlg::CUBCAutoEncoderUIDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCAutoEncoderUIDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUBCAutoEncoderUIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_SOURCE, m_editContentsFileName);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_CONTENTS_FILE, m_btnBrowserContentsFile);
	DDX_Control(pDX, IDC_SRC_VIDEO, m_src_video);
	DDX_Control(pDX, IDC_SRC_AUDIO, m_src_audio);
	DDX_Control(pDX, IDC_SRC_BITRATE, m_src_bitrate);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_BATCH, m_btnBrowserBatch);
	DDX_Control(pDX, IDC_EDIT_TARGET, m_editTargetName);
	DDX_Control(pDX, IDC_EDIT_BATCH, m_editBatchFile);
	DDX_Control(pDX, IDC_SRC_VIDEO_CODEC, m_src_videoCodec);
	DDX_Control(pDX, IDC_TGT_VIDEO_CODEC, m_tgt_videoCodec);
	DDX_Control(pDX, IDC_TGT_VIDEO, m_tgt_video);
	DDX_Control(pDX, IDC_TGT_AUDIO, m_tgt_audio);
	DDX_Control(pDX, IDC_TGT_BITRATE, m_tgt_bitrate);
}

BEGIN_MESSAGE_MAP(CUBCAutoEncoderUIDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_CONTENTS_FILE, &CUBCAutoEncoderUIDlg::OnBnClickedButtonBrowserContentsFile)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_BATCH, &CUBCAutoEncoderUIDlg::OnBnClickedButtonBrowserBatch)
	ON_BN_CLICKED(IDOK, &CUBCAutoEncoderUIDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CUBCAutoEncoderUIDlg 메시지 처리기

BOOL CUBCAutoEncoderUIDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	m_currentPath = szDrive;
	m_currentPath += szPath;

	m_strBatch = m_currentPath ;
	m_strBatch += "x264_mp4_aac.bat";
	m_editBatchFile.SetWindowText(m_strBatch);
	if(access(m_strBatch,0) != 0){
		CString errMsg = "Can't find ";
		errMsg += m_strBatch;	
		AfxMessageBox(errMsg);
	}

	getTargetInfo();
/*
	CString encoderPath = _T("C:\\BadakEncoder\\mencoder.exe");
	if(access(encoderPath,0) != 0){
		AfxMessageBox("Can't find Badak Encoder !!! Please Install BadakEncoder on C:\\" );	
	}
*/
	m_editTargetName.EnableWindow(false);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUBCAutoEncoderUIDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCAutoEncoderUIDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCAutoEncoderUIDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CUBCAutoEncoderUIDlg::OnBnClickedButtonBrowserContentsFile()
{
	CString szFileExts = "*.avi; *.asf; *.mkv; *.mp4; *.mpg; *.mpeg; *.wmv; *.mov; *.tp; *.flv; *.mp3; *.wma; *.wav";
	CString szFileVideoExts = "*.avi; *.asf; *.mkv; *.mp4; *.mpg; *.mpeg; *.wmv; *.mov; *.tp; *.flv";
	CString szFileMusicExts = "*.mp3; *.wma; *.wav";
	CString filter;
	filter.Format("All Media Files|%s|"
		          "All Video Files|%s|"
		          "All Music Files|%s|"
				  "AVI Files (*.avi)|*.avi|"
				  "ASF Files (*.asf)|*.asf|"
				  "MKV Files (*.mkv)|*.mkv|"
				  "MP4 Files (*.mp4)|*.mp4|"
				  "MPEG Files (*.mpg;*.mpeg)|*.mpg;*.mpeg|"
				  "WMV Files (*.wmv)|*.wmv|"
				  "MOV Files (*.mov)|*.mov|"
				  "TP Files (*.tp)|*.tp|"
				  "FLV Files (*.flv)|*.flv|"
				  "MP3 Files (*.mp3)|*.mp3|"
				  "WAV Files (*.wav)|*.wav|"
				  "WMA Files (*.wma)|*.wma||"
				 , szFileExts
				 , szFileVideoExts
				 , szFileMusicExts
				 );

	if(IsValidOpenFolder())
	{
		::SetCurrentDirectory(m_szPathOpenFile);
	}
	else
	{
		// 내비디오
		TCHAR szMyVideos[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyVideos, CSIDL_MYVIDEO, FALSE);
		::SetCurrentDirectory(szMyVideos);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	SetPathOpenFile(strFullpath);

	m_editContentsFileName.SetWindowText(dlg.GetPathName());
	//m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strSource = dlg.GetPathName();
	m_editContentsFileName.SetSel(0, -1);


	if(LoadContents(dlg.GetPathName())){
		m_editTargetName.EnableWindow(true);
	}
	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}


void CUBCAutoEncoderUIDlg::SetPathOpenFile(LPCTSTR str)
{
	m_szPathOpenFile = str;
}

bool CUBCAutoEncoderUIDlg::IsValidOpenFolder()
{
	if(m_szPathOpenFile.IsEmpty())
		return false;

	CString szFolder = m_szPathOpenFile;
	if(szFolder.Right(1)=="\\")
		szFolder += "*.*";

	CFileFind ff;
	return ff.FindFile(szFolder);
}

bool CUBCAutoEncoderUIDlg::LoadContents(LPCTSTR lpszFullPath)
{
	CFileStatus fs;
	if(CFile::GetStatus(lpszFullPath, fs))
	{
		CString video,videoCodec,audio;
		int bitrate = GetBitRate(lpszFullPath, video,videoCodec, audio, m_strBitrate);
		if(bitrate > 0){
			m_src_video.SetWindowText(video);
			m_src_videoCodec.SetWindowText(videoCodec);
			m_src_audio.SetWindowText(audio);
			m_src_bitrate.SetWindowText(m_strBitrate);
			m_tgt_bitrate.SetWindowText(m_strBitrate);

			char drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
			_splitpath(lpszFullPath, drive, path, filename, ext);

			CString tag;
			if(m_strTargetExt.CompareNoCase(ext)==0){
				tag = _T("_");
			}

			m_strTarget = drive;
			m_strTarget += path;
			m_strTarget += filename;
			m_strTarget += tag;
			m_strTarget += m_strTargetExt;

			m_editTargetName.SetWindowText(m_strTarget);
			m_editTargetName.EnableWindow(true);

			return true;
		}
		AfxMessageBox(m_strBitrate, MB_OK, 0);
		return false;
	}
	AfxMessageBox(_T("Invalid file"), MB_OK, 0);
	return false;
}

unsigned long CUBCAutoEncoderUIDlg::GetBitRate(LPCTSTR fullpath, CString& video_format, CString& video_codec, CString& audio_format, CString& errStr)
{
    MediaInfo MI;
	size_t ret = MI.Open(fullpath);
	if(ret==0){
		errStr = fullpath;
		errStr += _T(" open failed");
		return 0;
	}
	errStr = MI.Get(Stream_General, 0, _T("OverallBitRate"), Info_Text, Info_Name).c_str();
	unsigned long bitRate = strtoul(errStr,NULL,10);
	//std::wcout  << bitRate << std::endl;
	unsigned long retval = unsigned long(bitRate/1024);

	video_format = MI.Get(Stream_General, 0, _T("Format"), Info_Text, Info_Name).c_str();
	//std::wcout << _T("Video Format = [") << video_format.c_str() << _T("]") << std::endl;

	video_codec = MI.Get(Stream_General, 0, _T("Video_Format_List"), Info_Text, Info_Name).c_str();
	//video_codec = MI.Get(Stream_General, 0, _T("CodecID/Hint"), Info_Text, Info_Name).c_str();
	//std::wcout << _T("Video Format = [") << video_format.c_str() << _T("]") << std::endl;

	audio_format = MI.Get(Stream_General, 0, _T("Audio_Format_List"), Info_Text, Info_Name).c_str();
	//std::wcout << _T("Audio Format = [") << audio_format.c_str() << _T("]") <<std::endl;

	MI.Close();
	return retval;
}

bool CUBCAutoEncoderUIDlg::getTargetInfo()
{
	FILE* fp = fopen(this->m_strBatch,"r");
	if(!fp) {
		CString errMsg = this->m_strBatch;
		errMsg += " open failed";
		AfxMessageBox(errMsg, MB_OK, 0);
		return false;
	}

	CString stream;
	char buf[1025];
	while(fgets(buf,1024,fp) > 0) {
		stream += buf;
		memset(buf,0x00,0);
	}
	fclose(fp);

	int nIndex = 0;
	int nPos = 0;
	CString strToken = stream.Tokenize(" ", nPos);
	CStringArray strArray;
	while(strToken != "")
	{
		strArray.Add(strToken);
		strToken = stream.Tokenize(" ", nPos);
		nIndex++;
	}
	
	CString tgt_format = "mp4";
	CString tgt_codec, tgt_audio;
	for(int i=0;i<nIndex;i++){
		//if(strArray[i].Mid(0, strlen("format=")) == "format="){
		//	tgt_format = strArray[i].Mid(strlen("format="));	
		//}else
		if(strArray[i] == "-vcodec" && i < nIndex-1){
			tgt_codec = strArray[i+1];
		}else if(strArray[i] == "-acodec" && i < nIndex-1){
			tgt_audio = strArray[i+1];
		}
	}

	this->m_tgt_video.SetWindowText(tgt_format);	
	this->m_tgt_videoCodec.SetWindowText(tgt_codec);	
	this->m_tgt_audio.SetWindowText(tgt_audio);	

	m_strTargetExt = ".";
	m_strTargetExt += tgt_format;

	return true;

}
void CUBCAutoEncoderUIDlg::OnBnClickedButtonBrowserBatch()
{
	CString filter=  "Batch Files (*.bat)|*.bat|";

	::SetCurrentDirectory(m_currentPath);

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	m_editBatchFile.SetWindowText(dlg.GetPathName());
	m_strBatch = dlg.GetPathName();

	getTargetInfo();

}

void CUBCAutoEncoderUIDlg::OnBnClickedOk()
{
	if(IsSame()) {
		return;
	}
	
	m_editBatchFile.GetWindowText(m_strBatch);

	CString command = m_strBatch;
	command.Format("%s \"%s\" \"%s\" %s", m_strBatch,m_strSource,m_strTarget,m_strBitrate);

	int retval = system(command);
	if(retval != 0) {
		CString errMsg = "Commnad ";
		errMsg += command;
		errMsg += " failed";
		AfxMessageBox(errMsg);
	}else{
		AfxMessageBox("Encoding Complete");
	}
	
	
	//OnOK();
}


bool CUBCAutoEncoderUIDlg::IsSame()
{
	CString tgt_format, tgt_codec, tgt_audio;

	this->m_tgt_video.GetWindowText(tgt_format);	
	this->m_tgt_videoCodec.GetWindowText(tgt_codec);	
	this->m_tgt_audio.GetWindowText(tgt_audio);	

	CString src_format, src_codec, src_audio;

	this->m_src_video.GetWindowText(src_format);	
	this->m_src_videoCodec.GetWindowText(src_codec);	
	this->m_src_audio.GetWindowText(src_audio);	


	if( src_format == "MPEG-4" && tgt_format == "MP4" && 
		src_codec == "AVC" && tgt_codec == "libx264" && 
		src_audio == "AAC" && tgt_audio == "aac" ){
	
		AfxMessageBox("Source file's format and target encoding format are same( MPEG-4==MP4, AVC==x264, AAC=faac is actually same).  The encoding is not necessary");
		return true;
	
	}

	return false;

}