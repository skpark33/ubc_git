// ShowmeMsgDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCShopAnnouncer.h"
#include "ShowmeMsgDlg.h"
#include <ci/libDebug/ciDebug.h>

#include <tlhelp32.h>

// CShowmeMsgDlg 대화 상자입니다.
ciSET_DEBUG(5, "CShowmeMsgDlg");

IMPLEMENT_DYNAMIC(CShowmeMsgDlg, CDialog)

CShowmeMsgDlg::CShowmeMsgDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CShowmeMsgDlg::IDD, pParent)
{
}

CShowmeMsgDlg::~CShowmeMsgDlg()
{
}

void CShowmeMsgDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT_SHOWME_REQ, m_showme_req_text);

    CString strText("010-1234-1234 고객님께서 03-15-2016 10:35:21에 \r\n실물상품보기 요청을 하였습니다.\r\n");
    //m_showme_req_text.SetReadOnly(FALSE);
    m_showme_req_text.SetWindowTextA(strText);
}


BEGIN_MESSAGE_MAP(CShowmeMsgDlg, CDialog)
    ON_BN_CLICKED(IDOK, &CShowmeMsgDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CShowmeMsgDlg 메시지 처리기입니다.


void CShowmeMsgDlg::OnBnClickedOk()
{
    // TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    CString strText("010-1234-1234 고객님께서 03-15-2016 10:35:21에 \r\n실물상품보기 요청을 하였습니다.\r\n");
    //m_showme_req_text.SetReadOnly(FALSE);
    m_showme_req_text.SetWindowTextA(strText);

    OnOK();
}

BOOL CShowmeMsgDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    return TRUE;
}

