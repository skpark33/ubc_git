#include "stdafx.h"
#include "elShopAnnouncerEndpoint.h"
#include "common/libElComponent/Base/JsonBase.h"
#include <ci/libBase/ciStringUtil.h>

#include "UBCShopAnnouncerDlg.h"

ciSET_DEBUG(5, "elShopAnnouncerEndpoint");

elShopAnnouncerEndpoint::elShopAnnouncerEndpoint(CUBCShopAnnouncerDlg* pDlg)
{
     m_shop_announce_dlg_ = pDlg;
}

elShopAnnouncerEndpoint::~elShopAnnouncerEndpoint(void)
{
}

string  elShopAnnouncerEndpoint::GetRedirectUrl()
{
    return redirect_url_;
}

bool elShopAnnouncerEndpoint::Submit(string el_command_type, string el_command_id, string str_command, string& entity, elRecord& field_name_arr, elRecordList& records)
{
    ciDEBUG(5, ("Submit(%s, %s, %s)", el_command_type.c_str(), el_command_id.c_str(), str_command.c_str()));


    if (el_command_type == "CustomerWishlist") {
        SetNotifyCustomerWishlist_(records);
        m_shop_announce_dlg_->NotifyWishlist(records);
        ciDEBUG(5, ("Submit end NotifyWishlist!!"));
    } else {
        ciERROR(("Unknown command type : %s, %s", el_command_type.c_str(), el_command_id.c_str() ));
        return false;
    }
    
    return true;
}


bool elShopAnnouncerEndpoint::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    // elCommand마다 초기화 할 수 있기 때문에 같은 id로 이미 초기화 했으면 다시 안한다.
    if (IsInit() == true && my_id == id_) {
        ciDEBUG(5, ("Already Init(%s)", my_id.c_str()));
        return true;
    }

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elEndPointRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;
    
    group_ = p_my_rule_->group;
    user_ = p_my_rule_->user;
    pass_ = p_my_rule_->pass;
    host_ = p_my_rule_->host;
    port_ = p_my_rule_->port;
    url_  = p_my_rule_->url; 
    protocol_ = p_my_rule_->protocol;
    return_type_ = p_my_rule_->return_type;
    redirect_url_ = p_my_rule_->redirect_url;
    ciStringUtil::safeToUpper(return_type_);

    //webapi_session_.InitSession(host_, url_, port_);
    is_init_ = true;    

    return true;
}

void elShopAnnouncerEndpoint::End()
{
    ciDEBUG(5, ("End(%s)", id_.c_str()));

    Stop();
    is_init_ = false;
}

bool elShopAnnouncerEndpoint::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));

    if (is_start_ == true) {
        ciDEBUG(5, ("Already Start(%s)", id_.c_str()));
        return true;
    }

    if (option == ADMIN_STATE_START_NO) {
        ciDEBUG(5, ("Start ADMIN_STATE_START_NO (%d)", option));
    } else if (option == ADMIN_STATE_START_ALL) {
        InitTimer("elEndpoint Timer", 60);
        StartTimer();
        StartThread();
    } else if (option == ADMIN_STATE_START_TIMER) {
        StartTimer();
    } else if (option == ADMIN_STATE_START_THREAD) {
        StartThread();
    } else if (option == ADMIN_STATE_START_CHILD) {
    } else {
        ciERROR(("Failed! Start option(%s)", option.c_str()));
        return false;
    }

    is_start_ = true;
    return true;
}

bool elShopAnnouncerEndpoint::Stop()
{
    ciDEBUG(5, ("Stop()"));

    if (is_start_ == false) {
        ciDEBUG(5, ("Already Stop(%s)", id_.c_str()));
        return true;
    }

    StopTimer();
    StopThread();

    // Stop child
    for (unsigned int i = 0; i < child_vec_.size(); i++) {
        child_vec_[i]->Stop();
    }

    is_start_ = false;
    return true;
}


bool elShopAnnouncerEndpoint::SetNotifyCustomerWishlist_(elRecordList& records)
{
    ciDEBUG(5, ("SetNotifyCustomerWishlist_(%d)", records.size()));

    elRuleRepository* p_rule_repository = elRuleRepository::GetInstance();
    p_rule_repository->GetPropertyCodeName("LOCATION", "code");


    string str_request_time("");
    string str_flore_name("");
    multimap<string, elRecord>::iterator itr;
    multimap<string, elRecord> sort_wishlist_mmap_;

    // requestTime 기준으로 asc 소트
    for (size_t i = 0; i < records.size(); i++) {
        str_request_time = records[i][6].value;  // requestTime로 확인
        sort_wishlist_mmap_.insert(multimap<string, elRecord>::value_type(str_request_time, records[i]));
        ciDEBUG(5, ("SetNotifyCustomerWishlist_(%s, %s)", str_request_time.c_str(), records[i][2].value.c_str()));
    }

    records.clear();

    // requestTime 기준으로 asc 소트
    //for (itr = sort_wishlist_mmap_.begin(); itr != sort_wishlist_mmap_.end(); itr++) {
    //    records.push_back(itr->second);
    //}

    // requestTime 기준으로 desc 소트
    multimap<string, elRecord>::reverse_iterator rit;
    for (rit = sort_wishlist_mmap_.rbegin(); rit != sort_wishlist_mmap_.rend(); ++rit) {
        // 층 필드 추가
        elField field;        
        field.value = p_rule_repository->GetPropertyCodeName("LOCATION", rit->second[2].value);
        field.name = "flore";
        field.type = "VARCHAR";
        field.is_exist = true;
        rit->second.push_back(field);
        
        records.push_back(rit->second);
    }

    return true;
}



bool elShopAnnouncerEndpoint::TestSetNotifyCustomerWishlist_(elRecordList& records)
{
    ciDEBUG(5, ("SetNotifyCustomerWishlist_(%d)", records.size()));

    string str_request_id("");
    map<string, elRecord>::iterator itr;
    map<string, elRecord> new_wishlist_map_;

    for (size_t i = 0; i < records.size(); i++) {
        str_request_id = records[i][0].value;  // requestId로 확인
	    itr = m_wishlist_notify_map_.find(str_request_id);

	    if (itr == m_wishlist_notify_map_.end()) {
		    new_wishlist_map_[str_request_id] = records[i];
	    }
    }
    m_wishlist_notify_map_.clear();
    m_wishlist_notify_map_ = new_wishlist_map_;

    return true;
}