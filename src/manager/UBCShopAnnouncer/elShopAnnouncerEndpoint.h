/** \class elShopAnnouncerEndpoint
 *  Copyright �� 2015, SQIsoft ELiga. All rights reserved.
 *
 *  \brief elShopAnnouncerEndpoint 
 *  
 *  \author SQIsoft, detect8
 *  \version 1.0
 *  \purpose
 *  \date 2016/03/29 11:01:00
 */

#ifndef _elShopAnnouncerEndpoint_h_
#define _elShopAnnouncerEndpoint_h_

#include "common/libElComponent/Endpoint/elEndpoint.h"

class CUBCShopAnnouncerDlg;

class elShopAnnouncerEndpoint : public elEndpoint
{
public:
    elShopAnnouncerEndpoint(CUBCShopAnnouncerDlg* pDlg);
    virtual ~elShopAnnouncerEndpoint(void);

    virtual bool Submit(string el_command_type, string el_command_id, string str_command, string& entity, elRecord& field_name_arr, elRecordList& records);

    bool    Init(string my_id);
    void    End();
    bool    Start(string option = ADMIN_STATE_START_NO);
    bool    Stop();

    string  GetRedirectUrl();

protected:
    bool    SetNotifyCustomerWishlist_(elRecordList& records);
    bool    TestSetNotifyCustomerWishlist_(elRecordList& records);

protected:
    ciMutex		    m_lock_;

    string          group_;
    string          user_;
    string          pass_;
    string          host_;
    ciLong          port_;    
    string          url_;
    string          protocol_;
    string          return_type_;
    string          redirect_url_;

    string          entity_;
    elRecord        field_name_arr_;

    
    map<string, elRecord> m_wishlist_notify_map_;

    CUBCShopAnnouncerDlg* m_shop_announce_dlg_;
};

#endif //_elShopAnnouncerEndpoint_h_