#include "StdAfx.h"
#include "UBCShopAnnounceWorld.h"

#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciEnv.h>
#include <common/libInstall/installUtil.h>
#include <common/libCommon/ubcIni.h>

ciSET_DEBUG(5, "UBCShopAnnounceWorld");

UBCShopAnnounceWorld::UBCShopAnnounceWorld() {
	//ciDEBUG(1,("UBCShopAnnounceWorld()"));

}

UBCShopAnnounceWorld::~UBCShopAnnounceWorld() {
	//ciDEBUG(1,("~UBCShopAnnounceWorld()"));
}

ciBoolean UBCShopAnnounceWorld::init(int& p_argc, char** &p_argv) 
{
	ciDEBUG(1,("UBCShopAnnounceWorld::init(%d)", p_argc));	
	
	if(ciWorld::init(p_argc,p_argv) == ciFalse) {
		ciERROR(("ciWorld::init() is FAILED"));
		return ciFalse;
	}

    //시스템 전체의 디버그를 한번에 무력화한다. IGNORE_COP_DEBUG=1
	//const char*  ignoreDebugEnv = getenv("IGNORE_COP_DEBUG");
	//if(ignoreDebugEnv==0 || ignoreDebugEnv[0] != '1'){
	//	ciDebug::setDebugOn();
    //    // log 파일을 오픈한다.
	//	ciWorld::ylogOn();
	//}

    //UBCShopAnnouncer 의 디버그를 enable 한다. UBCSHOPANNOUNCER_COP_DEBUG=1
	const char*  copDebugEnv = getenv("UBCSHOPANNOUNCER_COP_DEBUG");
	if(copDebugEnv != NULL && copDebugEnv[0] == '1'){
		ciDebug::setDebugOn();
        // log 파일을 오픈한다.
		ciWorld::ylogOn();

        // 224테스트 : 이 세줄이 밖으로 빠져 있으면 UBCSHOPANNOUNCER_COP_DEBUG가 정의되어 있지 않은 경우 
        //             프로그램 종료시 copTaoCI.dll에서 TO-DO 오류가 발생한다.
	    ciEnv::defaultEnv(true,"utv1");
	    if(ciWorld::hasWatcher() == ciTrue) {}
	    extraJob();

	}

		
/*
	_watcher = ciWatcher::getInstance(_procName.c_str(), stdout);
	_watcher->init();
	_console = new ciConsoleHandler();
*/	
	// +w is Default Option
	
	return ciTrue;
}

void UBCShopAnnounceWorld::extraJob()
{

	// SiteID 를 UBCVariables.ini 로 옮겨놓는다.
	//installUtil::getInstance()->propertyMigration1();

	// Starter 에서 브라우저가 펌웨어뷰 뜬 다음 뜨도록 할것
	//installUtil::getInstance()->runBrowserAfterFirmware(_edition);
	
	//ciString dsService;
	//ciXProperties::getInstance()->get("COP.NAME_GROUP.Site",dsService);
	//if(dsService!="PM=*") {
	//	ciDEBUG(1,("DiRECTORY SERVICE ADD"));
	//	installUtil::getInstance()->setProperty("COP.DIRECTORY_SERVICE.PM/Site","UBC_SITE,mgrId,siteId",ciFalse);
	//	installUtil::getInstance()->setProperty("COP.NAME_GROUP.Site","PM=*",ciFalse);
	//}
	

}


ciBoolean UBCShopAnnounceWorld::fini(long p_finiCode)
{
	ciDEBUG(1,("fini()"));

	//ciWorld::ylogOff();
    //	return ciWorld::fini(p_finiCode);
	ciDEBUG(1,("fini end()"));
	return 1;
}



void  UBCShopAnnounceWorldThread::init() 
{ 
	world = new UBCShopAnnounceWorld();
	if(!world) {
		 exit(1);
	}
	if(world->init(__argc, __argv) == ciFalse) {
		 world->fini(2);
		 exit(1);
	}
}

void 
UBCShopAnnounceWorldThread::run()
{
	ciDEBUG(1,("world->run()"));
	world->run();
	//world->fini();

}

void 
UBCShopAnnounceWorldThread::destroy()
{
	ciDEBUG(1,("destroy()"));
	world->fini(0);
}