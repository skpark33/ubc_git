// UBCShopAnnouncerDlg.h : 헤더 파일
//

#pragma once

#include "resource.h"
#include "TrayIcon.h"
#include "ShowmeMsgDlg.h"
#include <vector>

#include "common/libElComponent/Component/elGateway.h"
#include "elMcmApiEndpoint.h"
#include "elShopAnnouncerEndpoint.h"
#include "afxcmn.h"
#include "HoverButton.h"

// CUBCShopAnnouncerDlg 대화 상자
class CUBCShopAnnouncerDlg : public CDialog
{
// 생성입니다.
public:
	CUBCShopAnnouncerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
    ~CUBCShopAnnouncerDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCSHOPANNOUNCER_DIALOG };
    enum { WM_ICON_NOTIFY = WM_USER + 1};
    enum { TEST_TIMER, HIDE_TIMER };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

protected:
    LRESULT OnTrayNotification(WPARAM wParam,LPARAM lParam);

/// [ business
public:
    void    NotifyWishlist(elRecordList& record_list);

protected:
    void    InitElComponent_();
    void    EndElComponent_();
    void    TestNotifyShowmeMessage_();
    void    NotifyShowmeMessage_();

    // list control
    void    InitListCtrlShowme_();
    int     ShowListCtrlShowme_();
    void	InitButton_();

    // Shell 실행
    void    ExecuteBrowser_();
    void    ShellExeTest_();
    bool    GetPidVector_(const char* exename, vector<unsigned long>& pid_vec);
    HWND    GetWHandle_(unsigned long pid);
    void    GetDefaultBrowser_(LPTSTR szBrowserName);
    CWnd*   SearchWindow_(CWnd* p_cwnd, CString& cstrSearchCaptionName, CString& cstrSearchClassName);

    // 중복 확인
    bool    IsOverlapWindow();

    // 다이알로그 시작위치 우측하단
    void    SetMyDlgPos_();

private:
    CBitmap   m_background;
    CBrush    m_brush;
	CTrayIcon m_TrayIcon;
    ULONG     m_timecount_;
    std::vector<CShowmeMsgDlg*> m_showme_dlg_vec;

    // elComponent
    elGateway*               m_gateway_;
    elMcmApiEndpoint*        m_mcm_api_endpoint_;
    elShopAnnouncerEndpoint* m_shop_announcer_endpoint_;

    ciMutex		             m_lock_;
    elRecordList             m_records_;

    // 윈도우 초기 위치
    int m_start_x_;
    int m_start_y_;

///]
public:
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnExit();
    afx_msg void OnBnClickedButtonSearch();
    afx_msg void OnBnClickedButtonHide();
    afx_msg void OnMenuAnnounce();
    afx_msg void OnMenuShowme();
    afx_msg void OnLvnItemchangedListShowme(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnNMDblclkListShowme(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);

    CListCtrl       m_list_ctrl_showme;
    CHoverButton    m_btn_showme_search;
    CHoverButton    m_btn_dlg_hide;
};

