#ifndef _UBCShopAnnounceWorld_h_
#define _UBCShopAnnounceWorld_h_

#include <ci/libWorld/ciWorld.h> 
#include "ci/libThread/ciThread.h"
#include <common/libScratch/scratchUtil.h>

class UBCShopAnnounceWorld : public ciWorld {
public:
	UBCShopAnnounceWorld();
	~UBCShopAnnounceWorld();
 	
	//[
	//	From ciWorld
	//
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);

    void	extraJob();
		//
	//]
protected:
	ciString _edition;
};
	
class UBCShopAnnounceWorldThread : public virtual ciThread {
public:
	void init();
	void run();
	void destroy();
	
protected:
	UBCShopAnnounceWorld* world;
};
#endif //_UBCShopAnnounceWorld_h_
