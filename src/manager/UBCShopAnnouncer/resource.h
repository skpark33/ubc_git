//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCShopAnnouncer.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_UBCSHOPANNOUNCER_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       130
#define IDI_TRAY_ICON2                  134
#define IDD_DIALOG_SHOWME               135
#define IDB_BTN_SHOWME_SEARCH           136
#define IDB_BTN_DLG_HIDE                137
#define IDB_TEXT_SHOWME_WISHLIST        138
#define IDB_TEXT_SHOWME_WISHLIST_WIDE   139
#define IDI_TRAY_ICON                   140
#define IDI_ICON1                       142
#define IDI_TRAY_ICON3                  142
#define IDI_ICON2                       145
#define IDB_BTN_DLG_HIDE_SMALL          148
#define IDB_BTN_SHOWME_SEARCH_SMALL     150
#define IDB_SHOWME_DLG_BG               151
#define IDB_TEXT_SHOWME_WISHLIST_WHITE  152
#define IDB_BITMAP2                     155
#define IDB_TEXT_MCM_SHOWME             155
#define IDB_BITMAP1                     156
#define IDB_BTN_SHOWME_LIST             156
#define IDB_BITMAP3                     157
#define IDB_BTN_SHOWME_HIDE             157
#define IDC_EDIT_SHOWME_REQ             1002
#define IDC_LIST_SHOWME                 1003
#define IDC_BUTTON_SEARCH               1004
#define IDC_BUTTON_HIDE                 1005
#define IDC_STATIC_SHOWME_WISHLIST      1006
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define IDM_EXIT                        32774
#define ID_MENU_ANNOUNCE                32775
#define ID_MENU_SHOWME                  32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        158
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
