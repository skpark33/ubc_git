// UBCShopAnnouncerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCShopAnnouncer.h"
#include "UBCShopAnnouncerDlg.h"
#include "ShowmeMsgDlg.h"
#include "common/libElComponent/Endpoint/elEndpointManager.h"
#include <ci/libDebug/ciDebug.h>
#include <tlhelp32.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ciSET_DEBUG(5, "CUBCShopAnnouncerDlg");

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CUBCShopAnnouncerDlg 대화 상자




CUBCShopAnnouncerDlg::CUBCShopAnnouncerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCShopAnnouncerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

    m_gateway_ = NULL;
    m_mcm_api_endpoint_ = NULL;
    m_shop_announcer_endpoint_ = NULL;
}
CUBCShopAnnouncerDlg::~CUBCShopAnnouncerDlg()
{
    m_TrayIcon.RemoveIcon();

    EndElComponent_();
    //Sleep(500);
}

void CUBCShopAnnouncerDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_LIST_SHOWME, m_list_ctrl_showme);
    DDX_Control(pDX, IDC_BUTTON_SEARCH, m_btn_showme_search);
    DDX_Control(pDX, IDC_BUTTON_HIDE, m_btn_dlg_hide);

}

BEGIN_MESSAGE_MAP(CUBCShopAnnouncerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
    ON_WM_ERASEBKGND()
	ON_WM_QUERYDRAGICON()
    ON_MESSAGE(WM_ICON_NOTIFY,OnTrayNotification)
    ON_WM_TIMER()
	//}}AFX_MSG_MAP
    ON_COMMAND(IDM_EXIT, &CUBCShopAnnouncerDlg::OnExit)
    ON_BN_CLICKED(IDC_BUTTON_SEARCH, &CUBCShopAnnouncerDlg::OnBnClickedButtonSearch)
    ON_BN_CLICKED(IDC_BUTTON_HIDE, &CUBCShopAnnouncerDlg::OnBnClickedButtonHide)
    ON_COMMAND(ID_MENU_ANNOUNCE, &CUBCShopAnnouncerDlg::OnMenuAnnounce)
    ON_COMMAND(ID_MENU_SHOWME, &CUBCShopAnnouncerDlg::OnMenuShowme)
    ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_SHOWME, &CUBCShopAnnouncerDlg::OnLvnItemchangedListShowme)
    ON_NOTIFY(NM_DBLCLK, IDC_LIST_SHOWME, &CUBCShopAnnouncerDlg::OnNMDblclkListShowme)
    ON_BN_CLICKED(IDC_BUTTON_SEARCH, &CUBCShopAnnouncerDlg::OnBnClickedButtonSearch)
    ON_BN_CLICKED(IDC_BUTTON_HIDE, &CUBCShopAnnouncerDlg::OnBnClickedButtonHide)
END_MESSAGE_MAP()


// CUBCShopAnnouncerDlg 메시지 처리기
bool CUBCShopAnnouncerDlg::IsOverlapWindow()
{
    HANDLE hEvent;
    hEvent = CreateEvent(NULL, FALSE, TRUE, AfxGetAppName()); 
    if ( GetLastError() == ERROR_ALREADY_EXISTS) { 
        //AfxMessageBox("UBCShopAnnouncer가 이미 실행중입니다.");
        return true;
    }
    return false;
}

//ON_WM_ERASEBKGND()
BOOL CUBCShopAnnouncerDlg::OnEraseBkgnd(CDC* pDC)
{
    CRect rt;  
    this->GetWindowRect(&rt); 
    this->ScreenToClient(&rt);  
    pDC->FillRect(&rt, &m_brush); 
    return TRUE; 
}

BOOL CUBCShopAnnouncerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

    if (IsOverlapWindow()) {
        ciERROR(("Failed! UBCShopAnnouncer already executed!"));
        PostQuitMessage(0);
        //return FALSE;
    }

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

    // UBCShopAnnounce의 TrayIcon 생성
    if(!m_TrayIcon.Create(this,WM_ICON_NOTIFY,_T("SHOW ME"),NULL,IDR_MENU1)) {
        ciERROR(("Failed! m_TrayIcon.Create()"));
        return FALSE;
    }

    //
	m_TrayIcon.SetIcon(IDI_TRAY_ICON3);


	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.   
    m_background.LoadBitmapA(IDB_SHOWME_DLG_BG);
    m_brush.CreatePatternBrush(&m_background);

    InitListCtrlShowme_();
    InitButton_();
    InitElComponent_();
    SetMyDlgPos_();

    // 테스트
    //ShellExeTest_();

    m_timecount_ = 0;
    SetTimer(HIDE_TIMER, 10, NULL);


	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUBCShopAnnouncerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCShopAnnouncerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
    //m_list_ctrl_showme.ShowScrollBar( SB_VERT, TRUE );
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCShopAnnouncerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

LRESULT CUBCShopAnnouncerDlg::OnTrayNotification(WPARAM wParam,LPARAM lParam)
{
	return m_TrayIcon.OnTrayNotification(wParam,lParam);
}

void CUBCShopAnnouncerDlg::OnTimer(UINT nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(nIDEvent == HIDE_TIMER)
	{
		ShowWindow(SW_HIDE);
		KillTimer(HIDE_TIMER);

        // 테스트 타이머
        //SetTimer(TEST_TIMER, 10000, NULL);
    } else if (nIDEvent == TEST_TIMER) {
        ShowWindow(SW_SHOW);

        //TestNotifyShowmeMessage_();
        //NotifyShowmeMessage_();
	    
        KillTimer(TEST_TIMER);
        //SetTimer(TEST_TIMER, 10, NULL);

    }

	CDialog::OnTimer(nIDEvent);
}

void CUBCShopAnnouncerDlg::OnExit()
{
    // TODO: 여기에 명령 처리기 코드를 추가합니다.
    for (int i = 0; i < (int) m_showme_dlg_vec.size(); i++) {
        delete m_showme_dlg_vec[i];
    }
    m_showme_dlg_vec.clear();

    PostQuitMessage(0);
}

void CUBCShopAnnouncerDlg::SetMyDlgPos_()
{
    ciDEBUG(5, ("SetMyDlgPos_()"));

    RECT desk_rect;
    RECT my_rect;
    HWND desk_hwnd = ::GetDesktopWindow();
    
    if(!::GetWindowRect(desk_hwnd, &desk_rect)) {
        m_start_x_ = 0;
        m_start_y_ = 0;
        return;
    }
    this->GetWindowRect(&my_rect);

    m_start_x_ = desk_rect.right - my_rect.right;
    m_start_y_ = desk_rect.bottom - my_rect.bottom - 40;
    ciDEBUG(5, ("SetMyDlgPos_ Okay(%d, %d)", m_start_x_, m_start_y_));
}

void CUBCShopAnnouncerDlg::NotifyWishlist(elRecordList& record_list)
{
    ciDEBUG(5, ("NotifyWishlist()"));
    //{
    // guard를 걸면 두번째 죽는다. 왜?
    //ciMutex guard(m_lock_);
    m_records_.clear();
    m_records_ = record_list;
    if (ShowListCtrlShowme_() == 0) {
        //ShowWindow(SW_HIDE);
        return;
    }
    //}

    m_list_ctrl_showme.ModifyStyle( WS_HSCROLL, 0 );
    ::SetWindowPos(this->GetSafeHwnd(), HWND_TOPMOST, m_start_x_, m_start_y_, 0, 0, SWP_NOSIZE);
    ShowWindow(SW_SHOW);
    

    ::SetWindowPos(this->GetSafeHwnd(), HWND_NOTOPMOST, m_start_x_, m_start_y_, 0, 0, SWP_NOSIZE);
}

void CUBCShopAnnouncerDlg::NotifyShowmeMessage_()
{
    m_timecount_++;

    CShowmeMsgDlg* showme_msg_dlg = new CShowmeMsgDlg;
    showme_msg_dlg->Create(IDD_DIALOG_SHOWME, this);

    // 최신 알리미가 맨 뒤로 간다.
    //showme_msg_dlg->SetWindowPos(showme_msg_dlg->GetOwner()->, m_timecount_*20+480, m_timecount_*20+270, 0, 0, SWP_NOSIZE);
    // 맨위로
    ::SetWindowPos(showme_msg_dlg->GetSafeHwnd(), HWND_TOPMOST, m_timecount_*20+480, m_timecount_*20+270, 0, 0, SWP_NOSIZE);
    showme_msg_dlg->ShowWindow(SW_SHOW);        

    m_showme_dlg_vec.push_back(showme_msg_dlg);

    // 알리미 위치 초기화
    if (m_timecount_*20+480 > 960) {
        m_timecount_ = 0;
    }
}

void CUBCShopAnnouncerDlg::InitElComponent_()
{
    // 이 클래스를 External elEndpoint로 등록하기 위해 id, type, name을 mcm_rule.cfg에 설정한 대로 등록한다.
    //EP_MCM_API_001
    //    id_ = "EP_SHOP_ANNOUNCER_001";
    //    type_ = "WINDOW";
    //    name_ = "EP_SHOP_ANNOUNCER_001";

    // Endpoint 생성하고 NotifyShowmeMessage로 이벤트를 받도록 this 등록
    m_mcm_api_endpoint_ = new elMcmApiEndpoint();
    m_shop_announcer_endpoint_ = new elShopAnnouncerEndpoint(this);

    elEndpointManager* endpoint_manager = elEndpointManager::GetInstance();
    endpoint_manager->AddEndpoint("EP_MCM_API_001", m_mcm_api_endpoint_);
    endpoint_manager->AddEndpoint("EP_SHOP_ANNOUNCER_001", m_shop_announcer_endpoint_);

    m_gateway_ = new elGateway;
    //bool b_ret = m_gateway_->Init("MCMGW", "C:\\SQISoft\\UTV1.0\\execute\\data\\mcm_rule.cfg");
    bool b_ret = m_gateway_->Init("MCMGW", "data\\mcm_rule.cfg");
    if (!b_ret) {
        ciDEBUG(5, ("###MCM gateway 생성 오류"));
        //AfxMessageBox("###MCM gateway 생성 오류");
        PostQuitMessage(0);
    }
    m_gateway_->Start(ADMIN_STATE_START_ALL);
}

void CUBCShopAnnouncerDlg::EndElComponent_()
{
    if (m_gateway_) {
        m_gateway_->Stop();
        delete m_gateway_;
    }

    elEndpointManager* endpoint_manager = elEndpointManager::GetInstance();

    if (m_mcm_api_endpoint_) {
        endpoint_manager->DelEndpoint(m_mcm_api_endpoint_->GetId());
        delete m_mcm_api_endpoint_;
    }
    if (m_shop_announcer_endpoint_) {
        endpoint_manager->DelEndpoint(m_shop_announcer_endpoint_->GetId());
        delete m_shop_announcer_endpoint_;
    }
    elEndpointManager::ClearInstance();
}

// EX
//CreateFont(20  //  1   nHeight
//           , 50  //  2   nWidth
//           , 0  //  3   nEscapement
//           , 0  //  4   nOrientation
//           , 0  //  5   fnWeight
//           , 0  //  6   fdwItalic
//           , 0  //  7   fdwUnderline
//           , 0  //  8   fdwStrikeOut
//           , 0  //  9   fdwCharSet
//           , 0  //  10  fdwOutputPrecision
//           , 0  //  11  fdwClipPrecision
//           , 1  //  12  fdwQuality
//           , 0  //  13  fdwPitchAndFamily
//           , "궁서"   //  14  lpszFace
//           );
void CUBCShopAnnouncerDlg::InitListCtrlShowme_()
{
    ciDEBUG(5, ("InitListCtrlShowme_"));

    // Font 변경
    HFONT hNewFont;    
    hNewFont=CreateFont( 11,0,0,0,0,0,0,0,HANGEUL_CHARSET,3,2,1,        
                                   VARIABLE_PITCH | FF_MODERN,"돋움체");
    m_list_ctrl_showme.SendMessage( WM_SETFONT, (WPARAM)hNewFont, (LPARAM)TRUE);

    
    m_list_ctrl_showme.InsertColumn(0, _T("  고객번호"), LVCFMT_CENTER, 90);
    m_list_ctrl_showme.InsertColumn(1, _T("날짜"), LVCFMT_CENTER, 110);
    m_list_ctrl_showme.InsertColumn(2, _T("위치"), LVCFMT_CENTER, 43);
     
    m_list_ctrl_showme.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
    //m_list_ctrl_showme.SetExtendedStyle(LVS_EX_FULLROWSELECT);
    m_list_ctrl_showme.ModifyStyle(LVS_TYPEMASK, LVS_REPORT); 
    m_list_ctrl_showme.ModifyStyle( WS_HSCROLL, 0 );
    

    
    /*** 테스트
    for(int i=0;i<5;i++)
    {
        CString str_custormerId;
        CString str_date;
        CString str_flore;

        str_custormerId.Format("010-1234-1234");
        str_date.Format("2016-05-04 10:35");
        str_flore.Format("%d층", i+1);

        //각 컬럼에 ITEM삽입
        m_list_ctrl_showme.InsertItem(i, "0", 0); //컬럼줄

        m_list_ctrl_showme.SetItemText(i, 0, str_custormerId);
        m_list_ctrl_showme.SetItemText(i, 1, str_date);
        m_list_ctrl_showme.SetItemText(i, 2, str_flore);

        //InsertItem(row no, 문자열, 컬럼번호)
        //SetItemText(row no, 컬럼번호, 문자열)
    }
    ***/
}

void CUBCShopAnnouncerDlg::InitButton_()
{
    m_btn_showme_search.LoadBitmap(IDB_BTN_SHOWME_LIST, RGB(255,255,255));
    m_btn_dlg_hide.LoadBitmap(IDB_BTN_SHOWME_HIDE, RGB(255,255,255));
}

int CUBCShopAnnouncerDlg::ShowListCtrlShowme_()
{
    ciDEBUG(5, ("ShowListCtrlShowme_()"));

    m_list_ctrl_showme.DeleteAllItems(); 
    if (m_records_.size() == 0) {
        return 0;
    }

    for(int i = 0;i < (int) m_records_.size();i++)
    {
        CString str_custormerId;
        CString str_date;
        CString str_flore;
     
        if (m_records_[i][1].value == "") {
            m_records_[i][1].value = "000-0000-0000";
        }
        str_custormerId.Format("%-14s", m_records_[i][1].value.c_str());
        str_date.Format("%s", m_records_[i][6].value.substr(0,16).c_str());
        str_flore.Format("%s", m_records_[i][8].value.c_str());

        //각 컬럼에 ITEM삽입 행, 열
        m_list_ctrl_showme.InsertItem(i, _T("SHOWME"), 0);
        m_list_ctrl_showme.SetItemText(i, 0, str_custormerId);
        m_list_ctrl_showme.SetItemText(i, 1, str_date);
        m_list_ctrl_showme.SetItemText(i, 2, str_flore);

    }
    ciDEBUG(5, ("ShowListCtrlShowme_ Success(%d)", m_records_.size()));

    return (int)m_records_.size();
}

void CUBCShopAnnouncerDlg::TestNotifyShowmeMessage_()
{
    CString cstr_time_cnt;

    m_timecount_++;
    cstr_time_cnt.Format(_T("TEST_TIMER dddddddddddddddddddddddddddddddddddddddddddddddddddddd %d"), m_timecount_);

    //CShowmeMsgDlg showme_msg_dlg;
    //showme_msg_dlg.MoveWindow(m_timecount_*100, m_timecount_*100, 200, 100);
   	//INT_PTR nResponse = showme_msg_dlg.MessageBox(cstr_time_cnt.GetBuffer());
    //if (nResponse == IDOK)
    {
	    // TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
	    //  코드를 배치합니다.
        // MB_ICONEXCLAMATION, MB_OK
        //AfxMessageBox(cstr_time_cnt.GetBuffer(), MB_ICONEXCLAMATION);
    }

    CShowmeMsgDlg* showme_msg_dlg = new CShowmeMsgDlg;
    showme_msg_dlg->Create(IDD_DIALOG_SHOWME, this);

    // 윈도우 사이즈
    //showme_msg_dlg->MoveWindow(m_timecount_*20+960, m_timecount_*20+540, 400,150);

    ::SetWindowPos(showme_msg_dlg->GetSafeHwnd(), HWND_TOP, m_timecount_*20+480, m_timecount_*20+270, 0, 0, SWP_NOSIZE);
    // 최신 알리미가 맨 뒤로 간다.
    //showme_msg_dlg->SetWindowPos(showme_msg_dlg->GetOwner(), m_timecount_*20+480, m_timecount_*20+270, 0, 0, SWP_NOSIZE);
    // 최신 알리미가 맨 앞으로 간다.
    //showme_msg_dlg->SetWindowPos(NULL, m_timecount_*20+480, m_timecount_*20+270, 0, 0, SWP_NOSIZE);
    showme_msg_dlg->ShowWindow(SW_SHOW);        

    m_showme_dlg_vec.push_back(showme_msg_dlg);

    // 알리미 위치 초기화
    if (m_timecount_*20+480 > 960) {
        m_timecount_ = 0;
    }
}

void CUBCShopAnnouncerDlg::OnBnClickedButtonSearch()
{
    // TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    ExecuteBrowser_();
    ShowWindow(SW_HIDE);
    
}

void CUBCShopAnnouncerDlg::OnBnClickedButtonHide()
{
    // TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    ShowWindow(SW_HIDE);
}


void CUBCShopAnnouncerDlg::ExecuteBrowser_()
{
    ciDEBUG(5, ("ExecuteBrowser_"));
    
    // "http://211.232.57.186:8088/mcm"
    HINSTANCE hinst = ShellExecute(NULL, "open", m_shop_announcer_endpoint_->GetRedirectUrl().c_str(), NULL, NULL, SW_SHOW);    
    //HINSTANCE hinst = ShellExecute(NULL, "open", "iexplore.exe", m_shop_announcer_endpoint_->GetRedirectUrl().c_str(), NULL, SW_SHOW);
    unsigned long nRet = (unsigned long) hinst;
    if (nRet <= 32)  {
        ciERROR(("ShellExecute error hinstance(%d)", nRet));
    } else {
        ciDEBUG(5, ("ShellExecute success hinstance(%d)", nRet));
    }
}

CWnd* CUBCShopAnnouncerDlg::SearchWindow_(CWnd* p_cwnd, CString& cstrSearchCaptionName, CString& cstrSearchClassName)
{
    if (p_cwnd == NULL) {
        ciDEBUG(5, ("##### 0. p_cwnd is NULL"));
        return NULL;
    }

    //////////////////////////////////////////////////////
    // 입력된 윈도우에서 먼저 찾는다.
    // 1 찾는 윈도우가 현재 윈도우가 맞는지 확인
    CString cstr_parent_caption_text;

    // DEBUG-[8688]UBCShopAnnouncerDlg.cpp(line:448)::----- 0. Finding. Search Window hwnd(000615E4) CAPTION() CLASSNAME(Edit)
    // 이상하게 caption이 안읽어진다.
    p_cwnd->GetWindowText(cstr_parent_caption_text); 
    //::GetWindowTextW(p_cwnd->GetSafeHwnd(), (LPTSTR)cstr_parent_caption_text.GetBuffer(1024), 1024);


    CString cstr_parent_class_name;
    ::GetClassName(p_cwnd->GetSafeHwnd(), cstr_parent_class_name.GetBuffer(MAX_PATH), MAX_PATH);
    ciDEBUG(5, ("----- 0. Finding. Search Window hwnd(%p) CAPTION(%s)(%d) CLASSNAME(%s)"
        , p_cwnd->GetSafeHwnd(), (LPTSTR) cstr_parent_caption_text.GetBuffer(), cstr_parent_caption_text.GetLength(), (LPTSTR) cstr_parent_class_name.GetBuffer()));

    if (cstr_parent_caption_text == cstrSearchCaptionName && cstr_parent_class_name == cstrSearchClassName) {
        // 윈도우 찾음        
        ciDEBUG(5, ("##### 0. Finded. Search Window hwnd(%p) CAPTION(%s) CLASSNAME(%s)", p_cwnd->GetSafeHwnd(), (LPTSTR) cstr_parent_caption_text.GetBuffer(), (LPTSTR) cstr_parent_class_name.GetBuffer()));
        cstr_parent_caption_text.ReleaseBuffer();
        cstr_parent_class_name.ReleaseBuffer();

        return p_cwnd;
    }
    ciDEBUG(5, ("##### 0. No find"));

    //////////////////////////////////////////////////////
    // Child를 찾는다.
    CWnd* first_child = p_cwnd->GetWindow(GW_CHILD);
    if (first_child == NULL) {
        ciDEBUG(5, ("##### 1. first_child is NULL"));
        return NULL;
    }

    // 1 찾는 윈도우가 현재 윈도우가 맞는지 확인
    CString cstr_caption_text;
    first_child->GetWindowText(cstr_caption_text);

    CString cstr_class_name;
    ::GetClassName(first_child->GetSafeHwnd(), cstr_class_name.GetBuffer(MAX_PATH), MAX_PATH);

    ciDEBUG(5, ("----- 1. Finding. Search Window hwnd(%p) CAPTION(%s) CLASSNAME(%s)", first_child->GetSafeHwnd(), (LPTSTR) cstr_caption_text.GetBuffer(), (LPTSTR) cstr_class_name.GetBuffer()));
    if (cstr_caption_text == cstrSearchCaptionName && cstr_class_name == cstrSearchClassName) {
        // 윈도우 찾음        
        ciDEBUG(5, ("##### 1. Finded. Search Window hwnd(%p) CAPTION(%s) CLASSNAME(%s)", first_child->GetSafeHwnd(), (LPTSTR) cstr_caption_text.GetBuffer(), (LPTSTR) cstr_class_name.GetBuffer()));
        cstr_caption_text.ReleaseBuffer();
        cstr_class_name.ReleaseBuffer();

        return first_child;
    }
    ciDEBUG(5, ("##### 1. No find"));

    // 2 재귀적으로 Child를 뒤진다.
    CWnd* grand_child = SearchWindow_(first_child, cstrSearchCaptionName, cstrSearchClassName);
    if (grand_child) {
        // 윈도우 찾음      
        ciDEBUG(5, ("##### 2. find"));
        return grand_child;
    }
    ciDEBUG(5, ("##### 2. No find"));


    // 3 못찾으면 sibling을 찾는다.
    CWnd* sibling_cwnd = first_child->GetNextWindow(GW_HWNDNEXT);
    while (sibling_cwnd) {
        // sibling 먼저 찾고 그 밑에 child를 재귀적으로 찾음.
        grand_child = SearchWindow_(sibling_cwnd, cstrSearchCaptionName, cstrSearchClassName);
        if (grand_child) {
            // 윈도우 찾음   
            ciDEBUG(5, ("##### 3. find"));
            return grand_child;
        }
        ciDEBUG(5, ("##### 3. No find"));
        sibling_cwnd = sibling_cwnd->GetNextWindow(GW_HWNDNEXT);
    }

    ciDEBUG(5, ("##### 4. No find"));
    return NULL;
}

void CUBCShopAnnouncerDlg::ShellExeTest_()
{
    ciDEBUG(5, ("ShellExeTest"));

    SHELLEXECUTEINFO si;
    CString str_exe;
    ZeroMemory(&si, sizeof(SHELLEXECUTEINFO));

    si.cbSize = sizeof(SHELLEXECUTEINFO);
    si.lpVerb = _T("open");     // mode : print, write
    si.lpFile = _T("iexplore.exe"); // exe file
    si.lpParameters = _T("http://211.232.57.186:8088/mcm");   // exe argument
    si.nShow = SW_SHOWNORMAL;
    //si.hwnd;
    si.fMask = SEE_MASK_NOCLOSEPROCESS | SEE_MASK_FLAG_DDEWAIT;
    
    BOOL bRet = ::ShellExecuteEx(&si);

    ciDEBUG(5, ("\nShellExeTest si.hwnd(%p) si.hProcess(%p)", si.hwnd, si.hProcess));

    if (bRet == TRUE) {
        ::WaitForInputIdle(si.hProcess, INFINITE);
        //::Sleep(1000);

        vector<unsigned long> pid_vec;
        if (GetPidVector_("iexplore.exe", pid_vec)) {
            for (size_t i = 0; i < pid_vec.size(); i++) {
                ciDEBUG(5, ("@@@@@@@@@@@@@@ ShellExeTest Start si.hwnd(%p) si.hProcess(%p)", si.hwnd, si.hProcess));
                HWND hwnd = GetWHandle_(pid_vec[i]); 
                //클래스 이름 : TabWindowClass
                //창 캡션 :  Google - Internet Explorer
                //CWnd::GetWindowText 창 텍스트 또는 캡션 제목(있는 경우)을 반환합니다.
                //CWnd::GetWindowTextLength 창의 텍스트 또는 캡션 제목의 길이를 반환합니다

                CString cstrSearchCaptionName;
                CString cstrSearchClassName;

                CWnd* pWnd = CWnd::FromHandle(hwnd);
                //cstrSearchCaptionName = "http://211.232.57.186:8088/mcm";
                //cstrSearchClassName = "Edit";
                cstrSearchCaptionName = "http://211.232.57.186:8088/mcm - Internet Explorer";
                cstrSearchClassName = "TabWindowClass";

                CWnd* pFindedWnd = SearchWindow_(pWnd, cstrSearchCaptionName, cstrSearchClassName);
                
            }
        }

        //typedef DWORD (WINAPI *TFuncProc)(HANDLE hProcess);        
        //TFuncProc GetProcessIdFunc; 
        //HINSTANCE lib=::LoadLibraryA("kernel32.dll"); 
        //GetProcessIdFunc=(TFuncProc)::GetProcAddress(lib,"GetProcessId"); 
        //if(GetProcessIdFunc) 
//        { 
//            DWORD pid=::GetProcessId(si.hProcess); 
            //DWORD pid=GetProcessIdFunc(si.hProcess); 
            //::WindowFromAccessibleObject()

//            if(pid != 0) 
//            { 
//                HWND hwnd = GetWHandle_(pid); 
//                ciDEBUG(5, ("ShellExeTest iexplore pid(%ld) si.hwnd(%p) hwnd(%p)", pid, si.hwnd, hwnd)); 
//            } 
//        } 
        //FreeLibrary(lib); 
    }
}



/***
typedef struct tagPROCESSENTRY32
{
    DWORD   dwSize;
    DWORD   cntUsage;
    DWORD   th32ProcessID;          // this process
    ULONG_PTR th32DefaultHeapID;
    DWORD   th32ModuleID;           // associated exe
    DWORD   cntThreads;
    DWORD   th32ParentProcessID;    // this process's parent process
    LONG    pcPriClassBase;         // Base priority of process's threads
    DWORD   dwFlags;
    CHAR    szExeFile[MAX_PATH];    // Path
} PROCESSENTRY32;
typedef PROCESSENTRY32 *  PPROCESSENTRY32;
typedef PROCESSENTRY32 *  LPPROCESSENTRY32;
***/

bool CUBCShopAnnouncerDlg::GetPidVector_(const char* exename, vector<unsigned long>& pid_vec)
{
	ciDEBUG(5, ("GetPidVector_(%s)", exename));

	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE ) {
		ciERROR(("HANDLE을 생성할 수 없습니다"));
		return false;
	}
	PROCESSENTRY32 pe32 ;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	char strProcessName[512];

	if ( !Process32First ( hSnapshot, &pe32 ) )	{
		ciERROR(("Process32First failed."));
		::CloseHandle(hSnapshot);
		return false;
	}
	
	do 	{
		if( stricmp(pe32.szExeFile, exename) == 0 )		{		
			ciDEBUG(5, ("Process founded(%s) th32ProcessID(%ld), th32ModuleID(%ld), th32ParentProcessID(%ld) dwFlags(%ld)"
                ,exename, pe32.th32ProcessID, pe32.th32ModuleID, pe32.th32ParentProcessID, pe32.dwFlags));

			pid_vec.push_back(pe32.th32ProcessID);
		}
		
	} while (Process32Next( hSnapshot, &pe32 ));
	ciDEBUG(5, ("Process founded count(%d)", pid_vec.size()));
	::CloseHandle(hSnapshot);

	return true;
}



typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
    if (!::IsWindowVisible(hwnd)) return TRUE;
	DWORD pid;
    ::GetWindowThreadProcessId(hwnd, &pid);
	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;
	if (pe->pid != pid) return TRUE;
	pe->hwnd = hwnd;
	return FALSE;
}

HWND CUBCShopAnnouncerDlg::GetWHandle_(unsigned long pid)
{
    ciDEBUG(5, ("GetWHandle_(pid:%ld)", pid));
	if(pid>0){
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
        ::EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);
		
        // 최상위 윈도우 찾기? 일단 주석
		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1){
            parent =  ::GetParent(child);
			if(!parent){
                ciDEBUG(5, ("GetWHandle_(child hwnd:%p)", child));
				return child;
			}
			child = parent;
		}
        ciDEBUG(5, ("GetWHandle_(e.hwnd:%p)", e.hwnd));
		return e.hwnd;
	}
    ciDEBUG(5, ("GetWHandle_(HWND is NULL)"));
	return (HWND)0;
} 

void CUBCShopAnnouncerDlg::GetDefaultBrowser_(LPTSTR szBrowserName)
{
    HFILE h = ::_lcreat("dummy.htm", 0);
    ::_lclose(h);

    ::FindExecutable("", NULL, szBrowserName);
    ::DeleteFile("dummy.htm");
}

void CUBCShopAnnouncerDlg::OnMenuAnnounce()
{
    // TODO: 여기에 명령 처리기 코드를 추가합니다.
    m_list_ctrl_showme.ModifyStyle( WS_HSCROLL, 0 );
    ::SetWindowPos(this->GetSafeHwnd(), HWND_TOPMOST, m_start_x_, m_start_y_, 0, 0, SWP_NOSIZE);
    ShowWindow(SW_SHOW);
    
    ::SetWindowPos(this->GetSafeHwnd(), HWND_NOTOPMOST, m_start_x_, m_start_y_, 0, 0, SWP_NOSIZE);


}

void CUBCShopAnnouncerDlg::OnMenuShowme()
{
    // TODO: 여기에 명령 처리기 코드를 추가합니다.
    ExecuteBrowser_();
}

void CUBCShopAnnouncerDlg::OnLvnItemchangedListShowme(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
    // TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    *pResult = 0;

}

void CUBCShopAnnouncerDlg::OnNMDblclkListShowme(NMHDR *pNMHDR, LRESULT *pResult)
{
    // TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    *pResult = 0;

    // 리스트 항목 더블 클릭 시 아이템이 있으면 브라우저 띄움
    LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

    // 아이템 없으면 -1
    int row = pNMIA->iItem;
    int col = pNMIA->iSubItem;
    if (row >= 0) {
        ExecuteBrowser_();
        ShowWindow(SW_HIDE);        
    }

}
