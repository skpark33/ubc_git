#include "stdafx.h"
#include "elMcmApiEndpoint.h"
#include "common/libElComponent/Base/JsonBase.h"
#include <ci/libBase/ciStringUtil.h>

ciSET_DEBUG(5, "elMcmApiEndpoint");

elMcmApiEndpoint::elMcmApiEndpoint(void)
{
}

elMcmApiEndpoint::~elMcmApiEndpoint(void)
{
}

bool elMcmApiEndpoint::Submit(string el_command_type, string el_command_id, string str_command, string& entity, elRecord& field_name_arr, elRecordList& records)
{
    ciDEBUG(5, ("Submit(%s, %s, %s)", el_command_type.c_str(), el_command_id.c_str(), str_command.c_str()));

    string out_str_result("");
    bool ret_val;

    entity_ = entity;
    field_name_arr_ = field_name_arr;

    if (el_command_type == "getCustomerWishlist") {
        //http://211.232.57.186:8088/mcm/api/getCustomerWishlist.do?completeFlag=N
        webapi_session_.InitSession(host_, entity, port_);
        if (!webapi_session_.SendGetResult(str_command, out_str_result, "Content-Type: application/json;charset=UTF-8", false)) {
        //if (!webapi_session_.SendPostResult(str_command, out_str_result, "", false)) {
            ciERROR(("Failed getCustomerWishlist!!!! Submit:SendGetResult  WEB_GET"));
            return false;
        }
        ciDEBUG(5, ("Result(%s) OK", out_str_result.c_str()));
        ret_val = ParseCustomerWishlist_(out_str_result, records);

    } else {
        ciERROR(("Unknown command type : %s, %s", el_command_type.c_str(), el_command_id.c_str() ));
        return false;
    }

    return ret_val;
}

bool elMcmApiEndpoint::Init(string my_id)
{
    ciDEBUG(5, ("Init(%s)", my_id.c_str()));

    // elCommand마다 초기화 할 수 있기 때문에 같은 id로 이미 초기화 했으면 다시 안한다.
    if (IsInit() == true && my_id == id_) {
        ciDEBUG(5, ("Already Init(%s)", my_id.c_str()));
        return true;
    }

    p_rule_repository_ = elRuleRepository::GetInstance();
    p_my_rule_ = (elEndPointRule*) p_rule_repository_->GetEligaRule(my_id);
    if (!p_my_rule_) {
        ciERROR(("Failed! Get Rule(%s)", my_id.c_str()));
        return false;
    }
    id_ = my_id;
    type_ = p_my_rule_->type;
    name_ = p_my_rule_->name;
    oper_state_ = p_my_rule_->oper_state;
    admin_state_ = p_my_rule_->admin_state;
    
    group_ = p_my_rule_->group;
    user_ = p_my_rule_->user;
    pass_ = p_my_rule_->pass;
    host_ = p_my_rule_->host;
    port_ = p_my_rule_->port;
    url_  = p_my_rule_->url; 
    protocol_ = p_my_rule_->protocol;
    return_type_ = p_my_rule_->return_type;
    ciStringUtil::safeToUpper(return_type_);

    webapi_session_.InitSession(host_, url_, port_);
    
    is_init_ = true;
    return true;
}

void elMcmApiEndpoint::End()
{
    ciDEBUG(5, ("End(%s)", id_.c_str()));

    Stop();
    is_init_ = false;
}

bool elMcmApiEndpoint::Start(string option)
{
    ciDEBUG(5, ("Start(%s)", option.c_str()));
    
    if (is_start_ == true) {
        ciDEBUG(5, ("Already Start(%s)", id_.c_str()));
        return true;
    }

    if (option == ADMIN_STATE_START_NO) {
        ciDEBUG(5, ("Start ADMIN_STATE_START_NO (%d)", option));
    } else if (option == ADMIN_STATE_START_ALL) {
        InitTimer("elEndpoint Timer", 60);
        StartTimer();
        StartThread();
    } else if (option == ADMIN_STATE_START_TIMER) {
        StartTimer();
    } else if (option == ADMIN_STATE_START_THREAD) {
        StartThread();
    } else if (option == ADMIN_STATE_START_CHILD) {
    } else {
        ciERROR(("Failed! Start option(%s)", option.c_str()));
        return false;
    }

    is_start_ = true;
    return true;
}

bool elMcmApiEndpoint::Stop()
{
    ciDEBUG(5, ("Stop()"));

    if (is_start_ == false) {
        ciDEBUG(5, ("Already Stop(%s)", id_.c_str()));
        return true;
    }

    StopTimer();
    StopThread();

    // Stop child
    for (unsigned int i = 0; i < child_vec_.size(); i++) {
        child_vec_[i]->Stop();
    }

    is_start_ = false;
    return true;
}

bool elMcmApiEndpoint::ParseCustomerWishlist_(string& str_result, elRecordList& records)
{
    ciDEBUG(5, ("ParseCustomerWishlist_(%s)", str_result.c_str() ));

    JsonValue* temp_value = NULL;
    JsonObject* post_result_obj = NULL;
    JsonArray* record_array = NULL;

    string resultMsg;
    string resultFlag;
    string entity;
    string siteId;
    string attrNames;

    JsonValue* json_value = JsonValue::create(str_result.c_str(), 0, (int) str_result.length());
    if (json_value == NULL) {
        ciERROR(("Failed! ParseJsonResult_:str_result, JsonValue::create(%s) ", JsonValue::getErrorString().c_str() ));
        return false;
    }

    if (json_value->isObject() == false) {
        ciERROR(("Failed! ParseJsonResult_:json_value is not JsonObject "));
        delete json_value;
        return false;
    }
    post_result_obj = (JsonObject*) json_value;

    resultFlag = post_result_obj->getString("resultFlag");
    resultMsg = post_result_obj->getString("resultMsg");
    
    // false는 고객요청이 없는 경우이므로 record를 clear하고 true를 리턴한다. 
    // "resultMsg" : "customer Request List is Empty"
    if (resultFlag == "false") {
        ciDEBUG(5, ("API Call resultFlag(%s), resultMsg(%s)", resultFlag.c_str(), resultMsg.c_str() ));
        records.clear();
        delete post_result_obj;
        return true;
    }

    temp_value = post_result_obj->get("customerRequest");
    if (!temp_value) {
        ciERROR(("Failed! JsonValue is NULL "));
        delete post_result_obj;
        return false;
    } else if (!temp_value->isArray()) {
        ciERROR(("Failed! ParseJsonResult_:post_result_obj is not JsonArray "));
        delete post_result_obj;
        return false;
    }

    if(!GetRecordList_((JsonArray*) temp_value, records)) {
        delete post_result_obj;
        return false;
    }
    
    delete post_result_obj;
    return true;

}



bool elMcmApiEndpoint::GetRecordList_(JsonArray* pJsonArr, elRecordList& records)
{
    ciDEBUG(5, ("GetRecordList_()"));

    string str_temp("");
    JsonValue* temp_value = NULL;
    JsonArray* json_record = NULL;
    
    for (int i = 0; i < pJsonArr->getCount(); i++) {
        temp_value = pJsonArr->get(i);
        if (!temp_value) {
            ciERROR(("Failed! JsonValue(%d) is NULL ", i));
            return false;
        } else if (!temp_value->isObject()) {
            ciERROR(("Failed! record(%d) is not JsonObject ", i));
            return false;
        }

        elRecord record;
        if (!GetRecord_((JsonObject*) temp_value, record)) {
            ciERROR(("Failed! GetRecord_(%d)", i));
            return false;
        }
        records.push_back(record);        
    }
    ciDEBUG(5, ("End GetRecordList_(Record size:%d)", records.size()));

    return true;
}

bool elMcmApiEndpoint::GetRecord_(JsonObject* pObject, elRecord& record)
{
    ciDEBUG(5, ("GetRecord_(%s)", pObject->toString().c_str()));

    string str_temp("");
    JsonValue* temp_value = NULL;
    JsonString* temp_string = NULL;

    vector<string> str_name_vec;
    str_name_vec.push_back("requestId");
    str_name_vec.push_back("customerId");
    str_name_vec.push_back("locationCode");
    str_name_vec.push_back("locationName");
    str_name_vec.push_back("shopId");
    str_name_vec.push_back("deviceType");
    str_name_vec.push_back("requestTime");
    str_name_vec.push_back("completeFlag");
    
    for (int i = 0; i < pObject->getCount(); i++) {
        elField field;
        field.name = str_name_vec[i];
        field.type = EL_FIELD_TYPE_VAR_STRING;
        field.value = pObject->getString(str_name_vec[i].c_str());
        field.is_exist = true;

        record.push_back(field);
    }
    ciDEBUG(5, ("End GetRecord_(Field size:%d)", record.size()));
    return true;
}
