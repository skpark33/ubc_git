#pragma once

#include "afxwin.h"
#include "resource.h"
#include <vector>
using namespace std;
// CShowmeMsgDlg 대화 상자입니다.

class CShowmeMsgDlg : public CDialog
{
	DECLARE_DYNAMIC(CShowmeMsgDlg)

public:
	CShowmeMsgDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CShowmeMsgDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_SHOWME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

    virtual BOOL OnInitDialog();
    
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedOk();
    CEdit m_showme_req_text;
};

