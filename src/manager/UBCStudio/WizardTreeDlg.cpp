// WizardTreeDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "WizardTreeDlg.h"
#include "Enviroment.h"

#define MINUS_IMG_IDX	1
#define LINK_IMG_IDX	1
#define UNLINK_IMG_IDX	2
#define REPEAT_IMG_IDX	3

// CWizardTreeDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CWizardTreeDlg, CDialog)

CWizardTreeDlg::CWizardTreeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWizardTreeDlg::IDD, pParent)
	, m_wndWizard (NULL, 0,false)
	, m_Reposition(this)
{
	m_pDragImage = NULL; // 드래그시 생성된 이미지 사용
	m_hDragItem = NULL;  // 드래그시 처음 선택된 아이템 핸들 기억용
	m_pDragContentsInfo = NULL;

	m_pMainWizardContentsInfo = NULL;

	m_fontBold.CreateFont(
		14,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_BOLD,                   // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		"Tahoma"                   // lpszFacename
	);
}

CWizardTreeDlg::~CWizardTreeDlg()
{
}

void CWizardTreeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_WIZARD, m_treeWizardHier);
	DDX_Control(pDX, IDC_LIST_WIZARD_LIST, m_lcWizardList);
	DDX_Control(pDX, IDC_BUTTON_EXPAND, m_bnExpandTree);
	DDX_Control(pDX, IDC_BUTTON_LINK, m_bnLink);
	DDX_Control(pDX, IDC_BUTTON_UNLINK, m_bnUnlink);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_PLAY, m_bnPreview);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDC_STATIC_MAINWIZARD, m_gbGroupMainWizard);
	DDX_Control(pDX, IDC_STATIC_WIZARD, m_gbGroupWizard);
	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_gbGroupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_STATIC_WIZARDTREE, m_gbGroupWizardTree);
	DDX_Control(pDX, IDC_BUTTON_MAIN_WIZARD, m_bnMainWizard);
}


BEGIN_MESSAGE_MAP(CWizardTreeDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CWizardTreeDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CWizardTreeDlg::OnBnClickedCancel)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_TREE_WIZARD, &CWizardTreeDlg::OnNMCustomdrawTreeWizard)
	ON_NOTIFY(TVN_BEGINDRAG, IDC_TREE_WIZARD, &CWizardTreeDlg::OnTvnBegindragTreeWizard)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_PLAY, &CWizardTreeDlg::OnBnClickedButtonPreviewPlay)
	ON_NOTIFY(TVN_SELCHANGING, IDC_TREE_WIZARD, &CWizardTreeDlg::OnTvnSelchangingTreeWizard)
	ON_BN_CLICKED(IDC_BUTTON_LINK, &CWizardTreeDlg::OnBnClickedButtonLink)
	ON_BN_CLICKED(IDC_BUTTON_UNLINK, &CWizardTreeDlg::OnBnClickedButtonUnlink)
	ON_NOTIFY(LVN_BEGINDRAG, IDC_LIST_WIZARD_LIST, &CWizardTreeDlg::OnLvnBegindragListWizardList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_WIZARD_LIST, &CWizardTreeDlg::OnNMClickListWizardList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_WIZARD_LIST, &CWizardTreeDlg::OnNMDblclkListWizardList)
	ON_NOTIFY(NM_DBLCLK, IDC_TREE_WIZARD, &CWizardTreeDlg::OnNMDblclkTreeWizard)
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BUTTON_EXPAND, &CWizardTreeDlg::OnBnClickedButtonExpand)
	ON_BN_CLICKED(IDC_BUTTON_MAIN_WIZARD, &CWizardTreeDlg::OnBnClickedButtonMainWizard)
END_MESSAGE_MAP()


// CWizardTreeDlg 메시지 처리기입니다.

void CWizardTreeDlg::OnBnClickedOk()
{
	OnOK();
}

void CWizardTreeDlg::OnBnClickedCancel()
{
	OnCancel();
}

void CWizardTreeDlg::OnLvnBegindragListWizardList(NMHDR *pNMHDR, LRESULT *pResult)
{
//typedef struct tagNMLISTVIEW
//{
//    NMHDR   hdr;
//    int     iItem;
//    int     iSubItem;
//    UINT    uNewState;
//    UINT    uOldState;
//    UINT    uChanged;
//    POINT   ptAction;
//    LPARAM  lParam;
//} NMLISTVIEW, *LPNMLISTVIEW;
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nCnt = m_lcWizardList.GetSelectedCount();
	if (nCnt < 1) return;

	int nSelIndex = m_lcWizardList.GetNextItem(-1, LVIS_SELECTED);
	CPoint pt;

	// 드래그 이미지 생성
	if(m_pDragImage) m_pDragImage->DeleteImageList();
	m_pDragImage = m_lcWizardList.CreateDragImage(nSelIndex, &pt);

	CPoint ptAction(pNMLV->ptAction);

	// 드래그시 사용할 이미지 크기 계산
	CRect rcItem;
	m_lcWizardList.GetItemRect(nSelIndex, &rcItem, LVIR_BOUNDS);

	CRect rcWnd;
	m_lcWizardList.GetWindowRect(&rcWnd);
	ScreenToClient(&rcWnd);

	// 드래그를 시작
	//m_pDragImage->BeginDrag(0, CPoint(0,0));
	//m_pDragImage->BeginDrag(0, CPoint(ptAction.x - rcItem.left, ptAction.y - rcItem.top - rcWnd.top));
	m_pDragImage->BeginDrag(0, CPoint(ptAction.x, ptAction.y - rcItem.bottom - rcItem.Height()/2));
	//m_pDragImage->BeginDrag(0, CPoint(ptAction.x - rcWnd.left, ptAction.y - rcWnd.top));

	m_lcWizardList.ClientToScreen(&ptAction);
	ScreenToClient(&ptAction);

	// 드래그 이미지 표시
	m_pDragImage->DragEnter(this, ptAction);

	// 마우스 메시지를 잡아두고
	SetCapture();

	m_pDragContentsInfo = (CONTENTS_INFO*)m_lcWizardList.GetItemData(nSelIndex);
}

void CWizardTreeDlg::OnTvnBegindragTreeWizard(NMHDR *pNMHDR, LRESULT *pResult)
{
//typedef struct tagNMTREEVIEW {
//    NMHDR hdr;
//    UINT action;
//    TVITEM itemOld;
//    TVITEM itemNew;
//    POINT ptDrag;
//} NMTREEVIEW, *LPNMTREEVIEW;

	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	*pResult = 0;

	// 드래그 가능한 노드인지 체크
	// 마법사콘텐츠만 드래그 가능
	if( m_treeWizardHier.GetItemData(pNMTreeView->itemNew.hItem) == NULL )
	{
		return;
	}

	// 드래그 이미지 생성
	if(m_pDragImage) m_pDragImage->DeleteImageList();
	m_pDragImage = m_treeWizardHier.CreateDragImage(pNMTreeView->itemNew.hItem);

	CPoint ptAction(pNMTreeView->ptDrag);

	// 드래그시 사용할 이미지 크기 계산
	RECT rcItem;
	m_treeWizardHier.GetItemRect(pNMTreeView->itemNew.hItem, &rcItem, TRUE); // 아이콘을 포함하는 크기

	CRect rcWnd;
	m_treeWizardHier.GetWindowRect(&rcWnd);
	ScreenToClient(&rcWnd);

	// 드래그를 시작
	m_pDragImage->BeginDrag(0, CPoint(ptAction.x - rcItem.left + 16, ptAction.y - rcItem.top - rcWnd.top));

	m_treeWizardHier.ClientToScreen(&ptAction);
	ScreenToClient(&ptAction);

	// 드래그 이미지 표시
	m_pDragImage->DragEnter(this, ptAction);

	// 마우스 메시지를 잡아두고
	SetCapture();

	// 현재 선택된 아이템 핸들을 기억
	m_hDragItem = pNMTreeView->itemNew.hItem;
}

void CWizardTreeDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	CDialog::OnMouseMove(nFlags, point);

	// 드래그 중이 아니라면
	if(!m_pDragImage) return;

	CPoint ptScreen(point);
	ClientToScreen(&ptScreen);
	CWnd* pWndDrop = WindowFromPoint(ptScreen);
	BOOL bCopyEnable = FALSE;
	HTREEITEM hTargetItem = NULL;

	CPoint ptClient = ptScreen;
	pWndDrop->ScreenToClient(&ptClient);

	if(pWndDrop->m_hWnd == m_treeWizardHier.m_hWnd)
	{
		// 마우스가 위치한 아이템을 검사한다.항목이 트리 뷰 항목위에 있는지 확인하고 그렇다면 항목이 밝게 표시되도록한다.
		hTargetItem = m_treeWizardHier.HitTest(ptClient);
		bCopyEnable = (hTargetItem != NULL);

		// 드랍 가능한 노드인지 체크
		if(hTargetItem)
		{
			if((m_treeWizardHier.GetItemState(hTargetItem, TVIS_OVERLAYMASK) & INDEXTOOVERLAYMASK(MINUS_IMG_IDX)) != 0)
			{
				bCopyEnable = FALSE;
			}
			else
			{
				if(!IsFitChildNode(hTargetItem, m_hDragItem))
				{
					bCopyEnable = FALSE;
				}

				if(!IsFitChildNode(hTargetItem, (DWORD_PTR)m_pDragContentsInfo))
				{
					bCopyEnable = FALSE;
				}
			}
		}
	}

	// 드래그 이미지 그리기 중지
	m_pDragImage->DragLeave(this);
	//m_pDragImage->DragShowNolock(FALSE);

	if(bCopyEnable)
	{
		m_treeWizardHier.SelectDropTarget(hTargetItem);
	}
	else
	{
		m_treeWizardHier.SelectDropTarget(NULL);
	}

	// 드래그 이미지를 다시 보여준다.
	m_pDragImage->DragEnter(this, point);
	//m_pDragImage->DragShowNolock(TRUE);
	m_pDragImage->DragMove(point);

	SetCursor(LoadCursor(NULL, (bCopyEnable ? IDC_ARROW : IDC_NO)));
}

void CWizardTreeDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	CDialog::OnLButtonUp(nFlags, point);

	if(!m_pDragImage) return;

	// 마우스 메시지 캡쳐 기능을 제거한다.
	ReleaseCapture();

	// 드래그 과정을 중단한다.
	m_pDragImage->DragLeave(&m_treeWizardHier);
	m_pDragImage->EndDrag();
	m_pDragImage->DeleteImageList();
	m_pDragImage = NULL;

	// 일단 마지막으로 밝게 표시되었던 항목을 찾는다.
	HTREEITEM hTargetItem = m_treeWizardHier.GetDropHilightItem();
	if(!hTargetItem) return;

	// 밝게 표시된 드롭 항목의 선택을 취소한다.
	m_treeWizardHier.SelectDropTarget(NULL);

	CONTENTS_INFO* pContentsInfo = NULL;

	if(m_hDragItem)
	{
		// 현재 자식의 부모 아이템 핸들을 구한다.
		HTREEITEM hParentItem = m_treeWizardHier.GetNextItem(m_hDragItem, TVGN_PARENT);

		if( m_hDragItem != hTargetItem &&  // 선택된 아이템과 이동될 곳의 아이템이 같다면 이동할 필요가 없다.
			hParentItem != hTargetItem )   // 이동하려는 곳이 자신이 직접속한 항목 이라면 이동할 필요가 없다.
		{
			pContentsInfo = (CONTENTS_INFO*)m_treeWizardHier.GetItemData(m_hDragItem);
			SetWizardAction(m_hDragItem, NULL);
		}
	}
	else if(m_pDragContentsInfo)
	{
		pContentsInfo = m_pDragContentsInfo;
	}

	if(pContentsInfo)
	{
		SetWizardAction(hTargetItem, pContentsInfo);

		RefreshTree(m_strRefreshContentsId);

		HTREEITEM hWizardItem = FindChild(NULL, (DWORD_PTR)m_pDragContentsInfo);
		if(hWizardItem)
		{
			m_treeWizardHier.SelectItem(hWizardItem);
			m_treeWizardHier.Expand(hWizardItem, TVE_EXPAND);
			m_treeWizardHier.EnsureVisible(hWizardItem);
		}
	}

	m_hDragItem = NULL;
	m_pDragContentsInfo = NULL;
}

void CWizardTreeDlg::OnNMCustomdrawTreeWizard(NMHDR *pNMHDR, LRESULT *pResult)
{
//typedef struct tagNMTVCUSTOMDRAW {
//    NMCUSTOMDRAW nmcd;
//    COLORREF clrText;
//    COLORREF clrTextBk;
//#if (_WIN32_IE &gt;= 0x0400)
//    int iLevel;
//#endif
//} NMTVCUSTOMDRAW, *LPNMTVCUSTOMDRAW;
//typedef struct tagNMCUSTOMDRAWINFO {
//    NMHDR hdr;
//    DWORD dwDrawStage;
//    HDC hdc;
//    RECT rc;
//    DWORD_PTR dwItemSpec;
//    UINT uItemState;
//    LPARAM lItemlParam;
//} NMCUSTOMDRAW, *LPNMCUSTOMDRAW;
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	NMTVCUSTOMDRAW* pcd = (NMTVCUSTOMDRAW*)pNMHDR;
	*pResult = 0;

	switch ( pcd->nmcd.dwDrawStage )
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW; break;
	case CDDS_ITEMPREPAINT :
		HTREEITEM hItem = (HTREEITEM)pcd->nmcd.dwItemSpec;
		if(pNMCD->lItemlParam != NULL)
		{
			//m_treeWizardHier.SetItemState(hItem, TVIS_BOLD, TVIS_BOLD);
			//pcd->clrText = RGB(0, 0, 0);
			::SelectObject(pNMCD->hdc, (HFONT)m_fontBold);
			*pResult = CDRF_NEWFONT;
		}
		break;
	}
}

BOOL CWizardTreeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bnExpandTree.LoadBitmap(IDB_BTN_TREE , RGB(255, 255, 255));
	m_bnLink   .LoadBitmap(IDB_BTN_PLUS , RGB(255, 255, 255));
	m_bnUnlink .LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_bnPreview.LoadBitmap(IDB_BTN_PLAY , RGB(255, 255, 255));
	m_bnOK     .LoadBitmap(IDB_BTN_CLOSE, RGB(255, 255, 255));

	m_bnExpandTree.SetToolTipText(LoadStringById(IDS_WIZARD_TREE_BUT001));
	m_bnLink   .SetToolTipText(LoadStringById(IDS_WIZARD_TREE_BUT002));
	m_bnUnlink .SetToolTipText(LoadStringById(IDS_WIZARD_TREE_BUT003));
	m_bnPreview.SetToolTipText(LoadStringById(IDS_WIZARD_TREE_BUT004));

	m_lcWizardList.SetExtendedStyle(m_lcWizardList.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );
	m_lcWizardList.InsertColumn(0, LoadStringById(IDS_WIZARD_TREE_LST001), LVCFMT_LEFT, 190);

	CBitmap bmpContentsList1;
	bmpContentsList1.LoadBitmap(IDB_CONTENTS_LIST32);

	CBitmap bmpContentsList2;
	bmpContentsList2.LoadBitmap(IDB_CONTENTS_LIST32_ERROR);

	CBitmap bmpContentsList3;
	bmpContentsList3.LoadBitmap(IDB_CONTENTS_LIST32_SERVER);

	CBitmap bmpNormalNode;
	bmpNormalNode.LoadBitmap(IDB_OVERLAP_MINUS32);

	CBitmap bmpRepeat;
	bmpRepeat.LoadBitmap(IDB_OVERLAP_REPEAT32);

	if(m_ilWizardImage.Create(32, 32, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilWizardImage.Add(&bmpContentsList1, RGB(255,255,255));
		m_ilWizardImage.Add(&bmpContentsList2, RGB(255,255,255));
		m_ilWizardImage.Add(&bmpContentsList3, RGB(255,255,255));

		int nOverlayIndex = m_ilWizardImage.GetImageCount();

		m_ilWizardImage.Add(&bmpNormalNode, RGB(255,255,255));
		m_ilWizardImage.Add(&bmpRepeat, RGB(255,255,255));

		m_ilWizardImage.SetOverlayImage(nOverlayIndex  , MINUS_IMG_IDX ); // minus
		m_ilWizardImage.SetOverlayImage(nOverlayIndex+1, REPEAT_IMG_IDX); // repeat

		m_treeWizardHier.SetImageList(&m_ilWizardImage, TVSIL_NORMAL);
		m_lcWizardList.SetImageList(&m_ilWizardImage, LVSIL_SMALL);
	}

	CRect rcPreview;
	m_staticContents.GetWindowRect(rcPreview);
	rcPreview.DeflateRect(2,2);
	ScreenToClient(rcPreview);

	m_wndWizard.Create(NULL, "", WS_CHILD, rcPreview, this, 0xfeff);
	m_wndWizard.ShowWindow(SW_SHOW);

	RefreshList();
	RefreshTree(m_strInContentsId);

	HTREEITEM hWizardItem = m_treeWizardHier.GetChildItem(NULL);
	if(hWizardItem)
	{
		m_treeWizardHier.SelectItem(hWizardItem);
		m_treeWizardHier.Expand(hWizardItem, TVE_EXPAND);
		m_treeWizardHier.EnsureVisible(hWizardItem);
	}

	InitPosition(m_rcClient);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CWizardTreeDlg::RefreshList()
{
	m_lcWizardList.DeleteAllItems();

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return;

	CONTENTS_INFO_MAP* contents_map = pDoc->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_WIZARD) continue;
		if(m_strInContentsId == strContentsId)
		{
			m_pMainWizardContentsInfo = pContentsInfo;
			m_bnMainWizard.SetWindowText(pContentsInfo->strContentsName);
			continue;
		}

		int nIndex = m_lcWizardList.InsertItem(m_lcWizardList.GetItemCount(), pContentsInfo->strContentsName, pContentsInfo->nContentsType+1);
		m_lcWizardList.SetItemData(nIndex, (DWORD_PTR)pContentsInfo);
	}

//	m_lcWizardList.SetSortHeader(0);
//	m_lcWizardList.SortItems(CUTBListCtrlEx::Compare, (DWORD_PTR)&m_lcWizardList);
	//m_lcWizardList.SetSortHeader(0);
}

void CWizardTreeDlg::RefreshTree(CString strInContentsId)
{
	m_strRefreshContentsId = strInContentsId;

	m_treeWizardHier.DeleteAllItems();

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return;

	CWaitMessageBox wait;

	m_treeWizardHier.SetRedraw(FALSE);
	m_treeWizardHier.EnableWindow(FALSE);

	CONTENTS_INFO_MAP* contents_map = pDoc->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_WIZARD) continue;

		if(!strInContentsId.IsEmpty() && strInContentsId != strContentsId) continue;

		HTREEITEM hWizardItem = m_treeWizardHier.InsertItem(pContentsInfo->strContentsName, pContentsInfo->nContentsType+1, pContentsInfo->nContentsType+1);
		m_treeWizardHier.SetItemData(hWizardItem, (DWORD_PTR)pContentsInfo);

		InsertChildNode(hWizardItem, strContentsId);

		//TVITEMEX TVX;
		//TVX.mask = TVIF_INTEGRAL | TVIF_HANDLE;
		//TVX.hItem = hWizardItem;
		//TVX.iIntegral = 2;
		//m_treeWizardHier.SetItem(reinterpret_cast<TVITEM*>(&TVX));

		m_treeWizardHier.Expand(hWizardItem, TVE_EXPAND);
	}

	m_treeWizardHier.Expand(NULL, TVE_EXPAND);
	//m_treeWizardHier.SortChildren(NULL);

	m_treeWizardHier.EnableWindow(TRUE);
	m_treeWizardHier.SetRedraw(TRUE);
}

CONTENTS_INFO* CWizardTreeDlg::InsertChildNode(HTREEITEM hParentItem, CString strContentsId)
{
	if(strContentsId.IsEmpty()) return NULL;

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return NULL;

	CONTENTS_INFO* pContentsInfo = pDoc->GetContents(strContentsId);
	if(!pContentsInfo) return NULL;

	if(pContentsInfo->nContentsType != CONTENTS_WIZARD) return NULL;

	if(pContentsInfo->strWizardXML.IsEmpty()) return NULL;

	CXmlParser xmlWizardDtlData;
	if(xmlWizardDtlData.LoadXML(pContentsInfo->strWizardXML) != XMLPARSER_SUCCESS) return NULL;

	if(xmlWizardDtlData.SetXPath("/FlashWizard") != XMLPARSER_SUCCESS) return NULL;

	int nCnt = atoi(xmlWizardDtlData.GetAttribute("contents_count"));

	for(int i=0; i<nCnt ;i++)
	{
		CString strXPath;
		strXPath.Format("/FlashWizard/context [@index=\"%d\"]", i);
		if(xmlWizardDtlData.SetXPath(strXPath) != XMLPARSER_SUCCESS) return NULL;

		CString strType = xmlWizardDtlData.GetAttribute("type");
		CString strId   = xmlWizardDtlData.GetAttribute("id");
		CString strName;

		// 0000932: 플레쉬 마법사 계층구조에서 콘텐츠ID대신 콘텐츠명으로 보여줄것
		CONTENTS_INFO* pElementInfo = pDoc->GetContents(strId);
		if(pElementInfo)
		{
			strName = pElementInfo->strContentsName;
		}

		strXPath.Format("/FlashWizard/context [@index=\"%d\"]/value", i);
		if(xmlWizardDtlData.SetXPath(strXPath) != XMLPARSER_SUCCESS) return NULL;

		CString strValue = (xmlWizardDtlData.GetSelCount() > 0 ? xmlWizardDtlData.GetData(0) : _T(""));

		strType.MakeLower();

		CString strText;
		int nContensType = 0;
		if(strType == "text" )
		{
			nContensType = CONTENTS_TEXT +1;
			strText = strValue;
		}
		else if(strType == "image")
		{
			nContensType = CONTENTS_IMAGE+1;
			strText = strName;
		}
		else if(strType == "video")
		{
			nContensType = CONTENTS_VIDEO+1;
			strText = strName;
		}

		HTREEITEM hContentsItem = m_treeWizardHier.InsertItem(strText, nContensType, nContensType, hParentItem);

		strXPath.Format("/FlashWizard/context [@index=\"%d\"]/action", i);
		if( xmlWizardDtlData.SetXPath(strXPath) == XMLPARSER_SUCCESS &&
			xmlWizardDtlData.GetSelCount()      > 0                  &&
		   !xmlWizardDtlData.GetAttribute("type").IsEmpty()          )
		{
//			CString strActionType = xmlWizardDtlData.GetAttribute("type");
			CString strActionId   = xmlWizardDtlData.GetAttribute("id");
//			CString strActionName = xmlWizardDtlData.GetAttribute("name");

			CONTENTS_INFO* pChildInfo = pDoc->GetContents(strActionId);

			// IsFitChildNode에서 Parent노드의 GetItemData로 판단하므로 미리 넣어둘것.
			m_treeWizardHier.SetItemData(hContentsItem, (DWORD_PTR)pChildInfo);

			if(IsFitChildNode(hContentsItem, (DWORD_PTR)pChildInfo))
			{
				pChildInfo = InsertChildNode(hContentsItem, strActionId);

				if(pChildInfo)
				{
					strText += _T(" : ") + pChildInfo->strContentsName;
					m_treeWizardHier.SetItemText(hContentsItem, strText);
					//m_treeWizardHier.SetItemState(hContentsItem, TVIS_OVERLAYMASK, INDEXTOOVERLAYMASK(LINK_IMG_IDX));
				}
				else
				{
					//m_treeWizardHier.SetItemState(hContentsItem, TVIS_OVERLAYMASK, INDEXTOOVERLAYMASK(UNLINK_IMG_IDX));
				}
			}
			else
			{
				if(pChildInfo)
				{
					strText += _T(" : ") + pChildInfo->strContentsName;
					m_treeWizardHier.SetItemText(hContentsItem, strText);
					m_treeWizardHier.SetItemData(hContentsItem, (DWORD_PTR)pChildInfo);
				}

				m_treeWizardHier.SetItemState(hContentsItem, TVIS_OVERLAYMASK, INDEXTOOVERLAYMASK(REPEAT_IMG_IDX));
			}
			
			// ItemData로 링크 여부를 판단하므로 child노드를 넣은 후 값을 다시 설정할 것.
			m_treeWizardHier.SetItemData(hContentsItem, (DWORD_PTR)pChildInfo);
		}
		else
		{
			m_treeWizardHier.SetItemState(hContentsItem, TVIS_OVERLAYMASK, INDEXTOOVERLAYMASK(MINUS_IMG_IDX));
		}

		//m_treeWizardHier.Expand(hContentsItem, TVE_EXPAND);
	}

	return pContentsInfo;
}

HTREEITEM CWizardTreeDlg::FindChild(HTREEITEM hParent, DWORD_PTR pFindData )
{
	HTREEITEM hChild = m_treeWizardHier.GetNextItem( hParent, TVGN_CHILD );

	while( NULL != hChild )
	{
		if( m_treeWizardHier.GetItemData( hChild ) == pFindData) return hChild;

		HTREEITEM hSubChild = FindChild( hChild, pFindData );
		if( hSubChild ) return hSubChild;

		hChild = m_treeWizardHier.GetNextItem( hChild, TVGN_NEXT );
	}

	return NULL;
}

void CWizardTreeDlg::OnBnClickedButtonMainWizard()
{
	PlayPreview(m_pMainWizardContentsInfo);
}

void CWizardTreeDlg::OnBnClickedButtonPreviewPlay()
{
	POSITION pos = m_lcWizardList.GetFirstSelectedItemPosition();
	if(!pos) return;

	int nItem = m_lcWizardList.GetNextSelectedItem(pos);
	CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcWizardList.GetItemData(nItem);

	PlayPreview(pContentsInfo);
}

void CWizardTreeDlg::PlayPreview(CONTENTS_INFO* pContentsInfo)
{
	if(!pContentsInfo) return;

	m_wndWizard.CloseFile();

	m_wndWizard.m_contentsId = pContentsInfo->strId;
	m_wndWizard.m_contentsName = pContentsInfo->strContentsName;
	m_wndWizard.m_comment[0] = pContentsInfo->strComment[0];
	m_wndWizard.m_strMediaFullPath.Format("%s\\%sFlashWizard\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, pContentsInfo->strComment[0]);
	//m_wndWizard.m_wizardXML = pContentsInfo->strWizardXML;
	m_wndWizard.m_wizardXML = GetFullWizardXML();
	m_wndWizard.m_runningTime = pContentsInfo->nRunningTime;
	m_wndWizard.OpenFile(0);
	m_wndWizard.Play();
}

CString CWizardTreeDlg::GetFullWizardXML()
{
	CString strFullWizardXML = _T("<?xml version=\"1.0\"?><wizard>");

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return _T("");

	CONTENTS_INFO_MAP* contents_map = pDoc->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_WIZARD) continue;
		if(pContentsInfo->strWizardXML.IsEmpty()) continue;

		CString strWizardXML = pContentsInfo->strWizardXML.Mid(pContentsInfo->strWizardXML.Find(_T("<FlashWizard")));		strWizardXML.Replace("xml:space=\"preserve\"","");
		strWizardXML.Replace("xml:space=\"preserve\"","");
		strWizardXML.Replace("space=\"preserve\"","");

		strFullWizardXML += strWizardXML;
	}

	strFullWizardXML += _T("</wizard>");

	return strFullWizardXML;
}

CONTENTS_INFO* CWizardTreeDlg::GetTreeItemInfo(HTREEITEM hItem, int& nContentsIndex)
{
	nContentsIndex = 0;
	if(hItem == NULL) return NULL;

	HTREEITEM hWizardItem = m_treeWizardHier.GetParentItem(hItem);
	if(hWizardItem == NULL) return NULL;

	if(m_treeWizardHier.GetItemData(hWizardItem) == NULL) return NULL;

	HTREEITEM hSiblingItem = hItem;
	while( (hSiblingItem = m_treeWizardHier.GetPrevSiblingItem(hSiblingItem)) != NULL)
	{
		nContentsIndex++;
	}

	return (CONTENTS_INFO*)m_treeWizardHier.GetItemData(hWizardItem);
}

void CWizardTreeDlg::SetWizardAction(HTREEITEM hItem, CONTENTS_INFO* pChildContentsInfo)
{
	if(hItem == NULL) return;

	int nIndex = 0;
	CONTENTS_INFO* pParentContentsInfo = GetTreeItemInfo(hItem, nIndex);
	if(!pParentContentsInfo) return;

	CXmlParser xmlWizardData;
	if(pParentContentsInfo->strWizardXML.IsEmpty()) return;
	if(xmlWizardData.LoadXML(pParentContentsInfo->strWizardXML) != XMLPARSER_SUCCESS) return;

	CString strChildContentsId;
	CString strChildContentsName;
	if(pChildContentsInfo)
	{
		CXmlParser xmlChildData;
		if(!pChildContentsInfo->strWizardXML.IsEmpty() && 
			xmlChildData.LoadXML(pChildContentsInfo->strWizardXML) == XMLPARSER_SUCCESS)
		{
			if(xmlChildData.SetXPath("/FlashWizard") == XMLPARSER_SUCCESS)
			{
				strChildContentsName = xmlChildData.GetAttribute("name");
			}
		}

		strChildContentsId = pChildContentsInfo->strId;
	}

	CString strXPath;
	strXPath.Format("/FlashWizard/context [@index=\"%d\"]/action", nIndex);
	if(xmlWizardData.SetXPath(strXPath) != XMLPARSER_SUCCESS) return;
	if(xmlWizardData.GetSelCount() <= 0) return;
	if(xmlWizardData.GetAttribute("type").IsEmpty()) return;

	xmlWizardData.SetAttribute("id", strChildContentsId);
	xmlWizardData.SetAttribute("name", strChildContentsName);

	// xml 저장
	CString strXml = xmlWizardData.GetXML("/");
	strXml.Replace("\t","");
	strXml.Replace("\r","");
	strXml.Replace("\n","");
	strXml.Replace("xml:space=\"preserve\"","");
	strXml.Replace("space=\"preserve\"","");
	pParentContentsInfo->strWizardXML = strXml;
}

void CWizardTreeDlg::OnNMClickListWizardList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	BOOL bLinkEnable = FALSE;

	CONTENTS_INFO* pContentsInfo = NULL;
	if(pNMLV->iItem >= 0 && pNMLV->iItem < m_lcWizardList.GetItemCount())
	{
		pContentsInfo = (CONTENTS_INFO*)m_lcWizardList.GetItemData(pNMLV->iItem);
	}

	if(pContentsInfo && m_treeWizardHier.GetSelectedItem())
	{
		UINT nState = m_treeWizardHier.GetItemState(m_treeWizardHier.GetSelectedItem(), TVIS_OVERLAYMASK);
	
		if(nState != INDEXTOOVERLAYMASK(MINUS_IMG_IDX) && IsFitChildNode(m_treeWizardHier.GetSelectedItem(), (DWORD_PTR)pContentsInfo))
		{
			bLinkEnable = TRUE;
		}
	}

	m_bnLink.EnableWindow(bLinkEnable);
}

void CWizardTreeDlg::OnTvnSelchangingTreeWizard(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	*pResult = 0;

	BOOL bLinkEnable = FALSE;

	CONTENTS_INFO* pContentsInfo = NULL;
	POSITION pos = m_lcWizardList.GetFirstSelectedItemPosition();
	if(pos)
	{
		int nItem = m_lcWizardList.GetNextSelectedItem(pos);
		pContentsInfo = (CONTENTS_INFO*)m_lcWizardList.GetItemData(nItem);
	}

	UINT nState = m_treeWizardHier.GetItemState(pNMTreeView->itemNew.hItem, TVIS_OVERLAYMASK);

	if((nState & INDEXTOOVERLAYMASK(MINUS_IMG_IDX)) == 0 && pContentsInfo && IsFitChildNode(pNMTreeView->itemNew.hItem, (DWORD_PTR)pContentsInfo))
	{
		bLinkEnable = TRUE;
	}

	m_bnLink.EnableWindow(bLinkEnable);
	m_bnUnlink.EnableWindow( (nState & INDEXTOOVERLAYMASK(REPEAT_IMG_IDX)) != 0 || m_treeWizardHier.ItemHasChildren(pNMTreeView->itemNew.hItem));
}

void CWizardTreeDlg::OnNMDblclkListWizardList(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;
	
	if(m_bnLink.IsWindowEnabled())
	{
		OnBnClickedButtonLink();
	}
}

void CWizardTreeDlg::OnBnClickedButtonLink()
{
	HTREEITEM hTargetItem = m_treeWizardHier.GetSelectedItem();
	if(hTargetItem == NULL) return;
	if(m_treeWizardHier.GetParentItem(hTargetItem) == NULL) return;

	int nCnt = m_lcWizardList.GetSelectedCount();
	if (nCnt < 1) return;
	int nSelIndex = m_lcWizardList.GetNextItem(-1, LVIS_SELECTED);

	CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcWizardList.GetItemData(nSelIndex);

	SetWizardAction(hTargetItem, pContentsInfo);

	RefreshTree(m_strRefreshContentsId);

	HTREEITEM hWizardItem = FindChild(NULL, (DWORD_PTR)pContentsInfo);
	if(hWizardItem)
	{
		m_treeWizardHier.SelectItem(hWizardItem);
		m_treeWizardHier.Expand(hWizardItem, TVE_EXPAND);
		m_treeWizardHier.EnsureVisible(hWizardItem);
	}
}

void CWizardTreeDlg::OnNMDblclkTreeWizard(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	if(m_bnUnlink.IsWindowEnabled())
	{
		OnBnClickedButtonUnlink();
	}
}

void CWizardTreeDlg::OnBnClickedButtonUnlink()
{
	HTREEITEM hSrcItem = m_treeWizardHier.GetSelectedItem();
	if(hSrcItem == NULL) return;

	if(m_treeWizardHier.GetParentItem(hSrcItem) == NULL) return;

	CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_treeWizardHier.GetItemData(hSrcItem);

	SetWizardAction(hSrcItem, NULL);

	RefreshTree(m_strRefreshContentsId);

	HTREEITEM hWizardItem = FindChild(NULL, (DWORD_PTR)pContentsInfo);
	if(hWizardItem)
	{
		m_treeWizardHier.SelectItem(hWizardItem);
		m_treeWizardHier.Expand(hWizardItem, TVE_EXPAND);
		m_treeWizardHier.EnsureVisible(hWizardItem);
	}
}

BOOL CWizardTreeDlg::IsFitChildNode(HTREEITEM hSrcItem, HTREEITEM hChildItem)
{
	if(hSrcItem == NULL || hChildItem == NULL) return TRUE;

	DWORD_PTR pChildData = m_treeWizardHier.GetItemData(hChildItem);

	BOOL bFindIt = FALSE;
	HTREEITEM hParentItem = hSrcItem;
	while( (hParentItem = m_treeWizardHier.GetParentItem(hParentItem)) != NULL)
	{
		DWORD_PTR pItemData = m_treeWizardHier.GetItemData(hParentItem);
		bFindIt = (pItemData == pChildData);
		if(bFindIt) break;

		bFindIt = (FindChild(hChildItem, pItemData) != NULL);
		if(bFindIt) break;
	}

	return !bFindIt;
}

BOOL CWizardTreeDlg::IsFitChildNode(HTREEITEM hSrcItem, DWORD_PTR pInItemData)
{
	if(hSrcItem == NULL || pInItemData == NULL) return TRUE;

	BOOL bFindIt = FALSE;
	HTREEITEM hParentItem = hSrcItem;
	while( (hParentItem = m_treeWizardHier.GetParentItem(hParentItem)) != NULL)
	{
		DWORD_PTR pItemData = m_treeWizardHier.GetItemData(hParentItem);
		bFindIt = (pItemData == pInItemData);
		if(bFindIt) break;
	}

	return !bFindIt;
}

int CWizardTreeDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}

void CWizardTreeDlg::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);

	m_Reposition.AddControl(&m_gbGroupMainWizard, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_Reposition.AddControl(&m_bnMainWizard, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);

	m_Reposition.AddControl(&m_gbGroupWizard, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_MOVE);
	m_Reposition.AddControl(&m_lcWizardList, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_MOVE);

	m_Reposition.AddControl(&m_gbGroupPreview, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_Reposition.AddControl(&m_staticContents, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_Reposition.AddControl(&m_wndWizard, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_Reposition.AddControl(&m_bnPreview, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);

	m_Reposition.AddControl(&m_bnLink, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);

	m_Reposition.AddControl(&m_gbGroupWizardTree, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_bnExpandTree, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_Reposition.AddControl(&m_bnUnlink, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_Reposition.AddControl(&m_treeWizardHier, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_Reposition.AddControl(&m_bnOK, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
}

void CWizardTreeDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CWizardTreeDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	CDialog::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMinTrackSize.x = 500;
	lpMMI->ptMinTrackSize.y = 500;
}

void CWizardTreeDlg::OnBnClickedButtonExpand()
{
	ExpandAllChild(NULL, TVE_EXPAND);
	m_treeWizardHier.Expand(NULL, TVE_EXPAND);
}

void CWizardTreeDlg::ExpandAllChild(HTREEITEM hParent, UINT nCode)
{
	HTREEITEM hChild = m_treeWizardHier.GetNextItem( hParent, TVGN_CHILD );

	while( NULL != hChild )
	{
		ExpandAllChild( hChild, nCode );
		m_treeWizardHier.Expand( hChild, nCode );
		hChild = m_treeWizardHier.GetNextItem( hChild, TVGN_NEXT );
	}
}
