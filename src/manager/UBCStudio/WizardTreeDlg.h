#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "resource.h"
#include "UBCStudioDoc.h"
#include "Schedule.h"
#include "common/libXmlParser.h"
#include "common/HoverButton.h"
#include "ReposControl.h"

// CWizardTreeDlg 대화 상자입니다.

class CWizardTreeDlg : public CDialog
{
	DECLARE_DYNAMIC(CWizardTreeDlg)

	CUBCStudioDoc*	m_pDocument;
	CString			m_strInContentsId;

public:
	CWizardTreeDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CWizardTreeDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_WIZARD_TREE_VIEW };

	void				SetDocument(CUBCStudioDoc* pDoc) { m_pDocument=pDoc; }
	CUBCStudioDoc*		GetDocument() { return m_pDocument; }

	void				SetContentsId(CString strContentsId) { m_strInContentsId = strContentsId; }

protected:

	CImageList			m_ilWizardImage;
	CListCtrl			m_lcWizardList;
	CTreeCtrl			m_treeWizardHier;
	CFont				m_fontBold;
	CString				m_strRefreshContentsId;

	void				RefreshList();
	void				RefreshTree(CString strContentsId);
	CONTENTS_INFO*		InsertChildNode(HTREEITEM hParentItem, CString strContentsId);
	HTREEITEM			FindChild(HTREEITEM hParent, DWORD_PTR pFindData);
	void				ExpandAllChild(HTREEITEM hParent, UINT nCode);
	CONTENTS_INFO*		GetTreeItemInfo(HTREEITEM hItem, int& nContentsIndex);
	void				SetWizardAction(HTREEITEM hItem, CONTENTS_INFO* pChildContentsInfo);

	BOOL				IsFitChildNode(HTREEITEM hSrcItem, HTREEITEM hChildItem);
	BOOL				IsFitChildNode(HTREEITEM hSrcItem, DWORD_PTR pInItemData);

	CImageList*			m_pDragImage;		// 드래그시 생성된 이미지 사용
	HTREEITEM			m_hDragItem;		// 드래그시 처음 선택된 아이템 핸들 기억용
	CONTENTS_INFO*		m_pDragContentsInfo;

	CHoverButton		m_bnExpandTree;
	CHoverButton		m_bnLink;
	CHoverButton		m_bnUnlink;
	CHoverButton		m_bnPreview;
	CHoverButton		m_bnOK;

	CONTENTS_INFO*		m_pMainWizardContentsInfo;
	CButton				m_bnMainWizard;
	CStatic				m_gbGroupMainWizard;
	CStatic				m_gbGroupWizard;
	CStatic				m_gbGroupPreview;
	CStatic				m_staticContents;
	CStatic				m_gbGroupWizardTree;

	CWizardSchedule		m_wndWizard;
	void				PlayPreview(CONTENTS_INFO* pContentsInfo);
	CString				GetFullWizardXML();

	void				InitPosition(CRect rc);
	CRect				m_rcClient;
	CReposControl		m_Reposition;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnNMCustomdrawTreeWizard(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTvnBegindragTreeWizard(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonPreviewPlay();
	afx_msg void OnTvnSelchangingTreeWizard(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonLink();
	afx_msg void OnBnClickedButtonUnlink();
	afx_msg void OnLvnBegindragListWizardList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListWizardList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListWizardList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkTreeWizard(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBnClickedButtonExpand();
	afx_msg void OnBnClickedButtonMainWizard();
};
