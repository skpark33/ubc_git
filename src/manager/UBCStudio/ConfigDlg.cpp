// ConfigDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "ConfigDlg.h"
#include "Enviroment.h"


// CConfigDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CConfigDlg, CDialog)

CConfigDlg::CConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDlg::IDD, pParent)
{

}

CConfigDlg::~CConfigDlg()
{
}

void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_KB_TEMP_SAVE, m_kbAutoTempSave);
	DDX_Control(pDX, IDC_KB_USE_TIMEBASE_PLAYCONTENTS, m_kbUseTimeBasePlayContents);
	DDX_Control(pDX, IDC_KB_USE_CLICK_PLAYCONTENTS, m_kbUseClickPlayContents);
	DDX_Control(pDX, IDC_EB_TEMP_SAVE_INTERVAL, m_ebTempSaveInterval);
	DDX_Control(pDX, IDC_SP_TEMP_SAVE_INTERVAL, m_spTempSaveInterval);
	DDX_Control(pDX, IDC_KB_USE_TIMECHECK, m_kbUseTimeCheck);
	DDX_Control(pDX, IDC_KB_AUTO_ENCODING, m_kbAutoEncoding);
}


BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CConfigDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CConfigDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_KB_TEMP_SAVE, &CConfigDlg::OnBnClickedKbTempSave)
END_MESSAGE_MAP()


// CConfigDlg 메시지 처리기입니다.

void CConfigDlg::OnBnClickedOk()
{
	if(m_kbAutoTempSave.GetCheck() && m_ebTempSaveInterval.GetValueInt() <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONFIGDLG_MSG001));
		return;
	}

	GetEnvPtr()->m_bUseAutoTempSave = m_kbAutoTempSave.GetCheck();
	GetEnvPtr()->m_nAutoTempSaveInterval = m_ebTempSaveInterval.GetValueInt();
	GetEnvPtr()->m_bUseTimeBasePlayContents = m_kbUseTimeBasePlayContents.GetCheck();
	GetEnvPtr()->m_bAutoEncoding = m_kbAutoEncoding.GetCheck();
	GetEnvPtr()->m_bUseClickPlayContents = m_kbUseClickPlayContents.GetCheck();
	GetEnvPtr()->m_bUseTimeCheck = m_kbUseTimeCheck.GetCheck(); // skpark same_size_file_problem

	char buf[1024];
	CString szPath, szTemp;
	szPath.Format(_T("%s%sdata\\%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, UBCVARS_INI);

	::WritePrivateProfileString(_T("ROOT"), _T("USE_TIME_BASE_SCHEDULE"), (GetEnvPtr()->m_bUseTimeBasePlayContents ? _T("1") : _T("0")), szPath);
	::WritePrivateProfileString(_T("ROOT"), _T("USE_CLICK_SCHEDULE"), (GetEnvPtr()->m_bUseClickPlayContents ? _T("1") : _T("0")), szPath);
	::WritePrivateProfileString(_T("ROOT"), _T("USE_AUTO_TEMP_SAVE"), (GetEnvPtr()->m_bUseAutoTempSave ? _T("1") : _T("0")), szPath);
	::WritePrivateProfileString(_T("ROOT"), _T("AUTO_TEMP_SAVE_INTERVAL"), ToString(GetEnvPtr()->m_nAutoTempSaveInterval), szPath);
	::WritePrivateProfileString(_T("ROOT"), _T("USE_TIME_CHECK"), (GetEnvPtr()->m_bUseTimeCheck ? _T("1") : _T("0")), szPath);// skpark same_size_file_problem
	::WritePrivateProfileString(_T("ROOT"), _T("AUTO_ENCODING"), (GetEnvPtr()->m_bAutoEncoding ? _T("1") : _T("0")), szPath);// skpark 2016.02.26 AutoEncoding

	OnOK();
}

void CConfigDlg::OnBnClickedCancel()
{
	OnCancel();
}

BOOL CConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_kbAutoTempSave.SetCheck(GetEnvPtr()->m_bUseAutoTempSave);
	m_ebTempSaveInterval.SetValue(GetEnvPtr()->m_nAutoTempSaveInterval);
	m_spTempSaveInterval.SetPos32(GetEnvPtr()->m_nAutoTempSaveInterval);
	m_spTempSaveInterval.SetRange32(1,60);

	m_ebTempSaveInterval.EnableWindow(m_kbAutoTempSave.GetCheck());
	m_spTempSaveInterval.EnableWindow(m_kbAutoTempSave.GetCheck());

	m_kbUseTimeBasePlayContents.SetCheck(GetEnvPtr()->m_bUseTimeBasePlayContents);
	m_kbAutoEncoding.SetCheck(GetEnvPtr()->m_bAutoEncoding);

	m_kbUseClickPlayContents.SetCheck(GetEnvPtr()->m_bUseClickPlayContents);

	m_kbUseTimeCheck.SetCheck(GetEnvPtr()->m_bUseTimeCheck); //skpark same_size_file_problem

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CConfigDlg::OnBnClickedKbTempSave()
{
	m_ebTempSaveInterval.EnableWindow(m_kbAutoTempSave.GetCheck());
	m_spTempSaveInterval.EnableWindow(m_kbAutoTempSave.GetCheck());
}
