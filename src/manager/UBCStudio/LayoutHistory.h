#pragma once
#include "DataContainer.h"

class CLayoutHistory
{
protected:
	struct SInfo{
		FRAME_LINK*		pFrame;
		TEMPLATE_LINK*	pTemplate;
		SInfo(){
			pFrame = NULL;
			pTemplate = NULL;
		}
	};

	enum EStat{
		eStart, eMiddle, eEnd
	};
public:
	CLayoutHistory* GetObject();
	void Release();

	void Add(FRAME_LINK*, TEMPLATE_LINK*);
	bool GetPrev(FRAME_LINK*, TEMPLATE_LINK*);
	bool GetNext(FRAME_LINK*, TEMPLATE_LINK*);
	EStat GetStat();
private:
	CLayoutHistory();
	~CLayoutHistory();
public:
private:
	EStat m_PosStat;
	POSITION m_CurPos;
	CList<SInfo*> m_listInfo;
	
	static CLayoutHistory* m_pLayoutHistory;
};