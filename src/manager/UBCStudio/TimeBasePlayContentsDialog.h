#pragma once

#include "afxwin.h"
#include "afxdtctl.h"

#include "SubPlayContentsDialog.h"

// CTimeBasePlayContentsDialog 대화 상자입니다.

class CTimeBasePlayContentsDialog : public CSubPlayContentsDialog
{
	DECLARE_DYNAMIC(CTimeBasePlayContentsDialog)

public:
	CTimeBasePlayContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTimeBasePlayContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TIME_BASE_PLAYCONTENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	PLAYCONTENTS_INFO*	m_pPlayContentsInfo;

public:

	virtual bool	GetPlayContentsInfo(PLAYCONTENTS_INFO& info, int nIndex);
	virtual bool	SetPlayContentsInfo(PLAYCONTENTS_INFO& info);
	virtual int		GetPlayContentsCount();
	virtual bool    InputErrorCheck();

	afx_msg void OnSize(UINT nType, int cx, int cy);

	CDateTimeCtrl m_dateStart;
	CDateTimeCtrl m_timeStart;

	CDateTimeCtrl m_dateEnd;
	CDateTimeCtrl m_timeEnd;

	void	SetRunningTime(ULONG ulRunningTime);		///<콘텐츠의 running time을 설정하여 playcontents의 종료 시간을 설정한다
};
