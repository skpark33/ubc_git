// ImageContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "ImageContentsDialog.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "common/MD5Util.h"

// CImageContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CImageContentsDialog, CSubContentsDialog)

CImageContentsDialog::CImageContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CImageContentsDialog::IDD, pParent)
	, m_wndImage (NULL, 0, false)
	, m_reposControl(this)
	, m_strLocation("")
	, m_bPreviewMode(false)
	, m_bPermanent(FALSE)
{
}

CImageContentsDialog::~CImageContentsDialog()
{
}

void CImageContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_WIDTH, m_staticContentsWidth);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_HEIGHT, m_staticContentsHeight);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_FILE_SIZE, m_staticContentsFileSize);
	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_groupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_BUTTON_FULL_SCREEN, m_btnFullScreen);
	DDX_Control(pDX, IDC_BUTTON_FIT_TO_WINDOW, m_btnFitToWindow);
	DDX_Control(pDX, IDC_BUTTON_ORIGINAL, m_btnOriginal);
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	DDX_Control(pDX, IDC_BUTTON_BROWSER_CONTENTS_FILE, m_btnBrowseContentsFile);
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);
	DDX_Control(pDX, IDC_EDIT_FELICA_URL, m_editFelicaUrl);
}


BEGIN_MESSAGE_MAP(CImageContentsDialog, CSubContentsDialog)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_CONTENTS_FILE, &CImageContentsDialog::OnBnClickedButtonBrowserContentsFile)
	ON_BN_CLICKED(IDC_BUTTON_ORIGINAL, &CImageContentsDialog::OnBnClickedButtonOriginal)
	ON_BN_CLICKED(IDC_BUTTON_FIT_TO_WINDOW, &CImageContentsDialog::OnBnClickedButtonFitToWindow)
	ON_BN_CLICKED(IDC_BUTTON_FULL_SCREEN, &CImageContentsDialog::OnBnClickedButtonFullScreen)
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, &CImageContentsDialog::OnBnClickedPermanentCheck)
END_MESSAGE_MAP()


// CImageContentsDialog 메시지 처리기입니다.

BOOL CImageContentsDialog::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnFullScreen.LoadBitmap(IDB_BTN_FULL, RGB(255, 255, 255));
	m_btnFitToWindow.LoadBitmap(IDB_BTN_FIT, RGB(255, 255, 255));
	m_btnOriginal.LoadBitmap(IDB_BTN_1_1, RGB(255, 255, 255));
	m_btnBrowseContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));

	m_btnFullScreen.SetToolTipText(LoadStringById(IDS_IMAGECONTENTSDIALOG_BUT001));
	m_btnFitToWindow.SetToolTipText(LoadStringById(IDS_IMAGECONTENTSDIALOG_BUT002));
	m_btnOriginal.SetToolTipText(LoadStringById(IDS_IMAGECONTENTSDIALOG_BUT003));
	m_btnBrowseContentsFile.SetToolTipText(LoadStringById(IDS_IMAGECONTENTSDIALOG_BUT004));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(0);

	// 창일향 : 폰트(System), 폰트크기(36), 배경색(Medium Blue), 전경색(White), 속도(중간)으로 고정하고 변경할 수 없도록 한다.
	//if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
	//{
	//	m_editContentsPlaySecond.SetValue(1);

	//	GetDlgItem(IDC_STATIC_RUNNING_TIME)->ShowWindow(FALSE);
	//	GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND)->SetWindowText(FALSE);

	//	GetDlgItem(IDC_STATIC_RUNNING_TIME2)->ShowWindow(TRUE);
	//	GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND2)->ShowWindow(TRUE);

	//	m_editContentsPlayMinute.EnableWindow(FALSE);
	//	GetDlgItem(IDC_STATIC_CONTENTS_PLAY_MINUTE)->EnableWindow(FALSE);
	//}
	//else
	{
		GetDlgItem(IDC_STATIC_RUNNING_TIME)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND)->ShowWindow(TRUE);

		GetDlgItem(IDC_STATIC_RUNNING_TIME2)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND2)->ShowWindow(FALSE);
	}

	//
	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);
	client_rect.DeflateRect(1,1);

	m_wndImage.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndImage.ShowWindow(SW_SHOW);

	m_reposControl.AddControl(&m_groupPreview, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_staticContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_wndImage, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.AddControl(&m_btnFullScreen, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnFitToWindow, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnOriginal, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);

	//
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsWidth);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsHeight);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsFileSize);

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}//if

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CImageContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CSubContentsDialog::OnOK();
}

void CImageContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CSubContentsDialog::OnCancel();
	GetParent()->PostMessage(WM_CLOSE);
}

void CImageContentsDialog::Stop()
{
	m_wndImage.Stop(false);
}

bool CImageContentsDialog::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData(TRUE);

	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	//m_editContentsFileName.GetWindowText(info.strLocation);
	info.strLocalLocation = m_strLocation;
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();

	//
	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(info.strLocalLocation, dir, path, filename, ext);

	info.strLocalLocation.Format("%s%s", dir, path); 
	info.strFilename.Format("%s%s", filename, ext); 
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	//
	if(!IsFitContentsName(info.strContentsName)) return false;
	if( info.strLocalLocation.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG002), MB_ICONSTOP);
		return false;
	}
	if( info.strFilename.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG003), MB_ICONSTOP);
		return false;
	}
	if(info.nRunningTime == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
		return false;
	}

	m_editFelicaUrl.GetWindowText(info.strComment[1]);
	info.strComment[1].Trim();
	if(!info.strComment[1].IsEmpty())
	{	
		CString strFelicaUrl = info.strComment[1];
		strFelicaUrl.MakeLower();
		if(strFelicaUrl.Find(_T("http://")) < 0)
		{
			UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG007), MB_ICONSTOP);
			return false;
		}
	}

	return true;
}

bool CImageContentsDialog::SetContentsInfo(CONTENTS_INFO& info)
{
	m_editContentsName.SetWindowText(info.strContentsName);
	//m_editContentsFileName.SetWindowText(info.strLocation + info.strFilename);
	m_editContentsFileName.SetWindowText(info.strFilename);
	m_strLocation = info.strLocalLocation + info.strFilename;
	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	m_editFelicaUrl.SetWindowText(info.strComment[1]);

	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}//if

	CString strFileName = info.strFilename;
	strFileName.MakeLower();
	m_btnBrowseContentsFile.EnableWindow(strFileName != _T("dummy.jpg"));

	UpdateData(FALSE);

	LoadContents(info.strLocalLocation + info.strFilename);

	return true;
}

void CImageContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CSubContentsDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();
}

void CImageContentsDialog::OnBnClickedButtonBrowserContentsFile()
{
	CString szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
	CString filter;

	// 창일향 : 콘텐츠는 오직 JPG 파일 뿐이다
	if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
	{
		szFileExts = "*.jpg; *.jpeg;";
		filter.Format("JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;||"
					 , szFileExts
					 );
	}
	else
	{
		szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
		filter.Format("All Image Files|%s|"
					  "Bitmap Files (*.bmp)|*.bmp|"
					  "JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;|"
					  "GIF Files (*.gif)|*.gif|"
					  "PCX Files (*.pcx)|*.pcx|"
					  "PNG Files (*.png)|*.png|"
					  "TIFF Files (*.tif)|*.tif;*.tiff||"
					 , szFileExts
					 );
	}

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, CONTENTS_IMAGE))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!CheckTotalContentsSize(dlg.GetPathName())) return;

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	//m_editContentsFileName.SetWindowText(dlg.GetPathName());
	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = dlg.GetPathName();
	m_editContentsFileName.SetSel(0, -1);

	m_wndImage.CloseFile();

	LoadContents(dlg.GetPathName());

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);
	if(contents_name.GetLength() == 0)
	{
		char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
		_splitpath(dlg.GetPathName(), drive, path, filename, ext);

		m_editContentsName.SetWindowText(filename);
	}

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(m_ulFileSize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
	}
	m_strFileMD5 = CString(szMd5);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CImageContentsDialog::OnBnClickedButtonOriginal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//m_btnFullScreen;
	//m_btnFitToWindow;
	//m_btnOriginal;

	m_wndImage.m_nType = 1;
	m_wndImage.Invalidate(FALSE);
}

void CImageContentsDialog::OnBnClickedButtonFitToWindow()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//m_btnFullScreen;
	//m_btnFitToWindow;
	//m_btnOriginal;

	m_wndImage.m_nType = 2;
	m_wndImage.Invalidate();
}

void CImageContentsDialog::OnBnClickedButtonFullScreen()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//m_btnFullScreen;
	//m_btnFitToWindow;
	//m_btnOriginal;

	m_wndImage.m_nType = 0;
	m_wndImage.Invalidate();
}

bool CImageContentsDialog::LoadContents(LPCTSTR lpszFullPath)
{
	CFileStatus fs;
//	if(CFile::GetStatus(lpszFullPath, fs))
	if(CEnviroment::GetFileSize(lpszFullPath, fs.m_size))
	{
		m_wndImage.m_strMediaFullPath = lpszFullPath;
		if(m_wndImage.OpenFile(1))
		{
			int width;
			int height;

			if(m_wndImage.m_bAnimatedGif)
			{
				width = m_wndImage.m_bgGIF.GetSize().cx;
				height = m_wndImage.m_bgGIF.GetSize().cy;
			}
			else
			{
				width = m_wndImage.m_bgImage.GetWidth();
				height = m_wndImage.m_bgImage.GetHeight();
			}

			m_staticContentsWidth.SetWindowText(::ToString(width) + " ");
			m_staticContentsHeight.SetWindowText(::ToString(height) + " ");
			CString strMinute, strSecond;
			m_editContentsPlayMinute.GetWindowText(strMinute);
			m_editContentsPlaySecond.GetWindowText(strSecond);
			if(strMinute == "0" && strSecond == "0")
			{
				m_editContentsPlaySecond.SetWindowText("15");
			}//if

			m_staticContentsFileSize.SetWindowText(::ToMoneyTypeString((ULONGLONG)(fs.m_size/1024)) + " ");
			m_ulFileSize = fs.m_size;

			m_wndImage.Play();

			m_wndImage.Invalidate();
		}
	
		return true;
	}

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨트롤의 사용가능여부 설정 \n
/// @param (BOOL) bEnable : (in) TRUE : 사용가능, FALSE : 사용 불가
/////////////////////////////////////////////////////////////////////////////////
void CImageContentsDialog::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editContentsFileName.EnableWindow(bEnable);
	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	m_btnBrowseContentsFile.EnableWindow(bEnable);
	CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
	pBtn->EnableWindow(FALSE);
	m_editFelicaUrl.EnableWindow(bEnable);
}

void CImageContentsDialog::OnBnClickedPermanentCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//24시간을 설정
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		// 창일향 : 폰트(System), 폰트크기(36), 배경색(Medium Blue), 전경색(White), 속도(중간)으로 고정하고 변경할 수 없도록 한다.
		//if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
		//{
		//	m_editContentsPlayMinute.SetWindowText("");
		//	m_editContentsPlaySecond.SetWindowText("1");
		//	m_editContentsPlayMinute.EnableWindow(FALSE);
		//	m_editContentsPlaySecond.EnableWindow(TRUE);
		//}
		//else
		{
			m_editContentsPlayMinute.SetWindowText("");
			m_editContentsPlaySecond.SetWindowText("15");

			m_editContentsPlayMinute.EnableWindow(TRUE);
			m_editContentsPlaySecond.EnableWindow(TRUE);
		}
	}//if
}

BOOL CImageContentsDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}