#pragma once
#include "resource.h"

// CCycleIterationDlg 대화 상자입니다.

class CCycleIterationDlg : public CDialog
{
	DECLARE_DYNAMIC(CCycleIterationDlg)

public:
	CCycleIterationDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCycleIterationDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_CYCLE_ITERATION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CString m_strIterCount;
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();

	int	GetIterCount() { return atoi(m_strIterCount); }
};
