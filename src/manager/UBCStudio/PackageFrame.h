// PackageFrame.h : interface of the CPackageFrame class
//


#pragma once


class CPackageFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CPackageFrame)
public:
	CPackageFrame();

// Attributes
public:

// Operations
public:

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CPackageFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	afx_msg LRESULT OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFrameInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnContentsListChanged(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT	OnConfigSaveComplete(WPARAM wParam, LPARAM lParam);	///<complete config save message handler
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};
