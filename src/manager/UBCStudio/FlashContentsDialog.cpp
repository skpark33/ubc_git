// FlashContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "FlashContentsDialog.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "common/UbcGUID.h"
#include "common/UbcImageList.h"
#include <IMAGEHLP.H>
#pragma comment(lib, "imagehlp.lib")
#include "common/MD5Util.h"

#ifdef _UBCSTUDIO_EE_
#include "ubccopcommon\ftpmultisite.h"
#endif//_UBCSTUDIO_EE_

// CFlashContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFlashContentsDialog, CSubContentsDialog)

CFlashContentsDialog::CFlashContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CFlashContentsDialog::IDD, pParent)
	, m_wndFlash (NULL, 0, false)
	, m_reposControl (this)
	, m_strLocation("")
	, m_bPreviewMode(false)
	, m_bPermanent(FALSE)
{
	m_ulFileSize = 0;
	m_brushBG.CreateSolidBrush(RGB(255,255,255));
}

CFlashContentsDialog::~CFlashContentsDialog()
{
	DeleteSubFilesContents();
}

void CFlashContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_CONTENTS_FILE, m_btnBrowseContentsFile);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_FILE_SIZE, m_staticContentsFileSize);
	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_groupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);
	DDX_Control(pDX, IDC_EDIT_FELICA_URL, m_editFelicaUrl);
	DDX_Control(pDX, IDC_LIST_FILES, m_lcFiles);
	DDX_Control(pDX, IDC_TREE_FILES, m_treeFiles);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_ADD_FOLDER, m_btnBrowseAddFolder);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_DEL_FOLDER, m_btnBrowseDelFolder);
	DDX_Control(pDX, IDC_BUTTON_DOWNLOAD_FOLDER, m_btnDownloadFolder);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_ADD_FILE, m_btnBrowseAddFile);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_DEL_FILE, m_btnBrowseDelFile);
	DDX_Control(pDX, IDC_BUTTON_DOWNLOAD_CONTENTS, m_btnDownloadContents);
}


BEGIN_MESSAGE_MAP(CFlashContentsDialog, CSubContentsDialog)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_CONTENTS_FILE, &CFlashContentsDialog::OnBnClickedButtonBrowserContentsFile)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, &CFlashContentsDialog::OnBnClickedPermanentCheck)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_FILES, &CFlashContentsDialog::OnTvnSelchangedTreeFiles)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_ADD_FOLDER, &CFlashContentsDialog::OnBnClickedButtonBrowserAddFolder)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_DEL_FOLDER, &CFlashContentsDialog::OnBnClickedButtonBrowserDelFolder)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_ADD_FILE, &CFlashContentsDialog::OnBnClickedButtonBrowserAddFile)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_DEL_FILE, &CFlashContentsDialog::OnBnClickedButtonBrowserDelFile)
	ON_BN_CLICKED(IDC_BUTTON_DOWNLOAD_FOLDER, &CFlashContentsDialog::OnBnClickedButtonDownloadFolder)
	ON_BN_CLICKED(IDC_BUTTON_DOWNLOAD_CONTENTS, &CFlashContentsDialog::OnBnClickedButtonDownloadContents)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_FILES, &CFlashContentsDialog::OnNMDblclkListFiles)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_FILES, &CFlashContentsDialog::OnLvnKeydownListFiles)
	ON_NOTIFY(TVN_KEYDOWN, IDC_TREE_FILES, &CFlashContentsDialog::OnTvnKeydownTreeFiles)
END_MESSAGE_MAP()


// CFlashContentsDialog 메시지 처리기입니다.

BOOL CFlashContentsDialog::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnBrowseContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btnBrowseContentsFile.SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN001));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	m_btnBrowseAddFolder.LoadBitmap(IDB_BTN_PLUS , RGB(255, 255, 255));
	m_btnBrowseDelFolder.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnBrowseAddFile  .LoadBitmap(IDB_BTN_PLUS , RGB(255, 255, 255));
	m_btnBrowseDelFile  .LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));

	m_btnBrowseAddFolder.SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN002));
	m_btnBrowseDelFolder.SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN003));
	m_btnBrowseAddFile  .SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN004));
	m_btnBrowseDelFile  .SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN005));

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		m_btnDownloadFolder.LoadBitmap(IDB_CONT_DOWN, RGB(255, 255, 255));
		m_btnDownloadFolder.SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN006));

		m_btnDownloadContents.LoadBitmap(IDB_CONT_DOWN, RGB(255, 255, 255));
		m_btnDownloadContents.SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN007));
	}
	else
	{
		m_btnDownloadFolder.EnableWindow(FALSE);
		m_btnDownloadFolder.ShowWindow(SW_HIDE);

		m_btnDownloadContents.EnableWindow(FALSE);
		m_btnDownloadContents.ShowWindow(SW_HIDE);
	}

	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(0);

	//
	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);
	client_rect.DeflateRect(1,1);

	m_wndFlash.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndFlash.ShowWindow(SW_SHOW);

	m_reposControl.AddControl(&m_groupPreview, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_staticContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_wndFlash, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.Move();

	//
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsFileSize);

	InitSubFilesCtrl();

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}//if

	// 부속파일처리 관련 수정
	CONTENTS_INFO info;
	SetContentsInfo(info);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFlashContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnOK();
}

void CFlashContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnCancel();
	GetParent()->PostMessage(WM_CLOSE);
}

void CFlashContentsDialog::Stop()
{
	//m_wndFlash.Stop(false);
}

bool CFlashContentsDialog::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData(TRUE);

	// 부속파일처리 관련 수정
	info.strId = m_strContentsId;

	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	//m_editContentsFileName.GetWindowText(info.strLocation);
	info.strLocalLocation = m_strLocation;
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();

	//
	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(info.strLocalLocation, dir, path, filename, ext);

	info.strLocalLocation.Format("%s%s", dir, path); 
	info.strFilename.Format("%s%s", filename, ext);
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	//
	if(!IsFitContentsName(info.strContentsName)) return false;
	if( info.strLocalLocation.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG002), MB_ICONSTOP);
		return false;
	}
	if( info.strFilename.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG003), MB_ICONSTOP);
		return false;
	}
	if(info.nRunningTime == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
		return false;
	}

	m_editFelicaUrl.GetWindowText(info.strComment[1]);
	info.strComment[1].Trim();
	if(!info.strComment[1].IsEmpty())
	{
		CString strFelicaUrl = info.strComment[1];
		strFelicaUrl.MakeLower();
		if(strFelicaUrl.Find(_T("http://")) < 0)
		{
			UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG007), MB_ICONSTOP);
			return false;
		}
	}

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return false;

	CONTENTS_INFO_MAP* contents_map = pDoc->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;

		contents_map->RemoveKey(strFileKey);
		delete pContentsInfo;
	}

	pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		contents_map->SetAt(strFileKey, (void*&)pContentsInfo);

		// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
		// 서버에 등록된 컨텐츠인경우 파일이 변경되면 ID를 변경하도록 한다.
		if( m_bIsChangedFile &&
		   !pContentsInfo->strServerLocation.IsEmpty() &&
			pContentsInfo->strServerLocation.MakeLower().CompareNoCase("\\contents\\enc\\") < 0)
		{
			pContentsInfo->strId = (CUbcGUID::GetInstance()->ToGUID(_T("")));
		}
	}

	m_mapSubFilesContents.RemoveAll();

	return true;
}

bool CFlashContentsDialog::SetContentsInfo(CONTENTS_INFO& info)
{
	// 부속파일처리 관련 수정
	m_strContentsId = info.strId;
	if(m_strContentsId.IsEmpty()) m_strContentsId = CUbcGUID::GetInstance()->ToGUID(_T(""));

	m_editContentsName.SetWindowText(info.strContentsName);
	//m_editContentsFileName.SetWindowText(info.strLocation + info.strFilename);
	m_editContentsFileName.SetWindowText(info.strFilename);
	m_strLocation = info.strLocalLocation + info.strFilename;
	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	m_editFelicaUrl.SetWindowText(info.strComment[1]);

	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}//if

	CreateSubFilesContents();
	RefreshFilesTree();
	RefreshFilesList(_T(""));

	UpdateData(FALSE);

	LoadContents(info.strLocalLocation + info.strFilename);

	return true;
}

HBRUSH CFlashContentsDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CSubContentsDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if(	pWnd->GetSafeHwnd() != m_staticContentsFileSize.GetSafeHwnd())
	{
		pDC->SetBkColor(RGB(255,255,255));
		//pDC->SetBkMode(TRANSPARENT);

		hbr = (HBRUSH)m_brushBG;
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CFlashContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CSubContentsDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();

	if(m_wndFlash.GetSafeHwnd() && m_wndFlash.m_pFlash != NULL && m_wndFlash.m_pFlash->GetSafeHwnd() )
	{
		CRect rect;
		m_wndFlash.GetClientRect(rect);
		m_wndFlash.m_pFlash->MoveWindow(rect);
	}
}

void CFlashContentsDialog::OnBnClickedButtonBrowserContentsFile()
{
	CString filter;
	CString szFileExts = "*.swf";
	filter.Format("Flash Files (%s)|%s||",szFileExts,szFileExts);

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정
	//CFileDialog dlg(TRUE, _T(""), filename, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, filter, this);
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, CONTENTS_FLASH)){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!CheckTotalContentsSize(dlg.GetPathName())) return;

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	//m_editContentsFileName.SetWindowText(dlg.GetPathName());
	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = dlg.GetPathName();
	m_editContentsFileName.SetSel(0, -1);

	m_editContentsPlaySecond.SetWindowText("15");

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);
	if(contents_name.GetLength() == 0)
	{
		char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
		_splitpath(dlg.GetPathName(), drive, path, filename, ext);

		m_editContentsName.SetWindowText(filename);
	}

	LoadContents(dlg.GetPathName());

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(m_ulFileSize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
	}
	m_strFileMD5 = CString(szMd5);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

bool CFlashContentsDialog::LoadContents(LPCTSTR lpszFullPath)
{
	CFileStatus fs;
//	if(CFile::GetStatus(lpszFullPath, fs))
	if(CEnviroment::GetFileSize(lpszFullPath, fs.m_size))
	{
		m_ulFileSize = fs.m_size;
		m_staticContentsFileSize.SetWindowText(::ToMoneyTypeString((ULONGLONG)(fs.m_size/1024)) + " ");

		m_wndFlash.Stop(false);
		m_wndFlash.m_strMediaFullPath = lpszFullPath;
		m_wndFlash.OpenFile(1);
		m_wndFlash.Play();

		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨트롤의 사용가능여부 설정 \n
/// @param (BOOL) bEnable : (in) TRUE : 사용가능, FALSE : 사용 불가
/////////////////////////////////////////////////////////////////////////////////
void CFlashContentsDialog::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editContentsFileName.EnableWindow(bEnable);
	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	m_btnBrowseContentsFile.EnableWindow(bEnable);
	CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
	pBtn->EnableWindow(FALSE);
	m_editFelicaUrl.EnableWindow(bEnable);

	m_btnBrowseAddFolder.EnableWindow(bEnable);
	m_btnBrowseDelFolder.EnableWindow(bEnable);
//	m_btnDownloadFolder .EnableWindow(bEnable);
	m_btnBrowseAddFile  .EnableWindow(bEnable);
	m_btnBrowseDelFile  .EnableWindow(bEnable);
//	m_btnDownloadContents.EnableWindow(bEnable);
}

void CFlashContentsDialog::OnBnClickedPermanentCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//24시간을 설정
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText("");
		m_editContentsPlaySecond.SetWindowText("15");

		m_editContentsPlayMinute.EnableWindow(TRUE);
		m_editContentsPlaySecond.EnableWindow(TRUE);
	}//if
}

BOOL CFlashContentsDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CFlashContentsDialog::InitSubFilesCtrl()
{
	m_treeFiles.SetImageList(CUbcImageList::GetInstance()->GetImageList(), TVSIL_NORMAL);
	m_lcFiles.SetImageList(CUbcImageList::GetInstance()->GetImageList(), LVSIL_SMALL);

	m_lcFiles.SetExtendedStyle(m_lcFiles.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_lcFiles.InsertColumn(0, LoadStringById(IDS_FLASHCONTENTSDIALOG_STR001), LVCFMT_LEFT, 120);
	m_lcFiles.InsertColumn(1, LoadStringById(IDS_FLASHCONTENTSDIALOG_STR002), LVCFMT_RIGHT, 60);

	m_lcFiles.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CFlashContentsDialog::CreateSubFilesContents()
{
	DeleteSubFilesContents();

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return;

	CONTENTS_INFO_MAP* contents_map = pDoc->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;
		if(pContentsInfo->strLocalLocation.IsEmpty()) continue;

		CString strFileKey = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
		strFileKey.Replace(_T("\\"), _T("/"));
		strFileKey.MakeLower();
		if(m_mapSubFilesContents[strFileKey] != NULL) continue;

		// 임시 데이터 구조에 리스트를 넣는다
		CONTENTS_INFO* pBakContentsInfo = new CONTENTS_INFO;
		*pBakContentsInfo = *pContentsInfo;
		m_mapSubFilesContents[strFileKey] = (void*&)pBakContentsInfo;
	}
}

void CFlashContentsDialog::DeleteSubFilesContents()
{
	POSITION pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo = NULL;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );
		if(pContentsInfo) delete pContentsInfo;
	}
	m_mapSubFilesContents.RemoveAll();
}

void CFlashContentsDialog::RefreshFilesTree()
{
	m_treeFiles.SetRedraw(FALSE);
	m_treeFiles.DeleteAllItems();

	int nFolderIndex = CUbcImageList::GetInstance()->GetFolderIconIndex(GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH, FALSE);
	int nFolderSelIndex = CUbcImageList::GetInstance()->GetFolderIconIndex(GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH, TRUE);

	HTREEITEM hRootItem = m_treeFiles.InsertItem(_T("Contents")
												, CUbcImageList::GetInstance()->GetFolderIconIndex(_T("C:\\"), FALSE)
												, CUbcImageList::GetInstance()->GetFolderIconIndex(_T("C:\\"), TRUE)
												, NULL);

	CStringArray astrPathList;

	POSITION pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;
		if(pContentsInfo->strLocalLocation.IsEmpty()) continue;

		CString strChildPath;
		CString strContentsPath = UBC_CONTENTS_PATH;
		int nFindPos = pContentsInfo->strLocalLocation.MakeLower().Find(strContentsPath.MakeLower());
		if(nFindPos >= 0)
		{
			strChildPath = pContentsInfo->strLocalLocation.Mid(nFindPos + strContentsPath.GetLength());
		}

		if(strChildPath.IsEmpty()) continue;

		BOOL bIsExist = FALSE;
		int nIndex = -1;
		for(int i=0; i<astrPathList.GetCount() ;i++)
		{
			if(strChildPath.MakeLower() == astrPathList[i].MakeLower())
			{
				bIsExist = TRUE;
				break;
			}

			if(strChildPath < astrPathList[i])
			{
				nIndex = i;
				break;
			}
		}

		if(bIsExist) continue;

		if(nIndex >= 0) astrPathList.InsertAt(nIndex, strChildPath);
		else            astrPathList.Add(strChildPath);
	}

	for(int i=0; i<astrPathList.GetCount() ;i++)
	{
		HTREEITEM hNewItem = NULL;
		HTREEITEM hItem = hRootItem;

		CString strPath = astrPathList[i];
		int nPos = 0;
		CString strFolder;
		while((strFolder = strPath.Tokenize("/\\", nPos)).IsEmpty() == FALSE)
		{
			if(!hNewItem)
			{
				HTREEITEM hFindItem = NULL;

				if( m_treeFiles.ItemHasChildren( hItem ) )
				{
					HTREEITEM hSubItem = m_treeFiles.GetChildItem( hItem );
					while( hSubItem )
					{
						if( strFolder.MakeLower() == m_treeFiles.GetItemText(hSubItem).MakeLower() )
						{
							hFindItem = hSubItem;
							break;
						}

						hSubItem = m_treeFiles.GetNextSiblingItem( hSubItem );
					}
				}

				if(hFindItem)
				{
					hItem = hFindItem;
				}
				else
				{
					hNewItem = hItem;
				}
			}

			if(hNewItem)
			{
				hNewItem = m_treeFiles.InsertItem(strFolder, nFolderIndex, nFolderSelIndex, hNewItem);
			}
		}
	}

	m_treeFiles.Expand(hRootItem, TVE_EXPAND);
	m_treeFiles.SetRedraw(TRUE);
}

void CFlashContentsDialog::RefreshFilesList(CString strLocation)
{
	m_lcFiles.DeleteAllItems();

	POSITION pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;
		if(pContentsInfo->strLocalLocation.IsEmpty()) continue;

		CString strChildPath;
		CString strContentsPath = UBC_CONTENTS_PATH;
		int nFindPos = pContentsInfo->strLocalLocation.MakeLower().Find(strContentsPath.MakeLower());
		if(nFindPos >= 0)
		{
			strChildPath = pContentsInfo->strLocalLocation.Mid(nFindPos + strContentsPath.GetLength());
		}

		if(strChildPath.MakeLower() != strLocation.MakeLower()) continue;

		int nRow = m_lcFiles.GetItemCount();
		m_lcFiles.InsertItem(nRow, _T(""));
		UpdateFilesListRow(nRow, pContentsInfo);
	}
}

void CFlashContentsDialog::UpdateFilesListRow(int nRow, CONTENTS_INFO* pContentsInfo)
{
	if(!pContentsInfo) return;

	CString strFullPath = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;

	TCHAR drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(strFullPath, drive, path, filename, ext);

	CFileStatus fs;
	CString strFileSize;
	int nIconIndex = CUbcImageList::GetInstance()->GetFileIconIndex(ext);
	int nOverlapIdx = 0;
	// 로컬에 파일이 존재하는 경우
	//if(CFile::GetStatus(strFullPath, fs))
	if(CEnviroment::GetFileSize(strFullPath, fs.m_size))
	{
		strFileSize = ::ToFileSize(fs.m_size, 'A');
	}
	// 서버에 파일이 존재하는 경우
	else if(pContentsInfo->bServerFileExist)
	{
		strFileSize = ::ToFileSize(pContentsInfo->nFilesize, 'A');
		nOverlapIdx = SERVER_IMG_IDX;
	}
	// 로컬과 서버에 모두 존재하지 않는 경우
	else
	{
		strFileSize = ::ToFileSize(pContentsInfo->nFilesize, 'A');
		nOverlapIdx = NIX_IMG_IDX;
	}

	m_lcFiles.SetItem(nRow, 0, LVIF_IMAGE|LVIF_TEXT, pContentsInfo->strFilename, nIconIndex, 0, 0, NULL);
	m_lcFiles.SetItemText(nRow, 1, strFileSize);
	if(nOverlapIdx > 0)
	{
		m_lcFiles.SetItemState(nRow, LVIS_OVERLAYMASK, INDEXTOOVERLAYMASK(nOverlapIdx));
	}

	m_lcFiles.SetItemData(nRow, (DWORD_PTR)pContentsInfo);
}

void CFlashContentsDialog::OnTvnSelchangedTreeFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	*pResult = 0;

	RefreshFilesList(GetTreeFullPath(pNMTreeView->itemNew.hItem));
}

// skpark 2013.2.25  전에 사용하던 폴더를 초기 폴더로 지정
static 	CString s_strSaveFolder="";  	
static int CALLBACK BrowseCallbackProc(
					HWND hwnd,UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	switch (uMsg)
	{
	case BFFM_INITIALIZED:
		{
			SendMessage(hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)lpData );
		}
		break;
	}

	return 0; // Always return 0.
}

void CFlashContentsDialog::OnBnClickedButtonBrowserAddFolder()
{
	CString strChildPath = GetTreeFullPath(m_treeFiles.GetSelectedItem());

	TCHAR szDirName[MAX_PATH] = {0};

	BROWSEINFO bi;
	ZeroMemory(&bi,sizeof(BROWSEINFO));
	bi.hwndOwner = this->m_hWnd;
	bi.pszDisplayName = szDirName;			// 선택된 디렉토리명 저장 
	bi.lpszTitle = _T("Select Folder");
	//bi.ulFlags = BIF_NEWDIALOGSTYLE;

	// skpark 2013.2.25  전에 사용하던 폴더를 초기 폴더로 지정
	CString initPath = s_strSaveFolder;
	bi.ulFlags        = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT | BIF_VALIDATE;
	bi.lpfn           = BrowseCallbackProc;
	bi.lParam         = (LPARAM)initPath.GetBuffer( initPath.GetLength() );

	LPITEMIDLIST idl = SHBrowseForFolder(&bi);
	initPath.ReleaseBuffer();  	// skpark 2013.2.25  전에 사용하던 폴더를 초기 폴더로 지정
	if(!idl) return;

	TCHAR szFolderPath[MAX_PATH] = {0};
	SHGetPathFromIDList(idl, szFolderPath);	// 전체 PATH을 포함한 디렉토리명
//	UbcMessageBox(szFolderPath);

	s_strSaveFolder = szFolderPath; 	// skpark 2013.2.25  전에 사용하던 폴더를 초기 폴더로 지정


	CString strToPath = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH + strChildPath;
	strToPath.Replace(_T("/"), _T("\\"));
	MakeSureDirectoryPathExists(strToPath);
	if(!ShellCopyFile(szFolderPath, strToPath, FALSE)) return;

	TCHAR drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(szFolderPath, drive, path, filename, ext);

	CString strFolderName;
	strFolderName.Format(_T("%s%s"), filename, ext);
	AddSubFolder(strChildPath + strFolderName + "/", szFolderPath);

	RefreshFilesTree();
	SelectTreeItem(strChildPath + strFolderName + "/");
	RefreshFilesList(strChildPath + strFolderName + "/");

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CFlashContentsDialog::SelectTreeItem(CString strChildPath)
{
	m_treeFiles.SetRedraw(FALSE);
	HTREEITEM hRootItem = m_treeFiles.GetRootItem();
	HTREEITEM hItem = hRootItem;

	int nPos = 0;
	CString strFolder;
	while((strFolder = strChildPath.Tokenize("/\\", nPos)).IsEmpty() == FALSE)
	{
		if( !m_treeFiles.ItemHasChildren( hItem ) ) break;

		HTREEITEM hSubItem = m_treeFiles.GetChildItem( hItem );
		while( hSubItem )
		{
			if( strFolder.MakeLower() == m_treeFiles.GetItemText(hSubItem).MakeLower() )
			{
				hItem = hSubItem;
				break;
			}

			hSubItem = m_treeFiles.GetNextSiblingItem( hSubItem );
		}
	}

	m_treeFiles.SelectItem( hItem );
	m_treeFiles.Expand(hItem, TVE_EXPAND);
	m_treeFiles.SetRedraw(TRUE);
}

void CFlashContentsDialog::AddSubFolder(CString strChildPath, CString szFolderPath)
{
	CFileFind ff;
	BOOL bWorking = ff.FindFile(szFolderPath + _T("\\*.*"));
	while(bWorking)
	{
		bWorking = ff.FindNextFile();

		if(ff.IsDots()) continue;

		if(ff.IsDirectory())
		{
			AddSubFolder(strChildPath + ff.GetFileName() + "/", ff.GetFilePath());
		}
		else
		{
			BOOL bExist;
			AddSubFileContents(strChildPath, ff.GetFilePath(), bExist);
		}
	}
}

void CFlashContentsDialog::OnBnClickedButtonBrowserDelFolder()
{
	if(!m_btnBrowseDelFolder.IsWindowEnabled()) return;

	HTREEITEM hSelItem = m_treeFiles.GetSelectedItem();
	if(!hSelItem)
	{
		UbcMessageBox(LoadStringById(IDS_FLASHCONTENTSDIALOG_MSG003));
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_FLASHCONTENTSDIALOG_MSG001), MB_YESNO) != IDYES) return;

	if(m_treeFiles.GetParentItem(hSelItem) == NULL)
	{
		DeleteSubFilesContents();
		m_treeFiles.DeleteAllItems();

		HTREEITEM hRootItem = m_treeFiles.InsertItem(_T("Contents")
													, CUbcImageList::GetInstance()->GetFolderIconIndex(_T("C:\\"), FALSE)
													, CUbcImageList::GetInstance()->GetFolderIconIndex(_T("C:\\"), TRUE)
													, NULL);
	}
	else
	{
		CString strTreePath = GetTreeFullPath(hSelItem);

		POSITION pos = m_mapSubFilesContents.GetStartPosition();
		while(pos != NULL)
		{
			CString strFileKey;
			CONTENTS_INFO* pContentsInfo;
			m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

			if(!pContentsInfo) continue;
			if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
			if(pContentsInfo->strParentId != m_strContentsId) continue;
			if(pContentsInfo->strLocalLocation.IsEmpty()) continue;

			CString strChildPath;
			CString strContentsPath = UBC_CONTENTS_PATH;
			int nFindPos = pContentsInfo->strLocalLocation.MakeLower().Find(strContentsPath.MakeLower());
			if(nFindPos >= 0)
			{
				strChildPath = pContentsInfo->strLocalLocation.Mid(nFindPos + strContentsPath.GetLength());
			}

			if(!strTreePath.IsEmpty() && strChildPath.MakeLower().Find(strTreePath.MakeLower()) != 0) continue;

			m_mapSubFilesContents.RemoveKey(strFileKey);
			delete pContentsInfo;
		}

		m_treeFiles.DeleteItem(hSelItem);
	}

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CFlashContentsDialog::OnBnClickedButtonBrowserAddFile()
{
	CString strChildPath = GetTreeFullPath(m_treeFiles.GetSelectedItem());

	CString strFilter;
	CString szFileExts = "*.*";
	strFilter.Format("All Files (%s)|%s||", szFileExts, szFileExts);

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT | OFN_ENABLESIZING , strFilter, this);

	// skpark 2013.2.25 복수개의 파일 선택시, 이름의 합 만큼의 버퍼를 지정하지 않으면 대량 선택은 처리하지 못한다.
	// 버퍼가 충분히 크지 않으면 어쩔 것인가...황당한 인터페이스라 하지 않을 수 없다.
	// 하는 수 없이 1000개까지 들어올수 있도록 잡는다.
	char filename_buf[256*1000] = {0, };
	dlg.m_ofn.lpstrFile = filename_buf;
	dlg.m_ofn.nMaxFile = 256*1000;

	if(dlg.DoModal() != IDOK) return;

	POSITION pos = dlg.GetStartPosition();

	while(pos)
	{
		CString strPathName = dlg.GetNextPathName(pos);
		CString strToPath = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH + strChildPath;
		strToPath.Replace(_T("/"), _T("\\"));

		MakeSureDirectoryPathExists(strToPath);

		if(!ShellCopyFile(strPathName, strToPath, FALSE)) {
			return;
		}

		BOOL bExist = FALSE;
		CONTENTS_INFO* pContentsInfo = AddSubFileContents(strChildPath, strPathName, bExist);
		if(pContentsInfo)
		{
			int nRow = -1;
			if(bExist)
			{
				for(int i=0; i<m_lcFiles.GetItemCount() ;i++)
				{
					CString strTmp = m_lcFiles.GetItemText(i, 0);
					if(strTmp.MakeLower() == pContentsInfo->strFilename.MakeLower())
					{
						nRow = i;
						break;
					}
				}
			}

			if(nRow < 0)
			{
				nRow = m_lcFiles.GetItemCount();
				m_lcFiles.InsertItem(nRow, _T(""));
			}
			UpdateFilesListRow(nRow, pContentsInfo);
		}
	}
	//if(!CheckTotalContentsSize(dlg.GetPathName())) return;

	//RefreshFilesList(strChildPath);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CFlashContentsDialog::OnBnClickedButtonBrowserDelFile()
{
	if(!m_btnBrowseDelFile.IsWindowEnabled()) return;

	if(UbcMessageBox(LoadStringById(IDS_FLASHCONTENTSDIALOG_MSG002), MB_YESNO) != IDYES) return;

	POSITION pos = m_lcFiles.GetFirstSelectedItemPosition();

	if(!pos)
	{
		UbcMessageBox(LoadStringById(IDS_FLASHCONTENTSDIALOG_MSG003));
		return;
	}

	CArray<int, int> anDelItems;

	while(pos)
	{
		int nItem = m_lcFiles.GetNextSelectedItem(pos);
		anDelItems.Add(nItem);
	}

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	if(anDelItems.GetCount() > 0)
	{
		m_bIsChangedFile = TRUE;
	}

	for(int i=anDelItems.GetCount()-1; i>=0 ;i--)
	{
		CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(anDelItems[i]);
		if(pContentsInfo)
		{
			CString strFileKey = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
			strFileKey.Replace(_T("\\"), _T("/"));
			strFileKey.MakeLower();
			m_mapSubFilesContents.RemoveKey(strFileKey);
			delete pContentsInfo;
			m_lcFiles.DeleteItem(anDelItems[i]);
		}
	}

	RefreshFilesList(GetTreeFullPath(m_treeFiles.GetSelectedItem()));
}

CONTENTS_INFO* CFlashContentsDialog::AddSubFileContents(CString strChildPath, CString strFilepath, BOOL& bExist)
{
	bExist = FALSE;

	TCHAR drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(strFilepath, drive, path, filename, ext);

	CONTENTS_INFO* pContentsInfo = new CONTENTS_INFO;

	pContentsInfo->strId = (CUbcGUID::GetInstance()->ToGUID(_T("")));
	pContentsInfo->strContentsName = CPreventChar::GetInstance()->ConvertToAvailableChar(filename);
	pContentsInfo->nContentsType = CONTENTS_FILE;
	pContentsInfo->strParentId = m_strContentsId;
	pContentsInfo->strLocalLocation = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH + strChildPath;
	pContentsInfo->strFilename.Format(_T("%s%s"), filename, ext);
	pContentsInfo->bLocalFileExist = true;
	pContentsInfo->SetModified(); //skpark same_size_file_problem 2014.06.11
	TraceLog(("skpark %s file changed !!!" , pContentsInfo->strFilename));

	CFileStatus fs;
	//if(CFile::GetStatus(strFilepath, fs))
	if(CEnviroment::GetFileSize(strFilepath, fs.m_size))
		pContentsInfo->nFilesize = fs.m_size;

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(pContentsInfo->nFilesize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)(pContentsInfo->strLocalLocation + pContentsInfo->strFilename), szMd5);
	}
	pContentsInfo->strFileMD5 = szMd5;

	CString strFileKey = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
	strFileKey.Replace(_T("\\"), _T("/"));
	strFileKey.MakeLower();
	if(m_mapSubFilesContents[strFileKey] != NULL)
	{
		delete (CONTENTS_INFO*)m_mapSubFilesContents[strFileKey];
		bExist = TRUE;
	}

	m_mapSubFilesContents[strFileKey] = pContentsInfo;

	return pContentsInfo;
}

CString CFlashContentsDialog::GetTreeFullPath(HTREEITEM hItem)
{
	CString strChildPath;
	while(hItem)
	{
		CString strItemText = m_treeFiles.GetItemText(hItem);
		hItem = m_treeFiles.GetParentItem(hItem);
		if(hItem)
		{
			strChildPath = strItemText + _T("/") + strChildPath;
		}
	}

	return strChildPath;
}

// 폴더를 선택하여 다운로드
void CFlashContentsDialog::OnBnClickedButtonDownloadFolder()
{
#ifdef _UBCSTUDIO_EE_
	CFtpMultiSite multiFtp(GetEnvPtr()->m_strHttpOnly.CompareNoCase("CLIENT") == 0);

	CString szPackage;
	szPackage.Format("%s%s%s.ini", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONFIG_PATH, GetEnvPtr()->m_PackageInfo.szPackage);
	multiFtp.AddSite(GetEnvPtr()->m_PackageInfo.szSiteID, szPackage);
	multiFtp.OnlyAddFile();

	CString strTreePath = GetTreeFullPath(m_treeFiles.GetSelectedItem());

	POSITION pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;
		if(pContentsInfo->strLocalLocation.IsEmpty()) continue;

		CString strChildPath;
		CString strContentsPath = UBC_CONTENTS_PATH;
		int nFindPos = pContentsInfo->strLocalLocation.MakeLower().Find(strContentsPath.MakeLower());
		if(nFindPos >= 0)
		{
			strChildPath = pContentsInfo->strLocalLocation.Mid(nFindPos + strContentsPath.GetLength());
		}

		//if(strChildPath.MakeLower() != strTreePath.MakeLower()) continue;
		if(strChildPath.Find(strTreePath) != 0) continue;

		if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
			GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
		   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
		{
			multiFtp.AddDownloadFile(pContentsInfo->strServerLocation + pContentsInfo->strFilename);
		}
	}

	if(!multiFtp.RunFtp(CFtpMultiSite::eDownload)) return;

	pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;
		if(pContentsInfo->strLocalLocation.IsEmpty()) continue;

		CString strChildPath;
		CString strContentsPath = UBC_CONTENTS_PATH;
		int nFindPos = pContentsInfo->strLocalLocation.MakeLower().Find(strContentsPath.MakeLower());
		if(nFindPos >= 0)
		{
			strChildPath = pContentsInfo->strLocalLocation.Mid(nFindPos + strContentsPath.GetLength());
		}

		//if(strChildPath.MakeLower() != strTreePath.MakeLower()) continue;
		if(strChildPath.Find(strTreePath) != 0) continue;

		if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
			GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
		   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
		{
			pContentsInfo->bLocalFileExist = true;
			pContentsInfo->bServerFileExist = true;
		}
	}

	RefreshFilesList(strTreePath);
#endif//_UBCSTUDIO_EE_
}

// 파일을 선택하여 다운로드
void CFlashContentsDialog::OnBnClickedButtonDownloadContents()
{
	if(DownloadSelectedContents())
	{
		RefreshFilesList(GetTreeFullPath(m_treeFiles.GetSelectedItem()));
	}
}

// 파일을 선택하여 다운로드
BOOL CFlashContentsDialog::DownloadSelectedContents()
{
#ifdef _UBCSTUDIO_EE_
	CFtpMultiSite multiFtp(GetEnvPtr()->m_strHttpOnly.CompareNoCase("CLIENT") == 0);

	CString szPackage;
	szPackage.Format("%s%s%s.ini", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONFIG_PATH, GetEnvPtr()->m_PackageInfo.szPackage);
	multiFtp.AddSite(GetEnvPtr()->m_PackageInfo.szSiteID, szPackage);
	multiFtp.OnlyAddFile();

	if(m_lcFiles.GetSelectedCount() > 0)
	{
		POSITION pos = m_lcFiles.GetFirstSelectedItemPosition();
		while(pos)
		{
			int nItem = m_lcFiles.GetNextSelectedItem(pos);
			CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(nItem);
			if(pContentsInfo == NULL) continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				multiFtp.AddDownloadFile(pContentsInfo->strServerLocation + pContentsInfo->strFilename);
			}
		}
	}
	else
	{
		for(int nItem = 0; nItem < m_lcFiles.GetItemCount(); nItem++)
		{
			CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(nItem);
			if(pContentsInfo == NULL) continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				multiFtp.AddDownloadFile(pContentsInfo->strServerLocation + pContentsInfo->strFilename);
			}
		}
	}

	if(!multiFtp.RunFtp(CFtpMultiSite::eDownload)) return FALSE;

	if(m_lcFiles.GetSelectedCount() > 0)
	{
		POSITION pos = m_lcFiles.GetFirstSelectedItemPosition();
		while(pos)
		{
			int nItem = m_lcFiles.GetNextSelectedItem(pos);
			CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(nItem);
			if(pContentsInfo == NULL) continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				pContentsInfo->bLocalFileExist = true;
				pContentsInfo->bServerFileExist = true;
			}
		}
	}
	else
	{
		for(int nItem = 0; nItem < m_lcFiles.GetItemCount(); nItem++)
		{
			CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(nItem);
			if(pContentsInfo == NULL) continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				pContentsInfo->bLocalFileExist = true;
				pContentsInfo->bServerFileExist = true;
			}
		}
	}
#endif//_UBCSTUDIO_EE_
	return TRUE;
}

void CFlashContentsDialog::OnNMDblclkListFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(pNMLV->iItem);
	if(!pContentsInfo) return;

	if( GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
		!pContentsInfo->bLocalFileExist  &&
		 pContentsInfo->bServerFileExist )
	{
		CWaitMessageBox wait;

		if(!DownloadSelectedContents()) return;

		m_lcFiles.DeleteItem(nRow);
		m_lcFiles.InsertItem(nRow, _T(""));
		UpdateFilesListRow(nRow, pContentsInfo);
		m_lcFiles.SetItemState(nRow, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
	}

	CString strExecute = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
	strExecute.Replace(_T("/"), _T("\\"));

	::ShellExecute(GetSafeHwnd(), "open", strExecute, NULL, NULL, SW_SHOWNORMAL);
}

void CFlashContentsDialog::OnLvnKeydownListFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	*pResult = 0;

	if(pLVKeyDow->wVKey == VK_DELETE)
		OnBnClickedButtonBrowserDelFile();
}

void CFlashContentsDialog::OnTvnKeydownTreeFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTVKEYDOWN pTVKeyDown = reinterpret_cast<LPNMTVKEYDOWN>(pNMHDR);
	*pResult = 0;

	if(pTVKeyDown->wVKey == VK_DELETE)
		OnBnClickedButtonBrowserDelFolder();
}
