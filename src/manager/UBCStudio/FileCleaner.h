#pragma once
#include "common\utblistctrl.h"

// CFileCleaner dialog
struct SFCInfo{
	CString szDrive;
	CString szPath;
	CString szName;
	CString szExe;
	ULONGLONG lSize;
};

class CFileCleaner : public CDialog
{
	DECLARE_DYNAMIC(CFileCleaner)
public:
private:
	void InitList();
	void InsertItems(bool bChek=true);
	void InitContents();
	void DeleteFile(CString, bool wastebasket);
	void DeleteFolderFiles(CString);
	int GetFileType(CString);
public:
	enum EFC_COL{eCheck, EFC_DRV, EFC_FILE, EFC_SIZE };
private:
	ULONGLONG	m_TotalSize;
	CUTBListCtrl m_lcList;
	CImageList	m_ilContents;
	CStringList	m_lsContents;
	CStringArray m_arDrives;
	CList<SFCInfo*> m_lsFiles;

	static int CALLBACK CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

public:
	CFileCleaner(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFileCleaner();

// Dialog Data
	enum { IDD = IDD_FILE_CLEANER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonRemove();
	afx_msg void OnNMDblclkListFcnFiles(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnColumnclickListFcnFiles(NMHDR *pNMHDR, LRESULT *pResult);
};
