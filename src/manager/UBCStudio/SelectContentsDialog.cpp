// SelectContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "UBCStudioDoc.h"
#include "SelectContentsDialog.h"

// CSelectContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSelectContentsDialog, CDialog)

CSelectContentsDialog::CSelectContentsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectContentsDialog::IDD, pParent)
	, m_reposControl (this)
	, m_pContentsInfo (NULL)
{

}

CSelectContentsDialog::~CSelectContentsDialog()
{
}

void CSelectContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CONTENTS, m_lcContents);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CSelectContentsDialog, CDialog)
	ON_WM_SIZE()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONTENTS, &CSelectContentsDialog::OnNMDblclkListContents)
END_MESSAGE_MAP()


// CSelectContentsDialog 메시지 처리기입니다.

BOOL CSelectContentsDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	HICON hIcon = LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_CONTENTS));
	SetIcon(hIcon, TRUE);
	SetIcon(hIcon, FALSE);

	m_btnOK.LoadBitmap(IDB_BTN_SELECT, RGB(255, 255, 255));
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));

	m_btnOK.SetToolTipText(LoadStringById(IDS_SELECTCONTENTSDIALOG_BUT001));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_SELECTCONTENTSDIALOG_BUT002));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	m_lcContents.m_bContentsFilter[CONTENTS_ETC] = FALSE;
	m_lcContents.SetDocument(m_pDocument);
	m_lcContents.Initialize();

	m_reposControl.AddControl(&m_lcContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnOK, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnCancel, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSelectContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int nItem = m_lcContents.GetNextSelectedItem(pos);
		m_pContentsInfo = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);

		CDialog::OnOK();
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_SELECTCONTENTSDIALOG_MSG001), MB_ICONWARNING);
	}
}

void CSelectContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();
}

void CSelectContentsDialog::OnNMDblclkListContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	OnOK();
}
