#pragma once

//#define LOGIN_DAT				_T("UBCLogin.dat")
//#define ORBCONN_INI				_T("UBCConnect.ini")
//#define UBCVARS_INI				_T("UBCVariables.ini")
//
//#ifdef _UBCSTUDIO_EE_
//#define ENVIROMENT_INI			_T("UBCStudioEE.ini")
//#else
//#define ENVIROMENT_INI			_T("UBCStudioPE.ini")
//#endif

#define NUM_ENV_CONTENTS		20
#define NUM_ENV_INEXISTENT		(NUM_ENV_CONTENTS)
#define NUM_ENV_SERVER			(NUM_ENV_CONTENTS*2)

#define STR_ENV_SERVER			_T("server")
#define STR_ENV_TIME			_T("%Y/%m/%d %H:%M:%S")
#define STR_ENV_EMPTY_TIME		_T("1970/01/01 09:00:00")

struct SLoginInfo
{
	int  nPWSize;
	char szSite[MAX_PATH];
	char szID[MAX_PATH];
	char szPW[MAX_PATH];
};

struct SFtpInfo{
	char szPmId[256];
	char szIP[16];
	char szID[16];
	char szPW[16];
	int  nPort;
};

struct SHostInfo4Studio{
	BYTE	displayNo;
	bool	networkUse;
	bool	operationalState;
	short	displayCounter;
	time_t	lastPackageTime;
	CString	siteId;
	CString	hostId;
	CString	autoPackage;
	CString	currentPackage;
	CString	lastPackage;

	SHostInfo4Studio()
	{
		displayNo = 1;
		networkUse = 0;
		operationalState = 0;
		displayCounter = 1;
		lastPackageTime = 0;
	}
};

struct SPackageInfo4Studio{
	bool	bShow;
	bool	bModify;
	time_t  tmUpdateTime;
	CString	szDrive;
	CString	szProcID;
	CString	szSiteID;
	CString	szPackage;
	CString	szDescript;
	CString	szUpdateUser;
	CString	szUpdateTime;

	// 패키지객체 속성추가
	LONG	nContentsCategory;	// 종류 (모닝,..)
	LONG	nPurpose         ;	// 용도별 (교육용,..)
	LONG	nHostType        ;	// 단말타입 (키오스크..)
	LONG	nVertical        ;	// 가로/세로 방향 (가로,..)
	LONG	nResolution      ;	// 해상도 (1920x1080,..)
	bool	bIsPublic        ;	// 공개여부
	bool	bIsVerify        ;	// 승인여부
	time_t	tmValidationDate ;	// 패키지유효기간
	CString	strValidationDate;	// 패키지유효기간

	SPackageInfo4Studio()
	{
		bShow = true;
		bModify = false;
		tmUpdateTime = 0;

		// 패키지객체 속성추가
		nContentsCategory = -1;
		nPurpose          = -1;
		nHostType         = -1;
		nVertical         = -1;
		nResolution       = -1;
		bIsPublic         = false;
		bIsVerify         = true;
		tmValidationDate  = 0;
	}

	void Clear()
	{
		bShow = true;
		bModify = false;
		tmUpdateTime = 0;
		szDrive.Empty();
		szProcID.Empty();
		szSiteID.Empty();
		szPackage.Empty();
		szDescript.Empty();
		szUpdateUser.Empty();
		szUpdateTime.Empty();

		// 패키지객체 속성추가
		nContentsCategory = -1;
		nPurpose          = -1;
		nHostType         = -1;
		nVertical         = -1;
		nResolution       = -1;
		bIsPublic         = false;
		bIsVerify         = true;
		tmValidationDate  = 0;
		strValidationDate.Empty();
	}
};

struct SCodeItem
{
	CString strMgrId;
	CString strCodeId;
	CString strCategoryName;
	CString strEnumString;
	int		nEnumNumber;
	bool	bVisible;
	short	nDisplayOrder;

	SCodeItem()
	{
		nEnumNumber = 0;
		bVisible = 0;
		nDisplayOrder = 0;
	}
};

// 0001408: Customer 정보 입력을 받는다.
struct SCustomerInfo
{
	CString mgrId;
	CString customerId;
	CString url;
	CString description;
	int		gmt;
	CString language;
	
	SCustomerInfo()
	{
		mgrId.Empty();
		customerId.Empty();
		url.Empty();
		description.Empty();
		gmt = 9999;
		language.Empty();
	}
};

typedef CList<SCodeItem> CodeItemList;
typedef CList<SCustomerInfo>			CustomerInfoList;	// 0001408: Customer 정보 입력을 받는다.

#define GetEnvPtr()		CEnviroment::GetObject()

#ifdef _RELEASE_LOG
	#define TraceLog(_f_) \
		{ \
			CString szTraceLogBuf; \
			szTraceLogBuf.Format(_T("%s(%d) : "), __FILE__, __LINE__); \
			szTraceLogBuf.AppendFormat _f_; \
			OutputDebugString(szTraceLogBuf); \
		}
#elif _DEBUG
	extern CString g_szTraceLogBuf;
	//CString g_szTraceLogBuf;
	#define TraceLog(_f_) \
		g_szTraceLogBuf.Format _f_; \
		TRACE("%s(%d) :\n >>> %s\n", __FILE__, __LINE__, g_szTraceLogBuf)
#elif _TAO
	#define TraceLog(_f_) \
		if(true){ \
		CString szTraceLogBuf; \
		szTraceLogBuf.Format _f_; \
		GetEnvPtr()->Log(__FILE__, __LINE__, szTraceLogBuf); }
#else
	#define TraceLog(_f_)
//	extern CString g_szTraceLogBuf;
//	#define TraceLog(_f_) \
//	g_szTraceLogBuf.Format _f_; \
//	AfxMessageBox (g_szTraceLogBuf)
#endif//_DEBUG

class CSimpleFtp;

class CEnviroment
{
public:
	static CEnviroment* GetObject();
	static void Release();

	enum { eStudioPE, eStudioEE };
	enum { eUBCType, eUSTBType };
	enum { eAuthNoUser, eAuthSuper, eAuthSite, eAuthUser };
	enum { eSQISOFT, eNARSHA, eLOTTE, eADASSET, eKIA, eHYUNDAI, eKMNL, eKMCL, eKPOST, eUBGOLF  }; // 2010.09.30 eADASSET=창일

	void 					Log(const char* szfilename, int nLine, const char *szFormat);

	void 					SaveLoginInfo(bool);

	bool 					Connect(CSimpleFtp*, LPCTSTR);
	//bool 					GetFtpInfo(LPCTSTR, SFtpInfo*);
	bool 					GetFile(LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR);
	bool 					PutFile(LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR);

	UINT 					RefreshPackageList(bool bAll, LPCTSTR szWhere=NULL);
	void 					RemoveAllPackage();
	void 					RemovePackageWithout(LPCTSTR);
	void 					AddPackage(SPackageInfo4Studio* pInfo);
	void 					ChangeCurPackage(SPackageInfo4Studio info);
	SPackageInfo4Studio*	FindPackage(CString, CString);

	void 					GetTempMaxSize(int& cx, int& cy);
	void 					GetTempMinSize(int& cx, int& cy);

	void 					InitHostInfo(CString strSiteId, CString strHostId, CString strWhere=_T(""));
	void 					InitHostInfo(CString strPlayContentsId, CList<SHostInfo4Studio*>& lsHost);
	void 					RemoveHost();
	void					SetPathOpenFile(LPCTSTR);
	bool					IsValidOpenFolder();

	static bool				GetFileSize(CString, ULONGLONG&);
	static SYSTEMTIME		CheckTime(SYSTEMTIME&);

	bool					GetAutoUpdate();
	void					SetAutoUpdate(bool bValue);
private:
	CEnviroment();
	~CEnviroment();

	void					InitPath();
	void					InitLoginInfo();
	char*					Encrypt(char* buf, int nSize);
	void					Load_ini();
	void					Load_ee_ini();
	void					Load_pe_ini();
public:
	const int				m_Edition;
	int						m_Authority;
	int						m_StudioType;
	int						m_Customer;
	CString					m_strCustomer;
	bool					m_bLicense;
	bool					m_bAutoEncoding;
	ULONGLONG				m_TotalContentsSize;

	int						m_nMaxPlayTemplateCount;
	int						m_nMaxContentsCount;
	ULONGLONG				m_lMaxContentsSize;

	bool					m_bUseTimeBasePlayContents;
	bool					m_bUseClickPlayContents;	// 0000782: 부모-자식 스케줄 기능을 수정한다.
	bool					m_bUseTimeCheck;	// skpark 2014.7.29 size_size_file_problem

	bool					m_bUseTV;

	bool					m_bUseAutoTempSave;
	int						m_nAutoTempSaveInterval;

	CString					m_strHttpOnly;	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.

	CString					m_szSite;
	CString					m_szLoginID;
	CString					m_szPassword;

	CString					m_strPackage;
	SPackageInfo4Studio		m_PackageInfo;

//	CList<CString>			m_lsSite;
	//CMap<LPCTSTR, LPCTSTR, SPackageInfo4Studio, SPackageInfo4Studio&>	m_mapPackage;
	CMapStringToPtr			m_mapPackage;
	CList<SHostInfo4Studio*>	m_lsHost;
	CList<CRect>			m_lsTempRect;

	CString					m_szDrive;
	CString					m_szPath;
	CString					m_szFile;
	CString					m_szName;
	CString					m_szExt;
	CString					m_szModule;

	CString					m_szPathOpenFile;
	
private:
	static CEnviroment* m_pThis;
};