#include "StdAfx.h"
#include "ReplaceInFile.h"

CReplaceInFile::CReplaceInFile(void)
{
}

CReplaceInFile::~CReplaceInFile(void)
{
}

void CReplaceInFile::AddItem(CString strSrc, CString strDst)
{
	m_mapItems[strSrc] = strDst;
}

void CReplaceInFile::DeleteItem(CString strSrc)
{
	m_mapItems.RemoveKey(strSrc);
}

void CReplaceInFile::DeleteAllItem()
{
	m_mapItems.RemoveAll();
}

int CReplaceInFile::GetCount()
{
	return m_mapItems.GetCount();
}

int CReplaceInFile::ReplaceInFile(CString strSrcFilename, CString strDstFilename/*=_T("")*/, BOOL bNoCase/*=TRUE*/)
{
	CString strContext;
	int nCount = 0;

	if(!LoadFile(strSrcFilename, strContext))
	{
		return -1;
	}

	POSITION pos = m_mapItems.GetStartPosition();
	while(pos)
	{
		CString strSrc, strDst;
		m_mapItems.GetNextAssoc(pos, strSrc, strDst);

		if(strSrc.IsEmpty()) continue;

		if(strSrc.CompareNoCase(strDst) == 0) continue;

		if(bNoCase)
		{
			int nSrcLen = strSrc.GetLength();
			int nDstLen = strDst.GetLength();

			for(int i=0; i<strContext.GetLength()-nSrcLen ;i++)
			{
				CString strMid = strContext.Mid(i, nSrcLen);

				if(strMid.CompareNoCase(strSrc) == 0)
				{
					strContext.Delete(i, nSrcLen);
					strContext.Insert(i, strDst);
					nCount++;
					i += nDstLen;
				}
			}
		}
		else
		{
			nCount += strContext.Replace(strSrc, strDst);
		}
	}

	if(!strDstFilename.IsEmpty() && !SaveFile(strDstFilename, strContext))
	{
		return -1;
	}

	return nCount;
}

BOOL CReplaceInFile::LoadFile(CString strFilename, CString& strContext)
{
	if(strFilename.IsEmpty()) return FALSE;

	CFileStatus fs;
	if(!CFile::GetStatus(strFilename, fs)) return FALSE;

	//CStdioFile file;
	////if(!file.Open(strFilename, CFile::modeRead | CFile::typeBinary))
	//if(!file.Open(strFilename, CFile::modeRead))
	//{
	//	return FALSE;
	//}

	//BOOL bRet = file.ReadString(strContext);

	CFile file;
	if(!file.Open(strFilename, CFile::modeRead | CFile::typeBinary)) return FALSE;

	int size = (int)file.GetLength();
	LPBYTE buf = new BYTE[size+2];
	::memset(buf, 0, size+2);

	BOOL bRet = (file.Read(buf, size) > 0);

	int nHdrByte = 0;
#ifdef UNICODE
	if(buf[0] == 0xff && buf[1] == 0xfe)
	{	
		nHdrByte = 2;
	}
#endif
	strContext = (LPTSTR)(buf + nHdrByte);

	file.Close();

	return bRet;
}

BOOL CReplaceInFile::SaveFile(CString strFilename, CString strContext)
{
	if(strFilename.IsEmpty()) return FALSE;

	CStdioFile file;
	//if(!file.Open(strFilename, CFile::modeWrite | CFile::typeBinary))
	if(!file.Open(strFilename, CFile::modeCreate | CFile::modeWrite))
	{
		return FALSE;
	}

	TRY
	{
		file.WriteString(strContext);
	}
	CATCH(CFileException, e);                                          
	{
		TRACE(_T("WriteData: An exception occured while writing to the file %s\n"), strFilename);
		e->Delete();
		file.Close();
		return FALSE;
	}
	END_CATCH

	file.Close();
	return TRUE;
}
