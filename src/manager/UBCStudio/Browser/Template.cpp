// Template.cpp : 구현 파일입니다.
//

#include "stdafx.h"
//#include "UBCStudio.h"
//#include "Host.h"
#include "Template.h"

#include "io.h"

// CTemplate

#define		FRAME_CHECK_ID			2000	// id
#define		FRAME_CHECK_TIME		100	// time

#define		TEMPLATE_SHOW_ID		2001	// id
#define		TEMPLATE_SHOW_TIME		100	// time

#define		TEMPLATE_HIDE_ID		2002	// id
#define		TEMPLATE_HIDE_TIME		100	// time

UINT	CTemplate::m_nTemplateID = 0xfffe;

CTemplate*	CTemplate::m_pPhoneTemplate = NULL;
CMutex		CTemplate::m_mutexPhone;


IMPLEMENT_DYNAMIC(CTemplate, CWnd)

CTemplate* CTemplate::GetTemplateObject(CWnd* pParent, LPCSTR lpszID, CString& strErrorMessage)
{

	CTemplate* tmpl = new CTemplate(pParent, lpszID);

	if(tmpl != NULL)
	{
		BOOL ret_value = tmpl->Create(NULL, "Template", WS_CHILD, CRect(0,0,100,100), pParent, m_nTemplateID--);
		if(ret_value)
		{
			return tmpl;
		}
		else
		{
			strErrorMessage = "Fail to create Template !!!";
			delete tmpl;
			return NULL;
		}
	}

	strErrorMessage = "Fail to create Template !!!";

	return NULL;
}

CTemplate::CTemplate(CWnd* pParent, LPCSTR lpszID)
:	m_pParentWnd (pParent)
,	m_strErrorMessage ("")
,	m_bLoadFromLocal (true)
,	m_strLoadID (lpszID)
,	m_nPlayCount(0)
{
	char	tmpBuffer[1024];

	GetPrivateProfileString(lpszID, "templateId", "", tmpBuffer, 1024, GetDataPath());
	m_templateId = tmpBuffer;

	GetPrivateProfileString(lpszID, "width", "", tmpBuffer, 1024, GetDataPath());
	m_width = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "height", "", tmpBuffer, 1024, GetDataPath());
	m_height = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "bgColor", "", tmpBuffer, 1024, GetDataPath());
	m_bgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "bgImage", "", tmpBuffer, 1024, GetDataPath());
	m_bgImage = tmpBuffer;

	//
	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}

	//
	CString strPhoneTemplateId, stPhoneFrameId;
//	::GetPhoneConfig(strPhoneTemplateId, stPhoneFrameId, nPortNo);

	if(strPhoneTemplateId == m_templateId.c_str())
	{
		m_pPhoneTemplate = this;
		m_bPhoneTemplate = true;
	}
	else
		m_bPhoneTemplate = false;
}

CTemplate::~CTemplate()
{

	if(CTemplate::m_pPhoneTemplate == this)
		CTemplate::m_pPhoneTemplate = NULL;
}


BEGIN_MESSAGE_MAP(CTemplate, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_DESTROY()

//	ON_MESSAGE(ID_PLAY_NEXT_SCHEDULE, OnPlayNextSchedule)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()



// CTemplate 메시지 처리기입니다.



int CTemplate::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	//
	CString m_strBGImage, LocalTempFullPath;
//	GetLocalPath(m_strBGImage, LocalTempFullPath, "", m_bgImage.c_str());

	//
	double xscale=1.0f, yscale=1.0f;
//	CHost::GetScale(xscale, yscale);
	if(xscale == 0.0f && yscale == 0.0f)
	{
		int scr_width = GetSystemMetrics(SM_CXSCREEN);
		int scr_height = GetSystemMetrics(SM_CYSCREEN);


//		if(ciArgParser::getInstance()->isSet("+aspectratio"))
		{
			int width, height;
			width = scr_width;
			height = m_width * scr_height / scr_width;
			if(height < m_height)
			{
				height = scr_height;
				width = m_height * scr_width / scr_height;

				xscale = ((double)scr_height) / ((double)m_height);
			}
			else
			{
				xscale = ((double)scr_width) / ((double)m_width);
			}
			yscale = xscale;
//			CHost::SetScale(xscale, yscale);	// 템플릿 크기가 다른경우 개별설정 위해 제거
		}
//		else
//		{
//			xscale = ((double)scr_width) / ((double)m_width);
//			yscale = ((double)scr_height) / ((double)m_height);
////			CHost::SetScale(xscale, yscale);	// 템플릿 크기가 다른경우 개별설정 위해 제거
//		}
	}
	m_fXScale = xscale;
	m_fYScale = yscale;

	//
//	CHost::GetTemplateXY(m_nTemplateX, m_nTemplateY);
	if(m_nTemplateX == INT_MAX)
	{
		int scr_width = GetSystemMetrics(SM_CXSCREEN);

		m_nTemplateX = (scr_width/2) - ( (m_width * xscale) / 2.0f );

		if(m_nTemplateX < 0) m_nTemplateX = 0;
	}
	if(m_nTemplateY == INT_MAX)
	{
		int scr_height = GetSystemMetrics(SM_CYSCREEN);

		m_nTemplateY = (scr_height/2) - ( (m_height * yscale) / 2.0f );

		if(m_nTemplateY < 0) m_nTemplateY = 0;
	}
	//CHost::SetTemplateXY(x, y);	// 템플릿 크기가 다른경우 개별설정 위해 제거
	MoveWindow(m_nTemplateX, m_nTemplateY, m_width * xscale, m_height * yscale);

	//
	if(m_bLoadFromLocal)
	{
		LoadFrame();
		m_bLoadFromLocal = false;
	}
	else
	{
//		InitFrame();
	}


	return 0;
}

void CTemplate::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	CRect rect;
	GetClientRect(rect);

	dc.FillSolidRect(rect, m_rgbBgColor);

	if(m_bgImg.IsValid())
	{
		m_bgImg.Draw(dc.m_hDC, rect);
	}
}

void CTemplate::OnDestroy()
{

	m_mutex.Lock();

	KillTimer(TEMPLATE_SHOW_ID);
	KillTimer(TEMPLATE_HIDE_ID);
	KillTimer(FRAME_CHECK_ID);

	POSITION pos = m_mapFrame.GetStartPosition();
	if(pos == NULL)
	{
	}
	else
	{
		while(pos != NULL)
		{
			CString frameId;
			CFrame* frame;
			m_mapFrame.GetNextAssoc( pos, frameId, (void*&)frame );

			frame->DestroyWindow();
			delete frame;
		}
		m_mapFrame.RemoveAll();
	}
	m_mutex.Unlock();

	CWnd::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

}


bool CTemplate::OpenFile()
{

	//
	if(m_strBGImage.GetLength() != 0 && m_bgImg.Load(m_strBGImage) == false)
	{
	}

	//
	m_mutex.Lock();

	POSITION pos = m_mapFrame.GetStartPosition();
	if(pos == NULL)
	{
	}
	else
	{
		while(pos != NULL)
		{
			CString frameId;
			CFrame* frame;
			m_mapFrame.GetNextAssoc( pos, frameId, (void*&)frame );

			frame->OpenFile();
		}
	}
	m_mutex.Unlock();


	return true;
}

bool CTemplate::IsBetween(bool bDefaultSchedule)
{
	bool between = false;

	m_mutex.Lock();

	POSITION pos = m_mapFrame.GetStartPosition();
	while(pos != NULL)
	{
		CString frameId;
		CFrame* frame;
		m_mapFrame.GetNextAssoc( pos, frameId, (void*&)frame );

		between |= frame->IsBetween(bDefaultSchedule);
	}

	m_mutex.Unlock();

	return between;
}

void CTemplate::Play()
{
	KillTimer(TEMPLATE_HIDE_ID);
	SetTimer(TEMPLATE_SHOW_ID, TEMPLATE_SHOW_TIME, NULL);

	int count = m_mapFrame.GetCount();
	if(count > 0)
	{
		OnTimer(FRAME_CHECK_ID); // 즉시 play
		SetTimer(FRAME_CHECK_ID, FRAME_CHECK_TIME, NULL);
	}
}

void CTemplate::Stop()
{
	m_mutex.Lock();

	KillTimer(FRAME_CHECK_ID);
	KillTimer(TEMPLATE_SHOW_ID);
	SetTimer(TEMPLATE_HIDE_ID, TEMPLATE_HIDE_TIME, NULL);

	POSITION pos = m_mapFrame.GetStartPosition();
	if(pos == NULL)
	{
	}
	else
	{
		while(pos != NULL)
		{
			CString frameId;
			CFrame* frame;
			m_mapFrame.GetNextAssoc( pos, frameId, (void*&)frame );

			frame->Stop();
		}
	}
	m_mutex.Unlock();
}


void CTemplate::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case TEMPLATE_SHOW_ID:
		KillTimer(TEMPLATE_SHOW_ID);
		ShowWindow(SW_SHOW);
		break;

	case TEMPLATE_HIDE_ID:
		KillTimer(TEMPLATE_HIDE_ID);
		ShowWindow(SW_HIDE);
		break;

	case FRAME_CHECK_ID:
		m_mutex.Lock();

		KillTimer(FRAME_CHECK_ID);

		POSITION pos = m_mapFrame.GetStartPosition();
		while(pos != NULL)
		{
			CString frameId;
			CFrame* frame;
			m_mapFrame.GetNextAssoc( pos, frameId, (void*&)frame );

			if( frame->IsBetween(false) )
			{
				if( frame->IsCurrentPlay() == false )
					frame->Play();
			}
			else if( frame->IsBetween(true) )
			{
				if( frame->IsCurrentPlay() == false )
					frame->Play();
			}
			else
				frame->Stop();
		}

		m_mutex.Unlock();

		SetTimer(FRAME_CHECK_ID, FRAME_CHECK_TIME, NULL);

		break;
	}
}

CString	CTemplate::ToString()
{
	m_mutexPhone.Lock();

	CString str;
	str.Format(
		"templateId = %s\r\n"
		"width = %d\r\n"
		"height = %d\r\n"
		"bgColor = %s\r\n"
		"bgImage = %s\r\n"
		"\r\n"
		"m_bPhoneTemplate = %d\r\n"
		"m_strBGImage = %s\r\n"
		"m_PhoneTemplate = %s\r\n"
		,	m_templateId.c_str()
		,	m_width
		,	m_height
		,	m_bgColor.c_str()
		,	m_bgImage.c_str()

		,	m_bPhoneTemplate
		,	m_strBGImage
		,	( m_pPhoneTemplate == NULL ? "NULL" : m_pPhoneTemplate->GetTemplateId() )

	);

	m_mutexPhone.Unlock();

	return str;
}

bool CTemplate::LoadFrame()
{
	char	tmpBuffer[1024];

	GetPrivateProfileString(m_strLoadID, "FrameList", "", tmpBuffer, 1024, GetDataPath());
	CString frame_list = tmpBuffer;

	//
	CFrameArray	listPIPFrame;

	int pos = 0;
	CString token = frame_list.Tokenize(",", pos);
	while(token != "")
	{
		CString strErrorMessage;
		CFrame* frame = CFrame::GetFrameObject(this, token, strErrorMessage);
		if(frame != NULL)
		{
			if(frame->IsPIP())
			{
				listPIPFrame.Add(frame);
			}
			else
			{
				m_mutex.Lock();

				m_mapFrame.SetAt(frame->GetFrameId(), frame);

				if( m_bPhoneTemplate && frame->GetGrade() == 1)
				{
					CFrame::m_mutexPhone.Lock();

					CFrame::m_pPhoneFrame = frame;

					CFrame::m_mutexPhone.Unlock();
				}

				m_mutex.Unlock();
			}
		}
		else
		{
			strErrorMessage.Insert(0, "\t");
		}

		token = frame_list.Tokenize(",", pos);
	}

	// create PIP frame
	int count = listPIPFrame.GetCount();
	if(count > 0)
	{
		for(int i=0; i<count; i++)
		{
			CFrame*	frame = listPIPFrame[i];
			CString strFrameId = frame->GetFrameId();

			CString strErrorMessage;
			frame = CFrame::CreatePIPFrameObject(this, frame, strErrorMessage);
			if(frame)
			{

				m_mutex.Lock();

				POSITION pos = m_mapFrame.GetStartPosition();
				if(pos == NULL)
				{
				}
				else
				{
					while(pos != NULL)
					{
						CString frameId;
						CFrame* frm;
						m_mapFrame.GetNextAssoc( pos, frameId, (void*&)frm );

						if(frm->FrameInFrame(frame))
						{
						}
					}
				}
				m_mapFrame.SetAt(frame->GetFrameId(), frame);

				m_mutex.Unlock();
			}
			else
			{
				strErrorMessage.Insert(0, "\t");
			}
		}
	}

	return true;
}

bool CTemplate::Save()
{
	WritePrivateProfileString(m_strLoadID, "templateId", m_templateId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "width", ::ToString(m_width), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "height", ::ToString(m_height), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "bgColor", m_bgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "bgImage", m_bgImage.c_str(), GetDataPath());

	//
	m_mutex.Lock();

	CString str_frame_list;

	POSITION pos = m_mapFrame.GetStartPosition();
	if(pos == NULL)
	{
	}
	else
	{
		while(pos != NULL)
		{
			CString frameId;
			CFrame* frame;
			m_mapFrame.GetNextAssoc( pos, frameId, (void*&)frame );

			frame->Save();

			if(str_frame_list.GetLength() != 0)
				str_frame_list.Append(",");

			str_frame_list.Append(frame->GetLoadID());
		}
	}

	m_mutex.Unlock();

	WritePrivateProfileString(m_strLoadID, "FrameList", str_frame_list, GetDataPath());

	return true;
}

LRESULT CTemplate::OnPlayNextSchedule(WPARAM wParam, LPARAM lParam)
{
	m_mutex.Lock();

	POSITION pos = m_mapFrame.GetStartPosition();
	if(pos == NULL)
	{
	}
	else
	{
		while(pos != NULL)
		{
			CString frameId;
			CFrame* frame;
			m_mapFrame.GetNextAssoc( pos, frameId, (void*&)frame );

			if( frame->GetGrade() == 1)
			{
//				frame->PostMessage(ID_PLAY_NEXT_SCHEDULE);
				break;
			}
		}
	}

	m_mutex.Unlock();

	return 0;
}

void CTemplate::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnMouseMove(nFlags, point);

//	::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
}
