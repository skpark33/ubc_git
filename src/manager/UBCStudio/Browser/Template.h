#pragma once

#include "Frame.h"
#include "ximage.h"

// CTemplate

class CTemplate : public CWnd
{
	DECLARE_DYNAMIC(CTemplate)

public:
	static	CTemplate*	GetTemplateObject(CWnd* pParent, LPCSTR lpszID, CString& strErrorMessage);

	CTemplate(CWnd* pParent, LPCSTR lpszID);
	virtual ~CTemplate();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();

	afx_msg LRESULT OnPlayNextSchedule(WPARAM wParam, LPARAM lParam);

protected:
	DECLARE_MESSAGE_MAP()

	// frame attributes
	ciString	m_templateId;
	ciShort		m_width;
	ciShort		m_height;
	ciString	m_bgColor;
	ciString	m_bgImage;

	//
	bool		m_bPhoneTemplate;
	COLORREF	m_rgbBgColor;
	CFrameMap	m_mapFrame;

	CString		m_strBGImage;
	CxImage		m_bgImg;

	double		m_fXScale;
	double		m_fYScale;

	int			m_nTemplateX;
	int			m_nTemplateY;

	static UINT	m_nTemplateID;

	CMutex		m_mutex;

	CWnd*		m_pParentWnd;

	CString		m_strErrorMessage;

	bool		m_bLoadFromLocal;
	CString		m_strLoadID;

	int			m_nPlayCount;

public:

	static	CTemplate*	m_pPhoneTemplate;
	static	CMutex		m_mutexPhone;

	bool	LoadFrame();
	bool	OpenFile();
	bool	IsPhoneTemplate() { return m_bPhoneTemplate; };
	bool	IsBetween(bool bDefaultSchedule);
	void	Play();
	void	Stop();

	LPCSTR	GetTemplateId() { return m_templateId.c_str(); };

	void	GetScale(double& fXScale, double& fYScale) { fXScale = m_fXScale; fYScale = m_fYScale; };

	LPCSTR	GetLoadID() { return m_strLoadID; };
	bool	Save();

	CString	m_debugString;
	virtual CString	ToString();

	int		GetPlayCount() { return m_nPlayCount; };
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};


typedef CArray<CTemplate*, CTemplate*> CTemplateArray;
typedef CMapStringToPtr		CTemplateMap;


