// FlashEventSink.cpp: implementation of the CFlashEventSink class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FlashEventSink.h"
#include "Schedule.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CFlashEventSink::CFlashEventSink(void* pParent)
:	m_refCount(0)
,	m_pParent(NULL)
{	
	m_pParent	= pParent;
}

CFlashEventSink::~CFlashEventSink()
{

}


/******************************************************************************
*   IUnknown Interfaces -- All COM objects must implement, either directly or 
*   indirectly, the IUnknown interface.
******************************************************************************/
STDMETHODIMP CFlashEventSink::QueryInterface(REFIID riid, void ** ppvObj)
{
	if (riid == IID_IUnknown)
	{
		*ppvObj = static_cast<_IShockwaveFlashEvents*>(this);
	}	
	else if (riid == DIID__IShockwaveFlashEvents)
	{	
		*ppvObj = static_cast<_IShockwaveFlashEvents*>(this);
	}
	else if (riid == IID_IDispatch)
	{	
		*ppvObj = static_cast<IDispatch*>(this);
	}	
	else
	{
	    char clsidStr[256];
	    WCHAR wClsidStr[256];
	    char txt[512];

	    StringFromGUID2(riid, (LPOLESTR)&wClsidStr, 256);
	    // Convert down to ANSI
	    WideCharToMultiByte(CP_ACP, 0, wClsidStr, -1, clsidStr, 256, NULL, NULL);
	    sprintf(txt, "riid is : %s: Unsupported Interface", clsidStr);

		*ppvObj = NULL;
		return E_NOINTERFACE;
	}
	
	static_cast<IUnknown*>(*ppvObj)->AddRef();
	return S_OK;
}

STDMETHODIMP_(ULONG) CFlashEventSink::AddRef()
{
	return ++m_refCount;
}

STDMETHODIMP_(ULONG) CFlashEventSink::Release()
{
	if (--m_refCount == 0)
	{
		delete this;
		return 0;
	}
	return m_refCount;
}

/******************************************************************************
*   IDispatch Interface -- This interface allows this class to be used as an
*   automation server, allowing its functions to be called by other COM
*   objects.  NOT USED IN THIS DEMO
******************************************************************************/
STDMETHODIMP CFlashEventSink::GetTypeInfoCount(UINT *iTInfo)
{
   return E_NOTIMPL;
}

STDMETHODIMP CFlashEventSink::GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo)
{
   return E_NOTIMPL;
}

STDMETHODIMP CFlashEventSink::GetIDsOfNames(REFIID riid,  
                                         OLECHAR **rgszNames, 
                                         UINT cNames,  LCID lcid,
                                         DISPID *rgDispId)
{
   HRESULT hr = E_FAIL;
   return hr;
}


STDMETHODIMP CFlashEventSink::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid,
                                  WORD wFlags, DISPPARAMS* pDispParams,
                                  VARIANT* pVarResult, EXCEPINFO* pExcepInfo,
                                  UINT* puArgErr)
{
   if ((riid != IID_NULL))
      return E_INVALIDARG;
   
   HRESULT hr = S_OK;  // Initialize

   __DEBUG__("dispIdMember", dispIdMember);

   switch (dispIdMember)
   {
   case 150:  // fscommand
	   {
		   // 플래쉬 위자드에서 넘어오는 형태 (생성여부, 콘텐츠 Id)
		   // 생성여부 (0 : 새로만드는 플래쉬 -> XML 데이터를 넘겨줘야함, 1: 이미 만들어진 플래쉬 -> XML 데이터를 넘겨주지말아야 함)
		   // pDispParams->rgvarg[1].bstrVal -> fscommand의 첫번째 파라메터
		   // pDispParams->rgvarg[0].bstrVal -> fscommand의 두번째 파라메터
		   CString strContentsId(pDispParams->rgvarg[0].bstrVal);
		   __DEBUG__("Recv fscommand[0]", strContentsId);

		   CString strIsCreated(pDispParams->rgvarg[1].bstrVal);
		   __DEBUG__("Recv fscommand[1]", strIsCreated);

		   ((CWizardSchedule*)m_pParent)->RecvFsCommand(strContentsId, strIsCreated);
	   }
	   break;

   case 197:  // FlashCall
	   {
		   // pDispParams->rgvarg->bstrVal; -> 플래시쪽에서 넘어온 이벤트 데이타(XML)
		   __DEBUG__("Recv FlashCall", _NULL);
	   }
	   break;
   }//switch
 
   return hr;
}

/******************************************************************************
*   EApplication Interface 
/*************************************************************************/

