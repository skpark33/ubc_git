#pragma once

class CContentsInfoRef
{
protected:
	CContentsInfoRef(void);
	virtual ~CContentsInfoRef(void);

	static CContentsInfoRef* _instance;
	static CCriticalSection	_cs;

public:
	static		CContentsInfoRef* getInstance();
	static void clearInstance();

	CString		GetWizardXml(CString strContentsId);
};
