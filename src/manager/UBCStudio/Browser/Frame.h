#pragma once

#include "Schedule.h"

// CFrame
class CTemplate;

class CFrame : public CWnd
{
	DECLARE_DYNAMIC(CFrame)

public:
	static	CFrame*	GetFrameObject(CTemplate* pParent, LPCSTR lpszID, CString& strErrorMessage);
	static	CFrame*	CreatePIPFrameObject(CTemplate* pParent, CFrame* pFrame, CString& strErrorMessage);

	CFrame(CTemplate* pParentWnd, LPCSTR lpszID);
	virtual ~CFrame();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();

	afx_msg LRESULT OnPlayNextSchedule(WPARAM wParam, LPARAM lParam);

protected:
	DECLARE_MESSAGE_MAP()

	//attribute
	ciString	m_templateId;
	ciString	m_frameId;
	ciShort		m_grade;
	ciShort		m_width;
	ciShort		m_height;
	ciShort		m_x;
	ciShort		m_y;
	ciBoolean	m_isPIP;
	ciString	m_borderStyle;
	ciShort		m_borderThickness;
	ciString	m_borderColor;
	ciShort		m_cornerRadius;

	COLORREF	m_rgbBorderColor;

	//
	static UINT	m_nFrameID;

	bool		m_bCurrentPlay;

	int			m_nCurrentScheduleIndex;
	int			m_nCurrentDefaultScheduleIndex;
	CSchedule*	m_pCurrentSchedule;

	void		DrawSquareFrame(CDC* pDC);
	void		DrawRoundFrame(CDC* pDC);

	void		PlayNextSchedule();
	bool		GetNextSchedule(int& index);
	bool		GetNextDefaultSchedule(int& index);

	bool		SortDefaultSchedule();

	CScheduleArray	m_listSchedule;
	CScheduleArray	m_listDefaultSchedule;
	CScheduleMap	m_mapSchedule;

	CMutex		m_mutex;

	CTemplate*	m_pParentWnd;

	CFrame*		m_pPIPParentWnd;
	void		SetClipRgn(int x, int y, int width, int height);
	void		ResetClipRgn();

	CString		m_strErrorMessage;

	bool		m_bLoadFromLocal;
	CString		m_strLoadID;

	int			m_nPlayCount;

public:

	static	CFrame*	m_pPhoneFrame;
	static	CMutex	m_mutexPhone;

	LPCSTR	GetFrameId() { return m_frameId.c_str(); };
	int 	GetGrade() { return m_grade; };
	bool	IsCurrentPlay() { return m_bCurrentPlay; };
	bool	IsPIP() { return m_isPIP; };
	bool	LoadSchedule(bool bDefaultSchedule);
	bool	CheckSchedule();
	bool	OpenFile();
	bool	IsBetween(bool bDefaultSchedule);
	void	Play();
	void	Stop();

	bool	FrameInFrame(CFrame* frame);
	void	SetPIPParentWnd(CFrame* pWnd) { m_pPIPParentWnd = pWnd; };

	LPCSTR	GetLoadID() { return m_strLoadID; };
	bool	Save();

	CString	m_debugString;
	virtual CString	ToString();

	void	SetPlayCount(int count) { m_nPlayCount = count; };
	void	ResetPlayCount();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};

typedef CArray<CFrame*, CFrame*>	CFrameArray;
typedef CMapStringToPtr				CFrameMap;


