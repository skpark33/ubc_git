// Schedule.cpp : 구현 파일입니다.
//

#include "stdafx.h"

#include <afxinet.h>

//#include "UBCStudio.h"
//#include "Host.h"
#include "Template.h"
#include "Frame.h"
#include "Schedule.h"
#include "VertDraw.h"
#include "..\MemDC.h"
#include <float.h>
#include "ContentsInfoRef.h"

//#include "..\VideoChat\VideoView.h"
#include "io.h"
//#include "Mshtml.h"

//#define		USE_MMTIMER		// 멀티미디어 타이머 사용


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSchedule

#define		BUFFER_SIZE					32768
#define		TIMER_MINIMUM_TIME			15
#define		VIDEO_CAPTION_SHIFT_STEP	2
#define		HORIZONTAL_TICKER_CAPTION_SHIFT_STEP	1
#define		VERTICAL_TICKER_CAPTION_SHIFT_STEP		1

#define		SCHEDULE_SHOW_ID			4000	// id
#define		SCHEDULE_SHOW_TIME			100		// time(ms)

#define		SCHEDULE_HIDE_ID			4001	// id
#define		SCHEDULE_HIDE_TIME			100		// time(ms)

#define		CAPTION_SHIFT_ID			4002	// id
#define		CAPTION_SHIFT_TIME			25		// time(ms)

#define		REFRESH_CONTENTS_ID			4003	// id
#define		REFRESH_CONTENTS_TIME		(60*60*1000)		// time(ms)

#define		CAPTION_NEXT_ID				4004	// id
#define		CAPTION_NEXT_TIME			(3*1000)// time(ms) -> 3초후에 다음 항목으로 이동

#define		CAPTION_CLEAR_ID			4005	// id
#define		CAPTION_CLEAR_TIME			(1*1000)// time(ms) -> 1초간 빈화면

#define		BUF_SIZE					(1024*100)	//ini 파일에서 읽는 버퍼 사이즈				
#define		ID_SLIDE_SHOW_END			4010		//슬라이드 쇼 종료 타이머 ID
#define		TIME_SLIDE_SHOW_END			5000		//슬라이드 쇼 종료 타이머 시간

#define		ID_PRESENTAION_END			4011		//Presentation 종료 타이머 ID
#define		TIME_PRESENTAION_END		1000		//Presentation 종료 타이머 시간

#define		ID_PRESENTAION_SHOW			4012		//Presentation Show 타이머 ID
#define		TIME_PRESENTAION_SHOW		5000		//Presentation Show 타이머 시간

#define		ID_REDRAW_SCHEDULE			4013		//Schedule redraw 타이머 ID
#define		TIME_REDRAW_SCHEDULE		250			//Schedule redraw 타이머 시간

#define		ID_VIDEO_REPAINT			5000		//VMR 사용시에 비디오 영역을 다시 그리는 타이머 ID
#define		TIME_VIDEO_REPAINT			500			//VMR 사용시에 비디오 영역을 다시 그리는 타이머 시간

#define		TIME_PPT_PLAYING			(87)			//PPT Schedule의 Play time	
#define		TIME_PPT_NONE				(10)			//PPT가 설치되지 않았을 때 Play time

enum E_RSS_TYPE
{
	E_RSS_ONE_LINE = 0,		//단일 라인 RSS
	E_RSS_MULTI_LINE,		//Multi line RSS
	E_RSS_DETAIL			//내용이 있는 RSS
};

UINT	CSchedule::m_nScheduleID = 0xf000;
UINT	CProgressBAR::m_nProgressBARID = 0xe000;

CMapStringToPtr CSchedule::m_mapSchedule;
CSchedule*	CSchedule::m_pPhoneSchedule = NULL;
CCriticalSection CSchedule::m_csSchedulePhone;

CMapStringToString CFlashSchedule::g_mapFlashObject;


CSchedule* CSchedule::GetScheduleObject(CFrame* pParent, LPCSTR lpszID, CRect& rect, bool bDefaultSchedule, CString& strErrorMessage)
{
	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(lpszID, "contentsType", "", tmpBuffer, BUF_SIZE, GetDataPath());
	ciLong	contentsType = atoi(tmpBuffer);

	switch(contentsType)
	{
	case CONTENTS_VIDEO: // Video
		{
			CVideoSchedule* schedule = new CVideoSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "VideoSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Video Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_SMS: // SMS
		{
			// 임시 막아둠 (SMS -> HorizontalTicker)

			//CSMSSchedule* schedule = new CSMSSchedule(pParentParent, pParent, reply, bDefaultSchedule);
			//if(schedule == NULL) return NULL;
			//BOOL ret = schedule->Create(NULL, "SMSSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			//if(ret == NULL)
			//{
			//	strErrorMessage = "Fail to create SMS Schedule !!!";
			//	delete schedule;
			//	schedule = NULL;
			//}
			//return schedule;

			CHorizontalTickerSchedule* schedule = new CHorizontalTickerSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "HorizontalTickerSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create HorizontalTicker Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_IMAGE: // Image
		{
			CPictureSchedule* schedule = new CPictureSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "ImageSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Image Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_PROMOTION: // Campaigne (집계 현황)
		{
			CPromotionSchedule* schedule = new CPromotionSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "PromitionSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Promition Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_TV: // TV
		break;

	case CONTENTS_TICKER: // Ticker
		{
			CVerticalTickerSchedule* schedule = new CVerticalTickerSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "VerticalTickerSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create VerticalTicker Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_PHONE: // Phone (화상통화)
		{
			CPhoneSchedule* schedule = new CPhoneSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "PhoneSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Phone Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_WEBBROWSER: // URL WebBrowser (URL 직접navigate)
		{
			CURLSchedule* schedule = new CURLSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "URLSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create URL Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_FLASH: // FLASH
	case CONTENTS_FLASH_TEMPLATE:
	case CONTENTS_DYNAMIC_FLASH:
		{
			CFlashSchedule* schedule = new CFlashSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "FlashSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Flash Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_WEBCAM: // Web Cam
		{
			CWebCamSchedule* schedule = new CWebCamSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "WebcamSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Webcam Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_RSS:
		{
			CRSSSchedule* schedule = new CRSSSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "RSSSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create RSS Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_TEXT:
		{
			CSMSSchedule* schedule = new CSMSSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "SMSSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create SMS Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;

	case CONTENTS_TYPING:
		{
			CTypingTickerSchedule* schedule = new CTypingTickerSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "TypingTickerSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create TypingTicker Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;
	case CONTENTS_PPT:		//MS-Office Power Point
		{
			CPowerPointSchedule* schedule = new CPowerPointSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "PowerPointSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Power Point Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}//case
		break;
	case CONTENTS_WIZARD:
		{
			CWizardSchedule* schedule = new CWizardSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "WizardSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create RSS Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}
		break;
/*
	case CONTENTS_HWP:		//Hwp
		{
			CHwpSchedule* schedule = new CHwpSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "HwpSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Hwp Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}//case
		break;
	case CONTENTS_EXCEL:	//Excel
		{
			CExcelSchedule* schedule = new CExcelSchedule(pParent, lpszID, bDefaultSchedule);
			if(schedule == NULL) return NULL;
			BOOL ret = schedule->Create(NULL, "ExcelSchedule", WS_CHILD, rect, pParent, m_nScheduleID--);
			if(ret == NULL)
			{
				strErrorMessage = "Fail to create Excel Schedule !!!";
				delete schedule;
				schedule = NULL;
			}
			return schedule;
		}//case
		break;
*/
	default:
		strErrorMessage = "Uknown ContentsType Contents !!!";
		break;
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////////

CSchedule::CSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	m_bDefaultSchedule(bDefaultSchedule)
,	m_bOpen(false)
,	m_bMute(false)
,	m_bPlay(false)
,	m_pParentWnd(pParentWnd)
,	m_strMediaFullPath ("")
,	m_strErrorMessage ("")
,	m_bLoadFromLocal (true)
,	m_strLoadID (lpszID)
,	m_nPlayCount (0)

,	m_siteId ("00")
,	m_hostId ("00")
,	m_templateId ("00")
,	m_frameId ("00")
,	m_scheduleId ("00")
,	m_startTime ("00:00:00")
,	m_endTime ("00:00:01")
,	m_playOrder (0)
,	m_timeScope (0)
,	m_openningFlag (ciFalse)
,	m_priority (0)
,	m_castingState (0)
,	m_operationalState (0)
,	m_adminState (ciFalse)
,	m_parentScheduleId ("")
,	m_contentsId ("00")
,	m_contentsName ("")
,	m_contentsType (0)
,	m_contentsState (0)
,	m_location ("")
,	m_filename ("")
,	m_volume (0)
,	m_runningTime (1)
,	m_dwStartTick(0)
{
	SetSchedule(lpszID);

	CSchedule::m_mapSchedule.SetAt(m_scheduleId.c_str(), this);
}

CSchedule::~CSchedule()
{
	CSchedule::m_mapSchedule.RemoveKey(m_scheduleId.c_str());
}

IMPLEMENT_DYNAMIC(CSchedule, CWnd)

BEGIN_MESSAGE_MAP(CSchedule, CWnd)
	ON_WM_TIMER()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_HIDE_CONTENTS, OnHideContents)
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	//ON_MESSAGE(WM_INVLIDATE_FRAME, OnInvalidateFrame)	
END_MESSAGE_MAP()


void CSchedule::OnDestroy()
{
	//__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	CloseFile();

	CWnd::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	//__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
}
void CSchedule::SetSchedule(LPCSTR lpszID)
{
	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(lpszID, "siteId", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_siteId = tmpBuffer;

	GetPrivateProfileString(lpszID, "hostId", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_hostId = tmpBuffer;

	GetPrivateProfileString(lpszID, "templateId", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_templateId = tmpBuffer;

	GetPrivateProfileString(lpszID, "frameId", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_frameId = tmpBuffer;

	GetPrivateProfileString(lpszID, "scheduleId", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_scheduleId = tmpBuffer;

	GetPrivateProfileString(lpszID, "startDate", "", tmpBuffer, BUF_SIZE, GetDataPath());
	time_t tmTmp = atoi(tmpBuffer);
	m_startDate = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "endDate", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_endDate = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "startTime", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_startTime = tmpBuffer;

	GetPrivateProfileString(lpszID, "endTime", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_endTime = tmpBuffer;

	GetPrivateProfileString(lpszID, "playOrder", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_playOrder = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "timeScope", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_timeScope = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "openningFlag", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_openningFlag = (bool)atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "priority", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_priority = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "castingState", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_castingState = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "operationalState", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_operationalState = (bool)atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "adminState", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_adminState = (bool)atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "parentScheduleId", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_parentScheduleId = tmpBuffer;

	GetPrivateProfileString(lpszID, "contentsId", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_contentsId = tmpBuffer;

	GetPrivateProfileString(lpszID, "contentsName", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_contentsName = tmpBuffer;

	GetPrivateProfileString(lpszID, "contentsType", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_contentsType = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "contentsState", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_contentsState = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "location", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_location = tmpBuffer;
	
	int nLength = m_location.length();
	if(nLength != 0 && m_location[nLength-1] != '/' && m_location[nLength-1] != '\\')
	{
		m_location += '/';
	}//if

	GetPrivateProfileString(lpszID, "filename", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_filename = tmpBuffer;

	GetPrivateProfileString(lpszID, "volume", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_volume = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "runningTime", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_runningTime = atol(tmpBuffer);

	CString key, strTmp, strSpace;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		key.Format("comment%d", i+1);
		GetPrivateProfileString(lpszID, key, "", tmpBuffer, BUF_SIZE, GetDataPath());
		strTmp = tmpBuffer;
		strSpace = strTmp.Left(2);
		if(strSpace == "| ")	//첫번째 문자가 공백일경우 ini에서 읽을때 사라지므로 "|"를 붙였다.
		{
			strTmp.Delete(0, 1);
		}//if
		m_comment[i] = strTmp;
	}//for

	//
	int nY = m_startDate.GetYear();
	nY = (nY <= 1970 ? 1971 : nY);		//1970 을 그대로 CTime에 넣으면 오류발생

	int nM = m_startDate.GetMonth();
	nM = (nM == 0 ? 1 : nM);			//0 을 그대로 CTime에 넣으면 오류발생

	int nD = m_startDate.GetDay();
	nD = (nD == 0 ? 1 : nD);			//0 을 그대로 CTime에 넣으면 오류발생

	CTime start_date(nY, nM, nD, 0, 0, 0);
	m_timeStartDate = start_date;


	nY = m_endDate.GetYear();
	nY = (nY <= 1970 ? 1971 : nY);		//1970 을 그대로 CTime에 넣으면 오류발생

	nM = m_endDate.GetMonth();
	nM = (nM == 0 ? 1 : nM);			//0 을 그대로 CTime에 넣으면 오류발생

	nD = m_endDate.GetDay();
	nD = (nD == 0 ? 1 : nD);			//0 을 그대로 CTime에 넣으면 오류발생

	CTime end_date(nY, nM, nD, 23, 59, 59);
	m_timeEndDate = end_date;

	m_strStartTime = m_startTime.c_str();
	m_strEndTime = m_endTime.c_str();
}


bool CSchedule::IsUseFile()
{
	if(	m_contentsType == 4		// TV 제외
	||	m_contentsType == 7		// URL 제외
		)
		return false;

	return true;
}

bool CSchedule::IsBetween()
{
	if(m_bOpen == false) return false;
	if(m_adminState == false) return false;
	if(m_castingState != SCH_READY) return false;
	if(m_contentsState != CON_READY) return false;

	CTime current_time = CTime::GetCurrentTime();

	if(m_bDefaultSchedule)
	{
		if(m_parentScheduleId.length() > 0)
		{
			CSchedule* schedule;
			if(CSchedule::m_mapSchedule.Lookup(m_parentScheduleId.c_str(), (void*&)schedule))
			{
				return schedule->IsPlay();
			}
			return false;
		}
		else if(m_timeStartDate <= current_time && current_time <= m_timeEndDate)
		{
			int hour = current_time.GetHour();

			switch(m_timeScope)
			{
			case 0: // 24
				return true;
				break;

			case 7: // 0-7
				if(0 <= hour && hour < 7)
					return true;
				break;

			case 1: // 7-9
				if(7 <= hour && hour < 9)
					return true;
				break;

			case 2: // 9-12
				if(9 <= hour && hour < 12)
					return true;
				break;

			case 3: // 12-13
				if(12 <= hour && hour < 13)
					return true;
				break;

			case 4: // 13-18
				if(13 <= hour && hour < 18)
					return true;
				break;

			case 5: // 18-22
				if(18 <= hour && hour < 22)
					return true;
				break;

			case 6: // 22-24
				if(22 <= hour)
					return true;
				break;
			}
		}
	}
	else
	{
		if(m_timeStartDate <= current_time && current_time <= m_timeEndDate)
		{
			CString str_currenttime = current_time.Format("%H:%M:%S");

			if(m_strStartTime < m_strEndTime)
			{
				if(m_strStartTime <= str_currenttime && str_currenttime <= m_strEndTime)
					return true;
			}
			else
			{
				if(m_strStartTime <= str_currenttime || str_currenttime >= m_strEndTime)
					return true;
			}
		}
	}

	return false;
}

bool CSchedule::DeleteFile()
{
	CString temp_path;
	try
	{
		if(IsUseFile() && m_strMediaFullPath.GetLength() > 0)
		{
			int i=0;
			CFileStatus fs;
			do
			{
				temp_path.Format("%s.%d.bak", m_strMediaFullPath, i);
				i++;
			}
			//while(_access(temp_path, 00) == 0);
			while(CFile::GetStatus(temp_path, fs));

			CFile::Rename(m_strMediaFullPath, temp_path);
			CFile::Remove(temp_path);
		}

		return true;
	}
	catch(CFileException* ex)
	{
		CString msg;
		TCHAR szCause[1024];
		ex->GetErrorMessage(szCause, 1024);
		msg.Format("(%d) (%s) %s", ex->m_cause, temp_path, szCause);
		ex->Delete();
	}

	return false;
}

bool CSchedule::FileUpdate(LPCSTR lpszContentsId, LPCSTR lpszLocation, LPCSTR lpszFilename)
{
	if(strcmp(m_contentsId.c_str(), lpszContentsId) == 0)
	{
		m_location = lpszLocation;
		m_filename = lpszFilename;

		if(CloseFile())
			DeleteFile();

		return true;
	}
	return false;
}


bool CSchedule::CloseFile()
{
	Stop();

	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if

	return true;
}


bool CSchedule::Play(bool bResume)
{
	m_nPlayCount ++;

	KillTimer(SCHEDULE_HIDE_ID);
	SetTimer(SCHEDULE_SHOW_ID, SCHEDULE_SHOW_TIME, NULL);
	m_bPlay = true;

	return true;
}

bool CSchedule::Stop(bool bHide)
{
	KillTimer(SCHEDULE_SHOW_ID);
	if(bHide)
		SetTimer(SCHEDULE_HIDE_ID, SCHEDULE_HIDE_TIME, NULL);
	m_bPlay = false;

	return true;
}

bool CSchedule::Save()
{
	WritePrivateProfileString(m_strLoadID, "siteId", m_siteId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "hostId", m_hostId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "templateId", m_templateId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "frameId", m_frameId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "scheduleId", m_scheduleId.c_str(), GetDataPath());
//	WritePrivateProfileString(m_strLoadID, "startDate", ::ToString((int)m_startDate.get()), GetDataPath());
//	WritePrivateProfileString(m_strLoadID, "endDate", ::ToString((int)m_endDate.getTime()), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "startTime", m_startTime.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "endTime", m_endTime.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "playOrder", ::ToString(m_playOrder), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "timeScope", ::ToString(m_timeScope), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "openningFlag", ::ToString(m_openningFlag), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "priority", ::ToString(m_priority), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "castingState", ::ToString(m_castingState), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "operationalState", ::ToString(m_operationalState), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "adminState", ::ToString(m_adminState), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "parentScheduleId", m_parentScheduleId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "contentsId", m_contentsId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "contentsType", ::ToString(m_contentsType), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "contentsName", m_contentsName.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "location", m_location.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "filename", m_filename.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "runningTime", ::ToString(m_runningTime), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "contentsState", ::ToString(m_contentsState), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "volume", ::ToString(m_volume), GetDataPath());

	CString key, strTmp;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		key.Format("comment%d", i+1);
		strTmp = m_comment[i].c_str();
		if(strTmp.GetAt(0) == ' ')
		{
			strTmp = "|" + strTmp;
			m_comment[i] = strTmp;
		}//if

		WritePrivateProfileString(m_strLoadID, key, m_comment[i].c_str(), GetDataPath());
	}//if
	
	return true;
}

void CSchedule::OnTimer(UINT_PTR nIDEvent)
{
	CWnd::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case SCHEDULE_SHOW_ID:
		KillTimer(SCHEDULE_SHOW_ID);
		ShowWindow(SW_SHOW);
		break;

	case SCHEDULE_HIDE_ID:
		KillTimer(SCHEDULE_HIDE_ID);
		ShowWindow(SW_HIDE);
		break;
	}
}

void CSchedule::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnRButtonUp(nFlags, point);

//	CString msg;// = ToString();
//	UbcMessageBox(msg);
}

void CSchedule::OnMouseMove(UINT nFlags, CPoint point)
{
	CWnd::OnMouseMove(nFlags, point);
}

void CSchedule::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	
	CWnd::OnLButtonDown(nFlags, point);

	//::AfxGetMainWnd()->PostMessage(WM_LBUTTONDOWN, nFlags, (LPARAM)MAKELONG(point.x, point.y));
}


// Modified by 정운형 2008-11-20 오전 11:58
// 변경내역 :  Template play list 동적구성 작업
LRESULT CSchedule::OnHideContents(WPARAM wParam, LPARAM lParam)
{
	KillTimer(SCHEDULE_SHOW_ID);
	//SetTimer(SCHEDULE_HIDE_ID, SCHEDULE_HIDE_TIME, NULL);
	ShowWindow(SW_HIDE);

	return 0;
}
// Modified by 정운형 2008-11-20 오전 11:58
// 변경내역 :  Template play list 동적구성 작업

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CVideoSchedule

CVideoSchedule::CVideoSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_pInterface (NULL)
,	m_rgbBgColor (0xffffffff)
,	m_rgbFgColor (0xffffffff)
,	m_nTimerId (0)
,	m_bPause(false)
{
	m_nVedioRenderMode = E_WINDOWLESS;
	m_nVedioOpenType = E_OPEN_INIT;
	SetSchedule(lpszID);
}

CVideoSchedule::~CVideoSchedule()
{
}

IMPLEMENT_DYNAMIC(CVideoSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CVideoSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	//ON_MESSAGE(CAPTION_SHIFT_ID, OnCaptionShift)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
END_MESSAGE_MAP()

BOOL CVideoSchedule::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	return TRUE;

	//return CSchedule::OnEraseBkgnd(pDC);
}

void CVideoSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	if(m_nVedioRenderMode == E_WINDOWLESS)
	{
		if(m_pInterface)
		{
			if(!m_pInterface->RePaintVideo())
			{
				__DEBUG__("Video repaint fail", m_strMediaFullPath);
			}//if
		}//if
	}//if
}


int CVideoSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;
/*
	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}
*/
	return 0;
}

void CVideoSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();
}

/*
LRESULT CVideoSchedule::OnCaptionShift(WPARAM wParam, LPARAM lParam)
{
	if(m_bUseCaption)
	{
		m_offset -= VIDEO_CAPTION_SHIFT_STEP;

		CRect client_rect;
		GetClientRect(client_rect);

		CRect rect = client_rect;
		rect.right = m_nCaptionWidth;
		rect.OffsetRect(m_offset, 0);
		if(m_pInterface)
		{
			((CVMR9Interface*)m_pInterface)->UpdateBitmapParams(100, RGB(255, 255, 255), rect);
		}//if

		if(m_offset < -m_nCaptionWidth)
		{
			m_offset = client_rect.Width();
		}//if
	}//if

	return 0;
}
*/

void CVideoSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	char	tmpBuffer[BUF_SIZE];

	CString key;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		key.Format("comment%d", i+1);
		GetPrivateProfileString(lpszID, key, "", tmpBuffer, BUF_SIZE, GetDataPath());
		m_comment[i] = tmpBuffer;
	}

	GetPrivateProfileString(lpszID, "bgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_bgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "fgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "font", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_font = tmpBuffer;

	GetPrivateProfileString(lpszID, "fontSize", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fontSize = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "playSpeed", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_playSpeed = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "soundVolume", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_soundVolume = atoi(tmpBuffer);

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}
/*
	if(	m_comment[0].length() != 0 ||
		m_comment[1].length() != 0 ||
		m_comment[2].length() != 0 ||
		m_comment[3].length() != 0 ||
		m_comment[4].length() != 0 ||
		m_comment[5].length() != 0 ||
		m_comment[6].length() != 0 ||
		m_comment[7].length() != 0 ||
		m_comment[8].length() != 0 ||
		m_comment[9].length() != 0
	)
		m_bUseCaption = true;
	else
		m_bUseCaption = false;
*/
	// 볼륨
	if(m_pInterface)
	{
		m_pInterface->SetVolume(m_soundVolume);
	}//if
}

double CVideoSchedule::GetTotalPlayTime()
{
	REFTIME tm = 0;
	if(m_pInterface)
	{
		tm = m_pInterface->GetTotalPlayTime();
	}//if

	return tm;
}

double CVideoSchedule::GetCurrentPlayTime()
{
	REFTIME tm = 0;
	if(m_pInterface)
	{
		tm = m_pInterface->GetCurrentPlayTime();
	}//if

	return tm;
}

bool CVideoSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	if(nGrade > 1)
	{
		m_bMute = true;
	}

	if(_access(m_strMediaFullPath, 00) != 0)
	{
		return false;
	}

	if(m_nVedioOpenType == E_OPEN_INIT)
	{
		m_bOpen = OpenDSRender();
		if(!m_bOpen)
		{
			//__DEBUG__("Directshow render open faile", _NULL);
			return false;
		}//if
	}
	else
	{
		m_bOpen = true;
	}//if

	return true; 
}

/*
// Modified by 정운형 2009-01-08 오후 3:14
// 변경내역 :  video schedule의 업데이트시에는 캡션파일을 다시 만들어줘야함
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// caption image 파일을 만든다 \n
/////////////////////////////////////////////////////////////////////////////////
void CVideoSchedule::CreateCaptionImage()
{
	if(!m_bUseCaption)
		return;

	if(!m_bOpen)
		return;

	//__DEBUG__("\t\t\tCaption Mode", _NULL);

	CVMR9Interface* pVMR9Interface = (CVMR9Interface*)m_pInterface;
	pVMR9Interface->SetLayerZOrder(0, 0);

	// create caption bmp-file
	CString caption_filename = GetCaptionFilename(m_strMediaFullPath);

	CRect client_rect;
	GetClientRect(client_rect);

	CRect rect = client_rect;

	CDC dc;
	CDC MemDC;
	MemDC.CreateCompatibleDC(&dc);

	//
	double xscale = 1.0, yscale = 1.0;
	//m_pParentParentWnd->GetScale(xscale, yscale);

	// 글자 폭
	CFont font;
	if(m_font.length() > 0)
		font.CreatePointFont(m_fontSize*10 * xscale, m_font.c_str());
	else
		font.CreatePointFont(m_fontSize*10 * xscale, "Tahoma");

	CFont *pOldFont = (CFont*)MemDC.SelectObject(&font);

	CString msg;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		if(m_comment[i].length() == 0)
			continue;
		if(i != 0 && msg.GetLength() > 0)
			msg.Append(" ");
		msg.Append(m_comment[i].c_str());
	}//for

	CSize size = MemDC.GetOutputTextExtent(msg);

	rect.right = size.cx + 10;
	if(rect.right < (client_rect.Width() + 10) )
		rect.right = client_rect.Width() + 10;

	// 비트맵 생성
	CBitmap bitmap;
	{
		int bmp_byte_size = rect.Width() * rect.Height() * 4;
		LPBYTE b = new BYTE[bmp_byte_size];
		::memset(b, 0xff, bmp_byte_size);

		BITMAP bm;
		bm.bmType = 0;
		bm.bmWidth = rect.Width();
		bm.bmHeight = rect.Height();
		bm.bmWidthBytes = rect.Width()*4; // 32bit
		bm.bmPlanes = 1;
		bm.bmBitsPixel = 32;
		bm.bmBits = b;
		BOOL ret = bitmap.CreateBitmapIndirect(&bm);
		delete[]b;
	}
	CBitmap *pOldBitmap = (CBitmap*)MemDC.SelectObject(&bitmap);

	MemDC.FillSolidRect(rect, RGB(255,255,255));

	// 글자 주변 경계
	{
		MemDC.SetBkMode(1); // TRANSPARENT
		if(m_bgColor.length() == 0)
		{
			MemDC.SetTextColor(RGB(0,0,0));
		}
		else
		{
			MemDC.SetTextColor(m_rgbBgColor);
		}//if

		for(int cx=-4; cx<=4; cx++)
			for(int cy=-4; cy<=4; cy++)
			{
				CRect rrrr = rect;
				rrrr.bottom -= rrrr.Height() / 33; // 아래 여백 3%
				rrrr.OffsetRect(cx,cy);
				MemDC.DrawText(msg, rrrr, DT_CENTER | DT_BOTTOM | DT_SINGLELINE);
			}
	}
	// 글자 내부
	{
		MemDC.SetBkMode(1); // TRANSPARENT
		if(m_fgColor.length() == 0 || m_rgbFgColor == RGB(255,255,255))
			MemDC.SetTextColor(RGB(254,254,254));
		else
			MemDC.SetTextColor(m_rgbFgColor);
		for(int cx=-1; cx<=1; cx++)
			for(int cy=-1; cy<=1; cy++)
			{
				CRect rrrr = rect;
				rrrr.bottom -= rrrr.Height() / 33; // 아래 여백 3%
				rrrr.OffsetRect(cx,cy);
				MemDC.DrawText(msg, rrrr, DT_CENTER | DT_BOTTOM | DT_SINGLELINE);
			}
	}

	CxImage image;
	HBITMAP hbmp = (HBITMAP)bitmap;

	bool ret1 = image.CreateFromHBITMAP(hbmp);
	CString strError;
	if(ret1 == false)
	{
		//strError.Format("Fail to create caption object[%s]", caption_filename);
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);
	}

	image.Crop(CRect(0,0,rect.Width(),rect.Height()));

	bool ret2 = image.Save(caption_filename, CXIMAGE_FORMAT_BMP); //bmp 파일로 생성
	if(ret2 == false)
	{
		//strError.Format("Fail to save Caption file[%s]", caption_filename);
		//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);
	}

	image.Destroy();

	m_nCaptionWidth = rect.Width();

	pVMR9Interface->SetRect(client_rect);
	pVMR9Interface->SetBitmap(caption_filename, 100, RGB(255, 255, 255), rect);
}
// Modified by 정운형 2009-01-08 오후 3:14
// 변경내역 :  video schedule의 업데이트시에는 캡션파일을 다시 만들어줘야함
*/

bool CVideoSchedule::CloseFile()
{
	if(m_bOpen)
	{
		CloseDSRender();
	}//if

	return true;
}

bool CVideoSchedule::Play(bool bResume)
{
	if(m_bPause && m_pInterface)
	{
		m_pInterface->Play();
		m_bPause = false;

		return true;
	}//if

	if(m_nVedioOpenType == E_OPEN_PLAY)
	{
		m_bOpen = OpenDSRender();
	}//if

	CSchedule::Play(bResume);
	if(!m_bOpen)
	{
		CSchedule::Stop();

		return false;
	}//if

	if(m_pInterface)
	{
		m_pInterface->SeekToStart();
		m_pInterface->Play();
		m_pInterface->SetAspectRatioMode(ASPECT_RATIO_MODE::STRETCHED);
		m_bPause = false;
		m_pInterface->SetVolume( m_bMute ? 0 : m_soundVolume );

		if(m_nVedioRenderMode == E_WINDOWLESS)
		{
			SetTimer(ID_VIDEO_REPAINT, TIME_VIDEO_REPAINT, NULL);
		}//if
	}//if
/*
	if(m_bUseCaption)
	{	
		REFTIME tm = 0.0f;
		CRect rect;
		GetClientRect(rect);
		m_offset = rect.Width();

		double xscale = 1.0, yscale = 1.0;

		m_nMS = m_playSpeed / 40 / xscale; // 스피드 보정
		//m_nMS = m_playSpeed; // 스피드 보정
		if(m_nMS < TIMER_MINIMUM_TIME )
		{
			m_nMS = TIMER_MINIMUM_TIME;
		}//if
		//__DEBUG__("\t\t\tPlay Speed", m_nMS);

#ifdef USE_MMTIMER
		//__DEBUG__("\t\t\tMultiMedia Timer Mode", _NULL);
		TIMECAPS tc;
		timeGetDevCaps(&tc, sizeof(TIMECAPS));

		if(m_nMS < tc.wPeriodMin)
		{
			m_nMS = tc.wPeriodMin;
		}
		else if(m_nMS > tc.wPeriodMax)
		{
			m_nMS = tc.wPeriodMax;
		}//if

		timeBeginPeriod(m_nMS);

		if(m_nTimerId == 0)
		{
			m_nTimerId = timeSetEvent(m_nMS, m_nMS, CVideoSchedule::timeProc, (DWORD)m_hWnd, TIME_PERIODIC);
		}//if

		if(!m_nTimerId)
		{
			//OnErrorMessage((WPARAM)"MultiMedia Timer Setting Error !!!", 0);
		}//if
#else
		//__DEBUG__("\t\t\tWindow Timer Mode", _NULL);
		SetTimer(CAPTION_SHIFT_ID, m_nMS, NULL);
#endif
	}//if
*/
	//Invalidate();

	//__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

void CVideoSchedule::SoundVolumeMute(bool bMute)
{
	m_bMute = bMute;
	if(m_pInterface)
	{
		m_pInterface->SetVolume(m_bMute ? 0 : m_soundVolume);
	}//if
}

bool CVideoSchedule::Pause()
{
	//__DEBUG_SCHEDULE_BEGIN__(m_scheduleId.c_str())

	if(m_pInterface)
	{
		m_pInterface->Pause();
		m_bPause = true;
	}//if

	//__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())

	return true;
}

bool CVideoSchedule::Stop(bool bHide)
{
/*
	if(m_bUseCaption)
	{
#ifdef USE_MMTIMER
		timeKillEvent(m_nTimerId);
		timeEndPeriod(m_nMS);
		m_nTimerId = 0;
#else
		KillTimer(CAPTION_SHIFT_ID);
#endif
		m_offset = m_nCaptionWidth;
		OnCaptionShift(0,0);	
	}//if
*/

	if(m_pInterface)
	{
		if(m_nVedioRenderMode == E_WINDOWLESS)
		{
			KillTimer(ID_VIDEO_REPAINT);
		}//if
	}//if


	if(m_nVedioOpenType == E_OPEN_PLAY)
	{
		CloseDSRender();
		m_bOpen = true;		//파일 open 상태는 true로 유지
	}
	else
	{
		if(m_pInterface)
		{
			m_pInterface->Stop();
		}//if
	}//if

	CSchedule::Stop(bHide);

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Directshow render를 옵션에 맞추어 설정하고 그래프 빌더를 구성한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVideoSchedule::OpenDSRender()
{
	bool bRet = false;
	if(m_nVedioRenderMode == E_OVERLAY)
	{
		//__DEBUG__("\t\t\tOverlay Interface Mode", _NULL);
		m_pInterface = new COverlayInterface(this);
		m_pInterface->SetParentWindow(GetSafeHwnd());

		if(m_pInterface->Open(m_strMediaFullPath))
		{
			bRet = true;
			//크기조정
			//m_pInterface->SetAspectRatioMode(CBasicInterface::ASPECT_RATIO_MODE::STRETCHED);
			//CreateCaptionImage();
		}
		else
		{
			m_bOpen = false;
			bRet = false;
			//CString strError;
			//strError.Format("Video file[%s] render fail", m_strMediaFullPath);
			//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);
		}//if
	}
	else
	{
		m_pInterface = new CVMRRender(this);
		m_pInterface->SetParentWindow(GetSafeHwnd());
		m_pInterface->SetRenderType(m_nVedioRenderMode);
		if(!m_pInterface->Open(m_strMediaFullPath))
		{
			m_bOpen = false;
			bRet = false;
			//CString strError;
			//strError.Format("Video file[%s] render fail", m_strMediaFullPath);
			//OnErrorMessage((WPARAM)(LPCSTR)strError, 0);
		}
		else
		{
			bRet = true;
			//m_pInterface->SetAspectRatioMode(CBasicInterface::ASPECT_RATIO_MODE::STRETCHED);
		}//if
	}//if

	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Directshow render를 종료한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CVideoSchedule::CloseDSRender()
{
	//Stop();
	m_bOpen = false;
	if(m_pInterface)
	{
		m_pInterface->Close();
		delete m_pInterface;
	}//if
	m_pInterface = NULL;
}


bool CVideoSchedule::Save()
{
	CSchedule::Save();

	WritePrivateProfileString(m_strLoadID, "bgColor", m_bgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fgColor", m_fgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "font", m_font.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fontSize", ::ToString(m_fontSize), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "playSpeed", ::ToString(m_playSpeed), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "soundVolume", ::ToString(m_soundVolume), GetDataPath());

	return true;
}

CString CVideoSchedule::GetCaptionFilename(CString strFilename)
{
	int nIndex = strFilename.ReverseFind( '.' );

	strFilename.Delete( nIndex, (strFilename.GetLength() - nIndex) );
	strFilename += ".bmp";

	return strFilename;
}


void CVideoSchedule::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSchedule::OnTimer(nIDEvent);

	if(nIDEvent == CAPTION_SHIFT_ID)
	{
		/*if(m_bUseCaption)
		{
			OnCaptionShift(0,0);
		}*/
	}
	else if(nIDEvent == ID_VIDEO_REPAINT)
	{
		Invalidate(FALSE);
	}//if
}
/*
void CALLBACK CVideoSchedule::timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2)
{
	::PostMessage((HWND)dwUser, CAPTION_SHIFT_ID, 0, 0);
}
*/

CString to_string(ciString* pString, int count)
{
	CString str;

	for(int i=0; i<count; i++)
	{
		if(i != 0) str.Append("\r\n");
		CString tmp;
		tmp.Format("comment[%d] = \"%s\"", i, pString[i].c_str());
		str.Append(tmp);
	}

	return str;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPictureSchedule

CPictureSchedule::CPictureSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_bAnimatedGif(false)
,	m_nType (0)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CPictureSchedule::~CPictureSchedule()
{
}

IMPLEMENT_DYNAMIC(CPictureSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CPictureSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

int CPictureSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rect;
	GetClientRect(rect);

	m_bgGIF.Create("Animated GIF", WS_CHILD, rect, this);

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
//		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	return 0;
}


void CPictureSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();
}


void CPictureSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
	CMemDC memDC(&dc);

	if(m_bOpen)
	{
		if(m_bAnimatedGif)
		{
			CRect rect;
			GetClientRect(rect);

			SIZE size = m_bgGIF.GetSize();

			memDC.FillSolidRect(size.cx, 0, rect.Width(), size.cy, RGB(255,255,255));
			memDC.FillSolidRect(0, size.cy, rect.Width(), rect.Height(), RGB(255,255,255));
		}
		else
		{
			if(m_bgImage.IsValid())
			{
				CRect rect;
				GetClientRect(rect);

				switch(m_nType)
				{
				case 0:	// fullscreen
					m_bgImage.Draw2(memDC.m_hDC, rect);
					break;

				case 1:	// original
					dc.FillSolidRect(rect, RGB(255,255,255));
					rect.right = rect.left + m_bgImage.GetWidth();
					rect.bottom = rect.top + m_bgImage.GetHeight();
					m_bgImage.Draw2(memDC.m_hDC, rect);
					break;

				case 2:	// fittoscreen
					{
						dc.FillSolidRect(rect, RGB(255,255,255));

						double scale;

						int image_width = m_bgImage.GetWidth();
						int image_height = m_bgImage.GetHeight();

						int scr_width = rect.Width();
						int scr_height = rect.Height();

						int width, height;
						width = scr_width;
						height = image_width * scr_height / scr_width;
						if(height < image_height)
						{
							height = scr_height;
							width = image_height * scr_width / scr_height;

							scale = ((double)scr_height) / ((double)image_height);
						}
						else
						{
							scale = ((double)scr_width) / ((double)image_width);
						}

						rect.right = rect.left + m_bgImage.GetWidth() * scale;
						rect.bottom = rect.top + m_bgImage.GetHeight() * scale;

						m_bgImage.Draw2(memDC.m_hDC, rect);
					}
					break;
				}
			}
		}
	}
}

void CPictureSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);
}

double CPictureSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CPictureSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CPictureSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	bool bRet = false; 
	CString strExt = FindExtension(m_strMediaFullPath);
	if(strExt.CompareNoCase("gif") == 0)
	{
		if(!m_bgGIF.Load(m_strMediaFullPath))
		{
			if(m_bgGIF.GetSafeHwnd())
			{
				m_bgGIF.ShowWindow(SW_HIDE);
				m_bgGIF.Stop();
				m_bgGIF.UnLoad();
			}//if
			//__DEBUG__("Gif image load fail", _NULL);
			//__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
			return false;
		}//if
		
		if(m_bgGIF.IsAnimatedGIF())
		{
			//__DEBUG__("\t\t\tAnimated GIF Mode", _NULL);
			m_bAnimatedGif = true;
			m_bgGIF.ShowWindow(SW_SHOW);
			bRet = true;
		}
		else
		{
			if(m_bgGIF.GetSafeHwnd())
			{
				m_bgGIF.ShowWindow(SW_HIDE);
				m_bgGIF.Stop();
				m_bgGIF.UnLoad();
			}//if

			//__DEBUG__("\t\t\tImage Mode", _NULL);
			m_bAnimatedGif = false;
			int nImgType = CxImage::GetTypeIdFromName(strExt);
			bRet = m_bgImage.Load(m_strMediaFullPath, nImgType);
		}//if
	}
	else
	{
		//__DEBUG__("\t\t\tImage Mode", _NULL);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		bRet = m_bgImage.Load(m_strMediaFullPath, nImgType);
	}//if

	if(bRet)
	{
		m_bOpen = true;

		return true;
	}//if
	
	m_bOpen = false;
	
	return false;
}

bool CPictureSchedule::CloseFile()
{
	Stop();

	if(m_bOpen)
	{
		m_bOpen = false;

		if(m_bAnimatedGif)
		{
			m_bgGIF.UnLoad();
		}
		else
		{
			if(m_bgImage.IsValid())
			{
				m_bgImage.Destroy();
			}//if
		}//if

		m_bAnimatedGif = false;
	}
	return true;
}

bool CPictureSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);
	if(!m_bOpen)
	{
		CSchedule::Stop();

		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	if(m_bAnimatedGif)
	{
		m_bgGIF.Draw();
	}//if

	return true;
}

bool CPictureSchedule::Pause()
{
	Stop();

	return true;
}

bool CPictureSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

	if(m_bAnimatedGif)
	{
		m_bgGIF.Stop();
	}//if

	CSchedule::Stop();

	return true;
}

bool CPictureSchedule::Save()
{
	CSchedule::Save();

	return true;
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPromotionSchedule

CProgressBAR* CProgressBAR::GetProgressBARObject(CSchedule* pParentWnd, LPCSTR lpszID)
{
	// check valid
	CProgressBAR* progress_bar = new CProgressBAR(pParentWnd, lpszID);

	BOOL ret_value = progress_bar->Create(NULL, "ProgressBAR", WS_CHILD | WS_VISIBLE, CRect(0,0,100,100), pParentWnd, m_nProgressBARID--);
	if(ret_value)
	{
		return progress_bar;
	}
	else
	{
		delete progress_bar;
	}

	return NULL;
}

CProgressBAR::CProgressBAR(CSchedule* pParentWnd, LPCSTR lpszID)
:	m_pParentWnd(pParentWnd)
,	m_fScaleX(1.0f)
,	m_fScaleY(1.0f)
,	m_strErrorMessage ("")
,	m_strLoadID(lpszID)
{
	//
	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(lpszID, "contentsId", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_contentsId = tmpBuffer;

	GetPrivateProfileString(lpszID, "progressBarId", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_progressBarId = tmpBuffer;

	GetPrivateProfileString(lpszID, "itemName", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_itemName = tmpBuffer;

	GetPrivateProfileString(lpszID, "x", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_x = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "y", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_y = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "width", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_width = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "height", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_height = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "fgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "bgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_bgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "borderColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_borderColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "borderThickness", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_borderThickness = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "valueVisible", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_valueVisible = (bool)atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "font", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_font = tmpBuffer;

	GetPrivateProfileString(lpszID, "fontSize", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fontSize = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "fontColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fontColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "minValue", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_minValue = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "maxValue", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_maxValue = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "direction", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_direction = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "valueX", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_valueX = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "valueY", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_valueY = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "valueWidth", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_valueWidth = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "valueHeight", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_valueHeight = atoi(tmpBuffer);

	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}
	else
		m_rgbFgColor = RGB(255,0,0);

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	else
		m_rgbBgColor = RGB(255,255,255);

	if(m_borderColor.length() > 1)
	{
		m_rgbBorderColor = ::GetColorFromString(m_borderColor.c_str()+1);
	}
	else
		m_rgbBorderColor = RGB(0,0,0);

	if(m_fontColor.length() > 1)
	{
		m_rgbFontColor = ::GetColorFromString(m_fontColor.c_str()+1);
	}
	else
		m_rgbFontColor = RGB(0,0,0);

	m_value = m_minValue;
}

CProgressBAR::~CProgressBAR()
{
}

IMPLEMENT_DYNAMIC(CProgressBAR, CWnd)

BEGIN_MESSAGE_MAP(CProgressBAR, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
END_MESSAGE_MAP()

int CProgressBAR::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	ReposWindow();

	return 0;
}

void CProgressBAR::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	DrawBar(&dc);
}

void CProgressBAR::DrawBar(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);

	int width = rect.Width();
	int height = rect.Height();

	CRect rect_fg, rect_bg;
	int value;

	double xscale = 1.0f, yscale = 1.0f;
//	m_pParentParentWnd->GetScale(xscale, yscale);

	switch(m_direction)
	{
	case 0: // left to right
		value = (width - m_borderThickness*2 * xscale) * (m_value - m_minValue) / (m_maxValue - m_minValue); 

		rect_fg = rect;
		rect_fg.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale, m_borderThickness * xscale, m_borderThickness * yscale);
		rect_fg.right = rect_fg.left + value;

		rect_bg = rect;
		rect_bg.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale, m_borderThickness * xscale, m_borderThickness * yscale);
		rect_bg.left = rect_fg.right;

		break;

	case 1: // right to left
		value = (width - m_borderThickness*2 * xscale) * (m_value - m_minValue) / (m_maxValue - m_minValue); 

		rect_bg = rect;
		rect_bg.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale, m_borderThickness * xscale, m_borderThickness * yscale);
		rect_bg.right = rect_bg.left + value;

		rect_fg = rect;
		rect_fg.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale, m_borderThickness * xscale, m_borderThickness * yscale);
		rect_fg.left = rect_bg.right;

		break;

	case 2: // top to bottom
		value = (height - m_borderThickness*2 * yscale) * (m_value - m_minValue) / (m_maxValue - m_minValue);

		rect_fg = rect;
		rect_fg.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale, m_borderThickness * xscale, m_borderThickness * yscale);
		rect_fg.bottom -= value;

		rect_bg = rect;
		rect_bg.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale, m_borderThickness * xscale, m_borderThickness * yscale);
		rect_bg.top = rect_fg.bottom;

		break;

	case 3: // bottom to top
		value = (height - m_borderThickness*2 * yscale) * (m_value - m_minValue) / (m_maxValue - m_minValue);

		rect_bg = rect;
		rect_bg.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale, m_borderThickness * xscale, m_borderThickness * yscale);
		rect_bg.bottom -= value;

		rect_fg = rect;
		rect_fg.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale, m_borderThickness * xscale, m_borderThickness * yscale);
		rect_fg.top = rect_bg.bottom;

		break;
	}

	// 백그라운드
	pDC->FillSolidRect(rect_bg, m_rgbBgColor);

	if(m_direction == 0 || m_direction == 1)
	{
		// 가로

#if 0
		// 포그라운드 solid bar
		pDC->FillSolidRect(rect_left, m_rgbFgColor);
#else
		// 포그라운드 gradient bar
		TRIVERTEX        vert [4];
		GRADIENT_TRIANGLE    gTri[2];
		vert [0] .x       =  rect_fg.left;
		vert [0] .y       =  rect_fg.top;
		vert [0] .Red     =  0xff00;
		vert [0] .Green   =  0xff00;
		vert [0] .Blue    =  0xff00;
		vert [0] .Alpha   =  0x0000;

		vert [1] .x       =  rect_fg.right;
		vert [1] .y       =  rect_fg.top;
		vert [1] .Red     =  0xff00;
		vert [1] .Green   =  0xff00;
		vert [1] .Blue    =  0xff00;
		vert [1] .Alpha   =  0x0000;

		vert [2] .x       =  rect_fg.right;
		vert [2] .y       =  rect_fg.bottom; 
		vert [2] .Red     =  (m_rgbFgColor & 0x000000FF) << 8;
		vert [2] .Green   =  (m_rgbFgColor & 0x0000FF00);
		vert [2] .Blue    =  (m_rgbFgColor & 0x00FF0000) >> 8;
		vert [2] .Alpha   =  0x0000;

		vert [3] .x       =  rect_fg.left;
		vert [3] .y       =  rect_fg.bottom;
		vert [3] .Red     =  (m_rgbFgColor & 0x000000FF) << 8;
		vert [3] .Green   =  (m_rgbFgColor & 0x0000FF00);
		vert [3] .Blue    =  (m_rgbFgColor & 0x00FF0000) >> 8;
		vert [3] .Alpha   =  0x0000;

		gTri[0].Vertex1   = 0;
		gTri[0].Vertex2   = 1;
		gTri[0].Vertex3   = 2;

		gTri[1].Vertex1   = 0;
		gTri[1].Vertex2   = 2;
		gTri[1].Vertex3   = 3;
		GradientFill(pDC->m_hDC,vert,4,&gTri,2,GRADIENT_FILL_TRIANGLE);
#endif
	}
	else if(m_direction == 2 || m_direction == 3)
	{
		// 세로

#if 0
		// 포그라운드 solid bar
		pDC->FillSolidRect(rect_down, m_rgbFgColor);
#else
		// 포그라운드 gradient bar
		TRIVERTEX        vert [4];
		GRADIENT_TRIANGLE    gTri[2];
		vert [0] .x       =  rect_fg.left;
		vert [0] .y       =  rect_fg.top;
		vert [0] .Red     =  0xff00;
		vert [0] .Green   =  0xff00;
		vert [0] .Blue    =  0xff00;
		vert [0] .Alpha   =  0x0000;

		vert [1] .x       =  rect_fg.right;
		vert [1] .y       =  rect_fg.top;
		vert [1] .Red     =  (m_rgbFgColor & 0x000000FF) << 8;
		vert [1] .Green   =  (m_rgbFgColor & 0x0000FF00);
		vert [1] .Blue    =  (m_rgbFgColor & 0x00FF0000) >> 8;
		vert [1] .Alpha   =  0x0000;

		vert [2] .x       =  rect_fg.right;
		vert [2] .y       =  rect_fg.bottom; 
		vert [2] .Red     =  (m_rgbFgColor & 0x000000FF) << 8;
		vert [2] .Green   =  (m_rgbFgColor & 0x0000FF00);
		vert [2] .Blue    =  (m_rgbFgColor & 0x00FF0000) >> 8;
		vert [2] .Alpha   =  0x0000;

		vert [3] .x       =  rect_fg.left;
		vert [3] .y       =  rect_fg.bottom;
		vert [3] .Red     =  0xff00;
		vert [3] .Green   =  0xff00;
		vert [3] .Blue    =  0xff00;
		vert [3] .Alpha   =  0x0000;

		gTri[0].Vertex1   = 0;
		gTri[0].Vertex2   = 1;
		gTri[0].Vertex3   = 2;

		gTri[1].Vertex1   = 0;
		gTri[1].Vertex2   = 2;
		gTri[1].Vertex3   = 3;
		GradientFill(pDC->m_hDC,vert,4,&gTri,2,GRADIENT_FILL_TRIANGLE);
#endif
	}

	// 테두리
	CRect rrrr = rect;
	for(int i=0; i<m_borderThickness * xscale; i++)
	{
		pDC->Draw3dRect(rrrr, m_rgbBorderColor, m_rgbBorderColor);
		rrrr.DeflateRect(1,0,1,0);
	}
	rrrr = rect;
	for(int i=0; i<m_borderThickness * yscale; i++)
	{
		pDC->Draw3dRect(rrrr, m_rgbBorderColor, m_rgbBorderColor);
		rrrr.DeflateRect(0,1,0,1);
	}
}

void CProgressBAR::DrawText(CDC* pDC)
{
	double xscale = 1.0f, yscale = 1.0f;
//	m_pParentParentWnd->GetScale(xscale, yscale);

	if(m_direction == 0 || m_direction == 1)
	{
		// 가로

		// 값
		CFont font;
		LOGFONT lf;
		memset(&lf, 0, sizeof (LOGFONT));
		lf.lfCharSet = (BYTE)GetTextCharsetInfo(pDC->GetSafeHdc(), NULL, 0);
		strcpy(lf.lfFaceName, m_font.c_str());
		lf.lfHeight = m_fontSize * xscale;
		lf.lfWeight = FW_BOLD;
		font.CreateFontIndirect(&lf);
		pDC->SelectObject(&font);

		pDC->SetBkMode(1); //TRANSPARENT

		int val = m_maxValue - m_value;
		if(val < 0) val = 0;

		CString value;
		value.Format("%d", val);
#if 0
		// 그래프 내부 글자
		CRgn rgn_left;
		rgn_left.CreateRectRgnIndirect(rect_left);
		pDC->SelectClipRgn(&rgn_left);
		pDC->SetTextColor(m_rgbBgColor);
		pDC->DrawText(value, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		CRgn rgn_right;
		rgn_right.CreateRectRgnIndirect(rect_right);
		pDC->SelectClipRgn(&rgn_right);
		pDC->SetTextColor(m_rgbFgColor);
		pDC->DrawText(value, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		pDC->SelectClipRgn(NULL);
#else
		// 그래프 외부 글자
		CRect rect(
			m_valueX * m_fScaleX,
			m_valueY * m_fScaleY,
			(m_valueX + m_valueWidth) * m_fScaleX,
			(m_valueY + m_valueHeight) * m_fScaleY
		);

		pDC->SetTextColor(m_rgbFontColor);
		pDC->DrawText(value, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

#endif
/*
		//최대, 최소값
		if(m_valueVisible == ciTrue)
		{
			CString min_value;
			min_value.Format("%d", m_minValue);

			CString max_value;
			max_value.Format("%d", m_maxValue);

			CSize min_size = pDC->GetOutputTextExtent(min_value);
			CSize max_size = pDC->GetOutputTextExtent(max_value);

			rect_left.right = rect_left.left;
			rect_left.left -= min_size.cx;

			rect_right.left = rect_right.right;
			rect_right.right += max_size.cx;

//			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText(min_value, rect_left,  DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			pDC->DrawText(max_value, rect_right, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		}
*/
	}
	else if(m_direction == 2 || m_direction == 3)
	{
		//세로

		// 값
		CFont font;
		LOGFONT lf;
		memset(&lf, 0, sizeof (LOGFONT));
		lf.lfCharSet = (BYTE)GetTextCharsetInfo(pDC->GetSafeHdc(), NULL, 0);
		strcpy(lf.lfFaceName, m_font.c_str());
		lf.lfHeight = m_fontSize * xscale;
		lf.lfWeight = FW_BOLD;
		font.CreateFontIndirect(&lf);
		pDC->SelectObject(&font);

		pDC->SetBkMode(1); //TRANSPARENT

		int val = m_maxValue - m_value;
		if(val < 0) val = 0;

		CString value;
		value.Format("%d", val);
#if 0
		// 그래프 내부 글자
		CRgn rgn_up;
		rgn_up.CreateRectRgnIndirect(rect_up);
		pDC->SelectClipRgn(&rgn_up);
		pDC->SetTextColor(m_rgbFgColor);
		pDC->DrawText(value, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		CRgn rgn_down;
		rgn_down.CreateRectRgnIndirect(rect_down);
		pDC->SelectClipRgn(&rgn_down);
		pDC->SetTextColor(m_rgbBgColor);
		pDC->DrawText(value, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		pDC->SelectClipRgn(NULL);
#else
		// 그래프 외부 글자
		CRect rect(
			m_valueX * m_fScaleX,
			m_valueY * m_fScaleY,
			(m_valueX + m_valueWidth) * m_fScaleX,
			(m_valueY + m_valueHeight) * m_fScaleY
		);

		pDC->SetTextColor(m_rgbFontColor);
		pDC->DrawText(value, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
#endif
/*
		//최대, 최소값
		if(m_valueVisible == ciTrue)
		{
			CString min_value;
			min_value.Format("%d", m_minValue);

			CString max_value;
			max_value.Format("%d", m_maxValue);

			CSize min_size = pDC->GetOutputTextExtent(min_value);
			CSize max_size = pDC->GetOutputTextExtent(max_value);

			rect_up.bottom = rect_up.top;
			rect_up.top -= min_size.cy;

			rect_down.top = rect_down.bottom;
			rect_down.bottom += max_size.cy;

//			pDC->SetTextColor(RGB(0,0,0));
			pDC->DrawText(min_value, rect_up,  DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			pDC->DrawText(max_value, rect_down, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		}
*/
	}
}

void CProgressBAR::ReposWindow()
{
	CRect rect(
		m_x * m_fScaleX,
		m_y * m_fScaleY,
		(m_x+m_width) * m_fScaleX,
		(m_y+m_height) * m_fScaleY
	);

	MoveWindow(rect);
}

bool CProgressBAR::Save()
{
	WritePrivateProfileString(m_strLoadID, "contentsId", m_contentsId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "progressBarId", m_progressBarId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "itemName", m_itemName.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "x", ::ToString(m_x), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "y", ::ToString(m_y), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "width", ::ToString(m_width), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "height", ::ToString(m_height), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fgColor", m_fgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "bgColor", m_bgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "borderColor", m_borderColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "borderThickness", ::ToString(m_borderThickness), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "valueVisible", ::ToString(m_valueVisible), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "font", m_font.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fontSize", ::ToString(m_fontSize), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fontColor", m_fontColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "minValue", ::ToString(m_minValue), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "maxValue", ::ToString(m_maxValue), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "direction", ::ToString(m_direction), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "valueX", ::ToString(m_valueX), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "valueY", ::ToString(m_valueY), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "valueWidth", ::ToString(m_valueWidth), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "valueHeight", ::ToString(m_valueHeight), GetDataPath());

	return true;
}


////////////////////////////////////////////////////////////////////////////////////////////////
CPromotionSchedule::CPromotionSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CPromotionSchedule::~CPromotionSchedule()
{
}

IMPLEMENT_DYNAMIC(CPromotionSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CPromotionSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
END_MESSAGE_MAP()

int CPromotionSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
//		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	if(m_bLoadFromLocal)
	{
		LoadProgressBar();
		m_bLoadFromLocal = false;
	}
	else
	{
//		InitProgressBar();
	}

	UpdateProgressBARValue();

	return 0;
}

void CPromotionSchedule::OnDestroy()
{
	CloseFile();

	POSITION pos = m_mapProgressBAR.GetStartPosition();
	if(pos != NULL)
	{
		while(pos != NULL)
		{
			CString id;
			CProgressBAR* bar;

			m_mapProgressBAR.GetNextAssoc( pos, id, (void*&)bar );

			delete bar;
		}

		m_mapProgressBAR.RemoveAll();
	}

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

}

void CPromotionSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	// 배경
	if(m_bgImage.IsValid())
	{
		CRect rect;
		GetClientRect(rect);

		m_bgImage.Draw2(dc.GetSafeHdc(), rect);
	}

	// 프로그래스 바
	POSITION pos = m_mapProgressBAR.GetStartPosition();
	if(pos != NULL)
	{
		while(pos != NULL)
		{
			CString id;
			CProgressBAR* bar;

			m_mapProgressBAR.GetNextAssoc( pos, id, (void*&)bar );

			bar->DrawText(&dc);
		}
	}
}

void CPromotionSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(lpszID, "promotionValueList", "", tmpBuffer, BUF_SIZE, GetDataPath());
	//m_promotionValueList.fromString(tmpBuffer);

	UpdateProgressBARValue();

}

double CPromotionSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CPromotionSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CPromotionSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	CString strExt = FindExtension(m_strMediaFullPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	bool ret = m_bgImage.Load(m_strMediaFullPath, nImgType);
	if(ret == false)
	{
		return false;
	}

	m_bOpen = true;

	double scale_x = 1.0f;
	double scale_y = 1.0f;

	if(m_bgImage.IsValid())
	{
		CRect rect;
		GetClientRect(rect);

		scale_x = ((double)rect.Width()) / ((double)m_bgImage.GetWidth());
		scale_y = ((double)rect.Height()) / ((double)m_bgImage.GetHeight());
	}

	POSITION pos = m_mapProgressBAR.GetStartPosition();
	if(pos == NULL)
	{
	}
	else
	{
		while(pos != NULL)
		{
			CString id;
			CProgressBAR* bar;

			m_mapProgressBAR.GetNextAssoc( pos, id, (void*&)bar );

			bar->SetScale(scale_x, scale_y);
			bar->ReposWindow();
		}
	}

	return true;
}

bool CPromotionSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();
		
		m_bOpen = false;

		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if
	}//if
	return true;
}

bool CPromotionSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);
	if(!m_bOpen)
	{
		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	return true;
}

bool CPromotionSchedule::Pause()
{
	Stop();

	return true;
}

bool CPromotionSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

	return true;
}

bool CPromotionSchedule::Save()
{
	CSchedule::Save();

	//
//	WritePrivateProfileString(m_strLoadID, "promotionValueList", m_promotionValueList.toString(), GetDataPath());

	//
	CString str_progressbar_list;
	POSITION pos = m_mapProgressBAR.GetStartPosition();
	if(pos != NULL)
	{
		while(pos != NULL)
		{
			CString id;
			CProgressBAR* bar;

			m_mapProgressBAR.GetNextAssoc( pos, id, (void*&)bar );

			bar->Save();

			if(str_progressbar_list.GetLength() != 0)
				str_progressbar_list.Append(",");

			str_progressbar_list.Append(bar->GetLoadID());
		}
	}

	WritePrivateProfileString(m_strLoadID, "ProgressBarList", str_progressbar_list, GetDataPath());

	return true;
}

bool CPromotionSchedule::LoadProgressBar()
{
	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(m_strLoadID, "ProgressBarList", "", tmpBuffer, BUF_SIZE, GetDataPath());
	CString progressbar_list = tmpBuffer;

	//
	int pos = 0;
	CString token = progressbar_list.Tokenize(",", pos);
	while(token != "")
	{
		CProgressBAR* progress_bar = CProgressBAR::GetProgressBARObject(this, token);
		if(progress_bar)
		{
			m_mapProgressBAR.SetAt(progress_bar->GetProgressBarId(), progress_bar);
		}

		token = progressbar_list.Tokenize(",", pos);
	}

	return true;
}

bool CPromotionSchedule::UpdateProgressBARValue()
{
	//for(int i=0; i<m_promotionValueList.length(); i++)
	//{
	//	CProgressBAR* bar;
	//	CString id; id.Format("%d", i);
	//	if(m_mapProgressBAR.Lookup(id, (void*&)bar))
	//	{
	//		CString value = m_promotionValueList[i].c_str();
	//		if(value.GetLength() > 0)
	//		{
	//			bar->SetValue(atoi(value));
	//			if(bar->GetSafeHwnd())
	//				bar->Invalidate(FALSE);
	//		}
	//	}
	//	else
	//	{
	//	}
	//}

	if(GetSafeHwnd())
		Invalidate();


	return true;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPhoneSchedule

CPhoneSchedule::CPhoneSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_bSendRejectMessage (false)
,	m_bPlaying (false)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CPhoneSchedule::~CPhoneSchedule()
{
}

IMPLEMENT_DYNAMIC(CPhoneSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CPhoneSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

int CPhoneSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
//		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	return 0;
}

void CPhoneSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	CRect rect;
	GetClientRect(rect);

	dc.FillSolidRect(rect, RGB(128,128,128));
}

void CPhoneSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

}

void CPhoneSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(lpszID, "phoneNumber", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_phoneNumber = tmpBuffer;

	GetPrivateProfileString(lpszID, "isOrigin", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_isOrigin = (bool)atoi(tmpBuffer);

	//
	m_runningTime = LONG_MAX;

	//
	if(m_phoneNumber.length() > 0)
	{
		CString tokens = m_phoneNumber.c_str();

		int pos = 0;
		m_strPhone	= tokens.Tokenize("/", pos);
		m_strPort	= tokens.Tokenize("/", pos);

	}

	//
	if(m_castingState == SCH_READY)
	{
		m_bSendRejectMessage = false;
	}
	else
	{
		if(    m_castingState == SCH_READY_FAIL 
			&& m_bSendRejectMessage == false
			&& m_isOrigin == ciTrue
		)
		{
			m_bSendRejectMessage = true;
//			::AfxGetMainWnd()->PostMessage(WM_PHONE_REJECT);
		}
	}

}

double CPhoneSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CPhoneSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CPhoneSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	m_bOpen = true;

	return true;
}

bool CPhoneSchedule::CloseFile()
{
	if(m_bPlaying)
	{
		m_bOpen = false;
		Stop();
	}
	return true;
}

bool CPhoneSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);
	if(!m_bOpen)
	{
		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	CRect rect;
	GetClientRect(rect);

	if(m_bPlaying == false)
	{
//		ShowViewDlg((LPSTR)(LPCSTR)m_strPhone, (LPSTR)(LPCSTR)m_strPort, rect, this, m_strMediaFullPath);
		m_bPlaying = true;
		::AfxGetMainWnd()->SetFocus();
	}

	CSchedule::m_csSchedulePhone.Lock();
	CSchedule::m_pPhoneSchedule = this;
	CSchedule::m_csSchedulePhone.Unlock();

	

	return true;
}

bool CPhoneSchedule::Pause()
{
	Stop();

	return true;
}

bool CPhoneSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

	if(m_bPlaying)
	{
//		DeleteViewDlg();
		m_bPlaying = false;
	}

	CSchedule::m_csSchedulePhone.Lock();
	if(CSchedule::m_pPhoneSchedule == this)
		CSchedule::m_pPhoneSchedule = NULL;
	CSchedule::m_csSchedulePhone.Unlock();

	return true;
}

bool CPhoneSchedule::Save()
{
	CSchedule::Save();

	//
	WritePrivateProfileString(m_strLoadID, "phoneNumber", m_phoneNumber.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "isOrigin", ::ToString(m_isOrigin), GetDataPath());

	return true;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CWebCamSchedule

CWebCamSchedule::CWebCamSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_bPlaying (false)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CWebCamSchedule::~CWebCamSchedule()
{
}

IMPLEMENT_DYNAMIC(CWebCamSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CWebCamSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

int CWebCamSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
//		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	return 0;
}

void CWebCamSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	CRect rect;
	GetClientRect(rect);

	dc.FillSolidRect(rect, RGB(128,128,128));
}

void CWebCamSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

}

void CWebCamSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	//
	CString strPhoneTemplateId, strPhoneFrameId;
	int nPortNo = 0;
//	::GetPhoneConfig(strPhoneTemplateId, strPhoneFrameId, nPortNo);

	m_strPhone	= "127.0.0.1";
	m_strPort.Format("%d", nPortNo);

}

double CWebCamSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CWebCamSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CWebCamSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	m_bOpen = true;

	return true;
}

bool CWebCamSchedule::CloseFile()
{
	if(m_bPlaying)
	{
		m_bOpen = false;
		Stop();
	}
	return true;
}

bool CWebCamSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);
	if(!m_bOpen)
	{
		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	CRect rect;
	GetClientRect(rect);

	if(m_bPlaying == false)
	{
//		ShowViewDlg((LPSTR)(LPCSTR)m_strPhone, (LPSTR)(LPCSTR)m_strPort, rect, this, m_strMediaFullPath, false);
		m_bPlaying = true;
		::AfxGetMainWnd()->SetFocus();
	}

	
	return true;
}

bool CWebCamSchedule::Pause()
{
	Stop();

	return true;
}

bool CWebCamSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

	if(m_bPlaying)
	{
//		DeleteViewDlg();
		m_bPlaying = false;
	}

	return true;
}

bool CWebCamSchedule::Save()
{
	CSchedule::Save();

	return true;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSMSSchedule

CSMSSchedule::CSMSSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_rgbBgColor(RGB(0,0,0))
,	m_rgbFgColor(RGB(255,255,255))
,	m_nLineCount(0)
,	m_direction(0)
,	m_align(5)
// <!-- 메가박스용 (2017.05.10 seventhstone)
,	m_bGridMode(false)
// -->
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CSMSSchedule::~CSMSSchedule()
{
	if(m_fontSMS.GetSafeHandle())
	{
		m_fontSMS.DeleteObject();
	}//if
}

IMPLEMENT_DYNAMIC(CSMSSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CSMSSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

int CSMSSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
//		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	return 0;
}

void CSMSSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CSMSSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	//
	CRect client_rect;
	GetClientRect(client_rect);

	CMemDC memDC(&dc);

	if(m_bgImage.IsValid())
	{
#if 1	
		// 바둑판
		for(int x=0; x<client_rect.Width(); x+=m_bgImage.GetWidth())
			for(int y=0; y<client_rect.Height(); y+=m_bgImage.GetHeight())
				m_bgImage.Draw2(memDC.GetSafeHdc(), x, y);
#else
		// 늘이기
		m_bgImage.Draw2(memDC.GetSafeHdc(), 0, 0, client_rect.Width(), client_rect.Height());
#endif
	}
	else
	{
		memDC.FillSolidRect(client_rect, m_rgbBgColor);
	}//if

	if(!m_bOpen)
	{
		return;
	}//if

	memDC.SetBkMode(TRANSPARENT);	//글자의 배경색을 보이게 하기 위하여...
	memDC.SetTextColor(m_rgbFgColor);
	memDC.SetBkColor(m_rgbBgColor);
	memDC.SelectObject(m_fontSMS);

	// <!-- 메가박스용 (2017.05.10 seventhstone)
	if( m_bGridMode )
	{
		DrawGridText((CDC*)&memDC);
		return;
	}
	// -->

	if(m_direction == TEXT_DIRECTION_HORIZONTAL)
	{
		CRect rectText(0, 0, 0, 0);
		memDC.DrawText(m_strSMS, &rectText, DT_CALCRECT);	//글자가 출력될 영역을 계산
		//GetTextExtentPoint32 ==> 문자열
		//CSize sizeText = memDC.GetOutputTextExtent(m_strSMS);	==> 한 문자
		//if(m_nLineCount > 1)
		//{
		//	sizeText.cy = sizeText.cy*m_nLineCount;
		//}//if

		int nPosX = 0, nPosY = 0, nCx = 0, nCy = 0, nGapX = 0, nGapY = 0;
		if(client_rect.Width() <= rectText.Width())
		//if(client_rect.Width() <= sizeText.cx)
		{
			nCx = client_rect.Width();
			nGapX = 0;
		}
		else
		{
			nCx = rectText.Width();
			nGapX = client_rect.Width() - rectText.Width();
			//nCx = sizeText.cx;
			//nGapX = client_rect.Width() - sizeText.cx;
		}//if

		if(client_rect.Height() <= rectText.Height())
		//if(client_rect.Height() <= sizeText.cy)
		{
			nCy = client_rect.Height();
			nGapY = 0;
		}
		else
		{
			nCy = rectText.Height();
			nGapY = client_rect.Height() - rectText.Height();
			//nCy = sizeText.cy;
			//nGapY = client_rect.Height() - sizeText.cy;
		}//if

		UINT uFormat;
		switch(m_align)
		{
		case TEXT_ALIGN_LT:
			{
				nPosX = 0;
				nPosY = 0;
				uFormat = DT_LEFT;
			}
			break;
		case TEXT_ALIGN_CT:
			{
				nPosX = nGapX/2;
				nPosY = 0;
				uFormat = DT_CENTER;
			}
			break;
		case TEXT_ALIGN_RT:
			{
				nPosX = nGapX;
				nPosY = 0;
				uFormat = DT_RIGHT;
			}
			break;
		case TEXT_ALIGN_LM:
			{
				nPosX = 0;
				nPosY = nGapY/2;
				uFormat = DT_LEFT;
			}
			break;
		case TEXT_ALIGN_CM:
			{
				nPosX = nGapX/2;
				nPosY = nGapY/2;
				uFormat = DT_CENTER;
			}
			break;
		case TEXT_ALIGN_RM:
			{
				nPosX = nGapX;
				nPosY = nGapY/2;
				uFormat = DT_RIGHT;
			}
			break;
		case TEXT_ALIGN_LB:
			{
				nPosX = 0;
				nPosY = nGapY;
				uFormat = DT_LEFT;
			}
			break;
		case TEXT_ALIGN_CB:
			{
				nPosX = nGapX/2;
				nPosY = nGapY;
				uFormat = DT_CENTER;
			}
			break;
		case TEXT_ALIGN_RB:
			{
				nPosX = nGapX;
				nPosY = nGapY;
				uFormat = DT_RIGHT;
			}
			break;
		default :	//TEXT_ALIGN_CM
			{
				nPosX = nGapX/2;
				nPosY = nGapY/2;
				uFormat = DT_CENTER;
			}
		}//switch

		CRect rectDraw(nPosX, nPosY, nCx+nPosX, nCy+nPosY);
		//memDC.FillSolidRect(rectDraw, RGB(0, 255, 0));
		memDC.DrawText(m_strSMS, rectDraw, uFormat);
		//memDC.DrawText(m_strSMS, client_rect, DT_RIGHT);
	}
	else
	{
		UINT uFormat;
		switch(m_align)
		{
		case TEXT_ALIGN_LT:
			{
				uFormat = DV_TEXTTOP;
			}
			break;
		case TEXT_ALIGN_CT:
			{
				uFormat = DV_HCENTER | DV_TEXTTOP;
			}
			break;
		case TEXT_ALIGN_RT:
			{
				uFormat = DV_RIGHT | DV_TEXTTOP;
			}
			break;
		case TEXT_ALIGN_LM:
			{
				uFormat = DV_VCENTER | DV_TEXTCENTER;
			}
			break;
		case TEXT_ALIGN_CM:
			{
				uFormat = DV_VCENTER | DV_HCENTER | DV_TEXTCENTER;
			}
			break;
		case TEXT_ALIGN_RM:
			{
				uFormat = DV_VCENTER | DV_RIGHT | DV_TEXTCENTER;
			}
			break;
		case TEXT_ALIGN_LB:
			{
				uFormat = DV_BOTTOM | DV_TEXTBOTTOM;
			}
			break;
		case TEXT_ALIGN_CB:
			{
				uFormat = DV_BOTTOM | DV_HCENTER | DV_TEXTBOTTOM;
			}
			break;
		case TEXT_ALIGN_RB:
			{
				uFormat = DV_BOTTOM | DV_RIGHT | DV_TEXTBOTTOM;
			}
			break;
		default :	//TEXT_ALIGN_CM
			{
				uFormat = DV_VCENTER | DV_HCENTER | DV_TEXTCENTER;
			}
		}//switch

		DrawVertText(memDC, m_strSMS, m_strSMS.GetLength(), &client_rect, uFormat, 3, 5);
	}//if
}

void CSMSSchedule::DrawGridText(CDC* pDC)
{
	int length = m_arrSMS.GetCount();

	//int screen_cx = GetSystemMetrics(SM_CXSCREEN);
	CRect client_rect;
	GetClientRect(client_rect);
	int screen_cx = client_rect.Width();
	double grid_width = (double)screen_cx / (double)length;
	double grid_height = (double)m_nGridHeight * grid_width / (double)m_nGridWidth;

	int font_size = (int)((double)m_fontSize * grid_width / (double)m_nGridWidth);
	CFont font;
	if(!IsAvailableFont(m_font.c_str()))
	{
		font.CreatePointFont(font_size*10, "System");
	}
	else
	{
		font.CreatePointFont(font_size*10, m_font.c_str());
	}

	pDC->SelectObject(&font);

	for(int i=0; i<length; i++)
	{
		CString text = m_arrSMS.GetAt(i);
		COLORREF color = m_arrColor.GetAt(i);

		//CRect rect(m_nGridWidth*i,0,m_nGridWidth*(i+1),m_nGridHeight);
		CRect rect(grid_width*i,0,grid_width*(i+1),grid_height);

		pDC->SetTextColor(color);

		pDC->DrawText(text, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

		// 테두리 (보색)
		int red = ~(m_rgbBgColor & 0x0000ff);
		int green = ~(( m_rgbBgColor & 0x00ff00 ) >> 8);
		int blue = ~(( m_rgbBgColor & 0xff0000 ) >> 16);
		pDC->Draw3dRect(rect, RGB(red,green,blue), RGB(red,green,blue));
	}
}
//
//BOOL CSMSSchedule::CreatePopup(CWnd* pParentWnd)
//{
//	int length = m_arrSMS.GetCount();
//
//	CRect client_rect;
//	GetClientRect(client_rect);
//	int screen_cx = client_rect.Width();
//	double grid_width = (double)screen_cx / (double)length;
//	double grid_height = (double)m_nGridHeight * grid_width / (double)m_nGridWidth;
//
//	client_rect.SetRect(0, 0, (int)grid_width, (int)grid_height);
//
//	if(GetSafeHwnd()==NULL)
//		return CreateEx( NULL, NULL, _T("Preview"), WS_POPUP/*|WS_CAPTION|WS_VISIBLE*/, client_rect, pParentWnd, NULL );
//
//	MoveWindow(client_rect);
//	return TRUE;
//}

bool CSMSSchedule::CreateFile()
{
	m_bGridMode = false;

	//
	if(m_fontSMS.GetSafeHandle())
	{
		m_fontSMS.DeleteObject();
	}//if

	if(!IsAvailableFont(m_font.c_str()))
	{
		m_fontSMS.CreatePointFont(m_fontSize*10, "System");
	}
	else
	{
		m_fontSMS.CreatePointFont(m_fontSize*10, m_font.c_str());
	}//if

	m_strSMS = "";
	//뒤에서부터 빈라인을 확인하여 라인숫자를 정한다.
	for(m_nLineCount = TICKER_COUNT; m_nLineCount>0; m_nLineCount--)
	{
		if(m_comment[m_nLineCount].length() > 0)
		{
			break;
		}//if
	}//for

	//각 라인을 합쳐서 하나의 문자열로 만든다.
	for(int i=0; i<m_nLineCount; i++)
	{
		m_strSMS.Append(m_comment[i].c_str());
		m_strSMS.Append("\r\n");
	}//for
	m_strSMS.Replace("&", "&&");
	m_strSMS.TrimRight();

	// <!-- 메가박스용 (2017.05.10 seventhstone)
	// sample : <WxH=1280,720>TEST
	CString str = m_strSMS;
	str.Replace("\r", "");
	str.Replace("\n", "");

	int pos = 0;
	while(true)
	{
		CString tag = str.Tokenize("<=,", pos);
		if( tag != "WxH" ) break;

		CString width = str.Tokenize("<=,", pos);
		m_nGridWidth = atoi(width);
		if( m_nGridWidth==0 ) break;

		CString height = str.Tokenize("<=,", pos);
		m_nGridHeight = atoi(height);
		if( m_nGridHeight==0 ) break;

		pos = str.Find('>');
		if( pos<0 ) break;

		m_bGridMode = true;
		m_arrSMS.RemoveAll();
		m_arrColor.RemoveAll();

		USES_CONVERSION;
		CStringW body = A2W( ((LPCSTR)str + pos + 1) ); // ANSI to UCS-2
		COLORREF font_color = m_rgbFgColor;
		bool tag_open = false;
		CString tag_string = "";
		for(int i=0; i<body.GetLength(); i++)
		{
			wchar_t unicode[2] = {0};
			unicode[0] = body.GetAt(i);

			CString ansi = W2A(unicode);

			if( ansi=="<" && tag_open==false)
			{
				tag_open = true;
				continue;
			}
			if( ansi==">" && tag_open )
			{
				tag_open = false;

				if( tag_string.Left(4) == "RGB=")
				{
					int pos2 = 0;
					while(true)
					{
						CString tag = tag_string.Tokenize("=,", pos2);
						if( tag != "RGB" ) break;

						CString str_red = tag_string.Tokenize("=,", pos2);
						if(str_red=="") break;
						int red = atoi(str_red);

						CString str_green = tag_string.Tokenize("=,", pos2);
						if(str_green=="") break;
						int green = atoi(str_green);

						CString str_blue = tag_string.Tokenize("=,", pos2);
						if(str_blue=="") break;
						int blue = atoi(str_blue);

						font_color = RGB(red,green,blue);
						break;
					}
				}
				if( tag_string=="/RGB" )
				{
					font_color = m_rgbFgColor;
				}

				tag_string = "";
				continue;
			}
			if( tag_open )
			{
				tag_string += ansi;
				continue;
			}

			m_arrSMS.Add(ansi);
			m_arrColor.Add(font_color);
		}
		break;
	}
	// -->

	return true;
}

void CSMSSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(lpszID, "bgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_bgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "fgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "font", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_font = tmpBuffer;

	GetPrivateProfileString(lpszID, "fontSize", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fontSize = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "direction", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_direction = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "align", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_align = atoi(tmpBuffer);

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}

/*
Quotation mark						&#34	&quot;
Ampersand							&#38	&amp;
Less-than sign						&#60	&lt;
Greater-than sign					&#62	&gt;
*/
/*
	for(int i=0; i<TICKER_COUNT; i++)
	{
		CString str = m_comment[i].c_str();

		str.Replace("&", "&&");
		str.Replace("&", "&amp;");
		str.Replace("\"", "&quot;");
		str.Replace("\'", "&apos;");
		str.Replace("<", "&lt;");
		str.Replace(">", "&gt;");

		m_comment[i] = str;
	}
*/
	CreateFile();
}

double CSMSSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CSMSSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CSMSSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			m_bOpen = false;
			return false;
		}//if
	}
/*
	if(!m_bFileNotExist)
	{
		CreateFile();
	}//if
*/
	m_bOpen = true;

	return m_bOpen;
}

bool CSMSSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();

		m_bOpen = false;
		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if

		if(m_fontSMS.GetSafeHandle())
		{
			m_fontSMS.DeleteObject();
		}//if
	}
	return true;
}

bool CSMSSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);
	if(!m_bOpen)
	{
		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	return true;
}

bool CSMSSchedule::Pause()
{
	Stop();

	return true;
}

bool CSMSSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

	return true;
}

bool CSMSSchedule::Save()
{
	CSchedule::Save();

	WritePrivateProfileString(m_strLoadID, "bgColor", m_bgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fgColor", m_fgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "font", m_font.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fontSize", ::ToString(m_fontSize), GetDataPath());

	return true;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHorizontalTickerSchedule

CHorizontalTickerSchedule::CHorizontalTickerSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_offset (0)
,	m_nTimerId (0)
,	m_rgbBgColor(RGB(0,0,0))
,	m_rgbFgColor(RGB(255,255,255))
,	m_bUseUnicode (false)
,	m_nShiftStep(HORIZONTAL_TICKER_CAPTION_SHIFT_STEP)
,	m_nMS(E_TICKER_NORMAL)
,	m_bExitThread(true)
,	m_hevtShift(NULL)
,	m_pthreadShift(NULL)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CHorizontalTickerSchedule::~CHorizontalTickerSchedule()
{
	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if
}


IMPLEMENT_DYNAMIC(CHorizontalTickerSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CHorizontalTickerSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_MESSAGE(CAPTION_SHIFT_ID, OnCaptionShift)
END_MESSAGE_MAP()


int CHorizontalTickerSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
//		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	CPaintDC dc(this);
	dc.SelectObject(&m_fontTicker);
	if( m_bUseUnicode )
	{
		ASSERT(dc.m_hDC != NULL);
		SIZE size;
		VERIFY(::GetTextExtentPoint32W(dc.m_hDC, m_strTickerW, m_strTickerW.GetLength(), &size));

		m_nTickerWidth = size.cx;
	}
	else
	{
		CSize text_size = dc.GetOutputTextExtent(m_strTicker);
		m_nTickerWidth = text_size.cx;
	}

	CRect client_rect;
	GetClientRect(client_rect);
	m_nWidth = client_rect.Width();
	m_nHeight = client_rect.Height();

	return 0;
}

void CHorizontalTickerSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CHorizontalTickerSchedule::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSchedule::OnTimer(nIDEvent);

	if(nIDEvent == CAPTION_SHIFT_ID)
	{
		OnCaptionShift(0,0);
	}
}

void CHorizontalTickerSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	CRect client_rect;
	GetClientRect(client_rect);

	CMemDC memDC(&dc);

	if(m_bgImage.IsValid())
	{
#if 1	
		// 바둑판
		for(int x=0; x<client_rect.Width(); x+=m_bgImage.GetWidth())
			for(int y=0; y<client_rect.Height(); y+=m_bgImage.GetHeight())
				m_bgImage.Draw2(memDC.GetSafeHdc(), x, y);
#else
		// 늘이기
		m_bgImage.Draw(memDC.GetSafeHdc(), 0, 0, client_rect.Width(), client_rect.Height());
#endif
	}
	else
	{
		memDC.FillSolidRect(client_rect, m_rgbBgColor);
	}

	if(m_bOpen && m_nTickerWidth != 0)
	{
		int nOffset = 0;
		GetOffset(&nOffset);
		CRect rect = client_rect;
		rect.left = nOffset;
		rect.right = nOffset + m_nTickerWidth;

		memDC.SetBkMode(TRANSPARENT);
		memDC.SetTextColor(m_rgbFgColor);
		memDC.SetBkColor(m_rgbBgColor);
		memDC.SelectObject(m_fontTicker);

		CRect rectText(0, 0, 0, 0);
		if(m_bUseUnicode)
		{
			::DrawTextW(memDC.m_hDC, m_strTickerW, m_strTickerW.GetLength(), &rectText, DT_CALCRECT);	//글자가 출력될 영역을 계산
		}
		else
		{
			memDC.DrawText(m_strTicker, &rectText, DT_CALCRECT);	//글자가 출력될 영역을 계산
		}//if

		int nPosY = 0, nCy = 0, nGapY = 0;
		if(client_rect.Height() <= rectText.Height())
		{
			nCy = client_rect.Height();
			nGapY = 0;
		}
		else
		{
			nCy = rectText.Height();
			nGapY = client_rect.Height() - rectText.Height();
		}//if

		//세로 방향 가운데에 문자 높이만큼만 영역을 설정
		nPosY = nGapY/2;
		rect.top = nPosY;
		rect.bottom = nPosY + nCy;
		//memDC.FillSolidRect(rect, RGB(0, 255, 0));
	

		do
		{
			if( m_bUseUnicode )
			{
				::DrawTextW(memDC.m_hDC, m_strTickerW, m_strTickerW.GetLength(), rect, DT_SINGLELINE | DT_VCENTER);
			}
			else
			{
				memDC.DrawText(m_strTicker, rect, DT_SINGLELINE | DT_VCENTER);
			}
			rect.OffsetRect(m_nTickerWidth, 0);
		}
		while(rect.left < client_rect.right);
	}
}

LRESULT CHorizontalTickerSchedule::OnCaptionShift(WPARAM wParam, LPARAM lParam)
{
	DecreaseOffset();

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Offset 값을 줄인다. \n
/////////////////////////////////////////////////////////////////////////////////
void CHorizontalTickerSchedule::DecreaseOffset()
{
	m_csOffset.Lock();
	m_offset -= m_nShiftStep;

	//if(m_offset == -m_nTickerWidth)
	if(m_offset <= -m_nTickerWidth)
	{
		m_offset = 0;
	}//if
	m_csOffset.Unlock();

	Invalidate(FALSE);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 offset 값을 반환한다. \n
/// @param (int*) pnOffset : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CHorizontalTickerSchedule::GetOffset(int* pnOffset)
{
	m_csOffset.Lock();
	(*pnOffset) = m_offset;
	m_csOffset.Unlock();
}

void CALLBACK CHorizontalTickerSchedule::timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2)
{
	::PostMessage((HWND)dwUser, CAPTION_SHIFT_ID, 0, 0);
}

bool CHorizontalTickerSchedule::CreateFile()
{
	double xscale = 1.0f, yscale = 1.0f;
	//m_pParentParentWnd->GetScale(xscale, yscale);

	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}

	if(!IsAvailableFont(m_font.c_str()))
	{
		m_fontTicker.CreatePointFont(m_fontSize*10, "System");
	}
	else
	{
		m_fontTicker.CreatePointFont(m_fontSize*10, m_font.c_str());
	}//if

	if(m_bUseUnicode)
	{
		m_strTickerW = L"";
		//CStringW header = L"▶";
		for(int i=0; i<TICKER_COUNT; i++)
		{
			if(m_commentW[i].GetLength() > 0)
			{
				//m_strTickerW.Append(header);
				m_strTickerW.Append(m_commentW[i]);
				m_strTickerW.Append(L"    ");
			}//if
		}//if
		m_strTickerW.Replace(L"&", L"&&");
	}
	else
	{
		m_strTicker = "";
		//CString header;
		//header.LoadString(IDS_TICKER_HEADER);
		for(int i=0; i<TICKER_COUNT; i++)
		{
			if(m_comment[i].length() > 0)
			{
				//m_strTicker.Append(header);
				m_strTicker.Append(m_comment[i].c_str());
				m_strTicker.Append("    ");
			}//if
		}//if
		m_strTicker.Replace("&", "&&");
	}

	if(GetSafeHwnd())
	{
		CPaintDC dc(this);
		dc.SelectObject(&m_fontTicker);
		if( m_bUseUnicode )
		{
			ASSERT(dc.m_hDC != NULL);
			SIZE size;
			VERIFY(::GetTextExtentPoint32W(dc.m_hDC, m_strTickerW, m_strTickerW.GetLength(), &size));

			m_nTickerWidth = size.cx;
		}
		else
		{
			CSize text_size = dc.GetOutputTextExtent(m_strTicker);
			m_nTickerWidth = text_size.cx;
		}

		//
		CRect client_rect;
		GetClientRect(client_rect);
		m_nWidth = client_rect.Width();
		m_nHeight = client_rect.Height();
	}

	return true;
}

int ANSItoUNICODE(LPCSTR lpszANSI, CStringW& strUnicode)
{
	WCHAR lpszOut[1024];

	::MultiByteToWideChar(949, 0, lpszANSI, -1, lpszOut, 1024);
	strUnicode = lpszOut;

	return 0;
}

void CHorizontalTickerSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	char	tmpBuffer[BUF_SIZE];

	//for(int i=0; i<TICKER_COUNT; i++)
	//{
	//	if(ciArgParser::getInstance()->isSet("+japan"))
	//	{
	//		CStringW	wstr;
	//		ANSItoUNICODE(m_comment[i].c_str(), wstr);
	//		m_commentW[i] = wstr;
	//		m_bUseUnicode = true;
	//	}
	//	else
	//		m_bUseUnicode = false;
	//}//for

	GetPrivateProfileString(lpszID, "bgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_bgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "fgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "font", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_font = tmpBuffer;

	GetPrivateProfileString(lpszID, "fontSize", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fontSize = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "playSpeed", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_playSpeed = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "direction", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_direction = atoi(tmpBuffer);


	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}

	//속도를 총 5단계로 한다.
	//빠름(1~49) >> 조금빠름(50~99) >> 보통(100~199) >> 조금느림(200~499) >> 아주느림(500~1000)
	if(m_playSpeed < 50)
	{
		//빠름(1~49)
		m_playSpeed = E_TICKER_FAST;
	}
	else if(m_playSpeed >= 50 && m_playSpeed < 100)
	{
		//조금빠름(50~99)
		m_playSpeed = E_TICKER_LITTLE_FAST;
	}
	else if(m_playSpeed >= 100 && m_playSpeed < 200)
	{
		//보통(100~199)
		m_playSpeed = E_TICKER_NORMAL;
	}
	else if(m_playSpeed >= 200 && m_playSpeed < 500)
	{
		//조금느림(200~499)
		m_playSpeed = E_TICKER_LITTLE_SLOW;
	}
	else if(m_playSpeed >= 500 && m_playSpeed <= 1000)
	{
		//아주느림(500~1000)
		m_playSpeed = E_TICKET_SLOW;
	}//if

	CreateFile();

}


double CHorizontalTickerSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CHorizontalTickerSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CHorizontalTickerSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			//m_bOpen = false;
			//return false;
		}//if
	}
/*
	if(!m_bFileNotExist)
	{
		CreateFile();
	}//if
*/
	m_bOpen = true;

	return true;
}

bool CHorizontalTickerSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();
		
		m_bOpen = false;
		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if

		if(m_fontTicker.GetSafeHandle())
		{
			m_fontTicker.DeleteObject();
		}//if
	}
	return true;
}

bool CHorizontalTickerSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);
	if(!m_bOpen)
	{
		return false;
	}//if

	//
	double xscale = 1.0f, yscale = 1.0f;
	//m_pParentParentWnd->GetScale(xscale, yscale);

	//
	m_dwStartTick = ::GetTickCount();
	m_offset = 0;
	m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;

	
	//속도를 총 5단게로 한다.
	switch(m_playSpeed)
	{
	case E_TICKER_FAST:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP*3;
			m_nMS = 20;	//시스템에 부하를 적게주는 최대속도이다.
		}
		break;
	case E_TICKER_LITTLE_FAST:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP*2;
			m_nMS = 20;
		}
		break;
	case E_TICKER_NORMAL:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;
			m_nMS = 20;
		}
		break;
	case E_TICKER_LITTLE_SLOW:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;
			m_nMS = 40;
		}
		break;
	case E_TICKET_SLOW:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;
			m_nMS = 80;
		}
		break;
	default:
		{
			m_nShiftStep = HORIZONTAL_TICKER_CAPTION_SHIFT_STEP;
			m_nMS = 20;
		}
	}//switch

#ifdef USE_MMTIMER

	m_nMS = m_playSpeed / 10 / xscale; // 스피드 보정
	//m_nMS = m_playSpeed; // 스피드 보정
	if(m_nMS < TIMER_MINIMUM_TIME) m_nMS = TIMER_MINIMUM_TIME;

	TIMECAPS tc;
	timeGetDevCaps(&tc, sizeof(TIMECAPS));

	if(m_nMS < tc.wPeriodMin) m_nMS = tc.wPeriodMin;
	else if(m_nMS > tc.wPeriodMax) m_nMS = tc.wPeriodMax;

	timeBeginPeriod(m_nMS);

	if(m_nTimerId == 0)
		m_nTimerId = timeSetEvent(m_nMS, m_nMS, CHorizontalTickerSchedule::timeProc, (DWORD)m_hWnd, TIME_PERIODIC);

	if(!m_nTimerId)
	{
		OnErrorMessage((WPARAM)"Multimedia timer setting error", 0);
	}

#else

	//__DEBUG__("\t\t\tWindow Timer Mode", m_nMS);
	//SetTimer(CAPTION_SHIFT_ID, m_nMS, NULL);
	m_pthreadShift = ::AfxBeginThread(CHorizontalTickerSchedule::CaptionShiftThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_pthreadShift->m_bAutoDelete = TRUE;
	m_bExitThread = false;
	m_hevtShift = CreateEvent(NULL, FALSE, FALSE, NULL);
	m_pthreadShift->ResumeThread();

#endif
	
	return true;
}


UINT CHorizontalTickerSchedule::CaptionShiftThread(LPVOID pParam)
{
	DWORD dwEventRet = 0;
	CHorizontalTickerSchedule* pParent = (CHorizontalTickerSchedule*)pParam;

	while(!pParent->m_bExitThread)
	{
		dwEventRet = WaitForSingleObject(pParent->m_hevtShift, pParent->m_nMS);
		if(dwEventRet == WAIT_OBJECT_0)
		{
			//__DEBUG__("Event set : CaptionShiftThread", _NULL);
			break;
		}//if

		//__DEBUG__("Shift ticker", pParent->m_nMS);
		pParent->DecreaseOffset();
		
	}//while

	CloseHandle(pParent->m_hevtShift);
	//__DEBUG__("Exit CaptionShiftThread", _NULL);

	return 1;
}


bool CHorizontalTickerSchedule::Pause()
{
	Stop();

	return true;
}

bool CHorizontalTickerSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

#ifdef USE_MMTIMER

	timeKillEvent(m_nTimerId);
	timeEndPeriod(m_nMS);
	m_nTimerId = 0;

#else

	//KillTimer(CAPTION_SHIFT_ID);
	m_bExitThread = true;
	SetEvent(m_hevtShift);

#endif

	CSchedule::Stop();

	return true;
}

bool CHorizontalTickerSchedule::Save()
{
	CSchedule::Save();

	WritePrivateProfileString(m_strLoadID, "bgColor", m_bgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fgColor", m_fgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "font", m_font.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fontSize", ::ToString(m_fontSize), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "playSpeed", ::ToString(m_playSpeed), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "direction", ::ToString(m_direction), GetDataPath());

	return true;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CVerticalTickerSchedule

CVerticalTickerSchedule::CVerticalTickerSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_offset (0)
,	m_nTickerIndex (0)
,	m_nDelayCount (0)
,	m_nTimerId (0)
,	m_rgbBgColor(RGB(0,0,0))
,	m_rgbFgColor(RGB(255,255,255))
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CVerticalTickerSchedule::~CVerticalTickerSchedule()
{
	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if
}


IMPLEMENT_DYNAMIC(CVerticalTickerSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CVerticalTickerSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_MESSAGE(CAPTION_SHIFT_ID, OnCaptionShift)
END_MESSAGE_MAP()


int CVerticalTickerSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
//		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	//
	CRect client_rect;
	GetClientRect(client_rect);
	m_nWidth = client_rect.Width();
	m_nHeight = client_rect.Height();

	return 0;
}

void CVerticalTickerSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CVerticalTickerSchedule::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSchedule::OnTimer(nIDEvent);

	if(nIDEvent == CAPTION_SHIFT_ID)
	{
		OnCaptionShift(0,0);
	}
}

void CVerticalTickerSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	double xscale = 1.0f, yscale = 1.0f;
	//m_pParentParentWnd->GetScale(xscale, yscale);

	CRect client_rect;
	GetClientRect(client_rect);

	CMemDC memDC(&dc);

	if(m_bgImage.IsValid())
	{
#if 1
		// 바둑판
		for(int x=0; x<client_rect.Width(); x+=m_bgImage.GetWidth())
			for(int y=0; y<client_rect.Height(); y+=m_bgImage.GetHeight())
				m_bgImage.Draw2(memDC.GetSafeHdc(), x, y);
#else
		// 늘이기
		m_bgImage.Draw(memDC.GetSafeHdc(), 0, 0, client_rect.Width(), client_rect.Height());
#endif
	}

	memDC.SetBkMode(1);
	memDC.SetTextColor(m_rgbFgColor);

	CFont	tmp_font;
	CSize	text_size;
	int		size;

	// ticker1
	memDC.SelectObject(&m_fontTicker);
	text_size = memDC.GetOutputTextExtent(m_strTicker1);
	size = m_fontSize;
	if(m_nWidth < text_size.cx)
	{
		do
		{
			size = (size*9)/10;
			if(tmp_font.GetSafeHandle())
			{
				tmp_font.DeleteObject();
			}//if

			if(!IsAvailableFont(m_font.c_str()))
			{
				tmp_font.CreatePointFont(size*10 * xscale, "System");
			}
			else
			{
				tmp_font.CreatePointFont(size*10 * xscale, m_font.c_str());
			}//if

			memDC.SelectObject(&tmp_font);
			text_size = memDC.GetOutputTextExtent(m_strTicker1);
		}
		while(m_nWidth < text_size.cx);
	}
	CRect rect(0, m_offset, m_nWidth, m_offset+m_nHeight);
	memDC.DrawText(m_strTicker1, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	// ticker2
	memDC.SelectObject(&m_fontTicker);
	text_size = memDC.GetOutputTextExtent(m_strTicker2);
	size = m_fontSize;
	if(m_nWidth < text_size.cx)
	{
		do
		{
			size = (size*9)/10;
			if(tmp_font.GetSafeHandle())
			{
				tmp_font.DeleteObject();
			}//if

			if(!IsAvailableFont(m_font.c_str()))
			{
				tmp_font.CreatePointFont(size*10 * xscale, "System");
			}
			else
			{
				tmp_font.CreatePointFont(size*10 * xscale, m_font.c_str());
			}//if

			memDC.SelectObject(&tmp_font);
			text_size = memDC.GetOutputTextExtent(m_strTicker2);
		}
		while(m_nWidth < text_size.cx);
	}
	rect.OffsetRect(0, m_nHeight);
	memDC.DrawText(m_strTicker2, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
}

LRESULT CVerticalTickerSchedule::OnCaptionShift(WPARAM wParam, LPARAM lParam)
{
	if(m_nDelayCount == 0)
	{
		m_offset -= VERTICAL_TICKER_CAPTION_SHIFT_STEP;

		if(m_offset == -m_nHeight)
		{
			m_offset = 0;
			m_nDelayCount = 30;

			m_strTicker1 = m_strTicker2;
			m_nTickerIndex++;
			if(m_nTickerIndex > m_nMaxTicker)
				m_nTickerIndex = 0;
			m_strTicker2 = m_comment[m_nTickerIndex].c_str();
		}

		Invalidate(FALSE);
	}
	else 
		m_nDelayCount--;

	return 0;
}

void CALLBACK CVerticalTickerSchedule::timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2)
{
	::PostMessage((HWND)dwUser, CAPTION_SHIFT_ID, 0, 0);
}

bool CVerticalTickerSchedule::CreateFile()
{
	double xscale = 1.0f, yscale = 1.0f;
//	m_pParentParentWnd->GetScale(xscale, yscale);

	//
	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if

	if(!IsAvailableFont(m_font.c_str()))
	{
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, "System");
	}
	else
	{
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, m_font.c_str());
	}//if

	for(int i=0; i<TICKER_COUNT; i++)
	{
		if(m_comment[i].length() > 0)
		{
			m_nMaxTicker = i;
		}
	}

	if(GetSafeHwnd())
	{
		//
		CRect client_rect;
		GetClientRect(client_rect);
		m_nWidth = client_rect.Width();
		m_nHeight = client_rect.Height();
	}

	return true;
}

void CVerticalTickerSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(lpszID, "bgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_bgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "fgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "font", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_font = tmpBuffer;

	GetPrivateProfileString(lpszID, "fontSize", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fontSize = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "playSpeed", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_playSpeed = atoi(tmpBuffer);

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}

	CreateFile();
}


double CVerticalTickerSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CVerticalTickerSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CVerticalTickerSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			m_bOpen = false;

			return false;
		}//if
	}
/*
	if(!m_bFileNotExist)
	{
		CreateFile();
	}//if
*/
	m_bOpen = true;

	return true;
}

bool CVerticalTickerSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();
		
		m_bOpen = false;
		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if

		if(m_fontTicker.GetSafeHandle())
		{
			m_fontTicker.DeleteObject();
		}//if
	}
	return true;
}

bool CVerticalTickerSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);

	if(!m_bOpen)
	{
		return false;
	}//if

	m_strTicker1 = "";
	m_strTicker2 = m_comment[0].c_str();

	double xscale = 1.0f, yscale = 1.0f;
//	m_pParentParentWnd->GetScale(xscale, yscale);

	//
	m_dwStartTick = ::GetTickCount();

	m_nMS = m_playSpeed / 5 / yscale; // 스피드 보정
	if(m_nMS < TIMER_MINIMUM_TIME) m_nMS = TIMER_MINIMUM_TIME;

#ifdef USE_MMTIMER

	TIMECAPS tc;
	timeGetDevCaps(&tc, sizeof(TIMECAPS));

	if(m_nMS < tc.wPeriodMin) m_nMS = tc.wPeriodMin;
	else if(m_nMS > tc.wPeriodMax) m_nMS = tc.wPeriodMax;

	timeBeginPeriod(m_nMS);

	if(m_nTimerId == 0)
		m_nTimerId = timeSetEvent(m_nMS, m_nMS, CVerticalTickerSchedule::timeProc, (DWORD)m_hWnd, TIME_PERIODIC);

	if(!m_nTimerId)
	{
	}

#else

	//__DEBUG__("\t\t\tWindow Timer Mode", _NULL);
	SetTimer(CAPTION_SHIFT_ID, m_nMS, NULL);

#endif


	return true;
}

bool CVerticalTickerSchedule::Pause()
{
	Stop();

	return true;
}

bool CVerticalTickerSchedule::Stop(bool bHide)
{
#ifdef USE_MMTIMER

	//
	timeKillEvent(m_nTimerId);
	timeEndPeriod(m_nMS);
	m_nTimerId = 0;

#else

	KillTimer(CAPTION_SHIFT_ID);

#endif

	CSchedule::Stop(bHide);

	return true;
}

bool CVerticalTickerSchedule::Save()
{
	CSchedule::Save();

	WritePrivateProfileString(m_strLoadID, "bgColor", m_bgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fgColor", m_fgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "font", m_font.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fontSize", ::ToString(m_fontSize), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "playSpeed", ::ToString(m_playSpeed), GetDataPath());

	return true;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CRSSSchedule

CRSSSchedule::CRSSSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CFlashSchedule (pParentWnd, lpszID, bDefaultSchedule)
, m_strRssFlashPath("")
, m_nRssType(0)
, m_strErrorString("")
, m_strCategoryString("")
, m_strLineCount("")
{
	SetSchedule(lpszID);
}

CRSSSchedule::~CRSSSchedule()
{
}


IMPLEMENT_DYNAMIC(CRSSSchedule, CFlashSchedule)

BEGIN_MESSAGE_MAP(CRSSSchedule, CFlashSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
END_MESSAGE_MAP()

void CRSSSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
/*
	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		CRect rtClient;
		GetClientRect(rtClient);

		CMemDC memDC(&dc);
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}//if
*/
}


int CRSSSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	
	return 0;
}


bool CRSSSchedule::CreateFile()
{
	CRect rtClient;
	GetClientRect(&rtClient);
	m_width = rtClient.Width();
	m_height = rtClient.Height();

	//Rss 플래쉬 파일의 경로
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);
	m_strRssFlashPath = szDrive;
	m_strRssFlashPath += szPath;

	m_nRssType = atoi(m_comment[2].c_str());		//Rss 종류
	m_strErrorString = m_comment[3].c_str();		//장애 문자
	m_strCategoryString = m_comment[4].c_str();		//카테고리
	m_strLineCount = m_comment[5].c_str();			//라인 수

	unsigned short LangType = PRIMARYLANGID(GetSystemDefaultLangID());  //프라이머리 언어만 뽑는다. 
	if(LangType==LANG_JAPANESE)    //일문 윈도우 
	{
		m_strRssFlashPath += "flash\\JP_";
	}
	else
	{
		m_strRssFlashPath += "flash\\";
	}//if

	switch(m_nRssType)
	{
	case E_RSS_ONE_LINE:		//한 라인 Rss
		{
			m_strRssFlashPath += "Con_rss.swf";
		}
		break;
	case E_RSS_MULTI_LINE:		//Multi line Rss
		{
			m_strRssFlashPath += "Con_rss_line.swf";
		}
		break;
	case E_RSS_DETAIL:		//내용을 보여주는 Rss
		{
			m_strRssFlashPath += "Con_rss_description.swf";
		}
		break;
	default:
		{
			m_strRssFlashPath += "Con_rss.swf";
		}
	}//switch

	//RSS 플래쉬 파일이 존재하여야 한다.
	CFileStatus status;
	if(CFile::GetStatus(m_strRssFlashPath, status ) == FALSE)
	{
		//SetNoContentsFile();
		return false;
	}//if

/*
	//장애 이미지의 경로
	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}//if
*/

	return true;
}


void CRSSSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(lpszID, "bgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_bgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "fgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "fontSize", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fontSize = atoi(tmpBuffer);
}



bool CRSSSchedule::Play()
{
	CSchedule::Play();

	if(!m_bOpen)
	{
		CSchedule::Stop();

		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

/*
	if(m_bFileNotExist)
	{
		return true;
	}//if
*/

	m_csFlash.Lock();

	//if(m_pFlash == NULL && !m_bFileNotExist)
	if(m_pFlash == NULL)
	{
		static int id = 0xd000;

		m_pFlash = new CShockwaveFlash();

		CRect client_rect;
		GetClientRect(client_rect);

		BOOL ret = m_pFlash->Create("Flash", WS_CHILD | WS_VISIBLE, client_rect, this, id--);
		if(ret == FALSE)
		{
			CSchedule::Stop();

			return false;
		}//if

		// skpark 2009.6.19 속도개선을 위해서 Quality 조정
		m_pFlash->put_Quality2("high");
	}

	if(m_bPlaying == false)		// 재실행방지 -> 동일多오픈시 플래시 버그 방지 때문
	{
		m_pFlash->LoadMovie(0, m_strRssFlashPath);
		m_pFlash->ShowWindow(SW_SHOW);
		
		m_bPlaying = true;
	}
	else
	{
		if(m_pFlash != NULL)
		{
			m_pFlash->Rewind();
			m_pFlash->Play();
		}//if
	}//if

	if(m_pFlash != NULL)
	{
		//fscommand를 사용하지 않으므로 플래쉬가 로딩이 완료될때까지 기다린다.
		Sleep(500);

		CString strFgColor = "0x";
		strFgColor += m_fgColor.c_str();

		CString strBgColor = "0x";
		strBgColor += m_bgColor.c_str();

		CString strCall;
		if(m_nRssType == E_RSS_MULTI_LINE)
		{
			strCall.Format("<invoke name=\'makeRss\'><arguments><string>%s</string><string>%s</string><string>%d</string><string>%s</string>\
<string>%s</string><string>%d</string><string>%d</string><string>%s</string><string>%s</string><string>%s</string></arguments></invoke>",
			m_comment[0].c_str(), m_strMediaFullPath, m_fontSize, strFgColor, strBgColor, m_width, m_height,
			m_strErrorString, m_strCategoryString, m_strLineCount);
		}
		else
		{
			strCall.Format("<invoke name=\'makeRss\'><arguments><string>%s</string><string>%s</string><string>%d</string><string>%s</string>\
<string>%s</string><string>%d</string><string>%d</string><string>%s</string><string>%s</string></arguments></invoke>",
			m_comment[0].c_str(), m_strMediaFullPath, m_fontSize, strFgColor, strBgColor, m_width, m_height,
			m_strErrorString, m_strCategoryString);
		}//if

		m_pFlash->CallFunction(strCall);
	}//if

	m_csFlash.Unlock();

	return true;
}


bool CRSSSchedule::Save()
{
	CFlashSchedule::Save();

	CString strTmp;
	WritePrivateProfileString(m_strLoadID, "bgColor", m_bgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fgColor", m_fgColor.c_str(), GetDataPath());
	strTmp.Format("%d", m_fontSize);
	WritePrivateProfileString(m_strLoadID, "fontSize", strTmp, GetDataPath());

	return true;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CWizardSchedule
CWizardSchedule::CWizardSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CFlashSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_wizardXML("")
,	m_wizardFiles("")
,	m_strWizardFlashPath("")
,	m_pFlashEventConnectionPoint(NULL)
,	m_pFlashEventSink(NULL)
{
	SetSchedule(lpszID);
}

CWizardSchedule::~CWizardSchedule()
{
	//CloseFile();
}

IMPLEMENT_DYNAMIC(CWizardSchedule, CFlashSchedule)

BEGIN_MESSAGE_MAP(CWizardSchedule, CFlashSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CWizardSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	char	tmpBuffer[BUF_SIZE];

	GetPrivateProfileString(lpszID, "wizardXML", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_wizardXML = tmpBuffer;

	GetPrivateProfileString(lpszID, "wizardFiles", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_wizardFiles = tmpBuffer;
}


void CWizardSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.
/*
	if(m_bFileNotExist && m_bgImage.IsValid())
	{
		CRect rtClient;
		GetClientRect(rtClient);

		CMemDC memDC(&dc);
		memDC.FillSolidRect(rtClient, RGB(255, 255, 255));
		m_bgImage.Draw2(memDC.GetSafeHdc(), 10, 10);
	}//if
*/
}


int CWizardSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	return 0;
}


bool CWizardSchedule::CreateFile()
{
	//Rss 플래쉬 파일의 경로
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);
	m_strWizardFlashPath = szDrive;
	m_strWizardFlashPath += szPath;
	m_strWizardFlashPath += "\\FlashWizard\\";
	m_strWizardFlashPath += m_comment[0].c_str();

	//Wizard 플래쉬 파일이 존재하여야 한다.
	CFileStatus status;
	if(CFile::GetStatus(m_strWizardFlashPath, status ) == FALSE)
	{
		//SetNoContentsFile();
	}//if
/*
	//장애 이미지의 경로
	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}//if
*/
	return true;
}


bool CWizardSchedule::Play(bool bResume)
{
	CSchedule::Play();

	if(!m_bOpen)
	{
		CSchedule::Stop();

		return false;
	}//if

	m_dwStartTick = ::GetTickCount();
/*
	if(m_bFileNotExist)
	{
		return true;
	}//if
*/
	m_csFlash.Lock();

	if(m_pFlash == NULL)
	{
		static int id = 0xd000;

		m_pFlash = new CShockwaveFlash();

		CRect client_rect;
		GetClientRect(client_rect);

		BOOL ret = m_pFlash->Create("Flash", WS_CHILD | WS_VISIBLE, client_rect, this, id--);
		if(ret == FALSE || m_pFlash->GetSafeHwnd() == NULL)
		{
			delete m_pFlash;
			m_pFlash = NULL;
			CSchedule::Stop();

			return false;
		}//if

		// skpark 2009.6.19 속도개선을 위해서 Quality 조정
		m_pFlash->put_Quality2("high");

		ConnectFlashEvent();
	}//if

	if(m_bPlaying == false && m_pFlash)		// 재실행방지 -> 동일多오픈시 플래시 버그 방지 때문
	{
		m_pFlash->LoadMovie(0, m_strWizardFlashPath);
		m_pFlash->ShowWindow(SW_SHOW);
		
		m_bPlaying = true;
	}
	else
	{
		if(m_pFlash != NULL)
		{
			m_pFlash->Rewind();
			m_pFlash->Play();
		}//if
	}//if

	if(/*m_pFlash != NULL &&*/m_wizardXML.length() != 0)
	{
		//fscommand를 사용하지 않으므로 플래쉬가 로딩이 완료될때까지 기다린다.
		//Sleep(500);
		
		CString strCall;

		//strCall.Format("<invoke name=\'makeFlash\'><arguments><string>%s</string></arguments></invoke>", m_wizardXML.c_str());
		strCall.Format("<invoke name=\'makeFlash\'><arguments><string>%s</string><string>%s</string></arguments></invoke>",
							m_wizardXML.c_str(), m_contentsId.c_str());

		strCall.Remove(0x09);
		strCall.Replace(" xml:space=\"preserve\"", "");
		strCall.Replace(" xmlns=\"http://www.w3.org/XML/1998/namespace\"", "");
		m_pFlash->CallFunction(strCall);
	}//if

	m_csFlash.Unlock();

	return true;
}

bool CWizardSchedule::Stop(bool bHide)
{
	if(m_bPlaying == true)	// 재실행방지 -> 동일多오픈시 플래시 버그 방지 때문
	{
		m_csFlash.Lock();

		if(m_pFlash != NULL)
		{
			DisConnectFlashEvent();
//			m_pFlash->StopPlay();
//			m_pFlash->Stop();
			delete m_pFlash;
			m_pFlash = NULL;
		}
		m_bPlaying = false;

		m_csFlash.Unlock();
	}

	CSchedule::Stop();

	return true;
}


bool CWizardSchedule::Save()
{
	CFlashSchedule::Save();

	CString strTmp;
	WritePrivateProfileString(m_strLoadID, "wizardXML", m_wizardXML.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "wizardFiles", m_wizardFiles.c_str(), GetDataPath());
	
	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Flash 이벤트를 연결한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CWizardSchedule::ConnectFlashEvent()
{
	if(!m_pFlash)
	{
		return false;
	}//if

	DisConnectFlashEvent();

	LPUNKNOWN pUnk = m_pFlash->GetControlUnknown();
	if(!pUnk)
	{
		return false;
	}//if

	IShockwaveFlash* pFlash = NULL;
	HRESULT hr = pUnk->QueryInterface(__uuidof(IShockwaveFlash), (void **)&pFlash);
	if(!SUCCEEDED(hr)) 
  	{ 
		return false;
	}//if

	IConnectionPointContainer* lpContainer = NULL;
	_IShockwaveFlashEvents* pFlashEvent = NULL;

	hr = pFlash->QueryInterface(IID_IConnectionPointContainer, (void **)&lpContainer);
	if(!SUCCEEDED(hr)) 
  	{ 
		return false;
	}//if

	hr = lpContainer->FindConnectionPoint(ShockwaveFlashObjects::DIID__IShockwaveFlashEvents, &m_pFlashEventConnectionPoint);
	if(!SUCCEEDED(hr)) 
  	{ 
		return false;
	}//if

	m_pFlashEventSink = new CFlashEventSink(this);
  	
  	m_pFlashEventSink->QueryInterface(IID_IUnknown, (void**)&pUnk);
  	if(pUnk == NULL)
	{
  		return false;
	}//if
  		
  	// Setup advisory connection!
  	hr = m_pFlashEventConnectionPoint->Advise(pUnk, &m_pFlashEventSink->m_Cookie);
  	if(!SUCCEEDED(hr))
	{
  		return false;
	}//if

	//lpConPoint->Advise((ShockwaveFlashObjects::_IShockwaveFlashEvents *)pFlashEvent, &dwConPointId);

	lpContainer->Release();

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Flash 이벤트를 연결을 해제한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CWizardSchedule::DisConnectFlashEvent()
{
	if(m_pFlashEventConnectionPoint)
	{
		m_pFlashEventConnectionPoint->Unadvise(m_pFlashEventSink->m_Cookie); // releases cookie	
 		m_pFlashEventConnectionPoint->Release();
		m_pFlashEventConnectionPoint = NULL;
	}//if

	if(m_pFlashEventSink)
	{
		m_pFlashEventSink->Release();
		m_pFlashEventSink = NULL;
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Flash로 부터 콘텐츠 Id를 받는다. \n
/// @param (CString) strContentsId : (in) 콘텐츠 Id
/// @param (CString) strIsCreated : (in) 생성여부 (0 : 새로만드는 플래쉬 -> XML 데이터를 넘겨줘야함, 1: 이미 만들어진 플래쉬 -> XML 데이터를 넘겨주지말아야 함)
/////////////////////////////////////////////////////////////////////////////////
void CWizardSchedule::RecvFsCommand(CString strContentsId, CString strIsCreated)
{
	//if(strContentsId.Find("Contents_", 0) == -1)
	//{
	//	strContentsId.Format("Contents_%s", strContentsId);
	//}//if

	/*char cBuffer[BUF_SIZE] = { 0x00 };
	CString strPath = GetAppPath();
	strPath += "config\\";
	strPath += ::GetHostName();
	strPath += ".ini";
	GetPrivateProfileString(strContentsId, "wizardXML", "", cBuffer, BUF_SIZE, strPath);*/

	/*
	if(strIsCreated == "0")
	{
		CString strXML =  CContentsInfoRef::getInstance()->GetWizardXml(strContentsId);

		if(m_pFlash != NULL && strXML.GetLength() != 0)
		{
			CString strCall;
			strCall.Format("<invoke name=\'makeFlash\'><arguments><string>%s</string></arguments></invoke>", strXML);
			strCall.Remove(0x09);
			strCall.Replace(" xml:space=\"preserve\"", "");
			strCall.Replace(" xmlns=\"http://www.w3.org/XML/1998/namespace\"", "");
			__DEBUG__(strCall, _NULL);
			m_pFlash->CallFunction(strCall);
		}//if
	}//if
	*/
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CTypingTickerSchedule

CTypingTickerSchedule::CTypingTickerSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_nCommentIndex (0)
,	m_rgbBgColor(RGB(0,0,0))
,	m_rgbFgColor(RGB(255,255,255))
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CTypingTickerSchedule::~CTypingTickerSchedule()
{
	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if
}


IMPLEMENT_DYNAMIC(CTypingTickerSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CTypingTickerSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


int CTypingTickerSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
//		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	GetClientRect(m_rectTicker);
	m_rectTicker.DeflateRect(10,10);

	return 0;
}

void CTypingTickerSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CTypingTickerSchedule::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSchedule::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case CAPTION_NEXT_ID:
		KillTimer(CAPTION_NEXT_ID);

		if(m_nCommentIndex > m_nCommentCount)
			m_nCommentIndex = 0;

		m_wstrTickerSource = m_comment[m_nCommentIndex].c_str();

		m_nTickerIndex = 0;
		m_nCommentIndex++;

		SetTimer(CAPTION_CLEAR_ID, CAPTION_CLEAR_TIME, NULL);
		break;

	case CAPTION_CLEAR_ID:
		m_wstrTickerShow = "";
		Invalidate(FALSE);
		SetTimer(CAPTION_SHIFT_ID, m_nMS, NULL);
		break;

	case CAPTION_SHIFT_ID:
		m_nTickerIndex++;
		if(m_nTickerIndex > m_wstrTickerSource.GetLength())
		{
			KillTimer(CAPTION_SHIFT_ID);
			SetTimer(CAPTION_NEXT_ID, CAPTION_NEXT_TIME, NULL);
		}
		m_wstrTickerShow = m_wstrTickerSource.Left(m_nTickerIndex);
		Invalidate(FALSE);
		break;
	}
}

void CTypingTickerSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	if(m_bOpen)
	{
		CRect client_rect;
		GetClientRect(client_rect);

		//
		CMemDC memDC(&dc);

		if(m_bgImage.IsValid())
		{
#if 1	
			// 바둑판
			for(int x=0; x<client_rect.Width(); x+=m_bgImage.GetWidth())
				for(int y=0; y<client_rect.Height(); y+=m_bgImage.GetHeight())
					m_bgImage.Draw2(memDC.GetSafeHdc(), x, y);
#else
			// 늘이기
			m_bgImage.Draw2(memDC.GetSafeHdc(), 0, 0, client_rect.Width(), client_rect.Height());
#endif
		}

		memDC.SelectObject(&m_fontTicker);
		memDC.SetBkMode(1); // TRANSPARENT
		memDC.SetTextColor(m_rgbFgColor);
		memDC.DrawText(m_wstrTickerShow, m_rectTicker, DT_MODIFYSTRING);
	}
}

bool CTypingTickerSchedule::CreateFile()
{
	double xscale = 1.0f, yscale = 1.0f;
//	m_pParentParentWnd->GetScale(xscale, yscale);

	if(m_fontTicker.GetSafeHandle())
	{
		m_fontTicker.DeleteObject();
	}//if

	if(!IsAvailableFont(m_font.c_str()))
	{
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, "System");
	}
	else
	{
		m_fontTicker.CreatePointFont(m_fontSize*10 * xscale, m_font.c_str());
	}//if

	return true;
}

void CTypingTickerSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);

	char	tmpBuffer[BUF_SIZE];

	m_nCommentCount = 0;
	for(int i=0; i<TICKER_COUNT; i++)
	{
		if(m_comment[i].length() != 0)
		{
			m_nCommentCount = i;
		}//if
	}//for

	GetPrivateProfileString(lpszID, "bgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_bgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "fgColor", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fgColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "font", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_font = tmpBuffer;

	GetPrivateProfileString(lpszID, "fontSize", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_fontSize = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "playSpeed", "", tmpBuffer, BUF_SIZE, GetDataPath());
	m_playSpeed = atoi(tmpBuffer);

	if(m_bgColor.length() > 1)
	{
		m_rgbBgColor = ::GetColorFromString(m_bgColor.c_str()+1);
	}
	if(m_fgColor.length() > 1)
	{
		m_rgbFgColor = ::GetColorFromString(m_fgColor.c_str()+1);
	}

	CreateFile();
}


double CTypingTickerSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CTypingTickerSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CTypingTickerSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			m_bOpen = false;

			return false;
		}//if
	}
/*
	if(!m_bFileNotExist)
	{
		CreateFile();
	}//if
*/
	m_bOpen = true;

	return true;
}

bool CTypingTickerSchedule::CloseFile()
{
	if(m_bOpen)
	{
		Stop();
	
		m_bOpen = false;
		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if

		if(m_fontTicker.GetSafeHandle())
		{
			m_fontTicker.DeleteObject();
		}//if
	}
	return true;
}

bool CTypingTickerSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);

	if(!m_bOpen)
	{
		return false;
	}//if

	double xscale = 1.0f, yscale = 1.0f;
//	m_pParentParentWnd->GetScale(xscale, yscale);

	m_dwStartTick = ::GetTickCount();

	m_nMS = m_playSpeed / xscale; // 스피드 보정
	if(m_nMS < TIMER_MINIMUM_TIME) m_nMS = TIMER_MINIMUM_TIME;

	SetTimer(CAPTION_NEXT_ID, CAPTION_NEXT_TIME, NULL);

	return true;
}

bool CTypingTickerSchedule::Pause()
{
	Stop();

	return true;
}

bool CTypingTickerSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

	KillTimer(CAPTION_SHIFT_ID);
	KillTimer(CAPTION_NEXT_ID);

	return true;
}

bool CTypingTickerSchedule::Save()
{
	CSchedule::Save();

	WritePrivateProfileString(m_strLoadID, "bgColor", m_bgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fgColor", m_fgColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "font", m_font.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "fontSize", ::ToString(m_fontSize), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "playSpeed", ::ToString(m_playSpeed), GetDataPath());

	return true;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CURLSchedule

CURLSchedule::CURLSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_bNavigateError(false)
,	m_nErrorCode(0)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CURLSchedule::~CURLSchedule()
{
}

IMPLEMENT_DYNAMIC(CURLSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CURLSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CURLSchedule, CSchedule)
	ON_EVENT(CURLSchedule, 0xf000, 259/*DISPID_DOCUMENTCOMPLETE*/, DocumentCompleteWebBrowser, VTS_DISPATCH VTS_PVARIANT)
	ON_EVENT(CURLSchedule, 0xf000, 252/*DISPID_NAVIGATECOMPLETE2*/, NavigateComplete2, VTS_DISPATCH VTS_PVARIANT)
	ON_EVENT(CURLSchedule, 0xf000, 271/*DISPID_NAVIGATEERROR*/, NavigateError, VTS_DISPATCH VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PBOOL)
END_EVENTSINK_MAP()


void CURLSchedule::NavigateComplete2(LPDISPATCH pDisp, VARIANT* URL)
{
	if(!m_bOpen)
	{
		return;
	}//if

	if(m_bNavigateError)
	{
		return;
	}//if

	if(m_WebBrowser.get_Application() == pDisp)
	{
		//__DEBUG__("NavigateComplete2", _NULL);

		m_WebBrowser.Invalidate(FALSE);
	}//if
}

void CURLSchedule::NavigateError(LPDISPATCH pDisp, VARIANT* pvURL, VARIANT* pvFrame, VARIANT* pvStatusCode, VARIANT_BOOL* pvbCancel)
{
	if(!m_bOpen)
	{
		return;
	}//if

	if(m_WebBrowser.get_Application() == pDisp)
	{
		m_nErrorCode = pvStatusCode->lVal;
		//__DEBUG__("NavigateError code", m_nErrorCode);
		m_bNavigateError = true;
		m_WebBrowser.Stop();
		if(m_bgImage.IsValid())
		{
			m_WebBrowser.MoveWindow(0, 0, 0, 0);
			Invalidate(FALSE);
		}//if
	}//if
}

void CURLSchedule::DocumentCompleteWebBrowser(LPDISPATCH pDisp, VARIANT* URL)
{
	if(!m_bOpen)
	{
		return;
	}//if

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	//pDisp == get_Application() 일때가 최종 프레임이 호출된 상태입니다
	if(m_WebBrowser.get_Application() != pDisp)
	{
		return;
	}//if

/*
	// IE안의 스크롤바 제거
	IDispatch *pDisp2 = m_webBrowser.get_Document();

	CString strOption = _T("no");   //[ yes | no | auto ]

	if (pDisp2 != NULL)
	{
		IHTMLDocument2* pHTMLDocument2;
		HRESULT hr;
		hr = pDisp2->QueryInterface(IID_IHTMLDocument2, (void**)&pHTMLDocument2);
		if(hr == S_OK)
		{
			IHTMLElement *pIElement;
			hr = pHTMLDocument2->get_body(&pIElement);
			if(hr == S_OK)
			{
				IHTMLBodyElement *pIBodyElement;
				hr = pIElement->QueryInterface(IID_IHTMLBodyElement, (void**)&pIBodyElement);
				if(hr == S_OK)
				{

					BSTR bstr;
					bstr = strOption.AllocSysString();
					hr = pIBodyElement->put_scroll(bstr);
				}//if
				pIBodyElement->Release();
			}//if
			pIElement->Release();
		}
		pDisp2->Release ();
	} 
*/
}


int CURLSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	m_strMediaFullPath = GetFilePath();

	return 0;
}

void CURLSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

CString CURLSchedule::GetFilePath()
{
	//예전에는 m_filename이 URL을 나타냈으나
	//배경 이미지를 지원하기 위하여
	//m_filename는 배경 이미지를, m_comment1이 URL을 나타내도록 한다.
	CString strFileName = m_filename.c_str();
	if(strFileName.Find("http", 0) != -1)
	{
		//파일 이름에 "http"가 있다면 URL을 나타낸다.
		m_comment[0] = m_filename.c_str();
		m_filename = "";
	}//if

	return m_filename.c_str();
}

void CURLSchedule::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	if(m_bNavigateError)
	{
		if(m_bgImage.IsValid())
		{
			CRect client_rect;
			GetClientRect(client_rect);

			CMemDC memDC(&dc);
			m_bgImage.Draw2(memDC.GetSafeHdc(), 0, 0, client_rect.Width(), client_rect.Height());
		}//if
	}//if
}


bool CURLSchedule::CreateFile()
{
	return true;
}

void CURLSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);
}


double CURLSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CURLSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CURLSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}//if

	CRect client_rect;
	GetClientRect(client_rect);
	BOOL ret = m_WebBrowser.Create("URL", WS_CHILD | WS_VISIBLE , client_rect, this, 0xf000);
	if(ret == FALSE)
	{
		//WritePlayLog(m_contentsId.c_str(),m_contentsName.c_str(),"FAIL", m_runningTime, m_strLogFileString, "", "Web control create fail");
		//CSchedule::Stop();
		//__ERROR__("Web control create fail !!!", m_scheduleId.c_str());
		m_bOpen = false;

		return false;
	}//if	

	CreateFile();

	if(m_strMediaFullPath.GetLength() != 0)
	{
		CString strExt = FindExtension(m_strMediaFullPath);
		int nImgType = CxImage::GetTypeIdFromName(strExt);
		/*
		if(!m_bgImage.Load(m_strMediaFullPath, nImgType))
		{
			m_bOpen = false;
			return false;
		}//if
		*/
		m_bgImage.Load(m_strMediaFullPath, nImgType);
	}

	m_bOpen = true;

	return true;
	//return false;
}

bool CURLSchedule::CloseFile()
{
	if(m_bOpen)
	{
		m_bOpen = false;
		Stop();

		//m_webBrowser.DestroyWindow();			//왜 죽는걸까....

		if(m_bgImage.IsValid())
		{
			m_bgImage.Destroy();
		}//if
	}//if

	return true;
}

bool CURLSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);

	if(!m_bOpen)
	{
		return false;
	}//if

	//Sets or gets a value that indicates whether the object can display dialog boxes
	m_WebBrowser.put_Silent(TRUE);
	m_bNavigateError = false;

	m_dwStartTick = ::GetTickCount();

	//m_filename는 배경 이미지를 나타내고
	//m_comment[0]가 URL을 나타낸다.
	//m_webBrowser.Navigate(m_strMediaFullPath, NULL, NULL, NULL, NULL);
	m_WebBrowser.Navigate(m_comment[0].c_str(), NULL, NULL, NULL, NULL);
	CRect client_rect;
	GetClientRect(client_rect);
	m_WebBrowser.MoveWindow(&client_rect);

	return true;
}

bool CURLSchedule::Pause()
{
	Stop();

	return true;
}

bool CURLSchedule::Stop(bool bHide)
{
	if(m_WebBrowser.GetSafeHwnd())
	{
		m_WebBrowser.Stop();
		m_WebBrowser.MoveWindow(0, 0, 0, 0);
	}//if

	CSchedule::Stop();

	return true;
}

bool CURLSchedule::Save()
{
	CSchedule::Save();

	return true;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CFlashSchedule

CFlashSchedule::CFlashSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_bPlaying(false)
,	m_bEventMode (false)
,	m_nMaxProbability (0)
,	m_pFlash (NULL)
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
}

CFlashSchedule::~CFlashSchedule()
{
}

IMPLEMENT_DYNAMIC(CFlashSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CFlashSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

int CFlashSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	m_strMediaFullPath = GetFilePath();

	return 0;
}

void CFlashSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

CString CFlashSchedule::GetFilePath()
{
	CString LocalFullPath, LocalTempFullPath;
//	GetLocalPath(LocalFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
/*
	if(LocalFullPath.GetLength() > 0 && LocalFullPath.GetAt(0) == '.')
	{
		char buf[MAX_PATH];
		::ZeroMemory(buf, sizeof(buf));
		strcpy(buf, ::GetAppPath());

		strcat(buf, "\\");
		strcat(buf, LocalFullPath);
		LocalFullPath = buf;

		LocalFullPath.Replace("\\\\", "\\");
	}
*/
	return LocalFullPath;
}

bool CFlashSchedule::CreateFile()
{
	//이벤트 모드에서는 파일명이 다르므로 확인하지 않는다.
	if(m_bEventMode)
	{
		return true;
	}//if

	if(_access(m_strMediaFullPath, 00) != 0)
	{
		return false;
	}
	return true;
}

void CFlashSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);
}


double CFlashSchedule::GetTotalPlayTime()
{
	if(m_bDefaultSchedule)
		return (double)m_runningTime*1000; // sec -> millisec
	else
		return (double)DBL_MAX;
}

double CFlashSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	return (double)(dwEndTick - m_dwStartTick);
}

bool CFlashSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}

	if(CreateFile())
	{
		m_bOpen = true;
		return true;
	}

	return false;
}

bool CFlashSchedule::CloseFile()
{
	if(m_bPlaying)
	{
		m_bOpen = false;
		//m_bPlaying = false;
		Stop();
/*
		if(m_pFlash != NULL)
		{
			m_pFlash->DestroyWindow();
			delete m_pFlash;
			m_pFlash = NULL;
		}
*/
	}
	return true;
}

bool CFlashSchedule::Play(bool bResume)
{
	CSchedule::Play(bResume);		// 재실행방지 -> 동일多오픈시 플래시 버그 방지 때문

	if(!m_bOpen)
	{
		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

/*
	if(m_bFileNotExist)
	{
		return true;
	}//if
*/

	m_csFlash.Lock();

	if(m_pFlash == NULL)
	{
		static int id = 0xd000;

		m_pFlash = new CShockwaveFlash();

		CRect client_rect;
		GetClientRect(client_rect);

		BOOL ret = m_pFlash->Create("Flash", WS_CHILD | WS_VISIBLE, client_rect, this, id--);
		if(ret == FALSE)
		{
			CSchedule::Stop();

			return false;
		}
		// skpark 2009.6.19 속도개선을 위해서 Quality 조정
		m_pFlash->put_Quality2("high");
	}

	if(m_bPlaying == false)		// 재실행방지 -> 동일多오픈시 플래시 버그 방지 때문
	{
		if(m_pFlash != NULL)
		{
			//__DEBUG__("LoadMovie", _NULL);

			if(m_bEventMode)
			{
				bool find = false;
				bool all_fault = true;

				do
				{
					int rnd = rand() % m_nMaxProbability;

					int count = m_flashList.GetCount();
					for(int i=0; i<count; i++)
					{
						FLASH_OBJECT& obj = m_flashList.GetAt(i);
						obj.Load();
						if(obj.CheckCount())
							all_fault = false;
						if( obj.prob_min <= rnd && rnd < obj.prob_max )
						{
							if(obj.IncreaseCount())
							{
								find = true;
								obj.Save();
								m_strMediaFullPath = obj.fullPath;
								break;
							}
						}
					}

					if(all_fault)
					{
						m_strMediaFullPath = "";
						break;
					}
				}
				while(find == false);
			}
			m_pFlash->LoadMovie(0, m_strMediaFullPath);
			m_pFlash->ShowWindow(SW_SHOW);
		}
		m_bPlaying = true;
	}
	else
	{
		if(m_pFlash != NULL)
		{
			m_pFlash->Rewind();
			m_pFlash->Play();
		}
	}

	m_csFlash.Unlock();

	return true;
}

bool CFlashSchedule::Pause()
{
	Stop();

	return true;
}

bool CFlashSchedule::Stop(bool bHide)
{
	if(m_bPlaying == true)	// 재실행방지 -> 동일多오픈시 플래시 버그 방지 때문
	{
		m_csFlash.Lock();

		if(m_pFlash != NULL)
		{
//			m_pFlash->StopPlay();
			m_pFlash->Stop();
			delete m_pFlash;
			m_pFlash = NULL;
		}
		m_bPlaying = false;

		m_csFlash.Unlock();
	}

	CSchedule::Stop(bHide);

	return true;
}

bool CFlashSchedule::Save()
{
	CSchedule::Save();

	return true;
}

////////////////////////////////////////////////////////////////////////
bool FLASH_OBJECT::CheckCount()
{
	if(cur_count >= max_count)
		return false;
	return true;
}

bool FLASH_OBJECT::IncreaseCount()
{
	if(cur_count >= max_count)
		return false;
	cur_count++;
	return true;
}

void FLASH_OBJECT::Save()
{
	CTime tm = CTime::GetCurrentTime();
	CString str_tm = tm.Format("%Y%m%d");

	WritePrivateProfileString(filename, "Time", str_tm, GetDataPath());
	WritePrivateProfileString(filename, "Count", ::ToString(cur_count), GetDataPath());
}

void FLASH_OBJECT::Load()
{
	char buf[256];
	GetPrivateProfileString(filename, "Time", "", buf, 255, GetDataPath());

	CTime tm = CTime::GetCurrentTime();
	CString str_tm = tm.Format("%Y%m%d");

	if(str_tm == buf)
	{
		GetPrivateProfileString(filename, "Count", "0", buf, 255, GetDataPath());
		cur_count = atoi(buf);
	}
	else
		cur_count = 0;
}


// Modified by 정운형 2008-11-13 오전 9:39
// 변경내역 :  Power point shcdule작업
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPowerPointSchedule
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 모드에서의 생성자 \n
/// @param (CFrame*) pParentWnd : (in) schedule을 갖는 frame의 포인터
/// @param (LPCSTR) lpszID : (in) schedule의 id
/// @param (bool) bDefaultSchedule : (in) default schedule인지 여부 플래그 값
/////////////////////////////////////////////////////////////////////////////////
CPowerPointSchedule::CPowerPointSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_bSlideShowEnd(true)	
,	m_strPPTViewPath(_T(""))
,	m_strLog(_T(""))
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
	m_font.CreatePointFont(20*10, "Tahoma");
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CPowerPointSchedule::~CPowerPointSchedule()
{

}


IMPLEMENT_DYNAMIC(CPowerPointSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CPowerPointSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_CREATE 이벤트 함수로 window가 처음 생성될때 호출된다 \n
/// @param (LPCREATESTRUCT) lpCreateStruct : (in) 윈도우 생성 정보
/// @return <형: int> \n
///			<0: 성공> \n
///			<-1: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
int CPowerPointSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;
/*
	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());

		//ppt viewer의 경로를 가져온다.
		bool bRet = GetPPTViewerPath(m_strPPTViewPath);
		if(bRet == false || m_strPPTViewPath == _T(""))
		{
			//property 파일에 경로가 없는경우
			m_strPPTViewPath.Format(_T("%s\\pptview\\PPTVIEW.EXE"), GetAppPath());
		}//if
	}
*/
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_DESTORY 이벤트 함수 \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_PAINT 이벤트 함수로 window가 그려질때 호출된다 \n
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::OnPaint()
{
	//CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	if(m_bOpen)
	{
		//이곳에서 어떤 방법으로든 배경을 그려줘야 할텐데...
		CPaintDC dc(this);

		CRect client_rect;
		GetClientRect(client_rect);
		dc.FillSolidRect(client_rect, RGB(0, 0, 0));
		dc.SetTextColor(RGB(128,128,128));
		dc.SetBkColor(RGB(0, 0, 0));
		dc.SelectObject(&m_font);
		dc.DrawText(m_strLog, client_rect, DT_MODIFYSTRING);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 모드에서 schedule을 설정하는 함수 \n
/// @param (LPCSTR) lpszID : (in) schedule의 id
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents의 전체 재생시간을 반환하는 함수 \n
/// @return <형: double> \n
///			contents의 전체 재생시간 \n
/////////////////////////////////////////////////////////////////////////////////
double CPowerPointSchedule::GetTotalPlayTime()
{
	/*
	if(m_bDefaultSchedule)
	{
		return (double)m_runningTime*1000; // sec -> millisec
	}
	else
	{
		return (double)DBL_MAX;				//double의 최대값
	}//if
	*/

	//ppt 파일의 슬라이드 쇼 시간을 정할수가 없으므로 일단 최대값을 리턴
	return (double)DBL_MAX;				//double의 최대값
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contens의 현재 재생경과 시간을 반환하는 함수 \n
/// @return <형: double> \n
///			contents가 재생된 경과 시간 \n
///			<값: 설명> \n
/////////////////////////////////////////////////////////////////////////////////
double CPowerPointSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	//return (double)(dwEndTick - m_dwStartTick);
	//일단 page down을 통하여 다음으로 넘어거기전가지는 자동으로
	//넘어가지 않도록 하기 위하여...
	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents 파일을 open하는 함수 \n
/// @param (int) nGrade : (in) contens의 재생우선 순위 grade
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}//if

	if(_access(m_strMediaFullPath, 00) != 0)
	{
		return false;
	}//if

	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);
	//GetCurrentDirectory(MAX_PATH, szModule);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);
	//m_strPPTViewPath.Format("%s%spptview\\PPTVIEW.EXE", cDrive, cPath);
	m_strPPTViewPath.Format("%s%sHNC\\HOfficeViewer70\\HSlideviewer70\\HptView.exe", cDrive, cPath);
	if(_access(m_strPPTViewPath, 00) != 0)
	{
		return false;
	}//if

	m_bOpen = true;	

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents 파일을 close 하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::CloseFile()
{
	if(m_bOpen)
	{
		m_bOpen = false;
		Stop();

		//pptview 프로그램이 구동 중이라면 강제로 종료해야 하나?
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 play하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::Play(bool bResume)
{
	CSchedule::Play();

	if(!m_bOpen)
	{
		return false;
	}//if

	m_dwStartTick = ::GetTickCount();

	//pptview 프로그램을 실행시킨다.
	/*
	/L - 텍스트 파일을 포함한 파일 목록을 읽어라. 
		(예) pptview.exe /L "Playlist.txt" 
	/S - 뷰어 스플래쉬 스크린 없이 빠른 시작을 해라. 
	/P - PPT 파일을 인쇄하라 
		(예) pptview.exe /P "Persentation.ppt" 
	/D - 슬라이드쇼가 끝날 때 바로 대화 상자를 열어라 
	/N# - PPT 파일에서 특정 슬라이드 번호 “#”을 열어라. 
		(예) /pptview.exe /n5 "presentation.ppt"
	*/

	CString strCmd;
	UINT nRet;
	//strCmd.Format(_T("%s /S %s"), m_strPPTViewPath, m_strMediaFullPath);
	strCmd.Format(_T("%s %s"), m_strPPTViewPath, m_strMediaFullPath);
	nRet = WinExec(strCmd, SW_MAXIMIZE);

	if(nRet < 31)
	{
		return false;
	}//if

	m_bSlideShowEnd = false;

	//배경에 뿌려주는 문자열
	m_strLog.Format(_T("\r\nNow, A slide show is begin....\r\n\
If you want to see a next schedule,\r\npress 'Page Down' key, please...!\r\n"));

	Invalidate(FALSE);

	CSchedule::Play(bResume);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 일시 정지 하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::Pause()
{
	Stop();

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 정지하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

	m_bSlideShowEnd = true;

	//pptview 프로그래을 강제로 종료 시켜야하나?

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// shcedule의 내용을 ini 파일로 저장하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CPowerPointSchedule::Save()
{
	CSchedule::Save();

	return true;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule의 정보를 문자열로 반환하는 함수 \n
/// @return <형: CString> \n
///			schedule의 벙보 문자열 \n
/////////////////////////////////////////////////////////////////////////////////
CString	CPowerPointSchedule::ToString()
{
	CString str;
	str.Format(
		"-Power Point Schedule-\r\n"
		"%s\r\n"
		,	CSchedule::ToString()
	);

	return str;
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// shcedule의 정보 문자열을 logdialog에 설정하는 함수 \n
/// @param (CPropertyGrid*) pGrid : (in) shcedule의 정보 문자열을 설정하는 grid 컨트롤의 포인터
/////////////////////////////////////////////////////////////////////////////////
void CPowerPointSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("POWER POINT Schedule Attributes");

	hs = pGrid->AddSection("member variables");
}
*/



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHwpSchedule

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 모드에서의 생성자 \n
/// @param (CFrame*) pParentWnd : (in) schedule을 갖는 frame의 포인터
/// @param (LPCSTR) lpszID : (in) schedule의 id
/// @param (bool) bDefaultSchedule : (in) default schedule인지 여부 플래그 값
/////////////////////////////////////////////////////////////////////////////////
CHwpSchedule::CHwpSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_bSlideShowEnd(true)	
,	m_strHwpViewPath(_T(""))
,	m_strLog(_T(""))
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
	m_font.CreatePointFont(20*10, "Tahoma");
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CHwpSchedule::~CHwpSchedule()
{

}


IMPLEMENT_DYNAMIC(CHwpSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CHwpSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_CREATE 이벤트 함수로 window가 처음 생성될때 호출된다 \n
/// @param (LPCREATESTRUCT) lpCreateStruct : (in) 윈도우 생성 정보
/// @return <형: int> \n
///			<0: 성공> \n
///			<-1: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
int CHwpSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;
/*
	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}
*/
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_DESTORY 이벤트 함수 \n
/////////////////////////////////////////////////////////////////////////////////
void CHwpSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_PAINT 이벤트 함수로 window가 그려질때 호출된다 \n
/////////////////////////////////////////////////////////////////////////////////
void CHwpSchedule::OnPaint()
{
	//CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	if(m_bOpen)
	{
		//이곳에서 어떤 방법으로든 배경을 그려줘야 할텐데...
		CPaintDC dc(this);

		CRect client_rect;
		GetClientRect(client_rect);
		dc.FillSolidRect(client_rect, RGB(0, 0, 0));
		dc.SetTextColor(RGB(128,128,128));
		dc.SetBkColor(RGB(0, 0, 0));
		dc.SelectObject(&m_font);
		dc.DrawText(m_strLog, client_rect, DT_MODIFYSTRING);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 모드에서 schedule을 설정하는 함수 \n
/// @param (LPCSTR) lpszID : (in) schedule의 id
/////////////////////////////////////////////////////////////////////////////////
void CHwpSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents의 전체 재생시간을 반환하는 함수 \n
/// @return <형: double> \n
///			contents의 전체 재생시간 \n
/////////////////////////////////////////////////////////////////////////////////
double CHwpSchedule::GetTotalPlayTime()
{
	/*
	if(m_bDefaultSchedule)
	{
		return (double)m_runningTime*1000; // sec -> millisec
	}
	else
	{
		return (double)DBL_MAX;				//double의 최대값
	}//if
	*/

	//ppt 파일의 슬라이드 쇼 시간을 정할수가 없으므로 일단 최대값을 리턴
	return (double)DBL_MAX;				//double의 최대값
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contens의 현재 재생경과 시간을 반환하는 함수 \n
/// @return <형: double> \n
///			contents가 재생된 경과 시간 \n
///			<값: 설명> \n
/////////////////////////////////////////////////////////////////////////////////
double CHwpSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	//return (double)(dwEndTick - m_dwStartTick);
	//일단 page down을 통하여 다음으로 넘어거기전가지는 자동으로
	//넘어가지 않도록 하기 위하여...
	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents 파일을 open하는 함수 \n
/// @param (int) nGrade : (in) contens의 재생우선 순위 grade
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHwpSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}//if

	if(_access(m_strMediaFullPath, 00) != 0)
	{
		return false;
	}//if

	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);
	//GetCurrentDirectory(MAX_PATH, szModule);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);
	m_strHwpViewPath.Format("%s%sHNC\\HOfficeViewer70\\HwpViewer70\\HwpView.exe", cDrive, cPath);
	if(_access(m_strHwpViewPath, 00) != 0)
	{
		return false;
	}//if

	m_bOpen = true;

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents 파일을 close 하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHwpSchedule::CloseFile()
{
	if(m_bOpen)
	{
		m_bOpen = false;
		Stop();
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 play하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHwpSchedule::Play(bool bResume)
{
	if(!m_bOpen)
	{
		return false;
	}//if

	//CSchedule::Play();

	m_dwStartTick = ::GetTickCount();

	CString strCmd;
	UINT nRet;
	strCmd.Format(_T("%s %s"), m_strHwpViewPath, m_strMediaFullPath);
	nRet = WinExec(strCmd, SW_MAXIMIZE);

	if(nRet < 31)
	{
		return false;
	}//if

	m_bSlideShowEnd = false;

	//배경에 뿌려주는 문자열
	m_strLog.Format(_T("\r\nNow, A slide show is begin....\r\n\
If you want to see a next schedule,\r\npress 'Page Down' key, please...!\r\n"));

	Invalidate(FALSE);

	CSchedule::Play(bResume);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 일시 정지 하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHwpSchedule::Pause()
{
	Stop();

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 정지하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHwpSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

	m_bSlideShowEnd = true;

	//pptview 프로그래을 강제로 종료 시켜야하나?

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// shcedule의 내용을 ini 파일로 저장하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CHwpSchedule::Save()
{
	CSchedule::Save();

	return true;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule의 정보를 문자열로 반환하는 함수 \n
/// @return <형: CString> \n
///			schedule의 벙보 문자열 \n
/////////////////////////////////////////////////////////////////////////////////
CString	CHwpSchedule::ToString()
{
	CString str;
	str.Format(
		"-Hangul Schedule-\r\n"
		"%s\r\n"
		,	CSchedule::ToString()
	);

	return str;
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// shcedule의 정보 문자열을 logdialog에 설정하는 함수 \n
/// @param (CPropertyGrid*) pGrid : (in) shcedule의 정보 문자열을 설정하는 grid 컨트롤의 포인터
/////////////////////////////////////////////////////////////////////////////////
void CHwpSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("Hangul Schedule Attributes");

	hs = pGrid->AddSection("member variables");
}
*/



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CExcelSchedule
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 모드에서의 생성자 \n
/// @param (CFrame*) pParentWnd : (in) schedule을 갖는 frame의 포인터
/// @param (LPCSTR) lpszID : (in) schedule의 id
/// @param (bool) bDefaultSchedule : (in) default schedule인지 여부 플래그 값
/////////////////////////////////////////////////////////////////////////////////
CExcelSchedule::CExcelSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule)
:	CSchedule (pParentWnd, lpszID, bDefaultSchedule)
,	m_bSlideShowEnd(true)	
,	m_strExcelViewPath(_T(""))
,	m_strLog(_T(""))
{
	SetSchedule(lpszID);

	m_dwStartTick = ::GetTickCount();
	m_font.CreatePointFont(20*10, "Tahoma");
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CExcelSchedule::~CExcelSchedule()
{

}


IMPLEMENT_DYNAMIC(CExcelSchedule, CSchedule)

BEGIN_MESSAGE_MAP(CExcelSchedule, CSchedule)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_CREATE 이벤트 함수로 window가 처음 생성될때 호출된다 \n
/// @param (LPCREATESTRUCT) lpCreateStruct : (in) 윈도우 생성 정보
/// @return <형: int> \n
///			<0: 성공> \n
///			<-1: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
int CExcelSchedule::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CSchedule::OnCreate(lpCreateStruct) == -1)
		return -1;
/*
	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	if(m_filename.length() > 0)
	{
		CString LocalTempFullPath;
		GetLocalPath(m_strMediaFullPath, LocalTempFullPath, m_location.c_str(), m_filename.c_str());
	}

	__DEBUG_SCHEDULE_END__(m_scheduleId.c_str())
*/
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_DESTORY 이벤트 함수 \n
/////////////////////////////////////////////////////////////////////////////////
void CExcelSchedule::OnDestroy()
{
	CloseFile();

	CSchedule::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// WM_PAINT 이벤트 함수로 window가 그려질때 호출된다 \n
/////////////////////////////////////////////////////////////////////////////////
void CExcelSchedule::OnPaint()
{
	//CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CSchedule::OnPaint()을(를) 호출하지 마십시오.

	if(m_bOpen)
	{
		//이곳에서 어떤 방법으로든 배경을 그려줘야 할텐데...
		CPaintDC dc(this);

		CRect client_rect;
		GetClientRect(client_rect);
		dc.FillSolidRect(client_rect, RGB(0, 0, 0));
		dc.SetTextColor(RGB(128,128,128));
		dc.SetBkColor(RGB(0, 0, 0));
		dc.SelectObject(&m_font);
		dc.DrawText(m_strLog, client_rect, DT_MODIFYSTRING);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 모드에서 schedule을 설정하는 함수 \n
/// @param (LPCSTR) lpszID : (in) schedule의 id
/////////////////////////////////////////////////////////////////////////////////
void CExcelSchedule::SetSchedule(LPCSTR lpszID)
{
	//CSchedule::SetSchedule(lpszID);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents의 전체 재생시간을 반환하는 함수 \n
/// @return <형: double> \n
///			contents의 전체 재생시간 \n
/////////////////////////////////////////////////////////////////////////////////
double CExcelSchedule::GetTotalPlayTime()
{
	/*
	if(m_bDefaultSchedule)
	{
		return (double)m_runningTime*1000; // sec -> millisec
	}
	else
	{
		return (double)DBL_MAX;				//double의 최대값
	}//if
	*/

	//ppt 파일의 슬라이드 쇼 시간을 정할수가 없으므로 일단 최대값을 리턴
	return (double)DBL_MAX;				//double의 최대값
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contens의 현재 재생경과 시간을 반환하는 함수 \n
/// @return <형: double> \n
///			contents가 재생된 경과 시간 \n
///			<값: 설명> \n
/////////////////////////////////////////////////////////////////////////////////
double CExcelSchedule::GetCurrentPlayTime()
{
	DWORD dwEndTick = ::GetTickCount();

	//return (double)(dwEndTick - m_dwStartTick);
	//일단 page down을 통하여 다음으로 넘어거기전가지는 자동으로
	//넘어가지 않도록 하기 위하여...
	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents 파일을 open하는 함수 \n
/// @param (int) nGrade : (in) contens의 재생우선 순위 grade
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CExcelSchedule::OpenFile(int nGrade)
{
	if(m_bOpen)
	{
		return true;
	}//if

	if(_access(m_strMediaFullPath, 00) != 0)
	{
		return false;
	}//if

	char cModule[MAX_PATH];
	::ZeroMemory(cModule, MAX_PATH);
	::GetModuleFileName(NULL, cModule, MAX_PATH);
	//GetCurrentDirectory(MAX_PATH, szModule);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(cModule, cDrive, cPath, cFilename, cExt);
	m_strExcelViewPath.Format("%s%sHNC\\HOfficeViewer70\\HNexcelViewer70\\NxlView.exe", cDrive, cPath);
	if(_access(m_strExcelViewPath, 00) != 0)
	{
		return false;
	}//if

	m_bOpen = true;	

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// contents 파일을 close 하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CExcelSchedule::CloseFile()
{
	if(m_bOpen)
	{
		m_bOpen = false;
		Stop();

		//pptview 프로그램이 구동 중이라면 강제로 종료해야 하나?
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 play하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CExcelSchedule::Play(bool bResume)
{
	if(!m_bOpen)
	{
		return false;
	}//if

	//CSchedule::Play();

	m_dwStartTick = ::GetTickCount();

	CString strCmd;
	UINT nRet;
	strCmd.Format(_T("%s %s"), m_strExcelViewPath, m_strMediaFullPath);
	nRet = WinExec(strCmd, SW_MAXIMIZE);

	if(nRet < 31)
	{
		return false;
	}//if

	m_bSlideShowEnd = false;

	//배경에 뿌려주는 문자열
	m_strLog.Format(_T("\r\nNow, A slide show is begin....\r\n\
If you want to see a next schedule,\r\npress 'Page Down' key, please...!\r\n"));

	Invalidate(FALSE);

	CSchedule::Play(bResume);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 일시 정지 하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CExcelSchedule::Pause()
{
	Stop();

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule을 정지하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CExcelSchedule::Stop(bool bHide)
{
	CSchedule::Stop(bHide);

	m_bSlideShowEnd = true;

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// shcedule의 내용을 ini 파일로 저장하는 함수 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CExcelSchedule::Save()
{
	CSchedule::Save();

	return true;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// schedule의 정보를 문자열로 반환하는 함수 \n
/// @return <형: CString> \n
///			schedule의 벙보 문자열 \n
/////////////////////////////////////////////////////////////////////////////////
CString	CExcelSchedule::ToString()
{
	CString str;
	str.Format(
		"-Excel Schedule-\r\n"
		"%s\r\n"
		,	CSchedule::ToString()
	);

	return str;
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// shcedule의 정보 문자열을 logdialog에 설정하는 함수 \n
/// @param (CPropertyGrid*) pGrid : (in) shcedule의 정보 문자열을 설정하는 grid 컨트롤의 포인터
/////////////////////////////////////////////////////////////////////////////////
void CExcelSchedule::ToGrid(CPropertyGrid* pGrid)
{
	CSchedule::ToGrid(pGrid);

	HSECTION hs = pGrid->AddSection("Excel Schedule Attributes");

	hs = pGrid->AddSection("member variables");
}
*/

