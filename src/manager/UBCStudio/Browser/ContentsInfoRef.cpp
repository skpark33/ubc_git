#include "StdAfx.h"
#include "ContentsInfoRef.h"
#include "DataContainer.h"

CContentsInfoRef* CContentsInfoRef::_instance;
CCriticalSection CContentsInfoRef::_cs;

CContentsInfoRef::CContentsInfoRef(void)
{
}

CContentsInfoRef::~CContentsInfoRef(void)
{
}

CContentsInfoRef* CContentsInfoRef::getInstance()
{
	_cs.Lock();
	if(_instance == NULL)
	{
		_instance = new CContentsInfoRef();
	}
	_cs.Unlock();

	return _instance;
}

void CContentsInfoRef::clearInstance()
{
	_cs.Lock();
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
	_cs.Unlock();
}

CString CContentsInfoRef::GetWizardXml(CString strContentsId)
{
	CString strWizardXml;
	if(!strContentsId.IsEmpty())
	{
		CONTENTS_INFO* pContentsInfo = CDataContainer::getInstance()->GetContents(strContentsId);
		if(pContentsInfo)
		{
			strWizardXml = pContentsInfo->strWizardXML;
		}
	}

	return strWizardXml;
}