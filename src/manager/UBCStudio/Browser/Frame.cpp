// Frame.cpp : 구현 파일입니다.
//

#include "stdafx.h"
//#include "UBCStudio.h"
//#include "Host.h"
#include "Template.h"
#include "Frame.h"
#include "..\MemDC.h"


// CFrame

#define		SCHEDULE_CHECK_ID		3000	// id
#define		SCHEDULE_CHECK_TIME		100		// time(ms)

#define		FRAME_SHOW_ID			3001	// id
#define		FRAME_SHOW_TIME			100		// time(ms)

#define		FRAME_HIDE_ID			3002	// id
#define		FRAME_HIDE_TIME			100		// time(ms)


UINT	CFrame::m_nFrameID = 0xff00;

CFrame*	CFrame::m_pPhoneFrame = NULL;
CMutex	CFrame::m_mutexPhone;


IMPLEMENT_DYNAMIC(CFrame, CWnd)


CFrame*	CFrame::GetFrameObject(CTemplate* pParent, LPCSTR lpszID, CString& strErrorMessage)
{

	// check valid
	CFrame* frame = new CFrame(pParent, lpszID);

	if(frame != NULL)
	{
		if(frame->m_grade == 1)
			frame->SetPlayCount(pParent->GetPlayCount());

		if(frame->IsPIP())	// PIP 일경우 그냥 리턴 (나중에 create)
			return frame;

		return CreatePIPFrameObject(pParent, frame, strErrorMessage);
	}

	strErrorMessage = "Fail to create frame !!!";

	return NULL;
}

CFrame* CFrame::CreatePIPFrameObject(CTemplate* pParent, CFrame* pFrame, CString& strErrorMessage)
{
	BOOL ret = pFrame->Create(NULL, "Frame", WS_CHILD, CRect(0,0,100,100), pParent, m_nFrameID--);
	if(ret)
	{
		return pFrame;
	}
	else
	{
		strErrorMessage = "Fail to create Frame !!!";

		delete pFrame;
		return NULL;
	}
}


CFrame::CFrame(CTemplate* pParentWnd, LPCSTR lpszID)
:	m_nCurrentScheduleIndex(-1)
,	m_nCurrentDefaultScheduleIndex(-1)
,	m_pCurrentSchedule(NULL)
,	m_bCurrentPlay(false)
,	m_pPIPParentWnd(NULL)
,	m_pParentWnd(pParentWnd)
,	m_strErrorMessage ("")
,	m_bLoadFromLocal (true)
,	m_strLoadID (lpszID)
,	m_nPlayCount (0)
{
	char	tmpBuffer[1024];

	GetPrivateProfileString(lpszID, "templateId", "", tmpBuffer, 1024, GetDataPath());
	m_templateId = tmpBuffer;

	GetPrivateProfileString(lpszID, "frameId", "", tmpBuffer, 1024, GetDataPath());
	m_frameId = tmpBuffer;

	GetPrivateProfileString(lpszID, "grade", "", tmpBuffer, 1024, GetDataPath());
	m_grade = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "width", "", tmpBuffer, 1024, GetDataPath());
	m_width = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "height", "", tmpBuffer, 1024, GetDataPath());
	m_height = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "x", "", tmpBuffer, 1024, GetDataPath());
	m_x = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "y", "", tmpBuffer, 1024, GetDataPath());
	m_y = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "isPIP", "", tmpBuffer, 1024, GetDataPath());
	m_isPIP = (bool)atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "borderStyle", "", tmpBuffer, 1024, GetDataPath());
	m_borderStyle = tmpBuffer;

	GetPrivateProfileString(lpszID, "borderThickness", "", tmpBuffer, 1024, GetDataPath());
	m_borderThickness = atoi(tmpBuffer);

	GetPrivateProfileString(lpszID, "borderColor", "", tmpBuffer, 1024, GetDataPath());
	m_borderColor = tmpBuffer;

	GetPrivateProfileString(lpszID, "cornerRadius", "", tmpBuffer, 1024, GetDataPath());
	m_cornerRadius = atoi(tmpBuffer);

	//
	if(m_borderColor.length() > 1)
	{
		m_rgbBorderColor = ::GetColorFromString(m_borderColor.c_str()+1);
	}

	//
	CString strPhoneTemplateId, stPhoneFrameId;
//	::GetPhoneConfig(strPhoneTemplateId, stPhoneFrameId, nPortNo);

	if(strPhoneTemplateId == m_templateId.c_str() &&
		stPhoneFrameId == m_frameId.c_str() )
	{
		m_pPhoneFrame = this;
	}
}

CFrame::~CFrame()
{

	if(CFrame::m_pPhoneFrame == this)
		CFrame::m_pPhoneFrame = NULL;
}


BEGIN_MESSAGE_MAP(CFrame, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_DESTROY()

//	ON_MESSAGE(ID_PLAY_NEXT_SCHEDULE, OnPlayNextSchedule)

	ON_WM_MOUSEMOVE()

END_MESSAGE_MAP()


// CFrame 메시지 처리기입니다.


int CFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	//
	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);
	MoveWindow(
		(int)(m_x * xscale), 
		(int)(m_y * yscale), 
		(int)(m_width * xscale), 
		(int)(m_height * yscale)
	);

	//
	if(m_bLoadFromLocal)
	{
		LoadSchedule(true);
		LoadSchedule(false);
		m_bLoadFromLocal = false;
	}
	else
	{
		//InitSchedule(true);
		//InitSchedule(false);
	}


	return 0;
}

void CFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	if(strcmp(m_borderStyle.c_str(), "solid") == 0)
	{
		if(m_borderThickness > 0)
		{
			if(m_cornerRadius == 0)
				DrawSquareFrame(&dc);	// 직사각형
			else
				DrawRoundFrame(&dc);	// 둥근 사각형
		}
	}
	else if(strcmp(m_borderStyle.c_str(), "inset") == 0)
	{
		//
	}
	else if(strcmp(m_borderStyle.c_str(), "outset") == 0)
	{
		//
	}
	else if(strcmp(m_borderStyle.c_str(), "none") == 0)
	{
		//
	}
}

void CFrame::OnDestroy()
{

	m_mutex.Lock();

	KillTimer(FRAME_SHOW_ID);
	KillTimer(FRAME_HIDE_ID);
	KillTimer(SCHEDULE_CHECK_ID);

	// delete schedule
	int count = m_listSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listSchedule[i];
		schedule->DestroyWindow();
		delete schedule;
	}
	m_listSchedule.RemoveAll();

	// delete default schedule
	count = m_listDefaultSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listDefaultSchedule[i];
		schedule->DestroyWindow();
		delete schedule;
	}
	m_listDefaultSchedule.RemoveAll();

	//
	m_mapSchedule.RemoveAll();

	m_mutex.Unlock();


	CWnd::OnDestroy();

}

void CFrame::DrawSquareFrame(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);

	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);

	int xborder_thickness = (int)(m_borderThickness * xscale);
	int yborder_thickness = (int)(m_borderThickness * yscale);

	// top
	CRect top;
	top = rect;
	top.bottom = top.top + yborder_thickness;
	pDC->FillSolidRect(top, m_rgbBorderColor);

	// bottom
	CRect bottom;
	bottom = rect;
	bottom.top = bottom.bottom - yborder_thickness;
	pDC->FillSolidRect(bottom, m_rgbBorderColor);

	// left
	CRect left;
	left = rect;
	left.right = left.left + xborder_thickness;
	pDC->FillSolidRect(left, m_rgbBorderColor);

	// right
	CRect right;
	right = rect;
	right.left = right.right - xborder_thickness;
	pDC->FillSolidRect(right, m_rgbBorderColor);
}

void CFrame::DrawRoundFrame(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);

	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);

	int xborder_thickness = (int)(m_borderThickness * xscale);
	int yborder_thickness = (int)(m_borderThickness * yscale);

	// draw mask
	{
		CMemDCAlt memDC(pDC, rect, xborder_thickness, yborder_thickness, SRCAND);

		memDC.FillSolidRect(rect, RGB(255,255,255));

		CBrush brushBlue(RGB(0, 0, 0));
		CBrush* pOldBrush = memDC.SelectObject(&brushBlue);

		CPen penBlack;
		penBlack.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
		CPen* pOldPen = memDC.SelectObject(&penBlack);

		memDC.RoundRect(rect, CPoint(xborder_thickness, yborder_thickness));

		memDC.SelectObject(pOldBrush);
		memDC.SelectObject(pOldPen);
	}

	// draw original
	{
		CMemDCAlt memDC(pDC, rect, xborder_thickness, yborder_thickness, SRCPAINT);

		CBrush brushBlue(m_rgbBorderColor);
		CBrush* pOldBrush = memDC.SelectObject(&brushBlue);

		CPen penBlack;
		penBlack.CreatePen(PS_SOLID, 1, m_rgbBorderColor);
		CPen* pOldPen = memDC.SelectObject(&penBlack);

		memDC.RoundRect(rect, CPoint(xborder_thickness, yborder_thickness));

		memDC.SelectObject(pOldBrush);
		memDC.SelectObject(pOldPen);
	}
}


void CFrame::OnTimer(UINT_PTR nIDEvent)
{
	CWnd::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case FRAME_SHOW_ID:
		KillTimer(FRAME_SHOW_ID);
		ShowWindow(SW_SHOW);
		break;

	case FRAME_HIDE_ID:
		KillTimer(FRAME_HIDE_ID);
		ShowWindow(SW_HIDE);
		break;

	case SCHEDULE_CHECK_ID:
		KillTimer(SCHEDULE_CHECK_ID);
		CheckSchedule();
		SetTimer(SCHEDULE_CHECK_ID, SCHEDULE_CHECK_TIME, NULL);
		break;
	}
}

bool CFrame::CheckSchedule()
{
	m_mutex.Lock();

	if(m_pCurrentSchedule == NULL)
	{
		PlayNextSchedule();
	}
	else
	{
		if(m_pCurrentSchedule->IsDefaultSchedule())
		{
			int new_sch_index;
			if(GetNextSchedule(new_sch_index))
			{
				CSchedule* old_schedule = m_pCurrentSchedule;

				m_nCurrentScheduleIndex = new_sch_index;
				m_pCurrentSchedule = m_listSchedule[new_sch_index];
				m_pCurrentSchedule->Play();

				if(old_schedule != m_pCurrentSchedule)
					old_schedule->Stop();
			}
			else
			{
				double lCur, lTotal;
				lCur = m_pCurrentSchedule->GetCurrentPlayTime();
				lTotal = m_pCurrentSchedule->GetTotalPlayTime();

				if(m_pCurrentSchedule->IsBetween() && lTotal <= lCur ||
					m_pCurrentSchedule->IsBetween() == false)
				{
					CSchedule* old_schedule = m_pCurrentSchedule;

					if(GetNextDefaultSchedule(new_sch_index))
					{
						m_nCurrentDefaultScheduleIndex = new_sch_index;
						m_pCurrentSchedule = m_listDefaultSchedule[new_sch_index];
						if(m_grade == 1)
						{
							int play_count = m_pCurrentSchedule->GetPlayCount();
							if(m_nPlayCount > 0 && play_count >= m_nPlayCount)
							{
//								::AfxGetMainWnd()->PostMessageA(WM_PLAY_NEXT_DEFAULT_TEMPLATE);

								if(old_schedule && old_schedule != m_pCurrentSchedule)
									old_schedule->Stop();

								m_mutex.Unlock();

								return true;
							}
						}

						m_pCurrentSchedule->Play();
					}

					if(old_schedule != m_pCurrentSchedule)
						old_schedule->Stop();
				}
			}
		}
		else
		{
			double lCur, lTotal;
			lCur = m_pCurrentSchedule->GetCurrentPlayTime();
			lTotal = m_pCurrentSchedule->GetTotalPlayTime();

			if( (m_pCurrentSchedule->IsBetween() && lTotal <= lCur) ||
				m_pCurrentSchedule->IsBetween() == false )
			{
				CSchedule* old_schedule = m_pCurrentSchedule;

				int new_sch_index;
				if(GetNextSchedule(new_sch_index))
				{
					m_nCurrentScheduleIndex = new_sch_index;
					m_pCurrentSchedule = m_listSchedule[new_sch_index];
				}
				else if(GetNextDefaultSchedule(new_sch_index))
				{
					m_nCurrentDefaultScheduleIndex = new_sch_index;
					m_pCurrentSchedule = m_listDefaultSchedule[new_sch_index];

					if(m_grade == 1)
					{
						int play_count = m_pCurrentSchedule->GetPlayCount();
						if(m_nPlayCount > 0 && play_count >= m_nPlayCount)
						{
//							::AfxGetMainWnd()->PostMessageA(WM_PLAY_NEXT_DEFAULT_TEMPLATE);

							if(old_schedule && old_schedule != m_pCurrentSchedule)
								old_schedule->Stop();

							m_mutex.Unlock();

							return true;
						}
					}
				}

				m_pCurrentSchedule->Play();

				if(old_schedule != m_pCurrentSchedule)
					old_schedule->Stop();
			}
		}
	}

	m_mutex.Unlock();

	return true;
}


bool CFrame::OpenFile()
{

	m_mutex.Lock();

	POSITION pos = m_mapSchedule.GetStartPosition();
	if(pos == NULL)
	{
	}
	else
	{
		while(pos != NULL)
		{
			CString scheduleId;
			CSchedule* schedule;
			m_mapSchedule.GetNextAssoc( pos, scheduleId, (void*&)schedule );

			if(schedule->IsOpen() == false)
				schedule->OpenFile(m_grade);
		}
	}
	m_mutex.Unlock();


	return true;
}

bool CFrame::IsBetween(bool bDefaultSchedule)
{
	m_mutex.Lock();
/*
	bool between = false;
	POSITION pos = m_mapSchedule.GetStartPosition();
	while(pos != NULL)
	{
		CString scheduleId;
		CSchedule* schedule;
		m_mapSchedule.GetNextAssoc( pos, scheduleId, (void*&)schedule );

		if(schedule->IsDefaultSchedule() == bDefaultSchedule)
			between |= schedule->IsBetween();
	}
*/
	bool between = false;
	if(bDefaultSchedule)
	{
		int count = m_listDefaultSchedule.GetCount();
		for(int i=0; i<count; i++)
		{
			CSchedule* schedule = m_listDefaultSchedule.GetAt(i);
			if(schedule->IsDefaultSchedule() == bDefaultSchedule)
				between |= schedule->IsBetween();
		}
	}
	else
	{
		int count = m_listSchedule.GetCount();
		for(int i=0; i<count; i++)
		{
			CSchedule* schedule = m_listSchedule.GetAt(i);
			if(schedule->IsDefaultSchedule() == bDefaultSchedule)
				between |= schedule->IsBetween();
		}
	}

	m_mutex.Unlock();

	return between;
}

void CFrame::Play()
{
	if(m_bCurrentPlay == true) return;

	KillTimer(FRAME_HIDE_ID);
	SetTimer(FRAME_SHOW_ID, FRAME_SHOW_TIME, NULL);
	m_bCurrentPlay = true;

	if(m_pPIPParentWnd)
	{
		m_pPIPParentWnd->SetClipRgn(m_x, m_y, m_width, m_height);
	}

	m_mutex.Lock();

	ResetPlayCount();

	PlayNextSchedule();

	m_mutex.Unlock();
}

void CFrame::Stop()
{
	if(m_bCurrentPlay == false) return;

	m_bCurrentPlay = false;

	m_mutex.Lock();

	KillTimer(SCHEDULE_CHECK_ID);
	KillTimer(FRAME_SHOW_ID);
	SetTimer(FRAME_HIDE_ID, FRAME_HIDE_TIME, NULL);

	if(m_pPIPParentWnd)
	{
		m_pPIPParentWnd->ResetClipRgn();
	}

	if(m_pCurrentSchedule)
	{
		m_pCurrentSchedule->Stop();
		m_pCurrentSchedule = NULL;
	}

	m_mutex.Unlock();
}

void CFrame::PlayNextSchedule()
{
//	m_mutex.Lock();

	if(m_listSchedule.GetCount() > 0)
	{
		int index;
		if(GetNextSchedule(index))
		{
			CSchedule* old_schedule = m_pCurrentSchedule;

			m_nCurrentScheduleIndex = index;
			m_pCurrentSchedule = m_listSchedule[index];
			m_pCurrentSchedule->Play();

			if(old_schedule && old_schedule != m_pCurrentSchedule)
				old_schedule->Stop();

//			m_mutex.Unlock();

			SetTimer(SCHEDULE_CHECK_ID, SCHEDULE_CHECK_TIME, NULL);


			return;
		}
	}

	if(m_listDefaultSchedule.GetCount() > 0)
	{
		int index;
		if(GetNextDefaultSchedule(index))
		{
			CSchedule* old_schedule = m_pCurrentSchedule;
		
			m_nCurrentDefaultScheduleIndex = index;
			m_pCurrentSchedule = m_listDefaultSchedule[index];
			if(m_grade == 1)
			{
				int play_count = m_pCurrentSchedule->GetPlayCount();
				if(m_nPlayCount > 0 && play_count >= m_nPlayCount)
				{
//					::AfxGetMainWnd()->PostMessageA(WM_PLAY_NEXT_DEFAULT_TEMPLATE);

					if(old_schedule && old_schedule != m_pCurrentSchedule)
						old_schedule->Stop();

					return;
				}
			}
			m_pCurrentSchedule->Play();

			if(old_schedule && old_schedule != m_pCurrentSchedule)
				old_schedule->Stop();

//			m_mutex.Unlock();

			SetTimer(SCHEDULE_CHECK_ID, SCHEDULE_CHECK_TIME, NULL);


			return;
		}
	}

//	m_mutex.Unlock();
}

bool CFrame::GetNextSchedule(int& index)
{
//	m_mutex.Lock();

	if(m_listSchedule.GetCount() == 0)
	{
//		m_mutex.Unlock();

		return false;
	}

	index = m_nCurrentScheduleIndex+1;

	bool find = false;
	bool round = false;

	while(1)
	{
		if(index >= m_listSchedule.GetCount())
		{
			if(round == true)
			{
//				m_mutex.Unlock();

				return false;
			}
			else
			{
				round = true;
				index = 0;
			}
		}

		CSchedule* schedule = m_listSchedule[index];

		if(schedule->IsBetween())
		{
//			m_mutex.Unlock();

			return true;
		}

		index++;
	}

//	m_mutex.Unlock();

	return false;
}

bool CFrame::GetNextDefaultSchedule(int& index)
{
//	m_mutex.Lock();

//	if(m_templateId != CHost::GetDefaultTemplateid()) // defailt template만 재생
//	{
////		m_mutex.Lock();
//
//		return false;
//	}

	if(m_listDefaultSchedule.GetCount() == 0)
	{
//		m_mutex.Unlock();

		return false;
	}

	index = m_nCurrentDefaultScheduleIndex+1;
	if(index >= m_listDefaultSchedule.GetCount())
		index = 0;

	bool find = false;
	bool round = false;

	while(1) // 지정된 시간대만 검색
	{
		if(index >= m_listDefaultSchedule.GetCount())
		{
			if(round == true)
			{
//				m_mutex.Unlock();
				break;
			}
			else
			{
				round = true;
				index = 0;
			}
		}

		CSchedule* schedule = m_listDefaultSchedule[index];

		if(schedule->IsBetween() /*&& schedule->GetTimeScope() != 0 */) // 임시 막아둠 -> 종일 방송도 같이 방송
		{
//			m_mutex.Unlock();
			return true;
		}

		index++;
	}

	index = m_nCurrentDefaultScheduleIndex+1;
	if(index >= m_listDefaultSchedule.GetCount())
		index = 0;

	find = false;
	round = false;

	while(1) // 24시간 검색
	{
		if(index >= m_listDefaultSchedule.GetCount())
		{
			if(round == true)
			{
//				m_mutex.Unlock();
				return false;
			}
			else
			{
				round = true;
				index = 0;
			}
		}

		CSchedule* schedule = m_listDefaultSchedule[index];

		if(schedule->IsBetween() && schedule->GetTimeScope() == 0 )
		{
//			m_mutex.Unlock();
			return true;
		}

		index++;
	}

//	m_mutex.Unlock();

	return false;
}

bool CFrame::SortDefaultSchedule()
{
//	m_mutex.Lock();

	int count = m_listDefaultSchedule.GetCount();
	for(int i=0; i<count-1; i++)
	{
		for(int j=i+1; j<count; j++)
		{
			CSchedule* schedule1 = m_listDefaultSchedule[i];
			CSchedule* schedule2 = m_listDefaultSchedule[j];

			if(schedule1->GetPlayOrder() > schedule2->GetPlayOrder())
			{
				m_listDefaultSchedule.SetAt(i, schedule2);
				m_listDefaultSchedule.SetAt(j, schedule1);

				if(m_pCurrentSchedule == schedule1)
					m_nCurrentDefaultScheduleIndex = j;
				if(m_pCurrentSchedule == schedule2)
					m_nCurrentDefaultScheduleIndex = i;
			}
		}
	}

//	m_mutex.Unlock();

	return true;
}

void CFrame::SetClipRgn(int x, int y, int width, int height)
{

	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);

	CPoint pts[4];
	pts[0].SetPoint((x - m_x) * xscale,			(y - m_y) * yscale);
	pts[1].SetPoint((x - m_x + width) * xscale,	(y - m_y) * yscale);
	pts[2].SetPoint((x - m_x + width) * xscale,	(y - m_y + height) * yscale);
	pts[3].SetPoint((x - m_x) * xscale,			(y - m_y + height) * yscale);

	CRgn rgn_in;
	rgn_in.CreatePolygonRgn(pts, 4, ALTERNATE);

	pts[0].SetPoint(0,					0);
	pts[1].SetPoint(m_width * xscale,	0);
	pts[2].SetPoint(m_width * xscale,	m_height * yscale);
	pts[3].SetPoint(0,					m_height * yscale);

	CRgn rgn_out;
	rgn_out.CreatePolygonRgn(pts, 4, ALTERNATE);

	CRgn rgn_xor;
	rgn_xor.CreateRectRgn( 0, 0, 50, 50 );

	rgn_xor.CombineRgn( &rgn_in, &rgn_out, RGN_XOR );

	SetWindowRgn((HRGN)rgn_xor.GetSafeHandle(), TRUE);

}

void CFrame::ResetClipRgn()
{

	SetWindowRgn(NULL, TRUE);

}

bool CFrame::FrameInFrame(CFrame* frame)
{
	return	(m_x <= frame->m_x && frame->m_x <= m_x+m_width) &&
			(m_y <= frame->m_y && frame->m_y <= m_y+m_height);
}

CString	CFrame::ToString()
{
	m_mutex.Lock();

	CString str;
	str.Format(
		"templateId = %s\r\n"
		"frameId = %s\r\n"
		"grade = %d\r\n"
		"width = %d\r\n"
		"height = %d\r\n"
		"x = %d\r\n"
		"y = %d\r\n"
		"isPIP = %d\r\n"
		"borderStyle = %d\r\n"
		"borderThickness = %d\r\n"
		"borderColor = %s\r\n"
		"cornerRadius = %d\r\n"
		"\r\n"
		"m_bCurrentPlay = %d\r\n"
		"m_pCurrentSchedule = %s\r\n"
		"m_pPIPParentWnd = %s\r\n"
		,	m_templateId.c_str()
		,	m_frameId.c_str()
		,	m_grade
		,	m_width
		,	m_height
		,	m_x
		,	m_y
		,	m_isPIP
		,	m_borderStyle.c_str()
		,	m_borderThickness
		,	m_borderColor.c_str()
		,	m_cornerRadius

		,	m_bCurrentPlay
		,	( (m_pCurrentSchedule == NULL) ? "NULL" : m_pCurrentSchedule->GetScheduleId() )
		,	( (m_pPIPParentWnd == NULL) ? "NULL" : m_pPIPParentWnd->GetFrameId() )

	);

	m_mutex.Unlock();

	return str;
}

LRESULT CFrame::OnPlayNextSchedule(WPARAM wParam, LPARAM lParam)
{
	if(m_bCurrentPlay)
	{
		m_mutex.Lock();

		PlayNextSchedule();

		m_mutex.Unlock();
	}

	return 0;
}

bool CFrame::LoadSchedule(bool bDefaultSchedule)
{
	char	tmpBuffer[1024];

	if(bDefaultSchedule)
		GetPrivateProfileString(m_strLoadID, "DefaultScheduleList", "", tmpBuffer, 1024, GetDataPath());
	else
		GetPrivateProfileString(m_strLoadID, "ScheduleList", "", tmpBuffer, 1024, GetDataPath());
	CString schedule_list = tmpBuffer;

	//
	double xscale, yscale;
	m_pParentWnd->GetScale(xscale, yscale);

	//
	CRect rect;
	GetClientRect(rect);
	rect.DeflateRect(m_borderThickness * xscale, m_borderThickness * yscale);

	//
	int pos = 0;
	CString token = schedule_list.Tokenize(",", pos);
	while(token != "")
	{
		CString strErrorMessage;
		CSchedule* schedule = CSchedule::GetScheduleObject(this, token, rect, bDefaultSchedule, strErrorMessage);
		if(schedule)
		{
			m_mutex.Lock();

			if(bDefaultSchedule)
			{
				m_listDefaultSchedule.Add(schedule);
			}
			else
			{
				m_listSchedule.Add(schedule);
			}

			m_mapSchedule.SetAt(schedule->GetScheduleId(), schedule);

			SortDefaultSchedule();

			m_mutex.Unlock();
		}
		else
		{
			strErrorMessage.Insert(0, "\t\t");
		}

		token = schedule_list.Tokenize(",", pos);
	}

	return true;
}

bool CFrame::Save()
{
	WritePrivateProfileString(m_strLoadID, "templateId", m_templateId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "frameId", m_frameId.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "grade", ::ToString(m_grade), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "width", ::ToString(m_width), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "height", ::ToString(m_height), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "x", ::ToString(m_x), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "y", ::ToString(m_y), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "isPIP", ::ToString(m_isPIP), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "borderStyle", m_borderStyle.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "borderThickness", ::ToString(m_borderThickness), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "borderColor", m_borderColor.c_str(), GetDataPath());
	WritePrivateProfileString(m_strLoadID, "cornerRadius", ::ToString(m_cornerRadius), GetDataPath());

	m_mutex.Lock();

	CString str_schedule_list;
	CString str_defaultschedule_list;

	// schedule
	int count = m_listSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listSchedule[i];

		schedule->Save();

		if(str_schedule_list.GetLength() != 0)
			str_schedule_list.Append(",");

		str_schedule_list.Append(schedule->GetLoadID());
	}

	// default schedule
	count = m_listDefaultSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listDefaultSchedule[i];

		schedule->Save();

		if(str_defaultschedule_list.GetLength() != 0)
			str_defaultschedule_list.Append(",");

		str_defaultschedule_list.Append(schedule->GetLoadID());
	}

	m_mutex.Unlock();

	WritePrivateProfileString(m_strLoadID, "ScheduleList", str_schedule_list, GetDataPath());
	WritePrivateProfileString(m_strLoadID, "DefaultScheduleList", str_defaultschedule_list, GetDataPath());

	return true;
}

void CFrame::ResetPlayCount()
{
	int count = m_listDefaultSchedule.GetCount();
	for(int i=0; i<count; i++)
	{
		CSchedule* schedule = m_listDefaultSchedule[i];

		schedule->ResetPlayCount();
	}
}

void CFrame::OnMouseMove(UINT nFlags, CPoint point)
{
	CWnd::OnMouseMove(nFlags, point);

//	::AfxGetMainWnd()->PostMessage(WM_VIEW_MOUSE_CURSOR);
}
