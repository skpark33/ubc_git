// VideoContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "VideoContentsDialog.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common\ubcdefine.h"
#include "common/PreventChar.h"
#include "common/MD5Util.h"

#define		WM_CONTENTS_PLAY		1024


// CVideoContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CVideoContentsDialog, CSubContentsDialog)

CVideoContentsDialog::CVideoContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CVideoContentsDialog::IDD, pParent)
	, m_bContentsMute (true)
	, m_bPreviewMute (true)
	, m_wndVideo (NULL, 0, false)
	, m_reposControl (this)
	, m_strLocation("")
	, m_bPreviewMode(false)
	, m_cbxContentsFont(IDB_TTF_BMP)
{
	m_ulFileSize = 0;
}

CVideoContentsDialog::~CVideoContentsDialog()
{
}

void CVideoContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_CONTENTS_FILE, m_btnBrowserContentsFile);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_VOLUME, m_editContentsVolume);
	DDX_Control(pDX, IDC_SLIDER_CONTENTS_VOLUME, m_sliderContentsVolume);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_PLAY_MINUTE, m_staticContentsPlayMinute);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_PLAY_SECOND, m_staticContentsPlaySecond);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_WIDTH, m_staticContentsWidth);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_HEIGHT, m_staticContentsHeight);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_FILE_SIZE, m_staticContentsFileSize);

	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_groupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_PLAY, m_btnPreviewPlay);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_STOP, m_btnPreviewStop);
	DDX_Control(pDX, IDC_SLIDER_PREVIEW_POSITION, m_sliderPreviewPosition);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_MUTE, m_btnPreviewMute);
	DDX_Control(pDX, IDC_SLIDER_PREVIEW_VOLUME, m_sliderPreviewVolume);

	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FONT_SIZE, m_editContentsFontSize);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_FONT, m_cbxContentsFont);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_TEXT_COLOR, m_cbxContentsTextColor);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_BG_COLOR, m_cbxContentsBGColor);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SPEED, m_editContentsPlaySpeed);
	DDX_Control(pDX, IDC_SLIDER_CONTENTS_PLAY_SPEED, m_sliderContentsPlaySpeed);
	DDX_Control(pDX, IDC_LIST_CONTENTS_COMMENT, m_listctrlContentsComment);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_ADD, m_btnContentsCommentAdd);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_DELETE, m_btnContentsCommentDelete);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_UP, m_btnContentsCommentUp);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_DOWN, m_btnContentsCommentDown);
	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가
	DDX_Control(pDX, IDC_EDIT_FELICA_URL, m_editFelicaUrl);
}


BEGIN_MESSAGE_MAP(CVideoContentsDialog, CSubContentsDialog)
	ON_WM_HSCROLL()
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_EN_CHANGE(IDC_EDIT_CONTENTS_VOLUME, &CVideoContentsDialog::OnEnChangeEditContentsVolume)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_CONTENTS_FILE, &CVideoContentsDialog::OnBnClickedButtonBrowserContentsFile)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_PLAY, &CVideoContentsDialog::OnBnClickedButtonPreviewPlay)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_STOP, &CVideoContentsDialog::OnBnClickedButtonPreviewStop)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_MUTE, &CVideoContentsDialog::OnBnClickedButtonPreviewMute)

	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST_CONTENTS_COMMENT, &CVideoContentsDialog::OnLvnEndlabeleditListContentsComment)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_CONTENTS_COMMENT, &CVideoContentsDialog::OnLvnKeydownListContentsComment)
	ON_EN_KILLFOCUS(IDC_EDIT_CONTENTS_PLAY_SPEED, &CVideoContentsDialog::OnEnKillfocusEditContentsPlaySpeed)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_UP, &CVideoContentsDialog::OnBnClickedButtonContentsCommentUp)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_DOWN, &CVideoContentsDialog::OnBnClickedButtonContentsCommentDown)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_ADD, &CVideoContentsDialog::OnBnClickedButtonContentsCommentAdd)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_DELETE, &CVideoContentsDialog::OnBnClickedButtonContentsCommentDelete)
	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가
END_MESSAGE_MAP()


// CVideoContentsDialog 메시지 처리기입니다.

BOOL CVideoContentsDialog::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnBrowserContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));

	m_btnPreviewPlay.LoadBitmap(IDB_BTN_PLAY, RGB(255, 255, 255));
	m_btnPreviewStop.LoadBitmap(IDB_BTN_STOP, RGB(255, 255, 255));
	//m_btnMute;

	m_btnContentsCommentAdd.LoadBitmap(IDB_BTN_PLUS, RGB(255, 255, 255));
	m_btnContentsCommentDelete.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnContentsCommentUp.LoadBitmap(IDB_BTN_UP, RGB(255, 255, 255));
	m_btnContentsCommentDown.LoadBitmap(IDB_BTN_DOWN, RGB(255, 255, 255));

	//Tool tip
	m_btnBrowserContentsFile.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT001));

	m_btnPreviewPlay.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT002));
	m_btnPreviewStop.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT003));
	m_btnPreviewMute.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT004));

	m_btnContentsCommentAdd.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT005));
	m_btnContentsCommentDelete.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT006));
	m_btnContentsCommentUp.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT007));
	m_btnContentsCommentDown.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT008));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가
	m_editContentsFontSize.SetValue(36);

	m_cbxContentsFont.SetPreviewStyle (CFontPreviewCombo::NAME_ONLY, false);
	m_cbxContentsFont.SetFontHeight (12, true);

	m_cbxContentsTextColor.InitializeDefaultColors();
	m_cbxContentsBGColor.InitializeDefaultColors();

	m_cbxContentsTextColor.SetSelectedColorValue(RGB(255,255,255));
	m_cbxContentsBGColor.SetSelectedColorValue(RGB(0,0,0));

	m_editContentsPlaySpeed.SetValue(100);
	m_sliderContentsPlaySpeed.SetRange(10,1000);
	m_sliderContentsPlaySpeed.SetPos(100);

	CRect rect;
	m_listctrlContentsComment.GetClientRect(rect);
	m_listctrlContentsComment.InsertColumn(0, "comment", LVCFMT_LEFT, rect.Width() + 100);
	m_listctrlContentsComment.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER); // auto size
	m_listctrlContentsComment.SetTextBkColor(RGB(232,232,232));
	m_listctrlContentsComment.SetExtendedStyle(m_listctrlContentsComment.GetExtendedStyle() | LVS_EX_FULLROWSELECT);
	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가

	m_editContentsVolume.SetValue(0);

	OnBnClickedButtonPreviewMute();

	m_editContentsVolume.SetLimitText(3);
	m_editContentsVolume.SetValue(100);

//	m_hHandCur = GetSysHandCursor();
//	ASSERT( m_hHandCur != NULL );
//	m_sliderContentsVolume.SetFlipCursor(m_hHandCur);
	m_sliderContentsVolume.BuildThumbItem(IDB_SLIDER_THUMB, 11, 21);
	m_sliderContentsVolume.BuildBackItem(IDB_SLIDER_NORMAL_LONG, IDB_SLIDER_ACTIVE_LONG);
	m_sliderContentsVolume.SetTopOffset(0);
	m_sliderContentsVolume.SetLineSize(0);
	m_sliderContentsVolume.SetRange(0, 100);
	m_sliderContentsVolume.SetPos(100);

//	m_hHandCur = GetSysHandCursor();
//	ASSERT( m_hHandCur != NULL );
//	m_sliderPreviewVolume.SetFlipCursor(m_hHandCur);
	m_sliderPreviewVolume.BuildThumbItem(IDB_SLIDER_THUMB, 11, 21);
	m_sliderPreviewVolume.BuildBackItem(IDB_SLIDER_NORMAL_SHORT, IDB_SLIDER_ACTIVE_SHORT);
	m_sliderPreviewVolume.SetTopOffset(0);
	m_sliderPreviewVolume.SetLineSize(0);
	m_sliderPreviewVolume.SetRange(0, 100);
	m_sliderPreviewVolume.SetPos(100);

	//
	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);
	client_rect.DeflateRect(1,1);

	//m_wndVideo.m_bUseVMR9 = true;
	//m_wndVideo.m_bUseVMR9 = false;
	m_wndVideo.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndVideo.ShowWindow(SW_SHOW);

	m_reposControl.AddControl(&m_groupPreview, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_staticContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_wndVideo, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
//	m_reposControl.AddControl(&m_wndVideo, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnPreviewPlay, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnPreviewStop, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_sliderPreviewPosition, REPOS_FIX, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnPreviewMute, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_sliderPreviewVolume, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.Move();

	//
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsPlayMinute);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsPlaySecond);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsWidth);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsHeight);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsFileSize);

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}//if

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CVideoContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CSubContentsDialog::OnOK();
}

void CVideoContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CSubContentsDialog::OnCancel();
	GetParent()->PostMessage(WM_CLOSE);
}

void CVideoContentsDialog::Stop()
{
	m_wndVideo.Stop(false);
}

bool CVideoContentsDialog::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData(TRUE);

	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	//m_editContentsFileName.GetWindowText(info.strLocation);
	info.strLocalLocation = m_strLocation;
	info.nRunningTime = (ULONG)m_wndVideo.GetTotalPlayTime();
	info.nSoundVolume = m_editContentsVolume.GetValueInt();

	//
	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(info.strLocalLocation, dir, path, filename, ext);

	info.strLocalLocation.Format("%s%s", dir, path); 
	info.strFilename.Format("%s%s", filename, ext); 

	// Modified by 정운형 2008-12-24 오후 3:03
	// 변경내역 :  캡션기능 추가
	info.nFontSize = m_editContentsFontSize.GetValueInt();
	m_cbxContentsFont.GetLBText(m_cbxContentsFont.GetCurSel(), info.strFont);
	info.strFgColor = ::GetColorFromString(m_cbxContentsTextColor.GetSelectedColorValue());
	info.strBgColor = ::GetColorFromString(m_cbxContentsBGColor.GetSelectedColorValue());
	info.nPlaySpeed = m_editContentsPlaySpeed.GetValueInt();
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	for(int i=0; i<10; i++)
	{
		if(i<m_listctrlContentsComment.GetItemCount())
		{
			info.strComment[i] = m_listctrlContentsComment.GetItemText(i, 0);
		}
		else
		{
			info.strComment[i] = "";
		}//if
	}//if
	// Modified by 정운형 2008-12-24 오후 3:03
	// 변경내역 :  캡션기능 추가

	//
	if(!IsFitContentsName(info.strContentsName)) return false;
	if( info.strLocalLocation.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG002), MB_ICONSTOP);
		return false;
	}
	if( info.strFilename.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG003), MB_ICONSTOP);
		return false;
	}

	/*// Modified by 정운형 2008-12-24 오후 3:03
	// 변경내역 :  캡션기능 추가
	if(info.nFontSize == 0)
	{
		UbcMessageBox("Font Size is Zero !!!", MB_ICONSTOP);
		return false;
	}//if
	// Modified by 정운형 2008-12-24 오후 3:03
	// 변경내역 :  캡션기능 추가*/

	m_editFelicaUrl.GetWindowText(info.strComment[1]);
	info.strComment[1].Trim();
	if(!info.strComment[1].IsEmpty())
	{	
		CString strFelicaUrl = info.strComment[1];
		strFelicaUrl.MakeLower();
		if(strFelicaUrl.Find(_T("http://")) < 0)
		{
			UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG007), MB_ICONSTOP);
			return false;
		}
	}

	return true;
}

bool CVideoContentsDialog::SetContentsInfo(CONTENTS_INFO& info)
{
	m_editContentsName.SetWindowText(info.strContentsName);
	//m_editContentsFileName.SetWindowText(info.strLocation + info.strFilename);
	m_editContentsFileName.SetWindowText(info.strFilename);
	m_strLocation = info.strLocalLocation + info.strFilename;
	m_editContentsVolume.SetWindowText(::ToString(info.nSoundVolume));
	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	m_editFelicaUrl.SetWindowText(info.strComment[1]);

	OnEnChangeEditContentsVolume();

	// Modified by 정운형 2008-12-24 오후 3:03
	// 변경내역 :  캡션기능 추가
	m_editContentsFontSize.SetWindowText(::ToString(info.nFontSize));
	for(int i=0; i<m_cbxContentsFont.GetCount(); i++)
	{
		CString font_name;
		m_cbxContentsFont.GetLBText(i,font_name);
		if(font_name == info.strFont)
		{
			m_cbxContentsFont.SetCurSel(i);
			break;
		}
	}
	m_cbxContentsTextColor.SetSelectedColorValue(::GetColorFromString(info.strFgColor));
	m_cbxContentsBGColor.SetSelectedColorValue(::GetColorFromString(info.strBgColor));
	m_editContentsPlaySpeed.SetWindowText(::ToString(info.nPlaySpeed));
	OnEnKillfocusEditContentsPlaySpeed();

	m_listctrlContentsComment.DeleteAllItems();
	for(int i=0, j=0; i<10; i++)
	{
		if(info.strComment[i].GetLength() != 0)
			m_listctrlContentsComment.InsertItem(j++, info.strComment[i]);
	}//if
	// Modified by 정운형 2008-12-24 오후 3:03
	// 변경내역 :  캡션기능 추가

	UpdateData(FALSE);

	LoadContents(info.strLocalLocation + info.strFilename);

	return true;
}

void CVideoContentsDialog::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSubContentsDialog::OnHScroll(nSBCode, nPos, pScrollBar);

	if( pScrollBar->GetSafeHwnd() == m_sliderContentsVolume.GetSafeHwnd() )
	{
		m_editContentsVolume.SetWindowText(::ToString((int)m_sliderContentsVolume.GetPos()));
	}
	else if( pScrollBar->GetSafeHwnd() == m_sliderPreviewPosition.GetSafeHwnd() )
	{
		if(m_wndVideo.m_pInterface != NULL)
		{
			m_wndVideo.m_pInterface->SetPosition(m_sliderPreviewPosition.GetPos());
		}//if

	}
	else if( pScrollBar->GetSafeHwnd() == m_sliderPreviewVolume.GetSafeHwnd() )
	{
		if(m_wndVideo.m_pInterface != NULL)
			m_wndVideo.m_pInterface->SetVolume(m_sliderPreviewVolume.GetPos());
		//if(m_wndVideo.m_pVMRRender != NULL)
		//{
		//	m_wndVideo.m_pVMRRender->VMR_SetVolume(m_sliderPreviewVolume.GetPos());
		//}//if
	}
	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가
	else if( pScrollBar->GetSafeHwnd() == m_sliderContentsPlaySpeed.GetSafeHwnd() )
	{
		m_editContentsPlaySpeed.SetWindowText(::ToString((int)m_sliderContentsPlaySpeed.GetPos()));
	}//if
	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가
}

void CVideoContentsDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CSubContentsDialog::OnTimer(nIDEvent);

	if(nIDEvent == WM_CONTENTS_PLAY)
	{
		double tm = m_wndVideo.GetCurrentPlayTime();
		m_sliderPreviewPosition.SetPos((int)tm);
	}
}

void CVideoContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CSubContentsDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();

	//if(m_wndVideo.GetSafeHwnd() && m_wndVideo.m_pVMRRender != NULL)
	if(m_wndVideo.GetSafeHwnd() && m_wndVideo.m_pInterface != NULL)
	{
		CRect rect;
		m_wndVideo.GetClientRect(rect);
		m_wndVideo.m_pInterface->SetVideoRect(rect);
		///m_wndVideo.m_pVMRRender->->SetVideoRect(rect);
	}
}

void CVideoContentsDialog::OnEnChangeEditContentsVolume()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CSubContentsDialog::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int vol = m_editContentsVolume.GetValueInt();
	m_sliderContentsVolume.SetPos(vol);
	m_sliderContentsVolume.Invalidate(FALSE);

	if(vol != m_sliderContentsVolume.GetPos())
	{
		m_editContentsVolume.SetWindowText(::ToString((int)m_sliderContentsVolume.GetPos()));
	}
}

void CVideoContentsDialog::OnBnClickedButtonBrowserContentsFile()
{
	CString szFileExts = "*.avi; *.asf; *.mkv; *.mp4; *.mpg; *.mpeg; *.wmv; *.mov; *.tp; *.flv; *.mp3; *.wma; *.wav";
	CString szFileVideoExts = "*.avi; *.asf; *.mkv; *.mp4; *.mpg; *.mpeg; *.wmv; *.mov; *.tp; *.flv";
	CString szFileMusicExts = "*.mp3; *.wma; *.wav";
	CString filter;
	filter.Format("All Media Files|%s|"
		          "All Video Files|%s|"
		          "All Music Files|%s|"
				  "AVI Files (*.avi)|*.avi|"
				  "ASF Files (*.asf)|*.asf|"
				  "MKV Files (*.mkv)|*.mkv|"
				  "MP4 Files (*.mp4)|*.mp4|"
				  "MPEG Files (*.mpg;*.mpeg)|*.mpg;*.mpeg|"
				  "WMV Files (*.wmv)|*.wmv|"
				  "MOV Files (*.mov)|*.mov|"
				  "TP Files (*.tp)|*.tp|"
				  "FLV Files (*.flv)|*.flv|"
				  "MP3 Files (*.mp3)|*.mp3|"
				  "WAV Files (*.wav)|*.wav|"
				  "WMA Files (*.wma)|*.wma||"
				 , szFileExts
				 , szFileVideoExts
				 , szFileMusicExts
				 );

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내비디오
		TCHAR szMyVideos[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyVideos, CSIDL_MYVIDEO, FALSE);
		::SetCurrentDirectory(szMyVideos);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, CONTENTS_VIDEO)){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!CheckTotalContentsSize(dlg.GetPathName())) return;

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	//m_editContentsFileName.SetWindowText(dlg.GetPathName());
	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = dlg.GetPathName();
	m_editContentsFileName.SetSel(0, -1);

	m_wndVideo.CloseFile();

	LoadContents(dlg.GetPathName());

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);
	if(contents_name.GetLength() == 0)
	{
		char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
		_splitpath(dlg.GetPathName(), drive, path, filename, ext);

		m_editContentsName.SetWindowText(filename);
	}

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(m_ulFileSize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
	}
	m_strFileMD5 = CString(szMd5);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CVideoContentsDialog::OnBnClickedButtonPreviewPlay()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	EnableAllControls(FALSE);

	m_wndVideo.m_bMute = m_bPreviewMute;
	m_wndVideo.Play(true);

	SetTimer(WM_CONTENTS_PLAY, 100, NULL);
}

void CVideoContentsDialog::OnBnClickedButtonPreviewStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	EnableAllControls(TRUE);

	m_wndVideo.Stop(false);

	KillTimer(WM_CONTENTS_PLAY);
}

void CVideoContentsDialog::OnBnClickedButtonPreviewMute()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_bPreviewMute = !m_bPreviewMute;
	m_wndVideo.m_bMute = m_bPreviewMute;

	if(m_bPreviewMute)
	{
		m_btnPreviewMute.LoadBitmap(IDB_BTN_MUTE_ON, RGB(232,168,192));
		if(m_wndVideo.m_pInterface != NULL)
			m_wndVideo.m_pInterface->SetVolume(0);
	}
	else
	{
		m_btnPreviewMute.LoadBitmap(IDB_BTN_MUTE_OFF, RGB(232,168,192));
		if(m_wndVideo.m_pInterface != NULL)
			m_wndVideo.m_pInterface->SetVolume(m_sliderPreviewVolume.GetPos());
	}
}

void CVideoContentsDialog::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editContentsFileName.EnableWindow(bEnable);
	m_btnBrowserContentsFile.EnableWindow(bEnable);
	m_editContentsVolume.EnableWindow(bEnable);
	m_sliderContentsVolume.EnableWindow(bEnable);
	m_staticContentsPlayMinute.EnableWindow(bEnable);
	m_staticContentsPlaySecond.EnableWindow(bEnable);
	m_staticContentsWidth.EnableWindow(bEnable);
	m_staticContentsHeight.EnableWindow(bEnable);
	m_staticContentsFileSize.EnableWindow(bEnable);

	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가
	m_editContentsFontSize.EnableWindow(bEnable);
	m_cbxContentsFont.EnableWindow(bEnable);
	m_cbxContentsTextColor.EnableWindow(bEnable);
	m_cbxContentsBGColor.EnableWindow(bEnable);
	m_editContentsPlaySpeed.EnableWindow(bEnable);
	m_sliderContentsPlaySpeed.EnableWindow(bEnable);
	m_listctrlContentsComment.EnableWindow(bEnable);
	m_btnContentsCommentAdd.EnableWindow(bEnable);
	m_btnContentsCommentDelete.EnableWindow(bEnable);
	m_btnContentsCommentUp.EnableWindow(bEnable);
	m_btnContentsCommentDown.EnableWindow(bEnable);
	// Modified by 정운형 2008-12-23 오후 4:01
	// 변경내역 :  캡션기능 추가
	m_editFelicaUrl.EnableWindow(bEnable);
}

bool CVideoContentsDialog::LoadContents(LPCTSTR lpszFullPath)
{
	CFileStatus fs;
//	if(CFile::GetStatus(lpszFullPath, fs))
	if(CEnviroment::GetFileSize(lpszFullPath, fs.m_size))
	{
		m_wndVideo.m_strMediaFullPath = lpszFullPath;

		if(m_wndVideo.OpenFile(1))
		{
			//m_wndVideo.m_soundVolume = m_sliderPreviewVolume.GetPos();
			//m_wndVideo.Play();
			//m_wndVideo.Pause();

			int play_time = (int)m_wndVideo.GetTotalPlayTime();
			int play_minute = play_time / 60;
			int play_second = play_time % 60;

			char szTemp[10];
			_splitpath(lpszFullPath, NULL, NULL, NULL, szTemp);
			CString szExt = szTemp;
			szExt.MakeLower();
			
			CRect video_rect;
			if(IsSame(szExt, ".wav") || IsSame(szExt, ".mp3")){
				ZeroMemory(video_rect, sizeof(video_rect));
			}
			else if(m_wndVideo.m_pInterface != NULL){
				m_wndVideo.m_pInterface->GetVideoRect(video_rect);
			}//if

			m_sliderPreviewPosition.SetRange(0, play_time);

			m_wndVideo.m_soundVolume = m_sliderPreviewVolume.GetPos();

			m_staticContentsPlayMinute.SetWindowText(::ToString(play_minute) + " ");
			m_staticContentsPlaySecond.SetWindowText(::ToString(play_second) + " ");
			m_staticContentsWidth.SetWindowText(::ToString(video_rect.Width()) + " ");
			m_staticContentsHeight.SetWindowText(::ToString(video_rect.Height()) + " ");
			m_staticContentsFileSize.SetWindowText(::ToMoneyTypeString((ULONGLONG)(fs.m_size/1024)) + " ");
			m_ulFileSize = fs.m_size;

			m_wndVideo.Play();
			m_wndVideo.Stop(false);
		}

		m_wndVideo.Invalidate();

		return true;
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_VIDEOCONTENTSDIALOG_MSG001), MB_ICONSTOP);
	}

	return false;
}


// Modified by 정운형 2008-12-23 오후 4:01
// 변경내역 :  캡션기능 추가
void CVideoContentsDialog::OnLvnEndlabeleditListContentsComment(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pDispInfo->item.pszText != NULL)
	{
		m_listctrlContentsComment.SetItemText(pDispInfo->item.iItem, pDispInfo->item.iSubItem, pDispInfo->item.pszText );
		m_listctrlContentsComment.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER); // auto size
	}
}


void CVideoContentsDialog::OnLvnKeydownListContentsComment(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	switch(pLVKeyDow->wVKey)
	{
	case VK_F2:
		for(int i=0; i<m_listctrlContentsComment.GetItemCount(); i++)
		{
			if(m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED))
			{
				m_listctrlContentsComment.SetFocus();
				m_listctrlContentsComment.EditLabel(i);
			}
		}
		break;
	case VK_DELETE:
		OnBnClickedButtonContentsCommentDelete();
		break;
	}
}

void CVideoContentsDialog::OnEnKillfocusEditContentsPlaySpeed()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int vol = m_editContentsPlaySpeed.GetValueInt();
	m_sliderContentsPlaySpeed.SetPos(vol);
	m_sliderContentsPlaySpeed.Invalidate(FALSE);

	if(vol != m_sliderContentsPlaySpeed.GetPos())
	{
		m_editContentsPlaySpeed.SetWindowText(::ToString((int)m_sliderContentsPlaySpeed.GetPos()));
	}
}

void CVideoContentsDialog::OnBnClickedButtonContentsCommentUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	for(int i=1; i<m_listctrlContentsComment.GetItemCount(); i++)
	{
		UINT state = m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED);
		if(state == LVIS_SELECTED)
		{
			MoveItem(i, i-1);
		}
	}
}

void CVideoContentsDialog::OnBnClickedButtonContentsCommentDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	for(int i=m_listctrlContentsComment.GetItemCount()-2; i>=0; i--)
	{
		UINT state = m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED);
		if(state == LVIS_SELECTED)
		{
			MoveItem(i, i+1);
		}
	}
}

void CVideoContentsDialog::OnBnClickedButtonContentsCommentAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(m_listctrlContentsComment.GetItemCount() < TICKER_COUNT)
	{
		int index = m_listctrlContentsComment.GetItemCount();
		m_listctrlContentsComment.SetFocus();
		m_listctrlContentsComment.InsertItem(index, "");
		m_listctrlContentsComment.EditLabel(index);
	}
}

void CVideoContentsDialog::OnBnClickedButtonContentsCommentDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int count = m_listctrlContentsComment.GetItemCount();

	for(int i=0; i<count; i++)
	{
		if(m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED))
		{
			m_listctrlContentsComment.DeleteItem(i);
			count--;
			i--;
		}
	}
}

void CVideoContentsDialog::MoveItem(int nItem, int nItemNewPos)
{
	int count = m_listctrlContentsComment.GetItemCount();

	if( nItem == nItemNewPos ) return;
	if( nItem < 0 || nItemNewPos < 0 ) return;
	if( count <= nItem || count <= nItemNewPos) return;

	CString str = m_listctrlContentsComment.GetItemText(nItem, 0);
	UINT old_state = m_listctrlContentsComment.GetItemState(nItem, 0xffff);

	m_listctrlContentsComment.DeleteItem(nItem);

	m_listctrlContentsComment.InsertItem(nItemNewPos, str);
	m_listctrlContentsComment.SetItemState(nItemNewPos, old_state, 0xffff);
}
// Modified by 정운형 2008-12-23 오후 4:01
// 변경내역 :  캡션기능 추가

BOOL CVideoContentsDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}