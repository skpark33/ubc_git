// MainFrm.cpp : implementation of the CMainFrame class
//
#include "stdafx.h"
#include "UBCStudio.h"
#include "MainFrm.h"
#include "ConfigDlg.h"

#include "NotifySaveDlg.h"
#include "RunPreviewThread.h"
#include "ConfigSaveThread.h"
#include "FtpUploadDlg.h"

#include "LayoutFrame.h"
#include "PackageFrame.h"
#include "PlayContentsSearchFrame.h"

#include "Dbghelp.h"
#include "shlwapi.h"
#include "ExportDlg.h"
#include "ExportEEDlg.h"

// Modified by 정운형 2009-01-28 오후 1:43
// 변경내역 :  이미지 추가 작업
#include "HostSelectDlg.h"
#include "LocalHostInfo.h"
// Modified by 정운형 2009-01-28 오후 1:43
// 변경내역 :  이미지 추가 작업

#include "RegistDlg.h"
#include "NetworkHostDlg.h"
//using namespace std;
#include "common\libscratch\scratchUtil.h"
#include "common\libscratch\orbconn.h"
#include "DeletePackageDlg.h"
#include "NetworkHostManageDlg.h"
#include "FileTranInfo.h"
#include "FileCleaner.h"
#include "UnusedContentDlg.h"
#include "AnnounceDlg.h"

#include <tlhelp32.h>
#include <shlwapi.h>
#include <winternl.h>

//#include <winsvc.h>
//#include <process.h>
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "ubccommon\ftpreportdlg.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "ReplaceInFile.h"

#ifdef _UBCSTUDIO_EE_
#include "LoginDlg.h"
#include "EEPackageDlg.h"
#include "ubccopcommon\ubccopcommon.h"
#include "ubccopcommon\ftpmultisite.h"
//#include "ContentsManagerDlg.h"
#include "PublicContents/PublicContentsDlg.h"
#else
#define GetSiteName(x) x
#endif//_UBCSTUDIO_EE_

#ifdef _TAO
#include "CopModule.h"
#endif//_TAO

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define TIMER_INIT		555		//초기 편성관리 자동로드 타이머 id
#define TIMER_NEW		556		//새로만들기 타이머 id
#define TIMER_EXIT		557		//프로그램 종료 타이머 id
#define TIMER_AUTO_SAVE 600		// 0001128: Studio 임시 저장 및 복구
#define MIN_WIDTH		700
#define MIN_HEIGHT		630

#ifdef SubclassWindow 
#undef SubclassWindow 
#endif

typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if (!IsWindowVisible(hwnd)) return TRUE;
	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);
	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;
	if (pe->pid != pid) return TRUE;
	pe->hwnd = hwnd;
	return FALSE;
}


// CMainFrame
IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_WM_COPYDATA()
	ON_WM_GETMINMAXINFO()
	ON_WM_WINDOWPOSCHANGING()
	ON_MESSAGE(WM_TEMPLATE_INFO_CHANGED, OnTemplateInfoChanged)
	ON_MESSAGE(WM_FRAME_INFO_CHANGED, OnFrameInfoChanged)
	ON_MESSAGE(WM_ALL_INFO_CHANGED, OnAllInfoChanged)
	ON_MESSAGE(WM_TEMPLATE_LIST_CHANGED, OnTemplateListChanged)
	ON_MESSAGE(WM_PLAY_TEMPLATE_LIST_CHANGED, OnPlayTemplateListChanged)
	ON_MESSAGE(WM_CONTENTS_LIST_CHANGED, OnContentsListChanged)
	ON_MESSAGE(WM_COMPLETE_FILE_COPY, OnCompleteFileCopy)
	ON_MESSAGE(WM_FTP_UPLOAD, OnFTPUpload)
	ON_MESSAGE(WM_PREVIEW_COMPELETE, OnPreviewComplete)
	ON_MESSAGE(WM_SAVE_CONFIG_COMPELETE, OnConfigSaveComplete)
	ON_COMMAND_RANGE(ID_LAYOUT_MANAGE, ID_PLAYCONTENTS_SEARCH, OnNewFrame)
	//ON_COMMAND(ID_INFO_EXPORT, &CMainFrame::OnInfoExport)
	//ON_COMMAND(ID_REGIST_KEY, &CMainFrame::OnRegistKey)
	ON_COMMAND(ID_INFO_IMPORT, &CMainFrame::OnInfoImport)
	ON_COMMAND(ID_SAVE, &CMainFrame::OnSave)
	ON_COMMAND(ID_SAVE_AS, &CMainFrame::OnSaveAs)
	ON_COMMAND(ID_INFO_NEW, &CMainFrame::OnInfoNew)
	ON_COMMAND(ID_TOOLS_FILECLEANER, OnFileCleaner)
	ON_COMMAND(ID_TOOLS_EXPLORER, OnExplorer)
	ON_COMMAND(ID_NETWORK_EXPORT, &CMainFrame::OnNetworkExport)
	ON_COMMAND(ID_PACKAGE_CLOSE, &CMainFrame::OnPackageClose)
	ON_COMMAND(ID_PACKAGE_DELETE, &CMainFrame::OnPackageDelete)
	ON_COMMAND(ID_EDIT_NETWORK_HOST, &CMainFrame::OnEditNetworkHost)
	ON_COMMAND(ID_FILE_RECOVERY, &CMainFrame::OnFileRecovery)
	ON_COMMAND(ID_FILE_RESERVATION, &CMainFrame::OnFileReservation)
	ON_COMMAND(ID_TOOLS_UNUSEDCONTENTSLIST, OnUnusedContentsList)
	ON_MESSAGE(WM_INFOIMPORT_COMPELETE, OnImportComplete)
	ON_NOTIFY_EX(TTN_NEEDTEXTA, 0, OnToolTipText)
	ON_NOTIFY_EX(TTN_NEEDTEXTW, 0, OnToolTipText)
	ON_COMMAND_RANGE(ID_LATEST_PACKAGE_1, ID_LATEST_PACKAGE_10, &CMainFrame::OnLatestPackage)
	ON_COMMAND(ID_AUTO_UPDATE, &CMainFrame::OnAutoUpdate)
	ON_COMMAND(ID_CONTENTS_MANAGER, &CMainFrame::OnContentsManager)
	ON_COMMAND(ID_CONFIGURATION, &CMainFrame::OnConfiguration)
	ON_COMMAND(ID_FILE_LOAD_FROM_TEMPORARY, &CMainFrame::OnFileLoadFromTemporary)
	ON_UPDATE_COMMAND_UI(ID_SAVE, &CMainFrame::OnUpdateSave)
	ON_UPDATE_COMMAND_UI(ID_SAVE_AS, &CMainFrame::OnUpdateSaveAs)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_MANAGE, &CMainFrame::OnUpdateLayoutManage)
	ON_UPDATE_COMMAND_UI(ID_PACKAGE_MANAGE, &CMainFrame::OnUpdatePackageManage)
	ON_UPDATE_COMMAND_UI(ID_CONTENTS_MANAGER, &CMainFrame::OnUpdateContentsManager)
	ON_UPDATE_COMMAND_UI(ID_NETWORK_EXPORT, &CMainFrame::OnUpdateNetworkExport)
	ON_UPDATE_COMMAND_UI(ID_AUTO_UPDATE, &CMainFrame::OnUpdateAutoUpdate)
	ON_UPDATE_COMMAND_UI(ID_INFO_IMPORT, &CMainFrame::OnUpdateInfoImport)
	ON_UPDATE_COMMAND_UI(ID_INFO_NEW, &CMainFrame::OnUpdateInfoNew)
	ON_UPDATE_COMMAND_UI(ID_PACKAGE_DELETE, &CMainFrame::OnUpdatePackageDelete)
	ON_UPDATE_COMMAND_UI(ID_FILE_RECOVERY, &CMainFrame::OnUpdateFileRecovery)
	ON_UPDATE_COMMAND_UI(ID_FILE_LOAD_FROM_TEMPORARY, &CMainFrame::OnUpdateFileLoadFromTemporary)
	ON_UPDATE_COMMAND_UI(ID_PACKAGE_CLOSE, &CMainFrame::OnUpdatePackageClose)
	ON_COMMAND(ID_FILE_NOTIFICATION, &CMainFrame::OnFileNotification)
	ON_COMMAND(ID_CHANGE_PACKAGE_PROPERTY, &CMainFrame::OnChangePackageProperty)
	ON_UPDATE_COMMAND_UI(ID_CHANGE_PACKAGE_PROPERTY, &CMainFrame::OnUpdateChangePackageProperty)
	ON_COMMAND(ID_TOOLS_AUTOENCODING, &CMainFrame::OnToolsAutoencoding)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame construction/destruction
CMainFrame::CMainFrame()
//:	m_interface(this)
:	m_pNotiDlg(NULL)
,	m_pthreadPreview(NULL)
,	m_pThreadConfigSave(NULL)
,	m_pdlgFtp(NULL)
,	m_sStateSave(E_STATE_SAVE_NONE)
{
	m_bRecovery = false;
}

CMainFrame::~CMainFrame()
{
	ClearLocalHostInfoArray();
	
	if(m_pNotiDlg)
	{
		delete m_pNotiDlg;
	}//if
}

void CMainFrame::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(m_document.IsModify())
	{
		int ret_value = UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG001), MB_YESNOCANCEL|MB_ICONWARNING);

		if(IDCANCEL == ret_value) return;
		else if(IDYES == ret_value)
		{
			//m_bTerminate = true;
			m_sStateSave = E_STATE_SAVE_EXIT;
			OnSave();

			if(m_pThreadConfigSave)
				WaitForSingleObject(m_pThreadConfigSave->m_hThread, 5000);
		}
	}//if

	CEnviroment::Release();
	scratchUtil::clearInstance();
#ifdef _TAO
	DestroyORB();
	CopComRelease();
#endif//_TAO
	CMDIFrameWnd::OnClose();
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	TraceLog(("OnCreate()"));
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if(!m_wndBitmapBG.SubclassWindow(m_hWndMDIClient))
	{
		//TRACE("Failed to subclass MDI client window.\n");
		return -1;
	}

	TraceLog(("m_wndToolBar.CreateEx()"));
	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioPE)
	{
		if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
			| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
			!m_wndToolBar.LoadToolBar(IDR_MAINFRAME_PE))
		{
			TRACE0("Failed to create toolbar\n");
			return -1;      // fail to create
		}
	}
	else if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
			| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
			!m_wndToolBar.LoadToolBar(IDR_MAINFRAME_EE))
		{
			TRACE0("Failed to create toolbar\n");
			return -1;      // fail to create
		}
	}

	TraceLog(("m_wndStatusBar.Create()"));
	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioPE)
		InitToolbarForPE();
	else if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
		InitToolbarForEE();

	//WriteRegistFile("");
	GetEnvPtr()->m_bLicense = CheckLicense();
	TraceLog(("CheckLicense(%d)",GetEnvPtr()->m_bLicense));

	// 2011.04.28 ubckill.bat 을 FirmwareView 측에서 호출하도록 함.
	////먼저 실행중인 UBC system 프로그램들을 전부 종료한다
	//CString strDrive, strKillPath;
	//strDrive = GetEnvPtr()->m_szDrive;
	//strDrive += _T("\\");

	//UINT nRet;
	//strKillPath.Format("%s%s%s", strDrive, UBC_EXECUTE_PATH, "ubckill.bat");
	//TraceLog(("WinExec(%s)", strKillPath));
	//nRet = WinExec(strKillPath, SW_HIDE);
	//if(nRet < 31)
	//{
	////	UbcMessageBox("Fail to kill UBC programs !!!", MB_ICONERROR);
	//}//if
/*
	scratchUtil* aUtil = scratchUtil::getInstance();
	if(!aUtil->ubcKill(14007))
	{
		//UbcMessageBox("Fail to kill UBC programs !!!", MB_ICONWARNING);
	}//if
*/

#ifdef _VER_SHARED_DATE
	if(!CheckRegist())
	{
		SetTimer(TIMER_EXIT, 10, NULL);
		return 0;
	}//if
#endif

	if(CEnviroment::eNARSHA == GetEnvPtr()->m_Customer){
		SetIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME_NS),false);
	}else{
		SetIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME),false);
	}

	// 패키지예약 메뉴 및 툴바를 삭제한다.
	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		//if( CEnviroment::eKIA     == GetEnvPtr()->m_Customer ||
		//	CEnviroment::eHYUNDAI == GetEnvPtr()->m_Customer ||
		//	CEnviroment::eKMNL    == GetEnvPtr()->m_Customer ||
		//	CEnviroment::eKMCL    == GetEnvPtr()->m_Customer )
		//{
			CToolBarCtrl& ctrToolBar = m_wndToolBar.GetToolBarCtrl(); 
			ctrToolBar.DeleteButton( ctrToolBar.CommandToIndex(ID_FILE_RESERVATION) );

			//CMenu* pMainMenu = GetMenu();
			//if(pMainMenu) pMainMenu->DeleteMenu(ID_FILE_RESERVATION, MF_BYCOMMAND);
		//}
	}
/*
	m_interface.SetParentWindow(GetSafeHwnd());
	if(m_interface.Open("dummy.avi"))
	{
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG002), MB_ICONSTOP);
	}
*/
	//초기화 타이머
	SetTimer(TIMER_INIT, 10, NULL);

	if(GetEnvPtr()->m_bUseAutoTempSave && GetEnvPtr()->m_nAutoTempSaveInterval > 0)
	{
		SetTimer(TIMER_AUTO_SAVE, GetEnvPtr()->m_nAutoTempSaveInterval * 60 * 1000, NULL);
	}
	AfxBeginThread(CMainFrame::StrongSecurityThread, this);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE){
		cs.style &= ~FWS_ADDTOTITLE ;
		SetTitle(LoadStringById(IDS_MAINFRAME_UBCEE));
	}
	else if(GetEnvPtr()->m_Edition == CEnviroment::eStudioPE){
		cs.style &= ~FWS_ADDTOTITLE ;
		SetTitle(LoadStringById(IDS_MAINFRAME_UBCPE));
	}

	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	RECT stRect;
	if(SystemParametersInfo(SPI_GETWORKAREA, 0, &stRect, 0)){
		if(stRect.bottom < 740){
			cs.cy = stRect.bottom-40;
			if(cs.cy > stRect.bottom){
				cs.cy = 480;
			}
		}else{
			cs.cy = 740;
		}

		if(stRect.right < 1024){
			cs.cx = stRect.right-40;
			if(cs.cx > stRect.right){
				cs.cx = 640;
			}
		}else{
			cs.cx = 1024;
		}
	}

	//cs.lpszName = "UBCStudio PE";

	return TRUE;
}

// CMainFrame diagnostics
#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

void CMainFrame::InitToolbarForPE()
{
	// Set up hot bar image lists.
	CImageList imageList;
	CBitmap    bitmap;

	// Create and set the normal toolbar image list.
	bitmap.LoadBitmap(IDB_TOOLBAR_ENABLE_PE);
	//bitmap.LoadBitmap(IDB_TOOLBAR);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	// Create and set the disable toolbar image list.
	bitmap.LoadBitmap(IDB_TOOLBAR_DISABLE_PE);
	//bitmap.LoadBitmap(IDB_TOOLBAR);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETDISABLEDIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	// Create and set the hot toolbar image list.
	bitmap.LoadBitmap(IDB_TOOLBAR_HOT_PE);
	//bitmap.LoadBitmap(IDB_TOOLBAR_DISABLE);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETHOTIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	//tool bar 크기 조정
	m_wndToolBar.SetSizes(CSize(48+7,48+6), CSize(48, 48));

	// 열기버튼 옆에 드랍다운 툴버튼 추가
	m_wndToolBar.GetToolBarCtrl().SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);
	TBBUTTONINFO tbi;
	tbi.dwMask= TBIF_STYLE;
	tbi.cbSize= sizeof(TBBUTTONINFO);
	m_wndToolBar.GetToolBarCtrl().GetButtonInfo(ID_INFO_IMPORT, &tbi);
	tbi.fsStyle |= TBSTYLE_DROPDOWN;
	m_wndToolBar.GetToolBarCtrl().SetButtonInfo(ID_INFO_IMPORT, &tbi);
}

void CMainFrame::InitToolbarForEE()
{
	// Set up hot bar image lists.
	CImageList imageList;
	CBitmap    bitmap;

	// Create and set the normal toolbar image list.
	bitmap.LoadBitmap(IDB_TOOLBAR_ENABLE_EE);
	//bitmap.LoadBitmap(IDB_TOOLBAR);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	// Create and set the disable toolbar image list.
	bitmap.LoadBitmap(IDB_TOOLBAR_DISABLE_EE);
	//bitmap.LoadBitmap(IDB_TOOLBAR);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETDISABLEDIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	// Create and set the hot toolbar image list.
	bitmap.LoadBitmap(IDB_TOOLBAR_HOT_EE);
	//bitmap.LoadBitmap(IDB_TOOLBAR_DISABLE);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETHOTIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	//tool bar 크기 조정
	m_wndToolBar.SetSizes(CSize(48+7,48+6), CSize(48, 48));

	// 열기버튼 옆에 드랍다운 툴버튼 추가
	m_wndToolBar.GetToolBarCtrl().SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);
	TBBUTTONINFO tbi;
	tbi.dwMask= TBIF_STYLE;
	tbi.cbSize= sizeof(TBBUTTONINFO);
	m_wndToolBar.GetToolBarCtrl().GetButtonInfo(ID_INFO_IMPORT, &tbi);
	tbi.fsStyle |= TBSTYLE_DROPDOWN;
	m_wndToolBar.GetToolBarCtrl().SetButtonInfo(ID_INFO_IMPORT, &tbi);

	// Toolbar 에 사이트와 로긴ID 표시
	//if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE){
	//	CRect rect;
	//	int nIndex = m_wndToolBar.GetToolBarCtrl().CommandToIndex(ID_TOOLBAR_TEXT);
	//	m_wndToolBar.SetButtonInfo(nIndex, ID_TOOLBAR_TEXT, TBBS_SEPARATOR, 340);
	//	m_wndToolBar.GetToolBarCtrl().GetItemRect(nIndex, &rect);

	//	rect.left += 5;

	//	m_LoginInfoDlg.Create(CLoginInfoDlg::IDD, &m_wndToolBar);
	//	m_LoginInfoDlg.MoveWindow(&rect);
	//	m_LoginInfoDlg.ShowWindow(SW_SHOW);
	//}else{
	//	int nIndex = m_wndToolBar.GetToolBarCtrl().CommandToIndex(ID_TOOLBAR_TEXT);
	//	m_wndToolBar.SetButtonInfo(nIndex, ID_TOOLBAR_TEXT, TBBS_SEPARATOR, 0);
	//}
}

void CMainFrame::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CMDIFrameWnd::OnWindowPosChanging(lpwndpos);

	if(MIN_WIDTH > lpwndpos->cx)
		lpwndpos->cx = MIN_WIDTH;
	if(MIN_HEIGHT > lpwndpos->cy)
		lpwndpos->cy = MIN_HEIGHT;
}

// CMainFrame message handlers
void CMainFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CMDIFrameWnd::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMinTrackSize.x = MIN_WIDTH;
	lpMMI->ptMinTrackSize.y = MIN_HEIGHT;
}

CWnd* CMainFrame::GetChildFrame(UINT nID)
{
	CMDIFrameWnd* pFrame;
	pFrame = DYNAMIC_DOWNCAST( CMDIFrameWnd, this );
	CWnd* pClient = CWnd::FromHandle( pFrame->m_hWndMDIClient );

	CRuntimeClass *pRtc;

	switch(nID)
	{
	case ID_LAYOUT_MANAGE:
		pRtc = RUNTIME_CLASS(CLayoutFrame);
		break;
	case ID_PACKAGE_MANAGE:
		pRtc = RUNTIME_CLASS(CPackageFrame);
		break;
	case ID_PLAYCONTENTS_SEARCH:
		pRtc = RUNTIME_CLASS(CPlayContentsSearchFrame);
		break;
	default:
		pRtc = NULL;
		break;
	}

	if(pRtc)
	{
		for (int i=0 ; i<255; i++)
		{
			int id = AFX_IDM_FIRST_MDICHILD + i;

			CWnd *pWnd = pClient->GetDlgItem(id);

			// 한번만 띄우기로 한 프레임이 검색된 결과이므로
			if(pWnd && pWnd->IsKindOf(pRtc)) return pWnd;
		}
	}

	// 한번 띄우기로한 프레임이 검색되지 않은 케이스이므로 NULL 리턴
	return NULL;
}

void CMainFrame::OnNewFrame(UINT nID)
{
	TraceLog(("OnNewFrame(%d)", nID));
	if(!m_document.m_bOpened)
	{
		return;
	}//if

	CWnd * pWnd = GetChildFrame(nID);

	if(pWnd)
	{
		pWnd->BringWindowToTop();
		pWnd->SetActiveWindow();
		if(pWnd->IsIconic())
			pWnd->ShowWindow(SW_RESTORE);
	}
	else
	{
		// 새창 띄우기 로직

		CUBCStudioApp *pApp = (CUBCStudioApp*)::AfxGetApp();
		CMultiDocTemplate *pTemplate;

		switch(nID)
		{
		case ID_LAYOUT_MANAGE:
			pTemplate = pApp->m_pLayoutTemplate;
			break;
		case ID_PACKAGE_MANAGE:
			pTemplate = pApp->m_pPackageTemplate;
			break;
		case ID_PLAYCONTENTS_SEARCH:
			pTemplate = pApp->m_pPlayContentsSearchTemplate;
			break;
		default:
			pTemplate = NULL;
			break;
		}

		if(pTemplate){
			TraceLog(("OnNewFrame(%d)", nID));
			pTemplate->OpenDocumentFile(NULL);
		}
	}
}

LRESULT CMainFrame::OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam)
{
	CWnd* wnd = GetChildFrame(ID_PACKAGE_MANAGE);

	if(wnd)
	{
		wnd->SendMessage(WM_TEMPLATE_INFO_CHANGED);
	}

	return 0;
}

LRESULT CMainFrame::OnFrameInfoChanged(WPARAM wParam, LPARAM lParam)
{
	CWnd* wnd = GetChildFrame(ID_PACKAGE_MANAGE);

	if(wnd)
	{
		wnd->SendMessage(WM_FRAME_INFO_CHANGED);
	}

	return 0;
}

LRESULT CMainFrame::OnTemplateListChanged(WPARAM wParam, LPARAM lParam)
{
	CWnd* wnd = GetChildFrame(ID_LAYOUT_MANAGE);

	if(wnd)
	{
		wnd->SendMessage(WM_TEMPLATE_LIST_CHANGED);
	}

	return 0;
}

LRESULT CMainFrame::OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam)
{
	CWnd* wnd = GetChildFrame(ID_PACKAGE_MANAGE);

	if(wnd)
	{
		wnd->SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED);
	}

	return 0;
}

LRESULT CMainFrame::OnContentsListChanged(WPARAM wParam, LPARAM lParam)
{
	CWnd* wnd = GetChildFrame(ID_PACKAGE_MANAGE);

	if(wnd && wParam != 0) 
	{
		wnd->SendMessage(WM_CONTENTS_LIST_CHANGED);
	}

	return 0;
}

LRESULT CMainFrame::OnAllInfoChanged(WPARAM wParam, LPARAM lParam)
{
	CWnd* pWnd = GetChildFrame(ID_PACKAGE_MANAGE);
	if(pWnd){
		pWnd->SendMessage(WM_FRAME_INFO_CHANGED);
		pWnd->SendMessage(WM_TEMPLATE_INFO_CHANGED);
		pWnd->SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED);
		pWnd->SendMessage(WM_CONTENTS_LIST_CHANGED);
	}

	return 0;
}

// 콘텐츠 패키지열기
void CMainFrame::OnInfoImport()
{
	CWaitMessageBox wait;

	//Scheduel이 열려있으며, 수정이 되어있는 상태라면
	//먼저 저장을 하도록 유도한다.
	TraceLog(("OnInfoImport()"));
	if(m_document.IsModify())
	{
		int nRet = UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG001), MB_YESNOCANCEL|MB_ICONWARNING);

		if(nRet == IDYES)
		{
			m_sStateSave = E_STATE_SAVE_OPEN;
			OnSave();
			return;
		}
		else if(nRet == IDCANCEL)
		{
			return;
		}//if
	}//if

#ifdef _UBCSTUDIO_EE_
	// User Login Process
	bool bNewPackage = false;
	CString szSite, szID, szPW, szPackage;

	szSite.Empty();
	szID.Empty();
	szPW.Empty();
	szPackage.Empty();

	if(CEnviroment::eAuthNoUser == GetEnvPtr()->m_Authority)
	{
		for(int i = 0; i < __argc; i++)
		{
			CString szTemp = *(__argv+i);

			if(szTemp.Find("+siteid") == 0){
				szSite = szTemp.Mid(strlen("+siteid="));
			}
			else if(szTemp.Find("+userid") == 0){
				szID = szTemp.Mid(strlen("+userid="));
			}
			else if(szTemp.Find("+passwd") == 0){
				szPW = szTemp.Mid(strlen("+passwd="));
			}
			else if(szTemp.Find("+package") == 0){
				szPackage = szTemp.Mid(strlen("+package="));
			}
			else if(szTemp.Find("+newpackage") == 0){
				bNewPackage = true;
			}
		}

		if(!szSite.IsEmpty() && !szID.IsEmpty() && !szPW.IsEmpty())
		{
			char szBuf[1000];
			CString szPath, szIP, szPort;
			szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

			::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", szBuf, sizeof(szBuf), szPath);
			szIP = szBuf;
			::GetPrivateProfileString("ORB_NAMESERVICE", "PORT", "", szBuf, sizeof(szBuf), szPath);
			szPort = szBuf;

			if(!InitORB(szIP, atoi(szPort))) return;

			// 로그인계정의 유효기간을 체크한다
			CString strValidationDate;
			GetEnvPtr()->m_Authority = copCheckLogin(szSite, szID, szPW,strValidationDate, GetEnvPtr()->m_strCustomer);
			//if( strValidationDate.Left(8) != _T("1970/01/01") && strValidationDate < CTime::GetCurrentTime().Format(STR_ENV_TIME) )
			//{
			//	CString strMsg;
			//	strMsg.Format(LoadStringById(IDS_LOGINDLG_MSG008), strValidationDate);
			//	UbcMessageBox(strMsg);
			//	return;
			//}
		}

		if(CEnviroment::eAuthNoUser == GetEnvPtr()->m_Authority)
		{
			// 0000554: 코바연결을 시도하는 시점을 로그인 버튼을 누른 시점으로 변경한다.
			//SOrbConnInfo* pInfo = COrbConn::GetObject()->GetCur();
			//if(!InitORB(pInfo->ip, pInfo->port))
			//{
			//	CString szBuf;
			//	szBuf.Format(LoadStringById(IDS_MAINFRAME_MSG028), pInfo->ip, pInfo->port);
			//	UbcMessageBox(szBuf);
			//}

			wait.Hide();

			CLoginDlg dlgLogin;
			if(dlgLogin.DoModal() != IDOK)
			{
				PostMessage(WM_CLOSE, 0, 0);
				return;
			}
		}
		else
		{
			GetEnvPtr()->m_szSite = szSite;
			GetEnvPtr()->m_szLoginID = szID;
			GetEnvPtr()->m_szPassword = szPW;
		}

		CWaitMessageBox wait;

		// 권한정보를 가져온다
		InitAuth(GetEnvPtr()->m_szLoginID, GetEnvPtr()->m_strCustomer);

		//POSITION pos = GetEnvPtr()->m_lsSite.Find(GetEnvPtr()->m_szSite);
		//if(pos == 0)
		//{
		//	GetEnvPtr()->m_lsSite.AddTail(GetEnvPtr()->m_szSite);
		//}

		//if(GetEnvPtr()->m_Authority == CEnviroment::eAuthSuper)
		//	InitSite("*", GetEnvPtr()->m_lsSite);
		//else
		//	InitSite(GetEnvPtr()->m_szSite, GetEnvPtr()->m_lsSite);		

		PrepareSiteList();

		if(szPackage.IsEmpty())
			return;
	}

	//if(IsWindow(m_sttToolBar.m_hWnd)){
	//	CString szTemp;
	//	szTemp.Format("%s : %s", GetEnvPtr()->m_szSite, GetEnvPtr()->m_szLoginID);
	//	m_sttToolBar.SetWindowText(szTemp);
	//}

	//m_LoginInfoDlg.m_szSite = GetEnvPtr()->m_szSite;
	//m_LoginInfoDlg.m_szUser = GetEnvPtr()->m_szLoginID;
	//m_LoginInfoDlg.UpdateData(FALSE);

	if(bNewPackage)
	{
		OnInfoNew();
	}
	else
	{
		MakeLocalHostInfoArray();

		int nCount = m_aryLocalHostInfo.GetCount();
		if(nCount == 0)
		{
			//Open 할 수 있는 package이 없다면 새로 만들기로 넘어간다.
			SetTimer(TIMER_NEW, 100, NULL);
			return;
		}//if

		// Host Selection Process
		CEEPackageDlg dlgPackage;
		dlgPackage.SetParameter(&m_document, szPackage);
		if(dlgPackage.DoModal() != IDOK) return;
		TraceLog(("dlgPackage.DoModal(IDOK)"));
	}
#else
	MakeLocalHostInfoArray();

	int nCount = m_aryLocalHostInfo.GetCount();
	if(nCount == 0)
	{
		//Open 할 수 있는 package이 없다면 새로 만들기로 넘어간다.
		SetTimer(TIMER_NEW, 100, NULL);
		return;
	}//if

	CHostSelectDlg dlgHost;
	dlgHost.SetHostInfoList(&m_aryLocalHostInfo);
	dlgHost.SetParentPtr(this, &m_document, false);
	if(dlgHost.DoModal() != IDOK) return;
#endif
	m_document.m_bNew = false;
	m_document.m_bOpened = true;

	TraceLog(("CloseAllDocuments()"));
	AfxGetApp()->CloseAllDocuments(FALSE);

	TraceLog(("OnNewFrame(ID_PACKAGE_MANAGE)"));
	OnNewFrame(ID_PACKAGE_MANAGE);

	TraceLog(("OnInfoImport(Complete)"));
}

//void CMainFrame::OnInfoExport()
//{
//
//	m_document.SaveContents();
//}

LRESULT CMainFrame::OnCompleteFileCopy(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case 0:	// cancel
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG003), MB_ICONWARNING);
		break;

	case 1:	// all ok
		{
			CStringArray* paryError = (CStringArray*)lParam;
			if(paryError != NULL && paryError->GetCount() != 0)
			{
				CString strResult = LoadStringById(IDS_MAINFRAME_MSG004);
				for(int i=0; i<paryError->GetCount(); i++)
				{
					strResult += paryError->GetAt(i);
					strResult += "\r\n";
				}//for
				delete paryError;
				strResult += "\r\n";
				strResult += LoadStringById(IDS_MAINFRAME_MSG005);
				if(MessageBox(strResult, LoadStringById(IDS_MAINFRAME_TTL001), MB_OKCANCEL|MB_ICONQUESTION) == IDCANCEL)
				{
					break;
				}//if
			}//if

			CWaitMessageBox wait;
			if(m_pNotiDlg)
			{
				delete m_pNotiDlg;
				m_pNotiDlg = NULL;
			}//if
			m_pNotiDlg = new CNotifySaveDlg;
			m_pNotiDlg->SetMode(CNotifySaveDlg::eSavePackage);
			m_pNotiDlg->Create(IDD_NOTIFY_SAVE_DLG, this);
			m_pNotiDlg->ShowWindow(SW_NORMAL);
			m_pNotiDlg->CenterWindow();
			this->EnableWindow(FALSE);

			m_pThreadConfigSave = (CConfigSaveThread*)AfxBeginThread(
										RUNTIME_CLASS(CConfigSaveThread),
										THREAD_PRIORITY_NORMAL,
										0,  CREATE_SUSPENDED, NULL);
			if(m_pThreadConfigSave)
			{
				m_pThreadConfigSave->m_bAutoDelete = TRUE;
				m_pThreadConfigSave->SetThreadParam(this, &m_document);
				m_pThreadConfigSave->ResumeThread();
			}
			else
			{
				UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG006), MB_ICONSTOP);
			}//if	
		}
		break;

	case 2:	// fail to make directory
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG007), MB_ICONSTOP);
		break;

	case 3:	// file exception
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG008), MB_ICONSTOP);
		break;

	case 4:	// source open error
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG009), MB_ICONSTOP);
		break;

	case 5:	// target open error
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG010), MB_ICONSTOP);
		break;
	case 6:	// Disk full
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG029), MB_ICONSTOP);
		break;
	}//switch
/*
	if(wParam != 1)
	{
		int ret = m_document.DeleteExportFolder();
	}//if
*/
	return 0;
}


void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent ==TIMER_EXIT)
	{
		KillTimer(TIMER_EXIT);
		this->PostMessage(WM_CLOSE, 0, 0);
	}
	else if(nIDEvent == TIMER_INIT)
	{
		//TraceLog(("OnTimer(TIMER_INIT)"));
		KillTimer(TIMER_INIT);

		if(CEnviroment::eStudioEE == GetEnvPtr()->m_Edition){
			OnInfoImport();
		}
	}
	else if(nIDEvent == TIMER_NEW)
	{
		KillTimer(TIMER_NEW);
		OnInfoNew();
	}
	// 0001128: Studio 임시 저장 및 복구
	else if(nIDEvent == TIMER_AUTO_SAVE)
	{
		m_document.SaveToTemporaryFile();
	}

	CMDIFrameWnd::OnTimer(nIDEvent);
}

BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	switch(pCopyDataStruct->dwData)
	{
	case eCDSetPackage:
		return LoadPackage((LPCTSTR)pCopyDataStruct->lpData, pCopyDataStruct->cbData);
	case eCDNewPackage:
		OnInfoNew();
		return TRUE;
	}

	return CMDIFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

BOOL CMainFrame::LoadPackage(LPCTSTR szPackage, int nLen, CString strDrive)
{
	if(szPackage == NULL) return FALSE;

	CString szBuf = GetEnvPtr()->m_PackageInfo.szPackage;

	// 0000756: 같은 스케줄이 두개의 다른 드라이브에 각각 저장되어 있을때, c드라이브의 스케줄을 열어둔 상태에서 d드라이브의 스케줄이 안열립니다.
	if(szBuf == szPackage && strDrive == GetEnvPtr()->m_PackageInfo.szDrive)
		return TRUE;

#ifdef _UBCSTUDIO_EE_
	// Host Selection Process
	CEEPackageDlg dlgPackage;
	dlgPackage.SetParameter(&m_document, szPackage, strDrive);
	if(dlgPackage.DoModal() != IDOK) return FALSE;
#else
	CHostSelectDlg dlgHost;
	dlgHost.SetHostInfoList(&m_aryLocalHostInfo);
	dlgHost.SetParentPtr(this, &m_document, false, strDrive, szPackage);
	if(dlgHost.DoModal() != IDOK) return FALSE;
#endif

	m_document.m_bNew = false;
	m_document.m_bOpened = true;

	AfxGetApp()->CloseAllDocuments(FALSE);
	OnNewFrame(ID_PACKAGE_MANAGE);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// complete preview message handler\n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CMainFrame::OnPreviewComplete(WPARAM wParam, LPARAM lParam)
{
	this->EnableWindow(TRUE);
	if(m_pNotiDlg)
	{
		m_pNotiDlg->ShowWindow(SW_HIDE);
		delete m_pNotiDlg;
		m_pNotiDlg = NULL;
	}//if
//	this->EnableWindow(TRUE);
	

	if(wParam != 1)
	{
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG011), MB_ICONERROR);
	}//if

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// complete config save message handler\n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CMainFrame::OnConfigSaveComplete(WPARAM wParam, LPARAM lParam)
{
	this->EnableWindow(TRUE);
	if(m_pNotiDlg)
	{
		m_pNotiDlg->ShowWindow(SW_HIDE);
		delete m_pNotiDlg;
		m_pNotiDlg = NULL;
	}//if

//	this->EnableWindow(TRUE);
	

	if(wParam != 1)
	{
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG006), MB_ICONSTOP);
	}
	else
	{			
		m_document.m_bNew = false;

		//열려있는 각 view에 host명을 바꾸도록 한다
		CWnd* wnd = GetChildFrame(ID_PACKAGE_MANAGE);
		if(wnd)
		{
			wnd->SendMessage(WM_SAVE_CONFIG_COMPELETE);
		}//if

		wnd = GetChildFrame(ID_LAYOUT_MANAGE);
		if(wnd)
		{
			wnd->SendMessage(WM_SAVE_CONFIG_COMPELETE);
		}//if

		switch(m_sStateSave )
		{
		case E_STATE_SAVE_NONE:
			{
				m_sStateSave = E_STATE_SAVE_NONE;
				UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG012), MB_ICONINFORMATION);
			}
			break;
		case E_STATE_SAVE_NEW:
			{
				m_sStateSave = E_STATE_SAVE_NONE;
				OnInfoNew();
			}
			break;
		case E_STATE_SAVE_OPEN:
			{
				m_sStateSave = E_STATE_SAVE_NONE;
				OnInfoImport();
			}
			break;
		case E_STATE_SAVE_CLOSE:
			{
				m_sStateSave = E_STATE_SAVE_NONE;
				OnPackageClose();
			}
			break;
		case E_STATE_SAVE_EXIT:
			{
				m_sStateSave = E_STATE_SAVE_NONE;
				CMDIFrameWnd::OnClose();
			}
		default:
			{
				m_sStateSave = E_STATE_SAVE_NONE;
				UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG012), MB_ICONINFORMATION);
			}
		}//switch
	}//if

	SendMessage(WM_ALL_INFO_CHANGED);

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Run preview browser \n
/// @param (CString) strTemplateID : (in) Preview 하려는 template id
/////////////////////////////////////////////////////////////////////////////////
void CMainFrame::RunPreview(CString strTemplateID)
{
	CWaitMessageBox wait;
	if(m_pNotiDlg)
	{
		delete m_pNotiDlg;
		m_pNotiDlg = NULL;
	}//if
	m_pNotiDlg = new CNotifySaveDlg;
	m_pNotiDlg->SetMode(CNotifySaveDlg::ePreview);
	m_pNotiDlg->Create(IDD_NOTIFY_SAVE_DLG, this);
	m_pNotiDlg->ShowWindow(SW_NORMAL);
	m_pNotiDlg->CenterWindow();
	this->EnableWindow(FALSE);

	m_pthreadPreview = (CRunPreviewThread*)AfxBeginThread(
								RUNTIME_CLASS(CRunPreviewThread),
								THREAD_PRIORITY_NORMAL,
								0,  CREATE_SUSPENDED, NULL);
	if(m_pthreadPreview)
	{
		m_pthreadPreview->m_bAutoDelete = TRUE;
		m_pthreadPreview->SetThreadParam(this, &m_document, strTemplateID, true);
		m_pthreadPreview->ResumeThread();
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG013), MB_ICONERROR);
	}//if	
}

void CMainFrame::OnSave()
{
	if(!m_document.m_bOpened) return;

	if(!m_document.InputErrorCheck()) return;

	//새로 만든 package라면 save as만 가능...
	if(m_document.m_bNew)
	{
		OnSaveAs();
		return;
	}//if

	// 0001121: 컨텐츠 패키지 저장시 SITEid 가 일치하지 않으면 다른이름으로 저장
	if(GetEnvPtr()->m_PackageInfo.bIsPublic && GetEnvPtr()->m_PackageInfo.szSiteID != GetEnvPtr()->m_szSite)
	{
		OnSaveAs();
		return;
	}

	CWaitMessageBox wait;
	if(m_pNotiDlg){
		delete m_pNotiDlg;
		m_pNotiDlg = NULL;
	}

	m_pNotiDlg = new CNotifySaveDlg;
	m_pNotiDlg->SetMode(CNotifySaveDlg::eSavePackage);
	m_pNotiDlg->Create(IDD_NOTIFY_SAVE_DLG, this);
	m_pNotiDlg->ShowWindow(SW_NORMAL);
	m_pNotiDlg->CenterWindow();
	this->EnableWindow(FALSE);

	GetEnvPtr()->m_PackageInfo.bModify = true;

	m_pThreadConfigSave = (CConfigSaveThread*)AfxBeginThread(
										RUNTIME_CLASS(CConfigSaveThread),
										THREAD_PRIORITY_NORMAL,
										0,  CREATE_SUSPENDED, NULL);
	if(m_pThreadConfigSave)
	{
		m_document.SetTargetDirive(m_document.GetSourceDirive());
		m_pThreadConfigSave->m_bAutoDelete = TRUE;
		m_pThreadConfigSave->SetThreadParam(this, &m_document);
		m_pThreadConfigSave->ResumeThread();
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG006), MB_ICONSTOP);
	}//if	
}

void CMainFrame::OnFileRecovery()
{
	m_bRecovery = false;
	if(UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG014),MB_YESNO|MB_ICONWARNING)!=IDYES){
		return;
	}

	SPackageInfo4Studio* pInfo = GetEnvPtr()->FindPackage(GetEnvPtr()->m_strPackage, STR_ENV_SERVER);
	if(!pInfo)
	{
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG015), MB_ICONWARNING);
		return;
	}

	CString szFile, szLocal, szRemote;
	szFile.Format("%s.ini", pInfo->szPackage);
	// 2010.09.13 현재 로드한 콘텐츠 패키지를 복원해야 하므로 GetEnvPtr()->m_szDrive 대신 m_document.GetSourceDirive() 로 수정함.
	szLocal.Format("%s%s"
				, m_document.GetSourceDirive() //GetEnvPtr()->m_szDrive
				, UBC_CONFIG_PATH);
	szRemote.Format("/config/");

	if(!GetEnvPtr()->GetFile(szFile, szRemote, szLocal, pInfo->szSiteID)){
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG016), MB_ICONWARNING);
		return;
	}

	// 2010.09.13 현재 로드한 콘텐츠 패키지를 복원해야 하므로 GetEnvPtr()->m_szDrive 대신 m_document.GetSourceDirive() 로 수정함.
	//szLocal.Format("%s\\", GetEnvPtr()->m_szDrive);
	szLocal = m_document.GetSourceDirive();
	pInfo = GetEnvPtr()->FindPackage(GetEnvPtr()->m_strPackage, szLocal);
	if(!pInfo)
	{
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG031), MB_ICONWARNING);
		return;
	}

	m_bRecovery = true;
	GetEnvPtr()->m_PackageInfo.bModify = true;

	CInfoImportThread* pThread = (CInfoImportThread*)AfxBeginThread(
								RUNTIME_CLASS(CInfoImportThread),
								THREAD_PRIORITY_NORMAL,
								0,  CREATE_SUSPENDED, NULL);
	if(pThread){
//		pThread->m_bInfoMsg = false;
		pThread->m_bAutoDelete = TRUE;
		pThread->SetThreadParam(this, &m_document, pInfo->szDrive, pInfo->szPackage);
		pThread->ResumeThread();

		m_pNotiDlg = new CNotifySaveDlg;
		m_pNotiDlg->SetMode(CNotifySaveDlg::eSavePackage);
		m_pNotiDlg->Create(IDD_NOTIFY_SAVE_DLG, this);
		m_pNotiDlg->ShowWindow(SW_NORMAL);
		m_pNotiDlg->CenterWindow();
	}
}

void CMainFrame::OnFileReservation()
{
	if(m_document.IsModify())
	{
		int nRet = UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG001), MB_YESNOCANCEL | MB_ICONWARNING);
		
		if(nRet == IDYES)
		{
			m_sStateSave = E_STATE_SAVE_NONE;
			OnSave();
			return;
		}
		else if(nRet == IDCANCEL) return;
	}

	bool bCanAdd = true;
	if(m_document.m_bOpened && !GetHostName().IsEmpty())
	{
		CString strMsg;
		bCanAdd = CDataContainer::getInstance()->CheckPrimaryFrame(false, strMsg);
		if(!bCanAdd)
		{
			UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG030) + CString(_T("\n\n")) + strMsg);
		}
	}

#ifdef _UBCSTUDIO_EE_
	CString strIniPath;
	strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	int nRet = 0;
	bool bOldModify = GetEnvPtr()->m_PackageInfo.bModify;
	if(CEnviroment::eAuthSuper == GetEnvPtr()->m_Authority){
		nRet = RunPackageRevDlg(
			eSAStudio, 
			"*",
			"",
			GetEnvPtr()->m_strPackage, 
			GetEnvPtr()->m_PackageInfo.szSiteID,
			GetEnvPtr()->m_Authority,
			GetEnvPtr()->m_szLoginID,
			GetEnvPtr()->m_szSite,
			strIniPath,
			GetEnvPtr()->m_PackageInfo.bModify,
			GetEnvPtr()->m_PackageInfo.szDrive,
			GetEnvPtr()->m_strCustomer,
			bCanAdd);
	}else{
		nRet = RunPackageRevDlg(
			eSAStudio, 
			GetEnvPtr()->m_szSite, 
			"",
			GetEnvPtr()->m_strPackage, 
			GetEnvPtr()->m_PackageInfo.szSiteID,
			GetEnvPtr()->m_Authority,
			GetEnvPtr()->m_szLoginID,
			GetEnvPtr()->m_szSite,
			strIniPath,
			GetEnvPtr()->m_PackageInfo.bModify,
			GetEnvPtr()->m_PackageInfo.szDrive,
			GetEnvPtr()->m_strCustomer,
			bCanAdd );
	}

	if(nRet == eSRSaveAs){
		OnSaveAs();
		return;
	}

	if(bOldModify && !GetEnvPtr()->m_PackageInfo.bModify)
	{
		GetEnvPtr()->RefreshPackageList(true);
	}
#endif
}

void CMainFrame::OnUnusedContentsList()
{
	CUnusedContentDlg dlg;
	dlg.DoModal();
}

LRESULT CMainFrame::OnImportComplete(WPARAM wParam, LPARAM lParam)
{
	if(m_pNotiDlg){
		m_pNotiDlg->ShowWindow(SW_HIDE);
		delete m_pNotiDlg;
		m_pNotiDlg = NULL;
	}
	
	if(m_bRecovery){
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG017), MB_ICONINFORMATION);
	}else{
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG018), MB_ICONINFORMATION);
	}
	
	m_bRecovery = false;

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 호스트의 이름을 반환한다. \n
/// @return <형: CString> \n
///			<설정된 호스트의 이름> \n
/////////////////////////////////////////////////////////////////////////////////
CString CMainFrame::GetHostName(void)
{
	return m_document.GetHostName();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 드라이브 문자열을 반환한다. \n
/// @return <형: CString> \n
///			<설정된 드라이브 문자열> \n
/////////////////////////////////////////////////////////////////////////////////
CString	CMainFrame::GetSourceDirive(void)
{
	return m_document.GetSourceDirive();
}

void CMainFrame::OnInfoNew()
{
	//Scheduel이 열려있으며, 수정이 되어있는 상태라면
	//먼저 저장을 하도록 유도한다.
	if(m_document.IsModify())
	{
		int nRet = UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG001), MB_YESNOCANCEL | MB_ICONWARNING);
		if(nRet == IDYES)
		{
			m_sStateSave = E_STATE_SAVE_NEW;
			OnSave();
			return;
		}
		else if(nRet == IDCANCEL)
		{
			return;
		}//if
	}//if

	CString strDrive, strPath;
	strDrive = GetEnvPtr()->m_szDrive;
	strDrive += _T("\\");

	strPath = strDrive;
	strPath.Append(SAMPLE_CONFIG_PATH);

	ClearLocalHostInfoArray();

	m_document.GetHostInfo(strPath, strDrive, m_aryLocalHostInfo);
	
	if(m_aryLocalHostInfo.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG019), MB_ICONWARNING);
		return;
	}//if

//#ifdef _UBCSTUDIO_EE_
	// Host Selection Process
//	CEEPackageDlg dlgPackage;
//	if(dlgPackage.DoModal() != IDOK){
//		return;
//	}

//#else
	CHostSelectDlg dlgHost;
	dlgHost.SetHostInfoList(&m_aryLocalHostInfo);
	dlgHost.SetParentPtr(this, &m_document, true);
	if(dlgHost.DoModal() != IDOK){
		return;
	}
//#endif
	m_document.m_bNew = true;
	m_document.m_bOpened = true;
	AfxGetApp()->CloseAllDocuments(FALSE);
	OnNewFrame(ID_PACKAGE_MANAGE);
}

void CMainFrame::OnSaveAs()
{
	if(!m_document.m_bOpened) return;

	if(!m_document.InputErrorCheck()) return;

	if(m_document.SaveContents())
	{
		m_document.m_bNew = false;
	}
}

// Modified by 정운형 2009-01-28 오후 1:43
// 변경내역 :  이미지 추가 작업
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// locla host info 배열을 정리한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CMainFrame::ClearLocalHostInfoArray(void)
{
	if(m_aryLocalHostInfo.GetCount())
	{
		CLocalHostInfo* pclsHost = NULL;
		for(int i=0; i<m_aryLocalHostInfo.GetCount(); i++)
		{
			pclsHost = (CLocalHostInfo*)m_aryLocalHostInfo.GetAt(i);
			if(pclsHost)
			{
				delete pclsHost;
			}//if
		}//for
		m_aryLocalHostInfo.RemoveAll();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// locla host info 배열을 만든다 \n
/////////////////////////////////////////////////////////////////////////////////
void CMainFrame::MakeLocalHostInfoArray()
{
	//drive 정보 가져오기
	InitDriveLabel();
	ClearLocalHostInfoArray();

	POSITION pos = m_mapDriveToLabel.GetStartPosition();
	if(pos != NULL)
	{
		CString strDrive, strLabel;
		UINT unType;
		while(pos != NULL)
		{
			m_mapDriveToLabel.GetNextAssoc(pos, strDrive, strLabel);
			unType = GetDriveType(strDrive);
			if(strLabel == "")
			{
				switch(unType)
				{
				case DRIVE_REMOVABLE:
					strLabel = LoadStringById(IDS_MAINFRAME_STR001);
					break;
				case DRIVE_FIXED:
					strLabel = LoadStringById(IDS_MAINFRAME_STR002);
					break;
				case DRIVE_REMOTE:
					strLabel = LoadStringById(IDS_MAINFRAME_STR003);
					break;
				case DRIVE_CDROM:
					strLabel = LoadStringById(IDS_MAINFRAME_STR004);
					break;
				case DRIVE_RAMDISK:
					strLabel = LoadStringById(IDS_MAINFRAME_STR005);
					break;
				default:
					strLabel = LoadStringById(IDS_MAINFRAME_STR006);
					break;
				}//switch
			}//if

			CString strPath = strDrive;
			strPath.Append(UBC_CONFIG_PATH);

			m_document.GetHostInfo(strPath, strDrive, m_aryLocalHostInfo);
		}//while
	}//if
}


void CMainFrame::InitDriveLabel()
{
	m_mapVolumeToLabel.RemoveAll();
	m_mapDriveToLabel.RemoveAll();

	//
	GetProcessVolume();

	//
	CStringArray	drive_list;
	int nPos =0;
	char drive[] = "?:\\";
	DWORD dwDriveList=::GetLogicalDrives();
	while(dwDriveList)
	{
		if(dwDriveList & 1)
		{
			drive[0] = 'A' + nPos;
			drive_list.Add(drive);
		}
		dwDriveList >>=1;
		nPos++;
	}

	//
	char volume[255];
	for(int i=0; i<drive_list.GetCount(); i++)
	{
		CString drive = drive_list.GetAt(i);

		BOOL ret = GetVolumeNameForVolumeMountPoint(drive, volume, 255);

		if(ret)
		{
			CString label;
			if(m_mapVolumeToLabel.Lookup(volume, label))
				m_mapDriveToLabel.SetAt(drive, label);
		}
	}
}

void  CMainFrame::GetProcessVolume()
{
	char buf[MAX_PATH];           // buffer for unique volume identifiers
	HANDLE hVol;                  // handle for the volume scan
	BOOL bFlag;                   // generic results flag

	// Open a scan for volumes.
	hVol = FindFirstVolume (buf, MAX_PATH );

	if (hVol == INVALID_HANDLE_VALUE)
		return;

	// We have a volume; process it.
	bFlag = _GetProcessVolume (hVol, buf, MAX_PATH);

	// Do while we have volumes to process.
	while (bFlag) 
	{
		bFlag = _GetProcessVolume (hVol, buf, MAX_PATH);
	}

	// Close out the volume scan.
	bFlag = FindVolumeClose( hVol );
}

BOOL CMainFrame::_GetProcessVolume(HANDLE hVol, char* lpBuf, int iBufSize)
{
	DWORD dwSysFlags;            // flags that describe the file system
	char FileSysNameBuf[MAX_PATH];
	char labelname[MAX_PATH];

	BOOL ret_val = GetVolumeInformation( lpBuf, labelname, MAX_PATH, NULL, NULL, &dwSysFlags, FileSysNameBuf, MAX_PATH);

	if(ret_val)
	{
		m_mapVolumeToLabel.SetAt(lpBuf, labelname);
	}

	return FindNextVolume( hVol, lpBuf, iBufSize );
}
// Modified by 정운형 2009-01-28 오후 1:43
// 변경내역 :  이미지 추가 작업


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 정식등록되었는지 사용기간 확인 \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CMainFrame::CheckRegist(void)
{
	CString strMsg;
	CString strRegPath = GetEnvPtr()->m_szDrive;
	strRegPath.Append("\\");
	strRegPath.Append(UBC_EXECUTE_PATH);
	strRegPath.Append(FILE_NAME_REG);

	if(!::PathFileExists(strRegPath))
	{
		strMsg.Format(LoadStringById(IDS_MAINFRAME_MSG020)); 
		UbcMessageBox(strMsg, MB_ICONERROR);
		return false;
	}//if

	ST_REG_INFO stReg;
	memset(&stReg, 0x00, sizeof(ST_REG_INFO));
	FILE* pFile;
	pFile = fopen(strRegPath.GetBuffer(strRegPath.GetLength()), "r+");
	if(pFile == NULL)
	{
		strMsg.Format(LoadStringById(IDS_MAINFRAME_MSG020)); 
		UbcMessageBox(strMsg, MB_ICONERROR);
		return false;
	}//if

	UINT unRead = fread(&stReg, sizeof(char), sizeof(ST_REG_INFO), pFile);
/*
	if(unRead < sizeof(ST_REG_INFO))
	{
		fclose(pFile);
		strMsg.Format("This program is a illegal copy program !!!\r\n\r\nRe-Install UBC system package or Contact to SQI Soft(C)."); 
		UbcMessageBox(strMsg, MB_ICONERROR);
		return false;
	}//if
*/

	if(stReg.bFirstUse)
	{
		//처음 사용이면 현재시간을 begin, 30일 후를 end에 설정하고, FirstUse 값을 false로 설정
		CTime tmNow = CTime::GetCurrentTime();
		//CTime tmEnd = tmNow;
		//tmEnd += CTimeSpan(30, 0, 0, 0);

		tmNow.GetAsSystemTime(stReg.tmBegin);
		//tmEnd.GetAsSystemTime(stReg.tmEnd);

		stReg.bFirstUse = false;
		stReg.bRegist = false;
		//memset(&stReg.szRegKey, 0x00, 64);

		fseek(pFile, 0, SEEK_SET);
		int nWrite = fwrite(&stReg, sizeof(char), sizeof(ST_REG_INFO), pFile); 
		fclose(pFile);

		//////////////////////////////////////////////////////
		/*memset(&stReg, 0x00, sizeof(ST_REG_INFO));
		pFile = fopen(strRegPath.GetBuffer(strRegPath.GetLength()), "r+");
		UINT unRead = fread(&stReg, sizeof(char), sizeof(ST_REG_INFO), pFile);
		fclose(pFile);*/
		return true;
	}//if

	//처음 사용자가 아니라면, 등록이 되었는지를 확인한다
	if(stReg.bRegist)
	{
		/*
		if(strcmp(stReg.szRegKey, KEY_MAGIC_NUMBER) == 0)
		{
			fclose(pFile);
			return true;
		}
		else
		{
			fclose(pFile);
			strMsg.Format("Registration KEY incorrect !!!\r\n\r\nPlease, Contact to SQI Soft(C)."); 
			UbcMessageBox(strMsg, MB_ICONERROR);
			return false;
		}//if
		*/
		fclose(pFile);
		return true;
	}//if

	//등록이 안된 사용자라면, 사용 기간을 확인한다
	//CTime tmEnd(stReg.tmEnd);
	CTime tmBegin(stReg.tmBegin);
	tmBegin += CTimeSpan(30, 0, 0, 0);
	CTime tmNow = CTime::GetCurrentTime();
	if(tmNow > tmBegin)
	{
		fclose(pFile);
		strMsg.Format(LoadStringById(IDS_MAINFRAME_MSG021)); 
		int nRet = MessageBox(strMsg, LoadStringById(IDS_MAINFRAME_TTL002), MB_OKCANCEL|MB_ICONQUESTION);
		if(nRet == IDOK)
		{
			CRegistDlg dlg;
			if(dlg.DoModal() == IDOK)
			{
				WriteRegistFile(dlg.m_strRegistKey);
				return true;
			}//if
		}//if
		
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG022), MB_ICONINFORMATION);
		return false;
	}//if
	
	fclose(pFile);
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Regist 파일 쓰기 \n
/// @param (CString) strKey : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CMainFrame::WriteRegistFile(CString strKey)
{
	CString strMsg;
	CString strRegPath = GetEnvPtr()->m_szDrive;
	strRegPath.Append("\\");
	strRegPath.Append(UBC_EXECUTE_PATH);
	strRegPath.Append(FILE_NAME_REG);

	FILE *pFile;
	pFile = fopen(strRegPath.GetBuffer(strRegPath.GetLength()), "w");

	ST_REG_INFO stReg;
	memset(&stReg, 0x00, sizeof(ST_REG_INFO));
	strcpy(stReg.szCopyRight, LoadStringById(IDS_MAINFRAME_STR007));
	stReg.bFirstUse = false;
	stReg.bRegist = true;
	//strcpy(stReg.szRegKey, strKey);

	fwrite(&stReg, sizeof(char), sizeof(ST_REG_INFO), pFile);
	fclose(pFile);
}
/*
void CMainFrame::OnRegistKey()
{
	CRegistDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
		WriteRegistFile(dlg.m_strRegistKey);
	}//if
}
*/
void CMainFrame::OnNetworkExport()
{
	CString strMsg;

	if(!m_document.m_bOpened || GetHostName() == ""){
		strMsg.Format(LoadStringById(IDS_MAINFRAME_MSG023), GetHostName());
		UbcMessageBox(strMsg, MB_ICONWARNING);
		return;
	}

	if(m_document.IsModify())
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_MAINFRAME_MSG024), GetHostName());
		UbcMessageBox(strMsg, MB_ICONWARNING);
		return;
	}

	if(!CDataContainer::getInstance()->CheckPrimaryFrame()) return;

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioPE)
		ExportForPE();
	else if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
		ExportForEE();
}

void CMainFrame::ExportForPE()
{
	CString strMsg;

	if(!GetEnvPtr()->m_bLicense){
		// License 가 없다면 Nework export 로 작업한다. - 원격 모드로 인식
		//Nework Export
		CNetworkHostDlg dlg;

		if(dlg.DoModal() == IDOK){
			if(m_pdlgFtp){
				delete m_pdlgFtp;
				m_pdlgFtp = NULL;
			}

			CExportDlg dlgExp;
			dlgExp.SetMonitorCnt(2);
			dlgExp.SetNetworkMode();
			dlgExp.SetComment(LoadStringById(IDS_MAINFRAME_STR008));

			for(int i = 0; i < dlg.m_arySelectedNetInfo.GetCount(); i++){
				CNetDeviceInfo* pclsNetInfo = (CNetDeviceInfo*)dlg.m_arySelectedNetInfo.GetAt(i);
				
				if(pclsNetInfo->m_nMonitorCnt >= 2){
					if(dlgExp.DoModal() != IDOK){
						dlg.ClearNetDeviceArray();
						return;
					}
					break;
				}
			}

			int nA, nB;
			dlgExp.GetMonitor(nA, nB);

			CString szArg = " +ini " + m_document.GetHostName();
			szArg += " +nomsg +display ";
			//if(nA){
			//	szArg += " 0";
			//	if(nB) szArg += "1";
			//}
			//else if(nB){
			//	szArg += " 1";
			//}

			CFtpUploadDlg ftpdlg(this, dlg.m_arySelectedNetInfo, szArg, nA, nB, dlg.m_bAll);
			ftpdlg.DoModal();

			strMsg = ftpdlg.GetErrorMsg();
			if(!strMsg.IsEmpty()){
				//CFileTranInfo dlgInfo;
				//dlgInfo.m_szInfo = strMsg;
				//dlgInfo.DoModal();
		
				CFtpReportDlg* pDlg = new CFtpReportDlg;
				pDlg->Create(CFtpReportDlg::IDD);

				int nPos = 0;
				CString szToken = strMsg.Tokenize("\n", nPos);
				while(!szToken.IsEmpty()){
					szToken += "\n";
					if(szToken.Find(">> ") >= 0)
						pDlg->WriteEditHighLightBox(szToken.GetBuffer(0), RGB(0,0,0), RGB(255,255,0));
					else
						pDlg->WriteEditBox(szToken.GetBuffer(0));

					szToken = strMsg.Tokenize("\n", nPos);
				}

				pDlg->ShowWindow(SW_SHOW);
			}
		}
		dlg.ClearNetDeviceArray();
	}else{
		// License 가 존재하면 무조건 self update 한다. - Local 모드로 인식
		// Local Export
		CExportDlg dlg;
		dlg.SetMonitorCnt(scratchUtil::getInstance()->getMonitorCount());

		if(dlg.DoModal() == IDOK){
			CString szCommand = _T("UBCImporter.exe");
			TerminateProcess(szCommand);

			int nA, nB;
			dlg.GetMonitor(nA, nB);
			
			CString szArg = " +self +drive " + m_document.GetSourceDirive() + " +ini " + m_document.GetHostName();
			szArg += " +display ";
			if(nA){
				szArg += " 0";
				if(nB) szArg += "1";
			}
			else if(nB){
				szArg += " 1";
			}

			szCommand = GetEnvPtr()->m_szDrive;
			szCommand += _T("\\");
			szCommand += UBC_EXECUTE_PATH;
			szCommand += _T("UBCImporter.exe");
			if(scratchUtil::getInstance()->createProcess(szCommand.GetBuffer(0), szArg.GetBuffer(0), 0, FALSE)){
				// TRACE("%s %s succeed", szCommand.GetBuffer(0), szArg.GetBuffer(0));
			}
		}
	}
}

void CMainFrame::ExportForEE()
{
	// 서버에 컨텐츠패키지 전송을 시작합니다.\n(컨텐츠 양에 따라 시간이 오래걸릴 수 있습니다.)
	CWaitMessageBox wait(LoadStringById(IDS_MAINFRAME_MSG033));

	CString szSite, szTemp;

	// Register 안된 콘텐츠 패키지라면 이름을 체크한다.
	// 이름은 Site_Schedu.ini 규칙을 따라야 한다.
	POSITION pos;
	if(GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
	{
		//int nRet = -1;
		//pos = GetEnvPtr()->m_lsSite.GetHeadPosition();

		//while(pos)
		//{
		//	szSite = GetEnvPtr()->m_lsSite.GetNext(pos);
		//	szSite += "_";

		//	nRet = GetEnvPtr()->m_strPackage.Find(szSite);
		//	if(nRet >= 0) break;
		//}

		//// Site List 에 없다면 Ini 에 있는 Site 를 비교한다.
		//if(nRet < 0 && !GetEnvPtr()->m_PackageInfo.szSiteID.IsEmpty())
		//{
		//	szSite = GetEnvPtr()->m_PackageInfo.szSiteID;
		//	nRet = GetEnvPtr()->m_strPackage.Find(szSite);
		//}

		CString strSiteName = GetSiteName(GetEnvPtr()->m_PackageInfo.szSiteID);

		// Site 가 이름에 없다면 Save As 를 유도한다.
		if(strSiteName.IsEmpty())
		{
			UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG025), MB_ICONINFORMATION);
			OnSaveAs();
			return;
		}
	}

	// 네트워크 사용으로 수정함
	CDataContainer::getInstance()->m_strNetworkUse = "1";

	szTemp.Format("%s%s%s.ini", 
					GetEnvPtr()->m_PackageInfo.szDrive, 
					UBC_CONFIG_PATH, 
					GetEnvPtr()->m_PackageInfo.szPackage);
	CDataContainer::getInstance()->Save(szTemp);

	CExportEEDlg dlgExp;
	if(dlgExp.DoModal() != IDOK) return;

	if(dlgExp.m_bSaveAs)
	{
		OnSaveAs();
		return;
	}

//	CFtpUploadDlg dlgFtp(true, !dlgExp.m_szStartDT.IsEmpty());
	std::multimap<std::string,void*> mapSite;

	pos = dlgExp.m_SelectInfo.GetHeadPosition();
	while(pos)
	{
		SHostInfo4Studio* pHost = dlgExp.m_SelectInfo.GetNext(pos);
		if(!pHost)	continue;

		std::string siteId = pHost->siteId;
		mapSite.insert(std::pair<std::string,void*>(siteId,(void*)pHost));

//		SFtpInfo ftpInfo;
//		if(!GetEnvPtr()->GetFtpInfo(pHost->siteId, &ftpInfo))
//			continue;

//		CNetDeviceInfo* pNetDvc = new CNetDeviceInfo();
//		pNetDvc->m_nFTPPort			= ftpInfo.nPort;
//		pNetDvc->m_nSvrPort			= 0;
//		pNetDvc->m_strDeviceName	= pHost->siteId;
//		pNetDvc->m_strID			= ftpInfo.szID;
//		pNetDvc->m_strIP			= ftpInfo.szIP;
//		pNetDvc->m_strPWD			= ftpInfo.szPW;

//		dlgFtp.m_aryHostInfo.Add(pNetDvc);
	}

//	dlgFtp.DoModal();
//	dlgFtp.ClearNetDeviceArray();

	CString szPackage;
	szPackage.Format("%s%s%s.ini", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONFIG_PATH, GetEnvPtr()->m_PackageInfo.szPackage);

#ifdef _UBCSTUDIO_EE_
	CFtpMultiSite dlgMultiFtp(GetEnvPtr()->m_strHttpOnly.CompareNoCase("CLIENT") == 0);
	//std::string sBuf;
	//for(std::multimap<std::string,void*>::iterator Iter = mapSite.begin(); Iter != mapSite.end(); Iter++){
	//	std::string sSite = Iter->first;
	//	if(sBuf == sSite)
	//		continue;

	//	sBuf = sSite;
	//	dlgMultiFtp.AddSite(sSite.c_str(), szPackage);
	//}


	if(IsSame(GetEnvPtr()->m_PackageInfo.szSiteID,"")){
		szTemp.Format(LoadStringById(IDS_MAINFRAME_MSG026), GetEnvPtr()->m_PackageInfo.szPackage); 
		UbcMessageBox(szTemp, MB_ICONERROR);
		return;
	}

	if(!dlgMultiFtp.AddSite(GetEnvPtr()->m_PackageInfo.szSiteID, szPackage))
	{
		szTemp.Format(LoadStringById(IDS_MAINFRAME_MSG027), GetEnvPtr()->m_PackageInfo.szSiteID, GetEnvPtr()->m_PackageInfo.szSiteID);
		UbcMessageBox(szTemp, MB_ICONERROR);
		return;
	}

	//+ 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	CReplaceInFile replaceInFile;
//	CString strClearChngList;

	POSITION posChngContents = CDataContainer::getInstance()->GetChangeContentsIdMap()->GetStartPosition();
	while(posChngContents)
	{
		CString strSrcId, strDstId;
		CDataContainer::getInstance()->GetChangeContentsIdMap()->GetNextAssoc(posChngContents, strSrcId, strDstId);

		if(strSrcId.IsEmpty() || strDstId.IsEmpty()) continue;

		if(strSrcId.CompareNoCase(strDstId) == 0) continue;

		// 변경하여 저장된 컨텐츠를 서버에 새로운 카피본으로 만들고있습니다.\n(컨텐츠 양에 따라 시간이 오래걸릴 수 있습니다.)
		wait.Show(LoadStringById(IDS_MAINFRAME_MSG034));

		CStringList children; // 새로운 부속파일 리스트
		CDataContainer::getInstance()->GetChildrenFileList(strSrcId,children);

		// 서버의 컨텐츠폴더 복사
		if(!copyContents(strSrcId, strDstId, GetEnvPtr()->m_PackageInfo.szPackage, children))
		{
			CString strMsg;
			strMsg.Format(_T("copyContents fail\nFrom : %s\nTo : %s"), strSrcId, strDstId);
			UbcMessageBox(strMsg);
			return;
		}

		// 패키지의 컨텐츠 ID 변경
		replaceInFile.AddItem(strSrcId, strDstId);

//		strClearChngList += strDstId + _T(":") + strDstId + _T(",");
	}

//	strClearChngList.Trim(_T(","));

	int nReplacedCnt = 0;
	if(replaceInFile.GetCount() > 0)
	{
		nReplacedCnt = replaceInFile.ReplaceInFile(szPackage, szPackage);
		if(nReplacedCnt < 0)
		{
			UbcMessageBox("ReplaceInFile fail");
			return;
		}

		//replaceInFile.DeleteAllItem();
		//replaceInFile.AddItem(strClearChngList, _T(""));
		//nReplacedCnt = replaceInFile.ReplaceInFile(szPackage, szPackage, TRUE);
		//if(nReplacedCnt < 0)
		//{
		//	UbcMessageBox("ReplaceInFile 실패");
		//	return;
		//}
	}
	//- 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제

	// skpark same_size_file_problem  upload/download 시에 호출될 Callback 객체를 등록한다.
	if(GetEnvPtr()->m_bUseTimeCheck) {
		TraceLog(("skpark same_size_file_problem set"));
		dlgMultiFtp.SetFTPCallback(CDataContainer::getInstance());
		CDataContainer::getInstance()->m_bUseTimeCheck = true;
	}

	if(dlgMultiFtp.RunFtp(CFtpMultiSite::eUpload))
	{
		wait.Show(LoadStringById(IDS_MAINFRAME_MSG036));

		std::string errMsg;
		if(copSetPackage(mapSite, dlgExp.m_szStartDT, dlgExp.m_szEndDT, errMsg))
		{
			UbcMessageBox(IDS_MAINFRAME_MSG037);
		}
		else
		{
			errMsg += "\n";
			errMsg += LoadStringById(IDS_MAINFRAME_MSG038);
			UbcMessageBox(errMsg.c_str());
		}
	}

	// 콘텐츠 패키지생성후 네트웍전송한뒤에 콘텐츠 패키지 복원 또는 콘텐츠다운로드시 등록되지않은 콘텐츠 패키지로 나옴
	if(GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
	{
		SPackageInfo4Studio* pInfo = GetEnvPtr()->FindPackage(GetEnvPtr()->m_PackageInfo.szPackage, GetEnvPtr()->m_PackageInfo.szDrive);
		if(pInfo)
		{
			// 서버에 등록된 콘텐츠 패키지이므로 플래그 수정
			GetEnvPtr()->m_PackageInfo.szProcID = _T("1");

			*pInfo = GetEnvPtr()->m_PackageInfo;

			SPackageInfo4Studio* pNewInfo = new SPackageInfo4Studio;
			*pNewInfo = *pInfo;

			// 로컬 ini 정보로 수정한다.
			pNewInfo->szDrive = STR_ENV_SERVER;

			// 서버 ini 정보는 안보이도록 한다.
			pNewInfo->bShow = false;

			// 로컬 ini 정보를 등록한다.
			GetEnvPtr()->AddPackage(pNewInfo);
		}
	}

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	// 패키지 정보 ini가 변경되었으므로 다시 로드하도록 한다.
	if(nReplacedCnt > 0)
	{
		m_document.Load(GetEnvPtr()->m_PackageInfo.szDrive, GetEnvPtr()->m_PackageInfo.szPackage);
	}

#endif//_UBCSTUDIO_EE_
	//GetEnvPtr()->InitPackageList(true);

	//szTemp = dlgFtp.GetErrorMsg();
	//if(!szTemp.IsEmpty()){
	//	CFileTranInfo dlgInfo;
	//	dlgInfo.m_szInfo = szTemp;
	//	dlgInfo.DoModal();
	//}
}

LRESULT CMainFrame::OnFTPUpload(WPARAM wParam, LPARAM lParam)
{
	CString strMsg = "";
	CString strTmp;

	if(!m_pdlgFtp)	return 0;

	if(m_pdlgFtp->m_aryFailHostInfo.GetCount() != 0)
	{
		//각 호스트의 결과 메시지를 출력해 준다
		CNetDeviceInfo* pclsNetInfo = NULL;
		for(int i=0; i<m_pdlgFtp->m_aryFailHostInfo.GetCount(); i++)
		{
			pclsNetInfo = (CNetDeviceInfo*)m_pdlgFtp->m_aryFailHostInfo.GetAt(i);
			strTmp.Format("%s [ %s ]\r\n", pclsNetInfo->m_strDeviceName, pclsNetInfo->m_strIP);
			strMsg += strTmp;
			strMsg += pclsNetInfo->m_strErrorMsg;
			strMsg += "\r\n\r\n";
		}//for

		if(strMsg != "")
		{
			strMsg.TrimRight("\r\n");
			UbcMessageBox(strMsg, MB_ICONINFORMATION);
		}//if
	}//if

	if(m_pdlgFtp)
	{
		if(m_pdlgFtp->GetSafeHwnd())
		{
			m_pdlgFtp->ShowWindow(SW_HIDE);
		}//if
		m_pdlgFtp->ClearNetDeviceArray();
		delete m_pdlgFtp;
		m_pdlgFtp = NULL;
	}//if

	return 0;
}

void CMainFrame::OnPackageClose()
{
	if(m_document.IsModify())
	{
		int ret_value = UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG001), MB_YESNOCANCEL | MB_ICONWARNING);
		if(ret_value == IDYES)
		{
			//m_bTerminate = true;
			m_sStateSave = E_STATE_SAVE_CLOSE;
			OnSave();
			return;
		}
		else if(ret_value == IDCANCEL)
		{
			return;
		}
	}//if

	m_document.ClosePackage();
	m_document.m_bNew = false;
	m_document.m_bOpened = false;
	m_sStateSave	= E_STATE_SAVE_NONE;

	AfxGetApp()->CloseAllDocuments(FALSE);
}

void CMainFrame::OnPackageDelete()
{
	CDeletePackageDlg dlg;
	//dlg.SetHostInfoList(&m_aryLocalHostInfo);
	dlg.DoModal();
}

void CMainFrame::OnEditNetworkHost()
{
	CNetworkHostManageDlg dlg;
	dlg.DoModal();
}

bool CMainFrame::CheckLicense()
{
	TCHAR  szTemp[MAX_PATH] = { 0x00 };
	DWORD  dwBufSize = MAX_PATH;
	CString szComputerName;
	std::string strAdtMac;

	//먼저 컴퓨터의 이름(호스트 명을 구한다
	TraceLog(("GetComputerNameEx()"));
	if(!GetComputerNameEx(ComputerNamePhysicalDnsHostname, szTemp, &dwBufSize)){
		return FALSE;
	}
	szComputerName = szTemp;
	szComputerName.MakeUpper();
	//컴퓨터의 네트워크카드의 맥 주소를 얻어온다.
	scratchUtil* aUtil = scratchUtil::getInstance();
	/*
	TraceLog(("aUtil->loadAdapter()"));
	int nCnt = aUtil->loadAdapter(E_ADT_TYPE_LOCAL);
	if(nCnt == 0){
		return false;
	}

	// 0 번째 맥 주소만 가지고 판단한다
	//ciString strAdtMac;
	TraceLog(("aUtil->getAdaptermacaddr()"));
	strAdtMac = aUtil->getAdaptermacaddr(0);
	if(strAdtMac == ""){
		return false;
	}
	*/
	//등록파일을 읽어서 컴퓨터 이름과 네트워크 카드의 맥주소를 비교하여,
	//하나라도 불일치한다면 정품이 아니다.
	std::string strHost, strMac;
	TraceLog(("aUtil->readAuthFile()"));
	if(!aUtil->readAuthFile(strHost, strMac)){
		return false;
	}

	//컴퓨터 이름(호스트네임) 비교
	TraceLog(("Computer Name : %s",strHost.c_str()));
	if(szComputerName != strHost.c_str()){
		return false;
	}

	//맥 주소 적용
	TraceLog(("Mac Address : %s",strMac.c_str()));
	//if(strAdtMac != strMac.c_str()){
	if(!aUtil->IsExistmacaddr(strMac.c_str())){
		return false;
	}

	return true;
}

BOOL CMainFrame::OnToolTipText(UINT nID, NMHDR* pNMHDR, LRESULT *pResult)
{
	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
	CString strTipText = _T("");

	switch(pNMHDR->idFrom)
	{
	case ID_NETWORK_EXPORT:
		if(GetEnvPtr()->m_bLicense){
			strTipText = LoadStringById(IDS_MAINFRAME_STR009);
		}
		break;
	case ID_TOOLS_FILECLEANER:
		strTipText = LoadStringById(IDS_MAINFRAME_STR010);
		break;
	case ID_TOOLS_EXPLORER:
		strTipText = LoadStringById(IDS_MAINFRAME_STR011);
		break;
	}

	if(!strTipText.IsEmpty()){
		if(pNMHDR->code == TTN_NEEDTEXTA)
			lstrcpyn(pTTTA->szText, strTipText, _countof(pTTTA->szText));
		else
			_mbstowcsz(pTTTW->szText, strTipText, _countof(pTTTW->szText));
		return TRUE;
	}

	return CFrameWnd::OnToolTipText(nID, pNMHDR, pResult);
}

void CMainFrame::TerminateProcess(CString processName)
{
	CArray<DWORD,DWORD> processIDs;

	if(IsAliveProcess(processName, &processIDs))  //살아있으면
	{
		for(int i=0; i < processIDs.GetSize(); i++)
		{
			DWORD processID = processIDs.GetAt(i);
			KillProcess(processID);
		}
	}
}

BOOL CMainFrame::IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs)
{
	CString newName = "";

	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);

	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(PROCESSENTRY32);

	BOOL bIsAlive = FALSE;

	while(Process32Next(hSnapShot,&pEntry))
	{	
		// 1. 프로세스명 얻기
		CString newName = pEntry.szExeFile;

		newName.MakeUpper();
		processName.MakeUpper();

		// 2. 프로세스명 비교
		if( processName.Find(newName) >= 0 )
		{
			if(processIDs != NULL)
				processIDs->Add(pEntry.th32ProcessID);
			bIsAlive = TRUE;
		}
	}

	CloseHandle(hSnapShot);

	return bIsAlive;
}

void CMainFrame::KillProcess(DWORD dwProcessId)
{
	HANDLE hProcessGUI = ::OpenProcess(PROCESS_TERMINATE, FALSE, dwProcessId);

	if( hProcessGUI ) 
	{
		::TerminateProcess(hProcessGUI, 0);
		//AppMonitor::SafeTerminateProcess(hProcessGUI, 0);
		::CloseHandle(hProcessGUI);
	}
}

void CMainFrame::OnFileCleaner()
{
	if(m_document.IsModify())
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_MAINFRAME_MSG024), GetHostName());
		UbcMessageBox(strMsg, MB_ICONWARNING);
		return;
	}

	CFileCleaner dlg;
	dlg.DoModal();
}

void CMainFrame::OnExplorer()
{
	// 내문서
	TCHAR szMyDocPath[MAX_PATH] = {0,};
	::SHGetSpecialFolderPath(NULL, szMyDocPath, CSIDL_PERSONAL, FALSE);
	CString szOpenFolderPath(szMyDocPath);
/*
	// 최근폴더
	TCHAR szCurrentPath[MAX_PATH] = {0,};
	//::GetCurrentDirectory(MAX_PATH, szCurrentPath);
	::SHGetSpecialFolderPath(NULL, szCurrentPath, CSIDL_RECENT, FALSE);

	CFileFind ff;
	if(ff.FindFile(szCurrentPath))
	{
		szOpenFolderPath = szCurrentPath;
	}
*/
	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		szOpenFolderPath = CEnviroment::GetObject()->m_szPathOpenFile;
	}

	::ShellExecute(NULL, "open", szOpenFolderPath, NULL, NULL, SW_SHOW);
}

BOOL CMainFrame::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	LPNMHDR lpnm = (LPNMHDR)lParam;
	LPNMTOOLBAR lpnmTB = (LPNMTOOLBAR)lParam;

	switch(lpnm->code)
	{
		case TBN_DROPDOWN:
			if(m_document.IsModify())
			{
				int nRet = UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG001), MB_YESNOCANCEL|MB_ICONWARNING);

				if(nRet == IDYES)
				{
					m_sStateSave = E_STATE_SAVE_NONE;
					OnSave();
					return FALSE;
				}
				else if(nRet == IDCANCEL)
				{
					return FALSE;
				}
			}

			CMenu menu;
			VERIFY(menu.LoadMenu(IDR_MENU_POPUP));
			CMenu* pPopup = menu.GetSubMenu(0);
			ASSERT(pPopup != NULL);
			for(int i=0;i<2;i++) { // skpark 2013.2.21 한번하면 이상하게 아무것도 안나오므로 두번한다.
				if(SetLatestPackageMenu(pPopup))
				{
					RECT rect = {0,0,0,0};
					m_wndToolBar.GetWindowRect(&rect);
					pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON, rect.left + ((48+7) * 2) + 8, rect.bottom - 3, this);
				}
			}
		return FALSE; //indicates the TBN_DROPDOWN
	}

	return CMDIFrameWnd::OnNotify(wParam, lParam, pResult);
}

void CMainFrame::OnLatestPackage(UINT nID)
{
	LoadPackage( m_arLatestPackageName[0][nID-ID_LATEST_PACKAGE_1]
				, m_arLatestPackageName[0][nID-ID_LATEST_PACKAGE_1].GetLength()
				, m_arLatestPackageName[1][nID-ID_LATEST_PACKAGE_1]
				);
}

bool CMainFrame::SetLatestPackageMenu(CMenu* pPopup)
{
	// skpark 2013.2.21 한번했으면 다시 조사 안한다.
	if(m_arLatestPackageName[0].GetCount() > 0){
		for(int i=0; i<10 ;i++)
		{
			if(i < m_arLatestPackageName[0].GetCount())
			{
				// 2010.10.06 스튜디오의 빠른콘텐츠 패키지열기 툴바에 경로포함하여 보여주기
				pPopup->ModifyMenu(ID_LATEST_PACKAGE_1+i, MF_BYCOMMAND, ID_LATEST_PACKAGE_1+i, m_arLatestPackageName[1][i] + m_arLatestPackageName[0][i]);
			}
			else
			{
				pPopup->DeleteMenu(ID_LATEST_PACKAGE_1+i, MF_BYCOMMAND);
			}
		}		
		return  true;
	}


	m_arLatestPackageName[0].RemoveAll();
	m_arLatestPackageName[1].RemoveAll();

	if(!pPopup) return false;

	CWaitMessageBox wait;

	CString strCurDateTime = CTime::GetCurrentTime().Format(_T("%Y/%m/%d %H:%M:%S"));
	CStringArray arLatestPackageDate;

	MakeLocalHostInfoArray();

	for(int i=0; i<m_aryLocalHostInfo.GetCount(); i++)
	{
		CLocalHostInfo* pclsHost = (CLocalHostInfo*)m_aryLocalHostInfo.GetAt(i);
		if(!pclsHost) continue;

		CString strDrive, strNetworkUse, strHostName, strDescription, strSite, strLastWrittenTime, strLastWrittenUser;
		
		long nContentsCategory, nPurpose, nHostType, nVertical, nResolution;
		bool bIsPublic, bIsVerify;
		CString strValidationDate;

		pclsHost->GetLocalHostInfo(strDrive, strNetworkUse, strHostName, strDescription, strSite, strLastWrittenTime, strLastWrittenUser
								  , nContentsCategory, nPurpose, nHostType, nVertical, nResolution, bIsPublic, bIsVerify, strValidationDate);

		// 0000774: 최근콘텐츠 패키지 빠른 열기에서 템플릿 콘텐츠 패키지는 안보이도록 하기.
		if(strHostName.Find(SAMPLE_FILE_KEY) == 0) continue;
		if(strHostName.Find(PREVIEW_FILE_KEY) == 0) continue;
		if(strHostName.Find(TEMPORARY_SAVE_FILE_KEY) == 0) continue;
		// 0001491: 유효기간이 지난 패키지도 열 수있도록 한다.
		//if(!strValidationDate.IsEmpty() && strValidationDate != _T("1970/01/01 09:00:00") && strCurDateTime > strValidationDate) continue;

		int nFindIdx = 0;
		for(nFindIdx=0; nFindIdx<arLatestPackageDate.GetCount() ;nFindIdx++)
		{
			if(strLastWrittenTime > arLatestPackageDate[nFindIdx])
			{
				break;
			}
		}

		m_arLatestPackageName[0].InsertAt(nFindIdx, strHostName);
		m_arLatestPackageName[1].InsertAt(nFindIdx, strDrive   );
		arLatestPackageDate.InsertAt(nFindIdx, strLastWrittenTime);
	}

	for(int i=0; i<10 ;i++)
	{
		if(i < m_arLatestPackageName[0].GetCount())
		{
			// 2010.10.06 스튜디오의 빠른콘텐츠 패키지열기 툴바에 경로포함하여 보여주기
			pPopup->ModifyMenu(ID_LATEST_PACKAGE_1+i, MF_BYCOMMAND, ID_LATEST_PACKAGE_1+i, m_arLatestPackageName[1][i] + m_arLatestPackageName[0][i]);
		}
		else
		{
			pPopup->DeleteMenu(ID_LATEST_PACKAGE_1+i, MF_BYCOMMAND);
		}
	}

	return m_arLatestPackageName[0].GetCount() > 0;
}

void CMainFrame::OnAutoUpdate()
{
	GetEnvPtr()->SetAutoUpdate(!GetEnvPtr()->GetAutoUpdate());
}

void CMainFrame::OnContentsManager()
{
#ifdef _UBCSTUDIO_EE_
	CPublicContentsDlg dlg(GetEnvPtr()->m_PackageInfo.szSiteID, GetEnvPtr()->m_PackageInfo.szPackage, GetEnvPtr()->m_strCustomer);
	if(dlg.DoModal() != IDOK) return;

	CONTENTS_INFO_LIST& lsContentsList = dlg.GetSelectedContentsIdList();

	int nAddCnt = 0;
	for(int i=0; i<lsContentsList.GetCount() ;i++)
	{
		CONTENTS_INFO* pPublicContents = (CONTENTS_INFO*)lsContentsList.GetAt(i);
		if(!pPublicContents) continue;

		// 이미 등록되어 있는지 체크
		if(CDataContainer::getInstance()->GetContents(pPublicContents->strId)) continue;

		CONTENTS_INFO* pContents = new CONTENTS_INFO;
		*pContents = *pPublicContents;

		// 부속파일처리 관련 수정
		CString strCompareStr = "/contents/" + (!pContents->strParentId.IsEmpty() ? pContents->strParentId : pContents->strId) + "/";
		if(pContents->strServerLocation.MakeLower().Find(strCompareStr.MakeLower()) == 0)
		{
			pContents->strLocalLocation = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH + pContents->strServerLocation.Mid(strCompareStr.GetLength());
		}
		else
		{
			pContents->strLocalLocation = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH;
		}

		CString strErrMsg;
		CDataContainer::getInstance()->AddContents(pContents, strErrMsg);

		nAddCnt++;
	}

	if(nAddCnt > 0)
	{
		CWnd* pWnd = GetChildFrame(ID_PACKAGE_MANAGE);
		if(pWnd) pWnd->SendMessage(WM_CONTENTS_LIST_CHANGED);
	}
#endif
}

// 0001128: Studio 임시 저장 및 복구
void CMainFrame::OnConfiguration()
{
	CConfigDlg dlg;
	if(dlg.DoModal() != IDOK) return;

	KillTimer(TIMER_AUTO_SAVE);

	if(GetEnvPtr()->m_bUseAutoTempSave && GetEnvPtr()->m_nAutoTempSaveInterval > 0)
	{
		SetTimer(TIMER_AUTO_SAVE, GetEnvPtr()->m_nAutoTempSaveInterval * 60 * 1000, NULL);
	}
}

void CMainFrame::OnFileLoadFromTemporary()
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString ini_path;
	ini_path.Format(_T("%s\\%s%s.ini"), szDrive, UBC_CONFIG_PATH, TEMPORARY_SAVE_FILE_KEY);

	if(::PathFileExists(ini_path) == FALSE)
	{
		UbcMessageBox(LoadStringById(IDS_UBCSTUDIODOC_MSG007));
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG032), MB_YESNO) != IDYES) return;

	m_document.LoadFromTemporaryFile();

	AfxGetApp()->CloseAllDocuments(FALSE);
	OnNewFrame(ID_PACKAGE_MANAGE);
}

void CMainFrame::OnUpdateSave(CCmdUI *pCmdUI)
{
	bool enable = m_document.m_bOpened && IsAuth(_T("PSAV"));

	if( GetEnvPtr()->m_Authority == CEnviroment::eAuthUser )
	{
		// site_user일 경우 패키지의site와 유저의site 다를경우 직접save못하게(save_as만 가능)
		enable = enable && ( GetEnvPtr()->m_szSite == GetEnvPtr()->m_PackageInfo.szSiteID );
	}

	pCmdUI->Enable(enable);
	//pCmdUI->Enable(m_document.m_bOpened && IsAuth(_T("PSAV")));
}

void CMainFrame::OnUpdateSaveAs(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_document.m_bOpened && IsAuth(_T("PSAV")));
}

void CMainFrame::OnUpdateNetworkExport(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	if(GetEnvPtr()->m_bLicense){
		pCmdUI->SetText(LoadStringById(IDS_MAINFRAME_MEN001));		
	}else{
		pCmdUI->SetText(LoadStringById(IDS_MAINFRAME_MEN002));
	}

	pCmdUI->Enable(m_document.m_bOpened && IsAuth(_T("PSND")));
}

void CMainFrame::OnUpdateLayoutManage(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_document.m_bOpened && CEnviroment::eADASSET != GetEnvPtr()->m_Customer);
}

void CMainFrame::OnUpdatePackageManage(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_document.m_bOpened);
}

void CMainFrame::OnUpdateAutoUpdate(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(GetEnvPtr()->GetAutoUpdate());
}

void CMainFrame::OnUpdateContentsManager(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_document.m_bOpened);
}

void CMainFrame::OnUpdateInfoNew(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(IsAuth(_T("PREG")) || IsAuth(_T("PMOD")));
}

void CMainFrame::OnUpdateInfoImport(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(IsAuth(_T("PMOD")));
}

void CMainFrame::OnUpdatePackageDelete(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(IsAuth(_T("PDEL")));
}

void CMainFrame::OnUpdateFileRecovery(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_document.m_bOpened && IsAuth(_T("PMOD")));
}

void CMainFrame::OnUpdateFileLoadFromTemporary(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(IsAuth(_T("PMOD")));
}

void CMainFrame::OnUpdatePackageClose(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_document.m_bOpened);
}

void CMainFrame::OnFileNotification()
{
	CAnnounceDlg dlg;
	dlg.DoModal();
}


UINT CMainFrame::StrongSecurityThread(LPVOID param)
{
	TraceLog(("StrongSecurityThread"));
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	char buf[1024];
	CString szPath2, szTemp;
	szPath2.Format("%s%sdata\\%s", szDrive, szPath, UBCVARS_INI);

	::GetPrivateProfileString("ROOT", "SEC", "", buf, 1024, szPath2);
	szTemp = buf;
	if(szTemp != "S"){
		return 0;
	}

	CMainFrame* pDlg = (CMainFrame*)param;
	if(pDlg)
	{
		while(1) {
			if(pDlg->StrongSecurity()<0){
				UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG039));
				exit(1);				
			}
			::Sleep(3000);
		}
	}
	return 0;
}


int CMainFrame::StrongSecurity()
{
	HANDLE handle_snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if( handle_snapshot == INVALID_HANDLE_VALUE )
	{
		// ERROR !!!
		TraceLog(("Fail to CreateToolhelp32Snapshot !!!"));
		return -1;
	}

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	BOOL is_alive = FALSE;

	if( !::Process32First(handle_snapshot, &pe32) )
	{
		// ERROR !!!
		TraceLog(("Fail to Process32First !!!"));
		::CloseHandle(handle_snapshot);
		return -1;
	}

	do
	{
		if( strnicmp(pe32.szExeFile, "UBC", 3) == 0 ){
			continue;
		}
		if( strnicmp(pe32.szExeFile, "iexp", 4) == 0 ){
			continue;
		}
		if( strnicmp(pe32.szExeFile, "expl", 4) == 0 ){
			continue;
		}
		if( strnicmp(pe32.szExeFile, "task", 4) == 0 ){
			continue;
		}
		if( strnicmp(pe32.szExeFile, "ezQ", 3) == 0 ){
			continue;
		}

		if(getWHandle(pe32.th32ProcessID)){
			//UbcMessageBox(pe32.szExeFile);
			TraceLog(("%s is run", pe32.szExeFile));
			return -2;		
		}
	}
	while( ::Process32Next(handle_snapshot, &pe32) );
	::CloseHandle(handle_snapshot);
	return 1;
}

HWND			
CMainFrame::getWHandle(unsigned long pid)
{
	if(pid>0){
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);
		/*
		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1){
			parent =  GetParent(child);
			if(!parent){
				return child;
			}
			child = parent;
		}
		*/

		if ( ::IsWindowVisible( e.hwnd ) == FALSE) return 0; // 안보이는 윈도우 제외..
		if ( ::GetWindowTextLength(e.hwnd) == 0 ) return 0; //캡션바에 글자가없으면제외

		return e.hwnd;
	}
	return (HWND)0;
}

void CMainFrame::OnChangePackageProperty()
{
	if(!m_document.m_bOpened) return;

	m_document.ChangePackageProperty();

	//열려있는 각 view에 host명을 바꾸도록 한다
	CWnd* wnd = GetChildFrame(ID_PACKAGE_MANAGE);
	if(wnd) wnd->SendMessage(WM_SAVE_CONFIG_COMPELETE);

	wnd = GetChildFrame(ID_LAYOUT_MANAGE);
	if(wnd) wnd->SendMessage(WM_SAVE_CONFIG_COMPELETE);
}

void CMainFrame::OnUpdateChangePackageProperty(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_document.m_bOpened && IsAuth(_T("PSAV")));
}

void CMainFrame::OnToolsAutoencoding()
{
	TraceLog(("Run %s","UBCAutoEncoderUI"));

	CString szPath, szParam;
	szPath.Format("\"%s%s\"", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	HINSTANCE nRet = ShellExecute(NULL, NULL, _T("UBCAutoEncoderUI.exe"), szParam, szPath, SW_SHOWNORMAL);
}
