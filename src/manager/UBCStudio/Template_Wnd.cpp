// Template_Wnd.cpp : 구현 파일입니다.
//
#include "stdafx.h"
#include "UBCStudio.h"
#include "MainFrm.h"
#include "Template_Wnd.h"
#include "UBCStudioDoc.h"
#include "MemDC.h"
#include "InPlaceEdit.h"
#include "Enviroment.h"

IMPLEMENT_DYNAMIC(CTemplate_Wnd, CWnd)

CTemplate_Wnd::CTemplate_Wnd(int nIndex, TEMPLATE_WND_TYPE eWndType, bool bIsVertical)
:	m_nIndex (nIndex)
,	m_pFocusWindow (NULL)
,	m_eWndType (eWndType)
,	m_bIsVertical (bIsVertical)
,	m_nSelectTemplateLinkIndex (-1)
{
	m_fontNormal.CreatePointFont(9*10, "Tahoma");

	m_fontBold.CreateFont(
		14,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_BOLD,                   // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		"Tahoma"                   // lpszFacename
	);

	int r,g,b;
	::GetColorRGB(::GetSysColor(COLOR_HIGHLIGHT), r,g,b);
	r += (255-r)*2/3; if(r>255) r=255;
	g += (255-g)*2/3; if(g>255) g=255;
	b += (255-b)*2/3; if(b>255) b=255;

	TEMPLATE_SELECT_OUTLINE_COLOR	= RGB(0,0,0);
	TEMPLATE_SELECT_BG_COLOR		= ::GetSysColor(COLOR_HIGHLIGHT);//RGB(255,224,224);
	TEMPLATE_DESELECT_OUTLINE_COLOR	= RGB(0,0,0);
	TEMPLATE_DESELECT_BG_COLOR		= RGB(255,255,255);
	FRAME_SELECT_OUTLINE_COLOR		= RGB(0,0,0);
	FRAME_SELECT_TEXT_COLOR			= RGB(255,255,255);
	FRAME_SELECT_BG_COLOR			= RGB(r,g,b);//RGB(255,64,64);
	FRAME_DESELECT_OUTLINE_COLOR	= RGB(0,0,0);
	FRAME_DESELECT_TEXT_COLOR		= RGB(0,0,0);
	FRAME_DESELECT_BG_COLOR			= RGB(224,224,224);
	TEMPLATE_INFO_SELECT_TEXT_COLOR		= RGB(255,255,255);
	TEMPLATE_INFO_DESELECT_TEXT_COLOR	= RGB(128,128,128);

	::GetColorRGB(::GetSysColor(COLOR_HIGHLIGHT), r,g,b);
	r += (255-r)/10; if(r>255) r=255;
	g += (255-g)/10; if(g>255) g=255;
	b += (255-b)/10; if(b>255) b=255;
	TEMPLATE_INFO_TIMES_BG_COLOR = RGB(r,g,b);//RGB(255,64,64);
}

CTemplate_Wnd::~CTemplate_Wnd()
{
}

BEGIN_MESSAGE_MAP(CTemplate_Wnd, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEWHEEL()
	ON_MESSAGE(WM_TEMPLATE_LIST_CHANGED, OnTemplateListChanged)
	ON_MESSAGE(WM_PLAY_TEMPLATE_LIST_CHANGED, OnPlayTemplateListChanged)
	ON_MESSAGE(WM_TEMPLATE_FRAME_SELECT_CHANGED, OnTemplateFrameSelectChanged)
	ON_MESSAGE(WM_PLAYCONTENTS_LIST_CHANGED, OnPlayContentsListChanged)
	ON_MESSAGE(WM_ENDLABELEDIT, OnEndLabelEdit)
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


// CTemplate_Wnd 메시지 처리기입니다.


int CTemplate_Wnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	CBitmap bmp1;
	bmp1.LoadBitmap(IDB_CONTENTS_LIST16);

	CBitmap bmp2;
	bmp2.LoadBitmap(IDB_CONTENTS_LIST16_ERROR);

	CBitmap bmp3;
	bmp3.LoadBitmap(IDB_CONTENTS_LIST16_SERVER);

	if(m_ilContentsList.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilContentsList.Add(&bmp1, RGB(255,255,255)); //바탕의 회색이 마스크
		m_ilContentsList.Add(&bmp2, RGB(255,255,255)); //바탕의 회색이 마스크
		m_ilContentsList.Add(&bmp3, RGB(255,255,255)); //바탕의 회색이 마스크
	}

	RecalcLayout();

	return 0;
}

void CTemplate_Wnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	CRect client_rect;

	if(m_bIsVertical)
	{
		int pos = GetScrollPos(SB_VERT);
		dc.SetWindowOrg(0, pos);

		GetClientRect(client_rect);
		client_rect.OffsetRect(0, pos);
	}
	else
	{
		int pos = GetScrollPos(SB_HORZ);
		dc.SetWindowOrg(pos, 0);

		GetClientRect(client_rect);
		client_rect.OffsetRect(pos, 0);
	}

	CMemDC memDC(&dc);
	memDC.FillSolidRect(client_rect, TEMPLATE_DESELECT_BG_COLOR); // fill background

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();
	DrawTemplate(&memDC, template_list);
}

void CTemplate_Wnd::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	SetFocus();

	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);

	if(m_bIsVertical == false)
	{
		CRect rect;
		GetClientRect(rect);

		TEMPLATE_LINK_LIST* template_list = GetTemplateList();

		int height = GetTotalWidth(template_list->GetCount());

		int pos;

		switch(nSBCode)
		{
		case SB_PAGEUP:
			pos = GetScrollPos(SB_HORZ);
			pos -= rect.Width();
			SetScrollPos(SB_HORZ, pos);
			break;
		case SB_PAGEDOWN:
			pos = GetScrollPos(SB_HORZ);
			pos += rect.Width();
			SetScrollPos(SB_HORZ, pos);
			break;
		case SB_LINEUP:
			pos = GetScrollPos(SB_HORZ);
			pos -= ( ( TEMPLATE_WIDTH + TEMPLATE_IN_LEFT_GAP + TEMPLATE_IN_RIGHT_GAP ) / 3 );
			SetScrollPos(SB_HORZ, pos);
			break;
		case SB_LINEDOWN:
			pos = GetScrollPos(SB_HORZ);
			pos += ( ( TEMPLATE_WIDTH + TEMPLATE_IN_LEFT_GAP + TEMPLATE_IN_RIGHT_GAP ) / 3 );
			SetScrollPos(SB_HORZ, pos);
			break;
		case SB_THUMBTRACK:
			SetScrollPos(SB_HORZ, nPos);
			break;
		default:
			break;
		}

		Invalidate(FALSE);
	}
}

void CTemplate_Wnd::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	SetFocus();

	CWnd::OnVScroll(nSBCode, nPos, pScrollBar);

	if(m_bIsVertical)
	{
		CRect rect;
		GetClientRect(rect);

		TEMPLATE_LINK_LIST* template_list = GetTemplateList();

		int height = GetTotalHeight(template_list->GetCount());

		int pos;

		switch(nSBCode)
		{
		case SB_PAGEUP:
			pos = GetScrollPos(SB_VERT);
			pos -= rect.Height();
			SetScrollPos(SB_VERT, pos);
			break;
		case SB_PAGEDOWN:
			pos = GetScrollPos(SB_VERT);
			pos += rect.Height();
			SetScrollPos(SB_VERT, pos);
			break;
		case SB_LINEUP:
			pos = GetScrollPos(SB_VERT);
			pos -= ( ( TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT + TEMPLATE_IN_TOP_GAP + TEMPLATE_IN_BOTTOM_GAP ) / 3 );
			SetScrollPos(SB_VERT, pos);
			break;
		case SB_LINEDOWN:
			pos = GetScrollPos(SB_VERT);
			pos += ( ( TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT + TEMPLATE_IN_TOP_GAP + TEMPLATE_IN_BOTTOM_GAP ) / 3 );
			SetScrollPos(SB_VERT, pos);
			break;
		case SB_THUMBTRACK:
			SetScrollPos(SB_VERT, nPos);
			break;
		default:
			break;
		}

		Invalidate(FALSE);
	}
}

void CTemplate_Wnd::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();

	if(m_bIsVertical)
	{
		int height = GetTotalHeight(template_list->GetCount());

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_PAGE | SIF_RANGE;
		info.nMin = 0;
		info.nMax = height;
		info.nPage = cy;
		info.nPos = 0;
		info.nTrackPos = 2;

		SetScrollInfo(SB_VERT, &info);
	}
	else
	{
		int width = GetTotalWidth(template_list->GetCount());

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_PAGE | SIF_RANGE;
		info.nMin = 0;
		info.nMax = width;
		info.nPage = cx;
		info.nPos = 0;
		info.nTrackPos = 2;

		SetScrollInfo(SB_HORZ, &info);
	}
}

void CTemplate_Wnd::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(m_pFocusWindow)
		m_pFocusWindow->SetFocus();

	CWnd::OnLButtonUp(nFlags, point);

	int pos = (m_bIsVertical ? GetScrollPos(SB_VERT) : GetScrollPos(SB_HORZ));

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();

	CRect info_edit_rect;
	bool template_find = false;
	bool info_find = false;
	int template_count = template_list->GetCount();

	CString strTemplateId = "";
	if(m_pDocument->GetSelectTemplateLink() != NULL)
		strTemplateId = m_pDocument->GetSelectTemplateLink()->pTemplateInfo->strId;

	int index = 0;
	for(; index<template_count && template_find == false; index++)
	{
		TEMPLATE_LINK& template_link = template_list->GetAt(index);

		CRect template_rect = template_link.rcViewRectItem;
		CRect info_view_rect = template_link.rcViewRectInfo[1];
		if(m_bIsVertical)
		{
			template_rect.OffsetRect(0, -pos);
			info_view_rect.OffsetRect(0, -pos);
		}
		else
		{
			template_rect.OffsetRect(-pos, 0);
			info_view_rect.OffsetRect(-pos, 0);
		}

		if(template_rect.PtInRect(point))
		{
			if(m_nSelectTemplateLinkIndex != index)
			{
				m_nSelectTemplateLinkIndex = index;
				Invalidate(FALSE);
			}
			if(strTemplateId != template_link.pTemplateInfo->strId)
			{
				template_find = true;
				strTemplateId = template_link.pTemplateInfo->strId;
			}
			info_view_rect.right -= 35;
			if(info_view_rect.PtInRect(point))
			{
				info_edit_rect = info_view_rect;
				info_find = true;
			}
			EnsureVisible(index);
		}
	}
	index--;

	if(template_find == true)
	{
		GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)strTemplateId, (LPARAM)(LPCTSTR)"");
	}
	if(info_find && m_eWndType == TEMPLATE_WND_PACKAGE )
	{
		TEMPLATE_LINK& template_link = template_list->GetAt(index);
		CInPlaceEdit* control = new CInPlaceEdit(this, info_edit_rect, WS_CHILD | ES_NUMBER | ES_RIGHT, 1000, ::ToString(template_link.nPlayTimes));
		control->SetFont(&m_fontNormal);
		control->ShowWindow(SW_SHOW);
	}
}

void CTemplate_Wnd::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnRButtonUp(nFlags, point);

	if(m_pFocusWindow)
		m_pFocusWindow->SetFocus();
}

BOOL CTemplate_Wnd::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();

	if(m_bIsVertical)
	{
		int height = GetTotalHeight(template_list->GetCount());
		int pos = GetScrollPos(SB_VERT);

		if(zDelta > 0)
			pos -= ( ( TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT + TEMPLATE_IN_TOP_GAP + TEMPLATE_IN_BOTTOM_GAP ) / 3 );
		else
			pos += ( ( TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT + TEMPLATE_IN_TOP_GAP + TEMPLATE_IN_BOTTOM_GAP ) / 3 );

		SetScrollPos(SB_VERT, pos);
	}
	else
	{
		int width = GetTotalWidth(template_list->GetCount());
		int pos = GetScrollPos(SB_HORZ);

		if(zDelta > 0)
			pos -= ( ( TEMPLATE_WIDTH + TEMPLATE_IN_LEFT_GAP + TEMPLATE_IN_RIGHT_GAP ) / 3 );
		else
			pos += ( ( TEMPLATE_WIDTH + TEMPLATE_IN_LEFT_GAP + TEMPLATE_IN_RIGHT_GAP ) / 3 );

		SetScrollPos(SB_HORZ, pos);
	}

	Invalidate(FALSE);

	return TRUE;
	//return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}

LRESULT CTemplate_Wnd::OnTemplateListChanged(WPARAM wParam, LPARAM lParam)
{
	RecalcLayout();
	Invalidate(FALSE);
	return 0;
}

LRESULT CTemplate_Wnd::OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam)
{
	RecalcLayout();

	switch(wParam)
	{
	case 0: // 템플릿 삭제
		m_nSelectTemplateLinkIndex = -1;
		break;

	case 1: // 템플릿 추가
		{
			TEMPLATE_LINK_LIST* template_list = GetTemplateList();
			m_nSelectTemplateLinkIndex = template_list->GetCount() - 1;
			EnsureVisible(m_nSelectTemplateLinkIndex);
		}
		break;
	}

	Invalidate(FALSE);
	return 0;
}

LRESULT CTemplate_Wnd::OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam)
{
	RecalcLayout();
	Invalidate(FALSE);
	return 0;
}

LRESULT CTemplate_Wnd::OnPlayContentsListChanged(WPARAM wParam, LPARAM lParam)
{
	Invalidate(FALSE);
	return 0;
}

LRESULT CTemplate_Wnd::OnEndLabelEdit(WPARAM wParam, LPARAM lParam)
{
	TEMPLATE_LINK_LIST* template_list = GetTemplateList();
	TEMPLATE_LINK& template_link = template_list->GetAt(m_nSelectTemplateLinkIndex);

	template_link.nPlayTimes = _ttoi((LPCTSTR)wParam);

	Invalidate(FALSE);

	return 0;
}

TEMPLATE_LINK_LIST* CTemplate_Wnd::GetTemplateList()
{
	switch(m_eWndType)
	{
	case TEMPLATE_WND_LAYOUT:
		return m_pDocument->GetAllTemplateList();
		break;

	case TEMPLATE_WND_PACKAGE:
		return m_pDocument->GetPlayTemplateList();
		break;
	}
	return NULL;
}

void CTemplate_Wnd::DrawTemplate(CDC* pDC, TEMPLATE_LINK_LIST* pTemplateList)
{
	TEMPLATE_LINK* select_template = m_pDocument->GetSelectTemplateLink();
	CString info_str;

	int count = pTemplateList->GetCount();
	for(int i=0; i<count; i++)
	{
		TEMPLATE_LINK& template_link = pTemplateList->GetAt(i);

		CRect rect = template_link.rcViewRect[m_nIndex];
		rect.InflateRect(1,1);

		COLORREF times_bg_color = RGB(255,255,255);
		COLORREF bg_color = RGB(255,255,255);

		if( select_template != NULL && template_link.pTemplateInfo == select_template->pTemplateInfo )
		{
			if(m_nSelectTemplateLinkIndex == i || m_eWndType == TEMPLATE_WND_LAYOUT)
			{
				times_bg_color = TEMPLATE_INFO_TIMES_BG_COLOR;
				bg_color = TEMPLATE_SELECT_BG_COLOR;

				pDC->FillSolidRect(template_link.rcViewRectItem, TEMPLATE_SELECT_BG_COLOR); // fill select template
				if(m_bIsVertical)	pDC->FillSolidRect(template_link.rcViewRectItem.left,template_link.rcViewRectItem.top,template_link.rcViewRectItem.Width(),1,RGB(228,228,228));
				else				pDC->FillSolidRect(template_link.rcViewRectItem.left,template_link.rcViewRectItem.top,1,template_link.rcViewRectItem.Height(),RGB(228,228,228));

				if(m_eWndType == TEMPLATE_WND_PACKAGE)
				{
					CRect info_view_rect = template_link.rcViewRectInfo[1];
					info_view_rect.right -= 37;
					pDC->FillSolidRect(info_view_rect, times_bg_color);
				}

				pDC->Draw3dRect(rect, TEMPLATE_SELECT_OUTLINE_COLOR, TEMPLATE_SELECT_OUTLINE_COLOR);
				pDC->SetBkColor(TEMPLATE_SELECT_BG_COLOR);
			}
			else
			{
				pDC->FillSolidRect(template_link.rcViewRectItem, FRAME_SELECT_BG_COLOR); // fill select template
				if(m_bIsVertical)	pDC->FillSolidRect(template_link.rcViewRectItem.left,template_link.rcViewRectItem.top,template_link.rcViewRectItem.Width(),1,RGB(228,228,228));
				else				pDC->FillSolidRect(template_link.rcViewRectItem.left,template_link.rcViewRectItem.top,1,template_link.rcViewRectItem.Height(),RGB(228,228,228));
				pDC->Draw3dRect(rect, TEMPLATE_SELECT_OUTLINE_COLOR, TEMPLATE_SELECT_OUTLINE_COLOR);
				pDC->SetBkColor(FRAME_SELECT_BG_COLOR);
			}
			pDC->SetTextColor( TEMPLATE_INFO_SELECT_TEXT_COLOR );
		}
		else
		{
			COLORREF bg_col = (i % 2) ? RGB(245,245,248) : TEMPLATE_DESELECT_BG_COLOR;
			times_bg_color = (i % 2) ? RGB(239,239,255) : RGB(245,245,255);
			bg_color = bg_col;

			pDC->FillSolidRect(template_link.rcViewRectItem, bg_col); // fill deselect template
			if(m_bIsVertical)	pDC->FillSolidRect(template_link.rcViewRectItem.left,template_link.rcViewRectItem.top,template_link.rcViewRectItem.Width(),1,RGB(228,228,228));
			else				pDC->FillSolidRect(template_link.rcViewRectItem.left,template_link.rcViewRectItem.top,1,template_link.rcViewRectItem.Height(),RGB(228,228,228));

			if(m_eWndType == TEMPLATE_WND_PACKAGE)
			{
				CRect info_view_rect = template_link.rcViewRectInfo[1];
				info_view_rect.right -= 37;
				pDC->FillSolidRect(info_view_rect, times_bg_color);
			}

			pDC->Draw3dRect(rect, TEMPLATE_DESELECT_OUTLINE_COLOR, TEMPLATE_DESELECT_OUTLINE_COLOR);
			pDC->SetBkColor(bg_col);
			pDC->SetTextColor( TEMPLATE_INFO_DESELECT_TEXT_COLOR );
		}
		pDC->SelectObject(&m_fontBold);

		pDC->DrawText(template_link.pTemplateInfo->strId, template_link.rcViewRectInfo[0], DT_TOP | DT_LEFT | DT_SINGLELINE | DT_VCENTER);

		if(m_eWndType == TEMPLATE_WND_PACKAGE)
		{
			info_str.Format("%d times", template_link.nPlayTimes);
			pDC->SetBkColor(times_bg_color);
			pDC->DrawText(info_str, template_link.rcViewRectInfo[1], DT_TOP | DT_RIGHT | DT_SINGLELINE| DT_VCENTER);

			info_str.Format(" times", template_link.nPlayTimes);
			pDC->SetBkColor(bg_color);
			pDC->DrawText(info_str, template_link.rcViewRectInfo[1], DT_TOP | DT_RIGHT | DT_SINGLELINE| DT_VCENTER);
		}
		else
		{
			info_str.Format("%d x %d", template_link.pTemplateInfo->rcRect.Width(), template_link.pTemplateInfo->rcRect.Height());
			pDC->DrawText(info_str, template_link.rcViewRectInfo[1], DT_TOP | DT_RIGHT | DT_SINGLELINE| DT_VCENTER);
		}

		//
		DrawFrame(pDC, &template_link);
	}
}

void CTemplate_Wnd::DrawFrame(CDC* pDC, TEMPLATE_LINK* pTemplateLink)
{
	pDC->SelectObject(&m_fontBold);

	int count = pTemplateLink->arFrameLinkList.GetCount();
	for(int i=0; i<count; i++)
	{
		FRAME_LINK& frame_link = pTemplateLink->arFrameLinkList.GetAt(i);

		CRect rect = frame_link.rcViewRect[m_nIndex];

		pDC->Draw3dRect(rect, FRAME_DESELECT_OUTLINE_COLOR, FRAME_DESELECT_OUTLINE_COLOR); // outline
		rect.DeflateRect(1,1);

		TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();

		if( template_link != NULL && pTemplateLink->pTemplateInfo == template_link->pTemplateInfo )
		{
			pDC->FillSolidRect(rect, FRAME_SELECT_BG_COLOR);
			pDC->SetBkColor(FRAME_SELECT_BG_COLOR);
		}
		else
		{
			pDC->FillSolidRect(rect, FRAME_DESELECT_BG_COLOR);
			pDC->SetBkColor(FRAME_DESELECT_BG_COLOR);
		}
		pDC->SetTextColor( FRAME_DESELECT_TEXT_COLOR );
		//pDC->DrawText(frame_link.pFrameInfo->strId, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

		if(m_eWndType == TEMPLATE_WND_PACKAGE)
		{
			// draw playcontents list
			CRect frame_rect = frame_link.rcViewRect[m_nIndex];
			frame_rect.DeflateRect(1,1);

			CPoint ptPlayContents(frame_rect.left, frame_rect.top);

			for(int j=0; j<frame_link.pFrameInfo->arCyclePlayContentsList.GetCount(); j++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(j);
				CONTENTS_INFO* pContentsInfo = m_pDocument->GetContents(pPlayContentsInfo->strContentsId);
				if(pContentsInfo)
				{
					if(pContentsInfo->bLocalFileExist)
					{
						m_ilContentsList.Draw(pDC, pContentsInfo->nContentsType+1, ptPlayContents, ILD_TRANSPARENT );
					}
					else
					{
						if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty()){
							if(pContentsInfo->bServerFileExist)
								m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_SERVER, ptPlayContents, ILD_TRANSPARENT );
							else
								m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT, ptPlayContents, ILD_TRANSPARENT );
						}else{
							m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT, ptPlayContents, ILD_TRANSPARENT );
						}
					}

					ptPlayContents.x += 16;
					if(ptPlayContents.x+16 > frame_rect.right)
					{
						ptPlayContents.y += 16;
						ptPlayContents.x = frame_rect.left;
					}//if
				}//if
				// Modified by 정운형 2008-12-24 오전 10:11
				// 변경내역 :  캡션기능 추가 - 버그 수정
			}
			for(int j=0; j<frame_link.pFrameInfo->arTimePlayContentsList.GetCount(); j++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(j);
				CONTENTS_INFO* pContentsInfo = m_pDocument->GetContents(pPlayContentsInfo->strContentsId);
				if(pContentsInfo)
				{
					if(pContentsInfo->bLocalFileExist)
					{
						m_ilContentsList.Draw(pDC, pContentsInfo->nContentsType+1, ptPlayContents, ILD_TRANSPARENT );
					}
					else
					{
						if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty()){
							if(pContentsInfo->bServerFileExist)
								m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_SERVER, ptPlayContents, ILD_TRANSPARENT );
							else
								m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT, ptPlayContents, ILD_TRANSPARENT );
						}else{
							m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT, ptPlayContents, ILD_TRANSPARENT );
						}
					}//if

					ptPlayContents.x += 16;
					if(ptPlayContents.x+16 > frame_rect.right)
					{
						ptPlayContents.y += 16;
						ptPlayContents.x = frame_rect.left;
					}//if
				}//if
				// Modified by 정운형 2008-12-24 오전 10:11
				// 변경내역 :  캡션기능 추가 - 버그 수정
			}
		}
	}
}

int CTemplate_Wnd::GetViewRectPosVert(int count)
{
	return
	(
		TEMPLATE_OUT_TOP_GAP + 
		TEMPLATE_IN_TOP_GAP +
		(
			count *
			(
				TEMPLATE_HEIGHT + 
				TEMPLATE_INFO_HEIGHT + 
				TEMPLATE_IN_BOTTOM_GAP +
				TEMPLATE_IN_TOP_GAP
			)
		)
	);
}

int CTemplate_Wnd::GetViewRectPosHorz(int count)
{
	return
	(
		TEMPLATE_OUT_LEFT_GAP + 
		TEMPLATE_IN_LEFT_GAP +
		(
			count *
			(
				TEMPLATE_WIDTH + 
				TEMPLATE_IN_RIGHT_GAP +
				TEMPLATE_IN_LEFT_GAP
			)
		)
	);
}

int CTemplate_Wnd::GetTotalHeight(int count)
{
	return
	(
		TEMPLATE_OUT_TOP_GAP + 
		(
			count *
			(
				TEMPLATE_IN_TOP_GAP + 
				TEMPLATE_HEIGHT + 
				TEMPLATE_INFO_HEIGHT + 
				TEMPLATE_IN_BOTTOM_GAP
			)
		) + 
		TEMPLATE_OUT_BOTTOM_GAP
	);
}

int CTemplate_Wnd::GetTotalWidth(int count)
{
	return
	(
		TEMPLATE_OUT_LEFT_GAP + 
		(
			count *
			(
				TEMPLATE_IN_LEFT_GAP + 
				TEMPLATE_WIDTH + 
				TEMPLATE_IN_RIGHT_GAP
			)
		) + 
		TEMPLATE_OUT_RIGHT_GAP
	);
}

void CTemplate_Wnd::RecalcLayout()
{
	CRect client_rect;
	GetClientRect(client_rect);

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();
	int count = template_list->GetCount();

	if(m_bIsVertical)
	{
		for(int i=0; i<count; i++)
		{
			int viewrect_top = GetViewRectPosVert(i);

			//
			TEMPLATE_LINK& template_link = template_list->GetAt(i);

			template_link.RecalcLayout();

			template_link.rcViewRect[m_nIndex]        = template_link.pTemplateInfo->rcRect;
			template_link.rcViewRect[m_nIndex].left	  *= template_link.fScale;
			template_link.rcViewRect[m_nIndex].top	  *= template_link.fScale;
			template_link.rcViewRect[m_nIndex].right  *= template_link.fScale;
			template_link.rcViewRect[m_nIndex].bottom *= template_link.fScale;

			template_link.rcViewRect[m_nIndex].OffsetRect(TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP, viewrect_top + TEMPLATE_INFO_HEIGHT);

			//
			template_link.rcViewRectItem.SetRect(
				TEMPLATE_OUT_LEFT_GAP,
				viewrect_top - TEMPLATE_IN_TOP_GAP,
				TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP + TEMPLATE_WIDTH + TEMPLATE_IN_RIGHT_GAP,
				GetViewRectPosVert(i+1) - TEMPLATE_IN_TOP_GAP
			);

			//
			template_link.rcViewRectInfo[0].SetRect(
				TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP,
				viewrect_top, 
				TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP + TEMPLATE_WIDTH / 2,
				viewrect_top + TEMPLATE_INFO_HEIGHT
			);
			//if(m_eWndType == TEMPLATE_WND_PACKAGE)
			{
				template_link.rcViewRectInfo[1].SetRect(
					TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP + TEMPLATE_WIDTH / 2,
					viewrect_top, 
					TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP + TEMPLATE_WIDTH, 
					viewrect_top + TEMPLATE_INFO_HEIGHT
				);
			}
			//else
			//{
			//	template_link.rcViewRectInfo[1].SetRect(-1,-1,-1,-1);
			//}

			//
			int count2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<count2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				frame_link.rcViewRect[m_nIndex] = frame_link.pFrameInfo->rcRect;
				frame_link.rcViewRect[m_nIndex].left		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].top		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].right		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].bottom	*= template_link.fScale;

				frame_link.rcViewRect[m_nIndex].OffsetRect(TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP, viewrect_top + TEMPLATE_INFO_HEIGHT);
			}
		}

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_RANGE | SIF_PAGE;
		info.nMin = 0;
		info.nMax = GetTotalHeight(count);
		info.nPage = client_rect.Height();
		info.nTrackPos = 2;

		SetScrollInfo(SB_VERT, &info);
	}
	else
	{
		for(int i=0; i<count; i++)
		{
			int viewrect_left = GetViewRectPosHorz(i);

			//
			TEMPLATE_LINK& template_link = template_list->GetAt(i);

			template_link.rcViewRect[m_nIndex] = template_link.pTemplateInfo->rcRect;
			template_link.rcViewRect[m_nIndex].left		*= template_link.fScale;
			template_link.rcViewRect[m_nIndex].top		*= template_link.fScale;
			template_link.rcViewRect[m_nIndex].right		*= template_link.fScale;
			template_link.rcViewRect[m_nIndex].bottom		*= template_link.fScale;

			template_link.rcViewRect[m_nIndex].OffsetRect(viewrect_left, TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_INFO_HEIGHT);

			//
			template_link.rcViewRectItem.SetRect(
				viewrect_left - TEMPLATE_IN_LEFT_GAP,
				TEMPLATE_OUT_TOP_GAP,
				GetViewRectPosHorz(i+1) - TEMPLATE_IN_LEFT_GAP,
				TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT + TEMPLATE_IN_BOTTOM_GAP
			);

			//
			template_link.rcViewRectInfo[0].SetRect(
				viewrect_left, 
				TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP,
				viewrect_left + TEMPLATE_WIDTH / 2,
				TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_INFO_HEIGHT
			);
			//if(m_eWndType == TEMPLATE_WND_PACKAGE)
			{
				template_link.rcViewRectInfo[1].SetRect(
					viewrect_left + TEMPLATE_WIDTH / 2, 
					TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP,
					viewrect_left + TEMPLATE_WIDTH,
					TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_INFO_HEIGHT
				);
			}
			//else
			//{
			//	template_link.rcViewRectInfo[1].SetRect(-1,-1,-1,-1);
			//}

			//
			int count2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<count2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				frame_link.rcViewRect[m_nIndex] = frame_link.pFrameInfo->rcRect;
				frame_link.rcViewRect[m_nIndex].left		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].top		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].right		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].bottom	*= template_link.fScale;

				frame_link.rcViewRect[m_nIndex].OffsetRect(viewrect_left, TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_INFO_HEIGHT);
			}
		}

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_RANGE | SIF_PAGE;
		info.nMin = 0;
		info.nMax = GetTotalWidth(count);
		info.nPage = client_rect.Width();
		info.nTrackPos = 2;

		SetScrollInfo(SB_HORZ, &info);
	}
}

void CTemplate_Wnd::ScrollToEnd()
{
	CRect rect;
	GetClientRect(rect);

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();
	int count = template_list->GetCount();

	if(m_bIsVertical)
	{
		int height = GetTotalHeight(count);// - TEMPLATE_IN_TOP_GAP;

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_ALL;
		info.nMin = 0;
		info.nMax = height;
		info.nPage = rect.Height();
		info.nPos = height - rect.Height();
		info.nTrackPos = 2;

		SetScrollInfo(SB_VERT, &info);
	}
	else
	{
		int width = GetTotalWidth(count);

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_ALL;
		info.nMin = 0;
		info.nMax = width;
		info.nPage = rect.Width();
		info.nPos = width - rect.Width();
		info.nTrackPos = 2;

		SetScrollInfo(SB_HORZ, &info);
	}
}

void CTemplate_Wnd::EnsureVisible(int nIndex)
{
	int scroll_pos;
	int item_pos;

	CRect rect;
	GetClientRect(rect);

	if(m_bIsVertical)
	{
		scroll_pos = GetScrollPos(SB_VERT);
		item_pos = GetViewRectPosVert(nIndex) - TEMPLATE_IN_TOP_GAP;

		if(item_pos < scroll_pos)
		{
			SCROLLINFO info;
			info.cbSize = sizeof(info);
			info.fMask = SIF_POS;
			info.nPos = item_pos;
			SetScrollInfo(SB_VERT, &info);
			Invalidate(FALSE);
		}

		scroll_pos = GetScrollPos(SB_VERT) + rect.Height();
		item_pos = GetViewRectPosVert(nIndex + 1) - TEMPLATE_IN_TOP_GAP;

		if(item_pos > scroll_pos)
		{
			SCROLLINFO info;
			info.cbSize = sizeof(info);
			info.fMask = SIF_POS;
			info.nPos = item_pos - rect.Height();
			SetScrollInfo(SB_VERT, &info);
			Invalidate(FALSE);
		}
	}
	else
	{
		scroll_pos = GetScrollPos(SB_HORZ);
		item_pos = GetViewRectPosHorz(nIndex) - TEMPLATE_IN_LEFT_GAP;

		if(item_pos < scroll_pos)
		{
			SCROLLINFO info;
			info.cbSize = sizeof(info);
			info.fMask = SIF_POS;
			info.nPos = item_pos;
			SetScrollInfo(SB_HORZ, &info);
			Invalidate(FALSE);
		}

		scroll_pos = GetScrollPos(SB_HORZ) + rect.Width();
		item_pos = GetViewRectPosHorz(nIndex + 1) - TEMPLATE_IN_LEFT_GAP;

		if(item_pos > scroll_pos)
		{
			SCROLLINFO info;
			info.cbSize = sizeof(info);
			info.fMask = SIF_POS;
			info.nPos = item_pos - rect.Width();
			SetScrollInfo(SB_HORZ, &info);
			Invalidate(FALSE);
		}
	}
}

void CTemplate_Wnd::EnsureVisible(LPCTSTR strID)
{
	TEMPLATE_LINK_LIST* template_list = GetTemplateList();
	int count = template_list->GetCount();
	for(int i=0; i<count; i++)
	{
		TEMPLATE_LINK& template_link = template_list->GetAt(i);
		if(template_link.pTemplateInfo->strId == strID)
		{
			EnsureVisible(i);
			break;
		}
	}
}

void CTemplate_Wnd::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	GetParent()->SendMessage(WM_TEMPLATE_SELECTED, 0, 0);
	CWnd::OnLButtonDblClk(nFlags, point);

	TraceLog(("OnLButtonDblClk()"));

	//if(!m_pDocument || !m_pDocument->m_bOpened) return;
	CMainFrame* pmainFrame = (CMainFrame*)::AfxGetMainWnd();
	if(!pmainFrame) return;

	TraceLog(("OnLButtonDblClk 1()"));

	CWnd * pWnd = pmainFrame->GetChildFrame(ID_LAYOUT_MANAGE);

	if(pWnd)
	{
		TraceLog(("Open new layoutTemplate"));
		pWnd->BringWindowToTop();
		pWnd->SetActiveWindow();
		if(pWnd->IsIconic()) {
			pWnd->ShowWindow(SW_RESTORE);
		}
	}
	else
	{
		CUBCStudioApp *pApp = (CUBCStudioApp*)::AfxGetApp();
		CMultiDocTemplate *pTemplate = pApp->m_pLayoutTemplate;
		if(pTemplate){
			TraceLog(("Open layoutTemplate"));
			pTemplate->OpenDocumentFile(NULL);
		}
		pWnd = pmainFrame->GetChildFrame(ID_LAYOUT_MANAGE);
		if(pWnd) {
			TraceLog(("pWnd is not null"));
		}
	}
	

	int pos = (m_bIsVertical ? GetScrollPos(SB_VERT) : GetScrollPos(SB_HORZ));

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();

	CRect info_edit_rect;
	bool template_find = false;
	bool info_find = false;
	int template_count = template_list->GetCount();

	CString strTemplateId = "";
	if(m_pDocument->GetSelectTemplateLink() != NULL)
		strTemplateId = m_pDocument->GetSelectTemplateLink()->pTemplateInfo->strId;

	if(pWnd) {
		TraceLog(("SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED(%s))", strTemplateId));
		//GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)strTemplateId, (LPARAM)(LPCTSTR)"");
		pWnd->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)strTemplateId, (LPARAM)(LPCTSTR)"");
	}
}
