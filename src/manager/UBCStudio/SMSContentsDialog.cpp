// SMSContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "SMSContentsDialog.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "common/MD5Util.h"

// CSMSContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSMSContentsDialog, CSubContentsDialog)

CSMSContentsDialog::CSMSContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CSMSContentsDialog::IDD, pParent)
	, m_wndSMS (NULL, 0, false)
	, m_reposControl (this)
	, m_strLocation("")
	, m_bPreviewMode(false)
	, m_bPermanent(FALSE)
	, m_cbxContentsFont(IDB_TTF_BMP)
{
	m_nAlign = 1;
}

CSMSContentsDialog::~CSMSContentsDialog()
{
}

void CSMSContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_CONTENTS_FILE, m_btnBrowserContentsFile);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_WIDTH, m_staticContentsWidth);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_HEIGHT, m_staticContentsHeight);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_FILE_SIZE, m_staticContentsFileSize);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FONT_SIZE, m_editContentsFontSize);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_FONT, m_cbxContentsFont);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_TEXT_COLOR, m_cbxContentsTextColor);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_BG_COLOR, m_cbxContentsBGColor);
//	DDX_Control(pDX, IDC_LIST_CONTENTS_COMMENT, m_listctrlContentsComment);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_ADD, m_btnContentsCommentAdd);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_DELETE, m_btnContentsCommentDelete);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_UP, m_btnContentsCommentUp);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_DOWN, m_btnContentsCommentDown);
	DDX_Control(pDX, IDC_BUTTON_DELFILE, m_btnDelFile);
	DDX_Control(pDX, IDC_BUTTON_ZOOM_IN, m_btnZoomIn);
	DDX_Control(pDX, IDC_BUTTON_ZOOM_OUT, m_btnZoomOut);
	DDX_Control(pDX, IDC_BUTTON_ORIGINAL, m_btnOriginal);

	DDX_Control(pDX, IDC_CHECK_ALIGN1, m_chkAlign[0]);
	DDX_Control(pDX, IDC_CHECK_ALIGN2, m_chkAlign[1]);
	DDX_Control(pDX, IDC_CHECK_ALIGN3, m_chkAlign[2]);
	DDX_Control(pDX, IDC_CHECK_ALIGN4, m_chkAlign[3]);
	DDX_Control(pDX, IDC_CHECK_ALIGN5, m_chkAlign[4]);
	DDX_Control(pDX, IDC_CHECK_ALIGN6, m_chkAlign[5]);
	DDX_Control(pDX, IDC_CHECK_ALIGN7, m_chkAlign[6]);
	DDX_Control(pDX, IDC_CHECK_ALIGN8, m_chkAlign[7]);
	DDX_Control(pDX, IDC_CHECK_ALIGN9, m_chkAlign[8]);

	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_groupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_PLAY, m_btnPreviewPlay);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);

	DDX_Control(pDX, IDC_STATIC_COMMENT_TEXT_COLOR, m_staticCommentTextColor);
	DDX_Control(pDX, IDC_COMBO_COMMENT_TEXT_COLOR, m_cbxCommentTextColor);
}


BEGIN_MESSAGE_MAP(CSMSContentsDialog, CSubContentsDialog)
	ON_WM_SIZE()
//	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST_CONTENTS_COMMENT, &CSMSContentsDialog::OnLvnEndlabeleditListContentsComment)
//	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_CONTENTS_COMMENT, &CSMSContentsDialog::OnLvnKeydownListContentsComment)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_CONTENTS_FILE, &CSMSContentsDialog::OnBnClickedButtonBrowserContentsFile)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_UP, &CSMSContentsDialog::OnBnClickedButtonContentsCommentUp)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_DOWN, &CSMSContentsDialog::OnBnClickedButtonContentsCommentDown)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_PLAY, &CSMSContentsDialog::OnBnClickedButtonPreviewPlay)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_ADD, &CSMSContentsDialog::OnBnClickedButtonContentsCommentAdd)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_DELETE, &CSMSContentsDialog::OnBnClickedButtonContentsCommentDelete)
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, &CSMSContentsDialog::OnBnClickedPermanentCheck)
	ON_EN_CHANGE(IDC_EDIT_CONTENTS_COMMENT, &CSMSContentsDialog::OnEnChangeEditContentsComment)
	ON_BN_CLICKED(IDC_CHECK_ALIGN1, OnBnClickedAlign1)
	ON_BN_CLICKED(IDC_CHECK_ALIGN2, OnBnClickedAlign2)
	ON_BN_CLICKED(IDC_CHECK_ALIGN3, OnBnClickedAlign3)
	ON_BN_CLICKED(IDC_CHECK_ALIGN4, OnBnClickedAlign4)
	ON_BN_CLICKED(IDC_CHECK_ALIGN5, OnBnClickedAlign5)
	ON_BN_CLICKED(IDC_CHECK_ALIGN6, OnBnClickedAlign6)
	ON_BN_CLICKED(IDC_CHECK_ALIGN7, OnBnClickedAlign7)
	ON_BN_CLICKED(IDC_CHECK_ALIGN8, OnBnClickedAlign8)
	ON_BN_CLICKED(IDC_CHECK_ALIGN9, OnBnClickedAlign9)
	ON_BN_CLICKED(IDC_BUTTON_DELFILE, OnBnClickedDeletefile)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON_ZOOM_IN, &CSMSContentsDialog::OnBnClickedButtonZoomIn)
	ON_BN_CLICKED(IDC_BUTTON_ZOOM_OUT, &CSMSContentsDialog::OnBnClickedButtonZoomOut)
	ON_BN_CLICKED(IDC_BUTTON_ORIGINAL, &CSMSContentsDialog::OnBnClickedButtonOriginal)
	ON_CBN_SELCHANGE(IDC_COMBO_COMMENT_TEXT_COLOR, &CSMSContentsDialog::OnCbnSelchangeComboCommentTextColor)
END_MESSAGE_MAP()


// CSMSContentsDialog 메시지 처리기입니다.

BOOL CSMSContentsDialog::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnBrowserContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btnContentsCommentAdd.LoadBitmap(IDB_BTN_PLUS, RGB(255, 255, 255));
	m_btnContentsCommentDelete.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnContentsCommentUp.LoadBitmap(IDB_BTN_UP, RGB(255, 255, 255));
	m_btnContentsCommentDown.LoadBitmap(IDB_BTN_DOWN, RGB(255, 255, 255));
//	m_btnPreviewPlay.LoadBitmap(IDB_BTN_VIEW, RGB(255, 255, 255));
	m_btnPreviewPlay.LoadBitmap(IDB_BTN_PLAY, RGB(255, 255, 255));

	m_btnBrowserContentsFile.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT001));
	m_btnContentsCommentAdd.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT002));
	m_btnContentsCommentDelete.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT003));
	m_btnContentsCommentUp.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT004));
	m_btnContentsCommentDown.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT005));
	m_btnPreviewPlay.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT006));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(15);

	m_editContentsFontSize.SetValue(45);

//	m_cbxContentsFont.SubclassDlgItem (IDC_COMBO_CONTENTS_FONT, this);
//	m_cbxContentsFont.m_csSample = "No Fate But What We Make";
	m_cbxContentsFont.SetPreviewStyle (CFontPreviewCombo::NAME_ONLY, false);
	m_cbxContentsFont.SetFontHeight (12, true);

	for(int i=0; i<m_cbxContentsFont.GetCount(); i++)
	{
		CString font_name;
		m_cbxContentsFont.GetLBText(i,font_name);
		if(font_name == "System")
		{
			m_cbxContentsFont.SetCurSel(i);
			break;
		}
	}

	m_cbxContentsTextColor.InitializeDefaultColors();
	m_cbxContentsBGColor.InitializeDefaultColors();
	m_cbxCommentTextColor.InitializeDefaultColors();

	m_cbxContentsTextColor.SetSelectedColorValue(RGB(255,255,255));
	m_cbxContentsBGColor.SetSelectedColorValue(RGB(0,0,0));

	m_btnDelFile.LoadBitmap(IDB_BTN_DELFILE, RGB(236, 233, 216));
	m_btnDelFile.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT003));

	m_btnZoomIn.LoadBitmap(IDB_BTN_PLUS, RGB(236, 233, 216));
	m_btnZoomIn.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT007));

	m_btnZoomOut.LoadBitmap(IDB_BTN_MINUS, RGB(236, 233, 216));
	m_btnZoomOut.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT008));

	m_btnOriginal.LoadBitmap(IDB_BTN_1_1, RGB(236, 233, 216));
	m_btnOriginal.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT009));

//	CRect rect;
//	m_listctrlContentsComment.GetClientRect(rect);
//	m_listctrlContentsComment.InsertColumn(0, "comment", LVCFMT_LEFT, rect.Width() + 100);
//	m_listctrlContentsComment.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER); // auto size
//	m_listctrlContentsComment.SetTextBkColor(RGB(232,232,232));
//	m_listctrlContentsComment.SetExtendedStyle(m_listctrlContentsComment.GetExtendedStyle() | LVS_EX_FULLROWSELECT);

	//
	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);
	client_rect.DeflateRect(1,1);

	m_wndSMS.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndSMS.ShowWindow(SW_SHOW);

	//
	m_reposControl.AddControl(&m_groupPreview, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_staticContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_wndSMS, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnPreviewPlay, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnZoomIn, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnZoomOut, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnOriginal, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);

	m_reposControl.Move();

	//
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsWidth);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsHeight);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsFileSize);

	for(int i = 0; i < 9; i++)
	{
		for(int j = 0; j < CButtonST::BTNST_MAX_COLORS; j++)
		{
			m_chkAlign[i].SetColor(j, RGB(0xFF,0xFF,0xFF));
		}
		m_chkAlign[i].SetCheck(false);
	}

	m_chkAlign[0].SetIcon(IDI_ALIGN_LT_DN, IDI_ALIGN_LT_UP);
	m_chkAlign[1].SetIcon(IDI_ALIGN_CT_DN, IDI_ALIGN_CT_UP);
	m_chkAlign[2].SetIcon(IDI_ALIGN_RT_DN, IDI_ALIGN_RT_UP);
	m_chkAlign[3].SetIcon(IDI_ALIGN_LM_DN, IDI_ALIGN_LM_UP);
	m_chkAlign[4].SetIcon(IDI_ALIGN_CM_DN, IDI_ALIGN_CM_UP);
	m_chkAlign[5].SetIcon(IDI_ALIGN_RM_DN, IDI_ALIGN_RM_UP);
	m_chkAlign[6].SetIcon(IDI_ALIGN_LB_DN, IDI_ALIGN_LB_UP);
	m_chkAlign[7].SetIcon(IDI_ALIGN_CB_DN, IDI_ALIGN_CB_UP);
	m_chkAlign[8].SetIcon(IDI_ALIGN_RB_DN, IDI_ALIGN_RB_UP);

	m_nAlign = 5;
	m_chkAlign[4].SetCheck(true);
/*
	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);

	CRect r;
	pContents->GetRect(r);
	r.right += 50;
	pContents->SetRect(r);

	CString strDebug;
	strDebug.Format("%d, %d, %d, %d : %d", r.left, r.top, r.right, r.bottom, pContents->GetLimitText());
	pContents->SetWindowText(strDebug);
*/
	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}//if

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSMSContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnOK();
}

void CSMSContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnCancel();
	GetParent()->PostMessage(WM_CLOSE);
}

void CSMSContentsDialog::Stop()
{
	m_wndSMS.Stop(false);
}

bool CSMSContentsDialog::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData();
	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	//m_editContentsFileName.GetWindowText(info.strLocation);
	info.strLocalLocation = m_strLocation;
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();
	info.nFontSize = m_editContentsFontSize.GetValueInt();
	m_cbxContentsFont.GetLBText(m_cbxContentsFont.GetCurSel(), info.strFont);
	info.strFgColor = ::GetColorFromString(m_cbxContentsTextColor.GetSelectedColorValue());
	info.strBgColor = ::GetColorFromString(m_cbxContentsBGColor.GetSelectedColorValue());
	info.nAlign = m_nAlign;
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);
	for(int i=0 ; i<TICKER_COUNT ; i++)
	{
		info.strComment[i] = "";
		if( i < pContents->GetLineCount() )
		{
			char szLine[255+1] = {0};
			pContents->GetLine(i, szLine, 255);
			info.strComment[i] = szLine;
		}
	}

	//
	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(info.strLocalLocation, dir, path, filename, ext);

	info.strLocalLocation.Format("%s%s", dir, path); 
	info.strFilename.Format("%s%s", filename, ext); 

	//
	if(!IsFitContentsName(info.strContentsName)) return false;
	if(info.nRunningTime == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
		return false;
	}
	if(info.nFontSize == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG005), MB_ICONSTOP);
		return false;
	}

	return true;
}

bool CSMSContentsDialog::SetContentsInfo(CONTENTS_INFO& info)
{
	m_editContentsFileName.GetWindowText(m_wndSMS.m_strMediaFullPath);

	m_editContentsName.SetWindowText(info.strContentsName);
	//m_editContentsFileName.SetWindowText(info.strLocation + info.strFilename);
	m_editContentsFileName.SetWindowText(info.strFilename);

	for(int i = 0; i < 9; i++){
		m_chkAlign[i].SetCheck(false);
	}
	m_nAlign = info.nAlign;
	m_chkAlign[m_nAlign-1].SetCheck(true);
	
	if(info.strFilename.IsEmpty()){
		m_strLocation = _T("");
		info.nFilesize = 0;
	}else{
		m_strLocation = info.strLocalLocation + info.strFilename;
	}

	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}//if
	m_editContentsFontSize.SetWindowText(::ToString(info.nFontSize));

	int nFindFont = -1;
	int nSystemFont = -1;
	for(int i=0; i<m_cbxContentsFont.GetCount(); i++)
	{
		CString font_name;
		m_cbxContentsFont.GetLBText(i,font_name);
		
		if(font_name == "System")
		{
			nSystemFont = i;
		}

		if(font_name == info.strFont)
		{
			nFindFont = i;
			break;
		}
	}

	if(nFindFont >= 0) m_cbxContentsFont.SetCurSel(nFindFont);
	else               m_cbxContentsFont.SetCurSel(nSystemFont);

	m_cbxContentsTextColor.SetSelectedColorValue(::GetColorFromString(info.strFgColor));
	m_cbxContentsBGColor.SetSelectedColorValue(::GetColorFromString(info.strBgColor));

//	m_listctrlContentsComment.DeleteAllItems();

	int nEndPos = 0;
	for(int i=TICKER_COUNT-1 ; i>=0; i--)
	{
		if(!info.strComment[i].IsEmpty())
		{
			nEndPos = i;
			break;
		}
	}

	CString strContents = _T("");
	for(int i=0; i<=nEndPos; i++)
	{
		if(i>0) strContents += _T("\r\n");
		strContents += info.strComment[i];
	}

	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);
	pContents->SetWindowText(strContents);

	UpdateData(FALSE);

	OnEnChangeEditContentsComment();

	return true;
}

void CSMSContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CSubContentsDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();
}

//void CSMSContentsDialog::OnLvnEndlabeleditListContentsComment(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	*pResult = 0;
//
//	if(pDispInfo->item.pszText != NULL)
//	{
//		m_listctrlContentsComment.SetItemText(pDispInfo->item.iItem, pDispInfo->item.iSubItem, pDispInfo->item.pszText );
//		m_listctrlContentsComment.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER); // auto size
//	}
//}
//
//void CSMSContentsDialog::OnLvnKeydownListContentsComment(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	*pResult = 0;
//
//	switch(pLVKeyDow->wVKey)
//	{
//	case VK_F2:
//		for(int i=0; i<m_listctrlContentsComment.GetItemCount(); i++)
//		{
//			if(m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED))
//			{
//				m_listctrlContentsComment.SetFocus();
//				m_listctrlContentsComment.EditLabel(i);
//			}
//		}
//		break;
//	case VK_DELETE:
//		OnBnClickedButtonContentsCommentDelete();
//		break;
//	}
//}

void CSMSContentsDialog::OnBnClickedButtonBrowserContentsFile()
{
	CString szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
	CString filter;
	filter.Format("All Image Files|%s|"
				  "Bitmap Files (*.bmp)|*.bmp|"
				  "JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;|"
				  "GIF Files (*.gif)|*.gif|"
				  "PCX Files (*.pcx)|*.pcx|"
				  "PNG Files (*.png)|*.png|"
				  "TIFF Files (*.tif)|*.tif;*.tiff||"
				 , szFileExts
				 );

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, CONTENTS_IMAGE)){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	//m_editContentsFileName.SetWindowText(dlg.GetPathName());
	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = dlg.GetPathName();
	m_editContentsFileName.SetSel(0, -1);

	m_image.Destroy();

	CFileStatus fs;
//		if(CFile::GetStatus(dlg.GetPathName(), fs))
	if(CEnviroment::GetFileSize(dlg.GetPathName(), fs.m_size))
	{
		if(m_image.Load(dlg.GetPathName()))
		{
			int width = m_image.GetWidth();
			int height = m_image.GetHeight();

			m_staticContentsWidth.SetWindowText(::ToString(width) + " ");
			m_staticContentsHeight.SetWindowText(::ToString(height) + " ");
			m_staticContentsFileSize.SetWindowText(::ToMoneyTypeString((ULONGLONG)(fs.m_size/1024)) + " ");
			m_ulFileSize = fs.m_size;
		}
	}

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);
	if(contents_name.GetLength() == 0)
	{
		char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
		_splitpath(dlg.GetPathName(), drive, path, filename, ext);

		m_editContentsName.SetWindowText(filename);
	}

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(m_ulFileSize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
	}
	m_strFileMD5 = CString(szMd5);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CSMSContentsDialog::OnBnClickedButtonContentsCommentAdd()
{
//	if(m_listctrlContentsComment.GetItemCount() < TICKER_COUNT)
//	{
//		int index = m_listctrlContentsComment.GetItemCount();
//		m_listctrlContentsComment.SetFocus();
//		m_listctrlContentsComment.InsertItem(index, "");
//		m_listctrlContentsComment.EditLabel(index);
//	}
}

void CSMSContentsDialog::OnBnClickedButtonContentsCommentDelete()
{
//	int count = m_listctrlContentsComment.GetItemCount();
//	for(int i=0; i<count; i++)
//	{
//		if(m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED))
//		{
//			m_listctrlContentsComment.DeleteItem(i);
//			count--;
//			i--;
//		}
//	}
}

void CSMSContentsDialog::OnBnClickedButtonContentsCommentUp()
{
//	for(int i=1; i<m_listctrlContentsComment.GetItemCount(); i++)
//	{
//		UINT state = m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED);
//		if(state == LVIS_SELECTED)
//		{
//			MoveItem(i, i-1);
//		}
//	}
}

void CSMSContentsDialog::OnBnClickedButtonContentsCommentDown()
{
//	for(int i=m_listctrlContentsComment.GetItemCount()-2; i>=0; i--)
//	{
//		UINT state = m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED);
//		if(state == LVIS_SELECTED)
//		{
//			MoveItem(i, i+1);
//		}
//	}
}

void CSMSContentsDialog::OnBnClickedButtonPreviewPlay()
{
	SetPlayContents();

	if( m_wndSMS.m_bGridMode )
	{
		//m_wndSMSPopup.CreatePopup((CWnd*)this);
		//	::AfxMessageBox("fail !!!");
		//m_wndSMSPopup.ShowWindow(SW_SHOW);
		//m_wndSMSPopup.Play();
		//m_wndSMSPopup.Invalidate();
		CPreviewDlg dlg(&m_wndSMS);
		dlg.DoModal();
		return;
	}
	m_wndSMS.Play();
	m_wndSMS.Invalidate();
}

void CSMSContentsDialog::SetPlayContents()
{
	UpdateData();
	m_wndSMS.CloseFile();

	//m_editContentsFileName.GetWindowText(m_wndSMS.m_strMediaFullPath);
	m_wndSMS.m_strMediaFullPath = m_strLocation;

	CString str_font;
	m_cbxContentsFont.GetLBText(m_cbxContentsFont.GetCurSel(), str_font);

	COLORREF text_color = m_cbxContentsTextColor.GetSelectedColorValue();
	COLORREF bg_color = m_cbxContentsBGColor.GetSelectedColorValue();

	m_wndSMS.m_font = str_font;
	m_wndSMS.m_fontSize = m_editContentsFontSize.GetValueInt();
	m_wndSMS.m_fgColor = ::GetColorFromString(text_color);
	m_wndSMS.m_bgColor = ::GetColorFromString(bg_color);
	m_wndSMS.m_rgbFgColor = text_color;
	m_wndSMS.m_rgbBgColor = bg_color;
	m_wndSMS.m_align = m_nAlign;
	m_wndSMS.m_strSMS = "";

	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);
	for(int i=0 ; i<TICKER_COUNT ; i++)
	{
		m_wndSMS.m_comment[i] = "";
		if( i < pContents->GetLineCount() )
		{
			char szLine[255+1] = {0};
			pContents->GetLine(i, szLine, 255);
			m_wndSMS.m_comment[i] = szLine;

			if(i>0) m_wndSMS.m_strSMS += "\r\n";
			m_wndSMS.m_strSMS += szLine;
		}
	}

	m_wndSMS.CreateFile();
	m_wndSMS.OpenFile(0);
}

void CSMSContentsDialog::MoveItem(int nItem, int nItemNewPos)
{
	//int count = m_listctrlContentsComment.GetItemCount();

	//if( nItem == nItemNewPos ) return;
	//if( nItem < 0 || nItemNewPos < 0 ) return;
	//if( count <= nItem || count <= nItemNewPos) return;

	//CString str = m_listctrlContentsComment.GetItemText(nItem, 0);
	//UINT old_state = m_listctrlContentsComment.GetItemState(nItem, 0xffff);

	//m_listctrlContentsComment.DeleteItem(nItem);

	//m_listctrlContentsComment.InsertItem(nItemNewPos, str);
	//m_listctrlContentsComment.SetItemState(nItemNewPos, old_state, 0xffff);
}

void CSMSContentsDialog::OnBnClickedPermanentCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//24시간을 설정
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText("");
		m_editContentsPlaySecond.SetWindowText("15");

		m_editContentsPlayMinute.EnableWindow(TRUE);
		m_editContentsPlaySecond.EnableWindow(TRUE);
	}//if
}

void CSMSContentsDialog::OnBnClickedAlign1()
{
	m_nAlign = 1;
	for(int i = 0; i < 9; i++)
		m_chkAlign[i].SetCheck(false);
	m_chkAlign[0].SetCheck(true);
	UpdateData(false);
}

void CSMSContentsDialog::OnBnClickedAlign2()
{
	m_nAlign = 2;
	for(int i = 0; i < 9; i++)
		m_chkAlign[i].SetCheck(false);
	m_chkAlign[1].SetCheck(true);
	UpdateData(false);
}

void CSMSContentsDialog::OnBnClickedAlign3()
{
	m_nAlign = 3;
	for(int i = 0; i < 9; i++)
		m_chkAlign[i].SetCheck(false);
	m_chkAlign[2].SetCheck(true);
	UpdateData(false);
}

void CSMSContentsDialog::OnBnClickedAlign4()
{
	m_nAlign = 4;
	for(int i = 0; i < 9; i++)
		m_chkAlign[i].SetCheck(false);
	m_chkAlign[3].SetCheck(true);
	UpdateData(false);
}

void CSMSContentsDialog::OnBnClickedAlign5()
{
	m_nAlign = 5;
	for(int i = 0; i < 9; i++)
		m_chkAlign[i].SetCheck(false);
	m_chkAlign[4].SetCheck(true);
	UpdateData(false);
}

void CSMSContentsDialog::OnBnClickedAlign6()
{
	m_nAlign = 6;
	for(int i = 0; i < 9; i++)
		m_chkAlign[i].SetCheck(false);
	m_chkAlign[5].SetCheck(true);
	UpdateData(false);
}

void CSMSContentsDialog::OnBnClickedAlign7()
{
	m_nAlign = 7;
	for(int i = 0; i < 9; i++)
		m_chkAlign[i].SetCheck(false);
	m_chkAlign[6].SetCheck(true);
	UpdateData(false);
}

void CSMSContentsDialog::OnBnClickedAlign8()
{
	m_nAlign = 8;
	for(int i = 0; i < 9; i++)
		m_chkAlign[i].SetCheck(false);
	m_chkAlign[7].SetCheck(true);
	UpdateData(false);
}

void CSMSContentsDialog::OnBnClickedAlign9()
{
	m_nAlign = 9;
	for(int i = 0; i < 9; i++)
		m_chkAlign[i].SetCheck(false);
	m_chkAlign[8].SetCheck(true);
	UpdateData(false);
}

void CSMSContentsDialog::OnBnClickedDeletefile()
{
	m_ulFileSize = 0;
	m_editContentsFileName.SetWindowText(_T(""));
	m_staticContentsFileSize.SetWindowText(_T(""));
	m_strLocation = _T("");
	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	m_strFileMD5 = _T("");
}

BOOL CSMSContentsDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CSMSContentsDialog::OnEnChangeEditContentsComment()
{
	// 10개로 제한.
	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);

	DWORD dwSel = pContents->GetSel();
	int nScroll = pContents->GetScrollPos(SB_HORZ);

	CString strContents;
	for(int i=0; i<pContents->GetLineCount() ;i++)
	{
		if(i!=0) strContents += "\r\n";

		char szBuf[255+1] = {0};
		pContents->GetLine(i, szBuf, 255);

		//scratchUtil::getInstance()->hasDoubleByte(strBuf.Mid(255))

		strContents += szBuf;
	}

	pContents->SetWindowText(strContents);
	pContents->SetSel(dwSel, FALSE);
//	pContents->SetFocus();

	//
	if( strContents.Left(5) == "<WxH=" )
	{
		m_staticCommentTextColor.ShowWindow(SW_SHOW);
		m_cbxCommentTextColor.ShowWindow(SW_SHOW);
	}
	else
	{
		m_staticCommentTextColor.ShowWindow(SW_HIDE);
		m_cbxCommentTextColor.ShowWindow(SW_HIDE);
	}
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 콘텐츠 미리보기에서 ZoomIn(확대) 기능을 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 더 이상 확대할 수 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CSMSContentsDialog::ZoomIn()
{
	if(m_wndSMS.m_fontSize >= 1000)
	{
		return false;
	}//if

	m_wndSMS.Stop();
	m_wndSMS.m_fontSize = m_wndSMS.m_fontSize + 2;
	m_wndSMS.CreateFile();
	m_wndSMS.OpenFile(0);

	m_wndSMS.Play();
	m_wndSMS.Invalidate();

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 콘텐츠 미리보기에서 ZoomOut(축소) 기능을 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 더 이상 확대할 수 없음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CSMSContentsDialog::ZoomOut()
{
	if(m_wndSMS.m_fontSize <= 2)
	{
		return false;
	}//if

	m_wndSMS.Stop();
	m_wndSMS.m_fontSize = m_wndSMS.m_fontSize - 2;
	m_wndSMS.CreateFile();
	m_wndSMS.OpenFile(0);

	m_wndSMS.Play();
	m_wndSMS.Invalidate();

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 콘텐츠 미리보기에서 ZoomOut(축소)/ZoomIn(확대) 기능을 수행하지않은 원래상태로 복귀한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CSMSContentsDialog::RestoreZoom()
{
	m_wndSMS.Stop();
	m_wndSMS.m_fontSize = m_editContentsFontSize.GetValueInt();
	m_wndSMS.CreateFile();
	m_wndSMS.OpenFile(0);

	m_wndSMS.Play();
	m_wndSMS.Invalidate();
}

void CSMSContentsDialog::OnBnClickedButtonZoomIn()
{
	ZoomIn();
}

void CSMSContentsDialog::OnBnClickedButtonZoomOut()
{
	ZoomOut();
}

void CSMSContentsDialog::OnBnClickedButtonOriginal()
{
	RestoreZoom();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨트롤의 사용가능여부 설정 \n
/// @param (BOOL) bEnable : (in) TRUE : 사용가능, FALSE : 사용 불가
/////////////////////////////////////////////////////////////////////////////////
void CSMSContentsDialog::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editContentsFileName.EnableWindow(bEnable);
	m_btnBrowserContentsFile.EnableWindow(bEnable);
	m_btnDelFile.EnableWindow(bEnable);

	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	GetDlgItem(IDC_PERMANENT_CHECK)->EnableWindow(FALSE);

	m_cbxContentsFont.EnableWindow(bEnable);
	m_editContentsFontSize.EnableWindow(bEnable);
	m_cbxContentsTextColor.EnableWindow(bEnable);
	m_cbxContentsBGColor.EnableWindow(bEnable);

	for(int i = 0; i < 9; i++)
	{
		m_chkAlign[i].EnableWindow(bEnable);
	}

	((CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT))->SetReadOnly(TRUE);
}


// CPreviewDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPreviewDlg, CDialog)

CPreviewDlg::CPreviewDlg(CSMSSchedule* sms, CWnd* pParent /*=NULL*/)
	: CDialog(CPreviewDlg::IDD, pParent)
	, m_pSMS (sms)
	, m_wndSMS (NULL, 0, false)
{
}

CPreviewDlg::~CPreviewDlg()
{
}

void CPreviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPreviewDlg, CDialog)
END_MESSAGE_MAP()

// CPreviewDlg 메시지 처리기입니다.

BOOL CPreviewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	int length = m_pSMS->m_arrSMS.GetCount();

	CRect client_rect;
	//GetClientRect(client_rect);
	int screen_cx = GetSystemMetrics(SM_CXSCREEN);
	double grid_width = (double)screen_cx / (double)length;
	double grid_height = (double)m_pSMS->m_nGridHeight * grid_width / (double)m_pSMS->m_nGridWidth;

	client_rect.SetRect(0, 0, screen_cx, (int)grid_height);

	m_wndSMS.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndSMS.ShowWindow(SW_SHOW);

	client_rect.right += GetSystemMetrics(SM_CXDLGFRAME)*2;
	client_rect.bottom += GetSystemMetrics(SM_CYDLGFRAME)*2 + GetSystemMetrics(SM_CYCAPTION);// + 10;
	MoveWindow(client_rect);
	this->CenterWindow();

	//
	m_wndSMS.m_strMediaFullPath = m_pSMS->m_strMediaFullPath;
	m_wndSMS.m_font = m_pSMS->m_font;
	m_wndSMS.m_fontSize = m_pSMS->m_fontSize;
	m_wndSMS.m_fgColor = m_pSMS->m_fgColor;
	m_wndSMS.m_bgColor = m_pSMS->m_bgColor;
	m_wndSMS.m_rgbFgColor = m_pSMS->m_rgbFgColor;
	m_wndSMS.m_rgbBgColor = m_pSMS->m_rgbBgColor;
	m_wndSMS.m_align = m_pSMS->m_align;
	m_wndSMS.m_strSMS = m_pSMS->m_strSMS;

	for(int i=0 ; i<TICKER_COUNT ; i++)
	{
		m_wndSMS.m_comment[i] = m_pSMS->m_comment[i];
	}

	m_wndSMS.CreateFile();
	m_wndSMS.OpenFile(0);

	m_wndSMS.Play();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSMSContentsDialog::OnCbnSelchangeComboCommentTextColor()
{
	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);

	int start, end;
	pContents->GetSel(start, end);
	if( start==end )
	{
		m_cbxCommentTextColor.SetCurSel(-1);
		return;
	}

	CString comment;
	pContents->GetWindowText(comment);

	USES_CONVERSION;
	CStringW wcomment = A2W( (LPCSTR)comment ); // ANSI to UCS-2

	CStringW wselect_comment = wcomment.Mid(start, end-start);
	CString select_comment = W2A((LPCWSTR)wselect_comment);

	COLORREF color = m_cbxCommentTextColor.GetSelectedColorValue();
    int red = color & 0x0000ff;
    int green = ( color & 0x00ff00 ) >> 8;
    int blue = ( color & 0xff0000 ) >> 16;

	CString new_comment;
	new_comment.Format("<RGB=%d,%d,%d>%s</RGB>", red, green, blue, select_comment);

	pContents->ReplaceSel(new_comment);
	m_cbxCommentTextColor.SetCurSel(-1);
}
