#pragma once

#include "afxwin.h"
#include "afxcmn.h"

#include "CyclePlayContentsDialog.h"
#include "TimeBasePlayContentsDialog.h"

//#include "XPTabCtrl.h"
#include "common/HoverButton.h"
#include "common/UBCTabCtrl.h"

///////////////////////////////////////////////////////////////////////////
// CPlayContentsDialog 대화 상자입니다.

class CUBCStudioDoc;
class CContentsListCtrl;

class CPlayContentsDialog : public CSubPlayContentsDialog
{
	DECLARE_DYNAMIC(CPlayContentsDialog)

public:
	CPlayContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPlayContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PLAYCONTENTS};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	bool	m_bEditContents;
	CUBCStudioDoc*	m_pDocument;
	CContentsListCtrl*	m_pContentsListCtrl;

	PLAYCONTENTS_INFO_LIST	m_listPlayContentsInfo;
	CString				m_strContentsId;
	void				RemoveAllPlayContentsList();
	int					m_bCyclePlayContents;

public:

	virtual bool	GetPlayContentsInfo(PLAYCONTENTS_INFO& info, int nIndex);
	virtual bool	SetPlayContentsInfo(PLAYCONTENTS_INFO& info);
	virtual int		GetPlayContentsCount();
	virtual bool    InputErrorCheck();

	afx_msg void OnTcnSelchangeTabContentsTime(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonSelectContents();
	afx_msg void OnBnClickedButtonEditContents();

	/*CTabCtrl	m_tabContentsTime;
	CTabCtrl	m_tabContents;*/
	CStatic		m_staticContentsType;
	CStatic		m_staticContentsName;
	CStatic		m_staticContentsRunningTime;
	CStatic		m_staticContentsFileName;

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	//CButton		m_btnOK;
	CHoverButton		m_btnOK;
	CHoverButton		m_btnCancel;
	CHoverButton		m_btnSelectContents;
	CHoverButton		m_btnEditContents;
//	CXPTabCtrl			m_tabContentsTime;
//	CXPTabCtrl			m_tabContents;
	CUBCTabCtrl			m_tabContentsTime;
	CUBCTabCtrl			m_tabContents;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	CCyclePlayContentsDialog		m_wndCyclePlayContents;
	CTimeBasePlayContentsDialog		m_wndTimeBasePlayContents;

	void	SetEditMode(bool bEditContents=true) { m_bEditContents = bEditContents; };
	void	SetDocument(CUBCStudioDoc* pDoc) { m_pDocument=pDoc; };
	void	SetContentsListCtrl(CContentsListCtrl* pListCtrl) { m_pContentsListCtrl=pListCtrl; };
	bool	RefreshContentsInfo();
	void	SetCyclePlayContentsMode(bool bCyclePlayContents) { m_bCyclePlayContents = bCyclePlayContents; };
};
