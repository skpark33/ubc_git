// TemplatePropertyDialog.cpp : 구현 파일입니다.
//
#include "stdafx.h"
#include "UBCStudio.h"
#include "TemplatePropertyDialog.h"
#include "Enviroment.h"

// CTemplatePropertyDialog 대화 상자입니다.
IMPLEMENT_DYNAMIC(CTemplatePropertyDialog, CDialog)

CTemplatePropertyDialog::CTemplatePropertyDialog(CWnd* pParent /*=NULL*/)
:	CDialog(CTemplatePropertyDialog::IDD, pParent)
,	m_bNowSetting (true)
{
	m_brushBG.CreateSolidBrush(RGB(255,255,255));
}

CTemplatePropertyDialog::~CTemplatePropertyDialog()
{
}

void CTemplatePropertyDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_TEMPLATE_ID, m_editTemplateID);
	DDX_Control(pDX, IDC_EDIT_WIDTH, m_editWidth);
	DDX_Control(pDX, IDC_EDIT_HEIGHT, m_editHeight);
	DDX_Control(pDX, IDC_SPIN_WIDTH, m_spinWidth);
	DDX_Control(pDX, IDC_SPIN_HEIGHT, m_spinHeight);
	DDX_Control(pDX, IDC_COMBO_BG_COLOR, m_cbxBGColor);
	DDX_Control(pDX, IDC_EDIT_DESCRIPTION, m_editDescription);
	DDX_Control(pDX, IDC_EDIT_SHORTCUT, m_edtShortcut);
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	DDX_Control(pDX, IDC_BUTTON_BG_IMAGE, m_btnBGImg);
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	DDX_Control(pDX, IDC_EDIT_BG_IMAGE, m_edBGImage);
	DDX_Control(pDX, IDC_STATIC_BGIMAGE, m_stBGImage);
	DDX_Control(pDX, IDC_COMBO_BGTYPE, m_cbBGType);
	DDX_Control(pDX, IDC_STATIC_BGTYPE, m_stBGType);
}


BEGIN_MESSAGE_MAP(CTemplatePropertyDialog, CDialog)
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE(IDC_EDIT_WIDTH, &CTemplatePropertyDialog::OnEnChangeEdit)
	ON_EN_CHANGE(IDC_EDIT_HEIGHT, &CTemplatePropertyDialog::OnEnChangeEdit)
	ON_CBN_SELCHANGE(IDC_COMBO_BG_COLOR, &CTemplatePropertyDialog::OnEnChangeEdit)
	ON_EN_CHANGE(IDC_EDIT_BG_IMAGE, &CTemplatePropertyDialog::OnEnChangeEdit)
	ON_EN_CHANGE(IDC_EDIT_SHORTCUT, &CTemplatePropertyDialog::OnEnChangeEdit)
	ON_BN_CLICKED(IDC_BUTTON_BG_IMAGE, &CTemplatePropertyDialog::OnBnClickedButtonBgImage)
END_MESSAGE_MAP()


// CTemplatePropertyDialog 메시지 처리기입니다.

BOOL CTemplatePropertyDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnBGImg.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btnBGImg.SetToolTipText(LoadStringById(IDS_TEMPLATEPROPERTYDIALOG_BUT001));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	m_listNoCTLWnd.Add(&m_editTemplateID);
	m_listNoCTLWnd.Add(&m_editWidth);
	m_listNoCTLWnd.Add(&m_editHeight);
	m_listNoCTLWnd.Add(&m_spinWidth);
	m_listNoCTLWnd.Add(&m_spinHeight);
	m_listNoCTLWnd.Add(&m_cbxBGColor);
	m_listNoCTLWnd.Add(&m_edBGImage);
	m_listNoCTLWnd.Add(&m_editDescription);

	m_spinWidth.SetRange32(0, 10000);
	m_spinHeight.SetRange32(0, 10000);

	m_cbxBGColor.InitializeDefaultColors();
	m_cbxBGColor.SetSelectedColorValue(RGB(0,0,0));

	SetTemplateInfo(NULL);

	m_bNowSetting = false;

	if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType){
		m_edBGImage.ShowWindow(SW_SHOW);
		m_stBGImage.ShowWindow(SW_SHOW);
		m_cbBGType.ShowWindow(SW_SHOW);
		m_stBGType.ShowWindow(SW_SHOW);
		m_btnBGImg.ShowWindow(SW_SHOW);

		m_cbBGType.AddString("Full screen");
		m_cbBGType.AddString("Tile");
		m_cbBGType.AddString("Original");
		m_cbBGType.SetCurSel(0);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CTemplatePropertyDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();
}

void CTemplatePropertyDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnCancel();
}

HBRUSH CTemplatePropertyDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	int count = m_listNoCTLWnd.GetCount();
	for(int i=0; i<count; i++)
	{
		CWnd* wnd = m_listNoCTLWnd.GetAt(i);
		if(wnd->GetSafeHwnd() == pWnd->GetSafeHwnd())
			return hbr;
	}

	pDC->SetBkColor(RGB(255,255,255));
	//pDC->SetBkMode(TRANSPARENT);
	hbr = (HBRUSH)m_brushBG;

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CTemplatePropertyDialog::OnEnChangeEdit()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(m_bNowSetting == false)
	{
		CWnd* wnd = GetParent();
		if(wnd->GetSafeHwnd())
			wnd->SendMessage(WM_TEMPLATE_INFO_CHANGED);
	}
}

void CTemplatePropertyDialog::OnBnClickedButtonBgImage()
{
	CString szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
	CString filter;
	filter.Format("All Image Files|%s|"
				  "Bitmap Files (*.bmp)|*.bmp|"
				  "JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;|"
				  "GIF Files (*.gif)|*.gif|"
				  "PCX Files (*.pcx)|*.pcx|"
				  "PNG Files (*.png)|*.png|"
				  "TIFF Files (*.tif)|*.tif;*.tiff||"
				 , szFileExts
				 );

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	CString strFullpath = dlg.GetPathName();

	CFileFind ff;
	if(!ff.FindFile(strFullpath))
		return;
	ff.Close();

	TCHAR szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(strFullpath, NULL, NULL, szFilename, szExt);

	CString szDesFile;
	szDesFile.Format("%s\\%s%s%s", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, szFilename, szExt);

	if(!ShellCopyFile(strFullpath, szDesFile, FALSE)) return;

	szDesFile.Format("%s%s", szFilename, szExt);
	m_edBGImage.SetWindowText(szDesFile);

	if(m_bNowSetting == false)
	{
		CWnd* wnd = GetParent();
		if(wnd->GetSafeHwnd())
			wnd->SendMessage(WM_TEMPLATE_INFO_CHANGED);
	}
}

void CTemplatePropertyDialog::SetTemplateInfo(TEMPLATE_LINK* pTemplateLink)
{
	BOOL enable_window;
	m_bNowSetting = true;
	if(pTemplateLink != NULL)
	{
		enable_window = TRUE;

		m_editTemplateID.SetWindowText(pTemplateLink->pTemplateInfo->strId);
		m_editWidth.SetValue(pTemplateLink->pTemplateInfo->rcRect.Width());
		m_editHeight.SetValue(pTemplateLink->pTemplateInfo->rcRect.Height());
		m_cbxBGColor.SetSelectedColorValue(pTemplateLink->pTemplateInfo->crBgColor);
		m_edBGImage.SetWindowText(pTemplateLink->pTemplateInfo->strBgImage);
		m_editDescription.SetWindowText("");
		m_edtShortcut.SetKeyValue(pTemplateLink->pTemplateInfo->strShortCut);
		m_cbBGType.SetCurSel(pTemplateLink->pTemplateInfo->nBgType);
	}
	else
	{
		enable_window = FALSE;

		m_editTemplateID.SetWindowText("");
		m_editWidth.SetWindowText("");
		m_editHeight.SetWindowText("");
		m_cbxBGColor.SetCurSel(-1);;
		m_edBGImage.SetWindowText("");
		m_editDescription.SetWindowText("");
		m_edtShortcut.SetKeyValue(0, 0);
		m_cbBGType.SetCurSel(0);
	}

	m_editTemplateID.EnableWindow(enable_window);
	m_editWidth.EnableWindow(enable_window);
	m_editHeight.EnableWindow(enable_window);
	m_cbxBGColor.EnableWindow(enable_window);;
	m_edBGImage.EnableWindow(enable_window);
	m_editDescription.EnableWindow(enable_window);
	m_edBGImage.SetReadOnly();

	Invalidate(FALSE);

	m_bNowSetting = false;
}

bool CTemplatePropertyDialog::GetTemplateInfo(TEMPLATE_LINK* pTemplateLink)
{
	if(pTemplateLink != NULL)
	{
		m_editTemplateID.GetWindowText(pTemplateLink->pTemplateInfo->strId);
		pTemplateLink->pTemplateInfo->rcRect.right = m_editWidth.GetValueInt();
		pTemplateLink->pTemplateInfo->rcRect.bottom = m_editHeight.GetValueInt();
		pTemplateLink->pTemplateInfo->crBgColor = m_cbxBGColor.GetSelectedColorValue();
		m_edBGImage.GetWindowText(pTemplateLink->pTemplateInfo->strBgImage);
		m_editDescription.GetWindowText(pTemplateLink->pTemplateInfo->strDescription);
		m_edtShortcut.GetWindowText(pTemplateLink->pTemplateInfo->strShortCut);
		pTemplateLink->pTemplateInfo->nBgType = m_cbBGType.GetCurSel();

		return true;
	}
	return false;
}
