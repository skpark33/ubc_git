#pragma once
#include "afxwin.h"
#include "afxdtctl.h"


// CTimeSelect dialog

class CTimeSelect : public CDialog
{
	DECLARE_DYNAMIC(CTimeSelect)

public:
	CTimeSelect(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimeSelect();
	CString m_szStartDT;
	CString m_szEndDT;

// Dialog Data
	enum { IDD = IDD_TIMESELECT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CButton m_ckStart;
	CButton m_ckEnd;
	CDateTimeCtrl m_tmDate1;
	CDateTimeCtrl m_tmTime1;
	CDateTimeCtrl m_tmDate2;
	CDateTimeCtrl m_tmTime2;

	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedCheckStart();
	afx_msg void OnBnClickedCheckEnd();
	afx_msg void OnBnClickedOk();
};
