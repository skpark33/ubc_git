#pragma once
#include "afxwin.h"


// CLoginInfoDlg dialog

class CLoginInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CLoginInfoDlg)

public:
	CLoginInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLoginInfoDlg();

// Dialog Data
	enum { IDD = IDD_LOGIN_INFO };

	CString m_szSite;
	CString m_szUser;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
};
