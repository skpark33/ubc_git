// FtpUploadDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "FtpUploadDlg.h"
#include "MainFrm.h"
#include "Dbghelp.h"
#include "shlwapi.h"
#include "Enviroment.h"
#include "common\libscratch\scratchUtil.h"

#define	BUF_SIZE		(1024*1024)	// 파일 복사 버퍼 = 1 MByte
#define TM_START_FTP	1020
// CFtpUploadDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFtpUploadDlg, CDialog)

////////////////////////////////////
// Studio PE 에서 사용하는 생성자...
CFtpUploadDlg::CFtpUploadDlg(CWnd* pParent, CPtrArray& aryHostInfo, CString strArgs, bool bA, bool bB, bool bAll)
	: CDialog(CFtpUploadDlg::IDD, pParent)
,	m_pParentWnd (pParent)
,	m_ulTotalSize (0)
,	m_pThread (NULL)
,	m_bProcessThread (true)
,	m_nIndexHostInfo(0)
,	m_strPackageFilePath("")
,	m_strPackageName("")
,	m_pFtpConnection(NULL)
,	m_strArgs("")
{
	ASSERT(GetEnvPtr()->m_Edition == CEnviroment::eStudioPE);

	m_bAll = bAll;
	m_bUpload = TRUE;
	m_bReserve = FALSE;

	m_pClient = NULL;
	m_bForceStop = FALSE;

	CNetDeviceInfo* pclsNetInfo = NULL;
	CNetDeviceInfo* pclsNewInfo = NULL;
	
	for(int i=0; i<aryHostInfo.GetCount(); i++)
	{
		pclsNetInfo = (CNetDeviceInfo*)aryHostInfo.GetAt(i);
		pclsNewInfo = new CNetDeviceInfo();
		pclsNewInfo->m_nFTPPort			= pclsNetInfo->m_nFTPPort;
		pclsNewInfo->m_nSvrPort			= pclsNetInfo->m_nSvrPort;
		pclsNewInfo->m_strDeviceName	= pclsNetInfo->m_strDeviceName;
		pclsNewInfo->m_strID			= pclsNetInfo->m_strID;
		pclsNewInfo->m_strIP			= pclsNetInfo->m_strIP;
		pclsNewInfo->m_strPWD			= pclsNetInfo->m_strPWD;
		m_aryHostInfo.Add(pclsNewInfo);
	}//for

	m_strArgs = strArgs;
	m_DisplalySide[0] = bA;
	m_DisplalySide[1] = bB;
}

////////////////////////////////////
// Studio EE 에서 사용하는 생성자...
CFtpUploadDlg::CFtpUploadDlg(bool bUpload, bool bReserve, CWnd* pParent) : 
	CDialog(CFtpUploadDlg::IDD, pParent)
,	m_pParentWnd (pParent)
,	m_ulTotalSize (0)
,	m_pThread (NULL)
,	m_bProcessThread (true)
,	m_nIndexHostInfo(0)
,	m_strPackageFilePath("")
,	m_strPackageName("")
,	m_pFtpConnection(NULL)
,	m_strArgs("")
{
	ASSERT(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE);

	m_bAll = true;
	m_bUpload = bUpload;
	m_bReserve = bReserve;

	m_pClient = NULL;
	m_bForceStop = FALSE;

	CNetDeviceInfo* pclsNetInfo = NULL;
	CNetDeviceInfo* pclsNewInfo = NULL;

	m_strArgs.Empty();
}

CFtpUploadDlg::~CFtpUploadDlg()
{
	if(m_pFtpConnection)
	{
		m_pFtpConnection->Close();
		delete m_pFtpConnection;
		m_pFtpConnection = NULL;
	}//if

	m_mapContents.RemoveAll();
	ClearNetDeviceArray();
}

void CFtpUploadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_CURRENT, m_pbarCurrent);
	DDX_Control(pDX, IDC_PROGRESS_TOTAL, m_pbarTotal);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_staticStatus);
	DDX_Control(pDX, IDC_BUTTON_CANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_STATIC_HOST, m_staticHost);
}


BEGIN_MESSAGE_MAP(CFtpUploadDlg, CDialog)
	ON_WM_TIMER()
	ON_MESSAGE(WM_COMPLETE_FTP_CONNECTION, OnCompleteFtpConnection)
	ON_MESSAGE(WM_COMPLETE_FTP_UPLOAD, OnCompleteFtpUpload)
//	ON_MESSAGE(WM_DISPLAY_STATUS, OnDisplayStatus)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CFtpUploadDlg::OnBnClickedButtonCancel)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CFtpUploadDlg 메시지 처리기입니다.
BOOL CFtpUploadDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
//	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
//	m_btnCancel.SetToolTipText("Cancel");
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	SetTimer(TM_START_FTP, 250, NULL);

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE){
//		m_btnCancel.EnableWindow(FALSE);
//		m_btnCancel.ShowWindow(SW_HIDE);

		m_pbarTotal.SetStyle(PROGRESS_TEXT);
		m_pbarTotal.SetRange(0, 1000);
		m_pbarTotal.SetPos(0);
		m_pbarTotal.SetText("");

		m_pbarCurrent.SetStyle(PROGRESS_TEXT);
		m_pbarCurrent.SetRange(0, 1000);
		m_pbarCurrent.SetPos(0);
		m_pbarCurrent.SetText("");
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFtpUploadDlg::OnClose()
{
	if(m_pThread){
//		::WaitForSingleObject(m_pThread->m_hThread, INFINITE);
		::WaitForSingleObject(m_pThread->m_hThread, 5000);
	}
	m_pThread = NULL;

//	if(!m_szErrorReporter.IsEmpty()){
//		UbcMessageBox(m_szErrorReporter, MB_ICONWARNING);
//	}

	CDialog::OnClose();
}

void CFtpUploadDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Network host 정보 배열을 정리한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CFtpUploadDlg::ClearNetDeviceArray()
{
	CNetDeviceInfo* pclsDevInfo = NULL;
	for(int i=0; i<m_aryHostInfo.GetCount(); i++)
	{
		pclsDevInfo = (CNetDeviceInfo*)m_aryHostInfo.GetAt(i);
		delete pclsDevInfo;
	}//for
	m_aryHostInfo.RemoveAll();
}

void CFtpUploadDlg::OnBnClickedButtonCancel()
{
	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE){
		if(UbcMessageBox(LoadStringById(IDS_FTPUPLOADDLG_MSG001), MB_YESNO) != IDYES)
			return;
		if(m_pThread){
			TerminateThread(m_pThread->m_hThread, -1);
		}
		PostMessage(WM_CLOSE, 0, 0);
	}
	else if(GetEnvPtr()->m_Edition == CEnviroment::eStudioPE){
		if(UbcMessageBox(LoadStringById(IDS_FTPUPLOADDLG_MSG001), MB_YESNO) != IDYES)
			return;
		if(m_pClient){
			m_bForceStop = TRUE;
			m_pClient->Stop();
		}
	}

//	CDialog::OnCancel();
}

void CFtpUploadDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == TM_START_FTP){
		KillTimer(TM_START_FTP);
		//Start(true);
		//////////////////////////////////////////////
		// 기존 FTP 구조를 Socket 전송 구조로 변경함 - 구현석
		if(GetEnvPtr()->m_Edition == CEnviroment::eStudioPE){
			RunFTP(); 
		}
		// Studio EE 관련 코드 추가 - FTP 로 송수신
		else if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE){
			if(m_bUpload){
				m_pThread = AfxBeginThread(UploadProc, (LPVOID)this);
			}else{
				m_pThread = AfxBeginThread(DownloadProc, (LPVOID)this);
			}

		}
		//////////////////////////////////////////////
	}
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Upload 과정을 시작한다 \n
/// @param (bool) bShowWindow : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CFtpUploadDlg::Start(bool bShowWindow)
{
	if(CDataContainer::getInstance()->GetContentsMap()->GetCount() == 0)
	{
		m_pParentWnd->PostMessage(WM_FTP_UPLOAD, FTP_ERROR_EMPTY_CONTENTS); // Empty contents
		//OnBnClickedButtonCancel();

		return;
	}//if

	//복사할 scheduel 파일 경로
	CMainFrame* pclsFrm = (CMainFrame*)m_pParentWnd;
	m_strPackageFilePath = pclsFrm->GetSourceDirive();
	m_strPackageFilePath.Append(UBC_CONFIG_PATH);
	m_strPackageName = pclsFrm->GetHostName();
	m_strPackageFilePath.Append(m_strPackageName);
	m_strPackageFilePath.Append(".ini");

	m_pbarTotal.SetStyle(PROGRESS_TEXT);
	m_pbarTotal.SetRange(0, 1000);
	m_pbarTotal.SetPos(0);
	m_pbarTotal.SetText("");

	m_pbarCurrent.SetStyle(PROGRESS_TEXT);
	m_pbarCurrent.SetRange(0, 1000);
	m_pbarCurrent.SetPos(0);
	m_pbarCurrent.SetText("");

	if(bShowWindow)
	{
		CenterWindow();
		ShowWindow(SW_SHOW);
	}

	m_staticStatus.SetWindowText("Initializing uploading...");
	m_ulTotalSize = GetTotalFileSize();
	m_aryFailHostInfo.RemoveAll();

	//연결시도...
	CNetDeviceInfo* pclsNetInfo = NULL;
	while(m_nIndexHostInfo < m_aryHostInfo.GetCount())
	{
		if(ConnectFtp(m_nIndexHostInfo)){
			return ;
		}//if

		pclsNetInfo = (CNetDeviceInfo*)m_aryHostInfo.GetAt(m_nIndexHostInfo);
		pclsNetInfo->m_nErrorCode = FTP_ERROR_CONNECT_FAIL;
		MakeErrorMsg(pclsNetInfo, FTP_ERROR_CONNECT_FAIL);
		m_aryFailHostInfo.Add(pclsNetInfo);	//실패한 호스트

		//다음 호스트 연결 시도
		m_nIndexHostInfo++;		
	}//while

	m_pParentWnd->PostMessage(WM_FTP_UPLOAD, FTP_ERROR_CONNECT_FAIL);	//connect fail
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 인덱스의 호스트에 연결을 시도한다 \n
/// @param (int) nIndex : (in) 연결을 시도하는 호스트배열의 인덱스
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CFtpUploadDlg::ConnectFtp(int nIndex)
{
	if(nIndex >= m_aryHostInfo.GetCount()){
		return false;
	}//if

	if(m_pFtpConnection)
	{
		m_pFtpConnection->Close();
		delete m_pFtpConnection;
		m_pFtpConnection = NULL;
	}//if

	GET_FTP_CONNECTION_PARAM* param = new GET_FTP_CONNECTION_PARAM;
	if(param != NULL)
	{
		param->pParentWnd		= m_pParentWnd;
		param->pWnd				= this;
		param->pInternetSession	= &m_internetSession;
		param->ppFtpConnection	= &(m_pFtpConnection);
		param->pHostInfo		= (CNetDeviceInfo*)m_aryHostInfo.GetAt(nIndex);

		CString strMsg;
		strMsg.Format("%s [%s]", param->pHostInfo->m_strDeviceName, param->pHostInfo->m_strIP);
		m_staticHost.SetWindowText(strMsg);
		m_staticStatus.SetWindowText("Trying to connect...");

		m_pbarCurrent.SetText("NOW - 00%% (00:00:00)");
		m_pbarCurrent.SetPos(0);

		m_pbarTotal.SetText("00%% (00:00:00)");
		m_pbarTotal.SetPos(0);

//		m_btnCancel.EnableWindow(FALSE);

		m_pThread = ::AfxBeginThread(CFtpUploadDlg::GetFtpConnectionThread, param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = TRUE;
		m_pThread->ResumeThread();

		return true;
	}//if

	return false;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FTP 연결의 결과 이벤트 처리 \n
/// @param (WPARAM) wParam : (in) 결과 코드값
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CFtpUploadDlg::OnCompleteFtpConnection(WPARAM wParam, LPARAM lParam)
{
	if(wParam == TRUE)
	{
		FTP_UPLOAD_PARAM* param = new FTP_UPLOAD_PARAM;
		if(param != NULL)
		{
			param->pParentWnd		= m_pParentWnd;
			param->pWnd				= this;
			param->pStatic			= &m_staticStatus;
			param->pbarCurrent		= &m_pbarCurrent;
			param->pbarTotal		= &m_pbarTotal;
			param->pFtpConnection	= m_pFtpConnection;
			param->ulTotalSize		= m_ulTotalSize;
			//param->mapContents		= &m_mapContents;

			m_pThread = ::AfxBeginThread(CFtpUploadDlg::FtpUploadThread, param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
			m_pThread->m_bAutoDelete = TRUE;
			m_pThread->ResumeThread();
		}//if
	}
	else
	{
		//실패한 호스트 정보를 넣어준다.
		CNetDeviceInfo* pclsNetInfo = NULL;
		pclsNetInfo = (CNetDeviceInfo*)m_aryHostInfo.GetAt(m_nIndexHostInfo);
		pclsNetInfo->m_nErrorCode = FTP_ERROR_CONNECT_FAIL;
		MakeErrorMsg(pclsNetInfo, FTP_ERROR_CONNECT_FAIL);
		m_aryFailHostInfo.Add(pclsNetInfo);	//실패한 호스트

		//다음 연결 시도
		m_nIndexHostInfo++;
		while(m_nIndexHostInfo < m_aryHostInfo.GetCount())
		{
			if(ConnectFtp(m_nIndexHostInfo)){
				return 0;
			}//if

			pclsNetInfo = (CNetDeviceInfo*)m_aryHostInfo.GetAt(m_nIndexHostInfo);
			pclsNetInfo->m_nErrorCode = FTP_ERROR_CONNECT_FAIL;
			MakeErrorMsg(pclsNetInfo, FTP_ERROR_CONNECT_FAIL);
			m_aryFailHostInfo.Add(pclsNetInfo);	//실패한 호스트

			m_nIndexHostInfo++;
		}//while

		m_pParentWnd->PostMessage(WM_FTP_UPLOAD, FTP_ERROR_CONNECT_FAIL);	//connect fail
	}//if

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// upload하려는 contents파일의 총 크기를 계산한다 \n
/// @return <형: ULONGLONG> \n
///			upload하는 contents 파일들 사이즈의 총합 \n
/////////////////////////////////////////////////////////////////////////////////
ULONGLONG CFtpUploadDlg::GetTotalFileSize()
{
	ULONGLONG total_size = 0;

	CONTENTS_INFO_MAP* contents_map = CDataContainer::getInstance()->GetContentsMap();

	//중복된 콘텐츠 파일 크기계산 방지
	CMapStringToString mapContents;
	CONTENTS_INFO* stContents = NULL;

	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		CString file_fullpath;
		file_fullpath.Format("%s%s", pContentsInfo->strLocalLocation, pContentsInfo->strFilename);

		//중복 방지
		if(m_mapContents.Lookup(file_fullpath, (void*&)stContents)){
			continue;
		}
		else if(!m_bAll && !CDataContainer::getInstance()->IsExistPlayContentsUsingContents(strContentsId)){
			continue;
		}
		else{
			m_mapContents.SetAt(file_fullpath, pContentsInfo);
		}

		CFileStatus fs;
//		if(CFile::GetStatus(file_fullpath, fs))
		if(CEnviroment::GetFileSize(file_fullpath, fs.m_size))
		{
			pContentsInfo->nFilesize = fs.m_size;
			total_size += fs.m_size;
		}
	}

	return total_size;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FTP upload 결과 이벤트 처리 \n
/// @param (WPARAM) wParam : (in) 결과 코드값
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CFtpUploadDlg::OnCompleteFtpUpload(WPARAM wParam, LPARAM lParam)
{
	int nRet = (int)wParam;
	CNetDeviceInfo* pclsNetInfo = NULL;
	pclsNetInfo = (CNetDeviceInfo*)m_aryHostInfo.GetAt(m_nIndexHostInfo);
	pclsNetInfo->m_nErrorCode = nRet;
	MakeErrorMsg(pclsNetInfo, nRet, lParam);
//	m_aryFailHostInfo.Add(pclsNetInfo);	//실패한 호스트

	//Upload가 실패하였을 경우 실패 결과를 넣어 준다.
	if(nRet == FTP_ERROR_OK)
	{
		/*pclsNetInfo->m_nErrorCode = nRet;
		MakeErrorMsg(pclsNetInfo, nRet);
		m_aryFailHostInfo.Add(pclsNetInfo);*/

		//성공했다면, host에 update 이벤트를 날린다
		scratchUtil* aUtil = scratchUtil::getInstance();
		if(!aUtil->socketAgent(pclsNetInfo->m_strIP, pclsNetInfo->m_nSvrPort, "import", m_strArgs))
		{
			pclsNetInfo->m_nErrorCode = FTP_ERROR_NOTIFY_UPDATE;
			MakeErrorMsg(pclsNetInfo, FTP_ERROR_NOTIFY_UPDATE);
			m_aryFailHostInfo.Add(pclsNetInfo);	//실패한 호스트
			nRet = FTP_ERROR_NOTIFY_UPDATE;
		}//if
	}//if

	//모든 호스트에 작업을 완료했다면
	if(m_nIndexHostInfo >= m_aryHostInfo.GetCount()-1)
	{
		m_pParentWnd->PostMessage(WM_FTP_UPLOAD, nRet);

		return 0;
	}//if

	//다음 연결 시도
	m_nIndexHostInfo++;
	while(m_nIndexHostInfo < m_aryHostInfo.GetCount())
	{
		if(ConnectFtp(m_nIndexHostInfo)){
			return 0;
		}//if

		pclsNetInfo = (CNetDeviceInfo*)m_aryHostInfo.GetAt(m_nIndexHostInfo);
		pclsNetInfo->m_nErrorCode = FTP_ERROR_CONNECT_FAIL;
		MakeErrorMsg(pclsNetInfo, FTP_ERROR_CONNECT_FAIL);
		m_aryFailHostInfo.Add(pclsNetInfo);	//실패한 호스트

		m_nIndexHostInfo++;
	}//while

	m_pParentWnd->PostMessage(WM_FTP_UPLOAD, FTP_ERROR_CONNECT_FAIL);	//connect fail
	
//	DestroyWindow();

	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////


UINT CFtpUploadDlg::GetFtpConnectionThread(LPVOID pParam)
{
	GET_FTP_CONNECTION_PARAM* param = (GET_FTP_CONNECTION_PARAM*)pParam;

	CString ServerAddr, ServerId, ServerPwd;
	int nPort;
	CNetDeviceInfo* pHostInfo = param->pHostInfo;
	ServerAddr	= pHostInfo->m_strIP;
	ServerId	= pHostInfo->m_strID;
	ServerPwd	= pHostInfo->m_strPWD;
	nPort		= pHostInfo->m_nFTPPort;

	try
	{
		CFtpConnection* connect = param->pInternetSession->GetFtpConnection(ServerAddr, ServerId, ServerPwd, nPort);
		*(param->ppFtpConnection) = connect;
		if(*(param->ppFtpConnection) != NULL)
		{
			param->pWnd->PostMessage(WM_COMPLETE_FTP_CONNECTION, TRUE);
			delete param;

			return 0;
		}
	}
	catch(CInternetException* ex)
	{
		CString msg;
		TCHAR szCause[1024];
		ex->GetErrorMessage(szCause, 1024);
		msg.Format("(%d) %s", ex->m_dwError, szCause);
		ex->Delete();
	}//try

	param->pWnd->PostMessage(WM_COMPLETE_FTP_CONNECTION, FALSE);
	delete param;

	return 0;
}


UINT CFtpUploadDlg::FtpUploadThread(LPVOID pParam)
{
	FTP_UPLOAD_PARAM* param = (FTP_UPLOAD_PARAM*)pParam;

	ULONGLONG nCompleteUploadSize = 0;
	ULONGLONG nCurrentUploadSize = 0;
	ULONGLONG nCurrentFileSize = 0;
	BYTE* buf = new BYTE[BUF_SIZE];
	//BYTE buf[BUF_SIZE] = {0x00};

	int nUploadCount = 0;

	int i=-1;
	CString strContentsID, strLocalPath;
	CONTENTS_INFO* pContentsInfo;
	CFtpUploadDlg* pDlg = (CFtpUploadDlg*)param->pWnd;
	CONTENTS_INFO_MAP* pmapContents = &(pDlg->m_mapContents);
	int count = pmapContents->GetCount();
	POSITION pos = pmapContents->GetStartPosition();

	while(pos != NULL)
	{
		i++;
		pmapContents->GetNextAssoc(pos, strContentsID, (void*&)pContentsInfo);
		strLocalPath = pContentsInfo->strLocalLocation;
		strLocalPath.Append(pContentsInfo->strFilename);

		nCompleteUploadSize += nCurrentFileSize;
		nCurrentFileSize = pContentsInfo->nFilesize;
		nCurrentUploadSize = 0;

		CString status;
		status.Format("%s (%d/%d)", pContentsInfo->strFilename, i+1, count);
		param->pStatic->SetWindowText(status);

		param->pbarCurrent->SetText("NOW - 00%% (00:00:00)");
//		param->pbarCurrent->SetPos(0);

		param->pbarTotal->SetText("00%% (00:00:00)");
	
		DWORD	dwStartTick = 0;
		DWORD	dwEndTick = 0;
/*
		if(pDlg->m_bProcessThread == false)
		{
			delete []buf;

			pDlg->PostMessage(WM_COMPLETE_FTP_UPLOAD, 0);	// cancel
			return 0;
		}//if
*/

		if(pContentsInfo->strFilename == "")
		{
			nUploadCount++;
			continue;
		}//if

		try
		{
			CString strServerPath = FTP_CONTENTS_PATH;
			strServerPath.Append(pContentsInfo->strFilename);

			// upload
			CFile* server_file = param->pFtpConnection->OpenFile(strServerPath, GENERIC_WRITE, FTP_TRANSFER_TYPE_BINARY, 1);
			if(server_file)
			{
				CFile local_file;
				if(local_file.Open(strLocalPath, CFile::modeRead|CFile::shareDenyNone|CFile::typeBinary))
				{
					dwStartTick = ::GetTickCount();
					//memset(&buf, 0x00, sizeof(BYTE)*BUF_SIZE);

					// read & write
					int nRead;
					while((nRead = local_file.Read(buf, BUF_SIZE)) != 0)
					{
						/*
						pDlg->m_mutex.Lock();
						if(pDlg->m_bProcessThread == false)
						{
						pDlg->m_mutex.Unlock();
						server_file->Close();
						local_file.Close();
						delete []buf;

						pDlg->PostMessage(WM_COMPLETE_FTP_UPLOAD, 0); // cancel
						return 0;
						}//if
						*/

						server_file->Write(buf, nRead);
						nCurrentUploadSize += nRead;

						dwEndTick = ::GetTickCount();

						int remain_sec_current = (int)((dwEndTick-dwStartTick) * (pContentsInfo->nFilesize - nCurrentUploadSize) / nCurrentUploadSize / 1000);
						int remain_sec_total = (int)((dwEndTick-dwStartTick) * ( param->ulTotalSize - nCompleteUploadSize - nCurrentUploadSize) / nCurrentUploadSize / 1000);

						CTimeSpan remain_time_current(remain_sec_current);
						int percent_current = (int)(nCurrentUploadSize*1000/pContentsInfo->nFilesize);
						status.Format("NOW - %02d%% (%s)", percent_current/10, remain_time_current.Format("%H:%M:%S"));
						param->pbarCurrent->SetText(status);
						param->pbarCurrent->SetPos(percent_current);

						CTimeSpan remain_time_total(remain_sec_total);
						int percent_total = (int)((nCompleteUploadSize+nCurrentUploadSize)*1000/param->ulTotalSize);
						status.Format("%02d%% (%s)", percent_total/10, remain_time_total.Format("%H:%M:%S"));
						param->pbarTotal->SetText(status);
						param->pbarTotal->SetPos(percent_total);

						//pDlg->m_mutex.Unlock();
					}//while
					local_file.Close();
				}
				else
				{
					TRACE("Local file open fail\r\n");
					param->pWnd->PostMessage(WM_COMPLETE_FTP_UPLOAD, FTP_ERROR_LOCAL_FILE_OPEN, (LPARAM)(LPSTR)(LPCTSTR)pContentsInfo->strFilename);		//local file open error
					server_file->Close();
					delete server_file;
					delete []buf;
					delete param;
					return 0;
				}//if

				server_file->Close();
				delete server_file;

				nUploadCount++;
			}
			else
			{
				TRACE("Server file open fail\r\n");
				param->pWnd->PostMessage(WM_COMPLETE_FTP_UPLOAD, FTP_ERROR_REMOTE_FILE_OPEN, (LPARAM)(LPSTR)(LPCTSTR)pContentsInfo->strFilename);		//remote file open error
				delete []buf;
				delete param;
				return 0;
			}//if
		}
		catch(CInternetException* ex)
		{
			CString msg;
			TCHAR szCause[1024];
			ex->GetErrorMessage(szCause, 1024);
			msg.Format("(%d) %s", ex->m_dwError, szCause);
			ex->Delete();

			param->pWnd->PostMessage(WM_COMPLETE_FTP_UPLOAD, FTP_ERROR_UNKNOWN, (LPARAM)(LPSTR)(LPCTSTR)msg);		//Unknown file exception
			delete []buf;
			delete param;
			return 0;
		}//try
	}//while

	if(nUploadCount == count)
	{
		//package 파일 upload
		CString strLocalFile = pDlg->m_strPackageFilePath;
		CString strRemoteFile = FTP_CONFIG_PATH;
		strRemoteFile.Append(pDlg->m_strPackageName);
		strRemoteFile.Append(".ini");
		if(!pDlg->m_pFtpConnection->PutFile(strLocalFile, strRemoteFile, FTP_TRANSFER_TYPE_BINARY, 1))
		{
			param->pWnd->PostMessage(WM_COMPLETE_FTP_UPLOAD, FTP_ERROR_UNKNOWN, (LPARAM)(LPSTR)(LPCTSTR)"Package file upload fail");	//fail
		}
		else
		{
			param->pWnd->PostMessage(WM_COMPLETE_FTP_UPLOAD, FTP_ERROR_OK);			//all ok
		}//if
	}
	else
	{
		CString strMsg;
		strMsg.Format("Contents file upload fail !!!\r\nLocal contents file counts = %d, Uploaded contents file counts = %d", count, nUploadCount);
		param->pWnd->PostMessage(WM_COMPLETE_FTP_UPLOAD, FTP_ERROR_UNKNOWN, (LPARAM)(LPSTR)(LPCTSTR)strMsg);		//fail
	}//if

	delete []buf;
	delete param;

	return 0;
}

/*
LRESULT CFtpUploadDlg::OnDisplayStatus(WPARAM wParam, LPARAM lParam)
{
	DISPLAY_PARAM* param = (DISPLAY_PARAM*)wParam;

	CString		title			= param->title;
	CString		current_text	= param->current_text;
	int			current_pos		= param->current_pos;
	CString		total_text		= param->total_text;
	int			total_pos		= param->total_pos;

	delete param;

	m_staticStatus.SetWindowText(title);

	m_pbarCurrent.SetText(current_text);
	m_pbarCurrent.SetPos(current_pos);

	m_pbarTotal.SetText(total_text);
	m_pbarTotal.SetPos(total_pos);

	return 0;
}
*/


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 장애 타입에 따른 에러 메시지를 만들어 준다 \n
/// @param (CNetDeviceInfo*) pclsNetInfo : (in/out) 장애가 발생한 NetDeviceInfo
/// @param (int) nErrorCode : (in) 장애 타입
/// @param (LPARAM) lParam : (in) 파일 이름등의 부가 정보
/////////////////////////////////////////////////////////////////////////////////
CString CFtpUploadDlg::MakeErrorMsg(CNetDeviceInfo* pclsNetInfo, int nErrorCode, LPARAM lParam)
{
	CString strMsg;
	switch(nErrorCode)
	{
	case FTP_ERROR_OK:					//all ok
		{
			strMsg = LoadStringById(IDS_FTPUPLOADDLG_MSG002);
		}
		break;
	case FTP_ERROR_EMPTY_CONTENTS:		// Empty contents
		{
			strMsg = LoadStringById(IDS_FTPUPLOADDLG_MSG003);
		}
		break;
	case FTP_ERROR_USER_CANCEL:			//user cancel	
		{
			strMsg = LoadStringById(IDS_FTPUPLOADDLG_MSG004);
		}
		break;
	case FTP_ERROR_CONNECT_FAIL:		//connect fail
		{
			strMsg.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG005)); 
		}
		break;
	case FTP_ERROR_LOCAL_FILE_OPEN:		//local file open error
		{
			LPSTR lpszFileName = (LPSTR)lParam;
			strMsg.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG006), lpszFileName);
		}
		break;
	case FTP_ERROR_REMOTE_FILE_OPEN:	//remote file open error
		{
			LPSTR lpszFileName = (LPSTR)lParam;
			strMsg.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG007), lpszFileName);
		}
		break;
	case FTR_ERROR_REMOTE_FILE_EXIST:
		{
			LPSTR lpszFileName = (LPSTR)lParam;
			strMsg.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG008), lpszFileName);
		}
		break;
	case FTR_ERROR_REMOTE_SPACE:
		{
			strMsg.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG009));
		}
		break;
	case FTP_ERROR_NOTIFY_UPDATE:
		{
			LPSTR lpszPackageName = (LPSTR)lParam;
			strMsg.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG010), lpszPackageName);
		}
		break;
	case FTP_ERROR_UNKNOWN:				//Unknown file exception
		{
			//strMsg = "Unknown file exception occured !!!\r\nTry again after close another application";
			strMsg = (LPSTR)lParam;
		}
		break;
	default:
		{
			strMsg = LoadStringById(IDS_FTPUPLOADDLG_MSG011);
		}
	}//switch

	if(pclsNetInfo){
		pclsNetInfo->m_strErrorMsg = strMsg;
	}
	return strMsg;
}

//////////////////////////////////
// 2009-04-24 이후 추가 시작 - 구현석 ------------------------------
//////////////////////////////////
void CFtpUploadDlg::RunFTP()
{
	TraceLog(("RunFTP()"));

	if(CDataContainer::getInstance()->GetContentsMap()->GetCount() == 0){
		m_pParentWnd->PostMessage(WM_FTP_UPLOAD, FTP_ERROR_EMPTY_CONTENTS);
		return;
	}

	if(m_aryHostInfo.GetCount() == 0){
		m_pParentWnd->PostMessage(WM_FTP_UPLOAD, FTP_ERROR_EMPTY_CONTENTS);
		return;
	}

	CMainFrame* pMain = (CMainFrame*)m_pParentWnd;
	m_strPackageName = pMain->GetHostName();
	m_strPackageFilePath  = pMain->GetSourceDirive();
	m_strPackageFilePath += UBC_CONFIG_PATH;
	m_strPackageFilePath += m_strPackageName;
	m_strPackageFilePath += _T(".ini");

	m_pbarTotal.SetStyle(PROGRESS_TEXT);
	m_pbarTotal.SetRange(0, 1000);
	m_pbarTotal.SetPos(0);
	m_pbarTotal.SetText("");

	m_pbarCurrent.SetStyle(PROGRESS_TEXT);
	m_pbarCurrent.SetRange(0, 1000);
	m_pbarCurrent.SetPos(0);
	m_pbarCurrent.SetText("");

	CenterWindow();
	ShowWindow(SW_SHOW);

	m_staticStatus.SetWindowText("Initializing uploading...");
	m_ulTotalSize = GetTotalFileSize();
	m_aryFailHostInfo.RemoveAll();
	m_szErrorReporter = "";

	m_pThread = AfxBeginThread(RunFTPThread, (LPVOID)this);
}

void CFtpUploadDlg::RunFileTransfer(CFtpUploadDlg* pDlg)
{
	if(!pDlg->m_pClient){
		pDlg->m_pClient = new CFTClient();
	}
	pDlg->m_btnCancel.EnableWindow(FALSE);

	CFTClient* pClient = pDlg->m_pClient;
	CString szID;
	CONTENTS_INFO* pInfo;
	CString szErrorTitle, szTemp;

	POSITION pos = m_mapContents.GetStartPosition();
	while(pos)
	{
		m_mapContents.GetNextAssoc(pos, szID, (void*&)pInfo);
		if(!pInfo) continue;
		if(!pInfo->bLocalFileExist) continue;
		if( pInfo->strFilename.IsEmpty() &&
		   (pInfo->nContentsType==CONTENTS_SMS        ||
		    pInfo->nContentsType==CONTENTS_TICKER     ||
		    pInfo->nContentsType==CONTENTS_TEXT       ||
			pInfo->nContentsType==CONTENTS_TV         ||
		    pInfo->nContentsType==CONTENTS_WEBBROWSER ||
			pInfo->nContentsType==CONTENTS_RSS        ||
			pInfo->nContentsType==CONTENTS_WIZARD     ))
		{
			continue;
		}

		CString strChildPath;
		CString strContentsPath = UBC_CONTENTS_PATH;
		int nFindPos = pInfo->strLocalLocation.MakeLower().Find(strContentsPath.MakeLower());
		if(nFindPos >= 0)
		{
			strChildPath = pInfo->strLocalLocation.Mid(nFindPos + strContentsPath.GetLength());
		}

		// 전송해야 할 파일을 추가한다
		if(pClient->AddFile(pInfo->strLocalLocation + pInfo->strFilename, UBC_CONTENTS_PATH + strChildPath)) continue;

		if(pInfo->strFilename.IsEmpty())
		{
			szTemp = MakeErrorMsg(NULL, FTP_ERROR_EMPTY_CONTENTS, (LPARAM)(LPCTSTR)pInfo->strFilename);
			szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG012), pInfo->strContentsName);
			MakeErrorReport(szErrorTitle, szTemp);
		}
		else
		{
			szTemp = MakeErrorMsg(NULL, FTP_ERROR_LOCAL_FILE_OPEN, (LPARAM)(LPCTSTR)pInfo->strFilename);
			szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG013), pInfo->strFilename);
			MakeErrorReport(szErrorTitle, szTemp);
		}
	}

	// INI 파일은 무조건 전송한다
	if(!pClient->SetINI(m_strPackageFilePath))
	{
		delete pDlg->m_pClient;
		pDlg->m_pClient = NULL;

		szTemp = MakeErrorMsg(NULL, FTP_ERROR_LOCAL_FILE_OPEN, (LPARAM)(LPCTSTR)pInfo->strFilename);
		szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG013), pInfo->strFilename);
		MakeErrorReport(szErrorTitle, szTemp);
		return;
	}

	for(int i = 0; i < m_aryHostInfo.GetCount(); i++)
	{
		m_nIndexHostInfo = i;
		CNetDeviceInfo* pHostInfo = (CNetDeviceInfo*)m_aryHostInfo.GetAt(i);
		if(!pHostInfo)	continue;

		CString strMsg;
		strMsg.Format("%s [%s]", pHostInfo->m_strDeviceName, pHostInfo->m_strIP);
		m_staticHost.SetWindowText(strMsg);
		m_staticStatus.SetWindowText("Trying to connect...");

		if(!pClient->Connect(pHostInfo->m_strIP ,FT_PORT))
		{
			pHostInfo->m_nErrorCode = FTP_ERROR_CONNECT_FAIL;
			szTemp = MakeErrorMsg(pHostInfo, FTP_ERROR_CONNECT_FAIL, (LPARAM)0);
			szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG014), pHostInfo->m_strDeviceName);
			MakeErrorReport(szErrorTitle, szTemp);

			continue;
		}

		pDlg->m_btnCancel.EnableWindow(TRUE);
		pClient->SetInfoCtrl(&m_pbarCurrent, &m_pbarTotal, &m_staticStatus);
		pClient->SendFiles();
		pClient->Close();

		if(pDlg->m_bForceStop)
		{
			pDlg->m_bForceStop = FALSE;
			delete pDlg->m_pClient;
			pDlg->m_pClient = NULL;
			return;
		}

		char szErr[1024];
		int nErr = 0;
		BYTE nFileRet = pClient->GetStatus();
		if(E_ST_SUCCESS != nFileRet)
		{
			nErr = ((CFTMsgSocket*)pClient->GetSocket())->GetError(szErr);
		}

		if(E_ST_TIMEOUT == nFileRet)
		{
			pHostInfo->m_nErrorCode = FTP_ERROR_UNKNOWN;
			szTemp = MakeErrorMsg(pHostInfo, FTP_ERROR_UNKNOWN, (LPARAM)0);
			szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG015), pHostInfo->m_strDeviceName);
			MakeErrorReport(szErrorTitle, szTemp);
		}

		pos = pClient->GetFiles()->GetStartPosition();
		while(pos)
		{
			CString szFile;
			SFileInfo* pInfo = NULL;
			pClient->GetFiles()->GetNextAssoc(pos, szFile, (void*&)pInfo);

			if(!pInfo) continue;

			int nErrorCode = FTP_ERROR_OK;
			switch(pInfo->flag)
			{
			case EFT_FF_INIT:
				nErrorCode = EFT_FF_UNKNOWN;
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG016), pInfo->szFile, pHostInfo->m_strDeviceName);
				break;
			case EFT_FF_CREATE:
				nErrorCode = EFT_FF_UNKNOWN;
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG017), pInfo->szFile, pHostInfo->m_strDeviceName);
				break;
			case EFT_FF_SENDDATA:
				nErrorCode = EFT_FF_UNKNOWN;
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG017), pInfo->szFile, pHostInfo->m_strDeviceName);
				break;
			case EFT_FF_EXIST:
				nErrorCode = FTR_ERROR_REMOTE_FILE_EXIST;
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG018), pInfo->szFile, pHostInfo->m_strDeviceName);
				break;
			case EFT_FF_DSERR:
				nErrorCode = FTR_ERROR_REMOTE_SPACE;
#if _ML_ENG_
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG019), pInfo->szFile, pHostInfo->m_strDeviceName);
#else
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG019), pHostInfo->m_strDeviceName, pInfo->szFile);
#endif
				break;
			case EFT_FF_OPENERR:
				nErrorCode = FTP_ERROR_REMOTE_FILE_OPEN;
#if _ML_ENG_
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG020), pInfo->szFile, pHostInfo->m_strDeviceName);
#else
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG020), pHostInfo->m_strDeviceName, pInfo->szFile);
#endif
				break;
			case EFT_FF_CREAERR:
				nErrorCode = FTP_ERROR_REMOTE_FILE_OPEN;
#if _ML_ENG_
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG020), pInfo->szFile, pHostInfo->m_strDeviceName);
#else
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG020), pHostInfo->m_strDeviceName, pInfo->szFile);
#endif
				break;
			case EFT_FF_COPYERR:
				nErrorCode = FTP_ERROR_UNKNOWN;
#if _ML_ENG_
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG021), pInfo->szFile, pHostInfo->m_strDeviceName);
#else
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG021), pHostInfo->m_strDeviceName, pInfo->szFile);
#endif
				break;
			case EFT_FF_UNKNOWN:
				nErrorCode = FTP_ERROR_UNKNOWN;
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG022));
				break;
			default:
				continue;
			}

			pHostInfo->m_nErrorCode = nErrorCode;
			szTemp = (EFT_FF_UNKNOWN != nErrorCode ? MakeErrorMsg(pHostInfo, nErrorCode, (LPARAM)(LPCTSTR)pInfo->szFile) : _T(""));
			MakeErrorReport(szErrorTitle, szTemp);
		}

		// 2010.07.02 gwangsoo 정상적인 경우에 소켓에러 발생으로 전송되지 못하게되므로 원래대로 소스를 원복한다.
		// 2010.06.23 gwangsoo 전송이 성공한 경우에만 단말에 콘텐츠 패키지를 실행하도록 한다.
		if(E_ST_TIMEOUT != nFileRet)
		//if(E_ST_SUCCESS == nFileRet)
		{
			scratchUtil* aUtil = scratchUtil::getInstance();
			CString szArgs = m_strArgs;

			if(pHostInfo->m_nMonitorCnt == 1)
			{
				szArgs += " 0";
			}
			else if(m_DisplalySide[0])
			{
				szArgs += " 0";
				if(m_DisplalySide[1]) szArgs += "1";
			}
			else if(m_DisplalySide[1])
			{
				szArgs += " 1";
			}

			if(!aUtil->socketAgent(pHostInfo->m_strIP ,pHostInfo->m_nSvrPort ,"import" ,szArgs))
			{
				pHostInfo->m_nErrorCode = FTP_ERROR_NOTIFY_UPDATE;
				szErrorTitle.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG023), pHostInfo->m_strDeviceName);
				szTemp = MakeErrorMsg(pHostInfo, FTP_ERROR_NOTIFY_UPDATE);
				MakeErrorReport(szErrorTitle, szTemp);
			}
		}
	}

	if(pDlg->m_pClient)
	{
		delete pDlg->m_pClient;
		pDlg->m_pClient = NULL;
	}
}

UINT CFtpUploadDlg::RunFTPThread(LPVOID pVoid)
{
	CFtpUploadDlg* pDlg = (CFtpUploadDlg*)pVoid;
	if(!pDlg)	return 0;

	pDlg->RunFileTransfer(pDlg);
	pDlg->PostMessage(WM_CLOSE);
	AfxEndThread(0);

	return 0;
}

CString CFtpUploadDlg::GetErrorMsg()
{
	return m_szErrorReporter;
}

void CFtpUploadDlg::MakeErrorReport(LPCTSTR szTitle, LPCTSTR szDescript)
{
	if(!m_szErrorReporter.IsEmpty()){
		m_szErrorReporter += "\n";
		m_szErrorReporter += "-----------------------------------------\n";
	}

	m_szErrorReporter += szTitle;
	if(szDescript && strlen(szDescript) > 0){
		m_szErrorReporter += "\n\n";
		m_szErrorReporter += szDescript;
	}
}

//////////////////////////////////
// Enterprise edition 을 위한 코드
CONTENTS_INFO_MAP* CFtpUploadDlg::GetContentsInfoMap()
{
	return &m_mapContents;
}

UINT CFtpUploadDlg::UploadProc(LPVOID param)
{
	TraceLog(("UploadProc()"));
	if(param){
		((CFtpUploadDlg*)param)->Upload();
		((CFtpUploadDlg*)param)->PostMessage(WM_CLOSE);
	}
	AfxEndThread(0);
	return 0;
}

void CFtpUploadDlg::Upload()
{
	CString szTemp;

//	for(int i=0; i< m_aryHostInfo.GetCount(); i++)
	{
//		CNetDeviceInfo* pNetDvc = (CNetDeviceInfo*)m_aryHostInfo.GetAt(m_nIndexHostInfo);
//		if(!pNetDvc)	continue;

		CSimpleFtp ftp;
//		ftp.SetTimeOut(300);
		ftp.SetReceiver(this);

//		TraceLog(("ftp.Connect(%s)",pNetDvc->m_strIP));
//		if(!ftp.Connect(pNetDvc->m_strIP, pNetDvc->m_strID, pNetDvc->m_strPWD, pNetDvc->m_nFTPPort)){
//			szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG024), pNetDvc->m_strDeviceName);
//			MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG025), szTemp);
//			continue;
//		}

		if(!GetEnvPtr()->Connect(&ftp, GetEnvPtr()->m_PackageInfo.szSiteID)){
			szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG024), GetEnvPtr()->m_PackageInfo.szSiteID);
			MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG025), szTemp);
			return;
		}

		szTemp.Format("0 %%");
		m_pbarTotal.SetText(szTemp);
		m_pbarTotal.SetPos(0);

		int nConCnt = 0;
		int nCurCnt = 0;
		int nTotalCnt = 0;

//		szTemp.Format("%s", pNetDvc->m_strDeviceName);
		szTemp.Format("%s", GetEnvPtr()->m_PackageInfo.szSiteID);
		m_staticHost.SetWindowText(szTemp);

		// Uploading package ini file 
		CString szFile, szRemote, szLocal;
		szFile.Format("%s.ini", GetEnvPtr()->m_strPackage);
		szLocal.Format("%s\\%s", GetEnvPtr()->m_PackageInfo.szDrive, SAMPLE_CONFIG_PATH);

//		if(m_bReserve)
//			szRemote.Format("/rev/config/");
//		else
			szRemote.Format("/config/");

		TraceLog(("ftp.PutFile(%s,%s,%s)",szFile,szLocal,szRemote));
		int nRet = ftp.PutFile(szFile, szLocal, szRemote);
		if(nRet != CSimpleFtp::eSuccess){
			szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG026), GetEnvPtr()->m_strPackage);
			MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG027), szTemp);
		}

		// Uploading package template background file 
		TEMPLATE_LINK_LIST* pTempList = CDataContainer::getInstance()->GetPlayTemplateList();
		if(pTempList){
			for(int i = 0; i < pTempList->GetCount(); i++){
				TEMPLATE_INFO* pTempInfo = pTempList->GetAt(i).pTemplateInfo;
				if(!pTempInfo)	continue;

				if(pTempInfo->strBgImage.IsEmpty())
					continue;

				szFile.Format("%s", pTempInfo->strBgImage);
				szLocal.Format("%s\\%s", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONTENTS_PATH);

//				if(m_bReserve)
//					szRemote.Format("/rev/contents/%s/", GetEnvPtr()->m_strPackage);
//				else
				// 부속파일처리 관련 수정
				//szRemote.Format("/contents/%s/", GetEnvPtr()->m_strPackage);
				szRemote.Format("/contents/%s/", pTempInfo->strId);

				TraceLog(("ftp.PutFile(%s,%s,%s)",szFile,szLocal,szRemote));
				int nRet = ftp.PutFile(szFile, szLocal, szRemote, false);
				if(nRet != CSimpleFtp::eSuccess){
					szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG028), szFile);
					MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG029), szTemp);
				}
			}
		}

		// Uploading contents file
		CString szKey;
		bool bOverwite = false;
		CONTENTS_INFO* pInfo;
		CONTENTS_INFO_MAP* pMapContents = CDataContainer::getInstance()->GetContentsMap();
		nTotalCnt = pMapContents->GetCount();
		POSITION pos = pMapContents->GetStartPosition();
		while(pos){
			if(!ftp.IsTimeOut()){
				pMapContents->GetNextAssoc(pos, szKey, (void*&)pInfo);
			}
			else if(nConCnt < 3){
				nConCnt++;
				Sleep(1000);
				if(ftp.ReConnect())
					break;
			}
			else{
				break;
			}

			if(!pInfo){
				continue;
			}
			else if(pInfo->strFilename.IsEmpty() && 
				(pInfo->nContentsType==CONTENTS_SMS ||pInfo->nContentsType==CONTENTS_TICKER||
				 pInfo->nContentsType==CONTENTS_TEXT||pInfo->nContentsType==CONTENTS_TV    ||
				 pInfo->nContentsType==CONTENTS_RSS ||pInfo->nContentsType==CONTENTS_WIZARD||
				 pInfo->nContentsType==CONTENTS_WEBBROWSER))
			{
				continue;
			}
			else if(!pInfo->bLocalFileExist){
				szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG033), pInfo->strFilename);
				MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG030), szTemp);
				continue;
			}

			szTemp.Format("%s", pInfo->strFilename);
			m_staticStatus.SetWindowText(szTemp);

			szLocal.Format("%s\\%s", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONTENTS_PATH);

			if(pInfo->strServerLocation.IsEmpty())
			{
				// 부속파일처리 관련 수정
				//pInfo->strServerLocation.Format("/contents/%s/", GetEnvPtr()->m_strPackage);
				pInfo->strServerLocation.Format("/contents/%s/", pInfo->strId);
			}

//			if(m_bReserve){
//				szRemote = "/rev";
//				szRemote += pInfo->strServerLocation;
//			}else{
				szRemote = pInfo->strServerLocation;
//			}
			
			szTemp.Format("0 %%");
			m_pbarCurrent.SetText(szTemp);
			m_pbarCurrent.SetPos(0);

			TraceLog(("ftp.Upload(%s,%s,%s)",pInfo->strFilename,szLocal,szRemote));
			nRet = ftp.Upload(pInfo->strFilename, szLocal, szRemote, bOverwite);

			if(nRet == CSimpleFtp::eSuccess){
				pInfo->bLocalFileExist = true;
			}else if(nRet == CSimpleFtp::eRemoteFileNotFound){
				szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG031), GetEnvPtr()->m_szSite, pInfo->strFilename);
				MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG032), szTemp);
			}else if(nRet == CSimpleFtp::eLocalFileNotFound){
				szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG033), pInfo->strFilename);
				MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG030), szTemp);
			}else if(nRet == CSimpleFtp::eRemoteOpenFail){
				szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG033), pInfo->strFilename);
				MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG034), szTemp);
			}else if(nRet == CSimpleFtp::eLocalOpenFail){
				szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG035), pInfo->strFilename);
				MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG036), szTemp);
			}else if(nRet == CSimpleFtp::eAlreadyExistFile){
				/*if(UbcMessageBox(LoadStringById(IDS_FTPUPLOADDLG_MSG041), MB_ICONINFORMATION|MB_YESNO)==IDYES){
					ftp.Upload(pInfo->strFilename, szLocal, szRemote, bOverwite=true);
					
					szTemp.Format("100 %%");
					m_pbarCurrent.SetText(szTemp);
					m_pbarCurrent.SetPos(1000);

					nCurCnt++;
					DWORD dwPerMill = (1000*nCurCnt)/nTotalCnt;
					szTemp.Format("%.1f %%", dwPerMill/10.0f);
					m_pbarTotal.SetText(szTemp);
					m_pbarTotal.SetPos(dwPerMill);
					continue;
				}*/

				szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG035), pInfo->strFilename);
				MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG037), szTemp);
			}else{
				szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG035), pInfo->strFilename);
				MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG038), szTemp);
			}
			
			if(ftp.IsTimeOut()){
				ftp.Close();
				TraceLog(("ftp.Close()"));
				continue;
			}

			szTemp.Format("100 %%");
			m_pbarCurrent.SetText(szTemp);
			m_pbarCurrent.SetPos(1000);

			nCurCnt++;
			DWORD dwPerMill = (1000*nCurCnt)/nTotalCnt;
			szTemp.Format("%.1f %%", dwPerMill/10.0f);
			m_pbarTotal.SetText(szTemp);
			m_pbarTotal.SetPos(dwPerMill);
		}
	}
}

UINT CFtpUploadDlg::DownloadProc(LPVOID param)
{
	TraceLog(("DownloadProc()"));
	if(param){
		((CFtpUploadDlg*)param)->Download();
		((CFtpUploadDlg*)param)->PostMessage(WM_CLOSE);
	}
	AfxEndThread(0);
	return 0;
}

void CFtpUploadDlg::Download()
{
	CString szTemp;
	CSimpleFtp ftp;
//	ftp.SetTimeOut(120);
	ftp.SetReceiver(this);

	m_szErrorReporter.Empty();
	szTemp.Format("%s", GetEnvPtr()->m_PackageInfo.szSiteID);
	m_staticHost.SetWindowText(szTemp);

	if(!GetEnvPtr()->Connect(&ftp, GetEnvPtr()->m_PackageInfo.szSiteID)){
		szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG024), GetEnvPtr()->m_PackageInfo.szSiteID);
		MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG025), szTemp);
		return;
	}

	CString szLocal, szRemote;
	szLocal.Format("%s\\%s", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);

	int nCurCnt, nTotalCnt, nConCnt;
	CString szKey;
	CONTENTS_INFO* pInfo;

	nConCnt = 0;
	nCurCnt = 0;
	nTotalCnt = m_mapContents.GetCount();
	POSITION pos = m_mapContents.GetStartPosition();

	while(pos){
		if(!ftp.IsTimeOut()){
			m_mapContents.GetNextAssoc(pos, szKey, (void*&)pInfo);
		}
		else if(nConCnt < 3){
			nConCnt++;
			Sleep(1000);
			if(ftp.ReConnect())
				break;
		}
		else{
			break;
		}

		if(!pInfo) continue;

		if(pInfo->bLocalFileExist)
			continue;
		if(pInfo->strFilename.IsEmpty() && 
			(pInfo->nContentsType==CONTENTS_SMS ||pInfo->nContentsType==CONTENTS_TICKER||
			 pInfo->nContentsType==CONTENTS_TEXT||pInfo->nContentsType==CONTENTS_TV    ||
			 pInfo->nContentsType==CONTENTS_RSS ||pInfo->nContentsType==CONTENTS_WIZARD||
			 pInfo->nContentsType==CONTENTS_WEBBROWSER))
			continue;

		szTemp.Format("%s", pInfo->strFilename);
		m_staticStatus.SetWindowText(szTemp);

		if(pInfo->strServerLocation.IsEmpty())
		{
			// 부속파일처리 관련 수정
			//pInfo->strServerLocation.Format("/contents/%s/", GetEnvPtr()->m_strPackage);
			pInfo->strServerLocation.Format("/contents/%s/", pInfo->strId);
		}

		szRemote = pInfo->strServerLocation;
//		pInfo->bServerFileExist = false;

		TraceLog(("ftp.Download(%s,%s,%s)",pInfo->strFilename,szRemote,szLocal));
		int nRet = ftp.Download(pInfo->strFilename, szRemote, szLocal, false);
		
		if(ftp.IsTimeOut()){
			ftp.Close();
			TraceLog(("ftp.Close()"));
			continue;
		}

		if(nRet == CSimpleFtp::eSuccess){
			pInfo->bLocalFileExist = true;
			pInfo->bServerFileExist = true;
		}else if(nRet == CSimpleFtp::eRemoteFileNotFound){
			szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG031), GetEnvPtr()->m_szSite, pInfo->strFilename);
			MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG032), szTemp);
		}else if(nRet == CSimpleFtp::eLocalFileNotFound){
			szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG033), pInfo->strFilename);
			MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG030), szTemp);
		}else if(nRet == CSimpleFtp::eRemoteOpenFail){
			szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG033), pInfo->strFilename);
			MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG034), szTemp);
		}else if(nRet == CSimpleFtp::eLocalOpenFail){
			szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG035), pInfo->strFilename);
			MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG036), szTemp);
		}else if(nRet == CSimpleFtp::eAlreadyExistFile){
			szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG035), pInfo->strFilename);
			MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG039), szTemp);
		}else{
			szTemp.Format(LoadStringById(IDS_FTPUPLOADDLG_MSG035), pInfo->strFilename);
			MakeErrorReport(LoadStringById(IDS_FTPUPLOADDLG_MSG040), szTemp);
		}

		szTemp.Format("100 %%");
		m_pbarCurrent.SetText(szTemp);
		m_pbarCurrent.SetPos(1000);

		nCurCnt++;
		DWORD dwPerMill = (1000*nCurCnt)/nTotalCnt;
		szTemp.Format("%.1f %%", dwPerMill/10.0f);
		m_pbarTotal.SetText(szTemp);
		m_pbarTotal.SetPos(dwPerMill);
	}
}

void CFtpUploadDlg::InvokeSize(ULONGLONG ullSize, DWORD dwPerMill)
{
	CString szTemp;
	szTemp.Format("%.1f %%", dwPerMill/10.0f);
	m_pbarCurrent.SetText(szTemp);
	m_pbarCurrent.SetPos(dwPerMill);
//	TRACE("CFtpUploadDlg::InvokeSize(%d %%)\n", dwPerMill);
}

//////////////////////////////////
// 2009-04-24 이후 추가 끝 - 구현석 ------------------------------
//////////////////////////////////
