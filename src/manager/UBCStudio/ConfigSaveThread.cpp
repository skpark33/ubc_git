// ConfigSaveThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "ConfigSaveThread.h"

#include "MainFrm.h"
#include "UBCStudioDoc.h"

// CConfigSaveThread
IMPLEMENT_DYNCREATE(CConfigSaveThread, CWinThread)

CConfigSaveThread::CConfigSaveThread()
{
	m_pParent = NULL;
	m_pParentDoc = NULL;
}

CConfigSaveThread::~CConfigSaveThread()
{
}

BOOL CConfigSaveThread::InitInstance()
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CConfigSaveThread::ExitInstance()
{
	::CoUninitialize();

	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CConfigSaveThread, CWinThread)
END_MESSAGE_MAP()


// CConfigSaveThread 메시지 처리기입니다.

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Set parent params \n
/// @param (void*) pParent : (in) thread owner window pointer
/// @param (void*) pParentDoc : (in) thread owner window's document pointer
/////////////////////////////////////////////////////////////////////////////////
void CConfigSaveThread::SetThreadParam(void* pParent, void* pParentDoc)
{
	m_pParent = pParent;
	m_pParentDoc = pParentDoc;
}

int CConfigSaveThread::Run()
{
	CMainFrame* pParent = (CMainFrame*)m_pParent;
	CUBCStudioDoc* pParentDoc = (CUBCStudioDoc*)m_pParentDoc;

	if(pParentDoc->SaveConfig()){
		pParent->PostMessage(WM_SAVE_CONFIG_COMPELETE, 1, 0);
	}else{
		pParent->PostMessage(WM_SAVE_CONFIG_COMPELETE, 0, 0);
	}

	ExitThread(0);
	return 0;
}
