#include "stdafx.h"
#include "resource.h"
#include "CopModule.h"
#include "Enviroment.h"
#include "CustomerInfoDlg.h"
#include "ubccopcommon/ubccopcommon.h"

#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libConfig/ciEnv.h>

#include <ci/libWorld/ciWorld.h>
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciAny.h>

#include "cci/libWorld/cciGUIORBThread.h"
//#include "UTIL/InternalIPClient/InternalIPClient.h"
#include <common/libCommon/ubcPermition.h>

BOOL GetChildSite(CString strCustomer, CString strSiteId, cciStringList& list);

#define CMD_PARAM_CNT		3

ciSET_DEBUG(1, "UBCStudioEE");
cciGUIORBThread* g_pOrbThread = NULL;
BOOL g_bIsInitORB = FALSE;

BOOL copErrCheck(cciReply& reply, bool bShowMsgBox=true);

BOOL IsInitORB()
{
	return g_bIsInitORB;
}

BOOL InitORB(LPCTSTR szIP, int nPort)
{
	TraceLog(("InitORB(%s:%d)", szIP, nPort));

	if(g_bIsInitORB) return TRUE;

	g_bIsInitORB = FALSE;
	// ORB Init
//-ORBInitRef NameService=corbaloc:iiop:211.232.57.202:14109/NameService -ORBInitRef InterfaceRepository=corbaloc:iiop:211.232.57.202:14110/InterfaceRepository
//	char* argv[CMD_PARAM_CNT] = {
//				UBCSTUDIOEEEXE_STR,
//				"-ORBInitRef",
//				"NameService=corbaloc:iiop:211.232.57.202:14109/NameService"
//	};
/*
	CString szBuf;
	int argc = CMD_PARAM_CNT;	//__argc
	char* argv[CMD_PARAM_CNT];	//__argv
	
	argv[0] = (char*)malloc(sizeof(UBCSTUDIOEEEXE_STR)+1);
	strcpy(argv[0], UBCSTUDIOEEEXE_STR);
	
	argv[1] = (char*)malloc(sizeof("-ORBInitRef")+1);
	strcpy(argv[1], "-ORBInitRef");

	szBuf.Format("NameService=corbaloc:iiop:%s:%d/NameService", szIP, nPort);
	argv[2] = (char*)malloc(szBuf.GetLength()+1);
	strcpy(argv[2], szBuf);

	char **ppv = argv;

	InternalIPClient ipClient;
	TraceLog(("InternalIPClient::init()"));
	ciBoolean bRet = ipClient.init(argc, ppv);
	
	for(int i = 0; i < CMD_PARAM_CNT; i++){
		free(argv[i]);
	}
*/
	if(g_pOrbThread){
		g_pOrbThread->destroy();
		delete g_pOrbThread;
		g_pOrbThread = NULL;
	}

	/*
	InternalIPClient ipClient(szIP, nPort);
	TraceLog(("InternalIPClient::init()"));
	ciBoolean bRet = ipClient.init(__argc, __argv);

	if (bRet == ciFalse){
		TraceLog(("InternalIPClient::init(failed)"));
		return FALSE;
	}
	*/

	// ACE init
	TraceLog(("ACE::init()"));
	int nRet = ACE::init();

	if (!g_pOrbThread){
		g_pOrbThread = new cciGUIORBThread(__argc,__argv);
		if(g_pOrbThread != NULL)
			g_pOrbThread->start();
	}

	// ciWorld Init
	ciWorld* pWorld = ciWorld::getWorld();

	TraceLog(("ciWorld::init()"));
	if(pWorld->init(__argc, __argv) == ciFalse) {
		ciERROR(("cciORBWorld::init() is FAILED"));
		return FALSE;
	}
	ciDebug::setDebugOn();
	ciWorld::ylogOn();
	ciEnv::defaultEnv(true,"utv1");
	if(ciWorld::hasWatcher() == ciTrue) {
		TraceLog(("ciWorld::hasWatcher(fail)"));
		return FALSE;
	}

	for(int i=0; i<30 ; i++)
	{
		Sleep(1000);
		if(CheckORB())
		{
			g_bIsInitORB = TRUE;
			break;
		}
	}

	TraceLog(("InitORB(Succeed)"));
	return TRUE;
}

void DestroyORB()
{
	if(g_pOrbThread){
		//g_pOrbThread->stop();
		//g_pOrbThread->join();
		g_pOrbThread->destroy();
		delete g_pOrbThread;
		g_pOrbThread = NULL;
	}

	g_bIsInitORB = FALSE;
}

BOOL CheckORB()
{
	cciCall aCall("get");
	cciEntity aEntity("PM=*");
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

void copTrace(const char* szPath, int nLine, const char* szMsg)
{
	if(__debuger__.isDebugOn(1)) { 
		char szFile[MAX_PATH], szExe[MAX_PATH];
		_tsplitpath(szPath, NULL, NULL, szFile, szExe);

		unsigned long tid = PTHREAD_SELF(); 
		ciInt aSize = printf("TRACE-[%u]%s%s(line:%d)::",tid, szFile, szExe, nLine); 
		aSize += printf(szMsg); 

		ciDebug::setSize(aSize);
		if(ciDebug::hasLog()){
			ciDebug::log("TRACE-[%u]%s%s(line:%d)::",tid, szFile, szExe, nLine); 
			ciDebug::log(szMsg);
			ciDebug::logLineEnd();
		}
	}
}

BOOL copErrCheck(cciReply& reply, bool bShowMsgBox/*=true*/)
{
	ciLong errCode=0;
	reply.getItem("ERROR_CODE", errCode);
	ciString errMsg;
	reply.getItem("ERROR_MSG", errMsg);

	if(errCode < 0 )
	{
		TraceLog(("ERROR_CODE [%d]", errCode));
		TraceLog(("ERROR_MSG [%s]", errMsg.c_str()));

		// Display this message  :      skpark 20100621
		CString strMsg;
		strMsg.Format(_T("%s[code:%d]"), errMsg.c_str(), errCode);
		if(bShowMsgBox) UbcMessageBox(strMsg, MB_ICONERROR);
		return FALSE;
	}

	return TRUE;
}

BOOL GetMyIp(CString& myIp)
{
	char a_hostname[128];
	char a_hostip[32];
	::memset(a_hostname, 0, sizeof(a_hostname));
	::memset(a_hostip, 0, sizeof(a_hostip));

	BOOL retval = true;
	if(gethostname(a_hostname, 127) != 0)
	{
		strcpy(a_hostip, "0.0.0.0");
		retval = false;
	}
	else
	{
		hostent *p;
		p = gethostbyname(a_hostname);
		if(p == NULL)
		{
			strcpy(a_hostip, "0.0.0.0");
			retval = false;
		}
		else
		{
			strcpy(a_hostip, inet_ntoa(*(struct in_addr *) p->h_addr));
		}
	}
	myIp = a_hostip;
	return retval;
}

int copCheckLogin(CString& strSite, CString strID, CString strPW, 
				  CString& strValidationDate, CString& strCustomer)
{
	static int loginFailCount = 0;

	if(loginFailCount >= 5){
		UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG013), MB_ICONERROR);
		return -1;
	}

	cciCall aCall("login");
	//cciEntity aEntity("PM=1/Site=%s/User=%s", strSite, strID);
	cciEntity aEntity("PM=1/Site=*/User=%s", strID);
	aCall.setEntity(aEntity);

	ciString strPasswd = strPW;
	aCall.addItem("password", strPasswd);
	//aCall.addItem("via", _T("STUDIO")); // 로그남기기용

	CString via;
	GetMyIp(via);
	aCall.addItem("via", via); // 로그남기기용, 자신의 IP 가 들어감

	try {
		TraceLog(("%s", aEntity.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return -1;
		}

		int nResult = 0;

		//if(aCall.hasMore()) {
		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("failed getReply"));
				return 0;
			}

			ciLong errCode=0;
			reply.getItem("ERROR_CODE", errCode);
			if(errCode== -3 && ++loginFailCount >= 5) {
				UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG013), MB_ICONERROR);
				return -1;
			}
			if(errCode== -5){
				UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG012), MB_ICONERROR);
				return -1;
			}
			if(!copErrCheck(reply)){
				return -1;
			}
			reply.unwrap();

			// 패스워드가 오랜되었다는 경고
			reply.getItem("ERROR_CODE", errCode);
			if(errCode== -6){
				UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG011), MB_ICONWARNING);
			}			


			ciLong userType;
			if(reply.getItem("userType", userType))
			{
				nResult = userType;
			}
			else
			{
				continue;
			}
			
			ciTime validationDate;
			reply.getItem("validationDate", validationDate);

			CTime tmValidationDate(validationDate.getTime());
			strValidationDate = tmValidationDate.Format(STR_ENV_TIME);

			//ciTime lastPWChanageTime;
			//reply.getItem("lastPWChanageTime", lastPWChanageTime);

			//CTime tmLastPWChanageTime(lastPWChanageTime.getTime());
			//strLastPWChanageTime = tmLastPWChanageTime.Format(STR_ENV_TIME);

			TraceLog(("User ValidationDate [%s]", strValidationDate));

			ciString siteId;
			reply.getItem("siteId", siteId);
			TraceLog(("%s", siteId.c_str()));
			strSite = siteId.c_str();

			// 0001408: Customer 정보 입력을 받는다.
			ciString customer;
			reply.getItem("customer", customer);
			TraceLog(("%s", customer.c_str()));
			strCustomer = customer.c_str();

			// Customer가 없는 경우 선택하는 다이얼로그를 팝업한다.
			CCustomerInfoDlg dlg(strCustomer);
			if(dlg.DoModal() == IDOK)
			{
				strCustomer = dlg.GetSelectCustumerName();
				int nUserTimezoneMinute = dlg.GetSelectCustumerTimezone();

				if(nUserTimezoneMinute >= -24*60 && nUserTimezoneMinute <= 24*60)
				{
					CString strTimeZoneName;
					LONG nMinute = 9999;
					if( GetTimeZoneMinute(strTimeZoneName, nMinute) &&
						nUserTimezoneMinute != nMinute  )
					{
						int nOsTimezone = nMinute * (nMinute>=0 ? 1 : -1);
						CString strOsTimezone;
						strOsTimezone.Format(_T("(UTC%c%02d:%02d)")
										, (nMinute>=0 ? '-' : '+')
										, nOsTimezone/60
										, nOsTimezone-(nOsTimezone/60)*60
										);

						int nUserTimezone = nUserTimezoneMinute * (nUserTimezoneMinute>=0 ? 1 : -1);
						CString strUserTimezone;
						strUserTimezone.Format(_T("(UTC%c%02d:%02d)")
										, (nUserTimezoneMinute>=0 ? '-' : '+')
										, nUserTimezone/60
										, nUserTimezone-(nUserTimezone/60)*60
										);

						CString strMsg;
						strMsg.Format(LoadStringById(IDS_UBCSTUDIO_STR003)
									, strOsTimezone
									, strUserTimezone
									);

						UbcMessageBox(strMsg);
						return -3;
					}
				}

				if(!dlg.GetSelectCustumerLanguage().IsEmpty())
				{
					CString strOsLocaleName = GetLocaleName();

					TraceLog(("strOsLocaleName : %s", strOsLocaleName));
					TraceLog(("GetSelectCustumerLanguage() : %s", dlg.GetSelectCustumerLanguage()));
					TraceLog(("CompareNoCase : %d", strOsLocaleName.CompareNoCase(dlg.GetSelectCustumerLanguage())));

					if(!strOsLocaleName.IsEmpty() && strOsLocaleName.CompareNoCase(dlg.GetSelectCustumerLanguage()) != 0)
					{
						CString strMsg;
						strMsg.Format(LoadStringById(IDS_UBCSTUDIO_STR004)
									, strOsLocaleName
									, dlg.GetSelectCustumerLanguage()
									);

						UbcMessageBox(strMsg);
						return -3;
					}
				}
			}

			ciString siteList;	// 2010.10.14 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
			ciString hostList;

			if(strSite != _T("Default") || nResult != 1)
			{
				reply.getItem("siteList", siteList);
				reply.getItem("hostList", hostList);

				TraceLog(("siteList : %s", siteList.c_str()));
				TraceLog(("hostList : %s", hostList.c_str()));

				cciStringList childSitelist;
				// 사용자 계정의 경우 해당사이트만 볼수있게한다. 하위사이트에 대한 권한이 없음
				if(CEnviroment::eAuthUser == userType) childSitelist.addItem(siteId.c_str());
				else                                   GetChildSite(strCustomer, siteId.c_str(), childSitelist);

				cciStringList cciSiteList(siteList.c_str());
		
				for(int i=0; i<cciSiteList.length(); i++)
				{
					// 사용자 계정의 경우 해당사이트만 볼수있게한다. 하위사이트에 대한 권한이 없음
					if(CEnviroment::eAuthUser == userType) childSitelist.addItem(cciSiteList[i].c_str());
					else                                   GetChildSite(strCustomer, cciSiteList[i].c_str(), childSitelist);
				}

				TraceLog(("siteList : %s", childSitelist.toString()));

				siteList = childSitelist.toString();
			}

			// 2010.10.14 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
			ubcPermition::getInstance()->init(strID, userType, siteList.c_str(), hostList.c_str());
			return nResult;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return -2;
	}

	return CEnviroment::eAuthNoUser;
}

BOOL GetChildSite(CString strCustomer, CString strSiteId, cciStringList& list)
{
	if(strCustomer.IsEmpty()) strCustomer = _T("*");
	if(strSiteId.IsEmpty()) strSiteId = _T("*");

	cciCall aCall("get");
	cciEntity aEntity("PM=*/Franchize=%s/ChildSite=%s", strCustomer, strSiteId);
	aCall.setEntity(aEntity);
	
	cciAny nullAny;
	aCall.addItem("siteId", nullAny);
	aCall.wrap();

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			//if(!CopErrCheck(reply)) return FALSE;

			ciString siteId;

			reply.unwrap();
			reply.getItem("siteId", siteId);

			list.addItem(siteId.c_str());
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

bool InitSite(CString szSite, CList<CString>& lsSite)
{
	cciCall aCall("get");
	cciEntity aEntity("PM=*/Site=%s", szSite);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			//ciString mgrId, siteName, shopOpenTime, shopCloseTime;
			//CCI::CCI_StringList holiday;
			ciString siteId;

			reply.unwrap();
			//reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			//reply.getItem("siteName", siteName);
			//reply.getItem("shopOpenTime", shopOpenTime);
			//reply.getItem("shopCloseTime", shopCloseTime);
			//reply.getItem("holiday", holiday);

			if(siteId.length() == 0)
				continue;

			POSITION pos = lsSite.Find(siteId.c_str());
			if(pos == 0){
				lsSite.AddTail(siteId.c_str());
			}
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}
	return TRUE;
}

CString GetSiteName(CString szSiteId)
{
	if(szSiteId.IsEmpty()) return _T("");

	cciCall aCall("get");
	cciEntity aEntity("PM=*/Site=%s", szSiteId);
//	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			//ciString mgrId, siteName, shopOpenTime, shopCloseTime;
			//CCI::CCI_StringList holiday;
			ciString siteId, siteName;

			reply.unwrap();
			//reply.getItem("mgrId", mgrId);
			reply.getItem("siteId", siteId);
			reply.getItem("siteName", siteName);
			//reply.getItem("shopOpenTime", shopOpenTime);
			//reply.getItem("shopCloseTime", shopCloseTime);
			//reply.getItem("holiday", holiday);

			if(siteId.length() == 0)
				continue;

			return siteName.c_str();
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return _T("");
	}

	return _T("");
}

void copGetPackage(LPCTSTR szSite, LPCTSTR szWhere)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("PM=*/Site=%s/Program=*", szSite);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);
	TraceLog(("%s", aEntity.toString()));

	if(szWhere && _tcsclen(szWhere) > 0) aCall.addItem("whereClause", szWhere);

	try {
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return;
		}

		cciReply reply;
		SPackageInfo4Studio info;
		ciString szProcID, szSiteID, szPackage, szDescript, szUpdateUser;
		ciTime tmUpdateDate;

		// 패키지객체 속성추가
		ciLong		contentsCategory;	// 종류 (모닝,..)
		ciLong		purpose;			// 용도별 (교육용,..)
		ciLong		hostType;			// 단말타입 (키오스크..)
		ciLong		vertical;			// 가로/세로 방향 (가로,..)
		ciLong		resolution;			// 해상도 (1920x1080,..)
		ciBoolean	isPublic;			// 공개여부
		ciBoolean	isVerify;			// 승인여부
		ciTime	 	validationDate;		// 패키지유효기간

		while(aCall.hasMore()) {  //skpark 2010.11.04
		while(aCall.getReply(reply)){
			ciString szBuf = reply.toString();
			if(szBuf.length() < 10) continue;

			reply.unwrap();
			info.bModify = false;

			info.szDrive = STR_ENV_SERVER;
			
			reply.getItem("mgrId", szProcID);
			info.szProcID = szProcID.c_str();

			reply.getItem("siteId", szSiteID);
			info.szSiteID = szSiteID.c_str();
			
			reply.getItem("programId", szPackage);
			info.szPackage = szPackage.c_str();
			
			reply.getItem("description", szDescript);
			info.szDescript = szDescript.c_str();
			
			reply.getItem("lastUpdateId", szUpdateUser);
			info.szUpdateUser = szUpdateUser.c_str();

			reply.getItem("lastUpdateTime", tmUpdateDate);
			info.tmUpdateTime = tmUpdateDate.getTime();

			CTime tmUpdate(info.tmUpdateTime);
			info.szUpdateTime = tmUpdate.Format(STR_ENV_TIME);

			// 패키지객체 속성추가
			reply.getItem("contentsCategory", contentsCategory); info.nContentsCategory = contentsCategory;
			reply.getItem("purpose"         , purpose         ); info.nPurpose          = purpose         ;
			reply.getItem("hostType"        , hostType        ); info.nHostType         = hostType        ;
			reply.getItem("vertical"        , vertical        ); info.nVertical         = vertical        ;
			reply.getItem("resolution"      , resolution      ); info.nResolution       = resolution      ;
			reply.getItem("isPublic"        , isPublic        ); info.bIsPublic         = isPublic        ;
			reply.getItem("isVerify"        , isVerify        ); info.bIsVerify         = isVerify        ;
			reply.getItem("validationDate"  , validationDate  ); info.tmValidationDate  = validationDate.getTime();

			CTime tmValidationDate(info.tmValidationDate);
			info.strValidationDate = tmValidationDate.Format(STR_ENV_TIME);

			SPackageInfo4Studio* pInfo = new SPackageInfo4Studio;
			*pInfo = info;
			GetEnvPtr()->AddPackage(pInfo);
		}}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
	}
}

BOOL copSetPackage(std::multimap<std::string, void*>& mapSite, CString szSTime, CString szETime, std::string& errMsg)
{
	CString szTemp;
	std::string strSTime = szSTime;
	std::string strETime = szSTime;
	std::list<std::string> strList;

	std::multimap<std::string, void*>::iterator iter;

	for(iter = mapSite.begin(); iter != mapSite.end(); iter++)
	{
		SHostInfo4Studio* pInfo = (SHostInfo4Studio*)iter->second;

		std::string strSiteSide;
		strSiteSide = pInfo->hostId;
		strSiteSide += ":";
		strSiteSide += pInfo->displayNo==1?"A":"B";
		strList.push_back(strSiteSide);
	}

	return applyPackage(GetEnvPtr()->m_szLoginID, GetEnvPtr()->m_PackageInfo.szSiteID, GetEnvPtr()->m_strPackage, strSTime.c_str(), strETime.c_str(), strList,errMsg);

	//if(mapSite.empty())
	//{
	//	std::string strPackage = GetEnvPtr()->m_strPackage;
	//	applyPackage(GetEnvPtr()->m_PackageInfo.szSiteID, strPackage, strSTime, strETime, strList);
	//	return;
	//}

	//std::string strOld = "";
	//std::multimap<std::string, void*>::iterator iter;

	//for(iter = mapSite.begin(); iter != mapSite.end(); iter++){
	//	std::string strSite = iter->first;
	//	SHostInfo4Studio* pInfo = (SHostInfo4Studio*)iter->second;

	//	if(strOld != "" && strOld != strSite){
	//		std::string strPackage = GetEnvPtr()->m_strPackage;
	//		applyPackage(GetEnvPtr()->m_PackageInfo.szSiteID, strPackage, strSTime, strETime, strList);
	//		strList.clear();
	//	}else{
	//		std::string strSiteSide;
	//		strSiteSide = pInfo->hostId;
	//		strSiteSide += ":";
	//		strSiteSide += pInfo->displayNo==1?"A":"B";
	//		
	//		strList.push_back(strSiteSide);
	//	}
	//	strOld = strSite;
	//}

	//if(strOld != ""){
	//	std::string strPackage = GetEnvPtr()->m_strPackage;
	//	applyPackage(GetEnvPtr()->m_PackageInfo.szSiteID, strPackage, strSTime, strETime, strList);
	//}
}

BOOL applyPackage(CString strUserId, CString strSite, CString strPackage, CString strSTime, CString strETime, std::list<std::string> strList, std::string& errMsg)
{
	CString szTemp;
	cciCall aCall("apply");
	szTemp.Format("PM=*/Site=%s/Program=%s", strSite, strPackage);
	cciEntity aEntity(szTemp);
	aCall.setEntity(aEntity);

	cciStringList strTemp(strList);
	aCall.addItem("hostIdList", strTemp.get());
	aCall.addItem("applyTime", strSTime);
	aCall.addItem("endTime", strETime);
	aCall.addItem("userId", strUserId);	// 로그에 남기기 위해 추가함

	TraceLog(("%s", aEntity.toString()));

	try {
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}

		cciReply reply;
		ciLong lPort = 0;
		ciString strBuf;

		ciLong iRet=0;
		ciString strRet;

		while(aCall.getReply(reply))
		{
			ciString szBuf = reply.toString();
			if(szBuf.length() < 10) continue;

			reply.unwrap();
			reply.getItem("ERROR_CODE", iRet);
			reply.getItem("ERROR_MSG", strRet);
			break;
		}

		if(iRet <= 0)
		{
			// todo 여기에 에러처리
			errMsg = strRet;
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL getCodeList(CodeItemList& list, CString strCustomer)
{
	cciCall aCall("bulkget");
	cciEntity aEntity("OD=OD/Code=*");
	aCall.setEntity(aEntity);

	CString strWhere;
	strWhere.Format(_T("customer='%s' order by categoryName, dorder, enumNumber"), strCustomer);
	aCall.addItem("whereClause", strWhere);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			ciString mgrId, codeId, categoryName, enumString;
			ciLong enumNumber;
			ciBoolean visible;
			ciShort dorder;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("codeId", codeId);
			reply.getItem("categoryName", categoryName);
			reply.getItem("enumString", enumString);
			reply.getItem("enumNumber", enumNumber);
			reply.getItem("visible", visible);
			reply.getItem("dorder", dorder);

			SCodeItem item;
			item.strMgrId = mgrId.c_str();
			item.strCodeId = codeId.c_str();
			item.strCategoryName = categoryName.c_str();
			item.strEnumString = enumString.c_str();
			item.nEnumNumber = enumNumber;
			item.bVisible = visible;
			item.nDisplayOrder = dorder;

			list.AddTail(item);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL getCustomerList(CustomerInfoList& list)
{
	cciCall aCall("get");
	cciEntity aEntity("OD=OD/Customer=*");
	aCall.setEntity(aEntity);

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			ciString mgrId, customerId, url, description, language;
			ciShort gmt=9999;

			reply.unwrap();
			reply.getItem("mgrId", mgrId);
			reply.getItem("customerId", customerId);
			reply.getItem("url", url);
			reply.getItem("description", description);
			reply.getItem("gmt", gmt);
			reply.getItem("language", language);

			SCustomerInfo item;
			item.mgrId = mgrId.c_str();
			item.customerId = customerId.c_str();
			item.url = url.c_str();
			item.description = description.c_str();
			item.gmt = gmt;
			item.language = language.c_str();

			item.url.Replace("\r\n","\n");
			item.url.Replace("\n","\r\n");

			list.AddTail(item);
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

// 인자로 받은 패키지내에서 유일하게 사용되고있다면 move, 아니면 copy하여 처리한다.
BOOL copyContents(CString strSrcId, CString strDstId, CString strPackage, CStringList& children)
{
	cciCall aCall("copyContents");
	cciEntity aEntity("PM=1");
	aCall.setEntity(aEntity);

	aCall.addItem("source", strSrcId);
	aCall.addItem("target", strDstId);
	aCall.addItem("package", strPackage);
	
	if(!children.IsEmpty()){
		cciStringList cciTemp;
		POSITION p = children.GetHeadPosition();
		while(p){
			CString child  = children.GetNext(p);
			if(!child.IsEmpty()){
				cciTemp.addItem(child);
			}
		}
		aCall.addItem("children", cciTemp.get());
	}
	

	TraceLog(("%s", aEntity.toString()));

	CWaitMessageBox wait;

	try {
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL GetAllCategory(CStringList& aCategoryList)
{
	cciCall aCall("getCategory");
	cciEntity aEntity("OD=OD");
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	CString strProgramList;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call()){
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("failed call"));
			return FALSE;
		}

		while(aCall.hasMore()) {
			cciReply reply;
			if ( ! aCall.getReply(reply)) {
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(&reply)) return FALSE;

			cciStringList categoryList;

			reply.unwrap();
			reply.getItem("categoryList", categoryList);

			for(int i=0; i<categoryList.length() ;i++)
			{
				aCategoryList.AddTail(categoryList[i].c_str());
			}
			ciString buf = categoryList.toString();
			TraceLog(("category=%s", buf.c_str()));
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("%s", ex._name()));
		return FALSE;
	}

	return TRUE;
}

BOOL CopErrCheck(cciReply* reply, bool bShowMsgBox/*=true*/)
{
	ciLong errCode=0;
	reply->getItem("ERROR_CODE", errCode);
	ciString errMsg;
	reply->getItem("ERROR_MSG", errMsg);

	if(errCode < 0 )
	{
		TraceLog(("ERROR_CODE [%d]", errCode));
		TraceLog(("ERROR_MSG [%s]", errMsg.c_str()));

		// Display this message  :      skpark 20100621
		CString strMsg;
		strMsg.Format(_T("%s[code:%d]"), errMsg.c_str(), errCode);
		if(bShowMsgBox) UbcMessageBox(strMsg, MB_ICONERROR);
		return FALSE;
	}

	return TRUE;
}
// 2010.08.06 by gwangsoo
BOOL IsExistPackage(LPCTSTR szPackage, BOOL &bExist)
{
	cciCall aCall("get");
	cciEntity aEntity("PM=*/Site=*/Program=%s", szPackage);
	aCall.setFilter(ubcPermition::getInstance()->getFilter()); // 0000679: 두개 이상의 사이트를 관리하는 User 를 만들수 있어야 한다.
	aCall.setEntity(aEntity);

	bExist = FALSE;

	try {
		TraceLog(("%s", aCall.toString()));
		if(!aCall.call())
		{
			if(aCall.getError() && _tcslen(aCall.getError()) > 0)
			{
				UbcMessageBox(aCall.getError());
			}
			TraceLog(("Failed : cciCall::call()"));
			return FALSE;
		}

		while(aCall.hasMore())
		{
			cciReply reply;
			if ( ! aCall.getReply(reply))
			{
				TraceLog(("Failed : cciCall::getReply()"));
				continue;
			}

			if(!CopErrCheck(&reply)) return FALSE;

			if(reply.length() == 0)
				continue;

			bExist = TRUE;
			break;
		}
	}catch(CORBA::Exception& ex){
		TraceLog(("Catch CORBA::Exception (%s)", ex._name()));
		return FALSE;
	}

	return TRUE;
}

