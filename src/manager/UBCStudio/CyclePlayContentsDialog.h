#pragma once
#include "afxdtctl.h"
#include "afxwin.h"

#include "SubPlayContentsDialog.h"

// CCyclePlayContentsDialog 대화 상자입니다.

class CCyclePlayContentsDialog : public CSubPlayContentsDialog
{
	DECLARE_DYNAMIC(CCyclePlayContentsDialog)

public:
	CCyclePlayContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCyclePlayContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CYCLE_PLAYCONTENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	int		m_nTimeScope;
	bool	m_bEditMode;
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정
	PLAYCONTENTS_INFO*	m_pPlayContentsInfo;
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정

public:

	virtual bool	GetPlayContentsInfo(PLAYCONTENTS_INFO& info, int nIndex);
	virtual bool	SetPlayContentsInfo(PLAYCONTENTS_INFO& info);
	virtual int		GetPlayContentsCount();

	afx_msg void OnBnClickedCheckTimeAll();
	afx_msg void OnBnClickedCheckTime();

	CDateTimeCtrl	m_dateStart;
	CDateTimeCtrl	m_dateEnd;

	CButton		m_checkTime[8];
	CButton m_dontDownloadCheck;
};
