#pragma once
#include "afxwin.h"

#include "ReposControl.h"
#include "Template_Wnd.h"

// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업
#include "common/HoverButton.h"
// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업


class CUBCStudioDoc;

// CSelectTemplateDialog 대화 상자입니다.

class CSelectTemplateDialog : public CDialog
{
	DECLARE_DYNAMIC(CSelectTemplateDialog)

public:
	CSelectTemplateDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSelectTemplateDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SELECT_TEMPLATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CReposControl	m_reposControl;
	CUBCStudioDoc*	m_pDocument;

	CTemplate_Wnd	m_wndTemplate;

public:

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

	LRESULT	OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam);
	LRESULT	OnTemplateSelected(WPARAM wParam, LPARAM lParam);

	CStatic m_frameTemplate;

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	/*
	CButton m_btnOK;
	CButton m_btnCancel;
	*/
	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	void	SetDocument(CUBCStudioDoc* pDocument) { m_pDocument = pDocument; };
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
};
