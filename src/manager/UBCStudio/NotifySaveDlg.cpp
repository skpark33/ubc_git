// NotifySaveDlg.cpp : 구현 파일입니다.
//
#include "stdafx.h"
#include "UBCStudio.h"
#include "NotifySaveDlg.h"

// CNotifySaveDlg 대화 상자입니다.
IMPLEMENT_DYNAMIC(CNotifySaveDlg, CDialog)

CNotifySaveDlg::CNotifySaveDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNotifySaveDlg::IDD, pParent)
{
	m_Mode = eSavePackage;
}

CNotifySaveDlg::~CNotifySaveDlg()
{
#ifndef _DEBUG
	m_Image.Stop();
	m_Image.UnLoad();
#endif
}

void CNotifySaveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CNotifySaveDlg, CDialog)

END_MESSAGE_MAP()


// CNotifySaveDlg 메시지 처리기입니다.

BOOL CNotifySaveDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_Image.Create("Animated GIF", WS_CHILD|WS_VISIBLE, CRect(0, 0, 0, 0), this);

	switch(m_Mode)
	{
	case ePreview:
		m_Image.Load(MAKEINTRESOURCE(IDR_PREPARE_PREVIEW_GIF),_T("GIF"));
		break;
	case eSavePackage:
		m_Image.Load(MAKEINTRESOURCE(IDR_PREPARE_SAVE_GIF),_T("GIF"));
		break;
	case eLoadPackage:
		m_Image.Load(MAKEINTRESOURCE(IDGIF_LOADPACKAGE),_T("GIF"));
		break;
	case eDownPackage:
		m_Image.Load(MAKEINTRESOURCE(IDGIF_DOWNPACKAGE),_T("GIF"));
		break;
	case eUpPackage:
		m_Image.Load(MAKEINTRESOURCE(IDGIF_UPPACKAGE),_T("GIF"));
		break;
	case eDownContents:
		m_Image.Load(MAKEINTRESOURCE(IDGIF_DOWNCONTENTS),_T("GIF"));
		break;
	case eUpContents:
		m_Image.Load(MAKEINTRESOURCE(IDGIF_UPCONTENTS),_T("GIF"));
		break;
	default:
		m_Image.Load(MAKEINTRESOURCE(IDR_PREPARE_SAVE_GIF),_T("GIF"));
		break;
	}

	SIZE stSize = m_Image.GetSize();
	this->MoveWindow(0, 0, stSize.cx, stSize.cy);
	this->CenterWindow();

	m_Image.ShowWindow(SW_SHOW);
	m_Image.Draw();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Save, preview 모드 설정(true : save mode, false : preview mode) \n
/// @param (bool) bSave : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CNotifySaveDlg::SetMode(EMode eMode)
{
	m_Mode = eMode;
}
