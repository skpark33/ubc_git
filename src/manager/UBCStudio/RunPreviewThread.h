#pragma once



// CRunPreviewThread

class CRunPreviewThread : public CWinThread
{
	DECLARE_DYNCREATE(CRunPreviewThread)

protected:
	CRunPreviewThread();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CRunPreviewThread();

protected:
	void*	m_pParent;			///<Parent window
	void*	m_pParentDoc;		///<Parent document
	CString	m_strTemplateID;	///<TemplateId to preview
	bool	m_bSave;			///<Falsg to be save

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	void	SetThreadParam(void* pParent, void* pParentDoc, CString strTemplateID, bool bSave);	///<Set parent params

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual int Run();
};


