// DeletePackageDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "DeletePackageDlg.h"
#include "LocalHostInfo.h"
#include "MainFrm.h"


// CDeletePackageDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDeletePackageDlg, CDialog)

CDeletePackageDlg::CDeletePackageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDeletePackageDlg::IDD, pParent)
{
}

CDeletePackageDlg::~CDeletePackageDlg()
{
}

void CDeletePackageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PACKAGE_LIST, m_lcPackageList);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, ID_DELETE_BTN, m_btnDelete);
}


BEGIN_MESSAGE_MAP(CDeletePackageDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CDeletePackageDlg::OnBnClickedOk)
	ON_BN_CLICKED(ID_DELETE_BTN, &CDeletePackageDlg::OnBnClickedDeleteBtn)
END_MESSAGE_MAP()


// CDeletePackageDlg 메시지 처리기입니다.

void CDeletePackageDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	OnOK();
}

BOOL CDeletePackageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btnDelete.LoadBitmap(IDB_BTN_DELETE, RGB(255, 255, 255));
	m_btnOK.LoadBitmap(IDB_BTN_CLOSE, RGB(255, 255, 255));

	m_btnDelete.SetToolTipText(LoadStringById(IDS_DELETEPACKAGEDLG_BUT001));
	m_btnOK.SetToolTipText(LoadStringById(IDS_DELETEPACKAGEDLG_BUT002));

	CBitmap bmp;
	bmp.LoadBitmap(IDB_CHECK);

	if(m_imgCheck.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_imgCheck.Add(&bmp, RGB(255,255,255)); //바탕의 회색이 마스크
	}//if
	m_lcPackageList.SetImageList(&m_imgCheck, LVSIL_SMALL);

	m_lcPackageList.SetExtendedStyle(m_lcPackageList.GetExtendedStyle()|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES|LVS_EX_SUBITEMIMAGES);
	
	CRect clsRect;
	m_lcPackageList.GetClientRect(&clsRect);
	int nWidth = clsRect.Width();
	m_lcPackageList.InsertColumn(0, LoadStringById(IDS_DELETEPACKAGEDLG_LST001), LVCFMT_LEFT, (int)((clsRect.Width()/10)*1.5));
	m_lcPackageList.InsertColumn(1, LoadStringById(IDS_DELETEPACKAGEDLG_LST002), LVCFMT_LEFT, (int)((clsRect.Width()/10)*1.5));
	m_lcPackageList.InsertColumn(2, LoadStringById(IDS_DELETEPACKAGEDLG_LST003), LVCFMT_LEFT, (int)((clsRect.Width()/10)*2.5));
	m_lcPackageList.InsertColumn(3, LoadStringById(IDS_DELETEPACKAGEDLG_LST004), LVCFMT_LEFT, (int)((clsRect.Width()/10)*2));
	m_lcPackageList.InsertColumn(4, LoadStringById(IDS_DELETEPACKAGEDLG_LST005), LVCFMT_LEFT, (int)((clsRect.Width()/10)*2.5));

	m_lcPackageList.Initialize(false);

	InitList();

	//드라이브명으로 정렬 시킨다
	m_lcPackageList.Sort(0, true);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Host list를 list control에 설정 \n
/////////////////////////////////////////////////////////////////////////////////
void CDeletePackageDlg::InitList()
{
	m_lcPackageList.DeleteAllItems();

	CMainFrame* pFrm = (CMainFrame*)AfxGetMainWnd();
	pFrm->MakeLocalHostInfoArray();
	if(pFrm->m_aryLocalHostInfo.GetCount() == 0)
	{
		return;
	}//if

	int nIdx;
	CString strDrive, strNetworkUse, strHostName, strDesc, strSite, strWrittenTime, strWrittenID;
	long nContentsCategory, nPurpose, nHostType, nVertical, nResolution;
	bool bIsPublic, bIsVerify;
	CString strValidationDate;

	CLocalHostInfo* pclsHost = NULL;
	for(int i=0; i<pFrm->m_aryLocalHostInfo.GetCount(); i++)
	{
		pclsHost = (CLocalHostInfo*)pFrm->m_aryLocalHostInfo.GetAt(i);
		pclsHost->GetLocalHostInfo(strDrive, strNetworkUse, strHostName, strDesc, strSite, strWrittenTime, strWrittenID
								 , nContentsCategory, nPurpose, nHostType, nVertical, nResolution, bIsPublic, bIsVerify, strValidationDate);

		//샘플파일과 preview 파일은 보여주지 않는다
		if(strHostName.Find(SAMPLE_FILE_KEY) == 0) continue;
		if(strHostName.Find(PREVIEW_FILE_KEY) == 0) continue;
		if(strHostName.Find(TEMPORARY_SAVE_FILE_KEY) == 0) continue;

		int nDI = 7;
		UINT type = GetDriveType(strDrive);
		switch(type)
		{
		case DRIVE_REMOVABLE:	nDI = 6;	break;
		case DRIVE_FIXED:		nDI = 7;	break;
		case DRIVE_REMOTE:		nDI = 8;	break;
		case DRIVE_CDROM:		nDI = 10;	break;
		case DRIVE_RAMDISK:		nDI = 11;	break;
		}
		nIdx = m_lcPackageList.InsertItem(i, strDrive, nDI);

		LVITEM item;
		item.mask = LVIF_TEXT|LVIF_IMAGE;
		item.iItem = nIdx;
		item.iSubItem = 1;

		if(strNetworkUse == "1")
		{
			item.iImage = 3;
			item.pszText = "Server";
		}
		else
		{
			item.iImage = 2;
			item.pszText = "User";
		}//if
		m_lcPackageList.SetItem(&item);
		m_lcPackageList.SetItemText(nIdx, 2, strHostName);
		m_lcPackageList.SetItemText(nIdx, 3, strDesc);
		m_lcPackageList.SetItemText(nIdx, 4, strWrittenTime);
		m_lcPackageList.SetItemData(i, (DWORD)pclsHost);
	}//for
}


void CDeletePackageDlg::OnBnClickedDeleteBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CStringArray aryPackage;
	CMapStringToString mapPackage;
	CString strPath, strDrive, strPackage;
	for(int i=0; i<m_lcPackageList.GetItemCount(); i++)
	{
		if(m_lcPackageList.GetCheck(i))
		{
			strDrive = m_lcPackageList.GetItemText(i, 0);
			strPackage = m_lcPackageList.GetItemText(i, 2);
			strPath.Format("%s%s%s.ini", strDrive, UBC_CONFIG_PATH, strPackage);
			aryPackage.Add(strPath);
			mapPackage.SetAt(strPath, strPackage);
		}//if
	}//for

	if(aryPackage.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_DELETEPACKAGEDLG_MSG001), MB_ICONWARNING);
		return;
	}//if

	CString strVal;
	CString strMsg = "Package : ";
	bool bFail = false;
	for(int i=0; i<aryPackage.GetCount(); i++)
	{
		strPath = aryPackage.GetAt(i);
		if(!DeleteFile(strPath))
		{
			bFail = true;
			if(mapPackage.Lookup(strPath, strVal))
			{
				strMsg += "[ ";
				strMsg += strVal;
				strMsg += " ] ";
			}//if
		}//if
	}//for

	if(!bFail)
	{
		UbcMessageBox(LoadStringById(IDS_DELETEPACKAGEDLG_MSG002), MB_ICONINFORMATION);
	}
	else
	{
		strMsg.TrimRight();
		CString strTmp;
		strTmp.Format(LoadStringById(IDS_DELETEPACKAGEDLG_MSG003), strMsg);
		UbcMessageBox(strTmp, MB_ICONERROR);
	}//if

	InitList();
}
