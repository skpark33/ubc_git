// FlashImagePlayerContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "FlashImagePlayerContentsDialog.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "common/UbcGUID.h"
#include "common/UbcImageList.h"
#include <IMAGEHLP.H>
#pragma comment(lib, "imagehlp.lib")
#include "common/MD5Util.h"

#ifdef _UBCSTUDIO_EE_
#include "ubccopcommon\ftpmultisite.h"
#endif//_UBCSTUDIO_EE_


#include <io.h>
#include <sys/stat.h>
#include "shlwapi.h"
#include "common\libscratch\scratchUtil.h"

#include "FlashImageplayerEffectsOderChangeDlg.h"

// CFlashImagePlayerContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFlashImagePlayerContentsDialog, CSubContentsDialog)

CFlashImagePlayerContentsDialog::CFlashImagePlayerContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CFlashImagePlayerContentsDialog::IDD, pParent)
	, m_wndFlash (NULL, 0, false)
	, m_reposControl (this)
	, m_strLocation("")
	, m_bPreviewMode(false)
	, m_bPermanent(FALSE)
	, m_pDlg ( NULL )
{
	m_ulFileSize = 0;
	m_brushBG.CreateSolidBrush(RGB(255,255,255));
}

CFlashImagePlayerContentsDialog::~CFlashImagePlayerContentsDialog()
{
	DeleteSubFilesContents();
}

void CFlashImagePlayerContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FOLDER_NAME, m_editContentsFolderName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_INTERVAL, m_editContentsPlayInterval);
	DDX_Control(pDX, IDC_LIST_FILES, m_lcFiles);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_ADD_FILE, m_btnBrowseAddFile);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_DEL_FILE, m_btnBrowseDelFile);
	DDX_Control(pDX, IDC_BUTTON_DOWNLOAD_CONTENTS, m_btnDownloadContents);
	DDX_Control(pDX, IDC_BUTTON_MOVE_UP, m_btnMoveUp);
	DDX_Control(pDX, IDC_BUTTON_MOVE_DOWN, m_btnMoveDown);
	DDX_Control(pDX, IDC_EDIT_FELICA_URL, m_editFelicaUrl);
	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_groupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_EDIT_EFFECTS_ORDER, m_editEffectsOrder);
}


BEGIN_MESSAGE_MAP(CFlashImagePlayerContentsDialog, CSubContentsDialog)
	ON_WM_CTLCOLOR()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, &CFlashImagePlayerContentsDialog::OnBnClickedPermanentCheck)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_FILES, &CFlashImagePlayerContentsDialog::OnNMDblclkListFiles)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_FILES, &CFlashImagePlayerContentsDialog::OnLvnKeydownListFiles)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_ADD_FILE, &CFlashImagePlayerContentsDialog::OnBnClickedButtonBrowserAddFile)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_DEL_FILE, &CFlashImagePlayerContentsDialog::OnBnClickedButtonBrowserDelFile)
	ON_BN_CLICKED(IDC_BUTTON_DOWNLOAD_CONTENTS, &CFlashImagePlayerContentsDialog::OnBnClickedButtonDownloadContents)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_UP, &CFlashImagePlayerContentsDialog::OnBnClickedButtonMoveUp)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_DOWN, &CFlashImagePlayerContentsDialog::OnBnClickedButtonMoveDown)
	ON_BN_CLICKED(IDC_BUTTON_CHANGE_EFFECTS_ORDER, &CFlashImagePlayerContentsDialog::OnBnClickedButtonChangeEffectsOrder)
	ON_MESSAGE(WM_UPDATE_EFFECTS_ORDER, OnUpdateEffectsOrder)
END_MESSAGE_MAP()


// CFlashImagePlayerContentsDialog 메시지 처리기입니다.

BOOL CFlashImagePlayerContentsDialog::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	m_btnBrowseAddFile  .LoadBitmap(IDB_BTN_PLUS , RGB(255, 255, 255));
	m_btnBrowseDelFile  .LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnMoveUp			.LoadBitmap(IDB_BTN_UP, RGB(255, 255, 255));
	m_btnMoveDown		.LoadBitmap(IDB_BTN_DOWN, RGB(255, 255, 255));

	m_btnBrowseAddFile  .SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN004));
	m_btnBrowseDelFile  .SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN005));

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		m_btnDownloadContents.LoadBitmap(IDB_CONT_DOWN, RGB(255, 255, 255));
		m_btnDownloadContents.SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN007));
	}
	else
	{
		m_btnDownloadContents.EnableWindow(FALSE);
		m_btnDownloadContents.ShowWindow(SW_HIDE);
	}

	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(0);

	//
	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);
	client_rect.DeflateRect(1,1);

	m_wndFlash.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndFlash.ShowWindow(SW_SHOW);

	m_reposControl.AddControl(&m_groupPreview, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_staticContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_wndFlash, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.Move();

	//
	m_listNoCTLWnd.Add((CWnd*)&m_editContentsFolderName);

	InitSubFilesCtrl();

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}//if

	// 부속파일처리 관련 수정
	CONTENTS_INFO info;
	SetContentsInfo(info);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFlashImagePlayerContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnOK();
}

void CFlashImagePlayerContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnCancel();
	GetParent()->PostMessage(WM_CLOSE);
}

void CFlashImagePlayerContentsDialog::Stop()
{
	//m_wndFlash.Stop(false);
}

bool CFlashImagePlayerContentsDialog::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData(TRUE);

	CopyFlashImagePlayerFile(); // imagePlayer_{(GUID)}.swf 파일 복사
	UpdateDataXML(); // pictureViewer.xml 파일 업데이트

	// 부속파일처리 관련 수정
	info.strId = m_strContentsId;

	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	info.strLocalLocation = m_strLocation;
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();

	//
	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(info.strLocalLocation, dir, path, filename, ext);

	info.strLocalLocation.Format("%s%s", dir, path); 
	info.strFilename.Format("%s%s", filename, ext);
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	info.nPlaySpeed = m_editContentsPlayInterval.GetValueInt();
	info.strWizardXML = GetDataXML();

	//
	if(!IsFitContentsName(info.strContentsName)) return false;
	if( info.strLocalLocation.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG002), MB_ICONSTOP);
		return false;
	}
	if( info.strFilename.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG003), MB_ICONSTOP);
		return false;
	}
	if(info.nRunningTime == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
		return false;
	}

	m_editFelicaUrl.GetWindowText(info.strComment[1]);
	info.strComment[1].Trim();
	if(!info.strComment[1].IsEmpty())
	{
		CString strFelicaUrl = info.strComment[1];
		strFelicaUrl.MakeLower();
		if(strFelicaUrl.Find(_T("http://")) < 0)
		{
			UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG007), MB_ICONSTOP);
			return false;
		}
	}

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return false;

	CONTENTS_INFO_MAP* contents_map = pDoc->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;

		contents_map->RemoveKey(strFileKey);
		delete pContentsInfo;
	}

	pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		contents_map->SetAt(strFileKey, (void*&)pContentsInfo);

		// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
		// 서버에 등록된 컨텐츠인경우 파일이 변경되면 ID를 변경하도록 한다.
		if( m_bIsChangedFile &&
		   !pContentsInfo->strServerLocation.IsEmpty() &&
			pContentsInfo->strServerLocation.MakeLower().CompareNoCase("\\contents\\enc\\") < 0)
		{
			pContentsInfo->strId = (CUbcGUID::GetInstance()->ToGUID(_T("")));
		}
	}

	m_mapSubFilesContents.RemoveAll();

	return true;
}

bool CFlashImagePlayerContentsDialog::SetContentsInfo(CONTENTS_INFO& info)
{
	// 부속파일처리 관련 수정
	m_strContentsId = info.strId;
	if(m_strContentsId.IsEmpty()) m_strContentsId = CUbcGUID::GetInstance()->ToGUID(_T(""));
	m_editContentsName.SetWindowText(info.strContentsName);
	m_strLocation = info.strLocalLocation + info.strFilename;
	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	m_editFelicaUrl.SetWindowText(info.strComment[1]);

	//
	CString str_sub_folder_path;
	if( info.strFilename.GetLength() == 0 )
	{
		// imagePlayer_{FIP-(GUID)}.swf 파일이름 생성
		str_sub_folder_path = "imagePlayer_" + m_strContentsId;
		str_sub_folder_path.Insert(13, "FIP-"); //21, "-IP"
	}
	else
	{
		// imagePlayer_{FIP-(GUID)}.swf 파일이름만 추출 -> 서브폴더(이미지저장용)
		char drv[5]={0},path[MAX_PATH]={0},fn[MAX_PATH]={0},ext[MAX_PATH]={0};
		_splitpath(info.strFilename, drv, path, fn, ext);
		str_sub_folder_path = fn;
	}
	m_editContentsFolderName.SetWindowText(str_sub_folder_path);

	if( info.nPlaySpeed == 0 ) info.nPlaySpeed = 5;
	m_editContentsPlayInterval.SetWindowText(::ToString(info.nPlaySpeed));

	m_strDataXML = info.strWizardXML;
	CString str_data_xml = m_strDataXML;
	str_data_xml.Trim(" \t\r\n");

	m_listEffectsOrder.RemoveAll();
	if( str_data_xml.GetLength() == 0 )
	{	// add all effects
		m_listEffectsOrder.Add(EFFECTS_MOSAIC);
		m_listEffectsOrder.Add(EFFECTS_DISSOLVE);
		m_listEffectsOrder.Add(EFFECTS_PERSPECTIVE);
		m_listEffectsOrder.Add(EFFECTS_IRIS);
		m_listEffectsOrder.Add(EFFECTS_REVOLVE);
		m_listEffectsOrder.Add(EFFECTS_PUSH);
		m_listEffectsOrder.Add(EFFECTS_ZOOM);
		m_listEffectsOrder.Add(EFFECTS_CUBE);
		m_listEffectsOrder.Add(EFFECTS_WIPE);
	}
	else
	{
		int start_pos = 0;
		int end_pos = 0;
		while(1)
		{
			start_pos = m_strDataXML.Find("<effect>", start_pos);
			if(start_pos < 0) break;
			end_pos = m_strDataXML.Find("</effect>", start_pos);
			if(end_pos < 0) break;
			start_pos += 8;

			CString str_filename = m_strDataXML.Mid(start_pos, end_pos-start_pos);
			str_filename.Trim(" \t\r\n");
			m_listEffectsOrder.Add(str_filename);
		}

		// 플래시 ImagePlayer.swf버그로 인해 맨앞effect를 맨뒤로 이동
		if( m_listEffectsOrder.GetCount() > 0 )
		{
			CString str_effect = m_listEffectsOrder.GetAt(0);
			m_listEffectsOrder.RemoveAt(0);
			m_listEffectsOrder.Add(str_effect);
		}
	}
	m_editEffectsOrder.SetWindowText(CFlashImageplayerEffectsOderChangeDlg::GeEffectsOrderString(m_listEffectsOrder));

	if( info.strId.GetLength() == 0 )
	{
		// imagePlayer_{FIP-(GUID)}.swf 복사본 전체경로
		info.strFilename = str_sub_folder_path + ".swf";
		m_strLocation = GetEnvPtr()->m_PackageInfo.szDrive;
		m_strLocation += UBC_CONTENTS_PATH;//"..\\..\\contents\\enc\\";
		m_strLocation += info.strFilename;

		// imagePlayer.swf 원본 전체경로
		m_strSourceLocation = GetAppPath();
		m_strSourceLocation += "flash\\imagePlayer.swf";

		// 파일크기
		struct _stati64 stat = {0};
		_stati64((LPCSTR)m_strSourceLocation, &stat);
		m_ulFileSize = stat.st_size;
	}

	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}//if

	CreateSubFilesContents();
	str_sub_folder_path += "/";
	RefreshFilesList(str_sub_folder_path);

	UpdateDataXML(false); // pictureViewer.xml 파일생성

	UpdateData(FALSE);

	LoadContents(info.strLocalLocation + info.strFilename);

	return true;
}

HBRUSH CFlashImagePlayerContentsDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CSubContentsDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if(	pWnd->GetSafeHwnd() != m_editContentsFolderName.GetSafeHwnd())
	{
		pDC->SetBkColor(RGB(255,255,255));
		//pDC->SetBkMode(TRANSPARENT);

		hbr = (HBRUSH)m_brushBG;
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CFlashImagePlayerContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CSubContentsDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();

	if(m_wndFlash.GetSafeHwnd() && m_wndFlash.m_pFlash != NULL && m_wndFlash.m_pFlash->GetSafeHwnd() )
	{
		CRect rect;
		m_wndFlash.GetClientRect(rect);
		m_wndFlash.m_pFlash->MoveWindow(rect);
	}
}

bool CFlashImagePlayerContentsDialog::LoadContents(LPCTSTR lpszFullPath)
{
	CFileStatus fs;
//	if(CFile::GetStatus(lpszFullPath, fs))
	if(CEnviroment::GetFileSize(lpszFullPath, fs.m_size))
	{
		m_ulFileSize = fs.m_size;

		m_wndFlash.Stop(false);
		m_wndFlash.m_strMediaFullPath = lpszFullPath;
		m_wndFlash.OpenFile(1);
		m_wndFlash.Play();

		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨트롤의 사용가능여부 설정 \n
/// @param (BOOL) bEnable : (in) TRUE : 사용가능, FALSE : 사용 불가
/////////////////////////////////////////////////////////////////////////////////
void CFlashImagePlayerContentsDialog::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editContentsFolderName.EnableWindow(bEnable);
	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	m_btnBrowseContentsFile.EnableWindow(bEnable);
	CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
	pBtn->EnableWindow(FALSE);
	m_editFelicaUrl.EnableWindow(bEnable);

	m_btnBrowseAddFile  .EnableWindow(bEnable);
	m_btnBrowseDelFile  .EnableWindow(bEnable);
	m_btnMoveUp         .EnableWindow(bEnable);
	m_btnMoveDown       .EnableWindow(bEnable);
}

void CFlashImagePlayerContentsDialog::OnBnClickedPermanentCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//24시간을 설정
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText("");
		m_editContentsPlaySecond.SetWindowText("15");

		m_editContentsPlayMinute.EnableWindow(TRUE);
		m_editContentsPlaySecond.EnableWindow(TRUE);
	}//if
}

BOOL CFlashImagePlayerContentsDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CFlashImagePlayerContentsDialog::InitSubFilesCtrl()
{
	m_lcFiles.SetImageList(CUbcImageList::GetInstance()->GetImageList(), LVSIL_SMALL);

	m_lcFiles.SetExtendedStyle(m_lcFiles.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_lcFiles.InsertColumn(0, LoadStringById(IDS_FLASHCONTENTSDIALOG_STR001), LVCFMT_LEFT, 250);
	m_lcFiles.InsertColumn(1, LoadStringById(IDS_FLASHCONTENTSDIALOG_STR002), LVCFMT_RIGHT, 90);

	m_lcFiles.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CFlashImagePlayerContentsDialog::CreateSubFilesContents()
{
	DeleteSubFilesContents();

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return;

	CONTENTS_INFO_MAP* contents_map = pDoc->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;
		if(pContentsInfo->strLocalLocation.IsEmpty()) continue;

		CString strFileKey = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
		strFileKey.Replace(_T("\\"), _T("/"));
		strFileKey.MakeLower();
		if(m_mapSubFilesContents[strFileKey] != NULL) continue;

		// 임시 데이터 구조에 리스트를 넣는다
		CONTENTS_INFO* pBakContentsInfo = new CONTENTS_INFO;
		*pBakContentsInfo = *pContentsInfo;
		m_mapSubFilesContents[strFileKey] = (void*&)pBakContentsInfo;
	}
}

void CFlashImagePlayerContentsDialog::DeleteSubFilesContents()
{
	POSITION pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo = NULL;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );
		if(pContentsInfo) delete pContentsInfo;
	}
	m_mapSubFilesContents.RemoveAll();
}

void CFlashImagePlayerContentsDialog::RefreshFilesList(CString strLocation)
{
	m_lcFiles.DeleteAllItems();

	POSITION pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;
		if(pContentsInfo->strLocalLocation.IsEmpty()) continue;

		// pictureViewer.xml 파일은 자동생성되는 파일이므로 보여주지 않음
		if( stricmp(pContentsInfo->strFilename, "pictureViewer.xml") == 0 ) continue;

		CString strChildPath;
		CString strContentsPath = UBC_CONTENTS_PATH;
		int nFindPos = pContentsInfo->strLocalLocation.MakeLower().Find(strContentsPath.MakeLower());
		if(nFindPos >= 0)
		{
			strChildPath = pContentsInfo->strLocalLocation.Mid(nFindPos + strContentsPath.GetLength());
		}

		if(strChildPath.MakeLower() != strLocation.MakeLower()) continue;

		int nRow = m_lcFiles.GetItemCount();
		m_lcFiles.InsertItem(nRow, _T(""));
		UpdateFilesListRow(nRow, pContentsInfo);
	}

	ReorderFileListFromDataXML(m_strDataXML); // pictureViewer.xml에 나열되어있는 순서대로 정렬
}

void CFlashImagePlayerContentsDialog::UpdateFilesListRow(int nRow, CONTENTS_INFO* pContentsInfo)
{
	if(!pContentsInfo) return;

	CString strFullPath = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;

	TCHAR drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(strFullPath, drive, path, filename, ext);

	CFileStatus fs;
	CString strFileSize;
	int nIconIndex = CUbcImageList::GetInstance()->GetFileIconIndex(ext);
	int nOverlapIdx = 0;
	// 로컬에 파일이 존재하는 경우
	//if(CFile::GetStatus(strFullPath, fs))
	if(CEnviroment::GetFileSize(strFullPath, fs.m_size))
	{
		strFileSize = ::ToFileSize(fs.m_size, 'A');
	}
	// 서버에 파일이 존재하는 경우
	else if(pContentsInfo->bServerFileExist)
	{
		strFileSize = ::ToFileSize(pContentsInfo->nFilesize, 'A');
		nOverlapIdx = SERVER_IMG_IDX;
	}
	// 로컬과 서버에 모두 존재하지 않는 경우
	else
	{
		strFileSize = ::ToFileSize(pContentsInfo->nFilesize, 'A');
		nOverlapIdx = NIX_IMG_IDX;
	}

	m_lcFiles.SetItem(nRow, 0, LVIF_IMAGE|LVIF_TEXT, pContentsInfo->strFilename, nIconIndex, 0, 0, NULL);
	m_lcFiles.SetItemText(nRow, 1, strFileSize);
	if(nOverlapIdx > 0)
	{
		m_lcFiles.SetItemState(nRow, LVIS_OVERLAYMASK, INDEXTOOVERLAYMASK(nOverlapIdx));
	}

	m_lcFiles.SetItemData(nRow, (DWORD_PTR)pContentsInfo);
}

// skpark 2013.2.25  전에 사용하던 폴더를 초기 폴더로 지정
static 	CString s_strSaveFolder="";  	
static int CALLBACK BrowseCallbackProc(
					HWND hwnd,UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	switch (uMsg)
	{
	case BFFM_INITIALIZED:
		{
			SendMessage(hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)lpData );
		}
		break;
	}

	return 0; // Always return 0.
}

void CFlashImagePlayerContentsDialog::OnBnClickedButtonBrowserAddFile()
{
	CString strChildPath;
	m_editContentsFolderName.GetWindowText(strChildPath);
	strChildPath += "/";

	CString strFilter;
	CString szFileExts = "*.jpg; *.jpeg; *.png";
	strFilter.Format("All Image Files (%s)|%s|"
					  "JPEG Files (*.jpg; *.jpeg)|*.jpg;*.jpeg;|"
					  "PNG Files (*.png)|*.png||"
					  , szFileExts, szFileExts);
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT | OFN_ENABLESIZING , strFilter, this);

	// skpark 2013.2.25 복수개의 파일 선택시, 이름의 합 만큼의 버퍼를 지정하지 않으면 대량 선택은 처리하지 못한다.
	// 버퍼가 충분히 크지 않으면 어쩔 것인가...황당한 인터페이스라 하지 않을 수 없다.
	// 하는 수 없이 10000개까지 들어올수 있도록 잡는다.
	char* buf_filename = new char[MAX_PATH*10000];
	::ZeroMemory(buf_filename, MAX_PATH*10000);
	dlg.m_ofn.lpstrFile = buf_filename;
	dlg.m_ofn.nMaxFile = MAX_PATH*10000;

	if(dlg.DoModal() != IDOK)
	{
		delete[]buf_filename;
		return;
	}

	CopyFlashImagePlayerFile(); // imagePlayer_{(GUID)}.swf 파일 복사

	POSITION pos = dlg.GetStartPosition();
	while(pos)
	{
		CString strPathName = dlg.GetNextPathName(pos);
		CString strToPath = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH + strChildPath;
		strToPath.Replace(_T("/"), _T("\\"));
		MakeSureDirectoryPathExists(strToPath);
		if(!ShellCopyFile(strPathName, strToPath, FALSE)) {
			delete[]buf_filename;
			return;
		}

		BOOL bExist = FALSE;
		CONTENTS_INFO* pContentsInfo = AddSubFileContents(strChildPath, strPathName, bExist);
		if(pContentsInfo)
		{
			int nRow = -1;
			if(bExist)
			{
				for(int i=0; i<m_lcFiles.GetItemCount() ;i++)
				{
					CString strTmp = m_lcFiles.GetItemText(i, 0);
					if(strTmp.MakeLower() == pContentsInfo->strFilename.MakeLower())
					{
						nRow = i;
						break;
					}
				}
			}

			if(nRow < 0)
			{
				nRow = m_lcFiles.GetItemCount();
				m_lcFiles.InsertItem(nRow, _T(""));
			}
			UpdateFilesListRow(nRow, pContentsInfo);
		}
	}
	//if(!CheckTotalContentsSize(dlg.GetPathName())) return;

	//RefreshFilesList(strChildPath);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;

	m_strDataXML = GetDataXML(""); // pictureViewer.xml 데이터 업데이트
	UpdateDataXML(false); // pictureViewer.xml 파일 업데이트
	LoadContents(m_strLocation);

	delete[]buf_filename;
}

void CFlashImagePlayerContentsDialog::OnBnClickedButtonBrowserDelFile()
{
	if(!m_btnBrowseDelFile.IsWindowEnabled()) return;

	if(UbcMessageBox(LoadStringById(IDS_FLASHCONTENTSDIALOG_MSG002), MB_YESNO) != IDYES) return;

	CopyFlashImagePlayerFile(); // imagePlayer_{(GUID)}.swf 파일 복사

	POSITION pos = m_lcFiles.GetFirstSelectedItemPosition();

	if(!pos)
	{
		UbcMessageBox(LoadStringById(IDS_FLASHCONTENTSDIALOG_MSG003));
		return;
	}

	CArray<int, int> anDelItems;

	while(pos)
	{
		int nItem = m_lcFiles.GetNextSelectedItem(pos);
		anDelItems.Add(nItem);
	}

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	if(anDelItems.GetCount() > 0)
	{
		m_bIsChangedFile = TRUE;
	}

	for(int i=anDelItems.GetCount()-1; i>=0 ;i--)
	{
		CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(anDelItems[i]);
		if(pContentsInfo)
		{
			CString strFileKey = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
			strFileKey.Replace(_T("\\"), _T("/"));
			strFileKey.MakeLower();
			m_mapSubFilesContents.RemoveKey(strFileKey);
			delete pContentsInfo;
			m_lcFiles.DeleteItem(anDelItems[i]);
		}
	}

	m_strDataXML = GetDataXML(""); // pictureViewer.xml 데이터 업데이트

	CString str_sub_folder_path;
	m_editContentsFolderName.GetWindowText(str_sub_folder_path);
	str_sub_folder_path += "/";
	RefreshFilesList(str_sub_folder_path);

	UpdateDataXML(false); // pictureViewer.xml 파일 업데이트
	LoadContents(m_strLocation);
}

CONTENTS_INFO* CFlashImagePlayerContentsDialog::AddSubFileContents(CString strChildPath, CString strFilepath, BOOL& bExist)
{
	bExist = FALSE;

	TCHAR drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(strFilepath, drive, path, filename, ext);

	CONTENTS_INFO* pContentsInfo = new CONTENTS_INFO;

	pContentsInfo->strId = (CUbcGUID::GetInstance()->ToGUID(_T("")));
	pContentsInfo->strContentsName = CPreventChar::GetInstance()->ConvertToAvailableChar(filename);
	pContentsInfo->nContentsType = CONTENTS_FILE;
	pContentsInfo->strParentId = m_strContentsId;
	pContentsInfo->strLocalLocation = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH + strChildPath;
	pContentsInfo->strFilename.Format(_T("%s%s"), filename, ext);
	pContentsInfo->bLocalFileExist = true;
	pContentsInfo->SetModified(); //skpark same_size_file_problem 2014.06.11
	TraceLog(("skpark %s file changed !!!" , pContentsInfo->strFilename));

	CFileStatus fs;
	//if(CFile::GetStatus(strFilepath, fs))
	if(CEnviroment::GetFileSize(strFilepath, fs.m_size))
		pContentsInfo->nFilesize = fs.m_size;

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(pContentsInfo->nFilesize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)(pContentsInfo->strLocalLocation + pContentsInfo->strFilename), szMd5);
	}
	pContentsInfo->strFileMD5 = szMd5;

	CString strFileKey = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
	strFileKey.Replace(_T("\\"), _T("/"));
	strFileKey.MakeLower();
	if(m_mapSubFilesContents[strFileKey] != NULL)
	{
		delete (CONTENTS_INFO*)m_mapSubFilesContents[strFileKey];
		bExist = TRUE;
	}

	m_mapSubFilesContents[strFileKey] = pContentsInfo;

	return pContentsInfo;
}

// 파일을 선택하여 다운로드
void CFlashImagePlayerContentsDialog::OnBnClickedButtonDownloadContents()
{
	if(DownloadSelectedContents())
	{
		CString str_sub_folder_path;
		m_editContentsFolderName.GetWindowText(str_sub_folder_path);
		str_sub_folder_path += "/";
		RefreshFilesList(str_sub_folder_path);
	}
}

// 파일을 선택하여 다운로드
BOOL CFlashImagePlayerContentsDialog::DownloadSelectedContents()
{
#ifdef _UBCSTUDIO_EE_
	CFtpMultiSite multiFtp(GetEnvPtr()->m_strHttpOnly.CompareNoCase("CLIENT") == 0);

	CString szPackage;
	szPackage.Format("%s%s%s.ini", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONFIG_PATH, GetEnvPtr()->m_PackageInfo.szPackage);
	multiFtp.AddSite(GetEnvPtr()->m_PackageInfo.szSiteID, szPackage);
	multiFtp.OnlyAddFile();

	if(m_lcFiles.GetSelectedCount() > 0)
	{
		POSITION pos = m_lcFiles.GetFirstSelectedItemPosition();
		while(pos)
		{
			int nItem = m_lcFiles.GetNextSelectedItem(pos);
			CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(nItem);
			if(pContentsInfo == NULL) continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				multiFtp.AddDownloadFile(pContentsInfo->strServerLocation + pContentsInfo->strFilename);
			}
		}
	}
	else
	{
		for(int nItem = 0; nItem < m_lcFiles.GetItemCount(); nItem++)
		{
			CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(nItem);
			if(pContentsInfo == NULL) continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				multiFtp.AddDownloadFile(pContentsInfo->strServerLocation + pContentsInfo->strFilename);
			}
		}
	}

	if(!multiFtp.RunFtp(CFtpMultiSite::eDownload)) return FALSE;

	if(m_lcFiles.GetSelectedCount() > 0)
	{
		POSITION pos = m_lcFiles.GetFirstSelectedItemPosition();
		while(pos)
		{
			int nItem = m_lcFiles.GetNextSelectedItem(pos);
			CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(nItem);
			if(pContentsInfo == NULL) continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				pContentsInfo->bLocalFileExist = true;
				pContentsInfo->bServerFileExist = true;
			}
		}
	}
	else
	{
		for(int nItem = 0; nItem < m_lcFiles.GetItemCount(); nItem++)
		{
			CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(nItem);
			if(pContentsInfo == NULL) continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				pContentsInfo->bLocalFileExist = true;
				pContentsInfo->bServerFileExist = true;
			}
		}
	}
#endif//_UBCSTUDIO_EE_
	return TRUE;
}

void CFlashImagePlayerContentsDialog::OnNMDblclkListFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcFiles.GetItemData(pNMLV->iItem);
	if(!pContentsInfo) return;

	if( GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
		!pContentsInfo->bLocalFileExist  &&
		 pContentsInfo->bServerFileExist )
	{
		CWaitMessageBox wait;

		if(!DownloadSelectedContents()) return;

		m_lcFiles.DeleteItem(nRow);
		m_lcFiles.InsertItem(nRow, _T(""));
		UpdateFilesListRow(nRow, pContentsInfo);
		m_lcFiles.SetItemState(nRow, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
	}

	CString strExecute = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
	strExecute.Replace(_T("/"), _T("\\"));

	::ShellExecute(GetSafeHwnd(), "open", strExecute, NULL, NULL, SW_SHOWNORMAL);
}

void CFlashImagePlayerContentsDialog::OnLvnKeydownListFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	*pResult = 0;

	if(pLVKeyDow->wVKey == VK_DELETE)
		OnBnClickedButtonBrowserDelFile();
}

BOOL CFlashImagePlayerContentsDialog::CopyFlashImagePlayerFile()
{
	 // imagePlayer_{(GUID)}.swf 파일 복사

	if( m_strLocation.GetLength()==0 || m_strSourceLocation.GetLength()==0 ) return FALSE;

	if( !PathFileExists(m_strSourceLocation) )
	{
		CString msg;
		msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG007), m_strLocation + " (1)");
		UbcMessageBox(msg, MB_ICONSTOP);
		return FALSE;
	}

	if( PathFileExists(m_strLocation) ) return TRUE;

	BOOL ret_val = ShellCopyFile(m_strSourceLocation, m_strLocation, FALSE);

	if( !ret_val )
	{
		CString msg;
		msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG007), m_strLocation + " (2)");
		UbcMessageBox(msg, MB_ICONSTOP);
	}

	return ret_val;
}

CString CFlashImagePlayerContentsDialog::GetDataXML(LPCTSTR lpszNewLineDelimiter)
{
	CString str_delimiter = (lpszNewLineDelimiter==NULL) ? "" : lpszNewLineDelimiter;

	//
	CString str_interval;
	m_editContentsPlayInterval.GetWindowText(str_interval);

	//
	CStringArray list_playfiles;
	int count = m_lcFiles.GetItemCount();
	for(int i=0; i<count; i++)
	{
		list_playfiles.Add(m_lcFiles.GetItemText(i,0));
	}

	//
	CString ret_val = "";
								ret_val += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	ret_val += str_delimiter;	ret_val += "<transition>";
	ret_val += str_delimiter;	ret_val += "	<!-- 10=10sec  -->";
	ret_val += str_delimiter;	ret_val += "	<interval> " + str_interval + " </interval>";
	ret_val += str_delimiter;	ret_val += "	<!-- mosaic, dissolve, perspective, iris, revolve, push, zoom, cube, wipe  -->";
	ret_val += str_delimiter;	ret_val += "	<effects>";
	//ret_val += str_delimiter;	ret_val += "		<effect> mosaic  </effect>";
	//ret_val += str_delimiter;	ret_val += "		<effect> dissolve </effect>";
	//ret_val += str_delimiter;	ret_val += "		<effect> perspective </effect>";
	//ret_val += str_delimiter;	ret_val += "		<effect> iris  </effect>";
	//ret_val += str_delimiter;	ret_val += "		<effect> revolve </effect>";
	//ret_val += str_delimiter;	ret_val += "		<effect> push  </effect>";
	//ret_val += str_delimiter;	ret_val += "		<effect> zoom  </effect>";
	//ret_val += str_delimiter;	ret_val += "		<effect> cube  </effect>";
	//ret_val += str_delimiter;	ret_val += "		<effect> wipe  </effect>";
	if( m_listEffectsOrder.GetCount() > 0 )
	{
	// 플래시 ImagePlayer.swf버그로 인해 맨뒤effect를 맨앞으로 이동
	ret_val += str_delimiter;	ret_val += "		<effect> " + m_listEffectsOrder.GetAt(m_listEffectsOrder.GetCount()-1) + " </effect>";
	}
	for(int i=0; i<m_listEffectsOrder.GetCount()-1; i++)
	{
	ret_val += str_delimiter;	ret_val += "		<effect> " + m_listEffectsOrder.GetAt(i) + " </effect>";
	}

	ret_val += str_delimiter;	ret_val += "	</effects>";
	ret_val += str_delimiter;	ret_val += "	<!-- random=무작위, order=순서대로, effect이름=한가지효과(ex. zoom) -->";
	ret_val += str_delimiter;	ret_val += "	<type> order </type>";
	ret_val += str_delimiter;	ret_val += "	<playList>";

	for(int i=0; i<list_playfiles.GetCount(); i++)
	{
	ret_val += str_delimiter;	ret_val += "		<file>" + list_playfiles.GetAt(i) + "</file>";
	}

	ret_val += str_delimiter;	ret_val += "	</playList>";
	ret_val += str_delimiter;	ret_val += "</transition>";

	return ret_val;
}

BOOL CFlashImagePlayerContentsDialog::UpdateDataXML(bool bReplaceXML)
{
	//
	if( bReplaceXML )
	{
		// delete data.xml from sub_contents_map
		m_bIsChangedFile = TRUE;

		POSITION pos = m_mapSubFilesContents.GetStartPosition();
		while(pos != NULL)
		{
			CString strFileKey;
			CONTENTS_INFO* pContentsInfo;
			m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

			if(!pContentsInfo) continue;
			if( stricmp(pContentsInfo->strFilename, "pictureViewer.xml") == 0 )
			{
				m_mapSubFilesContents.RemoveKey(strFileKey);
				delete pContentsInfo;
			}
		}
	}

	//
	CString strChildPath;
	m_editContentsFolderName.GetWindowText(strChildPath);
	strChildPath += "/";

	CString strPathName = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH + strChildPath;
	strPathName.Replace(_T("/"), _T("\\"));
	strPathName += "pictureViewer.xml";

	CFile file_xml;
	if( !file_xml.Open(strPathName, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
	{

		CString str_sub_path = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH + strChildPath;
		str_sub_path.Replace(_T("/"), _T("\\"));

		if( _access(str_sub_path, 00) == 0 )
		{
			// exist sub_folder -> file write error -> show error-message
			CString msg;
			msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG007), strPathName);
			UbcMessageBox(msg, MB_ICONSTOP);
		}

		return FALSE;
	}

	// pictureViewer.xml file write
	CString str_data_xml = GetDataXML("\r\n");
	char* buf_utf8 = new char[str_data_xml.GetLength()*3];
	scratchUtil::getInstance()->AnsiToUTF8((LPSTR)(LPCSTR)str_data_xml, buf_utf8, str_data_xml.GetLength()*3);
	BYTE header[3] = { 0xEF, 0xBB, 0xBF }; // utf8 BOM
	file_xml.Write(header, 3);
	file_xml.Write(buf_utf8, strlen(buf_utf8));

	// write dummy
	static bool randomize = false;
	if( randomize == false )
	{
		randomize = true;
		srand( (unsigned)time( NULL ) ); // only run 1-times
	}
	int dummy_size = rand();
	char* dummy_buf = new char[dummy_size];
	memset(dummy_buf, 0x20, dummy_size);
	file_xml.Write(dummy_buf, dummy_size);

	file_xml.Close();
	delete[]buf_utf8;
	delete[]dummy_buf;

	//
	if( bReplaceXML )
	{
		BOOL bExist;
		AddSubFileContents(strChildPath, strPathName, bExist);
	}

	return TRUE;
}

void CFlashImagePlayerContentsDialog::ReorderFileListFromDataXML(CString strXml)
{
	strXml.Trim(" \t");
	if( strXml.GetLength() == 0 ) return;

	int start_pos = 0;
	int end_pos = 0;

	//
	CStringArray list_file;
	while(1)
	{
		start_pos = strXml.Find("<file>", start_pos);
		if(start_pos < 0) break;
		end_pos = strXml.Find("</file>", start_pos);
		if(end_pos < 0) break;
		start_pos += 6;

		CString str_filename = strXml.Mid(start_pos, end_pos-start_pos);
		str_filename.Trim(" \t");
		list_file.Add(str_filename);
	}

	//
	CPtrArray list_contents;
	int count = m_lcFiles.GetItemCount();
	for(int i=0; i<count; i++)
	{
		CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcFiles.GetItemData(i);
		list_contents.Add(info);
	}
	m_lcFiles.DeleteAllItems();

	//
	int idx = 0;
	count = list_file.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str_filename = list_file.GetAt(i);

		for(int j=0; j<list_contents.GetCount(); j++)
		{
			CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)list_contents.GetAt(j);
			if( stricmp(str_filename, pContentsInfo->strFilename) == 0 )
			{
				m_lcFiles.InsertItem(idx, "");
				UpdateFilesListRow(idx, pContentsInfo);
				list_contents.RemoveAt(j);
				idx++;
				break;
			}
		}
	}
	//
	for(int j=0; j<list_contents.GetCount(); j++)
	{
		CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)list_contents.GetAt(j);
		m_lcFiles.InsertItem(idx, "");
		UpdateFilesListRow(idx, pContentsInfo);
	}
}

void CFlashImagePlayerContentsDialog::OnBnClickedButtonMoveUp()
{
	int count = m_lcFiles.GetItemCount();

	bool bChange = false;
	for(int i=1; i<count; i++)
	{
		UINT prev_state = m_lcFiles.GetItemState(i-1, LVIS_SELECTED);
		UINT cur_state = m_lcFiles.GetItemState(i, LVIS_SELECTED);

		if( cur_state == LVIS_SELECTED && prev_state != LVIS_SELECTED )
		{
			CONTENTS_INFO* prev_info = (CONTENTS_INFO*)m_lcFiles.GetItemData(i-1);
			CONTENTS_INFO* cur_info = (CONTENTS_INFO*)m_lcFiles.GetItemData(i);

			UpdateFilesListRow(i-1, cur_info);
			UpdateFilesListRow(i, prev_info);

			m_lcFiles.SetItemState(i-1, LVIS_SELECTED, LVIS_SELECTED);
			m_lcFiles.SetItemState(i, NULL, LVIS_SELECTED);

			bChange = true;
		}
	}

	if( bChange )
	{
		UpdateDataXML(false); // pictureViewer.xml 파일 업데이트
		LoadContents(m_strLocation);
	}
}

void CFlashImagePlayerContentsDialog::OnBnClickedButtonMoveDown()
{
	int count = m_lcFiles.GetItemCount();

	bool bChange = false;
	for(int i=count-2; i>=0; i--)
	{
		UINT cur_state = m_lcFiles.GetItemState(i, LVIS_SELECTED);
		UINT next_state = m_lcFiles.GetItemState(i+1, LVIS_SELECTED);

		if( cur_state == LVIS_SELECTED && next_state != LVIS_SELECTED )
		{
			CONTENTS_INFO* cur_info = (CONTENTS_INFO*)m_lcFiles.GetItemData(i);
			CONTENTS_INFO* next_info = (CONTENTS_INFO*)m_lcFiles.GetItemData(i+1);

			UpdateFilesListRow(i, next_info);
			UpdateFilesListRow(i+1, cur_info);

			m_lcFiles.SetItemState(i, NULL, LVIS_SELECTED);
			m_lcFiles.SetItemState(i+1, LVIS_SELECTED, LVIS_SELECTED);

			bChange = true;
		}
	}

	if( bChange )
	{
		UpdateDataXML(false); // pictureViewer.xml 파일 업데이트
		LoadContents(m_strLocation);
	}
}

void CFlashImagePlayerContentsDialog::OnBnClickedButtonChangeEffectsOrder()
{
	if( m_pDlg == NULL )
	{
		m_pDlg = new CFlashImageplayerEffectsOderChangeDlg(this);
		m_pDlg->SetSelectedEffectsOrder(m_listEffectsOrder);
		m_pDlg->Create(IDD_FLASH_IMAGEPLAYER_EFFECTS_ORDER_CHANGE, this);
		m_pDlg->ShowWindow(SW_HIDE);

		CRect rect;
		m_pDlg->GetClientRect(rect);

		m_sizeDlg = rect.Size();
	}

	CRect rect;
	m_editEffectsOrder.GetWindowRect(rect);

	m_pDlg->MoveWindow(rect.left, rect.top+rect.Height(), m_sizeDlg.cx, m_sizeDlg.cy);
	m_pDlg->ShowWindow(SW_SHOW);
}

LRESULT CFlashImagePlayerContentsDialog::OnUpdateEffectsOrder(WPARAM wParam, LPARAM lParam)
{
	if( m_pDlg )
	{
		m_editEffectsOrder.SetWindowText(m_pDlg->GetSelectedEffectsOrderString());
		m_listEffectsOrder.Copy(m_pDlg->GetSelectedEffectsOrder());

		m_strDataXML = GetDataXML("");

		UpdateDataXML(false); // pictureViewer.xml 파일 업데이트
		LoadContents(m_strLocation);
	}

	if( m_pDlg )
	{
		m_pDlg->DestroyWindow();
		delete m_pDlg;
		m_pDlg = NULL;
	}

	return 0;
}
