#pragma once

#include "common\libscratch\scratchUtil.h"

class CScanNetHost
{
protected:
	// Data 및 배열 관련
	CPtrArray	m_ItemArray;
	LONG		m_nItemIndex;

	// Thread 관련
	LONG		m_nRunThreadCnt;

	static UINT	ScanHostThread(LPVOID param);

	CRITICAL_SECTION m_csLock;

	// ScanThread에서 사용
	void	MoveFirst();
	IPInfo*	GetNextItem();

	int  InitItemArray();
	bool DeleteAllItem();

public:
	CScanNetHost(void);
	~CScanNetHost(void);

	// Processing
	bool AutoSearch(int nThreadCnt=128);
};
