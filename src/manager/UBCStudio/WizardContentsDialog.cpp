// WizardContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "WizardContentsDialog.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "WizardSubContents.h"
#include "TextInputDlg.h"
#include "ImageSelectDlg.h"
#include "VideoSelectDlg.h"
#include "common/ubcdefine.h"
#include "common/MD5Util.h"

#define DEFAULT_XML			"<?xml version=\"1.0\" encoding=\"EUC-KR\" standalone=\"yes\" ?> " \
							"<FlashWizard name=\"\" contents_count=\"0\">" \
								"<context index=\"0\" type=\"\" id=\"\"><value /></context>" \
							"</FlashWizard>"

// CWizardContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CWizardContentsDialog, CSubContentsDialog)

CWizardContentsDialog::CWizardContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CWizardContentsDialog::IDD, pParent)
	, m_wndWizard (NULL, 0,false)
	, m_bPreviewMode(false)
	, m_reposControl (this)
	, m_bPermanent(FALSE)
	, m_strLocation("")
{
	m_strWizardPath.Format("%s\\%sFlashWizard\\", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
}

CWizardContentsDialog::~CWizardContentsDialog()
{

}

BOOL CWizardContentsDialog::DestroyWindow()
{
	for(int i=0 ;i<m_tabSubContents.GetItemCount(); i++)
	{
		CWnd* pWnd = (CWnd*)m_tabSubContents.GetItemParam(i);
		if(pWnd) delete pWnd;
	}
	m_tabSubContents.DeleteAllItems();

	return CSubContentsDialog::DestroyWindow();
}

void CWizardContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_groupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_PLAY, m_btnPreviewPlay);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);
	DDX_Control(pDX, IDC_EDIT_FELICA_URL, m_editFelicaUrl);
	DDX_Control(pDX, IDC_CB_WIZARD_TYPE, m_cbWizardType);
	DDX_Control(pDX, IDC_TAB_SUB_CONTENTS, m_tabSubContents);
	DDX_Control(pDX, IDC_BUTTON_DELFILE, m_btnDelFile);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_CONTENTS_FILE, m_btnBrowserContentsFile);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_WAIT_MINUTE, m_editContentsWaitMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_WAIT_SECOND, m_editContentsWaitSecond);
	DDX_Check(pDX, IDC_KB_WAIT_PERMANENT_CHECK, m_bWaitPermanent);
}


BEGIN_MESSAGE_MAP(CWizardContentsDialog, CSubContentsDialog)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_PLAY, OnBnClickedButtonPreviewPlay)
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, OnBnClickedPermanentCheck)
	ON_CBN_SELCHANGE(IDC_CB_WIZARD_TYPE, &CWizardContentsDialog::OnCbnSelchangeCbWizardType)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_SUB_CONTENTS, &CWizardContentsDialog::OnTcnSelchangeTabSubContents)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_CONTENTS_FILE, &CWizardContentsDialog::OnBnClickedButtonBrowserContentsFile)
	ON_BN_CLICKED(IDC_BUTTON_DELFILE, &CWizardContentsDialog::OnBnClickedButtonDelfile)
	ON_BN_CLICKED(IDC_KB_WAIT_PERMANENT_CHECK, &CWizardContentsDialog::OnBnClickedKbWaitPermanentCheck)
END_MESSAGE_MAP()


// CWizardContentsDialog 메시지 처리기입니다.

// CWizardContentsDialog message handlers
BOOL CWizardContentsDialog::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	m_editContentsName.SetValue("");
	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(15);

	m_btnPreviewPlay.LoadBitmap(IDB_BTN_PLAY, RGB(255, 255, 255));
	m_btnPreviewPlay.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT001));

	m_btnBrowserContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btnBrowserContentsFile.SetToolTipText(LoadStringById(IDS_HORZTICKERCONTENTSDIALOG_BUT001));

	m_btnDelFile.LoadBitmap(IDB_BTN_DELFILE, RGB(236, 233, 216));
	m_btnDelFile.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT003));

	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);

	m_wndWizard.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);

	m_reposControl.AddControl(&m_groupPreview, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_staticContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_wndWizard, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.Move();

	m_tabSubContents.InitImageListHighColor(IDB_TAB_CONTENTS_LIST, RGB(192,192,192));

	InitWizardType();

	m_editContentsWaitMinute.SetValue(0);
	m_editContentsWaitSecond.SetValue(15);

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CWizardContentsDialog::Stop()
{
	m_wndWizard.Stop(false);
}

bool CWizardContentsDialog::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData();

	CString strWizardFile;
	m_cbWizardType.GetWindowText(strWizardFile);

	info.nContentsType = GetContentsType();
	info.strContentsName = m_editContentsName.GetValueString();
	info.strLocalLocation = m_strLocation;
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();
//	info.strLocation = m_strWizardPath + strWizardFile + ".swf";
	info.strComment[0] = strWizardFile + ".swf";
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	//
	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(info.strLocalLocation, dir, path, filename, ext);

	info.strLocalLocation.Format("%s%s", dir, path); 
	info.strFilename.Format("%s%s", filename, ext); 

	if(!IsFitContentsName(info.strContentsName)) return false;
	if(m_cbWizardType.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG008), MB_ICONSTOP);
		return false;
	}
//	if( info.strLocation.GetLength() == 0 )
//	{
//		UbcMessageBox(CONTENTSDIALOG_MSG002, MB_ICONSTOP);
//		return false;
//	}
//	if( info.strFilename.GetLength() == 0 )
//	{
//		UbcMessageBox(CONTENTSDIALOG_MSG003, MB_ICONSTOP);
//		return false;
//	}
//	if(info.nRunningTime == 0){
//		UbcMessageBox(CONTENTSDIALOG_MSG004, MB_ICONSTOP);
//		return false;
//	}

	m_editFelicaUrl.GetWindowText(info.strComment[1]);
	info.strComment[1].Trim();
	if(!info.strComment[1].IsEmpty())
	{
		CString strFelicaUrl = info.strComment[1];
		strFelicaUrl.MakeLower();
		if(strFelicaUrl.Find(_T("http://")) < 0)
		{
			UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG007), MB_ICONSTOP);
			return false;
		}
	}

	if(m_xmlCurData.SetXPath("/FlashWizard") != XMLPARSER_SUCCESS) return false;
	if(m_xmlCurData.SetAttribute("BackgroundImage", info.strFilename) != XMLPARSER_SUCCESS)
	{
		m_xmlCurData.NewAttribute("BackgroundImage");
		m_xmlCurData.SetAttribute("BackgroundImage", info.strFilename);
	}

	// 0000910: 마법사콘텐츠에서 상위메뉴로 돌아가는 시간을 설정할 수 있도록 한다.
	int nDueTime = m_editContentsWaitMinute.GetValueInt()*60 + m_editContentsWaitSecond.GetValueInt();

	if(m_xmlCurData.SetAttribute("DueTime", ::ToString(nDueTime)) != XMLPARSER_SUCCESS)
	{
		m_xmlCurData.NewAttribute("DueTime");
		m_xmlCurData.SetAttribute("DueTime", ::ToString(nDueTime));
	}

	info.strWizardFiles = "";
	for(int i=0 ;i<m_tabSubContents.GetItemCount(); i++)
	{
		CWizardSubContents* pWnd = (CWizardSubContents*)m_tabSubContents.GetItemParam(i);
		if(!pWnd) continue;

		CString strContentsId;
		CString strValue;

		pWnd->GetData(strContentsId, strValue);

		if(!strContentsId.IsEmpty())
		{
			info.strWizardFiles += strContentsId + ",";
		}

		CString strXPath;
		strXPath.Format("/FlashWizard/context [@index=\"%d\"]/value", i);
		if(m_xmlCurData.SetXPath(strXPath) != XMLPARSER_SUCCESS) return false;
		int nCnt = m_xmlCurData.GetSelCount();

		strXPath.Format("/FlashWizard/context [@index=\"%d\"]", i);
		if(m_xmlCurData.SetXPath(strXPath) != XMLPARSER_SUCCESS) return false;

		for(int j=0; j<nCnt ;j++)
		{
			m_xmlCurData.DeleteElement("value");
		}

		m_xmlCurData.SetAttribute("id", strContentsId);

		if(strValue.GetLength() <= 0)
		{
			m_xmlCurData.NewElement("value");
		}
		else
		{
			int nPos = 0;
			CString strTok = strValue.Tokenize("\n", nPos);
			while(1)
			{
				strTok.TrimRight("\r\n");
				CString strLineValue = strTok;
				m_xmlCurData.NewElement("value", 0, strLineValue);
				if(nPos == (strValue.GetLength()+1)) break;
				strTok = strValue.Tokenize("\n", nPos);
			}
		}
	}

	info.strWizardFiles.TrimRight(",");

	// xml 저장
	CString strXml = m_xmlCurData.GetXML("/");
	strXml.Replace("\t","");
	strXml.Replace("\r","");
	strXml.Replace("\n","");
	strXml.Replace("xml:space=\"preserve\"","");
	strXml.Replace("space=\"preserve\"","");
	info.strWizardXML = strXml;

	return true;
}

bool CWizardContentsDialog::SetContentsInfo(CONTENTS_INFO& info)
{
//	m_strContentsId = info.strId;
	m_editContentsName.SetValue(info.strContentsName);
	m_editContentsFileName.SetWindowText(info.strFilename);
	m_editFelicaUrl.SetWindowText(info.strComment[1]);

	if(info.strFilename.IsEmpty()){
		m_strLocation = _T("");
		info.nFilesize = 0;
	}else{
		m_strLocation = info.strLocalLocation + info.strFilename;
	}

	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}

	if(!info.strWizardXML.IsEmpty())
	{
		InitXmlDataFromString(info.strWizardXML);
	}

	CString strWizardType = info.strComment[0];
	if(strWizardType.ReverseFind('.') >= 0)
	{
		strWizardType = strWizardType.Left(strWizardType.ReverseFind('.'));
	}
	if(m_xmlCurData.SetXPath("/FlashWizard") == XMLPARSER_SUCCESS)
	{
		strWizardType = m_xmlCurData.GetAttribute("name");
	}

	int nIndex = -1;
	for(int i=0; i<m_cbWizardType.GetCount() ;i++)
	{
		CString strTmp;
		m_cbWizardType.GetLBText(i, strTmp);
		if(strWizardType == strTmp)
		{
			nIndex = i;
			break;
		}
	}

	m_cbWizardType.SetCurSel(nIndex);

	InitSubContentsTab();

	// 0000910: 마법사콘텐츠에서 상위메뉴로 돌아가는 시간을 설정할 수 있도록 한다.
	if(m_xmlCurData.SetXPath("/FlashWizard") == XMLPARSER_SUCCESS)
	{
		int nDueTime = _ttoi(m_xmlCurData.GetAttribute("DueTime"));
		if(nDueTime >= 1440)
		{
			m_editContentsWaitMinute.SetWindowText("1440");
			m_editContentsWaitSecond.SetWindowText("00");
			m_bWaitPermanent = TRUE;
			CButton* pBtn = (CButton*)GetDlgItem(IDC_KB_WAIT_PERMANENT_CHECK);
			pBtn->SetCheck(TRUE);
			m_editContentsWaitMinute.EnableWindow(FALSE);
			m_editContentsWaitSecond.EnableWindow(FALSE);
		}
		else
		{
			m_editContentsWaitMinute.SetWindowText(::ToString(nDueTime / 60));
			m_editContentsWaitSecond.SetWindowText(::ToString(nDueTime % 60));
			m_bWaitPermanent = FALSE;
			CButton* pBtn = (CButton*)GetDlgItem(IDC_KB_WAIT_PERMANENT_CHECK);
			pBtn->SetCheck(FALSE);
		}
	}

	UpdateData(FALSE);

	OnBnClickedButtonPreviewPlay();

	return true;
}

void CWizardContentsDialog::SetPlayContents()
{
	m_wndWizard.CloseFile();

	m_wndWizard.m_contentsId = m_strContentsId;
	m_wndWizard.m_contentsName = m_editContentsName.GetValueString();
	CString strWizardFile;
	m_cbWizardType.GetWindowText(strWizardFile);
	m_wndWizard.m_comment[0] = strWizardFile + ".swf";
	m_wndWizard.m_strMediaFullPath = m_strWizardPath + strWizardFile + ".swf";

	//
	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(m_strLocation, dir, path, filename, ext);

	CString strFilename;
	strFilename.Format("%s%s", filename, ext); 

	if(m_xmlCurData.SetXPath("/FlashWizard") != XMLPARSER_SUCCESS) return;
	if(m_xmlCurData.SetAttribute("BackgroundImage", strFilename) != XMLPARSER_SUCCESS)
	{
		m_xmlCurData.NewAttribute("BackgroundImage");
		m_xmlCurData.SetAttribute("BackgroundImage", strFilename);
	}

	// 0000910: 마법사콘텐츠에서 상위메뉴로 돌아가는 시간을 설정할 수 있도록 한다.
	int nDueTime = m_editContentsWaitMinute.GetValueInt()*60 + m_editContentsWaitSecond.GetValueInt();

	if(m_xmlCurData.SetAttribute("DueTime", ::ToString(nDueTime)) != XMLPARSER_SUCCESS)
	{
		m_xmlCurData.NewAttribute("DueTime");
		m_xmlCurData.SetAttribute("DueTime", ::ToString(nDueTime));
	}

	for(int i=0 ;i<m_tabSubContents.GetItemCount(); i++)
	{
		CWizardSubContents* pWnd = (CWizardSubContents*)m_tabSubContents.GetItemParam(i);
		if(!pWnd) continue;

		CString strContentsId;
		CString strValue;

		pWnd->GetData(strContentsId, strValue);

		CString strXPath;
		strXPath.Format("/FlashWizard/context [@index=\"%d\"]/value", i);
		if(m_xmlCurData.SetXPath(strXPath) != XMLPARSER_SUCCESS) return;
		int nCnt = m_xmlCurData.GetSelCount();

		strXPath.Format("/FlashWizard/context [@index=\"%d\"]", i);
		if(m_xmlCurData.SetXPath(strXPath) != XMLPARSER_SUCCESS) return;

		for(int j=0; j<nCnt ;j++)
		{
			m_xmlCurData.DeleteElement("value");
		}

		m_xmlCurData.SetAttribute("id", strContentsId);

		if(strValue.GetLength() <= 0)
		{
			m_xmlCurData.NewElement("value");
		}
		else
		{
			int nPos = 0;
			CString strTok = strValue.Tokenize("\n", nPos);
			while(1)
			{
				strTok.TrimRight("\r\n");
				CString strLineValue = strTok;
				m_xmlCurData.NewElement("value", 0, strLineValue);
				if(nPos == (strValue.GetLength()+1)) break;
				strTok = strValue.Tokenize("\n", nPos);
			}
		}
	}

	CString strXml = m_xmlCurData.GetXML("/");
	strXml.Replace("\t","");
	strXml.Replace("\r","");
	strXml.Replace("\n","");
	strXml.Replace("xml:space=\"preserve\"","");
	strXml.Replace("space=\"preserve\"","");
	m_wndWizard.m_wizardXML = strXml;

	m_wndWizard.m_runningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();
	m_wndWizard.OpenFile(0);
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨트롤의 사용가능여부 설정 \n
/// @param (BOOL) bEnable : (in) TRUE : 사용가능, FALSE : 사용 불가
/////////////////////////////////////////////////////////////////////////////////
void CWizardContentsDialog::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editContentsFileName.EnableWindow(bEnable);
	m_cbWizardType.EnableWindow(bEnable);
	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	GetDlgItem(IDC_PERMANENT_CHECK)->EnableWindow(FALSE);
	m_editFelicaUrl.EnableWindow(bEnable);
	m_btnBrowserContentsFile.EnableWindow(bEnable);
	m_btnDelFile.EnableWindow(bEnable);
	m_editContentsWaitMinute.EnableWindow(bEnable);
	m_editContentsWaitSecond.EnableWindow(bEnable);
	GetDlgItem(IDC_KB_WAIT_PERMANENT_CHECK)->EnableWindow(FALSE);

	for(int i=0 ;i<m_tabSubContents.GetItemCount(); i++)
	{
		CWizardSubContents* pWnd = (CWizardSubContents*)m_tabSubContents.GetItemParam(i);
		if(!pWnd) continue;
		pWnd->SetPreviewMode(m_bPreviewMode);
	}
}

void CWizardContentsDialog::OnBnClickedButtonPreviewPlay()
{
	UpdateData(TRUE);

	if(m_cbWizardType.GetCurSel() >= 0)
	{
		SetPlayContents();
		m_wndWizard.Play();
	}

	UpdateData(FALSE);
}

void CWizardContentsDialog::OnBnClickedPermanentCheck()
{
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent){
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}else{
		m_editContentsPlayMinute.SetWindowText("0");
		m_editContentsPlaySecond.SetWindowText("15");

		m_editContentsPlayMinute.EnableWindow(TRUE);
		m_editContentsPlaySecond.EnableWindow(TRUE);
	}
}

BOOL CWizardContentsDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CWizardContentsDialog::OnOK() {}
void CWizardContentsDialog::OnCancel()
{
	GetParent()->PostMessage(WM_CLOSE);
}

void CWizardContentsDialog::InitXmlDataFromFile(CString strFile)
{
	CFileStatus fs;
	if(CFile::GetStatus(strFile, fs))
	{
		if(m_xmlCurData.Load(strFile) != XMLPARSER_SUCCESS)
		{
			m_xmlCurData.LoadXML(DEFAULT_XML);
		}
	}
	else
	{
		m_xmlCurData.LoadXML(DEFAULT_XML);
	}
}

void CWizardContentsDialog::InitXmlDataFromString(CString strXml)
{
	if(strXml.IsEmpty()) strXml = DEFAULT_XML;

	if(m_xmlCurData.LoadXML(strXml) != XMLPARSER_SUCCESS)
	{
		m_xmlCurData.LoadXML(DEFAULT_XML);
	}
}

void CWizardContentsDialog::InitWizardType()
{
	m_cbWizardType.ResetContent();

	CFileFind ff;
	BOOL bWorking = ff.FindFile(m_strWizardPath + "*.swf");
	while(bWorking)
	{
		bWorking = ff.FindNextFile();

		if(ff.IsDots()) continue;
		if(ff.IsDirectory()) continue;

		m_cbWizardType.AddString(ff.GetFileTitle());
	}
}

void CWizardContentsDialog::OnCbnSelchangeCbWizardType()
{
	CString strSrcWizardFile;
	CString strSrcWizardXmlFile;
	if(m_cbWizardType.GetCurSel() >= 0)
	{
		m_cbWizardType.GetWindowText(strSrcWizardFile);
		strSrcWizardXmlFile = m_strWizardPath + strSrcWizardFile + ".xml";
	}

	InitXmlDataFromFile(strSrcWizardXmlFile);
	InitSubContentsTab();

	OnBnClickedButtonPreviewPlay();
}

void CWizardContentsDialog::InitSubContentsTab()
{
	for(int i=0 ;i<m_tabSubContents.GetItemCount(); i++)
	{
		CWnd* pWnd = (CWnd*)m_tabSubContents.GetItemParam(i);
		if(pWnd) delete pWnd;
	}
	m_tabSubContents.DeleteAllItems();

	if(m_xmlCurData.SetXPath("/FlashWizard") != XMLPARSER_SUCCESS) return;

	int nCnt = atoi(m_xmlCurData.GetAttribute("contents_count"));

	for(int i=0; i<nCnt ;i++)
	{
		CString strXPath;
		strXPath.Format("/FlashWizard/context [@index=\"%d\"]", i);
		if(m_xmlCurData.SetXPath(strXPath) != XMLPARSER_SUCCESS) return;

		CString strType  = m_xmlCurData.GetAttribute("type");
		CString strId    = m_xmlCurData.GetAttribute("id");

		strXPath.Format("/FlashWizard/context [@index=\"%d\"]/value", i);
		if(m_xmlCurData.SetXPath(strXPath) != XMLPARSER_SUCCESS) return;

		int nCnt = m_xmlCurData.GetSelCount();
		CString strValue;
		for(int j=0; j<nCnt ;j++)
		{
			strValue += (j>0 ? "\r\n" : "") + m_xmlCurData.GetData(j);
		}

		strType.MakeLower();

		CWizardSubContents* pDlg = NULL;
		int nIndex = m_tabSubContents.GetItemCount();
		if(strType == "text")
		{
			m_tabSubContents.InsertItem(nIndex, ::ToString(nIndex+1) + ".Text", EICO_TEXT);
			pDlg = new CTextInputDlg(this);
			pDlg->SetDocument(GetDocument());
			pDlg->Create(IDD_TEXT_INPUT_DLG, this);
		}
		else if(strType == "image")
		{
			m_tabSubContents.InsertItem(nIndex, ::ToString(nIndex+1) + ".Image", EICO_IMAGE);
			pDlg = new CImageSelectDlg(this);
			pDlg->SetDocument(GetDocument());
			pDlg->Create(IDD_IMAGE_SELECT_DLG, this);
		}
		else if(strType == "video")
		{
			m_tabSubContents.InsertItem(nIndex, ::ToString(nIndex+1) + ".Video", EICO_VIDEO);
			pDlg = new CVideoSelectDlg(this);
			pDlg->SetDocument(GetDocument());
			pDlg->Create(IDD_VIDEO_SELECT_DLG, this);
		}
		else
		{
			continue;
		}

		pDlg->SetPreviewMode(m_bPreviewMode);
		pDlg->SetData(strId, strValue);

		//CRect rcTab, rcTabItem;
		//m_tabSubContents.GetWindowRect(rcTab);
		//ScreenToClient(rcTab);
		//m_tabSubContents.GetItemRect(0, rcTabItem);

		//rcTab.DeflateRect(3,rcTabItem.Height()+5,4,4);
		//pDlg->SetParent(this);
		//pDlg->SetWindowPos(NULL, rcTab.left, rcTab.top, rcTab.Width(), rcTab.Height(), SWP_HIDEWINDOW);

		m_tabSubContents.SetItemParam(nIndex, (LPARAM)pDlg);
	}

	if(m_tabSubContents.GetItemCount() > 0)
	{
		CRect rcTab, rcTabItem;
		m_tabSubContents.GetWindowRect(rcTab);
		ScreenToClient(rcTab);
		m_tabSubContents.GetItemRect(0, rcTabItem);

		rcTab.DeflateRect(3, m_tabSubContents.GetRowCount() * rcTabItem.Height() + 5, 4, 4);

		for(int i=0 ;i<m_tabSubContents.GetItemCount(); i++)
		{
			CWizardSubContents* pWnd = (CWizardSubContents*)m_tabSubContents.GetItemParam(i);
			if(!pWnd) continue;

			pWnd->SetParent(this);
			pWnd->SetWindowPos(NULL, rcTab.left, rcTab.top, rcTab.Width(), rcTab.Height(), SWP_HIDEWINDOW);
		}
	}

	ToggleTab(0);
}

void CWizardContentsDialog::OnTcnSelchangeTabSubContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	ToggleTab(m_tabSubContents.GetCurSel());
	*pResult = 0;
}

void CWizardContentsDialog::ToggleTab(int nIndex)
{
	m_tabSubContents.SetCurSel(nIndex);

	for(int i=0 ;i<m_tabSubContents.GetItemCount(); i++)
	{
		CWizardSubContents* pWnd = (CWizardSubContents*)m_tabSubContents.GetItemParam(i);
		if(!pWnd) continue;

//		pWnd->EnableWindow(nIndex == i);
		pWnd->ShowWindow  (nIndex == i);
	}
}

void CWizardContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CSubContentsDialog::OnSize(nType, cx, cy);

	m_reposControl.Move();

	if( m_wndWizard.GetSafeHwnd()           &&
		m_wndWizard.m_pFlash != NULL        &&
		m_wndWizard.m_pFlash->GetSafeHwnd() )
	{
		CRect rect;
		m_wndWizard.GetClientRect(rect);
		m_wndWizard.m_pFlash->MoveWindow(rect);
	}
}

void CWizardContentsDialog::OnBnClickedButtonBrowserContentsFile()
{
	CString szFileExts;
	CString filter;

	// 창일향 : 콘텐츠는 오직 JPG 파일 뿐이다
	if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
	{
		szFileExts = "*.jpg; *.jpeg;";
		filter.Format("JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;||"
					 , szFileExts
					 );
	}
	else
	{
		szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
		filter.Format("All Image Files|%s|"
					  "Bitmap Files (*.bmp)|*.bmp|"
					  "JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;|"
					  "GIF Files (*.gif)|*.gif|"
					  "PCX Files (*.pcx)|*.pcx|"
					  "PNG Files (*.png)|*.png|"
					  "TIFF Files (*.tif)|*.tif;*.tiff||"
					 , szFileExts
					 );
	}

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, CONTENTS_IMAGE))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	//m_editContentsFileName.SetWindowText(dlg.GetPathName());
	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = dlg.GetPathName();
	m_editContentsFileName.SetSel(0, -1);

	CFileStatus fs;
//		if(CFile::GetStatus(dlg.GetPathName(), fs))
	if(CEnviroment::GetFileSize(dlg.GetPathName(), fs.m_size))
	{
		m_ulFileSize = fs.m_size;
	}

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);
	if(contents_name.GetLength() == 0)
	{
		char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
		_splitpath(dlg.GetPathName(), drive, path, filename, ext);

		m_editContentsName.SetWindowText(filename);
	}

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(m_ulFileSize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
	}
	m_strFileMD5 = CString(szMd5);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CWizardContentsDialog::OnBnClickedButtonDelfile()
{
	m_ulFileSize = 0;
	m_editContentsFileName.SetWindowText(_T(""));
//	m_staticContentsFileSize.SetWindowText(_T(""));
	m_strLocation = _T("");
	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	m_strFileMD5 = _T("");
}

void CWizardContentsDialog::OnBnClickedKbWaitPermanentCheck()
{
	m_bWaitPermanent = (m_bWaitPermanent == TRUE ? FALSE : TRUE);

	if(m_bWaitPermanent)
	{
		m_editContentsWaitMinute.SetWindowText("1440");
		m_editContentsWaitSecond.SetWindowText("00");

		m_editContentsWaitMinute.EnableWindow(FALSE);
		m_editContentsWaitSecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsWaitMinute.SetWindowText("0");
		m_editContentsWaitSecond.SetWindowText("15");

		m_editContentsWaitMinute.EnableWindow(TRUE);
		m_editContentsWaitSecond.EnableWindow(TRUE);
	}
}
