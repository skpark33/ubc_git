#include "stdafx.h"
#include <afxmt.h>
#include "resource.h"
#include "DataContainer.h"
#include "Schedule.h"
#include "Enviroment.h"
#include "common/PreventChar.h"
#include "common/libXmlParser.h"
#include "common/UbcGUID.h"

// Modified by 정운형 2009-01-07 오후 5:59
// 변경내역 :  CLI 연동 수정
//#include "resource.h"
//#include "MainFrm.h"
// Modified by 정운형 2009-01-07 오후 5:59
// 변경내역 :  CLI 연동 수정


// Ini에서 읽어 올때 값의 앞부분에 공백이 있는 경우 공백이 제거된 상태로 값이 구해지기 때문에
// 앞부분이 공백이 있는 문자열은 문자열앞에 | 문자를 추가하여 로드하고 저장하도록 한다.
CString ForSaveComment(CString strRawValue)
{
	if(strRawValue.GetLength() > 0 && strRawValue[0] == ' ') strRawValue = "|"+strRawValue;
	return strRawValue;
}

CString ForLoadComment(CString strRawValue)
{
	if( strRawValue.GetLength() > 1 && strRawValue.Left(2) == "| ") strRawValue.Delete(0);
	return strRawValue;
}

////////////////////////////////////////////////////////////////////////////////
// CONTENTS_INFO
////////////////////////////////////////////////////////////////////////////////
bool CONTENTS_INFO::Save(LPCTSTR lpszFullPath, LPCTSTR lpszSaveID)
{
	// 0000911: 마법사콘텐츠의 저장되는 XML내에 마법사콘텐츠 자신의 콘텐츠ID를 넣어서 저장하도록함
	if(CONTENTS_WIZARD == nContentsType && !strWizardXML.IsEmpty())
	{
		CXmlParser xmlWizardData;

		if( xmlWizardData.LoadXML(strWizardXML)    == XMLPARSER_SUCCESS &&
			xmlWizardData.SetXPath("/FlashWizard") == XMLPARSER_SUCCESS )
		{
			if(xmlWizardData.SetAttribute("id", strId) != XMLPARSER_SUCCESS)
			{
				xmlWizardData.NewAttribute("id");
				xmlWizardData.SetAttribute("id", strId);
			}

			// xml 저장
			CString strXml = xmlWizardData.GetXML("/");
			strXml.Replace("\t",_T(""));
			strXml.Replace("\r",_T(""));
			strXml.Replace("\n",_T(""));
			strWizardXML = strXml;
		}
	}

	UbcIniPtr()->WriteProfileString(lpszSaveID, "contentsId"  , strId                    );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "contentsType", ::ToString(nContentsType));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "contentsName", strContentsName          );

	// 부속파일처리 관련 수정
	CString strChildPath;
	if(!strParentId.IsEmpty())
	{
		CString strContentsPath = UBC_CONTENTS_PATH;
		int nFindPos = strLocalLocation.MakeLower().Find(strContentsPath.MakeLower());
		if(nFindPos >= 0)
		{
			strChildPath = strLocalLocation.Mid(nFindPos + strContentsPath.GetLength());
		}
	}

	//if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	//{
		//if(bNew){
		//	strServerLocation = "/contents/";
		//	strServerLocation += GetEnvPtr()->m_strPackage;
		//	strServerLocation += "/";
		//}
		//else
		if(strServerLocation.IsEmpty() || strServerLocation.MakeLower().Compare("\\contents\\enc\\")==0)
		{
			strServerLocation = "/contents/";
			// 부속파일처리 관련 수정
			//strServerLocation += GetEnvPtr()->m_strPackage;
			strServerLocation += (!strParentId.IsEmpty() ? strParentId : strId);
			strServerLocation += "/";
			strServerLocation += strChildPath;
		}
	//}
	//else
	//{
	//	strServerLocation = "\\Contents\\ENC\\" + strChildPath;
	//}

	UbcIniPtr()->WriteProfileString(lpszSaveID, "location"			, strServerLocation            );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "filename"          , strFilename                  );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "runningTime"       , ::ToString(nRunningTime)     );

	UbcIniPtr()->WriteProfileString(lpszSaveID, "volume"            , ::ToString(nFilesize)        );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "contentsState"     , ::ToString(nContentsState)   );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "promotionValueList", strPromotionValueList        );

	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment1"          , ForSaveComment(strComment[0]));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment2"          , ForSaveComment(strComment[1]));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment3"          , ForSaveComment(strComment[2]));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment4"          , ForSaveComment(strComment[3]));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment5"          , ForSaveComment(strComment[4]));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment6"          , ForSaveComment(strComment[5]));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment7"          , ForSaveComment(strComment[6]));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment8"          , ForSaveComment(strComment[7]));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment9"          , ForSaveComment(strComment[8]));
	UbcIniPtr()->WriteProfileString(lpszSaveID, "comment10"         , ForSaveComment(strComment[9]));

	UbcIniPtr()->WriteProfileString(lpszSaveID, "bgColor"           , strBgColor                   );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "fgColor"           , strFgColor                   );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "font"              , strFont                      );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "fontSize"          , ::ToString(nFontSize)        );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "playSpeed"         , ::ToString(nPlaySpeed)       );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "soundVolume"       , ::ToString(nSoundVolume)     );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "direction"         , ::ToString(nDirection)       );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "align"             , ::ToString(nAlign)           );

	UbcIniPtr()->WriteProfileString(lpszSaveID, "width"             , ::ToString(nWidth)           );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "height"            , ::ToString(nHeight)          );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "currentComment"    , ::ToString(nCurrentComment)  );

	// 0000611: 플레쉬 콘텐츠 마법사
	UbcIniPtr()->WriteProfileString(lpszSaveID, "wizardXML"         , strWizardXML                 );	// wizard 용 XML string
	UbcIniPtr()->WriteProfileString(lpszSaveID, "wizardFiles"       , strWizardFiles               );	// wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

	UbcIniPtr()->WriteProfileString(lpszSaveID, "parentId"          , strParentId                  );
	UbcIniPtr()->WriteProfileString(lpszSaveID, "isPublic"          , ::ToString(bIsPublic)        );

	UbcIniPtr()->WriteProfileString(lpszSaveID, "MD5"               , strFileMD5                   );	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	UbcIniPtr()->WriteProfileString(lpszSaveID, "IncludeCategory"   , strIncludeCategory           );	
	UbcIniPtr()->WriteProfileString(lpszSaveID, "ExcludeCategory"   , strExcludeCategory           );	

	//skpark same_size_file_problem 2014.06.11
	//이러면 안된다 lastModifiedTime 은 modify 되었을때 바뀌고, write 할때는 무조건 값을 쓴다...
	// write 할때 modify 여부를 가지고 lastModifiedTime 을 결정해서는 안된다.
	// lastModifiedTime 은 유지되어야 한다.
	if(CDataContainer::getInstance()->m_bUseTimeCheck) {
		UbcIniPtr()->WriteProfileString(lpszSaveID, "LastModifiedTime"   , strRegisterTime         );	
		//UbcIniPtr()->WriteProfileString(lpszSaveID, "X_LastModifiedTime"   , strVerifyTime         );	
		TraceLog(("write LastModifiedTime[%s]=%s", lpszSaveID,strRegisterTime));
		CString key = lpszSaveID;
		int keyPrefixLen = strlen("Contents_");
		if(key.GetLength() > keyPrefixLen && key.Mid(0, keyPrefixLen) == "Contents_" ){ 
			UbcIniPtr()->WriteProfileInt(lpszSaveID, "Modified"   , bModified);	
			TraceLog(("write Modified[%s]=%d", lpszSaveID,bModified));
		}
	}
	return true;
}

bool CONTENTS_INFO::Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID)
{
	strId                 =                UbcIniPtr()->GetProfileString(lpszLoadID, "contentsId"        );
	nContentsType         =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "contentsType"      );     
	strParentId           =                UbcIniPtr()->GetProfileString(lpszLoadID, "parentId"          );
	strContentsName       =                UbcIniPtr()->GetProfileString(lpszLoadID, "contentsName"      );
	strServerLocation     =                UbcIniPtr()->GetProfileString(lpszLoadID, "location"          );
	strFilename           =                UbcIniPtr()->GetProfileString(lpszLoadID, "filename"          );
	nRunningTime          =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "runningTime"       );     
	nFilesize             =        _atoi64(UbcIniPtr()->GetProfileString(lpszLoadID, "volume"            ));      
	nContentsState        =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "contentsState"     );
	strPromotionValueList =                UbcIniPtr()->GetProfileString(lpszLoadID, "promotionValueList");
	strComment[0]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment1"          ));
	strComment[1]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment2"          ));
	strComment[2]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment3"          ));
	strComment[3]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment4"          ));
	strComment[4]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment5"          ));
	strComment[5]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment6"          ));
	strComment[6]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment7"          ));
	strComment[7]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment8"          ));
	strComment[8]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment9"          ));
	strComment[9]         = ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment10"         ));
	strBgColor            =                UbcIniPtr()->GetProfileString(lpszLoadID, "bgColor"           );
	strFgColor            =                UbcIniPtr()->GetProfileString(lpszLoadID, "fgColor"           );
	strFont               =                UbcIniPtr()->GetProfileString(lpszLoadID, "font"              );
	nFontSize             =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "fontSize"          );
	nPlaySpeed            =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "playSpeed"         );
	nSoundVolume          =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "soundVolume"       );
	nDirection            =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "direction"         );
	nAlign                =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "align"             , 5);
	nWidth                =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "width"             );
	nHeight               =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "height"            );
	nCurrentComment       =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "currentComment"    , 2);
	strWizardXML          =                UbcIniPtr()->GetProfileString(lpszLoadID, "wizardXML"         );	// wizard 용 XML string
	strWizardFiles        =                UbcIniPtr()->GetProfileString(lpszLoadID, "wizardFiles"       ); // wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)
	bIsPublic             =                UbcIniPtr()->GetProfileInt   (lpszLoadID, "isPublic"          );     
	strFileMD5            =                UbcIniPtr()->GetProfileString(lpszLoadID, "MD5"               );	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	strIncludeCategory    =                UbcIniPtr()->GetProfileString(lpszLoadID, "IncludeCategory"   );	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	strExcludeCategory    =                UbcIniPtr()->GetProfileString(lpszLoadID, "ExcludeCategory"   );	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	strRegisterTime		  =					UbcIniPtr()->GetProfileString(lpszLoadID, "LastModifiedTime" );	 //skpark same_size_file_problem 2014.06.11
	//strVerifyTime		  =					UbcIniPtr()->GetProfileString(lpszLoadID, "X_LastModifiedTime" );	 //skpark same_size_file_problem 2014.06.11
	
	// 부속파일처리 관련 수정
	CString strCompareStr = "/contents/" + (!strParentId.IsEmpty() ? strParentId : strId) + "/";
	if(strServerLocation.MakeLower().Find(strCompareStr.MakeLower()) == 0)
	{
		strLocalLocation = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH + strServerLocation.Mid(strCompareStr.GetLength());
	}
	else
	{
		strLocalLocation = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH;
	}
	
	// skpark same_size_file_problem 2014.06.11
	bModified		  =					UbcIniPtr()->GetProfileInt(lpszLoadID, "Modified" );	 //skpark same_size_file_problem 2014.06.11
	if(bModified){ // skpark same_size_file_problem 2014.06.11
		CHANGE_INFO* aInfo = new CHANGE_INFO();
		aInfo->modifiedTime = strRegisterTime;
		aInfo->fullpath = strLocalLocation + strFilename;
		CDataContainer::getInstance()->m_ModifiedTimeMap[lpszLoadID] = aInfo;
		TraceLog(("Load m_ModifiedTimeMap[%s]=%s,%s", lpszLoadID,strFilename,strRegisterTime));
	}
	
	// // file 진위여부를 체크하기 위해 MD5를 사용한다.
	// file size 는 열때마다 계산한다. copy 등에의해 수정 한경우도 있으므로
	//CFileFind ff;
	//CString szPath;
	////szPath.Format("%s%s%s", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONTENTS_PATH, strFilename);
	//szPath.Format("%s%s", strLocalLocation, strFilename);
	//if(ff.FindFile(szPath))
	//{
	//	ff.FindNextFile();
	//	nFilesize = ff.GetLength();
	//	bLocalFileExist = true; 
	//}
	//ff.Close();

	// 각 컨텐츠를 서버에있는지 일일이 확인하는 작업에는 시간이 오래 소요될 수 있으므로
	// 다음과 같이 Enterprise 스튜디오에서 서버에 등록된 패키지의 경우에는 패키지를 로드할때 모두 서버에 등록된 콘텐츠라고 본다
	bServerFileExist = (GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty());

	return true;
}
//skpark same_size_file_problem
void CONTENTS_INFO::SetModified()
{
	this->bModified = true;

	CTime now = time(NULL);
	//this->strVerifyTime = this->strRegisterTime;
	this->strRegisterTime.Format("%04d/%02d/%02d %02d:%02d:%02d" 					
		,now.GetYear(),now.GetMonth(),now.GetDay()
		,now.GetHour(),now.GetMinute(),now.GetSecond());

	CString key = "Contents_" + this->strId;

	CHANGE_INFO* aInfo=0;
	if(CDataContainer::getInstance()->m_ModifiedTimeMap.Lookup(key, (void*&)aInfo)){
		aInfo->modifiedTime = this->strRegisterTime;
		//aInfo->x_modifiedTime = this->strVerifyTime;
		TraceLog(("set m_ModifiedTimeMap[%s]=%s,%s", key, this->strRegisterTime, this->strFilename));
	}else{
		aInfo = new CHANGE_INFO;
		aInfo->modifiedTime = this->strRegisterTime;
		//aInfo->x_modifiedTime = this->strVerifyTime;
		if(	this->strLocalLocation.IsEmpty() ||
			this->strLocalLocation.MakeLower().Find("\\contents\\enc\\") < 0)
		{
			// strLocalLocation 이 "\\contents\\enc\\" 를 포함하지 않는다면, 아직 LocalLocation 이 제대로 설정되지 않은 경우이다.
			// 직접 구해서 넣어 주어야 한다.
			CString location = GetEnvPtr()->m_PackageInfo.szDrive + UBC_CONTENTS_PATH;
			CString strCompareStr = "/contents/" + (!this->strParentId.IsEmpty() ? this->strParentId : this->strId) + "/";
			if(this->strServerLocation.MakeLower().Find(strCompareStr.MakeLower()) == 0)
			{
				location.Append(strServerLocation.Mid(strCompareStr.GetLength()));
			}
			aInfo->fullpath = location + this->strFilename;
		}else{
			aInfo->fullpath = this->strLocalLocation + this->strFilename;
		}
		CDataContainer::getInstance()->m_ModifiedTimeMap[key] = aInfo;
		TraceLog(("create m_ModifiedTimeMap[%s]=%s,%s", key, this->strRegisterTime, aInfo->fullpath));
	}

}

void CONTENTS_INFO::UnSetModified()
{
	this->bModified = false;

	CString key = "Contents_" + this->strId;

	CHANGE_INFO *aInfo = 0;
	if(CDataContainer::getInstance()->m_ModifiedTimeMap.Lookup(key, (void*&)aInfo)){
		if(aInfo) delete aInfo;	
		CDataContainer::getInstance()->m_ModifiedTimeMap.RemoveKey(key); 
		TraceLog(("unset m_ModifiedTimeMap[%s]=%s,%s", key, this->strRegisterTime, this->strFilename));
	}
}


////////////////////////////////////////////////////////////////////////////////
// PLAYCONTENTS_INFO
////////////////////////////////////////////////////////////////////////////////
bool PLAYCONTENTS_INFO::Save(LPCTSTR lpszFullPath, CONTENTS_INFO* pContentsInfo)
{
	CString app_name;
	app_name.Format("Template%sFrame%sSchedule%s", strTemplateId, strFrameId, strId);	// Template77Frame02Schedule7702_0001

	UbcIniPtr()->WriteProfileString(app_name, "templateId"      , strTemplateId         );
	UbcIniPtr()->WriteProfileString(app_name, "frameId"         , strFrameId            );
	UbcIniPtr()->WriteProfileString(app_name, "scheduleId"      , strId                 );
	UbcIniPtr()->WriteProfileString(app_name, "startDate"       , ::ToString(nStartDate));
	UbcIniPtr()->WriteProfileString(app_name, "endDate"         , ::ToString(nEndDate  ));
	UbcIniPtr()->WriteProfileString(app_name, "startTime"       , strStartTime          );
	UbcIniPtr()->WriteProfileString(app_name, "endTime"         , strEndTime            );
	UbcIniPtr()->WriteProfileString(app_name, "playOrder"       , ::ToString(nPlayOrder));
	UbcIniPtr()->WriteProfileString(app_name, "timeScope"       , ::ToString(nTimeScope));
	UbcIniPtr()->WriteProfileString(app_name, "openningFlag"    , "1"                   );
	UbcIniPtr()->WriteProfileString(app_name, "priority"        , ::ToString(nPriority) );
	UbcIniPtr()->WriteProfileString(app_name, "castingState"    , "3"                   );
	UbcIniPtr()->WriteProfileString(app_name, "operationalState", "1"                   );
	UbcIniPtr()->WriteProfileString(app_name, "adminState"      , "1"                   );
	UbcIniPtr()->WriteProfileString(app_name, "parentScheduleId", strParentPlayContentsId);
	UbcIniPtr()->WriteProfileString(app_name, "touchTime"       , strTouchTime          );
	UbcIniPtr()->WriteProfileString(app_name, "bDontDownload"     , ::ToString(bDontDownload)          );

	if(pContentsInfo)
		pContentsInfo->Save(lpszFullPath, app_name);

	return true;
}

bool PLAYCONTENTS_INFO::Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID)
{
	strTemplateId           = UbcIniPtr()->GetProfileString(lpszLoadID, "templateId"      );
	strFrameId              = UbcIniPtr()->GetProfileString(lpszLoadID, "frameId"         );
	strId                   = UbcIniPtr()->GetProfileString(lpszLoadID, "scheduleId"      );
	nStartDate              = UbcIniPtr()->GetProfileInt   (lpszLoadID, "startDate"       );     
	nEndDate                = UbcIniPtr()->GetProfileInt   (lpszLoadID, "endDate"         );     
	strStartTime            = UbcIniPtr()->GetProfileString(lpszLoadID, "startTime"       );
	strEndTime              = UbcIniPtr()->GetProfileString(lpszLoadID, "endTime"         );
	nPlayOrder              = UbcIniPtr()->GetProfileInt   (lpszLoadID, "playOrder"       );     
	nTimeScope              = UbcIniPtr()->GetProfileInt   (lpszLoadID, "timeScope"       );     
	nPriority               = UbcIniPtr()->GetProfileInt   (lpszLoadID, "priority"        , 3);     
	strParentPlayContentsId = UbcIniPtr()->GetProfileString(lpszLoadID, "parentScheduleId");
	strTouchTime            = UbcIniPtr()->GetProfileString(lpszLoadID, "touchTime"       );
	strContentsId           = UbcIniPtr()->GetProfileString(lpszLoadID, "contentsId"      );
	bDontDownload           = UbcIniPtr()->GetProfileIntA   (lpszLoadID, "bDontDownload"        , 0);     

	if(!strContentsId.IsEmpty())
	{
		CONTENTS_INFO* pContentsInfo = CDataContainer::getInstance()->GetContents(strContentsId);
		if(pContentsInfo == NULL)
		{
			pContentsInfo = new CONTENTS_INFO;
			pContentsInfo->Load(lpszFullPath, lpszLoadID);

			CString strErrMsg;
			if(!CDataContainer::getInstance()->AddContents(pContentsInfo, strErrMsg))
			{
				CString strTemp;
				strTemp.Format(LoadStringById(IDS_DATACONTAINER_MSG003), strContentsId, strErrMsg);
//				UbcMessageBox(strTemp, MB_ICONWARNING);
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// FRAME_INFO
////////////////////////////////////////////////////////////////////////////////
bool FRAME_INFO::AddPlayContents(PLAYCONTENTS_INFO* pInPlayContentsInfo)
{
	pInPlayContentsInfo->strTemplateId = strTemplateId;
	pInPlayContentsInfo->strFrameId = strId;
	CString strTmpID = pInPlayContentsInfo->strId;

	pInPlayContentsInfo->strId.Format("%s%s_%s", pInPlayContentsInfo->strTemplateId, pInPlayContentsInfo->strFrameId, strTmpID);

	if(pInPlayContentsInfo->bIsDefaultPlayContents)
		arCyclePlayContentsList.Add(pInPlayContentsInfo);
	else
		arTimePlayContentsList.Add(pInPlayContentsInfo);

	return true;
}

bool FRAME_INFO::DeletePlayContents(PLAYCONTENTS_INFO* pInPlayContentsInfo)
{
	int nCount = arCyclePlayContentsList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)arCyclePlayContentsList.GetAt(i);
		if(pInPlayContentsInfo == pPlayContentsInfo)
		{
			delete pPlayContentsInfo;
			arCyclePlayContentsList.RemoveAt(i);
			return true;
		}
	}

	nCount = arTimePlayContentsList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)arTimePlayContentsList.GetAt(i);
		if(pInPlayContentsInfo == pPlayContentsInfo)
		{
			delete pPlayContentsInfo;
			arTimePlayContentsList.RemoveAt(i);
			return true;
		}
	}

	return false;
}

void FRAME_INFO::DeletePlayContentsList()
{
	int nCount = arCyclePlayContentsList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)arCyclePlayContentsList.GetAt(i);
		delete pPlayContentsInfo;
	}
	arCyclePlayContentsList.RemoveAll();

	//
	nCount = arTimePlayContentsList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)arTimePlayContentsList.GetAt(i);
		delete pPlayContentsInfo;
	}
	arTimePlayContentsList.RemoveAll();
}

bool FRAME_INFO::Save(LPCTSTR lpszFullPath, PLAYCONTENTS_INFO_LIST* pCyclePlayContentsInfoList, PLAYCONTENTS_INFO_LIST* pTimePlayContentsInfoList)
{
	TCHAR szDrive[_MAX_DRIVE] = { 0x00 };
	TCHAR szDir  [_MAX_DIR  ]	= { 0x00 };
	TCHAR szFname[_MAX_FNAME]	= { 0x00 };
	TCHAR szExt  [_MAX_EXT  ]	= { 0x00 };

	_tsplitpath(lpszFullPath, szDrive, szDir, szFname, szExt);
	CString strHostName = szFname;

	CString app_name;
	app_name.Format("Template%sFrame%s", strTemplateId, strId);	// Template77Frame02

	UbcIniPtr()->WriteProfileString(app_name, "templateId"     , strTemplateId                      );
	UbcIniPtr()->WriteProfileString(app_name, "frameId"        , strId                              );
	UbcIniPtr()->WriteProfileString(app_name, "grade"          , ::ToString(nGrade         )        );
	UbcIniPtr()->WriteProfileString(app_name, "width"          , ::ToString(rcRect.Width() )        );
	UbcIniPtr()->WriteProfileString(app_name, "height"         , ::ToString(rcRect.Height())        );
	UbcIniPtr()->WriteProfileString(app_name, "x"              , ::ToString(rcRect.left    )        );
	UbcIniPtr()->WriteProfileString(app_name, "y"              , ::ToString(rcRect.top     )        );
	UbcIniPtr()->WriteProfileString(app_name, "isPIP"          , ::ToString(bIsPIP         )        );
	UbcIniPtr()->WriteProfileString(app_name, "zIndex"         , ::ToString(nZIndex        )        );
	UbcIniPtr()->WriteProfileString(app_name, "alpha"          , ::ToString(nAlpha         )        );
	UbcIniPtr()->WriteProfileString(app_name, "isTV"           , ::ToString(bIsTV          )        );
	UbcIniPtr()->WriteProfileString(app_name, "borderStyle"    , strBorderStyle                     );
	UbcIniPtr()->WriteProfileString(app_name, "borderThickness", ::ToString(nBorderThickness)       );
	UbcIniPtr()->WriteProfileString(app_name, "borderColor"    , ::GetColorFromString(crBorderColor));
	UbcIniPtr()->WriteProfileString(app_name, "cornerRadius"   , ::ToString(nCornerRadius)          );
	UbcIniPtr()->WriteProfileString(app_name, "comment1"       , ForSaveComment(strComment[0])      ); // 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
	UbcIniPtr()->WriteProfileString(app_name, "comment2"       , ForSaveComment(strComment[1])      );
	UbcIniPtr()->WriteProfileString(app_name, "comment3"       , ForSaveComment(strComment[2])      );

	CString strIdList;
	if(pCyclePlayContentsInfoList)
	{
		for(int i=0; i<pCyclePlayContentsInfoList->GetCount(); i++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = pCyclePlayContentsInfoList->GetAt(i);
			if(strIdList.GetLength() != 0)
			{
				strIdList.Append(",");
			}

			if(pPlayContentsInfo->strId.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				// 0000737: 콘텐츠 패키지 INI 파일에 콘텐츠 패키지 ID길이 가 계속 늘어남
				//pPlayContentsInfo->strId.Replace(SAMPLE_FILE_KEY, strHostName);
				pPlayContentsInfo->strId = pPlayContentsInfo->strId.Left(pPlayContentsInfo->strId.Find(SAMPLE_FILE_KEY))
									 + strHostName
									 + pPlayContentsInfo->strId.Right(6);
			}

			if(pPlayContentsInfo->strParentPlayContentsId.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				//pPlayContentsInfo->strId.Replace(SAMPLE_FILE_KEY, strHostName);
				pPlayContentsInfo->strParentPlayContentsId = pPlayContentsInfo->strParentPlayContentsId.Left(pPlayContentsInfo->strParentPlayContentsId.Find(SAMPLE_FILE_KEY))
												   + strHostName
												   + pPlayContentsInfo->strParentPlayContentsId.Right(6);
			}

			strIdList.Append(app_name + "Schedule" + pPlayContentsInfo->strId);
		}
	}
	UbcIniPtr()->WriteProfileString(app_name, "DefaultScheduleList", strIdList);

	strIdList = _T("");
	if(pTimePlayContentsInfoList)
	{
		for(int i=0; i<pTimePlayContentsInfoList->GetCount(); i++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = pTimePlayContentsInfoList->GetAt(i);
			if(strIdList.GetLength() != 0)
			{
				strIdList.Append(",");
			}

			if(pPlayContentsInfo->strId.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				// 0000737: 콘텐츠 패키지 INI 파일에 콘텐츠 패키지 ID길이 가 계속 늘어남
				pPlayContentsInfo->strId = pPlayContentsInfo->strId.Left(pPlayContentsInfo->strId.Find(SAMPLE_FILE_KEY))
									 + strHostName
									 + pPlayContentsInfo->strId.Right(6);
			}

			if(pPlayContentsInfo->strParentPlayContentsId.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				pPlayContentsInfo->strParentPlayContentsId = pPlayContentsInfo->strParentPlayContentsId.Left(pPlayContentsInfo->strParentPlayContentsId.Find(SAMPLE_FILE_KEY))
												   + strHostName
												   + pPlayContentsInfo->strParentPlayContentsId.Right(6);
			}

			strIdList.Append(app_name + "Schedule" + pPlayContentsInfo->strId);
		}
	}
	UbcIniPtr()->WriteProfileString(app_name, "ScheduleList", strIdList);

	return true;
}

bool FRAME_INFO::Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID)
{
	strTemplateId                 =                      UbcIniPtr()->GetProfileString(lpszLoadID, "templateId"         );
	strId                         =                      UbcIniPtr()->GetProfileString(lpszLoadID, "frameId"            );
	nGrade                        =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "grade"              );
	rcRect.left                   =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "x"                  );
	rcRect.top                    =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "y"                  );
	rcRect.right                  =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "width"              ) + rcRect.left;
	rcRect.bottom                 =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "height"             ) + rcRect.top ;
	bIsPIP                        =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "isPIP"              );
	nZIndex                       =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "zIndex"             );
	nAlpha                        =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "alpha"              );
	bIsTV                         =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "isTV"               );
	strBorderStyle                =                      UbcIniPtr()->GetProfileString(lpszLoadID, "borderStyle"        );
	nBorderThickness              =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "borderThickness"    );
	crBorderColor                 = ::GetColorFromString(UbcIniPtr()->GetProfileString(lpszLoadID, "borderColor"        ));
	nCornerRadius                 =                      UbcIniPtr()->GetProfileInt   (lpszLoadID, "cornerRadius"       );
	strCyclePlayContentsIdList    =                      UbcIniPtr()->GetProfileString(lpszLoadID, "DefaultScheduleList");
	strTimeBasePlayContentsIdList =                      UbcIniPtr()->GetProfileString(lpszLoadID, "ScheduleList"       );
	strComment[0]                 =       ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment1"           ));  // 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
	strComment[1]                 =       ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment2"           ));
	strComment[2]                 =       ForLoadComment(UbcIniPtr()->GetProfileString(lpszLoadID, "comment3"           ));

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// TEMPLATE_INFO
////////////////////////////////////////////////////////////////////////////////
bool TEMPLATE_INFO::Save(LPCTSTR lpszFullPath, FRAME_LINK_LIST* pFrameLinkList)
{
	CString app_name;
	app_name.Format("Template%s", strId);

	UbcIniPtr()->WriteProfileString(app_name, "templateId" , strId                          );
	UbcIniPtr()->WriteProfileString(app_name, "width"      , ::ToString(rcRect.Width())     );
	UbcIniPtr()->WriteProfileString(app_name, "height"     , ::ToString(rcRect.Height())    );
	UbcIniPtr()->WriteProfileString(app_name, "bgColor"    , ::GetColorFromString(crBgColor));
	UbcIniPtr()->WriteProfileString(app_name, "bgImage"    , strBgImage                     );
	UbcIniPtr()->WriteProfileString(app_name, "bgType"     , ::ToString(nBgType)            );
	UbcIniPtr()->WriteProfileString(app_name, "description", strDescription                 );
	UbcIniPtr()->WriteProfileString(app_name, "shortCut"   , strShortCut                    );

	CString strIdList;
	if(pFrameLinkList)
	{
		for(int i=0; i<pFrameLinkList->GetCount(); i++)
		{
			FRAME_LINK& frame_link = pFrameLinkList->GetAt(i);

			if(strIdList.GetLength() != 0)
				strIdList.Append(",");
			strIdList.Append(app_name + "Frame" + frame_link.pFrameInfo->strId);
		}
	}
	UbcIniPtr()->WriteProfileString(app_name, "FrameList", strIdList);

	return true;
}

bool TEMPLATE_INFO::Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID)
{
	strId           = UbcIniPtr()->GetProfileString(lpszLoadID, "templateId" );
	rcRect.right    = UbcIniPtr()->GetProfileInt   (lpszLoadID, "width"      );
	rcRect.bottom   = UbcIniPtr()->GetProfileInt   (lpszLoadID, "height"     );
	crBgColor       = ::GetColorFromString(UbcIniPtr()->GetProfileString(lpszLoadID, "bgColor"));
	strBgImage      = UbcIniPtr()->GetProfileString(lpszLoadID, "bgImage"    );
	nBgType         = UbcIniPtr()->GetProfileInt   (lpszLoadID, "bgType"     );
	strDescription  = UbcIniPtr()->GetProfileString(lpszLoadID, "description");
	strShortCut     = UbcIniPtr()->GetProfileString(lpszLoadID, "shortCut"   );
	strFrameList    = UbcIniPtr()->GetProfileString(lpszLoadID, "FrameList"  );

	return true;
}


////////////////////////////////////////////////////////////////////////////////
// TEMPLATE_LINK
////////////////////////////////////////////////////////////////////////////////
void TEMPLATE_LINK::DeleteFrameList()
{
	int nCount = arFrameLinkList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		FRAME_LINK& frm_link = arFrameLinkList.GetAt(i);
		frm_link.pFrameInfo->DeletePlayContentsList();
		delete frm_link.pFrameInfo;
	}
	arFrameLinkList.RemoveAll();
}

void TEMPLATE_LINK::RecalcLayout()
{
	int nWidth = TEMPLATE_WIDTH;
	int nHeight = pTemplateInfo->rcRect.Width() * TEMPLATE_HEIGHT / TEMPLATE_WIDTH;
	if(nHeight < pTemplateInfo->rcRect.Height())
	{
		nHeight = TEMPLATE_HEIGHT;
		nWidth = pTemplateInfo->rcRect.Height() * TEMPLATE_WIDTH / TEMPLATE_HEIGHT;

		fScale = ((double)TEMPLATE_HEIGHT) / ((double)pTemplateInfo->rcRect.Height());
	}
	else
	{
		fScale = ((double)TEMPLATE_WIDTH) / ((double)pTemplateInfo->rcRect.Width());
	}
}

// 강제로 PIP 로 설정등 과 같이 프레임에 강제 설정해야 하는 정보를 이 함수에서 설정한다.
void TEMPLATE_LINK::SetForceFrameInfo()
{
	ReCalcPIP();
}

void TEMPLATE_LINK::ReCalcPIP()
{
	int nCount = arFrameLinkList.GetCount();

	// 강제로 PIP 값 설정
	for(int i=0; i<nCount; i++)
	{
		FRAME_LINK& selframe_link = arFrameLinkList.GetAt(i);

		bool bIsInclusion = false;	// 다른 프레임에 포함되어 있는지
		bool bIsOverlap   = false;	// 다른 프레임과 겹쳐있는지
		bool bHaveFrame   = false;	// 다른 프레임을 포함하고 있는지

		bool bIsPIP = false;

		for(int j=0; j<nCount; j++)
		{
			FRAME_LINK& frame_link = arFrameLinkList.GetAt(j);
			if(selframe_link.pFrameInfo->strId == frame_link.pFrameInfo->strId)
			{
				bIsPIP = selframe_link.pFrameInfo->bIsPIP;
				continue;
			}

			// 다른 프레임과 겹쳐있는지 여부 체크
			CRect rcOverlap;
			if(selframe_link.pFrameInfo->rcRect == frame_link.pFrameInfo->rcRect)
			{
				bIsInclusion = true;
				bHaveFrame = true;
			}
			else if( ::IntersectRect(rcOverlap, selframe_link.pFrameInfo->rcRect, frame_link.pFrameInfo->rcRect) )
			{
				// 다른 프레임에 포함되어 있는지 여부 체크
				if( selframe_link.pFrameInfo->rcRect == rcOverlap )
				{
					bIsInclusion = true;
				}
				// 다른 프레임을 포함하고 있는지 여부 체크
				else if( frame_link.pFrameInfo->rcRect == rcOverlap )
				{
					bHaveFrame = true;
				}
				else
				{
					bIsOverlap = true;
				}
			}
		}

		// PIP를 갖고있는(포함하는) 프레임은 PIP로 설정할 수 없도록한다
		if(bHaveFrame) selframe_link.pFrameInfo->bIsPIP = false;
		// 다른 프레임과 일부분이 겹치는 프레임은 PIP속성을 설정할 수 있다(사용자가 직접 설정, 이전값 유지) 
		else if(bIsOverlap) selframe_link.pFrameInfo->bIsPIP = bIsPIP;
		// 다른 프레임안에 완전히 포함되며 겹쳐있지 않은 프레임은 자동으로 PIP속성을 갖도록한다
		else if(bIsInclusion) selframe_link.pFrameInfo->bIsPIP = true;
		// 그외 다른 프레임과 겹치지 않는 프레임은 PIP로 설정할 수 없도록한다
		else selframe_link.pFrameInfo->bIsPIP = false;
	}
}

////////////////////////////////////////////////////////////////////////////////
// CDataContainer
////////////////////////////////////////////////////////////////////////////////

CDataContainer*	CDataContainer::_instance;
CCriticalSection CDataContainer::_cs;

CDataContainer* CDataContainer::getInstance()
{
	_cs.Lock();
	if(_instance == NULL)
	{
		_instance = new CDataContainer();
	}
	_cs.Unlock();

	return _instance;
}

void CDataContainer::clearInstance()
{
	_cs.Lock();
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
	_cs.Unlock();
}

CDataContainer::CDataContainer()
:	m_strSoundVolume("100")
,	m_strDefaultTemplate(_T(""))
,	m_strAdminState("1")
,	m_strOperationalState("0")
,	m_strDescription(_T(""))
,	m_strNetworkUse("0")
,	m_strSite(_T(""))
,	m_strLastWrittenUser(_T(""))
,	m_strLastWrittenTime(_T(""))
,	m_bMonitorOn(false)
,	m_bMonitorOff(false)
,   m_bUseTimeCheck(false)  //skpark same_size_file_problem
{
}

CDataContainer::~CDataContainer()
{
	DeleteAllData();
}

bool CDataContainer::DeleteAllData(bool bIncludeBackup/*=true*/)
{
	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		m_ContentsMap.GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );
		if(pContentsInfo) delete pContentsInfo;
	}
	m_ContentsMap.RemoveAll();

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);
		template_link.DeleteFrameList();
		if(template_link.pTemplateInfo) delete template_link.pTemplateInfo;
	}
	m_AllTemplateList.RemoveAll();
	m_PlayTemplateList.RemoveAll();

	// Template Size 삭제-구현석
	GetEnvPtr()->m_lsTempRect.RemoveAll();

	m_strSoundVolume		= "100";
	m_strDefaultTemplate	= _T("");
	m_strAdminState			= _T("1");
	m_strOperationalState	= _T("0");

	if(bIncludeBackup) DeleteAllBackupData();

	//skpark same_size_file_problem 2014.06.12
	pos =  m_ModifiedTimeMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CHANGE_INFO* aInfo=0;
		m_ModifiedTimeMap.GetNextAssoc( pos, strContentsId, (void*&)aInfo );
		if(aInfo) delete aInfo;
	}
	m_ModifiedTimeMap.RemoveAll();

	m_strIniPath = ""; //skpark same_size_file_problem 2014.06.12

	return true;
}

bool CDataContainer::AddContents(CONTENTS_INFO* pInContentsInfo, CString& strErrMsg)
{
	CONTENTS_INFO* pContentsInfo = NULL;
	int nSeq = 1;
	int nIdx;
	CString strId, strNewId, strTmpName;

	// 2009-05-11 contents name 없으면 무조건 skip
	if(pInContentsInfo->strContentsName.Trim().IsEmpty())
	{
		strErrMsg = LoadStringById(IDS_DATACONTAINER_MSG004);
		return false;
	}

	//contents id가 없을 경우
	if(pInContentsInfo->strId.GetLength() == 0)
	{
//		if(pInContentsInfo->strFilename == _T(""))
//		{
//			strTmpName = pInContentsInfo->strContentsName;
//			strTmpName.Replace(" ", _T(""));
//		}
//		else
//		{
//			nIdx = pInContentsInfo->strFilename.Find('.', 0);
//			if(nIdx != -1)
//			{
//				strTmpName = pInContentsInfo->strFilename.Left(nIdx);
//			}
//			else
//			{
//				strTmpName = pInContentsInfo->strFilename;
//			}
//		}
//
//		// [], 는 (), 로 치환함
////		strTmpName.Replace("[", "(");
////		strTmpName.Replace("]", ")");
////		strTmpName.Replace(",", "_");
//		strTmpName = CPreventChar::GetInstance()->ConvertToAvailableChar(strTmpName);
//
//		strId.Format("%s_%02d", strTmpName, nSeq);
//		strNewId = strId;
//
//		while(m_ContentsMap.Lookup(strNewId, (void*&)pContentsInfo))
//		{
//			if(nSeq == 99)
//			{
//				strId = strNewId;
//				nSeq = 1;
//			}
//			strNewId.Format("%s_%02d", strId, nSeq++);
//		}

		//pInContentsInfo->strId = strNewId;

		//// 0000911: 마법사콘텐츠의 저장되는 XML내에 마법사콘텐츠 자신의 콘텐츠ID를 넣어서 저장하도록함
		//if(CONTENTS_WIZARD == pInContentsInfo->nContentsType && !pInContentsInfo->strWizardXML.IsEmpty())
		//{
		//	CXmlParser xmlWizardData;

		//	if( xmlWizardData.LoadXML(pInContentsInfo->strWizardXML) == XMLPARSER_SUCCESS &&
		//		xmlWizardData.SetXPath("/FlashWizard")               == XMLPARSER_SUCCESS )
		//	{
		//		if(xmlWizardData.SetAttribute("id", strNewId) != XMLPARSER_SUCCESS)
		//		{
		//			xmlWizardData.NewAttribute("id");
		//			xmlWizardData.SetAttribute("id", strNewId);
		//		}

		//		// xml 저장
		//		CString strXml = xmlWizardData.GetXML("/");
		//		strXml.Replace("\t",_T(""));
		//		strXml.Replace("\r",_T(""));
		//		strXml.Replace("\n",_T(""));
		//		pInContentsInfo->strWizardXML = strXml;
		//	}
		//}
	}

	// GUID 를 사용하여 Contents ID 를 전환한다.
	strNewId = CUbcGUID::GetInstance()->ToGUID(pInContentsInfo->strId);

	//contents의 id가 중복되지 않도록 해 준다
	if(m_ContentsMap.Lookup(strNewId, (void*&)pContentsInfo))
	{
		//같은 contents 객체이면 추가하지 않는다.
		if(*pContentsInfo == *pInContentsInfo)
		{
			strErrMsg = LoadStringById(IDS_DATACONTAINER_MSG005);
			delete pInContentsInfo;
			return false;
		}

		// GUID 를 사용하여 Contents ID 를 전환한다.
		strNewId = CUbcGUID::GetInstance()->ToGUID(_T(""));

		////같은 contents가 아니라면 중복되지 않는 id를 만들어 준다
		//nSeq = 1;
		//strId = pInContentsInfo->strId;
		//strNewId = strId;
		//while(m_ContentsMap.Lookup(strNewId, (void*&)pContentsInfo))
		//{
		//	if(nSeq == 99)
		//	{
		//		strId = strNewId;
		//		nSeq = 1;
		//	}//if
		//	strNewId.Format("%s_%02d", strId, nSeq++);
		//}//while

		//pInContentsInfo->strId = strNewId;
	}//if

	// 콘텐츠ID가 새로 부여되는 경우
	if(pInContentsInfo->strId != strNewId)
	{
		// 0000911: 마법사콘텐츠의 저장되는 XML내에 마법사콘텐츠 자신의 콘텐츠ID를 넣어서 저장하도록함
		if(CONTENTS_WIZARD == pInContentsInfo->nContentsType && !pInContentsInfo->strWizardXML.IsEmpty())
		{
			CXmlParser xmlWizardData;

			if( xmlWizardData.LoadXML(pInContentsInfo->strWizardXML) == XMLPARSER_SUCCESS &&
				xmlWizardData.SetXPath("/FlashWizard")               == XMLPARSER_SUCCESS )
			{
				if(xmlWizardData.SetAttribute("id", strNewId) != XMLPARSER_SUCCESS)
				{
					xmlWizardData.NewAttribute("id");
					xmlWizardData.SetAttribute("id", strNewId);
				}

				// xml 저장
				CString strXml = xmlWizardData.GetXML("/");
				strXml.Replace("\t",_T(""));
				strXml.Replace("\r",_T(""));
				strXml.Replace("\n",_T(""));
				pInContentsInfo->strWizardXML = strXml;
			}
		}
	}

	pInContentsInfo->strId = strNewId;

	// [], 는 (), 로 치환함
//	pInContentsInfo->strContentsName.Replace("[", "(");
//	pInContentsInfo->strContentsName.Replace("]", ")");
//	pInContentsInfo->strContentsName.Replace(",", "_");
	pInContentsInfo->strContentsName = CPreventChar::GetInstance()->ConvertToAvailableChar(pInContentsInfo->strContentsName);

	//이제 contents map에 없었거나, 중복되지 않은 id로 만들어주었다
	//마지막으로 contents의 이름이 중복되지 않도록 해 준다
	nSeq = 1;
	CString strName, strNewName;
	strName = pInContentsInfo->strContentsName;
	strNewName = strName;
	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos)
	{
		m_ContentsMap.GetNextAssoc(pos, strId, (void*&)pContentsInfo);
		if((pContentsInfo != NULL) && (strNewName == pContentsInfo->strContentsName))
		{
			if(nSeq == 99)
			{
				strName = strNewName;
				nSeq = 1;
			}//if
			strNewName.Format("%s_%02d", strName, nSeq++);
			pos = m_ContentsMap.GetStartPosition();
		}//if
	}//while

	pInContentsInfo->strContentsName = strNewName;
	m_ContentsMap.SetAt(pInContentsInfo->strId, pInContentsInfo);

	return true;
}

bool CDataContainer::AddTemplate(TEMPLATE_INFO* pInTemplateInfo)
{
	// 구현석 같은 템플릿 제거. 2009-6-15 같은 템플릿이 있으면 제대로 표현 못하는 버그 있음.
	for(int i = 0; i < m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == pInTemplateInfo->strId) return false;
	}

	// Template Size 저장-구현석
	POSITION pos = GetEnvPtr()->m_lsTempRect.Find(pInTemplateInfo->rcRect);
	if(!pos) GetEnvPtr()->m_lsTempRect.AddTail(pInTemplateInfo->rcRect);

	TEMPLATE_LINK link(pInTemplateInfo);
	link.RecalcLayout();
	m_AllTemplateList.Add(link);

	int count = m_AllTemplateList.GetCount();
	for(int i=0; i<count-1; i++)
	{
		TEMPLATE_LINK& link1 = m_AllTemplateList.GetAt(i);
		for(int j=i+1; j<count; j++)
		{
			TEMPLATE_LINK& link2 = m_AllTemplateList.GetAt(j);
			if(link1.pTemplateInfo->strId > link2.pTemplateInfo->strId)
			{
				TEMPLATE_LINK tmp_link;
				tmp_link = link1;
				link1 = link2;
				link2 = tmp_link;
			}
		}
	}

	return true;
}

int CDataContainer::AddPlayTemplate(LPCTSTR lpszTemplateId, int nPlayTimes)
{
	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			TEMPLATE_LINK link(template_link.pTemplateInfo, nPlayTimes, template_link.fScale);
			link.arFrameLinkList.Copy(template_link.arFrameLinkList);
			m_PlayTemplateList.Add(link);
			return m_PlayTemplateList.GetCount()-1;
		}
	}

	return -1;
}

int CDataContainer::AddPlayTemplate(LPCTSTR lpszTemplateId, LPCTSTR szPlayTimes)
{
	CString szTimes = szPlayTimes;
	int nRet = szTimes.Find(_T("("));
	if(nRet == -1){
		return AddPlayTemplate(lpszTemplateId, atoi(szPlayTimes));
	}else{
		return AddPlayTemplate(lpszTemplateId, 1);
	}
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 열려있는 host의 설명을 반환한다. \n
/// @return <형: CString> \n
///			열려있는 host의 설명 \n
/////////////////////////////////////////////////////////////////////////////////
CString CDataContainer::GetDescription(void)
{
	return m_strDescription;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 열려있는 host의 설명을 설정한다. \n
/// @param (CString) strDesc : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CDataContainer::SetDescription(CString strDesc)
{
	m_strDescription = strDesc;
}


TEMPLATE_INFO* CDataContainer::CreateNewTemplate()
{
	// new template id 설정
	CRect clsOrgRect(0,0,0,0);

	int nMinId = 1;

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link= m_AllTemplateList.GetAt(i);

		// Modified by 정운형 2009-03-04 오후 6:17
		// 변경내역 :  Template 추가시에 사이즈 조정
		if(i == 0)
		{
			clsOrgRect = template_link.pTemplateInfo->rcRect;
		}
		// Modified by 정운형 2009-03-04 오후 6:17
		// 변경내역 :  Template 추가시에 사이즈 조정

		if(nMinId == atoi(template_link.pTemplateInfo->strId))
		{
			nMinId = atoi(template_link.pTemplateInfo->strId) + 1;
			i = -1;
		}
	}

	TEMPLATE_INFO* pTemplateInfo = new TEMPLATE_INFO;
	pTemplateInfo->strId.Format("%02d", nMinId);
	pTemplateInfo->rcRect = clsOrgRect;
	pTemplateInfo->crBgColor = RGB(0,0,0);

	if( AddTemplate(pTemplateInfo) == false )
	{
		delete pTemplateInfo;
		pTemplateInfo = NULL;
	}

	return pTemplateInfo;
}

FRAME_INFO* CDataContainer::CreateNewFrame(LPCTSTR lpszTemplateId)
{
	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(lpszTemplateId);
	if(pTemplateLink == NULL) return NULL;

	// new frame id 설정
	int nMinId = 1;
	bool bHasPrimaryFrame = false;
	int nCount = pTemplateLink->arFrameLinkList.GetCount();
	int nMaxZIndex = 0;

	for(int i=0; i<nCount; i++)
	{
		FRAME_LINK& frame_link = pTemplateLink->arFrameLinkList.GetAt(i);

		if((frame_link.pFrameInfo->nGrade == 1) && bHasPrimaryFrame == false)
		{
			bHasPrimaryFrame = true;
		}

		if(nMinId == atoi(frame_link.pFrameInfo->strId))
		{
			nMinId = atoi(frame_link.pFrameInfo->strId) + 1;
			i = -1;
		}

		if(frame_link.pFrameInfo->nZIndex < PIP_ZINDEX && nMaxZIndex < frame_link.pFrameInfo->nZIndex)
		{
			nMaxZIndex = frame_link.pFrameInfo->nZIndex;
		}
	}

	FRAME_INFO* pFrameInfo = new FRAME_INFO;
	pFrameInfo->strTemplateId = pTemplateLink->pTemplateInfo->strId;
	pFrameInfo->strId.Format("%02d", nMinId);
	pFrameInfo->nGrade = (bHasPrimaryFrame ? 2 : 1);
	pFrameInfo->rcRect.SetRect(0,0,320,240);
	pFrameInfo->strBorderStyle = "solid";
	pFrameInfo->nBorderThickness = 0;
	pFrameInfo->nZIndex = ++nMaxZIndex;

	FRAME_LINK frame_link(pFrameInfo);
	pTemplateLink->arFrameLinkList.Add(frame_link);

	// play리스트에도 추가
	nCount = m_PlayTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);
		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			template_link.arFrameLinkList.Add(frame_link);
		}
	}

	return pFrameInfo;	
}

TEMPLATE_INFO* CDataContainer::CreateNewTemplateFromServer(TEMPLATE_LINK* pPublicTemplateLink)
{
	if(!pPublicTemplateLink) return NULL;

	// USTB 는 같은 크기의 템플릿만 가능함-구현석
	if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
	{
		for(int i=0; i < m_PlayTemplateList.GetCount(); i++)
		{
			TEMPLATE_LINK& PlayTempLink = m_PlayTemplateList.GetAt(i);
			if( PlayTempLink.pTemplateInfo->rcRect.Width()  != pPublicTemplateLink->pTemplateInfo->rcRect.Width()  ||
				PlayTempLink.pTemplateInfo->rcRect.Height() != pPublicTemplateLink->pTemplateInfo->rcRect.Height() )
			{
				UbcMessageBox(LoadStringById(IDS_DATACONTAINER_MSG006), MB_ICONERROR);
				return NULL;
			}
		}
	}

	TEMPLATE_INFO* pNewTemplateInfo = CreateNewTemplate();
	if(pNewTemplateInfo == NULL) return NULL;

	/////////////////////////////////////////////////////////////////////
	//Template info 멤버 복사(id는 복사하지 않는다)
	CString strNewTemplateId = pNewTemplateInfo->strId;
	*pNewTemplateInfo = *(pPublicTemplateLink->pTemplateInfo);
	pNewTemplateInfo->strId = strNewTemplateId;
	pNewTemplateInfo->strShortCut = _T("");

	/////////////////////////////////////////////////////////////////////
	//Template Link 멤버 복사
	TEMPLATE_LINK* pNewTemplateLink = GetTemplateLink(pNewTemplateInfo->strId);
	if(pNewTemplateLink == NULL) return NULL;

	//Frame Link, Frame Info
	for(int i=0; i<pPublicTemplateLink->arFrameLinkList.GetCount(); i++)
	{
		FRAME_LINK FrameLink = pPublicTemplateLink->arFrameLinkList.GetAt(i);
		FRAME_LINK NewFrameLink;
		NewFrameLink.Copy(FrameLink);

		// 새로생성된 템플릿ID로 설정하고 레이아웃만 생성하므로 복사된 플레이콘텐츠리스트는 삭제한다.
		NewFrameLink.pFrameInfo->strTemplateId = pNewTemplateInfo->strId;
		NewFrameLink.pFrameInfo->strId.Format("%02d", i+1);
		NewFrameLink.pFrameInfo->DeletePlayContentsList();

		pNewTemplateLink->arFrameLinkList.Add(NewFrameLink);
	}

	pNewTemplateLink->nPlayTimes = pPublicTemplateLink->nPlayTimes;
	pNewTemplateLink->fScale	 = pPublicTemplateLink->fScale;
	for(int i=0; i<5; i++)
	{
		pNewTemplateLink->rcViewRect[i] = pPublicTemplateLink->rcViewRect[i];
	}

	pNewTemplateLink->rcViewRectItem    = pPublicTemplateLink->rcViewRectItem;
	pNewTemplateLink->rcViewRectInfo[0] = pPublicTemplateLink->rcViewRectInfo[0];
	pNewTemplateLink->rcViewRectInfo[1] = pPublicTemplateLink->rcViewRectInfo[1];
	/////////////////////////////////////////////////////////////////////

	if( AddTemplate(pNewTemplateInfo) == false )
	{
		delete pNewTemplateInfo;
		pNewTemplateInfo = NULL;
	}

	return pNewTemplateInfo;
}


// Modified by 정운형 2009-02-19 오후 8:31
// 변경내역 :  template 추가 버그 수정
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기존의 template size, color등 기본정보만 복사하여 새로운 template를 생성한다. \n
/// @param (LPCTSTR) lpszSrcTemplateID : (in) 복사하려는 template id
/// @return <형: LPCTSTR> \n
///			새로 생성한 template의 id \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR CDataContainer::CreateCopyTemplate(LPCTSTR lpszSrcTemplateID)
{
	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(lpszSrcTemplateID);
	if(pTemplateLink == NULL) return _T("");

	// USTB 는 같은 크기의 템플릿만 가능함-구현석
	if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
	{
		for(int i=0; i < m_PlayTemplateList.GetCount(); i++)
		{
			TEMPLATE_LINK& PlayTempLink = m_PlayTemplateList.GetAt(i);
			if( PlayTempLink.pTemplateInfo->rcRect.Width()  != pTemplateLink->pTemplateInfo->rcRect.Width()  ||
				PlayTempLink.pTemplateInfo->rcRect.Height() != pTemplateLink->pTemplateInfo->rcRect.Height() )
			{
				UbcMessageBox(LoadStringById(IDS_DATACONTAINER_MSG006), MB_ICONERROR);
				return _T("");
			}
		}
	}

	TEMPLATE_INFO* pNewTemplateInfo = CreateNewTemplate();
	if(pNewTemplateInfo == NULL) return _T("");
	pTemplateLink = GetTemplateLink(lpszSrcTemplateID);
	if(pTemplateLink == NULL) return _T("");

	/////////////////////////////////////////////////////////////////////
	//Template info 멤버 복사(id는 복사하지 않는다)
	CString strNewTemplateId = pNewTemplateInfo->strId;
	*pNewTemplateInfo = *(pTemplateLink->pTemplateInfo);
	pNewTemplateInfo->strId = strNewTemplateId;
	pNewTemplateInfo->strShortCut = _T("");

	/////////////////////////////////////////////////////////////////////
	//Template Link 멤버 복사
	TEMPLATE_LINK* pNewTemplateLink = GetTemplateLink(pNewTemplateInfo->strId);
	if(pNewTemplateLink == NULL) return _T("");

	//Frame Link, Frame Info
	for(int i=0; i<pTemplateLink->arFrameLinkList.GetCount(); i++)
	{
		FRAME_LINK FrameLink = pTemplateLink->arFrameLinkList.GetAt(i);
		FRAME_LINK NewFrameLink;
		NewFrameLink.Copy(FrameLink);

		// 새로생성된 템플릿ID로 설정하고 레이아웃만 생성하므로 복사된 플레이콘텐츠리스트는 삭제한다.
		NewFrameLink.pFrameInfo->strTemplateId = pNewTemplateInfo->strId;
		NewFrameLink.pFrameInfo->DeletePlayContentsList();

		pNewTemplateLink->arFrameLinkList.Add(NewFrameLink);
	}

	pNewTemplateLink->nPlayTimes = pTemplateLink->nPlayTimes;
	pNewTemplateLink->fScale	 = pTemplateLink->fScale;
	for(int i=0; i<5; i++)
	{
		pNewTemplateLink->rcViewRect[i] = pTemplateLink->rcViewRect[i];
	}

	pNewTemplateLink->rcViewRectItem    = pTemplateLink->rcViewRectItem;
	pNewTemplateLink->rcViewRectInfo[0] = pTemplateLink->rcViewRectInfo[0];
	pNewTemplateLink->rcViewRectInfo[1] = pTemplateLink->rcViewRectInfo[1];
	/////////////////////////////////////////////////////////////////////

	//Template Play Link에 추가
	AddPlayTemplate(pNewTemplateInfo->strId, pNewTemplateLink->nPlayTimes);

	return pNewTemplateInfo->strId;
}

// skpark add 2013.
int
CDataContainer::GetChildrenFileList(LPCTSTR lpszContentsId,CStringList& fileList)
{
	//CString debugFile = "C:\\SQISoft\\UTV1.0\\execute\\log\\";
	//debugFile.Append(lpszContentsId);
	//debugFile.Append(".ini");

	char buf[32];
	int file_counter=0;
	int match_counter=0;

	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_ContentsMap.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );


		if(!pContentsInfo) continue;

		// 자기 자신은 포함시켜야 한다.
		if(pContentsInfo->strId != lpszContentsId){
			// 부속파일이 아님것은 제외
			//if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
			// 자식이 아닌것은 제외
			if(pContentsInfo->strParentId != lpszContentsId) continue;
		}

		// lpszContentsId 의 부속파일만을 골라내었다.
		match_counter++;
		// 부속파일의 정확한 패스를 얻어낸다.
		// 확인을 위해 이를 INI 파일에 Write해보자
		CString full_path = pContentsInfo->strServerLocation + pContentsInfo->strFilename;
		fileList.AddTail(full_path);

		//memset(buf,0x00,32);
		//sprintf(buf,"%d",match_counter);
		//WritePrivateProfileString("MATCHED",buf,full_path,debugFile);
	}

	//memset(buf,0x00,32);
	//sprintf(buf,"%d",match_counter);
	//WritePrivateProfileString("MATCHED","counter",buf,debugFile);

	return match_counter;
}

// Modified by 정운형 2009-02-19 오후 8:31
// 변경내역 :  template 추가 버그 수정


bool CDataContainer::DeleteContents(LPCTSTR lpszContentsId)
{
	int nTemplateCount = m_AllTemplateList.GetCount();

	for(int nTemplateIdx=0; nTemplateIdx<nTemplateCount; nTemplateIdx++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(nTemplateIdx);

		CStringArray astrDelPlayContentsList;

		int nFrameCount = template_link.arFrameLinkList.GetCount();
		for(int nFrameIdx=0; nFrameIdx<nFrameCount; nFrameIdx++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(nFrameIdx);

			int nPlayContentsCount = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
			for(int nPlayContentsIdx=nPlayContentsCount-1; nPlayContentsIdx>=0; nPlayContentsIdx--)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(nPlayContentsIdx);
				if(pPlayContentsInfo->strContentsId == lpszContentsId)
				{
					astrDelPlayContentsList.Add(pPlayContentsInfo->strId);
					frame_link.pFrameInfo->DeletePlayContents(pPlayContentsInfo);
				}
				//// 0000611: 플레쉬 콘텐츠 마법사
				//else
				//{
				//	CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
				//	if(pContentsInfo)
				//	{
				//		pContentsInfo->strWizardFiles += ",";
				//		pContentsInfo->strWizardFiles.Replace(lpszContentsId+CString(","),_T(""));
				//		pContentsInfo->strWizardFiles.TrimRight(",");
				//	}
				//}
			}

			nPlayContentsCount = frame_link.pFrameInfo->arTimePlayContentsList.GetCount();
			for(int nPlayContentsIdx=nPlayContentsCount-1; nPlayContentsIdx>=0; nPlayContentsIdx--)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(nPlayContentsIdx);
				if(pPlayContentsInfo->strContentsId == lpszContentsId)
				{
					frame_link.pFrameInfo->DeletePlayContents(pPlayContentsInfo);
				}
				//// 0000611: 플레쉬 콘텐츠 마법사
				//else
				//{
				//	CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
				//	if(pContentsInfo)
				//	{
				//		pContentsInfo->strWizardFiles += ",";
				//		pContentsInfo->strWizardFiles.Replace(lpszContentsId+CString(","),_T(""));
				//		pContentsInfo->strWizardFiles.TrimRight(",");
				//	}
				//}
			}
		}

		// 삭제된 플레이콘텐츠가 부모플레이콘텐츠로 설정된 플레이콘텐츠의 설정을 클리어한다
		for(int nDelPlayContentsIdx=0; nDelPlayContentsIdx<astrDelPlayContentsList.GetSize() ; nDelPlayContentsIdx++)
		{
			for(int nFrameIdx=0; nFrameIdx<nFrameCount; nFrameIdx++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(nFrameIdx);

				int nPlayContentsCount = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
				for(int nPlayContentsIdx=0; nPlayContentsIdx<nPlayContentsCount; nPlayContentsIdx++)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(nPlayContentsIdx);
					if(pPlayContentsInfo->strParentPlayContentsId == astrDelPlayContentsList[nDelPlayContentsIdx])
					{
						pPlayContentsInfo->strParentPlayContentsId.Empty();
					}
				}
			}
		}
	}

	// 부속파일이 있는 경우 부속파일을 모두 지운다
	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_ContentsMap.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != lpszContentsId) continue;

		m_ContentsMap.RemoveKey(strFileKey);
		delete pContentsInfo;
	}

	CONTENTS_INFO* pContentsInfo = NULL;
	if(m_ContentsMap.Lookup( lpszContentsId, (void*&)pContentsInfo ))
	{
		BOOL ret_value = m_ContentsMap.RemoveKey(lpszContentsId);
		delete pContentsInfo;

		DeleteContentsInWizards(lpszContentsId);

		return true;
	}

	return false;
}

bool CDataContainer::DeleteTemplate(LPCTSTR lpszTemplateId)
{
	// delete from play list
	int nCount = m_PlayTemplateList.GetCount();
	for(int i=nCount-1; i>=0; i--)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			m_PlayTemplateList.RemoveAt(i);
			break;
		}
	}

	// delete from all list
	nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			template_link.DeleteFrameList();
			if(template_link.pTemplateInfo) delete template_link.pTemplateInfo;
			template_link.pTemplateInfo = NULL;
			m_AllTemplateList.RemoveAt(i);
			CheckTemplateRect();
			return true;
		}
	}

	CheckTemplateRect();
	return false;
}

bool CDataContainer::DeleteContentsInWizards(CString strDelContentsId)
{
	if(strDelContentsId.IsEmpty()) return true;

	CXmlParser xmlWizardData;

	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo = NULL;
		m_ContentsMap.GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(strContentsId == strDelContentsId) continue;
		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_WIZARD) continue;

		pContentsInfo->strWizardFiles += ",";
		pContentsInfo->strWizardFiles.Replace(strDelContentsId+CString(","),_T(""));
		pContentsInfo->strWizardFiles.TrimRight(",");

		CString strXPath;
		if(!pContentsInfo->strWizardXML.IsEmpty() &&
			xmlWizardData.LoadXML(pContentsInfo->strWizardXML) == XMLPARSER_SUCCESS)
		{
			strXPath.Format("//context [@id=\"%s\"]", strDelContentsId);
			if(xmlWizardData.SetXPath(strXPath) == XMLPARSER_SUCCESS)
			{
				for(int i=0; i<xmlWizardData.GetSelCount() ;i++)
				{
					xmlWizardData.SetAttribute("id", _T(""), i);
				}
			}

			strXPath.Format("//action [@id=\"%s\"]", strDelContentsId);
			if(xmlWizardData.SetXPath(strXPath) == XMLPARSER_SUCCESS)
			{
				for(int i=0; i<xmlWizardData.GetSelCount() ;i++)
				{
					xmlWizardData.SetAttribute("id", _T(""), i);
					xmlWizardData.SetAttribute("name", _T(""), i);
				}
			}

			// xml 저장
			CString strXml = xmlWizardData.GetXML("/");
			strXml.Replace("\t",_T(""));
			strXml.Replace("\r",_T(""));
			strXml.Replace("\n",_T(""));
			pContentsInfo->strWizardXML = strXml;
		}
	}

	return true;
}

void CDataContainer::CheckTemplateRect()
{
	// 존재하지 않는 template Size 삭제-구현석
	POSITION pos = GetEnvPtr()->m_lsTempRect.GetTailPosition();

	while(pos)
	{
		bool bRemove = true;
		POSITION posOld = pos;
		CRect rcTemp = GetEnvPtr()->m_lsTempRect.GetPrev(pos);

		int nCount = m_AllTemplateList.GetCount();
		for(int i=0; i<nCount; i++)
		{
			TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);
			if(!template_link.pTemplateInfo) continue;

			if(template_link.pTemplateInfo->rcRect == rcTemp)
				bRemove = false;
		}

		if(bRemove)
		{
			GetEnvPtr()->m_lsTempRect.RemoveAt(posOld);
		}
	}
}

bool CDataContainer::CheckPrimaryFrame()
{
	CString strMsg;
	return CheckPrimaryFrame(true, strMsg);
}

bool CDataContainer::CheckPrimaryFrame(bool bMsg, CString& strOutMsg)
{
	CString szMsg;
	int nCount = m_PlayTemplateList.GetCount();

	for(int i=0; i<nCount; i++)
	{
		bool bPriFrm = false;
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);
		int nCount2 = template_link.arFrameLinkList.GetCount();

		for(int j=0; j<nCount2; j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			if(1 == frame_link.pFrameInfo->nGrade)
			{
				bPriFrm = true;
				bool bCheck = false;
			
				if(frame_link.pFrameInfo->arCyclePlayContentsList.GetCount() > 0) bCheck = true;
				if(frame_link.pFrameInfo->arTimePlayContentsList.GetCount()  > 0) bCheck = true;
				if(!bCheck)
				{
#ifdef _ML_ENG_
					szMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG001), frame_link.pFrameInfo->strId, template_link.pTemplateInfo->strId);
#else
					szMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG001), template_link.pTemplateInfo->strId, frame_link.pFrameInfo->strId);
#endif
//					if(UbcMessageBox(szMsg, MB_ICONWARNING|MB_YESNO) != IDYES)
//						return false;
//					else
//						continue;
					if(bMsg) UbcMessageBox(szMsg, MB_ICONWARNING);
					else     strOutMsg = szMsg;
					return false;
				}
			}
		}

		if(!bPriFrm)
		{
			szMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG002), template_link.pTemplateInfo->strId);
//			if(UbcMessageBox(szMsg, MB_ICONWARNING|MB_YESNO) != IDYES)
//				return false;
//			else
//				continue;
			if(bMsg) UbcMessageBox(szMsg, MB_ICONWARNING);
			else     strOutMsg = szMsg;
			return false;
		}
	}

	return true;
}

bool CDataContainer::DeleteFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId)
{
	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(lpszTemplateId);
	if(pTemplateLink == NULL) return false;

	// delete from play list
	int nCount = m_PlayTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);
		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			int nCount2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<nCount2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);
				if(frame_link.pFrameInfo->strId == lpszFrameId)
				{
					template_link.arFrameLinkList.RemoveAt(j);
					break;
				}
			}
		}
	}

	// delete from all list
	nCount = pTemplateLink->arFrameLinkList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		FRAME_LINK& frame_link= pTemplateLink->arFrameLinkList.GetAt(i);
		if(frame_link.pFrameInfo->strId == lpszFrameId)
		{
			delete frame_link.pFrameInfo;
			pTemplateLink->arFrameLinkList.RemoveAt(i);
			break;
		}
	}

	return true;
}

bool CDataContainer::DeletePlayTemplate(int nIndex)
{
	if(nIndex < 0 || nIndex > m_PlayTemplateList.GetCount()-1)
		return false;

	m_PlayTemplateList.RemoveAt(nIndex);

	return true;
}

CONTENTS_INFO* CDataContainer::GetContents(LPCTSTR lpszContentsId)
{
	CONTENTS_INFO* pContentsInfo = NULL;
	m_ContentsMap.Lookup( lpszContentsId, (void*&)pContentsInfo );
	return pContentsInfo;
}

TEMPLATE_LINK* CDataContainer::GetTemplateLink(LPCTSTR lpszTemplateId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
			return &(m_AllTemplateList.GetAt(i));
	}
	return NULL;
}

FRAME_LINK* CDataContainer::GetFrameLink(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			int nCount2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<nCount2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				if(frame_link.pFrameInfo->strId == lpszFrameId)
					return &(template_link.arFrameLinkList.GetAt(j));
			}
		}
	}
	return NULL;
}

PLAYCONTENTS_INFO* CDataContainer::GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszPlayContentsId)
{
	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(lpszTemplateId);
	if(!pTemplateLink) return NULL;

	for(int i=0; i<pTemplateLink->arFrameLinkList.GetCount() ;i++)
	{
		FRAME_INFO* pFrameInfo = pTemplateLink->arFrameLinkList[i].pFrameInfo;
		if(!pFrameInfo) return NULL;

		for(int j=0; j<pFrameInfo->arCyclePlayContentsList.GetCount() ;j++)
		{
			if(pFrameInfo->arCyclePlayContentsList[j]->strId == lpszPlayContentsId)
			{
				return pFrameInfo->arCyclePlayContentsList[j];
			}
		}
	}

	return NULL;
}

PLAYCONTENTS_INFO* CDataContainer::GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId, LPCTSTR lpszPlayContentsId)
{
	FRAME_LINK* pFrameLink = GetFrameLink(lpszTemplateId, lpszFrameId);
	if(!pFrameLink) return NULL;

	for(int i=0; i<pFrameLink->pFrameInfo->arCyclePlayContentsList.GetCount() ;i++)
	{
		if(pFrameLink->pFrameInfo->arCyclePlayContentsList[i]->strId == lpszPlayContentsId)
		{
			return pFrameLink->pFrameInfo->arCyclePlayContentsList[i];
		}
	}

	return NULL;
}

bool CDataContainer::MoveFordwardPlayTemplate(int nIndex)
{
	int nCount = m_PlayTemplateList.GetCount();

	if(nIndex <= 0 || nIndex > nCount-1 )
		return false;

	TEMPLATE_LINK target_info; target_info = m_PlayTemplateList.GetAt(nIndex-1);
	TEMPLATE_LINK source_info; source_info = m_PlayTemplateList.GetAt(nIndex);

	m_PlayTemplateList.SetAt(nIndex-1, source_info);
	m_PlayTemplateList.SetAt(nIndex,   target_info);

	return true;
}

bool CDataContainer::MoveBackwardPlayTemplate(int nIndex)
{
	int nCount = m_PlayTemplateList.GetCount();

	if(nIndex < 0 || nIndex >= nCount-1 )
		return false;

	TEMPLATE_LINK target_info; target_info = m_PlayTemplateList.GetAt(nIndex+1);
	TEMPLATE_LINK source_info; source_info = m_PlayTemplateList.GetAt(nIndex);

	m_PlayTemplateList.SetAt(nIndex+1, source_info);
	m_PlayTemplateList.SetAt(nIndex,   target_info);

	return true;
}

bool CDataContainer::IsExistContentsInTemplate(LPCTSTR lpszTemplateId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			int nCount2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<nCount2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				if(frame_link.pFrameInfo->arCyclePlayContentsList.GetCount() > 0) return true;
				if(frame_link.pFrameInfo->arTimePlayContentsList.GetCount()   > 0) return true;
			}
		}
	}

	return false;
}

bool CDataContainer::IsExistContentsInFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId == lpszTemplateId)
		{
			int nCount2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<nCount2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				if(frame_link.pFrameInfo->strId == lpszFrameId)
				{
					if(frame_link.pFrameInfo->arCyclePlayContentsList.GetCount() > 0) return true;
					if(frame_link.pFrameInfo->arTimePlayContentsList.GetCount()   > 0) return true;
				}
			}
		}
	}

	return false;
}

bool CDataContainer::IsUsedShortcut(LPCTSTR szShortcut)
{
	if(!szShortcut) return false;

	CString szTemp = szShortcut;
	if(szTemp.IsEmpty()) return false;

	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strShortCut == szShortcut) return true;
	}

	return false;
}

bool CDataContainer::IsExistPlayTemplateList(LPCTSTR lpszTemplateId)
{
	int nCount = m_PlayTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);
		if(template_link.pTemplateInfo->strId == lpszTemplateId) return true;
	}

	return false;
}

bool CDataContainer::IsExistPlayContentsUsingContents(LPCTSTR lpszContentsId)
{
	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		int nCount2 = template_link.arFrameLinkList.GetCount();
		for(int j=0; j<nCount2; j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			int nCount3 = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
			for(int k=0; k<nCount3; k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);
				if(pPlayContentsInfo->strContentsId == lpszContentsId) return true;

				// 0000611: 플레쉬 콘텐츠 마법사
				CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
				if(pContentsInfo)
				{
					CString strWizardFiles = pContentsInfo->strWizardFiles + ",";
					if(strWizardFiles.Find(lpszContentsId+CString(",")) >= 0) return true;
				}
			}

			nCount3 = frame_link.pFrameInfo->arTimePlayContentsList.GetCount();
			for(int k=0; k<nCount3; k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);
				if(pPlayContentsInfo->strContentsId == lpszContentsId) return true;

				// 0000611: 플레쉬 콘텐츠 마법사
				CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
				if(pContentsInfo)
				{
					CString strWizardFiles = pContentsInfo->strWizardFiles + ",";
					if(strWizardFiles.Find(lpszContentsId+CString(",")) >= 0) return true;
				}
			}
		}
	}

	return false;
}

int CDataContainer::GetContentsUsingCount(LPCTSTR lpszContentsId)
{
	int nUsingCount = 0;

	int nCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		int nCount2 = template_link.arFrameLinkList.GetCount();
		for(int j=0; j<nCount2; j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			int nCount3 = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
			for(int k=0; k<nCount3; k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);
				if(pPlayContentsInfo->strContentsId == lpszContentsId) nUsingCount++;
				// 0000611: 플레쉬 콘텐츠 마법사
				else
				{
					CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
					if(pContentsInfo)
					{
						CString strWizardFiles = pContentsInfo->strWizardFiles + ",";
						if(strWizardFiles.Find(lpszContentsId+CString(",")) >= 0) nUsingCount++;
					}
				}
			}
	
			nCount3 = frame_link.pFrameInfo->arTimePlayContentsList.GetCount();
			for(int k=0; k<nCount3; k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);
				if(pPlayContentsInfo->strContentsId == lpszContentsId) nUsingCount++;
				// 0000611: 플레쉬 콘텐츠 마법사
				else
				{
					CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
					if(pContentsInfo)
					{
						CString strWizardFiles = pContentsInfo->strWizardFiles + ",";
						if(strWizardFiles.Find(lpszContentsId+CString(",")) >= 0) nUsingCount++;
					}
				}
			}
		}
	}

	return nUsingCount;
}

bool CDataContainer::Load(LPCTSTR lpszFullPath)
{
	TraceLog(("Load(%s)",lpszFullPath));

	DeleteAllData();

	UbcIniPtr()->Read(lpszFullPath);
	m_strIniPath = lpszFullPath;  //skpark same_size_file_problem 2014.06.12

	CString strTemplateList    ;
	CString strPlayTemplateList;
	CString strContentsList    ;
	CString strEtcList         ;
	CString strChngContentsList;

	m_strSoundVolume       = UbcIniPtr()->GetProfileString("Host", "soundVolume"     ); 
	m_strDefaultTemplate   = UbcIniPtr()->GetProfileString("Host", "defaultTemplate" );	
	m_strAdminState        = UbcIniPtr()->GetProfileString("Host", "adminState"      );	
	m_strOperationalState  = UbcIniPtr()->GetProfileString("Host", "operationalState");	
	m_strDescription       = UbcIniPtr()->GetProfileString("Host", "description"     );	
	m_strNetworkUse        = UbcIniPtr()->GetProfileString("Host", "networkUse"      );	
	m_strSite              = UbcIniPtr()->GetProfileString("Host", "site"            );	
	m_strLastWrittenUser   = UbcIniPtr()->GetProfileString("Host", "lastUpdateId"    );	
	m_strLastWrittenTime   = UbcIniPtr()->GetProfileString("Host", "lastUpdateTime"  );	
	strTemplateList        = UbcIniPtr()->GetProfileString("Host", "TemplateList"    );	
	strPlayTemplateList    = UbcIniPtr()->GetProfileString("Host", "templatePlayList"); 
	strContentsList        = UbcIniPtr()->GetProfileString("Host", "ContentsList"    ); 
	strEtcList             = UbcIniPtr()->GetProfileString("Host", "EtcList"         ); 
	strChngContentsList    = UbcIniPtr()->GetProfileString("Host", "ChngContentsList"); // 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_nContentsCategory    = UbcIniPtr()->GetProfileInt   ("Host", "contentsCategory"); // 패키지객체 속성추가
	m_nPurpose             = UbcIniPtr()->GetProfileInt   ("Host", "purpose"         ); // ..
	m_nHostType            = UbcIniPtr()->GetProfileInt   ("Host", "hostType"        ); // ..
	m_nVertical            = UbcIniPtr()->GetProfileInt   ("Host", "vertical"        ); // ..
	m_nResolution          = UbcIniPtr()->GetProfileInt   ("Host", "resolution"      ); // ..
	m_bIsPublic            = UbcIniPtr()->GetProfileInt   ("Host", "isPublic"        ); // ..
	m_bIsVerify            = UbcIniPtr()->GetProfileInt   ("Host", "isVerify"        ); // ..
	m_strValidationDate    = UbcIniPtr()->GetProfileString("Host", "validationDate"  ); // ..
	if(m_strValidationDate.GetLength() < 4 || m_strValidationDate.Left(4) <= _T("2010"))
	{
		m_strValidationDate = _T("2037/12/31 23:59:59");
	}

	m_bMonitorOn           = UbcIniPtr()->GetProfileInt   ("Host", "monitorOn"         ); // Monitor On
	m_bMonitorOff          = UbcIniPtr()->GetProfileInt   ("Host", "monitorOff"         ); // Monitor Off

	CUbcGUID::GetInstance()->Clear();

	//strContentsList.Replace(" ", _T(""));
	int nPos = 0;
	CString strTok = strContentsList.Tokenize(",", nPos);
	while(strTok != _T(""))
	{
		strTok.TrimLeft();
		strTok.TrimRight();
		CString strContentsID = strTok;
		strContentsID.Replace("Contents_", _T(""));
		CONTENTS_INFO* pContentsInfo = GetContents(strContentsID);
		if(pContentsInfo == NULL)
		{
			pContentsInfo = new CONTENTS_INFO;
			pContentsInfo->Load(lpszFullPath, strTok);

			CString strErrMsg;
			if(!AddContents(pContentsInfo, strErrMsg))
			{
				CString strTemp;
				strTemp.Format(LoadStringById(IDS_DATACONTAINER_MSG007), strContentsID, strErrMsg);
//				UbcMessageBox(strTemp, MB_ICONWARNING);
			}
		}

		strTok = strContentsList.Tokenize(",", nPos);
	}

	// ETC리스트는 콘텐츠리스트와 ini 파일에 따로 저장되어 있지만
	// 프로그램 내부 처리상 같은 리스트로 관리하는것이 편하여 그렇게 함.
	nPos = 0;
	strTok = strEtcList.Tokenize(",", nPos);
	while(strTok != _T(""))
	{
		strTok.TrimLeft();
		strTok.TrimRight();
		CString strEtcID = strTok;
		strEtcID.Replace("Etc_", _T(""));
		CONTENTS_INFO* pEtcInfo = GetContents(strEtcID);
		if(pEtcInfo == NULL)
		{
			pEtcInfo = new CONTENTS_INFO;
			pEtcInfo->Load(lpszFullPath, strTok);

			CString strErrMsg;
			if(!AddContents(pEtcInfo, strErrMsg))
			{
				CString strTemp;
				strTemp.Format(LoadStringById(IDS_DATACONTAINER_MSG007), strEtcID, strErrMsg);
//				UbcMessageBox(strTemp, MB_ICONWARNING);
			}
		}

		strTok = strEtcList.Tokenize(",", nPos);
	}

	ContentsIdToGUID();

	int nTemplatePos = 0;
	CString strTemplateId = strTemplateList.Tokenize(",", nTemplatePos);
	while(strTemplateId != _T(""))
	{
		TEMPLATE_INFO* pTemplateInfo = new TEMPLATE_INFO;
		pTemplateInfo->Load(lpszFullPath, strTemplateId);

		if(!AddTemplate(pTemplateInfo))
		{
			// 구현석 같은 템플릿 제거. 2009-6-15 같은 템플릿이 있으면 제대로 표현 못하는 버그 있음.
			delete pTemplateInfo;
			pTemplateInfo = NULL;
			strTemplateId = strTemplateList.Tokenize(",", nTemplatePos);
			continue;
		}

		//
		int nFramePos = 0;
		CString strFrameId = pTemplateInfo->strFrameList.Tokenize(",", nFramePos);
		while(strFrameId != _T(""))
		{
			FRAME_INFO* pFrameInfo = CreateNewFrame(pTemplateInfo->strId);
			pFrameInfo->Load(lpszFullPath, strFrameId);

			int nPlayContentsPos = 0;
			CString strPlayContentsId = pFrameInfo->strCyclePlayContentsIdList.Tokenize(",", nPlayContentsPos);
			while(strPlayContentsId != _T(""))
			{
				// 순환플레이콘텐츠상의 __preview_ 키워드가 있으면 무시한다
				//if(strPlayContentsId.Find(PREVIEW_FILE_KEY) < 0)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
					pPlayContentsInfo->Load(lpszFullPath, strPlayContentsId);
					pPlayContentsInfo->bIsDefaultPlayContents = true;

					pPlayContentsInfo->strContentsId = CUbcGUID::GetInstance()->GetGUID(pPlayContentsInfo->strContentsId);

					pFrameInfo->arCyclePlayContentsList.Add(pPlayContentsInfo);
				}

				strPlayContentsId = pFrameInfo->strCyclePlayContentsIdList.Tokenize(",", nPlayContentsPos);
			}

			// 정시플레이콘텐츠 입력은 메인프레임에만 할수 있도록 하므로 로드시에도 메인프레임만 로드한다
			if(pFrameInfo->nGrade != 1) pFrameInfo->strTimeBasePlayContentsIdList = _T("");

			nPlayContentsPos = 0;
			strPlayContentsId = pFrameInfo->strTimeBasePlayContentsIdList.Tokenize(",", nPlayContentsPos);
			while(strPlayContentsId != _T(""))
			{
				// 정시플레이콘텐츠상의 __preview_ 키워드가 있으면 무시한다
				//if(strPlayContentsId.Find(PREVIEW_FILE_KEY) < 0)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
					pPlayContentsInfo->Load(lpszFullPath, strPlayContentsId);
					pPlayContentsInfo->bIsDefaultPlayContents = false;

					pPlayContentsInfo->strContentsId = CUbcGUID::GetInstance()->GetGUID(pPlayContentsInfo->strContentsId);

					pFrameInfo->arTimePlayContentsList.Add(pPlayContentsInfo);
				}

				strPlayContentsId = pFrameInfo->strTimeBasePlayContentsIdList.Tokenize(",", nPlayContentsPos);
			}

			strFrameId = pTemplateInfo->strFrameList.Tokenize(",", nFramePos);
		}

		strTemplateId = strTemplateList.Tokenize(",", nTemplatePos);
	}

	// 강제로 PIP 로 설정등 과 같이 프레임에 강제 설정해야 하는 정보를 이 함수에서 설정한다.
	SetForceInfo();

	nTemplatePos = 0;
	strTemplateId = strPlayTemplateList.Tokenize(",", nTemplatePos);
	while(strTemplateId != _T(""))
	{
		int pos = 0;
		CString tmpl_id = strTemplateId.Tokenize("/", pos);
		CString play_times = strTemplateId.Tokenize("/", pos);

		// Modified by 정운형 2009-02-12 오후 5:58
		// 변경내역 :  Template id 파싱 오류
		tmpl_id.Trim();
		// Modified by 정운형 2009-02-12 오후 5:58
		// 변경내역 :  Template id 파싱 오류
		AddPlayTemplate(tmpl_id, play_times);

		strTemplateId = strPlayTemplateList.Tokenize(",", nTemplatePos);
	}

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	// 변경되는 컨텐츠ID 리스트로 맵객체에 초기화
	InitChangeContentsIdMap(strChngContentsList);

	// Load를 호출하는 부분에서 Load후 데이터를 변경시키므로 호출하는 부분의 Load 함수 마지막에서 호출하도록 함.
	//CreateBackupData();

	return true;
}

bool CDataContainer::Save(LPCTSTR lpszFullPath, BOOL bCreateBackup /*= TRUE*/, CString strPreviewTemplateID/*=_T("")*/)
{
	TraceLog(("Save(%s)",lpszFullPath));

	UbcIniPtr()->ClearInstance();
	UbcIniPtr()->SetFileName(lpszFullPath);

	// host
	UbcIniPtr()->WriteProfileString("Host", "soundVolume"     , m_strSoundVolume     );
	UbcIniPtr()->WriteProfileString("Host", "defaultTemplate" , m_strDefaultTemplate );
	UbcIniPtr()->WriteProfileString("Host", "adminState"      , m_strAdminState      );
	UbcIniPtr()->WriteProfileString("Host", "operationalState", m_strOperationalState);
	UbcIniPtr()->WriteProfileString("Host", "description"     , m_strDescription     );

	if(!strPreviewTemplateID.IsEmpty())
	{
		UbcIniPtr()->WriteProfileString("Host", "defaultTemplate", strPreviewTemplateID);
	}

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		UbcIniPtr()->WriteProfileString("Host", "networkUse", m_strNetworkUse);
		//UbcIniPtr()->WriteProfileString("Host", "site", m_strSite);
	}
	else
	{
		//Studio에서 저장할때는 networkUse는 항상 0, site는 공백으로 저장한다
		UbcIniPtr()->WriteProfileString("Host", "networkUse", "0");
		//UbcIniPtr()->WriteProfileString("Host", "site", _T(""));
	}

	// Enterprise 관련 추가 항목
	UbcIniPtr()->WriteProfileString("Host", "site"            , m_strSite           );
	UbcIniPtr()->WriteProfileString("Host", "lastUpdateId"    , m_strLastWrittenUser);
	UbcIniPtr()->WriteProfileString("Host", "lastUpdateTime"  , m_strLastWrittenTime);
	UbcIniPtr()->WriteProfileInt   ("Host", "contentsCategory", m_nContentsCategory );  // 패키지객체 속성추가
	UbcIniPtr()->WriteProfileInt   ("Host", "purpose"         , m_nPurpose          );  // ..
	UbcIniPtr()->WriteProfileInt   ("Host", "hostType"        , m_nHostType         );  // ..
	UbcIniPtr()->WriteProfileInt   ("Host", "vertical"        , m_nVertical         );  // ..
	UbcIniPtr()->WriteProfileInt   ("Host", "resolution"      , m_nResolution       );  // ..
	UbcIniPtr()->WriteProfileInt   ("Host", "isPublic"        , m_bIsPublic         );  // ..
	UbcIniPtr()->WriteProfileInt   ("Host", "monitorOn"       , m_bMonitorOn         );  // ..
	UbcIniPtr()->WriteProfileInt   ("Host", "monitorOff"      , m_bMonitorOff         );  // ..
	UbcIniPtr()->WriteProfileInt   ("Host", "isVerify"        , 1                   );  // .. (무조건 승인된 패키지로)
	UbcIniPtr()->WriteProfileString("Host", "validationDate"  , m_strValidationDate );  // ..

	UbcIniPtr()->WriteProfileString("Host", "volume"		  , ToString(GetTotalContentsSize()));	// 패키지의 총 용량

	// re-numbering shortcut (골프장전용 2016-09-05)
	if(CEnviroment::eUBGOLF == GetEnvPtr()->m_Customer)
	{
		char shortcut_key = '0';
		int nPlayTemplateCount = m_PlayTemplateList.GetCount();
		for(int i=0; i<nPlayTemplateCount; i++,shortcut_key++)
		{
			TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);

			if(i == nPlayTemplateCount-2) shortcut_key='P';
			if(i == nPlayTemplateCount-1) shortcut_key='R';
			else if(shortcut_key == ':') shortcut_key='A';

			CString str_shortcut;
			str_shortcut.Format("CTRL+%c", shortcut_key);

			template_link.pTemplateInfo->strShortCut = str_shortcut;
		}
	}

	// save template, frame, playcontents
	CString strTemplateIdList;
	int nTemplateCount = m_AllTemplateList.GetCount();
	for(int i=0; i<nTemplateCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(!strPreviewTemplateID.IsEmpty() && strPreviewTemplateID != template_link.pTemplateInfo->strId)
		{
			continue;
		}

		if(strTemplateIdList.GetLength() > 0) strTemplateIdList.Append(",");

		strTemplateIdList.Append("Template" + template_link.pTemplateInfo->strId);

		template_link.pTemplateInfo->Save(lpszFullPath, &(template_link.arFrameLinkList));

		int nFrameCount = template_link.arFrameLinkList.GetCount();
		for(int j=0; j<nFrameCount; j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			frame_link.pFrameInfo->Save(lpszFullPath, &(frame_link.pFrameInfo->arCyclePlayContentsList), &(frame_link.pFrameInfo->arTimePlayContentsList));

			for(int k=0; k<frame_link.pFrameInfo->arCyclePlayContentsList.GetCount(); k++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);
				CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);

				pPlayContentsInfo->Save(lpszFullPath, pContentsInfo);
			}

			// 정시플레이콘텐츠 입력은 메인프레임에만 할수 있도록 하므로 저장시에도 메인프레임만 저장한다
			if(frame_link.pFrameInfo->nGrade == 1)
			{
				for(int k=0; k<frame_link.pFrameInfo->arTimePlayContentsList.GetCount(); k++)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);
					CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);

					pPlayContentsInfo->Save(lpszFullPath, pContentsInfo);
				}
			}
		}
	}

	UbcIniPtr()->WriteProfileString("Host", "TemplateList", strTemplateIdList);

	// playlist
	CString strPlayTemplateIdList = _T("");
	int nPlayTemplateCount = m_PlayTemplateList.GetCount();
	for(int i=0; i<nPlayTemplateCount; i++)
	{
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);

		if(!strPreviewTemplateID.IsEmpty() && strPreviewTemplateID != template_link.pTemplateInfo->strId)
		{
			continue;
		}

		if(strPlayTemplateIdList.GetLength() > 0) strPlayTemplateIdList.Append(",");

		strPlayTemplateIdList.Append(template_link.pTemplateInfo->strId);
		strPlayTemplateIdList.Append("/");
		strPlayTemplateIdList.Append(::ToString(template_link.nPlayTimes));
	}

	UbcIniPtr()->WriteProfileString("Host", "templatePlayList", strPlayTemplateIdList);

	SaveContents(lpszFullPath);

	if(bCreateBackup)
	{
		// 변경사항을 체크하기위해 백업본을 생성한다.
		CreateBackupData();
	}

	UbcIniPtr()->Write(lpszFullPath);

	return true;
}

bool CDataContainer::SaveContents(LPCTSTR lpszFullPath)
{
	// Modified by 정운형 2009-03-09 오후 4:04
	// 변경내역 :  Contents map을 읽어서 Contents list를 config 파일에 저장한다.
	// ETC리스트는 콘텐츠리스트와 ini 파일에 따로 저장되어 있지만
	// 프로그램 내부 처리상 같은 리스트로 관리하는것이 편하여 그렇게 함.
	CString strEtcList;
	CString strContentsList;
	POSITION pos = GetContentsMap()->GetStartPosition();
	while(pos)
	{
		CString strContentsID, strContentsSection;
		CONTENTS_INFO* pContentsInfo = NULL;
		GetContentsMap()->GetNextAssoc(pos, strContentsID, (void*&)pContentsInfo);
		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType == CONTENTS_ETC)
		{
			strContentsSection.Format("Etc_%s", pContentsInfo->strId);
			if(pContentsInfo->Save(lpszFullPath, strContentsSection))
			{
				strEtcList += "Etc_";
				strEtcList += pContentsInfo->strId;
				strEtcList += ",";
			}
		}
		else
		{
			strContentsSection.Format("Contents_%s", pContentsInfo->strId);
			if(pContentsInfo->Save(lpszFullPath, strContentsSection))
			{
				strContentsList += "Contents_";
				strContentsList += pContentsInfo->strId;
				strContentsList += ",";
			}

			// 2011.10.18 플레쉬위저드의 하위 플레쉬컨텐츠에 배경파일이 다운로드가 안되는 문제 해결을 위하여
			//            플레쉬위저드는 무조건 다운로드 되도록 기타리스트에 넣는다.
			if( pContentsInfo->nContentsType == CONTENTS_WIZARD &&
				strEtcList.Find("Etc_" + pContentsInfo->strId + ",") < 0 )
			{
				strContentsSection.Format("Etc_%s", pContentsInfo->strId);
				if(pContentsInfo->Save(lpszFullPath, strContentsSection))
				{
					strEtcList += "Etc_";
					strEtcList += pContentsInfo->strId;
					strEtcList += ",";
				}
			}

			// 0000611: 플레쉬 콘텐츠 마법사
			// 플레쉬위저드 콘텐츠의 경우 관련 콘텐츠가 있는지 확인하여 기타리스트에 추가한다.
			if( pContentsInfo->nContentsType == CONTENTS_WIZARD &&
			   !pContentsInfo->strWizardFiles.IsEmpty()         )
			{
				int nPos = 0;
				CString strTok = pContentsInfo->strWizardFiles.Tokenize(",", nPos);
				while(strTok != _T(""))
				{
					strTok.Trim();
					CString strContentsID = strTok;
					strTok = pContentsInfo->strWizardFiles.Tokenize(",", nPos);

					// 기타리스트에서 정확하게 일치하는 콘텐츠가 있는지 확인
					if(strEtcList.Find("Etc_" + strContentsID + ",") >= 0) continue;

					CONTENTS_INFO* pEtcInfo = GetContents(strContentsID);
					if(!pEtcInfo) continue;

					strContentsSection.Format("Etc_%s", strContentsID);
					if(pEtcInfo->Save(lpszFullPath, strContentsSection))
					{
						strEtcList += "Etc_";
						strEtcList += strContentsID;
						strEtcList += ",";
					}
				}
			}
		}
	}

	//Contents List 저장
	if(strContentsList != _T(""))
	{
		strContentsList.TrimRight(", ");
		UbcIniPtr()->WriteProfileString("Host", "ContentsList", strContentsList);
	}
	// Modified by 정운형 2009-03-09 오후 4:04
	// 변경내역 :  Contents map을 읽어서 Contents list를 config 파일에 저장한다.

	//Etc List 저장
	if(strEtcList != _T(""))
	{
		strEtcList.TrimRight(", ");
		UbcIniPtr()->WriteProfileString("Host", "EtcList", strEtcList);
	}

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	CString strChngContentsList = GetChangeContentsIdList();
	if(!strChngContentsList.IsEmpty())
	{
		UbcIniPtr()->WriteProfileString("Host", "ChngContentsList", strChngContentsList);
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Preview를 위하여 지정된 template config 항목을 저장한다 \n
/// @param (LPCTSTR) lpszFullPath : (in) 저장할 config 파일의 경로
/// @param (CString) strTemplateID : (in) 저장할 template id
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDataContainer::SavePreview(LPCTSTR lpszFullPath, CString strTemplateID)
{
	return Save(lpszFullPath, FALSE, strTemplateID);
}

// 강제로 PIP 로 설정등 과 같이 프레임에 강제 설정해야 하는 정보를 이 함수에서 설정한다.
void CDataContainer::SetForceInfo()
{
	for(int i=0; i<m_AllTemplateList.GetCount() ;i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);
		template_link.SetForceFrameInfo();
	}
}

void CDataContainer::DeleteAllBackupData()
{
	POSITION pos = m_ContentsMapOld.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo = NULL;
		m_ContentsMapOld.GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );
		if(pContentsInfo) delete pContentsInfo;
	}
	m_ContentsMapOld.RemoveAll();

	int nCount = m_AllTemplateListOld.GetCount();
	for(int i=0; i<nCount; i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateListOld.GetAt(i);

		template_link.DeleteFrameList();
		delete template_link.pTemplateInfo;
	}
	m_AllTemplateListOld.RemoveAll();
	m_PlayTemplateListOld.RemoveAll();
}

void CDataContainer::CreateBackupData()
{
	DeleteAllBackupData();

	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pSrcContentsInfo = NULL;
		m_ContentsMap.GetNextAssoc( pos, strContentsId, (void*&)pSrcContentsInfo );

		if(pSrcContentsInfo)
		{
			CONTENTS_INFO* pContentsInfo = new CONTENTS_INFO;
			*pContentsInfo = *pSrcContentsInfo;
			m_ContentsMapOld[strContentsId] = (void*&)pContentsInfo;
		}
	}

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& SrcTemplateLink = m_AllTemplateList.GetAt(i);
		TEMPLATE_LINK BackTemplateLink;
		BackTemplateLink.Copy(SrcTemplateLink);
		m_AllTemplateListOld.Add(BackTemplateLink);
	}

	for(int i=0; i<m_PlayTemplateList.GetCount(); i++)
	{
		m_PlayTemplateListOld.Add(m_PlayTemplateList.GetAt(i));
	}
}

bool CDataContainer::IsModified()
{
	if(m_ContentsMap.GetCount() != m_ContentsMapOld.GetCount()) return true;

	POSITION pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfoNew;
		m_ContentsMap.GetNextAssoc( pos, strContentsId, (void*&)pContentsInfoNew );

		CONTENTS_INFO* pContentsInfoOld = NULL;
		if(!m_ContentsMapOld.Lookup(strContentsId, (void*&)pContentsInfoOld)) return true;

		if(!pContentsInfoNew && !pContentsInfoOld) continue;

		if(!pContentsInfoNew) return true;
		if(!pContentsInfoOld) return true;

		if( !(*pContentsInfoNew == *pContentsInfoOld) ) return true;
	}

	if(m_AllTemplateList.GetCount() != m_AllTemplateListOld.GetCount()) return true;
	if(m_PlayTemplateList.GetCount() != m_PlayTemplateListOld.GetCount()) return true;

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		if( !(m_AllTemplateListOld.GetAt(i) == m_AllTemplateList.GetAt(i)) ) return true;
	}

	for(int i=0; i<m_PlayTemplateList.GetCount(); i++)
	{
		if( !(m_PlayTemplateListOld.GetAt(i) == m_PlayTemplateList.GetAt(i)) ) return true;
	}

	return false;
}

// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
bool CDataContainer::CheckOverlayModeFrame(CString strTemplateId, CString strOverlayModeFrameId, bool bMsg/*=true*/)
{
	if(strTemplateId.IsEmpty()) strOverlayModeFrameId.Empty();

	for(int i=0; i<m_AllTemplateList.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link = m_AllTemplateList.GetAt(i);

		if(template_link.pTemplateInfo->strId != strTemplateId && !strTemplateId.IsEmpty()) continue;

		// 같은 레이아웃의 다른 프레임에 고화질 동영상 전용으로 설정되어 있는지 체크
		bool bOverlayModeFrame = !strOverlayModeFrameId.IsEmpty();
		for(int j=0; j<template_link.arFrameLinkList.GetCount() ;j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			if( frame_link.pFrameInfo->strId != strOverlayModeFrameId &&
				frame_link.pFrameInfo->strComment[0] == _T("9")       )
			{
				if(bOverlayModeFrame)
				{
					CString strMsg;
					strMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG008), template_link.pTemplateInfo->strId);
					if(bMsg) UbcMessageBox(strMsg);
					return false;
				}

				bOverlayModeFrame = true;
			}
		}

		// 고화질 동영상 전용으로 설정된 프레임이 없다면 다른 프레임에 동영상 플레이콘텐츠가 설정되었는지 체크할 필요 없음.
		if(!bOverlayModeFrame) continue;

		// 같은 레이아웃의 다른 프레임에 동영상 플레이콘텐츠가 설정되어 있는지 체크
		for(int j=0; j<template_link.arFrameLinkList.GetCount() ;j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			if( frame_link.pFrameInfo->strId != strOverlayModeFrameId &&
			    frame_link.pFrameInfo->strComment[0] != _T("9")       )
			{
				for(int k=0; k<frame_link.pFrameInfo->arCyclePlayContentsList.GetCount() ;k++)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);

					if(pPlayContentsInfo)
					{
						CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
						if(pContentsInfo && pContentsInfo->nContentsType == CONTENTS_VIDEO)
						{
							CString strMsg;
							if(strOverlayModeFrameId.IsEmpty())
							{
								strMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG009)
											, template_link.pTemplateInfo->strId
											, frame_link.pFrameInfo->strId
											);
							}
							else
							{
								strMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG010)
											, template_link.pTemplateInfo->strId
											, frame_link.pFrameInfo->strId
											);
							}
							if(bMsg) UbcMessageBox(strMsg);
							return false;
						}
					}
				}

				for(int k=0; k<frame_link.pFrameInfo->arTimePlayContentsList.GetCount() ;k++)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);

					if(pPlayContentsInfo)
					{
						CONTENTS_INFO* pContentsInfo = GetContents(pPlayContentsInfo->strContentsId);
						if(pContentsInfo && pContentsInfo->nContentsType == CONTENTS_VIDEO)
						{
							CString strMsg;
							if(strOverlayModeFrameId.IsEmpty())
							{
								strMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG009)
											, template_link.pTemplateInfo->strId
											, frame_link.pFrameInfo->strId
											);
							}
							else
							{
								strMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG010)
											, template_link.pTemplateInfo->strId
											, frame_link.pFrameInfo->strId
											);
							}
							if(bMsg) UbcMessageBox(strMsg);
							return false;
						}
					}
				}
			}
		}
	}

	return true;
}

// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
bool CDataContainer::CheckContentsCanBeAddedToFrame(CString strTemplateId, CString strFrameId, CString strContentsId)
{
	CONTENTS_INFO* pContents = GetContents(strContentsId);
	if(!pContents) return true;

	TEMPLATE_LINK* pTemplateLink = GetTemplateLink(strTemplateId);
	if(!pTemplateLink) return true;

	if(pContents->nContentsType == CONTENTS_ETC)
	{
		return false;
	}

	// 창일향 : 메인 프레임이 선택되었을 때는 콘텐츠 패키지를 추가도 삭제도,  편집도 할 수 없다.  메인 프레임외에 다른 프레임들만 선택하고 플레이콘텐츠 추가, 삭제, 편집이 가능하다.
	if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer )
	{
		FRAME_LINK* pFrameLink = GetFrameLink(strTemplateId, strFrameId);
		if(pFrameLink && pFrameLink->pFrameInfo->nGrade == 1)
		{
			return false;
		}
	}

	if(pContents->nContentsType != CONTENTS_VIDEO)
	{
		return true;
	}

	// 같은 레이아웃의 다른 프레임에 고화질 동영상 전용으로 설정되어 있는지 체크
	CString strOverlayModeFrameId;
	for(int j=0; j<pTemplateLink->arFrameLinkList.GetCount() ;j++)
	{
		FRAME_LINK& frame_link = pTemplateLink->arFrameLinkList.GetAt(j);

		if( frame_link.pFrameInfo->strComment[0] == _T("9") )
		{
			strOverlayModeFrameId = frame_link.pFrameInfo->strId;
			break;
		}
	}

	// 고화질 동영상 전용으로 설정된 프레임이 없다면 다른 프레임에 동영상 플레이콘텐츠가 설정되었는지 체크할 필요 없음.
	if(strOverlayModeFrameId.IsEmpty()) return true;

	// 비디오 콘텐츠를 넣으려는 프레임이 고화질 동영상 전용 프레임인경우 true 리턴
	return (strFrameId == strOverlayModeFrameId);
}

// 0000891: 어떤 프레임에 자식 플레이콘텐츠만 있고, 자식 플레이콘텐츠외에 아무런 다른 플레이콘텐츠가 없다면 저장시 경고해주어야함.
bool CDataContainer::CheckPlayContentsRelation()
{
	for(int i=0; i<m_PlayTemplateList.GetCount(); i++)
	{
		bool bPriFrm = false;
		TEMPLATE_LINK& template_link = m_PlayTemplateList.GetAt(i);

		for(int j=0; j<template_link.arFrameLinkList.GetCount(); j++)
		{
			FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

			int nPlayContentsCount = frame_link.pFrameInfo->arCyclePlayContentsList.GetCount();
			if(nPlayContentsCount == 0) continue;

			int nParentPlayContentsCount = 0;
			for(int nPlayContentsIdx=0; nPlayContentsIdx<nPlayContentsCount; nPlayContentsIdx++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(nPlayContentsIdx);
				if(pPlayContentsInfo && !pPlayContentsInfo->strParentPlayContentsId.IsEmpty())
				{
					nParentPlayContentsCount++;
				}
			}

			// 자식 플레이콘텐츠외에 아무런 다른 플레이콘텐츠가 없다면
			if(nParentPlayContentsCount == nPlayContentsCount)
			{
				CString szMsg;
#ifdef _ML_ENG_
				szMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG003), frame_link.pFrameInfo->strId, template_link.pTemplateInfo->strId);
#else
				szMsg.Format(LoadStringById(IDS_DATACONTAINER_MSG003), template_link.pTemplateInfo->strId, frame_link.pFrameInfo->strId);
#endif
				UbcMessageBox(szMsg, MB_ICONWARNING);
				return false;
			}
		}
	}

	return true;
}

bool CDataContainer::ContentsIdToGUID()
{
	POSITION pos = GetContentsMap()->GetStartPosition();
	while(pos)
	{
		CString strContentsID, strContentsSection;
		CONTENTS_INFO* pContentsInfo = NULL;
		GetContentsMap()->GetNextAssoc(pos, strContentsID, (void*&)pContentsInfo);
		if(!pContentsInfo) continue;
		if(pContentsInfo->strWizardXML.IsEmpty()) continue;

		POSITION posGuid = CUbcGUID::GetInstance()->GetMapTable()->GetStartPosition();
		while(posGuid)
		{
			CString strOldContentsID, strNewContentsID;
			CUbcGUID::GetInstance()->GetMapTable()->GetNextAssoc(posGuid, strOldContentsID, strNewContentsID);

			if(strOldContentsID.IsEmpty() || strNewContentsID.IsEmpty()) continue;

			CString strOld;
			strOld.Format(_T("id=\"%s\""), strOldContentsID);
			CString strNew;
			strNew.Format(_T("id=\"%s\""), strNewContentsID);

			pContentsInfo->strWizardXML.Replace(strOld, strNew);
		}
	}

	return true;
}

// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
// 변경되는 컨텐츠ID 리스트로 맵객체에 초기화
void CDataContainer::InitChangeContentsIdMap(CString strChngContentsList)
{
	m_ChangeContentsIdMap.RemoveAll();

	int nPos = 0;
	CString strChangeId;
	while( !(strChangeId = strChngContentsList.Tokenize(",", nPos)).IsEmpty() )
	{
		int nTmp = 0;
		CString strSrcId = strChangeId.Tokenize(_T(":"), nTmp);
		CString strDstId = strChangeId.Tokenize(_T(":"), nTmp);

		if(strSrcId.IsEmpty() || strDstId.IsEmpty()) continue;
		if(strSrcId.CompareNoCase(strDstId) == 0) continue;

		m_ChangeContentsIdMap[strSrcId] = strDstId;
	}
}

// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
// 변경되는 컨텐츠ID정보를 설정한다
void CDataContainer::ChangeContentsId(CString strSrcId, CString strDstId)
{
	TraceLog(("ChangeContentsId(%s --> %s)", strSrcId, strDstId));
	m_ChangeContentsIdMap[strSrcId] = strDstId;
	//skpark same_file_size_problem key 값이 변경되면 m_ModifiedTimeMap 의 값도 변경해야 한다.
	CString srcKey = "Contents_" + strSrcId;
	CString dstKey = "Contents_" + strDstId;
	CHANGE_INFO* aInfo=0;
	if(m_ModifiedTimeMap.Lookup(srcKey, (void*&)aInfo)){
		TraceLog(("Change m_ModifiedTimeMap Id(%s --> %s)", srcKey, dstKey));
		m_ModifiedTimeMap[dstKey] = aInfo;
		m_ModifiedTimeMap.RemoveKey(srcKey);
	}
}

// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
// 변경되는 컨텐츠ID의 리스트를 문자열로 리턴한다
CString CDataContainer::GetChangeContentsIdList()
{
	CString strChngContentsList;

	POSITION pos = m_ChangeContentsIdMap.GetStartPosition();
	while(pos)
	{
		CString strSrcId, strDstId;
		m_ChangeContentsIdMap.GetNextAssoc(pos, strSrcId, strDstId);

		if(strSrcId.IsEmpty() || strDstId.IsEmpty()) continue;

		strChngContentsList += strSrcId + _T(":") + strDstId + _T(",");
	}

	strChngContentsList.Trim(_T(","));

	return strChngContentsList;
}

ULONGLONG CDataContainer::GetTotalContentsSize()
{
	ULONGLONG lTotalContentsSize = 0;

	CString strContentsID;
	CONTENTS_INFO* pContentsInfo;
	CONTENTS_INFO_MAP* pContentsMap = GetContentsMap();
	POSITION pos = pContentsMap->GetStartPosition();
	while(pos)
	{
		pContentsMap->GetNextAssoc(pos, strContentsID, (void*&)pContentsInfo);
		
		if(!pContentsInfo || !pContentsInfo->nFilesize || pContentsInfo->strFilename.IsEmpty())
		{
			continue;
		}

		lTotalContentsSize += pContentsInfo->nFilesize;
	}

	return lTotalContentsSize;
}

//
// skpark same_size_file_problem  CFtpMultiSite::IFtpCallback 인터페이스
//
bool	CDataContainer::isModified(const char* contentsId)
{
	// skpark same_size_file_problem 해당 contents 가 modified 된것인지를 식별한다.
	// 이 함수는 오직 file_size 가 같은 경우에만 호출해야 한다.!!!!!
	if(!m_bUseTimeCheck) return false;

	CString key;
	key.Format("Contents_%s", contentsId);

	TraceLog(("isModified(%s)", key));

	CHANGE_INFO* aInfo=0;
	m_ModifiedTimeMap.Lookup(key, (void*&)aInfo);

	if(aInfo==0) return false;
	
	TraceLog(("Yes It's modified(%s)", key));
	// 이함수는 오직 파일 사이즈가 같은 경우에만 호출해야 한다.
	aInfo->bIsSame = true;
	return true;
}

bool	CDataContainer::ChangeFileTime(const char* contentsId)
{ 
	if(!m_bUseTimeCheck) return false;

	// skpark same_size_file_problem 해당 contents 의 file 시간을 LastModifiedTime 과 일치시킨다.
	CString key;
	key.Format("Contents_%s", contentsId);

	TraceLog(("ChangeFileTime(%s)", key));

	CHANGE_INFO* aInfo=0;
	m_ModifiedTimeMap.Lookup(key, (void*&)aInfo);

	if(aInfo==0) {
		TraceLog(("%s is  Not founded in m_ModifiedTimeMap", key));
		return false;
	}

	if(aInfo->bIsSame == false) {
		// 파일 사이즈가 같은 경우에만 파일타임을 변경한다.	
		return false;
	}

	CString target = aInfo->fullpath;
	CString modifiedTime = aInfo->modifiedTime;

	TraceLog(("ChangeFileTime target = %s", target));
	TraceLog(("ChangeFileTime modifiedTime = %s", modifiedTime));

	HANDLE hFile = ::CreateFileA(target, 
								GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile==INVALID_HANDLE_VALUE) {
		TraceLog(("%s file open error", target));
		return false;		
	}
	FILETIME ft;
	if(TimeStringToFileTime(modifiedTime,&ft)){
		if(::SetFileTime(hFile,&ft,&ft,&ft)){
			TraceLog(("%s file set file time succeed", target));
			CloseHandle(hFile);
			return true;
		}else{
			TraceLog(("%s file set file time failed (ErrorCode=%d)",target, ::GetLastError()));
		
		}
	}
	CloseHandle(hFile);
	TraceLog(("%s file set file time failed", target));
	return false;
}


bool	CDataContainer::ChangeModifiedFlag(bool flag)
{
	// skpark same_size_file_problem 해당 ini 의 modified 값을 모두 변경하고, ini 에 write 한다.
	if(!m_bUseTimeCheck) return false;


	TraceLog(("ChangeModifiedFlag"));

	int counter=0;
	POSITION  pos =  m_ModifiedTimeMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CHANGE_INFO* aInfo=0;
		m_ModifiedTimeMap.GetNextAssoc( pos, strContentsId, (void*&)aInfo );

		if(aInfo==0) continue;
	
		if(!m_strIniPath.IsEmpty()){
			TraceLog(("%s ChangeModifiedFlag[%s]=0", m_strIniPath, strContentsId));
			if(!WritePrivateProfileString(strContentsId,"modified","0",m_strIniPath)){
				TraceLog(("ERROR %s ChangeModifiedFlag[%s]=0 failed", m_strIniPath, strContentsId));
			}else{
				TraceLog(("%s ChangeModifiedFlag[%s]=0 succeed", m_strIniPath, strContentsId));
			}
		}
		UbcIniPtr()->WriteProfileInt(strContentsId, "Modified", 0);
		if(!aInfo->bIsSame){  // 파일사이즈가 같지 않으면, modifiedTime 도 지운다. 
			if(!WritePrivateProfileString(strContentsId,"LastModifiedTime","",m_strIniPath)){
				TraceLog(("ERROR %s ChangeModifiedTime[%s]=0 failed", m_strIniPath, strContentsId));
			}else{
				TraceLog(("%s ChangeModifiedTime[%s]=0 succeed", m_strIniPath, strContentsId));
			}
			UbcIniPtr()->WriteProfileString(strContentsId, "LastModifiedTime","");
		}
		if(aInfo) delete aInfo;
		counter++;
	}
	m_ModifiedTimeMap.RemoveAll();

	pos = m_ContentsMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		m_ContentsMap.GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );
		if(pContentsInfo && pContentsInfo->GetModified()) {
			pContentsInfo->UnSetModified();
			TraceLog(("skpark %s file reset !!!" , pContentsInfo->strFilename));
		}
	}

	return true;
}

