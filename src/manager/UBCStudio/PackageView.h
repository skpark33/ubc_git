// PackageView.h : interface of the CPackageView class
//


#pragma once

#include "afxcmn.h"
#include "afxwin.h"

#include "CheckListCtrl.h"
#include "ReposControl.h"
#include "OleListDropTarget.h"
//#include "XPTabCtrl.h"

#include "ContentsListCtrl.h"
#include "Template_Wnd.h"
#include "Frame_Wnd.h"

#include "common/HoverButton.h"
#include "common/UBCTabCtrl.h"
#include "common/CheckComboBox.h"
#include "common/UTBListCtrlEx.h"
#include "common/hoverbutton.h"

class CPackageView : public CFormView
{
protected: // create from serialization only
	CPackageView();
	DECLARE_DYNCREATE(CPackageView)

public:
	enum{ IDD = IDD_PACKAGE_VIEW };

// Attributes
public:
	CUBCStudioDoc* GetDocument() const;

// Operations
public:
	CStatic		m_stTotalSize;
	void		UpdateTotalSize();

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CPackageView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	void UpdateAllInfo();
	bool DownloadSelectedContents();

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	CReposControl		m_reposControl;
	CImageList			m_ilContentsList16;
	CImageList*			m_ilContentDrag;
	CImageList*			m_ilPlayContentsDrag;
	COleListDropTarget	m_OleDrop;
	CImageList			m_ilFrameColorList;

	bool				RegisterDND();
	bool				UnregisterDND();

public:
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLvnBegindragListContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnNMDblclkListContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownListContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonAddContents();
	afx_msg void OnBnClickedButtonRemoveContents();
	afx_msg void OnBnClickedButtonModifyContents();
	afx_msg void OnBnClickedButtonDownloadContents();
	afx_msg void OnBnClickedButtonAddTemplate();
	afx_msg void OnBnClickedButtonRemoveTemplate();
	afx_msg void OnBnClickedButtonMoveUpTemplate();
	afx_msg void OnBnClickedButtonMoveDownTemplate();
	afx_msg void OnBnClickedButtonPreview();
	afx_msg void OnBnClickedButtonMovePrevFrame();
	afx_msg void OnBnClickedButtonMoveNextFrame();
	afx_msg void OnTcnSelchangeTabPlayContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownListCyclePlayContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListCyclePlayContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownListTimeBasePlayContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListTimeBasePlayContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonAddPlayContents();
	afx_msg void OnBnClickedButtonRemovePlayContents();
	afx_msg void OnBnClickedButtonModifyPlayContents();
	afx_msg	void OnBnClickedButtonResizePlayContents();  //skpark
	afx_msg	void OnBnClickedButtonToExcel();  //skpark
	afx_msg void OnBnClickedButtonMoveUpPlayContents();
	afx_msg void OnBnClickedButtonMoveDownPlayContents();
	afx_msg LRESULT OnAddContents(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnClearContentsSelection(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFrameInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnContentsListChanged(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT	OnConfigSaveComplete(WPARAM wParam, LPARAM lParam);	///<complete config save message handler
	afx_msg LRESULT	OnContentsFilter(WPARAM wParam, LPARAM lParam);

	enum { eDateTime = 0, eTimeZone
		 , ePeriod = 0, eTime 
		 , eContentsType, eContentsName, eRunningTime, eFileName, eSoundVolume
		 , eParentPlayContents
		 , ePlayContentsEnd };

	CCheckComboBox		m_cbContentsFilter;
	CContentsListCtrl	m_lcContents;
	CTemplate_Wnd		m_wndTemplate;
	CFrame_Wnd			m_wndFrame;

	CStatic				m_frameTemplate;
	CStatic				m_frameFrame;
//	CXPTabCtrl			m_tabPlayContents;
	CUBCTabCtrl			m_tabPlayContents;
//	CStatic				m_framePlayContents;
	CUTBListCtrlEx		m_lcCyclePlayContents;
	CUTBListCtrlEx		m_lcTimeBasePlayContents;

	PLAYCONTENTS_INFO*	m_selectedContents;
	PLAYCONTENTS_INFO_LIST	m_selectedContentsList;
	// Modified by ����?E2009-01-21 ����E11:21
	// ���泻�� :  �̹���E�߰� �۾�E
	/*
	CButton		m_btnAddContents;
	CButton		m_btnRemoveContents;
	CButton		m_btnModifyContents;
	
	CButton		m_btnAddTemplate;
	CButton		m_btnRemoveTemplate;
	CButton		m_btnMoveUpTemplate;
	CButton		m_btnMoveDownTemplate;
	
	CButton		m_btnPreview;
	CButton		m_btnMovePrevFrame;
	CButton		m_btnMoveNextFrame;

	CButton		m_btnAddPlayContents;
	CButton		m_btnRemovePlayContents;
	CButton		m_btnModifyPlayContents;
	CButton		m_btnMoveUpPlayContents;
	CButton		m_btnMoveDownPlayContents;
	*/
	CHoverButton	m_btnAddContents;
	CHoverButton	m_btnRemoveContents;
	CHoverButton	m_btnModifyContents;
	CHoverButton	m_btnDownloadContents;
	CHoverButton	m_btnContentsManager;
	CHoverButton	m_btnWizardTree;

	CHoverButton	m_btnAddTemplate;
	CHoverButton	m_btnRemoveTemplate;
	CHoverButton	m_btnMoveUpTemplate;
	CHoverButton	m_btnMoveDownTemplate;
	CHoverButton	m_btnCopyTemplate;
	
	CHoverButton	m_btnPreview;
	CHoverButton	m_btnMovePrevFrame;
	CHoverButton	m_btnMoveNextFrame;

	CHoverButton	m_btnAddPlayContents;
	CHoverButton	m_btnRemovePlayContents;
	CHoverButton	m_btnModifyPlayContents;
	CHoverButton	m_btnMoveUpPlayContents;
	CHoverButton	m_btnMoveDownPlayContents;
	
	CHoverButton	m_btnRemoveRelation;

	CTabCtrl		m_tabRelation;
	CTreeCtrl		m_treeRelation;
	HTREEITEM		m_hSelectItem;

	CHoverButton	m_btnSizeToggle;  //skpark
	CHoverButton	m_btnHSizeToggle; //skpark 215.12.21

	int				m_toggle; //skpark
	int				m_h_toggle; //skpark
	CPoint			m_dragStartPoint; //skpark

	BOOL		GetVideoInfo(CString sPathVideo, SFile& file);

	CUTBListCtrlEx*	GetCurrentPlayContentsListCtrl();
	bool		InsertPlayContents(PLAYCONTENTS_INFO* info, int nMode, bool bSelection=false);		// 0 = Add List & Map, 1 = Only Add List, 2 = Modify
	bool		RefreshPlayContentsList();
	bool		DeletePlayContents(POSITION pos);
	void		ClearPlayContentsSelection();
	void		InitTabPlayContents();
	void		ToggleTabPlayContents(int nCurSel);

	int			GetPrevNoSelectPlayContentsIndex(CUTBListCtrlEx* pListCtrl, int nIndex);
	int			GetNextNoSelectPlayContentsIndex(CUTBListCtrlEx* pListCtrl, int nIndex);

	void		InitFrameColorList();
	void		RefreshRelation();
	bool		IsFitInsertRelation(PLAYCONTENTS_INFO* pParentInfo, PLAYCONTENTS_INFO* pInsertInfo);

	void		ShowItems(int shows); //skpark
	void		InvalidateItems(); //skpark

#ifndef _UBCSTUDIO_PE_
	void		InputAdvertiserName() ; //skpark UBGOLF
#endif

	// Modified by ����?E2009-01-06 ����E11:35
	// ���泻�� :  CLI ���� ����
	void		SortDefaultPlayContentsByPlayOrder();						///<default playcontents�� playorder�� ������E?list control�� ������ sort�Ѵ�
	// Modified by ����?E2009-01-06 ����E11:35
	// ���泻�� :  CLI ���� ����
	afx_msg void OnBnClickedButtonCopyTemplate();
	afx_msg void OnLvnBegindragListCyclePlayContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTvnKeydownTreeRelation(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonRemoveRelation();
	afx_msg void OnNMClickListCyclePlayContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickTreeRelation(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonContentsManager();
	afx_msg void OnBnClickedButtonWizardTree();
	afx_msg void OnNMRclickTreeRelation(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnGotoTemplate();
	afx_msg void OnNMRclickListTimeBasePlayContents(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTimebaserclickIteration();
	afx_msg void OnCirclebaserclickCopy();
	afx_msg void OnNMRclickListCyclePlaycontents(NMHDR *pNMHDR, LRESULT *pResult);
	CHoverButton m_btnCondition;
	afx_msg void OnBnClickedButtonCondition();
	afx_msg void OnBnClickedButtonHSizeToggle();

	CRect m_rcCon;
	CRect m_rcHToggle;
	CRect m_rcToggle;
	CRect m_rcTemplate;
	CRect m_rcFrame;
	CRect m_rcWndTemplate;
	CRect m_rcWndFrame;
	CRect m_rcTab;
	CRect m_rcList;
	CRect m_rcList2;

	void InitHSizeToggle();
	void HSizeToggle(int toggle);

	CEdit m_editNameFilter;
	CHoverButton m_btNameFilter;

	afx_msg void OnBnClickedBtNameFilter();
	CStatic m_staticNameFilter;

	int		m_nPrevDropIndex;
	void	ReorderListCtrl(CUTBListCtrlEx& lc);
};

#ifndef _DEBUG  // debug version in PackageView.cpp
inline CUBCStudioDoc* CPackageView::GetDocument() const
   { return reinterpret_cast<CUBCStudioDoc*>(m_pDocument); }
#endif

