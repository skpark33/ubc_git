#pragma once
#include "afxwin.h"

#include "SubContentsDialog.h"

#include "EditEx.h"

#include "ReposControl.h"

#include "Frame.h"
#include "Schedule.h"

#include "common/HoverButton.h"
#include "common/utblistctrlex.h"
#include "afxcmn.h"


// CFlashImagePlayerContentsDialog 대화 상자입니다.

class CFlashImageplayerEffectsOderChangeDlg;

class CFlashImagePlayerContentsDialog : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CFlashImagePlayerContentsDialog)

public:
	CFlashImagePlayerContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFlashImagePlayerContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FLASH_IMAGEPLAYER_CONTENTS };

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CBrush	m_brushBG;
	CFlashSchedule	m_wndFlash;
	CReposControl	m_reposControl;
	bool			m_bPreviewMode;		///<미리보기 모드
	BOOL			m_bPermanent;		///<24시간 running time

	bool			LoadContents(LPCTSTR lpszFullPath);

public:

	virtual CONTENTS_TYPE	GetContentsType() { return CONTENTS_FLASH; };
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnBnClickedButtonBrowserContentsFile();

	CEdit		m_editContentsName;
	CEdit		m_editContentsFolderName;
	CEdit		m_editFelicaUrl;
	CHoverButton	m_btnBrowseContentsFile;
	CEditEx		m_editContentsPlayMinute;
	CEditEx		m_editContentsPlaySecond;
	CEditEx		m_editContentsPlayInterval;

	CStatic		m_groupPreview;
	CStatic		m_staticContents;

	CString		m_strLocation;
	CString		m_strSourceLocation;

	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
	void	EnableAllControls(BOOL bEnable);			///<컨트롤의 사용가능여부 설정

	// 부속파일처리 관련 수정
	CONTENTS_INFO_MAP	m_mapSubFilesContents;
	void			CreateSubFilesContents();
	void			DeleteSubFilesContents();

	CString			m_strContentsId;
	CImageList		m_ilSystemImg;
	CUTBListCtrlEx	m_lcFiles;
	CHoverButton	m_btnBrowseAddFile  ;
	CHoverButton	m_btnBrowseDelFile  ;
	CHoverButton	m_btnDownloadContents;
	CHoverButton	m_btnMoveUp;
	CHoverButton	m_btnMoveDown;

	void			InitSubFilesCtrl();
	//void			RefreshFilesTree();
	void			RefreshFilesList(CString strLocation);
	void			UpdateFilesListRow(int nRow, CONTENTS_INFO* pContentsInfo);
	//void			AddSubFolder(CString strChildPath, CString szFolderPath);
	CONTENTS_INFO*	AddSubFileContents(CString strChildPath, CString strFilepath, BOOL& bExist);
	//void			SelectTreeItem(CString strChildPath);
	//CString			GetTreeFullPath(HTREEITEM hItem);
	BOOL			DownloadSelectedContents();

	static int CALLBACK CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnBnClickedPermanentCheck();
	//afx_msg void OnTvnSelchangedTreeFiles(NMHDR *pNMHDR, LRESULT *pResult);
	//afx_msg void OnBnClickedButtonBrowserAddFolder();
	//afx_msg void OnBnClickedButtonBrowserDelFolder();
	afx_msg void OnBnClickedButtonBrowserAddFile();
	afx_msg void OnBnClickedButtonBrowserDelFile();
	//afx_msg void OnBnClickedButtonDownloadFolder();
	afx_msg void OnBnClickedButtonDownloadContents();
	afx_msg void OnNMDblclkListFiles(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownListFiles(NMHDR *pNMHDR, LRESULT *pResult);
	//afx_msg void OnTvnKeydownTreeFiles(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonMoveUp();
	afx_msg void OnBnClickedButtonMoveDown();

protected:
	CString		m_strDataXML;

	BOOL		CopyFlashImagePlayerFile(); // imagePlayer.swf -> imagePlayer_{GUID}.swf
	CString		GetDataXML(LPCSTR lpszNewLineDelimiter=NULL);
	BOOL		UpdateDataXML(bool bReplaceXML=true);
	void		ReorderFileListFromDataXML(CString strXml);


	CFlashImageplayerEffectsOderChangeDlg*	m_pDlg;
	CSize			m_sizeDlg;
	CStringArray	m_listEffectsOrder;

public:
	CEdit m_editEffectsOrder;
	afx_msg void OnBnClickedButtonChangeEffectsOrder();
	afx_msg LRESULT	OnUpdateEffectsOrder(WPARAM wParam, LPARAM lParam);
};
