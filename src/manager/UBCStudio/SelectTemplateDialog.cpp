// SelectTemplateDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "SelectTemplateDialog.h"

#include "UBCStudioDoc.h"

// CSelectTemplateDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSelectTemplateDialog, CDialog)

CSelectTemplateDialog::CSelectTemplateDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectTemplateDialog::IDD, pParent)
	, m_reposControl (this)
	, m_wndTemplate (4, CTemplate_Wnd::TEMPLATE_WND_LAYOUT, true)
{
}

CSelectTemplateDialog::~CSelectTemplateDialog()
{
}

void CSelectTemplateDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TEMPLATE, m_frameTemplate);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CSelectTemplateDialog, CDialog)
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_WM_GETMINMAXINFO()
	ON_MESSAGE(WM_TEMPLATE_FRAME_SELECT_CHANGED, OnTemplateFrameSelectChanged)
	ON_MESSAGE(WM_TEMPLATE_SELECTED, OnTemplateSelected)
END_MESSAGE_MAP()


// CSelectTemplateDialog 메시지 처리기입니다.

BOOL CSelectTemplateDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	HICON hIcon = LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_LAYOUT));
	SetIcon(hIcon, TRUE);
	SetIcon(hIcon, FALSE);

	m_btnOK.LoadBitmap(IDB_BTN_SELECT, RGB(255, 255, 255));
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));

	m_btnOK.SetToolTipText(LoadStringById(IDS_SELECTTEMPLATEDIALOG_BUT001));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_SELECTTEMPLATEDIALOG_BUT002));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	CRect rect;
	m_frameTemplate.GetWindowRect(rect);
	ScreenToClient(rect);

	m_wndTemplate.SetDocument(m_pDocument);
	m_wndTemplate.CreateEx(WS_EX_CLIENTEDGE, NULL, "Template", WS_CHILD | WS_VISIBLE | WS_VSCROLL, rect, this, 0xfeff);

	m_reposControl.AddControl(&m_wndTemplate, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnOK, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnCancel, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSelectTemplateDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnOK();
}

void CSelectTemplateDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();
}

BOOL CSelectTemplateDialog::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CRect rect;
	m_wndTemplate.GetWindowRect(rect);

	if(rect.PtInRect(pt))
	{
		m_wndTemplate.OnMouseWheel(nFlags, zDelta, pt);
		return TRUE;
	}

	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

LRESULT CSelectTemplateDialog::OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam)
{
	m_pDocument->SetSelectTemplateId((LPCTSTR)wParam);
	m_wndTemplate.SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, wParam, lParam);

	return 0;
}

void CSelectTemplateDialog::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMinTrackSize.x = 265 + ::GetSystemMetrics(SM_CXBORDER);
	lpMMI->ptMaxTrackSize.x = 265 + ::GetSystemMetrics(SM_CXBORDER);
}

LRESULT CSelectTemplateDialog::OnTemplateSelected(WPARAM wParam, LPARAM lParam)
{
	OnOK();

	return 0;
}