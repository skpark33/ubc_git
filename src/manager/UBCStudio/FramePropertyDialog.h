#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "EditEx.h"
#include "ColorPickerCB.h"
#include "Frame_Wnd.h"
#include "XInfoTip.h"

// CFramePropertyDialog 대화 상자입니다.
class CFramePropertyDialog : public CDialog
{
	DECLARE_DYNAMIC(CFramePropertyDialog)
public:
	CFramePropertyDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFramePropertyDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FRAME_PROPERTY };

protected:
	void SetTVSize(int nWidth, int nHeight, int MouseType=0);
	int GetMaxZIndex(TEMPLATE_LINK*);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CArray<CWnd*, CWnd*>	m_listNoCTLWnd;
	CBrush	m_brushBG;
	bool	m_bNowSetting;

	CFont			m_fontTip;
	CXInfoTip		m_Tip;

public:
	CEdit			m_editTemplateID;
	CEdit			m_editFrameID;
	//CEditEx			m_editGrade;
	CComboBox		m_cbxTVFrame;
	CEditEx			m_editZOrder;
	CSpinButtonCtrl	m_spinZOrder;
	CEditEx			m_editPosX;
	CEditEx			m_editPosY;
	CEditEx			m_editWidth;
	CEditEx			m_editHeight;
	CComboBox		m_cbxBorderStyle;
	CEditEx			m_editBorderThickness;
	CColorPickerCB	m_cbxBorderColor;
	CEditEx			m_editCornerRadius;
	CComboBox		m_cbxPIP;
	CEditEx			m_editAlpha;
	CEdit			m_editDescription;
	CButton			m_kbOverlayMode;	// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
//	CEdit			m_editComment1;
	CEdit			m_editComment2;
	CEdit			m_editComment3;
	//CSpinButtonCtrl	m_spinGrade;
	CSpinButtonCtrl	m_spinPosX;
	CSpinButtonCtrl	m_spinPosY;
	CSpinButtonCtrl	m_spinWidth;
	CSpinButtonCtrl	m_spinHeight;
	CSpinButtonCtrl	m_spinBorderThickness;
	CSpinButtonCtrl	m_spinCornerRadius;
	CSpinButtonCtrl	m_spinAlpha;
	CComboBox		m_comboFrameLevel;

	void		SetFrameInfo(FRAME_LINK* pFrameLink, int MouseType=0);
	bool		GetFrameInfo(FRAME_LINK* pFrameLink);
	int			GetIndex(CString strStyle);
	bool		GetPipCtrlStatus(FRAME_LINK* pFrameLink, bool bEnable);

	afx_msg void OnCbnSelchangeFrameLevelCombo();
	afx_msg LRESULT	OnPrimaryFrameOverlap(WPARAM wParam, LPARAM lParam);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnChangeEditGrade();
	afx_msg void OnComboSelChangePip();
	afx_msg void OnEnChangeTvframe();
	afx_msg void OnEnChangeEditZOder();
	afx_msg void OnEnChangeEditTrans();
	afx_msg void OnBnClickedKbOverlayMode();
};
