#pragma once
#include <afxmt.h>
#include <afxinet.h>

#define SPFTP_EXC_LEN			1024
#define SPFTP_BUF_SIZE			1024*4

interface ISpFtp
{
	virtual void InvokeSize(ULONGLONG, DWORD)=0;
};

class CSimpleFtp
{
public:
	enum {eNone=0, eConnect, eDownload, eUpload, eGetFile, ePutFile};
	enum {
		efail=0, eSuccess, eDisconnect,
		eRemoteFileNotFound, eLocalFileNotFound,
		eRemoteOpenFail, eLocalOpenFail,
		eAlreadyExistFile, eUnknownError
	};

	CSimpleFtp();
	~CSimpleFtp();

	bool Connect(LPCTSTR, LPCTSTR, LPCTSTR, int);
	bool ReConnect();
	void Destroy();
	void Close();
	
	void SetTimeOut(DWORD);
	void SetReceiver(ISpFtp*);
	bool IsTimeOut();
	LPCTSTR GetErrMsg();

	int GetFile(LPCTSTR, LPCTSTR, LPCTSTR, bool =true);
	int PutFile(LPCTSTR, LPCTSTR, LPCTSTR, bool =true);
	int Download(LPCTSTR, LPCTSTR, LPCTSTR, bool =true);
	int Upload(LPCTSTR, LPCTSTR, LPCTSTR, bool =true);
private:
	static UINT TimeProc(LPVOID);
public:
private:
	int m_Stat;
	int m_iPort;
	CString m_szIP;
	CString m_szID;
	CString m_szPW;

	char m_szException[SPFTP_EXC_LEN];

	bool m_bTimeOut;
	DWORD m_nCurTime;
	DWORD m_nTimeOut;

	bool m_bThread;
	CWinThread* m_pThread;

	ISpFtp* m_pReceiver;
	CInternetFile* m_pFile;
	CInternetSession m_Session;
	CFtpConnection* m_pConnection;
};