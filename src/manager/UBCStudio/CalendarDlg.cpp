// CalendarDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CalendarDlg.h"

// CCalendarDlg dialog
IMPLEMENT_DYNAMIC(CCalendarDlg, CDialog)

CCalendarDlg::CCalendarDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCalendarDlg::IDD, pParent)
{
	ZeroMemory(&m_stSel, sizeof(m_stSel));
}

CCalendarDlg::~CCalendarDlg()
{
}

void CCalendarDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MONTHCALENDAR1, m_ctrCalendar);
}

BEGIN_MESSAGE_MAP(CCalendarDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCalendarDlg::OnBnClickedOk)
	ON_WM_PAINT()
END_MESSAGE_MAP()

// CCalendarDlg message handlers
BOOL CCalendarDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	if(m_stSel.wYear != 0){
		m_ctrCalendar.SetCurSel(&m_stSel);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CCalendarDlg::OnBnClickedOk()
{
	m_ctrCalendar.GetCurSel(&m_stSel);

	OnOK();
}

void CCalendarDlg::SetCurSel(LPSYSTEMTIME pST)
{
	memcpy(&m_stSel, pST, sizeof(SYSTEMTIME));
}

void CCalendarDlg::GetCurSel(LPSYSTEMTIME pST)
{
	memcpy(pST, &m_stSel, sizeof(SYSTEMTIME));
}

void CCalendarDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CDialog::OnPaint() for painting messages
	CRect rc;
	GetClientRect(&rc);
	dc.FillSolidRect(&rc, RGB(0xff,0xff,0xff));
}
