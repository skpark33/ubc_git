// UBCStudio.h : main header file for the UBCStudio application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols
/*
#if defined(DEBUG) | defined(_DEBUG)
#define CRTDBG_MAP_ALLOC
#include  <stdlib.h>
#include <crtdbg.h>
//#define new new(_CLIENT_BLOCK, __FILE__, __LINE__)
#endif
*/

// CUBCStudioApp:
// See UBCStudio.cpp for the implementation of this class
//

class CUBCStudioApp : public CWinApp
{
public:
	CUBCStudioApp();

protected:

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	static BOOL SetThreadLocaleEx(LCID lcLocale);

// Implementation
	afx_msg void OnAppAbout();

	DECLARE_MESSAGE_MAP()

public:
	CMultiDocTemplate*	m_pLayoutTemplate;
	CMultiDocTemplate*	m_pPackageTemplate;
	CMultiDocTemplate*	m_pPlayContentsSearchTemplate;

};

extern CUBCStudioApp theApp;