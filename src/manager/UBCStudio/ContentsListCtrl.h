#pragma once

#include "CheckListCtrl.h"
#include "schedule.h"

// CContentsListCtrl

typedef struct
{
	CString		sPathName;
	CString		sFileName;
	CString		video;
	CString		audio;
	int			width;
	int			height;
	int			time;
	ULONGLONG	filesize;
	int			estimatesize;
	BOOL		hassami;
} SFile;

class CUBCStudioDoc;

class CContentsListCtrl : public CCheckListCtrl
{
	DECLARE_DYNAMIC(CContentsListCtrl)
public:
	CContentsListCtrl();
	virtual ~CContentsListCtrl();
	enum { eOWFAIL, eOWTRUE, eOWSKIP };

	BOOL m_bContentsFilter[CONTENTS_CNT];
	CString m_strNameFilter;

protected:
	DECLARE_MESSAGE_MAP()

	CImageList		m_ilContentsList;
	CUBCStudioDoc*	m_pDocument;
	int m_OverwriteCheck;

	void UpdateTotalSize();

public:
	afx_msg LRESULT	OnAddContents(WPARAM wParam, LPARAM lParam);

	void	SetDocument(CUBCStudioDoc* pDoc) { m_pDocument=pDoc; };

	void	Initialize();
	void	ClearContentsSelection();
	bool	RemoveSelectedContents();
	bool	DeleteContents(POSITION pos);
	bool	ModifyContents(CArray<CONTENTS_INFO*>&);
	bool	ModifyContents(CONTENTS_INFO* info);
	bool	InsertContents(CONTENTS_INFO* info, int nMode, bool bChangeState=true);		// 0 = Add List & Map, 1 = Only Add List, 2 = Modify
	bool	NewContents();
	bool	RefreshContents();

	BOOL	GetVideoInfo(CString sPathVideo, SFile& file);

	bool	ChangeContentsName(CONTENTS_INFO* pInfo);			///<콘텐츠 파일에 '['. ']' 문자가 있는경우에 이를 바꾸어 준다
	int     OverWriteContentsFile(CONTENTS_INFO* pInfo);		///<콘텐츠 파일의 덮어쓰는지 여부를 정하여 복사한다

	// Modified by 정운형 2009-01-13 오후 7:25
	// 변경내역 :  콘텐츠 추가 수정
	void	RunCLICmd(CONTENTS_INFO* pInfo, bool bCreate=true);
	// Modified by 정운형 2009-01-13 오후 7:25
	// 변경내역 :  콘텐츠 추가 수정
	BOOL SortCur();

	// Modified by skpark 2016.02.26
	// 변경내역 : 자동 인코딩을 위한 source format 정보획득
	unsigned long GetBitRate(LPCTSTR fullpath, 
		CString& video_format, CString& video_codec, CString& audio_format, CString& errStr);
};
