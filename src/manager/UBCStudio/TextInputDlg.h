#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "resource.h"
#include "WizardSubContents.h"
#include "ReposControl.h"


// CTextInputDlg 대화 상자입니다.

class CTextInputDlg : public CWizardSubContents
{
	DECLARE_DYNAMIC(CTextInputDlg)

public:
	CTextInputDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTextInputDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TEXT_INPUT_DLG };

	virtual void SetData(CString strContentsId, CString strValue);
	virtual void GetData(CString& strContentsId, CString& strValue);

	virtual CONTENTS_TYPE GetContentsType() { return CONTENTS_TEXT; }
	virtual void SetPreviewMode(bool bPreviewMode);

protected:
	CEdit		  m_ebTextData;
	CReposControl m_reposControl;

	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
