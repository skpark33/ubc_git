// TimeSelect.cpp : implementation file
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "TimeSelect.h"
#include "Enviroment.h"

// CTimeSelect dialog

IMPLEMENT_DYNAMIC(CTimeSelect, CDialog)

CTimeSelect::CTimeSelect(CWnd* pParent /*=NULL*/)
	: CDialog(CTimeSelect::IDD, pParent)
{
	m_szStartDT.Empty();
	m_szEndDT.Empty();
}

CTimeSelect::~CTimeSelect()
{
}

void CTimeSelect::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_START, m_ckStart);
	DDX_Control(pDX, IDC_CHECK_END, m_ckEnd);
	DDX_Control(pDX, IDC_DATESELECT, m_tmDate1);
	DDX_Control(pDX, IDC_TIMESELECT, m_tmTime1);
	DDX_Control(pDX, IDC_DATESELECT2, m_tmDate2);
	DDX_Control(pDX, IDC_TIMESELECT2, m_tmTime2);
}


BEGIN_MESSAGE_MAP(CTimeSelect, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK_START, &CTimeSelect::OnBnClickedCheckStart)
	ON_BN_CLICKED(IDC_CHECK_END, &CTimeSelect::OnBnClickedCheckEnd)
	ON_BN_CLICKED(IDOK, &CTimeSelect::OnBnClickedOk)
END_MESSAGE_MAP()


// CTimeSelect message handlers

BOOL CTimeSelect::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	SYSTEMTIME sttm;
	::GetLocalTime(&sttm);
	m_tmDate1.SetTime(&sttm);
	m_tmTime1.SetTime(&sttm);
	m_tmDate2.SetTime(&sttm);
	m_tmTime2.SetTime(&sttm);

	m_ckStart.SetCheck(FALSE);
	m_ckEnd.SetCheck(FALSE);

	m_tmDate1.EnableWindow(FALSE);
	m_tmTime1.EnableWindow(FALSE);
	m_tmDate2.EnableWindow(FALSE);
	m_tmTime2.EnableWindow(FALSE);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTimeSelect::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CTimeSelect::OnBnClickedCheckStart()
{
	UpdateData();
	if(m_ckStart.GetCheck()){
		m_tmDate1.EnableWindow(TRUE);
		m_tmTime1.EnableWindow(TRUE);
	}else{
		m_tmDate1.EnableWindow(FALSE);
		m_tmTime1.EnableWindow(FALSE);
	}
	UpdateData(FALSE);
}

void CTimeSelect::OnBnClickedCheckEnd()
{
	UpdateData();
	if(m_ckEnd.GetCheck()){
		m_tmDate2.EnableWindow(TRUE);
		m_tmTime2.EnableWindow(TRUE);
	}else{
		m_tmDate2.EnableWindow(FALSE);
		m_tmTime2.EnableWindow(FALSE);
	}
	UpdateData(FALSE);
}

void CTimeSelect::OnBnClickedOk()
{
	UpdateData();
	SYSTEMTIME stmStart, stmEnd, stmBuf;
	
	m_tmDate1.GetTime(&stmStart);
	m_tmTime1.GetTime(&stmBuf);
	stmStart.wHour = stmBuf.wHour;
	stmStart.wMinute = stmBuf.wMinute;
	stmStart.wSecond = stmBuf.wSecond;
	stmStart.wMilliseconds = stmBuf.wMilliseconds;

	m_tmDate2.GetTime(&stmEnd);
	m_tmTime2.GetTime(&stmBuf);
	stmEnd.wHour = stmBuf.wHour;
	stmEnd.wMinute = stmBuf.wMinute;
	stmEnd.wSecond = stmBuf.wSecond;
	stmEnd.wMilliseconds = stmBuf.wMilliseconds;

	CTime tmStart(stmStart), tmEnd(stmEnd), tmCurrent;

	if(m_ckStart.GetCheck()){
		m_szStartDT = tmStart.Format(STR_ENV_TIME);
	}else{
		m_szStartDT.Empty();
	}

	if(m_ckEnd.GetCheck()){
		m_szEndDT = tmEnd.Format(STR_ENV_TIME);
	}else{
		m_szEndDT.Empty();
	}

	if(m_ckStart.GetCheck() && m_ckEnd.GetCheck() && tmStart >= tmEnd){
		UbcMessageBox(LoadStringById(IDS_TIMESELECT_MSG001));
		return;
	}

	tmCurrent = CTime::GetCurrentTime();

	if(m_ckStart.GetCheck() && tmStart < tmCurrent){
		m_szStartDT.Empty();
	}

	if(m_ckEnd.GetCheck() && tmEnd < tmCurrent){
		m_szEndDT.Empty();
	}

	OnOK();
}
