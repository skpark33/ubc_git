#include "stdafx.h"

#include "InPlaceEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CInPlaceEdit

CInPlaceEdit::CInPlaceEdit(CWnd* pParent, CRect& rect, DWORD dwStyle, UINT nID, LPCTSTR lpszInitText)
{
	m_strInitText = lpszInitText;
	m_bExitOnArrows = FALSE;
	m_Rect = rect;  // For bizarre CE bug.
	m_bAlreadyEnding = FALSE;

	DWORD dwEditStyle = /*WS_BORDER|*/WS_CHILD|/*WS_VISIBLE|*/ES_AUTOHSCROLL | dwStyle;

	//  if (!Create(dwEditStyle, rect, pParent, nID))
	if (! CreateEx(WS_EX_CLIENTEDGE, _T("EDIT"), NULL, dwEditStyle, rect, pParent, nID))
		return;

	ModifyStyleEx(NULL, GetExStyle()  | WS_EX_CLIENTEDGE);

	m_clrBack = GetSysColor(COLOR_WINDOW);
	m_clrText = GetSysColor(COLOR_WINDOWTEXT);
	m_Brush.CreateSolidBrush(m_clrBack);

	SetFont(pParent->GetFont());

	SetWindowText(m_strInitText);
	SetFocus();

	SetSel(0, -1);
	SetSel(-1, 0);
}

CInPlaceEdit::~CInPlaceEdit()
{
}

void CInPlaceEdit::SetColors(COLORREF clrBack, COLORREF clrText)
{
	m_clrBack = clrBack;
	m_clrText = clrText;
	m_Brush.DeleteObject();
	m_Brush.CreateSolidBrush(m_clrBack);
}

BEGIN_MESSAGE_MAP(CInPlaceEdit, CEdit)
	ON_WM_KILLFOCUS()
	ON_WM_CHAR()
	ON_WM_KEYDOWN()
	ON_WM_GETDLGCODE()
	ON_WM_CREATE()
	ON_WM_CTLCOLOR_REFLECT( )
	ON_WM_CREATE()
END_MESSAGE_MAP()

////////////////////////////////////////////////////////////////////////////
// CInPlaceEdit message handlers

// If an arrow key (or associated) is pressed, then exit if
//  a) The Ctrl key was down, or
//  b) m_bExitOnArrows == TRUE
void CInPlaceEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if ((nChar == VK_PRIOR || nChar == VK_NEXT ||
		nChar == VK_RIGHT || nChar == VK_LEFT) &&
		(m_bExitOnArrows || GetKeyState(VK_CONTROL) < 0))
	{
		GetParent()->SetFocus();
		return;
	}
	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

int CInPlaceEdit::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CEdit::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

// As soon as this edit loses focus, kill it.
void CInPlaceEdit::OnKillFocus(CWnd* pNewWnd)
{
	CEdit::OnKillFocus(pNewWnd);
	EndEdit();
}

void CInPlaceEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (nChar == VK_TAB || nChar == VK_RETURN)
	{
		GetParent()->SetFocus();    // This will destroy this window
		return;
	}
	if (nChar == VK_ESCAPE) 
	{
		CancelEdit();
		return;
	}

	CEdit::OnChar(nChar, nRepCnt, nFlags);

	//// Resize edit control if needed
	//
	//// Get text extent
	//CString str;
	//GetWindowText( str );

	//// add some extra buffer
	//str += _T("  ");
	//
	//CWindowDC dc(this);
	//CFont *pFontDC = dc.SelectObject(GetFont());
	//CSize size = dc.GetTextExtent( str );
	//dc.SelectObject( pFontDC );
	//   
	//// Get client rect
	//CRect ParentRect;
	//GetParent()->GetClientRect( &ParentRect );
	//
	//// Check whether control needs to be resized
	//// and whether there is space to grow
	//if (size.cx > m_Rect.Width())
	//{
	//    if( size.cx + m_Rect.left < ParentRect.right )
	//        m_Rect.right = m_Rect.left + size.cx;
	//    else
	//        m_Rect.right = ParentRect.right;
	//    MoveWindow( &m_Rect );
	//}
}

UINT CInPlaceEdit::OnGetDlgCode() 
{
	return DLGC_WANTALLKEYS;
}

////////////////////////////////////////////////////////////////////////////
// CInPlaceEdit overrides

// Stoopid win95 accelerator key problem workaround - Matt Weagle.
BOOL CInPlaceEdit::PreTranslateMessage(MSG* pMsg) 
{
	// Catch the Alt key so we don't choke if focus is going to an owner drawn button
	if (pMsg->message == WM_SYSCHAR)
		return TRUE;
	return CEdit::PreTranslateMessage(pMsg);
}

////////////////////////////////////////////////////////////////////////////
// CInPlaceEdit implementation

void CInPlaceEdit::CancelEdit()
{
	// restore previous text
	if (IsWindow(GetSafeHwnd()))
	{
		SetWindowText(m_strInitText);
		delete this;//SendMessage(WM_CLOSE, 0, 0);
	}
}

void CInPlaceEdit::EndEdit()
{
	CString str;

	// EFW - BUG FIX - Clicking on a grid scroll bar in a derived class
	// that validates input can cause this to get called multiple times
	// causing assertions because the edit control goes away the first time.
	if(m_bAlreadyEnding)
		return;

	m_bAlreadyEnding = TRUE;
	GetWindowText(str);

	CWnd* pOwner = GetOwner();
	if (pOwner)
		pOwner->SendMessage(WM_ENDLABELEDIT, (WPARAM)(LPCTSTR)str, NULL );

	// Close this window (PostNcDestroy will delete this)
	if (IsWindow(GetSafeHwnd()))
		delete this;//PostMessage(WM_CLOSE);
}

HBRUSH CInPlaceEdit::CtlColor ( CDC* pDC, UINT nCtlColor )
{
	pDC->SetTextColor(m_clrText);
	pDC->SetBkColor(m_clrBack);
	return m_Brush;
}
