#ifndef __POPUPMENU_H__
#define __POPUPMENU_H__

#include "BCMenu.h"

enum enumMainMenu { 
	MNU_TOOL_SCHEDULE,
	MNU_TOOL_CONTENTS,
	MNU_TOOL_ADCOMM,
	MNU_TOOL_USER,
	MNU_TOOL_ENV,
	MNU_TOOL_ADPERS,
	MNU_TOOL_REPORT, // ���� ����
};

enum enumContentsPop { 
	MNU_TRAYICON = 0,	// TrayIcon
	MNU_CONTENTS,		// ������
	MNU_SITELIST,
	MNU_SITE,
	MNU_HOST,
	MNU_PROCESS,
	MNU_DEVICE,
	MNU_HOSTLIST
};

BOOL PopupMenu(UINT nMenuID, UINT nSubMenuPos, CWnd* pParentWnd, int pointX=0, int pointY=0);
BOOL PopupMenu(BCMenu* pPopupMenu, CWnd* pParentWnd, int pointX=0, int pointY=0);

#endif