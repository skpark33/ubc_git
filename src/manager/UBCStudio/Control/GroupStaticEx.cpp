// GroupStaticEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "GroupStaticEx.h"


// CGroupStaticEx

IMPLEMENT_DYNAMIC(CGroupStaticEx, CStatic)

CGroupStaticEx::CGroupStaticEx(COLORREF textColor, COLORREF bkColor)
:	m_rgbTextColor (textColor)
,	m_rgbBkColor (bkColor)
{
	m_brush.CreateSolidBrush( m_rgbBkColor );
}

CGroupStaticEx::~CGroupStaticEx()
{
}


BEGIN_MESSAGE_MAP(CGroupStaticEx, CStatic)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()


// CGroupStaticEx 메시지 처리기입니다.


BOOL CGroupStaticEx::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CRect rect;
	GetClientRect(rect);

	rect.DeflateRect(1,1);
	rect.top += 6;

	pDC->FillSolidRect(rect, m_rgbBkColor );

	return TRUE;
	//return CStatic::OnEraseBkgnd(pDC);
}

HBRUSH CGroupStaticEx::CtlColor(CDC* pDC, UINT nCtlColor)
{
	// TODO:  여기서 DC의 특성을 변경합니다.

	pDC->SetTextColor( m_rgbTextColor );
	pDC->SetBkColor( m_rgbBkColor );

	// TODO:  부모 처리기가 호출되지 않을 경우 Null이 아닌 브러시를 반환합니다.
	return m_brush;
}
