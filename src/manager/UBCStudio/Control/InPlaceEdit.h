#pragma once


#define		WM_ENDLABELEDIT		(WM_USER + 488)


class CInPlaceEdit : public CEdit
{
	// Construction
public:
	CInPlaceEdit(CWnd* pParent, CRect& rect, DWORD dwStyle, UINT nID, LPCTSTR lpszInitText);
	void SetColors(COLORREF clrBack, COLORREF clrText);

	// Attributes
public:
	// Operations
public:
	void CancelEdit();
	void EndEdit();

	// Overrides
	// ClassWizard generated virtual function overrides
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	// Implementation
public:
	virtual ~CInPlaceEdit();

	// Generated message map functions
protected:
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg UINT OnGetDlgCode();
	afx_msg HBRUSH CtlColor ( CDC* pDC, UINT nCtlColor );
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	DECLARE_MESSAGE_MAP()

private:

	CString		m_strInitText;
	BOOL		m_bExitOnArrows;
	CRect		m_Rect;
	BOOL		m_bAlreadyEnding;

	COLORREF	m_clrBack;
	COLORREF	m_clrText;
	CBrush		m_Brush;
};
