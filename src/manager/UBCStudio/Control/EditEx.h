#pragma once


// CEditEx

class CEditEx : public CEdit
{
	DECLARE_DYNAMIC(CEditEx)

public:
	CEditEx();
	virtual ~CEditEx();

protected:
	DECLARE_MESSAGE_MAP()

	CString	m_str;

public:

	int		GetValueInt();
	LPCTSTR	GetValueString();
	LPCTSTR	GetValueIntMoneyTypeString();

	void	SetValue(int value);
	void	SetValue(LPCTSTR value);
	void	SetValueToMoneyType(int value);
};


