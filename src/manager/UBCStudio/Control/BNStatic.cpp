#include "stdafx.h"
#include "BNStatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define BEVELLINE_SPACE   6


/////////////////////////////////////////////////////////////////////////////
// CBNStatic

CBNStatic::CBNStatic()
{
	m_clrBackColor                    = ::GetSysColor(COLOR_3DFACE);
	m_clrTextBackColor                = ::GetSysColor(COLOR_3DFACE);
	m_clrTextColor                    = ::GetSysColor(COLOR_WINDOWTEXT);
	m_clrSaveTextColor                = m_clrTextColor;
	m_clrHightLight                   = ::GetSysColor(COLOR_HIGHLIGHT);
	m_hBackBrush                      = ::CreateSolidBrush(m_clrBackColor);
	m_bForceNoOwnerDraw               = FALSE;
	m_bUseDefaultBackColor            = TRUE;
	m_bUseDefaultTextBackColor        = TRUE;
	m_bUseDefaultTextColor            = TRUE;
	m_bLink                           = FALSE;
	m_bUseBevelLine                   = FALSE;
	m_bUseHighLighting                = FALSE;
	m_bVerticalText                   = FALSE;
	m_bInside                         = FALSE;
	m_bTextFlash                      = FALSE;
	m_bBackFlash                      = FALSE;
	m_bFlashTextState                 = FALSE;
	m_bFlashBackState                 = FALSE;
	m_bBlockMessage                   = FALSE;
	m_bLowerCase                      = FALSE;
	m_bUpperCase                      = FALSE;
	m_bStretchBitmap                  = FALSE;
	m_hCursor                         = NULL;
	m_hBackImage                      = NULL;
	m_uiAlignment                     = HORZ_LEFT | VERT_TOP;

	::GetObject((HFONT)GetStockObject(DEFAULT_GUI_FONT), sizeof(m_stLF), &m_stLF);

	m_cFont.CreateFontIndirect(&m_stLF);
}

// --------------------------------------------------------------------------
CBNStatic::~CBNStatic()
{
	if (m_hBackImage) DeleteObject(m_hBackImage);

	m_cFont.DeleteObject();

	DeleteObject(m_hBackBrush);
}

// --------------------------------------------------------------------------
// PROTECTED MEMBER FUNCTIONS
// --------------------------------------------------------------------------

void CBNStatic::DrawAlignedText(CDC *pDC, LPCTSTR lpszText, RECT stRect, UINT uiAlignment)
{
	UINT uiFormat = 0;

	if ((m_uiAlignment & HORZ_LEFT  ) == HORZ_LEFT  ) uiFormat |= DT_LEFT;
	if ((m_uiAlignment & HORZ_CENTER) == HORZ_CENTER) uiFormat |= DT_CENTER;
	if ((m_uiAlignment & HORZ_RIGHT ) == HORZ_RIGHT ) uiFormat |= DT_RIGHT;

	BOOL bSingleLine = !IsMultiLineString(lpszText);
	BOOL bIsVTop     = ((m_uiAlignment & VERT_TOP   ) == VERT_TOP   );
	BOOL bIsVCenter  = ((m_uiAlignment & VERT_CENTER) == VERT_CENTER);
	BOOL bIsVBottom  = ((m_uiAlignment & VERT_BOTTOM) == VERT_BOTTOM);

	if (bSingleLine || (!bSingleLine && bIsVTop))
	{
		CRect rect = stRect;

		if (bIsVCenter) uiFormat |= DT_VCENTER | DT_SINGLELINE;

		if (bIsVBottom)
		{
			CSize cSize = pDC->GetTextExtent(lpszText);
			rect.top += (stRect.bottom - stRect.top) - cSize.cy;
		}

		pDC->DrawText(lpszText, -1, &rect, uiFormat);
		return;
	}

	// MultiLine Drawing (TOP or BOTTOM)
	pDC->DrawText(_T("**********"), -1, &stRect, uiFormat);
}

// --------------------------------------------------------------------------

int CBNStatic::GetWindowText(LPTSTR lpszStringBuf, int nMaxCount)
{
	m_bBlockMessage = TRUE;
	int iRet = CStatic::GetWindowText(lpszStringBuf, nMaxCount);
	m_bBlockMessage = FALSE;

	return iRet;
}

// --------------------------------------------------------------------------

void CBNStatic::GetWindowText(CString &rString)
{
	m_bBlockMessage = TRUE;
	CStatic::GetWindowText(rString);
	m_bBlockMessage = FALSE;
}

// --------------------------------------------------------------------------

BOOL CBNStatic::IsMultiLineString(LPCTSTR lpszText)
{
	for (unsigned int I=0; I<_tcslen(lpszText); I++)
	{
		if (lpszText[I] == _T('\n')) return TRUE;
	}

	return FALSE;
}

// --------------------------------------------------------------------------
CString CBNStatic::MakeVerticalText(LPCTSTR lpszText)
{
	CString strSource, strDest;
	strSource = (lpszText) ? lpszText : m_strText;

	for (int I=0; I<strSource.GetLength(); I++)
	{
		if (I > 0) strDest += _T('\n');
		strDest += strSource[I];
	}

	return strDest;
}

// --------------------------------------------------------------------------

BOOL CBNStatic::ReconstructFont()
{
	m_cFont.DeleteObject();
	BOOL bRet = m_cFont.CreateFontIndirect(&m_stLF);
	RedrawWindow();

	return bRet;
}

// --------------------------------------------------------------------------

void CBNStatic::SetWindowText(LPCTSTR lpszString)
{
	m_bBlockMessage = TRUE;
	CStatic::SetWindowText(lpszString);
	m_bBlockMessage = FALSE;
}

// --------------------------------------------------------------------------

void CBNStatic::TrackMouse(BOOL bHover, BOOL bLeave)
{
	TRACKMOUSEEVENT stTME;

	stTME.cbSize      = sizeof(stTME);
	stTME.dwFlags     = 0;
	stTME.hwndTrack   = GetSafeHwnd();
	stTME.dwHoverTime = 50;

	if (bHover) stTME.dwFlags |= TME_HOVER;
	if (bLeave) stTME.dwFlags |= TME_LEAVE;

	_TrackMouseEvent(&stTME);
}

// --------------------------------------------------------------------------
// PUBLIC MEMBER FUNCTIONS
// --------------------------------------------------------------------------

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// COLOR FUNCTIONS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CBNStatic &CBNStatic::SetBkColor(COLORREF clr)
{
	m_bUseDefaultBackColor = FALSE;
	m_clrBackColor         = clr;

	DeleteObject(m_hBackBrush);

	m_hBackBrush = CreateSolidBrush(m_clrBackColor);

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetDefaultBkColor()
{
	m_bUseDefaultBackColor = TRUE;
	m_clrBackColor         = ::GetSysColor(COLOR_3DFACE);

	DeleteObject(m_hBackBrush);

	m_hBackBrush = CreateSolidBrush(m_clrBackColor);

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetDefaultTextBackColor()
{
	m_bUseDefaultTextBackColor = TRUE;
	m_clrTextBackColor         = ::GetSysColor(COLOR_3DFACE);

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetDefaultTextColor()
{
	m_bUseDefaultTextColor = TRUE;
	m_clrTextColor         = ::GetSysColor(COLOR_WINDOWTEXT);

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetHighLightColor(COLORREF clr)
{
	m_clrHightLight = clr;
	RedrawWindow();
	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetTextBackColor(COLORREF clr)
{
	m_bUseDefaultTextBackColor = FALSE;
	m_clrTextBackColor         = clr;

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetTextColor(COLORREF clr)
{
	m_bUseDefaultTextColor = FALSE;
	m_clrTextColor         = clr;

	RedrawWindow();

	return *this;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// FONT FUNCTIONS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CBNStatic &CBNStatic::SetFontBold(BOOL bBold)
{
	m_stLF.lfWeight = bBold ? FW_BOLD : FW_NORMAL;
	ReconstructFont();
	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetFontItalic(BOOL bItalic)
{
	m_stLF.lfItalic = bItalic;
	ReconstructFont();
	return *this;
}

CBNStatic &CBNStatic::SetFontStrike(BOOL bStrike)
{
	m_stLF.lfStrikeOut = bStrike;
	ReconstructFont();
	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetFontName(LPCTSTR szFaceName)
{
	_tcscpy(m_stLF.lfFaceName, szFaceName);
	ReconstructFont();
	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetFontSize(BOOL iSize)
{
	m_stLF.lfHeight = iSize;
	ReconstructFont();
	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetFontUnderline(BOOL bUnderline)
{
	m_stLF.lfUnderline = bUnderline;
	ReconstructFont();
	return *this;
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// OTHER FUNCTIONS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CBNStatic &CBNStatic::FlashBackground(BOOL bActivate, UINT uiTime)
{
	if (m_bBackFlash) KillTimer(2);

	if (bActivate) SetTimer(2, uiTime, NULL);
	else InvalidateRect(NULL, FALSE);

	m_bBackFlash = bActivate;

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::FlashText(BOOL bActivate, UINT uiTime)
{
	if (m_bTextFlash)
	{
		SetWindowText(m_strText);
		KillTimer(1);
	}

	if (bActivate)
	{
		SetTimer(1, uiTime, NULL);
	}
	else InvalidateRect(NULL, FALSE);

	m_bTextFlash = bActivate;

	return *this;
}

// --------------------------------------------------------------------------

void CBNStatic::ForceNoOwnerDraw(BOOL bForce)
{
	m_bForceNoOwnerDraw = bForce;
	RedrawWindow();
}

// --------------------------------------------------------------------------

BOOL CBNStatic::GetBorder()
{
	return ((GetStyle() & WS_BORDER) == WS_BORDER) ? TRUE : FALSE;
}

// --------------------------------------------------------------------------

UINT CBNStatic::GetHorzAlignment()
{
	if ((m_uiAlignment & HORZ_LEFT) == HORZ_LEFT) return HORZ_LEFT;

	if ((m_uiAlignment & HORZ_CENTER) == HORZ_CENTER) return HORZ_CENTER;

	if ((m_uiAlignment & HORZ_RIGHT) == HORZ_RIGHT) return HORZ_RIGHT;

	return 0;
}

// --------------------------------------------------------------------------

UINT CBNStatic::GetHorzAlignmentIndex()
{
	if ((m_uiAlignment & HORZ_LEFT) == HORZ_LEFT) return 0;

	if ((m_uiAlignment & HORZ_CENTER) == HORZ_CENTER) return 1;

	if ((m_uiAlignment & HORZ_RIGHT) == HORZ_RIGHT) return 2;

	return 0;
}

// --------------------------------------------------------------------------

BOOL CBNStatic::GetSunken()
{
	return ( (GetExStyle() & WS_EX_STATICEDGE) == WS_EX_STATICEDGE) ? TRUE : FALSE;
}

// --------------------------------------------------------------------------

CString CBNStatic::GetText()
{
	return m_strText;
}

// --------------------------------------------------------------------------

UINT CBNStatic::GetVertAlignment()
{
	if ((m_uiAlignment & VERT_TOP) == VERT_TOP) return VERT_TOP;

	if ((m_uiAlignment & VERT_CENTER) == VERT_CENTER) return VERT_CENTER;

	if ((m_uiAlignment & VERT_BOTTOM) == VERT_BOTTOM) return VERT_BOTTOM;

	return 0;
}

// --------------------------------------------------------------------------

UINT CBNStatic::GetVertAlignmentIndex()
{
	if ((m_uiAlignment & VERT_TOP) == VERT_TOP) return 0;
	if ((m_uiAlignment & VERT_CENTER) == VERT_CENTER) return 1;
	if ((m_uiAlignment & VERT_BOTTOM) == VERT_BOTTOM) return 2;

	return 0;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetAlignment(UINT uiAlign)
{
	DWORD dwRemove = 0;
	DWORD dwHorz = 0;
	DWORD dwVert = 0;

	if ((uiAlign & HORZ_RIGHT) == HORZ_RIGHT)
	{
		dwHorz        = SS_RIGHT;
		dwRemove      = SS_LEFT | SS_CENTER;
		m_uiAlignment = HORZ_RIGHT;
	}

	if ((uiAlign & HORZ_CENTER) == HORZ_CENTER)
	{
		dwHorz        = SS_CENTER;
		dwRemove      = SS_LEFT | SS_RIGHT;
		m_uiAlignment = HORZ_CENTER;
	}

	if ((uiAlign & HORZ_LEFT) == HORZ_LEFT)
	{
		dwHorz        = SS_LEFT;
		dwRemove      = SS_RIGHT | SS_CENTER;
		m_uiAlignment = HORZ_LEFT;
	}

	ModifyStyle(dwRemove, dwHorz);

	if ((uiAlign & VERT_TOP   ) == VERT_TOP   ) dwVert = VERT_TOP;
	if ((uiAlign & VERT_CENTER) == VERT_CENTER) dwVert = VERT_CENTER;
	if ((uiAlign & VERT_BOTTOM) == VERT_BOTTOM) dwVert = VERT_BOTTOM;

	m_uiAlignment |= dwVert;

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetBackImage(UINT nID)
{
	if (m_hBackImage) DeleteObject(m_hBackImage);
		m_hBackImage = NULL;

	if (nID != -1) {
		m_bmpBackImage.LoadBitmap(nID);
		m_hBackImage = m_bmpBackImage;
		//m_hBackImage = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(nID));
		if(m_hBackImage == NULL)
		{
			CString str;
			str.Format(_T("SetBackImage(%d)---> LoadBitmap() Fail!!!"), nID);
			UbcMessageBox(str);
		}
		else
		{
			m_bmpBackImage
	}
	InvalidateRect(NULL, TRUE);

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetBorder(BOOL bSet)
{
	ModifyStyle(bSet ? 0 : WS_BORDER,
			  bSet ? WS_BORDER : 0,
			  SWP_DRAWFRAME);

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetCursor(HCURSOR hCursor)
{
	m_hCursor = hCursor;
	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetCursor(UINT uiCursorID)
{
	m_hCursor = AfxGetApp()->LoadCursor(uiCursorID);
	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetDisabled(BOOL bSet)
{
	ModifyStyle(bSet ? 0 : WS_DISABLED,
			  bSet ? WS_DISABLED : 0,
			  0);

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetLink(BOOL bSet)
{
	m_bLink = bSet;
	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetLinkCursor(HCURSOR hCursor)
{
	m_hCursor = hCursor;
	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetLinkCursor(UINT uiCursorID)
{
	m_hCursor = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(uiCursorID));
	return *this;
}


// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetText(LPCTSTR szText)
{
	m_strText = szText;

	if (m_bVerticalText)
	{
		SetWindowText( MakeVerticalText(szText) );
	}
	else SetWindowText(szText);

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------

CBNStatic &CBNStatic::SetSunken(BOOL bSet)
{
	ModifyStyleEx(bSet ? 0 : WS_EX_STATICEDGE,
				bSet ? WS_EX_STATICEDGE : 0,
				SWP_DRAWFRAME);

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------
CBNStatic &CBNStatic::SetUpperCase(BOOL bSet)
{
	m_bUpperCase = bSet;
	if(m_bUpperCase)
		m_bLowerCase = FALSE;
	
	RedrawWindow();
	return *this;
}

CBNStatic &CBNStatic::SetLowerCase(BOOL bSet)
{
	m_bLowerCase = bSet;
	if(m_bLowerCase)
		m_bUpperCase = FALSE;
	
	RedrawWindow();
	return *this;
}


// --------------------------------------------------------------------------
CBNStatic &CBNStatic::SetVerticalText(BOOL bSet)
{
	if (bSet && !m_bVerticalText)
	{
		SetWindowText( MakeVerticalText(m_strText) );
	}
	else if (m_bVerticalText) SetWindowText(m_strText);

	m_bVerticalText = bSet;

	RedrawWindow();

	return *this;
}

// --------------------------------------------------------------------------
CBNStatic &CBNStatic::StretchBitmap(BOOL bStretch)
{
	m_bStretchBitmap = bStretch;
	if (m_hBackImage) RedrawWindow();
	return *this;
}

// --------------------------------------------------------------------------
CBNStatic &CBNStatic::UseBevelLine(BOOL bUse)
{
	m_bUseBevelLine = bUse;
	RedrawWindow();
	return *this;
}

// --------------------------------------------------------------------------
CBNStatic &CBNStatic::UseHighLighting(BOOL bUse)
{
	m_bUseHighLighting = bUse;

	if (!bUse) SetTextColor(m_clrSaveTextColor);

	return *this;
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CBNStatic, CStatic)
	//{{AFX_MSG_MAP(CBNStatic)
	ON_WM_ENABLE()
	ON_WM_ERASEBKGND()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBNStatic message handlers

BOOL CBNStatic::DestroyWindow() 
{
	return CStatic::DestroyWindow();
}

// --------------------------------------------------------------------------

HBRUSH CBNStatic::CtlColor(CDC *pDC, UINT nCtlColor)
{
	if (!m_bForceNoOwnerDraw) return NULL;

	if (CTLCOLOR_STATIC == nCtlColor)
	{
		pDC->SelectObject(&m_cFont);
		pDC->SetTextColor(!m_bUseDefaultTextColor ? m_clrTextColor : ::GetSysColor(COLOR_WINDOWTEXT));
		pDC->SetBkMode(TRANSPARENT);
	}

	return m_hBackBrush;
}

// --------------------------------------------------------------------------

void CBNStatic::PreSubclassWindow() 
{
	CStatic::PreSubclassWindow();

	ModifyStyle(0, SS_NOTIFY);

	if ((GetStyle() & SS_CENTER) == SS_CENTER) m_uiAlignment = GetVertAlignment() | HORZ_CENTER;

	if ((GetStyle() & SS_RIGHT) == SS_RIGHT) m_uiAlignment = GetVertAlignment() | HORZ_RIGHT;

	if ((GetStyle() & SS_CENTERIMAGE) == SS_CENTERIMAGE) m_uiAlignment = GetHorzAlignment() | VERT_CENTER;

	GetWindowText(m_strText);
}

// --------------------------------------------------------------------------

LRESULT CBNStatic::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if (message == WM_MOUSEHOVER)
	{
		if (m_bUseHighLighting)
		{
			m_clrSaveTextColor = m_clrTextColor;
			SetTextColor(m_clrHightLight);
		}
	}

	else if (message == WM_MOUSELEAVE)
	{
		// Determine if mouse pointer is within control when tooltip is used
		// because when mouse go over tooltip, this message is sended.
		TrackMouse(TRUE, FALSE);

		m_bInside = FALSE;

		if (m_bUseHighLighting) SetTextColor(m_clrSaveTextColor);
	}

	else if (message == WM_SETTEXT)
	{
		if (!m_bBlockMessage)
		{
			m_strText = (LPCTSTR)lParam;
		}
	}

	else if (message == WM_GETTEXT)
	{
		if (!m_bBlockMessage)
		{
			int iCount = (int)wParam;

			memset((char *)lParam, 0, iCount + 1);
			memcpy((char *)lParam, m_strText, iCount);
			return TRUE;
		}
	}

	return CStatic::WindowProc(message, wParam, lParam);
}

// --------------------------------------------------------------------------

void CBNStatic::OnEnable(BOOL bEnable) 
{
	SetRedraw(FALSE);
	CStatic::OnEnable(bEnable);
	SetRedraw(TRUE);
	Invalidate();
}

// --------------------------------------------------------------------------

BOOL CBNStatic::OnEraseBkgnd(CDC* pDC) 
{
	return CStatic::OnEraseBkgnd(pDC);
}

// --------------------------------------------------------------------------

void CBNStatic::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if (m_bLink) 
	{
		ShellExecute(NULL, _T("open"), m_strText, NULL, NULL, SW_SHOWNORMAL);
	}
	
	CStatic::OnLButtonDown(nFlags, point);
}

// --------------------------------------------------------------------------

void CBNStatic::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (!m_bInside)
	{
		m_bInside = TRUE;
		TrackMouse(TRUE, TRUE);
	}

	CStatic::OnMouseMove(nFlags, point);
}

// --------------------------------------------------------------------------

BOOL CBNStatic::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	if (m_hCursor)
	{
		::SetCursor(m_hCursor);
		return TRUE;
	}

	return CStatic::OnSetCursor(pWnd, nHitTest, message);
}

// --------------------------------------------------------------------------

void CBNStatic::OnTimer(UINT nIDEvent) 
{
	switch(nIDEvent)
	{
	case 1 : // TEXT FLASH
			 if (!m_bTextFlash) break;

			 m_bFlashTextState = !m_bFlashTextState;
			 SetWindowText(m_bFlashTextState ? _T("") : m_strText);

			 if (!m_bFlashTextState) InvalidateRect(NULL, FALSE);

			 if (!m_bUseDefaultBackColor && m_clrBackColor != GetSysColor(COLOR_3DFACE)) {
			   InvalidateRect(NULL, FALSE);
			 }
			 break;

	case 2 : // BACK FLASH
			 if (!m_bBackFlash) break;

			 m_bFlashBackState = !m_bFlashBackState;

			 InvalidateRect(NULL, TRUE);
			 break;
	}

	CStatic::OnTimer(nIDEvent);
}

// --------------------------------------------------------------------------

void CBNStatic::OnPaint() 
{
	if (m_bForceNoOwnerDraw)
	{
		CStatic::OnPaint();
		return;
	}

	CPaintDC dc(this); // device context for painting
	CRect    rectClient;
	CString  strText = (!m_bVerticalText) ? m_strText : MakeVerticalText(m_strText);

	if (m_bLowerCase) strText.MakeLower();
	if (m_bUpperCase) strText.MakeUpper();

	GetClientRect(&rectClient);

	// Get Horz & Vert Alignment
	UINT uiHorzAlignment = DT_LEFT;
	UINT uiVertAlignment = 0;

	if ((m_uiAlignment & HORZ_CENTER) == HORZ_CENTER) uiHorzAlignment = DT_CENTER;

	if ((m_uiAlignment & HORZ_RIGHT) == HORZ_RIGHT) uiHorzAlignment = DT_RIGHT;

	if ((m_uiAlignment & VERT_CENTER) == VERT_CENTER) uiVertAlignment = 1;

	if ((m_uiAlignment & VERT_BOTTOM) == VERT_BOTTOM) uiVertAlignment = 2;

	// Set current font
	CFont *pOldFont = dc.SelectObject(&m_cFont);

	// Set Back Mode
	int iOldMode = dc.SetBkMode(TRANSPARENT);

	// Erase background
	CBrush cBrush(::GetSysColor(COLOR_3DFACE));

	if (!m_bUseDefaultBackColor)
	{
		cBrush.DeleteObject();
		cBrush.CreateSolidBrush(m_clrBackColor);
	}

	if (m_bBackFlash && m_bFlashBackState)
	{
		COLORREF clr = m_bUseDefaultBackColor ? m_clrBackColor / 2 : ::GetSysColor(COLOR_3DFACE);

		cBrush.DeleteObject();
		cBrush.CreateSolidBrush(clr);
	}

	dc.FillRect(rectClient, &cBrush);

	cBrush.DeleteObject();

	if (m_hBackImage)
	{
		BITMAP stBitmap;

		GetObject(m_hBackImage, sizeof(stBitmap), &stBitmap);

		CDC memDC;
		memDC.CreateCompatibleDC(&dc);
		memDC.SelectObject(m_hBackImage);

		if (m_bStretchBitmap)
		{
			dc.StretchBlt(0,
						0,
						rectClient.Width(),
						rectClient.Height(),
						&memDC,
						0,
						0,
						stBitmap.bmWidth,
						stBitmap.bmHeight,
						SRCCOPY);
		}
		else 
		{
			int X = (rectClient.Width() - stBitmap.bmWidth) / 2;
			int Y = (rectClient.Height() - stBitmap.bmHeight) / 2;
			int W = rectClient.Width();
			int H = rectClient.Height();

			if (X < 0)
			{
				X = -X / 2;
				W = rectClient.Width() - X;
			}

			if (Y < 0)
			{
				Y = -Y / 2;
				H = rectClient.Height() - Y;
			}

			dc.BitBlt(X,
					Y,
					W,
					H,
					&memDC,
					0,
					0,
					SRCCOPY);
		}

		memDC.DeleteDC();
	}

	dc.SetTextColor(!m_bUseDefaultTextColor ? m_clrTextColor : ::GetSysColor(COLOR_WINDOWTEXT));
	DrawAlignedText(&dc, strText, rectClient, m_uiAlignment);

	dc.Draw3dRect(rectClient, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));

	// Restore
	dc.SelectObject(pOldFont);
	dc.SetBkMode(iOldMode);
}

// --------------------------------------------------------------------------
