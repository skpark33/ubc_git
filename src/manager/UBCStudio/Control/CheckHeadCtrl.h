// CheckHeadCtrl.h : header file
//

#pragma once


/////////////////////////////////////////////////////////////////////////////
// CCheckHeadCtrl window

class CCheckHeadCtrl : public CHeaderCtrl
{
// Construction
public:
	CCheckHeadCtrl();
	virtual ~CCheckHeadCtrl();
	//virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);

// Attributes
public:

// Operations
public:

// Overrides
public:

// Implementation
public:

	int		m_iSortColumn;
	bool	m_bSortAscending;

	bool	m_bCheckBoxType;
	void	SetSortArrow( const int iSortColumn );

	DECLARE_MESSAGE_MAP()

protected:

	afx_msg BOOL  OnItemClicked(NMHDR* pNMHDR, LRESULT* pResult);

};


