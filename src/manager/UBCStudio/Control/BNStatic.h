#if !defined(AFX_BNSTATIC_H__50B040A0_5E0B_11D4_BC43_0050BAA38233__INCLUDED_)
#define AFX_BNSTATIC_H__50B040A0_5E0B_11D4_BC43_0050BAA38233__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CBNStatic window

class CBNStatic : public CStatic
{
public :
	enum
	{
		HORZ_LEFT	= 0x01,
		HORZ_CENTER	= 0x02,
		HORZ_RIGHT	= 0x04,
		VERT_TOP	= 0x08,
		VERT_CENTER	= 0x10,
		VERT_BOTTOM	= 0x20
	};

protected :
	COLORREF          m_clrBackColor;
	COLORREF          m_clrTextBackColor;
	COLORREF          m_clrTextColor;
	COLORREF          m_clrHightLight;
	COLORREF          m_clrSaveTextColor;
	HBRUSH            m_hBackBrush;
	BOOL              m_bForceNoOwnerDraw;
	BOOL              m_bUseDefaultBackColor;
	BOOL              m_bUseDefaultTextBackColor;
	BOOL              m_bUseDefaultTextColor;
	BOOL              m_bLink;
	BOOL              m_bUseBevelLine;
	BOOL              m_bUseHighLighting;
	BOOL              m_bVerticalText;
	BOOL              m_bInside;
	BOOL              m_bTextFlash;
	BOOL              m_bBackFlash;
	BOOL              m_bFlashTextState;
	BOOL              m_bFlashBackState;
	BOOL              m_bBlockMessage;
	BOOL              m_bLowerCase;
	BOOL              m_bUpperCase;
	BOOL              m_bStretchBitmap;
	LOGFONT           m_stLF;
	CFont             m_cFont;
	HCURSOR           m_hCursor;
	HBITMAP           m_hBackImage;
	CBitmap			  m_bmpBackImage;
	UINT              m_uiAlignment;
	UINT              m_uiAngle;
	CString           m_strText;

	void    DrawAlignedText(CDC *pDC, LPCTSTR lpszText, RECT stRect, UINT uiAlignment);
	int     GetWindowText(LPTSTR lpszStringBuf, int nMaxCount);
	BOOL    IsMultiLineString(LPCTSTR lpszText);
	CString MakeVerticalText(LPCTSTR lpszText = NULL);
	BOOL    ReconstructFont();
	void    SetWindowText(LPCTSTR lpszString);
	void    TrackMouse(BOOL bHover, BOOL bLeave);

public :
	CBNStatic();
	virtual ~CBNStatic();
	void    GetWindowText(CString &rString);

	// Color functions
	CBNStatic &SetBkColor(COLORREF clr = ::GetSysColor(COLOR_3DFACE));
	CBNStatic &SetDefaultBkColor();
	CBNStatic &SetDefaultTextBackColor();
	CBNStatic &SetDefaultTextColor();
	CBNStatic &SetHighLightColor(COLORREF clr = ::GetSysColor(COLOR_HIGHLIGHT));
	CBNStatic &SetTextBackColor(COLORREF clr = ::GetSysColor(COLOR_3DFACE));
	CBNStatic &SetTextColor(COLORREF clr = ::GetSysColor(COLOR_WINDOWTEXT));

	// Font functions
	CBNStatic &SetFontBold(BOOL bBold);
	CBNStatic &SetFontItalic(BOOL bItalic);
	CBNStatic &SetFontStrike(BOOL bStrike);
	CBNStatic &SetFontName(LPCTSTR szFaceName);
	CBNStatic &SetFontSize(int iSize);
	CBNStatic &SetFontUnderline(BOOL bUnderline);

	// Other functions
	CBNStatic &FlashBackground(BOOL bActivate, UINT uiTime = 500);
	CBNStatic &FlashText(BOOL bActivate, UINT uiTime = 500);

	void ForceNoOwnerDraw(BOOL bForce);

	BOOL    GetBorder();
	UINT    GetHorzAlignment();
	UINT    GetHorzAlignmentIndex();
	BOOL    GetSunken();
	CString GetText();
	UINT    GetVertAlignment();
	UINT    GetVertAlignmentIndex();

	CBNStatic &SetAlignment(UINT uiAlign = HORZ_LEFT | VERT_TOP);
	CBNStatic &SetBackImage(UINT nID = -1);
	CBNStatic &SetBorder(BOOL bSet);
	CBNStatic &SetCursor(HCURSOR hCursor);
	CBNStatic &SetCursor(UINT uiCursorID);
	CBNStatic &SetDisabled(BOOL bSet);
	CBNStatic &SetLink(BOOL bSet);
	CBNStatic &SetLinkCursor(HCURSOR hCursor);
	CBNStatic &SetLinkCursor(UINT uiCursorID);
	CBNStatic &SetLowerCase(BOOL bSet);
	CBNStatic &SetText(LPCTSTR szText);
	CBNStatic &SetSunken(BOOL bSet);
	CBNStatic &SetUpperCase(BOOL bSet);
	CBNStatic &SetVerticalText(BOOL bSet);
	CBNStatic &StretchBitmap(BOOL bStretch);
	CBNStatic &UseBevelLine(BOOL bUse);
	CBNStatic &UseHighLighting(BOOL bUse);

	// INLINE
	COLORREF GetBkColor()             { return m_clrBackColor; }
	COLORREF GetHighLightColor()      { return m_clrHightLight; }
	COLORREF GetTextBackColor()       { return m_clrTextBackColor; }
	COLORREF GetTextColor()           { return m_clrTextColor; }
	BOOL     IsDefaultBkColor()       { return m_bUseDefaultBackColor; }
	BOOL     IsDefaultTextBackColor() { return m_bUseDefaultTextBackColor; }
	BOOL     IsDefaultTextColor()     { return m_bUseDefaultTextColor; }

	BOOL     GetFontBold()            { return ((m_stLF.lfWeight & FW_BOLD) == FW_BOLD) ? TRUE : FALSE; }
	BOOL     GetFontItalic()          { return m_stLF.lfItalic; }
	CString  GetFontName()            { return m_stLF.lfFaceName; }
	LONG     GetFontSize()            { return m_stLF.lfHeight; }
	BOOL     GetFontUnderline()       { return m_stLF.lfUnderline; }

	UINT     GetAlignment()                   { return m_uiAlignment; }
	UINT     GetAngle()                       { return m_uiAngle; }
	BOOL     GetBevelLine()                   { return m_bUseBevelLine; }
	BOOL     GetLink()                        { return m_bLink; }
	BOOL     GetVerticalText()                { return m_bVerticalText; }
	BOOL     IsBitmapStretched()              { return m_bStretchBitmap; }
	BOOL     IsDisabled()                     { return ((GetStyle() & WS_DISABLED) == WS_DISABLED); }
	BOOL     IsLowerCase()                    { return m_bLowerCase; }
	BOOL     IsUpperCase()                    { return m_bUpperCase; }

 public :
	//{{AFX_VIRTUAL(CBNStatic)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void PreSubclassWindow();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

protected :
	//{{AFX_MSG(CBNStatic)
	afx_msg void OnEnable(BOOL bEnable);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
 };

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BNSTATIC_H__50B040A0_5E0B_11D4_BC43_0050BAA38233__INCLUDED_)
