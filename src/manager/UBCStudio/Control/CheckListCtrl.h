#pragma once

#include <afxtempl.h>
#include "CheckHeadCtrl.h"



class CCheckListCtrl : public CListCtrl
{
public:
	CCheckListCtrl();

	enum SORT_TYPE { SORTTYPE_STRING=0, SORTTYPE_NUMBER};
	typedef struct
	{
		HWND	hWnd;			// 리스트컨트롤 핸들
		int		nCol;			// 정렬기준 컬럼
		bool	bAscend;		// true=내림차순, false=오름차순
		SORT_TYPE	sortType;	//
	} SORT_PARAM;
	typedef CArray<CCheckListCtrl::SORT_TYPE, CCheckListCtrl::SORT_TYPE> SortTypeArray;

protected:
	CCheckHeadCtrl	m_checkHeadCtrl;
//	virtual void PreSubclassWindow();

public:
	virtual ~CCheckListCtrl();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnItemChanged(NMHDR* pNMHDR, LRESULT* pResult);		
	afx_msg BOOL OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);

protected:
	DECLARE_MESSAGE_MAP()

	CImageList	m_checkImgList;
	CImageList	m_checkImgList2;

	int			m_nSortCol;
	bool		m_bAscend;

	static int CALLBACK SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam);

	BOOL		m_blInited;
	bool		m_bCheckBoxType;
	bool		m_bRowColorType;
	int			m_nColumnCount;

	SortTypeArray	m_listSortType;
	CUIntArray		m_listTextColor;
	CUIntArray		m_listBackColor;

public:

	virtual BOOL _SortItems(PFNLVCOMPARE pfnCompare, DWORD data);

	BOOL Initialize(bool bCheckBoxType=true, bool bIsRowColorType=true, UINT nID=0);

	BOOL	Sort(int col=-1, bool ascend=true);
	bool	SetColumnType(int nCol, SORT_TYPE sortType, COLORREF crText=0xFFFFFFFF, COLORREF crBack=0xFFFFFFFF);
	int		InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat=LVCFMT_LEFT, int nWidth=-1, int nSubItem=-1);

	virtual	bool GetRowColor(int nRow, COLORREF& crText, COLORREF& clrBk);
	virtual	bool GetItemColor(int nRow, int nCol, COLORREF& crText, COLORREF& clrBk);

	int		GetColumnCount();
	void	AdjustColumnWidth(); // auto column fit

	// background image control
protected:
	bool	m_bLButton;
	int		m_cx;
	int		m_cy;
	int		m_nColumn;

	bool	m_bScrollBkImg;
	CBitmap	m_bmBkImg;
	BOOL	_SetBkImage(LPCTSTR lpszName, BOOL bFromFile = FALSE);

	afx_msg void	OnPaint();
//	afx_msg BOOL	OnEraseBkgnd(CDC* pDC);
	afx_msg void	OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void	OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL	OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void	OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void	OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnHdnEndtrack(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnHdnBegintrack(NMHDR *pNMHDR, LRESULT *pResult);

public:
	BOOL	SetBkImage(UINT nID);				// from resource
	BOOL	SetBkImage(LPCTSTR lpszName);		// from resource
	BOOL	SetBkImageFromFile(LPCTSTR lpszFileName);	// from file
	BOOL	SetBkImage(CBitmap *pBitmap);

	void	SetScrollBkImg(bool bScrollBkImg) { m_bScrollBkImg = bScrollBkImg; };
};


