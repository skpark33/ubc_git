#pragma once


// CGroupStaticEx

class CGroupStaticEx : public CStatic
{
	DECLARE_DYNAMIC(CGroupStaticEx)

public:
	CGroupStaticEx(COLORREF textColor, COLORREF bkColor);
	virtual ~CGroupStaticEx();

protected:
	DECLARE_MESSAGE_MAP()

	COLORREF	m_rgbTextColor;
	COLORREF	m_rgbBkColor;

public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	CBrush	m_brush;
};


