// PlayContentsSearchFrame.cpp : implementation of the CPlayContentsSearchFrame class
//
#include "stdafx.h"
#include "UBCStudio.h"

#include "PlayContentsSearchFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPlayContentsSearchFrame

IMPLEMENT_DYNCREATE(CPlayContentsSearchFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPlayContentsSearchFrame, CMDIChildWnd)
	ON_WM_GETMINMAXINFO()
	ON_MESSAGE(WM_TEMPLATE_INFO_CHANGED, OnTemplateInfoChanged)
	ON_MESSAGE(WM_FRAME_INFO_CHANGED, OnFrameInfoChanged)
	ON_MESSAGE(WM_PLAY_TEMPLATE_LIST_CHANGED, OnPlayTemplateListChanged)
END_MESSAGE_MAP()


// CPlayContentsSearchFrame construction/destruction

CPlayContentsSearchFrame::CPlayContentsSearchFrame()
{
	// TODO: add member initialization code here
}

CPlayContentsSearchFrame::~CPlayContentsSearchFrame()
{
}


BOOL CPlayContentsSearchFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style = WS_CHILD | WS_VISIBLE | WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU
		| FWS_ADDTOTITLE | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;

	return TRUE;
}


// CPlayContentsSearchFrame diagnostics

#ifdef _DEBUG
void CPlayContentsSearchFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPlayContentsSearchFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// CPlayContentsSearchFrame message handlers

void CPlayContentsSearchFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);

//	lpMMI->ptMinTrackSize.x = 720;
//	lpMMI->ptMinTrackSize.y = 540;
}

LRESULT CPlayContentsSearchFrame::OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam)
{
	GetActiveView()->SendMessage(WM_TEMPLATE_INFO_CHANGED);

	return 0;
}

LRESULT CPlayContentsSearchFrame::OnFrameInfoChanged(WPARAM wParam, LPARAM lParam)
{
	GetActiveView()->SendMessage(WM_FRAME_INFO_CHANGED);

	return 0;
}

LRESULT CPlayContentsSearchFrame::OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam)
{
	GetActiveView()->SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED);

	return 0;
}
