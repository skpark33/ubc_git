// ImageSelectDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ImageSelectDlg.h"


// CImageSelectDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CImageSelectDlg, CWizardSubContents)

CImageSelectDlg::CImageSelectDlg(CWnd* pParent /*=NULL*/)
	: CWizardSubContents(CImageSelectDlg::IDD, pParent)
	, m_wndImage (NULL, 0, false)
	, m_reposControl(this)
{
}

CImageSelectDlg::~CImageSelectDlg()
{
}

void CImageSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CWizardSubContents::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CB_CONTENTS, m_cbContents);
	DDX_Control(pDX, IDC_FRAME_PREVIEW, m_framePreview);
}


BEGIN_MESSAGE_MAP(CImageSelectDlg, CWizardSubContents)
	ON_CBN_SELCHANGE(IDC_CB_CONTENTS, &CImageSelectDlg::OnCbnSelchangeCbContents)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CImageSelectDlg 메시지 처리기입니다.

void CImageSelectDlg::SetData(CString strContentsId, CString strValue)
{
	int nIndex = -1;
	CString strFullPath;
	for(int i=0; i<m_cbContents.GetCount() ;i++)
	{
		CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_cbContents.GetItemData(i);
		if(!pContentsInfo) continue;

		if(pContentsInfo->strId == strContentsId)
		{
			nIndex = i;
			strFullPath = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
			break;
		}
	}

	m_cbContents.SetCurSel(nIndex);
	LoadContents(strFullPath);
}

void CImageSelectDlg::GetData(CString& strContentsId, CString& strValue)
{
	strContentsId = "";
	strValue = "";

	if(m_cbContents.GetCurSel() < 0)
	{
		return;
	}

	CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_cbContents.GetItemData(m_cbContents.GetCurSel());
	if(pContentsInfo)
	{
		strContentsId = pContentsInfo->strId;
		strValue      = pContentsInfo->strFilename;
	}
}

void CImageSelectDlg::SetPreviewMode(bool bPreviewMode)
{
	CWizardSubContents::SetPreviewMode(bPreviewMode);
	m_cbContents.EnableWindow(!bPreviewMode);
}

BOOL CImageSelectDlg::OnInitDialog()
{
	CWizardSubContents::OnInitDialog();

	CRect rcPreview;
	m_framePreview.GetWindowRect(rcPreview);
	rcPreview.DeflateRect(2,2);
	ScreenToClient(rcPreview);

	m_wndImage.Create(NULL, "", WS_CHILD, rcPreview, this, 0xfeff);
	m_wndImage.ShowWindow(SW_SHOW);

	m_cbContents.ResetContent();

	CONTENTS_INFO_MAP* contents_map = GetDocument()->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(pContentsInfo && pContentsInfo->nContentsType == GetContentsType())
		{
			int nIndex = m_cbContents.AddString(pContentsInfo->strContentsName);
			m_cbContents.SetItemData(nIndex, (DWORD_PTR)pContentsInfo);
		}
	}

	m_reposControl.AddControl(&m_cbContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_wndImage  , REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_framePreview, REPOS_FIX , REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.Move();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CImageSelectDlg::OnCbnSelchangeCbContents()
{
	CString strFullPath;
	if(m_cbContents.GetCurSel() >= 0)
	{
		CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_cbContents.GetItemData(m_cbContents.GetCurSel());
		if(pContentsInfo)
		{
			strFullPath = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
		}
	}

	LoadContents(strFullPath);
}

bool CImageSelectDlg::LoadContents(LPCTSTR lpszFullPath)
{
	m_wndImage.CloseFile();

	if(!IsExistFile(lpszFullPath)) return false;

	m_wndImage.m_strMediaFullPath = lpszFullPath;
	if(m_wndImage.OpenFile(1))
	{
		int width;
		int height;

		if(m_wndImage.m_bAnimatedGif)
		{
			width = m_wndImage.m_bgGIF.GetSize().cx;
			height = m_wndImage.m_bgGIF.GetSize().cy;
		}
		else
		{
			width = m_wndImage.m_bgImage.GetWidth();
			height = m_wndImage.m_bgImage.GetHeight();
		}

		m_wndImage.Play();

		m_wndImage.m_nType = 2;
		m_wndImage.Invalidate();
	}

	return true;
}

void CImageSelectDlg::OnSize(UINT nType, int cx, int cy)
{
	CWizardSubContents::OnSize(nType, cx, cy);

	m_reposControl.Move();
}
