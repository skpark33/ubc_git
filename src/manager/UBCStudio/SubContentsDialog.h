#pragma once

#include "Schedule.h"
#include "DataContainer.h"
#include "UBCStudioDoc.h"

typedef struct
{
	LPTSTR			extension;
	CONTENTS_TYPE	type;
} EXT_TYPE;

extern EXT_TYPE ext_type[];

// CSubContentsDialog 대화 상자입니다.

class CSubContentsDialog : public CDialog
{
	DECLARE_DYNAMIC(CSubContentsDialog)

	CUBCStudioDoc* m_pDocument;

public:
	CSubContentsDialog(UINT nIDTemplate, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubContentsDialog();

// 대화 상자 데이터입니다.

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CBrush					m_brushBG;
	CArray<CWnd*, CWnd*>	m_listNoCTLWnd;
	ULONGLONG				m_ulFileSize;
	CString					m_strFileMD5;		// file 진위여부를 체크하기 위해 MD5를 사용한다.
	BOOL					m_bIsChangedFile;	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제

	static bool				IsValidType(CString szExt, CONTENTS_TYPE type=CONTENTS_NOT_DEFINE); 
	bool					IsFitContentsName(CString strContentsName, bool bErrMsg=true);
	bool					CheckTotalContentsSize(CString strFilePath);

public:
	void					SetDocument(CUBCStudioDoc* pDoc) { m_pDocument=pDoc; };
	CUBCStudioDoc*			GetDocument();
	BOOL					IsChangedFile() { return m_bIsChangedFile; }	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제

	virtual CONTENTS_TYPE	GetContentsType() = 0;
	virtual void			Stop() = 0;
	virtual bool			GetContentsInfo(CONTENTS_INFO& info) = 0;
	virtual bool			SetContentsInfo(CONTENTS_INFO& info) = 0;
	virtual void			SetPreviewMode(bool bPreviewMode) = 0;

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
