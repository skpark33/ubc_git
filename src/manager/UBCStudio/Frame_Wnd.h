#pragma once

#include "afxmt.h"

#define		FRAME_LEVEL_STEP		15

#define		RGB_BRIGHT				0x3f
#define		RGB_DARK				0x00

#define		FRAME_OUT_BACKGROUND_COLOR		RGB(RGB_BRIGHT, RGB_BRIGHT, RGB_BRIGHT)
#define		FRAME_IN_BACKGROUND_COLOR		RGB(RGB_DARK, RGB_DARK, RGB_DARK)

#define		FRAME_PRIMARY_COLOR				RGB(254, 233, 177)
#define		FRAME_PRIMARY_SELECT_COLOR		RGB(253, 187, 13)
#define		FRAME_PRIMARY_DROP_COLOR		RGB(202, 147, 2)

class CUBCStudioDoc;

// CFrame_Wnd

class CFrame_Wnd : public CWnd
{
	DECLARE_DYNAMIC(CFrame_Wnd)
public:
	enum WND_TYPE { 
		WND_LAYOUT = 0,
		WND_PACKAGE,
	};

	enum MOUSE_TYPE { 
		//MOUSE_IDLE = -1,
		MOUSE_IDLE = 0,
		MOUSE_OVER_LEFT_TOP,
		MOUSE_OVER_TOP,
		MOUSE_OVER_RIGHT_TOP,
		MOUSE_OVER_LEFT,
		MOUSE_OVER_CENTER,
		MOUSE_OVER_RIGHT,
		MOUSE_OVER_LEFT_BOTTOM,
		MOUSE_OVER_BOTTOM,
		MOUSE_OVER_RIGHT_BOTTOM,
		MOUSE_SIZING_LEFT_TOP,
		MOUSE_SIZING_TOP,
		MOUSE_SIZING_RIGHT_TOP,
		MOUSE_SIZING_LEFT,
		MOUSE_SIZING_CENTER,
		MOUSE_SIZING_RIGHT,
		MOUSE_SIZING_LEFT_BOTTOM,
		MOUSE_SIZING_BOTTOM,
		MOUSE_SIZING_RIGHT_BOTTOM,
	};

	enum ALIGN_TO_FRAME
	{
		ALIGN_CENTER_VERTICAL   = 0x0001
	,	ALIGN_CENTER_HORIZONTAL = 0x0002
	,   ALIGN_CENTER_BOTH       = ALIGN_CENTER_VERTICAL | ALIGN_CENTER_HORIZONTAL
	,   ALIGN_LEFT              = 0x0010
	,   ALIGN_RIGHT             = 0x0020
	,   ALIGN_TOP               = 0x0100
	,   ALIGN_BOTTOM            = 0x0200
	,   ALIGN_LEFT_TOP          = ALIGN_LEFT | ALIGN_TOP
	,   ALIGN_RIGHT_BOTTOM      = ALIGN_RIGHT | ALIGN_BOTTOM
	,   ALIGN_RIGHT_TOP         = ALIGN_RIGHT | ALIGN_TOP
	,   ALIGN_LEFT_BOTTOM       = ALIGN_LEFT | ALIGN_BOTTOM
	};

	CFrame_Wnd(int nIndex, WND_TYPE, bool bEditable=true);
	virtual ~CFrame_Wnd();

	void ReleaseCapturedFrame();
protected:
	COLORREF	FRAME_SELECT_COLOR;
	COLORREF	FRAME_SELECT_BG_COLOR;
	COLORREF	FRAME_SELECT_DROP_COLOR;
	COLORREF	BOX_COLOR;
	COLORREF	BOX_BG_COLOR;
	WND_TYPE	m_wndType;

	CString		m_strSelectTemplateId;
	CString		m_strSelectFrameId;
	CString		m_strDropFrameId;		// 드래그 & 드랍 될 프레임ID

	MOUSE_TYPE	m_MouseType;
	CPoint		m_pointStart;
	CRect		m_rectStart;
	CFont		m_font;

	bool		m_bEditable;
	CUBCStudioDoc*	m_pDocument;
	CWnd*			m_pFocusWindow;

	double		m_fScale;

	int			m_nIndex;
	int			m_nMagneticFrame;

	bool		m_bPasteReady;
	FRAME_INFO	m_stPasteReady;

	void		HitTest(CPoint& point);
	CPoint      GetPointCorrection(MOUSE_TYPE mouseType, CPoint ptCurrent, CRect rcFrame, CRect rcTemplate);
	CRect		GetNewFrameInfoRect(bool bShiftKey, MOUSE_TYPE mouseType, CPoint ptCurrent, CPoint ptStart, CRect rcFrame, CRect rcTemplate);
	CRect       GetInfoToRect(CRect rcInfo);
	CRect		GetMagneticRect(MOUSE_TYPE mouseType, CString strInFrameId, CRect rcNewFrame, CRect rcFrame, bool bFixedSize, bool bShiftKey);
	BOOL		SetCursor(MOUSE_TYPE mouseType);

	void		DrawBackground(CDC* pDC);
	void		DrawTemplate(CDC* pDC, TEMPLATE_LINK* pTemplateInfo);
	void		DrawFrame(CDC* pDC, TEMPLATE_LINK* pTemplateInfo);
	void		DrawInformation(CDC* pDC);
	void		DrawDebugInfo(CDC* pDC);
	DWORD		GetFrmRunningTime(FRAME_INFO*);
	CString		GetTimeStr(UINT);
	CPoint		GetClientToTemplate(CPoint ptPoint);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	LPCTSTR		GetDropedFrameId() { return m_strDropFrameId; };
	void		SetDocument(CUBCStudioDoc* pDocument) { m_pDocument = pDocument; };
	void		RecalcLayout();

	FRAME_INFO*	CreateNewFrame();
	bool		DeleteFrame();

	bool		MovePrevFrame();
	bool		MoveNextFrame();

	bool		HitTest2(CPoint& point); // 드래그 드랍시 이용

	void		SetMagneticFrame(int nMagneticFrame);
	int			GetMagneticFrame();

	void		AlignToFrame(ALIGN_TO_FRAME nAlign, CString strInFrameId =_T(""));
	void		MoveFrame(int nXoffset, int nYoffset, CString strInFrameId =_T(""));

	bool		CopyFrameReady(CString strInFrameId =_T(""));
	bool		PasteFrame();
};