// PlayContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "PlayContentsDialog.h"
#include "UBCStudioDoc.h"
#include "SubContentsDialog.h"
#include "DataContainer.h"
#include "SelectContentsDialog.h"
#include "ContentsListCtrl.h"

// CPlayContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPlayContentsDialog, CSubPlayContentsDialog)

CPlayContentsDialog::CPlayContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubPlayContentsDialog(CPlayContentsDialog::IDD, pParent)
	, m_bEditContents (false)
	, m_bCyclePlayContents(true)
{
	// Modified by 정운형 2008-12-26 오후 1:56
	// 변경내역 :  캡션기능 추가 - 버그수정
	m_pContentsListCtrl = NULL;
	// Modified by 정운형 2008-12-26 오후 1:56
	// 변경내역 :  캡션기능 추가 - 버그수정
}

CPlayContentsDialog::~CPlayContentsDialog()
{
	RemoveAllPlayContentsList();
}

void CPlayContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubPlayContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_CONTENTS_TIME, m_tabContentsTime);
	DDX_Control(pDX, IDC_TAB_CONTENTS, m_tabContents);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_TYPE, m_staticContentsType);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_NAME, m_staticContentsName);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_RUNNING_TIME, m_staticContentsRunningTime);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_FILE_NAME, m_staticContentsFileName);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BUTTON_SELECT_CONTENTS, m_btnSelectContents);
	DDX_Control(pDX, IDC_BUTTON_EDIT_CONTENTS, m_btnEditContents);
}


BEGIN_MESSAGE_MAP(CPlayContentsDialog, CSubPlayContentsDialog)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_CONTENTS_TIME, &CPlayContentsDialog::OnTcnSelchangeTabContentsTime)
	ON_BN_CLICKED(IDC_BUTTON_EDIT_CONTENTS, &CPlayContentsDialog::OnBnClickedButtonEditContents)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_CONTENTS, &CPlayContentsDialog::OnBnClickedButtonSelectContents)
END_MESSAGE_MAP()


// CPlayContentsDialog 메시지 처리기입니다.

BOOL CPlayContentsDialog::OnInitDialog()
{
	CSubPlayContentsDialog::OnInitDialog();

	CWaitMessageBox wait;

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	HICON hIcon = LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_PACKAGE));
	SetIcon(hIcon, TRUE);
	SetIcon(hIcon, FALSE);

	m_btnSelectContents.LoadBitmap(IDB_BTN_SELECT, RGB(255, 255, 255));
	m_btnEditContents.LoadBitmap(IDB_BTN_EDIT_CONTENTS, RGB(255, 255, 255));
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));

	m_btnSelectContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSDIALOG_BUT001));
	m_btnEditContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSDIALOG_BUT002));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSDIALOG_BUT003));

	m_tabContentsTime.InitImageListHighColor(IDB_TAB_LIST, RGB(192,192,192));
	m_tabContents.InitImageListHighColor(IDB_TAB_CONTENTS, RGB(192,192,192));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	if(m_bEditContents)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = m_listPlayContentsInfo.GetAt(0);
		m_bCyclePlayContents = pPlayContentsInfo->bIsDefaultPlayContents ? true : false;
	}//if

	//
	int index = 0;
	/*m_tabContentsTime.InsertItem(index++, "Cycle");
	m_tabContentsTime.InsertItem(index++, "Time Base");*/
	if(m_bCyclePlayContents)
	{
		m_tabContentsTime.InsertItem(index++, "Cycle", 0);
	}
	else
	{
		m_tabContentsTime.InsertItem(index++, "Time Base", 1);
	}//if

	CRect time_rect;
	m_tabContentsTime.GetWindowRect(time_rect);
	ScreenToClient(time_rect);
	time_rect.DeflateRect(5,25,5,5);

	TCITEM item;
	item.mask = TCIF_PARAM;

	if(m_bCyclePlayContents)
	{
		m_wndCyclePlayContents.Create(IDD_CYCLE_PLAYCONTENTS, this);
		m_wndCyclePlayContents.MoveWindow(time_rect);
		m_wndCyclePlayContents.ShowWindow(SW_SHOW);

		item.lParam = (LPARAM)(CSubPlayContentsDialog*)&m_wndCyclePlayContents;
		m_tabContentsTime.SetItem(0, &item);
	}
	else
	{
		m_wndTimeBasePlayContents.Create(IDD_TIME_BASE_PLAYCONTENTS, this);
		m_wndTimeBasePlayContents.MoveWindow(time_rect);
		//m_wndTimeBasePlayContents.ShowWindow(SW_HIDE);
		m_wndTimeBasePlayContents.ShowWindow(SW_SHOW);

		item.lParam = (LPARAM)(CSubPlayContentsDialog*)&m_wndTimeBasePlayContents;
		//m_tabContentsTime.SetItem(1, &item);
		m_tabContentsTime.SetItem(0, &item);
	}//if

	//
	m_tabContents.InsertItem(0, "Contents", 0);

	m_listNoCTLWnd.Add(&m_staticContentsType);
	m_listNoCTLWnd.Add(&m_staticContentsName);
	m_listNoCTLWnd.Add(&m_staticContentsRunningTime);
	m_listNoCTLWnd.Add(&m_staticContentsFileName);
	m_listNoCTLWnd.Add(this);

	// 기존 콘텐츠 패키지인 경우 수정모드로
	if(m_bEditContents)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = m_listPlayContentsInfo.GetAt(0);

		m_btnOK.SetWindowText(LoadStringById(IDS_PLAYCONTENTSDIALOG_BUT004));
		m_btnOK.LoadBitmap(IDB_BTN_MODIFY, RGB(255, 255, 255));
		m_btnOK.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSDIALOG_BUT004));

		//m_tabContentsTime.SetCurSel(pPlayContentsInfo->bIsDefaultPlayContents ? 0 : 1);
		m_tabContentsTime.SetCurSel(0);

		LRESULT result;
		OnTcnSelchangeTabContentsTime(NULL, &result);

		if(m_bCyclePlayContents)
		{
			m_wndCyclePlayContents.SetPlayContentsInfo(*pPlayContentsInfo);
		}
		else
		{
			m_wndTimeBasePlayContents.SetPlayContentsInfo(*pPlayContentsInfo);
		}//if

		RefreshContentsInfo();
	}
	else
	{
		m_btnOK.SetWindowText(LoadStringById(IDS_PLAYCONTENTSDIALOG_BUT005));
		m_btnOK.LoadBitmap(IDB_BTN_ADD, RGB(255, 255, 255));
		m_btnOK.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSDIALOG_BUT005));

		//m_tabContentsTime.SetCurSel(m_bCyclePlayContents ? 0 : 1);
		m_tabContentsTime.SetCurSel(0);

		LRESULT result;
		OnTcnSelchangeTabContentsTime(NULL, &result);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

bool CPlayContentsDialog::InputErrorCheck()
{
	TCITEM item;
	item.mask = TCIF_PARAM;

	if(m_tabContentsTime.GetItem(m_tabContentsTime.GetCurSel(), &item))
	{
		CSubPlayContentsDialog* pSubPlayContents = (CSubPlayContentsDialog*)item.lParam;

		if(!pSubPlayContents->InputErrorCheck()) return false;
	}

	return true;
}

void CPlayContentsDialog::OnOK()
{
	if(!InputErrorCheck()) return;

	TCITEM item;
	item.mask = TCIF_PARAM;

	if(m_tabContentsTime.GetItem(m_tabContentsTime.GetCurSel(), &item))
	{
		CSubPlayContentsDialog* pSubPlayContents = (CSubPlayContentsDialog*)item.lParam;

		RemoveAllPlayContentsList();
		int count = pSubPlayContents->GetPlayContentsCount();
		for(int i=0; i<count || i==0; i++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
			if(pSubPlayContents->GetPlayContentsInfo(*pPlayContentsInfo, i))
			{
				pPlayContentsInfo->strContentsId = m_strContentsId;

				m_listPlayContentsInfo.Add(pPlayContentsInfo);
				
				if( pPlayContentsInfo->strContentsId.GetLength() == 0 ||
					m_pDocument->GetContents(pPlayContentsInfo->strContentsId) == NULL)
				{
					UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSDIALOG_MSG001), MB_ICONSTOP);
					return;
				}
				CSubPlayContentsDialog::OnOK();
			}
			else
			{
				delete pPlayContentsInfo;
			}
		}
	}
}

void CPlayContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CSubPlayContentsDialog::OnCancel();
}

void CPlayContentsDialog::OnTcnSelchangeTabContentsTime(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(m_bCyclePlayContents)
	{
		m_wndCyclePlayContents.ShowWindow(SW_SHOW);
	}
	else
	{
		m_wndTimeBasePlayContents.ShowWindow(SW_SHOW);
	}//if

/*
	int sel = m_tabContentsTime.GetCurSel();

	if(sel == 0)
	{
		m_wndCyclePlayContents.ShowWindow(SW_SHOW);
		m_wndTimeBasePlayContents.ShowWindow(SW_HIDE);
	}
	else
	{
		m_wndCyclePlayContents.ShowWindow(SW_HIDE);
		m_wndTimeBasePlayContents.ShowWindow(SW_SHOW);
	}
*/
}

void CPlayContentsDialog::OnBnClickedButtonSelectContents()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CSelectContentsDialog dlg;
	dlg.SetDocuments(m_pDocument);
	if(dlg.DoModal() == IDOK)
	{
		m_strContentsId = dlg.m_pContentsInfo->strId;
		RefreshContentsInfo();

		CONTENTS_INFO* info = m_pDocument->GetContents(m_strContentsId);

		if(IsWindow(m_wndTimeBasePlayContents.GetSafeHwnd()))
			m_wndTimeBasePlayContents.SetRunningTime(info->nRunningTime);
	}
}

void CPlayContentsDialog::OnBnClickedButtonEditContents()
{
	CONTENTS_INFO* info = m_pDocument->GetContents(m_strContentsId);
	if(info == NULL)
	{
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSDIALOG_MSG001), MB_ICONINFORMATION);
	}
	else
	{
		if( (m_bEditContents && m_pDocument->GetContentsUsingCount(m_strContentsId) > 1) ||
			(!m_bEditContents && m_pDocument->GetContentsUsingCount(m_strContentsId) > 0) )
		{
			int ret_value = UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSDIALOG_MSG002), MB_ICONWARNING | MB_YESNO);
			if(ret_value == IDNO)
				return;
		}

		// Modified by 정운형 2008-12-26 오후 1:56
		// 변경내역 :  캡션기능 추가 - 버그수정
		//m_pContentsListCtrl->ModifyContents(info);
		if(m_pContentsListCtrl)
		{
			m_pContentsListCtrl->ModifyContents(info);
		}//if
		// Modified by 정운형 2008-12-26 오후 1:56
		// 변경내역 :  캡션기능 추가 - 버그수정
		RefreshContentsInfo();
	}
}

bool CPlayContentsDialog::GetPlayContentsInfo(PLAYCONTENTS_INFO& info, int nIndex)
{
	PLAYCONTENTS_INFO* pPlayContentsInfo = m_listPlayContentsInfo.GetAt(nIndex);
	info = *pPlayContentsInfo;
	return false;
}

bool CPlayContentsDialog::SetPlayContentsInfo(PLAYCONTENTS_INFO& info)
{ 
	RemoveAllPlayContentsList();
	PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
	*pPlayContentsInfo = info;

	m_listPlayContentsInfo.Add(pPlayContentsInfo);
	m_strContentsId = info.strContentsId;
	return false;
}

int CPlayContentsDialog::GetPlayContentsCount()
{
	return m_listPlayContentsInfo.GetCount();
}

bool CPlayContentsDialog::RefreshContentsInfo()
{
	CONTENTS_INFO* info = m_pDocument->GetContents(m_strContentsId);
	if(info != NULL)
	{
		CString space = " ";

		m_staticContentsType.SetWindowText(space + CONTENTS_TYPE_TO_STRING[info->nContentsType+1]);
		m_staticContentsName.SetWindowText(space + info->strContentsName);
		m_staticContentsFileName.SetWindowText(space + info->strFilename);

		if(CONTENTS_PPT == info->nContentsType){
			m_staticContentsRunningTime.SetWindowText("");
		}else{
			CString tm;
			tm.Format(" %02ld:%02ld", info->nRunningTime/60, info->nRunningTime % 60);
			m_staticContentsRunningTime.SetWindowText(tm);
		}

		//OnTime 은 종료 시간을 설정해 준다
//		if(!m_bCyclePlayContents)
//		{
//			m_wndTimeBasePlayContents.SetRunningTime(info->nRunningTime);
//		}//if

		return true;
	}
	else
	{
		m_staticContentsType.SetWindowText("No Contents");
		m_staticContentsName.SetWindowText("");
		m_staticContentsFileName.SetWindowText("");
		m_staticContentsRunningTime.SetWindowText("");
	}
	return false;
}

void CPlayContentsDialog::RemoveAllPlayContentsList()
{
	for(int i=0; i<m_listPlayContentsInfo.GetCount(); i++)
	{
		PLAYCONTENTS_INFO* info = m_listPlayContentsInfo.GetAt(i);
		delete info;
	}
	m_listPlayContentsInfo.RemoveAll();
}
