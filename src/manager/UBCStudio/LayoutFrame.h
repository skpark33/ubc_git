// LayoutFrame.h : interface of the CLayoutFrame class
//


#pragma once


class CLayoutFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CLayoutFrame)
public:
	CLayoutFrame();

// Attributes
public:

// Operations
public:

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

// Implementation
public:
	virtual ~CLayoutFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg LRESULT	OnTemplateListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnConfigSaveComplete(WPARAM wParam, LPARAM lParam);	///<complete config save message handler
	afx_msg	LRESULT OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam);

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};
