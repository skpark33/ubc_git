// CheckImageSizeDlg.cpp : implementation file
//
#include "stdafx.h"
#include "CheckImageSizeDlg.h"

// CCheckImageSizeDlg dialog
IMPLEMENT_DYNAMIC(CCheckImageSizeDlg, CDialog)

CCheckImageSizeDlg::CCheckImageSizeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCheckImageSizeDlg::IDD, pParent)
{
}

CCheckImageSizeDlg::~CCheckImageSizeDlg()
{
}

void CCheckImageSizeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCheckImageSizeDlg, CDialog)
	ON_BN_CLICKED(IDCANCEL, &CCheckImageSizeDlg::OnBnClickedCancel)
END_MESSAGE_MAP()

// CCheckImageSizeDlg message handlers
BOOL CCheckImageSizeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CCheckImageSizeDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
