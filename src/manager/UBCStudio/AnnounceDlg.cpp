// AnnounceDlg.cpp : implementation file
//

#include "stdafx.h"
#include <algorithm>

#include "UBCStudio.h"
#include "AnnounceDlg.h"
#include "Enviroment.h"
#include "common/UbcGUID.h"
#include "common\libscratch\scratchUtil.h"
#include "common/libProfileManager/ProfileManager.h"

// CAnnounceDlg dialog
IMPLEMENT_DYNAMIC(CAnnounceDlg, CDialog)

CAnnounceDlg::CAnnounceDlg(CWnd* pParent /*=NULL*/)	: CDialog(CAnnounceDlg::IDD, pParent)
,	m_cbFont(IDB_TTF_BMP)
,	m_wndTicker (NULL, 0, false)
{
}

CAnnounceDlg::~CAnnounceDlg()
{
}

void CAnnounceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_TITLE, m_szTitle);
	DDX_Text(pDX, IDC_EDIT_FILE, m_szFile);
	DDX_Text(pDX, IDC_EDIT_HEIGHT, m_szHeight);
	DDX_Text(pDX, IDC_EDIT_FONTSIZE, m_szFontSize);
	DDX_Text(pDX, IDC_EDIT_ALPA, m_szAlpa);
	DDX_Text(pDX, IDC_EDIT_COMMENT, m_szComment);

	DDX_Control(pDX, IDC_START_DATE, m_dtcStartDateTime);
	DDX_Control(pDX, IDC_END_DATE, m_dtcEndDateTime);
	DDX_Control(pDX, IDC_COMBO_POSITION, m_cbPosition);
	DDX_Control(pDX, IDC_COMBO_FONT, m_cbFont);
	DDX_Control(pDX, IDC_COMBO_FGCOLOR, m_cbFGColor);
	DDX_Control(pDX, IDC_COMBO_BGCOLOR, m_cbBGColor);
	DDX_Control(pDX, IDC_SLIDER_SPEED, m_sldSpeed);
	DDX_Control(pDX, IDC_SPIN_FONTSIZE, m_spFontSize);
	DDX_Control(pDX, IDC_SPIN_HEIGHT, m_spHeight);
	DDX_Control(pDX, IDC_SPIN_ALPA, m_spAlpa);

	DDX_Control(pDX, IDC_BUTTON_FILE, m_btFile);
	DDX_Control(pDX, IDC_BUTTON_PLAY, m_btPlay);
	DDX_Control(pDX, IDC_BUTTON_STOP, m_btStop);
}

BEGIN_MESSAGE_MAP(CAnnounceDlg, CDialog)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_EN_CHANGE(IDC_EDIT_COMMENT, OnEnChangeEditComment)
	ON_BN_CLICKED(IDC_BUTTON_FILE, &CAnnounceDlg::OnBnClickedButtonFile)
	ON_BN_CLICKED(IDC_BUTTON_PLAY, &CAnnounceDlg::OnBnClickedButtonPlay)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CAnnounceDlg::OnBnClickedButtonStop)
END_MESSAGE_MAP()

// CAnnounceDlg message handlers
BOOL CAnnounceDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_spHeight.SetRange32(0, 100);
	m_spFontSize.SetRange32(0, 100);
	m_spAlpa.SetRange32(0, 100);
	m_sldSpeed.SetRange(1, 5);
	m_sldSpeed.SetPos(3);

	m_cbPosition.AddString("0.Bottom");
	m_cbPosition.AddString("1.Top");
	m_cbPosition.AddString("2.Middle");

	m_btFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btPlay.LoadBitmap(IDB_BTN_PLAY, RGB(236, 233, 216));
	m_btStop.LoadBitmap(IDB_BTN_STOP, RGB(236, 233, 216));

	m_cbFGColor.InitializeDefaultColors();
	m_cbBGColor.InitializeDefaultColors();

	m_cbFGColor.SetSelectedColorValue(GetColorFromString("#FFFFFF"));
	m_cbBGColor.SetSelectedColorValue(GetColorFromString("#000000"));

	m_cbFont.SetPreviewStyle (CFontPreviewCombo::NAME_ONLY, false);
	m_cbFont.SetFontHeight (12, true);

	int nFindFont = -1;
	int nSystemFont = -1;
	for(int i=0; i<m_cbFont.GetCount(); i++)
	{
		CString font_name;
		m_cbFont.GetLBText(i,font_name);

		if(font_name == LoadStringById(IDS_ANNOUNCEDLG_FONT01))
		{
			nSystemFont = i;
		}
		if(font_name == _T("System"))
		{
			nFindFont = i;
			break;
		}
	}

	if(nFindFont >= 0) m_cbFont.SetCurSel(nFindFont);
	else               m_cbFont.SetCurSel(nSystemFont);

	m_szTitle = _T("");
	m_szFile = _T("");
	m_szHeight = _T("100");
	m_szFontSize = _T("24");
	m_szAlpa = _T("0");
	m_cbPosition.SetCurSel(0);

	m_sldSpeed.SetPos(E_TICKET_SLOW - E_TICKER_NORMAL + 1);

	m_dtcStartDateTime.SetFormat("yyyy/MM/dd tt HH:mm");
	m_dtcEndDateTime.SetFormat("yyyy/MM/dd tt HH:mm");

	CTime tmCurTime = CTime::GetCurrentTime();
	m_dtcStartDateTime.GetTime(tmCurTime);

	CTime tmEndTime = tmCurTime + CTimeSpan(1,0,0,0);
	m_dtcEndDateTime.SetTime(&tmEndTime);
	InitDateRange(m_dtcEndDateTime);

	UpdateData(FALSE);

	CRect client_rect;
	GetDlgItem(IDC_STATIC_PREVIEW)->GetWindowRect(client_rect);
	ScreenToClient(client_rect);
	client_rect.DeflateRect(1,1);

	m_wndTicker.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndTicker.ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

int CAnnounceDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	return 0;
}

void CAnnounceDlg::OnDestroy()
{
	CDialog::OnDestroy();
}

void CAnnounceDlg::OnClose()
{
	CDialog::OnClose();
}

void CAnnounceDlg::OnEnChangeEditComment()
{
	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_COMMENT);

	DWORD dwSel = pContents->GetSel();
	int nScroll = pContents->GetScrollPos(SB_HORZ);

	CString strContents;
	for(int i=0; i<pContents->GetLineCount() ;i++)
	{
		if(i!=0) strContents += "\r\n";

		char szBuf[255+1] = {0};
		pContents->GetLine(i, szBuf, 255);

		strContents += szBuf;
	}

	pContents->SetWindowText(strContents);
	pContents->SetSel(dwSel, FALSE);
}

void CAnnounceDlg::OnBnClickedOk()
{
	CString szMsg;
	UpdateData();

	if(m_szComment.IsEmpty())
	{
		szMsg = LoadStringById(IDS_ANNOUNCEDLG_MSG001);
		MessageBox(szMsg, m_szTitle, MB_ICONINFORMATION);
		return;
	}

	CTime tmCur = CTime::GetCurrentTime();
	CTime tmStart, tmEnd;

	m_dtcStartDateTime.GetTime(tmStart);
	m_dtcEndDateTime.GetTime(tmEnd);

	if(tmEnd <= tmStart)
	{
		szMsg = LoadStringById(IDS_ANNOUNCEDLG_MSG002);
		MessageBox(szMsg, m_szTitle, MB_ICONINFORMATION);
		return;
	}

	CString strFont;
	m_cbFont.GetLBText(m_cbFont.GetCurSel(), strFont);

	CTime tmStartTime;
	CTime tmEndTime;

	m_dtcStartDateTime.GetTime(tmStartTime);
	m_dtcEndDateTime.GetTime(tmEndTime);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, _T("UBCAnnounce.ini"));

	CProfileManager objIniManager(szPath);

	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("lock"), _T("0"));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("isStart"), _T("1"));
	//objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("announceId"), CUbcGUID::GetInstance()->ToGUID(_T("")));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("announceId"), _T("local"));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("mgrId"), _T("1"));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("siteId"), GetEnvPtr()->m_szSite);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("title"), m_szTitle);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("serialNo"), _T("0"));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("creator"), GetEnvPtr()->m_szLoginID);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("createTime"), _T("1311214524"));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("hostIdList"), _T(""));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("startTime"), ToString((ULONGLONG)tmStartTime.GetTime()));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("endTime"), ToString((ULONGLONG)tmEndTime.GetTime()));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("position"), ToString(m_cbPosition.GetCurSel()));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("height"), ToString(m_szHeight.IsEmpty()?100:atoi(m_szHeight)));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("font"), strFont);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("fontSize"), ToString(m_szFontSize.IsEmpty()?24:atoi(m_szFontSize)));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("fgColor"), GetColorFromString(m_cbFGColor.GetSelectedColorValue()));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("bgColor"), GetColorFromString(m_cbBGColor.GetSelectedColorValue()));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("bglocation"), _T(""));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("bgFile"), m_szFile);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("playSpeed"), ToString(E_TICKET_SLOW - m_sldSpeed.GetPos() + 1));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("alpha"), ToString(m_szAlpa.IsEmpty()?0:atoi(m_szAlpa)));

	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_COMMENT);
	for(int i=0 ; i<TICKER_COUNT ; i++)
	{
		CString strValue;
		if( i < pContents->GetLineCount() )
		{
			char szLine[255+1] = {0};
			pContents->GetLine(i, szLine, 255);
			strValue = szLine;
		}

		CString strKey;
		strKey.Format(_T("comment%d"), i+1);
		objIniManager.WriteProfileString(_T("ANNOUNCE"), strKey, strValue);
	}

	objIniManager.Write();

	// UBC��۰�ȹExporter ȣ��
	TerminateProcess(_T("UBCImporter.exe"));

	CString szArg;
	//szArg.Format(_T(" +self +drive %s +bpi %s"
	//szArg.Format(_T(" +export +drive %s +announce %s")
	//			, "all" //GetEnvPtr()->m_szDrive
	//			, _T("UBCAnnounce.ini")
	//			);
	szArg = _T(" +export +announce");

	//szArg += " +display ";
	//if(nA){
	//szArg += " 0";
	//if(nB) szArg += "1";
	//}
	//else if(nB){
	//szArg += " 1";
	//}

	CString strCommand;
	strCommand.Format(_T("%s\\%s%s")
					, GetEnvPtr()->m_szDrive
					, UBC_EXECUTE_PATH
					, _T("UBCImporter.exe")
					);

	scratchUtil::getInstance()->createProcess(strCommand.GetBuffer(0), szArg.GetBuffer(0), 0, FALSE);

	OnOK();
}

void CAnnounceDlg::OnBnClickedButtonFile()
{
	HKEY hKey;
	DWORD dwType = REG_SZ;
	DWORD dwSize = 256;
	CString strFullpath;
	char szPersonalPath[256] = { 0x00 };
	CString strRootKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders";
	LONG lResult = RegOpenKeyEx(HKEY_CURRENT_USER, strRootKey, 0, KEY_READ, &hKey);
	
	if(lResult == ERROR_SUCCESS){
		lResult = RegQueryValueEx(hKey, "My Pictures", NULL, &dwType, (LPBYTE)&szPersonalPath, &dwSize);
		if(lResult == ERROR_SUCCESS){
			strFullpath = szPersonalPath;
			strFullpath.Append("\\Image Files");
		}else{
			strFullpath = "C:\\Image Files";
		}
	}else{
		strFullpath = "C:\\Image Files";
	}

	CString filter = "All Image Files (*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff)|*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff|All Files (*.*)|*.*||";
	
	CFileDialog dlg(TRUE, NULL, strFullpath, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, filter, this);
	if(dlg.DoModal() == IDOK)
	{
		m_szFile = dlg.GetFileName();
		CString szSPathName = dlg.GetPathName();
		CString szTPathName;
		
		szTPathName.Format("%s\\%s%s\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER);
		::CreateDirectory(szTPathName, NULL);

		szTPathName += m_szFile;
		::CopyFile(szSPathName, szTPathName, false);

		GetDlgItem(IDC_EDIT_FILE)->SetWindowText(m_szFile);

		m_imBack.Destroy();

		CFileStatus fs;
		if(CFile::GetStatus(szTPathName, fs)){
			if(m_imBack.Load(szTPathName)){
				int width = m_imBack.GetWidth();
				int height = m_imBack.GetHeight();
			}
		}

		m_wndTicker.CloseFile();
		if(m_szFile.IsEmpty())
			m_wndTicker.m_strMediaFullPath = "";
		else
			m_wndTicker.m_strMediaFullPath.Format("%s\\%s%s\\%s", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER, m_szFile);
		m_wndTicker.Invalidate();
	}
}

void CAnnounceDlg::OnBnClickedButtonPlay()
{
	InitPreview();
	EnableAllItems(FALSE);
	m_wndTicker.Play();
}

void CAnnounceDlg::OnBnClickedButtonStop()
{
	EnableAllItems(TRUE);
	m_wndTicker.Stop(false);
}

void CAnnounceDlg::InitPreview()
{
	UpdateData();
	m_wndTicker.CloseFile();

	if(m_szFile.IsEmpty())
		m_wndTicker.m_strMediaFullPath = "";
	else
		m_wndTicker.m_strMediaFullPath.Format("%s\\%s%s\\%s", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER, m_szFile);

	CString str_font;
	m_cbFont.GetLBText(m_cbFont.GetCurSel(), str_font);

	COLORREF text_color = m_cbFGColor.GetSelectedColorValue();
	COLORREF bg_color = m_cbBGColor.GetSelectedColorValue();

	m_wndTicker.m_font = str_font;
	m_wndTicker.m_fontSize = m_szFontSize.IsEmpty()?1:atoi(m_szFontSize);
	m_wndTicker.m_fgColor = ::GetColorFromString(text_color);
	m_wndTicker.m_bgColor = ::GetColorFromString(bg_color);
	m_wndTicker.m_rgbFgColor = text_color;
	m_wndTicker.m_rgbBgColor = bg_color;
	m_wndTicker.m_playSpeed = E_TICKET_SLOW - m_sldSpeed.GetPos() + 1;

	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_COMMENT);
	for(int i=0 ; i<TICKER_COUNT ; i++)
	{
		CString strValue;
		if( i < pContents->GetLineCount() )
		{
			char szLine[255+1] = {0};
			pContents->GetLine(i, szLine, 255);
			strValue = szLine;
		}

		m_wndTicker.m_comment[i] = strValue;
	}

	m_wndTicker.CreateFile();
	m_wndTicker.OpenFile(0);
}

void CAnnounceDlg::EnableAllItems(BOOL bEnable)
{
	GetDlgItem(IDC_EDIT_TITLE)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_FILE)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_HEIGHT)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_FONTSIZE)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_ALPA)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_COMMENT)->EnableWindow(bEnable);
	GetDlgItem(IDOK)->EnableWindow(bEnable);
	GetDlgItem(IDCANCEL)->EnableWindow(bEnable);

	m_dtcStartDateTime.EnableWindow(bEnable);
	m_dtcEndDateTime.EnableWindow(bEnable);
	m_cbPosition.EnableWindow(bEnable);
	m_cbFont.EnableWindow(bEnable);
	m_cbFGColor.EnableWindow(bEnable);
	m_cbBGColor.EnableWindow(bEnable);
	m_sldSpeed.EnableWindow(bEnable);
	m_spFontSize.EnableWindow(bEnable);
	m_spHeight.EnableWindow(bEnable);
	m_spAlpa.EnableWindow(bEnable);

	m_btFile.EnableWindow(bEnable);
//	m_btPlay.EnableWindow(bEnable);
//	m_btStop.EnableWindow(bEnable);
}
