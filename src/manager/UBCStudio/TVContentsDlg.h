#pragma once

#include "EditEx.h"
#include "SubContentsDialog.h"
#include "BitSlider.h"

class CTVContentsDlg : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CTVContentsDlg)
public:

	//+ 아래 enum 은 플레이어에서 TV SDK에 정의 되어있는것을 그대로 가져옴
	enum TV_MODE
	{
//		modeNone    = 0,
		modeAnalogAir  ,
		modeAnalogCatv ,
		modeDigitalAir ,
		modeDigitalCatv,
		modeComposite  ,
		modeSvhs			// S-VHS 외부입력
	};
	//-

	CTVContentsDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTVContentsDlg();
	virtual BOOL OnInitDialog();

	virtual CONTENTS_TYPE	GetContentsType() { return CONTENTS_TV; }
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);
	virtual void	SetPreviewMode(bool bPreviewMode)  { m_bPreviewMode = bPreviewMode; }

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TV_CONTENTS };
protected:
	CEditEx		m_editContentsName;
	CComboBox	m_cbTvSource;
	CEditEx		m_edtTvChannel;
	CEditEx		m_editContentsPlayMinute;
	CEditEx		m_editContentsPlaySecond;
	BOOL		m_bPermanent;		///<24시간 running time
	CEditEx		m_editContentsVolume;
	CBitSlider	m_sliderContentsVolume;
	CEdit		m_editFelicaUrl;

	bool		m_bPreviewMode;
	void		EnableAllControls(BOOL bEnable);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedPermanentCheck();
	afx_msg void OnEnChangeEditContentsVolume();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};