// UBCStudioDoc.cpp : implementation of the CUBCStudioDoc class
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "UBCStudioDoc.h"
#include "DriveSelectDialog.h"
#include "FileCopyDialog.h"
#include "shlwapi.h"
#include "Dbghelp.h"
#include "DataContainer.h"
#include "LocalHostInfo.h"
#include "Enviroment.h"
#include "MainFrm.h"

#include "ChangePackagePropertyDlg.h"

#ifdef _UBCSTUDIO_EE_
#include "ubccopcommon\ftpmultisite.h"
#endif//_UBCSTUDIO_EE_

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CUBCStudioDoc

IMPLEMENT_DYNCREATE(CUBCStudioDoc, CDocument)

BEGIN_MESSAGE_MAP(CUBCStudioDoc, CDocument)
END_MESSAGE_MAP()


int CUBCStudioDoc::s_schedule_count=0;
// CUBCStudioDoc construction/destruction

CUBCStudioDoc::CUBCStudioDoc()
{
	TraceLog(("CUBCStudioDoc()"));

	m_strSite.Empty();
	CDataContainer::getInstance();
	m_bNew = false;
	m_bOpened = false;
}

CUBCStudioDoc::~CUBCStudioDoc()
{
	
}

BOOL CUBCStudioDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



// CUBCStudioDoc serialization

void CUBCStudioDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CUBCStudioDoc diagnostics

#ifdef _DEBUG
void CUBCStudioDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CUBCStudioDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG



// CUBCStudioDoc commands
// Modified by 정운형 2009-01-28 오후 1:43
// 변경내역 :  이미지 추가 작업
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 편성관리 설정파일을 읽어 편성관리를 초기화 한다 \n
/// @param (CString) strDrive : (in) 편성관리를 읽어오는 drive
/// @param (CString) strHost : (in) 편성관리를 초기화 할 Host name
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCStudioDoc::Load(CString strDrive, CString strHost)
{
	TraceLog(("Load(%s, %s)", strDrive, strHost));
	CString ini_path;
	// 이름에 Space 가 있으면 '_' 로 변경함
	if(strHost.Find(" ") >= 0){
		int i = 0;
		char szTemp[10];
		CFileFind ff;
		CString szOldName;

		szOldName.Format("%s%s%s.ini", strDrive, UBC_CONFIG_PATH, strHost);
		strHost.Replace(" ", "_");

		while(1){
			if(i != 0){
				strHost += _T("_");
				strHost += _itot(i, szTemp, 10);
			}

			ini_path.Format("%s%s%s.ini", strDrive, UBC_CONFIG_PATH, strHost);

			if(!ff.FindFile(ini_path))
				break;
			i++;
		}
		::rename(szOldName, ini_path);
	}

	m_strSourceDrive = strDrive;
	m_strHostName = strHost;
	GetEnvPtr()->m_strPackage = m_strHostName;
	if(m_strSourceDrive == _T("") || m_strHostName == _T(""))
	{
		return false;
	}//if

	ini_path = m_strSourceDrive;
	ini_path.Append(UBC_CONFIG_PATH);
	ini_path.Append(m_strHostName);
	ini_path.Append(".ini");

	bool ret = CDataContainer::getInstance()->Load(ini_path);
	CDataContainer::getInstance()->Save(ini_path, false);

	// location 수정
//	CString root_path = m_strSourceDrive;
//	root_path.Append(UBC_ROOT_PATH);

	CONTENTS_INFO_MAP* pContentsMap = GetContentsMap();
	POSITION pos = pContentsMap->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo = NULL;
		pContentsMap->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );
		if(!pContentsInfo) continue;

//		pContentsInfo->strLocalLocation.Insert(0, root_path);
		//Local system에 contents 파일이 실제로 존재하는지 검사한다
		if(!pContentsInfo->strLocalLocation.IsEmpty() && !pContentsInfo->strFilename.IsEmpty() && 
			PathFileIsSame(pContentsInfo->strLocalLocation + pContentsInfo->strFilename, pContentsInfo->nFilesize, pContentsInfo->strRegisterTime)){ 
			pContentsInfo->bLocalFileExist = true;
		}
		else if(pContentsInfo->strFilename.IsEmpty() && 
			(pContentsInfo->nContentsType==CONTENTS_SMS||pContentsInfo->nContentsType==CONTENTS_TICKER||
			 pContentsInfo->nContentsType==CONTENTS_TEXT||pContentsInfo->nContentsType==CONTENTS_TV||
			 pContentsInfo->nContentsType==CONTENTS_RSS||pContentsInfo->nContentsType==CONTENTS_WIZARD||
			 pContentsInfo->nContentsType==CONTENTS_WEBBROWSER))
		{
			pContentsInfo->bLocalFileExist = true;
		}
		else{
			pContentsInfo->bLocalFileExist = false;
		}
	}

	// 변경사항을 체크하기위해 백업본을 생성한다.
	CDataContainer::getInstance()->CreateBackupData();

	CWnd* wnd = ::AfxGetMainWnd();
	if(wnd->GetSafeHwnd())
	{
		wnd->SendMessage(WM_TEMPLATE_INFO_CHANGED);
		wnd->SendMessage(WM_TEMPLATE_LIST_CHANGED);
		wnd->SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED);
		wnd->SendMessage(WM_PLAYCONTENTS_LIST_CHANGED);
		wnd->SendMessage(WM_CONTENTS_LIST_CHANGED, 1);
	}

	TraceLog(("CUBCStudioDoc::Load(Success)"));
	return ret;
}

// Modified by 정운형 2009-01-28 오후 1:43
// 변경내역 :  이미지 추가 작업
bool CUBCStudioDoc::SaveContents()
{
	CDriveSelectDialog dlg;
	dlg.m_strHostName = m_strHostName;
	dlg.m_strDesc = GetDescription();
	m_strSite.Empty();

	dlg.m_nCategorys[CDriveSelectDialog::eCategory  ] = CDataContainer::getInstance()->m_nContentsCategory;
	dlg.m_nCategorys[CDriveSelectDialog::ePurpose   ] = CDataContainer::getInstance()->m_nPurpose         ;
	dlg.m_nCategorys[CDriveSelectDialog::eHostType  ] = CDataContainer::getInstance()->m_nHostType        ;
	dlg.m_nCategorys[CDriveSelectDialog::eVertical  ] = CDataContainer::getInstance()->m_nVertical        ;
	dlg.m_nCategorys[CDriveSelectDialog::eResolution] = CDataContainer::getInstance()->m_nResolution      ;
	dlg.m_bIsPublic                                   = CDataContainer::getInstance()->m_bIsPublic        ;
	dlg.m_strValidationDate = CDataContainer::getInstance()->m_strValidationDate;

	dlg.m_bMonitorOn = CDataContainer::getInstance()->m_bMonitorOn;
	dlg.m_bMonitorOff = CDataContainer::getInstance()->m_bMonitorOff;

	if(dlg.DoModal() != IDOK) return false;

	if(dlg.m_bTemplate)
	{
		m_bNew = true;
	}

	if(dlg.m_strDrive != m_strSourceDrive)
	{
		// 0000736 드라이브가 바뀌는 경우 모든 콘텐츠가 서버로부터 다운로드 완료되어 복사가능한지 확인한다
		if(!DownloadAllContents(true)) return false;
	}

	m_strTargetDrive = dlg.m_strDrive;
	m_strHostName = dlg.m_strHostName;

	GetEnvPtr()->m_strPackage = m_strHostName;
	CDataContainer::getInstance()->m_strNetworkUse = dlg.m_bServer?"1":"0";

	CDataContainer::getInstance()->m_bMonitorOn = dlg.m_bMonitorOn;
	CDataContainer::getInstance()->m_bMonitorOff = dlg.m_bMonitorOff;

	if(!dlg.m_strSite.IsEmpty())
	{
		m_strSite = dlg.m_strSite;
	}

	//if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		SPackageInfo4Studio Info;

		Info.bShow = true;
		Info.bModify = false;
		Info.szDrive = m_strTargetDrive;
		Info.szSiteID = m_strSite;
		Info.szPackage = m_strHostName;
		Info.szDescript = dlg.m_strDesc;
		Info.szUpdateUser = GetEnvPtr()->m_szLoginID;

		SYSTEMTIME st;
		::GetLocalTime(&st);
		CTime tmCur(st);
		Info.szUpdateTime = tmCur.Format(STR_ENV_TIME);
		Info.tmUpdateTime = tmCur.GetTime();

		// 패키지객체 속성추가
		Info.nContentsCategory = dlg.m_nCategorys[CDriveSelectDialog::eCategory  ];
		Info.nPurpose          = dlg.m_nCategorys[CDriveSelectDialog::ePurpose   ];
		Info.nHostType         = dlg.m_nCategorys[CDriveSelectDialog::eHostType  ];
		Info.nVertical         = dlg.m_nCategorys[CDriveSelectDialog::eVertical  ];
		Info.nResolution       = dlg.m_nCategorys[CDriveSelectDialog::eResolution];
		Info.bIsPublic         = dlg.m_bIsPublic        ;

		// todo 패키지 유효기간을 저장시 설정할지 매니져에서 관리자가 설정할지 결정해야함.
		COleDateTime tmTemp;
		tmTemp.ParseDateTime(dlg.m_strValidationDate);
		Info.tmValidationDate  = tmTemp;
		Info.strValidationDate = dlg.m_strValidationDate;

		GetEnvPtr()->ChangeCurPackage(Info);
	}

	SetDescription(dlg.m_strDesc);

	// 패키지객체 속성추가
	CDataContainer::getInstance()->m_nContentsCategory = GetEnvPtr()->m_PackageInfo.nContentsCategory;
	CDataContainer::getInstance()->m_nPurpose          = GetEnvPtr()->m_PackageInfo.nPurpose         ;
	CDataContainer::getInstance()->m_nHostType         = GetEnvPtr()->m_PackageInfo.nHostType        ;
	CDataContainer::getInstance()->m_nVertical         = GetEnvPtr()->m_PackageInfo.nVertical        ;
	CDataContainer::getInstance()->m_nResolution       = GetEnvPtr()->m_PackageInfo.nResolution      ;
	CDataContainer::getInstance()->m_bIsPublic         = GetEnvPtr()->m_PackageInfo.bIsPublic        ;
	CDataContainer::getInstance()->m_bIsVerify         = GetEnvPtr()->m_PackageInfo.bIsVerify        ;
	CDataContainer::getInstance()->m_strValidationDate = GetEnvPtr()->m_PackageInfo.strValidationDate;

	if(m_strTargetDrive != m_strSourceDrive)
	{
		CFileCopyDialog fc_dialog(::AfxGetMainWnd(), dlg.m_strDrive);
		fc_dialog.DoModal();
	}
	else
	{
		CWnd* pWnd = ::AfxGetMainWnd();
		if(pWnd->GetSafeHwnd())
		{
			pWnd->PostMessage(WM_COMPLETE_FILE_COPY, 1, 0); // all ok
		}//if
	}//if

	return true;
}

// Modified by 정운형 2008-12-29 오전 11:29
// 변경내역 :  Export 수정
bool CUBCStudioDoc::SaveConfig()
{
	CString ini_path = m_strTargetDrive;
	ini_path.Append(UBC_CONFIG_PATH);
	if(!::PathFileExists(ini_path))
	{
		if(::MakeSureDirectoryPathExists(ini_path) == FALSE)
		{
			UbcMessageBox(LoadStringById(IDS_UBCSTUDIODOC_MSG001), MB_ICONSTOP);
			return false;
		}
	}

	ini_path.Append(m_strHostName);
	ini_path.Append(".ini");
	DeleteFile(ini_path);

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		if(!m_strSite.IsEmpty()){
			CDataContainer::getInstance()->m_strSite = m_strSite;
			m_strSite.Empty();
		}

		CDataContainer::getInstance()->m_strLastWrittenUser = GetEnvPtr()->m_szLoginID;
	}
//	SYSTEMTIME st;
//	::GetLocalTime(&st);
//	CTime tmCur(st);
	CDataContainer::getInstance()->m_strLastWrittenTime = CTime::GetCurrentTime().Format(STR_ENV_TIME);

	//if(!CDataContainer::getInstance()->Save(ini_path, bNew))
	//{
	//	UbcMessageBox(LoadStringById(IDS_UBCSTUDIODOC_MSG002), MB_ICONSTOP);
	//	return false;
	//}

#ifdef _USE_DRM
	//저장된 ini파일을 숨심 속성으로 만들어 준다
	//SetFileAttributes(ini_path, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM);
#endif

#ifdef _DEBUG
//	CString strCopyConfig = "D:/project/utv1/config/";
//	strCopyConfig.Append(m_strHostName);
//	strCopyConfig.Append(".ini");
//	::CopyFile(ini_path, strCopyConfig, FALSE);
#endif

	//Drive type을 체크하여 Fixed인경우 AutoRun 관련 파일을 복사하지 않는다
	UINT unType = GetDriveType(m_strTargetDrive);
	if(unType != DRIVE_FIXED)
	{
		
		//Copy auto run inf, bat, ico 파일
		CString strSourcePath = m_strSourceDrive;
		strSourcePath.Append(UBC_EXECUTE_PATH);
		CString strTargetPath = m_strTargetDrive;
		//strTargetPath.Append(EXCUTE_PATH);

		CString strInf = "autorun.inf";
		CString strBat = "autorun.bat";
		CString strIcon = "UBCStudio.img";

		/* skpark 2012.11.24  USB_Device 자동실행은 처리하지 않는다.
		//inf 파일
		if(::PathFileExists(strSourcePath + strInf))
		{
			::CopyFile(strSourcePath + strInf, strTargetPath + strInf, FALSE);
		}//if

		//Batch 파일
		if(::PathFileExists(strSourcePath + strBat))
		{
			::CopyFile(strSourcePath + strBat, strTargetPath + strBat, FALSE);
		}//if
		*/

		//icon 파일
		if(::PathFileExists(strSourcePath + strIcon))
		{
			::CopyFile(strSourcePath + strIcon, strTargetPath + strIcon, FALSE);
		}//if
	}//if

	//소스 드라이브와 타겟 드라이브가 같지 않다면
	//콘텐츠 맵의 경로를 바꾸어 주어야 한다.
	if(m_strSourceDrive != m_strTargetDrive)
	{
		CString strContentsID;
		CONTENTS_INFO* pContentsInfo;
		CONTENTS_INFO_MAP* pContentsMap = GetContentsMap();
		POSITION pos = pContentsMap->GetStartPosition();
		while(pos)
		{
			 pContentsMap->GetNextAssoc(pos, strContentsID, (void*&)pContentsInfo);
			 if(pContentsInfo)
			 {
				 pContentsInfo->strLocalLocation = m_strTargetDrive + UBC_CONTENTS_PATH;
			 }//if
		}//while

#ifdef _USE_DRM
		//contents 폴더와 config 폴더를 숨김 속성으로 변경 해 준다.
		//config 폴더
		CString strHidden = m_strTargetDrive;
		//strHidden.Append(UBC_CONFIG_PATH);
		//SetFileAttributes(strHidden, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM);

		//contents의 ENC
		strHidden = m_strTargetDrive;
		strHidden.Append(UBC_CONTENTS_PATH);
		SetFileAttributes(strHidden, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM);

		//contents 폴더
		strHidden = m_strTargetDrive;
		strHidden.Append(UBC_CONTENTS_ROOT);
		SetFileAttributes(strHidden, FILE_ATTRIBUTE_HIDDEN|FILE_ATTRIBUTE_SYSTEM);
#endif
	}//if

	m_strSourceDrive = m_strTargetDrive;

	if(!CDataContainer::getInstance()->Save(ini_path))
	{
		UbcMessageBox(LoadStringById(IDS_UBCSTUDIODOC_MSG002), MB_ICONSTOP);
		return false;
	}

	/*
	if(m_strSourceDrive == m_strTargetDrive)
	{
		return true;
	}//if

	//
	CString source_path;
	CString temp_path;
	CString target_path = m_strTargetDrive;
	target_path.Append(EXPORT_ROOT_PATH);

	source_path = m_strTargetDrive;
	source_path.Append(UBC_ROOT_PATH);

	temp_path = m_strTargetDrive;
	temp_path.Append(TEMP_ROOT_PATH);

	//
	if(::PathFileExists(temp_path))
	{
		if(DeleteFolder(temp_path) != 0)
		{
			UbcMessageBox("Delete old file(s) ERROR !!!", MB_ICONSTOP);
			return false;
		}
	}

	bool exist_source_path = false;
	if(::PathFileExists(source_path))
	{
		exist_source_path = true;
		if(::MoveFile(source_path, temp_path) == FALSE)
		{
			UbcMessageBox("Move old file(s) ERROR !!!", MB_ICONSTOP);
			return false;
		}
	}

	if(::MoveFile(target_path, source_path) == FALSE)
	{
		if(exist_source_path)
			::MoveFile(temp_path, source_path);

		UbcMessageBox("Move file(s) ERROR !!!", MB_ICONSTOP);
		return false;
	}

	m_strSourceDrive = m_strTargetDrive;
*/
	return true;
}
// Modified by 정운형 2008-12-29 오전 11:29
// 변경내역 :  Export 수정
/*
int CUBCStudioDoc::DeleteExportFolder()
{
	CString target_path = m_strTargetDrive;
	target_path.Append(EXPORT_ROOT_PATH);

	return DeleteFolder(target_path);
}
*/
CONTENTS_INFO_MAP* CUBCStudioDoc::GetContentsMap()
{
	return CDataContainer::getInstance()->GetContentsMap();
}

TEMPLATE_LINK_LIST* CUBCStudioDoc::GetAllTemplateList()
{
	return CDataContainer::getInstance()->GetAllTemplateList();
}

TEMPLATE_LINK_LIST* CUBCStudioDoc::GetPlayTemplateList()
{
	return CDataContainer::getInstance()->GetPlayTemplateList();
}

bool CUBCStudioDoc::MoveFordwardPlayTemplate(int nIndex)
{
	return CDataContainer::getInstance()->MoveFordwardPlayTemplate(nIndex);
}

bool CUBCStudioDoc::MoveBackwardPlayTemplate(int nIndex)
{
	return CDataContainer::getInstance()->MoveBackwardPlayTemplate(nIndex);
}

void CUBCStudioDoc::SetSelectTemplateId(LPCTSTR lpszId)
{
	m_strSelectTemplateId = (lpszId == NULL ? "" : lpszId);
}

void CUBCStudioDoc::SetSelectFrameId(LPCTSTR lpszId)
{
	m_strSelectFrameId = (lpszId == NULL ? "" : lpszId);
}

LPCTSTR CUBCStudioDoc::GetSelectTemplateId()
{
	return m_strSelectTemplateId;
}

LPCTSTR CUBCStudioDoc::GetSelectFrameId()
{
	return m_strSelectFrameId;
}

CONTENTS_INFO* CUBCStudioDoc::GetContents(LPCTSTR lpszContentsId)
{
	return CDataContainer::getInstance()->GetContents(lpszContentsId);
}

PLAYCONTENTS_INFO* CUBCStudioDoc::GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId, LPCTSTR lpszPlayContentsId)
{
	return CDataContainer::getInstance()->GetPlayContents(lpszTemplateId, lpszFrameId, lpszPlayContentsId);
}

PLAYCONTENTS_INFO* CUBCStudioDoc::GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszPlayContentsId)
{
	return CDataContainer::getInstance()->GetPlayContents(lpszTemplateId, lpszPlayContentsId);
}

TEMPLATE_LINK* CUBCStudioDoc::GetTemplateLink(LPCTSTR lpszTemplateId)
{
	return CDataContainer::getInstance()->GetTemplateLink(lpszTemplateId);
}

TEMPLATE_LINK* CUBCStudioDoc::GetSelectTemplateLink()
{
	return CDataContainer::getInstance()->GetTemplateLink(m_strSelectTemplateId);
}

FRAME_LINK* CUBCStudioDoc::GetSelectFrameLink()
{
	return CDataContainer::getInstance()->GetFrameLink(m_strSelectTemplateId, m_strSelectFrameId);
}

bool CUBCStudioDoc::AddContents(CONTENTS_INFO* pContentsInfo)
{
	if(pContentsInfo != NULL)
	{
		CString strErrMsg;
		return CDataContainer::getInstance()->AddContents(pContentsInfo, strErrMsg);
	}
	return false;
}

TEMPLATE_INFO* CUBCStudioDoc::CreateNewTemplate()
{
	return CDataContainer::getInstance()->CreateNewTemplate();
}

bool CUBCStudioDoc::AddTemplate(TEMPLATE_INFO* pTemplateInfo)
{
	if(pTemplateInfo != NULL)
	{
		return CDataContainer::getInstance()->AddTemplate(pTemplateInfo);
	}
	return false;
}

int CUBCStudioDoc::AddPlayTemplate(LPCTSTR lpszTemplateId)
{
	return CDataContainer::getInstance()->AddPlayTemplate(lpszTemplateId);
}

FRAME_INFO*	CUBCStudioDoc::CreateNewFrame(LPCTSTR lpszTemplateId)
{
	return CDataContainer::getInstance()->CreateNewFrame(lpszTemplateId);
}

TEMPLATE_INFO*	CUBCStudioDoc::CreateNewTemplateFromServer(TEMPLATE_LINK* pPublicTemplateLink)
{
	return CDataContainer::getInstance()->CreateNewTemplateFromServer(pPublicTemplateLink);
}

bool CUBCStudioDoc::DeleteContents(LPCTSTR lpszContentsId)
{
	bool bRet = CDataContainer::getInstance()->DeleteContents(lpszContentsId);

	CWnd* wnd = ::AfxGetMainWnd();
	if(wnd->GetSafeHwnd())
	{
		wnd->SendMessage(WM_ALL_INFO_CHANGED);
	}

	return bRet;
}

bool CUBCStudioDoc::DeleteTemplate(LPCTSTR lpszTemplateId)
{
	return CDataContainer::getInstance()->DeleteTemplate(lpszTemplateId);
}

bool CUBCStudioDoc::DeleteFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId)
{
	return CDataContainer::getInstance()->DeleteFrame(lpszTemplateId, lpszFrameId);
}

bool CUBCStudioDoc::DeletePlayTemplate(int nIndex)
{
	return CDataContainer::getInstance()->DeletePlayTemplate(nIndex);
}

bool CUBCStudioDoc::IsExistContentsInTemplate(LPCTSTR lpszTemplateId)
{
	return CDataContainer::getInstance()->IsExistContentsInTemplate(lpszTemplateId);
}

bool CUBCStudioDoc::IsExistContentsInFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId)
{
	return CDataContainer::getInstance()->IsExistContentsInFrame(lpszTemplateId, lpszFrameId);
}

bool CUBCStudioDoc::IsExistPlayTemplateList(LPCTSTR lpszTemplateId)
{
	return CDataContainer::getInstance()->IsExistPlayTemplateList(lpszTemplateId);
}

bool CUBCStudioDoc::IsExistPlayContentsUsingContents(LPCTSTR lpszContents)
{
	return CDataContainer::getInstance()->IsExistPlayContentsUsingContents(lpszContents);
}

int CUBCStudioDoc::GetContentsUsingCount(LPCTSTR lpszContentsId)
{
	return CDataContainer::getInstance()->GetContentsUsingCount(lpszContentsId);
}

bool CUBCStudioDoc::CheckOverlayModeFrame(CString strTemplateId, CString strOverlayModeFrameId)
{
	return CDataContainer::getInstance()->CheckOverlayModeFrame(strTemplateId, strOverlayModeFrameId);
}

bool CUBCStudioDoc::CheckContentsCanBeAddedToFrame(CString strTemplateId, CString strFrameId, CString strContentsId)
{
	return CDataContainer::getInstance()->CheckContentsCanBeAddedToFrame(strTemplateId, strFrameId, strContentsId);
}

bool CUBCStudioDoc::CheckPlayContentsRelation()
{
	return CDataContainer::getInstance()->CheckPlayContentsRelation();
}

int CUBCStudioDoc::DeleteFolder(LPCTSTR lpszTargetPath)
{
	TCHAR path[MAX_PATH];
	::ZeroMemory(path, sizeof(path));
	_tcscpy(path, lpszTargetPath);

	SHFILEOPSTRUCT FileOp;//쉘 구조체 

	FileOp.hwnd = NULL;
	FileOp.wFunc = FO_DELETE;//지우고
	FileOp.pFrom = path;//인자로 받은 폴더
	FileOp.pTo = NULL;//복사할때 사용
	FileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOERRORUI ;//화면에 메세지 표시 않함
	FileOp.fAnyOperationsAborted = false;//나머진 뭔지 모르겠네요
	FileOp.hNameMappings = NULL;
	FileOp.lpszProgressTitle = NULL;

	return SHFileOperation(&FileOp);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 경로의 호스트 정보를 구한다 \n
/// @param (LPCTSTR) lpszPath : (in) Config 파일이 있는 경로
/// @param (CString) strDrive : (in) Config 파일이 있는 드라이브 문자열
/// @param (CPtrArray&) aryHostInfo : (out) 호스트 정보의 배열
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCStudioDoc::GetHostInfo(LPCTSTR lpszPath, CString strDrive, CPtrArray& aryHostInfo)
{
	CString find_path = lpszPath;
	find_path.Append("\\*.ini");
	CString strHostName, strHostPath, strDesc, strNetworkUse, strSite, strWrittenTime, strWrittenID;
	CLocalHostInfo* pclsHost = NULL;
	CTime tmWritten;
	char buf[1024] = { 0x00 };

	long nContentsCategory, nPurpose, nHostType, nVertical, nResolution;
	bool bIsPublic, bIsVerify;
	CString strValidationDate;

	CFileFind clsFinder;
	BOOL bWorking = clsFinder.FindFile(find_path);
	while (bWorking)
	{
		bWorking = clsFinder.FindNextFile();

		if(clsFinder.IsDots()) continue;
		if(clsFinder.IsDirectory()) continue;

		//샘플파일과 preview 파일을 제외한 호스트 정보를 읽어서
		//배열에 넣어준다
		strHostName = clsFinder.GetFileTitle();

		//ini 파일에서 정보를 읽어온다
		strHostPath.Format("%s%s.ini", lpszPath, strHostName);

		CProfileManager objIniManager(strHostPath);

		strDesc           = objIniManager.GetProfileString("Host", "description"     );
		strNetworkUse     = objIniManager.GetProfileString("Host", "networkUse"      );
		strSite           = objIniManager.GetProfileString("Host", "site"            );
		strWrittenTime    = objIniManager.GetProfileString("Host", "lastUpdateTime"  );
		strWrittenID      = objIniManager.GetProfileString("Host", "lastUpdateId"    );
		nContentsCategory = objIniManager.GetProfileInt   ("Host", "contentsCategory");  // 패키지객체 속성추가
		nPurpose          = objIniManager.GetProfileInt   ("Host", "purpose"         );  // ..
		nHostType         = objIniManager.GetProfileInt   ("Host", "HostType"        );  // ..
		nVertical         = objIniManager.GetProfileInt   ("Host", "vertical"        );  // ..
		nResolution       = objIniManager.GetProfileInt   ("Host", "resolution"      );  // ..
		bIsPublic         = objIniManager.GetProfileInt   ("Host", "isPublic"        );  // ..
		bIsVerify         = objIniManager.GetProfileInt   ("Host", "isVerify"        );  // ..
		strValidationDate = objIniManager.GetProfileString("Host", "validationDate"  );  // ..

		pclsHost = new CLocalHostInfo;
		pclsHost->SetLocalHostInfo(strDrive, strNetworkUse, strHostName, strDesc, strSite, strWrittenTime, strWrittenID
								 , nContentsCategory, nPurpose, nHostType, nVertical, nResolution, bIsPublic, bIsVerify, strValidationDate);
		aryHostInfo.Add(pclsHost);
	}//while

	clsFinder.Close();
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Run preview browser \n
/// @param (CString) strTemplateID : (in) Preview 하려는 template id
/// @param (bool) bSaveConfig : (in) 편성 내용이 변경되어 config 파일을 저장하여야 하는지 여부
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCStudioDoc::RunPreview(CString strTemplateID, bool bSaveConfig)
{
	if(bSaveConfig)
	{
		if(!PreviewSaveConfig(strTemplateID))
		{
			return false;
		}//if
	}//if

	CString strLoardID;
	strLoardID.Format("Template%s", strTemplateID);

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString strCmd, strBRWPath, strDrive, strCurrentPath;
	strDrive = szDrive;
	strDrive.Append("\\");
#ifdef _DEBUG
	strBRWPath.Format("C:\\%s%s", UBC_EXECUTE_PATH, EXCUTE_FILE);
#else
	strBRWPath.Format("%s%s%s", strDrive, UBC_EXECUTE_PATH, EXCUTE_FILE);
#endif
	strCurrentPath.Format("%s%s", m_strSourceDrive, UBC_EXECUTE_PATH);
	strCmd.Format("%s +socketproxy +host %s +template %s", EXCUTE_ARG_IGNORE, PREVIEW_FILE_KEY, strTemplateID);

	STARTUPINFO stStartupInfo = {0}; 
	stStartupInfo.cb = sizeof(STARTUPINFO);
	PROCESS_INFORMATION stProcessInfo; 
	BOOL bRet =  CreateProcess(strBRWPath,
								(LPSTR)(LPCTSTR)strCmd,
								NULL,
								NULL,
								FALSE,
								0,
								NULL,
								strCurrentPath,
								&stStartupInfo,
								&stProcessInfo);

	return (bool)bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Preview를 위하여 현재 open 호스트의 config 파일을 저장한다 \n
/// @param (CString) strTemplateID : (in) 저장 하려는 template id
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCStudioDoc::PreviewSaveConfig(CString strTemplateID)
{
	//
	CString strConfigPath = m_strSourceDrive;
	strConfigPath.Append(UBC_CONFIG_PATH);
	if(::PathFileExists(strConfigPath) == FALSE)
	{
		return false;
	}//if

	CString strPreviewName;
	strPreviewName.Format("%s.ini", PREVIEW_FILE_KEY);
	//strConfigPath.Append(m_strHostName);
	//strConfigPath.Append(".ini");
	strConfigPath.Append(strPreviewName);
	DeleteFile(strConfigPath);
	if(CDataContainer::getInstance()->SavePreview(strConfigPath, strTemplateID))
	{
#ifdef _DEBUG
//		CString strCopyConfig = "D:/project/utv1/config/";
//		strCopyConfig.Append(PREVIEW_FILE_KEY);
//		strCopyConfig.Append(".ini");
//		::CopyFile(strConfigPath, strCopyConfig, FALSE);
#endif
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 호스트의 이름을 반환한다. \n
/// @return <형: CString> \n
///			<설정된 호스트의 이름> \n
/////////////////////////////////////////////////////////////////////////////////
CString CUBCStudioDoc::GetHostName(void)
{
	return m_strHostName;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 source 드라이브 문자열을 반환한다. \n
/// @return <형: CString> \n
///			<설정된 source 드라이브 문자열> \n
/////////////////////////////////////////////////////////////////////////////////
CString	CUBCStudioDoc::GetSourceDirive(void)
{
	return m_strSourceDrive;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Source 드라이브 문자열을 설정한다 \n
/// @param (CString) strDrive : (in) 설정하려는 drive 문자열
/////////////////////////////////////////////////////////////////////////////////
void CUBCStudioDoc::SetSourceDirive(CString strDrive)
{
	m_strSourceDrive = strDrive;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설정된 target 드라이브 문자열을 반환한다. \n
/// @return <형: CString> \n
///			<설정된 target 드라이브 문자열> \n
/////////////////////////////////////////////////////////////////////////////////
CString CUBCStudioDoc::GetTargetDirive()
{
	return m_strTargetDrive;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Target 드라이브 문자열을 설정한다 \n
/// @param (CString) strDrive : (in) 설정하려는 drive 문자열
/////////////////////////////////////////////////////////////////////////////////
void CUBCStudioDoc::SetTargetDirive(CString strDrive)
{
	m_strTargetDrive = strDrive;
}


// Modified by 정운형 2009-02-19 오후 8:31
// 변경내역 :  template 추가 버그 수정
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기존의 template size, color등 기본정보만 복사하여 새로운 template를 생성한다. \n
/// @param (LPCTSTR) lpszSrcTemplateID : (in) 복사하려는 template id
/// @return <형: LPCTSTR> \n
///			새로 생성한 template의 id \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR CUBCStudioDoc::CreateCopyTemplate(LPCTSTR lpszSrcTemplateID)
{
	return CDataContainer::getInstance()->CreateCopyTemplate(lpszSrcTemplateID);
}
// Modified by 정운형 2009-02-19 오후 8:31
// 변경내역 :  template 추가 버그 수정

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Load한 package을 메모리에서 모두 지우고 초기화 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCStudioDoc::ClosePackage(void)
{
	CDataContainer::getInstance()->DeleteAllData();

	GetEnvPtr()->m_strPackage = _T("");
	GetEnvPtr()->m_PackageInfo.Clear();

	m_strSelectTemplateId	= _T("");
	m_strSelectFrameId		= _T("");
	m_strSourceDrive		= _T("");
	m_strTargetDrive		= _T("");
	m_strHostName			= _T("");
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 열려있는 host의 설명을 반환한다. \n
/// @return <형: CString> \n
///			열려있는 host의 설명 \n
/////////////////////////////////////////////////////////////////////////////////
CString CUBCStudioDoc::GetDescription(void)
{
	return CDataContainer::getInstance()->GetDescription();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 열려있는 host의 설명을 설정한다. \n
/// @param (CString) strDesc : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CUBCStudioDoc::SetDescription(CString strDesc)
{
	CDataContainer::getInstance()->SetDescription(strDesc);
}

CString CUBCStudioDoc::CreateCopyPlayTemplate(CString strSrcTemplateID)
{
	CString strNewTemplateID = CreateCopyTemplate(strSrcTemplateID);
	if(strNewTemplateID.IsEmpty()) return strNewTemplateID;

	TEMPLATE_LINK* pSrcTemplate = CDataContainer::getInstance()->GetTemplateLink(strSrcTemplateID);
	TEMPLATE_LINK* pNewTemplate = CDataContainer::getInstance()->GetTemplateLink(strNewTemplateID);

	for(int i=0; i<pSrcTemplate->arFrameLinkList.GetCount() ;i++)
	{
		FRAME_LINK& SrcFrameLink = pSrcTemplate->arFrameLinkList.GetAt(i);
		FRAME_LINK& NewFrameLink = pNewTemplate->arFrameLinkList.GetAt(i);

		for(int j=0; j<SrcFrameLink.pFrameInfo->arCyclePlayContentsList.GetCount(); j++)
		{
			PLAYCONTENTS_INFO* pNewPlayContentsInfo = new PLAYCONTENTS_INFO;
			*pNewPlayContentsInfo = *(SrcFrameLink.pFrameInfo->arCyclePlayContentsList.GetAt(j));
			pNewPlayContentsInfo->strTemplateId = NewFrameLink.pFrameInfo->strTemplateId;
			pNewPlayContentsInfo->strFrameId = NewFrameLink.pFrameInfo->strId;
			NewFrameLink.pFrameInfo->arCyclePlayContentsList.Add(pNewPlayContentsInfo);
		}

		for(int j=0; j<SrcFrameLink.pFrameInfo->arTimePlayContentsList.GetCount(); j++)
		{
			PLAYCONTENTS_INFO* pNewPlayContentsInfo = new PLAYCONTENTS_INFO;
			*pNewPlayContentsInfo = *(SrcFrameLink.pFrameInfo->arTimePlayContentsList.GetAt(j));
			pNewPlayContentsInfo->strTemplateId = NewFrameLink.pFrameInfo->strTemplateId;
			pNewPlayContentsInfo->strFrameId = NewFrameLink.pFrameInfo->strId;
			NewFrameLink.pFrameInfo->arTimePlayContentsList.Add(pNewPlayContentsInfo);
		}
	}

	return strNewTemplateID;
}

// 새로추가할 플레이콘텐츠의 기본시작시간을 구한다
CTime CUBCStudioDoc::GetNewPlayContentsStartTime(int nPlayContentsType)
{
	CTime tmNewPlayContentsStartTime = CTime::GetCurrentTime();

	// 정시 플레이콘텐츠가 아닌경우 기존과 같이 현재시간을 기준으로 설정하도록 함
	if(nPlayContentsType != 1) return tmNewPlayContentsStartTime;

	TEMPLATE_LINK_LIST* pTemplateLinkList = GetPlayTemplateList();
	if(pTemplateLinkList == NULL) return tmNewPlayContentsStartTime;

	// 새로운 플레이콘텐츠의 시작시간을 구하기위해 가장큰 종료시간을 구한다
	for(int nTemplateIdx=0; nTemplateIdx<pTemplateLinkList->GetCount() ; nTemplateIdx++)
	{
		TEMPLATE_LINK& TemplateLink = pTemplateLinkList->GetAt(nTemplateIdx);

		for(int nFrameIdx=0; nFrameIdx<TemplateLink.arFrameLinkList.GetCount(); nFrameIdx++)
		{
			FRAME_LINK& FrameLink = TemplateLink.arFrameLinkList.GetAt(nFrameIdx);

			for(int nPlayContentsIdx=0; nPlayContentsIdx<FrameLink.pFrameInfo->arTimePlayContentsList.GetCount(); nPlayContentsIdx++)
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)FrameLink.pFrameInfo->arTimePlayContentsList.GetAt(nPlayContentsIdx);
				if(pPlayContentsInfo == NULL) continue;

				CTime tmEndDate(pPlayContentsInfo->nEndDate);

				int pos = 0;
				CString hour  = pPlayContentsInfo->strEndTime.Tokenize("/:-", pos);
				CString minute= pPlayContentsInfo->strEndTime.Tokenize("/:-", pos);
				CString second= pPlayContentsInfo->strEndTime.Tokenize("/:-", pos);
				
				CTime tmEndDateTime = CTime( tmEndDate.GetYear()
											,tmEndDate.GetMonth()
											,tmEndDate.GetDay()
											,atoi(hour)
											,atoi(minute)
											,atoi(second)
											);

				if(tmNewPlayContentsStartTime < tmEndDateTime)
				{
					tmNewPlayContentsStartTime = tmEndDateTime;
				}
			}
		}
	}

	return tmNewPlayContentsStartTime;
}

bool CUBCStudioDoc::InputErrorCheck()
{
	// 저장, 다른이름저장등은 임시로 수시로 저장하게 되므로
	// 네트웍 전송시 및 콘텐츠 패키지 예약에서만 체크하도록 함.
//	if(!CDataContainer::getInstance()->CheckPrimaryFrame()) return false;

	if(!CheckTimeBasePlayContentsOverlap(1)) {
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG006), MB_ICONWARNING);
		return false;
	}

	if(!CheckOverlayModeFrame(_T(""),_T(""))) {
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG006), MB_ICONWARNING);
		return false;
	}

	// 0000891: 어떤 프레임에 자식 플레이콘텐츠만 있고, 자식 플레이콘텐츠외에 아무런 다른 플레이콘텐츠가 없다면 저장시 경고해주어야함.
	// 경고는 하되, 저장은 될수 있도록 함.
	CheckPlayContentsRelation();

	if(!CheckMaxContentsAtSave()) {
		UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG006), MB_ICONWARNING);
		return false; // skpark 2013.1.23
	}

	//skpark 2016.09.01 for UBGolf
	if(CEnviroment::eUBGOLF == GetEnvPtr()->m_Customer)	{
		CString no_ad_name_contents;
		if(!CheckAdvertiserName(no_ad_name_contents)) {
			CString msg;
			msg = LoadStringById(IDS_MAINFRAME_MSG040);
			msg += L"\r\n";
			msg += no_ad_name_contents;
			UbcMessageBox(msg, MB_ICONWARNING);
		}
	}

	return true;
}

//skpark 2016.09.01 for UBGolf
bool CUBCStudioDoc::CheckAdvertiserName(CString& no_ad_name_contents)
{
	CONTENTS_INFO_MAP* contents_map = GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		// 동영상,그림 중에 괄호로 시작하지 않는 콘텐츠는 광고주표시가 안된것으로 본다.
		if( (pContentsInfo->nContentsType == CONTENTS_VIDEO || pContentsInfo->nContentsType == CONTENTS_IMAGE)  &&
			(pContentsInfo->strContentsName.GetLength() < 2 || pContentsInfo->strContentsName[0] != '(')		)
		{
			no_ad_name_contents += pContentsInfo->strContentsName;
			no_ad_name_contents += L"\r\n";
		}

	}

	if(no_ad_name_contents.IsEmpty())
	{
		return true;
	}
	return false;
}

bool CUBCStudioDoc::CheckTimeBasePlayContentsOverlap(int nPlayContentsType, bool bMsg /*= true*/, bool bCheckIsNewOnly /*=true*/)
{
	// 순환플레이콘텐츠의 경우 체크하지않음
	if(nPlayContentsType == 0) return true;

	TraceLog(("CheckTimeBasePlayContentsOverlap start"));
	
	CWaitMessageBox wait;

	CArray<PLAYCONTENTS_INFO*> arTimebaseData;

	TEMPLATE_LINK_LIST* pTemplateLinkList = GetPlayTemplateList();
	if(pTemplateLinkList == NULL) return true;

	for(int nTemplateIdx=0; nTemplateIdx<pTemplateLinkList->GetCount() ; nTemplateIdx++)
	{
		TEMPLATE_LINK& TemplateLink = pTemplateLinkList->GetAt(nTemplateIdx);

		for(int nFrameIdx=0; nFrameIdx<TemplateLink.arFrameLinkList.GetCount(); nFrameIdx++)
		{
			FRAME_LINK& FrameLink = TemplateLink.arFrameLinkList.GetAt(nFrameIdx);

			for(int nPlayContentsIdx=0; nPlayContentsIdx<FrameLink.pFrameInfo->arTimePlayContentsList.GetCount(); nPlayContentsIdx++)
			{
				PLAYCONTENTS_INFO* pSrcPlayContentsInfo = (PLAYCONTENTS_INFO*)FrameLink.pFrameInfo->arTimePlayContentsList.GetAt(nPlayContentsIdx);
				if(pSrcPlayContentsInfo == NULL) continue;

				arTimebaseData.Add(pSrcPlayContentsInfo);
			}
		}
	}

	CMapStringToString mapDupCheck;
	int iterationCount = arTimebaseData.GetCount();

	int* startDateArr  = new int[iterationCount];
	int* endDateArr = new int[iterationCount];
	for(int i=0;i<iterationCount;i++){
		endDateArr[i] = 0;
		startDateArr[i] = 0;
	}

	int ErrCount=0;
	CString strOverlapInfo;
	for(int nDataIdx=0; nDataIdx< iterationCount;nDataIdx++)
	{
		PLAYCONTENTS_INFO* pSrcPlayContentsInfo = (PLAYCONTENTS_INFO*)arTimebaseData.GetAt(nDataIdx);
		if(pSrcPlayContentsInfo == NULL) continue;

		if(bCheckIsNewOnly){
			if(pSrcPlayContentsInfo->bIsNew){
				TraceLog(("It's new (%d)", nDataIdx));
			}else{
				continue;
			}
		}

		/*
		CTime tmSrcStartDate;
		TimeBasePlayContentsToTime(pSrcPlayContentsInfo->nStartDate, 0,0,0, tmSrcStartDate);
		
		CTime tmSrcEndDate;
		TimeBasePlayContentsToTime(pSrcPlayContentsInfo->nEndDate  , 23,59,59, tmSrcEndDate);
		*/
		int SrcStartDate =	TimeBasePlayContentsToTime(pSrcPlayContentsInfo->nStartDate, 0);
		int SrcEndDate =		TimeBasePlayContentsToTime(pSrcPlayContentsInfo->nEndDate,86399);


		CString strPlayContentsInfo;

		// 기간이 겹치는 경우 시간도 겹치는지 확인해야 함
		for(int nCheckIdx=0; nCheckIdx<iterationCount ;nCheckIdx++)
		{
			if(nDataIdx == nCheckIdx) continue;

			PLAYCONTENTS_INFO* pCmpPlayContentsInfo = (PLAYCONTENTS_INFO*)arTimebaseData.GetAt(nCheckIdx);
			if(pCmpPlayContentsInfo == NULL) continue;


			if(startDateArr[nCheckIdx] == 0){
				startDateArr[nCheckIdx] = TimeBasePlayContentsToTime(pCmpPlayContentsInfo->nStartDate, 0);
				endDateArr[nCheckIdx] = TimeBasePlayContentsToTime(pCmpPlayContentsInfo->nEndDate  ,86399);
			}
			int CmpStartDate = startDateArr[nCheckIdx];
			int CmpEndDate = endDateArr[nCheckIdx];

			// 기간이 겹치는지 확인
			if( (CmpStartDate <= SrcStartDate && SrcStartDate <  CmpEndDate) ||
				(CmpStartDate <  SrcEndDate   && SrcEndDate   <= CmpEndDate) )
			{
				// 시간이 겹치는지 확인
				if( (pCmpPlayContentsInfo->strStartTime <= pSrcPlayContentsInfo->strStartTime && pSrcPlayContentsInfo->strStartTime <  pCmpPlayContentsInfo->strEndTime) ||
					(pCmpPlayContentsInfo->strStartTime <  pSrcPlayContentsInfo->strEndTime   && pSrcPlayContentsInfo->strEndTime   <= pCmpPlayContentsInfo->strEndTime) )
				{

					CString dupId;
					if(nDataIdx < nCheckIdx){
						dupId.Format("%d=%d", nDataIdx, nCheckIdx);
					}else{
						dupId.Format("%d=%d", nCheckIdx, nDataIdx);
					}
					if(!mapDupCheck[dupId].IsEmpty()){
						// 이미 비교된바 있다.
						continue;
					}
					mapDupCheck[dupId] = "INIT";
					TraceLog(("%s", dupId));

					CTime tmCmpStartDate(pCmpPlayContentsInfo->nStartDate);
					CTime tmCmpEndDate(pCmpPlayContentsInfo->nEndDate);

					CString strCmp;
					strCmp.Format("Frame[%s-%s]\t:[%s~%s]\t[%s~%s]"
								 , pCmpPlayContentsInfo->strTemplateId
								 , pCmpPlayContentsInfo->strFrameId
								 , tmCmpStartDate.Format("%Y/%m/%d")
								 , tmCmpEndDate.Format("%Y/%m/%d")
								 , pCmpPlayContentsInfo->strStartTime
								 , pCmpPlayContentsInfo->strEndTime
								 );
					strPlayContentsInfo += "\n-->" + strCmp;
				}
			}
		}

		if(!strPlayContentsInfo.IsEmpty())
		{
			CTime tmSrcStartDate(pSrcPlayContentsInfo->nStartDate);
			CTime tmSrcEndDate(pSrcPlayContentsInfo->nEndDate);
			CString strSrc;
			strSrc.Format("Frame[%s-%s]\t:[%s~%s]\t[%s~%s]"
						 , pSrcPlayContentsInfo->strTemplateId
						 , pSrcPlayContentsInfo->strFrameId
						 , tmSrcStartDate.Format("%Y/%m/%d")
						 , tmSrcEndDate.Format("%Y/%m/%d")
						 , pSrcPlayContentsInfo->strStartTime
						 , pSrcPlayContentsInfo->strEndTime
						 );
			strOverlapInfo += "\n\n" + strSrc + strPlayContentsInfo;
			ErrCount++;
		}

		if(ErrCount >= 10){
			// 너무 많이 겹치면, 일단 끊고 겹치는 것을 수정하도록 유도한다.
			break;
		}

	}

	if(!strOverlapInfo.IsEmpty() && bMsg)
	{
		CString strMsg = LoadStringById(IDS_PLAYCONTENTSVIEW_MSG008) + strOverlapInfo;
		UbcMessageBox(strMsg, MB_ICONWARNING);
	}

	TraceLog(("CheckTimeBasePlayContentsOverlap end"));

	delete [] startDateArr;
	delete [] endDateArr;
	return strOverlapInfo.IsEmpty();
}

CTime CUBCStudioDoc::TimeBasePlayContentsToTime(int nDate, CString strTime)
{
	CTime tmDate(nDate);

	int pos = 0;
	CString hour  = strTime.Tokenize("/:-", pos);
	CString minute= strTime.Tokenize("/:-", pos);
	CString second= strTime.Tokenize("/:-", pos);

	CTime tmDateTime = CTime( tmDate.GetYear()
							 ,tmDate.GetMonth()
							 ,tmDate.GetDay()
							 ,atoi(hour)
							 ,atoi(minute)
							 ,atoi(second)
							 );

	return tmDateTime;
}

bool CUBCStudioDoc::IsModify()
{
	return CDataContainer::getInstance()->IsModified();
}

// 플레이 리스트 갯수가 지정된 갯수를 넘어서는지 체크
bool CUBCStudioDoc::CheckMaxPlayTemplateCount(bool bMsg/*=true*/)
{
	if(GetEnvPtr()->m_nMaxPlayTemplateCount == 0) return true;

	if(GetPlayTemplateList()->GetCount() >= GetEnvPtr()->m_nMaxPlayTemplateCount)
	{
		if(bMsg)
		{
			CString strMsg;
			strMsg.Format(LoadStringById(IDS_UBCSTUDIODOC_MSG003), GetEnvPtr()->m_nMaxPlayTemplateCount);
			UbcMessageBox(strMsg);
		}

		return false;
	}

	return true;
}

// skpark 2013.1.24  콘텐츠의 갯수가 지정된 갯수를 넘어서는지 저장시에만 체크
bool CUBCStudioDoc::CheckMaxContentsAtSave()
{
	TraceLog(("CheckMaxContentsAtSave(%d)", s_schedule_count));

	// skpark   부속파일을 포함한 총 파일 갯수가 1000개를 넘으면 너무 많다!!!
	int contents_count = GetContentsMap()->GetCount();
	if(contents_count > MAX_CONTENTS_COUNT) 
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_UBCSTUDIODOC_MSG010), MAX_CONTENTS_COUNT , contents_count);
		UbcMessageBox(strMsg);
		// return false 해서는 안된다. 그냥 주의사항일뿐이다.
	}
	// skpark schedule 의 갯수가 6000개를 넘으면 너무 많다.
	if(s_schedule_count > MAX_SCHEDULE_COUNT) 
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_UBCSTUDIODOC_MSG010), MAX_SCHEDULE_COUNT , s_schedule_count);
		UbcMessageBox(strMsg);
		return false;
	}

	ULONGLONG lTotalContentsSize = GetTotalContentsSize();
	long contents_size = lTotalContentsSize/(1024*1024);

	if(contents_size > MAX_CONTENTS_SIZE) 
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_UBCSTUDIODOC_MSG011), MAX_CONTENTS_SIZE , contents_size);
		UbcMessageBox(strMsg);
		// return false 해서는 안된다. 그냥 주의사항일뿐이다.
	}
	return true;
}

// 콘텐츠의 갯수가 지정된 갯수를 넘어서는지 체크
bool CUBCStudioDoc::CheckMaxContentsCount(int nAddSize, bool bMsg/*=true*/)
{
	if(GetEnvPtr()->m_nMaxContentsCount == 0)
	{
		// 창일향 : 밀어넣을 수 있는 파일의 총갯수 제한이 있다.  128개까지만 밀어넣을 수 있다.
		if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer &&
			GetContentsMap()->GetCount() + nAddSize > 128    )
		{
			if(bMsg)
			{
				CString strMsg;
				strMsg.Format(LoadStringById(IDS_UBCSTUDIODOC_MSG004), 128);
				UbcMessageBox(strMsg);
			}

			return false;
		}
		else
		{
			return true;
		}
	}

	if(GetContentsMap()->GetCount() + nAddSize > GetEnvPtr()->m_nMaxContentsCount)
	{
		if(bMsg)
		{
			CString strMsg;
			strMsg.Format(LoadStringById(IDS_UBCSTUDIODOC_MSG004), GetEnvPtr()->m_nMaxContentsCount);
			UbcMessageBox(strMsg);
		}

		return false;
	}

	return true;
}

// 콘텐츠의 총 용량이 지정된 용량을 넘어서는지 체크
bool CUBCStudioDoc::CheckTotalContentsSize(ULONGLONG lAddSize, bool bMsg/*=true*/)
{
	if(GetEnvPtr()->m_lMaxContentsSize == 0) return true;

	ULONGLONG lTotalContentsSize = GetTotalContentsSize();

	if(lTotalContentsSize + lAddSize > GetEnvPtr()->m_lMaxContentsSize)
	{
		if(bMsg)
		{
			CString strMsg;
			strMsg.Format(LoadStringById(IDS_UBCSTUDIODOC_MSG005)
						, ToMoneyTypeString(GetEnvPtr()->m_lMaxContentsSize)
						, ToMoneyTypeString(lTotalContentsSize)
						, ToMoneyTypeString(lAddSize)
						);
			UbcMessageBox(strMsg);
		}

		return false;
	}

	return true;
}

ULONGLONG CUBCStudioDoc::GetTotalContentsSize()
{
	return CDataContainer::getInstance()->GetTotalContentsSize();
}

bool CUBCStudioDoc::DownloadAllContents(bool bMsg)
{
#ifdef _UBCSTUDIO_EE_
	if(GetEnvPtr()->m_Edition != CEnviroment::eStudioEE) return true;
	if(GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty()) return true;

	bool bAddedFile = false;

	CFtpMultiSite multiFtp(GetEnvPtr()->m_strHttpOnly.CompareNoCase("CLIENT") == 0);

	CString szPackage;
	szPackage.Format("%s%s%s.ini", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONFIG_PATH, GetEnvPtr()->m_PackageInfo.szPackage);
	multiFtp.AddSite(GetEnvPtr()->m_PackageInfo.szSiteID, szPackage);
	multiFtp.OnlyAddFile();

	CONTENTS_INFO_MAP* contents_map = GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(pContentsInfo && !pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty())
		{
			multiFtp.AddDownloadFile(pContentsInfo->strServerLocation + pContentsInfo->strFilename);
			bAddedFile = true;
		}
	}

	if(bMsg && bAddedFile)
	{
		int nResult = UbcMessageBox(LoadStringById(IDS_UBCSTUDIODOC_MSG006), MB_YESNOCANCEL);

		if(nResult == IDCANCEL) return false;
		if(nResult == IDNO) return true;
	}

	if(!multiFtp.RunFtp(CFtpMultiSite::eDownload)) return false;

	pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(pContentsInfo && !pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty())
		{
			pContentsInfo->bLocalFileExist = true;
			pContentsInfo->bServerFileExist = true;
		}
	}

	CWnd* wnd = ::AfxGetMainWnd();
	if(wnd->GetSafeHwnd())
	{
		wnd->SendMessage(WM_ALL_INFO_CHANGED);
	}
#endif
	return true;
}

// 0001128: Studio 임시 저장 및 복구
void CUBCStudioDoc::LoadFromTemporaryFile()
{
	CWaitMessageBox wait;

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString ini_path;
	ini_path.Format(_T("%s\\%s%s.ini"), szDrive, UBC_CONFIG_PATH, TEMPORARY_SAVE_FILE_KEY);

	if(::PathFileExists(ini_path) == FALSE)
	{
		UbcMessageBox(LoadStringById(IDS_UBCSTUDIODOC_MSG007));
		return;
	}

	// 임시 패키지 저장
	CDataContainer::getInstance()->Load(ini_path);

	CONTENTS_INFO_MAP* pContentsMap = GetContentsMap();
	POSITION pos = pContentsMap->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo = NULL;
		pContentsMap->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );
		if(!pContentsInfo) continue;

//		pContentsInfo->strLocalLocation.Insert(0, root_path);
		//Local system에 contents 파일이 실제로 존재하는지 검사한다
		if(!pContentsInfo->strLocalLocation.IsEmpty() && !pContentsInfo->strFilename.IsEmpty() && PathFileExists(pContentsInfo->strLocalLocation + pContentsInfo->strFilename)){
			pContentsInfo->bLocalFileExist = true;
		}
		else if(pContentsInfo->strFilename.IsEmpty() && 
			(pContentsInfo->nContentsType==CONTENTS_SMS||pContentsInfo->nContentsType==CONTENTS_TICKER||
			 pContentsInfo->nContentsType==CONTENTS_TEXT||pContentsInfo->nContentsType==CONTENTS_TV||
			 pContentsInfo->nContentsType==CONTENTS_RSS||pContentsInfo->nContentsType==CONTENTS_WIZARD||
			 pContentsInfo->nContentsType==CONTENTS_WEBBROWSER))
		{
			pContentsInfo->bLocalFileExist = true;
		}
		else{
			pContentsInfo->bLocalFileExist = false;
		}
	}

	// 변경사항을 체크하기위해 백업본을 생성한다.
	CDataContainer::getInstance()->CreateBackupData();

	// Document 상태저장
	m_bNew = UbcIniPtr()->GetProfileInt(_T("TemporaryDocInfo"), _T("New"));
	m_bOpened = UbcIniPtr()->GetProfileInt(_T("TemporaryDocInfo"), _T("Opened"));
	m_strHostName = UbcIniPtr()->GetProfileString(_T("TemporaryDocInfo"), _T("HostName"));
	m_strSourceDrive = UbcIniPtr()->GetProfileString(_T("TemporaryDocInfo"), _T("SourceDrive"));
	m_strTargetDrive = UbcIniPtr()->GetProfileString(_T("TemporaryDocInfo"), _T("TargetDrive"));
	m_strSite = UbcIniPtr()->GetProfileString(_T("TemporaryDocInfo"), _T("Site"));

	// Env 상태저장
	GetEnvPtr()->m_strPackage = UbcIniPtr()->GetProfileString(_T("TemporaryEnvInfo"), _T("PackageName"));
	GetEnvPtr()->m_PackageInfo.bShow = UbcIniPtr()->GetProfileInt(_T("TemporaryEnvInfo"), _T("Show"));
	GetEnvPtr()->m_PackageInfo.bModify = UbcIniPtr()->GetProfileInt(_T("TemporaryEnvInfo"), _T("Modify"));
	GetEnvPtr()->m_PackageInfo.tmUpdateTime = _ttoi64(UbcIniPtr()->GetProfileString(_T("TemporaryEnvInfo"), _T("tmUpdateTime")));
	GetEnvPtr()->m_PackageInfo.szDrive = UbcIniPtr()->GetProfileString(_T("TemporaryEnvInfo"), _T("Drive"));
	GetEnvPtr()->m_PackageInfo.szProcID = UbcIniPtr()->GetProfileString(_T("TemporaryEnvInfo"), _T("ProcID"));
	GetEnvPtr()->m_PackageInfo.szSiteID = UbcIniPtr()->GetProfileString(_T("TemporaryEnvInfo"), _T("SiteID"));
	GetEnvPtr()->m_PackageInfo.szPackage = UbcIniPtr()->GetProfileString(_T("TemporaryEnvInfo"), _T("Package"));
	GetEnvPtr()->m_PackageInfo.szDescript = UbcIniPtr()->GetProfileString(_T("TemporaryEnvInfo"), _T("Descript"));
	GetEnvPtr()->m_PackageInfo.szUpdateUser = UbcIniPtr()->GetProfileString(_T("TemporaryEnvInfo"), _T("UpdateUser"));
	GetEnvPtr()->m_PackageInfo.szUpdateTime = UbcIniPtr()->GetProfileString(_T("TemporaryEnvInfo"), _T("szUpdateTime"));

	CWnd* wnd = ::AfxGetMainWnd();
	if(wnd->GetSafeHwnd())
	{
		wnd->SendMessage(WM_TEMPLATE_INFO_CHANGED);
		wnd->SendMessage(WM_TEMPLATE_LIST_CHANGED);
		wnd->SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED);
		wnd->SendMessage(WM_PLAYCONTENTS_LIST_CHANGED);
		wnd->SendMessage(WM_CONTENTS_LIST_CHANGED, 1);
	}
}

// 0001128: Studio 임시 저장 및 복구
void CUBCStudioDoc::SaveToTemporaryFile()
{
	if(!m_bOpened) return;

	CWaitMessageBox wait;

	CString ini_path;
	ini_path = m_strSourceDrive;
	ini_path.Append(UBC_CONFIG_PATH);
	ini_path.Append(TEMPORARY_SAVE_FILE_KEY);
	ini_path.Append(".ini");

	// 임시 패키지 저장
	CDataContainer::getInstance()->Save(ini_path, FALSE);

	// Document 상태저장
	UbcIniPtr()->WriteProfileInt(_T("TemporaryDocInfo"), _T("New"), m_bNew);
	UbcIniPtr()->WriteProfileInt(_T("TemporaryDocInfo"), _T("Opened"), m_bOpened);
	UbcIniPtr()->WriteProfileString(_T("TemporaryDocInfo"), _T("HostName"), m_strHostName);
	UbcIniPtr()->WriteProfileString(_T("TemporaryDocInfo"), _T("SourceDrive"), m_strSourceDrive);
	UbcIniPtr()->WriteProfileString(_T("TemporaryDocInfo"), _T("TargetDrive"), m_strTargetDrive);
	UbcIniPtr()->WriteProfileString(_T("TemporaryDocInfo"), _T("Site"), m_strSite);

	// Env 상태저장
	UbcIniPtr()->WriteProfileString(_T("TemporaryEnvInfo"), _T("PackageName"), GetEnvPtr()->m_strPackage);
	UbcIniPtr()->WriteProfileInt(_T("TemporaryEnvInfo"), _T("Show"), GetEnvPtr()->m_PackageInfo.bShow);
	UbcIniPtr()->WriteProfileInt(_T("TemporaryEnvInfo"), _T("Modify"), GetEnvPtr()->m_PackageInfo.bModify);
	UbcIniPtr()->WriteProfileString(_T("TemporaryEnvInfo"), _T("tmUpdateTime"), ToString((ULONGLONG)GetEnvPtr()->m_PackageInfo.tmUpdateTime));
	UbcIniPtr()->WriteProfileString(_T("TemporaryEnvInfo"), _T("Drive"), GetEnvPtr()->m_PackageInfo.szDrive);
	UbcIniPtr()->WriteProfileString(_T("TemporaryEnvInfo"), _T("ProcID"), GetEnvPtr()->m_PackageInfo.szProcID);
	UbcIniPtr()->WriteProfileString(_T("TemporaryEnvInfo"), _T("SiteID"), GetEnvPtr()->m_PackageInfo.szSiteID);
	UbcIniPtr()->WriteProfileString(_T("TemporaryEnvInfo"), _T("Package"), GetEnvPtr()->m_PackageInfo.szPackage);
	UbcIniPtr()->WriteProfileString(_T("TemporaryEnvInfo"), _T("Descript"), GetEnvPtr()->m_PackageInfo.szDescript);
	UbcIniPtr()->WriteProfileString(_T("TemporaryEnvInfo"), _T("UpdateUser"), GetEnvPtr()->m_PackageInfo.szUpdateUser);
	UbcIniPtr()->WriteProfileString(_T("TemporaryEnvInfo"), _T("szUpdateTime"), GetEnvPtr()->m_PackageInfo.szUpdateTime);

	UbcIniPtr()->Write();
}

//skpark same_size_file_problem
BOOL CUBCStudioDoc::PathFileIsSame(LPCSTR pszPath, ULONGLONG nSize, CString& modifiedTime)
{
	if( ::PathFileExists(pszPath) == FALSE ) return FALSE;

	CFileStatus fs;
	if( CFile::GetStatus(pszPath, fs) == FALSE) return FALSE;

	if( fs.m_size != nSize ) return FALSE;

	// skpark same_size_file_problem
	CString target = pszPath;
	if(GetEnvPtr()->m_bUseTimeCheck && isNewFile(target,modifiedTime)){
		return FALSE;
	}

	return TRUE;
}

BOOL CUBCStudioDoc::ChangePackageProperty()
{
//	CDriveSelectDialog dlg;
	CChangePackagePropertyDlg dlg;

//	dlg.m_strHostName = m_strHostName;
	dlg.m_strDesc = GetDescription();
//	m_strSite.Empty();

	dlg.m_nCategorys[CDriveSelectDialog::eCategory  ] = CDataContainer::getInstance()->m_nContentsCategory;
	dlg.m_nCategorys[CDriveSelectDialog::ePurpose   ] = CDataContainer::getInstance()->m_nPurpose         ;
	dlg.m_nCategorys[CDriveSelectDialog::eHostType  ] = CDataContainer::getInstance()->m_nHostType        ;
	dlg.m_nCategorys[CDriveSelectDialog::eVertical  ] = CDataContainer::getInstance()->m_nVertical        ;
	dlg.m_nCategorys[CDriveSelectDialog::eResolution] = CDataContainer::getInstance()->m_nResolution      ;
	dlg.m_bIsPublic                                   = CDataContainer::getInstance()->m_bIsPublic        ;
	dlg.m_strValidationDate = CDataContainer::getInstance()->m_strValidationDate;

	dlg.m_bMonitorOn = CDataContainer::getInstance()->m_bMonitorOn;
	dlg.m_bMonitorOff = CDataContainer::getInstance()->m_bMonitorOff;

	if(dlg.DoModal() != IDOK) return false;

//	if(dlg.m_bTemplate)
//	{
//		m_bNew = true;
//	}

//	if(dlg.m_strDrive != m_strSourceDrive)
//	{
//		// 0000736 드라이브가 바뀌는 경우 모든 콘텐츠가 서버로부터 다운로드 완료되어 복사가능한지 확인한다
//		if(!DownloadAllContents(true)) return false;
//	}

//	m_strTargetDrive = dlg.m_strDrive;
//	m_strHostName = dlg.m_strHostName;

//	GetEnvPtr()->m_strPackage = m_strHostName;
//	CDataContainer::getInstance()->m_strNetworkUse = dlg.m_bServer?"1":"0";

	CDataContainer::getInstance()->m_bMonitorOn = dlg.m_bMonitorOn;
	CDataContainer::getInstance()->m_bMonitorOff = dlg.m_bMonitorOff;

//	if(!dlg.m_strSite.IsEmpty())
//	{
//		m_strSite = dlg.m_strSite;
//	}

	//if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		SPackageInfo4Studio Info;

		Info.bShow = true;
		Info.bModify = false;
		Info.szDrive = m_strTargetDrive;
		Info.szSiteID = m_strSite;
		Info.szPackage = m_strHostName;
		Info.szDescript = dlg.m_strDesc;
		Info.szUpdateUser = GetEnvPtr()->m_szLoginID;

		SYSTEMTIME st;
		::GetLocalTime(&st);
		CTime tmCur(st);
		Info.szUpdateTime = tmCur.Format(STR_ENV_TIME);
		Info.tmUpdateTime = tmCur.GetTime();

		// 패키지객체 속성추가
		Info.nContentsCategory = dlg.m_nCategorys[CDriveSelectDialog::eCategory  ];
		Info.nPurpose          = dlg.m_nCategorys[CDriveSelectDialog::ePurpose   ];
		Info.nHostType         = dlg.m_nCategorys[CDriveSelectDialog::eHostType  ];
		Info.nVertical         = dlg.m_nCategorys[CDriveSelectDialog::eVertical  ];
		Info.nResolution       = dlg.m_nCategorys[CDriveSelectDialog::eResolution];
		Info.bIsPublic         = dlg.m_bIsPublic        ;

		// todo 패키지 유효기간을 저장시 설정할지 매니져에서 관리자가 설정할지 결정해야함.
		COleDateTime tmTemp;
		tmTemp.ParseDateTime(dlg.m_strValidationDate);
		Info.tmValidationDate  = tmTemp;
		Info.strValidationDate = dlg.m_strValidationDate;

		GetEnvPtr()->ChangeCurPackage(Info);
	}

	SetDescription(dlg.m_strDesc);

	// 패키지객체 속성추가
	CDataContainer::getInstance()->m_nContentsCategory = GetEnvPtr()->m_PackageInfo.nContentsCategory;
	CDataContainer::getInstance()->m_nPurpose          = GetEnvPtr()->m_PackageInfo.nPurpose         ;
	CDataContainer::getInstance()->m_nHostType         = GetEnvPtr()->m_PackageInfo.nHostType        ;
	CDataContainer::getInstance()->m_nVertical         = GetEnvPtr()->m_PackageInfo.nVertical        ;
	CDataContainer::getInstance()->m_nResolution       = GetEnvPtr()->m_PackageInfo.nResolution      ;
	CDataContainer::getInstance()->m_bIsPublic         = GetEnvPtr()->m_PackageInfo.bIsPublic        ;
	CDataContainer::getInstance()->m_bIsVerify         = GetEnvPtr()->m_PackageInfo.bIsVerify        ;
	CDataContainer::getInstance()->m_strValidationDate = GetEnvPtr()->m_PackageInfo.strValidationDate;

//	if(m_strTargetDrive != m_strSourceDrive)
//	{
//		CFileCopyDialog fc_dialog(::AfxGetMainWnd(), dlg.m_strDrive);
//		fc_dialog.DoModal();
//	}
//	else
	//{
	//	CWnd* pWnd = ::AfxGetMainWnd();
	//	if( pWnd->GetSafeHwnd() )
	//	{
	//		pWnd->PostMessage(WM_COMPLETE_FILE_COPY, 1, 0); // all ok
	//	}//if
	//}//if


	return TRUE;
}
