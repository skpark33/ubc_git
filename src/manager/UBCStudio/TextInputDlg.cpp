// TextInputDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TextInputDlg.h"


// CTextInputDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTextInputDlg, CWizardSubContents)

CTextInputDlg::CTextInputDlg(CWnd* pParent /*=NULL*/)
	: CWizardSubContents(CTextInputDlg::IDD, pParent)
	, m_reposControl(this)
{

}

CTextInputDlg::~CTextInputDlg()
{
}

void CTextInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CWizardSubContents::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EB_TEXT_DATA, m_ebTextData);
}


BEGIN_MESSAGE_MAP(CTextInputDlg, CWizardSubContents)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CTextInputDlg 메시지 처리기입니다.

void CTextInputDlg::SetData(CString strContentsId, CString strValue)
{
	m_ebTextData.SetWindowText(strValue);
}

void CTextInputDlg::GetData(CString& strContentsId, CString& strValue)
{
	strContentsId = "";
	m_ebTextData.GetWindowText(strValue);
}

void CTextInputDlg::SetPreviewMode(bool bPreviewMode)
{
	CWizardSubContents::SetPreviewMode(bPreviewMode);
	m_ebTextData.SetReadOnly(bPreviewMode);
}

BOOL CTextInputDlg::OnInitDialog()
{
	CWizardSubContents::OnInitDialog();

	m_ebTextData.SetWindowText("");

	m_reposControl.AddControl(&m_ebTextData, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.Move();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CTextInputDlg::OnSize(UINT nType, int cx, int cy)
{
	CWizardSubContents::OnSize(nType, cx, cy);

	m_reposControl.Move();
}
