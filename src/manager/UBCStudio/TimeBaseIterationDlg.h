#pragma once
#include "resource.h"

// CTimeBaseIterationDlg 대화 상자입니다.

class CTimeBaseIterationDlg : public CDialog
{
	DECLARE_DYNAMIC(CTimeBaseIterationDlg)

public:
	CTimeBaseIterationDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTimeBaseIterationDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_TIMEBASE_ITERATION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CString m_strIterPeriod;
	CString m_strIterCount;
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();

	int	GetIterPeriod() { return atoi(m_strIterPeriod); }
	int	GetIterCount() { return atoi(m_strIterCount); }
};
