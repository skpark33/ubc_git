#pragma once
#include "afxcmn.h"

// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업
#include "common/HoverButton.h"
#include "afxwin.h"
// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업
//#include "CheckListCtrl.h"
#include "common\utblistctrl.h"

class CNetDeviceInfo
{
public:
	CNetDeviceInfo()			{	m_strDeviceName = "";
									m_strIP = "";
									m_strID = "";
									m_strPWD = "";
									m_bShutdownAuto = FALSE;
									m_strShutdownTime = "";
									m_bShutdownWeekday = FALSE;
									m_strWeekShutdownTime = "";
									m_strUserCreate = "";
									m_strErrorMsg = "";
									m_strMacAddr = "";
									m_strAlive = "";
									m_strDescript = "";
									m_nMonitorCnt = 0;
									m_nFTPPort = 0;
									m_nSvrPort = 0;
									m_nErrorCode = 0;
								};
	virtual~CNetDeviceInfo()	{};

	CNetDeviceInfo& operator= (const CNetDeviceInfo& info)
	{
		m_strDeviceName		= info.m_strDeviceName;
		m_strIP				= info.m_strIP;
		m_strID				= info.m_strID;
		m_strPWD			= info.m_strPWD;
		m_bShutdownAuto     = info.m_bShutdownAuto;
		m_strShutdownTime	= info.m_strShutdownTime;
		m_bShutdownWeekday  = info.m_bShutdownWeekday;
		m_strWeekShutdownTime = info.m_strWeekShutdownTime;
		m_strUserCreate		= info.m_strUserCreate;
		m_strErrorMsg		= info.m_strErrorMsg;
		m_strAlive			= info.m_strAlive;
		m_strDescript		= info.m_strDescript;
		m_nFTPPort			= info.m_nFTPPort;
		m_nSvrPort			= info.m_nSvrPort;
		m_nErrorCode		= info.m_nErrorCode;

		return *this;
	}

	CString		m_strDeviceName;
	CString		m_strIP;
	CString		m_strID;
	CString		m_strPWD;
	BOOL		m_bShutdownAuto;
	CString		m_strShutdownTime;
	BOOL		m_bShutdownWeekday;
	CString     m_strWeekShutdownTime;
	CString		m_strUserCreate;
	CString		m_strErrorMsg;
	CString		m_strMacAddr;
	CString		m_strAlive;
	CString		m_strDescript;
	int			m_nMonitorCnt;
	int			m_nFTPPort;
	int			m_nSvrPort;
	int			m_nErrorCode;
};


// CNetworkHostDlg 대화 상자입니다.

class CNetworkHostDlg : public CDialog
{
	DECLARE_DYNAMIC(CNetworkHostDlg)

	static int CALLBACK CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

public:
	CNetworkHostDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNetworkHostDlg();

	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;
	CHoverButton	m_btnRefresh;
	CHoverButton	m_btnEdit;
	CPtrArray		m_aryNetDevice;
	CPtrArray		m_arySelectedNetInfo;
	CImageList		m_imgCheck;
	CUTBListCtrl	m_lcList;

	CButton			m_chkAll;
	BOOL			m_bAll;
	int				m_nFindHostType;

	void			InitNetListCtrl();
	void			LoadNetDeviceInfo(void);
	void			ClearNetDeviceArray(void);
	void			ScanNetHost(int nType);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NETWORK_HOST_DLG };
	enum {eCheck, E_HOST, E_DESCRIPT, E_IPADDR, E_DISPLYCNT, E_FTPPORT, E_SVRPORT, E_SDTIME, E_MACADDR, E_USRCREATE};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnNMClickNetworkHostList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkNetworkHostList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedRefreshBtn();
	afx_msg void OnBnClickedEditBtn();
	afx_msg void OnLvnColumnclickHostList(NMHDR *pNMHDR, LRESULT *pResult);
};
