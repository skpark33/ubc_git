#pragma once

#include "afxwin.h"
#include "common/HoverButton.h"


// CRenameContentsFileDlg 대화 상자입니다.

class CRenameContentsFileDlg : public CDialog
{
	DECLARE_DYNAMIC(CRenameContentsFileDlg)

public:
	CRenameContentsFileDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRenameContentsFileDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CONTENTS_RENAME_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;

	CString			m_strNewName;		///<새로 지정되는 파일 이름
	CString			m_strOldFileName;	///<변경전의 이전 파일 이름
	CString			m_strMsg;			///<상단에 출력되는 메시지
	CString			m_strENCPath;		///<콘텐츠 폴더의 경로

	void	MakeAvailableName(void);	///<사용이 가능한 파일이름을 만들어 준다

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
