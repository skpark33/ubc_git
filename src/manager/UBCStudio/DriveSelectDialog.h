#pragma once
#include "afxcmn.h"

#include "CheckListCtrl.h"
#include "afxwin.h"

#include "common/HoverButton.h"
#include "common/UTBListCtrlEx.h"
#include "afxdtctl.h"

// CDriveSelectDialog 대화 상자입니다.

class CDriveSelectDialog : public CDialog
{
	DECLARE_DYNAMIC(CDriveSelectDialog)

public:
	CDriveSelectDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDriveSelectDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DRIVE_SELECT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	CImageList	m_ilDrives;

	CMapStringToString	m_mapVolumeToLabel;
	CMapStringToString	m_mapDriveToLabel;

	int	GetIniNames(CString& myName, CString& targetPath, CStringArray& outList);

public:

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnNMDblclkListDrives(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnSetfocusEditHostName();
	afx_msg void OnLvnItemchangedListDrives(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListDrives(NMHDR *pNMHDR, LRESULT *pResult);

	CListCtrl		m_lcDrives;
	CEdit			m_editHostName;
	CEdit			m_editDescription;
	CString			m_strSite;
	CString			m_strHostName;
	CString			m_strDrive;
	CString			m_strDesc;
	bool			m_bTemplate;
	bool			m_bServer;

	enum { eCategory	// 종류 (모닝,..)
		 , ePurpose		// 용도별 (교육용,..)
		 , eHostType	// 단말타입 (키오스크..)
		 , eVertical	// 가로/세로 방향 (가로,..)
		 , eResolution	// 해상도 (1920x1080,..)
		 , eMaxCnt };
	CUTBListCtrlEx	m_lcCategory[eMaxCnt];
	LONG			m_nCategorys[eMaxCnt];
	CButton m_kbPublic;

	void InitCategoryListCtrl();

	// 패키지객체 속성추가
	bool			m_bIsPublic        ;	// 공개여부
	CString			m_strValidationDate;	// 패키지유효기간

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	//CHoverButton	m_btnSearchAllDrive;
	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	void	GetProcessVolume ();
	BOOL	_GetProcessVolume (HANDLE hVol, char* lpBuf, int iBufSize);
	void	InitDriveLabel();

	bool	IsExistFile(CString strPath, CString strFileName, CString& strOutFileName);
	void	InitDriveListCtrl();
	afx_msg void OnBnClickedOk();
	CDateTimeCtrl m_dtValidationDate;
	afx_msg void OnBnClickedBnSite();
	CButton m_ckMonitorOn;
	CButton m_ckMonitorOff;
	bool	m_bMonitorOn;
	bool	m_bMonitorOff;
	afx_msg void OnBnClickedCheckMonitoron();
	afx_msg void OnBnClickedCheckMonitoroff();
};


