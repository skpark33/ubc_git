#pragma once
#include "afxwin.h"
#include "resource.h"
#include "PictureEx.h"

// CNotifySaveDlg 대화 상자입니다.

class CNotifySaveDlg : public CDialog
{
	DECLARE_DYNAMIC(CNotifySaveDlg)

public:
	// 대화 상자 데이터입니다.
	enum { IDD = IDD_NOTIFY_SAVE_DLG };
	enum EMode{ ePreview, eSavePackage, eLoadPackage, eDownPackage, eUpPackage, eDownContents, eUpContents };

	CNotifySaveDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNotifySaveDlg();

	void SetMode(EMode);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	CPictureEx	m_Image;
	EMode		m_Mode;

	DECLARE_MESSAGE_MAP()
public:	
	virtual BOOL OnInitDialog();
};
