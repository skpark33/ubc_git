#pragma once

#include "CheckListCtrl.h"
#include "afxwin.h"

#include "common/HoverButton.h"
#include "common/UTBListCtrlEx.h"
#include "afxdtctl.h"


// CChangePackagePropertyDlg 대화 상자입니다.

class CChangePackagePropertyDlg : public CDialog
{
	DECLARE_DYNAMIC(CChangePackagePropertyDlg)

public:
	CChangePackagePropertyDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CChangePackagePropertyDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CHANGE_PACKAGE_PROPERTY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

public:

	CEdit			m_editDescription;
	CString			m_strDesc;

	enum { eCategory	// 종류 (모닝,..)
		 , ePurpose		// 용도별 (교육용,..)
		 , eHostType	// 단말타입 (키오스크..)
		 , eVertical	// 가로/세로 방향 (가로,..)
		 , eResolution	// 해상도 (1920x1080,..)
		 , eMaxCnt };
	CUTBListCtrlEx	m_lcCategory[eMaxCnt];
	LONG			m_nCategorys[eMaxCnt];
	CButton m_kbPublic;

	void InitCategoryListCtrl();

	// 패키지객체 속성추가
	bool			m_bIsPublic        ;	// 공개여부
	CString			m_strValidationDate;	// 패키지유효기간

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	//CHoverButton	m_btnSearchAllDrive;
	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	afx_msg void OnBnClickedOk();
	CDateTimeCtrl m_dtValidationDate;

	CButton m_ckMonitorOn;
	CButton m_ckMonitorOff;
	bool	m_bMonitorOn;
	bool	m_bMonitorOff;
	afx_msg void OnBnClickedCheckMonitoron();
	afx_msg void OnBnClickedCheckMonitoroff();
};
