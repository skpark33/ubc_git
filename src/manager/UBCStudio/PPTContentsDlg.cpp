// PPTContentsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "PPTContentsDlg.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "common/MD5Util.h"

// CPPTContentsDlg dialog

IMPLEMENT_DYNAMIC(CPPTContentsDlg, CSubContentsDialog)

CPPTContentsDlg::CPPTContentsDlg(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CPPTContentsDlg::IDD, pParent)
	, m_wndContents(NULL, 0, false)
{
}

CPPTContentsDlg::~CPPTContentsDlg()
{
}

void CPPTContentsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_FILE_SIZE, m_staticContentsFileSize);
	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_groupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_CONTENTS_FILE, m_btnBrowseContentsFile);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_PLAY, m_btnPreviewPlay);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);
}


BEGIN_MESSAGE_MAP(CPPTContentsDlg, CSubContentsDialog)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, OnBnClickedPermanentCheck)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_PLAY, OnBnClickedButtonPreviewPlay)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_CONTENTS_FILE, OnBnClickedButtonBrowserContentsFile)
END_MESSAGE_MAP()


// CPPTContentsDlg message handlers

BOOL CPPTContentsDlg::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_btnPreviewPlay.LoadBitmap(IDB_BTN_PLAY, RGB(255, 255, 255));
	m_btnBrowseContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btnBrowseContentsFile.SetToolTipText(LoadStringById(IDS_PPTCONTENTSDLG_BUT001));

	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(0);

	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);
	client_rect.DeflateRect(1,1);

	m_wndContents.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndContents.ShowWindow(SW_SHOW);

	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsFileSize);

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}//if

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPPTContentsDlg::OnDestroy()
{
	CSubContentsDialog::OnDestroy();

	// TODO: Add your message handler code here
	Stop();
}

void CPPTContentsDlg::OnSize(UINT nType, int cx, int cy)
{
}

CONTENTS_TYPE CPPTContentsDlg::GetContentsType()
{
	return CONTENTS_PPT;
}

void CPPTContentsDlg::Stop()
{
	m_wndContents.Stop();
}

bool CPPTContentsDlg::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData(TRUE);

	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	//m_editContentsFileName.GetWindowText(info.strLocation);
	info.strLocalLocation = m_strLocation;
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();

	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(info.strLocalLocation, dir, path, filename, ext);

	info.strLocalLocation.Format("%s%s", dir, path); 
	info.strFilename.Format("%s%s", filename, ext); 
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	if(!IsFitContentsName(info.strContentsName)) return false;
	if( info.strLocalLocation.GetLength() == 0 ){
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG002), MB_ICONSTOP);
		return false;
	}
	if( info.strFilename.GetLength() == 0 ){
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG003), MB_ICONSTOP);
		return false;
	}
	if(info.nRunningTime == 0){
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
		return false;
	}

	return true;
}

bool CPPTContentsDlg::SetContentsInfo(CONTENTS_INFO& info)
{
	m_editContentsName.SetWindowText(info.strContentsName);
	//m_editContentsFileName.SetWindowText(info.strLocation + info.strFilename);
	m_editContentsFileName.SetWindowText(info.strFilename);
	m_strLocation = info.strLocalLocation + info.strFilename;
	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

//	if(info.nRunningTime >= 1440){
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
//	}else{
//		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
//		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
//		m_bPermanent = FALSE;
//		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
//		pBtn->SetCheck(FALSE);
//	}

	UpdateData(FALSE);

	LoadContents(info.strLocalLocation + info.strFilename);

	return true;
}

void CPPTContentsDlg::OnBnClickedPermanentCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//24시간을 설정
//	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);
	m_bPermanent = TRUE;

//	if(m_bPermanent){
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
//	}else{
//		m_editContentsPlayMinute.SetWindowText("");
//		m_editContentsPlaySecond.SetWindowText("15");
//
//		m_editContentsPlayMinute.EnableWindow(TRUE);
//		m_editContentsPlaySecond.EnableWindow(TRUE);
//	}
}

void CPPTContentsDlg::OnBnClickedButtonPreviewPlay()
{
	m_wndContents.Play();
}

void CPPTContentsDlg::OnBnClickedButtonBrowserContentsFile()
{
	CString filter;
	CString szFileExts = "*.ppt; *.pps; *.pptx; *.ppsx;";
	filter.Format("All Powerpoint Files|%s|"
				  "Powerpoint Files(*.ppt)|*.ppt|"
				  "Powerpoint Files(*.pptx)|*.pptx|"
				  "Powerpoint Slideshow Files(*.pps)|*.pps|"
				  "Powerpoint Slideshow Files(*.ppsx)|*.ppsx||"
				 ,szFileExts );

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, CONTENTS_PPT))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!CheckTotalContentsSize(dlg.GetPathName())) return;

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = dlg.GetPathName();
	m_editContentsFileName.SetSel(0, -1);

	m_editContentsPlaySecond.SetWindowText("15");

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);
	if(contents_name.GetLength() == 0)
	{
		char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
		_splitpath(dlg.GetPathName(), drive, path, filename, ext);

		m_editContentsName.SetWindowText(filename);
	}

	LoadContents(dlg.GetPathName());

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(m_ulFileSize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
	}
	m_strFileMD5 = CString(szMd5);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

bool CPPTContentsDlg::LoadContents(LPCTSTR lpszFullPath)
{
	CFileStatus fs;
//	if(CFile::GetStatus(lpszFullPath, fs))
	if(CEnviroment::GetFileSize(lpszFullPath, fs.m_size))
	{
		m_staticContentsFileSize.SetWindowText(::ToMoneyTypeString((ULONGLONG)(fs.m_size/1024)) + " ");
		m_ulFileSize = fs.m_size;

		CString szFile = m_strLocation;

		m_wndContents.m_strMediaFullPath = szFile;
		m_wndContents.OpenFile(1);
//		m_wndContents.Play();

		return true;
	}

	return false;
}

BOOL CPPTContentsDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CPPTContentsDlg::OnOK() {}
void CPPTContentsDlg::OnCancel()
{
	GetParent()->PostMessage(WM_CLOSE);
}

void CPPTContentsDlg::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_btnBrowseContentsFile.EnableWindow(bEnable);
	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	GetDlgItem(IDC_PERMANENT_CHECK)->EnableWindow(bEnable);
}
