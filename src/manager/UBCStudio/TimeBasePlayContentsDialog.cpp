// TimeBasePlayContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "TimeBasePlayContentsDialog.h"


// CTimeBasePlayContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTimeBasePlayContentsDialog, CSubPlayContentsDialog)

CTimeBasePlayContentsDialog::CTimeBasePlayContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubPlayContentsDialog(CTimeBasePlayContentsDialog::IDD, pParent)
	,m_pPlayContentsInfo(NULL)
{

}

CTimeBasePlayContentsDialog::~CTimeBasePlayContentsDialog()
{
	if(m_pPlayContentsInfo)
	{
		delete m_pPlayContentsInfo;
	}
}

void CTimeBasePlayContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubPlayContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DATE_START, m_dateStart);
	DDX_Control(pDX, IDC_TIME_START, m_timeStart);
	DDX_Control(pDX, IDC_DATE_END, m_dateEnd);
	DDX_Control(pDX, IDC_TIME_END, m_timeEnd);
}


BEGIN_MESSAGE_MAP(CTimeBasePlayContentsDialog, CSubPlayContentsDialog)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CTimeBasePlayContentsDialog 메시지 처리기입니다.

BOOL CTimeBasePlayContentsDialog::OnInitDialog()
{
	CSubPlayContentsDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_timeStart.SetFormat("HH:mm:ss");
	m_timeEnd.SetFormat("HH:mm:ss");

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CTimeBasePlayContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubPlayContentsDialog::OnOK();
}

void CTimeBasePlayContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubPlayContentsDialog::OnCancel();
}

bool CTimeBasePlayContentsDialog::GetPlayContentsInfo(PLAYCONTENTS_INFO& info, int nIndex)
{
	if(m_pPlayContentsInfo == NULL)
	{
		//return false;
		m_pPlayContentsInfo = new PLAYCONTENTS_INFO;
		*m_pPlayContentsInfo = info;
	}//if

	m_pPlayContentsInfo->bIsDefaultPlayContents = false;

	CTime start_date, end_date;
	m_dateStart.GetTime(start_date);
	m_dateEnd.GetTime(end_date);

	m_pPlayContentsInfo->nStartDate = (int)start_date.GetTime();
	m_pPlayContentsInfo->nEndDate = (int)end_date.GetTime();

	CTime start_time, end_time;
	m_timeStart.GetTime(start_time);
	m_timeEnd.GetTime(end_time);

	m_pPlayContentsInfo->strStartTime= start_time.Format("%H:%M:%S");
	m_pPlayContentsInfo->strEndTime= end_time.Format("%H:%M:%S");

	info = *m_pPlayContentsInfo;

	return true;
}

bool CTimeBasePlayContentsDialog::SetPlayContentsInfo(PLAYCONTENTS_INFO& info)
{
	m_pPlayContentsInfo = new PLAYCONTENTS_INFO;
	*m_pPlayContentsInfo = info;

	if(info.nStartDate > 0)
	{
		//int pos = 0;
		//CString year = info.nStartDate.Tokenize("/:-", pos);
		//CString month= info.nStartDate.Tokenize("/:-", pos);
		//CString day  = info.nStartDate.Tokenize("/:-", pos);

		//CTime start_date(atoi(year), atoi(month), atoi(day), 0,0,0);
		//m_dateStart.SetTime(&start_date);
		CTime start_date(info.nStartDate);
		m_dateStart.SetTime(&start_date);
	}

	if(info.strStartTime.GetLength() > 0)
	{
		int pos = 0;
		CString hour  = info.strStartTime.Tokenize("/:-", pos);
		CString minute= info.strStartTime.Tokenize("/:-", pos);
		CString second= info.strStartTime.Tokenize("/:-", pos);
		//CString second= 0;

		CTime start_time(2000, 1, 1, atoi(hour), atoi(minute), atoi(second));
		m_timeStart.SetTime(&start_time);
	}

	if(info.nEndDate > 0)
	{
		//int pos = 0;
		//CString year = info.nEndDate.Tokenize("/:-", pos);
		//CString month= info.nEndDate.Tokenize("/:-", pos);
		//CString day  = info.nEndDate.Tokenize("/:-", pos);

		//CTime end_date(atoi(year), atoi(month), atoi(day), 0,0,0);
		//m_dateEnd.SetTime(&end_date);
		CTime end_date(info.nEndDate);
		m_dateEnd.SetTime(&end_date);
	}

	if(info.strEndTime.GetLength() > 0)
	{
		int pos = 0;
		CString hour  = info.strEndTime.Tokenize("/:-", pos);
		CString minute= info.strEndTime.Tokenize("/:-", pos);
		CString second= info.strEndTime.Tokenize("/:-", pos);
		//CString second= 0;

		CTime end_time(2000, 1, 1, atoi(hour), atoi(minute), atoi(second));
		m_timeEnd.SetTime(&end_time);

		CTime tmTest;
		m_timeEnd.GetTime(tmTest);
		TRACE(tmTest.Format("%Y/%m/%d %H:%M:%S\n"));
	}

	return true;
}

int CTimeBasePlayContentsDialog::GetPlayContentsCount()
{
	return 1;
}

void CTimeBasePlayContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CSubPlayContentsDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 콘텐츠의 running time을 설정하여 playcontents의 종료 시간을 설정한다 \n
/// @param (ULONG) ulRunningTime : (in) 콘텐츠의 running 시간
/////////////////////////////////////////////////////////////////////////////////
void CTimeBasePlayContentsDialog::SetRunningTime(ULONG ulRunningTime)
{
//	CTime tmEnd;
//	m_timeEnd.GetTime(tmEnd);
	CTime tmStartDate;
	m_dateStart.GetTime(tmStartDate);
	CTime tmStartTime;
	m_timeStart.GetTime(tmStartTime);
	CTime tmStartDateTime(tmStartDate.GetYear()
						, tmStartDate.GetMonth()
						, tmStartDate.GetDay()
						, tmStartTime.GetHour()
						, tmStartTime.GetMinute()
						, tmStartTime.GetSecond()
						);
	CTimeSpan spanRunning(ulRunningTime);
	CTime tmEndDateTime = tmStartDateTime + spanRunning;

	m_dateEnd.SetTime(&tmEndDateTime);
	m_timeEnd.SetTime(&tmEndDateTime);
}

bool CTimeBasePlayContentsDialog::InputErrorCheck()
{
	CTime tmStartDate;
	CTime tmStartTime;
	CTime tmEndDate  ;
	CTime tmEndTime  ;

	m_dateStart.GetTime(tmStartDate);
	m_timeStart.GetTime(tmStartTime);
	m_dateEnd.GetTime(tmEndDate);
	m_timeEnd.GetTime(tmEndTime);

	if( tmStartDate.Format("%Y%m%d") + tmStartTime.Format("%H%M%S") >
		tmEndDate.Format("%Y%m%d") + tmEndTime.Format("%H%M%S") )
	{
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSDIALOG_MSG003), MB_ICONSTOP);
		m_timeStart.SetFocus();
		return false;
	}

	return true;
}