#pragma once
#include "afxcmn.h"
#include "common\utblistctrlex.h"


// CDisplayConditionDlg 대화 상자입니다.

class CDisplayConditionDlg : public CDialog
{
	DECLARE_DYNAMIC(CDisplayConditionDlg)

public:
	CDisplayConditionDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDisplayConditionDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CONDITION_DLG };
	enum { eCheck, eCategory, eMaxCol };


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	void InitList();
	void InitSelected();
	void RefreshList();
	void AddSel(CUTBListCtrlEx& from , CUTBListCtrlEx& to);
	void _AddSel(int nHostIndex, CUTBListCtrlEx& from , CUTBListCtrlEx& to);
	void DelSel(CUTBListCtrlEx& to);
	void _DelSel(int nHostIndex, CUTBListCtrlEx& to);
	void toString(CString& strIncludeCategory,	CString& strExcludeCategory);
	void fromString(CString& strIncludeCategory,	CString& strExcludeCategory);
	
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBnInAdd();
	afx_msg void OnBnClickedBnInDel();
	afx_msg void OnBnClickedBnExAdd();
	afx_msg void OnBnClickedBnExDel();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	
	CUTBListCtrlEx m_lcInList;
	CUTBListCtrlEx m_lcInSelected;
	CUTBListCtrlEx m_lcExList;
	CUTBListCtrlEx m_lcExSelected;

	CStringArray	m_lsInclude;
	CStringArray	m_lsExclude;

protected:
	CString				m_szColum[eMaxCol];

public:
	afx_msg void OnNMDblclkListInCandidate(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListInSelected(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListExCandidate(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListExSelected(NMHDR *pNMHDR, LRESULT *pResult);
};
