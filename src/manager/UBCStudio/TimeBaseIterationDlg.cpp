// TimeBaseIterationDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TimeBaseIterationDlg.h"


// CTimeBaseIterationDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTimeBaseIterationDlg, CDialog)

CTimeBaseIterationDlg::CTimeBaseIterationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTimeBaseIterationDlg::IDD, pParent)
	, m_strIterPeriod(_T(""))
	, m_strIterCount(_T(""))
{

}

CTimeBaseIterationDlg::~CTimeBaseIterationDlg()
{
}

void CTimeBaseIterationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ITER_PERIOD, m_strIterPeriod);
	DDX_Text(pDX, IDC_EDIT_ITER_COUNT, m_strIterCount);
	DDV_MaxChars(pDX, m_strIterPeriod, 4 ); 
	DDV_MaxChars(pDX, m_strIterCount, 4 ); 

}


BEGIN_MESSAGE_MAP(CTimeBaseIterationDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CTimeBaseIterationDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CTimeBaseIterationDlg 메시지 처리기입니다.

void CTimeBaseIterationDlg::OnBnClickedOk()
{
	GetDlgItemText(IDC_EDIT_ITER_PERIOD, m_strIterPeriod);
	GetDlgItemText(IDC_EDIT_ITER_COUNT,  m_strIterCount);

	OnOK();
}

BOOL CTimeBaseIterationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
