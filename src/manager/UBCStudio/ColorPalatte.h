#pragma once

#define FRAME_COLOR_CNT 99

class CColorPalatte
{
protected:
	CColorPalatte(void);
	virtual ~CColorPalatte(void);

	static		CColorPalatte* _instance;
	static		CCriticalSection _cs;

	COLORREF	m_ColorPalatte[FRAME_COLOR_CNT+50];

	void		InitPalatte();

public:
	static		CColorPalatte* getInstance();
	static void clearInstance();

	COLORREF GetFrameColor(int nIndex);
};
