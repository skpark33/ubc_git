#include "stdafx.h"
#include "SimpleFtp.h"
#include <memory>
#include "Enviroment.h"

CSimpleFtp::CSimpleFtp()
{
	m_Stat = eNone;
	m_pFile = NULL;
	m_pConnection = NULL;
	m_pReceiver = NULL;
	
	m_pThread = NULL;
	m_bThread = false;

	m_iPort = 21;
	m_szIP.Empty();
	m_szID.Empty();
	m_szPW.Empty();

	m_nCurTime = ::GetTickCount();
	m_bTimeOut = false;
	m_nTimeOut = 60000;
	TraceLog(("new CSimpleFtp()"));
}

CSimpleFtp::~CSimpleFtp()
{
	Destroy();
	TraceLog(("delete CSimpleFtp()"));
}

bool CSimpleFtp::Connect(LPCTSTR szIP, LPCTSTR szID, LPCTSTR szPW, int iPort)
{
	Close();

	m_szIP = szIP;
	m_szID = szID;
	m_szPW = szPW;
	m_iPort = iPort;

	try{
		TraceLog(("Connect(%s, %s)", szIP, szID));
		m_pConnection = m_Session.GetFtpConnection(szIP, szID, szPW, iPort);
	}catch (CInternetException* pEx){
		pEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		TraceLog(("Internet exception !!!\n\t%s", m_szException));
		pEx->Delete();

		m_pConnection = NULL;
		return false;
	}

	if(!m_pConnection){
		TraceLog(("Connect(Failed)"));
		return false;
	}

	if(!m_pThread){
		m_bThread = true;
		m_nCurTime = ::GetTickCount();
		m_pThread = AfxBeginThread(TimeProc, (LPVOID)this);
	}

	TraceLog(("Connect(Succeed)"));
	return true;
}

bool CSimpleFtp::ReConnect()
{
	TraceLog(("ReConnect()"));
	return Connect(m_szIP, m_szID, m_szPW, m_iPort);
}

void CSimpleFtp::Destroy()
{
	if(m_pThread){
		m_bThread = false;
		WaitForSingleObject(m_pThread->m_hThread, 5000);
		m_pThread = NULL;
	}

	Close();

	m_Stat = eNone;
	TraceLog(("Destroy()"));
	return;
}

void CSimpleFtp::Close()
{
	try{
		if(m_pConnection){
			m_pConnection->Close();
			delete m_pConnection;
			m_pConnection = NULL;
		}
	}catch (CInternetException* pEx){
		pEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		TraceLog(("Internet exception !!!\n\t%s", m_szException));
		pEx->Delete();
	}
	TraceLog(("Close()"));
}

void CSimpleFtp::SetTimeOut(DWORD Sec)
{
	m_nTimeOut = Sec * 1000;
}

void CSimpleFtp::SetReceiver(ISpFtp* pReceiver)
{
	m_pReceiver = pReceiver;
}

bool CSimpleFtp::IsTimeOut()
{
	m_nCurTime = ::GetTickCount();
	return m_bTimeOut;
}

LPCTSTR CSimpleFtp::GetErrMsg()
{
	return m_szException;
}

int CSimpleFtp::GetFile(LPCTSTR szFile, LPCTSTR szSPath, LPCTSTR szTPath, bool bOverWrite)
{
	if(!m_pConnection){
		TraceLog(("GetFile(Disconnected)"));
		return eDisconnect;
	}

	BOOL bRet = TRUE;
	CString szLocalFile, szRemoteFile;
	szRemoteFile = szSPath;
	szRemoteFile += szFile;
	szLocalFile = szTPath;
	szLocalFile += szFile;

	m_Stat = eGetFile;
	m_nCurTime = ::GetTickCount();
	m_bTimeOut = false;

	try{
		CFileFind ffLocal;
		CFtpFileFind ffRemote(m_pConnection);

		ULONGLONG ullLocalSize = 0;
		ULONGLONG ullRemoteSize = 0;

		if(!ffRemote.FindFile(szRemoteFile)){
			TraceLog(("Remote File Not Found"));
			return eRemoteFileNotFound;
		}

		ffRemote.FindNextFile();
		ullRemoteSize = ffRemote.GetLength();
		ffRemote.Close();

		if(!bOverWrite){
			if(ffLocal.FindFile(szLocalFile)){
				ffLocal.FindNextFile();
				ullLocalSize = ffLocal.GetLength();
				ffLocal.Close();
				
				if(ullLocalSize == ullRemoteSize){
					TraceLog(("Already Exist File"));
					return eAlreadyExistFile;
				}
			}
		}

		m_nCurTime = ::GetTickCount();
		m_bTimeOut = false;
		TraceLog(("GetFile(%s)", szFile));
		bRet = m_pConnection->GetFile(szRemoteFile, szLocalFile, false, FILE_ATTRIBUTE_NORMAL,FTP_TRANSFER_TYPE_BINARY);
	}catch (CInternetException* pEx){
		pEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		pEx->Delete();
		TraceLog(("Internet exception !!!\n\t%s", m_szException));
		return efail;
	}catch (CFileException* pFEx){
		pFEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		pFEx->Delete();
		TraceLog(("File exception !!!\n\t%s", m_szException));
		return efail;
	}

	if(!bRet){
		TraceLog(("GetFile(Unknown Error)"));
		return eUnknownError;
	}

	TraceLog(("GetFile(Succeed)"));
	return eSuccess;
}

int CSimpleFtp::PutFile(LPCTSTR szFile, LPCTSTR szSPath, LPCTSTR szTPath, bool bOverWrite)
{
	if(!m_pConnection){
		TraceLog(("PutFile(Disconnected)"));
		return eDisconnect;
	}

	BOOL bRet = TRUE;
	CString szLocalFile, szRemoteFile;
	szRemoteFile = szTPath;
	szRemoteFile += szFile;
	szLocalFile = szSPath;
	szLocalFile += szFile;

	m_Stat = ePutFile;
	m_nCurTime = ::GetTickCount();
	m_bTimeOut = false;

	try{
		CFileFind ffLocal;
		CFtpFileFind ffRemote(m_pConnection);

		ULONGLONG ullLocalSize = 0;
		ULONGLONG ullRemoteSize = 0;

		if(!ffLocal.FindFile(szLocalFile)){
			TraceLog(("PutFile(Local File Not Found)"));
			return eLocalFileNotFound;
		}

		ffLocal.FindNextFile();
		ullLocalSize = ffLocal.GetLength();
		ffLocal.Close();

		if(!bOverWrite){
			if(ffRemote.FindFile(szRemoteFile)){
				ffRemote.FindNextFile();
				ullRemoteSize = ffRemote.GetLength();
				ffRemote.Close();

				if(ullLocalSize == ullRemoteSize){
					TraceLog(("PutFile(Already Exist File)"));
					return eAlreadyExistFile;
				}
			}
		}

		m_nCurTime = ::GetTickCount();
		m_bTimeOut = false;

		TraceLog(("CreateDirectory(%s)",szTPath));
		m_pConnection->CreateDirectory(szTPath);

		TraceLog(("PutFile(%s)",szFile));
		bRet = m_pConnection->PutFile(szLocalFile, szRemoteFile);
	}catch (CInternetException* pEx){
		pEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		pEx->Delete();
		TraceLog(("Internet exception !!!\n\t%s", m_szException));
		return efail;
	}catch (CFileException* pFEx){
		pFEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		pFEx->Delete();
		TraceLog(("File exception !!!\n\t%s", m_szException));
		return efail;
	}

	if(!bRet){
		TraceLog(("PutFile(Unknown Error)"));
		return eUnknownError;
	}

	TraceLog(("PutFile(Succeed)"));
	return eSuccess;
}

int CSimpleFtp::Download(LPCTSTR szFile, LPCTSTR szSPath, LPCTSTR szTPath, bool bOverWrite)
{
	if(!m_pConnection){
		TraceLog(("Download(Disconnected)"));
		return eDisconnect;
	}

	CString szLocalFile, szRemoteFile;
	szRemoteFile = szSPath;
	szRemoteFile += szFile;
	szLocalFile = szTPath;
	szLocalFile += szFile;

	m_Stat = eDownload;
	m_nCurTime = ::GetTickCount();
	m_bTimeOut = false;

	try{
		CFileFind ffLocal;
		CFtpFileFind ffRemote(m_pConnection);

		ULONGLONG ullLocalSize = 0;
		ULONGLONG ullRemoteSize = 0;

		if(!ffRemote.FindFile(szRemoteFile)){
			TraceLog(("Download(Remote File Not Found)"));
			return eRemoteFileNotFound;
		}

		ffRemote.FindNextFile();
		ullRemoteSize = ffRemote.GetLength();
		ffRemote.Close();

		if(!bOverWrite){
			if(ffLocal.FindFile(szLocalFile)){
				ffLocal.FindNextFile();
				ullLocalSize = ffLocal.GetLength();
				ffLocal.Close();
				
				if(ullLocalSize == ullRemoteSize){
					TraceLog(("Download(Already Exist File)"));
					return eAlreadyExistFile;
				}
			}
		}

		CFile fLocal;
		if(!fLocal.Open(szLocalFile, CFile::modeCreate|CFile::modeWrite)){
			TraceLog(("Download(Local Open Fail)"));
			return eLocalOpenFail;
		}

//		std::auto_ptr<CInternetFile> pfRemote;
		m_pFile = m_pConnection->OpenFile(szRemoteFile, GENERIC_READ, FTP_TRANSFER_TYPE_BINARY, 1);
		if(!m_pFile){
			fLocal.Close();
			TraceLog(("Download(Remote Open Fail)"));
			return eRemoteOpenFail;
		}
//		pfRemote.reset(m_pFile);
		
		UINT nRead;
		DWORD dwPerMill = 0;
		ULONGLONG ullFileSize = 0;
		BYTE buf[SPFTP_BUF_SIZE];

		m_nCurTime = ::GetTickCount();
		m_bTimeOut = false;

//		while((nRead = pfRemote->Read(buf, sizeof(buf))) != 0){
		while((nRead = m_pFile->Read(buf, sizeof(buf))) != 0){
			fLocal.Write(&buf, nRead);
			m_nCurTime = ::GetTickCount();
			
			if(m_pReceiver){
				ullFileSize += nRead;

				if(ullRemoteSize)
					dwPerMill = (ullFileSize*1000)/ullRemoteSize;
				else
					dwPerMill = 1000;
				
				m_pReceiver->InvokeSize(ullFileSize, dwPerMill);
			}
		}
		m_nCurTime = ::GetTickCount();
		fLocal.Close();
		if(m_pFile){
//			pfRemote->Close();
			m_pFile->Close();
			delete m_pFile;
			m_pFile = NULL;
		}

	}catch (CInternetException* pEx){
		pEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		pEx->Delete();
		TraceLog(("Internet exception !!!\n\t%s", m_szException));
		return efail;
	}catch (CFileException* pFEx){
		pFEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		pFEx->Delete();
		TraceLog(("File exception !!!\n\t%s", m_szException));
		return efail;
	}

	TraceLog(("Download(Succeed)"));
	return eSuccess;
}

int CSimpleFtp::Upload(LPCTSTR szFile, LPCTSTR szSPath, LPCTSTR szTPath, bool bOverWrite)
{
	if(!m_pConnection){
		TraceLog(("Upload(Disconnected)"));
		return eDisconnect;
	}

	CString szLocalFile, szRemoteFile;
	szRemoteFile = szTPath;
	szRemoteFile += szFile;
	szLocalFile = szSPath;
	szLocalFile += szFile;

	m_Stat = eUpload;
	m_nCurTime = ::GetTickCount();
	m_bTimeOut = false;

	try{
		CFileFind ffLocal;
		CFtpFileFind ffRemote(m_pConnection);

		ULONGLONG ullLocalSize = 0;
		ULONGLONG ullRemoteSize = 0;

		if(!ffLocal.FindFile(szLocalFile)){
			TraceLog(("Upload(LocalFile Not Found)"));
			return eLocalFileNotFound;
		}

		ffLocal.FindNextFile();
		ullLocalSize = ffLocal.GetLength();
		ffLocal.Close();

		if(!bOverWrite){
			if(ffRemote.FindFile(szRemoteFile)){
				ffRemote.FindNextFile();
				ullRemoteSize = ffRemote.GetLength();
				ffRemote.Close();

				if(ullLocalSize == ullRemoteSize){
					TraceLog(("Upload(Already Exist File)"));
					return eAlreadyExistFile;
				}
			}
		}

		CFile fLocal;
		if(!fLocal.Open(szLocalFile, CFile::modeRead|CFile::shareDenyNone|CFile::typeBinary)){
			TraceLog(("Upload(Local Open Fail)"));
			return eLocalOpenFail;
		}

		m_pConnection->CreateDirectory(szTPath);

//		std::auto_ptr<CInternetFile> pfRemote;
		m_pFile = m_pConnection->OpenFile(szRemoteFile, GENERIC_WRITE, FTP_TRANSFER_TYPE_BINARY, 1);
		if(!m_pFile){
			fLocal.Close();
			TraceLog(("Remote Open Fail)"));
			return eRemoteOpenFail;
		}
//		pfRemote.reset(m_pFile);
		
		UINT nRead;
		DWORD dwPerMill = 0;
		ULONGLONG ullFileSize = 0;
		BYTE buf[SPFTP_BUF_SIZE];

		m_nCurTime = ::GetTickCount();
		m_bTimeOut = false;

		while((nRead = fLocal.Read(buf, sizeof(buf))) != 0){
//			pfRemote->Write(&buf, nRead);
			m_pFile->Write(&buf, nRead);
			m_nCurTime = ::GetTickCount();

			if(m_pReceiver){
				ullFileSize += nRead;
				
				if(ullLocalSize)
					dwPerMill = (ullFileSize*1000)/ullLocalSize;
				else
					dwPerMill = 1000;

				m_pReceiver->InvokeSize(ullFileSize, dwPerMill);
			}
		}
		m_nCurTime = ::GetTickCount();
		if(m_pFile){
//			pfRemote->Close();
			m_pFile->Close();
			delete m_pFile;
			m_pFile = NULL;
		}
		fLocal.Close();

	}catch (CInternetException* pEx){
		pEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		pEx->Delete();
		TraceLog(("Internet exception !!!\n\t%s", m_szException));
		return efail;
	}catch (CFileException* pFEx){
		pFEx->GetErrorMessage(m_szException, SPFTP_EXC_LEN);
		pFEx->Delete();
		TraceLog(("File exception !!!\n\t%s", m_szException));
		return efail;
	}

	TraceLog(("Upload(Succeed)"));
	return eSuccess;
}

UINT CSimpleFtp::TimeProc(LPVOID pVoid)
{
	CSimpleFtp* pFtp = (CSimpleFtp*)pVoid;
	if(!pFtp){
		AfxEndThread(0);
		return 0;
	}
	TraceLog(("TimeProc(Start)"));


	while(pFtp->m_bThread){
		Sleep(500);
		if(::GetTickCount()-pFtp->m_nCurTime > pFtp->m_nTimeOut){
			if(!pFtp->m_bTimeOut){
				pFtp->m_bTimeOut = true;
				TraceLog(("TimeProc(Time Out)"));
			
				try{
					if(pFtp->m_Stat==eDownload||pFtp->m_Stat==eUpload){
						if(pFtp->m_pFile){
							pFtp->m_pFile->Close();
							delete pFtp->m_pFile;
							pFtp->m_pFile = NULL;
							TraceLog(("CInternetFile::Close()"));
						}
						if(pFtp->m_pConnection){
							pFtp->Close();
							TraceLog(("CFtpConnection::Close()"));
						}
					}
				}catch (CInternetException* pEx){
					pEx->GetErrorMessage(pFtp->m_szException, SPFTP_EXC_LEN);
					pEx->Delete();
					TraceLog(("Internet exception !!!\n\t%s", pFtp->m_szException));
				}
			}
		}
	}

	TraceLog(("TimeProc(Exit)"));
	AfxEndThread(0);
	return 0;
}