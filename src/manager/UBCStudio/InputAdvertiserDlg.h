#pragma once
#include "afxwin.h"
#include "resource.h"


// InputAdvertiserDlg 대화 상자입니다.

class InputAdvertiserDlg : public CDialog
{
	DECLARE_DYNAMIC(InputAdvertiserDlg)

public:
	InputAdvertiserDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~InputAdvertiserDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_INPUT_ADVERTISER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_editName;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	CString m_advertiser;
};
