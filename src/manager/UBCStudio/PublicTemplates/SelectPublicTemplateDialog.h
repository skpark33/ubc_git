#pragma once
#include "afxwin.h"

#include "ReposControl.h"
#include "PublicTemplateWnd.h"

#include "common/HoverButton.h"
#include "common/libHttpRequest/HttpRequest.h"


// CSelectPublicTemplateDialog 대화 상자입니다.

class CSelectPublicTemplateDialog : public CDialog
{
	DECLARE_DYNAMIC(CSelectPublicTemplateDialog)

public:
	CSelectPublicTemplateDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSelectPublicTemplateDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SELECT_PUBLIC_TEMPLATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CReposControl	m_reposControl;

	CPublicTemplateWnd	m_wndTemplate;

public:

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnBnClickedDelete();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	LRESULT	OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam);
	LRESULT	OnTemplateSelected(WPARAM wParam, LPARAM lParam);

	CStatic m_frameTemplate;

	CHoverButton	m_btnDelete;
	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;
	

	//////////////////////////////
	TEMPLATE_LINK*		GetSelectTemplateLink();
};
