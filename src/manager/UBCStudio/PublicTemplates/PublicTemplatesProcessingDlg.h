#pragma once

#include "common/UTBListCtrlEx.h"

#include "etc.h"


// CPublicTemplatesProcessingDlg 대화 상자입니다.

class CPublicTemplatesProcessingDlg : public CDialog
{
	DECLARE_DYNAMIC(CPublicTemplatesProcessingDlg)

public:
	CPublicTemplatesProcessingDlg(LPCSTR szUserId, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPublicTemplatesProcessingDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PROCESSING };

protected:
	enum {
		COLUMN_ID = 0,	// 컨텐츠 이름
		COLUMN_STATUS,		// 진행상태(%)
		COLUMN_RESULT,		// 전송결과
		COLUMN_COUNT,
	};

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CWinThread*	m_pThread;
	BOOL		DeleteThread();

	CTemplateObjectList		m_listCopyFromPrivateToPublic;

	CTemplateObjectList*	m_listCurrent;				// ↖현재 templates 리스트 포인터

	CString					m_strServerIP;
	int						m_nServerPort;
	CString					m_strRegisterId;

public:

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnChangeStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeProcessing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompletedAllProcessing(WPARAM wParam, LPARAM lParam);
	afx_msg void	OnLvnKeydownListCommonContents(NMHDR *pNMHDR, LRESULT *pResult);

	CStatic			m_stcStatus;
	CUTBListCtrlEx	m_lcTemplates;
	CButton			m_btnOK;
	CButton			m_btnCancel;

public:

	void	AddCopyFromPrivateToPublicTemplates(TEMPLATE_LINK& info);
	void	AddCopyFromPrivateToPublicTemplates(TEMPLATE_LINK_LIST& info_list);
};


typedef struct {
	HWND		hParentWnd;
	int			nThreadSerial;
	CString		strRegisterId;
	CString		strCustomer;
	CString		strServerIP;
	int			nServerPort;
	CTemplateObjectList*	pListObjList;
} PT_COMMAND_THREAD_PARAM;

static UINT CopyFromPrivateToPublicTemplatesThreadFunc(LPVOID pParam);
