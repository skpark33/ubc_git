// PublicTemplatesProcessingDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "etc.h"
#include "resource.h"
#include "PublicTemplatesProcessingDlg.h"

//#include "ThreadFunc.h"
#include "Enviroment.h"

#include "common/libHttpRequest/HttpRequest.h"


CMutex		g_mutexThreadPTProcessing;
int			g_nThreadPTProcessing = 0;

//extern CString	WEB_SERVER_IP;

// CPublicTemplatesProcessingDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPublicTemplatesProcessingDlg, CDialog)

CPublicTemplatesProcessingDlg::CPublicTemplatesProcessingDlg(LPCSTR szUserId, CWnd* pParent /*=NULL*/)
	: CDialog(CPublicTemplatesProcessingDlg::IDD, pParent)
	, m_strRegisterId ( szUserId )
	, m_pThread ( NULL )
	, m_listCurrent ( NULL )
{
	char szBuf[1000] = {0};
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

	::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", szBuf, sizeof(szBuf), szPath);

	m_strServerIP = szBuf;
	m_nServerPort = 8080;
}

CPublicTemplatesProcessingDlg::~CPublicTemplatesProcessingDlg()
{
	int count = m_listCopyFromPrivateToPublic.GetCount();
	for(int i=0; i<count; i++)
	{
		CTemplateObject* obj = m_listCopyFromPrivateToPublic.GetAt(i);
		if(obj->pTemplateInfo) delete obj->pTemplateInfo;
		for(int j=0; j<obj->arFrameLinkList.GetCount(); j++)
		{
			FRAME_LINK& link = obj->arFrameLinkList.GetAt(j);
			if(link.pFrameInfo) delete link.pFrameInfo;
		}
		delete obj;
	}
	m_listCopyFromPrivateToPublic.RemoveAll();
}

void CPublicTemplatesProcessingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_PROCESSING_STATUS, m_stcStatus);
	DDX_Control(pDX, IDC_LIST_COMMON_CONTENTS, m_lcTemplates);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CPublicTemplatesProcessingDlg, CDialog)
	ON_WM_TIMER()
	ON_MESSAGE(WM_CHANGE_STATUS, OnChangeStatus)
	ON_MESSAGE(WM_CHANGE_PROCESSING, OnChangeProcessing)
	ON_MESSAGE(WM_COMPLETED_ALL_PROCESSING, OnCompletedAllProcessing)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_COMMON_CONTENTS, OnLvnKeydownListCommonContents)
END_MESSAGE_MAP()


// CPublicTemplatesProcessingDlg 메시지 처리기입니다.

BOOL CPublicTemplatesProcessingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//
	m_lcTemplates.SetExtendedStyle(m_lcTemplates.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	LISTCTRL_COLUMN_INFO info[COLUMN_COUNT] = {
		//{ "", idx, width, align },
		{ "Template ID"/*LoadStringById(IDS_CM_LC_COLUMN_FILENAME)*/, COLUMN_ID,    100, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_STATUS),   COLUMN_STATUS,   100, LVCFMT_CENTER },
		{ LoadStringById(IDS_CM_LC_COLUMN_RESULT),   COLUMN_RESULT,   150, LVCFMT_LEFT },
	};

	InitListCtrlColumn(m_lcTemplates, info, COLUMN_COUNT);

	m_lcTemplates.SetProgressBarCol(COLUMN_STATUS);
	m_lcTemplates.InitHeader(IDB_LIST_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcTemplates.SetSortEnable(false);

	//
	m_btnOK.EnableWindow(FALSE);
	m_btnCancel.EnableWindow(FALSE);

	SetTimer(TIMER_INIT_ID, TIMER_INIT_TIME, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPublicTemplatesProcessingDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnOK();
}

void CPublicTemplatesProcessingDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnCancel();

	m_btnOK.EnableWindow(TRUE);
	m_btnCancel.EnableWindow(FALSE);

	DeleteThread();
}

void CPublicTemplatesProcessingDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	DeleteThread();

	if(nIDEvent == TIMER_INIT_ID)
	{
		KillTimer(TIMER_INIT_ID);

		GUARD(g_mutexThreadPTProcessing);

			AFX_THREADPROC thread_func = NULL;
			m_listCurrent = NULL;

			PT_COMMAND_THREAD_PARAM* param = new PT_COMMAND_THREAD_PARAM;
			param->hParentWnd = GetSafeHwnd();
			param->strRegisterId = m_strRegisterId;
			param->strCustomer = GetEnvPtr()->m_strCustomer;
			param->strServerIP = m_strServerIP;
			param->nServerPort = m_nServerPort;
			param->nThreadSerial = g_nThreadPTProcessing;

			if(m_listCopyFromPrivateToPublic.GetCount() > 0)
			{
				m_stcStatus.SetWindowText("Copy to public template"/*LoadStringById(IDS_CM_PROCESSING_TYPE_COPY_TO_COMMON_CONTENTS)*/);
				thread_func = CopyFromPrivateToPublicTemplatesThreadFunc;
				param->pListObjList = &m_listCopyFromPrivateToPublic;
				m_listCurrent = &m_listCopyFromPrivateToPublic;
			}

			if(m_listCurrent && thread_func && param)
			{
				for(int i=0; i<m_listCurrent->GetCount(); i++)
				{
					CTemplateObject* obj = m_listCurrent->GetAt(i);

					obj->processingResultValue = 0;

					int idx = m_lcTemplates.GetItemCount();
					m_lcTemplates.InsertItem(idx, _T(""));

					CString str_id = "Template ";
					str_id += (obj->pTemplateInfo != NULL ? obj->pTemplateInfo->strId : "");

					m_lcTemplates.SetItemText(idx, COLUMN_ID, str_id);
					m_lcTemplates.SetItemText(idx, COLUMN_STATUS, _T("0"));
					m_lcTemplates.SetItemText(idx, COLUMN_RESULT, _T(""));

					m_lcTemplates.SetItemData(idx, (DWORD)obj);
				}

				//
				m_pThread = ::AfxBeginThread(thread_func, param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
				m_pThread->m_bAutoDelete = TRUE;
				m_pThread->ResumeThread();

				//
				m_btnOK.EnableWindow(FALSE);
				m_btnCancel.EnableWindow(TRUE);
			}
			else
			{
				OnOK();
			}
		END_GUARD;
	}
}

BOOL CPublicTemplatesProcessingDlg::DeleteThread()
{
	if(m_pThread)
	{
		BeginWaitCursor();
		GUARD(g_mutexThreadPTProcessing);
			g_nThreadPTProcessing++;
		END_GUARD;

		try
		{
			DWORD dwExitCode;

			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, 3000);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}

			//delete m_pThread;
		}
		catch(...)
		{
		}

		m_pThread = NULL;
		EndWaitCursor();
	}

	return TRUE;
}

LRESULT CPublicTemplatesProcessingDlg::OnChangeStatus(WPARAM wParam, LPARAM lParam)
{
	int idx = wParam;
	if(m_listCurrent==NULL || idx >= m_listCurrent->GetCount()) return 0;

	CTemplateObject* obj = (CTemplateObject*)m_listCurrent->GetAt(idx);

	switch(lParam)
	{
	default:
	case eUnknownError:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_UNKNOWN_ERROR));
		break;
	case eNowFTPProcessing:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_FTP_PROCESSING));
		break;
	case eCompleted:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_COMPLETED));
		m_lcTemplates.SetItemText(idx, COLUMN_STATUS, _T("100"));
		break;
	case eAlreadyContentsExist:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ALREADY_CONTENTS_EXIST));
		break;
	case eAlreadyContentsVerified:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ALREADY_CONTENTS_VERIFIED));
		break;
	case eErrorOpenLocalFile:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_OPEN_LOCAL_FILE));
		break;
	case eErrorCreateRemoteFile:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CREATE_REMOTE_FILE));
		break;
	case eErrorConnectRemoteServer:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CONNECT_REMOTE_SERVER));
		break;
	case eErrorFTPProcessing:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_FTP_PROCESSING));
		break;
	case eErrorCreateLocalFile:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CREATE_LOCAL_FILE));
		break;
	case eErrorOpenRemoteFile:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_OPEN_REMOTE_FILE));
		break;
	case eErrorNowWorking:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_NOW_WORKING));
		break;
	case eErrorNotExist:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_NOT_EXIST));
		break;
	case eErrorCantDo:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CANT_DO));
		break;
	case eErrorUsingAnotherPackage:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_USING_ANOTHER_PACKAGE));
		break;
	case eErrorNoContentsFile:
		m_lcTemplates.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_NO_CONTENTS_FILE));
		break;
	}
	return 0;
}

LRESULT CPublicTemplatesProcessingDlg::OnChangeProcessing(WPARAM wParam, LPARAM lParam)
{
	if(wParam >= m_lcTemplates.GetItemCount()) return 0;

	int idx = wParam;

	m_lcTemplates.SetItemText(idx, COLUMN_STATUS, ToString(lParam));

	return 0;
}

LRESULT CPublicTemplatesProcessingDlg::OnCompletedAllProcessing(WPARAM wParam, LPARAM lParam)
{
	OnCancel();

	if(m_listCurrent)
	{
		bool all_complete = true;
		for(int i=0; i<m_listCurrent->GetCount(); i++)
		{
			CTemplateObject* obj = m_listCurrent->GetAt(i);
			int result = obj->processingResultValue;

			if(result != eCompleted)
			{
				all_complete = false;
				break;
			}
		}

		if(all_complete)
		{
			UbcMessageBox(CM_NOTIFY_SUCCESS_ALL_PROCESSING, MB_ICONINFORMATION);
			OnOK();
		}
		else
		{
			UbcMessageBox(LoadStringById(IDS_CM_NOTIFY_MSG_ERROR_IN_PROCESS_LIST), MB_ICONWARNING);
		}
	}

	return 0;
}

void CPublicTemplatesProcessingDlg::AddCopyFromPrivateToPublicTemplates(TEMPLATE_LINK& info)
{
	TEMPLATE_LINK* obj = new TEMPLATE_LINK;

	obj->Copy(info);

	CTemplateObject* obj2 = new CTemplateObject;
	*(TEMPLATE_LINK*)obj2 = *obj;

	m_listCopyFromPrivateToPublic.Add(obj2);
}

void CPublicTemplatesProcessingDlg::AddCopyFromPrivateToPublicTemplates(TEMPLATE_LINK_LIST &info_list)
{
	for(int i=0; i<info_list.GetCount(); i++)
	{
		TEMPLATE_LINK& info = info_list.GetAt(i);

		TEMPLATE_LINK* obj = new TEMPLATE_LINK;

		obj->Copy(info);

		CTemplateObject* obj2 = new CTemplateObject;
		*(TEMPLATE_LINK*)obj2 = *obj;

		m_listCopyFromPrivateToPublic.Add(obj2);
	}
}

void CPublicTemplatesProcessingDlg::OnLvnKeydownListCommonContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pLVKeyDow->wVKey == VK_F5)
	{
		for(int i=0; i<m_lcTemplates.GetItemCount(); i++)
		{
			CTemplateObject* obj = (CTemplateObject*)m_lcTemplates.GetItemData(i);

			if(obj->processingResultString.GetLength() > 0)
			{
				UbcMessageBox(obj->processingResultString);
			}
		}
	}
}

UINT CopyFromPrivateToPublicTemplatesThreadFunc(LPVOID pParam)
{
	PT_COMMAND_THREAD_PARAM* param = (PT_COMMAND_THREAD_PARAM*)pParam;

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_SERVER, param->strServerIP, param->nServerPort);

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadPTProcessing; i++)
	{
		CTemplateObject* obj = NULL;

		CStringArray send_msg_list;
		GUARD(g_mutexThreadPTProcessing);
			if(param->nThreadSerial == g_nThreadPTProcessing)
			{
				obj = param->pListObjList->GetAt(i);
				if(obj != NULL)
				{
					CString send_msg;

					// add template
					// http://aaa.bbb.ccc.ddd:eeee/UBC_PublicTemplates/create_template.asp?width=1&height=2&bgColor=3&description=4&registerId=5&customer=6
					send_msg.Format(_T("width=%d&height=%d&bgColor=%s&description=%s&registerId=%s&customer=%s")
						, obj->pTemplateInfo->rcRect.Width()		// width
						, obj->pTemplateInfo->rcRect.Height()		// height
						, ConvertToURLString(::GetColorFromString(obj->pTemplateInfo->crBgColor))	// bgColor
						, ConvertToURLString(obj->pTemplateInfo->strDescription)					// description
						, ConvertToURLString(param->strRegisterId)	// registerId
						, ConvertToURLString(param->strCustomer)	// customer
					);
					send_msg_list.Add(send_msg);

					// add frame
					for(int j=0; j<obj->arFrameLinkList.GetCount(); j++)
					{
						FRAME_LINK& link = obj->arFrameLinkList.GetAt(j);

						// http://localhost/UBC_PublicTemplates/create_frame.asp?templateId={01234567-89AB-CDEF-0123-456789ABCDEF}&grade=2&width=3&height=4&x=5&y=6&isPip=1&borderStyle=7&borderThickness=8&borderColor=9&cornerRadius=10&description=11&comment1=12&comment2=13&comment3=14&alpha=0&isTV=1&customer=6&zindex=16
						send_msg.Format(_T("grade=%d&width=%d&height=%d&x=%d&y=%d&isPip=%d&borderStyle=%s&borderThickness=%d&borderColor=%s&cornerRadius=%d&description=%s&comment1=%s&comment2=%s&comment3=%s&alpha=%d&isTV=%d&customer=%s&zindex=%d")
							, link.pFrameInfo->nGrade								// grade
							, link.pFrameInfo->rcRect.Width()						// width
							, link.pFrameInfo->rcRect.Height()						// height
							, link.pFrameInfo->rcRect.left							// x
							, link.pFrameInfo->rcRect.top							// y
							, link.pFrameInfo->bIsPIP								// isPip
							, ConvertToURLString(link.pFrameInfo->strBorderStyle)	// borderStyle
							, link.pFrameInfo->nBorderThickness						// borderThickness
							, ::GetColorFromString(link.pFrameInfo->crBorderColor)	// borderColor
							, link.pFrameInfo->nCornerRadius						// cornerRadius
							, ConvertToURLString(link.pFrameInfo->strDescription)	// description
							, ConvertToURLString(link.pFrameInfo->strComment[0])	// comment1
							, ConvertToURLString(link.pFrameInfo->strComment[1])	// comment2
							, ConvertToURLString(link.pFrameInfo->strComment[2])	// comment3
							, link.pFrameInfo->nAlpha								// alpha
							, link.pFrameInfo->bIsTV								// isTV
							, ConvertToURLString(param->strCustomer)				// customer
							, link.pFrameInfo->nZIndex								// zindex
						);
						send_msg_list.Add(send_msg);
					}
				}
			}
		END_GUARD;
		if(obj == NULL) continue;

		bool no_error = true;
		CString temp_template_id = "";
		int result_code = eUnknownError;
		for(int j=0; j<send_msg_list.GetCount() && no_error; j++)
		{
			const CString& send_msg = send_msg_list.GetAt(j);

			CString response = _T("");
			result_code = eUnknownError;

			if(j==0)
			{
				http_request.RequestPost("/UBC_PublicTemplates/create_template.asp", send_msg, response);
			}
			else
			{
				CString tmp_send_msg;
				tmp_send_msg.Format("templateId=%s&%s", temp_template_id, send_msg);
				http_request.RequestPost("/UBC_PublicTemplates/create_frame.asp", tmp_send_msg, response);
			}

			if( response.GetLength() == 38 && 
				response.GetAt(0) == '{' && 
				response.GetAt(9) == '-' && 
				response.GetAt(14) == '-' && 
				response.GetAt(19) == '-' && 
				response.GetAt(24) == '-' && 
				response.GetAt(37) == '}' )
			{
				result_code = eCompleted;

				if(j == 0) temp_template_id = response;
			}
			else // 기타 알수없는 에러
			{
				GUARD(g_mutexThreadPTProcessing);
					if(param->nThreadSerial == g_nThreadPTProcessing)
						obj->processingResultString  = response;
				END_GUARD;

				// 에러 발생시 -> 이전 생성 객체 삭제
				no_error = false;
				result_code = eUnknownError;
				if(j != 0)
				{
					CString send_msg;
					send_msg.Format("templateId=%s&customer=%s", temp_template_id, ConvertToURLString(param->strCustomer));
					http_request.RequestPost("/UBC_PublicTemplates/remove_template_frame.asp", send_msg, response);
				}
			}

			GUARD(g_mutexThreadPTProcessing);
				if(param->nThreadSerial == g_nThreadPTProcessing)
				{
					::PostMessage(param->hParentWnd, WM_CHANGE_PROCESSING, i, (j+1)*100 / send_msg_list.GetCount() );
				}
			END_GUARD;
		}
		GUARD(g_mutexThreadPTProcessing);
			if(param->nThreadSerial == g_nThreadPTProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadPTProcessing);
		if(param->nThreadSerial == g_nThreadPTProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}
