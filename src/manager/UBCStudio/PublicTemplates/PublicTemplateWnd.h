#pragma once

#include "common/libHttpRequest/HttpRequest.h"

// CPublicTemplateWnd

class CPublicTemplateWnd : public CWnd
{
	DECLARE_DYNAMIC(CPublicTemplateWnd)

public:
	CPublicTemplateWnd(int nIndex, bool bIsVertical);
	virtual ~CPublicTemplateWnd();

protected:
	DECLARE_MESSAGE_MAP()

	COLORREF	TEMPLATE_SELECT_OUTLINE_COLOR;
	COLORREF	TEMPLATE_SELECT_BG_COLOR;
	COLORREF	TEMPLATE_DESELECT_OUTLINE_COLOR;
	COLORREF	TEMPLATE_DESELECT_BG_COLOR;
	COLORREF	FRAME_SELECT_OUTLINE_COLOR;
	COLORREF	FRAME_SELECT_TEXT_COLOR;
	COLORREF	FRAME_SELECT_BG_COLOR;
	COLORREF	FRAME_DESELECT_OUTLINE_COLOR;
	COLORREF	FRAME_DESELECT_TEXT_COLOR;
	COLORREF	FRAME_DESELECT_BG_COLOR;
	COLORREF	TEMPLATE_INFO_SELECT_TEXT_COLOR;
	COLORREF	TEMPLATE_INFO_DESELECT_TEXT_COLOR;

	int		m_nIndex;
	bool	m_bIsVertical;
	CFont	m_fontNormal;
	CFont	m_fontBold;
	CWnd*	m_pFocusWindow;

	CString	m_strSelectTemplateId;

	TEMPLATE_LINK_LIST		m_listTemplateLink;

	TEMPLATE_LINK_LIST*		GetTemplateList() { return &m_listTemplateLink; };

	int		m_nSelectTemplateLinkIndex;

	CHttpRequest	m_HttpRequest;

public:

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg LRESULT OnTemplateListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam);

	void	Load();

	void	SetFocusWindow(CWnd* pFocusWindow) { m_pFocusWindow = pFocusWindow; };

	void	ResetSelectTemplateLinkIndex() { m_nSelectTemplateLinkIndex = -1; };
	int		GetSelectTemplateLinkIndex() { return m_nSelectTemplateLinkIndex; };
	int 	DeleteSelectTemplateLink();
	TEMPLATE_LINK*	GetSelectTemplateLink();

	void	DrawTemplate(CDC* pDC, TEMPLATE_LINK_LIST* pTemplateList);
	void	DrawFrame(CDC* pDC, TEMPLATE_LINK* pTemplateLink);

	int		GetViewRectPosVert(int count);
	int		GetViewRectPosHorz(int count);

	int		GetTotalHeight(int count);
	int		GetTotalWidth(int count);

	void	RecalcLayout();
	void	ScrollToEnd();

	void	EnsureVisible(int nIndex);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
};
