// PublicTemplateWnd.cpp : 구현 파일입니다.
//
#include "stdafx.h"
#include "UBCStudio.h"
#include "PublicTemplateWnd.h"
#include "MemDC.h"
#include "Enviroment.h"
#include "etc.h"

IMPLEMENT_DYNAMIC(CPublicTemplateWnd, CWnd)

CPublicTemplateWnd::CPublicTemplateWnd(int nIndex, bool bIsVertical)
:	m_nIndex (nIndex)
,	m_pFocusWindow (NULL)
,	m_bIsVertical (bIsVertical)
,	m_nSelectTemplateLinkIndex (-1)
{
	m_fontNormal.CreatePointFont(9*10, "Tahoma");

	m_fontBold.CreateFont(
		14,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_BOLD,                   // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		"Tahoma"                   // lpszFacename
	);

	int r,g,b;
	::GetColorRGB(::GetSysColor(COLOR_HIGHLIGHT), r,g,b);
	r += (255-r)*2/3; if(r>255) r=255;
	g += (255-g)*2/3; if(g>255) g=255;
	b += (255-b)*2/3; if(b>255) b=255;

	TEMPLATE_SELECT_OUTLINE_COLOR	= RGB(0,0,0);
	TEMPLATE_SELECT_BG_COLOR		= ::GetSysColor(COLOR_HIGHLIGHT);//RGB(255,224,224);
	TEMPLATE_DESELECT_OUTLINE_COLOR	= RGB(0,0,0);
	TEMPLATE_DESELECT_BG_COLOR		= RGB(255,255,255);
	FRAME_SELECT_OUTLINE_COLOR		= RGB(0,0,0);
	FRAME_SELECT_TEXT_COLOR			= RGB(255,255,255);
	FRAME_SELECT_BG_COLOR			= RGB(r,g,b);//RGB(255,64,64);
	FRAME_DESELECT_OUTLINE_COLOR	= RGB(0,0,0);
	FRAME_DESELECT_TEXT_COLOR		= RGB(0,0,0);
	FRAME_DESELECT_BG_COLOR			= RGB(224,224,224);
	TEMPLATE_INFO_SELECT_TEXT_COLOR		= RGB(255,255,255);
	TEMPLATE_INFO_DESELECT_TEXT_COLOR	= RGB(128,128,128);

	//
	char szBuf[1000] = {0};
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

	::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", szBuf, sizeof(szBuf), szPath);

	m_HttpRequest.Init(CHttpRequest::CONNECTION_SERVER, szBuf, 8080);
	//m_HttpRequest.Init("127.0.0.1", 80);
}

CPublicTemplateWnd::~CPublicTemplateWnd()
{
	for(int i=0; i<m_listTemplateLink.GetCount(); i++)
	{
		TEMPLATE_LINK& template_link = m_listTemplateLink.GetAt(i);
		template_link.DeleteFrameList();
		if(template_link.pTemplateInfo) delete template_link.pTemplateInfo;
	}
	m_listTemplateLink.RemoveAll();
}

BEGIN_MESSAGE_MAP(CPublicTemplateWnd, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDBLCLK()
	ON_MESSAGE(WM_TEMPLATE_FRAME_SELECT_CHANGED, OnTemplateFrameSelectChanged)
END_MESSAGE_MAP()


// CPublicTemplateWnd 메시지 처리기입니다.


int CPublicTemplateWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	Load();

	RecalcLayout();

	return 0;
}

void CPublicTemplateWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	CRect client_rect;

	if(m_bIsVertical)
	{
		int pos = GetScrollPos(SB_VERT);
		dc.SetWindowOrg(0, pos);

		GetClientRect(client_rect);
		client_rect.OffsetRect(0, pos);
	}
	else
	{
		int pos = GetScrollPos(SB_HORZ);
		dc.SetWindowOrg(pos, 0);

		GetClientRect(client_rect);
		client_rect.OffsetRect(pos, 0);
	}

	CMemDC memDC(&dc);
	memDC.FillSolidRect(client_rect, TEMPLATE_DESELECT_BG_COLOR); // fill background

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();
	DrawTemplate(&memDC, template_list);
}

void CPublicTemplateWnd::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	SetFocus();

	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);

	if(m_bIsVertical == false)
	{
		TEMPLATE_LINK_LIST* template_list = GetTemplateList();

		int height = GetTotalWidth(template_list->GetCount());

		int pos;

		switch(nSBCode)
		{
		case SB_PAGEUP:
			pos = GetScrollPos(SB_HORZ);
			pos -= height;
			SetScrollPos(SB_HORZ, pos);
			break;
		case SB_PAGEDOWN:
			pos = GetScrollPos(SB_HORZ);
			pos += height;
			SetScrollPos(SB_HORZ, pos);
			break;
		case SB_LINEUP:
			pos = GetScrollPos(SB_HORZ);
			pos -= SCROLL_DELTA;
			SetScrollPos(SB_HORZ, pos);
			break;
		case SB_LINEDOWN:
			pos = GetScrollPos(SB_HORZ);
			pos += SCROLL_DELTA;
			SetScrollPos(SB_HORZ, pos);
			break;
		case SB_THUMBTRACK:
			SetScrollPos(SB_HORZ, nPos);
			break;
		default:
			break;
		}

		Invalidate(FALSE);
	}
}

void CPublicTemplateWnd::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	SetFocus();

	CWnd::OnVScroll(nSBCode, nPos, pScrollBar);

	if(m_bIsVertical)
	{
		TEMPLATE_LINK_LIST* template_list = GetTemplateList();

		int height = GetTotalHeight(template_list->GetCount());

		int pos;

		switch(nSBCode)
		{
		case SB_PAGEUP:
			pos = GetScrollPos(SB_VERT);
			pos -= height;
			SetScrollPos(SB_VERT, pos);
			break;
		case SB_PAGEDOWN:
			pos = GetScrollPos(SB_VERT);
			pos += height;
			SetScrollPos(SB_VERT, pos);
			break;
		case SB_LINEUP:
			pos = GetScrollPos(SB_VERT);
			pos -= SCROLL_DELTA;
			SetScrollPos(SB_VERT, pos);
			break;
		case SB_LINEDOWN:
			pos = GetScrollPos(SB_VERT);
			pos += SCROLL_DELTA;
			SetScrollPos(SB_VERT, pos);
			break;
		case SB_THUMBTRACK:
			SetScrollPos(SB_VERT, nPos);
			break;
		default:
			break;
		}

		Invalidate(FALSE);
	}
}

void CPublicTemplateWnd::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();

	if(m_bIsVertical)
	{
		int height = GetTotalHeight(template_list->GetCount());

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_PAGE | SIF_RANGE;
		info.nMin = 0;
		info.nMax = height;
		info.nPage = cy;
		info.nPos = 0;
		info.nTrackPos = 2;

		SetScrollInfo(SB_VERT, &info);
	}
	else
	{
		int width = GetTotalWidth(template_list->GetCount());

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_PAGE | SIF_RANGE;
		info.nMin = 0;
		info.nMax = width;
		info.nPage = cx;
		info.nPos = 0;
		info.nTrackPos = 2;

		SetScrollInfo(SB_HORZ, &info);
	}
}

void CPublicTemplateWnd::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(m_pFocusWindow)
		m_pFocusWindow->SetFocus();

	CWnd::OnLButtonUp(nFlags, point);

	int pos = (m_bIsVertical ? GetScrollPos(SB_VERT) : GetScrollPos(SB_HORZ));

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();

	CRect info_edit_rect;
	bool template_find = false;
	bool info_find = false;
	int template_count = template_list->GetCount();

	//CString strTemplateId = "";
	//if(m_pDocument->GetSelectTemplateLink() != NULL)
	//	strTemplateId = m_pDocument->GetSelectTemplateLink()->pTemplateInfo->strId;
	CString strTemplateId = m_strSelectTemplateId;

	int index = 0;
	for(; index<template_count && template_find == false; index++)
	{
		TEMPLATE_LINK& template_link = template_list->GetAt(index);

		CRect template_rect = template_link.rcViewRectItem;
		CRect info_view_rect = template_link.rcViewRectInfo[1];
		if(m_bIsVertical)
		{
			template_rect.OffsetRect(0, -pos);
			info_view_rect.OffsetRect(0, -pos);
		}
		else
		{
			template_rect.OffsetRect(-pos, 0);
			info_view_rect.OffsetRect(-pos, 0);
		}

		if(template_rect.PtInRect(point))
		{
			if(m_nSelectTemplateLinkIndex != index)
			{
				m_nSelectTemplateLinkIndex = index;
				Invalidate(FALSE);
			}
			if(strTemplateId != template_link.pTemplateInfo->strId)
			{
				template_find = true;
				strTemplateId = template_link.pTemplateInfo->strId;
			}
			info_view_rect.right -= 35;
			if(info_view_rect.PtInRect(point))
			{
				info_edit_rect = info_view_rect;
				info_find = true;
			}
			EnsureVisible(index);
		}
	}
	index--;

	if(template_find == true)
	{
		m_strSelectTemplateId = strTemplateId;
		GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)strTemplateId, (LPARAM)(LPCTSTR)"");
	}
}

void CPublicTemplateWnd::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnRButtonUp(nFlags, point);

	if(m_pFocusWindow)
		m_pFocusWindow->SetFocus();
}

BOOL CPublicTemplateWnd::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();

	if(m_bIsVertical)
	{
		int height = GetTotalHeight(template_list->GetCount());
		int pos = GetScrollPos(SB_VERT);

		if(zDelta > 0)
			pos -= SCROLL_DELTA;
		else
			pos += SCROLL_DELTA;

		SetScrollPos(SB_VERT, pos);
	}
	else
	{
		int width = GetTotalWidth(template_list->GetCount());
		int pos = GetScrollPos(SB_HORZ);

		if(zDelta > 0)
			pos -= SCROLL_DELTA;
		else
			pos += SCROLL_DELTA;

		SetScrollPos(SB_HORZ, pos);
	}

	Invalidate(FALSE);

	return TRUE;
	//return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}

void CPublicTemplateWnd::DrawTemplate(CDC* pDC, TEMPLATE_LINK_LIST* pTemplateList)
{
	TEMPLATE_LINK* select_template = NULL;//m_pDocument->GetSelectTemplateLink();
	CString info_str;

	int count = pTemplateList->GetCount();
	for(int i=0; i<count; i++)
	{
		TEMPLATE_LINK& template_link = pTemplateList->GetAt(i);

		CRect rect = template_link.rcViewRect[m_nIndex];
		rect.InflateRect(1,1);

		//if( select_template != NULL && template_link.pTemplateInfo == select_template->pTemplateInfo )
		if( m_strSelectTemplateId == template_link.pTemplateInfo->strId )
		{
			if(m_nSelectTemplateLinkIndex == i )//|| m_eWndType == TEMPLATE_WND_LAYOUT)
			{
				pDC->FillSolidRect(template_link.rcViewRectItem, TEMPLATE_SELECT_BG_COLOR); // fill select template
				pDC->Draw3dRect(rect, TEMPLATE_SELECT_OUTLINE_COLOR, TEMPLATE_SELECT_OUTLINE_COLOR);
				pDC->SetBkColor(TEMPLATE_SELECT_BG_COLOR);
			}
			else
			{
				pDC->FillSolidRect(template_link.rcViewRectItem, FRAME_SELECT_BG_COLOR); // fill select template
				pDC->Draw3dRect(rect, TEMPLATE_SELECT_OUTLINE_COLOR, TEMPLATE_SELECT_OUTLINE_COLOR);
				pDC->SetBkColor(FRAME_SELECT_BG_COLOR);
			}
			pDC->SetTextColor( TEMPLATE_INFO_SELECT_TEXT_COLOR );
		}
		else
		{
			CRect rect_bg = template_link.rcViewRectItem;
			rect_bg.right = 440;

			pDC->FillSolidRect(rect_bg, (i%2 ? TEMPLATE_DESELECT_BG_COLOR : RGB(247,247,247)) ); // fill deselect template
			pDC->Draw3dRect(rect, TEMPLATE_DESELECT_OUTLINE_COLOR, TEMPLATE_DESELECT_OUTLINE_COLOR);
			pDC->SetBkColor( (i%2 ? TEMPLATE_DESELECT_BG_COLOR : RGB(247,247,247)) );
			pDC->SetTextColor( TEMPLATE_INFO_DESELECT_TEXT_COLOR );
		}
		pDC->SelectObject(&m_fontBold);

		// id 표시안함
		//pDC->DrawText(template_link.pTemplateInfo->strId, template_link.rcViewRectInfo[0], DT_TOP | DT_LEFT | DT_SINGLELINE | DT_VCENTER);

		//if(m_eWndType == TEMPLATE_WND_PACKAGE)
		//{
		//	info_str.Format("%d times", template_link.nPlayTimes);
		//	pDC->DrawText(info_str, template_link.rcViewRectInfo[1], DT_TOP | DT_RIGHT | DT_SINGLELINE| DT_VCENTER);
		//}
		//else
		{
			info_str.Format("%d x %d", template_link.pTemplateInfo->rcRect.Width(), template_link.pTemplateInfo->rcRect.Height());
			pDC->DrawText(info_str, template_link.rcViewRectInfo[1], DT_TOP | DT_RIGHT | DT_SINGLELINE| DT_VCENTER);
		}

		//
		DrawFrame(pDC, &template_link);
	}
}

void CPublicTemplateWnd::DrawFrame(CDC* pDC, TEMPLATE_LINK* pTemplateLink)
{
	pDC->SelectObject(&m_fontBold);

	int count = pTemplateLink->arFrameLinkList.GetCount();
	for(int i=0; i<count; i++)
	{
		FRAME_LINK& frame_link = pTemplateLink->arFrameLinkList.GetAt(i);

		CRect rect = frame_link.rcViewRect[m_nIndex];

		pDC->Draw3dRect(rect, FRAME_DESELECT_OUTLINE_COLOR, FRAME_DESELECT_OUTLINE_COLOR); // outline
		rect.DeflateRect(1,1);

		TEMPLATE_LINK* template_link = NULL;//m_pDocument->GetSelectTemplateLink();

		if( template_link != NULL && pTemplateLink->pTemplateInfo == template_link->pTemplateInfo )
		{
			pDC->FillSolidRect(rect, FRAME_SELECT_BG_COLOR);
			pDC->SetBkColor(FRAME_SELECT_BG_COLOR);
		}
		else
		{
			pDC->FillSolidRect(rect, FRAME_DESELECT_BG_COLOR);
			pDC->SetBkColor(FRAME_DESELECT_BG_COLOR);
		}
		pDC->SetTextColor( FRAME_DESELECT_TEXT_COLOR );
		//pDC->DrawText(frame_link.pFrameInfo->strId, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

		//if(m_eWndType == TEMPLATE_WND_PACKAGE)
		//{
		//	// draw playcontents list
		//	CRect frame_rect = frame_link.rcViewRect[m_nIndex];
		//	frame_rect.DeflateRect(1,1);

		//	CPoint ptPlayContents(frame_rect.left, frame_rect.top);

		//	for(int j=0; j<frame_link.pFrameInfo->arCyclePlayContentsList.GetCount(); j++)
		//	{
		//		PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(j);
		//		CONTENTS_INFO* pContentsInfo = m_pDocument->GetContents(pPlayContentsInfo->strContentsId);
		//		if(pContentsInfo)
		//		{
		//			if(pContentsInfo->bLocalFileExist)
		//			{
		//				m_ilContentsList.Draw(pDC, pContentsInfo->nContentsType+1, ptPlayContents, ILD_TRANSPARENT );
		//			}
		//			else
		//			{
		//				if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty()){
		//					if(pContentsInfo->bServerFileExist)
		//						m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_SERVER, ptPlayContents, ILD_TRANSPARENT );
		//					else
		//						m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT, ptPlayContents, ILD_TRANSPARENT );
		//				}else{
		//					m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT, ptPlayContents, ILD_TRANSPARENT );
		//				}
		//			}

		//			ptPlayContents.x += 16;
		//			if(ptPlayContents.x+16 > frame_rect.right)
		//			{
		//				ptPlayContents.y += 16;
		//				ptPlayContents.x = frame_rect.left;
		//			}//if
		//		}//if
		//		// Modified by 정운형 2008-12-24 오전 10:11
		//		// 변경내역 :  캡션기능 추가 - 버그 수정
		//	}
		//	for(int j=0; j<frame_link.pFrameInfo->arTimePlayContentsList.GetCount(); j++)
		//	{
		//		PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(j);
		//		CONTENTS_INFO* pContentsInfo = m_pDocument->GetContents(pPlayContentsInfo->strContentsId);
		//		if(pContentsInfo)
		//		{
		//			if(pContentsInfo->bLocalFileExist)
		//			{
		//				m_ilContentsList.Draw(pDC, pContentsInfo->nContentsType+1, ptPlayContents, ILD_TRANSPARENT );
		//			}
		//			else
		//			{
		//				if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty()){
		//					if(pContentsInfo->bServerFileExist)
		//						m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_SERVER, ptPlayContents, ILD_TRANSPARENT );
		//					else
		//						m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT, ptPlayContents, ILD_TRANSPARENT );
		//				}else{
		//					m_ilContentsList.Draw(pDC, (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT, ptPlayContents, ILD_TRANSPARENT );
		//				}
		//			}//if

		//			ptPlayContents.x += 16;
		//			if(ptPlayContents.x+16 > frame_rect.right)
		//			{
		//				ptPlayContents.y += 16;
		//				ptPlayContents.x = frame_rect.left;
		//			}//if
		//		}//if
		//		// Modified by 정운형 2008-12-24 오전 10:11
		//		// 변경내역 :  캡션기능 추가 - 버그 수정
		//	}
		//}
	}
}

int CPublicTemplateWnd::GetViewRectPosVert(int count)
{
	return
	(
		TEMPLATE_OUT_TOP_GAP + 
		TEMPLATE_IN_TOP_GAP +
		(
			count *
			(
				TEMPLATE_HEIGHT + 
				TEMPLATE_INFO_HEIGHT + 
				TEMPLATE_IN_BOTTOM_GAP +
				TEMPLATE_IN_TOP_GAP
			)
		)
	);
}

int CPublicTemplateWnd::GetViewRectPosHorz(int count)
{
	return
	(
		TEMPLATE_OUT_LEFT_GAP + 
		TEMPLATE_IN_LEFT_GAP +
		(
			count *
			(
				TEMPLATE_WIDTH + 
				TEMPLATE_IN_RIGHT_GAP +
				TEMPLATE_IN_LEFT_GAP
			)
		)
	);
}

int CPublicTemplateWnd::GetTotalHeight(int count)
{
	return
	(
		TEMPLATE_OUT_TOP_GAP + 
		(
			count *
			(
				TEMPLATE_IN_TOP_GAP + 
				TEMPLATE_HEIGHT + 
				TEMPLATE_INFO_HEIGHT + 
				TEMPLATE_IN_BOTTOM_GAP
			)
		) + 
		TEMPLATE_OUT_BOTTOM_GAP
	);
}

int CPublicTemplateWnd::GetTotalWidth(int count)
{
	return
	(
		TEMPLATE_OUT_LEFT_GAP + 
		(
			count *
			(
				TEMPLATE_IN_LEFT_GAP + 
				TEMPLATE_WIDTH + 
				TEMPLATE_IN_RIGHT_GAP
			)
		) + 
		TEMPLATE_OUT_RIGHT_GAP
	);
}

void CPublicTemplateWnd::RecalcLayout()
{
	CRect client_rect;
	GetClientRect(client_rect);

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();
	int count = template_list->GetCount();

	if(m_bIsVertical)
	{
		for(int i=0; i<count; i++)
		{
			int viewrect_top = GetViewRectPosVert(i);

			//
			TEMPLATE_LINK& template_link = template_list->GetAt(i);

			template_link.RecalcLayout();

			template_link.rcViewRect[m_nIndex]        = template_link.pTemplateInfo->rcRect;
			template_link.rcViewRect[m_nIndex].left	  *= template_link.fScale;
			template_link.rcViewRect[m_nIndex].top	  *= template_link.fScale;
			template_link.rcViewRect[m_nIndex].right  *= template_link.fScale;
			template_link.rcViewRect[m_nIndex].bottom *= template_link.fScale;

			template_link.rcViewRect[m_nIndex].OffsetRect(TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP, viewrect_top);

			//
			template_link.rcViewRectItem.SetRect(
				TEMPLATE_OUT_LEFT_GAP,
				viewrect_top - TEMPLATE_IN_TOP_GAP,
				TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP + TEMPLATE_WIDTH + TEMPLATE_IN_RIGHT_GAP,
				GetViewRectPosVert(i+1) - TEMPLATE_IN_TOP_GAP
			);

			//
			template_link.rcViewRectInfo[0].SetRect(
				TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP,
				viewrect_top + TEMPLATE_HEIGHT, 
				TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP + TEMPLATE_WIDTH / 2,
				viewrect_top + TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT
			);
			//if(m_eWndType == TEMPLATE_WND_PACKAGE)
			{
				template_link.rcViewRectInfo[1].SetRect(
					TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP + TEMPLATE_WIDTH / 2,
					viewrect_top + TEMPLATE_HEIGHT, 
					TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP + TEMPLATE_WIDTH, 
					viewrect_top + TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT
				);
			}
			//else
			//{
			//	template_link.rcViewRectInfo[1].SetRect(-1,-1,-1,-1);
			//}

			//
			int count2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<count2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				frame_link.rcViewRect[m_nIndex] = frame_link.pFrameInfo->rcRect;
				frame_link.rcViewRect[m_nIndex].left		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].top		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].right		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].bottom	*= template_link.fScale;

				frame_link.rcViewRect[m_nIndex].OffsetRect(TEMPLATE_OUT_LEFT_GAP + TEMPLATE_IN_LEFT_GAP, viewrect_top);
			}
		}

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_RANGE | SIF_PAGE;
		info.nMin = 0;
		info.nMax = GetTotalHeight(count);
		info.nPage = client_rect.Height();
		info.nTrackPos = 2;

		SetScrollInfo(SB_VERT, &info);
	}
	else
	{
		for(int i=0; i<count; i++)
		{
			int viewrect_left = GetViewRectPosHorz(i);

			//
			TEMPLATE_LINK& template_link = template_list->GetAt(i);

			template_link.rcViewRect[m_nIndex] = template_link.pTemplateInfo->rcRect;
			template_link.rcViewRect[m_nIndex].left		*= template_link.fScale;
			template_link.rcViewRect[m_nIndex].top		*= template_link.fScale;
			template_link.rcViewRect[m_nIndex].right		*= template_link.fScale;
			template_link.rcViewRect[m_nIndex].bottom		*= template_link.fScale;

			template_link.rcViewRect[m_nIndex].OffsetRect(viewrect_left, TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP);

			//
			template_link.rcViewRectItem.SetRect(
				viewrect_left - TEMPLATE_IN_LEFT_GAP,
				TEMPLATE_OUT_TOP_GAP,
				GetViewRectPosHorz(i+1) - TEMPLATE_IN_LEFT_GAP,
				TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT + TEMPLATE_IN_BOTTOM_GAP
			);

			//
			template_link.rcViewRectInfo[0].SetRect(
				viewrect_left, 
				TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_HEIGHT,
				viewrect_left + TEMPLATE_WIDTH / 2,
				TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT
			);
			//if(m_eWndType == TEMPLATE_WND_PACKAGE)
			{
				template_link.rcViewRectInfo[1].SetRect(
					viewrect_left + TEMPLATE_WIDTH / 2, 
					TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_HEIGHT,
					viewrect_left + TEMPLATE_WIDTH,
					TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP + TEMPLATE_HEIGHT + TEMPLATE_INFO_HEIGHT
				);
			}
			//else
			//{
			//	template_link.rcViewRectInfo[1].SetRect(-1,-1,-1,-1);
			//}

			//
			int count2 = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<count2; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				frame_link.rcViewRect[m_nIndex] = frame_link.pFrameInfo->rcRect;
				frame_link.rcViewRect[m_nIndex].left		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].top		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].right		*= template_link.fScale;
				frame_link.rcViewRect[m_nIndex].bottom	*= template_link.fScale;

				frame_link.rcViewRect[m_nIndex].OffsetRect(viewrect_left, TEMPLATE_OUT_TOP_GAP + TEMPLATE_IN_TOP_GAP);
			}
		}

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_RANGE | SIF_PAGE;
		info.nMin = 0;
		info.nMax = GetTotalWidth(count);
		info.nPage = client_rect.Width();
		info.nTrackPos = 2;

		SetScrollInfo(SB_HORZ, &info);
	}
}

void CPublicTemplateWnd::ScrollToEnd()
{
	CRect rect;
	GetClientRect(rect);

	TEMPLATE_LINK_LIST* template_list = GetTemplateList();
	int count = template_list->GetCount();

	if(m_bIsVertical)
	{
		int height = GetTotalHeight(count);// - TEMPLATE_IN_TOP_GAP;

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_ALL;
		info.nMin = 0;
		info.nMax = height;
		info.nPage = rect.Height();
		info.nPos = height - rect.Height();
		info.nTrackPos = 2;

		SetScrollInfo(SB_VERT, &info);
	}
	else
	{
		int width = GetTotalWidth(count);

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_ALL;
		info.nMin = 0;
		info.nMax = width;
		info.nPage = rect.Width();
		info.nPos = width - rect.Width();
		info.nTrackPos = 2;

		SetScrollInfo(SB_HORZ, &info);
	}
}

void CPublicTemplateWnd::EnsureVisible(int nIndex)
{
	int scroll_pos;
	int item_pos;

	CRect rect;
	GetClientRect(rect);

	if(m_bIsVertical)
	{
		scroll_pos = GetScrollPos(SB_VERT);
		item_pos = GetViewRectPosVert(nIndex) - TEMPLATE_IN_TOP_GAP;

		if(item_pos < scroll_pos)
		{
			SCROLLINFO info;
			info.cbSize = sizeof(info);
			info.fMask = SIF_POS;
			info.nPos = item_pos;
			SetScrollInfo(SB_VERT, &info);
			Invalidate(FALSE);
		}

		scroll_pos = GetScrollPos(SB_VERT) + rect.Height();
		item_pos = GetViewRectPosVert(nIndex + 1) - TEMPLATE_IN_TOP_GAP;

		if(item_pos > scroll_pos)
		{
			SCROLLINFO info;
			info.cbSize = sizeof(info);
			info.fMask = SIF_POS;
			info.nPos = item_pos - rect.Height();
			SetScrollInfo(SB_VERT, &info);
			Invalidate(FALSE);
		}
	}
	else
	{
		scroll_pos = GetScrollPos(SB_HORZ);
		item_pos = GetViewRectPosHorz(nIndex) - TEMPLATE_IN_LEFT_GAP;

		if(item_pos < scroll_pos)
		{
			SCROLLINFO info;
			info.cbSize = sizeof(info);
			info.fMask = SIF_POS;
			info.nPos = item_pos;
			SetScrollInfo(SB_HORZ, &info);
			Invalidate(FALSE);
		}

		scroll_pos = GetScrollPos(SB_HORZ) + rect.Width();
		item_pos = GetViewRectPosHorz(nIndex + 1) - TEMPLATE_IN_LEFT_GAP;

		if(item_pos > scroll_pos)
		{
			SCROLLINFO info;
			info.cbSize = sizeof(info);
			info.fMask = SIF_POS;
			info.nPos = item_pos - rect.Width();
			SetScrollInfo(SB_HORZ, &info);
			Invalidate(FALSE);
		}
	}
}


void CPublicTemplateWnd::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	GetParent()->SendMessage(WM_TEMPLATE_SELECTED, 0, 0);

	CWnd::OnLButtonDblClk(nFlags, point);
}

BOOL GetTemplatesObjectList(TEMPLATE_LINK_LIST& infoList, CStringArray& str_list)
{
	TEMPLATE_INFO* info = NULL;

	for(int i=0; i<str_list.GetCount(); i++)
	{
		const CString& line = str_list.GetAt(i);

		int equal_idx = line.Find(_T("="));

		if(equal_idx > 0 && line.GetLength() >= equal_idx+1 )
		{
			int pos = 0;
			CString key = line.Tokenize(_T("="), pos);
			LPCTSTR szValue = (LPCTSTR)line+equal_idx+1;

			if(key == "templateId")
			{
				info = new TEMPLATE_INFO;
				info->strId = szValue;

				TEMPLATE_LINK link(info);

				infoList.Add(link);
			}
			else if(key == "width"				&& info)	info->rcRect.right = atoi(szValue);
			else if(key == "height"				&& info)	info->rcRect.bottom = atoi(szValue);
			else if(key == "bgColor"			&& info)	info->crBgColor = GetColorFromString(szValue);
			else if(key == "description"		&& info)	info->strDescription = szValue;
			else if(key == "registerTime"		&& info)	info->strRegisterTime = szValue;
			else if(key == "registerId"			&& info)	info->strRegisterId = szValue;
		}
	}

	return TRUE;
}

BOOL GetFramesObjectList(TEMPLATE_LINK_LIST& infoList, CStringArray& str_list)
{
	FRAME_INFO* info = NULL;

	for(int i=0; i<str_list.GetCount(); i++)
	{
		const CString& line = str_list.GetAt(i);

		int equal_idx = line.Find(_T("="));

		if(equal_idx > 0 && line.GetLength() >= equal_idx+1 )
		{
			int pos = 0;
			CString key = line.Tokenize(_T("="), pos);
			LPCTSTR szValue = (LPCTSTR)line+equal_idx+1;

			if(key == "templateId")
			{
				info = new FRAME_INFO;
				info->strTemplateId = szValue;

				bool find = false;
				for(int j=0; j<infoList.GetCount(); j++)
				{
					TEMPLATE_LINK& templ_link = infoList.GetAt(j);
					if(templ_link.pTemplateInfo && templ_link.pTemplateInfo->strId == info->strTemplateId)
					{
						FRAME_LINK link(info);
						templ_link.arFrameLinkList.Add(link);
						find = true;
						break;
					}
				}
				if( !find )
				{
					delete info;
					info = NULL;
				}
			}
			else if(key == "frameId"			&& info)	info->strId = szValue;
			else if(key == "grade"				&& info)	info->nGrade = atoi(szValue);
			else if(key == "x"					&& info)	info->rcRect.left = atoi(szValue);
			else if(key == "y"					&& info)	info->rcRect.top = atoi(szValue);
			else if(key == "width"				&& info)	info->rcRect.right = info->rcRect.left + atoi(szValue);
			else if(key == "height"				&& info)	info->rcRect.bottom = info->rcRect.top + atoi(szValue);
			else if(key == "isPIP"				&& info)	info->bIsPIP = atoi(szValue);
			else if(key == "borderStyle"		&& info)	info->strBorderStyle = szValue;
			else if(key == "borderThickness"	&& info)	info->nBorderThickness = atoi(szValue);
			else if(key == "borderColor"		&& info)	info->crBorderColor = GetColorFromString(szValue);
			else if(key == "cornerRadius"		&& info)	info->nCornerRadius = atoi(szValue);
			else if(key == "description"		&& info)	info->strDescription = szValue;
			else if(key == "comment1"			&& info)	info->strComment[0] = szValue;
			else if(key == "comment2"			&& info)	info->strComment[1] = szValue;
			else if(key == "comment3"			&& info)	info->strComment[2] = szValue;
			else if(key == "alpha"				&& info)	info->nAlpha = atoi(szValue);
			else if(key == "isTV"				&& info)	info->bIsTV = atoi(szValue);
			else if(key == "zindex"				&& info)	info->nZIndex = atoi(szValue);
		}
	}

	return TRUE;
}

void CPublicTemplateWnd::Load()
{
	CString send_msg;
	send_msg.Format(_T("customer=%s"), ConvertToURLString(GetEnvPtr()->m_strCustomer) );		// customer

	CString out_msg = "";
	if( m_HttpRequest.RequestPost("UBC_PublicTemplates/get_template_list.asp", send_msg, out_msg) )
	{
		CStringArray token_list;
		int pos = 0;
		CString token = out_msg.Tokenize(_T("\r\n"), pos);

		while(token != _T(""))
		{
			token_list.Add(token);
			token = out_msg.Tokenize(_T("\r\n"), pos);
		}

		if(token_list.GetCount() > 0)
		{
			GetTemplatesObjectList(m_listTemplateLink, token_list);
		}
	}

	out_msg = "";
	if( m_HttpRequest.RequestPost("UBC_PublicTemplates/get_frame_list.asp", send_msg, out_msg) )
	{
		CStringArray token_list;
		int pos = 0;
		CString token = out_msg.Tokenize(_T("\r\n"), pos);

		while(token != _T(""))
		{
			token_list.Add(token);
			token = out_msg.Tokenize(_T("\r\n"), pos);
		}

		if(token_list.GetCount() > 0)
		{
			GetFramesObjectList(m_listTemplateLink, token_list);
		}
	}
}

LRESULT CPublicTemplateWnd::OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam)
{
	RecalcLayout();
	Invalidate(FALSE);
	return 0;
}

int CPublicTemplateWnd::DeleteSelectTemplateLink()
{
	if(m_listTemplateLink.GetCount() > 0 && m_nSelectTemplateLinkIndex >= 0 && m_listTemplateLink.GetCount() > m_nSelectTemplateLinkIndex)
	{
		TEMPLATE_LINK& link = m_listTemplateLink.GetAt(m_nSelectTemplateLinkIndex);
		if( link.pTemplateInfo )
		{
			CString send_msg;
			send_msg.Format("templateId=%s&customer=%s", link.pTemplateInfo->strId, ConvertToURLString(GetEnvPtr()->m_strCustomer));

			CString response;
			m_HttpRequest.RequestPost("/UBC_PublicTemplates/remove_template_frame.asp", send_msg, response);

			if(response.CompareNoCase("true") == 0)
			{
				return 1;
			}
			else if(response.CompareNoCase("false") == 0)
			{
				return 0;
			}
			return -1;
		}
	}
	return -2;
}

TEMPLATE_LINK* CPublicTemplateWnd::GetSelectTemplateLink()
{
	TEMPLATE_LINK_LIST* template_list = GetTemplateList();
	int idx = GetSelectTemplateLinkIndex();
	if( idx >= 0 && 
		template_list->GetCount() > 0 && 
		template_list->GetCount() > idx )
	{
		TEMPLATE_LINK& link = template_list->GetAt(idx);
		return &link;
	}

	return NULL;
}
