// SelectPublicTemplateDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "SelectPublicTemplateDialog.h"

#include "UBCStudioDoc.h"

// CSelectPublicTemplateDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSelectPublicTemplateDialog, CDialog)

CSelectPublicTemplateDialog::CSelectPublicTemplateDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectPublicTemplateDialog::IDD, pParent)
	, m_reposControl (this)
	, m_wndTemplate (4, true)
{
}

CSelectPublicTemplateDialog::~CSelectPublicTemplateDialog()
{
}

void CSelectPublicTemplateDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TEMPLATE, m_frameTemplate);
	DDX_Control(pDX, IDC_BN_DEL, m_btnDelete);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CSelectPublicTemplateDialog, CDialog)
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_WM_GETMINMAXINFO()
	ON_MESSAGE(WM_TEMPLATE_FRAME_SELECT_CHANGED, OnTemplateFrameSelectChanged)
	ON_MESSAGE(WM_TEMPLATE_SELECTED, OnTemplateSelected)
	ON_BN_CLICKED(IDC_BN_DEL, OnBnClickedDelete)
END_MESSAGE_MAP()


// CSelectPublicTemplateDialog 메시지 처리기입니다.

BOOL CSelectPublicTemplateDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	HICON hIcon = LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_LAYOUT));
	SetIcon(hIcon, TRUE);
	SetIcon(hIcon, FALSE);

	m_btnDelete.LoadBitmap(IDB_BTN_DELETE, RGB(255, 255, 255));
	m_btnOK.LoadBitmap(IDB_BTN_SELECT, RGB(255, 255, 255));
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));

	m_btnOK.SetToolTipText(LoadStringById(IDS_SELECTTEMPLATEDIALOG_BUT001));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_SELECTTEMPLATEDIALOG_BUT002));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	CRect rect;
	m_frameTemplate.GetWindowRect(rect);
	ScreenToClient(rect);

	BeginWaitCursor();
	m_wndTemplate.CreateEx(WS_EX_CLIENTEDGE, NULL, "Template", WS_CHILD | WS_VISIBLE | WS_VSCROLL, rect, this, 0xfeff);
	EndWaitCursor();

	m_reposControl.AddControl(&m_wndTemplate, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnDelete, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnOK, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnCancel, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSelectPublicTemplateDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnOK();
}

void CSelectPublicTemplateDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();
	Invalidate();
}

BOOL CSelectPublicTemplateDialog::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CRect rect;
	m_wndTemplate.GetWindowRect(rect);

	if(rect.PtInRect(pt))
	{
		m_wndTemplate.OnMouseWheel(nFlags, zDelta, pt);
		return TRUE;
	}

	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

LRESULT CSelectPublicTemplateDialog::OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam)
{
//	m_pDocument->SetSelectTemplateId((LPCTSTR)wParam);
	m_wndTemplate.SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, wParam, lParam);

	return 0;
}

void CSelectPublicTemplateDialog::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMinTrackSize.x = 261 + ::GetSystemMetrics(SM_CXSIZEFRAME)*2 + ::GetSystemMetrics(SM_CXBORDER)*2;
	lpMMI->ptMaxTrackSize.x = 261 + ::GetSystemMetrics(SM_CXSIZEFRAME)*2 + ::GetSystemMetrics(SM_CXBORDER)*2;
}

LRESULT CSelectPublicTemplateDialog::OnTemplateSelected(WPARAM wParam, LPARAM lParam)
{
	OnOK();

	return 0;
}

void CSelectPublicTemplateDialog::OnBnClickedDelete()
{
	BeginWaitCursor();
	int ret_val = m_wndTemplate.DeleteSelectTemplateLink();
	EndWaitCursor();

	switch(ret_val)
	{
	case 1: // success
		UbcMessageBox(::LoadStringById(IDS_CM_SUCCEED_TO_DELETE_PT), MB_ICONINFORMATION); // "삭제에 성공하였습니다."
		break;
	case 0: // fail
		UbcMessageBox(::LoadStringById(IDS_CM_FAIL_TO_DELETE_PT), MB_ICONSTOP); // "삭제에 실패하였습니다.\r\n서버에 존재하지 않는 공용템플릿 입니다."
		break;
	case -1: // server communication fail
		UbcMessageBox(::LoadStringById(IDS_CM_FAIL_TO_CONNECT_SERVER), MB_ICONSTOP); // "서버와 연결이 실패하였습니다."
		break;
	case -2: // no select
		UbcMessageBox(::LoadStringById(IDS_CM_SELECT_TEMPLATE), MB_ICONWARNING); // "템플릿을 선택하여 주세요."
		break;
	}
}

TEMPLATE_LINK* CSelectPublicTemplateDialog::GetSelectTemplateLink()
{
	return m_wndTemplate.GetSelectTemplateLink();
}
