// RegistDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "RegistDlg.h"


// CRegistDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CRegistDlg, CDialog)

CRegistDlg::CRegistDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRegistDlg::IDD, pParent)
	, m_strRegistKey(_T(""))
{

}

CRegistDlg::~CRegistDlg()
{
}

void CRegistDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Text(pDX, IDC_REGIST_KEY_EDT, m_strRegistKey);
}


BEGIN_MESSAGE_MAP(CRegistDlg, CDialog)
	ON_BN_CLICKED(IDCANCEL, &CRegistDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CRegistDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CRegistDlg 메시지 처리기입니다.

void CRegistDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CRegistDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if(m_strRegistKey != KEY_MAGIC_NUMBER)
	{
		UbcMessageBox(LoadStringById(IDS_REGISTDLG_MSG001), MB_ICONERROR);
		return;
	}//if

	OnOK();
}

BOOL CRegistDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btnOK.LoadBitmap(IDB_BTN_OK, RGB(255, 255, 255));
	m_btnOK.SetToolTipText(LoadStringById(IDS_REGISTDLG_BTN001));

	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_REGISTDLG_BTN002));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
