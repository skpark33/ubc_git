#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "afxdtctl.h"

#include "ReposControl.h"
#include "common/CheckComboBox.h"
#include "CheckListCtrl.h"

// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업
#include "common/HoverButton.h"
// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업


// CPlayContentsSearchView 폼 뷰입니다.

class CPlayContentsSearchView : public CFormView
{
	DECLARE_DYNCREATE(CPlayContentsSearchView)

protected:
	CPlayContentsSearchView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CPlayContentsSearchView();

public:
	enum { IDD = IDD_PLAYCONTENTS_SEARCH_VIEW };

	CUBCStudioDoc* GetDocument() const;

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	virtual void OnInitialUpdate();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CReposControl	m_reposControl;

public:
	afx_msg void OnCbnSelchangeComboPlayContentsType();
	afx_msg void OnBnClickedButtonSearch();

	CComboBox		m_cbxPlayContentsType;
	CDateTimeCtrl	m_dateStart;
	CDateTimeCtrl	m_timeStart;
	CDateTimeCtrl	m_dateEnd;
	CDateTimeCtrl	m_timeEnd;
	CCheckComboBox	m_cbxTimeZone;
	CCheckComboBox	m_cbxContentsType;
	CEdit			m_editContentsName;
	CEdit			m_editFilename;
	CCheckListCtrl	m_lcPlayContents;

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	//CButton			m_btnSearch;
	CHoverButton			m_btnSearch;
	CHoverButton			m_btnTimeZone;
	CHoverButton			m_btnModifyPlayContents;
	CHoverButton			m_btnDeletePlayContents;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
};

#ifndef _DEBUG  // debug version in PackageView.cpp
inline CUBCStudioDoc* CPlayContentsSearchView::GetDocument() const
   { return reinterpret_cast<CUBCStudioDoc*>(m_pDocument); }
#endif


