#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "EditEx.h"
#include "ColorPickerCB.h"
#include "ShortcutEdit.h"

// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업
#include "common/HoverButton.h"
// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업

// CTemplatePropertyDialog 대화 상자입니다.

class CTemplatePropertyDialog : public CDialog
{
	DECLARE_DYNAMIC(CTemplatePropertyDialog)

public:
	CTemplatePropertyDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTemplatePropertyDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TEMPLATE_PROPERTY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CArray<CWnd*, CWnd*>	m_listNoCTLWnd;

	CBrush	m_brushBG;

	bool	m_bNowSetting;

public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnChangeEdit();
	afx_msg void OnBnClickedButtonBgImage();

	CEdit			m_editTemplateID;
	CEditEx			m_editWidth;
	CEditEx			m_editHeight;
	CSpinButtonCtrl	m_spinWidth;
	CSpinButtonCtrl	m_spinHeight;
	CColorPickerCB	m_cbxBGColor;
	CEdit			m_editDescription;
	CShortcutEdit	m_edtShortcut;

	CEdit			m_edBGImage;
	CStatic			m_stBGImage;
	CComboBox		m_cbBGType;
	CStatic			m_stBGType;

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	CHoverButton		m_btnBGImg;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	void	SetTemplateInfo(TEMPLATE_LINK* pTemplateLink);
	bool	GetTemplateInfo(TEMPLATE_LINK* pTemplateLink);
};
