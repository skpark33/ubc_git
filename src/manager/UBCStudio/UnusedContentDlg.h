#pragma once
#include "resource.h"
#include "common\utblistctrl.h"

// CUnusedContentDlg dialog
class CUnusedContentDlg : public CDialog
{
	DECLARE_DYNAMIC(CUnusedContentDlg)
public:
	CUnusedContentDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CUnusedContentDlg();

// Dialog Data
	enum { IDD = IDD_UNUSEDCONTENT };
	enum { eCheck, eType, eName, eTime, eFile, eColEnd };

private:
	void InitList();
	void RefreshContents();

	CImageList m_ilHostList;
	CUTBListCtrl m_lscContents;
	CString m_szColTitle[eColEnd];

	static int CALLBACK Compare(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonDelete();
};
