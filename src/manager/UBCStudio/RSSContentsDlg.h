#pragma once
#include "resource.h"
#include "SubContentsDialog.h"
#include "common/FontPreviewCombo.h"
#include "ColorPickerCB.h"
#include "EditEx.h"
#include "Schedule.h"

#include "common/HoverButton.h"

// CRSSContentsDlg dialog
class CRSSContentsDlg : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CRSSContentsDlg)

public:
	CRSSContentsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRSSContentsDlg();

// Dialog Data
	enum { IDD = IDD_RSS_CONTENTS };

	virtual CONTENTS_TYPE	GetContentsType() { return CONTENTS_RSS; }
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);
	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
	void	EnableAllControls(BOOL bEnable);			///<컨트롤의 사용가능여부 설정

	void	SetPlayContents();

	bool	m_bPreviewMode;
	BOOL	m_bPermanent;
	CEditEx	m_editContentsName;
	CEditEx	m_editContentsFileName;
	CEditEx	m_editContentsPlayMinute;
	CEditEx	m_editContentsPlaySecond;
	CEditEx m_editURL;
	CStatic	m_staticContents;
	CString m_strLocation;
	CHoverButton m_btnBrowserContentsFile;
	CHoverButton m_btnDelFile;
	CHoverButton m_btnPreviewPlay;
	CFontPreviewCombo m_cbxContentsFont;
	CEditEx m_editContentsFontSize;
	CColorPickerCB m_cbxContentsTextColor;
	CColorPickerCB m_cbxContentsBGColor;
	CEditEx m_edtHeight;
	CSpinButtonCtrl m_spnHeight;
	CEditEx m_edtWidth;
	CSpinButtonCtrl  m_spnWidth;
	CEditEx m_edtLineNum;
	CSpinButtonCtrl m_spnLineNum;
	CRSSSchedule	m_wndRss;

	int m_nRssType;
	CString m_strTopicFilter;
	CString m_strErrText;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnBnClickedFileName();
	afx_msg void OnBnClickedButtonPreviewPlay();
	afx_msg void OnBnClickedPermanentCheck();
	afx_msg void OnBnClickedDeletefile();
	afx_msg void OnBnClickedRbRssType1();
	afx_msg void OnBnClickedRbRssType2();
	afx_msg void OnBnClickedRbRssType3();
};
