#include "StdAfx.h"
#include "ColorPalatte.h"
#include "math.h"

CColorPalatte*	CColorPalatte::_instance;
CCriticalSection CColorPalatte::_cs;

CColorPalatte* CColorPalatte::getInstance()
{
	_cs.Lock();
	if(_instance == NULL)
	{
		_instance = new CColorPalatte();
	}
	_cs.Unlock();

	return _instance;
}

void CColorPalatte::clearInstance()
{
	_cs.Lock();
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
	_cs.Unlock();
}

CColorPalatte::CColorPalatte(void)
{
	InitPalatte();
}

CColorPalatte::~CColorPalatte(void)
{
}

void CColorPalatte::InitPalatte()
{
	int nIndex = 0;
//	m_ColorPalatte[nIndex++] = (DWORD)0xfffafa; // snow
//	m_ColorPalatte[nIndex++] = (DWORD)0xfff5ee; // seashell
//	m_ColorPalatte[nIndex++] = (DWORD)0xf8f8ff; // ghostwhite
//	m_ColorPalatte[nIndex++] = (DWORD)0xfffaf0; // floralwhite
//	m_ColorPalatte[nIndex++] = (DWORD)0xf5f5f5; // whitesmoke
	m_ColorPalatte[nIndex++] = (DWORD)0xf0f8ff; // aliceblue
//	m_ColorPalatte[nIndex++] = (DWORD)0xfdf5e6; // oldlace
	m_ColorPalatte[nIndex++] = (DWORD)0xf5fffa; // mintcream
	m_ColorPalatte[nIndex++] = (DWORD)0xffefd5; // papayawhip
	m_ColorPalatte[nIndex++] = (DWORD)0xffdab9; // peachpuff
	m_ColorPalatte[nIndex++] = (DWORD)0xfaf0e6; // linen
	m_ColorPalatte[nIndex++] = (DWORD)0xeee8aa; // palegoldenrod
	m_ColorPalatte[nIndex++] = (DWORD)0xffe4e1; // mistyrose
	m_ColorPalatte[nIndex++] = (DWORD)0xffe4b5; // moccasin
//	m_ColorPalatte[nIndex++] = (DWORD)0xffdead; // navajowhite
	m_ColorPalatte[nIndex++] = (DWORD)0xd2b48c; // tan
	m_ColorPalatte[nIndex++] = (DWORD)0xf5deb3; // wheat
	m_ColorPalatte[nIndex++] = (DWORD)0xfafad2; // lightgoldenrodyellow
//	m_ColorPalatte[nIndex++] = (DWORD)0xffffe0; // lightyellow
	m_ColorPalatte[nIndex++] = (DWORD)0xfff8dc; // cornsilk
	m_ColorPalatte[nIndex++] = (DWORD)0xfaebd7; // antiquewhite
	m_ColorPalatte[nIndex++] = (DWORD)0xf5f5dc; // beige
	m_ColorPalatte[nIndex++] = (DWORD)0xfffacd; // lemonchiffon
	m_ColorPalatte[nIndex++] = (DWORD)0xfffff0; // ivory
	m_ColorPalatte[nIndex++] = (DWORD)0xf0e68c; // khaki
	m_ColorPalatte[nIndex++] = (DWORD)0xe6e6fa; // lavender
	m_ColorPalatte[nIndex++] = (DWORD)0xfff0f5; // lavenderblush
	m_ColorPalatte[nIndex++] = (DWORD)0xffe4c4; // bisque
	m_ColorPalatte[nIndex++] = (DWORD)0xffebcd; // blanchedalmond
	m_ColorPalatte[nIndex++] = (DWORD)0xdeb887; // burlywood
	m_ColorPalatte[nIndex++] = (DWORD)0xcd853f; // peru
	m_ColorPalatte[nIndex++] = (DWORD)0x00ced1; // darkturquoise
	m_ColorPalatte[nIndex++] = (DWORD)0x00bfff; // deepskyblue
	m_ColorPalatte[nIndex++] = (DWORD)0x7fffd4; // aquamarine
	m_ColorPalatte[nIndex++] = (DWORD)0x1e90ff; // dodgerblue
	m_ColorPalatte[nIndex++] = (DWORD)0xf0ffff; // azure
	m_ColorPalatte[nIndex++] = (DWORD)0x00ffff; // cyan
	m_ColorPalatte[nIndex++] = (DWORD)0xf0fff0; // honeydew
	m_ColorPalatte[nIndex++] = (DWORD)0x87cefa; // lightskyblue
//	m_ColorPalatte[nIndex++] = (DWORD)0xafeeee; // paleturquoise
	m_ColorPalatte[nIndex++] = (DWORD)0xe0ffff; // lightcyan
	m_ColorPalatte[nIndex++] = (DWORD)0xadd8e6; // lightblue
	m_ColorPalatte[nIndex++] = (DWORD)0xb0c4de; // lightsteelblue
	m_ColorPalatte[nIndex++] = (DWORD)0x40e0d0; // turquoise
	m_ColorPalatte[nIndex++] = (DWORD)0x48d1cc; // mediumturquoise
	m_ColorPalatte[nIndex++] = (DWORD)0x00ffff; // aqua
	m_ColorPalatte[nIndex++] = (DWORD)0x7b68ee; // mediumslateblue
//	m_ColorPalatte[nIndex++] = (DWORD)0x191970; // midnightblue
	m_ColorPalatte[nIndex++] = (DWORD)0x6495ed; // cornflowerblue
//	m_ColorPalatte[nIndex++] = (DWORD)0x0000cd; // mediumblue
	m_ColorPalatte[nIndex++] = (DWORD)0x6a5acd; // slateblue
	m_ColorPalatte[nIndex++] = (DWORD)0x4682b4; // steelblue
//	m_ColorPalatte[nIndex++] = (DWORD)0x0000ff; // blue
	m_ColorPalatte[nIndex++] = (DWORD)0x483d8b; // darkslateblue
	m_ColorPalatte[nIndex++] = (DWORD)0x5f9ea0; // cadetblue
	m_ColorPalatte[nIndex++] = (DWORD)0x87ceeb; // skyblue
//	m_ColorPalatte[nIndex++] = (DWORD)0x4169e1; // royalblue
	m_ColorPalatte[nIndex++] = (DWORD)0xb0e0e6; // powderblue
//	m_ColorPalatte[nIndex++] = (DWORD)0x000080; // navy
//	m_ColorPalatte[nIndex++] = (DWORD)0x00008b; // darkblue
	m_ColorPalatte[nIndex++] = (DWORD)0x8a2be2; // blueviolet
	m_ColorPalatte[nIndex++] = (DWORD)0x8b008b; // darkmagenta
	m_ColorPalatte[nIndex++] = (DWORD)0x9932cc; // darkorchid
	m_ColorPalatte[nIndex++] = (DWORD)0x9400d3; // darkviolet
	m_ColorPalatte[nIndex++] = (DWORD)0xff00ff; // magenta
//	m_ColorPalatte[nIndex++] = (DWORD)0xff00ff; // fuchsia
	m_ColorPalatte[nIndex++] = (DWORD)0xc71585; // mediumvioletred
	m_ColorPalatte[nIndex++] = (DWORD)0xba55d3; // mediumorchid
	m_ColorPalatte[nIndex++] = (DWORD)0x9370db; // mediumpurple
	m_ColorPalatte[nIndex++] = (DWORD)0xdc143c; // crimson
	m_ColorPalatte[nIndex++] = (DWORD)0xff1493; // deeppink
	m_ColorPalatte[nIndex++] = (DWORD)0xffb6c1; // lightpink
	m_ColorPalatte[nIndex++] = (DWORD)0xff69b4; // hotpink
//	m_ColorPalatte[nIndex++] = (DWORD)0xffc0cb; // pink
	m_ColorPalatte[nIndex++] = (DWORD)0xdda0dd; // plum
	m_ColorPalatte[nIndex++] = (DWORD)0x800080; // purple
	m_ColorPalatte[nIndex++] = (DWORD)0xee82ee; // violet
	m_ColorPalatte[nIndex++] = (DWORD)0xd8bfd8; // thistle
	m_ColorPalatte[nIndex++] = (DWORD)0xda70d6; // orchid
	m_ColorPalatte[nIndex++] = (DWORD)0x4b0082; // indigo
	m_ColorPalatte[nIndex++] = (DWORD)0xa52a2a; // brown
	m_ColorPalatte[nIndex++] = (DWORD)0xe9967a; // darksalmon
	m_ColorPalatte[nIndex++] = (DWORD)0xf08080; // lightcoral
	m_ColorPalatte[nIndex++] = (DWORD)0xcd5c5c; // indianred
	m_ColorPalatte[nIndex++] = (DWORD)0xffa07a; // lightsalmon
	m_ColorPalatte[nIndex++] = (DWORD)0xdb7093; // palevioletred
	m_ColorPalatte[nIndex++] = (DWORD)0xf4a460; // sandybrown
	m_ColorPalatte[nIndex++] = (DWORD)0xfa8072; // salmon
	m_ColorPalatte[nIndex++] = (DWORD)0xff6347; // tomato
	m_ColorPalatte[nIndex++] = (DWORD)0xff4500; // ornagered
	m_ColorPalatte[nIndex++] = (DWORD)0xff0000; // red
	m_ColorPalatte[nIndex++] = (DWORD)0x800000; // maroon
	m_ColorPalatte[nIndex++] = (DWORD)0x8b0000; // darkred
	m_ColorPalatte[nIndex++] = (DWORD)0xb22222; // firebrick
	m_ColorPalatte[nIndex++] = (DWORD)0xd2691e; // chocolate
	m_ColorPalatte[nIndex++] = (DWORD)0x8b4513; // saddlebrown
	m_ColorPalatte[nIndex++] = (DWORD)0xa0522d; // sienna
	m_ColorPalatte[nIndex++] = (DWORD)0xbc8f8f; // rosybrown
	m_ColorPalatte[nIndex++] = (DWORD)0xff7f50; // coral
	m_ColorPalatte[nIndex++] = (DWORD)0xff8c00; // darkorange
	m_ColorPalatte[nIndex++] = (DWORD)0xffa500; // orange
	m_ColorPalatte[nIndex++] = (DWORD)0xb8860b; // darkgoldenrod
	m_ColorPalatte[nIndex++] = (DWORD)0xffd700; // gold
	m_ColorPalatte[nIndex++] = (DWORD)0xffff00; // yellow
	m_ColorPalatte[nIndex++] = (DWORD)0x7fff00; // chartreuse
	m_ColorPalatte[nIndex++] = (DWORD)0x7cfc00; // lawngreen
	m_ColorPalatte[nIndex++] = (DWORD)0x00ff00; // lime
	m_ColorPalatte[nIndex++] = (DWORD)0x32cd32; // limegreen
	m_ColorPalatte[nIndex++] = (DWORD)0x00ff7f; // springgreen
	m_ColorPalatte[nIndex++] = (DWORD)0x3cb371; // mediumseagreen
	m_ColorPalatte[nIndex++] = (DWORD)0xadff2f; // greenyellow
	m_ColorPalatte[nIndex++] = (DWORD)0x8fbc8f; // darkseagreen
	m_ColorPalatte[nIndex++] = (DWORD)0x90ee90; // lightgreen
	m_ColorPalatte[nIndex++] = (DWORD)0x98fb98; // palegreen
	m_ColorPalatte[nIndex++] = (DWORD)0x9acd32; // yellowgreen
	m_ColorPalatte[nIndex++] = (DWORD)0x2e8b57; // seagreen
	m_ColorPalatte[nIndex++] = (DWORD)0x00fa9a; // mediumspringgreen
	m_ColorPalatte[nIndex++] = (DWORD)0x20b2aa; // lightseagreen
	m_ColorPalatte[nIndex++] = (DWORD)0x66cdaa; // mediumaquamarine
	m_ColorPalatte[nIndex++] = (DWORD)0x228b22; // forestgreen
	m_ColorPalatte[nIndex++] = (DWORD)0x008b8b; // darkcyan
	m_ColorPalatte[nIndex++] = (DWORD)0x008080; // teal
	m_ColorPalatte[nIndex++] = (DWORD)0x006400; // darkgreen
	m_ColorPalatte[nIndex++] = (DWORD)0x556b2f; // darkolivegreen
	m_ColorPalatte[nIndex++] = (DWORD)0x008000; // green
	m_ColorPalatte[nIndex++] = (DWORD)0x808000; // olive
	m_ColorPalatte[nIndex++] = (DWORD)0x6b8e23; // olivedrab
	m_ColorPalatte[nIndex++] = (DWORD)0xbdb76b; // darkkhaki
	m_ColorPalatte[nIndex++] = (DWORD)0xdaa520; // goldenrod
}

COLORREF CColorPalatte::GetFrameColor(int nIndex)
{
	COLORREF color = RGB(180, 180, 180);

	if(nIndex < FRAME_COLOR_CNT)
	{
		color = m_ColorPalatte[nIndex]; // Frame_Wnd.h FRAME_LEVEL_STEP
	}
	else
	{
		int nBaseColor = 250 - (nIndex/3+1) * 15;
		color = RGB( (nIndex%3 == 0 ? 250 : nBaseColor)  // red 
					,(nIndex%3 == 1 ? 250 : nBaseColor)  // green 
					,(nIndex%3 == 2 ? 250 : nBaseColor));// blue
	}

	return color;
}
