#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "resource.h"
#include "WizardSubContents.h"
#include "Schedule.h"
#include "ReposControl.h"


// CImageSelectDlg 대화 상자입니다.

class CImageSelectDlg : public CWizardSubContents
{
	DECLARE_DYNAMIC(CImageSelectDlg)

public:
	CImageSelectDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CImageSelectDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_IMAGE_SELECT_DLG };

	virtual void SetData(CString strContentsId, CString strValue);
	virtual void GetData(CString& strContentsId, CString& strValue);

	virtual CONTENTS_TYPE GetContentsType() { return CONTENTS_IMAGE; }
	virtual void SetPreviewMode(bool bPreviewMode);

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	
	CComboBox			m_cbContents;
	CPictureSchedule	m_wndImage;
	CStatic				m_framePreview;
	CReposControl		m_reposControl;
	bool				LoadContents(LPCTSTR lpszFullPath);

public:
	afx_msg void OnCbnSelchangeCbContents();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
