// OrbInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "OrbInfoDlg.h"
#include "Enviroment.h"
#include "OrbInfoModDlg.h"
#include "common\libscratch\OrbConn.h"
#include <winsock2.h>

#define STR_OI_NAME		LoadStringById(IDS_ORBINFODLG_LIST001)
#define STR_OI_IP		LoadStringById(IDS_ORBINFODLG_LIST002)
#define STR_OI_PORT		LoadStringById(IDS_ORBINFODLG_LIST003)

static BOOL ConnectTest(CString strIp, short nPort)
{
	TraceLog(("ConnectTest IP:%s:%d", strIp, nPort));

	// open
	SOCKET hSocket = INVALID_SOCKET;
	hSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(hSocket == INVALID_SOCKET) return FALSE;

	TraceLog(("create socket"));

	// connect
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr((LPCTSTR)strIp);
	addr.sin_port = htons(nPort);
	if(	connect(hSocket, (sockaddr*)&addr, sizeof(addr)) == SOCKET_ERROR ) return FALSE;

	TraceLog(("connect"));

	// close
	closesocket(hSocket);
	return TRUE;
}

// COrbInfoDlg dialog
IMPLEMENT_DYNAMIC(COrbInfoDlg, CDialog)

COrbInfoDlg::COrbInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COrbInfoDlg::IDD, pParent)
{
	m_bQuit = false;
}

COrbInfoDlg::~COrbInfoDlg()
{
}

void COrbInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CONNSERVER, m_lsctrOrbInfo);
}

BEGIN_MESSAGE_MAP(COrbInfoDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_CI_ADD, &COrbInfoDlg::OnBnClickedButtonCiAdd)
	ON_BN_CLICKED(IDC_BUTTON_CI_DEL, &COrbInfoDlg::OnBnClickedButtonCiDel)
	ON_BN_CLICKED(IDC_BUTTON_CI_MOD, &COrbInfoDlg::OnBnClickedButtonCiMod)
	ON_BN_CLICKED(IDOK, &COrbInfoDlg::OnBnClickedOk)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_CONNSERVER, OnLvnColumnclickListOrbInfo)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_CONNSERVER, OnNMCustomdrawListOrbInfo)
	ON_NOTIFY(NM_CLICK, IDC_LIST_CONNSERVER, OnNMClickListOrbInfo)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONNSERVER, OnNMDblclkListOrbInfo)
	ON_BN_CLICKED(IDC_BUTTON_CI_TEST, &COrbInfoDlg::OnBnClickedButtonCiTest)
END_MESSAGE_MAP()

// COrbInfoDlg message handlers
BOOL COrbInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitList();
	InsertData();
	SelectCurInfo();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void COrbInfoDlg::OnDestroy()
{
	CDialog::OnDestroy();

	COrbConn::Release();
}

void COrbInfoDlg::OnBnClickedButtonCiAdd()
{
	COrbInfoModDlg dlg;
	dlg.SetList(&m_lsctrOrbInfo);
	if(dlg.DoModal() != IDOK)
		return;

	int nPort;
	CString szName, szIP;
	dlg.GetOrbInfo(szName, szIP, nPort);

	SOrbConnInfo Info;
	ZeroMemory(&Info, sizeof(SOrbConnInfo));
	strncpy(Info.name, szName, sizeof(Info.name));
	strncpy(Info.ip, szIP, sizeof(Info.ip));
	Info.port = nPort;

	COrbConn::GetObject()->Add(&Info);
	InsertData();
}

void COrbInfoDlg::OnBnClickedButtonCiMod()
{
	for(int nRow = 0; nRow < m_lsctrOrbInfo.GetItemCount(); nRow++){
		if(!m_lsctrOrbInfo.GetCheck(nRow))
			continue;

		SOrbConnInfo* pInfo = (SOrbConnInfo*)m_lsctrOrbInfo.GetItemData(nRow);
		if(!pInfo)	continue;

		COrbInfoModDlg dlg;
		dlg.m_bModify = true;
		dlg.SetOrbInfo(pInfo->name, pInfo->ip, pInfo->port);
		if(dlg.DoModal() != IDOK)
			return;

		CString szName, szIP;
		int nPort;
		dlg.GetOrbInfo(szName, szIP, nPort);

		if(m_szName == szName){
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_ORBINFODLG_MSG001), szName);
			if(UbcMessageBox(szMsg, MB_YESNO) != IDYES)
				return;

			m_szName = szName;
			m_szIP = szIP;
			m_nPort = nPort;
			m_bQuit = true;

			OnOK();
		}
		
		strncpy(pInfo->name, szName, sizeof(pInfo->name));
		strncpy(pInfo->ip, szIP, sizeof(pInfo->ip));
		pInfo->port = nPort;

		COrbConn::GetObject()->Mod(pInfo);
		InsertData();
		return;
	}
}

void COrbInfoDlg::OnBnClickedButtonCiDel()
{
	if(UbcMessageBox(LoadStringById(IDS_ORBINFODLG_MSG002), MB_YESNO) != IDYES){
		return;
	}

	for(int nRow = 0; nRow < m_lsctrOrbInfo.GetItemCount(); nRow++){
		if(!m_lsctrOrbInfo.GetCheck(nRow))
			continue;

		SOrbConnInfo* pInfo = (SOrbConnInfo*)m_lsctrOrbInfo.GetItemData(nRow);

		if(m_szName == pInfo->name){
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_ORBINFODLG_MSG003), m_szName);
			UbcMessageBox(szMsg);
			return;
		}

		COrbConn::GetObject()->Del(pInfo);	
		InsertData();
		return;
	}
}

void COrbInfoDlg::OnBnClickedOk()
{
	for(int i = 0; i < m_lsctrOrbInfo.GetItemCount(); i++){
		if(m_lsctrOrbInfo.GetCheck(i)){
			SOrbConnInfo* pInfo = (SOrbConnInfo*)m_lsctrOrbInfo.GetItemData(i);
			if(!pInfo)	return;

			m_szName = pInfo->name;
			m_szIP = pInfo->ip;
			m_nPort = pInfo->port;

			SOrbConnInfo* pCurInfo = COrbConn::GetObject()->GetCur();
			if(strcmp(pCurInfo->ip, pInfo->ip) || pCurInfo->port != pInfo->port){
				m_bQuit = true;
			}

			OnOK();
			return;
		}
	}

	UbcMessageBox(LoadStringById(IDS_ORBINFODLG_MSG004));
}

void COrbInfoDlg::InitList()
{
	m_lsctrOrbInfo.SetExtendedStyle(m_lsctrOrbInfo.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	
	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_LIST_HEADER);
	m_ilOrbInfo.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilOrbInfo.Add(&bmCheck, RGB(255, 255, 255));
	m_lsctrOrbInfo.SetImageList(&m_ilOrbInfo, LVSIL_SMALL);

	m_szColum[eColName] = STR_OI_NAME;
	m_szColum[eColIP]	= STR_OI_IP;
	m_szColum[eColPort] = STR_OI_PORT;

	for(int nRow = 0; nRow < eColEnd; nRow++){
		m_lsctrOrbInfo.InsertColumn(nRow, m_szColum[nRow], LVCFMT_LEFT, 100);
	}

	m_lsctrOrbInfo.InitHeader(IDB_LIST_HEADER);
}

void COrbInfoDlg::InsertData()
{
	m_lsctrOrbInfo.DeleteAllItems();

	int nRow = 0;
	OrbConList* pList = COrbConn::GetObject()->GetList();
	for(OrbConList::iterator Iter = pList->begin(); Iter != pList->end(); Iter++){
		SOrbConnInfo* pInfo = (*Iter);
		if(!pInfo)		continue;
		if(!pInfo->use)	continue;

		m_lsctrOrbInfo.InsertItem(nRow, pInfo->name, CUTBListCtrl::LC_RDOU);
		m_lsctrOrbInfo.SetItemText(nRow, eColIP, pInfo->ip);
		m_lsctrOrbInfo.SetItemText(nRow, eColPort, ToString(pInfo->port));
		m_lsctrOrbInfo.SetItemData(nRow, (DWORD_PTR)pInfo);

		if(m_szName == pInfo->name && m_szIP == pInfo->ip && m_nPort == pInfo->port){
			LVITEM item;
			item.iItem = 0;
			item.iSubItem = 0;
			item.mask = LVIF_IMAGE;
			item.iImage = CUTBListCtrl::LC_RDOS;

			m_lsctrOrbInfo.SetItem(&item);
			m_lsctrOrbInfo.SetCheck(nRow, true);
		}else{
			m_lsctrOrbInfo.SetCheck(nRow, false);
		}
		
		nRow++;
	}
}

void COrbInfoDlg::SelectCurInfo()
{
	SOrbConnInfo* pInfo = COrbConn::GetObject()->GetCur();

	LVITEM item;
	for(int i = 0; i < m_lsctrOrbInfo.GetItemCount(); i++){
		SOrbConnInfo* pInfoList = (SOrbConnInfo*)m_lsctrOrbInfo.GetItemData(i);
		if(!pInfoList)	continue;

		item.iItem = i;
		item.iSubItem = 0;
		item.mask = LVIF_IMAGE;
		

		if(strcmp(pInfoList->name, pInfo->name) == 0){
			m_lsctrOrbInfo.SetCheck(i, true);
			item.iImage = CUTBListCtrl::LC_RDOS;
		}else{
			m_lsctrOrbInfo.SetCheck(i, false);
			item.iImage = CUTBListCtrl::LC_RDOU;
		}
		
		m_lsctrOrbInfo.SetItem(&item);
	}
}

void COrbInfoDlg::OnLvnColumnclickListOrbInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	int	nColumn = pNMLV->iSubItem;
	m_lsctrOrbInfo.SetSortHeader(nColumn);
	m_lsctrOrbInfo.SortItems(CompareProc, (DWORD_PTR)this);
}

void COrbInfoDlg::OnNMCustomdrawListOrbInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
	// TODO: Add your control notification handler code here
	*pResult = 0;

	if( CDDS_PREPAINT == pLVCD->nmcd.dwDrawStage ){
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if( CDDS_ITEMPREPAINT == pLVCD->nmcd.dwDrawStage ){
		COLORREF crText;
        COLORREF crTextBk;

		if(m_lsctrOrbInfo.GetCheck(pLVCD->nmcd.dwItemSpec)){
			crText = RGB(255,255,255);
			crTextBk = RGB(49,106,197);
		}else{
			crText = RGB(0,0,0);
			crTextBk = RGB(255,255,255);
		}

		pLVCD->clrText = crText;
        pLVCD->clrTextBk = crTextBk;
		*pResult = CDRF_DODEFAULT;
	}
}

void COrbInfoDlg::OnNMClickListOrbInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	int nRow = pNMLV->iItem;
//	int	nColumn = pNMLV->iSubItem;

	if(nRow < 0)	return;

	LVITEM item;
	for(int i = 0; i < m_lsctrOrbInfo.GetItemCount(); i++){
		item.iItem = i;
		item.iSubItem = 0;
		item.mask = LVIF_IMAGE;
		item.iImage = CUTBListCtrl::LC_RDOU;

		m_lsctrOrbInfo.SetItem(&item);
		m_lsctrOrbInfo.SetCheck(i, false);
	}

	item.iItem = nRow;
	item.iSubItem = 0;
	item.mask = LVIF_IMAGE;
	item.iImage = CUTBListCtrl::LC_RDOS;

	m_lsctrOrbInfo.SetItem(&item);
	m_lsctrOrbInfo.SetCheck(nRow);
}

void COrbInfoDlg::OnNMDblclkListOrbInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	OnBnClickedOk();
}

int CALLBACK COrbInfoDlg::CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	COrbInfoDlg* pDlg = (COrbInfoDlg*)lParam;
	CUTBListCtrl* pListCtrl = &pDlg->m_lsctrOrbInfo;

	LVFINDINFO    lvFind;
	lvFind.flags = LVFI_PARAM;

	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	CString strItem1 = pListCtrl->GetItemText(nIndex1, pListCtrl->GetCurSortCol());
	CString strItem2 = pListCtrl->GetItemText(nIndex2, pListCtrl->GetCurSortCol());

	if(pListCtrl->IsAscend())
		return strItem1.Compare(strItem2);
	else
		return strItem2.Compare(strItem1);
}

void COrbInfoDlg::SetOrbInfo(LPCTSTR szName, LPCTSTR szIP, int nPort)
{
	m_szName = szName;
	m_szIP = szIP;
	m_nPort = nPort;
}

void COrbInfoDlg::GetOrbInfo(CString& szName, CString& szIP, int& nPort)
{
	szName = m_szName;
	szIP = m_szIP;
	nPort = m_nPort;
}

void COrbInfoDlg::OnBnClickedButtonCiTest()
{
	for(int nRow = 0; nRow < m_lsctrOrbInfo.GetItemCount(); nRow++)
	{
		if(!m_lsctrOrbInfo.GetCheck(nRow)) continue;

		SOrbConnInfo* pInfo = (SOrbConnInfo*)m_lsctrOrbInfo.GetItemData(nRow);
		if(!pInfo) continue;

		CWaitMessageBox wait;

		CString szMsg;
		if(ConnectTest(pInfo->ip, pInfo->port) == FALSE)
		{
			szMsg = LoadStringById(IDS_ORBINFODLG_MSG006);
			//pDlg->PostMessage(WM_WAIT_FOR_LOGIN, FALSE, NULL);
		}
		else
		{
			szMsg = LoadStringById(IDS_ORBINFODLG_MSG007);
		}

		UbcMessageBox(szMsg);

		return;
	}
}
