#pragma once
#include "interface.h"
#include <map>
#include <list>
#include <string>
#include "Enviroment.h"

struct SHostInfo4Studio;
class cciReply;

BOOL IsInitORB();
BOOL InitORB(LPCTSTR, int);
void DestroyORB();
BOOL CheckORB();
void copTrace(const char*, int, const char*);
BOOL GetMyIp(CString& myIp);
int  copCheckLogin(CString&, CString, CString, CString&, CString& strCustomer);
bool InitSite(CString szSite, CList<CString>& lsSite);
CString GetSiteName(CString szSiteId);
void copGetPackage(LPCTSTR szSite, LPCTSTR szWhere = NULL);
BOOL copSetPackage(std::multimap<std::string, void*>&, CString, CString,std::string& errMsg);
BOOL applyPackage(CString, CString, CString, CString, CString, std::list<std::string>, std::string& errMsg);
BOOL getCodeList(CodeItemList& list, CString strCustomer);
BOOL getCustomerList(CustomerInfoList& list);
BOOL copyContents(CString, CString, CString, CStringList& children);
BOOL GetAllCategory(CStringList& aCategoryList);
BOOL CopErrCheck(cciReply* reply, bool bShowMsgBox=true);
BOOL IsExistPackage(LPCTSTR szPackage, BOOL &bExist);	// 2010.08.06 by gwangsoo
