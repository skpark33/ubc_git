#pragma once
#include "afxcmn.h"
#include "afxdtctl.h"
#include "afxwin.h"
#include "common/HoverButton.h"
#include "common/UTBListCtrl.h"

// CEditHostDlg 대화 상자입니다.

class CEditHostDlg : public CDialog
{
	DECLARE_DYNAMIC(CEditHostDlg)

public:
	CEditHostDlg(CWnd* pParent, CString strUsetCreate, CStringArray& aryHostName);   // 표준 생성자입니다.
	virtual ~CEditHostDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_EDIT_HOST_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	void AddHollyday(CString);
	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

	void InitShutdownWeek(int nIndex);

	DECLARE_MESSAGE_MAP()
public:
	CString			m_strHostName;		///<host명
	CString			m_strOldHostName;	///<수정되기 전의 host 이름
	CIPAddressCtrl	m_addrIP;			///<host ip 주소를 표현하는 컨트롤
	CString			m_strFTPPort;		///<host ftp의 port
	CString			m_strSvrPort;		///<host 서버의 port
	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;
	CString			m_strIPAddr;		///<host ip 주소 문자열
	BOOL			m_bShutdownAuto;
	CString			m_strShutdownTime;	///<host 장비가 자동으로 shutdown되는 시간:분 
	BOOL			m_bShutdownWeekday;
	CString			m_strWeekShutdownTime;	///<host 장비가 자동으로 shutdown되는 시간:분 
//	CString			m_strHours;			///<host 장비가 자동으로 shutdown되는 시간
//	CString			m_strMinutes;		///<host 장비가 자동으로 shutdown되는 분
	CString			m_strUserCreate;	///<자동으로 검색된 자입가 아닌 사용자가 직접 추가한 host 인지 여부("true", "false")
	CStringArray	m_aryHostName;		///<ini 파일에 존재하는 host들의 이름 배열
	CDateTimeCtrl	m_tmShutdown;
	CDateTimeCtrl	m_tmPoweron;
	CButton			m_ckShutdown;
	CButton			m_ckPoweron;
	CString			m_szDesc;
	CString			m_strMacAddress;
	int				m_nMonitorCnt;

	CButton			m_ckWeek[7];
	CDateTimeCtrl	m_tmShutdownWeek[7];
	CUTBListCtrl	m_lscHoliday;
	CMonthCalCtrl	m_ctrCalendar;

	CButton			m_kbShutdownAuto;
	CButton			m_kbShutdownWeekday;
	void			InitShutdownAuto(BOOL bEnable);
	void			InitShutdownWeekday(BOOL bEnable);

	CString			m_strHostID1;		///<hostID
	CString			m_strHostID2;		///<hostID

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedCheckShutdown();
	afx_msg void OnBnClickedCheckPoweron();
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
	afx_msg void OnBnClickedCheckWeek6();
	afx_msg void OnBnClickedCheckWeek0();
	afx_msg void OnBnClickedCheckWeek1();
	afx_msg void OnBnClickedCheckWeek2();
	afx_msg void OnBnClickedCheckWeek3();
	afx_msg void OnBnClickedCheckWeek4();
	afx_msg void OnBnClickedCheckWeek5();
	afx_msg void OnBnClickedKbShutdownAuto();
	afx_msg void OnBnClickedKbWeekdayShutdown();
	CButton m_ckW1;
	CButton m_ckW2;
	CButton m_ckW3;
	CButton m_ckW4;
	CButton m_ckW5;
	CButton m_ckW6;
	CButton m_ckW0;
};
