#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#include "ReposControl.h"
#include "ContentsListCtrl.h"

// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업
#include "common/HoverButton.h"
// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업

class CUBCStudioDoc;

// CSelectContentsDialog 대화 상자입니다.

class CSelectContentsDialog : public CDialog
{
	DECLARE_DYNAMIC(CSelectContentsDialog)

public:
	CSelectContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSelectContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SELECT_CONTENTS };

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CUBCStudioDoc*	m_pDocument;

	CReposControl	m_reposControl;

public:

	afx_msg void OnSize(UINT nType, int cx, int cy);

	CContentsListCtrl	m_lcContents;
	
	CONTENTS_INFO*		m_pContentsInfo;

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	/*
	CButton		m_btnOK;
	CButton		m_btnCancel;
	*/
	CHoverButton			m_btnOK;
	CHoverButton			m_btnCancel;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	void	SetDocuments(CUBCStudioDoc* pDoc) { m_pDocument = pDoc; };
	afx_msg void OnNMDblclkListContents(NMHDR *pNMHDR, LRESULT *pResult);
};
