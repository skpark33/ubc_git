#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "common/HoverButton.h"
#include "CheckListCtrl.h"


// CDeletePackageDlg 대화 상자입니다.

class CDeletePackageDlg : public CDialog
{
	DECLARE_DYNAMIC(CDeletePackageDlg)

public:
	CDeletePackageDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDeletePackageDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DELETE_PACKAGE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CCheckListCtrl	m_lcPackageList;
	CHoverButton	m_btnDelete;
	CHoverButton	m_btnOK;
	CImageList		m_imgCheck;						///<Check box image list

	void		InitList();							///<Host list를 list control에 설정

	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedDeleteBtn();
};
