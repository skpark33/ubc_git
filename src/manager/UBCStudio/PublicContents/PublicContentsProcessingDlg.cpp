// PublicContentsProcessingDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "etc.h"
#include "resource.h"
#include "PublicContentsProcessingDlg.h"

//#include "ThreadFunc.h"
#include "Enviroment.h"

#include "common/libHttpRequest/HttpRequest.h"


CMutex		g_mutexThreadPCProcessing;
int			g_nThreadPCProcessing = 0;

//extern CString	WEB_SERVER_IP;

// CPublicContentsProcessingDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPublicContentsProcessingDlg, CDialog)

CPublicContentsProcessingDlg::CPublicContentsProcessingDlg(LPCTSTR szSiteID, LPCTSTR szProgramID, CWnd* pParent /*=NULL*/)
	: CDialog(CPublicContentsProcessingDlg::IDD, pParent)
	, m_pThread ( NULL )
	, m_listCurrent ( NULL )
	, m_bUpdatedListDetached ( false )
	, m_listUpdated ( NULL )
	, m_strCSiteID ( szSiteID )
	, m_strProgramID ( szProgramID )
{
	char szBuf[1000] = {0};
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

	::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", szBuf, sizeof(szBuf), szPath);

	m_strServerIP = szBuf;
	m_nServerPort = 8080;
}

CPublicContentsProcessingDlg::~CPublicContentsProcessingDlg()
{
	if(m_bUpdatedListDetached == false && m_listUpdated != NULL)
	{
		for(int i=0; i<m_listUpdated->GetCount(); i++)
		{
			CONTENTS_INFO* info = m_listUpdated->GetAt(i);
			delete info;
		}
		m_listUpdated->RemoveAll();
		delete m_listUpdated;
		m_listUpdated = NULL;
	}
}

void CPublicContentsProcessingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_PROCESSING_STATUS, m_stcStatus);
	DDX_Control(pDX, IDC_LIST_COMMON_CONTENTS, m_lcContents);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CPublicContentsProcessingDlg, CDialog)
	ON_WM_TIMER()
	ON_MESSAGE(WM_CHANGE_STATUS, OnChangeStatus)
	ON_MESSAGE(WM_CHANGE_PROCESSING, OnChangeProcessing)
	ON_MESSAGE(WM_COMPLETED_ALL_PROCESSING, OnCompletedAllProcessing)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_COMMON_CONTENTS, OnLvnKeydownListCommonContents)
END_MESSAGE_MAP()


// CPublicContentsProcessingDlg 메시지 처리기입니다.

BOOL CPublicContentsProcessingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//
	m_lcContents.SetExtendedStyle(m_lcContents.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	LISTCTRL_COLUMN_INFO info[COLUMN_COUNT] = {
		//{ "", idx, width, align },
		{ LoadStringById(IDS_CM_LC_COLUMN_CONTENTS_NAME), COLUMN_CNAME,    100, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_FILENAME), COLUMN_FNAME,    150, LVCFMT_LEFT },
		//{ LoadStringById(IDS_CM_LC_COLUMN_LOCATION), COLUMN_LOCATION, 150, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_FILESIZE), COLUMN_SIZE,     100, LVCFMT_RIGHT },
		{ LoadStringById(IDS_CM_LC_COLUMN_STATUS),   COLUMN_STATUS,   100, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_RESULT),   COLUMN_RESULT,   150, LVCFMT_LEFT },
	};

	InitListCtrlColumn(m_lcContents, info, COLUMN_COUNT);

	m_lcContents.SetProgressBarCol(COLUMN_STATUS);
	m_lcContents.InitHeader(IDB_LIST_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcContents.SetSortEnable(false);

	//
	m_btnOK.EnableWindow(FALSE);
	m_btnCancel.EnableWindow(FALSE);

	SetTimer(TIMER_INIT_ID, TIMER_INIT_TIME, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPublicContentsProcessingDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnOK();
}

void CPublicContentsProcessingDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnCancel();

	m_btnOK.EnableWindow(TRUE);
	m_btnCancel.EnableWindow(FALSE);

	DeleteThread();
}

void CPublicContentsProcessingDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	DeleteThread();

	if(nIDEvent == TIMER_INIT_ID)
	{
		KillTimer(TIMER_INIT_ID);

		GUARD(g_mutexThreadPCProcessing);

			AFX_THREADPROC thread_func = NULL;
			m_listCurrent = NULL;

			PC_COMMAND_THREAD_PARAM* param = new PC_COMMAND_THREAD_PARAM;
			param->hParentWnd = GetSafeHwnd();
			param->strServerIP = m_strServerIP;
			param->nServerPort = m_nServerPort;
			param->nThreadSerial = g_nThreadPCProcessing;
			param->strSiteID = m_strCSiteID;
			param->strProgramID = m_strProgramID;

/*			if(m_listUpload.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_UPLOADING));
				thread_func = UploadThreadFunc;
				param->pListObjList = m_listCurrent= &m_listUpload;
			}
			else if(m_listDownload.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_DOWNLOADING));
				thread_func = DownloadThreadFunc;
				param->pListObjList = m_listCurrent= &m_listDownload;
			}
			else if(m_listCopyToPrivate.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_COPY_TO_PACKAGE));
				thread_func = CopyToPrivateThreadFunc;
				param->pListObjList = m_listCurrent= &m_listCopyToPrivate;

				m_listUpdated = new CONTENTS_INFO_LIST;
			}
			else */ if(m_listCopyFromPrivateToPublic.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_COPY_TO_COMMON_CONTENTS));
				thread_func = CopyFromPrivateToPublicThreadFunc;
				param->pListObjList = &m_listCopyFromPrivateToPublic;
				m_listCurrent = &m_listCopyFromPrivateToPublic;
			}
/*			else if(m_listModify.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_MODIFY));
				thread_func = ModifyThreadFunc;
				param->pListObjList = m_listCurrent = &m_listModify;
			}
			else if(m_listVerify.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_VERIFY));
				thread_func = VerifyThreadFunc;
				param->pListObjList = m_listCurrent = &m_listVerify;
			}
			else if(m_listEncode.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_ENCODE));
				thread_func = EncodeThreadFunc;
				param->pListObjList = m_listCurrent = &m_listEncode;
			}
			else if(m_listBackup.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_BACKUP));
				thread_func = BackupThreadFunc;
				param->pListObjList = m_listCurrent = &m_listBackup;
			}
			else if(m_listRestore.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_RESTORE));
				thread_func = RestoreThreadFunc;
				param->pListObjList = m_listCurrent = &m_listRestore;
			}
			else if(m_listCompress.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_COMPRESS));
				thread_func = CompressThreadFunc;
				param->pListObjList = m_listCurrent = &m_listCompress;
			}
			else if(m_listDecompress.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_DECOMPRESS));
				thread_func = DecompressThreadFunc;
				param->pListObjList = m_listCurrent = &m_listDecompress;
			}
			else if(m_listDelete.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_DELETE));
				thread_func = DeleteThreadFunc;
				param->pListObjList = m_listCurrent = &m_listDelete;
			}
*/
			if(m_listCurrent && thread_func && param)
			{
				for(int i=0; i<m_listCurrent->GetCount(); i++)
				{
					CContentsObject* obj = (CContentsObject*)m_listCurrent->GetAt(i);

					obj->processingResultValue = 0;

					int idx = m_lcContents.GetItemCount();
					m_lcContents.InsertItem(idx, _T(""));

					m_lcContents.SetItemText(idx, COLUMN_CNAME, obj->strContentsName);
					m_lcContents.SetItemText(idx, COLUMN_FNAME, obj->strFilename);
					//m_lcContents.SetItemText(idx, COLUMN_LOCATION, obj->strServerLocation);
					m_lcContents.SetItemText(idx, COLUMN_SIZE, ToMoneyTypeString(obj->nFilesize));
					m_lcContents.SetItemText(idx, COLUMN_STATUS, _T("0"));
					m_lcContents.SetItemText(idx, COLUMN_RESULT, _T(""));

					m_lcContents.SetItemData(idx, (DWORD)obj);
				}

				//
				m_pThread = ::AfxBeginThread(thread_func, param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
				m_pThread->m_bAutoDelete = TRUE;
				m_pThread->ResumeThread();

				//
				m_btnOK.EnableWindow(FALSE);
				m_btnCancel.EnableWindow(TRUE);
			}
			else
			{
				OnOK();
			}
		END_GUARD;
	}
}

BOOL CPublicContentsProcessingDlg::DeleteThread()
{
	if(m_pThread)
	{
		BeginWaitCursor();
		GUARD(g_mutexThreadPCProcessing);
			g_nThreadPCProcessing++;
		END_GUARD;

		try
		{
			DWORD dwExitCode;

			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, 3000);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}

			//delete m_pThread;
		}
		catch(...)
		{
		}

		m_pThread = NULL;
		EndWaitCursor();
	}

	return TRUE;
}

LRESULT CPublicContentsProcessingDlg::OnChangeStatus(WPARAM wParam, LPARAM lParam)
{
	int idx = wParam;
	if(m_listCurrent==NULL || idx >= m_listCurrent->GetCount()) return 0;

	CContentsObject* obj = m_listCurrent->GetAt(idx);

	switch(lParam)
	{
	default:
	case eUnknownError:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_UNKNOWN_ERROR));
		break;
	case eNowFTPProcessing:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_FTP_PROCESSING));
		break;
	case eCompleted:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_COMPLETED));
		m_lcContents.SetItemText(idx, COLUMN_STATUS, _T("100"));

		if(m_listUpdated != NULL)
		{
			CONTENTS_INFO* info = new CONTENTS_INFO;
			*info = *(CONTENTS_INFO*)obj;
			m_listUpdated->Add(info);
		}
		break;
	case eAlreadyContentsExist:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ALREADY_CONTENTS_EXIST));
		break;
	case eAlreadyContentsVerified:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ALREADY_CONTENTS_VERIFIED));
		break;
	case eErrorOpenLocalFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_OPEN_LOCAL_FILE));
		break;
	case eErrorCreateRemoteFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CREATE_REMOTE_FILE));
		break;
	case eErrorConnectRemoteServer:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CONNECT_REMOTE_SERVER));
		break;
	case eErrorFTPProcessing:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_FTP_PROCESSING));
		break;
	case eErrorCreateLocalFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CREATE_LOCAL_FILE));
		break;
	case eErrorOpenRemoteFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_OPEN_REMOTE_FILE));
		break;
	case eErrorNowWorking:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_NOW_WORKING));
		break;
	case eErrorNotExist:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_NOT_EXIST2));
		break;
	case eErrorCantDo:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CANT_DO));
		break;
	case eErrorUsingAnotherPackage:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_USING_ANOTHER_PACKAGE));
		break;
	case eErrorNoContentsFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_NO_CONTENTS_FILE));
		break;
	}
	return 0;
}

LRESULT CPublicContentsProcessingDlg::OnChangeProcessing(WPARAM wParam, LPARAM lParam)
{
	if(wParam >= m_lcContents.GetItemCount()) return 0;

	int idx = wParam;

	m_lcContents.SetItemText(idx, COLUMN_STATUS, ToString(lParam));

	return 0;
}

LRESULT CPublicContentsProcessingDlg::OnCompletedAllProcessing(WPARAM wParam, LPARAM lParam)
{
	OnCancel();

	if(m_listCurrent)
	{
		bool all_complete = true;
		for(int i=0; i<m_listCurrent->GetCount(); i++)
		{
			CContentsObject* obj = m_listCurrent->GetAt(i);
			int result = obj->processingResultValue;

			if(result != eCompleted)
			{
				all_complete = false;
				break;
			}
		}

		if(all_complete)
		{
			UbcMessageBox(CM_NOTIFY_SUCCESS_ALL_PROCESSING, MB_ICONINFORMATION);
			OnOK();
		}
		else
		{
			UbcMessageBox(LoadStringById(IDS_CM_NOTIFY_MSG_ERROR_IN_PROCESS_LIST), MB_ICONWARNING);
		}
	}

	return 0;
}

void CPublicContentsProcessingDlg::AddCopyFromPrivateContents(CONTENTS_INFO* info)
{
	CContentsObject* obj = new CContentsObject;

	*(CONTENTS_INFO*)obj = *info;

	m_listCopyFromPrivateToPublic.Add(obj);
}

void CPublicContentsProcessingDlg::AddCopyFromPrivateContents(CONTENTS_INFO_LIST &info_list)
{
	for(int i=0; i<info_list.GetCount(); i++)
	{
		CONTENTS_INFO* info = info_list.GetAt(i);

		CContentsObject* obj = new CContentsObject;

		*(CONTENTS_INFO*)obj = *info;

		m_listCopyFromPrivateToPublic.Add(obj);
	}
}

void CPublicContentsProcessingDlg::OnLvnKeydownListCommonContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pLVKeyDow->wVKey == VK_F5)
	{
		CContentsObjectList obj_list;

		for(int i=0; i<m_lcContents.GetItemCount(); i++)
		{
			CContentsObject* obj = (CContentsObject*)m_lcContents.GetItemData(i);

			if(obj->processingResultString.GetLength() > 0)
			{
				UbcMessageBox(obj->processingResultString);
			}
		}
	}
}

UINT CopyFromPrivateToPublicThreadFunc(LPVOID pParam)
{
	PC_COMMAND_THREAD_PARAM* param = (PC_COMMAND_THREAD_PARAM*)pParam;

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_SERVER, param->strServerIP, param->nServerPort);

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadPCProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadPCProcessing);
			if(param->nThreadSerial == g_nThreadPCProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString send_msg;
		send_msg.Format(_T("siteId=%s&programId=%s&contentsId=%s"), 
			ConvertToURLString(param->strSiteID), 
			ConvertToURLString(param->strProgramID), 
			ConvertToURLString(obj->strId) );

		TRACE(_T("CopyFromPrivateToPublicThreadFunc : (%s)\r\n"), send_msg);

		CString response = _T("");

		http_request.RequestPost("/UBC_PublicContents/copy_from_private_to_public.asp", send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("already")) == 0)
		{
			result_code = eAlreadyContentsExist;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else if(response.CompareNoCase(_T("cant")) == 0)
		{
			result_code = eErrorCantDo;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadPCProcessing);
			if(param->nThreadSerial == g_nThreadPCProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadPCProcessing);
		if(param->nThreadSerial == g_nThreadPCProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}
