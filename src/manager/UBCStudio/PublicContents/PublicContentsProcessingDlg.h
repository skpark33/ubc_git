#pragma once

#include "common/UTBListCtrlEx.h"

#include "etc.h"


// CPublicContentsProcessingDlg 대화 상자입니다.

class CPublicContentsProcessingDlg : public CDialog
{
	DECLARE_DYNAMIC(CPublicContentsProcessingDlg)

public:
	CPublicContentsProcessingDlg(LPCTSTR lpszSiteID, LPCTSTR lpszProgramID, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPublicContentsProcessingDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PROCESSING };

protected:
	enum {
		COLUMN_CNAME = 0,	// 컨텐츠 이름
		COLUMN_FNAME,		// 파일 이름
		//COLUMN_LOCATION,	// 파일 위치
		COLUMN_SIZE,		// 파일크기
		COLUMN_STATUS,		// 진행상태(%)
		COLUMN_RESULT,		// 전송결과
		COLUMN_COUNT,
	};

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CWinThread*	m_pThread;
	BOOL		DeleteThread();

	CString		m_strCSiteID;		// 현재 site id
	CString		m_strProgramID;		// 현재 program id

	//CContentsObjectList		m_listUpload;
	//CContentsObjectList		m_listDownload;
	//CContentsObjectList		m_listCopyToPrivate;
	CContentsObjectList			m_listCopyFromPrivateToPublic;
	//CContentsObjectList		m_listVerify;
	//CContentsObjectList		m_listEncode;
	//CContentsObjectList		m_listBackup;
	//CContentsObjectList		m_listRestore;
	//CContentsObjectList		m_listCompress;
	//CContentsObjectList		m_listDecompress;
	//CContentsObjectList		m_listDelete;
	//CContentsObjectList		m_listModify;

	CContentsObjectList*	m_listCurrent;				// ↖현재 contents 리스트 포인터

	bool					m_bUpdatedListDetached;		// m_listUpdated 삭제여부
	CONTENTS_INFO_LIST*		m_listUpdated;				// 업데이트된 리스트

	CString					m_strServerIP;
	int						m_nServerPort;

public:

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnChangeStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeProcessing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompletedAllProcessing(WPARAM wParam, LPARAM lParam);
	afx_msg void	OnLvnKeydownListCommonContents(NMHDR *pNMHDR, LRESULT *pResult);

	CStatic			m_stcStatus;
	CUTBListCtrlEx	m_lcContents;
	CButton			m_btnOK;
	CButton			m_btnCancel;

public:

	//void	AddUploadContents(CContentsObject* obj) { m_listUpload.Add(obj); };
	//void	AddUploadContents(CContentsObjectList& obj_list) { m_listUpload.Copy(obj_list); };
	//void	AddDownloadContents(CContentsObject* obj) { m_listDownload.Add(obj); };
	//void	AddDownloadContents(CContentsObjectList& obj_list) { m_listDownload.Copy(obj_list); };
	//void	AddModifyContents(CContentsObject* obj) { m_listModify.Add(obj); };
	//void	AddModifyContents(CContentsObjectList& obj_list) { m_listModify.Copy(obj_list); };
	//void	AddCopyToPrivateContents(CContentsObject* obj) { m_listCopyToPrivate.Add(obj); };
	//void	AddCopyToPrivateContents(CContentsObjectList& obj_list) { m_listCopyToPrivate.Copy(obj_list); };
//	void	AddCopyFromPrivateContents(CContentsObject* obj) { m_listCopyFromPrivateToPublic.Add(obj); };
//	void	AddCopyFromPrivateContents(CContentsObjectList& obj_list) { m_listCopyFromPrivateToPublic.Copy(obj_list); };
	void	AddCopyFromPrivateContents(CONTENTS_INFO* info);
	void	AddCopyFromPrivateContents(CONTENTS_INFO_LIST &info_list);
	//void	AddVerifyContents(CContentsObject* obj) { m_listVerify.Add(obj); };
	//void	AddVerifyContents(CContentsObjectList& obj_list) { m_listVerify.Copy(obj_list); };
	//void	AddEncodeContents(CContentsObject* obj) { m_listEncode.Add(obj); };
	//void	AddEncodeContents(CContentsObjectList& obj_list) { m_listEncode.Copy(obj_list); };
	//void	AddBackupContents(CContentsObject* obj) { m_listBackup.Add(obj); };
	//void	AddBackupContents(CContentsObjectList& obj_list) { m_listBackup.Copy(obj_list); };
	//void	AddRestoreContents(CContentsObject* obj) { m_listRestore.Add(obj); };
	//void	AddRestoreContents(CContentsObjectList& obj_list) { m_listRestore.Copy(obj_list); };
	//void	AddCompressContents(CContentsObject* obj) { m_listCompress.Add(obj); };
	//void	AddCompressContents(CContentsObjectList& obj_list) { m_listCompress.Copy(obj_list); };
	//void	AddDecompressContents(CContentsObject* obj) { m_listDecompress.Add(obj); };
	//void	AddDecompressContents(CContentsObjectList& obj_list) { m_listDecompress.Copy(obj_list); };
	//void	AddDeleteContents(CContentsObject* obj) { m_listDelete.Add(obj); };
	//void	AddDeleteContents(CContentsObjectList& obj_list) { m_listDelete.Copy(obj_list); };

	CONTENTS_INFO_LIST*		GetContentsUpdatedList() { m_bUpdatedListDetached=true; return m_listUpdated; };
	int		GetContentsUpdatedCount() { if(m_listUpdated) return m_listUpdated->GetCount(); return 0; };
};


typedef struct {
	HWND		hParentWnd;
	int			nThreadSerial;
	CString		strServerIP;
	int			nServerPort;
	CString		strSiteID;
	CString		strProgramID;
	CContentsObjectList*	pListObjList;
} PC_COMMAND_THREAD_PARAM;

static UINT CopyFromPrivateToPublicThreadFunc(LPVOID pParam);
