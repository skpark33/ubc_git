#pragma once

#include "common\HoverButton.h"
#include "ReposControl2.h"
#include "SplitStatic.h"

#include "common/UTBListCtrlEx.h"
//#include "UBCCopCommon/CopModule.h"
#include "afxcmn.h"

#include "common/libHttpRequest/HttpRequest.h"


///////////////////////////////////////////////////////////////////////////////
// CPublicContentsDlg 대화 상자입니다.

class CPublicContentsDlg : public CDialog
{
	DECLARE_DYNAMIC(CPublicContentsDlg)

public:
	CPublicContentsDlg(LPCTSTR szSiteId, LPCTSTR szProgramId, LPCTSTR szCustomer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPublicContentsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	//enum { IDD = IDD_PUBLIC_CONTENTS_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CString				m_strSiteId;
	CString				m_strProgramId;
	CString				m_strCustomer;

	bool				m_bSingleSelect;
	CReposControl2		m_repos;
	int					m_nMovingSplitType;
	CRect				m_rectSizeMin;
	bool				m_bInitialize;

	CHttpRequest		m_HttpRequest;

	CONTENTS_INFO_LIST		m_listAllPublicContents;	// 모든 컨텐츠 리스트
	CONTENTS_INFO_LIST		m_listSelectedContents;		// 선택 컨텐츠 리스트

	void				InitControl();
	void				InitData();
	void				ClearData();
	void				CheckScrollBarVisible(CUTBListCtrlEx& lc);
	void				GetFilterString(CString& strFilter);
	void				GetSelectItemsStringFromList(CUTBListCtrlEx& lc, LPCTSTR lpszField , CString& filter);
	void				GetSelectItemsStringFromList(CStringArray& ar, LPCTSTR lpszField , CString& filter);

	void				GetContents(LPCTSTR szSite, CONTENTS_INFO_LIST& lsContents, LPCTSTR szWhere = NULL);

public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnNMClickFilterList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClickFilterList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickContentsList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClickContentsList(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg LRESULT OnMoveSpliter(WPARAM wParam, LPARAM lParam);

	CHoverButton		m_btnRefresh;
	CEdit				m_editContentsName;
	CEdit				m_editCategory;
	CEdit				m_editRegister;
	CEdit				m_editRequester;
	CEdit				m_editFilename;
	CDateTimeCtrl		m_dtcStart;
	CDateTimeCtrl		m_dtcEnd;
	CUTBListCtrlEx		m_lcContentsType;
	CUTBListCtrlEx		m_lcVerify;
	CUTBListCtrlEx		m_lcContentsCategory;
	CUTBListCtrlEx		m_lcPurpose;
	CUTBListCtrlEx		m_lcModel;
	CUTBListCtrlEx		m_lcDirection;
	CUTBListCtrlEx		m_lcResolution;

	CStatic				m_stcGroup;
	CSplitStatic		m_stcSplit;
	CUTBListCtrlEx		m_lcContents;
	CHoverButton		m_btnSelect;
	CHoverButton		m_btnCancel;

	void				SetSingleSelection(bool bSingleSelect) { m_bSingleSelect=bSingleSelect; };
	CONTENTS_INFO_LIST&	GetSelectedContentsIdList() { return m_listSelectedContents; };
};
