// PublicContentsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
//#include "CopModule.h"
#include "PublicContentsDlg.h"

#include "common\UbcCode.h"
#include "Enviroment.h"

#include "common\libCommon\utvmacro.h"

#ifdef _ML_KOR_
static const char* CONTENTS_TYPE_TO_STRING[] = {
	"동영상", "티커", "이미지", "Promotion", "TV", "티커", "Phone", "웹", "플래시", "WebCam", "RSS",
	"Clock", "텍스트", "Flash", "Flash", "Typing", "파워포인트", "Etc", "마법사", "부속파일"
};
#else
static const char* CONTENTS_TYPE_TO_STRING[] = {
	"Video", "Ticker", "Image", "Promotion", "TV", "Ticker", "Phone", "Web", "Flash", "WebCam", "RSS",
	"Clock", "Text", "Flash", "Flash", "Typing", "PPT", "Etc", "Wizard", "File Attachments"
};
#endif

// CPublicContentsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPublicContentsDlg, CDialog)

CPublicContentsDlg::CPublicContentsDlg(LPCTSTR szSiteId, LPCTSTR szProgramId, LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(IDD_PUBLIC_CONTENTS_DLG, pParent)
	, m_strSiteId ( szSiteId )
	, m_strProgramId ( szProgramId )
	, m_strCustomer ( szCustomer )
	, m_bSingleSelect ( false )
	, m_repos (this)
	, m_nMovingSplitType ( 0 )
	, m_bInitialize ( false )
{
	char szBuf[1000] = {0};
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

	::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", szBuf, sizeof(szBuf), szPath);

	m_HttpRequest.Init(CHttpRequest::CONNECTION_SERVER, szBuf, 8080);
}

CPublicContentsDlg::~CPublicContentsDlg()
{
	int count = m_listAllPublicContents.GetCount();
	for(int i=0; i<count; i++)
	{
		CONTENTS_INFO* info = (CONTENTS_INFO*)m_listAllPublicContents.GetAt(i);
		if(info) delete info;
	}
	m_listAllPublicContents.RemoveAll();
}

void CPublicContentsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_stcGroup);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_btnRefresh);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CATEGORY, m_editCategory);
	DDX_Control(pDX, IDC_EDIT_REGISTER, m_editRegister);
	DDX_Control(pDX, IDC_EDIT_REQUESTER, m_editRequester);
	DDX_Control(pDX, IDC_EDIT_FILENAME, m_editFilename);
	DDX_Control(pDX, IDC_DATE_START, m_dtcStart);
	DDX_Control(pDX, IDC_DATE_END, m_dtcEnd);
	DDX_Control(pDX, IDC_LIST_CONTENTS_TYPE, m_lcContentsType);
	DDX_Control(pDX, IDC_LIST_VERIFY, m_lcVerify);
	DDX_Control(pDX, IDC_LIST_CONTENTS_CATEGORY, m_lcContentsCategory);
	DDX_Control(pDX, IDC_LIST_PURPOSE, m_lcPurpose);
	DDX_Control(pDX, IDC_LIST_MODEL, m_lcModel);
	DDX_Control(pDX, IDC_LIST_DIRECTION, m_lcDirection);
	DDX_Control(pDX, IDC_LIST_RESOLUTION, m_lcResolution);
	DDX_Control(pDX, IDC_STATIC_SPLIT, m_stcSplit);
	DDX_Control(pDX, IDC_LIST_CONTENTS, m_lcContents);
	DDX_Control(pDX, IDOK, m_btnSelect);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CPublicContentsDlg, CDialog)
	ON_WM_GETMINMAXINFO()
	ON_WM_SETCURSOR()
	ON_WM_SIZE()
	ON_NOTIFY(NM_CLICK, IDC_LIST_CONTENTS_TYPE, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONTENTS_TYPE, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CONTENTS_TYPE, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_CONTENTS_TYPE, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_VERIFY, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_VERIFY, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_VERIFY, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_VERIFY, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_CONTENTS_CATEGORY, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONTENTS_CATEGORY, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CONTENTS_CATEGORY, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_CONTENTS_CATEGORY, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_PURPOSE, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PURPOSE, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_PURPOSE, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_PURPOSE, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_MODEL, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_MODEL, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_MODEL, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_MODEL, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_DIRECTION, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_DIRECTION, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_DIRECTION, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_DIRECTION, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_RESOLUTION, OnNMClickFilterList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RESOLUTION, OnNMClickFilterList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_RESOLUTION, OnNMRClickFilterList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_RESOLUTION, OnNMRClickFilterList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_CONTENTS, OnNMClickContentsList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONTENTS, OnNMClickContentsList)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CONTENTS, OnNMRClickContentsList)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_CONTENTS, OnNMRClickContentsList)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnBnClickedButtonRefresh)
	ON_MESSAGE(WM_MOVE_SPLITER, OnMoveSpliter)
END_MESSAGE_MAP()


// CPublicContentsDlg 메시지 처리기입니다.


#define		FILTER_HEIGHT				150
#define		CONTENTS_LIST_HEIGHT		65


BOOL CPublicContentsDlg::OnInitDialog()
{
	BeginWaitCursor();
	
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	GetWindowRect(m_rectSizeMin);

	//
	InitControl();

	//
	InitData();

	EndWaitCursor();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPublicContentsDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	// 스케줄로부터 가져오기

	m_listSelectedContents.RemoveAll();

	int count = m_lcContents.GetItemCount();
	for(int i=0; i<count; i++)
	{
		CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(i);
		if(m_lcContents.GetCheck(i))
		{
			if(info)
			{
				info->strSiteId			= m_strSiteId;
				info->strProgramId		= m_strProgramId;
				info->bServerFileExist	= true;

				m_listSelectedContents.Add(info);

				int count2 = m_listAllPublicContents.GetCount();
				for(int j=0; j<count2; j++)
				{
					CONTENTS_INFO* info2 = (CONTENTS_INFO*)m_listAllPublicContents.GetAt(j);

					CString location = info2->strServerLocation.MakeUpper();
					if(location.Find(info->strId) >= 0 && info2->strId != info->strId )
					{
						m_listSelectedContents.Add(info2);
					}
				}
			}
		}
	}

	CDialog::OnOK();
}

void CPublicContentsDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	ClearData();

	CDialog::OnCancel();
}

void CPublicContentsDlg::InitControl()
{
	m_repos.AddControl((CWnd*)&m_stcGroup, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_btnRefresh, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editContentsName, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editCategory, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editRegister, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editRequester, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_editFilename, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_dtcStart, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_dtcEnd, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcContentsType, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcVerify, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcContentsCategory, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcPurpose, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcModel, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcDirection, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcResolution, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_stcSplit, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_lcContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnSelect, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_repos.AddControl((CWnd*)&m_btnCancel, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	m_btnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));

	CTime start_time(2011, 4, 1, 0, 0, 0);
	m_dtcStart.SetTime(&start_time);

	CRect rect;
	m_lcContentsType.GetClientRect(rect);
	m_lcContentsType.SetExtendedStyle(m_lcContentsType.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcContentsType.SetSelTextColor(RGB(0,0,0));
	m_lcContentsType.SetSelBackColor(RGB(220,220,220));
	m_lcContentsType.InsertColumn(0, LoadStringById(IDS_EEPACKAGEDLG_LST018), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcContentsType.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcVerify.GetClientRect(rect);
	m_lcVerify.SetExtendedStyle(m_lcVerify.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcVerify.SetSelTextColor(RGB(0,0,0));
	m_lcVerify.SetSelBackColor(RGB(220,220,220));
	m_lcVerify.InsertColumn(0, LoadStringById(IDS_EEPACKAGEDLG_LST015), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcVerify.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcContentsCategory.GetClientRect(rect);
	m_lcContentsCategory.SetExtendedStyle(m_lcContentsCategory.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcContentsCategory.SetSelTextColor(RGB(0,0,0));
	m_lcContentsCategory.SetSelBackColor(RGB(220,220,220));
	m_lcContentsCategory.InsertColumn(0, LoadStringById(IDS_EEPACKAGEDLG_LST009), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcContentsCategory.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcPurpose.GetClientRect(rect);
	m_lcPurpose.SetExtendedStyle(m_lcPurpose.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcPurpose.SetSelTextColor(RGB(0,0,0));
	m_lcPurpose.SetSelBackColor(RGB(220,220,220));
	m_lcPurpose.InsertColumn(0, LoadStringById(IDS_EEPACKAGEDLG_LST010), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcPurpose.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcModel.GetClientRect(rect);
	m_lcModel.SetExtendedStyle(m_lcModel.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcModel.SetSelTextColor(RGB(0,0,0));
	m_lcModel.SetSelBackColor(RGB(220,220,220));
	m_lcModel.InsertColumn(0, LoadStringById(IDS_EEPACKAGEDLG_LST011), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcModel.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcDirection.GetClientRect(rect);
	m_lcDirection.SetExtendedStyle(m_lcDirection.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcDirection.SetSelTextColor(RGB(0,0,0));
	m_lcDirection.SetSelBackColor(RGB(220,220,220));
	m_lcDirection.InsertColumn(0, LoadStringById(IDS_EEPACKAGEDLG_LST012), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcDirection.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcResolution.GetClientRect(rect);
	m_lcResolution.SetExtendedStyle(m_lcResolution.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcResolution.SetSelTextColor(RGB(0,0,0));
	m_lcResolution.SetSelBackColor(RGB(220,220,220));
	m_lcResolution.InsertColumn(0, LoadStringById(IDS_EEPACKAGEDLG_LST013), LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
	m_lcResolution.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcContents.GetClientRect(rect);
	m_lcContents.SetExtendedStyle(m_lcContents.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);
	m_lcContents.SetSelTextColor(RGB(0,0,0));
	m_lcContents.SetSelBackColor(RGB(220,220,220));
	m_lcContents.InsertColumn( 0, "", LVCFMT_LEFT, 22);
	m_lcContents.InsertColumn( 1, LoadStringById(IDS_PUBLICCONTENTSDLG_LST001), LVCFMT_LEFT, 200);
	m_lcContents.InsertColumn( 2, LoadStringById(IDS_PUBLICCONTENTSDLG_LST002), LVCFMT_LEFT, 100);
	m_lcContents.InsertColumn( 3, LoadStringById(IDS_PUBLICCONTENTSDLG_LST003), LVCFMT_LEFT, 150);
	m_lcContents.InsertColumn( 4, LoadStringById(IDS_PUBLICCONTENTSDLG_LST004), LVCFMT_LEFT, 125);
	m_lcContents.InsertColumn( 5, LoadStringById(IDS_PUBLICCONTENTSDLG_LST005), LVCFMT_LEFT, 150);
	m_lcContents.InsertColumn( 6, LoadStringById(IDS_PUBLICCONTENTSDLG_LST006), LVCFMT_LEFT, 60);
	m_lcContents.InsertColumn( 7, LoadStringById(IDS_PUBLICCONTENTSDLG_LST007), LVCFMT_LEFT, 150);
	m_lcContents.InsertColumn( 8, LoadStringById(IDS_PUBLICCONTENTSDLG_LST008), LVCFMT_LEFT, 150);
	m_lcContents.InsertColumn( 9, LoadStringById(IDS_PUBLICCONTENTSDLG_LST009), LVCFMT_LEFT, 60);
	m_lcContents.InsertColumn(10, LoadStringById(IDS_PUBLICCONTENTSDLG_LST010), LVCFMT_LEFT, 150);
	m_lcContents.InsertColumn(11, LoadStringById(IDS_PUBLICCONTENTSDLG_LST011), LVCFMT_LEFT, 150);
	m_lcContents.InsertColumn(12, LoadStringById(IDS_PUBLICCONTENTSDLG_LST012), LVCFMT_LEFT, 150);
	m_lcContents.InsertColumn(13, LoadStringById(IDS_PUBLICCONTENTSDLG_LST013), LVCFMT_LEFT, 150);
	m_lcContents.InsertColumn(14, LoadStringById(IDS_PUBLICCONTENTSDLG_LST014), LVCFMT_LEFT, 150);
	m_lcContents.InsertColumn(15, LoadStringById(IDS_PUBLICCONTENTSDLG_LST015), LVCFMT_LEFT, 150);
	m_lcContents.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_btnSelect.LoadBitmap(IDB_BTN_IMPORT2, RGB(255,255,255));
	m_btnCancel.LoadBitmap(IDB_BTN_CLOSE, RGB(255,255,255));
}

void CPublicContentsDlg::InitData()
{
	//
	m_lcContentsType.InsertItem(0, CONTENTS_TYPE_TO_STRING[CON_VIDEO] );	// CON_VIDEO					0
	m_lcContentsType.SetItemData(0, CON_VIDEO);
	m_lcContentsType.InsertItem(1, CONTENTS_TYPE_TO_STRING[CON_IMAGE] );	// CON_IMAGE					2
	m_lcContentsType.SetItemData(1, CON_IMAGE);
	m_lcContentsType.InsertItem(2, CONTENTS_TYPE_TO_STRING[CON_SMS] );	// CON_TICKER					5
	m_lcContentsType.SetItemData(2, CON_SMS);
	m_lcContentsType.InsertItem(3, CONTENTS_TYPE_TO_STRING[CON_FLASH] );	// CON_FLASH					8
	m_lcContentsType.SetItemData(3, CON_FLASH);
	m_lcContentsType.InsertItem(4, CONTENTS_TYPE_TO_STRING[CON_TEXT] );		// CON_TEXT						12
	m_lcContentsType.SetItemData(4, CON_TEXT);
	m_lcContentsType.InsertItem(5, CONTENTS_TYPE_TO_STRING[CON_URL] );		// CON_URL						7
	m_lcContentsType.SetItemData(5, CON_URL);
	m_lcContentsType.InsertItem(6, CONTENTS_TYPE_TO_STRING[CON_PPT] );		// CON_PPT						16
	m_lcContentsType.SetItemData(6, CON_PPT);
	m_lcContentsType.InsertItem(7, CONTENTS_TYPE_TO_STRING[CON_TV] );		// CON_TV						4
	m_lcContentsType.SetItemData(7, CON_TV);
	m_lcContentsType.InsertItem(8, CONTENTS_TYPE_TO_STRING[CON_RSS] );		// CON_RSS						10
	m_lcContentsType.SetItemData(8, CON_RSS);
	m_lcContentsType.InsertItem(9, CONTENTS_TYPE_TO_STRING[CON_WIZARD] );	// CON_WIZARD					18
	m_lcContentsType.SetItemData(9, CON_WIZARD);

	//CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcVerify, _T(""));
	m_lcVerify.InsertItem(0, LoadStringById(IDS_EEPACKAGEDLG_STR003));
	m_lcVerify.InsertItem(1, LoadStringById(IDS_EEPACKAGEDLG_STR004));

	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcContentsCategory, _T("Kind"));
	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcPurpose, _T("Purpose"));
	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcModel, _T("HostType"));
	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcDirection, _T("Direction"));
	CUbcCode::GetInstance(m_strCustomer)->FillListCtrl(m_lcResolution, _T("Resolution"));

	CheckScrollBarVisible(m_lcContentsType);
	CheckScrollBarVisible(m_lcVerify);
	CheckScrollBarVisible(m_lcContentsCategory);
	CheckScrollBarVisible(m_lcPurpose);
	CheckScrollBarVisible(m_lcModel);
	CheckScrollBarVisible(m_lcDirection);
	CheckScrollBarVisible(m_lcResolution);
}

void CPublicContentsDlg::ClearData()
{
	int count = m_listAllPublicContents.GetCount();
	for(int i=0; i<count; i++)
	{
		CONTENTS_INFO* info = (CONTENTS_INFO*)m_listAllPublicContents.GetAt(i);
		if(info) delete info;
	}
	m_listAllPublicContents.RemoveAll();
	m_lcContents.DeleteAllItems();
}

void CPublicContentsDlg::CheckScrollBarVisible(CUTBListCtrlEx& lc)
{
	lc.SetColumnWidth(0, LVSCW_AUTOSIZE);
	int auto_width = lc.GetColumnWidth(0);

	CRect rect;
	lc.GetClientRect(rect);

	CScrollBar* pBar =  lc.GetScrollBarCtrl(SB_VERT);
	int width = (pBar && pBar->IsWindowVisible()) ? rect.Width()-::GetSystemMetrics(SM_CXVSCROLL) : rect.Width();

	if(width < auto_width) width = auto_width;

	lc.SetColumnWidth(0, width);
}

void CPublicContentsDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnGetMinMaxInfo(lpMMI);

	if(m_bInitialize)
	{
		lpMMI->ptMinTrackSize.x = m_rectSizeMin.Width();
		lpMMI->ptMinTrackSize.y = m_rectSizeMin.Height();
	}
	else
	{
		m_bInitialize = true;
	}
}

BOOL CPublicContentsDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(pWnd->GetSafeHwnd() == m_stcSplit.GetSafeHwnd())
	{
		m_nMovingSplitType = 1;
		SetCursor(::LoadCursor(NULL, IDC_SIZENS));
		return TRUE;
	}
	else
	{
		m_nMovingSplitType = 0;
		SetCursor(::LoadCursor(NULL, IDC_ARROW));
	}

	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}

void CPublicContentsDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if(GetSafeHwnd())
	{
		m_repos.MoveControl(TRUE);
		Invalidate(TRUE);
	}

	if(m_lcContents.GetSafeHwnd())
	{
		int move = 0;
		CRect rect;
		m_lcContents.GetWindowRect(rect);
		if(rect.Height() < CONTENTS_LIST_HEIGHT)
			move = rect.Height() - CONTENTS_LIST_HEIGHT;

		if(move != 0)
		{
			m_repos.Move((CWnd*)&m_stcGroup, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcContentsType, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcVerify, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcContentsCategory, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcPurpose, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcModel, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcDirection, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_lcResolution, 0, 0, 0, move);
			m_repos.Move((CWnd*)&m_stcSplit, 0, move, 0, move);
			m_repos.Move((CWnd*)&m_lcContents, 0, move, 0, 0);

			m_repos.MoveControl(TRUE);

			CheckScrollBarVisible(m_lcContentsType);
			CheckScrollBarVisible(m_lcVerify);
			CheckScrollBarVisible(m_lcContentsCategory);
			CheckScrollBarVisible(m_lcPurpose);
			CheckScrollBarVisible(m_lcModel);
			CheckScrollBarVisible(m_lcDirection);
			CheckScrollBarVisible(m_lcResolution);
		}
	}
}

BOOL GetContentsObjectList(CONTENTS_INFO_LIST& infoList, CStringArray& str_list)
{
	CONTENTS_INFO* info = NULL;

	for(int i=0; i<str_list.GetCount(); i++)
	{
		const CString& line = str_list.GetAt(i);

		int equal_idx = line.Find(_T("="));

		if(equal_idx > 0 && line.GetLength() >= equal_idx+1 )
		{
			int pos = 0;
			CString key = line.Tokenize(_T("="), pos);
			LPCTSTR szValue = (LPCTSTR)line+equal_idx+1;

			if(key == "contentsId")
			{
				info = new CONTENTS_INFO;
				info->strId = szValue;
				infoList.Add(info);
			}
			else if(key == "mgrId"				&& info)	info->strMgrId = szValue;
			else if(key == "siteId"				&& info)	info->strSiteId = szValue;
			else if(key == "programId"			&& info)	info->strProgramId = szValue;
			else if(key == "category"			&& info)	info->strCategory = szValue;
			else if(key == "contentsName"		&& info)	info->strContentsName = szValue;
			else if(key == "contentsType"		&& info)	info->nContentsType = _ttoi(szValue);
			else if(key == "parentsId"			&& info)	info->strParentId = szValue;
			else if(key == "registerId"			&& info)	info->strRegisterId = szValue;
			else if(key == "registerTime"		&& info)	info->strRegisterTime = szValue;
			else if(key == "location"			&& info)	info->strServerLocation = szValue;
			else if(key == "filename"			&& info)	info->strFilename = szValue;
			else if(key == "runningTime"		&& info)	info->nRunningTime = _ttoi64(szValue);
			else if(key == "verifier"			&& info)	info->strVerifier = szValue;
			else if(key == "description"		&& info)	info->strDescription = szValue;
			else if(key == "comment1"			&& info)	info->strComment[0] = szValue;
			else if(key == "comment2"			&& info)	info->strComment[1] = szValue;
			else if(key == "comment3"			&& info)	info->strComment[2] = szValue;
			else if(key == "comment4"			&& info)	info->strComment[3] = szValue;
			else if(key == "comment5"			&& info)	info->strComment[4] = szValue;
			else if(key == "comment6"			&& info)	info->strComment[5] = szValue;
			else if(key == "comment7"			&& info)	info->strComment[6] = szValue;
			else if(key == "comment8"			&& info)	info->strComment[7] = szValue;
			else if(key == "comment9"			&& info)	info->strComment[8] = szValue;
			else if(key == "comment10"			&& info)	info->strComment[9] = szValue;
			else if(key == "bgColor"			&& info)	info->strBgColor = szValue;
			else if(key == "fgColor"			&& info)	info->strFgColor = szValue;
			else if(key == "font"				&& info)	info->strFont = szValue;
			else if(key == "fontSize"			&& info)	info->nFontSize = _ttoi(szValue);
			else if(key == "playSpeed"			&& info)	info->nPlaySpeed = _ttoi(szValue);
			else if(key == "soundVolume"		&& info)	info->nSoundVolume = _ttoi(szValue);
			else if(key == "isPublic"			&& info)	info->bIsPublic = szValue;
			else if(key == "align"				&& info)	info->nAlign = _ttoi(szValue);
			else if(key == "width"				&& info)	info->nWidth = _ttoi(szValue);
			else if(key == "height"				&& info)	info->nHeight = _ttoi(szValue);
			else if(key == "currentComment"		&& info)	info->nCurrentComment = _ttoi(szValue);
			else if(key == "volume"				&& info)	info->nFilesize = _ttoi64(szValue);
			else if(key == "contentsState"		&& info)	info->nContentsState = _ttoi(szValue);
			else if(key == "contentsCategory"	&& info)	info->nContentsCategory = _ttoi(szValue);
			else if(key == "purpose"			&& info)	info->nPurpose = _ttoi(szValue);
			else if(key == "hostType"			&& info)	info->nHostType = _ttoi(szValue);
			else if(key == "vertical"			&& info)	info->nVertical = _ttoi(szValue);
			else if(key == "resolution"			&& info)	info->nResolution = _ttoi(szValue);
			else if(key == "validationDate"		&& info)	info->strValidationDate = szValue;
			else if(key == "requester"			&& info)	info->strRequester = szValue;
			else if(key == "verifyMembers"		&& info)	info->strVerifyMembers = szValue;
			else if(key == "verifyTime"			&& info)	info->strVerifyTime = szValue;
			else if(key == "registerType"		&& info)	info->strRegisterType = szValue;
			else if(key == "wizardXML"			&& info)	info->strWizardXML = szValue;
			else if(key == "wizardFiles"		&& info)	info->strWizardFiles = szValue;
		}
	}

	for(int i=0; i<infoList.GetCount(); i++)
	{
		info = infoList.GetAt(i);

		if(info->strFilename.IsEmpty())
			info->bLocalFileExist = true;
		else
		{
			CString fullpath;
			fullpath.Format(_T("%s\\%s%s"), GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, info->strFilename);

			CFileStatus fs;
			//if(CFile::GetStatus(fullpath, fs) && fs.m_size == info->nFilesize)
			if(CEnviroment::GetFileSize(fullpath, fs.m_size) && fs.m_size == info->nFilesize)
			{
				info->bLocalFileExist = true;
			}
		}
	}

	return TRUE;
}

void CPublicContentsDlg::OnBnClickedButtonRefresh()
{
	BeginWaitCursor();

	ClearData();

	//
	CString filter;
	GetFilterString(filter);

	//
	CString out_msg;
	if( m_HttpRequest.RequestPost("UBC_PublicContents/get_public_list.asp", filter, out_msg) )
	{
		CStringArray token_list;
		int pos = 0;
		CString token = out_msg.Tokenize(_T("\r\n"), pos);

		while(token != _T(""))
		{
			token_list.Add(token);
			token = out_msg.Tokenize(_T("\r\n"), pos);
		}

		if(token_list.GetCount() > 0)
		{
			GetContentsObjectList(m_listAllPublicContents, token_list);
		}
	}

	int idx = 0;
	int count = m_listAllPublicContents.GetCount();
	for(int i=0; i<count; i++)
	{
		CONTENTS_INFO* info = m_listAllPublicContents.GetAt(i);

		if(info->nContentsType == 19) continue;

		m_lcContents.InsertItem(idx, "");
		m_lcContents.SetItemText(idx, 1, info->strContentsName);
		m_lcContents.SetItemText(idx, 2, CONTENTS_TYPE_TO_STRING[info->nContentsType] );
		m_lcContents.SetItemText(idx, 3, info->strRegisterId);
		m_lcContents.SetItemText(idx, 4, info->strRegisterTime);
		m_lcContents.SetItemText(idx, 5, info->strFilename);

		CString str;
		str.Format("%02d:%02d", info->nRunningTime / 60, info->nRunningTime % 60);
		m_lcContents.SetItemText(idx, 6, str);

		m_lcContents.SetItemText(idx, 7, info->strVerifier);
		m_lcContents.SetItemText(idx, 8, info->strRequester);

		m_lcContents.SetItemText(idx, 9, info->nContentsState & CON_VERIFIED ? "O" : "X");

		SCodeItem item;

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("Kind", info->nContentsCategory);
		m_lcContents.SetItemText(idx, 10, item.strEnumString);

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("Purpose", info->nPurpose);
		m_lcContents.SetItemText(idx, 11, item.strEnumString);

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("HostType", info->nHostType);
		m_lcContents.SetItemText(idx, 12, item.strEnumString);

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("Direction", info->nVertical);
		m_lcContents.SetItemText(idx, 13, item.strEnumString);

		item = CUbcCode::GetInstance(m_strCustomer)->GetCodeInfo("Resolution", info->nResolution);
		m_lcContents.SetItemText(idx, 14, item.strEnumString);

		m_lcContents.SetItemText(idx, 15, info->strCategory);

		m_lcContents.SetItemData(idx, (DWORD)info);

		idx++;
	}

	EndWaitCursor();
}

void CPublicContentsDlg::OnNMClickFilterList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;

	int index = pNMItemActivate->iItem;
	if(index < 0) return;

	CWnd* wnd = CWnd::FromHandle(pNMHDR->hwndFrom);
	if(wnd)
	{
		CUTBListCtrlEx* plc = (CUTBListCtrlEx*)wnd;

		//LVHITTESTINFO oInfo ;
		//oInfo.pt = pNMItemActivate->ptAction ;
		//plc->HitTest(&oInfo);

		// Check Box Icon Click
		//if( oInfo.flags == LVHT_ONITEMSTATEICON ) 
		{
			bool all_check = true;
			for(int i=0; i<plc->GetItemCount(); i++)
			{
				//if((i==index && plc->GetCheck(i) != 0) || (i!=index && plc->GetCheck(i) == 0))
				if(plc->GetCheck(i) == 0)
				{
					all_check = false;
					break;
				}
			}
			plc->SetCheckHdrState(all_check ? TRUE : FALSE);

			//*pResult = 1; // 무시하고 싶으면 셋팅
		}
	}
}

void CPublicContentsDlg::OnNMRClickFilterList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 1;
}

void CPublicContentsDlg::OnNMClickContentsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;

	int index = pNMItemActivate->iItem;
	if(index < 0) return;

	if(m_bSingleSelect)
	{
		CWnd* wnd = CWnd::FromHandle(pNMHDR->hwndFrom);
		if(wnd)
		{
			CUTBListCtrlEx* plc = (CUTBListCtrlEx*)wnd;

			LVHITTESTINFO oInfo ;
			oInfo.pt = pNMItemActivate->ptAction ;
			plc->HitTest(&oInfo);

			// Check Box Icon Click
			if( oInfo.flags == LVHT_ONITEMSTATEICON ) 
			{

				int count = m_lcContents.GetItemCount();
				for(int i=0; i<count; i++)
				{
					if(index == i)
					{
						m_lcContents.SetItemState(i, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
					}
					else
					{
						m_lcContents.SetCheck(i, FALSE);
						m_lcContents.SetItemState(i, NULL, LVIS_SELECTED | LVIS_FOCUSED);
					}
				}
			}
		}
	}
}

void CPublicContentsDlg::OnNMRClickContentsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 1;
}

LRESULT CPublicContentsDlg::OnMoveSpliter(WPARAM wParam, LPARAM lParam)
{ 
	CString msg;

	CRect rect1, rect2;
	m_stcGroup.GetWindowRect(rect1);
	m_lcContents.GetWindowRect(rect2);

	if(rect1.Height() + lParam < FILTER_HEIGHT)
		lParam = FILTER_HEIGHT - rect1.Height();
	else if(rect2.Height() - lParam < CONTENTS_LIST_HEIGHT)
		lParam = rect2.Height() - CONTENTS_LIST_HEIGHT;

	if(lParam != 0)
	{
		m_repos.Move((CWnd*)&m_stcGroup, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcContentsType, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcVerify, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcContentsCategory, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcPurpose, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcModel, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcDirection, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_lcResolution, 0, 0, 0, lParam);
		m_repos.Move((CWnd*)&m_stcSplit, 0, lParam, 0, lParam);
		m_repos.Move((CWnd*)&m_lcContents, 0, lParam, 0, 0);
		m_repos.Move((CWnd*)&m_btnSelect, 0, lParam, 0, lParam);

		CheckScrollBarVisible(m_lcContentsType);
		CheckScrollBarVisible(m_lcVerify);
		CheckScrollBarVisible(m_lcContentsCategory);
		CheckScrollBarVisible(m_lcPurpose);
		CheckScrollBarVisible(m_lcModel);
		CheckScrollBarVisible(m_lcDirection);
		CheckScrollBarVisible(m_lcResolution);

		m_repos.MoveControl(TRUE);
	}

	return 0;
}

void CPublicContentsDlg::GetFilterString(CString& strFilter)
{
	CStringArray filter_list;
	CString filter;

//	filter_list.Add("contentsType <> '19'"); // 부속파일은 안보이게						//-----------------------------> asp로 이동
//	filter_list.Add("parentsId = '' or parentsId is null"); // 부속파일은 안보이게		//-----------------------------> asp로 이동

	//
	CString contentsId;
	m_editContentsName.GetWindowText(contentsId);
	if(contentsId.GetLength() > 0)
	{
		//filter.Format("contentsName like ('%%%%%s%%%%')", contentsId);		//-----------------------------> asp로 이동
		filter.Format("contentsName=%s", contentsId);
		filter_list.Add(filter);
	}

	CString category;
	m_editCategory.GetWindowText(category);
	if(category.GetLength() > 0)
	{
		//filter.Format("category like ('%%%%%s%%%%')", category);		//-----------------------------> asp로 이동
		filter.Format("category=%s", category);
		filter_list.Add(filter);
	}

	CString register_;
	m_editRegister.GetWindowText(register_);
	if(register_.GetLength() > 0)
	{
		//filter.Format("registerId like ('%%%%%s%%%%')", register_);		//-----------------------------> asp로 이동
		filter.Format("register=%s", register_);
		filter_list.Add(filter);
	}

	CString requester;
	m_editRequester.GetWindowText(requester);
	if(requester.GetLength() > 0)
	{
		//filter.Format("requester like ('%%%%%s%%%%')", requester);		//-----------------------------> asp로 이동
		filter.Format("requester=%s", requester);
		filter_list.Add(filter);
	}

	CString filename;
	m_editFilename.GetWindowText(filename);
	if(filename.GetLength() > 0)
	{
		//filter.Format("filename like ('%%%%%s%%%%')", filename);		//-----------------------------> asp로 이동
		filter.Format("filename=%s", filename);
		filter_list.Add(filter);
	}

	CTimeSpan span(1, 0, 0, 0);
	CTime start_time;
	m_dtcStart.GetTime(start_time);
	CString startTime = start_time.Format("%Y/%m/%d");
	CTime end_time;
	m_dtcEnd.GetTime(end_time);
	end_time += span;
	CString endTime = end_time.Format("%Y/%m/%d");
	{
		//filter.Format("registerTime > '%s' and registerTime < '%s'", startTime, endTime);		//-----------------------------> asp로 이동
		filter.Format("startTime=%s", startTime);
		filter_list.Add(filter);
		filter.Format("endTime=%s", endTime);
		filter_list.Add(filter);
	}

	if(!m_lcContentsType.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcContentsType, "contentsType", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	if(!m_lcVerify.GetCheckHdrState())
	{
		// not all
		if(m_lcVerify.GetCheck(0))
		{
			// 승인
			//filter_list.Add("contentsState in ('2', '3')");		//-----------------------------> asp로 이동
			filter_list.Add("contentsState='2','3'");
		}
		else if(m_lcVerify.GetCheck(1))
		{
			// 미승인
			//filter_list.Add("contentsState  in ('0', '1')");		//-----------------------------> asp로 이동
			filter_list.Add("contentsState='0','1'");
		}
	}

	if(!m_lcContentsCategory.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcContentsCategory, "contentsCategory", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	if(!m_lcPurpose.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcPurpose, "purpose", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	if(!m_lcModel.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcModel, "hostType", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	if(!m_lcDirection.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcDirection, "vertical", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	if(!m_lcResolution.GetCheckHdrState())
	{
		// not all
		GetSelectItemsStringFromList(m_lcResolution, "resolution", filter);
		if(filter.GetLength() > 0) filter_list.Add(filter);
	}

	//
	int count = filter_list.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str = filter_list.GetAt(i);

		if(strFilter.GetLength() > 0)
		{
			strFilter += "&";
		}
		strFilter += str;
	}
	//strFilter += " group by contentsName";		//-----------------------------> asp로 이동
}

void CPublicContentsDlg::GetSelectItemsStringFromList(CUTBListCtrlEx& lc, LPCTSTR lpszField , CString& filter)
{
	filter = "";

	int count = lc.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if(lc.GetCheck(i))
		{
			if(filter.GetLength() > 0) filter += ",";

			CString tmp;
			tmp.Format("'%d'", lc.GetItemData(i));

			filter += tmp;
		}
	}
	if(filter.GetLength() > 0)
	{
		CString tmp;
		tmp.Format("%s=", lpszField);
		filter.Insert(0, tmp);
	}
}

void CPublicContentsDlg::GetSelectItemsStringFromList(CStringArray& ar, LPCTSTR lpszField , CString& filter)
{
	filter = "";

	int count = ar.GetCount();
	for(int i=0; i<count; i++)
	{
		if(filter.GetLength() > 0) filter += ",";

		CString tmp;
		tmp.Format("'%s'", ar.GetAt(i));

		filter += tmp;
	}
	if(filter.GetLength() > 0)
	{
		CString tmp;
		tmp.Format("%s=", lpszField);
		filter.Insert(0, tmp);
	}
}
