// stdafx.cpp : source file that includes just the standard includes
// UBCStudio.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "Tlhelp32.h"
#include "io.h"
#include "fcntl.h"
#include "common/libCommon/SecurityIni.h"

CString ToString(int nValue)
{
	CString str;
	str.Format(_T("%d"), nValue);

	return str;
}

CString ToString(double fValue, int nDegree)
{
	CString strFormat;
	strFormat.Format(_T("%%.%df"), nDegree);

	CString szBuf;
	szBuf.Format(strFormat, fValue);
	return szBuf;
}

CString ToString(ULONG ulValue)
{
	CString str;
	str.Format(_T("%ld"), ulValue);

	return str;
}

CString ToString(ULONGLONG ulValue)
{
	CString str;
	str.Format(_T("%I64d"), ulValue);

	return str;
}

CString ToMoneyTypeString(LPCTSTR lpszStr)
{
	CString str = lpszStr;

	int nLength = str.Find(_T("."));
	if(nLength<0)
        nLength = str.GetLength();

	while( nLength > 3 )
	{
		nLength = nLength - 3;
		str.Insert( nLength, ',' );
	}

	return str;
}

CString ToMoneyTypeString(int value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%d"), value);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(ULONG ulvalue)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%ld"), ulvalue);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(ULONGLONG ulvalue)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%I64d"), ulvalue);
	return ToMoneyTypeString(buf);
}

CString ToFileSize(ULONGLONG lVal, char chUnit, bool bIncUnit /*= true*/)
{
	CString strVal;
	CString strUnit;

	if(chUnit == 'A' || chUnit == 'a')
	{
		if     (lVal / 1024 == 0) chUnit = 'B';
		else if(lVal / (1024*1024) == 0) chUnit = 'K';
		else if(lVal / (1024*1024*1024) == 0) chUnit = 'M';
		else chUnit = 'G';
	}

	if(chUnit == 'B' || chUnit == 'b')
	{
		strVal = ToString(lVal);
		if(bIncUnit) strUnit = _T("Byte");
	}
	else if(chUnit == 'K' || chUnit == 'k')
	{
		strVal = ToString(lVal/1024, 1);
		if(bIncUnit) strUnit = _T("KB");
	}
	else if(chUnit == 'M' || chUnit == 'm')
	{
		strVal = ToString(lVal/(1024*1024.), 1);
		if(bIncUnit) strUnit = _T("MB");
	}
	else if(chUnit == 'G' || chUnit == 'g')
	{
		strVal = ToString(lVal/(1024*1024*1024.), 1);
		if(bIncUnit) strUnit = _T("GB");
	}

	return strVal + _T(" ") + strUnit;
}

CString GetTimeZoneString(int nTimeZone)
{
	switch(nTimeZone)
	{
	case 0: return _T("All Day");
	case 7: return _T("0 ~ 7h");
	case 1: return _T("7 ~ 9h");
	case 2: return _T("9 ~ 12h");
	case 3: return _T("12 ~ 13h");
	case 4: return _T("13 ~ 18h");
	case 5: return _T("18 ~ 22h");
	case 6: return _T("22 ~ 24h");
	}

	return _T("");
}

CString GetContentsTypeString(int nContentsType)
{
	switch(nContentsType)
	{
	case 0: return _T("Video");
	case 1:	return _T("Ticker");
	case 2:	return _T("Image");
	case 3:	return _T("Promotion");
	case 4:	return _T("TV");
	case 5:	return _T("Ticker");
	case 6:	return _T("Phone");
	case 7:	return _T("WebPage");
	case 8:	return _T("Flash");
	case 9:	return _T("WebCam");
	case 10:return _T("RSS");
	case 11:return _T("Clock");
	case 12:return _T("Text");
	case 13:return _T("Flash");
	case 14:return _T("Flash");
	case 15:return _T("Typing");
	case 16:return _T("PPT");
	case 17:return _T("Etc");
	case 18:return _T("Wizard");
	}

	return _T("");
}

COLORREF GetColorFromString(LPCTSTR lpszColor)
{
	COLORREF rgb;
	if(*lpszColor == '#')
		_stscanf(lpszColor+1, _T("%x"), &rgb);
	else
		_stscanf(lpszColor, _T("%x"), &rgb);

	COLORREF red	= rgb & 0x00FF0000;
	COLORREF green	= rgb & 0x0000FF00;
	COLORREF blue	= rgb & 0x000000FF;

	red = red >> 16;
	blue = blue << 16;

	return red | green | blue;
}

CString GetColorFromString(COLORREF rgb)
{
	COLORREF red	= rgb & 0x000000FF;
	COLORREF green	= rgb & 0x0000FF00;
	COLORREF blue	= rgb & 0x00FF0000;

	green = green >> 8;
	blue = blue >> 16;

	CString ret;
	ret.Format(_T("#%02X%02X%02X"), red, green, blue);

	return ret;
}

void GetColorRGB(COLORREF rgb, int& r, int& g, int& b)
{
	r = rgb & 0x000000FF;
	g = rgb & 0x0000FF00;
	b = rgb & 0x00FF0000;

	g >>= 8;
	b >>= 16;
}

LPCTSTR GetDataPath()
{
	static CString config_path = _T("");

	if(config_path.GetLength() == 0)
	{
//		CString path = ciEnv::newEnv("PROJECT_HOME");

//		config_path.Format(_T("%s\\config\\%s.ini"), path, ::GetHostName());
	}

	return config_path;
}

CString MakeCurrency(ULONGLONG lVar)
{
	CString szDigit;
	CString szBuff = _T("");
	ULONGLONG v = lVar;
	
	int i = 0;
	do{
		if(i && !(i%3))
			szBuff = _T(",") + szBuff;
		szDigit.Format(_T("%c"), (v%10) + '0');
		szBuff = szDigit + szBuff;
		v /= 10;
		i++;
	}while(v);
	return szBuff;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일명에서 확장자를 분리하여 반환 \n
/// @param (const CString&) strFileName : (in) 파일명
/// @return <형: CString> \n
///			<파일의 확장자> \n
/////////////////////////////////////////////////////////////////////////////////
CString	FindExtension(const CString& strFileName)
{
	int nlen = strFileName.GetLength();
	int i;
	for (i = nlen-1; i >= 0; i--){
		if (strFileName[i] == '.'){
			return strFileName.Mid(i+1);
		}
	}
	return CString(_T(""));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VARIANT를 문자열로 변환한다. \n
/// @param (VARIANT*) var : (in) VARIANT 변수
/// @return <형: CString> \n
///			<VARIANT 변수의 문자열> \n
/////////////////////////////////////////////////////////////////////////////////
CString	VariantToString(VARIANT* var)
{
	CString str;
	switch(var->vt)
	{
	case VT_BSTR:
		{
			return CString(var->bstrVal);
		}
		break;
	case VT_BSTR | VT_BYREF:
		{
			return CString(*var->pbstrVal);
		}
		break;
	case VT_I4:
		{
			str.Format(_T("%d"), var->lVal);
			return str;
		}
		break;
	case VT_I4 | VT_BYREF:
		{
			str.Format(_T("%d"), *var->plVal);
			return str;
		}
		break;
	case VT_R8:
		{
			str.Format(_T("%f"), var->dblVal);
			return str;
		}
		break;
	default:
		{
			return _T("");
		}
	}//switch
}


LPCTSTR GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UBCConnect.ini 파일의 Server IP 반환 \n
/// @return <형: CString> \n
///			<접속서버의 IP 주소> \n
/////////////////////////////////////////////////////////////////////////////////
CString	GetConnectServerIP()
{
	CString strIP = _T("");
	CString strIniPath;
	strIniPath.Format(_T("%sdata\\%s"), GetAppPath(), ORBCONN_INI);

	// todo 다음에 적용할것
	//CSecurityIni securityIni;
	//securityIni.ReadValue(_T("ORB_NAMESERVICE"), _T("IP"), strIniPath);

	TCHAR cBuffer[1024*100] = { 0x00 };
	GetPrivateProfileString(_T("ORB_NAMESERVICE"), _T("IP"), _T(""), cBuffer, 1024*100, strIniPath);

	return cBuffer;
}


bool CALLBACK FPC_EnumFontProc(LPLOGFONT lplf, LPTEXTMETRIC lptm, DWORD dwType, LPARAM lpData)	
{	
	if(_tcsstr(lplf->lfFaceName, _T("@")) != NULL)
		return TRUE;

	CStringArray* paryFontName = (CStringArray*)lpData;
	paryFontName->Add(lplf->lfFaceName);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시스템에 지정된 폰트가 있는지를 반환 \n
/// @param (CString) strFontName : (in) 검사하려는 폰트 이름
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsAvailableFont(CString strFontName)
{
	HWND hWnd = GetDesktopWindow();
	CStringArray aryFontName;
	EnumFonts(::GetDC(hWnd), 0,(FONTENUMPROC) FPC_EnumFontProc,(LPARAM)&aryFontName); //Enumerate font

	for(int i=0; i<aryFontName.GetSize(); i++)
	{
		if(strFontName == aryFontName.GetAt(i))
		{
			return true;
		}//if
	}//for

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UBCBrowser.ini 파일의 경로 반환 \n
/// @return <형: LPCTSTR> \n
///			<UBCBrowser.ini 파일의 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR	GetBRWConfigPath()
{
	static CString strBRWConfig = _T("");

	if(strBRWConfig.GetLength() == 0)
	{
		strBRWConfig.Format(_T("%sdata\\UBCBrowser.ini"), GetAppPath());
	}//if

	return strBRWConfig;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용할 Vidoe render 종류를 반환 \n
/// @return <형: int> \n
///			<값: 1: Overlay, 2:VMR7,....> \n
/////////////////////////////////////////////////////////////////////////////////
int GetVidoeRenderType()
{
	int nType;
	TCHAR cBuffer[1024*100] = { 0x00 };
	CString strAppName, strIniPath;
	//strAppName = GetAppName();
	strIniPath = GetBRWConfigPath();

	GetPrivateProfileString(_T("ROOT"), _T("VIDEO_RENDER"), _T(""), cBuffer, 1024*100, GetBRWConfigPath());
	nType = _ttoi(cBuffer);

	if(nType == 0)
	{
		nType = 2;
		WritePrivateProfileString(_T("ROOT"), _T("VIDEO_RENDER"), _T("2"), GetBRWConfigPath());
	}//if

	return nType;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용할 Vidoe open하는 방법을 반환 \n
/// @return <형: int> \n
///			<값: 1: 초기화과정에 open, 2:매번 플레이시에 open> \n
/////////////////////////////////////////////////////////////////////////////////
int GetVidoeOpenType()
{
	int nType;
	TCHAR cBuffer[1024*100] = { 0x00 };
	CString strAppName, strIniPath;
	//strAppName = GetAppName();
	strIniPath = GetBRWConfigPath();

	GetPrivateProfileString(_T("ROOT"), _T("VIDEO_OPEN_TYPE"), _T(""), cBuffer, 1024*100, GetBRWConfigPath());
	nType = _ttoi(cBuffer);

	if(nType == 0)
	{
		nType = E_OPEN_INIT;
		WritePrivateProfileString(_T("ROOT"), _T("VIDEO_OPEN_TYPE"), _T("1"), GetBRWConfigPath());
	}//if

	return nType;
}

CString GetErrMessage(int nErrCode, CString strDefaultMsg /*= _T("")*/)
{
	CString strErrMsg;

	LPVOID lpMsgBuf = NULL;

	if(nErrCode != 0)
	{
		FormatMessage(  FORMAT_MESSAGE_ALLOCATE_BUFFER
					  | FORMAT_MESSAGE_IGNORE_INSERTS 
					  | FORMAT_MESSAGE_FROM_SYSTEM     
					  ,	NULL
					  , nErrCode
					  , 0
					  , (LPTSTR)&lpMsgBuf
					  , 0
					  , NULL );
	}

	if(!lpMsgBuf)
	{
		strErrMsg.Format(_T("%s [%d]"), strDefaultMsg, nErrCode);
	}
	else
	{
		strErrMsg.Format(_T("%s [%d]"), (LPCTSTR)lpMsgBuf, nErrCode);
	}

	LocalFree( lpMsgBuf );

	return strErrMsg;
}

bool IsExistFile(CString strFilePath)
{
	CFileStatus fs;
	return (CFile::GetStatus(strFilePath, fs));
}

CString LoadStringById(UINT nID)
{
	CString strValue;
	if(!strValue.LoadString(nID)) return _T("");
	return strValue;
}

void TerminateProcess(CString processName)
{
	CArray<DWORD,DWORD> processIDs;

	if(IsAliveProcess(processName, &processIDs))  //살아있으면
	{
		for(int i=0; i < processIDs.GetSize(); i++)
		{
			DWORD processID = processIDs.GetAt(i);
			KillProcess(processID);
		}
	}
}

BOOL IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs)
{
	CString newName = "";

	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);

	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(PROCESSENTRY32);

	BOOL bIsAlive = FALSE;

	while(Process32Next(hSnapShot,&pEntry))
	{	
		// 1. 프로세스명 얻기
		CString newName = pEntry.szExeFile;

		newName.MakeUpper();
		processName.MakeUpper();

		// 2. 프로세스명 비교
		if( processName.Find(newName) >= 0 )
		{
			if(processIDs != NULL)
				processIDs->Add(pEntry.th32ProcessID);
			bIsAlive = TRUE;
		}
	}

	CloseHandle(hSnapShot);

	return bIsAlive;
}

void KillProcess(DWORD dwProcessId)
{
	HANDLE hProcessGUI = ::OpenProcess(PROCESS_TERMINATE, FALSE, dwProcessId);

	if( hProcessGUI ) 
	{
		::TerminateProcess(hProcessGUI, 0);
		//AppMonitor::SafeTerminateProcess(hProcessGUI, 0);
		::CloseHandle(hProcessGUI);
	}
}

void InitDateRange(CDateTimeCtrl& dtControl)
{
	CTime tmFrom = CTime(2000,1,1,0,0,0);
	CTime tmTo = CTime(2037,12,31,23,59,59);
	dtControl.SetRange(&tmFrom, &tmTo);
}

int FindNoCase(CString strSrc, CString strSub, int nStart)
{
	strSrc.MakeLower();
	strSub.MakeLower();
	return strSrc.Find(strSub, nStart);
}

unsigned long RandomNumber(unsigned long max) 
{
	//land 함수는 전역 seed 를 공유할뿐만아니라, 회전주기가 짧고 값의 분포(확률)이 일정하지 않으므로, 사용하지 않는다.
	//단, 이 함수는 1초 내에 여러번 호출된다면, 같은 값을 리턴하므로, 이런 경우에는 사용하지 않는다.
	time_t now = time(NULL);
	unsigned long retval = unsigned long(now % time_t(max-1));
	if(retval == 0) retval = max;
	return retval;
}

void RandomSpace(unsigned long max, CString& strSpace) 
{
	// 임의의 길이의 스페이이스 문자열을 리턴한다.
	unsigned long length = RandomNumber(max);

	char* buf = new char[max+1]; 
	memset(buf,0x00,max+1);
	memset(buf,0x20,length);
	strSpace = buf;
	delete [] buf;
}

bool AppendStringToFile(CString& fullpath, CString& appendStr)
{
	int fp=::open(fullpath, O_WRONLY | O_APPEND );
	if(fp){
		write(fp,"\n",1);
		write(fp,appendStr,appendStr.GetLength());
		close(fp);
		return true;
	}
	return false;
}

bool TimeStringToFileTime(CString& timeStr,  LPFILETIME pft)
{
//	TraceLog(("TimeStringToFileTime=%s", timeStr));

	if(timeStr.IsEmpty() || timeStr.GetLength() != 19) {
		return false;
	}

	struct tm	c_tm;
	memset(&c_tm, 0x00, sizeof(struct tm));

	sscanf(timeStr.GetBuffer(), "%04d/%02d/%02d %02d:%02d:%02d",
		&c_tm.tm_year, &c_tm.tm_mon, &c_tm.tm_mday,
		&c_tm.tm_hour, &c_tm.tm_min, &c_tm.tm_sec);

	c_tm.tm_year -= 1900;
	c_tm.tm_mon -= 1;
	
	time_t now1;
	time(&now1);
	struct tm* localTime = localtime(&now1);
	c_tm.tm_isdst = localTime->tm_isdst;

	time_t a_time = mktime(&c_tm);

	if(a_time <= 0) {
		//TraceLog(("Time change error"));
		return false;
	}

	CTime debugTime(a_time);
	//TraceLog(("Set TimeStringToFileTime=%04d/%02d/%02d %02d:%02d:%02d"
	//					, debugTime.GetYear()
	//					, debugTime.GetMonth()
	//					, debugTime.GetDay()
	//					, debugTime.GetHour()
	//					, debugTime.GetMinute()
	//					, debugTime.GetSecond()));
	
	 // Note that LONGLONG is a 64-bit value
	 LONGLONG ll;
	 ll = Int32x32To64(a_time, 10000000) + 116444736000000000;
	 pft->dwLowDateTime = (DWORD)ll;
	 pft->dwHighDateTime = ll >> 32;

	 return true;
}

bool	
isNewFile(CString& target, CString& modifiedTime)
{
	//TraceLog(("isNewFile?=%s", target));
	if(modifiedTime.IsEmpty()){
		return false;
	}
	
	HANDLE hFile = ::CreateFileA(target, 
								GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE , NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile==INVALID_HANDLE_VALUE) {
		//TraceLog(("%s file open error", target));
		return false;		
	}
	FILETIME cTime, aTime, wTime, ft;
		
	if(TimeStringToFileTime(modifiedTime,&ft)){
		if(::GetFileTime(hFile, &cTime, &aTime, &wTime)){
			//TraceLog(("%s file time get file time succeed", target));
			if(aTime.dwHighDateTime < ft.dwHighDateTime || 
				(aTime.dwHighDateTime == ft.dwHighDateTime && aTime.dwLowDateTime < ft.dwLowDateTime) ) {
				//TraceLog(("Yes %s file is new (%s)", target, modifiedTime));
				CloseHandle(hFile);
				return true;			
			}
			//TraceLog(("No, %s file is not new (%s)", target, modifiedTime));
			CloseHandle(hFile);
			return false;
		}
		//TraceLog(("%s file get file time failed (ErrorCode=%d)",target, ::GetLastError()));
	}else{
		//TraceLog(("%s TimeStringToFileTime", modifiedTime));
	}
	CloseHandle(hFile);
	return false;
}

