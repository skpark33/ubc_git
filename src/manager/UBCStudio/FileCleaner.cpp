// FileCleaner.cpp : implementation file
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "FileCleaner.h"
#include "Schedule.h"
#include "common/libProfileManager/ProfileManager.h"

// CFileCleaner dialog

IMPLEMENT_DYNAMIC(CFileCleaner, CDialog)

CFileCleaner::CFileCleaner(CWnd* pParent /*=NULL*/)
	: CDialog(CFileCleaner::IDD, pParent)
{
}

CFileCleaner::~CFileCleaner()
{
}

void CFileCleaner::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_LIST_FCN_FILES, m_lcList);
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CFileCleaner, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, &CFileCleaner::OnBnClickedButtonRemove)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_FCN_FILES, &CFileCleaner::OnNMDblclkListFcnFiles)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_FCN_FILES, &CFileCleaner::OnLvnColumnclickListFcnFiles)
END_MESSAGE_MAP()


// CFileCleaner message handlers

BOOL CFileCleaner::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitContents();
	InitList();
	InsertItems(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CFileCleaner::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
	POSITION pos = m_lsFiles.GetHeadPosition();
	while(pos){
		SFCInfo* pInfo = m_lsFiles.GetNext(pos);
		if(!pInfo)	continue;
		delete pInfo;
	}
	m_lsFiles.RemoveAll();
}

void CFileCleaner::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);

	CBitmap bmpContents;
	bmpContents.LoadBitmap(IDB_FILE_ICONS);
	m_ilContents.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilContents.Add(&bmpContents, RGB(255, 255, 255));
	m_lcList.SetImageList(&m_ilContents, LVSIL_SMALL);

	m_lcList.InsertColumn(eCheck, "", LVCFMT_LEFT, 22);
	m_lcList.InsertColumn(EFC_DRV , LoadStringById(IDS_FILECLEANER_LST001), LVCFMT_CENTER, 42);
	m_lcList.InsertColumn(EFC_FILE, LoadStringById(IDS_FILECLEANER_LST002), LVCFMT_LEFT, 250);
	m_lcList.InsertColumn(EFC_SIZE, LoadStringById(IDS_FILECLEANER_LST003), LVCFMT_RIGHT, 90, -1);

	m_lcList.InitHeader(IDB_LIST_HEADER);
}

void CFileCleaner::InsertItems(bool bChek)
{
	int nCnt, nRow = 0;
	CFileFind ff;
	ULONGLONG nSize = 0;
	
	CString szPath;
	CString szSize;

	m_TotalSize = 0;

	szPath.Format("C:\\ftproot\\%s*.*", UBC_CONTENTS_PATH);
	BOOL bRet = ff.FindFile(szPath);
	
	nCnt = 0;
	while(bRet)
	{
		bRet = ff.FindNextFile();
		if(ff.IsDots())			continue;
		if(ff.IsDirectory())	continue;

		nCnt++;
		nSize += ff.GetLength();
	}
	ff.Close();

	if(nCnt)
	{
		char sDrv[MAX_PATH], sDir[MAX_PATH], sFile[MAX_PATH], sExt[MAX_PATH];
		_splitpath(szPath, sDrv, sDir, sFile, sExt);

		SFCInfo* info = new SFCInfo;
		info->szDrive = sDrv;
		info->szPath = sDir;
		info->szName = _T("FTP Temporary Folder");
		info->szExe = _T("");
		info->lSize = nSize;

		m_lcList.InsertItem(nRow, "", -1);

		LVITEM itemDrv;
		itemDrv.iItem = nRow;
		itemDrv.iSubItem = EFC_DRV;
		itemDrv.mask = LVIF_IMAGE|LVIF_TEXT;
		itemDrv.iImage = 20;
		itemDrv.pszText = sDrv;
		m_lcList.SetItem(&itemDrv);

		LVITEM itemFile;
		itemFile.iItem = nRow;
		itemFile.iSubItem = EFC_FILE;
		itemFile.mask = LVIF_IMAGE|LVIF_TEXT;
		itemFile.iImage = 14;
		itemFile.pszText = _T("FTP Temporary Folder");
		m_lcList.SetItem(&itemFile);

//		szSize.Format(_T("%s byte"), MakeCurrency(nSize));
		szSize.Format(_T("%s"), MakeCurrency(nSize));
		m_lcList.SetItemText(nRow, EFC_SIZE, szSize);

		m_lcList.SetItemData(nRow, (DWORD)info);
		m_lsFiles.AddTail(info);

		nRow++;
		m_TotalSize += nSize;
	}

	for(int i = 0; i < m_arDrives.GetCount(); i++)
	{
		CFileFind ff;
		szPath = m_arDrives.GetAt(i) + UBC_CONTENTS_PATH + "*";
		bRet = ff.FindFile(szPath);

		while(bRet)
		{
			bRet = ff.FindNextFile();
			if(ff.IsDots())			continue;
			if(ff.IsDirectory())	continue;

			CString szPath = ff.GetFilePath();
			POSITION pos = m_lsContents.Find(szPath);
			if(pos) continue;

			char sDrv[MAX_PATH], sDir[MAX_PATH], sFile[MAX_PATH], sExt[MAX_PATH];
			_splitpath(szPath, sDrv, sDir, sFile, sExt);
			nSize = ff.GetLength();
			
			SFCInfo* info = new SFCInfo;
			info->szDrive = sDrv;
			info->szPath = sDir;
			info->szName = sFile;
			info->szExe = sExt;
			info->lSize = nSize;

			CString szFile = info->szName+info->szExe;

			m_lcList.InsertItem(nRow, "", -1);
			m_lcList.SetCheck(nRow, bChek);

			LVITEM itemDrv;
			itemDrv.iItem = nRow;
			itemDrv.iSubItem = EFC_DRV;
			itemDrv.mask = LVIF_IMAGE|LVIF_TEXT;
			itemDrv.iImage = 20;
			itemDrv.pszText = sDrv;
			m_lcList.SetItem(&itemDrv);

			LVITEM itemFile;
			itemFile.iItem = nRow;
			itemFile.iSubItem = EFC_FILE;
			itemFile.mask = LVIF_IMAGE|LVIF_TEXT;
			itemFile.iImage = GetFileType(info->szExe);
			itemFile.pszText = szFile.GetBuffer(0);
			m_lcList.SetItem(&itemFile);

//			szSize.Format("%s byte", MakeCurrency(nSize));
			szSize.Format(_T("%s"), MakeCurrency(nSize));
			m_lcList.SetItemText(nRow, EFC_SIZE, szSize);

			m_lcList.SetItemData(nRow, (DWORD)info);
			m_lsFiles.AddTail(info);

			m_TotalSize += nSize;
			nRow++;
		}
		ff.Close();
	}

	CString szTemp;
	szTemp.Format("Total file number : %d\tTotal Size : %s byte", nRow, MakeCurrency(m_TotalSize));
	GetDlgItem(IDC_STATIC)->SetWindowText(szTemp);
}

void CFileCleaner::InitContents()
{
	int nPos =0;
	char drive[] = "?:\\";
	CStringArray arDrives;
	DWORD dwDriveList = ::GetLogicalDrives();

	while(dwDriveList)
	{
		if(dwDriveList & 1 && ('A'+nPos) != 'X')
		{
			drive[0] = 'A' + nPos;
			arDrives.Add(drive);
		}

		nPos++;
		dwDriveList >>=1;
	}

	m_arDrives.RemoveAll();
	m_lsContents.RemoveAll();

	CString szContent;
	// 기본 제공되는 Contents 는 지우면 안됨.
	char szModule[MAX_PATH], szTemp[MAX_PATH];
	::GetModuleFileName(NULL, szModule, MAX_PATH);
	_tsplitpath(szModule, szTemp, NULL, NULL, NULL);

	szContent.Format("%s\\%snot_400.avi", szTemp , UBC_CONTENTS_PATH);
	m_lsContents.AddTail(szContent);
	szContent.Format("%s\\%snot_400.swf", szTemp , UBC_CONTENTS_PATH);
	m_lsContents.AddTail(szContent);
	szContent.Format("%s\\%snot_400.png", szTemp , UBC_CONTENTS_PATH);
	m_lsContents.AddTail(szContent);
	////////////////////////////////

	for(int i = 0; i < arDrives.GetCount(); i++)
	{
		int nDI = 0;
		CString szDrive = arDrives.GetAt(i);
		UINT type = GetDriveType(szDrive);
		switch(type)
		{
		case DRIVE_UNKNOWN:		nDI = 0;	continue;
		case DRIVE_NO_ROOT_DIR:	nDI = 1;	continue;
		case DRIVE_REMOVABLE:	nDI = 2;	break;
		case DRIVE_FIXED:		nDI = 3;	break;
		case DRIVE_REMOTE:		nDI = 4;	continue;
		case DRIVE_CDROM:		nDI = 5;	continue;
		case DRIVE_RAMDISK:		nDI = 6;	break;
		default:				nDI = 0;	continue;
		}

		CFileFind ff;
		m_arDrives.Add(szDrive);
		CString szPath = szDrive + UBC_CONFIG_PATH + "*.ini";
		BOOL bRet = ff.FindFile(szPath);

		while(bRet)
		{
			bRet = ff.FindNextFile();
			if(ff.IsDots())			continue;
			if(ff.IsDirectory())	continue;

			CString szFile = ff.GetFileName();
//			if(0 <= szFile.Find(SAMPLE_FILE_KEY))
//				continue;
//			if(0 <= szFile.Find(PREVIEW_FILE_KEY))
//				continue;
			
			szPath = szDrive + UBC_CONFIG_PATH + szFile;

			CProfileManager objIniManager(szPath);

			int iStart = 0;
			CString szContentList = objIniManager.GetProfileString("Host", "ContentsList");
			szContent = szContentList.Tokenize(",", iStart);
			while(!szContent.IsEmpty())
			{
				CString strTemp = objIniManager.GetProfileString(szContent, "filename");

				if(!strTemp.IsEmpty())
				{
					szContent.Format("%s%s%s", szDrive , UBC_CONTENTS_PATH, strTemp);
					m_lsContents.AddTail(szContent);
				}

				szContent = szContentList.Tokenize(",", iStart);
			}
		}

		ff.Close();
	}
}

int CFileCleaner::GetFileType(CString szFile)
{
	szFile.MakeLower();

	if     (szFile.Find(".swf")  >= 0) return (CONTENTS_FLASH+1);
	else if(szFile.Find(".avi")  >= 0) return (CONTENTS_VIDEO+1);
	else if(szFile.Find(".mpg")  >= 0) return (CONTENTS_VIDEO+1);
	else if(szFile.Find(".mpeg") >= 0) return (CONTENTS_VIDEO+1);
	else if(szFile.Find(".tp")   >= 0) return (CONTENTS_VIDEO+1);
	else if(szFile.Find(".txt")  >= 0) return (CONTENTS_SMS+1);
	else if(szFile.Find(".bmp")  >= 0) return (CONTENTS_IMAGE+1);
	else if(szFile.Find(".gif")  >= 0) return (CONTENTS_IMAGE+1);
	else if(szFile.Find(".jpg")  >= 0) return (CONTENTS_IMAGE+1);
	else if(szFile.Find(".png")  >= 0) return (CONTENTS_IMAGE+1);
	else if(szFile.Find(".mp2")  >= 0) return (CONTENTS_IMAGE+1);
	else if(szFile.Find(".mp3")  >= 0) return (CONTENTS_PROMOTION+1);
	else if(szFile.Find(".mp4")  >= 0) return (CONTENTS_PROMOTION+1);
	else if(szFile.Find(".wav")  >= 0) return (CONTENTS_PROMOTION+1);
	else if(szFile.Find(".htm")  >= 0) return (CONTENTS_WEBBROWSER+1);
	else if(szFile.Find(".html") >= 0) return (CONTENTS_WEBBROWSER+1);
	else if(szFile.Find(".ppt")  >= 0) return (CONTENTS_PPT+1);
	else if(szFile.Find(".pps")  >= 0) return (CONTENTS_PPT+1);
	else if(szFile.Find(".pptx") >= 0) return (CONTENTS_PPT+1);
	else if(szFile.Find(".ppsx") >= 0) return (CONTENTS_PPT+1);
	else if(szFile.Find(".flv")  >= 0) return (CONTENTS_VIDEO+1);
	else return (CONTENTS_NOT_DEFINE+1);
}

void CFileCleaner::DeleteFile(CString szFile, bool bWastebasket)
{
	if(bWastebasket) CFile::Remove(szFile);
	else             ::DeleteFile(szFile);
}

void CFileCleaner::DeleteFolderFiles(CString szPath)
{
	CFileFind ff;
	BOOL bRet = ff.FindFile(szPath);

	while(bRet)
	{
		bRet = ff.FindNextFile();
		if(ff.IsDots())			continue;
		if(ff.IsDirectory())	continue;

		CString szFile = ff.GetFilePath();
		::DeleteFile(szFile);
	}

	ff.Close();
}

void CFileCleaner::OnBnClickedButtonRemove()
{
	for(int i = m_lcList.GetItemCount()-1; i >= 0 ; i--)
	{
		if(!m_lcList.GetCheck(i)) continue;

		SFCInfo* pInfo = (SFCInfo *)m_lcList.GetItemData(i);
		if(!pInfo)	continue;
		
		if(pInfo->szName.Compare("FTP Temporary Folder"))
			::DeleteFile(pInfo->szDrive+pInfo->szPath+pInfo->szName+pInfo->szExe);
		else
			DeleteFolderFiles(pInfo->szDrive+pInfo->szPath+_T("*.*"));
		
		m_lcList.DeleteItem(i);

		POSITION pos = m_lsFiles.Find(pInfo);
		m_lsFiles.RemoveAt(pos);
	}

	m_TotalSize = 0;
	POSITION pos = m_lsFiles.GetHeadPosition();
	while(pos)
	{
		SFCInfo* pInfo = m_lsFiles.GetNext(pos);
		if(!pInfo) continue;
		m_TotalSize += pInfo->lSize;
	}

	CString szTemp;
	szTemp.Format("Total file number : %d\tTotal Size : %s byte", m_lsFiles.GetCount(), MakeCurrency(m_TotalSize));
	GetDlgItem(IDC_STATIC)->SetWindowText(szTemp);
}

void CFileCleaner::OnNMDblclkListFcnFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	if(pNMLV->iItem < 0) return;

	SFCInfo* pInfo = (SFCInfo *)m_lcList.GetItemData(pNMLV->iItem);
	if(!pInfo) return;

	if(pInfo->szName.Compare("FTP Temporary Folder"))
	{
		CString strFile = pInfo->szDrive+pInfo->szPath+pInfo->szName+pInfo->szExe;
		::ShellExecute(NULL, _T("OPEN"), strFile, NULL, NULL, SW_NORMAL);
	}
	else
	{
		::ShellExecute(NULL, _T("OPEN"), pInfo->szDrive+pInfo->szPath, NULL, NULL, SW_NORMAL);
	}

	*pResult = 0;
}

void CFileCleaner::OnLvnColumnclickListFcnFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int	nColumn = pNMLV->iSubItem;

	if(nColumn == eCheck)
	{
		BOOL bCheck = !m_lcList.GetCheckHdrState();
		for(int nRow=0; nRow<m_lcList.GetItemCount() ;nRow++)
		{
			m_lcList.SetCheck(nRow, bCheck);
		}

		m_lcList.SetCheckHdrState(bCheck);
	}
	else
	{
		m_lcList.SetSortHeader(nColumn);
		m_lcList.SortItems(CompareList, (DWORD_PTR)this);
	}
}

int CFileCleaner::CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	return ((CFileCleaner*)lParam)->m_lcList.Compare(lParam1, lParam2);
}
