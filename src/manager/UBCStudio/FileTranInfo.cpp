// FileTranInfo.cpp : implementation file
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "FileTranInfo.h"


// CFileTranInfo dialog

IMPLEMENT_DYNAMIC(CFileTranInfo, CDialog)

CFileTranInfo::CFileTranInfo(CWnd* pParent /*=NULL*/)
	: CDialog(CFileTranInfo::IDD, pParent)
{
}

CFileTranInfo::~CFileTranInfo()
{
}

void CFileTranInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_REC_FT_INFO, m_ctrInfo);
}


BEGIN_MESSAGE_MAP(CFileTranInfo, CDialog)
END_MESSAGE_MAP()


// CFileTranInfo message handlers

BOOL CFileTranInfo::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_ctrInfo.SetWindowText(m_szInfo);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
