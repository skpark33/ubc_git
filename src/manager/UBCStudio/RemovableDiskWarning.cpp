// RemovableDiskWarning.cpp : 구현 파일입니다.
//
// skpark USB_Device

#include "stdafx.h"
#include "RemovableDiskWarning.h"


// RemovableDiskWarning 대화 상자입니다.

IMPLEMENT_DYNAMIC(RemovableDiskWarning, CDialog)

RemovableDiskWarning::RemovableDiskWarning(CWnd* pParent /*=NULL*/)
	: CDialog(RemovableDiskWarning::IDD, pParent)
{
}

RemovableDiskWarning::~RemovableDiskWarning()
{
}

void RemovableDiskWarning::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_REMOVABLE_WARN, RemovableWarning);
	DDX_Control(pDX, IDC_REMOVABLE_PACKAGE_LIST, RemovablePackageList);
	DDX_Control(pDX, ID_REMOVABLE_OK, RemovableOK);
	DDX_Control(pDX, ID_REMOVABLE_CANCEL, RemovableCancel);
	DDX_Control(pDX, IDC_REMOVABLE_IGNORE, RemovableIgnore);
}


BEGIN_MESSAGE_MAP(RemovableDiskWarning, CDialog)
	ON_BN_CLICKED(ID_REMOVABLE_OK, &RemovableDiskWarning::OnBnClickedRemovableOk)
	ON_BN_CLICKED(ID_REMOVABLE_CANCEL, &RemovableDiskWarning::OnBnClickedRemovableCancel)
	ON_BN_CLICKED(IDC_REMOVABLE_IGNORE, &RemovableDiskWarning::OnBnClickedRemovableIgnore)
END_MESSAGE_MAP()


// RemovableDiskWarning 메시지 처리기입니다.

void RemovableDiskWarning::OnBnClickedRemovableOk()
{
	// Removable Disk 에 있는 ini를 삭제한다.  

	CString targetPath = this->drive;
	targetPath.Append("\\SQISoft\\UTV1.0\\execute\\config\\");

	for(int i=0; i<iniList.GetCount() ;i++) {
		CString targetFile = targetPath;
		targetFile.Append(iniList[i]);
		::DeleteFile(targetFile);
	}
	CDialog::OnOK();

}

void RemovableDiskWarning::OnBnClickedRemovableCancel()
{
	// 아무것도 안하고 통과, Save 작업은 중단된다.
	CDialog::OnCancel();

}

void RemovableDiskWarning::OnBnClickedRemovableIgnore()
{
	// 아무것도 안하고 통과, Save 작업은 계속된다.
	CDialog::OnOK();
}

BOOL RemovableDiskWarning::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	// 경고 문자를 보여준다.
	RemovableWarning.SetWindowText(LoadStringById(IDS_STR_REMOVABLE_WARNING));
	RemovableWarning.Invalidate();

	// ini 목록을 보여준다.
	for(int i=0; i<iniList.GetCount() ;i++) {
		RemovablePackageList.AddString(iniList[i]);
	}
	//RemovablePackageList.RedrawWindow();
	RemovablePackageList.Invalidate();
	
	RemovableOK.SetWindowText(LoadStringById(IDS_STR_REMOVABLE_BUTTON_OK));
	RemovableCancel.SetWindowText(LoadStringById(IDS_STR_REMOVABLE_BUTTON_Cancel));
	RemovableIgnore.SetWindowText(LoadStringById(IDS_STR_REMOVABLE_BUTTON_Ignore));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
