// PlayContentsSearchView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "UBCStudioDoc.h"
#include "PlayContentsSearchView.h"

#include "Schedule.h"

// CPlayContentsSearchView

IMPLEMENT_DYNCREATE(CPlayContentsSearchView, CFormView)

CPlayContentsSearchView::CPlayContentsSearchView()
:	CFormView(CPlayContentsSearchView::IDD)
,	m_reposControl (this)
{

}

CPlayContentsSearchView::~CPlayContentsSearchView()
{
}

void CPlayContentsSearchView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_PLAYCONTENTS_TYPE, m_cbxPlayContentsType);
	DDX_Control(pDX, IDC_DATE_START, m_dateStart);
	DDX_Control(pDX, IDC_TIME_START, m_timeStart);
	DDX_Control(pDX, IDC_DATE_END, m_dateEnd);
	DDX_Control(pDX, IDC_TIME_END, m_timeEnd);
//	DDX_Control(pDX, IDC_BUTTON_TIME_SCOPE, m_cbxTimeZone);	// time-zone 사용하지 않음
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_TYPE, m_cbxContentsType);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_FILE_NAME, m_editFilename);
	DDX_Control(pDX, IDC_LIST_PLAYCONTENTS, m_lcPlayContents);
	DDX_Control(pDX, IDC_BUTTON_SEARCH, m_btnSearch);
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	DDX_Control(pDX, IDC_BUTTON_TIME_SCOPE, m_btnTimeZone);
	DDX_Control(pDX, IDC_BUTTON_MODIFY_PLAYCONTENTS, m_btnModifyPlayContents);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_PLAYCONTENTS, m_btnDeletePlayContents);
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
}

BEGIN_MESSAGE_MAP(CPlayContentsSearchView, CFormView)
	ON_CBN_SELCHANGE(IDC_COMBO_PLAYCONTENTS_TYPE, &CPlayContentsSearchView::OnCbnSelchangeComboPlayContentsType)
	ON_BN_CLICKED(IDC_BUTTON_SEARCH, &CPlayContentsSearchView::OnBnClickedButtonSearch)
END_MESSAGE_MAP()


// CPlayContentsSearchView 진단입니다.

#ifdef _DEBUG
void CPlayContentsSearchView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPlayContentsSearchView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif

CUBCStudioDoc* CPlayContentsSearchView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCStudioDoc)));
	return (CUBCStudioDoc*)m_pDocument;
}
#endif //_DEBUG


// CPlayContentsSearchView 메시지 처리기입니다.

void CPlayContentsSearchView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

//	ResizeParentToFit();
	{
		// determine current size of the client area as if no scrollbars present
		CRect rectClient;
		GetWindowRect(rectClient);
		CRect rect = rectClient;
		CalcWindowRect(rect);
		rectClient.left += rectClient.left - rect.left;
		rectClient.top += rectClient.top - rect.top;
		rectClient.right -= rect.right - rectClient.right;
		rectClient.bottom -= rect.bottom - rectClient.bottom;
		rectClient.OffsetRect(-rectClient.left, -rectClient.top);
		ASSERT(rectClient.left == 0 && rectClient.top == 0);

		// determine desired size of the view
		CRect rectView(0, 0, m_totalDev.cx, m_totalDev.cy);
		{
			if (rectClient.right <= m_totalDev.cx)
				rectView.right = rectClient.right;
			if (rectClient.bottom <= m_totalDev.cy)
				rectView.bottom = rectClient.bottom;
		}
		CalcWindowRect(rectView, CWnd::adjustOutside);
		rectView.OffsetRect(-rectView.left, -rectView.top);
		ASSERT(rectView.left == 0 && rectView.top == 0);
		{
			if (rectClient.right <= m_totalDev.cx)
				rectView.right = rectClient.right;
			if (rectClient.bottom <= m_totalDev.cy)
				rectView.bottom = rectClient.bottom;
		}

		rectView.DeflateRect(::GetSystemMetrics(SM_CXDLGFRAME)-1, ::GetSystemMetrics(SM_CYDLGFRAME)-1);

		m_reposControl.SetParentRect(CRect(0,0,rectView.Width(), rectView.Height()));

		SetScrollSizes(MM_TEXT, CSize(1,1));
	}

	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	m_cbxPlayContentsType.AddString("All");
	m_cbxPlayContentsType.AddString("Cycle Play-Contents");
	m_cbxPlayContentsType.AddString("Time Base Play-Contents");
	m_cbxPlayContentsType.SetCurSel(0);

	CTime start_time(2000, 1, 1, 0, 0, 0);
	m_dateStart.SetTime(&start_time);
	m_timeStart.SetTime(&start_time);

	CTime end_time(2999, 12, 31, 23, 59, 59);
	m_dateEnd.SetTime(&end_time);
	m_timeEnd.SetTime(&end_time);

	//
	// time-zone 사용하지 않음
	//
	//m_cbxTimeZone.AddString("0 ~ 7h", 7);
	//m_cbxTimeZone.AddString("7 ~ 9h", 1);
	//m_cbxTimeZone.AddString("9 ~ 12h", 2);
	//m_cbxTimeZone.AddString("12 ~ 13h", 3);
	//m_cbxTimeZone.AddString("13 ~ 18h", 4);
	//m_cbxTimeZone.AddString("18 ~ 22h", 5);
	//m_cbxTimeZone.AddString("22 ~ 24h", 6);
	//m_cbxTimeZone.CheckAll(TRUE);

	m_cbxContentsType.AddString("Video", 0);
	m_cbxContentsType.AddString("Image", 2);
	m_cbxContentsType.AddString("Ticker", 5);
	m_cbxContentsType.AddString("Flash", 8);
	m_cbxContentsType.AddString("Text", 1);
	m_cbxContentsType.AddString("Camera", 9);
	//m_cbxContentsType.AddString("Promotion", 3);
	//m_cbxContentsType.AddString("TV", 4);
	//m_cbxContentsType.AddString("Phone", 6);
	//m_cbxContentsType.AddString("Web", 7);
	m_cbxContentsType.CheckAll(TRUE);

	//
	int index = 0;
	m_lcPlayContents.SetExtendedStyle(m_lcPlayContents.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_HEADERDRAGDROP | LVS_EX_SUBITEMIMAGES );
	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST001), LVCFMT_LEFT, 75);
	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST002), LVCFMT_LEFT, 75);
	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST003), LVCFMT_LEFT, 75);
	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST004), LVCFMT_LEFT, 75);

	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST005), LVCFMT_LEFT, 140);
	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST006), LVCFMT_LEFT, 140);
	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST007), LVCFMT_LEFT, 90);
	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST008), LVCFMT_LEFT, 150);
	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST009), LVCFMT_LEFT, 50);
	m_lcPlayContents.InsertColumn(index++, LoadStringById(IDS_PLAYCONTENTSSEARCHVIEW_LST010), LVCFMT_LEFT, 150);

	m_lcPlayContents.Initialize(false);

	m_reposControl.AddControl(&m_lcPlayContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.Move();

}

void CPlayContentsSearchView::OnCbnSelchangeComboPlayContentsType()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//
	// time-zone 사용하지 않음
	//

	//switch(m_cbxPlayContentsType.GetCurSel())
	//{
	//case 0:
	//	m_timeStart.EnableWindow(TRUE);
	//	m_timeEnd.EnableWindow(TRUE);
	//	m_cbxTimeZone.EnableWindow(TRUE);
	//	break;
	//case 1:
	//	m_timeStart.EnableWindow(FALSE);
	//	m_timeEnd.EnableWindow(FALSE);
	//	m_cbxTimeZone.EnableWindow(TRUE);
	//	break;
	//case 2:
	//	m_timeStart.EnableWindow(TRUE);
	//	m_timeEnd.EnableWindow(TRUE);
	//	m_cbxTimeZone.EnableWindow(FALSE);
	//	break;
	//}
}

void CPlayContentsSearchView::OnBnClickedButtonSearch()
{
	m_lcPlayContents.DeleteAllItems();

	//
	int nPlayContentsType;
	switch(m_cbxPlayContentsType.GetCurSel())
	{
	case 0:	nPlayContentsType = 0x0003;	break;
	case 1:	nPlayContentsType = 0x0001;	break;
	case 2:	nPlayContentsType = 0x0002;	break;
	}

	//
	CTime start_date;		m_dateStart.GetTime(start_date);
	CString str_start_date= start_date.Format("%Y/%m/%d");
	int tm_start_date = (int)start_date.GetTime();
	CTime start_time;		m_timeStart.GetTime(start_time);
	CString str_start_time= start_time.Format("%H:%M:%S");

	//
	CTime end_date;			m_dateEnd.GetTime(end_date);
	CString str_end_date  = end_date.Format("%Y/%m/%d");
	int tm_end_date = (int)end_date.GetTime();
	CTime end_time;			m_timeEnd.GetTime(end_time);
	CString str_end_time  = end_time.Format("%H:%M:%S");

	//
	//CCheckComboBox	m_cbxTimeZone;

	//
	bool contents_type_list[11];
	for(int i=0; i<11; i++)
	{
		if(m_cbxContentsType.GetCheck(i))
			contents_type_list[i] = true;
		else
			contents_type_list[i] = false;
	}

	//
	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);

	//
	CString file_name;
	m_editFilename.GetWindowText(file_name);

	//
	CUBCStudioDoc* pDoc = GetDocument();

	TEMPLATE_LINK_LIST* template_link_list = pDoc->GetAllTemplateList();

	if(template_link_list != NULL)
	{
		int template_count = template_link_list->GetCount();
		for(int i=0; i<template_count; i++)
		{
			TEMPLATE_LINK& template_link = template_link_list->GetAt(i);

			int frame_count = template_link.arFrameLinkList.GetCount();
			for(int j=0; j<frame_count; j++)
			{
				FRAME_LINK& frame_link = template_link.arFrameLinkList.GetAt(j);

				if(nPlayContentsType & 0x0001)
				{
					for(int k=0; k<frame_link.pFrameInfo->arCyclePlayContentsList.GetCount(); k++)
					{
						PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);
						CONTENTS_INFO* pContentsInfo = pDoc->GetContents(pPlayContentsInfo->strContentsId);

						if( tm_start_date <= pPlayContentsInfo->nStartDate && pPlayContentsInfo->nEndDate <= tm_end_date &&
							str_start_time <= pPlayContentsInfo->strStartTime && pPlayContentsInfo->strEndTime <= str_end_time &&
							pContentsInfo != NULL && contents_type_list[pContentsInfo->nContentsType] &&
							(contents_name.GetLength() == 0 || pContentsInfo->strContentsName.Find(contents_name) >= 0) &&
							(file_name.GetLength() == 0 || pContentsInfo->strFilename.Find(file_name) >= 0)
						)
						{
							int index = m_lcPlayContents.GetItemCount();
							m_lcPlayContents.InsertItem(index, pPlayContentsInfo->strTemplateId);
							m_lcPlayContents.SetItemText(index, 1, pPlayContentsInfo->strFrameId);
							m_lcPlayContents.SetItemText(index, 2, pPlayContentsInfo->strId);
							m_lcPlayContents.SetItemText(index, 3, pContentsInfo->strId);

							//m_lcPlayContents.SetItemText(index, 4, pPlayContentsInfo->nStartDate + " ~ " + pPlayContentsInfo->nEndDate);

							if(pPlayContentsInfo->bIsDefaultPlayContents)
								m_lcPlayContents.SetItemText(index, 5, TIME_SCOPE_TO_STRING[pPlayContentsInfo->nTimeScope]); // cycle
							else
								m_lcPlayContents.SetItemText(index, 5, pPlayContentsInfo->strStartTime + " ~ " + pPlayContentsInfo->strEndTime); // time base

							m_lcPlayContents.SetItemText(index, 6, CONTENTS_TYPE_TO_STRING[pContentsInfo->nContentsType + 1]);
							m_lcPlayContents.SetItemText(index, 7, pContentsInfo->strContentsName);
							m_lcPlayContents.SetItemText(index, 9, pContentsInfo->strFilename);

							CString tm;
							tm.Format("%02ld:%02ld", pContentsInfo->nRunningTime/60, pContentsInfo->nRunningTime % 60);
							m_lcPlayContents.SetItemText(index, 8, tm);

							m_lcPlayContents.SetItemData(index, (DWORD)pPlayContentsInfo);
						}
					}
				}
				if(nPlayContentsType & 0x0002)
				{
					for(int k=0; k<frame_link.pFrameInfo->arTimePlayContentsList.GetCount(); k++)
					{
						PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);
						CONTENTS_INFO* pContentsInfo = pDoc->GetContents(pPlayContentsInfo->strContentsId);

						if( tm_start_date <= pPlayContentsInfo->nStartDate && pPlayContentsInfo->nEndDate <= tm_end_date &&
							str_start_time <= pPlayContentsInfo->strStartTime && pPlayContentsInfo->strEndTime <= str_end_time &&
							pContentsInfo != NULL && contents_type_list[pContentsInfo->nContentsType] &&
							(contents_name.GetLength() == 0 || pContentsInfo->strContentsName.Find(contents_name) >= 0) &&
							(file_name.GetLength() == 0 || pContentsInfo->strFilename.Find(file_name) >= 0)
						)
						{
							int index = m_lcPlayContents.GetItemCount();
							m_lcPlayContents.InsertItem(index, pPlayContentsInfo->strTemplateId);
							m_lcPlayContents.SetItemText(index, 1, pPlayContentsInfo->strFrameId);
							m_lcPlayContents.SetItemText(index, 2, pPlayContentsInfo->strId);
							m_lcPlayContents.SetItemText(index, 3, pContentsInfo->strId);

							//m_lcPlayContents.SetItemText(index, 4, pPlayContentsInfo->nStartDate + " ~ " + pPlayContentsInfo->nEndDate);

							if(pPlayContentsInfo->bIsDefaultPlayContents)
								m_lcPlayContents.SetItemText(index, 5, TIME_SCOPE_TO_STRING[pPlayContentsInfo->nTimeScope]); // cycle
							else
								m_lcPlayContents.SetItemText(index, 5, pPlayContentsInfo->strStartTime + " ~ " + pPlayContentsInfo->strEndTime); // time base

							m_lcPlayContents.SetItemText(index, 6, CONTENTS_TYPE_TO_STRING[pContentsInfo->nContentsType + 1]);
							m_lcPlayContents.SetItemText(index, 7, pContentsInfo->strContentsName);
							m_lcPlayContents.SetItemText(index, 9, pContentsInfo->strFilename);

							CString tm;
							tm.Format("%02ld:%02ld", pContentsInfo->nRunningTime/60, pContentsInfo->nRunningTime % 60);
							m_lcPlayContents.SetItemText(index, 8, tm);

							m_lcPlayContents.SetItemData(index, (DWORD)pPlayContentsInfo);
						}
					}
				}
			}
		}
	}
}
