// FlashImageplayerEffectsOderChangeDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "FlashImageplayerEffectsOderChangeDlg.h"


// CFlashImageplayerEffectsOderChangeDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFlashImageplayerEffectsOderChangeDlg, CDialog)

CFlashImageplayerEffectsOderChangeDlg::CFlashImageplayerEffectsOderChangeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFlashImageplayerEffectsOderChangeDlg::IDD, pParent)
	, m_pParentWnd ( pParent )
{
	m_nSendUpdateMessage = 0;
}

CFlashImageplayerEffectsOderChangeDlg::~CFlashImageplayerEffectsOderChangeDlg()
{
}

void CFlashImageplayerEffectsOderChangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_EFFECTS, m_lcEffects);
}


BEGIN_MESSAGE_MAP(CFlashImageplayerEffectsOderChangeDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_UP, &CFlashImageplayerEffectsOderChangeDlg::OnBnClickedButtonMoveUp)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_DOWN, &CFlashImageplayerEffectsOderChangeDlg::OnBnClickedButtonMoveDown)
	ON_WM_NCACTIVATE()
END_MESSAGE_MAP()


// CFlashImageplayerEffectsOderChangeDlg 메시지 처리기입니다.

typedef struct {
	char* effect_id;
	int effect_res_id;
} EFFECT_ITEM;

EFFECT_ITEM _effects[] = { { EFFECTS_MOSAIC,		IDS_EFFECTS_MOSAIC },
							{ EFFECTS_DISSOLVE,		IDS_EFFECTS_DISSOLVE }, 
							{ EFFECTS_PERSPECTIVE,	IDS_EFFECTS_PERSPECTIVE }, 
							{ EFFECTS_IRIS,			IDS_EFFECTS_IRIS }, 
							{ EFFECTS_REVOLVE,		IDS_EFFECTS_REVOLVE }, 
							{ EFFECTS_PUSH,			IDS_EFFECTS_PUSH }, 
							{ EFFECTS_ZOOM,			IDS_EFFECTS_ZOOM }, 
							{ EFFECTS_CUBE,			IDS_EFFECTS_CUBE }, 
							{ EFFECTS_WIPE,			IDS_EFFECTS_WIPE },
							{ EFFECTS_END_MARK,		0 } }; // end mark

CString LoadString(UINT id)
{
	CString str;
	str.LoadString(id);
	return str;
}

BOOL CFlashImageplayerEffectsOderChangeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_lcEffects.ModifyStyle(WS_HSCROLL | WS_VSCROLL, NULL);
	m_lcEffects.SetExtendedStyle(m_lcEffects.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_CHECKBOXES);

	CRect rect;
	m_lcEffects.GetClientRect(rect);

	m_lcEffects.InsertColumn(0, "Check", 0, 20);
	m_lcEffects.InsertColumn(1, "Effects Name", 0, rect.Width()-20);

	//
	int idx = 0;
	for(int i=0; i<m_listSelectedEffectsOrder.GetCount(); i++)
	{
		const CString& str_effect = m_listSelectedEffectsOrder.GetAt(i);

		for(int j=0; strlen(_effects[j].effect_id)>0; j++)
		{
			if( str_effect == _effects[j].effect_id )
			{
				m_lcEffects.InsertItem(idx, _effects[j].effect_id);
				m_lcEffects.SetItemText(idx, 1, ::LoadString(_effects[j].effect_res_id));
				m_lcEffects.SetCheck(idx);
				idx++;
				break;
			}
		}
	}
	for(int i=0; strlen(_effects[i].effect_id)>0; i++)
	{
		bool find = false;
		for(int j=0; j<m_listSelectedEffectsOrder.GetCount(); j++)
		{
			const CString& str_effect = m_listSelectedEffectsOrder.GetAt(j);

			if( str_effect == _effects[i].effect_id )
			{
				find = true;
				break;
			}
		}

		if( find == false )
		{
			m_lcEffects.InsertItem(idx, _effects[i].effect_id);
			m_lcEffects.SetItemText(idx, 1, ::LoadString(_effects[i].effect_res_id));
			idx++;
		}
	}


	//m_lcEffects.SetColumnWidth(1, LVSCW_AUTOSIZE );

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFlashImageplayerEffectsOderChangeDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnOK();
}

void CFlashImageplayerEffectsOderChangeDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnCancel();
}

void CFlashImageplayerEffectsOderChangeDlg::OnBnClickedButtonMoveUp()
{
	int idx = -1;
	for(int i=0; i<m_lcEffects.GetItemCount(); i++)
	{
		if( m_lcEffects.GetItemState(i, LVIS_SELECTED) != NULL )
		{
			idx = i;
			break;
		}
	}
	//int idx = m_lcEffects.GetSelectionMark();
	if( idx > 0 )
	{
		CString str_up_str1 = m_lcEffects.GetItemText(idx-1, 0);
		CString str_up_str2 = m_lcEffects.GetItemText(idx-1, 1);
		BOOL check_up = m_lcEffects.GetCheck(idx-1);

		CString str_down_str1 = m_lcEffects.GetItemText(idx, 0);
		CString str_down_str2 = m_lcEffects.GetItemText(idx, 1);
		BOOL check_down = m_lcEffects.GetCheck(idx);

		m_lcEffects.SetItemText(idx-1, 0, str_down_str1);
		m_lcEffects.SetItemText(idx-1, 1, str_down_str2);
		m_lcEffects.SetCheck(idx-1, check_down);

		m_lcEffects.SetItemText(idx, 0, str_up_str1);
		m_lcEffects.SetItemText(idx, 1, str_up_str2);
		m_lcEffects.SetCheck(idx, check_up);

		m_lcEffects.SetItemState(idx-1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		m_lcEffects.SetItemState(idx, NULL, LVIS_SELECTED | LVIS_FOCUSED);
	}

	m_lcEffects.SetFocus();
}

void CFlashImageplayerEffectsOderChangeDlg::OnBnClickedButtonMoveDown()
{
	int idx = -1;
	for(int i=0; i<m_lcEffects.GetItemCount(); i++)
	{
		if( m_lcEffects.GetItemState(i, LVIS_SELECTED) != NULL )
		{
			idx = i;
			break;
		}
	}
	//int idx = m_lcEffects.GetSelectionMark();
	if( idx < m_lcEffects.GetItemCount()-1 )
	{
		CString str_up_str1 = m_lcEffects.GetItemText(idx, 0);
		CString str_up_str2 = m_lcEffects.GetItemText(idx, 1);
		BOOL check_up = m_lcEffects.GetCheck(idx);

		CString str_down_str1 = m_lcEffects.GetItemText(idx+1, 0);
		CString str_down_str2 = m_lcEffects.GetItemText(idx+1, 1);
		BOOL check_down = m_lcEffects.GetCheck(idx+1);

		m_lcEffects.SetItemText(idx, 0, str_down_str1);
		m_lcEffects.SetItemText(idx, 1, str_down_str2);
		m_lcEffects.SetCheck(idx, check_down);

		m_lcEffects.SetItemText(idx+1, 0, str_up_str1);
		m_lcEffects.SetItemText(idx+1, 1, str_up_str2);
		m_lcEffects.SetCheck(idx+1, check_up);

		m_lcEffects.SetItemState(idx+1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		m_lcEffects.SetItemState(idx, NULL, LVIS_SELECTED | LVIS_FOCUSED);
	}

	m_lcEffects.SetFocus();
}

BOOL CFlashImageplayerEffectsOderChangeDlg::OnNcActivate(BOOL bActive)
{
	if( m_nSendUpdateMessage++ == 1 )
	{
		m_listSelectedEffectsOrder.RemoveAll();
		m_strSelectedEffectsOrderString = "";

		// pre-check (checked item is NULL?)
		bool nothing_check_item = true;
		for(int i=0; i<m_lcEffects.GetItemCount(); i++)
		{
			if( m_lcEffects.GetCheck(i) )
			{
				nothing_check_item = false;
				break;
			}
		}

		// get checked item (or all)
		for(int i=0; i<m_lcEffects.GetItemCount(); i++)
		{
			if( m_lcEffects.GetCheck(i) || nothing_check_item ) // get cheched (or all)
			{
				m_listSelectedEffectsOrder.Add(m_lcEffects.GetItemText(i, 0));

				if( m_strSelectedEffectsOrderString.GetLength() > 0 ) m_strSelectedEffectsOrderString += ", ";
				m_strSelectedEffectsOrderString += m_lcEffects.GetItemText(i, 1);
			}
		}

		m_pParentWnd->PostMessage(WM_UPDATE_EFFECTS_ORDER);
		ShowWindow(SW_HIDE);
		//TRACE("EFFECTS ORDER STRING = %s, %d\n", m_strSelectedEffectsOrder, m_nSendUpdateMessage);
	}

	return CDialog::OnNcActivate(bActive);
}

CString CFlashImageplayerEffectsOderChangeDlg::GeEffectsOrderString(CStringArray& listEffectsOrder)
{
	CString ret_str;
	for(int i=0; i<listEffectsOrder.GetCount(); i++)
	{
		const CString& str_effect = listEffectsOrder.GetAt(i);

		for(int j=0; strlen(_effects[j].effect_id)>0; j++)
		{
			if( str_effect == _effects[j].effect_id )
			{
				if( ret_str.GetLength() > 0 ) ret_str += ", ";
				ret_str += ::LoadString(_effects[j].effect_res_id);
				break;
			}
		}
	}

	return ret_str;
}
