// EEHostSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EEPackageDlg.h"
#include "Enviroment.h"
#include "LocalHostInfo.h"
#include "CopModule.h"
#include "NotifySaveDlg.h"
#include "InfoImportThread.h"
#include "common/UbcCode.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common/libProfileManager/ProfileManager.h"

// CEEPackageDlg dialog

IMPLEMENT_DYNAMIC(CEEPackageDlg, CDialog)

CEEPackageDlg::CEEPackageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEEPackageDlg::IDD, pParent)
{
	m_pThread = NULL;
	m_pDocument = NULL;
	m_pNotiDlg = NULL;
}

CEEPackageDlg::~CEEPackageDlg()
{
}

void CEEPackageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HOSTEE, m_lcPackage);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_DT_START_DATE, m_dtStartDate);
	DDX_Control(pDX, IDC_DT_END_DATE, m_dtEndDate);
	DDX_Control(pDX, IDC_LIST_CATEGORY, m_lcFilter[eFilterCategory]);
	DDX_Control(pDX, IDC_LIST_PURPOSE, m_lcFilter[eFilterPurpose]);
	DDX_Control(pDX, IDC_LIST_HOSTTYPE, m_lcFilter[eFilterHostType]);
	DDX_Control(pDX, IDC_LIST_VERTICAL, m_lcFilter[eFilterVertical]);
	DDX_Control(pDX, IDC_LIST_RESOLUTION, m_lcFilter[eFilterResolution]);
	DDX_Control(pDX, IDC_LIST_PUBLIC, m_lcFilter[eFilterPublic]);
	DDX_Control(pDX, IDC_LIST_APPROVED, m_lcFilter[eFilterVerify]);
	DDX_Control(pDX, IDC_CB_INCLUDED_CONTENTS, m_cbxIncludedContents);
}

BEGIN_MESSAGE_MAP(CEEPackageDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_MESSAGE(WM_INFOIMPORT_COMPELETE, OnImportComplete)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOSTEE, &CEEPackageDlg::OnNMDblclkListHostee)
	ON_BN_CLICKED(IDC_BUTTON_FROM_SERVER, &CEEPackageDlg::OnBnClickedButtonFromServer)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_HOSTEE, &CEEPackageDlg::OnLvnItemchangedListHostee)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CEEPackageDlg::OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BN_REGISTER, OnBnClickedBnRegister)
	ON_CBN_SELCHANGE(IDC_CB_INCLUDED_CONTENTS, OnCbnSelchangeCbIncludedContents)
END_MESSAGE_MAP()

// CEEPackageDlg message handlers
BOOL CEEPackageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CTime tmCur = CTime::GetCurrentTime();
	CTime tmStart = CTime(2000,1,1,0,0,0);
	m_dtStartDate.SetTime(&tmStart);
	m_dtEndDate  .SetTime(&tmCur);

	InitDateRange(m_dtStartDate);
	InitDateRange(m_dtEndDate);

	m_dtStartDate.SetFormat(_T("yyyy/MM/dd"));
	m_dtEndDate.SetFormat(_T("yyyy/MM/dd"));

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));

	m_cbxIncludedContents.InsertString(0, LoadStringById(IDS_UBCSTUDIO_STR001));
	m_cbxIncludedContents.InsertString(1, LoadStringById(IDS_UBCSTUDIO_STR002));
	m_cbxIncludedContents.SetCurSel(0);

	InitFilterListCtrl();

	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterCategory  ], _T("Kind"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterPurpose   ], _T("Purpose"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterHostType  ], _T("HostType"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterVertical  ], _T("Direction"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterResolution], _T("Resolution"));

	m_lcFilter[eFilterPublic].InsertItem(0, LoadStringById(IDS_EEPACKAGEDLG_STR001));
	m_lcFilter[eFilterPublic].SetItemData(0,1);
	m_lcFilter[eFilterPublic].InsertItem(1, LoadStringById(IDS_EEPACKAGEDLG_STR002));
	m_lcFilter[eFilterPublic].SetItemData(1,0);

	m_lcFilter[eFilterVerify].InsertItem(0, LoadStringById(IDS_EEPACKAGEDLG_STR003));
	m_lcFilter[eFilterVerify].SetItemData(0,1);
	m_lcFilter[eFilterVerify].InsertItem(1, LoadStringById(IDS_EEPACKAGEDLG_STR004));
	m_lcFilter[eFilterVerify].SetItemData(1,0);

	for(int i=0; i<eFilterMaxCnt ;i++)
	{
		CRect rect;
		m_lcFilter[i].GetClientRect(rect);
		CScrollBar* pBar = m_lcFilter[i].GetScrollBarCtrl(SB_VERT);
		int width = (pBar && pBar->IsWindowVisible()) ? rect.Width()-::GetSystemMetrics(SM_CXVSCROLL) : rect.Width();
		m_lcFilter[i].SetColumnWidth(0, width);
		//m_lcFilter[i].SetColumnWidth(0, LVSCW_AUTOSIZE);
	}

	InitList();
	LoadFilterData();
	RefreshList(m_szSelPackage);

	// 특정패키지를 열어야 하는 경우.
	if(!m_szSelPackage.IsEmpty())
	{
		for(int nRow = 0; nRow < m_lcPackage.GetItemCount(); nRow++)
		{
			CString szPackage = m_lcPackage.GetItemText(nRow, eColName);
			CString szDrive = m_lcPackage.GetItemText(nRow, eColDrive);

			// 0000756: 같은 스케줄이 두개의 다른 드라이브에 각각 저장되어 있을때, c드라이브의 스케줄을 열어둔 상태에서 d드라이브의 스케줄이 안열립니다.
			if(szPackage == m_szSelPackage && (m_szSelDrive.IsEmpty() || szDrive == m_szSelDrive))
			{
				POSITION pos = (POSITION)m_lcPackage.GetItemData(nRow);
				CString strKey;
				SPackageInfo4Studio* pInfo=NULL;
				GetEnvPtr()->m_mapPackage.GetNextAssoc(pos, strKey, (void*&)pInfo);

				// 서버시간이 더 최신인 경우 확인과정을 거치도록 한다.
				BOOL bFromServer = FALSE;
				CString strUpdateDate = m_lcPackage.GetItemText(nRow, eColUpdateDate);
				CString strServerDate = m_lcPackage.GetItemText(nRow, eColServerDate);

				//if(!strServerDate.IsEmpty() && strServerDate > strUpdateDate)
				if(!strServerDate.IsEmpty())
				{
					bFromServer = (UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG006), MB_YESNO) == IDYES);
				}

				if(LoadPackage(pInfo, bFromServer))
				{
					GetDlgItem(IDOK)->EnableWindow(FALSE);
					GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
					m_bnRefresh.EnableWindow(FALSE);
					GetDlgItem(IDC_BUTTON_FROM_SERVER)->EnableWindow(FALSE);
					m_lcPackage.EnableWindow(FALSE);
				}

				return TRUE;
			}
		}

		UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG001), MB_ICONWARNING);
		OnCancel();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CEEPackageDlg::OnDestroy()
{
	CDialog::OnDestroy();

	if(m_pThread){
		::WaitForSingleObject(m_pThread->m_hThread, 5000);
		m_pThread = NULL;
	}
	TraceLog(("OnDestroy()"));
}

void CEEPackageDlg::OnLvnItemchangedListHostee(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
	
	BOOL bEnable = FALSE;
	int nRow = m_lcPackage.GetRadioSelectedIndex();
	if(nRow >= 0)
	{
		POSITION pos = (POSITION)m_lcPackage.GetItemData(nRow);
		CString strKey;
		SPackageInfo4Studio* pInfo=NULL;
		GetEnvPtr()->m_mapPackage.GetNextAssoc(pos, strKey, (void*&)pInfo);
		bEnable = (pInfo->szProcID.GetLength() > 0 && pInfo->bModify);
	}

	GetDlgItem(IDC_BUTTON_FROM_SERVER)->EnableWindow(bEnable);
}

void CEEPackageDlg::OnNMDblclkListHostee(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	OnBnClickedOk();
}

void CEEPackageDlg::InitList()
{
	m_lcPackage.SetExtendedStyle(m_lcPackage.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	CBitmap bmp;
	bmp.LoadBitmap(IDB_ANNOTATIONS16);

	if(m_ilPackage.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilPackage.Add(&bmp, RGB(255,255,255));
		m_lcPackage.SetImageList(&m_ilPackage, LVSIL_SMALL);
	}

	m_lcPackage.InsertColumn(eColName      , LoadStringById(IDS_EEPACKAGEDLG_LST002), LVCFMT_LEFT  , 248);
	m_lcPackage.InsertColumn(eColDrive     , LoadStringById(IDS_EEPACKAGEDLG_LST001), LVCFMT_LEFT  ,  40);
	m_lcPackage.InsertColumn(eColRegister  , LoadStringById(IDS_EEPACKAGEDLG_LST003), LVCFMT_LEFT  ,  18);
	m_lcPackage.InsertColumn(eColUpdateDate, LoadStringById(IDS_EEPACKAGEDLG_LST005), LVCFMT_CENTER, 110);
	m_lcPackage.InsertColumn(eColServerDate, LoadStringById(IDS_EEPACKAGEDLG_LST008), LVCFMT_CENTER, 110);
	m_lcPackage.InsertColumn(eColUpdateUser, LoadStringById(IDS_EEPACKAGEDLG_LST006), LVCFMT_LEFT  ,  50);
	m_lcPackage.InsertColumn(eColSite      , LoadStringById(IDS_EEPACKAGEDLG_LST007), LVCFMT_LEFT  ,  70);
	m_lcPackage.InsertColumn(eDescript     , LoadStringById(IDS_EEPACKAGEDLG_LST016), LVCFMT_LEFT  ,  70);
	m_lcPackage.InsertColumn(eCategory     , LoadStringById(IDS_EEPACKAGEDLG_LST009), LVCFMT_LEFT  ,  70);
	m_lcPackage.InsertColumn(ePurpose      , LoadStringById(IDS_EEPACKAGEDLG_LST010), LVCFMT_LEFT  ,  70);
	m_lcPackage.InsertColumn(eHostType     , LoadStringById(IDS_EEPACKAGEDLG_LST011), LVCFMT_LEFT  ,  70);
	m_lcPackage.InsertColumn(eVertical     , LoadStringById(IDS_EEPACKAGEDLG_LST012), LVCFMT_LEFT  ,  70);
	m_lcPackage.InsertColumn(eResolution   , LoadStringById(IDS_EEPACKAGEDLG_LST013), LVCFMT_LEFT  ,  70);
	m_lcPackage.InsertColumn(ePublic       , LoadStringById(IDS_EEPACKAGEDLG_LST014), LVCFMT_LEFT  ,  70);
	m_lcPackage.InsertColumn(eVerify       , LoadStringById(IDS_EEPACKAGEDLG_LST015), LVCFMT_LEFT  ,  70);
	m_lcPackage.InsertColumn(eValidateDate , LoadStringById(IDS_EEPACKAGEDLG_LST017), LVCFMT_LEFT  , 110);

	m_lcPackage.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_RADIOBOX);
}

void CEEPackageDlg::RefreshList(CString strSelPackage)
{
	m_lcPackage.SetRedraw(FALSE);
	m_lcPackage.DeleteAllItems();

	CWaitMessageBox wait;

	bool bAll = true;
	CString strWhere;

	CString strPackage     ;
	CString strTagList     ;
	CString strRegister    ;
	CString strContentsName;
	CString strContentsId  ;
	CString strStartDate   ;
	CString strEndDate     ;
	CString strFilter[eFilterMaxCnt];
	CString strCurDateTime = CTime::GetCurrentTime().Format(_T("%Y/%m/%d %H:%M:%S"));

	GetDlgItemText(IDC_EB_PACKAGE , strPackage     );
	GetDlgItemText(IDC_EB_TAG     , strTagList     );
	GetDlgItemText(IDC_EB_REGISTER, strRegister    );
	if(m_cbxIncludedContents.GetCount() > 2)
	{
		m_cbxIncludedContents.GetLBText(1, strContentsName);
		strContentsId = m_strFilterContentsId;
	}

	CTime tmTemp;
	m_dtStartDate.GetTime(tmTemp);
	strStartDate = tmTemp.Format("%Y/%m/%d");
	m_dtEndDate.GetTime(tmTemp);
	tmTemp += CTimeSpan(1,0,0,0);
	strEndDate = tmTemp.Format("%Y/%m/%d");

	if(!strSelPackage.IsEmpty())
	{
		bAll = false;
		strWhere.Format(_T("programId = '%s'"), strSelPackage);
	}
	else
	{
		// 패키지이름
		strPackage.Trim();
		if(!strPackage.IsEmpty())
		{
			strWhere.AppendFormat(_T(" %s lower(programId) like '%%%s%%'")
								, (strWhere.IsEmpty() ? _T("") : _T("and"))
								, strPackage.MakeLower()
								);
		}

		// 태그, 키워드
		strTagList.Trim();
		if(!strTagList.IsEmpty())
		{
			int pos = 0;
			CString strTag;
			while((strTag = strTagList.Tokenize("/", pos)) != _T(""))
			{
				//strWhere.AppendFormat(_T(" %s concat('/', lower(description), '/') like '%%/%s/%%'")
				strWhere.AppendFormat(_T(" %s description like '%%%s%%'")
									, (strWhere.IsEmpty() ? _T("") : _T("and"))
									, strTag.MakeLower()
									);
			}
		}

		// 작성자
		strRegister.Trim();
		if(!strRegister.IsEmpty())
		{
			strWhere.AppendFormat(_T(" %s lastUpdateId = '%s'")
								, (strWhere.IsEmpty() ? _T("") : _T("and"))
								, strRegister
								);
		}

		// 포함콘텐츠
		strContentsId.Trim();
		if(!strContentsId.IsEmpty())
		{
			strWhere.AppendFormat(_T(" %s programId in (select programId from ubc_contents where lower(contentsId) = lower('%s'))")
								, (strWhere.IsEmpty() ? _T("") : _T("and"))
								, strContentsId
								);
		}

		// 작성일자
		strWhere.AppendFormat(_T(" %s lastUpdateTime between CAST('%s 00:00:00' as datetime) and CAST('%s 23:59:59' as datetime)")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strStartDate
							, strEndDate
							);
		
		for(int i=0; i<eFilterMaxCnt ;i++)
		{
			CString strFieldName;
			switch(i)
			{
			case eFilterCategory  : strFieldName = _T("contentsCategory"); break; // 종류별
			case eFilterPurpose   : strFieldName = _T("purpose"); break; // 용도별
			case eFilterHostType  : strFieldName = _T("hostType"); break; // 단말타입별
			case eFilterVertical  : strFieldName = _T("vertical"); break; // 가로/세로별
			case eFilterResolution: strFieldName = _T("resolution"); break; // 해상도별
			case eFilterPublic    : strFieldName = _T("isPublic"); break; // 공개여부
			case eFilterVerify    : strFieldName = _T("isVerify"); break; // 승인여부
			}

			// not all
			strFilter[i] = GetSelectItemsStringFromList(m_lcFilter[i], strFieldName);
			if(!strFilter[i].IsEmpty())
			{
				strWhere.AppendFormat(_T(" %s %s")
									, (strWhere.IsEmpty() ? _T("") : _T("and"))
									, strFilter[i]
									);
			}
		}

		//strWhere += _T(" order by siteId, programId");
	}

	TraceLog(("%s", strWhere));

	GetEnvPtr()->RefreshPackageList(bAll, strWhere);

	POSITION pos = GetEnvPtr()->m_mapPackage.GetStartPosition();
	while(pos)
	{
		CString strKey;
		SPackageInfo4Studio* pInfo=NULL;
		POSITION curpos = pos;
		GetEnvPtr()->m_mapPackage.GetNextAssoc( pos, strKey, (void*&)pInfo);
		if(!pInfo) continue;
		if(!pInfo->bShow) continue;

		if(strSelPackage.IsEmpty())
		{
			if(!strPackage.IsEmpty() && FindNoCase(pInfo->szPackage,strPackage) < 0) continue;
			if(!strRegister.IsEmpty() && strRegister != pInfo->szUpdateUser) continue;
			if(!strFilter[eFilterPublic    ].IsEmpty() && strFilter[eFilterPublic    ].Find(_T("'") + ToString(pInfo->bIsPublic?1:0    ) + _T("'")) < 0) continue;
			if(!strFilter[eFilterVerify    ].IsEmpty() && strFilter[eFilterVerify    ].Find(_T("'") + ToString(pInfo->bIsVerify?1:0    ) + _T("'")) < 0) continue;
			if(!strFilter[eFilterCategory  ].IsEmpty() && strFilter[eFilterCategory  ].Find(_T("'") + ToString(pInfo->nContentsCategory) + _T("'")) < 0) continue;
			if(!strFilter[eFilterPurpose   ].IsEmpty() && strFilter[eFilterPurpose   ].Find(_T("'") + ToString(pInfo->nPurpose         ) + _T("'")) < 0) continue;
			if(!strFilter[eFilterHostType  ].IsEmpty() && strFilter[eFilterHostType  ].Find(_T("'") + ToString(pInfo->nHostType        ) + _T("'")) < 0) continue;
			if(!strFilter[eFilterVertical  ].IsEmpty() && strFilter[eFilterVertical  ].Find(_T("'") + ToString(pInfo->nVertical        ) + _T("'")) < 0) continue;
			if(!strFilter[eFilterResolution].IsEmpty() && strFilter[eFilterResolution].Find(_T("'") + ToString(pInfo->nResolution      ) + _T("'")) < 0) continue;
			if(strStartDate > pInfo->szUpdateTime || strEndDate < pInfo->szUpdateTime) continue;
			// 0001491: 유효기간이 지난 패키지도 열 수있도록 한다.
			//if(!pInfo->strValidationDate.IsEmpty() && pInfo->strValidationDate != _T("1970/01/01 09:00:00") && strCurDateTime > pInfo->strValidationDate) continue;

			// 태그, 키워드
			if(!strTagList.IsEmpty())
			{
				BOOL bNoFound = FALSE;
				int pos = 0;
				CString strTag;
				while((strTag = strTagList.Tokenize("/", pos)) != _T(""))
				{
					if(pInfo->szDescript.Find(strTag) < 0)
					{
						bNoFound = TRUE;
						continue;
					}
				}

				if(bNoFound) continue;
			}

			if(!strContentsId.IsEmpty() && pInfo->szDrive != STR_ENV_SERVER)
			{
				CString strIniPath;
				strIniPath.Format("%s%s%s.ini", pInfo->szDrive, UBC_CONFIG_PATH, pInfo->szPackage);

				CProfileManager objIniManager(strIniPath);
				if(strContentsId != objIniManager.GetProfileString("contents_" + strContentsId, "contentsId")) continue;
			}
		}

		int nRow = m_lcPackage.GetItemCount();

		m_lcPackage.InsertItem(nRow, pInfo->szPackage, pInfo->bModify?3:-1);
		m_lcPackage.SetItemText(nRow, eColDrive, pInfo->szDrive);
		m_lcPackage.SetItem(nRow, eColRegister, LVIF_IMAGE, NULL, strlen(pInfo->szProcID)?1:-1, 0, 0, 0);

		if(strlen(pInfo->szUpdateTime))
		{
			m_lcPackage.SetItemText(nRow, eColUpdateDate, pInfo->szUpdateTime);
		}
		else if(0 < pInfo->tmUpdateTime)
		{
			CTime tmUpdate(pInfo->tmUpdateTime);
			m_lcPackage.SetItemText(nRow, eColUpdateDate, tmUpdate.Format(STR_ENV_TIME));
		}

		// 서버에 등록되어 있고 변경된 콘텐츠 패키지인 경우
		if(strlen(pInfo->szProcID) && pInfo->bModify)
		{
			SPackageInfo4Studio* pServerInfo = GetEnvPtr()->FindPackage(pInfo->szPackage, STR_ENV_SERVER);
			if(pServerInfo)
			{
				if(pServerInfo->szUpdateTime.GetLength())
				{
					m_lcPackage.SetItemText(nRow, eColServerDate, pServerInfo->szUpdateTime);
				}
				else if(0 < pServerInfo->tmUpdateTime)
				{
					CTime tmUpdate(pServerInfo->tmUpdateTime);
					m_lcPackage.SetItemText(nRow, eColServerDate, tmUpdate.Format(STR_ENV_TIME));
				}
			}
		}

		m_lcPackage.SetItemText(nRow, eColUpdateUser, pInfo->szUpdateUser);
		m_lcPackage.SetItemText(nRow, eColSite      , pInfo->szSiteID);
		m_lcPackage.SetItemText(nRow, eDescript     , pInfo->szDescript);
		m_lcPackage.SetItemText(nRow, eCategory     , CUbcCode::GetInstance()->GetCodeName("Kind"    , pInfo->nContentsCategory));
		m_lcPackage.SetItemText(nRow, ePurpose      , CUbcCode::GetInstance()->GetCodeName("Purpose"    , pInfo->nPurpose         ));
		m_lcPackage.SetItemText(nRow, eHostType     , CUbcCode::GetInstance()->GetCodeName("HostType", pInfo->nHostType        ));
		m_lcPackage.SetItemText(nRow, eVertical     , CUbcCode::GetInstance()->GetCodeName("Direction", pInfo->nVertical        ));
		m_lcPackage.SetItemText(nRow, eResolution   , CUbcCode::GetInstance()->GetCodeName("Resolution"  , pInfo->nResolution      ));
		m_lcPackage.SetItemText(nRow, ePublic       , (pInfo->bIsPublic?LoadStringById(IDS_EEPACKAGEDLG_STR001):LoadStringById(IDS_EEPACKAGEDLG_STR002)));
		m_lcPackage.SetItemText(nRow, eVerify       , (pInfo->bIsVerify?LoadStringById(IDS_EEPACKAGEDLG_STR003):LoadStringById(IDS_EEPACKAGEDLG_STR004)));
		
		if(pInfo->tmValidationDate > 0)
		{
			m_lcPackage.SetItemText(nRow, eValidateDate , pInfo->strValidationDate);
		}

		m_lcPackage.SetItemData(nRow, (DWORD_PTR)curpos);

		COLORREF crText = RGB(0,0,0);
		COLORREF crTextBk = RGB(255,255,255);

		if(!pInfo->szProcID.IsEmpty())
		{
			crText = RGB(100,0,50);
			crTextBk = RGB(255,255,200);
		}

		m_lcPackage.SetRowColor(nRow, crTextBk, crText);
	}

	m_lcPackage.SortList(eColUpdateDate, false, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcPackage);
	m_lcPackage.SetRedraw(TRUE);
}

void CEEPackageDlg::SetParameter(CDocument* pDocument, CString szPackage, CString szDrive)
{
	m_pDocument = pDocument;
	m_szSelPackage = szPackage;
	m_szSelDrive = szDrive;	// 0000756: 같은 스케줄이 두개의 다른 드라이브에 각각 저장되어 있을때, c드라이브의 스케줄을 열어둔 상태에서 d드라이브의 스케줄이 안열립니다.
}

void CEEPackageDlg::OnBnClickedButtonFromServer()
{
	int nRow = m_lcPackage.GetRadioSelectedIndex();
	if(nRow >= 0)
	{
		POSITION pos = (POSITION)m_lcPackage.GetItemData(nRow);
		CString strKey;
		SPackageInfo4Studio* pInfo=NULL;
		GetEnvPtr()->m_mapPackage.GetNextAssoc(pos, strKey, (void*&)pInfo);
		if(LoadPackage(pInfo, TRUE))
		{
			GetDlgItem(IDOK)->EnableWindow(FALSE);
			GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
			m_bnRefresh.EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_FROM_SERVER)->EnableWindow(FALSE);
			m_lcPackage.EnableWindow(FALSE);
		}

		return;
	}

	UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG002), MB_ICONWARNING);
}

void CEEPackageDlg::OnBnClickedOk()
{
	int nRow = m_lcPackage.GetRadioSelectedIndex();

	if(nRow < 0)
	{
		UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG002), MB_ICONWARNING);
		return;
	}

	POSITION pos = (POSITION)m_lcPackage.GetItemData(nRow);
	CString strKey;
	SPackageInfo4Studio* pInfo=NULL;
	GetEnvPtr()->m_mapPackage.GetNextAssoc(pos, strKey, (void*&)pInfo);
	
	// 서버시간이 더 최신인 경우 확인과정을 거치도록 한다.
	BOOL bFromServer = FALSE;
	CString strUpdateDate = m_lcPackage.GetItemText(nRow, eColUpdateDate);
	CString strServerDate = m_lcPackage.GetItemText(nRow, eColServerDate);

	if(!strServerDate.IsEmpty() && strServerDate > strUpdateDate)
	{
		bFromServer = (UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG005), MB_YESNO) == IDYES);
	}

	if(LoadPackage(pInfo, bFromServer))
	{
		GetDlgItem(IDOK)->EnableWindow(FALSE);
		GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
		m_bnRefresh.EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_FROM_SERVER)->EnableWindow(FALSE);
		m_lcPackage.EnableWindow(FALSE);
	}
}

void CEEPackageDlg::OnBnClickedButtonRefresh()
{
	RefreshList(_T(""));
	UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG003), MB_ICONWARNING);
	SaveFilterData();
}

bool CEEPackageDlg::LoadPackage(SPackageInfo4Studio* pInfo, BOOL bFromServer/*=FALSE*/)
{
	CWaitMessageBox wait;

	if(!pInfo)
	{
		UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG001), MB_ICONERROR);
		OnCancel();
		return false;
	}

	// 서버에만 있는 콘텐츠 패키지이면 FTP로 받아온다
	if(bFromServer || pInfo->szDrive == STR_ENV_SERVER)
	{
		// Ftp로 ini 를 받아 온다
		CString szFile, szLocal, szRemote;
		szFile.Format("%s.ini", pInfo->szPackage);
		szLocal.Format("%s\\%s", GetEnvPtr()->m_szDrive, UBC_CONFIG_PATH);
		szRemote.Format("/config/");

		TraceLog(("GetFile(%s,%s,%s)", szFile, szRemote, szLocal));
		if(!GetEnvPtr()->GetFile(szFile, szRemote, szLocal, pInfo->szSiteID))
		{
			UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG004),MB_ICONERROR);
			return false;
		}
		TraceLog(("GetFile(Success)"));

		if(!bFromServer)
		{
			// 0000892: 서버에만 있는 콘텐츠 패키지를 열때 스튜디오가 죽는 문제 수정
			// 로컬 ini 파일 정보 생성
			SPackageInfo4Studio* pNewInfo = new SPackageInfo4Studio;
			*pNewInfo = *pInfo;

			// 로컬 ini 정보로 수정한다.
			pNewInfo->szDrive.Format(_T("%s\\"), GetEnvPtr()->m_szDrive);

			// 서버 ini 정보는 안보이도록 한다.
			pNewInfo->bShow = false;

			// 로컬 ini 정보를 등록한다.
			GetEnvPtr()->AddPackage(pNewInfo);
			*pInfo = *pNewInfo;
		}
	}

	TraceLog(("ChangeCurPackage(%s)", pInfo->szPackage));
	GetEnvPtr()->ChangeCurPackage(*pInfo);

	m_pThread = (CInfoImportThread*)AfxBeginThread(
								RUNTIME_CLASS(CInfoImportThread),
								THREAD_PRIORITY_NORMAL,
								0,  CREATE_SUSPENDED, NULL);
	if(m_pThread && (m_pDocument != NULL))
	{
		m_pThread->m_bAutoDelete = TRUE;
		m_pThread->SetThreadParam(this, m_pDocument, pInfo->szDrive, pInfo->szPackage);
		m_pThread->ResumeThread();

		if(!m_pNotiDlg){
			m_pNotiDlg = new CNotifySaveDlg;
			m_pNotiDlg->SetMode(CNotifySaveDlg::eLoadPackage);
			m_pNotiDlg->Create(IDD_NOTIFY_SAVE_DLG, this);
		}
		m_pNotiDlg->ShowWindow(SW_NORMAL);
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG001), MB_ICONERROR);
		OnCancel();
	}

	return true;
}

LRESULT CEEPackageDlg::OnImportComplete(WPARAM wParam, LPARAM lParam)
{
	TraceLog(("OnImportComplete(%d,%d)",wParam,lParam));
	if(m_pNotiDlg){
		m_pNotiDlg->ShowWindow(SW_HIDE);
		delete m_pNotiDlg;
		m_pNotiDlg = NULL;
	}

	if(wParam != 1){
		UbcMessageBox(LoadStringById(IDS_EEPACKAGEDLG_MSG001), MB_ICONERROR);
	}

	OnOK();
	return 0;
}

void CEEPackageDlg::InitFilterListCtrl()
{
	for(int i=0; i<eFilterMaxCnt ;i++)
	{
		CString strColName;
		switch(i)
		{
		case eFilterCategory  : strColName = LoadStringById(IDS_EEPACKAGEDLG_LST009); break;
		case eFilterPurpose   : strColName = LoadStringById(IDS_EEPACKAGEDLG_LST010); break;
		case eFilterHostType  : strColName = LoadStringById(IDS_EEPACKAGEDLG_LST011); break;
		case eFilterVertical  : strColName = LoadStringById(IDS_EEPACKAGEDLG_LST012); break;
		case eFilterResolution: strColName = LoadStringById(IDS_EEPACKAGEDLG_LST013); break;
		case eFilterPublic    : strColName = LoadStringById(IDS_EEPACKAGEDLG_LST014); break;
		case eFilterVerify    : strColName = LoadStringById(IDS_EEPACKAGEDLG_LST015); break;
		}

		CRect rect;
		m_lcFilter[i].GetClientRect(rect);
		m_lcFilter[i].SetExtendedStyle(m_lcFilter[i].GetExtendedStyle() | LVS_EX_FULLROWSELECT);
		m_lcFilter[i].SetSelTextColor(RGB(0,0,0));
		m_lcFilter[i].SetSelBackColor(RGB(220,220,220));
		m_lcFilter[i].InsertColumn(0, strColName, LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
		m_lcFilter[i].InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
		m_lcFilter[i].HideScrollBars(SB_HORZ);
	}
}

void CEEPackageDlg::OnBnClickedBnRegister()
{
	CString strSelUserId;
	CString strSelUserName;

	GetSelectUsers(strSelUserId, strSelUserName);

	SetDlgItemText(IDC_EB_REGISTER, strSelUserId);
}

void CEEPackageDlg::SaveFilterData()
{
	CString strPackage     ;
	CString strTag         ;
	CString strRegister    ;
	CString strContentsName;
	CString strContentsId  ;
	CString strStartDate   ;
	CString strEndDate     ;

	GetDlgItemText(IDC_EB_PACKAGE , strPackage     );
	GetDlgItemText(IDC_EB_TAG     , strTag         );
	GetDlgItemText(IDC_EB_REGISTER, strRegister    );
	if(m_cbxIncludedContents.GetCount() > 2)
	{
		m_cbxIncludedContents.GetLBText(1, strContentsName);
		strContentsId = m_strFilterContentsId;
	}
	SYSTEMTIME stmTemp;
	m_dtStartDate.GetTime(&stmTemp);
	CTime tmTemp = CTime(stmTemp);
	strStartDate = tmTemp.Format("%Y/%m/%d");
	m_dtEndDate.GetTime(&stmTemp);
	tmTemp = CTime(stmTemp);
	strEndDate = tmTemp.Format("%Y/%m/%d");
	
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	for(int i=0; i<eFilterMaxCnt ;i++)
	{
		CString strKey;
		switch(i)
		{
		case eFilterCategory  : strKey = _T("Category"); break; // 종류별
		case eFilterPurpose   : strKey = _T("Purpose"); break; // 용도별
		case eFilterHostType  : strKey = _T("HostType"); break; // 단말타입별
		case eFilterVertical  : strKey = _T("Vertical"); break; // 가로/세로별
		case eFilterResolution: strKey = _T("Resolution"); break; // 해상도별
		case eFilterPublic    : strKey = _T("IsPublic"); break; // 공개여부
		case eFilterVerify    : strKey = _T("IsVerify"); break; // 승인여부
		}

		CString strList;
		CStringArray ar;
		if(GetSelectItemsList(m_lcFilter[i], ar) > 0)
		{
			for(int j=0; j<ar.GetCount() ;j++)
			{
				strList += ar[j] + _T(",");
			}
		}
		WritePrivateProfileString("PACKAGE-FILTER", strKey, strList, szPath);
	}

	WritePrivateProfileString("PACKAGE-FILTER", "Package"     , strPackage     , szPath);
	WritePrivateProfileString("PACKAGE-FILTER", "Tag"         , strTag         , szPath);
	WritePrivateProfileString("PACKAGE-FILTER", "Register"    , strRegister    , szPath);
	WritePrivateProfileString("PACKAGE-FILTER", "ContentsName", strContentsName, szPath);
	WritePrivateProfileString("PACKAGE-FILTER", "ContentsId"  , strContentsId  , szPath);
//	WritePrivateProfileString("PACKAGE-FILTER", "StartDate"   , strStartDate   , szPath);
//	WritePrivateProfileString("PACKAGE-FILTER", "EndDate"     , strEndDate     , szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "IsPublic"    , ToString(nFilter[eFilterPublic    ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "IsVerify"    , ToString(nFilter[eFilterVerify    ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "Category"    , ToString(nFilter[eFilterCategory  ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "Purpose"     , ToString(nFilter[eFilterPurpose   ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "HostType"    , ToString(nFilter[eFilterHostType  ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "Vertical"    , ToString(nFilter[eFilterVertical  ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "Resolution"  , ToString(nFilter[eFilterResolution]), szPath);
}

void CEEPackageDlg::LoadFilterData()
{
	CString strPackage     ;
	CString strTag         ;
	CString strRegister    ;
	CString strContentsName;
	CString strContentsId  ;
	CString strStartDate   ;
	CString strEndDate     ;

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("PACKAGE-FILTER", "Package"     , "", buf, 1024, szPath); strPackage      = buf;
	GetPrivateProfileString("PACKAGE-FILTER", "Tag"         , "", buf, 1024, szPath); strTag          = buf;
	GetPrivateProfileString("PACKAGE-FILTER", "Register"    , "", buf, 1024, szPath); strRegister     = buf;
	GetPrivateProfileString("PACKAGE-FILTER", "ContentsName", "", buf, 1024, szPath); strContentsName = buf;
	GetPrivateProfileString("PACKAGE-FILTER", "ContentsId"  , "", buf, 1024, szPath); strContentsId   = buf;
//	GetPrivateProfileString("PACKAGE-FILTER", "StartDate"   , "", buf, 1024, szPath); strStartDate    = buf;
//	GetPrivateProfileString("PACKAGE-FILTER", "EndDate"     , "", buf, 1024, szPath); strEndDate      = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "Category"    , "", buf, 1024, szPath); strFilter[eFilterCategory  ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "Purpose"     , "", buf, 1024, szPath); strFilter[eFilterPurpose   ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "HostType"    , "", buf, 1024, szPath); strFilter[eFilterHostType  ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "Vertical"    , "", buf, 1024, szPath); strFilter[eFilterVertical  ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "Resolution"  , "", buf, 1024, szPath); strFilter[eFilterResolution] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "IsPublic"    , "", buf, 1024, szPath); strFilter[eFilterPublic    ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "IsVerify"    , "", buf, 1024, szPath); strFilter[eFilterVerify    ] = buf;

	SetDlgItemText(IDC_EB_PACKAGE , strPackage     );
	SetDlgItemText(IDC_EB_TAG     , strTag         );
	SetDlgItemText(IDC_EB_REGISTER, strRegister    );
	
	if(m_cbxIncludedContents.GetCount() > 2)
	{
		m_cbxIncludedContents.DeleteString(1);
		m_strFilterContentsId = _T("");
	}

	if(!strContentsId.IsEmpty())
	{
		m_strFilterContentsId = strContentsId;
		m_cbxIncludedContents.InsertString(1, strContentsName);
		m_cbxIncludedContents.SetCurSel(1);
	}

	COleDateTime tmTemp;
	CTime tmCur = CTime::GetCurrentTime();
	if(strStartDate.IsEmpty())
	{
		int nAddMonth = -2;
		int nTotalMonth = tmCur.GetYear() * 12 + tmCur.GetMonth() + nAddMonth;

		CTime tmStartDate  = CTime( nTotalMonth/12 - ((nTotalMonth%12) == 0 ? 1 : 0)
								, ((nTotalMonth%12) == 0 ? 12 : (nTotalMonth%12))
								, tmCur.GetDay()
								, tmCur.GetHour()
								, tmCur.GetMinute()
								, tmCur.GetSecond()
								);
		strStartDate = tmStartDate.Format("%Y/%m/01");
	}

	tmTemp.ParseDateTime(strStartDate);
	m_dtStartDate.SetTime(tmTemp);

	if(strEndDate.IsEmpty())
	{
		strEndDate = tmCur.Format("%Y/%m/%d");
	}

	tmTemp.ParseDateTime(strEndDate);
	m_dtEndDate.SetTime(tmTemp);

	for(int i=0; i<eFilterMaxCnt ;i++)
	{
		CString strKey;
		switch(i)
		{
		case eFilterCategory  : strKey = _T("Category"); break; // 종류별
		case eFilterPurpose   : strKey = _T("Purpose"); break; // 용도별
		case eFilterHostType  : strKey = _T("HostType"); break; // 단말타입별
		case eFilterVertical  : strKey = _T("Vertical"); break; // 가로/세로별
		case eFilterResolution: strKey = _T("Resolution"); break; // 해상도별
		case eFilterPublic    : strKey = _T("IsPublic"); break; // 공개여부
		case eFilterVerify    : strKey = _T("IsVerify"); break; // 승인여부
		}

		GetPrivateProfileString("PACKAGE-FILTER", strKey, "", buf, 1024, szPath);
		CString strList = buf;

		int iStart = 0;
		CString strValue;
		while(!(strValue = strList.Tokenize(_T(","), iStart)).IsEmpty())
		{
			int nFilter = _ttoi(strValue);
			int nIndex = CUbcCode::GetInstance()->FindListCtrl(m_lcFilter[i], nFilter);
			if(nIndex >= 0)
			{
				m_lcFilter[i].SetCheck(nIndex, TRUE);
			}
		}
	}
}

void CEEPackageDlg::OnCbnSelchangeCbIncludedContents()
{
	CString strContentsId;
	CString strContentsName;

	int idx = m_cbxIncludedContents.GetCurSel();
	int count = m_cbxIncludedContents.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxIncludedContents.DeleteString(1);
			m_strFilterContentsId = _T("");
		}
	}
	else if(idx == count-1 )
	{
		if(GetSelectContents(GetEnvPtr()->m_strCustomer, strContentsId, strContentsName))
		{
			if(count > 2)
			{
				m_strFilterContentsId = _T("");
				m_cbxIncludedContents.DeleteString(1);
				m_cbxIncludedContents.SetCurSel(0);
			}

			m_strFilterContentsId = strContentsId;
			m_cbxIncludedContents.InsertString(1, strContentsName);
			m_cbxIncludedContents.SetCurSel(1);
		}
		else
		{
			m_cbxIncludedContents.SetCurSel(count-2);
		}
	}
}

int CEEPackageDlg::GetSelectItemsList(CUTBListCtrlEx& lc, CStringArray& ar)
{
	int count = lc.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if(lc.GetCheck(i))
		{
			ar.Add(ToString(lc.GetItemData(i)));
		}
	}

	return ar.GetCount();
}

CString CEEPackageDlg::GetSelectItemsStringFromList(CStringArray& ar, CString strField)
{
	CString strFilter;

	int count = ar.GetCount();
	for(int i=0; i<count; i++)
	{
		strFilter.AppendFormat("'%s',", ar.GetAt(i));
	}

	strFilter.Trim(_T(','));

	if(strFilter.GetLength() > 0)
	{
		CString tmp;
		tmp.Format("%s in (", strField);

		strFilter = tmp + strFilter + ")";
	}

	return strFilter;
}

CString CEEPackageDlg::GetSelectItemsStringFromList(CUTBListCtrlEx& lc, CString strField)
{
	CStringArray ar;
	GetSelectItemsList(lc, ar);
	return GetSelectItemsStringFromList(ar, strField);
}
