#pragma once

//#include "ProgressCtrlEx.h"
#include "Schedule.h"
#include "NetworkHostDlg.h"
#include "afxwin.h"
#include "SimpleFtp.h"
#include "common/HoverButton.h"
#include "libFileTransfer/FTClient.h"

// CFtpUploadDlg 대화 상자입니다.

class CFtpUploadDlg : public CDialog, public ISpFtp
{
	DECLARE_DYNAMIC(CFtpUploadDlg)

public:
	CFtpUploadDlg(bool bUpload, bool bReserve=false, CWnd* pParent=NULL);
	CFtpUploadDlg(CWnd* pParent, CPtrArray& aryHostInfo, CString strArgs, bool bA, bool bB, bool bAll);

	virtual ~CFtpUploadDlg();
	virtual BOOL OnInitDialog();
	CString GetErrorMsg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FTP_UPLOAD_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CWnd*				m_pParentWnd;					///<부모 윈도우 포인터
	CONTENTS_INFO_MAP	m_mapContents;					///<콘텐츠 파일의 중복된 복사를 방지를 위한 맵(Key:파일의 경로, Val:CONTENTS_INFO*) 
	int					m_nIndexHostInfo;				///<현재 upload를 진행중인 HostInfo 배열의 인덱스
	ULONGLONG			m_ulTotalSize;					///<Upload를 하려는 콘텐츠 파일의 총 크기
	CString				m_strArgs;
	bool				m_DisplalySide[2];

	CButton				m_btnCancel;

	CWinThread*			m_pThread;						///<FTP upload를 진행하는 thread
	CInternetSession	m_internetSession;				///<Internet session 포인터
	CFtpConnection*		m_pFtpConnection;				///<FTP connection을 갖는 포인터

	static UINT	GetFtpConnectionThread(LPVOID param);	///<Ftp 연결을 진행하는 thread
	static UINT	FtpUploadThread(LPVOID param);			///<Contents파일을 upload하는 thread

	ULONGLONG	GetTotalFileSize();						///<upload하려는 contents파일의 총 크기를 계산한다
	bool		ConnectFtp(int nIndex);					///<지정된 인덱스의 호스트에 연결을 시도한다
	void		Start(bool bShowWindow=false);			///<Upload 과정을 시작한다
	CString		MakeErrorMsg(CNetDeviceInfo* pclsNetInfo, int nErrorCode, LPARAM lParam = 0);	///<장애 타입에 따른 에러 메시지를 만들어 준다

	//////////////////////////////////////
	// Studio PE 관련 FTP 구조변경을 위한 메소드 - 구현석
	CString m_szErrorReporter;
	CFTClient* m_pClient;
	BOOL m_bForceStop;

	void RunFTP();
	void RunFileTransfer(CFtpUploadDlg* pDlg);
	void MakeErrorReport(LPCTSTR szTitle, LPCTSTR szDescript);
	static UINT RunFTPThread(LPVOID param);

	//////////////////////////////////////
	// Studio EE 관련 코드 추가 - 구현석
public:
	CONTENTS_INFO_MAP* GetContentsInfoMap();
	virtual void InvokeSize(ULONGLONG, DWORD);

protected:
	bool m_bAll;
	bool m_bUpload;
	bool m_bReserve;

	void Upload();
	void Download();
	static UINT UploadProc(LPVOID param);
	static UINT DownloadProc(LPVOID param);
//////////////////////////////////////

public:
	CString				m_strPackageFilePath;			///<Upload를 하려는 Package의 ini파일 전체 경로
	CString				m_strPackageName;				///<Upload를 하려는 Package의 이름
	CPtrArray			m_aryHostInfo;					///<Upload를 하려는 대상 호스트 정보를 갖는 배열
	CPtrArray			m_aryFailHostInfo;				///<Upload 결과 실패한 호스트 정보 배열

	CStatic				m_staticStatus;					///<Upload 진행 상태를 표시하는 Static
	CStatic				m_staticHost;					///<Upload하는 대상 Host를 표시하는 Static
	CProgressCtrlEx		m_pbarCurrent;					///<현재 진행중인 상태를 표시하는 프로그래스바
	CProgressCtrlEx		m_pbarTotal;					///<전체 진행중인 상태를 표시하는 프로그래스바

	bool	m_bProcessThread;							///<User cancel을 위한 thread 진행여부 flag
	CMutex	m_mutex;									///<thread  동기화

	void	ClearNetDeviceArray(void);					///<Network host 정보 배열을 정리한다.
	LRESULT OnCompleteFtpConnection(WPARAM wParam, LPARAM lParam);	///<FTP 연결의 결과 이벤트 처리
	LRESULT OnCompleteFtpUpload(WPARAM wParam, LPARAM lParam);		///<FTP upload 결과 이벤트 처리
	afx_msg void OnBnClickedButtonCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//afx_msg LRESULT OnDisplayStatus(WPARAM wParam, LPARAM lParam);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
};

typedef struct {
	CString		title;
	CString		current_text;
	int			current_pos;
	CString		total_text;
	int			total_pos;
} DISPLAY_PARAM;

typedef struct {
	CWnd*				pParentWnd;
	CWnd*				pWnd;
	CInternetSession*	pInternetSession;
	CFtpConnection**	ppFtpConnection;
	CNetDeviceInfo*		pHostInfo;
} GET_FTP_CONNECTION_PARAM;

typedef struct {
	CWnd*				pParentWnd;
	CWnd*				pWnd;
	CStatic*			pStatic;
	CProgressCtrlEx*	pbarCurrent;
	CProgressCtrlEx*	pbarTotal;
	CFtpConnection*		pFtpConnection;
	ULONGLONG			ulTotalSize;
//	CONTENTS_INFO_MAP*	mapContents;
} FTP_UPLOAD_PARAM;

