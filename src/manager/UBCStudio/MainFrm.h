// MainFrm.h : interface of the CMainFrame class
//
#pragma once

#include "resource.h"
#include "UBCStudioDoc.h"
//#include "OverlayInterface.h"
#include "LoginInfoDlg.h"
#include "MainFrmBitmapBG.h"

class CNotifySaveDlg;
class CRunPreviewThread;
class CConfigSaveThread;
class CFtpUploadDlg;

//Package이 열려있는 상태에서 저장기능을 수행할 때
//저장 이후에 열기, 새로만들기, 프로그램 종료 등을 수행할지를
//나타내는 상태 값
enum E_STATE_SAVE
{
	E_STATE_SAVE_NONE		= 0,		//저장후에 아무것도 하지 않음
	E_STATE_SAVE_NEW,					//저장후에 새로만들기 기능을 함
	E_STATE_SAVE_OPEN,					//저장후에 open(import)기능을 함
	E_STATE_SAVE_CLOSE,					//저장후에 열려있는 package를 닫는다.(메모리에서 열려있는 scheduel 정보를 모두 삭제)
	E_STATE_SAVE_EXIT					//저장후에 프로그램을 종료 함
};


class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:

// Operations
public:
	CWnd*				GetChildFrame(UINT nID);

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CMainFrmBitmapBG	m_wndBitmapBG;
//	CStatic		m_sttToolBar;
//	CFont		m_TextFont;
//	CLoginInfoDlg m_LoginInfoDlg;

// Generated message map functions
protected:
	// Modified by 정운형 2008-12-24 오후 2:47
	// 변경내역 :  캡션기능 추가 - 버그수정
	//virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	// Modified by 정운형 2008-12-24 오후 2:47
	// 변경내역 :  캡션기능 추가 - 버그수정
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	DECLARE_MESSAGE_MAP()

	CUBCStudioDoc		m_document;
	//COverlayInterface	m_interface;
	//bool				m_bModified;
	bool				m_bRecovery;

	CNotifySaveDlg*		m_pNotiDlg;			///<편성관리 저장, preview기능 수행중 config 파일의 저장을 알리는 대화상자 
	CFtpUploadDlg*		m_pdlgFtp;
	CRunPreviewThread*	m_pthreadPreview;		///<Preview 기능을 수행하는 thread
	CConfigSaveThread*	m_pThreadConfigSave;	///<Save as 기능에서 편성관리 정보를 config 파일에 저장하는 thread
	//bool				m_bTerminate;			///<프로그램을 종료시키는지 여부의 플래그 값
//	bool				m_bNew;					///<새로 package 만들기를 하였는지 여부
//	bool				m_bOpened;				///<Package을 import(open)한 상태인지 여부
	short				m_sStateSave;			///<저장후에 다른 기능을 수행하는지 나타내는 상태 값

	void				OnNewFrame(UINT nID);

	bool				CheckRegist(void);					///<정식등록되었는지 사용기간 확인
	void				WriteRegistFile(CString strKey);	///<Regist 파일 쓰기

	void InitToolbarForPE();
	void InitToolbarForEE();
	bool CheckLicense();
	void ExportForPE();
	void ExportForEE();

public:
	CPtrArray			m_aryLocalHostInfo;							///<locla system에 존재하는 host 정보들의 배열
	CMapStringToString	m_mapVolumeToLabel;
	CMapStringToString	m_mapDriveToLabel;

	void TerminateProcess(CString processName);
	BOOL IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs);
	void KillProcess(DWORD dwProcessId);
	BOOL LoadPackage(LPCTSTR, int, CString strDrive=_T(""));

	CStringArray m_arLatestPackageName[2];
	bool SetLatestPackageMenu(CMenu* pPopup);

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnInfoImport();
	afx_msg void OnSave();
	afx_msg void OnClose();
	afx_msg void OnInfoNew();
	afx_msg void OnSaveAs();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//afx_msg void OnRegistKey();
	afx_msg void OnNetworkExport();
	afx_msg void OnPackageClose();
	afx_msg void OnPackageDelete();
	afx_msg void OnEditNetworkHost();
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);

	afx_msg LRESULT OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFrameInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTemplateListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnContentsListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAllInfoChanged(WPARAM wParam, LPARAM lParam);
	//afx_msg void OnInfoExport();
	afx_msg LRESULT OnCompleteFileCopy(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFTPUpload(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnPreviewComplete(WPARAM wParam, LPARAM lParam);	///<complete preview message handler
	afx_msg LRESULT	OnConfigSaveComplete(WPARAM wParam, LPARAM lParam);	///<complete config save message handler
	
	void	RunPreview(CString strTemplateID);							///<Run preview browser
	CString	GetHostName(void);											///<설정된 호스트의 이름을 반환한다.
	CString	GetSourceDirive(void);										///<설정된 드라이브 문자열을 반환한다.

	void	ClearLocalHostInfoArray(void);								///<locla host info 배열을 정리한다
	void	MakeLocalHostInfoArray(void);								///<locla host info 배열을 만든다
	BOOL	_GetProcessVolume(HANDLE hVol, char* lpBuf, int iBufSize);
	void	GetProcessVolume();
	void	InitDriveLabel();

	afx_msg void OnUpdateSave(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSaveAs(CCmdUI *pCmdUI);
	afx_msg void OnUpdateNetworkExport(CCmdUI *pCmdUI);
	afx_msg void OnUpdateLayoutManage(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePackageManage(CCmdUI *pCmdUI);
	afx_msg BOOL OnToolTipText(UINT nID, NMHDR* pNMHDR, LRESULT *pResult);
	afx_msg void OnFileCleaner();
	afx_msg void OnExplorer();
	afx_msg void OnFileRecovery();
	afx_msg void OnFileReservation();
	afx_msg void OnUnusedContentsList();
	afx_msg LRESULT OnImportComplete(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnLatestPackage(UINT nID);
protected:
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
public:
	afx_msg void OnUpdateAutoUpdate(CCmdUI *pCmdUI);
	afx_msg void OnAutoUpdate();
	afx_msg void OnContentsManager();
	afx_msg void OnConfiguration();
	afx_msg void OnFileLoadFromTemporary();
	afx_msg void OnUpdateContentsManager(CCmdUI *pCmdUI);
	afx_msg void OnUpdateInfoImport(CCmdUI *pCmdUI);
	afx_msg void OnUpdateInfoNew(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePackageDelete(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFileRecovery(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFileLoadFromTemporary(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePackageClose(CCmdUI *pCmdUI);
	afx_msg void OnFileNotification();
	afx_msg void OnChangePackageProperty();

	HWND getWHandle(unsigned long pid);
	int StrongSecurity();
	static UINT	StrongSecurityThread(LPVOID param);

	afx_msg void OnUpdateChangePackageProperty(CCmdUI *pCmdUI);
	afx_msg void OnToolsAutoencoding();
};