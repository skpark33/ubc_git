#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "common/HoverButton.h"
#include "NetworkHostDlg.h"
//#include "CheckListCtrl.h"
#include "common\utblistctrl.h"

// CNetworkHostManageDlg 대화 상자입니다.

class CNetworkHostManageDlg : public CDialog
{
	DECLARE_DYNAMIC(CNetworkHostManageDlg)

	static int CALLBACK CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

public:
	CNetworkHostManageDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CNetworkHostManageDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NETWORK_HOST_MANAGE_DLG };
	enum {eCheck, E_HOST, E_DESCRIPT, E_IPADDR, E_FTPPORT, E_SVRPORT, E_SDTIME, E_WSDTIME, E_MACADDR, E_DISPLYCNT, E_USRCREATE};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CImageList		m_imgCheck;
	CUTBListCtrl	m_lcList;			///<호스트 정보를 나타내는 리스트 컨트롤
//	CHoverButton	m_btnRefresh;		///<호스트 정보를 새로 검색하는 Refresh 버튼
//	CHoverButton	m_btnAdd;			///<새로운 host를 추가하는 Add 버튼
//	CHoverButton	m_btnModify;		///<host 정보를 수정하는 modify 버튼
//	CHoverButton	m_btnDelete;		///<선택된 host 정보를 삭제하는 delete 버튼
//	CHoverButton	m_btnOK;
	CPtrArray		m_aryNetDevice;		///<검색된 호스트 정보를 갖는 배열
	int				m_nFindHostType;

	void			InitNetListCtrl();							///<호스트 list 컨트롤의 데이터를 설정
	void			ScanNetHost(int nType);						///<로컬 네트워크상의 호스트를 조회하여 ini 파일에 기록한다
	void			LoadNetDeviceInfo(void);					///<ini파일로부터 호스트들의 정보를 읽는다
	void			WriteDeviceInfo(CNetDeviceInfo& clsHost);	///<호스트의 정보를 ini파일에 기록한다
	void			DeleteDeviceInfo(CString strHostName);		///<ini 파일에서 호스트 정보를 삭제한다
	void			ClearNetDeviceArray(void);					///<호스트 정보를 갖는 배열을 비운다

	afx_msg void OnBnClickedRefreshBtn();
	afx_msg void OnBnClickedAddBtn();
	afx_msg void OnBnClickedModifyBtn();
	afx_msg void OnBnClickedDeleteBtn();
	afx_msg void OnBnClickedOk();
	
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOnBtn();
	afx_msg void OnBnClickedOffBtn();
	afx_msg void OnBnClickedRebootBtn();
	afx_msg void OnLvnColumnclickHostList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkHostList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnRemote();
};
