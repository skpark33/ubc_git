#pragma once
#include "afxwin.h"
#include "common/HoverButton.h"

enum
{
	E_CONTENTS_COPY_USE_EXIST	= 0,
	E_CONTENTS_COPY_OVERWRITE,
	E_CONTENTS_COPY_RENAME,
	E_CONTENTS_COPY_MODALDLG
};


// CContentsOverwriteDlg 대화 상자입니다.

class CContentsOverwriteDlg : public CDialog
{
	DECLARE_DYNAMIC(CContentsOverwriteDlg)

public:
	CContentsOverwriteDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CContentsOverwriteDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CONTENTS_OVERWRITE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;
	CHoverButton	m_btnSourcePreview;
	CHoverButton	m_btnTargetPreview;
	CStatic			m_staticSourcePath;
	CStatic			m_staticTargetPath;
	CStatic			m_staticSourceSize;
	CStatic			m_staticTargetSize;
	CStatic			m_staticSourceTime;
	CStatic			m_staticTargetTime;
	CEdit			m_edtContentsName;
	CStatic			m_iconSource;
	CStatic			m_iconTarget;
	CStatic			m_staticSourceName;
	CStatic			m_staticTargetName;
	short			m_sContentsCopy;		///<Contents 파일을 overwrite 할지등의 방법
	CString			m_strOrgContentsName;	///<수정전 콘텐츠 파일의 이름
	CString			m_strNewContentsName;	///<수정된 콘텐츠 파일의 이름
	CONTENTS_INFO*	m_pinfoSource;			///<Source 파일의 경로
	CONTENTS_INFO*	m_pinfoTarget;			///<Target 파일의 경로
	CImageList		m_imgContentsList;		///<Contents 타입을 나타내는이미지 리스트
	CPoint			m_ptSourceIcon;			///<Source icon을 표시하는 영역
	CPoint			m_ptTargetIcon;			///<Target icon을 표시하는 영역
	BOOL			m_bAllOfFiles;

	void	SetContentsInfo(CONTENTS_INFO* infoSource, CONTENTS_INFO* infoTarget);	///<Source와 Target의 콘텐츠 정보를 설정한다.
	void	MakeAvailableName(void);												///<사용이 가능한 파일이름을 만들어 준다

	afx_msg void OnBnClickedOverwriteRadio();
	afx_msg void OnBnClickedRenameRadio();
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedNocopyRadio();
	afx_msg void OnBnClickedSourcePreviewBtn();
	afx_msg void OnBnClickedTargetPreviewBtn();
	afx_msg void OnPaint();
};
