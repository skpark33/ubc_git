// ExportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "ExportDlg.h"


// CExportDlg dialog

IMPLEMENT_DYNAMIC(CExportDlg, CDialog)

CExportDlg::CExportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExportDlg::IDD, pParent)
{
	m_nCheckA = 1;
	m_nCheckB = 0;
	m_nMonitorCnt = 0;

	m_nNetMode = false;
	m_szComment = "";
}

CExportDlg::~CExportDlg()
{
}

void CExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_EXPORT_INFO, m_sttComment);
}


BEGIN_MESSAGE_MAP(CExportDlg, CDialog)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK_EXPORT_MONITERA, &CExportDlg::OnBnClickedCheckExportMonitera)
	ON_BN_CLICKED(IDC_CHECK_EXPORT_MONITERB, &CExportDlg::OnBnClickedCheckExportMoniterb)
END_MESSAGE_MAP()


// CExportDlg message handlers

BOOL CExportDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	CButton* pCheckA = (CButton*)GetDlgItem(IDC_CHECK_EXPORT_MONITERA);
	CButton* pCheckB = (CButton*)GetDlgItem(IDC_CHECK_EXPORT_MONITERB);

	if(m_nMonitorCnt < 2){
		pCheckA->SetCheck(1);
		pCheckB->SetCheck(0);
		pCheckB->EnableWindow(FALSE);
	}else{
		pCheckA->SetCheck(1);
	}

	m_nCheckA = pCheckA->GetCheck();
	m_nCheckB = pCheckB->GetCheck();

	if(!m_szComment.IsEmpty())
		m_sttComment.SetWindowText(m_szComment);

	//if(m_nNetMode){
	//	GetDlgItem(IDOK)->SetWindowText("OK");
	//	GetDlgItem(IDCANCEL)->ShowWindow(SW_HIDE);

	//	CRect rc;
	//	GetClientRect(&rc);
	//	int nCenter = rc.Width()/2;

	//	GetDlgItem(IDOK)->GetClientRect(&rc);
	//	GetDlgItem(IDOK)->ClientToScreen(&rc);
	//	ScreenToClient(&rc);
	//	GetDlgItem(IDOK)->MoveWindow(nCenter-rc.Width()/2, rc.top, rc.Width(), rc.Height());
	//}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CExportDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnClose();
}

void CExportDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CExportDlg::SetComment(LPCTSTR str)
{
	m_szComment = str;
}

void CExportDlg::SetNetworkMode(bool bMode)
{
	m_nNetMode = bMode;
}

void CExportDlg::SetMonitorCnt(int nCnt)
{
	m_nMonitorCnt = nCnt;
}
void CExportDlg::OnBnClickedCheckExportMonitera()
{
	CButton* pCheckA = (CButton*)GetDlgItem(IDC_CHECK_EXPORT_MONITERA);
	CButton* pCheckB = (CButton*)GetDlgItem(IDC_CHECK_EXPORT_MONITERB);

	if(m_nMonitorCnt < 2){
		pCheckA->SetCheck(1);
	}else{
		if(pCheckA->GetCheck() == 0 && pCheckB->GetCheck() == 0)
			pCheckA->SetCheck(1);
	}

	m_nCheckA = pCheckA->GetCheck();
	m_nCheckB = pCheckB->GetCheck();
}

void CExportDlg::OnBnClickedCheckExportMoniterb()
{
	CButton* pCheckB = (CButton*)GetDlgItem(IDC_CHECK_EXPORT_MONITERB);
	CButton* pCheckA = (CButton*)GetDlgItem(IDC_CHECK_EXPORT_MONITERA);

	if(m_nMonitorCnt < 2){
		pCheckB->SetCheck(0);
	}else{
		if(pCheckA->GetCheck() == 0 && pCheckB->GetCheck() == 0)
			pCheckB->SetCheck(1);
	}

	m_nCheckA = pCheckA->GetCheck();
	m_nCheckB = pCheckB->GetCheck();
}

void CExportDlg::GetMonitor(int &nCheckA, int &nCheckB)
{

	nCheckA = m_nCheckA;
	nCheckB = m_nCheckB;
}