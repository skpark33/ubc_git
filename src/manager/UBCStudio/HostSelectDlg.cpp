// HostSelectDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "HostSelectDlg.h"
#include "LocalHostInfo.h"
#include "Enviroment.h"
#include "common/libProfileManager/ProfileManager.h"

// CHostSelectDlg 대화 상자입니다.
IMPLEMENT_DYNAMIC(CHostSelectDlg, CDialog)

CHostSelectDlg::CHostSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHostSelectDlg::IDD, pParent)
	, m_strSelectedHostName(_T(""))
	, m_bSelect(false)
	, m_nWidthTop(0)
	, m_nHeightTop(0)
	, m_nWidthBottom(0)
	, m_nHeightBottom(0)
	, m_pryLocalHostInfo(NULL)
	, m_strSelectedDrive(_T(""))
	, m_pclsDoc(NULL)
	, m_pThreadImport(NULL)
	, m_frmParent(NULL)
	, m_bNew(false)
{

}

CHostSelectDlg::~CHostSelectDlg()
{
	m_imgGif.Stop();
	m_imgGif.UnLoad();
}

void CHostSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_HOST_LIST, m_listHost);
	DDX_Text(pDX, IDC_HOST_NAME_EDT, m_strSelectedHostName);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_HOST_NAME_EDT, m_edtHostName);
	DDX_Control(pDX, IDC_DELETE, m_btnDelete);
}


BEGIN_MESSAGE_MAP(CHostSelectDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CHostSelectDlg::OnBnClickedOk)
	ON_NOTIFY(NM_CLICK, IDC_HOST_LIST, &CHostSelectDlg::OnNMClickHostList)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDCANCEL, &CHostSelectDlg::OnBnClickedCancel)
	ON_MESSAGE(WM_INFOIMPORT_COMPELETE, OnImportComplete)
	ON_NOTIFY(NM_DBLCLK, IDC_HOST_LIST, &CHostSelectDlg::OnNMDblclkHostList)
	ON_BN_CLICKED(IDC_DELETE, &CHostSelectDlg::OnBnClickedDelete)
END_MESSAGE_MAP()


// CHostSelectDlg 메시지 처리기입니다.

void CHostSelectDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strSelectedHostName == "")
	{
		UbcMessageBox(LoadStringById(IDS_HOSTSELECTDLG_MSG001), MB_ICONWARNING);
		return;
	}//if

	m_btnOK.EnableWindow(FALSE);
	m_btnCancel.EnableWindow(FALSE);

	SPackageInfo4Studio stNewInfo;
	stNewInfo.szDrive = m_strSelectedDrive;
	stNewInfo.szPackage = m_strSelectedHostName;

	//if(!GetEnvPtr()->FindPackage(pNewInfo->szPackage, pNewInfo->szDrive, pNewInfo))
	//{
	//	CString strKey = pNewInfo->szDrive + _T("/") + pNewInfo->szPackage;
	//	strKey.MakeLower();
	//	GetEnvPtr()->m_mapPackage[strKey] = pNewInfo;
	//}

	GetEnvPtr()->ChangeCurPackage(stNewInfo);

	m_pThreadImport = (CInfoImportThread*)AfxBeginThread(
								RUNTIME_CLASS(CInfoImportThread),
								THREAD_PRIORITY_NORMAL,
								0,  CREATE_SUSPENDED, NULL);
	if(m_pThreadImport && (m_pclsDoc != NULL))
	{
		m_pThreadImport->m_bAutoDelete = TRUE;
		if(!m_bNew)
		{
			m_pThreadImport->SetThreadParam(this, m_pclsDoc, m_strSelectedDrive, m_strSelectedHostName);
		}
		else
		{
			m_pThreadImport->SetThreadParam(this, m_pclsDoc, m_strSelectedDrive, SAMPLE_FILE_KEY + m_strSelectedHostName);
		}//if
		m_pThreadImport->ResumeThread();
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_HOSTSELECTDLG_MSG002), MB_ICONERROR);
		OnCancel();
	}//if

	m_bSelect = true;
	if(m_imgGif.GetSafeHwnd())
	{
		m_imgGif.ShowWindow(SW_SHOW);
	}//if
	m_imgGif.Draw();

//	OnOK();
}


BOOL CHostSelectDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	//상단 배경 이미지
	//HBITMAP hBitmapTop = 0;
	if(CEnviroment::eNARSHA == GetEnvPtr()->m_Customer){
		m_bmpBitmapTop.LoadBitmap(IDB_SPLASH_NS);
		//hBitmapTop = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SPLASH_NS));
	}
	else if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		m_bmpBitmapTop.LoadBitmap(IDB_SPLASH_EE);
		//hBitmapTop = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SPLASH_EE));
	}
	else
	{
		m_bmpBitmapTop.LoadBitmap(IDB_SPLASH);
		//hBitmapTop = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SPLASH));
	}

	m_imgTop.CreateFromHBITMAP(m_bmpBitmapTop);
	m_nWidthTop = m_imgTop.GetWidth();
	m_nHeightTop = m_imgTop.GetHeight();

	//하단 배경 이미지
	//HBITMAP hBitmapBottom;
	if(!m_bNew)
	{
		m_bmpBitmapBottom.LoadBitmap(IDB_OPEN_BG);
		//hBitmapBottom = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_OPEN_BG));
	}
	else
	{
		m_bmpBitmapBottom.LoadBitmap(IDB_OPEN_SAMPLE_BG);
		//hBitmapBottom = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_OPEN_SAMPLE_BG));
	}

	m_imgBottom.CreateFromHBITMAP(m_bmpBitmapBottom);
	m_nWidthBottom = m_imgBottom.GetWidth();
	m_nHeightBottom = m_imgBottom.GetHeight();

	//전체 창의 크기 변경
	//WINDOWPLACEMENT stWndPlc;
	//memset(&stWndPlc, 0x00, sizeof(WINDOWPLACEMENT));
	//this->GetWindowPlacement(&stWndPlc);
	//this->MoveWindow(stWndPlc.ptMinPosition.x, stWndPlc.ptMinPosition.y, nWidthTop, nHeightTop+nHeightBottom);
	this->MoveWindow(0, 0, m_nWidthTop, (m_nHeightTop+m_nHeightBottom));
	this->CenterWindow();

	//리스트 컨트롤의 크기, 위치 변경
	//m_listHost.MoveWindow(15, m_nHeightTop+27, m_nWidthBottom-30, m_nHeightBottom-(27+85));
	m_listHost.MoveWindow(15, m_nHeightTop+30, m_nWidthBottom-30, m_nHeightBottom-(30+85));

	//에디트 창의 위치,크기 변경
	m_edtHostName.MoveWindow(131, m_nHeightTop+158+4+100, m_nWidthBottom-(131+15), 20);

	//버튼위 위치 조정
	m_btnDelete.MoveWindow(15, m_nHeightTop+200+100, 74, 22);
	m_btnDelete.LoadBitmap(IDB_BTN_DELETE, RGB(255, 255, 255));
	m_btnDelete.SetToolTipText(LoadStringById(IDS_DELETEPACKAGEDLG_BUT001));

	m_btnOK.MoveWindow(178+200, m_nHeightTop+200+100, 74, 22);
	m_btnOK.LoadBitmap(IDB_POP_BTN_SELECT, RGB(255, 255, 255));
	m_btnOK.SetToolTipText(LoadStringById(IDS_HOSTSELECTDLG_BTN001));

	m_btnCancel.MoveWindow(258+200, m_nHeightTop+200+100, 74, 22);
	m_btnCancel.LoadBitmap(IDB_POP_BTN_CANCEL, RGB(255, 255, 255));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_HOSTSELECTDLG_BTN002));

	//GIF 이미지 생성
	CRect clsTop(0, 0, m_nWidthTop, m_nHeightTop);
	m_imgGif.Create("Animated GIF", WS_CHILD|WS_VISIBLE, clsTop, this);

	if(CEnviroment::eNARSHA == GetEnvPtr()->m_Customer){
		m_imgGif.Load(MAKEINTRESOURCE(IDR_SPLASH_GIF_NS),_T("GIF"));
	}else{
		m_imgGif.Load(MAKEINTRESOURCE(IDR_SPLASH_GIF),_T("GIF"));
	}
	
	if(m_imgGif.GetSafeHwnd())
	{
		m_imgGif.ShowWindow(SW_HIDE);
	}//if
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	CBitmap bmp;
	bmp.LoadBitmap(IDB_CHECK);

	if(m_imgCheck.Create(16, 15, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_imgCheck.Add(&bmp, RGB(255,255,255)); //바탕의 회색이 마스크
	}//if
	m_listHost.SetImageList(&m_imgCheck, LVSIL_SMALL);

	CRect clsRect;
	m_listHost.GetClientRect(clsRect);
	m_listHost.SetExtendedStyle(m_listHost.GetExtendedStyle() | /*LVS_EX_CHECKBOXES |*/ LVS_EX_FULLROWSELECT|LVS_EX_SUBITEMIMAGES);

	if(!m_bNew)
	{
		m_listHost.InsertColumn(0, LoadStringById(IDS_HOSTSELECTDLG_LST001), LVCFMT_LEFT, (int)((clsRect.Width()/10)*2));
		m_listHost.InsertColumn(1, LoadStringById(IDS_HOSTSELECTDLG_LST002), LVCFMT_LEFT, (int)((clsRect.Width()/10)*2.5));
		m_listHost.InsertColumn(2, LoadStringById(IDS_HOSTSELECTDLG_LST003), LVCFMT_LEFT, (int)((clsRect.Width()/10)*3));
		m_listHost.InsertColumn(3, LoadStringById(IDS_HOSTSELECTDLG_LST004), LVCFMT_LEFT, (int)((clsRect.Width()/10)*2));
	}
	else
	{
		m_listHost.InsertColumn(0, LoadStringById(IDS_HOSTSELECTDLG_LST001), LVCFMT_LEFT, (int)((clsRect.Width()/10)*2));
		m_listHost.InsertColumn(1, LoadStringById(IDS_HOSTSELECTDLG_LST005), LVCFMT_LEFT, (int)((clsRect.Width()/10)*5));
		m_listHost.InsertColumn(2, LoadStringById(IDS_HOSTSELECTDLG_LST004), LVCFMT_LEFT, (int)((clsRect.Width()/10)*3));
	}//if

	m_listHost.Initialize(false);

	InitList();

	//드라이브명으로 정렬 시킨다
	m_listHost.Sort(0, true);

	if(!m_strSelectedDrive.IsEmpty() && !m_strSelectedHostName.IsEmpty())
	{
		OnBnClickedOk();
	}

	CWaitMessageBox::Hide();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Host list를 list control에 설정 \n
/////////////////////////////////////////////////////////////////////////////////
void CHostSelectDlg::InitList()
{
	if(!m_pryLocalHostInfo)
	{
		return;
	}//if

	if(m_pryLocalHostInfo->GetCount() == 0)
	{
		return;
	}//if

	int nIdx;
	CString strDrive, strNetworkUse, strHostName, strDesc, strSite, strWrittenTime, strWrittenID;
	long nContentsCategory, nPurpose, nHostType, nVertical, nResolution;
	bool bIsPublic, bIsVerify;
	CString strValidationDate;

	CLocalHostInfo* pclsHost = NULL;
	for(int i=0; i<m_pryLocalHostInfo->GetCount(); i++)
	{
		pclsHost = (CLocalHostInfo*)m_pryLocalHostInfo->GetAt(i);
		pclsHost->GetLocalHostInfo(strDrive, strNetworkUse, strHostName, strDesc, strSite, strWrittenTime, strWrittenID
								 , nContentsCategory, nPurpose, nHostType, nVertical, nResolution, bIsPublic, bIsVerify, strValidationDate);
		if(!m_bNew)
		{
			//새로만들기가 아니면 샘플파일과 preview 파일은 보여주지 않는다
			if(strHostName.Find(SAMPLE_FILE_KEY) == 0) continue;
			if(strHostName.Find(PREVIEW_FILE_KEY) == 0) continue;
			if(strHostName.Find(TEMPORARY_SAVE_FILE_KEY) == 0) continue;

			nIdx = m_listHost.InsertItem(i, strDrive, 0);
			//nIdx = m_listHost.InsertItem(i, "", -1);
			//m_listHost.SetItemText(nIdx, 0, strDrive);

			LVITEM item;
			/*
			item.mask = LVIF_INDENT;
			item.iItem = nIdx;
			item.iSubItem = 0;
			item.iIndent = -2;
			m_listHost.SetItem(&item);
			*/
			
			item.mask = LVIF_TEXT|LVIF_IMAGE;
			item.iItem = nIdx;
			item.iSubItem = 1;
			
			if(strNetworkUse == "1")
			{
				item.iImage = 3;
				item.pszText = "Server";
				//strNetworkUse = "Network";
			}
			else
			{
				item.iImage = 2;
				item.pszText = "User";
				//strNetworkUse = "Local";
			}//if

			//m_listHost.SetItemText(nIdx, 1, strNetworkUse);
			m_listHost.SetItem(&item);
			m_listHost.SetItemText(nIdx, 2, strHostName);
			m_listHost.SetItemText(nIdx, 3, strDesc);
			m_listHost.SetItemData(i, (DWORD)pclsHost);
		}
		else
		{
			//새로만들기라면 샘플 파일만 보여준다
			if(strHostName.Find(SAMPLE_FILE_KEY, 0) == -1)
			{
				continue;
			}//if

			// 창일향의 경우 새로 만들기 버튼을 눌렀을 때,  나올 수 있는 템플릿 ini 의 리스트는 종전과는 달리 다음과 같은 접두어로 시작하는 ini 파일로 제한한다. __template_adasset
			if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
			{
				if(strHostName.Find(SAMPLE_FILE_KEY _T("adasset"), 0) == -1)
				{
					continue;
				}
			}

			nIdx = m_listHost.InsertItem(i, strDrive, 0);
			m_listHost.SetItemText(nIdx, 1, strHostName.Right(strHostName.GetLength()-11));
			m_listHost.SetItemText(nIdx, 2, strDesc);
			m_listHost.SetItemData(i, (DWORD)pclsHost);
		}//if
	}//for
}


void CHostSelectDlg::OnNMClickHostList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	LVITEM item;
	POSITION pos = m_listHost.GetFirstSelectedItemPosition();
	if(pos == NULL)
	{
		for(int i=0; i<m_listHost.GetItemCount(); i++)
		{
			item.iItem = i;
			item.iSubItem = 0;
			item.mask = LVIF_IMAGE;
			item.iImage = 0;

			m_listHost.SetItem(&item);
		}//for
		m_strSelectedHostName = "";
		UpdateData(FALSE);
		return;
	}//if

	int nItem = m_listHost.GetNextSelectedItem(pos);

	for(int i=0; i<m_listHost.GetItemCount(); i++)
	{
		item.iItem = i;
		item.iSubItem = 0;
		item.mask = LVIF_IMAGE;

		if(i == nItem)
		{
			item.iImage = 1;
			if(!m_bNew)
			{
				m_strSelectedHostName = m_listHost.GetItemText(i, 2);
				m_strSelectedDrive = m_listHost.GetItemText(i, 0);
			}
			else
			{
				m_strSelectedHostName = m_listHost.GetItemText(i, 1);
				m_strSelectedDrive = m_listHost.GetItemText(i, 0);
			}//if
		}
		else
		{
			item.iImage = 0;
		}//if

		m_listHost.SetItem(&item);
	}//for

	UpdateData(FALSE);
}

// Modified by 정운형 2009-01-28 오후 1:43
// 변경내역 :  이미지 추가 작업
void CHostSelectDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
	CRect rectDlg;
	GetClientRect(&rectDlg);
	dc.FillSolidRect(0, 0, rectDlg.Width(), rectDlg.Height(), RGB(0,0,0));

	CDC* hDC = this->GetDC();
	if(m_imgTop.IsValid())
	{
		m_imgTop.Draw(hDC->m_hDC, 0, 0, m_nWidthTop, m_nHeightTop);
	}//if

	if(m_imgBottom.IsValid())
	{
		m_imgBottom.Draw(hDC->m_hDC, 0, m_nHeightTop, m_nWidthBottom, m_nHeightBottom);
	}//if

	//if(m_bSelect)
	//{
	//	m_imgGif.Draw();
	//}//if

	//내부 컨트롤 다시 그리기
	m_listHost.Invalidate();
	m_edtHostName.Invalidate();
	m_btnOK.Invalidate();
	m_btnCancel.Invalidate();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Host info 배열을 설정 \n
/// @param (CPtrArray*) paryHostInfo : (in) 설정하려는 host info 배열
/////////////////////////////////////////////////////////////////////////////////
void CHostSelectDlg::SetHostInfoList(CPtrArray* paryHostInfo)
{
	m_pryLocalHostInfo = paryHostInfo;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 메인 frame의 document 포인터를 설정 \n
/// @param (CWnd*) pFrm : (in) 메인 frame의 포인터
/// @param (void*) pclsDoc : (in) 메인 frame의 document 포인터
/// @param (bool) bNew : (in) 새로만들기 상태인지 나타내는 flag
/////////////////////////////////////////////////////////////////////////////////
void CHostSelectDlg::SetParentPtr(CWnd* pFrm, void* pclsDoc, bool bNew, CString strSelDrive, CString strSelHostName)
{
	m_frmParent = pFrm;
	m_pclsDoc = pclsDoc;
	m_bNew = bNew;
	m_strSelectedDrive = strSelDrive;
	m_strSelectedHostName = strSelHostName;
}

void CHostSelectDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_imgGif.Stop();
	if(m_imgGif.GetSafeHwnd())
	{
		m_imgGif.ShowWindow(SW_HIDE);
	}//if

	OnCancel();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// complete import message handler\n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CHostSelectDlg::OnImportComplete(WPARAM wParam, LPARAM lParam)
{
	if(wParam != 1)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTSELECTDLG_MSG002), MB_ICONERROR);
	}//if

	OnOK();

	return 0;
}
// Modified by 정운형 2009-01-28 오후 1:43
// 변경내역 :  이미지 추가 작업
void CHostSelectDlg::OnNMDblclkHostList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	OnBnClickedOk();
}

void CHostSelectDlg::OnBnClickedDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	// template 파일을 삭제한다.....

	if(UbcMessageBox(LoadStringById(IDS_ORBINFODLG_MSG002), MB_YESNO) != IDYES)
	{
		return;
	}

	if(m_strSelectedHostName == "")
	{
		UbcMessageBox(LoadStringById(IDS_HOSTSELECTDLG_MSG001), MB_ICONWARNING);
		return;
	}//if

	CString full_path = m_strSelectedDrive;
	full_path += "SQISoft\\UTV1.0\\execute\\config\\__template_";
	full_path += m_strSelectedHostName;
	full_path += ".ini";

	//UbcMessageBox(full_path, MB_ICONWARNING);
	
	CString msg = m_strSelectedHostName;
	if(!::DeleteFile(full_path)) {
		msg += LoadStringById(IDS_DELETE_PACKAGE_FAILED);
		UbcMessageBox(msg, MB_ICONWARNING);
		return;
	}

	msg += LoadStringById(IDS_DELETE_PACKAGE_SUCCEED);
	UbcMessageBox(msg, MB_OK);
	OnBnClickedCancel();

	return;
}
