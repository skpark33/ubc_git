#pragma once

#include "Schedule.h"
#include "DataContainer.h"

// CSubPlayContentsDialog 대화 상자입니다.
class CSubPlayContentsDialog : public CDialog
{
	DECLARE_DYNAMIC(CSubPlayContentsDialog)

public:
	CSubPlayContentsDialog(UINT nIDTemplate, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubPlayContentsDialog();

// 대화 상자 데이터입니다.

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CBrush	m_brushBG;
	CArray<CWnd*, CWnd*>	m_listNoCTLWnd;

public:
	virtual bool	GetPlayContentsInfo(PLAYCONTENTS_INFO& info, int nIndex) = 0;
	virtual bool	SetPlayContentsInfo(PLAYCONTENTS_INFO& info) = 0;
	virtual int		GetPlayContentsCount() = 0;
	virtual bool    InputErrorCheck() { return TRUE; };

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
