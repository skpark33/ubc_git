// RenameContentsFileDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "RenameContentsFileDlg.h"
#include "shlwapi.h"
#include "Dbghelp.h"
#include "common/PreventChar.h"

#define TIMER_NAME_SEL	1233


// CRenameContentsFileDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CRenameContentsFileDlg, CDialog)

CRenameContentsFileDlg::CRenameContentsFileDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRenameContentsFileDlg::IDD, pParent)
	, m_strNewName(_T(""))
	, m_strOldFileName(_T(""))
	, m_strMsg(_T(""))
	, m_strENCPath(_T(""))
{

}

CRenameContentsFileDlg::~CRenameContentsFileDlg()
{
}

void CRenameContentsFileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_OLD_NAME_EDT, m_strOldFileName);
	DDX_Text(pDX, IDC_NEW_NAME_EDT, m_strNewName);
	DDX_Text(pDX, IDC_MSG_STATIC, m_strMsg);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CRenameContentsFileDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CRenameContentsFileDlg::OnBnClickedOk)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CRenameContentsFileDlg 메시지 처리기입니다.

void CRenameContentsFileDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if(m_strNewName == m_strOldFileName)
	{
		return;
	}//if

//	if( (m_strNewName.Find("[", 0) != -1) ||
//		(m_strNewName.Find("]", 0) != -1) )
	if(CPreventChar::GetInstance()->HavePreventChar(m_strNewName))
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_RENAMECONTENTSFILEDLG_MSG001), CPreventChar::GetInstance()->GetPreventChar());
		UbcMessageBox(strMsg, MB_ICONERROR);
		return;
	}//if

	OnOK();
}

BOOL CRenameContentsFileDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btnOK.LoadBitmap(IDB_BTN_APPLY, RGB(255, 255, 255));
	m_btnOK.SetToolTipText(LoadStringById(IDS_RENAMECONTENTSFILEDLG_BTN001));
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_RENAMECONTENTSFILEDLG_BTN002));

	SetTimer(TIMER_NAME_SEL, 100, NULL);


	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CRenameContentsFileDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == TIMER_NAME_SEL)
	{
		KillTimer(TIMER_NAME_SEL);

		MakeAvailableName();

		CEdit* pEdt = (CEdit*)GetDlgItem(IDC_NEW_NAME_EDT);
		pEdt->SetFocus();
		pEdt->SetSel(0, -1);
	}//if

	CDialog::OnTimer(nIDEvent);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용이 가능한 파일이름을 만들어 준다 \n
/////////////////////////////////////////////////////////////////////////////////
void CRenameContentsFileDlg::MakeAvailableName(void)
{
	CString strNumberFileName, strCompareExist;
	CString strExt = "";
	//int nIdx = m_strOldFileName.Find(".", 0);
	int nIdx = m_strOldFileName.ReverseFind('.');
	if(nIdx != -1)
	{
		int nCount = m_strOldFileName.GetLength() - nIdx;
		strNumberFileName = m_strOldFileName.Left(nIdx);
		strExt = m_strOldFileName.Right(nCount);
	}
	else
	{
		strNumberFileName = m_strOldFileName;
	}//if

	//(콘텐츠 파일 정보를 ini 파일에 기록할 때, '[', ']' 문자가 있으면 오류 발생 하므로...
	//만약 콘텐츠 파일 이름에 '[', ']'가 있다면 '(', ')'로 변경한다.
	//strNumberFileName.Replace(_T("["), _T("("));
	//strNumberFileName.Replace(_T("]"), _T(")"));
	//strNumberFileName.Replace(_T(","), _T("_"));
	strNumberFileName = CPreventChar::GetInstance()->ConvertToAvailableChar(strNumberFileName);

	//ENC 폴더에 같은 파일 이름이 있다면 중복된 파일이름 변경
	strCompareExist = m_strENCPath;
	CString strTmpFileName = strNumberFileName;
	strCompareExist.Append(strNumberFileName);
	strCompareExist.Append(strExt);
	int nSeq = 1;
	while(::PathFileExists(strCompareExist))
	{
		if(nSeq == 99)
		{
			strTmpFileName = strNumberFileName;
			nSeq = 1;
		}//if
		strNumberFileName.Format("%s_%02d", strTmpFileName, nSeq++);
		strCompareExist = m_strENCPath;
		strCompareExist.Append(strNumberFileName);
		strCompareExist.Append(strExt);
	}//while

	//이름이 바뀌었다면...
	m_strNewName = strNumberFileName + strExt;

	UpdateData(FALSE);
}
