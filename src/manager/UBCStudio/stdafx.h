// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxinet.h>

#include <afxdisp.h>        // MFC Automation classes

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <fstream>

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

//#ifdef _ML_ENG_
//#include "UBCStudio_en.h"
//#elif _ML_KOR_
//#include "UBCStudio_kr.h"
//#elif _ML_JAP_
//#include "UBCStudio_jp.h"
//#endif


// XP Theme (for VS2005)
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#include "common/libCommon/utvMacro.h"
#include "UBCCommon\ubccommon.h"
#include "common\ubcdefine.h"
#include "UbcCommon/WaitMessageBox.h"

//#define		IMPORT_ROOT_PATH				_T("SQISoft")
//#define		IMPORT_CONFIG_PATH				_T("SQISoft\\UTV1.0\\execute\\config\\")
//#define		IMPORT_CONTENTS_PATH			_T("SQISoft\\Contents\\ENC\\")
//#define		IMPORT_CONTENTS_ROOT			_T("SQISoft\\Contents\\")
//#define		EXPORT_ROOT_PATH				("SQISoft_New")
//#define		EXPORT_CONFIG_PATH				("SQISoft_New\\UTV1.0\\execute\\config\\")
//#define		EXPORT_CONTENTS_PATH			("SQISoft_New\\Contents\\ENC\\")
//#define		TEMP_ROOT_PATH					("SQISoft_Old")

//Sample 파일들의 경로(현재는 기존의 일반 config와 contents 파일들의 경로와 같다)
#define		SAMPLE_CONFIG_PATH				UBC_CONFIG_PATH //_T("SQISoft\\UTV1.0\\execute\\config\\")
//#define		SAMPLE_CONTENTS_PATH			("SQISoft\\Contents\\ENC\\")
#define		FTP_CONTENTS_PATH				UBC_CONTENTS_PATH //_T("SQISoft\\Contents\\ENC\\")
#define		FTP_CONFIG_PATH					UBC_CONFIG_PATH //_T("SQISoft\\UTV1.0\\execute\\config\\")


#define		TEMPLATE_WIDTH					200
#define		TEMPLATE_HEIGHT					150
#define		TEMPLATE_INFO_HEIGHT			20
#define		TEMPLATE_OUT_TOP_GAP			0//10	// 윈도우-아이템간격
#define		TEMPLATE_OUT_BOTTOM_GAP			0//10	// 윈도우-아이템간격
#define		TEMPLATE_OUT_LEFT_GAP			0//10	// 윈도우-아이템간격
#define		TEMPLATE_OUT_RIGHT_GAP			0//10	// 윈도우-아이템간격
#define		TEMPLATE_IN_TOP_GAP				6//10	// 아이템내부 간격
#define		TEMPLATE_IN_BOTTOM_GAP			10//10	// 아이템내부 간격
#define		TEMPLATE_IN_LEFT_GAP			11//10	// 아이템내부 간격
#define		TEMPLATE_IN_RIGHT_GAP			11//10	// 아이템내부 간격

#define		SCROLL_DELTA					10

#define		WM_DSINTERFACES_GRAPHNOTIFY				(WM_USER + 100)

#define		WM_CLEAR_CONTENTS_SELECTION				(WM_USER + 101)
#define		WM_ADD_CONTENTS							(WM_USER + 102)
#define		WM_TEMPLATE_INFO_CHANGED				(WM_USER + 103)
#define		WM_FRAME_INFO_CHANGED					(WM_USER + 104)
#define		WM_TEMPLATE_FRAME_SELECT_CHANGED		(WM_USER + 105)
#define		WM_TEMPLATE_LIST_CHANGED				(WM_USER + 106)
#define		WM_PLAY_TEMPLATE_LIST_CHANGED			(WM_USER + 107)
#define		WM_PLAYCONTENTS_LIST_CHANGED				(WM_USER + 108)
#define		WM_CONTENTS_LIST_CHANGED				(WM_USER + 109)
#define		WM_COMPLETE_FILE_COPY					(WM_USER + 110)
#define		WM_DISPLAY_STATUS						(WM_USER + 111)

#define		WM_INFOIMPORT_COMPELETE					(WM_USER + 112)
#define		WM_PREVIEW_COMPELETE					(WM_USER + 113)
#define		WM_SAVE_CONFIG_COMPELETE				(WM_USER + 114)
#define		WM_COMPLETE_FTP_CONNECTION				(WM_USER + 115)
#define		WM_COMPLETE_FTP_UPLOAD					(WM_USER + 116)
#define		WM_FTP_UPLOAD							(WM_USER + 117)
#define		WM_PRIMARY_FRAME_OVERLAP				(WM_USER + 118)		//Frame에 Primary frame이 한개 이상임을 알려주는 메시지
#define		WM_HIDE_CONTENTS						(WM_USER + 119)
#define		WM_TEMPLATE_SELECTED					(WM_USER + 120)		//Template select dialog에서 template가 선택되었음을 알려주는 메시지		
#define		WM_ALL_INFO_CHANGED						(WM_USER + 121)


#include "DataContainer.h"


CString		ToString(int nValue);
CString		ToString(double fValue, int nDegree=3);
CString		ToString(ULONG ulValue);
CString		ToString(ULONGLONG ulValue);

CString		ToMoneyTypeString(LPCTSTR lpszStr);
CString		ToMoneyTypeString(int value);
CString		ToMoneyTypeString(ULONG ulvalue);
CString		ToMoneyTypeString(ULONGLONG ulvalue);

CString		ToFileSize(ULONGLONG lVal, char chUnit, bool bIncUnit = true);

CString		GetTimeZoneString(int nTimeZone);
CString		GetContentsTypeString(int nContentsType);

COLORREF	GetColorFromString(LPCTSTR lpszColor);
CString		GetColorFromString(COLORREF rgb);
void		GetColorRGB(COLORREF rgb, int& r, int& g, int& b);

LPCTSTR		GetDataPath();

CString		MakeCurrency(ULONGLONG lVar);

CString		FindExtension(const CString& strFileName);		///<파일명에서 확장자를 분리하여 반환
CString		VariantToString(VARIANT* var);					///<VARIANT를 문자열로 변환한다.

LPCTSTR		GetAppPath();
CString		GetConnectServerIP(void);						///<UBCConnect.ini 파일의 Server IP 반환
bool		IsAvailableFont(CString strFontName);			///<시스템에 지정된 폰트가 있는지를 반환
LPCTSTR		GetBRWConfigPath(void);							///<UBCBrowser.ini 파일의 경로 반환
int			GetVidoeRenderType(void);						///<사용할 Vidoe render 종류를 반환(1: Overlay, 2:VMR7,....)
int			GetVidoeOpenType(void);							///<사용할 Vidoe open하는 방법을 반환(1: 초기화과정에 open, 2:매번 플레이시에 open)
CString		GetErrMessage(int nErrCode, CString strDefaultMsg = _T(""));
bool		IsExistFile(CString strFilePath);
CString		LoadStringById(UINT nID);

void		TerminateProcess(CString processName);
BOOL		IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs);
void		KillProcess(DWORD dwProcessId);

void		InitDateRange(CDateTimeCtrl& dtControl);
int			FindNoCase(CString strSrc, CString strSub, int nStart=0);


unsigned long	RandomNumber(unsigned long max);
void			RandomSpace(unsigned long max, CString& strSpace);
bool			AppendStringToFile(CString& fullpath, CString& appendStr);


bool TimeStringToFileTime(CString& timeStr,  LPFILETIME pft); // skpark same_size_file_problem
bool isNewFile(CString& target, CString& modifiedTime); // skpark same_size_file_problem

//#include "DataContainer.h"

// Modified by 정운형 2008-12-09 오후 4:39
// 변경내역 :  HP demo
//#define HP_DEMO
// Modified by 정운형 2008-12-09 오후 4:39
// 변경내역 :  HP demo

//#define		EXCUTE_PATH				_T("SQISoft\\UTV1.0\\execute\\")
#define		EXCUTE_FILE				_T("UTV_brwClient2.exe")

#define		EXCUTE_ARG_IGNORE		_T("+appId BROWSER +autoscale +aspectratio +local +log +ignore_env +mouse")
#define		EXCUTE_ARG				_T("+appId BROWSER +autoscale +aspectratio +local +log +mouse")


#define SAMPLE_FILE_KEY				_T("__template_")
#define	PREVIEW_FILE_KEY			_T("__preview_")
#define TEMPORARY_SAVE_FILE_KEY		_T("__TemporarySaved_")	// 0001128: Studio 임시 저장 및 복구

//Shared version(사용기간 제한) 정의
//#define	_VER_SHARED_DATE

#define KEY_MAGIC_NUMBER			_T("SQI!UTV@2109")		//매직넘버
#define FILE_NAME_REG				_T("UBC.dat")				//등록정보 파일 이름

//Shared version인 경우 사용시작 날자와 종료 날자를 갖는 구조체
typedef struct _ST_REG_INFO
{
	bool		bFirstUse;				//처음 사용을 시작했는지 여부
	bool		bRegist;				//정식 등록을 하였는지 여부
	SYSTEMTIME	tmBegin;				//제품 사용을 시작한 날자, 시간
	//SYSTEMTIME	tmEnd;					//제품 사용이 종료되는 날자, 시간
	TCHAR		szCopyRight[128];		//Copy Right 문자열(현재는 임시로 회사명)
	//TCHAR		szRegKey[64];			//제품 등록된 키값
	//TCHAR		szCompany[48];			//사용자 회사 이름
	//TCHAR		szPostion[48];			//사용 부서
	//TCHAR		szUserName[32];			//사용자 이름
	//TCHAR		szEmail[32];			//사용자 e-mail 주소
	//TCHAR		szPhone[32];			//사용자 연락처
} ST_REG_INFO;

//FTP 전송 오류 코드
enum {
	FTP_ERROR_OK	=	0,			//All Ok
	FTP_ERROR_EMPTY_CONTENTS,		//Empty contents
	FTP_ERROR_USER_CANCEL,			//User cancel
	FTP_ERROR_CONNECT_FAIL,			//Connect fail
	FTP_ERROR_LOCAL_FILE_OPEN,		//Local file open error
	FTP_ERROR_REMOTE_FILE_OPEN,		//Remote file open error
	FTR_ERROR_REMOTE_FILE_EXIST,
	FTR_ERROR_REMOTE_SPACE,
	FTP_ERROR_NOTIFY_UPDATE,		//Update notify fail
	FTP_ERROR_UNKNOWN,				//Unknown file exception
};

#define _USE_DRM

using namespace std;

enum E_VIDEO_OPEN_TYPE
{
	E_OPEN_INIT = 1,		//초기화 과정에 open
	E_OPEN_PLAY,			//플레이시기에 매번 open
};


#define MAX_CONTENTS_COUNT 1000          // 최대 갯수는 의무사항이 아닌 주의사항일 뿐이다. 
#define MAX_CONTENTS_SIZE  1024*2  // 2048M 최대크기는 의무사항이 아닌 주의사항일 뿐이다.
#define MAX_SCHEDULE_COUNT 6000          // 최대 갯수는 의무사항이 아닌 주의사항일 뿐이다. 
