#pragma once
#include "afxdtctl.h"
#include "resource.h"

// CCalendarDlg dialog

class CCalendarDlg : public CDialog
{
	DECLARE_DYNAMIC(CCalendarDlg)

public:
	CCalendarDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCalendarDlg();

// Dialog Data
	enum { IDD = IDD_CALENDAR };

	void SetCurSel(LPSYSTEMTIME);
	void GetCurSel(LPSYSTEMTIME);

protected:
	SYSTEMTIME m_stSel;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CMonthCalCtrl m_ctrCalendar;
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
};
