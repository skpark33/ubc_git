// CustomerInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CustomerInfoDlg.h"
#include "Enviroment.h"
#include "CopModule.h"


// CCustomerInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCustomerInfoDlg, CDialog)

CCustomerInfoDlg::CCustomerInfoDlg(CString strCustomerInfo, CWnd* pParent /*=NULL*/)
	: CDialog(CCustomerInfoDlg::IDD, pParent)
	, m_strCustumerName(strCustomerInfo)
{

}

CCustomerInfoDlg::~CCustomerInfoDlg()
{
}

void CCustomerInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CB_CUSTOMER_INFO, m_cbCustomerInfo);
}

void CCustomerInfoDlg::OnBnClickedCancel()
{
}

BEGIN_MESSAGE_MAP(CCustomerInfoDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCustomerInfoDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCustomerInfoDlg::OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_CB_CUSTOMER_INFO, &CCustomerInfoDlg::OnCbnSelchangeCbCustomerInfo)
END_MESSAGE_MAP()


// CCustomerInfoDlg 메시지 처리기입니다.

void CCustomerInfoDlg::OnBnClickedOk()
{
	if(m_cbCustomerInfo.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_CUSTOMERINFO_MSG001));
		return;
	}

	m_cbCustomerInfo.GetLBText(m_cbCustomerInfo.GetCurSel(), m_strCustumerName);

	m_nCustumerTimezone = 9999;
	m_strCustumerLanguage = _T("");

	// 0001408: Customer 정보 입력을 받는다.
	POSITION pos = m_listCustumerInfo.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SCustomerInfo info = m_listCustumerInfo.GetNext(pos);
		if(m_strCustumerName != info.customerId) continue;

		m_nCustumerTimezone = info.gmt;
		m_strCustumerLanguage = info.language;

		CString szPath;
		szPath.Format(_T("%s%sdata\\%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, UBCVARS_INI);
		::WritePrivateProfileString("CUSTOMER_INFO", "NAME", m_strCustumerName, szPath);
		::WritePrivateProfileString("CUSTOMER_INFO", "GMT", ToString(info.gmt), szPath);
		::WritePrivateProfileString("CUSTOMER_INFO", "LANGUAGE", info.language, szPath);

		// URL.INI를 새로 작성한다.
		szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, UBCURL_INI);
		CFile file;
		if(file.Open(szPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
		{
			file.Write((LPCTSTR)info.url, info.url.GetLength()*sizeof(TCHAR));
			file.Close();
		}
	}

	OnOK();
}

BOOL CCustomerInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

#ifdef _UBCSTUDIO_EE_
	// 0001408: Customer 정보 입력을 받는다.
	getCustomerList(m_listCustumerInfo);
#endif

	int nSelIndex = -1;

	POSITION pos = m_listCustumerInfo.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SCustomerInfo info = m_listCustumerInfo.GetNext(pos);
		int nIndex = m_cbCustomerInfo.AddString(info.customerId);

		if(m_strCustumerName == info.customerId)
		{
			nSelIndex = nIndex;
		}
	}

	if(nSelIndex >= 0)
	{
		m_cbCustomerInfo.SetCurSel(nSelIndex);
		OnBnClickedOk();
	}

	//m_cbCustomerInfo.AddString(_T("KIA"));
	//m_cbCustomerInfo.AddString(_T("HYUNDAI"));
	//m_cbCustomerInfo.AddString(_T("SQISOFT"));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCustomerInfoDlg::OnCbnSelchangeCbCustomerInfo()
{
	UpdateData(TRUE);

	CString strTimezone = _T("Not set");
	CString strLanguage = _T("Not set");

	m_cbCustomerInfo.GetLBText(m_cbCustomerInfo.GetCurSel(), m_strCustumerName);

	// 0001408: Customer 정보 입력을 받는다.
	POSITION pos = m_listCustumerInfo.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SCustomerInfo info = m_listCustumerInfo.GetNext(pos);
		if(m_strCustumerName != info.customerId) continue;

		if(info.gmt >= -24*60 && info.gmt <= 24*60)
		{
			int nTimezone = info.gmt * (info.gmt>=0 ? 1 : -1);

			strTimezone.Format(_T("UTC%c%02d:%02d")
							, (info.gmt>=0 ? '-' : '+')
							, nTimezone/60
							, nTimezone-(nTimezone/60)*60
							);
		}

		if(!info.language.IsEmpty())
		{
			strLanguage = info.language;
		}

		break;
	}

	SetDlgItemText(IDC_TXT_TIMEZONE, strTimezone);
	SetDlgItemText(IDC_TXT_LANGUAGE, strLanguage);
}
