#pragma once
#include "afxcmn.h"


#define		WM_UPDATE_EFFECTS_ORDER		(WM_USER + 2049)

#define		EFFECTS_MOSAIC				"mosaic"
#define		EFFECTS_DISSOLVE			"dissolve"
#define		EFFECTS_PERSPECTIVE			"perspective"
#define		EFFECTS_IRIS				"iris"
#define		EFFECTS_REVOLVE				"revolve"
#define		EFFECTS_PUSH				"push"
#define		EFFECTS_ZOOM				"zoom"
#define		EFFECTS_CUBE				"cube"
#define		EFFECTS_WIPE				"wipe"
#define		EFFECTS_END_MARK			""


// CFlashImageplayerEffectsOderChangeDlg 대화 상자입니다.

class CFlashImageplayerEffectsOderChangeDlg : public CDialog
{
	DECLARE_DYNAMIC(CFlashImageplayerEffectsOderChangeDlg)

public:
	CFlashImageplayerEffectsOderChangeDlg(CWnd* pParent);   // 표준 생성자입니다.
	virtual ~CFlashImageplayerEffectsOderChangeDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FLASH_IMAGEPLAYER_EFFECTS_ORDER_CHANGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CWnd*	m_pParentWnd;

	int		m_nSendUpdateMessage;
	CStringArray	m_listSelectedEffectsOrder;
	CString			m_strSelectedEffectsOrderString;

public:
	afx_msg void OnBnClickedButtonMoveUp();
	afx_msg void OnBnClickedButtonMoveDown();
	afx_msg BOOL OnNcActivate(BOOL bActive);

	CListCtrl m_lcEffects;

	CStringArray&	GetSelectedEffectsOrder() { return m_listSelectedEffectsOrder; };
	CString			GetSelectedEffectsOrderString() { return m_strSelectedEffectsOrderString; };

	void			SetSelectedEffectsOrder(CStringArray& listEffectsOrder) { m_listSelectedEffectsOrder.Copy(listEffectsOrder); };

	static	CString GeEffectsOrderString(CStringArray& listEffectsOrder);
};
