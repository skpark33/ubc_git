/************************************************************************************/
/*! @file LocalHostInfo.h
	@brief Local system의 존재하는 host 정보를 갖는 객체 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/01/28\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2009/01/28:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once


//! Local system의 존재하는 host 정보를 갖는 객체
/*!
	
*/
class CLocalHostInfo
{
public:
	CLocalHostInfo();																		///<생성자
	virtual~CLocalHostInfo(void);															///<소멸자

	void	SetLocalHostInfo(CString strDrive, CString strNetworkUse,
							CString strHostName, CString strDescription,
							CString strSite, CString strLastWrittenTime,
							CString strLastWrittenUser,
							long nContentsCategory, long nPurpose, long nHostType,
							long nVertical, long nResolution, bool bIsPublic, bool bIsVerify,
							CString strValidationDate);					///<local host 정보를 설정

	void	GetLocalHostInfo(CString& strDrive, CString& strNetworkUse,
							CString& strHostName, CString& strDescription,
							CString& strSite, CString& strLastWrittenTime,
							CString& strLastWriteenUser,
							long& nContentsCategory, long& nPurpose, long& nHostType,
							long& nVertical, long& nResolution, bool& bIsPublic, bool& bIsVerify,
							CString& strValidationDate);					///<local host 정보를 반환

protected:
	CString		m_strDrive;																	///<host 정보를 갖고있는 드라이브 문자열
	CString		m_strNetworkUse;															///<Network use 여부(BRW에서 만든 ini 파일은 1, Studio에서 만든 ini 파일은 0)
	CString		m_strHostName;																///<local system에 존재하는 host 이름
	CString		m_strDescription;															///<Host에 대한 설명
	CString		m_strSite;																	///<Host의 site 명
	CString		m_strLastWrittenUser;
	CString		m_strLastWrittenTime;														///<마지막으로 저장된 시간

	// 패키지객체 속성추가
	LONG		m_nContentsCategory;	// 종류 (모닝,..)
	LONG		m_nPurpose         ;	// 용도별 (교육용,..)
	LONG		m_nHostType        ;	// 단말타입 (키오스크..)
	LONG		m_nVertical        ;	// 가로/세로 방향 (가로,..)
	LONG		m_nResolution      ;	// 해상도 (1920x1080,..)
	bool		m_bIsPublic        ;	// 공개여부
	bool		m_bIsVerify        ;	// 승인여부
	CString		m_strValidationDate;	// 패키지유효기간
};