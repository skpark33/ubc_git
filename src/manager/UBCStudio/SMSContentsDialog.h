#pragma once


#include "SubContentsDialog.h"

#include "EditEx.h"
#include "ColorPickerCB.h"
#include "common/FontPreviewCombo.h"
#include "ximage.h"

#include "ReposControl.h"

#include "Frame.h"
#include "Schedule.h"

#include "common/HoverButton.h"
#include "common/BtnST.h"

// CSMSContentsDialog 대화 상자입니다.

class CSMSContentsDialog : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CSMSContentsDialog)

public:
	CSMSContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSMSContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SMS_CONTENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CxImage			m_image;
	CSMSSchedule	m_wndSMS;
	CReposControl	m_reposControl;
	bool			m_bPreviewMode;		///<미리보기 모드
	BOOL			m_bPermanent;		///<24시간 running time

	void	SetPlayContents();
	void	MoveItem(int nItem, int nItemNewPos);

	bool	ZoomIn();					///<콘텐츠 미리보기에서 ZoomIn(확대) 기능을 한다.
	bool	ZoomOut();					///<콘텐츠 미리보기에서 ZoomOut(축소) 기능을 한다.
	void	RestoreZoom(void);			///<콘텐츠 미리보기에서 ZoomOut(축소)/ZoomIn(확대) 기능을 수행하지않은 원래상태로 복귀한다.

public:

	virtual CONTENTS_TYPE	GetContentsType() { return CONTENTS_TEXT; };
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);

	afx_msg void OnSize(UINT nType, int cx, int cy);
//	afx_msg void OnLvnEndlabeleditListContentsComment(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnLvnKeydownListContentsComment(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedButtonBrowserContentsFile();
	afx_msg void OnBnClickedButtonContentsCommentAdd();
	afx_msg void OnBnClickedButtonContentsCommentDelete();
	afx_msg void OnBnClickedButtonContentsCommentUp();
	afx_msg void OnBnClickedButtonContentsCommentDown();
	afx_msg void OnBnClickedButtonPreviewPlay();

	CEdit			m_editContentsName;
	CEdit			m_editContentsFileName;
	
	CStatic			m_staticContentsWidth;
	CStatic			m_staticContentsHeight;
	CStatic			m_staticContentsFileSize;
	CEditEx			m_editContentsPlayMinute;
	CEditEx			m_editContentsPlaySecond;
	CEditEx			m_editContentsFontSize;
	CFontPreviewCombo	m_cbxContentsFont;
	CColorPickerCB	m_cbxContentsTextColor;
	CColorPickerCB	m_cbxContentsBGColor;
//	CListCtrl		m_listctrlContentsComment;

	CStatic			m_groupPreview;
	CStatic			m_staticContents;

	CHoverButton		m_btnBrowserContentsFile;
	CHoverButton		m_btnContentsCommentAdd;
	CHoverButton		m_btnContentsCommentDelete;
	CHoverButton		m_btnContentsCommentUp;
	CHoverButton		m_btnContentsCommentDown;
	CHoverButton		m_btnPreviewPlay;
	CHoverButton		m_btnDelFile;
	
	CHoverButton		m_btnZoomIn;
	CHoverButton		m_btnZoomOut;
	CHoverButton		m_btnOriginal;

	CString				m_strLocation;
	CButtonST			m_chkAlign[9];
	int					m_nAlign;

	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
	void	EnableAllControls(BOOL bEnable);			///<컨트롤의 사용가능여부 설정

	afx_msg void OnBnClickedPermanentCheck();
	afx_msg void OnEnChangeEditContentsComment();
	afx_msg void OnBnClickedAlign1();
	afx_msg void OnBnClickedAlign2();
	afx_msg void OnBnClickedAlign3();
	afx_msg void OnBnClickedAlign4();
	afx_msg void OnBnClickedAlign5();
	afx_msg void OnBnClickedAlign6();
	afx_msg void OnBnClickedAlign7();
	afx_msg void OnBnClickedAlign8();
	afx_msg void OnBnClickedAlign9();
	afx_msg void OnBnClickedDeletefile();
	afx_msg void OnBnClickedButtonZoomIn();
	afx_msg void OnBnClickedButtonZoomOut();
	afx_msg void OnBnClickedButtonOriginal();

	CStatic			m_staticCommentTextColor;
	CColorPickerCB	m_cbxCommentTextColor;
	afx_msg void OnCbnSelchangeComboCommentTextColor();
};

class CPreviewDlg : public CDialog
{
	DECLARE_DYNAMIC(CPreviewDlg)

public:
	CPreviewDlg(CSMSSchedule* sms, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPreviewDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PREVIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CSMSSchedule*	m_pSMS;
	CSMSSchedule	m_wndSMS;

public:
	virtual BOOL OnInitDialog();
};
