#pragma once
#include "afxwin.h"
#include "resource.h"

// skpark USB_Device

// RemovableDiskWarning 대화 상자입니다.

class RemovableDiskWarning : public CDialog
{
	DECLARE_DYNAMIC(RemovableDiskWarning)

public:
	RemovableDiskWarning(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~RemovableDiskWarning();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DRIVE_CONFIRM_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CStatic RemovableWarning;
	CListBox RemovablePackageList;
	CButton RemovableOK;
	CButton RemovableCancel;
	CButton RemovableIgnore;
	afx_msg void OnBnClickedRemovableOk();
	afx_msg void OnBnClickedRemovableCancel();
	afx_msg void OnBnClickedRemovableIgnore();

	CStringArray iniList;
	CString drive;
	CString myName;
	virtual BOOL OnInitDialog();
};
