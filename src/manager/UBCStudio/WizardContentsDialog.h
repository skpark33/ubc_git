#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "resource.h"
#include "SubContentsDialog.h"
#include "Schedule.h"
#include "EditEx.h"
#include "common/HoverButton.h"
#include "common/UBCTabCtrl.h"
#include "common/libXmlParser.h"
#include "ReposControl.h"


// CWizardContentsDialog 대화 상자입니다.

class CWizardContentsDialog : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CWizardContentsDialog)

public:
	CWizardContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CWizardContentsDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_WIZARD_CONTENTS };
	enum {
		EICO_VIDEO,	EICO_IMAGE,	EICO_TICKER,
		EICO_FLASH,	EICO_TEXT,	EICO_WEB,	EICO_PPT,
		EICON_TV, EICON_RSS, EICO_WIZARD,
		EICO_UNKNOWN
	};

	virtual CONTENTS_TYPE	GetContentsType() { return CONTENTS_WIZARD; }
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);
	virtual void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };

	void			SetPlayContents();

	CString			m_strContentsId;
	bool			m_bPreviewMode;
	BOOL			m_bPermanent;
	CEditEx			m_editContentsName;
	CEdit			m_editContentsFileName;
	CEditEx			m_editContentsPlayMinute;
	CEditEx			m_editContentsPlaySecond;
	CEdit			m_editFelicaUrl;
	CComboBox		m_cbWizardType;
	CUBCTabCtrl		m_tabSubContents;
	CStatic			m_groupPreview;
	CStatic			m_staticContents;
	CHoverButton	m_btnBrowserContentsFile;
	CHoverButton	m_btnDelFile;
	CHoverButton	m_btnPreviewPlay;
	CWizardSchedule m_wndWizard;
	CReposControl	m_reposControl;
	CBrush			m_brushBG;
	CEditEx			m_editContentsWaitMinute; // 0000910: 마법사콘텐츠에서 상위메뉴로 돌아가는 시간을 설정할 수 있도록 한다.
	CEditEx			m_editContentsWaitSecond; // ..
	BOOL			m_bWaitPermanent;         // ..

	CString			m_strLocation;

	CString			m_strWizardPath;
	CXmlParser		m_xmlCurData;

	void	InitXmlDataFromFile(CString strFile);
	void	InitXmlDataFromString(CString strXml);
	void	InitWizardType();
	void	InitSubContentsTab();
	void	ToggleTab(int nIndex);
	void	EnableAllControls(BOOL bEnable);			///<컨트롤의 사용가능여부 설정

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnBnClickedButtonPreviewPlay();
	afx_msg void OnBnClickedPermanentCheck();
	afx_msg void OnCbnSelchangeCbWizardType();
	afx_msg void OnTcnSelchangeTabSubContents(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL DestroyWindow();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedButtonBrowserContentsFile();
	afx_msg void OnBnClickedButtonDelfile();
	afx_msg void OnBnClickedKbWaitPermanentCheck();
};
