// MainFrmBitmapBG.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "MainFrmBitmapBG.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrmBitmapBG

CMainFrmBitmapBG::CMainFrmBitmapBG()
{
	VERIFY(m_bmp.LoadBitmap(IDB_VNCLOGO));
}

CMainFrmBitmapBG::~CMainFrmBitmapBG()
{
}


BEGIN_MESSAGE_MAP(CMainFrmBitmapBG, CWnd)
	//{{AFX_MSG_MAP(CMainFrmBitmapBG)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMainFrmBitmapBG message handlers

void CMainFrmBitmapBG::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
              
	RedrawWindow(NULL, NULL,
 		RDW_INVALIDATE|RDW_ERASE|RDW_ERASENOW|RDW_ALLCHILDREN);
}

BOOL CMainFrmBitmapBG::OnEraseBkgnd(CDC* pDC) 
{
	CWnd::OnEraseBkgnd(pDC);

	BITMAP bm ;
	CDC dcMem ;

	VERIFY(m_bmp.GetObject(sizeof(bm), (LPVOID)&bm));

	dcMem.CreateCompatibleDC(pDC);
	CBitmap* pOldBMP = (CBitmap*) dcMem.SelectObject(&m_bmp);

	CRect rect;
	GetClientRect(rect);

	int nMargin = 4;
	pDC->FillSolidRect(nMargin, nMargin, rect.right-(nMargin*2), rect.bottom-(nMargin*2), RGB(115, 148, 173));
	//pDC->FillSolidRect(nMargin, nMargin, rect.right-(nMargin*2), rect.bottom-(nMargin*2), RGB(123, 155, 179));

	pDC->BitBlt( (rect.right-bm.bmWidth) - nMargin - 2
				,(rect.bottom-bm.bmHeight) - nMargin - 2
				,bm.bmWidth
				,bm.bmHeight
				,&dcMem
				,0, 0
				,SRCCOPY);
  
   dcMem.SelectObject(pOldBMP) ;

   return TRUE;
}