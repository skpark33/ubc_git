#pragma once

#include <time.h>
#include "afxwin.h"
#include "resource.h"
#include "interface.h"
#include "common/UTBListCtrlEx.h"
#include "common\HoverButton.h"

struct SPackageInfo4Studio;
class CInfoImportThread;
class CNotifySaveDlg;

class CEEPackageDlg : public CDialog
{
	DECLARE_DYNAMIC(CEEPackageDlg)
	DECLARE_MESSAGE_MAP()

public:
	CEEPackageDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEEPackageDlg();
	void SetParameter(CDocument* pDocument, CString szPackage=_T(""), CString szDrive=_T(""));

// Dialog Data
	enum { IDD = IDD_EE_PACKAGE };
	enum { eColName, eColDrive, eColRegister, eColUpdateDate, eColServerDate, eColUpdateUser, eColSite
		 , eDescript, eCategory, ePurpose, eHostType, eVertical, eResolution, ePublic, eVerify, eValidateDate
		 , eColEnd };

protected:
	CHoverButton m_bnRefresh;

	enum { eFilterCategory  	// 종류 (모닝,..)
		 , eFilterPurpose   	// 용도별 (교육용,..)
		 , eFilterHostType  	// 단말타입 (키오스크..)
		 , eFilterVertical  	// 가로/세로 방향 (가로,..)
		 , eFilterResolution	// 해상도 (1920x1080,..)
		 , eFilterPublic        // 공개여부
		 , eFilterVerify        // 승인여부
		 , eFilterMaxCnt    };
	CUTBListCtrlEx	m_lcFilter[eFilterMaxCnt];
	LONG			m_nFilter[eFilterMaxCnt];
	CString			m_strFilterContentsId;

	CComboBox		m_cbxIncludedContents;
	CDateTimeCtrl	m_dtStartDate;
	CDateTimeCtrl	m_dtEndDate;

	void InitFilterListCtrl();
	void InitList();
	void RefreshList(CString strSelPackage);
	bool LoadPackage(SPackageInfo4Studio*, BOOL bFromServer=FALSE);

	void LoadFilterData();
	void SaveFilterData();
	int	 GetSelectItemsList(CUTBListCtrlEx& lc, CStringArray& ar);
	CString	GetSelectItemsStringFromList(CUTBListCtrlEx& lc, CString strField);
	CString	GetSelectItemsStringFromList(CStringArray& ar, CString strField);

protected:
	CImageList m_ilPackage;
	CUTBListCtrlEx m_lcPackage;
	CString m_szSelPackage;
	CString m_szSelDrive;

	CDocument* m_pDocument;
	CNotifySaveDlg* m_pNotiDlg;
	CInfoImportThread* m_pThread;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedOk();
	afx_msg LRESULT OnImportComplete(WPARAM wParam, LPARAM lParam);
	afx_msg void OnNMDblclkListHostee(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonFromServer();
	afx_msg void OnLvnItemchangedListHostee(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnBnClickedBnRegister();
	afx_msg void OnCbnSelchangeCbIncludedContents();
};
