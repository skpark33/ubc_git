#pragma once
#include "afxwin.h"
#include "common/HoverButton.h"


// CRegistDlg 대화 상자입니다.

class CRegistDlg : public CDialog
{
	DECLARE_DYNAMIC(CRegistDlg)

public:
	CRegistDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRegistDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_REGIST_DLG };

	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;
	CString			m_strRegistKey;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
};
