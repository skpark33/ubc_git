// WebContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "WebContentsDialog.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "common/MD5Util.h"

// CWebContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CWebContentsDialog, CSubContentsDialog)

CWebContentsDialog::CWebContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CWebContentsDialog::IDD, pParent)
	, m_wndWeb (NULL, 0,false)
	, m_bPreviewMode(false)
{
	m_bVScroll = false;
	m_bHScroll = false;
	m_strURL = _T("http://");
	m_strFile = _T("");
	m_strLocation = _T("");
}

CWebContentsDialog::~CWebContentsDialog()
{
}

void CWebContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_URL, m_editURL);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_PLAY, m_btnPreviewPlay);
	DDX_Control(pDX, IDC_BUTTON_FILENAME, m_btnBrowserContentsFile);
	DDX_Control(pDX, IDC_BUTTON_DELFILE, m_btnDelFile);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);
	DDX_Control(pDX, IDC_EDIT_FELICA_URL, m_editFelicaUrl);
}

BEGIN_MESSAGE_MAP(CWebContentsDialog, CSubContentsDialog)
	ON_BN_CLICKED(IDC_BUTTON_FILENAME, OnBnClickedFileName)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_PLAY, &CWebContentsDialog::OnBnClickedButtonPreviewPlay)
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, &CWebContentsDialog::OnBnClickedPermanentCheck)
	ON_BN_CLICKED(IDC_WEBCONTENTS_VSCROLL, &CWebContentsDialog::OnBnClickedWebcontentsVscroll)
	ON_BN_CLICKED(IDC_WEBCONTENTS_HSCROLL, &CWebContentsDialog::OnBnClickedWebcontentsHscroll)
	ON_BN_CLICKED(IDC_BUTTON_DELFILE, &CWebContentsDialog::OnBnClickedDeletefile)
END_MESSAGE_MAP()

// CWebContentsDialog 메시지 처리기입니다.
BOOL CWebContentsDialog::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
//	m_btnPreviewPlay.LoadBitmap(IDB_BTN_VIEW, RGB(255, 255, 255));
	m_btnPreviewPlay.LoadBitmap(IDB_BTN_PLAY, RGB(255, 255, 255));
	m_btnPreviewPlay.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT001));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnBrowserContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btnBrowserContentsFile.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT002));

	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(15);
	
	m_editURL.SetWindowText(m_strURL);
	m_editContentsFileName.SetWindowText(m_strFile);

	CButton* pVCheck = (CButton*)GetDlgItem(IDC_WEBCONTENTS_VSCROLL);
	CButton* pHCheck = (CButton*)GetDlgItem(IDC_WEBCONTENTS_HSCROLL);
	pVCheck->SetCheck(m_bVScroll);
	pHCheck->SetCheck(m_bHScroll);

	m_btnDelFile.LoadBitmap(IDB_BTN_DELFILE, RGB(236, 233, 216));
	m_btnDelFile.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT003));

	//
	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);

	m_wndWeb.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	//m_wndWeb.ShowWindow(SW_SHOW);

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}//if

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CWebContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CSubContentsDialog::OnOK();
}

void CWebContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

//	CSubContentsDialog::OnCancel();
	GetParent()->PostMessage(WM_CLOSE);
}

void CWebContentsDialog::Stop()
{
	m_wndWeb.Stop(false);
}

bool CWebContentsDialog::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData(TRUE);

	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();
	
	info.strLocalLocation = m_strLocation;
	m_strURL = _T("");
	m_strFile = _T("");

	CString szTemp;
	m_editURL.GetWindowText(szTemp);
	if(szTemp.Left(7) != _T("http://") && szTemp.Left(8) != _T("https://") && szTemp.Left(7) != _T("file://"))
		m_strURL = _T("http://");
	m_strURL += szTemp;
	info.strComment[0] = m_strURL;

	m_editContentsFileName.GetWindowText(m_strFile);
	info.strFilename = m_strFile;
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	
	if(m_bVScroll)
		info.strComment[2] = _T("Vertical Scroll");
	else
		info.strComment[2] = _T("");

	if(m_bHScroll)
		info.strComment[1] = _T("Horizontal Scroll");
	else
		info.strComment[1] = _T("");

//	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
//	_splitpath(info.strLocation, dir, path, filename, ext);

//	info.strLocation.Format("%s%s", dir, path); 
//	info.strFilename.Format("%s%s", filename, ext); 

	if(!IsFitContentsName(info.strContentsName)) return false;
	if(info.nRunningTime == 0){
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
		return false;
	}
	if(info.strComment[0].IsEmpty()){
		UbcMessageBox(LoadStringById(IDS_WEBCONTENTSDIALOG_MSG001), MB_ICONSTOP);
		return false;
	}

	if(info.strComment[0].GetLength() < 10){
		UbcMessageBox(LoadStringById(IDS_WEBCONTENTSDIALOG_MSG002), MB_ICONSTOP);
		return false;
	}

	m_editFelicaUrl.GetWindowText(info.strComment[1]);
	info.strComment[1].Trim();
	if(!info.strComment[1].IsEmpty())
	{	
		CString strFelicaUrl = info.strComment[1];
		strFelicaUrl.MakeLower();
		if(strFelicaUrl.Find(_T("http://")) < 0)
		{
			UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG007), MB_ICONSTOP);
			return false;
		}
	}

	return true;
}

bool CWebContentsDialog::SetContentsInfo(CONTENTS_INFO& info)
{
	m_editContentsName.SetWindowText(info.strContentsName);
	m_strLocation = _T("");
	m_strURL = _T("");
	m_strFile = _T("");
	m_editFelicaUrl.SetWindowText(info.strComment[1]);

	if(info.strComment[0].IsEmpty()){
		m_strURL = _T("http://");
	}else{
		if(info.strComment[0].Left(7) != _T("http://") && info.strComment[0].Left(8) != _T("https://") && info.strComment[0].Left(7) != _T("file://"))
			m_strURL = _T("http://");
		m_strURL += info.strComment[0];
	}
	m_editURL.SetWindowText(m_strURL);

	m_strFile = info.strFilename;
	m_editContentsFileName.SetWindowText(m_strFile);

	if(info.strFilename.IsEmpty()){
		m_strLocation = _T("");
		info.nFilesize = 0;
	}else{
		m_strLocation = info.strLocalLocation;
	}

	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	if(info.strComment[2].IsEmpty()){
		m_bVScroll = false;
	}else{
		m_bVScroll = true;
	}
	((CButton*)GetDlgItem(IDC_WEBCONTENTS_VSCROLL))->SetCheck(m_bVScroll);

	if(info.strComment[1].IsEmpty()){
		m_bHScroll = false;
	}else{
		m_bHScroll = true;
	}
	((CButton*)GetDlgItem(IDC_WEBCONTENTS_HSCROLL))->SetCheck(m_bHScroll);

	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}

	UpdateData(FALSE);

	return true;
}

void CWebContentsDialog::OnBnClickedFileName()
{
	CString szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
	CString filter;
	filter.Format("All Image Files|%s|"
				  "Bitmap Files (*.bmp)|*.bmp|"
				  "JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;|"
				  "GIF Files (*.gif)|*.gif|"
				  "PCX Files (*.pcx)|*.pcx|"
				  "PNG Files (*.png)|*.png|"
				  "TIFF Files (*.tif)|*.tif;*.tiff||"
				 , szFileExts
				 );

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, CONTENTS_IMAGE)){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = _T("");
	m_editContentsFileName.SetSel(0, -1);

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);

	char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_splitpath(dlg.GetPathName(), drive, path, filename, ext);

	if(contents_name.GetLength() == 0)
		m_editContentsName.SetWindowText(filename);

	m_strLocation  = drive;
	m_strLocation += path;

	CFileStatus fs;
	if(CEnviroment::GetFileSize(dlg.GetPathName(), fs.m_size))
	{
		m_ulFileSize = fs.m_size;
	}

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(m_ulFileSize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
	}
	m_strFileMD5 = CString(szMd5);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CWebContentsDialog::OnBnClickedButtonPreviewPlay()
{
	//m_wndWeb.m_strMediaFullPath = m_editURL.GetValueString();
	SetPlayContents();
	m_wndWeb.Play();
}


void CWebContentsDialog::SetPlayContents()
{
	UpdateData(FALSE);
	m_wndWeb.CloseFile();

	CString strTmp;
	m_editContentsName.GetWindowText(strTmp);
	m_wndWeb.m_contentsName = strTmp;

	m_editContentsFileName.GetWindowText(strTmp);
	m_wndWeb.m_filename = strTmp;

	m_wndWeb.m_location = m_strLocation;
	m_wndWeb.m_strMediaFullPath = m_wndWeb.m_location.c_str() + strTmp;

	m_editURL.GetWindowText(strTmp);
	m_wndWeb.m_comment[0] = strTmp;

	CString strMin, strSec;
	m_editContentsPlayMinute.GetWindowText(strMin);
	m_editContentsPlaySecond.GetWindowText(strSec);
	m_wndWeb.m_runningTime = atoi(strMin)*60 + atoi(strSec);

	m_wndWeb.OpenFile(0);
}

void CWebContentsDialog::OnBnClickedPermanentCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//24시간을 설정
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText("0");
		m_editContentsPlaySecond.SetWindowText("15");

		m_editContentsPlayMinute.EnableWindow(TRUE);
		m_editContentsPlaySecond.EnableWindow(TRUE);
	}//if
}

void CWebContentsDialog::OnBnClickedWebcontentsVscroll()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_WEBCONTENTS_VSCROLL);
	if(pCheck->GetCheck())
		m_bVScroll = true;
	else
		m_bVScroll = false;
}

void CWebContentsDialog::OnBnClickedWebcontentsHscroll()
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_WEBCONTENTS_HSCROLL);
	if(pCheck->GetCheck())
		m_bHScroll = true;
	else
		m_bHScroll = false;
}

void CWebContentsDialog::OnBnClickedDeletefile()
{
	m_ulFileSize = 0;
	m_editContentsFileName.SetWindowText(_T(""));
//	m_staticContentsFileSize.SetWindowText(_T(""));
	m_strLocation = _T("");
	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	m_strFileMD5 = _T("");
}

BOOL CWebContentsDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CWebContentsDialog::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editURL.EnableWindow(bEnable);
	m_editContentsFileName.EnableWindow(bEnable);
	m_btnBrowserContentsFile.EnableWindow(bEnable);
	m_btnDelFile.EnableWindow(bEnable);

	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	GetDlgItem(IDC_PERMANENT_CHECK)->EnableWindow(bEnable);
	m_editFelicaUrl.EnableWindow(bEnable);
}
