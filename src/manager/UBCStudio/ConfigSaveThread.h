#pragma once



// CConfigSaveThread

class CConfigSaveThread : public CWinThread
{
	DECLARE_DYNCREATE(CConfigSaveThread)

protected:
	CConfigSaveThread();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CConfigSaveThread();

	void*	m_pParent;			///<Parent window
	void*	m_pParentDoc;		///<Parent document

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	void	SetThreadParam(void* pParent, void* pParentDoc);	///<Set parent params

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual int Run();
};


