// PackageView.cpp : implementation of the CPackageView class
// m_lcContents ,, m_lcContents

#include "stdafx.h"
#include "UBCStudio.h"
#include "UBCStudioDoc.h"
#include "DataContainer.h"
#include "ContentsDialog.h"
#include "PlayContentsDialog.h"
#include "SelectTemplateDialog.h"
#include "MainFrm.h"
#include "Enviroment.h"
#include "FtpUploadDlg.h"
#include "FileTranInfo.h"
#include "ColorPalatte.h"
#include "PackageView.h"
#include "WizardTreeDlg.h"
#include "GoToTemplateDlg.h"
#include "common/UbcGUID.h"
#include "PlayContentsExcelSave.h"
#include "CycleIterationDlg.h"
#include "TimeBaseIterationDlg.h"
#ifdef _UBCSTUDIO_PE_
#else
#include "DisplayConditionDlg.h"
#include "InputAdvertiserDlg.h"
#endif
#include <map>


// Modified by 정운형 2009-01-06 오전 11:35
// 변경내역 :  CLI 연동 수정
#include "Dbghelp.h"
// Modified by 정운형 2009-01-06 오전 11:35
// 변경내역 :  CLI 연동 수정

#include "ubccommon\ftpreportdlg.h"
#ifdef _UBCSTUDIO_EE_
#include "ubccopcommon\ftpmultisite.h"
//#include "ProcessingDlg.h"
#include "PublicContents/PublicContentsProcessingDlg.h"
#endif//_UBCSTUDIO_EE_

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

typedef map<int,PLAYCONTENTS_INFO*>   PLAYCONTENTS_INFO_MAP;
// CPackageView

IMPLEMENT_DYNCREATE(CPackageView, CFormView)

BEGIN_MESSAGE_MAP(CPackageView, CFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_NOTIFY(LVN_BEGINDRAG, IDC_LIST_CONTENTS, &CPackageView::OnLvnBegindragListContents)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONTENTS, &CPackageView::OnNMDblclkListContents)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_CONTENTS, &CPackageView::OnLvnKeydownListContents)
	ON_BN_CLICKED(IDC_BUTTON_ADD_CONTENTS, &CPackageView::OnBnClickedButtonAddContents)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_CONTENTS, &CPackageView::OnBnClickedButtonRemoveContents)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY_CONTENTS, &CPackageView::OnBnClickedButtonModifyContents)
	ON_BN_CLICKED(IDC_BUTTON_DOWNLOAD_CONTENTS, &CPackageView::OnBnClickedButtonDownloadContents)
	ON_BN_CLICKED(IDC_BUTTON_ADD_TEMPLATE, &CPackageView::OnBnClickedButtonAddTemplate)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_TEMPLATE, &CPackageView::OnBnClickedButtonRemoveTemplate)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_UP_TEMPLATE, &CPackageView::OnBnClickedButtonMoveUpTemplate)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_DOWN_TEMPLATE, &CPackageView::OnBnClickedButtonMoveDownTemplate)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW, &CPackageView::OnBnClickedButtonPreview)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_PREV_FRAME, &CPackageView::OnBnClickedButtonMovePrevFrame)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_NEXT_FRAME, &CPackageView::OnBnClickedButtonMoveNextFrame)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_PLAYCONTENTS, &CPackageView::OnTcnSelchangeTabPlayContents)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_CYCLE_PLAYCONTENTS, &CPackageView::OnLvnKeydownListCyclePlayContents)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CYCLE_PLAYCONTENTS, &CPackageView::OnNMDblclkListCyclePlayContents)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_TIME_BASE_PLAYCONTENTS, &CPackageView::OnLvnKeydownListTimeBasePlayContents)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_TIME_BASE_PLAYCONTENTS, &CPackageView::OnNMDblclkListTimeBasePlayContents)
	ON_BN_CLICKED(IDC_BUTTON_ADD_PLAYCONTENTS, &CPackageView::OnBnClickedButtonAddPlayContents)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_PLAYCONTENTS, &CPackageView::OnBnClickedButtonRemovePlayContents)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY_PLAYCONTENTS, &CPackageView::OnBnClickedButtonToExcel)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_UP_PLAYCONTENTS, &CPackageView::OnBnClickedButtonMoveUpPlayContents)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_DOWN_PLAYCONTENTS, &CPackageView::OnBnClickedButtonMoveDownPlayContents)
	ON_BN_CLICKED(IDC_BUTTON_SIZE_TOGGLE, &CPackageView::OnBnClickedButtonResizePlayContents)
	ON_MESSAGE(WM_ADD_CONTENTS, OnAddContents)
	ON_MESSAGE(WM_CLEAR_CONTENTS_SELECTION, OnClearContentsSelection)
	ON_MESSAGE(WM_TEMPLATE_FRAME_SELECT_CHANGED, OnTemplateFrameSelectChanged)
	ON_MESSAGE(WM_TEMPLATE_INFO_CHANGED, OnTemplateInfoChanged)
	ON_MESSAGE(WM_FRAME_INFO_CHANGED, OnFrameInfoChanged)
	ON_MESSAGE(WM_PLAY_TEMPLATE_LIST_CHANGED, OnPlayTemplateListChanged)
	ON_MESSAGE(WM_CONTENTS_LIST_CHANGED, OnContentsListChanged)

	ON_MESSAGE(WM_SAVE_CONFIG_COMPELETE, OnConfigSaveComplete)
	ON_BN_CLICKED(IDC_BUTTON_COPY_TEMPLATE, &CPackageView::OnBnClickedButtonCopyTemplate)
	ON_NOTIFY(LVN_BEGINDRAG, IDC_LIST_CYCLE_PLAYCONTENTS, &CPackageView::OnLvnBegindragListCyclePlayContents)
	ON_NOTIFY(TVN_KEYDOWN, IDC_TREE_RELATION, &CPackageView::OnTvnKeydownTreeRelation)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_RELATION, &CPackageView::OnBnClickedButtonRemoveRelation)
	ON_NOTIFY(NM_CLICK, IDC_LIST_CYCLE_PLAYCONTENTS, &CPackageView::OnNMClickListCyclePlayContents)
	ON_NOTIFY(NM_CLICK, IDC_TREE_RELATION, &CPackageView::OnNMClickTreeRelation)
	ON_MESSAGE(WM_CHECKCOMBO_MSG, &CPackageView::OnContentsFilter)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_MANAGER, &CPackageView::OnBnClickedButtonContentsManager)
	ON_BN_CLICKED(IDC_BUTTON_WIZARD_TREE, &CPackageView::OnBnClickedButtonWizardTree)
	ON_NOTIFY(NM_RCLICK, IDC_TREE_RELATION, &CPackageView::OnNMRclickTreeRelation)
	ON_COMMAND(ID_GOTO_TEMPLATE, &CPackageView::OnGotoTemplate)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_TIME_BASE_PLAYCONTENTS, &CPackageView::OnNMRclickListTimeBasePlayContents)
	ON_COMMAND(ID_TIMEBASERCLICK_ITERATION, &CPackageView::OnTimebaserclickIteration)
	ON_COMMAND(ID_CIRCLEBASERCLICK_COPY, &CPackageView::OnCirclebaserclickCopy)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CYCLE_PLAYCONTENTS, &CPackageView::OnNMRclickListCyclePlaycontents)
	ON_BN_CLICKED(IDC_BUTTON_CONDITION, &CPackageView::OnBnClickedButtonCondition)
	ON_BN_CLICKED(IDC_BUTTON_H_SIZE_TOGGLE, &CPackageView::OnBnClickedButtonHSizeToggle)
	ON_BN_CLICKED(IDC_BT_NAME_FILTER, &CPackageView::OnBnClickedBtNameFilter)
END_MESSAGE_MAP()

// CPackageView construction/destruction

CPackageView::CPackageView()
:	CFormView(CPackageView::IDD)
,	m_reposControl (this)
,	m_ilContentDrag(NULL)
,   m_ilPlayContentsDrag(NULL)
,	m_wndTemplate (2, CTemplate_Wnd::TEMPLATE_WND_PACKAGE, false)
,	m_wndFrame(3, CFrame_Wnd::WND_PACKAGE, false)
,   m_hSelectItem(NULL)
{
	// TODO: add construction code here
	m_toggle=0;
	m_h_toggle=-1;
	m_selectedContents=NULL;
	m_dragStartPoint = CPoint(0,0);
	m_nPrevDropIndex = -1;
}

CPackageView::~CPackageView()
{
}

void CPackageView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CONTENTS, m_lcContents);
	DDX_Control(pDX, IDC_BUTTON_ADD_CONTENTS, m_btnAddContents);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_CONTENTS, m_btnRemoveContents);
	DDX_Control(pDX, IDC_BUTTON_MODIFY_CONTENTS, m_btnModifyContents);
	DDX_Control(pDX, IDC_BUTTON_DOWNLOAD_CONTENTS, m_btnDownloadContents);
	DDX_Control(pDX, IDC_STATIC_TEMPLATE, m_frameTemplate);
	DDX_Control(pDX, IDC_BUTTON_ADD_TEMPLATE, m_btnAddTemplate);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_TEMPLATE, m_btnRemoveTemplate);
	DDX_Control(pDX, IDC_BUTTON_MOVE_UP_TEMPLATE, m_btnMoveUpTemplate);
	DDX_Control(pDX, IDC_BUTTON_MOVE_DOWN_TEMPLATE, m_btnMoveDownTemplate);
	DDX_Control(pDX, IDC_BUTTON_COPY_TEMPLATE, m_btnCopyTemplate);
	DDX_Control(pDX, IDC_STATIC_FRAME, m_frameFrame);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW, m_btnPreview);
	DDX_Control(pDX, IDC_BUTTON_MOVE_PREV_FRAME, m_btnMovePrevFrame);
	DDX_Control(pDX, IDC_BUTTON_MOVE_NEXT_FRAME, m_btnMoveNextFrame);
	DDX_Control(pDX, IDC_TAB_PLAYCONTENTS, m_tabPlayContents);
	DDX_Control(pDX, IDC_LIST_CYCLE_PLAYCONTENTS, m_lcCyclePlayContents);
	DDX_Control(pDX, IDC_LIST_TIME_BASE_PLAYCONTENTS, m_lcTimeBasePlayContents);
	DDX_Control(pDX, IDC_BUTTON_ADD_PLAYCONTENTS, m_btnAddPlayContents);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_PLAYCONTENTS, m_btnRemovePlayContents);
	DDX_Control(pDX, IDC_BUTTON_MODIFY_PLAYCONTENTS, m_btnModifyPlayContents);
	DDX_Control(pDX, IDC_BUTTON_MOVE_UP_PLAYCONTENTS, m_btnMoveUpPlayContents);
	DDX_Control(pDX, IDC_BUTTON_MOVE_DOWN_PLAYCONTENTS, m_btnMoveDownPlayContents);
	//DDX_Control(pDX, IDC_STATIC_PLAYCONTENTS, m_framePlayContents);
	DDX_Control(pDX, IDC_STATIC_TOTALSIZE, m_stTotalSize);
	DDX_Control(pDX, IDC_TAB_RELATION, m_tabRelation);
	DDX_Control(pDX, IDC_TREE_RELATION, m_treeRelation);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_RELATION, m_btnRemoveRelation);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_FILTER, m_cbContentsFilter);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_MANAGER, m_btnContentsManager);
	DDX_Control(pDX, IDC_BUTTON_WIZARD_TREE, m_btnWizardTree);
	DDX_Control(pDX, IDC_BUTTON_SIZE_TOGGLE, m_btnSizeToggle);
	DDX_Control(pDX, IDC_BUTTON_CONDITION, m_btnCondition);
	DDX_Control(pDX, IDC_BUTTON_H_SIZE_TOGGLE, m_btnHSizeToggle);
	DDX_Control(pDX, IDC_EDIT_NAME_FILTER, m_editNameFilter);
	DDX_Control(pDX, IDC_BT_NAME_FILTER, m_btNameFilter);
	DDX_Control(pDX, IDC_STATIC_NAME_FILTER, m_staticNameFilter);
}

BOOL CPackageView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CPackageView::OnDestroy()
{
	CFormView::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	UnregisterDND();
}

void CPackageView::OnInitialUpdate()
{
	TraceLog(("OnInitialUpdate(start)"));
	CFormView::OnInitialUpdate();

	InitFrameColorList();

	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnAddContents.LoadBitmap(IDB_BTN_PLUS, RGB(255, 255, 255));
	m_btnRemoveContents.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnModifyContents.LoadBitmap(IDB_BTN_EDIT, RGB(255, 255, 255));

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE){
		m_btnDownloadContents.LoadBitmap(IDB_CONT_DOWN, RGB(255, 255, 255));
		m_btnContentsManager.LoadBitmap(IDB_CONT_MANAGER, RGB(255, 255, 255));
	}else{
		m_btnDownloadContents.EnableWindow(FALSE);
		m_btnDownloadContents.ShowWindow(SW_HIDE);

		m_btnContentsManager.EnableWindow(FALSE);
		m_btnContentsManager.ShowWindow(SW_HIDE);
	}
	
	// 0000783: 플래쉬 위자드 기능을 대폭 수정한다.
	m_btnWizardTree.LoadBitmap(IDB_BTN_TREE, RGB(255, 255, 255));
#ifdef _UBCSTUDIO_PE_
	m_btnCondition.ShowWindow(SW_HIDE);
#else
	m_btnCondition.LoadBitmap(IDB_BTN_CONDITION, RGB(255, 255, 255));
#endif
	m_btNameFilter.LoadBitmap(IDB_BTN_UPDATE, RGB(255, 255, 255));

	m_btnAddTemplate.LoadBitmap(IDB_BTN_PLUS, RGB(255, 255, 255));
	m_btnRemoveTemplate.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnMoveUpTemplate.LoadBitmap(IDB_BTN_PREVIOUS, RGB(255, 255, 255));
	m_btnMoveDownTemplate.LoadBitmap(IDB_BTN_NEXT, RGB(255, 255, 255));
	m_btnCopyTemplate.LoadBitmap(IDB_BTN_COPY, RGB(255, 255, 255));

	m_btnPreview.LoadBitmap(IDB_BTN_VIEW, RGB(255, 255, 255));
	m_btnMovePrevFrame.LoadBitmap(IDB_BTN_COLUMNS_PREVIOUS, RGB(255, 255, 255));
	m_btnMoveNextFrame.LoadBitmap(IDB_BTN_COLUMNS_NEXT, RGB(255, 255, 255));

	m_btnAddPlayContents.LoadBitmap(IDB_BTN_PLUS, RGB(255, 255, 255));
	m_btnRemovePlayContents.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnModifyPlayContents.LoadBitmap(IDB_BTN_EXCEL_2, RGB(255, 255, 255));
	m_btnMoveUpPlayContents.LoadBitmap(IDB_BTN_UP, RGB(255, 255, 255));
	m_btnMoveDownPlayContents.LoadBitmap(IDB_BTN_DOWN, RGB(255, 255, 255));

	m_btnRemoveRelation.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));

	m_btnSizeToggle.LoadBitmap(IDB_BTN_SIZETOGGLE, RGB(255, 255, 255));//skpark
	m_btnHSizeToggle.LoadBitmap(IDB_BTN_H_SIZETOGGLE, RGB(255, 0, 255));//skpark

	//Tool tip
	m_btnAddContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN001));
	m_btnRemoveContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN002));
	m_btnModifyContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN003));

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE){
		m_btnDownloadContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN004));
		m_btnContentsManager.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN019));
	}

	// 0000783: 플래쉬 위자드 기능을 대폭 수정한다.
	m_btnWizardTree.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN020));
	if(CEnviroment::eUBGOLF == GetEnvPtr()->m_Customer)	{
		m_btnCondition.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN022));
	}else{
		m_btnCondition.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN021));
	}

	m_btnAddTemplate.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN005));
	m_btnRemoveTemplate.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN006));
	m_btnMoveUpTemplate.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN007));
	m_btnMoveDownTemplate.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN008));
	m_btnCopyTemplate.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN017));

	m_btnPreview.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN009));
	m_btnMovePrevFrame.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN010));
	m_btnMoveNextFrame.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN011));

	m_btnAddPlayContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN012));
	m_btnRemovePlayContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN013));
	m_btnModifyPlayContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN014));
	m_btnMoveUpPlayContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN015));
	m_btnMoveDownPlayContents.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN016));

	m_btnRemoveRelation.SetToolTipText(LoadStringById(IDS_PLAYCONTENTSVIEW_BTN018));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

//	ResizeParentToFit();
	{
		// determine current size of the client area as if no scrollbars present
		CRect rectClient;
		GetWindowRect(rectClient);
		CRect rect = rectClient;
		CalcWindowRect(rect);
		rectClient.left += rectClient.left - rect.left;
		rectClient.top += rectClient.top - rect.top;
		rectClient.right -= rect.right - rectClient.right;
		rectClient.bottom -= rect.bottom - rectClient.bottom;
		rectClient.OffsetRect(-rectClient.left, -rectClient.top);
		ASSERT(rectClient.left == 0 && rectClient.top == 0);

		// determine desired size of the view
		CRect rectView(0, 0, m_totalDev.cx, m_totalDev.cy);
		{
			if (rectClient.right <= m_totalDev.cx)
				rectView.right = rectClient.right;
			if (rectClient.bottom <= m_totalDev.cy)
				rectView.bottom = rectClient.bottom;
		}
		CalcWindowRect(rectView, CWnd::adjustOutside);
		rectView.OffsetRect(-rectView.left, -rectView.top);
		ASSERT(rectView.left == 0 && rectView.top == 0);
		{
			if (rectClient.right <= m_totalDev.cx)
				rectView.right = rectClient.right;
			if (rectClient.bottom <= m_totalDev.cy)
				rectView.bottom = rectClient.bottom;
		}

		rectView.DeflateRect(::GetSystemMetrics(SM_CXDLGFRAME)-1, ::GetSystemMetrics(SM_CYDLGFRAME)-1);

		m_reposControl.SetParentRect(CRect(0,0,rectView.Width(), rectView.Height()));

		SetScrollSizes(MM_TEXT, CSize(1,1));
	}

	CRect rect;
	//
	m_frameTemplate.GetWindowRect(rect);
	ScreenToClient(rect);

	m_wndTemplate.SetFocusWindow(&m_btnAddTemplate);
	m_wndTemplate.SetDocument(GetDocument());
	m_wndTemplate.CreateEx(WS_EX_CLIENTEDGE, NULL, "Template", WS_CHILD | WS_VISIBLE | WS_HSCROLL, rect, this, 0xfeff);

	//
	m_frameFrame.GetWindowRect(rect);
	ScreenToClient(rect);

	m_wndFrame.SetDocument(GetDocument());
	m_wndFrame.CreateEx(WS_EX_CLIENTEDGE, NULL, "Frame", WS_CHILD | WS_VISIBLE, rect, this, 0xfefe);

	//
	m_lcCyclePlayContents.SetParent(this);
	m_lcTimeBasePlayContents.SetParent(this);

	m_stTotalSize.SetWindowText("0 Byte");

	// void	AddControl(CWnd* pControl, int nLeft, int nTop, int nRight, int nBottom, int nHorzCount=1, int nVertCount=1);
	m_reposControl.AddControl(&m_cbContentsFilter , REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_reposControl.AddControl(&m_editNameFilter   , REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_reposControl.AddControl(&m_btNameFilter	  , REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_reposControl.AddControl(&m_lcContents       , REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnHSizeToggle  , REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_MOVE);  //skpark

	m_reposControl.AddControl(&m_btnAddContents   , REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnRemoveContents, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnModifyContents, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_stTotalSize      , REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		m_reposControl.AddControl(&m_btnDownloadContents, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
		m_reposControl.AddControl(&m_btnContentsManager, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	}

	// 0000783: 플래쉬 위자드 기능을 대폭 수정한다.
	m_reposControl.AddControl(&m_btnWizardTree, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnCondition, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);


	m_reposControl.AddControl(&m_frameTemplate      , REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_wndTemplate        , REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnAddTemplate     , REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnRemoveTemplate  , REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnMoveUpTemplate  , REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnMoveDownTemplate, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnCopyTemplate    , REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);

	m_reposControl.AddControl(&m_frameFrame      , REPOS_FIX , REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_wndFrame        , REPOS_FIX , REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnPreview      , REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX );
	m_reposControl.AddControl(&m_btnMovePrevFrame, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX );
	m_reposControl.AddControl(&m_btnMoveNextFrame, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX );

	m_reposControl.AddControl(&m_tabPlayContents        , REPOS_FIX , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	//m_reposControl.AddControl(&m_framePlayContents      , REPOS_FIX , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_lcCyclePlayContents    , REPOS_FIX , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_lcTimeBasePlayContents , REPOS_FIX , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnAddPlayContents     , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnRemovePlayContents  , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnModifyPlayContents  , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnMoveUpPlayContents  , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnMoveDownPlayContents, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.AddControl(&m_btnSizeToggle  , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);  //skpark

	m_reposControl.AddControl(&m_tabRelation        , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_treeRelation       , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnRemoveRelation  , REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_tabRelation.InsertItem(0, LoadStringById(IDS_PLAYCONTENTSVIEW_REL001), -1);

	m_treeRelation.SetImageList(&m_ilFrameColorList, TVSIL_NORMAL);

	m_reposControl.Move();

	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_VIDEO         +1], CONTENTS_VIDEO         );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_IMAGE         +1], CONTENTS_IMAGE         );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_SMS           +1], CONTENTS_SMS           );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_FLASH         +1], CONTENTS_FLASH         );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_TEXT          +1], CONTENTS_TEXT          );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_WEBBROWSER    +1], CONTENTS_WEBBROWSER    );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_PPT           +1], CONTENTS_PPT           );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_TV            +1], CONTENTS_TV            );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_RSS           +1], CONTENTS_RSS           );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_WIZARD        +1], CONTENTS_WIZARD        );
	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_ETC           +1], CONTENTS_ETC           );
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_PROMOTION     ], CONTENTS_PROMOTION     );
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_TICKER        ], CONTENTS_TICKER        );
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_PHONE         ], CONTENTS_PHONE         );
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_WEBCAM        ], CONTENTS_WEBCAM        );
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_CLOCK         ], CONTENTS_CLOCK         );
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_FLASH_TEMPLATE], CONTENTS_FLASH_TEMPLATE);
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_DYNAMIC_FLASH ], CONTENTS_DYNAMIC_FLASH );
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_TYPING        ], CONTENTS_TYPING        );
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_HWP           ], CONTENTS_HWP           );
//	m_cbContentsFilter.AddString(CONTENTS_TYPE_TO_STRING[CONTENTS_EXCEL         ], CONTENTS_EXCEL         );
	m_cbContentsFilter.CheckAll(TRUE);

	//
	m_lcContents.SetDocument(GetDocument());
	m_lcContents.Initialize();

	//
	m_tabPlayContents.InitImageListHighColor(IDB_TAB_LIST, RGB(192,192,192));
	m_tabPlayContents.InsertItem(0, LoadStringById(IDS_PLAYCONTENTSVIEW_LST001), 0);
//	m_tabPlayContents.InsertItem(1, LoadStringById(IDS_PLAYCONTENTSVIEW_LST002), 1);

	//
	CBitmap bmp1;
	bmp1.LoadBitmap(IDB_CONTENTS_LIST16);
	CBitmap bmp2;
	bmp2.LoadBitmap(IDB_CONTENTS_LIST16_ERROR);
	CBitmap bmp3;
	bmp3.LoadBitmap(IDB_CONTENTS_LIST16_SERVER);
	CBitmap bmp4;
	bmp4.LoadBitmap(IDB_PLAY_CONTENTS_LIST);

	if(m_ilContentsList16.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilContentsList16.Add(&bmp1, RGB(255,255,255)); //바탕의 회색이 마스크
		m_ilContentsList16.Add(&bmp2, RGB(255,255,255)); //바탕의 회색이 마스크
		m_ilContentsList16.Add(&bmp3, RGB(255,255,255)); //바탕의 회색이 마스크
		m_ilContentsList16.Add(&bmp4, RGB(255,255,255)); //바탕의 회색이 마스크
	}

	//
	m_lcCyclePlayContents.SetExtendedStyle(m_lcCyclePlayContents.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_HEADERDRAGDROP | LVS_EX_SUBITEMIMAGES );
	m_lcCyclePlayContents.SetImageList(&m_ilContentsList16, LVSIL_SMALL);
	m_lcCyclePlayContents.InsertColumn(eDateTime      , LoadStringById(IDS_PLAYCONTENTSVIEW_LST003), LVCFMT_LEFT, 150);
	m_lcCyclePlayContents.InsertColumn(eTimeZone      , LoadStringById(IDS_PLAYCONTENTSVIEW_LST004), LVCFMT_LEFT, 65);
	m_lcCyclePlayContents.InsertColumn(eContentsType  , LoadStringById(IDS_PLAYCONTENTSVIEW_LST005), LVCFMT_LEFT, 90);
	m_lcCyclePlayContents.InsertColumn(eContentsName  , LoadStringById(IDS_PLAYCONTENTSVIEW_LST006), LVCFMT_LEFT, 150);
	m_lcCyclePlayContents.InsertColumn(eRunningTime   , LoadStringById(IDS_PLAYCONTENTSVIEW_LST007), LVCFMT_LEFT, 50);
	m_lcCyclePlayContents.InsertColumn(eFileName      , LoadStringById(IDS_PLAYCONTENTSVIEW_LST008), LVCFMT_LEFT, 150);
	m_lcCyclePlayContents.InsertColumn(eSoundVolume   , LoadStringById(IDS_PLAYCONTENTSVIEW_LST009), LVCFMT_LEFT, 50);
	m_lcCyclePlayContents.InsertColumn(eParentPlayContents, LoadStringById(IDS_PLAYCONTENTSVIEW_LST011), LVCFMT_LEFT, 50);
	m_lcCyclePlayContents.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcCyclePlayContents.SetSortEnable(false);

	m_lcTimeBasePlayContents.SetExtendedStyle(m_lcTimeBasePlayContents.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_HEADERDRAGDROP | LVS_EX_SUBITEMIMAGES );
	m_lcTimeBasePlayContents.SetImageList(&m_ilContentsList16, LVSIL_SMALL);
	m_lcTimeBasePlayContents.InsertColumn(ePeriod        , LoadStringById(IDS_PLAYCONTENTSVIEW_LST012), LVCFMT_LEFT, 150);
	m_lcTimeBasePlayContents.InsertColumn(eTime          , LoadStringById(IDS_PLAYCONTENTSVIEW_LST013), LVCFMT_LEFT, 115);
	m_lcTimeBasePlayContents.InsertColumn(eContentsType  , LoadStringById(IDS_PLAYCONTENTSVIEW_LST005), LVCFMT_LEFT, 90);
	m_lcTimeBasePlayContents.InsertColumn(eContentsName  , LoadStringById(IDS_PLAYCONTENTSVIEW_LST006), LVCFMT_LEFT, 150);
	m_lcTimeBasePlayContents.InsertColumn(eRunningTime   , LoadStringById(IDS_PLAYCONTENTSVIEW_LST007), LVCFMT_LEFT, 50);
	m_lcTimeBasePlayContents.InsertColumn(eFileName      , LoadStringById(IDS_PLAYCONTENTSVIEW_LST008), LVCFMT_LEFT, 150);
	m_lcTimeBasePlayContents.InsertColumn(eSoundVolume   , LoadStringById(IDS_PLAYCONTENTSVIEW_LST009), LVCFMT_LEFT, 50);
	m_lcTimeBasePlayContents.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcTimeBasePlayContents.SetSortEnable(true);
	//
	LRESULT result;
	OnTcnSelchangeTabPlayContents(NULL, &result);

	RefreshRelation();

	//
	RegisterDND();

	CMainFrame* pFrm = (CMainFrame*)AfxGetMainWnd();
	CString strHostName = pFrm->GetHostName();
	strHostName.Replace(SAMPLE_FILE_KEY, "");

	// 2010.10.06 스튜디오의 타이틀에 콘텐츠 패키지경로 보여주기
	CString strDrive = pFrm->GetSourceDirive();
	strDrive.MakeUpper();

	CString strTitle;
	if(strHostName == "")
	{
		strTitle = LoadStringById(IDS_PLAYCONTENTSVIEW_STR001);
	}
	else
	{
		strTitle.Format(LoadStringById(IDS_PLAYCONTENTSVIEW_STR002), strDrive, strHostName);

		// <!-- 타이틀바 뒤에 (공개/비공개) 추가
		strTitle += " - (";
		strTitle += ( GetEnvPtr()->m_PackageInfo.bIsPublic ? LoadStringById(IDS_EEPACKAGEDLG_STR001) : LoadStringById(IDS_EEPACKAGEDLG_STR002) );
		strTitle += ")";
		// -->
	}//if

	GetDocument()->SetTitle(strTitle);

	// 창일향은 기본적으로 하나의 템플릿만을 사용할 수 있다.  이는 지울수도, 새로 추가할 수도 없으므로,  레이아웃 스테이지의 “+”,”-“ 를 비롯한 모든 버튼은 disable 해야 한다
	m_btnAddTemplate.EnableWindow(CEnviroment::eADASSET != GetEnvPtr()->m_Customer);
	m_btnRemoveTemplate.EnableWindow(CEnviroment::eADASSET != GetEnvPtr()->m_Customer);
	m_btnMoveUpTemplate.EnableWindow(CEnviroment::eADASSET != GetEnvPtr()->m_Customer);
	m_btnMoveDownTemplate.EnableWindow(CEnviroment::eADASSET != GetEnvPtr()->m_Customer);
	m_btnCopyTemplate.EnableWindow(CEnviroment::eADASSET != GetEnvPtr()->m_Customer);

	InitHSizeToggle();
	m_h_toggle=0;

	m_staticNameFilter.ShowWindow(SW_HIDE);
	m_editNameFilter.ShowWindow(SW_HIDE);
	m_btNameFilter.ShowWindow(SW_HIDE);

	//m_toggle--;
	//this->m_btnSizeToggle.ShowWindow(SW_SHOW);
	//OnBnClickedButtonResizePlayContents();
	TraceLog(("OnInitialUpdate(end)"));
}

// CPackageView diagnostics

#ifdef _DEBUG
void CPackageView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPackageView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CUBCStudioDoc* CPackageView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCStudioDoc)));
	return (CUBCStudioDoc*)m_pDocument;
}
#endif //_DEBUG


// CPackageView message handlers

void CPackageView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();

	if(m_toggle > 0){
		m_toggle++;
		ShowItems(SW_SHOW); //skpark
		InvalidateItems(); //skpark
	}

	if(m_h_toggle >= 1 && m_h_toggle %2 == 1){
		HSizeToggle(0);
		m_h_toggle = 0;
	}
	if(m_h_toggle >= 0){
		InitHSizeToggle();
	}
}

void CPackageView::OnLvnBegindragListContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
	if(pNMHDR->hwndFrom != m_lcContents.m_hWnd) return;

	int count = m_lcContents.GetSelectedCount();
	if (count == 1)
	{
		int idx = m_lcContents.GetNextItem(-1, LVIS_SELECTED);
		CPoint pt;
		m_ilContentDrag = m_lcContents.CreateDragImage(idx, &pt);
		if(m_ilContentDrag != NULL)
		{
			CPoint ptAction(pNMLV->ptAction);
			CRect rcItem;
			m_lcContents.GetItemRect(idx, &rcItem, LVIR_BOUNDS);
			m_ilContentDrag->BeginDrag(0, CPoint(ptAction.x - rcItem.left, ptAction.y - rcItem.top));

			m_lcContents.ClientToScreen(&ptAction);
			ScreenToClient(&ptAction);
			m_ilContentDrag->DragEnter(this, ptAction);
			SetCapture();
		}
	}
	else
	{
		m_ilContentDrag = new CImageList;
		m_ilContentDrag->Create(IDB_MULTI_DRAG, 30, 0, RGB(255,0,255));
		if(m_ilContentDrag != NULL)
		{
			m_ilContentDrag->BeginDrag(0, CPoint(0, 0));
			CPoint ptAction(pNMLV->ptAction);
			m_lcContents.ClientToScreen(&ptAction);
			ScreenToClient(&ptAction);
			m_ilContentDrag->DragEnter(this, ptAction);
			SetCapture();
		}
	}
}

void CPackageView::OnMouseMove(UINT nFlags, CPoint point)
{
	CFormView::OnMouseMove(nFlags, point);

	CPoint ptScreen(point);
	ClientToScreen(&ptScreen);
	CWnd* pWndDrop = WindowFromPoint(ptScreen);
	bool bCopyEnable = false;

	if( m_nPrevDropIndex >= 0 )
	{
		m_lcCyclePlayContents.SetItemState (m_nPrevDropIndex, 0, LVIS_DROPHILITED);
		// Redraw previous item
		m_lcCyclePlayContents.RedrawItems (m_nPrevDropIndex, m_nPrevDropIndex);
	}

	if(m_ilContentDrag != NULL)
	{
		m_ilContentDrag->DragShowNolock(FALSE);

		// 순환 및 정시 플레이콘텐츠리스트로 드래그할때
		if((pWndDrop->m_hWnd == m_lcCyclePlayContents.m_hWnd || pWndDrop->m_hWnd == m_lcTimeBasePlayContents.m_hWnd)
			&& strlen(GetDocument()->GetSelectFrameId()) != 0 )
		{
			bCopyEnable = true;
			if(m_wndFrame.HitTest2(CPoint(-1,-1)) == false)
			{
				m_ilContentDrag->DragShowNolock(TRUE);
				m_ilContentDrag->DragMove(point);
			}

			// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
			POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
			while(pos != NULL)
			{
				int nItem = m_lcContents.GetNextSelectedItem(pos);
				CONTENTS_INFO* pContents = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
				CString strContentsId = (pContents ? pContents->strId : _T(""));

				if(!GetDocument()->CheckContentsCanBeAddedToFrame(GetDocument()->GetSelectTemplateId(), GetDocument()->GetSelectFrameId(), strContentsId))
				{
					bCopyEnable = false;
					break;
				}
			}
		}
		// 프래임안으로 드래그할때
		else if(pWndDrop->m_hWnd == m_wndFrame.m_hWnd)
		{
			bCopyEnable = true;
			CPoint ptClient = ptScreen;
			m_wndFrame.ScreenToClient(&ptClient);
			if(m_wndFrame.HitTest2(ptClient) == false)
			{
				m_ilContentDrag->DragShowNolock(TRUE);
				m_ilContentDrag->DragMove(point);
			}

			if(strlen(m_wndFrame.GetDropedFrameId()) == 0)
			{
				bCopyEnable = false;
			}
			else
			{
				// 0000543: "고화질 동영상용 프레임". 속성을 추가한다.
				POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
				while(pos != NULL)
				{
					int nItem = m_lcContents.GetNextSelectedItem(pos);
					CONTENTS_INFO* pContents = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
					CString strContentsId = (pContents ? pContents->strId : _T(""));

					if(!GetDocument()->CheckContentsCanBeAddedToFrame(GetDocument()->GetSelectTemplateId(), m_wndFrame.GetDropedFrameId(), strContentsId))
					{
						bCopyEnable = false;
					}
				}
			}
		}
		// 그외의 화면이나 컨트롤로 드래그할 경우
		else
		{
			if(m_wndFrame.HitTest2(CPoint(-1,-1)) == false)
			{
				m_ilContentDrag->DragShowNolock(TRUE);
				m_ilContentDrag->DragMove(point);
			}

			m_wndFrame.Invalidate();
		}

		SetCursor(LoadCursor(NULL, (bCopyEnable ? IDC_ARROW : IDC_NO)));
	}
	else if(m_ilPlayContentsDrag != NULL)
	{
		m_ilPlayContentsDrag->DragShowNolock(FALSE);

		// 플레이콘텐츠 관계 트리안으로 드래그할때
		if(pWndDrop->m_hWnd == m_treeRelation.m_hWnd)
		{
			TVHITTESTINFO treeinfo;
			memset(&treeinfo, 0x00, sizeof(TVHITTESTINFO));
			treeinfo.pt = ptScreen;
			m_treeRelation.ScreenToClient(&treeinfo.pt);
			m_treeRelation.HitTest(&treeinfo);

			if( TVHT_ONITEM & treeinfo.flags &&
				treeinfo.hItem != NULL       &&
				m_treeRelation.GetParentItem(treeinfo.hItem) == NULL )
			{
				//m_treeRelation.Select(treeinfo.hItem, TVGN_DROPHILITE);
				m_treeRelation.SelectItem(treeinfo.hItem);
				m_treeRelation.Expand(treeinfo.hItem, TVE_EXPAND);

				bCopyEnable = true;

				m_ilPlayContentsDrag->DragShowNolock(TRUE);
				m_ilPlayContentsDrag->DragMove(point);
			}
		}
		// 순환 플레이콘텐츠리스트로 드래그할때
		else if((pWndDrop->m_hWnd == m_lcCyclePlayContents.m_hWnd )
			&& strlen(GetDocument()->GetSelectFrameId()) != 0 )
		{
			bCopyEnable = true;
			if(m_wndFrame.HitTest2(CPoint(-1,-1)) == false)
			{
				m_ilContentDrag->DragShowNolock(TRUE);
				m_ilContentDrag->DragMove(point);
			}

			// Get the item that is below cursor
			m_lcCyclePlayContents.ScreenToClient(&ptScreen);
			UINT uFlags;
			m_nPrevDropIndex = m_lcCyclePlayContents.HitTest(ptScreen, &uFlags);

			if( m_nPrevDropIndex >= 0 )
			{
				// Highlight it
				m_lcCyclePlayContents.SetItemState(m_nPrevDropIndex, LVIS_DROPHILITED, LVIS_DROPHILITED);
				// Redraw item
				m_lcCyclePlayContents.RedrawItems(m_nPrevDropIndex, m_nPrevDropIndex);
				m_lcCyclePlayContents.UpdateWindow();
			}
		}
		// 그외의 화면이나 컨트롤로 드래그할 경우
		else
		{
			m_ilPlayContentsDrag->DragShowNolock(TRUE);
			m_ilPlayContentsDrag->DragMove(point);
		}

		SetCursor(LoadCursor(NULL, (bCopyEnable ? IDC_ARROW : IDC_NO)));
	}
}

void CPackageView::OnLButtonUp(UINT nFlags, CPoint point)
{
	CString szTemp;
	CFormView::OnLButtonUp(nFlags, point);

	ReleaseCapture();

	CPoint ptScreen(point);
	ClientToScreen(&ptScreen);
	CWnd* pWndDrop = WindowFromPoint(ptScreen);
	if (!pWndDrop) return;

	if( m_nPrevDropIndex >= 0 )
	{
		m_lcCyclePlayContents.SetItemState (m_nPrevDropIndex, 0, LVIS_DROPHILITED);
		// Redraw previous item
		m_lcCyclePlayContents.RedrawItems (m_nPrevDropIndex, m_nPrevDropIndex);
	}
	m_nPrevDropIndex = -1;

	if(m_ilPlayContentsDrag != NULL)
	{
		m_ilPlayContentsDrag->EndDrag();
		delete m_ilPlayContentsDrag;
		m_ilPlayContentsDrag = NULL;

		if( pWndDrop->m_hWnd == m_treeRelation.m_hWnd ) {;

			TVHITTESTINFO treeinfo;
			memset(&treeinfo, 0x00, sizeof(TVHITTESTINFO));
			treeinfo.pt = ptScreen;
			m_treeRelation.ScreenToClient(&treeinfo.pt);
			m_treeRelation.HitTest(&treeinfo);

			if( TVHT_ONITEM & treeinfo.flags &&
				treeinfo.hItem != NULL       &&
				m_treeRelation.GetParentItem(treeinfo.hItem) == NULL )
			{
				int nIndex = m_lcCyclePlayContents.GetNextItem(-1, LVIS_SELECTED);

				PLAYCONTENTS_INFO* pInsertInfo = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(nIndex);
				if(!pInsertInfo) return;

				CUBCStudioDoc *pDoc = GetDocument();
				CONTENTS_INFO* pContentsInfo = pDoc->GetContents(pInsertInfo->strContentsId);

				PLAYCONTENTS_INFO* pParentInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(treeinfo.hItem);

				if(!IsFitInsertRelation(pParentInfo, pInsertInfo)) return;

				HTREEITEM hRootItem = m_treeRelation.GetRootItem();
				HTREEITEM hInsertItem = NULL;

				TEMPLATE_LINK* pTemplateLink = pDoc->GetTemplateLink(pInsertInfo->strTemplateId);
				int nFrameColorIndex = -1;
				for(int nTemp=0; nTemp<pTemplateLink->arFrameLinkList.GetCount() ; nTemp++)
				{
					if(pTemplateLink->arFrameLinkList[nTemp].pFrameInfo->strId == pInsertInfo->strFrameId)
					{
						nFrameColorIndex = (pTemplateLink->arFrameLinkList[nTemp].pFrameInfo->nGrade == 1 ? 99 : _ttoi(pInsertInfo->strFrameId));
						break;
					}
				}

				// 부모 플레이콘텐츠 생성
				if(hRootItem == treeinfo.hItem)
				{
					hInsertItem = m_treeRelation.InsertItem(pContentsInfo->strContentsName, nFrameColorIndex, nFrameColorIndex, NULL, NULL);
				}
				// 자식 플레이콘텐츠 생성
				else
				{
					PLAYCONTENTS_INFO* pParentInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(treeinfo.hItem);
					if(pParentInfo) pInsertInfo->strParentPlayContentsId = pParentInfo->strId;
					hInsertItem = m_treeRelation.InsertItem(pContentsInfo->strContentsName, nFrameColorIndex, nFrameColorIndex, treeinfo.hItem, NULL);

					m_lcCyclePlayContents.SetRowColor(nIndex, ::GetSysColor(COLOR_WINDOW), RGB(191,191,191));
				}

				//m_treeRelation.Select(treeinfo.hItem, TVGN_DROPHILITE);
				m_treeRelation.SelectItem(hInsertItem);
				m_treeRelation.Expand(treeinfo.hItem, TVE_EXPAND);

				m_treeRelation.SetItemData(hInsertItem, (DWORD_PTR)pInsertInfo);

				m_treeRelation.Invalidate(FALSE);
			}
		}
		// 순환 플레이콘텐츠리스트로 드래그할때
		else if( pWndDrop->m_hWnd == m_lcCyclePlayContents.m_hWnd  			)
		{
			int nIndex = m_lcCyclePlayContents.GetNextItem(-1, LVIS_SELECTED);

			PLAYCONTENTS_INFO* pInsertInfo = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(nIndex);
			if(!pInsertInfo) return;

			//CPoint ptList(point);
			//CPoint ptList2(point);
			//m_lcCyclePlayContents.ClientToScreen(&ptList2);
			//m_lcCyclePlayContents.ScreenToClient(&ptList);

			//double dy = (point.y - m_dragStartPoint.y);
			//TraceLog(("Drop End =%d,%d, dy=%f",  point.x, point.y , dy ));

			//int moveIndex = int( (dy>0) ? floor( ((dy-9.0)/18.0)+0.5 ) : ceil( ((dy+9.0)/18.0)-0.5 ) ); // 반올림한다.
			CPoint ptList(ptScreen);
			m_lcCyclePlayContents.ScreenToClient(&ptList);
			UINT uFlags;
			int drop_index = m_lcCyclePlayContents.HitTest(ptList, &uFlags);
			int moveIndex = 0;
			if( drop_index >= 0 ) moveIndex = drop_index - nIndex;
			int scroll_v_pos = m_lcCyclePlayContents.GetScrollPos(SB_VERT);

			if( abs(moveIndex) > 0 )   // 움직임이 1칸이상이라함
			{
				TraceLog(("move index =%d,",  moveIndex));

				// 리스트를 playorder 순으로 소팅한다.
				PLAYCONTENTS_INFO_MAP	 sortedMap;
				for(int i=0; i<m_lcCyclePlayContents.GetItemCount(); i++)
				{
					PLAYCONTENTS_INFO* info = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(i);
					sortedMap.insert(PLAYCONTENTS_INFO_MAP::value_type(info->nPlayOrder,info));
					TraceLog(("PlayOrder=%d,%s", info->nPlayOrder, info->strContentsId));
				}
				
				// 맵상에서 타겟객체가 원래 몇째에 위치해 있는지 알아낸다.
				int currentPosition = 0;
				PLAYCONTENTS_INFO_MAP::iterator itr;
				for(itr=sortedMap.begin();itr!=sortedMap.end();itr++)
				{
					if( *pInsertInfo == *(itr->second) ) break;
					currentPosition++;
				}

				// 맵상에서 타겟객체가 몇번째로 움직이는 것인지 알아낸다.
				currentPosition += moveIndex;
				if(currentPosition <= 0) currentPosition = 0;
				if(currentPosition >= sortedMap.size()) currentPosition = sortedMap.size() - 1;

				// 맵상에서 알아낸 새위치의 playOrder 값을 알아낸다.
				int newOrder = 0, idx = 0;
				for(itr=sortedMap.begin();itr!=sortedMap.end();itr++,idx++)
				{
					if(currentPosition == idx)
						newOrder = itr->second->nPlayOrder;
				}

				int oldOrder = pInsertInfo->nPlayOrder;
				TraceLog(("old order = %d, new order =%d,",  oldOrder, newOrder));
				
				if(moveIndex>0){  // 아래로 끌어내렸다면
					// 올드위치와 새위치 사이값들을 미리 하나씩 위로 댕긴다.
					for(int i = 0;i<m_lcCyclePlayContents.GetItemCount();i++){
						PLAYCONTENTS_INFO* info = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(i);
						if(info->nPlayOrder > newOrder) {
							break;
						}
						if(info->nPlayOrder > oldOrder) {
							info->nPlayOrder--;
						}
					}
				}else if(moveIndex<0){  // 위로 끌어 올렸다면
					// 새위치와 올린 위치 사이값들을 미리 하나씩 아래로 댕긴다.
					for(int i = 0;i<m_lcCyclePlayContents.GetItemCount();i++){
						PLAYCONTENTS_INFO* info = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(i);
						// 원래 자기 자신이 있던 자리를 만나면 더이상 밀지 않는다.
						if(info->nPlayOrder >= oldOrder) {
							break;
						}
						if(info->nPlayOrder >= newOrder) {
							info->nPlayOrder++;
						}
					}
				}

				// 타겟에 새위치를 부여하여 적용 한다
				pInsertInfo->nPlayOrder = newOrder;
				InsertPlayContents(pInsertInfo, 2, true);

				// 뒤로 밀린 놈들의 새위치를 적용 한다
				for(int i = 0;i<m_lcCyclePlayContents.GetItemCount();i++){
					PLAYCONTENTS_INFO* info = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(i);
					if(info->nPlayOrder > newOrder) {
						InsertPlayContents(info, 2, false);
					}
				}
				RefreshPlayContentsList();
			}
			if( drop_index >= 0 )
			{
				m_lcCyclePlayContents.EnsureVisible(0, FALSE);

				CRect ref;
				m_lcCyclePlayContents.GetItemRect(0,ref,LVIR_BOUNDS);
				CSize szHeight(0, ref.Height() * scroll_v_pos);
				m_lcCyclePlayContents.Scroll(szHeight);           
				m_lcCyclePlayContents.SetScrollPos(SB_VERT, scroll_v_pos);

				m_lcCyclePlayContents.SetItemState(drop_index, LVIS_SELECTED, LVIS_SELECTED);
				//m_lcCyclePlayContents.EnsureVisible(drop_index, FALSE);
			}
		}
	}
	else if(m_ilContentDrag != NULL)
	{
		m_ilContentDrag->EndDrag();
		delete m_ilContentDrag;
		m_ilContentDrag = NULL;

		// 순환 및 정시 플레이콘텐츠리스트로 드래그할때
		if( pWndDrop->m_hWnd == m_lcCyclePlayContents.m_hWnd    ||
			pWndDrop->m_hWnd == m_lcTimeBasePlayContents.m_hWnd )
		{
			// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
			POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
			while(pos != NULL)
			{
				int nItem = m_lcContents.GetNextSelectedItem(pos);
				CONTENTS_INFO* pContents = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
				CString strContentsId = (pContents ? pContents->strId : _T(""));

				if(!GetDocument()->CheckContentsCanBeAddedToFrame(GetDocument()->GetSelectTemplateId(), GetDocument()->GetSelectFrameId(), strContentsId))
				{
					m_wndFrame.Invalidate(FALSE);
					return;
				}
			}
		}
		// 프레임안에 드랍할때
		else if(pWndDrop->m_hWnd == m_wndFrame.m_hWnd)
		{
			if(strlen(m_wndFrame.GetDropedFrameId()) == 0)
			{
				m_wndFrame.Invalidate(FALSE);
				return;
			}

			// 0000543: "고화질 동영상용 프레임". 속성을 추가한다.
			POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
			while(pos != NULL)
			{
				int nItem = m_lcContents.GetNextSelectedItem(pos);
				CONTENTS_INFO* pContents = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
				CString strContentsId = (pContents ? pContents->strId : _T(""));

				if(!GetDocument()->CheckContentsCanBeAddedToFrame(GetDocument()->GetSelectTemplateId(), m_wndFrame.GetDropedFrameId(), strContentsId))
				{
					m_wndFrame.HitTest2(CPoint(-1,-1));
					m_wndFrame.Invalidate(FALSE);
					return;
				}
			}

			CPoint ptClient = ptScreen;
			m_wndFrame.ScreenToClient(&ptClient);
			m_wndFrame.HitTest2(CPoint(-1,-1));
			m_wndFrame.OnLButtonDown(0, ptClient);
			m_wndFrame.Invalidate(FALSE);
		}
		else
		{
			m_wndFrame.Invalidate(FALSE);
			return;
		}

		ClearPlayContentsSelection();

		// USTB 콘텐츠 총량 제한및 동영상 프레임 1개로 제한- by 구현석
		ULONGLONG TotalFileSize = 0;
		CUBCStudioDoc *pDoc = GetDocument();

		if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
		{
			FRAME_LINK* pFrameLink = pDoc->GetSelectFrameLink();
			if(!pFrameLink)	return;

			TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
			if(!pTempLink)	return;

			TEMPLATE_LINK_LIST* pPlayTempList = pDoc->GetPlayTemplateList();
			if(!pPlayTempList)	return;
			
			for(int iTmp = 0; iTmp < pPlayTempList->GetCount(); iTmp++)
			{
				int nVideoCnt = 0;
				bool bSelTemp = false;
				if(pTempLink->pTemplateInfo->strId == pPlayTempList->GetAt(iTmp).pTemplateInfo->strId)
				{
					bSelTemp = true;
				}

				for(int iFrm = 0; iFrm < pPlayTempList->GetAt(iTmp).arFrameLinkList.GetCount(); iFrm++)
				{
					FRAME_INFO*	pFrmInfo = pPlayTempList->GetAt(iTmp).arFrameLinkList.GetAt(iFrm).pFrameInfo;
					if(!pFrmInfo)	continue;

					for(int iCS = 0; iCS < pFrmInfo->arCyclePlayContentsList.GetCount(); iCS++)
					{
						PLAYCONTENTS_INFO* pScdInfo = pFrmInfo->arCyclePlayContentsList.GetAt(iCS);
						if(!pScdInfo)	continue;

						CONTENTS_INFO* pCtsInfo = CDataContainer::getInstance()->GetContents(pScdInfo->strContentsId);
						if(!pCtsInfo)	continue;

						TotalFileSize += pCtsInfo->nFilesize;

						if(bSelTemp)
						{
							if(CONTENTS_TV == pCtsInfo->nContentsType || CONTENTS_VIDEO == pCtsInfo->nContentsType)
							{
								if(pFrameLink->pFrameInfo->strId != pFrmInfo->strId)
								{
									nVideoCnt++;
									if(nVideoCnt > 2)
									{
										UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG001), MB_ICONERROR);
										return;
									}
								}
							}
						}
					}

					for(int iCS = 0; iCS < pFrmInfo->arTimePlayContentsList.GetCount(); iCS++)
					{
						PLAYCONTENTS_INFO* pScdInfo = pFrmInfo->arTimePlayContentsList.GetAt(iCS);
						if(!pScdInfo)	continue;

						CONTENTS_INFO* pCtsInfo = CDataContainer::getInstance()->GetContents(pScdInfo->strContentsId);
						if(!pCtsInfo)	continue;

						TotalFileSize += pCtsInfo->nFilesize;

						if(bSelTemp)
						{
							if(CONTENTS_TV == pCtsInfo->nContentsType || CONTENTS_VIDEO == pCtsInfo->nContentsType)
							{
								if(pFrameLink->pFrameInfo->strId != pFrmInfo->strId)
								{
									nVideoCnt++;
									if(nVideoCnt > 2)
									{
										UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG001), MB_ICONERROR);
										return;
									}
								}
							}
						}
					}
				}
			}
		}
		// USTB ----
		
		POSITION pos = m_lcContents.GetFirstSelectedItemPosition();

		// 새로추가할 플레이콘텐츠의 기본시작시간을 구한다
		CTime tmNewStartTime = GetDocument()->GetNewPlayContentsStartTime(m_tabPlayContents.GetCurSel());

		while(pos != NULL)
		{
			int nItem = m_lcContents.GetNextSelectedItem(pos);

			CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
			if(!info) continue;

			// USTB 콘텐츠 총량 제한 - by 구현석
			if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
			{
				TotalFileSize += info->nFilesize;
				if(GetEnvPtr()->m_TotalContentsSize != 0 && TotalFileSize > GetEnvPtr()->m_TotalContentsSize)
				{
					szTemp.Format(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG002), ToMoneyTypeString(GetEnvPtr()->m_TotalContentsSize));
					UbcMessageBox(szTemp, MB_ICONERROR);
					return;
				}
			}

			//if(CONTENTS_PPT == info->nContentsType){
			//	TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
			//	if(!pTempLink)	continue;

			//	int nCnt = pTempLink->arFrameLinkList.GetCount();
			//	if(nCnt > 1){
			//		szTemp.Format(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG003));
			//		UbcMessageBox(szTemp, MB_ICONERROR);
			//		continue;
			//	}
			//}

			// 새로운 플레이콘텐츠생성
			PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
			
			/*
			pPlayContentsInfo->nEndDate = 1924255600;//"2999/12/31";
			//pPlayContentsInfo->nEndDate = tmNewStartTime.GetTime();
			pPlayContentsInfo->nTimeScope = 0;
			pPlayContentsInfo->strStartTime = "00:00:00";
			pPlayContentsInfo->strEndTime = "23:59:59";
			*/
			pPlayContentsInfo->strContentsId = info->strId;

			// 순환플레이콘텐츠인경우
			if(m_tabPlayContents.GetCurSel() == 0)
			{
				// 현재시간을 기준으로 설정
				tmNewStartTime = GetDocument()->GetNewPlayContentsStartTime(m_tabPlayContents.GetCurSel());

				pPlayContentsInfo->nStartDate = (int)tmNewStartTime.GetTime();
				//CTimeSpan tmSpanYear(31536000);		//1년
				//tmNewStartTime += tmSpanYear;
				//pPlayContentsInfo->nEndDate = (int)tmNewStartTime.GetTime();
				CTime tmMax = CTime(2035,12,31,23,59,59);  //skpark max 로 바꿈 2012.11.08
				pPlayContentsInfo->nEndDate = (int)tmMax.GetTime();
				pPlayContentsInfo->nTimeScope = 0;
				pPlayContentsInfo->strStartTime = _T("00:00");
				pPlayContentsInfo->strEndTime = _T("23:59");
				//pPlayContentsInfo->strStartTime = tmNewStartTime.Format("%H:%M:%S");
				//CTimeSpan tmSpanSec(info->nRunningTime);
				//tmNewStartTime += tmSpanSec;
				//pPlayContentsInfo->strEndTime = tmNewStartTime.Format("%H:%M:%S");

				pPlayContentsInfo->bIsDefaultPlayContents = true;
				PLAYCONTENTS_INFO* pTmpInfo = NULL;
				int nCnt = m_lcCyclePlayContents.GetItemCount();
				if(nCnt == 0)
				{
					pPlayContentsInfo->nPlayOrder = 1;
				}
				else
				{
					pTmpInfo = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(nCnt-1);
					if(pTmpInfo)
					{
						pPlayContentsInfo->nPlayOrder = pTmpInfo->nPlayOrder+1;
					}
					else
					{
						pPlayContentsInfo->nPlayOrder = 1;
					}//if
				}//if
			}
			// 정시플레이콘텐츠인경우
			else
			{
				pPlayContentsInfo->nStartDate = (int)tmNewStartTime.GetTime();
				pPlayContentsInfo->strStartTime = tmNewStartTime.Format("%H:%M:%S");
				pPlayContentsInfo->nTimeScope = 0;
				
				//skpark nRunningTime 이 5분보다 작은 경우 한해서 EndTime 을 자동셋팅해준다.
				// nRunningTime이 5분 보다 큰 경우 무조건 5분으로 잡는다.
				if(info->nRunningTime <= 60*5) {  
					CTimeSpan tmSpan(info->nRunningTime);
					tmNewStartTime += tmSpan;
				}else{
					CTimeSpan tmSpan(60*5);
					tmNewStartTime += tmSpan;
				}
				pPlayContentsInfo->nEndDate = (int)tmNewStartTime.GetTime();
				pPlayContentsInfo->strEndTime = tmNewStartTime.Format("%H:%M:%S");

				pPlayContentsInfo->bIsDefaultPlayContents = false;
				PLAYCONTENTS_INFO* pTmpInfo = NULL;
				int nCnt = m_lcTimeBasePlayContents.GetItemCount();
				if(nCnt == 0)
				{
					pPlayContentsInfo->nPlayOrder = 1;
				}
				else
				{
					pTmpInfo = (PLAYCONTENTS_INFO*)m_lcTimeBasePlayContents.GetItemData(nCnt-1);
					if(pTmpInfo)
					{
						pPlayContentsInfo->nPlayOrder = pTmpInfo->nPlayOrder+1;
					}
					else
					{
						pPlayContentsInfo->nPlayOrder = 1;
					}//if
				}//if
			}//if
			
			InsertPlayContents(pPlayContentsInfo, 0);
		}
	}
}



bool CPackageView::RegisterDND()
{
	m_lcContents.DragAcceptFiles();
	if(!m_OleDrop.Register(&m_lcContents))
	{
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG004), MB_ICONSTOP );
	}
	m_OleDrop.SetParent(this);

	return true;
}

bool CPackageView::UnregisterDND()
{
	m_OleDrop.Revoke();
	m_OleDrop.SetParent(NULL);

	return true;
}

LRESULT CPackageView::OnAddContents(WPARAM wParam, LPARAM lParam)
{
	if(!wParam) return 0;

	CStringArray* pFileList = (CStringArray*)wParam;

	// 콘텐츠의 갯수가 지정된 갯수를 넘어가는지 체크
	if(!GetDocument()->CheckMaxContentsCount(pFileList->GetCount())) return 0;

	ULONGLONG lAddSize = 0;
	for(int i=0; i<pFileList->GetCount() ;i++)
	{
		ULONGLONG lFileSize = 0;
		if(GetEnvPtr()->GetFileSize(pFileList->GetAt(i), lFileSize))
		{
			lAddSize += lFileSize;
		}
	}

	// 콘텐츠의 총 용량이 지정된 용량을 넘어서는지 체크
	if(!GetDocument()->CheckTotalContentsSize(lAddSize)) return 0;

	::AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED);
	return m_lcContents.SendMessage(WM_ADD_CONTENTS, wParam, lParam);
}

LRESULT CPackageView::OnClearContentsSelection(WPARAM wParam, LPARAM lParam)
{
	m_lcContents.ClearContentsSelection();
	return 0;
}

void CPackageView::OnNMDblclkListContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	OnBnClickedButtonModifyContents();
}

void CPackageView::OnLvnKeydownListContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pLVKeyDow->wVKey == VK_DELETE)
		OnBnClickedButtonRemoveContents();
}

void CPackageView::OnBnClickedButtonAddContents()
{
	// 콘텐츠의 갯수가 지정된 갯수를 넘어가는지 체크
	if(!GetDocument()->CheckMaxContentsCount(1)) return;

	if(m_lcContents.NewContents())
		::AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED);
}

void CPackageView::OnBnClickedButtonRemoveContents()
{
	if(m_lcContents.RemoveSelectedContents())
	{
		RefreshPlayContentsList();
		RefreshRelation();
		::AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED);
	}
}

void CPackageView::OnBnClickedButtonModifyContents()
{
	bool bDownload = false;
	CArray<CONTENTS_INFO*> arSInfo;
	POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
	while(pos != NULL)
	{
		int nItem = m_lcContents.GetNextSelectedItem(pos);

		CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
		if(info == NULL) continue;

		if(!info->bLocalFileExist && info->bServerFileExist)
			bDownload = true;

		arSInfo.Add(info);
	}

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && bDownload)
	{
		if(!DownloadSelectedContents()) return;
	}

	if(m_lcContents.ModifyContents(arSInfo))
	{
		RefreshPlayContentsList();
		RefreshRelation();
		::AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED);
	}
}

void CPackageView::OnBnClickedButtonDownloadContents()
{
	DownloadSelectedContents();	
}

void CPackageView::OnBnClickedButtonAddTemplate()
{
	// 플레이 리스트 갯수가 지정된 갯수를 넘어가는지 체크
	if(!GetDocument()->CheckMaxPlayTemplateCount()) return;

	CUBCStudioDoc doc;

	CSelectTemplateDialog dlg;
	dlg.SetDocument(&doc);
	if(dlg.DoModal() != IDOK) return;

	LPCTSTR strTemplateId = doc.GetSelectTemplateId();
	// Modified by 정운형 2009-02-19 오후 8:31
	// 변경내역 :  template 추가 버그 수정
	//GetDocument()->AddPlayTemplate(strTemplateId);
	//GetDocument()->SetSelectTemplateId(strTemplateId);
	//SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)strTemplateId, (WPARAM)(LPCTSTR)"");
	
	LPCTSTR lpszNewTemplateId = GetDocument()->CreateCopyTemplate(strTemplateId);
	if(lpszNewTemplateId == "") return;

	GetDocument()->SetSelectTemplateId(lpszNewTemplateId);
	SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)lpszNewTemplateId, (WPARAM)(LPCTSTR)"");

	// Modified by 정운형 2009-02-19 오후 8:31
	// 변경내역 :  template 추가 버그 수정
	m_wndTemplate.SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED, 1); // 템플릿 추가
	
	::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_LIST_CHANGED);

	InitTabPlayContents();
}

void CPackageView::OnBnClickedButtonRemoveTemplate()
{
	CUBCStudioDoc* pDoc = GetDocument();

	CString strSelectTemplateId = pDoc->GetSelectTemplateId();
	if(strSelectTemplateId == ""){
		return;
	}

	TEMPLATE_LINK_LIST* pTempList = pDoc->GetAllTemplateList();
	if(!pTempList)	return;

	if(pTempList->GetCount() <= 1){
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG005), MB_ICONWARNING);
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG003), MB_ICONWARNING | MB_YESNO) == IDNO){
		return;
	}

	bool exist_play_template = false;
	if( pDoc->IsExistContentsInTemplate(strSelectTemplateId)){
		exist_play_template = true;
		int ret_value = UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG007), MB_ICONWARNING | MB_YESNO);
		if(ret_value == IDNO)
			return;
	}

	if(pDoc->DeleteTemplate(strSelectTemplateId)){
		SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)"", (WPARAM)(LPCTSTR)"");
		m_wndTemplate.SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED, 0); // 템플릿 삭제
		
		::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_LIST_CHANGED);
	}
}

void CPackageView::OnBnClickedButtonMoveUpTemplate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int index = m_wndTemplate.GetSelectTemplateLinkIndex();
	if(index < 0)
		return;

	if(GetDocument()->MoveFordwardPlayTemplate(index))
	{
		m_wndTemplate.RecalcLayout();
		m_wndTemplate.m_nSelectTemplateLinkIndex--;
		m_wndTemplate.EnsureVisible(m_wndTemplate.m_nSelectTemplateLinkIndex);
		m_wndTemplate.Invalidate(FALSE);
		// Modified by 정운형 2008-12-26 오후 1:56
		// 변경내역 :  캡션기능 추가 - 버그수정
		::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_INFO_CHANGED);
		// Modified by 정운형 2008-12-26 오후 1:56
		// 변경내역 :  캡션기능 추가 - 버그수정
	}
}

void CPackageView::OnBnClickedButtonMoveDownTemplate()
{
	int index = m_wndTemplate.GetSelectTemplateLinkIndex();
	if(index < 0)
		return;

	if(GetDocument()->MoveBackwardPlayTemplate(index))
	{
		m_wndTemplate.RecalcLayout();
		m_wndTemplate.m_nSelectTemplateLinkIndex++;
		m_wndTemplate.EnsureVisible(m_wndTemplate.m_nSelectTemplateLinkIndex);
		m_wndTemplate.Invalidate(FALSE);
		// Modified by 정운형 2008-12-26 오후 1:56
		// 변경내역 :  캡션기능 추가 - 버그수정
		::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_INFO_CHANGED);
		// Modified by 정운형 2008-12-26 오후 1:56
		// 변경내역 :  캡션기능 추가 - 버그수정
	}
}

void CPackageView::OnBnClickedButtonPreview()
{
	CUBCStudioDoc* pDoc = GetDocument();
	CString strSelectTemplateID = pDoc->GetSelectTemplateId();
	if(strSelectTemplateID == ""){
		return; 
	}

#ifdef _UBCSTUDIO_EE_
	CFtpMultiSite multiFtp(GetEnvPtr()->m_strHttpOnly.CompareNoCase("CLIENT") == 0);

	CString szPackage;
	szPackage.Format("%s%s%s.ini", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONFIG_PATH, GetEnvPtr()->m_PackageInfo.szPackage);
	multiFtp.AddSite(GetEnvPtr()->m_PackageInfo.szSiteID, szPackage);
	multiFtp.OnlyAddFile();

	TEMPLATE_LINK* pTLink = CDataContainer::getInstance()->GetTemplateLink(strSelectTemplateID);
	int frame_count = pTLink->arFrameLinkList.GetCount();
	for(int j=0; j<frame_count; j++)
	{
		FRAME_LINK& frame_link = pTLink->arFrameLinkList.GetAt(j);

		for(int k=0; k<frame_link.pFrameInfo->arCyclePlayContentsList.GetCount(); k++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);
			if(!pPlayContentsInfo)	continue;

			CONTENTS_INFO* pContentsInfo = CDataContainer::getInstance()->GetContents(pPlayContentsInfo->strContentsId);
			if(!pContentsInfo)	continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				multiFtp.AddDownloadFile(pContentsInfo->strServerLocation + pContentsInfo->strFilename);
			}
		}

		for(int k=0; k<frame_link.pFrameInfo->arTimePlayContentsList.GetCount(); k++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);
			if(!pPlayContentsInfo)	continue;

			CONTENTS_INFO* pContentsInfo = CDataContainer::getInstance()->GetContents(pPlayContentsInfo->strContentsId);
			if(!pContentsInfo)	continue;

			if(!pContentsInfo->bLocalFileExist && pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				multiFtp.AddDownloadFile(pContentsInfo->strServerLocation + pContentsInfo->strFilename);
			}
		}
	}

	if(!multiFtp.RunFtp(CFtpMultiSite::eDownload)) return;

	for(int j=0; j<frame_count; j++)
	{
		FRAME_LINK& frame_link = pTLink->arFrameLinkList.GetAt(j);

		for(int k=0; k<frame_link.pFrameInfo->arCyclePlayContentsList.GetCount(); k++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arCyclePlayContentsList.GetAt(k);
			if(!pPlayContentsInfo)	continue;

			CONTENTS_INFO* pContentsInfo = CDataContainer::getInstance()->GetContents(pPlayContentsInfo->strContentsId);
			if(!pContentsInfo)	continue;

			if(!pContentsInfo->bLocalFileExist && !pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				pContentsInfo->bLocalFileExist = true;
				pContentsInfo->bServerFileExist = true;
			}
		}

		for(int k=0; k<frame_link.pFrameInfo->arTimePlayContentsList.GetCount(); k++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = frame_link.pFrameInfo->arTimePlayContentsList.GetAt(k);
			if(!pPlayContentsInfo)	continue;

			CONTENTS_INFO* pContentsInfo = CDataContainer::getInstance()->GetContents(pPlayContentsInfo->strContentsId);
			if(!pContentsInfo)	continue;

			if(!pContentsInfo->bLocalFileExist && pContentsInfo->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				pContentsInfo->bLocalFileExist = true;
				pContentsInfo->bServerFileExist = true;
			}
		}
	}

#endif//_UBCSTUDIO_EE_

	UpdateAllInfo();
//	RefreshPlayContentsList();
//	::AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED);

	CMainFrame* pclsMainFrame = (CMainFrame*)AfxGetMainWnd();
	pclsMainFrame->RunPreview(strSelectTemplateID);
}

void CPackageView::OnBnClickedButtonMovePrevFrame()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_wndFrame.MovePrevFrame();
}

void CPackageView::OnBnClickedButtonMoveNextFrame()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_wndFrame.MoveNextFrame();
}

void CPackageView::OnTcnSelchangeTabPlayContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	ToggleTabPlayContents(m_tabPlayContents.GetCurSel());
}

void CPackageView::ToggleTabPlayContents(int nCurSel)
{
	m_tabPlayContents.SetCurSel(nCurSel);

	m_lcCyclePlayContents.ShowWindow((nCurSel == 0 ? SW_SHOW : SW_HIDE));
	m_lcCyclePlayContents.EnableWindow(nCurSel == 0);

	m_lcTimeBasePlayContents.ShowWindow((nCurSel != 0 ? SW_SHOW : SW_HIDE));
	m_lcTimeBasePlayContents.EnableWindow(nCurSel != 0);

	if(nCurSel == 0) 
	{
		m_lcCyclePlayContents.SetFocus();
	}
	else
	{
		m_lcTimeBasePlayContents.SetFocus();
	}
	
	//m_toggle--;
	//this->m_btnSizeToggle.ShowWindow(SW_SHOW);
	//OnBnClickedButtonResizePlayContents();
}

void CPackageView::OnLvnKeydownListCyclePlayContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pLVKeyDow->wVKey == VK_DELETE)
	{
		OnBnClickedButtonRemovePlayContents();
	}
}

void CPackageView::OnNMDblclkListCyclePlayContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	OnBnClickedButtonModifyPlayContents();
}

void CPackageView::OnLvnKeydownListTimeBasePlayContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pLVKeyDow->wVKey == VK_DELETE)
	{
		OnBnClickedButtonRemovePlayContents();
	}
}

void CPackageView::OnNMDblclkListTimeBasePlayContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	OnBnClickedButtonModifyPlayContents();
}

// 플레이콘텐츠추가
void CPackageView::OnBnClickedButtonAddPlayContents()
{
	CString strSelectedFrameID = GetDocument()->GetSelectFrameId();
	if(strSelectedFrameID == "")
	{
		return;
	}

	// 창일향 : 메인 프레임이 선택되었을 때는 콘텐츠 패키지를 추가도 삭제도,  편집도 할 수 없다.  메인 프레임외에 다른 프레임들만 선택하고 플레이콘텐츠 추가, 삭제, 편집이 가능하다.
	if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer )
	{
		FRAME_LINK* pFrameLink = GetDocument()->GetSelectFrameLink();
		if(pFrameLink && pFrameLink->pFrameInfo->nGrade == 1)
		{
			UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG012));
			return;
		}
	}

	CPlayContentsDialog dlg;
	dlg.SetDocument(GetDocument());
	dlg.SetContentsListCtrl(&m_lcContents);
	dlg.SetCyclePlayContentsMode(m_tabPlayContents.GetCurSel() == 0);
	if(dlg.DoModal() != IDOK) return;

	ClearPlayContentsSelection();
	int count = dlg.GetPlayContentsCount();
	for(int i=0; i<count; i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
		dlg.GetPlayContentsInfo(*pPlayContentsInfo, i);

		// 순환플레이콘텐츠의 경우
		if(pPlayContentsInfo->bIsDefaultPlayContents)
		{
			PLAYCONTENTS_INFO* pTmpInfo = NULL;
			int nCnt = m_lcCyclePlayContents.GetItemCount();
			if(nCnt == 0)
			{
				pPlayContentsInfo->nPlayOrder = 1;
			}
			else
			{
				pTmpInfo = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(nCnt-1);
				if(pTmpInfo)
				{
					pPlayContentsInfo->nPlayOrder = pTmpInfo->nPlayOrder+1;
				}
				else
				{
					pPlayContentsInfo->nPlayOrder = 1;
				}//if
			}//if

			m_tabPlayContents.SetCurSel(0);
		}
		// 정시플레이콘텐츠의 경우
		else
		{
			PLAYCONTENTS_INFO* pTmpInfo = NULL;
			int nCnt = m_lcTimeBasePlayContents.GetItemCount();
			if(nCnt == 0)
			{
				pPlayContentsInfo->nPlayOrder = 1;
			}
			else
			{
				pTmpInfo = (PLAYCONTENTS_INFO*)m_lcTimeBasePlayContents.GetItemData(nCnt-1);
				if(pTmpInfo)
				{
					pPlayContentsInfo->nPlayOrder = pTmpInfo->nPlayOrder+1;
				}
				else
				{
					pPlayContentsInfo->nPlayOrder = 1;
				}//if

			}//if
			m_tabPlayContents.SetCurSel(1);
		}//if

		InsertPlayContents(pPlayContentsInfo, 0);
		/*
		if(pPlayContentsInfo->bIsDefaultPlayContents)
			m_tabPlayContents.SetCurSel(0);
		else
			m_tabPlayContents.SetCurSel(1);
		*/
		LRESULT result;
		OnTcnSelchangeTabPlayContents(NULL, &result);
	}

	// 정시플레이콘텐츠의 시간이 겹치는 경우 알림
	GetDocument()->CheckTimeBasePlayContentsOverlap(m_tabPlayContents.GetCurSel());

	GetCurrentPlayContentsListCtrl()->SetFocus();
}

void CPackageView::OnBnClickedButtonRemovePlayContents()
{
	// 창일향 : 메인 프레임이 선택되었을 때는 콘텐츠 패키지를 추가도 삭제도,  편집도 할 수 없다.  메인 프레임외에 다른 프레임들만 선택하고 플레이콘텐츠 추가, 삭제, 편집이 가능하다.
	if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer )
	{
		FRAME_LINK* pFrameLink = GetDocument()->GetSelectFrameLink();
		if(pFrameLink && pFrameLink->pFrameInfo->nGrade == 1)
		{
			UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG012));
			return;
		}
	}

	CUTBListCtrlEx* pCurrentPlayContents = GetCurrentPlayContentsListCtrl();

	bool selected_exist = false;
	POSITION pos = pCurrentPlayContents->GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		selected_exist = true;
		int nItem = pCurrentPlayContents->GetNextSelectedItem(pos);
	}

	if(selected_exist)
	{
		int ret_value = UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG006), MB_ICONWARNING | MB_YESNO);
		if(ret_value == IDNO)
			return;
	}

	pos = pCurrentPlayContents->GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		DeletePlayContents(pos);
//		RefreshPlayContentsList();
		RefreshRelation();
	}

	pCurrentPlayContents->SetFocus();
}

void CPackageView::OnBnClickedButtonToExcel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// skpark 2012.12.11
	// playContents 를 Excel 출력한다.
	CPlayContentsSave dlg;

	CUTBListCtrlEx* pList = GetCurrentPlayContentsListCtrl();

	CString strExcelFile = dlg.Save(  LoadStringById(IDS_PLAYCONTENTSVIEW_LST001)
									//, m_lcCyclePlayContents
									, *pList
									);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG019), strExcelFile);
		UbcMessageBox(strMsg);
	}

}

void CPackageView::OnBnClickedButtonResizePlayContents()
{
	// skpark 2012.12.07
	// 이 버튼은 순환플레이컨텐츠창을 크게 하거나 작게하는 토글 버튼으로
	// 우 되돌리기 수정 버튼은 없어진다.  어차피 더블클릭하면 수정이 되므로 필요없다.

	m_toggle++;
	if(m_toggle %2==0){
		m_reposControl.Move();
		ShowItems(SW_SHOW);
	}else if(m_toggle % 2 == 1){
	
		//m_reposControl.Move();
		ShowItems(SW_HIDE);

		CRect rcCon;
		m_lcContents.GetWindowRect(rcCon);

		CRect rcTemplate;
		m_frameTemplate.GetWindowRect(rcTemplate);

		CRect rcTab;
		m_tabPlayContents.GetWindowRect(rcTab);

		CRect rcList;
		m_lcCyclePlayContents.GetWindowRect(rcList);

		CRect rcList2;
		m_lcTimeBasePlayContents.GetWindowRect(rcList2);

		int offsetX = rcCon.left-5;
		int offsetY = rcCon.top-30;

		CRect rcB1,rcB2,rcB3,rcB4,rcB5;

		m_btnAddTemplate.GetWindowRect(rcB1);
		m_btnRemoveTemplate.GetWindowRect(rcB2);
		m_btnMoveUpTemplate.GetWindowRect(rcB3);
		m_btnMoveDownTemplate.GetWindowRect(rcB4);
		m_btnCopyTemplate.GetWindowRect(rcB5);

		CRect rcB;
		m_btnAddPlayContents.GetWindowRect(rcB);

		CRect rcBR;
		m_btnRemoveRelation.GetWindowRect(rcBR);

		m_btnAddPlayContents.SetWindowPos(NULL,rcB.left-offsetX,rcB1.top-offsetY,rcB1.Width(),rcB1.Height(),SWP_SHOWWINDOW);
		m_btnRemovePlayContents.SetWindowPos(NULL,rcB.left-offsetX,rcB2.top-offsetY,rcB2.Width(),rcB2.Height(),SWP_SHOWWINDOW);
		m_btnModifyPlayContents.SetWindowPos(NULL,rcB.left-offsetX,rcB3.top-offsetY,rcB3.Width(),rcB3.Height(),SWP_SHOWWINDOW);
		m_btnMoveUpPlayContents.SetWindowPos(NULL,rcB.left-offsetX,rcB4.top-offsetY,rcB4.Width(),rcB4.Height(),SWP_SHOWWINDOW);
		m_btnMoveDownPlayContents.SetWindowPos(NULL,rcB.left-offsetX,rcB5.top-offsetY,rcB5.Width(),rcB5.Height(),SWP_SHOWWINDOW);

		CRect rcToggle;
		m_btnSizeToggle.GetWindowRect(rcToggle);
		m_btnSizeToggle.SetWindowPos(NULL,rcToggle.left-offsetX,rcTemplate.top-offsetY-6,rcToggle.Width(),rcToggle.Height(),SWP_SHOWWINDOW);

		//char buf[400];
		//memset(buf,0x00,400);
		//sprintf(buf,"Before\n ContentsBasket=%d,%d,%d,%d\n LayoutArea=%d,%d,%d,%d\n TABArea=%d,%d,%d,%d\n ListArea=%d,%d,%d,%d", 
		//	rcCon.left, rcCon.top, rcCon.right, rcCon.bottom,
		//	rcTemplate.left, rcTemplate.top, rcTemplate.right, rcTemplate.bottom,
		//	rcTab.left, rcTab.top, rcTab.right, rcTab.bottom,
		//	//rcFrame.left, rcFrame.top, rcFrame.right, rcFrame.bottom,
		//	rcList.left, rcList.top, rcList.right, rcList.bottom);
		//UbcMessageBox(buf, MB_ICONWARNING );

		m_tabPlayContents.SetWindowPos(NULL,
			rcTab.left-offsetX,
			rcTemplate.top-offsetY,
			rcTab.Width(),
			rcTab.bottom-rcTemplate.top, 
			SWP_SHOWWINDOW);

		if(m_tabPlayContents.GetCurSel() == 0) {
			m_lcCyclePlayContents.SetWindowPos(NULL,
				rcList.left-offsetX,
				rcTemplate.top-offsetY+27,
				rcList.Width(),
				rcList.bottom-rcTemplate.top-27, 
				SWP_SHOWWINDOW);
			m_lcTimeBasePlayContents.SetWindowPos(NULL,
				rcList2.left-offsetX,
				rcTemplate.top-offsetY+27,
				rcList2.Width(),
				rcList2.bottom-rcTemplate.top-27, 
				SWP_HIDEWINDOW);
		}else{
			m_lcCyclePlayContents.SetWindowPos(NULL,
				rcList.left-offsetX,
				rcTemplate.top-offsetY+27,
				rcList.Width(),
				rcList.bottom-rcTemplate.top-27, 
				SWP_HIDEWINDOW);
			m_lcTimeBasePlayContents.SetWindowPos(NULL,
				rcList2.left-offsetX,
				rcTemplate.top-offsetY+27,
				rcList2.Width(),
				rcList2.bottom-rcTemplate.top-27, 
				SWP_SHOWWINDOW);
		}

		m_btnRemoveRelation.SetWindowPos(NULL,rcBR.left-offsetX,rcB1.top-offsetY,rcB1.Width(),rcB1.Height(),SWP_SHOWWINDOW);

		CRect rcTabR;
		m_tabRelation.GetWindowRect(rcTabR);

		CRect rcTree;
		m_treeRelation.GetWindowRect(rcTree);

		m_tabRelation.SetWindowPos(NULL,
				rcTabR.left-offsetX,
				rcTemplate.top-offsetY,
				rcTabR.Width(),
				rcTabR.bottom-rcTemplate.top, 
				SWP_SHOWWINDOW);
		m_treeRelation.SetWindowPos(NULL,
				rcTree.left-offsetX,
				rcTemplate.top-offsetY+27,
				rcTree.Width(),
				rcTree.bottom-rcTemplate.top-27, 
				SWP_SHOWWINDOW);

		//RefreshPlayContentsList();
		//UpdateData(FALSE);
		
		//memset(buf,0x00,400);
		//sprintf(buf,"After\n ContentsBasket=%d,%d,%d,%d\n LayoutArea=%d,%d,%d,%d\n TABArea=%d,%d,%d,%d\n ListArea=%d,%d,%d,%d", 
		//	rcCon.left, rcCon.top, rcCon.right, rcCon.bottom,
		//	rcTemplate.left, rcTemplate.top, rcTemplate.right, rcTemplate.bottom,
		//	rcTab.left, rcTab.top, rcTab.right, rcTab.bottom,
		//	//rcFrame.left, rcFrame.top, rcFrame.right, rcFrame.bottom,
		//	rcList.left, rcList.top, rcList.right, rcList.bottom);
		//UbcMessageBox(buf, MB_ICONWARNING );
	}
	InvalidateItems();

}

void CPackageView::ShowItems(int shows)
{
	m_wndTemplate.ShowWindow(shows);
	m_wndFrame.ShowWindow(shows);
	m_frameTemplate.ShowWindow(shows);
	m_frameFrame.ShowWindow(shows);

	m_btnAddTemplate.ShowWindow(shows);
	m_btnRemoveTemplate.ShowWindow(shows);
	m_btnMoveUpTemplate.ShowWindow(shows);
	m_btnMoveDownTemplate.ShowWindow(shows);
	m_btnCopyTemplate.ShowWindow(shows);

	m_btnPreview.ShowWindow(shows);
	m_btnMovePrevFrame.ShowWindow(shows);
	m_btnMoveNextFrame.ShowWindow(shows);

	this->m_btnHSizeToggle.ShowWindow(shows);
	//m_btnRemoveRelation.ShowWindow(shows);
	//m_tabRelation.ShowWindow(shows);
	//m_treeRelation.ShowWindow(shows);
}

void CPackageView::InvalidateItems()
{
	m_wndTemplate.Invalidate(false);
	m_wndFrame.Invalidate(false);
	m_frameTemplate.Invalidate(false);
	m_frameFrame.Invalidate(false);

	m_tabPlayContents.Invalidate(false);
	m_lcCyclePlayContents.Invalidate(false);
	m_lcTimeBasePlayContents.Invalidate(false);

	m_btnAddTemplate.Invalidate(false);
	m_btnRemoveTemplate.Invalidate(false);
	m_btnMoveUpTemplate.Invalidate(false);
	m_btnMoveDownTemplate.Invalidate(false);
	m_btnCopyTemplate.Invalidate(false);

	m_btnPreview.Invalidate(false);
	m_btnMovePrevFrame.Invalidate(false);
	m_btnMoveNextFrame.Invalidate(false);

	m_btnRemoveRelation.Invalidate(false);
	m_tabRelation.Invalidate(false);
	m_treeRelation.Invalidate(false);

	m_btnAddPlayContents.Invalidate(false);
	m_btnRemovePlayContents.Invalidate(false);
	m_btnModifyPlayContents.Invalidate(false);
	m_btnMoveUpPlayContents.Invalidate(false);
	m_btnMoveDownPlayContents.Invalidate(false);

	m_btnSizeToggle.Invalidate(false);
	m_btnHSizeToggle.Invalidate(false);

}

void CPackageView::OnBnClickedButtonModifyPlayContents()
{
	TraceLog(("OnBnClickedButtonModifyPlayContents()"));

	// 창일향 : 메인 프레임이 선택되었을 때는 콘텐츠 패키지를 추가도 삭제도,  편집도 할 수 없다.  메인 프레임외에 다른 프레임들만 선택하고 플레이콘텐츠 추가, 삭제, 편집이 가능하다.
	if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer )
	{
		FRAME_LINK* pFrameLink = GetDocument()->GetSelectFrameLink();
		if(pFrameLink && pFrameLink->pFrameInfo->nGrade == 1)
		{
			UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG012));
			return;
		}
	}

	CUTBListCtrlEx* pCurrentPlayContents = GetCurrentPlayContentsListCtrl();
	POSITION pos = pCurrentPlayContents->GetFirstSelectedItemPosition();
	CArray<PLAYCONTENTS_INFO*> arSInfo;

	while(pos != NULL)
	{
		int nItem = pCurrentPlayContents->GetNextSelectedItem(pos);
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)pCurrentPlayContents->GetItemData(nItem);
		if(!pPlayContentsInfo)	continue;
		arSInfo.Add(pPlayContentsInfo);
	}

	for(int i = 0; i < arSInfo.GetCount(); i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = arSInfo.GetAt(i);

		CPlayContentsDialog dlg;
		dlg.SetEditMode();
		dlg.SetPlayContentsInfo(*pPlayContentsInfo);
		// Modified by 정운형 2008-12-26 오후 1:56
		// 변경내역 :  캡션기능 추가 - 버그수정
		dlg.SetContentsListCtrl(&m_lcContents);
		// Modified by 정운형 2008-12-26 오후 1:56
		// 변경내역 :  캡션기능 추가 - 버그수정

		if(dlg.DoModal() == IDOK)
		{
			dlg.GetPlayContentsInfo(*pPlayContentsInfo, 0);
			InsertPlayContents(pPlayContentsInfo, 2);
		}
	}

	// Modified by 정운형 2008-12-26 오후 1:56
	// 변경내역 :  캡션기능 추가 - 버그수정
	RefreshPlayContentsList();
	RefreshRelation();

	::AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED);

	// 정시플레이콘텐츠의 시간이 겹치는 경우 알림
	GetDocument()->CheckTimeBasePlayContentsOverlap(m_tabPlayContents.GetCurSel());

	// Modified by 정운형 2008-12-26 오후 1:56
	// 변경내역 :  캡션기능 추가 - 버그수정
	pCurrentPlayContents->SetFocus();
}

void CPackageView::OnBnClickedButtonMoveUpPlayContents()
{
	CUTBListCtrlEx* pCurrentPlayContents = GetCurrentPlayContentsListCtrl();

	for(int nItem=0; nItem<pCurrentPlayContents->GetItemCount(); nItem++)
	{
		if(pCurrentPlayContents->GetItemState(nItem, LVIS_SELECTED))
		{
			int new_index = GetPrevNoSelectPlayContentsIndex(pCurrentPlayContents, nItem);
			if(new_index >= 0)
			{
				PLAYCONTENTS_INFO* prev_info = (PLAYCONTENTS_INFO*)pCurrentPlayContents->GetItemData(nItem);
				PLAYCONTENTS_INFO* new_info = (PLAYCONTENTS_INFO*)pCurrentPlayContents->GetItemData(new_index);

				// Modified by 정운형 2009-01-06 오픸E11:35
				// 변경내역 :  CLI 연동 수정
				/*
				PLAYCONTENTS_INFO info;
				info = *prev_info;
				*prev_info = *new_info;
				*new_info = info;
				*/
				PLAYCONTENTS_INFO info;
				info = *prev_info;
				*prev_info = *new_info;
				//play order를 서로 바꾸어 준다
				(*prev_info).nPlayOrder = info.nPlayOrder;
				info.nPlayOrder = (*new_info).nPlayOrder;
				*new_info = info;
				// Modified by 정운형 2009-01-06 오픸E11:35
				// 변경내역 :  CLI 연동 수정

				InsertPlayContents(prev_info, 2, false);
				InsertPlayContents(new_info, 2, true);
			}
		}
	}
	pCurrentPlayContents->SetFocus();
}


void CPackageView::OnBnClickedButtonMoveDownPlayContents()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CUTBListCtrlEx* pCurrentPlayContents = GetCurrentPlayContentsListCtrl();

	for(int nItem=pCurrentPlayContents->GetItemCount()-1; nItem>=0; nItem--)
	{
		if(pCurrentPlayContents->GetItemState(nItem, LVIS_SELECTED))
		{
			int new_index = GetNextNoSelectPlayContentsIndex(pCurrentPlayContents, nItem);
			if(new_index >= 0)
			{
				PLAYCONTENTS_INFO* next_info = (PLAYCONTENTS_INFO*)pCurrentPlayContents->GetItemData(nItem);
				PLAYCONTENTS_INFO* new_info = (PLAYCONTENTS_INFO*)pCurrentPlayContents->GetItemData(new_index);

				// Modified by 정운형 2009-01-07 오픸E11:24
				// 변경내역 :  CLI 연동 수정
				/*
				PLAYCONTENTS_INFO info;
				info = *next_info;
				*next_info = *new_info;
				*new_info = info;
				*/
				PLAYCONTENTS_INFO info;
				info = *next_info;
				*next_info = *new_info;
				//play order를 서로 바꾸어 준다
				(*next_info).nPlayOrder = info.nPlayOrder;
				info.nPlayOrder = (*new_info).nPlayOrder;
				*new_info = info;

				InsertPlayContents(next_info, 2, false);
				InsertPlayContents(new_info, 2, true);
			}
		}
	}
	pCurrentPlayContents->SetFocus();
}

LRESULT CPackageView::OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam)
{
	LPCTSTR strTemplateId = (LPCTSTR)wParam;
	LPCTSTR strFrameId = (LPCTSTR)lParam;

	CString strOldTemplateId = GetDocument()->GetSelectTemplateId();

	GetDocument()->SetSelectTemplateId(strTemplateId);
	GetDocument()->SetSelectFrameId(strFrameId);

	m_wndTemplate.SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, wParam, lParam);
	m_wndFrame.SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, wParam, lParam);

	RefreshPlayContentsList();

	if(strOldTemplateId != GetDocument()->GetSelectTemplateId())
	{
		RefreshRelation();
	}

	return 0;
}

LRESULT CPackageView::OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam)
{
	m_wndTemplate.RecalcLayout();
	m_wndTemplate.Invalidate(FALSE);

	m_wndFrame.RecalcLayout();
	m_wndFrame.Invalidate(FALSE);

	return 0;
}

LRESULT CPackageView::OnFrameInfoChanged(WPARAM wParam, LPARAM lParam)
{
	m_wndTemplate.RecalcLayout();
	m_wndTemplate.Invalidate(FALSE);

	m_wndFrame.RecalcLayout();
	m_wndFrame.Invalidate(FALSE);

	// 정시플레이콘텐츠 입력은 메인프레임에만 할수 있도록 함
	InitTabPlayContents();

	return 0;
}

LRESULT CPackageView::OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam)
{
	OnTemplateFrameSelectChanged((WPARAM)(LPCTSTR)"", (LPARAM)(LPCTSTR)"");

	//m_wndTemplate.RecalcLayout();
	//m_wndTemplate.Invalidate(FALSE);

	//m_wndFrame.RecalcLayout();
	//m_wndFrame.Invalidate(FALSE);

	return 0;
}

LRESULT CPackageView::OnContentsListChanged(WPARAM wParam, LPARAM lParam)
{
	m_lcContents.RefreshContents();
	m_lcContents.SortCur();
	return 0;
}

CUTBListCtrlEx* CPackageView::GetCurrentPlayContentsListCtrl()
{
	if(m_tabPlayContents.GetCurSel() == 0)
		return &m_lcCyclePlayContents;
	else
		return &m_lcTimeBasePlayContents;
}

// 0 = Add List & Map, 1 = Only Add List, 2 = Modify
bool CPackageView::InsertPlayContents(PLAYCONTENTS_INFO* pPlayContentsInfo, int nMode, bool bSelection)
{
	CUBCStudioDoc *pDoc = GetDocument();
	FRAME_LINK* frame_link = pDoc->GetSelectFrameLink();
	CONTENTS_INFO* pContentsInfo = pDoc->GetContents(pPlayContentsInfo->strContentsId);

	if(frame_link == NULL)
	{
		if(nMode == 0) delete pPlayContentsInfo;
		return false;
	}

	if(nMode == 0 || nMode == 2){
		pPlayContentsInfo->bIsNew = true;
	}

	CUTBListCtrlEx* pPlayContentsList = (pPlayContentsInfo->bIsDefaultPlayContents ? &m_lcCyclePlayContents : &m_lcTimeBasePlayContents);
	int idx = -1;

	switch(nMode)
	{
	default:
		delete pPlayContentsInfo;
		return false;

	case 0:	// Add Map & List
		{
			//requestid = templateId + frameId + "_" + hostid + "_" + playorder(4자리)
			CMainFrame* pclsFrame = (CMainFrame*)::AfxGetMainWnd();
			CString strHostID = pclsFrame->GetHostName();
			//host id에 '-' 문자가 있을경우는 '-' 문자 뒤의 5자리문자만 사용
			int nIdx = strHostID.ReverseFind('-');
			if(nIdx != -1)
			{
				strHostID.Delete(0, nIdx+1);
			}//if

			//__template_ 가 들어가있는 sample 파일에서 스케줄을 추가할때는,
			//host 명을 __template_로 바꾸어 스케줄 ID를 생성하도록 하며,
			//저장할 때 다시 스케줄 ID를 저장된 host 명으로 바꾼다
			if(strHostID.Find(SAMPLE_FILE_KEY, 0) != -1)
			{
				strHostID = SAMPLE_FILE_KEY;
			}//

			//+ 0001433: 순환 및 정시 플레이콘텐츠가 중복생성되는 문제.
			pPlayContentsInfo->strId = CUbcGUID::GetInstance()->ToGUID("");
			//Cycle playcontents일경우 play order 앞에 'c'를
			//Ontime playcontents인 경우에는 play order 앞에 't'를 붙여준다.
			//if(pPlayContentsInfo->bIsDefaultPlayContents)
			//{
			//	pPlayContentsInfo->strId.Format("%s_c%04d", strHostID, pPlayContentsInfo->nPlayOrder);
			//}
			//else
			//{
			//	pPlayContentsInfo->strId.Format("%s_t%04d", strHostID, pPlayContentsInfo->nPlayOrder);
			//}//if
			//- 0001433: 순환 및 정시 플레이콘텐츠가 중복생성되는 문제.

			if(frame_link->pFrameInfo->AddPlayContents(pPlayContentsInfo) == false)
				return false;

			::AfxGetMainWnd()->SendMessage(WM_PLAYCONTENTS_LIST_CHANGED);
			m_wndTemplate.SendMessage(WM_PLAYCONTENTS_LIST_CHANGED);
		}

	case 1: // Only Add List
		idx = pPlayContentsList->GetItemCount();
		pPlayContentsList->InsertItem(idx, "", -1);
		break;

	case 2: // Modify
		for(int i=0; i<pPlayContentsList->GetItemCount(); i++)
		{
			PLAYCONTENTS_INFO* inf = (PLAYCONTENTS_INFO*)pPlayContentsList->GetItemData(i);
			if(inf == pPlayContentsInfo)
			{
				idx = i;
				break;
			}
		}
		if(idx < 0)
			return false;
	
		::AfxGetMainWnd()->SendMessage(WM_PLAYCONTENTS_LIST_CHANGED);
		m_wndTemplate.SendMessage(WM_PLAYCONTENTS_LIST_CHANGED);

		break;
	}

	CTime start_date(pPlayContentsInfo->nStartDate);
	CTime end_date(pPlayContentsInfo->nEndDate);

	LVITEM item;
	item.mask = LVIF_TEXT|LVIF_IMAGE;
	item.iItem = idx;
	item.iSubItem = 0;

	SYSTEMTIME stm;
	GetLocalTime(&stm);
	CTime tmCur(stm);

	// 순환플레이콘텐츠
	if(pPlayContentsInfo->bIsDefaultPlayContents)
	{
		start_date.GetAsSystemTime(stm);
		stm.wHour = 0;
		stm.wMinute = 0;
		stm.wSecond = 1;
		CTime tmStart(CEnviroment::CheckTime(stm));

		end_date.GetAsSystemTime(stm);
		stm.wHour = 23;
		stm.wMinute = 59;
		stm.wSecond = 59;
		CTime tmEnd(CEnviroment::CheckTime(stm));

		if(tmStart <= tmCur && tmCur <= tmEnd)
			item.iImage = (NUM_ENV_CONTENTS*3)+1;
		else
			item.iImage = (NUM_ENV_CONTENTS*3)+1+1;

		CString szCont;
		LVITEM itemPS;
		memset(&itemPS, 0x00, sizeof(LVITEM));
		itemPS.mask = LVIF_TEXT | LVIF_IMAGE;
		itemPS.iItem = idx;
		itemPS.iSubItem = eParentPlayContents;
		itemPS.iImage = -1;

		if(!pPlayContentsInfo->strParentPlayContentsId.IsEmpty())
		{
			TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
			if(pTempLink)
			{
				for(int ifrm = 0; ifrm < pTempLink->arFrameLinkList.GetCount(); ifrm++)
				{
					if(!pTempLink->arFrameLinkList.GetAt(ifrm).pFrameInfo) continue;

					for(int iScd = 0; iScd < pTempLink->arFrameLinkList.GetAt(ifrm).pFrameInfo->arCyclePlayContentsList.GetCount(); iScd++)
					{
						if(!pTempLink->arFrameLinkList.GetAt(ifrm).pFrameInfo->arCyclePlayContentsList.GetAt(iScd))
							continue;

						if(pTempLink->arFrameLinkList.GetAt(ifrm).pFrameInfo->arCyclePlayContentsList.GetAt(iScd)->strId != pPlayContentsInfo->strParentPlayContentsId)
							continue;

						CONTENTS_INFO* pCtsInfo = CDataContainer::getInstance()->GetContents(pTempLink->arFrameLinkList.GetAt(ifrm).pFrameInfo->arCyclePlayContentsList.GetAt(iScd)->strContentsId);
						if(!pCtsInfo) continue;

						szCont = "Frame:";
						szCont += pTempLink->arFrameLinkList.GetAt(ifrm).pFrameInfo->strId;
						szCont += " ";
						szCont += pCtsInfo?pCtsInfo->strContentsName:"";

						//pPlayContentsList->SetItemText(idx, eParentPlayContents, szCont);
						//pPlayContentsList->SetItemText(idx, eParentPlayContents, pPlayContentsInfo->strParentPlayContentsId);

						if(pCtsInfo->bLocalFileExist)
						{
							itemPS.iImage = pCtsInfo->nContentsType+1;
						}
						else
						{
							if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
							{
								if(pCtsInfo->bServerFileExist)
									itemPS.iImage = (pCtsInfo->nContentsType+1) + NUM_ENV_SERVER;
								else
									itemPS.iImage = (pCtsInfo->nContentsType+1) + NUM_ENV_INEXISTENT;
							}
							else
							{
								itemPS.iImage = (pCtsInfo->nContentsType+1) + NUM_ENV_INEXISTENT;
							}
						}
						//pPlayContentsList->SetItem(&itemPS);
					}
				}
			}
		}
		// 클릭스케줄 시, 다른 페이지로 이동하는 기능
		else if(!pPlayContentsInfo->strTouchTime.IsEmpty())
		{
			int pos = 0;
			CString strTarTemplateId = pPlayContentsInfo->strTouchTime.Tokenize(_T(","), pos);
			int nDuration = _ttoi(pPlayContentsInfo->strTouchTime.Tokenize(_T(","), pos));
			szCont.Format(_T("Goto [%s] Page"), strTarTemplateId);
		}

		//pPlayContentsList->SetItemText(idx, eParentPlayContents, szCont);
		itemPS.pszText = szCont.GetBuffer(0);
		pPlayContentsList->SetItem(&itemPS);
	}
	// 정시플레이콘텐츠
	else
	{
		int pos = 0;
		CString hour  = pPlayContentsInfo->strStartTime.Tokenize("/:-", pos);
		CString minute= pPlayContentsInfo->strStartTime.Tokenize("/:-", pos);
		CString second= pPlayContentsInfo->strStartTime.Tokenize("/:-", pos);

		start_date.GetAsSystemTime(stm);
		stm.wHour = atoi(hour);
		stm.wMinute = atoi(minute);
		stm.wSecond = atoi(second);
		CTime tmStart(CEnviroment::CheckTime(stm));

		pos = 0;
		hour  = pPlayContentsInfo->strEndTime.Tokenize("/:-", pos);
		minute= pPlayContentsInfo->strEndTime.Tokenize("/:-", pos);
		second= pPlayContentsInfo->strEndTime.Tokenize("/:-", pos);

		end_date.GetAsSystemTime(stm);
		stm.wHour = atoi(hour);
		stm.wMinute = atoi(minute);
		stm.wSecond = atoi(second);
		CTime tmEnd(CEnviroment::CheckTime(stm));

		if(tmStart <= tmCur && tmCur <= tmEnd)
			item.iImage = (NUM_ENV_CONTENTS*3)+1;
		else
			item.iImage = (NUM_ENV_CONTENTS*3)+1+1;
	}

	CString szTemp;
	// 순환플레이콘텐츠
	if(pPlayContentsInfo->bIsDefaultPlayContents)
	{
		szTemp = start_date.Format("%Y/%m/%d") + " ~ " + end_date.Format("%Y/%m/%d");
		item.pszText = szTemp.GetBuffer(0);
		pPlayContentsList->SetItem(&item);
		pPlayContentsList->SetItemText(idx, eTimeZone, TIME_SCOPE_TO_STRING[pPlayContentsInfo->nTimeScope]); // cycle

		if(!pPlayContentsInfo->strParentPlayContentsId.IsEmpty())
		{
			pPlayContentsList->SetRowColor(idx, ::GetSysColor(COLOR_WINDOW), RGB(191,191,191));
			//pPlayContentsList->SetRowColor(idx, RGB(191,191,191), RGB(64,64,64));
		}
		else
		{
			pPlayContentsList->SetRowColor(idx, ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_WINDOWTEXT));
		}
	}
	// 정시플레이콘텐츠
	else
	{
		szTemp.Format("%s~%s", start_date.Format("%Y/%m/%d"), end_date.Format("%Y/%m/%d"));
		item.pszText = szTemp.GetBuffer(0);
		pPlayContentsList->SetItem(&item);

		szTemp.Format("%s~%s", pPlayContentsInfo->strStartTime, pPlayContentsInfo->strEndTime);
		pPlayContentsList->SetItemText(idx, eTime, szTemp); // time base
	}

	if(pContentsInfo == NULL)
	{
		item.mask = LVIF_TEXT | LVIF_IMAGE;
		item.iItem = idx;
		item.iSubItem = eContentsType;
		item.iImage = 0;
		item.pszText = (LPSTR)CONTENTS_TYPE_TO_STRING[0];
		pPlayContentsList->SetItem(&item);

		pPlayContentsList->SetItemText(idx, eContentsName, "");
		pPlayContentsList->SetItemText(idx, eRunningTime , "");
		pPlayContentsList->SetItemText(idx, eFileName    , "");
		pPlayContentsList->SetItemText(idx, eSoundVolume , "");
	}
	else
	{
		item.mask = LVIF_TEXT | LVIF_IMAGE;
		item.iItem = idx;
		item.iSubItem = eContentsType;
		if(pContentsInfo->bLocalFileExist)
		{
			item.iImage = pContentsInfo->nContentsType+1;
		}
		else
		{
			if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty()){
				if(pContentsInfo->bServerFileExist)
					item.iImage = (pContentsInfo->nContentsType+1) + NUM_ENV_SERVER;
				else
					item.iImage = (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT;
			}else{
				item.iImage = (pContentsInfo->nContentsType+1) + NUM_ENV_INEXISTENT;
			}
		}
		item.pszText = (LPSTR)CONTENTS_TYPE_TO_STRING[pContentsInfo->nContentsType + 1];
		pPlayContentsList->SetItem(&item);

		pPlayContentsList->SetItemText(idx, eContentsName, pContentsInfo->strContentsName);
		pPlayContentsList->SetItemText(idx, eFileName    , pContentsInfo->strFilename);
		pPlayContentsList->SetItemText(idx, eSoundVolume , ::ToString(pContentsInfo->nSoundVolume));

		if(CONTENTS_PPT == pContentsInfo->nContentsType){
			pPlayContentsList->SetItemText(idx, eRunningTime, "");
		}else{
			CString tm;
			tm.Format("%02ld:%02ld", pContentsInfo->nRunningTime/60, pContentsInfo->nRunningTime % 60);
			pPlayContentsList->SetItemText(idx, eRunningTime, tm);
		}
	}

	if(nMode == 0 || nMode == 1 || bSelection)
	{
		pPlayContentsList->SetItemState(idx, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
		pPlayContentsList->EnsureVisible(idx, FALSE);
	}
	else
	{
		pPlayContentsList->SetItemState(idx, 0, LVIS_SELECTED);
	}

	pPlayContentsList->SetItemData(idx, (DWORD)pPlayContentsInfo);

	pPlayContentsList->SetFocus();

	return true;
}

bool CPackageView::RefreshPlayContentsList()
{
	m_lcCyclePlayContents.DeleteAllItems();
	m_lcTimeBasePlayContents.DeleteAllItems();

	CUBCStudioDoc *pDoc = GetDocument();
	FRAME_LINK* frame_link = pDoc->GetSelectFrameLink();

	CPtrArray aryCyclePlayContents;

	if(frame_link == NULL) return false;

	int nCnt = frame_link->pFrameInfo->arCyclePlayContentsList.GetCount();
	//frame link로 부터 playcontents을 가져와 play order순서를 ?임시 배열에 넣는다
	for(int i=0; i<nCnt; i++)
	{
		bool bInsert = false;
		PLAYCONTENTS_INFO* info = (PLAYCONTENTS_INFO*)frame_link->pFrameInfo->arCyclePlayContentsList.GetAt(i);
		if(info == NULL)
			continue;

		if(aryCyclePlayContents.GetCount() == 0)
		{
			aryCyclePlayContents.Add(info);
		}
		else
		{
			PLAYCONTENTS_INFO* TmpInfo;
			for(int j=0; j<aryCyclePlayContents.GetCount(); j++)
			{
				TmpInfo = (PLAYCONTENTS_INFO*)aryCyclePlayContents.GetAt(j);
				if(info->nPlayOrder <= TmpInfo->nPlayOrder)
				{
					aryCyclePlayContents.InsertAt(j, info, 1);
					bInsert = true;
					break;
				}//if
			}//for

			if(!bInsert)
			{
				aryCyclePlayContents.Add(info);
			}//if
		}//if
		//InsertPlayContents(info, 1);
	}//for

	//임시 배열로부터 list control에 playcontents을 넣는다
	for(int k=0; k<aryCyclePlayContents.GetCount(); k++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)aryCyclePlayContents.GetAt(k);
		InsertPlayContents(pPlayContentsInfo, 1);
	}//for

	for(int i=0; i<frame_link->pFrameInfo->arTimePlayContentsList.GetCount(); i++)
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)frame_link->pFrameInfo->arTimePlayContentsList.GetAt(i);
		InsertPlayContents(pPlayContentsInfo, 1);
	}

	ClearPlayContentsSelection();

	// 정시플레이콘텐츠 입력은 메인프레임에만 할수 있도록 함
	InitTabPlayContents();

	return true;
}
// Modified by 정운형 2009-01-06 오픸E11:35
// 변경내역 :  CLI 연동 수정

bool CPackageView::DeletePlayContents(POSITION pos)
{
	CUTBListCtrlEx* pCurrentPlayContents = GetCurrentPlayContentsListCtrl();
	int nItem = pCurrentPlayContents->GetNextSelectedItem(pos);

	if(pos != NULL)
		DeletePlayContents(pos);

	PLAYCONTENTS_INFO* info = (PLAYCONTENTS_INFO*)pCurrentPlayContents->GetItemData(nItem);
	PLAYCONTENTS_INFO DeleteInfo = *info;

	FRAME_LINK* frame_link = GetDocument()->GetSelectFrameLink();
	if(frame_link->pFrameInfo->DeletePlayContents(info))
	{
		if(pCurrentPlayContents->DeleteItem(nItem))
		{
			::AfxGetMainWnd()->SendMessage(WM_PLAYCONTENTS_LIST_CHANGED);
			m_wndTemplate.SendMessage(WM_PLAYCONTENTS_LIST_CHANGED);
			return true;
		}
	}
	return false;
}

void CPackageView::ClearPlayContentsSelection()
{
	for(int i=0; i<m_lcCyclePlayContents.GetItemCount(); i++)
		m_lcCyclePlayContents.SetItemState(i, 0, LVIS_FOCUSED | LVIS_SELECTED);
	for(int i=0; i<m_lcTimeBasePlayContents.GetItemCount(); i++)
		m_lcTimeBasePlayContents.SetItemState(i, 0, LVIS_FOCUSED | LVIS_SELECTED);
}

int CPackageView::GetPrevNoSelectPlayContentsIndex(CUTBListCtrlEx* pListCtrl, int nIndex)
{
	for(int i=nIndex-1; i>=0; i--)
	{
		if(pListCtrl->GetItemState(i, LVIS_SELECTED) == 0)
			return i;
	}
	return -1;
}

int CPackageView::GetNextNoSelectPlayContentsIndex(CUTBListCtrlEx* pListCtrl, int nIndex)
{
	for(int i=nIndex+1; i<pListCtrl->GetItemCount(); i++)
	{
		if(pListCtrl->GetItemState(i, LVIS_SELECTED) == 0)
			return i;
	}
	return -1;
}

void CPackageView::UpdateAllInfo()
{
	SendMessage(WM_FRAME_INFO_CHANGED);
	SendMessage(WM_TEMPLATE_INFO_CHANGED);
	SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED);
	SendMessage(WM_CONTENTS_LIST_CHANGED);

	m_lcContents.Sort(0, false);
}

bool CPackageView::DownloadSelectedContents()
{
#ifdef _UBCSTUDIO_EE_
	CFtpMultiSite multiFtp(GetEnvPtr()->m_strHttpOnly.CompareNoCase("CLIENT") == 0);

	CString szPackage;
	szPackage.Format("%s%s%s.ini", GetEnvPtr()->m_PackageInfo.szDrive, UBC_CONFIG_PATH, GetEnvPtr()->m_PackageInfo.szPackage);
	multiFtp.AddSite(GetEnvPtr()->m_PackageInfo.szSiteID, szPackage);
	multiFtp.OnlyAddFile();

	if(m_lcContents.GetSelectedCount() > 0)
	{
		POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
		while(pos)
		{
			int nItem = m_lcContents.GetNextSelectedItem(pos);
			CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
			if(info == NULL) continue;

			if(!info->bLocalFileExist && !info->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				multiFtp.AddDownloadFile(info->strServerLocation + info->strFilename);
			}
		}
	}
	else
	{
		for(int nItem = 0; nItem < m_lcContents.GetItemCount(); nItem++)
		{
			CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
			if(info == NULL) continue;

			if(!info->bLocalFileExist && !info->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				multiFtp.AddDownloadFile(info->strServerLocation + info->strFilename);
			}
		}
	}

	if(!multiFtp.RunFtp(CFtpMultiSite::eDownload)) return false;

	if(m_lcContents.GetSelectedCount() > 0)
	{
		POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
		while(pos)
		{
			int nItem = m_lcContents.GetNextSelectedItem(pos);
			CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
			if(info == NULL) continue;

			if(!info->bLocalFileExist && !info->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				info->bLocalFileExist = true;
				info->bServerFileExist = true;
			}
		}
	}
	else
	{
		for(int nItem = 0; nItem < m_lcContents.GetItemCount(); nItem++)
		{
			CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
			if(info == NULL) continue;

			if(!info->bLocalFileExist && !info->strFilename.IsEmpty() &&
				GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
			{
				info->bLocalFileExist = true;
				info->bServerFileExist = true;
			}
		}
	}
#endif//_UBCSTUDIO_EE_

	UpdateAllInfo();
//	RefreshPlayContentsList();
//	::AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED);

	return true;
}

// Modified by 정운형 2009-01-06 오픸E11:35
// 변경내역 :  CLI 연동 수정
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// default playcontents의 playorder의 순서를 ?list control의 순서를 sort한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CPackageView::SortDefaultPlayContentsByPlayOrder()
{
	int nCnt = m_lcCyclePlayContents.GetItemCount();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// complete config save message handler\n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CPackageView::OnConfigSaveComplete(WPARAM wParam, LPARAM lParam)
{
	CMainFrame* pFrm = (CMainFrame*)AfxGetMainWnd();
	CString strHostName = pFrm->GetHostName();
	strHostName.Replace(SAMPLE_FILE_KEY, "");

	// 2010.10.06 스튜디오의 타이틀에 콘텐츠 패키지경로 보여주기
	CString strDrive = pFrm->GetSourceDirive();
	strDrive.MakeUpper();

	CString strTitle;
	if(strHostName == "")
	{
		strTitle = LoadStringById(IDS_PLAYCONTENTSVIEW_STR001);
	}
	else
	{
		strTitle.Format(LoadStringById(IDS_PLAYCONTENTSVIEW_STR002), strDrive, strHostName);

		// <!-- 타이틀바 뒤에 (공개/비공개) 추가
		strTitle += " - (";
		strTitle += ( GetEnvPtr()->m_PackageInfo.bIsPublic ? LoadStringById(IDS_EEPACKAGEDLG_STR001) : LoadStringById(IDS_EEPACKAGEDLG_STR002) );
		strTitle += ")";
		// -->
	}//if

	GetDocument()->SetTitle(strTitle);

	return 0;
}

void CPackageView::UpdateTotalSize()
{
	ULONGLONG ulSize = GetDocument()->GetTotalContentsSize();

	CString szTotalSize = ToFileSize(ulSize, 'A');
	//if(ulSize > 1)
	//	szTotalSize += "s";

	if(IsWindow(m_stTotalSize.GetSafeHwnd()))
	{
		m_stTotalSize.SetWindowText(szTotalSize);
	}
}

void CPackageView::OnBnClickedButtonCopyTemplate()
{
	// 플레이 리스트 갯수가 지정된 갯수를 넘어가는지 체크
	if(!GetDocument()->CheckMaxPlayTemplateCount()) return;

	CUBCStudioDoc* pDoc = GetDocument();

	CString strSelectTemplateId = pDoc->GetSelectTemplateId();
	if(strSelectTemplateId == "") return;

	TEMPLATE_LINK_LIST* pTempList = pDoc->GetAllTemplateList();
	if(!pTempList) return;

	CString strNewPlayTemplateID = pDoc->CreateCopyPlayTemplate(strSelectTemplateId);
	if(strNewPlayTemplateID.IsEmpty()) return;

	pDoc->SetSelectTemplateId(strNewPlayTemplateID);
	SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)strNewPlayTemplateID, (WPARAM)(LPCTSTR)"");

	m_wndTemplate.SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED, 1); // 템플릿 추가

	::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_LIST_CHANGED);

	InitTabPlayContents();
}

void CPackageView::InitTabPlayContents()
{
	FRAME_LINK* pFrmLink = GetDocument()->GetSelectFrameLink();
	if(!pFrmLink)
	{
		if(m_tabPlayContents.GetItemCount() > 1)
		{
			m_tabPlayContents.DeleteItem(1);
			ToggleTabPlayContents(0);
			RECT rect = {0};
			m_tabPlayContents.GetWindowRect(&rect);
			ScreenToClient(&rect);
			InvalidateRect(&rect);
		}

		return;
	}

	// Primary Frame이고 정시플레이콘텐츠탭이 없는 경우 정시플레이콘텐츠탭을 생성한다
	if( pFrmLink->pFrameInfo->nGrade == 1 &&
		m_tabPlayContents.GetItemCount() <= 1 &&
		GetEnvPtr()->m_bUseTimeBasePlayContents )
	{
		m_tabPlayContents.InsertItem(1, LoadStringById(IDS_PLAYCONTENTSVIEW_LST002), 1);
		ToggleTabPlayContents(0);
		RECT rect = {0};
		m_tabPlayContents.GetWindowRect(&rect);
		ScreenToClient(&rect);
		InvalidateRect(&rect);
	}
	// Secondary Frame이고 정시플레이콘텐츠탭이 있는 경우 정시플레이콘텐츠탭을 삭제한다
	else if( pFrmLink->pFrameInfo->nGrade == 2   &&
			 m_tabPlayContents.GetItemCount() > 1 )
	{
		m_tabPlayContents.DeleteItem(1);
		ToggleTabPlayContents(0);
		RECT rect = {0};
		m_tabPlayContents.GetWindowRect(&rect);
		ScreenToClient(&rect);
		InvalidateRect(&rect);
	}
}

void CPackageView::OnLvnBegindragListCyclePlayContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pNMHDR->hwndFrom == m_lcCyclePlayContents.m_hWnd)
	{
		int count = m_lcCyclePlayContents.GetSelectedCount();
		if (count != 1) return;

		int idx = m_lcCyclePlayContents.GetNextItem(-1, LVIS_SELECTED);
		CPoint pt;
		m_ilPlayContentsDrag = m_lcCyclePlayContents.CreateDragImage(idx, &pt);
		if(m_ilPlayContentsDrag != NULL)
		{
			//CPoint ptAction(pNMLV->ptAction);
			m_dragStartPoint = pNMLV->ptAction;
			CRect rcItem;
			m_lcCyclePlayContents.GetItemRect(idx, &rcItem, LVIR_BOUNDS);
			//m_ilPlayContentsDrag->BeginDrag(0, CPoint(m_dragStartPoint.x - rcItem.left, m_dragStartPoint.y - rcItem.top));

			m_lcCyclePlayContents.ClientToScreen(&m_dragStartPoint);
			ScreenToClient(&m_dragStartPoint);
			TraceLog(("Drag Start=%d,%d", m_dragStartPoint.x, m_dragStartPoint.y));
			//m_ilPlayContentsDrag->DragEnter(this, m_dragStartPoint);
			SetCapture();

			m_nPrevDropIndex = -1;
		}
	}
}

void CPackageView::InitFrameColorList()
{
	// 비트맵용 버퍼 준비
	DWORD dwSize = 4 * 16 * 16;  // 비트맵 버퍼 크기
	BYTE* pBuf = new BYTE[dwSize];
	memset(pBuf, 0x00, sizeof(dwSize));

	BYTE color[4] = {0};
	color[0] = 255;  // blue 
	color[1] = 255;  // green 
	color[2] = 255;  // red 
	color[3] = 0  ;

	// 바탕 그리기
	for(int nHeight=0; nHeight<16 ;nHeight++)
	{
		for(int nWidth=0; nWidth<16 ; nWidth++)
		{
			BYTE color[4] = {0};
			color[0] = 255;  // blue 
			color[1] = 255;  // green 
			color[2] = 255;  // red 
			memcpy(pBuf + (nHeight * 16 * sizeof(COLORREF)) + (nWidth * sizeof(COLORREF)), &color, sizeof(COLORREF));
		}
	}

	color[0] = 0;  // blue 
	color[1] = 0;  // green 
	color[2] = 0;  // red 
	color[3] = 0;

	// 테두리 그리기
	for(int nHeight=1; nHeight<15 ;nHeight++)
	{
		for(int nWidth=1; nWidth<15 ; nWidth++)
		{
			memcpy(pBuf + (nHeight * 16 * sizeof(COLORREF)) + (nWidth * sizeof(COLORREF)), &color, sizeof(COLORREF));
		}
	}

	m_ilFrameColorList.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1);

	for(int i=0; i<100 ;i++)
	{
		COLORREF colorRGB = RGB(180,180,180);
		if( i == 99 )
		{
			colorRGB = FRAME_PRIMARY_COLOR;
		}
		else
		{
			colorRGB = CColorPalatte::getInstance()->GetFrameColor(i);
		}

		color[0] = GetBValue(colorRGB);  // blue 
		color[1] = GetGValue(colorRGB);  // green 
		color[2] = GetRValue(colorRGB);  // red 
		color[3] = 0;

		// 색깔 채우기
		for(int nHeight=2; nHeight<14 ;nHeight++)
		{
			for(int nWidth=2; nWidth<14 ; nWidth++)
			{
				memcpy(pBuf + (nHeight * 16 * sizeof(COLORREF)) + (nWidth * sizeof(COLORREF)), &color, sizeof(COLORREF));
			}
		}

		CBitmap bmpColor;
		bmpColor.CreateBitmap(16, 16, 1, 32, pBuf);

		m_ilFrameColorList.Add(&bmpColor, RGB(255,255,255)); //바탕의 회색이 마스크
	}

	CBitmap bmpColor;
	bmpColor.LoadBitmap(IDB_PLUS_RECT);
	m_ilFrameColorList.Add(&bmpColor, RGB(255,255,255)); //바탕의 회색이 마스크

	delete pBuf;
}

bool CPackageView::IsFitInsertRelation(PLAYCONTENTS_INFO* pParentInfo, PLAYCONTENTS_INFO* pInsertInfo)
{
	if(!pInsertInfo->strParentPlayContentsId.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG010));
		return false;
	}

	// 클릭스케줄 시, 다른 페이지로 이동하는 기능
	if(pParentInfo && !pParentInfo->strTouchTime.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG017));
		return false;
	}

	// 클릭스케줄 시, 다른 페이지로 이동하는 기능
	if(!pInsertInfo->strTouchTime.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG017));
		return false;
	}

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return true;

	// 부모로 등록가능한지 체크
	if(pParentInfo == NULL)
	{
		// 0000782: 부모 자식 스케줄 기능을 수정한다.
		if(GetEnvPtr()->m_bUseClickPlayContents) // parent 콘텐츠 패키지를 클릭하는 경우에 작동하도록 하는 경우이면
		{
			CONTENTS_INFO* pContentsInfo = pDoc->GetContents(pInsertInfo->strContentsId);
			if(pContentsInfo)
			{
				// 클릭을 인식하지 못하는 콘텐츠의 경우 parent 플레이콘텐츠로 설정할수 없도록 한다
				if( //pContentsInfo->nContentsType == CONTENTS_WEBBROWSER     ||
					//pContentsInfo->nContentsType == CONTENTS_FLASH          ||
					//pContentsInfo->nContentsType == CONTENTS_RSS            ||
					//pContentsInfo->nContentsType == CONTENTS_FLASH_TEMPLATE ||
					//pContentsInfo->nContentsType == CONTENTS_DYNAMIC_FLASH  ||
					pContentsInfo->nContentsType == CONTENTS_PPT            ||
					pContentsInfo->nContentsType == CONTENTS_WIZARD         )
				{
					CString strMsg;
					strMsg.Format(//_T("%s\n(%s, %s, %s, %s, %s)")
								  _T("%s\n(%s, %s)")
								, LoadStringById(IDS_PLAYCONTENTSVIEW_MSG015)
								//, CONTENTS_TYPE_TO_STRING[CONTENTS_WEBBROWSER +1]
								//, CONTENTS_TYPE_TO_STRING[CONTENTS_FLASH      +1]
								//, CONTENTS_TYPE_TO_STRING[CONTENTS_RSS        +1]
								, CONTENTS_TYPE_TO_STRING[CONTENTS_PPT        +1]
								, CONTENTS_TYPE_TO_STRING[CONTENTS_WIZARD     +1]
								);
					UbcMessageBox(strMsg);
					return false;
				}
			}
		}
	}
	// 자식으로 등록 가능한지 체크
	else
	{
		// 부모프레임과 같은 프레임에 속해있는지 체크
		if(pParentInfo->strFrameId == pInsertInfo->strFrameId)
		{
			UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG011));
			return false;
		}

		// 같은 프레임에 속한 플레이콘텐츠가 등록되어 있는지 체크
		TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
		if(!pTempLink) return true;

		for(int ifrm = 0; ifrm < pTempLink->arFrameLinkList.GetCount(); ifrm++)
		{
			FRAME_INFO*	pFrameInfo = pTempLink->arFrameLinkList[ifrm].pFrameInfo;
			if(!pFrameInfo) continue;

			if(pInsertInfo->strFrameId == pFrameInfo->strId)
			{
				for(int iScd = 0; iScd < pFrameInfo->arCyclePlayContentsList.GetCount(); iScd++)
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)pFrameInfo->arCyclePlayContentsList[iScd];
					if(!pPlayContentsInfo) continue;
					if(!pPlayContentsInfo->bIsDefaultPlayContents) continue;
					if(pPlayContentsInfo->strParentPlayContentsId.IsEmpty()) continue;

					if(pPlayContentsInfo->strId == pInsertInfo->strId) continue;

					if(pPlayContentsInfo->strParentPlayContentsId == pParentInfo->strId)
					{
						UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG014));
						return false;
					}
				}
				break;
			}
		}
	}

	return true;
}

void CPackageView::RefreshRelation()
{
	m_treeRelation.DeleteAllItems();

	if(GetEnvPtr()->m_bUseClickPlayContents)
	{
		m_treeRelation.InsertItem(LoadStringById(IDS_PLAYCONTENTSVIEW_REL003), 100, 100);
	}
	else
	{
		m_treeRelation.InsertItem(LoadStringById(IDS_PLAYCONTENTSVIEW_REL002), 100, 100);
	}

	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return;

	TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
	if(!pTempLink) return;

	for(int ifrm = 0; ifrm < pTempLink->arFrameLinkList.GetCount(); ifrm++)
	{
		FRAME_INFO*	pFrameInfo = pTempLink->arFrameLinkList[ifrm].pFrameInfo;
		if(!pFrameInfo) continue;

		for(int iScd = 0; iScd < pFrameInfo->arCyclePlayContentsList.GetCount(); iScd++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)pFrameInfo->arCyclePlayContentsList[iScd];
			if(!pPlayContentsInfo) continue;
			if(!pPlayContentsInfo->bIsDefaultPlayContents) continue;
			if(pPlayContentsInfo->strParentPlayContentsId.IsEmpty()) continue;

			HTREEITEM hRootItem = m_treeRelation.GetRootItem();
			HTREEITEM hFindItem = hRootItem;
			HTREEITEM hParentItem = NULL;
			HTREEITEM hInsertItem = NULL;

			int nFrameColorIndex = -1;

			while( (hFindItem = m_treeRelation.GetNextSiblingItem(hFindItem)) )
			{
				PLAYCONTENTS_INFO* pFindInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hFindItem);
				if(!pFindInfo) continue;

				if(pFindInfo->strId == pPlayContentsInfo->strParentPlayContentsId)
				{
					hParentItem = hFindItem;
					break;
				}
			}

			if(!hParentItem)
			{
				PLAYCONTENTS_INFO* pParentPlayContentsInfo = pDoc->GetPlayContents(pPlayContentsInfo->strTemplateId, pPlayContentsInfo->strParentPlayContentsId);
				if(!pParentPlayContentsInfo)
				{
					pPlayContentsInfo->strParentPlayContentsId.Empty();
					continue;
				}

				CONTENTS_INFO* pContentsInfo = pDoc->GetContents(pParentPlayContentsInfo->strContentsId);
				TEMPLATE_LINK* pTemplateLink = pDoc->GetTemplateLink(pParentPlayContentsInfo->strTemplateId);

				if(pContentsInfo && pTemplateLink)
				{
					for(int nTemp=0; nTemp<pTemplateLink->arFrameLinkList.GetCount() ; nTemp++)
					{
						if(pTemplateLink->arFrameLinkList[nTemp].pFrameInfo->strId == pParentPlayContentsInfo->strFrameId)
						{
							nFrameColorIndex = (pTemplateLink->arFrameLinkList[nTemp].pFrameInfo->nGrade == 1 ? 99 : _ttoi(pTemplateLink->arFrameLinkList[nTemp].pFrameInfo->strId));
							break;
						}
					}

					hParentItem = m_treeRelation.InsertItem(pContentsInfo->strContentsName, nFrameColorIndex, nFrameColorIndex, NULL, NULL);
					m_treeRelation.SetItemData(hParentItem, (DWORD_PTR)pParentPlayContentsInfo);
				}
			}

			nFrameColorIndex = (pFrameInfo->nGrade == 1 ? 99 : _ttoi(pFrameInfo->strId));
			CONTENTS_INFO* pContentsInfo = pDoc->GetContents(pPlayContentsInfo->strContentsId);
			if(pContentsInfo)
			{
				hInsertItem = m_treeRelation.InsertItem(pContentsInfo->strContentsName, nFrameColorIndex, nFrameColorIndex, hParentItem, NULL);
				m_treeRelation.SetItemData(hInsertItem, (DWORD_PTR)pPlayContentsInfo);
				m_treeRelation.Expand(hParentItem, TVE_EXPAND);
			}
		}
	}

	// 클릭스케줄 시, 다른 페이지로 이동하는 기능
	for(int ifrm = 0; ifrm < pTempLink->arFrameLinkList.GetCount(); ifrm++)
	{
		FRAME_INFO*	pFrameInfo = pTempLink->arFrameLinkList[ifrm].pFrameInfo;
		if(!pFrameInfo) continue;

		for(int iScd = 0; iScd < pFrameInfo->arCyclePlayContentsList.GetCount(); iScd++)
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)pFrameInfo->arCyclePlayContentsList[iScd];
			if(!pPlayContentsInfo) continue;
			if(!pPlayContentsInfo->bIsDefaultPlayContents) continue;
			if(pPlayContentsInfo->strTouchTime.IsEmpty()) continue;

			HTREEITEM hRootItem = m_treeRelation.GetRootItem();
			HTREEITEM hFindItem = hRootItem;
			HTREEITEM hParentItem = NULL;
			HTREEITEM hInsertItem = NULL;

			int nFrameColorIndex = -1;

			while( (hFindItem = m_treeRelation.GetNextSiblingItem(hFindItem)) )
			{
				PLAYCONTENTS_INFO* pFindInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hFindItem);
				if(!pFindInfo) continue;

				if(pFindInfo->strId == pPlayContentsInfo->strId)
				{
					hParentItem = hFindItem;
					break;
				}
			}

			if(hParentItem)
			{
				// 딸린 자식들의 관계를 모두 삭제한다
				HTREEITEM hChildItem = NULL;
				while( (hChildItem = m_treeRelation.GetChildItem(hParentItem)) )
				{
					PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hChildItem);
					if(pPlayContentsInfo)
					{
						pPlayContentsInfo->strParentPlayContentsId.Empty();
					}
					m_treeRelation.DeleteItem(hChildItem);
				}
			}
			else
			{
				CONTENTS_INFO* pContentsInfo = pDoc->GetContents(pPlayContentsInfo->strContentsId);
				TEMPLATE_LINK* pTemplateLink = pDoc->GetTemplateLink(pPlayContentsInfo->strTemplateId);

				if(pContentsInfo && pTemplateLink)
				{
					for(int nTemp=0; nTemp<pTemplateLink->arFrameLinkList.GetCount() ; nTemp++)
					{
						if(pTemplateLink->arFrameLinkList[nTemp].pFrameInfo->strId == pPlayContentsInfo->strFrameId)
						{
							nFrameColorIndex = (pTemplateLink->arFrameLinkList[nTemp].pFrameInfo->nGrade == 1 ? 99 : _ttoi(pTemplateLink->arFrameLinkList[nTemp].pFrameInfo->strId));
							break;
						}
					}

					hParentItem = m_treeRelation.InsertItem(pContentsInfo->strContentsName, nFrameColorIndex, nFrameColorIndex, NULL, NULL);
					m_treeRelation.SetItemData(hParentItem, (DWORD_PTR)pPlayContentsInfo);
				}
			}

			nFrameColorIndex = 98;

			int pos = 0;
			CString strTarTemplateId = pPlayContentsInfo->strTouchTime.Tokenize(_T(","), pos);
			int nDuration = _ttoi(pPlayContentsInfo->strTouchTime.Tokenize(_T(","), pos));
			CString strItemText;
			strItemText.Format(_T("Goto [%s] Page"), strTarTemplateId);

			hInsertItem = m_treeRelation.InsertItem(strItemText, nFrameColorIndex, nFrameColorIndex, hParentItem, NULL);
			m_treeRelation.SetItemData(hInsertItem, (DWORD_PTR)NULL);
			m_treeRelation.Expand(hParentItem, TVE_EXPAND);
		}
	}

	m_treeRelation.SelectItem(m_treeRelation.GetRootItem());

	RECT rect = {0};
	m_treeRelation.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect);
}

void CPackageView::OnTvnKeydownTreeRelation(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTVKEYDOWN pTVKeyDown = reinterpret_cast<LPNMTVKEYDOWN>(pNMHDR);
	*pResult = 0;

	if(pTVKeyDown->wVKey != VK_DELETE) return;

	OnBnClickedButtonRemoveRelation();
}

void CPackageView::OnBnClickedButtonRemoveRelation()
{
	HTREEITEM hSelItem = m_treeRelation.GetSelectedItem();
	if(!hSelItem) return;
	if(m_treeRelation.GetRootItem() == hSelItem) return;
	if(UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG009), MB_YESNO) != IDYES) return;

	HTREEITEM hParentItem = m_treeRelation.GetParentItem(hSelItem);

	// 클릭스케줄 시, 다른 페이지로 이동하는 기능
	if(hParentItem)
	{
		PLAYCONTENTS_INFO* pParentPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hParentItem);
		if(pParentPlayContentsInfo && !pParentPlayContentsInfo->strTouchTime.IsEmpty())
		{
			hSelItem = hParentItem;
			hParentItem = NULL;
		}
	}

	// 부모 플레이콘텐츠인 경우
	if(hParentItem == NULL)
	{
		// 딸린 자식들의 관계를 모두 삭제한다
		HTREEITEM hChildItem = NULL;
		while( (hChildItem = m_treeRelation.GetChildItem(hSelItem)) )
		{
			PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hChildItem);
			if(pPlayContentsInfo)
			{
				pPlayContentsInfo->strParentPlayContentsId.Empty();
			}
			m_treeRelation.DeleteItem(hChildItem);
		}

		PLAYCONTENTS_INFO* pParentPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hSelItem);
		if(pParentPlayContentsInfo)
		{
			pParentPlayContentsInfo->strTouchTime.Empty();
		}

		HTREEITEM hPrevSiblingItem = m_treeRelation.GetPrevSiblingItem(hSelItem);
		HTREEITEM hNextSiblingItem = m_treeRelation.GetNextSiblingItem(hSelItem);
		if     (hPrevSiblingItem) m_treeRelation.SelectItem(hPrevSiblingItem);
		else if(hNextSiblingItem) m_treeRelation.SelectItem(hNextSiblingItem);
		else                      m_treeRelation.SelectItem(m_treeRelation.GetRootItem());

		m_treeRelation.DeleteItem(hSelItem);
	}
	// 자식 플레이콘텐츠인 경우
	else
	{
		HTREEITEM hPrevSiblingItem = m_treeRelation.GetPrevSiblingItem(hSelItem);
		HTREEITEM hNextSiblingItem = m_treeRelation.GetNextSiblingItem(hSelItem);
		if     (hPrevSiblingItem) m_treeRelation.SelectItem(hPrevSiblingItem);
		else if(hNextSiblingItem) m_treeRelation.SelectItem(hNextSiblingItem);
		else                      m_treeRelation.SelectItem(hParentItem);

		// 선택된 플레이콘텐츠의 관계만 삭제한다
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hSelItem);
		if(pPlayContentsInfo)
		{
			pPlayContentsInfo->strParentPlayContentsId.Empty();
			m_treeRelation.DeleteItem(hSelItem);
		}
	}

	RefreshPlayContentsList();
}

void CPackageView::OnNMClickListCyclePlayContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	if( m_lcCyclePlayContents.GetItemCount() <= 0            ||
		m_lcCyclePlayContents.GetItemCount() >= pNMLV->iItem )
	{
		return;
	}

	PLAYCONTENTS_INFO* pSelInfo = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(pNMLV->iItem);
	if(!pSelInfo) return;

	// 선택한 플레이콘텐츠로 플레이콘텐츠 관계 트리에서 항목을 선택하도록 한다
	for(HTREEITEM hParentItem = m_treeRelation.GetChildItem(NULL);
		hParentItem != NULL ;
		hParentItem = m_treeRelation.GetNextSiblingItem(hParentItem) )
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hParentItem);
		if(!pPlayContentsInfo) continue;

		if(pPlayContentsInfo == pSelInfo)
		{
			m_treeRelation.SelectItem(hParentItem);
			m_treeRelation.Expand(hParentItem, TVE_EXPAND);
			return;
		}
		else
		{
			for( HTREEITEM hChildItem = m_treeRelation.GetChildItem(hParentItem);
			     hChildItem != NULL ;
			     hChildItem = m_treeRelation.GetNextSiblingItem(hChildItem) )
			{
				PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hChildItem);
				if(!pPlayContentsInfo) continue;

				if(pPlayContentsInfo == pSelInfo)
				{
					m_treeRelation.SelectItem(hChildItem);
					m_treeRelation.Expand(hChildItem, TVE_EXPAND);
					return;
				}
			}
		}
	}
}

void CPackageView::OnNMClickTreeRelation(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	TVHITTESTINFO treeinfo;
	memset(&treeinfo, 0x00, sizeof(TVHITTESTINFO));
	::GetCursorPos(&treeinfo.pt);
	m_treeRelation.ScreenToClient(&treeinfo.pt);
	m_treeRelation.HitTest(&treeinfo);

	if( !(TVHT_ONITEM & treeinfo.flags) ||
		treeinfo.hItem == NULL                         ||
		treeinfo.hItem == m_treeRelation.GetRootItem() )
	{
		return;
	}

	PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(treeinfo.hItem);
	if(!pPlayContentsInfo) return;

	// 플레이콘텐츠 관계 트리에서 선택한 플레이콘텐츠로 프레임 및 플레이콘텐츠리스트에서 항목을 선택하도록 한다
	SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)pPlayContentsInfo->strTemplateId, (LPARAM)(LPCTSTR)pPlayContentsInfo->strFrameId);

	for(int nListIdx=0; nListIdx<m_lcCyclePlayContents.GetItemCount(); nListIdx++)
	{
		PLAYCONTENTS_INFO* pSelInfo = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(nListIdx);
		if(!pSelInfo) continue;

		if(pPlayContentsInfo == pSelInfo)
		{
			ToggleTabPlayContents(0);
			m_lcCyclePlayContents.SetItemState(nListIdx, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
			break;
		}
	}
}

LRESULT	CPackageView::OnContentsFilter(WPARAM wParam, LPARAM lParam)
{
//	if(m_cbContentsFilter.IsDropped()) return 0;

	for(int i=0; i<CONTENTS_CNT ;i++)
	{
		m_lcContents.m_bContentsFilter[i] = FALSE;
	}

	CString strCheckedIDs = m_cbContentsFilter.GetCheckedIDs();

	TraceLog(("CheckedIDs = [%s]", strCheckedIDs));
	strCheckedIDs.Replace("(","");
	strCheckedIDs.Replace(")","");

	if(strCheckedIDs == _T("All"))
	{
		for(int i=0; i<CONTENTS_CNT ;i++)
		{
			m_lcContents.m_bContentsFilter[i] = TRUE;
		}
	}
	else
	{
		int nPos = 0;
		CString strTok = strCheckedIDs.Tokenize(",", nPos);
		while(strTok != "")
		{
			strTok.Trim();
			int nIndex = atoi(strTok);
			strTok = strCheckedIDs.Tokenize(",", nPos);

			if(nIndex < 0 || nIndex >= CONTENTS_CNT) continue;

			m_lcContents.m_bContentsFilter[nIndex] = TRUE;
		}
	}

	m_lcContents.RefreshContents();
	m_lcContents.SortCur();

	return 0;
}

void CPackageView::OnBnClickedButtonContentsManager()
{
#ifdef _UBCSTUDIO_EE_
	if(UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG018), MB_YESNO) != IDYES) return;

	// 공용 Contents 바스켓으로 복사
	CPublicContentsProcessingDlg dlg(GetEnvPtr()->m_PackageInfo.szSiteID, GetEnvPtr()->m_PackageInfo.szPackage);

	int nCnt = 0;
	POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
	while(pos != NULL)
	{
		int nItem = m_lcContents.GetNextSelectedItem(pos);

		CONTENTS_INFO* pInfo = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
		if(!pInfo) continue;
		if(pInfo->bIsPublic) continue;

		if(pInfo->bServerFileExist)
		{
			dlg.AddCopyFromPrivateContents(pInfo);
			nCnt++;
		}
	}

	if(nCnt<=0)
	{
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG013));
		return;
	}

	dlg.DoModal();
#endif
}

// 0000783: 플래쉬 위자드 기능을 대폭 갋수정한다.
void CPackageView::OnBnClickedButtonWizardTree()
{
	POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
	if(pos == NULL)
	{
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG016));
		return;
	}

	int nItem = m_lcContents.GetNextSelectedItem(pos);

	CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
	if(pContentsInfo && pContentsInfo->nContentsType == CONTENTS_WIZARD)
	{
		CWizardTreeDlg dlg;
		dlg.SetDocument(GetDocument());
		dlg.SetContentsId(pContentsInfo->strId);
		dlg.DoModal();
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_PLAYCONTENTSVIEW_MSG016));
		return;
	}
}

void CPackageView::OnNMRclickTreeRelation(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!GetEnvPtr()->m_bUseClickPlayContents) return;

	TVHITTESTINFO treeinfo;
	memset(&treeinfo, 0x00, sizeof(TVHITTESTINFO));
	::GetCursorPos(&treeinfo.pt);
	m_treeRelation.ScreenToClient(&treeinfo.pt);
	m_treeRelation.HitTest(&treeinfo);

	if( !(TVHT_ONITEM & treeinfo.flags) ||
		treeinfo.hItem == NULL                         ||
		treeinfo.hItem == m_treeRelation.GetRootItem() )
	{
		return;
	}

	PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(treeinfo.hItem);
	if(!pPlayContentsInfo) return;

	m_hSelectItem = treeinfo.hItem;

	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));
	CMenu* pPopup = menu.GetSubMenu(1);
	ASSERT(pPopup != NULL);

	CPoint pt;
	GetCursorPos(&pt);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);

	*pResult = 0;
}

void CPackageView::OnGotoTemplate()
{
	if(!m_hSelectItem) return;

	PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(m_hSelectItem);
	if(!pPlayContentsInfo) return;

//	pPlayContentsInfo->strTouchTime = _T("T:12,30,06");

	CString strTarTemplateId;
	int nDuration = 0;

	if(!pPlayContentsInfo->strTouchTime.IsEmpty())
	{
		int pos = 0;
		strTarTemplateId = pPlayContentsInfo->strTouchTime.Tokenize(_T(","), pos);
		nDuration = _ttoi(pPlayContentsInfo->strTouchTime.Tokenize(_T(","), pos));
	}

	CGoToTemplateDlg dlg;
	dlg.SetDocument(GetDocument());
	dlg.SetParam( strTarTemplateId, nDuration, pPlayContentsInfo->strTemplateId );

	if(dlg.DoModal() != IDOK) return;

	dlg.GetParam( strTarTemplateId, nDuration );

	pPlayContentsInfo->strTouchTime.Format(_T("%s,%d,%s"), strTarTemplateId, nDuration, pPlayContentsInfo->strTemplateId);

	// 부모 플레이콘텐츠인 경우는 딸린 자식들의 관계를 모두 삭제 후 이동할 페이지 정보를 설정한다
	HTREEITEM hChildItem = NULL;
	while( (hChildItem = m_treeRelation.GetChildItem(m_hSelectItem)) )
	{
		PLAYCONTENTS_INFO* pPlayContentsInfo = (PLAYCONTENTS_INFO*)m_treeRelation.GetItemData(hChildItem);
		if(pPlayContentsInfo)
		{
			pPlayContentsInfo->strParentPlayContentsId.Empty();
		}
		m_treeRelation.DeleteItem(hChildItem);
	}

	//HTREEITEM hPrevSiblingItem = m_treeRelation.GetPrevSiblingItem(hSelItem);
	//HTREEITEM hNextSiblingItem = m_treeRelation.GetNextSiblingItem(hSelItem);
	//if     (hPrevSiblingItem) m_treeRelation.SelectItem(hPrevSiblingItem);
	//else if(hNextSiblingItem) m_treeRelation.SelectItem(hNextSiblingItem);
	//else                      m_treeRelation.SelectItem(m_treeRelation.GetRootItem());

	RefreshPlayContentsList();
	RefreshRelation();
}

void CPackageView::OnNMRclickListTimeBasePlayContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	TraceLog(("OnNMRclickListTimeBasePlayContents()"));

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	if( m_lcTimeBasePlayContents.GetItemCount() <= 0            ||
		m_lcTimeBasePlayContents.GetItemCount() < pNMLV->iItem )
	{
		m_selectedContents=NULL;
		TraceLog(("list=%d, selected number=%d", m_lcTimeBasePlayContents.GetItemCount(), pNMLV->iItem));
		return;
	}

	m_selectedContents = (PLAYCONTENTS_INFO*)m_lcTimeBasePlayContents.GetItemData(pNMLV->iItem);
	if(!m_selectedContents) {
		TraceLog(("selected contents is null, list=%d, selected number=%d", m_lcTimeBasePlayContents.GetItemCount(), pNMLV->iItem));
		return;
	}

	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));
	CMenu* pPopup = menu.GetSubMenu(2);
	ASSERT(pPopup != NULL);

	CPoint pt;
	GetCursorPos(&pt);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CPackageView::OnTimebaserclickIteration()
{
	TraceLog(("OnTimebaserclickIteration()"));
	
	if(m_selectedContents == NULL) return;

	CTimeBaseIterationDlg dlg;
	if(dlg.DoModal() != IDOK) return;

	int period = dlg.GetIterPeriod();
	int count = dlg.GetIterCount();

	if(period <= 0) return;
	if(count <= 0) return;

	int aDay = 60*60*24;

	int succeed = 0;
	for(int i=1;i<=count;i++){

		int offset = period*i;

		PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
		*pPlayContentsInfo = *m_selectedContents;

		if(	pPlayContentsInfo->strStartTime.GetLength() < 8	||
			pPlayContentsInfo->strEndTime.GetLength() < 8	){
			TraceLog(("%s ~ %s is invalid time format", pPlayContentsInfo->strStartTime, pPlayContentsInfo->strEndTime));
			continue;
		}

		int nStartHour = atoi(pPlayContentsInfo->strStartTime.Mid(0,2));
		int nStartMin  = atoi(pPlayContentsInfo->strStartTime.Mid(3,2));
		int nStartSec  = atoi(pPlayContentsInfo->strStartTime.Mid(6,2));
		int nStartOffset = (nStartHour*60*60 + nStartMin*60 + nStartSec) + offset;

		int nEndHour = atoi(pPlayContentsInfo->strEndTime.Mid(0,2));
		int nEndMin  = atoi(pPlayContentsInfo->strEndTime.Mid(3,2));
		int nEndSec  = atoi(pPlayContentsInfo->strEndTime.Mid(6,2));
		int nEndOffset = (nEndHour*60*60 + nEndMin*60 + nEndSec) + offset;

		if(nStartOffset >= aDay || nEndOffset >= aDay){
			TraceLog(("Day over data ignored"));
			break;
		}

		nStartHour = int(nStartOffset/(60*60));
		nStartMin  = int((nStartOffset-(nStartHour*60*60))/60);
		nStartSec  = nStartOffset - (nStartHour*60*60) - (nStartMin*60);

		nEndHour = int(nEndOffset/(60*60));
		nEndMin  = int((nEndOffset-(nEndHour*60*60))/60);
		nEndSec  = nEndOffset - (nEndHour*60*60) - (nEndMin*60);

		//CTime tmStartTime((__int64)(pPlayContentsInfo->nStartDate/aDay) + nStartOffset);
		//CTime tmEndTime((__int64)(pPlayContentsInfo->nEndDate/aDay) + nEndOffset);

		//pPlayContentsInfo->nStartDate	= 	(int)(tmStartTime.GetTime()/aDay);
		//pPlayContentsInfo->nEndDate     =   (int)(tmEndTime.GetTime()/aDay);
		//pPlayContentsInfo->strStartTime.Format("%02d:%02d:%02d", tmStartTime.GetHour(), tmStartTime.GetMinute(), tmStartTime.GetSecond());
		//pPlayContentsInfo->strEndTime.Format("%02d:%02d:%02d", tmEndTime.GetHour(), tmEndTime.GetMinute(), tmEndTime.GetSecond());
		
		pPlayContentsInfo->strStartTime.Format("%02d:%02d:%02d", nStartHour,nStartMin,nStartSec);
		pPlayContentsInfo->strEndTime.Format("%02d:%02d:%02d", nEndHour,nEndMin,nEndSec);

		TraceLog(("%s ~ %s", pPlayContentsInfo->strStartTime, pPlayContentsInfo->strEndTime));

		InsertPlayContents(pPlayContentsInfo, 0);
		ReorderListCtrl(m_lcTimeBasePlayContents);

		LRESULT result;
		OnTcnSelchangeTabPlayContents(NULL, &result);
		succeed++;
	}
	// 정시플레이콘텐츠의 시간이 겹치는 경우 알림
	GetDocument()->CheckTimeBasePlayContentsOverlap(m_tabPlayContents.GetCurSel());
	GetCurrentPlayContentsListCtrl()->SetFocus();
	
	CString msg;
	if(count > succeed){
		UbcMessageBox(LoadStringById(IDS_PACKAGEVIEW_MSG006));
	}
	msg.Format(LoadStringById(IDS_PACKAGEVIEW_MSG005), succeed);
	UbcMessageBox(msg);

}

void CPackageView::OnCirclebaserclickCopy()
{
	TraceLog(("OnCirclebaserclickCopy()"));

	if(m_selectedContents == NULL) return;

	CCycleIterationDlg dlg;
	if( dlg.DoModal() == IDCANCEL ) return;

	int cnt = dlg.GetIterCount();
	for(int i=0; i<cnt; i++)
	{
		for(int j=0; j<m_selectedContentsList.GetCount(); j++)
		{
			PLAYCONTENTS_INFO* info = (PLAYCONTENTS_INFO*)m_selectedContentsList.GetAt(j);

			PLAYCONTENTS_INFO* pPlayContentsInfo = new PLAYCONTENTS_INFO;
			*pPlayContentsInfo = *info;

			InsertPlayContents(pPlayContentsInfo, 0, true);
		}
	}
	ReorderListCtrl(m_lcCyclePlayContents);

	LRESULT result;
	OnTcnSelchangeTabPlayContents(NULL, &result);
	GetCurrentPlayContentsListCtrl()->SetFocus();
}

void CPackageView::OnNMRclickListCyclePlaycontents(NMHDR *pNMHDR, LRESULT *pResult)
{
	TraceLog(("OnNMRclickListCyclePlaycontents()"));

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	if( m_lcCyclePlayContents.GetItemCount() <= 0            ||
		m_lcCyclePlayContents.GetItemCount() < pNMLV->iItem )
	{
		m_selectedContents=NULL;
		TraceLog(("list=%d, selected number=%d", m_lcCyclePlayContents.GetItemCount(), pNMLV->iItem));
		return;
	}

	m_selectedContents = (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(pNMLV->iItem);
	if(!m_selectedContents) {
		TraceLog(("selected contents is null, list=%d, selected number=%d", m_lcCyclePlayContents.GetItemCount(), pNMLV->iItem));
		return;
	}
	m_selectedContentsList.RemoveAll();
	for(int i=0; i<m_lcCyclePlayContents.GetItemCount(); i++)
	{
		if( m_lcCyclePlayContents.GetItemState(i, LVIS_SELECTED) )
		{
			m_selectedContentsList.Add( (PLAYCONTENTS_INFO*)m_lcCyclePlayContents.GetItemData(i) );
		}
	}

	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));
	CMenu* pPopup = menu.GetSubMenu(3);
	ASSERT(pPopup != NULL);

	CPoint pt;
	GetCursorPos(&pt);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CPackageView::OnBnClickedButtonCondition()
{
#ifdef _UBCSTUDIO_PE_
	return;
#else

	//skpark 2016.09.01 for UBGolf
	if(CEnviroment::eUBGOLF == GetEnvPtr()->m_Customer)	{
		InputAdvertiserName();
		return;
	}


	CDisplayConditionDlg dlg;

	POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
	if(pos == NULL)
	{
		//UbcMessageBox();
		return ;
	}
	
	int nItem = m_lcContents.GetNextSelectedItem(pos);
	CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
	if(info == NULL) {
		//UbcMessageBox();
		return ;
	}

	dlg.fromString(info->strIncludeCategory,info->strExcludeCategory);

	if(dlg.DoModal() != IDOK) return;

	CString strIncludeCategory;
	CString strExcludeCategory;
	dlg.toString(strIncludeCategory,strExcludeCategory);

	pos = m_lcContents.GetFirstSelectedItemPosition();
	while(pos != NULL)
	{
		int nItem = m_lcContents.GetNextSelectedItem(pos);

		CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
		if(info == NULL) continue;

		info->strIncludeCategory = strIncludeCategory;
		info->strExcludeCategory = strExcludeCategory;
	}

	m_lcContents.RefreshContents();
#endif
}

void CPackageView::OnBnClickedButtonHSizeToggle()
{
	// skpark 2012.12.07
	if(m_h_toggle<0) {
		InitHSizeToggle();	
		m_h_toggle++;
		return;
	}
	m_h_toggle++;
	HSizeToggle(m_h_toggle);

}

void CPackageView::InitHSizeToggle()
{
	m_lcContents.GetWindowRect(m_rcCon);
	m_frameTemplate.GetWindowRect(m_rcTemplate);
	m_wndTemplate.GetWindowRect(m_rcWndTemplate);
	m_frameFrame.GetWindowRect(m_rcFrame);
	m_wndFrame.GetWindowRect(m_rcWndFrame);
	m_tabPlayContents.GetWindowRect(m_rcTab);
	m_lcCyclePlayContents.GetWindowRect(m_rcList);
	m_lcTimeBasePlayContents.GetWindowRect(m_rcList2);
	m_btnHSizeToggle.GetWindowRect(m_rcHToggle);
	m_btnSizeToggle.GetWindowRect(m_rcToggle);

	ScreenToClient(m_rcCon);
	ScreenToClient(m_rcTemplate);
	ScreenToClient(m_rcFrame);
	ScreenToClient(m_rcWndTemplate);
	ScreenToClient(m_rcWndFrame);
	ScreenToClient(m_rcTab);
	ScreenToClient(m_rcList);
	ScreenToClient(m_rcList2);
	ScreenToClient(m_rcHToggle);
	ScreenToClient(m_rcToggle);
}

void CPackageView::HSizeToggle(int toggle)
{
	char buf[64];
	if(toggle %2==0 ){
		m_btnSizeToggle.ShowWindow(SW_SHOW);

		sprintf(buf,"shrink toggle=%ld", toggle);
		m_lcContents.SetWindowPos(NULL,m_rcCon.left,m_rcCon.top,m_rcCon.Width(),m_rcCon.Height(),SWP_SHOWWINDOW);
		m_btnHSizeToggle.SetWindowPos(NULL,m_rcHToggle.left,m_rcHToggle.top,m_rcHToggle.Width(),m_rcHToggle.Height(),SWP_SHOWWINDOW);
		m_btnSizeToggle.SetWindowPos(NULL,m_rcToggle.left,m_rcToggle.top,m_rcToggle.Width(),m_rcToggle.Height(),SWP_SHOWWINDOW);
		m_wndTemplate.SetWindowPos(NULL,m_rcWndTemplate.left,m_rcWndTemplate.top,m_rcWndTemplate.Width(),m_rcWndTemplate.Height(),SWP_SHOWWINDOW);
		m_wndFrame.SetWindowPos(NULL,m_rcWndFrame.left,m_rcWndFrame.top,m_rcWndFrame.Width(),m_rcWndFrame.Height(),SWP_SHOWWINDOW);
		m_frameTemplate.SetWindowPos(NULL,m_rcTemplate.left,m_rcTemplate.top,m_rcTemplate.Width(),m_rcTemplate.Height(),SWP_SHOWWINDOW);
		m_frameFrame.SetWindowPos(NULL,m_rcFrame.left,m_rcFrame.top,m_rcFrame.Width(),m_rcFrame.Height(),SWP_SHOWWINDOW);
		m_tabPlayContents.SetWindowPos(NULL,m_rcTab.left,m_rcTab.top,m_rcTab.Width(),m_rcTab.Height(),SWP_SHOWWINDOW);
		if(m_tabPlayContents.GetCurSel() == 0) {
			m_lcCyclePlayContents.SetWindowPos(NULL,m_rcList.left,m_rcList.top,m_rcList.Width(),m_rcList.Height(),SWP_SHOWWINDOW);
		}else{	
			m_lcTimeBasePlayContents.SetWindowPos(NULL,m_rcList2.left,m_rcList2.top,m_rcList2.Width(),m_rcList2.Height(),SWP_SHOWWINDOW);
		}
		m_staticNameFilter.ShowWindow(SW_HIDE);
		m_editNameFilter.ShowWindow(SW_HIDE);
		m_btNameFilter.ShowWindow(SW_HIDE);
	}else {
		sprintf(buf,"expand toggle=%ld", toggle);
		//UbcMessageBox(buf);

		m_btnSizeToggle.ShowWindow(SW_HIDE);

		int expand = m_rcCon.Width() + 100;

		m_lcContents.SetWindowPos(NULL,
			m_rcCon.left,m_rcCon.top
			,m_rcCon.Width()+expand,	m_rcCon.Height(),SWP_SHOWWINDOW);

		m_btnHSizeToggle.SetWindowPos(NULL,
			m_rcHToggle.left+expand,m_rcHToggle.top,
			m_rcHToggle.Width(),m_rcHToggle.Height(),SWP_SHOWWINDOW);
		/*
		m_btnSizeToggle.SetWindowPos(NULL,
			m_rcToggle.left+(expand/2),m_rcToggle.top,
			m_rcToggle.Width(),m_rcToggle.Height(),SWP_SHOWWINDOW);
		*/
		m_wndTemplate.SetWindowPos(NULL,
			m_rcWndTemplate.left+expand,		m_rcWndTemplate.top,
			m_rcWndTemplate.Width()-expand,	m_rcWndTemplate.Height(),
			SWP_SHOWWINDOW);
		m_wndFrame.SetWindowPos(NULL,
			m_rcWndFrame.left+expand,			m_rcWndFrame.top,
			m_rcWndFrame.Width()-expand,		m_rcWndFrame.Height(),
			SWP_SHOWWINDOW);

		m_frameTemplate.SetWindowPos(NULL,
			m_rcTemplate.left+expand,		m_rcTemplate.top,
			m_rcTemplate.Width()-expand,	m_rcTemplate.Height(),
			SWP_SHOWWINDOW);
		m_frameFrame.SetWindowPos(NULL,
			m_rcFrame.left+expand,			m_rcFrame.top,
			m_rcFrame.Width()-expand,		m_rcFrame.Height(),
			SWP_SHOWWINDOW);

		m_tabPlayContents.SetWindowPos(NULL,
			m_rcTab.left+expand,			m_rcTab.top,
			m_rcTab.Width()-expand,			m_rcTab.Height(),
			SWP_SHOWWINDOW);

		if(m_tabPlayContents.GetCurSel() == 0) {
			m_lcCyclePlayContents.SetWindowPos(NULL,
				m_rcList.left+expand,		m_rcList.top,
				m_rcList.Width()-expand,	m_rcList.Height(),
				SWP_SHOWWINDOW);
			m_lcTimeBasePlayContents.SetWindowPos(NULL,
				m_rcList2.left+expand,		m_rcList2.top,
				m_rcList2.Width()-expand,			m_rcList2.Height(),
				SWP_HIDEWINDOW);
		}else{
			m_lcCyclePlayContents.SetWindowPos(NULL,
				m_rcList.left+expand,		m_rcList.top,
				m_rcList.Width()-expand,	m_rcList.Height(),
				SWP_HIDEWINDOW);
			m_lcTimeBasePlayContents.SetWindowPos(NULL,
				m_rcList2.left+expand,		m_rcList2.top,
				m_rcList2.Width()-expand,			m_rcList2.Height(),
				SWP_SHOWWINDOW);
		}
		m_staticNameFilter.ShowWindow(SW_SHOW);
		m_editNameFilter.ShowWindow(SW_SHOW);
		m_btNameFilter.ShowWindow(SW_SHOW);

	}
	InvalidateItems();
	//m_btnHSizeToggle.ShowWindow(SW_SHOW);
}
void CPackageView::OnBnClickedBtNameFilter()
{
	CString strName;

	m_editNameFilter.GetWindowText(strName);
	this->m_lcContents.m_strNameFilter = strName;
	
	TraceLog(("OnBnClickedBtNameFilter = [%s]", (strName.IsEmpty() ? _T("") : strName)));

	m_lcContents.RefreshContents();
	m_lcContents.SortCur();
}

void CPackageView::ReorderListCtrl(CUTBListCtrlEx& lc)
{
	int cnt = lc.GetItemCount();
	for(int i=0; i<cnt; i++)
	{
		PLAYCONTENTS_INFO* info = (PLAYCONTENTS_INFO*)lc.GetItemData(i);

		info->nPlayOrder = i+1;
	}
}

#ifndef _UBCSTUDIO_PE_
void CPackageView::InputAdvertiserName()
{
	int counter = 0;
	CArray<CONTENTS_INFO*> arSInfo;
	POSITION pos = m_lcContents.GetFirstSelectedItemPosition();
	while(pos != NULL)
	{
		int nItem = m_lcContents.GetNextSelectedItem(pos);

		CONTENTS_INFO* info = (CONTENTS_INFO*)m_lcContents.GetItemData(nItem);
		if(info == NULL) continue;

		arSInfo.Add(info);
		counter++;
	}

	if(counter == 0){
		return ; 
	}

	InputAdvertiserDlg aDlg;
	if(aDlg.DoModal() != IDOK) return;

	CString name = "(";
	name += aDlg.m_advertiser;
	name += ")";
	for(int i = 0; i < arSInfo.GetCount(); i++)
	{
		CONTENTS_INFO* info = arSInfo.GetAt(i);

		info->strDescription = name;
		CString contentsName = name;
		contentsName += info->strContentsName;
		info->strContentsName = contentsName;
		info->bNameChanged = TRUE;

	}

	if(counter > 0)
	{
		RefreshPlayContentsList();
		RefreshRelation();
		m_lcContents.RefreshContents();
		m_lcContents.SortCur();
	}
}
#endif
