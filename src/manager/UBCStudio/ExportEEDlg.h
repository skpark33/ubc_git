#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"

struct SHostInfo4Studio;

// CExportEEDlg dialog
class CExportEEDlg : public CDialog
{
	DECLARE_DYNAMIC(CExportEEDlg)
public:
	enum { eCheck, eSiteID, eHostID, eDisplayNo, eAutoSche, eCurSche, eLastSche, eCreator, eMaxCol };

public:
	// In/Out param
	bool				m_bSaveAs;
	CString				m_szStartDT;
	CString				m_szEndDT;
	CList<SHostInfo4Studio*>	m_SelectInfo;

private:
	CString m_szMsgBoxTitle;

	CString				m_szColum[eMaxCol];
	CUTBListCtrlEx		m_lcHostList;
	CUTBListCtrlEx		m_lcSelHostList;

	CHoverButton		m_bnRefresh;
	CHoverButton		m_bnAdd;
	CHoverButton		m_bnDel;
	CComboBox			m_cbOnOff;
	CComboBox			m_cbOperation;

	void				InitHostList();
	void				RefreshHostList();

	void				InitSelHostList();
	void				RefreshSelHostList();

	void				UpdateRowList(CUTBListCtrlEx* pList, int nRow, SHostInfo4Studio* pHost);

	void				AddSelHost(int nHostIndex);
	void				DelSelHost(int nSelHostIndex);

public:
	CExportEEDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CExportEEDlg();

// Dialog Data
	enum { IDD = IDD_EE_EXPORT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonUploadServer();
	afx_msg void OnBnClickedButtonHostRefresh();
	afx_msg void OnBnClickedBnAdd();
	afx_msg void OnBnClickedBnDel();
	afx_msg void OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListSelHost(NMHDR *pNMHDR, LRESULT *pResult);
};
