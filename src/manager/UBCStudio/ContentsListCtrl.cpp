// ContentsListCtrl.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "UBCStudioDoc.h"
#include "ContentsDialog.h"
#include "AviReader.h"
#include "Enviroment.h"
#include "CheckImageSizeDlg.h"
#include "SubPlayContentsDialog.h"
#include "common/MD5Util.h"

// Modified by 정운형 2009-01-13 오후 7:25
// 변경내역 :  콘텐츠 추가 수정
#include "MainFrm.h"
#include "shlwapi.h"
#include "Dbghelp.h"
// Modified by 정운형 2009-01-13 오후 7:25
// 변경내역 :  콘텐츠 추가 수정

#include "RenameContentsFileDlg.h"
#include "ContentsOverwriteDlg.h"
#include "PackageView.h"
#include "ImgConvert\ImageConvert.h"

#include "common/PreventChar.h"
#include "ContentsListCtrl.h"
#include "ubccopcommon\ubccopcommon.h"
#include "util/UBCAutoEncoder/MediaInfoDLL.h"

#define SHARE_IMG_IDX	1

// CContentsListCtrl

IMPLEMENT_DYNAMIC(CContentsListCtrl, CCheckListCtrl)

CContentsListCtrl::CContentsListCtrl()
{
	m_OverwriteCheck = E_CONTENTS_COPY_MODALDLG;

	for(int i=0; i<CONTENTS_CNT ;i++)
	{
		m_bContentsFilter[i] = TRUE;
	}
	m_strNameFilter = "";
}

CContentsListCtrl::~CContentsListCtrl()
{
}

BEGIN_MESSAGE_MAP(CContentsListCtrl, CCheckListCtrl)
	ON_MESSAGE(WM_ADD_CONTENTS, OnAddContents)
END_MESSAGE_MAP()

// CContentsListCtrl 메시지 처리기입니다.
void CContentsListCtrl::Initialize()
{
	CBitmap bmp1;
	bmp1.LoadBitmap(IDB_CONTENTS_LIST32);

	CBitmap bmp2;
	bmp2.LoadBitmap(IDB_CONTENTS_LIST32_ERROR);

	CBitmap bmp3;
	bmp3.LoadBitmap(IDB_CONTENTS_LIST32_SERVER);

	CBitmap bmp4;
	bmp4.LoadBitmap(IDB_OVERLAP_SHARE32);

	if(m_ilContentsList.Create(32, 32, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilContentsList.Add(&bmp1, RGB(255,255,255)); //바탕의 회색이 마스크
		m_ilContentsList.Add(&bmp2, RGB(255,255,255)); //바탕의 회색이 마스크
		m_ilContentsList.Add(&bmp3, RGB(255,255,255)); //바탕의 회색이 마스크

		int nOverlayIndex = m_ilContentsList.GetImageCount();
		m_ilContentsList.Add(&bmp4, RGB(255,255,255));

		m_ilContentsList.SetOverlayImage(nOverlayIndex, SHARE_IMG_IDX);
	}

	SetImageList(&m_ilContentsList, LVSIL_SMALL);
	SetExtendedStyle(GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	//InsertColumn(0, "Type", 0, 36);
	//InsertColumn(0, "Type", 0, 70);
	InsertColumn(0, LoadStringById(IDS_CONTENTSLISTCTRL_TYPE), 0, 34);
	InsertColumn(1, LoadStringById(IDS_CONTENTSLISTCTRL_NAME), 0, 120);
	InsertColumn(2, LoadStringById(IDS_CONTENTSLISTCTRL_TIME), 0, 46);
	InsertColumn(3, LoadStringById(IDS_CONTENTSLISTCTRL_FILE), 0, 120);

	//skpark 2014.03.03
	InsertColumn(4, LoadStringById(IDS_CONTENTSLISTCTRL_SIZE), 0, 54);
	InsertColumn(5, LoadStringById(IDS_CONTENTSLISTCTRL_INCLUDE), 0, 100);
	InsertColumn(6, LoadStringById(IDS_CONTENTSLISTCTRL_EXCLUDE), 0, 100);
#if defined(DEBUG) | defined(_DEBUG)
	InsertColumn(4, _T("Local"), 0, 200);
	InsertColumn(5, _T("Server"), 0, 200);
	InsertColumn(6, _T("id"), 0, 200);
#endif

	CCheckListCtrl::Initialize(false);

	RefreshContents();

	//콘텐츠 type으로 정렬 시킨다
	Sort(0, false);
}

LRESULT CContentsListCtrl::OnAddContents(WPARAM wParam, LPARAM lParam)
{
	ClearContentsSelection();
	CStringArray* file_list = (CStringArray*)wParam;
	m_OverwriteCheck = E_CONTENTS_COPY_MODALDLG;

	for(int i=0; i < file_list->GetCount(); i++)
	{
		CString strFullpath = file_list->GetAt(i);

		if(_tcslen(strFullpath) <= MAX_PATH)
		{
			// 0000696: 임의 폴더를 통째로 업로드 하는 기능을 개발한다.
			// 폴더인 경우 폴더를 묶어서 처리하도록 한다.
			//DWORD dwRet = GetFileAttributes(strFullpath);
			//if(dwRet != 0xffffffff && (dwRet & FILE_ATTRIBUTE_DIRECTORY))
			//{
			//	CString strOutFileName;
			//	if(RunFolderArchiver(strFullpath, strOutFileName))
			//	{
			//		strFullpath = strOutFileName;
			//	}
			//	else
			//	{
			//		continue;
			//	}
			//}

			TCHAR drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
			_tsplitpath(strFullpath, drive, path, filename, ext);

			CString szTemp;
			szTemp.Format(_T("%s%s"), drive, path);
			CEnviroment::GetObject()->SetPathOpenFile(szTemp);

			CString fn;
			fn.Append(filename);
			fn.Append(ext);

			if('a' <= drive[0] && drive[0] <= 'z')
				drive[0] = drive[0] - 'a' + 'A';

			if( _tcslen(drive) == 2 &&
				drive[0] >= 'A'     &&
				drive[0] <= 'Z'     &&
				drive[1] == ':'     )
			{
				CONTENTS_TYPE contents_type = CONTENTS_NOT_DEFINE;
				int idx = 0;

				// 0000696: 임의 폴더를 통째로 업로드 하는 기능을 개발한다.
				// 폴더인 경우 폴더를 묶어서 처리하도록 한다.
				DWORD dwRet = GetFileAttributes(strFullpath);
				if(dwRet != 0xffffffff && (dwRet & FILE_ATTRIBUTE_DIRECTORY))
				{
					contents_type = CONTENTS_ETC;
					fn.Append(_T(".tar"));
				}

				while(ext_type[idx].extension[0] != 0)
				{
					// 창일향 : 콘텐츠 바스켓에 Drag & Drop 으로 밀어넣을 수 있는 콘텐츠는 오직 JPG 파일 뿐이다
					if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer )
					{
						if( _tcsicmp(_T(".jpg") , ext_type[idx].extension) != 0 &&
							_tcsicmp(_T(".jpeg"), ext_type[idx].extension) != 0 )
						{
							idx++;
							continue;
						}
					}

					if( _tcsicmp(ext, ext_type[idx].extension) == 0)
					{
						contents_type = ext_type[idx].type;
						break;
					}
					idx++;
				}

				if(contents_type != CONTENTS_NOT_DEFINE)
				{
					CONTENTS_INFO* info = new CONTENTS_INFO;

					info->strContentsName = filename;
					info->nContentsType = contents_type;
					info->strFilename = fn;
					//info->strId = filename;
					info->strLocalLocation = drive;
					info->strLocalLocation.Append(path);
					info->nSoundVolume = 100;
					info->bLocalFileExist = true;

					//(콘텐츠 파일 정보를 ini 파일에 기록할 때, '[', ']' 문자가 있으면 오류 발생 하므로
					//이름을 바꾸도록 한다.
					if(!ChangeContentsName(info))
					{
						delete info;
						break;
					}//if

					// 0000696: 임의 폴더를 통째로 업로드 하는 기능을 개발한다.
					// 폴더인 경우 폴더를 묶어서 처리하도록 한다.
					DWORD dwRet = GetFileAttributes(strFullpath);
					if(dwRet != 0xffffffff && (dwRet & FILE_ATTRIBUTE_DIRECTORY))
					{
						info->strContentsName.Format(_T("%s%s"), filename, ext);

						CString strOutFileName;
						if(RunFolderArchiver(strFullpath, info->strChngFileName))
						{
							//콘텐츠 이름이 변경된 경우 바꾸어 준다
							if(info->bNameChanged)
							{
								info->bNameChanged = false;
								info->strFilename = info->strChngFileName;
								info->strChngFileName = "";
							}

							CMainFrame* pclsFrame = (CMainFrame*)::AfxGetMainWnd();
							info->strLocalLocation = pclsFrame->GetSourceDirive() + UBC_CONTENTS_PATH;
						}
						else
						{
							continue;
						}
					}

					//콘텐츠 파일의 중복을 확인하여 복사를 한다
					int nOverWriteRet = OverWriteContentsFile(info);
					if(nOverWriteRet == eOWFAIL){
						delete info;
						break;
					}
					else if(nOverWriteRet == eOWSKIP){
						delete info;
						continue;
					}

					if(contents_type == CONTENTS_VIDEO)
					{
						// avi 정보
						SFile file;
						CAviReader	ar;
						if(ar.Open(strFullpath, -1, TRUE))
						{
							CString sAvi;
							const CAviReader::SAVIStreamHeader &vids = ar.GetVids();
							const CAviReader::SHdrl hdrl = ar.GetMainHdr();
							const WAVEFORMATEX		&wf = ar.GetWaveFormatex();

							file.width = hdrl.dwWidth;
							file.height = hdrl.dwHeight;
							//file.audio = WaveFormatTag2Str(wf.wFormatTag);
							file.video = FOURCC2Str(vids.fccHandler);

							file.time = MulDiv(vids.dwLength, vids.dwScale, vids.dwRate);
							/*
							if(wf.wFormatTag==0x2001)								// dts 처리..
							{
							CString s;
							s.Format("%s 파일은 소리가 DTS 로 인코딩 되었습니다. DTS 는 처리 못하셈.",sPathName);
							MessageBox(s, APP_NAME, MB_OK);
							}
							*/
						}
						else
						{
							GetVideoInfo(strFullpath, file);
						}

						info->nRunningTime = file.time;
					}
					else if(CONTENTS_IMAGE == contents_type){
						// 이미지는 크기를 체크한다.
						CSize sScreen;
						GetEnvPtr()->GetTempMaxSize((int&)sScreen.cx, (int&)sScreen.cy);
						CImageConvert imgCnv;

						CString szSFile = ((CMainFrame*)AfxGetMainWnd())->GetSourceDirive();
						szSFile += UBC_CONTENTS_PATH;
						szSFile += info->strFilename;

						CString szTFile = ((CMainFrame*)AfxGetMainWnd())->GetSourceDirive();
						szTFile += UBC_CONTENTS_PATH;
						szTFile += "__temp__";
						szTFile += info->strFilename;

						CSize sImage;
						ULONG ulSize;

						HCURSOR oldCursor = SetCursor(LoadCursor(NULL, IDC_WAIT)) ;
						imgCnv.GetFileInfo(szSFile, (int&)sImage.cx, (int&)sImage.cy, ulSize);
						SetCursor(oldCursor);

						if(sScreen.cx < sImage.cx || sScreen.cy < sImage.cy){
//							CString szMsg;
//							szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG001), sImage.cx, sImage.cy, sScreen.cx, sScreen.cy);
//							if(MessageBox(szMsg, LoadStringById(IDS_CONTENTSLISTCTRL_TTL001), MB_ICONQUESTION|MB_YESNO) == IDYES){
							CCheckImageSizeDlg dlg;
							if(dlg.DoModal() == IDOK){
								int cx = sScreen.cx;
								int cy = sScreen.cy;

								if(sScreen.cx < sScreen.cy){
									cy = (cx*sImage.cy)/sImage.cx;
								}else{
									cx = (cy*sImage.cx)/sImage.cy;
								}

								oldCursor = SetCursor(LoadCursor(NULL, IDC_WAIT)) ;
								imgCnv.ImgConvert(szSFile, szTFile, cx, cy, ulSize, false);
								DeleteFile(szSFile);
								MoveFile(szTFile, szSFile);
								DeleteFile(szTFile);
								SetCursor(oldCursor);
							}
						}
						info->nRunningTime = 15;
					}
					else if(CONTENTS_PPT == contents_type){
						info->nRunningTime = 86400;
					}
					else if(CONTENTS_ETC == contents_type){
						info->nRunningTime = 0;
					}
					else{
						info->nRunningTime = 15;
						//info->nRunningTime = 32000;
					}


					//skpark 2014.05.20 .txt 파일의 경우, random space를 더한다. 
					/*
					if(_tcsicmp(ext,".txt")==0){
						CString strSpace;
						RandomSpace(256,strSpace);
						CString fullpath = info->strLocalLocation+info->strFilename;
						TraceLog(("RandomSpace=[%s](%d) on %s", strSpace, strSpace.GetLength(), fullpath));
						AppendStringToFile(fullpath,strSpace);

					}
					*/
					// 변화된 파일의 크기를 다시 구한다.
					CEnviroment::GetFileSize(info->strLocalLocation+info->strFilename, info->nFilesize);
					

					// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
					if(info->nFilesize < 1000000)
					{
						CWaitMessageBox wait;
						char szMd5[16*2+1] = {0};
						CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)(info->strLocalLocation+info->strFilename), szMd5);
						info->strFileMD5 = szMd5;
					}



					//CLI에 새로운 콘텐츠가 추가되었음을 알린다
					//RunCLICmd(info);
					if(InsertContents(info, 0)){
						info->SetModified(); //skpark same_size_file_problem 2014.06.11
						TraceLog(("skpark %s file changed !!!" , info->strFilename));

					}
				}
				else
				{
					CString msg;
					msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), strFullpath);
					UbcMessageBox(msg, MB_ICONSTOP);
				}
			}
			else
			{
				CString msg;
				msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), strFullpath);
				UbcMessageBox(msg, MB_ICONSTOP);
			}//if
		}
		else
		{
			CString msg;
			msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG003), strFullpath);
			UbcMessageBox(msg, MB_ICONSTOP);
		}//if
	}//for

	delete file_list;
	m_OverwriteCheck = E_CONTENTS_COPY_MODALDLG;

	return 0;
}

void CContentsListCtrl::ClearContentsSelection()
{
	int count = GetItemCount();
	for(int i=0; i<count; i++)
		SetItemState(i, 0, LVIS_FOCUSED | LVIS_SELECTED);
}

bool CContentsListCtrl::RemoveSelectedContents()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	bool bSelectedExist = false;
	bool bPlayContentsExist = false;
	POSITION pos = GetFirstSelectedItemPosition();

	while(pos != NULL && bPlayContentsExist == false){
		bSelectedExist = true;

		int nItem = GetNextSelectedItem(pos);
		CONTENTS_INFO* info = (CONTENTS_INFO*)GetItemData(nItem);

		bPlayContentsExist |= m_pDocument->IsExistPlayContentsUsingContents(info->strId);
	}

	if(bSelectedExist){
		int ret_value = UbcMessageBox(LoadStringById(IDS_CONTENTSLISTCTRL_MSG004), MB_ICONWARNING | MB_YESNO);
		if(ret_value == IDNO)
			return false;
	}

	// 창일향 : dummy.jpg 는 지울수 없어야 한다
	if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer )
	{
		POSITION pos = GetFirstSelectedItemPosition();
		while(pos != NULL)
		{
			int nItem = GetNextSelectedItem(pos);
			CONTENTS_INFO* pInfo = (CONTENTS_INFO*)GetItemData(nItem);

			CString strFileName = pInfo->strFilename;
			strFileName.MakeLower();

			if(strFileName == _T("dummy.jpg"))
			{
				UbcMessageBox(LoadStringById(IDS_CONTENTSLISTCTRL_MSG010));
				return false;
			}
		}
	}

	if(bPlayContentsExist){
		int ret_value = UbcMessageBox(LoadStringById(IDS_CONTENTSLISTCTRL_MSG005), MB_ICONWARNING | MB_YESNO);
		if(ret_value == IDNO)
			return false;
	}

	CStringArray arBuf;
	pos = GetFirstSelectedItemPosition();

	while(pos != NULL){
		int nItem = GetNextSelectedItem(pos);
		CONTENTS_INFO* info = (CONTENTS_INFO*)GetItemData(nItem);
		arBuf.Add(info->strId);
	}

	for(int i = 0; i < arBuf.GetCount(); i++){
		m_pDocument->DeleteContents(arBuf.GetAt(i));
	}

	UpdateTotalSize();

	return true;

	//pos = GetFirstSelectedItemPosition();
	//if(pos != NULL){
	//	DeleteContents(pos);
	//	return true;
	//}
	//return false;
}

bool CContentsListCtrl::DeleteContents(POSITION pos)
{
	int nItem = GetNextSelectedItem(pos);

	if(pos != NULL)
		DeleteContents(pos);

	CONTENTS_INFO* info = (CONTENTS_INFO*)GetItemData(nItem);
	UpdateTotalSize();
/*
	// Modified by 정운형 2009-01-19 오후 8:26
	// 변경내역 :  콘텐츠 삭제기능 수정
	//ENC 폴더에서 해당 파일을 삭제 한다
	CMainFrame* pclsFrame = (CMainFrame*)::AfxGetMainWnd();
	CString strSourceDrive = pclsFrame->GetSourceDirive();
	CString strDeleteFile = strSourceDrive;
	strDeleteFile.Append(UBC_CONTENTS_PATH);
	strDeleteFile.Append(info->strFilename);
	DeleteFile(strDeleteFile);

	//CLI로 콘텐츠 파일이 삭제 되었음을 알린다
	RunCLICmd(info, false);
	// Modified by 정운형 2009-01-19 오후 8:26
	// 변경내역 :  콘텐츠 삭제기능 수정
*/
	if(m_pDocument->DeleteContents(info->strId)){
		return true;
	}
	return false;
}

bool CContentsListCtrl::ModifyContents(CArray<CONTENTS_INFO*>& arSInfo)
{
	bool ret_value = false;
//	CArray<CONTENTS_INFO*> arSInfo;
//	POSITION pos = GetFirstSelectedItemPosition();
//	while(pos != NULL)
//	{
//		int nItem = GetNextSelectedItem(pos);
//
//		CONTENTS_INFO* info = (CONTENTS_INFO*)GetItemData(nItem);
//		if(info == NULL)	continue;
//		arSInfo.Add(info);
//	}

	m_OverwriteCheck = E_CONTENTS_COPY_MODALDLG;

	for(int i = 0; i < arSInfo.GetCount(); i++)
	{
		CONTENTS_INFO* info = arSInfo.GetAt(i);
//		CString szOld = info->strFilename;
		CONTENTS_INFO infoTemp = *info;

//		if(!info->bLocalFileExist && !info->strFilename.IsEmpty() &&
//			GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && 
//		   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
//		{
//			CString szFile, szLocal, szRemote;
//			
//			szFile.Format(_T("%s"), info->strFilename);
//			szLocal.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
//
//			if(info->strServerLocation.IsEmpty())
//				szRemote.Format(_T("/contents/%s/"), GetEnvPtr()->m_strPackage);
//			else
//				szRemote.Format(_T("%s"), info->strServerLocation);
//
////			if(GetEnvPtr()->GetFile(szFile, szRemote, szLocal))
////				info->bLocalFileExist = true;
////			else
////				info->bServerFileExist = false;
//		}

		CContentsDialog dlg;
		dlg.SetDocument(m_pDocument);
		dlg.SetContentsInfo(*info);
		dlg.SetPreviewMode(info->bIsPublic);

		if(dlg.DoModal() == IDOK)
		{
			dlg.GetContentsInfo(*info);
			//Local system에 contents 파일이 실제로 존재하는지 검사한다
			//콘텐츠의 파일이 없는 경우는...text 이거나 ticker 인 경우
			//파일이 없어도 없다고 설정하면 안된다...ㅡㅡ;
			if(!info->strLocalLocation.IsEmpty() && !info->strFilename.IsEmpty() && PathFileExists(info->strLocalLocation + info->strFilename)){
				info->bLocalFileExist = true;
			}
			else if(info->strFilename.IsEmpty() && 
				(info->nContentsType==CONTENTS_SMS  || info->nContentsType==CONTENTS_TICKER ||
				 info->nContentsType==CONTENTS_TEXT || info->nContentsType==CONTENTS_TV     ||
				 info->nContentsType==CONTENTS_RSS  || info->nContentsType==CONTENTS_WIZARD || 
				 info->nContentsType==CONTENTS_WEBBROWSER)){
				info->bLocalFileExist = true;
			}
			else{
				info->bLocalFileExist = false;
			}

			//if(szOld != info->strFilename){
			if( infoTemp.strFilename != info->strFilename ||
				infoTemp.strLocalLocation != info->strLocalLocation )
			{
				//콘텐츠 파일의 중복을 확인하여 복사를 한다
				if(OverWriteContentsFile(info) == eOWFAIL)
				{
//					delete info;
					info->strFilename = infoTemp.strFilename;
					info->strLocalLocation = infoTemp.strLocalLocation;
					return false;
				}//if
			}

			//RunCLICmd(info);
			ClearContentsSelection();
			if(InsertContents(info, 2)){
				//info->bModified = true; //skpark same_size_file_problem 2014.06.11
				ret_value = true;
			}
		}//if
	}

	m_OverwriteCheck = E_CONTENTS_COPY_MODALDLG;
	return ret_value;
}

bool CContentsListCtrl::InsertContents(CONTENTS_INFO* info, int nMode, bool bChangeState)
{
	if(info == NULL)
	{
		return false;
	}//if

	if(info->nContentsType == CONTENTS_FILE) return false;

	int idx = -1;

	// 파워포인트 파일은 열었다가 닫았을때 사이즈가 변경되는 문제가 있어서 컨텐츠에 추가될때 읽기전용속성으로 바꾼다.
	if(info->nContentsType == CONTENTS_PPT)
	{
		CString szPath;
		szPath.Format("%s%s", info->strLocalLocation, info->strFilename);
		::SetFileAttributes(szPath, FILE_ATTRIBUTE_READONLY);
	}

	//skpark 2016.02.23 Auto-Encoding 
	if(nMode == 0 && GetEnvPtr()->m_bAutoEncoding && info->nContentsType == CONTENTS_VIDEO){
		TraceLog(("AutoEncoding Check Target=%s",info->strFilename));

		CString szPath;
		szPath.Format("%s%s", info->strLocalLocation, info->strFilename);

		CString video_format,video_codec,audio_formt,birateStr;
		unsigned long bitrate = this->GetBitRate(szPath,video_format,video_codec,audio_formt,birateStr);
		if(bitrate > 0){
			if(video_codec != "AVC") {
				TraceLog(("Encoding Case(%s)!!!",szPath));

				CString msg;
				msg.Format(LoadStringById(IDS_ENCODING_MSG001), szPath, video_codec);
				if(UbcMessageBox(msg, MB_ICONWARNING|MB_YESNO)==IDYES)
				{
					char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
					_splitpath(szPath, drive, path, filename, ext);
					CString target ;
					target.Format("%s\\%s\\%s.mp4", drive,path,filename);

					CString targetFile ;
					targetFile.Format("%s.mp4", filename);

					CString command ;
					command.Format("x264_mp4_aac.bat \"%s\" \"%s\" %s", szPath,target,birateStr);
					TraceLog(("Encoding : %s", command));		
					int errorlevel = system(command);
					if(errorlevel == 0){
						msg.Format(IDS_ENCODING_MSG002,targetFile);
						UbcMessageBox(msg, MB_ICONWARNING);
						info->strFilename = targetFile;
					}else{
						msg.Format(IDS_ENCODING_MSG003,errorlevel);
						UbcMessageBox(msg, MB_ICONERROR);
					}
				}
			}else{
				TraceLog(("source codec is alreay AVC(%s) : don't need to encoding",szPath));
			}
		}else{
			TraceLog(("Get Bitrate Failed(%s) : %s",szPath, birateStr));
		}
	}

	switch(nMode)
	{
	default:
		{
			delete info;
			return false;
		}
	case 0:	// Add Map & List
		{
			if(m_pDocument->AddContents(info) == false)
				return false;
		}
	case 1: // Only Add List
		{
			if(!m_bContentsFilter[info->nContentsType]) break;
			if(!m_strNameFilter.IsEmpty() &&
				info->strContentsName.Find(m_strNameFilter) < 0) break;

			idx = GetItemCount();
			if(info->bLocalFileExist)
			{
				InsertItem(idx, CONTENTS_TYPE_TO_STRING[info->nContentsType + 1], info->nContentsType+1);
			}
			else
			{
				if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty())
				{
					if(info->bServerFileExist)
						InsertItem(idx, CONTENTS_TYPE_TO_STRING[info->nContentsType + 1], (info->nContentsType+1) + NUM_ENV_SERVER);
					else
						InsertItem(idx, CONTENTS_TYPE_TO_STRING[info->nContentsType + 1], (info->nContentsType+1) + NUM_ENV_INEXISTENT);
				}
				else
				{
					InsertItem(idx, CONTENTS_TYPE_TO_STRING[info->nContentsType + 1], (info->nContentsType+1) + NUM_ENV_INEXISTENT);
				}
			}
			break;
		}
	case 2: // Modify
		{
			for(int i=0; i<GetItemCount(); i++)
			{
				CONTENTS_INFO* inf = (CONTENTS_INFO*)GetItemData(i);
				if(inf == info)
				{
					idx = i;
					break;
				}
			}
			if(idx < 0)
				return false;

			LVITEM item;
			item.mask = LVIF_IMAGE;
			item.iItem = idx;
			item.iSubItem = 0;
			if(info->bLocalFileExist)
			{
				item.iImage = info->nContentsType + 1;
			}
			else
			{
				if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE && !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty()){
					if(info->bServerFileExist)
						item.iImage = (info->nContentsType+1) + NUM_ENV_SERVER;
					else
						item.iImage = (info->nContentsType+1) + NUM_ENV_INEXISTENT;
				}else{
					item.iImage = (info->nContentsType+1) + NUM_ENV_INEXISTENT;
				}
			}//if
			SetItem(&item);
			break;
		}
	}//switch

	UpdateTotalSize();

	SetItemText(idx, 1, info->strContentsName);
	SetItemText(idx, 3, info->strFilename);

	//skpark 2014.03.03
	SetItemText(idx, 4, ::ToString(info->nFilesize/1024) + "KB");
	SetItemText(idx, 5, info->strIncludeCategory);
	SetItemText(idx, 6, info->strExcludeCategory);


#if defined(DEBUG) | defined(_DEBUG)
	SetItemText(idx, 4, info->strLocalLocation);
	SetItemText(idx, 5, info->strServerLocation);
	SetItemText(idx, 6, info->strId);
#endif

	if(CONTENTS_PPT == info->nContentsType){
		SetItemText(idx, 2, "");
	}else{
		CString tm;
		tm.Format("%02ld:%02ld", info->nRunningTime/60, info->nRunningTime % 60);
		SetItemText(idx, 2, tm);
	}

	if(info->bIsPublic)
	{
		SetItemState(idx, LVIS_OVERLAYMASK, INDEXTOOVERLAYMASK(SHARE_IMG_IDX));
	}

	SetItemData(idx, (DWORD)info);
	if(bChangeState)
	{
		SetItemState(idx, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
		EnsureVisible(idx, FALSE);
	}

	return true;
}

bool CContentsListCtrl::NewContents()
{
	TraceLog(("NewContents()"));

	CContentsDialog dlg;
	dlg.SetDocument(m_pDocument);
	if(dlg.DoModal() != IDOK) return false;

	CONTENTS_INFO* info = new CONTENTS_INFO;
	dlg.GetContentsInfo(*info);
	info->nSoundVolume = 100;
	info->bLocalFileExist = true;

	if(!info->strFilename.IsEmpty())
	{
		//(콘텐츠 파일 정보를 ini 파일에 기록할 때, '[', ']' 문자가 있으면 오류 발생 하므로
		//이름을 바꾸도록 한다.
		if(!ChangeContentsName(info))
		{
			delete info;
			return false;
		}//if

		//콘텐츠 파일의 중복을 확인하여 복사를 한다
		m_OverwriteCheck = E_CONTENTS_COPY_MODALDLG;
		if(OverWriteContentsFile(info) != eOWTRUE)
		{
			delete info;
			return false;
		}//if
		m_OverwriteCheck = E_CONTENTS_COPY_MODALDLG;
	}

	//RunCLICmd(info);
	ClearContentsSelection();
	if(InsertContents(info, 0)){
		info->SetModified(); //skpark same_size_file_problem 2014.06.11
		TraceLog(("skpark %s file changed !!!" , info->strFilename));
	}

	return true;
}

bool CContentsListCtrl::RefreshContents()
{
	DeleteAllItems();

	CONTENTS_INFO_MAP* contents_map = m_pDocument->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(pContentsInfo)
		{
			InsertContents(pContentsInfo, 1);
		}//if
	}

	ClearContentsSelection();

	return true;
}

BOOL CContentsListCtrl::GetVideoInfo(CString sPathVideo, SFile& file)
{
    USES_CONVERSION;

    WCHAR wFile[MAX_PATH];
    HRESULT hr = 0;
	BOOL ret = FALSE;

	IGraphBuilder	*pGB = NULL;
	IBasicVideo		*pBV = NULL;
	IMediaSeeking	*pMS = NULL;

    // Convert filename to wide character string
    wcscpy(wFile, T2W(sPathVideo));

	// Get the interface for DirectShow's GraphBuilder
	JIF(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void **)&pGB));

    if(SUCCEEDED(hr))
    {
        // Have the graph builder construct its the appropriate graph automatically
        JIF(pGB->RenderFile(wFile, NULL));
        JIF(pGB->QueryInterface(IID_IBasicVideo, (void **)&pBV));
		JIF(pGB->QueryInterface(IID_IMediaSeeking, (void **)&pMS));

		LONG lHeight, lWidth;
		HRESULT hr = S_OK;
		if (pBV)
		{
			hr = pBV->GetVideoSize(&lWidth, &lHeight);
			file.width = lWidth;
			file.height = lHeight;
			ret = TRUE;
		}

		if(pMS)
		{
			LONGLONG time;
			pMS->GetDuration(&time);
			file.time = (int)(time/1000/10000);
		}
	}

	SAFE_RELEASE(pBV);
	SAFE_RELEASE(pMS);
	SAFE_RELEASE(pGB);
	
	return ret;
}

bool CContentsListCtrl::ModifyContents(CONTENTS_INFO* info)
{
	if(info != NULL)
	{
		CContentsDialog dlg;
		dlg.SetDocument(m_pDocument);
		dlg.SetContentsInfo(*info);
		dlg.SetPreviewMode(info->bIsPublic);
		if(dlg.DoModal() == IDOK)
		{
			// Modified by 정운형 2008-12-26 오후 1:56
			// 변경내역 :  캡션기능 추가 - 버그수정
			dlg.GetContentsInfo(*info);
			// Modified by 정운형 2008-12-26 오후 1:56
			// 변경내역 :  캡션기능 추가 - 버그수정
			if(InsertContents(info, 2, false)){
				//info->bModified = true; //skpark same_size_file_problem 2014.06.11
				return true;
			}else{
				return false;
			}
		}
	}
	return false;
}

// Modified by 정운형 2009-01-13 오후 7:25
// 변경내역 :  콘텐츠 추가 수정
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// CLI 연동 명령어를 파일로 작성하여 CLI 명령어를 전달한다 \n
/// @param (CONTENTS_INFO*) pInfo : (in) 생성된 coontens 내용
/// @param (bool) bCreate : (in) 콘텐츠가 생성되었는지 삭제 되었는지 여부
/////////////////////////////////////////////////////////////////////////////////
void CContentsListCtrl::RunCLICmd(CONTENTS_INFO* pInfo, bool bCreate)
{
#ifndef HP_DEMO

	return;

#else

	TCHAR szPath[MAX_PATH];
	::ZeroMemory(szPath, MAX_PATH);
	::GetModuleFileName(NULL, szPath, MAX_PATH);
	int length = _tcslen(szPath) - 1;
	while( (length > 0) && (szPath[length] != _T('\\')) )
		szPath[length--] = 0;

	CString strCliPath;
	strCliPath = szPath;
	strCliPath.Append("config\\cli\\");
	MakeSureDirectoryPathExists(strCliPath);

	FILE* pFile;
	CString strPath = szPath;
	strPath.Append("config\\cli\\");
	CString strFileName;
	DWORD dwTick = GetTickCount();
	if(bCreate)
	{
		strFileName.Format("contents_create_%s_%3d.cli", pInfo->strId, dwTick);
	}
	else
	{
		strFileName.Format("contents_delete_%s_%3d.cli", pInfo->strId, dwTick);
	}//if

	strPath.Append(strFileName);
	pFile = fopen(strPath.GetBuffer(strPath.GetLength()), "w");
	if(pFile == NULL)
	{
		CString strError;
		strError.Format("CLI file create fail : %s\r\n", strPath);
		TRACE(strError);
		return;
	}//if

	CString strCmd;
	if(bCreate)
	{
		strCmd.Format("create CM=0/Contents=%s attributes=[\
contentsType=%d, \
contentsName=\"%s\", \
filename=\"%s\", \
runningTime=%d, \
location=/Contents/ENC/, \
isRaw=false, \
contentsState=3, \
verifier=skpark]",
				pInfo->strId,
				pInfo->nContentsType,
				pInfo->strContentsName,
				pInfo->strFilename,
				pInfo->nRunningTime
				);
	}
	else
	{
		strCmd.Format("remove RM=0/ADRequest=%s", pInfo->strId);
	}//if

	strCmd.Append("\n");

	fwrite(strCmd.GetBuffer(strCmd.GetLength()), sizeof(char), strCmd.GetLength(), pFile); 
	fclose(pFile);
#ifndef _DEBUG
	CString strExcute;
	strExcute = szPath;
	strExcute.Append("clInvoker.exe");
	strExcute.Append(" -ORBInitRef NameService=corbaloc:iiop:211.232.57.202:14009/NameService \
-ORBInitRef InterfaceRepository=corbaloc:iiop:211.232.57.202:14010/InterfaceRepository \
+noES +noDS +socketproxy +ignore_env +noxlog +project utv1 +post +run ");
	strExcute.Append(strFileName);
	int nRet = WinExec(strExcute, SW_HIDE);
	if(nRet < 31)
	{
		TRACE("Running CLI fail\r\n");
	}//if
#endif

#endif
}
// Modified by 정운형 2009-01-13 오후 7:25
// 변경내역 :  콘텐츠 추가 수정



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 콘텐츠 파일에 '['. ']' 문자가 있는경우에 이를 바꾸어 준다 \n
/// @param (CONTENTS_INFO*) pInfo : (in/out) 콘텐츠 정보를 갖는 객체
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CContentsListCtrl::ChangeContentsName(CONTENTS_INFO* pInfo)
{
	CMainFrame* pclsFrame = (CMainFrame*)::AfxGetMainWnd();
	CString strSourceDrive = pclsFrame->GetSourceDirive();
	CString strCompareExist = strSourceDrive;
	strCompareExist.Append(UBC_CONTENTS_PATH);

	//if( pInfo->strFilename.Find("[") >= 0 ||
	//   pInfo->strFilename.Find("]") >= 0 ||
	//    pInfo->strFilename.Find(",") >= 0 )
	if(CPreventChar::GetInstance()->HavePreventChar(pInfo->strFilename))
	{
		CRenameContentsFileDlg dlgRename;
		dlgRename.m_strOldFileName = pInfo->strFilename;
		dlgRename.m_strENCPath = strCompareExist;
		dlgRename.m_strMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG006), CPreventChar::GetInstance()->GetPreventChar());

		if(dlgRename.DoModal() != IDOK) return false;

		//콘텐츠 이름을 바꾸어 준다
		pInfo->bNameChanged = true;
		pInfo->strChngFileName = dlgRename.m_strNewName;
	}

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 콘텐츠 파일의 덮어쓰는지 여부를 정하여 복사한다 \n
/// @param (CONTENTS_INFO*) pInfo : (in/out) 콘텐츠 정보를 갖는 객체
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
int CContentsListCtrl::OverWriteContentsFile(CONTENTS_INFO* pInfo)
{
	CString strSourceFile, strTargetFile;
	CMainFrame* pclsFrame = (CMainFrame*)::AfxGetMainWnd();
	CString strSourceDrive = pclsFrame->GetSourceDirive();
	CString strCompareExist = strSourceDrive;
	strCompareExist.Append(UBC_CONTENTS_PATH);

	// Location 이 없으면 파일이 아님..
	if(pInfo->strLocalLocation.IsEmpty())
		return eOWTRUE;

	// Disk space
	ULONGLONG lFreeBytesAvailable, lTotalBytes, lTotalFree;
	GetDiskFreeSpaceEx(strSourceDrive,(PULARGE_INTEGER)&lFreeBytesAvailable,  (PULARGE_INTEGER)&lTotalBytes, (PULARGE_INTEGER)&lTotalFree);

	// file size 계산
	CFileFind ff;
	CString szPath;
	szPath.Format("%s%s", pInfo->strLocalLocation, pInfo->strFilename);
	if(ff.FindFile(szPath)){
		ff.FindNextFile();
		pInfo->nFilesize = ff.GetLength();
	}
	ff.Close();

	// 최소 disk space 를 8MByte 이상 유지하도록 한다.
	if(lFreeBytesAvailable < (pInfo->nFilesize+8000000)){
		UbcMessageBox(LoadStringById(IDS_CONTENTSLISTCTRL_MSG009), MB_ICONERROR);
		return eOWFAIL;
	}

	//ENC 폴더에 같은 이름의 파일이 있는지 알아 본다
	if(strCompareExist != pInfo->strLocalLocation)
	{
		if(pInfo->bNameChanged)
		{
			strCompareExist.Append(pInfo->strChngFileName);
		}
		else
		{
			strCompareExist.Append(pInfo->strFilename);
		}//if

		if(::PathFileExists(strCompareExist))
		{
			int OverwriteCheck = E_CONTENTS_COPY_MODALDLG;
			CString szNewContentsName;
			CONTENTS_INFO ExistInfo = *(pInfo);
			ExistInfo.strLocalLocation = strSourceDrive + UBC_CONTENTS_PATH;

			if(pInfo->bNameChanged){
				ExistInfo.strFilename = pInfo->strChngFileName;
			}

			//ENC 폴더에 같은 이름의 파일이 있다면 확인 창을 보여준다
			if(E_CONTENTS_COPY_MODALDLG == m_OverwriteCheck){
				CContentsOverwriteDlg dlgOverwrite;
				dlgOverwrite.SetContentsInfo(pInfo, &ExistInfo);

				if(dlgOverwrite.DoModal() != IDOK){
					return eOWFAIL;
				}

				OverwriteCheck = dlgOverwrite.m_sContentsCopy;
				szNewContentsName = dlgOverwrite.m_strNewContentsName;

				if(dlgOverwrite.m_bAllOfFiles){
					m_OverwriteCheck = dlgOverwrite.m_sContentsCopy;
				}
			}else{
				OverwriteCheck = m_OverwriteCheck;
			}

			CWaitMessageBox wait;

			//파일을 overwrite할것인지 여부에 따라서
			//복사하는 방법을 결정한다.
			switch(OverwriteCheck)
			{
			case E_CONTENTS_COPY_USE_EXIST:
				{
					pInfo->strLocalLocation = ExistInfo.strLocalLocation;
					if(pInfo->bNameChanged)
					{
						pInfo->strFilename = pInfo->strChngFileName;
					}//if

					// 기존 List에 존재하는 콘텐츠라면 콘텐츠 생성을 하지 않는다
					if( CONTENTS_SMS       !=pInfo->nContentsType && CONTENTS_TICKER!=pInfo->nContentsType && 
						CONTENTS_WEBBROWSER!=pInfo->nContentsType && CONTENTS_RSS   !=pInfo->nContentsType &&
						CONTENTS_WIZARD    !=pInfo->nContentsType )
					{
						for(int i = 0; i < GetItemCount(); i++){
							CONTENTS_INFO* item_info = (CONTENTS_INFO*)GetItemData(i);
							if(item_info->nContentsType == pInfo->nContentsType && pInfo->strFilename == item_info->strFilename){
									return eOWSKIP;
							}
						}
					}
				}
				break;
			case E_CONTENTS_COPY_OVERWRITE:
				{
					strSourceFile = pInfo->strLocalLocation + pInfo->strFilename;
					strTargetFile = ExistInfo.strLocalLocation + ExistInfo.strFilename;

					if(!ShellCopyFile(strSourceFile, strTargetFile, FALSE))
					{
						CString msg;
						msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG007), strSourceFile);
						UbcMessageBox(msg, MB_ICONSTOP);
						return eOWFAIL;
					}//if

					pInfo->strLocalLocation = ExistInfo.strLocalLocation;
					pInfo->strFilename = ExistInfo.strFilename;

					// 기존 List에 존재하는 콘텐츠라면 콘텐츠 생성을 하지 않는다
					if( CONTENTS_SMS       !=pInfo->nContentsType && CONTENTS_TICKER!=pInfo->nContentsType && 
						CONTENTS_WEBBROWSER!=pInfo->nContentsType && CONTENTS_RSS   !=pInfo->nContentsType &&
						CONTENTS_WIZARD    !=pInfo->nContentsType )
					{
						for(int i = 0; i < GetItemCount(); i++){
							CONTENTS_INFO* item_info = (CONTENTS_INFO*)GetItemData(i);
							if(item_info->nContentsType == pInfo->nContentsType && pInfo->strFilename == item_info->strFilename){
									return eOWSKIP;
							}
						}
					}
				}
				break;
			case E_CONTENTS_COPY_RENAME:
				{
					m_OverwriteCheck = E_CONTENTS_COPY_MODALDLG;
					strSourceFile = pInfo->strLocalLocation + pInfo->strFilename;
					strTargetFile = ExistInfo.strLocalLocation + szNewContentsName;

					if(!ShellCopyFile(strSourceFile, strTargetFile, TRUE))
					{
						CString msg;
						msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG007), strSourceFile);
						UbcMessageBox(msg, MB_ICONSTOP);
						return eOWFAIL;
					}//if

					pInfo->strLocalLocation = ExistInfo.strLocalLocation;
					pInfo->strFilename = szNewContentsName;
				}
				break;
			default:
				{
					UbcMessageBox(LoadStringById(IDS_CONTENTSLISTCTRL_MSG008), MB_ICONERROR);
					return eOWFAIL;
				}
			}//switch

			
		}
		else
		{
			//중복된 파일이 없는 경우
			CWaitMessageBox wait;

			strSourceFile = pInfo->strLocalLocation + pInfo->strFilename;

			CString strTargetFileName;
			if(pInfo->bNameChanged)
			{
				strTargetFileName = pInfo->strChngFileName;
			}
			else
			{
				strTargetFileName = pInfo->strFilename;
			}//if
			strTargetFile = strSourceDrive + UBC_CONTENTS_PATH + strTargetFileName;

			if(!ShellCopyFile(strSourceFile, strTargetFile, TRUE))
			{
				CString msg;
				msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG007), strSourceFile);
				UbcMessageBox(msg, MB_ICONSTOP);
				return eOWFAIL;
			}//if

			pInfo->strLocalLocation = strSourceDrive + UBC_CONTENTS_PATH;
			pInfo->strFilename = strTargetFileName;

			
		}//if
	}
	else
	{
		//콘텐츠 폴더에서 파일을 추가하는 경우
		if(pInfo->bNameChanged)
		{
			CWaitMessageBox wait;

			strSourceFile = pInfo->strLocalLocation + pInfo->strFilename;
			strTargetFile = pInfo->strLocalLocation + pInfo->strChngFileName;

			if(!ShellCopyFile(strSourceFile, strTargetFile, TRUE))
			{
				CString msg;
				msg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG007), strSourceFile);
				UbcMessageBox(msg, MB_ICONSTOP);
				return eOWFAIL;
			}//if

			pInfo->strFilename = pInfo->strChngFileName;

			
		}//if
	}//if

	return eOWTRUE;
}

BOOL CContentsListCtrl::SortCur()
{
	return Sort(m_nSortCol, m_bAscend);
}

void CContentsListCtrl::UpdateTotalSize()
{
	TraceLog(("UpdateTotalSize()"));

	if(GetParent() && GetParent()->IsKindOf(RUNTIME_CLASS(CPackageView)))
	{
		CPackageView* pView = (CPackageView*)GetParent();
		pView->UpdateTotalSize();
	}
}

unsigned long CContentsListCtrl::GetBitRate(LPCTSTR fullpath, CString& video_format, CString& video_codec, CString& audio_format, CString& errStr)
{
	MediaInfoDLL::MediaInfo MI;
	size_t ret = MI.Open(fullpath);
	if(ret==0){
		errStr = fullpath;
		errStr += _T(" open failed");
		return 0;
	}
	errStr = MI.Get(MediaInfoDLL::Stream_General, 0, _T("OverallBitRate"), MediaInfoDLL::Info_Text, MediaInfoDLL::Info_Name).c_str();
	unsigned long bitRate = strtoul(errStr,NULL,10);
	//std::wcout  << bitRate << std::endl;
	unsigned long retval = unsigned long(bitRate/1024);

	video_format = MI.Get(MediaInfoDLL::Stream_General, 0, _T("Format"), MediaInfoDLL::Info_Text, MediaInfoDLL::Info_Name).c_str();
	//std::wcout << _T("Video Format = [") << video_format.c_str() << _T("]") << std::endl;

	video_codec = MI.Get(MediaInfoDLL::Stream_General, 0, _T("Video_Format_List"), MediaInfoDLL::Info_Text, MediaInfoDLL::Info_Name).c_str();
	//video_codec = MI.Get(Stream_General, 0, _T("CodecID/Hint"), Info_Text, Info_Name).c_str();
	//std::wcout << _T("Video Format = [") << video_format.c_str() << _T("]") << std::endl;

	audio_format = MI.Get(MediaInfoDLL::Stream_General, 0, _T("Audio_Format_List"), MediaInfoDLL::Info_Text, MediaInfoDLL::Info_Name).c_str();
	//std::wcout << _T("Audio Format = [") << audio_format.c_str() << _T("]") <<std::endl;

	MI.Close();
	return retval;
}
