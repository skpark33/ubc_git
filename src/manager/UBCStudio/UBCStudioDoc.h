// UBCStudioDoc.h : interface of the CUBCStudioDoc class
//


#pragma once


class CUBCStudioDoc : public CDocument
{
public:
	CUBCStudioDoc();
protected: // create from serialization only
	DECLARE_DYNCREATE(CUBCStudioDoc)

// Attributes
public:

// Operations
public:
	
// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CUBCStudioDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:

	DECLARE_MESSAGE_MAP()

	CString		m_strSelectTemplateId;
	CString		m_strSelectFrameId;
	CString		m_strSourceDrive;
	CString		m_strTargetDrive;
	CString		m_strHostName;
	CString		m_strSite;
	int			DeleteFolder(LPCTSTR lpszTargetPath);	

public:
	static		int		s_schedule_count;
	bool		m_bNew;					///<새로 package 만들기를 하였는지 여부
	bool		m_bOpened;				///<Package을 import(open)한 상태인지 여부

	bool	Load(CString strDrive, CString strHost);		///<편성관리 설정파일을 읽어 편성관리를 초기화 한다
	bool	SaveContents();
	bool	SaveConfig();
//	int		DeleteExportFolder(void);
	void	ClosePackage(void);							///<Load한 package을 메모리에서 모두 지우고 초기화 한다.

	CONTENTS_INFO_MAP*		GetContentsMap();
	TEMPLATE_LINK_LIST*		GetAllTemplateList();
	TEMPLATE_LINK_LIST*		GetPlayTemplateList();

	void			SetSelectTemplateId(LPCTSTR lpszId);
	void			SetSelectFrameId(LPCTSTR lpszId);
	LPCTSTR			GetSelectTemplateId();
	LPCTSTR			GetSelectFrameId();

	CONTENTS_INFO*	GetContents(LPCTSTR lpszContentsId);
	PLAYCONTENTS_INFO*	GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId, LPCTSTR lpszPlayContentsId);
	PLAYCONTENTS_INFO*	GetPlayContents(LPCTSTR lpszTemplateId, LPCTSTR lpszPlayContentsId);
	TEMPLATE_LINK*	GetTemplateLink(LPCTSTR lpszTemplateId);

	TEMPLATE_LINK*	GetSelectTemplateLink();
	FRAME_LINK*		GetSelectFrameLink();

	bool			MoveFordwardPlayTemplate(int nIndex);
	bool			MoveBackwardPlayTemplate(int nIndex);

	bool			AddContents(CONTENTS_INFO* pContentsInfo);
	bool			AddTemplate(TEMPLATE_INFO* pTemplateInfo);
	int 			AddPlayTemplate(LPCTSTR lpszTemplateId);

	TEMPLATE_INFO*	CreateNewTemplate();
	FRAME_INFO*		CreateNewFrame(LPCTSTR lpszTemplateId);
	TEMPLATE_INFO*	CreateNewTemplateFromServer(TEMPLATE_LINK* pPublicTemplateLink);

	// Modified by 정운형 2009-02-19 오후 8:31
	// 변경내역 :  template 추가 버그 수정
	LPCTSTR			CreateCopyTemplate(LPCTSTR lpszSrcTemplateID);	///<기존의 template size, color등 기본정보만 복사하여 새로운 template를 생성한다. 
	// Modified by 정운형 2009-02-19 오후 8:31
	// 변경내역 :  template 추가 버그 수정

	CString         CreateCopyPlayTemplate(CString strSrcTemplateID);

	bool			DeleteContents(LPCTSTR lpszContentsId);
	bool			DeleteTemplate(LPCTSTR lpszTemplateId);
	bool			DeleteFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId);
	bool			DeletePlayTemplate(int nIdnex);

	bool			IsExistContentsInTemplate(LPCTSTR lpszTemplateId);
	bool			IsExistContentsInFrame(LPCTSTR lpszTemplateId, LPCTSTR lpszFrameId);
	bool			IsExistPlayTemplateList(LPCTSTR lpszTemplateId);
	bool			IsExistPlayContentsUsingContents(LPCTSTR lpszContents);
	int 			GetContentsUsingCount(LPCTSTR lpszContentsId);
	bool 			CheckOverlayModeFrame(CString strTemplateId, CString strOverlayModeFrameId);
	bool			CheckContentsCanBeAddedToFrame(CString strTemplateId, CString strFrameId, CString strContentsId);
	bool			CheckPlayContentsRelation();

	bool			GetHostInfo(LPCTSTR lpszPath, CString strDrive, CPtrArray& aryHostInfo);	///<지정된 경로의 호스트 정보를 구한다
	CString			GetHostName(void);														///<설정된 호스트의 이름을 반환한다.
	bool			RunPreview(CString strTemplateID, bool bSaveConfig);					///<Run preview browser
	bool			PreviewSaveConfig(CString strTemplateID =_T(""));							///<Preview를 위하여 현재 open 호스트의 config 파일을 저장한다
	CString			GetSourceDirive(void);													///<설정된 source 드라이브 문자열을 반환한다.
	void			SetSourceDirive(CString strDrive);										///<Source 드라이브 문자열을 설정한다
	CString			GetTargetDirive(void);													///<설정된 target 드라이브 문자열을 반환한다.
	void			SetTargetDirive(CString strDrive);										///<Target 드라이브 문자열을 설정한다
	CString			GetDescription(void);													///<열려있는 host의 설명을 반환한다.
	void			SetDescription(CString strDesc);										///<열려있는 host의 설명을 설정한다.

	CTime			GetNewPlayContentsStartTime(int nPlayContentsType);
	bool			CheckTimeBasePlayContentsOverlap(int nPlayContentsType, bool bMsg = true, bool bCheckIsNewOnly = true);
	CTime			TimeBasePlayContentsToTime(int nDate, CString strTime);
	
	int				TimeBasePlayContentsToTime(int nDate, int offset){	return (int(nDate/86400)*86400+offset);}

	bool			CheckMaxPlayTemplateCount(bool bMsg=true);
	bool			CheckMaxContentsAtSave(); //skpark 2013.01.23
	bool			CheckMaxContentsCount(int nAddSize, bool bMsg=true);
	bool			CheckTotalContentsSize(ULONGLONG lAddSize, bool bMsg=true);
	ULONGLONG		GetTotalContentsSize();
	bool			DownloadAllContents(bool bMsg);
	bool			CheckAdvertiserName(CString& no_ad_name_contents); //skpark 2016.09.01

	bool			InputErrorCheck();
	//virtual BOOL	IsModified(); // Doc,View 구조가 요상하니 사용하지 말것.
	bool			IsModify();

	// 0001128: Studio 임시 저장 및 복구
	void			LoadFromTemporaryFile();
	void			SaveToTemporaryFile();

	BOOL			PathFileIsSame(LPCSTR pszPath, ULONGLONG nSize, CString& modifiedTime);  //skpark same_size_file_problem

	BOOL			ChangePackageProperty();
};

