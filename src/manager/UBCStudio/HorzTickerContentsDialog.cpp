// HorzTickerContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "HorzTickerContentsDialog.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "common/CharType.h"
#include "common/MD5Util.h"

// CHorzTickerContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHorzTickerContentsDialog, CSubContentsDialog)

CHorzTickerContentsDialog::CHorzTickerContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CHorzTickerContentsDialog::IDD, pParent)
	, m_wndTicker (NULL, 0, false)
	, m_reposControl (this)
	, m_strLocation("")
	, m_bPreviewMode(false)
	, m_bPermanent(FALSE)
	, m_cbxContentsFont(IDB_TTF_BMP)
{
}

CHorzTickerContentsDialog::~CHorzTickerContentsDialog()
{
}

void CHorzTickerContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_CONTENTS_FILE, m_btnBrowserContentsFile);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_WIDTH, m_staticContentsWidth);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_HEIGHT, m_staticContentsHeight);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_FILE_SIZE, m_staticContentsFileSize);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FONT_SIZE, m_editContentsFontSize);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_FONT, m_cbxContentsFont);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_TEXT_COLOR, m_cbxContentsTextColor);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_BG_COLOR, m_cbxContentsBGColor);
	DDX_Control(pDX, IDC_SLIDER_CONTENTS_PLAY_SPEED, m_sliderContentsPlaySpeed);
//	DDX_Control(pDX, IDC_LIST_CONTENTS_COMMENT, m_listctrlContentsComment);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_ADD, m_btnContentsCommentAdd);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_DELETE, m_btnContentsCommentDelete);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_UP, m_btnContentsCommentUp);
	DDX_Control(pDX, IDC_BUTTON_CONTENTS_COMMENT_DOWN, m_btnContentsCommentDown);
	DDX_Control(pDX, IDC_BUTTON_DELFILE, m_btnDelFile);
	DDX_Control(pDX, IDC_BUTTON_ZOOM_IN, m_btnZoomIn);
	DDX_Control(pDX, IDC_BUTTON_ZOOM_OUT, m_btnZoomOut);
	DDX_Control(pDX, IDC_BUTTON_ORIGINAL, m_btnOriginal);

	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_groupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_PLAY, m_btnPreviewPlay);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_STOP, m_btnPreviewStop);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);
}


BEGIN_MESSAGE_MAP(CHorzTickerContentsDialog, CSubContentsDialog)
	ON_WM_HSCROLL()
	ON_WM_SIZE()
//	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST_CONTENTS_COMMENT, &CHorzTickerContentsDialog::OnLvnEndlabeleditListContentsComment)
//	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_CONTENTS_COMMENT, &CHorzTickerContentsDialog::OnLvnKeydownListContentsComment)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_CONTENTS_FILE, &CHorzTickerContentsDialog::OnBnClickedButtonBrowserContentsFile)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_UP, &CHorzTickerContentsDialog::OnBnClickedButtonContentsCommentUp)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_DOWN, &CHorzTickerContentsDialog::OnBnClickedButtonContentsCommentDown)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_PLAY, &CHorzTickerContentsDialog::OnBnClickedButtonPreviewPlay)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_STOP, &CHorzTickerContentsDialog::OnBnClickedButtonPreviewStop)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_ADD, &CHorzTickerContentsDialog::OnBnClickedButtonContentsCommentAdd)
	ON_BN_CLICKED(IDC_BUTTON_CONTENTS_COMMENT_DELETE, &CHorzTickerContentsDialog::OnBnClickedButtonContentsCommentDelete)
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, &CHorzTickerContentsDialog::OnBnClickedPermanentCheck)
	ON_EN_CHANGE(IDC_EDIT_CONTENTS_COMMENT, &CHorzTickerContentsDialog::OnEnChangeEditContentsComment)
	ON_BN_CLICKED(IDC_BUTTON_DELFILE, OnBnClickedDeletefile)
	ON_BN_CLICKED(IDC_BUTTON_ZOOM_IN, &CHorzTickerContentsDialog::OnBnClickedButtonZoomIn)
	ON_BN_CLICKED(IDC_BUTTON_ZOOM_OUT, &CHorzTickerContentsDialog::OnBnClickedButtonZoomOut)
	ON_BN_CLICKED(IDC_BUTTON_ORIGINAL, &CHorzTickerContentsDialog::OnBnClickedButtonOriginal)
END_MESSAGE_MAP()


// CHorzTickerContentsDialog 메시지 처리기입니다.

BOOL CHorzTickerContentsDialog::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnBrowserContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btnContentsCommentAdd.LoadBitmap(IDB_BTN_PLUS, RGB(255, 255, 255));
	m_btnContentsCommentDelete.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnContentsCommentUp.LoadBitmap(IDB_BTN_UP, RGB(255, 255, 255));
	m_btnContentsCommentDown.LoadBitmap(IDB_BTN_DOWN, RGB(255, 255, 255));
	m_btnPreviewPlay.LoadBitmap(IDB_BTN_PLAY, RGB(255, 255, 255));
	m_btnPreviewStop.LoadBitmap(IDB_BTN_STOP, RGB(255, 255, 255));

	m_btnBrowserContentsFile.SetToolTipText(LoadStringById(IDS_HORZTICKERCONTENTSDIALOG_BUT001));
	m_btnContentsCommentAdd.SetToolTipText(LoadStringById(IDS_HORZTICKERCONTENTSDIALOG_BUT002));
	m_btnContentsCommentDelete.SetToolTipText(LoadStringById(IDS_HORZTICKERCONTENTSDIALOG_BUT003));
	m_btnContentsCommentUp.SetToolTipText(LoadStringById(IDS_HORZTICKERCONTENTSDIALOG_BUT004));
	m_btnContentsCommentDown.SetToolTipText(LoadStringById(IDS_HORZTICKERCONTENTSDIALOG_BUT005));
	m_btnPreviewPlay.SetToolTipText(LoadStringById(IDS_HORZTICKERCONTENTSDIALOG_BUT006));
	m_btnPreviewStop.SetToolTipText(LoadStringById(IDS_HORZTICKERCONTENTSDIALOG_BUT007));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(15);

	m_editContentsFontSize.SetValue(45);

//	m_cbxContentsFont.SubclassDlgItem (IDC_COMBO_CONTENTS_FONT, this);
//	m_cbxContentsFont.m_csSample = "No Fate But What We Make";
	m_cbxContentsFont.SetPreviewStyle (CFontPreviewCombo::NAME_ONLY, false);
	m_cbxContentsFont.SetFontHeight (12, true);

	for(int i=0; i<m_cbxContentsFont.GetCount(); i++)
	{
		CString font_name;
		m_cbxContentsFont.GetLBText(i,font_name);
		if(font_name == "System")
		{
			m_cbxContentsFont.SetCurSel(i);
			break;
		}
	}

	m_cbxContentsTextColor.InitializeDefaultColors();
	m_cbxContentsBGColor.InitializeDefaultColors();

	m_cbxContentsTextColor.SetSelectedColorValue(RGB(255,255,255));
	m_cbxContentsBGColor.SetSelectedColorValue(RGB(0,0,0));

	m_sliderContentsPlaySpeed.SetRange(1,5);
	m_sliderContentsPlaySpeed.SetPos(3);

	// 창일향 : 폰트(System), 폰트크기(50), 배경색(Medium Blue), 전경색(White), 속도(중간)으로 고정하고 변경할 수 없도록 한다.
	if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
	{
		m_editContentsPlaySecond.SetValue(1);

		m_cbxContentsFont.EnableWindow(FALSE);

		m_editContentsFontSize.SetValue(50);
		m_editContentsFontSize.EnableWindow(FALSE);

		m_cbxContentsTextColor.SetSelectedColorName("White");
		m_cbxContentsTextColor.EnableWindow(FALSE);
		m_cbxContentsBGColor.SetSelectedColorName("MidnightBlue");
		m_cbxContentsBGColor.EnableWindow(FALSE);

		m_sliderContentsPlaySpeed.EnableWindow(FALSE);

		GetDlgItem(IDC_STATIC_RUNNING_TIME)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND)->SetWindowText(FALSE);

		GetDlgItem(IDC_STATIC_RUNNING_TIME2)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND2)->ShowWindow(TRUE);

		m_editContentsPlayMinute.EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_MINUTE)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_STATIC_RUNNING_TIME)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND)->ShowWindow(TRUE);

		GetDlgItem(IDC_STATIC_RUNNING_TIME2)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND2)->ShowWindow(FALSE);
	}

	m_btnDelFile.LoadBitmap(IDB_BTN_DELFILE, RGB(236, 233, 216));
	m_btnDelFile.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT003));

	m_btnZoomIn.LoadBitmap(IDB_BTN_PLUS, RGB(236, 233, 216));
	m_btnZoomIn.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT007));
	m_btnZoomIn.EnableWindow(FALSE);
	m_btnZoomIn.ShowWindow(SW_HIDE);

	m_btnZoomOut.LoadBitmap(IDB_BTN_MINUS, RGB(236, 233, 216));
	m_btnZoomOut.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT008));
	m_btnZoomOut.EnableWindow(FALSE);
	m_btnZoomOut.ShowWindow(SW_HIDE);

	m_btnOriginal.LoadBitmap(IDB_BTN_1_1, RGB(236, 233, 216));
	m_btnOriginal.SetToolTipText(LoadStringById(IDS_SMSCONTENTSDIALOG_BUT009));
	m_btnOriginal.EnableWindow(FALSE);
	m_btnOriginal.ShowWindow(SW_HIDE);

//	CRect rect;
//	m_listctrlContentsComment.GetClientRect(rect);
//	m_listctrlContentsComment.InsertColumn(0, "comment", LVCFMT_LEFT, rect.Width() + 100);
//	m_listctrlContentsComment.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER); // auto size
//	m_listctrlContentsComment.SetTextBkColor(RGB(232,232,232));
//	m_listctrlContentsComment.SetExtendedStyle(m_listctrlContentsComment.GetExtendedStyle() | LVS_EX_FULLROWSELECT);

	//
	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);
	client_rect.DeflateRect(1,1);

	m_wndTicker.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndTicker.ShowWindow(SW_SHOW);

	//
	m_reposControl.AddControl(&m_groupPreview, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_staticContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_wndTicker, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnPreviewPlay, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnZoomIn, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnZoomOut, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnOriginal, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnPreviewStop, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);

	m_reposControl.Move();

	//
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsWidth);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsHeight);
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsFileSize);

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}//if

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CHorzTickerContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnOK();
}

void CHorzTickerContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnCancel();
	GetParent()->PostMessage(WM_CLOSE);
}

void CHorzTickerContentsDialog::Stop()
{
	m_wndTicker.Stop(false);
}

bool CHorzTickerContentsDialog::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData(TRUE);

	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	//m_editContentsFileName.GetWindowText(info.strLocation);
	info.strLocalLocation = m_strLocation;
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();
	info.nFontSize = m_editContentsFontSize.GetValueInt();
	m_cbxContentsFont.GetLBText(m_cbxContentsFont.GetCurSel(), info.strFont);
	info.strFgColor = ::GetColorFromString(m_cbxContentsTextColor.GetSelectedColorValue());
	info.strBgColor = ::GetColorFromString(m_cbxContentsBGColor.GetSelectedColorValue());
	info.nPlaySpeed = E_TICKET_SLOW - m_sliderContentsPlaySpeed.GetPos() + 1;
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);

	// 0000735: [창일향]의 경우, 티커, 문자 입력창에서 전각문자를 사용할 수 없도록 할것.
	if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
	{
		CString strText;
		pContents->GetWindowText(strText);
		if(CCharType::GetInstance()->IsIncludeType(strText, CCharType::CT_FULL_WIDTH))
		{
			UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG009), MB_ICONSTOP);
			return false;
		}
	}

	for(int i=0 ; i<TICKER_COUNT ; i++)
	{
		info.strComment[i] = "";
		if( i < pContents->GetLineCount() )
		{
			char szLine[255+1] = {0};
			pContents->GetLine(i, szLine, 255);
			info.strComment[i] = szLine;
		}
	}

	//
	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(info.strLocalLocation, dir, path, filename, ext);

	info.strLocalLocation.Format("%s%s", dir, path); 
	info.strFilename.Format("%s%s", filename, ext); 

	//
	if(!IsFitContentsName(info.strContentsName)) return false;
	if(info.nRunningTime == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
		return false;
	}
	if(info.nFontSize == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG005), MB_ICONSTOP);
		return false;
	}

	return true;
}

bool CHorzTickerContentsDialog::SetContentsInfo(CONTENTS_INFO& info)
{
	m_editContentsName.SetWindowText(info.strContentsName);
	//m_editContentsFileName.SetWindowText(info.strLocation + info.strFilename);
	m_editContentsFileName.SetWindowText(info.strFilename);

	if(info.strFilename.IsEmpty()){
		m_strLocation = _T("");
		info.nFilesize = 0;
	}else{
		m_strLocation = info.strLocalLocation + info.strFilename;
	}

	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}//if
	m_editContentsFontSize.SetWindowText(::ToString(info.nFontSize));

	int nFindFont = -1;
	int nSystemFont = -1;
	for(int i=0; i<m_cbxContentsFont.GetCount(); i++)
	{
		CString font_name;
		m_cbxContentsFont.GetLBText(i,font_name);
		
		if(font_name == "System")
		{
			nSystemFont = i;
		}

		if(font_name == info.strFont)
		{
			nFindFont = i;
			break;
		}
	}

	if(nFindFont >= 0) m_cbxContentsFont.SetCurSel(nFindFont);
	else               m_cbxContentsFont.SetCurSel(nSystemFont);

	m_cbxContentsTextColor.SetSelectedColorValue(::GetColorFromString(info.strFgColor));
	m_cbxContentsBGColor.SetSelectedColorValue(::GetColorFromString(info.strBgColor));

	// Schedule.cpp 의 코드를 그대로 가져옴
	//속도를 총 5단계로 한다.
	//빠름(1~49) >> 조금빠름(50~99) >> 보통(100~199) >> 조금느림(200~499) >> 아주느림(500~1000)
	if(info.nPlaySpeed < 50)
	{
		//빠름(1~49)
		info.nPlaySpeed = E_TICKER_FAST;
	}
	else if(info.nPlaySpeed >= 50 && info.nPlaySpeed < 100)
	{
		//조금빠름(50~99)
		info.nPlaySpeed = E_TICKER_LITTLE_FAST;
	}
	else if(info.nPlaySpeed >= 100 && info.nPlaySpeed < 200)
	{
		//보통(100~199)
		info.nPlaySpeed = E_TICKER_NORMAL;
	}
	else if(info.nPlaySpeed >= 200 && info.nPlaySpeed < 500)
	{
		//조금느림(200~499)
		info.nPlaySpeed = E_TICKER_LITTLE_SLOW;
	}
	else if(info.nPlaySpeed >= 500 && info.nPlaySpeed <= 1000)
	{
		//아주느림(500~1000)
		info.nPlaySpeed = E_TICKET_SLOW;
	}//if

	m_sliderContentsPlaySpeed.SetPos(E_TICKET_SLOW - info.nPlaySpeed + 1);

	int nEndPos = 0;
	for(int i=TICKER_COUNT-1 ; i>=0; i--)
	{
		if(!info.strComment[i].IsEmpty())
		{
			nEndPos = i;
			break;
		}
	}

	CString szContents = _T("");
	for(int i=0; i<=nEndPos; i++)
	{
		if(i>0) szContents += _T("\r\n");
		szContents += info.strComment[i];
	}

	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);
	pContents->SetWindowText(szContents);

	UpdateData(FALSE);

	return true;
}

void CHorzTickerContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CSubContentsDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();
}

//void CHorzTickerContentsDialog::OnLvnEndlabeleditListContentsComment(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	*pResult = 0;
//
//	if(pDispInfo->item.pszText != NULL)
//	{
//		m_listctrlContentsComment.SetItemText(pDispInfo->item.iItem, pDispInfo->item.iSubItem, pDispInfo->item.pszText );
//		m_listctrlContentsComment.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER); // auto size
//	}
//}
//
//void CHorzTickerContentsDialog::OnLvnKeydownListContentsComment(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	*pResult = 0;
//
//	switch(pLVKeyDow->wVKey)
//	{
//	case VK_F2:
//		for(int i=0; i<m_listctrlContentsComment.GetItemCount(); i++)
//		{
//			if(m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED))
//			{
//				m_listctrlContentsComment.SetFocus();
//				m_listctrlContentsComment.EditLabel(i);
//			}
//		}
//		break;
//	case VK_DELETE:
//		OnBnClickedButtonContentsCommentDelete();
//		break;
//	}
//}

void CHorzTickerContentsDialog::OnBnClickedButtonBrowserContentsFile()
{
	CString szFileExts;
	CString filter;

	// 창일향 : 콘텐츠는 오직 JPG 파일 뿐이다
	if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
	{
		szFileExts = "*.jpg; *.jpeg;";
		filter.Format("JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;||"
					 , szFileExts
					 );
	}
	else
	{
		szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
		filter.Format("All Image Files|%s|"
					  "Bitmap Files (*.bmp)|*.bmp|"
					  "JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;|"
					  "GIF Files (*.gif)|*.gif|"
					  "PCX Files (*.pcx)|*.pcx|"
					  "PNG Files (*.png)|*.png|"
					  "TIFF Files (*.tif)|*.tif;*.tiff||"
					 , szFileExts
					 );
	}

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, CONTENTS_IMAGE))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	//m_editContentsFileName.SetWindowText(dlg.GetPathName());
	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = dlg.GetPathName();
	m_editContentsFileName.SetSel(0, -1);

	m_wndTicker.CloseFile();

	CFileStatus fs;
//	if(CFile::GetStatus(dlg.GetPathName(), fs))
	if(CEnviroment::GetFileSize(dlg.GetPathName(), fs.m_size))
	{
		// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
		char szMd5[16*2+1] = {0};
		if(fs.m_size < 1000000)
		{
			CWaitMessageBox wait;
			CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
		}
		m_strFileMD5 = CString(szMd5);

		m_wndTicker.m_strMediaFullPath = dlg.GetPathName();
		if(m_wndTicker.OpenFile(1))
		{
			int width = m_wndTicker.m_bgImage.GetWidth();
			int height = m_wndTicker.m_bgImage.GetHeight();

			m_staticContentsWidth.SetWindowText(::ToString(width) + " ");
			m_staticContentsHeight.SetWindowText(::ToString(height) + " ");
			m_staticContentsFileSize.SetWindowText(::ToMoneyTypeString((ULONGLONG)(fs.m_size/1024)) + " ");
			m_ulFileSize = fs.m_size;

			m_wndTicker.Invalidate();
		}
	}

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);
	if(contents_name.GetLength() == 0)
	{
		char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
		_splitpath(dlg.GetPathName(), drive, path, filename, ext);

		m_editContentsName.SetWindowText(filename);
	}

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CHorzTickerContentsDialog::OnBnClickedButtonContentsCommentAdd()
{
//	if(m_listctrlContentsComment.GetItemCount() < TICKER_COUNT)
//	{
//		int index = m_listctrlContentsComment.GetItemCount();
//		m_listctrlContentsComment.SetFocus();
//		m_listctrlContentsComment.InsertItem(index, "");
//		m_listctrlContentsComment.EditLabel(index);
//	}
}

void CHorzTickerContentsDialog::OnBnClickedButtonContentsCommentDelete()
{
//	int count = m_listctrlContentsComment.GetItemCount();
//	for(int i=0; i<count; i++)
//	{
//		if(m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED))
//		{
//			m_listctrlContentsComment.DeleteItem(i);
//			count--;
//			i--;
//		}
//	}
}

void CHorzTickerContentsDialog::OnBnClickedButtonContentsCommentUp()
{
//	for(int i=1; i<m_listctrlContentsComment.GetItemCount(); i++)
//	{
//		UINT state = m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED);
//		if(state == LVIS_SELECTED)
//		{
//			MoveItem(i, i-1);
//		}
//	}
}

void CHorzTickerContentsDialog::OnBnClickedButtonContentsCommentDown()
{
//	for(int i=m_listctrlContentsComment.GetItemCount()-2; i>=0; i--)
//	{
//		UINT state = m_listctrlContentsComment.GetItemState(i, LVIS_SELECTED);
//		if(state == LVIS_SELECTED)
//		{
//			MoveItem(i, i+1);
//		}
//	}
}

void CHorzTickerContentsDialog::OnBnClickedButtonPreviewPlay()
{
	SetPlayContents();
	EnableAllControls(FALSE);
	m_wndTicker.Play();
}

void CHorzTickerContentsDialog::OnBnClickedButtonPreviewStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	EnableAllControls(TRUE);
	m_wndTicker.Stop(false);
}

void CHorzTickerContentsDialog::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editContentsFileName.EnableWindow(bEnable);
	m_btnBrowserContentsFile.EnableWindow(bEnable);
	m_btnDelFile.EnableWindow(bEnable);
	m_staticContentsWidth.EnableWindow(bEnable);
	m_staticContentsHeight.EnableWindow(bEnable);
	m_staticContentsFileSize.EnableWindow(bEnable);
	//m_editContentsPlayMinute.EnableWindow(bEnable);
	//m_editContentsPlaySecond.EnableWindow(bEnable);
	m_editContentsFontSize.EnableWindow(bEnable);
	m_cbxContentsFont.EnableWindow(bEnable);
	m_cbxContentsTextColor.EnableWindow(bEnable);
	m_cbxContentsBGColor.EnableWindow(bEnable);
	m_sliderContentsPlaySpeed.EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_CONTENTS_COMMENT)->EnableWindow(bEnable);
	m_btnContentsCommentAdd.EnableWindow(bEnable);
	m_btnContentsCommentDelete.EnableWindow(bEnable);
	m_btnContentsCommentUp.EnableWindow(bEnable);
	m_btnContentsCommentDown.EnableWindow(bEnable);
	GetDlgItem(IDC_PERMANENT_CHECK)->EnableWindow(bEnable);

	if(!m_bPermanent)
	{
		m_editContentsPlayMinute.EnableWindow(bEnable);
		m_editContentsPlaySecond.EnableWindow(bEnable);
	}//if

	// 창일향 : 폰트(System), 폰트크기(50), 배경색(Medium Blue), 전경색(White), 속도(중간)으로 고정하고 변경할 수 없도록 한다.
	if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
	{
		m_cbxContentsFont.EnableWindow(FALSE);
		m_editContentsFontSize.EnableWindow(FALSE);
		m_cbxContentsTextColor.EnableWindow(FALSE);
		m_cbxContentsBGColor.EnableWindow(FALSE);
		m_sliderContentsPlaySpeed.EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_RUNNING_TIME)->ShowWindow(FALSE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND)->SetWindowText(FALSE);
		GetDlgItem(IDC_STATIC_RUNNING_TIME2)->ShowWindow(TRUE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_SECOND2)->ShowWindow(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_CONTENTS_PLAY_MINUTE)->EnableWindow(FALSE);
	}
}

void CHorzTickerContentsDialog::SetPlayContents()
{
	m_wndTicker.CloseFile();

	//m_editContentsFileName.GetWindowText(m_wndTicker.m_strMediaFullPath);
	m_wndTicker.m_strMediaFullPath = m_strLocation;

	CString str_font;
	m_cbxContentsFont.GetLBText(m_cbxContentsFont.GetCurSel(), str_font);

	COLORREF text_color = m_cbxContentsTextColor.GetSelectedColorValue();
	COLORREF bg_color = m_cbxContentsBGColor.GetSelectedColorValue();

	m_wndTicker.m_font = str_font;
	m_wndTicker.m_fontSize = m_editContentsFontSize.GetValueInt();
	m_wndTicker.m_rgbFgColor = text_color;
	m_wndTicker.m_rgbBgColor = bg_color;
	m_wndTicker.m_playSpeed = E_TICKET_SLOW - m_sliderContentsPlaySpeed.GetPos() + 1;

	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);
	for(int i=0 ; i<TICKER_COUNT ; i++)
	{
		m_wndTicker.m_comment[i] = "";
		if( i < pContents->GetLineCount() )
		{
			char szLine[255+1] = {0};
			pContents->GetLine(i, szLine, 255);
			m_wndTicker.m_comment[i] = szLine;
		}
	}

	m_wndTicker.CreateFile();
	m_wndTicker.OpenFile(0);
}

void CHorzTickerContentsDialog::MoveItem(int nItem, int nItemNewPos)
{
	//int count = m_listctrlContentsComment.GetItemCount();

	//if( nItem == nItemNewPos ) return;
	//if( nItem < 0 || nItemNewPos < 0 ) return;
	//if( count <= nItem || count <= nItemNewPos) return;

	//CString str = m_listctrlContentsComment.GetItemText(nItem, 0);
	//UINT old_state = m_listctrlContentsComment.GetItemState(nItem, 0xffff);

	//m_listctrlContentsComment.DeleteItem(nItem);

	//m_listctrlContentsComment.InsertItem(nItemNewPos, str);
	//m_listctrlContentsComment.SetItemState(nItemNewPos, old_state, 0xffff);
}

void CHorzTickerContentsDialog::OnBnClickedPermanentCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//24시간을 설정
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		// 창일향 : 폰트(System), 폰트크기(36), 배경색(Medium Blue), 전경색(White), 속도(중간)으로 고정하고 변경할 수 없도록 한다.
		if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
		{
			m_editContentsPlayMinute.SetWindowText("");
			m_editContentsPlaySecond.SetWindowText("1");
			m_editContentsPlayMinute.EnableWindow(FALSE);
			m_editContentsPlaySecond.EnableWindow(TRUE);
		}
		else
		{
			m_editContentsPlayMinute.SetWindowText("");
			m_editContentsPlaySecond.SetWindowText("15");

			m_editContentsPlayMinute.EnableWindow(TRUE);
			m_editContentsPlaySecond.EnableWindow(TRUE);
		}
	}//if
}

void CHorzTickerContentsDialog::OnBnClickedDeletefile()
{
	m_ulFileSize = 0;
	m_editContentsFileName.SetWindowText(_T(""));
	m_staticContentsFileSize.SetWindowText(_T(""));
	m_strLocation = _T("");
	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	m_strFileMD5 = _T("");
}

BOOL CHorzTickerContentsDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CHorzTickerContentsDialog::OnEnChangeEditContentsComment()
{
	// 10개로 제한.
	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_CONTENTS_COMMENT);

	DWORD dwSel = pContents->GetSel();
	int nScroll = pContents->GetScrollPos(SB_HORZ);

	CString strContents;
	for(int i=0; i<pContents->GetLineCount() ;i++)
	{
		if(i!=0) strContents += "\r\n";

		char szBuf[255+1] = {0};
		pContents->GetLine(i, szBuf, 255);

		//scratchUtil::getInstance()->hasDoubleByte(strBuf.Mid(255))

		strContents += szBuf;
	}

	pContents->SetWindowText(strContents);
	pContents->SetSel(dwSel, FALSE);
//	pContents->SetFocus();
}

void CHorzTickerContentsDialog::OnBnClickedButtonZoomIn()
{
//	ZoomIn();
}

void CHorzTickerContentsDialog::OnBnClickedButtonZoomOut()
{
//	ZoomOut();
}

void CHorzTickerContentsDialog::OnBnClickedButtonOriginal()
{
//	RestoreZoom();
}
