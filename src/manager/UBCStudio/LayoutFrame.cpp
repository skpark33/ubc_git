// LayoutFrame.cpp : implementation of the CLayoutFrame class
//
#include "stdafx.h"
#include "UBCStudio.h"

#include "LayoutFrame.h"
#include "Enviroment.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLayoutFrame

IMPLEMENT_DYNCREATE(CLayoutFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CLayoutFrame, CMDIChildWnd)
	ON_WM_GETMINMAXINFO()
	ON_MESSAGE(WM_TEMPLATE_FRAME_SELECT_CHANGED, OnTemplateFrameSelectChanged)
	ON_MESSAGE(WM_TEMPLATE_LIST_CHANGED, OnTemplateListChanged)
	ON_MESSAGE(WM_SAVE_CONFIG_COMPELETE, OnConfigSaveComplete)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CLayoutFrame construction/destruction

CLayoutFrame::CLayoutFrame()
{
	// TODO: add member initialization code here
}

CLayoutFrame::~CLayoutFrame()
{
}

BOOL CLayoutFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style = WS_CHILD | WS_VISIBLE | WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU
		| FWS_ADDTOTITLE | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;

	return TRUE;
}


// CLayoutFrame diagnostics

#ifdef _DEBUG
void CLayoutFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CLayoutFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

BOOL CLayoutFrame::PreTranslateMessage(MSG* pMsg)
{
	return CMDIChildWnd::PreTranslateMessage(pMsg);
}

// CLayoutFrame message handlers

void CLayoutFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);

//	lpMMI->ptMinTrackSize.x = 720;
//	lpMMI->ptMinTrackSize.y = 540;
}

LRESULT CLayoutFrame::OnTemplateListChanged(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->SendMessage(WM_TEMPLATE_LIST_CHANGED);
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// complete config save message handler\n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CLayoutFrame::OnConfigSaveComplete(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->SendMessage(WM_SAVE_CONFIG_COMPELETE);
}

int CLayoutFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDR_LAYOUT), false);

	return 0;
}

LRESULT CLayoutFrame::OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam)
{
	LPCTSTR strTemplateId = (LPCTSTR)wParam;

	TraceLog(("CLayoutFrame::OnTemplateFrameSelectChanged (%s)", strTemplateId));
	return GetActiveView()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, 
		(WPARAM)(LPCTSTR)strTemplateId, (LPARAM)(LPCTSTR)"");
}
