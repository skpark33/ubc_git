#pragma once
#include "afxcmn.h"

#include "common/HoverButton.h"
#include "PictureEx.h"
#include "ximage.h"
#include "afxwin.h"
#include "InfoImportThread.h"
#include "CheckListCtrl.h"

// CHostSelectDlg 대화 상자입니다.

class CHostSelectDlg : public CDialog
{
	DECLARE_DYNAMIC(CHostSelectDlg)

public:
	CHostSelectDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHostSelectDlg();

	void	SetHostInfoList(CPtrArray* paryHostInfo);					///<Host info 배열을 설정
	void	InitList(void);												///<Host list를 list control에 설정
	void	SetParentPtr(CWnd* pFrm, void* pclsDoc, bool bNew = false, CString strSelDrive=_T(""), CString strSelHostName=_T(""));	///<메인 frame, document 포인터를 설정

	CPtrArray*			m_pryLocalHostInfo;				///<Host info 배열
	CString				m_strSelectedHostName;			///<Host list들 중 선택된 Host name;
	CString				m_strSelectedDrive;				///<Host list들 중 선택된 Host의 drive;
	CImageList			m_imgCheck;						///<Check box image list
	void*				m_pclsDoc;						///<메인 frame의 document 포인터
	CInfoImportThread*	m_pThreadImport;				///<Import prcess thread
	CWnd*				m_frmParent;					///<MainFrame 포인터

	CPictureEx			m_imgGif;						///<GIF splash 파일을 prcess하는 객체
	CxImage				m_imgTop;						///<상단의 배경 bmp 파일을 process하는 객체
	CxImage				m_imgBottom;					///<하단의 배경 bmp 파일을 process하는 객체
	CHoverButton		m_btnOK;						///<select 버튼
	CHoverButton		m_btnCancel;					///<cancel 버튼
	CHoverButton		m_btnDelete;					///<delete 버튼
	CCheckListCtrl		m_listHost;						///<host 정보를 보여주는 list 컨트롤
	CEdit				m_edtHostName;					///<선택된 hot 이름을 보여주는 컨트롤
	bool				m_bSelect;						///<host가 선택되었는지를 나타내는 flag
	int					m_nWidthTop;					///<상단 영역의 넓이
	int					m_nHeightTop;					///<상단 연역의 높이
	int					m_nWidthBottom;					///<하단 영역의 넓이
	int					m_nHeightBottom;				///<하단 영역의 높이
	bool				m_bNew;							///<새로만들기 상태인지 나타내는 flag

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SELECT_HOST_DLG };

	CBitmap m_bmpBitmapTop;
	CBitmap m_bmpBitmapBottom;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	afx_msg void OnNMClickHostList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnPaint();
	afx_msg void OnBnClickedCancel();
	afx_msg LRESULT	OnImportComplete(WPARAM wParam, LPARAM lParam);	///<complete import message handler
	afx_msg void OnNMDblclkHostList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedDelete();
};
