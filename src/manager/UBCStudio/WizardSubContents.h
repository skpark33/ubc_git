#pragma once
#include "UBCStudioDoc.h"
#include "Schedule.h"

class CWizardSubContents : public CDialog
{
	DECLARE_DYNAMIC(CWizardSubContents)

	CUBCStudioDoc* m_pDocument;

public:
	CWizardSubContents(UINT nIDTemplate, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CWizardSubContents();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	bool	m_bPreviewMode;
	CBrush	m_brushBG;
	CArray<CWnd*, CWnd*>	m_listNoCTLWnd;

public:
	void	       SetDocument(CUBCStudioDoc* pDoc) { m_pDocument=pDoc; };
	CUBCStudioDoc* GetDocument();

	virtual void SetData(CString strContentsId, CString strValue) = 0;
	virtual void GetData(CString& strContentsId, CString& strValue) = 0;

	virtual CONTENTS_TYPE GetContentsType() = 0;
	virtual void SetPreviewMode(bool bPreviewMode) { m_bPreviewMode = bPreviewMode; };

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
