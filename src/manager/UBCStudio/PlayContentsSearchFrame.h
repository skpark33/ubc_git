// PlayContentsSearchFrame.h : interface of the CPlayContentsSearchFrame class
//


#pragma once


class CPlayContentsSearchFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CPlayContentsSearchFrame)
public:
	CPlayContentsSearchFrame();

// Attributes
public:

// Operations
public:

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CPlayContentsSearchFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	afx_msg LRESULT OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFrameInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam);
};
