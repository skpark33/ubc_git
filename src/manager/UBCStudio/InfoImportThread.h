#pragma once



// CInfoImportThread

class CInfoImportThread : public CWinThread
{
	DECLARE_DYNCREATE(CInfoImportThread)
protected:
	CInfoImportThread();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CInfoImportThread();

protected:
	void*	m_pParent;			///<Parent window
	void*	m_pParentDoc;		///<Parent document
	CString	m_strDrive;			///<import 하려는 host 정보가 있는 드라이브
	CString	m_strHost;			///<import 하려는 host 이름
	
public:
	bool    m_bInfoMsg;
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	void	SetThreadParam(void* pParent, void* pParentDoc, CString strDrive, CString strHost);	///<Set parent params

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual int Run();
};


