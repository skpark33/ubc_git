// RunPreviewThred.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "RunPreviewThread.h"

#include "MainFrm.h"
#include "UBCStudioDoc.h"


// CRunPreviewThread

IMPLEMENT_DYNCREATE(CRunPreviewThread, CWinThread)

CRunPreviewThread::CRunPreviewThread()
{
	m_pParent = NULL;
	m_pParentDoc = NULL;
	m_strTemplateID = "";
	m_bSave = false;
}

CRunPreviewThread::~CRunPreviewThread()
{
}

BOOL CRunPreviewThread::InitInstance()
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CRunPreviewThread::ExitInstance()
{
	::CoUninitialize();

	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CRunPreviewThread, CWinThread)
END_MESSAGE_MAP()


// CRunPreviewThread 메시지 처리기입니다.

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Set parent params \n
/// @param (void*) pParent : (in) thread owner window pointer
/// @param (void*) pParentDoc : (in) thread owner window's document pointer
/// @param (CString) strTemplateID : (in) Template ID to preview
/// @param (bool) bSave : (in) Flag to be save
/////////////////////////////////////////////////////////////////////////////////
void CRunPreviewThread::SetThreadParam(void* pParent, void* pParentDoc, CString strTemplateID, bool bSave)
{
	m_pParent = pParent;
	m_pParentDoc = pParentDoc;
	m_strTemplateID = strTemplateID;
	m_bSave = bSave;
}

int CRunPreviewThread::Run()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CMainFrame* pParent = (CMainFrame*)m_pParent;
	CUBCStudioDoc* pParentDoc = (CUBCStudioDoc*)m_pParentDoc;
	if(pParentDoc->RunPreview(m_strTemplateID, m_bSave))
	{
		pParent->PostMessage(WM_PREVIEW_COMPELETE, 1, 0);
	}
	else
	{
		pParent->PostMessage(WM_PREVIEW_COMPELETE, 0, 0);
	}//if

	ExitThread(0);
	return 0;
}
