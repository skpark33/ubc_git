// ExportEEDlg.cpp : implementation file
//
#include "stdafx.h"
#include "UBCStudio.h"
#include "ExportEEDlg.h"
#include "Enviroment.h"

#define STR_EPLH_SITEID			LoadStringById(IDS_EXPORTEEDLG_LST001)
#define STR_EPLH_HOSTID			LoadStringById(IDS_EXPORTEEDLG_LST002)
#define STR_EPLH_DISPLAY		LoadStringById(IDS_EXPORTEEDLG_LST003)
#define STR_EPLH_AUTOSCHE		LoadStringById(IDS_EXPORTEEDLG_LST004)
#define STR_EPLH_CURRSCHE		LoadStringById(IDS_EXPORTEEDLG_LST005)
#define STR_EPLH_LASTSCHE		LoadStringById(IDS_EXPORTEEDLG_LST007)
#define STR_EPLH_CREATOR		LoadStringById(IDS_EXPORTEEDLG_LST006)

// CExportEEDlg dialog
IMPLEMENT_DYNAMIC(CExportEEDlg, CDialog)

CExportEEDlg::CExportEEDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExportEEDlg::IDD, pParent)
{
	m_bSaveAs = false;
}

CExportEEDlg::~CExportEEDlg()
{
	POSITION pos = m_SelectInfo.GetHeadPosition();
	while(pos)
	{
		SHostInfo4Studio* pHost = m_SelectInfo.GetNext(pos);
		if(pHost) delete pHost;
	}
	m_SelectInfo.RemoveAll();
}

void CExportEEDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcHostList);
	DDX_Control(pDX, IDC_LIST_SEL_HOST, m_lcSelHostList);
	DDX_Control(pDX, IDC_BUTTON_HOST_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDC_CB_ONOFF, m_cbOnOff);
	DDX_Control(pDX, IDC_CB_OPERATION, m_cbOperation);
}

BEGIN_MESSAGE_MAP(CExportEEDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_UPLOAD_SERVER, &CExportEEDlg::OnBnClickedButtonUploadServer)
	ON_BN_CLICKED(IDC_BUTTON_HOST_REFRESH, &CExportEEDlg::OnBnClickedButtonHostRefresh)
	ON_BN_CLICKED(IDC_BN_ADD, &CExportEEDlg::OnBnClickedBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, &CExportEEDlg::OnBnClickedBnDel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOST, &CExportEEDlg::OnNMDblclkListHost)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SEL_HOST, &CExportEEDlg::OnNMDblclkListSelHost)
END_MESSAGE_MAP()

// CExportEEDlg message handlers
BOOL CExportEEDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_SelectInfo.RemoveAll();
	m_szMsgBoxTitle = LoadStringById(IDS_EXPORTEEDLG_STR001);

	if(CEnviroment::eAuthSuper != GetEnvPtr()->m_Authority)
	{
		((CEdit*)GetDlgItem(IDC_EDIT_FILTER_SITEID))->SetReadOnly(TRUE);
		SetDlgItemText(IDC_EDIT_FILTER_SITEID, GetEnvPtr()->m_szSite);
	}

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnAdd    .LoadBitmap(IDB_BUTTON_NEXT2  , RGB(255,255,255));
	m_bnDel    .LoadBitmap(IDB_BUTTON_PREV2  , RGB(255,255,255));

	m_bnRefresh.SetToolTipText(LoadStringById(IDS_EXPORTEEDLG_BTN001));

	m_cbOnOff.AddString(LoadStringById(IDS_EXPORTEEDLG_STR010));
	m_cbOnOff.AddString(LoadStringById(IDS_EXPORTEEDLG_STR006));
	m_cbOnOff.AddString(LoadStringById(IDS_EXPORTEEDLG_STR007));
	m_cbOnOff.SetCurSel(0);

	m_cbOperation.AddString(LoadStringById(IDS_EXPORTEEDLG_STR010));
	m_cbOperation.AddString(LoadStringById(IDS_EXPORTEEDLG_STR008));
	m_cbOperation.AddString(LoadStringById(IDS_EXPORTEEDLG_STR009));
	m_cbOperation.SetCurSel(0);

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("HOST-FILTER", "AdminStat", "0", buf, 1024, szPath);
	m_cbOnOff.SetCurSel(atoi(buf));
	GetPrivateProfileString("HOST-FILTER", "OperaStat", "0", buf, 1024, szPath);
	m_cbOperation.SetCurSel(atoi(buf));
	GetPrivateProfileString("HOST-FILTER", "SiteID", "", buf, 1024, szPath);
	SetDlgItemText(IDC_EDIT_FILTER_SITEID, buf);
	GetPrivateProfileString("HOST-FILTER", "HostID", "", buf, 1024, szPath);
	SetDlgItemText(IDC_EDIT_FILTER_HOST, buf);

	InitHostList();
//	RefreshHostList();

	InitSelHostList();

	// 스튜디오에서는 서버로만 저장하도록 하며, 단말에 내리는것은 매니져에서 하도록 한다.
	//if( CEnviroment::eKIA     == GetEnvPtr()->m_Customer ||
	//	CEnviroment::eHYUNDAI == GetEnvPtr()->m_Customer ||
	//	CEnviroment::eKMNL    == GetEnvPtr()->m_Customer ||
	//	CEnviroment::eKMCL    == GetEnvPtr()->m_Customer )
	//{
		OnBnClickedButtonUploadServer();
	//}
	//else
	//{
	//	RefreshSelHostList();
	//}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CExportEEDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcHostList.SaveColumnState("EE_EXPORT_HOST-LIST", szPath);
	m_lcSelHostList.SaveColumnState("EE_EXPORT_SELHOST-LIST", szPath);
}

void CExportEEDlg::InitHostList()
{
	m_lcHostList.SetExtendedStyle(m_lcHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck    ] = _T("");
	m_szColum[eSiteID   ] = STR_EPLH_SITEID;
	m_szColum[eHostID   ] = STR_EPLH_HOSTID;
	m_szColum[eDisplayNo] = STR_EPLH_DISPLAY;
	m_szColum[eAutoSche ] = STR_EPLH_AUTOSCHE;
	m_szColum[eCurSche  ] = STR_EPLH_CURRSCHE;
	m_szColum[eLastSche ] = STR_EPLH_LASTSCHE;
	m_szColum[eCreator  ] = STR_EPLH_CREATOR;

	m_lcHostList.InsertColumn(eCheck    , m_szColum[eCheck    ], LVCFMT_CENTER, 22);
	m_lcHostList.InsertColumn(eSiteID   , m_szColum[eSiteID   ], LVCFMT_LEFT  , 40);
	m_lcHostList.InsertColumn(eHostID   , m_szColum[eHostID   ], LVCFMT_LEFT  , 60);
	m_lcHostList.InsertColumn(eDisplayNo, m_szColum[eDisplayNo], LVCFMT_LEFT  , 50);
	m_lcHostList.InsertColumn(eAutoSche , m_szColum[eAutoSche ], LVCFMT_LEFT  , 80);
	m_lcHostList.InsertColumn(eCurSche  , m_szColum[eCurSche  ], LVCFMT_LEFT  , 80);
	m_lcHostList.InsertColumn(eLastSche , m_szColum[eLastSche ], LVCFMT_LEFT  , 80);
	m_lcHostList.InsertColumn(eCreator  , m_szColum[eCreator  ], LVCFMT_LEFT  , 40);

	m_lcHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcHostList.LoadColumnState("EE_EXPORT_HOST-LIST", szPath);
}

void CExportEEDlg::RefreshHostList()
{
	CString strWhere;
	CString strTmp;

	CString strSiteID;
	GetDlgItemText(IDC_EDIT_FILTER_SITEID, strSiteID);

	if(strSiteID.IsEmpty())
	{
		if(CEnviroment::eAuthSuper != GetEnvPtr()->m_Authority)
		{
			strTmp.Format(_T("siteId like '%%%s%%'"), GetEnvPtr()->m_szSite);
			strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
		}
	}
	else
	{
		strTmp.Format(_T("siteId like '%%%s%%'"), strSiteID);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	CString strHost;
	GetDlgItemText(IDC_EDIT_FILTER_HOST, strHost);

	if(!strHost.IsEmpty())
	{
		strTmp.Format(_T("hostId like '%%%s%%'"), strHost);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	if(m_cbOnOff.GetCurSel())
	{
		strTmp.Format(_T("adminState = %d"), (m_cbOnOff.GetCurSel()==1)? 1:0);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	if(m_cbOperation.GetCurSel())
	{
		strTmp.Format(_T("operationalState = %d"), (m_cbOperation.GetCurSel()==1)? 1:0);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	WritePrivateProfileString("HOST-FILTER", "AdminStat", ToString(m_cbOnOff.GetCurSel()), szPath);
	WritePrivateProfileString("HOST-FILTER", "OperaStat", ToString(m_cbOperation.GetCurSel()), szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteID", strSiteID, szPath);
	WritePrivateProfileString("HOST-FILTER", "HostID", strHost, szPath);

	m_lcHostList.DeleteAllItems();

	GetEnvPtr()->InitHostInfo("*", "*", strWhere);

	POSITION pos = GetEnvPtr()->m_lsHost.GetHeadPosition();
	while(pos)
	{
		SHostInfo4Studio* pInfo = GetEnvPtr()->m_lsHost.GetNext(pos);
		if(!pInfo) continue;

		int nRow = m_lcHostList.GetItemCount();
		m_lcHostList.InsertItem(nRow, "");
		UpdateRowList(&m_lcHostList, nRow, pInfo);
	}
}

void CExportEEDlg::OnBnClickedButtonHostRefresh()
{
	RefreshHostList();
}

void CExportEEDlg::InitSelHostList()
{
	m_lcSelHostList.SetExtendedStyle(m_lcSelHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck    ] = _T("");
	m_szColum[eSiteID   ] = STR_EPLH_SITEID;
	m_szColum[eHostID   ] = STR_EPLH_HOSTID;
	m_szColum[eDisplayNo] = STR_EPLH_DISPLAY;
	m_szColum[eAutoSche ] = STR_EPLH_AUTOSCHE;
	m_szColum[eCurSche  ] = STR_EPLH_CURRSCHE;
	m_szColum[eLastSche ] = STR_EPLH_LASTSCHE;
	m_szColum[eCreator  ] = STR_EPLH_CREATOR;

	m_lcSelHostList.InsertColumn(eCheck    , m_szColum[eCheck    ], LVCFMT_CENTER, 22);
	m_lcSelHostList.InsertColumn(eSiteID   , m_szColum[eSiteID   ], LVCFMT_LEFT  , 40);
	m_lcSelHostList.InsertColumn(eHostID   , m_szColum[eHostID   ], LVCFMT_LEFT  , 60);
	m_lcSelHostList.InsertColumn(eDisplayNo, m_szColum[eDisplayNo], LVCFMT_LEFT  , 50);
	m_lcSelHostList.InsertColumn(eAutoSche , m_szColum[eAutoSche ], LVCFMT_LEFT  , 80);
	m_lcSelHostList.InsertColumn(eCurSche  , m_szColum[eCurSche  ], LVCFMT_LEFT  , 80);
	m_lcSelHostList.InsertColumn(eLastSche , m_szColum[eLastSche ], LVCFMT_LEFT  , 80);
	m_lcSelHostList.InsertColumn(eCreator  , m_szColum[eCreator  ], LVCFMT_LEFT  , 40);

	m_lcSelHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcSelHostList.LoadColumnState("EE_EXPORT_SELHOST-LIST", szPath);
}

void CExportEEDlg::RefreshSelHostList()
{
	m_lcSelHostList.DeleteAllItems();

	GetEnvPtr()->InitHostInfo(GetEnvPtr()->m_strPackage, m_SelectInfo);

	POSITION pos = m_SelectInfo.GetHeadPosition();
	while(pos)
	{
		SHostInfo4Studio* pHost = m_SelectInfo.GetNext(pos);
		if(!pHost) continue;

		int nRow = m_lcSelHostList.GetItemCount();
		m_lcSelHostList.InsertItem(nRow, "");
		UpdateRowList(&m_lcSelHostList, nRow, pHost);
	}
}

void CExportEEDlg::UpdateRowList(CUTBListCtrlEx* pList, int nRow, SHostInfo4Studio* pHost)
{
	if(!pList || nRow < 0 || !pHost) return;

	pList->SetItemText(nRow, eSiteID   , pHost->siteId);
	pList->SetItemText(nRow, eHostID   , pHost->hostId);
	pList->SetItemText(nRow, eDisplayNo, (pHost->displayNo==1?LoadStringById(IDS_EXPORTEEDLG_STR002):LoadStringById(IDS_EXPORTEEDLG_STR003)));
	pList->SetItemText(nRow, eAutoSche , pHost->autoPackage);
	pList->SetItemText(nRow, eCurSche  , pHost->currentPackage);
	pList->SetItemText(nRow, eLastSche , pHost->lastPackage);
	pList->SetItemText(nRow, eCreator  , (pHost->networkUse?LoadStringById(IDS_EXPORTEEDLG_STR004):LoadStringById(IDS_EXPORTEEDLG_STR005)));

	pList->SetItemData(nRow, (DWORD_PTR)pHost);

	if(pHost->operationalState)
	{
		pList->SetRowColor(nRow, RGB(255,255,0), ::GetSysColor(COLOR_WINDOWTEXT));
	}
	else
	{
		pList->SetRowColor(nRow, RGB(200,200,200), ::GetSysColor(COLOR_WINDOWTEXT));
	}
}

void CExportEEDlg::OnBnClickedButtonUploadServer()
{
	POSITION pos = m_SelectInfo.GetHeadPosition();
	while(pos)
	{
		SHostInfo4Studio* pHost = m_SelectInfo.GetNext(pos);
		if(pHost) delete pHost;
	}
	m_SelectInfo.RemoveAll();

	m_szStartDT.Empty();
	m_szEndDT.Empty();

	OnOK();
}

void CExportEEDlg::OnBnClickedOk()
{
	if(m_SelectInfo.GetCount() <= 0)
	{
		if(MessageBox(LoadStringById(IDS_EXPORTEEDLG_MSG001),  m_szMsgBoxTitle, MB_ICONQUESTION|MB_YESNO) != IDYES)
			return;
	}

	m_szStartDT.Empty();
	m_szEndDT.Empty();

	OnOK();
}

void CExportEEDlg::OnBnClickedBnAdd()
{
	bool bChecked = false;
	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(m_lcHostList.GetCheck(i))
		{
			bChecked = true;
			break;
		}
	}

	if(!bChecked)
	{
		UbcMessageBox(LoadStringById(IDS_EXPORTEEDLG_MSG005));
		return;
	}

	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(!m_lcHostList.GetCheck(i)) continue;

		AddSelHost(i);

		m_lcHostList.SetCheck(i, FALSE);
	}

	m_lcHostList.SetCheckHdrState(FALSE);
}

void CExportEEDlg::OnBnClickedBnDel()
{
	for(int i=m_lcSelHostList.GetItemCount()-1; i>=0 ;i--)
	{
		if(!m_lcSelHostList.GetCheck(i)) continue;
		DelSelHost(i);
	}

	m_lcSelHostList.SetCheckHdrState(FALSE);
}

void CExportEEDlg::OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	AddSelHost(pNMLV->iItem);
}

void CExportEEDlg::OnNMDblclkListSelHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	DelSelHost(pNMLV->iItem);
}

void CExportEEDlg::AddSelHost(int nHostIndex)
{
	if(nHostIndex < 0 || nHostIndex >= m_lcHostList.GetItemCount()) return;

	SHostInfo4Studio* pHost = (SHostInfo4Studio*)m_lcHostList.GetItemData(nHostIndex);

	// 이미 추가된 단말인지 체크
	for(int i=0; i<m_lcSelHostList.GetItemCount() ;i++)
	{
		SHostInfo4Studio* pSelHost = (SHostInfo4Studio*)m_lcSelHostList.GetItemData(i);
		if( pHost->hostId    == pSelHost->hostId    &&
			pHost->displayNo == pSelHost->displayNo )
		{
			return;
		}
	}

	SHostInfo4Studio* pSelHost = new SHostInfo4Studio;
	*pSelHost = *pHost;

	m_SelectInfo.AddTail(pSelHost);
	int nRow = m_lcSelHostList.GetItemCount();
	m_lcSelHostList.InsertItem(nRow, "");
	UpdateRowList(&m_lcSelHostList, nRow, pSelHost);
}

void CExportEEDlg::DelSelHost(int nSelHostIndex)
{
	if(nSelHostIndex < 0 || nSelHostIndex >= m_lcSelHostList.GetItemCount()) return;

	SHostInfo4Studio* pSelHost = (SHostInfo4Studio*)m_lcSelHostList.GetItemData(nSelHostIndex);

	POSITION pos = m_SelectInfo.Find(pSelHost);
	if(pos) m_SelectInfo.RemoveAt(pos);

	if(pSelHost) delete pSelHost;

	m_lcSelHostList.DeleteItem(nSelHostIndex);
}

BOOL CExportEEDlg::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_SITEID)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOST  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_ONOFF          )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_OPERATION      )->GetSafeHwnd() )
		{
			OnBnClickedButtonHostRefresh();
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}
