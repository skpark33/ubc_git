#pragma once

interface IStudio
{
	virtual LPVOID Invoke(WPARAM, LPARAM) = 0;
};