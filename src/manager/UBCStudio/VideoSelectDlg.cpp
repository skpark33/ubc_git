// VideoSelectDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VideoSelectDlg.h"


// CVideoSelectDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CVideoSelectDlg, CWizardSubContents)

CVideoSelectDlg::CVideoSelectDlg(CWnd* pParent /*=NULL*/)
	: CWizardSubContents(CVideoSelectDlg::IDD, pParent)
	, m_wndVideo (NULL, 0, false)
	, m_reposControl(this)
{

}

CVideoSelectDlg::~CVideoSelectDlg()
{
}

void CVideoSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CWizardSubContents::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CB_CONTENTS, m_cbContents);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_PLAY, m_btnPreviewPlay);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_STOP, m_btnPreviewStop);
	DDX_Control(pDX, IDC_FRAME_PREVIEW, m_framePreview);
}


BEGIN_MESSAGE_MAP(CVideoSelectDlg, CWizardSubContents)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_PLAY, &CVideoSelectDlg::OnBnClickedButtonPreviewPlay)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_STOP, &CVideoSelectDlg::OnBnClickedButtonPreviewStop)
	ON_CBN_SELCHANGE(IDC_CB_CONTENTS, &CVideoSelectDlg::OnCbnSelchangeCbContents)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CVideoSelectDlg 메시지 처리기입니다.

void CVideoSelectDlg::SetData(CString strContentsId, CString strValue)
{
	int nIndex = -1;
	CString strFullPath;
	for(int i=0; i<m_cbContents.GetCount() ;i++)
	{
		CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_cbContents.GetItemData(i);
		if(!pContentsInfo) continue;

		if(pContentsInfo->strId == strContentsId)
		{
			nIndex = i;
			strFullPath = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
			break;
		}
	}

	m_cbContents.SetCurSel(nIndex);

	OnBnClickedButtonPreviewStop();
}

void CVideoSelectDlg::GetData(CString& strContentsId, CString& strValue)
{
	strContentsId = "";
	strValue = "";

	if(m_cbContents.GetCurSel() < 0)
	{
		return;
	}

	CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_cbContents.GetItemData(m_cbContents.GetCurSel());
	if(pContentsInfo)
	{
		strContentsId = pContentsInfo->strId;
		strValue      = pContentsInfo->strFilename;
	}
}

void CVideoSelectDlg::SetPreviewMode(bool bPreviewMode)
{
	CWizardSubContents::SetPreviewMode(bPreviewMode);
	m_cbContents.EnableWindow(!bPreviewMode);
}

BOOL CVideoSelectDlg::OnInitDialog()
{
	CWizardSubContents::OnInitDialog();

	CRect rcPreview;
	m_framePreview.GetWindowRect(rcPreview);
	rcPreview.DeflateRect(2,2);
	ScreenToClient(rcPreview);

	m_wndVideo.Create(NULL, "", WS_CHILD, rcPreview, this, 0xfeff);
	m_wndVideo.ShowWindow(SW_SHOW);

	m_cbContents.ResetContent();

	CONTENTS_INFO_MAP* contents_map = GetDocument()->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strContentsId;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strContentsId, (void*&)pContentsInfo );

		if(pContentsInfo && pContentsInfo->nContentsType == GetContentsType())
		{
			int nIndex = m_cbContents.AddString(pContentsInfo->strContentsName);
			m_cbContents.SetItemData(nIndex, (DWORD_PTR)pContentsInfo);
		}
	}

	m_btnPreviewPlay.LoadBitmap(IDB_BTN_PLAY, RGB(255, 255, 255));
	m_btnPreviewStop.LoadBitmap(IDB_BTN_STOP, RGB(255, 255, 255));

	m_btnPreviewPlay.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT002));
	m_btnPreviewStop.SetToolTipText(LoadStringById(IDS_VIDEOCONTENTSDIALOG_BUT003));

	BOOL bIsPreview = TRUE;

	m_btnPreviewPlay.EnableWindow(bIsPreview);
	m_btnPreviewPlay.ShowWindow(bIsPreview);
	m_btnPreviewStop.EnableWindow(!bIsPreview);
	m_btnPreviewStop.ShowWindow(!bIsPreview);

	m_reposControl.AddControl(&m_cbContents    , REPOS_FIX , REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnPreviewPlay, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnPreviewStop, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_wndVideo      , REPOS_FIX , REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_framePreview  , REPOS_FIX , REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	
	m_reposControl.Move();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CVideoSelectDlg::OnBnClickedButtonPreviewPlay()
{
	CString strFullPath;
	if(m_cbContents.GetCurSel() >= 0)
	{
		CONTENTS_INFO* pContentsInfo = (CONTENTS_INFO*)m_cbContents.GetItemData(m_cbContents.GetCurSel());
		if(pContentsInfo)
		{
			strFullPath = pContentsInfo->strLocalLocation + pContentsInfo->strFilename;
		}
	}

	BOOL bIsPreview = LoadContents(strFullPath);

	m_btnPreviewPlay.EnableWindow(!bIsPreview);
	m_btnPreviewPlay.ShowWindow(!bIsPreview);
	m_btnPreviewStop.EnableWindow(bIsPreview);
	m_btnPreviewStop.ShowWindow(bIsPreview);
}

void CVideoSelectDlg::OnBnClickedButtonPreviewStop()
{
	m_wndVideo.CloseFile();
	m_wndVideo.Invalidate();

	BOOL bIsPreview = TRUE;

	m_btnPreviewPlay.EnableWindow(bIsPreview);
	m_btnPreviewPlay.ShowWindow(bIsPreview);
	m_btnPreviewStop.EnableWindow(!bIsPreview);
	m_btnPreviewStop.ShowWindow(!bIsPreview);
}

void CVideoSelectDlg::OnCbnSelchangeCbContents()
{
	OnBnClickedButtonPreviewStop();
}

bool CVideoSelectDlg::LoadContents(LPCTSTR lpszFullPath)
{
	m_wndVideo.CloseFile();

	bool bResult = false;
	m_wndVideo.m_strMediaFullPath = lpszFullPath;
	if(IsExistFile(lpszFullPath) && m_wndVideo.OpenFile(1))
	{
		m_wndVideo.Play();
//		m_wndVideo.Stop(false);
		bResult = true;
	}

	m_wndVideo.Invalidate();

	return bResult;
}

void CVideoSelectDlg::OnSize(UINT nType, int cx, int cy)
{
	CWizardSubContents::OnSize(nType, cx, cy);

	m_reposControl.Move();

	if(m_wndVideo.GetSafeHwnd() && m_wndVideo.m_pInterface != NULL)
	{
		CRect rect;
		m_wndVideo.GetClientRect(rect);
		m_wndVideo.m_pInterface->SetVideoRect(rect);
	}
}
