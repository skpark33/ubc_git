// EditHostDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "EditHostDlg.h"
#include "common\ubcdefine.h"

// CEditHostDlg 대화 상자입니다.
IMPLEMENT_DYNAMIC(CEditHostDlg, CDialog)

CEditHostDlg::CEditHostDlg(CWnd* pParent, CString strUsetCreate, CStringArray& aryHostName)
	: CDialog(CEditHostDlg::IDD, pParent)
	, m_strHostID1(_T(""))
	, m_strHostID2(_T(""))
	, m_strFTPPort(_T("14005"))
	, m_strSvrPort(_T("14007"))
	, m_strIPAddr(_T(""))
	, m_strShutdownTime(_T(""))
//	, m_strHours(_T(""))
//	, m_strMinutes(_T(""))
	, m_strOldHostName(_T(""))
	, m_strMacAddress(_T(""))
	, m_nMonitorCnt(1)
	, m_bShutdownAuto(FALSE)
	, m_bShutdownWeekday(FALSE)
{
	m_strUserCreate = strUsetCreate;
	m_aryHostName.Copy(aryHostName);
}

CEditHostDlg::~CEditHostDlg()
{
}

void CEditHostDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_HOST_NAME_EDT, m_strHostID1);
	DDX_Text(pDX, IDC_HOST_NAME_EDT2, m_strHostID2);
	DDX_Control(pDX, IDC_IP_ADDR, m_addrIP);
	DDX_Text(pDX, IDC_FTP_PORT_EDT, m_strFTPPort);
	DDX_Text(pDX, IDC_SVR_PORT_EDT, m_strSvrPort);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//	DDX_Text(pDX, IDC_HOUR_EDT, m_strHours);
	//	DDX_Text(pDX, IDC_MIN_EDT, m_strMinutes);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN, m_tmShutdown);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK0, m_tmShutdownWeek[0]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK1, m_tmShutdownWeek[1]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK2, m_tmShutdownWeek[2]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK3, m_tmShutdownWeek[3]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK4, m_tmShutdownWeek[4]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK5, m_tmShutdownWeek[5]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK6, m_tmShutdownWeek[6]);
	DDX_Control(pDX, IDC_TIME_POWERON, m_tmPoweron);
	DDX_Control(pDX, IDC_CHECK_SHUTDOWN, m_ckShutdown);
	DDX_Control(pDX, IDC_CHECK_POWERON, m_ckPoweron);
	DDX_Text(pDX, IDC_SVR_MACADDR, m_strMacAddress);
	DDX_Text(pDX, IDC_SVR_MONITCNT, m_nMonitorCnt);
	DDX_Text(pDX, IDC_DESCRIPT, m_szDesc);

	DDX_Control(pDX, IDC_CHECK_WEEK0, m_ckWeek[0]);
	DDX_Control(pDX, IDC_CHECK_WEEK1, m_ckWeek[1]);
	DDX_Control(pDX, IDC_CHECK_WEEK2, m_ckWeek[2]);
	DDX_Control(pDX, IDC_CHECK_WEEK3, m_ckWeek[3]);
	DDX_Control(pDX, IDC_CHECK_WEEK4, m_ckWeek[4]);
	DDX_Control(pDX, IDC_CHECK_WEEK5, m_ckWeek[5]);
	DDX_Control(pDX, IDC_CHECK_WEEK6, m_ckWeek[6]);
	DDX_Control(pDX, IDC_LIST_HOLIDAY, m_lscHoliday);
	DDX_Control(pDX, IDC_MONTHCALENDAR2, m_ctrCalendar);
	DDX_Control(pDX, IDC_KB_SHUTDOWN_AUTO, m_kbShutdownAuto);
	DDX_Control(pDX, IDC_KB_WEEKDAY_SHUTDOWN, m_kbShutdownWeekday);
	DDX_Control(pDX, IDC_CHECK_WEEKDAY1, m_ckW1);
	DDX_Control(pDX, IDC_CHECK_WEEKDAY2, m_ckW2);
	DDX_Control(pDX, IDC_CHECK_WEEKDAY3, m_ckW3);
	DDX_Control(pDX, IDC_CHECK_WEEKDAY4, m_ckW4);
	DDX_Control(pDX, IDC_CHECK_WEEKDAY5, m_ckW5);
	DDX_Control(pDX, IDC_CHECK_WEEKDAY6, m_ckW6);
	DDX_Control(pDX, IDC_CHECK_WEEKDAY0, m_ckW0);
}

BEGIN_MESSAGE_MAP(CEditHostDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CEditHostDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CEditHostDlg::OnBnClickedCancel)\
	ON_BN_CLICKED(IDC_CHECK_SHUTDOWN, &CEditHostDlg::OnBnClickedCheckShutdown)
	ON_BN_CLICKED(IDC_CHECK_POWERON, &CEditHostDlg::OnBnClickedCheckPoweron)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CEditHostDlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CEditHostDlg::OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_CHECK_WEEK6, &CEditHostDlg::OnBnClickedCheckWeek6)
	ON_BN_CLICKED(IDC_CHECK_WEEK0, &CEditHostDlg::OnBnClickedCheckWeek0)
	ON_BN_CLICKED(IDC_CHECK_WEEK1, &CEditHostDlg::OnBnClickedCheckWeek1)
	ON_BN_CLICKED(IDC_CHECK_WEEK2, &CEditHostDlg::OnBnClickedCheckWeek2)
	ON_BN_CLICKED(IDC_CHECK_WEEK3, &CEditHostDlg::OnBnClickedCheckWeek3)
	ON_BN_CLICKED(IDC_CHECK_WEEK4, &CEditHostDlg::OnBnClickedCheckWeek4)
	ON_BN_CLICKED(IDC_CHECK_WEEK5, &CEditHostDlg::OnBnClickedCheckWeek5)
	ON_BN_CLICKED(IDC_KB_SHUTDOWN_AUTO, &CEditHostDlg::OnBnClickedKbShutdownAuto)
	ON_BN_CLICKED(IDC_KB_WEEKDAY_SHUTDOWN, &CEditHostDlg::OnBnClickedKbWeekdayShutdown)
END_MESSAGE_MAP()

// CEditHostDlg 메시지 처리기입니다.
BOOL CEditHostDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btnOK.LoadBitmap(IDB_BTN_APPLY, RGB(255, 255, 255));
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_btnOK.SetToolTipText(LoadStringById(IDS_EDITHOSTDLG_BUT001));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_EDITHOSTDLG_BUT002));

	((CEdit*)GetDlgItem(IDC_DESCRIPT))->SetLimitText(50);

	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_FTP_PORT_EDT);
	pEdit->SetLimitText(5);
	pEdit = (CEdit*)GetDlgItem(IDC_SVR_PORT_EDT);
	pEdit->SetLimitText(5);
	pEdit = (CEdit*)GetDlgItem(IDC_HOUR_EDT);
	pEdit->SetLimitText(2);
	pEdit = (CEdit*)GetDlgItem(IDC_MIN_EDT);
	pEdit->SetLimitText(2);

	((CEdit*)GetDlgItem(IDC_SVR_MONITCNT))->SetLimitText(1);

	m_lscHoliday.SetExtendedStyle(m_lscHoliday.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lscHoliday.InsertColumn(0, "", LVCFMT_CENTER, 10);
	m_lscHoliday.InsertColumn(1, LoadStringById(IDS_EDITHOSTDLG_LST001), LVCFMT_CENTER, 90);
	m_lscHoliday.InitHeader(IDB_LIST_HEADER);

	m_tmShutdown.SetFormat(" HH : mm");
	for(int i=0; i<7 ;i++)
	{
		m_tmShutdownWeek[i].SetFormat(" HH : mm");
	}
	m_tmPoweron.SetFormat(" HH : mm");

	m_strOldHostName = m_strHostName;
	int nPos = m_strHostName.ReverseFind('-');
	if(nPos >= 0)
	{
		m_strHostID1 = m_strHostName.Left(nPos);
		m_strHostID2 = m_strHostName.Mid(nPos+1);
	}
	else
	{
		m_strHostID1 = m_strHostName;
	}

	if(m_strIPAddr != ""){
		m_addrIP.SetWindowText(m_strIPAddr);
	}

	bool bShutdown = false;
	if(m_strShutdownTime != "")
	{
		int iStart = 0;
		CString szToken = m_strShutdownTime.Tokenize(EH_SEPARATOR, iStart);
		while(!szToken.IsEmpty())
		{
			if(szToken.Find(":") >= 0)
			{
				int iPos = 0;
				CString szHour = szToken.Tokenize(":", iPos);
				CString szMin = szToken.Tokenize(":", iPos);
				CTime tmShut(2000, 1, 1, atoi(szHour), atoi(szMin), 0);
				m_tmShutdown.SetTime(&tmShut);
				bShutdown = true;

				for(int i=0; i<7 ;i++)
				{
					m_tmShutdownWeek[i].SetTime(&tmShut);
				}
			}
			else if(szToken.Find(".") >=0)
			{
				AddHollyday(szToken);
			}
			else if(szToken.GetLength() == 1)
			{
				int n = atoi(szToken);
				switch(n){
					case 0 : m_ckW0.SetCheck(true); break;
					case 1 : m_ckW1.SetCheck(true); break;
					case 2 : m_ckW2.SetCheck(true); break;
					case 3 : m_ckW3.SetCheck(true); break;
					case 4 : m_ckW4.SetCheck(true); break;
					case 5 : m_ckW5.SetCheck(true); break;
					case 6 : m_ckW6.SetCheck(true); break;
				}
				//if(0 <= n && n <= 6) {
				//	m_ckWeek[n].SetCheck(true);
				//}
			}
			szToken = m_strShutdownTime.Tokenize(EH_SEPARATOR, iStart);
		}
	}

	m_strWeekShutdownTime.Replace(_T("("), _T(""));
	m_strWeekShutdownTime.Replace(_T(")"), _T(""));
	if(!m_strWeekShutdownTime.IsEmpty())
	{
		int iStart = 0;
		CString szToken;
		while(!(szToken = m_strWeekShutdownTime.Tokenize(_T(","), iStart)).IsEmpty())
		{
			if(szToken.Find(_T("|")) >= 0)
			{
				int nDayOfWeek = _ttoi(szToken.Left(szToken.Find(_T("|"))));

				szToken = szToken.Mid(szToken.Find(_T("|"))+1);

				if(szToken.Find(":") >= 0)
				{
					int iPos = 0;
					CTime tmShut(2000, 1, 1
							   , _ttoi(szToken.Left(szToken.Find(_T(":"))))
							   , _ttoi(szToken.Mid (szToken.Find(_T(":"))+1))
							   , 0
							   );

					m_tmShutdownWeek[nDayOfWeek].SetTime(&tmShut);
					m_ckWeek[nDayOfWeek].SetCheck(true);
				}
			}
		}
	}

	m_ckShutdown.SetCheck(bShutdown);
	m_tmShutdown.EnableWindow(bShutdown);
	InitDateRange(m_tmShutdown);

	for(int i=0; i<7 ; i++)
	{
		m_tmShutdownWeek[i].EnableWindow(m_ckWeek[i].GetCheck());
		InitDateRange(m_tmShutdownWeek[i]);
	}

	m_tmPoweron.EnableWindow(FALSE);

	m_kbShutdownAuto.SetCheck(m_bShutdownAuto);
	m_kbShutdownWeekday.SetCheck(m_bShutdownWeekday);

	InitShutdownAuto(m_kbShutdownAuto.GetCheck());
	InitShutdownWeekday(m_kbShutdownWeekday.GetCheck());

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CEditHostDlg::OnBnClickedCheckShutdown()
{
	m_tmShutdown.EnableWindow(m_ckShutdown.GetCheck());
}

void CEditHostDlg::OnBnClickedCheckPoweron()
{
	m_tmPoweron.EnableWindow(m_ckPoweron.GetCheck());
}

void CEditHostDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	m_addrIP.GetWindowText(m_strIPAddr);
//	m_strShutdownTime = m_strHours;
//	m_strShutdownTime.TrimLeft();
//	m_strShutdownTime.TrimRight();
//	m_strShutdownTime.Append(":");
//	m_strShutdownTime.Append(m_strMinutes);

	m_bShutdownAuto = m_kbShutdownAuto.GetCheck();
	m_bShutdownWeekday = m_kbShutdownWeekday.GetCheck();

	m_strShutdownTime.Empty();

	if(m_bShutdownAuto)
	{
		if(m_ckShutdown.GetCheck())
		{
			CTime tmShut;
			m_tmShutdown.GetTime(tmShut);
			m_strShutdownTime.Format("%02d:%02d", tmShut.GetHour(), tmShut.GetMinute());
		}
		
		if(m_ckW1.GetCheck()){
			//if(!m_strShutdownTime.IsEmpty()){ 
				m_strShutdownTime += EH_SEPARATOR;
			//}
			m_strShutdownTime += "1";
		}
		if(m_ckW2.GetCheck()){
			//if(!m_strShutdownTime.IsEmpty()){ 
				m_strShutdownTime += EH_SEPARATOR;
			//}
			m_strShutdownTime += "2";
		}
		if(m_ckW3.GetCheck()){
			//if(!m_strShutdownTime.IsEmpty()){ 
				m_strShutdownTime += EH_SEPARATOR;
			//}
			m_strShutdownTime += "3";
		}
		if(m_ckW4.GetCheck()){
			//if(!m_strShutdownTime.IsEmpty()){ 
				m_strShutdownTime += EH_SEPARATOR;
			//}
			m_strShutdownTime += "4";
		}
		if(m_ckW5.GetCheck()){
			//if(!m_strShutdownTime.IsEmpty()){ 
				m_strShutdownTime += EH_SEPARATOR;
			//}
			m_strShutdownTime += "5";
		}
		if(m_ckW6.GetCheck()){
			//if(!m_strShutdownTime.IsEmpty()){ 
				m_strShutdownTime += EH_SEPARATOR;
			//}
			m_strShutdownTime += "6";
		}
		if(m_ckW0.GetCheck()){
			if(!m_strShutdownTime.IsEmpty()){ 
				m_strShutdownTime += EH_SEPARATOR;
			}
			m_strShutdownTime += "0";
		}


		for(int nRow = 0; nRow < m_lscHoliday.GetItemCount(); nRow++)
		{
			CString szBuf = m_lscHoliday.GetItemText(nRow, 1);
			m_strShutdownTime += EH_SEPARATOR;
			m_strShutdownTime += szBuf;
		}
	}

	if(m_bShutdownWeekday)
	{
		for(int i = 0; i < 7; i++)
		{
			if(m_ckWeek[i].GetCheck())
			{
				m_strShutdownTime += EH_SEPARATOR;
				m_strShutdownTime += ToString(i);
			}
		}

		m_strWeekShutdownTime.Empty();

		for(int i=0; i<7 ;i++)
		{
			if(m_ckWeek[i].GetCheck())
			{
				CTime tmShut;
				m_tmShutdownWeek[i].GetTime(tmShut);
				CString strTmp;
				strTmp.Format("%d|%02d:%02d", i, tmShut.GetHour(), tmShut.GetMinute());
				m_strWeekShutdownTime += (m_strWeekShutdownTime.IsEmpty() ? _T("") : _T(",")) + strTmp;
			}
		}

		if(!m_strWeekShutdownTime.IsEmpty())
		{
			m_strWeekShutdownTime = _T("(") + m_strWeekShutdownTime + _T(")");
		}
	}

	m_strHostID1.Trim();
	m_strHostID2.Trim();

	if(m_strHostID1.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_EDITHOSTDLG_MSG001), MB_ICONSTOP);
		GetDlgItem(IDC_HOST_NAME_EDT)->SetFocus();
		return;
	}

	if(m_strHostID2.GetLength() != 5)
	{
		UbcMessageBox(LoadStringById(IDS_EDITHOSTDLG_MSG006), MB_ICONSTOP);
		GetDlgItem(IDC_HOST_NAME_EDT2)->SetFocus();
		return;
	}

	//유효성 체크
	for(int i=0; i<m_strHostID2.GetLength() ;i++)
	{
		if(m_strHostID2[i] < '0' || m_strHostID2[i] > '9')
		{
			UbcMessageBox(LoadStringById(IDS_EDITHOSTDLG_MSG006), MB_ICONSTOP);
			GetDlgItem(IDC_HOST_NAME_EDT2)->SetFocus();
			return;
		}
	}

	m_strHostName = m_strHostID1 + _T("-") + m_strHostID2;

	// 중복체크
	if(m_strHostName != m_strOldHostName)
	{
		for(int i=0; i<m_aryHostName.GetCount(); i++)
		{
			if(m_strHostName == m_aryHostName.GetAt(i))
			{
				UbcMessageBox(LoadStringById(IDS_EDITHOSTDLG_MSG002), MB_ICONSTOP);
				return;
			}
		}
	}

	if(m_strIPAddr == "" || m_strIPAddr == "0.0.0.0")
	{
		UbcMessageBox(LoadStringById(IDS_EDITHOSTDLG_MSG003), MB_ICONSTOP);
		return;
	}

	if(m_strFTPPort == "" || atoi(m_strFTPPort) == 0)
	{
		UbcMessageBox(LoadStringById(IDS_EDITHOSTDLG_MSG004), MB_ICONSTOP);
		return;
	}

	if(m_strSvrPort == "" || atoi(m_strSvrPort) == 0)
	{
		UbcMessageBox(LoadStringById(IDS_EDITHOSTDLG_MSG005), MB_ICONSTOP);
		return;
	}

	OnOK();
}

void CEditHostDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CEditHostDlg::OnBnClickedButtonAdd()
{
	SYSTEMTIME st;
	m_ctrCalendar.GetCurSel(&st);

	CString str;
	str.Format("%d.%02d", st.wMonth, st.wDay);

	for(int nRow = 0; nRow < m_lscHoliday.GetItemCount(); nRow++){
		CString szBuf = m_lscHoliday.GetItemText(nRow, 1);
		if(szBuf == str)	return;
	}

	AddHollyday(str);
}

void CEditHostDlg::OnBnClickedButtonDel()
{
	POSITION pos = m_lscHoliday.GetFirstSelectedItemPosition();
	while(pos){
		int nRow = m_lscHoliday.GetNextSelectedItem(pos);
		m_lscHoliday.DeleteItem(nRow);
	}
}

void CEditHostDlg::AddHollyday(CString str)
{
	int nRow = m_lscHoliday.GetItemCount();
	m_lscHoliday.InsertItem(nRow, "");
	m_lscHoliday.SetItemText(nRow, 1, str);
	m_lscHoliday.SetItemData(nRow, nRow);

	m_lscHoliday.SortItems(CompareSite, (DWORD_PTR)this);
}

int CEditHostDlg::CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	LVFINDINFO lvFind;
	lvFind.flags = LVFI_PARAM;

	lvFind.lParam = lParam1;
	int nIndex1 = ((CEditHostDlg*)lParam)->m_lscHoliday.FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = ((CEditHostDlg*)lParam)->m_lscHoliday.FindItem(&lvFind);

	int nCol = 1;
	CString strItem1 = ((CEditHostDlg*)lParam)->m_lscHoliday.GetItemText(nIndex1, nCol);
	CString strItem2 = ((CEditHostDlg*)lParam)->m_lscHoliday.GetItemText(nIndex2, nCol);
	return strItem1.Compare(strItem2);
}

void CEditHostDlg::InitShutdownWeek(int nIndex)
{
	m_tmShutdownWeek[nIndex].EnableWindow(m_ckWeek[nIndex].GetCheck());
	if(m_ckWeek[nIndex].GetCheck() && m_ckShutdown.GetCheck())
	{
		CTime tmShut;
		m_tmShutdown.GetTime(tmShut);
		m_tmShutdownWeek[nIndex].SetTime(&tmShut);
	}
}

void CEditHostDlg::OnBnClickedCheckWeek0()
{
	InitShutdownWeek(0);
}

void CEditHostDlg::OnBnClickedCheckWeek1()
{
	InitShutdownWeek(1);
}

void CEditHostDlg::OnBnClickedCheckWeek2()
{
	InitShutdownWeek(2);
}

void CEditHostDlg::OnBnClickedCheckWeek3()
{
	InitShutdownWeek(3);
}

void CEditHostDlg::OnBnClickedCheckWeek4()
{
	InitShutdownWeek(4);
}

void CEditHostDlg::OnBnClickedCheckWeek5()
{
	InitShutdownWeek(5);
}

void CEditHostDlg::OnBnClickedCheckWeek6()
{
	InitShutdownWeek(6);
}

void CEditHostDlg::OnBnClickedKbShutdownAuto()
{
	InitShutdownAuto(m_kbShutdownAuto.GetCheck());
}

void CEditHostDlg::OnBnClickedKbWeekdayShutdown()
{
	InitShutdownWeekday(m_kbShutdownWeekday.GetCheck());
}

void CEditHostDlg::InitShutdownAuto(BOOL bEnable)
{
	m_ckShutdown.EnableWindow(bEnable);
	m_tmShutdown.EnableWindow(bEnable && m_ckShutdown.GetCheck());

	m_ckW1.EnableWindow(bEnable);
	m_ckW2.EnableWindow(bEnable);
	m_ckW3.EnableWindow(bEnable);
	m_ckW4.EnableWindow(bEnable);
	m_ckW5.EnableWindow(bEnable);
	m_ckW6.EnableWindow(bEnable);
	m_ckW0.EnableWindow(bEnable);
	
	m_ctrCalendar.EnableWindow(bEnable);
	GetDlgItem(IDC_BUTTON_ADD)->EnableWindow(bEnable);
	GetDlgItem(IDC_BUTTON_DEL)->EnableWindow(bEnable);
	m_lscHoliday.EnableWindow(bEnable);
}

void CEditHostDlg::InitShutdownWeekday(BOOL bEnable)
{
	for(int i=0; i<7 ;i++)
	{
		m_ckWeek[i].EnableWindow(bEnable);
		if(bEnable) InitShutdownWeek(i);
		else        m_tmShutdownWeek[i].EnableWindow(FALSE);
	}
}