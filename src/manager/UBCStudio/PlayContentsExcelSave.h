#pragma once

#include "UBCManager/Excel/Excel.h"
#include "common\utblistctrlex.h"

class CPlayContentsSave
{
public:
	CPlayContentsSave(void);
	virtual ~CPlayContentsSave(void);

	CString	Save(CString strSheetName, CUTBListCtrlEx& lcList);
private:
	_Application m_excelApp;	// m_excelApp is the Excel _Application object
	_Workbook   m_book;

	BOOL	SaveDataList(short nSheet, CString strSheetName, CUTBListCtrlEx& lcList);
	CString GetCell(int nCol, int nRow);
};
