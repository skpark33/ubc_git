#pragma once

#include "afxwin.h"
#include "afxdtctl.h"
#include "afxcmn.h"
#include "enviroment.h"

#include "Browser\Frame.h"
#include "Browser\Schedule.h"

#include "common\hoverbutton.h"
#include "common\utblistctrlex.h"
#include "common\colorpickercb.h"
#include "common\fontpreviewcombo.h"
#include "common\libCximage\ximage.h"

#define STR_ANNOFOLDER	"announce"

// CAnnounceDlg dialog
class CAnnounceDlg : public CDialog
{
	DECLARE_DYNAMIC(CAnnounceDlg)

private:
	void InitPreview();
	void EnableAllItems(BOOL bEnable);
public:
private:
	CString m_szTitle;
	CString m_szFile;
	CHoverButton m_btFile;
	CHoverButton m_btPlay;
	CHoverButton m_btStop;
	CDateTimeCtrl m_dtcStartDateTime;
	CDateTimeCtrl m_dtcEndDateTime;
	CComboBox m_cbPosition;
	CString m_szHeight;
	CSpinButtonCtrl m_spHeight;
	CFontPreviewCombo m_cbFont;
	CString m_szFontSize;
	CSpinButtonCtrl m_spFontSize;
	CColorPickerCB m_cbFGColor;
	CColorPickerCB m_cbBGColor;
	CSliderCtrl m_sldSpeed;
	CString m_szAlpa;
	CSpinButtonCtrl m_spAlpa;
	CString m_szComment;

	CxImage m_imBack;
	CHorizontalTickerSchedule	m_wndTicker;

	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
public:
	CAnnounceDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAnnounceDlg();

// Dialog Data
	enum { IDD = IDD_ANNOUNCEPROP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnChangeEditComment();
	afx_msg void OnBnClickedButtonFile();
	afx_msg void OnBnClickedButtonPlay();
	afx_msg void OnBnClickedButtonStop();
};
