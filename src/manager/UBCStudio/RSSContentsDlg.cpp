// RSSContentsDlg.cpp : implementation file
//
#include "stdafx.h"
#include "RSSContentsDlg.h"
#include "Enviroment.h"
#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "common/MD5Util.h"

// CRSSContentsDlg dialog
IMPLEMENT_DYNAMIC(CRSSContentsDlg, CSubContentsDialog)

CRSSContentsDlg::CRSSContentsDlg(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CRSSContentsDlg::IDD, pParent)
	, m_wndRss (NULL, 0,false)
	, m_strTopicFilter(_T(""))
	, m_strErrText(_T(""))
	, m_nRssType(0)
	, m_cbxContentsFont(IDB_TTF_BMP)
{
	m_strLocation = _T("");
}

CRSSContentsDlg::~CRSSContentsDlg()
{
}

void CRSSContentsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_URL, m_editURL);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Control(pDX, IDC_BUTTON_PREVIEW_PLAY, m_btnPreviewPlay);
	DDX_Control(pDX, IDC_BUTTON_FILENAME, m_btnBrowserContentsFile);
	DDX_Control(pDX, IDC_BUTTON_DELFILE, m_btnDelFile);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);

	DDX_Control(pDX, IDC_COMBO_CONTENTS_FONT, m_cbxContentsFont);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FONT_SIZE, m_editContentsFontSize);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_TEXT_COLOR, m_cbxContentsTextColor);
	DDX_Control(pDX, IDC_COMBO_CONTENTS_BG_COLOR, m_cbxContentsBGColor);
	DDX_Control(pDX, IDC_EDIT_HEIGHT, m_edtHeight);
	DDX_Control(pDX, IDC_SPIN_HEIGHT, m_spnHeight);
	DDX_Control(pDX, IDC_EDIT_WIDTH, m_edtWidth);
	DDX_Control(pDX, IDC_SPIN_WIDTH, m_spnWidth);
	DDX_Control(pDX, IDC_EDIT_LINENUM, m_edtLineNum);
	DDX_Control(pDX, IDC_SPIN_LINENUM, m_spnLineNum);
	DDX_Text(pDX, IDC_EDIT_RSS_TOPIC_FILTER, m_strTopicFilter);
	DDX_Text(pDX, IDC_EDIT_ERR_TEXT, m_strErrText);
	DDX_Radio(pDX, IDC_RB_RSS_TYPE1, m_nRssType);
}

BEGIN_MESSAGE_MAP(CRSSContentsDlg, CSubContentsDialog)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_FILENAME, OnBnClickedFileName)
	ON_BN_CLICKED(IDC_BUTTON_PREVIEW_PLAY, OnBnClickedButtonPreviewPlay)
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, OnBnClickedPermanentCheck)
	ON_BN_CLICKED(IDC_BUTTON_DELFILE, OnBnClickedDeletefile)
	ON_BN_CLICKED(IDC_RB_RSS_TYPE1, &CRSSContentsDlg::OnBnClickedRbRssType1)
	ON_BN_CLICKED(IDC_RB_RSS_TYPE2, &CRSSContentsDlg::OnBnClickedRbRssType2)
	ON_BN_CLICKED(IDC_RB_RSS_TYPE3, &CRSSContentsDlg::OnBnClickedRbRssType3)
END_MESSAGE_MAP()

// CRSSContentsDlg message handlers
BOOL CRSSContentsDlg::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_editContentsName.SetValue("");
	m_editContentsFileName.SetValue("");
	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(15);
	m_editURL.SetValue("http://");
	m_editContentsFontSize.SetValue(45);
	
	m_cbxContentsTextColor.InitializeDefaultColors();;
	m_cbxContentsBGColor.InitializeDefaultColors();;
	
	m_cbxContentsTextColor.SetSelectedColorValue(RGB(0,0,0));
	m_cbxContentsBGColor.SetSelectedColorValue(RGB(255,255,255));

	m_edtHeight.SetValue(100);
	m_spnHeight.SetRange(0, 999);
	m_edtWidth.SetValue(300);
	m_spnWidth.SetRange(0, 999);
	m_edtLineNum.SetValue(10);
	m_spnLineNum.SetRange(0, 100);

	m_cbxContentsFont.SetPreviewStyle (CFontPreviewCombo::NAME_ONLY, false);
	m_cbxContentsFont.SetFontHeight (12, true);

	m_btnBrowserContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btnBrowserContentsFile.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT002));

	m_btnDelFile.LoadBitmap(IDB_BTN_DELFILE, RGB(236, 233, 216));
	m_btnDelFile.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT003));
	
	m_btnPreviewPlay.LoadBitmap(IDB_BTN_PLAY, RGB(255, 255, 255));
	m_btnPreviewPlay.SetToolTipText(LoadStringById(IDS_WEBCONTENTSDIALOG_BUT001));

	m_edtLineNum.EnableWindow(m_nRssType == 1);

	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);

	m_wndRss.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}//if

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CRSSContentsDlg::OnDestroy()
{
	CSubContentsDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CRSSContentsDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CSubContentsDialog::OnClose();
}

void CRSSContentsDlg::Stop()
{
	m_wndRss.Stop(false);
}

bool CRSSContentsDlg::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData();

	info.nContentsType = GetContentsType();
	info.strContentsName = m_editContentsName.GetValueString();
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();
	info.nFontSize = m_editContentsFontSize.GetValueInt();
	m_cbxContentsFont.GetLBText(m_cbxContentsFont.GetCurSel(), info.strFont);

	CString szTextColor = ::GetColorFromString(m_cbxContentsTextColor.GetSelectedColorValue());
	szTextColor.Replace("#", "");
	info.strFgColor = szTextColor;

	CString szBgColor = ::GetColorFromString(m_cbxContentsBGColor.GetSelectedColorValue());
	szBgColor.Replace("#", "");
	info.strBgColor = szBgColor;
	
	info.strFilename = m_editContentsFileName.GetValueString();
	
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	info.strLocalLocation = m_strLocation;
	info.strComment[0] = m_editURL.GetValueString();
	
	info.nHeight = m_edtHeight.GetValueInt();
	info.nWidth = m_edtWidth.GetValueInt();
//	info.nCurrentComment = m_edtLineNum.GetValueInt();

	// RSS 관련 수정 (0000918, 0000875, 0000872, 0000871, 0000869)
	info.strComment[2] = ::ToString(m_nRssType);		// 종류
	info.strComment[3] = m_strErrText;					// 장애 텍스트
	info.strComment[4] = m_strTopicFilter;				// 토픽 필터
	info.strComment[5] = (m_nRssType == 1 ? m_edtLineNum.GetValueString() : ""); // 라인수

	if(!IsFitContentsName(info.strContentsName)) return false;
	if(info.nRunningTime == 0){
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
		return false;
	}
	if(info.strComment[0].IsEmpty()){
		UbcMessageBox(LoadStringById(IDS_WEBCONTENTSDIALOG_MSG001), MB_ICONSTOP);
		return false;
	}

	if(info.strComment[0].GetLength() < 10){
		UbcMessageBox(LoadStringById(IDS_WEBCONTENTSDIALOG_MSG002), MB_ICONSTOP);
		return false;
	}

	if(m_nRssType == 1 && m_edtLineNum.GetValueInt() < 1)
	{
		UbcMessageBox(LoadStringById(IDS_RSSCONTENTSDIALOG_MSG001), MB_ICONSTOP);
		return false;
	}

	return true;
}

bool CRSSContentsDlg::SetContentsInfo(CONTENTS_INFO& info)
{
	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	m_editContentsName.SetValue(info.strContentsName);
	m_editContentsFontSize.SetValue(info.nFontSize);
	m_editContentsFileName.SetValue(info.strFilename);

	m_edtHeight.SetValue(info.nHeight);
	m_edtWidth.SetValue(info.nWidth);

	// RSS 관련 수정 (0000918, 0000875, 0000872, 0000871, 0000869)
	m_nRssType = _ttoi(info.strComment[2]);		// 종류
	m_strErrText = info.strComment[3];			// 장애 텍스트
	m_strTopicFilter = info.strComment[4];		// 토픽 필터
	m_edtLineNum.SetValue((m_nRssType == 1 ? _ttoi(info.strComment[5]) : 0));
	m_edtLineNum.EnableWindow(m_nRssType == 1);

	if(info.strComment[0].IsEmpty()){
		m_editURL.SetValue("http://");
	}else{
		CString szUrl = "";
		if(info.strComment[0].Left(7) != _T("http://") && info.strComment[0].Left(8) != _T("https://"))
			szUrl = _T("http://");
		szUrl += info.strComment[0];
		m_editURL.SetValue(szUrl);
	}

	if(info.strFilename.IsEmpty()){
		m_strLocation = _T("");
		info.nFilesize = 0;
	}else{
		m_strLocation = info.strLocalLocation;
	}
	
	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}

	for(int i=0; i<m_cbxContentsFont.GetCount(); i++)
	{
		CString font_name;
		m_cbxContentsFont.GetLBText(i,font_name);
		if(font_name == info.strFont)
		{
			m_cbxContentsFont.SetCurSel(i);
			break;
		}
	}

	m_cbxContentsTextColor.SetSelectedColorValue(::GetColorFromString(info.strFgColor));
	m_cbxContentsBGColor.SetSelectedColorValue(::GetColorFromString(info.strBgColor));

	UpdateData(FALSE);

	return true;
}

void CRSSContentsDlg::SetPlayContents()
{
	UpdateData();

	m_wndRss.CloseFile();

	m_wndRss.m_contentsName = m_editContentsName.GetValueString();
	m_wndRss.m_filename = m_editContentsFileName.GetValueString();
	m_wndRss.m_location = m_strLocation;
	m_wndRss.m_strMediaFullPath = m_wndRss.m_location.c_str();
	m_wndRss.m_strMediaFullPath += m_editContentsFileName.GetValueString();

	CString szURL;
	CString szFont;
	CString szTextColor, szBgColor;

	m_cbxContentsFont.GetLBText(m_cbxContentsFont.GetCurSel(), szFont);

	szTextColor = ::GetColorFromString(m_cbxContentsTextColor.GetSelectedColorValue());
	szTextColor.Replace("#", "");

	szBgColor = ::GetColorFromString(m_cbxContentsBGColor.GetSelectedColorValue());
	szBgColor.Replace("#", "");

	CString szPath;
	char szIP[1024];
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);
	::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", szIP, sizeof(szIP), szPath);
/*
	szURL = "http://";
	szURL += szIP;
	szURL += ":8080/ubc_rss_pub.asp?totalline=100";
	szURL += "&url=";
	szURL += m_editURL.GetValueString();
	szURL += "&width=";
	szURL += m_edtWidth.GetValueString();
	szURL += "&height=";
	szURL += m_edtHeight.GetValueString();
	szURL += "&font=";
	szURL += szFont;
	szURL += "&fontsize=";
	szURL += m_editContentsFontSize.GetValueString();
	szURL += "&fontcolor=";
	szURL += szTextColor;
	szURL += "&bgcolor=";
	szURL += szBgColor;
	szURL += "&line=";
	szURL += m_edtLineNum.GetValueString();
*/
	//m_wndWeb.m_comment[0] = szURL;
	m_wndRss.m_comment[0] = m_editURL.GetValueString();
	m_wndRss.m_fontSize = atoi(m_editContentsFontSize.GetValueString());
	m_wndRss.m_fgColor = szTextColor;
	m_wndRss.m_bgColor = szBgColor;
	m_wndRss.m_runningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();

	// RSS 관련 수정 (0000918, 0000875, 0000872, 0000871, 0000869)
	m_wndRss.m_comment[2] = ::ToString(m_nRssType);		// 종류
	m_wndRss.m_comment[3] = m_strErrText;					// 장애 텍스트
	m_wndRss.m_comment[4] = m_strTopicFilter;				// 토픽 필터
	m_wndRss.m_comment[5] = (m_nRssType == 1 ? m_edtLineNum.GetValueString() : ""); // 라인수

	m_wndRss.OpenFile(0);
}

void CRSSContentsDlg::OnBnClickedFileName()
{
	CString szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
	CString filter;
	filter.Format("All Image Files|%s|"
				  "Bitmap Files (*.bmp)|*.bmp|"
				  "JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;|"
				  "GIF Files (*.gif)|*.gif|"
				  "PCX Files (*.pcx)|*.pcx|"
				  "PNG Files (*.png)|*.png|"
				  "TIFF Files (*.tif)|*.tif;*.tiff||"
				 , szFileExts
				 );

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, CONTENTS_IMAGE)){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = _T("");
	m_editContentsFileName.SetSel(0, -1);

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);

	char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_splitpath(dlg.GetPathName(), drive, path, filename, ext);

	if(contents_name.GetLength() == 0)
		m_editContentsName.SetWindowText(filename);

	m_strLocation  = drive;
	m_strLocation += path;

	CFileStatus fs;
	if(CEnviroment::GetFileSize(dlg.GetPathName(), fs.m_size))
	{
		m_ulFileSize = fs.m_size;
	}

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(m_ulFileSize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
	}
	m_strFileMD5 = CString(szMd5);

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
	m_bIsChangedFile = TRUE;
}

void CRSSContentsDlg::OnBnClickedButtonPreviewPlay()
{
	SetPlayContents();
	m_wndRss.Play();
}

void CRSSContentsDlg::OnBnClickedPermanentCheck()
{
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent){
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}else{
		m_editContentsPlayMinute.SetWindowText("0");
		m_editContentsPlaySecond.SetWindowText("15");

		m_editContentsPlayMinute.EnableWindow(TRUE);
		m_editContentsPlaySecond.EnableWindow(TRUE);
	}
}

void CRSSContentsDlg::OnBnClickedDeletefile()
{
	m_ulFileSize = 0;
	m_editContentsFileName.SetValue("");
	m_strLocation = _T("");
	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	m_strFileMD5 = _T("");
}

BOOL CRSSContentsDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CRSSContentsDlg::OnOK() {}
void CRSSContentsDlg::OnCancel()
{
	GetParent()->PostMessage(WM_CLOSE);
}

void CRSSContentsDlg::OnBnClickedRbRssType1()
{
	UpdateData(TRUE);
	m_edtLineNum.SetValue(0);
	m_edtLineNum.EnableWindow(m_nRssType == 1);
	UpdateData(FALSE);
}

void CRSSContentsDlg::OnBnClickedRbRssType2()
{
	if(!m_edtLineNum.IsWindowEnabled())
	{
		UpdateData(TRUE);
		m_edtLineNum.SetValue(10);
		m_edtLineNum.EnableWindow(m_nRssType == 1);
		UpdateData(FALSE);
	}
}

void CRSSContentsDlg::OnBnClickedRbRssType3()
{
	UpdateData(TRUE);
	m_edtLineNum.SetValue(0);
	m_edtLineNum.EnableWindow(m_nRssType == 1);
	UpdateData(FALSE);
}

void CRSSContentsDlg::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editURL.EnableWindow(bEnable);
	m_editContentsFileName.EnableWindow(bEnable);
	m_btnBrowserContentsFile.EnableWindow(bEnable);
	m_btnDelFile.EnableWindow(bEnable);

	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	GetDlgItem(IDC_PERMANENT_CHECK)->EnableWindow(bEnable);

	m_cbxContentsFont.EnableWindow(bEnable);
	m_editContentsFontSize.EnableWindow(bEnable);
	m_cbxContentsTextColor.EnableWindow(bEnable);
	m_cbxContentsBGColor.EnableWindow(bEnable);
	m_edtHeight.EnableWindow(bEnable);
	m_spnHeight.EnableWindow(bEnable);
	m_edtWidth.EnableWindow(bEnable);
	m_spnWidth.EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_RSS_TOPIC_FILTER)->EnableWindow(bEnable);
	GetDlgItem(IDC_EDIT_ERR_TEXT)->EnableWindow(bEnable);
	m_edtLineNum.EnableWindow(bEnable);
	m_spnLineNum.EnableWindow(bEnable);
	GetDlgItem(IDC_RB_RSS_TYPE1)->EnableWindow(bEnable);
	GetDlgItem(IDC_RB_RSS_TYPE2)->EnableWindow(bEnable);
	GetDlgItem(IDC_RB_RSS_TYPE3)->EnableWindow(bEnable);
}
