#pragma once
#include "afxwin.h"
#include "control/FontStatic.h"

// CExportDlg dialog

class CExportDlg : public CDialog
{
	DECLARE_DYNAMIC(CExportDlg)

public:
	CExportDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CExportDlg();

	CStatic m_sttComment;
//	CFontStatic m_sttComment;

// Dialog Data
	enum { IDD = IDD_EXPORT_DLG };

	void SetComment(LPCTSTR str);
	void SetNetworkMode(bool bMode=true);
	void SetMonitorCnt(int nCnt);
	void GetMonitor(int &nCheckA, int &nCheckB);

protected:
	bool m_nNetMode;
	int m_nCheckA;
	int m_nCheckB;
	int m_nMonitorCnt;
	CString m_szComment;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedCheckExportMonitera();
	afx_msg void OnBnClickedCheckExportMoniterb();
};
