#pragma once

#include "Control/PictureEx.h"

// CSplashDlg 대화 상자입니다.

class CSplashDlg : public CDialog
{
	DECLARE_DYNAMIC(CSplashDlg)

public:
	CSplashDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSplashDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SPLASH_DLG };

protected:
	CPictureEx		m_clsGif;			///<Gif image operation class

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
};
