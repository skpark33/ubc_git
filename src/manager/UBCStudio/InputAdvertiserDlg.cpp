// InputAdvertiserDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "InputAdvertiserDlg.h"


// InputAdvertiserDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(InputAdvertiserDlg, CDialog)

InputAdvertiserDlg::InputAdvertiserDlg(CWnd* pParent /*=NULL*/)
	: CDialog(InputAdvertiserDlg::IDD, pParent)
{

}

InputAdvertiserDlg::~InputAdvertiserDlg()
{
}

void InputAdvertiserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_editName);
}


BEGIN_MESSAGE_MAP(InputAdvertiserDlg, CDialog)
	ON_BN_CLICKED(IDOK, &InputAdvertiserDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &InputAdvertiserDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// InputAdvertiserDlg 메시지 처리기입니다.

void InputAdvertiserDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	this->m_editName.GetWindowText(this->m_advertiser);
	OnOK();
}

void InputAdvertiserDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
