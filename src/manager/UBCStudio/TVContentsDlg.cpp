#include "stdafx.h"
#include "UBCStudio.h"
#include "TVContentsDlg.h"
#include "common/PreventChar.h"

IMPLEMENT_DYNAMIC(CTVContentsDlg, CSubContentsDialog)

CTVContentsDlg::CTVContentsDlg(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CTVContentsDlg::IDD, pParent)
,	m_bPreviewMode(false)
,	m_bPermanent(FALSE)
{
}

CTVContentsDlg::~CTVContentsDlg()
{
}

void CTVContentsDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_COMBO_TVSOURCE, m_cbTvSource);
	DDX_Control(pDX, IDC_EDIT_TVCHANNEL, m_edtTvChannel);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_VOLUME, m_editContentsVolume);
	DDX_Control(pDX, IDC_SLIDER_CONTENTS_VOLUME, m_sliderContentsVolume);
	DDX_Control(pDX, IDC_EDIT_FELICA_URL, m_editFelicaUrl);
}

BEGIN_MESSAGE_MAP(CTVContentsDlg, CSubContentsDialog)
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, &CTVContentsDlg::OnBnClickedPermanentCheck)
	ON_EN_CHANGE(IDC_EDIT_CONTENTS_VOLUME, &CTVContentsDlg::OnEnChangeEditContentsVolume)
	ON_WM_HSCROLL()
END_MESSAGE_MAP()

BOOL CTVContentsDlg::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	m_cbTvSource.AddString(LoadStringById(IDS_TVCONTENTSDIALOG_STR001));
	m_cbTvSource.AddString(LoadStringById(IDS_TVCONTENTSDIALOG_STR002));
	m_cbTvSource.AddString(LoadStringById(IDS_TVCONTENTSDIALOG_STR003));
	m_cbTvSource.AddString(LoadStringById(IDS_TVCONTENTSDIALOG_STR004));
	m_cbTvSource.AddString(LoadStringById(IDS_TVCONTENTSDIALOG_STR005));
	m_cbTvSource.AddString(LoadStringById(IDS_TVCONTENTSDIALOG_STR006));
	m_cbTvSource.SetCurSel(modeAnalogAir);

	//m_edtTvChannel.SetValue(0);

	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(15);

	m_editContentsVolume.SetValue(0);
	m_editContentsVolume.SetLimitText(3);
	m_editContentsVolume.SetValue(100);

	m_sliderContentsVolume.BuildThumbItem(IDB_SLIDER_THUMB, 11, 21);
	m_sliderContentsVolume.BuildBackItem(IDB_SLIDER_NORMAL_LONG, IDB_SLIDER_ACTIVE_LONG);
	m_sliderContentsVolume.SetTopOffset(0);
	m_sliderContentsVolume.SetLineSize(0);
	m_sliderContentsVolume.SetRange(0, 100);
	m_sliderContentsVolume.SetPos(100);

	if(m_bPreviewMode)
	{
		EnableAllControls(FALSE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CTVContentsDlg::Stop()
{
}

bool CTVContentsDlg::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData(TRUE);

	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	info.strLocalLocation.Empty();
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();
	info.nSoundVolume = m_editContentsVolume.GetValueInt();

	info.strComment[2].Format(_T("%d"), m_cbTvSource.GetCurSel()+1);
	info.strComment[3] = m_edtTvChannel.GetValueString();

	if(!IsFitContentsName(info.strContentsName)) return false;
	if(info.nRunningTime == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
		return false;
	}
	if(info.strComment[3].GetLength() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_TVCONTENTSDIALOG_MSG001), MB_ICONSTOP);
		return false;
	}

	m_editFelicaUrl.GetWindowText(info.strComment[1]);
	info.strComment[1].Trim();
	if(!info.strComment[1].IsEmpty())
	{
		CString strFelicaUrl = info.strComment[1];
		strFelicaUrl.MakeLower();
		if(strFelicaUrl.Find(_T("http://")) < 0)
		{
			UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG007), MB_ICONSTOP);
			return false;
		}
	}

	return true;
}

bool CTVContentsDlg::SetContentsInfo(CONTENTS_INFO& info)
{
	m_editContentsName.SetWindowText(info.strContentsName);

	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText(_T("1440"));
		m_editContentsPlaySecond.SetWindowText(_T("00"));
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}//if

	m_cbTvSource.SetCurSel(_ttoi(info.strComment[2])-1);
	m_edtTvChannel.SetWindowText(info.strComment[3]);
	m_editContentsVolume.SetWindowText(::ToString(info.nSoundVolume));
	OnEnChangeEditContentsVolume();
	m_editFelicaUrl.SetWindowText(info.strComment[1]);

	UpdateData(FALSE);

	return true;
}

void CTVContentsDlg::OnBnClickedPermanentCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//24시간을 설정
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent)
	{
		m_editContentsPlayMinute.SetWindowText(_T("1440"));
		m_editContentsPlaySecond.SetWindowText(_T("00"));

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(_T(""));
		m_editContentsPlaySecond.SetWindowText(_T("15"));

		m_editContentsPlayMinute.EnableWindow(TRUE);
		m_editContentsPlaySecond.EnableWindow(TRUE);
	}//if
}

BOOL CTVContentsDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == GetDlgItem(IDC_EDIT_CONTENTS_NAME)->GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CTVContentsDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CSubContentsDialog::OnHScroll(nSBCode, nPos, pScrollBar);

	if( pScrollBar->GetSafeHwnd() == m_sliderContentsVolume.GetSafeHwnd() )
	{
		m_editContentsVolume.SetWindowText(::ToString((int)m_sliderContentsVolume.GetPos()));
	}
}

void CTVContentsDlg::OnEnChangeEditContentsVolume()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CSubContentsDialog::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	int vol = m_editContentsVolume.GetValueInt();
	m_sliderContentsVolume.SetPos(vol);
	m_sliderContentsVolume.Invalidate(FALSE);

	if(vol != m_sliderContentsVolume.GetPos())
	{
		m_editContentsVolume.SetWindowText(::ToString((int)m_sliderContentsVolume.GetPos()));
	}
}

void CTVContentsDlg::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_cbTvSource.EnableWindow(bEnable);
	m_edtTvChannel.EnableWindow(bEnable);
	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	GetDlgItem(IDC_PERMANENT_CHECK)->EnableWindow(bEnable);
	m_editContentsVolume.EnableWindow(bEnable);
	m_sliderContentsVolume.EnableWindow(bEnable);
	m_editFelicaUrl.EnableWindow(bEnable);
}
