
#include "stdafx.h"
#include "myutil.h"



CString  GetThisPath()
{
	static TCHAR szModuleName[MAX_PATH*4];
	static BOOL bIsFirst = TRUE;
	if(bIsFirst)
	{
		::GetModuleFileName(NULL,szModuleName,MAX_PATH*4);
		*(_tcsrchr( szModuleName, '\\' ) + 1) = NULL;
		bIsFirst = FALSE;
	}
	return CString(szModuleName);
}


CString GetIniStr(LPCTSTR szSection, LPCTSTR szKey, CString sIniFileName, 
				CString sDefaultValue)
{
    TCHAR  szBuf[10000];
	if(sIniFileName.Find(':')==-1 && sIniFileName.Left(2)!=_T("\\\\"))
		sIniFileName = GetThisPath() + sIniFileName;
    GetPrivateProfileString(szSection,szKey,sDefaultValue,szBuf,9999,sIniFileName);
    return CString(szBuf);
}


BOOL SetIniStr(LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szValue, CString sIniFileName)
{
	if(sIniFileName.Find(':')==-1 && sIniFileName.Left(2)!=_T("\\\\"))
	    sIniFileName = GetThisPath() + sIniFileName;
	return WritePrivateProfileString(szSection, szKey, szValue, sIniFileName);
}
int	GetIniInt(LPCTSTR szSection, LPCTSTR szKey, int nDefault, CString sIniFileName)
{
	int		nRet;
	if(sIniFileName.Find(':')==-1 && sIniFileName.Left(2)!=_T("\\\\"))
	    sIniFileName = GetThisPath() + sIniFileName;
    nRet = GetPrivateProfileInt(szSection, szKey, nDefault, sIniFileName);
	return nRet;
}


BOOL SetIniInt(LPCTSTR szSection, LPCTSTR szKey, int nValue, CString sIniFileName)
{
	CString sValue;
	sValue.Format(_T("%d"), nValue);
	return SetIniStr(szSection, szKey, sValue, sIniFileName);
}

void FindFiles(CString fn, CStringList& files)
{
	CString path = fn;
	path.Replace('/', '\\');
	path = path.Left(path.ReverseFind('\\')+1);

	WIN32_FIND_DATA findData;
	HANDLE h = FindFirstFile(fn, &findData);
	if(h != INVALID_HANDLE_VALUE)
	{
		do {files.AddTail(path + findData.cFileName);}
		while(FindNextFile(h, &findData));

		FindClose(h);
	}
}

static TCHAR ret[_MAX_FNAME];
TCHAR* GetFileNameFromPath( LPCTSTR szPathName )
{
   TCHAR drive[_MAX_DRIVE];
   TCHAR dir[_MAX_DIR];
   TCHAR fname[_MAX_FNAME];
   TCHAR ext[_MAX_EXT];
   _tsplitpath( szPathName, drive, dir, fname, ext );
   _tcscpy(ret,fname);
   _tcscat(ret,ext);
   return ret;
}



TCHAR* GetFileNameFromPathWithOutExt( LPCTSTR szPathName )
{
   TCHAR drive[_MAX_DRIVE];
   TCHAR dir[_MAX_DIR];
   TCHAR fname[_MAX_FNAME];
   TCHAR ext[_MAX_EXT];
   _tsplitpath( szPathName, drive, dir, fname, ext );
   _tcscpy(ret,fname);
   return ret;
}


BOOL IsFolder(LPCTSTR szPathName)
{
	DWORD dwRet;
	dwRet = GetFileAttributes(szPathName);
	if(dwRet==0xffffffff) return FALSE;					// 오류 발생
	if(dwRet & FILE_ATTRIBUTE_DIRECTORY) return TRUE;
	return FALSE;
}

BOOL IsFile(LPCTSTR szPathName)
{
	DWORD dwRet;
	dwRet = GetFileAttributes(szPathName);
	if(dwRet==0xffffffff) return FALSE;					// 오류 발생
	if(dwRet & FILE_ATTRIBUTE_DIRECTORY) return FALSE;
	return TRUE;
}


void SortStringList(CStringList &slFiles, SORTSTRINGLIST_METHOD nMethod)
{
	POSITION p,q,r;
	p = slFiles.GetHeadPosition();
	CString temp;

	if(nMethod==SSLM_NORMAL)
	{
		while(p)
		{
			q = p;
			temp = slFiles.GetAt(p);
			while(q)
			{
				r = q;
				slFiles.GetPrev(r);
				if(r && slFiles.GetAt(r) > temp) slFiles.GetAt(q) = slFiles.GetAt(r);
				else  break;
				slFiles.GetPrev(q);
			}
			slFiles.GetAt(q) = temp;
			slFiles.GetNext(p);
		}
	}
	else if(nMethod==SSLM_COMPARESTRING)
	{
		while(p)
		{
			q = p;
			temp = slFiles.GetAt(p);
			while(q)
			{
				r = q;
				slFiles.GetPrev(r);
				if(r && CompareString(LOCALE_USER_DEFAULT, NORM_IGNORECASE, slFiles.GetAt(r), -1, temp, -1)==CSTR_GREATER_THAN)
					slFiles.GetAt(q) = slFiles.GetAt(r);
				else break;
				slFiles.GetPrev(q);
			}
			slFiles.GetAt(q) = temp;
			slFiles.GetNext(p);
		}
	}
	else
		ASSERT(0);
}


TCHAR*	GetExtFromPath(LPCTSTR szPathName)
{
   TCHAR drive[_MAX_DRIVE];
   TCHAR dir[_MAX_DIR];
   TCHAR fname[_MAX_FNAME];
   TCHAR ext[_MAX_EXT];
   _tsplitpath( szPathName, drive, dir, fname, ext );
   _tcscpy(ret,ext);
   return ret;
}




int GetFileSize(LPCTSTR sPathName)
{
	HANDLE	hFile;
	int		nSize;
	hFile = ::CreateFile(sPathName, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 
		FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if(hFile==INVALID_HANDLE_VALUE) return -1;
	nSize = ::GetFileSize(hFile, NULL);
	CloseHandle(hFile);
	return nSize;
}


CString AddPath(CString sLeft, CString sRight, CString sPathChar)
{
	CString sRet;
	sRight = 
		sRight.GetLength()==0 ? _T("") : 
		(sRight.GetLength()==1 && sRight.Left(1)==sPathChar) ? _T("") :  
		sRight.Left(1)==sPathChar ? sRight.Mid(1) : sRight;

	sRet = (sLeft.Right(1)==sPathChar) ?	sLeft + sRight : sLeft + sPathChar + sRight;
	return sRet;
}

void PumpUntilEmpty(HWND hWnd)
{
	MSG msg;
	while(::PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE))
	{
		::TranslateMessage(&msg);
		::DispatchMessage(&msg);
	}
}


static CString  g_sGetDirectoryDir;
static int CALLBACK BrowseCallbackProc(HWND hwnd,UINT msg,LPARAM lp, LPARAM pData)
{
	TCHAR buf[MAX_PATH];
	switch(msg) 
	{
	// when dialog is first initialized, change directory to one chosen above
		case BFFM_INITIALIZED: 
			_tcscpy(buf,g_sGetDirectoryDir);
			::SendMessage(hwnd,BFFM_SETSELECTION,TRUE,(LPARAM)buf);
			break;
	// if you picked BIF_STATUSTEXT above, you can fill status here
		case BFFM_SELCHANGED:
			if (::SHGetPathFromIDList((LPITEMIDLIST) lp ,buf)) 
				SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)buf);
			break;
	}
	return 0;
}

#ifndef BIF_NEWDIALOGSTYLE
  #define BIF_NEWDIALOGSTYLE     0x0040   // Use the new dialog layout with the ability to resize
  #define BIF_USENEWUI           (BIF_NEWDIALOGSTYLE | BIF_EDITBOX)
#endif
CString GetDirectory(HWND hWndParent, LPCTSTR szRoot, LPCTSTR szTitle)
{
	BROWSEINFO bi;
    bi.hwndOwner=hWndParent;		//owner of created dialog box
    bi.pidlRoot=0;					//unused
    bi.pszDisplayName=0;			//buffer to receive name displayed by folder (not a valid path)
    bi.lpszTitle=szTitle;			//title is "Browse for Folder", this is an instruction
	bi.lpfn = BrowseCallbackProc;	//callback routine called when dialog has been initialized
    bi.lParam=0;					//passed to callback routine
    bi.ulFlags=
		BIF_RETURNONLYFSDIRS |		//only allow user to select a directory
		BIF_STATUSTEXT |			//create status text field we will be writing to in callback
		BIF_NEWDIALOGSTYLE |		// resizing 가능한 새로운 스타일..OleInitialize() 가 호출 되어있어야 함..
//		BIF_BROWSEFORCOMPUTER|		//only allow user to select a computer
//		BIF_BROWSEFORPRINTER |		//only allow user to select a printer
//		BIF_BROWSEINCLUDEFILES|		//displays files too which user can pick
//		BIF_DONTGOBELOWDOMAIN|		//when user is exploring the "Entire Network" they
									// are not allowed into any domain
		0; 
	g_sGetDirectoryDir = szRoot;

	LPITEMIDLIST lpItemId=::SHBrowseForFolder(&bi); 
	if (lpItemId)
	{
		TCHAR szBuf[MAX_PATH];
		::SHGetPathFromIDList(lpItemId, szBuf);
		::GlobalFree(lpItemId);
		return CString(szBuf);
	}
	return _T("");
}


static OS_VERSION _CheckOSVersionheckOSVerion(CString* psServiceRelease)
{
	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	if (GetVersionEx(&osv))
	{
		if(psServiceRelease)
			*psServiceRelease = osv.szCSDVersion;	// note: szCSDVersion =  service pack  release  
		switch(osv.dwPlatformId)
		{
		case VER_PLATFORM_WIN32s:					//Win32s on Windows 3.1.
			return OSVER_31;
			
		case VER_PLATFORM_WIN32_WINDOWS:			//WIN32 on 95 or 98
			if (osv.dwMinorVersion == 0)			
			{
				if ( osv.szCSDVersion[1] == 'C' ) return OSVER_95OSR2;
				else return OSVER_95;
			}
			else if(osv.dwMinorVersion == 10)		
			{
				if ( osv.szCSDVersion[1] == 'A' ) return OSVER_98SE;
				else return OSVER_98;
			}
			else if(osv.dwMinorVersion == 90)		return OSVER_ME;
			break;

		case VER_PLATFORM_WIN32_NT:					//Win32 on Windows NT.
			if(osv.dwMajorVersion == 3) 			return OSVER_NT351;
			else if(osv.dwMajorVersion == 4) 		return OSVER_NT4;
			else if(osv.dwMajorVersion == 5)
			{
				if(osv.dwMinorVersion == 0)			return OSVER_2000;
				else if(osv.dwMinorVersion == 1)	return OSVER_XP;
				else if(osv.dwMinorVersion == 2)	return OSVER_2003;
			}
			return OSVER_FUTURE;					// future os
		}
		return OSVER_UNKNOWN;
	}
	return OSVER_ERROR;
}
OS_VERSION CheckOSVersion(CString* psServiceRelease)
{
	static OS_VERSION version = OSVER_ERROR;	// static 으로 캐쉬한다(한번만 불리면 다음에는 체크 안한다..)
	if(version==OSVER_ERROR || psServiceRelease) version = _CheckOSVersionheckOSVerion(psServiceRelease);
	return version;
}


int GetFileList(CString strPathName, CStringArray &arrFileList, BOOL bRecursive/*=TRUE*/, CString strFilter/*=_T("*.*")*/)
{
	WIN32_FIND_DATA		FindFileData;
	HANDLE				hFind;
	CString				strParent;

	if(!IsFolder(strPathName)) return 0;

	// 끝에 \ 가 붙어 있게했다.
	strParent = strPathName.Right(1) == _T("\\") ? strPathName : strPathName + _T("\\");
	if((hFind = ::FindFirstFile(strParent + strFilter, &FindFileData)) ==INVALID_HANDLE_VALUE) return 0;
	do
	{
		if(_tcscmp(FindFileData.cFileName, _T(".")) == 0 || _tcscmp(FindFileData.cFileName, _T("..")) == 0) continue;
		if(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if(bRecursive) GetFileList(strParent + FindFileData.cFileName, arrFileList, bRecursive);
			continue;
		}
		arrFileList.Add(strParent + FindFileData.cFileName);
	} while(FindNextFile(hFind, &FindFileData));
	FindClose(hFind);
#ifndef _MFC_VER
	return arrFileList.GetCount();
#else
	return arrFileList.GetSize();
#endif
}


void GetProgramFilesDirectory(TCHAR* out)
{
	HKEY hKey;
	lstrcpy(out, _T("C:\\Program Files"));
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE,_T("Software\\Microsoft\\Windows\\CurrentVersion"),0,KEY_READ,&hKey) == ERROR_SUCCESS)
	{
		ULONG l = 1024;
		ULONG t=REG_SZ;
		RegQueryValueEx(hKey,_T("ProgramFilesDir"),NULL,&t,(BYTE*)out,&l );
		RegCloseKey(hKey);
	}
}


BOOL LaunchInternetExplorer(LPCTSTR szUrl)
{
	TCHAR szExplorer[MAX_PATH];
	TCHAR szParam[MAX_PATH*4];		// 프로그램 경로와 url 을 담을 버퍼
	STARTUPINFO si = {0,};
	PROCESS_INFORMATION pi;
	HINSTANCE hInst;

	GetProgramFilesDirectory(szExplorer);
	lstrcat(szExplorer, _T("\\Internet Explorer\\iexplore.exe"));

	wsprintf(szParam, _T("\"%s\" \"%s\""), szExplorer, szUrl);

	// ie 실행
	if(CreateProcess(NULL, (TCHAR*)(LPCTSTR)szParam, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
	{
		CloseHandle( pi.hProcess );
		CloseHandle( pi.hThread );
		return TRUE;
	}

	// ie 실행이 실패하면 탐색기로 열기
	wsprintf(szParam, _T("\"%s\""), szUrl);
	hInst = ShellExecute(NULL, _T("open"), _T("explorer"), szParam, NULL, SW_SHOWNORMAL);
	if(hInst<=(HINSTANCE)32)  return FALSE;
	return TRUE;
}
