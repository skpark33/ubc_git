
#pragma once


CString	GetThisPath();

#define	DEFAULT_INI_FILE_NAME	_T("SETTING.INI")
CString	GetIniStr(LPCTSTR szSection, LPCTSTR szKey, 					
				CString sIniFileName=DEFAULT_INI_FILE_NAME,
				CString sDefaultValue=_T(""));						
BOOL	SetIniStr(LPCTSTR szSection, LPCTSTR szKey,					
				  LPCTSTR szValue, 
				  CString sIniFileName=DEFAULT_INI_FILE_NAME);	
int		GetIniInt(LPCTSTR szSection, LPCTSTR szKey, int nDefault,	
				CString sIniFileName=DEFAULT_INI_FILE_NAME);	
BOOL	SetIniInt(LPCTSTR szSection, LPCTSTR szKey, int nValue,		
				CString sIniFileName=DEFAULT_INI_FILE_NAME);	


void FindFiles(CString fn, CStringList& files);

TCHAR* 	GetFileNameFromPathWithOutExt(LPCTSTR szPathName );			
BOOL	IsFolder(LPCTSTR szPathName);								
BOOL	IsFile(LPCTSTR szPathName);									

enum SORTSTRINGLIST_METHOD {SSLM_NORMAL, SSLM_COMPARESTRING,};
void SortStringList(CStringList &slFiles, SORTSTRINGLIST_METHOD nMethod=SSLM_NORMAL);

TCHAR*	GetExtFromPath(LPCTSTR szPathName);							

TCHAR* GetFileNameFromPath( LPCTSTR szPathName );

int GetFileSize(LPCTSTR sPathName);

CString AddPath(CString sLeft, CString sRight, CString sPathChar=_T("\\"));
void PumpUntilEmpty(HWND hWnd=NULL);									
CString GetDirectory(HWND hWndParent, LPCTSTR szRoot, 
						LPCTSTR szTitle)						;	

enum OS_VERSION
{
	OSVER_ERROR,	OSVER_UNKNOWN,
	OSVER_31,		
	OSVER_95,		OSVER_95OSR2,
	OSVER_98,		OSVER_98SE, OSVER_ME,
	OSVER_NT351,	OSVER_NT4,	OSVER_2000,	OSVER_XP,
	OSVER_2003,		OSVER_FUTURE,
};

OS_VERSION	CheckOSVersion(CString* psServiceRelease=NULL);			


int GetFileList(CString strPathName, CStringArray &arrFileList, BOOL bRecursive=TRUE, CString strFilter=_T("*.*"));

BOOL LaunchInternetExplorer(LPCTSTR szUrl);
