#include "stdafx.h"
#include "AviReader.h"
#include "myUtil.h"



// fourcc 랑 char* 랑 비교할때 쓰는 함수
static BOOL Comp(CHAR src[4], CHAR* dst)
{
	return memcmp(src,dst,4)==0 ? TRUE : FALSE;
}
static BOOL Comp(FOURCC src, CHAR* dst)
{
	return memcmp(&src,dst,4)==0 ? TRUE : FALSE;
}

// fourcc 를 char* 로 보여줄때 쓰는 함수
CString FOURCC2Str(FOURCC fcc)
{
	CString str;
	str.Format(_T("%c%c%c%c"), (fcc)&0xff, (fcc>>8)&0xff,(fcc>>16)&0xff,(fcc>>24)&0xff);
	return str;
}

CAviReader::CAviReader()
{
	Init();
}

CAviReader::~CAviReader()
{

}

// 초기화
void CAviReader::Init()
{
	m_hFile = NULL;
	m_dwFilePos = -1;
	m_dwFileDataSize = -1;
	m_bHasText = FALSE;

	m_dwMoviListPos = 0;
	m_dwMoviListSize = 0;

	m_dwOdmlListPos = 0;
	m_dwOdmlListSize = 0;

	m_dwIdx1StartPos = 0;
	m_dwIdx1Size = 0;

	m_bHasValidIndex = FALSE;
	m_nLastError = ERR_NOERROR;
	m_sISFT = _T("");
	m_dwTimeOutWhenReadIDX = -1;
	m_bSkipIndex = FALSE;
	ZeroMemory(&m_hdrl,sizeof(m_hdrl));
	ZeroMemory(&m_strhAuds,sizeof(m_strhAuds));
	ZeroMemory(&m_strhVids,sizeof(m_strhVids));
	ZeroMemory(&m_strhTxt,sizeof(m_strhTxt));
	ZeroMemory(&m_wf,sizeof(m_wf));
	ZeroMemory(&m_bi,sizeof(m_bi));
}

// file pointer 를 정해진 바이트 만큼 스킵한다.
DWORD CAviReader::SkipFilePointer(INT64 nSkip)
{
//	ASSERT(nSkip);
	nSkip+=(nSkip&1);		// 짝수로 맞춘다..
	m_dwFilePos = ::SetFilePointer(m_hFile, (UINT)nSkip, (PLONG) NULL, FILE_CURRENT);			// 파일 위치 조정
	return m_dwFilePos;
}

// 스트림 읽기
BOOL CAviReader::ReadData(void* pData, UINT nLen)
{
	ASSERT(nLen);
	ASSERT((nLen&1)==0);	// 짝수 이어야만 한다..
	nLen+=(nLen&1);		// 짝수로 맞춘다..
	DWORD dwReadByte;
	if(::ReadFile(m_hFile, pData, (UINT)nLen, &dwReadByte, NULL)==FALSE) {ASSERT(0); return FALSE;}
	m_dwFilePos+= dwReadByte;
	if(dwReadByte!=nLen) { ASSERT(0); return FALSE;}
#ifdef _DEBUG
//SkipFilePointer(0);
#endif
	return TRUE;
}

// avi 헤더 읽기.
BOOL CAviReader::ReadHeader()
{
	SAviHeader	head;
	if(ReadData(&head, sizeof(head))==FALSE) return FALSE;
	// riff 인가?
	if(head.riff!=FOURCC_RIFF) return FALSE;

	char	id[4];
	if(ReadData(id, sizeof(id))==FALSE) return FALSE;
	if(Comp(id, "AVI ")==FALSE) return FALSE;
	m_dwFileDataSize = head.size;			// m_dwFileDataSize 는 junk 를 제외한 데이타 사이즈이다.

	return TRUE;
}

// sub chunk 읽기.
BOOL CAviReader::ReadHdrlChunk()
{
	if(ReadData(&m_hdrl, sizeof(m_hdrl))==FALSE) {return FALSE; }
	if(Comp(m_hdrl.header.id,"avih")==FALSE) return FALSE;
	return TRUE;
}

// stream list;
BOOL CAviReader::ReadStrl(int size)
{
	while(size)
	{
		SChunkHeader	header;
		if(ReadData(&header, sizeof(header))==FALSE) return FALSE;
		size-=sizeof(header);

		if(Comp(header.id,"strh"))	// stream header
		{
			SAVIStreamHeader	strh;
			ZeroMemory(&strh,sizeof(strh));
			if(ReadData(&strh, header.size)==FALSE) return FALSE;
			size-=header.size;
			if(Comp(strh.fccType,"vids"))		// video 정보
			{
				m_strhVids = strh;

				SChunkHeader	header;
				if(ReadData(&header, sizeof(header))==FALSE) return FALSE;
				if(Comp(header.id,"strf")==FALSE) ASSERT(0);
				size-=sizeof(header);

				// bitmap 정보 읽기
				//ASSERT(header.size==sizeof(m_bi));
				if(header.size>sizeof(m_bi))
				{
					//ASSERT(0);						// 뭔가 잘못 됬다?
					ReadData(&m_bi,sizeof(m_bi));
					size-=sizeof(m_bi);
				}
				else
				{
					ReadData(&m_bi,header.size);
					size-=header.size;
				}
			}
			else if(Comp(strh.fccType,"auds"))	// audio 정보
			{
				m_strhAuds = strh;

				SChunkHeader	header;
				if(ReadData(&header, sizeof(header))==FALSE) return FALSE;
				if(Comp(header.id,"strf")==FALSE) ASSERT(0);
				size-=sizeof(header);

				// wave 정보 읽기
				//ASSERT(header.size==sizeof(m_wf));
				ReadData(&m_wf,sizeof(m_wf));
				size-=sizeof(m_wf);
				if(size)
					SkipFilePointer(size);	// 그냥 남아있는거 넘기고 도망간다.. ( 안그러면 간혹 쫑난다. )
				return TRUE;
			}
			else if(Comp(strh.fccType,"txts")) 
			{
				ASSERT(0);		// 이런게 있는지 보고 싶군..
				m_strhTxt = strh;
				m_bHasText = TRUE;
			}
			else ASSERT(0);
		}
		else if(Comp(header.id,"strf"))
		{
			ASSERT(0);
		}
		else if(Comp(header.id,"strd"))
		{
			SkipFilePointer(header.size);
			size-=header.size;
		}
		else if(Comp(header.id,"strn"))		// stream name
		{
			int stringlen = size+size%2;	// 짝수로 맞춘다.
			char* buf;
			buf = (char*)malloc(stringlen+1);
			if(ReadData(buf, stringlen)==FALSE) return FALSE;
			// "Divx;-)3.2 920kbps 1kfps" 같은 내용이 들어가 있다..
			free(buf);						// 그냥 버린다?
			size-=stringlen;
		}
		else if(Comp(header.id,"JUNK"))
		{
			SkipFilePointer(header.size);
			size-=header.size;
		}
		else if(Comp(header.id,"LIST"))
		{
			SkipFilePointer(header.size);
			size-=header.size;
		}
		else if(Comp(header.id,"indx"))	// 뭐하는 놈인지는 잘 모르겠다..
		{
			SkipFilePointer(header.size);
			size-=header.size;
		}
		else
		{
//			ASSERT(0);					// 뭔가 잘못된 파일?
			SkipFilePointer(size);		// 그냥 skip
			return TRUE;
		}

		if(size<0) ASSERT(0);
	}
	return TRUE;
}


// "LIST" 처리
BOOL CAviReader::ReadLIST(const SChunkHeader& ch)
{
	FOURCC	id;
	if(ReadData(&id, sizeof(id))==FALSE) return FALSE;
	if(Comp(id, "hdrl"))
	{
		if(ReadHdrlChunk()==FALSE) { m_nLastError=ERR_HDRLERROR; return FALSE; }
	}
	else if(Comp(id, "strl"))	// stream list
	{
		if(ReadStrl(ch.size-sizeof(id))==FALSE) { m_nLastError=ERR_STREAMERROR; return FALSE; }
	}
	else
	{
		//ASSERT(0);
		SkipFilePointer(ch.size-sizeof(id));
	}
	return TRUE;
}


BOOL CAviReader::ReadAvifile()
{
	// avi 가맞는 지 확인한다.
	if(ReadHeader()==FALSE) { /*ASSERT(0); */m_nLastError=ERR_NOTAVIFILE; return FALSE; }

	SChunkHeader	ch;

	for(;;)
	{
		// 청크 헤더 읽기.
		if(m_dwFileSize==m_dwFilePos) break;

		if(m_dwFilePos + sizeof(ch) >= m_dwFileSize)
		{
			break;
		}

		if(ReadData(&ch, sizeof(ch))==FALSE) break;
		if(ch.id==FOURCC_LIST)
		{
			if(ReadLIST(ch)==FALSE) { ASSERT(0); return FALSE; }
		}
		else
		{
			if(m_dwFilePos + sizeof(ch) >= m_dwFileSize-8)
			{
				break;			// 끝..
			}

			//ASSERT(0);
			break;					// 에러상황 -> 뒤쪽의 쓰지 않는 부분인가?
		}
	}
	return TRUE;
}

// 파일 정보 읽기.
BOOL CAviReader::Open(CString sFilePath, DWORD dwTimeOut, BOOL bSkipIndex)
{
	Init();
	BOOL ret;
	m_dwTimeOutWhenReadIDX  = dwTimeOut;
	m_bSkipIndex = bSkipIndex;
	m_hFile = ::CreateFile(sFilePath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 
						FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if(m_hFile==INVALID_HANDLE_VALUE) {ASSERT(0); m_nLastError = ERR_CANTFILEOPEN; return FALSE; }
	m_dwFileSize = ::GetFileSize(m_hFile,NULL);							// 파일 크기.
	::SetFilePointer(m_hFile, 0, (PLONG) NULL, FILE_BEGIN);			// 파일 위치 조정
	ret = ReadAvifile();
	CloseHandle(m_hFile);

	m_sFilePath = sFilePath;
	m_sFileName = GetFileNameFromPath(sFilePath);

	return ret;
}

