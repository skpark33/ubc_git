/*

  Copyright (C) 2004-2006 hardkoder@gmail , http://www.kipple.pe.kr

  이 소프트웨어는 어떠한 명시적 또는 묵시적 보증도 없이 "있는 그대로" 제공됩니다. 그 
  어떤 경우에도 작성자는 이 소프트웨어의 사용으로 인한 손해에 대해 책임을 지지 않습니다.

  다음 제한 규정을 준수하는 경우에 한하여 상업적인 응용 프로그램을 포함하는 모든 용도로 이 소프트웨어를 
  사용하고 자유롭게 수정 및 재배포할 수 있는 권한이 누구에게나 부여됩니다.

  1. 이 소프트웨어의 출처를 잘못 표시하거나 원래 소프트웨어를 자신이 작성했다고 주장해서는 안 됩니다. 제품에 
     이 소프트웨어를 사용하는 경우 요구 사항은 아니지만 제품 설명서에 인정 조항을 넣어 주시면 감사하겠습니다.
  2. 수정된 소스 버전은 반드시 명확하게 표시되어야 하며 원래 소프트웨어로 오인되도록 잘못 표시해서는 안 됩니다.
  3. 모든 소스 배포 시 이 공지를 삭제하거나 수정할 수 없습니다.


  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

*/


#pragma once


#include <Mmsystem.h>		// fourcc 정의
#include <vfw.h>

CString FOURCC2Str(FOURCC fcc);

class CAviReader  
{
public :
	enum						// 에러 코드. GetLastError() 의 리턴값이다.
	{
		ERR_NOERROR,
		ERR_NOTAVIFILE,
		ERR_CANTFILEOPEN,
		ERR_HDRLERROR,			// avi main header 를 읽는중 에러 발생
		ERR_STREAMERROR,		// "strl" 청크를 읽는중 에러 발생
		ERR_MOVIERROR,			// "movi" 청크를 읽는중 에러 발생
		ERR_IDX1ERROR,			// "idx1" 청크를 읽는중 에러 발생 - 무시해도 되는 경우가 있다.
	};

	struct	SAviHeader
	{
		FOURCC	riff;
		DWORD	size;
	};
	struct	SChunkHeader
	{
		SChunkHeader() { memset(this, 0, sizeof(*this));}	
		FOURCC	id;
		DWORD	size;
	};
	struct SHdrl				// main avi header;
	{
		SChunkHeader	header;
		DWORD dwMicroSecPerFrame;
		DWORD dwMaxBytesPerSec;
		DWORD dwReserved1;
		DWORD dwFlags;
		DWORD dwTotalFrames;
		DWORD dwInitialFrames;
		DWORD dwStreams;
		DWORD dwSuggestedBufferSize;
		DWORD dwWidth;
		DWORD dwHeight;
		DWORD dwScale;
		DWORD dwRate;
		DWORD dwStart;
		DWORD dwLength;
	};
	struct SAVIStreamHeader		// auds / vids
	{
		FOURCC fccType;
		FOURCC fccHandler;
		DWORD  dwFlags;
		DWORD  dwPriority;
		DWORD  dwInitialFrames;
		DWORD  dwScale;
		DWORD  dwRate;
		DWORD  dwStart;
		DWORD  dwLength;
		DWORD  dwSuggestedBufferSize;
		DWORD  dwQuality;
		DWORD  dwSampleSize;
		RECT   rcFrame;
	};

	struct SAviIndexEntry		// idx1
	{
		DWORD  ckid;
		DWORD  dwFlags;
		DWORD  dwChunkOffset;
		DWORD  dwChunkLength;
	};


public:
	CAviReader();
	virtual ~CAviReader();
	BOOL					Open(CString sFilePath, DWORD dwTimeOut=-1, BOOL bSkipIndex=FALSE);
	const SAVIStreamHeader&	GetAuds() { return m_strhAuds; }
	const SAVIStreamHeader&	GetVids() { return m_strhVids; }
	const SHdrl&			GetMainHdr() { return m_hdrl; }
	const WAVEFORMATEX&		GetWaveFormatex() { return m_wf; }
	const BITMAPINFO&		GetBitmapInfo() { return m_bi; }
	CString					GetFilePath() { return m_sFilePath; }
	CString					GetFileName() { return m_sFileName; }
	DWORD					GetFileSize() { return m_dwFileSize; }
	DWORD					GetFileDataSize() { return m_dwFileDataSize; }

	DWORD					GetMoviListStartPos() { return m_dwMoviListPos; };	// LIST('movi' ..) 시작 위치
	DWORD					GetMoviListSize() { return m_dwMoviListSize; };		// LIST('movi' ..) 의 사이즈.
	DWORD					GetOdmlListStartPos() { return m_dwOdmlListPos; };	// LIST('odml' ..) 시작 위치
	DWORD					GetOdmlListSize() { return m_dwOdmlListSize; };		// LIST('odml' ..) 의 사이즈.

	DWORD					GetIdx1StartPos() { return m_dwIdx1StartPos; };		// ['idx1'<avi index>] 의 시작 위치
	DWORD					GetIdxSize() { return m_dwIdx1Size; };				// ['idx1'<avi index>] 의 사이즈..
	

	BOOL					HasIndex() { return (m_hdrl.dwFlags&AVIF_HASINDEX); }	// 인덱스 있나?
	BOOL					HasText() { return m_bHasText; }	// 자막 있나?
	BOOL					HasValidIndex() { return m_bHasValidIndex; }	// 인덱스가 온전한가?
	UINT					GetLastError() { return m_nLastError; }
	void					Init();
	const CString&			GetJUNK() { return m_sJUNK; }
	const CString&			GetISFT() { return m_sISFT; }
	const CString&			GetIENG() { return m_sIENG; }
	const CString&			GetICMT() { return m_sICMT; }

private :
	BOOL					ReadData(void* pData, UINT nLen);
	DWORD					SkipFilePointer(INT64 nSkip);
	int						ReadChar(CString& sRet, int nDesiredLen);


private :
	BOOL					ReadAvifile();
	BOOL					ReadHeader();
	BOOL					ReadHdrlChunk();
	BOOL					ReadLIST(const SChunkHeader& ch);
	BOOL					ReadStrl(int size);

private :
	SHdrl					m_hdrl;
	SAVIStreamHeader		m_strhAuds;
	SAVIStreamHeader		m_strhVids;
	SAVIStreamHeader		m_strhTxt;
	WAVEFORMATEX			m_wf;
	BITMAPINFO				m_bi;
	CString					m_sFilePath;
	CString					m_sFileName;
	DWORD					m_dwFileDataSize;
	DWORD					m_dwFileSize;

	DWORD					m_dwMoviListPos;
	DWORD					m_dwMoviListSize;
	DWORD					m_dwOdmlListPos;
	DWORD					m_dwOdmlListSize;
	DWORD					m_dwIdx1StartPos;
	DWORD					m_dwIdx1Size;

	BOOL					m_bHasText;
	BOOL					m_bHasValidIndex;
	UINT					m_nLastError;
	CString					m_sJUNK;
	CString					m_sISFT;
	CString					m_sIENG;
	CString					m_sICMT;
	CString					m_sINAM;						// NAME. Stores the title of the subject of the title, such as "Seattle Front above"
	DWORD					m_dwTimeOutWhenReadIDX;			// 인덱스 읽을때 타임아웃. ( mili sec )
	BOOL					m_bSkipIndex;					// 인덱스 정보 읽을지 말지 여부..
															// 읽지 않으면 분석 시간이 짧아 진다..

private :
	HANDLE					m_hFile;
	DWORD					m_dwFilePos;
};
