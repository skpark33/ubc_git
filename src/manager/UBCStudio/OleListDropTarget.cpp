// OleListDropTarget.cpp: implementation of the COleListDropTarget class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OleListDropTarget.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COleListDropTarget::COleListDropTarget(CWnd* pParent)
{
	m_pParent = pParent;
}

COleListDropTarget::~COleListDropTarget()
{
}

//
// OnDragEnter is called by OLE dll's when drag cursor enters
// a window that is REGISTERed with the OLE dll's
//
DROPEFFECT COleListDropTarget::OnDragEnter(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point )
{
	CStringArray filenameList;
	EnumFileNamesFromDataObject(pDataObject->m_lpDataObject, filenameList);

	if(filenameList.GetCount() == 0)
	{
		return DROPEFFECT_NONE;
	}

    // if the control key is held down, return a drop effect COPY 
    if((dwKeyState&MK_CONTROL) == MK_CONTROL)
	{
        return DROPEFFECT_COPY;
	}
    else
	{
	    // Otherwise return a drop effect of MOVE
        return DROPEFFECT_MOVE;
	}
}

//
// OnDragLeave is called by OLE dll's when drag cursor leaves
// a window that is REGISTERed with the OLE dll's
//
void COleListDropTarget::OnDragLeave(CWnd* pWnd)
{
    // Call base class implementation
    COleDropTarget::OnDragLeave(pWnd);
}

// 
// OnDragOver is called by OLE dll's when cursor is dragged over 
// a window that is REGISTERed with the OLE dll's
//
DROPEFFECT COleListDropTarget::OnDragOver(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point )
{
	CStringArray filenameList;
	EnumFileNamesFromDataObject(pDataObject->m_lpDataObject, filenameList);

	if(filenameList.GetCount() == 0)
	{
        return DROPEFFECT_NONE;
	}

    if((dwKeyState&MK_CONTROL) == MK_CONTROL)
	{
        return DROPEFFECT_NONE;
	}
    else
	{
        return DROPEFFECT_MOVE;  // move source
	}
}

BOOL COleListDropTarget::OnDrop(CWnd* pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point )
{
	CStringArray* filenameList = new CStringArray();
	EnumFileNamesFromDataObject(pDataObject->m_lpDataObject, *filenameList);

	if(filenameList->GetCount() != 0)
	{
		if(m_pParent)
		{
			m_pParent->PostMessage(WM_ADD_CONTENTS, (WPARAM)filenameList);
		}
	}

    if( (dropEffect & DROPEFFECT_MOVE) != DROPEFFECT_MOVE )
	{
        return FALSE;
	}

    return TRUE;
}

bool EnumFileNamesFromDataObject(LPDATAOBJECT spDataObject, CStringArray& filenameList)
{
	filenameList.RemoveAll();

	if (spDataObject == NULL)
	{
		ATLASSERT(0);
		return false;
	}

	CStgMedium medium; // ���髹�ʪΪǡ����ª���ۯ���ƪ����
	FORMATETC fe = {CF_HDROP, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL};
	HRESULT hr = spDataObject->GetData(&fe, &medium);
	if (FAILED(hr))
	{
		return false;
	}

	// �ի�����٣��֪?����
	int nCount = ::DragQueryFile((HDROP)medium.hGlobal, -1, NULL, 0);
	for (int nIdx = 0; nIdx < nCount; nIdx++)
	{
		TCHAR szPath[_MAX_PATH];
		::DragQueryFile((HDROP)medium.hGlobal, nIdx, szPath, sizeof(szPath));

		filenameList.Add(szPath);
	}

	return true;
};
