// ContentsOverwriteDlg.cpp : 구현 파일입니다.
//
#include "stdafx.h"
#include "UBCStudio.h"
#include "ContentsOverwriteDlg.h"
#include "ContentsDialog.h"
#include "shlwapi.h"
#include "Dbghelp.h"
#include "Enviroment.h"
#include "common/PreventChar.h"

// CContentsOverwriteDlg 대화 상자입니다.
IMPLEMENT_DYNAMIC(CContentsOverwriteDlg, CDialog)

CContentsOverwriteDlg::CContentsOverwriteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CContentsOverwriteDlg::IDD, pParent)
	, m_sContentsCopy(E_CONTENTS_COPY_USE_EXIST)
	, m_strOrgContentsName("")
	, m_strNewContentsName("")
	, m_pinfoSource(NULL)
	, m_pinfoTarget(NULL)
	, m_ptSourceIcon(0, 0)
	, m_ptTargetIcon(0, 0)
{
	m_bAllOfFiles = FALSE;
}

CContentsOverwriteDlg::~CContentsOverwriteDlg()
{
}

void CContentsOverwriteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SOURCE_PATH_STATIC, m_staticSourcePath);
	DDX_Control(pDX, IDC_TARGET_PATH_STATIC, m_staticTargetPath);
	DDX_Control(pDX, IDC_SOURCE_SIZE_STATIC, m_staticSourceSize);
	DDX_Control(pDX, IDC_TARGET_SIZE_STATIC, m_staticTargetSize);
	DDX_Control(pDX, IDC_SOURCE_TIME_STATIC, m_staticSourceTime);
	DDX_Control(pDX, IDC_TARGET_TIME_STATIC, m_staticTargetTime);
	DDX_Control(pDX, IDC_FILE_NAME_EDT, m_edtContentsName);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_SOURCE_ICON, m_iconSource);
	DDX_Control(pDX, IDC_TARGET_ICON, m_iconTarget);
	DDX_Control(pDX, IDC_SOURCE_NAME_STATIC, m_staticSourceName);
	DDX_Control(pDX, IDC_TARGET_NAME_STATIC, m_staticTargetName);
	DDX_Control(pDX, IDC_SOURCE_PREVIEW_BTN, m_btnSourcePreview);
	DDX_Control(pDX, IDC_TARGET_PREVIEW_BTN, m_btnTargetPreview);
}

BEGIN_MESSAGE_MAP(CContentsOverwriteDlg, CDialog)
	ON_BN_CLICKED(IDC_OVERWRITE_RADIO, &CContentsOverwriteDlg::OnBnClickedOverwriteRadio)
	ON_BN_CLICKED(IDC_RENAME_RADIO, &CContentsOverwriteDlg::OnBnClickedRenameRadio)
	ON_BN_CLICKED(IDOK, &CContentsOverwriteDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_NOCOPY_RADIO, &CContentsOverwriteDlg::OnBnClickedNocopyRadio)
	ON_BN_CLICKED(IDC_SOURCE_PREVIEW_BTN, &CContentsOverwriteDlg::OnBnClickedSourcePreviewBtn)
	ON_BN_CLICKED(IDC_TARGET_PREVIEW_BTN, &CContentsOverwriteDlg::OnBnClickedTargetPreviewBtn)
	ON_WM_PAINT()
END_MESSAGE_MAP()

// CContentsOverwriteDlg 메시지 처리기입니다.
void CContentsOverwriteDlg::OnBnClickedOverwriteRadio()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_sContentsCopy = E_CONTENTS_COPY_OVERWRITE;
	m_edtContentsName.SetWindowText(m_strOrgContentsName);
	m_edtContentsName.EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_ALLFILE)->EnableWindow(TRUE);
}

void CContentsOverwriteDlg::OnBnClickedRenameRadio()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	MakeAvailableName();
	m_sContentsCopy = E_CONTENTS_COPY_RENAME;
	((CButton*)GetDlgItem(IDC_CHECK_ALLFILE))->SetCheck(FALSE);
	GetDlgItem(IDC_CHECK_ALLFILE)->EnableWindow(FALSE);
}

void CContentsOverwriteDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_edtContentsName.GetWindowText(m_strNewContentsName);

	//if( (m_strNewContentsName.Find("[", 0) != -1) ||
	//	(m_strNewContentsName.Find("]", 0) != -1) )
	if(CPreventChar::GetInstance()->HavePreventChar(m_strNewContentsName))
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_CONTENTSOVERWRITEDLG_MSG001), CPreventChar::GetInstance()->GetPreventChar());
		UbcMessageBox(strMsg, MB_ICONERROR);
		return;
	}//if

	if(m_sContentsCopy == E_CONTENTS_COPY_RENAME)
	{
		//Overwrite를 하지 않는데도 이름이 바뀌지 않는다면
		if((m_strOrgContentsName == m_strNewContentsName) || m_strNewContentsName == "")
		{
			UbcMessageBox(LoadStringById(IDS_CONTENTSOVERWRITEDLG_MSG002), MB_ICONWARNING);
			m_edtContentsName.SetFocus();
			m_edtContentsName.SetSel(0, -1);
			return;
		}//if
	}//if

	if(((CButton*)GetDlgItem(IDC_CHECK_ALLFILE))->GetCheck())
		m_bAllOfFiles = TRUE;
	else
		m_bAllOfFiles = FALSE;

	OnOK();
}

BOOL CContentsOverwriteDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btnOK.LoadBitmap(IDB_BTN_APPLY, RGB(255, 255, 255));
	m_btnOK.SetToolTipText(LoadStringById(IDS_CONTENTSOVERWRITEDLG_BUT001));
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_CONTENTSOVERWRITEDLG_BUT002));
	m_btnSourcePreview.LoadBitmap(IDB_BTN_PREVIEW, RGB(255, 255, 255));
	m_btnSourcePreview.SetToolTipText(LoadStringById(IDS_CONTENTSOVERWRITEDLG_BUT003));
	m_btnTargetPreview.LoadBitmap(IDB_BTN_PREVIEW, RGB(255, 255, 255));
	m_btnTargetPreview.SetToolTipText(LoadStringById(IDS_CONTENTSOVERWRITEDLG_BUT003));

	CButton* pRadio = (CButton*)GetDlgItem(IDC_RENAME_RADIO);
	pRadio->SetCheck(TRUE);

	CBitmap bmp1;
	bmp1.LoadBitmap(IDB_CONTENTS_LIST32);

	CBitmap bmp2;
	bmp2.LoadBitmap(IDB_CONTENTS_LIST32_ERROR);

	CBitmap bmp3;
	bmp3.LoadBitmap(IDB_CONTENTS_LIST32_SERVER);

	if(m_imgContentsList.Create(32, 32, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_imgContentsList.Add(&bmp1, RGB(255,255,255)); //바탕의 회색이 마스크
		m_imgContentsList.Add(&bmp2, RGB(255,255,255)); //바탕의 회색이 마스크
		m_imgContentsList.Add(&bmp3, RGB(255,255,255)); //바탕의 회색이 마스크
	}//if

	WINDOWPLACEMENT stWndPlc;
	memset(&stWndPlc, 0x00, sizeof(WINDOWPLACEMENT));
	if(m_iconSource.GetWindowPlacement(&stWndPlc))
	{
		m_ptSourceIcon = CPoint(stWndPlc.rcNormalPosition.left-3, stWndPlc.rcNormalPosition.top-3);
	}//if
	m_iconSource.ShowWindow(SW_HIDE);

	memset(&stWndPlc, 0x00, sizeof(WINDOWPLACEMENT));
	if(m_iconTarget.GetWindowPlacement(&stWndPlc))
	{
		m_ptTargetIcon = CPoint(stWndPlc.rcNormalPosition.left-3, stWndPlc.rcNormalPosition.top-3);
	}//if
	m_iconTarget.ShowWindow(SW_HIDE);

	CString strPath, strTime;
	if(m_pinfoSource != NULL)
	{
		if(m_pinfoSource->bNameChanged)
		{
			m_staticSourceName.SetWindowText(m_pinfoSource->strChngFileName);
		}
		else
		{
			m_staticSourceName.SetWindowText(m_pinfoSource->strFilename);
		}//if

		strPath = m_pinfoSource->strLocalLocation + m_pinfoSource->strFilename;
		m_staticSourcePath.SetWindowText(strPath);

		CFileStatus fs;
		if(CFile::GetStatus(strPath, fs)){
			strTime = fs.m_mtime.Format("%Y/%m/%d %H:%m:%S");
			m_staticSourceTime.SetWindowText(strTime);

			CEnviroment::GetFileSize(strPath, fs.m_size);
			
			if(fs.m_size > 1024)
			{
				m_staticSourceSize.SetWindowText(::ToMoneyTypeString((ULONG)(fs.m_size/1024)) + " KByte");
			}
			else
			{
				m_staticSourceSize.SetWindowText(::ToMoneyTypeString((ULONG)(fs.m_size)) + " Byte");
			}
		}
	}//if

	if(m_pinfoTarget != NULL)
	{
		strPath = m_pinfoTarget->strLocalLocation + m_pinfoTarget->strFilename;
		m_staticTargetName.SetWindowText(m_pinfoTarget->strFilename);
		m_staticTargetPath.SetWindowText("UBC contents folder");
		m_strOrgContentsName = m_pinfoTarget->strFilename;
		m_edtContentsName.SetWindowText(m_strOrgContentsName);

		CFileStatus fs;
		if(CFile::GetStatus(strPath, fs)){
			strTime = fs.m_mtime.Format("%Y/%m/%d %H:%m:%S");
			m_staticTargetTime.SetWindowText(strTime);

			CEnviroment::GetFileSize(strPath, fs.m_size);

			if(fs.m_size > 1024)
			{
				m_staticTargetSize.SetWindowText(::ToMoneyTypeString((ULONG)(fs.m_size/1024)) + " KByte");
			}
			else
			{
				m_staticTargetSize.SetWindowText(::ToMoneyTypeString((ULONG)(fs.m_size)) + " Byte");
			}
		}
	}//if

	m_edtContentsName.EnableWindow(FALSE);
	OnBnClickedRenameRadio();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Source와 Target의 콘텐츠 정보를 설정한다. \n
/// @param (CONTENTS_INFO*) pinfoSource : (in) Source 콘텐츠 파일의 정보
/// @param (CONTENTS_INFO*) pinfoTarget : (in) Target 콘텐츠 파일의 정보
/////////////////////////////////////////////////////////////////////////////////
void CContentsOverwriteDlg::SetContentsInfo(CONTENTS_INFO* pinfoSource, CONTENTS_INFO* pinfoTarget)
{
	m_pinfoSource = pinfoSource;
	m_pinfoTarget = pinfoTarget;
}


void CContentsOverwriteDlg::OnBnClickedNocopyRadio()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_sContentsCopy = E_CONTENTS_COPY_USE_EXIST;
	m_edtContentsName.SetWindowText(m_strOrgContentsName);
	m_edtContentsName.EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_ALLFILE)->EnableWindow(TRUE);
}

void CContentsOverwriteDlg::OnBnClickedSourcePreviewBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_pinfoSource == NULL)
	{
		return;
	}//if

	CContentsDialog dlg;
//	dlg.SetDocument(m_pDocument);
	dlg.SetContentsInfo(*(m_pinfoSource));
	dlg.SetPreviewMode(true);
	dlg.DoModal();
}

void CContentsOverwriteDlg::OnBnClickedTargetPreviewBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_pinfoTarget == NULL)
	{
		return;
	}//if

	CContentsDialog dlg;
//	dlg.SetDocument(m_pDocument);
	dlg.SetContentsInfo(*m_pinfoTarget);
	dlg.SetPreviewMode(true);
	dlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용이 가능한 파일이름을 만들어 준다 \n
/////////////////////////////////////////////////////////////////////////////////
void CContentsOverwriteDlg::MakeAvailableName(void)
{
	CString strNumberFileName, strCompareExist;
	CString strExt = "";
	//int nIdx = m_strOrgContentsName.Find(".", 0);
	int nIdx = m_strOrgContentsName.ReverseFind('.');
	if(nIdx != -1)
	{
		int nCount = m_strOrgContentsName.GetLength() - nIdx;
		strNumberFileName = m_strOrgContentsName.Left(nIdx);
		strExt = m_strOrgContentsName.Right(nCount);
	}
	else
	{
		strNumberFileName = m_strOrgContentsName;
	}//if

	//(콘텐츠 파일 정보를 ini 파일에 기록할 때, '[', ']' 문자가 있으면 오류 발생 하므로...
	//만약 콘텐츠 파일 이름에 '[', ']'가 있다면 '(', ')'로 변경한다.
	strNumberFileName = CPreventChar::GetInstance()->ConvertToAvailableChar(strNumberFileName);
//	if( (strNumberFileName.Find("[", 0) != -1) ||
//		(strNumberFileName.Find("]", 0) != -1) )
//	{
//		strNumberFileName.Replace('[', '(');
//		strNumberFileName.Replace(']', ')');
//	}//if

	//ENC 폴더에 같은 파일 이름이 있다면 중복된 파일이름 변경
	strCompareExist = m_pinfoTarget->strLocalLocation;
	CString strTmpFileName = strNumberFileName;
	strCompareExist.Append(strNumberFileName);
	strCompareExist.Append(strExt);
	int nSeq = 1;
	while(::PathFileExists(strCompareExist))
	{
		if(nSeq == 99)
		{
			strTmpFileName = strNumberFileName;
			nSeq = 1;
		}//if
		strNumberFileName.Format("%s_%02d", strTmpFileName, nSeq++);
		strCompareExist = m_pinfoTarget->strLocalLocation;
		strCompareExist.Append(strNumberFileName);
		strCompareExist.Append(strExt);
	}//while

	//이름이 바뀌었다면...
	m_edtContentsName.SetWindowText(strNumberFileName + strExt);
	m_edtContentsName.EnableWindow(TRUE);
	m_edtContentsName.SetFocus();
	m_edtContentsName.SetSel(0, -1);
}

void CContentsOverwriteDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
	m_imgContentsList.Draw(&dc, m_pinfoSource->nContentsType+1, m_ptSourceIcon, ILD_TRANSPARENT);

	m_imgContentsList.Draw(&dc, m_pinfoTarget->nContentsType+1, m_ptTargetIcon, ILD_TRANSPARENT);


	/*
	CDC* pDc = NULL;
	CRect rtIcon;
	CPoint ptIcon;

	if(m_pinfoSource)
	{
		pDc = m_iconSource.GetDC();
		if(pDc)
		{
			m_iconSource.GetClientRect(&rtIcon);
			ptIcon.x = rtIcon.left;
			ptIcon.y = rtIcon.top;
			m_imgContentsList.Draw(pDc, m_pinfoSource->nContentsType+1, ptIcon, ILD_TRANSPARENT);
		}//if
		ReleaseDC(pDc);
	}//if

	if(m_pinfoTarget)
	{
		pDc = m_iconTarget.GetDC();
		if(pDc)
		{
			m_iconTarget.GetClientRect(&rtIcon);
			ptIcon.x = rtIcon.left-3;
			ptIcon.y = rtIcon.top-3;
			m_imgContentsList.Draw(pDc, m_pinfoTarget->nContentsType+1, ptIcon, ILD_NORMAL);
		}//if	
		ReleaseDC(pDc);
	}//if
	*/
}
