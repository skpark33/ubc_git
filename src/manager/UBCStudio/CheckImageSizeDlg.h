#pragma once
#include "resource.h"

// CCheckImageSizeDlg dialog
class CCheckImageSizeDlg : public CDialog
{
	DECLARE_DYNAMIC(CCheckImageSizeDlg)

public:
	CCheckImageSizeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCheckImageSizeDlg();

// Dialog Data
	enum { IDD = IDD_IMGSIZECHECK };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
};
