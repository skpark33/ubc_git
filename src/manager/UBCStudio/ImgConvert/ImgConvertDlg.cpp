// ImgConvertDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "ImgConvert.h"
#include "ImgConvertDlg.h"
#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CImgConvertDlg 대화 상자




CImgConvertDlg::CImgConvertDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImgConvertDlg::IDD, pParent)
	, m_bkeepRatio(true)
	, m_strTargetWidth(_T(""))
	, m_strTargetHeight(_T(""))
{
	m_clsConvert.SetProgressObject(this);
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CImgConvertDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_KEEP_RATIO_CHECK, m_checkKeepRatio);
	DDX_Control(pDX, IDC_SOURCE_LIST, m_listSource);
	DDX_Control(pDX, IDC_TARGET_LIST, m_listTarget);
	DDX_Text(pDX, IDC_TARGET_WIDTH_EDT, m_strTargetWidth);
	DDX_Text(pDX, IDC_TARGET_HEIGHT_EDT, m_strTargetHeight);
}

BEGIN_MESSAGE_MAP(CImgConvertDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_KEEP_RATIO_CHECK, &CImgConvertDlg::OnBnClickedKeepRatioCheck)
	ON_BN_CLICKED(IDOK, &CImgConvertDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_SOURCE_ADD_BTN, &CImgConvertDlg::OnBnClickedSourceAddBtn)
	ON_BN_CLICKED(IDC_SOURCE_DEL_BTN, &CImgConvertDlg::OnBnClickedSourceDelBtn)
	ON_BN_CLICKED(IDC_SOURCE_CONVERT_BTN, &CImgConvertDlg::OnBnClickedSourceConvertBtn)
	ON_BN_CLICKED(IDC_CLEARL_BTN, &CImgConvertDlg::OnBnClickedClearlBtn)
END_MESSAGE_MAP()


// CImgConvertDlg 메시지 처리기

BOOL CImgConvertDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	//디폴트로 비율을 유지한다.
	m_checkKeepRatio.SetCheck(m_bkeepRatio);

	//Source 리스트 컨트롤 초기화
	m_listSource.SetExtendedStyle(LVS_EX_CHECKBOXES  | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	

	CRect rectList;
	m_listSource.GetClientRect(&rectList);
	int nWidth = rectList.Width();
	m_listSource.InsertColumn(0, "No.", LVCFMT_LEFT, (nWidth/10)*1);
	m_listSource.InsertColumn(1, "Path", LVCFMT_LEFT, (nWidth/10)*3);
	m_listSource.InsertColumn(2, "Width", LVCFMT_RIGHT, (nWidth/10)*2);
	m_listSource.InsertColumn(3, "Height", LVCFMT_RIGHT, (nWidth/10)*2);
	m_listSource.InsertColumn(4, "Size", LVCFMT_RIGHT, (nWidth/10)*2);

	m_listTarget.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_listTarget.InsertColumn(0, "No.", LVCFMT_LEFT, (nWidth/10)*1);
	m_listTarget.InsertColumn(1, "Path", LVCFMT_LEFT, (nWidth/10)*3);
	m_listTarget.InsertColumn(2, "Width", LVCFMT_RIGHT, (nWidth/10)*1.5);
	m_listTarget.InsertColumn(3, "Height", LVCFMT_RIGHT, (nWidth/10)*1.5);
	m_listTarget.InsertColumn(4, "Size", LVCFMT_RIGHT, (nWidth/10)*1.5);
	m_listTarget.InsertColumn(5, "Result", LVCFMT_RIGHT, (nWidth/10)*1.5);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CImgConvertDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CImgConvertDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CImgConvertDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
/*
void CImgConvertDlg::OnBnClickedSourceBrowseBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strFilter = "All Files (*.*)|*.*||";
	CFileDialog dlg(TRUE, _T(""), _T(""), OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFilter, this);
	if(dlg.DoModal() == IDOK)
	{
		m_strSoutcePath = dlg.GetPathName();

		char cDrive[_MAX_DRIVE]	= { 0x00 };
		char cDir[_MAX_DIR]		= { 0x00 };
		char cFname[_MAX_FNAME]	= { 0x00 };
		char cExt[_MAX_EXT]		= { 0x00 };

		_splitpath(m_strSoutcePath, cDrive, cDir, cFname, cExt);

		//Make Target file name
		CString strTmpName;
		int nIdx = 0;
		do
		{
			strTmpName.Format("%s%s%s(%d)%s", cDrive, cDir, cFname, ++nIdx, cExt);
		} while(_access(strTmpName, 0) == 0);

		m_strTargetPath = strTmpName;

		UpdateData(FALSE);
	}//if
}
*/
void CImgConvertDlg::OnBnClickedKeepRatioCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bkeepRatio = !m_bkeepRatio;
	m_checkKeepRatio.SetCheck(m_bkeepRatio);
}

void CImgConvertDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CImgConvertDlg::OnBnClickedSourceAddBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strFilter = "All Files (*.*)|*.*||";
	CFileDialog dlg(TRUE, _T(""), _T(""), OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFilter, this);
	if(dlg.DoModal() == IDOK)
	{
		CString strSourcePath = dlg.GetPathName();

		//원본 파일의 정보
		int nWidth, nHeight;
		ULONG ulSize;
		if(!m_clsConvert.GetFileInfo(strSourcePath, nWidth, nHeight, ulSize))
		{
			AfxMessageBox("Can not load source file !!!", MB_ICONEXCLAMATION);
			return;
		}//if

		InsertSourceItem(strSourcePath, nWidth, nHeight, ulSize);
	}//if
}

void CImgConvertDlg::OnBnClickedSourceDelBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nCnt = m_listSource.GetItemCount();
	for(int i=0; i<nCnt; i++)
	{
		if(m_listSource.GetCheck(i))
		{
			DeleteLsit(i);
		}//if
	}//for
}

void CImgConvertDlg::OnBnClickedSourceConvertBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if(m_strTargetWidth == _T("") ||
		m_strTargetHeight == _T(""))
	{
		AfxMessageBox("Target width & height must not be zero !!!", MB_ICONEXCLAMATION);
		return;
	}//if

	CString strSourcePath, strTargetPath;
	int nTargetWidth, nTargetHeight;
	ULONG ulTargetSize;
	char cDrive[_MAX_DRIVE]	= { 0x00 };
	char cDir[_MAX_DIR]		= { 0x00 };
	char cFname[_MAX_FNAME]	= { 0x00 };
	char cExt[_MAX_EXT]		= { 0x00 };
	bool bRet;
	CFileInfoList listFileInfo;

	int nCnt = m_listSource.GetItemCount();
	for(int i=0; i<nCnt; i++)
	{
		if(!m_listSource.GetCheck(i))
		{
			continue;
		}//if

		strSourcePath = m_listSource.GetItemText(i, 1);
		_splitpath(strSourcePath, cDrive, cDir, cFname, cExt);

		//Make Target file name
		CString strTmpName;
		int nIdx = 0;
		do
		{
			strTmpName.Format("%s%s%s(%d)%s", cDrive, cDir, cFname, ++nIdx, cExt);
		} while(_access(strTmpName, 0) == 0);

		strTargetPath = strTmpName;
		nTargetWidth = atoi(m_strTargetWidth);
		nTargetHeight = atoi(m_strTargetHeight);
/*
		bRet = m_clsConvert.ImgConvert(strSourcePath, strTargetPath, nTargetWidth, nTargetHeight, ulTargetSize, m_bkeepRatio);
		//결과를 Target list에 넣는다
		InsertTargetItem(strTargetPath, nTargetWidth, nTargetHeight, ulTargetSize, bRet);
*/
		CImgFileInfo clsFileInfo;
		clsFileInfo.m_strSourcePath = strSourcePath;
		clsFileInfo.m_strTargetPath = strTargetPath;
		clsFileInfo.m_nTargetWidth = nTargetWidth;
		clsFileInfo.m_nTargetHeight = nTargetHeight;

		listFileInfo.AddTail(clsFileInfo);

	}//if

	if(listFileInfo.GetCount() != 0)
	{
		m_clsConvert.ImgConvert(&listFileInfo);
		InsertTargetItem(&listFileInfo);

		listFileInfo.RemoveAll();
	}//if

}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 원본파일의 정보를 리스트에 넣는다. \n
/// @param (CString) strSourcePath : (in) 원본파일의 경로
/// @param (int) nWidth : (in) 원본파일의 넓이
/// @param (int) nHeight : (in) 원본파일의 높이
/// @param (ULONG) ulSize : (in) 원본파일의 크기(Byte);
/////////////////////////////////////////////////////////////////////////////////
void CImgConvertDlg::InsertSourceItem(CString strSourcePath, int nWidth, int nHeight, ULONG ulSize)
{
	int nCnt = m_listSource.GetItemCount();
	CString strTmp;
	strTmp.Format("%d", nCnt+1);

	m_listSource.InsertItem(nCnt, strTmp); 
	m_listSource.SetItemText(nCnt, 1, strSourcePath);
	strTmp.Format("%d", nWidth);
	m_listSource.SetItemText(nCnt, 2, strTmp);
	strTmp.Format("%d", nHeight);
	m_listSource.SetItemText(nCnt, 3, strTmp);
	strTmp.Format("%d", ulSize);
	m_listSource.SetItemText(nCnt, 4, strTmp);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 변환된 파일의 결과를 Target list 컨트롤에 넣는다 \n
/// @param (CString) strTargetPath : (in) 변환된 파일의 경로
/// @param (int) nTargetWidth : (in) 변환된 파일의 넓이
/// @param (int) nTargetHeight : (in) 변환된 파일의 높이
/// @param (ULONG) ulTargetSize : (in) 변환된 파일의 크기
/// @param (bool) bRet : (in) 변환된 결과
/////////////////////////////////////////////////////////////////////////////////
void CImgConvertDlg::InsertTargetItem(CString strTargetPath, int nTargetWidth, int nTargetHeight, ULONG ulTargetSize, bool bRet)
{
	int nCnt = m_listTarget.GetItemCount();
	CString strTmp;
	strTmp.Format("%d", nCnt+1);

	m_listTarget.InsertItem(nCnt, strTmp); 
	m_listTarget.SetItemText(nCnt, 1, strTargetPath);
	strTmp.Format("%d", nTargetWidth);
	m_listTarget.SetItemText(nCnt, 2, strTmp);
	strTmp.Format("%d", nTargetHeight);
	m_listTarget.SetItemText(nCnt, 3, strTmp);
	strTmp.Format("%d", ulTargetSize);
	m_listTarget.SetItemText(nCnt, 4, strTmp);
	if(bRet)
	{
		strTmp = _T("Success");
	}
	else
	{
		strTmp = _T("Fail");
	}//if
	m_listTarget.SetItemText(nCnt, 5, strTmp);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 변환된 파일의 결과를 Target list 컨트롤에 넣는다 \n
/// @param (CFileInfoList*) plistFileInfo : (in) 변환된 파일의 정보를 갖는 리스트
/////////////////////////////////////////////////////////////////////////////////
void CImgConvertDlg::InsertTargetItem(CFileInfoList* plistFileInfo)
{
	int nCnt;
	CString strTmp;
	POSITION pos = plistFileInfo->GetHeadPosition();
	while(pos)
	{
		CImgFileInfo& clsFileInfo = plistFileInfo->GetNext(pos);
		nCnt = m_listTarget.GetItemCount();
		strTmp.Format("%d", nCnt+1);

		m_listTarget.InsertItem(nCnt, strTmp); 
		m_listTarget.SetItemText(nCnt, 1, clsFileInfo.m_strTargetPath);
		strTmp.Format("%d", clsFileInfo.m_nTargetWidth);
		m_listTarget.SetItemText(nCnt, 2, strTmp);
		strTmp.Format("%d", clsFileInfo.m_nTargetHeight);
		m_listTarget.SetItemText(nCnt, 3, strTmp);
		strTmp.Format("%d", clsFileInfo.m_ulTargetSize);
		m_listTarget.SetItemText(nCnt, 4, strTmp);
		if(clsFileInfo.m_bResult)
		{
			strTmp = _T("Success");
		}
		else
		{
			strTmp = _T("Fail");
		}//if
		m_listTarget.SetItemText(nCnt, 5, strTmp);
	}//while
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 선택된 인덱스의 아이템을 Source, Target 리스트에서 삭제한다 \n
/// @param (int) nIndex : (in) Source list 컨트롤에서 선택된 삭제될 아이템의 인덱스
/////////////////////////////////////////////////////////////////////////////////
void CImgConvertDlg::DeleteLsit(int nIndex)
{
	if(nIndex >= m_listSource.GetItemCount())
	{
		return;
	}//if

	m_listSource.DeleteItem(nIndex);

	//Target list
	if(m_listTarget.GetItemCount() > nIndex)
	{
		m_listTarget.DeleteItem(nIndex);
	}//if
}

void CImgConvertDlg::OnBnClickedClearlBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_listSource.DeleteAllItems();
	m_listTarget.DeleteAllItems();
/*
	m_strTargetWidth = _T("");
	m_strTargetHeight = _T("");
	UpdateData(FALSE);
*/
}

void CImgConvertDlg::GetProgressValue(int nTotal, int nNow, int nPercent, CString strFileName)
{
	TRACE("==============================================\r\n");
	TRACE("\tTotal = %d\r\n", nTotal);
	TRACE("\tNow = %d\r\n", nNow);
	TRACE("\tPercent = %d\r\n", nPercent);
	TRACE("\tFile = %s\r\n", strFileName);
	TRACE("==============================================\r\n");
}
