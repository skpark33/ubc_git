// NetworkHostManageDlg.cpp : 구현 파일입니다.
//
#include "stdafx.h"
#include "UBCStudio.h"
#include "NetworkHostManageDlg.h"
#include "common\libscratch\scratchUtil.h"
#include "EditHostDlg.h"
#include "Enviroment.h"
#include "libFileTransfer\FTClient.h"
#include "ScanNetHost.h"

// CNetworkHostManageDlg 대화 상자입니다.
IMPLEMENT_DYNAMIC(CNetworkHostManageDlg, CDialog)

CNetworkHostManageDlg::CNetworkHostManageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNetworkHostManageDlg::IDD, pParent)
	, m_nFindHostType(0)
{
}

CNetworkHostManageDlg::~CNetworkHostManageDlg()
{
	ClearNetDeviceArray();
}

void CNetworkHostManageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_HOST_LIST, m_lcList);
	//	DDX_Control(pDX, IDC_REFRESH_HOST_BTN, m_btnRefresh);
	//	DDX_Control(pDX, IDC_ADD_BTN, m_btnAdd);
	//	DDX_Control(pDX, IDC_MODIFY_BTN, m_btnModify);
	//	DDX_Control(pDX, IDC_DELETE_BTN, m_btnDelete);
	//	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Radio(pDX, IDC_RB_FIND_HOST_TYPE, m_nFindHostType);
}

BEGIN_MESSAGE_MAP(CNetworkHostManageDlg, CDialog)
	ON_BN_CLICKED(IDC_REFRESH_HOST_BTN, &CNetworkHostManageDlg::OnBnClickedRefreshBtn)
	ON_BN_CLICKED(IDC_ADD_BTN, &CNetworkHostManageDlg::OnBnClickedAddBtn)
	ON_BN_CLICKED(IDC_MODIFY_BTN, &CNetworkHostManageDlg::OnBnClickedModifyBtn)
	ON_BN_CLICKED(IDC_DELETE_BTN, &CNetworkHostManageDlg::OnBnClickedDeleteBtn)
	ON_BN_CLICKED(IDOK, &CNetworkHostManageDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_ON_BTN, &CNetworkHostManageDlg::OnBnClickedOnBtn)
	ON_BN_CLICKED(IDC_OFF_BTN, &CNetworkHostManageDlg::OnBnClickedOffBtn)
	ON_BN_CLICKED(IDC_REBOOT_BTN, &CNetworkHostManageDlg::OnBnClickedRebootBtn)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_HOST_LIST, &CNetworkHostManageDlg::OnLvnColumnclickHostList)
	ON_NOTIFY(NM_DBLCLK, IDC_HOST_LIST, &CNetworkHostManageDlg::OnNMDblclkHostList)
	ON_BN_CLICKED(IDC_BTN_REMOTE, &CNetworkHostManageDlg::OnBnClickedBtnRemote)
END_MESSAGE_MAP()

// CNetworkHostManageDlg 메시지 처리기입니다.
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 호스트 정보를 갖는 배열을 비운다 \n
/////////////////////////////////////////////////////////////////////////////////
void CNetworkHostManageDlg::ClearNetDeviceArray()
{
	CNetDeviceInfo* pclsDevInfo = NULL;
	for(int i=0; i<m_aryNetDevice.GetCount(); i++)
	{
		pclsDevInfo = (CNetDeviceInfo*)m_aryNetDevice.GetAt(i);
		delete pclsDevInfo;
	}//for
	m_aryNetDevice.RemoveAll();
}

void CNetworkHostManageDlg::OnBnClickedRefreshBtn()
{
	if( UbcMessageBox(LoadStringById(IDS_NETWORKHOSTMANAGEDLG_MSG009), MB_YESNO) != IDYES ) return;

	UpdateData(TRUE);

	ScanNetHost(m_nFindHostType);

	LoadNetDeviceInfo();
	InitNetListCtrl();
}

void CNetworkHostManageDlg::OnBnClickedAddBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CStringArray aryHostName;
	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		aryHostName.Add(m_lcList.GetItemText(i, E_HOST));
	}//for

	CEditHostDlg dlg(this, "true", aryHostName);
	if(dlg.DoModal() == IDOK)
	{
		CNetDeviceInfo clsHost;
		clsHost.m_strDeviceName		  = dlg.m_strHostName;
		clsHost.m_strIP				  = dlg.m_strIPAddr;
//		clsHost.m_nFTPPort			  = atoi(dlg.m_strFTPPort);
		clsHost.m_nFTPPort			  = FT_PORT;
		clsHost.m_nSvrPort			  = atoi(dlg.m_strSvrPort);
		clsHost.m_bShutdownAuto       = dlg.m_bShutdownAuto;
		clsHost.m_strShutdownTime	  = dlg.m_strShutdownTime;
		clsHost.m_bShutdownWeekday    = dlg.m_bShutdownWeekday;
		clsHost.m_strWeekShutdownTime = dlg.m_strWeekShutdownTime;
		clsHost.m_strUserCreate		  = dlg.m_strUserCreate;
		clsHost.m_strID				  = "sqi";
		clsHost.m_strPWD			  = "utvdev7";
		clsHost.m_strMacAddr		  = dlg.m_strMacAddress;
		clsHost.m_nMonitorCnt		  = dlg.m_nMonitorCnt;
		clsHost.m_strDescript		  = dlg.m_szDesc;

		WriteDeviceInfo(clsHost);
		LoadNetDeviceInfo();
		InitNetListCtrl();

		if(!clsHost.m_strIP.IsEmpty())
		{
			CWaitMessageBox wait;
			if(dlg.m_bShutdownAuto)
			{
				scratchUtil::getInstance()->socketAgent(clsHost.m_strIP
													  , clsHost.m_nSvrPort
													  , "shutdownTime"
													  , clsHost.m_strShutdownTime.IsEmpty()?"NOSET":clsHost.m_strShutdownTime
													  );
			}

			if(dlg.m_bShutdownWeekday)
			{
				scratchUtil::getInstance()->socketAgent(clsHost.m_strIP
													  , clsHost.m_nSvrPort
													  , "week_shutdownTime"
													  , clsHost.m_strWeekShutdownTime.IsEmpty()?"NOSET":clsHost.m_strWeekShutdownTime
													  );
			}
		}
	}//if
}

void CNetworkHostManageDlg::OnBnClickedModifyBtn()
{
	CStringArray aryHostName;
	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		aryHostName.Add(m_lcList.GetItemText(i, E_HOST));
	}//for

	CString strUserCreate, strOldHostName;
	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		if(m_lcList.GetCheck(i))
		{
			strUserCreate = m_lcList.GetItemText(i, E_USRCREATE);
			CEditHostDlg dlg(this, strUserCreate, aryHostName);

			dlg.m_strHostName		  = m_lcList.GetItemText(i, E_HOST);
			strOldHostName			  = dlg.m_strHostName; 
			dlg.m_strIPAddr			  = m_lcList.GetItemText(i, E_IPADDR);
			dlg.m_strFTPPort		  = m_lcList.GetItemText(i, E_FTPPORT);
			dlg.m_strSvrPort		  = m_lcList.GetItemText(i, E_SVRPORT);
			dlg.m_strShutdownTime	  = m_lcList.GetItemText(i, E_SDTIME);
			dlg.m_strWeekShutdownTime = m_lcList.GetItemText(i, E_WSDTIME);
			dlg.m_szDesc			  = m_lcList.GetItemText(i, E_DESCRIPT);
			
			dlg.m_strMacAddress = m_lcList.GetItemText(i, E_MACADDR);
			CString szDisplay = m_lcList.GetItemText(i, E_DISPLYCNT);
			dlg.m_nMonitorCnt = szDisplay.IsEmpty()?1:atoi(szDisplay);

			if(dlg.DoModal() == IDOK)
			{
				CNetDeviceInfo clsHost;
				clsHost.m_strDeviceName		  = dlg.m_strHostName;
				clsHost.m_strIP				  = dlg.m_strIPAddr;
//				clsHost.m_nFTPPort			  = atoi(dlg.m_strFTPPort);
				clsHost.m_nFTPPort			  = FT_PORT;
				clsHost.m_nSvrPort			  = atoi(dlg.m_strSvrPort);
				clsHost.m_bShutdownAuto       = dlg.m_bShutdownAuto;
				clsHost.m_strShutdownTime	  = dlg.m_strShutdownTime;
				clsHost.m_bShutdownWeekday    = dlg.m_bShutdownWeekday;
				clsHost.m_strWeekShutdownTime = dlg.m_strWeekShutdownTime;
				clsHost.m_strUserCreate		  = dlg.m_strUserCreate;
				clsHost.m_strID				  = "sqi";
				clsHost.m_strPWD			  = "utvdev7";
				clsHost.m_strMacAddr		  = dlg.m_strMacAddress;
				clsHost.m_nMonitorCnt		  = dlg.m_nMonitorCnt;
				clsHost.m_strDescript		  = dlg.m_szDesc;

				if(strOldHostName != dlg.m_strHostName)
				{
					//먼저 수정되기전의 host 정보를 ini 파일에서 삭제한뒤에,
					//새로운 정보를 ini파일에 쓴다.
					DeleteDeviceInfo(strOldHostName);
				}//if
				WriteDeviceInfo(clsHost);

				if(!clsHost.m_strIP.IsEmpty())
				{
					CWaitMessageBox wait;
					if(dlg.m_bShutdownAuto)
					{
						scratchUtil::getInstance()->socketAgent(clsHost.m_strIP
															  , clsHost.m_nSvrPort
															  , "shutdownTime"
															  , clsHost.m_strShutdownTime.IsEmpty()?"NOSET":clsHost.m_strShutdownTime
															  );
					}

					if(dlg.m_bShutdownWeekday)
					{
						scratchUtil::getInstance()->socketAgent(clsHost.m_strIP
															  , clsHost.m_nSvrPort
															  , "week_shutdownTime"
															  , clsHost.m_strWeekShutdownTime.IsEmpty()?"NOSET":clsHost.m_strWeekShutdownTime
															  );
					}
				}
			}//if
		}//if
	}//for

	LoadNetDeviceInfo();
	InitNetListCtrl();
}

void CNetworkHostManageDlg::OnBnClickedDeleteBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(MessageBox(LoadStringById(IDS_NETWORKHOSTMANAGEDLG_MSG001), LoadStringById(IDS_NETWORKHOSTMANAGEDLG_MSG002), MB_OKCANCEL|MB_ICONQUESTION) == IDNO)
	{
		return;
	}//if

	CString strHostName;

	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		if(m_lcList.GetCheck(i))
		{
			strHostName = m_lcList.GetItemText(i, E_HOST);
			DeleteDeviceInfo(strHostName);
		}//if
	}//for

	LoadNetDeviceInfo();
	InitNetListCtrl();
}

void CNetworkHostManageDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	OnOK();
}

BOOL CNetworkHostManageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
//	m_btnRefresh.LoadBitmap(IDB_BTN_REFRESH, RGB(255, 255, 255));
//	m_btnAdd.LoadBitmap(IDB_BTN_ADD, RGB(255, 255, 255));
//	m_btnModify.LoadBitmap(IDB_BTN_MODIFY, RGB(255, 255, 255));
//	m_btnDelete.LoadBitmap(IDB_BTN_DELETE, RGB(255, 255, 255));
//	m_btnOK.LoadBitmap(IDB_BTN_OK, RGB(255, 255, 255));

//	m_btnRefresh.SetToolTipText("Refresh network host");
//	m_btnAdd.SetToolTipText("Add new network host");
//	m_btnModify.SetToolTipText("Modify selected host");
//	m_btnDelete.SetToolTipText("Delete network host");
//	m_btnOK.SetToolTipText("OK");

	CBitmap bmp;
	bmp.LoadBitmap(IDB_CHECK);
	m_imgCheck.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1);
	m_imgCheck.Add(&bmp, RGB(255,255,255));

	m_lcList.SetImageList(&m_imgCheck, LVSIL_SMALL);
//	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
	
	CRect clsRect;
	m_lcList.GetClientRect(&clsRect);
	int nWidth = clsRect.Width();

	m_lcList.InsertColumn(eCheck     , _T("")                     , LVCFMT_LEFT  , 22           );
	m_lcList.InsertColumn(E_HOST     , LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST001), LVCFMT_LEFT  , nWidth*2.2/10);
	m_lcList.InsertColumn(E_IPADDR   , LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST002), LVCFMT_CENTER, nWidth*  2/10);
	m_lcList.InsertColumn(E_FTPPORT  , LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST003), LVCFMT_RIGHT , nWidth*1.2/10);
	m_lcList.InsertColumn(E_SVRPORT  , LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST004), LVCFMT_RIGHT , nWidth*1.2/10);
	m_lcList.InsertColumn(E_SDTIME   , LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST005), LVCFMT_RIGHT , 50           );
	m_lcList.InsertColumn(E_WSDTIME  , LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST010), LVCFMT_RIGHT , 50           );
	m_lcList.InsertColumn(E_MACADDR  , LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST006), LVCFMT_CENTER, nWidth*1.2/10);
	m_lcList.InsertColumn(E_DISPLYCNT, LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST007), LVCFMT_CENTER, nWidth*1.2/10);
	m_lcList.InsertColumn(E_USRCREATE, LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST008), LVCFMT_CENTER, nWidth*1.2/10);
	m_lcList.InsertColumn(E_DESCRIPT , LoadStringById(IDS_NETWORKHOSTMANAGEDLG_LST009), LVCFMT_CENTER, nWidth*2.2/10);

	m_lcList.InitHeader(IDB_LIST_HEADER);

	//ScanNetHost();

	LoadNetDeviceInfo();
	InitNetListCtrl();
	
	//드라이브명으로 정렬 시킨다
	//m_lcList.SetSortHeader(E_HOST);
	//m_lcList.SortItems(CompareList, (DWORD_PTR)this);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 호스트 list 컨트롤의 데이터를 설정 \n
/////////////////////////////////////////////////////////////////////////////////
void CNetworkHostManageDlg::InitNetListCtrl()
{
	m_lcList.DeleteAllItems();
	
	if(m_aryNetDevice.GetCount() == 0)
	{
		//UbcMessageBox("Can't found any network host !!!", MB_ICONWARNING);
		//CDialog::OnCancel();
		return;
	}//if

	CNetDeviceInfo* pclsNetInfo = NULL;
	CString szBuff;
	for(int i=0; i<m_aryNetDevice.GetCount(); i++)
	{
		pclsNetInfo = (CNetDeviceInfo*)m_aryNetDevice.GetAt(i);

		m_lcList.InsertItem(i, "", -1);

		m_lcList.SetItem(i, E_HOST, LVIF_IMAGE|LVIF_TEXT, pclsNetInfo->m_strDeviceName
						, (pclsNetInfo->m_strAlive.Compare("true") ? 5 : 4)
						, 0, 0, NULL);
		
		m_lcList.SetItemText(i, E_IPADDR, pclsNetInfo->m_strIP);
		
		szBuff.Format("%d", pclsNetInfo->m_nFTPPort);
		m_lcList.SetItemText(i, E_FTPPORT, szBuff);
		
		szBuff.Format("%d", pclsNetInfo->m_nSvrPort);
		m_lcList.SetItemText(i, E_SVRPORT, szBuff);
		
		m_lcList.SetItemText(i, E_SDTIME, pclsNetInfo->m_strShutdownTime);
		m_lcList.SetItemText(i, E_WSDTIME, pclsNetInfo->m_strWeekShutdownTime);
		m_lcList.SetItemText(i, E_MACADDR, pclsNetInfo->m_strMacAddr);

		szBuff.Format("%d", pclsNetInfo->m_nMonitorCnt);
		m_lcList.SetItemText(i, E_DISPLYCNT, szBuff);

		m_lcList.SetItemText(i, E_USRCREATE, pclsNetInfo->m_strUserCreate);
		m_lcList.SetItemText(i, E_DESCRIPT, pclsNetInfo->m_strDescript);

		m_lcList.SetItemData(i, (DWORD)pclsNetInfo);
	}//for
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 네트워크상의 호스트를 조회하여 ini 파일에 기록한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CNetworkHostManageDlg::ScanNetHost(int nType)
{
	CWaitMessageBox wait;

	if(nType == 1)
	{
		CScanNetHost scan;
		scan.AutoSearch();
	}
	else
	{
		scratchUtil* aUtil = scratchUtil::getInstance();
		aUtil->autoSearch(14008);
		//BeginWaitCursor();
		//scratchUtil* aUtil = scratchUtil::getInstance();
		//if(!aUtil->autoSearch(14008))
		//{
		//}//if
		//EndWaitCursor();
	}
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ini파일로부터 호스트들의 정보를 읽는다 \n
/////////////////////////////////////////////////////////////////////////////////
void CNetworkHostManageDlg::LoadNetDeviceInfo()
{
	ClearNetDeviceArray();

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString strInfoPath = szDrive;
	strInfoPath.Append("\\");
	strInfoPath.Append(szPath);
	strInfoPath.Append("\\data\\");
	strInfoPath.Append(ENVIROMENT_INI);

	char buf[1024];

	::GetPrivateProfileString("NetworkDevice", "DeviceNameList", "", buf, 1024, strInfoPath);
	CString strNameList = buf;
	//strNameList.Replace(" ", "");

	int nPos = 0;
	CString strTok = strNameList.Tokenize(",", nPos);
	while(strTok != "")
	{
		CNetDeviceInfo* pclsDevInfo = new CNetDeviceInfo();

		strTok.Trim();
		pclsDevInfo->m_strDeviceName = strTok;

		::GetPrivateProfileString(strTok, "IP", "", buf, 1024, strInfoPath);		
		pclsDevInfo->m_strIP = buf;

//		::GetPrivateProfileString(strTok, "FTPPort", "", buf, 1024, strInfoPath);
//		pclsDevInfo->m_nFTPPort = atoi(buf);
		pclsDevInfo->m_nFTPPort = FT_PORT;
		
		::GetPrivateProfileString(strTok, "SvrPort", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_nSvrPort = atoi(buf);
		
		::GetPrivateProfileString(strTok, "ID", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strID = buf;
		
		::GetPrivateProfileString(strTok, "PWD", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strPWD = buf;

		::GetPrivateProfileString(strTok, "SHUTDOWNTIME", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strShutdownTime = buf;

		::GetPrivateProfileString(strTok, "WEEK_SHUTDOWNTIME", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strWeekShutdownTime = buf;
		
		::GetPrivateProfileString(strTok, "USERCREATE", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strUserCreate = buf;

		::GetPrivateProfileString(strTok, "MAC", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strMacAddr = buf;

		::GetPrivateProfileString(strTok, "Alive", "true", buf, 1024, strInfoPath);
		pclsDevInfo->m_strAlive = buf;

		pclsDevInfo->m_nMonitorCnt = ::GetPrivateProfileInt(strTok, "MonitorCount", 1, strInfoPath);

		::GetPrivateProfileString(strTok, "Description", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strDescript = buf;

		m_aryNetDevice.Add(pclsDevInfo);
		strTok = strNameList.Tokenize(",", nPos);
	}//while
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 호스트의 정보를 ini파일에 기록한다 \n
/// @param (CNetDeviceInfo&) clsHost : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CNetworkHostManageDlg::WriteDeviceInfo(CNetDeviceInfo& clsHost)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString strInfoPath = szDrive;
	strInfoPath.Append("\\");
	strInfoPath.Append(szPath);
	strInfoPath.Append("\\data\\");
	strInfoPath.Append(ENVIROMENT_INI);

	char buf[1024];

	::GetPrivateProfileString("NetworkDevice", "DeviceNameList", "", buf, 1024, strInfoPath);
	CString strNameList = buf;
	bool bExist = false;
	int nPos = 0;
	CString strTok = strNameList.Tokenize(",", nPos);
	while(strTok != "")
	{
		strTok.TrimLeft();
		strTok.TrimRight();
		if(strTok == clsHost.m_strDeviceName)
		{
			bExist = true;
		}//if
		strTok = strNameList.Tokenize(",", nPos);
	}//while
	
	if(!bExist)
	{
		strNameList.Append(",");
		strNameList.Append(clsHost.m_strDeviceName);
	}//if

	::WritePrivateProfileString("NetworkDevice", "DeviceNameList", strNameList, strInfoPath);	
	::WritePrivateProfileString(clsHost.m_strDeviceName, "IP", clsHost.m_strIP, strInfoPath);
	
	CString strBuf;
	strBuf.Format("%d", clsHost.m_nFTPPort);
	::WritePrivateProfileString(clsHost.m_strDeviceName, "FTPPort", strBuf, strInfoPath);
	
	strBuf.Format("%d", clsHost.m_nSvrPort);
	::WritePrivateProfileString(clsHost.m_strDeviceName, "SvrPort", strBuf, strInfoPath);

	::WritePrivateProfileString(clsHost.m_strDeviceName, "ID", clsHost.m_strID, strInfoPath);	
	::WritePrivateProfileString(clsHost.m_strDeviceName, "PWD", clsHost.m_strPWD,strInfoPath);	
	if(clsHost.m_bShutdownAuto)
	{
		::WritePrivateProfileString(clsHost.m_strDeviceName, "SHUTDOWNTIME", clsHost.m_strShutdownTime, strInfoPath);
	}
	if(clsHost.m_bShutdownWeekday)
	{
		::WritePrivateProfileString(clsHost.m_strDeviceName, "WEEK_SHUTDOWNTIME", clsHost.m_strWeekShutdownTime, strInfoPath);	
	}
	::WritePrivateProfileString(clsHost.m_strDeviceName, "USERCREATE", clsHost.m_strUserCreate, strInfoPath);

	::WritePrivateProfileString(clsHost.m_strDeviceName, "MAC", clsHost.m_strMacAddr , strInfoPath);

	strBuf.Format("%d", clsHost.m_nMonitorCnt);
	::WritePrivateProfileString(clsHost.m_strDeviceName, "MonitorCount", strBuf, strInfoPath);

	strBuf = clsHost.m_strDescript;
	::WritePrivateProfileString(clsHost.m_strDeviceName, "Description", strBuf, strInfoPath);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ini 파일에서 호스트 정보를 삭제한다 \n
/// @param (CString) strHostName : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CNetworkHostManageDlg::DeleteDeviceInfo(CString strHostName)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString strInfoPath = szDrive;
	strInfoPath.Append("\\");
	strInfoPath.Append(szPath);
	strInfoPath.Append("\\data\\");
	strInfoPath.Append(ENVIROMENT_INI);

	char buf[1024];

	//ini 파일의 섹션 삭제
	::WritePrivateProfileString(strHostName, NULL, NULL, strInfoPath);

	//Device name list 에서 삭제
	::GetPrivateProfileString("NetworkDevice", "DeviceNameList", "", buf, 1024, strInfoPath);
	CString strNameList = buf;
	//strNameList.Replace(" ", "");

	int nPos = 0;
	CString strNewNameList = "";
	CString strTok = strNameList.Tokenize(",", nPos);
	while(strTok != "")
	{
		strTok.TrimLeft();
		strTok.TrimRight();
		if(strTok != strHostName)
		{
			strNewNameList += strTok;
			strNewNameList += ",";
		}//if
		strTok = strNameList.Tokenize(",", nPos);
	}//while
	strNewNameList.TrimRight(",");
	::WritePrivateProfileString("NetworkDevice", "DeviceNameList", strNewNameList, strInfoPath);


	/*strHostName.Append(",");
	strNameList.Replace(strHostName, "");
	::WritePrivateProfileString("NetworkDevice", "DeviceNameList", strNameList, strInfoPath);*/
}

void CNetworkHostManageDlg::OnBnClickedOnBtn()
{
	if(MessageBox(LoadStringById(IDS_NETWORKHOSTMANAGEDLG_MSG003), LoadStringById(IDS_NETWORKHOSTMANAGEDLG_MSG004), MB_YESNO) != IDYES)
		return;

	CStringArray aryHostName;
	for(int i=0; i<m_lcList.GetItemCount(); i++){
		aryHostName.Add(m_lcList.GetItemText(i, E_HOST));
	}

	CString strUserCreate, strOldHostName;
	for(int i=0; i<m_lcList.GetItemCount(); i++){
		if(m_lcList.GetCheck(i)){
			CString szMac = m_lcList.GetItemText(i, E_MACADDR);
			
			if(!szMac.IsEmpty())
				scratchUtil::getInstance()->powerOn( szMac, 14004);
		}
		m_lcList.SetCheck(i, 0);
	}
}

void CNetworkHostManageDlg::OnBnClickedOffBtn()
{
	if(MessageBox(LoadStringById(IDS_NETWORKHOSTMANAGEDLG_MSG005), LoadStringById(IDS_NETWORKHOSTMANAGEDLG_MSG006), MB_YESNO) != IDYES)
		return;

	CStringArray aryHostName;
	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		aryHostName.Add(m_lcList.GetItemText(i, E_HOST));
	}

	CString strUserCreate, strOldHostName;
	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		if(m_lcList.GetCheck(i))
		{
			CString szIP = m_lcList.GetItemText(i, E_IPADDR);
			int nPort = atoi(m_lcList.GetItemText(i, E_SVRPORT));

			if(!szIP.IsEmpty())
				scratchUtil::getInstance()->socketAgent( szIP, nPort, "FV_SHUTDOWN", "");
		}
		m_lcList.SetCheck(i, 0);
	}
}

void CNetworkHostManageDlg::OnBnClickedRebootBtn()
{
	if(MessageBox(LoadStringById(IDS_NETWORKHOSTMANAGEDLG_MSG007), LoadStringById(IDS_NETWORKHOSTMANAGEDLG_MSG008), MB_YESNO) != IDYES)
		return;

	CStringArray aryHostName;
	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		aryHostName.Add(m_lcList.GetItemText(i, E_HOST));
	}

	CString strUserCreate, strOldHostName;
	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		if(m_lcList.GetCheck(i))
		{
			CString szIP = m_lcList.GetItemText(i, E_IPADDR);
			int nPort = atoi(m_lcList.GetItemText(i, E_SVRPORT));

			if(!szIP.IsEmpty())
				scratchUtil::getInstance()->socketAgent( szIP, nPort, "FV_REBOOT", "");
		}
		m_lcList.SetCheck(i, 0);
	}
}

void CNetworkHostManageDlg::OnLvnColumnclickHostList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int	nColumn = pNMLV->iSubItem;

	if(nColumn == eCheck)
	{
		BOOL bCheck = !m_lcList.GetCheckHdrState();
		for(int nRow=0; nRow<m_lcList.GetItemCount() ;nRow++)
		{
			m_lcList.SetCheck(nRow, bCheck);
		}

		m_lcList.SetCheckHdrState(bCheck);
	}
	else
	{
		m_lcList.SetSortHeader(nColumn);
		m_lcList.SortItems(CompareList, (DWORD_PTR)this);
	}
}

int CNetworkHostManageDlg::CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	return ((CNetworkHostManageDlg*)lParam)->m_lcList.Compare(lParam1, lParam2);
}

void CNetworkHostManageDlg::OnNMDblclkHostList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	if(pNMLV->iItem < 0) return;

	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		m_lcList.SetCheck(i, (pNMLV->iItem == i));
	}

	OnBnClickedModifyBtn();

	*pResult = 0;
}

void CNetworkHostManageDlg::OnBnClickedBtnRemote()
{
	//POSITION pos = m_lcList.GetFirstSelectedItemPosition();
	//if(pos == NULL) return;

	//int nItem = m_lcList.GetNextSelectedItem(pos);

	for(int nItem=0; nItem<m_lcList.GetItemCount(); nItem++)
	{
		if(!m_lcList.GetCheck(nItem)) continue;

		CString strIpAddress = m_lcList.GetItemText(nItem, E_IPADDR);

		if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // 창일향 : 원격 로그인은 텔넷으로 대체한다. (기존 UKIPA version 처럼)
		{
			CString szParam = " ";
			szParam.Format(" %s 2233", strIpAddress);

			HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
		}
	else if( CEnviroment::eKPOST  == GetEnvPtr()->m_Customer ) // android adb call
	{
		CString szPath;
		szPath.Format(_T("%s%s\\adb.exe"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

		CString szParam = " ";
		szParam.Format(" /k %s connect %s ", szPath, strIpAddress);
		TraceLog((szParam));

		HINSTANCE nRet = ShellExecute(NULL, NULL, "cmd.exe", szParam, "", SW_SHOWNORMAL);
	}

	else if( CEnviroment::eKPOST  == GetEnvPtr()->m_Customer ) // android adb call
	{
		CString szPath;
		szPath.Format(_T("%s%s\\adb.exe"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

		CString szParam = " ";
		szParam.Format(" /k %s connect %s ", szPath, strIpAddress);
		TraceLog((szParam));

		HINSTANCE nRet = ShellExecute(NULL, NULL, "cmd.exe", szParam, "", SW_SHOWNORMAL);
	}
		else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
		{
			int nVncPort = atoi(m_lcList.GetItemText(nItem, E_HOST).Right(5));

			CString szPath;
#ifdef _DEBUG
		szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
		szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif

			CString szParam;
			szParam.Format(_T("%s:%d"), strIpAddress, nVncPort);

			HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);
		}
		else if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
		{
			CString szParam = " ";
			szParam += strIpAddress;

			HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
		}
	}
}
