#pragma once

#include "Schedule.h"
#include "EditEx.h"
#include "common/HoverButton.h"
#include "SubContentsDialog.h"

// CPPTContentsDlg dialog

class CPPTContentsDlg : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CPPTContentsDlg)
public:
private:
	bool LoadContents(LPCTSTR lpszFullPath);
public:
private:
	BOOL  m_bPermanent;
	CEdit m_editContentsName;
	CEditEx m_editContentsFileName;
	CEditEx m_editContentsPlayMinute;
	CEditEx m_editContentsPlaySecond;
	CStatic m_staticContentsFileSize;
	CStatic m_groupPreview;
	CStatic m_staticContents;
	CString m_strLocation;
	CPowerPointSchedule m_wndContents;
	CHoverButton m_btnBrowseContentsFile;
	CHoverButton m_btnPreviewPlay;
	bool	m_bPreviewMode;

public:
	CPPTContentsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPPTContentsDlg();

	virtual CONTENTS_TYPE GetContentsType();
	virtual void Stop();
	virtual bool GetContentsInfo(CONTENTS_INFO& info);
	virtual bool SetContentsInfo(CONTENTS_INFO& info);

	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
	void	EnableAllControls(BOOL bEnable);			///<컨트롤의 사용가능여부 설정


// Dialog Data
	enum { IDD = IDD_POWERPOINT_CONTENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedPermanentCheck();
	afx_msg void OnBnClickedButtonPreviewPlay();
	afx_msg void OnBnClickedButtonBrowserContentsFile();
};
