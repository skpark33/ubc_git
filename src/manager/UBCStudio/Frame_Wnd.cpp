// Frame_Wnd.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "Frame_Wnd.h"
#include "UBCStudioDoc.h"
#include "MemDC.h"
#include "ColorPalatte.h"
#include "Enviroment.h"

#define		BOX_THICKNESS			9

#define		FRAME_OUT_THICKNESS		3
#define		FRAME_IN_THICKNESS		10

#define		FRAME_SETRECT(X, Y, Z)			{ X.SetRect(Y[0], Z[0], Y[1], Z[1]); X.NormalizeRect(); }

#define		INFOMATION_WIDTH		100


#define IsSHIFTPressed() ( 0x8000 ==(GetKeyState(VK_SHIFT) & 0x8000   ))
#define IsCTRLPressed()  ( 0x8000 ==(GetKeyState(VK_CONTROL) & 0x8000 ))

// CFrame_Wnd

IMPLEMENT_DYNAMIC(CFrame_Wnd, CWnd)

CFrame_Wnd::CFrame_Wnd(int nIndex, WND_TYPE eWndType, bool bEditable)
:	m_MouseType (MOUSE_IDLE)
,	m_strSelectFrameId ("")
,	m_strDropFrameId ("")
,	m_bEditable (bEditable)
,	m_pFocusWindow (NULL)
,	m_nIndex (nIndex)
,   m_nMagneticFrame(0)
,	m_bPasteReady(false)
{
	FRAME_SELECT_COLOR			= RGB(0,0,255);
	FRAME_SELECT_BG_COLOR		= ::GetSysColor(COLOR_HIGHLIGHT);
	BOX_COLOR					= RGB(0,0,255);
	BOX_BG_COLOR				= RGB(128,128,255);
	m_wndType					= eWndType;
	FRAME_SELECT_DROP_COLOR = RGB(255,0,0);

//	m_font.CreatePointFont(9*10, "Tahoma");
	m_font.CreateFont(
		14,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_BOLD,                   // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		"Tahoma"                   // lpszFacename
	);
}

CFrame_Wnd::~CFrame_Wnd()
{
}

BEGIN_MESSAGE_MAP(CFrame_Wnd, CWnd)
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_MESSAGE(WM_TEMPLATE_FRAME_SELECT_CHANGED, OnTemplateFrameSelectChanged)
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()

// CFrame_Wnd 메시지 처리기입니다.

void CFrame_Wnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.
	CMemDC memDC(&dc);
	DrawBackground(&memDC);

	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();
	if(template_link != NULL)
	{
		DrawTemplate(&memDC, template_link);
		DrawFrame(&memDC, template_link);

		if(WND_PACKAGE == m_wndType){
			DrawInformation(&memDC);
		}
	}

	DrawDebugInfo(&memDC);
}

BOOL CFrame_Wnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(	SetCursor(m_MouseType) )
		return TRUE;

	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

void CFrame_Wnd::OnLButtonDown(UINT nFlags, CPoint point)
{
	TRACE("CFrame_Wnd::OnLButtonDown()\n");
//	if(m_pFocusWindow)
//		m_pFocusWindow->SetFocus();
	SetFocus();
	CWnd::OnLButtonDown(nFlags, point);
	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();

	// 크기가 작은 Frame 을 선택하기 위한 변수
	CString szSmallFrameID = "";
	UINT nFrameArea = (UINT)-1;

	if(template_link != NULL)
	{
		if(m_MouseType == MOUSE_IDLE)
		{
			int count = template_link->arFrameLinkList.GetCount();

			for(int i=0; i<count; i++)
			{
				FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);

				if(frame_link.rcViewRect[m_nIndex].PtInRect(point))
				{
					//if(m_strSelectFrameId != frame_link.pFrameInfo->strId){
					//	GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)m_strSelectTemplateId, (LPARAM)(LPCTSTR)frame_link.pFrameInfo->strId);
					//}
					//break;

					if(nFrameArea > (frame_link.rcViewRect[m_nIndex].Height()*frame_link.rcViewRect[m_nIndex].Width()))
					{
						nFrameArea = frame_link.rcViewRect[m_nIndex].Height()*frame_link.rcViewRect[m_nIndex].Width();
						szSmallFrameID = frame_link.pFrameInfo->strId;
					}
				}
			}

			if(m_strSelectFrameId != szSmallFrameID)
			{
				GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)m_strSelectTemplateId, (LPARAM)(LPCTSTR)szSmallFrameID);
			}
		}
#ifdef _OLD_SRC_
		// Modified by 정운형 2009-01-12 오전 11:31
		// 변경내역 :  마우스 커서 조정
		/*
		else
		{	
			m_MouseType = (MOUSE_TYPE)((int)m_MouseType + (int)MOUSE_SIZING_LEFT_TOP);
			
			m_pointStart = point;

			int count = template_link->arFrameLinkList.GetCount();
			for(int i=0; i<count; i++)
			{
				FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
				if(frame_link.pFrameInfo->strId == m_strSelectFrameId)
					m_rectStart = frame_link.rcViewRect[m_nIndex];
			}

			SetCapture();
		}
		*/
#endif _OLD_SRC_
		else if(m_MouseType >= MOUSE_OVER_LEFT_TOP &&  m_MouseType <= MOUSE_OVER_RIGHT_BOTTOM)
		{
			m_MouseType = (MOUSE_TYPE)((int)m_MouseType + (int)MOUSE_OVER_RIGHT_BOTTOM);
			m_pointStart = point;

			int count = template_link->arFrameLinkList.GetCount();

			for(int i=0; i<count; i++)
			{
				FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
				if(frame_link.pFrameInfo->strId == m_strSelectFrameId)
				{
					//m_rectStart = frame_link.rcViewRect[m_nIndex];
					m_rectStart = frame_link.pFrameInfo->rcRect;

					switch(m_MouseType)
					{
					case MOUSE_SIZING_LEFT_TOP:
						m_pointStart = m_rectStart.TopLeft();
						break;
					case MOUSE_SIZING_TOP:
						m_pointStart.x = m_rectStart.CenterPoint().x;	// middle
						m_pointStart.y = m_rectStart.top ;	// top
						break;
					case MOUSE_SIZING_RIGHT_TOP:
						m_pointStart.x = m_rectStart.right;	// right
						m_pointStart.y = m_rectStart.top ;	// top
						break;
					case MOUSE_SIZING_LEFT:
						m_pointStart.x = m_rectStart.left;	// left
						m_pointStart.y = m_rectStart.CenterPoint().y;	// middle
						break;
					case MOUSE_SIZING_CENTER:
						m_pointStart = m_rectStart.CenterPoint();	// center
						break;
					case MOUSE_SIZING_RIGHT:
						m_pointStart.x = m_rectStart.right;	// right
						m_pointStart.y = m_rectStart.CenterPoint().y;	// middle
						break;
					case MOUSE_SIZING_LEFT_BOTTOM:
						m_pointStart.x = m_rectStart.left;		// left
						m_pointStart.y = m_rectStart.bottom;	// bottom
						break;
					case MOUSE_SIZING_BOTTOM:
						m_pointStart.x = m_rectStart.CenterPoint().x;	// middle
						m_pointStart.y = m_rectStart.bottom;	// bottom
						break;
					case MOUSE_SIZING_RIGHT_BOTTOM:
						m_pointStart = m_rectStart.BottomRight();
						break;
					}

					break;
				}
			}

			SetCapture();
		}
		// Modified by 정운형 2009-01-12 오전 11:31
		// 변경내역 :  마우스 커서 조정
	}
}

void CFrame_Wnd::OnMouseMove(UINT nFlags, CPoint point)
{
	//TRACE("CFrame_Wnd::OnMouseMove()\n");
	CWnd::OnMouseMove(nFlags, point);

	HitTest(point);
}

void CFrame_Wnd::OnLButtonUp(UINT nFlags, CPoint point)
{
	TRACE("CFrame_Wnd::OnLButtonUp()\n");

	CWnd::OnLButtonUp(nFlags, point);
	// Modified by 정운형 2009-01-12 오전 11:31
	// 변경내역 :  마우스 커서 조정
	/*
	if(m_MouseType != MOUSE_IDLE)
	{
		
		m_MouseType = (MOUSE_TYPE)((int)m_MouseType - (int)MOUSE_SIZING_LEFT_TOP);
		
		ReleaseCapture();
	}
	else
		HitTest(point);
	*/
	if(m_MouseType == MOUSE_IDLE)
	{
		HitTest(point);
	}
	else if(m_MouseType >= MOUSE_SIZING_LEFT_TOP && m_MouseType <= MOUSE_SIZING_RIGHT_BOTTOM)
	{
		//m_MouseType = (MOUSE_TYPE)((int)m_MouseType - (int)MOUSE_SIZING_LEFT_TOP);
//		m_MouseType = (MOUSE_TYPE)((int)m_MouseType - (int)MOUSE_OVER_RIGHT_BOTTOM);
	
		::ReleaseCapture();
	}
	m_MouseType = MOUSE_IDLE;
	// Modified by 정운형 2009-01-12 오전 11:31
	// 변경내역 :  마우스 커서 조정
	GetParent()->SendMessage(WM_FRAME_INFO_CHANGED, 0, m_MouseType);
}

void CFrame_Wnd::ReleaseCapturedFrame()
{
	if(MOUSE_SIZING_LEFT_TOP <= m_MouseType && m_MouseType <= MOUSE_SIZING_RIGHT_BOTTOM){
		m_MouseType = (MOUSE_TYPE)((int)m_MouseType - (int)MOUSE_OVER_RIGHT_BOTTOM);
	}
	::ReleaseCapture();
}

void CFrame_Wnd::HitTest(CPoint& point)
{
	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();
	if(template_link == NULL) return;

	int count = template_link->arFrameLinkList.GetCount();

	// 프레임 선택
	if(m_MouseType < MOUSE_SIZING_LEFT_TOP || m_MouseType > MOUSE_SIZING_RIGHT_BOTTOM ) // idle or over
	{
		for(int i=0; i<count; i++)
		{
			FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
			if(m_strSelectFrameId != frame_link.pFrameInfo->strId)
				continue;

			for(int j=0; j<9; j++)
			{
				if(frame_link.rcViewRectSizing[m_nIndex][j].PtInRect(point) && m_bEditable)
				{
					m_MouseType = (MOUSE_TYPE)(j+1);
					SetCursor(m_MouseType);
					return;
				}
			}
		}

		m_MouseType = MOUSE_IDLE;
		SetCursor(m_MouseType);

		return;
	}

	// 프레임 이동 및 크기조절
	for(int i=0; i<count; i++)
	{
		FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
		if(frame_link.pFrameInfo->strId != m_strSelectFrameId) continue;

		point = GetPointCorrection(m_MouseType, GetClientToTemplate(point), m_rectStart, template_link->pTemplateInfo->rcRect);

		CRect rcNewFrameInfoRect(0,0,0,0);
		bool bShiftKey = (GetKeyState(VK_SHIFT) & 0x8000) == 0x8000;

		rcNewFrameInfoRect = GetNewFrameInfoRect(bShiftKey, m_MouseType, point, m_pointStart, m_rectStart, template_link->pTemplateInfo->rcRect);
		rcNewFrameInfoRect = GetMagneticRect(m_MouseType, m_strSelectFrameId, rcNewFrameInfoRect, m_rectStart, (m_MouseType == MOUSE_SIZING_CENTER), bShiftKey);

		frame_link.pFrameInfo->rcRect = rcNewFrameInfoRect;
		frame_link.rcViewRect[m_nIndex] = GetInfoToRect(rcNewFrameInfoRect);

		// 큰 프레임 안에 완전히 들어간 프레임은 강제로 PIP 로 설정
		template_link->ReCalcPIP();

		break;
	}

	static int nTick = GetTickCount();
	if(GetTickCount() - nTick > 50)
	{
		nTick = GetTickCount();
		GetParent()->SendMessage(WM_FRAME_INFO_CHANGED, 0, m_MouseType);
	}

	RedrawWindow();
}

BOOL CFrame_Wnd::SetCursor(MOUSE_TYPE mouseType)
{
	switch(mouseType)
	{
	case MOUSE_SIZING_LEFT_TOP:
	case MOUSE_OVER_LEFT_TOP:
	case MOUSE_SIZING_RIGHT_BOTTOM:
	case MOUSE_OVER_RIGHT_BOTTOM:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENWSE));
		return TRUE;
		break;
	case MOUSE_SIZING_BOTTOM:
	case MOUSE_OVER_BOTTOM:
	case MOUSE_SIZING_TOP:
	case MOUSE_OVER_TOP:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENS));
		return TRUE;
		break;
	case MOUSE_SIZING_LEFT_BOTTOM:
	case MOUSE_OVER_LEFT_BOTTOM:
	case MOUSE_SIZING_RIGHT_TOP:
	case MOUSE_OVER_RIGHT_TOP:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENESW));
		return TRUE;
		break;
	case MOUSE_SIZING_LEFT:
	case MOUSE_OVER_LEFT:
	case MOUSE_SIZING_RIGHT:
	case MOUSE_OVER_RIGHT:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		return TRUE;
		break;
	case MOUSE_SIZING_CENTER:
	case MOUSE_OVER_CENTER:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
		return TRUE;
		break;
	default:
		/*
		// Modified by 정운형 2009-01-09 오후 5:33
		// 변경내역 :  마우스 커서 조정
		{
			::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		}
		// Modified by 정운형 2009-01-09 오후 5:33
		// 변경내역 :  마우스 커서 조정
		*/
		break;
	}

	return FALSE;
}

void CFrame_Wnd::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	RecalcLayout();
}

void CFrame_Wnd::RecalcLayout()
{
	CRect client_rect;
	GetClientRect(client_rect);
	if(WND_PACKAGE == m_wndType)
	{
		if(client_rect.Width() > INFOMATION_WIDTH)
		{
			client_rect.right -= INFOMATION_WIDTH;
		}
	}

	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();

	if(template_link!= NULL)
	{
		int CLIENT_WIDTH = client_rect.Width() - ((FRAME_OUT_THICKNESS + FRAME_IN_THICKNESS) * 2);
		int CLIENT_HEIGHT = client_rect.Height() - ((FRAME_OUT_THICKNESS + FRAME_IN_THICKNESS) * 2);

		{
			int width, height;
			width = CLIENT_WIDTH;
			height = template_link->pTemplateInfo->rcRect.Width() * CLIENT_HEIGHT / CLIENT_WIDTH;
			if(height < template_link->pTemplateInfo->rcRect.Height())
			{
				height = CLIENT_HEIGHT;
				width = template_link->pTemplateInfo->rcRect.Height() * CLIENT_WIDTH / CLIENT_HEIGHT;

				m_fScale = ((double)CLIENT_HEIGHT) / ((double)template_link->pTemplateInfo->rcRect.Height());
			}
			else
			{
				m_fScale = ((double)CLIENT_WIDTH) / ((double)template_link->pTemplateInfo->rcRect.Width());
			}
		}

		template_link->rcViewRect[m_nIndex] = GetInfoToRect(template_link->pTemplateInfo->rcRect);

		int count = template_link->arFrameLinkList.GetCount();
		for(int i=0; i<count;i++)
		{
			FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
			frame_link.rcViewRect[m_nIndex] = GetInfoToRect(frame_link.pFrameInfo->rcRect);
		}
	}
}

void CFrame_Wnd::DrawBackground(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);

	pDC->FillSolidRect(rect, FRAME_OUT_BACKGROUND_COLOR);

	rect.DeflateRect (FRAME_OUT_THICKNESS, FRAME_OUT_THICKNESS);
	rect.DeflateRect (FRAME_IN_THICKNESS, FRAME_IN_THICKNESS);

	pDC->FillSolidRect(rect, FRAME_OUT_BACKGROUND_COLOR);
	//pDC->FillSolidRect(rect, FRAME_IN_BACKGROUND_COLOR);

	rect.InflateRect (FRAME_IN_THICKNESS, FRAME_IN_THICKNESS);

//#define RENDER_FRAME

#ifdef RENDER_FRAME

	int left[2], h_center[2], right[2], top[2], v_center[2], bottom[2];

	left[0] = rect.left;
	left[1] = rect.left + FRAME_IN_THICKNESS;

	h_center[0] = rect.left + FRAME_IN_THICKNESS;
	h_center[1] = rect.right - FRAME_IN_THICKNESS;

	right[0] = rect.right - FRAME_IN_THICKNESS;
	right[1] = rect.right;

	top[0] = rect.top;
	top[1] = rect.top + FRAME_IN_THICKNESS;

	v_center[0] = rect.top + FRAME_IN_THICKNESS;
	v_center[1] = rect.bottom - FRAME_IN_THICKNESS;

	bottom[0] = rect.bottom - FRAME_IN_THICKNESS;
	bottom[1] = rect.bottom;

	int pos_x[8][4] = {
		{left[0],     left[1],     left[1],     left[0]},
		{h_center[0], h_center[1], h_center[1], h_center[0]},
		{right[0],    right[1],    right[1],    right[0]},
		{left[0],     left[1],     left[1],     left[0]},
		{right[0],    right[1],    right[1],    right[0]},
		{left[0],     left[1],     left[1],     left[0]},
		{h_center[0], h_center[1], h_center[1], h_center[0]},
		{right[0],    right[1],    right[1],    right[0]}
	};
	int pos_y[8][4] = {
		{top[0], top[0], top[1], top[1]},
		{top[0], top[0], top[1], top[1]},
		{top[0], top[0], top[1], top[1]},
		{v_center[0], v_center[0], v_center[1], v_center[1]},
		{v_center[0], v_center[0], v_center[1], v_center[1]},
		{bottom[0], bottom[0], bottom[1], bottom[1]},
		{bottom[0], bottom[0], bottom[1], bottom[1]},
		{bottom[0], bottom[0], bottom[1], bottom[1]}
	};

	int rgbBright = ((int)RGB_BRIGHT) << 8;
	int rgbDark = ((int)RGB_DARK) << 8 ;

	int rgb_color[8][4] = {
		{rgbBright, rgbBright, rgbDark,   rgbBright},
		{rgbBright, rgbBright, rgbDark,   rgbDark},
		{rgbBright, rgbBright, rgbBright, rgbDark},
		{rgbBright, rgbDark,   rgbDark,   rgbBright},
		{rgbDark,   rgbBright, rgbBright, rgbDark},
		{rgbBright, rgbDark,   rgbBright, rgbBright},
		{rgbDark,   rgbDark,   rgbBright, rgbBright},
		{rgbDark,   rgbBright, rgbBright, rgbBright}
	};

	int vertex[8][2][3] = {
		{ {0,1,2}, {0,2,3} },
		{ {0,1,2}, {0,2,3} },
		{ {1,2,3}, {1,0,3} },
		{ {0,1,2}, {0,2,3} },
		{ {0,1,2}, {0,2,3} },
		{ {1,2,3}, {0,1,3} },
		{ {0,1,2}, {0,2,3} },
		{ {0,1,2}, {0,2,3} }
	};

	TRIVERTEX        vert[4];
	GRADIENT_TRIANGLE    gTri[2];
	for(int i=0; i<8; i++)
	{
		for(int j=0; j<4; j++)
		{
			vert [j].x       =  pos_x[i][j];
			vert [j].y       =  pos_y[i][j];
			vert [j].Red     =  vert [j].Green   =  vert [j].Blue    =  rgb_color[i][j];
			vert [j].Alpha   =  0x0000;
		}

		for(int j=0; j<2; j++)
		{
			gTri[j].Vertex1   = vertex[i][j][0];
			gTri[j].Vertex2   = vertex[i][j][1];
			gTri[j].Vertex3   = vertex[i][j][2];
		}

		GradientFill(pDC->m_hDC,vert,4,&gTri,2,GRADIENT_FILL_TRIANGLE);
	}

#endif
}

void CFrame_Wnd::DrawTemplate(CDC* pDC, TEMPLATE_LINK* pTemplateInfo)
{
	pDC->FillSolidRect(pTemplateInfo->rcViewRect[m_nIndex], pTemplateInfo->pTemplateInfo->crBgColor);
}

void CFrame_Wnd::DrawFrame(CDC* pDC, TEMPLATE_LINK* pTemplateInfo)
{
	int count = pTemplateInfo->arFrameLinkList.GetCount();

	int clr;
	// draw frame w/o selected frame
	for(int i=0; i<count;i++)
	{
		FRAME_LINK& frame_link = pTemplateInfo->arFrameLinkList.GetAt(i);

		if(frame_link.pFrameInfo->strId != m_strSelectFrameId && frame_link.pFrameInfo->strId != m_strDropFrameId)
		{
			// Modified by 정운형 2009-03-04 오후 8:20
			// 변경내역 :  Primary Frame color 조정
			if(frame_link.pFrameInfo->nGrade == 1)
			{
				pDC->FillSolidRect(frame_link.rcViewRect[m_nIndex], FRAME_PRIMARY_COLOR);
			}
			else
			{
				int nFrameIndex = _ttoi(frame_link.pFrameInfo->strId);
				COLORREF color = CColorPalatte::getInstance()->GetFrameColor(nFrameIndex);

				//clr = 250 - (i+1) * FRAME_LEVEL_STEP;

				pDC->FillSolidRect(frame_link.rcViewRect[m_nIndex], color);
			}//if
			// Modified by 정운형 2009-03-04 오후 8:20
			// 변경내역 :  Primary Frame color 조정

			CRect border_rect = frame_link.rcViewRect[m_nIndex];
			int thickness = (int)(frame_link.pFrameInfo->nBorderThickness * m_fScale);
			for(int j=0; j<thickness; j++)
			{
				pDC->Draw3dRect(border_rect, frame_link.pFrameInfo->crBorderColor, frame_link.pFrameInfo->crBorderColor);	// 선택 프레임 외곽선
				border_rect.DeflateRect(1,1);
			}
			
			// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
			if(frame_link.pFrameInfo->strComment[0] == _T("9"))
			{
				pDC->DrawText(_T("HD-Video"), frame_link.rcViewRect[m_nIndex], DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
			}
		}
	}

	// draw selected frame
	if(m_strSelectFrameId != "")
	{
		for(int i=0; i<pTemplateInfo->arFrameLinkList.GetCount(); i++)
		{
			FRAME_LINK& frame_link = pTemplateInfo->arFrameLinkList.GetAt(i);
			if(frame_link.pFrameInfo->strId == m_strSelectFrameId)
			{
				// Modified by 정운형 2009-03-04 오후 8:20
				// 변경내역 :  Primary Frame color 조정
				if(frame_link.pFrameInfo->nGrade == 1)
				{
					pDC->FillSolidRect(frame_link.rcViewRect[m_nIndex], FRAME_PRIMARY_SELECT_COLOR);
				}
				else
				{
					pDC->FillSolidRect(frame_link.rcViewRect[m_nIndex], FRAME_SELECT_BG_COLOR);
				}//if
				// Modified by 정운형 2009-03-04 오후 8:20
				// 변경내역 :  Primary Frame color 조정

				CRect border_rect = frame_link.rcViewRect[m_nIndex];
				int thickness = (int)(frame_link.pFrameInfo->nBorderThickness * m_fScale);
				for(int j=0; j<thickness; j++)
				{
					pDC->Draw3dRect(border_rect, frame_link.pFrameInfo->crBorderColor, frame_link.pFrameInfo->crBorderColor);	// 선택 프레임 외곽선
					border_rect.DeflateRect(1,1);
				}

				// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
				if(frame_link.pFrameInfo->strComment[0] == _T("9"))
				{
					pDC->DrawText(_T("HD-Video"), frame_link.rcViewRect[m_nIndex], DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
				}
				break;
			}
		}
	}

	// draw droped frame
	if(m_strDropFrameId != "")
	{
		for(int i=0; i<pTemplateInfo->arFrameLinkList.GetCount(); i++)
		{
			FRAME_LINK& frame_link = pTemplateInfo->arFrameLinkList.GetAt(i);
			if(frame_link.pFrameInfo->strId == m_strDropFrameId)
			{
				// Modified by 정운형 2009-03-04 오후 8:20
				// 변경내역 :  Primary Frame color 조정
				if(frame_link.pFrameInfo->nGrade == 1)
				{
					pDC->FillSolidRect(frame_link.rcViewRect[m_nIndex], FRAME_PRIMARY_DROP_COLOR);
				}
				else
				{
					int r,g,b;
					::GetColorRGB(::GetSysColor(COLOR_HIGHLIGHT), r,g,b);
					r += (255-r)/2; if(r>255) r=255;
					g += (255-g)/2; if(g>255) g=255;
					b += (255-b)/2; if(b>255) b=255;
					pDC->FillSolidRect(frame_link.rcViewRect[m_nIndex], RGB(r, g, b));
				}//if
				// Modified by 정운형 2009-03-04 오후 8:20
				// 변경내역 :  Primary Frame color 조정

				CRect border_rect = frame_link.rcViewRect[m_nIndex];
				int thickness = (int)(frame_link.pFrameInfo->nBorderThickness * m_fScale);
				for(int j=0; j<thickness; j++)
				{
					pDC->Draw3dRect(border_rect, frame_link.pFrameInfo->crBorderColor, frame_link.pFrameInfo->crBorderColor);	// 선택 프레임 외곽선
					border_rect.DeflateRect(1,1);
				}

				// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
				if(frame_link.pFrameInfo->strComment[0] == _T("9"))
				{
					pDC->DrawText(_T("HD-Video"), frame_link.rcViewRect[m_nIndex], DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
				}
				break;
			}
		}
	}
	// draw sizing box
	for(int i=0; i<count;i++)
	{
		FRAME_LINK& frame_link = pTemplateInfo->arFrameLinkList.GetAt(i);

		if(frame_link.pFrameInfo->strId == m_strSelectFrameId)
		{
			//pDC->Draw3dRect(frame->viewRect[m_nIndex], FRAME_SELECT_COLOR, FRAME_SELECT_COLOR);	// 선택 프레임 외곽선

			int width_half = frame_link.rcViewRect[m_nIndex].left + frame_link.rcViewRect[m_nIndex].Width() / 2;
			int height_half = frame_link.rcViewRect[m_nIndex].top + frame_link.rcViewRect[m_nIndex].Height() / 2;

			int left[2], h_center[2], right[2], top[2], v_center[2], bottom[2];

			left[0] = frame_link.rcViewRect[m_nIndex].left - BOX_THICKNESS;// + 1;
			left[1] = frame_link.rcViewRect[m_nIndex].left;// + 1;

			h_center[0] = width_half - BOX_THICKNESS/2;
			h_center[1] = width_half - BOX_THICKNESS/2 + BOX_THICKNESS;

			right[0] = frame_link.rcViewRect[m_nIndex].right;// - 1;
			right[1] = frame_link.rcViewRect[m_nIndex].right + BOX_THICKNESS;// - 1;

			top[0] = frame_link.rcViewRect[m_nIndex].top - BOX_THICKNESS;// + 1;
			top[1] = frame_link.rcViewRect[m_nIndex].top;// + 1;

			v_center[0] = height_half - BOX_THICKNESS/2;
			v_center[1] = height_half - BOX_THICKNESS/2 + BOX_THICKNESS;

			bottom[0] = frame_link.rcViewRect[m_nIndex].bottom;// - 1;
			bottom[1] = frame_link.rcViewRect[m_nIndex].bottom + BOX_THICKNESS;// - 1;

			FRAME_SETRECT(frame_link.rcViewRectSizing[m_nIndex][0], left,     top);
			FRAME_SETRECT(frame_link.rcViewRectSizing[m_nIndex][1], h_center, top);
			FRAME_SETRECT(frame_link.rcViewRectSizing[m_nIndex][2], right,    top);
			FRAME_SETRECT(frame_link.rcViewRectSizing[m_nIndex][3], left,     v_center);
			FRAME_SETRECT(frame_link.rcViewRectSizing[m_nIndex][4], h_center, v_center);
			FRAME_SETRECT(frame_link.rcViewRectSizing[m_nIndex][5], right,    v_center);
			FRAME_SETRECT(frame_link.rcViewRectSizing[m_nIndex][6], left,     bottom);
			FRAME_SETRECT(frame_link.rcViewRectSizing[m_nIndex][7], h_center, bottom);
			FRAME_SETRECT(frame_link.rcViewRectSizing[m_nIndex][8], right,    bottom);

#ifdef _DEBUG
			for(int t1=0; t1<5; t1++)
			{
				for(int t2=0; t2<9; t2++)
				{
					if( frame_link.rcViewRectSizing[t1][t2].Width()  == 0 && 
						frame_link.rcViewRectSizing[t1][t2].Height() == 0 )
					{
						CString str;
						str.Format("Index[%d][%d], width=%d, height=%d\r\n"
								 , t1
								 , t2
								 , frame_link.rcViewRectSizing[t1][t2].Width()
								 , frame_link.rcViewRectSizing[t1][t2].Height()
								 );
						TRACE(str);
					}//if
				}//for
			}
#endif

			if(m_bEditable)
			{
				for(int i=0; i<9; i++)
				{
					pDC->FillSolidRect(frame_link.rcViewRectSizing[m_nIndex][i], BOX_BG_COLOR );
					pDC->Draw3dRect(frame_link.rcViewRectSizing[m_nIndex][i], BOX_COLOR, BOX_COLOR);
				}
			}
		}
		//else
		//{
		//	for(int i=0; i<9; i++)
		//	{
		//		frame_link.rcViewRectSizing[i].SetRect(-1,-1,-1,-1);
		//	}
		//}
	}
}

void CFrame_Wnd::DrawInformation(CDC* pDC)
{
	TraceLog(("DrawInformation()"));
	
	DWORD TotalRunningTime = 0;
	DWORD TemplRunningTime = 0;
	static int prev_count = 0;
	int curr_count = 0;
	bool counted = false;
	int TemplCount=0;

	TEMPLATE_LINK_LIST* pTempPlayList = m_pDocument->GetPlayTemplateList();

	if(pTempPlayList){
		for(int i = 0; i < pTempPlayList->GetCount(); i++){
			TEMPLATE_LINK* pTempLink = &(pTempPlayList->GetAt(i));
			if(!pTempLink)	continue;

//			TEMPLATE_INFO* pTempInfo = pTempLink->pTemplateInfo;
//			if(!pTempInfo)	continue;

			for(int j = 0; j < pTempLink->arFrameLinkList.GetCount(); j++){
				FRAME_INFO*	pFrmInfo = pTempLink->arFrameLinkList.GetAt(j).pFrameInfo;
				if(!pFrmInfo)	continue;

				if(pFrmInfo->nGrade != 1)
					continue;
				counted = true;
				curr_count += (pFrmInfo->arCyclePlayContentsList.GetSize() + pFrmInfo->arTimePlayContentsList.GetSize());
				TotalRunningTime += (GetFrmRunningTime(pFrmInfo)*pTempLink->nPlayTimes);
				TraceLog(("Counted=%d", curr_count));
				break;
			}
		}
	}

	TEMPLATE_LINK* pTempSelLink = m_pDocument->GetSelectTemplateLink();
	if(pTempSelLink){
//		TEMPLATE_INFO* pTempInfo = pTempSelLink->pTemplateInfo;

		int PlayTimes = pTempSelLink->nPlayTimes;

		for(int j = 0; j < pTempSelLink->arFrameLinkList.GetCount(); j++){
			FRAME_INFO*	pFrmInfo = pTempSelLink->arFrameLinkList.GetAt(j).pFrameInfo;
			if(!pFrmInfo)	continue;

			if(pFrmInfo->nGrade != 1)
				continue;

			TemplCount = (pFrmInfo->arCyclePlayContentsList.GetSize() + pFrmInfo->arTimePlayContentsList.GetSize());
			TemplRunningTime = (GetFrmRunningTime(pFrmInfo)*pTempSelLink->nPlayTimes);
			break;
		}
	}

	CRect rc;
	GetClientRect(&rc);
	CString szTemp;

	int x = rc.Width()-INFOMATION_WIDTH;
	int y = 10;

	CFont* ftOld = (CFont*)pDC->SelectObject(&m_font);
	COLORREF clOldText = pDC->SetTextColor(RGB(255,255,255)); 
	COLORREF clOldBk = pDC->SetBkColor(FRAME_OUT_BACKGROUND_COLOR);

	pDC->TextOut(x, y, "Total time");
	pDC->TextOut(x, (y+=15), GetTimeStr(TotalRunningTime));
	pDC->TextOut(x, (y+=25), "Frame time");
	pDC->TextOut(x, (y+=15), GetTimeStr(TemplRunningTime));
	pDC->TextOut(x, y+=25, "Total count");
	pDC->TextOut(x, (y+=15), ::ToString(curr_count));
	pDC->TextOut(x, (y+=25), "Frame count");
	pDC->TextOut(x, (y+=15), ::ToString(TemplCount));

	pDC->SelectObject(&ftOld);
	pDC->SetTextColor(clOldText);
	pDC->SetBkColor(clOldBk);

	if(counted && prev_count != curr_count){
		CUBCStudioDoc::s_schedule_count = curr_count;
		prev_count = curr_count;
		if(curr_count > MAX_SCHEDULE_COUNT){
			CString strMsg;
			strMsg.Format(LoadStringById(IDS_UBCSTUDIODOC_MSG010), MAX_SCHEDULE_COUNT , curr_count);
			UbcMessageBox(strMsg);
		}
	}

}

DWORD CFrame_Wnd::GetFrmRunningTime(FRAME_INFO* pFrmInfo)
{
	if(!pFrmInfo)
		return 0;

	DWORD cycledTime = 0;
	int nCnt = pFrmInfo->arCyclePlayContentsList.GetCount();
	for(int i = 0; i < nCnt; i++){
		PLAYCONTENTS_INFO* pSchInfo = pFrmInfo->arCyclePlayContentsList.GetAt(i);
		if(!pSchInfo)	continue;

		CONTENTS_INFO* pConInfo = m_pDocument->GetContents(pSchInfo->strContentsId);
		if(!pConInfo)	continue;

		cycledTime += pConInfo->nRunningTime;
	}

	DWORD timedTime = 0;
	//nCnt = pFrmInfo->arTimePlayContentsList.GetCount();
	//for(int i = 0; i < nCnt; i++){
	//	PLAYCONTENTS_INFO* pSchInfo = pFrmInfo->arTimePlayContentsList.GetAt(i);
	//	if(!pSchInfo)	continue;

	//	CONTENTS_INFO* pConInfo = m_pDocument->GetContents(pSchInfo->strContentsId);
	//	if(!pConInfo)	continue;

	//	timedTime += pConInfo->nRunningTime;
	//}

	return (cycledTime>timedTime)?cycledTime:timedTime;
}

CString CFrame_Wnd::GetTimeStr(UINT secTot)
{
	CString szTemp;
	UINT min = secTot / 60;
	UINT sec = secTot % 60;

	if(min > 0){
		szTemp.Format("%d min %d sec", min, sec);
	}else{
		szTemp.Format("%d sec", sec);
	}

	return szTemp;
}

FRAME_INFO* CFrame_Wnd::CreateNewFrame()
{
	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();

	if(template_link != NULL)
	{
		FRAME_INFO* pFrameInfo = m_pDocument->CreateNewFrame(template_link->pTemplateInfo->strId);
		if(pFrameInfo != NULL)
		{
			RecalcLayout();
			GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)template_link->pTemplateInfo->strId, (WPARAM)(LPCTSTR)pFrameInfo->strId );
		}
		return pFrameInfo;
	}
	return NULL;
}

bool CFrame_Wnd::DeleteFrame()
{
	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();

	if(template_link != NULL)
	{
		if(m_pDocument->DeleteFrame(template_link->pTemplateInfo->strId, m_strSelectFrameId))
		{
			GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)template_link->pTemplateInfo->strId, (WPARAM)(LPCTSTR)"" );
			return true;
		}
	}
	return false;
}

bool CFrame_Wnd::MovePrevFrame()
{
	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();

	if(template_link != NULL)
	{
		CStringArray id_list;
		for(int i=0; i<template_link->arFrameLinkList.GetCount(); i++)
		{
			FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
			id_list.Add(frame_link.pFrameInfo->strId);
		}

		if(id_list.GetCount() > 0)
		{
			CString strFrameId;

			bool find = false;
			for(int i=0; i<id_list.GetCount(); i++)
			{
				if(id_list.GetAt(i) == m_strSelectFrameId)
				{
					if(i == 0)
					{
						find = true;
						strFrameId = id_list.GetAt(id_list.GetCount()-1);
						break;
					}
					else
					{
						find = true;
						strFrameId = id_list.GetAt(i-1);
						break;
					}
				}
			}

			if(find == false)
			{
				strFrameId = id_list.GetAt(0);
			}

			GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)m_strSelectTemplateId, (LPARAM)(LPCTSTR)strFrameId);
			return true;
		}
	}
	return false;
}

bool CFrame_Wnd::MoveNextFrame()
{
	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();

	if(template_link!= NULL)
	{
		CStringArray id_list;
		for(int i=0; i<template_link->arFrameLinkList.GetCount(); i++)
		{
			FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
			id_list.Add(frame_link.pFrameInfo->strId);
		}

		if(id_list.GetCount() > 0)
		{
			CString strFrameId;

			bool find = false;
			for(int i=0; i<id_list.GetCount(); i++)
			{
				if(id_list.GetAt(i) == m_strSelectFrameId)
				{
					if(i == id_list.GetCount()-1)
					{
						find = true;
						strFrameId = id_list.GetAt(0);
						break;
					}
					else
					{
						find = true;
						strFrameId = id_list.GetAt(i+1);
						break;
					}
				}
			}

			if(find == false)
			{
				strFrameId = id_list.GetAt(0);
			}

			GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)m_strSelectTemplateId, (LPARAM)(LPCTSTR)strFrameId);
			return true;
		}
	}
	return false;
}

bool CFrame_Wnd::HitTest2(CPoint& point)
{
	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();

	if(point.x < 0 && point.y < 0)
	{
		if(m_strDropFrameId.GetLength() > 0)
		{
			m_strDropFrameId = "";
			Invalidate(FALSE);
			return true;
		}
	}
	else if(template_link != NULL)
	{
		// 크기가 작은 Frame 을 선택하기 위한 변수
		CString strSmallFrameID;
		UINT nFrameArea = (UINT)-1;

		int count = template_link->arFrameLinkList.GetCount();

		for(int i=0; i<count; i++)
		{
			FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);

			if(frame_link.rcViewRect[m_nIndex].PtInRect(point))
			{
				if(nFrameArea > (frame_link.rcViewRect[m_nIndex].Height()*frame_link.rcViewRect[m_nIndex].Width()))
				{
					nFrameArea = frame_link.rcViewRect[m_nIndex].Height()*frame_link.rcViewRect[m_nIndex].Width();
					strSmallFrameID = frame_link.pFrameInfo->strId;
				}
			}
		}

		m_strDropFrameId = strSmallFrameID;

		Invalidate(FALSE);

		return !strSmallFrameID.IsEmpty();
	}

	return false;
}

LRESULT CFrame_Wnd::OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam)
{

	LPCTSTR strTemplateId = (LPCTSTR)wParam;
	LPCTSTR strFrameId = (LPCTSTR)lParam;

	TraceLog(("CFrame_Wnd::OnTemplateFrameSelectChanged (%s)", strTemplateId));

	bool frame_change = false;

	if(m_strSelectTemplateId != strTemplateId)
	{
		m_strSelectTemplateId = strTemplateId;
		m_strSelectFrameId = "";
		RecalcLayout();
		frame_change = true;
	}

	if(m_strSelectFrameId != strFrameId || frame_change)
	{
		m_strSelectFrameId = strFrameId;
		Invalidate(FALSE);
	}

	return 0;
}

// (x,y) 와 (cx,cy)가 역전되지 않도록함
CPoint CFrame_Wnd::GetPointCorrection(MOUSE_TYPE mouseType, CPoint ptCurrent, CRect rcFrame, CRect rcTemplate)
{
	CPoint ptResult = ptCurrent;

	switch(mouseType)
	{
	case MOUSE_SIZING_LEFT_TOP:
		if(ptCurrent.x > rcFrame.right) ptResult.x = rcFrame.right;		// left
		if(ptCurrent.y > rcFrame.bottom) ptResult.y = rcFrame.bottom;	// top
		break;
	case MOUSE_SIZING_TOP:
		if(ptCurrent.y > rcFrame.bottom) ptResult.y = rcFrame.bottom;	// top
		break;
	case MOUSE_SIZING_RIGHT_TOP:
		if(ptCurrent.x < rcFrame.left) ptResult.x = rcFrame.left;		// right
		if(ptCurrent.y > rcFrame.bottom) ptResult.y = rcFrame.bottom;	// top
		break;
	case MOUSE_SIZING_LEFT:
		if(ptCurrent.x > rcFrame.right) ptResult.x = rcFrame.right;		// left
		break;
	case MOUSE_SIZING_CENTER:
//		rcTemplate.left   += rcFrame.right - rcFrame.CenterPoint().x;
//		rcTemplate.right  -= rcFrame.right - rcFrame.CenterPoint().x;
//		rcTemplate.top    += rcFrame.bottom - rcFrame.CenterPoint().y;
//		rcTemplate.bottom -= rcFrame.bottom - rcFrame.CenterPoint().y;
		break;
	case MOUSE_SIZING_RIGHT:
		if(ptCurrent.x < rcFrame.left) ptResult.x = rcFrame.left;		// right
		break;
	case MOUSE_SIZING_LEFT_BOTTOM:
		if(ptCurrent.x > rcFrame.right) ptResult.x = rcFrame.right;		// left
		if(ptCurrent.y < rcFrame.top) ptResult.y = rcFrame.top;			// bottom
		break;
	case MOUSE_SIZING_BOTTOM:
		if(ptCurrent.y < rcFrame.top) ptResult.y = rcFrame.top;			// bottom
		break;
	case MOUSE_SIZING_RIGHT_BOTTOM:
		if(ptCurrent.x < rcFrame.left) ptResult.x = rcFrame.left;		// right
		if(ptCurrent.y < rcFrame.top) ptResult.y = rcFrame.top;			// bottom
		break;
	}

	// 템플릿영역을 벗어나는경우 보정
	if(mouseType == MOUSE_SIZING_CENTER)
	{
		if(ptResult.x < rcTemplate.left   + rcFrame.Width()/2 ) ptResult.x = rcTemplate.left   + rcFrame.Width()/2;
		if(ptResult.y < rcTemplate.top    + rcFrame.Height()/2) ptResult.y = rcTemplate.top    + rcFrame.Height()/2;
		if(ptResult.x > rcTemplate.right  - rcFrame.Width()/2 ) ptResult.x = rcTemplate.right  - rcFrame.Width()/2;
		if(ptResult.y > rcTemplate.bottom - rcFrame.Height()/2) ptResult.y = rcTemplate.bottom - rcFrame.Height()/2;
	}
	else
	{
		if(ptResult.x < rcTemplate.left  ) ptResult.x = rcTemplate.left  ;
		if(ptResult.y < rcTemplate.top   ) ptResult.y = rcTemplate.top   ;
		if(ptResult.x > rcTemplate.right ) ptResult.x = rcTemplate.right ;
		if(ptResult.y > rcTemplate.bottom) ptResult.y = rcTemplate.bottom;
	}

	return ptResult;
}

CRect CFrame_Wnd::GetNewFrameInfoRect(bool bShiftKey, MOUSE_TYPE mouseType, CPoint ptCurrent, CPoint ptStart, CRect rcFrame, CRect rcTemplate)
{
	CRect rcResult = rcFrame;
	CSize sizeCurrent(abs(ptStart.x - ptCurrent.x), abs(ptStart.y - ptCurrent.y));

	switch(mouseType)
	{
	case MOUSE_SIZING_LEFT_TOP:
		rcResult.left += (ptCurrent.x - ptStart.x);
		rcResult.top  += (ptCurrent.y - ptStart.y);
		break;

	case MOUSE_SIZING_TOP:
		rcResult.top += (ptCurrent.y - ptStart.y);
		break;

	case MOUSE_SIZING_RIGHT_TOP:
		rcResult.right += (ptCurrent.x - ptStart.x);
		rcResult.top   += (ptCurrent.y - ptStart.y);
		break;

	case MOUSE_SIZING_LEFT:
		rcResult.left += (ptCurrent.x - ptStart.x);
		break;

	case MOUSE_SIZING_CENTER:
		if(bShiftKey)
		{
			// 더많이 움직인 좌표를 기준으로 덜 움직인 좌표를 고정시킨다.
			if( sizeCurrent.cx > sizeCurrent.cy ) ptCurrent.y = ptStart.y;
			else                                  ptCurrent.x = ptStart.x;
		}

		rcResult.left   += (ptCurrent.x - ptStart.x);
		rcResult.top    += (ptCurrent.y - ptStart.y);
		rcResult.right  += (ptCurrent.x - ptStart.x);
		rcResult.bottom	+= (ptCurrent.y - ptStart.y);
		break;

	case MOUSE_SIZING_RIGHT:
		rcResult.right += (ptCurrent.x - ptStart.x);
		break;

	case MOUSE_SIZING_LEFT_BOTTOM:
		rcResult.left   += (ptCurrent.x - ptStart.x);
		rcResult.bottom	+= (ptCurrent.y - ptStart.y);
		break;

	case MOUSE_SIZING_BOTTOM:
		rcResult.bottom += (ptCurrent.y - ptStart.y);
		break;

	case MOUSE_SIZING_RIGHT_BOTTOM:
		rcResult.right  += (ptCurrent.x - ptStart.x);
		rcResult.bottom	+= (ptCurrent.y - ptStart.y);
		break;
	}

	if(bShiftKey)
	{
		int nPointX = 0;
		int nPointY = 0;

		switch(mouseType)
		{
		case MOUSE_SIZING_RIGHT_TOP    :
				nPointX = rcResult.left + double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
				nPointY = rcResult.bottom - double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();

				if( nPointX <= rcTemplate.right && nPointY >= rcTemplate.top)
				{
					if(rcFrame.Width() > rcFrame.Height())
					{
						rcResult.right = nPointX;
					}
					else
					{
						rcResult.top = nPointY;
					}
				}
				else
				{
					if( nPointX > rcTemplate.right )
					{
						rcResult.right  = rcTemplate.right;
						rcResult.top = rcResult.bottom - double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();
					}
					if( nPointY < rcTemplate.top )
					{
						rcResult.top  = rcTemplate.top;
						rcResult.right = rcResult.left + double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
					}
				}
				break;
		case MOUSE_SIZING_LEFT_BOTTOM  :
				nPointX = rcResult.right - double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
				nPointY = rcResult.top + double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();

				if( nPointX >= rcTemplate.left && nPointY <= rcTemplate.bottom)
				{
					if(rcFrame.Width() > rcFrame.Height())
					{
						rcResult.left = nPointX;
					}
					else
					{
						rcResult.bottom = nPointY;
					}
				}
				else
				{
					if( nPointX < rcTemplate.left )
					{
						rcResult.left  = rcTemplate.left;
						rcResult.bottom = rcResult.top + double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();
					}
					if( nPointY > rcTemplate.bottom )
					{
						rcResult.bottom  = rcTemplate.bottom;
						rcResult.left = rcResult.right - double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
					}
				}
				break;
		case MOUSE_SIZING_LEFT_TOP     :
				nPointX = rcResult.right - double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
				nPointY = rcResult.bottom - double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();

				if( nPointX >= rcTemplate.left && nPointY >= rcTemplate.top)
				{
					if(rcFrame.Width() > rcFrame.Height())
					{
						rcResult.left = nPointX;
					}
					else
					{
						rcResult.top = nPointY;
					}
				}
				else
				{
					if( nPointX < rcTemplate.left )
					{
						rcResult.left  = rcTemplate.left;
						rcResult.top = rcResult.bottom - double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();
					}
					if( nPointY < rcTemplate.top )
					{
						rcResult.top  = rcTemplate.top;
						rcResult.left = rcResult.right - double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
					}
				}
				break;
		case MOUSE_SIZING_RIGHT_BOTTOM :
				nPointX = rcResult.left + double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
				nPointY = rcResult.top + double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();

				if( nPointX <= rcTemplate.right && nPointY <= rcTemplate.bottom)
				{
					if(rcFrame.Width() > rcFrame.Height())
					{
						rcResult.right = nPointX;
					}
					else
					{
						rcResult.bottom = nPointY;
					}
				}
				else
				{
					if( nPointX > rcTemplate.right )
					{
						rcResult.right  = rcTemplate.right;
						rcResult.bottom = rcResult.top + double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();
					}
					if( nPointY > rcTemplate.bottom )
					{
						rcResult.bottom  = rcTemplate.bottom;
						rcResult.right = rcResult.left + double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
					}
				}
				break;
		}
	}

	return rcResult;
}

CRect CFrame_Wnd::GetInfoToRect(CRect rcInfo)
{
	rcInfo.left   = rcInfo.left   * m_fScale + FRAME_OUT_THICKNESS + FRAME_IN_THICKNESS;
	rcInfo.right  = rcInfo.right  * m_fScale + FRAME_OUT_THICKNESS + FRAME_IN_THICKNESS;
	rcInfo.top    = rcInfo.top    * m_fScale + FRAME_OUT_THICKNESS + FRAME_IN_THICKNESS;
	rcInfo.bottom = rcInfo.bottom * m_fScale + FRAME_OUT_THICKNESS + FRAME_IN_THICKNESS;

	return rcInfo;
}

CRect CFrame_Wnd::GetMagneticRect(MOUSE_TYPE mouseType, CString strInFrameId, CRect rcNewFrame, CRect rcFrame, bool bFixedSize, bool bShiftKey)
{
	CRect rcResult = rcNewFrame;

	if(m_nMagneticFrame <= 0) rcResult;

	if(strInFrameId.IsEmpty()) return rcResult;

	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();
	if(template_link == NULL) return rcResult;

	CRect rcCmpValue(template_link->pTemplateInfo->rcRect.left   - rcNewFrame.left  
					,template_link->pTemplateInfo->rcRect.top    - rcNewFrame.top   
					,template_link->pTemplateInfo->rcRect.right  - rcNewFrame.right 
					,template_link->pTemplateInfo->rcRect.bottom - rcNewFrame.bottom
					);

	int nGap = m_nMagneticFrame / m_fScale;

	int count = template_link->arFrameLinkList.GetCount();
	for(int i=0; i<count; i++)
	{
		FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
		if(strInFrameId == frame_link.pFrameInfo->strId) continue;

		// left
		if( mouseType == MOUSE_SIZING_LEFT_TOP    ||
			mouseType == MOUSE_SIZING_LEFT        ||
			mouseType == MOUSE_SIZING_LEFT_BOTTOM ||
			mouseType == MOUSE_SIZING_CENTER      )
		{
			if( abs(rcCmpValue.left  ) > abs(frame_link.pFrameInfo->rcRect.left   - rcNewFrame.left  )) rcCmpValue.left   = frame_link.pFrameInfo->rcRect.left   - rcNewFrame.left  ;
			if( abs(rcCmpValue.left  ) > abs(frame_link.pFrameInfo->rcRect.right  - rcNewFrame.left  )) rcCmpValue.left   = frame_link.pFrameInfo->rcRect.right  - rcNewFrame.left  ;
		}

		// right
		if( mouseType == MOUSE_SIZING_RIGHT_TOP    ||
			mouseType == MOUSE_SIZING_RIGHT        ||
			mouseType == MOUSE_SIZING_RIGHT_BOTTOM ||
			mouseType == MOUSE_SIZING_CENTER       )
		{
			if( abs(rcCmpValue.right ) > abs(frame_link.pFrameInfo->rcRect.right  - rcNewFrame.right )) rcCmpValue.right  = frame_link.pFrameInfo->rcRect.right  - rcNewFrame.right ;
			if( abs(rcCmpValue.right ) > abs(frame_link.pFrameInfo->rcRect.left   - rcNewFrame.right )) rcCmpValue.right  = frame_link.pFrameInfo->rcRect.left   - rcNewFrame.right ;
		}

		// top
		if( mouseType == MOUSE_SIZING_LEFT_TOP  ||
			mouseType == MOUSE_SIZING_TOP       ||
			mouseType == MOUSE_SIZING_RIGHT_TOP ||
			mouseType == MOUSE_SIZING_CENTER    )
		{
			if( abs(rcCmpValue.top   ) > abs(frame_link.pFrameInfo->rcRect.top    - rcNewFrame.top   )) rcCmpValue.top    = frame_link.pFrameInfo->rcRect.top    - rcNewFrame.top   ;
			if( abs(rcCmpValue.top   ) > abs(frame_link.pFrameInfo->rcRect.bottom - rcNewFrame.top   )) rcCmpValue.top    = frame_link.pFrameInfo->rcRect.bottom - rcNewFrame.top   ;
		}

		// bottom
		if( mouseType == MOUSE_SIZING_LEFT_BOTTOM  ||
			mouseType == MOUSE_SIZING_BOTTOM       ||
			mouseType == MOUSE_SIZING_RIGHT_BOTTOM ||
			mouseType == MOUSE_SIZING_CENTER       )
		{
			if( abs(rcCmpValue.bottom) > abs(frame_link.pFrameInfo->rcRect.bottom - rcNewFrame.bottom)) rcCmpValue.bottom = frame_link.pFrameInfo->rcRect.bottom - rcNewFrame.bottom;
			if( abs(rcCmpValue.bottom) > abs(frame_link.pFrameInfo->rcRect.top    - rcNewFrame.bottom)) rcCmpValue.bottom = frame_link.pFrameInfo->rcRect.top    - rcNewFrame.bottom;
		}
	}

	int nHorizon = 0;
	int nVertical = 0;

	int nLeftRight = 0;
	int nTopBottom = 0;

	if(abs(rcCmpValue.left) <= nGap || abs(rcCmpValue.right) <= nGap)
	{
		nLeftRight = (abs(rcCmpValue.left) <= abs(rcCmpValue.right) ? -1:1);
		nHorizon = (abs(rcCmpValue.left) <= abs(rcCmpValue.right) ? rcCmpValue.left : rcCmpValue.right);
	}

	if(abs(rcCmpValue.top) <= nGap || abs(rcCmpValue.bottom) <= nGap)
	{
		nTopBottom = (abs(rcCmpValue.top) <= abs(rcCmpValue.bottom) ? -1:1);
		nVertical = (abs(rcCmpValue.top) <= abs(rcCmpValue.bottom) ? rcCmpValue.top : rcCmpValue.bottom);
	}

	// 사이즈고정
	if(bFixedSize)
	{
		CPoint ptMoveTo = rcNewFrame.TopLeft();

		if     (nLeftRight < 0) ptMoveTo.x += rcCmpValue.left;
		else if(nLeftRight > 0) ptMoveTo.x += rcCmpValue.right;

		if     (nTopBottom < 0) ptMoveTo.y += rcCmpValue.top;
		else if(nTopBottom > 0) ptMoveTo.y += rcCmpValue.bottom;

		rcResult.MoveToXY(ptMoveTo);
	}
	// 비율고정
	else if(bShiftKey)
	{
		// 가로기준
		if(abs(nHorizon) > abs(nVertical))
		{
			// left
			if(nLeftRight < 0)
			{
				rcResult.left += rcCmpValue.left;
				double dNewHeight = double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();
				if(mouseType == MOUSE_SIZING_LEFT_TOP   ) rcResult.top    = rcResult.bottom - dNewHeight;
				if(mouseType == MOUSE_SIZING_LEFT_BOTTOM) rcResult.bottom = rcResult.top    + dNewHeight;
			}
			// right
			else if(nLeftRight > 0)
			{
				rcResult.right += rcCmpValue.right;
				double dNewHeight = double(rcFrame.Height() * rcResult.Width()) / rcFrame.Width();
				if(mouseType == MOUSE_SIZING_RIGHT_TOP   ) rcResult.top    = rcResult.bottom - dNewHeight;
				if(mouseType == MOUSE_SIZING_RIGHT_BOTTOM) rcResult.bottom = rcResult.top    + dNewHeight;
			}
		}
		// 세로기준
		else
		{
			// top
			if(nTopBottom < 0)
			{
				rcResult.top += rcCmpValue.top;
				double dNewWidth = double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
				if(mouseType == MOUSE_SIZING_LEFT_TOP ) rcResult.left  = rcResult.right - dNewWidth;
				if(mouseType == MOUSE_SIZING_RIGHT_TOP) rcResult.right = rcResult.left  + dNewWidth;
			}
			// bottom
			else if(nTopBottom > 0)
			{
				rcResult.bottom += rcCmpValue.bottom;
				double dNewWidth = double(rcFrame.Width() * rcResult.Height()) / rcFrame.Height();
				if(mouseType == MOUSE_SIZING_LEFT_BOTTOM ) rcResult.left  = rcResult.right - dNewWidth;
				if(mouseType == MOUSE_SIZING_RIGHT_BOTTOM) rcResult.right = rcResult.left  + dNewWidth;
			}
		}
	}
	else 
	{
		if(nLeftRight < 0) rcResult.left += rcCmpValue.left;
		if(nLeftRight > 0) rcResult.right += rcCmpValue.right;

		if(nTopBottom < 0) rcResult.top += rcCmpValue.top;
		if(nTopBottom > 0) rcResult.bottom += rcCmpValue.bottom;
	}

	return rcResult;
}

void CFrame_Wnd::SetMagneticFrame(int nMagneticFrame)
{
	m_nMagneticFrame = nMagneticFrame;
}

int CFrame_Wnd::GetMagneticFrame()
{
	return m_nMagneticFrame;
}

void CFrame_Wnd::DrawDebugInfo(CDC* pDC)
{
	POINT ptCursorPos = {0};
	::GetCursorPos(&ptCursorPos);
	ScreenToClient(&ptCursorPos);
	CPoint ptClientToTemplate = GetClientToTemplate(ptCursorPos);

#ifdef _DEBUG
	CPoint ptBasePos(0,0);
	CString strText;
	CString strFormat;
	strFormat.Format("Mouse Point     (%4d, %4d)\n"
		             "ClientToTemplate(%4d, %4d)\n"
					, ptCursorPos.x
					, ptCursorPos.y
					, ptClientToTemplate.x
					, ptClientToTemplate.y
					);
	strText += strFormat;

	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();
	if(template_link != NULL)
	{
		ptBasePos.x = template_link->rcViewRect[m_nIndex].right;
		ptBasePos.y = template_link->rcViewRect[m_nIndex].top;
		
		strFormat.Format("\nTemplate     Rect(%4d, %4d, %4d, %4d)(W:%4d,H:%4d)"
						 "\nTemplateInfo Rect(%4d, %4d, %4d, %4d)(W:%4d,H:%4d)\n"
						, template_link->rcViewRect[m_nIndex].left 
						, template_link->rcViewRect[m_nIndex].top
						, template_link->rcViewRect[m_nIndex].right
						, template_link->rcViewRect[m_nIndex].bottom
						, template_link->rcViewRect[m_nIndex].Width()
						, template_link->rcViewRect[m_nIndex].Height()
						, template_link->pTemplateInfo->rcRect.left 
						, template_link->pTemplateInfo->rcRect.top
						, template_link->pTemplateInfo->rcRect.right
						, template_link->pTemplateInfo->rcRect.bottom
						, template_link->pTemplateInfo->rcRect.Width()
						, template_link->pTemplateInfo->rcRect.Height()
						);
		strText += strFormat;

		int count = template_link->arFrameLinkList.GetCount();

		for(int i=0; i<count;i++)
		{
			FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
			if(frame_link.pFrameInfo->strId == m_strSelectFrameId)
			{
				strFormat.Format("\nFrame     Rect(%4d, %4d, %4d, %4d)(W:%4d,H:%4d)"
								 "\nFrameInfo Rect(%4d, %4d, %4d, %4d)(W:%4d,H:%4d)\n"
								, frame_link.rcViewRect[m_nIndex].left 
								, frame_link.rcViewRect[m_nIndex].top
								, frame_link.rcViewRect[m_nIndex].right
								, frame_link.rcViewRect[m_nIndex].bottom
								, frame_link.rcViewRect[m_nIndex].Width()
								, frame_link.rcViewRect[m_nIndex].Height()
								, frame_link.pFrameInfo->rcRect.left 
								, frame_link.pFrameInfo->rcRect.top
								, frame_link.pFrameInfo->rcRect.right
								, frame_link.pFrameInfo->rcRect.bottom
								, frame_link.pFrameInfo->rcRect.Width()
								, frame_link.pFrameInfo->rcRect.Height()
								);
				strText += strFormat;
				break;
			}
		}
	}

	strFormat.Format("\nFRAME_OUT_THICKNESS (%4d)"
					 "\nFRAME_IN_THICKNESS  (%4d)"
					 "\nfScale (%f)"
					, FRAME_OUT_THICKNESS
					, FRAME_IN_THICKNESS
					, m_fScale
					);
	strText += strFormat;

	CRect rcDebug(0, 0, 400, 200);
	rcDebug.MoveToXY(ptBasePos.x + 10, ptBasePos.y);

	CFont* ftOld = (CFont*)pDC->SelectObject(&m_font);
	COLORREF clOldText = pDC->SetTextColor(RGB(255,255,255)); 
	COLORREF clOldBk = pDC->SetBkColor(FRAME_OUT_BACKGROUND_COLOR);

	pDC->DrawText(strText, rcDebug, DT_LEFT);

	pDC->SelectObject(&ftOld);
	pDC->SetTextColor(clOldText);
	pDC->SetBkColor(clOldBk);
#endif
}

CPoint CFrame_Wnd::GetClientToTemplate(CPoint ptPoint)
{
	ptPoint.x = (ptPoint.x - FRAME_OUT_THICKNESS - FRAME_IN_THICKNESS) / m_fScale;
	ptPoint.y = (ptPoint.y - FRAME_OUT_THICKNESS - FRAME_IN_THICKNESS) / m_fScale;

	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();
	if(template_link != NULL)
	{
		ptPoint.x -= template_link->pTemplateInfo->rcRect.left;
		ptPoint.y -= template_link->pTemplateInfo->rcRect.top;
	}

	return ptPoint;
}

void CFrame_Wnd::AlignToFrame(ALIGN_TO_FRAME nAlign, CString strInFrameId /*=""*/)
{
	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();
	if(template_link == NULL) return;

	if(strInFrameId.IsEmpty())
	{
		strInFrameId = m_strSelectFrameId;
	}

	int count = template_link->arFrameLinkList.GetCount();
	for(int i=0; i<count; i++)
	{
		FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
		if(strInFrameId != frame_link.pFrameInfo->strId) continue;

		CPoint ptTemplateCenter = template_link->pTemplateInfo->rcRect.CenterPoint();
		CPoint ptFrameCenter = frame_link.pFrameInfo->rcRect.CenterPoint();

		CRect rcFrame = frame_link.pFrameInfo->rcRect;

		if( (nAlign & ALIGN_CENTER_VERTICAL) == ALIGN_CENTER_VERTICAL )
		{
			rcFrame.MoveToY(rcFrame.top + ptTemplateCenter.y - ptFrameCenter.y);
		}
		else if( (nAlign & ALIGN_TOP) == ALIGN_TOP )
		{
			rcFrame.MoveToY(template_link->pTemplateInfo->rcRect.top);
		}
		else if( (nAlign & ALIGN_BOTTOM) == ALIGN_BOTTOM )
		{
			rcFrame.MoveToY(template_link->pTemplateInfo->rcRect.bottom - rcFrame.Height());
		}

		if( (nAlign & ALIGN_CENTER_HORIZONTAL) == ALIGN_CENTER_HORIZONTAL )
		{
			rcFrame.MoveToX(rcFrame.left + ptTemplateCenter.x - ptFrameCenter.x);
		}
		else if( (nAlign & ALIGN_LEFT) == ALIGN_LEFT )
		{
			rcFrame.MoveToX(template_link->pTemplateInfo->rcRect.top);
		}
		else if( (nAlign & ALIGN_RIGHT) == ALIGN_RIGHT )
		{
			rcFrame.MoveToX(template_link->pTemplateInfo->rcRect.right - rcFrame.Width());
		}

		frame_link.pFrameInfo->rcRect = rcFrame;
		frame_link.rcViewRect[m_nIndex] = GetInfoToRect(rcFrame);

		template_link->ReCalcPIP();

		Invalidate();

		GetParent()->SendMessage(WM_FRAME_INFO_CHANGED, 0, MOUSE_SIZING_CENTER);
	}
}

void CFrame_Wnd::MoveFrame(int nXoffset, int nYoffset, CString strInFrameId /*=""*/)
{
	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();
	if(template_link == NULL) return;

	if(strInFrameId.IsEmpty())
	{
		strInFrameId = m_strSelectFrameId;
	}

	int count = template_link->arFrameLinkList.GetCount();
	for(int i=0; i<count; i++)
	{
		FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
		if(strInFrameId != frame_link.pFrameInfo->strId) continue;

		CRect rcFrame = frame_link.pFrameInfo->rcRect;

		if(template_link->pTemplateInfo->rcRect.right < rcFrame.right + nXoffset)
		{
			nXoffset = 0;
		}

		if(template_link->pTemplateInfo->rcRect.bottom < rcFrame.bottom + nYoffset)
		{
			nYoffset = 0;
		}

		if(nXoffset == 0 && nYoffset == 0) return;

		rcFrame.OffsetRect(nXoffset, nYoffset);

		frame_link.pFrameInfo->rcRect = rcFrame;
		frame_link.rcViewRect[m_nIndex] = GetInfoToRect(rcFrame);

		template_link->ReCalcPIP();

		Invalidate();

		GetParent()->SendMessage(WM_FRAME_INFO_CHANGED, 0, MOUSE_SIZING_CENTER);
	}
}

void CFrame_Wnd::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(m_bEditable)
	{
		// Copy
		if(IsCTRLPressed() && nChar == 'C' && !m_strSelectFrameId.IsEmpty())
		{
			CopyFrameReady(m_strSelectFrameId);
		}
		// Paste
		else if(IsCTRLPressed() && nChar == 'V')
		{
			PasteFrame();
		}
		else if(nChar == VK_NUMPAD1) AlignToFrame(CFrame_Wnd::ALIGN_LEFT_BOTTOM );
		else if(nChar == VK_NUMPAD2) AlignToFrame(CFrame_Wnd::ALIGN_BOTTOM      );
		else if(nChar == VK_NUMPAD3) AlignToFrame(CFrame_Wnd::ALIGN_RIGHT_BOTTOM);
		else if(nChar == VK_NUMPAD4) AlignToFrame(CFrame_Wnd::ALIGN_LEFT        );
		else if(nChar == VK_NUMPAD5) AlignToFrame(CFrame_Wnd::ALIGN_CENTER_BOTH );
		else if(nChar == VK_NUMPAD6) AlignToFrame(CFrame_Wnd::ALIGN_RIGHT       );
		else if(nChar == VK_NUMPAD7) AlignToFrame(CFrame_Wnd::ALIGN_LEFT_TOP    );
		else if(nChar == VK_NUMPAD8) AlignToFrame(CFrame_Wnd::ALIGN_TOP         );
		else if(nChar == VK_NUMPAD9) AlignToFrame(CFrame_Wnd::ALIGN_RIGHT_TOP   );
	}

	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CFrame_Wnd::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN && m_bEditable)
	{
		if     (pMsg->wParam == VK_LEFT ) { MoveFrame(-1, 0); return TRUE; }
		else if(pMsg->wParam == VK_RIGHT) { MoveFrame( 1, 0); return TRUE; }
		else if(pMsg->wParam == VK_UP   ) { MoveFrame( 0,-1); return TRUE; }
		else if(pMsg->wParam == VK_DOWN ) { MoveFrame( 0, 1); return TRUE; }
	}

	return CWnd::PreTranslateMessage(pMsg);
}

bool CFrame_Wnd::CopyFrameReady(CString strInFrameId /*=""*/)
{
	m_bPasteReady = false;

	if(strInFrameId.IsEmpty())
	{
		strInFrameId = m_strSelectFrameId;
	}

	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();
	if(template_link == NULL) return false;

	int count = template_link->arFrameLinkList.GetCount();
	for(int i=0; i<count; i++)
	{
		FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
		if(strInFrameId == frame_link.pFrameInfo->strId)
		{
			m_bPasteReady = true;
			m_stPasteReady = *frame_link.pFrameInfo;

			// 프레임에 속한 콘텐츠는 복사할 필요가 없으므로 삭제한다
			m_stPasteReady.DeletePlayContentsList();
			break;
		}
	}

	return m_bPasteReady;
}

bool CFrame_Wnd::PasteFrame()
{
	if(!m_bPasteReady) return false;

	TEMPLATE_LINK* template_link = m_pDocument->GetSelectTemplateLink();
	if(template_link == NULL) return false;

	// 원본의 템플릿ID와 현재 선택된 템플릿ID가 다른경우 처리하지 않는다.
	// 복사해도 되지만, 해상도 및 세로 가로 비율이 다른경우가 있을 수 있으므로 하지않도록 함.
	if(m_stPasteReady.strTemplateId != template_link->pTemplateInfo->strId) return false;

	FRAME_INFO* pFrameInfo = m_pDocument->CreateNewFrame(template_link->pTemplateInfo->strId);
	if(pFrameInfo == NULL) return false;

	// 유지해야할 속성은 따로 저장해 둔다
	CString strNewTemplateId = pFrameInfo->strTemplateId;
	CString strNewId         = pFrameInfo->strId        ;
	int		nNewGrade        = pFrameInfo->nGrade       ;
	int		nNewZIndex       = pFrameInfo->nZIndex      ;

	int count = template_link->arFrameLinkList.GetCount();
	for(int i=0; i<count; i++)
	{
		FRAME_LINK& frame_link = template_link->arFrameLinkList.GetAt(i);
		if(strNewId == frame_link.pFrameInfo->strId)
		{
			//frame_link.Copy(m_stPasteReady);
			*frame_link.pFrameInfo = m_stPasteReady;

			// 원본위치와 동일하게 되는 경우 헤깔리므로 프레임 생성위치로 이동시킨다
			frame_link.pFrameInfo->rcRect.MoveToXY(0, 0);

			frame_link.pFrameInfo->strTemplateId = strNewTemplateId;
			frame_link.pFrameInfo->strId         = strNewId        ;
			frame_link.pFrameInfo->nGrade        = nNewGrade       ;
			frame_link.pFrameInfo->nZIndex       = nNewZIndex      ;

			RecalcLayout();
			GetParent()->SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, (WPARAM)(LPCTSTR)template_link->pTemplateInfo->strId, (WPARAM)(LPCTSTR)strNewId );
			break;
		}
	}

	return true;
}