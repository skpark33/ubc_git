// CyclePlayContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "CyclePlayContentsDialog.h"


// CCyclePlayContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCyclePlayContentsDialog, CSubPlayContentsDialog)

CCyclePlayContentsDialog::CCyclePlayContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubPlayContentsDialog(CCyclePlayContentsDialog::IDD, pParent)
	, m_bEditMode (false)
	, m_nTimeScope(-1)
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정
	,m_pPlayContentsInfo(NULL)
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정
{
	
}

CCyclePlayContentsDialog::~CCyclePlayContentsDialog()
{
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정
	if(m_pPlayContentsInfo)
	{
		delete m_pPlayContentsInfo;
	}//if
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정
}

void CCyclePlayContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubPlayContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DATE_START, m_dateStart);
	DDX_Control(pDX, IDC_DATE_END, m_dateEnd);
	DDX_Control(pDX, IDC_CHECK_TIME_0, m_checkTime[0]);
	DDX_Control(pDX, IDC_CHECK_TIME_1, m_checkTime[1]);
	DDX_Control(pDX, IDC_CHECK_TIME_2, m_checkTime[2]);
	DDX_Control(pDX, IDC_CHECK_TIME_3, m_checkTime[3]);
	DDX_Control(pDX, IDC_CHECK_TIME_4, m_checkTime[4]);
	DDX_Control(pDX, IDC_CHECK_TIME_5, m_checkTime[5]);
	DDX_Control(pDX, IDC_CHECK_TIME_6, m_checkTime[6]);
	DDX_Control(pDX, IDC_CHECK_TIME_7, m_checkTime[7]);
	DDX_Control(pDX, IDC_DONTDOWNLOAD_CHECK, m_dontDownloadCheck);
}


BEGIN_MESSAGE_MAP(CCyclePlayContentsDialog, CSubPlayContentsDialog)
	ON_BN_CLICKED(IDC_CHECK_TIME_0, &CCyclePlayContentsDialog::OnBnClickedCheckTimeAll)
	ON_BN_CLICKED(IDC_CHECK_TIME_1, &CCyclePlayContentsDialog::OnBnClickedCheckTime)
	ON_BN_CLICKED(IDC_CHECK_TIME_2, &CCyclePlayContentsDialog::OnBnClickedCheckTime)
	ON_BN_CLICKED(IDC_CHECK_TIME_3, &CCyclePlayContentsDialog::OnBnClickedCheckTime)
	ON_BN_CLICKED(IDC_CHECK_TIME_4, &CCyclePlayContentsDialog::OnBnClickedCheckTime)
	ON_BN_CLICKED(IDC_CHECK_TIME_5, &CCyclePlayContentsDialog::OnBnClickedCheckTime)
	ON_BN_CLICKED(IDC_CHECK_TIME_6, &CCyclePlayContentsDialog::OnBnClickedCheckTime)
	ON_BN_CLICKED(IDC_CHECK_TIME_7, &CCyclePlayContentsDialog::OnBnClickedCheckTime)
END_MESSAGE_MAP()


// CCyclePlayContentsDialog 메시지 처리기입니다.

BOOL CCyclePlayContentsDialog::OnInitDialog()
{
	CSubPlayContentsDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	CTime tmStart;
	m_dateStart.GetTime(tmStart);
	//CTimeSpan spanYear(31536000 * 10);		//1년 * 10
	//tmStart += spanYear;
	//m_dateEnd.SetTime(&tmStart);
	CTime tmMax = CTime(2030,12,31,23,59,59);  //skpark max 로 바꿈 2012.11.08
	m_dateEnd.SetTime(&tmMax);

	InitDateRange(m_dateEnd);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCyclePlayContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubPlayContentsDialog::OnOK();
}

void CCyclePlayContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubPlayContentsDialog::OnCancel();
	GetParent()->PostMessage(WM_CLOSE);
}

// Modified by 정운형 2008-12-30 오전 10:57
// 변경내역 :  PlayContents 추가부분 수정
/*
bool CCyclePlayContentsDialog::GetPlayContentsInfo(PLAYCONTENTS_INFO& info, int nIndex)
{
	info.isDefaultPlayContents = true;

	CTime start_date, end_date;
	m_dateStart.GetTime(start_date);
	m_dateEnd.GetTime(end_date);

	info.nStartDate = start_date.GetTime();
	info.nEndDate = end_date.GetTime();

	int count = 0;
	for(int i=0; i<8; i++)
		if(m_checkTime[i].GetCheck() == BST_CHECKED)
		{
			if(nIndex == count)
			{
				info.nTimeScope = i;
				switch(i)
				{
				case 0:
					info.strStartTime = "00:00";	info.strEndTime = "23:59";	break;
				case 1:
					info.strStartTime = "07:00";	info.strEndTime = "08:59";	break;
				case 2:
					info.strStartTime = "09:00";	info.strEndTime = "11:59";	break;
				case 3:
					info.strStartTime = "12:00";	info.strEndTime = "12:59";	break;
				case 4:
					info.strStartTime = "13:00";	info.strEndTime = "17:59";	break;
				case 5:
					info.strStartTime = "18:00";	info.strEndTime = "21:59";	break;
				case 6:
					info.strStartTime = "22:00";	info.strEndTime = "23:59";	break;
				}
				return true;
			}
			count++;
		}

	UbcMessageBox("Select Time Scope !!!", MB_ICONSTOP);
	return false;
}
*/
bool CCyclePlayContentsDialog::GetPlayContentsInfo(PLAYCONTENTS_INFO& info, int nIndex)
{
	if(m_pPlayContentsInfo == NULL)
	{
		//return false;
		m_pPlayContentsInfo = new PLAYCONTENTS_INFO;
		*m_pPlayContentsInfo = info;
	}//if

	m_pPlayContentsInfo->bIsDefaultPlayContents = true;

	CTime start_date, end_date;
	m_dateStart.GetTime(start_date);
	m_dateEnd.GetTime(end_date);

	m_pPlayContentsInfo->nStartDate = (int)start_date.GetTime();
	m_pPlayContentsInfo->nEndDate = (int)end_date.GetTime();

	m_pPlayContentsInfo->bDontDownload = this->m_dontDownloadCheck.GetCheck();

	int count = 0;
	for(int i=0; i<8; i++)
	{
		if(m_checkTime[i].GetCheck() == BST_CHECKED)
		{
			if(nIndex == count)
			{
				m_pPlayContentsInfo->nTimeScope = i;
				switch(i)
				{
				case 0:
					m_pPlayContentsInfo->strStartTime = "00:00";	m_pPlayContentsInfo->strEndTime = "23:59";	break;
				case 1:
					m_pPlayContentsInfo->strStartTime = "07:00";	m_pPlayContentsInfo->strEndTime = "08:59";	break;
				case 2:
					m_pPlayContentsInfo->strStartTime = "09:00";	m_pPlayContentsInfo->strEndTime = "11:59";	break;
				case 3:
					m_pPlayContentsInfo->strStartTime = "12:00";	m_pPlayContentsInfo->strEndTime = "12:59";	break;
				case 4:
					m_pPlayContentsInfo->strStartTime = "13:00";	m_pPlayContentsInfo->strEndTime = "17:59";	break;
				case 5:
					m_pPlayContentsInfo->strStartTime = "18:00";	m_pPlayContentsInfo->strEndTime = "21:59";	break;
				case 6:
					m_pPlayContentsInfo->strStartTime = "22:00";	m_pPlayContentsInfo->strEndTime = "23:59";	break;
				}//switch

				info = *m_pPlayContentsInfo;
				return true;
			}//if
			count++;
		}//if
	}//for

	UbcMessageBox(LoadStringById(IDS_CYCLEPLAYCONTENTSDIALOG_MSG001), MB_ICONSTOP);
	return false;
}
// Modified by 정운형 2008-12-30 오전 10:57
// 변경내역 :  PlayContents 추가부분 수정

bool CCyclePlayContentsDialog::SetPlayContentsInfo(PLAYCONTENTS_INFO& info)
{
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정
	m_pPlayContentsInfo = new PLAYCONTENTS_INFO;
	*m_pPlayContentsInfo = info;
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정


	m_bEditMode = true;

	if(info.nStartDate > 0)
	{
		//int pos = 0;
		//CString year = info.nStartDate.Tokenize("/:-", pos);
		//CString month= info.nStartDate.Tokenize("/:-", pos);
		//CString day  = info.nStartDate.Tokenize("/:-", pos);

		//CTime start_date(atoi(year), atoi(month), atoi(day), 0,0,0);
		//m_dateStart.SetTime(&start_date);
		CTime start_date(info.nStartDate);
		m_dateStart.SetTime(&start_date);
	}

	if(info.nEndDate > 0)
	{
		//int pos = 0;
		//CString year = info.nEndDate.Tokenize("/:-", pos);
		//CString month= info.nEndDate.Tokenize("/:-", pos);
		//CString day  = info.nEndDate.Tokenize("/:-", pos);

		//CTime end_date(atoi(year), atoi(month), atoi(day), 0,0,0);
		//m_dateEnd.SetTime(&end_date);
		CTime end_date(info.nEndDate);
		m_dateEnd.SetTime(&end_date);
	}

	for(int i=0 ;i<8; i++)
		m_checkTime[i].SetCheck( (i == info.nTimeScope) ? BST_CHECKED : BST_UNCHECKED );
	m_nTimeScope = info.nTimeScope;

	if(info.nTimeScope == 0)
		OnBnClickedCheckTimeAll();

	m_dontDownloadCheck.SetCheck(info.bDontDownload);

	return true;
}

int CCyclePlayContentsDialog::GetPlayContentsCount()
{
	if(m_checkTime[0].GetCheck() == BST_CHECKED)
		return 1;

	int count = 0;
	for(int i=1; i<8; i++)
		if(m_checkTime[i].GetCheck() == BST_CHECKED)
		{
			count++;
		}

	return count;
}

void CCyclePlayContentsDialog::OnBnClickedCheckTimeAll()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int check = (m_checkTime[0].GetCheck() == BST_CHECKED) ? BST_CHECKED : BST_UNCHECKED;
	int new_time_scope = -1;

	for(int i=1; i<8; i++)
	{
		if(m_bEditMode && m_nTimeScope == 0)
		{
			if(m_checkTime[i].GetCheck() == BST_UNCHECKED)
				new_time_scope = i;
		}
		m_checkTime[i].SetCheck(check);
	}

	if(m_bEditMode)
	{
		if( check == BST_UNCHECKED)
		{
			if( new_time_scope < 0 )
				new_time_scope = 1;
			m_nTimeScope = new_time_scope;
			m_checkTime[m_nTimeScope].SetCheck(BST_CHECKED);
		}
		else
		{
			m_nTimeScope = 0;
		}
	}
}

void CCyclePlayContentsDialog::OnBnClickedCheckTime()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(m_bEditMode)
	{
		m_checkTime[m_nTimeScope].SetCheck(BST_UNCHECKED);
		if(m_nTimeScope == 0)
		{
			OnBnClickedCheckTimeAll();
		}
		else
		{
			int new_time_scope = -1;
			for(int i=0; i<8; i++)
				if(m_checkTime[i].GetCheck() == BST_CHECKED)
				{
					new_time_scope = i;
					if(i == 0)
						OnBnClickedCheckTimeAll();
				}

			if(new_time_scope >= 0)
			{
				if(m_nTimeScope >= 0)
					m_checkTime[m_nTimeScope].SetCheck(BST_UNCHECKED);
				m_nTimeScope = new_time_scope;
			}
			else
			{
				m_checkTime[m_nTimeScope].SetCheck(BST_CHECKED);
			}
		}
	}
	else
	{
		bool all_check = true;

		for(int i=1; i<8; i++)
		{
			if(m_checkTime[i].GetCheck() == BST_UNCHECKED)
			{
				all_check = false;
				break;
			}
		}

		if(all_check)
			m_checkTime[0].SetCheck(BST_CHECKED);
		else
			m_checkTime[0].SetCheck(BST_UNCHECKED);
	}
}
