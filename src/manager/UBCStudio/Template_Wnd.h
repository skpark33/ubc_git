#pragma once


// CTemplate_Wnd

class CUBCStudioDoc;

class CTemplate_Wnd : public CWnd
{
	DECLARE_DYNAMIC(CTemplate_Wnd)

public:
	enum TEMPLATE_WND_TYPE { 
		TEMPLATE_WND_LAYOUT = 0,
		TEMPLATE_WND_PACKAGE,
	};

	CTemplate_Wnd(int nIndex, TEMPLATE_WND_TYPE eWndType, bool bIsVertical);
	virtual ~CTemplate_Wnd();

protected:
	DECLARE_MESSAGE_MAP()

	COLORREF	TEMPLATE_SELECT_OUTLINE_COLOR;
	COLORREF	TEMPLATE_SELECT_BG_COLOR;
	COLORREF	TEMPLATE_DESELECT_OUTLINE_COLOR;
	COLORREF	TEMPLATE_DESELECT_BG_COLOR;
	COLORREF	FRAME_SELECT_OUTLINE_COLOR;
	COLORREF	FRAME_SELECT_TEXT_COLOR;
	COLORREF	FRAME_SELECT_BG_COLOR;
	COLORREF	FRAME_DESELECT_OUTLINE_COLOR;
	COLORREF	FRAME_DESELECT_TEXT_COLOR;
	COLORREF	FRAME_DESELECT_BG_COLOR;
	COLORREF	TEMPLATE_INFO_SELECT_TEXT_COLOR;
	COLORREF	TEMPLATE_INFO_DESELECT_TEXT_COLOR;
	COLORREF	TEMPLATE_INFO_TIMES_BG_COLOR;

	TEMPLATE_WND_TYPE	m_eWndType;
	CUBCStudioDoc*		m_pDocument;

	int		m_nIndex;
	bool	m_bIsVertical;
	CFont	m_fontNormal;
	CFont	m_fontBold;
	CWnd*	m_pFocusWindow;

	CImageList	m_ilContentsList;

	TEMPLATE_LINK_LIST*		GetTemplateList();

public:
	int		m_nSelectTemplateLinkIndex;

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg LRESULT OnRegisteredMouseWheel(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTemplateListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPlayContentsListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEndLabelEdit(WPARAM wParam, LPARAM lParam);

	void	SetFocusWindow(CWnd* pFocusWindow) { m_pFocusWindow = pFocusWindow; };
	void	SetDocument(CUBCStudioDoc* pDoc) { m_pDocument = pDoc; };

	void	ResetSelectTemplateLinkIndex() { m_nSelectTemplateLinkIndex = -1; };
	int		GetSelectTemplateLinkIndex() { return m_nSelectTemplateLinkIndex; };

	void	DrawTemplate(CDC* pDC, TEMPLATE_LINK_LIST* pTemplateList);
	void	DrawFrame(CDC* pDC, TEMPLATE_LINK* pTemplateLink);

	int		GetViewRectPosVert(int count);
	int		GetViewRectPosHorz(int count);

	int		GetTotalHeight(int count);
	int		GetTotalWidth(int count);

	void	RecalcLayout();
	void	ScrollToEnd();

	void	EnsureVisible(int nIndex);
	void	EnsureVisible(LPCTSTR strID);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
};


