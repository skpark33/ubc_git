#include "StdAfx.h"
#include "Enviroment.h"
#include "ScanNetHost.h"
#include <Iphlpapi.h>
#include <winsock2.h>

CScanNetHost::CScanNetHost(void)
: m_nRunThreadCnt(0)
, m_nItemIndex(0)
{
	::InitializeCriticalSection(&m_csLock);
}

CScanNetHost::~CScanNetHost(void)
{
	DeleteAllItem();
	DeleteCriticalSection(&m_csLock);
}

bool CScanNetHost::DeleteAllItem()
{
	::EnterCriticalSection(&m_csLock);

	for(int i=0; i<m_ItemArray.GetCount() ;i++)
	{
		IPInfo* pItem = (IPInfo*)m_ItemArray[i];
		if(pItem) delete pItem;
	}

	m_ItemArray.RemoveAll();

	::LeaveCriticalSection(&m_csLock);

	return true;
}

void CScanNetHost::MoveFirst()
{
	::EnterCriticalSection(&m_csLock);

	InterlockedExchange(&m_nItemIndex, 0);

	::LeaveCriticalSection(&m_csLock);
}

IPInfo* CScanNetHost::GetNextItem()
{
	IPInfo* pItem = NULL;

	::EnterCriticalSection(&m_csLock);

	if(m_nItemIndex < 0 || m_nItemIndex >= m_ItemArray.GetCount())
	{
		::LeaveCriticalSection(&m_csLock);
		return NULL;
	}

	pItem = (IPInfo*)m_ItemArray[m_nItemIndex++];

	::LeaveCriticalSection(&m_csLock);

	return pItem;
}

int CScanNetHost::InitItemArray()
{
	DeleteAllItem();

	//IPInfo* pItem = new IPInfo;
	//pItem->ip = "211.232.57.245";
	//m_ItemArray.Add(pItem);

	//return m_ItemArray.GetCount();

	//adapter list 정보구하기
	IP_ADAPTER_INFO adapterInfo[100];
	ULONG ulOutputBufLen = sizeof(adapterInfo);

	if(::GetAdaptersInfo(adapterInfo, &ulOutputBufLen) != ERROR_SUCCESS) return 0;

	for(PIP_ADAPTER_INFO pIpAdapterInfo = &(adapterInfo[0]); pIpAdapterInfo != NULL ; pIpAdapterInfo = pIpAdapterInfo->Next)
	{
		// ip & subnet mask 구하기
		for(PIP_ADDR_STRING pIpAddrString = &(pIpAdapterInfo->IpAddressList); pIpAddrString != NULL ; pIpAddrString = pIpAddrString->Next)
		{
			CString strTmp;
			strTmp.Format("%s(%s)\r\n", pIpAddrString->IpAddress.String, pIpAddrString->IpMask.String);

			DWORD ip = ntohl(inet_addr(pIpAddrString->IpAddress.String));
			DWORD mask = (strlen(pIpAddrString->IpMask.String) > 0 ? ntohl(inet_addr(pIpAddrString->IpMask.String)) : 0xFFFFFF00);

			BYTE* ip_b = (BYTE *)(&ip);
			BYTE* mask_b = (BYTE *)(&mask);
			BYTE temp_b[4] = {0};

			// 배열만들기
			for(int first=mask_b[0] ; first<=0xFF ; first++)
			{
				if(mask_b[0] == 0xFF) temp_b[0] = ip_b[0];
				else temp_b[0] = first;

				for(int second=mask_b[1] ; second<=0xFF ; second++)
				{
					if(mask_b[1] == 0xFF) temp_b[1] = ip_b[1];
					else temp_b[1] = second;

					for(int third=mask_b[2] ; third<=0xFF ; third++)
					{
						if(mask_b[2] == 0xFF) temp_b[2] = ip_b[2];
						else temp_b[2] = third;

						for(int forth=mask_b[3] ; forth<=0xFF ; forth++)
						{
							if(mask_b[3] == 0xFF) temp_b[3] = ip_b[3];
							else temp_b[3] = forth;

							CString strTempIp;
							strTempIp.Format(_T("%d.%d.%d.%d")
											, temp_b[3]
											, temp_b[2]
											, temp_b[1]
											, temp_b[0]
											);

							IPInfo* pItem = new IPInfo;
							pItem->ip = strTempIp;
							m_ItemArray.Add(pItem);
						}
					}
				}
			}
		}
	}

	return m_ItemArray.GetCount();
}

bool CScanNetHost::AutoSearch(int nThreadCnt/*=255*/)
{
	// 1. read ini file

	CString strFilename;
	strFilename.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	TCHAR buf[4096];
	memset(buf,0x00,4096);
	GetPrivateProfileString("NetworkDevice", "DeviceNameList", "", buf, sizeof(buf), strFilename);
	CString strDeviceNameList = buf;

	CMapStringToPtr aHostMap;
	if(!strDeviceNameList.IsEmpty())
	{
		int nPos=0;
		CString strHostName;
		while( !(strHostName = strDeviceNameList.Tokenize(",", nPos)).IsEmpty() )
		{
			IPInfo* pInfo = new IPInfo();
			pInfo->hostName = strHostName;

			memset(buf, 0x00, 4096);
			GetPrivateProfileString(strHostName, "USERCREATE", "false", buf, 4096, strFilename);
			pInfo->userCreate = buf;

			memset(buf,0x00,4096);
			GetPrivateProfileString(strHostName, "SHUTDOWNTIME", "", buf, 4096, strFilename);
			pInfo->shutdownTime = buf;

			memset(buf,0x00,4096);
			GetPrivateProfileString(strHostName, "IP", "", buf, 4096, strFilename);
			pInfo->ip = buf;
		
			memset(buf,0x00,4096);
			GetPrivateProfileString(strHostName, "FTPPort", "21", buf, 4096, strFilename);
			pInfo->ftpPort = buf;

			memset(buf,0x00,4096);
			GetPrivateProfileString(strHostName, "SvrPort", "14007", buf, 4096, strFilename);
			pInfo->svrPort = buf;

			memset(buf,0x00,4096);
			GetPrivateProfileString(strHostName, "ID", "", buf, 4096, strFilename);
			pInfo->id = buf;

			memset(buf,0x00,4096);
			GetPrivateProfileString(strHostName, "PWD", "", buf, 4096, strFilename);
			pInfo->pwd = buf;

			memset(buf,0x00,4096);
			GetPrivateProfileString(strHostName, "MAC", "", buf, 4096, strFilename);
			pInfo->mac = buf;

			memset(buf,0x00,4096);
			GetPrivateProfileString(strHostName, "MonitorCount", "1", buf, 4096, strFilename);
			pInfo->monitorCount = buf;

			memset(buf,0x00,4096);
			GetPrivateProfileString(strHostName, "Description", "", buf, 4096, strFilename);
			pInfo->description = buf;

			pInfo->isAlive = false;  // 일단 죽어있는것으로 간주한다.

			aHostMap[pInfo->hostName.c_str()] = pInfo;
		}
	}

	//2. ip array 만들기
	InitItemArray();

	//3. ip array로 호스트 조회
	CPtrArray aThread;
	m_nRunThreadCnt = (m_ItemArray.GetCount() > nThreadCnt ? nThreadCnt : m_ItemArray.GetCount());
	for(int i=0; i<m_nRunThreadCnt ;i++)
	{
		CWinThread* pThread = AfxBeginThread(&CScanNetHost::ScanHostThread, (LPVOID)this, 0, 0, CREATE_SUSPENDED);
		aThread.Add(pThread);
	}

	for(int i=0; i<aThread.GetCount() ;i++)
	{
		CWinThread* pThread = (CWinThread*)aThread[i];
		if(pThread)
		{
			pThread->ResumeThread();
		}
	}

	// 스레드가 완료될때까지 기다림.
	while(m_nRunThreadCnt > 0) Sleep(1000);

	// 4.  merge 1 & 2
	for(int i=0; i<m_ItemArray.GetCount() ;i++)
	{
		IPInfo* pScanInfo = (IPInfo*)m_ItemArray[i];
		if(!pScanInfo) continue;
		if( pScanInfo->hostName.length() <= 0 ) continue;
		if( pScanInfo->hostName == _T("pe_ping") ) continue;

		IPInfo* pMapInfo = (IPInfo*)aHostMap[pScanInfo->hostName.c_str()];
		if(!pMapInfo)
		{
			pMapInfo = new IPInfo();
			pMapInfo->owner = false;
		}

		pMapInfo->ip = pScanInfo->ip;
		pMapInfo->hostName = pScanInfo->hostName;
		pMapInfo->shutdownTime = pScanInfo->shutdownTime;
		pMapInfo->mac = pScanInfo->mac;
		pMapInfo->monitorCount = pScanInfo->monitorCount;
		pMapInfo->isAlive = true; // 발견되었으므로 살아있다.

		aHostMap[pMapInfo->hostName.c_str()] = pMapInfo;
	}

	strDeviceNameList = "";

	// 5. build DeviceNameList & write ini file
	POSITION pos = aHostMap.GetStartPosition();
	while(pos != NULL)
	{
		CString strHostName;
		IPInfo* pMapInfo = NULL;
		aHostMap.GetNextAssoc( pos, strHostName, (void*&)pMapInfo );

		if(!pMapInfo) continue;
		if( pMapInfo->hostName.length() > 0 )
		{
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "IP", pMapInfo->ip.c_str(), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "FTPPort", pMapInfo->ftpPort.c_str(), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "SvrPort", pMapInfo->svrPort.c_str(), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "ID", pMapInfo->id.c_str(), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "PWD", pMapInfo->pwd.c_str(), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "SHUTDOWNTIME", pMapInfo->shutdownTime.c_str(), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "USERCREATE", pMapInfo->userCreate.c_str(), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "MAC", pMapInfo->mac.c_str(), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "MonitorCount", pMapInfo->monitorCount.c_str(), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "Alive", (pMapInfo->isAlive ? "true" : "false"), strFilename);
			WritePrivateProfileString(pMapInfo->hostName.c_str(), "Description", pMapInfo->description.c_str(), strFilename);

			strDeviceNameList += strHostName + _T(",");
		}

		delete pMapInfo;
	}

	strDeviceNameList.Trim(_T(","));

	WritePrivateProfileString("NetworkDevice", "DeviceNameList", strDeviceNameList, strFilename);

	// 6. memory clear
	DeleteAllItem();

	return true;
}

UINT CScanNetHost::ScanHostThread(LPVOID param)
{
	CScanNetHost* pData = (CScanNetHost*)param;
	if(!pData) return 0;

	scratchUtil* aUtil = scratchUtil::getInstance();

	IPInfo* pIpInfo = NULL;
	while( (pIpInfo = pData->GetNextItem()))
	{
		std::string retval;
		if(aUtil->socketAgent3(pIpInfo->ip.c_str(), 14007, "pe_ping", " ", retval))
		{
			aUtil->splitResult(retval.c_str(), pIpInfo->hostName, pIpInfo->shutdownTime, pIpInfo->mac, pIpInfo->monitorCount);
		}
	}

	InterlockedDecrement(&(pData->m_nRunThreadCnt));

	return 0;
}
