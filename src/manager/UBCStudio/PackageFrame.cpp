// PackageFrame.cpp : implementation of the CPackageFrame class
//
#include "stdafx.h"
#include "UBCStudio.h"

#include "PackageFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPackageFrame

IMPLEMENT_DYNCREATE(CPackageFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPackageFrame, CMDIChildWnd)
	ON_WM_GETMINMAXINFO()
	ON_MESSAGE(WM_TEMPLATE_INFO_CHANGED, OnTemplateInfoChanged)
	ON_MESSAGE(WM_FRAME_INFO_CHANGED, OnFrameInfoChanged)
	ON_MESSAGE(WM_PLAY_TEMPLATE_LIST_CHANGED, OnPlayTemplateListChanged)
	ON_MESSAGE(WM_CONTENTS_LIST_CHANGED, OnContentsListChanged)

	ON_MESSAGE(WM_SAVE_CONFIG_COMPELETE, OnConfigSaveComplete)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CPackageFrame construction/destruction

CPackageFrame::CPackageFrame()
{
	// TODO: add member initialization code here
}

CPackageFrame::~CPackageFrame()
{
}


BOOL CPackageFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style = WS_CHILD | WS_VISIBLE | WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU
		| FWS_ADDTOTITLE | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;

	return TRUE;
}


// CPackageFrame diagnostics

#ifdef _DEBUG
void CPackageFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPackageFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// CPackageFrame message handlers

void CPackageFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);

//	lpMMI->ptMinTrackSize.x = 720;
//	lpMMI->ptMinTrackSize.y = 540;
}

LRESULT CPackageFrame::OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->SendMessage(WM_TEMPLATE_INFO_CHANGED);
}

LRESULT CPackageFrame::OnFrameInfoChanged(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->SendMessage(WM_FRAME_INFO_CHANGED);
}

LRESULT CPackageFrame::OnPlayTemplateListChanged(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED);
}

LRESULT CPackageFrame::OnContentsListChanged(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->SendMessage(WM_CONTENTS_LIST_CHANGED);
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// complete config save message handler\n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CPackageFrame::OnConfigSaveComplete(WPARAM wParam, LPARAM lParam)
{
	return GetActiveView()->SendMessage(WM_SAVE_CONFIG_COMPELETE);
}

int CPackageFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDR_PACKAGE), false);

	return 0;
}
