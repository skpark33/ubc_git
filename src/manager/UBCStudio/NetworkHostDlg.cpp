// NetworkHostDlg.cpp : 구현 파일입니다.
//
#include "stdafx.h"
#include "UBCStudio.h"
#include "NetworkHostDlg.h"
#include "NetworkHostManageDlg.h"
#include "Enviroment.h"
#include "ScanNetHost.h"

#include "common\libscratch\scratchUtil.h"
#include "libFileTransfer\FTClient.h"

// CNetworkHostDlg 대화 상자입니다.
IMPLEMENT_DYNAMIC(CNetworkHostDlg, CDialog)

CNetworkHostDlg::CNetworkHostDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNetworkHostDlg::IDD, pParent)
	, m_nFindHostType(0)
{
	m_bAll = FALSE;
}

CNetworkHostDlg::~CNetworkHostDlg()
{
}

void CNetworkHostDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NETWORK_HOST_LIST, m_lcList);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_REFRESH_BTN, m_btnRefresh);
	DDX_Control(pDX, IDC_EDIT_BTN, m_btnEdit);
	DDX_Control(pDX, IDC_CHECK_ALL, m_chkAll);
	DDX_Radio(pDX, IDC_RB_FIND_HOST_TYPE, m_nFindHostType);
}

BEGIN_MESSAGE_MAP(CNetworkHostDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CNetworkHostDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CNetworkHostDlg::OnBnClickedCancel)
	ON_NOTIFY(NM_CLICK, IDC_NETWORK_HOST_LIST, &CNetworkHostDlg::OnNMClickNetworkHostList)
	ON_NOTIFY(NM_DBLCLK, IDC_NETWORK_HOST_LIST, &CNetworkHostDlg::OnNMDblclkNetworkHostList)
	ON_BN_CLICKED(IDC_REFRESH_BTN, &CNetworkHostDlg::OnBnClickedRefreshBtn)
	ON_BN_CLICKED(IDC_EDIT_BTN, &CNetworkHostDlg::OnBnClickedEditBtn)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_NETWORK_HOST_LIST, &CNetworkHostDlg::OnLvnColumnclickHostList)
END_MESSAGE_MAP()

// CNetworkHostDlg 메시지 처리기입니다.
void CNetworkHostDlg::OnBnClickedOk()
{
	UpdateData();
	m_bAll = m_chkAll.GetCheck();

	m_arySelectedNetInfo.RemoveAll();

	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		if(m_lcList.GetCheck(i))
		{
			m_arySelectedNetInfo.Add((CNetDeviceInfo*)m_lcList.GetItemData(i));
		}//if
	}//for

	if(m_arySelectedNetInfo.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_NETWORKHOSTDLG_MSG001), MB_ICONWARNING);
		return;
	}//if

	OnOK();
}

void CNetworkHostDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ClearNetDeviceArray();

	OnCancel();
}

void CNetworkHostDlg::LoadNetDeviceInfo()
{
	ClearNetDeviceArray();

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString strInfoPath = szDrive;
	strInfoPath.Append("\\");
	strInfoPath.Append(szPath);
	strInfoPath.Append("\\data\\");
	strInfoPath.Append(ENVIROMENT_INI);

	char buf[1024];

	::GetPrivateProfileString("NetworkDevice", "DeviceNameList", "", buf, 1024, strInfoPath);
	CString strNameList = buf;
	//strNameList.Replace(" ", "");

	int nPos = 0;
	CString strTok = strNameList.Tokenize(",", nPos);
	while(strTok != "")
	{
		CNetDeviceInfo* pclsDevInfo = new CNetDeviceInfo();

		strTok.Trim();
		pclsDevInfo->m_strDeviceName = strTok;
		
		::GetPrivateProfileString(strTok, "IP", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strIP = buf;
		
//		::GetPrivateProfileString(strTok, "FTPPort", "", buf, 1024, strInfoPath);
//		pclsDevInfo->m_nFTPPort = atoi(buf);
		pclsDevInfo->m_nFTPPort = FT_PORT;
		
		::GetPrivateProfileString(strTok, "SvrPort", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_nSvrPort = atoi(buf);
		
		::GetPrivateProfileString(strTok, "ID", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strID = buf;
		
		::GetPrivateProfileString(strTok, "PWD", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strPWD = buf;
		
		::GetPrivateProfileString(strTok, "SHUTDOWNTIME", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strShutdownTime = buf;
		
		::GetPrivateProfileString(strTok, "USERCREATE", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strUserCreate = buf;

		::GetPrivateProfileString(strTok, "MAC", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strMacAddr = buf;

		::GetPrivateProfileString(strTok, "Alive", "true", buf, 1024, strInfoPath);
		pclsDevInfo->m_strAlive = buf;

		pclsDevInfo->m_nMonitorCnt = ::GetPrivateProfileInt(strTok, "MonitorCount", 1, strInfoPath);

		::GetPrivateProfileString(strTok, "Description", "", buf, 1024, strInfoPath);
		pclsDevInfo->m_strDescript = buf;

		m_aryNetDevice.Add(pclsDevInfo);
		strTok = strNameList.Tokenize(",", nPos);
	}//while
}


void CNetworkHostDlg::ClearNetDeviceArray()
{
	CNetDeviceInfo* pclsDevInfo = NULL;
	for(int i=0; i<m_aryNetDevice.GetCount(); i++)
	{
		pclsDevInfo = (CNetDeviceInfo*)m_aryNetDevice.GetAt(i);
		delete pclsDevInfo;
	}//for
	m_aryNetDevice.RemoveAll();
}

BOOL CNetworkHostDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_btnOK.LoadBitmap(IDB_BTN_APPLY, RGB(255, 255, 255));
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_btnRefresh.LoadBitmap(IDB_BTN_REFRESH, RGB(255, 255, 255));
	m_btnEdit.LoadBitmap(IDB_BTN_EDIT_NETWORK, RGB(255, 255, 255));

	m_btnOK.SetToolTipText(LoadStringById(IDS_NETWORKHOSTDLG_BTN001));
	m_btnCancel.SetToolTipText(LoadStringById(IDS_NETWORKHOSTDLG_BTN002));
	m_btnRefresh.SetToolTipText(LoadStringById(IDS_NETWORKHOSTDLG_BTN003));
	m_btnEdit.SetToolTipText(LoadStringById(IDS_NETWORKHOSTDLG_BTN004));

	CBitmap bmp;
	bmp.LoadBitmap(IDB_CHECK);
	m_imgCheck.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1);
	m_imgCheck.Add(&bmp, RGB(255,255,255));

	m_lcList.SetImageList(&m_imgCheck, LVSIL_SMALL);
//	m_lcList.SetExtendedStyle(m_listHost.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
//	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
//	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);

	CRect clsRect;
	m_lcList.GetClientRect(&clsRect);
	int nWidth = clsRect.Width();

	m_lcList.InsertColumn(eCheck     , _T("")               , LVCFMT_LEFT  , 22                  );
	m_lcList.InsertColumn(E_HOST     , LoadStringById(IDS_NETWORKHOSTDLG_LST001), LVCFMT_LEFT  , (int)(nWidth*2.2/10));
	m_lcList.InsertColumn(E_DESCRIPT , LoadStringById(IDS_NETWORKHOSTDLG_LST009), LVCFMT_CENTER, (int)(nWidth*2.2/10));
	m_lcList.InsertColumn(E_IPADDR   , LoadStringById(IDS_NETWORKHOSTDLG_LST002), LVCFMT_CENTER, (int)(nWidth*  2/10));
	m_lcList.InsertColumn(E_DISPLYCNT, LoadStringById(IDS_NETWORKHOSTDLG_LST007), LVCFMT_CENTER, (int)(nWidth*1.2/10));
	m_lcList.InsertColumn(E_FTPPORT  , LoadStringById(IDS_NETWORKHOSTDLG_LST003), LVCFMT_RIGHT , (int)(nWidth*1.2/10));
	m_lcList.InsertColumn(E_SVRPORT  , LoadStringById(IDS_NETWORKHOSTDLG_LST004), LVCFMT_RIGHT , (int)(nWidth*1.2/10));
	m_lcList.InsertColumn(E_SDTIME   , LoadStringById(IDS_NETWORKHOSTDLG_LST005), LVCFMT_RIGHT , (int)(nWidth*  2/10));
	m_lcList.InsertColumn(E_MACADDR  , LoadStringById(IDS_NETWORKHOSTDLG_LST006), LVCFMT_CENTER, (int)(nWidth*1.2/10));
	m_lcList.InsertColumn(E_USRCREATE, LoadStringById(IDS_NETWORKHOSTDLG_LST008), LVCFMT_CENTER, (int)(nWidth*1.2/10));

	m_lcList.InitHeader(IDB_LIST_HEADER);

	//ScanNetHost();
	LoadNetDeviceInfo();
	InitNetListCtrl();

	//드라이브명으로 정렬 시킨다
	//m_lcList.SetSortHeader(E_HOST);
	//m_lcList.SortItems(CompareList, (DWORD_PTR)this);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CNetworkHostDlg::InitNetListCtrl()
{
	m_lcList.DeleteAllItems();
	
	if(m_aryNetDevice.GetCount() == 0)
	{
		//UbcMessageBox("Can't found any network host !!!", MB_ICONWARNING);
		//CDialog::OnCancel();
		return;
	}//if

	int nRow = 0;
	CString szBuff;
	CNetDeviceInfo* pclsNetInfo = NULL;
	for(int i=0; i<m_aryNetDevice.GetCount(); i++)
	{
		pclsNetInfo = (CNetDeviceInfo*)m_aryNetDevice.GetAt(i);

		m_lcList.InsertItem(i, "", -1);

		m_lcList.SetItem(i, E_HOST, LVIF_IMAGE|LVIF_TEXT, pclsNetInfo->m_strDeviceName
						, (pclsNetInfo->m_strAlive.Compare("true") ? 5 : 4)
						, 0, 0, NULL);

		m_lcList.SetItemText(nRow, E_IPADDR, pclsNetInfo->m_strIP);
		
		szBuff.Format("%d", pclsNetInfo->m_nFTPPort);
		m_lcList.SetItemText(nRow, E_FTPPORT, szBuff);
		
		szBuff.Format("%d", pclsNetInfo->m_nSvrPort);
		m_lcList.SetItemText(nRow, E_SVRPORT, szBuff);
		
		m_lcList.SetItemText(nRow, E_SDTIME, pclsNetInfo->m_strShutdownTime);
		m_lcList.SetItemText(nRow, E_MACADDR, pclsNetInfo->m_strMacAddr);

		szBuff.Format("%d", pclsNetInfo->m_nMonitorCnt);
		m_lcList.SetItemText(nRow, E_DISPLYCNT, szBuff);
		m_lcList.SetItemText(nRow, E_USRCREATE, pclsNetInfo->m_strUserCreate);

		m_lcList.SetItemText(nRow, E_DESCRIPT, pclsNetInfo->m_strDescript);
		
		m_lcList.SetItemData(nRow, (DWORD)pclsNetInfo);
		nRow++;
	}//for
}

void CNetworkHostDlg::OnNMClickNetworkHostList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	/*
	*pResult = 0;
	NMLISTVIEW* pstNMListView = (NMLISTVIEW*)pNMHDR;
	int nItem = pstNMListView->iItem;

	if(m_lcList.GetCheck(nItem))
	{
		m_lcList.SetCheck(nItem, FALSE);
	}
	else
	{
		m_lcList.SetCheck(nItem, TRUE);
	}//if
	*/

/*
	LVITEM item;

	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		item.iItem = i;
		item.iSubItem = 0;
		item.mask = LVIF_IMAGE;
		item.iImage = 0;

		m_lcList.SetItem(&item);
	}//for

	POSITION pos = m_lcList.GetFirstSelectedItemPosition();
	while(pos)
	{
		int nItem = m_lcList.GetNextSelectedItem(pos);
		item.iItem = nItem;
		item.iSubItem = 0;
		item.mask = LVIF_IMAGE;
		item.iImage = 1;

		m_lcList.SetItem(&item);
	}//while
*/
}

void CNetworkHostDlg::OnNMDblclkNetworkHostList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

//	OnBnClickedOk();
}

void CNetworkHostDlg::ScanNetHost(int nType)
{
	CWaitMessageBox wait;

	if(nType == 1)
	{
		CScanNetHost scan;
		scan.AutoSearch();
	}
	else
	{
		scratchUtil* aUtil = scratchUtil::getInstance();
		aUtil->autoSearch(14008);
		//BeginWaitCursor();
		//scratchUtil* aUtil = scratchUtil::getInstance();
		//if(!aUtil->autoSearch(14008))
		//{
		//}//if
		//EndWaitCursor();
	}
}

void CNetworkHostDlg::OnBnClickedRefreshBtn()
{
	if( UbcMessageBox(LoadStringById(IDS_NETWORKHOSTDLG_MSG002), MB_YESNO) != IDYES ) return;

	UpdateData(TRUE);

	ScanNetHost(m_nFindHostType);

	LoadNetDeviceInfo();
	InitNetListCtrl();
}

void CNetworkHostDlg::OnBnClickedEditBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CNetworkHostManageDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
		LoadNetDeviceInfo();
		InitNetListCtrl();
	}//if
}

void CNetworkHostDlg::OnLvnColumnclickHostList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int	nColumn = pNMLV->iSubItem;

	if(nColumn == eCheck)
	{
		BOOL bCheck = !m_lcList.GetCheckHdrState();
		for(int nRow=0; nRow<m_lcList.GetItemCount() ;nRow++)
		{
			m_lcList.SetCheck(nRow, bCheck);
		}

		m_lcList.SetCheckHdrState(bCheck);
	}
	else
	{
		m_lcList.SetSortHeader(nColumn);
		m_lcList.SortItems(CompareList, (DWORD_PTR)this);
	}
}

int CNetworkHostDlg::CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	return ((CNetworkHostDlg*)lParam)->m_lcList.Compare(lParam1, lParam2);
}
