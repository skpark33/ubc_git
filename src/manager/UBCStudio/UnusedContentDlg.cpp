// UnusedContentDlg.cpp : implementation file
//
#include "stdafx.h"
#include "UnusedContentDlg.h"
#include "DataContainer.h"
#include "Enviroment.h"
#include "Schedule.h"

#define STR_TYPE	_T("Type")
#define STR_NAME	_T("Name")
#define STR_TIME	_T("Runtime")
#define STR_FILE	_T("File name")

// CUnusedContentDlg dialog
IMPLEMENT_DYNAMIC(CUnusedContentDlg, CDialog)

CUnusedContentDlg::CUnusedContentDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUnusedContentDlg::IDD, pParent)
{
}

CUnusedContentDlg::~CUnusedContentDlg()
{
}

void CUnusedContentDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CONTENTS, m_lscContents);
}

BEGIN_MESSAGE_MAP(CUnusedContentDlg, CDialog)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_CONTENTS, OnLvnColumnclickList)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_CONTENTS, OnNMCustomdrawList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_CONTENTS, OnNMClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONTENTS, OnNMDblclkList)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, &CUnusedContentDlg::OnBnClickedButtonDelete)
END_MESSAGE_MAP()

// CUnusedContentDlg message handlers
BOOL CUnusedContentDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitList();
	RefreshContents();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CUnusedContentDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CUnusedContentDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnClose();
}

void CUnusedContentDlg::InitList()
{
	m_szColTitle[eCheck] = _T("");
	m_szColTitle[eType] = STR_TYPE;
	m_szColTitle[eName] = STR_NAME;
	m_szColTitle[eTime] = STR_TIME;
	m_szColTitle[eFile] = STR_FILE;

	m_lscContents.SetExtendedStyle(m_lscContents.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES);

	CBitmap bmp1;
	bmp1.LoadBitmap(IDB_CONTENTS_LIST16);

	CBitmap bmp2;
	bmp2.LoadBitmap(IDB_CONTENTS_LIST16_ERROR);

	CBitmap bmp3;
	bmp3.LoadBitmap(IDB_CONTENTS_LIST16_SERVER);

	m_ilHostList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);

	m_ilHostList.Add(&bmp1, RGB(255, 255, 255));
	m_ilHostList.Add(&bmp2, RGB(255, 255, 255));
	m_ilHostList.Add(&bmp3, RGB(255, 255, 255));

	m_lscContents.SetImageList(&m_ilHostList, LVSIL_SMALL);

	for(int nCol = 0; nCol < eColEnd; nCol++)
	{
		m_lscContents.InsertColumn(nCol, m_szColTitle[nCol], LVCFMT_LEFT, 100);
	}

	m_lscContents.SetColumnWidth(eCheck, 22); // 2010.02.17 by gwangsoo

	m_lscContents.InitHeader(IDB_LIST_HEADER);
}

void CUnusedContentDlg::RefreshContents()
{
	CONTENTS_INFO_MAP* pmapInfo = CDataContainer::getInstance()->GetContentsMap();
	POSITION pos = pmapInfo->GetStartPosition();
	POSITION posOld = 0;

	m_lscContents.DeleteAllItems();

	int nRow = 0;
	while(pos)
	{
		CString szKey;
		CONTENTS_INFO* pInfo = NULL;
	
		posOld = pos;
		pmapInfo->GetNextAssoc(pos, szKey, (void*&)pInfo);
		if(!pInfo)	continue;

		if(CDataContainer::getInstance()->IsExistPlayContentsUsingContents(pInfo->strId)) continue;

		m_lscContents.InsertItem(nRow, "", -1);
		int nImageIdx = pInfo->nContentsType+1;

		if(!pInfo->bLocalFileExist)
		{
			if( GetEnvPtr()->m_Edition == CEnviroment::eStudioEE &&
			   !GetEnvPtr()->m_PackageInfo.szProcID.IsEmpty()   &&
				pInfo->bServerFileExist                          )
			{
				nImageIdx = pInfo->nContentsType + 1 + NUM_ENV_SERVER;
			}
			else
			{
				nImageIdx = pInfo->nContentsType + 1 + NUM_ENV_INEXISTENT;
			}
		}

		m_lscContents.SetItemText(nRow, eType, CONTENTS_TYPE_TO_STRING[pInfo->nContentsType + 1]);
		LVITEM itemType;
		itemType.iItem = nRow;
		itemType.iSubItem = eType;
		itemType.mask = LVIF_IMAGE;
		itemType.iImage = nImageIdx;
		m_lscContents.SetItem(&itemType);

		m_lscContents.SetItemText(nRow, eName, pInfo->strContentsName);
		m_lscContents.SetItemText(nRow, eFile, pInfo->strFilename);

		if(CONTENTS_PPT == pInfo->nContentsType)
		{
			m_lscContents.SetItemText(nRow, eTime, "");
		}
		else
		{
			CString tm;
			tm.Format("%02ld:%02ld", pInfo->nRunningTime/60, pInfo->nRunningTime % 60);
			m_lscContents.SetItemText(nRow, eTime, tm);
		}

		m_lscContents.SetItemData(nRow++, (DWORD_PTR)posOld);
		TraceLog((szKey));
	}
}

int CUnusedContentDlg::Compare(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	return ((CUnusedContentDlg*)lParam)->m_lscContents.Compare(lParam1, lParam2);
}

void CUnusedContentDlg::OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int	nColumn = pNMLV->iSubItem;
	
	// 2010.02.17 by gwangsoo
	if(nColumn == m_lscContents.GetColPos(m_szColTitle[eCheck]))
	{
		bool bCheck = !m_lscContents.GetCheckHdrState();
		for(int nRow=0; nRow<m_lscContents.GetItemCount() ;nRow++)
		{
			m_lscContents.SetCheck(nRow, bCheck);
		}
		m_lscContents.SetCheckHdrState(bCheck);
	}
	else
	{
		m_lscContents.SetSortHeader(nColumn);
		m_lscContents.SortItems(Compare, (DWORD_PTR)this);
	}
}

void CUnusedContentDlg::OnNMCustomdrawList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
}

void CUnusedContentDlg::OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
}

void CUnusedContentDlg::OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
}

void CUnusedContentDlg::OnBnClickedButtonDelete()
{
	int nCnt = 0;
	CString szBuf;

	CStringList slConId;
	CONTENTS_INFO_MAP* pmapInfo = CDataContainer::getInstance()->GetContentsMap();

	for(int nRow = m_lscContents.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscContents.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lscContents.GetItemData(nRow);
		if(!pos) continue;

		CString szKey;
		CONTENTS_INFO* pInfo = NULL;
		pmapInfo->GetNextAssoc(pos, szKey, (void*&)pInfo);
		if(!pInfo) continue;

		slConId.AddTail(pInfo->strId);
	}

	if(slConId.GetCount() == 0) return;

	POSITION pos = slConId.GetHeadPosition();
	while(pos)
	{
		CString szId = slConId.GetNext(pos);
		if(szId.IsEmpty()) continue;

		CDataContainer::getInstance()->DeleteContents(szId);
	}

	RefreshContents();
	AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED, (WPARAM)slConId.GetCount());
}
