// LayoutView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "UBCStudioDoc.h"
#include "LayoutView.h"
#include "MainFrm.h"
#include "LayoutHistory.h"
#include "Enviroment.h"

#ifdef _UBCSTUDIO_EE_
#include "PublicTemplates/PublicTemplatesProcessingDlg.h"
#include "PublicTemplates/SelectPublicTemplateDialog.h"
#endif

// CLayoutView
IMPLEMENT_DYNCREATE(CLayoutView, CFormView)

CLayoutView::CLayoutView()
:	CFormView(CLayoutView::IDD)
,	m_reposControl (this)
,	m_wndTemplate (0, CTemplate_Wnd::TEMPLATE_WND_LAYOUT, true)
,	m_wndFrame(1, CFrame_Wnd::WND_LAYOUT, true)
{
	m_bCtrl = FALSE;
}

CLayoutView::~CLayoutView()
{
}

void CLayoutView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TEMPLATE, m_frameTemplate);
	DDX_Control(pDX, IDC_BUTTON_CREATE_TEMPLATE, m_btnCreateTemplate);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_TEMPLATE, m_btnRemoveTemplate);
	DDX_Control(pDX, IDC_BUTTON_TEMPLATE_FROM_SERVER, m_btnCreateFromServer);
	DDX_Control(pDX, IDC_BUTTON_TEMPLATE_TO_SERVER, m_btnSaveToServer);
	DDX_Control(pDX, IDC_STATIC_FRAME, m_frameFrame);
	DDX_Control(pDX, IDC_TAB_PROPERTY, m_tabProperty);
	DDX_Control(pDX, IDC_STATIC_FRAME_PROPERTY, m_frameFrameProperty);
	DDX_Control(pDX, IDC_BUTTON_CREATE_FRAME, m_btnCreateFrame);
	DDX_Control(pDX, IDC_BUTTON_COPY_FRAME, m_btnCopyFrame);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_FRAME, m_btnRemoveFrame);
	DDX_Control(pDX, IDC_BUTTON_MOVE_PREV_FRAME, m_btnMovePrevFrame);
	DDX_Control(pDX, IDC_BUTTON_MOVE_NEXT_FRAME, m_btnMoveNextFrame);
	DDX_Control(pDX, IDC_BUTTON_FRAME_UP, m_btnFramUp);
	DDX_Control(pDX, IDC_BUTTON_FRAME_DN, m_btnFramDn);
	DDX_Control(pDX, IDC_BUTTON_ALIGN_LEFT, m_btnFramAlignLeft);
	DDX_Control(pDX, IDC_BUTTON_ALIGN_RIGHT, m_btnFramAlignRight);
	DDX_Control(pDX, IDC_BUTTON_ALIGN_TOP, m_btnFramAlignTop);
	DDX_Control(pDX, IDC_BUTTON_ALIGN_BOTTOM, m_btnFramAlignBottom);
	DDX_Control(pDX, IDC_BUTTON_ALIGN_VCENTER, m_btnFramAlignVCenter);
	DDX_Control(pDX, IDC_BUTTON_ALIGN_HCENTER, m_btnFramAlignHCenter);
	DDX_Control(pDX, IDC_SLIDER_MAGNETIC, m_sliderMagnetic);
	DDX_Control(pDX, IDC_TXT_MAGNETIC, m_txtMagnetic);
	DDX_Control(pDX, IDC_BUTTON_RETURN, m_btReturn);
}

BEGIN_MESSAGE_MAP(CLayoutView, CFormView)
	ON_WM_SIZE()
	ON_MESSAGE(WM_TEMPLATE_FRAME_SELECT_CHANGED, OnTemplateFrameSelectChanged)
	ON_MESSAGE(WM_TEMPLATE_INFO_CHANGED, OnTemplateInfoChanged)
	ON_MESSAGE(WM_FRAME_INFO_CHANGED, OnFrameInfoChanged)
	ON_MESSAGE(WM_TEMPLATE_LIST_CHANGED, OnTemplateListChanged)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_PROPERTY, &CLayoutView::OnTcnSelchangeTabProperty)
	ON_BN_CLICKED(IDC_BUTTON_CREATE_TEMPLATE, &CLayoutView::OnBnClickedButtonCreateTemplate)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_TEMPLATE, &CLayoutView::OnBnClickedButtonRemoveTemplate)
	ON_BN_CLICKED(IDC_BUTTON_CREATE_FRAME, &CLayoutView::OnBnClickedButtonCreateFrame)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_FRAME, &CLayoutView::OnBnClickedButtonRemoveFrame)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_PREV_FRAME, &CLayoutView::OnBnClickedButtonMovePrevFrame)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_NEXT_FRAME, &CLayoutView::OnBnClickedButtonMoveNextFrame)
	ON_BN_CLICKED(IDC_BUTTON_FRAME_UP, &CLayoutView::OnBnClickedButtonFrameUp)
	ON_BN_CLICKED(IDC_BUTTON_FRAME_DN, &CLayoutView::OnBnClickedButtonFrameDn)
	ON_MESSAGE(WM_SAVE_CONFIG_COMPELETE, OnConfigSaveComplete)
	ON_WM_KEYDOWN()
	ON_BN_CLICKED(IDC_BUTTON_ALIGN_LEFT, &CLayoutView::OnBnClickedButtonAlignLeft)
	ON_BN_CLICKED(IDC_BUTTON_ALIGN_RIGHT, &CLayoutView::OnBnClickedButtonAlignRight)
	ON_BN_CLICKED(IDC_BUTTON_ALIGN_TOP, &CLayoutView::OnBnClickedButtonAlignTop)
	ON_BN_CLICKED(IDC_BUTTON_ALIGN_BOTTOM, &CLayoutView::OnBnClickedButtonAlignBottom)
	ON_BN_CLICKED(IDC_BUTTON_ALIGN_VCENTER, &CLayoutView::OnBnClickedButtonAlignVcenter)
	ON_BN_CLICKED(IDC_BUTTON_ALIGN_HCENTER, &CLayoutView::OnBnClickedButtonAlignHcenter)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_MAGNETIC, &CLayoutView::OnNMCustomdrawSliderMagnetic)
	ON_BN_CLICKED(IDC_BUTTON_COPY_FRAME, &CLayoutView::OnBnClickedButtonCopyFrame)
	ON_BN_CLICKED(IDC_BUTTON_TEMPLATE_FROM_SERVER, &CLayoutView::OnBnClickedButtonTemplateFromServer)
	ON_BN_CLICKED(IDC_BUTTON_TEMPLATE_TO_SERVER, &CLayoutView::OnBnClickedButtonTemplateToServer)
	ON_BN_CLICKED(IDC_BUTTON_RETURN, &CLayoutView::OnBnClickedButtonReturn)
END_MESSAGE_MAP()

// CLayoutView 진단입니다.
#ifdef _DEBUG
void CLayoutView::AssertValid() const
{
	CFormView::AssertValid();
}

CUBCStudioDoc* CLayoutView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCStudioDoc)));
	return (CUBCStudioDoc*)m_pDocument;
}

#ifndef _WIN32_WCE
void CLayoutView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CLayoutView 메시지 처리기입니다.

void CLayoutView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnCreateTemplate.LoadBitmap(IDB_BTN_PLUS, RGB(255, 255, 255));
	m_btnRemoveTemplate.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnCreateFromServer.LoadBitmap(IDB_BTN_LAYOUT_DOWN, RGB(255, 255, 255));
	m_btnSaveToServer.LoadBitmap(IDB_BTN_LAYOUT_UP, RGB(255, 255, 255));
	m_btnCreateFrame.LoadBitmap(IDB_BTN_PLUS, RGB(255, 255, 255));
	m_btnCopyFrame.LoadBitmap(IDB_BTN_COPY, RGB(255, 255, 255));
	m_btnRemoveFrame.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_btnMovePrevFrame.LoadBitmap(IDB_BTN_COLUMNS_PREVIOUS, RGB(255, 255, 255));
	m_btnMoveNextFrame.LoadBitmap(IDB_BTN_COLUMNS_NEXT, RGB(255, 255, 255));

	m_btnCreateTemplate.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT001));
	m_btnRemoveTemplate.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT002));
	m_btnCreateFromServer.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT016));
	m_btnSaveToServer.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT017));
	m_btnCreateFrame.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT006));
	m_btnCopyFrame.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT015));
	m_btnRemoveFrame.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT003));
	m_btnMovePrevFrame.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT004));
	m_btnMoveNextFrame.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT005));

	m_btnFramUp.LoadBitmap(IDB_BTN_FRAMEUP, RGB(255, 255, 255));
	m_btnFramDn.LoadBitmap(IDB_BTN_FRAMEDN, RGB(255, 255, 255));
	m_btnFramUp.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT007));
	m_btnFramDn.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT008));
//	m_btnFramUp.EnableWindow(FALSE);
//	m_btnFramDn.EnableWindow(FALSE);

	m_btnFramAlignLeft   .LoadBitmap(IDB_BTN_ALIGN_LEFT   , RGB(255, 255, 255));
	m_btnFramAlignRight  .LoadBitmap(IDB_BTN_ALIGN_RIGHT  , RGB(255, 255, 255));
	m_btnFramAlignTop    .LoadBitmap(IDB_BTN_ALIGN_TOP    , RGB(255, 255, 255));
	m_btnFramAlignBottom .LoadBitmap(IDB_BTN_ALIGN_BOTTOM , RGB(255, 255, 255));
	m_btnFramAlignVCenter.LoadBitmap(IDB_BTN_ALIGN_VCENTER, RGB(255, 255, 255));
	m_btnFramAlignHCenter.LoadBitmap(IDB_BTN_ALIGN_HCENTER, RGB(255, 255, 255));

	m_btnFramAlignLeft   .SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT009));
	m_btnFramAlignRight  .SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT010));
	m_btnFramAlignTop    .SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT011));
	m_btnFramAlignBottom .SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT012));
	m_btnFramAlignVCenter.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT013));
	m_btnFramAlignHCenter.SetToolTipText(LoadStringById(IDS_LAYOUTVIEW_BUT014));

	m_sliderMagnetic.SetRange(0, 20);
	m_sliderMagnetic.SetPos(5);

	m_tabProperty.InitImageListHighColor(IDB_TAB_LAYOUT_LIST, RGB(192,192,192));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

//	ResizeParentToFit();
	{
		// determine current size of the client area as if no scrollbars present
		CRect rectClient;
		GetWindowRect(rectClient);
		CRect rect = rectClient;
		CalcWindowRect(rect);
		rectClient.left += rectClient.left - rect.left;
		rectClient.top += rectClient.top - rect.top;
		rectClient.right -= rect.right - rectClient.right;
		rectClient.bottom -= rect.bottom - rectClient.bottom;
		rectClient.OffsetRect(-rectClient.left, -rectClient.top);
		ASSERT(rectClient.left == 0 && rectClient.top == 0);

		// determine desired size of the view
		CRect rectView(0, 0, m_totalDev.cx, m_totalDev.cy);
		{
			if (rectClient.right <= m_totalDev.cx)
				rectView.right = rectClient.right;
			if (rectClient.bottom <= m_totalDev.cy)
				rectView.bottom = rectClient.bottom;
		}
		CalcWindowRect(rectView, CWnd::adjustOutside);
		rectView.OffsetRect(-rectView.left, -rectView.top);
		ASSERT(rectView.left == 0 && rectView.top == 0);
		{
			if (rectClient.right <= m_totalDev.cx)
				rectView.right = rectClient.right;
			if (rectClient.bottom <= m_totalDev.cy)
				rectView.bottom = rectClient.bottom;
		}

		rectView.DeflateRect(::GetSystemMetrics(SM_CXDLGFRAME)-1, ::GetSystemMetrics(SM_CYDLGFRAME)-1);

		m_reposControl.SetParentRect(CRect(0,0,rectView.Width(), rectView.Height()));

		SetScrollSizes(MM_TEXT, CSize(1,1));
	}

	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//
	m_tabProperty.InsertItem(0, LoadStringById(IDS_LAYOUTVIEW_LST001), 0);
	m_tabProperty.InsertItem(1, LoadStringById(IDS_LAYOUTVIEW_LST002), 1);

	CRect rect;
	//
	m_frameTemplate.GetWindowRect(rect);
	ScreenToClient(rect);

	m_wndTemplate.SetFocusWindow(&m_btnCreateTemplate);
	m_wndTemplate.SetDocument(GetDocument());
	m_wndTemplate.CreateEx(WS_EX_CLIENTEDGE, NULL, "Template", WS_CHILD | WS_VISIBLE | WS_VSCROLL, rect, this, 0xfeff);

	//
	m_frameFrame.GetWindowRect(rect);
	ScreenToClient(rect);

	m_wndFrame.SetDocument(GetDocument());
	m_wndFrame.CreateEx(WS_EX_CLIENTEDGE, NULL, "Frame", WS_CHILD | WS_VISIBLE, rect, this, 0xfefe);

	//
	m_frameFrameProperty.GetWindowRect(rect);
	ScreenToClient(rect);

	m_dlgTemplateProperty.Create(IDD_TEMPLATE_PROPERTY, this);
	m_dlgTemplateProperty.MoveWindow(rect);
	m_dlgTemplateProperty.ShowWindow(SW_SHOW);

	m_dlgFrameProperty.Create(IDD_FRAME_PROPERTY, this);
	m_dlgFrameProperty.MoveWindow(rect);
	m_dlgFrameProperty.ShowWindow(SW_HIDE);

	m_reposControl.AddControl(&m_wndTemplate, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnCreateTemplate, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnRemoveTemplate, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnCreateFromServer, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnSaveToServer, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);

	m_reposControl.AddControl(&m_wndFrame, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.AddControl(&m_tabProperty, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_dlgTemplateProperty, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_dlgFrameProperty, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnCreateFrame, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnCopyFrame, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnRemoveFrame, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnMovePrevFrame, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnMoveNextFrame, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnFramUp, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnFramDn, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnFramAlignLeft   , REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnFramAlignRight  , REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnFramAlignTop    , REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnFramAlignBottom , REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnFramAlignVCenter, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btnFramAlignHCenter, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_txtMagnetic, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_sliderMagnetic, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_reposControl.AddControl(&m_btReturn, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);

	m_reposControl.Move();

	CMainFrame* pFrm = (CMainFrame*)AfxGetMainWnd();
	CString strHostName = pFrm->GetHostName();
	strHostName.Replace(SAMPLE_FILE_KEY, "");
	
	// 2010.10.06 스튜디오의 타이틀에 콘텐츠 패키지경로 보여주기
	CString strDrive = pFrm->GetSourceDirive();
	strDrive.MakeUpper();

	CString strTitle;
	if(strHostName == "")
	{
		strTitle = LoadStringById(IDS_LAYOUTVIEW_MSG010);
	}
	else
	{
		strTitle.Format(LoadStringById(IDS_LAYOUTVIEW_MSG011), strDrive, strHostName);

		// <!-- 타이틀바 뒤에 (공개/비공개) 추가
		strTitle += " - (";
		strTitle += ( GetEnvPtr()->m_PackageInfo.bIsPublic ? LoadStringById(IDS_EEPACKAGEDLG_STR001) : LoadStringById(IDS_EEPACKAGEDLG_STR002) );
		strTitle += ")";
		// -->
	}//if

	GetDocument()->SetTitle(strTitle);

	if(CEnviroment::eStudioPE == GetEnvPtr()->m_Edition)
	{
		m_btnCreateFromServer.ShowWindow(SW_HIDE);
		m_btnCreateFromServer.EnableWindow(FALSE);
		m_btnSaveToServer.ShowWindow(SW_HIDE);
		m_btnSaveToServer.EnableWindow(FALSE);
	}
}

BOOL CLayoutView::PreTranslateMessage(MSG* pMsg)
{
	switch(pMsg->message)
	{
	case WM_KEYDOWN:
		switch(pMsg->wParam)
		{
		case VK_CONTROL:
			m_bCtrl = TRUE;
			break;
		case 'z':
		case 'Z':
			TRACE(">>> WM_KEYDOWN(%d %d)\n", pMsg->wParam, m_bCtrl);
			break;
		case 'y':
		case 'Y':
			TRACE(">>> WM_KEYDOWN(%d %d)\n", pMsg->wParam, m_bCtrl);
			break;
		}
		break;
	case WM_KEYUP:
		switch(pMsg->wParam)
		{
		case VK_CONTROL:
			m_bCtrl = FALSE;
			break;
		}
		break;
	}
	return CFormView::PreTranslateMessage(pMsg);
}

void CLayoutView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();
}

LRESULT CLayoutView::OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam)
{
	LPCTSTR strTemplateId = (LPCTSTR)wParam;
	LPCTSTR strFrameId = (LPCTSTR)lParam;

	TraceLog(("CLayoutView::OnTemplateFrameSelectChanged (%s)", strTemplateId));

	GetDocument()->SetSelectTemplateId(strTemplateId);
	GetDocument()->SetSelectFrameId(strFrameId);

	m_wndTemplate.SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, wParam, lParam);
	m_wndFrame.SendMessage(WM_TEMPLATE_FRAME_SELECT_CHANGED, wParam, lParam);

	TEMPLATE_LINK* template_link = GetDocument()->GetSelectTemplateLink();
	FRAME_LINK* frame_link = GetDocument()->GetSelectFrameLink();

	m_dlgTemplateProperty.SetTemplateInfo(template_link);
	m_dlgFrameProperty.SetFrameInfo(frame_link);

	LRESULT result;
	if(frame_link == NULL){
		m_tabProperty.SetCurSel(0);
		OnTcnSelchangeTabProperty(NULL, &result);
	}
	else{
		m_tabProperty.SetCurSel(1);
		OnTcnSelchangeTabProperty(NULL, &result);

		int nCnt = template_link->arFrameLinkList.GetCount();
		if(nCnt > 1){
			int nIdx = 0;
			for(nIdx = 0; nIdx < nCnt; nIdx++){
				FRAME_LINK& flk = template_link->arFrameLinkList.GetAt(nIdx);
				if(frame_link->pFrameInfo->strId == flk.pFrameInfo->strId)
					break;
			}

			//if(0 == nIdx){
			//	m_btnFramUp.EnableWindow(TRUE);
			//	m_btnFramDn.EnableWindow(FALSE);
			//}
			//else if(nIdx == (nCnt-1)){
			//	m_btnFramUp.EnableWindow(FALSE);
			//	m_btnFramDn.EnableWindow(TRUE);
			//}
			//else{
			//	m_btnFramUp.EnableWindow(TRUE);
			//	m_btnFramDn.EnableWindow(TRUE);
			//}
		}
		//else{
		//	m_btnFramUp.EnableWindow(FALSE);
		//	m_btnFramDn.EnableWindow(FALSE);
		//}
	}

	return 0;
}

LRESULT CLayoutView::OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam)
{
	if( m_dlgTemplateProperty.GetTemplateInfo(GetDocument()->GetSelectTemplateLink()) )
	{
		m_wndTemplate.RecalcLayout();
		m_wndTemplate.Invalidate(FALSE);

		m_wndFrame.RecalcLayout();
		m_wndFrame.Invalidate(FALSE);

		::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_INFO_CHANGED);
	}

	return 0;
}

LRESULT CLayoutView::OnFrameInfoChanged(WPARAM wParam, LPARAM lParam)
{
	FRAME_LINK* frame_link = GetDocument()->GetSelectFrameLink();
	if(wParam == 0){
		m_dlgFrameProperty.SetFrameInfo(frame_link, lParam);
	}

	if( m_dlgFrameProperty.GetFrameInfo(frame_link) )
	{
		////////////////////////////////////////////////////////
		// Modified by 정운형 2009-03-04 오후 5:18
		// 변경내역 :  Primary frame 설정 변경 수정
		TEMPLATE_LINK* pSelTempLink = GetDocument()->GetSelectTemplateLink();
		if(pSelTempLink == NULL)
		{
			return NULL;
		}

		int nCnt = 0;
		for(int i = 0; i < pSelTempLink->arFrameLinkList.GetCount(); i++)
		{
			FRAME_LINK FrmLink = (FRAME_LINK)pSelTempLink->arFrameLinkList.GetAt(i);
			if(FrmLink.pFrameInfo->nGrade == 1)
			{
				nCnt++;
			}
		}

		if(nCnt > 1)
		{
			// 만약 Frame 이 Capture 된 상태면 Capture 를 풀어줌. 
			m_wndFrame.ReleaseCapturedFrame();

			CString strMsg;
			strMsg.Format(LoadStringById(IDS_LAYOUTVIEW_MSG001), pSelTempLink->pTemplateInfo->strId);
			UbcMessageBox(strMsg, MB_ICONWARNING);

			frame_link->pFrameInfo->nGrade = 2;
			m_dlgFrameProperty.PostMessage(WM_PRIMARY_FRAME_OVERLAP, 0, 0);
		}
		// Modified by 정운형 2009-03-04 오후 5:18
		// 변경내역 :  Primary frame 설정 변경 수정
		////////////////////////////////////////////////////////
		m_wndTemplate.RecalcLayout();
		m_wndTemplate.Invalidate(FALSE);

		m_wndFrame.RecalcLayout();
		m_wndFrame.Invalidate(FALSE);

		::AfxGetMainWnd()->SendMessage(WM_FRAME_INFO_CHANGED);
	}

	return 0;
}

LRESULT CLayoutView::OnTemplateListChanged(WPARAM wParam, LPARAM lParam)
{
	m_wndTemplate.RecalcLayout();
	m_wndTemplate.ScrollToEnd();
	OnTemplateFrameSelectChanged((WPARAM)(LPCTSTR)"", (LPARAM)(LPCTSTR)"");

	return 0;
}

void CLayoutView::OnTcnSelchangeTabProperty(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int sel = m_tabProperty.GetCurSel();
	if(sel == 0)
	{
		m_dlgTemplateProperty.ShowWindow(SW_SHOW);
		m_dlgFrameProperty.ShowWindow(SW_HIDE);

		//m_btnFramUp.EnableWindow(FALSE);
		//m_btnFramDn.EnableWindow(FALSE);
	}
	else
	{
		m_dlgTemplateProperty.ShowWindow(SW_HIDE);
		m_dlgFrameProperty.ShowWindow(SW_SHOW);
	}
}

void CLayoutView::OnBnClickedButtonCreateTemplate()
{
	CUBCStudioDoc* pDoc = GetDocument();

	TEMPLATE_INFO* template_info = pDoc->CreateNewTemplate();

	if(template_info != NULL)
	{
		m_wndTemplate.RecalcLayout();
		//m_wndTemplate.ScrollToEnd();
		OnTemplateFrameSelectChanged((WPARAM)(LPCTSTR)template_info->strId, (LPARAM)(LPCTSTR)"");

		::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_INFO_CHANGED);

		// Modified by 정운형 2009-03-04 오후 6:24
		// 변경내역 :  Template 추가 기능 수정
		OnBnClickedButtonCreateFrame();
		// Modified by 정운형 2009-03-04 오후 6:24
		// 변경내역 :  Template 추가 기능 수정

		m_wndTemplate.EnsureVisible(template_info->strId);
	}
}

void CLayoutView::OnBnClickedButtonRemoveTemplate()
{
	CUBCStudioDoc* pDoc = GetDocument();
	TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
	if(pTempLink == NULL)
		return;

	TEMPLATE_LINK_LIST* pTempList = pDoc->GetAllTemplateList();
	if(!pTempList)	return;

	if(pTempList->GetCount() <= 1)
	{
		UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG002), MB_ICONWARNING);
		return;
	}

	TEMPLATE_LINK_LIST* pPlayTempList = pDoc->GetPlayTemplateList();
	if(!pPlayTempList)	return;

	//for(int i = 0; i < pPlayTempList->GetCount(); i++){
	//	if(pPlayTempList->GetAt(i).pTemplateInfo->strId == pTempLink->pTemplateInfo->strId){
	//		UbcMessageBox("The played template layout should not remove", MB_ICONWARNING);
	//		return;
	//	}
	//}

	if(UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG003), MB_ICONWARNING | MB_YESNO) == IDNO) return;

	bool exist_play_template = false;
	if( pDoc->IsExistPlayTemplateList(pDoc->GetSelectTemplateId()) )
	{
		exist_play_template = true;
		int ret_value = UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG004), MB_ICONWARNING | MB_YESNO);
		if(ret_value == IDNO)
			return;
	}

	if(pDoc->DeleteTemplate(pDoc->GetSelectTemplateId()))
	{
		OnTemplateFrameSelectChanged((WPARAM)(LPCTSTR)"", (LPARAM)(LPCTSTR)"");
		if(exist_play_template)
			::AfxGetMainWnd()->SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED);
		::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_INFO_CHANGED);
	}
}

void CLayoutView::OnBnClickedButtonCreateFrame()
{
	//USTB type 은 일반 frame 3개 pip frame 1개만 가능함
	if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
	{
		CUBCStudioDoc* pDoc = GetDocument();
		TEMPLATE_LINK* pTemplateLink = pDoc->GetSelectTemplateLink();
		if(!pTemplateLink) return;

		FRAME_LINK_LIST* pFrmList = &(pTemplateLink->arFrameLinkList);
		int count = pFrmList->GetCount();

		for(int i = 0; i < pFrmList->GetCount(); i++)
		{
			FRAME_INFO* pFrmInfo = pFrmList->GetAt(i).pFrameInfo;
			if(!pFrmInfo) continue;

			if(pFrmInfo->bIsPIP)
			{
				count--;
				break;
			}
		}

		//if(count >= 3){
		//	UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG005), MB_ICONERROR);
		//	return;
		//}
	}

	if(m_wndFrame.CreateNewFrame())
	{
		::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_INFO_CHANGED);
	}
}

void CLayoutView::OnBnClickedButtonRemoveFrame()
{
	CUBCStudioDoc* pDoc = GetDocument();
	FRAME_LINK* frame_link = pDoc->GetSelectFrameLink();
	if(frame_link == NULL)
		return;

	int ret_value = UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG006), MB_ICONWARNING | MB_YESNO);
	if(ret_value == IDNO)
		return;

	if( pDoc->IsExistContentsInFrame(pDoc->GetSelectTemplateId(), pDoc->GetSelectFrameId()) )
	{
		int ret_value = UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG007), MB_ICONWARNING | MB_YESNO);
		if(ret_value == IDNO)
			return;
	}

	if(m_wndFrame.DeleteFrame())
	{
		::AfxGetMainWnd()->SendMessage(WM_PLAY_TEMPLATE_LIST_CHANGED);
		::AfxGetMainWnd()->SendMessage(WM_FRAME_INFO_CHANGED);
	}
}

void CLayoutView::OnBnClickedButtonMovePrevFrame()
{
	m_wndFrame.MovePrevFrame();
}

void CLayoutView::OnBnClickedButtonMoveNextFrame()
{
	m_wndFrame.MoveNextFrame();
}

void CLayoutView::OnBnClickedButtonFrameUp()
{
	CUBCStudioDoc* pDoc = GetDocument();
	if(!pDoc)	return;

	TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
	if(!pTempLink)	return;

	FRAME_LINK* pFrmLink = pDoc->GetSelectFrameLink();
	if(!pFrmLink)	return;
	CString szFrameId = pFrmLink->pFrameInfo->strId;

	int nIdx = 0;
	int nCnt = pTempLink->arFrameLinkList.GetCount();
	for(nIdx = 0; nIdx < nCnt-1; nIdx++){
		FRAME_LINK& flk = pTempLink->arFrameLinkList.GetAt(nIdx);

		if(szFrameId == flk.pFrameInfo->strId){
			FRAME_LINK flk1 = pTempLink->arFrameLinkList.GetAt(nIdx);
			FRAME_LINK flk2 = pTempLink->arFrameLinkList.GetAt(nIdx+1);

			pTempLink->arFrameLinkList.SetAt(nIdx, flk2);
			pTempLink->arFrameLinkList.SetAt(nIdx+1, flk1);

			Invalidate();
			::AfxGetMainWnd()->SendMessage(WM_ALL_INFO_CHANGED);
			break;
		}
	}

	if(nIdx == nCnt-1){
		UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG008));
		return;
	}

	TEMPLATE_LINK_LIST* pPlayTemplatList = pDoc->GetPlayTemplateList();
	if(!pPlayTemplatList)	return;

	for(int i=0; i<pPlayTemplatList->GetCount(); i++)
	{
		TEMPLATE_LINK& PlayTempLink = pPlayTemplatList->GetAt(i);

		if(PlayTempLink.pTemplateInfo->strId == pTempLink->pTemplateInfo->strId)
		{
			for(int j=0; j < PlayTempLink.arFrameLinkList.GetCount()-1; j++){
				FRAME_LINK& flk = PlayTempLink.arFrameLinkList.GetAt(j);

				if(szFrameId == flk.pFrameInfo->strId){
					FRAME_LINK flk1 = PlayTempLink.arFrameLinkList.GetAt(j);
					FRAME_LINK flk2 = PlayTempLink.arFrameLinkList.GetAt(j+1);

					PlayTempLink.arFrameLinkList.SetAt(j, flk2);
					PlayTempLink.arFrameLinkList.SetAt(j+1, flk1);

					::AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED, 1);
					break;
				}
			}
		}
	}
}

void CLayoutView::OnBnClickedButtonFrameDn()
{
	CUBCStudioDoc* pDoc = GetDocument();
	if(!pDoc)	return;

	TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
	if(!pTempLink)	return;

	FRAME_LINK* pFrmLink = pDoc->GetSelectFrameLink();
	if(!pFrmLink)	return;
	CString szFrameId = pFrmLink->pFrameInfo->strId;

	int nIdx = 0;
	int nCnt = pTempLink->arFrameLinkList.GetCount();
	for(nIdx = 1; nIdx < nCnt; nIdx++){
		FRAME_LINK& flk = pTempLink->arFrameLinkList.GetAt(nIdx);

		if(szFrameId == flk.pFrameInfo->strId){
			FRAME_LINK flk1 = pTempLink->arFrameLinkList.GetAt(nIdx-1);
			FRAME_LINK flk2 = pTempLink->arFrameLinkList.GetAt(nIdx);

			pTempLink->arFrameLinkList.SetAt(nIdx-1, flk2);
			pTempLink->arFrameLinkList.SetAt(nIdx, flk1);

			Invalidate();
			::AfxGetMainWnd()->SendMessage(WM_ALL_INFO_CHANGED);
			break;
		}
	}

	if(nIdx == nCnt){
		UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG009));
		return;
	}

	TEMPLATE_LINK_LIST* pPlayTemplatList = pDoc->GetPlayTemplateList();
	if(!pPlayTemplatList)	return;

	for(int i=0; i<pPlayTemplatList->GetCount(); i++)
	{
		TEMPLATE_LINK& PlayTempLink = pPlayTemplatList->GetAt(i);

		if(PlayTempLink.pTemplateInfo->strId == pTempLink->pTemplateInfo->strId)
		{
			for(int j=1; j < PlayTempLink.arFrameLinkList.GetCount(); j++){
				FRAME_LINK& flk = PlayTempLink.arFrameLinkList.GetAt(j);

				if(szFrameId == flk.pFrameInfo->strId){
					FRAME_LINK flk1 = PlayTempLink.arFrameLinkList.GetAt(j-1);
					FRAME_LINK flk2 = PlayTempLink.arFrameLinkList.GetAt(j);

					PlayTempLink.arFrameLinkList.SetAt(j-1, flk2);
					PlayTempLink.arFrameLinkList.SetAt(j, flk1);

					::AfxGetMainWnd()->SendMessage(WM_CONTENTS_LIST_CHANGED, 1);
					break;
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// complete config save message handler\n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CLayoutView::OnConfigSaveComplete(WPARAM wParam, LPARAM lParam)
{
	CMainFrame* pFrm = (CMainFrame*)AfxGetMainWnd();
	CString strHostName = pFrm->GetHostName();
	strHostName.Replace(SAMPLE_FILE_KEY, "");
	
	// 2010.10.06 스튜디오의 타이틀에 콘텐츠 패키지경로 보여주기
	CString strDrive = pFrm->GetSourceDirive();
	strDrive.MakeUpper();

	CString strTitle;
	if(strHostName == "")
	{
		strTitle = LoadStringById(IDS_LAYOUTVIEW_MSG010);
	}
	else
	{
		strTitle.Format(LoadStringById(IDS_LAYOUTVIEW_MSG011), strDrive, strHostName);

		// <!-- 타이틀바 뒤에 (공개/비공개) 추가
		strTitle += " - (";
		strTitle += ( GetEnvPtr()->m_PackageInfo.bIsPublic ? LoadStringById(IDS_EEPACKAGEDLG_STR001) : LoadStringById(IDS_EEPACKAGEDLG_STR002) );
		strTitle += ")";
		// -->
	}//if

	GetDocument()->SetTitle(strTitle);

	return 0;
}

void CLayoutView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CFormView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CLayoutView::OnBnClickedButtonAlignLeft()
{
	m_wndFrame.AlignToFrame(CFrame_Wnd::ALIGN_LEFT);
}

void CLayoutView::OnBnClickedButtonAlignRight()
{
	m_wndFrame.AlignToFrame(CFrame_Wnd::ALIGN_RIGHT);
}

void CLayoutView::OnBnClickedButtonAlignTop()
{
	m_wndFrame.AlignToFrame(CFrame_Wnd::ALIGN_TOP);
}

void CLayoutView::OnBnClickedButtonAlignBottom()
{
	m_wndFrame.AlignToFrame(CFrame_Wnd::ALIGN_BOTTOM);
}

void CLayoutView::OnBnClickedButtonAlignVcenter()
{
	m_wndFrame.AlignToFrame(CFrame_Wnd::ALIGN_CENTER_VERTICAL);
}

void CLayoutView::OnBnClickedButtonAlignHcenter()
{
	m_wndFrame.AlignToFrame(CFrame_Wnd::ALIGN_CENTER_HORIZONTAL);
}

void CLayoutView::OnNMCustomdrawSliderMagnetic(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);

	m_wndFrame.SetMagneticFrame(m_sliderMagnetic.GetPos());

	*pResult = 0;
}

void CLayoutView::OnBnClickedButtonCopyFrame()
{
	if(m_wndFrame.CopyFrameReady())
	{
		m_wndFrame.PasteFrame();
	}
}

void CLayoutView::OnBnClickedButtonTemplateFromServer()
{
#ifdef _UBCSTUDIO_EE_
	CUBCStudioDoc* pDoc = GetDocument();
	if(!pDoc) return;

	CSelectPublicTemplateDialog dlg;
	if(dlg.DoModal() != IDOK) return;

	TEMPLATE_LINK* pPublicTemplateLink = dlg.GetSelectTemplateLink();
	if(!pPublicTemplateLink) return;

	TEMPLATE_INFO* pTemplateinfo = pDoc->CreateNewTemplateFromServer(pPublicTemplateLink);

	if(pTemplateinfo != NULL)
	{
		m_wndTemplate.RecalcLayout();
		m_wndTemplate.ScrollToEnd();
		OnTemplateFrameSelectChanged((WPARAM)(LPCTSTR)pTemplateinfo->strId, (LPARAM)(LPCTSTR)"");

		::AfxGetMainWnd()->SendMessage(WM_TEMPLATE_INFO_CHANGED);

		// Modified by 정운형 2009-03-04 오후 6:24
		// 변경내역 :  Template 추가 기능 수정
		OnBnClickedButtonCreateFrame();
		// Modified by 정운형 2009-03-04 오후 6:24
		// 변경내역 :  Template 추가 기능 수정
	}
#endif
}

void CLayoutView::OnBnClickedButtonTemplateToServer()
{
#ifdef _UBCSTUDIO_EE_
	CUBCStudioDoc* pDoc = GetDocument();
	if(!pDoc) return;

	TEMPLATE_LINK* pTemplateLink = pDoc->GetSelectTemplateLink();
	if(!pTemplateLink)
	{
		UbcMessageBox(LoadStringById(IDS_LAYOUTVIEW_MSG012));
		return;
	}

	CPublicTemplatesProcessingDlg dlg(GetEnvPtr()->m_szLoginID);
	dlg.AddCopyFromPrivateToPublicTemplates(*pTemplateLink);
	dlg.DoModal();
#endif
}

void CLayoutView::OnBnClickedButtonReturn()
{
	CMainFrame* pmainFrame = (CMainFrame*)::AfxGetMainWnd();
	if(!pmainFrame) return;

	TraceLog(("OnBnClickedButtonReturn 1()"));
	CWnd * pWnd = pmainFrame->GetChildFrame(ID_PACKAGE_MANAGE);

	if(pWnd)
	{
		TraceLog(("Open package manager"));
		pWnd->BringWindowToTop();
		pWnd->SetActiveWindow();
		if(pWnd->IsIconic()) {
			pWnd->ShowWindow(SW_RESTORE);
		}
	}
	else
	{
		CUBCStudioApp *pApp = (CUBCStudioApp*)::AfxGetApp();
		CMultiDocTemplate *pTemplate = pApp->m_pPackageTemplate;
		if(pTemplate){
			TraceLog(("Open layoutTemplate"));
			pTemplate->OpenDocumentFile(NULL);
		}
	}
}
