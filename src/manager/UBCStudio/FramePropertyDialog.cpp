// FramePropertyDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "FramePropertyDialog.h"
#include "UBCStudioDoc.h"
#include "Enviroment.h"
#include <map>

// CFramePropertyDialog 대화 상자입니다.
IMPLEMENT_DYNAMIC(CFramePropertyDialog, CDialog)

CFramePropertyDialog::CFramePropertyDialog(CWnd* pParent /*=NULL*/)
:	CDialog(CFramePropertyDialog::IDD, pParent)
,	m_bNowSetting (false)
{
	m_brushBG.CreateSolidBrush(RGB(255,255,255));
}

CFramePropertyDialog::~CFramePropertyDialog()
{
}

void CFramePropertyDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_TEMPLATE_ID, m_editTemplateID);
	DDX_Control(pDX, IDC_EDIT_FRAME_ID, m_editFrameID);
	//DDX_Control(pDX, IDC_EDIT_GRADE, m_editGrade);
	DDX_Control(pDX, IDC_EDIT_ZORDER, m_editZOrder);
	DDX_Control(pDX, IDC_COMBO_TVFRAME, m_cbxTVFrame);
	DDX_Control(pDX, IDC_SPIN_ZORDER, m_spinZOrder);
	DDX_Control(pDX, IDC_EDIT_POS_X, m_editPosX);
	DDX_Control(pDX, IDC_EDIT_POS_Y, m_editPosY);
	DDX_Control(pDX, IDC_EDIT_WIDTH, m_editWidth);
	DDX_Control(pDX, IDC_EDIT_HEIGHT, m_editHeight);
	DDX_Control(pDX, IDC_COMBO_BORDER_STYLE, m_cbxBorderStyle);
	DDX_Control(pDX, IDC_EDIT_BORDER_THICKNESS, m_editBorderThickness);
	DDX_Control(pDX, IDC_COMBO_BORDER_COLOR, m_cbxBorderColor);
	DDX_Control(pDX, IDC_EDIT_CORNER_RADIUS, m_editCornerRadius);
	DDX_Control(pDX, IDC_COMBO_PIP, m_cbxPIP);
	DDX_Control(pDX, IDC_EDIT_TRANS, m_editAlpha);
	DDX_Control(pDX, IDC_EDIT_DESCRIPTION, m_editDescription);
	DDX_Control(pDX, IDC_EDIT_COMMENT2, m_editComment2);
	DDX_Control(pDX, IDC_EDIT_COMMENT3, m_editComment3);
	//DDX_Control(pDX, IDC_SPIN_GRADE, m_spinGrade);
	DDX_Control(pDX, IDC_SPIN_POS_X, m_spinPosX);
	DDX_Control(pDX, IDC_SPIN_POS_Y, m_spinPosY);
	DDX_Control(pDX, IDC_SPIN_WIDTH, m_spinWidth);
	DDX_Control(pDX, IDC_SPIN_HEIGHT, m_spinHeight);
	DDX_Control(pDX, IDC_SPIN_BORDER_THICKNESS, m_spinBorderThickness);
	DDX_Control(pDX, IDC_SPIN_CORNER_RADIUS, m_spinCornerRadius);
	DDX_Control(pDX, IDC_SPIN_TRANS, m_spinAlpha);
	DDX_Control(pDX, IDC_FRAME_LEVEL_COMBO, m_comboFrameLevel);
	DDX_Control(pDX, IDC_KB_OVERLAY_MODE, m_kbOverlayMode);	// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
}


BEGIN_MESSAGE_MAP(CFramePropertyDialog, CDialog)
	ON_WM_CTLCOLOR()
	//ON_EN_CHANGE(IDC_EDIT_GRADE, &CFramePropertyDialog::OnEnChangeEditGrade)
	ON_EN_CHANGE(IDC_EDIT_POS_X, &CFramePropertyDialog::OnEnChangeEditGrade)
	ON_EN_CHANGE(IDC_EDIT_POS_Y, &CFramePropertyDialog::OnEnChangeEditGrade)
	ON_EN_CHANGE(IDC_EDIT_WIDTH, &CFramePropertyDialog::OnEnChangeEditGrade)
	ON_EN_CHANGE(IDC_EDIT_HEIGHT, &CFramePropertyDialog::OnEnChangeEditGrade)
	ON_EN_CHANGE(IDC_EDIT_BORDER_THICKNESS, &CFramePropertyDialog::OnEnChangeEditGrade)
	ON_EN_CHANGE(IDC_EDIT_CORNER_RADIUS, &CFramePropertyDialog::OnEnChangeEditGrade)
	ON_EN_CHANGE(IDC_EDIT_ZORDER, &CFramePropertyDialog::OnEnChangeEditZOder)
	ON_EN_CHANGE(IDC_EDIT_TRANS, &CFramePropertyDialog::OnEnChangeEditTrans)
	ON_CBN_SELCHANGE(IDC_COMBO_BORDER_STYLE, &CFramePropertyDialog::OnEnChangeEditGrade)
	ON_CBN_SELCHANGE(IDC_COMBO_BORDER_COLOR, &CFramePropertyDialog::OnEnChangeEditGrade)
	ON_CBN_SELCHANGE(IDC_COMBO_PIP, &CFramePropertyDialog::OnComboSelChangePip)
	ON_CBN_SELCHANGE(IDC_FRAME_LEVEL_COMBO, &CFramePropertyDialog::OnCbnSelchangeFrameLevelCombo)
	ON_CBN_SELCHANGE(IDC_COMBO_TVFRAME, &CFramePropertyDialog::OnEnChangeTvframe)
	ON_MESSAGE(WM_PRIMARY_FRAME_OVERLAP, OnPrimaryFrameOverlap)
	ON_BN_CLICKED(IDC_KB_OVERLAY_MODE, &CFramePropertyDialog::OnBnClickedKbOverlayMode)
END_MESSAGE_MAP()


// CFramePropertyDialog 메시지 처리기입니다.

BOOL CFramePropertyDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_listNoCTLWnd.Add(&m_editTemplateID);
	m_listNoCTLWnd.Add(&m_editFrameID);
	//m_listNoCTLWnd.Add(&m_editGrade);
	m_listNoCTLWnd.Add(&m_cbxTVFrame);
	m_listNoCTLWnd.Add(&m_editZOrder);
	m_listNoCTLWnd.Add(&m_spinZOrder);
	m_listNoCTLWnd.Add(&m_editPosX);
	m_listNoCTLWnd.Add(&m_editPosY);
	m_listNoCTLWnd.Add(&m_editWidth);
	m_listNoCTLWnd.Add(&m_editHeight);
	m_listNoCTLWnd.Add(&m_cbxBorderStyle);
	m_listNoCTLWnd.Add(&m_editBorderThickness);
	m_listNoCTLWnd.Add(&m_cbxBorderColor);
	m_listNoCTLWnd.Add(&m_editCornerRadius);
	m_listNoCTLWnd.Add(&m_cbxPIP);
	m_listNoCTLWnd.Add(&m_editDescription);
//	m_listNoCTLWnd.Add(&m_kbOverlayMode);	// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
	m_listNoCTLWnd.Add(&m_editComment2);
	m_listNoCTLWnd.Add(&m_editComment3);
	//m_listNoCTLWnd.Add(&m_spinGrade);
	m_listNoCTLWnd.Add(&m_spinPosX);
	m_listNoCTLWnd.Add(&m_spinPosY);
	m_listNoCTLWnd.Add(&m_spinWidth);
	m_listNoCTLWnd.Add(&m_spinHeight);
	m_listNoCTLWnd.Add(&m_spinBorderThickness);
	m_listNoCTLWnd.Add(&m_spinCornerRadius);
	m_listNoCTLWnd.Add(&m_comboFrameLevel);
	m_listNoCTLWnd.Add(&m_editAlpha);

	//m_spinGrade.SetRange32(0, 10000);
	m_spinPosX.SetRange32(-10000, 10000);
	m_spinPosY.SetRange32(-10000, 10000);
	m_spinWidth.SetRange32(0, 10000);
	m_spinHeight.SetRange32(0, 10000);
	m_spinBorderThickness.SetRange32(0, 10000);
	m_spinCornerRadius.SetRange32(0, 10000);
	m_spinAlpha.SetRange32(0, 100);

	if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType){
		m_spinZOrder.SetRange32(0, 3);
	}else{
		m_spinZOrder.SetRange32(0, PIP_ZINDEX);
	}

	//Combo Set Cursor
	//m_comboFrameLevel.SetCurSel(0);
	m_comboFrameLevel.AddString("Primary Frame");
	m_comboFrameLevel.AddString("Secondary Frame");

	m_cbxBorderStyle.AddString("none");
	m_cbxBorderStyle.AddString("solid");
	m_cbxBorderStyle.AddString("inset");
	m_cbxBorderStyle.AddString("outset");

	m_cbxBorderColor.InitializeDefaultColors();
	m_cbxBorderColor.SetSelectedColorValue(RGB(0,0,0));

	m_cbxPIP.AddString("false");
	m_cbxPIP.AddString("true");

	m_cbxTVFrame.AddString("false");
	m_cbxTVFrame.AddString("true");

	SetFrameInfo(NULL);

	m_fontTip.CreatePointFont(8*10, "Tahoma");
	m_Tip.Create(this);
	m_Tip.SetShowDelay(1);
	m_Tip.SetFont(&m_fontTip);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFramePropertyDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();
}

void CFramePropertyDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnCancel();
}

HBRUSH CFramePropertyDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	int count = m_listNoCTLWnd.GetCount();
	for(int i=0; i<count; i++)
	{
		CWnd* wnd = m_listNoCTLWnd.GetAt(i);
		if(wnd->GetSafeHwnd() == pWnd->GetSafeHwnd())
			return hbr;
	}

	pDC->SetBkColor(RGB(255,255,255));
	//pDC->SetBkMode(TRANSPARENT);
	hbr = (HBRUSH)m_brushBG;

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CFramePropertyDialog::OnEnChangeEditGrade()
{
	if(m_bNowSetting == false){
		GetParent()->SendMessage(WM_FRAME_INFO_CHANGED, 1);
	}
}

void CFramePropertyDialog::OnComboSelChangePip()
{
	//USTB type 은 일반 frame 3개 pip frame 1개만 가능함
	if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType && m_cbxPIP.GetCurSel() == 1)
	{
		CFormView* pView = (CFormView*)GetParent();
		if(!pView)	return;

		CUBCStudioDoc* pDoc = (CUBCStudioDoc*)pView->GetDocument();
		if(!pDoc)	return;

		TEMPLATE_LINK* pTemplateLink = pDoc->GetSelectTemplateLink();
		if(!pTemplateLink)	return;

		FRAME_LINK_LIST* pFrmList = &(pTemplateLink->arFrameLinkList);
		
		for(int i = 0; i < pFrmList->GetCount(); i++){
			FRAME_INFO* pFrmInfo = pFrmList->GetAt(i).pFrameInfo;
			if(!pFrmInfo)	continue;

			if(pFrmInfo->bIsPIP)
			{
				m_cbxPIP.SetCurSel(0);
				UbcMessageBox(LoadStringById(IDS_FRAMEPROPERTYDIALOG_MSG001), MB_ICONERROR);
				return;
			}
		}
	}

	if(m_cbxPIP.GetCurSel() == 0)
	{
		CFormView* pView = (CFormView*)GetParent();
		if(!pView)	return;

		CUBCStudioDoc* pDoc = (CUBCStudioDoc*)pView->GetDocument();
		if(!pDoc)	return;

		TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
		
		if(pTempLink){
			m_editZOrder.SetValue(GetMaxZIndex(pTempLink));
		}
	}
	else
	{
		// PIP 는 1000 으로 고정함.
		m_editZOrder.SetValue(PIP_ZINDEX);
	}

	OnEnChangeEditGrade();
}

void CFramePropertyDialog::OnCbnSelchangeFrameLevelCombo()
{
	CFormView* pView = (CFormView*)GetParent();
	if(!pView) return;

	CUBCStudioDoc* pDoc = (CUBCStudioDoc*)pView->GetDocument();
	if(!pDoc) return;

	FRAME_LINK* pFrmLink = pDoc->GetSelectFrameLink();
	if(!pFrmLink) return;

	// 정시플레이콘텐츠이 있는 경우 Primary->Secondary로 변경 불가하도록 함.
	if( pFrmLink->pFrameInfo->nGrade  == 1 &&
		m_comboFrameLevel.GetCurSel() == 1 &&
		pFrmLink->pFrameInfo->arTimePlayContentsList.GetCount() > 0 )
	{
		m_comboFrameLevel.SetCurSel(0);
		UbcMessageBox(LoadStringById(IDS_FRAMEPROPERTYDIALOG_MSG002), MB_ICONWARNING);
		return;
	}

	OnEnChangeEditGrade();
}


void CFramePropertyDialog::SetFrameInfo(FRAME_LINK* pFrameLink, int MouseType)
{
	BOOL enable_window;
	m_bNowSetting = true;

	if(pFrameLink != NULL)
	{
		enable_window = TRUE;

		m_editTemplateID.SetWindowText(pFrameLink->pFrameInfo->strTemplateId);
		m_editFrameID.SetWindowText(pFrameLink->pFrameInfo->strId);
		//m_editGrade.SetValue(pFrameLink->pFrameInfo->nGrade);
		m_editPosX.SetValue(pFrameLink->pFrameInfo->rcRect.left);
		m_editPosY.SetValue(pFrameLink->pFrameInfo->rcRect.top);

		m_editZOrder.SetValue(pFrameLink->pFrameInfo->nZIndex);
		m_cbxTVFrame.SetCurSel(pFrameLink->pFrameInfo->bIsTV?1:0);
		m_editAlpha.SetValue(pFrameLink->pFrameInfo->nAlpha);

		if(pFrameLink->pFrameInfo->bIsTV == 1)
		{
			SetTVSize(pFrameLink->pFrameInfo->rcRect.Width(), pFrameLink->pFrameInfo->rcRect.Height(), MouseType);
		}
		else
		{
			m_editWidth.SetValue(pFrameLink->pFrameInfo->rcRect.Width());
			m_editHeight.SetValue(pFrameLink->pFrameInfo->rcRect.Height());
		}
		
		m_cbxBorderStyle.SetCurSel(GetIndex(pFrameLink->pFrameInfo->strBorderStyle));
		m_editBorderThickness.SetValue(pFrameLink->pFrameInfo->nBorderThickness);
		m_cbxBorderColor.SetSelectedColorValue(pFrameLink->pFrameInfo->crBorderColor);
		m_editCornerRadius.SetValue(pFrameLink->pFrameInfo->nCornerRadius);
		m_cbxPIP.SetCurSel((pFrameLink->pFrameInfo->bIsPIP) ? 1 : 0);
		m_editDescription.SetWindowText(pFrameLink->pFrameInfo->strDescription);
		m_kbOverlayMode.SetCheck( pFrameLink->pFrameInfo->strComment[0] == _T("9") );	// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
		m_editComment2.SetWindowText(pFrameLink->pFrameInfo->strComment[1]);
		m_editComment3.SetWindowText(pFrameLink->pFrameInfo->strComment[2]);
		m_comboFrameLevel.SetCurSel((pFrameLink->pFrameInfo->nGrade == 1 ? 0 : 1));
	}
	else
	{
		enable_window = FALSE;

		m_editTemplateID.SetWindowText("");
		m_editFrameID.SetWindowText("");
		//m_editGrade.SetWindowText("");
		m_editPosX.SetWindowText("");
		m_editPosY.SetWindowText("");
		m_editWidth.SetWindowText("");
		m_editHeight.SetWindowText("");
		m_cbxBorderStyle.SetCurSel(-1);
		m_editBorderThickness.SetWindowText("");
		m_cbxBorderColor.SetCurSel(-1);
		m_editCornerRadius.SetWindowText("");
		m_cbxPIP.SetCurSel(-1);
		m_editDescription.SetWindowText("");
		m_kbOverlayMode.SetCheck(FALSE);	// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
		m_editComment2.SetWindowText("");
		m_editComment3.SetWindowText("");

		m_comboFrameLevel.SetCurSel(-1);
	}

	m_bNowSetting = false;

	m_editTemplateID.EnableWindow(enable_window);
	m_editFrameID.EnableWindow(enable_window);
	//m_editGrade.EnableWindow(enable_window);
	m_editPosX.EnableWindow(enable_window);
	m_editPosY.EnableWindow(enable_window);
	m_editWidth.EnableWindow(enable_window);
	m_editHeight.EnableWindow(enable_window);
	m_cbxBorderStyle.EnableWindow(enable_window);
	m_editBorderThickness.EnableWindow(enable_window);
	m_cbxBorderColor.EnableWindow(enable_window);
	m_editCornerRadius.EnableWindow(enable_window);
	m_cbxPIP.EnableWindow(GetPipCtrlStatus(pFrameLink, enable_window));
	m_editDescription.EnableWindow(enable_window);
	m_kbOverlayMode.EnableWindow(enable_window);	// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
	m_editComment2.EnableWindow(enable_window);
	m_editComment3.EnableWindow(enable_window);

	m_comboFrameLevel.EnableWindow(enable_window);
	m_editZOrder.EnableWindow(enable_window);

	if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
	{
		m_cbxTVFrame.EnableWindow(enable_window);
		m_editAlpha.EnableWindow(enable_window);
		m_editZOrder.EnableWindow(enable_window);
	}
	else
	{
		m_cbxTVFrame.SetCurSel(0);
		m_cbxTVFrame.EnableWindow(FALSE);
		m_editAlpha.EnableWindow(FALSE);
		m_editZOrder.EnableWindow(FALSE);
		
		m_cbxTVFrame.ShowWindow(SW_HIDE);
		m_editAlpha.ShowWindow(SW_HIDE);
		m_editZOrder.ShowWindow(SW_HIDE);

		m_spinAlpha.ShowWindow(SW_HIDE);
		m_spinZOrder.ShowWindow(SW_HIDE);

		GetDlgItem(IDC_STATIC1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC3)->ShowWindow(SW_HIDE);
	}

	Invalidate(FALSE);
}

bool CFramePropertyDialog::GetFrameInfo(FRAME_LINK* pFrameLink)
{
	if(pFrameLink != NULL)
	{
		m_editTemplateID.GetWindowText(pFrameLink->pFrameInfo->strTemplateId);
		m_editFrameID.GetWindowText(pFrameLink->pFrameInfo->strId);
		//pFrameLink->pFrameInfo->nGrade = m_editGrade.GetValueInt();

		TEMPLATE_INFO* pTempInfo = NULL;
		FRAME_INFO* pFrmInfo = NULL;
		CFormView* pView = (CFormView*)GetParent();
		if(pView){
			CUBCStudioDoc* pDoc = (CUBCStudioDoc*)pView->GetDocument();
			if(pDoc){
				TEMPLATE_LINK* pTemplateLink = pDoc->GetSelectTemplateLink();
				if(pTemplateLink){
					pTempInfo = pTemplateLink->pTemplateInfo;
				}
			}
		}

		if(m_editPosX.GetValueInt() < 0){
			m_editPosX.SetValue(0);
		}
		else if(pTempInfo && pTempInfo->rcRect.Width() <=  m_editPosX.GetValueInt()){
			m_editPosX.SetValue(pTempInfo->rcRect.right-1);
		}
		pFrameLink->pFrameInfo->rcRect.left = m_editPosX.GetValueInt();

		if(m_editPosY.GetValueInt() < 0){
			m_editPosY.SetValue(0);
		}
		else if(pTempInfo && pTempInfo->rcRect.Height() <=  m_editPosY.GetValueInt()){
			m_editPosY.SetValue(pTempInfo->rcRect.bottom-1);
		}
		pFrameLink->pFrameInfo->rcRect.top = m_editPosY.GetValueInt();

		if(pTempInfo){
			CRect rect_width;
			m_editWidth.GetWindowRect(rect_width);
			CRect rect_height;
			m_editHeight.GetWindowRect(rect_height);

			bool show_tooltip = false;

			if(m_editWidth.GetValueInt() <= 0){
				m_editWidth.SetValue(1);
				CPoint pt(rect_width.left, rect_width.top + rect_width.Height()/2 );
				m_Tip.Show(LoadStringById(IDS_AVAILABLE_MIN_VALUE), &pt);
				MessageBeep(MB_OK);
				show_tooltip = true;
			}
			else if(pTempInfo->rcRect.Width() < pFrameLink->pFrameInfo->rcRect.left + m_editWidth.GetValueInt()){
				m_editWidth.SetValue(pTempInfo->rcRect.Width()-pFrameLink->pFrameInfo->rcRect.left);
				CPoint pt(rect_width.left, rect_width.top + rect_width.Height()/2 );
				m_Tip.Show(LoadStringById(IDS_AVAILABLE_MAX_VALUE), &pt);	
				MessageBeep(MB_OK);
				show_tooltip = true;
			}
			pFrameLink->pFrameInfo->rcRect.right = pFrameLink->pFrameInfo->rcRect.left + m_editWidth.GetValueInt();

			if(m_editHeight.GetValueInt() <= 0){
				m_editHeight.SetValue(1);
				CPoint pt(rect_height.left, rect_height.top + rect_height.Height()/2 );
				m_Tip.Show(LoadStringById(IDS_AVAILABLE_MIN_VALUE), &pt);	
				MessageBeep(MB_OK);
				show_tooltip = true;
			}
			else if(pTempInfo->rcRect.Height() < pFrameLink->pFrameInfo->rcRect.top + m_editHeight.GetValueInt()){
				m_editHeight.SetValue(pTempInfo->rcRect.Height()-pFrameLink->pFrameInfo->rcRect.top);
				CPoint pt(rect_height.left, rect_height.top + rect_height.Height()/2 );
				m_Tip.Show(LoadStringById(IDS_AVAILABLE_MAX_VALUE), &pt);	
				MessageBeep(MB_OK);
				show_tooltip = true;
			}
			pFrameLink->pFrameInfo->rcRect.bottom = pFrameLink->pFrameInfo->rcRect.top + m_editHeight.GetValueInt();

			if( show_tooltip==false )
			{
				m_Tip.Hide();
			}
		}else{	
			pFrameLink->pFrameInfo->rcRect.right = pFrameLink->pFrameInfo->rcRect.left + m_editWidth.GetValueInt();
			pFrameLink->pFrameInfo->rcRect.bottom = pFrameLink->pFrameInfo->rcRect.top + m_editHeight.GetValueInt();
		}

		m_cbxBorderStyle.GetLBText(m_cbxBorderStyle.GetCurSel(), pFrameLink->pFrameInfo->strBorderStyle);
		pFrameLink->pFrameInfo->nBorderThickness = m_editBorderThickness.GetValueInt();
		pFrameLink->pFrameInfo->crBorderColor = m_cbxBorderColor.GetSelectedColorValue();
		pFrameLink->pFrameInfo->nCornerRadius = m_editCornerRadius.GetValueInt();
		pFrameLink->pFrameInfo->bIsPIP = (m_cbxPIP.GetCurSel() == 1);
		m_editDescription.GetWindowText(pFrameLink->pFrameInfo->strDescription);
		pFrameLink->pFrameInfo->strComment[0] = (m_kbOverlayMode.GetCheck()?_T("9"):_T(""));	// 0000543: "고화질 동영상용 프레임". 속성을 추가한다. 
		m_editComment2.GetWindowText(pFrameLink->pFrameInfo->strComment[1]);
		m_editComment3.GetWindowText(pFrameLink->pFrameInfo->strComment[2]);

		pFrameLink->pFrameInfo->nZIndex = m_editZOrder.GetValueInt();
		pFrameLink->pFrameInfo->bIsTV = (m_cbxTVFrame.GetCurSel() == 1);
		pFrameLink->pFrameInfo->nAlpha = m_editAlpha.GetValueInt();

		if(m_comboFrameLevel.GetCurSel() == 0){
			pFrameLink->pFrameInfo->nGrade = 1;
		}else{
			pFrameLink->pFrameInfo->nGrade = 2;
		}

		return true;
	}
	return false;
}

void CFramePropertyDialog::OnEnChangeEditZOder()
{
	if(!IsWindow(m_editZOrder.m_hWnd))
		return;

	if(m_editZOrder.GetValueInt() < 0)
		m_editZOrder.SetValue(0);
	else if(PIP_ZINDEX < m_editZOrder.GetValueInt())
		m_editZOrder.SetValue(PIP_ZINDEX);

	CFormView* pView = (CFormView*)GetParent();
	if(!pView)		return;

	CUBCStudioDoc* pDoc = (CUBCStudioDoc*)pView->GetDocument();
	if(!pDoc)		return;

	TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
	if(!pTempLink)	return;

	FRAME_LINK* pFrmLink = pDoc->GetSelectFrameLink();
	if(!pFrmLink)	return;

	int nOldZIndex = pFrmLink->pFrameInfo->nZIndex;
	pFrmLink->pFrameInfo->nZIndex = m_editZOrder.GetValueInt();

	for(int i = 0; i < pTempLink->arFrameLinkList.GetCount(); i++){
		FRAME_INFO*	pFrameInfo = pTempLink->arFrameLinkList.GetAt(i).pFrameInfo;
		if(!pFrameInfo)	continue;

		if(pFrameInfo->strId != pFrmLink->pFrameInfo->strId && m_editZOrder.GetValueInt() == pFrameInfo->nZIndex){
			pFrameInfo->nZIndex = nOldZIndex;
			break;
		}
	}
}

void CFramePropertyDialog::OnEnChangeEditTrans()
{
	if(!IsWindow(m_editAlpha.m_hWnd))
		return;

	if(m_editAlpha.GetValueInt() < 0)
		m_editAlpha.SetValue(0);
	else if(100 < m_editAlpha.GetValueInt())
		m_editAlpha.SetValue(100);

	if(m_bNowSetting == false){
		GetParent()->SendMessage(WM_FRAME_INFO_CHANGED, 1);
	}
}

void CFramePropertyDialog::OnEnChangeTvframe()
{
	CFormView* pView = (CFormView*)GetParent();
	if(!pView)	return;

	CUBCStudioDoc* pDoc = (CUBCStudioDoc*)pView->GetDocument();
	if(!pDoc)	return;

	FRAME_LINK* pFrmLink = pDoc->GetSelectFrameLink();
	if(!pFrmLink)	return;

	if(m_cbxTVFrame.GetCurSel() != 1){
		pFrmLink->pFrameInfo->bIsTV = false;
		return;
	}

	pFrmLink->pFrameInfo->bIsTV = true;
	SetTVSize(m_editWidth.GetValueInt(), m_editHeight.GetValueInt());

	if(m_bNowSetting == false){
		GetParent()->SendMessage(WM_FRAME_INFO_CHANGED, 1);
	}
}

void CFramePropertyDialog::SetTVSize(int nWidth, int nHeight, int MouseType)
{
	if(CFrame_Wnd::MOUSE_IDLE < MouseType && MouseType < CFrame_Wnd::MOUSE_SIZING_LEFT_TOP)
		return;

	CFormView* pView = (CFormView*)GetParent();
	if(!pView)	return;

	CUBCStudioDoc* pDoc = (CUBCStudioDoc*)pView->GetDocument();
	if(!pDoc)	return;

	TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
	if(!pTempLink)	return;

	FRAME_LINK* pFrmLink = pDoc->GetSelectFrameLink();
	if(!pFrmLink)	return;

	int nRight = pTempLink->pTemplateInfo->rcRect.right;
	int nBottom = pTempLink->pTemplateInfo->rcRect.bottom;

	int nTVWidth = (4*nHeight)/3;
	int nTVHeight = (3*nWidth)/4;

	if(MouseType == CFrame_Wnd::MOUSE_IDLE){
		if(nRight < pFrmLink->pFrameInfo->rcRect.left + nTVWidth){
			m_editHeight.SetValue(nTVHeight);
		}
		else if(nBottom < pFrmLink->pFrameInfo->rcRect.top + nTVHeight){
			m_editWidth.SetValue(nTVWidth);
		}
		else if(m_editHeight.GetValueInt() > m_editWidth.GetValueInt()){
			m_editHeight.SetValue(nTVHeight);
		}else{
			m_editWidth.SetValue(nTVWidth);
		}
	}
	else if(nRight < pFrmLink->pFrameInfo->rcRect.left + nTVWidth){
//		m_editWidth.GetValueInt();
	}
	else if(nBottom < pFrmLink->pFrameInfo->rcRect.top + nTVHeight){
//		 m_editHeight.GetValueInt();
	}
	else if(MouseType == CFrame_Wnd::MOUSE_SIZING_TOP || MouseType == CFrame_Wnd::MOUSE_SIZING_BOTTOM){
		m_editWidth.SetValue(nTVWidth);
		m_editHeight.SetValue(nHeight);
	}
	else if(MouseType == CFrame_Wnd::MOUSE_SIZING_LEFT || MouseType == CFrame_Wnd::MOUSE_SIZING_RIGHT){
		m_editWidth.SetValue(nWidth);
		m_editHeight.SetValue(nTVHeight);
	}
//	else if(m_editHeight.GetValueInt() == nHeight){
//		m_editWidth.SetValue(nWidth);
//		m_editHeight.SetValue(nTVHeight);
//	}
	else{
		m_editWidth.SetValue(nTVWidth);
		m_editHeight.SetValue(nHeight);
	}
	
	pFrmLink->pFrameInfo->rcRect.right = pFrmLink->pFrameInfo->rcRect.left + m_editWidth.GetValueInt();
	pFrmLink->pFrameInfo->rcRect.bottom = pFrmLink->pFrameInfo->rcRect.top + m_editHeight.GetValueInt();
}

int CFramePropertyDialog::GetIndex(CString strStyle)
{
	if(strStyle == "none")
		return 0;
	else if(strStyle == "solid")
		return 1;
	else if(strStyle == "inset")
		return 2;
	else if(strStyle == "outset")
		return 3;

	return -1;
}

int CFramePropertyDialog::GetMaxZIndex(TEMPLATE_LINK* pTempLink)
{
	int nMaxZIndex = 0;
	CMap<int,int,int,int> mapZIndex;

	for(int i = 0; i < pTempLink->arFrameLinkList.GetCount(); i++){
		FRAME_INFO* pFrmInfo = pTempLink->arFrameLinkList.GetAt(i).pFrameInfo;
		if(!pFrmInfo)	continue;

		if(pFrmInfo->nZIndex < PIP_ZINDEX){
			mapZIndex.SetAt(pFrmInfo->nZIndex, pFrmInfo->nZIndex);

			if(nMaxZIndex < pFrmInfo->nZIndex)
				nMaxZIndex = pFrmInfo->nZIndex;
		}
	}

	int val;
	for(int i = 1; i < nMaxZIndex; i++){
		if(!mapZIndex.Lookup(i, val))
			return i;
	}

	return ++nMaxZIndex;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Primary frame overlap message handler\n
/// @param (WPARAM) wParam : (in) 부가정보
/// @param (LPARAM) lParam : (in) 부가정보
/// @return <형: LRESULT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
LRESULT CFramePropertyDialog::OnPrimaryFrameOverlap(WPARAM wParam, LPARAM lParam)
{
	m_comboFrameLevel.SetCurSel(1);

	return 0;
}

void CFramePropertyDialog::OnBnClickedKbOverlayMode()
{
	if(m_kbOverlayMode.GetCheck())
	{
		CFormView* pView = (CFormView*)GetParent();
		if(!pView)		return;

		CUBCStudioDoc* pDoc = (CUBCStudioDoc*)pView->GetDocument();
		if(!pDoc)		return;

		TEMPLATE_LINK* pTempLink = pDoc->GetSelectTemplateLink();
		if(!pTempLink)	return;

		FRAME_LINK* pFrmLink = pDoc->GetSelectFrameLink();
		if(!pFrmLink)	return;

		// 같은 레이아웃의 다른 프레임에 고화질 동영상 전용으로 설정되어 있는지 체크
		// 같은 레이아웃의 다른 프레임에 동영상 플레이콘텐츠가 설정되어 있는지 체크
		m_kbOverlayMode.SetCheck(pDoc->CheckOverlayModeFrame(pTempLink->pTemplateInfo->strId, pFrmLink->pFrameInfo->strId));

		if(m_kbOverlayMode.GetCheck())
		{
			UbcMessageBox(LoadStringById(IDS_FRAMEPROPERTYDIALOG_MSG003));
		}
	}

	if(m_bNowSetting == false)
	{
		GetParent()->SendMessage(WM_FRAME_INFO_CHANGED, 1);
	}
}

bool CFramePropertyDialog::GetPipCtrlStatus(FRAME_LINK* pFrameLink, bool bEnable)
{
	if(!bEnable) return bEnable;

	if(!pFrameLink) return false;

	CFormView* pView = (CFormView*)GetParent();
	if(!pView) return false;

	CUBCStudioDoc* pDoc = (CUBCStudioDoc*)pView->GetDocument();
	if(!pDoc) return false;

	TEMPLATE_LINK* pTemplateLink = pDoc->GetSelectTemplateLink();
	if(!pTemplateLink) return false;

	bool bIsInclusion = false;	// 다른 프레임에 포함되어 있는지
	bool bIsOverlap   = false;	// 다른 프레임과 겹쳐있는지
	bool bHaveFrame   = false;	// 다른 프레임을 포함하고 있는지

	int count = pTemplateLink->arFrameLinkList.GetCount();
	for(int i=0; i<count; i++)
	{
		FRAME_LINK& frame_link = pTemplateLink->arFrameLinkList.GetAt(i);
		if(pFrameLink->pFrameInfo->strId == frame_link.pFrameInfo->strId)
		{
			continue;
		}

		// 다른 프레임과 겹쳐있는지 여부 체크
		CRect rcOverlap;
		if(pFrameLink->pFrameInfo->rcRect == frame_link.pFrameInfo->rcRect)
		{
			bIsInclusion = true;
			bHaveFrame = true;
		}
		else if( ::IntersectRect(rcOverlap, pFrameLink->pFrameInfo->rcRect, frame_link.pFrameInfo->rcRect) )
		{
			// 다른 프레임에 포함되어 있는지 여부 체크
			if( pFrameLink->pFrameInfo->rcRect == rcOverlap )
			{
				bIsInclusion = true;
			}
			// 다른 프레임을 포함하고 있는지 여부 체크
			else if( frame_link.pFrameInfo->rcRect == rcOverlap )
			{
				bHaveFrame = true;
			}
			else
			{
				bIsOverlap = true;
			}
		}
	}

	// PIP를 갖고있는(포함하는) 프레임은 PIP로 설정할 수 없도록한다
	if(bHaveFrame) return false;
	// 다른 프레임과 일부분이 겹치는 프레임은 PIP속성을 설정할 수 있다(사용자가 직접 설정, 이전값 유지) 
	if(bIsOverlap) return true;
	// 다른 프레임안에 완전히 포함되며 겹쳐있지 않은 프레임은 자동으로 PIP속성을 갖도록한다
	if(bIsInclusion) return false;
	// 그외 다른 프레임과 겹치지 않는 프레임은 PIP로 설정할 수 없도록한다
	return false;
}