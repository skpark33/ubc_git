#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "ReposControl.h"
#include "Template_Wnd.h"
#include "Frame_Wnd.h"
#include "FramePropertyDialog.h"
#include "TemplatePropertyDialog.h"

//#include "XPTabCtrl.h"
#include "common/HoverButton.h"
#include "common/UBCTabCtrl.h"

// CLayoutView 폼 뷰입니다.

class CLayoutView : public CFormView
{
	DECLARE_DYNCREATE(CLayoutView)

protected:
	CLayoutView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CLayoutView();

public:
	enum { IDD = IDD_LAYOUT_VIEW };
	CUBCStudioDoc* GetDocument() const;

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	BOOL            m_bCtrl;
	CReposControl	m_reposControl;
	CTemplate_Wnd	m_wndTemplate;
	CFrame_Wnd		m_wndFrame;
	CTemplatePropertyDialog	m_dlgTemplateProperty;
	CFramePropertyDialog	m_dlgFrameProperty;

public:
	CStatic		m_frameTemplate;
	CStatic		m_frameFrame;
	//CTabCtrl	m_tabProperty;
	CStatic		m_frameFrameProperty;

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	/*
	CButton		m_btnCreateTemplate;
	CButton		m_btnRemoveTemplate;
	CButton		m_btnCreateFrame;
	CButton		m_btnRemoveFrame;
	CButton		m_btnMovePrevFrame;
	CButton		m_btnMoveNextFrame;
	*/
	CHoverButton	m_btnCreateTemplate;
	CHoverButton	m_btnRemoveTemplate;
	CHoverButton	m_btnCreateFromServer;
	CHoverButton	m_btnSaveToServer;
	
	CHoverButton	m_btnCreateFrame;
	CHoverButton	m_btnCopyFrame;
	CHoverButton	m_btnRemoveFrame;
	CHoverButton	m_btnMovePrevFrame;
	CHoverButton	m_btnMoveNextFrame;
	CHoverButton	m_btnFramUp;
	CHoverButton	m_btnFramDn;
	CHoverButton	m_btnFramAlignLeft;
	CHoverButton	m_btnFramAlignRight;
	CHoverButton	m_btnFramAlignTop;
	CHoverButton	m_btnFramAlignBottom;
	CHoverButton	m_btnFramAlignVCenter;
	CHoverButton	m_btnFramAlignHCenter;
//	CXPTabCtrl		m_tabProperty;
	CUBCTabCtrl		m_tabProperty;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	// Modified by 정운형 2009-03-04 오후 5:18
	// 변경내역 :  Primary frame 설정 변경 수정
	FRAME_LINK*	GetPrimaryFrameLink(void);			///<현재 템플릿의 Primary FrameLink를 가져온다.
	// Modified by 정운형 2009-03-04 오후 5:18
	// 변경내역 :  Primary frame 설정 변경 수정

	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnTemplateFrameSelectChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTemplateInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFrameInfoChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTemplateListChanged(WPARAM wParam, LPARAM lParam);
	afx_msg void OnTcnSelchangeTabProperty(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonCreateTemplate();
	afx_msg void OnBnClickedButtonRemoveTemplate();
	afx_msg void OnBnClickedButtonCreateFrame();
	afx_msg void OnBnClickedButtonRemoveFrame();
	afx_msg void OnBnClickedButtonMovePrevFrame();
	afx_msg void OnBnClickedButtonMoveNextFrame();
	afx_msg void OnBnClickedButtonFrameUp();
	afx_msg void OnBnClickedButtonFrameDn();
	afx_msg LRESULT	OnConfigSaveComplete(WPARAM wParam, LPARAM lParam);		///<complete config save message handler
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnBnClickedButtonAlignLeft();
	afx_msg void OnBnClickedButtonAlignRight();
	afx_msg void OnBnClickedButtonAlignTop();
	afx_msg void OnBnClickedButtonAlignBottom();
	afx_msg void OnBnClickedButtonAlignVcenter();
	afx_msg void OnBnClickedButtonAlignHcenter();
	CSliderCtrl m_sliderMagnetic;
	afx_msg void OnNMCustomdrawSliderMagnetic(NMHDR *pNMHDR, LRESULT *pResult);
	CStatic m_txtMagnetic;
	afx_msg void OnBnClickedButtonCopyFrame();
	afx_msg void OnBnClickedButtonTemplateFromServer();
	afx_msg void OnBnClickedButtonTemplateToServer();
	CHoverButton m_btReturn;
	afx_msg void OnBnClickedButtonReturn();
};


#ifndef _DEBUG  // debug version in PackageView.cpp
inline CUBCStudioDoc* CLayoutView::GetDocument() const
   { return reinterpret_cast<CUBCStudioDoc*>(m_pDocument); }
#endif
