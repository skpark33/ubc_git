#include "stdafx.h"
#include "Enviroment.h"
#include "mainfrm.h"
#include "LocalHostInfo.h"
#include "SimpleFtp.h"
#include "common/libFTPClient\FTPClient.h"
#include "common/libFileServiceWrap/FileServiceWrapDll.h"
#ifdef _DEBUG
	#pragma comment(lib, "FileServiceWrap_d.lib")
#else
	#pragma comment(lib, "FileServiceWrap.lib")
#endif

//#include "CustomerInfoDlg.h"

#ifdef _TAO
#include "CopModule.h"
#include "common/libCommon/ubcMuxRegacy.h"
#include "common/libCommon/ubcHost.h"
#endif//_TAO

CString g_szTraceLogBuf = _T("");
CEnviroment* CEnviroment::m_pThis = NULL;

CEnviroment::CEnviroment() :
#ifdef _UBCSTUDIO_EE_
	m_Edition(eStudioEE),
	m_Authority(eAuthNoUser),
#else
	m_Edition(eStudioPE),
	m_Authority(eAuthSuper),
#endif//_UBCSTUDIO_EE_
	m_bLicense(false)
{
	m_TotalContentsSize = 0;

	m_lMaxContentsSize = 0;
	m_nMaxContentsCount = 0;
	m_nMaxPlayTemplateCount = 0;

	m_bUseTimeBasePlayContents = false;
	m_bUseClickPlayContents = false;	// 0000782: 부모-자식 스케줄 기능을 수정한다.
	m_bUseTimeCheck = false;	// skpark same_size_file_problem

	m_bUseTV = false;

	m_szSite.Empty();
	m_szLoginID.Empty();
	m_szPassword.Empty();

	m_strPackage.Empty();

//	m_lsSite.RemoveAll();
	m_lsHost.RemoveAll();
	RemoveAllPackage();
	m_lsTempRect.RemoveAll();

	m_bUseAutoTempSave = false;
	m_nAutoTempSaveInterval = 0;

	m_bAutoEncoding = false;

	InitPath();
	InitLoginInfo();

	Load_ini();
#ifdef _UBCSTUDIO_EE_
	Load_ee_ini();
#else
	Load_pe_ini();
#endif//_UBCSTUDIO_EE_
}

CEnviroment::~CEnviroment()
{
	RemoveHost();
}

CEnviroment* CEnviroment::GetObject()
{
	if(m_pThis)
		return m_pThis;
	m_pThis = new CEnviroment;
	return m_pThis;
}

void CEnviroment::Release()
{
	if(!m_pThis)	return;
	delete m_pThis;
	m_pThis = NULL;
}

void CEnviroment::InitPath()
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	m_szDrive = szDrive;
	m_szPath = szPath;
	m_szFile = m_szName = szFilename;
	m_szFile += m_szExt = szExt;
	m_szModule = szModule;
}

void CEnviroment::Log(const char* szfilename, int nLine, const char* szMsg)
{
#ifdef _TAO
	copTrace(szfilename, nLine, szMsg);
#endif//_TAO
}

void CEnviroment::InitLoginInfo()
{
#ifdef _UBCSTUDIO_EE_
	CString szFile = m_szDrive + m_szPath + _T("data\\");
	szFile += LOGIN_DAT;

	std::ifstream ifLogin(szFile, ios::in | ios_base::binary);
	if( ifLogin.fail() ) {
		ifLogin.close();
		return;
	}

	SLoginInfo Info;
	ZeroMemory(&Info, sizeof(Info));
	ifLogin.read(reinterpret_cast<char*>(&Info),sizeof(SLoginInfo));
	if( ifLogin.fail() ) {
		ifLogin.close();
		return;
	}
	ifLogin.close();

	m_szSite = Info.szSite;
	m_szLoginID = Info.szID;
	m_szPassword = Encrypt(Info.szPW, Info.nPWSize);
#endif//_UBCSTUDIO_EE_
}

void CEnviroment::SaveLoginInfo(bool bCheck)
{
	SLoginInfo Info;
	ZeroMemory(&Info, sizeof(Info));

	CString szFile = m_szDrive + m_szPath + _T("data\\");
	szFile += LOGIN_DAT;

	std::ofstream ofLogin(szFile, ios_base::binary);
	if( ofLogin.fail() ) {
		ofLogin.close();
		return;
	}

	if(bCheck){
		strcpy(Info.szSite, m_szSite);
		strcpy(Info.szID, m_szLoginID);
		strcpy(Info.szPW, m_szPassword);

		Info.nPWSize = strlen(Info.szPW);
		Encrypt(Info.szPW, Info.nPWSize);
	}

	ofLogin.write(reinterpret_cast<char*>(&Info),sizeof(SLoginInfo));
	if( ofLogin.fail() ) {
		ofLogin.close();
		return;
	}

	ofLogin.close();
}

char* CEnviroment::Encrypt(char* buf, int nSize)
{
	char cKey = 'A';
	for(int i = 0; i < nSize; i++){
		cKey++;
		if(cKey >= 'z')	cKey = '0';
		buf[i] ^= cKey;
	}
	return buf;
}

UINT CEnviroment::RefreshPackageList(bool bAll, LPCTSTR szWhere)
{
	if(bAll || m_mapPackage.GetCount() == 0)// 전체를 갱신
	{
		RemoveAllPackage();
#ifdef _TAO
		copGetPackage((GetEnvPtr()->m_Authority==CEnviroment::eAuthSuper)?_T("*"):GetEnvPtr()->m_szSite, szWhere);
#endif//_TAO
	}
	// Local drive package 만 갱신
	else
	{
		RemovePackageWithout(STR_ENV_SERVER);
	}

	CPtrArray& arLocalHost = ((CMainFrame*)AfxGetMainWnd())->m_aryLocalHostInfo;
	CString strDrive, strNetworkUse, strHostName, strDesc, strSite, strWrittenTime, strWrittenID;

	long nContentsCategory, nPurpose, nHostType, nVertical, nResolution;
	bool bIsPublic, bIsVerify;
	COleDateTime tmValidationDate;
	CString strValidationDate;

	for(int i = 0; i<arLocalHost.GetCount(); i++)
	{
		CLocalHostInfo* pLocalHost = (CLocalHostInfo*)arLocalHost.GetAt(i);
		if(!pLocalHost)	continue;

		pLocalHost->GetLocalHostInfo(strDrive, strNetworkUse, strHostName, strDesc, strSite, strWrittenTime, strWrittenID
									, nContentsCategory, nPurpose, nHostType, nVertical, nResolution, bIsPublic, bIsVerify, strValidationDate);

		if(strHostName.Find(SAMPLE_FILE_KEY) == 0) continue;
		if(strHostName.Find(PREVIEW_FILE_KEY) == 0) continue;
		if(strHostName.Find(TEMPORARY_SAVE_FILE_KEY) == 0) continue;

		SPackageInfo4Studio* pLocalinfo = new SPackageInfo4Studio;
		pLocalinfo->bModify = false;

		// 패키지객체 속성추가
		pLocalinfo->nContentsCategory = nContentsCategory;
		pLocalinfo->nPurpose          = nPurpose         ;
		pLocalinfo->nHostType         = nHostType        ;
		pLocalinfo->nVertical         = nVertical        ;
		pLocalinfo->nResolution       = nResolution      ;
		pLocalinfo->bIsPublic         = bIsPublic        ;
		pLocalinfo->bIsVerify         = bIsVerify        ;
		pLocalinfo->strValidationDate = strValidationDate;
		tmValidationDate.ParseDateTime(strValidationDate);
		pLocalinfo->tmValidationDate  = (tmValidationDate.GetStatus() == COleDateTime::valid ? tmValidationDate : 0);

		SPackageInfo4Studio* pServerInfo = FindPackage(strHostName, STR_ENV_SERVER);
		if(pServerInfo)
		{
			// 로컬에 존재하면 서버는 보이지 않도록 함
			pServerInfo->bShow = false;

			// 서버와 동일한 ProcID 를 부여함
			pLocalinfo->szProcID = pServerInfo->szProcID;

			// 파일 비교후 변경 여부 체크
			if(strWrittenTime.IsEmpty())
			{
				pLocalinfo->bModify = (pServerInfo->szUpdateTime != STR_ENV_EMPTY_TIME);
			}
			else
			{
				pLocalinfo->bModify = (strWrittenTime != pServerInfo->szUpdateTime);
			}

			// 패키지객체 속성추가
			pLocalinfo->nContentsCategory = pServerInfo->nContentsCategory;
			pLocalinfo->nPurpose          = pServerInfo->nPurpose         ;
			pLocalinfo->nHostType         = pServerInfo->nHostType        ;
			pLocalinfo->nVertical         = pServerInfo->nVertical        ;
			pLocalinfo->nResolution       = pServerInfo->nResolution      ;
			pLocalinfo->bIsPublic         = pServerInfo->bIsPublic        ;
			pLocalinfo->bIsVerify         = pServerInfo->bIsVerify        ;
			pLocalinfo->tmValidationDate  = pServerInfo->tmValidationDate ;
			pLocalinfo->strValidationDate = pServerInfo->strValidationDate;
		}

		pLocalinfo->szDrive = strDrive;
		pLocalinfo->tmUpdateTime = 0;
		pLocalinfo->szSiteID = strSite;
		pLocalinfo->szPackage = strHostName;
		pLocalinfo->szDescript = strDesc;
		pLocalinfo->szUpdateUser = (strWrittenID.IsEmpty()?m_szLoginID:strWrittenID);

		if(strWrittenTime.IsEmpty() && !pLocalinfo->szProcID.IsEmpty())
			pLocalinfo->szUpdateTime = STR_ENV_EMPTY_TIME;
		else
			pLocalinfo->szUpdateTime = strWrittenTime;

		AddPackage(pLocalinfo);
	}

	return 0;
}

void CEnviroment::RemoveAllPackage()
{
	POSITION pos = m_mapPackage.GetStartPosition();
	while(pos)
	{
		CString strKey;
		SPackageInfo4Studio* pInfo=NULL;
		GetEnvPtr()->m_mapPackage.GetNextAssoc(pos, strKey, (void*&)pInfo);
		if(pInfo) delete pInfo;
	}
	m_mapPackage.RemoveAll();
}

void CEnviroment::RemovePackageWithout(LPCTSTR szLocation)
{
	POSITION pos = m_mapPackage.GetStartPosition();
	while(pos)
	{
		CString strKey;
		SPackageInfo4Studio* pInfo=NULL;
		GetEnvPtr()->m_mapPackage.GetNextAssoc(pos, strKey, (void*&)pInfo);
		if(pInfo)
		{
			if(szLocation && pInfo->szDrive == szLocation) continue;
			delete pInfo;
		}

		m_mapPackage.RemoveKey(strKey);
	}
}

void CEnviroment::AddPackage(SPackageInfo4Studio* pInfo)
{
	SPackageInfo4Studio* pNewInfo = FindPackage(pInfo->szPackage, pInfo->szDrive);
	if(!pNewInfo)
	{
		pNewInfo = new SPackageInfo4Studio;
		*pNewInfo = *pInfo;

		CString strKey = pNewInfo->szDrive + _T("/") + pNewInfo->szPackage;
		strKey.MakeLower();
		m_mapPackage[strKey] = pNewInfo;
	}

	//if(strlen(pNewInfo->szSiteID) && pNewInfo->szDrive == STR_ENV_SERVER){
	//	POSITION pos = m_lsSite.Find(pNewInfo->szSiteID);
	//	if(pos == 0){
	//		m_lsSite.AddTail(pNewInfo->szSiteID);
	//	}
	//}
}

void CEnviroment::ChangeCurPackage(SPackageInfo4Studio info)
{
	SPackageInfo4Studio* pNewInfo = FindPackage(info.szPackage, info.szDrive);
	if(!pNewInfo)
	{
		pNewInfo = new SPackageInfo4Studio;
		*pNewInfo = info;

		CString strKey = pNewInfo->szDrive + _T("/") + pNewInfo->szPackage;
		strKey.MakeLower();
		m_mapPackage[strKey] = pNewInfo;

		info = *pNewInfo;
	}

	m_PackageInfo = info;
}

SPackageInfo4Studio* CEnviroment::FindPackage(CString strInPackage, CString strInDrive)
{
	CString strKey = strInDrive + _T("/") + strInPackage;
	strKey.MakeLower();

	//BOOL bFind = m_mapPackage.Lookup(strKey, info);
	SPackageInfo4Studio* pInfo = (SPackageInfo4Studio*)m_mapPackage[strKey];
	if(pInfo)
	{
		BOOL bFind = !pInfo->szPackage.IsEmpty();
		TraceLog(("FindPackage [%d][%s]", bFind, strKey));
	}

	return pInfo;
}

void CEnviroment::GetTempMaxSize(int& cx, int& cy)
{
	cx = 0;
	cy = 0;

	POSITION pos = m_lsTempRect.GetHeadPosition();
	while(pos){
		CRect rc = m_lsTempRect.GetNext(pos);
		if(cx < rc.Width())
			cx = rc.Width();
		if(cy < rc.Height())
			cy = rc.Height();
	}

	if(cx == 0)
		cx = 0x7FFFFFFF;
	if(cy == 0)
		cy = 0x7FFFFFFF;
}

void CEnviroment::GetTempMinSize(int& cx, int& cy)
{
	cx = 0x7FFFFFFF;
	cy = 0x7FFFFFFF;

	POSITION pos = m_lsTempRect.GetHeadPosition();
	for(int i = 0; i < m_lsTempRect.GetCount(); i++){
		CRect rc = m_lsTempRect.GetNext(pos);
		if(cx > rc.Width())
			cx = rc.Width();
		if(cy > rc.Height())
			cy = rc.Height();
	}

	if(cx == 0x7FFFFFFF)
		cx = 0;
	if(cy == 0x7FFFFFFF)
		cy = 0;
}

void CEnviroment::InitHostInfo(CString strSiteId, CString strHostId, CString strWhere/*=""*/)
{
#ifdef _TAO
	ubcHost aHostInfo;

	if(strSiteId.IsEmpty()) strSiteId = (m_Authority == eAuthSuper ? m_szSite : _T("*"));
	if(strHostId.IsEmpty()) strHostId = _T("*");

	if(strWhere.IsEmpty())
	{
		aHostInfo.init(strSiteId, strHostId);
	}
	else
	{
		aHostInfo.init(strSiteId, strHostId, strWhere);
	}

//	aHostInfo.printIt();

	ubcHostInfoList* list = aHostInfo.get();
	ubcHostInfoList::iterator itr;

	RemoveHost();

	for(itr=list->begin(); itr!=list->end(); itr++)
	{
		ubcHostInfo* aData  = (*itr);
		if(!aData) continue;

		SHostInfo4Studio* pInfo = new SHostInfo4Studio;

		pInfo->displayNo = 1;
		pInfo->siteId = aData->siteId.c_str();
		pInfo->hostId= aData->hostId.c_str();
		pInfo->operationalState = aData->operationalState;
		pInfo->displayCounter  = aData->displayCounter;
		pInfo->autoPackage = aData->autoSchedule1.c_str();
		pInfo->currentPackage = aData->currentSchedule1.c_str();
		pInfo->lastPackage = aData->lastSchedule1.c_str();
		pInfo->lastPackageTime = aData->lastScheduleTime1.getTime();
		pInfo->networkUse = aData->networkUse1;

		m_lsHost.AddTail(pInfo);

		if(pInfo->displayCounter != 2)
			continue;

		pInfo = new SHostInfo4Studio;

		pInfo->displayNo = 2;
		pInfo->siteId = aData->siteId.c_str();
		pInfo->hostId= aData->hostId.c_str();
		pInfo->operationalState = aData->operationalState;
		pInfo->displayCounter  = aData->displayCounter;
		pInfo->autoPackage = aData->autoSchedule2.c_str();
		pInfo->currentPackage = aData->currentSchedule2.c_str();
		pInfo->lastPackage = aData->lastSchedule2.c_str();
		pInfo->lastPackageTime = aData->lastScheduleTime2.getTime();
		pInfo->networkUse = aData->networkUse2;

		m_lsHost.AddTail(pInfo);
	}
#endif//_TAO
}

void CEnviroment::InitHostInfo(CString strPlayContentsId, CList<SHostInfo4Studio*>& lsHost)
{
#ifdef _TAO
	ubcHost aHostInfo;

	aHostInfo.init(strPlayContentsId);

//	aHostInfo.printIt();

	ubcHostInfoList* list = aHostInfo.get();
	ubcHostInfoList::iterator itr;

	for(itr=list->begin(); itr!=list->end(); itr++)
	{
		ubcHostInfo* aData  = (*itr);
		if(!aData) continue;

		SHostInfo4Studio* pInfo = new SHostInfo4Studio;

		pInfo->displayNo = 1;
		pInfo->siteId = aData->siteId.c_str();
		pInfo->hostId= aData->hostId.c_str();
		pInfo->operationalState = aData->operationalState;
		pInfo->displayCounter  = aData->displayCounter;
		pInfo->autoPackage = aData->autoSchedule1.c_str();
		pInfo->currentPackage = aData->currentSchedule1.c_str();
		pInfo->lastPackage = aData->lastSchedule1.c_str();
		pInfo->lastPackageTime = aData->lastScheduleTime1.getTime();
		pInfo->networkUse = aData->networkUse1;

		lsHost.AddTail(pInfo);

		if(pInfo->displayCounter != 2)
			continue;

		pInfo = new SHostInfo4Studio;

		pInfo->displayNo = 2;
		pInfo->siteId = aData->siteId.c_str();
		pInfo->hostId= aData->hostId.c_str();
		pInfo->operationalState = aData->operationalState;
		pInfo->displayCounter  = aData->displayCounter;
		pInfo->autoPackage = aData->autoSchedule2.c_str();
		pInfo->currentPackage = aData->currentSchedule2.c_str();
		pInfo->lastPackage = aData->lastSchedule2.c_str();
		pInfo->lastPackageTime = aData->lastScheduleTime2.getTime();
		pInfo->networkUse = aData->networkUse2;

		lsHost.AddTail(pInfo);
	}
#endif//_TAO
}

void CEnviroment::RemoveHost()
{
	POSITION pos = m_lsHost.GetHeadPosition();
	while(pos){
		SHostInfo4Studio* pInfo = m_lsHost.GetNext(pos);
		if(!pInfo) continue;
		delete pInfo;
		pInfo = NULL;
	}
	m_lsHost.RemoveAll();
}

bool CEnviroment::Connect(CSimpleFtp* pFtp, LPCTSTR szSite)
{
#ifdef _TAO
	if(!pFtp)	return false;
	ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(szSite);
	if(!aData)	return false;
	
	CString szMsg;
//	TraceLog(("Connect(%s)",aData->ipAddress.c_str()));
	return pFtp->Connect(aData->getFTPAddress(),aData->ftpId.c_str(),aData->ftpPasswd.c_str(),aData->ftpPort);
#else
	return false;
#endif//_TAO
}
// skpark 2010.10.07  사용되지 않는 코드 제외
//bool CEnviroment::GetFtpInfo(LPCTSTR szSite, SFtpInfo* pInfo)
//{
//#ifdef _TAO
//	ubcMuxData* aData = ubcMux::getInstance()->getMuxData(szSite);
//	if(!aData)	return false;
//	
//	strncpy(pInfo->szPmId, aData->pmId.c_str(), sizeof(pInfo->szPmId));
//	strncpy(pInfo->szIP, aData->ipAddress.c_str(), sizeof(pInfo->szIP));
//	strncpy(pInfo->szID, aData->ftpId.c_str(), sizeof(pInfo->szID));
//	strncpy(pInfo->szPW, aData->ftpPasswd.c_str(), sizeof(pInfo->szPW));
//	pInfo->nPort = aData->ftpPort;
//	return true;
//#else
//	return false;
//#endif//_TAO
//}

bool CEnviroment::GetFile(LPCTSTR szFile, LPCTSTR szRemote, LPCTSTR szLocal, LPCTSTR szSite)
{
	bool bRet = false;
#ifdef _TAO
	ubcMuxData* aData = NULL;
	if(szSite)
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(szSite);
	else
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(m_szSite);

	if(!aData){
		return bRet;
	}

	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	if(m_strHttpOnly.CompareNoCase("CLIENT") == 0)
	{
		CFileServiceWrap objFileSvcClient(aData->getFTPAddress_s().c_str(), aData->ftpPort);

		if(!objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str()))
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		bRet = objFileSvcClient.GetFile(CString(szRemote)+CString(szFile), CString(szLocal)+CString(szFile), 0, NULL);
		if(!bRet)
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		objFileSvcClient.Logout();
	}
	else
	{
	//	CSimpleFtp ftp;
		nsFTP::CFTPClient FtpClient;

		//CString szMsg;
		//if(!ftp.Connect(aData->ipAddress.c_str(),aData->ftpId.c_str(),aData->ftpPasswd.c_str(),aData->ftpPort)){
		//	return bRet;
		//}
		//ftp.SetTimeOut(300);
		//if(ftp.GetFile(szFile, szRemote, szLocal) != CSimpleFtp::eSuccess){
		//	return bRet;
		//}
		//if(ftp.IsTimeOut()){
		//	return bRet;
		//}

		nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
		if (!FtpClient.Login(loginInfo)) {
			return bRet;
		}

		FtpClient.SetResumeMode(false);
		FtpClient.ChangeWorkingDirectory("/");

		bRet = FtpClient.DownloadFile(
				tstring(szRemote)+tstring(szFile), 
				tstring(szLocal)+tstring(szFile),
				nsFTP::CRepresentation(nsFTP::CType::Image()), true);

		FtpClient.Logout();
	}

	// 파일 다운로드가 안됐지만 파일이 생성된경우 파일을 삭제해야 함.
	if(!bRet){
		CString szBuf = szLocal;
		szBuf += szFile;

		CFileFind ff;
		if(ff.FindFile(szBuf)){
			::DeleteFile(szBuf);
		}
		ff.Close();
	}
#endif//_TAO

	return bRet;
}

bool CEnviroment::PutFile(LPCTSTR szFile, LPCTSTR szLocal, LPCTSTR szRemote, LPCTSTR szSite)
{
	bool bRet = false;
#ifdef _TAO
	ubcMuxData* aData = NULL;
	if(szSite)
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(szSite);
	else
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(m_szSite);

	if(!aData){
		return bRet;
	}
	
	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	if(m_strHttpOnly.CompareNoCase("CLIENT") == 0)
	{
		CFileServiceWrap objFileSvcClient(aData->getFTPAddress_s().c_str(), aData->ftpPort);

		if(!objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str()))
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		bRet = objFileSvcClient.PutFile(CString(szLocal)+CString(szFile), CString(szRemote)+CString(szFile), 0, NULL);
		if(!bRet)
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		objFileSvcClient.Logout();
	}
	else
	{
	//	CSimpleFtp ftp;
		nsFTP::CFTPClient FtpClient;

		//CString szMsg;
		//if(!ftp.Connect(aData->ipAddress.c_str(),aData->ftpId.c_str(),aData->ftpPasswd.c_str(),aData->ftpPort)){
		//	return bRet;
		//}
		//ftp.SetTimeOut(300);
		//if(ftp.PutFile(szFile, szLocal, szRemote) != CSimpleFtp::eSuccess){
		//	return bRet;
		//}
		//if(ftp.IsTimeOut()){
		//	return bRet;
		//}

		nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
		if (!FtpClient.Login(loginInfo)) {
			return bRet;
		}

		FtpClient.SetResumeMode(false);
		FtpClient.ChangeWorkingDirectory("/");

		FtpClient.MakeDirectory(tstring(szRemote));
		bRet = FtpClient.UploadFile(
				tstring(szLocal)+tstring(szFile), 
				tstring(szRemote)+tstring(szFile),
				false, nsFTP::CRepresentation(nsFTP::CType::Image()), true);

		FtpClient.Logout();
	}
#endif//_TAO
	return bRet;
}

void CEnviroment::SetPathOpenFile(LPCTSTR str)
{
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);
	::WritePrivateProfileString("STUDIO", "PathOpenFile", str, szPath);
	m_szPathOpenFile = str;
}

bool CEnviroment::IsValidOpenFolder()
{
	if(m_szPathOpenFile.IsEmpty())
		return false;

	CString szFolder = m_szPathOpenFile;
	if(szFolder.Right(1)=="\\")
		szFolder += "*.*";

	CFileFind ff;
	return ff.FindFile(szFolder);
}

void CEnviroment::Load_ini()
{
	char buf[1024];
	CString szPath, szTemp;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, UBCVARS_INI);

	::GetPrivateProfileString(_T("CUSTOMER_INFO"), _T("NAME"), _T("SQISOFT"), buf, 1024, szPath);
	szTemp = buf;

	// 0001408: Customer 정보 입력을 받는다.
	// 로그인과정으로 이동
	//if(szTemp.IsEmpty())
	//{
	//	CCustomerInfoDlg dlg;
	//	if(dlg.DoModal() == IDOK)
	//	{
	//		szTemp = dlg.GetSelectCustumerInfo();
	//		::WritePrivateProfileString("CUSTOMER_INFO", "NAME", szTemp, szPath);
	//	}
	//}

	if     (szTemp == "NARSHA" ) m_Customer = eNARSHA;
	else if(szTemp == "LOTTE"  ) m_Customer = eLOTTE;
	else if(szTemp == "ADASSET") m_Customer = eADASSET; // 창일향 : 
	else if(szTemp == "KIA"    ) m_Customer = eKIA;
	else if(szTemp == "HYUNDAI") m_Customer = eHYUNDAI;
	else if(szTemp == "KMNL"   ) m_Customer = eKMNL;
	else if(szTemp == "KMCL"   ) m_Customer = eKMCL;
	else if(szTemp == "KPOST"  ) m_Customer = eKPOST; // 우정국
	else if(szTemp == "UBGOLF"  ) m_Customer = eUBGOLF; // 어반트러스트 골프
	else                         m_Customer = eSQISOFT;

	m_strCustomer = szTemp;

	::GetPrivateProfileString(_T("STUDIO"), _T("AUTO_ENCODING"), _T("0"), buf, 1024, szPath);
	m_bAutoEncoding = bool(_ttoi(buf));

	::GetPrivateProfileString(_T("STUDIO"), _T("TOTAL_CONTENTS"), _T("0"), buf, 1024, szPath);
	m_TotalContentsSize = _ttoi64(buf);

	::GetPrivateProfileString(_T("STUDIO"), _T("PathOpenFile"), _T(""), buf, 1024, szPath);
	m_szPathOpenFile = buf;

	::GetPrivateProfileString(_T("STUDIO"), _T("MAX_CONTENTS_SIZE"), _T("0"), buf, 1024, szPath);
	m_lMaxContentsSize = _ttoi64(buf);

	::GetPrivateProfileString(_T("STUDIO"), _T("MAX_CONTENTS_COUNT"), _T("0"), buf, 1024, szPath);
	m_nMaxContentsCount = _ttoi(buf);

	::GetPrivateProfileString(_T("STUDIO"), _T("MAX_PLAY_TEMPLATE_COUNT"), _T("0"), buf, 1024, szPath);
	m_nMaxPlayTemplateCount = _ttoi(buf);

	::GetPrivateProfileString(_T("ROOT"), _T("USE_TV"), _T("0"), buf, 1024, szPath);
	m_bUseTV = (_ttoi(buf) == 1);

	// 정시플레이콘텐츠은 PE버젼 이거나 USE_TIME_BASE_SCHEDULE=1 인경우 활성화 되도록 한다
	::GetPrivateProfileString(_T("ROOT"), _T("USE_TIME_BASE_SCHEDULE"), _T("0"), buf, 1024, szPath);
	// 2011.04.18 USE_TIME_BASE_SCHEDULE 이 명시적으로 0으로 설정된 EE버젼의 경우만 나오지 않도록 변경
	m_bUseTimeBasePlayContents = (_ttoi(buf) == 1 || m_Edition == eStudioPE);

	// 0000782: 부모-자식 스케줄 기능을 수정한다.
	m_bUseClickPlayContents = (1 == ::GetPrivateProfileInt(_T("ROOT"), _T("USE_CLICK_SCHEDULE"), 0, szPath));
	// skpark same_size_file_problem
	m_bUseTimeCheck = (1 == ::GetPrivateProfileInt(_T("ROOT"), _T("USE_TIME_CHECK"), 0, szPath));

	// 0001128: Studio 임시 저장 및 복구
	m_bUseAutoTempSave = (1 == ::GetPrivateProfileInt(_T("ROOT"), _T("USE_AUTO_TEMP_SAVE"), 0, szPath));
	m_nAutoTempSaveInterval = ::GetPrivateProfileInt(_T("ROOT"), _T("AUTO_TEMP_SAVE_INTERVAL"), 0, szPath);

	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	::GetPrivateProfileString(_T("ROOT"), _T("HTTP_ONLY"), _T(""), buf, 1024, szPath);
	m_strHttpOnly = buf;
}

void CEnviroment::Load_ee_ini()
{
	char buf[1024];
	CString szPath, szTemp;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, ENVIROMENT_INI);

	::GetPrivateProfileString(_T("STUDIO"), _T("TYPE"), _T("UBC"), buf, 1024, szPath);
	szTemp = buf;
	if(szTemp == _T("UBC"))
		m_StudioType = eUBCType;
	else if(szTemp == _T("USTB"))
		m_StudioType = eUSTBType;
	else
		m_StudioType = eUBCType;

	::GetPrivateProfileString(_T("STUDIO"), _T("TOTAL_CONTENTS"), _T("0"), buf, 1024, szPath);
	m_TotalContentsSize = _ttoi64(buf);
}

void CEnviroment::Load_pe_ini()
{
	CString szPath;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, ENVIROMENT_INI);

	m_StudioType = eUBCType;
	m_TotalContentsSize = 0;
}

bool CEnviroment::GetFileSize(CString szPath, ULONGLONG& ulSize)
{
	CFileFind ff;
	bool bRet = true;

	if(ff.FindFile(szPath)){
		ff.FindNextFile();
		ulSize = ff.GetLength();
	}else{
		ulSize = 0;
		bRet = false;
	}

	ff.Close();
	return bRet;
}

SYSTEMTIME CEnviroment::CheckTime(SYSTEMTIME& stSrc)
{
	if(stSrc.wYear <= 1970){
		stSrc.wYear = 1970;
		if(stSrc.wMonth == 1 && stSrc.wDay == 1 && stSrc.wHour < 9)
			stSrc.wHour = 9;
	}
	return stSrc;
}

bool CEnviroment::GetAutoUpdate()
{
	char buf[1024];
	CString szPath;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, UBCVARS_INI);

	::GetPrivateProfileString(_T("ROOT"), _T("AutoUpdateFlag"), _T("1"), buf, 1024, szPath);
	int nValue = atoi(buf);

	return (nValue != 0);
}

void CEnviroment::SetAutoUpdate(bool bValue)
{
	char buf[1024];
	CString szPath, szTemp;
	szPath.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, UBCVARS_INI);

	::WritePrivateProfileString(_T("ROOT"), _T("AutoUpdateFlag"), ToString((int)bValue), szPath);
}