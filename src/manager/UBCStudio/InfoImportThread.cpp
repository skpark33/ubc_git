// InfoImportThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "InfoImportThread.h"

#include "UBCStudioDoc.h"
#include "HostSelectDlg.h"


// CInfoImportThread

IMPLEMENT_DYNCREATE(CInfoImportThread, CWinThread)

CInfoImportThread::CInfoImportThread()
:	m_pParent(NULL)
,	m_pParentDoc(NULL)
,	m_strDrive(_T(""))
,	m_strHost(_T(""))
{
	m_bInfoMsg = true;
}

CInfoImportThread::~CInfoImportThread()
{
}

BOOL CInfoImportThread::InitInstance()
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CInfoImportThread::ExitInstance()
{
	::CoUninitialize();

	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CInfoImportThread, CWinThread)
END_MESSAGE_MAP()


// CInfoImportThread 메시지 처리기입니다.

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Set parent params \n
/// @param (void*) pParent : (in) thread owner window pointer
/// @param (void*) pParentDoc : (in) thread owner window's document pointer
/// @param (CString) strDrive : (in) import 하려는 host 정보가 있는 드라이브
/// @param (CString) strHost : (in) import 하려는 host 이름
/////////////////////////////////////////////////////////////////////////////////
void CInfoImportThread::SetThreadParam(void* pParent, void* pParentDoc, CString strDrive, CString strHost)
{
	m_pParent = pParent;
	m_pParentDoc = pParentDoc;
	m_strDrive = strDrive;
	m_strHost = strHost;
}

int CInfoImportThread::Run()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CUBCStudioDoc* pParentDoc = (CUBCStudioDoc*)m_pParentDoc;
	CWnd* pParent = (CWnd*)m_pParent;
	
	bool bRet = pParentDoc->Load(m_strDrive, m_strHost);

	if(m_bInfoMsg){
		pParent->PostMessage(WM_INFOIMPORT_COMPELETE, (WPARAM)bRet, 0);
	}

	ExitThread(0);
	return 0;
}
