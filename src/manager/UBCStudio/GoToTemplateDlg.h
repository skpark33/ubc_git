#pragma once

#include "afxwin.h"
#include "resource.h"       // main symbols
#include "DataContainer.h"
#include "UBCStudioDoc.h"
#include "EditEx.h"

// CGoToTemplateDlg 대화 상자입니다.

class CGoToTemplateDlg : public CDialog
{
	DECLARE_DYNAMIC(CGoToTemplateDlg)

	CUBCStudioDoc*  m_pDocument;
	CString			m_strTarTemplateId;
	int				m_nDuration;
	CString			m_strSelfTemplateId;

public:
	CGoToTemplateDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CGoToTemplateDlg();

	void			SetDocument(CUBCStudioDoc* pDoc) { m_pDocument=pDoc; };
	CUBCStudioDoc*	GetDocument() { return m_pDocument; }
	void			SetParam(CString strTarTemplateId, int nDuration, CString strSelfTemplateId);
	void			GetParam(CString& strTarTemplateId, int& nDuration);

	CComboBox		m_cbTemplate;
	CEditEx			m_ebDuration;
	CSpinButtonCtrl m_spDuration;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_GOTO_TEMPLATE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
};
