// WaitingListCtrl.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "etc.h"
#include "resource.h"
#include "WaitingListCtrl.h"


#define		TIMER_WAITING_ID		12345
#define		TIMER_WAITING_TIME		25


// CWaitingListCtrl

IMPLEMENT_DYNAMIC(CWaitingListCtrl, CListCtrl)

HRESULT ReadImgResource(UINT nResID, LPCTSTR lpType, CImage& img)
{
	HMODULE hModule= AfxGetResourceHandle();
	//HMODULE hModule = ::GetModuleHandle(_T("ContentsManagerDll.dll"));
	HRSRC src = FindResource(hModule, MAKEINTRESOURCE(nResID), lpType);
	if(src == NULL) return E_FAIL;
	HGLOBAL hGlobal = LoadResource(hModule, src);
	if(hGlobal == NULL) return E_FAIL;
	void* resData = LockResource(hGlobal);
	int size = SizeofResource(hModule,src);

	IStream * pStream = NULL;
	ULONG dwReadWrite = 0;
	CreateStreamOnHGlobal(NULL, TRUE, &pStream);    // IStream을 만들어서 적기
	pStream->Write(resData, size, &dwReadWrite);

#if 1
	// CImage ver.
	//CImage img;
	return img.Load(pStream);
	/*
	HDC memDC;
	memDC = CreateCompatibleDC(hDC);
	HBITMAP hBitmap = CreateCompatibleBitmap(hDC,img.GetWidth(),img.GetHeight());
	SelectObject(memDC,hBitmap);
	img.BitBlt(memDC,0,0,img.GetWidth(),img.GetHeight(),0,0,SRCCOPY);
	DeleteDC(memDC);
	img.Destroy();
	return hBitmap;
	*/
#else
	// GDI++ ver.
	HBITMAP hBmp;
	Bitmap *bm = Bitmap::FromStream(pStream);
	bm->GetHBITMAP(RGB(0,0,0), &hBmp);
	return hBmp;
#endif
}

CWaitingListCtrl::CWaitingListCtrl()
: m_nSortCol (0)
, m_bAscend (false)
{
	//	HINSTANCE oldHand = AfxGetResourceHandle();
	//	HMODULE module = ::GetModuleHandle(_T("ContentsManagerDll.dll"));
	//	AfxSetResourceHandle(module);

	//HRESULT result = m_img.Load(_T("process-working.png"));
	HRESULT result = ReadImgResource(IDB_PNG_PROCESSING, _T("PNG"), m_img);
	m_nIndex = 1;
	m_bTimer = false;

	//	AfxSetResourceHandle(oldHand);
}

CWaitingListCtrl::~CWaitingListCtrl()
{
}


BEGIN_MESSAGE_MAP(CWaitingListCtrl, CListCtrl)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_NOTIFY_REFLECT_EX(LVN_COLUMNCLICK, OnLvnColumnclick)
END_MESSAGE_MAP()



// CWaitingListCtrl 메시지 처리기입니다.



void CWaitingListCtrl::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CListCtrl::OnPaint()을(를) 호출하지 마십시오.

	CWnd::DefWindowProc(WM_PAINT, (WPARAM)dc.GetSafeHdc(), 0);

	if(m_bTimer)
	{
		CHeaderCtrl* header = GetHeaderCtrl();
		CRect rcHeader(0,0,0,0);
		if(header) header->GetClientRect(rcHeader);

		CRect rcClient, rcWindow;
		GetClientRect(rcClient);
		GetWindowRect(rcWindow);

		int x = m_nIndex;
		int y = x / 8;
		x %= 8;

		dc.FillSolidRect(
			rcClient.left + (rcClient.Width()-32)/2,
			rcClient.top + ((rcClient.Height()-rcHeader.Height())-32)/2 + rcHeader.Height(),
			32, 32,
			RGB(255,255,255));

		if(!m_img.IsNull())
		{
			m_img.Draw(
				dc.GetSafeHdc(),
				rcClient.left + (rcClient.Width()-32)/2,
				rcClient.top + ((rcClient.Height()-rcHeader.Height())-32)/2 + rcHeader.Height(),
				//rcClient.top + (rcClient.Height()-32)/2,
				32, 32,
				x*32, y*32,
				32, 32
				);
		}
	}
}

void CWaitingListCtrl::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CListCtrl::OnTimer(nIDEvent);

	if(TIMER_WAITING_ID == nIDEvent)
	{
		m_nIndex++;
		if(m_nIndex >= 32) m_nIndex = 1;
		Invalidate(FALSE);

		if(m_bTimer)
			SetTimer(TIMER_WAITING_ID, TIMER_WAITING_TIME, NULL);
		else
			KillTimer(TIMER_WAITING_ID);
	}
}

void CWaitingListCtrl::StartWaitingIcon()
{
	m_bTimer = true;
	SetTimer(TIMER_WAITING_ID, TIMER_WAITING_TIME, NULL);
}

void CWaitingListCtrl::StopWaitingIcon()
{
	m_bTimer = false;
	Invalidate();
}

BOOL CWaitingListCtrl::OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int	nSortCol = pNMLV->iSubItem;

	HDITEM hdi;
	hdi.mask = HDI_FORMAT;
	if(m_nSortCol >= 0)
	{
		GetHeaderCtrl()->GetItem(m_nSortCol, &hdi);
		hdi.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);
		GetHeaderCtrl()->SetItem(m_nSortCol, &hdi);
	}

	// 정렬한다.
	SORT_PARAM	sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = nSortCol;
	sort_param.bAscend = (m_nSortCol != nSortCol) ? true : !m_bAscend;

	// 현재 정렬기준 컬럼과 정렬모드를 저장한다.
	m_nSortCol = nSortCol;
	m_bAscend = sort_param.bAscend;

	GetHeaderCtrl()->GetItem(m_nSortCol, &hdi);
	hdi.fmt &= ~(HDF_SORTUP | HDF_SORTDOWN);
	hdi.fmt |= (m_bAscend ? HDF_SORTUP : HDF_SORTDOWN);
	GetHeaderCtrl()->SetItem(m_nSortCol, &hdi);

	SortItems(&SortColumn, (LPARAM)&sort_param);

	return TRUE;
}

BOOL CWaitingListCtrl::Sort(int col, bool ascend)
{
	if(col<0)
	{
		col = m_nSortCol;
		ascend = m_bAscend;
	}
	else
	{
		m_nSortCol = col;
		m_bAscend = ascend;
	}

	//
	SORT_PARAM	sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = col;
	sort_param.bAscend = ascend;

//	m_checkHeadCtrl.SetSortArrow( col );

//    return SortItems(&SortColumn, (LPARAM)&sort_param);
	return SortItems(&SortColumn, (LPARAM)&sort_param);
}

int CALLBACK CWaitingListCtrl::SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam)
{
	SORT_PARAM*	ptrSortParam = (SORT_PARAM *)lSortParam;
	CListCtrl*	pListCtrl = (CListCtrl *)CWnd::FromHandle(ptrSortParam->hWnd);

	LVFINDINFO	lvFind;
	lvFind.flags = LVFI_PARAM;
	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	//lvFind2.flags = LVFI_PARAM;
	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	CString strItem1 = pListCtrl->GetItemText(nIndex1, ptrSortParam->nCol);
	CString	strItem2 = pListCtrl->GetItemText(nIndex2, ptrSortParam->nCol);

	CString		strItemPadding1;
	CString		strItemPadding2;

	//switch(ptrSortParam->sortType)
	//{
	//default:
	//case SORTTYPE_STRING:
		strItemPadding1 = strItem1;
		strItemPadding2 = strItem2;
	//	break;

	//case SORTTYPE_NUMBER:
	//	{
	//		strItem1.Replace(_T(","), _T(""));
	//		strItem2.Replace(_T(","), _T(""));

	//		double i1 = _tstof(strItem1);
	//		double i2 = _tstof(strItem2);

	//		strItemPadding1.Format(_T("%020.5f"), i1);
	//		strItemPadding2.Format(_T("%020.5f"), i2);
	//	}
	//	break;
	//}

	if (ptrSortParam->bAscend == true)
		return strItemPadding1.Compare(strItemPadding2);
	else
		return strItemPadding2.Compare(strItemPadding1);
}
