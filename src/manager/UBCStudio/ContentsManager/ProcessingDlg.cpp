// ProcessingDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "etc.h"
#include "resource.h"
#include "ProcessingDlg.h"

#include "ThreadFunc.h"
#include "Enviroment.h"

CMutex		g_mutexThreadProcessing;
int			g_nThreadProcessing = 0;

extern CString	WEB_SERVER_IP;

// CProcessingDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CProcessingDlg, CDialog)

CProcessingDlg::CProcessingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProcessingDlg::IDD, pParent)
	, m_pThread ( NULL )
	, m_listCurrent ( NULL )
	, m_bUpdatedListDetached ( false )
	, m_listUpdated ( NULL )
	, m_listCopyFromPrivateTmp ( NULL )
{
	if(WEB_SERVER_IP.GetLength() == 0)
	{
		char szBuf[1000] = {0};
		CString szPath;
		szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

		::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", szBuf, sizeof(szBuf), szPath);
		WEB_SERVER_IP = szBuf;
	}
}

CProcessingDlg::~CProcessingDlg()
{

	if(m_bUpdatedListDetached == false && m_listUpdated != NULL)
	{
		for(int i=0; i<m_listUpdated->GetCount(); i++)
		{
			CONTENTS_INFO* info = m_listUpdated->GetAt(i);
			delete info;
		}
		m_listUpdated->RemoveAll();
		delete m_listUpdated;
		m_listUpdated = NULL;
	}
}

void CProcessingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_PROCESSING_STATUS, m_stcStatus);
	DDX_Control(pDX, IDC_LIST_COMMON_CONTENTS, m_lcContents);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CProcessingDlg, CDialog)
	ON_WM_TIMER()
	ON_MESSAGE(WM_CHANGE_STATUS, OnChangeStatus)
	ON_MESSAGE(WM_CHANGE_PROCESSING, OnChangeProcessing)
	ON_MESSAGE(WM_COMPLETED_ALL_PROCESSING, OnCompletedAllProcessing)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_COMMON_CONTENTS, OnLvnKeydownListCommonContents)
END_MESSAGE_MAP()


// CProcessingDlg 메시지 처리기입니다.

BOOL CProcessingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//
	m_lcContents.SetExtendedStyle(m_lcContents.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	LISTCTRL_COLUMN_INFO info[COLUMN_COUNT] = {
		//{ "", idx, width, align },
		{ LoadStringById(IDS_CM_LC_COLUMN_CONTENTS_NAME), COLUMN_CNAME,    100, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_FILENAME), COLUMN_FNAME,    150, LVCFMT_LEFT },
		//{ LoadStringById(IDS_CM_LC_COLUMN_LOCATION), COLUMN_LOCATION, 150, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_FILESIZE), COLUMN_SIZE,     100, LVCFMT_RIGHT },
		{ LoadStringById(IDS_CM_LC_COLUMN_STATUS),   COLUMN_STATUS,   100, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_RESULT),   COLUMN_RESULT,   150, LVCFMT_LEFT },
	};

	InitListCtrlColumn(m_lcContents, info, COLUMN_COUNT);

	m_lcContents.SetProgressBarCol(COLUMN_STATUS);
	m_lcContents.InitHeader(IDB_LIST_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcContents.SetSortEnable(false);

	//
	m_btnOK.EnableWindow(FALSE);
	m_btnCancel.EnableWindow(FALSE);

	SetTimer(TIMER_INIT_ID, TIMER_INIT_TIME, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CProcessingDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnOK();
}

void CProcessingDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnCancel();

	m_btnOK.EnableWindow(TRUE);
	m_btnCancel.EnableWindow(FALSE);

	DeleteThread();
}

void CProcessingDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	DeleteThread();

	if(nIDEvent == TIMER_INIT_ID)
	{
		KillTimer(TIMER_INIT_ID);

		GUARD(g_mutexThreadProcessing);

			AFX_THREADPROC thread_func = NULL;
			m_listCurrent = NULL;

			COMMAND_THREAD_PARAM* param = new COMMAND_THREAD_PARAM;
			param->hParentWnd = GetSafeHwnd();
			param->nThreadSerial = g_nThreadProcessing;
			param->strSiteID = m_strCSiteID;
			param->strProgramID = m_strProgramID;

			if(m_listUpload.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_UPLOADING));
				thread_func = UploadThreadFunc;
				param->pListObjList = m_listCurrent= &m_listUpload;
			}
			else if(m_listDownload.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_DOWNLOADING));
				thread_func = DownloadThreadFunc;
				param->pListObjList = m_listCurrent= &m_listDownload;
			}
			else if(m_listCopyToPrivate.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_COPY_TO_PACKAGE));
				thread_func = CopyToPrivateThreadFunc;
				param->pListObjList = m_listCurrent= &m_listCopyToPrivate;

				m_listUpdated = new CONTENTS_INFO_LIST;
			}
			else if(m_listCopyFromPrivate.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_COPY_TO_COMMON_CONTENTS));
				thread_func = CopyFromPrivateThreadFunc;
				param->pListObjList = m_listCopyFromPrivateTmp;
				m_listCurrent = &m_listCopyFromPrivate;
			}
			else if(m_listModify.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_MODIFY));
				thread_func = ModifyThreadFunc;
				param->pListObjList = m_listCurrent = &m_listModify;
			}
			else if(m_listVerify.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_VERIFY));
				thread_func = VerifyThreadFunc;
				param->pListObjList = m_listCurrent = &m_listVerify;
			}
			else if(m_listEncode.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_ENCODE));
				thread_func = EncodeThreadFunc;
				param->pListObjList = m_listCurrent = &m_listEncode;
			}
			else if(m_listBackup.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_BACKUP));
				thread_func = BackupThreadFunc;
				param->pListObjList = m_listCurrent = &m_listBackup;
			}
			else if(m_listRestore.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_RESTORE));
				thread_func = RestoreThreadFunc;
				param->pListObjList = m_listCurrent = &m_listRestore;
			}
			else if(m_listCompress.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_COMPRESS));
				thread_func = CompressThreadFunc;
				param->pListObjList = m_listCurrent = &m_listCompress;
			}
			else if(m_listDecompress.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_DECOMPRESS));
				thread_func = DecompressThreadFunc;
				param->pListObjList = m_listCurrent = &m_listDecompress;
			}
			else if(m_listDelete.GetCount() > 0)
			{
				m_stcStatus.SetWindowText(LoadStringById(IDS_CM_PROCESSING_TYPE_DELETE));
				thread_func = DeleteThreadFunc;
				param->pListObjList = m_listCurrent = &m_listDelete;
			}

			if(m_listCurrent && thread_func && param)
			{
				for(int i=0; i<m_listCurrent->GetCount(); i++)
				{
					CContentsObject* obj = (CContentsObject*)m_listCurrent->GetAt(i);

					obj->processingResultValue = 0;

					int idx = m_lcContents.GetItemCount();
					m_lcContents.InsertItem(idx, _T(""));

					m_lcContents.SetItemText(idx, COLUMN_CNAME, obj->strContentsName);
					m_lcContents.SetItemText(idx, COLUMN_FNAME, obj->strFilename);
					//m_lcContents.SetItemText(idx, COLUMN_LOCATION, obj->strServerLocation);
					m_lcContents.SetItemText(idx, COLUMN_SIZE, ToMoneyTypeString(obj->nFilesize));
					m_lcContents.SetItemText(idx, COLUMN_STATUS, _T("0"));
					m_lcContents.SetItemText(idx, COLUMN_RESULT, _T(""));

					m_lcContents.SetItemData(idx, (DWORD)obj);
				}

				//
				m_pThread = ::AfxBeginThread(thread_func, param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
				m_pThread->m_bAutoDelete = TRUE;
				m_pThread->ResumeThread();

				//
				m_btnOK.EnableWindow(FALSE);
				m_btnCancel.EnableWindow(TRUE);
			}
			else
			{
				OnOK();
			}
		END_GUARD;
	}
}

BOOL CProcessingDlg::DeleteThread()
{
	if(m_pThread)
	{
		CWaitMessageBox wait;
		GUARD(g_mutexThreadProcessing);
			g_nThreadProcessing++;
		END_GUARD;

		try
		{
			DWORD dwExitCode;

			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, 3000);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}

			//delete m_pThread;
		}
		catch(...)
		{
		}

		m_pThread = NULL;
		
	}

	return TRUE;
}

LRESULT CProcessingDlg::OnChangeStatus(WPARAM wParam, LPARAM lParam)
{
	int idx = wParam;
	if(m_listCurrent==NULL || idx >= m_listCurrent->GetCount()) return 0;

	CContentsObject* obj = m_listCurrent->GetAt(idx);

	switch(lParam)
	{
	default:
	case eUnknownError:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_UNKNOWN_ERROR));
		break;
	case eNowFTPProcessing:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_FTP_PROCESSING));
		break;
	case eCompleted:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_COMPLETED));
		m_lcContents.SetItemText(idx, COLUMN_STATUS, _T("100"));

		if(m_listUpdated != NULL)
		{
			CONTENTS_INFO* info = new CONTENTS_INFO;
			*info = *(CONTENTS_INFO*)obj;
			m_listUpdated->Add(info);
		}
		break;
	case eAlreadyContentsExist:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ALREADY_CONTENTS_EXIST));
		break;
	case eAlreadyContentsVerified:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ALREADY_CONTENTS_VERIFIED));
		break;
	case eErrorOpenLocalFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_OPEN_LOCAL_FILE));
		break;
	case eErrorCreateRemoteFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CREATE_REMOTE_FILE));
		break;
	case eErrorConnectRemoteServer:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CONNECT_REMOTE_SERVER));
		break;
	case eErrorFTPProcessing:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_FTP_PROCESSING));
		break;
	case eErrorCreateLocalFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CREATE_LOCAL_FILE));
		break;
	case eErrorOpenRemoteFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_OPEN_REMOTE_FILE));
		break;
	case eErrorNowWorking:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_NOW_WORKING));
		break;
	case eErrorNotExist:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_NOT_EXIST));
		break;
	case eErrorCantDo:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_CANT_DO));
		break;
	case eErrorUsingAnotherPackage:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_USING_ANOTHER_PACKAGE));
		break;
	case eErrorNoContentsFile:
		m_lcContents.SetItemText(idx, COLUMN_RESULT, LoadStringById(IDS_CM_STATUS_MSG_ERROR_NO_CONTENTS_FILE));
		break;
	}
	return 0;
}

LRESULT CProcessingDlg::OnChangeProcessing(WPARAM wParam, LPARAM lParam)
{
	if(wParam >= m_lcContents.GetItemCount()) return 0;

	int idx = wParam;

	m_lcContents.SetItemText(idx, COLUMN_STATUS, ToString(lParam));

	return 0;
}

LRESULT CProcessingDlg::OnCompletedAllProcessing(WPARAM wParam, LPARAM lParam)
{
	OnCancel();

	if(m_listCurrent)
	{
		bool all_complete = true;
		for(int i=0; i<m_listCurrent->GetCount(); i++)
		{
			CContentsObject* obj = m_listCurrent->GetAt(i);
			int result = obj->processingResultValue;

			if(result != eCompleted)
			{
				all_complete = false;
				break;
			}
		}

		if(all_complete)
		{
			UbcMessageBox(CM_NOTIFY_SUCCESS_ALL_PROCESSING, MB_ICONINFORMATION);
			OnOK();
		}
		else
		{
			UbcMessageBox(LoadStringById(IDS_CM_NOTIFY_MSG_ERROR_IN_PROCESS_LIST), MB_ICONWARNING);
		}
	}

	return 0;
}

void CProcessingDlg::AddCopyFromPrivateContents(CONTENTS_INFO* info)
{
	CContentsObject* obj = new CContentsObject;

	*(CONTENTS_INFO*)obj = *info;

	m_listCopyFromPrivate.Add(obj);

	if(m_listCopyFromPrivateTmp == NULL)
		m_listCopyFromPrivateTmp = new CContentsObjectList;
	m_listCopyFromPrivateTmp->Add(obj);
}

void CProcessingDlg::AddCopyFromPrivateContents(CArray<CONTENTS_INFO*> &info_list)
{
	for(int i=0; i<info_list.GetCount(); i++)
	{
		CONTENTS_INFO* info = info_list.GetAt(i);

		CContentsObject* obj = new CContentsObject;

		*(CONTENTS_INFO*)obj = *info;

		m_listCopyFromPrivate.Add(obj);

		if(m_listCopyFromPrivateTmp == NULL)
			m_listCopyFromPrivateTmp = new CContentsObjectList;
		m_listCopyFromPrivateTmp->Add(obj);
	}
}

void CProcessingDlg::OnLvnKeydownListCommonContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pLVKeyDow->wVKey == VK_F5)
	{
		CContentsObjectList obj_list;

		for(int i=0; i<m_lcContents.GetItemCount(); i++)
		{
			CContentsObject* obj = (CContentsObject*)m_lcContents.GetItemData(i);

			if(obj->processingResultString.GetLength() > 0)
			{
				UbcMessageBox(obj->processingResultString);
			}
		}
	}
}
