// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// ContentsManagerDll.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

#include "etc.h"



extern CMutex	g_mutexThreadMain;
extern int		g_nThreadMain;


BOOL RequestGet(LPCTSTR lpUrl, CString& strOutMsg)
{
	BOOL bRet = FALSE;

	DWORD dwSearviceType;
	CString strServer, strObject;
	INTERNET_PORT nPort;

	if(!AfxParseURL(lpUrl, dwSearviceType, strServer, strObject, nPort))
	{
		ASSERT(0);
		return bRet;
	}

	CString strReciveMessage = _T("");
	DWORD dwReadSize;
	CString strHeader = _T("Accept: */*\r\n");

	CInternetSession Session;
	CHttpConnection* pServer = NULL;
	CHttpFile *pFile = NULL;

	try
	{
		pServer = Session.GetHttpConnection(strServer, nPort);
		pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_GET, strObject);

		pFile->SendRequest(strHeader, (LPVOID)(LPCTSTR)strHeader, strHeader.GetLength());
	}
	catch (CInternetException* e)
	{
		TCHAR szError[255];
		e->GetErrorMessage(szError,255);
		e->Delete();
		return bRet;
	}

	char szLen[32]="";
	DWORD dwLenSize = sizeof(szLen);
	pFile->QueryInfo( HTTP_QUERY_CONTENT_LENGTH, szLen, &dwLenSize );
	DWORD length = atoi( szLen);

	//int length = pFile->GetLength();
	TCHAR* buf = new TCHAR[length+1];
	::ZeroMemory(buf, sizeof(TCHAR)*(length+1));
	dwReadSize = pFile->Read(buf, length);
	strReciveMessage = buf;
	delete[]buf;

	if(dwReadSize != length)
		bRet = FALSE;
	else
		bRet = TRUE;

	strOutMsg = strReciveMessage;

	if(pFile)
	{
		pFile->Close();
		delete pFile;
		pFile = NULL;
	}

	if(pServer)
	{
		pServer->Close();
		delete pServer;
		pServer = NULL;
	}

	Session.Close();

	return bRet;
}

BOOL RequestPost(LPCTSTR lpUrl, CString& strSendMsg, CString& strOutMsg)
{
	BOOL bRet = FALSE;

	DWORD dwSearviceType;
	CString strServer, strObject;
	INTERNET_PORT nPort;

	if(!AfxParseURL(lpUrl, dwSearviceType, strServer, strObject, nPort))
	{
		ASSERT(0);
		return bRet;
	}

	CString strReciveMessage("");
	DWORD dwReadSize;

	CInternetSession Session;
	CHttpConnection* pServer = NULL;
	CHttpFile *pFile = NULL;

	CString strHeader = "Content-Type: application/x-www-form-urlencoded\r\n";
	try
	{
		pServer = Session.GetHttpConnection(strServer, nPort);
		pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_POST, strObject);

		pFile->SendRequest(strHeader, (LPVOID)(LPCTSTR)strSendMsg, strSendMsg.GetLength());
	}
	catch (CInternetException* e)
	{
		TCHAR szError[255];
		e->GetErrorMessage(szError,255);
		e->Delete();
		return bRet;
	}

	char szLen[32]="";
	DWORD dwLenSize = sizeof(szLen);
	pFile->QueryInfo( HTTP_QUERY_CONTENT_LENGTH, szLen, &dwLenSize );
	DWORD length = atoi( szLen);

	//int length = pFile->GetLength();
	TCHAR* buf = new TCHAR[length+1];
	::ZeroMemory(buf, sizeof(TCHAR)*(length+1));
	dwReadSize = pFile->Read(buf, length);
	strReciveMessage = buf;
	delete[]buf;

	if(dwReadSize != (DWORD)strReciveMessage.GetLength())
		bRet = FALSE;
	else
		bRet = TRUE;

	strOutMsg = strReciveMessage;

	if(pFile)
	{
		pFile->Close();
		delete pFile;
		pFile = NULL;
	}

	if(pServer)
	{
		pServer->Close();
		delete pServer;
		pServer = NULL;
	}

	Session.Close();

	return bRet;
}

void InitListCtrlColumn(CListCtrl& lc, LISTCTRL_COLUMN_INFO* pColumnInfo, int count)
{
	if(lc.GetSafeHwnd() == NULL) return;

	for(int i=0; i<count; i++)
		lc.InsertColumn(i, _T(""));

	LVCOLUMN col;
	col.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_FMT;
	col.cchTextMax = 255;

	for(int i=0; i<count; i++)
	{
		col.pszText = pColumnInfo[i].text.GetBuffer();
		col.cx = pColumnInfo[i].width;
		col.fmt = pColumnInfo[i].align;
		lc.SetColumn(pColumnInfo[i].idx, &col);
	}
}

CString ConvertToURLString(CString str, bool bReturnNullString)
{
	if(str.GetLength() == 0) return (bReturnNullString ? _T("(null)") : _T(""));
	str.Replace(_T("%"), _T("%25"));
	str.Replace(_T(" "), _T("%20"));
	str.Replace(_T("&"), _T("%26"));
	str.Replace(_T("="), _T("%3d"));
	str.Replace(_T("+"), _T("%2b"));
	str.Replace(_T("?"), _T("%3f"));
	return str;
}

/*
void CContentsObject::AddAttribute(LPCTSTR lpszKey, LPCTSTR lpszValue)
{
	m_mapAttributes.SetAt(lpszKey, lpszValue);
}

void CContentsObject::AddAttribute(LPCTSTR lpszKey, int nValue)
{
	TCHAR buf[32] = {0};
	_stprintf(buf, _T("%d"), nValue);
	m_mapAttributes.SetAt(lpszKey, buf);
}

void CContentsObject::AddAttribute(LPCTSTR lpszKey, CTime& tValue)
{
	m_mapAttributes.SetAt(lpszKey, tValue.Format(_T("%Y/%m/%d %H:%M:%S")));
}

CString CContentsObject::GetAttributeString(LPCTSTR lpszKey)
{
	CString value = _T("");
	m_mapAttributes.Lookup(lpszKey, value);
	return value;
}

int CContentsObject::GetAttributeInt(LPCTSTR lpszKey)
{
	CString value = _T("0");
	m_mapAttributes.Lookup(lpszKey, value);
	return _ttoi(value);
}

CTime CContentsObject::GetAttributeDataTime(LPCTSTR lpszKey)
{
	CString value = _T("");
	m_mapAttributes.Lookup(lpszKey, value); // 0123/56/89 12:45:78
	if(value.GetLength() == 19 && value[4]==_T('/') && value[7]==_T('/') && value[13]==_T(':') && value[16]==_T(':'))
		return CTime(_ttoi((LPCTSTR)value), _ttoi((LPCTSTR)value+5), _ttoi((LPCTSTR)value+8), _ttoi((LPCTSTR)value+11), _ttoi((LPCTSTR)value+14), _ttoi((LPCTSTR)value+17));
	return CTime(0);
}
*/