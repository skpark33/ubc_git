
#pragma once


/////////////////////////////////////////////////////////////////
typedef struct {
	HWND		hParentWnd;
	int			nThreadSerial;
	CString		strSiteID;
	CString		strUserID;
	CString		strPassword;
} LOGIN_THREAD_PARAM;

UINT LoginThreadFunc(LPVOID pParam);

/////////////////////////////////////////////////////////////////
typedef struct {
	HWND		hParentWnd;
	int			nThreadSerial;
	CString		strContentsId;
	int 		nContentsType;
	CString		strKeyword;
	CString		strContentsState;
} GET_COMMON_LIST_THREAD_PARAM;

UINT GetCommonListThreadFunc(LPVOID pParam);

/////////////////////////////////////////////////////////////////
typedef struct {
	HWND		hParentWnd;
	int			nThreadSerial;
	CString		strSiteID;
	CString		strProgramID;
	CContentsObjectList*	pListObjList;
} COMMAND_THREAD_PARAM;

UINT UploadThreadFunc(LPVOID pParam);
UINT DownloadThreadFunc(LPVOID pParam);
UINT ModifyThreadFunc(LPVOID pParam);
UINT CopyToPrivateThreadFunc(LPVOID pParam);
UINT CopyFromPrivateThreadFunc(LPVOID pParam);
UINT VerifyThreadFunc(LPVOID pParam);
UINT EncodeThreadFunc(LPVOID pParam);
UINT BackupThreadFunc(LPVOID pParam);
UINT RestoreThreadFunc(LPVOID pParam);
UINT CompressThreadFunc(LPVOID pParam);
UINT DecompressThreadFunc(LPVOID pParam);
UINT DeleteThreadFunc(LPVOID pParam);
