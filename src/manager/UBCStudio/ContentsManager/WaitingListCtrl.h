#pragma once

#include <atlimage.h>

// CWaitingListCtrl

class CWaitingListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CWaitingListCtrl)

public:
	CWaitingListCtrl();
	virtual ~CWaitingListCtrl();

	afx_msg BOOL OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult);

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void	StartWaitingIcon();
	void	StopWaitingIcon();

	BOOL	Sort(int col, bool ascend);

protected:

	CImage	m_img;
	int		m_nIndex;
	bool	m_bTimer;

	int		m_nSortCol;
	bool	m_bAscend;

	static int CALLBACK SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam);
};

typedef struct
{
	HWND	hWnd;			// 리스트컨트롤 핸들
	int		nCol;			// 정렬기준 컬럼
	bool	bAscend;		// true=내림차순, false=오름차순
} SORT_PARAM;
