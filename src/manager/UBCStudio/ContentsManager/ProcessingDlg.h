#pragma once

#include "common/UTBListCtrlEx.h"

#include "etc.h"

// CProcessingDlg 대화 상자입니다.

class CProcessingDlg : public CDialog
{
	DECLARE_DYNAMIC(CProcessingDlg)

public:
	CProcessingDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CProcessingDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PROCESSING };

protected:
	enum {
		COLUMN_CNAME = 0,
		COLUMN_FNAME,
		//COLUMN_LOCATION,
		COLUMN_SIZE,
		COLUMN_STATUS,
		COLUMN_RESULT,
		COLUMN_COUNT,
	};

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CWinThread*	m_pThread;
	BOOL		DeleteThread();

	CString		m_strCSiteID;
	CString		m_strProgramID;

	CContentsObjectList		m_listUpload;
	CContentsObjectList		m_listDownload;
	CContentsObjectList		m_listCopyToPrivate;
	CContentsObjectList		m_listCopyFromPrivate;
	CContentsObjectList*	m_listCopyFromPrivateTmp;
	CContentsObjectList		m_listVerify;
	CContentsObjectList		m_listEncode;
	CContentsObjectList		m_listBackup;
	CContentsObjectList		m_listRestore;
	CContentsObjectList		m_listCompress;
	CContentsObjectList		m_listDecompress;
	CContentsObjectList		m_listDelete;
	CContentsObjectList		m_listModify;

	CContentsObjectList*	m_listCurrent;

	bool					m_bUpdatedListDetached;
	CONTENTS_INFO_LIST*		m_listUpdated;

public:

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnChangeStatus(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeProcessing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompletedAllProcessing(WPARAM wParam, LPARAM lParam);
	afx_msg void	OnLvnKeydownListCommonContents(NMHDR *pNMHDR, LRESULT *pResult);

	CStatic			m_stcStatus;
	CUTBListCtrlEx	m_lcContents;
	CButton			m_btnOK;
	CButton			m_btnCancel;

public:

	void	SetSiteInfo(LPCTSTR lpszSiteID, LPCTSTR lpszProgramID)
					{ m_strCSiteID=lpszSiteID; m_strProgramID=lpszProgramID; };

	void	AddUploadContents(CContentsObject* obj) { m_listUpload.Add(obj); };
	void	AddUploadContents(CContentsObjectList& obj_list) { m_listUpload.Copy(obj_list); };
	void	AddDownloadContents(CContentsObject* obj) { m_listDownload.Add(obj); };
	void	AddDownloadContents(CContentsObjectList& obj_list) { m_listDownload.Copy(obj_list); };
	void	AddModifyContents(CContentsObject* obj) { m_listModify.Add(obj); };
	void	AddModifyContents(CContentsObjectList& obj_list) { m_listModify.Copy(obj_list); };
	void	AddCopyToPrivateContents(CContentsObject* obj) { m_listCopyToPrivate.Add(obj); };
	void	AddCopyToPrivateContents(CContentsObjectList& obj_list) { m_listCopyToPrivate.Copy(obj_list); };
//	void	AddCopyFromPrivateContents(CContentsObject* obj) { m_listCopyFromPrivate.Add(obj); };
//	void	AddCopyFromPrivateContents(CContentsObjectList& obj_list) { m_listCopyFromPrivate.Copy(obj_list); };
	void	AddCopyFromPrivateContents(CONTENTS_INFO* info);
	void	AddCopyFromPrivateContents(CArray<CONTENTS_INFO*> &info_list);
	void	AddVerifyContents(CContentsObject* obj) { m_listVerify.Add(obj); };
	void	AddVerifyContents(CContentsObjectList& obj_list) { m_listVerify.Copy(obj_list); };
	void	AddEncodeContents(CContentsObject* obj) { m_listEncode.Add(obj); };
	void	AddEncodeContents(CContentsObjectList& obj_list) { m_listEncode.Copy(obj_list); };
	void	AddBackupContents(CContentsObject* obj) { m_listBackup.Add(obj); };
	void	AddBackupContents(CContentsObjectList& obj_list) { m_listBackup.Copy(obj_list); };
	void	AddRestoreContents(CContentsObject* obj) { m_listRestore.Add(obj); };
	void	AddRestoreContents(CContentsObjectList& obj_list) { m_listRestore.Copy(obj_list); };
	void	AddCompressContents(CContentsObject* obj) { m_listCompress.Add(obj); };
	void	AddCompressContents(CContentsObjectList& obj_list) { m_listCompress.Copy(obj_list); };
	void	AddDecompressContents(CContentsObject* obj) { m_listDecompress.Add(obj); };
	void	AddDecompressContents(CContentsObjectList& obj_list) { m_listDecompress.Copy(obj_list); };
	void	AddDeleteContents(CContentsObject* obj) { m_listDelete.Add(obj); };
	void	AddDeleteContents(CContentsObjectList& obj_list) { m_listDelete.Copy(obj_list); };

	CONTENTS_INFO_LIST*		GetContentsUpdatedList() { m_bUpdatedListDetached=true; return m_listUpdated; };
	int		GetContentsUpdatedCount() { if(m_listUpdated) return m_listUpdated->GetCount(); return 0; };
};
