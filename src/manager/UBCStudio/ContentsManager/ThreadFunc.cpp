// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// ContentsManagerDll.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"
#include "etc.h"

#include "ThreadFunc.h"
#include "Enviroment.h"
#include "common\libCommon\ubcMuxRegacy.h"

#define		FTP_BUFFER_SIZE		16384

extern CMutex	g_mutexThreadMain;
extern int		g_nThreadMain;
extern CString	WEB_SERVER_IP;

UINT LoginThreadFunc(LPVOID pParam)
{
	LOGIN_THREAD_PARAM* param = (LOGIN_THREAD_PARAM*)pParam;

	CString url;
	url.Format(_T("http://%s:%d/ContentsManager/ubc_get_user_level.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

	CString send_msg;
	send_msg.Format(_T("siteId=%s&userId=%s&pw=%s"), param->strSiteID, param->strUserID, param->strPassword);

	TRACE(_T("LoginThreadFunc : %s(%s)\r\n"), url, send_msg);

	CString response = _T("");
	if(RequestPost(url, send_msg, response) && response.GetLength() == 1)
	{
		GUARD(g_mutexThreadMain);
			if(param->nThreadSerial == g_nThreadMain)
				::PostMessage(param->hParentWnd, WM_COMPLETE_LOGIN, 1, atoi(response));
		END_GUARD;
	}
	else
	{
		GUARD(g_mutexThreadMain);
			if(param->nThreadSerial == g_nThreadMain)
				::PostMessage(param->hParentWnd, WM_COMPLETE_LOGIN, 0, 0);
		END_GUARD;
	}

	delete param;

	return 0;
}

UINT GetCommonListThreadFunc(LPVOID pParam)
{
	GET_COMMON_LIST_THREAD_PARAM* param = (GET_COMMON_LIST_THREAD_PARAM*)pParam;

	CString url;
	url.Format(_T("http://%s:%d/ContentsManager/ubc_get_common_list.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

	CString send_msg = _T("");
	send_msg.Format(_T("contentsId=%s&contentsState=%s&keyword=%s"), 
		ConvertToURLString(param->strContentsId, false), 
		ConvertToURLString(param->strContentsState, false), 
		ConvertToURLString(param->strKeyword, false) );

	if(param->nContentsType >= 0)
	{
		if(send_msg.GetLength() > 0) send_msg += "&";
		send_msg += "contentsType=";
		send_msg += ToString(param->nContentsType);
	}

	CString* response = new CString();
	*response = _T("");

	TRACE(_T("GetCommonListThreadFunc : %s(%s)\r\n"), url, send_msg);

	if(RequestPost(url, send_msg, *response))
	{
		GUARD(g_mutexThreadMain);
			if(param->nThreadSerial == g_nThreadMain)
				::PostMessage(param->hParentWnd, WM_COMPLETE_GET_COMMON_LIST, 1, (LPARAM)response);
			else
				delete response;
		END_GUARD;
	}
	else
	{
		GUARD(g_mutexThreadMain);
			if(param->nThreadSerial == g_nThreadMain)
				::PostMessage(param->hParentWnd, WM_COMPLETE_GET_COMMON_LIST, 0, (LPARAM)response);
			else
				delete response;
		END_GUARD;
	}

	delete param;

	return 0;
}


extern CMutex	g_mutexThreadProcessing;
extern int		g_nThreadProcessing;

UINT UploadThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData((LPCTSTR)param->strSiteID);
		if(aData == NULL)
		{
			obj->processingResultString = _T("mux data is NULL !!!");
			continue;
		}

		CString send_msg = _T("");

		send_msg += _T("contentsId=");
		send_msg += ConvertToURLString(obj->strId);

		send_msg += _T("&contentsName=");
		send_msg += ConvertToURLString(obj->strContentsName);

		send_msg += _T("&contentsType=");
		send_msg += ToString(obj->nContentsType);

		send_msg += _T("&registerTime=");
		send_msg += CTime::GetCurrentTime().Format(_T("%Y/%m/%d%%20%H:%M:%S")); //'현재시간 입력

		send_msg += _T("&isRaw=");
		send_msg += _T("0"); //' 0

		send_msg += _T("&contentsState=");
		if(obj->strFilename.GetLength() > 0)
			send_msg += _T("0"); //' 0
		else
			send_msg += _T("1"); //' 0

		send_msg += _T("&contentsStateTime=");
		send_msg += CTime::GetCurrentTime().Format(_T("%Y/%m/%d%%20%H:%M:%S")); //'현재시간 입력

		send_msg += _T("&location=");
		send_msg += _T("/contents/COMMON/"); //' "/contents/COMMON/"

		send_msg += _T("&filename=");
		send_msg += ConvertToURLString(obj->strFilename); //' ""

		send_msg += _T("&width=");
		send_msg += ToString(obj->nWidth); // ' 0

		send_msg += _T("&height=");
		send_msg += ToString(obj->nHeight); // ' 0

		send_msg += _T("&volume=");
		send_msg += ToString(obj->nFilesize); // ' 0

		send_msg += _T("&runningTime=");
		send_msg += ToString(obj->nRunningTime); // ' 0

		send_msg += _T("&encodingTime=");
		send_msg += CTime::GetCurrentTime().Format(_T("%Y/%m/%d%%20%H:%M:%S")); // '현재시간 입력

		send_msg += _T("&comment1=");
		send_msg += ConvertToURLString(obj->strComment[0]); // ' ""

		send_msg += _T("&comment2=");
		send_msg += ConvertToURLString(obj->strComment[1]);

		send_msg += _T("&comment3=");
		send_msg += ConvertToURLString(obj->strComment[2]);

		send_msg += _T("&comment4=");
		send_msg += ConvertToURLString(obj->strComment[3]);

		send_msg += _T("&comment5=");
		send_msg += ConvertToURLString(obj->strComment[4]);

		send_msg += _T("&comment6=");
		send_msg += ConvertToURLString(obj->strComment[5]);

		send_msg += _T("&comment7=");
		send_msg += ConvertToURLString(obj->strComment[6]);

		send_msg += _T("&comment8=");
		send_msg += ConvertToURLString(obj->strComment[7]);

		send_msg += _T("&comment9=");
		send_msg += ConvertToURLString(obj->strComment[8]);

		send_msg += _T("&comment10=");
		send_msg += ConvertToURLString(obj->strComment[9]);

		send_msg += _T("&currentComment=");
		send_msg += ToString(obj->nCurrentComment); // ' 0

		send_msg += _T("&bgColor=");
		send_msg += ConvertToURLString(obj->strBgColor); // ' ""

		send_msg += _T("&fgColor=");
		send_msg += ConvertToURLString(obj->strFgColor);// ' ""

		send_msg += _T("&font=");
		send_msg += ConvertToURLString(obj->strFont); // ' ""

		send_msg += _T("&fontSize=");
		send_msg += ToString(obj->nFontSize); // ' 0

		send_msg += _T("&playSpeed=");
		send_msg += ToString(obj->nPlaySpeed); // ' 0

		send_msg += _T("&soundVolume=");
		send_msg += ToString(obj->nSoundVolume); // ' 0

		send_msg += _T("&isPublic=");
		send_msg += _T("1"); // ' 1

		send_msg += _T("&direction=");
		send_msg += ToString(obj->nDirection); // ' 0

		send_msg += _T("&align=");
		send_msg += ToString(obj->nAlign); // ' 0

		send_msg += _T("&isUsed=");
		send_msg += _T("1"); // ' 1

		send_msg += _T("&wizardXML=");
		send_msg += ConvertToURLString(obj->strWizardXML); // ' 1

		send_msg += _T("&wizardFiles=");
		send_msg += ConvertToURLString(obj->strWizardFiles); // ' 1

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_create_common.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		TRACE(_T("UploadThreadFunc : %s -> %s\r\n"), url, send_msg);

		CString response;
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eNowFTPProcessing;

			GUARD(g_mutexThreadProcessing);
				if(param->nThreadSerial == g_nThreadProcessing)
				{
					obj->processingResultValue = result_code;
					::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
				}
			END_GUARD;

			// 파일 ftp 전송
			if(obj->strFilename.GetLength() > 0)
			{
				TRY
				{
					CInternetSession is;
					CFtpConnection* ftp_conn = is.GetFtpConnection(aData->getFTPAddress(), aData->ftpId.c_str(), aData->ftpPasswd.c_str(), aData->ftpPort, TRUE);
					if(ftp_conn)
					{
						ftp_conn->SetCurrentDirectory(obj->strServerLocation);

						CInternetFile* ftp_file = ftp_conn->OpenFile(obj->strFilename, GENERIC_WRITE);
						if(ftp_file)
						{
							CString localpath = obj->strLocalLocation;
							localpath += obj->strFilename;

							CFile local_file;
							if(local_file.Open(localpath, CFile::modeRead | CFile::typeBinary))
							{
								ULONGLONG read_size = 0;
								ULONGLONG total_size = local_file.GetLength();
								LPBYTE buf = new BYTE[FTP_BUFFER_SIZE];
								DWORD last_tick = ::GetTickCount();
								while(total_size > read_size && param->nThreadSerial == g_nThreadProcessing)
								{
									UINT size = local_file.Read(buf, FTP_BUFFER_SIZE);
									ftp_file->Write(buf, size);
									read_size += size;

									DWORD tick = ::GetTickCount();
									if( (last_tick < tick && tick - last_tick > 250) ||
										(last_tick > tick && tick + (MAXDWORD - last_tick) > 250) )
									{
										last_tick = tick;

										GUARD(g_mutexThreadProcessing);
											if(param->nThreadSerial == g_nThreadProcessing)
												::PostMessage(param->hParentWnd, WM_CHANGE_PROCESSING, i, (total_size == 0 ? 100 : read_size*100/total_size) );
										END_GUARD;
									}
								}
								GUARD(g_mutexThreadProcessing);
									if(param->nThreadSerial == g_nThreadProcessing)
										::PostMessage(param->hParentWnd, WM_CHANGE_PROCESSING, i, (total_size == 0 ? 100 : read_size*100/total_size) );
								END_GUARD;
								delete[]buf;

								result_code = eCompleted;
							}
							else
							{
								result_code = eErrorOpenLocalFile;
							}

							ftp_file->Close();
							delete ftp_file;
						}
						else
						{
							result_code = eErrorCreateRemoteFile;
						}

						ftp_conn->Close();
						delete ftp_conn;
					}
					else
					{
						result_code = eErrorConnectRemoteServer;
					}
				}
				CATCH(CFileException, ex)
				{
					TCHAR msg[1024] = {0};
					ex->GetErrorMessage(msg, 1024);
					TRACE(_T("%s\r\n"), msg);
					result_code = eErrorFTPProcessing;
				}
				CATCH(CInternetException, ex)
				{
					TCHAR msg[1024] = {0};
					ex->GetErrorMessage(msg, 1024);
					TRACE(_T("%s\r\n"), msg);
					result_code = eErrorFTPProcessing;
				}
				END_CATCH
			}
			else
			{
				result_code = eCompleted;
			}

			//
			if(result_code == eCompleted)
			{
				url.Format(_T("http://%s:%d/ContentsManager/ubc_complete_to_upload.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

				CString send_msg = _T("");
				send_msg += _T("contentsId=");
				send_msg += obj->strId;

				TRACE(_T("UploadThreadFunc : %s(%s)\r\n"), url, send_msg);

				RequestPost(url, send_msg, response);

				if(response.CompareNoCase(_T("true")) == 0)
				{
					result_code = eCompleted;
				}
				else if(response.CompareNoCase(_T("false")) == 0)
				{
					result_code = eErrorNotExist;
				}
				else
				{
					obj->processingResultString = response;
					result_code = eUnknownError;
				}
			}
		}
		else if(response.CompareNoCase(_T("already")) == 0)
		{
			result_code = eAlreadyContentsExist;
		}
		else
		{
			obj->processingResultString = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	for(int i=0; i<param->pListObjList->GetCount(); i++)
	{
		CContentsObject* obj = param->pListObjList->GetAt(i);
		delete obj;
	}

	delete param;

	return 0;
}

UINT DownloadThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;
		ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData("COMMON");

		if(aData == NULL)
		{
			obj->processingResultString = _T("mux data is NULL !!!");
			continue;
		}

		int result_code = eUnknownError;

		TRY
		{
			CInternetSession is;
			CFtpConnection* ftp_conn = is.GetFtpConnection(aData->getFTPAddress(), aData->ftpId.c_str(), aData->ftpPasswd.c_str(), aData->ftpPort, TRUE);
			if(ftp_conn)
			{
				ULONGLONG total_size = 0;
				{
					CFtpFileFind finder(ftp_conn);
					CString remote_path;
					remote_path.Format(_T("%s%s"), obj->strServerLocation, obj->strFilename);
					BOOL bWorking = finder.FindFile(remote_path, INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE);
					if(bWorking)
					{
						finder.FindNextFile();
						total_size = finder.GetLength();
					}
				}

				ftp_conn->SetCurrentDirectory(obj->strServerLocation);

				CInternetFile* ftp_file = ftp_conn->OpenFile(obj->strFilename, GENERIC_READ);
				if(ftp_file)
				{
					CString local_path = _T("");
					local_path.Format(_T("%s\\%s%s"), GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, obj->strFilename);

					CFile local_file;
					if(local_file.Open(local_path, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
					{
						ULONGLONG write_size = 0;
						//ULONGLONG total_size = obj->nFilesize;
						LPBYTE buf = new BYTE[FTP_BUFFER_SIZE];
						DWORD last_tick = ::GetTickCount();

						while(total_size > write_size && param->nThreadSerial == g_nThreadProcessing)
						{
							UINT size = ftp_file->Read(buf, FTP_BUFFER_SIZE);
							local_file.Write(buf, size);
							write_size += size;

							DWORD tick = ::GetTickCount();
							if( (last_tick < tick && tick - last_tick > 250) ||
								(last_tick > tick && tick + (MAXDWORD - last_tick) > 250) )
							{
								last_tick = tick;

								GUARD(g_mutexThreadProcessing);
									if(param->nThreadSerial == g_nThreadProcessing)
										::PostMessage(param->hParentWnd, WM_CHANGE_PROCESSING, i, (total_size == 0 ? 100 : write_size*100/total_size) );
								END_GUARD;
							}
						}
						GUARD(g_mutexThreadProcessing);
							if(param->nThreadSerial == g_nThreadProcessing)
								::PostMessage(param->hParentWnd, WM_CHANGE_PROCESSING, i, (total_size == 0 ? 100 : write_size*100/total_size) );
						END_GUARD;

						delete[]buf;

						result_code = eCompleted;
						if(obj->nFilesize == write_size)
							obj->bLocalFileExist = true;
					}
					else
						result_code = eErrorCreateLocalFile;

					ftp_file->Close();
					delete ftp_file;
				}
				else
					result_code = eErrorOpenRemoteFile;

				ftp_conn->Close();
				delete ftp_conn;
			}
			else
				result_code = eErrorConnectRemoteServer;
		}
		CATCH(CFileException, ex)
		{
			TCHAR msg[1024] = {0};
			ex->GetErrorMessage(msg, 1024);
			TRACE(_T("%s\r\n"), msg);
			result_code = eErrorFTPProcessing;
		}
		CATCH(CInternetException, ex)
		{
			TCHAR msg[1024] = {0};
			ex->GetErrorMessage(msg, 1024);
			TRACE(_T("%s\r\n"), msg);
			result_code = eErrorFTPProcessing;

		}
		END_CATCH

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT CopyToPrivateThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		//CString url;
		//url.Format(_T("http://%s:%d/ContentsManager/ubc_copy_to_private.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		//CString send_msg;
		//send_msg.Format(_T("contentsId=%s&siteId=%s&programId=%s"), obj->strId, param->strSiteID, param->strProgramID);

		//TRACE(_T("CopyToPrivateThreadFunc : %s(%s)\r\n"), url, send_msg);

		//CString response = _T("");
		//RequestPost(url, send_msg, response);

		//int result_code = eUnknownError;

		//if(response.CompareNoCase(_T("true")) == 0)
		//{
		//	result_code = eCompleted;
		//}
		//else if(response.CompareNoCase(_T("already")) == 0)
		//{
		//	result_code = eAlreadyContentsExist;
		//}
		//else if(response.CompareNoCase(_T("false")) == 0)
		//{
		//	result_code = eErrorNotExist;
		//}
		//else if(response.CompareNoCase(_T("cant")) == 0)
		//{
		//	result_code = eErrorCantDo;
		//}
		//else
		//{
		//	obj->processingResultString  = response;
		//}

		int result_code = eCompleted;
		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT CopyFromPrivateThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_copy_from_private.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		CString send_msg;
		send_msg.Format(_T("siteId=%s&programId=%s&contentsId=%s"), 
			ConvertToURLString(param->strSiteID), 
			ConvertToURLString(param->strProgramID), 
			ConvertToURLString(obj->strId) );

		TRACE(_T("CopyFromPrivateThreadFunc : %s(%s)\r\n"), url, send_msg);

		CString response = _T("");
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("already")) == 0)
		{
			result_code = eAlreadyContentsExist;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else if(response.CompareNoCase(_T("cant")) == 0)
		{
			result_code = eErrorCantDo;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT ModifyThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString send_msg = _T("");

		send_msg += _T("contentsId=");
		send_msg += ConvertToURLString(obj->strId);

		send_msg += _T("&contentsName=");
		send_msg += ConvertToURLString(obj->strContentsName);

		send_msg += _T("&contentsType=");
		send_msg += ToString(obj->nContentsType);

		send_msg += _T("&registerTime=");
		send_msg += CTime::GetCurrentTime().Format(_T("%Y/%m/%d%%20%H:%M:%S")); //'현재시간 입력

		send_msg += _T("&isRaw=");
		send_msg += _T("0"); //' 0

		send_msg += _T("&contentsState=");
		send_msg += ToString(obj->nContentsState); //' 0

		send_msg += _T("&contentsStateTime=");
		send_msg += CTime::GetCurrentTime().Format(_T("%Y/%m/%d%%20%H:%M:%S")); //'현재시간 입력

		send_msg += _T("&location=");
		send_msg += ConvertToURLString(obj->strServerLocation); //' "/contents/COMMON/"

		send_msg += _T("&filename=");
		send_msg += ConvertToURLString(obj->strFilename); //' ""

		send_msg += _T("&width=");
		send_msg += ToString(obj->nWidth); // ' 0

		send_msg += _T("&height=");
		send_msg += ToString(obj->nHeight); // ' 0

		send_msg += _T("&volume=");
		send_msg += ToString(obj->nFilesize); // ' 0

		send_msg += _T("&runningTime=");
		send_msg += ToString(obj->nRunningTime); // ' 0

		send_msg += _T("&encodingTime=");
		send_msg += CTime::GetCurrentTime().Format(_T("%Y/%m/%d%%20%H:%M:%S")); // '현재시간 입력

		send_msg += _T("&comment1=");
		send_msg += ConvertToURLString(obj->strComment[0]); // ' ""

		send_msg += _T("&comment2=");
		send_msg += ConvertToURLString(obj->strComment[1]);

		send_msg += _T("&comment3=");
		send_msg += ConvertToURLString(obj->strComment[2]);

		send_msg += _T("&comment4=");
		send_msg += ConvertToURLString(obj->strComment[3]);

		send_msg += _T("&comment5=");
		send_msg += ConvertToURLString(obj->strComment[4]);

		send_msg += _T("&comment6=");
		send_msg += ConvertToURLString(obj->strComment[5]);

		send_msg += _T("&comment7=");
		send_msg += ConvertToURLString(obj->strComment[6]);

		send_msg += _T("&comment8=");
		send_msg += ConvertToURLString(obj->strComment[7]);

		send_msg += _T("&comment9=");
		send_msg += ConvertToURLString(obj->strComment[8]);

		send_msg += _T("&comment10=");
		send_msg += ConvertToURLString(obj->strComment[9]);

		send_msg += _T("&currentComment=");
		send_msg += ToString(obj->nCurrentComment); // ' 0

		send_msg += _T("&bgColor=");
		send_msg += ConvertToURLString(obj->strBgColor); // ' ""

		send_msg += _T("&fgColor=");
		send_msg += ConvertToURLString(obj->strFgColor);// ' ""

		send_msg += _T("&font=");
		send_msg += ConvertToURLString(obj->strFont); // ' ""

		send_msg += _T("&fontSize=");
		send_msg += ToString(obj->nFontSize); // ' 0

		send_msg += _T("&playSpeed=");
		send_msg += ToString(obj->nPlaySpeed); // ' 0

		send_msg += _T("&soundVolume=");
		send_msg += ToString(obj->nSoundVolume); // ' 0

		send_msg += _T("&isPublic=");
		send_msg += _T("1"); // ' 1

		send_msg += _T("&direction=");
		send_msg += ToString(obj->nDirection); // ' 0

		send_msg += _T("&align=");
		send_msg += ToString(obj->nAlign); // ' 0

		send_msg += _T("&isUsed=");
		send_msg += _T("1"); // ' 1

		send_msg += _T("&wizardXML=");
		send_msg += ConvertToURLString(obj->strWizardXML); // ' 1

		send_msg += _T("&wizardFiles=");
		send_msg += ConvertToURLString(obj->strWizardFiles); // ' 1

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_set_common.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);//, obj->strId);

		TRACE(_T("ModifyThreadFunc : %s -> %s\r\n"), url, send_msg);

		CString response = _T("");
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT VerifyThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_check_to_verify.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		CString send_msg;
		send_msg.Format(_T("contentsId=%s"), ConvertToURLString(obj->strId) );

		TRACE(_T("VerifyThreadFunc : %s(%s)\r\n"), url, send_msg);

		CString response = _T("");
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("already")) == 0)
		{
			result_code = eAlreadyContentsVerified;
		}
		else if(response.CompareNoCase(_T("working")) == 0)
		{
			result_code = eErrorNowWorking;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT EncodeThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_encode_common.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		CString send_msg;
		send_msg.Format(_T("contentsId=%s"), ConvertToURLString(obj->strId));

		TRACE(_T("EncodeThreadFunc : %s(%s)\r\n"), url, send_msg);

		CString response = _T("");
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("nothing")) == 0)
		{
			result_code = eErrorNoContentsFile;
		}
		else if(response.CompareNoCase(_T("already")) == 0)
		{
			result_code = eAlreadyContentsExist;
		}
		else if(response.CompareNoCase(_T("cant")) == 0)
		{
			result_code = eErrorCantDo;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT BackupThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_move_to_backup.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		CString send_msg;
		send_msg.Format(_T("contentsId=%s"), ConvertToURLString(obj->strId));

		TRACE(_T("BackupThreadFunc : %s(%s)\r\n"), url, send_msg);

		CString response = _T("");
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("cant")) == 0)
		{
			result_code = eErrorCantDo;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else if(response.CompareNoCase(_T("using")) == 0)
		{
			result_code = eErrorUsingAnotherPackage;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT RestoreThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_move_to_restore.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		CString send_msg;
		send_msg.Format(_T("contentsId=%s"), ConvertToURLString(obj->strId));

		TRACE(_T("RestoreThreadFunc : %s(%s)\r\n"), url, send_msg);

		CString response = _T("");
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("cant")) == 0)
		{
			result_code = eErrorCantDo;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT CompressThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_move_to_compress.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		CString send_msg;
		send_msg.Format(_T("contentsId=%s"), ConvertToURLString(obj->strId));

		TRACE(_T("CompressThreadFunc : %s(%s)\r\n"), url, send_msg);

		CString response = _T("");
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("cant")) == 0)
		{
			result_code = eErrorCantDo;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else if(response.CompareNoCase(_T("fail")) == 0)
		{
			result_code = eErrorCreateRemoteFile;
		}
		else if(response.CompareNoCase(_T("using")) == 0)
		{
			result_code = eErrorUsingAnotherPackage;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT DecompressThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_move_to_decompress.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		CString send_msg;
		send_msg.Format(_T("contentsId=%s"), ConvertToURLString(obj->strId));

		TRACE(_T("DecompressThreadFunc : %s(%s)\r\n"), url, send_msg);

		CString response = _T("");
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("cant")) == 0)
		{
			result_code = eErrorCantDo;
		}
		else if(response.CompareNoCase(_T("fail")) == 0)
		{
			result_code = eErrorCreateRemoteFile;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

UINT DeleteThreadFunc(LPVOID pParam)
{
	COMMAND_THREAD_PARAM* param = (COMMAND_THREAD_PARAM*)pParam;

	for(int i=0; i<param->pListObjList->GetCount() && param->nThreadSerial == g_nThreadProcessing; i++)
	{
		CContentsObject* obj = NULL;

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
				obj = param->pListObjList->GetAt(i);
		END_GUARD;
		if(obj == NULL) continue;

		CString url;
		url.Format(_T("http://%s:%d/ContentsManager/ubc_delete_common.asp"), WEB_SERVER_IP, WEB_SERVER_PORT);

		CString send_msg;
		send_msg.Format(_T("contentsId=%s"), ConvertToURLString(obj->strId));

		TRACE(_T("DeleteThreadFunc : %s(%s)\r\n"), url, send_msg);

		CString response = _T("");
		RequestPost(url, send_msg, response);

		int result_code = eUnknownError;

		if(response.CompareNoCase(_T("true")) == 0)
		{
			result_code = eCompleted;
		}
		else if(response.CompareNoCase(_T("using")) == 0)
		{
			result_code = eErrorUsingAnotherPackage;
		}
		else if(response.CompareNoCase(_T("false")) == 0)
		{
			result_code = eErrorNotExist;
		}
		else
		{
			obj->processingResultString  = response;
		}

		GUARD(g_mutexThreadProcessing);
			if(param->nThreadSerial == g_nThreadProcessing)
			{
				obj->processingResultValue = result_code;
				::PostMessage(param->hParentWnd, WM_CHANGE_STATUS, i, result_code);
			}
		END_GUARD;
	}

	GUARD(g_mutexThreadProcessing);
		if(param->nThreadSerial == g_nThreadProcessing)
			::PostMessage(param->hParentWnd, WM_COMPLETED_ALL_PROCESSING, 0, 0);
	END_GUARD;

	delete param;

	return 0;
}

