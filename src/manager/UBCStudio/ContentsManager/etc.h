#pragma once

#define		WEB_SERVER_PORT					8080

#define		WM_COMPLETE_LOGIN				(WM_USER + 256)
#define		WM_COMPLETE_GET_COMMON_LIST		(WM_USER + 257)
#define		WM_COMPLETED_ALL_PROCESSING		(WM_USER + 258)
#define		WM_CHANGE_STATUS				(WM_USER + 259)
#define		WM_CHANGE_PROCESSING			(WM_USER + 260)

enum PROCESS_STATUS {
	eUnknownError = 0,
	eNowFTPProcessing,
	eCompleted,
	eCompletedVerified,
	eAlreadyContentsExist,
	eAlreadyContentsVerified,
	eErrorOpenLocalFile,
	eErrorCreateRemoteFile,
	eErrorConnectRemoteServer,
	eErrorFTPProcessing,
	eErrorCreateLocalFile,
	eErrorOpenRemoteFile,
	eErrorNowWorking,
	eErrorNotExist,
	eErrorCantDo,
	eErrorUsingAnotherPackage,
	eErrorNoContentsFile,
};


#define		TIMER_INIT_ID				1025
#define		TIMER_INIT_TIME				10


BOOL RequestGet(LPCTSTR lpUrl, CString& strOutMsg);
BOOL RequestPost(LPCTSTR lpUrl, CString& strSendMsg, CString& strOutMsg);


////////////////////////////////////////////////////////////////
typedef struct {
	CString	text;
	int		idx;
	int		width;
	int		align;
} LISTCTRL_COLUMN_INFO;

void		InitListCtrlColumn(CListCtrl& lc, LISTCTRL_COLUMN_INFO* pColumnInfo, int count);
CString		ConvertToURLString(CString str, bool bReturnNullString=true);

/////////////////////////////////////////////////////////////////
//class CContentsObject
//{
//protected:
//	CMapStringToString		m_mapAttributes;
//
//public:
//	CContentsObject() {};
//	virtual ~CContentsObject() {};
//
//	void		AddAttribute(LPCTSTR lpszKey, LPCTSTR lpszValue);
//	void		AddAttribute(LPCTSTR lpszKey, int nValue);
//	void		AddAttribute(LPCTSTR lpszKey, CTime& tValue);
//
//	CString		GetAttributeString(LPCTSTR lpszKey);
//	int			GetAttributeInt(LPCTSTR lpszKey);
//	CTime		GetAttributeDataTime(LPCTSTR lpszKey);
//};
//
//typedef		CArray<CContentsObject*, CContentsObject*>		CContentsObjectList;
class CContentsObject : public CONTENTS_INFO
{
public:
	CContentsObject() {};
	virtual ~CContentsObject() {};

	int 	processingResultValue;
	CString	processingResultString;
//	CString	strSiteID;
//	CString strProgramID;
};

typedef		CArray<CContentsObject*, CContentsObject*>		CContentsObjectList;

class CTemplateObject : public TEMPLATE_LINK
{
public:
	CTemplateObject() {};
	virtual ~CTemplateObject() {};

	int 	processingResultValue;
	CString	processingResultString;
};

typedef		CArray<CTemplateObject*, CTemplateObject*>		CTemplateObjectList;

/////////////////////////////////////////////////////////////////
class CGuard
{
public:
	CGuard(CMutex& mutex) : m_mutex(&mutex) { m_mutex->Lock(); };
	virtual ~CGuard() { if(m_mutex) m_mutex->Unlock(); };

	CMutex*		m_mutex;
};

#define		GUARD(x)		{ CGuard lock(x);
#define		END_GUARD		}

#ifdef _ML_ENG_
	#define	CM_CONTENTS_TYPE_WIZARD				CONTENTSDIALOG_STR011
	#define CM_NOTIFY_SUCCESS_ALL_PROCESSING	_T("All operations have been processed successfully.")
	#define CM_NOTIFY_MSG_NO_BADAK_ENCODER		_T("Can't find Encoder.\r\n\r\nPlease install Badak Encoder.")
#elif _ML_KOR_
	#define	CM_CONTENTS_TYPE_WIZARD				CONTENTSDIALOG_STR011
	#define CM_NOTIFY_SUCCESS_ALL_PROCESSING	_T("모든 작업이 정상적으로 처리되었습니다.")
	#define CM_NOTIFY_MSG_NO_BADAK_ENCODER		_T("인코더를 찾을 수 없습니다.\r\n\r\n바닥 인코더를 설치하여주시기 바랍니다.")
#elif _ML_JAP_
	#define	CM_CONTENTS_TYPE_WIZARD				CONTENTSDIALOG_STR011
	#define CM_NOTIFY_SUCCESS_ALL_PROCESSING	_T("すべての操作が正常に處理されています.")
	#define CM_NOTIFY_MSG_NO_BADAK_ENCODER		_T("エンコ-ダを見つけることができません。\r\n\r\nBadakエンコ-ダをインスト-ルしてください")
#endif
