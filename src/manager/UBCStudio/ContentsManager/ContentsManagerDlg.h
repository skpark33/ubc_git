#pragma once

#include <afxmt.h>
#include "afxwin.h"

#include "etc.h"
#include "WaitingListCtrl.h"


class CUBCStudioDoc;

// CContentsManagerDlg 대화 상자입니다.

class /*AFX_CONTROL_LIB_DLL*/ CContentsManagerDlg : public CDialog
{
	DECLARE_DYNAMIC(CContentsManagerDlg)

public:
	CContentsManagerDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CContentsManagerDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
#ifdef _CONTENTS_MANAGER_
	enum { IDD = IDD_CONTENTS_MANAGER };
#endif

protected:
	enum {
		COLUMN_STATUS = 0,
		COLUMN_TYPE,
		//COLUMN_ID,
		COLUMN_NAME,
		COLUMN_FILENAME,
		COLUMN_COUNT,
	};

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CUBCStudioDoc* m_pDocument;

	CString		m_strCSiteID;
	CString		m_strProgramID;

	CString		m_strUSiteID;
	CString		m_strUserID;
	CString		m_strPassword;

	int			m_nUserPriv;

	bool		m_bContentsUpadted;

	CWinThread*	m_pThread;
	BOOL		DeleteThread();

	void		InitControl();
	int			GetFilterTypeEnum(int idx);
	CString		GetFilterStateString(int idx);

	CString		GetContentsTypeString(int contents_type);
	CString		GetContentsStateString(int contents_state);

	void		SetOperationLevel(int nUserPriv, int nContentsState);

	BOOL		GetContentsObjectList(LPVOID pContentsObjList, CStringArray& str_list);
	BOOL		DeleteAllItems();
	BOOL		InsertItem(LPVOID pObject, int idx=-1);

	void		GetSelectItemList(CContentsObjectList& obj_list);

	bool					m_bUpdatedListDetached;
	CONTENTS_INFO_LIST*		m_listUpdated;

public:
	afx_msg void	OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnTimer(UINT_PTR nIDEvent);
	afx_msg void	OnNMDblclkListCommonContents(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void	OnBnClickedButtonRefresh();
	afx_msg void	OnBnClickedButtonModify();
	afx_msg void	OnBnClickedButtonAddNew();
	afx_msg void	OnBnClickedButtonCopyToPrivate();
	afx_msg void	OnBnClickedButtonCopyFromPrivate();
	afx_msg void	OnBnClickedButtonVerify();
	afx_msg void	OnBnClickedButtonEncoding();
	afx_msg void	OnBnClickedButtonBackup();
	afx_msg void	OnBnClickedButtonRestore();
	afx_msg void	OnBnClickedButtonCompress();
	afx_msg void	OnBnClickedButtonDecompress();
	afx_msg void	OnBnClickedButtonDelete();
	afx_msg LRESULT	OnCompleteLogin(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCompleteGetCommonList(WPARAM wParam, LPARAM lParam);

	CComboBox		m_cbxFilterState;
	CComboBox		m_cbxFilterType;
	CEdit			m_editFilterKeyword;
	CButton			m_btnFilterRefresh;

	CWaitingListCtrl	m_lcContents;

	CButton			m_btnModify;
	CButton			m_btnAddNew;
	CButton			m_btnCopyToPrivate;
	CButton			m_btnCopyFromPrivate;
	CButton			m_btnVerify;
	CButton			m_btnEncoding;
	CButton			m_btnBackup;
	CButton			m_btnRestore;
	CButton			m_btnCompress;
	CButton			m_btnDecompress;
	CButton			m_btnDestroy;
	CButton			m_btnClose;

	CStatic			m_group1;
	CStatic			m_group2;
	CStatic			m_group3;

public:
	void			SetDocument(CUBCStudioDoc* pDoc) { m_pDocument=pDoc; };

	void			SetSiteInfo(LPCTSTR lpszSiteID, LPCTSTR lpszProgramID)
							{ m_strCSiteID=lpszSiteID; m_strProgramID=lpszProgramID; };
	void			SetUserInfo(LPCTSTR lpszSiteID, LPCTSTR lpszUserID, LPCTSTR lpszPassword)
							{ m_strUSiteID=lpszSiteID; m_strUserID=lpszUserID; m_strPassword=lpszPassword; };

	CONTENTS_INFO_LIST*		GetContentsUpdatedList() { m_bUpdatedListDetached=true; return m_listUpdated; };
	bool			IsContentsUpadted() { if(m_listUpdated) return (m_listUpdated->GetCount()>0); return false; };

};

