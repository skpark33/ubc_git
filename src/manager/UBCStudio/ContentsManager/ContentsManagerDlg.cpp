// ContentsManagerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "ReposControl2.h"
#include "ContentsManagerDlg.h"

#include "ProcessingDlg.h"

#include "ThreadFunc.h"
#include "Schedule.h"

#include "ContentsDialog.h"
#include "ubccopcommon\ftpmultisite.h"
#include "Enviroment.h"
#include "ContentsListCtrl.h"

#include "common/PreventChar.h"

CMutex		g_mutexThreadMain;
int			g_nThreadMain = 0;

CString		WEB_SERVER_IP = _T("");

CReposControl2	g_reposControl;


// CContentsManagerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CContentsManagerDlg, CDialog)

CContentsManagerDlg::CContentsManagerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_CONTENTS_MANAGER, pParent)
	, m_strCSiteID ( _T("") )
	, m_strProgramID ( _T("") )
	, m_strUSiteID ( _T("") )
	, m_strUserID ( _T("") )
	, m_strPassword ( _T("") )	
	, m_pThread ( NULL )
	, m_nUserPriv ( 0 )
	, m_bUpdatedListDetached ( false )
	, m_listUpdated ( NULL )
{
	if(WEB_SERVER_IP.GetLength() == 0)
	{
		char szBuf[1000] = {0};
		CString szPath;
		szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

		::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", szBuf, sizeof(szBuf), szPath);
		WEB_SERVER_IP = szBuf;
	}
}

CContentsManagerDlg::~CContentsManagerDlg()
{
	if(m_bUpdatedListDetached == false && m_listUpdated != NULL)
	{
		for(int i=0; i<m_listUpdated->GetCount(); i++)
		{
			CONTENTS_INFO* info = m_listUpdated->GetAt(i);
			delete info;
		}
		m_listUpdated->RemoveAll();
		delete m_listUpdated;
		m_listUpdated = NULL;
	}
}

void CContentsManagerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_STATE, m_cbxFilterState);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_cbxFilterType);
	DDX_Control(pDX, IDC_EDIT_KEYWORD, m_editFilterKeyword);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_btnFilterRefresh);
	DDX_Control(pDX, IDC_LIST_COMMON_CONTENTS, m_lcContents);
	DDX_Control(pDX, IDC_BUTTON_MODIFY, m_btnModify);
	DDX_Control(pDX, IDC_BUTTON_ADDNEW, m_btnAddNew);
	DDX_Control(pDX, IDC_BUTTON_COPY_TO_PRIVATE, m_btnCopyToPrivate);
	DDX_Control(pDX, IDC_BUTTON_COPY_FROM_PRIVATE, m_btnCopyFromPrivate);
	DDX_Control(pDX, IDC_BUTTON_VERIFY, m_btnVerify);
	DDX_Control(pDX, IDC_BUTTON_ENCODING, m_btnEncoding);
	DDX_Control(pDX, IDC_BUTTON_BACKUP, m_btnBackup);
	DDX_Control(pDX, IDC_BUTTON_RESTORE, m_btnRestore);
	DDX_Control(pDX, IDC_BUTTON_COMPRESS, m_btnCompress);
	DDX_Control(pDX, IDC_BUTTON_DECOMPRESS, m_btnDecompress);
	DDX_Control(pDX, IDC_BUTTON_DESTROY, m_btnDestroy);
	DDX_Control(pDX, IDCANCEL, m_btnClose);
	DDX_Control(pDX, IDC_STATIC_GROUP1, m_group1);
	DDX_Control(pDX, IDC_STATIC_GROUP2, m_group2);
	DDX_Control(pDX, IDC_STATIC_GROUP3, m_group3);
}


BEGIN_MESSAGE_MAP(CContentsManagerDlg, CDialog)
	ON_WM_GETMINMAXINFO()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CContentsManagerDlg::OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY, &CContentsManagerDlg::OnBnClickedButtonModify)
	ON_BN_CLICKED(IDC_BUTTON_ADDNEW, &CContentsManagerDlg::OnBnClickedButtonAddNew)
	ON_BN_CLICKED(IDC_BUTTON_COPY_TO_PRIVATE, &CContentsManagerDlg::OnBnClickedButtonCopyToPrivate)
	ON_BN_CLICKED(IDC_BUTTON_COPY_FROM_PRIVATE, &CContentsManagerDlg::OnBnClickedButtonCopyFromPrivate)
	ON_BN_CLICKED(IDC_BUTTON_VERIFY, &CContentsManagerDlg::OnBnClickedButtonVerify)
	ON_BN_CLICKED(IDC_BUTTON_ENCODING, &CContentsManagerDlg::OnBnClickedButtonEncoding)
	ON_BN_CLICKED(IDC_BUTTON_BACKUP, &CContentsManagerDlg::OnBnClickedButtonBackup)
	ON_BN_CLICKED(IDC_BUTTON_RESTORE, &CContentsManagerDlg::OnBnClickedButtonRestore)
	ON_BN_CLICKED(IDC_BUTTON_COMPRESS, &CContentsManagerDlg::OnBnClickedButtonCompress)
	ON_BN_CLICKED(IDC_BUTTON_DECOMPRESS, &CContentsManagerDlg::OnBnClickedButtonDecompress)
	ON_BN_CLICKED(IDC_BUTTON_DESTROY, &CContentsManagerDlg::OnBnClickedButtonDelete)
	ON_MESSAGE(WM_COMPLETE_LOGIN, OnCompleteLogin)
	ON_MESSAGE(WM_COMPLETE_GET_COMMON_LIST, OnCompleteGetCommonList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_COMMON_CONTENTS, &CContentsManagerDlg::OnNMDblclkListCommonContents)
END_MESSAGE_MAP()


// CContentsManagerDlg 메시지 처리기입니다.

BOOL CContentsManagerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	InitControl();

	g_reposControl.SetParent(this);
	g_reposControl.AddControl((CWnd*)&m_group1, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_group2, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_group3, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_lcContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	g_reposControl.AddControl((CWnd*)&m_btnFilterRefresh, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnModify, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnAddNew, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnCopyToPrivate, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnCopyFromPrivate, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnVerify, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnEncoding, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnBackup, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnRestore, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnCompress, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnDecompress, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnDestroy, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	g_reposControl.AddControl((CWnd*)&m_btnClose, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	SetOperationLevel(0, -1);

	SetTimer(TIMER_INIT_ID, TIMER_INIT_TIME, NULL);

	MoveWindow(0, 0, 800, 600, FALSE);
	CenterWindow();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CContentsManagerDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	DeleteThread();

	DeleteAllItems();

	m_lcContents.StopWaitingIcon();

	CDialog::OnCancel();
}

void CContentsManagerDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMinTrackSize.x = 620 + ::GetSystemMetrics(SM_CXSIZEFRAME)*2 + ::GetSystemMetrics(SM_CXDLGFRAME)*2 + ::GetSystemMetrics(SM_CXBORDER)*2;
	lpMMI->ptMinTrackSize.y = 440 + ::GetSystemMetrics(SM_CYSIZEFRAME)*2 + ::GetSystemMetrics(SM_CYDLGFRAME)*2 + ::GetSystemMetrics(SM_CYBORDER)*2 + ::GetSystemMetrics(SM_CYCAPTION);
}

void CContentsManagerDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	g_reposControl.MoveControl(TRUE);
}

void CContentsManagerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
	case TIMER_INIT_ID:
		KillTimer(TIMER_INIT_ID);
		DeleteThread();

		GUARD(g_mutexThreadMain);

			LOGIN_THREAD_PARAM* param = new LOGIN_THREAD_PARAM;
			param->hParentWnd = GetSafeHwnd();
			param->nThreadSerial = g_nThreadMain;
			param->strSiteID = m_strUSiteID;
			param->strUserID = m_strUserID;
			param->strPassword = m_strPassword;

			m_pThread = ::AfxBeginThread(LoginThreadFunc, (LPVOID)param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
			m_pThread->m_bAutoDelete = TRUE;
			m_pThread->ResumeThread();

			m_lcContents.StartWaitingIcon();

		END_GUARD;
		break;
	}
}

BOOL CContentsManagerDlg::DeleteThread()
{
	if(m_pThread)
	{
		CWaitMessageBox wait;
		GUARD(g_mutexThreadMain);
			g_nThreadMain++;
		END_GUARD;

		try
		{
			DWORD dwExitCode;

			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, 3000);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}

			//delete m_pThread;
		}
		catch(...)
		{
		}

		m_pThread = NULL;
		
	}

	return TRUE;
}

void CContentsManagerDlg::InitControl()
{
	//
	m_cbxFilterState.AddString(LoadStringById(IDS_CM_CONTENTS_TYPE_ALL));//_T("전체"));
	m_cbxFilterState.AddString(GetContentsStateString(0));//_T("작업중"));
	m_cbxFilterState.AddString(GetContentsStateString(1));//_T("작업완료"));
	m_cbxFilterState.AddString(GetContentsStateString(2));//_T("검증됨"));
	m_cbxFilterState.AddString(GetContentsStateString(4));//_T("백업"));
	m_cbxFilterState.AddString(GetContentsStateString(8));//_T("압축"));
	m_cbxFilterState.SetCurSel(0);

	//
	m_cbxFilterType.AddString(GetContentsTypeString(-1));					//_T("전체"));
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_VIDEO));		//_T("동영상"));		// 0	CONTENTS_VIDEO
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_IMAGE));		//_T("이미지"));		// 2	CONTENTS_IMAGE
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_SMS));			//_T("티커"));			// 1	CONTENTS_SMS
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_FLASH));		//_T("플래시"));		// 8	CONTENTS_FLASH
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_TEXT));		//_T("텍스트"));		// 12	CONTENTS_TEXT
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_WEBBROWSER));	//_T("웹"));			// 7	CONTENTS_WEBBROWSER
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_PPT));			//_T("파워포인트"));	// 16	CONTENTS_PPT
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_TV));			//_T("TV"));			// 4	CONTENTS_TV
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_RSS));			//_T("RSS"));			// 10	CONTENTS_RSS
	m_cbxFilterType.AddString(GetContentsTypeString(CONTENTS_WIZARD));		//_T("마법사"));		// 18	CONTENTS_WIZARD
	m_cbxFilterType.SetCurSel(0);

	//
	m_lcContents.SetExtendedStyle(m_lcContents.GetExtendedStyle() | LVS_EX_FULLROWSELECT);

	LISTCTRL_COLUMN_INFO info[COLUMN_COUNT] = {
		//{ "", idx, width, align },
		{ LoadStringById(IDS_CM_LC_COLUMN_CONTENTS_STATE), COLUMN_STATUS,   100, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_CONTENTS_TYPE),  COLUMN_TYPE,     100, LVCFMT_LEFT },
//		{ LoadStringById(IDS_CM_LC_COLUMN_CONTENTS_ID),    COLUMN_ID,       150, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_CONTENTS_NAME),  COLUMN_NAME,     250, LVCFMT_LEFT },
		{ LoadStringById(IDS_CM_LC_COLUMN_FILENAME),       COLUMN_FILENAME, 250, LVCFMT_LEFT },
	};

	InitListCtrlColumn(m_lcContents, info, COLUMN_COUNT);
}

int CContentsManagerDlg::GetFilterTypeEnum(int idx)
{
	switch(idx)
	{
	case 1:
		return CONTENTS_VIDEO;
	case 2:
		return CONTENTS_IMAGE;
	case 3:
		return CONTENTS_SMS;
	case 4:
		return CONTENTS_FLASH;
	case 5:
		return CONTENTS_TEXT;
	case 6:
		return CONTENTS_WEBBROWSER;
	case 7:
		return CONTENTS_PPT;
	case 8:
		return CONTENTS_TV;
	case 9:
		return CONTENTS_RSS;
	case 10:
		return CONTENTS_WIZARD;
	}

	return CONTENTS_NOT_DEFINE;
}

CString CContentsManagerDlg::GetFilterStateString(int idx)
{
	switch(idx)
	{
	case 1:
		return CString(_T("working"));
	case 2:
		return CString(_T("encoded"));
	case 3:
		return CString(_T("verified"));
	case 4:
		return CString(_T("backup"));
	case 5:
		return CString(_T("compress"));
	}
	return CString(_T(""));
}

CString CContentsManagerDlg::GetContentsTypeString(int contents_type)
{
	switch(contents_type)
	{
	case CONTENTS_VIDEO:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_VIDEO);
	case CONTENTS_IMAGE:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_IMAGE);
	case CONTENTS_SMS:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_SMS);
	case CONTENTS_FLASH:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_FLASH);
	case CONTENTS_TEXT:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_TEXT);
	case CONTENTS_WEBBROWSER:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_WEBBROWSER);
	case CONTENTS_PPT:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_PPT);
	case CONTENTS_TV:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_TV);
	case CONTENTS_RSS:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_RSS);
	case CONTENTS_WIZARD:
		return LoadStringById(IDS_CM_CONTENTS_TYPE_WIZARD);
	}

	return LoadStringById(IDS_CM_CONTENTS_TYPE_ALL);
}

CString CContentsManagerDlg::GetContentsStateString(int contents_state)
{
	if(contents_state & 0x000008)
	{
		return LoadStringById(IDS_CM_CONTENTS_STATE_COMPRESS);
	}
	else if(contents_state & 0x000004)
	{
		return LoadStringById(IDS_CM_CONTENTS_STATE_BACKUP);
	}
	else if(contents_state & 0x000002)
	{
		return LoadStringById(IDS_CM_CONTENTS_STATE_VERIFIED);
	}
	else if(contents_state & 0x000001)
	{
		return LoadStringById(IDS_CM_CONTENTS_STATE_ENCODED);
	}
	return LoadStringById(IDS_CM_CONTENTS_STATE_WORKING);
}

void CContentsManagerDlg::SetOperationLevel(int nUserPriv, int nContentsState)
{
	m_btnModify.EnableWindow(TRUE);
	m_btnAddNew.EnableWindow(TRUE);
	m_btnCopyToPrivate.EnableWindow(TRUE);
	m_btnCopyFromPrivate.EnableWindow(TRUE);
	m_btnVerify.EnableWindow(nUserPriv==1||nUserPriv==2 ? TRUE : FALSE);
	m_btnEncoding.EnableWindow(TRUE);
	m_btnBackup.EnableWindow(TRUE);
	m_btnRestore.EnableWindow(TRUE);
	m_btnCompress.EnableWindow(TRUE);
	m_btnDecompress.EnableWindow(TRUE);
	m_btnDestroy.EnableWindow(nUserPriv==1||nUserPriv==2 ? TRUE : FALSE);
/*
	//
	m_btnModify.EnableWindow(FALSE);
	m_btnAddNew.EnableWindow(FALSE);
	m_btnCopyToPrivate.EnableWindow(FALSE);
	m_btnCopyFromPrivate.EnableWindow(FALSE);
	m_btnVerify.EnableWindow(FALSE);
	m_btnEncoding.EnableWindow(FALSE);
	m_btnBackup.EnableWindow(FALSE);
	m_btnRestore.EnableWindow(FALSE);
	m_btnCompress.EnableWindow(FALSE);
	m_btnDecompress.EnableWindow(FALSE);
	m_btnDestroy.EnableWindow(FALSE);
	switch(nContentsState)
	{
	case 0: // working
		m_btnAddNew.EnableWindow(TRUE);
		break;
	case 1: // encoded
		m_btnModify.EnableWindow(TRUE);
		m_btnAddNew.EnableWindow(TRUE);
		m_btnVerify.EnableWindow(nUserPriv==1||nUserPriv==2 ? TRUE : FALSE);
		m_btnEncoding.EnableWindow(TRUE);
		m_btnBackup.EnableWindow(TRUE);
		m_btnCompress.EnableWindow(TRUE);
		m_btnDestroy.EnableWindow(nUserPriv==1||nUserPriv==2 ? TRUE : FALSE);
		break;
	case 2: // verified
		m_btnModify.EnableWindow(TRUE);
		m_btnAddNew.EnableWindow(TRUE);
		m_btnCopyToPrivate.EnableWindow(TRUE);
		m_btnEncoding.EnableWindow(TRUE);
		m_btnBackup.EnableWindow(TRUE);
		m_btnCompress.EnableWindow(TRUE);
		m_btnDestroy.EnableWindow(nUserPriv==1||nUserPriv==2 ? TRUE : FALSE);
		break;
	case 3: // backup
		m_btnModify.EnableWindow(TRUE);
		m_btnAddNew.EnableWindow(TRUE);
		m_btnRestore.EnableWindow(TRUE);
		m_btnCompress.EnableWindow(TRUE);
		m_btnDestroy.EnableWindow(nUserPriv==1||nUserPriv==2 ? TRUE : FALSE);
		break;
	case 4: // compress
		m_btnAddNew.EnableWindow(TRUE);
		m_btnDecompress.EnableWindow(TRUE);
		m_btnDestroy.EnableWindow(nUserPriv==1||nUserPriv==2 ? TRUE : FALSE);
		break;
	}
*/
}

BOOL CContentsManagerDlg::GetContentsObjectList(LPVOID pContentsObjList, CStringArray& str_list)
{
	CContentsObjectList* obj_list = (CContentsObjectList*)pContentsObjList;

	CContentsObject* obj = NULL;

	for(int i=0; i<str_list.GetCount(); i++)
	{
		const CString& token = str_list.GetAt(i);

		int equal_idx = token.Find(_T("="));

		if(_tcsncmp((LPCTSTR)token, _T("contentsId"), 10) == 0 && equal_idx > 0 && token.GetLength() >= equal_idx+1)
		{
			obj = NULL;
			if(token.GetLength() > 11)
			{
				obj = new CContentsObject();

				obj->strId = (LPCTSTR)token+equal_idx+1;
				obj->strLocalLocation.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
			}

			if(obj) obj_list->Add(obj);
		}
		else if(equal_idx > 0 && token.GetLength() >= equal_idx+1 )
		{
			int pos = 0;
			CString key = token.Tokenize(_T("="), pos);

			if(obj)
			{
				LPCTSTR szValue = (LPCTSTR)token+equal_idx+1;

				//if(strcmp(key, "mgrId") == 0)
				//else if(strcmp(key, "siteId") == 0)
				//else if(strcmp(key, "programId") == 0)
				if(strcmp(key, "contentsId") == 0) obj->strId = szValue;
				//else if(strcmp(key, "category") == 0) obj->category = szValue;
				//else if(strcmp(key, "promotionId") == 0) obj->promotionId = szValue;
				else if(strcmp(key, "contentsName") == 0) obj->strContentsName = szValue;
				else if(strcmp(key, "contentsType") == 0) obj->nContentsType = _ttoi(szValue);
				//else if(strcmp(key, "parentsId") == 0) obj->parentsId = szValue;
				//else if(strcmp(key, "registerId") == 0) obj->registerId = szValue;
				//else if(strcmp(key, "registerTime") == 0) obj->registerTime = szValue;
				//else if(strcmp(key, "isRaw") == 0) obj->isRaw = szValue;
				else if(strcmp(key, "contentsState") == 0) obj->nContentsState = _ttoi(szValue);
				//else if(strcmp(key, "contentsStateTime") == 0) obj->contentsStateTime = szValue;
				//else if(strcmp(key, "contentsStateHistory") == 0) obj->contentsStateHistory = szValue;
				else if(strcmp(key, "location") == 0) obj->strServerLocation = szValue;
				else if(strcmp(key, "filename") == 0) obj->strFilename = szValue;
				else if(strcmp(key, "width") == 0) obj->nWidth = _ttoi(szValue);
				else if(strcmp(key, "height") == 0) obj->nHeight = _ttoi(szValue); 
				else if(strcmp(key, "volume") == 0) obj->nFilesize = _ttoi(szValue);
				else if(strcmp(key, "runningTime") == 0) obj->nRunningTime = _ttoi(szValue);
				//else if(strcmp(key, "verifier") == 0) obj->verifier = szValue;
				//else if(strcmp(key, "duration") == 0) obj->duration = szValue;
				//else if(strcmp(key, "videoCodec") == 0) obj->videoCodec = szValue;
				//else if(strcmp(key, "audioCodec") == 0) obj->audioCodec = szValue;
				//else if(strcmp(key, "encodingInfo") == 0) obj->encodingInfo = szValue;
				//else if(strcmp(key, "encodingTime") == 0) obj->encodingTime = szValue;
				//else if(strcmp(key, "description") == 0) obj->description = szValue;
				else if(strcmp(key, "comment1") == 0) obj->strComment[0] = szValue;
				else if(strcmp(key, "comment2") == 0) obj->strComment[1] = szValue;
				else if(strcmp(key, "comment3") == 0) obj->strComment[2] = szValue;
				else if(strcmp(key, "comment4") == 0) obj->strComment[3] = szValue;
				else if(strcmp(key, "comment5") == 0) obj->strComment[4] = szValue;
				else if(strcmp(key, "comment6") == 0) obj->strComment[5] = szValue;
				else if(strcmp(key, "comment7") == 0) obj->strComment[6] = szValue;
				else if(strcmp(key, "comment8") == 0) obj->strComment[7] = szValue;
				else if(strcmp(key, "comment9") == 0) obj->strComment[8] = szValue;
				else if(strcmp(key, "comment10") == 0) obj->strComment[9] = szValue;
				else if(strcmp(key, "currentComment") == 0) obj->nCurrentComment = _ttoi(szValue);
				else if(strcmp(key, "bgColor") == 0) obj->strBgColor = szValue;
				else if(strcmp(key, "fgColor") == 0) obj->strFgColor = szValue;
				else if(strcmp(key, "font") == 0) obj->strFont = szValue;
				else if(strcmp(key, "fontSize") == 0) obj->nFontSize = _ttoi(szValue);
				else if(strcmp(key, "playSpeed") == 0) obj->nPlaySpeed = _ttoi(szValue);
				else if(strcmp(key, "soundVolume") == 0) obj->nSoundVolume = _ttoi(szValue);
				//else if(strcmp(key, "promotionValueList") == 0) obj->promotionValueList = szValue;
				//else if(strcmp(key, "snapshotFile") == 0) obj->snapshotFile = szValue;
				else if(strcmp(key, "wizardFiles") == 0) obj->strWizardFiles = szValue;
				//else if(strcmp(key, "isPublic") == 0) obj->isPublic = szValue;
				//else if(strcmp(key, "serialNo") == 0) obj->serialNo = szValue;
				//else if(strcmp(key, "tvInputType") == 0) obj->tvInputType = szValue;
				//else if(strcmp(key, "tvChannel") == 0) obj->tvChannel = szValue;
				//else if(strcmp(key, "tvPortType") == 0) obj->tvPortType = szValue;
				else if(strcmp(key, "direction") == 0) obj->nDirection = _ttoi(szValue);
				else if(strcmp(key, "align") == 0) obj->nAlign = _ttoi(szValue);
				//else if(strcmp(key, "isUsed") == 0) obj->isUsed = szValue;
				else if(strcmp(key, "wizardXML") == 0) obj->strWizardXML = szValue;
			}
		}
	}

	for(int i=0; i<obj_list->GetCount(); i++)
	{
		obj = obj_list->GetAt(i);

		if(obj->strFilename.IsEmpty())
			obj->bLocalFileExist = true;
		else
		{
			CString fullpath;
			fullpath.Format(_T("%s\\%s%s"), GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, obj->strFilename);

			CFileStatus fs;
			if(CFile::GetStatus(fullpath, fs) && fs.m_size == obj->nFilesize)
			{
				obj->bLocalFileExist = true;
			}
		}
	}

	return TRUE;
}

BOOL CContentsManagerDlg::DeleteAllItems()
{
	int count = m_lcContents.GetItemCount();
	for(int i=0; i<count; i++)
	{
		CContentsObject* obj = (CContentsObject*)m_lcContents.GetItemData(i);
		delete obj;
	}

	return m_lcContents.DeleteAllItems();
}

BOOL CContentsManagerDlg::InsertItem(LPVOID pObject, int idx)
{
	CContentsObject* obj = (CContentsObject*)pObject;

	if(idx < 0)
	{
		idx = m_lcContents.GetItemCount();
		m_lcContents.InsertItem(idx, _T(""));
	}

	m_lcContents.SetItemText(idx, COLUMN_STATUS, GetContentsStateString(obj->nContentsState));
	m_lcContents.SetItemText(idx, COLUMN_TYPE, GetContentsTypeString(obj->nContentsType));
//	m_lcContents.SetItemText(idx, COLUMN_ID, obj->strId);
	m_lcContents.SetItemText(idx, COLUMN_NAME, obj->strContentsName);
	m_lcContents.SetItemText(idx, COLUMN_FILENAME, obj->strFilename);

	m_lcContents.SetItemData(idx, (DWORD)obj);

	return TRUE;
}

void CContentsManagerDlg::GetSelectItemList(CContentsObjectList& obj_list)
{
	for(int i=0; i<m_lcContents.GetItemCount(); i++)
	{
		UINT select = m_lcContents.GetItemState(i, LVIS_SELECTED);

		if(select)
		{
			CContentsObject* obj = (CContentsObject*)m_lcContents.GetItemData(i);
			obj->processingResultString = _T("");
			obj_list.Add(obj);
		}
	}
}

void CContentsManagerDlg::OnNMDblclkListCommonContents(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	OnBnClickedButtonModify();
}

void CContentsManagerDlg::OnBnClickedButtonRefresh()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	DeleteThread();
	DeleteAllItems();

	GUARD(g_mutexThreadMain);

		GET_COMMON_LIST_THREAD_PARAM* param = new GET_COMMON_LIST_THREAD_PARAM;
		param->hParentWnd = GetSafeHwnd();
		param->nThreadSerial = g_nThreadMain;
		//param->strContentsId;
		param->nContentsType = GetFilterTypeEnum(m_cbxFilterType.GetCurSel());
		m_editFilterKeyword.GetWindowText( param->strKeyword);
		param->strContentsState = GetFilterStateString(m_cbxFilterState.GetCurSel());

		m_pThread = ::AfxBeginThread(GetCommonListThreadFunc, (LPVOID)param, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = TRUE;
		m_pThread->ResumeThread();

		SetOperationLevel(m_nUserPriv, m_cbxFilterState.GetCurSel());

		m_lcContents.StartWaitingIcon();
	END_GUARD;
}

void CContentsManagerDlg::OnBnClickedButtonModify()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CProcessingDlg down_dlg;
	int down_cnt = 0;
	int modify_cnt = 0;

	CContentsObjectList obj_list;

	GetSelectItemList(obj_list);

	for(int i=0; i<obj_list.GetCount(); i++)
	{
		CContentsObject* obj = (CContentsObject*)obj_list.GetAt(i);

		if(obj->bLocalFileExist == false)
		{
			down_dlg.SetSiteInfo(m_strCSiteID, m_strProgramID);
			down_dlg.AddDownloadContents(obj);
			down_cnt++;
		}
	}

	if(down_cnt > 0)
	{
		down_dlg.DoModal();
	}

	if(obj_list.GetCount() > 0)
	{
		CContentsObjectList modified_list;

		for(int i=0; i<obj_list.GetCount(); i++)
		{
			CContentsObject* info = obj_list.GetAt(i);

			CContentsDialog dlg;
			dlg.SetDocument(m_pDocument);
			dlg.SetContentsInfo(*(CONTENTS_INFO*)info);
			dlg.SetPreviewMode(info->bIsPublic);

			CWaitMessageBox wait;
			if(dlg.DoModal() == IDOK)
			{
				dlg.GetContentsInfo(*(CONTENTS_INFO*)info);

				modified_list.Add(info);
			}
		}

		if(modified_list.GetCount() > 0)
		{
			CProcessingDlg down_dlg;

			for(int i=0; i<modified_list.GetCount(); i++)
			{
				CContentsObject* obj = modified_list.GetAt(i);
				down_dlg.AddModifyContents(obj);
			}

			down_dlg.DoModal();

			OnBnClickedButtonRefresh();
		}
	}
}

void CContentsManagerDlg::OnBnClickedButtonAddNew()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//CContentsObject* obj = new CContentsObject;

	//obj->AddAttribute(_T("mgrId"), _T("1"));
	//obj->AddAttribute(_T("contentsId"), _T("test02"));
	//obj->AddAttribute(_T("contentsName"), _T("test002"));
	//obj->AddAttribute(_T("contentsType"), _T("0"));
	//obj->AddAttribute(_T("location"), _T("/contents/COMMON/"));
	//obj->AddAttribute(_T("filename"), _T("test02.avi"));
	//obj->AddAttribute(_T("isPublic"), _T("1"));
	//obj->AddAttribute(_T("isUsed"), _T("1"));

	//obj->AddAttribute(_T("localPath"), _T("C:\\test02.avi"));

	//CProcessingDlg dlg;
	//dlg.AddUploadContents(obj);
	//dlg.DoModal();

	//delete obj;

	CContentsDialog dlg;
	dlg.SetDocument(m_pDocument);
	if(dlg.DoModal() != IDOK)
	{
		return;
	}//if

	CContentsObject* info = new CContentsObject;
	dlg.GetContentsInfo(*(CONTENTS_INFO*)info);
	info->strServerLocation = _T("/contents/COMMON/");
	info->nSoundVolume = 100;
	info->bLocalFileExist = true;

	if(info->strId.GetLength() == 0)
	{
		CString strTmpName;

		if(info->strFilename == "")
		{
			strTmpName = info->strContentsName;
			strTmpName.Replace(" ", "");
		}
		else
		{
			int nIdx = info->strFilename.Find('.', 0);
			if(nIdx != -1)
			{
				strTmpName = info->strFilename.Left(nIdx);
			}
			else
			{
				strTmpName = info->strFilename;
			}
		}

		// [], 는 (), 로 치환함
//		strTmpName.Replace("[", "(");
//		strTmpName.Replace("]", ")");
//		strTmpName.Replace(",", "_");
		strTmpName = CPreventChar::GetInstance()->ConvertToAvailableChar(strTmpName);

		info->strId = strTmpName;
	}


	if(info->strFilename != "")
	{
		//(콘텐츠 파일 정보를 ini 파일에 기록할 때, '[', ']' 문자가 있으면 오류 발생 하므로
		//이름을 바꾸도록 한다.
		CContentsListCtrl lc;
		if(!lc.ChangeContentsName((CONTENTS_INFO*)info))
		{
			delete info;
			return;
		}//if
		if(lc.OverWriteContentsFile(info) != CContentsListCtrl::eOWTRUE)
		{
			delete info;
			return;
		}//if
	}//if

	CProcessingDlg dlg_upload;
	dlg_upload.SetSiteInfo(m_strCSiteID, m_strProgramID);
	dlg_upload.AddUploadContents(info);
	dlg_upload.DoModal();

	OnBnClickedButtonRefresh();
}

void CContentsManagerDlg::OnBnClickedButtonCopyToPrivate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CContentsObjectList obj_list;

	GetSelectItemList(obj_list);

	if(obj_list.GetCount()> 0)
	{
		if(m_listUpdated == NULL)
			m_listUpdated = new CONTENTS_INFO_LIST;

		CProcessingDlg dlg;
		dlg.AddCopyToPrivateContents(obj_list);
		dlg.SetSiteInfo(m_strCSiteID, m_strProgramID);
		dlg.DoModal();
		if(dlg.GetContentsUpdatedCount() > 0)
		{
			CONTENTS_INFO_LIST*	info_list = dlg.GetContentsUpdatedList();
			for(int i=0; i<info_list->GetCount(); i++)
			{
				CONTENTS_INFO* info = info_list->GetAt(i);
				m_listUpdated->Add(info);
			}
			delete info_list;
		}
	}
}

void CContentsManagerDlg::OnBnClickedButtonCopyFromPrivate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//CContentsObjectList obj_list;

	//GetSelectItemList(obj_list);

	//if(obj_list.GetCount() > 0)
	//{
	//	CProcessingDlg dlg;
	//	dlg.AddCopyFromPrivateContents(obj_list);
	//	dlg.SetSiteInfo(m_strCSiteID, m_strProgramID);
	//	dlg.DoModal();
	//	OnBnClickedButtonRefresh();
	//}
}

void CContentsManagerDlg::OnBnClickedButtonVerify()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CContentsObjectList obj_list;

	GetSelectItemList(obj_list);

	if(obj_list.GetCount() > 0)
	{
		CProcessingDlg dlg;
		dlg.AddVerifyContents(obj_list);
		dlg.DoModal();
		OnBnClickedButtonRefresh();
	}
}

void CContentsManagerDlg::OnBnClickedButtonEncoding()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//CContentsObjectList obj_list;

	//GetSelectItemList(obj_list);

	//if(obj_list.GetCount() > 0)
	//{
	//	CProcessingDlg dlg;
	//	dlg.AddEncodeContents(obj_list);
	//	dlg.DoModal();
	//	OnBnClickedButtonRefresh();
	//}

	CString eocoder_path = _T("C:\\Program Files\\BadakEncoder\\BadakEncoder.exe");

	CFileStatus fs;
	if(CFile::GetStatus(eocoder_path, fs))
	{
		::ShellExecute(NULL, _T("Open"), eocoder_path, _T(""), _T(""), SW_SHOW );
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_CM_NOTIFY_MSG_NO_BADAK_ENCODER), MB_ICONEXCLAMATION);
	}
}

void CContentsManagerDlg::OnBnClickedButtonBackup()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CContentsObjectList obj_list;

	GetSelectItemList(obj_list);

	if(obj_list.GetCount() > 0)
	{
		CProcessingDlg dlg;
		dlg.AddBackupContents(obj_list);
		dlg.DoModal();
		OnBnClickedButtonRefresh();
	}
}

void CContentsManagerDlg::OnBnClickedButtonRestore()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CContentsObjectList obj_list;

	GetSelectItemList(obj_list);

	if(obj_list.GetCount() > 0)
	{
		CProcessingDlg dlg;
		dlg.AddRestoreContents(obj_list);
		dlg.DoModal();
		OnBnClickedButtonRefresh();
	}
}

void CContentsManagerDlg::OnBnClickedButtonCompress()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CContentsObjectList obj_list;

	GetSelectItemList(obj_list);

	if(obj_list.GetCount() > 0)
	{
		CProcessingDlg dlg;
		dlg.AddCompressContents(obj_list);
		dlg.DoModal();
		OnBnClickedButtonRefresh();
	}
}

void CContentsManagerDlg::OnBnClickedButtonDecompress()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CContentsObjectList obj_list;

	GetSelectItemList(obj_list);

	if(obj_list.GetCount() > 0)
	{
		CProcessingDlg dlg;
		dlg.AddDecompressContents(obj_list);
		dlg.DoModal();
		OnBnClickedButtonRefresh();
	}
}

void CContentsManagerDlg::OnBnClickedButtonDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CContentsObjectList obj_list;

	GetSelectItemList(obj_list);

	if(obj_list.GetCount() > 0)
	{
		CProcessingDlg dlg;
		dlg.AddDeleteContents(obj_list);
		dlg.DoModal();
		OnBnClickedButtonRefresh();
	}
}

LRESULT CContentsManagerDlg::OnCompleteLogin(WPARAM wParam, LPARAM lParam)
{
	m_lcContents.StopWaitingIcon();
	if(wParam)
	{
		CString title = LoadStringById(IDS_CM_TITLE);

		switch(lParam)
		{
		default:
		case 0:
			UbcMessageBox(LoadStringById(IDS_CM_NOTIFY_MSG_LOGIN_FAIL_CHECK_LOGIN_INFO));
			OnCancel();
			return 1;
			break;
		case 1:
			title += _T(" [Super User]");
			SetWindowText(title);
			break;
		case 2:
			title += _T(" [Site Manager]");
			SetWindowText(title);
			break;
		case 3:
			title += _T(" [Site Staff]");
			SetWindowText(title);
			break;
		}

		m_nUserPriv = wParam;

		OnBnClickedButtonRefresh();
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_CM_NOTIFY_MSG_LOGIN_FAIL_CHECK_NETWORK));
		OnCancel();
		return 1;
	}
	return 0;
}

LRESULT	CContentsManagerDlg::OnCompleteGetCommonList(WPARAM wParam, LPARAM lParam)
{
	m_lcContents.StopWaitingIcon();

	CString* response = (CString*)lParam;

	if(wParam)
	{
		CStringArray token_list;
		int pos = 0;
		CString token = response->Tokenize(_T("\r\n"), pos);

		while(token != _T(""))
		{
			token_list.Add(token);
			//TRACE(_T("%s\r\n"), token);
			token = response->Tokenize(_T("\r\n"), pos);
		}

		if(token_list.GetCount() > 0)
		{
			CContentsObjectList obj_list;

			GetContentsObjectList((LPVOID)&obj_list, token_list);

			int count = obj_list.GetCount();
			if(count > 0)
			{
				for(int i=0; i<count; i++)
				{
					CContentsObject* obj = (CContentsObject*)obj_list.GetAt(i);

					InsertItem((LPVOID)obj);
				}
			}
		}
		m_lcContents.Sort(0, true);
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_CM_NOTIFY_MSG_GET_LIST_FAIL_CHECK_NETWORK));
	}

	delete response;

	return 0;
}
