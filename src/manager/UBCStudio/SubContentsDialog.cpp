// SubContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "SubContentsDialog.h"
#include "common/PreventChar.h"
#include "Enviroment.h"

EXT_TYPE ext_type[] = {
	{".avi",	CONTENTS_VIDEO},
	{".asf",	CONTENTS_VIDEO},
	{".mkv",	CONTENTS_VIDEO},
	{".mp4",	CONTENTS_VIDEO},
	{".mpg",	CONTENTS_VIDEO},
	{".mpeg",	CONTENTS_VIDEO},
	{".wmv",	CONTENTS_VIDEO},
	{".mp3",	CONTENTS_VIDEO},
	{".wma",	CONTENTS_VIDEO},
	{".mov",	CONTENTS_VIDEO},
	{".tp",		CONTENTS_VIDEO},
	{".wav",	CONTENTS_VIDEO},
	{".flv",	CONTENTS_VIDEO},
	{".bmp",	CONTENTS_IMAGE},
	{".jpg",	CONTENTS_IMAGE},
	{".jpeg",	CONTENTS_IMAGE},
	{".gif",	CONTENTS_IMAGE},
	{".pcx",	CONTENTS_IMAGE},
	{".png",	CONTENTS_IMAGE},
	{".tif",	CONTENTS_IMAGE},
	{".tiff",	CONTENTS_IMAGE},
	{".swf",	CONTENTS_FLASH},
	{".ppt",	CONTENTS_PPT},
	{".pps",	CONTENTS_PPT},
	{".pptx",	CONTENTS_PPT},
	{".ppsx",	CONTENTS_PPT},
//	{".hwp",	CONTENTS_HWP},
//	{".nxl",	CONTENTS_EXCEL},
//	{".xls",	CONTENTS_EXCEL},
	{".txt",	CONTENTS_VIDEO},	// 텍스트파일을 단말에 내려보내기 위해 동영상콘텐츠로 간주하여 처리하도록 함.
	{".ttc",    CONTENTS_ETC},		// 2010.10.12 장광수 기타콘텐츠추가
	{".ttf",    CONTENTS_ETC},		// 2010.10.12 장광수 기타콘텐츠추가
	{".fon",    CONTENTS_ETC},		// 2010.10.12 장광수 기타콘텐츠추가
	{".tar",    CONTENTS_ETC},		// 2010.10.12 장광수 기타콘텐츠추가
	{"",		CONTENTS_NOT_DEFINE}
};

// CSubContentsDialog 대화 상자입니다.
IMPLEMENT_DYNAMIC(CSubContentsDialog, CDialog)

CSubContentsDialog::CSubContentsDialog(UINT nIDTemplate, CWnd* pParent /*=NULL*/)
	: CDialog(nIDTemplate, pParent)
	, m_pDocument(NULL)
	, m_bIsChangedFile(FALSE)	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
{
	m_ulFileSize = 0;
	m_brushBG.CreateSolidBrush(RGB(255,255,255));
}

CSubContentsDialog::~CSubContentsDialog()
{
}

void CSubContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSubContentsDialog, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

// CSubContentsDialog 메시지 처리기입니다.
HBRUSH CSubContentsDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int count = m_listNoCTLWnd.GetCount();
	for(int i=0; i<count; i++)
	{
		CWnd* wnd = m_listNoCTLWnd.GetAt(i);
		if(wnd->GetSafeHwnd() == pWnd->GetSafeHwnd())
			return hbr;
	}

	pDC->SetBkColor(RGB(255,255,255));
	//pDC->SetBkMode(TRANSPARENT);
	hbr = (HBRUSH)m_brushBG;

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

bool CSubContentsDialog::IsFitContentsName(CString strContentsName, bool bErrMsg/*=true*/)
{
	if(strContentsName.IsEmpty())
	{
		if(bErrMsg) UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG001), MB_ICONSTOP);
		return false;
	}
	if(CPreventChar::GetInstance()->HavePreventChar(strContentsName))
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_CONTENTSDIALOG_MSG006), CPreventChar::GetInstance()->GetPreventChar());
		if(bErrMsg) UbcMessageBox(strMsg, MB_ICONERROR);
		return false;
	}

	return true;
}

bool CSubContentsDialog::IsValidType(CString szExt, CONTENTS_TYPE conType)
{
	szExt.MakeLower();

	int nCnt = sizeof(ext_type)/sizeof(EXT_TYPE);

	for(int i = 0; i < nCnt; i++)
	{
		// 창일향 : 콘텐츠 바스켓에 Drag & Drop 으로 밀어넣을 수 있는 콘텐츠는 오직 JPG 파일 뿐이다
		if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer )
		{
			if( _tcsicmp(_T(".jpg") , ext_type[i].extension) != 0 &&
				_tcsicmp(_T(".jpeg"), ext_type[i].extension) != 0 )
			{
				continue;
			}
		}

		if(szExt != ext_type[i].extension)
			continue;

		if(conType == CONTENTS_NOT_DEFINE)
		{
			return true;
		}
		else if(conType == ext_type[i].type)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

bool CSubContentsDialog::CheckTotalContentsSize(CString strFilePath)
{
	if(strFilePath.IsEmpty()) return true;

	ULONGLONG lAddSize = 0;
	ULONGLONG lFileSize = 0;
	if(GetEnvPtr()->GetFileSize(strFilePath, lFileSize))
	{
		lAddSize = lFileSize - m_ulFileSize;
	}

	// 현재 콘텐츠의 변화량이 커지는 경우에만 체크함
	if(lAddSize > 0)
	{
		// 콘텐츠의 총 용량이 지정된 용량을 넘어서는지 체크
		if(!GetDocument()->CheckTotalContentsSize(lAddSize)) return false;
	}

	return true;
}

CUBCStudioDoc* CSubContentsDialog::GetDocument()
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCStudioDoc)));
	return (CUBCStudioDoc*)m_pDocument;
}