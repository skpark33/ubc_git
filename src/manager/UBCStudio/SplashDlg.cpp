// SplashDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "SplashDlg.h"

#define FILE_SPLASH_GIF		"splash2.gif"

// CSplashDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSplashDlg, CDialog)

CSplashDlg::CSplashDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSplashDlg::IDD, pParent)
{
}

CSplashDlg::~CSplashDlg()
{
#ifndef _DEBUG
	m_clsGif.UnLoad();
#endif
}

void CSplashDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSplashDlg, CDialog)
	
END_MESSAGE_MAP()


// CSplashDlg 메시지 처리기입니다.

BOOL CSplashDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
#ifndef _DEBUG
	CRect clsRect;
	GetClientRect(clsRect);

	m_clsGif.Create("Animated GIF", WS_CHILD|WS_VISIBLE, clsRect, this);

	//BOOL bRet = m_clsGif.Load(FILE_SPLASH_GIF);
	BOOL bRet = m_clsGif.Load(MAKEINTRESOURCE(IDR_SPLASH_GIF),_T("GIF"));

	SIZE stSize = m_clsGif.GetSize();
	this->MoveWindow(clsRect.left, clsRect.right, stSize.cx, stSize.cy);
	
	bRet = m_clsGif.Draw();
#endif

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
