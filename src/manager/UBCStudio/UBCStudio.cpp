// UBCStudio.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "MainFrm.h"
#include "UBCStudioDoc.h"
#include "LayoutFrame.h"
#include "LayoutView.h"
#include "PackageFrame.h"
#include "PackageView.h"
#include "PlayContentsSearchFrame.h"
#include "PlayContentsSearchView.h"
#include "Enviroment.h"
#include "common\libscratch\scratchUtil.h"
#include "common\UbcCode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CUBCStudioApp
BEGIN_MESSAGE_MAP(CUBCStudioApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CUBCStudioApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()

// CUBCStudioApp construction
CUBCStudioApp::CUBCStudioApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CUBCStudioApp object

CUBCStudioApp theApp;


// CUBCStudioApp initialization

BOOL CUBCStudioApp::InitInstance()
{
//TODO: call AfxInitRichEdit2() to initialize richedit2 library.
	/*
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetBreakAlloc(233);
#endif
	*/
	//AfxSetAllocStop(230);

	TraceLog(("InitInstance()"));
	// 프로세스 중복 실행 방지
	//if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE){
	//	HANDLE hMutex = ::CreateMutex(NULL,FALSE, "UBCStudio_EE");
	//	if (hMutex != NULL)
	//	{
	//		if(::GetLastError() == ERROR_ALREADY_EXISTS) {
	//			unsigned long lPid = scratchUtil::getInstance()->getPid(UBCSTUDIOEEEXE_STR);
	//			HWND hWnd = scratchUtil::getInstance()->getWHandle(lPid);

	//			if(hWnd){
	//				::ReleaseMutex(hMutex);
	//				UbcMessageBox(LoadStringById(IDS_UBCSTUDIOAPP_MSG001), MB_ICONERROR);
	//				exit(1);
	//				return FALSE;
	//			}
	//			// Windows Handle 이 없다면 좀비로 인식하고 죽임.
	//			TraceLog(("killProcess(%d)",lPid));
	//			scratchUtil::getInstance()->killProcess(lPid);
	//		}
	//		::ReleaseMutex(hMutex);
	//	}
	//}
	//else if(GetEnvPtr()->m_Edition == CEnviroment::eStudioPE){
	//	HANDLE hMutex = ::CreateMutex(NULL,FALSE, "UBCStudio_PE");
	//	if (hMutex != NULL)
	//	{
	//		if(::GetLastError() == ERROR_ALREADY_EXISTS) {
	//			unsigned long lPid = scratchUtil::getInstance()->getPid(UBCSTUDIOEXE_STR);
	//			HWND hWnd = scratchUtil::getInstance()->getWHandle(lPid);

	//			if(hWnd){
	//				::ReleaseMutex(hMutex);
	//				UbcMessageBox(LoadStringById(IDS_UBCSTUDIOAPP_MSG002), MB_ICONERROR);
	//				exit(1);
	//				return FALSE;
	//			}
	//			// Windows Handle 이 없다면 좀비로 인식하고 죽임.
	//			TraceLog(("killProcess(%d)",lPid));
	//			scratchUtil::getInstance()->killProcess(lPid);
	//		}
	//		::ReleaseMutex(hMutex);
	//	}
	//}

	// 0000691: Studio/Manager 기동시, 해당 프로그램을 방화벽 예외로 설정해주어야 함.
	//Firewall exception 등록
	if(!scratchUtil::getInstance()->AddToExceptionList())
	{
		TraceLog(("Fail to add windows firewall exception"));
	}

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	TraceLog(("CWinApp::InitInstance()"));
	CWinApp::InitInstance();

	// Initialize OLE libraries
	TraceLog(("AfxOleInit()"));
	if (!AfxOleInit())
	{
		UbcMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	AfxInitRichEdit();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization

	HMODULE hModule = LoadLibrary(_T("UBCStudioML.dll"));
	if(hModule) AfxSetResourceHandle(hModule);

	CString strCommandLine(m_lpCmdLine);
	if( strCommandLine.Find(_T("-L:eng")) >= 0 )
		SetThreadLocaleEx(MAKELCID(MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US),SORT_DEFAULT));
	else if( strCommandLine.Find(_T("-L:jpn")) >= 0 )
		SetThreadLocaleEx(MAKELCID(MAKELANGID(LANG_JAPANESE,SUBLANG_DEFAULT),SORT_DEFAULT));
	else if( strCommandLine.Find(_T("-L:kor")) >= 0 )
		SetThreadLocaleEx(MAKELCID(MAKELANGID(LANG_KOREAN,SUBLANG_KOREAN),SORT_DEFAULT));
	else
		SetThreadLocaleEx(MAKELCID(MAKELANGID(LANG_NEUTRAL,SUBLANG_SYS_DEFAULT),SORT_DEFAULT));

	SetRegistryKey(_T("SQISoft"));
	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	//CMultiDocTemplate* pDocTemplate;
#ifdef _UBCSTUDIO_EE_
	m_pLayoutTemplate = new CMultiDocTemplate(IDR_MAINFRAME_EE,
#else
	m_pLayoutTemplate = new CMultiDocTemplate(IDR_MAINFRAME,
#endif
		RUNTIME_CLASS(CUBCStudioDoc),
		RUNTIME_CLASS(CLayoutFrame), // custom MDI child frame
		RUNTIME_CLASS(CLayoutView));
	if (!m_pLayoutTemplate)
		return FALSE;
	AddDocTemplate(m_pLayoutTemplate);
#ifdef _UBCSTUDIO_EE_
	m_pPackageTemplate = new CMultiDocTemplate(IDR_MAINFRAME_EE,
#else
	m_pPackageTemplate = new CMultiDocTemplate(IDR_MAINFRAME,
#endif
		RUNTIME_CLASS(CUBCStudioDoc),
		RUNTIME_CLASS(CPackageFrame), // custom MDI child frame
		RUNTIME_CLASS(CPackageView));
	if (!m_pPackageTemplate)
		return FALSE;
	AddDocTemplate(m_pPackageTemplate);
#ifdef _UBCSTUDIO_EE_
	m_pPlayContentsSearchTemplate = new CMultiDocTemplate(IDR_MAINFRAME_EE,
#else
	m_pPlayContentsSearchTemplate = new CMultiDocTemplate(IDR_MAINFRAME,
#endif
		RUNTIME_CLASS(CUBCStudioDoc),
		RUNTIME_CLASS(CPlayContentsSearchFrame), // custom MDI child frame
		RUNTIME_CLASS(CPlayContentsSearchView));
	if (!m_pPlayContentsSearchTemplate)
		return FALSE;
	AddDocTemplate(m_pPlayContentsSearchTemplate);

	// create main MDI Frame window
	TraceLog(("new CMainFrame"));
	CMainFrame* pMainFrame = new CMainFrame;
#ifdef _UBCSTUDIO_EE_
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME_EE))
#else
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
#endif
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;
	// call DragAcceptFiles only if there's a suffix
	//  In an MDI app, this should occur immediately after setting m_pMainWnd
	// Enable drag/drop open
//	m_pMainWnd->DragAcceptFiles();

	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

/*
	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);


	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
*/
	// The main window has been initialized, so show and update it
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->CenterWindow();
	pMainFrame->UpdateWindow();

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE){
		free((void*)m_pszAppName);
		m_pszAppName=_tcsdup(_T("UBCStudio EE"));
	}
	else if(GetEnvPtr()->m_Edition == CEnviroment::eStudioPE){
		free((void*)m_pszAppName);
		m_pszAppName=_tcsdup(_T("UBCStudio PE"));
	}

	return TRUE;
}

int CUBCStudioApp::ExitInstance()
{
	CDataContainer::clearInstance();
	CUbcCode::ClearInstance();

	//if(m_pDispOM)
	//{
	//	delete m_pDispOM;
	//	m_pDispOM = NULL;
	//}

	return CWinApp::ExitInstance();
}

BOOL CUBCStudioApp::SetThreadLocaleEx(LCID lcLocale)
{
	OSVERSIONINFO osVersion;
	osVersion.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if (::GetVersionEx(&osVersion) == FALSE) return false;

	BOOL bResult = FALSE;

	// >= Windows Vista
	if (osVersion.dwMajorVersion == 6)
	{   
		HINSTANCE hKernelDll = ::LoadLibrary(_T("kernel32.dll"));  
		if (hKernelDll == NULL) return false;

		unsigned (WINAPI* SetThreadUILanguage)(LANGID) = (unsigned (WINAPI* )(LANGID))::GetProcAddress(hKernelDll, "SetThreadUILanguage");
		if (SetThreadUILanguage == NULL) return false;

		LANGID resLangID = SetThreadUILanguage(static_cast<LANGID>(lcLocale));
		bResult = (resLangID == LOWORD(lcLocale));
	}
	// <= Windows XP
	else 
	{
		bResult = ::SetThreadLocale(lcLocale);
	}

	return bResult;
}


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAboutDlg::OnBnClickedOk)
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString szCopyright, szVer, szUser, szSite,szCustomer;
	scratchUtil* su = scratchUtil::getInstance();

	std::string sVer;
	su->getVersion(sVer, false);

	if(CEnviroment::eSQISOFT == GetEnvPtr()->m_Customer){
		if(CEnviroment::eStudioPE == GetEnvPtr()->m_Edition){
			szCopyright  = "Copyright (C) 2009 SQI Soft Corporation.";
			szVer = "All rights reserved.";
			szSite.Format("Studio PE Version %s", sVer.c_str());
			szUser = "";
		}
		else if(CEnviroment::eStudioEE == GetEnvPtr()->m_Edition){
			szCopyright = "Copyright (C) 2009 SQI Soft Corporation.";
			szVer.Format("Studio EE Version %s", sVer.c_str());
			szSite = "Site : ";
			szSite += GetEnvPtr()->m_szSite;
			szUser = "User ID : ";
			szUser += GetEnvPtr()->m_szLoginID;
			szCustomer = "Customer ID : ";
			szCustomer += GetEnvPtr()->m_strCustomer;
		}
	}else{
		if(CEnviroment::eStudioPE == GetEnvPtr()->m_Edition){
			szCopyright  = "";
			szVer.Format("Studio PE Version %s", sVer.c_str());
			szSite = "";
			szUser = "";
		}
		else if(CEnviroment::eStudioEE == GetEnvPtr()->m_Edition){
			szCopyright = "";
			szVer.Format("Studio EE Version %s", sVer.c_str());
			szSite = "Site : ";
			szSite += GetEnvPtr()->m_szSite;
			szUser = "User ID : ";
			szUser += GetEnvPtr()->m_szLoginID;
			szCustomer = "Customer ID : ";
			szCustomer += GetEnvPtr()->m_strCustomer;

			CRect rc;
			GetDlgItem(IDC_ABOUT_COPYRIGHT)->GetClientRect(&rc);
			GetDlgItem(IDC_ABOUT_COPYRIGHT)->ClientToScreen(&rc);
			ScreenToClient(&rc);

			int nCY = rc.Height()/2;
			GetDlgItem(IDC_ABOUT_COPYRIGHT)->MoveWindow(rc.left, rc.top-rc.Height()+nCY, rc.Width(), rc.Height());
			GetDlgItem(IDC_ABOUT_VERSION)->MoveWindow(rc.left, rc.top+nCY, rc.Width(), rc.Height());
			GetDlgItem(IDC_STATIC_SITE)->MoveWindow(rc.left, rc.top+rc.Height()+nCY, rc.Width(), rc.Height());
			GetDlgItem(IDC_STATIC_USER)->MoveWindow(rc.left, rc.top+2*rc.Height()+nCY, rc.Width(), rc.Height());
			GetDlgItem(IDC_STATIC_CUSTOMER)->MoveWindow(rc.left, rc.top+3*rc.Height()+nCY, rc.Width(), rc.Height());
		}
	}

	GetDlgItem(IDC_ABOUT_COPYRIGHT)->SetWindowText(szCopyright);
	GetDlgItem(IDC_ABOUT_VERSION)->SetWindowText(szVer);
	GetDlgItem(IDC_STATIC_SITE)->SetWindowText(szSite);
	GetDlgItem(IDC_STATIC_USER)->SetWindowText(szUser);
	GetDlgItem(IDC_STATIC_CUSTOMER)->SetWindowText(szCustomer);

	CStatic* pLogo = (CStatic*)GetDlgItem(IDC_ABOUT_LOGO);
	if(CEnviroment::eNARSHA == GetEnvPtr()->m_Customer){
		pLogo->SetIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME_NS));
	}else{
		pLogo->SetIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME));
	}

	return TRUE;
}

// App command to run the dialog
void CUBCStudioApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

void CAboutDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}
