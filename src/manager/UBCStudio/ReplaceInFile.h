#pragma once

class CReplaceInFile
{
	CMapStringToString m_mapItems;

public:
	CReplaceInFile(void);
	~CReplaceInFile(void);

	void AddItem(CString strSrc, CString strDst);
	void DeleteItem(CString strSrc);
	void DeleteAllItem();
	int  GetCount();

	int  ReplaceInFile(CString strSrcFilename, CString strDstFilename=_T(""), BOOL bNoCase=TRUE);

private:
	BOOL LoadFile(CString strFilename, CString& strContext);
	BOOL SaveFile(CString strFilename, CString strContext);
};
