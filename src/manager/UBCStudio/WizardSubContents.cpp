// WizardSubContents.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "WizardSubContents.h"
#include "Enviroment.h"


// CWizardSubContents 대화 상자입니다.
IMPLEMENT_DYNAMIC(CWizardSubContents, CDialog)

CWizardSubContents::CWizardSubContents(UINT nIDTemplate, CWnd* pParent /*=NULL*/)
	: CDialog(nIDTemplate, pParent)
	, m_pDocument(NULL)
	, m_bPreviewMode(FALSE)
{
	m_brushBG.CreateSolidBrush(RGB(255,255,255));
}

CWizardSubContents::~CWizardSubContents()
{
}

void CWizardSubContents::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

void CWizardSubContents::OnOK(){}
void CWizardSubContents::OnCancel(){}

BEGIN_MESSAGE_MAP(CWizardSubContents, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CWizardSubContents 메시지 처리기입니다.

HBRUSH CWizardSubContents::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	int count = m_listNoCTLWnd.GetCount();
	for(int i=0; i<count; i++)
	{
		CWnd* wnd = m_listNoCTLWnd.GetAt(i);
		if(wnd->GetSafeHwnd() == pWnd->GetSafeHwnd())
			return hbr;
	}

	pDC->SetBkColor(RGB(255,255,255));
	//pDC->SetBkMode(TRANSPARENT);
	hbr = (HBRUSH)m_brushBG;

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

CUBCStudioDoc* CWizardSubContents::GetDocument()
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCStudioDoc)));
	return (CUBCStudioDoc*)m_pDocument;
}