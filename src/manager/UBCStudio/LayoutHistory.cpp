#include "stdafx.h"
#include "LayoutHistory.h"

CLayoutHistory* CLayoutHistory::m_pLayoutHistory = NULL;

CLayoutHistory::CLayoutHistory()
{
	m_PosStat = eStart;
	m_CurPos = NULL;
}

CLayoutHistory::~CLayoutHistory()
{
	POSITION pos = m_listInfo.GetHeadPosition();
	while(pos){
		SInfo* pInfo = m_listInfo.GetNext(pos);
		if(pInfo)	continue;
		if(pInfo->pFrame){
			delete pInfo->pFrame;
			pInfo->pFrame = NULL;
		}
		if(pInfo->pTemplate){
			delete pInfo->pTemplate;
			pInfo->pTemplate = NULL;
		}
		delete pInfo;
		pInfo = NULL;
	}
	m_listInfo.RemoveAll();
}

CLayoutHistory* CLayoutHistory::GetObject()
{
	if(m_pLayoutHistory)
		return m_pLayoutHistory;
	m_pLayoutHistory = new CLayoutHistory;
	return m_pLayoutHistory;
}

void CLayoutHistory::Release()
{
	if(m_pLayoutHistory){
		delete m_pLayoutHistory;
		m_pLayoutHistory = NULL;
	}
}

void CLayoutHistory::Add(FRAME_LINK* pFrame, TEMPLATE_LINK* pTemplate)
{
	SInfo* pInfo = NULL;
	POSITION pos = m_CurPos;

	if(eMiddle == m_PosStat){
		pos = m_listInfo.GetTailPosition();
		while(pos){
			pInfo = m_listInfo.GetPrev(pos);

			if(pInfo)	continue;
			
			if(pInfo->pFrame){
				delete pInfo->pFrame;
				pInfo->pFrame = NULL;
			}
			
			if(pInfo->pTemplate){
				delete pInfo->pTemplate;
				pInfo->pTemplate = NULL;
			}
			
			delete pInfo;
			pInfo = NULL;
			m_listInfo.RemoveTail();
			
			if(pos == m_CurPos)
				break;			
		}
	}

	m_PosStat = eEnd;

	pInfo = new SInfo;
	pInfo->pFrame = pFrame;
	pInfo->pTemplate = pTemplate;
	m_listInfo.AddTail(pInfo);
	m_CurPos = m_listInfo.GetTailPosition();
}

bool CLayoutHistory::GetPrev(FRAME_LINK* pFrame, TEMPLATE_LINK* pTemplate)
{
	if(eStart == m_PosStat)
		return false;

	SInfo* pInfo = m_listInfo.GetPrev(m_CurPos);

	if(m_CurPos){
		m_PosStat = eMiddle;
	}else{
		m_PosStat = eStart;
	}

	if(pInfo){
		pFrame = pInfo->pFrame;
		pTemplate = pInfo->pTemplate;
	}else{
		pFrame = NULL;
		pTemplate = NULL;
	}
	
	return true;
}

bool CLayoutHistory::GetNext(FRAME_LINK* pFrame, TEMPLATE_LINK* pTemplate)
{
	if(eEnd == m_PosStat)
		return false;

	SInfo* pInfo = NULL;
	POSITION pos = m_CurPos;

	m_listInfo.GetNext(pos);
	if(!pos)	return false;

	pInfo = m_listInfo.GetNext(pos);
	m_CurPos = pos;

	if(m_CurPos){
		m_PosStat = eMiddle;
	}else{
		m_PosStat = eEnd;
	}

	if(pInfo){
		pFrame = pInfo->pFrame;
		pTemplate = pInfo->pTemplate;
	}else{
		pFrame = NULL;
		pTemplate = NULL;
	}
	
	return true;
}

CLayoutHistory::EStat CLayoutHistory::GetStat()
{
	return m_PosStat;
}