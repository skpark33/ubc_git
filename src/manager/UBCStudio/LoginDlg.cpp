// LoginDlg.cpp : implementation file
//
#include "stdafx.h"
#include "LoginDlg.h"
#include "Enviroment.h"
#include "CopModule.h"

#include "common\libscratch\orbconn.h"
#include "common\libinstall\installUtil.h"

//#include "libTimeSyncer/TimeSyncTimer.h"
//#ifdef _DEBUG
//	#pragma comment(lib, "libTimeSyncer_d.lib")
//#else
//	#pragma comment(lib, "libTimeSyncer.lib")
//#endif

const UINT WM_WAIT_FOR_LOGIN = RegisterWindowMessage(_T("WM_WAIT_FOR_LOGIN"));

static BOOL ConnectTest(CString strIp, short nPort)
{
	TraceLog(("ConnectTest IP:%s:%d", strIp, nPort));

	// open
	SOCKET hSocket = INVALID_SOCKET;
	hSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(hSocket == INVALID_SOCKET) return FALSE;

	TraceLog(("create socket"));

	// connect
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr((LPCTSTR)strIp);
	addr.sin_port = htons(nPort);
	if(	connect(hSocket, (sockaddr*)&addr, sizeof(addr)) == SOCKET_ERROR ) return FALSE;

	TraceLog(("connect"));

	// close
	closesocket(hSocket);
	return TRUE;
}

// CLoginDlg dialog
IMPLEMENT_DYNAMIC(CLoginDlg, CDialog)

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoginDlg::IDD, pParent)
{
	m_bInit = false;
	m_nPort = 0;
	m_szIP.Empty();
	m_szName.Empty();
	m_pLoginThread = NULL;
}

CLoginDlg::~CLoginDlg()
{
}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_BG, m_BackImage);
	DDX_Control(pDX, IDOK, m_btLogin);
	DDX_Control(pDX, IDCANCEL, m_btExit);
	DDX_Control(pDX, IDC_BUTTON_CONINFO, m_btConInfo);
	DDX_Control(pDX, IDC_EDIT_SITE, m_ebSite);
	DDX_Control(pDX, IDC_EDIT_LOGINID, m_ebID);
	DDX_Control(pDX, IDC_EDIT_PASSWD, m_ebPW);
	DDX_Control(pDX, IDC_BN_OPEN_PE_STUDIO, m_btOpenPeStudio);
}

BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CLoginDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CLoginDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_CONINFO, &CLoginDlg::OnBnClickedButtonConinfo)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_REGISTERED_MESSAGE(WM_WAIT_FOR_LOGIN, LoginComplete)
	ON_WM_SETCURSOR()
	ON_BN_CLICKED(IDC_BN_OPEN_PE_STUDIO, &CLoginDlg::OnBnClickedBnOpenPeStudio)
END_MESSAGE_MAP()

// CLoginDlg message handlers
BOOL CLoginDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	//HBITMAP hBitmap = 0;
	if(CEnviroment::eNARSHA == GetEnvPtr()->m_Customer)
	{
		m_bmpLoginBG.LoadBitmap(IDB_LOGIN_BG_NS);
		//hBitmap = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_LOGIN_BG_NS));
	}
	else if(CEnviroment::eLOTTE == GetEnvPtr()->m_Customer)
	{
		
		m_bmpLoginBG.LoadBitmap(IDB_LOGIN_BG_LOTTE);
		//hBitmap = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_LOGIN_BG_LOTTE));
	}
	else if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
	{
		m_bmpLoginBG.LoadBitmap(IDB_LOGIN_BG_ADASSET);
		//hBitmap = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_LOGIN_BG_ADASSET));
	}
	else
	{
		m_bmpLoginBG.LoadBitmap(IDB_LOGIN_BG);
		//hBitmap = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_LOGIN_BG));
	}

	HBITMAP hOldBitmap = m_BackImage.SetBitmap(m_bmpLoginBG);
	DeleteObject(hOldBitmap);
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK_SAVE);

	m_ebSite.SetWindowText(GetEnvPtr()->m_szSite);
	m_ebID  .SetWindowText(GetEnvPtr()->m_szLoginID);
	m_ebPW  .SetWindowText(GetEnvPtr()->m_szPassword);

	if(GetEnvPtr()->m_szLoginID.IsEmpty())
		pCheck->SetCheck(false);
	else
		pCheck->SetCheck(true);

	char szBuf[1000];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

	::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", szBuf, sizeof(szBuf), szPath);
	m_szIP = szBuf;
	::GetPrivateProfileString("ORB_NAMESERVICE", "NAME", "", szBuf, sizeof(szBuf), szPath);
	m_szName = szBuf;
	::GetPrivateProfileString("ORB_NAMESERVICE", "PORT", "", szBuf, sizeof(szBuf), szPath);
	m_nPort = atoi(szBuf);

	m_btLogin.LoadBitmap(IDB_LOGIN_BT_LOGIN, RGB(0xff,0xff,0xff));
	m_btExit.LoadBitmap(IDB_LOGIN_BT_EXIT, RGB(0xff,0xff,0xff));
	m_btConInfo.LoadBitmap(IDB_LOGIN_BT_CONINFO, RGB(0xff,0xff,0xff));
	m_btOpenPeStudio.LoadBitmap(IDB_BTN_WITHOUT_LOGIN, RGB(0xff,0xff,0xff));

	SetTimer(0, 100, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
	KillTimer(0);
}

void CLoginDlg::OnBnClickedButtonConinfo()
{
	COrbInfoDlg dlg;
	dlg.SetOrbInfo(m_szName, m_szIP, m_nPort);
	if(dlg.DoModal() != IDOK) return;

	dlg.GetOrbInfo(m_szName, m_szIP, m_nPort);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

	::WritePrivateProfileString("ORB_NAMESERVICE", "IP", m_szIP, szPath);
	::WritePrivateProfileString("ORB_NAMESERVICE", "NAME", m_szName, szPath);
	::WritePrivateProfileString("ORB_NAMESERVICE", "PORT", ToString(m_nPort), szPath);

	// 2010.11.05 박선견요구 접속정보를 변경시 Agent와 TimeSyncer도 같이 변경하도록 함
	::WritePrivateProfileString("AGENTMUX", "IP", m_szIP, szPath);
	::WritePrivateProfileString("TIMESYNCER", "IP", m_szIP, szPath);

	if(dlg.m_bQuit)
	{
		// config/utv1.properties 파일의 COP.SOCKET_PROXY_SERVER.IP 값 설정
		installUtil::getInstance()->changeServerIP(m_szIP);

		// 0000554: 코바연결이 되어 있는 경우 재접속하도록 유도
		if(IsInitORB())
		{
			UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG007));
			OnCancel();
		}
	}
}

void CLoginDlg::OnBnClickedOk()
{
	//if(!m_bInit){
	//	SOrbConnInfo* pInfo = COrbConn::GetObject()->GetCur();
	//	if(!InitORB(pInfo->ip, pInfo->port)){
	//		CString szBuf;
	//		szBuf.Format(LoadStringById(IDS_MAINFRAME_MSG028), pInfo->ip, pInfo->port);
	//		UbcMessageBox(szBuf);
	//		return;
	//	}else{
	//		m_bInit = true;
	//	}
	//}

	m_ebSite.GetWindowText(m_strSite);
	m_ebID  .GetWindowText(m_strID  );
	m_ebPW  .GetWindowText(m_strPW  );

	// 로그인 화면을 ID,Passwd 만으로 변경한다
	m_strSite = _T("*");
	//if(m_strSite.IsEmpty())
	//{
	//	m_strSite = _T("Default");
	//	//UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG001));
	//	//m_ebSite.SetFocus();
	//	//return;
	//}

	if(m_strID.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG002));
		m_ebID.SetFocus();
		return;
	}

	if(m_strPW.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG003));
		m_ebPW.SetFocus();
		return;
	}

	if(m_szIP.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG004));
		OnBnClickedButtonConinfo();
		return;
	}

//	m_ebSite.EnableWindow(FALSE);
	m_ebID  .EnableWindow(FALSE);
	m_ebPW  .EnableWindow(FALSE);
	m_btConInfo.EnableWindow(FALSE);
	m_btLogin.EnableWindow(FALSE);
	m_btOpenPeStudio.EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_SAVE)->EnableWindow(FALSE);

	m_pLoginThread = AfxBeginThread(CLoginDlg::WaitForLogin, this);

/* 스레드로 처리함
	// 0000554: 코바연결을 시도하는 시점을 로그인 버튼을 누른 시점으로 변경한다.
	if(!InitORB(m_szIP, m_nPort))
	{
		CString szBuf;
		szBuf.Format(LoadStringById(IDS_LOGINDLG_MSG005), m_szIP);
		UbcMessageBox(szBuf);
		return;
	}

	for(int i=0; i<10 ; i++)
	{
		GetEnvPtr()->m_Authority = copCheckLogin(szSite, szID, szPW);
		if(GetEnvPtr()->m_Authority >= 0) break;
		Sleep(500);
	}

	if( CEnviroment::eAuthNoUser == GetEnvPtr()->m_Authority ||
	   (CEnviroment::eAuthSuper  != GetEnvPtr()->m_Authority &&
	    CEnviroment::eAuthSite   != GetEnvPtr()->m_Authority &&
	    CEnviroment::eAuthUser   != GetEnvPtr()->m_Authority ))
	{
		UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG006), MB_ICONERROR);
		return;
	}

	GetEnvPtr()->m_szSite = szSite;
	GetEnvPtr()->m_szLoginID = szID;
	GetEnvPtr()->m_szPassword = szPW;

	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK_SAVE);
	GetEnvPtr()->SaveLoginInfo(pCheck->GetCheck());

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

	::WritePrivateProfileString("ORB_NAMESERVICE", "IP", m_szIP, szPath);
	::WritePrivateProfileString("ORB_NAMESERVICE", "NAME", m_szName, szPath);
	::WritePrivateProfileString("ORB_NAMESERVICE", "PORT", ToString(m_nPort), szPath);

	OnOK();
*/
}

void CLoginDlg::OnBnClickedCancel()
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	if(m_pLoginThread)
	{
		delete m_pLoginThread;
		m_pLoginThread = NULL;
	}

	OnCancel();
}

void CLoginDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CDialog::OnPaint() for painting messages
}

void CLoginDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if(IsWindow(m_btLogin.m_hWnd))
	{
		m_btLogin.Invalidate(false);
		m_btExit.Invalidate(false);
		m_btConInfo.Invalidate(false);
		m_btOpenPeStudio.Invalidate(false);
	}

	CDialog::OnTimer(nIDEvent);
}

UINT CLoginDlg::WaitForLogin(LPVOID pParam)
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	CLoginDlg* pDlg = (CLoginDlg*)pParam;

	// 0001350: 매니저 및 스튜디오 Connect Test 를 버튼으로 뺀다. (접속테스트용 포트를 cop접속 포트로 한다)
	// 0000717: 로그인 하기 전에 Connect Test 를 먼저한다.
	//TimeSyncTimer timeSyncTimer;
	//if(timeSyncTimer.connectTest(5)==ciFalse)
	if(ConnectTest(pDlg->m_szIP, pDlg->m_nPort) == FALSE)
	{
		CString szBuf;
		szBuf.Format(LoadStringById(IDS_LOGINDLG_MSG005), pDlg->m_szIP);
		UbcMessageBox(szBuf);
		pDlg->PostMessage(WM_WAIT_FOR_LOGIN, FALSE, NULL);
		::CoUninitialize();
		return 0;
	}

	// 0000554: 코바연결을 시도하는 시점을 로그인 버튼을 누른 시점으로 변경한다.
	if(!InitORB(pDlg->m_szIP, pDlg->m_nPort))
	{
		CString szBuf;
		szBuf.Format(LoadStringById(IDS_LOGINDLG_MSG005), pDlg->m_szIP);
		UbcMessageBox(szBuf);
		pDlg->PostMessage(WM_WAIT_FOR_LOGIN, FALSE, NULL);
		::CoUninitialize();
		return 0;
	}

	// 로그인계정의 유효기간을 체크한다
	CString strValidationDate;
	// 패스워드 변경 경고를 내보낸다.
	CString strLastPWChangeTime;
	GetEnvPtr()->m_Authority = copCheckLogin(pDlg->m_strSite, pDlg->m_strID, pDlg->m_strPW, 
		strValidationDate, GetEnvPtr()->m_strCustomer);
	//if( strValidationDate.Left(8) != _T("1970/01/01") && strValidationDate < CTime::GetCurrentTime().Format(STR_ENV_TIME) )
	//{
	//	CString strMsg;
	//	strMsg.Format(LoadStringById(IDS_LOGINDLG_MSG008), strValidationDate);
	//	UbcMessageBox(strMsg);
	//	pDlg->PostMessage(WM_WAIT_FOR_LOGIN, FALSE, NULL);
	//	return 0;
	//}
	//CTime now;
	//now = now - CTimeSpan(90, 0, 0, 0);
	//if( strLastPWChangeTime.Left(8) != _T("1970/01/01") && strLastPWChangeTime < now.Format(STR_ENV_TIME) )
	//{
	//	CString strMsg;
	//	strMsg.Format(LoadStringById(IDS_LOGINDLG_MSG009), strLastPWChangeTime);
	//	UbcMessageBox(strMsg);
	//	pDlg->PostMessage(WM_WAIT_FOR_LOGIN, FALSE, NULL);
	//	return 0;
	//}
	pDlg->PostMessage(WM_WAIT_FOR_LOGIN, TRUE, NULL);
	::CoUninitialize();
	//AfxEndThread(0);

	return 0;
}

LRESULT CLoginDlg::LoginComplete(WPARAM wParam, LPARAM lParam)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	m_pLoginThread = NULL;

//	m_ebSite.EnableWindow(TRUE);
	m_ebID  .EnableWindow(TRUE);
	m_ebPW  .EnableWindow(TRUE);
	m_btConInfo.EnableWindow(TRUE);
	m_btLogin.EnableWindow(TRUE);
	m_btOpenPeStudio.EnableWindow(TRUE);
	GetDlgItem(IDC_CHECK_SAVE)->EnableWindow(TRUE);

	if(!wParam) return 0;

	if(GetEnvPtr()->m_Authority < 0) return 0;

	if( CEnviroment::eAuthNoUser == GetEnvPtr()->m_Authority ||
	   (CEnviroment::eAuthSuper  != GetEnvPtr()->m_Authority &&
	    CEnviroment::eAuthSite   != GetEnvPtr()->m_Authority &&
	    CEnviroment::eAuthUser   != GetEnvPtr()->m_Authority ))
	{
		UbcMessageBox(LoadStringById(IDS_LOGINDLG_MSG006), MB_ICONERROR);
		return 0;
	}

	GetEnvPtr()->m_szSite = m_strSite;
	GetEnvPtr()->m_szLoginID = m_strID;
	GetEnvPtr()->m_szPassword = m_strPW;

	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK_SAVE);
	GetEnvPtr()->SaveLoginInfo(pCheck->GetCheck());

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

	::WritePrivateProfileString("ORB_NAMESERVICE", "IP", m_szIP, szPath);
	::WritePrivateProfileString("ORB_NAMESERVICE", "NAME", m_szName, szPath);
	::WritePrivateProfileString("ORB_NAMESERVICE", "PORT", ToString(m_nPort), szPath);

	// 2010.11.05 박선견요구 접속정보를 변경시 Agent와 TimeSyncer도 같이 변경하도록 함
	::WritePrivateProfileString("AGENTMUX", "IP", m_szIP, szPath);
	::WritePrivateProfileString("TIMESYNCER", "IP", m_szIP, szPath);

	OnOK();
	return 0;
}

BOOL CLoginDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(m_pLoginThread)
	{
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_APPSTARTING));
		return TRUE;
	}

	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}

// 0000920: EE Studio 에서, "로그인하지 않고 사용하기" 버튼 만들기
void CLoginDlg::OnBnClickedBnOpenPeStudio()
{
	// PE Studio를 열고
	CString strPath;
	strPath.Format("\"%s%s%s\"", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, UBCSTUDIOEXE_STR);

	CString strParam = " +ignore_env";

	HINSTANCE nRet = ShellExecute(NULL, NULL, strPath, strParam, NULL, SW_SHOWNORMAL);

	// EE Studio를 닫는다
	OnBnClickedCancel();
}
