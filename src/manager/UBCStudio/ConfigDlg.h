#pragma once
#include "afxwin.h"
#include "EditEx.h"
#include "afxcmn.h"


// CConfigDlg 대화 상자입니다.

class CConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CConfigDlg)

public:
	CConfigDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CConfigDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CONFIG_DLG };

	CButton			m_kbAutoTempSave;
	CEditEx			m_ebTempSaveInterval;
	CSpinButtonCtrl m_spTempSaveInterval;

	CButton			m_kbUseTimeBasePlayContents;
	CButton			m_kbUseClickPlayContents;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedKbTempSave();
	CButton m_kbUseTimeCheck;
	CButton m_kbAutoEncoding;
};
