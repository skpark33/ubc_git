// DisplayConditionDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "copmodule.h"

#include "DisplayConditionDlg.h"


// CDisplayConditionDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDisplayConditionDlg, CDialog)

CDisplayConditionDlg::CDisplayConditionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDisplayConditionDlg::IDD, pParent)
{

}

CDisplayConditionDlg::~CDisplayConditionDlg()
{
}

void CDisplayConditionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_IN_CANDIDATE, m_lcInList);
	DDX_Control(pDX, IDC_LIST_IN_SELECTED, m_lcInSelected);
	DDX_Control(pDX, IDC_LIST_EX_CANDIDATE, m_lcExList);
	DDX_Control(pDX, IDC_LIST_EX_SELECTED, m_lcExSelected);
}


BEGIN_MESSAGE_MAP(CDisplayConditionDlg, CDialog)
	ON_BN_CLICKED(IDC_BN_IN_ADD, &CDisplayConditionDlg::OnBnClickedBnInAdd)
	ON_BN_CLICKED(IDC_BN_IN_DEL, &CDisplayConditionDlg::OnBnClickedBnInDel)
	ON_BN_CLICKED(IDC_BN_EX_ADD, &CDisplayConditionDlg::OnBnClickedBnExAdd)
	ON_BN_CLICKED(IDC_BN_EX_DEL, &CDisplayConditionDlg::OnBnClickedBnExDel)
	ON_BN_CLICKED(IDOK, &CDisplayConditionDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CDisplayConditionDlg::OnBnClickedCancel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_IN_CANDIDATE, &CDisplayConditionDlg::OnNMDblclkListInCandidate)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_IN_SELECTED, &CDisplayConditionDlg::OnNMDblclkListInSelected)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_EX_CANDIDATE, &CDisplayConditionDlg::OnNMDblclkListExCandidate)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_EX_SELECTED, &CDisplayConditionDlg::OnNMDblclkListExSelected)
END_MESSAGE_MAP()


// CDisplayConditionDlg 메시지 처리기입니다.

BOOL CDisplayConditionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	InitList();
	InitSelected();

	RefreshList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDisplayConditionDlg::InitList()
{
	m_szColum[eCheck ] = _T("");
	m_szColum[eCategory] = LoadStringById(IDS_EEPACKAGEDLG_LST016);

	// include
	m_lcInList.SetExtendedStyle(m_lcInList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lcInList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcInList.InsertColumn(eCategory , m_szColum[eCategory ], LVCFMT_LEFT  , 150);

	m_lcInList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	//exclude
	m_lcExList.SetExtendedStyle(m_lcExList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lcExList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcExList.InsertColumn(eCategory , m_szColum[eCategory ], LVCFMT_LEFT  , 150);

	m_lcExList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

}

void CDisplayConditionDlg::InitSelected()
{
	//include
	m_lcInSelected.SetExtendedStyle(m_lcInSelected.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcInSelected.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcInSelected.InsertColumn(eCategory , m_szColum[eCategory ], LVCFMT_LEFT  , 150);

	m_lcInSelected.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	//exclude
	m_lcExSelected.SetExtendedStyle(m_lcExSelected.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcExSelected.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcExSelected.InsertColumn(eCategory , m_szColum[eCategory ], LVCFMT_LEFT  , 150);

	m_lcExSelected.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CDisplayConditionDlg::RefreshList()
{
	CWaitMessageBox wait;

	CStringList 	aTagList;
	if(!GetAllCategory(aTagList)) {
		UbcMessageBox(LoadStringById(IDS_CONDITIONDLG_MSG001));
		return;
	}

	m_lcInList.DeleteAllItems();
	m_lcExList.DeleteAllItems();
	int nRow = 0;

	POSITION pos = aTagList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		CString info = aTagList.GetNext(pos);

		m_lcInList.InsertItem(nRow, "");
		m_lcInList.SetItemText(nRow, eCategory, info);
		m_lcInList.SetItemData(nRow, (DWORD_PTR)posOld);

		m_lcExList.InsertItem(nRow, "");
		m_lcExList.SetItemText(nRow, eCategory, info);
		m_lcExList.SetItemData(nRow, (DWORD_PTR)posOld);
		nRow++;
	}

	for(int i =0;i <m_lsInclude.GetCount();i++){
		CString strCategory = m_lsInclude[i];
		int nRow = m_lcInSelected.GetItemCount();
		m_lcInSelected.InsertItem(nRow, "");
		m_lcInSelected.SetItemText(nRow, eCategory, strCategory);
	}
	for(int i =0;i <m_lsExclude.GetCount();i++){
		CString strCategory = m_lsExclude[i];
		int nRow = m_lcExSelected.GetItemCount();
		m_lcExSelected.InsertItem(nRow, "");
		m_lcExSelected.SetItemText(nRow, eCategory, strCategory);
	}

}

void CDisplayConditionDlg::AddSel(CUTBListCtrlEx& from , CUTBListCtrlEx& to)
{
	bool bChecked = false;
	for(int i=0; i<from.GetItemCount() ;i++)
	{
		if(from.GetCheck(i))
		{
			bChecked = true;
			break;
		}
	}
	if(!bChecked)
	{
		UbcMessageBox(LoadStringById(IDS_CONDITIONDLG_MSG002));
		return;
	}
	for(int i=0; i<from.GetItemCount() ;i++)
	{
		if(!from.GetCheck(i)) continue;
		_AddSel(i, from, to);
		from.SetCheck(i, FALSE);
	}
	from.SetCheckHdrState(FALSE);
}

void CDisplayConditionDlg::_AddSel(int nHostIndex, CUTBListCtrlEx& from , CUTBListCtrlEx& to)
{
	if(nHostIndex < 0 || nHostIndex >= from.GetItemCount()) return;

	CString strCategory = from.GetItemText(nHostIndex, eCategory);

	// 이미 추가된 단말인지 체크
	for(int i=0; i<to.GetItemCount() ;i++)
	{
		CString strSelCategory = to.GetItemText(i, eCategory);
		if(strSelCategory == strCategory) return;
	}

	int nRow = to.GetItemCount();
	to.InsertItem(nRow, "");
	to.SetItemText(nRow, eCategory, strCategory);
}

void CDisplayConditionDlg::DelSel(CUTBListCtrlEx& to)
{
	for(int i=to.GetItemCount()-1; i>=0 ;i--)
	{
		if(!to.GetCheck(i)) continue;
		_DelSel(i, to);
	}

	to.SetCheckHdrState(FALSE);
}

void CDisplayConditionDlg::_DelSel(int nSelHostIndex, CUTBListCtrlEx& to)
{
	if(nSelHostIndex < 0 || nSelHostIndex >= to.GetItemCount()) return;

	to.DeleteItem(nSelHostIndex);
}

void CDisplayConditionDlg::OnBnClickedBnInAdd()
{
	AddSel(m_lcInList, m_lcInSelected);
}
void CDisplayConditionDlg::OnBnClickedBnInDel()
{
	DelSel(m_lcInSelected);
}

void CDisplayConditionDlg::OnBnClickedBnExAdd()
{
	AddSel(m_lcExList, m_lcExSelected);
}

void CDisplayConditionDlg::OnBnClickedBnExDel()
{
	DelSel(m_lcExSelected);
}

void CDisplayConditionDlg::OnNMDblclkListInCandidate(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
	_AddSel(pNMLV->iItem, m_lcInList, m_lcInSelected);
}

void CDisplayConditionDlg::OnNMDblclkListInSelected(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
	_DelSel(pNMLV->iItem, m_lcInSelected);
}

void CDisplayConditionDlg::OnNMDblclkListExCandidate(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
	_AddSel(pNMLV->iItem, m_lcExList, m_lcExSelected);
}

void CDisplayConditionDlg::OnNMDblclkListExSelected(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
	_DelSel(pNMLV->iItem, m_lcExSelected);
}

void CDisplayConditionDlg::OnBnClickedOk()
{
	m_lsInclude.RemoveAll();
	m_lsExclude.RemoveAll();

	for(int i=0; i<m_lcInSelected.GetItemCount() ;i++)
	{
		m_lsInclude.Add(m_lcInSelected.GetItemText(i, eCategory));
	}
	TraceLog(("%d row selected", m_lsInclude.GetSize()));

	for(int i=0; i<m_lcExSelected.GetItemCount() ;i++)
	{
		m_lsExclude.Add(m_lcExSelected.GetItemText(i, eCategory));
	}
	TraceLog(("%d row selected", m_lsExclude.GetSize()));

	OnOK();
}

void CDisplayConditionDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CDisplayConditionDlg::toString(CString& strIncludeCategory,	CString& strExcludeCategory)
{
	strIncludeCategory="";
	strExcludeCategory="";

	for(int i =0;i <m_lsInclude.GetCount();i++){
		if(!strIncludeCategory.IsEmpty()){
			strIncludeCategory += ",";
		}
		strIncludeCategory += m_lsInclude[i];
	}
	for(int i =0;i <m_lsExclude.GetCount();i++){
		if(!strExcludeCategory.IsEmpty()){
			strExcludeCategory += ",";
		}
		strExcludeCategory += m_lsExclude[i];
	}
}

void CDisplayConditionDlg::fromString(CString& strIncludeCategory,	CString& strExcludeCategory)
{
	{
		int pos = 0;
		CString token = strIncludeCategory.Tokenize(",", pos);
		while(token != "")
		{
			m_lsInclude.Add(token);
			token = strIncludeCategory.Tokenize(",", pos);
		}
	}
	{
		int pos = 0;
		CString token = strExcludeCategory.Tokenize(",", pos);
		while(token != "")
		{
			m_lsExclude.Add(token);
			token = strExcludeCategory.Tokenize(",", pos);
		}
	}
}