#pragma once
#include "afxcmn.h"


// CFileTranInfo dialog

class CFileTranInfo : public CDialog
{
	DECLARE_DYNAMIC(CFileTranInfo)

public:
	CFileTranInfo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFileTranInfo();

// Dialog Data
	enum { IDD = IDD_FILETRAN_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString       m_szInfo;
	CRichEditCtrl m_ctrInfo;
	virtual BOOL OnInitDialog();
};
