// GoToTemplateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "GoToTemplateDlg.h"


// CGoToTemplateDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CGoToTemplateDlg, CDialog)

CGoToTemplateDlg::CGoToTemplateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGoToTemplateDlg::IDD, pParent)
{

}

CGoToTemplateDlg::~CGoToTemplateDlg()
{
}

void CGoToTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CB_TEMPLATE, m_cbTemplate);
	DDX_Control(pDX, IDC_EB_DURATION, m_ebDuration);
	DDX_Control(pDX, IDC_SP_DURATION, m_spDuration);
}


BEGIN_MESSAGE_MAP(CGoToTemplateDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CGoToTemplateDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CGoToTemplateDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CGoToTemplateDlg 메시지 처리기입니다.

void CGoToTemplateDlg::SetParam(CString strTarTemplateId, int nDuration, CString strSelfTemplateId)
{
	m_strTarTemplateId = strTarTemplateId;
	m_nDuration = nDuration;
	m_strSelfTemplateId = strSelfTemplateId;
}

void CGoToTemplateDlg::GetParam(CString& strTarTemplateId, int& nDuration)
{
	strTarTemplateId = m_strTarTemplateId;
	nDuration = m_nDuration;
}

void CGoToTemplateDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	m_cbTemplate.GetWindowText(m_strTarTemplateId);
	if(m_strTarTemplateId.IsEmpty() ||
	   m_strTarTemplateId == LoadStringById(IDS_GOTOTEMPLATE_STR001))
	{
		UbcMessageBox(LoadStringById(IDS_GOTOTEMPLATE_MSG002));
		m_cbTemplate.SetFocus();
		return;
	}

	m_nDuration = m_ebDuration.GetValueInt();

	OnOK();
}

void CGoToTemplateDlg::OnBnClickedCancel()
{
	OnCancel();
}

BOOL CGoToTemplateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	int nSelIndex = 0;
	m_cbTemplate.ResetContent();
	m_cbTemplate.AddString(LoadStringById(IDS_GOTOTEMPLATE_STR001));
	if(GetDocument() && GetDocument()->GetPlayTemplateList())
	{
		TEMPLATE_LINK_LIST* pPlayTempList = GetDocument()->GetPlayTemplateList();

		for(int i = 0; i < pPlayTempList->GetCount(); i++)
		{
			if(m_strSelfTemplateId == pPlayTempList->GetAt(i).pTemplateInfo->strId)
			{
				continue;
			}

			int nIndex = m_cbTemplate.AddString(pPlayTempList->GetAt(i).pTemplateInfo->strId);

			if(m_strTarTemplateId == pPlayTempList->GetAt(i).pTemplateInfo->strId)
			{
				nSelIndex = nIndex;
			}
		}
	}

	m_cbTemplate.SetCurSel(nSelIndex);
//	m_cbTemplate.SetWindowText(m_strTarTemplateId);

	if(m_nDuration < 0) m_nDuration = 30;
	m_ebDuration.SetValue(m_nDuration);
	m_spDuration.SetRange(0, 60 * 60); // Max 1시간

	UpdateData(FALSE);

	if(m_cbTemplate.GetCount() <= 1)
	{
		UbcMessageBox(LoadStringById(IDS_GOTOTEMPLATE_MSG001));
		CDialog::OnCancel();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
