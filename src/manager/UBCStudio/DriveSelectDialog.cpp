// DriveSelectDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCStudio.h"
#include "DriveSelectDialog.h"
#include "Enviroment.h"
#include "common\libscratch\scratchUtil.h"
#include "common/PreventChar.h"
#include "common\UbcCode.h"
#include "RemovableDiskWarning.h"


#ifdef _UBCSTUDIO_EE_
#include "ubccopcommon\ubccopcommon.h"
#else
#define GetSiteName(x) x
#define GetSelectSite(x, y, z) FALSE
#endif//_UBCSTUDIO_EE_

#define		IDS_NO_HOST_NAME		"New Package"
#define		TIMER_EDIT_SEL			6789			///<Host name select timer

// CDriveSelectDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDriveSelectDialog, CDialog)

CDriveSelectDialog::CDriveSelectDialog(CWnd* pParent /*=NULL*/)
:	CDialog(CDriveSelectDialog::IDD, pParent)
,	m_strHostName("")
,	m_strDrive("")
,	m_strDesc("")
,	m_strSite("")
,	m_bMonitorOn(false)
,	m_bMonitorOff(false)
{
}

CDriveSelectDialog::~CDriveSelectDialog()
{
}

void CDriveSelectDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_DRIVES, m_lcDrives);
	DDX_Control(pDX, IDC_EDIT_HOST_NAME, m_editHostName);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_EDIT_DESC, m_editDescription);
	DDX_Control(pDX, IDC_LIST_CATEGORY, m_lcCategory[eCategory]);
	DDX_Control(pDX, IDC_LIST_PURPOSE, m_lcCategory[ePurpose]);
	DDX_Control(pDX, IDC_LIST_HOSTTYPE, m_lcCategory[eHostType]);
	DDX_Control(pDX, IDC_LIST_VERTICAL, m_lcCategory[eVertical]);
	DDX_Control(pDX, IDC_LIST_RESOLUTION, m_lcCategory[eResolution]);
	DDX_Control(pDX, IDC_CHECK_PUBLIC, m_kbPublic);
	DDX_Control(pDX, IDC_DT_VALIDATION_DATE, m_dtValidationDate);
	DDX_Control(pDX, IDC_CHECK_MONITORON, m_ckMonitorOn);
	DDX_Control(pDX, IDC_CHECK_MONITOROFF, m_ckMonitorOff);
}

BEGIN_MESSAGE_MAP(CDriveSelectDialog, CDialog)
	ON_WM_TIMER()
	ON_EN_SETFOCUS(IDC_EDIT_HOST_NAME, &CDriveSelectDialog::OnEnSetfocusEditHostName)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_DRIVES, &CDriveSelectDialog::OnNMDblclkListDrives)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_DRIVES, &CDriveSelectDialog::OnLvnItemchangedListDrives)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_DRIVES, &CDriveSelectDialog::OnNMCustomdrawListDrives)
	ON_BN_CLICKED(IDOK, &CDriveSelectDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BN_SITE, &CDriveSelectDialog::OnBnClickedBnSite)
	ON_BN_CLICKED(IDC_CHECK_MONITORON, &CDriveSelectDialog::OnBnClickedCheckMonitoron)
	ON_BN_CLICKED(IDC_CHECK_MONITOROFF, &CDriveSelectDialog::OnBnClickedCheckMonitoroff)
END_MESSAGE_MAP()


// CDriveSelectDialog 메시지 처리기입니다.
void CDriveSelectDialog::InitCategoryListCtrl()
{
	for(int i=0; i<eMaxCnt ;i++)
	{
		CString strColName;
		switch(i)
		{
		case eCategory  : strColName = LoadStringById(IDS_DRIVESELECTDIALOG_STR003); break;
		case ePurpose   : strColName = LoadStringById(IDS_DRIVESELECTDIALOG_STR004); break;
		case eHostType  : strColName = LoadStringById(IDS_DRIVESELECTDIALOG_STR005); break;
		case eVertical  : strColName = LoadStringById(IDS_DRIVESELECTDIALOG_STR006); break;
		case eResolution: strColName = LoadStringById(IDS_DRIVESELECTDIALOG_STR007); break;
		}

		m_lcCategory[i].SetExtendedStyle(m_lcCategory[i].GetExtendedStyle() | LVS_EX_FULLROWSELECT);
		m_lcCategory[i].SetSelTextColor(RGB(0,0,0));
		m_lcCategory[i].SetSelBackColor(RGB(220,220,220));
		m_lcCategory[i].InsertColumn(0, strColName, LVCFMT_LEFT, 100);
		m_lcCategory[i].InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_RADIOBOX);
		m_lcCategory[i].HideScrollBars(SB_HORZ);
	}
}

BOOL CDriveSelectDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_dtValidationDate.SetFormat(_T("yyyy/MM/dd HH:mm:ss"));

	InitCategoryListCtrl();

	CUbcCode::GetInstance()->FillListCtrl(m_lcCategory[eCategory], _T("Kind"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcCategory[ePurpose] , _T("Purpose"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcCategory[eHostType], _T("HostType"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcCategory[eVertical], _T("Direction"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcCategory[eResolution], _T("Resolution"));

	for(int i=0; i<eMaxCnt ;i++)
	{
		CRect rect;
		m_lcCategory[i].GetClientRect(rect);
		CScrollBar* pBar = m_lcCategory[i].GetScrollBarCtrl(SB_VERT);
		int width = (pBar && pBar->IsWindowVisible()) ? rect.Width()-::GetSystemMetrics(SM_CXVSCROLL) : rect.Width();
		m_lcCategory[i].SetColumnWidth(0, width);
		//m_lcFilter[i].SetColumnWidth(0, LVSCW_AUTOSIZE);
	}

	CRect rc;
	m_bTemplate = false;

	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		m_strSite = GetEnvPtr()->m_PackageInfo.szSiteID;
		CString strSiteName = GetSiteName(m_strSite);
		if(strSiteName.IsEmpty()) strSiteName = m_strSite;

		SetDlgItemText(IDC_EDIT_SITE_NAME, m_strSite);

		CButton* prdServer = (CButton*)GetDlgItem(IDC_RADIO_SVA_SERVER);
		CButton* prdLocal = (CButton*)GetDlgItem(IDC_RADIO_SVA_LOCAL);
		prdServer->SetCheck(TRUE);
		prdLocal->SetCheck(FALSE);
		m_bServer = true;
	}
	else
	{
		GetDlgItem(IDC_STATIC_SITE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_SITE_NAME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BN_SITE)->ShowWindow(SW_HIDE);

		m_lcDrives.GetClientRect(&rc);
		m_lcDrives.SetWindowPos(NULL, 0, 0, rc.Width()+5, rc.Height()+60, SWP_NOZORDER|SWP_NOMOVE);

		GetDlgItem(IDC_RADIO_SVA_SERVER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO_SVA_LOCAL)->ShowWindow(SW_HIDE);
		m_bServer = false;
	}

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	//m_btnSearchAllDrive.LoadBitmap(IDB_BTN_SEARCH_ALL_DRIVE, RGB(255, 255, 255));;
	m_btnOK.LoadBitmap(IDB_BTN_SAVE_AS, RGB(255, 255, 255));

	m_btnCancel.SetToolTipText(LoadStringById(IDS_DRIVESELECTDIALOG_BUT001));
	m_btnOK.SetToolTipText(LoadStringById(IDS_DRIVESELECTDIALOG_BUT002));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	SetWindowText(LoadStringById(IDS_DRIVESELECTDIALOG_STR001));
	m_btnOK.SetWindowText(LoadStringById(IDS_DRIVESELECTDIALOG_BUT002));

	CBitmap bmp;
	bmp.LoadBitmap(IDB_DRIVE_LIST);

	if(m_ilDrives.Create(48, 48, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilDrives.Add(&bmp, RGB(255,255,255)); //바탕의 회색이 마스크
	}

	m_lcDrives.SetExtendedStyle(m_lcDrives.GetExtendedStyle() | LVS_EX_FULLROWSELECT);
	m_lcDrives.SetImageList(&m_ilDrives, LVSIL_NORMAL);
	m_lcDrives.SetImageList(&m_ilDrives, LVSIL_SMALL);

	m_lcDrives.InsertColumn(0, LoadStringById(IDS_DRIVESELECTDIALOG_LST001), 0, 150);
	m_lcDrives.InsertColumn(1, LoadStringById(IDS_DRIVESELECTDIALOG_LST002), 0, 150);
	m_lcDrives.InsertColumn(2, LoadStringById(IDS_DRIVESELECTDIALOG_LST003), 0, 150);

	InitDriveLabel();
	InitDriveListCtrl();

	if(m_strHostName != ""){
		if(m_strHostName.Find(SAMPLE_FILE_KEY, 0) != -1){
			m_editHostName.SetWindowText(IDS_NO_HOST_NAME);
		}else{
			
			if(strlen(GetEnvPtr()->m_PackageInfo.szSiteID) > 0){
				CString szTemp = GetEnvPtr()->m_PackageInfo.szSiteID;
				szTemp += "_";
				m_strHostName.Replace(szTemp, "");
			}
			m_editHostName.SetWindowText(m_strHostName);
		}
	}else{
		m_editHostName.SetWindowText(IDS_NO_HOST_NAME);
	}

	if(m_strDesc != ""){
		m_editDescription.SetWindowText(m_strDesc);
	}

	COleDateTime tmTemp;
	tmTemp.ParseDateTime(m_strValidationDate);
	if(tmTemp.GetStatus() != COleDateTime::valid)
	{
		tmTemp.ParseDateTime(_T("2037/12/31 23:59:59"));
	}
	m_dtValidationDate.SetTime(tmTemp);

	for(int i=0; i<eMaxCnt ;i++)
	{
		if(m_nCategorys[i] >= 0)
		{
			int nIndex = CUbcCode::GetInstance()->FindListCtrl(m_lcCategory[i], m_nCategorys[i]);
			m_lcCategory[i].SetItemState(nIndex, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);
			m_lcCategory[i].EnsureVisible(nIndex, FALSE);
		}
	}

	m_kbPublic.SetCheck(FALSE);

	m_ckMonitorOn.SetCheck(m_bMonitorOn);
	m_ckMonitorOff.SetCheck(m_bMonitorOff);

	//
	// site_user인 경우 다른site의 패키지로 변경불가능 => 오직 현재유저site로만 저장가능
	if( GetEnvPtr()->m_Authority == CEnviroment::eAuthUser )
	{
		// 패키지site를 유저site로 강제변경
		m_strSite = GetEnvPtr()->m_szSite;//>m_PackageInfo.szSiteID;
		CString strSiteName = GetSiteName(m_strSite);
		if(strSiteName.IsEmpty()) strSiteName = m_strSite;

		SetDlgItemText(IDC_EDIT_SITE_NAME, m_strSite);

		// site변경버튼 사용불가능&안보이게
		GetDlgItem(IDC_BN_SITE)->EnableWindow(FALSE);
		GetDlgItem(IDC_BN_SITE)->ShowWindow(SW_HIDE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDriveSelectDialog::OnOK()
{
	m_editHostName.GetWindowText(m_strHostName);
	m_strHostName.Trim();

	if(m_strHostName.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_DRIVESELECTDIALOG_MSG006), MB_ICONWARNING);
		m_editHostName.SetFocus();
		m_editHostName.SetSel(0, -1);
		return;
	}

	if(m_strHostName.Find(" ") > 0)
	{
		UbcMessageBox(LoadStringById(IDS_DRIVESELECTDIALOG_MSG001), MB_ICONWARNING);
		m_editHostName.SetFocus();
		m_editHostName.SetSel(0, -1);
		return;
	}

	// 콘텐츠 패키지명 한글로 저장할 수 있도록 수정
	//if(scratchUtil::getInstance()->hasDoubleByte(m_strHostName))
	//{
	//	UbcMessageBox(LoadStringById(IDS_DRIVESELECTDIALOG_MSG008), MB_ICONWARNING);
	//	m_editHostName.SetFocus();
	//	m_editHostName.SetSel(0, -1);
	//	return;
	//}

	m_bTemplate = ((CButton*)GetDlgItem(IDC_CHECK_TEMPLATE))->GetCheck();

	m_bServer = false;
	if(GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		CButton* prdServer = (CButton*)GetDlgItem(IDC_RADIO_SVA_SERVER);
		m_bServer = prdServer->GetCheck();
	}

	if(CPreventChar::GetInstance()->HavePreventChar(m_strHostName, CPreventChar::ePackageName))
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_DRIVESELECTDIALOG_MSG009), CPreventChar::GetInstance()->GetPreventChar(CPreventChar::ePackageName));
		UbcMessageBox(strMsg, MB_ICONERROR);
		return;
	}//if

	if(m_bTemplate)
	{
		CString szTemp = SAMPLE_FILE_KEY;
		m_strHostName = szTemp += m_strHostName;
	}
	else if(m_bServer && GetEnvPtr()->m_Edition == CEnviroment::eStudioEE)
	{
		int iPos = 0;
		CString strSite;

		//if(m_strHostName.Find("_") >= 0)
		//{
		//	szSite = m_strHostName.Tokenize("_", iPos);
		//	if(!szSite.IsEmpty())
		//	{
		//		POSITION pos = GetEnvPtr()->m_lsSite.Find(szSite);
		//		if(pos)
		//		{
		//			m_strHostName = m_strHostName.Mid(iPos);
		//			if(m_strHostName.IsEmpty())
		//			{
		//				UbcMessageBox(LoadStringById(IDS_DRIVESELECTDIALOG_MSG002), MB_ICONWARNING);
		//				return;
		//			}
		//		}
		//	}
		//}

		GetDlgItemText(IDC_EDIT_SITE_NAME, strSite);
		if(strSite.IsEmpty())
		{
			UbcMessageBox(LoadStringById(IDS_DRIVESELECTDIALOG_MSG003), MB_ICONWARNING);
			return;
		}

		//m_strSite = strSite;
		m_strHostName = m_strSite + "_" + m_strHostName;
	}

	m_editDescription.GetWindowText(m_strDesc);
	m_strDesc.Trim();

	POSITION pos = m_lcDrives.GetFirstSelectedItemPosition();
	if(pos != NULL)
	{
		int nItem = m_lcDrives.GetNextSelectedItem(pos);
		m_strDrive = m_lcDrives.GetItemText(nItem, 1);
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_DRIVESELECTDIALOG_MSG004), MB_ICONWARNING);
		return;
	}

	if(m_bTemplate)
	{
		m_strDrive = GetEnvPtr()->m_szDrive;
		m_strDrive += "\\";
	}

	CString strPath = m_strDrive;
	strPath.Append(UBC_CONFIG_PATH);

	// skpark USB_Device 2012.10.16  Removable Disk 에 저장할때는 해당 Drive 에 저장할 수 있는 패키지가 오직 하나만
	// 가능하다는 경고를 띠워준다.
	// 현재 어떤 패키지가 해당 드라이브에 있는지 보여주고, 모두 삭제할 지 여부를 물어본다.
	// OK 를 누르면, 삭제되고 cancel 하면, 저장이 취소되어야 한다. ignore 를 누르면 그냥 무시하고 저장한다.
	CString aStrDrive = m_strDrive.Left(2);
	if(GetDriveType(aStrDrive)==DRIVE_REMOVABLE) {
		// 이동식 매체!!!
		// 먼저 해당 매체에 다른 패키지가 이미 있는지 조사해서 있으면, 지울것인가를 묻는 다이얼로그를 띠운다.
		CStringArray iniList;
		if(this->GetIniNames(m_strHostName,strPath,iniList) > 0){
			RemovableDiskWarning dlg;
			dlg.iniList.Copy(iniList);
			dlg.drive = aStrDrive;
			dlg.myName = m_strHostName;
			if(dlg.DoModal() != IDOK) {
				UbcMessageBox("Save Canceled", MB_ICONWARNING);
				return;
			}
		}
	}




	//해당 드라이브에 같은 호스트 이름의 package이 있는지 검사
	CString strOutFileName;
	bool localExist = IsExistFile(strPath, m_strHostName, strOutFileName);
	if(localExist)
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_DRIVESELECTDIALOG_MSG005), m_strHostName);
		if(m_strHostName != strOutFileName)
		{
			strMsg.AppendFormat(_T("\n\n(%s)"), LoadStringById(IDS_DRIVESELECTDIALOG_MSG010));
		}

		if(MessageBox(strMsg, LoadStringById(IDS_DRIVESELECTDIALOG_STR002), MB_OKCANCEL|MB_ICONQUESTION) == IDCANCEL)
		{
			return;
		}

		m_strHostName = strOutFileName;
	}

	if(m_strHostName == "" || m_strHostName == IDS_NO_HOST_NAME)
	{
		UbcMessageBox(LoadStringById(IDS_DRIVESELECTDIALOG_MSG006), MB_ICONWARNING);
		return;
	}
#ifdef _UBCSTUDIO_EE_
	if(m_bServer && !localExist) {  // skpark 20151013 서버에서 이름 중복체크를 해야 한다.
		BOOL bExist = FALSE;
		IsExistPackage(m_strHostName, bExist);
		if(bExist)
		{
			CString strMsg;
			strMsg.Format(LoadStringById(IDS_DRIVESELECTDIALOG_MSG005), m_strHostName);
			if(MessageBox(strMsg, LoadStringById(IDS_DRIVESELECTDIALOG_STR002), MB_OKCANCEL|MB_ICONQUESTION) == IDCANCEL)
			{
				return;
			}
		}
	}
#endif
	COleDateTime tmValidationDate;
	m_dtValidationDate.GetTime(tmValidationDate);
	m_strValidationDate = tmValidationDate.Format(_T("%Y/%m/%d %H:%M:%S"));

	// 0001491: 유효기간이 지난 패키지도 열 수있도록 한다.
	//if(m_strValidationDate < CTime::GetCurrentTime().Format(_T("%Y/%m/%d %H:%M:%S")))
	//{
	//	UbcMessageBox(LoadStringById(IDS_DRIVESELECTDIALOG_MSG016), MB_ICONWARNING);
	//	return;
	//}

	m_bMonitorOn = m_ckMonitorOn.GetCheck();
	m_bMonitorOff = m_ckMonitorOff.GetCheck();

	CDialog::OnOK();
}

void CDriveSelectDialog::OnNMDblclkListDrives(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	OnOK();
}

void CDriveSelectDialog::OnEnSetfocusEditHostName()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDriveSelectDialog::OnLvnItemchangedListDrives(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int index = pNMLV->iItem;
	if(index < 0)
		return;

	CString strTmp;
	m_editHostName.GetWindowText(strTmp);
	if(strTmp == "")
	{
		m_editHostName.SetWindowText(IDS_NO_HOST_NAME);
	}//if
//	SetTimer(TIMER_EDIT_SEL, 200, NULL);
}

void CDriveSelectDialog::OnNMCustomdrawListDrives(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
	*pResult = 0;
/*
	if( CDDS_PREPAINT == pLVCD->nmcd.dwDrawStage ){
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if( CDDS_ITEMPREPAINT == pLVCD->nmcd.dwDrawStage ){
		COLORREF crText;
        COLORREF crTextBk;

		int nRow = m_lcDrives.GetSelectionMark();
		if(nRow >= 0 && nRow == pLVCD->nmcd.dwItemSpec){
			crText = RGB(255,255,255);
			crTextBk = RGB(49,106,197);
		}else{
			crText = RGB(0,0,0);
			crTextBk = RGB(255,255,255);
		}

		pLVCD->clrText = crText;
        pLVCD->clrTextBk = crTextBk;
		*pResult = CDRF_DODEFAULT;
	}*/
}

void  CDriveSelectDialog::GetProcessVolume()
{
	char buf[MAX_PATH];           // buffer for unique volume identifiers
	HANDLE hVol;                  // handle for the volume scan
	BOOL bFlag;                   // generic results flag

	// Open a scan for volumes.
	hVol = FindFirstVolume (buf, MAX_PATH );

	if (hVol == INVALID_HANDLE_VALUE)
		return;

	// We have a volume; process it.
	bFlag = _GetProcessVolume (hVol, buf, MAX_PATH);

	// Do while we have volumes to process.
	while (bFlag) 
	{
		bFlag = _GetProcessVolume (hVol, buf, MAX_PATH);
	}

	// Close out the volume scan.
	bFlag = FindVolumeClose( hVol );
}

BOOL CDriveSelectDialog::_GetProcessVolume(HANDLE hVol, char* lpBuf, int iBufSize)
{
	DWORD dwSysFlags;            // flags that describe the file system
	char FileSysNameBuf[MAX_PATH];
	char labelname[MAX_PATH];

	BOOL ret_val = GetVolumeInformation( lpBuf, labelname, MAX_PATH, NULL, NULL, &dwSysFlags, FileSysNameBuf, MAX_PATH);

	if(ret_val)
	{
		m_mapVolumeToLabel.SetAt(lpBuf, labelname);
	}

	return FindNextVolume( hVol, lpBuf, iBufSize );
}

void CDriveSelectDialog::InitDriveLabel()
{
	m_mapVolumeToLabel.RemoveAll();
	m_mapDriveToLabel.RemoveAll();

	//
	GetProcessVolume();

	//
	CStringArray	drive_list;
	int nPos =0;
	char drive[] = "?:\\";
	DWORD dwDriveList=::GetLogicalDrives();
	while(dwDriveList)
	{
		if(dwDriveList & 1)
		{
			drive[0] = 'A' + nPos;
			drive_list.Add(drive);
		}
		dwDriveList >>=1;
		nPos++;
	}

	//
	char volume[255];
	for(int i=0; i<drive_list.GetCount(); i++)
	{
		CString drive = drive_list.GetAt(i);

		BOOL ret = GetVolumeNameForVolumeMountPoint(drive, volume, 255);

		if(ret)
		{
			CString label;
			if(m_mapVolumeToLabel.Lookup(volume, label))
				m_mapDriveToLabel.SetAt(drive, label);
		}
	}
}

bool CDriveSelectDialog::IsExistFile(CString strPath, CString strFileName, CString& strOutFileName)
{
	bool bResult = false;
	strOutFileName.Empty();

	CString strFindPath = strPath + strFileName + _T(".ini");

	CFileFind finder;
	BOOL bWorking = finder.FindFile(strFindPath);
	while(bWorking)
	{
		bWorking = finder.FindNextFile();

		if(finder.IsDots()) continue;

		if(finder.IsDirectory()) continue;

		bResult = true;
		strOutFileName = finder.GetFileTitle();

		break;
	}

	finder.Close();
	return bResult;
}

void CDriveSelectDialog::InitDriveListCtrl()
{
	m_lcDrives.DeleteAllItems();

	int count = 0;

	POSITION pos = m_mapDriveToLabel.GetStartPosition();
	if(pos == NULL)
	{
		return;
	}//if

	CString str;
	while(pos != NULL)
	{
		CString drive;
		CString label;

		m_mapDriveToLabel.GetNextAssoc( pos, drive, label );
		UINT type = GetDriveType(drive);
		if(label == "")
		{
			switch(type)
			{
			case DRIVE_REMOVABLE:
				label = "Removable Disk";
				break;
			case DRIVE_FIXED:
				label = "Local Disk";
				break;
			case DRIVE_REMOTE:
				label = "Remote Disk";
				break;
			case DRIVE_CDROM:
				label = "Optical Disk";
				break;
			case DRIVE_RAMDISK:
				label = "RAM Disk";
				break;
			default:
				label = "Unknown Disk";
				break;
			}//switch
		}//if

		CString path = drive;
		path.Append(UBC_CONFIG_PATH);

			int index = -1;
			for(int i=0; i<m_lcDrives.GetItemCount(); i++, index++)
			{
				CString drv = m_lcDrives.GetItemText(i, 1);
				if(drive < drv)
				{
					break;
				}//if
			}//for
			index++;
			count++;

			str.Format(" %s (%s)", label, drive.Left(2));
			m_lcDrives.InsertItem(index, str, type);
			m_lcDrives.SetItemText(index, 1, drive);
			m_lcDrives.SetItemText(index, 2, IDS_NO_HOST_NAME);

			if(drive.Compare("C:\\")==0){
				m_lcDrives.SetSelectionMark(index);
				m_lcDrives.SetItemState(index, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);
				m_lcDrives.SetFocus();
			}

		continue;
	}//while


	if(count == 0)
	{
		UbcMessageBox(LoadStringById(IDS_DRIVESELECTDIALOG_MSG007), MB_ICONWARNING);
		//CDialog::OnCancel();
	}//if
}


void CDriveSelectDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == TIMER_EDIT_SEL)
	{
		KillTimer(TIMER_EDIT_SEL);

		m_editHostName.SetFocus();
		m_editHostName.SetSel(0, -1);
	}//if

	CDialog::OnTimer(nIDEvent);
}

void CDriveSelectDialog::OnBnClickedOk()
{
	for(int i=0; i<eMaxCnt ;i++)
	{
		CString strErrMsg;
		switch(i)
		{
		case eCategory  : strErrMsg = LoadStringById(IDS_DRIVESELECTDIALOG_MSG011); break;
		case ePurpose   : strErrMsg = LoadStringById(IDS_DRIVESELECTDIALOG_MSG012); break;
		case eHostType  : strErrMsg = LoadStringById(IDS_DRIVESELECTDIALOG_MSG013); break;
		case eVertical  : strErrMsg = LoadStringById(IDS_DRIVESELECTDIALOG_MSG014); break;
		case eResolution: strErrMsg = LoadStringById(IDS_DRIVESELECTDIALOG_MSG015); break;
		}

		int nRow = m_lcCategory[i].GetRadioSelectedIndex();
		if(m_lcCategory[i].GetItemCount() > 0)
		{
			if(nRow < 0)
			{
				UbcMessageBox(strErrMsg);
				return;
			}
			m_nCategorys[i] = (long)m_lcCategory[i].GetItemData(nRow);
		}
	}

	m_bIsPublic = (m_kbPublic.GetCheck() == TRUE);

	OnOK();
}

BOOL CDriveSelectDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editHostName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			// 공백 과 사용제한 문자 체크
			if( (TCHAR)pMsg->wParam == ' ' ||
				CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam, CPreventChar::ePackageName) )
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDriveSelectDialog::OnBnClickedBnSite()
{
	CString strSiteId;
	CString strSiteName;
	if(GetSelectSite(GetEnvPtr()->m_strCustomer, strSiteId, strSiteName))
	{
		SetDlgItemText(IDC_EDIT_SITE_NAME, strSiteName);
		m_strSite = strSiteId;
	}
}

// skpark 2012.10.16 USB_Device  : USB 에 있는 패키지를 모두 가져온다.
int
CDriveSelectDialog::GetIniNames(CString& myName, CString& targetPath, CStringArray& outList)
{
	CString myNameBuf = myName + ".ini";

	CString pattern = targetPath;
	pattern.Append("*.ini");

	WIN32_FIND_DATA fileData;
	HANDLE hFind = FindFirstFile(pattern,&fileData);
	if(hFind == INVALID_HANDLE_VALUE){
		return 0;
	}

	int counter=0;
	do {
		if(fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
			continue;
		}
		if(myNameBuf.Compare(fileData.cFileName)==0){
			// 자기 자신은 제외한다.
			continue;
		}
		outList.Add(fileData.cFileName);
		counter++;

	} while(FindNextFile(hFind, &fileData));
	FindClose(hFind);

	return counter;
}
void CDriveSelectDialog::OnBnClickedCheckMonitoron()
{
	if(m_ckMonitorOn.GetCheck()){
		this->m_ckMonitorOff.SetCheck(false);
	}
}

void CDriveSelectDialog::OnBnClickedCheckMonitoroff()
{
	if(m_ckMonitorOff.GetCheck()){
		this->m_ckMonitorOn.SetCheck(false);
	}
}
