// NotiMngView.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "NotiMngView.h"
#include "AnnounceDlg.h"
#include "common/UbcGUID.h"
#include "common/libProfileManager/ProfileManager.h"
#include "common/MD5Util.h"

#define STR_SITEID			LoadStringById(IDS_NOTIMNGVIEW_LST001)
#define STR_ANNOUID			LoadStringById(IDS_NOTIMNGVIEW_LST002)
#define STR_TITLE			LoadStringById(IDS_NOTIMNGVIEW_LST003)
#define STR_STARTTIME		LoadStringById(IDS_NOTIMNGVIEW_LST004)
#define STR_ENDTIME			LoadStringById(IDS_NOTIMNGVIEW_LST005)
#define STR_CREATOR			LoadStringById(IDS_NOTIMNGVIEW_LST006)
#define STR_HOSTLIST		LoadStringById(IDS_NOTIMNGVIEW_LST007)
#define STR_CRETIME			LoadStringById(IDS_NOTIMNGVIEW_LST008)
#define STR_COMMENT			LoadStringById(IDS_NOTIMNGVIEW_LST009)

// CNotiMngView
IMPLEMENT_DYNCREATE(CNotiMngView, CFormView)

CNotiMngView::CNotiMngView() : CFormView(CNotiMngView::IDD)
,	m_Reposition(this)
{
	m_bInit = false;
}

CNotiMngView::~CNotiMngView()
{
}

void CNotiMngView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_NOTIFICATION, m_lscNoti);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_btAdd);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_btDel);
	DDX_Control(pDX, IDC_BUTTON_MOD, m_btMod);
	DDX_Control(pDX, IDC_BUTTON_REF, m_btRef);
}

BEGIN_MESSAGE_MAP(CNotiMngView, CFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()

	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_NOTIFICATION, OnLvnColumnclickList)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_NOTIFICATION, OnNMCustomdrawList)
	ON_NOTIFY(NM_CLICK, IDC_LIST_NOTIFICATION, OnNMClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_NOTIFICATION, OnNMDblclkList)

	ON_BN_CLICKED(IDC_BUTTON_ADD, OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_MOD, OnBnClickedButtonMod)
	ON_BN_CLICKED(IDC_BUTTON_REF, OnBnClickedButtonRef)
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
END_MESSAGE_MAP()

// CNotiMngView diagnostics
#ifdef _DEBUG
void CNotiMngView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CNotiMngView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CNotiMngView message handlers
int CNotiMngView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	GetClientRect(&m_rcClient);

	return 0;
}

void CNotiMngView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	if(m_bInit)	return;

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_NOTIMNGVIEW_STR001));

	m_btAdd.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btDel.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btMod.LoadBitmap(IDB_BUTTON_MODIFY, RGB(255,255,255));
	m_btRef.LoadBitmap(IDB_BUTTON_REFRESH2, RGB(255,255,255));

	m_btAdd.SetToolTipText(LoadStringById(IDS_NOTIMNGVIEW_STR002));
	m_btDel.SetToolTipText(LoadStringById(IDS_NOTIMNGVIEW_STR003));
	m_btMod.SetToolTipText(LoadStringById(IDS_NOTIMNGVIEW_STR004));
	m_btRef.SetToolTipText(LoadStringById(IDS_NOTIMNGVIEW_STR005));

	m_Reposition.SetParentRect(m_rcClient);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscNoti, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	InitList();
//	SetList();
}

void CNotiMngView::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CFormView::OnClose();
}

void CNotiMngView::OnDestroy()
{
	CFormView::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하콅E
	// 리스트의 컬럼상태 저픸E
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscNoti.SaveColumnState("NOTIMNG-LIST", szPath);
}

void CNotiMngView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_Reposition.Move();
}

void CNotiMngView::InitList()
{
	// 2010.02.17 by gwangsoo
	m_ColumTitle[eCheck     ] = _T("");
//	m_ColumTitle[eSiteId] = STR_SITEID;
//	m_ColumTitle[eAnnoId] = STR_ANNOUID;
	m_ColumTitle[eTitle     ] = STR_TITLE;
	m_ColumTitle[eStartTime ] = STR_STARTTIME;
	m_ColumTitle[eEndTime   ] = STR_ENDTIME;
	m_ColumTitle[eCreator   ] = STR_CREATOR;
	m_ColumTitle[eHostList  ] = STR_HOSTLIST;
	m_ColumTitle[eCreateTime] = STR_CRETIME;
	m_ColumTitle[eComment   ] = STR_COMMENT;

	m_lscNoti.SetExtendedStyle(m_lscNoti.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	//m_lscNoti.SetExtendedStyle(m_lscNoti.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_INFOTIP);

	for(int nCol = 0; nCol < eEnd; nCol++)
	{
		m_lscNoti.InsertColumn(nCol, m_ColumTitle[nCol], LVCFMT_LEFT, 150);
	}

	m_lscNoti.SetColumnWidth(m_lscNoti.GetColPos(m_ColumTitle[eCheck]), 22); // 2010.02.17 by gwangsoo

//	m_lscNoti.SetColumnWidth(eTitle, 300);
	m_lscNoti.InitHeader(IDB_LIST_HEADER);
//	m_lscNoti.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하콅E
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscNoti.LoadColumnState("NOTIMNG-LIST", szPath);
}

void CNotiMngView::SetList()
{
	if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority){
		CCopModule::GetObject()->GetAnnounce("*", m_lsAnnounce);
	}else{
		CCopModule::GetObject()->GetAnnounce(GetEnvPtr()->m_szSite, m_lsAnnounce);
	}

	SYSTEMTIME stm;
	ZeroMemory(&stm, sizeof(SYSTEMTIME));
	GetLocalTime(&stm);
	CTime tmCur(stm);

	m_lscNoti.DeleteAllItems();

	bool	redraw = false;
	int nRow = 0;
	POSITION pos = m_lsAnnounce.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SAnnounceInfo info = m_lsAnnounce.GetNext(pos);
		
		m_lscNoti.InsertItem(nRow, "");
		m_lscNoti.SetItemText(nRow, eStartTime, info.StartTime.Format(STR_DEF_TIME));
		m_lscNoti.SetItemText(nRow, eEndTime  , info.EndTime.Format(STR_DEF_TIME));
		m_lscNoti.SetItemText(nRow, eTitle    , info.Title);
		m_lscNoti.SetItemText(nRow, eCreator  , info.Creator);

		cciStringList strBuf = info.HostIdList;
		m_lscNoti.SetItemText(nRow, eHostList, strBuf.toString());

		m_lscNoti.SetItemText(nRow, eCreateTime, info.CreateTime.Format(STR_DEF_TIME));
		m_lscNoti.SetItemText(nRow, eComment, info.Comment[0]);


		//if(info.StartTime <= tmCur && tmCur < info.EndTime){
		//	CString szBuf = "*";
		//	szBuf += info.StartTime.Format(STR_DEF_TIME);
		//	m_lscNoti.SetItemText(nRow, eStartTime, szBuf);
		//}
		/*
		//skpark eSiteUser 자기것이 아닌 경퓖E 회색으로 처리
		if(CCopModule::eSiteUser == GetEnvPtr()->m_Authority){
			if(info.Creator != GetEnvPtr()->m_szLoginID){
				TraceLog(("NOT ALLOWED TO MODIFY : %s!=%s", info.Creator, GetEnvPtr()->m_szLoginID));
				m_lscNoti.SetRowColor(nRow, RGB(255,255,255), RGB(128,128,128));
				redraw = true;
			}
			
		}
		*/
		m_lscNoti.SetItemData(nRow, (DWORD_PTR)posOld);
		nRow++;
	}
	//if(redraw) {
	//	m_lscNoti.SetRedraw(TRUE);
	//}

}

int CNotiMngView::CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	return ((CNotiMngView*)lParam)->m_lscNoti.Compare(lParam1, lParam2);
}

void CNotiMngView::OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int	nColumn = pNMLV->iSubItem;
	
	// 2010.02.17 by gwangsoo
	if(nColumn == m_lscNoti.GetColPos(m_ColumTitle[eCheck]))
	{
		bool bCheck = !m_lscNoti.GetCheckHdrState();
		for(int nRow=0; nRow<m_lscNoti.GetItemCount() ;nRow++)
		{
			m_lscNoti.SetCheck(nRow, bCheck);
		}
		m_lscNoti.SetCheckHdrState(bCheck);
	}
	else
	{
		m_lscNoti.SetSortHeader(nColumn);
		m_lscNoti.SortItems(CompareSite, (DWORD_PTR)this);
	}
}

void CNotiMngView::OnNMCustomdrawList(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );

	if( CDDS_PREPAINT == pLVCD->nmcd.dwDrawStage ){
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;
	}

	if( CDDS_ITEMPREPAINT != pLVCD->nmcd.dwDrawStage )
		return;

	int nRow = pLVCD->nmcd.dwItemSpec;

	SYSTEMTIME stm;
	GetLocalTime(&stm);
	CTime tmCur(stm);

	POSITION pos = (POSITION)m_lscNoti.GetItemData(nRow);
	if(!pos)	return;

	SAnnounceInfo info = m_lsAnnounce.GetAt(pos);

	if(info.StartTime <= tmCur && tmCur < info.EndTime){
		COLORREF crText = RGB(0,0,0);
        COLORREF crTextBk = RGB(255,255,0);
		pLVCD->clrText = crText;
        pLVCD->clrTextBk = crTextBk;
		*pResult = CDRF_DODEFAULT;
	}

	//skpark eSiteUser 자기것이 아닌 경퓖E 회색으로 처리
	if(CCopModule::eSiteUser == GetEnvPtr()->m_Authority){
		if(info.Creator != GetEnvPtr()->m_szLoginID){
			TraceLog(("NOT ALLOWED TO MODIFY : %s!=%s", info.Creator, GetEnvPtr()->m_szLoginID));
			COLORREF crText = RGB(128,128,128);
			pLVCD->clrText = crText;
			//*pResult = CDRF_DODEFAULT;
			m_lscNoti.SetCheck(nRow,false);
			//m_lscNoti.RedrawItems(nRow,nRow);
		}
	}

}

void CNotiMngView::OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
/*
	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscNoti.GetItemData(nRow);
	SAnnounceInfo info = m_lsAnnounce.GetAt(pos);
*/
}

void CNotiMngView::OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscNoti.GetItemData(nRow);
	SAnnounceInfo info = m_lsAnnounce.GetAt(pos);

	CAnnounceDlg dlg;
	dlg.SetInfo(info);
	
	//skpark eSiteUser 자기것이 아닌 경퓖E 회색으로 처리
	if(CCopModule::eSiteUser == GetEnvPtr()->m_Authority){
		if(info.Creator != GetEnvPtr()->m_szLoginID){
			TraceLog(("NOT ALLOWED TO MODIFY : %s!=%s", info.Creator, GetEnvPtr()->m_szLoginID));
			dlg.m_bReadOnly = true;
		}
	}

	if(dlg.DoModal() != IDOK) return;

	dlg.GetInfo(info);

	if(info.AnnoId.IsEmpty())
	{
		info.AnnoId = CUbcGUID::GetInstance()->ToGUID(_T(""));
	}

	// 0001375: 긴급공햨E방식을 현재의 Event 방식에서 INI download 방식으로 변경한다.
	CString szLocalPath;
	szLocalPath.Format("%s%sdata\\", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	CString szAnnounceFile;
	szAnnounceFile.Format("UBCAnnounce_%s.ini", info.AnnoId);

	if(!SaveFile(szLocalPath + szAnnounceFile, info))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_NOTIMNGVIEW_MSG003), info.Title);
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!GetEnvPtr()->PutFile(szAnnounceFile, szLocalPath, "/config/", GetEnvPtr()->m_szSite))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_NOTIMNGVIEW_MSG004), info.Title);
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!info.BgFile.IsEmpty())
	{
		TraceLog(("BgFile exist, %s will be uploaded", info.BgFile));
		if(PutFile(info)){
			if(CCopModule::GetObject()->SetAnnounce(info.SiteId, info.AnnoId, info))
			{
				m_lsAnnounce.GetAt(pos) = info;
				m_lscNoti.SetItemText(nRow, eTitle, info.Title);
			}
		}
	}else{
		if(CCopModule::GetObject()->SetAnnounce(info.SiteId, info.AnnoId, info))
		{
			m_lsAnnounce.GetAt(pos) = info;
			m_lscNoti.SetItemText(nRow, eTitle, info.Title);
		}
	}
}

void CNotiMngView::OnBnClickedButtonAdd()
{
	CAnnounceDlg dlg;
	if(dlg.DoModal() != IDOK) return;

	SAnnounceInfo info;
	dlg.GetInfo(info);

	if(info.AnnoId.IsEmpty())
	{
		info.AnnoId = CUbcGUID::GetInstance()->ToGUID(_T(""));
	}

	// 0001375: 긴급공햨E방식을 현재의 Event 방식에서 INI download 방식으로 변경한다.
	CString szLocalPath;
	szLocalPath.Format("%s%sdata\\", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	CString szAnnounceFile;
	szAnnounceFile.Format("UBCAnnounce_%s.ini", info.AnnoId);

	if(!SaveFile(szLocalPath + szAnnounceFile, info))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_PLANVIEW_MSG008), info.Title);
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!GetEnvPtr()->PutFile(szAnnounceFile, szLocalPath, "/config/", GetEnvPtr()->m_szSite))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_PLANVIEW_MSG008), info.Title);
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	TraceLog(("BGFile=%s", info.BgFile));
	if(!info.BgFile.IsEmpty())
	{
		if(PutFile(info)){
			if(CCopModule::GetObject()->CreAnnounce(GetEnvPtr()->m_szSite, GetEnvPtr()->m_szLoginID, info))
			{
				TraceLog(("BGFile=%s", info.BgFile));
			}
		};
	}else{
		if(CCopModule::GetObject()->CreAnnounce(GetEnvPtr()->m_szSite, GetEnvPtr()->m_szLoginID, info))
		{
			TraceLog(("BGFile=%s", info.BgFile));
		}
	}
}

void CNotiMngView::OnBnClickedButtonDel()
{
	int nCnt = 0;
	for(int nRow = m_lscNoti.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscNoti.GetCheck(nRow)) continue;
		nCnt++;
	}

	if(nCnt == 0)
	{
		UbcMessageBox(LoadStringById(IDS_NOTIMNGVIEW_MSG002), MB_ICONINFORMATION);
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_NOTIMNGVIEW_MSG001), MB_YESNO|MB_ICONINFORMATION) != IDYES) return;

	for(int nRow = m_lscNoti.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscNoti.GetCheck(nRow)) continue;

		m_lscNoti.SetCheck(nRow, false);
		POSITION pos = (POSITION)m_lscNoti.GetItemData(nRow);
		SAnnounceInfo info = m_lsAnnounce.GetNext(pos);

		if(info.SiteId.GetLength()==0 || info.AnnoId.GetLength()==0) continue;
		if(CCopModule::GetObject()->DelAnnounce(info.SiteId, info.AnnoId))
		{
			// InvokeEvent를 흟E?서버로부터 이벤트를 받아서 처리되므로 주석처리
			//m_lscNoti.DeleteItem(nRow);
			//m_lsAnnounce.RemoveAt(pos);
		}
	}
}

void CNotiMngView::OnBnClickedButtonMod()
{
	int nCnt = 0;
	for(int nRow = m_lscNoti.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscNoti.GetCheck(nRow)) continue;
		nCnt++;
	}

	if(nCnt == 0)
	{
		UbcMessageBox(LoadStringById(IDS_NOTIMNGVIEW_MSG002), MB_ICONINFORMATION);
		return;
	}

	for(int nRow = 0; nRow < m_lscNoti.GetItemCount(); nRow++)
	{
		if(!m_lscNoti.GetCheck(nRow)) continue;

		m_lscNoti.SetCheck(nRow, false);
		POSITION pos = (POSITION)m_lscNoti.GetItemData(nRow);
		SAnnounceInfo info = m_lsAnnounce.GetAt(pos);

		CAnnounceDlg dlg;
		dlg.SetInfo(info);

		if(dlg.DoModal() != IDOK) return;

		dlg.GetInfo(info);

		if(info.AnnoId.IsEmpty())
		{
			info.AnnoId = CUbcGUID::GetInstance()->ToGUID(_T(""));
		}

		// 0001375: 긴급공햨E방식을 현재의 Event 방식에서 INI download 방식으로 변경한다.
		CString szLocalPath;
		szLocalPath.Format("%s%sdata\\", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

		CString szAnnounceFile;
		szAnnounceFile.Format("UBCAnnounce_%s.ini", info.AnnoId);

		if(!SaveFile(szLocalPath + szAnnounceFile, info))
		{
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_NOTIMNGVIEW_MSG003), info.Title);
			UbcMessageBox(szMsg, MB_ICONWARNING);
			continue;
		}

		if(!GetEnvPtr()->PutFile(szAnnounceFile, szLocalPath, "/config/", GetEnvPtr()->m_szSite))
		{
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_NOTIMNGVIEW_MSG004), info.Title);
			UbcMessageBox(szMsg, MB_ICONWARNING);
			continue;
		}

		if(!info.BgFile.IsEmpty())
		{
			TraceLog(("BGFile=%s", info.BgFile));
			if(PutFile(info)) {
				if(CCopModule::GetObject()->SetAnnounce(info.SiteId, info.AnnoId, info))
				{
					m_lsAnnounce.GetAt(pos) = info;
					m_lscNoti.SetItemText(nRow, eTitle, info.Title);

				}
			}
		}else{
			if(CCopModule::GetObject()->SetAnnounce(info.SiteId, info.AnnoId, info))
			{
				m_lsAnnounce.GetAt(pos) = info;
				m_lscNoti.SetItemText(nRow, eTitle, info.Title);

			}
		}
	}
}

bool
CNotiMngView::PutFile(SAnnounceInfo& info)
{
	TraceLog(("PutFile(%s, size=%ld)",info.BgFile, info.FileSize));

	if(!info.IsFileChanged){
		TraceLog(("%s not changed", info.BgFile));
		return true;
	}

	CString localLocation;
	localLocation.Format("%s\\%s%s\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER);

	CString serverLocation;
	serverLocation.Format("\\Contents\\announce\\%s\\", info.AnnoId);
	CString serverFullPath = serverLocation + info.BgFile;
	CString localFullPath = localLocation + info.BgFile;

	CString waitMsg;
	waitMsg.Format(LoadStringById(IDS_ANNOUNCE_WAIT002), info.BgFile);
	CWaitMessageBox wait(waitMsg);

	ULONGLONG localFileSize=0;
	CEnviroment::GetFileSize(localFullPath, localFileSize);

	if(localFileSize == 0 || info.FileSize != localFileSize){
		TraceLog(("local size(%d) and info.fileSize(%d) is different", localFileSize, info.FileSize));
		return false;
	}

	if(localFileSize < 1000000)
	{
		char szMd5[16*2+1] = {0};
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)localFullPath, szMd5);

		if(strcmp(info.FileMD5, szMd5) != 0)
		{
			TraceLog(("local MD5(%s) and info.MD5 is different", szMd5, info.FileMD5));
			return false;
		}
	}

	if(!GetEnvPtr()->PutFile(info.BgFile, localLocation, serverLocation, info.SiteId))
	{
		// 한퉩E큱E시도한다. (과踏 존재한다)
		if(!GetEnvPtr()->PutFile(info.BgFile, localLocation, serverLocation, info.SiteId)){
			CString msg;
			msg.Format(LoadStringById(IDS_NOTIMNGVIEW_MSG005),info.BgFile);
			UbcMessageBox(msg);
			return false;
		}
	}
	return true;
}

void CNotiMngView::OnBnClickedButtonRef()
{
	SetList();
	UbcMessageBox(LoadStringById(IDS_REFRASHLIST_MSG), MB_ICONINFORMATION);
}

LRESULT CNotiMngView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	cciEvent* ev = (cciEvent*)lParam;
/*
	ciString strType = ev->getEventType();
	CCI::CCI_AttributeList& tempList = ev->getAttributeList();
	cciAttributeList attList(tempList);
	cciAttributeList attList2;
*/
	switch(wParam)
	{
	case IEH_CREANNO:
		{
			cciAttributeList attList;
			ev->getItem("attributes", attList);
			CreateEvent(attList);
		}
		break;
	case IEH_CHGANNO:
		{
			cciAttributeList attList;
			ciString annoId;
			ev->getItem("attributes", attList);
			ev->getItem("announceId", annoId);
			ChangeEvent(annoId.c_str(), attList);
		}
		break;
	case IEH_RMVANNO:
		{
			CCI::CCI_AttributeList& tempList = ev->getAttributeList();
			cciAttributeList attList(tempList);
			RemoveEvent(attList);
		}
		break;
	case IEH_EXPANNO:
		{
//			cciAttributeList attList;
//			ev->getItem("attributes", attList);
			ciString annoId;
			ciBoolean isStart;
			ev->getItem("announceId", annoId);
			ev->getItem("isStart", isStart);
			ExpiredEvent(annoId.c_str(), isStart);
		}
		break;
	}

	return 0;
}

void CNotiMngView::CreateEvent(cciAttributeList& attList)
{
	SAnnounceInfo info;
	for(int i = 0; i < (int)attList.length(); i++){
		cciAttribute& attr = attList[i];
		
		ciString	strBuf;
		ciLong		lBuf;
		ciULong		ulBuf;
		ciTime		tmBuf;
		ciShort		sBuf;
		cciStringList ccislBuf;
		CCI::CCI_StringList slBuf;

		if(IsSame("mgrId", attr.getName())){
			attr.getValue(strBuf);
			info.MgrId = strBuf.c_str();
		}
		else if(IsSame("siteId", attr.getName())){
			attr.getValue(strBuf);
			info.SiteId = strBuf.c_str();
		}
		else if(IsSame("announceId", attr.getName())){
			attr.getValue(strBuf);
			info.AnnoId = strBuf.c_str();
		}
		else if(IsSame("title", attr.getName())){
			attr.getValue(strBuf);
			info.Title = strBuf.c_str();
		}
		else if(IsSame("serialNo", attr.getName())){
			attr.getValue(ulBuf);
			info.SerialNo = ulBuf;
		}
		else if(IsSame("creator", attr.getName())){
			attr.getValue(strBuf);
			info.Creator = strBuf.c_str();
		}
		else if(IsSame("createTime", attr.getName())){
			attr.getValue(tmBuf);
			info.CreateTime = tmBuf.getTime();
		}
		else if(IsSame("hostIdList", attr.getName())){
			attr.getValueStringList(slBuf);
			ccislBuf.set(slBuf);
			info.HostIdList = ccislBuf.getStringList();
		}
		else if(IsSame("startTime", attr.getName())){
			attr.getValue(tmBuf);
			info.StartTime = tmBuf.getTime();
		}
		else if(IsSame("endTime", attr.getName())){
			attr.getValue(tmBuf);
			info.EndTime = tmBuf.getTime();
		}
		else if(IsSame("position", attr.getName())){
			attr.getValue(lBuf);
			info.Position = lBuf;
		}
		else if(IsSame("height", attr.getName())){
			attr.getValue(sBuf);
			info.Height = sBuf;
		}
		else if(IsSame("font", attr.getName())){
			attr.getValue(strBuf);
			info.Font = strBuf.c_str();
		}
		else if(IsSame("fontSize", attr.getName())){
			attr.getValue(sBuf);
			info.FontSize = sBuf;
		}
		else if(IsSame("fgColor", attr.getName())){
			attr.getValue(strBuf);
			info.FgColor = strBuf.c_str();
		}
		else if(IsSame("bgColor", attr.getName())){
			attr.getValue(strBuf);
			info.BgColor = strBuf.c_str();
		}
		else if(IsSame("bgLocation", attr.getName())){
			attr.getValue(strBuf);
			info.BgLocation = strBuf.c_str();
		}
		else if(IsSame("bgFile", attr.getName())){
			attr.getValue(strBuf);
			info.BgFile = strBuf.c_str();
		}
		else if(IsSame("playSpeed", attr.getName())){
			attr.getValue(sBuf);
			info.Speed = sBuf;
		}
		else if(IsSame("alpha", attr.getName())){
			attr.getValue(sBuf);
			info.Alpha = sBuf;
		}
		else if(IsSame("comment1", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[0] = strBuf.c_str();
		}
		else if(IsSame("comment2", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[1] = strBuf.c_str();
		}
		else if(IsSame("comment3", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[2] = strBuf.c_str();
		}
		else if(IsSame("comment4", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[3] = strBuf.c_str();
		}
		else if(IsSame("comment5", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[4] = strBuf.c_str();
		}
		else if(IsSame("comment6", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[5] = strBuf.c_str();
		}
		else if(IsSame("comment7", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[6] = strBuf.c_str();
		}
		else if(IsSame("comment8", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[7] = strBuf.c_str();
		}
		else if(IsSame("comment9", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[8] = strBuf.c_str();
		}
		else if(IsSame("comment10", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[9] = strBuf.c_str();
		}
/*
	std::list<std::string> HostNameList;
	int	ContentsType;
	int LRMargin;
	int UDMargin;
	bool SourceSize;
	int SoundVolume;
	ULONGLONG FileSize;
	CString FileMD5;
*/

		else if(IsSame("hostNameList", attr.getName())){
			attr.getValueStringList(slBuf);
			ccislBuf.set(slBuf);
			info.HostNameList = ccislBuf.getStringList();
		}
		else if(IsSame("contentsType", attr.getName())){
			attr.getValue(sBuf);
			info.ContentsType = sBuf;
		}
		else if(IsSame("lrMargin", attr.getName())){
			attr.getValue(sBuf);
			info.LRMargin = sBuf;
		}
		else if(IsSame("udMargin", attr.getName())){
			attr.getValue(sBuf);
			info.UDMargin = sBuf;
		}
		else if(IsSame("sourceSize", attr.getName())){
			attr.getValue(sBuf);
			info.SourceSize = sBuf;
		}
		else if(IsSame("soundVolume", attr.getName())){
			attr.getValue(sBuf);
			info.SoundVolume = sBuf;
		}
		else if(IsSame("fileSize", attr.getName())){
			ciULongLong uBuf;
			attr.getValue(uBuf);
			info.FileSize = uBuf;
		}
		else if(IsSame("fileMD5", attr.getName())){
			attr.getValue(strBuf);
			info.FileMD5 = strBuf.c_str();
		}
		else if(IsSame("width", attr.getName())){
			attr.getValue(sBuf);
			info.Width = sBuf;
		}
		else if(IsSame("posX", attr.getName())){
			attr.getValue(sBuf);
			info.PosX = sBuf;
		}
		else if(IsSame("posY", attr.getName())){
			attr.getValue(sBuf);
			info.PosY = sBuf;
		}
		else if(IsSame("heightRatio", attr.getName())){
			ciFloat fBuf=0;
			attr.getValue(fBuf);
			if(fBuf==0) {
				fBuf=(9.0/16.0);
			}
			info.HeightRatio = fBuf;
		}
	}

	POSITION pos = m_lsAnnounce.AddTail(info);
	int nRow = m_lscNoti.GetItemCount();

	m_lscNoti.InsertItem(nRow, "");
	m_lscNoti.SetItemText(nRow, eStartTime, info.StartTime.Format(STR_DEF_TIME));
	m_lscNoti.SetItemText(nRow, eEndTime  , info.EndTime.Format(STR_DEF_TIME));
	m_lscNoti.SetItemText(nRow, eTitle    , info.Title);
	m_lscNoti.SetItemText(nRow, eCreator  , info.Creator);

	cciStringList strBuf = info.HostIdList;
	m_lscNoti.SetItemText(nRow, eHostList, strBuf.toString());
	
	m_lscNoti.SetItemText(nRow, eCreateTime, info.CreateTime.Format(STR_DEF_TIME));
	m_lscNoti.SetItemText(nRow, eComment, info.Comment[0]);

	m_lscNoti.SetItemData(nRow, (DWORD_PTR)pos);
}

void CNotiMngView::RemoveEvent(cciAttributeList& attList)
{
	ciString strBuf;
	for(int i = 0; i < (int)attList.length(); i++){
		cciAttribute& attr = attList[i];
		
		if(IsSame("announceId", attr.getName())){
			attr.getValue(strBuf);
		}
	}

	for(int nRow = 0; nRow < m_lscNoti.GetItemCount(); nRow++){
		POSITION pos = (POSITION)m_lscNoti.GetItemData(nRow);
		SAnnounceInfo data = m_lsAnnounce.GetAt(pos);

		if(data.AnnoId == strBuf.c_str())
		{
			m_lscNoti.DeleteItem(nRow);
			m_lsAnnounce.RemoveAt(pos);
			break;
		}
	}
}

void CNotiMngView::ChangeEvent(CString szAnnoId, cciAttributeList& attList)
{
	SAnnounceInfo info;
	for(int i = 0; i < (int)attList.length(); i++){
		cciAttribute& attr = attList[i];
		
		ciString	strBuf;
		ciLong		lBuf;
		ciULong		ulBuf;
		ciTime		tmBuf;
		ciShort		sBuf;
		cciStringList ccislBuf;
		CCI::CCI_StringList slBuf;

		if(IsSame("mgrId", attr.getName())){
			attr.getValue(strBuf);
			info.MgrId = strBuf.c_str();
		}
		else if(IsSame("siteId", attr.getName())){
			attr.getValue(strBuf);
			info.SiteId = strBuf.c_str();
		}
		else if(IsSame("announceId", attr.getName())){
			attr.getValue(strBuf);
			info.AnnoId = strBuf.c_str();
		}
		else if(IsSame("title", attr.getName())){
			attr.getValue(strBuf);
			info.Title = strBuf.c_str();
		}
		else if(IsSame("serialNo", attr.getName())){
			attr.getValue(ulBuf);
			info.SerialNo = ulBuf;
		}
		else if(IsSame("creator", attr.getName())){
			attr.getValue(strBuf);
			info.Creator = strBuf.c_str();
		}
		else if(IsSame("createTime", attr.getName())){
			attr.getValue(tmBuf);
			info.CreateTime = tmBuf.getTime();
		}
		else if(IsSame("hostIdList", attr.getName())){
			attr.getValueStringList(slBuf);
			ccislBuf.set(slBuf);
			info.HostIdList = ccislBuf.getStringList();
		}
		else if(IsSame("startTime", attr.getName())){
			attr.getValue(tmBuf);
			info.StartTime = tmBuf.getTime();
		}
		else if(IsSame("endTime", attr.getName())){
			attr.getValue(tmBuf);
			info.EndTime = tmBuf.getTime();
		}
		else if(IsSame("position", attr.getName())){
			attr.getValue(lBuf);
			info.Position = lBuf;
		}
		else if(IsSame("height", attr.getName())){
			attr.getValue(sBuf);
			info.Height = sBuf;
		}
		else if(IsSame("font", attr.getName())){
			attr.getValue(strBuf);
			info.Font = strBuf.c_str();
		}
		else if(IsSame("fontSize", attr.getName())){
			attr.getValue(sBuf);
			info.FontSize = sBuf;
		}
		else if(IsSame("fgColor", attr.getName())){
			attr.getValue(strBuf);
			info.FgColor = strBuf.c_str();
		}
		else if(IsSame("bgColor", attr.getName())){
			attr.getValue(strBuf);
			info.BgColor = strBuf.c_str();
		}
		else if(IsSame("bgLocation", attr.getName())){
			attr.getValue(strBuf);
			info.BgLocation = strBuf.c_str();
		}
		else if(IsSame("bgFile", attr.getName())){
			attr.getValue(strBuf);
			info.BgFile = strBuf.c_str();
		}
		else if(IsSame("playSpeed", attr.getName())){
			attr.getValue(sBuf);
			info.Speed = sBuf;
		}
		else if(IsSame("alpha", attr.getName())){
			attr.getValue(sBuf);
			info.Alpha = sBuf;
		}
		else if(IsSame("comment1", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[0] = strBuf.c_str();
		}
		else if(IsSame("comment2", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[1] = strBuf.c_str();
		}
		else if(IsSame("comment3", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[2] = strBuf.c_str();
		}
		else if(IsSame("comment4", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[3] = strBuf.c_str();
		}
		else if(IsSame("comment5", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[4] = strBuf.c_str();
		}
		else if(IsSame("comment6", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[5] = strBuf.c_str();
		}
		else if(IsSame("comment7", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[6] = strBuf.c_str();
		}
		else if(IsSame("comment8", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[7] = strBuf.c_str();
		}
		else if(IsSame("comment9", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[8] = strBuf.c_str();
		}
		else if(IsSame("comment10", attr.getName())){
			attr.getValue(strBuf);
			info.Comment[9] = strBuf.c_str();
		}
/*
	std::list<std::string> HostNameList;
	int	ContentsType;
	int LRMargin;
	int UDMargin;
	bool SourceSize;
	int SoundVolume;
	ULONGLONG FileSize;
	CString	FileMD5;
*/

		else if(IsSame("hostNameList", attr.getName())){
			attr.getValueStringList(slBuf);
			ccislBuf.set(slBuf);
			info.HostNameList = ccislBuf.getStringList();
		}
		else if(IsSame("contentsType", attr.getName())){
			attr.getValue(sBuf);
			info.ContentsType = sBuf;
		}
		else if(IsSame("lrMargin", attr.getName())){
			attr.getValue(sBuf);
			info.LRMargin = sBuf;
		}
		else if(IsSame("udMargin", attr.getName())){
			attr.getValue(sBuf);
			info.UDMargin = sBuf;
		}
		else if(IsSame("sourceSize", attr.getName())){
			attr.getValue(sBuf);
			info.SourceSize = sBuf;
		}
		else if(IsSame("soundVolume", attr.getName())){
			attr.getValue(sBuf);
			info.SoundVolume = sBuf;
		}
		else if(IsSame("fileSize", attr.getName())){
			ciULongLong uBuf;
			attr.getValue(uBuf);
			info.FileSize = uBuf;
		}
		else if(IsSame("fileMD5", attr.getName())){
			attr.getValue(strBuf);
			info.FileMD5 = strBuf.c_str();
		}
		else if(IsSame("width", attr.getName())){
			attr.getValue(sBuf);
			info.Width = sBuf;
		}
		else if(IsSame("posX", attr.getName())){
			attr.getValue(sBuf);
			info.PosX = sBuf;
		}
		else if(IsSame("posY", attr.getName())){
			attr.getValue(sBuf);
			info.PosY = sBuf;
		}
		else if(IsSame("heightRatio", attr.getName())){
			ciFloat fBuf=0;
			attr.getValue(fBuf);
			if(fBuf==0) {
				fBuf=(9.0/16.0);
			}
			info.HeightRatio = fBuf;
		}

	}

	for(int nRow = 0; nRow < m_lscNoti.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscNoti.GetItemData(nRow);
		SAnnounceInfo data = m_lsAnnounce.GetAt(pos);
		if(data.AnnoId != szAnnoId)
			continue;

		info.MgrId = data.MgrId;
		info.SiteId = data.SiteId;
		info.AnnoId = data.AnnoId;
		info.SerialNo = data.SerialNo;
		info.Creator = data.Creator;
		info.CreateTime = data.CreateTime;

		m_lsAnnounce.GetAt(pos) = info;

		m_lscNoti.SetItemText(nRow, eStartTime, info.StartTime.Format(STR_DEF_TIME));
		m_lscNoti.SetItemText(nRow, eEndTime, info.EndTime.Format(STR_DEF_TIME));
		m_lscNoti.SetItemText(nRow, eTitle, info.Title);
		m_lscNoti.SetItemText(nRow, eCreator, info.Creator);

		cciStringList strBuf = info.HostIdList;
		m_lscNoti.SetItemText(nRow, eHostList, strBuf.toString());

		m_lscNoti.SetItemText(nRow, eCreateTime, info.CreateTime.Format(STR_DEF_TIME));
		m_lscNoti.SetItemText(nRow, eComment, info.Comment[0]);
		return;
	}
}

void CNotiMngView::ExpiredEvent(CString szAnnoId, bool bStart)
{
	for(int nRow = 0; nRow < m_lscNoti.GetItemCount(); nRow++){
		POSITION pos = (POSITION)m_lscNoti.GetItemData(nRow);
		SAnnounceInfo info = m_lsAnnounce.GetAt(pos);
		if(info.AnnoId != szAnnoId)
			continue;

		//if(bStart){
		//	CString szBuf = "*";
		//	szBuf += info.StartTime.Format(STR_DEF_TIME);
		//	m_lscNoti.SetItemText(nRow, eStartTime, szBuf);
		//}else{
			CString szBuf = "";
			szBuf += info.StartTime.Format(STR_DEF_TIME);
			m_lscNoti.SetItemText(nRow, eStartTime, szBuf);
		//}

		return;
	}
}

// 0001375: 긴급공햨E방식을 현재의 Event 방식에서 INI download 방식으로 변경한다.
BOOL CNotiMngView::SaveFile(CString szPath, SAnnounceInfo info)
{
	CString strHostIdList;
	for(std::list<std::string>::iterator Iter = info.HostIdList.begin(); Iter != info.HostIdList.end(); Iter++)
	{
		CString strHostId = (*Iter).c_str();
		if(strHostId.IsEmpty()) continue;

		strHostIdList.AppendFormat(_T("\"%s\","), strHostId);
	}

	strHostIdList.Trim(_T(","));
	if(!strHostIdList.IsEmpty())
	{
		strHostIdList = _T("[") + strHostIdList + _T("]");
	}

	CProfileManager objIniManager(szPath);

	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("lock")      , _T("0"));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("isStart")   , _T("1"));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("announceId"), info.AnnoId);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("mgrId")     , _T("1"));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("siteId")    , info.SiteId);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("title")     , info.Title);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("serialNo")  , ToString(info.SerialNo));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("creator")   , info.Creator);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("createTime"), ToString((ULONGLONG)CTime::GetCurrentTime().GetTime()));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("hostIdList"), strHostIdList);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("startTime") , ToString((ULONGLONG)info.StartTime.GetTime()));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("endTime")   , ToString((ULONGLONG)info.EndTime.GetTime()));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("position")  , ToString(info.Position));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("height")    , ToString(info.Height));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("font")      , info.Font);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("fontSize")  , ToString(info.FontSize));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("fgColor")   , info.FgColor);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("bgColor")   , info.BgColor);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("bglocation"), info.BgLocation);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("bgFile")    , info.BgFile);
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("playSpeed") , ToString(info.Speed));
	objIniManager.WriteProfileString(_T("ANNOUNCE"), _T("alpha")     , ToString(info.Alpha));

	for(int i=0 ; i<10 ; i++)
	{
		CString strKey;
		strKey.Format(_T("comment%d"), i+1);
		objIniManager.WriteProfileString(_T("ANNOUNCE"), strKey, info.Comment[i]);
	}

	objIniManager.Write();

	return TRUE;
}