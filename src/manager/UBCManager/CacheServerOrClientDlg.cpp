// CacheServerOrClientDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Enviroment.h"
#include "ubccopcommon\copmodule.h"
#include "ubccopcommon\UBCCopCommon.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "ubccopcommon\MultiSelectHost.h"
#include "CacheServerOrClientDlg.h"
#include "common\TraceLog.h"
#include "CacheServerSelectDlg.h"

// CCacheServerOrClientDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCacheServerOrClientDlg, CDialog)

CCacheServerOrClientDlg::CCacheServerOrClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCacheServerOrClientDlg::IDD, pParent)
{
	m_server_or_client = NO_SET;
}

CCacheServerOrClientDlg::~CCacheServerOrClientDlg()
{
}

void CCacheServerOrClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RADIO_CACHE_MASTER, m_radioServer);
	DDX_Control(pDX, IDC_RADIO_CACHE_SLAVE, m_radioClient);

}


BEGIN_MESSAGE_MAP(CCacheServerOrClientDlg, CDialog)
	ON_BN_CLICKED(IDCANCEL, &CCacheServerOrClientDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CCacheServerOrClientDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CCacheServerOrClientDlg 메시지 처리기입니다.

void CCacheServerOrClientDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CCacheServerOrClientDlg::OnBnClickedOk()
{
	if(m_radioServer.GetCheck()){
		m_server_or_client = SERVER;
		TraceLog(("Cache Server selected"));
		OnOK();
		return;
	}
	
	if(m_radioClient.GetCheck()){
		m_server_or_client = CLIENT;
		TraceLog(("Cache Client selected"));
		OnOK();
		return;
	}

	OnCancel();

}
