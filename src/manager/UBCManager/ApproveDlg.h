#pragma once

#include "common/HoverButton.h"
#include "common/utblistctrlex.h"
#include "UBCCopCommon/control/reposcontrol2.h"

#include "UBCCopCommon/CopModule.h"
#include "afxwin.h"


// CApproveDlg 대화 상자입니다.

class CApproveDlg : public CDialog
{
	DECLARE_DYNAMIC(CApproveDlg)

public:
	CApproveDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CApproveDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_APPROVE };

	enum {	eCheckBox=0, eApproveType, eMgrId, eApproveId, eApproveState, eAction, eTarget, eDetails,
			eRequestedId, eRequestedTime, eApprovedId, eApprovedTime, eDescription1, eDescription2, eMaxCol };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

protected:
	CReposControl2		m_Reposition;

	bool		m_bInitSort;

	CString		m_strColumnName[eMaxCol];
	APPROVE_INFO_LIST	m_ApproveInfoList;
	APPROVE_INFO_LIST	m_PreApproveInfoList;

	void		InitList();
	void		InitControl();
	void		RefreshList();
	void		DeleteList();

	BOOL		SetApproveState(int idx, APPROVE_INFO::APPROVE_STATUS eStatus);
	BOOL		SetApproveState(APPROVE_INFO* info, APPROVE_INFO::APPROVE_STATUS eStatus);
	BOOL		SetApproveStateOfSelectedItem(APPROVE_INFO::APPROVE_STATUS eStatus);

	BOOL		RemoveApproveObjectOfSelectedItem();
	BOOL		RemoveApproveObject(APPROVE_INFO* info);

	BOOL		GetSelectedItemList(CUIntArray& listSelected);

	CString		GetApproveStatusString(APPROVE_INFO::APPROVE_STATUS eStatus);
	CString		GetApproveTypeString(APPROVE_INFO::APPROVE_TYPE eType);

public:
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnBnClickedButtonApprove();
	afx_msg void OnBnClickedButtonReject();
	afx_msg void OnBnClickedButtonRemove();

	CHoverButton	m_btnRefresh;
	CComboBox		m_cbxApproveType;
	CComboBox		m_cbxApproveState;
	CDateTimeCtrl	m_dtcFilterStart;
	CDateTimeCtrl	m_dtcFilterEnd;
	CHoverButton	m_btnApprove;
	CHoverButton	m_btnReject;
	CHoverButton	m_btnRemove;
	CUTBListCtrlEx	m_lcApprove;
	CHoverButton	m_btnCancel;

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	BOOL	GetApproveInfoList(APPROVE_INFO_LIST& infoList, LPCSTR lpszApprovedId=NULL, bool bShowErrorMessage=true);
	void	SetPreApproveInfoList(APPROVE_INFO_LIST& infoList) { m_PreApproveInfoList.Copy(infoList); };
};
