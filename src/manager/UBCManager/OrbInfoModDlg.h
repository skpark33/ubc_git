#pragma once

#include "common\UTBListCtrl.h"

// COrbInfoModDlg dialog
class COrbInfoModDlg : public CDialog
{
	DECLARE_DYNAMIC(COrbInfoModDlg)

public:
	COrbInfoModDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COrbInfoModDlg();

// Dialog Data
	enum { IDD = IDD_CONN_INFO_ORB };

	void SetList(CUTBListCtrl*);
	void SetOrbInfo(LPCTSTR, LPCTSTR, int);
	void GetOrbInfo(CString&, CString&, int&);

	bool m_bModify;
protected:
	CString m_szName;
	CString m_szIP;
	CString m_szPort;
	CUTBListCtrl* m_plscCur;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedOk();
	int m_nPort;
};
