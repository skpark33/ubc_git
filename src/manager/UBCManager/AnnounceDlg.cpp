// AnnounceDlg.cpp : implementation file
//

#include "stdafx.h"
#include <algorithm>

#include "UBCManager.h"
#include "AnnounceDlg.h"
#include "Enviroment.h"
#include "ubccopcommon\MultiSelectHost.h"

// CAnnounceDlg dialog
IMPLEMENT_DYNAMIC(CAnnounceDlg, CDialog)

CAnnounceDlg::CAnnounceDlg(CWnd* pParent /*=NULL*/)	: CDialog(CAnnounceDlg::IDD, pParent)
, m_isInit(false)
, m_bReadOnly(false)
//,	m_cbFont(IDB_TTF)
//,	m_wndTicker (NULL, 0, false)
{
	m_AnnoInfo.Speed = Mng_Browser::E_TICKER_NORMAL;
}

CAnnounceDlg::~CAnnounceDlg()
{
}

void CAnnounceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_TITLE, m_szTitle);
	//DDX_Text(pDX, IDC_EDIT_FILE, m_szFile);
	DDX_Control(pDX, IDC_EDIT_HEIGHT, m_editHeight);
	//DDX_Text(pDX, IDC_EDIT_FONTSIZE, m_szFontSize);
	//DDX_Text(pDX, IDC_EDIT_ALPA, m_szAlpa);
	//DDX_Text(pDX, IDC_EDIT_COMMENT, m_szComment);

	DDX_Control(pDX, IDC_START_DATE, m_dtcStartDate);
	DDX_Control(pDX, IDC_START_TIME, m_dtcStartTime);
	DDX_Control(pDX, IDC_END_DATE, m_dtcEndDate);
	DDX_Control(pDX, IDC_END_TIME, m_dtcEndTime);
	DDX_Control(pDX, IDC_COMBO_POSITION, m_cbPosition);
	//DDX_Control(pDX, IDC_COMBO_FONT, m_cbFont);
	//DDX_Control(pDX, IDC_COMBO_FGCOLOR, m_cbFGColor);
	//DDX_Control(pDX, IDC_COMBO_BGCOLOR, m_cbBGColor);
	//DDX_Control(pDX, IDC_SLIDER_SPEED, m_sldSpeed);
	//DDX_Control(pDX, IDC_SPIN_FONTSIZE, m_spFontSize);
	DDX_Control(pDX, IDC_SPIN_HEIGHT, m_spHeight);
	//DDX_Control(pDX, IDC_SPIN_ALPA, m_spAlpa);
	DDX_Control(pDX, IDC_LIST_TERMINAL, m_lscTerminal);

	//DDX_Control(pDX, IDC_BUTTON_FILE, m_btFile);
	//DDX_Control(pDX, IDC_BUTTON_PLAY, m_btPlay);
	//DDX_Control(pDX, IDC_BUTTON_STOP, m_btStop);
	DDX_Control(pDX, IDC_CB_CONTENTSTYPE, m_cbContentsType);
	DDX_Control(pDX, IDC_CB_ALIGN, m_cbAlign);
	DDX_Control(pDX, IDC_EDIT_SIZE, m_editSize);
	DDX_Control(pDX, IDC_EDIT_LR_MARGIN, m_editLRMargin);
	DDX_Control(pDX, IDC_EDIT_UD_MARGIN, m_editUDMargin);
	DDX_Control(pDX, IDC_PIC_FRAME, m_picFrame);
	DDX_Control(pDX, IDC_PIC_DISAPLAY, m_picDisplay);
	DDX_Control(pDX, IDC_EDIT_CONTENTSNAME, m_editContentsName);
	DDX_Control(pDX, IDC_SPIN_WIDTH, m_spSize);
	DDX_Control(pDX, IDC_SPIN_LRMARGIN, m_spLRMargin);
	DDX_Control(pDX, IDC_SPIN_UDMARGIN, m_spUDMargin);
	DDX_Control(pDX, IDC_RADIO_HORIZONTAL, m_radioHorizontal);
	DDX_Control(pDX, IDC_RADIO_VERTICAL, m_radioVertical);
	//DDX_Control(pDX, IDC_CHECK_SIZE, m_checkSize);
	DDX_Control(pDX, IDC_RADIO_SOURCE_SIZE, m_radioSourceSize);
	DDX_Control(pDX, IDC_RADIO_ABS_SIZE, m_radioABSSize);
	DDX_Control(pDX, IDC_RADIO_FULL_SIZE, m_radioFullSize);
	DDX_Control(pDX, IDC_EDIT_WIDTH2, m_editWidth2);
	DDX_Control(pDX, IDC_SPIN_WIDTH2, m_spWidth2);
	DDX_Control(pDX, IDC_EDIT_X_POS, m_editPosX);
	DDX_Control(pDX, IDC_SPIN_X_POS, m_spPosX);
	DDX_Control(pDX, IDC_EDIT_Y_POS, m_editPosY);
	DDX_Control(pDX, IDC_SPIN_Y_POS, m_spPosY);
}

BEGIN_MESSAGE_MAP(CAnnounceDlg, CDialog)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	//ON_EN_CHANGE(IDC_EDIT_COMMENT, OnEnChangeEditComment)

	ON_NOTIFY(NM_KILLFOCUS, IDC_START_DATE, &CAnnounceDlg::OnNMKillfocusStartDate)
	ON_NOTIFY(NM_KILLFOCUS, IDC_START_TIME, &CAnnounceDlg::OnNMKillfocusStartTime)
	ON_NOTIFY(NM_KILLFOCUS, IDC_END_DATE, &CAnnounceDlg::OnNMKillfocusEndDate)
	ON_NOTIFY(NM_KILLFOCUS, IDC_END_TIME, &CAnnounceDlg::OnNMKillfocusEndTime)

	//ON_BN_CLICKED(IDC_BUTTON_FILE, &CAnnounceDlg::OnBnClickedButtonFile)
	//ON_BN_CLICKED(IDC_BUTTON_PLAY, &CAnnounceDlg::OnBnClickedButtonPlay)
	//ON_BN_CLICKED(IDC_BUTTON_STOP, &CAnnounceDlg::OnBnClickedButtonStop)

	ON_BN_CLICKED(IDC_BUTTON_SELECT_HOST, &CAnnounceDlg::OnBnClickedButtonSelectHost)
	ON_BN_CLICKED(IDC_BT_CONTENTS, &CAnnounceDlg::OnBnClickedBtContents)
	ON_CBN_SELCHANGE(IDC_CB_CONTENTSTYPE, &CAnnounceDlg::OnCbnSelchangeCbContentstype)
	ON_BN_CLICKED(IDC_RADIO_HORIZONTAL, &CAnnounceDlg::OnBnClickedRadioHorizontal)
	ON_BN_CLICKED(IDC_RADIO_VERTICAL, &CAnnounceDlg::OnBnClickedRadioVertical)
	ON_EN_CHANGE(IDC_EDIT_SIZE, &CAnnounceDlg::OnEnChangeEditSize)
	ON_CBN_SELCHANGE(IDC_CB_ALIGN, &CAnnounceDlg::OnCbnSelchangeCbAlign)
	ON_EN_CHANGE(IDC_EDIT_LR_MARGIN, &CAnnounceDlg::OnEnChangeEditLrMargin)
	ON_EN_CHANGE(IDC_EDIT_UD_MARGIN, &CAnnounceDlg::OnEnChangeEditUdMargin)
	ON_CBN_SELCHANGE(IDC_COMBO_POSITION, &CAnnounceDlg::OnCbnSelchangeComboPosition)
	ON_EN_CHANGE(IDC_EDIT_HEIGHT, &CAnnounceDlg::OnEnChangeEditHeight)
	//ON_BN_CLICKED(IDC_CHECK_SIZE, &CAnnounceDlg::OnBnClickedCheckSize)
	ON_BN_CLICKED(IDC_RADIO_SOURCE_SIZE, &CAnnounceDlg::OnBnClickedRadioSourceSize)
	ON_BN_CLICKED(IDC_RADIO_ABS_SIZE, &CAnnounceDlg::OnBnClickedRadioAbsSize)
	ON_BN_CLICKED(IDC_RADIO_FULL_SIZE, &CAnnounceDlg::OnBnClickedRadioFullSize2)
	ON_EN_CHANGE(IDC_EDIT_WIDTH2, &CAnnounceDlg::OnEnChangeEditWidth2)
	ON_EN_CHANGE(IDC_EDIT_X_POS, &CAnnounceDlg::OnEnChangeEditXPos)
	ON_EN_CHANGE(IDC_EDIT_Y_POS, &CAnnounceDlg::OnEnChangeEditYPos)
END_MESSAGE_MAP()

// CAnnounceDlg message handlers
BOOL CAnnounceDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//m_spFontSize.SetRange32(0, 100);
	//m_spAlpa.SetRange32(0, 100);
	//m_sldSpeed.SetRange(1, 5);
	//m_sldSpeed.SetPos(3);

	if(m_bReadOnly){
		GetDlgItem(IDOK)->EnableWindow(false);
	}


	m_spHeight.SetRange32(1, 1920);
	m_spWidth2.SetRange32(1,1920);

	m_spSize.SetRange32(1,100);


	m_spWidth2.SetRange32(0,1920);
	m_spPosX.SetRange32(0,1920);
	m_spPosY.SetRange32(0,1920);

	m_cbPosition.AddString("0.Bottom");
	m_cbPosition.AddString("1.Top");
	m_cbPosition.AddString("2.Middle");
 
	m_cbContentsType.AddString(LoadStringById(IDS_CONTENTSDIALOG_STR002)); //동영상  0
	m_cbContentsType.AddString(LoadStringById(IDS_CONTENTSDIALOG_STR003)); //이미지  1
	m_cbContentsType.AddString(LoadStringById(IDS_CONTENTSDIALOG_STR004)); //티커    2 
	m_cbContentsType.AddString(LoadStringById(IDS_CONTENTSDIALOG_STR005)); //플래쉬  3
	m_cbContentsType.AddString(LoadStringById(IDS_CONTENTSDIALOG_STR007)); //WEB  4
	m_cbContentsType.AddString(LoadStringById(IDS_CONTENTSDIALOG_STR008)); //PPT  5

	m_cbAlign.AddString("0.Center");
	m_cbAlign.AddString("1.Top,Left");
	m_cbAlign.AddString("2.Top,Middle");
	m_cbAlign.AddString("3.Top,Right");
	m_cbAlign.AddString("4.Middle,Left");
	m_cbAlign.AddString("5.Center");
	m_cbAlign.AddString("6.Middle,Right");
	m_cbAlign.AddString("7.Bottom,Left");
	m_cbAlign.AddString("8.Bottom,Middle");
	m_cbAlign.AddString("9.Bottom,Right");

	m_radioHorizontal.SetCheck(TRUE);

	//m_picDisplay.MoveWindow(100,330,256,144);
	//m_picFrame.MoveWindow(100+(256/2)-(256/4),330+(144/2)-(144/4),256/2,144/2);

	//m_btFile.LoadBitmap(IDB_BUTTON_OPEN, RGB(236, 233, 216));
	//m_btPlay.LoadBitmap(IDB_BUTTON_PLAY, RGB(236, 233, 216));
	//m_btStop.LoadBitmap(IDB_BUTTON_STOP, RGB(236, 233, 216));

	//m_cbFGColor.InitializeDefaultColors();
	//m_cbBGColor.InitializeDefaultColors();

	if(m_AnnoInfo.FgColor.IsEmpty())
		m_AnnoInfo.FgColor = "#FFFFFF";
	//m_cbFGColor.SetSelectedColorValue(GetColorFromString(m_AnnoInfo.FgColor));

	if(m_AnnoInfo.BgColor.IsEmpty())
		m_AnnoInfo.BgColor = "#000000";
	//m_cbBGColor.SetSelectedColorValue(GetColorFromString(m_AnnoInfo.BgColor));

	//m_cbFont.SetPreviewStyle (CFontPreviewCombo::NAME_ONLY, false);
	//m_cbFont.SetFontHeight (12, true);

	if(m_AnnoInfo.Font.IsEmpty())
		m_AnnoInfo.Font = LoadStringById(IDS_ANNOUNCEDLG_FONT01);

	//int nFindFont = -1;
	//int nSystemFont = -1;
	//for(int i=0; i<m_cbFont.GetCount(); i++)
	//{
	//	CString font_name;
	//	m_cbFont.GetLBText(i,font_name);

	//	if(font_name == LoadStringById(IDS_ANNOUNCEDLG_FONT01))
	//	{
	//		nSystemFont = i;
	//	}
	//	if(font_name == m_AnnoInfo.Font)
	//	{
	//		nFindFont = i;
	//		break;
	//	}
	//}

	//if(nFindFont >= 0) m_cbFont.SetCurSel(nFindFont);
	//else               m_cbFont.SetCurSel(nSystemFont);

	m_cbContentsType.SetCurSel(m_AnnoInfo.ContentsType);

	m_szTitle = m_AnnoInfo.Title;
	m_editContentsName.SetWindowText(m_AnnoInfo.Comment[0]);
	
	m_editHeight.SetWindowText(ToString((int)m_AnnoInfo.Height));

	int sizeBuf = m_AnnoInfo.Height;
	if(sizeBuf > 100) sizeBuf = 100;
	m_editSize.SetWindowText(ToString(sizeBuf));

	int max = 100-sizeBuf;
	m_spLRMargin.SetRange32(0,max);
	m_spUDMargin.SetRange32(0,max);

	m_cbPosition.SetCurSel(m_AnnoInfo.Position);
	m_cbAlign.SetCurSel(m_AnnoInfo.Position);

	m_editLRMargin.SetWindowText(ToString(m_AnnoInfo.LRMargin));
	m_editUDMargin.SetWindowText(ToString(m_AnnoInfo.UDMargin));

	//m_checkSize.SetCheck(m_AnnoInfo.SourceSize);
	if(m_AnnoInfo.SourceSize == 1){
		m_radioSourceSize.SetCheck(true);
	}else if(m_AnnoInfo.SourceSize == 2){
		m_radioABSSize.SetCheck(true);
	}else{
		m_radioFullSize.SetCheck(true);
	}

	m_editWidth2.SetWindowText(ToString(m_AnnoInfo.Width));
	m_editPosX.SetWindowText(ToString(m_AnnoInfo.PosX));
	m_editPosY.SetWindowText(ToString(m_AnnoInfo.PosY));

	//m_szFile = m_AnnoInfo.BgFile;
	//m_szFontSize = ToString((int)m_AnnoInfo.FontSize);
	//m_szAlpa = ToString((int)m_AnnoInfo.Alpha);

	// Package.cpp 의 코드를 그대로 가져옴
	//속도를 총 5단계로 한다.
	//빠름(1~49) >> 조금빠름(50~99) >> 보통(100~199) >> 조금느림(200~499) >> 아주느림(500~1000)
	if(m_AnnoInfo.Speed < 50)
	{
		//빠름(1~49)
		m_AnnoInfo.Speed = Mng_Browser::E_TICKER_FAST;
	}
	else if(m_AnnoInfo.Speed >= 50 && m_AnnoInfo.Speed < 100)
	{
		//조금빠름(50~99)
		m_AnnoInfo.Speed = Mng_Browser::E_TICKER_LITTLE_FAST;
	}
	else if(m_AnnoInfo.Speed >= 100 && m_AnnoInfo.Speed < 200)
	{
		//보통(100~199)
		m_AnnoInfo.Speed = Mng_Browser::E_TICKER_NORMAL;
	}
	else if(m_AnnoInfo.Speed >= 200 && m_AnnoInfo.Speed < 500)
	{
		//조금느림(200~499)
		m_AnnoInfo.Speed = Mng_Browser::E_TICKER_LITTLE_SLOW;
	}
	else if(m_AnnoInfo.Speed >= 500 && m_AnnoInfo.Speed <= 1000)
	{
		//아주느림(500~1000)
		m_AnnoInfo.Speed = Mng_Browser::E_TICKET_SLOW;
	}//if

	//m_sldSpeed.SetPos(Mng_Browser::E_TICKET_SLOW - m_AnnoInfo.Speed + 1);

	m_dtcStartTime.SetFormat("tt HH:mm:ss");
	m_dtcEndTime.SetFormat("tt HH:mm:ss");

	if(m_AnnoInfo.StartTime.GetTime()){
		m_dtcStartDate.SetTime(&m_AnnoInfo.StartTime);
		m_dtcStartTime.SetTime(&m_AnnoInfo.StartTime);
	}
	//else{
	//	SYSTEMTIME stm;
	//	GetLocalTime(&stm);
	//	CTime tmCur(stm);
	//	CTimeSpan tmSpan(0,0,10,0);

	//	tmCur += tmSpan;
	//	m_dtcStartDate.SetTime(&tmCur);
	//	m_dtcStartTime.SetTime(&tmCur);
	//}

	CTime tmStart;
	m_dtcStartDate.GetTime(tmStart);

	if(m_AnnoInfo.EndTime.GetTime()){
		m_dtcEndDate.SetTime(&m_AnnoInfo.EndTime);
		m_dtcEndTime.SetTime(&m_AnnoInfo.EndTime);
	}else{
		CTimeSpan tmSpan(1,0,0,0);

		tmStart += tmSpan;
		m_dtcEndDate.SetTime(&tmStart);
		m_dtcEndTime.SetTime(&tmStart);
	}

	InitDateRange(m_dtcEndDate);
	InitDateRange(m_dtcEndTime);

	//m_szComment.Empty();
	//for(int i = 0; i < TICKER_COUNT; i++){
	//	if(m_AnnoInfo.Comment[i].IsEmpty())
	//		continue;
	//	m_szComment += m_AnnoInfo.Comment[i];
	//	m_szComment +="\r\n";
	//}

//	if(m_AnnoInfo.BgLocation.IsEmpty()){
		m_AnnoInfo.BgLocation.Format("/Contents/Enc/%s/", STR_ANNOFOLDER);
//	}

	InitList();
	SetList();

	OnCbnSelchangeCbAlign();
	OnCbnSelchangeCbContentstype();

	if(!m_AnnoInfo.Comment[0].IsEmpty())
	{
		this->m_cbContentsType.EnableWindow(false);
	}else{
		this->m_cbContentsType.EnableWindow(true);
	}

	// KIA, HYUNDAI 고객의 경우, 임시로 컨텐츠 타입을 사용할 수 없도록 한다.
	// 
	 if(	GetEnvPtr()->m_strCustomer == _T("HYUNDAI") ||
			GetEnvPtr()->m_strCustomer == _T("KIA") ||
			GetEnvPtr()->m_strCustomer == _T("KMCL") ||
			GetEnvPtr()->m_strCustomer == _T("KMCO") ||
			GetEnvPtr()->m_strCustomer == _T("KMNL") )
	 {
		this->m_cbContentsType.EnableWindow(false);
	 
	 }

	UpdateData(FALSE);

	//CRect client_rect;
	//GetDlgItem(IDC_STATIC_PREVIEW)->GetWindowRect(client_rect);
	//ScreenToClient(client_rect);
	//client_rect.DeflateRect(1,1);

	//m_wndTicker.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	//m_wndTicker.ShowWindow(SW_SHOW);

	m_isInit = true;
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

int CAnnounceDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	return 0;
}

void CAnnounceDlg::OnDestroy()
{
	CDialog::OnDestroy();
}

void CAnnounceDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnClose();
}
/*
void CAnnounceDlg::OnEnChangeEditComment()
{
	// 10개로 제한.
	CString szContents, szBuff;
	CEdit* pContents = (CEdit*)GetDlgItem(IDC_EDIT_COMMENT);
	pContents->GetWindowText(szContents);
	DWORD dwSel = pContents->GetSel();
	int nScroll = pContents->GetScrollPos(SB_VERT);

	int i=0, pos=0;
	szBuff = _T("");
	CString szItem = szContents.Tokenize(_T("\r\n"), pos);

	while(!szItem.IsEmpty() && 10 > i++){
		szBuff += szItem;
		szItem = szContents.Tokenize(_T("\r\n"), pos);

		if(szItem.IsEmpty())
			break;
		else if(10 > i)
			szBuff += _T("\r\n");
	}

	if(10 <= i){
		pContents->SetWindowText(szBuff);
		pContents->SetSel(dwSel, nScroll);
		pContents->SetFocus();
	}
}
*/
void CAnnounceDlg::SetInfo(SAnnounceInfo& info)
{
	m_AnnoInfo = info;
}

void CAnnounceDlg::GetInfo(SAnnounceInfo& info)
{
	info = m_AnnoInfo;
}

void CAnnounceDlg::InitList()
{
	m_lscTerminal.SetExtendedStyle(m_lscTerminal.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lscTerminal.InsertColumn(eHostID, LoadStringById(IDS_ANNOUNCEDLG_LST002), LVCFMT_LEFT, 100);
	m_lscTerminal.InsertColumn(eHostName, LoadStringById(IDS_ANNOUNCEDLG_LST003), LVCFMT_LEFT, 180);

	m_lscTerminal.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CAnnounceDlg::SetList()
{
	m_lscTerminal.DeleteAllItems();

	int nRow=0;
	for(std::list<std::string>::iterator Iter = m_AnnoInfo.HostIdList.begin(); Iter != m_AnnoInfo.HostIdList.end(); Iter++)
	{
		CString strHostId = (*Iter).c_str();
		if(strHostId.IsEmpty()) continue;
		m_lscTerminal.InsertItem(nRow, "");
		m_lscTerminal.SetItemText(nRow, eHostID, strHostId);
		nRow++;
	}
	nRow=0;
	for(std::list<std::string>::iterator Iter = m_AnnoInfo.HostNameList.begin(); Iter != m_AnnoInfo.HostNameList.end(); Iter++)
	{
		CString strHostName = (*Iter).c_str();
		if(strHostName.IsEmpty()) continue;

		m_lscTerminal.SetItemText(nRow, eHostName, strHostName);
		nRow++;
	}
	TraceLog(("%d host inserted", nRow));
}

void CAnnounceDlg::OnNMKillfocusStartDate(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	CTime tmBuf;
	m_dtcStartDate.GetTime(tmBuf);
	m_dtcStartTime.SetTime(&tmBuf);
}

void CAnnounceDlg::OnNMKillfocusStartTime(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	CTime tmBuf;
	m_dtcStartTime.GetTime(tmBuf);
	m_dtcStartDate.SetTime(&tmBuf);
}

void CAnnounceDlg::OnNMKillfocusEndDate(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	CTime tmBuf;
	m_dtcEndDate.GetTime(tmBuf);
	m_dtcEndTime.SetTime(&tmBuf);
}

void CAnnounceDlg::OnNMKillfocusEndTime(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	CTime tmBuf;
	m_dtcEndTime.GetTime(tmBuf);
	m_dtcEndDate.SetTime(&tmBuf);
}

void CAnnounceDlg::OnBnClickedOk()
{
	CString szMsg;
	UpdateData();

	//if(m_szComment.IsEmpty())
	//{
	//	szMsg = LoadStringById(IDS_ANNOUNCEDLG_MSG001);
	//	MessageBox(szMsg, m_szTitle, MB_ICONINFORMATION);
	//	return;
	//}

	SYSTEMTIME stm;
	GetLocalTime(&stm);
	CTime tmCur(stm);
	CTime tmStart, tmEnd;

	m_dtcStartDate.GetTime(tmStart);
	m_dtcEndDate.GetTime(tmEnd);

	CTimeSpan tSpan = tmStart - tmCur;
	//if(m_AnnoInfo.AnnoId.IsEmpty() && tSpan.GetTotalMinutes() < 0){
	//	szMsg = "You must set the start time is later than the current time.";
	//	MessageBox(szMsg, m_szTitle, MB_ICONINFORMATION);
	//	return;
	//}

	if(tmEnd <= tmStart){
		szMsg = LoadStringById(IDS_ANNOUNCEDLG_MSG002);
		MessageBox(szMsg, m_szTitle, MB_ICONINFORMATION);
		return;
	}

	tSpan = m_AnnoInfo.StartTime - tmCur;
	CTimeSpan tSpan2 = m_AnnoInfo.EndTime - tmCur;
	if(!m_AnnoInfo.AnnoId.IsEmpty() && tSpan.GetTotalMinutes() <= 0 && tSpan2.GetTotalMinutes() > 0){
		szMsg = LoadStringById(IDS_ANNOUNCEDLG_MSG003);
		if(MessageBox(szMsg, m_szTitle, MB_ICONINFORMATION|MB_YESNO) != IDYES){
			return;
		}
	}

	if(m_lscTerminal.GetItemCount() <= 0)
	{
		szMsg = LoadStringById(IDS_ANNOUNCEDLG_MSG004);
		MessageBox(szMsg, m_szTitle, MB_ICONINFORMATION);
		return;
	}

	m_AnnoInfo.HostIdList.clear();
	m_AnnoInfo.HostNameList.clear();

	for(int nRow = 0; nRow < m_lscTerminal.GetItemCount(); nRow++)
	{
		std::string strBuf = m_lscTerminal.GetItemText(nRow, eHostID);
		m_AnnoInfo.HostIdList.push_back(strBuf);
		std::string strBuf1 = m_lscTerminal.GetItemText(nRow, eHostName);
		m_AnnoInfo.HostNameList.push_back(strBuf1);
	}

	m_AnnoInfo.Title = m_szTitle;
	//m_AnnoInfo.BgFile = m_szFile;
	//m_AnnoInfo.Height = m_szHeight.IsEmpty()?100:atoi(m_szHeight);
	if(m_AnnoInfo.Title.IsEmpty()){
		szMsg = LoadStringById(IDS_ANNOUNCEDLG_MSG005);
		MessageBox(szMsg, m_szTitle, MB_ICONINFORMATION);
		return;
	}


	CString buf;

	m_editHeight.GetWindowText(buf);
	int nHeight=atoi(buf);
	m_AnnoInfo.Height = ((nHeight==0)?100:nHeight);

	//m_AnnoInfo.FontSize = m_szFontSize.IsEmpty()?24:atoi(m_szFontSize);
	//m_AnnoInfo.Speed = Mng_Browser::E_TICKET_SLOW - m_sldSpeed.GetPos() + 1;
	//m_AnnoInfo.Alpha = m_szAlpa.IsEmpty()?0:atoi(m_szAlpa);
	m_AnnoInfo.Position = m_cbPosition.GetCurSel();

	//m_AnnoInfo.FgColor = GetColorFromString(m_cbFGColor.GetSelectedColorValue());
	//m_AnnoInfo.BgColor = GetColorFromString(m_cbBGColor.GetSelectedColorValue());
	//m_cbFont.GetLBText(m_cbFont.GetCurSel(), m_AnnoInfo.Font);

	m_dtcStartTime.GetTime(m_AnnoInfo.StartTime);
	m_dtcEndTime.GetTime(m_AnnoInfo.EndTime);

	//for(int i = 0; i < TICKER_COUNT; i++){
	//	m_AnnoInfo.Comment[i].Empty();
	//}

	//int iStart = 0, i = 0;
	//CString szToken = m_szComment.Tokenize("\r\n", iStart);
	//while(!szToken.IsEmpty()){
	//	m_AnnoInfo.Comment[i++] = szToken;
	//	szToken = m_szComment.Tokenize("\r\n", iStart);
	//}
	if(m_radioSourceSize.GetCheck()){
		m_AnnoInfo.SourceSize = 1;
	}else if (m_radioABSSize.GetCheck()){
		m_AnnoInfo.SourceSize = 2;
	}else{
		m_AnnoInfo.SourceSize = 0;
	}

	m_AnnoInfo.ContentsType = m_cbContentsType.GetCurSel();

	if(m_AnnoInfo.ContentsType == e_TIKER){
		m_editHeight.GetWindowText(buf);
		m_AnnoInfo.Height = atoi(buf);
		m_AnnoInfo.Position = m_cbPosition.GetCurSel();
	}else{
		if(m_AnnoInfo.SourceSize==1){
			m_editSize.GetWindowText(buf);
			m_AnnoInfo.Height = atoi(buf);
			m_AnnoInfo.Position = m_cbAlign.GetCurSel();
		}else{
			m_editHeight.GetWindowText(buf);
			m_AnnoInfo.Height = atoi(buf);
		}
		if(m_AnnoInfo.ContentsType != e_WEB){ // web 은 BGFile 을 보여주지 않는다.
			m_AnnoInfo.Comment[0] = m_AnnoInfo.BgFile ;  // Comment1 에 file명을 넣어놓으므로, list 에 화일명을 볼수있다.
		}
	}

	m_editLRMargin.GetWindowText(buf);
	m_AnnoInfo.LRMargin = atoi(buf);

	m_editUDMargin.GetWindowText(buf);
	m_AnnoInfo.UDMargin = atoi(buf);

	m_editWidth2.GetWindowText(buf);
	m_AnnoInfo.Width = atoi(buf);
	
	m_editPosX.GetWindowText(buf);
	m_AnnoInfo.PosX = atoi(buf);
	
	m_editPosY.GetWindowText(buf);
	m_AnnoInfo.PosY = atoi(buf);

	OnOK();
}
/*
void CAnnounceDlg::OnBnClickedButtonFile()
{
	HKEY hKey;
	DWORD dwType = REG_SZ;
	DWORD dwSize = 256;
	CString strFullpath;
	char szPersonalPath[256] = { 0x00 };
	CString strRootKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders";
	LONG lResult = RegOpenKeyEx(HKEY_CURRENT_USER, strRootKey, 0, KEY_READ, &hKey);
	
	if(lResult == ERROR_SUCCESS){
		lResult = RegQueryValueEx(hKey, "My Pictures", NULL, &dwType, (LPBYTE)&szPersonalPath, &dwSize);
		if(lResult == ERROR_SUCCESS){
			strFullpath = szPersonalPath;
			strFullpath.Append("\\Image Files");
		}else{
			strFullpath = "C:\\Image Files";
		}
	}else{
		strFullpath = "C:\\Image Files";
	}

	CString filter = "All Image Files (*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff)|*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff|All Files (*.*)|*.*||";
	
	CFileDialog dlg(TRUE, NULL, strFullpath, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, filter, this);
	if(dlg.DoModal() == IDOK)
	{
		m_szFile = dlg.GetFileName();
		CString szSPathName = dlg.GetPathName();
		CString szTPathName;
		
		szTPathName.Format("%s\\%s%s\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER);
		::CreateDirectory(szTPathName, NULL);

		szTPathName += m_szFile;
		::CopyFile(szSPathName, szTPathName, false);

		GetDlgItem(IDC_EDIT_FILE)->SetWindowText(m_szFile);

		m_imBack.Destroy();

		CFileStatus fs;
		if(CFile::GetStatus(szTPathName, fs)){
			if(m_imBack.Load(szTPathName)){
				int width = m_imBack.GetWidth();
				int height = m_imBack.GetHeight();
			}
		}

		m_wndTicker.CloseFile();
		if(m_szFile.IsEmpty())
			m_wndTicker.m_strMediaFullPath = "";
		else
			m_wndTicker.m_strMediaFullPath.Format("%s\\%s%s\\%s", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER, m_szFile);
		m_wndTicker.Invalidate();
	}
}
*/
/*
void CAnnounceDlg::OnBnClickedButtonPlay()
{
	InitPreview();
	EnableAllItems(FALSE);
	m_wndTicker.Play();
}

void CAnnounceDlg::OnBnClickedButtonStop()
{
	EnableAllItems(TRUE);
	m_wndTicker.Stop(false);
}
*/
/*
void CAnnounceDlg::InitPreview()
{
	UpdateData();
	m_wndTicker.CloseFile();

	if(m_szFile.IsEmpty())
		m_wndTicker.m_strMediaFullPath = "";
	else
		m_wndTicker.m_strMediaFullPath.Format("%s\\%s%s\\%s", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER, m_szFile);

	CString str_font;
	m_cbFont.GetLBText(m_cbFont.GetCurSel(), str_font);

	COLORREF text_color = m_cbFGColor.GetSelectedColorValue();
	COLORREF bg_color = m_cbBGColor.GetSelectedColorValue();

	m_wndTicker.m_font = str_font;
	m_wndTicker.m_fontSize = m_szFontSize.IsEmpty()?1:atoi(m_szFontSize);
	m_wndTicker.m_fgColor = ::GetColorFromString(text_color);
	m_wndTicker.m_bgColor = ::GetColorFromString(bg_color);
	m_wndTicker.m_rgbFgColor = text_color;
	m_wndTicker.m_rgbBgColor = bg_color;
	m_wndTicker.m_playSpeed = Mng_Browser::E_TICKET_SLOW - m_sldSpeed.GetPos() + 1;

	int i, pos;
	for(i=0; i < TICKER_COUNT; i++){
		m_wndTicker.m_comment[i] = _T("");
	}

	i=0; pos=0;

	CString szContents = m_szComment;
	CString szItem = szContents.Tokenize(_T("\r\n"), pos);

	while(!szItem.IsEmpty()){
		m_wndTicker.m_comment[i++] = szItem;
		szItem = szContents.Tokenize(_T("\r\n"), pos);

		if(TICKER_COUNT <= i)	break;
	}

	m_wndTicker.CreateFile();
	m_wndTicker.OpenFile(0);
}
*/
void CAnnounceDlg::EnableAllItems(BOOL bEnable)
{
	GetDlgItem(IDC_EDIT_TITLE)->EnableWindow(bEnable);
	//GetDlgItem(IDC_EDIT_FILE)->EnableWindow(bEnable);
	m_editHeight.EnableWindow(bEnable);
	//GetDlgItem(IDC_EDIT_FONTSIZE)->EnableWindow(bEnable);
	//GetDlgItem(IDC_EDIT_ALPA)->EnableWindow(bEnable);
	//GetDlgItem(IDC_EDIT_COMMENT)->EnableWindow(bEnable);
	if(m_bReadOnly){
		GetDlgItem(IDOK)->EnableWindow(false);
	}else{
		GetDlgItem(IDOK)->EnableWindow(bEnable);
	}
	GetDlgItem(IDCANCEL)->EnableWindow(bEnable);

	m_dtcStartDate.EnableWindow(bEnable);
	m_dtcStartTime.EnableWindow(bEnable);
	m_dtcEndDate.EnableWindow(bEnable);
	m_dtcEndTime.EnableWindow(bEnable);
	m_cbPosition.EnableWindow(bEnable);
//	m_cbFont.EnableWindow(bEnable);
//	m_cbFGColor.EnableWindow(bEnable);
//	m_cbBGColor.EnableWindow(bEnable);
//	m_sldSpeed.EnableWindow(bEnable);
//	m_spFontSize.EnableWindow(bEnable);
	m_spHeight.EnableWindow(bEnable);
//	m_spAlpa.EnableWindow(bEnable);
	m_lscTerminal.EnableWindow(bEnable);

//	m_btFile.EnableWindow(bEnable);
//	m_btPlay.EnableWindow(bEnable);
//	m_btStop.EnableWindow(bEnable);
}

void CAnnounceDlg::OnBnClickedButtonSelectHost()
{
	CMultiSelectHost dlg(GetEnvPtr()->m_strCustomer);

	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	for(int i=0; i<m_lscTerminal.GetItemCount() ;i++)
	{
		dlg.m_astrSelHostList.Add( m_lscTerminal.GetItemText(i,eHostID) );
		dlg.m_astrSelHostNameList.Add( m_lscTerminal.GetItemText(i,eHostName) );
	}

	if(dlg.DoModal() != IDOK) return;

	m_AnnoInfo.HostIdList.clear();
	m_AnnoInfo.HostNameList.clear();

	int nRow=0;
	for(int i=0; i<dlg.m_astrSelHostList.GetCount() ;i++)
	{
		std::string strBuf = dlg.m_astrSelHostList[i];
		m_AnnoInfo.HostIdList.push_back(strBuf);
		nRow++;
	}
	for(int i=0; i<dlg.m_astrSelHostNameList.GetCount() ;i++)
	{
		std::string strBuf = dlg.m_astrSelHostNameList[i];
		m_AnnoInfo.HostNameList.push_back(strBuf);
	}

	TraceLog(("%d row selected from MultiSelectHost", nRow));

	SetList();
}

void CAnnounceDlg::OnBnClickedBtContents()
{
	UpdateData();

	CONTENTS_INFO info;
	AnnounceToContents(info);

	CContentsDialog dlg;
	dlg.SetContentsInfo(info);
	//dlg.SetDocument(m_pDocument);
	//dlg.SetPreviewMode(info->bIsPublic);

	if(dlg.DoModal() == IDOK)
	{
		dlg.GetContentsInfo(info);
		ContentsToAnnounce(info);
		if(!m_AnnoInfo.BgFile.IsEmpty()){
			TraceLog(("m_editContentsName=%s", m_AnnoInfo.BgFile));
			m_editContentsName.SetWindowText(m_AnnoInfo.BgFile);
			if(m_AnnoInfo.FileSize==0){
				CString localFullPath;
				localFullPath.Format("%s\\%s%s\\%s", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER, m_AnnoInfo.BgFile);
				ULONGLONG localFileSize=0;
				CEnviroment::GetFileSize(localFullPath, localFileSize);
				m_AnnoInfo.FileSize = localFileSize;
			}
		}else{
			TraceLog(("m_editContentsName=%s", m_AnnoInfo.Comment[0]));
			m_editContentsName.SetWindowText(m_AnnoInfo.Comment[0]);
		}
		UpdateData(TRUE);
	}
}

void CAnnounceDlg::AnnounceToContents(CONTENTS_INFO& info)
{
	// contentsType
	info.nContentsType = Mng_Browser::CONTENTS_TICKER;
	int contentsType = m_cbContentsType.GetCurSel();
	switch(contentsType){
		case e_VIDEO:  info.nContentsType = Mng_Browser::CONTENTS_VIDEO;break;
		case e_IMAGE:  info.nContentsType = Mng_Browser::CONTENTS_IMAGE;break;
		case e_TIKER:  info.nContentsType = Mng_Browser::CONTENTS_SMS;break;
		case e_FLASH:  info.nContentsType = Mng_Browser::CONTENTS_FLASH;break;
		case e_WEB:		info.nContentsType = Mng_Browser::CONTENTS_WEBBROWSER;break;
		case e_PPT:		info.nContentsType = Mng_Browser::CONTENTS_PPT;break;
	}

	if(!m_AnnoInfo.BgFile.IsEmpty()){
		info.strContentsName = m_AnnoInfo.BgFile;
	}else if(!this->m_szTitle.IsEmpty()) {
		info.strContentsName = this->m_szTitle;
	}
	info.nFontSize			= m_AnnoInfo.FontSize;			// ticker
	info.strFont			= m_AnnoInfo.Font;				// ticker
	info.nHeight			= m_AnnoInfo.Height;			// ticker
	info.nPlaySpeed			= m_AnnoInfo.Speed;				// ticker
	info.strBgColor			= m_AnnoInfo.BgColor;			// ticker
	info.strFgColor			= m_AnnoInfo.FgColor;			// ticker
	for(int i=0;i<10;i++){							
		info.strComment[i]	= m_AnnoInfo.Comment[i];		// ticker
	}

	info.strFilename		= m_AnnoInfo.BgFile;			//
	//info.strLocalLocation	= m_AnnoInfo.BgLocation;
	info.strLocalLocation.Format("%s\\%s%s\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER);

	info.nSoundVolume		= m_AnnoInfo.SoundVolume;
	info.nFilesize			= m_AnnoInfo.FileSize;
	info.strFileMD5			= m_AnnoInfo.FileMD5;
	info.strId				= m_AnnoInfo.AnnoId;
	info.strSiteId			= m_AnnoInfo.SiteId;

	if(contentsType==e_FLASH || contentsType==e_WEB || contentsType==e_PPT){
		// 플래쉬의 경우 comment[1] 에 runningTime 이 들어있다.
		info.nRunningTime		= strtoul(m_AnnoInfo.Comment[1],NULL,10);
	}
	info.nAlpha = m_AnnoInfo.Alpha;
}

void CAnnounceDlg::ContentsToAnnounce(CONTENTS_INFO& info)
{

	m_AnnoInfo.ContentsType = Mng_Browser::CONTENTS_TICKER;
	switch(info.nContentsType){
		case  Mng_Browser::CONTENTS_VIDEO:		m_AnnoInfo.ContentsType =e_VIDEO;break;
		case  Mng_Browser::CONTENTS_IMAGE:		m_AnnoInfo.ContentsType =e_IMAGE;break;
		case  Mng_Browser::CONTENTS_SMS:		m_AnnoInfo.ContentsType =e_TIKER;break;
		case  Mng_Browser::CONTENTS_FLASH:		m_AnnoInfo.ContentsType =e_FLASH;break;
		case  Mng_Browser::CONTENTS_WEBBROWSER:		m_AnnoInfo.ContentsType =e_WEB;break;
		case  Mng_Browser::CONTENTS_PPT:		m_AnnoInfo.ContentsType =e_PPT;break;
	}

	m_AnnoInfo.FontSize		=	info.nFontSize;				// ticker
	m_AnnoInfo.Font			=	info.strFont;				// ticker
	m_AnnoInfo.Height		=	info.nHeight;				// ticker
	m_AnnoInfo.Speed		=	info.nPlaySpeed;			// ticker
	m_AnnoInfo.BgColor		=	info.strBgColor;			// ticker
	m_AnnoInfo.FgColor		=	info.strFgColor;			// ticker
	m_AnnoInfo.BgFile		=	info.strFilename;
	for(int i=0;i<10;i++){									
		 m_AnnoInfo.Comment[i] = info.strComment[i];		// ticker
	}

	m_AnnoInfo.BgFile		=	info.strFilename;
	//m_AnnoInfo.BgLocation	=	info.strLocalLocation; 
	m_AnnoInfo.SoundVolume	=	info.nSoundVolume;
	m_AnnoInfo.FileSize		=	info.nFilesize;
	m_AnnoInfo.FileMD5			=	info.strFileMD5;
	m_AnnoInfo.IsFileChanged	=	info.bIsFileChanged;
	if(!info.strId.IsEmpty()){
		m_AnnoInfo.AnnoId = info.strId;
	}
	m_AnnoInfo.SiteId = info.strSiteId;
	if(info.nWidth && info.nHeight){
		m_AnnoInfo.HeightRatio = float(info.nHeight)/float(info.nWidth);
	}

	if(m_AnnoInfo.ContentsType==e_FLASH || m_AnnoInfo.ContentsType==e_WEB || m_AnnoInfo.ContentsType==e_PPT ){
		// 플래쉬의 경우 comment[1] 에 runningTime 이 들어있다.
		m_AnnoInfo.Comment[1].Format("%u", info.nRunningTime);
	}
	m_AnnoInfo.Alpha = info.nAlpha;
}


void CAnnounceDlg::OnCbnSelchangeCbContentstype()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	bool isTicker = false;
	if(m_cbContentsType.GetCurSel() == e_TIKER) {
		isTicker = true;
	}

	if(m_cbContentsType.GetCurSel() == e_PPT ) {  // 플래쉬도 절대크기 지정만 할 수 있다.
		m_radioSourceSize.EnableWindow(false);
		m_radioABSSize.EnableWindow(false);
		m_radioFullSize.EnableWindow(true);
	}else if(m_cbContentsType.GetCurSel() == e_FLASH || m_cbContentsType.GetCurSel() == e_WEB) {  // 플래쉬도 절대크기 지정만 할 수 있다.
		m_radioSourceSize.EnableWindow(false);
		m_radioABSSize.EnableWindow(true);
		m_radioFullSize.EnableWindow(true);
	}else{
		m_radioSourceSize.EnableWindow(!isTicker);
		m_radioABSSize.EnableWindow(!isTicker);
		m_radioFullSize.EnableWindow(!isTicker);
	}

	if((m_cbContentsType.GetCurSel()==e_FLASH || m_cbContentsType.GetCurSel() == e_WEB)  && m_radioSourceSize.GetCheck() ){
		m_radioABSSize.SetCheck(true);
		m_radioFullSize.SetCheck(false);
		m_radioSourceSize.SetCheck(false);
	}else if (m_cbContentsType.GetCurSel() == e_PPT ) {
		m_radioABSSize.SetCheck(false);
		m_radioFullSize.SetCheck(true);
		m_radioSourceSize.SetCheck(false);
	}

	bool isSource = true;
	if(m_radioSourceSize.GetCheck() == false){
		isSource = false;
	}

	bool isFull = false;
	if(m_radioFullSize.GetCheck() == true){
		isFull=true;
	}



	m_cbPosition.ShowWindow(isTicker && !isFull);
	GetDlgItem(IDC_STATIC4)->ShowWindow(isTicker && !isFull);

	m_editHeight.ShowWindow(isTicker || !isSource && !isFull);
	GetDlgItem(IDC_SPIN_HEIGHT)->ShowWindow(isTicker || !isSource && !isFull);
	GetDlgItem(IDC_STATIC5)->ShowWindow(isTicker || !isSource && !isFull);

	GetDlgItem(IDC_STATIC6)->ShowWindow(!isTicker && isSource && !isFull);
	GetDlgItem(IDC_STATIC7)->ShowWindow(!isTicker && isSource && !isFull);
	GetDlgItem(IDC_STATIC8)->ShowWindow(!isTicker && isSource && !isFull);
	GetDlgItem(IDC_STATIC9)->ShowWindow(!isTicker && isSource && !isFull);
	

	m_cbAlign.ShowWindow(!isTicker && isSource && !isFull);
	m_editSize.ShowWindow(!isTicker && isSource && !isFull);
	//m_checkSize.ShowWindow(!isTicker && isSource && !isFull);
	m_editLRMargin.ShowWindow(!isTicker && isSource && !isFull);
	m_editUDMargin.ShowWindow(!isTicker&& isSource && !isFull);
	m_spSize.ShowWindow(!isTicker && isSource && !isFull);
	m_spLRMargin.ShowWindow(!isTicker && isSource && !isFull);
	m_spUDMargin.ShowWindow(!isTicker && isSource && !isFull);


	GetDlgItem(IDC_STATIC10)->ShowWindow(!isTicker && !isSource && !isFull);
	GetDlgItem(IDC_STATIC11)->ShowWindow(!isTicker && !isSource && !isFull);
	GetDlgItem(IDC_STATIC12)->ShowWindow(!isTicker && !isSource && !isFull);

	m_editWidth2.ShowWindow(!isTicker && !isSource && !isFull);
	m_editPosX.ShowWindow(!isTicker && !isSource && !isFull);
	m_editPosY.ShowWindow(!isTicker && !isSource && !isFull);
	m_spWidth2.ShowWindow(!isTicker && !isSource && !isFull);
	m_spPosX.ShowWindow(!isTicker && !isSource && !isFull);
	m_spPosY.ShowWindow(!isTicker && !isSource && !isFull);


	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}


//	UpdateData();
}

void CAnnounceDlg::OnBnClickedRadioHorizontal()
{
	int x = 100;
	int y = 345;
	int width = 256;
	int height = 144;

	if(m_cbContentsType.GetCurSel() == e_TIKER) {
		RedrawTickerExample(x,y,width,height);
	}else{
		if(this->m_radioSourceSize.GetCheck()){
			RedrawExample(x,y,width,height);
		}else if(this->m_radioABSSize.GetCheck()){
			RedrawABSExample(x,y,width,height);
		}else{
			RedrawFullScreenExample(x,y,width,height);
		}
	}
}

void CAnnounceDlg::OnBnClickedRadioVertical()
{
	int x = 230;
	int y = 265;
	int width = 144;
	int height = 256;

	if(m_cbContentsType.GetCurSel() == e_TIKER) {
		RedrawTickerExample(x,y,width,height);
	}else{
		if(this->m_radioSourceSize.GetCheck()){
			RedrawExample(x,y,width,height);
		}else if(this->m_radioABSSize.GetCheck()){
			RedrawABSExample(x,y,width,height);
		}else{
			RedrawFullScreenExample(x,y,width,height);
		}
	}
}

void CAnnounceDlg::RedrawExample(int x, int y, int width, int height)
{
	CString buf;
	m_editSize.GetWindowText(buf);
	int fsize = atoi(buf);

	int maxMargin = ((100-fsize));

	m_editLRMargin.GetWindowText(buf);
	int lr = atoi(buf);
	if(lr>maxMargin) {
		lr = maxMargin;
	}
	if(lr<0){
		lr = 0;
	}
	lr = (width*lr)/100;  // 백분률로 환산

	m_editUDMargin.GetWindowText(buf);
	int ud = atoi(buf);
	if(ud>maxMargin) {
		ud = maxMargin;
	}
	if(ud<0) {
		ud = 0;
	}
	ud = (height*ud)/100; // 백분률로 환산
	
	int align = m_cbAlign.GetCurSel();

	TraceLog(("align=%d, fsize=%d, lr=%d, ud=%d", align, fsize, lr, ud));

	float	fwidth;
	float	fheight;

	if(width >= height){
		if(m_AnnoInfo.HeightRatio >= (9.0/16.0) ) {  // 세로가 비교적 길다. ex 10/16
			fheight = height;
			fwidth = fheight*(1.0/m_AnnoInfo.HeightRatio);
			TraceLog(("1HeightRatio=%f, fwidth=%f, fheight=%f", m_AnnoInfo.HeightRatio, fwidth, fheight));
		}else{    // ex 5/16 가로가 비교적 길다. 
			fwidth =  width;
			fheight = float(fwidth)*m_AnnoInfo.HeightRatio;
			TraceLog(("2HeightRatio=%f, fwidth=%f, fheight=%f", m_AnnoInfo.HeightRatio, fwidth, fheight));
		}
	}else{
		if(m_AnnoInfo.HeightRatio >= (16.0/9.0) ) {  // 세로가 비교적 길다. ex 20/9
			fheight = height;
			fwidth = fheight*(1.0/m_AnnoInfo.HeightRatio);
			TraceLog(("3HeightRatio=%f, fwidth=%f, fheight=%f", m_AnnoInfo.HeightRatio, fwidth, fheight));
		}else{    // ex 2/9 가로가 비교적 길다. 
			fwidth =  width;
			fheight = fwidth*m_AnnoInfo.HeightRatio;
			TraceLog(("4HeightRatio=%f, fwidth=%f, fheight=%f", m_AnnoInfo.HeightRatio, fwidth, fheight));
		}
	}
	fheight = fheight*(float(fsize)/100.0);
	fwidth  = fwidth*(float(fsize)/100.0);
	TraceLog(("5HeightRatio=%f, fwidth=%f, fheight=%f", m_AnnoInfo.HeightRatio, fwidth, fheight));

	int fx =	int(x+(width/2) - (fwidth/2));
	int fy = 	int(y+(height/2) - (fheight/2));

	switch(align){
		case 0: //center 
			break;  
		case 1: //Top-Left
			TraceLog(("Top-Left"));
			fx = x+lr;
			fy = y+ud;
			break;
		case 2: //Top-Middle
			fy = y+ud;
			break;
		case 3: //TOP-Right
			fx = x+width-fwidth-lr;
			fy = y+ud;
			break;
		case 4: //Middle-Left
			fx = x+lr;
			break;
		case 5:	//center
			break;  
		case 6: //Middle-Right
			fx = x+width-fwidth-lr;
			break;
		case 7: //Bottom-Left
			fx = x+lr;
			fy = y+height-fheight-ud;
			break;
		case 8: //Bottom-Middle
			fy = y+height-fheight-ud;
			break;
		case 9: //Bottom-Right;
			fx = x+width-fwidth-lr;
			fy = y+height-fheight-ud;
			break;
		default: break; //center
	}

/*
	CRect rect;
	m_picFrame.GetWindowRect(rect);

	int fx = rect.left;
	int fy = rect.top;
	int fwidth =  rect.right - rect.left;
	int fheight = rect.bottom - rect.top;
*/

	m_picFrame.MoveWindow(fx,fy,int(fwidth),int(fheight));
	m_picDisplay.MoveWindow(x,y,width,height);

	this->RedrawWindow();
}

void CAnnounceDlg::RedrawABSExample(int x, int y, int width, int height)
{
	float height_ratio = height/((width >= height) ? 1080.0 : 1920.0);
	float width_ratio = width/((width >= height) ? 1920.0 : 1080.0);

	TraceLog(("Height_Ratio=%f, Width_Ration=%f", height_ratio,width_ratio));

	CString buf;
	
	m_editHeight.GetWindowText(buf);
	float fheight = atof(buf)*height_ratio ;

	m_editWidth2.GetWindowText(buf);
	float fwidth = atof(buf)*width_ratio;

	m_editPosX.GetWindowText(buf);
	int fx = (atof(buf)*width_ratio) + x;

	m_editPosY.GetWindowText(buf);
	int fy = (atof(buf)*height_ratio) + y;

	TraceLog(("fheight=%f, fwidth=%f, fx=%d, fy=%d", fheight, fwidth, fx, fy));

	m_picFrame.MoveWindow(fx,fy,int(fwidth),int(fheight));
	m_picDisplay.MoveWindow(x,y,width,height);

	this->RedrawWindow();
}
void CAnnounceDlg::RedrawFullScreenExample(int x, int y, int width, int height)
{
	TraceLog(("RedrawFullScreenExample %d,%d,%d,%d", x, y, width,height));

	m_picFrame.MoveWindow(x,y,width,height);
	m_picDisplay.MoveWindow(x,y,width,height);

	this->RedrawWindow();
}

void CAnnounceDlg::RedrawTickerExample(int x, int y, int width, int height)
{
	int align = m_cbPosition.GetCurSel();

	int fwidth =  width-4;
	int fheight = 25;

	int fx =	x+2;
	int fy = 	y;

	TraceLog(("align=%d, fheight=%d", align, fheight));

	switch(align){
		case 0: //bottom 
			fy = y + height - fheight - 2;
			break;  
		case 1: //Top
			fy = y + 2;
			break;
		case 2: //Middle
			fy = y+(height/2)-(fheight/2);
			break;
		default: break; //center
	}

	m_picFrame.MoveWindow(fx,fy,fwidth,fheight);
	m_picDisplay.MoveWindow(x,y,width,height);

	this->RedrawWindow();
}


void CAnnounceDlg::OnEnChangeEditSize()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(m_isInit == false) {
		return;
	}

	CString buf;
	m_editSize.GetWindowText(buf);
	int fsize = atoi(buf);

	int max = (100-fsize);
	m_spLRMargin.SetRange32(0,max);
	m_spUDMargin.SetRange32(0,max);

	m_editLRMargin.GetWindowText(buf);
	int lr = atoi(buf);
	if(lr>max){
		CString strMax;
		strMax.Format("%d", max);	
		m_editLRMargin.SetWindowText(strMax);
	}
	m_editUDMargin.GetWindowText(buf);
	int ud = atoi(buf);
	if(ud>max){
		CString strMax;
		strMax.Format("%d", max);	
		m_editUDMargin.SetWindowText(strMax);
	}


	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}
}


void CAnnounceDlg::OnCbnSelchangeCbAlign()
{
	bool enable = true;
	int align = m_cbAlign.GetCurSel();
	if(align == 0 || align == 5) { // Center 인 경우는 마진이 의미 없다
		m_editLRMargin.EnableWindow(false);
		m_editUDMargin.EnableWindow(false);
		m_spLRMargin.EnableWindow(false);
		m_spUDMargin.EnableWindow(false);
		GetDlgItem(IDC_STATIC8)->EnableWindow(false);
		GetDlgItem(IDC_STATIC9)->EnableWindow(false);
	}else if(align == 2 || align == 8){  // 좌우마진이 의미 없다.
		m_editLRMargin.EnableWindow(false);
		m_editUDMargin.EnableWindow(true);
		m_spLRMargin.EnableWindow(false);
		m_spUDMargin.EnableWindow(true);
		GetDlgItem(IDC_STATIC8)->EnableWindow(false);
		GetDlgItem(IDC_STATIC9)->EnableWindow(true);
	}else if(align == 4 || align == 6){  // 상하마진이 의미 없다.
		m_editLRMargin.EnableWindow(true);
		m_editUDMargin.EnableWindow(false);
		m_spLRMargin.EnableWindow(true);
		m_spUDMargin.EnableWindow(false);
		GetDlgItem(IDC_STATIC8)->EnableWindow(true);
		GetDlgItem(IDC_STATIC9)->EnableWindow(false);
	}else{
		m_editLRMargin.EnableWindow(true);
		m_editUDMargin.EnableWindow(true);
		m_spLRMargin.EnableWindow(true);
		m_spUDMargin.EnableWindow(true);
		GetDlgItem(IDC_STATIC8)->EnableWindow(true);
		GetDlgItem(IDC_STATIC9)->EnableWindow(true);
	}
	if(m_isInit == false) {
		return;
	}

	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}
}

void CAnnounceDlg::OnEnChangeEditLrMargin()
{
	if(m_isInit == false) {
		return;
	}

	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}
}

void CAnnounceDlg::OnEnChangeEditUdMargin()
{
	if(m_isInit == false) {
		return;
	}

	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}
}

void CAnnounceDlg::OnCbnSelchangeComboPosition()
{
	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}
}

void CAnnounceDlg::OnEnChangeEditHeight()
{
	if(m_cbContentsType.GetCurSel() == e_TIKER) {
		return;
	}
	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}

}

//void CAnnounceDlg::OnBnClickedCheckSize()
//{
//	int useContentsSize = m_checkSize.GetCheck();
//	m_editSize.EnableWindow(useContentsSize);
//}

void CAnnounceDlg::OnBnClickedRadioSourceSize()
{
	if(m_cbContentsType.GetCurSel() == e_TIKER) {
		return;
	}
	bool isSource=true;

	GetDlgItem(IDC_STATIC6)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC7)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC8)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC9)->ShowWindow(isSource);

	m_cbAlign.ShowWindow(isSource);
	m_editSize.ShowWindow(isSource);
	//m_checkSize.ShowWindow(isSource);
	m_editLRMargin.ShowWindow(isSource);
	m_editUDMargin.ShowWindow(isSource);
	m_spSize.ShowWindow(isSource);
	m_spLRMargin.ShowWindow(isSource);
	m_spUDMargin.ShowWindow(isSource);

	GetDlgItem(IDC_STATIC5)->ShowWindow(!isSource);
	GetDlgItem(IDC_STATIC10)->ShowWindow(!isSource);
	GetDlgItem(IDC_STATIC11)->ShowWindow(!isSource);
	GetDlgItem(IDC_STATIC12)->ShowWindow(!isSource);

	m_editHeight.ShowWindow(!isSource);
	GetDlgItem(IDC_SPIN_HEIGHT)->ShowWindow(!isSource);
	m_editWidth2.ShowWindow(!isSource);
	m_editPosX.ShowWindow(!isSource);
	m_editPosY.ShowWindow(!isSource);
	m_spWidth2.ShowWindow(!isSource);
	m_spPosX.ShowWindow(!isSource);
	m_spPosY.ShowWindow(!isSource);

	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}


}

void CAnnounceDlg::OnBnClickedRadioAbsSize()
{
	if(m_cbContentsType.GetCurSel() == e_TIKER) {
		return;
	}

	bool isSource=false;

	GetDlgItem(IDC_STATIC6)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC7)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC8)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC9)->ShowWindow(isSource);

	m_cbAlign.ShowWindow(isSource);
	m_editSize.ShowWindow(isSource);
	//m_checkSize.ShowWindow(isSource);
	m_editLRMargin.ShowWindow(isSource);
	m_editUDMargin.ShowWindow(isSource);
	m_spSize.ShowWindow(isSource);
	m_spLRMargin.ShowWindow(isSource);
	m_spUDMargin.ShowWindow(isSource);

	GetDlgItem(IDC_STATIC5)->ShowWindow(!isSource);
	GetDlgItem(IDC_STATIC10)->ShowWindow(!isSource);
	GetDlgItem(IDC_STATIC11)->ShowWindow(!isSource);
	GetDlgItem(IDC_STATIC12)->ShowWindow(!isSource);

	m_editHeight.ShowWindow(!isSource);
	GetDlgItem(IDC_SPIN_HEIGHT)->ShowWindow(!isSource);
	m_editWidth2.ShowWindow(!isSource);
	m_editPosX.ShowWindow(!isSource);
	m_editPosY.ShowWindow(!isSource);
	m_spWidth2.ShowWindow(!isSource);
	m_spPosX.ShowWindow(!isSource);
	m_spPosY.ShowWindow(!isSource);

	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}

}
void CAnnounceDlg::OnBnClickedRadioFullSize2()
{
	if(m_cbContentsType.GetCurSel() == e_TIKER) {
		return;
	}

	bool isSource=false;

	GetDlgItem(IDC_STATIC6)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC7)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC8)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC9)->ShowWindow(isSource);

	m_cbAlign.ShowWindow(isSource);
	m_editSize.ShowWindow(isSource);
	//m_checkSize.ShowWindow(isSource);
	m_editLRMargin.ShowWindow(isSource);
	m_editUDMargin.ShowWindow(isSource);
	m_spSize.ShowWindow(isSource);
	m_spLRMargin.ShowWindow(isSource);
	m_spUDMargin.ShowWindow(isSource);

	GetDlgItem(IDC_STATIC5)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC10)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC11)->ShowWindow(isSource);
	GetDlgItem(IDC_STATIC12)->ShowWindow(isSource);

	m_editHeight.ShowWindow(isSource);
	GetDlgItem(IDC_SPIN_HEIGHT)->ShowWindow(isSource);
	m_editWidth2.ShowWindow(isSource);
	m_editPosX.ShowWindow(isSource);
	m_editPosY.ShowWindow(isSource);
	m_spWidth2.ShowWindow(isSource);
	m_spPosX.ShowWindow(isSource);
	m_spPosY.ShowWindow(isSource);

	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}
}


void CAnnounceDlg::OnEnChangeEditWidth2()
{
	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}
}

void CAnnounceDlg::OnEnChangeEditXPos()
{
	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}
}

void CAnnounceDlg::OnEnChangeEditYPos()
{
	if(m_radioVertical.GetCheck()){
		OnBnClickedRadioVertical();
	}else{
		OnBnClickedRadioHorizontal();
	}
}

