// LogExcelSave.h: interface for the CLogExcelSave class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOGEXCELSAVE_H__0ACC6CC4_173A_425F_A8D2_142A3965D8B0__INCLUDED_)
#define AFX_LOGEXCELSAVE_H__0ACC6CC4_173A_425F_A8D2_142A3965D8B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "Excel/Excel.h"


class CLogExcelSave  
{
public:
	CLogExcelSave();
	virtual ~CLogExcelSave();

	CString	Save(CString strSheetName, CListCtrl& lcList);
private:
	_Application m_excelApp;	// m_excelApp is the Excel _Application object
	_Workbook   m_book;

	BOOL	SaveDataList(short nSheet, CString strSheetName, CListCtrl& lcList);
};

#endif // !defined(AFX_LOGEXCELSAVE_H__0ACC6CC4_173A_425F_A8D2_142A3965D8B0__INCLUDED_)
