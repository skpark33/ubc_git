#pragma once
#include "afxwin.h"
#include "common\HoverButton.h"


// CBroadcastPlanNameInput 대화 상자입니다.

class CBroadcastPlanNameInput : public CDialog
{
	DECLARE_DYNAMIC(CBroadcastPlanNameInput)

public:
	enum DLG_MODE { eModeCreate, eModeSaveAs } ;

	CBroadcastPlanNameInput(DLG_MODE nMode, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBroadcastPlanNameInput();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_BROADCAST_PLAN_NAME_INPUT };

	// Output
	CString	m_strBroadcastPlanName;

protected:
	DLG_MODE m_nMode;

	CHoverButton m_bnDupCheck;
	CHoverButton m_bnOK      ;
	CHoverButton m_bnCancel  ;

	bool	InputErrorCheck();

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedBnDupCheck();
};
