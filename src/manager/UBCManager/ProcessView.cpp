// ProcessView.cpp : implementation file
//
#include "stdafx.h"
#include "UBCManager.h"
#include "ProcessView.h"
#include "Enviroment.h"
#include "HostView.h"
#include "HostScreenView.h"
#include "MainFrm.h"

#include "common\ubcdefine.h"
//#include "ubccopcommon\HostInfoDlg.h"
#include "ubccopcommon\AuthCheckDlg.h"
#include "common\libCommon\ubcMuxRegacy.h"
#include "common\libscratch\scratchUtil.h"
#include "common\libcommon\ubchost.h"
#include "common\libinstall\installutil.h"
#include "common\libcommon\utvUtil.h"
#include "common\UbcCode.h"
#include "ubccopcommon/SiteSelectDlg.h"
#include "ubccopcommon/PackageSelectDlg.h"
#include "ubccopcommon/ContentsSelectDlg.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "IPInputDLG.h"
#include "RemoteLoginSettingsDlg.h"

#define STR_ADMIN_ON		LoadStringById(IDS_PROCESSVIEW_LIST001)
#define STR_ADMIN_OFF		LoadStringById(IDS_PROCESSVIEW_LIST002)
#define STR_OPERA_ON		LoadStringById(IDS_PROCESSVIEW_LIST003)
#define STR_OPERA_OFF		LoadStringById(IDS_PROCESSVIEW_LIST004)

#define STR_HOST_SITE_NAME		LoadStringById(IDS_PROCESSVIEW_LIST005)
#define STR_HOST_ID				LoadStringById(IDS_PROCESSVIEW_LIST006)
#define STR_HOST_NAME			LoadStringById(IDS_PROCESSVIEW_LIST007)
#define STR_HOST_DISCNT			LoadStringById(IDS_PROCESSVIEW_LIST008)
#define STR_HOST_VNCSTAT		LoadStringById(IDS_PROCESSVIEW_LIST009)
#define STR_HOST_STARTER		LoadStringById(IDS_PROCESSVIEW_LIST010)
#define STR_HOST_BROWSER		LoadStringById(IDS_PROCESSVIEW_LIST011)
#define STR_HOST_PREDNLOAD		LoadStringById(IDS_PROCESSVIEW_LIST012)
#define STR_HOST_FIRMVIEW		LoadStringById(IDS_PROCESSVIEW_LIST013)
#define STR_HOST_TAG			LoadStringById(IDS_PROCESSVIEW_LIST014)
#define STR_HOST_MONITOR_STAT	LoadStringById(IDS_PROCESSVIEW_LIST015)
#define STR_HOST_TYPE   		LoadStringById(IDS_PROCESSVIEW_LIST016)

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)

// CProcessView
IMPLEMENT_DYNCREATE(CProcessView, CFormView)

CProcessView::CProcessView() : CFormView(CProcessView::IDD)
,	m_Reposition(this)
{
	m_pBackupHostList = NULL;
}

CProcessView::~CProcessView()
{
	DeleteBackupHostList();
}

void CProcessView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HOST, m_lscList);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_Filter);

	DDX_Control(pDX, IDC_BUTTON_HOST_REF, m_btnRefHost);
	DDX_Control(pDX, IDC_BUTTON_HOST_RESTART, m_btnRestart);

	DDX_Control(pDX, IDC_COMBO_FILTER_ADMIN, m_cbAdmin);
	DDX_Control(pDX, IDC_COMBO_FILTER_OPR, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOSTTYPE, m_cbHostType);
	DDX_Control(pDX, IDC_COMBO_FILTER_TAG, m_cbFilterTag);
}

BEGIN_MESSAGE_MAP(CProcessView, CFormView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()

	ON_COMMAND(ID_HOSTLIST_REMOTELOGIN, OnRemotelogin)
	ON_COMMAND(ID_HOSTLIST_REMOTELOGIN2, OnRemotelogin2) // 0001442: 원격접속 방법에 서버를 경유하지 않는 vnc원격접속을 만든다.
	ON_COMMAND(ID_HOSTLIST_REMOTELOGIN3, OnRemotelogin3) // 원격접속 방법에 지정된 서버를 경유하는 vnc원격접속을 만든다.
	ON_COMMAND(ID_HOSTLIST_REMOTE_DESKTOP, OnRemoteDesktop)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_HOST, OnNMRclickListHost)

	ON_BN_CLICKED(IDC_BUTTON_HOST_REF, OnBnClickedButtonHostrfs)
	ON_BN_CLICKED(IDC_BUTTON_HOST_RESTART, OnBnClickedButtonHostRestart)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_SITE, OnBnClickedButtonFilterSite)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_CONTENTS, OnBnClickedButtonFilterContents)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PACKAGE, OnBnClickedButtonFilterPackage)
	ON_REGISTERED_MESSAGE(WM_FILTER_HOST_CHANGE, OnFilterHostChanged)
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
	ON_COMMAND(ID_REMOTE_LOGIN_SETTINGS, &CProcessView::OnRemoteLoginSettings)
END_MESSAGE_MAP()

// CProcessView diagnostics
#ifdef _DEBUG
void CProcessView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CProcessView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CProcessView message handlers
int CProcessView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	GetClientRect(&m_rcClient);
//	GetEnvPtr()->InitSite();

	return 0;
}

void CProcessView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_PROCESSVIEW_STR001));

	CodeItemList listHostType;
	CUbcCode::GetInstance()->GetCategoryInfo(_T("HostType"), listHostType);

	POSITION pos = listHostType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listHostType.GetNext(pos);
		m_cbHostType.AddString(info.strEnumString, info.nEnumNumber);
	}

	CRect rc;
	m_cbHostType.GetWindowRect(&rc);
	m_cbHostType.SetDroppedWidth(rc.Width() * 1.5);

	m_cbAdmin.AddString("All");
	m_cbAdmin.AddString(STR_ADMIN_ON);
	m_cbAdmin.AddString(STR_ADMIN_OFF);
	m_cbAdmin.SetCurSel(0);

	m_cbOperation.AddString("All");
	m_cbOperation.AddString(STR_OPERA_ON);
	m_cbOperation.AddString(STR_OPERA_OFF);
	m_cbOperation.SetCurSel(0);

	CStringArray aTagList;
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_btnRefHost.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnRestart.LoadBitmap(IDB_BUTTON_RSTBRW, RGB(255,255,255));

	m_btnRefHost.SetToolTipText(LoadStringById(IDS_PROCESSVIEW_BTN001));
	m_btnRestart.SetToolTipText(LoadStringById(IDS_PROCESSVIEW_BTN002));

	// 권한에 따라 활성화 한다.
	m_btnRefHost.EnableWindow(IsAuth(_T("MQRY")));
	m_btnRestart.EnableWindow(IsAuth(_T("HBRS")));

	LoadFilterData();

	UpdateData(FALSE);

	InitPosition(m_rcClient);

	InitHostList();

//	GetEnvPtr()->InitHost(m_lsHost, NULL, NULL);
//	m_lscList.SetSortCol(m_lscList.GetColPos(m_szHostColum[eHostID]));

//	RefreshHost(false);

	SetScrollSizes(MM_TEXT, CSize(1,1));

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscList.LoadColumnState("PROCESS-LIST", szPath);
}

void CProcessView::OnDestroy()
{
	CFormView::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscList.SaveColumnState("PROCESS-LIST", szPath);
}

void CProcessView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscList, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_Filter, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
}

void CProcessView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_Reposition.Move();
}

void CProcessView::InitHostList()
{
	m_szHostColum[eCheck            ] = _T("");	// 2010.02.17 by gwangsoo
	m_szHostColum[eSiteName         ] = STR_HOST_SITE_NAME;
	m_szHostColum[eHostVNCStat      ] = STR_HOST_VNCSTAT;
	m_szHostColum[eHostName         ] = STR_HOST_NAME;
	m_szHostColum[eHostID           ] = STR_HOST_ID;
	m_szHostColum[eHostDisCnt       ] = STR_HOST_DISCNT;
	m_szHostColum[eHostStarter      ] = STR_HOST_STARTER;
	m_szHostColum[eHostBrowser      ] = STR_HOST_BROWSER;
	m_szHostColum[eHostPreDownloader] = STR_HOST_PREDNLOAD;
	m_szHostColum[eHostFirmware     ] = STR_HOST_FIRMVIEW;
	m_szHostColum[eTag              ] = STR_HOST_TAG;
	m_szHostColum[eMonitorStat      ] = STR_HOST_MONITOR_STAT;
	m_szHostColum[eHostType         ] = STR_HOST_TYPE;

	m_lscList.SetExtendedStyle(m_lscList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);

	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilHostList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilHostList.Add(&bmCheck, RGB(255, 255, 255));
	m_lscList.SetImageList(&m_ilHostList, LVSIL_SMALL);

	for(int nCol = 0; nCol < eHostEnd; nCol++)
	{
		m_lscList.InsertColumn(nCol, m_szHostColum[nCol], LVCFMT_LEFT, 100);
	}

	m_lscList.SetColumnWidth(m_lscList.GetColPos(m_szHostColum[eCheck]), 22); // 2010.02.17 by gwangsoo
	m_lscList.SetColumnWidth(m_lscList.GetColPos(m_szHostColum[eHostDisCnt]), 20);
	m_lscList.SetColumnWidth(m_lscList.GetColPos(m_szHostColum[eHostVNCStat]), 15);
	m_lscList.SetColumnWidth(m_lscList.GetColPos(m_szHostColum[eMonitorStat]), 15);

	m_lscList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CProcessView::RefreshHostList()
{
	int nRow = 0;
	LVITEM item;
	CString szBuf;
	POSITION pos = m_lsHost.GetHeadPosition();
	m_lscList.DeleteAllItems();

	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsHost.GetNext(pos);

		m_lscList.InsertItem(nRow, _T(""), -1);
	
		UpdateListRow(nRow, &info);
		m_lscList.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lscList.GetCurSortCol();
	if(-1 != nCol)
		m_lscList.SortList(nCol, m_lscList.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscList);
	else
		m_lscList.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscList);
}

// 단말 리스트에서 오른쪽 버튼 클릭시
void CProcessView::OnNMRclickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!IsAuth(_T("HVNC"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
	if(!pos) return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);

	pPopup = menu.GetSubMenu(2);
//	pPopup->EnableMenuItem(ID_HOSTLIST_REMOTELOGIN, Info.vncState);
	pPopup->EnableMenuItem(ID_HOSTLIST_REMOTELOGIN, (IsAuth(_T("HVNC")) ?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_HOSTLIST_REMOTE_DESKTOP, (IsAuth(_T("HVNC")) ?MF_ENABLED : MF_DISABLED | MF_GRAYED));

	CString strOsName = Info.os;
	strOsName.Trim();
	strOsName.MakeLower();
	if(!strOsName.IsEmpty() && strOsName.Find(_T("win")) < 0)
	{
		pPopup->DeleteMenu(ID_HOSTLIST_REMOTE_DESKTOP, MF_BYCOMMAND);
	}

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CProcessView::OnBnClickedButtonHostrfs()
{
	DeleteBackupHostList();
	RefreshHost();

	SaveFilterData();

	// 같은 필터를 사용하는 윈도우에 통지
	((CMainFrame*)AfxGetMainWnd())->UpdateFilterData(this);
}

BOOL CProcessView::RefreshHost(bool bCompMsg)
{
	CWaitMessageBox wait;

	CString szSite = (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite);

	UpdateData();

	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);

	CString strWhere;

	// 단말이름
	strHostName.Trim();
	if(!strHostName.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostName like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostName
							);
	}

	// 단말ID
	strHostId.Trim();
	if(!strHostId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostId like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostId
							);
	}

	// 단말타입
	if(!strHostType.IsEmpty())
	{
		strHostType.Replace(",", "','");
		strWhere.AppendFormat(_T(" %s hostType in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostType
							);
	}

	// 소속조직
	if(!strSiteId.IsEmpty())
	{
		strSiteId.Replace(_T(","), _T("','"));
		strWhere.AppendFormat(_T(" %s siteId in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strSiteId
							);
	}

	// 포함콘텐츠
	if(!strContentsId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 in (select programId from ubc_contents where lower(contentsId) = lower('%s'))"
								      " or lastSchedule2 in (select programId from ubc_contents where lower(contentsId) = lower('%s')))")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strContentsId
							, strContentsId
							);
	}

	// 방송중인 패키지
	if(!strPackageId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 = '%s' or lastSchedule2 = '%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strPackageId
							, strPackageId
							);
	}

	// 연결상태
	if(_ttoi(strOperaStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s operationalState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strOperaStat)==1 ? 1:0)
							);
	}

	// 사용상태
	if(_ttoi(strAdminStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s adminState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strAdminStat)==1 ? 1:0)
							);
	}

	// 검색어
	strCategory.Trim();
	if(!strCategory.IsEmpty())
	{
		int pos = 0;
		CString strTag;
		while((strTag = strCategory.Tokenize("/", pos)) != _T(""))
		{
			//strWhere.AppendFormat(_T(" %s concat('/', category, '/') like '%%/%s/%%'")
			strWhere.AppendFormat(_T(" %s category like '%%%s%%'")
							 	 , (strWhere.IsEmpty() ? _T("") : _T("and"))
								 , strTag.MakeLower()
								 );
		}
	}

	//BOOL bRet = GetEnvPtr()->RefreshHost(m_lsHost, szSite, (strWhere.IsEmpty() ? NULL : strWhere));	
	m_lsHost.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetHostForProcess(&aCall, szSite, "*", (strWhere.IsEmpty() ? NULL : strWhere));
	if(bRet)
	{
		bRet = CCopModule::GetObject()->GetHostData(&aCall, m_lsHost);
	}

	RefreshHostList();

	if(!bRet){
		UbcMessageBox(LoadStringById(IDS_PROCESSVIEW_MSG001), MB_ICONINFORMATION);
		return FALSE;
	}

	if(bCompMsg)
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_PROCESSVIEW_MSG002), m_lscList.GetItemCount());
		UbcMessageBox(strMsg, MB_ICONINFORMATION);
	}

	return bRet;
}

void CProcessView::OnBnClickedButtonHostRestart()
{
	CList<int> lsRow;
	for(int nRow = m_lscList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscList.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_PROCESSVIEW_MSG003), MB_ICONINFORMATION);
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_PROCESSVIEW_MSG004), MB_ICONINFORMATION|MB_YESNO) != IDYES){
		return;
	}

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow){
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscList.GetItemData(nRow);
		if(!posHost)	continue;

		m_lscList.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);
		CCopModule::GetObject()->Restart(Info.siteId, Info.hostId);
	}
}

LRESULT CProcessView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case IEH_OPSTAT:
// cli >>> post PM=2/Site=SQI/Host=SQI-06112 operationalStateChanged siteId="SQI", hostId="SQI-06116", operationalState=true
		return ChangeOPStat((SOpStat*)lParam);

	case IEH_ADSTAT:
		return ChangeAdStat((SAdStat*)lParam);

	case IEH_CHNGVNC:
		return ChangeVncStat((cciEvent*)lParam);

	case IEH_CHNGPRS:
		return ChangeProcessState((SProcessStateChange*)lParam);

		// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	case IEH_MOSTAT:
		return ChangeMonitorStat((SMonitorStat*)lParam);

	case IEH_UNKOWN:
		break;
	}
	return 0;
}

int CProcessView::ChangeOPStat(SOpStat* pOpStat)
{
	for(int nRow = 0; nRow < m_lscList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pOpStat->siteId || Info.hostId != pOpStat->hostId)
			continue;

		TraceLog(("operationalState : %s", pOpStat->operationalState?"true":"false"));
		Info.operationalState = pOpStat->operationalState;
		m_lsHost.GetAt(pos) = Info;

		m_lscList.Invalidate();

		return 0;
	}

	return 0;
}

int CProcessView::ChangeAdStat(SAdStat* pAdStat)
{
	for(int nRow = 0; nRow < m_lscList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pAdStat->siteId || Info.hostId != pAdStat->hostId)
			continue;

		Info.adminState = pAdStat->adminState;
		m_lsHost.GetAt(pos) = Info;

		LVITEM item;
		item.iItem = nRow;
		item.iSubItem = m_lscList.GetColPos(m_szHostColum[eSiteName]);
		item.mask = LVIF_IMAGE;
		item.iImage = (Info.adminState ? eMonitorOn : eMonitorOff);
		m_lscList.SetItem(&item);
		return 0;
	}

	return 0;
}

int CProcessView::ChangeVncStat(cciEvent* ev)
{
	ciString hostId;
	ciBoolean vncState;

	ev->getItem("hostId", hostId);
	ev->getItem("vncState", vncState);

	for(int nRow = 0; nRow < m_lscList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo info = m_lsHost.GetAt(pos);
		if(info.hostId != hostId.c_str()) continue;

		m_lsHost.GetAt(pos).vncState = vncState;

		LVITEM item;
		item.iItem = nRow;
		item.iSubItem = m_lscList.GetColPos(m_szHostColum[eHostVNCStat]);
		item.mask = LVIF_IMAGE|LVIF_TEXT;
		if(vncState){
			item.iImage = eVncOn;
			item.pszText = "";
		}else{
			item.iImage = eVncOff;
			item.pszText = "";
		}
		m_lscList.SetItem(&item);

		break;
	}

	return 0;
}

int CProcessView::ChangeProcessState(SProcessStateChange* pProcessStateChange)
{
	for(int nRow = 0; nRow < m_lscList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo info = m_lsHost.GetAt(pos);

		if(info.siteId != pProcessStateChange->siteId || info.hostId != pProcessStateChange->hostId)
			continue;

		info.starterState = pProcessStateChange->starterState;
		info.browserState = pProcessStateChange->browserState;
		info.browser1State = pProcessStateChange->browser1State;
		info.firmwareViewState = pProcessStateChange->firmwareViewState;
		info.preDownloaderState = pProcessStateChange->preDownloaderState;

		UpdateListRow(nRow, &info);

		return 0;
	}

	return 0;
}

void CProcessView::OnRemotelogin()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscList.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscList.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // 창일향 : 원격 로그인은 텔넷으로 대체한다. (기존 UKIPA version 처럼)
	{
		CString szParam = " ";
		szParam.Format(" %s 2233", Info.ipAddress);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
	else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
	{
/*
		ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(Info.siteId);
		if(!aData)	return;

		CString szPath;
#ifdef _DEBUG
		szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
		szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif
		TraceLog((szPath));

		int nPos = Info.hostId.Find(_T("-"));
		CString strId = (nPos < 0 ? _T("") : Info.hostId.Mid(nPos+1));

		CString szParam = " /proxy ";
		szParam += aData->ipAddress.c_str();
		szParam += ":5901 ID:";
		//szParam += ToString(Info.vncPort);
		szParam += strId;

		HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);
*/
		CMainFrame::RemoteLogin_viaServer(Info.siteId, Info.hostId);
	}
	else if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType){
		CString szPath = "";
		CString szParam = " ";
		szParam += Info.ipAddress;

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, szPath, SW_SHOWNORMAL);
	}
}

void CProcessView::OnRemotelogin2()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscList.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscList.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // 창일향 : 원격 로그인은 텔넷으로 대체한다. (기존 UKIPA version 처럼)
	{
		CString szParam = " ";
		szParam.Format(" %s 2233", Info.ipAddress);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
	else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
	{
/*
		CString szPath;
#ifdef _DEBUG
		szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
		szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif
		TraceLog((szPath));

		CString szParam;
		szParam.Format(_T("%s:%d"), Info.ipAddress, Info.vncPort);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);
*/
		CMainFrame::RemoteLogin_Direct(Info.ipAddress, Info.vncPort);
	}
	else if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType){
		CString szPath = "";
		CString szParam = " ";
		szParam += Info.ipAddress;

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, szPath, SW_SHOWNORMAL);
	}
}

void CProcessView::OnRemotelogin3()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscList.GetItemData(m_ptSelList.y);
//	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	
	if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // 창일향 : 원격 로그인은 텔넷으로 대체한다. (기존 UKIPA version 처럼)
	{
		CString szParam = " ";
		szParam.Format(" %s 2233", Info.ipAddress);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
	else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
	{
/*
		ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(Info.siteId);
		if(!aData) return;
		TraceLog((aData->ipAddress.c_str()));

		CIPInputDLG		aDlg;

		aDlg.setIpAddress(aData->ipAddress.c_str());

		if(aDlg.DoModal() == IDOK){

			CString repeaterIp = aDlg.getIpAddress();

			if(repeaterIp.IsEmpty()){
				return;
			}

			CString szPath;
	#ifdef _DEBUG
			szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
	#else
			szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
	#endif
			TraceLog((szPath));

			int nPos = Info.hostId.Find(_T("-"));
			CString strId = (nPos < 0 ? _T("") : Info.hostId.Mid(nPos+1));

			CString szParam = " /proxy ";
			szParam += repeaterIp;
			szParam += ":5901 ID:";
			//szParam += ToString(Info.vncPort);
			szParam += strId;

			HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);

		}
*/
		CMainFrame::RemoteLogin_viaSpecified(Info.siteId, Info.hostId);
	}
}

void CProcessView::OnRemoteDesktop()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lscList.GetItemData(m_ptSelList.y);
	if(!pos) return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	CString szParam;
	szParam.Format(_T(" /v %s"), Info.ipAddress);

	HINSTANCE nRet = ShellExecute(NULL, NULL, "mstsc.exe", szParam, "", SW_SHOWNORMAL);
}

void CProcessView::UpdateListRow(int nRow, SHostInfo* pInfo)
{
	if(!pInfo || m_lscList.GetItemCount() < nRow)
		return;

	// 0000707: 콘텐츠 다운로드 상태 조회 기능
    COLORREF crTextBk = COLOR_WHITE;
	if(IsJustNowHost(pInfo->hostId))
	{
		crTextBk = COLOR_LILAC;
	}
	else if(pInfo->operationalState)
	{
		crTextBk = COLOR_YELLOW;
	}
	else
	{
		crTextBk = COLOR_GRAY;
	}
	m_lscList.SetRowColor(nRow, crTextBk, m_lscList.GetTextColor());

	bool browserState = false;
	if(pInfo->displayCounter == 1 && pInfo->browserState) browserState = true;
	if(pInfo->displayCounter == 2 && pInfo->browserState && pInfo->browser1State) browserState = true;

	m_lscList.SetText(nRow, m_szHostColum[eSiteName         ], pInfo->siteName);
	m_lscList.SetText(nRow, m_szHostColum[eHostID           ], pInfo->hostId);
	m_lscList.SetText(nRow, m_szHostColum[eHostName         ], pInfo->hostName);
	m_lscList.SetText(nRow, m_szHostColum[eHostType         ], CUbcCode::GetInstance()->GetCodeName(_T("HostType"), pInfo->hostType));
	m_lscList.SetText(nRow, m_szHostColum[eHostDisCnt       ], ToString(pInfo->displayCounter));
	m_lscList.SetText(nRow, m_szHostColum[eHostStarter      ], (pInfo->starterState ? _T("O") : _T("X")));
	m_lscList.SetText(nRow, m_szHostColum[eHostBrowser      ], (browserState ? _T("O") : _T("X")));
	m_lscList.SetText(nRow, m_szHostColum[eHostPreDownloader], (pInfo->preDownloaderState ? _T("O") : _T("X")));
	m_lscList.SetText(nRow, m_szHostColum[eHostFirmware     ], (pInfo->firmwareViewState ? _T("O") : _T("X")));
	//CString strCategory;
	//GetDlgItemText(IDC_EDIT_FILTER_TAG, strCategory);
	//m_lscList.SetText(nRow, m_szHostColum[eTag              ], (strCategory.IsEmpty() ? pInfo->category : strCategory));
	m_lscList.SetText(nRow, m_szHostColum[eTag              ], pInfo->category);

	// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	//bool bMonitorStat = utvUtil::getInstance()->monitorState(pInfo->monitorOff, pInfo->monitorOffList);
	int nMonitorImgIdx = 2;
	if(pInfo->operationalState)
	{
		if(pInfo->monitorState > 0) nMonitorImgIdx = eMonitorOn;
		if(pInfo->monitorState == 0) nMonitorImgIdx = eMonitorOff;
		if(pInfo->monitorState < 0) nMonitorImgIdx = eMonitorUnknown;
	}

	LVITEM itemMonitorStat;
	itemMonitorStat.iItem = nRow;
	itemMonitorStat.iSubItem = eMonitorStat;//m_lscList.GetColPos(m_szHostColum[eMonitorStat]);
	itemMonitorStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemMonitorStat.pszText = (nMonitorImgIdx == eMonitorUnknown ? _T("Unknown") : (nMonitorImgIdx == eMonitorOn ? _T("On") : _T("Off")));
	itemMonitorStat.iImage = nMonitorImgIdx;
	m_lscList.SetItem(&itemMonitorStat);

	LVITEM itemHostVNCStat;
	itemHostVNCStat.iItem = nRow;
	itemHostVNCStat.iSubItem = m_lscList.GetColPos(m_szHostColum[eHostVNCStat]);
	itemHostVNCStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemHostVNCStat.pszText = (pInfo->vncState || pInfo->operationalState ? _T("On") : _T("Off"));
	itemHostVNCStat.iImage = (pInfo->vncState || pInfo->operationalState ? eVncOn : eVncOff);
	m_lscList.SetItem(&itemHostVNCStat);

	//LVITEM item;
	//item.iItem = nRow;
	//item.iSubItem = m_lscList.GetColPos(m_szHostColum[eHostStarter]);
	//item.mask = LVIF_IMAGE;
	//item.iImage = (pInfo->starterState ? eOn : eOff);
	//m_lscList.SetItem(&item);

	//item.iSubItem = m_lscList.GetColPos(m_szHostColum[eHostBrowser]);
	//item.mask = LVIF_IMAGE;
	//item.iImage = (browserState ? eOn : eOff);
	//m_lscList.SetItem(&item);

	//item.iSubItem = m_lscList.GetColPos(m_szHostColum[eHostPreDownloader]);
	//item.mask = LVIF_IMAGE;
	//item.iImage = (pInfo->preDownloaderState ? eOn : eOff);
	//m_lscList.SetItem(&item);

	//item.iSubItem = m_lscList.GetColPos(m_szHostColum[eHostFirmware]);
	//item.mask = LVIF_IMAGE;
	//item.iImage = (pInfo->firmwareViewState ? eOn : eOff);
	//m_lscList.SetItem(&item);
}

void CProcessView::BackupHostList()
{
	DeleteBackupHostList();

	m_pBackupHostList = new CMapStringToString();

	for(int nRow = 0; nRow < m_lscList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo info = m_lsHost.GetAt(pos);
		m_pBackupHostList->SetAt(info.hostId, info.hostId);
	}
}

void CProcessView::DeleteBackupHostList()
{
	if(m_pBackupHostList) delete m_pBackupHostList;
	m_pBackupHostList = NULL;
}

bool CProcessView::IsJustNowHost(CString strHostId)
{
	if(!m_pBackupHostList) return false;

	CString strValue;
	m_pBackupHostList->Lookup(strHostId, strValue);

	return strValue.IsEmpty();
}

void CProcessView::OnBnClickedButtonFilterSite()
{
	CString strSiteName;
	CString strSiteId;

	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
	if(dlg.DoModal() == IDOK)
	{
		SiteInfoList siteInfoList;
		if(dlg.GetSelectSiteInfoList(siteInfoList))
		{
			POSITION pos = siteInfoList.GetHeadPosition();
			while(pos)
			{
				SSiteInfo info = siteInfoList.GetNext(pos);
				strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
				strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
			}
		}
	}

	SetDlgItemText(IDC_EDIT_FILTER_SITENAME, strSiteName);
	m_strSiteId = strSiteId;
}

void CProcessView::OnBnClickedButtonFilterContents()
{
	CContentsSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, _T(""));
		m_strContentsId = _T("");
		return;
	}

	CONTENTS_INFO_EX_LIST& list = dlg.GetSelectedContentsIdList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		CONTENTS_INFO_EX& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, info.strContentsName);
		m_strContentsId = info.strContentsId;
	}
}

void CProcessView::OnBnClickedButtonFilterPackage()
{
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		m_strPackageId = _T("");
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, info.szPackage);
		m_strPackageId = info.szPackage;
	}
}

void CProcessView::SaveFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	WritePrivateProfileString("HOST-FILTER", "HostName"         , strHostName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostID"           , strHostId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostType"         , strHostType     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteName"         , strSiteName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteID"           , strSiteId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContents"  , strContentsName , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContentsID", strContentsId   , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackage"   , strPackageName  , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackageID" , strPackageId    , szPath);
	WritePrivateProfileString("HOST-FILTER", "AdminStat"        , strAdminStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "OperaStat"        , strOperaStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "Category"         , strCategory     , szPath);
}

void CProcessView::LoadFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("HOST-FILTER", "HostName"         , "", buf, 1024, szPath); strHostName     = buf;
	GetPrivateProfileString("HOST-FILTER", "HostID"           , "", buf, 1024, szPath); strHostId       = buf;
	GetPrivateProfileString("HOST-FILTER", "HostType"         , "", buf, 1024, szPath); strHostType     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteName"         , "", buf, 1024, szPath); strSiteName     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteID"           , "", buf, 1024, szPath); strSiteId       = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContents"  , "", buf, 1024, szPath); strContentsName = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContentsID", "", buf, 1024, szPath); strContentsId   = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackage"   , "", buf, 1024, szPath); strPackageName  = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackageID" , "", buf, 1024, szPath); strPackageId    = buf;
	GetPrivateProfileString("HOST-FILTER", "AdminStat"        , "", buf, 1024, szPath); strAdminStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "OperaStat"        , "", buf, 1024, szPath); strOperaStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "Category"         , "", buf, 1024, szPath); strCategory     = buf;

	SetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	SetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	if(strHostType.IsEmpty())
	{
		m_cbHostType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strHostType.Tokenize(",", pos)) != _T(""))
		{
			m_cbHostType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}
	SetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	m_strSiteId = strSiteId;
	SetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	m_strContentsId = strContentsId;
	SetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageId;
	m_cbAdmin.SetCurSel(_ttoi(strAdminStat));
	m_cbOperation.SetCurSel(_ttoi(strOperaStat));
	//SetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.SetWindowText(strCategory);
}

LRESULT CProcessView::OnFilterHostChanged(WPARAM wParam, LPARAM lParam)
{
	LoadFilterData();
	UpdateData(FALSE);
	return 0;
}

// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
int CProcessView::ChangeMonitorStat(SMonitorStat* pMonitorStat)
{
	for(int nRow = 0; nRow < m_lscList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pMonitorStat->siteId || Info.hostId != pMonitorStat->hostId)
			continue;

		Info.monitorState = pMonitorStat->monitorState;
		m_lsHost.GetAt(pos) = Info;

		UpdateListRow(nRow, &Info);

		//LVITEM item;
		//item.iItem = nRow;
		//item.iSubItem = m_lscList.GetColPos(m_szHostColum[eSiteName]);
		//item.mask = LVIF_IMAGE;
		//item.iImage = (Info.adminState ? eMonitorOn : eMonitorOff);
		//m_lscList.SetItem(&item);
//		m_lscList.Invalidate();
		return 0;
	}

	return 0;
}

void CProcessView::OnRemoteLoginSettings()
{
	CRemoteLoginSettingsDlg dlg;
	dlg.DoModal();
}
