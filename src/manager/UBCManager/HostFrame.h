#pragma once

#include "ubccopcommon\eventhandler.h"

// CHostFrame frame

class CHostFrame : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CHostFrame)
protected:
	CHostFrame();           // protected constructor used by dynamic creation
	virtual ~CHostFrame();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
