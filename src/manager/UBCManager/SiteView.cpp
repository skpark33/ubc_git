// SiteView.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "SiteView.h"
#include "Enviroment.h"
#include "SiteDelDlg.h"

#include "ubccopcommon\SiteInfoDlg.h"
#include "ubccopcommon\HostInfoDlg.h"
#include "ubccopcommon\AuthCheckDlg.h"

#include "common\libCommon\ubcSite.h"

#include <ci/libBase/cilisttype.h>
#include <cci/libValue/ccistringlist.h>

#define STR_SITE_ID			LoadStringById(IDS_SITEVIEW_LST001)
#define STR_SITE_NAME		LoadStringById(IDS_SITEVIEW_LST002)
#define STR_SITE_OPEN		LoadStringById(IDS_SITEVIEW_LST003)
#define STR_SITE_CLOSE		LoadStringById(IDS_SITEVIEW_LST004)
//#define STR_SITE_HOLI		LoadStringById(IDS_SITEVIEW_LST005)

// CSiteView
IMPLEMENT_DYNCREATE(CSiteView, CFormView)

CSiteView::CSiteView() : CFormView(CSiteView::IDD)
,	m_Reposition(this)
{
	m_bInit = false;
	m_szMsg.Empty();
}

CSiteView::~CSiteView()
{
}

void CSiteView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SITE, m_lscSite);
	DDX_Control(pDX, IDC_BUTTON_SITE_ADD, m_btnAddSite);
	DDX_Control(pDX, IDC_BUTTON_SITE_DEL, m_btnDelSite);
	DDX_Control(pDX, IDC_BUTTON_SITE_MOD, m_btnModSite);
	DDX_Control(pDX, IDC_BUTTON_SITE_REF, m_btnRefSite);
}

BEGIN_MESSAGE_MAP(CSiteView, CFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()

	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_SITE, OnLvnColumnclickListSite)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_SITE, OnNMCustomdrawListSite)
	ON_NOTIFY(NM_CLICK, IDC_LIST_SITE, OnNMClickListSite)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SITE, OnNMDblclkListSite)

	ON_BN_CLICKED(IDC_BUTTON_SITE_ADD, OnBnClickedButtonSiteadd)
	ON_BN_CLICKED(IDC_BUTTON_SITE_DEL, OnBnClickedButtonSitedel)
	ON_BN_CLICKED(IDC_BUTTON_SITE_MOD, OnBnClickedButtonSitemod)
	ON_BN_CLICKED(IDC_BUTTON_SITE_REF, OnBnClickedButtonSiterfs)
END_MESSAGE_MAP()

// CSiteView diagnostics
#ifdef _DEBUG
void CSiteView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSiteView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CSiteView message handlers
int CSiteView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	GetClientRect(&m_rcClient);
	GetEnvPtr()->InitSite();

	return 0;
}

void CSiteView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO: Add your specialized code here and/or call the base class
	if(m_bInit)	return;
	m_bInit = true;

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_SITEVIEW_STR001));

	m_btnAddSite.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btnDelSite.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btnModSite.LoadBitmap(IDB_BUTTON_MODIFY, RGB(255,255,255));
	m_btnRefSite.LoadBitmap(IDB_BUTTON_REFRESH2, RGB(255,255,255));

	m_btnAddSite.SetToolTipText(LoadStringById(IDS_SITEVIEW_STR002));
	m_btnDelSite.SetToolTipText(LoadStringById(IDS_SITEVIEW_STR003));
	m_btnModSite.SetToolTipText(LoadStringById(IDS_SITEVIEW_STR004));
	m_btnRefSite.SetToolTipText(LoadStringById(IDS_SITEVIEW_STR005));

	InitPosition(m_rcClient);
	InitSiteList();
	RefreshSiteList();

	SetScrollSizes(MM_TEXT, CSize(1,1));
}

void CSiteView::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CFormView::OnClose();
}

void CSiteView::OnDestroy()
{
	CFormView::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscSite.SaveColumnState("SITE-LIST", szPath);
}

void CSiteView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscSite, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
//	m_Reposition.AddControl(&m_btnAddSite, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
//	m_Reposition.AddControl(&m_btnDelSite, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
//	m_Reposition.AddControl(&m_btnModSite, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
}

void CSiteView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_Reposition.Move();
}

void CSiteView::InitSiteList()
{
	// 2010.02.17 by gwangsoo
	m_szSiteColum[eCheck    ] = _T("");
	m_szSiteColum[eSiteID   ] = STR_SITE_ID;
	m_szSiteColum[eSiteName ] = STR_SITE_NAME;
	m_szSiteColum[eSiteOpen ] = STR_SITE_OPEN;
	m_szSiteColum[eSiteClose] = STR_SITE_CLOSE;
//	m_szSiteColum[eSiteHoliday] = STR_SITE_HOLI;

	m_lscSite.SetExtendedStyle(m_lscSite.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	for(int nCol = 0; nCol < eSiteEnd; nCol++)
	{
		m_lscSite.InsertColumn(nCol, m_szSiteColum[nCol], LVCFMT_LEFT, 100);
	}

	m_lscSite.SetColumnWidth(m_lscSite.GetColPos(m_szSiteColum[eCheck]), 22); // 2010.02.17 by gwangsoo

	m_lscSite.InitHeader(IDB_LIST_HEADER);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscSite.LoadColumnState("SITE-LIST", szPath);
}

void CSiteView::RefreshSiteList()
{
	int nRow = 0;
	POSITION pos = GetEnvPtr()->m_lsSite.GetHeadPosition();
	m_lscSite.DeleteAllItems();

	while(pos){
		POSITION posOld = pos;
		SSiteInfo info = GetEnvPtr()->m_lsSite.GetNext(pos);

		m_lscSite.InsertItem(nRow, "");
		m_lscSite.SetItemText(nRow, eSiteID   , info.siteId);
		m_lscSite.SetItemText(nRow, eSiteName , info.siteName);
//		m_lscSite.SetItemText(nRow, eSiteOpen , info.shopOpenTime);
//		m_lscSite.SetItemText(nRow, eSiteClose, info.shopCloseTime);

//		cciStringList cciList(info.holiday);
//		m_lscSite.SetItemText(nRow, eSiteHoliday, cciList.toString());

		m_lscSite.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lscSite.GetCurSortCol();
	if(-1 != nCol)
		m_lscSite.SortList(nCol, m_lscSite.IsAscend(), CompareSite, (DWORD_PTR)this);
	else
		m_lscSite.SortList(nCol, CompareSite, (DWORD_PTR)this);
}

int CSiteView::CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	return ((CSiteView*)lParam)->m_lscSite.Compare(lParam1, lParam2);
}

void CSiteView::OnLvnColumnclickListSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int	nColumn = pNMLV->iSubItem;
	
	// 2010.02.17 by gwangsoo
	if(nColumn == m_lscSite.GetColPos(m_szSiteColum[eCheck]))
	{
		bool bCheck = !m_lscSite.GetCheckHdrState();
		for(int nRow=0; nRow<m_lscSite.GetItemCount() ;nRow++)
		{
			m_lscSite.SetCheck(nRow, bCheck);
		}
		m_lscSite.SetCheckHdrState(bCheck);
	}
	else
	{
		m_lscSite.SetSortHeader(nColumn);
		m_lscSite.SortItems(CompareSite, (DWORD_PTR)this);
	}
}

void CSiteView::OnNMCustomdrawListSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
}

void CSiteView::OnNMClickListSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
}

void CSiteView::OnNMDblclkListSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
//	int	nColumn = pNMLV->iSubItem;

	CArray<int> arRow;
	arRow.Add(nRow);
	ModifySite(arRow);
}

void CSiteView::OnBnClickedButtonSiteadd()
{
	if(CCopModule::eSiteAdmin < GetEnvPtr()->m_Authority){
		UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG009), MB_ICONWARNING);
		return;
	}

	CSiteInfoDlg dlg(GetEnvPtr()->m_strCustomer);
	if(dlg.DoModal() != IDOK)
		return;

	SSiteInfo Info = dlg.GetSite();
	if(!CCopModule::GetObject()->AddSite(GetEnvPtr()->m_strCustomer, Info)){
		UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG001), MB_ICONERROR);
		return;
	}

	if(GetEnvPtr()->m_lsSite.AddTail(Info))
	RefreshSiteList();
	UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG002), MB_ICONINFORMATION);
}

void CSiteView::OnBnClickedButtonSitedel()
{
	if(CCopModule::eSiteAdmin < GetEnvPtr()->m_Authority){
		UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG010), MB_ICONWARNING);
		return;
	}

	int targetCount = 0;
	for(int nRow = m_lscSite.GetItemCount()-1; 0 <= nRow; nRow--) {
		if(!m_lscSite.GetCheck(nRow)) continue;
		POSITION pos = (POSITION)m_lscSite.GetItemData(nRow);
		if(!pos) continue;
		targetCount++;
	}
	if(targetCount > 1) {
		UbcMessageBox(IDS_HOSTVIEW_MSG024);
		return;
	}



	int nCnt = 0;
	CString szBuf;
	for(int nRow = m_lscSite.GetItemCount()-1; 0 <= nRow; nRow--){
		if(!m_lscSite.GetCheck(nRow))
			continue;
		m_lscSite.SetCheck(nRow, FALSE);
		nCnt++;

//		szBuf.Format(LoadStringById(IDS_SITEVIEW_MSG003), m_lscSite.GetText(nRow, m_szSiteColum[eSiteID]));
//		if(UbcMessageBox(szBuf, MB_YESNO|MB_ICONINFORMATION) != IDYES){
//			continue;
//		}

		ubcSite  aSite(m_lscSite.GetText(nRow, m_szSiteColum[eSiteID]));


		ciStringList  programList;
		int programCount = aSite.getProgramList(programList);

		// program 이 있으면 삭제할 수 없다.
		if(programCount > 0) {
			UbcMessageBox(IDS_HOSTVIEW_MSG025);
			return;
		}

		ciStringList  hostList;
		int hostCount = aSite.getHostList(hostList);


		//해당 그룹을 삭제하면,  아래와 같은 단말(또는 프로그램)도 함께 삭제됩니다. 삭제하시겠습니까?
		//the terminal is also removed, if you remove the site. Do you want to remove the site.
		if(hostCount > 0 || programCount > 0){
			CSiteDelDlg dlg;
			dlg.SetList(hostList, programList);
			if(dlg.DoModal() != IDOK){
				continue;
			}
		}

		POSITION pos = (POSITION)m_lscSite.GetItemData(nRow);
		if(!pos)	continue;

		SSiteInfo Info = GetEnvPtr()->m_lsSite.GetAt(pos);

		if(Info.siteId.GetLength()==0) continue;
		if(!CCopModule::GetObject()->DelSite(Info.siteId))
			continue;

		GetEnvPtr()->m_lsSite.RemoveAt(pos);
		m_lscSite.DeleteItem(nRow);
	}

	if(nCnt <= 0) UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG004), MB_ICONINFORMATION);
	else          UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG005), MB_ICONINFORMATION);
}

void CSiteView::OnBnClickedButtonSitemod()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscSite.GetItemCount(); nRow++){
		if(!m_lscSite.GetCheck(nRow))
			continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG006), MB_ICONINFORMATION);
		return;
	}

	ModifySite(arRow);
}

void CSiteView::ModifySite(CArray<int>& arRow)
{
	bool bResult = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
TraceLog(("0================="));
		int nRow = arRow.GetAt(nCnt);
		m_lscSite.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscSite.GetItemData(nRow);
		if(!pos) continue;

		SSiteInfo Info = GetEnvPtr()->m_lsSite.GetAt(pos);
TraceLog(("1================="));

		CSiteInfoDlg dlg(GetEnvPtr()->m_strCustomer);
		dlg.SetSite(Info);
		if(dlg.DoModal() != IDOK) continue;
TraceLog(("2================="));
		Info = dlg.GetSite();
TraceLog(("3================="));
		
		if(!CCopModule::GetObject()->SetSite(Info))
		{
TraceLog(("4================="));
			m_szMsg.Format(LoadStringById(IDS_SITEVIEW_MSG007), Info.siteId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
TraceLog(("5================="));

		GetEnvPtr()->m_lsSite.GetAt(pos) = Info;
TraceLog(("6================="));

		bResult = true;
	}
TraceLog(("7================="));

	RefreshSiteList();
TraceLog(("8================="));

	if(arRow.GetCount() >= 1 && bResult)
	{
		UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG008), MB_ICONINFORMATION);
	}
TraceLog(("9================="));

}

void CSiteView::OnBnClickedButtonSiterfs()
{
	SSiteInfo Info;
	if(CCopModule::eSiteAdmin != GetEnvPtr()->m_Authority){
		Info.siteId = GetEnvPtr()->m_szSite;
	}

	GetEnvPtr()->RefreshSite(Info);
	RefreshSiteList();
	UbcMessageBox(LoadStringById(IDS_REFRASHLIST_MSG), MB_ICONINFORMATION);
}