#pragma once
#include "afxcmn.h"
#include "resource.h"
#include "afxwin.h"


// CLicenseInfoDlg 대화 상자입니다.

class CLicenseInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CLicenseInfoDlg)

public:
	CLicenseInfoDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLicenseInfoDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LICENSEINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	bool _getLicenseString(CString& streamBuf);

	DECLARE_MESSAGE_MAP()
public:
	//CRichEditCtrl LicenseInfoEditCtrl;
	virtual BOOL OnInitDialog();
	CEdit m_licenseInfoTxtCtrl;

};
