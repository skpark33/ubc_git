#pragma once

#include "resource.h"
#include <list>
#include "common\utblistctrl.h"

// CSiteDelDlg dialog
class CSiteDelDlg : public CDialog
{
	DECLARE_DYNAMIC(CSiteDelDlg)

public:
	CSiteDelDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSiteDelDlg();

// Dialog Data
	enum { IDD = IDD_SITEREMOVE };
	enum { eType, eName, eEnd};

public:
	void SetList(std::list<std::string>& slHost, std::list<std::string>& slPackage);

private:
	CUTBListCtrl m_lscSite;
	CString m_szSiteColum[eEnd];
	
	std::list<std::string> m_slHost;
	std::list<std::string> m_slPackage;

	void InitList();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
};
