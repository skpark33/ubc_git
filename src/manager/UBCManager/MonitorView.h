#pragma once
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "ubccopcommon\eventhandler.h"
#include "common\reposcontrol.h"
#include "common\CheckComboBox.h"
#include "UBCCopCommon\CopModule.h"
#include "afxwin.h"



// CMonitorView 폼 뷰입니다.
class CMainFrame;

class CMonitorView : public CFormView
{
	DECLARE_DYNCREATE(CMonitorView)

protected:
	CMonitorView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CMonitorView();

public:
	enum { IDD = IDD_MONITORVIEW };
	enum { eCheck, 
			eSiteId,
			eMonitorType,
			eMonitorName,
			eMonitorID,
			eMonitorDesc,
			eStartupTime,
			eShutdownTime,
			eMonitorUseTime,
			eWeekShutdownTime,
			eHoliday,
			   eMonitorEnd };

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	CRect m_rcClient;
	CReposControl m_Reposition;

	CUTBListCtrlEx m_lscMonitor;
	CString		 m_szMonitorColum[eMonitorEnd];

	CHoverButton m_btnAddMonitor;
	CHoverButton m_btnDelMonitor;
	CHoverButton m_btnModMonitor;
	CHoverButton m_btnRefMonitor;
	CHoverButton m_btnMonitorOff;
	CHoverButton m_btnMonitorOn;

	CComboBox m_cbAdmin;
	CComboBox m_cbOperation;
	CCheckComboBox m_cbMonitorType;

	CEdit m_editMonitorName;
	CEdit m_editMonitorId;

	CImageList m_ilMonitorList;

	CString m_strSiteId;

	MonitorInfoList m_lsMonitor;

	CMainFrame*  m_pMainFrame;
	CString		 m_szMsg;
	CPoint m_ptSelList;

	void	InitAuthCtrl();
	void	InitMonitorList();
	BOOL	RefreshMonitor(bool bCompMsg=true);
	void	RefreshMonitorList(POSITION posStart=NULL);
	void	UpdateListRow(int nRow, SMonitorInfo* pInfo);
	void	RefreshPaneText();
	void	ModifyMonitor(CArray<int>& arRow, bool bMsg);
	void	RemoveMonitor(CArray<int>& arRow, bool bMsg);
	void	PowerOn(CArray<int>& arRow, bool bMsg);
	void	PowerOff(CArray<int>& arRow, bool bMsg);
	void	InitPosition(CRect rc);
	int		ChangeOPStat(SDeviceOpStat* pOpStat);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnBnClickedButtonMonitorRef();
	afx_msg void OnBnClickedButtonMonitorCreate();
	afx_msg void OnBnClickedButtonMonitorDel();
	afx_msg void OnBnClickedButtonMonitorMod();
	afx_msg void OnBnClickedButtonMonitorOff();
	afx_msg void OnBnClickedButtonMonitorOn();
	afx_msg void OnBnClickedButtonFilterSite();
	afx_msg void OnDestroy();
	afx_msg void OnNMDblclkListMonitor(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	LRESULT InvokeEvent(WPARAM wParam, LPARAM lParam);
	afx_msg void OnNMRclickListMonitor(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDevicemenuPoweron();
	afx_msg void OnDevicemenuPoweroff();
	afx_msg void OnDevicemenuDetail();
	afx_msg void OnDevicemenuRemove();
};


