// MultiSelectMonitor.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "ubccopcommon\ubccopcommon.h"
#include "ubccopcommon\copModule.h"
#include "MultiSelectMonitor.h"

#include "ubccopcommon\SiteSelectDlg.h"

#include "common\UbcCode.h"
#include "common\TraceLog.h"

// CMultiSelectMonitor 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMultiSelectMonitor, CDialog)

CMultiSelectMonitor::CMultiSelectMonitor(LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CMultiSelectMonitor::IDD, pParent)
	, m_strCustomer ( szCustomer )
{

}

CMultiSelectMonitor::~CMultiSelectMonitor()
{
}

void CMultiSelectMonitor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FILTER_MONITORNAME3, m_editFilterMonitorName);
	DDX_Control(pDX, IDC_EDIT_FILTER_MONITOR3, m_editFilterMonitor);
	DDX_Control(pDX, IDC_BUTTON_FILTER_MONITORTYPE3, m_cbxMonitorType);
	DDX_Control(pDX, IDC_CB_SITE3, m_cbxSite);
	DDX_Control(pDX, IDC_CB_ONOFF3, m_cbOnOff);
	DDX_Control(pDX, IDC_CB_OPERATION3, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_MONITOR_REFRESH3, m_bnRefresh);
	DDX_Control(pDX, IDC_LIST_MONITOR3, m_lcMonitorList);
	DDX_Control(pDX, IDC_LIST_SEL_MONITOR3, m_lcSelMonitorList);
	DDX_Control(pDX, IDC_BN_ADD3, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL3, m_bnDel);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
}

BEGIN_MESSAGE_MAP(CMultiSelectMonitor, CDialog)
	ON_BN_CLICKED(IDOK, &CMultiSelectMonitor::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMultiSelectMonitor::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_ADD3, &CMultiSelectMonitor::OnBnClickedBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL3, &CMultiSelectMonitor::OnBnClickedBnDel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_MONITOR3, &CMultiSelectMonitor::OnNMDblclkListMonitor)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SEL_MONITOR3, &CMultiSelectMonitor::OnNMDblclkListSelMonitor)
	ON_BN_CLICKED(IDC_BUTTON_MONITOR_REFRESH3, &CMultiSelectMonitor::OnBnClickedButtonMonitorRefresh)
	ON_CBN_SELCHANGE(IDC_CB_SITE3, &CMultiSelectMonitor::OnCbnSelchangeCbSite)
END_MESSAGE_MAP()


// CMultiSelectMonitor 메시지 처리기입니다.

BOOL CMultiSelectMonitor::OnInitDialog()
{
	CWaitMessageBox wait;

	CDialog::OnInitDialog();

	if(!m_title.IsEmpty()){
		this->SetWindowText(m_title);
	}

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnAdd    .LoadBitmap(IDB_BUTTON_NEXT2  , RGB(255,255,255));
	m_bnDel    .LoadBitmap(IDB_BUTTON_PREV2  , RGB(255,255,255));
	m_bnOK     .LoadBitmap(IDB_BUTTON_SELECT , RGB(255,255,255));
	m_bnCancel .LoadBitmap(IDB_BUTTON_CANCEL , RGB(255,255,255));

	//m_bnRefresh.SetToolTipText(LoadStringById(IDS_MULTISELECTMONITOR_BTN001));  //

	CodeItemList listMonitorType;
	CUbcCode::GetInstance(m_strCustomer)->GetCategoryInfo(_T("SubMonitorType"), listMonitorType);

	POSITION pos = listMonitorType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listMonitorType.GetNext(pos);
		m_cbxMonitorType.AddString(info.strEnumString, info.nEnumNumber);
	}

	m_cbOnOff.AddString(LoadStringById(IDS_UBCMANAGER_STR001));
	m_cbOnOff.AddString(LoadStringById(IDS_HOSTVIEW_LIST003));
	m_cbOnOff.AddString(LoadStringById(IDS_HOSTVIEW_LIST004));
	m_cbOnOff.SetCurSel(0);

	m_cbOperation.AddString(LoadStringById(IDS_UBCMANAGER_STR001));
	m_cbOperation.AddString(LoadStringById(IDS_HOSTVIEW_LIST001));
	m_cbOperation.AddString(LoadStringById(IDS_HOSTVIEW_LIST002));
	m_cbOperation.SetCurSel(0);


	char buf[1024];
//	CString szPath;
//	szPath.Format("%s%sdata\\UBCManager.ini", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	GetPrivateProfileString("MONITOR-FILTER", "MonitorName", "", buf, 1024, m_strIniPath);
	m_editFilterMonitorName.SetWindowText(buf);
	GetPrivateProfileString("MONITOR-FILTER", "MonitorID", "", buf, 1024, m_strIniPath);
	m_editFilterMonitor.SetWindowText(buf);

	GetPrivateProfileString("MONITOR-FILTER", "SubMonitorType", "0", buf, 1024, m_strIniPath);
	CString strMonitorType = buf;
	if(strMonitorType.IsEmpty())
	{
		m_cbxMonitorType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strMonitorType.Tokenize(",", pos)) != _T(""))
		{
			m_cbxMonitorType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}

	GetPrivateProfileString("MONITOR-FILTER", "SiteName", "", buf, 1024, m_strIniPath);
	CString strSiteName = buf;
	m_cbxSite.AddString(LoadStringById(IDS_UBCMANAGER_STR001));
	m_cbxSite.AddString(LoadStringById(IDS_UBCMANAGER_STR002));
	m_cbxSite.SetCurSel(0);
	//if(CCopModule::eSiteAdmin != m_nAuthority)
	if(CCopModule::eSiteUser == m_nAuthority) // skpark 2012.11.1 siteUser 만 못고친다.
		                                      // siteManager 는 하위 사이트선택이 가능해야 한다.
	{
		m_cbxSite.InsertString(1, m_strSiteId);
		m_cbxSite.SetCurSel(1);
		m_cbxSite.EnableWindow(FALSE);

		m_strSite = m_strSiteId;
	}
	else
	{
		GetPrivateProfileString("MONITOR-FILTER", "SiteID", "", buf, 1024, m_strIniPath);
		m_strSite = buf;

		if(m_strSite.GetLength() > 0)
		{
			m_cbxSite.InsertString(1, strSiteName);
			m_cbxSite.SetCurSel(1);
		}
	}

	GetPrivateProfileString("MONITOR-FILTER", "AdminStat", "0", buf, 1024, m_strIniPath);
	m_cbOnOff.SetCurSel(atoi(buf));
	GetPrivateProfileString("MONITOR-FILTER", "OperaStat", "0", buf, 1024, m_strIniPath);
	m_cbOperation.SetCurSel(atoi(buf));

	InitMonitorList();
	RefreshMonitorList();

	InitSelMonitorList();

	RefreshSelMonitorList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMultiSelectMonitor::OnBnClickedOk()
{
	m_astrSelMonitorList.RemoveAll();
	m_astrSelMonitorNameList.RemoveAll();

	for(int i=0; i<m_lcSelMonitorList.GetItemCount() ;i++)
	{
		m_astrSelMonitorList.Add(m_lcSelMonitorList.GetItemText(i, eMonitorID));
		m_astrSelMonitorNameList.Add(m_lcSelMonitorList.GetItemText(i, eMonitorName));
	}
	TraceLog(("%d row selected", m_astrSelMonitorNameList.GetSize()));

	OnOK();
}

void CMultiSelectMonitor::OnBnClickedCancel()
{
	OnCancel();
}

void CMultiSelectMonitor::InitMonitorList()
{
	m_lcMonitorList.SetExtendedStyle(m_lcMonitorList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck ] = _T("");
	m_szColum[eMonitorName] = LoadStringById(IDS_MONITORVIEW_STR003);
	m_szColum[eMonitorID] = LoadStringById(IDS_MONITORVIEW_STR002);
	m_szColum[eGroup ] = LoadStringById(IDS_HOSTVIEW_LIST007);

	m_lcMonitorList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcMonitorList.InsertColumn(eMonitorName , m_szColum[eMonitorName ], LVCFMT_LEFT  , 100);
	m_lcMonitorList.InsertColumn(eMonitorID , m_szColum[eMonitorID ], LVCFMT_LEFT  , 100);
	m_lcMonitorList.InsertColumn(eGroup  , m_szColum[eGroup  ], LVCFMT_LEFT  ,  85);

	// 제일 마지막에 할것...
	m_lcMonitorList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcMonitorList.SetSortEnable(false);
//	m_lcMonitorList.SetFixedCol(eCheck);
}

void CMultiSelectMonitor::RefreshMonitorList()
{
	CWaitMessageBox wait;

	CStringArray filter_list;
	CString strTmp;

	//
	CString strMonitorName;
	m_editFilterMonitorName.GetWindowText(strMonitorName);
	if(!strMonitorName.IsEmpty())
	{
		strTmp.Format(_T("monitorName like '%%%s%%'"), strMonitorName);
		filter_list.Add(strTmp);
	}

	//
	CString strMonitor;
	m_editFilterMonitor.GetWindowText(strMonitor);
	if(!strMonitor.IsEmpty())
	{
		strTmp.Format(_T("monitorId like '%%%s%%'"), strMonitor);
		filter_list.Add(strTmp);
	}

	//
	CString strMonitorType = m_cbxMonitorType.GetCheckedIDs();
	if(strMonitorType == _T("All")) strMonitorType = _T("");
	else if(strMonitorType.GetLength() >= 2) strMonitorType = strMonitorType.Mid(1, strMonitorType.GetLength()-2);
	if(!strMonitorType.IsEmpty())
	{
		strTmp.Format(_T("monitorType in ('%s')")
					, strMonitorType
					);
		strTmp.Replace(",", "','");
		filter_list.Add(strTmp);
	}

	//
	CString strSiteName;
	m_cbxSite.GetWindowText(strSiteName);

	if(m_strSite.IsEmpty())
	{
		if(CCopModule::eSiteAdmin != m_nAuthority)
		{
			strTmp.Format(_T("siteId like '%%%s%%'"), m_strSiteId);
			filter_list.Add(strTmp);
		}
	}
	else
	{
		// skpark 2013.4.23 multiselect 가 가능하도록
		//strTmp.Format(_T("siteId like '%%%s%%'"), m_strSite);
		//filter_list.Add(strTmp);
		CString strSiteId = m_strSite;
		if(!strSiteId.IsEmpty()){
			strSiteId.Replace(_T(","), _T("','"));
			strTmp.Format(_T("siteId in ('%s')"), strSiteId);
			filter_list.Add(strTmp);
		}
	}

	//
	if(m_cbOnOff.GetCurSel())
	{
		strTmp.Format(_T("adminState = %d"), (m_cbOnOff.GetCurSel()==1)? 1:0);
		filter_list.Add(strTmp);
	}

	if(m_cbOperation.GetCurSel())
	{
		strTmp.Format(_T("operationalState = %d"), (m_cbOperation.GetCurSel()==1)? 1:0);
		filter_list.Add(strTmp);
	}


	//
	CString strWhere = "";
	int count = filter_list.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str = filter_list.GetAt(i);

		if(strWhere.GetLength() > 0)
		{
			strWhere += " and ";
		}

		strWhere += "( ";
		strWhere += str;
		strWhere += ") ";
	}

#ifdef _DEBUG
//	UbcMessageBox(strWhere);
#endif

	WritePrivateProfileString("MONITOR-FILTER", "MonitorName", strMonitorName, m_strIniPath);
	WritePrivateProfileString("MONITOR-FILTER", "MonitorID", strMonitor, m_strIniPath);
	WritePrivateProfileString("MONITOR-FILTER", "SubMonitorType", strMonitorType, m_strIniPath);
	WritePrivateProfileString("MONITOR-FILTER", "SiteName", strSiteName, m_strIniPath);
	WritePrivateProfileString("MONITOR-FILTER", "SiteID", m_strSite, m_strIniPath);

	WritePrivateProfileString("MONITOR-FILTER", "AdminStat", ToString(m_cbOnOff.GetCurSel()), m_strIniPath);
	WritePrivateProfileString("MONITOR-FILTER", "OperaStat", ToString(m_cbOperation.GetCurSel()), m_strIniPath);

	m_lsInfoList.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetMonitorList((CCopModule::eSiteAdmin == m_nAuthority ? _T("*") : m_strSiteId)
													, _T("*")
													, strWhere
													, true
													, true
													, m_lsInfoList 	);

	m_lcMonitorList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SMonitorInfo info = m_lsInfoList.GetNext(pos);

		m_lcMonitorList.InsertItem(nRow, "");
		m_lcMonitorList.SetItemText(nRow, eMonitorName, info.monitorName);
		m_lcMonitorList.SetItemText(nRow, eMonitorID, info.monitorId);
		m_lcMonitorList.SetItemText(nRow, eGroup , info.siteId);
		m_lcMonitorList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
}

void CMultiSelectMonitor::OnBnClickedButtonMonitorRefresh()
{
	RefreshMonitorList();
}

void CMultiSelectMonitor::InitSelMonitorList()
{
	m_lcSelMonitorList.SetExtendedStyle(m_lcSelMonitorList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcSelMonitorList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcSelMonitorList.InsertColumn(eMonitorName , m_szColum[eMonitorName ], LVCFMT_LEFT  , 100);
	m_lcSelMonitorList.InsertColumn(eMonitorID , m_szColum[eMonitorID ], LVCFMT_LEFT  , 100);

	// 제일 마지막에 할것...
	m_lcSelMonitorList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcSelMonitorList.SetSortEnable(false);
//	m_lcSelMonitorList.SetFixedCol(eCheck);
}

void CMultiSelectMonitor::RefreshSelMonitorList()
{
	m_lcSelMonitorList.DeleteAllItems();

	for(int i=0; i<m_astrSelMonitorList.GetCount() ;i++)
	{
		m_lcSelMonitorList.InsertItem(i, "");
		m_lcSelMonitorList.SetItemText(i, eMonitorID, m_astrSelMonitorList[i]);
	}
	for(int i=0; i<m_astrSelMonitorNameList.GetCount() ;i++)
	{
		//m_lcSelMonitorList.InsertItem(i, "");
		m_lcSelMonitorList.SetItemText(i, eMonitorName, m_astrSelMonitorNameList[i]);
	}
}

void CMultiSelectMonitor::OnNMDblclkListMonitor(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	AddSelMonitor(pNMLV->iItem);
}

void CMultiSelectMonitor::OnNMDblclkListSelMonitor(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	DelSelMonitor(pNMLV->iItem);
}

void CMultiSelectMonitor::OnBnClickedBnAdd()
{
	bool bChecked = false;
	for(int i=0; i<m_lcMonitorList.GetItemCount() ;i++)
	{
		if(m_lcMonitorList.GetCheck(i))
		{
			bChecked = true;
			break;
		}
	}

	if(!bChecked)
	{
		UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR019));
		return;
	}

	for(int i=0; i<m_lcMonitorList.GetItemCount() ;i++)
	{
		if(!m_lcMonitorList.GetCheck(i)) continue;

		AddSelMonitor(i);

		m_lcMonitorList.SetCheck(i, FALSE);
	}

	m_lcMonitorList.SetCheckHdrState(FALSE);
}

void CMultiSelectMonitor::OnBnClickedBnDel()
{
	for(int i=m_lcSelMonitorList.GetItemCount()-1; i>=0 ;i--)
	{
		if(!m_lcSelMonitorList.GetCheck(i)) continue;
		DelSelMonitor(i);
	}

	m_lcSelMonitorList.SetCheckHdrState(FALSE);
}

void CMultiSelectMonitor::AddSelMonitor(int nMonitorIndex)
{
	if(nMonitorIndex < 0 || nMonitorIndex >= m_lcMonitorList.GetItemCount()) return;

	CString strMonitorId = m_lcMonitorList.GetItemText(nMonitorIndex, eMonitorID);
	CString strMonitorName = m_lcMonitorList.GetItemText(nMonitorIndex, eMonitorName);

	// 이미 추가된 단말인지 체크
	for(int i=0; i<m_lcSelMonitorList.GetItemCount() ;i++)
	{
		CString strSelMonitorId = m_lcSelMonitorList.GetItemText(i, eMonitorID);
		if(strSelMonitorId == strMonitorId) return;
	}

	int nRow = m_lcSelMonitorList.GetItemCount();
	m_lcSelMonitorList.InsertItem(nRow, "");
	m_lcSelMonitorList.SetItemText(nRow, eMonitorID, strMonitorId);
	m_lcSelMonitorList.SetItemText(nRow, eMonitorName, strMonitorName);
}

void CMultiSelectMonitor::DelSelMonitor(int nSelMonitorIndex)
{
	if(nSelMonitorIndex < 0 || nSelMonitorIndex >= m_lcSelMonitorList.GetItemCount()) return;

	m_lcSelMonitorList.DeleteItem(nSelMonitorIndex);
}

BOOL CMultiSelectMonitor::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_SITEID3)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_MONITOR3 )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_ONOFF3          )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_OPERATION3      )->GetSafeHwnd() )
		{
			OnBnClickedButtonMonitorRefresh();
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CMultiSelectMonitor::OnCbnSelchangeCbSite()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbxSite.GetCurSel();
	int count = m_cbxSite.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxSite.DeleteString(1);
			m_strSite = "";
		}
	}
	else if(idx == count-1 )
	{
		CSiteSelectDlg dlg(m_strCustomer);
		//skpark 2013.4.23 multiselect 수정
		//dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_strSite = "";
				m_cbxSite.DeleteString(1);
				m_cbxSite.SetCurSel(0);
			}
			//skpark 2013.4.23 multiselect start[
			CString strSiteName,strSiteId;
			SiteInfoList siteInfoList;
			if(dlg.GetSelectSiteInfoList(siteInfoList))
			{
				POSITION pos = siteInfoList.GetHeadPosition();
				while(pos)
				{
					SSiteInfo info = siteInfoList.GetNext(pos);
					strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
					strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
				}
			}
			if(!strSiteName.IsEmpty()){
				m_cbxSite.InsertString(1, strSiteName);
				m_cbxSite.SetCurSel(1);
				m_strSite = strSiteId;
			}else{
				m_cbxSite.SetCurSel(0);
				m_strSite = "";
			
			}

			//SSiteInfo info;
			//dlg.GetSelectSiteInfo(info);

			//if(info.siteName.GetLength() > 0)
			//{
			//	m_cbxSite.InsertString(1, info.siteName);
			//	m_cbxSite.SetCurSel(1);
			//	m_strSite = info.siteId;
			//}
			//else
			//{
			//	m_cbxSite.SetCurSel(0);
			//	m_strSite = "";
			//}
			//skpark 2013.4.23 multiselect end]

		}
		else
		{
			m_cbxSite.SetCurSel(count-2);
		}
	}else{
		// skpark 2013.4.23 multiselect 
		// 콤보박스의 글자가 14자가 넘어가면 팝업으로 보여준다.
		CString strSiteName;
		m_cbxSite.GetWindowText(strSiteName);
		if(strSiteName.GetLength() > 14){
			CString msgStr = "Selected Group is\n";
			msgStr.Append(strSiteName);
			UbcMessageBox(msgStr);
		}
	}
}

