// MainFrm.cpp : implementation of the CMainFrame class
//
#include "stdafx.h"
#include "UBCManager.h"
#include "MainFrm.h"
#include "enviroment.h"

#include "LoginDlg.h"

//#include "SiteDoc.h"
//#include "SiteView.h"
#include "WebSiteView.h"
#include "SiteFrame.h"
//#include "HostDoc.h"
#include "HostView.h"
#include "HostFrame.h"
#include "PlanoView.h"
#include "PlanoFrame.h"
#include "LightView.h"
#include "MonitorView.h"
#include "LightFrame.h"
#include "MonitorFrame.h"
#include "IconFrame.h"
#include "IconView.h"
#include "PlanoMgrFrame.h"
#include "PlanoMgrView.h"
//#include "UserDoc.h"
//#include "UserView.h"
#include "WebUserView.h"
#include "UserFrame.h"
//#include "HostScreenDoc.h"
#include "HostScreenView.h"
#include "HostScreenFrm.h"
#include "GeoMapView.h"
#include "GeoMapFrm.h"
//#include "PackageDoc.h"
#include "PackageFrm.h"
#include "PackageView.h"
//#include "NotiMngDoc.h"
#include "NotiMngFrm.h"
#include "NotiMngView.h"
#include "DummyDoc.h"	// ��EDocument �� ŁE?
#include "BroadcastPlanFrm.h"
#include "BroadcastPlanView.h"
#include "ProcessFrm.h"
#include "ProcessView.h"
//#include "HostLogFrm.h"
//#include "HostLogView.h"
#include "FaultMngFrm.h"
#include "FaultMngView.h"
#include "BulletinboardDlg.h"
#include "WebCodeView.h"
#include "WebCodeFrame.h"
#include "WebPageView.h"
#include "WebPageFrame.h"
#include "UpdateCenterFrame.h"
#include "UpdateCenterView.h"
#include "SiteUserView.h"

#include "UBCCopCommon\CopModule.h"
//#include "UBCCopCommon\EventHandler.h"
#include "UBCCopCommon\PackageMngDlg.h"
#include "common\libscratch\scratchUtil.h"
#include "common\libscratch\orbconn.h"
#include "common\libinstall\installutil.h"
#include "common\libCommon\ubcMuxRegacy.h"
#include "UBCCopCommon\SiteSelectDlg.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include <hi\libHttp\hiHttpWrapper.h>

//skpark web-->client
#include "PackageChngLogDlg.h"
//#include "LogMainDlg.h"
#include "PrimaryLogFrame.h"
#include "PrimaryLogView.h"
#include "TrashCanFrame.h"
#include "TrashCanView.h"
#include "LMOSolutionFrm.h"   //skpark lmo_solution
#include "LMOSolutionView.h"  //skpark lmo_solution
#include "CodeFrm.h"   //skpark code
#include "CodeView.h"  //skpark code
#include "CacheServerSelectDlg.h" 
#include <Tlhelp32.h>
#include <shlwapi.h>
#include <winternl.h>

#include "AppUploadDlg.h"
#include "SysAdminLoginDlg.h"
#include "ServerFileUploadDlg.h"
#include "HsrCommandDlg.h"
#include "ApproveDlg.h"
#include "IPInputDLG.h"
#include "RemoteLoginSettingsDlg.h"
#include "DownloadGroupDlg.h"

#ifdef _DEBUG
bool g_test = false;
#else
bool g_test = false;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if (!IsWindowVisible(hwnd)) return TRUE;
	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);
	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;
	if (pe->pid != pid) return TRUE;
	pe->hwnd = hwnd;
	return FALSE;
}



// CMainFrame
IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_COMMAND(ID_FILE_SITEMANAGER, OnFileSitemanager)
	ON_COMMAND(ID_FILE_HOSTMANAGER, OnFileHostmanager)
	ON_COMMAND(ID_FILE_PLANO, OnFilePlano)
	ON_COMMAND(ID_FILE_USERMANAGER, OnFileUsermanager)
	ON_COMMAND(ID_FILE_SCREENMANGER, OnFileScreenManager)
	ON_COMMAND(ID_FILE_PACKAGEMANAGER, OnPackageManager)
	ON_COMMAND(ID_FILE_NOTIFICATION, OnNotification)
	ON_COMMAND(ID_TOOL_UBCSTUDIO, OnToolUBCStudio)
	ON_WM_WINDOWPOSCHANGING()
	ON_REGISTERED_MESSAGE(WM_WAIT_FOR_CREATED, OnInitializePost)
	ON_COMMAND(ID_AUTO_UPDATE, &CMainFrame::OnAutoUpdate)
	ON_COMMAND(ID_FILE_BROADCAST_PLAN, OnFileBroadcastPlanView)
	ON_REGISTERED_MESSAGE(WM_SET_STATUSBAR_TEXT, OnStatusBarPaneText)
	ON_COMMAND(ID_FILE_HOST_PROCESS, &CMainFrame::OnFileHostProcess)
//	ON_COMMAND(ID_FILE_HOST_LOG, &CMainFrame::OnFileHostLog)
	ON_COMMAND(ID_FILE_FAULT_MNG, &CMainFrame::OnFileFaultMng)
	ON_COMMAND(ID_APP_BULLETINBOARD, &CMainFrame::OnAppBulletinboard)
	ON_COMMAND(ID_APP_CODEMNG, &CMainFrame::OnAppCodeMng)
	ON_COMMAND(ID_LMO_SOLUTION_MENU, &CMainFrame::OnLMOSolutionMenu) //skpark lmo_solution
	ON_COMMAND(ID_LOG_PACKAGE_CHNG, &CMainFrame::OnLogPackageChng)
	ON_COMMAND(ID_LOG_POWER_ONOFF, &CMainFrame::OnLogPowerOnoff)
	ON_COMMAND(ID_LOG_CONNECTION, &CMainFrame::OnLogConnection)
	ON_COMMAND(ID_LOG_FAULT, &CMainFrame::OnLogFault)
	ON_COMMAND(ID_LOG_DOWNLOAD, &CMainFrame::OnLogDownload)
	ON_COMMAND(ID_LOG_LOGIN, &CMainFrame::OnLogLogin)
	ON_COMMAND(ID_STATISTICS_FAULT, &CMainFrame::OnStatisticsFault)
	ON_COMMAND(ID_STATISTICS_CONTENTS, &CMainFrame::OnStatisticsContents)
	ON_COMMAND(ID_STATISTICS_INTERACT, &CMainFrame::OnStatisticsInteract)
//	ON_COMMAND(ID_FILE_CONTENTS_MNG, &CMainFrame::OnFileContentsMng)	// 0001443: �Ŵ��� ��E��������E?��� ����E����
	ON_COMMAND(ID_WEB_PROGRAM_DOWN, &CMainFrame::OnWebProgramDown)
	ON_COMMAND(ID_WEB_MANUAL_DOWN, &CMainFrame::OnWebManualDown)
	ON_COMMAND(ID_WEB_VIDEO_MANUAL, &CMainFrame::OnWebVideoManual)
	ON_COMMAND(ID_FILE_UPDATE_CENTER, &CMainFrame::OnFileUpdateCenter)
	ON_UPDATE_COMMAND_UI(ID_AUTO_UPDATE, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_NOTIFICATION, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_SITEMANAGER, OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_HOSTMANAGER, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_CACHESERVER, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_PLANO, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_LIGHTLIST, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_MONITORLIST, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_PACKAGEMANAGER, &CMainFrame::OnUpdateMenuAuth)
//	ON_UPDATE_COMMAND_UI(ID_FILE_CONTENTS_MNG, &CMainFrame::OnUpdateMenuAuth)	// 0001443: �Ŵ��� ��E��������E?��� ����E����
	ON_UPDATE_COMMAND_UI(ID_FILE_BROADCAST_PLAN, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_SCREENMANGER, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_GEOMAP, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_HOST_PROCESS, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_FAULT_MNG, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_LOG_PACKAGE_CHNG, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_LOG_POWER_ONOFF, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_LOG_CONNECTION, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_LOG_FAULT, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_LOG_DOWNLOAD, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_LOG_LOGIN, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_FAULT, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_CONTENTS, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_INTERACT, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_APP_BULLETINBOARD, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_FILE_USERMANAGER, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_APP_CODEMNG, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_LMO_SOLUTION_MENU, &CMainFrame::OnUpdateMenuAuth) //skpark lmo_solution
	ON_UPDATE_COMMAND_UI(ID_FILE_UPDATE_CENTER, &CMainFrame::OnUpdateMenuAuth)
	
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_FIRMWARE, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_FIRMWARE2, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_UBC_PLAYER_APP, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_UBC_PLAYER_APP2, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_VACCINE_APP, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_VACCINE_DB, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_MANAGER_UPDATE, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_SERVER_UPDATE, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_APP_DGROUP, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_UBC_COMMON_PALYER_APP, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_UPLOAD_UBC_COMMON_LAUNCHER_APP, &CMainFrame::OnUpdateMenuAuth)

	ON_COMMAND(ID_LOG_PACKAGE_APPLY, &CMainFrame::OnLogPackageApply)
	ON_COMMAND(ID_LOG_BROADCAST_PLAN, &CMainFrame::OnLogBroadcastPlan)
	ON_COMMAND(ID_LOG_PLAYER_ERROR, &CMainFrame::OnLogPlayerError)
	ON_UPDATE_COMMAND_UI(ID_LOG_PACKAGE_APPLY, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_LOG_BROADCAST_PLAN, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_LOG_PLAYER_ERROR, &CMainFrame::OnUpdateMenuAuth)
	ON_COMMAND(ID_STATISTICS_BROADCASTPLAN, &CMainFrame::OnStatisticsBroadcastplan)
	ON_COMMAND(ID_STATISTICS_PACKAGE_REG, &CMainFrame::OnStatisticsPackageReg)
	ON_COMMAND(ID_STATISTICS_PACKAGE_USE, &CMainFrame::OnStatisticsPackageUse)
	ON_COMMAND(ID_STATISTICS_CONTENTS_REG, &CMainFrame::OnStatisticsContentsReg)
	ON_COMMAND(ID_STATISTICS_CONTENTS_USE, &CMainFrame::OnStatisticsContentsUse)
	ON_COMMAND(ID_STATISTICS_CONTENTS_ACC, &CMainFrame::OnStatisticsContentsAcc)
	ON_COMMAND(ID_STATISTICS_MSG_USE, &CMainFrame::OnStatisticsMsgUse)
	ON_COMMAND(ID_STATISTICS_APPLY, &CMainFrame::OnStatisticsApply)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_BROADCASTPLAN, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_PACKAGE_REG, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_PACKAGE_USE, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_CONTENTS_REG, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_CONTENTS_USE, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_CONTENTS_ACC, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_MSG_USE, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_STATISTICS_APPLY, &CMainFrame::OnUpdateMenuAuth)
	ON_REGISTERED_MESSAGE(WM_CLOSE_DIALOG, OnCloseDialog)
	ON_COMMAND(ID_ALPHA_UPDATE, &CMainFrame::OnAlphaUpdate)
	ON_UPDATE_COMMAND_UI(ID_ALPHA_UPDATE, &CMainFrame::OnUpdateMenuAuth)
	ON_COMMAND(ID_COLUMN_NEXT_SCHEDULE, &CMainFrame::OnColumnNextSchedule)
	ON_UPDATE_COMMAND_UI(ID_COLUMN_NEXT_SCHEDULE, &CMainFrame::OnUpdateMenuAuth)
	ON_COMMAND(ID_FILE_LIGHTLIST, &CMainFrame::OnFileLightlist)
	ON_COMMAND(ID_FILE_MONITORLIST, &CMainFrame::OnFileMonitorlist)
	ON_COMMAND(ID_FILE_ICONLIST, &CMainFrame::OnFileIconlist)
	ON_COMMAND(ID_FILE_PLANOMGR, &CMainFrame::OnFilePlanoMgr)
	ON_COMMAND(ID_FILE_CACHESERVER, &CMainFrame::OnFileCacheserver)
	ON_COMMAND(ID_WEB_WELCOMEBOARD, &CMainFrame::OnWebWelcomeboard)
	ON_COMMAND(ID_FILE_GEOMAP, &CMainFrame::OnFileGeomap)
	ON_COMMAND(ID_TIMESYNC, &CMainFrame::OnTimesync)
	ON_UPDATE_COMMAND_UI(ID_WEB_PROGRAM_DOWN, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_WEB_MANUAL_DOWN, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_WEB_VIDEO_MANUAL, &CMainFrame::OnUpdateMenuAuth)
	ON_UPDATE_COMMAND_UI(ID_WEB_WELCOMEBOARD, &CMainFrame::OnUpdateMenuAuth)
	ON_COMMAND(ID_UPLOAD_UBC_PLAYER_APP, &CMainFrame::OnUploadUbcPlayerApp)
	ON_COMMAND(ID_UPLOAD_FIRMWARE, &CMainFrame::OnUploadFirmware)
	ON_COMMAND(ID_UPLOAD_VACCINE_APP, &CMainFrame::OnUploadVaccineApp)
	ON_COMMAND(ID_UPLOAD_VACCINE_DB, &CMainFrame::OnUploadVaccineDB)
	ON_COMMAND(ID_TOOL_COMMANDER, &CMainFrame::OnToolCommander)
	ON_COMMAND(ID_TOOL_UPLOADER, &CMainFrame::OnToolUploader)
	ON_COMMAND(ID_UPLOAD_MANAGER_UPDATE, &CMainFrame::OnUploadManagerUpdate)
	ON_COMMAND(ID_UPLOAD_SERVER_UPDATE, &CMainFrame::OnUploadServerUpdate)
	ON_COMMAND(ID_TOOL_TRASHCAN, &CMainFrame::OnToolTrashcan)
	ON_COMMAND(ID_APPROVE, &CMainFrame::OnApprove)
	ON_COMMAND(ID_USE_AXIS2, &CMainFrame::OnUseAxis2)
	ON_UPDATE_COMMAND_UI(ID_USE_AXIS2, &CMainFrame::OnUpdateMenuAuth)
	ON_COMMAND(ID_UPLOAD_FIRMWARE2, &CMainFrame::OnUploadFirmware2)
	ON_COMMAND(ID_UPLOAD_UBC_PLAYER_APP2, &CMainFrame::OnUploadUbcPlayerApp2)
	ON_COMMAND(ID_APP_DGROUP, &CMainFrame::OnAppDgroup)
	ON_COMMAND(ID_UPLOAD_UBC_COMMON_PALYER_APP, &CMainFrame::OnUploadUbcCommonPalyerApp)
	ON_COMMAND(ID_UPLOAD_UBC_COMMON_LAUNCHER_APP, &CMainFrame::OnUploadUbcCommonLauncherApp)
	END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,  //  SBID_TEXT	         // status line indicator
	ID_SEPARATOR,  //  SBID_LED	
	ID_SEPARATOR,  //  SBID_HOST	
	ID_SEPARATOR,  //  SBID_PROC	
	ID_SEPARATOR,  //  SBID_COMM	
	ID_SEPARATOR,  //  SBID_HDD	
	ID_SEPARATOR,  //  SBID_MEM	
	ID_SEPARATOR,  //  SBID_CPU	
	ID_SEPARATOR,  //  SBID_ETC	
//	ID_INDICATOR_CAPS,
//	ID_INDICATOR_NUM,
//	ID_INDICATOR_SCRL,
};

// CMainFrame construction/destruction
CMainFrame::CMainFrame() :
	m_EventManager(this)
{
	m_pSiteTemp = NULL;
	m_pHostTemp = NULL;
	m_pPlanoTemp = NULL;
	m_pLightTemp = NULL;
	m_pPlanoMgrTemp = NULL;
	m_pIconTemp = NULL;
	m_pMonitorTemp = NULL;
	m_pUserTemp = NULL;
	m_pScreenTemp = NULL;
	m_pGeoMapTemp = NULL;
	m_pPackageTemp = NULL;
	m_pNotiTemp = NULL;
	m_pLMOTemp = NULL;  //skpark lmo_solution
	m_pBroadcastPlanTemp = NULL;
	m_pProcessTemp = NULL;
//	m_pHostLogTemp = NULL;
	m_pFaultMngTemp = NULL;
	m_pCodeTemp = NULL;
	m_pWebPageTemp = NULL;
	m_pUpdateCenterTemp = NULL;

	m_pPrimaryLogTemp = NULL;
	m_pTrashCanTemp = NULL;

	m_hLedGreen  = NULL;
	m_hLedRed    = NULL;
	m_hLedYellow = NULL;

	m_pServerFileUpload = NULL;
	m_pHsrCommand = NULL;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndBitmapBG.SubclassWindow(m_hWndMDIClient))
	{
		TRACE("Failed to subclass MDI client window.\n");
		return -1;
	}

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	// Set up hot bar image lists.
	CImageList imageList;
	CBitmap    bitmap;

	// Create and set the normal toolbar image list.
	if(CEnviroment::eNARSHA == CEnviroment::GetObject()->m_Customer){
		bitmap.LoadBitmap(IDB_TOOLBAR_ENABLE_NS);
	}else{
		bitmap.LoadBitmap(IDB_TOOLBAR_ENABLE);
	}
	//bitmap.LoadBitmap(IDB_TOOLBAR);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	// Create and set the disable toolbar image list.
	if(CEnviroment::eNARSHA == CEnviroment::GetObject()->m_Customer){
		bitmap.LoadBitmap(IDB_TOOLBAR_DISABLE_NS);
	}else{
		bitmap.LoadBitmap(IDB_TOOLBAR_DISABLE);
	}
	//bitmap.LoadBitmap(IDB_TOOLBAR);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETDISABLEDIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	// Create and set the hot toolbar image list.
	if(CEnviroment::eNARSHA == CEnviroment::GetObject()->m_Customer){
		bitmap.LoadBitmap(IDB_TOOLBAR_HOT_NS);
	}else{
		bitmap.LoadBitmap(IDB_TOOLBAR_HOT);
	}
	//bitmap.LoadBitmap(IDB_TOOLBAR_DISABLE);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETHOTIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	//tool bar ũ��E����
	m_wndToolBar.SetSizes(CSize(48+7,48+6), CSize(48, 48));

	// Toolbar �� ����Ʈ�� �α�ID ǥ��
	//CRect rect;
	//int nIndex = m_wndToolBar.GetToolBarCtrl().CommandToIndex(ID_TOOLBAR_TEXT);
	//m_wndToolBar.SetButtonInfo(nIndex, ID_TOOLBAR_TEXT, TBBS_SEPARATOR, 380);
	//m_wndToolBar.GetToolBarCtrl().GetItemRect(nIndex, &rect);

	//rect.left += 5;

	//m_LoginInfoDlg.Create(CLoginInfoDlg::IDD, &m_wndToolBar);
	//m_LoginInfoDlg.MoveWindow(&rect);
	//m_LoginInfoDlg.ShowWindow(SW_SHOW);

	if(CEnviroment::eNARSHA == CEnviroment::GetObject()->m_Customer){
		SetIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME_NS),false);
	}else{
		SetIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME),false);
	}

	// 0000554: �ڹٿ����� �õ��ϴ� ������ �α��� ��ư�� ���� �������� �����Ѵ�.
	//SOrbConnInfo* pInfo = COrbConn::GetObject()->GetCur();
	//if(!CCopModule::GetObject()->InitORB(pInfo->ip, pInfo->port)){
	//	CString szBuf;
	//	szBuf.Format(LoadStringById(IDS_MAINFRAME_MSG001), pInfo->ip, pInfo->port);
	//	UbcMessageBox(szBuf);
	//}

	m_hLedGreen  = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_GREEN_LED ), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR);
	m_hLedRed    = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_RED_LED   ), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR);
	m_hLedYellow = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_YELLOW_LED), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR);

	//m_hLedGreen  = AfxGetApp()->LoadIcon(IDI_GREEN_LED );
	//m_hLedRed    = AfxGetApp()->LoadIcon(IDI_RED_LED   );
	//m_hLedYellow = AfxGetApp()->LoadIcon(IDI_YELLOW_LED);

	CMenu* pMainMenu = GetMenu();	
	if(pMainMenu && CEnviroment::GetObject()->m_Customer == CEnviroment::eHYUNDAI)
	{
		pMainMenu->DeleteMenu(ID_STATISTICS_BROADCASTPLAN, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_STATISTICS_PACKAGE_REG, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_STATISTICS_PACKAGE_USE, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_STATISTICS_CONTENTS_REG, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_STATISTICS_CONTENTS_USE, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_STATISTICS_CONTENTS_ACC, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_STATISTICS_APPLY, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_STATISTICS_MSG_USE, MF_BYCOMMAND);
	}
	else if(pMainMenu && CEnviroment::GetObject()->m_Customer == CEnviroment::eKIA)
	{
		// 0001443: �Ŵ��� ��E��������E?��� ����E����
		//pMainMenu->DeleteMenu(ID_FILE_CONTENTS_MNG, MF_BYCOMMAND);
	}
	if(pMainMenu && CEnviroment::GetObject()->m_Customer != CEnviroment::eKPOST)
	{
		pMainMenu->DeleteMenu(ID_UPLOAD_FIRMWARE, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_UPLOAD_FIRMWARE2, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_UPLOAD_UBC_PLAYER_APP, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_UPLOAD_UBC_PLAYER_APP2, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_UPLOAD_VACCINE_APP, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_UPLOAD_VACCINE_DB, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_UPLOAD_MANAGER_UPDATE, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_UPLOAD_SERVER_UPDATE, MF_BYCOMMAND);
		pMainMenu->DeleteMenu(ID_APP_DGROUP, MF_BYCOMMAND);
	}
	AfxBeginThread(CMainFrame::StrongSecurityThread, this);

	GOTO_INITIALIZE_POST;

	return 0;
}

LRESULT CMainFrame::OnInitializePost(WPARAM wParam, LPARAM lParam)
{
	CLoginDlg dlg;
	if(dlg.DoModal() != IDOK)
	{
//		PostQuitMessage(0);
		PostMessage(WM_CLOSE);
		return -1;
	}

	CWaitMessageBox wait;

	// ���������� �����´�
	InitMenuAuth();

//	UbcMessageBox(GetEnvPtr()->m_szSite);

	//m_LoginInfoDlg.m_szSite = GetEnvPtr()->m_szSite;
	//m_LoginInfoDlg.m_szUser = GetEnvPtr()->m_szLoginID;
	//m_LoginInfoDlg.UpdateData(FALSE);

//	ShowWindow(SW_MAXIMIZE);

	m_wndStatusBar.SetPaneInfo(SBID_LED , ID_SEPARATOR, SBPS_NORMAL, 20);
	m_wndStatusBar.SetPaneInfo(SBID_HOST, ID_SEPARATOR, SBPS_NORMAL, 120);
	m_wndStatusBar.SetPaneInfo(SBID_PROC, ID_SEPARATOR, SBPS_NOBORDERS, 50);
	m_wndStatusBar.SetPaneInfo(SBID_COMM, ID_SEPARATOR, SBPS_NOBORDERS, 50);
	m_wndStatusBar.SetPaneInfo(SBID_HDD , ID_SEPARATOR, SBPS_NOBORDERS, 50);
	m_wndStatusBar.SetPaneInfo(SBID_MEM , ID_SEPARATOR, SBPS_NOBORDERS, 50);
	m_wndStatusBar.SetPaneInfo(SBID_CPU , ID_SEPARATOR, SBPS_NOBORDERS, 50);
	m_wndStatusBar.SetPaneInfo(SBID_ETC , ID_SEPARATOR, SBPS_NOBORDERS, 50);

	InitHeartbeat();
	InitAlarmSummary();

	CSiteSelectDlg::PrepareSiteList();

	// check approve-items
	CApproveDlg dlg_approve;
	APPROVE_INFO_LIST info_list;
	dlg_approve.GetApproveInfoList(info_list, GetEnvPtr()->m_szLoginID, false);
	if( info_list.GetCount() > 0 )
	{
		wait.Hide();

		dlg_approve.SetPreApproveInfoList(info_list);
		dlg_approve.DoModal();
	}

	wait.Hide();


	// ���DS : �ܸ���ϴ�E?�������� �� ȭ���� �������� �Ѵ�.
	//OpenDocument(eHostView);

	// 2012.07.11 �̼������䱸 �Ŵ��� �㶧 �������� ��E�E�E����.
	//CBulletinboardDlg dlgBulletinBoard(CBulletinboardDlg::NOTICE_POPUP);
	//dlgBulletinBoard.DoModal();

	// �ܸ���ȸ�� ��E��?�ϱ����� ȣ��Ʈ��ü�� ��EϿ?�������� �غ���.
	//AfxBeginThread(CMainFrame::BackgroundJobThread, GetSafeHwnd());
	AfxBeginThread(CMainFrame::BackgroundJobThread, this);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.cx = 1024;
	cs.cy = 768;
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

// CMainFrame diagnostics
#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

// CMainFrame message handlers
void CMainFrame::OnClose()
{
	KillTimer(TMID_HEARTBEAT);
//	KillTimer(TMID_SUMALARM);

	if(!CEventHandler::CloseWaitAllEvent(GetSafeHwnd())) return;
	m_EventManager.RemoveAllEvent();

	//CMDIFrameWnd* pFrame = DYNAMIC_DOWNCAST( CMDIFrameWnd, this );
	//CWnd* pClient = CWnd::FromHandle( pFrame->m_hWndMDIClient );

	//for(int i=0 ; i<255; i++)
	//{
	//	int id = AFX_IDM_FIRST_MDICHILD + i;

	//	CMDIChildWnd* pWnd = (CMDIChildWnd*)pClient->GetDlgItem(id);
	//	if(pWnd && pWnd->GetSafeHwnd() && ::IsWindow(pWnd->GetSafeHwnd()))
	//	{
	//		pWnd->SendMessage(WM_CLOSE);
	//	}
	//}

	// 0001414: �ܸ���ϵ�E�����κ��� �̺�Ʈ�� �޴� ȭ���� ������ ����������� ��E��?����.
	// ���̻�E�̺�Ʈ�� ���ƿ͵� ó������E�ʵ��� �Ѵ�
	//m_EventManager.SetInterfacePtr(NULL);

//	g_test = false;

	//POSITION pos = m_lsEvID.GetHeadPosition();
	//while(pos){
	//	int evId = m_lsEvID.GetNext(pos);
	//	m_EventManager.RemoveEvent(evId);
	//}

	if(m_pUserTemp) m_pUserTemp->CloseAllDocuments(TRUE);
	if(m_pHostTemp) m_pHostTemp->CloseAllDocuments(TRUE);
	if(m_pPlanoTemp) m_pPlanoTemp->CloseAllDocuments(TRUE);
	if(m_pLightTemp) m_pLightTemp->CloseAllDocuments(TRUE);
	if(m_pPlanoMgrTemp) m_pPlanoMgrTemp->CloseAllDocuments(TRUE);
	if(m_pIconTemp) m_pIconTemp->CloseAllDocuments(TRUE);
	if(m_pMonitorTemp) m_pMonitorTemp->CloseAllDocuments(TRUE);
	if(m_pSiteTemp) m_pSiteTemp->CloseAllDocuments(TRUE);
	if(m_pScreenTemp) m_pScreenTemp->CloseAllDocuments(TRUE);
	if(m_pGeoMapTemp) m_pGeoMapTemp->CloseAllDocuments(TRUE);
	if(m_pPackageTemp) m_pPackageTemp->CloseAllDocuments(TRUE);
	if(m_pNotiTemp) m_pNotiTemp->CloseAllDocuments(TRUE);
	if(m_pLMOTemp) m_pLMOTemp->CloseAllDocuments(TRUE);  //skpark lmo_solution
	if(m_pBroadcastPlanTemp) m_pBroadcastPlanTemp->CloseAllDocuments(TRUE);
	if(m_pProcessTemp) m_pProcessTemp->CloseAllDocuments(TRUE);
//	if(m_pHostLogTemp) m_pHostLogTemp->CloseAllDocuments(TRUE);
	if(m_pFaultMngTemp) m_pFaultMngTemp->CloseAllDocuments(TRUE);
	if(m_pCodeTemp) m_pCodeTemp->CloseAllDocuments(TRUE);
	if(m_pWebPageTemp) m_pWebPageTemp->CloseAllDocuments(TRUE);
	if(m_pUpdateCenterTemp) m_pUpdateCenterTemp->CloseAllDocuments(TRUE);
	if(m_pPrimaryLogTemp) m_pPrimaryLogTemp->CloseAllDocuments(TRUE);
	if(m_pTrashCanTemp) m_pTrashCanTemp->CloseAllDocuments(TRUE);

	scratchUtil::clearInstance();
	installUtil::clearInstance();
	ubcMux::clearInstance();

	CMDIFrameWnd::OnClose();
}

void CMainFrame::OnDestroy()
{
	if( m_pServerFileUpload )
	{
		m_pServerFileUpload->DestroyWindow();
		delete m_pServerFileUpload;
	}

	if( m_pHsrCommand )
	{
		m_pHsrCommand->DestroyWindow();
		delete m_pHsrCommand;
	}

	CMDIFrameWnd::OnDestroy();

	CEnviroment::Release();
	CCopModule::Release();
//	CEventManager::ClearEvent();
}

BOOL CMainFrame::OpenDocument(UINT nID, CString strTitle, CString strParam)
{
	CMDIChildWnd* pWnd = GetChildFrame(nID);

	if(!pWnd)
	{
		GetEnvPtr()->m_strDocParam = strParam;

		CDocument* pDoc = GetDocTemplate(nID)->OpenDocumentFile(NULL);
		if(!pDoc)
		{
			UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG002));
			return FALSE;
		}

		pDoc->SetTitle(strTitle);

		POSITION pos = pDoc->GetFirstViewPosition();
		CView* pView = pDoc->GetNextView(pos);
		if(!pView)
		{
			UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG003));
			return FALSE;
		}

		CFrameWnd* pFrm = pView->GetParentFrame();
		if(!pFrm)
		{
			UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG004));
			return FALSE;
		}

		pFrm->ShowWindow(SW_MAXIMIZE);
	}
	else
	{
		pWnd->MDIActivate();

		if(pWnd->IsIconic())
		{
			pWnd->ShowWindow(SW_RESTORE);
		}
	}
	return TRUE;
}

CMDIChildWnd* CMainFrame::GetChildFrame(UINT nID)
{
	CMDIFrameWnd* pFrame = DYNAMIC_DOWNCAST( CMDIFrameWnd, this );
	CWnd* pClient = CWnd::FromHandle( pFrame->m_hWndMDIClient );
	CRuntimeClass *pRC= NULL;

	switch(nID)
	{
	case eSiteView: pRC = RUNTIME_CLASS(CSiteFrame); break;
	case eHostView:	pRC = RUNTIME_CLASS(CHostFrame); break;
	case ePlanoView:	pRC = RUNTIME_CLASS(CPlanoFrame); break;
	case eLightView:	pRC = RUNTIME_CLASS(CLightFrame); break;
	case ePlanoMgrView:	pRC = RUNTIME_CLASS(CPlanoMgrFrame); break;
	case eIconView:	pRC = RUNTIME_CLASS(CIconFrame); break;
	case eMonitorView:	pRC = RUNTIME_CLASS(CMonitorFrame); break;
	case eUserView:	pRC = RUNTIME_CLASS(CUserFrame); break;
	case eScreenView: pRC = RUNTIME_CLASS(CHostScreenFrm); break;
	case eGeoMapView: pRC = RUNTIME_CLASS(CGeoMapFrm); break;
	case ePackageView: pRC = RUNTIME_CLASS(CPackageFrm); break;
	case eNotiView:pRC = RUNTIME_CLASS(CNotiMngFrm); break;
	case eLMOSolutionView:pRC = RUNTIME_CLASS(CLMOSolutionFrm); break;  //skpark lmo_solution
	case eBroadcastPlan: pRC = RUNTIME_CLASS(CBroadcastPlanFrm); break;
	case eProcessView: pRC = RUNTIME_CLASS(CProcessFrm); break;
//	case eHostLogView: pRC = RUNTIME_CLASS(CHostLogFrm); break;
	case eFaultMngView: pRC = RUNTIME_CLASS(CFaultMngFrm); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CWebCodeFrame); break;
	case eCodeView: pRC = RUNTIME_CLASS(CCodeFrm); break; // skpark code
	case eUpdateCenterView: pRC = RUNTIME_CLASS(CUpdateCenterFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CLogPackageFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CLogPowerOnOffFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CLogConnectionFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CLogFaultFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CLogDownloadFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CLogLoginFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CLogPriceFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CStatisticsFaultFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CStatisticsContentsFrame); break;
	//case eCodeView: pRC = RUNTIME_CLASS(CStatisticsInteractFrame); break;
	case ePrimaryLogView: pRC = RUNTIME_CLASS(CPrimaryLogFrame); break;
	case eTrashCanView: pRC = RUNTIME_CLASS(CTrashCanFrame); break;
	default:
		pRC = NULL;
		break;
	}

	if(!pRC) return NULL;

	for (int i=0 ; i<255; i++)
	{
		int id = AFX_IDM_FIRST_MDICHILD + i;

		CWnd *pWnd = pClient->GetDlgItem(id);

		if(pWnd)
		{
			if(pWnd->IsKindOf(pRC))
			{
				// �ѹ��� �翁E�?�� �������� �˻��� ����̹Ƿ�
				return (CMDIChildWnd*)pWnd;
			}
		}
	}

	return NULL;
}

CMultiDocTemplate* CMainFrame::GetDocTemplate(UINT nID)
{
	switch(nID)
	{
	case eSiteView:
		if(!m_pSiteTemp)
		{
			m_pSiteTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CSiteFrame), // custom MDI child frame
								//RUNTIME_CLASS(CWebSiteView));
								RUNTIME_CLASS(CSiteUserView));
			AfxGetApp()->AddDocTemplate(m_pSiteTemp);
		}
		return m_pSiteTemp;
	case eHostView:
		if(!m_pHostTemp)
		{
			m_pHostTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CHostFrame), // custom MDI child frame
								RUNTIME_CLASS(CHostView));
			AfxGetApp()->AddDocTemplate(m_pHostTemp);
		}
		return m_pHostTemp;
	case ePlanoView:
		if(!m_pPlanoTemp)
		{
			m_pPlanoTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CPlanoFrame), // custom MDI child frame
								RUNTIME_CLASS(CPlanoView));
			AfxGetApp()->AddDocTemplate(m_pPlanoTemp);
		}
		return m_pPlanoTemp;
	case eLightView:
		if(!m_pLightTemp)
		{
			m_pLightTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CLightFrame), // custom MDI child frame
								RUNTIME_CLASS(CLightView));
			AfxGetApp()->AddDocTemplate(m_pLightTemp);
		}
		return m_pLightTemp;
	case ePlanoMgrView:
		if(!m_pPlanoMgrTemp)
		{
			m_pPlanoMgrTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CPlanoMgrFrame), // custom MDI child frame
								RUNTIME_CLASS(CPlanoMgrView));
			AfxGetApp()->AddDocTemplate(m_pPlanoMgrTemp);
		}
		return m_pPlanoMgrTemp;
	case eIconView:
		if(!m_pIconTemp)
		{
			m_pIconTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CIconFrame), // custom MDI child frame
								RUNTIME_CLASS(CIconView));
			AfxGetApp()->AddDocTemplate(m_pIconTemp);
		}
		return m_pIconTemp;
	case eMonitorView:
		if(!m_pMonitorTemp)
		{
			m_pMonitorTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CMonitorFrame), // custom MDI child frame
								RUNTIME_CLASS(CMonitorView));
			AfxGetApp()->AddDocTemplate(m_pMonitorTemp);
		}
		return m_pMonitorTemp;
	case eUpdateCenterView:
		if(!m_pUpdateCenterTemp)
		{
			m_pUpdateCenterTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CUpdateCenterFrame), // custom MDI child frame
								RUNTIME_CLASS(CUpdateCenterView));
			AfxGetApp()->AddDocTemplate(m_pUpdateCenterTemp);
		}
		return m_pUpdateCenterTemp;
	case eUserView:
		if(!m_pUserTemp)
		{
			m_pUserTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CUserFrame), // custom MDI child frame
								//RUNTIME_CLASS(CWebUserView));
								RUNTIME_CLASS(CSiteUserView));
			AfxGetApp()->AddDocTemplate(m_pUserTemp);
		}
		return m_pUserTemp;
	case eScreenView:
		if(!m_pScreenTemp)
		{
			m_pScreenTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CHostScreenFrm), // custom MDI child frame
								RUNTIME_CLASS(CHostScreenView));
			AfxGetApp()->AddDocTemplate(m_pScreenTemp);
		}
		return m_pScreenTemp;
	case eGeoMapView:
		if(!m_pGeoMapTemp)
		{
			m_pGeoMapTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CGeoMapFrm), // custom MDI child frame
								RUNTIME_CLASS(CGeoMapView));
			AfxGetApp()->AddDocTemplate(m_pGeoMapTemp);
		}
		return m_pGeoMapTemp;
	case ePackageView:
		if(!m_pPackageTemp)
		{
			m_pPackageTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CPackageFrm), // custom MDI child frame
								RUNTIME_CLASS(CPackageView));
			AfxGetApp()->AddDocTemplate(m_pPackageTemp);
		}
		return m_pPackageTemp;
	case eNotiView :
		if(!m_pNotiTemp)
		{
			m_pNotiTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CNotiMngFrm), // custom MDI child frame
								RUNTIME_CLASS(CNotiMngView));
			AfxGetApp()->AddDocTemplate(m_pNotiTemp);
		}
		return m_pNotiTemp;
	case eLMOSolutionView :  //skpark lmo_solution
		if(!m_pLMOTemp)
		{
			m_pLMOTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CLMOSolutionFrm), // custom MDI child frame
								RUNTIME_CLASS(CLMOSolutionView));
			AfxGetApp()->AddDocTemplate(m_pLMOTemp);
		}
		return m_pLMOTemp;
	case eBroadcastPlan :
		if(!m_pBroadcastPlanTemp)
		{
			m_pBroadcastPlanTemp = new CMultiDocTemplate(IDR_MAINFRAME,
										RUNTIME_CLASS(CDummyDoc),
										RUNTIME_CLASS(CBroadcastPlanFrm), // custom MDI child frame
										RUNTIME_CLASS(CBroadcastPlanView));
			AfxGetApp()->AddDocTemplate(m_pBroadcastPlanTemp);
		}
		return m_pBroadcastPlanTemp;
	case eProcessView :
		if(!m_pProcessTemp)
		{
			m_pProcessTemp = new CMultiDocTemplate(IDR_MAINFRAME,
										RUNTIME_CLASS(CDummyDoc),
										RUNTIME_CLASS(CProcessFrm), // custom MDI child frame
										RUNTIME_CLASS(CProcessView));
			AfxGetApp()->AddDocTemplate(m_pProcessTemp);
		}
		return m_pProcessTemp;
	//case eHostLogView :
	//	if(!m_pHostLogTemp)
	//	{
	//		m_pHostLogTemp = new CMultiDocTemplate(IDR_MAINFRAME,
	//									RUNTIME_CLASS(CDummyDoc),
	//									RUNTIME_CLASS(CHostLogFrm), // custom MDI child frame
	//									RUNTIME_CLASS(CHostLogView));
	//		AfxGetApp()->AddDocTemplate(m_pHostLogTemp);
	//	}
	//	return m_pHostLogTemp;
	case eFaultMngView :
		if(!m_pFaultMngTemp)
		{
			m_pFaultMngTemp = new CMultiDocTemplate(IDR_MAINFRAME,
										RUNTIME_CLASS(CDummyDoc),
										RUNTIME_CLASS(CFaultMngFrm), // custom MDI child frame
										RUNTIME_CLASS(CFaultMngView));
			AfxGetApp()->AddDocTemplate(m_pFaultMngTemp);
		}
		return m_pFaultMngTemp;
	case eCodeView:
		if(!m_pCodeTemp)
		{
			m_pCodeTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CCodeFrm), //skpark code
								RUNTIME_CLASS(CCodeView)); //skpark code
								//RUNTIME_CLASS(CWebCodeFrame), // custom MDI child frame
								//RUNTIME_CLASS(CWebCodeView));
			AfxGetApp()->AddDocTemplate(m_pCodeTemp);
		}
		return m_pCodeTemp;
	case eWebPage:
		if(!m_pWebPageTemp)
		{
			m_pWebPageTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CWebPageFrame), // custom MDI child frame
								RUNTIME_CLASS(CWebPageView));
			AfxGetApp()->AddDocTemplate(m_pWebPageTemp);
		}
		return m_pWebPageTemp;
	case ePrimaryLogView:
		if(!m_pPrimaryLogTemp)
		{
			m_pPrimaryLogTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CPrimaryLogFrame), // custom MDI child frame
								RUNTIME_CLASS(CPrimaryLogView));
			AfxGetApp()->AddDocTemplate(m_pPrimaryLogTemp);
		}
		return m_pPrimaryLogTemp;
	case eTrashCanView:
		if(!m_pTrashCanTemp)
		{
			m_pTrashCanTemp = new CMultiDocTemplate(IDR_MAINFRAME,
								RUNTIME_CLASS(CDummyDoc),
								RUNTIME_CLASS(CTrashCanFrame), // custom MDI child frame
								RUNTIME_CLASS(CTrashCanView));
			AfxGetApp()->AddDocTemplate(m_pTrashCanTemp);
		}
		return m_pTrashCanTemp;
	}

	return NULL;
}

void CMainFrame::OnFileSitemanager()
{
	OpenDocument(eSiteView, LoadStringById(IDS_SITEVIEW_STR001), _T("Site"));
}

void CMainFrame::OnFileHostmanager()
{
	OpenDocument(eHostView, LoadStringById(IDS_HOSTVIEW_STR001));
}

void CMainFrame::OnFilePlano()
{
	OpenDocument(ePlanoView, LoadStringById(IDS_PLANOVIEW_STR001));
}


void CMainFrame::OnFileUsermanager()
{
	OpenDocument(eUserView, LoadStringById(IDS_USERVIEW_STR001), _T("User"));
}

void CMainFrame::OnFileScreenManager()
{
	OpenDocument(eScreenView, LoadStringById(IDS_HOSTSCREENVIEW_STR001));
}

void CMainFrame::OnPackageManager()
{
	OpenDocument(ePackageView, LoadStringById(IDS_PACKAGEVIEW_STR001));

	//CPackageMngDlg dlg;
	//if(CEnviroment::GetObject()->m_Authority = CCopModule::eSiteAdmin)
	//	dlg.Set("*", CEnviroment::GetObject()->m_szLoginID, CEnviroment::GetObject()->m_szPassword);
	//else
	//	dlg.Set(CEnviroment::GetObject()->m_szSite, CEnviroment::GetObject()->m_szLoginID, CEnviroment::GetObject()->m_szPassword);
	//dlg.DoModal();
}

void CMainFrame::OnNotification()
{
	OpenDocument(eNotiView, LoadStringById(IDS_NOTIMNGVIEW_STR001));
}

void CMainFrame::OnFileBroadcastPlanView()
{
	OpenDocument(eBroadcastPlan, LoadStringById(IDS_PLANVIEW_STR001));
}

void CMainFrame::OnToolUBCStudio()
{
	TraceLog(("Run %s",UBCSTUDIOEEEXE_STR));
/*	unsigned long lPid = scratchUtil::getInstance()->getPid(UBCSTUDIOEEEXE_STR);
	HWND hWnd = scratchUtil::getInstance()->getWHandle(lPid);
	if(hWnd){
		CWnd* pWnd = CWnd::FromHandle(hWnd);
		pWnd->ActivateTopParent();

		if(pWnd->IsIconic()){
			pWnd->ShowWindow(SW_RESTORE);
		}
		return;
	}
*/
//	UbcMessageBox(GetEnvPtr()->m_szSite);

	CString szPath, szParam;
	szPath.Format("\"%s%s\"", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
	szParam.Format("+siteid=%s +userid=%s +passwd=%s", GetEnvPtr()->m_szSite, GetEnvPtr()->m_szLoginID, GetEnvPtr()->m_szPassword);

//	scratchUtil* pUtil = scratchUtil::getInstance();
//	pUtil->createProcess(UBCSTUDIOEEEXE_STR, szParam, szPath, false);
	HINSTANCE nRet = ShellExecute(NULL, NULL, UBCSTUDIOEEEXE_STR, szParam, szPath, SW_SHOWNORMAL);
}

void CMainFrame::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CMDIFrameWnd::OnWindowPosChanging(lpwndpos);

	// TODO: Add your message handler code here
	int MIN_WIDTH = 700;
	int MIN_HEIGHT = 600;

	if(MIN_WIDTH > lpwndpos->cx)
		lpwndpos->cx = MIN_WIDTH;
	if(MIN_HEIGHT > lpwndpos->cy)
		lpwndpos->cy = MIN_HEIGHT;
}

void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	CWnd::OnTimer(nIDEvent);

	switch(nIDEvent)
	{
//	case TMID_SUMALARM : SetAlarmSummary(); break;
	case TMID_HEARTBEAT: SetHeartbeat();    break;
	}
}

int CMainFrame::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
		case IEH_UNKOWN:
		{
			cciEvent* ev = (cciEvent*)lParam;
			if(!ev)	break;
			ciString strType = ev->getEventType();

			if(strType == "heartbeat")
			{
				m_dwHeartbeat = GetTickCount();
				TRACE("InvokeEvent heartbeat : %d\n", m_dwHeartbeat);
			}
			break;
		}
		case IEH_SUMALAM:
			if(lParam)
			{
				SAlarmSummary stAlarmSummary = *((SAlarmSummary*)lParam);
				SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_PROC, (LPARAM)(LPCTSTR)ToString(stAlarmSummary.nProcessDown));
				SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_COMM, (LPARAM)(LPCTSTR)ToString(stAlarmSummary.nCommunicationFail));
				SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_HDD , (LPARAM)(LPCTSTR)ToString(stAlarmSummary.nHddOverload));
				SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_MEM , (LPARAM)(LPCTSTR)ToString(stAlarmSummary.nMemOverload));
				SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_CPU , (LPARAM)(LPCTSTR)ToString(stAlarmSummary.nCpuOverload));
				SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_ETC , (LPARAM)(LPCTSTR)ToString(stAlarmSummary.nEtcAlarm));
			}
			break;
		default:
			break;
	}

	return 0;
}

UINT CMainFrame::WaitForWindowCreated(LPVOID pParam)
{
	HWND hWnd = (HWND)pParam;
	if(hWnd)// && ::IsWindow(hWnd))
	{
		while(WaitForSingleObject(hWnd, 0) == WAIT_TIMEOUT);
		//while(!::IsWindowVisible(hWnd)) Sleep(100);
		::PostMessage(hWnd, WM_WAIT_FOR_CREATED, NULL, NULL);
	}

	return 0;
}

void CMainFrame::OnAutoUpdate()
{
	GetEnvPtr()->SetAutoUpdate(!GetEnvPtr()->GetAutoUpdate());
}

void CMainFrame::SetStatusBarPaneText(int nIndex, LPCTSTR lpszNewText)
{
	if(!::IsWindow(m_wndStatusBar.GetSafeHwnd())) return;
	m_wndStatusBar.SetPaneText(nIndex, lpszNewText);
	//m_wndStatusBar.GetStatusBarCtrl().SetText(lpszNewText, nIndex, 0);
}

LRESULT CMainFrame::OnStatusBarPaneText(WPARAM wParam, LPARAM lParam)
{
	SetStatusBarPaneText((int)wParam, (LPCTSTR)lParam);
	return 0;
}

void CMainFrame::OnFileHostProcess()
{
	OpenDocument(eProcessView, LoadStringById(IDS_PROCESSVIEW_STR001));
}

//void CMainFrame::OnFileHostLog()
//{
//	OpenDocument(eHostLogView, LoadStringById(IDS_HOSTLOGVIEW_STR001));
//}

void CMainFrame::OnFileFaultMng()
{
	OpenDocument(eFaultMngView, LoadStringById(IDS_FAULTMNGVIEW_STR001));
}

void CMainFrame::OnAppBulletinboard()
{
	// ���DS : �������� �� ȭ���� �������� �Ѵ�.
	//CBulletinboardDlg dlgBulletinBoard(CBulletinboardDlg::NOTICE_LIST);
	//dlgBulletinBoard.DoModal();
	OpenDocument(eWebPage, LoadStringById(IDS_APP_NOTICE_LIST_STR001), GetUrl(_T("NOTICE_LIST")));
}

UINT CMainFrame::BackgroundJobThread(LPVOID pParam)
{
	//CString strEntity;
	//strEntity.Format("PM=*/Site=%s/Host=*", GetEnvPtr()->m_szSite);
	//CCopModule::GetObject()->DummyCall("bulkget", strEntity, "hostId is null");

	//HWND hMainWnd = (HWND)pParam;
	//while(WaitForSingleObject(hMainWnd, 0) == WAIT_TIMEOUT);

	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// ������ �ð��� üũ�ϰ� ����ȭ �Ѵ�.
	CMainFrame* mainFrame = (CMainFrame*)pParam;
	mainFrame->ServerTimeCheck();

	while(g_test)
	{
		//// �˶��̺�Ʈ�� ������ �߻���Ű���� �Ͽ� �˶������� ������Ʈ�ϵ��� �Ѵ�.
		CCopModule::GetObject()->RequestAlarmSummary();
	}

	::CoUninitialize();

	return 0;
}

void CMainFrame::UpdateFilterData(CWnd* pFromWnd)
{
	CMDIFrameWnd* pFrame = DYNAMIC_DOWNCAST( CMDIFrameWnd, this );
	CWnd* pClient = CWnd::FromHandle( pFrame->m_hWndMDIClient );

	for (int i=0 ; i<255; i++)
	{
		int id = AFX_IDM_FIRST_MDICHILD + i;

		CMDIChildWnd* pWnd = (CMDIChildWnd*)pClient->GetDlgItem(id);
		if(pWnd && pWnd->GetActiveView() && pWnd->GetActiveView() != pFromWnd)
		{
			pWnd->GetActiveView()->SendMessage(WM_FILTER_HOST_CHANGE, 0, 0);
		}
	}
}

void CMainFrame::OnAppCodeMng()
{
	//OpenDocument(eCodeView, LoadStringById(IDS_CODEVIEW_STR001));
	// skpark code
	BOOL ret_val = OpenDocument(eCodeView, LoadStringById(IDS_CODEVIEW_STR001), _T("CODE"));
}

void CMainFrame::OnLMOSolutionMenu() //skpark lmo_solution
{
	BOOL ret_val = OpenDocument(eLMOSolutionView, LoadStringById(IDS_LMO_SOLUTIONVIEW_STR001), _T("LMO_SOLUTION"));
	/*
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(eLMOSolutionView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 3);
			}
		}
	}
	*/
}

void CMainFrame::InitAlarmSummary()
{
	m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_PROC, (HICON) LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_PROCESS_DOWN_LED), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR));
	m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_COMM, (HICON) LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_COMM_FAIL_LED   ), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR));
	m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_HDD , (HICON) LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_HDD_OVERLOAD_LED), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR));
	m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_MEM , (HICON) LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_MEM_OVERLOAD_LED), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR));
	m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_CPU , (HICON) LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_CPU_OVERLOAD_LED), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR));
	m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_ETC , (HICON) LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ETC_ALARM_LED   ), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR));

	m_lsEvID.AddTail(m_EventManager.AddEvent("* alarmSummary"));

//	SetAlarmSummary();
	m_wndStatusBar.SetPaneText(SBID_PROC, _T("0"));
	m_wndStatusBar.SetPaneText(SBID_COMM, _T("0"));
	m_wndStatusBar.SetPaneText(SBID_HDD , _T("0"));
	m_wndStatusBar.SetPaneText(SBID_MEM , _T("0"));
	m_wndStatusBar.SetPaneText(SBID_CPU , _T("0"));
	m_wndStatusBar.SetPaneText(SBID_ETC , _T("0"));

	// �˶��̺�Ʈ�� ������ �߻���Ű���� �Ͽ� �˶������� ������Ʈ�ϵ��� �Ѵ�.
	CCopModule::GetObject()->RequestAlarmSummary();

//	SetTimer(TMID_SUMALARM, 1000, NULL);
}

//void CMainFrame::SetAlarmSummary()
//{
//	m_wndStatusBar.SetPaneText(SBID_PROC, ToString(m_stAlarmSummary.nProcessDown));
//	m_wndStatusBar.SetPaneText(SBID_COMM, ToString(m_stAlarmSummary.nCommunicationFail));
//	m_wndStatusBar.SetPaneText(SBID_HDD , ToString(m_stAlarmSummary.nHddOverload));
//	m_wndStatusBar.SetPaneText(SBID_MEM , ToString(m_stAlarmSummary.nMemOverload));
//	m_wndStatusBar.SetPaneText(SBID_CPU , ToString(m_stAlarmSummary.nCpuOverload));
//	m_wndStatusBar.SetPaneText(SBID_ETC , ToString(m_stAlarmSummary.nEtcAlarm));
//}

void CMainFrame::OnLogPackageChng()
{
	// skpark web->client ����ÁE��Ű��E����ᰁE�α� !!!
	//OpenDocument(eWebPage, LoadStringById(IDS_LOG_PACKAGE_STR001), GetUrl(_T("LOG_PACKAGE_CHNG")));

	//CLogMainDlg dlg;
	////dlg.SetParam(Info.hostId, Info.hostName);
	//dlg.SetDefaultTab(1);  // �ι�°��
	//dlg.DoModal();
	BOOL ret_val = OpenDocument(ePrimaryLogView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("PrimaryLog"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(ePrimaryLogView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 1);
			}
		}
	}
}

void CMainFrame::OnLogPowerOnoff()
{
	// skpark web->client �Ŀ�OnOff �α� !!!
	//OpenDocument(eWebPage, LoadStringById(IDS_LOG_POWER_ONOFF_STR001), GetUrl(_T("LOG_POWER_ONOFF")));
	//CLogMainDlg dlg;
	////dlg.SetParam(Info.hostId, Info.hostName);
	//dlg.SetDefaultTab(2);  // ����°��
	//dlg.DoModal();
	BOOL ret_val = OpenDocument(ePrimaryLogView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("PrimaryLog"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(ePrimaryLogView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 2);
			}
		}
	}
}

void CMainFrame::OnLogConnection()
{
	// skpark web->client �Ŀ�OnOff �α� !!!
	//OpenDocument(eWebPage, LoadStringById(IDS_LOG_CONNECTION_STR001), GetUrl(_T("LOG_CONNECTION")));
	//CLogMainDlg dlg;
	////dlg.SetParam(Info.hostId, Info.hostName);
	//dlg.SetDefaultTab(3);  // �׹�°��
	//dlg.DoModal();
	BOOL ret_val = OpenDocument(ePrimaryLogView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("PrimaryLog"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(ePrimaryLogView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 3);
			}
		}
	}
}

void CMainFrame::OnLogFault()
{
	BOOL ret_val = OpenDocument(ePrimaryLogView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("PrimaryLog"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(ePrimaryLogView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 8); // 9��° ��
			}
		}
	}

	
	//OpenDocument(eWebPage, LoadStringById(IDS_LOG_FAULT_STR001), GetUrl(_T("LOG_FAULT")));
}

void CMainFrame::OnLogDownload()
{
	BOOL ret_val = OpenDocument(ePrimaryLogView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("PrimaryLog"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(ePrimaryLogView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 4);  // 5��°��
			}
		}
	}
	//OpenDocument(eWebPage, LoadStringById(IDS_LOG_DOWNLOAD_STR001), GetUrl(_T("LOG_DOWNLOAD")));
}

void CMainFrame::OnLogLogin()
{
	BOOL ret_val = OpenDocument(ePrimaryLogView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("PrimaryLog"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(ePrimaryLogView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 5);
			}
		}
	}
	//OpenDocument(eWebPage, LoadStringById(IDS_LOG_LOGIN_STR001), GetUrl(_T("LOG_LOGIN")));
}

void CMainFrame::OnStatisticsFault()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_FAULT_STR001), GetUrl(_T("STATISTICS_FAULT")));
}

void CMainFrame::OnStatisticsContents()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_CONTENTS_STR001), GetUrl(_T("STATISTICS_CONTENTS_BROD")));
}

void CMainFrame::OnStatisticsInteract()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_INTERACT_STR001), GetUrl(_T("STATISTICS_INTERACT")));
}

CString CMainFrame::GetUrl(CString strKey)
{
	//GUBUN       : UBC �� �Ƚ��� �� �Դϴ�.
	//SITE           : siteId �Դϴ�.
	//ID               : �α��� ���̵�E�Դϴ�.
	//PW             : �α��� ��ȣ �Դϴ�. 
	//PROGRAM  :  ������ �� ���α׷��� �ּ��Դϴ�.
	//
	//
	//http://211.232.57.219/default.aspx?GUBUN=UBC&SITE=HYUNDAI&ID=HYUNDAImanager&PW=HYUNDAImanager&PROGRAM=community/notice_list.aspx
	//
	//
	//PROGRAM �� ���̥ ���� ������E�����ϴ�.
	//community/notice_list.aspx          <- �������� 
	//community/program_list.aspx       <- ���α׷� �ٿ��ε�E
	//community/manual_list.aspx        <- �Ŵ���E�ٿ��ε�E
	//community/video_list.aspx           <- ������E�Ŵ���E
	//community/contents.aspx            <- ��������E?
	//
	//manager/code.aspx                     <- ��ŁEڵ封E?
	//manager/user.aspx                      <- �翁Eڰ�E?
	//manager/org.aspx                        <- ������E?

	//Statistics/fault_terminal.aspx        ��� ŁE�E
	//Statistics/contents_brod.aspx      ����ÁE��� ŁE�E
	//Statistics/inter_use.aspx             ��ȣ�ۿ�E�̿�EŁE�E

	//log/schedule.aspx           ��������Ű��E����E�α�
	//log/power.aspx                ��E?N/OFF �α�
	//log/connection.aspx         ������� �α�
	//log/fault.aspx                   ��� �α�
	//log/download.aspx          ����ÁE��?E�α�
	//log/login.aspx               �α��� �α�
	//log/price.aspx             ���ܰ����α�               (���ܰ����α״� ��Ƹ� �ش��մϴ�.)

	CString strEntry = _T("UNKNOWN");
	if     (CEnviroment::eKIA     == GetEnvPtr()->m_Customer) strEntry = _T("KIA");
	else if(CEnviroment::eHYUNDAI == GetEnvPtr()->m_Customer) strEntry = _T("HYUNDAI");
	else if(CEnviroment::eKMNL    == GetEnvPtr()->m_Customer) strEntry = _T("KMNL");
	else if(CEnviroment::eKMCL    == GetEnvPtr()->m_Customer) strEntry = _T("KMCL");
	else if(!GetEnvPtr()->m_strCustomer.IsEmpty()) strEntry = GetEnvPtr()->m_strCustomer;

	TCHAR szBuf[2048] = {0};
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, UBCURL_INI);
	GetPrivateProfileString(strEntry, _T("BASE"), "", szBuf, 2048, szPath); CString strBaseUrl(szBuf);
	GetPrivateProfileString(strEntry, strKey    , "", szBuf, 2048, szPath); CString strSubUrl(szBuf);

	CString strUrl;
	if(strBaseUrl.IsEmpty()) strUrl = _T("about:blank");
	else
	{
		strUrl.Format(_T("%s?GUBUN=%s&SITE=%s&ID=%s&PW=%s&PROGRAM=%s")
					, strBaseUrl
					, AsciiToBase64ForWeb(_T("UBC"))
					, AsciiToBase64ForWeb(GetEnvPtr()->m_szSite)
					, AsciiToBase64ForWeb(GetEnvPtr()->m_szLoginID)
					, AsciiToBase64ForWeb(GetEnvPtr()->m_szPassword)
					, AsciiToBase64ForWeb(strSubUrl)
					);
	}

	return strUrl;
}

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	//if(pMsg->message == WM_LBUTTONDOWN && m_wndStatusBar.GetSafeHwnd() == pMsg->hwnd)
	if(pMsg->message == WM_MOUSEMOVE && m_wndStatusBar.GetSafeHwnd() == pMsg->hwnd)
	{
		//CPoint pt = pMsg->pt;
		//m_wndStatusBar.ScreenToClient(&pt);

		//CRect rcStatus;
		//m_wndStatusBar.GetClientRect(&rcStatus);
		//int nRight = rcStatus.Width() - ::GetSystemMetrics(SM_CXVSCROLL);

		//int nPaneCnt = sizeof(indicators)/sizeof(UINT);
		//for(int i=(nPaneCnt-1); i>=0 ;i--)
		//{
		//	CString strParam;
		//	UINT nID = 0;
		//	UINT nStyle = 0;
		//	int nWidth = 0;

		//	m_wndStatusBar.GetPaneInfo(i, nID, nStyle, nWidth);

		//	strParam.Format("%d [%d:%d](%d)", pt.x, (nRight-nWidth), nRight, nWidth);
		//	m_wndStatusBar.SetPaneText(SBID_TEXT, strParam);
		//	if((nRight-nWidth) < pt.x && pt.x < nRight)
		//	{
		//		strParam.AppendFormat("->%d", i);
		//		m_wndStatusBar.SetPaneText(SBID_TEXT, strParam);
		//		break;
		//	}

		//	nRight -= nWidth;
		//}
	}

	return __super::PreTranslateMessage(pMsg);
}

// 0001443: �Ŵ��� ��E��������E?��� ����E����
//void CMainFrame::OnFileContentsMng()
//{
//	OpenDocument(eWebPage, LoadStringById(IDS_FILES_CONTENTSMNG_STR001), GetUrl(_T("CONTENTS_MANAGER")));
//}

void CMainFrame::OnWebProgramDown()
{
	OpenDocument(eWebPage, LoadStringById(IDS_WEB_PROGRAM_DOWN_STR001), GetUrl(_T("PROGRAM_DOWNLOAD")));
}

void CMainFrame::OnWebManualDown()
{
	OpenDocument(eWebPage, LoadStringById(IDS_WEB_MANUAL_DOWN_STR001), GetUrl(_T("MANUAL_DOWNLOAD")));
}

void CMainFrame::OnWebVideoManual()
{
	OpenDocument(eWebPage, LoadStringById(IDS_WEB_VIDEO_MANUAL_STR001), GetUrl(_T("VIDEO_MANUAL")));
}

void CMainFrame::InitMenuAuth()
{
	// ���������� �����´�
	InitAuth(GetEnvPtr()->m_szLoginID, GetEnvPtr()->m_strCustomer);

	//CMenu* pMainMenu = GetMenu();

	//m_bAutoMenuEnable = false;

	////if(!IsAuth(_T("HQRY"))) pMainMenu->RemoveMenu(ID_FILE_HOSTMANAGER, MF_BYCOMMAND);
	//pMainMenu->EnableMenuItem(ID_FILE_HOSTMANAGER, (IsAuth(_T("HQRY"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));

	//CToolBarCtrl& ctrToolBar = m_wndToolBar.GetToolBarCtrl();

	//if(!IsAuth(_T("HQRY")))
	//{
	//	//ctrToolBar.EnableButton( ctrToolBar.CommandToIndex(ID_FILE_HOSTMANAGER), FALSE ); // ID_FILE_HOSTMANAGER
	//	ctrToolBar.DeleteButton( ctrToolBar.CommandToIndex(ID_FILE_HOSTMANAGER) ); // ID_FILE_HOSTMANAGER
	//}

	//ctrToolBar.AutoSize();
}

void CMainFrame::OnUpdateMenuAuth(CCmdUI *pCmdUI)
{
//HQRY	�ܸ�-�����ȸ
//HDEL	�ܸ�-����
//HMOD	�ܸ�-����
//HREG	�ܸ�-����
//HOFF	�ܸ�-����E
//HRBT	�ܸ�-�ٽý���
//HBRS	�ܸ�-��E�E��簡�?
//HGMD	�ܸ�-�׷�E?�E
//HCLR	�ܸ�-�ܸ�û��
//HEXL	�ܸ�-��� ExcelExport
//HCRS	�ܸ�-����������E?
//HVNC	�ܸ�-��������
//HDNV	��?E�ᰁE����E
//HUSE	�ܸ����-�̻翁E
//HDTL	�ܸ����-�Ӽ�
//HSCR	�ܸ���ũ������ȸ
//HPNO	�ܸ�-��Ű������
//PQRY	��Ű��E�����ȸ
//PREG	��Ű��E�߰�
//PDEL	��Ű��E����
//PMOD	��Ű��E����
//PCHG	��Ű��E��Eú?�E
//PRSV	��Ű��E����E
//PSAV	��Ű��E����E
//PSND	��Ű��E��E?
//CQRY	��������E?��?
//EQRY	��ް���E�����ȸ
//EREG	��ް���E�߰�
//EDEL	��ް���E����
//EMOD	��ް���E����
//BQRY	��۰�ȹ-�����ȸ
//BREG	��۰�ȹ-�߰�
//BDEL	��۰�ȹ-����
//BMOD	��۰�ȹ-����E
//MPQR	���μ�������-��ȸ
//LQRY	�α�-��ȸ
//FQRY	���-�����ȸ
//FDEL	���-����
//SQRY	ŁE�E��ȸ
//SBRD	�������װ�E?
//SGRP	������E?
//SUSR	�翁Eڰ�E?
//SCOD	�ڵ封E?

	switch(pCmdUI->m_nID)
	{
	case ID_FILE_HOSTMANAGER    : pCmdUI->Enable(IsAuth(_T("HQRY"))); break;
	case ID_FILE_CACHESERVER    : pCmdUI->Enable(IsAuth(_T("HQRY"))); break;
	case ID_FILE_PLANO    : pCmdUI->Enable(IsAuth(_T("HQRY"))); break;
	case ID_FILE_LIGHTLIST    : pCmdUI->Enable(IsAuth(_T("HQRY"))); break;
	case ID_FILE_MONITORLIST    : pCmdUI->Enable(IsAuth(_T("HQRY"))); break;
	case ID_FILE_PACKAGEMANAGER : pCmdUI->Enable(IsAuth(_T("PQRY"))); break;
//	case ID_FILE_CONTENTS_MNG   : pCmdUI->Enable(IsAuth(_T("CQRY"))); break;	// 0001443: �Ŵ��� ��E��������E?��� ����E����
	case ID_FILE_NOTIFICATION   : pCmdUI->Enable(IsAuth(_T("TQRY"))); break;
	case ID_FILE_BROADCAST_PLAN : pCmdUI->Enable(IsAuth(_T("BQRY"))); break;
	case ID_FILE_SCREENMANGER   : pCmdUI->Enable(IsAuth(_T("HSCR"))); break;
	case ID_FILE_GEOMAP			: pCmdUI->Enable(IsAuth(_T("HSCR"))); break;
	case ID_FILE_HOST_PROCESS   : pCmdUI->Enable(IsAuth(_T("MQRY"))); break;
	case ID_FILE_FAULT_MNG      : pCmdUI->Enable(IsAuth(_T("FQRY"))); break;
	case ID_LOG_PACKAGE_APPLY   :
	case ID_LOG_PACKAGE_CHNG    : 
	case ID_LOG_POWER_ONOFF     : 
	case ID_LOG_CONNECTION      : 
	case ID_LOG_FAULT           : 
	case ID_LOG_DOWNLOAD        : 
	case ID_LOG_LOGIN           : 
	case ID_LOG_BROADCAST_PLAN  : 
	case ID_LOG_PLAYER_ERROR    : pCmdUI->Enable(IsAuth(_T("LQRY"))); break;
	case ID_STATISTICS_FAULT    : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_STATISTICS_CONTENTS : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_STATISTICS_INTERACT : pCmdUI->Enable(IsAuth(_T("SQRY"))); break;
	case ID_STATISTICS_BROADCASTPLAN : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_STATISTICS_PACKAGE_REG : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_STATISTICS_PACKAGE_USE : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_STATISTICS_CONTENTS_REG : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_STATISTICS_CONTENTS_USE : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_STATISTICS_CONTENTS_ACC : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_STATISTICS_MSG_USE : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_STATISTICS_APPLY    : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_APP_BULLETINBOARD   : pCmdUI->Enable(IsAuth(_T("SBRD"))); break;
	case ID_FILE_SITEMANAGER    : pCmdUI->Enable(IsAuth(_T("SGRP"))); break;
	case ID_FILE_USERMANAGER    : pCmdUI->Enable(IsAuth(_T("SUSR"))); break;
	case ID_APP_CODEMNG         : pCmdUI->Enable(IsAuth(_T("SCOD"))); break;
	case ID_LMO_SOLUTION_MENU   : pCmdUI->Enable(IsAuth(_T("SCOD"))); break; //skpark lmo_solution
	case ID_FILE_UPDATE_CENTER  : pCmdUI->Enable(IsAuth(_T("UQRY"))); break;
	case ID_AUTO_UPDATE         : pCmdUI->SetCheck(GetEnvPtr()->GetAutoUpdate()); break;
	case ID_USE_AXIS2         : pCmdUI->SetCheck(GetEnvPtr()->GetServerOS()); break;
	case ID_ALPHA_UPDATE         : pCmdUI->SetCheck(GetEnvPtr()->GetAlphaUpdate()); break;
	case ID_COLUMN_NEXT_SCHEDULE : pCmdUI->SetCheck(GetEnvPtr()->GetColumnNextSchedule()); break;
	case ID_WEB_PROGRAM_DOWN : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_WEB_MANUAL_DOWN : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_WEB_VIDEO_MANUAL : pCmdUI->Enable(IsHYUNDAI_KIA()); break;
	case ID_WEB_WELCOMEBOARD : pCmdUI->Enable(IsWIA()); break;

	case ID_UPLOAD_FIRMWARE: pCmdUI->Enable(IsKPOST()); break;
	case ID_UPLOAD_FIRMWARE2: pCmdUI->Enable(IsKPOST()); break;
	case ID_UPLOAD_UBC_PLAYER_APP: pCmdUI->Enable(IsKPOST()); break;
	case ID_UPLOAD_UBC_PLAYER_APP2: pCmdUI->Enable(IsKPOST()); break;
	case ID_UPLOAD_VACCINE_APP: pCmdUI->Enable(IsKPOST()); break;
	case ID_UPLOAD_VACCINE_DB: pCmdUI->Enable(IsKPOST()); break;
	case ID_UPLOAD_MANAGER_UPDATE: pCmdUI->Enable(IsKPOST()); break;
	case ID_UPLOAD_SERVER_UPDATE: pCmdUI->Enable(IsKPOST()); break;
	case ID_APP_DGROUP: pCmdUI->Enable(IsKPOST()); break;

	}

	if(pCmdUI->m_nID == ID_FILE_LIGHTLIST) {
		pCmdUI->Enable(false);
	}

}

void CMainFrame::OnFileUpdateCenter()
{
	OpenDocument(eUpdateCenterView, LoadStringById(IDS_UPDATECENTER_STR001));
}

void CMainFrame::InitHeartbeat()
{
	int evId = m_EventManager.AddEvent("* heartbeat");
	if(evId >= 0)
	{
		m_lsEvID.AddTail(evId);
		SetTimer(TMID_HEARTBEAT, 10000, NULL);
		m_dwHeartbeat = GetTickCount();
		TRACE("Start heartbeat : %d\n", m_dwHeartbeat);

		m_wndStatusBar.SetPaneText(SBID_TEXT, "");
		//m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_LED, (HICON) LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_GREEN_LED), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR));
		m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_LED, m_hLedGreen);

		m_EventManager.SetServerStatus(true);
	}
	else
	{
		m_wndStatusBar.SetPaneText(SBID_TEXT, "");
		//m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_LED, (HICON) LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_RED_LED), IMAGE_ICON, 16,16,LR_DEFAULTCOLOR));
		m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_LED, m_hLedRed);

		m_EventManager.SetServerStatus(false);
	}
}

void CMainFrame::SetHeartbeat()
{
	TRACE("Check heartbeat : %d\n", GetTickCount()-m_dwHeartbeat);
#ifdef _DEBUG
	if(GetTickCount() - m_dwHeartbeat > 70000) // heartbeat(60��) + Ÿ�̸ӽð�(10��)
#else
	if(GetTickCount() - m_dwHeartbeat > 90000) // heartbeat(60��) + Ÿ�̸ӽð�(10��) + 20��
#endif
	{
//			KillTimer(TMID_HEARTBEAT);
//			UbcMessageBox(LoadStringById(IDS_MAINFRAME_MSG005),MB_ICONERROR);
//			SetTimer(TMID_HEARTBEAT, 10000, NULL);
//			m_dwHeartbeat = GetTickCount();

		TRACE("Disconnected\n");
		m_wndStatusBar.SetPaneText(SBID_TEXT, LoadStringById(IDS_MAINFRAME_MSG005));
		//m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_LED, (HICON) LoadImage(AfxGetInstanceHandle()
		//																	, MAKEINTRESOURCE(IDI_YELLOW_LED)
		//																	, IMAGE_ICON
		//																	, 16,16
		//																	,LR_DEFAULTCOLOR));
		m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_LED, m_hLedYellow);

		m_EventManager.SetServerStatus(false);
	}
	else
	{
		TRACE("Connected\n");
		m_wndStatusBar.SetPaneText(SBID_TEXT, "");
		//m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_LED, (HICON) LoadImage(AfxGetInstanceHandle()
		//																	, MAKEINTRESOURCE(IDI_GREEN_LED)
		//																	, IMAGE_ICON
		//																	, 16,16
		//																	,LR_DEFAULTCOLOR));
		m_wndStatusBar.GetStatusBarCtrl().SetIcon(SBID_LED, m_hLedGreen);

		m_EventManager.SetServerStatus(true);
	}
}

void CMainFrame::OnLogPackageApply()
{
	// skpark web->client ����ÁE��Ű��E������� �α� --> ����ÁE��Ű��E����E�̷� ����Eȭ������ ��ü
	//OpenDocument(eWebPage, LoadStringById(IDS_LOG_PACKAGE_STR002), GetUrl(_T("LOG_PACKAGE_APPLY")));

	//CLogMainDlg dlg;
	////dlg.SetParam(Info.hostId, Info.hostName);
	//dlg.SetDefaultTab(0);  // ù��°��
	//dlg.DoModal();
	BOOL ret_val = OpenDocument(ePrimaryLogView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("PrimaryLog"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(ePrimaryLogView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 0);
			}
		}
	}
/*
	CPackageChngLogDlg dlg;
	//dlg.SetParam(Info.hostId, Info.hostName);
	dlg.DoModal();
*/
}

void CMainFrame::OnLogBroadcastPlan()
{
	BOOL ret_val = OpenDocument(ePrimaryLogView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("PrimaryLog"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(ePrimaryLogView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 6); // 7��° Tab
			}
		}
	}

	//OpenDocument(eWebPage, LoadStringById(IDS_LOG_BROADCASTPLAN_STR001), GetUrl(_T("LOG_BROADCASTPLAN")));
}

void CMainFrame::OnLogPlayerError()
{
	BOOL ret_val = OpenDocument(ePrimaryLogView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("PrimaryLog"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(ePrimaryLogView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 7); // 8��° Tab
			}
		}
	}
	
	//OpenDocument(eWebPage, LoadStringById(IDS_LOG_PLAYER_STR001), GetUrl(_T("LOG_PLAYER")));
}

void CMainFrame::OnStatisticsBroadcastplan()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_BP_STR001), GetUrl(_T("STATISTICS_BROADCASTPLAN")));
}

void CMainFrame::OnStatisticsPackageReg()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_PACKAGE_STR001), GetUrl(_T("STATISTICS_PKG_REG")));
}

void CMainFrame::OnStatisticsPackageUse()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_PACKAGE_STR002), GetUrl(_T("STATISTICS_PKG_USE")));
}

void CMainFrame::OnStatisticsContentsReg()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_CONTENTS_STR002), GetUrl(_T("STATISTICS_CONTENTS_REG")));
}

void CMainFrame::OnStatisticsContentsUse()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_CONTENTS_STR003), GetUrl(_T("STATISTICS_CONTENTS_USE")));
}

void CMainFrame::OnStatisticsContentsAcc()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_CONTENTS_STR004), GetUrl(_T("STATISTICS_CONTENTS_ACC")));
}

void CMainFrame::OnStatisticsMsgUse()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_MSG_USE_STR001), GetUrl(_T("STATISTICS_MSG_USE")));
}

void CMainFrame::OnStatisticsApply()
{
	OpenDocument(eWebPage, LoadStringById(IDS_STATISTICS_APPLY_STR001), GetUrl(_T("STATISTICS_APPLY")));
}

LRESULT CMainFrame::OnCloseDialog(WPARAM wParam, LPARAM lParam)
{
	CDialog* pDlg = (CDialog*)wParam;
	if(pDlg && pDlg->IsKindOf(RUNTIME_CLASS(CDialog)))
	{
		delete pDlg;
	}

	return 0;
}

void CMainFrame::OnAlphaUpdate()
{
	// TODO: ���⿡ ���� ó����E�ڵ带 �߰��մϴ�.
	GetEnvPtr()->SetAlphaUpdate(!GetEnvPtr()->GetAlphaUpdate());

}
void CMainFrame::OnColumnNextSchedule()
{
	// TODO: ���⿡ ���� ó����E�ڵ带 �߰��մϴ�.
	GetEnvPtr()->SetColumnNextSchedule(!GetEnvPtr()->GetColumnNextSchedule());

}

void CMainFrame::OnFileLightlist()
{
	OpenDocument(eLightView, LoadStringById(IDS_LIGHTVIEW_STR001));
}
void CMainFrame::OnFilePlanoMgr()
{
	OpenDocument(ePlanoMgrView, LoadStringById(IDS_PLANOMGRVIEW_STR017));
}
void CMainFrame::OnFileIconlist()
{
	OpenDocument(eIconView, LoadStringById(IDS_ICONVIEW_STR004));
}
void CMainFrame::OnFileMonitorlist()
{
	OpenDocument(eMonitorView, LoadStringById(IDS_MONITORVIEW_STR001));
}

void CMainFrame::OnFileCacheserver()
{
	CCacheServerSelectDlg dlg(GetEnvPtr()->m_strCustomer);

	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;

	dlg.DoModal();
}

UINT CMainFrame::StrongSecurityThread(LPVOID param)
{
	TraceLog(("StrongSecurityThread"));
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	char buf[1024];
	CString szPath2, szTemp;
	szPath2.Format("%s%sdata\\%s", szDrive, szPath, UBCVARS_INI);

	::GetPrivateProfileString("ROOT", "SEC", "", buf, 1024, szPath2);
	szTemp = buf;
	if(szTemp != "S"){
		return 0;
	}

	CMainFrame* pDlg = (CMainFrame*)param;
	if(pDlg)
	{
		while(1) {
			if(pDlg->StrongSecurity()<0){
				//UbcMessageBox("�ٸ� ����E��α׷���E�浹�� �ֽ��ϴ�. �ٸ� ������E�E����E���α׷��� ��� �����ϰ�E�翁EϿ?�ֽʽÿ�E);
				exit(1);				
			}
			::Sleep(3000);
		}
	}
	return 0;
}


int CMainFrame::StrongSecurity()
{
	HANDLE handle_snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if( handle_snapshot == INVALID_HANDLE_VALUE )
	{
		// ERROR !!!
		TraceLog(("Fail to CreateToolhelp32Snapshot !!!"));
		return -1;
	}

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	BOOL is_alive = FALSE;

	if( !::Process32First(handle_snapshot, &pe32) )
	{
		// ERROR !!!
		TraceLog(("Fail to Process32First !!!"));
		::CloseHandle(handle_snapshot);
		return -1;
	}

	do
	{
		if( strnicmp(pe32.szExeFile, "UBC", 3) == 0 ){
			continue;
		}
		if( strnicmp(pe32.szExeFile, "iexp", 4) == 0 ){
			continue;
		}
		if( strnicmp(pe32.szExeFile, "expl", 4) == 0 ){
			continue;
		}
		if( strnicmp(pe32.szExeFile, "task", 4) == 0 ){
			continue;
		}
		if( strnicmp(pe32.szExeFile, "ezQ", 3) == 0 ){
			continue;
		}

		if(getWHandle(pe32.th32ProcessID)){
			//UbcMessageBox(pe32.szExeFile);
			TraceLog(("%s is run", pe32.szExeFile));
			return -2;		
		}
	}
	while( ::Process32Next(handle_snapshot, &pe32) );
	::CloseHandle(handle_snapshot);
	return 1;
}

HWND			
CMainFrame::getWHandle(unsigned long pid)
{
	if(pid>0){
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);
		/*
		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1){
			parent =  GetParent(child);
			if(!parent){
				return child;
			}
			child = parent;
		}
		*/

		if ( ::IsWindowVisible( e.hwnd ) == FALSE) return 0; // �Ⱥ��̴� ������E����..
		if ( ::GetWindowTextLength(e.hwnd) == 0 ) return 0; //ĸ�ǹٿ� ���ڰ�����������

		return e.hwnd;
	}
	return (HWND)0;
}

void CMainFrame::OnWebWelcomeboard()
{
	TraceLog(("OnWebWelcomeboard"));
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	char buf[1024];
	CString szPath2, szTemp;
	szPath2.Format("%s%sdata\\%s", szDrive, szPath, ORBCONN_INI);

	::GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", buf, 1024, szPath2);

	CString url = "http://";
	url += buf;
	url += ":8000/" ;
	url += CEnviroment::GetObject()->m_strCustomer;
	url.MakeLower();
	TraceLog(("url=%s", url));
	OpenDocument(eWebPage,"WELCOMEBOARD", url);

}

void CMainFrame::OnFileGeomap()
{
	TraceLog(("OnFileGeomap"));
	OpenDocument(eGeoMapView, LoadStringById(IDS_GEOMAPVIEW_STR001));

}
time_t CMainFrame::GetServerTime()
{
	char szBuf[1000];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ORBCONN_INI);

	::GetPrivateProfileString("TIMESYNCER", "IP", "", szBuf, sizeof(szBuf), szPath);
	CString timeHost = szBuf;
	::GetPrivateProfileString("TIMESYNCER", "PORT", "", szBuf, sizeof(szBuf), szPath);
	long timePort = atol(szBuf);

	hiHttpWrapper aWrapper;  // main �� �����Ѵ�.
	if(!aWrapper.call("TIME",timeHost,timePort,"","")){
		CString msg;
		msg.Format("http time get failed(%s)", aWrapper.getErrorMsg());
		//UbcMessageBox(msg);
		return 0;
	}
	ciString timeVal;
	if(aWrapper.getResultMsg(timeVal)==ciFalse || timeVal.empty()){
		//UbcMessageBox("http time get failed");
		return 0;
	}
	time_t current = atol(timeVal.c_str());

	return current;
}

void CMainFrame::TimeSet(time_t current)
{
	CTime aTime = current;
	//TraceLog(("recv time ", aTime.GetT

	SYSTEMTIME localTime;
	::GetLocalTime(&localTime);

	localTime.wYear = aTime.GetYear();
	localTime.wMonth = aTime.GetMonth();
	localTime.wDay = aTime.GetDay();
	localTime.wHour = aTime.GetHour();
	localTime.wMinute = aTime.GetMinute();
	localTime.wSecond = aTime.GetSecond();

	if (!::SetLocalTime(&localTime)) {
		int nErrCode = ::GetLastError();
		LPTSTR szErr;
		int nRet = ::FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			(DWORD)nErrCode,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&szErr,
			0,
			NULL);

		if ( nRet != 0 ) {
			CString msg;
			msg.Format("::SetSystemTime() : Error [%d]%s", nErrCode, szErr);
			UbcMessageBox(msg);
		}
	}
	CString msg;
	msg.Format("TimeSync Complete : %04d/%02d/%02d %02d:%02d:%02d"
				,aTime.GetYear(),aTime.GetMonth(),aTime.GetDay()
				,aTime.GetHour(),aTime.GetMinute(),aTime.GetSecond());
	UbcMessageBox(msg);
	return ;
}


void CMainFrame::ServerTimeCheck()
{
	time_t current =  GetServerTime();	
	if(current==0) {
		return;
	}

	time_t now = time(NULL);

	int gap = int(now-current);
	if(abs(gap) > 60){

		CTime localTime = now;
		CTime serverTime = current;

		CString localStr;
		localStr.Format("%04d/%02d/%02d %02d:%02d:%02d"
					,localTime.GetYear(),localTime.GetMonth(),localTime.GetDay()
					,localTime.GetHour(),localTime.GetMinute(),localTime.GetSecond());
		CString serverStr;
		serverStr.Format("%04d/%02d/%02d %02d:%02d:%02d"
					,serverTime.GetYear(),serverTime.GetMonth(),serverTime.GetDay()
					,serverTime.GetHour(),serverTime.GetMinute(),serverTime.GetSecond());

		CString msg;
		msg.Format(LoadStringById(IDS_MAINFRAME_MSG006),localStr, serverStr);

		if(UbcMessageBox(msg, MB_ICONWARNING|MB_YESNO)==IDYES) {
			TimeSet(current);
		}
	}
}


void CMainFrame::OnTimesync()
{
	time_t current =  GetServerTime();	
	if(current==0) {
		return;
	}
	TimeSet(current);
}


bool
CMainFrame::IsHYUNDAI_KIA()
{
	if(	GetEnvPtr()->m_strCustomer == _T("HYUNDAI") ||
		GetEnvPtr()->m_strCustomer == _T("KIA") )
	{
		return true;
	}
	return false;
}

bool
CMainFrame::IsWIA()
{
	if(	GetEnvPtr()->m_strCustomer == _T("WIA")  )
	{
		return true;
	}
	return false;
}

bool
CMainFrame::IsKPOST()
{
	if(	GetEnvPtr()->m_strCustomer == _T("KPOST")  )
	{
		return true;
	}
	return false;
}

void CMainFrame::OnUploadFirmware()
{
	CAppUploadDlg dlg(CAppUploadDlg::eFirmware);
	dlg.DoModal();
}

void CMainFrame::OnUploadUbcPlayerApp()
{
	CAppUploadDlg dlg(CAppUploadDlg::eKPOSTPlayer);
	dlg.DoModal();
}

void CMainFrame::OnUploadVaccineApp()
{
	CAppUploadDlg dlg(CAppUploadDlg::eVaccineApp);
	dlg.DoModal();
}

void CMainFrame::OnUploadVaccineDB()
{
	CAppUploadDlg dlg(CAppUploadDlg::eVaccineDB);
	dlg.DoModal();
}

void CMainFrame::OnToolCommander()
{
	if( m_pHsrCommand==NULL ||
		( m_pHsrCommand && !m_pHsrCommand->IsWindowVisible() )
	  )
	{
		CSysAdminLoginDlg login_dlg;
		if( login_dlg.DoModal() != IDOK ) return;
	}

	if( m_pHsrCommand == NULL )
	{
		m_pHsrCommand = new CHsrCommandDlg(this);
		m_pHsrCommand->Create(IDD_HSR_COMMAND, this);
	}

	//if( !m_pHsrCommand->IsWindowVisible() )
	//{
		m_pHsrCommand->CenterWindow();
		m_pHsrCommand->ShowWindow(SW_SHOW);
	//}

}

void CMainFrame::OnToolUploader()
{
	if( m_pServerFileUpload==NULL ||
		( m_pServerFileUpload && !m_pServerFileUpload->IsLoginValid() && !m_pServerFileUpload->IsWindowVisible() )
	  )
	{
		CSysAdminLoginDlg login_dlg;
		if( login_dlg.DoModal() != IDOK ) return;
	}

	if( m_pServerFileUpload && !m_pServerFileUpload->IsLoginValid() )
	{
		m_pServerFileUpload->DestroyWindow();
		delete m_pServerFileUpload;
		m_pServerFileUpload = NULL;
	}

	if( m_pServerFileUpload == NULL )
	{
		m_pServerFileUpload = new CServerFileUploadDlg(this);
		m_pServerFileUpload->Create(IDD_SERVER_FILE_UPLOAD, this);
	}

	//if( !m_pServerFileUpload->IsWindowVisible() )
	//{
		m_pServerFileUpload->CenterWindow();
		m_pServerFileUpload->ShowWindow(SW_SHOW);
		m_pServerFileUpload->StartUpload();
	//}
}

void CMainFrame::OnUploadManagerUpdate()
{
	CAppUploadDlg dlg(CAppUploadDlg::eManagerUpdate);
	dlg.DoModal();
}

void CMainFrame::OnUploadServerUpdate()
{
	CAppUploadDlg dlg(CAppUploadDlg::eServerUpdate);
	dlg.DoModal();
}

void CMainFrame::OnToolTrashcan()
{
	BOOL ret_val = OpenDocument(eTrashCanView, LoadStringById(IDS_PRIMARYLOGVIEW_STR001), _T("TrashCan"));
	if(ret_val)
	{
		CMDIChildWnd* pWnd = GetChildFrame(eTrashCanView);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			CView* view = pWnd->GetActiveView();
			if(view && view->GetSafeHwnd())
			{
				view->SendMessage(WM_CHANGE_TAB, 1);
			}
		}
	}
}

void CMainFrame::OnApprove()
{
	CApproveDlg dlg;
	dlg.DoModal();
}

void CMainFrame::OnUseAxis2()
{
	GetEnvPtr()->SetServerOS(!GetEnvPtr()->GetServerOS());
}

CString CMainFrame::GetRemoteLoginOptions()
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, NULL, NULL);

	CString str_path;
	str_path.Format(_T("%s%sdata\\%s"), szDrive, szPath, UBCVARS_INI);

	char buf[1024];
	CString str_options = "/askexit";
	//
	::GetPrivateProfileString(_T("REMOTE_LOGIN"), _T("AutoScaling"), _T(""), buf, 1024, str_path);
	if( atoi(buf) != 0 )
	{
		if( str_options.GetLength() > 0 ) str_options+=" ";
		str_options += "/autoscaling";
	}

	//
	::GetPrivateProfileString(_T("REMOTE_LOGIN"), _T("ViewOnly"), _T(""), buf, 1024, str_path);
	if( atoi(buf) != 0 )
	{
		if( str_options.GetLength() > 0 ) str_options+=" ";
		str_options += "/viewonly";
	}

	//
	::GetPrivateProfileString(_T("REMOTE_LOGIN"), _T("ColorDepth"), _T(""), buf, 1024, str_path);
	if( strlen(buf) > 0 )
	{
		if( str_options.GetLength() > 0 ) str_options+=" ";
		str_options += "/noauto /";
		str_options += buf;
	}

	return str_options;
}

BOOL CMainFrame::RemoteLogin_Direct(LPCSTR lpszIP, int nVNCPort)
{
	CString szPath;
#ifdef _DEBUG
	szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
	szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif
	TraceLog((szPath));

	CString szParam;
	szParam.Format(_T("%s %s:%d"), GetRemoteLoginOptions(), lpszIP, nVNCPort);

	HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);

	if( (int)nRet > 32 ) return TRUE;
	return FALSE;
}

BOOL CMainFrame::RemoteLogin_viaServer(LPCSTR lpszSiteId, LPCSTR lpszHostId)
{
	ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(lpszSiteId);
	if(!aData) return FALSE;
	TraceLog((aData->ipAddress.c_str()));

	CString szPath;
#ifdef _DEBUG
	szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
	szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif
	TraceLog((szPath));

	CString str_host_id = lpszHostId;
	int nPos = str_host_id.Find(_T("-"));
	CString strId = (nPos < 0 ? _T("") : str_host_id.Mid(nPos+1));

	CString szParam = GetRemoteLoginOptions();
	szParam += " /proxy ";
	szParam += aData->ipAddress.c_str();
	szParam += ":5901 ID:";
	//szParam += ToString(Info.vncPort);
	szParam += strId;

	HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);

	if( (int)nRet > 32 ) return TRUE;
	return FALSE;
}

BOOL CMainFrame::RemoteLogin_viaSpecified(LPCSTR lpszSiteId, LPCSTR lpszHostId)
{
	ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(lpszSiteId);
	if(!aData) return FALSE;
	TraceLog((aData->ipAddress.c_str()));

	CIPInputDLG		aDlg;

	aDlg.setIpAddress(aData->ipAddress.c_str());

	if(aDlg.DoModal() != IDOK) return FALSE;

	CString repeaterIp = aDlg.getIpAddress();
	if(repeaterIp.IsEmpty()) return FALSE;

	CString szPath;
#ifdef _DEBUG
	szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
	szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif
	TraceLog((szPath));

	CString str_host_id = lpszHostId;
	int nPos = str_host_id.Find(_T("-"));
	CString strId = (nPos < 0 ? _T("") : str_host_id.Mid(nPos+1));

	CString szParam = GetRemoteLoginOptions();
	szParam += " /proxy ";
	szParam += repeaterIp;
	szParam += ":5901 ID:";
	//szParam += ToString(Info.vncPort);
	szParam += strId;

	HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);

	if( (int)nRet > 32 ) return TRUE;
	return FALSE;
}

void CMainFrame::OnUploadFirmware2()
{
	CAppUploadDlg dlg(CAppUploadDlg::eFirmware2);
	dlg.DoModal();
}

void CMainFrame::OnUploadUbcPlayerApp2()
{
	CAppUploadDlg dlg(CAppUploadDlg::eKPOSTPlayer2);
	dlg.DoModal();
}

void CMainFrame::OnAppDgroup()
{
	CDownloadGroupDlg dlg;
	dlg.DoModal();
}
void CMainFrame::OnUploadUbcCommonPalyerApp()
{
	CAppUploadDlg dlg(CAppUploadDlg::eUBCPlayer);
	dlg.DoModal();
}

void CMainFrame::OnUploadUbcCommonLauncherApp()
{
	CAppUploadDlg dlg(CAppUploadDlg::eUBCLauncher);
	dlg.DoModal();
}
