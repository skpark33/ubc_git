// HsrCommandDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "HsrCommandDlg.h"

#include <ci/libBase/ciStringUtil.h>
#include <hi/libHttp/hiHttpWrapper.h>
#include "common/libCommon/ubcIni.h"
#include "Enviroment.h"


#define		TIMER_AUTOLOGIN_ID		(1025)
#define		TIMER_AUTOLOGIN_TIME	(100)


// CHsrCommandDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHsrCommandDlg, CDialog)

CHsrCommandDlg::CHsrCommandDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHsrCommandDlg::IDD, pParent)
	, m_repos ( this )
{
	m_bLogin = false;

	m_fontResults.CreatePointFont(8*10, "Gulimche");
	m_enableRun = true;
}

CHsrCommandDlg::~CHsrCommandDlg()
{
}

void CHsrCommandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_SERVER_IP, m_editServerIP);
	DDX_Control(pDX, IDC_EDIT_SERVER_WEB_PORT, m_editServerWebPort);
	DDX_Control(pDX, IDC_EDIT_SERVER_CMD_PORT, m_editServerCmdPort);
	DDX_Control(pDX, IDC_BUTTON_CONNECT, m_btnConnect);
	DDX_Control(pDX, IDC_EDIT_RESULTS, m_editResults);
	DDX_Control(pDX, IDC_EDIT_COMMAND, m_editCommand);
	DDX_Control(pDX, IDC_BUTTON_RUN_COMMAND, m_btnRun);
	DDX_Control(pDX, IDC_BUTTON_CLEAR_RESULTS, m_btnClearResults);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_COMBO_CMD_TYPE, m_cbxCmdType);
	DDX_Control(pDX, IDC_STATIC_CMD_RESULTS, m_stcCmdResults);
	DDX_Control(pDX, IDC_BUTTON_SERVER_STOP, m_btServerStop);
	DDX_Control(pDX, IDC_BUTTON_SERVER_START, m_btServerStart);
}


BEGIN_MESSAGE_MAP(CHsrCommandDlg, CDialog)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CHsrCommandDlg::OnBnClickedButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_RUN_COMMAND, &CHsrCommandDlg::OnBnClickedButtonRunCommand)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_RESULTS, &CHsrCommandDlg::OnBnClickedButtonClearResults)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_SERVER_STOP, &CHsrCommandDlg::OnBnClickedButtonServerStop)
	ON_BN_CLICKED(IDC_BUTTON_SERVER_START, &CHsrCommandDlg::OnBnClickedButtonServerStart)
END_MESSAGE_MAP()


// CHsrCommandDlg 메시지 처리기입니다.

BOOL CHsrCommandDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	m_repos.AddControl((CWnd*)&m_editResults, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_cbxCmdType, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_editCommand, REPOS_FIX, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnRun, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnClearResults, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_stcCmdResults, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_btnCancel, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	//
	ubcConfig aIni("UBCConnect");

	ciString buf = "";
	aIni.get("ORB_NAMESERVICE", "IP", buf);
	m_editServerIP.SetWindowText(buf.c_str());

	buf = "";
	aIni.get("ORB_NAMESERVICE", "PORT", buf);
	m_editServerWebPort.SetWindowText(buf.c_str());

	m_editServerCmdPort.SetWindowText("14998");

	//
	m_editResults.SetFont(&m_fontResults);
	m_editResults.SetLimitText(UINT_MAX);

	//
	m_cbxCmdType.InsertString(0, "Command");
	m_cbxCmdType.InsertString(1, "CLI");
	m_cbxCmdType.SetCurSel(0);

	//
	EnableControls();

	//
	SetTimer(TIMER_AUTOLOGIN_ID, TIMER_AUTOLOGIN_TIME, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CHsrCommandDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )
	{
		m_repos.Move(cx, cy);
		Invalidate(TRUE);
	}
}

void CHsrCommandDlg::OnOK()
{
	if( m_bLogin == false ) return;

	OnBnClickedButtonRunCommand();

	//CDialog::OnOK();
}

void CHsrCommandDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnCancel();
	ShowWindow(SW_HIDE);
}

void CHsrCommandDlg::EnableControls()
{
	m_editServerIP.SetReadOnly(m_bLogin ? TRUE : FALSE);
	m_editServerWebPort.SetReadOnly(m_bLogin ? TRUE : FALSE);
	m_editServerCmdPort.SetReadOnly(m_bLogin ? TRUE : FALSE);
	m_btnConnect.SetWindowText(m_bLogin ? "Disconnect" : "Connect");

	//m_editResults;
	m_editCommand.EnableWindow(m_bLogin ? TRUE : FALSE);
	m_btnRun.EnableWindow((m_bLogin ? TRUE : FALSE) && m_enableRun);
	//m_btnClearResults;
	//m_btnCancel;

	if(CEnviroment::GetObject()->GetServerOS() == 1) {
		m_btServerStop.EnableWindow(m_bLogin ? TRUE : FALSE);
		m_btServerStart.EnableWindow(FALSE);
	}else{
		m_btServerStop.ShowWindow(SW_HIDE);
		m_btServerStart.ShowWindow(SW_HIDE);
	}
}

void CHsrCommandDlg::EnableRun(bool enableRun)
{
	m_btnRun.EnableWindow(enableRun);
	m_enableRun = enableRun;
}

void CHsrCommandDlg::OnBnClickedButtonConnect()
{
	m_editServerIP.GetWindowText(m_strServerIP);

	CString str;
	m_editServerWebPort.GetWindowText(str);
	m_nServerWebPort = _ttoi(str);

	m_editServerCmdPort.GetWindowText(str);
	m_nServerCmdPort = _ttoi(str);

	bool old_login = m_bLogin;

	if( m_bLogin == false )
	{
		m_bLogin = login(m_strCurrentPath);
		m_strCurrentPath.Replace("\r", "");
		m_strCurrentPath.Replace("\n", "");
		EnableControls();

		if( m_bLogin == false )
		{
			::AfxMessageBox("Login Failed !!!", MB_ICONSTOP);
			return;
		}

		int len = m_editResults.GetWindowTextLength();
		m_editResults.SetSel(len, len, TRUE);

		CString msg;
		msg.Format("Login = %s:%u:%u\r\n\r\n", m_strServerIP, m_nServerWebPort, m_nServerCmdPort);
		m_editResults.ReplaceSel(msg);

		CString str_output = m_strCurrentPath;
		str_output += " > ";
		m_editResults.ReplaceSel(str_output);

		m_editCommand.SetFocus();
	}
	else
	{
		// 로그인중
		m_bLogin = false;
		EnableControls();

		int len = m_editResults.GetWindowTextLength();
		m_editResults.SetSel(len, len, TRUE);
		m_editResults.ReplaceSel("\r\n\r\nLogout\r\n\r\n-----------------------------------------\r\n");
	}
}
void CHsrCommandDlg::OnBnClickedButtonRunCommand()
{
	_Run(0);
}

void CHsrCommandDlg::_Run(unsigned long timeout)
{
	CString str_cmd;
	m_editCommand.GetWindowText(str_cmd);
	if( str_cmd.GetLength() == 0 ) return;
	m_editCommand.SetWindowText("");

	int len = m_editResults.GetWindowTextLength();
	m_editResults.SetSel(len, len, TRUE);
	m_editResults.ReplaceSel(str_cmd);
	m_editResults.ReplaceSel("\r\n");

	CString str_output = "";

	int idx = m_cbxCmdType.GetCurSel();
	switch( idx )
	{
	case 0:
		runCommand(str_cmd, str_output, timeout);
		break;

	case 1:
		runCLI(str_cmd, str_output);
		break;

	default:
		break;
	}

	bool is_cd_cmd = (strncmp(str_cmd, "cd ", 3) == 0);
	if( is_cd_cmd )
	{
		m_strCurrentPath = str_output;
	}
	else
	{
		m_editResults.ReplaceSel(str_output);
	}
	m_editResults.ReplaceSel("\r\n");

	if( is_cd_cmd || runCommand("pwd", m_strCurrentPath) )
	{
		m_strCurrentPath.Replace("\r", "");
		m_strCurrentPath.Replace("\n", "");

		CString str_output = m_strCurrentPath;
		str_output += " > ";
		m_editResults.ReplaceSel(str_output);
	}
	else
	{
		m_editResults.ReplaceSel("PWD-ERROR > ");
	}
}

void CHsrCommandDlg::OnBnClickedButtonClearResults()
{
	CString str_result = m_strCurrentPath;
	str_result += " > ";
	m_editResults.SetWindowText(str_result);
}

bool CHsrCommandDlg::login(CString& strOutput)
{
	return runCommand("pwd", strOutput);
}


bool CHsrCommandDlg::runCommand(LPCSTR lpszCmd, CString& strOutput, unsigned long timeout)
{
	CString str_cmd = lpszCmd;
	CString header = "?HSR" ;

	CString body;
	body.Format("%s%05d%s", header, str_cmd.GetLength(), str_cmd);

	ciString encoded_body = ciStringUtil::base64_encode((const unsigned char*)(LPCSTR)body, body.GetLength());

	hiHttpWrapper aWrapper(m_strServerIP, m_nServerWebPort, timeout);
	if( !aWrapper.call("HSR", m_strServerIP, m_nServerCmdPort, encoded_body.c_str(), "") )
	{
		//ciERROR(("hiHttpWrapper call failed(%s)", aWrapper.getErrorMsg()));
		return ciFalse;
	}

	strOutput = "";
	do
	{
		const char* replyStr  = aWrapper.getNextMsg();
		strOutput += replyStr;
	} while( aWrapper.hasMore() );

	strOutput.Replace("\r", "");
	strOutput.Replace("\n", "\r\n");

	//if( command != "pwd" && command != "cd" )
	//{
	//	command = "pwd";	
	//	outVal = "";
	//	if(_runCommand(command.c_str(), outVal, ciFalse)) {
	//		ciString prompt = _ip + ":" ;
	//		if(outVal.length() > 1) {
	//			prompt += outVal.substr(0,outVal.length()-1);
	//		}
	//	   _watcher->setPrompt(prompt.c_str());
	//	}
	//}
	//if(command == "cd") {
	//	printf("%s\n", outVal.c_str());
	//	ciString prompt = _ip + ":" ;
	//	if(outVal.length() > 1) {
	//		prompt += outVal.substr(0,outVal.length()-1);
	//	}
	//   _watcher->setPrompt(prompt.c_str());
	//}
	return ciTrue;
}


bool CHsrCommandDlg::runCLI(LPCSTR lpszCLI, CString& strOutput)
{
	hiHttpWrapper aWrapper(m_strServerIP, m_nServerWebPort);
	if( !aWrapper.call("CALL", m_strServerIP, m_nServerCmdPort, lpszCLI, "") )
	{
		//ciERROR(("hiHttpWrapper call failed(%s)", aWrapper.getErrorMsg()));
		return ciFalse;
	}

	do
	{
		const char* replyStr = aWrapper.getNextMsg();
		strOutput += replyStr;
		strOutput += "\r\n";
	} while(aWrapper.hasMore());

	return ciTrue;
}

void CHsrCommandDlg::OnTimer(UINT_PTR nIDEvent)
{
	if( nIDEvent == TIMER_AUTOLOGIN_ID )
	{
		KillTimer(TIMER_AUTOLOGIN_ID);

		OnBnClickedButtonConnect();
	}

	CDialog::OnTimer(nIDEvent);
}

void CHsrCommandDlg::OnBnClickedButtonServerStop()
{
	//if(UbcMessageBox("이 작업은 5분 정도 걸릴 수 있습니다. 작업중, 강제로 종료하지 마십시오. 계속하시겠습니까?", MB_YESNO)==IDYES){
	if(UbcMessageBox(::LoadStringById(IDS_HSRCOMMANDDLG_MSG001), MB_YESNO)==IDYES){
		m_cbxCmdType.SetCurSel(0);
		m_editCommand.SetWindowText("ubcRun kill");
		CWaitMessageBox wait;
		_Run(300);
		this->m_btServerStart.EnableWindow(TRUE);
	}
}

void CHsrCommandDlg::OnBnClickedButtonServerStart()
{
	//if(UbcMessageBox("이 작업은 5분 정도 걸릴 수 있습니다. 작업중, 강제로 종료하지 마십시오. 계속하시겠습니까?", MB_YESNO)==IDYES){
	if(UbcMessageBox(::LoadStringById(IDS_HSRCOMMANDDLG_MSG001), MB_YESNO)==IDYES){
		m_cbxCmdType.SetCurSel(0);
		m_editCommand.SetWindowText("ubcRun all bg");
		CWaitMessageBox wait;
		m_btServerStart.EnableWindow(FALSE);
		_Run(180);
	}
}
