#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "Enviroment.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"
#include "ubccopcommon\eventhandler.h"
#include "afxdtctl.h"
#include "dp_daily_plan_table.h"


// CBroadcastPlanView 폼 뷰입니다.

class CBroadcastPlanView : public CFormView
{
	DECLARE_DYNCREATE(CBroadcastPlanView)

typedef struct tagLIST_FILTER
{
	CString strBpName;
	CString strTag;
	CString strRegister;
	CString strPackageId;
	BOOL	bBaseDate;
	CTime	tmBaseDate;
	BOOL	bStartDate;
	CTime   tmStartDate;
	BOOL	bEndDate;
	CTime   tmEndDate;
	CString strHostId;
	CString strHostName;
	CString strAdvancedFilter;
} ST_LIST_FILTER, *P_LIST_FILTER;

protected:
	CBroadcastPlanView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CBroadcastPlanView();

	enum { eCheck, eSiteName, ePlan, eDownloadTime, eRegisterId, eCalendar, eMaxCol };

	CReposControl				m_Reposition;
	CDateTimeCtrl				m_dtFilterBaseDate;
	CDateTimeCtrl				m_dtFilterStart;
	CDateTimeCtrl				m_dtFilterEnd;
	CUTBListCtrlEx				m_lcList;
	CHoverButton				m_bnRefresh;
	CHoverButton				m_bnNew;
	CHoverButton				m_bnDel;
	CHoverButton				m_bnMoveUp;
	CHoverButton				m_bnMoveDown;
	CHoverButton				m_bnExport;
	CStatic						m_Filter;
	CPoint						m_ptSelList;

	CString						m_szColum[eMaxCol];
	BroadcastingPlanInfoList	m_lsInfoList;

	void						InitList();
	ST_LIST_FILTER				m_stListFilter;
	CString						m_strHostId;
	void						RefreshList();
	CDp_daily_plan_table		m_dpDailyPlan;
	int							m_nDailyPlanView;
	CButton						m_rbDailyPlanView1;
	CButton						m_rbDailyPlanView2;
	CTime						m_tmSelDate;
	void						RefreshDailyPlan(CTime tmSelDate);
	CMapStringToPtr				m_mapTimePlanInfo;
	TimePlanInfoList*			GetTimePlanInfo(CString strBpId, BOOL bForceCall);
	void						RunStudio(CString strPackage);
	CImageList*					m_pListDragImage;

	CString						GetZOrder(int nIndex);

	void LoadFilterData();
	void SaveFilterData();

	// 서버 이벤트 관련
//	CList<int>					m_lsEvID;
//	CEventManager				m_EventManager;
//	static UINT					AddEventThread(LPVOID pParam);

	LRESULT						InvokeEvent(WPARAM, LPARAM);
	void						EventHandlerCreated(CString strMgrId, CString strSiteId, CString strBpId);
	void						EventHandlerChanged(CString strMgrId, CString strSiteId, CString strBpId);
	void						EventHandlerRemoved(CString strMgrId, CString strSiteId, CString strBpId);
	void OnNMCustomdrawList(NMHDR *pNMHDR, LRESULT *pResult); //skpark eSiteUser

public:
	enum { IDD = IDD_BROADCAST_PLAN_VIEW };

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBnClickedButtonFilterHost();
	afx_msg void OnBnClickedButtonPlanRefresh();
	afx_msg void OnBnClickedButtonPlanNew();
	afx_msg void OnNMDblclkListPlan(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonPlanDel();
	afx_msg void OnBnClickedButtonPlanUp();
	afx_msg void OnBnClickedButtonPlanDown();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnDestroy();
	afx_msg void OnLvnBegindragListPlan(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonPlanExport();
	afx_msg void OnBnClickedButtonFilterRegister();
	afx_msg void OnBnClickedButtonFilterPackage();
	afx_msg void OnLvnColumnclickListPlan(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedRbDailyPlanView();
	afx_msg void OnNMClickListPlan(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRclickListPlan(NMHDR *pNMHDR, LRESULT *pResult);

	DECLARE_EVENTSINK_MAP()
	void ItemDblClickDpDailyPlan(long nIndex);
	afx_msg void OnDownloadsummaryView();
	CButton m_kbAdvancedFilter;
	CComboBox m_cbAdvancedFilter;
	afx_msg void OnBnClickedKbAdvancedFilter();
};


