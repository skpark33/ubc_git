// MultiSelectLight.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "ubccopcommon\ubccopcommon.h"
#include "ubccopcommon\copModule.h"
#include "MultiSelectLight.h"

#include "ubccopcommon\SiteSelectDlg.h"

#include "common\UbcCode.h"
#include "common\TraceLog.h"

// CMultiSelectLight 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMultiSelectLight, CDialog)

CMultiSelectLight::CMultiSelectLight(LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CMultiSelectLight::IDD, pParent)
	, m_strCustomer ( szCustomer )
{

}

CMultiSelectLight::~CMultiSelectLight()
{
}

void CMultiSelectLight::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FILTER_LIGHTNAME, m_editFilterLightName);
	DDX_Control(pDX, IDC_EDIT_FILTER_LIGHT, m_editFilterLight);
	DDX_Control(pDX, IDC_BUTTON_FILTER_LIGHTTYPE, m_cbxLightType);
	DDX_Control(pDX, IDC_CB_SITE, m_cbxSite);
	DDX_Control(pDX, IDC_CB_ONOFF, m_cbOnOff);
	DDX_Control(pDX, IDC_CB_OPERATION, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_LIGHT_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_LIST_LIGHT, m_lcLightList);
	DDX_Control(pDX, IDC_LIST_SEL_LIGHT, m_lcSelLightList);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
}

BEGIN_MESSAGE_MAP(CMultiSelectLight, CDialog)
	ON_BN_CLICKED(IDOK, &CMultiSelectLight::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMultiSelectLight::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_ADD, &CMultiSelectLight::OnBnClickedBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, &CMultiSelectLight::OnBnClickedBnDel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_LIGHT, &CMultiSelectLight::OnNMDblclkListLight)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SEL_LIGHT, &CMultiSelectLight::OnNMDblclkListSelLight)
	ON_BN_CLICKED(IDC_BUTTON_LIGHT_REFRESH, &CMultiSelectLight::OnBnClickedButtonLightRefresh)
	ON_CBN_SELCHANGE(IDC_CB_SITE, &CMultiSelectLight::OnCbnSelchangeCbSite)
END_MESSAGE_MAP()


// CMultiSelectLight 메시지 처리기입니다.

BOOL CMultiSelectLight::OnInitDialog()
{
	CWaitMessageBox wait;

	CDialog::OnInitDialog();

	if(!m_title.IsEmpty()){
		this->SetWindowText(m_title);
	}

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnAdd    .LoadBitmap(IDB_BUTTON_NEXT2  , RGB(255,255,255));
	m_bnDel    .LoadBitmap(IDB_BUTTON_PREV2  , RGB(255,255,255));
	m_bnOK     .LoadBitmap(IDB_BUTTON_SELECT , RGB(255,255,255));
	m_bnCancel .LoadBitmap(IDB_BUTTON_CANCEL , RGB(255,255,255));

	//m_bnRefresh.SetToolTipText(LoadStringById(IDS_MULTISELECTLIGHT_BTN001));  //

	CodeItemList listLightType;
	CUbcCode::GetInstance(m_strCustomer)->GetCategoryInfo(_T("LightType"), listLightType);

	POSITION pos = listLightType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listLightType.GetNext(pos);
		m_cbxLightType.AddString(info.strEnumString, info.nEnumNumber);
	}

	m_cbOnOff.AddString(LoadStringById(IDS_UBCMANAGER_STR001));
	m_cbOnOff.AddString(LoadStringById(IDS_HOSTVIEW_LIST003));
	m_cbOnOff.AddString(LoadStringById(IDS_HOSTVIEW_LIST004));
	m_cbOnOff.SetCurSel(0);

	m_cbOperation.AddString(LoadStringById(IDS_UBCMANAGER_STR001));
	m_cbOperation.AddString(LoadStringById(IDS_HOSTVIEW_LIST001));
	m_cbOperation.AddString(LoadStringById(IDS_HOSTVIEW_LIST002));
	m_cbOperation.SetCurSel(0);


	char buf[1024];
//	CString szPath;
//	szPath.Format("%s%sdata\\UBCManager.ini", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	GetPrivateProfileString("LIGHT-FILTER", "LightName", "", buf, 1024, m_strIniPath);
	m_editFilterLightName.SetWindowText(buf);
	GetPrivateProfileString("LIGHT-FILTER", "LightID", "", buf, 1024, m_strIniPath);
	m_editFilterLight.SetWindowText(buf);

	GetPrivateProfileString("LIGHT-FILTER", "LightType", "0", buf, 1024, m_strIniPath);
	CString strLightType = buf;
	if(strLightType.IsEmpty())
	{
		m_cbxLightType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strLightType.Tokenize(",", pos)) != _T(""))
		{
			m_cbxLightType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}

	GetPrivateProfileString("LIGHT-FILTER", "SiteName", "", buf, 1024, m_strIniPath);
	CString strSiteName = buf;
	m_cbxSite.AddString(LoadStringById(IDS_UBCMANAGER_STR001));
	m_cbxSite.AddString(LoadStringById(IDS_UBCMANAGER_STR002));
	m_cbxSite.SetCurSel(0);
	//if(CCopModule::eSiteAdmin != m_nAuthority)
	if(CCopModule::eSiteUser == m_nAuthority) // skpark 2012.11.1 siteUser 만 못고친다.
		                                      // siteManager 는 하위 사이트선택이 가능해야 한다.
	{
		m_cbxSite.InsertString(1, m_strSiteId);
		m_cbxSite.SetCurSel(1);
		m_cbxSite.EnableWindow(FALSE);

		m_strSite = m_strSiteId;
	}
	else
	{
		GetPrivateProfileString("LIGHT-FILTER", "SiteID", "", buf, 1024, m_strIniPath);
		m_strSite = buf;

		if(m_strSite.GetLength() > 0)
		{
			m_cbxSite.InsertString(1, strSiteName);
			m_cbxSite.SetCurSel(1);
		}
	}

	GetPrivateProfileString("LIGHT-FILTER", "AdminStat", "0", buf, 1024, m_strIniPath);
	m_cbOnOff.SetCurSel(atoi(buf));
	GetPrivateProfileString("LIGHT-FILTER", "OperaStat", "0", buf, 1024, m_strIniPath);
	m_cbOperation.SetCurSel(atoi(buf));

	InitLightList();
	RefreshLightList();

	InitSelLightList();

	RefreshSelLightList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMultiSelectLight::OnBnClickedOk()
{
	m_astrSelLightList.RemoveAll();
	m_astrSelLightNameList.RemoveAll();

	for(int i=0; i<m_lcSelLightList.GetItemCount() ;i++)
	{
		m_astrSelLightList.Add(m_lcSelLightList.GetItemText(i, eLightID));
		m_astrSelLightNameList.Add(m_lcSelLightList.GetItemText(i, eLightName));
	}
	TraceLog(("%d row selected", m_astrSelLightNameList.GetSize()));

	OnOK();
}

void CMultiSelectLight::OnBnClickedCancel()
{
	OnCancel();
}

void CMultiSelectLight::InitLightList()
{
	m_lcLightList.SetExtendedStyle(m_lcLightList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck ] = _T("");
	m_szColum[eLightName] = LoadStringById(IDS_LIGHTVIEW_STR003);
	m_szColum[eLightID] = LoadStringById(IDS_LIGHTVIEW_STR002);
	m_szColum[eGroup ] = LoadStringById(IDS_HOSTVIEW_LIST007);

	m_lcLightList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcLightList.InsertColumn(eLightName , m_szColum[eLightName ], LVCFMT_LEFT  , 100);
	m_lcLightList.InsertColumn(eLightID , m_szColum[eLightID ], LVCFMT_LEFT  , 100);
	m_lcLightList.InsertColumn(eGroup  , m_szColum[eGroup  ], LVCFMT_LEFT  ,  85);

	// 제일 마지막에 할것...
	m_lcLightList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcLightList.SetSortEnable(false);
//	m_lcLightList.SetFixedCol(eCheck);
}

void CMultiSelectLight::RefreshLightList()
{
	CWaitMessageBox wait;

	CStringArray filter_list;
	CString strTmp;

	//
	CString strLightName;
	m_editFilterLightName.GetWindowText(strLightName);
	if(!strLightName.IsEmpty())
	{
		strTmp.Format(_T("lightName like '%%%s%%'"), strLightName);
		filter_list.Add(strTmp);
	}

	//
	CString strLight;
	m_editFilterLight.GetWindowText(strLight);
	if(!strLight.IsEmpty())
	{
		strTmp.Format(_T("lightId like '%%%s%%'"), strLight);
		filter_list.Add(strTmp);
	}

	//
	CString strLightType = m_cbxLightType.GetCheckedIDs();
	if(strLightType == _T("All")) strLightType = _T("");
	else if(strLightType.GetLength() >= 2) strLightType = strLightType.Mid(1, strLightType.GetLength()-2);
	if(!strLightType.IsEmpty())
	{
		strTmp.Format(_T("lightType in ('%s')")
					, strLightType
					);
		strTmp.Replace(",", "','");
		filter_list.Add(strTmp);
	}

	//
	CString strSiteName;
	m_cbxSite.GetWindowText(strSiteName);

	if(m_strSite.IsEmpty())
	{
		if(CCopModule::eSiteAdmin != m_nAuthority)
		{
			strTmp.Format(_T("siteId like '%%%s%%'"), m_strSiteId);
			filter_list.Add(strTmp);
		}
	}
	else
	{
		// skpark 2013.4.23 multiselect 가 가능하도록
		//strTmp.Format(_T("siteId like '%%%s%%'"), m_strSite);
		//filter_list.Add(strTmp);
		CString strSiteId = m_strSite;
		if(!strSiteId.IsEmpty()){
			strSiteId.Replace(_T(","), _T("','"));
			strTmp.Format(_T("siteId in ('%s')"), strSiteId);
			filter_list.Add(strTmp);
		}
	}

	//
	if(m_cbOnOff.GetCurSel())
	{
		strTmp.Format(_T("adminState = %d"), (m_cbOnOff.GetCurSel()==1)? 1:0);
		filter_list.Add(strTmp);
	}

	if(m_cbOperation.GetCurSel())
	{
		strTmp.Format(_T("operationalState = %d"), (m_cbOperation.GetCurSel()==1)? 1:0);
		filter_list.Add(strTmp);
	}


	//
	CString strWhere = "";
	int count = filter_list.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str = filter_list.GetAt(i);

		if(strWhere.GetLength() > 0)
		{
			strWhere += " and ";
		}

		strWhere += "( ";
		strWhere += str;
		strWhere += ") ";
	}

#ifdef _DEBUG
//	UbcMessageBox(strWhere);
#endif

	WritePrivateProfileString("LIGHT-FILTER", "LightName", strLightName, m_strIniPath);
	WritePrivateProfileString("LIGHT-FILTER", "LightID", strLight, m_strIniPath);
	WritePrivateProfileString("LIGHT-FILTER", "LightType", strLightType, m_strIniPath);
	WritePrivateProfileString("LIGHT-FILTER", "SiteName", strSiteName, m_strIniPath);
	WritePrivateProfileString("LIGHT-FILTER", "SiteID", m_strSite, m_strIniPath);

	WritePrivateProfileString("LIGHT-FILTER", "AdminStat", ToString(m_cbOnOff.GetCurSel()), m_strIniPath);
	WritePrivateProfileString("LIGHT-FILTER", "OperaStat", ToString(m_cbOperation.GetCurSel()), m_strIniPath);

	m_lsInfoList.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetLightList((CCopModule::eSiteAdmin == m_nAuthority ? _T("*") : m_strSiteId)
													, _T("*")
													, strWhere
													, true
													, true
													, m_lsInfoList 	);

	m_lcLightList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SLightInfo info = m_lsInfoList.GetNext(pos);

		m_lcLightList.InsertItem(nRow, "");
		m_lcLightList.SetItemText(nRow, eLightName, info.lightName);
		m_lcLightList.SetItemText(nRow, eLightID, info.lightId);
		m_lcLightList.SetItemText(nRow, eGroup , info.siteId);
		m_lcLightList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
}

void CMultiSelectLight::OnBnClickedButtonLightRefresh()
{
	RefreshLightList();
}

void CMultiSelectLight::InitSelLightList()
{
	m_lcSelLightList.SetExtendedStyle(m_lcSelLightList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcSelLightList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcSelLightList.InsertColumn(eLightName , m_szColum[eLightName ], LVCFMT_LEFT  , 100);
	m_lcSelLightList.InsertColumn(eLightID , m_szColum[eLightID ], LVCFMT_LEFT  , 100);

	// 제일 마지막에 할것...
	m_lcSelLightList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcSelLightList.SetSortEnable(false);
//	m_lcSelLightList.SetFixedCol(eCheck);
}

void CMultiSelectLight::RefreshSelLightList()
{
	m_lcSelLightList.DeleteAllItems();

	for(int i=0; i<m_astrSelLightList.GetCount() ;i++)
	{
		m_lcSelLightList.InsertItem(i, "");
		m_lcSelLightList.SetItemText(i, eLightID, m_astrSelLightList[i]);
	}
	for(int i=0; i<m_astrSelLightNameList.GetCount() ;i++)
	{
		//m_lcSelLightList.InsertItem(i, "");
		m_lcSelLightList.SetItemText(i, eLightName, m_astrSelLightNameList[i]);
	}
}

void CMultiSelectLight::OnNMDblclkListLight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	AddSelLight(pNMLV->iItem);
}

void CMultiSelectLight::OnNMDblclkListSelLight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	DelSelLight(pNMLV->iItem);
}

void CMultiSelectLight::OnBnClickedBnAdd()
{
	bool bChecked = false;
	for(int i=0; i<m_lcLightList.GetItemCount() ;i++)
	{
		if(m_lcLightList.GetCheck(i))
		{
			bChecked = true;
			break;
		}
	}

	if(!bChecked)
	{
		UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR019));
		return;
	}

	for(int i=0; i<m_lcLightList.GetItemCount() ;i++)
	{
		if(!m_lcLightList.GetCheck(i)) continue;

		AddSelLight(i);

		m_lcLightList.SetCheck(i, FALSE);
	}

	m_lcLightList.SetCheckHdrState(FALSE);
}

void CMultiSelectLight::OnBnClickedBnDel()
{
	for(int i=m_lcSelLightList.GetItemCount()-1; i>=0 ;i--)
	{
		if(!m_lcSelLightList.GetCheck(i)) continue;
		DelSelLight(i);
	}

	m_lcSelLightList.SetCheckHdrState(FALSE);
}

void CMultiSelectLight::AddSelLight(int nLightIndex)
{
	if(nLightIndex < 0 || nLightIndex >= m_lcLightList.GetItemCount()) return;

	CString strLightId = m_lcLightList.GetItemText(nLightIndex, eLightID);
	CString strLightName = m_lcLightList.GetItemText(nLightIndex, eLightName);

	// 이미 추가된 단말인지 체크
	for(int i=0; i<m_lcSelLightList.GetItemCount() ;i++)
	{
		CString strSelLightId = m_lcSelLightList.GetItemText(i, eLightID);
		if(strSelLightId == strLightId) return;
	}

	int nRow = m_lcSelLightList.GetItemCount();
	m_lcSelLightList.InsertItem(nRow, "");
	m_lcSelLightList.SetItemText(nRow, eLightID, strLightId);
	m_lcSelLightList.SetItemText(nRow, eLightName, strLightName);
}

void CMultiSelectLight::DelSelLight(int nSelLightIndex)
{
	if(nSelLightIndex < 0 || nSelLightIndex >= m_lcSelLightList.GetItemCount()) return;

	m_lcSelLightList.DeleteItem(nSelLightIndex);
}

BOOL CMultiSelectLight::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_SITEID)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_LIGHT  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_ONOFF          )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_OPERATION      )->GetSafeHwnd() )
		{
			OnBnClickedButtonLightRefresh();
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CMultiSelectLight::OnCbnSelchangeCbSite()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbxSite.GetCurSel();
	int count = m_cbxSite.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxSite.DeleteString(1);
			m_strSite = "";
		}
	}
	else if(idx == count-1 )
	{
		CSiteSelectDlg dlg(m_strCustomer);
		//skpark 2013.4.23 multiselect 수정
		//dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_strSite = "";
				m_cbxSite.DeleteString(1);
				m_cbxSite.SetCurSel(0);
			}
			//skpark 2013.4.23 multiselect start[
			CString strSiteName,strSiteId;
			SiteInfoList siteInfoList;
			if(dlg.GetSelectSiteInfoList(siteInfoList))
			{
				POSITION pos = siteInfoList.GetHeadPosition();
				while(pos)
				{
					SSiteInfo info = siteInfoList.GetNext(pos);
					strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
					strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
				}
			}
			if(!strSiteName.IsEmpty()){
				m_cbxSite.InsertString(1, strSiteName);
				m_cbxSite.SetCurSel(1);
				m_strSite = strSiteId;
			}else{
				m_cbxSite.SetCurSel(0);
				m_strSite = "";
			
			}

			//SSiteInfo info;
			//dlg.GetSelectSiteInfo(info);

			//if(info.siteName.GetLength() > 0)
			//{
			//	m_cbxSite.InsertString(1, info.siteName);
			//	m_cbxSite.SetCurSel(1);
			//	m_strSite = info.siteId;
			//}
			//else
			//{
			//	m_cbxSite.SetCurSel(0);
			//	m_strSite = "";
			//}
			//skpark 2013.4.23 multiselect end]

		}
		else
		{
			m_cbxSite.SetCurSel(count-2);
		}
	}else{
		// skpark 2013.4.23 multiselect 
		// 콤보박스의 글자가 14자가 넘어가면 팝업으로 보여준다.
		CString strSiteName;
		m_cbxSite.GetWindowText(strSiteName);
		if(strSiteName.GetLength() > 14){
			CString msgStr = "Selected Group is\n";
			msgStr.Append(strSiteName);
			UbcMessageBox(msgStr);
		}
	}
}

