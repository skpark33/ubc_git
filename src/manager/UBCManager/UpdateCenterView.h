#pragma once

#include "afxwin.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"
#include "ubccopcommon\eventhandler.h"
#include "common\CheckComboBox.h"
#include "UBCCopCommon\CopModule.h"

class CWOLDLG;
class CRemoteCliCall;
class CRemoteDeleteFileDlg;

struct SHostInfo;

// CUpdateCenterView form view
class CMainFrame;
class CUpdateCenterView : public CFormView
{
	DECLARE_DYNCREATE(CUpdateCenterView)
public:
	enum { eCheck, // 2010.02.17 by gwangsoo
		   eHostVNCStat, eMonitorStat, eHostType, 
		   eSiteName, eHostName, eTag, eVersion, eAutoUpdate, eDownloadTime,
		   eProtocolType, eDisk1Avail, eDisk2Avail, 
		   eHostID, eHostIP, eHostMacAddr, 
		   eHostEdition, eHostBootUpTime, eHostUpDate, eHostDesc, 
		   eHostEnd };
	enum { eMonitorOn, eMonitorOff, eMonitorUnknown, eVncOn, eVncOff };
	enum { eFront, eBack, eAll };

private:
	HostInfoList m_lsHost;

	CList<CWOLDLG*>  m_wolDlgList;
	CList<CRemoteCliCall*>  m_cliDlgList;
	CList<CRemoteDeleteFileDlg*>  m_delDlgList;

	void InitList();
	BOOL RefreshHost(bool bCompMsg=true);
	void RefreshHostList();
	void UpdateListRow(int nRow, SHostInfo* pInfo);
	void InitPosition(CRect);
	int ChangeOPStat(SOpStat*);
	int ChangeAdStat(SAdStat*);
	int ChangeVncStat(cciEvent*);
	int ChangeMonitorStat(SMonitorStat*); // 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	int ChangeDiskAvail(SDiskAvailChanged*);

	CMapStringToString* m_pBackupHostList;
	void BackupHostList();
	void DeleteBackupHostList();
	bool IsJustNowHost(CString strHostId);

	void LoadFilterData();
	void SaveFilterData();
	CString m_strSiteId;

//	virtual int InvokeEvent(WPARAM, LPARAM);
	LRESULT InvokeEvent(WPARAM, LPARAM);
public:
private:
	CMainFrame*  m_pMainFrame;
	CString		 m_szMsg;

	CUTBListCtrlEx m_lcList;
	CStatic		 m_Filter;
	CString		 m_szHostColum[eHostEnd];

	CHoverButton m_btnRefHost;
	CHoverButton m_btnUpdateNow;
	CHoverButton m_btnSetTime;
	CHoverButton m_btnChnSite;
	CHoverButton m_btnExcelSave;
	CHoverButton m_btnRemoteCall;
	CHoverButton m_btnWOL;
	CHoverButton m_btnHostOsChngPwd;
	CHoverButton m_btnDeleteFile;

	CComboBox m_cbAutoUpdate;
	CComboBox m_cbAdmin;
	CComboBox m_cbOperation;
	CCheckComboBox m_cbHostType;
	CHoverButton m_btAddTags;
	CHoverButton m_btDelTags;

	CPoint m_ptSelList;
	CRect m_rcClient;
	CReposControl m_Reposition;

//	CList<int> m_lsEvID;
//	CEventManager m_EventManager;
//	static UINT AddEventThread(LPVOID pParam);

	CImageList m_ilHostList;

//	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
//	static int CALLBACK CompareHost(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

protected:
	CUpdateCenterView();           // protected constructor used by dynamic creation
	virtual ~CUpdateCenterView();

public:
	enum { IDD = IDD_UPDATECENTER_VIEW };

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnBnClickedButtonHostrfs();
	afx_msg void OnBnClickButtonHostChgSite();
	afx_msg void OnBnClickedBnToExcel2();
	afx_msg void OnBnClickedButtonFilterSite();
	afx_msg LRESULT OnFilterHostChanged(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedButtonUpdateNow();
	afx_msg void OnBnClickedButtonSetTime();
	afx_msg void OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonRemoteCliCall();
	afx_msg void OnNMRclickListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnRemotelogin();
	afx_msg void OnRemotelogin2(); // 0001442: 원격접속 방법에 서버를 경유하지 않는 vnc원격접속을 만든다.
	afx_msg void OnRemotelogin3(); // 원격접속 방법에 서버를 경유하는 vnc원격접속을 만든다.
	afx_msg void OnRemoteDesktop();
	afx_msg void OnBnClickedButtonChngOsPw();	// 0001389: [RFP] 단말 OS 패스워드 변경 기능
	afx_msg void OnBnClickedButtonDeleteFile();	// 0001447: [RFP] 셋톱박스의 특정 파일 삭제 기능
	CComboBox m_cbFilterTag;
	CButton m_kbAdvancedFilter;
	CComboBox m_cbAdvancedFilter;
	afx_msg void OnBnClickedKbAdvancedFilter();
	afx_msg void OnBnClickedBnAddtag();
	afx_msg void OnBnClickedBnDeltag();
	afx_msg void OnBnClickedButtonWol();
	afx_msg void OnRemoteLoginSettings();
};


