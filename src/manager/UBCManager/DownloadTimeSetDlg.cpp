// DownloadTimeSetDlg.cpp : implementation file
//
#include "stdafx.h"
#include "UBCManager.h"
#include "DownloadTimeSetDlg.h"
#include "common\ubcdefine.h"

// CDownloadTimeSetDlg dialog
IMPLEMENT_DYNAMIC(CDownloadTimeSetDlg, CDialog)

CDownloadTimeSetDlg::CDownloadTimeSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDownloadTimeSetDlg::IDD, pParent)
{
}

CDownloadTimeSetDlg::~CDownloadTimeSetDlg()
{
}

void CDownloadTimeSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_KB_USE_MONITOR_OFF_TIME, m_ckUseMonitorOffTime);
	DDX_Control(pDX, IDC_TM_MONITOR_OFF, m_dtMonitorOff);
	DDX_Control(pDX, IDC_TM_MONITOR_ON, m_dtMonitorOn);
	DDX_Control(pDX, IDC_BN_ADD_MONITOR_OFF_TIME, m_bnAddMonitorOffTime);
	DDX_Control(pDX, IDC_BN_DEL_MONITOR_OFF_TIME, m_bnDelMonitorOffTime);
	DDX_Control(pDX, IDC_LIST_MONITOR_OFF_TIME, m_lscMonitorOffTime);
	DDX_Control(pDX, IDOK, m_btOK);
	DDX_Control(pDX, IDCANCEL, m_btCancel);
}

BEGIN_MESSAGE_MAP(CDownloadTimeSetDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CDownloadTimeSetDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_KB_USE_MONITOR_OFF_TIME, &CDownloadTimeSetDlg::OnBnClickedKbUseMonitorOffTime)
	ON_BN_CLICKED(IDC_BN_ADD_MONITOR_OFF_TIME, &CDownloadTimeSetDlg::OnBnClickedBnAddMonitorOffTime)
	ON_BN_CLICKED(IDC_BN_DEL_MONITOR_OFF_TIME, &CDownloadTimeSetDlg::OnBnClickedBnDelMonitorOffTime)
END_MESSAGE_MAP()

BOOL CDownloadTimeSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 모니터 절전 기능 관련
	m_dtMonitorOff.SetFormat(" HH : mm");
	m_dtMonitorOn.SetFormat(" HH : mm");
	m_bnAddMonitorOffTime.LoadBitmap(IDB_BUTTON_NEXT2, RGB(255,255,255));
	m_bnDelMonitorOffTime.LoadBitmap(IDB_BUTTON_PREV2, RGB(255,255,255));

	m_lscMonitorOffTime.SetExtendedStyle(m_lscMonitorOffTime.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lscMonitorOffTime.InsertColumn(0, LoadStringById(IDS_HOLLIDAYDLG_LST002), LVCFMT_CENTER, 120);
	m_lscMonitorOffTime.InitHeader(IDB_LIST_HEADER);

	InitMonitorOffTimeInfo();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDownloadTimeSetDlg::OnBnClickedOk()
{
	// 모니터 절전 기능 관련

	m_bMonitorOff = m_ckUseMonitorOffTime.GetCheck();
	
	m_property_name = "SET.UBCVariables.Download.availabletime";
	m_property_value = "";
	if(m_bMonitorOff == FALSE)
	{
		return;
	}


	m_listMonitorOffTime.clear();
	for(int nRow = 0; nRow < m_lscMonitorOffTime.GetItemCount(); nRow++)
	{
		std::string strBuf = m_lscMonitorOffTime.GetItemText(nRow, 0);
		
		if(!m_property_value.IsEmpty()) 
		{
			m_property_value += ",";
		}
		m_property_value += strBuf.c_str();

		m_listMonitorOffTime.push_back(strBuf);
	}

	OnOK();
}


// 모니터 절전 기능 관련
void CDownloadTimeSetDlg::InitMonitorOffTimeInfo()
{
	if(m_property_value.IsEmpty())
	{
		m_bMonitorOff = FALSE;
		return;
	}

	m_bMonitorOff = TRUE;
	int nPos=0;	
	CString szToken = m_property_value.Tokenize(",",nPos);
	while(!szToken.IsEmpty()){
		m_listMonitorOffTime.push_back(szToken.GetBuffer());
		szToken = m_property_value.Tokenize(",",nPos);
	}

	m_lscMonitorOffTime.DeleteAllItems();

	int nRow=0;
	for(std::list<std::string>::iterator Iter = m_listMonitorOffTime.begin(); Iter != m_listMonitorOffTime.end(); Iter++)
	{
		CString strTime = (*Iter).c_str();
		if(strTime.IsEmpty()) continue;

		m_lscMonitorOffTime.InsertItem(nRow, strTime);
		nRow++;
	}

	EnableMonitorOffInfo(m_bMonitorOff);
}

// 모니터 절전 기능 관련
void CDownloadTimeSetDlg::EnableMonitorOffInfo(BOOL bUseMonitorOff)
{
	m_ckUseMonitorOffTime.SetCheck(bUseMonitorOff);
	m_dtMonitorOff.EnableWindow(bUseMonitorOff);
	m_dtMonitorOn.EnableWindow(bUseMonitorOff);
	m_bnAddMonitorOffTime.EnableWindow(bUseMonitorOff);
	m_bnDelMonitorOffTime.EnableWindow(bUseMonitorOff);
	m_lscMonitorOffTime.EnableWindow(bUseMonitorOff);
}

// 모니터 절전 기능 관련
void CDownloadTimeSetDlg::OnBnClickedKbUseMonitorOffTime()
{
	EnableMonitorOffInfo(m_ckUseMonitorOffTime.GetCheck());
}

// 모니터 절전 기능 관련
void CDownloadTimeSetDlg::OnBnClickedBnAddMonitorOffTime()
{
	COleDateTime tmOff;
	m_dtMonitorOff.GetTime(tmOff);

	COleDateTime tmOn;
	m_dtMonitorOn.GetTime(tmOn);

	// 모니터 켜짐시간과 꺼짐시간의 시간 설정시 간격은 최소 5분이상으로 한다.
	if((tmOff-tmOn).GetTotalMinutes() < 5)
	{
		//UbcMessageBox("시작시간과 종료시간은 최소 5분 이상 차이가 나야 합니다");
		UbcMessageBox(::LoadStringById(IDS_DOWNLOADTIMESETDLG_MSG001));
		return;
	}

	CString strInTime;
	strInTime.Format(_T("%s-%s"), tmOn.Format(_T("%H:%M")), tmOff.Format(_T("%H:%M")));

	for(int nRow = 0; nRow < m_lscMonitorOffTime.GetItemCount(); nRow++)
	{
		CString strTime = m_lscMonitorOffTime.GetItemText(nRow, 0);
		if(strInTime == strTime) return;

		CString strOnTime = strTime.Left(5);
		CString strOffTime = strTime.Right(5);

		// 새로 입력하는 시간이 기존 시간에 겹치는지 여부
		if( (strOnTime <= tmOn.Format(_T("%H:%M")) && tmOn.Format(_T("%H:%M")) <= strOffTime) || 
			(strOnTime <= tmOff.Format(_T("%H:%M")) && tmOff.Format(_T("%H:%M")) <= strOffTime) )
		{
			UbcMessageBox(LoadStringById(IDS_HOLLIDAYDLG_MSG001));
			return;
		}

		// 새로 입력하는 시간에 기존시간이 포함되는지 여부
		if( (tmOn.Format(_T("%H:%M")) <= strOnTime && strOnTime <= tmOff.Format(_T("%H:%M"))) || 
			(tmOn.Format(_T("%H:%M")) <= strOffTime && strOffTime <= tmOff.Format(_T("%H:%M"))) )
		{
			UbcMessageBox(LoadStringById(IDS_HOLLIDAYDLG_MSG001));
			return;
		}
	}

	int nRow = m_lscMonitorOffTime.GetItemCount();
	m_lscMonitorOffTime.InsertItem(nRow, strInTime);
	m_lscMonitorOffTime.SortItems(CompareSite, (DWORD_PTR)&m_lscMonitorOffTime);
}

// 모니터 절전 기능 관련
void CDownloadTimeSetDlg::OnBnClickedBnDelMonitorOffTime()
{
	POSITION pos = m_lscMonitorOffTime.GetFirstSelectedItemPosition();
	while(pos)
	{
		int nRow = m_lscMonitorOffTime.GetNextSelectedItem(pos);
		m_lscMonitorOffTime.DeleteItem(nRow);
	}
}


int CDownloadTimeSetDlg::CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	LVFINDINFO lvFind;
	lvFind.flags = LVFI_PARAM;

	CListCtrl* pListCtrl = (CListCtrl*)lParam;
	if(!pListCtrl) return 0;

	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	int nCol = 0;
	CString strItem1 = pListCtrl->GetItemText(nIndex1, nCol);
	CString strItem2 = pListCtrl->GetItemText(nIndex2, nCol);

	return strItem1.Compare(strItem2);
}

