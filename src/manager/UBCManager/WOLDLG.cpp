// WOLDLG.cpp : 구현 파일입니다.
//
#include "stdafx.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "WOLDLG.h"
#include "common\UbcCode.h"
#include "HostExcelSave.h"

#include <winsock2.h>
#include "common/libCLITransfer/CTSocket.h"
#include "common/libCLITransfer/CLITransfer.h"
#ifdef _DEBUG
#pragma comment(lib, "libCLITransfer_d.lib")
#else
#pragma comment(lib, "libCLITransfer.lib")
#endif

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)
#define COLOR_DOWNLOAD		RGB(0,128,255)		// ::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_FAIL			RGB(176,0,0)


// CWOLDLG 대화 상자입니다.

IMPLEMENT_DYNAMIC(CWOLDLG, CDialog)

CWOLDLG::CWOLDLG(CWnd* pParent /*=NULL*/)
	: CDialog(CWOLDLG::IDD, pParent)
	//, m_CliTransfer(this)
	, m_progress_counter(0)
	, m_hasRelayInfo(false)
	, m_bIsPlaying(false)
{
	m_CliTransfer = new CCLITransfer(this);
}

CWOLDLG::~CWOLDLG()
{
	TraceLog(("CWOLDLG deleted"));
	Clear();
}

void CWOLDLG::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_BN_POWERON, m_btPowerOn);
	DDX_Control(pDX, IDC_BN_POWEROFF, m_btPowerOff);
	DDX_Control(pDX, IDC_BN_REFRESH, m_btRefresh);
	DDX_Control(pDX, IDC_BN_FILESAVE, m_btSave);
	DDX_Control(pDX, IDC_CHECK_EXCEPT, m_check_except);
	DDX_Control(pDX, IDCANCEL, m_btExit);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcList);
	DDX_Control(pDX, IDC_EDIT_WOL_PORT, m_edit_wolPort);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	DDX_Control(pDX, IDC_STATIC_COUNTER, m_stCounter);
	DDX_Control(pDX, IDC_STATIC_PROGRESS, m_stProgress);
}


BEGIN_MESSAGE_MAP(CWOLDLG, CDialog)
	ON_BN_CLICKED(ID_BN_POWERON, &CWOLDLG::OnBnClickedBnPoweron)
	ON_BN_CLICKED(IDC_BN_POWEROFF, &CWOLDLG::OnBnClickedBnPoweroff)
	ON_BN_CLICKED(IDC_BN_REFRESH, &CWOLDLG::OnBnClickedBnRefresh)
	ON_BN_CLICKED(IDC_BN_FILESAVE, &CWOLDLG::OnBnClickedBnFilesave)
	ON_BN_CLICKED(IDCANCEL, &CWOLDLG::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK_EXCEPT, &CWOLDLG::OnBnClickedCheckExcept)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CWOLDLG 메시지 처리기입니다.

void CWOLDLG::OnBnClickedBnPoweron()
{
	//if(m_hasRelayInfo==false){
	//	OnBnClickedBnRefresh();
	//}
	//CString cmd = "cmd /c C:\\SQISOFT\\UTV1.0\\execute\\UTV_WOL.exe macAddress 14008";
	_runCLI("cmd /c C:\\SQISOFT\\UTV1.0\\execute\\UTV_WOL.exe", eRelayHostIP);

}

void CWOLDLG::OnBnClickedBnPoweroff()
{
	_runCLI("C:\\SQISOFT\\UTV1.0\\execute\\shutdownSystem.bat");
}

void CWOLDLG::OnBnClickedBnRefresh()
{
	m_btPowerOn.EnableWindow(FALSE);
	m_btPowerOff.EnableWindow(FALSE);
	m_btRefresh.EnableWindow(FALSE);
	m_btSave.EnableWindow(FALSE);
	m_btExit.EnableWindow(FALSE);

	//CWaitMessageBox wait(LoadStringById(IDS_WOL_INIT_STR));
	_getRelayInfo();
}

void CWOLDLG::OnBnClickedBnFilesave()
{
	CHostExcelSave dlg;

	CString strExcelFile = dlg.Save(  LoadStringById(IDS_HOSTVIEW_STR001)
									, m_lcList
									);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}

void CWOLDLG::OnBnClickedCancel()
{
	CString wolPort="1234";
	m_edit_wolPort.GetWindowText(wolPort);
	if(!wolPort.IsEmpty()){
		_setWOLPort(wolPort);
	}

	if(m_CliTransfer->IsRunning())
	{
		m_CliTransfer->Stop();
		
		m_btExit.LoadBitmap(IDB_BITMAP_WOL_EXIT,RGB(255,255,255));
		m_Progress.SetPos(m_progress_counter);
		m_stProgress.SetWindowText(LoadStringById(IDS_WOL_STOP_STR));
		//m_CliTransfer->setHandler(0);

		UpdateData(FALSE);

	}
	else
	{
		// clear relayHostMap
		for(RelayHostInfoMap::iterator itr = m_relayHostMap.begin();itr!=m_relayHostMap.end();itr++){
			SRelayHostInfo* rInfo = (SRelayHostInfo*)itr->second;
			if(rInfo){
				delete rInfo;
			}
		}
		m_relayHostMap.clear();

		TraceLog(("OnCancel()"));
		OnCancel();
	}
}

void CWOLDLG::OnBnClickedCheckExcept()
{
	int checked = m_check_except.GetCheck();

	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		CString errCodeStr = m_lcList.GetItemText(i, eErrCode);
		TraceLog(("Error Code=%s", errCodeStr));
		int errCode = atoi(errCodeStr);
		if(errCode <= 0) continue;

		if(checked){
			m_lcList.SetCheck(i,false);
		}else{
			m_lcList.SetCheck(i,true);
		}
	}
	this->UpdateData();
}




void
CWOLDLG::_getWOLPort(CString& portStr)
{
	char buf[256];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, UBCVARS_INI);
	::GetPrivateProfileString("ROOT", "WOLPORT", "1234", buf, 256, szPath);
	portStr = buf;
}

void
CWOLDLG::_setWOLPort(CString portStr)
{
	char buf[256];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, UBCVARS_INI);
	::WritePrivateProfileString("ROOT", "WOLPORT", portStr, szPath);
}



BOOL CWOLDLG::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_btPowerOn.LoadBitmap(IDB_BITMAP_WOL_POWERON,RGB(255,255,255));
	m_btPowerOff.LoadBitmap(IDB_BITMAP_WOL_POWEROFF,RGB(255,255,255));
	m_btRefresh.LoadBitmap(IDB_BITMAP_WOL_RESCAN,RGB(255,255,255));
	m_btSave.LoadBitmap(IDB_BITMAP_WOL_SAVE,RGB(255,255,255));
	m_btExit.LoadBitmap(IDB_BITMAP_WOL_EXIT,RGB(255,255,255));

	CString wolPort="1234";
	_getWOLPort(wolPort);
	m_edit_wolPort.SetWindowText(wolPort);


	InitList();

	//_getRelayInfo();

	RefreshHostList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CWOLDLG::InitList()
{
	m_szHostColum[eCheck] = _T("");	
	m_szHostColum[eHostVNCStat] = LoadStringById(IDS_HOSTVIEW_LIST033);
	m_szHostColum[eMonitorStat] = LoadStringById(IDS_HOSTVIEW_LIST041);
	m_szHostColum[eMacAddress] = LoadStringById(IDS_HOSTVIEW_LIST019);
	m_szHostColum[eSiteName] = LoadStringById(IDS_HOSTVIEW_LIST007);
	m_szHostColum[eHostName] = LoadStringById(IDS_HOSTVIEW_LIST009);
	m_szHostColum[eHostID] = LoadStringById(IDS_HOSTVIEW_LIST008);
	m_szHostColum[eHostIP] = LoadStringById(IDS_HOSTVIEW_LIST016);
	m_szHostColum[eResult] = LoadStringById(IDS_REMOTECLICALL_STR007);
	m_szHostColum[eRelayHostName] = LoadStringById(IDS_RELAY_HOSTNAME_STR);
	m_szHostColum[eRelayHostIP] = LoadStringById(IDS_RELAY_HOSTIP_STR);
	m_szHostColum[eRelayHostID] = LoadStringById(IDS_RELAY_HOSTID_STR);
	m_szHostColum[eErrCode] = "CODE";

	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);
	//m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);

	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilHostList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilHostList.Add(&bmCheck, RGB(255, 255, 255));
	m_lcList.SetImageList(&m_ilHostList, LVSIL_SMALL);

	for(int nCol = 0; nCol < eHostEnd; nCol++)
	{
		m_lcList.InsertColumn(nCol, m_szHostColum[nCol], LVCFMT_LEFT, 100);
	}

	m_lcList.SetColumnWidth(eCheck, 22);
	m_lcList.SetColumnWidth(eHostVNCStat, 15);
	m_lcList.SetColumnWidth(eMonitorStat, 15);
	m_lcList.SetColumnWidth(eErrCode, 16);

	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
	//m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);

	m_stCounter.SetWindowText("");

}

void CWOLDLG::RefreshHostList(bool setRelayInfo/*=false*/)
{
	TraceLog(("RefreshHostList begin"));

	int nRow = 0;
	LVITEM item;
	CString szBuf;
	POSITION pos = m_lsSelHost.GetHeadPosition();
	m_lcList.DeleteAllItems();

	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsSelHost.GetNext(pos);

		if(setRelayInfo){
			_setRelayInfo(&info);
		}

		m_lcList.InsertItem(nRow, _T(""), -1);
	
		UpdateListRow(nRow, &info);

		m_lcList.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lcList.GetCurSortCol();
	if(-1 != nCol)
		m_lcList.SortList(nCol, m_lcList.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);
	else
		m_lcList.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);

	TraceLog(("RefreshHostList end"));
}

void CWOLDLG::_setRelayInfo(SHostInfo* pInfo)
{
	pInfo->hostMsg1 = "";	// hostId
	pInfo->hostMsg2 = "";	// hostName
	pInfo->location = "";	// ipAddress

	RelayHostInfoMap::iterator itr = m_relayHostMap.find(pInfo->siteId);
	if(itr!=m_relayHostMap.end()){
		SRelayHostInfo*	rInfo = (SRelayHostInfo*)itr->second;
		pInfo->hostMsg1 = rInfo->relay_hostId;
		pInfo->hostMsg2 = rInfo->relay_hostName;
		pInfo->location = rInfo->relay_ipAddress;
		//TraceLog(("Relay Info founded [%s],[%s],[%s]", pInfo->hostMsg1, pInfo->hostMsg2,pInfo->location));
		return;
	}	
	TraceLog(("Get Command Relay Info not founded"));
	return;
}

BOOL CWOLDLG::_getRelayInfo()
{

	TraceLog(("Get Command Relay Info"));

	// clear relayHostMap
	for(RelayHostInfoMap::iterator itr = m_relayHostMap.begin();itr!=m_relayHostMap.end();itr++){
		SRelayHostInfo* rInfo = (SRelayHostInfo*)itr->second;
		if(rInfo){
			delete rInfo;
		}
	}
	m_relayHostMap.clear();

	POSITION pos = m_lsSelHost.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsSelHost.GetNext(pos);

		SRelayHostInfo*	rInfo;

		RelayHostInfoMap::iterator itr = m_relayHostMap.find(info.siteId);
		if(itr!=m_relayHostMap.end()){
			rInfo = (SRelayHostInfo*)itr->second;
		}else{
			rInfo = new SRelayHostInfo();
			m_relayHostMap.insert(RelayHostInfoMap::value_type(info.siteId, rInfo));
		}
		rInfo->target_hostId_set.insert(std::set<std::string>::value_type(info.hostId));
	}	

	m_Progress.SetRange(0,m_relayHostMap.size());
	m_Progress.SetPos(0);
	m_stProgress.SetWindowText(LoadStringById(IDS_WOL_INIT_STR));
	CString stCounter;
	stCounter.Format("%d", m_relayHostMap.size());
	m_stCounter.SetWindowText(stCounter);
	
	// 오래걸리므로 Thread 로 뺀다.
	AfxBeginThread(ThreadGetRelayHostInfo, this);

	return TRUE;
}
UINT 
CWOLDLG::ThreadGetRelayHostInfo(LPVOID pParam)
{
	CWOLDLG* dlg = (CWOLDLG*)pParam;
	if(!dlg) return 0;

	TraceLog(("CWOLDLG::GetRelayHostInfo - Start"));
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	if(!CCopModule::GetObjectA()->GetRelayHostInfo(dlg->m_relayHostMap, dlg)){
		UbcMessageBox("Get Command Relay Infomation failed");
	}
	::CoUninitialize();
	TraceLog(("CWOLDLG::GetRelayHostInfo - End"));

	return 0;
}

void 
CWOLDLG::ResultComplete(bool retval) 
{
	m_stProgress.SetWindowText(LoadStringById(IDS_WOL_INIT_END_STR));
	m_hasRelayInfo = true;

	RefreshHostList(true);

	m_btPowerOn.EnableWindow(TRUE);
	m_btPowerOff.EnableWindow(TRUE);
	m_btRefresh.EnableWindow(TRUE);
	m_btSave.EnableWindow(TRUE);
	m_btExit.EnableWindow(TRUE);

	TraceLog(("Get Command Relay Succeed"));
}

void CWOLDLG::ProgressEvent(int max, int current)
{
	m_Progress.SetPos(current);
/*
	static int st_rate = 0;
	float f_rate = (float(current)/float(max))*20.0;

	int i_rate = int(f_rate);

	if(st_rate < i_rate) {
		TraceLog(("ProgressEvent(%d,%d,%d)", max,current,i_rate));
		st_rate = i_rate;
		m_Progress.SetPos(int(i_rate));
		CString rateStr;
		rateStr.Format("%d", i_rate);
		//m_Progress.EnableWindow(TRUE);
		m_Progress.UpdateData(FALSE);
		//m_Progress.SetWindowText(rateStr);

	}
*/
}



void CWOLDLG::UpdateListRow(int nRow, SHostInfo* pInfo)
{
	if(!pInfo || m_lcList.GetItemCount() < nRow)
		return;

	// 0000707: 콘텐츠 다운로드 상태 조회 기능
    COLORREF crTextBk = COLOR_WHITE;
	if(pInfo->operationalState)
	{
		crTextBk = COLOR_YELLOW;
	}
	else
	{
		crTextBk = COLOR_GRAY;
	}
	m_lcList.SetRowColor(nRow, crTextBk, m_lcList.GetTextColor());

	CString szBuf;
	
	m_lcList.SetCheck(nRow,true);

	m_lcList.SetItemText(nRow, eSiteName, pInfo->siteName);
	m_lcList.SetItemText(nRow, eHostID, pInfo->hostId);
	m_lcList.SetItemText(nRow, eHostName, pInfo->hostName);
	m_lcList.SetItemText(nRow, eMacAddress, pInfo->macAddress);
	m_lcList.SetItemText(nRow, eHostIP, pInfo->ipAddress);
	m_lcList.SetItemText(nRow, eRelayHostName, pInfo->hostMsg2); // hostMsg2 에 중계단말 Name 을 넣어놓는다.
	m_lcList.SetItemText(nRow, eRelayHostIP, pInfo->location); // location 에 중계단말 IP 을 넣어놓는다.
	m_lcList.SetItemText(nRow, eRelayHostID, pInfo->hostMsg1);  // hostMsg1 에 중계단말 ID 를 넣어놓는다.
	//m_lcList.SetItemText(nRow, eErrCode, "");  // 

	// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	//bool bMonitorStat = utvUtil::getInstance()->monitorState(pInfo->monitorOff, pInfo->monitorOffList);
	int nMonitorImgIdx = 2;
	if(pInfo->operationalState)
	{
		if(pInfo->monitorState > 0) nMonitorImgIdx = eMonitorOn;
		if(pInfo->monitorState == 0) nMonitorImgIdx = eMonitorOff;
		if(pInfo->monitorState < 0) nMonitorImgIdx = eMonitorUnknown;
	}

	LVITEM itemMonitorStat;
	itemMonitorStat.iItem = nRow;
	itemMonitorStat.iSubItem = eMonitorStat;//m_lcList.GetColPos(m_szHostColum[eMonitorStat]);
	itemMonitorStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemMonitorStat.pszText = (nMonitorImgIdx == eMonitorUnknown ? _T("Unknown") : (nMonitorImgIdx == eMonitorOn ? _T("On") : _T("Off")));
	itemMonitorStat.iImage = nMonitorImgIdx;
	m_lcList.SetItem(&itemMonitorStat);

	LVITEM itemHostVNCStat;
	itemHostVNCStat.iItem = nRow;
	itemHostVNCStat.iSubItem = eHostVNCStat;//m_lcList.GetColPos(m_szHostColum[eHostVNCStat]);
	itemHostVNCStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemHostVNCStat.pszText = (pInfo->vncState || pInfo->operationalState ? _T("On") : _T("Off"));
	itemHostVNCStat.iImage = (pInfo->vncState || pInfo->operationalState ? eVncOn : eVncOff);
	m_lcList.SetItem(&itemHostVNCStat);

	_setErrorCode(nRow,"",eInit);
}

int
CWOLDLG::_runCLI(const char* cmd, int ip_column_index/*=eHostIP*/)
{
	TraceLog(("_runCLI(%s)", cmd));

	CString wolPort;
	m_edit_wolPort.GetWindowText(wolPort);
	if(wolPort.IsEmpty()){
		wolPort = "1234";
	}

	m_progress_counter = 0;
	m_CliTransfer->DeleteAllItem();

	CString strCmd = cmd;
	m_CliTransfer->SetCommand(strCmd);


	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		if(!m_lcList.GetCheck(i)){
			continue;
		}

		CString strIp = m_lcList.GetItemText(i, ip_column_index);
		if(strIp.IsEmpty())
		{
			if(ip_column_index == eRelayHostIP){
				m_lcList.SetItemText(i, eResult, LoadStringById(IDS_NO_RELAY_HOST));
			}else{
				m_lcList.SetItemText(i, eResult, LoadStringById(IDS_REMOTECLICALL_STR003));
			}
			_setErrorCode(i,"-9", -9);

			continue;
		}

		if(ip_column_index == eRelayHostIP){  // macAddress wolPort 를 인수로 넣는다.
			CString strMacAddress = m_lcList.GetItemText(i, eMacAddress);
			if(strMacAddress.IsEmpty())
			{
				m_lcList.SetItemText(i, eResult, LoadStringById(IDS_NO_WOL_MACADDESS));
				//m_lcList.SetItemText(i, eErrCode, "-9");
				_setErrorCode(i, "-9", -9);
				continue;
			}
			strMacAddress.Replace("-","");
			CString arg = strMacAddress;
			arg.Append(" ");
			arg.Append(wolPort);
			CString command = "Send: ";

			command.Append(cmd);
			if(arg[0] != 0){  //skpark 2013.4.26 command 에 ip 별로 다른 arg 가 있을 수 있다.
				command.Append(" ");
				command.Append(arg);
			}
			TraceLog((command));

			m_lcList.SetItemText(i, eResult, "");
			//m_lcList.SetItemText(i, eErrCode, "");
			_setErrorCode(i,"",eInit);

			m_CliTransfer->AddItem(strIp,arg);

		}else{
			m_lcList.SetItemText(i, eResult, "");
			//m_lcList.SetItemText(i, eErrCode, "");
			_setErrorCode(i,"",eInit);
			m_CliTransfer->AddItem(strIp);
		}
	}

	int counter = m_CliTransfer->GetCount();
	CString stCounter;
	stCounter.Format("%d", counter);
	m_stCounter.SetWindowText(stCounter);

	if( counter > 0)
	{
		m_Progress.SetRange(0, counter);
		m_Progress.SetPos(0);
		m_stProgress.SetWindowText(LoadStringById(IDS_WOL_START_STR));

		m_bIsPlaying = true;
		if(m_CliTransfer->Run())
		{
		
			m_btPowerOn.EnableWindow(FALSE);
			m_btPowerOff.EnableWindow(FALSE);
			m_btRefresh.EnableWindow(FALSE);
			m_btSave.EnableWindow(FALSE);
			m_btExit.LoadBitmap(IDB_BITMAP_WOL_STOP,RGB(255,255,255));
			m_btExit.SetWindowText(LoadStringById(IDS_REMOTECLICALL_STR002));
		}
	}

	TraceLog(("_runCLI(%s,%d) end" , cmd, counter));
	return counter;
}

void CWOLDLG::CLIBeforeEvent(CCLITransfer::ITEM item, LPCTSTR cmd)
{
	TraceLog(("CLIBeforeEvent(%s)" , cmd));

	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		TraceLog(("CLIBeforeEvent(%s)" , item.szIp));

		if(item.arg[0] != 0){
			CString strMacAddress = m_lcList.GetItemText(i, eMacAddress);
			TraceLog(("CLIBeforeEvent(%s ??? %s) " , item.arg, strMacAddress));

			if(strMacAddress.IsEmpty()) continue;
			strMacAddress.Replace("-","");
			if(strncmp(strMacAddress,item.arg, strMacAddress.GetLength())!=0) continue;

			CString strIp = m_lcList.GetItemText(i, eRelayHostIP);
			if(strIp.IsEmpty()) continue;
			if(strIp != item.szIp) continue;

		}else{
		
			CString strIp = m_lcList.GetItemText(i, eHostIP);
			if(strIp.IsEmpty()) continue;
			if(strIp != item.szIp) continue;

		}
		TraceLog(("CLIBeforeEvent(%s,%s) matched" , item.szIp, item.arg));

		CString strMsg = "Send : " ;
		strMsg.Append(cmd);

		m_lcList.SetItemText(i, eResult, strMsg);
		//m_lcList.SetItemText(i, eErrCode, "");
		_setErrorCode(i,"",eInit);

		break;
	}
}

void CWOLDLG::CLIResultEvent(CCLITransfer::ITEM item)
{
	TraceLog(("CLIResultEvent(%s,%s)" , item.szIp, item.arg));

	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		if(item.arg[0] != 0){

			CString strMacAddress = m_lcList.GetItemText(i, eMacAddress);
			if(strMacAddress.IsEmpty()) continue;
			strMacAddress.Replace("-","");
			if(strncmp(strMacAddress,item.arg, strMacAddress.GetLength())!=0) continue;

			CString strIp = m_lcList.GetItemText(i, eRelayHostIP);
			if(strIp.IsEmpty()) continue;
			if(strIp != item.szIp) continue;

		}else{
		
			CString strIp = m_lcList.GetItemText(i, eHostIP);
			if(strIp.IsEmpty()) continue;
			if(strIp != item.szIp) continue;

		}
		TraceLog(("CLIResultEvent(%s,%s) matched" , item.szIp, item.arg));

		CString strMsg;
		if(item.nResult < 0) strMsg.Format(_T("%s [%d]"), LoadStringById(IDS_REMOTECLICALL_STR004), item.nResult);
		if(item.nResult == 0) strMsg.Format(_T("%s [%d]"), LoadStringById(IDS_REMOTECLICALL_STR005), item.nResult);
		if(item.nResult > 0) strMsg.Format(_T("%s [%d]"), LoadStringById(IDS_REMOTECLICALL_STR006), item.nResult);

		m_lcList.SetItemText(i, eResult, strMsg);

		CString strErrCode;
		strErrCode.Format(_T("%d"), item.nResult);
		//m_lcList.SetItemText(i, eErrCode, strErrCode);
		_setErrorCode(i,strErrCode,item.nResult);


		m_progress_counter++;
		m_Progress.SetPos(m_progress_counter);

		break;
	}
}

void CWOLDLG::CLIFinishEvent()
{
	TraceLog(("CLIFinishEvent()"));
	m_Progress.SetPos(m_progress_counter);
	m_stProgress.SetWindowText(LoadStringById(IDS_WOL_END_STR));

	m_btPowerOn.EnableWindow(TRUE);
	m_btPowerOff.EnableWindow(TRUE);
	m_btRefresh.EnableWindow(TRUE);
	m_btSave.EnableWindow(TRUE);
	m_btExit.LoadBitmap(IDB_BITMAP_WOL_EXIT,RGB(255,255,255));
	m_btExit.SetWindowText(LoadStringById(IDS_REMOTECLICALL_STR002));

	m_CliTransfer->setHandler(0);
	Sleep(300); // n_bIsPlaying 이 false 가 되면 this 객체가 destroy 되므로, 그전에 thread 들의 핸들러 값을 0 으로 할 기회를 준다.
	m_bIsPlaying = false;
	//UbcMessageBox(LoadStringById(IDS_REMOTECLICALL_MSG002));
}

void CWOLDLG::_setErrorCode(int nRow, LPCTSTR tooltipText, int errCode)
{
	int imageIdx = eInit;
	switch(errCode) {
		case -1: imageIdx = eConnectFail; break;
		case 0: imageIdx = eCommandFail; break;
		case 1 : imageIdx = eSucceed; break;
		case -9: imageIdx = eNA; break;
	};

	LVITEM itemErrorCode;
	itemErrorCode.iItem = nRow;
	itemErrorCode.iSubItem = eErrCode;//m_lcList.GetColPos(m_szHostColum[eHostVNCStat]);
	itemErrorCode.mask = LVIF_IMAGE|LVIF_TEXT;
	itemErrorCode.pszText = (LPSTR)tooltipText;
	itemErrorCode.iImage = imageIdx;
	m_lcList.SetItem(&itemErrorCode);
}

void CWOLDLG::OnDestroy()
{
	TraceLog(("OnDestroy"));
	this->m_CliTransfer->Stop();
	CDialog::OnDestroy();
}
