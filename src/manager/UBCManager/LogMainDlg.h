#pragma once
#include "afxcmn.h"
#include "resource.h"
#include "Enviroment.h"
//#include "common/UBCTabCtrl.h"
#include "common/HoverButton.h"
//#include "SubLogTab.h"
#include "afxwin.h"
#include "afxdtctl.h"
#include "common/utblistctrlex.h"

class CLogMainDlg;

class CSubLog {
public:
	CSubLog(CLogMainDlg* p) : m_parentDlg(p), m_alreadyInited(false) {}
	virtual ~CSubLog() {}
	virtual void InitList() = 0;
	virtual void RefreshList() = 0;

protected:
	CLogMainDlg*				m_parentDlg;
	bool					m_alreadyInited;
};

// 컨텐츠 패키지 변경명령 로그
class CSubApplyLog : public virtual CSubLog{
public:
	CSubApplyLog(CLogMainDlg* p) : CSubLog(p) {}
	virtual ~CSubApplyLog() {}
protected:
	enum { eSite, eHost, ePackage, eApplyTime, eHow, eWho, eWhy, eMaxCol };

	CString						m_LogColumn[eMaxCol];
	ApplyLogInfoList			m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();

};
// 컨텐츠 패키지 변경결과 로그
class CSubScheduleStateLog : public virtual CSubLog{
public:
	CSubScheduleStateLog(CLogMainDlg* p) : CSubLog(p) {}
	virtual ~CSubScheduleStateLog() {}
protected:
	enum { eSite, eHost, eEventTime, eLastSchedule1, eAutoSchedule1, eCurrentSchedule1, eMaxCol };

	CString						m_LogColumn[eMaxCol];
	ScheduleStateLogInfoList			m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();

};

// 전원On/Off 로그
class CSubPowerStateLog : public virtual CSubLog{
public:
	CSubPowerStateLog(CLogMainDlg* p) : CSubLog(p) {}
	virtual ~CSubPowerStateLog() {}
protected:
	enum { eSite, eHost, eEventTime, eBootUpTime, eBootDownTime, eMaxCol };

	CString						m_LogColumn[eMaxCol];

	PowerStateLogInfoList	m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();

};

// Connection 로그
class CSubConnectionStateLog : public virtual CSubLog{
public:
	CSubConnectionStateLog(CLogMainDlg* p) : CSubLog(p) {}
	virtual ~CSubConnectionStateLog() {}
protected:
	enum { eSite, eHost, eEventTime, eOperationalState, eMaxCol };

	CString						m_LogColumn[eMaxCol];

	ConnectionStateLogInfoList	m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();

};

// DownloadSummary 로그
class CSubDownloadSummaryLog : public virtual CSubLog{
public:
	CSubDownloadSummaryLog(CLogMainDlg* p) : CSubLog(p) {}
	virtual ~CSubDownloadSummaryLog() {}
protected:
	enum { eSiteName,eHostName,eProgramId,eBrwId,
		eProgramState,eProgramStartTime,eProgramEndTime,eProgress,eMaxCol };

	CString						m_LogColumn[eMaxCol];

	DownloadSummaryInfoList		m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();

};



// CLogMainDlg 대화 상자입니다.

class CLogMainDlg : public CDialog
{
	DECLARE_DYNAMIC(CLogMainDlg)

public:
	CLogMainDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLogMainDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LOG_MAIN };

	void SetDefaultTab(int p) 
	{
		m_defaultTab = p;
	}
	void SetParam(CString strHostId, CString strHostName)
	{
		m_strHostId = strHostId;
		m_strHostName = strHostName;
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CTabCtrl m_hostLogTabCtrl;
	virtual BOOL OnInitDialog();

protected:
	void InitTab();
	void InitList();
	void RefreshList();
	void ToggleTab(int nIndex);
	//CSubLogTab* CreateSubWindow(int nID);

	enum SUB_WIN   { eApplyLog,eScheduleStateLog,
					ePowerStateLog,eConnectionStateLog,
					eDownloadSummaryLog,eMaxTab };

	int						m_defaultTab;
	CString					m_title[eMaxTab];
	CSubLog*				m_subLog[eMaxTab];
	CUTBListCtrlEx*			m_listCtrl[eMaxTab];

	CString						m_strHostName;
	CString						m_strHostId;

public:
	// 컨텐츠 패키지 변경명령 로그
	friend class CSubApplyLog;
	CUTBListCtrlEx	m_ApplyLogListCtrl;
	
	// 컨텐츠 패키지 변경결과 로그
	friend class CSubScheduleStateLog;
	CUTBListCtrlEx m_ScheduleStateLogListCtrl;

	// PowerOnOff 로그
	friend class CSubPowerStateLog;
	CUTBListCtrlEx m_PowerStateLogListCtrl;

	// Connection 로그
	friend class CSubConnectionStateLog;
	CUTBListCtrlEx m_ConnectionStateLogListCtrl;

	// DownloadSummary 로그
	friend class CSubDownloadSummaryLog;
	CUTBListCtrlEx m_DownloadSummaryLogListCtrl;

	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnBnClickedButtonFilterHost();
	afx_msg void OnBnClickedBnToExcel();
	afx_msg void OnBnClickedButtonClose();
	CHoverButton m_bnRefresh;
	CHoverButton m_bnClose;
	CHoverButton m_btnExcelSave;
	CDateTimeCtrl m_dtFilterStart;
	CDateTimeCtrl m_dtFilterEnd;
	afx_msg void OnTcnSelchangeHostlogTab(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTcnSelchangingHostlogTab(NMHDR *pNMHDR, LRESULT *pResult);
	CStatic m_packageLabel;
	CEdit m_filterPackage;
	CButton m_btFilterPackage;
	CButton m_btFilterHost;
	CStatic m_labelFilterHost;
};

