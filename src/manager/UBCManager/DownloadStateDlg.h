#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "Enviroment.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"

// CDownloadStateDlg 대화 상자입니다.

class CDownloadStateDlg : public CDialog
{
	DECLARE_DYNAMIC(CDownloadStateDlg)

public:
	CDownloadStateDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDownloadStateDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DOWNLOAD_STATE };

	enum { eStatus, eBrwId, ePackage, eContentsName, eLocation, eFileName, eProgress, eStartTime, eEndTime, eReason, eMaxCol };

	CString					BRW_NAME[2];
	CString					m_szColum[eMaxCol];
	DownloadStateInfoList	m_lsInfoList;

	CUTBListCtrlEx			m_lcList;
	CImageList				m_ilListImage;
	CStatic					m_gbGroup;

	void					InitList();
	void					RefreshList();
	CString					GetReasonString(int nValue);
	CString					GetStateString(int nValue);

	void					InitPosition(CRect rc);
	CRect					m_rcClient;
	bool					m_bInitialize;
	CReposControl			m_Reposition;

	// Input Param
	//CString					m_strSiteId;
	//CString					m_strHostId;
	//CString					m_strPackageId;
	//int						m_nBrwId;
	SDownloadSummaryInfo	m_Info;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
};
