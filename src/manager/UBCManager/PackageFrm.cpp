// PackageFrm.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "PackageFrm.h"


// CPackageFrm

IMPLEMENT_DYNCREATE(CPackageFrm, CMDIChildWnd)

CPackageFrm::CPackageFrm()
{
}

CPackageFrm::~CPackageFrm()
{
}


BEGIN_MESSAGE_MAP(CPackageFrm, CMDIChildWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CPackageFrm message handlers

int CPackageFrm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDI_PACKAGE), false);

	return 0;
}
