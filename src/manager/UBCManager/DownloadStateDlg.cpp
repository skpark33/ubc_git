// DownloadStateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "DownloadStateDlg.h"


#define COLOR_FAIL			RGB(176,0,0)
#define COLOR_BAR_FORE		::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_BAR_BACK		::GetSysColor(COLOR_BTNFACE)
#define COLOR_BAR_TEXT		::GetSysColor(COLOR_WINDOWTEXT)

//다운로드 결과
enum E_DOWNLOAD_REASON
{
	E_REASON_INIT,    //초기상태
	E_REASON_CONNECTION_FAIL, //연결을 실패함
	E_REASON_FILE_NOTFOUND,  //서버에 파일이 존재하지 않음
	E_REASON_EXCEPTION_FTP,  //FTP 오류
	E_REASON_EXCEPTION_FILE, //파일액세스 오류
	E_REASON_LOCAL_EXIST,  //로컬에 존재하여 다운로드 하지 않음
	E_REASON_DOWNLOAD_SUCCESS, //다운로드 성공
	E_REASON_UNKNOWN,   //알수없는 오류(연결끊김, exception...)
	E_REASON_COPY_FAIL			//다운로드는 성공하였으나 ENC폴더 복사에 실패함.
};

//다운로드 결과
enum E_DOWNLOAD_STATE
{
	E_STATE_INIT			,  //초기상태
	E_STATE_DOWNLOADING		,  //다운로드 상태
	E_STATE_PARTIAL_FAIL	,  //일부파일 실패
	E_STATE_COMPLETE_FAIL	,  //전체 실패(접속실패등...)
	E_STATE_COMPLETE_SUCCESS,  //전체 성공
	E_STATE_COMPLETE_LOCAL_EXIST	//패키지의 모든컨텐츠 파일이 로컬에 존재하여 다운로드하지 않음
};

// 상태를 나타내는 이미지리스트 인덱스
enum E_STATE_BMP_INDEX
{
	E_STATE_IDX_OK
,	E_STATE_IDX_BLOCK
,	E_STATE_IDX_ERROR
,	E_STATE_IDX_WARNING
,	E_STATE_IDX_ING
,	E_STATE_IDX_MAX_COUNT
};


// CDownloadStateDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDownloadStateDlg, CDialog)

CDownloadStateDlg::CDownloadStateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDownloadStateDlg::IDD, pParent)
	, m_Reposition(this)
	, m_bInitialize(false)
{
	BRW_NAME[0] = LoadStringById(IDS_DOWNLOADSTATE_STR001);
	BRW_NAME[1] = LoadStringById(IDS_DOWNLOADSTATE_STR002);
}

CDownloadStateDlg::~CDownloadStateDlg()
{
}

void CDownloadStateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_lcList);
	DDX_Control(pDX, IDC_GROUP_BOX, m_gbGroup);
}


BEGIN_MESSAGE_MAP(CDownloadStateDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CDownloadStateDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CDownloadStateDlg::OnBnClickedCancel)
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CDownloadStateDlg 메시지 처리기입니다.

void CDownloadStateDlg::OnBnClickedOk()
{
	RefreshList();
}

void CDownloadStateDlg::OnBnClickedCancel()
{
	OnCancel();
}

BOOL CDownloadStateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_TXT_SITE_ID)->SetWindowText(m_Info.siteName);
	GetDlgItem(IDC_TXT_HOST_ID)->SetWindowText(m_Info.hostName);

	InitPosition(m_rcClient);

	InitList();
	RefreshList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDownloadStateDlg::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);

	m_szColum[eStatus      ] = LoadStringById(IDS_DOWNLOADSTATE_LIST001);
	m_szColum[eBrwId       ] = LoadStringById(IDS_DOWNLOADSTATE_LIST002);
	m_szColum[ePackage     ] = LoadStringById(IDS_DOWNLOADSTATE_LIST003);
	m_szColum[eContentsName] = LoadStringById(IDS_DOWNLOADSTATE_LIST004);
	m_szColum[eLocation    ] = LoadStringById(IDS_DOWNLOADSTATE_LIST005);
	m_szColum[eFileName    ] = LoadStringById(IDS_DOWNLOADSTATE_LIST006);
	m_szColum[eProgress    ] = LoadStringById(IDS_DOWNLOADSTATE_LIST007);
	m_szColum[eStartTime   ] = LoadStringById(IDS_DOWNLOADSTATE_LIST008);
	m_szColum[eEndTime     ] = LoadStringById(IDS_DOWNLOADSTATE_LIST009);
	m_szColum[eReason      ] = LoadStringById(IDS_DOWNLOADSTATE_LIST010);

	m_lcList.InsertColumn(eStatus      , m_szColum[eStatus      ], LVCFMT_CENTER, 24);
	m_lcList.InsertColumn(eBrwId       , m_szColum[eBrwId       ], LVCFMT_CENTER, 30);
	m_lcList.InsertColumn(ePackage     , m_szColum[ePackage     ], LVCFMT_LEFT  , 100);
	m_lcList.InsertColumn(eContentsName, m_szColum[eContentsName], LVCFMT_LEFT  , 100);
	m_lcList.InsertColumn(eLocation    , m_szColum[eLocation    ], LVCFMT_LEFT  , 100);
	m_lcList.InsertColumn(eFileName    , m_szColum[eFileName    ], LVCFMT_LEFT  , 100);
	m_lcList.InsertColumn(eProgress    , m_szColum[eProgress    ], LVCFMT_CENTER, 150);
	m_lcList.InsertColumn(eStartTime   , m_szColum[eStartTime   ], LVCFMT_CENTER, 110);
	m_lcList.InsertColumn(eEndTime     , m_szColum[eEndTime     ], LVCFMT_CENTER, 110);
	m_lcList.InsertColumn(eReason      , m_szColum[eReason      ], LVCFMT_CENTER, 200);

	m_lcList.SetProgressBarCol(eProgress);

	//
	CBitmap bmp1, bmp2;
	bmp1.LoadBitmap(IDB_OK_BLOCK_ERROR_WARNING);
	bmp2.LoadBitmap(IDB_CONTENTS_LIST16);

	CBitmap bmp3;
	bmp3.LoadBitmap(IDB_CONTENTS_LIST16_ERROR);

	CBitmap bmp4;
	bmp4.LoadBitmap(IDB_CONTENTS_LIST16_SERVER);

	if(m_ilListImage.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilListImage.Add(&bmp1, RGB(255,255,255));
		m_ilListImage.Add(&bmp2, RGB(255,255,255));
		m_ilListImage.Add(&bmp3, RGB(255,255,255));
		m_ilListImage.Add(&bmp4, RGB(255,255,255));
		m_lcList.SetImageList(&m_ilListImage, LVSIL_SMALL);
	}

	// 제일 마지막에 할것...
	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcList.LoadColumnState("DOWNLOAD_STATE-LIST", szPath);
}

void CDownloadStateDlg::RefreshList()
{
	m_lsInfoList.RemoveAll();

	CTime tmStart = CTime(m_Info.programStartTime);

	CString strWhere;
	strWhere.Format(_T("programId = '%s' and brwId = %d and startTime >= '%s' order by downloadId, programId, brwId")
				  , m_Info.programId
				  , m_Info.brwId
				  , (m_Info.programStartTime == 0 ? "" : tmStart.Format("%Y/%m/%d %H:%M:%S"))
				  );

	CCopModule::GetObject()->GetDownloadState(m_Info.siteId
											, m_Info.hostId
											, m_lsInfoList
											, strWhere
											);

	m_lcList.DeleteAllItems();

	CString strStateSummary1;
	CString strStateSummary2;

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SDownloadStateInfo info = m_lsInfoList.GetNext(pos);

		CTime tmStart = CTime(info.startTime);
		CTime tmEnd   = CTime(info.endTime  );

		int nStateIdx = -1;
		if(info.result)
		{
			nStateIdx = 0;
		}
		else
		{
			if( info.downState == E_STATE_PARTIAL_FAIL  ||
				info.downState == E_STATE_COMPLETE_FAIL )
			{
				nStateIdx = 2;
			}
			else if(info.downState == E_STATE_DOWNLOADING)
			{
				nStateIdx = 4;
			}
		}

		if(info.brwId == 0 && strStateSummary1.IsEmpty())
		{
			strStateSummary1.Format(_T("%s : %s %s"), BRW_NAME[info.brwId], info.progress, GetStateString(info.programState));
		}

		if(info.brwId == 1 && strStateSummary2.IsEmpty())
		{
			strStateSummary2.Format(_T("%s : %s %s"), BRW_NAME[info.brwId], info.progress, GetStateString(info.programState));
		}

		m_lcList.InsertItem(nRow, "", nStateIdx);
		m_lcList.SetItemText(nRow, eBrwId       , BRW_NAME[info.brwId] );
		m_lcList.SetItemText(nRow, ePackage     , info.programId       );
		m_lcList.SetItemText(nRow, eContentsName, info.contentsName    );
		m_lcList.SetItemText(nRow, eLocation    , info.location        );
		m_lcList.SetItemText(nRow, eFileName    , info.filename        );
		m_lcList.SetItemText(nRow, eStartTime   , (info.startTime == 0 ? "" : tmStart.Format("%Y/%m/%d %H:%M:%S")));
		m_lcList.SetItemText(nRow, eEndTime     , (info.endTime   == 0 ? "" : tmEnd  .Format("%Y/%m/%d %H:%M:%S")));
		m_lcList.SetItemText(nRow, eReason      , GetReasonString(info.reason));

		int nPercent = (info.volume <= 0 ? 0 : (info.currentVolume * 100) / info.volume);

		CString strDownState;
		strDownState.Format("%s / %s", ToFileSize(info.currentVolume, 'A'), ToFileSize(info.volume, 'A'));
		m_lcList.SetProgressBarText( nRow, eProgress
									, nPercent
									, DT_CENTER
									, strDownState
									, TRUE
									, (!info.result ? COLOR_FAIL : COLOR_BAR_FORE)
									, COLOR_BAR_BACK
									, COLOR_BAR_TEXT
									);

		LVITEM item;
		item.iItem = nRow;
		item.iSubItem = eContentsName;
		item.mask = LVIF_IMAGE;
		item.iImage = info.contentsType + 1 + E_STATE_IDX_MAX_COUNT;
		m_lcList.SetItem(&item);

		m_lcList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	GetDlgItem(IDC_TXT_STATE)->SetWindowText(strStateSummary1 + (strStateSummary1.IsEmpty() || strStateSummary2.IsEmpty() ? "" : _T(",")) + strStateSummary2);
}

void CDownloadStateDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcList.SaveColumnState("DOWNLOAD_STATE-LIST", szPath);
}

CString CDownloadStateDlg::GetReasonString(int nValue)
{
	switch(nValue)
	{
	case E_REASON_INIT			   : return LoadStringById(IDS_DOWNLOADSTATE_STR003); //초기상태
	case E_REASON_CONNECTION_FAIL  : return LoadStringById(IDS_DOWNLOADSTATE_STR004); //연결을 실패함
	case E_REASON_FILE_NOTFOUND	   : return LoadStringById(IDS_DOWNLOADSTATE_STR005); //서버에 파일이 존재하지 않음
	case E_REASON_EXCEPTION_FTP    : return LoadStringById(IDS_DOWNLOADSTATE_STR006); //FTP 오류
	case E_REASON_EXCEPTION_FILE   : return LoadStringById(IDS_DOWNLOADSTATE_STR007); //파일액세스 오류
	case E_REASON_LOCAL_EXIST      : return LoadStringById(IDS_DOWNLOADSTATE_STR008); //로컬에 존재하여 다운로드 하지 않음
	case E_REASON_DOWNLOAD_SUCCESS : return LoadStringById(IDS_DOWNLOADSTATE_STR009); //다운로드 성공
	case E_REASON_UNKNOWN	       : return LoadStringById(IDS_DOWNLOADSTATE_STR010); //알수없는 오류(연결끊김, exception...)
	case E_REASON_COPY_FAIL        : return LoadStringById(IDS_DOWNLOADSTATE_STR018); //알수없는 오류(연결끊김, exception...)
	}

	return _T("");
}

CString CDownloadStateDlg::GetStateString(int nValue)
{
	switch(nValue)
	{
	case E_STATE_INIT			     : return LoadStringById(IDS_DOWNLOADSTATE_STR011); //초기상태
	case E_STATE_DOWNLOADING	     : return LoadStringById(IDS_DOWNLOADSTATE_STR012); //다운로드 상태
	case E_STATE_PARTIAL_FAIL	     : return LoadStringById(IDS_DOWNLOADSTATE_STR013); //일부파일 실패
	case E_STATE_COMPLETE_FAIL	     : return LoadStringById(IDS_DOWNLOADSTATE_STR014); //전체 실패
	case E_STATE_COMPLETE_SUCCESS    : return LoadStringById(IDS_DOWNLOADSTATE_STR015); //전체 성공
	case E_STATE_COMPLETE_LOCAL_EXIST: return LoadStringById(IDS_DOWNLOADSTATE_STR015); //전체 성공
	}

	return _T("");
}

int CDownloadStateDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}

void CDownloadStateDlg::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lcList, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_gbGroup, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
}

void CDownloadStateDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CDownloadStateDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	CDialog::OnGetMinMaxInfo(lpMMI);

	//lpMMI->ptMinTrackSize.x = 675;
	//lpMMI->ptMinTrackSize.y = 400;
	if(m_bInitialize)
	{
		lpMMI->ptMinTrackSize.x = m_rcClient.Width();
		lpMMI->ptMinTrackSize.y = m_rcClient.Height();
	}
	else
	{
		m_bInitialize = true;
	}
}
