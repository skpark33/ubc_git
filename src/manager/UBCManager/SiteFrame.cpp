// SiteFrame.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "SiteFrame.h"


// CSiteFrame

IMPLEMENT_DYNCREATE(CSiteFrame, CMDIChildWnd)

CSiteFrame::CSiteFrame()
{

}

CSiteFrame::~CSiteFrame()
{
}


BEGIN_MESSAGE_MAP(CSiteFrame, CMDIChildWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CSiteFrame message handlers

int CSiteFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDR_SITEFRAME), false);

	return 0;
}
