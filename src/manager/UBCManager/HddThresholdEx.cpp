// HddThresholdEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Enviroment.h"
#include "HddThresholdEx.h"
#include "common\ubcdefine.h"
#include "ubccopcommon/PackageSelectDlg.h"

#include "common/libCommon/ubcUtil.h"
#include "common\UbcCode.h"
#include "common/UbcGUID.h"


// CHddThresholdEx 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHddThresholdEx, CDialog)

CHddThresholdEx::CHddThresholdEx(CWnd* pParent /*=NULL*/)
	: CDialog(CHddThresholdEx::IDD, pParent)
	, m_nSelect(0)
{

}

CHddThresholdEx::~CHddThresholdEx()
{
}

void CHddThresholdEx::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_THRESHOLD_RB_SELECT1, m_nSelect);
	DDX_Control(pDX, IDC_LC_LIST, m_lscPackage);
	DDX_Control(pDX, IDC_EB_FILEPATH_LIST, m_txtFilepathList);
	DDX_Control(pDX, IDC_THRESHOLD_RB_SELECT5, allRadioBtn);
	DDX_Control(pDX, IDC_THRESHOLD_RB_SELECT2, excludeRadioBtn);
	DDX_Control(pDX, IDC_THRESHOLD_RB_SELECT1, validRadioBtn);
}


BEGIN_MESSAGE_MAP(CHddThresholdEx, CDialog)
	ON_BN_CLICKED(IDC_BN_PACKAGE_SELECT, &CHddThresholdEx::OnBnClickedBnPackageSelect)
	ON_BN_CLICKED(IDOK, &CHddThresholdEx::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CHddThresholdEx::OnBnClickedCancel)
END_MESSAGE_MAP()


// CHddThresholdEx 메시지 처리기입니다.

BOOL CHddThresholdEx::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitPackageList();

	char buf[MAX_PATH* 10] = {0};
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	m_nSelect = GetPrivateProfileInt("HDDTHRESHOLD_DLG", "Select", 0, szPath);
	GetPrivateProfileString("HDDTHRESHOLD_DLG", "FilepathList", "", buf, MAX_PATH* 10, szPath);
	CString strFilepathList = buf;
	strFilepathList.Replace("|", "\r\n");
	m_txtFilepathList.SetWindowText(strFilepathList);

	allRadioBtn.EnableWindow(false);
	excludeRadioBtn.EnableWindow(false);
	validRadioBtn.EnableWindow(false);


	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CHddThresholdEx::InitPackageList()
{
	// 2010.02.17 by gwangsoo
	m_szColName[eSite        ] = LoadStringById(IDS_PACKAGEVIEW_LST001);
	m_szColName[ePackage     ] = LoadStringById(IDS_PACKAGEVIEW_LST002);
	m_szColName[eUser        ] = LoadStringById(IDS_PACKAGEVIEW_LST003);
	m_szColName[eTime        ] = LoadStringById(IDS_PACKAGEVIEW_LST004);
	m_szColName[eDesc        ] = LoadStringById(IDS_PACKAGEVIEW_LST005);
	m_szColName[eCategory    ] = LoadStringById(IDS_PACKAGEVIEW_LST006);
	m_szColName[ePurpose     ] = LoadStringById(IDS_PACKAGEVIEW_LST007);
	m_szColName[eHostType    ] = LoadStringById(IDS_PACKAGEVIEW_LST008);
	m_szColName[eVertical    ] = LoadStringById(IDS_PACKAGEVIEW_LST009);
	m_szColName[eResolution  ] = LoadStringById(IDS_PACKAGEVIEW_LST010);
	m_szColName[ePublic      ] = LoadStringById(IDS_PACKAGEVIEW_LST012);
	m_szColName[eVerify      ] = LoadStringById(IDS_PACKAGEVIEW_LST011);
	m_szColName[eValidateDate] = LoadStringById(IDS_PACKAGEVIEW_LST013);
	m_szColName[eVolume      ] = LoadStringById(IDS_PACKAGEVIEW_LST014);

	m_lscPackage.SetExtendedStyle(m_lscPackage.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);

	for(int nCol = 0; nCol < eEnd; nCol++)
	{
		if(nCol == eVolume)
		{
			m_lscPackage.InsertColumn(nCol, m_szColName[nCol], LVCFMT_RIGHT, 70);
		}
		else
		{
			m_lscPackage.InsertColumn(nCol, m_szColName[nCol], LVCFMT_LEFT, 100);
		}
	}

	m_lscPackage.SetColumnWidth(ePackage, 140);
	m_lscPackage.SetColumnWidth(eTime, 120);
	m_lscPackage.SetColumnWidth(eDesc, 120);
	m_lscPackage.SetColumnWidth(eValidateDate, 120);

	m_lscPackage.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CHddThresholdEx::OnBnClickedBnPackageSelect()
{
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		return;
	}

	m_lscPackage.DeleteAllItems();

	PackageInfoList& list = dlg.GetSelectedPackageList();

	int nRow = 0;
	POSITION pos = list.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SPackageInfo& info = list.GetNext(pos);

		m_lscPackage.InsertItem(nRow, "");

		m_lscPackage.SetItemText(nRow, eSite        , info.szSiteID    );
		m_lscPackage.SetItemText(nRow, ePackage     , info.szPackage   );
		m_lscPackage.SetItemText(nRow, eUser        , info.szUpdateUser);
		m_lscPackage.SetItemText(nRow, eDesc        , info.szDescript  );
		m_lscPackage.SetItemText(nRow, eVolume      , (info.volume < 0 ? "" : ToFileSize((ULONGLONG)info.volume, 'G')));
		m_lscPackage.SetItemText(nRow, eCategory    , CUbcCode::GetInstance()->GetCodeName("Kind"    , info.nContentsCategory));
		m_lscPackage.SetItemText(nRow, ePurpose     , CUbcCode::GetInstance()->GetCodeName("Purpose"    , info.nPurpose         ));
		m_lscPackage.SetItemText(nRow, eHostType    , CUbcCode::GetInstance()->GetCodeName("HostType", info.nHostType        ));
		m_lscPackage.SetItemText(nRow, eVertical    , CUbcCode::GetInstance()->GetCodeName("Direction", info.nVertical        ));
		m_lscPackage.SetItemText(nRow, eResolution  , CUbcCode::GetInstance()->GetCodeName("Resolution"  , info.nResolution      ));
		m_lscPackage.SetItemText(nRow, ePublic      , (info.bIsPublic?LoadStringById(IDS_PACKAGEVIEW_STR005):LoadStringById(IDS_PACKAGEVIEW_STR006)));
		m_lscPackage.SetItemText(nRow, eVerify      , (info.bIsVerify?LoadStringById(IDS_PACKAGEVIEW_STR007):LoadStringById(IDS_PACKAGEVIEW_STR008)));

		if(info.tmValidationDate > 0)
		{
			CTime tmValidationDate(info.tmValidationDate);
			m_lscPackage.SetItemText(nRow, eValidateDate, tmValidationDate.Format(STR_DEF_TIME));
		}

		CTime tm(info.tmUpdateTime);
		m_lscPackage.SetItemText(nRow, eTime, tm.Format(STR_DEF_TIME));

		m_lscPackage.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
}

void CHddThresholdEx::OnBnClickedOk()
{
	UpdateData(TRUE);

	CWaitMessageBox wait;

	m_aFailList.RemoveAll();

	CString strFilepathList;
	CString strPackageList;
	CStringArray aList;

	// 패키지 관련 삭제
	if(m_nSelect == 1 || m_nSelect == 2)
	{
		IsUsed();
		for(int i=0; i<m_lscPackage.GetItemCount() ;i++)
		{
			CString strPackage = m_lscPackage.GetItemText(i, ePackage);

			if(strPackage.IsEmpty()) continue;

			aList.Add(strPackage);
			strPackageList += strPackage;
			strPackageList += ",";
		}

		if(aList.GetCount() <= 0)
		{
			UbcMessageBox(IDS_HDDTHRESHOLD_MSG001);
			return;
		}
	}
	// 콘텐츠 파일/폴더 삭제
	else if(m_nSelect == 3)
	{
		for(int i=0; i<m_txtFilepathList.GetLineCount() ;i++)
		{
			TCHAR szPath[MAX_PATH] = {0};
			m_txtFilepathList.GetLine(i, szPath, MAX_PATH);

			CString strPath(szPath);
			strPath.Trim();

			if(!strPath.IsEmpty())
			{
				strFilepathList += strPath + "|";
				aList.Add(strPath);
			}
		}

		strFilepathList.Trim("|");

		if(aList.GetCount() <= 0)
		{
			UbcMessageBox(IDS_HDDTHRESHOLD_MSG002);
			return;
		}
	}
// 테스트 단말에 테스트 여부 체크 
	int targetHostCount = m_aHostList.GetCount();

	CString a_Customer = GetEnvPtr()->m_strCustomer;
	if(a_Customer == "KIA" || a_Customer == "HYUNDAI" || a_Customer == "HYUNDAIDP") 
	{
		time_t now = time(NULL);

		CString szPath;
		szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

		if(targetHostCount >  1) 
		{
			char lastSelect[2];				memset(lastSelect,0x00,2);
			char lastFilePathList[10240];	memset(lastFilePathList,0x00,10240);
			char lastPackageList[10240];	memset(lastPackageList,0x00,10240);
			char lastLoginId[256];			memset(lastLoginId,0x00,256);
			char lastTargetHostCount[11];	memset(lastTargetHostCount,0x00,11);
			char lastExecutionTime[11];		memset(lastExecutionTime,0x00,11);

			GetPrivateProfileString("HDDTHRESHOLD_DLG", "Select", "", lastSelect, 2, szPath);
			GetPrivateProfileString("HDDTHRESHOLD_DLG", "FilepathList", "", lastFilePathList, 10240, szPath);
			GetPrivateProfileString("HDDTHRESHOLD_DLG", "PackageList", "", lastPackageList, 10240, szPath);
			GetPrivateProfileString("HDDTHRESHOLD_DLG", "loginId", "", lastLoginId, 256, szPath);
			GetPrivateProfileString("HDDTHRESHOLD_DLG", "TargetHostCount", "", lastTargetHostCount, 11, szPath);
			GetPrivateProfileString("HDDTHRESHOLD_DLG", "ExecutionTime", "", lastExecutionTime, 11,szPath);
		
			if(	atoi(lastTargetHostCount) != 1		||
				ToString(m_nSelect) != lastSelect	||
				((!strFilepathList.IsEmpty()) && strFilepathList != lastFilePathList) ||
				((!strPackageList.IsEmpty()) && strPackageList != lastPackageList)	||
				GetEnvPtr()->m_szLoginID != lastLoginId				){
					

					TraceLog(("lastSelect=%s,%s", lastSelect, ToString(m_nSelect)));
					TraceLog(("lastTargetHostCount=%d,%d", atoi(lastTargetHostCount), targetHostCount));
					TraceLog(("lastFilepathListt=%s,%s", lastFilePathList, strFilepathList ));
					TraceLog(("lastPackageList=%s,%s", lastPackageList, strPackageList));
					TraceLog(("lastLoginId=%s,%s", lastLoginId, GetEnvPtr()->m_szLoginID));
					
					// test 부터 먼저 할것!!!	
					UbcMessageBox(IDS_HDDTHRESHOLD_MSG005);
					return;	
			
			}else{  // 다똑같이 일치하고, 1개에 테스트를 해봤다면, 그 때가 언제인지 본다.
				time_t  lastNow = (time_t)strtoul(lastExecutionTime,0,10);
				TraceLog(("lastExecutionTime=%I64d,%I64d, %d", lastNow, now, now-lastNow));
				if(now - lastNow > 60*60*12) {
					// test 부터 먼저 할것!!!	
					UbcMessageBox(IDS_HDDTHRESHOLD_MSG005);
					return;	
				}
			}
		}

		WritePrivateProfileString("HDDTHRESHOLD_DLG", "Select", ToString(m_nSelect), szPath);
		WritePrivateProfileString("HDDTHRESHOLD_DLG", "FilepathList", strFilepathList, szPath);
		WritePrivateProfileString("HDDTHRESHOLD_DLG", "PackageList", strPackageList, szPath);
		WritePrivateProfileString("HDDTHRESHOLD_DLG", "loginId", GetEnvPtr()->m_szLoginID, szPath);
		WritePrivateProfileString("HDDTHRESHOLD_DLG", "TargetHostCount", ToString(targetHostCount), szPath);
		WritePrivateProfileString("HDDTHRESHOLD_DLG", "ExecutionTime", ToString((ULONG)now), szPath);
	}

	CString strGUID = CUbcGUID::GetInstance()->ToGUID(_T(""));
	for(int i=0; i<targetHostCount ;i++)
	{
		BOOL bResult = TRUE;
		switch(m_nSelect)
		{
		case 0: bResult = CCopModule::GetObject()->CleanContents1("*", m_aHostList[i], strGUID, GetEnvPtr()->m_szLoginID);
				break;
		case 1: bResult = CCopModule::GetObject()->CleanContents2("*", m_aHostList[i], strGUID, GetEnvPtr()->m_szLoginID, aList);
				break;
		case 2: bResult = CCopModule::GetObject()->CleanContents3("*", m_aHostList[i], strGUID, GetEnvPtr()->m_szLoginID, aList);
				break;
		case 3: bResult = CCopModule::GetObject()->CleanContents4("*", m_aHostList[i], strGUID, GetEnvPtr()->m_szLoginID, aList);
				break;
		case 4: bResult = CCopModule::GetObject()->CleanAllContents("*", m_aHostList[i], strGUID, GetEnvPtr()->m_szLoginID);
				break;
		}

		if(!bResult)
		{
			m_aFailList.Add(m_aHostList[i]);
		}
	}

	if(m_aFailList.GetCount() > 0)
	{
		CString strHostList;

		for(int i=0; i<m_aFailList.GetCount() ;i++)
		{
			if(i > 100) break;

			if( (i % 10) == 0 ) strHostList += "\n";
			else strHostList += ", ";

			strHostList += m_aFailList[i];
		}

		if(m_aFailList.GetCount() > 100)
		{
			strHostList += "\n......................\n";
		}

		CString strMsg;
		strMsg.Format(LoadStringById(IDS_HDDTHRESHOLD_MSG003), m_aFailList.GetCount(), strHostList);

		if(UbcMessageBox(strMsg, MB_YESNO) == IDYES)
		{
			m_aHostList.RemoveAll();
			m_aFailList.Copy(m_aFailList);
			OnBnClickedOk();
			return;
		}
	}
	else
	{
		UbcMessageBox(IDS_HDDTHRESHOLD_MSG004);
	}

	OnOK();
}

void CHddThresholdEx::OnBnClickedCancel()
{
	OnCancel();
}

int CHddThresholdEx::IsUsed()
{
	int retval= 0;
	for(int nRow = m_lscPackage.GetItemCount()-1; nRow >= 0; nRow--)
	{
		//if(!m_lscPackage.GetCheck(nRow)) continue;

		CString strMsg;

		// 0000560: 매니저에서 콘텐츠 패키지를 삭제시, 해당 콘텐츠 패키지가 사용중이라면, 사용중이라는 경고문을 넣는다. 
		CString szPackage = m_lscPackage.GetItemText(nRow, ePackage);
		ciStringSet hostSet;
		int nCnt = ubcUtil::getInstance()->getHostListUsingThisProgram(szPackage, hostSet);
		if(nCnt > 0)
		{
			strMsg.Format(LoadStringById(IDS_PACKAGEVIEW_MSG007), szPackage);

			ciStringSet::iterator itr;
			for(itr=hostSet.begin();itr!=hostSet.end();itr++)
			{
				ciString hostId = (*itr);
				strMsg += _T("\n");
				strMsg += hostId.c_str();
			}

			if(UbcMessageBox(strMsg))
			{
				retval++;
				m_lscPackage.DeleteItem(nRow);
				continue;
			}
		}

		// 방송계획으로 사용중인지 체크하여 지울지 판단한다.
		CString strBpList;
		if(!GetBroadcastPlan(szPackage, strBpList)) continue;
		if(!strBpList.IsEmpty())
		{
			strMsg.Format(LoadStringById(IDS_PACKAGEVIEW_MSG008), szPackage);

			if(UbcMessageBox(strMsg + strBpList))
			{
				retval++;
				m_lscPackage.DeleteItem(nRow);
				continue;
			}
		}
	}
	m_lscPackage.UpdateData();
	return retval;
}

BOOL CHddThresholdEx::GetBroadcastPlan(CString strPackage, CString& strBpList)
{
	CString strWhere;
	CString strTmp;

	CTime tmCur = CTime::GetCurrentTime();
	CString strBaseDate = tmCur.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	//strTmp.Format(_T("startDate >= '%s' and endDate > '%s'"), strStartDate, strStartDate);
	strTmp.Format(_T("endDate > CAST('%s' as datetime)"), strBaseDate);
	strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;

	strTmp.Format(_T("bpId in (select bpId from UBC_TP where lower(programId) like lower('%%%s%%'))"), strPackage);
	strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;

	BroadcastingPlanInfoList lsBpInfoList;
	if(!CCopModule::GetObject()->GetBroadcastingPlanList( (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
														, lsBpInfoList
														, strWhere ))
	{
		return FALSE;
	}

	int nRow = 0;
	POSITION pos = lsBpInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SBroadcastingPlanInfo& info = lsBpInfoList.GetNext(pos);

		strBpList += _T("\n") + info.strBpName;
	}

	return TRUE;
}
