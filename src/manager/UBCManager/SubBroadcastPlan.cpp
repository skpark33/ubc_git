#include "StdAfx.h"
#include "UBCManager.h"
#include "SubBroadcastPlan.h"

// CSubBroadcastPlan 대화 상자입니다.
IMPLEMENT_DYNAMIC(CSubBroadcastPlan, CDialog)

CSubBroadcastPlan::CSubBroadcastPlan(UINT nIDTemplate, CWnd* pParent /*=NULL*/)
	: CDialog(nIDTemplate, pParent)
	, m_pBroadcastingPlanInfo(NULL)
{
}

CSubBroadcastPlan::~CSubBroadcastPlan()
{
}

void CSubBroadcastPlan::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSubBroadcastPlan, CDialog)
END_MESSAGE_MAP()


SBroadcastingPlanInfo* CSubBroadcastPlan::GetBroadcastPlanInfo()
{
	return m_pBroadcastingPlanInfo;
}

void CSubBroadcastPlan::SetBroadcastPlanInfo(SBroadcastingPlanInfo* pBroadcastingPlanInfo)
{
	m_pBroadcastingPlanInfo = pBroadcastingPlanInfo;
}

TimePlanInfoList* CSubBroadcastPlan::GetTimePlanInfoList()
{
	return m_pTimePlanInfoList;
}

void CSubBroadcastPlan::SetTimePlanInfoList(TimePlanInfoList* pTimePlanInfoList)
{
	m_pTimePlanInfoList = pTimePlanInfoList;
}

TargetHostInfoList* CSubBroadcastPlan::GetTargetHostInfoList()
{
	return m_pTargetHostInfoList;
}

void CSubBroadcastPlan::SetTargetHostInfoList(TargetHostInfoList* pTargetHostInfoList)
{
	m_pTargetHostInfoList = pTargetHostInfoList;
}

