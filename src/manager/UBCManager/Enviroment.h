#pragma once

//#include <memory>
#include "common\TraceLog.h"
#include "UBCCopCommon\CopModule.h"

#define GetEnvPtr()				CEnviroment::GetObject()

#define STR_ENV_TIME			_T("%Y/%m/%d %H:%M:%S")
#define STR_ENV_EMPTY_TIME		_T("1970/01/01 09:00:00")

struct SLoginInfo{
	int  nPWSize;
	char szSite[MAX_PATH];
	char szID[MAX_PATH];
	char szPW[MAX_PATH];
};
/*
struct SFtpInfo{
	char szPmId[256];
	char szIP[16];
	char szID[16];
	char szPW[16];
	int  nPort;
};
*/

class CEnviroment
{
public:
	static CEnviroment* GetObject();
	static void Release();

	void SaveLoginInfo(bool bCheck);

	enum { eUBCType, eUSTBType };
	enum { eSQISOFT, eNARSHA, eLOTTE, eADASSET, eKIA, eHYUNDAI, eKMNL, eKMCL, eKPOST, eHYUNDAIQ }; // 2010.09.30 eADASSET=창일

	BOOL InitUser();
	BOOL RefreshUser(SUserInfo&);
	BOOL InitSite();
	BOOL RefreshSite(SSiteInfo&);
//	BOOL InitHost(HostInfoList&, LPCTSTR, LPCTSTR);
//	BOOL RefreshHost(HostInfoList&, LPCTSTR, LPCTSTR);
	BOOL RefreshPackageLog(LPCTSTR szWhere);
	BOOL RefreshPowerLog(LPCTSTR szWhere);
	BOOL RefreshConnectionLog(LPCTSTR szWhere);
	BOOL RefreshFault(LPCTSTR szWhere);
	BOOL RefreshFaultLog(LPCTSTR szWhere);

	bool GetFile(LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR);
	bool PutFile(LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR);
	bool RenameFile(LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR);
	bool DeleteFile(LPCTSTR, LPCTSTR, LPCTSTR);

	bool GetAutoUpdate();
	void SetAutoUpdate(bool bValue);
	bool GetAlphaUpdate();
	void SetAlphaUpdate(bool bValue);
	bool GetColumnNextSchedule();
	void SetColumnNextSchedule(bool bValue);
	CString GetSiteId() { return m_szSite; }

	bool GetServerOS();
	void SetServerOS(bool bValue);


	static bool				GetFileSize(CString, ULONGLONG&);
	bool					IsValidOpenFolder();
	CString					m_szPathOpenFile;
	void					SetPathOpenFile(LPCTSTR);

	CString					GetServerVersion();

protected:
	CEnviroment();
	~CEnviroment();

	void InitPath();
	void InitLoginInfo();
	char* Encrypt(char* buf, int nSize);

public:
	CString m_szSite;
	CString m_szLoginID;
	CString m_szPassword;

	int m_Authority;
	int m_StudioType;
	int m_Customer;
	CString m_strCustomer;

	CString m_szDrive;
	CString m_szPath;
	CString m_szFile;
	CString m_szName;
	CString m_szExt;
	CString m_szModule;

	CString	m_strHttpOnly;	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.

	UserInfoList m_lsUser;
	SiteInfoList m_lsSite;
//	HostInfoList m_lsHost;
//	HostInfoList m_lsHostProcess;
//	HostInfoList m_lsUpdateCenter;
	PackageInfoList m_lsPackage;
	PackageStateLogInfoList m_lsPackageStateLog;
	PowerStateLogInfoList m_lsPowerStateLog;
	ConnectionStateLogInfoList m_lsConnectionStateLog;
	SLogConfigInfo m_stLogConfiginfo;

	FaultInfoList m_lsFault;
	FaultLogInfoList m_lsFaultLog;

	CString m_strDocParam;
	CString m_strSpecialRole;
protected:
//	CList<SFtpInfo>		m_lsFtp;
//	std::auto_ptr<SInfo>;

	static CEnviroment* m_pEnvPtr;
};