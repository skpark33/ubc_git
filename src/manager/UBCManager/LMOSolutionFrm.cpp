// LMOSolutionFrm.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "LMOSolutionFrm.h"
#include "Enviroment.h"


// CLMOSolutionFrm

IMPLEMENT_DYNCREATE(CLMOSolutionFrm, CMDIChildWnd)

CLMOSolutionFrm::CLMOSolutionFrm()
{
}

CLMOSolutionFrm::~CLMOSolutionFrm()
{
}


BEGIN_MESSAGE_MAP(CLMOSolutionFrm, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CLMOSolutionFrm message handlers

int CLMOSolutionFrm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDI_ANNOUNCE), false);

	return 0;
}

void CLMOSolutionFrm::OnClose()
{
	TraceLog(("CLMOSolutionFrm::OnClose begin"));

	CMDIChildWnd::OnClose();

	TraceLog(("CLMOSolutionFrm::OnClose end"));
}

