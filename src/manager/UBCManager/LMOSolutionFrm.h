#pragma once


// CLMOSolutionFrm frame

class CLMOSolutionFrm : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CLMOSolutionFrm)
protected:
	CLMOSolutionFrm();           // protected constructor used by dynamic creation
	virtual ~CLMOSolutionFrm();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
