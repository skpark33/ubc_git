#pragma once

#include "Enviroment.h"
#include "common/HoverButton.h"
#include "common/utblistctrlex.h"
#include "common\reposcontrol.h"
#include "afxwin.h"


#define		WM_TRASHCAN_TAB		(WM_USER + 1026)


/////////////////////////////////
// CTrashCanView �� ���Դϴ�.

class CTrashCanView : public CFormView
{
	DECLARE_DYNCREATE(CTrashCanView)

protected:
	CTrashCanView();           // ���� ����⿡ ���Ǵ� protected �������Դϴ�.
	virtual ~CTrashCanView();

public:
	enum { IDD = IDD_TRASHCAN };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV �����Դϴ�.
	virtual void OnInitialUpdate();

	DECLARE_MESSAGE_MAP()

	CMapStringToPtr		m_mapCanBrowser;
	CReposControl		m_Reposition;
	CRect				m_rcClient;

	enum SUB_WIN   { eHostCan, eSiteCan, eBPCan, 
					ePackageCan, eUserCan, eMaxTab };

	void		InitTab();
	void		InitList();
	void		RefreshList();

public:
	void		SetCurrentTab(int nIdx);

	CHoverButton	m_btnRefresh;

	CStatic			m_labelFilterObjectName;
	CEdit			m_editObjectName;

	CDateTimeCtrl	m_dtcFilterStart;
	CDateTimeCtrl	m_dtcFilterEnd;
	CHoverButton	m_btnExcelSave;

	CTabCtrl		m_tcLogTab;
	CUTBListCtrlEx	m_lcLog[eMaxTab];

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedBtnRefresh();
	afx_msg void OnBnClickedBtnToExcel();
	afx_msg void OnTcnSelchangeTrashCanTab(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg LRESULT OnChangeTab(WPARAM wParam, LPARAM lParam);
	CHoverButton m_btRecovery;
	CHoverButton m_btEliminate;
	afx_msg void OnBnClickedBnToRecovery();
	afx_msg void OnBnClickedBnToEliminate();
};


/////////////////////////////////
class CCanBrowser
{
public:
	CCanBrowser(CUTBListCtrlEx* pLC) : m_pCanListCtrl(pLC) {};
	virtual ~CCanBrowser() {};
	
	enum { eCheck, eSite, eObjId, eName, eTrashTime, eTrashId, eEntity, eAttrList, eId, eMaxCol };
protected:
	CUTBListCtrlEx*	m_pCanListCtrl;

	CString		m_strObjectName;
	CString		m_strFilterName;
	CTime		m_tmStart;
	CTime		m_tmEnd;

	CString						m_CanColumn[eMaxCol];
	TrashCanInfoList			m_CanDataList;

public:
	void		SetFilter(LPCSTR lpszHostId, CTime& tmStart, CTime& tmEnd)
						{ m_strFilterName=lpszHostId; m_tmStart=tmStart; m_tmEnd=tmEnd; };

	virtual void InitList() ;
	virtual void RefreshList();
	virtual void VisibleFilter(CTrashCanView* parent) {
		parent->m_labelFilterObjectName.ShowWindow(true);
		parent->m_editObjectName.ShowWindow(true);
	}

	virtual int Eliminate();
	virtual int Recovery();
};

/////////////////////////////////
// �ܸ� ������
/////////////////////////////////
class CHostCanBrowser: public virtual CCanBrowser
{
public:
	CHostCanBrowser(CUTBListCtrlEx* pLC) : CCanBrowser(pLC) { m_strObjectName = _T("Host"); };
	virtual ~CHostCanBrowser() {};
};
/////////////////////////////////
// ���� ������
/////////////////////////////////
class CSiteCanBrowser: public virtual CCanBrowser
{
public:
	CSiteCanBrowser(CUTBListCtrlEx* pLC) : CCanBrowser(pLC) { m_strObjectName = _T("Site"); };
	virtual ~CSiteCanBrowser() {};
};
/////////////////////////////////
// ��۰�ȹ ������
/////////////////////////////////
class CBPCanBrowser: public virtual CCanBrowser
{
public:
	CBPCanBrowser(CUTBListCtrlEx* pLC) : CCanBrowser(pLC) { m_strObjectName = _T("BP"); };
	virtual ~CBPCanBrowser() {};
};

/////////////////////////////////
// ��۰�ȹ ������
/////////////////////////////////
class CPackageCanBrowser: public virtual CCanBrowser
{
public:
	CPackageCanBrowser(CUTBListCtrlEx* pLC) : CCanBrowser(pLC) { m_strObjectName = _T("Program"); };
	virtual ~CPackageCanBrowser() {};
};
/////////////////////////////////
// ����� ������
/////////////////////////////////
class CUserCanBrowser: public virtual CCanBrowser
{
public:
	CUserCanBrowser(CUTBListCtrlEx* pLC) : CCanBrowser(pLC) { m_strObjectName = _T("User"); };
	virtual ~CUserCanBrowser() {};
};

