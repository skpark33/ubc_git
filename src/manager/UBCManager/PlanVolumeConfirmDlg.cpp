// PlanVolumeConfirmDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "PlanVolumeConfirmDlg.h"
#include "HddThresholdEx.h"

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)
#define COLOR_DOWNLOAD		RGB(0,128,255)		// ::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_FAIL			RGB(176,0,0)

// CPlanVolumeConfirmDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPlanVolumeConfirmDlg, CDialog)

CPlanVolumeConfirmDlg::CPlanVolumeConfirmDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPlanVolumeConfirmDlg::IDD, pParent)
	, m_ulTotalPackageSize(0)
{

}

CPlanVolumeConfirmDlg::~CPlanVolumeConfirmDlg()
{
}

void CPlanVolumeConfirmDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PACKAGE, m_lscPackage);
	DDX_Control(pDX, IDC_LIST_HOST, m_lscHost);
}


BEGIN_MESSAGE_MAP(CPlanVolumeConfirmDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CPlanVolumeConfirmDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPlanVolumeConfirmDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_HOST_LIST_REFRESH, &CPlanVolumeConfirmDlg::OnBnClickedBnHostListRefresh)
	ON_BN_CLICKED(IDC_BN_HOST_CLEAN, &CPlanVolumeConfirmDlg::OnBnClickedBnHostClean)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_PACKAGE, &CPlanVolumeConfirmDlg::OnLvnItemchangedListPackage)
END_MESSAGE_MAP()


// CPlanVolumeConfirmDlg 메시지 처리기입니다.

void CPlanVolumeConfirmDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CPlanVolumeConfirmDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

BOOL CPlanVolumeConfirmDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CWaitMessageBox wait(LoadStringById(IDS_PLANVOLUME_MSG001));

	InitPackageList();
	RefreshPackageList();

	TotalPackageSize();

	InitHostList();
	UpdateHostList();
	RefreshHostList(m_ulTotalPackageSize);

	if(m_lscHost.GetItemCount() <= 0)
	{
		OnBnClickedOk();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPlanVolumeConfirmDlg::InitPackageList()
{
	m_lscPackage.SetExtendedStyle(m_lscPackage.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);

	m_lscPackage.InsertColumn(ePCheck , "", LVCFMT_RIGHT, 22);
	m_lscPackage.InsertColumn(eSite   , LoadStringById(IDS_PACKAGEVIEW_LST001), LVCFMT_LEFT, 70);
	m_lscPackage.InsertColumn(ePackage, LoadStringById(IDS_PACKAGEVIEW_LST002), LVCFMT_LEFT, 140);
	m_lscPackage.InsertColumn(eVolume , LoadStringById(IDS_PACKAGEVIEW_LST014), LVCFMT_RIGHT, 70);

	m_lscPackage.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CPlanVolumeConfirmDlg::RefreshPackageList()
{
	m_lsPackageList.RemoveAll();
	m_lscPackage.DeleteAllItems();

	CString strPackageList;

	for(int i=0; i<m_aPackageList.GetCount(); i++)
	{
		strPackageList += m_aPackageList[i] + ",";
	}

	strPackageList.Trim(",");

	if(strPackageList.IsEmpty()) return;

	strPackageList = "'" + strPackageList + "'";
	strPackageList.Replace(",", "','");

	CString strWhere;
	strWhere.Format(" programId in (%s) ", strPackageList);

	CCopModule::GetObject()->GetPackage("*", m_lsPackageList, strWhere);

	int nRow = 0;
	POSITION pos = m_lsPackageList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SPackageInfo info = m_lsPackageList.GetNext(pos);

		m_lscPackage.InsertItem(nRow, "");
		m_lscPackage.SetCheck(nRow);
		m_lscPackage.SetItemText(nRow, eSite   , info.szSiteID );
		m_lscPackage.SetItemText(nRow, ePackage, info.szPackage);
		m_lscPackage.SetItemText(nRow, eVolume , (info.volume < 0 ? "" : ToFileSize((ULONGLONG)info.volume, 'G')));

		m_lscPackage.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	m_lscPackage.SetCheckHdrState(m_lscPackage.GetItemCount() > 0);
}

void CPlanVolumeConfirmDlg::InitHostList()
{
	m_lscHost.SetExtendedStyle(m_lscHost.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);

	m_lscHost.InsertColumn(eHCheck    , "", LVCFMT_LEFT, 22);
	m_lscHost.InsertColumn(eSiteName  , LoadStringById(IDS_HOSTVIEW_LIST007), LVCFMT_LEFT, 65);
	m_lscHost.InsertColumn(eHostName  , LoadStringById(IDS_HOSTVIEW_LIST009), LVCFMT_LEFT, 70);
	m_lscHost.InsertColumn(eHostId    , LoadStringById(IDS_HOSTVIEW_LIST008), LVCFMT_LEFT, 60);
	m_lscHost.InsertColumn(eDisk1Avail, LoadStringById(IDS_HOSTVIEW_LIST049), LVCFMT_RIGHT, 65);
	m_lscHost.InsertColumn(eDisk2Avail, LoadStringById(IDS_HOSTVIEW_LIST050), LVCFMT_RIGHT, 65);

	m_lscHost.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CPlanVolumeConfirmDlg::UpdateHostList()
{
	m_lsHostList.RemoveAll();

	CString strHostList;

	for(int i=0; i<m_aHostList.GetCount(); i++)
	{
		strHostList += m_aHostList[i] + ",";
	}

	strHostList.Trim(",");

	if(strHostList.IsEmpty()) return;

	strHostList = "'" + strHostList + "'";
	strHostList.Replace(",", "','");

	CString strWhere;
	strWhere.Format(" hostId in (%s) ", strHostList);

	cciCall aCall;
	if(CCopModule::GetObject()->GetHostForSelect(&aCall, "*", "*", strWhere))
	{
		CCopModule::GetObject()->GetHostData(&aCall, m_lsHostList);
	}
}

void CPlanVolumeConfirmDlg::RefreshHostList(ULONGLONG ulSize)
{
	m_lscHost.DeleteAllItems();

	float fTotalSize = _tstof(ToFileSize(ulSize, 'G', FALSE));

	int nRow = 0;
	POSITION pos = m_lsHostList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsHostList.GetNext(pos);

		int diskAvail = info.disk2Avail;
		TraceLog(("info.cpuTemp", info.cpuTemp));
		if(info.cpuTemp == 1){
			diskAvail = info.disk1Avail;
			TraceLog(("C Drive founded", diskAvail));
		}

		//if(fTotalSize < (info.disk2Avail < 0 ? info.disk1Avail : info.disk2Avail)) continue;
		if(fTotalSize < diskAvail) continue;

		m_lscHost.InsertItem(nRow, "");
		m_lscHost.SetItemText(nRow, eSiteName, info.siteName);
		m_lscHost.SetItemText(nRow, eHostId, info.hostId);
		m_lscHost.SetItemText(nRow, eHostName, info.hostName);
		m_lscHost.SetItemText(nRow, eDisk1Avail, (info.disk1Avail < 0 ? "" : ToString((double)info.disk1Avail, 2) + _T(" G")));
		m_lscHost.SetItemText(nRow, eDisk2Avail, (info.disk2Avail < 0 ? "" : ToString((double)info.disk2Avail, 2) + _T(" G")));

		m_lscHost.SetRowColor(nRow, (info.operationalState ? COLOR_YELLOW : COLOR_GRAY), m_lscHost.GetTextColor());

		m_lscHost.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
}

void CPlanVolumeConfirmDlg::OnBnClickedBnHostClean()
{
	CHddThresholdEx dlg;

	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsHostList.GetAt(posHost);

		dlg.m_aHostList.Add(Info.hostId);
	}

	if(dlg.m_aHostList.GetCount() <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	dlg.DoModal();
}

void CPlanVolumeConfirmDlg::OnBnClickedBnHostListRefresh()
{
	UpdateHostList();
	RefreshHostList(m_ulTotalPackageSize);
}

void CPlanVolumeConfirmDlg::TotalPackageSize()
{
	m_ulTotalPackageSize = 0;

	for(int i=0; i<m_lscPackage.GetItemCount() ;i++)
	{
		if(!m_lscPackage.GetCheck(i)) continue;

		POSITION pos = (POSITION)m_lscPackage.GetItemData(i);
		if(!pos) continue;

		SPackageInfo Info = m_lsPackageList.GetAt(pos);

		if(Info.volume > 0)
		{
			m_ulTotalPackageSize += Info.volume;
		}
	}

	CString strText;
	strText.Format("%s : %s", LoadStringById(IDS_PLANVOLUME_STR001), ToFileSize(m_ulTotalPackageSize, 'G'));
	SetDlgItemText(IDC_TXT_TOTAL_PACKAGE_SIZE, strText);
}

void CPlanVolumeConfirmDlg::OnLvnItemchangedListPackage(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMListView = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	*pResult = 0;

	if (pNMListView->uOldState == 0 && pNMListView->uNewState == 0)
		return;  // No change

	BOOL bPrevState = (BOOL)(((pNMListView->uOldState & LVIS_STATEIMAGEMASK)>>12)-1);   // Old check box state
	if (bPrevState < 0)       // On startup there's no previous state 
		bPrevState = 0; // so assign as false (unchecked)

	// New check box state
	BOOL bChecked=(BOOL)(((pNMListView->uNewState & LVIS_STATEIMAGEMASK)>>12)-1);
	if (bChecked < 0) // On non-checkbox notifications assume false
		bChecked = 0;

	if (bPrevState == bChecked) // No change in check box
		return;

	TraceLog(("OnLvnItemchangedListPackage"));

	TotalPackageSize();
	RefreshHostList(m_ulTotalPackageSize);
}
