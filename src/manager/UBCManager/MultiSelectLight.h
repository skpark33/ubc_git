#pragma once

#ifndef IDD_MULTI_SELECT_LIGHT
#define IDD_MULTI_SELECT_LIGHT	469
#endif//IDD_MULTI_SELECT_LIGHT

#include "afxwin.h"
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\CheckComboBox.h"


// CMultiSelectLight 대화 상자입니다.

class CMultiSelectLight : public CDialog
{
	DECLARE_DYNAMIC(CMultiSelectLight)

public:
	CMultiSelectLight(LPCTSTR szCustomer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMultiSelectLight();

	void SetTitle(CString& p) { m_title = p; }

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MULTI_SELECT_LIGHT };
	enum { eCheck, eLightName, eLightID, eGroup, eMaxCol };

	// In/Out Parameter
	CStringArray		m_astrSelLightList;    //lightIdList
	CStringArray		m_astrSelLightNameList;  //lightNameList

	int					m_nAuthority;
	CString				m_strSiteId;
	CString				m_strIniPath;

protected:
	CString				m_title;
	CString				m_strCustomer;

	CString				m_szColum[eMaxCol];
	LightInfoList		m_lsInfoList;

	CEdit				m_editFilterLightName;
	CEdit				m_editFilterLight;
	//CComboBox			m_cbxLightType;
	CCheckComboBox		m_cbxLightType;
	CComboBox			m_cbxSite;
	CComboBox			m_cbOnOff;
	CComboBox			m_cbOperation;
//	CEdit				m_editFilterDescription;
	CHoverButton		m_bnRefresh;
	CUTBListCtrlEx		m_lcLightList;
	CUTBListCtrlEx		m_lcSelLightList;
	CHoverButton		m_bnAdd;
	CHoverButton		m_bnDel;
	CHoverButton		m_bnOK;
	CHoverButton		m_bnCancel;

	CString				m_strSite;

	void				InitLightList();
	void				RefreshLightList();

	void				InitSelLightList();
	void				RefreshSelLightList();

	void				AddSelLight(int nLightIndex);
	void				DelSelLight(int nSelLightIndex);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonLightRefresh();
	afx_msg void OnBnClickedBnAdd();
	afx_msg void OnBnClickedBnDel();
	afx_msg void OnNMDblclkListLight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListSelLight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeCbSite();
};
