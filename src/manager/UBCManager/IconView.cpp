// IconView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "IconView.h"
#include "IconDetailInfo.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common\libcommon\utvUtil.h"
#include "common\UbcCode.h"
#include "UBCCopCommon\UbcMenuAuth.h"

// CIconView 대화 상자입니다.

IMPLEMENT_DYNCREATE(CIconView, CFormView)

CIconView::CIconView()
	: CFormView(CIconView::IDD)
	,	m_Reposition(this)
{

}

CIconView::~CIconView()
{
}

void CIconView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_FILTER_OBJECTTYPE, m_cbHostType);
	DDX_Control(pDX, IDC_BUTTON_FILTER_OBJECTTYPE2, m_cbMonitorType);
	DDX_Control(pDX, IDC_BUTTON_FILTER_OBJECTTYPE3, m_cbLightType);
	DDX_Control(pDX, IDC_COMBO_FILTER_OPR, m_cbObjectState);
	DDX_Control(pDX, IDC_BUTTON_ICON_REF, m_btnRefIcon);
	DDX_Control(pDX, IDC_BUTTON_ICON_CREATE, m_btnCreateIcon);
	DDX_Control(pDX, IDC_BUTTON_ICON_DEL, m_btnDeleteIcon);
	DDX_Control(pDX, IDC_LIST_ICON, m_lscIcon);
	DDX_Control(pDX, IDC_COMBO_FILTER_OBJECTCLASS, m_cbObjectClass);
	DDX_Control(pDX, IDC_BUTTON_ICON_SYNC, m_btnSyncIcon);
}


BEGIN_MESSAGE_MAP(CIconView, CFormView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_ICON_REF, &CIconView::OnBnClickedButtonIconRef)
	ON_BN_CLICKED(IDC_BUTTON_ICON_CREATE, &CIconView::OnBnClickedButtonIconCreate)
	ON_BN_CLICKED(IDC_BUTTON_ICON_DEL, &CIconView::OnBnClickedButtonIconDel)
	ON_CBN_SELCHANGE(IDC_COMBO_FILTER_OBJECTCLASS, &CIconView::OnCbnSelchangeComboFilterObjectclass)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_ICON, &CIconView::OnNMDblclkListIcon)
	ON_BN_CLICKED(IDC_BUTTON_ICON_SYNC, &CIconView::OnBnClickedButtonIconSync)
END_MESSAGE_MAP()



#define STR_OPERA_ON			LoadStringById(IDS_HOSTVIEW_LIST003)
#define STR_OPERA_OFF			LoadStringById(IDS_HOSTVIEW_LIST004)

#define STR_HOST_CLASS				LoadStringById(IDS_ICONVIEW_STR001)
#define STR_LIGHT_CLASS				LoadStringById(IDS_ICONVIEW_STR002)
#define STR_MONITOR_CLASS			LoadStringById(IDS_ICONVIEW_STR003)

#define STR_ICON_ID				LoadStringById(IDS_ICONVIEW_STR005)
#define STR_ICON_EXT			LoadStringById(IDS_ICONVIEW_STR006)
#define STR_ICON_CLASS			LoadStringById(IDS_ICONVIEW_STR007)
#define STR_ICON_TYPE			LoadStringById(IDS_ICONVIEW_STR008)
#define STR_ICON_STATE			LoadStringById(IDS_ICONVIEW_STR009)

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)
#define COLOR_DOWNLOAD		RGB(0,128,255)		// ::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_FAIL			RGB(176,0,0)

// CIconView 메시지 처리기입니다.

int CIconView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}
void CIconView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_HOSTVIEW_STR001));

	m_cbObjectClass.AddString("All");
	m_cbObjectClass.AddString(STR_HOST_CLASS);
	m_cbObjectClass.AddString(STR_LIGHT_CLASS);
	m_cbObjectClass.AddString(STR_MONITOR_CLASS);
	m_cbObjectClass.SetCurSel(0);

	
	m_cbObjectState.AddString("All");
	m_cbObjectState.AddString(STR_OPERA_ON);
	m_cbObjectState.AddString(STR_OPERA_OFF);
	m_cbObjectState.SetCurSel(0);

	CUbcCode::GetInstance()->GetCategoryInfo(_T("HostType"), m_listHostType);
	CUbcCode::GetInstance()->GetCategoryInfo(_T("LightType"), m_listLightType);
	CUbcCode::GetInstance()->GetCategoryInfo(_T("SubMonitorType"), m_listMonitorType);

	m_cbHostType.AddString("All", -1);
	m_cbHostType.EnableWindow(false);
	m_cbMonitorType.AddString("All", -1);
	m_cbMonitorType.EnableWindow(false);
	m_cbLightType.AddString("All", -1);
	m_cbLightType.EnableWindow(false);

	m_cbHostType.ShowWindow(SW_SHOW);
	m_cbMonitorType.ShowWindow(SW_HIDE);
	m_cbLightType.ShowWindow(SW_HIDE);

	m_btnRefIcon.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnCreateIcon.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btnDeleteIcon.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btnSyncIcon.LoadBitmap(IDB_BUTTON_ICON_SYNC, RGB(255,255,255));

	InitPosition(m_rcClient);

	UpdateData(FALSE);
	InitAuthCtrl();
	InitIconList();

	SetScrollSizes(MM_TEXT, CSize(1,1));

}

void CIconView::OnCbnSelchangeComboFilterObjectclass()
{
	int idx = m_cbObjectClass.GetCurSel();
	CodeItemList* p_List = 0;
	CCheckComboBox* p_cb = 0;
	switch(idx) {
		case 1 :  p_List = &m_listHostType;  
				  p_cb = &m_cbHostType;
				  m_cbLightType.ShowWindow(SW_HIDE);
				  m_cbMonitorType.ShowWindow(SW_HIDE);
				  break;
		case 2 :  p_List = &m_listLightType; 
				  p_cb = &m_cbLightType;
				  m_cbHostType.ShowWindow(SW_HIDE);
				  m_cbMonitorType.ShowWindow(SW_HIDE);
				  break;
		case 3 :  p_List = &m_listMonitorType; 
			      p_cb = &m_cbMonitorType;
				  m_cbLightType.ShowWindow(SW_HIDE);
				  m_cbHostType.ShowWindow(SW_HIDE);
				  break;
		default : 	return;
	}



	if(p_List){
		POSITION pos = p_List->GetHeadPosition();
		while(pos)
		{
			SCodeItem info = p_List->GetNext(pos);
			p_cb->AddString(info.strEnumString, info.nEnumNumber);
		}
		CRect rc;
		p_cb->GetWindowRect(&rc);
		p_cb->SetDroppedWidth(rc.Width() * 1.5);
		p_cb->EnableWindow(true);
		p_cb->ShowWindow(SW_SHOW);
	}
}
void CIconView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscIcon, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}


void CIconView::InitAuthCtrl()
{
	m_btnDeleteIcon.EnableWindow(IsAuth(_T("HDEL")));
	m_btnRefIcon.EnableWindow(IsAuth(_T("HQRY")));
	m_btnCreateIcon.EnableWindow(IsAuth(_T("HREG")));
}

void CIconView::InitIconList()
{
	m_szIconColum[eCheck] = _T("");	
	m_szIconColum[eIconTypeID] =	STR_ICON_ID;
	m_szIconColum[eExt] =			STR_ICON_EXT;
	m_szIconColum[eObjectClass] =	STR_ICON_CLASS;
	m_szIconColum[eObjectType] =	STR_ICON_TYPE;
	m_szIconColum[eObjectState] =	STR_ICON_STATE;

	m_lscIcon.SetExtendedStyle(m_lscIcon.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_INFOTIP |m_lscIcon.GetExtendedStyle());

	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilIconList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilIconList.Add(&bmCheck, RGB(255, 255, 255));
	m_lscIcon.SetImageList(&m_ilIconList, LVSIL_SMALL);

	m_lscIcon.InsertColumn(eCheck,			m_szIconColum[eCheck], LVCFMT_LEFT, 22);
	m_lscIcon.InsertColumn(eIconTypeID,		m_szIconColum[eIconTypeID], LVCFMT_LEFT, 240);
	m_lscIcon.InsertColumn(eExt,			m_szIconColum[eExt], LVCFMT_LEFT, 60);
	m_lscIcon.InsertColumn(eObjectClass,	m_szIconColum[eObjectClass], LVCFMT_LEFT, 120);
	m_lscIcon.InsertColumn(eObjectType,		m_szIconColum[eObjectType], LVCFMT_LEFT, 120);
	m_lscIcon.InsertColumn(eObjectState,	m_szIconColum[eObjectState], LVCFMT_LEFT, 60);

	m_lscIcon.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscIcon.LoadColumnState("ICON-LIST", szPath);

}

BOOL CIconView::RefreshIcon(bool bCompMsg)
{
	TraceLog(("RefreshIcon begin"));
	DWORD dwStartTime =  ::GetTickCount() ;

	CWaitMessageBox wait;
	
	UpdateData();

	CString strIconType     ;
	CString strIconClass     ;
	CString strOperaStat    ;

	CCheckComboBox* p_cb = 0;
	strIconClass = "";
	switch(m_cbObjectClass.GetCurSel()){
		case 1:		strIconClass = "Host"; p_cb = &m_cbHostType; break;
		case 2:		strIconClass = "Light"; p_cb = &m_cbLightType; break;
		case 3:		strIconClass = "Monitor"; p_cb = &m_cbMonitorType; break;
	}

	if(p_cb) strIconType = p_cb->GetCheckedIDs();
	if(strIconType == _T("All")) strIconType = _T("");
	else if(strIconType.GetLength() >= 2) strIconType = strIconType.Mid(1, strIconType.GetLength()-2);
	
	strOperaStat = ToString(m_cbObjectState.GetCurSel());

	CString strWhere;

	// objectClass
	if(!strIconClass.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s objectClass = '%s'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strIconClass
							);
	}

	// objectType
	if(!strIconType.IsEmpty())
	{
		strIconType.Replace(",", "','");
		strWhere.AppendFormat(_T(" %s objectType in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strIconType
							);
	}

	// 연결상태
	if(_ttoi(strOperaStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s objectState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strOperaStat)==1 ? 1:0)
							);
	}


	TraceLog(("m_lsIcon.RemoveAll() begin"));
	m_lsIcon.RemoveAll();
	TraceLog(("m_lsIcon.RemoveAll() end"));

	BOOL bRet = CCopModule::GetObject()->GetIconList("*", (strWhere.IsEmpty() ? NULL : strWhere),m_lsIcon);

	RefreshIconList();

	TraceLog(("RefreshIcon end"));

	if(!bRet)
	{
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR010), MB_ICONINFORMATION);
		return FALSE;
	}

	if(bCompMsg)
	{
		DWORD dwElapse = ::GetTickCount() - dwStartTime;
		DWORD dwElapseSec = dwElapse / 1000;
		CString strElapse;
		strElapse.Format("\nElapse time : %ld sec", dwElapseSec);

		CString strMsg;
		strMsg.Format(LoadStringById(IDS_ICONVIEW_STR011), m_lscIcon.GetItemCount());
		strMsg.Append(strElapse);

		UbcMessageBox(strMsg, MB_ICONINFORMATION);
	}

	return bRet;
}

void CIconView::RefreshIconList(POSITION posStart)
{
	TraceLog(("RefreshIconList begin"));

	m_lscIcon.SetRedraw(FALSE);

	int nRow = 0;
	CString szBuf;
	POSITION pos = m_lsIcon.GetHeadPosition();
	POSITION posTail = m_lsIcon.GetTailPosition();

	if(posStart)
	{
		pos = posStart;
		m_lsIcon.GetNext(pos);
	}
	else
	{
		m_lscIcon.DeleteAllItems();
	}

	while(pos)
	{
		POSITION posOld = pos;
		SNodeTypeInfo info = m_lsIcon.GetNext(pos);

		m_lscIcon.InsertItem(nRow, _T(""), -1);
	
		UpdateListRow(nRow, &info);  //OK

		m_lscIcon.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lscIcon.GetCurSortCol();
	if(-1 != nCol)
		m_lscIcon.SortList(nCol, m_lscIcon.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscIcon);
	else
		m_lscIcon.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscIcon);

	TraceLog(("RefreshIconList end"));

	m_lscIcon.SetRedraw(TRUE);

	//RefreshPaneText();
}


void CIconView::UpdateListRow(int nRow, SNodeTypeInfo* pInfo)
{
	if(!pInfo || m_lscIcon.GetItemCount() < nRow)
		return;

	CString strState;
	COLORREF crTextBk = COLOR_WHITE;
	if(pInfo->objectState)
	{
		crTextBk = COLOR_YELLOW;
		strState = LoadStringById(IDS_HOSTVIEW_LIST003);
	}
	else
	{
		crTextBk = COLOR_GRAY;
		strState = LoadStringById(IDS_HOSTVIEW_LIST004);
	}
	m_lscIcon.SetRowColor(nRow, crTextBk, m_lscIcon.GetTextColor());

	m_lscIcon.SetItemText(nRow, eIconTypeID, pInfo->nodeTypeId);
	m_lscIcon.SetItemText(nRow, eExt, pInfo->ext);
	if(pInfo->objectClass == "Host") {
		m_lscIcon.SetItemText(nRow, eObjectClass, LoadStringById(IDS_ICONVIEW_STR001));
		m_lscIcon.SetItemText(nRow, eObjectType, CUbcCode::GetInstance()->GetCodeName(_T("HostType"), pInfo->objectType));
	}else if(pInfo->objectClass == "Light") {
		m_lscIcon.SetItemText(nRow, eObjectClass, LoadStringById(IDS_ICONVIEW_STR002));
		m_lscIcon.SetItemText(nRow, eObjectType, CUbcCode::GetInstance()->GetCodeName(_T("LightType"), pInfo->objectType));
	}else if(pInfo->objectClass == "Monitor") {
		m_lscIcon.SetItemText(nRow, eObjectClass, LoadStringById(IDS_ICONVIEW_STR003));
		m_lscIcon.SetItemText(nRow, eObjectType, CUbcCode::GetInstance()->GetCodeName(_T("SubMonitorType"), pInfo->objectType));
	}
	m_lscIcon.SetItemText(nRow, eObjectState, strState);

}


void CIconView::ModifyIcon(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscIcon.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscIcon.GetItemData(nRow);
		if(!pos) continue;

		SNodeTypeInfo Info = m_lsIcon.GetAt(pos);
		CIconDetailInfo dlg;

		dlg.SetInfo(Info);
		if(dlg.DoModal() != IDOK)
		{
			dlg.GetInfo(Info);
			m_lsIcon.GetAt(pos) = Info;
			UpdateListRow(nRow, &Info); //OK
			continue;
		}
		dlg.GetInfo(Info);

		if(!dlg.Upload()){
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_ICONVIEW_STR018), Info.nodeTypeId);
			UbcMessageBox(szMsg, MB_ICONERROR);
			continue;
		}

		//if(!CCopModule::GetObject()->CreateIcon(Info)){
			if(!CCopModule::GetObject()->SetIcon(Info))
			{
				CString szMsg;
				szMsg.Format(LoadStringById(IDS_ICONVIEW_STR015), Info.nodeTypeId);
				UbcMessageBox(szMsg, MB_ICONERROR);
				continue;
			}
		//}
		bModified = true;
		m_lsIcon.GetAt(pos) = Info;

	}

	if(bModified)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR016), MB_ICONINFORMATION);
		RefreshIcon(false);
	}
}


void CIconView::OnDestroy()
{
	CFormView::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CIconView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CIconView::OnBnClickedButtonIconRef()
{
	RefreshIcon();
}

void CIconView::OnBnClickedButtonIconCreate()
{
	CIconDetailInfo dlg;

	dlg.SetCreateMode();
	if(dlg.DoModal() != IDOK)
	{
		return;
	}
	SNodeTypeInfo Info;
	dlg.GetInfo(Info);

	NodeTypeInfoList dummyList;
	CCopModule::GetObject()->GetIconList(Info.nodeTypeId,NULL,dummyList); 
	if(dummyList.GetSize() > 0){
		CString msg;
		msg.Format(LoadStringById(IDS_ICONVIEW_STR024), Info.nodeTypeId);
		UbcMessageBox(msg);
		return ;
	}

	if(!dlg.Upload()){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_ICONVIEW_STR018), Info.nodeTypeId);
		UbcMessageBox(szMsg, MB_ICONERROR);
		return;
	}

	if(!CCopModule::GetObject()->CreateIcon(Info)){
		//if(!CCopModule::GetObject()->SetIcon(Info))
		//{
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_ICONVIEW_STR015), Info.nodeTypeId);
			UbcMessageBox(szMsg, MB_ICONERROR);
			return;
		//}
	}
	UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR016), MB_ICONINFORMATION);
	RefreshIcon(false);
}

void CIconView::OnBnClickedButtonIconDel()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscIcon.GetItemCount(); nRow++)
	{
		if(!m_lscIcon.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	RemoveIcon(arRow, true);
}

void CIconView::RemoveIcon(CArray<int>& arRow, bool bMsg)
{
	if(UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR012), MB_YESNO|MB_ICONINFORMATION) != IDYES)
	{
		return;
	}

	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscIcon.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscIcon.GetItemData(nRow);
		if(!pos) continue;

		SNodeTypeInfo Info = m_lsIcon.GetAt(pos);

		TraceLog(("%s will be deleted", Info.nodeTypeId));

		if(!CCopModule::GetObject()->DelIcon(Info.nodeTypeId))
		{
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_ICONVIEW_STR020), Info.nodeTypeId);
			UbcMessageBox(szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;

		//m_lsIcon.RemoveAt(pos);
		//m_lscIcon.DeleteItem(nRow);
	}

	if(bModified)
	{
		if(bMsg) 	UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR021), MB_ICONINFORMATION);
		RefreshIcon(false);
	}

}



void CIconView::OnNMDblclkListIcon(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!IsAuth(_T("HDTL")) && !IsAuth(_T("HMOD"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	CArray<int> arRow;
	arRow.Add(nRow);
	ModifyIcon(arRow, true);
}

void CIconView::OnBnClickedButtonIconSync()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscIcon.GetItemCount(); nRow++)
	{
		if(!m_lscIcon.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	CString szServerLocation = "/Config/Plano/" ;
	CString szLocalLocation;
	szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

	int counter=0;

	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscIcon.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscIcon.GetItemData(nRow);
		if(!pos) continue;

		SNodeTypeInfo Info = m_lsIcon.GetAt(pos);

		CString iconFile = szLocalLocation + Info.nodeTypeId;

		if(::DeleteFile(iconFile) == 0){
			TraceLog(("ERROR : %s file delete failed", iconFile));
		}else{
			TraceLog(("%s file deleted", iconFile));
		}
		if(GetEnvPtr()->GetFile(Info.nodeTypeId, szServerLocation,szLocalLocation,"")){
			TraceLog(("Download succeed(%s,%s,%s)", Info.nodeTypeId, szLocalLocation,szServerLocation));
			counter++;
		}else{
			TraceLog(("ERROR : Download(%s,%s,%s) failed", Info.nodeTypeId, szLocalLocation,szServerLocation));
		}

	}

	CString msg;
	msg.Format(LoadStringById(IDS_ICONVIEW_STR025),counter);
	UbcMessageBox(msg);
	return;
}
