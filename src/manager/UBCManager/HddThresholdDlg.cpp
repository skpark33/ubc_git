// HddThresholdDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HddThresholdDlg.h"

// CHddThresholdDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHddThresholdDlg, CDialog)

CHddThresholdDlg::CHddThresholdDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHddThresholdDlg::IDD, pParent)
	, m_nSelect(0)
	, m_nHddThreshold(100)
{
}

CHddThresholdDlg::~CHddThresholdDlg()
{
}

void CHddThresholdDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_THRESHOLD_RB_SELECT1, m_nSelect);
	DDX_Text(pDX, IDC_THRESHOLD_EB_VALUE, m_nHddThreshold);
	DDV_MinMaxInt(pDX, m_nHddThreshold, 50, 100);
	DDX_Control(pDX, IDC_THRESHOLD_SPIN_VALUE, m_spHddThreshold);
}

BEGIN_MESSAGE_MAP(CHddThresholdDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CHddThresholdDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CHddThresholdDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_THRESHOLD_RB_SELECT1, &CHddThresholdDlg::OnBnClickedThresholdRbSelect)
	ON_BN_CLICKED(IDC_THRESHOLD_RB_SELECT2, &CHddThresholdDlg::OnBnClickedThresholdRbSelect)
END_MESSAGE_MAP()


// CHddThresholdDlg 메시지 처리기입니다.

BOOL CHddThresholdDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_THRESHOLD_EB_VALUE)->EnableWindow(m_nSelect == 0);
	GetDlgItem(IDC_THRESHOLD_SPIN_VALUE)->EnableWindow(m_nSelect == 0);

	m_spHddThreshold.SetRange32(50, 100);

	UpdateData(FALSE);

	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_THRESHOLD_EB_VALUE);
	if(m_nHddThreshold < 0)
	{
		pEdit->SetWindowText(_T(""));
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CHddThresholdDlg::OnBnClickedOk()
{
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_THRESHOLD_EB_VALUE);
	if(m_nSelect != 0)
	{
		// 오류가 나지 않도록 디폴트값 설정
		pEdit->SetWindowText("100");
		m_nHddThreshold = 100;
	}

	OnOK();
}

void CHddThresholdDlg::OnBnClickedCancel()
{
	OnCancel();
}

void CHddThresholdDlg::OnBnClickedThresholdRbSelect()
{
	CButton* pSelect = (CButton*)GetDlgItem(IDC_THRESHOLD_RB_SELECT1);
	m_nSelect = (pSelect->GetCheck() ? 0 : 1);

	GetDlgItem(IDC_THRESHOLD_EB_VALUE)->EnableWindow(m_nSelect == 0);
	GetDlgItem(IDC_THRESHOLD_SPIN_VALUE)->EnableWindow(m_nSelect == 0);
}
