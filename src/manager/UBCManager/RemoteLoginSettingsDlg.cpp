// RemoteLoginSettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "RemoteLoginSettingsDlg.h"


// CRemoteLoginSettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CRemoteLoginSettingsDlg, CDialog)

CRemoteLoginSettingsDlg::CRemoteLoginSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRemoteLoginSettingsDlg::IDD, pParent)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, NULL, NULL);

	m_strINIPath.Format(_T("%s%sdata\\%s"), szDrive, szPath, UBCVARS_INI);
}

CRemoteLoginSettingsDlg::~CRemoteLoginSettingsDlg()
{
}

void CRemoteLoginSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_AUTO_SCALING, m_chkAutoScaling);
	DDX_Control(pDX, IDC_CHECK_VIEW_ONLY, m_chkViewOnly);
	DDX_Control(pDX, IDC_COMBO_COLOR_DEPTH, m_cbxColorDepth);
}


BEGIN_MESSAGE_MAP(CRemoteLoginSettingsDlg, CDialog)
END_MESSAGE_MAP()


// CRemoteLoginSettingsDlg 메시지 처리기입니다.

BOOL CRemoteLoginSettingsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	InitControls();

	// auto scaling
	char buf[1024];
	::GetPrivateProfileString(_T("REMOTE_LOGIN"), _T("AutoScaling"), _T(""), buf, 1024, m_strINIPath);
	if( atoi(buf) != 0 )
	{
		m_chkAutoScaling.SetCheck(BST_CHECKED);
	}

	// view only
	::GetPrivateProfileString(_T("REMOTE_LOGIN"), _T("ViewOnly"), _T(""), buf, 1024, m_strINIPath);
	if( atoi(buf) != 0 )
	{
		m_chkViewOnly.SetCheck(BST_CHECKED);
	}

	// color depth
	::GetPrivateProfileString(_T("REMOTE_LOGIN"), _T("ColorDepth"), _T(""), buf, 1024, m_strINIPath);
	SetColorDepth(buf);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CRemoteLoginSettingsDlg::OnOK()
{
	// auto scaling
	CString str_auto_scaling = "0";
	if( m_chkAutoScaling.GetCheck() == BST_CHECKED )
		str_auto_scaling = "1";
	::WritePrivateProfileString(_T("REMOTE_LOGIN"), _T("AutoScaling"), str_auto_scaling, m_strINIPath);

	// view only
	CString str_view_only = "0";
	if( m_chkViewOnly.GetCheck() == BST_CHECKED )
		str_view_only = "1";
	::WritePrivateProfileString(_T("REMOTE_LOGIN"), _T("ViewOnly"), str_view_only, m_strINIPath);

	// color depth
	CString str_color_depth = "";
	int sel = m_cbxColorDepth.GetCurSel();
	if( sel > 0 )
	{
		m_cbxColorDepth.GetLBText(sel, str_color_depth);
		str_color_depth.Replace(" ", "");
	}
	::WritePrivateProfileString(_T("REMOTE_LOGIN"), _T("ColorDepth"), str_color_depth, m_strINIPath);

	CDialog::OnOK();
}

void CRemoteLoginSettingsDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnCancel();
}

void CRemoteLoginSettingsDlg::InitControls()
{
/*
Full colors
256colors
64colors
8colors
8greycolors
4greycolors
2greycolors
*/
	//
	m_cbxColorDepth.ResetContent();
	m_cbxColorDepth.AddString("Full colors");
	m_cbxColorDepth.AddString("256 colors");
	m_cbxColorDepth.AddString("64 colors");
	m_cbxColorDepth.AddString("8 colors");
	m_cbxColorDepth.AddString("8 grey colors");
	m_cbxColorDepth.AddString("4 grey colors");
	m_cbxColorDepth.AddString("2 grey colors");

	m_cbxColorDepth.SetCurSel(0);
}

void CRemoteLoginSettingsDlg::SetColorDepth(LPCSTR lpszColorDepth)
{
	int cnt = m_cbxColorDepth.GetCount();
	for(int i=0; i<cnt; i++)
	{
		CString str;
		m_cbxColorDepth.GetLBText(i, str);
		str.Replace(" ", "");

		if( stricmp(str, lpszColorDepth) == 0 )
		{
			m_cbxColorDepth.SetCurSel(i);
			break;
		}
	}
}
