#include "stdafx.h"
#include "resource.h"
#include "common\TraceLog.h"
#include "NodeControl.h"
#include "PlanoControl.h"
#include "Enviroment.h"
#include "ubccopcommon\CopModule.h"
#include "HostDetailDlg.h"
#include "LightDetailDlg.h"
#include "MonitorDetailDlg.h"

#define FONT_HEIGHT 14
#define FONT_WIDTH  10
#define INFO_OFFSET 2

CNodeControl::CNodeControl(CPlanoControl* parent)
: CDownloadPictureEx()
{
	m_planoCtrl = parent;
	m_NodeInfo = 0;
	m_action = ACTION_SELECT;
	m_moving = false;
	m_start_x = -1;
	m_start_y = -1;
	m_selected = false;
	m_DrawMode = false;
	m_isShowName = false;
}

CNodeControl::~CNodeControl()
{
	TraceLog(("~CNodeControl()"));
	Clear();
}

void
CNodeControl::Clear()
{
	if(m_NodeInfo){
		delete m_NodeInfo;
		m_NodeInfo = 0;
	}
}

void CNodeControl::SetNodeInfo(SNodeViewInfo* p) 
{ 
	TraceLog(("SetNodeInfo(op=%d)", p->operationalState));
	m_NodeInfo = p; 
	/*
	if(p->operationalState) 
	{
		SetBGColor(NODE_COLOR_YELLOW);
	}
	else
	{
		SetBGColor(NODE_COLOR_GRAY);
	}
	*/
}

bool
CNodeControl::CreateNode(CString& pObjectClass, CString& pObjectId, int x, int y)
{
	TraceLog(("CreateNode(%s,%s)", pObjectClass, pObjectId));

	if(m_planoCtrl==0){
		return FALSE;
	}

	SPlanoInfo* aPlanoInfo = this->m_planoCtrl->GetInfo();

	Clear();
	m_NodeInfo = new SNodeViewInfo;
	m_NodeInfo->mgrId = aPlanoInfo->mgrId;
	m_NodeInfo->siteId = aPlanoInfo->siteId;
	m_NodeInfo->planoId = aPlanoInfo->planoId;
	m_NodeInfo->objectClass = pObjectClass;
	m_NodeInfo->objectId = pObjectId;
	m_NodeInfo->x = x;
	m_NodeInfo->y = y;
	m_NodeInfo->nodeId = pObjectClass + "::" + pObjectId;

	NodeViewInfoList nodeInfoList;
	if(0 < CCopModule::GetObject()->GetNodeList(m_NodeInfo->mgrId,m_NodeInfo->siteId,m_NodeInfo->planoId,m_NodeInfo->nodeId,-1,-1,nodeInfoList)){
		CString errMsg;
		errMsg.Format(LoadStringById(IDS_PLANOVIEW_STR013), m_NodeInfo->nodeId);
		TraceLog((errMsg));
		UbcMessageBox(errMsg);
		return false;
	}

	
	if(!CCopModule::GetObject()->GetNodeObjectInfo(m_NodeInfo)){
		UbcMessageBox("Get Node Object infomation failed"); // 수정필요
		return false;
	}

	if(!CreateIcon(this->m_planoCtrl->IsShowInfo())){
		UbcMessageBox("Create Icon failed"); // 수정필요
		delete m_NodeInfo;
		m_NodeInfo = 0;
		return false;
	}
	TraceLog(("CreateIcon succeed(%s,%s)", pObjectClass, pObjectId));
	SetCreate();
	return true;
}

bool
CNodeControl::CreateObject()
{
	TraceLog(("CreateObject()"));
	if(m_NodeInfo==0){
		return false;
	}

	int retval = CCopModule::GetObject()->CreateNode(m_NodeInfo);

	if(retval < 0){
		TraceLog(("%s create failed (retval=%d)", m_NodeInfo->nodeId, retval));
		delete m_NodeInfo;
		m_NodeInfo = 0;
		return false;
	}

	if(retval == 0){
		CString errMsg  ;
		CString msg = LoadStringById(IDS_PLANOVIEW_STR010);
		errMsg.Format(msg, m_NodeInfo->objectId);
		TraceLog((errMsg));
		UbcMessageBox(errMsg);
		delete m_NodeInfo;
		m_NodeInfo = 0;
		return false;
	}
	return true;
}

bool
CNodeControl::DeleteObject()
{
	TraceLog(("DeleteObject()"));
	if(m_NodeInfo==0){
		return false;
	}

	int retval = CCopModule::GetObject()->DeleteNode(m_NodeInfo);

	if(retval <= 0){
		TraceLog(("%s delete failed (retval=%d)", m_NodeInfo->nodeId, retval));
		delete m_NodeInfo;
		m_NodeInfo = 0;
		return false;
	}
	return true;
}

bool
CNodeControl::ModifyObject()
{
	TraceLog(("ModifyObject()"));
	if(m_NodeInfo==0){
		return false;
	}

	int retval = CCopModule::GetObject()->SetNode(m_NodeInfo);

	if(retval <= 0){
		TraceLog(("%s set failed (retval=%d)", m_NodeInfo->nodeId, retval));
		delete m_NodeInfo;
		m_NodeInfo = 0;
		return false;
	}
	return true;
}

bool
CNodeControl::LoadIconFile()
{
	TraceLog(("LoadIconFile()"));

	if(m_NodeInfo==0){
		return FALSE;
	}

	CString iconFile;
	iconFile.Format("ICON_%s_%s_%d.png", m_NodeInfo->objectClass, m_NodeInfo->objectType, m_NodeInfo->operationalState);

	CString szServerLocation = "/Config/Plano/" ;
	CString szLocalLocation;
	szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

	return this->Load(iconFile, szLocalLocation, szServerLocation);
}

bool
CNodeControl::CreateIcon(bool showInfo)
{
	TraceLog(("CreateIcon()"));

	bool retval = LoadIconFile();

	if(retval) 
	{
		CPoint point(m_NodeInfo->x,m_NodeInfo->y);
		CSize size(this->GetWidth(), this->GetHeight());

		CRect rect(point,size);
		ScreenToClient(rect);

		retval = this->Create(NULL, 
					WS_CHILD|WS_VISIBLE|BS_OWNERDRAW|SS_NOTIFY,
					rect,
					(CWnd*)m_planoCtrl,
					IDC_PIC_NODE);

		this->ModifyStyleEx(0,WS_EX_STATICEDGE,0);

		if(retval){
			{
				CPoint point(m_NodeInfo->x,m_NodeInfo->y-FONT_HEIGHT-INFO_OFFSET);
				CSize size(FONT_WIDTH*m_NodeInfo->targetName.GetLength(), FONT_HEIGHT);
				CRect rect(point, size);

				m_nameLabel.Create(m_NodeInfo->targetName,
							WS_CHILD|BS_OWNERDRAW,
							rect,
							(CWnd*)m_planoCtrl,
							IDC_STATIC_NAMELABEL);
			}
			{
				/*
				CString value = ::ToString(m_NodeInfo->nodeValue);

				CPoint point(m_NodeInfo->x,m_NodeInfo->y+this->GetHeight()+INFO_OFFSET);
				CSize size(FONT_WIDTH*value.GetLength(),FONT_HEIGHT);
				CRect rect (point, size);

				m_valueLabel.Create(value,
							WS_CHILD|BS_OWNERDRAW,
							rect,
							(CWnd*)m_planoCtrl,
							IDC_STATIC_VALUELABEL);
							*/
			}
			if(showInfo) {
				m_nameLabel.ShowWindow(SW_SHOW);
				//m_valueLabel.ShowWindow(SW_SHOW);
			}
		}
	};
	return retval;
}

BEGIN_MESSAGE_MAP(CNodeControl, CDownloadPictureEx)
ON_WM_PAINT()
ON_WM_DESTROY()
ON_WM_LBUTTONDOWN()
ON_WM_LBUTTONUP()
ON_WM_RBUTTONUP()
ON_COMMAND(ID_PLANO_DELETENODE, &CNodeControl::OnPlanoDeletenode)
ON_WM_MOUSEMOVE()
//ON_WM_NCPAINT()
ON_COMMAND(ID_PLANO_REBOOT, &CNodeControl::OnPlanoReboot)
ON_COMMAND(ID_PLANO_MUTEON, &CNodeControl::OnPlanoMuteon)
ON_COMMAND(ID_PLANO_MUTEOFF, &CNodeControl::OnPlanoMuteoff)
ON_COMMAND(ID_PLANO_POWERON, &CNodeControl::OnPlanoPoweron)
ON_COMMAND(ID_PLANO_POWEROFF, &CNodeControl::OnPlanoPoweroff)
ON_WM_LBUTTONDBLCLK()
//ON_WM_NCMOUSEMOVE()
ON_COMMAND(ID_NODE_REMOTELOGIN2, &CNodeControl::OnNodeRemotelogin2)
ON_COMMAND(ID_NODE_REMOTELOGIN, &CNodeControl::OnNodeRemotelogin)
ON_COMMAND(ID_NODEMENU_PACKAGEAPPLY, &CNodeControl::OnNodemenuPackageapply)
ON_COMMAND(ID_NODEMENU_CANCELPACKAGE, &CNodeControl::OnNodemenuCancelpackage)
END_MESSAGE_MAP()

void CNodeControl::OnPaint()
{
	TraceLog(("OnPaint()"));
	if(this->m_planoCtrl->IsShowInfo()){
		m_nameLabel.ShowWindow(SW_SHOW);
		//m_valueLabel.ShowWindow(SW_SHOW);
	}else{
		m_nameLabel.ShowWindow(SW_HIDE);
		//m_valueLabel.ShowWindow(SW_HIDE);
	}
	CDownloadPictureEx::OnPaint();
}

void CNodeControl::OnDestroy()
{
	TraceLog(("OnDestroy()"));
	m_nameLabel.DestroyWindow();
	//m_valueLabel.DestroyWindow();
	CDownloadPictureEx::OnDestroy();
}



void CNodeControl::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	TraceLog(("OnRButtonUp"));

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	if(m_planoCtrl->IsEditMode()){
		pPopup = menu.GetSubMenu(6); //PLANO Menu

		//pPopup->EnableMenuItem(ID_PLANO_DELETENODE,	false);

		ClientToScreen(&point);
		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,point.x, point.y, this);
	}else{
		this->m_planoCtrl->SelectAll(false); // 그리고 해당하는 놈에 대해서만, 작동되어야 한다.
		this->Selected(true); // 먼저 Select 되어야 한다.  
		pPopup = menu.GetSubMenu(7); //Node Menu 2
		if(m_NodeInfo && (m_NodeInfo->objectClass == "Light" || m_NodeInfo->objectClass == "Monitor")){
			TraceLog(("It's Light"));
			pPopup->DeleteMenu(ID_NODE_REMOTELOGIN2,	MF_BYCOMMAND);
			pPopup->DeleteMenu(ID_NODE_REMOTELOGIN,	MF_BYCOMMAND);
			pPopup->DeleteMenu(ID_NODEMENU_PACKAGEAPPLY,	MF_BYCOMMAND);
			pPopup->DeleteMenu(ID_NODEMENU_CANCELPACKAGE,	MF_BYCOMMAND);
			pPopup->DeleteMenu(ID_PLANO_REBOOT,	MF_BYCOMMAND);
			pPopup->DeleteMenu(ID_PLANO_MUTEON,	MF_BYCOMMAND);
			pPopup->DeleteMenu(ID_PLANO_MUTEOFF,	MF_BYCOMMAND);
		}
		if(!m_DrawMode) ClientToScreen(&point);
		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,point.x, point.y, this);
		
	}

	CDownloadPictureEx::OnRButtonUp(nFlags, point);
}

void CNodeControl::OnPlanoDeletenode()
{
	UnLoad();
	//ShowWindow(SW_HIDE);
	m_nameLabel.DestroyWindow();
	//m_valueLabel.DestroyWindow();
	SetDelete();
	DestroyWindow();
	//this->m_planoCtrl->RedrawWindow();
}

//BOOL CNodeControl::PreTranslateMessage(MSG* pMsg)
//{
//	return CDownloadPictureEx::PreTranslateMessage(pMsg);
//}
void CNodeControl::OnLButtonDown(UINT nFlags, CPoint point)
{
	TraceLog(("OnLButtonDown"));
	if(m_planoCtrl->IsEditMode()){
		m_moving = true;
		this->SetCapture();
		ClientToScreen(&point);
		m_start_x = point.x;
		m_start_y = point.y;
	}	

	Selected((m_selected ? false : true));
	CDownloadPictureEx::OnLButtonDown(nFlags, point);
}
void CNodeControl::OnLButtonUp(UINT nFlags, CPoint point)
{
	TraceLog(("OnLButtonUp"));
	EndMoving();
	CDownloadPictureEx::OnLButtonUp(nFlags, point);
}

void CNodeControl::EndMoving()
{
	if(m_planoCtrl->IsEditMode() && m_moving){
		TraceLog(("EndMoving"));
		if(this->isNoAction()) {
			SetModify();
		}
		m_moving = false;
		::ReleaseCapture();
	}	
}


void CNodeControl::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(m_planoCtrl->IsEditMode() && m_moving){
		CRect rect;
		GetClientRect(rect);

		int width = rect.Width();
		int height = rect.Height();

		ClientToScreen(&point);

		int dx = point.x - m_start_x;
		int dy = point.y - m_start_y;

		m_start_x = point.x;
		m_start_y = point.y;

		int newx = this->m_NodeInfo->x + dx;
		int newy = this->m_NodeInfo->y + dy;

		TraceLog(("w:h=%d:%d, org point(%d,%d) --> move point(%d,%d)", 
			width,height,m_NodeInfo->x, m_NodeInfo->y, dx, dy));

		//CWnd* pWnd = GetParent();
		//SetWindowPos(pWnd,point.x,point.y,width,height,SWP_SHOWWINDOW);
		MoveWindow(newx,newy,width,height);
		//MoveWindow(point.x,point.y,width,height);
		this->m_NodeInfo->x = newx;
		this->m_NodeInfo->y = newy;
		//this->m_NodeInfo->x = point.x;
		//this->m_NodeInfo->y = point.y;

		if(this->m_planoCtrl->IsShowInfo()){

			this->m_nameLabel.MoveWindow(m_NodeInfo->x,
										 m_NodeInfo->y-FONT_HEIGHT-INFO_OFFSET,
										 FONT_WIDTH*m_NodeInfo->targetName.GetLength(), 
										 FONT_HEIGHT);					

			//CString value = ::ToString(m_NodeInfo->nodeValue);
			//this->m_valueLabel.MoveWindow(m_NodeInfo->x,
			//								m_NodeInfo->y+this->GetHeight()+INFO_OFFSET,
			//								FONT_WIDTH*value.GetLength(),
			//								FONT_HEIGHT);
		}
	}
	CDownloadPictureEx::OnMouseMove(nFlags, point);
}
/*
void CNodeControl::OnNcMouseMove(UINT nHitTest, CPoint point)
{
	CDownloadPictureEx::OnNcMouseMove(nHitTest, point);
}
*/
void CNodeControl::Selected(bool select, bool redraw /*=true*/)
{
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDownloadPictureEx::OnNcPaint()을(를) 호출하지 마십시오.

	TraceLog(("Selected(%d)", select));

	// convert to client coordinates                

	m_selected = select;
	if(m_DrawMode){
		TraceLog(("It's draw mode"));
		if(this->m_selected){
			CDC* dc = m_planoCtrl->GetDC();
			CPoint point(m_NodeInfo->x,m_NodeInfo->y);
			CSize size(this->GetWidth()-1, this->GetHeight()-1);
			CRect rect(point,size);
			dc->Draw3dRect(&rect, RGB(255,0,0), RGB(255,0,0));                
			m_planoCtrl->ReleaseDC(dc);
		}else{
			TraceLog(("Not Selected(%d)", redraw));
			if(redraw) m_planoCtrl->RedrawWindow();
		}
		
	}else{
		TraceLog(("It's not draw mode"));
		CRect rect;                
		GetWindowRect(&rect);                
		rect.OffsetRect((-rect.left), (-rect.top));
		// Draw a single line around the outside                
		CWindowDC dc(this);                
		//CBrush brush(2,RGB(255,0,0));
		if(select) {
			dc.Draw3dRect(&rect, RGB(255,0,0), RGB(255,0,0));                
			//dc.FrameRect(&rect, &brush);                
		}else{
			TraceLog(("Not Selected"));
			dc.Draw3dRect(&rect, RGB(255, 255, 255), RGB(255, 255, 255));                
		}
	}
	//this->RedrawWindow();
	return ; 
}

void CNodeControl::OnPlanoReboot()
{
	//UbcMessageBox("Reboot : not yet implemented");
	this->m_planoCtrl->Reboot();

}

void CNodeControl::OnPlanoMuteon()
{
	//UbcMessageBox("MuteOn : not yet implemented");
	this->m_planoCtrl->Muteon();
}

void CNodeControl::OnPlanoMuteoff()
{
	//UbcMessageBox("MuteOff : not yet implemented");
	this->m_planoCtrl->Muteoff();
}

void CNodeControl::OnPlanoPoweron()
{
	//UbcMessageBox("PowerOn : not yet implemented");
	this->m_planoCtrl->Poweron();
}

void CNodeControl::OnPlanoPoweroff()
{
	//UbcMessageBox("PowerOff : not yet implemented");
	this->m_planoCtrl->Poweroff();
}

void CNodeControl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	CString strName = "";
	if(this->m_NodeInfo->objectClass == "Host")
	{
		// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
		CHostDetailDlg dlg;

		SHostInfo Info;
		if(!toHostInfo(Info)){
			return;
		}

		dlg.SetInfo(Info);
		if(dlg.DoModal() == IDOK)
		{
			dlg.GetInfo(Info);

			if(!CCopModule::GetObject()->SetHost(Info, GetEnvPtr()->m_szLoginID))
			{
				CString szMsg;
				szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(szMsg, MB_ICONERROR);
			}else{
				strName = Info.hostName;
			}
		}
	}else
	if(this->m_NodeInfo->objectClass == "Light")
	{
		// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
		CLightDetailDlg dlg;

		SLightInfo Info;
		if(!toLightInfo(Info)){
			return;
		}
		//Info.siteId = this->m_NodeInfo->hostSiteId;
		//Info.hostId = this->m_NodeInfo->hostId;
		//Info.lightId = this->m_NodeInfo->objectId;

		dlg.SetInfo(Info);
		if(dlg.DoModal() == IDOK)
		{
			dlg.GetInfo(Info);

			if(!CCopModule::GetObject()->SetLight(Info, GetEnvPtr()->m_szLoginID))
			{
				CString szMsg;
				szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(szMsg, MB_ICONERROR);
			}else{
				strName = Info.lightName;
			}
		}
	}else
	if(this->m_NodeInfo->objectClass == "Monitor")
	{
		// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
		CMonitorDetailDlg dlg;

		SMonitorInfo Info;
		if(!toMonitorInfo(Info)){
			return;
		}
		//Info.siteId = this->m_NodeInfo->hostSiteId;
		//Info.hostId = this->m_NodeInfo->hostId;
		//Info.monitorId = this->m_NodeInfo->objectId;

		dlg.SetInfo(Info);
		if(dlg.DoModal() == IDOK)
		{
			dlg.GetInfo(Info);

			if(!CCopModule::GetObject()->SetMonitor(Info, GetEnvPtr()->m_szLoginID))
			{
				CString szMsg;
				szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(szMsg, MB_ICONERROR);
			}else{
				strName = Info.monitorName;
			}
		}
	}		
	TraceLog(("strNmae=%s", strName));
	if(!strName.IsEmpty()){
		if(this->m_planoCtrl->IsShowInfo()) {
			TraceLog(("name change"));
			this->m_nameLabel.SetWindowTextA(strName);
			this->m_nameLabel.MoveWindow(m_NodeInfo->x,
										 m_NodeInfo->y-FONT_HEIGHT-INFO_OFFSET,
										 FONT_WIDTH*strName.GetLength(), 
										 FONT_HEIGHT);					

			this->m_nameLabel.RedrawWindow();
		}
	}

	CDownloadPictureEx::OnLButtonDblClk(nFlags, point);
}

//skpark 1111
void CNodeControl::DrawIt() 
{
	TraceLog(("DrawIt()"));

	m_DrawMode = true;
	if(!m_nodeImage.IsNull() && m_planoCtrl)
	{
		TraceLog(("DrawIt(%d,%d,%d,%d)", m_NodeInfo->x,m_NodeInfo->y, this->GetWidth(), this->GetHeight()));
	
		CDC* dc = m_planoCtrl->GetDC();

		CPoint point(m_NodeInfo->x,m_NodeInfo->y);

		m_nodeImage.AlphaBlend(dc->GetSafeHdc(), point);
		if(this->m_selected){
			CSize size(this->GetWidth()-1, this->GetHeight()-1);
			CRect rect (point,size);
			dc->Draw3dRect(&rect, RGB(255,0,0), RGB(255,0,0));                
		}
		m_planoCtrl->ReleaseDC(dc);
	}
	//CStatic::OnPaint();

}

//skpark 1111
bool CNodeControl::Load(CString& fileName, CString& localLocation, CString& serverLocation) 
{
	if(m_planoCtrl->IsEditMode()){
		return CDownloadPictureEx::Load(fileName, localLocation, serverLocation);
	}

	TraceLog(("Load(%s,%s,%s)", fileName, localLocation, serverLocation));

	CString strMediaFullPath;
	strMediaFullPath.Format("%s%s", localLocation,fileName);

	CString strExt = FindExtension(strMediaFullPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	
	UnLoad();

	bool bRet = false;
	for(int i=0;i<2;i++) { // 2차례 시도한다.
		bRet = Download(fileName,localLocation,serverLocation);
		HRESULT result = m_nodeImage.Load(strMediaFullPath);
		TraceLog(("result=%ld", result));
		if(bRet){
			TraceLog(("%s file loaded from server", strMediaFullPath));
			return true;
		}
		TraceLog(("%s file not exist", strMediaFullPath));
	}
	TraceLog(("%s file load failed", strMediaFullPath));
	// 수정필요
	// 로드에 fail 할 때, default 를 하도록 한다.!!!
	return bRet;
}
// skpark 1111
void CNodeControl::UnLoad()
{
	if(m_planoCtrl->IsEditMode()){
		CDownloadPictureEx::UnLoad();
		return;
	}

	if(!m_nodeImage.IsNull())
	{
		m_nodeImage.Destroy();
	}
}
/*
BOOL CNodeControl::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(pMsg->message == WM_NCMOUSEMOVE ){
		TraceLog(("WM_NCMOUSEMOVE"));
		if(!m_planoCtrl->IsEditMode()){
			TraceLog(("Bubble Off"));
			this->m_nameLabel.ShowWindow(SW_HIDE);
		}		
	}else
	if(pMsg->message == WM_MOUSEMOVE ){
		TraceLog(("WM_MOUSEMOVE"));
		if(!m_planoCtrl->IsEditMode()){
			// 버블도움말을 보인다.
			TraceLog(("Bubble On : %d,%d", m_NodeInfo->x, m_NodeInfo->y-FONT_HEIGHT-INFO_OFFSET));
			this->m_nameLabel.MoveWindow(m_NodeInfo->x,
										 m_NodeInfo->y-FONT_HEIGHT-INFO_OFFSET,
										 FONT_WIDTH*m_NodeInfo->targetName.GetLength(), 
										 FONT_HEIGHT);					
			this->m_nameLabel.ShowWindow(SW_SHOW);
		}
	}

	return CDownloadPictureEx::PreTranslateMessage(pMsg);
}
*/
void CNodeControl::ShowName(bool show)
{
	TraceLog(("ShowName(show=%d, m_isShowName=%d)", show, m_isShowName));
	if(show){
		TraceLog(("Bubble On : %d,%d", m_NodeInfo->x, m_NodeInfo->y-FONT_HEIGHT-INFO_OFFSET));
		this->m_nameLabel.MoveWindow(m_NodeInfo->x,
									 m_NodeInfo->y-FONT_HEIGHT-INFO_OFFSET,
									 FONT_WIDTH*m_NodeInfo->targetName.GetLength(), 
									 FONT_HEIGHT);					
		this->m_nameLabel.ShowWindow(SW_SHOW);
		m_isShowName = true;	
	}else{
		TraceLog(("Bubble Off : %d,%d", m_NodeInfo->x, m_NodeInfo->y-FONT_HEIGHT-INFO_OFFSET));
		this->m_nameLabel.ShowWindow(SW_HIDE);
		m_isShowName = false;	
	}
}


void CNodeControl::OnNodeRemotelogin2()
{
	this->m_planoCtrl->Remotelogin2();
}

void CNodeControl::OnNodeRemotelogin()
{
	this->m_planoCtrl->Remotelogin();
}


void CNodeControl::OnNodemenuPackageapply()
{
	this->m_planoCtrl->ApplyPackage();
}

void CNodeControl::OnNodemenuCancelpackage()
{
	this->m_planoCtrl->CancelPackage();
}

bool CNodeControl::toHostInfo(SHostInfo& outInfo)
{

	HostInfoList lsHost;

	cciCall aCall;
	if(CCopModule::GetObject()->GetHostForAll(&aCall, this->m_NodeInfo->hostSiteId, this->m_NodeInfo->hostId, NULL))
	{
		CCopModule::GetObject()->GetHostData(&aCall, lsHost);
	}
	POSITION pos = lsHost.GetHeadPosition();
	if(pos){
		outInfo = lsHost.GetNext(pos);
		return true;
	}

	UbcMessageBox(LoadStringById(IDS_PLANOMGRVIEW_STR018), MB_ICONINFORMATION);
	return false;
}
bool CNodeControl::toMonitorInfo(SMonitorInfo& outInfo)
{

	MonitorInfoList lsMonitor;

	if(CCopModule::GetObject()->GetMonitorList(this->m_NodeInfo->hostSiteId, this->m_NodeInfo->objectId,
												NULL, false,false,lsMonitor))
	{
		POSITION pos = lsMonitor.GetHeadPosition();
		if(pos){
			outInfo = lsMonitor.GetNext(pos);
			return true;
		}
	}

	UbcMessageBox(LoadStringById(IDS_PLANOMGRVIEW_STR018), MB_ICONINFORMATION);
	return false;
}
bool CNodeControl::toLightInfo(SLightInfo& outInfo)
{

	LightInfoList lsLight;

	if(CCopModule::GetObject()->GetLightList(this->m_NodeInfo->hostSiteId, this->m_NodeInfo->objectId, 
												NULL, false,false,lsLight))
	{
		POSITION pos = lsLight.GetHeadPosition();
		if(pos){
			outInfo = lsLight.GetNext(pos);
			return true;
		}
	}
	UbcMessageBox(LoadStringById(IDS_PLANOMGRVIEW_STR018), MB_ICONINFORMATION);
	return false;
}
