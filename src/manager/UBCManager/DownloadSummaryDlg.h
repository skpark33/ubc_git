#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "Enviroment.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"
#include "common\CheckComboBox.h"

// CDownloadSummaryDlg 대화 상자입니다.

class CDownloadSummaryDlg : public CDialog
{
	DECLARE_DYNAMIC(CDownloadSummaryDlg)

typedef struct tagLIST_FILTER
{
	CString strBpId;
	CString strBpName;
	CString strBpPackageList;
	CString strBpHostList;
	CString strPackageId;
	CString strHostId;
	CString strHostName;
} ST_LIST_FILTER, *P_LIST_FILTER;

public:
	CDownloadSummaryDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDownloadSummaryDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DOWNLOAD_SUMMARY_DLG };
	enum { eStatus, eSite, eHostName, eHostId, eBrwId, ePackage, eProgress, eStartTime, eEndTime, eStatusDesc, eMaxCol };

	void SetParam(CString strBpId, CString strBpName, CString strPackageId, CString strHostId, CString strHostName)
		{
			m_stListFilter.strBpId = strBpId;
			m_stListFilter.strBpName = strBpName;
			m_stListFilter.strPackageId = strPackageId;
			m_stListFilter.strHostId = strHostId;
			m_stListFilter.strHostName = strHostName;
		}

protected:
	ST_LIST_FILTER				m_stListFilter;
	CRect						m_rcClient;
	bool						m_bInitialize;
	CReposControl				m_Reposition;
	CUTBListCtrlEx				m_lcList;
	CHoverButton				m_bnRefresh;
	CHoverButton				m_bnClose;
	CHoverButton				m_btnExcelSave;
	CCheckComboBox				m_cbBpPackage;
	CCheckComboBox				m_cbBpHost;
	CStatic						m_Filter;
	CString						m_strHostId;
	CString						m_strBpId;

	CString						BRW_NAME[2];
	CString						m_szColum[eMaxCol];
	CImageList					m_ilListImage;
	DownloadSummaryInfoList		m_lsInfoList;

	TimePlanInfoList			m_lsTplist;
	TargetHostInfoList			m_lsThlist;

	void						InitList();
	void						InitBpPackageAndHostCombo();
	void						RefreshList();
	CString						GetStateString(int nValue);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBnClickedButtonFilterHost();
	afx_msg void OnNMDblclkListDownload(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonFilterRegister();
	afx_msg void OnBnClickedButtonFilterPackage();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnBnClickedButtonFilterPlanName();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnBnClickedBnToExcel();
};
