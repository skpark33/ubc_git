#pragma once

#include "ubccopcommon\eventhandler.h"

// CLightFrame frame

class CLightFrame : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CLightFrame)
protected:
	CLightFrame();           // protected constructor used by dynamic creation
	virtual ~CLightFrame();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
