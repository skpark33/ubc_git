#pragma once

#include "ubccopcommon\eventhandler.h"


// CProcessFrm 프레임입니다.

class CProcessFrm : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CProcessFrm)
protected:
	CProcessFrm();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CProcessFrm();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
