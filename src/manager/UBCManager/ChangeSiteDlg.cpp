// ChangeSiteDlg.cpp : implementation file
//
#include "stdafx.h"
#include "ChangeSiteDlg.h"
#include "Enviroment.h"
#include "ubccopcommon/SiteSelectDlg.h"

// CChangeSiteDlg dialog
IMPLEMENT_DYNAMIC(CChangeSiteDlg, CDialog)

CChangeSiteDlg::CChangeSiteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChangeSiteDlg::IDD, pParent)
{
}

CChangeSiteDlg::~CChangeSiteDlg()
{
}

void CChangeSiteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_REBOOT, m_ckReboot);
}

BEGIN_MESSAGE_MAP(CChangeSiteDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CChangeSiteDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_SITE, &CChangeSiteDlg::OnBnClickedButtonSite)
END_MESSAGE_MAP()

// CChangeSiteDlg message handlers

BOOL CChangeSiteDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetDlgItemText(IDC_EDIT_SITENAME, m_szSelSiteName);
	m_ckReboot.SetCheck(true);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CChangeSiteDlg::OnBnClickedOk()
{
	if(m_szSelSiteName.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG004), MB_ICONINFORMATION);
		return;
	}

	m_bReboot = m_ckReboot.GetCheck();

	OnOK();
}

void CChangeSiteDlg::OnBnClickedButtonSite()
{
	CString strSiteName;
	CString strSiteId;

	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
	if(dlg.DoModal() == IDOK)
	{
		SSiteInfo info;
		if(dlg.GetSelectSiteInfo(info))
		{
			strSiteName = info.siteName;
			strSiteId = info.siteId;
		}
	}

	SetDlgItemText(IDC_EDIT_SITENAME, strSiteName);
	m_szSelSiteName = strSiteName;
	m_szSelSiteId = strSiteId;
}
