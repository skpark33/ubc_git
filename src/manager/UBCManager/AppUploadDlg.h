#pragma once

#include "common/libFileServiceWrap/FileServiceWrapDll.h"
#include "afxwin.h"
#include "afxcmn.h"

// CAppUploadDlg 대화 상자입니다.

class CAppUploadDlg 
	: public CDialog
	, public CFileServiceWrap::IProgressHandler
{
	DECLARE_DYNAMIC(CAppUploadDlg)

public:
	enum DLGTYPE { eFirmware, eKPOSTPlayer, eVaccineApp, eVaccineDB, eManagerUpdate, eServerUpdate, eFirmware2, eKPOSTPlayer2, eUBCPlayer, eUBCLauncher };

	CAppUploadDlg(DLGTYPE eDlgType, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAppUploadDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_APP_UPLOAD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	BOOL InitData(CString customer);

	DECLARE_MESSAGE_MAP()

	DLGTYPE	m_eDlgType;
	BOOL	m_bLogin;
	BOOL	m_bUploading;
	BOOL	m_bUserAbort;

	int		m_nOldVersion;
	CString	m_strNewVersion;
	bool	m_bIsAlphaVersion;
	ULONGLONG	m_ulNewFileSize;

	CString	m_strVersionServerPath;
	CString	m_strVersionServerOldPath;
	CString	m_strVersionServerTmpPath;
	CString	m_strVersionLocalPath;

	CString	m_strAppServerPath;
	CString	m_strAppServerOldPath;
	CString	m_strAppServerTmpPath;
	CString	m_strAppLocalPath;

	CString h_customer;
	CString l_customer;

	CFileServiceWrap m_objFileSvcClient;
	CWinThread*	m_pThread;

	BOOL	GetDataFromVersionFile(LPCSTR lpszPath);
	BOOL	GetZipFileNameFromShell(LPCSTR lpszPath);

public:
	CEdit	m_editOldVersion;
	CEdit	m_editOldFileUploadTime;
	CEdit	m_editOldFileSize;
	CEdit	m_editNewVersion;
	CButton	m_chkAlphaVersion;
	CEdit	m_editNewFilePath;
	CEdit	m_editNewFileSize;

	CProgressCtrl	m_pc;
	CStatic	m_stcProgress;

	CButton	m_btnNewFileSelect;
	CButton	m_btnUpload;
	CButton	m_btnCancel;

	afx_msg void OnBnClickedButtonNewFileSelect();
	afx_msg void OnBnClickedButtonUpload();
	afx_msg LRESULT OnCompleteUpload(WPARAM wParam, LPARAM lParam);

	static UINT	UploadThreadProc(LPVOID param);
	virtual void ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath);

	BOOL	UploadFile();
	CStatic m_stZipFileName;
	CString m_strZipLocalPath;
	CString m_strZipServerPath;
	CString m_strZipServerOldPath;
	CString m_strZipServerTmpPath;

	afx_msg void OnBnClickedButtonApplyCid();
	CEdit m_editCID;
};
