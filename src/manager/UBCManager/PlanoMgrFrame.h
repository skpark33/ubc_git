#pragma once

#include "ubccopcommon\eventhandler.h"

// CPlanoMgrFrame frame

class CPlanoMgrFrame : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CPlanoMgrFrame)
protected:
	CPlanoMgrFrame();           // protected constructor used by dynamic creation
	virtual ~CPlanoMgrFrame();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
