// MainFrm.h : interface of the CMainFrame class
//

#pragma once

const UINT WM_WAIT_FOR_CREATED  = RegisterWindowMessage(_T("WM_WAIT_FOR_CREATED"));
#define GOTO_INITIALIZE_POST	AfxBeginThread(CMainFrame::WaitForWindowCreated, GetSafeHwnd());

const UINT WM_SET_STATUSBAR_TEXT  = RegisterWindowMessage(_T("WM_SET_STATUSBAR_TEXT"));
const UINT WM_FILTER_HOST_CHANGE  = RegisterWindowMessage(_T("WM_FILTER_HOST_CHANGE"));

#define TMID_HEARTBEAT	1
#define TMID_SUMALARM	2

#define SBID_TEXT		0
#define SBID_LED		1
#define SBID_HOST		2
#define SBID_PROC		3
#define SBID_COMM		4
#define SBID_HDD 		5
#define SBID_MEM 		6
#define SBID_CPU 		7
#define SBID_ETC 		8

#include "LoginInfoDlg.h"
#include "MainFrmBitmapBG.h"
#include "ubccopcommon\eventhandler.h"

class CServerFileUploadDlg;
class CHsrCommandDlg;


class CMainFrame : public CMDIFrameWnd, public IEventHandler
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();
	enum { eSiteView, eHostView, eUserView, eScreenView, ePackageView, eNotiView, eBroadcastPlan, eProcessView
		 //, eHostLogView
		 , eFaultMngView, eCodeView, eWebPage, eUpdateCenterView, ePrimaryLogView,eLMOSolutionView, 
		 ePlanoView, eLightView, eMonitorView, eIconView, ePlanoMgrView, eGeoMapView, eTrashCanView	};
		 //, eLogPackage, eLogPowerOnOff, eLogConnection, eLogFault, eLogDownload, eLogLogin, eLogPrice
		 //, eStatisticsFault, eStatisticsContents, eStatisticsInteract };

// Attributes
public:
	BOOL OpenDocument(UINT nID, CString strTitle, CString strParam = "");
	CMDIChildWnd* GetChildFrame(UINT nID);
	CMultiDocTemplate* GetDocTemplate(UINT nID);

private:
	CMultiDocTemplate* m_pSiteTemp;
	CMultiDocTemplate* m_pHostTemp;
	CMultiDocTemplate* m_pPlanoTemp;
	CMultiDocTemplate* m_pLightTemp;
	CMultiDocTemplate* m_pPlanoMgrTemp;
	CMultiDocTemplate* m_pIconTemp;
	CMultiDocTemplate* m_pMonitorTemp;
	CMultiDocTemplate* m_pUserTemp;
	CMultiDocTemplate* m_pScreenTemp;
	CMultiDocTemplate* m_pGeoMapTemp;
	CMultiDocTemplate* m_pPackageTemp;
	CMultiDocTemplate* m_pNotiTemp;
	CMultiDocTemplate* m_pLMOTemp;
	CMultiDocTemplate* m_pBroadcastPlanTemp;
	CMultiDocTemplate* m_pProcessTemp;
	//CMultiDocTemplate* m_pHostLogTemp;
	CMultiDocTemplate* m_pFaultMngTemp;
	CMultiDocTemplate* m_pCodeTemp;
	CMultiDocTemplate* m_pWebPageTemp;
	CMultiDocTemplate* m_pUpdateCenterTemp;
	//CMultiDocTemplate* m_pLogPackageTemp;
	//CMultiDocTemplate* m_pLogPowerOnOffTemp;
	//CMultiDocTemplate* m_pLogConnectionTemp;
	//CMultiDocTemplate* m_pLogFaultTemp;
	//CMultiDocTemplate* m_pLogDownloadTemp;
	//CMultiDocTemplate* m_pLogLoginTemp;
	//CMultiDocTemplate* m_pLogPriceTemp;
	//CMultiDocTemplate* m_pStatisticsFaultTemp;
	//CMultiDocTemplate* m_pStatisticsContentsTemp;
	//CMultiDocTemplate* m_pStatisticsInteractTemp;
//	CLoginInfoDlg m_LoginInfoDlg;
	CMultiDocTemplate* m_pPrimaryLogTemp;
	CMultiDocTemplate* m_pTrashCanTemp;

// Operations
public:
	static UINT WaitForWindowCreated(LPVOID pParam);
	static UINT BackgroundJobThread(LPVOID pParam);

	void SetStatusBarPaneText(int nIndex, LPCTSTR lpszNewText);
	void UpdateFilterData(CWnd* pFromWnd);

private:
	CList<int> m_lsEvID;
	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CMainFrmBitmapBG	m_wndBitmapBG;

	HICON m_hLedGreen ;
	HICON m_hLedRed   ;
	HICON m_hLedYellow;

//	SAlarmSummary m_stAlarmSummary;
	void InitAlarmSummary();
//	void SetAlarmSummary();

	DWORD m_dwHeartbeat;
	void InitHeartbeat();
	void SetHeartbeat();

	void InitMenuAuth();

	CString GetUrl(CString strKey);

	CServerFileUploadDlg*	m_pServerFileUpload;
	CHsrCommandDlg*			m_pHsrCommand;

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnFileSitemanager();
	afx_msg void OnFileHostmanager();
	afx_msg void OnFilePlano();
	afx_msg void OnFileUsermanager();
	afx_msg void OnFileScreenManager();
	afx_msg void OnPackageManager();
	afx_msg void OnNotification();
	afx_msg void OnToolUBCStudio();
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);
	LRESULT OnInitializePost(WPARAM wParam, LPARAM lParam);
	afx_msg void OnAutoUpdate();
	afx_msg void OnFileBroadcastPlanView();
	LRESULT OnStatusBarPaneText(WPARAM wParam, LPARAM lParam);
	afx_msg void OnFileHostProcess();
//	afx_msg void OnFileHostLog();
	afx_msg void OnFileFaultMng();
	afx_msg void OnAppBulletinboard();
	afx_msg void OnAppCodeMng();
	afx_msg void OnLMOSolutionMenu(); //skpark lmo_solution
	afx_msg void OnLogPackageChng();
	afx_msg void OnLogPowerOnoff();
	afx_msg void OnLogConnection();
	afx_msg void OnLogFault();
	afx_msg void OnLogDownload();
	afx_msg void OnLogLogin();
	afx_msg void OnStatisticsFault();
	afx_msg void OnStatisticsContents();
	afx_msg void OnStatisticsInteract();
//	afx_msg void OnFileContentsMng(); // 0001443: 매니져 상 콘텐츠관리 기능 오류 수정
	afx_msg void OnWebProgramDown();
	afx_msg void OnWebManualDown();
	afx_msg void OnWebVideoManual();
	afx_msg void OnUpdateMenuAuth(CCmdUI *pCmdUI);
	afx_msg void OnFileUpdateCenter();
	afx_msg void OnLogPackageApply();
	afx_msg void OnLogBroadcastPlan();
	afx_msg void OnLogPlayerError();
	afx_msg void OnStatisticsBroadcastplan();
	afx_msg void OnStatisticsPackageReg();
	afx_msg void OnStatisticsPackageUse();
	afx_msg void OnStatisticsContentsReg();
	afx_msg void OnStatisticsContentsUse();
	afx_msg void OnStatisticsContentsAcc();
	afx_msg void OnStatisticsMsgUse();
	afx_msg void OnStatisticsApply();
	afx_msg LRESULT OnCloseDialog(WPARAM wParam, LPARAM lParam);
	afx_msg void OnAlphaUpdate();
	afx_msg void OnColumnNextSchedule();
	afx_msg void OnFileLightlist();
	afx_msg void OnFilePlanoMgr();
	afx_msg void OnFileIconlist();
	afx_msg void OnFileMonitorlist();
	afx_msg void OnFileCacheserver();

	HWND getWHandle(unsigned long pid);
	int StrongSecurity();
	static UINT	StrongSecurityThread(LPVOID param);

	afx_msg void OnWebWelcomeboard();
	afx_msg void OnFileGeomap();
	afx_msg void OnTimesync();

	time_t GetServerTime();
	void TimeSet(time_t current);
	void ServerTimeCheck();

	bool IsHYUNDAI_KIA();
	bool IsWIA();
	bool IsKPOST();
	afx_msg void OnUploadFirmware();
	afx_msg void OnUploadUbcPlayerApp();
	afx_msg void OnUploadVaccineApp();
	afx_msg void OnUploadVaccineDB();
	afx_msg void OnToolCommander();
	afx_msg void OnToolUploader();
	afx_msg void OnUploadManagerUpdate();
	afx_msg void OnUploadServerUpdate();
	afx_msg void OnToolTrashcan();
	afx_msg void OnApprove();
	afx_msg void OnUseAxis2();

	//
protected:
	static CString GetRemoteLoginOptions();
public:
	//static BOOL	RemoteLoginSettings();
	static BOOL	RemoteLogin_Direct(LPCSTR lpszIP, int nVNCPort);
	static BOOL	RemoteLogin_viaServer(LPCSTR lpszSiteId, LPCSTR lpszHostId);
	static BOOL	RemoteLogin_viaSpecified(LPCSTR lpszSiteId, LPCSTR lpszHostId);
	afx_msg void OnUploadFirmware2();
	afx_msg void OnUploadUbcPlayerApp2();
	afx_msg void OnAppDgroup();
	afx_msg void OnUploadUbcCommonPalyerApp();
	afx_msg void OnUploadUbcCommonLauncherApp();
};
