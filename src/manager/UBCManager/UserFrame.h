#pragma once


// CUserFrame frame

class CUserFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CUserFrame)
protected:
	CUserFrame();           // protected constructor used by dynamic creation
	virtual ~CUserFrame();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


