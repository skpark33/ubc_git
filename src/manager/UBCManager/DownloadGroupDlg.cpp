// DownloadGroupDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "DownloadGroupDlg.h"

#include "UBCCopCommon/ContentsSelectDlg.h"
#include "UBCCopCommon/PackageSelectDlg.h"
#include "UBCCopCommon/SiteSelectDlg.h"

#include "common\UbcCode.h"
#include "InputBox.h"

// CDownloadGroupDlg 대화 상자입니다.

// CDownloadGroupDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDownloadGroupDlg, CDialog)

CDownloadGroupDlg::CDownloadGroupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDownloadGroupDlg::IDD, pParent)
{

}

CDownloadGroupDlg::~CDownloadGroupDlg()
{
}

void CDownloadGroupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOSTTYPE, m_cbHostType);
	DDX_Control(pDX, IDC_COMBO_FILTER_ADMIN, m_cbAdmin);
	DDX_Control(pDX, IDC_CB_OPERATION, m_cbOperation);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcHostList);
	DDX_Control(pDX, IDC_LIST_SEL_HOST, m_lcSelHostList);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDC_BUTTON_PLAN_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_COMBO_FILTER_TAG, m_cbFilterTag);
	DDX_Control(pDX, IDC_COMBO_FILTER_CACHE, m_cbFilterCache);
	DDX_Control(pDX, IDC_CB_DGROUP, m_cbDGroup);
}


BEGIN_MESSAGE_MAP(CDownloadGroupDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CDownloadGroupDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CDownloadGroupDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_PLAN_REFRESH, &CDownloadGroupDlg::OnBnClickedButtonPlanRefresh)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOST, &CDownloadGroupDlg::OnNMDblclkListHost)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SEL_HOST, &CDownloadGroupDlg::OnNMDblclkListSelHost)
	ON_BN_CLICKED(IDC_BN_ADD, &CDownloadGroupDlg::OnBnClickedBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, &CDownloadGroupDlg::OnBnClickedBnDel)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_SITE, &CDownloadGroupDlg::OnBnClickedButtonFilterSite)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_CONTENTS, &CDownloadGroupDlg::OnBnClickedButtonFilterContents)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PACKAGE, &CDownloadGroupDlg::OnBnClickedButtonFilterPackage)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_HOST, &CDownloadGroupDlg::OnLvnColumnclickListHost)
	ON_NOTIFY(NM_CLICK, IDC_LIST_HOST, &CDownloadGroupDlg::OnNMClickListHost)
	ON_CBN_SELCHANGE(IDC_CB_DGROUP, &CDownloadGroupDlg::OnCbnSelchangeCbDgroup)
END_MESSAGE_MAP()


// CDownloadGroupDlg 메시지 처리기입니다.


void CDownloadGroupDlg::UpdateInfo()
{
}

bool CDownloadGroupDlg::InputErrorCheck(bool bMsg)
{
	if(m_lcSelHostList.GetItemCount() <= 0)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_PLANSELECTHOST_MSG001));
		return false;
	}

	return true;
}

void CDownloadGroupDlg::RefreshInfo()
{
//	RefreshHostList();
	RefreshHostCnt();
	TraceLog(("5.1"));
	RefreshSelHostList();
	TraceLog(("5.2"));
	RefreshSelHostCnt();
	TraceLog(("5.3"));
}

BOOL CDownloadGroupDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.



	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnAdd    .LoadBitmap(IDB_BUTTON_NEXT2  , RGB(255,255,255));
	m_bnDel    .LoadBitmap(IDB_BUTTON_PREV2  , RGB(255,255,255));

	m_bnRefresh.SetToolTipText(LoadStringById(IDS_PLANSELECTHOST_BTN001));

	CodeItemList listHostType;
	CUbcCode::GetInstance(GetEnvPtr()->m_strCustomer)->GetCategoryInfo(_T("HostType"), listHostType);

	POSITION pos = listHostType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listHostType.GetNext(pos);
		m_cbHostType.AddString(info.strEnumString, info.nEnumNumber);
	}

	m_cbDGroup.AddString("Group 1");
	m_cbDGroup.AddString("Group 2");
	m_cbDGroup.AddString("Group 3");
	m_cbDGroup.AddString("Group 4");
	m_cbDGroup.AddString("Group 5");
	m_cbDGroup.AddString("Group 6");
	m_cbDGroup.AddString("Group 7");
	m_cbDGroup.AddString("Group 8");
	m_cbDGroup.AddString("Group 9");
	m_cbDGroup.AddString("Group 10");
	m_cbDGroup.AddString("Group 11");
	m_cbDGroup.AddString("Group 12");
	m_cbAdmin.SetCurSel(0);

	m_cbAdmin.AddString(LoadStringById(IDS_PLANSELECTHOST_STR005));
	m_cbAdmin.AddString(LoadStringById(IDS_PLANSELECTHOST_STR001));
	m_cbAdmin.AddString(LoadStringById(IDS_PLANSELECTHOST_STR002));
	m_cbAdmin.SetCurSel(0);

	m_cbOperation.AddString(LoadStringById(IDS_PLANSELECTHOST_STR005));
	m_cbOperation.AddString(LoadStringById(IDS_PLANSELECTHOST_STR003));
	m_cbOperation.AddString(LoadStringById(IDS_PLANSELECTHOST_STR004));
	m_cbOperation.SetCurSel(0);

	m_cbFilterCache.AddString(LoadStringById(IDS_PLANSELECTHOST_STR005));
	m_cbFilterCache.AddString(LoadStringById(IDS_PLANSELECTHOST_STR009));
	m_cbFilterCache.AddString(LoadStringById(IDS_PLANSELECTHOST_STR010));
	m_cbFilterCache.SetCurSel(0);


	CStringArray aTagList;
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}


	LoadFilterData();

	UpdateData(FALSE);

	InitHostList();
	InitSelHostList();

	RefreshInfo();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDownloadGroupDlg::InitHostList()
{
	m_lcHostList.SetExtendedStyle(m_lcHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck  ] = _T("");
	m_szColum[eHostName] = LoadStringById(IDS_PLANSELECTHOST_LIST004);
	m_szColum[eHostID ] = LoadStringById(IDS_PLANSELECTHOST_LIST001);
	m_szColum[eDisplay] = LoadStringById(IDS_PLANSELECTHOST_LIST002);
	m_szColum[eGroup  ] = LoadStringById(IDS_PLANSELECTHOST_LIST003);
	m_szColum[eCategory] = LoadStringById(IDS_HOSTVIEW_LIST036);

	m_lcHostList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcHostList.InsertColumn(eHostName, m_szColum[eHostName], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eHostID , m_szColum[eHostID ], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eDisplay, m_szColum[eDisplay], LVCFMT_CENTER,  50);
	m_lcHostList.InsertColumn(eGroup  , m_szColum[eGroup  ], LVCFMT_LEFT  ,  85);
	m_lcHostList.InsertColumn(eCategory, m_szColum[eCategory], LVCFMT_LEFT  ,  85);

	// 제일 마지막에 할것...
	m_lcHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcHostList.SetSortEnable(false);
//	m_lcHostList.SetFixedCol(eCheck);
}

void CDownloadGroupDlg::RefreshHostList()
{
	CWaitMessageBox wait;

	CString szSite = (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite);

	UpdateData();

	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);

	CString strWhere;

	// 단말이름
	strHostName.Trim();
	if(!strHostName.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostName like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostName
							);
	}

	// 단말ID
	strHostId.Trim();
	if(!strHostId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostId like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostId
							);
	}

	// 단말타입
	if(!strHostType.IsEmpty())
	{
		strHostType.Replace(",", "','");
		strWhere.AppendFormat(_T(" %s hostType in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostType
							);
	}

	bool has_where_siteId_alreay = false;
	// 소속조직
	if(!strSiteId.IsEmpty() && strSiteId != "*")
	{
		strSiteId.Replace(_T(","), _T("','"));
		strWhere.AppendFormat(_T(" %s siteId in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strSiteId
							);
		has_where_siteId_alreay = true;

	}

	// 포함콘텐츠
	if(!strContentsId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 in (select programId from ubc_contents where lower(contentsId) = lower('%s'))"
								      " or lastSchedule2 in (select programId from ubc_contents where lower(contentsId) = lower('%s')))")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strContentsId
							, strContentsId
							);
	}

	// 방송중인 패키지
	if(!strPackageId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 = '%s' or lastSchedule2 = '%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strPackageId
							, strPackageId
							);
	}

	// 연결상태
	if(_ttoi(strOperaStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s operationalState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strOperaStat)==1 ? 1:0)
							);
	}

	// 사용상태
	if(_ttoi(strAdminStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s adminState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strAdminStat)==1 ? 1:0)
							);
	}

	// 검색어
	strCategory.Trim();
	if(!strCategory.IsEmpty())
	{
		int pos = 0;
		CString strTag;
		while((strTag = strCategory.Tokenize("/", pos)) != _T(""))
		{
			//strWhere.AppendFormat(_T(" %s concat('/', category, '/') like '%%/%s/%%'")
			strWhere.AppendFormat(_T(" %s category like '%%%s%%'")
							 	 , (strWhere.IsEmpty() ? _T("") : _T("and"))
								 , strTag.MakeLower()
								 );
		}
	}

	if(m_cbFilterCache.GetCurSel()==1) // localhost
	{
		strWhere.AppendFormat(_T(" %s domainName = 'localhost'")
							, (strWhere.IsEmpty() ? _T("") : _T("and")));
	}else if(m_cbFilterCache.GetCurSel()==2) {
		strWhere.AppendFormat(_T(" %s (domainName != 'localhost' or domainName is null)")
							, (strWhere.IsEmpty() ? _T("") : _T("and")));
	}

	m_lsInfoList.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetHostForSelect(&aCall
														, ((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority|| has_where_siteId_alreay == true) ? _T("*") : GetEnvPtr()->m_szSite)
														, _T("*")
														, strWhere
														);
	if(bRet)
	{
		CCopModule::GetObject()->GetHostData(&aCall, m_lsInfoList);
	}

	m_lcHostList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsInfoList.GetNext(pos);

		int nDisplayCnt = (info.displayCounter > 1 ? 3 : 1);
		for(int i=1; i<=nDisplayCnt ;i++)
		{
			CString strDisplay;

			if(i==1) strDisplay = LoadStringById(IDS_PLANSELECTHOST_STR006);
			if(i==2) strDisplay = LoadStringById(IDS_PLANSELECTHOST_STR007);
			if(i==3) strDisplay = LoadStringById(IDS_PLANSELECTHOST_STR008);

			m_lcHostList.InsertItem(nRow, "");
			m_lcHostList.SetItemText(nRow, eHostName, info.hostName);
			m_lcHostList.SetItemText(nRow, eHostID , info.hostId);
			m_lcHostList.SetItemText(nRow, eDisplay, strDisplay);
			m_lcHostList.SetItemText(nRow, eGroup  , info.siteId);
			m_lcHostList.SetItemText(nRow, eCategory, info.category);
			m_lcHostList.SetItemData(nRow, (DWORD_PTR)posOld);

			nRow++;
		}
	}

	RefreshHostCnt();
}

void CDownloadGroupDlg::OnBnClickedButtonPlanRefresh()
{
	RefreshHostList();
}

void CDownloadGroupDlg::InitSelHostList()
{
	m_lcSelHostList.SetExtendedStyle(m_lcSelHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lcSelHostList.InsertColumn(eCheck   , m_szColum[eCheck   ], LVCFMT_CENTER, 22);
	m_lcSelHostList.InsertColumn(eHostName, m_szColum[eHostName], LVCFMT_LEFT  , 110);
	m_lcSelHostList.InsertColumn(eHostID  , m_szColum[eHostID  ], LVCFMT_LEFT  , 60);

	// 제일 마지막에 할것...
	m_lcSelHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcSelHostList.SetSortEnable(false);
//	m_lcSelHostList.SetFixedCol(eCheck);
}

void CDownloadGroupDlg::RefreshSelHostCnt()
{
	CString strSelHostCnt;
	strSelHostCnt.Format(_T("[ %s ]"), ToCurrency(m_lcSelHostList.GetItemCount()));
	GetDlgItem(IDC_TXT_SEL_HOST_CNT)->SetWindowText(strSelHostCnt);
}

void CDownloadGroupDlg::RefreshSelHostList()
{
	m_lcSelHostList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsSelInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo& info = m_lsSelInfoList.GetNext(pos);

		m_lcSelHostList.InsertItem(nRow, "");
		m_lcSelHostList.SetItemText(nRow, eHostName, info.hostName);
		m_lcSelHostList.SetItemText(nRow, eHostID , info.hostId );
		m_lcSelHostList.SetItemData(nRow, (DWORD_PTR)posOld);

		info.monitorType = 0;

		nRow++;
	}
}

void CDownloadGroupDlg::OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	AddSelHost(pNMLV->iItem);
}

void CDownloadGroupDlg::OnNMDblclkListSelHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	DelSelHost(pNMLV->iItem);
}

void CDownloadGroupDlg::OnBnClickedBnAdd()
{
	bool bChecked = false;
	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(m_lcHostList.GetCheck(i))
		{
			bChecked = true;
			break;
		}
	}

	if(!bChecked)
	{
		UbcMessageBox(LoadStringById(IDS_PLANSELECTHOST_MSG001));
		return;
	}

	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(!m_lcHostList.GetCheck(i)) continue;

		AddSelHost(i);

		m_lcHostList.SetCheck(i, FALSE);
	}

	m_lcHostList.SetCheckHdrState(FALSE);

	RefreshSelHostCnt();
}

void CDownloadGroupDlg::OnBnClickedBnDel()
{
	for(int i=m_lcSelHostList.GetItemCount()-1; i>=0 ;i--)
	{
		if(!m_lcSelHostList.GetCheck(i)) continue;
		DelSelHost(i);
	}

	m_lcSelHostList.SetCheckHdrState(FALSE);

	RefreshSelHostCnt();
}

void CDownloadGroupDlg::AddSelHost(int nHostIndex)
{
	if(nHostIndex < 0 || nHostIndex >= m_lcHostList.GetItemCount()) return;

	POSITION posHost = (POSITION)m_lcHostList.GetItemData(nHostIndex);
	if(!posHost) return;

	SHostInfo infoHost = m_lsInfoList.GetAt(posHost);

	bool bExist = false;
	for(int i=0; i<m_lcSelHostList.GetItemCount() ;i++)
	{
		POSITION posSel = (POSITION)m_lcSelHostList.GetItemData(i);
		if(!posSel) return;
		SHostInfo& infoSel = m_lsSelInfoList.GetAt(posSel);

		if( infoSel.hostId == infoHost.hostId )
		{
			bExist = true;
			break;
		}
	}

	if(bExist) return;

	SHostInfo infoNew;
	infoNew.mgrId            = infoHost.mgrId;
	infoNew.siteId           = infoHost.siteId;
	infoNew.hostId		     = infoHost.hostId;
	infoNew.hostName		 = infoHost.hostName;
	infoNew.monitorType	 = PROC_TYPE::PROC_NEW;


	POSITION posNew = m_lsSelInfoList.AddTail(infoNew);

	int nRow = m_lcSelHostList.GetItemCount();
	m_lcSelHostList.InsertItem(nRow, "");
	m_lcSelHostList.SetItemText(nRow, eHostName, infoHost.hostName);
	m_lcSelHostList.SetItemText(nRow, eHostID , infoHost.hostId);
	m_lcSelHostList.SetItemData(nRow, (DWORD_PTR)posNew);
}

void CDownloadGroupDlg::DelSelHost(int nSelHostIndex)
{
	if(nSelHostIndex < 0 || nSelHostIndex >= m_lcSelHostList.GetItemCount()) return;

	POSITION posSel = (POSITION)m_lcSelHostList.GetItemData(nSelHostIndex);
	if(!posSel) return;
	SHostInfo& infoSel = m_lsSelInfoList.GetAt(posSel);

	if( infoSel.monitorType== PROC_TYPE::PROC_NEW )
	{
		m_lsSelInfoList.RemoveAt(posSel);
	}
	else
	{
		infoSel.monitorType = PROC_TYPE::PROC_DELETE;
	}

	m_lcSelHostList.DeleteItem(nSelHostIndex);
}

BOOL CDownloadGroupDlg::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOSTNAME)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOSTID  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_SITENAME)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_CONTENTS)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_PACKAGE )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_TAG     )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_COMBO_FILTER_ADMIN  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_OPERATION        )->GetSafeHwnd() )
		{
			OnBnClickedButtonPlanRefresh();
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDownloadGroupDlg::OnBnClickedButtonFilterSite()
{
	CString strSiteName;
	CString strSiteId;

	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
	if(dlg.DoModal() == IDOK)
	{
		SiteInfoList siteInfoList;
		if(dlg.GetSelectSiteInfoList(siteInfoList))
		{
			POSITION pos = siteInfoList.GetHeadPosition();
			while(pos)
			{
				SSiteInfo info = siteInfoList.GetNext(pos);
				strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
				strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
			}
		}
	}

	SetDlgItemText(IDC_EDIT_FILTER_SITENAME, strSiteName);
	m_strSiteId = strSiteId;
}

void CDownloadGroupDlg::OnBnClickedButtonFilterContents()
{
	CContentsSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, _T(""));
		m_strContentsId = _T("");
		return;
	}

	CONTENTS_INFO_EX_LIST& list = dlg.GetSelectedContentsIdList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		CONTENTS_INFO_EX& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, info.strContentsName);
		m_strContentsId = info.strContentsId;
	}
}

void CDownloadGroupDlg::OnBnClickedButtonFilterPackage()
{
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		m_strPackageId = _T("");
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, info.szPackage);
		m_strPackageId = info.szPackage;
	}
}

void CDownloadGroupDlg::SaveFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	WritePrivateProfileString("HOST-FILTER", "HostName"         , strHostName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostID"           , strHostId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostType"         , strHostType     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteName"         , strSiteName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteID"           , strSiteId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContents"  , strContentsName , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContentsID", strContentsId   , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackage"   , strPackageName  , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackageID" , strPackageId    , szPath);
	WritePrivateProfileString("HOST-FILTER", "AdminStat"        , strAdminStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "OperaStat"        , strOperaStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "Category"         , strCategory     , szPath);
}

void CDownloadGroupDlg::LoadFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("HOST-FILTER", "HostName"         , "", buf, 1024, szPath); strHostName     = buf;
	GetPrivateProfileString("HOST-FILTER", "HostID"           , "", buf, 1024, szPath); strHostId       = buf;
	GetPrivateProfileString("HOST-FILTER", "HostType"         , "", buf, 1024, szPath); strHostType     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteName"         , "", buf, 1024, szPath); strSiteName     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteID"           , "", buf, 1024, szPath); strSiteId       = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContents"  , "", buf, 1024, szPath); strContentsName = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContentsID", "", buf, 1024, szPath); strContentsId   = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackage"   , "", buf, 1024, szPath); strPackageName  = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackageID" , "", buf, 1024, szPath); strPackageId    = buf;
	GetPrivateProfileString("HOST-FILTER", "AdminStat"        , "", buf, 1024, szPath); strAdminStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "OperaStat"        , "", buf, 1024, szPath); strOperaStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "Category"         , "", buf, 1024, szPath); strCategory     = buf;

	SetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	SetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	if(strHostType.IsEmpty())
	{
		m_cbHostType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strHostType.Tokenize(",", pos)) != _T(""))
		{
			m_cbHostType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}
	SetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	m_strSiteId = strSiteId;
	SetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	m_strContentsId = strContentsId;
	SetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageId;
	m_cbAdmin.SetCurSel(_ttoi(strAdminStat));
	m_cbOperation.SetCurSel(_ttoi(strOperaStat));
	//SetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.SetWindowText(strCategory);
}

void CDownloadGroupDlg::OnLvnColumnclickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
	RefreshHostCnt();
}

void CDownloadGroupDlg::OnNMClickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	RefreshHostCnt();
	*pResult = 0;
}

void CDownloadGroupDlg::RefreshHostCnt()
{
	int nTotal = m_lcHostList.GetItemCount();
	int nCheckCnt = 0;

	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(!m_lcHostList.GetCheck(i)) continue;
		nCheckCnt++;
	}

	CString strHostCnt;
	strHostCnt.Format(_T("[ %s / %s ]"), ToCurrency(nCheckCnt), ToCurrency(nTotal));
	GetDlgItem(IDC_TXT_HOST_CNT)->SetWindowText(strHostCnt);
}

TargetHostInfoList* CDownloadGroupDlg::GetTargetHostInfoList()
{
	return &m_TargetHostInfoList;
}


void CDownloadGroupDlg::OnCbnSelchangeCbDgroup()
{
	UpdateData(TRUE);
	CString strGroupName;
	m_cbDGroup.GetLBText(m_cbDGroup.GetCurSel(), strGroupName);

	m_strGroupNo = strGroupName.Mid(strlen("Group "));

	CString strWhere;
	strWhere.Format("addr1='%s'", m_strGroupNo);

	m_lsSelInfoList.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetHostForSelect(&aCall
							, _T("*")
							, _T("*")
							, strWhere
							);
	if(bRet)
	{
		CCopModule::GetObject()->GetHostData(&aCall, m_lsSelInfoList);
	}

	RefreshSelHostList();


//	UbcMessageBox(strGroupName);
}


void CDownloadGroupDlg::OnBnClickedOk() 
{
	if(m_strGroupNo.IsEmpty()) return;

	CWaitMessageBox wait;

	int nRow = 0;
	POSITION pos = m_lsSelInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo& info = m_lsSelInfoList.GetNext(pos);

		BOOL changed = FALSE;
		if(info.monitorType == PROC_TYPE::PROC_NEW)
		{
			// addr1 에 넣어야 하지만, HostInfo 에 addr1가 없으므로 할수없이 location 에 넣었다가 set 할때 addr1로 set함
			info.location = m_strGroupNo;
			changed = TRUE;
			nRow++;
		}else
		if(info.monitorType == PROC_TYPE::PROC_DELETE)
		{
			// addr1 에 넣어야 하지만, HostInfo 에 addr1가 없으므로 할수없이 location 에 넣었다가 set 할때 addr1로 set함
			info.location = "";
			changed = TRUE;
			m_lsSelInfoList.RemoveAt(posOld);
		}else{
			nRow++;
		}

		if(changed)
		{
			CDirtyFlag dirtyFlag;
			dirtyFlag.SetDirty("addr1", true);

			BOOL bRet = CCopModule::GetObject()->SetHost(info,GetEnvPtr()->m_szLoginID,&dirtyFlag);

			if(!bRet) {
				TraceLog(("%s host set(addr1=%s) failed", info.hostId, info.location));
			}else{
				TraceLog(("%s host set(addr1=%s) succeed", info.hostId, info.location));
			}
		}
	}

	CString msg;
	//msg.Format("%d 개 단말이 %s 그룹으로 설정되었습니다", nRow, m_strGroupNo);
	msg.Format(::LoadStringById(IDS_DOWNLOADGROUPDLG_MSG001), nRow, m_strGroupNo);

	UbcMessageBox(msg);

	//OnOK();
}

void CDownloadGroupDlg::OnBnClickedCancel() 
{
	OnCancel();
}
