#pragma once


// CTrashCanFrame frame

class CTrashCanFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CTrashCanFrame)
protected:
	CTrashCanFrame();           // protected constructor used by dynamic creation
	virtual ~CTrashCanFrame();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


