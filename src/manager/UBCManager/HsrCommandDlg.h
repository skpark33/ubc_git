#pragma once
#include "afxwin.h"

#include "ReposControl.h"

// CHsrCommandDlg 대화 상자입니다.

class CHsrCommandDlg : public CDialog
{
	DECLARE_DYNAMIC(CHsrCommandDlg)

public:
	CHsrCommandDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHsrCommandDlg();

	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HSR_COMMAND };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

public:

	CEdit	m_editServerIP;
	CEdit	m_editServerWebPort;
	CEdit	m_editServerCmdPort;
	CButton	m_btnConnect;

	CEdit	m_editResults;
	CComboBox	m_cbxCmdType;
	CEdit	m_editCommand;
	CButton	m_btnRun;
	CButton	m_btnClearResults;
	CStatic	m_stcCmdResults;

	CButton	m_btnCancel;

protected:

	CReposControl	m_repos;

	CFont	m_fontResults;

	CString	m_strServerIP;
	UINT	m_nServerWebPort;
	UINT	m_nServerCmdPort;
	CString	m_strCurrentPath;

	bool	m_bLogin;
	bool	m_enableRun ;

	void	EnableControls();

	bool	login(CString& strOutput);
	bool	runCommand(LPCSTR lpszCmd, CString& strOutput, unsigned long timeout=0);
	bool	runCLI(LPCSTR lpszCLI, CString& strOutput);

	void	_Run(unsigned long timeout);

public:
	void	EnableRun(bool enableRun);

	afx_msg void OnBnClickedButtonConnect();
	afx_msg void OnBnClickedButtonRunCommand();
	afx_msg void OnBnClickedButtonClearResults();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CButton m_btServerStop;
	CButton m_btServerStart;
	afx_msg void OnBnClickedButtonServerStop();
	afx_msg void OnBnClickedButtonServerStart();
};
