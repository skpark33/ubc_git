// HostPackageInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "HostPackageInfoDlg.h"


// CHostPackageInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHostPackageInfoDlg, CDialog)

CHostPackageInfoDlg::CHostPackageInfoDlg(SHostInfo* pInfo, CWnd* pParent /*=NULL*/)
	: CDialog(CHostPackageInfoDlg::IDD, pParent)
{
	m_HostInfo = pInfo;
}

CHostPackageInfoDlg::~CHostPackageInfoDlg()
{
}

void CHostPackageInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_CURSCHE1, m_edtPackageCur1);
	DDX_Text(pDX, IDC_EDIT_LASTALLOCTIME1, m_edtPackageTime1);
	DDX_Text(pDX, IDC_EDIT_LASTALLOCSCHE1, m_edtPackageLast1);
	DDX_Text(pDX, IDC_EDIT_AUTOSCHE1, m_edtPackageAuto1);
	DDX_Text(pDX, IDC_EDIT_CURSCHE2, m_edtPackageCur2);
	DDX_Text(pDX, IDC_EDIT_LASTALLOCTIME2, m_edtPackageTime2);
	DDX_Text(pDX, IDC_EDIT_LASTALLOCSCHE2, m_edtPackageLast2);
	DDX_Text(pDX, IDC_EDIT_AUTOSCHE2, m_edtPackageAuto2);
	DDX_Control(pDX, IDC_LIST_REVSCHE1, m_lscDisplay1);
	DDX_Control(pDX, IDC_LIST_REVSCHE2, m_lscDisplay2);

}


BEGIN_MESSAGE_MAP(CHostPackageInfoDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CHostPackageInfoDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CHostPackageInfoDlg 메시지 처리기입니다.

void CHostPackageInfoDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

BOOL CHostPackageInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_edtPackageCur1 = m_HostInfo->currentPackage1;

	if(m_HostInfo->lastPackageTime1.GetTime())
		m_edtPackageTime1 = m_HostInfo->lastPackageTime1.Format(STR_DEF_TIME);
	
	m_edtPackageLast1 = m_HostInfo->lastPackage1;
	m_edtPackageAuto1 = m_HostInfo->autoPackage1;
	m_edtPackageCur2 = m_HostInfo->currentPackage2;

	if(m_HostInfo->lastPackageTime2.GetTime())
		m_edtPackageTime2 = m_HostInfo->lastPackageTime2.Format(STR_DEF_TIME);;
	
	m_edtPackageLast2 = m_HostInfo->lastPackage2;
	m_edtPackageAuto2 = m_HostInfo->autoPackage2;

	UpdateData(FALSE);

	InitList();
	SetList();


	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CHostPackageInfoDlg::InitList()
{
	m_lscDisplay1.SetExtendedStyle(m_lscDisplay1.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_lscDisplay2.SetExtendedStyle(m_lscDisplay2.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_lscDisplay1.InsertColumn(0, LoadStringById(IDS_HOSTDETAILDLG_LST001), LVCFMT_CENTER, 100);
	m_lscDisplay1.InsertColumn(1, LoadStringById(IDS_HOSTDETAILDLG_LST002), LVCFMT_CENTER, 70);
	m_lscDisplay1.InsertColumn(2, LoadStringById(IDS_HOSTDETAILDLG_LST003), LVCFMT_CENTER, 70);

	m_lscDisplay2.InsertColumn(0, LoadStringById(IDS_HOSTDETAILDLG_LST001), LVCFMT_CENTER, 100);
	m_lscDisplay2.InsertColumn(1, LoadStringById(IDS_HOSTDETAILDLG_LST002), LVCFMT_CENTER, 70);
	m_lscDisplay2.InsertColumn(2, LoadStringById(IDS_HOSTDETAILDLG_LST003), LVCFMT_CENTER, 70);
}

void CHostPackageInfoDlg::SetList()
{
	// 0001329: 단말 상세화면에서 기존에 해당 단말에 걸린 예약을 보여주던 자리에, 해당 단말이 포함된 방송계획을 보여준다.
//	PackageRevInfoList lsPackageRev;
//	CCopModule::GetObject()->GetPackageRev(m_HostInfo->siteId, m_HostInfo->hostId, 2, lsPackageRev);

	CString strWhere;
	strWhere.Format(_T("bpId in (select bpId from ubc_th where targetHost like '%%=%s' and side = 1) order by startDate"), m_HostInfo->hostId);
	BroadcastingPlanInfoList list1;
	CCopModule::GetObject()->GetBroadcastingPlanList(_T("*"), list1, strWhere);

	strWhere.Format(_T("bpId in (select bpId from ubc_th where targetHost like '%%=%s' and side = 2) order by startDate"), m_HostInfo->hostId);
	BroadcastingPlanInfoList list2;
	CCopModule::GetObject()->GetBroadcastingPlanList(_T("*"), list2, strWhere);

	int nRow = 0;
	POSITION pos = list1.GetHeadPosition();
	while(pos)
	{
		SBroadcastingPlanInfo info = list1.GetNext(pos);

		m_lscDisplay1.InsertItem(nRow, info.strBpName);
		m_lscDisplay1.SetItemText(nRow, 1, info.tmStartDate.Format(STR_DEF_TIME));
		m_lscDisplay1.SetItemText(nRow, 2, info.tmEndDate.Format(STR_DEF_TIME));

		nRow++;
	}

	nRow = 0;
	pos = list2.GetHeadPosition();
	while(pos)
	{
		SBroadcastingPlanInfo info = list2.GetNext(pos);

		m_lscDisplay2.InsertItem(nRow, info.strBpName);
		m_lscDisplay2.SetItemText(nRow, 1, info.tmStartDate.Format(STR_DEF_TIME));
		m_lscDisplay2.SetItemText(nRow, 2, info.tmEndDate.Format(STR_DEF_TIME));

		nRow++;
	}

	m_lscDisplay1.SortItems(CompareSite, (DWORD_PTR)&m_lscDisplay1);
	m_lscDisplay2.SortItems(CompareSite, (DWORD_PTR)&m_lscDisplay2);
}

int CHostPackageInfoDlg::CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	LVFINDINFO lvFind;
	lvFind.flags = LVFI_PARAM;

	lvFind.lParam = lParam1;
	int nIndex1 = ((CUTBListCtrl*)lParam)->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = ((CUTBListCtrl*)lParam)->FindItem(&lvFind);

	int nCol = 1;
	CString strItem1 = ((CUTBListCtrl*)lParam)->GetItemText(nIndex1, nCol);
	CString strItem2 = ((CUTBListCtrl*)lParam)->GetItemText(nIndex2, nCol);
	return strItem1.Compare(strItem2);
}