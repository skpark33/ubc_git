// stdafx.cpp : source file that includes just the standard includes
// UBCManager.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "Tlhelp32.h"
#include "atlenc.h"
#include "common/libCommon/SecurityIni.h"

CString ToMoneyTypeString(LPCTSTR lpszStr)
{
	CString str = lpszStr;

	int nLength = str.Find(_T("."));
	if(nLength<0)
        nLength = str.GetLength();

	while( nLength > 3 )
	{
		nLength = nLength - 3;
		str.Insert( nLength, ',' );
	}

	return str;
}

CString ToMoneyTypeString(int value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%d"), value);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(ULONG ulvalue)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%ld"), ulvalue);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(ULONGLONG ulvalue)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%I64d"), ulvalue);
	return ToMoneyTypeString(buf);
}

CString ToString(int nVal)
{
	CString szBuf;
	szBuf.Format("%d", nVal);
	return szBuf;
}

CString ToString(unsigned long lVal)
{
	CString szBuf;
	szBuf.Format("%ld", lVal);
	return szBuf;
}

CString ToString(double dVal, int nDegree)
{
	CString strFormat;
	strFormat.Format(_T("%%.%df"), nDegree);

	CString szBuf;
	szBuf.Format(strFormat, dVal);
	return szBuf;
}

CString ToString(ULONGLONG ulValue)
{
	CString str;
	str.Format("%I64d", ulValue);

	return str;
}

COLORREF GetColorFromString(LPCTSTR lpszColor)
{
	COLORREF rgb;
	if(*lpszColor == '#')
		sscanf(lpszColor+1, "%x", &rgb);
	else
		sscanf(lpszColor, "%x", &rgb);

	COLORREF red	= rgb & 0x00FF0000;
	COLORREF green	= rgb & 0x0000FF00;
	COLORREF blue	= rgb & 0x000000FF;

	red = red >> 16;
	blue = blue << 16;

	return red | green | blue;
}

CString GetColorFromString(COLORREF rgb)
{
	COLORREF red	= rgb & 0x000000FF;
	COLORREF green	= rgb & 0x0000FF00;
	COLORREF blue	= rgb & 0x00FF0000;

	green = green >> 8;
	blue = blue >> 16;

	CString ret;
	ret.Format("#%02X%02X%02X", red, green, blue);

	return ret;
}

void GetColorRGB(COLORREF rgb, int& r, int& g, int& b)
{
	r = rgb & 0x000000FF;
	g = rgb & 0x0000FF00;
	b = rgb & 0x00FF0000;

	g >>= 8;
	b >>= 16;
}

LPCTSTR GetDataPath()
{
	static CString config_path = "";

	if(config_path.GetLength() == 0)
	{
//		CString path = ciEnv::newEnv("PROJECT_HOME");

//		config_path.Format("%s\\config\\%s.ini", path, ::GetHostName());
	}

	return config_path;
}

CString	FindExtension(const CString& strFileName)
{
	int nlen = strFileName.GetLength();
	int i;
	for (i = nlen-1; i >= 0; i--){
		if (strFileName[i] == '.'){
			return strFileName.Mid(i+1);
		}
	}
	return CString(_T(""));
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VARIANT를 문자열로 변환한다. \n
/// @param (VARIANT*) var : (in) VARIANT 변수
/// @return <형: CString> \n
///			<VARIANT 변수의 문자열> \n
/////////////////////////////////////////////////////////////////////////////////
CString	VariantToString(VARIANT* var)
{
	CString str;
	switch(var->vt)
	{
	case VT_BSTR:
		{
			return CString(var->bstrVal);
		}
		break;
	case VT_BSTR | VT_BYREF:
		{
			return CString(*var->pbstrVal);
		}
		break;
	case VT_I4:
		{
			str.Format(_T("%d"), var->lVal);
			return str;
		}
		break;
	case VT_I4 | VT_BYREF:
		{
			str.Format(_T("%d"), *var->plVal);
			return str;
		}
		break;
	case VT_R8:
		{
			str.Format(_T("%f"), var->dblVal);
			return str;
		}
		break;
	default:
		{
			return "";
		}
	}//switch
}


LPCTSTR GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UBCConnect.ini 파일의 Server IP 반환 \n
/// @return <형: CString> \n
///			<접속서버의 IP 주소> \n
/////////////////////////////////////////////////////////////////////////////////
CString	GetConnectServerIP()
{
	CString strIP = "";
	CString strIniPath;
	strIniPath.Format("%sdata\\%s", GetAppPath(), ORBCONN_INI);

	// todo 다음에 적용할것
	//CSecurityIni securityIni;
	//securityIni.ReadValue(_T("ORB_NAMESERVICE"), _T("IP"), strIniPath);

	char cBuffer[1024*100] = { 0x00 };
	GetPrivateProfileString("ORB_NAMESERVICE", "IP", "", cBuffer, 1024*100, strIniPath);

	return cBuffer;
}


bool CALLBACK FPC_EnumFontProc(LPLOGFONT lplf, LPTEXTMETRIC lptm, DWORD dwType, LPARAM lpData)	
{	
	if(strstr(lplf->lfFaceName, "@") != NULL)
		return TRUE;

	CStringArray* paryFontName = (CStringArray*)lpData;
	paryFontName->Add(lplf->lfFaceName);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시스템에 지정된 폰트가 있는지를 반환 \n
/// @param (CString) strFontName : (in) 검사하려는 폰트 이름
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsAvailableFont(CString strFontName)
{
	HWND hWnd = GetDesktopWindow();
	CStringArray aryFontName;
	EnumFonts(::GetDC(hWnd), 0,(FONTENUMPROC) FPC_EnumFontProc,(LPARAM)&aryFontName); //Enumerate font

	for(int i=0; i<aryFontName.GetSize(); i++)
	{
		if(strFontName == aryFontName.GetAt(i))
		{
			return true;
		}//if
	}//for

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UBCBrowser.ini 파일의 경로 반환 \n
/// @return <형: LPCTSTR> \n
///			<UBCBrowser.ini 파일의 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR	GetBRWConfigPath()
{
	static CString strBRWConfig = "";

	if(strBRWConfig.GetLength() == 0)
	{
		strBRWConfig.Format("%sdata\\UBCBrowser.ini", GetAppPath());
	}//if

	return strBRWConfig;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UBCVariables.ini 파일의 경로 반환 \n
/// @return <형: LPCTSTR> \n
///			<UBCVariables.ini 파일의 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR	GetVARConfigPath()
{
	static CString strVARConfig = "";

	if(strVARConfig.GetLength() == 0)
	{
		strVARConfig.Format("%sdata\\UBCVariables.ini", GetAppPath());
	}//if

	return strVARConfig;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용할 Vidoe render 종류를 반환 \n
/// @return <형: int> \n
///			<값: 1: Overlay, 2:VMR7,....> \n
/////////////////////////////////////////////////////////////////////////////////
int GetVidoeRenderType()
{
	int nType;
	char cBuffer[1024*100] = { 0x00 };
	CString strAppName, strIniPath;
	//strAppName = GetAppName();
	strIniPath = GetBRWConfigPath();

	GetPrivateProfileString("ROOT", "VIDEO_RENDER", "", cBuffer, 1024*100, GetBRWConfigPath());
	nType = atoi(cBuffer);

	if(nType == 0)
	{
		nType = 2;
		WritePrivateProfileString("ROOT", "VIDEO_RENDER", "2", GetBRWConfigPath());
	}//if

	return nType;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용할 Vidoe open하는 방법을 반환 \n
/// @return <형: int> \n
///			<값: 1: 초기화과정에 open, 2:매번 플레이시에 open> \n
/////////////////////////////////////////////////////////////////////////////////
int GetVidoeOpenType()
{
	int nType;
	char cBuffer[1024*100] = { 0x00 };
	CString strAppName, strIniPath;
	//strAppName = GetAppName();
	strIniPath = GetBRWConfigPath();

	GetPrivateProfileString("ROOT", "VIDEO_OPEN_TYPE", "", cBuffer, 1024*100, GetBRWConfigPath());
	nType = atoi(cBuffer);

	if(nType == 0)
	{
		nType = E_OPEN_INIT;
		WritePrivateProfileString("ROOT", "VIDEO_OPEN_TYPE", "1", GetBRWConfigPath());
	}//if

	return nType;
}

int IsUseHostRegFromFile(void)
{
	static int use_host_reg_from_file = -1;

	if( use_host_reg_from_file < 0 )
	{
		CString str_var_path = "";
		str_var_path.Format("%sdata\\%s", GetAppPath(), UBCVARS_INI);

		char buf[1024] = {0};
		GetPrivateProfileString("ROOT", "USE_HOST_REG_FROM_FILE", "0", buf, sizeof(buf), str_var_path);
		use_host_reg_from_file = atoi(buf);
	}

	return use_host_reg_from_file;
}

CTime NormalizeTime(CTime tmValue)
{
	return NormalizeTime(tmValue.GetHour(), tmValue.GetMinute());
}

CTime NormalizeTime(int nHour, int nMinute)
{
	CTime tmCurTime = CTime::GetCurrentTime();
	return CTime( tmCurTime.GetYear()
				, tmCurTime.GetMonth()
				, tmCurTime.GetDay()
				, 0, 0, 0 )
				+ CTimeSpan(0, nHour, nMinute, 0);
}

CString GetContentsTypeString(int nContentsType)
{
	switch(nContentsType)
	{
	case 0: return "Video";
	case 1:	return "Ticker";
	case 2:	return "Image";
	case 3:	return "Promotion";
	case 4:	return "TV";
	case 5:	return "Ticker";
	case 6:	return "Phone";
	case 7:	return "WebPage";
	case 8:	return "Flash";
	case 9:	return "WebCam";
	case 10:return "RSS";
	case 11:return "Clock";
	case 12:return "Text";
	case 13:return "Flash";
	case 14:return "Flash";
	case 15:return "Typing";
	case 16:return "PPT";
	case 17:return "Etc";
	case 18:return "Wizard";
	}

	return "";
}

CString ToFileSize(ULONGLONG lVal, char chUnit, bool bIncUnit /*= true*/)
{
	CString strVal;
	CString strUnit;

	if(chUnit == 'A' || chUnit == 'a')
	{
		if     (lVal / 1024 == 0) chUnit = 'B';
		else if(lVal / (1024*1024) == 0) chUnit = 'K';
		else if(lVal / (1024*1024*1024) == 0) chUnit = 'M';
		else chUnit = 'G';
	}

	if(chUnit == 'B' || chUnit == 'b')
	{
		strVal = ToString(lVal);
		if(bIncUnit) strUnit = _T("Byte");
	}
	else if(chUnit == 'K' || chUnit == 'k')
	{
		strVal = ToString(lVal/1024, 1);
		if(bIncUnit) strUnit = _T("KByte");
	}
	else if(chUnit == 'M' || chUnit == 'm')
	{
		strVal = ToString(lVal/(1024*1024.), 1);
		if(bIncUnit) strUnit = _T("MByte");
	}
	else if(chUnit == 'G' || chUnit == 'g')
	{
		strVal = ToString(lVal/(1024*1024*1024.), 1);
		if(bIncUnit) strUnit = _T("GByte");
	}

	return strVal + strUnit;
}

CString LoadStringById(UINT nID)
{
	CString strValue;
	if(!strValue.LoadString(nID)) return _T("");
	return strValue;
}

void TerminateProcess(CString processName)
{
	CArray<DWORD,DWORD> processIDs;

	if(IsAliveProcess(processName, &processIDs))  //살아있으면
	{
		for(int i=0; i < processIDs.GetSize(); i++)
		{
			DWORD processID = processIDs.GetAt(i);
			KillProcess(processID);
		}
	}
}

BOOL IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs)
{
	CString newName = "";

	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);

	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(PROCESSENTRY32);

	BOOL bIsAlive = FALSE;

	while(Process32Next(hSnapShot,&pEntry))
	{	
		// 1. 프로세스명 얻기
		CString newName = pEntry.szExeFile;

		newName.MakeUpper();
		processName.MakeUpper();

		// 2. 프로세스명 비교
		if( processName.Find(newName) >= 0 )
		{
			if(processIDs != NULL)
				processIDs->Add(pEntry.th32ProcessID);
			bIsAlive = TRUE;
		}
	}

	CloseHandle(hSnapShot);

	return bIsAlive;
}

void KillProcess(DWORD dwProcessId)
{
	HANDLE hProcessGUI = ::OpenProcess(PROCESS_TERMINATE, FALSE, dwProcessId);

	if( hProcessGUI ) 
	{
		::TerminateProcess(hProcessGUI, 0);
		//AppMonitor::SafeTerminateProcess(hProcessGUI, 0);
		::CloseHandle(hProcessGUI);
	}
}

void InitDateRange(CDateTimeCtrl& dtControl)
{
	CTime tmFrom = CTime(2000,1,1,0,0,0);
	CTime tmTo = CTime(2037,12,31,23,59,59);
	dtControl.SetRange(&tmFrom, &tmTo);
}

CString AsciiToBase64(CString strAscii)
{
	int nDestLen = Base64EncodeGetRequiredLength(strAscii.GetLength());

	CString strBase64;
	Base64Encode((const BYTE*)(LPCSTR)strAscii, strAscii.GetLength(),strBase64.GetBuffer(nDestLen), &nDestLen);
	strBase64.ReleaseBuffer(nDestLen);

	return strBase64;
}

CString Base64ToAscii(CString strBase64)
{
	int nDecLen = Base64DecodeGetRequiredLength(strBase64.GetLength());

	CString strAscii;
	Base64Decode(strBase64, strBase64.GetLength(), (BYTE*)strAscii.GetBuffer(nDecLen), &nDecLen);
	strAscii.ReleaseBuffer(nDecLen);
	return strAscii;
}

CString AsciiToBase64ForWeb(CString strAscii)
{
	CString strBase64 = AsciiToBase64(strAscii);

	strBase64.Replace(_T("="), _T("."));
	strBase64.Replace(_T("+"), _T("*"));
	strBase64.Replace(_T("/"), _T("?"));

	return strBase64;
}

CString Base64ToAsciiForWeb(CString strBase64)
{
	CString strAscii = Base64ToAscii(strBase64);

	strAscii.Replace(_T("."), _T("="));
	strAscii.Replace(_T("*"), _T("+"));
	strAscii.Replace(_T("?"), _T("/"));

	return strAscii;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 짧은 스크린샷주기 여부 \n
/// @return <형: int> \n
///			<값: 1: 사용, 0:미사용(기본)> \n
/////////////////////////////////////////////////////////////////////////////////
int GetEnableShortScreenshotPeriod()
{
	static int enable = -1;
	if( enable >= 0 ) return enable;

	char cBuffer[1024] = { 0x00 };
	CString strIniPath = GetVARConfigPath();

	GetPrivateProfileString("ROOT", "ENABLE_SHORT_SCREENSHOT_PERIOD", "0", cBuffer, 1023, strIniPath);
	enable = atoi(cBuffer);

	return enable;
}
