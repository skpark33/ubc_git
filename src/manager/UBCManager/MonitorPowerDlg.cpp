// MonitorPowerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MonitorPowerDlg.h"


// CMonitorPowerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMonitorPowerDlg, CDialog)

CMonitorPowerDlg::CMonitorPowerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMonitorPowerDlg::IDD, pParent)
	, m_nMonitorState(-1)
{

}

CMonitorPowerDlg::~CMonitorPowerDlg()
{
}

void CMonitorPowerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMonitorPowerDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_ON, &CMonitorPowerDlg::OnBnClickedButtonOn)
	ON_BN_CLICKED(IDC_BUTTON_OFF, &CMonitorPowerDlg::OnBnClickedButtonOff)
END_MESSAGE_MAP()


// CMonitorPowerDlg 메시지 처리기입니다.

void CMonitorPowerDlg::OnBnClickedButtonOn()
{
	m_nMonitorState = 1;
	OnOK();
}

void CMonitorPowerDlg::OnBnClickedButtonOff()
{
	m_nMonitorState = 0;
	OnOK();
}
