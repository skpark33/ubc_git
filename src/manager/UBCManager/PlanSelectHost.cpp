// PlanSelectHost.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "PlanSelectHost.h"

#include "UBCCopCommon/ContentsSelectDlg.h"
#include "UBCCopCommon/PackageSelectDlg.h"
#include "UBCCopCommon/SiteSelectDlg.h"

#include "common\UbcCode.h"
#include "InputBox.h"


// CPlanSelectHost 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPlanSelectHost, CDialog)

CPlanSelectHost::CPlanSelectHost(CWnd* pParent /*=NULL*/)
	: CSubBroadcastPlan(CPlanSelectHost::IDD, pParent)
{

}

CPlanSelectHost::~CPlanSelectHost()
{
}

void CPlanSelectHost::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOSTTYPE, m_cbHostType);
	DDX_Control(pDX, IDC_COMBO_FILTER_ADMIN, m_cbAdmin);
	DDX_Control(pDX, IDC_CB_OPERATION, m_cbOperation);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcHostList);
	DDX_Control(pDX, IDC_LIST_SEL_HOST, m_lcSelHostList);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDC_BUTTON_PLAN_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_COMBO_FILTER_TAG, m_cbFilterTag);
	DDX_Control(pDX, IDC_COMBO_FILTER_CACHE, m_cbFilterCache);
}


BEGIN_MESSAGE_MAP(CPlanSelectHost, CDialog)
	ON_BN_CLICKED(IDOK, &CPlanSelectHost::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPlanSelectHost::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_PLAN_REFRESH, &CPlanSelectHost::OnBnClickedButtonPlanRefresh)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOST, &CPlanSelectHost::OnNMDblclkListHost)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SEL_HOST, &CPlanSelectHost::OnNMDblclkListSelHost)
	ON_BN_CLICKED(IDC_BN_ADD, &CPlanSelectHost::OnBnClickedBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, &CPlanSelectHost::OnBnClickedBnDel)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_SITE, &CPlanSelectHost::OnBnClickedButtonFilterSite)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_CONTENTS, &CPlanSelectHost::OnBnClickedButtonFilterContents)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PACKAGE, &CPlanSelectHost::OnBnClickedButtonFilterPackage)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_HOST, &CPlanSelectHost::OnLvnColumnclickListHost)
	ON_NOTIFY(NM_CLICK, IDC_LIST_HOST, &CPlanSelectHost::OnNMClickListHost)
	ON_BN_CLICKED(IDC_BN_ADDTAG, &CPlanSelectHost::OnBnClickedBnAddtag)
	ON_BN_CLICKED(IDC_BN_DELTAG, &CPlanSelectHost::OnBnClickedBnDeltag)
END_MESSAGE_MAP()


void CPlanSelectHost::OnBnClickedOk() {}
void CPlanSelectHost::OnBnClickedCancel() {}
// CPlanSelectHost 메시지 처리기입니다.

bool CPlanSelectHost::IsModified()
{
	POSITION pos = GetTargetHostInfoList()->GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STargetHostInfo& info = GetTargetHostInfoList()->GetNext(pos);

		if( info.nProcType == PROC_NEW    ||
			info.nProcType == PROC_UPDATE ||
			info.nProcType == PROC_DELETE ) 
		{
			return true;
		}
	}

	return false;
}

void CPlanSelectHost::UpdateInfo()
{
}

bool CPlanSelectHost::InputErrorCheck(bool bMsg)
{
	if(m_lcSelHostList.GetItemCount() <= 0)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_PLANSELECTHOST_MSG001));
		return false;
	}

	return true;
}

void CPlanSelectHost::RefreshInfo()
{
//	RefreshHostList();
	RefreshHostCnt();
	RefreshSelHostList();
	RefreshSelHostCnt();
}

BOOL CPlanSelectHost::OnInitDialog()
{
	CSubBroadcastPlan::OnInitDialog();

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnAdd    .LoadBitmap(IDB_BUTTON_NEXT2  , RGB(255,255,255));
	m_bnDel    .LoadBitmap(IDB_BUTTON_PREV2  , RGB(255,255,255));

	m_bnRefresh.SetToolTipText(LoadStringById(IDS_PLANSELECTHOST_BTN001));

	CodeItemList listHostType;
	CUbcCode::GetInstance(GetEnvPtr()->m_strCustomer)->GetCategoryInfo(_T("HostType"), listHostType);

	POSITION pos = listHostType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listHostType.GetNext(pos);
		m_cbHostType.AddString(info.strEnumString, info.nEnumNumber);
	}

	m_cbAdmin.AddString(LoadStringById(IDS_PLANSELECTHOST_STR005));
	m_cbAdmin.AddString(LoadStringById(IDS_PLANSELECTHOST_STR001));
	m_cbAdmin.AddString(LoadStringById(IDS_PLANSELECTHOST_STR002));
	m_cbAdmin.SetCurSel(0);

	m_cbOperation.AddString(LoadStringById(IDS_PLANSELECTHOST_STR005));
	m_cbOperation.AddString(LoadStringById(IDS_PLANSELECTHOST_STR003));
	m_cbOperation.AddString(LoadStringById(IDS_PLANSELECTHOST_STR004));
	m_cbOperation.SetCurSel(0);

	m_cbFilterCache.AddString(LoadStringById(IDS_PLANSELECTHOST_STR005));
	m_cbFilterCache.AddString(LoadStringById(IDS_PLANSELECTHOST_STR009));
	m_cbFilterCache.AddString(LoadStringById(IDS_PLANSELECTHOST_STR010));
	m_cbFilterCache.SetCurSel(0);


	CStringArray aTagList;
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	LoadFilterData();

	UpdateData(FALSE);

	InitHostList();
	InitSelHostList();

	RefreshInfo();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPlanSelectHost::InitHostList()
{
	m_lcHostList.SetExtendedStyle(m_lcHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck  ] = _T("");
	m_szColum[eHostName] = LoadStringById(IDS_PLANSELECTHOST_LIST004);
	m_szColum[eHostID ] = LoadStringById(IDS_PLANSELECTHOST_LIST001);
	m_szColum[eDisplay] = LoadStringById(IDS_PLANSELECTHOST_LIST002);
	m_szColum[eGroup  ] = LoadStringById(IDS_PLANSELECTHOST_LIST003);
	m_szColum[eCategory] = LoadStringById(IDS_HOSTVIEW_LIST036);

	m_lcHostList.InsertColumn(eCheck  , m_szColum[eCheck  ], LVCFMT_CENTER,  22);
	m_lcHostList.InsertColumn(eHostName, m_szColum[eHostName], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eHostID , m_szColum[eHostID ], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eDisplay, m_szColum[eDisplay], LVCFMT_CENTER,  50);
	m_lcHostList.InsertColumn(eGroup  , m_szColum[eGroup  ], LVCFMT_LEFT  ,  85);
	m_lcHostList.InsertColumn(eCategory, m_szColum[eCategory], LVCFMT_LEFT  ,  85);

	// 제일 마지막에 할것...
	m_lcHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcHostList.SetSortEnable(false);
//	m_lcHostList.SetFixedCol(eCheck);
}

void CPlanSelectHost::RefreshHostList()
{
	CWaitMessageBox wait;

	CString szSite = (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite);

	UpdateData();

	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);

	CString strWhere;

	// 단말이름
	strHostName.Trim();
	if(!strHostName.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostName like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostName
							);
	}

	// 단말ID
	strHostId.Trim();
	if(!strHostId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostId like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostId
							);
	}

	// 단말타입
	if(!strHostType.IsEmpty())
	{
		strHostType.Replace(",", "','");
		strWhere.AppendFormat(_T(" %s hostType in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostType
							);
	}

	bool has_where_siteId_alreay = false;
	// 소속조직
	if(!strSiteId.IsEmpty() && strSiteId != "*")
	{
		strSiteId.Replace(_T(","), _T("','"));
		strWhere.AppendFormat(_T(" %s siteId in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strSiteId
							);
		has_where_siteId_alreay = true;

	}

	// 포함콘텐츠
	if(!strContentsId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 in (select programId from ubc_contents where lower(contentsId) = lower('%s'))"
								      " or lastSchedule2 in (select programId from ubc_contents where lower(contentsId) = lower('%s')))")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strContentsId
							, strContentsId
							);
	}

	// 방송중인 패키지
	if(!strPackageId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 = '%s' or lastSchedule2 = '%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strPackageId
							, strPackageId
							);
	}

	// 연결상태
	if(_ttoi(strOperaStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s operationalState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strOperaStat)==1 ? 1:0)
							);
	}

	// 사용상태
	if(_ttoi(strAdminStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s adminState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strAdminStat)==1 ? 1:0)
							);
	}

	// 검색어
	strCategory.Trim();
	if(!strCategory.IsEmpty())
	{
		int pos = 0;
		CString strTag;
		while((strTag = strCategory.Tokenize("/", pos)) != _T(""))
		{
			//strWhere.AppendFormat(_T(" %s concat('/', category, '/') like '%%/%s/%%'")
			strWhere.AppendFormat(_T(" %s category like '%%%s%%'")
							 	 , (strWhere.IsEmpty() ? _T("") : _T("and"))
								 , strTag.MakeLower()
								 );
		}
	}

	if(m_cbFilterCache.GetCurSel()==1) // localhost
	{
		strWhere.AppendFormat(_T(" %s domainName = 'localhost'")
							, (strWhere.IsEmpty() ? _T("") : _T("and")));
	}else if(m_cbFilterCache.GetCurSel()==2) {
		strWhere.AppendFormat(_T(" %s (domainName != 'localhost' or domainName is null)")
							, (strWhere.IsEmpty() ? _T("") : _T("and")));
	}

	m_lsInfoList.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetHostForSelect(&aCall
														, ((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority|| has_where_siteId_alreay == true) ? _T("*") : GetEnvPtr()->m_szSite)
														, _T("*")
														, strWhere
														);
	if(bRet)
	{
		CCopModule::GetObject()->GetHostData(&aCall, m_lsInfoList);
	}

	m_lcHostList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsInfoList.GetNext(pos);

		int nDisplayCnt = (info.displayCounter > 1 ? 3 : 1);
		for(int i=1; i<=nDisplayCnt ;i++)
		{
			CString strDisplay;

			if(i==1) strDisplay = LoadStringById(IDS_PLANSELECTHOST_STR006);
			if(i==2) strDisplay = LoadStringById(IDS_PLANSELECTHOST_STR007);
			if(i==3) strDisplay = LoadStringById(IDS_PLANSELECTHOST_STR008);

			m_lcHostList.InsertItem(nRow, "");
			m_lcHostList.SetItemText(nRow, eHostName, info.hostName);
			m_lcHostList.SetItemText(nRow, eHostID , info.hostId);
			m_lcHostList.SetItemText(nRow, eDisplay, strDisplay);
			m_lcHostList.SetItemText(nRow, eGroup  , info.siteId);
			m_lcHostList.SetItemText(nRow, eCategory, info.category);
			m_lcHostList.SetItemData(nRow, (DWORD_PTR)posOld);

			nRow++;
		}
	}

	RefreshHostCnt();
}

void CPlanSelectHost::OnBnClickedButtonPlanRefresh()
{
	RefreshHostList();
}

void CPlanSelectHost::InitSelHostList()
{
	m_lcSelHostList.SetExtendedStyle(m_lcSelHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lcSelHostList.InsertColumn(eCheck   , m_szColum[eCheck   ], LVCFMT_CENTER, 22);
	m_lcSelHostList.InsertColumn(eHostName, m_szColum[eHostName], LVCFMT_LEFT  , 80);
	m_lcSelHostList.InsertColumn(eHostID  , m_szColum[eHostID  ], LVCFMT_LEFT  , 60);
	m_lcSelHostList.InsertColumn(eDisplay , m_szColum[eDisplay ], LVCFMT_CENTER, 30);

	// 제일 마지막에 할것...
	m_lcSelHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcSelHostList.SetSortEnable(false);
//	m_lcSelHostList.SetFixedCol(eCheck);
}

void CPlanSelectHost::RefreshSelHostCnt()
{
	CString strSelHostCnt;
	strSelHostCnt.Format(_T("[ %s ]"), ToCurrency(m_lcSelHostList.GetItemCount()));
	GetDlgItem(IDC_TXT_SEL_HOST_CNT)->SetWindowText(strSelHostCnt);
}

void CPlanSelectHost::RefreshSelHostList()
{
	m_lcSelHostList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = GetTargetHostInfoList()->GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STargetHostInfo& info = GetTargetHostInfoList()->GetNext(pos);

		CString strDisplay;

		if(info.nSide==1) strDisplay = LoadStringById(IDS_PLANSELECTHOST_STR006);
		if(info.nSide==2) strDisplay = LoadStringById(IDS_PLANSELECTHOST_STR007);
		if(info.nSide==3) strDisplay = LoadStringById(IDS_PLANSELECTHOST_STR008);

		CString strHostId;
		int nPos = info.strTargetHost.ReverseFind('=');
		if(nPos >= 0)
		{
			strHostId = info.strTargetHost.Mid(nPos+1);
		}

		m_lcSelHostList.InsertItem(nRow, "");
		m_lcSelHostList.SetItemText(nRow, eHostName, info.strHostName);
		m_lcSelHostList.SetItemText(nRow, eHostID , strHostId );
		m_lcSelHostList.SetItemText(nRow, eDisplay, strDisplay);
		m_lcSelHostList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
}

void CPlanSelectHost::OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	AddSelHost(pNMLV->iItem);
}

void CPlanSelectHost::OnNMDblclkListSelHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	DelSelHost(pNMLV->iItem);
}

void CPlanSelectHost::OnBnClickedBnAdd()
{
	bool bChecked = false;
	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(m_lcHostList.GetCheck(i))
		{
			bChecked = true;
			break;
		}
	}

	if(!bChecked)
	{
		UbcMessageBox(LoadStringById(IDS_PLANSELECTHOST_MSG001));
		return;
	}

	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(!m_lcHostList.GetCheck(i)) continue;

		AddSelHost(i);

		m_lcHostList.SetCheck(i, FALSE);
	}

	m_lcHostList.SetCheckHdrState(FALSE);

	RefreshSelHostCnt();
}

void CPlanSelectHost::OnBnClickedBnDel()
{
	for(int i=m_lcSelHostList.GetItemCount()-1; i>=0 ;i--)
	{
		if(!m_lcSelHostList.GetCheck(i)) continue;
		DelSelHost(i);
	}

	m_lcSelHostList.SetCheckHdrState(FALSE);

	RefreshSelHostCnt();
}

void CPlanSelectHost::AddSelHost(int nHostIndex)
{
	if(nHostIndex < 0 || nHostIndex >= m_lcHostList.GetItemCount()) return;

	CString strDisplay = m_lcHostList.GetItemText(nHostIndex, eDisplay);
	POSITION posHost = (POSITION)m_lcHostList.GetItemData(nHostIndex);
	if(!posHost) return;
	SHostInfo infoHost = m_lsInfoList.GetAt(posHost);

	CString strTargetHost;
	strTargetHost.Format(_T("PM=*/Site=%s/Host=%s"), infoHost.siteId, infoHost.hostId);

	bool bExist = false;
	for(int i=0; i<m_lcSelHostList.GetItemCount() ;i++)
	{
		POSITION posSel = (POSITION)m_lcSelHostList.GetItemData(i);
		if(!posSel) return;
		STargetHostInfo& infoSel = GetTargetHostInfoList()->GetAt(posSel);

		if( infoSel.strTargetHost == strTargetHost )
		{
			bExist = true;
			break;
		}
	}

	if(bExist) return;

	STargetHostInfo infoNew;
	infoNew.nProcType	  = PROC_TYPE::PROC_NEW;
	infoNew.strSiteId     = GetBroadcastPlanInfo()->strSiteId;
	infoNew.strBpId       = GetBroadcastPlanInfo()->strBpId;
	infoNew.strThId       = infoHost.hostId;
	infoNew.strTargetHost = strTargetHost;
	infoNew.strHostName   = infoHost.hostName;

	if     (strDisplay == LoadStringById(IDS_PLANSELECTHOST_STR008)) infoNew.nSide = 3;
	else if(strDisplay == LoadStringById(IDS_PLANSELECTHOST_STR007)) infoNew.nSide = 2;
	else															 infoNew.nSide = 1;

	POSITION posNew = GetTargetHostInfoList()->AddTail(infoNew);

	int nRow = m_lcSelHostList.GetItemCount();
	m_lcSelHostList.InsertItem(nRow, "");
	m_lcSelHostList.SetItemText(nRow, eHostName, infoHost.hostName);
	m_lcSelHostList.SetItemText(nRow, eHostID , infoHost.hostId);
	m_lcSelHostList.SetItemText(nRow, eDisplay, strDisplay);
	m_lcSelHostList.SetItemData(nRow, (DWORD_PTR)posNew);
}

void CPlanSelectHost::DelSelHost(int nSelHostIndex)
{
	if(nSelHostIndex < 0 || nSelHostIndex >= m_lcSelHostList.GetItemCount()) return;

	POSITION posSel = (POSITION)m_lcSelHostList.GetItemData(nSelHostIndex);
	if(!posSel) return;
	STargetHostInfo& infoSel = GetTargetHostInfoList()->GetAt(posSel);

	if( infoSel.nProcType == PROC_TYPE::PROC_NEW )
	{
		GetTargetHostInfoList()->RemoveAt(posSel);
	}
	else
	{
		infoSel.nProcType = PROC_TYPE::PROC_DELETE;
	}

	m_lcSelHostList.DeleteItem(nSelHostIndex);
}

BOOL CPlanSelectHost::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOSTNAME)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOSTID  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_SITENAME)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_CONTENTS)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_PACKAGE )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_TAG     )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_COMBO_FILTER_ADMIN  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_CB_OPERATION        )->GetSafeHwnd() )
		{
			OnBnClickedButtonPlanRefresh();
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CPlanSelectHost::OnBnClickedButtonFilterSite()
{
	CString strSiteName;
	CString strSiteId;

	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
	if(dlg.DoModal() == IDOK)
	{
		SiteInfoList siteInfoList;
		if(dlg.GetSelectSiteInfoList(siteInfoList))
		{
			POSITION pos = siteInfoList.GetHeadPosition();
			while(pos)
			{
				SSiteInfo info = siteInfoList.GetNext(pos);
				strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
				strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
			}
		}
	}

	SetDlgItemText(IDC_EDIT_FILTER_SITENAME, strSiteName);
	m_strSiteId = strSiteId;
}

void CPlanSelectHost::OnBnClickedButtonFilterContents()
{
	CContentsSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, _T(""));
		m_strContentsId = _T("");
		return;
	}

	CONTENTS_INFO_EX_LIST& list = dlg.GetSelectedContentsIdList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		CONTENTS_INFO_EX& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, info.strContentsName);
		m_strContentsId = info.strContentsId;
	}
}

void CPlanSelectHost::OnBnClickedButtonFilterPackage()
{
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		m_strPackageId = _T("");
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, info.szPackage);
		m_strPackageId = info.szPackage;
	}
}

void CPlanSelectHost::SaveFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	WritePrivateProfileString("HOST-FILTER", "HostName"         , strHostName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostID"           , strHostId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostType"         , strHostType     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteName"         , strSiteName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteID"           , strSiteId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContents"  , strContentsName , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContentsID", strContentsId   , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackage"   , strPackageName  , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackageID" , strPackageId    , szPath);
	WritePrivateProfileString("HOST-FILTER", "AdminStat"        , strAdminStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "OperaStat"        , strOperaStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "Category"         , strCategory     , szPath);
}

void CPlanSelectHost::LoadFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("HOST-FILTER", "HostName"         , "", buf, 1024, szPath); strHostName     = buf;
	GetPrivateProfileString("HOST-FILTER", "HostID"           , "", buf, 1024, szPath); strHostId       = buf;
	GetPrivateProfileString("HOST-FILTER", "HostType"         , "", buf, 1024, szPath); strHostType     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteName"         , "", buf, 1024, szPath); strSiteName     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteID"           , "", buf, 1024, szPath); strSiteId       = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContents"  , "", buf, 1024, szPath); strContentsName = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContentsID", "", buf, 1024, szPath); strContentsId   = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackage"   , "", buf, 1024, szPath); strPackageName  = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackageID" , "", buf, 1024, szPath); strPackageId    = buf;
	GetPrivateProfileString("HOST-FILTER", "AdminStat"        , "", buf, 1024, szPath); strAdminStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "OperaStat"        , "", buf, 1024, szPath); strOperaStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "Category"         , "", buf, 1024, szPath); strCategory     = buf;

	SetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	SetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	if(strHostType.IsEmpty())
	{
		m_cbHostType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strHostType.Tokenize(",", pos)) != _T(""))
		{
			m_cbHostType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}
	SetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	m_strSiteId = strSiteId;
	SetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	m_strContentsId = strContentsId;
	SetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageId;
	m_cbAdmin.SetCurSel(_ttoi(strAdminStat));
	m_cbOperation.SetCurSel(_ttoi(strOperaStat));
	//SetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.SetWindowText(strCategory);
}

void CPlanSelectHost::OnLvnColumnclickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
	RefreshHostCnt();
}

void CPlanSelectHost::OnNMClickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	RefreshHostCnt();
	*pResult = 0;
}

void CPlanSelectHost::RefreshHostCnt()
{
	int nTotal = m_lcHostList.GetItemCount();
	int nCheckCnt = 0;

	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		if(!m_lcHostList.GetCheck(i)) continue;
		nCheckCnt++;
	}

	CString strHostCnt;
	strHostCnt.Format(_T("[ %s / %s ]"), ToCurrency(nCheckCnt), ToCurrency(nTotal));
	GetDlgItem(IDC_TXT_HOST_CNT)->SetWindowText(strHostCnt);
}

void CPlanSelectHost::OnBnClickedBnAddtag()
{
	int nSelCnt = 0;
	for(int nRow = m_lcHostList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcHostList.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lcHostList.GetItemData(nRow);
		if(!posHost) continue;

		nSelCnt++;
	}

	if(nSelCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CInputBox dlg;
	dlg.m_strTitle = LoadStringById(IDS_HOSTVIEW_STR010);

	if(dlg.DoModal() != IDOK) return;

	CWaitMessageBox wait;

	// 검색어
	CStringArray aTagList;

	int pos = 0;
	CString strTag;
	while((strTag = dlg.m_strInputText.Tokenize("/", pos)) != _T(""))
	{
		aTagList.Add(strTag);
	}

	for(int nRow = m_lcHostList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcHostList.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lcHostList.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsInfoList.GetAt(posHost);

		CString strLowerCategory = "/" + Info.category + "/";
		strLowerCategory.MakeLower();
		for(int i=0; i<aTagList.GetCount(); i++)
		{
			CString strLowerTag = aTagList[i];
			strLowerTag.MakeLower();
			if(strLowerCategory.Find( "/" + strLowerTag + "/" ) < 0)
			{
				Info.category += "/" + aTagList[i];
			}
		}

		Info.category.Trim("/");

		CCopModule::GetObject()->SetCategory(Info, GetEnvPtr()->m_szLoginID, Info.category);
	}

	m_cbFilterTag.GetWindowText(strTag);

	// 검색어 콤보 초기화
	aTagList.RemoveAll();
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_cbFilterTag.SetWindowText(strTag);

	RefreshHostList();
}

void CPlanSelectHost::OnBnClickedBnDeltag()
{
	int nSelCnt = 0;
	for(int nRow = m_lcHostList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcHostList.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lcHostList.GetItemData(nRow);
		if(!posHost) continue;

		nSelCnt++;
	}

	if(nSelCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CInputBox dlg;
	dlg.m_strTitle = LoadStringById(IDS_HOSTVIEW_STR011);

	if(dlg.DoModal() != IDOK) return;

	CWaitMessageBox wait;

	// 검색어
	CStringArray aTagList;

	int pos = 0;
	CString strTag;
	while((strTag = dlg.m_strInputText.Tokenize("/", pos)) != _T(""))
	{
		aTagList.Add(strTag);
	}

	for(int nRow = m_lcHostList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcHostList.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lcHostList.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsInfoList.GetAt(posHost);

		CString strLowerCategory = "/" + Info.category + "/";
		strLowerCategory.MakeLower();
		for(int i=0; i<aTagList.GetCount(); i++)
		{
			CString strLowerTag = aTagList[i];
			strLowerTag.MakeLower();

			int nFind = strLowerCategory.Find( "/" + strLowerTag + "/" );
			if(nFind >= 0)
			{
				Info.category.Delete(nFind, strLowerTag.GetLength() + 1);
				strLowerCategory.Delete(nFind, strLowerTag.GetLength() + 1);
			}
		}

		Info.category.Trim("/");

		CCopModule::GetObject()->SetCategory(Info, GetEnvPtr()->m_szLoginID, Info.category);
	}

	m_cbFilterTag.GetWindowText(strTag);

	// 검색어 콤보 초기화
	aTagList.RemoveAll();
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_cbFilterTag.SetWindowText(strTag);

	RefreshHostList();
}
