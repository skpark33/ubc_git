#pragma once


// CPrimaryLogFrame frame

class CPrimaryLogFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CPrimaryLogFrame)
protected:
	CPrimaryLogFrame();           // protected constructor used by dynamic creation
	virtual ~CPrimaryLogFrame();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


