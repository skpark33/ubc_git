#pragma once

#include "resource.h"
#include "afxwin.h"
#include "common\utblistctrlex.h"
#include "common\libCLITransfer/CLITransfer.h"

// CRemoteCliCall 대화 상자입니다.

class CRemoteCliCall : public CDialog, CCLITransfer::IResultHandler
{
	DECLARE_DYNAMIC(CRemoteCliCall)

public:
	enum { eResult, eHostVNCStat, eMonitorStat, eHostType, 
		   eSiteName, eHostName, eHostID, eHostIP,
		   eHostEnd };
	enum { eMonitorOn, eMonitorOff, eMonitorUnknown, eVncOn, eVncOff };

	CRemoteCliCall(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRemoteCliCall();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_REMOTE_CLI_CALL_DLG };

	// input
	CList<SHostInfo> m_lsSelHost;
	bool			m_bIsPlaying;

private:
	CCLITransfer	m_CliTransfer;
	CImageList		m_ilHostList;
	CUTBListCtrlEx	m_lcList;
	CUTBListCtrlEx	m_lcQueryList;
	CString			m_szHostColum[eHostEnd];

	void InitList();
	BOOL RefreshHost(bool bCompMsg=true);
	void RefreshHostList();
	void UpdateListRow(int nRow, SHostInfo* pInfo);

	void CLIResultEvent(CCLITransfer::ITEM item);
	void CLIFinishEvent();

	bool DoServerQuery(CString& query, CString& errMsg);
	void RefreshQueryList();
	void InitQueryList();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBnFilesave();
};
