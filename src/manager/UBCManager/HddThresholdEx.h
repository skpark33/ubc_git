#pragma once

#include "resource.h"
#include "afxcmn.h"
#include "common\utblistctrlex.h"
#include "afxwin.h"

// CHddThresholdEx 대화 상자입니다.

class CHddThresholdEx : public CDialog
{
	DECLARE_DYNAMIC(CHddThresholdEx)

public:
	CHddThresholdEx(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHddThresholdEx();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HDD_THRESHOLD_EX };

	enum { eSite, ePackage, eUser, eTime, eDesc, eVolume
		 , eCategory, ePurpose, eHostType, eVertical, eResolution, ePublic, eVerify, eValidateDate
		 , eEnd };

	void InitPackageList();

	CStringArray m_aHostList;
	CStringArray m_aFailList;

private:
	CString			m_szColName[eEnd];
	CUTBListCtrlEx	m_lscPackage;
	int m_nSelect;			// 0=reserve, 1=immediate

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBnPackageSelect();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	int		IsUsed();
	BOOL	GetBroadcastPlan(CString strPackage, CString& strBpList);
	
	CEdit m_txtFilepathList;
	CButton allRadioBtn;
	CButton excludeRadioBtn;
	CButton validRadioBtn;
};
