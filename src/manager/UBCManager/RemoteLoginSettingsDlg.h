#pragma once
#include "afxwin.h"


// CRemoteLoginSettingsDlg 대화 상자입니다.

class CRemoteLoginSettingsDlg : public CDialog
{
	DECLARE_DYNAMIC(CRemoteLoginSettingsDlg)

public:
	CRemoteLoginSettingsDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRemoteLoginSettingsDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_REMOTE_LOGIN_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();

protected:
	virtual void OnOK();
	virtual void OnCancel();

public:
	CButton m_chkAutoScaling;
	CButton m_chkViewOnly;
	CComboBox m_cbxColorDepth;

protected:
	CString	m_strINIPath;

	void	InitControls();
	void	SetColorDepth(LPCSTR lpszColorDepth);
};
