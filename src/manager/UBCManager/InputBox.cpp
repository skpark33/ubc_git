// InputBox.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "InputBox.h"


// CInputBox 대화 상자입니다.

IMPLEMENT_DYNAMIC(CInputBox, CDialog)

CInputBox::CInputBox(CWnd* pParent /*=NULL*/)
	: CDialog(CInputBox::IDD, pParent)
	, m_strInputText(_T(""))
{

}

CInputBox::~CInputBox()
{
}

void CInputBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EB_TEXT, m_strInputText);
}


BEGIN_MESSAGE_MAP(CInputBox, CDialog)
	ON_BN_CLICKED(IDOK, &CInputBox::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CInputBox::OnBnClickedCancel)
END_MESSAGE_MAP()


// CInputBox 메시지 처리기입니다.

void CInputBox::OnBnClickedOk()
{
	UpdateData(TRUE);

	m_strInputText.Trim();
	if(m_strInputText.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_INPUTDLG_MSG001));
		return;
	}

	OnOK();
}

void CInputBox::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

BOOL CInputBox::OnInitDialog()
{
	CDialog::OnInitDialog();

	if(!m_strTitle.IsEmpty())
	{
		SetWindowText(m_strTitle);
	}

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
