// BroadcastPlanDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "BroadcastPlanDlg.h"
#include "common\ubcdefine.h"

#include "PlanScheduling.h"
#include "PlanPeriodSetting.h"
#include "PlanSelectHost.h"
#include "PlanSummary.h"
#include "BroadcastPlanNameInput.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "common/libProfileManager/ProfileManager.h"
#include "PlanVolumeConfirmDlg.h"

// CBroadcastPlanDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CBroadcastPlanDlg, CDialog)

CBroadcastPlanDlg::CBroadcastPlanDlg(PLAN_MODE mode, SBroadcastingPlanInfo* pInfo, CString strMaxZOrder, CWnd* pParent /*=NULL*/)
	: CDialog(CBroadcastPlanDlg::IDD, pParent)
	, m_nMode(mode)
	, m_pBroadcastingPlanInfo(pInfo)
	, m_strMaxZOrder(strMaxZOrder)
{
	ASSERT(pInfo);
}

CBroadcastPlanDlg::~CBroadcastPlanDlg()
{
}

void CBroadcastPlanDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_MAIN, m_tabMain);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_bnClose);
	DDX_Control(pDX, IDC_BUTTON_PREV, m_bnPrev);
	DDX_Control(pDX, IDC_BUTTON_NEXT, m_bnNext);
	DDX_Control(pDX, IDC_BUTTON_CREATE, m_bnCreate);
	DDX_Control(pDX, IDC_BUTTON_SAVE, m_bnSave);
	DDX_Control(pDX, IDC_BUTTON_SAVEAS, m_bnSaveAs);
	DDX_Control(pDX, IDC_BUTTON_TO_SERVER, m_bnFileTrans);
	DDX_Control(pDX, IDC_BUTTON_CAPACITY_CONFIRM, m_bnSizeConfirm);
}


BEGIN_MESSAGE_MAP(CBroadcastPlanDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CBroadcastPlanDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CBroadcastPlanDlg::OnBnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_PREV, &CBroadcastPlanDlg::OnBnClickedButtonPrev)
	ON_BN_CLICKED(IDC_BUTTON_NEXT, &CBroadcastPlanDlg::OnBnClickedButtonNext)
	ON_BN_CLICKED(IDC_BUTTON_CREATE, &CBroadcastPlanDlg::OnBnClickedButtonCreate)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CBroadcastPlanDlg::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_SAVEAS, &CBroadcastPlanDlg::OnBnClickedButtonSaveas)
	ON_NOTIFY(TCN_SELCHANGING, IDC_TAB_MAIN, &CBroadcastPlanDlg::OnTcnSelchangingTabMain)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_MAIN, &CBroadcastPlanDlg::OnTcnSelchangeTabMain)
	ON_BN_CLICKED(IDC_BUTTON_TO_SERVER, &CBroadcastPlanDlg::OnBnClickedButtonToServer)
	ON_BN_CLICKED(IDC_BUTTON_CAPACITY_CONFIRM, &CBroadcastPlanDlg::OnBnClickedButtonCapacityConfirm)
END_MESSAGE_MAP()


// CBroadcastPlanDlg 메시지 처리기입니다.

BOOL CBroadcastPlanDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString strModeName;
	if(m_nMode == eModeCreate) strModeName = LoadStringById(IDS_PLANDLG_STR001);
	if(m_nMode == eModeUpdate) strModeName = LoadStringById(IDS_PLANDLG_STR002);
	if(m_nMode == eModeReadonly) strModeName = LoadStringById(IDS_PLANDLG_STR002);

	CString strTitle;
	strTitle.Format(_T("%s%s")
				  , strModeName
				  , (m_nMode == eModeCreate ? _T("") : _T(" - ") + m_pBroadcastingPlanInfo->strBpName)
				  );

	m_pBroadcastingPlanInfo->strRegisterId = GetEnvPtr()->m_szLoginID;

	SetWindowText( strTitle );

	InitTimePlanInfoList();
	InitTargetHostInfoList();

	InitTab();
	InitButton();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CBroadcastPlanDlg::DeleteAllSubWindow()
{
	for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
		if(pDlg) delete pDlg;
	}
	m_tabMain.DeleteAllItems();
}

bool CBroadcastPlanDlg::InitTimePlanInfoList()
{
	m_lsTimePlanList.RemoveAll();
	if(m_pBroadcastingPlanInfo->strBpId.IsEmpty()) return true;

	CCopModule::GetObject()->GetTimePlan((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
										, m_pBroadcastingPlanInfo->strBpId
										, m_lsTimePlanList
										);
	return TRUE;
}

bool CBroadcastPlanDlg::InitTargetHostInfoList()
{
	m_lsTargetHostList.RemoveAll();
	if(m_pBroadcastingPlanInfo->strBpId.IsEmpty()) return true;

	CCopModule::GetObject()->GetTargetHost((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
										  , m_pBroadcastingPlanInfo->strBpId
										  , m_lsTargetHostList
										  );
	return TRUE;
}

void CBroadcastPlanDlg::InitTab()
{
	m_tabMain.InitImageListHighColor(IDB_PLAN_TABLIST, RGB(192,192,192));

	if(m_nMode == eModeCreate)
	{
		m_nTabOrder[eScheduling   ] = 0;
		m_nTabOrder[ePeriodSetting] = 1;
		m_nTabOrder[eSelectHost   ] = 2;
		m_nTabOrder[eSummary      ] = 3;

		// 일정작성
		m_tabMain.InsertItem(0, LoadStringById(IDS_PLANDLG_STR003), eScheduling);
		m_tabMain.SetItemParam(0, (LPARAM)CreateSubWindow(eScheduling));

		// 적용 기간 설정
		m_tabMain.InsertItem(1, LoadStringById(IDS_PLANDLG_STR004), ePeriodSetting);
		m_tabMain.SetItemParam(1, (LPARAM)CreateSubWindow(ePeriodSetting));

		// 적용 단말 설정
		m_tabMain.InsertItem(2, LoadStringById(IDS_PLANDLG_STR005), eSelectHost);
		m_tabMain.SetItemParam(2, (LPARAM)CreateSubWindow(eSelectHost));

		// 방송계획 요약
		m_tabMain.InsertItem(3, LoadStringById(IDS_PLANDLG_STR006), eSummary);
		m_tabMain.SetItemParam(3, (LPARAM)CreateSubWindow(eSummary));
	}
	else
	{
		m_nTabOrder[eSummary      ] = 0;
		m_nTabOrder[eScheduling   ] = 1;
		m_nTabOrder[ePeriodSetting] = 2;
		m_nTabOrder[eSelectHost   ] = 3;

		// 방송계획 요약
		m_tabMain.InsertItem(0, LoadStringById(IDS_PLANDLG_STR006), eSummary);
		m_tabMain.SetItemParam(0, (LPARAM)CreateSubWindow(eSummary));
		
		// 일정작성
		m_tabMain.InsertItem(1, LoadStringById(IDS_PLANDLG_STR003), eScheduling);
		m_tabMain.SetItemParam(1, (LPARAM)CreateSubWindow(eScheduling));
		
		// 적용 기간 설정
		m_tabMain.InsertItem(2, LoadStringById(IDS_PLANDLG_STR004), ePeriodSetting);
		m_tabMain.SetItemParam(2, (LPARAM)CreateSubWindow(ePeriodSetting));

		// 적용 단말 설정
		m_tabMain.InsertItem(3, LoadStringById(IDS_PLANDLG_STR005), eSelectHost);
		m_tabMain.SetItemParam(3, (LPARAM)CreateSubWindow(eSelectHost));
	}

	ToggleTab(0);
}

CSubBroadcastPlan* CBroadcastPlanDlg::CreateSubWindow(int nID)
{
	CSubBroadcastPlan* pDlg = NULL;

	switch(nID)
	{
	case eScheduling :
			pDlg = (CSubBroadcastPlan*)new CPlanScheduling(this);
			pDlg->SetBroadcastPlanInfo(m_pBroadcastingPlanInfo);
			pDlg->SetTimePlanInfoList(&m_lsTimePlanList);
			pDlg->SetTargetHostInfoList(&m_lsTargetHostList);
			pDlg->Create(IDD_PLAN_SCHEDULING, this);
			break;
	case ePeriodSetting :
			pDlg = (CSubBroadcastPlan*)new CPlanPeriodSetting(this);
			pDlg->SetBroadcastPlanInfo(m_pBroadcastingPlanInfo);
			pDlg->SetTimePlanInfoList(&m_lsTimePlanList);
			pDlg->SetTargetHostInfoList(&m_lsTargetHostList);
			pDlg->Create(IDD_PLAN_PERIOD_SETTING, this);
			break;
	case eSelectHost :
			pDlg = (CSubBroadcastPlan*)new CPlanSelectHost(this);
			pDlg->SetBroadcastPlanInfo(m_pBroadcastingPlanInfo);
			pDlg->SetTimePlanInfoList(&m_lsTimePlanList);
			pDlg->SetTargetHostInfoList(&m_lsTargetHostList);
			pDlg->Create(IDD_PLAN_SELECT_HOST, this);
			break;
	case eSummary :
			pDlg = (CSubBroadcastPlan*)new CPlanSummary(this);
			pDlg->SetBroadcastPlanInfo(m_pBroadcastingPlanInfo);
			pDlg->SetTimePlanInfoList(&m_lsTimePlanList);
			pDlg->SetTargetHostInfoList(&m_lsTargetHostList);
			pDlg->Create(IDD_PLAN_SUMMARY, this);
			break;
	default: return NULL;
	}

	CRect rcTab, rcTabItem;
	m_tabMain.GetWindowRect(rcTab);
	ScreenToClient(rcTab);
	m_tabMain.GetItemRect(0, rcTabItem);

	rcTab.DeflateRect(3,rcTabItem.Height()+5,4,4);
	pDlg->SetWindowPos(NULL, rcTab.left, rcTab.top, rcTab.Width(), rcTab.Height(), SWP_HIDEWINDOW);

	return pDlg;
}

void CBroadcastPlanDlg::ToggleTab(int nIndex)
{
	m_tabMain.SetCurSel(nIndex);

	for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
		if(!pDlg) continue;
		
		if(pDlg->IsWindowVisible() && nIndex != i)
		{
			pDlg->UpdateInfo();
		}
	}

	for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
		if(!pDlg) continue;

		pDlg->ShowWindow(nIndex == i);
		pDlg->EnableWindow(nIndex == i);
	}

	if(nIndex == m_nTabOrder[eSummary])
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(nIndex);
		if(pDlg)
		{
			pDlg->RefreshInfo();
		}
	}
}

void CBroadcastPlanDlg::InitButton()
{
	m_bnClose    .LoadBitmap(IDB_BUTTON_CLOSE     , RGB(255,255,255));
	m_bnPrev     .LoadBitmap(IDB_BUTTON_PREV      , RGB(255,255,255));
	m_bnNext     .LoadBitmap(IDB_BUTTON_NEXT      , RGB(255,255,255));
	m_bnCreate   .LoadBitmap(IDB_BUTTON_CREATE    , RGB(255,255,255));
	m_bnSave     .LoadBitmap(IDB_BUTTON_SAVE      , RGB(255,255,255));
	m_bnSaveAs   .LoadBitmap(IDB_BUTTON_SAVEAS    , RGB(255,255,255));
	m_bnFileTrans.LoadBitmap(IDB_BUTTON_FILE_TRANS, RGB(255,255,255));
	m_bnSizeConfirm.LoadBitmap(IDB_BUTTON_SIZECONFIRM, RGB(255,255,255));

	m_bnPrev     .SetToolTipText(LoadStringById(IDS_PLANDLG_BTN002));
	m_bnNext     .SetToolTipText(LoadStringById(IDS_PLANDLG_BTN003));
	m_bnCreate   .SetToolTipText(LoadStringById(IDS_PLANDLG_BTN004));
	m_bnSave     .SetToolTipText(LoadStringById(IDS_PLANDLG_BTN005));
	m_bnSaveAs   .SetToolTipText(LoadStringById(IDS_PLANDLG_BTN006));
	m_bnClose    .SetToolTipText(LoadStringById(IDS_PLANDLG_BTN007));
	m_bnFileTrans.SetToolTipText(LoadStringById(IDS_PLANDLG_BTN008));
	m_bnSizeConfirm.SetToolTipText(LoadStringById(IDS_PLANDLG_BTN009));

	m_bnPrev  .ShowWindow(m_nMode == eModeCreate);
	m_bnNext  .ShowWindow(m_nMode == eModeCreate);
	m_bnCreate.ShowWindow(m_nMode == eModeCreate);
	m_bnSave  .ShowWindow(m_nMode == eModeUpdate);
	m_bnSaveAs.ShowWindow(m_nMode == eModeUpdate);

	InitButtonStat();
}

void CBroadcastPlanDlg::InitButtonStat()
{
	m_bnPrev  .EnableWindow(FALSE);
	m_bnNext  .EnableWindow(FALSE);
	m_bnCreate.EnableWindow(FALSE);
	m_bnSave  .EnableWindow(FALSE);
	m_bnSaveAs.EnableWindow(FALSE);
	m_bnFileTrans.EnableWindow(FALSE);
	m_bnSizeConfirm.EnableWindow(FALSE);

	if(!IsAuth(_T("BREG")) && !IsAuth(_T("BMOD"))) return;

	// 0001374: [RFP] 지점에서 상위조직에서 설정한 방송계획을 볼 수 있고, 우선순위를 조정할 수 있는 기능
	if(m_pBroadcastingPlanInfo->bReadOnly)
	{
		m_bnSaveAs.EnableWindow(TRUE);
		return;
	}

	if(m_nMode == eModeCreate)
	{
		m_bnPrev  .EnableWindow(m_tabMain.GetCurSel() > 0);
		m_bnNext  .EnableWindow((m_tabMain.GetCurSel()+1) < m_tabMain.GetItemCount());
		m_bnCreate.EnableWindow((m_tabMain.GetCurSel()+1) == m_tabMain.GetItemCount());
		m_bnSizeConfirm.EnableWindow((m_tabMain.GetCurSel()+1) == m_tabMain.GetItemCount());
	}
	else if(m_nMode == eModeUpdate)
	{
		m_bnSave  .EnableWindow(TRUE);
		m_bnSaveAs.EnableWindow(TRUE);
		m_bnSizeConfirm.EnableWindow(TRUE);
	}

	if(m_pBroadcastingPlanInfo)
	{
		m_bnFileTrans.EnableWindow(!m_pBroadcastingPlanInfo->strBpId.IsEmpty());
	}
}

void CBroadcastPlanDlg::OnBnClickedOk()
{
}

void CBroadcastPlanDlg::OnBnClickedButtonClose()
{
	OnCancel();
}

void CBroadcastPlanDlg::OnBnClickedButtonPrev()
{
	ToggleTab(m_tabMain.GetCurSel()-1);
	InitButtonStat();
}

void CBroadcastPlanDlg::OnBnClickedButtonNext()
{
	UpdateData(TRUE);

	CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(m_tabMain.GetCurSel());
	if(pDlg)
	{
		if(pDlg->InputErrorCheck(true))
		{
			pDlg->UpdateInfo();
			ToggleTab(m_tabMain.GetCurSel()+1);
			InitButtonStat();
		}
	}

	UpdateData(FALSE);
}

void CBroadcastPlanDlg::OnBnClickedButtonCreate()
{
	// 오류 체크
	for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
		if(!pDlg) continue;

		if(!pDlg->InputErrorCheck(true))
		{
			ToggleTab(i);
			return;
		}
	}

	if(!CapacityConfirm()) return;

	// 방송계획명 입력 화면 호출
	CBroadcastPlanNameInput dlg(CBroadcastPlanNameInput::eModeCreate);
	if(dlg.DoModal() != IDOK) return;

	m_pBroadcastingPlanInfo->strBpName = dlg.m_strBroadcastPlanName;

	for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
		if(!pDlg) continue;
		pDlg->UpdateInfo();
	}

	// 방송계획 생성
	if(!CCopModule::GetObject()->CreBroadcastingPlan(*m_pBroadcastingPlanInfo))
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG001));
		return;
	}

	bool bRollBack = false;

	// 2011.05.06 방송계획생성시에는 우선순위작업을 하지않도록 한다.
	//// 방송계획 우선순위 설정
	//if(!CCopModule::GetObject()->SetBroadcastingPlanZorder(m_pBroadcastingPlanInfo->strSiteId
	//													 , m_pBroadcastingPlanInfo->strBpId
	//													 , m_strMaxZOrder
	//													 , _T("")))
	//{
	//	bRollBack = true;
	//}

	if(!bRollBack && !SaveTimePlanInfo(&m_lsTimePlanList, m_pBroadcastingPlanInfo))
	{
		bRollBack = true;
	}

	if(!bRollBack && !SaveTargetHostInfo(&m_lsTargetHostList, m_pBroadcastingPlanInfo))
	{
		bRollBack = true;
	}

	// 일정계획 생성이 실패한 경우
	if(bRollBack)
	{
		CCopModule::GetObject()->DelBroadcastingPlan(m_pBroadcastingPlanInfo->strSiteId, m_pBroadcastingPlanInfo->strBpId);
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG001));
	}
	else if(!CCopModule::GetObject()->SetBroadcastingPlanState( m_pBroadcastingPlanInfo->strSiteId
															  , m_pBroadcastingPlanInfo->strBpId
															  , 1 ))
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG005));
	}
	else if(!SaveFileAndToServer(m_pBroadcastingPlanInfo->strSiteId, m_pBroadcastingPlanInfo->strBpId))
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG007));
		m_bnFileTrans.EnableWindow(!m_pBroadcastingPlanInfo->strBpId.IsEmpty());
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG002));
		OnOK();
	}
}

void CBroadcastPlanDlg::OnBnClickedButtonSave()
{
	// 오류 체크
	for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
		if(!pDlg) continue;

		if(!pDlg->InputErrorCheck(true))
		{
			ToggleTab(i);
			return;
		}
	}

	if(!CapacityConfirm()) return;

	for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
		if(!pDlg) continue;
		pDlg->UpdateInfo();
	}

	// 방송계획 파일의 백업본 생성
	BOOL backup_bp = BackupBroadcastingPlan(m_pBroadcastingPlanInfo->strSiteId, m_pBroadcastingPlanInfo->strBpId, m_pBroadcastingPlanInfo->strBpName);

	// 방송계획 저장
	if(!CCopModule::GetObject()->SetBroadcastingPlan(*m_pBroadcastingPlanInfo))
	{
		// 오류 ==> 방송계획 파일의 백업본 복원
		if(backup_bp) RestoreBroadcastingPlan(m_pBroadcastingPlanInfo->strSiteId, m_pBroadcastingPlanInfo->strBpId, m_pBroadcastingPlanInfo->strBpName);

		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG003));
		return;
	}

	bool bRollBack = false;

	if(!SaveTimePlanInfo(&m_lsTimePlanList, m_pBroadcastingPlanInfo))
	{
		bRollBack = true;
	}

	if(!bRollBack && !SaveTargetHostInfo(&m_lsTargetHostList, m_pBroadcastingPlanInfo))
	{
		bRollBack = true;
	}

	// 일정계획 생성이 실패한 경우
	if(bRollBack)
	{
		// 오류 ==> 방송계획 파일의 백업본 복원
		if(backup_bp) RestoreBroadcastingPlan(m_pBroadcastingPlanInfo->strSiteId, m_pBroadcastingPlanInfo->strBpId, m_pBroadcastingPlanInfo->strBpName);

		// todo 변경저장인 경우 롤백을 어떻게 처리할것인가
//		CCopModule::GetObject()->SetBroadcastingPlan(*m_pBroadcastingPlanInfo);
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG003));
	}
	else if(!CCopModule::GetObject()->SetBroadcastingPlanState( m_pBroadcastingPlanInfo->strSiteId
															  , m_pBroadcastingPlanInfo->strBpId
															  , 1 ))
	{
		// 오류 ==> 방송계획 파일의 백업본 복원
		if(backup_bp) RestoreBroadcastingPlan(m_pBroadcastingPlanInfo->strSiteId, m_pBroadcastingPlanInfo->strBpId, m_pBroadcastingPlanInfo->strBpName);

		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG005));
	}
	else if(!SaveFileAndToServer(m_pBroadcastingPlanInfo->strSiteId, m_pBroadcastingPlanInfo->strBpId))
	{
		// 오류 ==> 방송계획 파일의 백업본 복원
		if(backup_bp) RestoreBroadcastingPlan(m_pBroadcastingPlanInfo->strSiteId, m_pBroadcastingPlanInfo->strBpId, m_pBroadcastingPlanInfo->strBpName);

		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG007));
		m_bnFileTrans.EnableWindow(!m_pBroadcastingPlanInfo->strBpId.IsEmpty());
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG004));
		OnOK();
	}
}

// 방송계획 파일 저장
bool CBroadcastPlanDlg::SaveFileAndToServer(CString strSiteId, CString strBpId)
{
	SBroadcastingPlanInfo	stBroadcastingPlanInfo;
	TimePlanInfoList		lsTimePlanList;
	TargetHostInfoList		lsTargetHostList;

	if(!CCopModule::GetObject()->GetBroadcastingPlan(strSiteId
												   , strBpId
												   , stBroadcastingPlanInfo))
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG008));
		return false;
	}

	if(stBroadcastingPlanInfo.strBpId.IsEmpty()) return false;

	CWaitMessageBox wait;

	CString szFile, szLocal, szRemote;
	szRemote.Format("/config/");
	szLocal.Format("%s\\%s"
				, GetEnvPtr()->m_szDrive
				, UBC_CONFIG_PATH);

	//szFile.Format("%s_%s.bpi", GetEnvPtr()->m_szSite, stBroadcastingPlanInfo.strBpName);
	szFile.Format("%s_%s.bpi", strSiteId, stBroadcastingPlanInfo.strBpName); //skpark 2013.07.18 지금까지...로그인한 SiteId 로 저장되고 있었다니...

	CString strFullPath;
	strFullPath.Format(_T("%s\\%s%s")
				, GetEnvPtr()->m_szDrive
				, UBC_CONFIG_PATH
				, szFile);

	CProfileManager objIniManager(strFullPath);

	objIniManager.WriteProfileString(_T("BP"), "mgrId"       , stBroadcastingPlanInfo.strMgrId      );
	objIniManager.WriteProfileString(_T("BP"), "siteId"      , stBroadcastingPlanInfo.strSiteId     );
	objIniManager.WriteProfileString(_T("BP"), "bpId"        , stBroadcastingPlanInfo.strBpId       );
	objIniManager.WriteProfileString(_T("BP"), "bpName"      , stBroadcastingPlanInfo.strBpName     );
	objIniManager.WriteProfileString(_T("BP"), "createTime"  , stBroadcastingPlanInfo.tmCreateTime.Format(_T("%Y/%m/%d %H:%M:%S")));
	objIniManager.WriteProfileString(_T("BP"), "startDate"   , stBroadcastingPlanInfo.tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S")) );
	objIniManager.WriteProfileString(_T("BP"), "endDate"     , stBroadcastingPlanInfo.tmEndDate.Format(_T("%Y/%m/%d %H:%M:%S"))   );
	objIniManager.WriteProfileString(_T("BP"), "weekInfo"    , stBroadcastingPlanInfo.strWeekInfo   );
	objIniManager.WriteProfileString(_T("BP"), "exceptDay"   , stBroadcastingPlanInfo.strExceptDay  );
	objIniManager.WriteProfileString(_T("BP"), "description" , stBroadcastingPlanInfo.strDescription);
	objIniManager.WriteProfileString(_T("BP"), "zorder"      , stBroadcastingPlanInfo.strZOrder     );
	objIniManager.WriteProfileString(_T("BP"), "registerId"  , stBroadcastingPlanInfo.strRegisterId );
	objIniManager.WriteProfileString(_T("BP"), "downloadTime", stBroadcastingPlanInfo.tmDownloadTime.Format(_T("%Y/%m/%d %H:%M:%S")));
	objIniManager.WriteProfileString(_T("BP"), "retryCount"  , ToString(stBroadcastingPlanInfo.nRetryCount));
	objIniManager.WriteProfileString(_T("BP"), "retryGap"    , ToString(stBroadcastingPlanInfo.nRetryGap  ));

	CString strTpList;
	CString strThList;

	// 선택패키지 조회 및 파일 저장
	if(!CCopModule::GetObject()->GetTimePlan((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
											, stBroadcastingPlanInfo.strBpId
											, lsTimePlanList))
	{
		return FALSE;
	}

	int i=0;
	POSITION pos = lsTimePlanList.GetHeadPosition();
	while(pos)
	{
		STimePlanInfo& info = lsTimePlanList.GetNext(pos);
		if(info.nProcType == PROC_DELETE) continue;

		CString strSection;
		strSection.Format(_T("TP_%d"), i++);
		objIniManager.WriteProfileString(strSection, "mgrId"     , info.strMgrId    );
		objIniManager.WriteProfileString(strSection, "siteId"    , info.strSiteId   );
		objIniManager.WriteProfileString(strSection, "bpId"      , info.strBpId     );
		objIniManager.WriteProfileString(strSection, "tpId"      , info.strTpId     );
		objIniManager.WriteProfileString(strSection, "createTime", info.tmCreateTime.Format(_T("%Y/%m/%d %H:%M:%S")));
		objIniManager.WriteProfileString(strSection, "startTime" , info.strStartTime);
		objIniManager.WriteProfileString(strSection, "endTime"   , info.strEndTime  );
		objIniManager.WriteProfileString(strSection, "programId" , info.strProgramId);
		objIniManager.WriteProfileString(strSection, "zorder"    , info.strZOrder   );

		strTpList += strSection + ",";
	}

	strTpList.Trim(_T(","));
	objIniManager.WriteProfileString(_T("BP"), "TpList", strTpList);

	// 적용단말 조회 및 파일 저장
	if(!CCopModule::GetObject()->GetTargetHost((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
											  , stBroadcastingPlanInfo.strBpId
											  , lsTargetHostList
											  ))
	{
		return FALSE;
	}

	i=0;
	pos = lsTargetHostList.GetHeadPosition();
	while(pos)
	{
		STargetHostInfo& info = lsTargetHostList.GetNext(pos);
		if(info.nProcType == PROC_DELETE) continue;

		CString strSection;
		strSection.Format(_T("TH_%d"), i++);
		objIniManager.WriteProfileString(strSection, "mgrId"     , info.strMgrId     );
		objIniManager.WriteProfileString(strSection, "siteId"    , info.strSiteId    );
		objIniManager.WriteProfileString(strSection, "bpId"      , info.strBpId      );
		objIniManager.WriteProfileString(strSection, "thId"      , info.strThId      );
		objIniManager.WriteProfileString(strSection, "targetHost", info.strTargetHost);
		objIniManager.WriteProfileString(strSection, "side"      , ToString(info.nSide));

		strThList += strSection + ",";
	}

	strThList.Trim(_T(","));
	objIniManager.WriteProfileString(_T("BP"), "ThList", strThList);

	objIniManager.Write();

	// 파일전송
	return GetEnvPtr()->PutFile(szFile, szLocal, szRemote, GetEnvPtr()->m_szSite);
}

void CBroadcastPlanDlg::OnBnClickedButtonSaveas()
{
	// 오류 체크
	for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
		if(!pDlg) continue;

		if(!pDlg->InputErrorCheck(true))
		{
			ToggleTab(i);
			return;
		}
	}

	if(!CapacityConfirm()) return;

	// 방송계획명 입력 화면 호출
	CBroadcastPlanNameInput dlg(CBroadcastPlanNameInput::eModeCreate);
	if(dlg.DoModal() != IDOK) return;

	SBroadcastingPlanInfo stSaveAsInfo = m_pBroadcastingPlanInfo->Clone();
	stSaveAsInfo.strSiteId = GetEnvPtr()->m_szSite;
	stSaveAsInfo.strBpName = dlg.m_strBroadcastPlanName;

	for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	{
		CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
		if(!pDlg) continue;
		pDlg->UpdateInfo();
	}

	// 방송계획 생성
	if(!CCopModule::GetObject()->CreBroadcastingPlan(stSaveAsInfo))
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG005));
		return;
	}

	bool bRollBack = false;

	// 방송계획 우선순위 설정
	if(!CCopModule::GetObject()->SetBroadcastingPlanZorder(stSaveAsInfo.strSiteId
														 , stSaveAsInfo.strBpId
														 , m_strMaxZOrder
														 , _T("")))
	{
		bRollBack = true;
	}

	if(!bRollBack)
	{
		TimePlanInfoList lsSaveAsTimePlanList;
		POSITION pos = m_lsTimePlanList.GetHeadPosition();
		while(pos)
		{
			POSITION posOld = pos;
			STimePlanInfo& info = m_lsTimePlanList.GetNext(pos);

			if(info.nProcType == PROC_DELETE) continue;

			STimePlanInfo infoSaveAs = info.Clone();
			infoSaveAs.nProcType = PROC_TYPE::PROC_NEW;
			infoSaveAs.strSiteId = GetEnvPtr()->m_szSite;
			infoSaveAs.strBpId   = stSaveAsInfo.strBpId;

			lsSaveAsTimePlanList.AddTail(infoSaveAs);
		}

		if(!bRollBack && !SaveTimePlanInfo(&lsSaveAsTimePlanList, &stSaveAsInfo))
		{
			bRollBack = true;
		}

		TargetHostInfoList lsSaveAsTargetHostList;
		pos = m_lsTargetHostList.GetHeadPosition();
		while(pos)
		{
			POSITION posOld = pos;
			STargetHostInfo& info = m_lsTargetHostList.GetNext(pos);

			if(info.nProcType == PROC_DELETE) continue;

			STargetHostInfo infoSaveAs = info.Clone();
			infoSaveAs.nProcType = PROC_TYPE::PROC_NEW;
			infoSaveAs.strSiteId = GetEnvPtr()->m_szSite;
			infoSaveAs.strBpId   = stSaveAsInfo.strBpId;

			lsSaveAsTargetHostList.AddTail(infoSaveAs);
		}

		if(!bRollBack && !SaveTargetHostInfo(&lsSaveAsTargetHostList, &stSaveAsInfo))
		{
			bRollBack = true;
		}
	}

	// 일정계획 생성이 실패한 경우
	if(bRollBack)
	{
		CCopModule::GetObject()->DelBroadcastingPlan(stSaveAsInfo.strSiteId, stSaveAsInfo.strBpId);
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG005));
	}
	else if(!CCopModule::GetObject()->SetBroadcastingPlanState( stSaveAsInfo.strSiteId
															  , stSaveAsInfo.strBpId
															  , 1 ))
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG005));
	}
	else if(!SaveFileAndToServer(stSaveAsInfo.strSiteId, stSaveAsInfo.strBpId))
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG007));
		m_bnFileTrans.EnableWindow(!m_pBroadcastingPlanInfo->strBpId.IsEmpty());
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG006));
		OnOK();
	}
}

void CBroadcastPlanDlg::OnTcnSelchangingTabMain(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = (m_nMode == eModeCreate ? -1 : 0);
}

void CBroadcastPlanDlg::OnTcnSelchangeTabMain(NMHDR *pNMHDR, LRESULT *pResult)
{
	ToggleTab(m_tabMain.GetCurSel());
	InitButtonStat();
	*pResult = 0;
}

bool CBroadcastPlanDlg::SaveTimePlanInfo(TimePlanInfoList* pInfoList, SBroadcastingPlanInfo* pPlanInfo)
{
	POSITION pos = pInfoList->GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STimePlanInfo& info = pInfoList->GetNext(pos);

		if(info.nProcType == PROC_UNKNOWN) continue;

		if(info.nProcType == PROC_NEW)
		{
			info.strBpId = pPlanInfo->strBpId;
			if(!CCopModule::GetObject()->CreTimePlan(info)) return false;
		}
		else if(info.nProcType == PROC_UPDATE)
		{
			if(!CCopModule::GetObject()->SetTimePlan(info)) return false;
		}
		else if(info.nProcType == PROC_DELETE)
		{
			if( info.strSiteId.GetLength()==0 || 
				info.strBpId.GetLength()==0 || 
				info.strTpId.GetLength()==0 ) return false;
			if(!CCopModule::GetObject()->DelTimePlan(info.strSiteId, info.strBpId, info.strTpId)) return false;
		}
	}

	return true;
}

bool CBroadcastPlanDlg::SaveTargetHostInfo(TargetHostInfoList* pInfoList, SBroadcastingPlanInfo* pPlanInfo)
{
	POSITION pos = pInfoList->GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STargetHostInfo& info = pInfoList->GetNext(pos);

		if(info.nProcType == PROC_UNKNOWN) continue;

		if(info.nProcType == PROC_NEW)
		{
			info.strBpId = pPlanInfo->strBpId;
			if(!CCopModule::GetObject()->CreTargetHost(info)) return false;
		}
		else if(info.nProcType == PROC_UPDATE)
		{
			if(!CCopModule::GetObject()->SetTargetHost(info)) return false;
		}
		else if(info.nProcType == PROC_DELETE)
		{
			if( info.strSiteId.GetLength()==0 || 
				info.strBpId.GetLength()==0 || 
				info.strThId.GetLength()==0 ) return false;
			if(!CCopModule::GetObject()->DelTargetHost(info.strSiteId, info.strBpId, info.strThId)) return false;
		}
	}

	return true;
}

void CBroadcastPlanDlg::OnBnClickedButtonToServer()
{
	if(SaveFileAndToServer(m_pBroadcastingPlanInfo->strSiteId, m_pBroadcastingPlanInfo->strBpId))
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG010));
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG009));
	}
}

bool CBroadcastPlanDlg::CapacityConfirm()
{
	//for(int i=0; i<m_tabMain.GetItemCount() ;i++)
	//{
	//	CSubBroadcastPlan* pDlg = (CSubBroadcastPlan*)m_tabMain.GetItemParam(i);
	//	if(!pDlg) continue;
	//	pDlg->UpdateInfo();
	//}

	CPlanVolumeConfirmDlg dlg;

	POSITION pos = m_lsTimePlanList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STimePlanInfo& info = m_lsTimePlanList.GetNext(pos);

		if(info.nProcType == PROC_DELETE) continue;
		
		dlg.m_aPackageList.Add(info.strProgramId);
	}

	pos = m_lsTargetHostList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STargetHostInfo& info = m_lsTargetHostList.GetNext(pos);

		if(info.nProcType == PROC_DELETE) continue;

		int nFindIndex = info.strTargetHost.ReverseFind('=');
		CString strHostId = (nFindIndex < 0 ? info.strTargetHost : info.strTargetHost.Mid(nFindIndex+1));
		
		dlg.m_aHostList.Add(strHostId);
	}

	return (dlg.DoModal() == IDOK);
}

void CBroadcastPlanDlg::OnBnClickedButtonCapacityConfirm()
{
	if(CapacityConfirm())
	{
		UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG011));
	}
}

BOOL CBroadcastPlanDlg::BackupBroadcastingPlan(CString strSiteId, CString strBpId, CString strBpName)
{
	//SBroadcastingPlanInfo	stBroadcastingPlanInfo;
	//TimePlanInfoList		lsTimePlanList;
	//TargetHostInfoList		lsTargetHostList;

	//if(!CCopModule::GetObject()->GetBroadcastingPlan(strSiteId
	//											   , strBpId
	//											   , stBroadcastingPlanInfo))
	//{
	//	UbcMessageBox(LoadStringById(IDS_PLANDLG_MSG008));
	//	return false;
	//}

	//if(stBroadcastingPlanInfo.strBpId.IsEmpty()) return false;

	//CWaitMessageBox wait;

	CString szFile, szLocal, szRemote;
	szRemote.Format("/config/");
	//szLocal.Format("%s\\%s"
	//			, GetEnvPtr()->m_szDrive
	//			, UBC_CONFIG_PATH);

	//szFile.Format("%s_%s.bpi", GetEnvPtr()->m_szSite, stBroadcastingPlanInfo.strBpName);
	szFile.Format("%s_%s.bpi", strSiteId, strBpName/*stBroadcastingPlanInfo.strBpName*/); //skpark 2013.07.18 지금까지...로그인한 SiteId 로 저장되고 있었다니...

	CString szBakFile;
	szBakFile.Format("%s_%s.bpi.bak", strSiteId, strBpName/*stBroadcastingPlanInfo.strBpName*/);

	//CString strFullPath;
	//strFullPath.Format(_T("%s\\%s%s")
	//			, GetEnvPtr()->m_szDrive
	//			, UBC_CONFIG_PATH
	//			, szFile);

	//CProfileManager objIniManager(strFullPath);

	//objIniManager.WriteProfileString(_T("BP"), "mgrId"       , stBroadcastingPlanInfo.strMgrId      );
	//objIniManager.WriteProfileString(_T("BP"), "siteId"      , stBroadcastingPlanInfo.strSiteId     );
	//objIniManager.WriteProfileString(_T("BP"), "bpId"        , stBroadcastingPlanInfo.strBpId       );
	//objIniManager.WriteProfileString(_T("BP"), "bpName"      , stBroadcastingPlanInfo.strBpName     );
	//objIniManager.WriteProfileString(_T("BP"), "createTime"  , stBroadcastingPlanInfo.tmCreateTime.Format(_T("%Y/%m/%d %H:%M:%S")));
	//objIniManager.WriteProfileString(_T("BP"), "startDate"   , stBroadcastingPlanInfo.tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S")) );
	//objIniManager.WriteProfileString(_T("BP"), "endDate"     , stBroadcastingPlanInfo.tmEndDate.Format(_T("%Y/%m/%d %H:%M:%S"))   );
	//objIniManager.WriteProfileString(_T("BP"), "weekInfo"    , stBroadcastingPlanInfo.strWeekInfo   );
	//objIniManager.WriteProfileString(_T("BP"), "exceptDay"   , stBroadcastingPlanInfo.strExceptDay  );
	//objIniManager.WriteProfileString(_T("BP"), "description" , stBroadcastingPlanInfo.strDescription);
	//objIniManager.WriteProfileString(_T("BP"), "zorder"      , stBroadcastingPlanInfo.strZOrder     );
	//objIniManager.WriteProfileString(_T("BP"), "registerId"  , stBroadcastingPlanInfo.strRegisterId );
	//objIniManager.WriteProfileString(_T("BP"), "downloadTime", stBroadcastingPlanInfo.tmDownloadTime.Format(_T("%Y/%m/%d %H:%M:%S")));
	//objIniManager.WriteProfileString(_T("BP"), "retryCount"  , ToString(stBroadcastingPlanInfo.nRetryCount));
	//objIniManager.WriteProfileString(_T("BP"), "retryGap"    , ToString(stBroadcastingPlanInfo.nRetryGap  ));

	//CString strTpList;
	//CString strThList;

	//// 선택패키지 조회 및 파일 저장
	//if(!CCopModule::GetObject()->GetTimePlan((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
	//										, stBroadcastingPlanInfo.strBpId
	//										, lsTimePlanList))
	//{
	//	return FALSE;
	//}

	//int i=0;
	//POSITION pos = lsTimePlanList.GetHeadPosition();
	//while(pos)
	//{
	//	STimePlanInfo& info = lsTimePlanList.GetNext(pos);
	//	if(info.nProcType == PROC_DELETE) continue;

	//	CString strSection;
	//	strSection.Format(_T("TP_%d"), i++);
	//	objIniManager.WriteProfileString(strSection, "mgrId"     , info.strMgrId    );
	//	objIniManager.WriteProfileString(strSection, "siteId"    , info.strSiteId   );
	//	objIniManager.WriteProfileString(strSection, "bpId"      , info.strBpId     );
	//	objIniManager.WriteProfileString(strSection, "tpId"      , info.strTpId     );
	//	objIniManager.WriteProfileString(strSection, "createTime", info.tmCreateTime.Format(_T("%Y/%m/%d %H:%M:%S")));
	//	objIniManager.WriteProfileString(strSection, "startTime" , info.strStartTime);
	//	objIniManager.WriteProfileString(strSection, "endTime"   , info.strEndTime  );
	//	objIniManager.WriteProfileString(strSection, "programId" , info.strProgramId);
	//	objIniManager.WriteProfileString(strSection, "zorder"    , info.strZOrder   );

	//	strTpList += strSection + ",";
	//}

	//strTpList.Trim(_T(","));
	//objIniManager.WriteProfileString(_T("BP"), "TpList", strTpList);

	//// 적용단말 조회 및 파일 저장
	//if(!CCopModule::GetObject()->GetTargetHost((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
	//										  , stBroadcastingPlanInfo.strBpId
	//										  , lsTargetHostList
	//										  ))
	//{
	//	return FALSE;
	//}

	//i=0;
	//pos = lsTargetHostList.GetHeadPosition();
	//while(pos)
	//{
	//	STargetHostInfo& info = lsTargetHostList.GetNext(pos);
	//	if(info.nProcType == PROC_DELETE) continue;

	//	CString strSection;
	//	strSection.Format(_T("TH_%d"), i++);
	//	objIniManager.WriteProfileString(strSection, "mgrId"     , info.strMgrId     );
	//	objIniManager.WriteProfileString(strSection, "siteId"    , info.strSiteId    );
	//	objIniManager.WriteProfileString(strSection, "bpId"      , info.strBpId      );
	//	objIniManager.WriteProfileString(strSection, "thId"      , info.strThId      );
	//	objIniManager.WriteProfileString(strSection, "targetHost", info.strTargetHost);
	//	objIniManager.WriteProfileString(strSection, "side"      , ToString(info.nSide));

	//	strThList += strSection + ",";
	//}

	//strThList.Trim(_T(","));
	//objIniManager.WriteProfileString(_T("BP"), "ThList", strThList);

	//objIniManager.Write();

	// 파일전송
	GetEnvPtr()->DeleteFile(szBakFile, szRemote, GetEnvPtr()->m_szSite);
	return GetEnvPtr()->RenameFile(szFile, szBakFile, szRemote, GetEnvPtr()->m_szSite);
}

BOOL CBroadcastPlanDlg::RestoreBroadcastingPlan(CString strSiteId, CString strBpId, CString strBpName)
{
	CString szFile, szLocal, szRemote;
	szRemote.Format("/config/");
	//szLocal.Format("%s\\%s"
	//			, GetEnvPtr()->m_szDrive
	//			, UBC_CONFIG_PATH);

	//szFile.Format("%s_%s.bpi", GetEnvPtr()->m_szSite, stBroadcastingPlanInfo.strBpName);
	szFile.Format("%s_%s.bpi", strSiteId, strBpName/*stBroadcastingPlanInfo.strBpName*/); //skpark 2013.07.18 지금까지...로그인한 SiteId 로 저장되고 있었다니...

	CString szBakFile;
	szBakFile.Format("%s_%s.bpi.bak", strSiteId, strBpName/*stBroadcastingPlanInfo.strBpName*/);

	GetEnvPtr()->DeleteFile(szFile, szRemote, GetEnvPtr()->m_szSite);
	return GetEnvPtr()->RenameFile(szBakFile, szFile, szRemote, GetEnvPtr()->m_szSite);
}
