// PlanScheduling.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "PlanScheduling.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "UBCDailyPlanTableOCX.h"
#include "UBCCopCommon\UbcMenuAuth.h"

// 0000796: 콘텐츠 패키지 예약시 실행시간은 적어도 5분이상으로 설정되도록 한다
#define MIN_RUNNING_TIME 5


// CPlanScheduling 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPlanScheduling, CDialog)

CPlanScheduling::CPlanScheduling(CWnd* pParent /*=NULL*/)
	: CSubBroadcastPlan(CPlanScheduling::IDD, pParent)
{

}

CPlanScheduling::~CPlanScheduling()
{
}

void CPlanScheduling::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BN_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_LC_LIST, m_lcList);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDC_DP_DAILY_PLAN_TABLE, m_dpDailyPlanTable);
	DDX_Control(pDX, IDC_SP_RUNNINGTIME, m_spRunningTime);
	DDX_Control(pDX, IDC_BN_CIRCLE_SELECT, m_bnCircleSelect);
	DDX_Control(pDX, IDC_BN_ZORDER_UP, m_bnZorderUP);
	DDX_Control(pDX, IDC_BN_ZORDER_DN, m_bnZorderDown);
	DDX_Control(pDX, IDC_DT_STARTTIME, m_dtStart);
	DDX_Control(pDX, IDC_DT_ENDTIME, m_dtEnd);
}


BEGIN_MESSAGE_MAP(CPlanScheduling, CDialog)
	ON_BN_CLICKED(IDOK, &CPlanScheduling::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPlanScheduling::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_REFRESH, &CPlanScheduling::OnBnClickedBnRefresh)
	ON_NOTIFY(NM_DBLCLK, IDC_LC_LIST, &CPlanScheduling::OnNMDblclkLcList)
//	ON_NOTIFY(NM_CLICK, IDC_LC_LIST, &CPlanScheduling::OnNMClickLcList)
	ON_NOTIFY(NM_RCLICK, IDC_LC_LIST, &CPlanScheduling::OnNMRclickLcList)
	ON_COMMAND(ID_HOSTLIST_EDITPACKAGE, &CPlanScheduling::OnHostlistEditpackage)
	ON_BN_CLICKED(IDC_BN_ADD, &CPlanScheduling::OnBnClickedBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, &CPlanScheduling::OnBnClickedBnDel)
	ON_BN_CLICKED(IDC_BN_ZORDER_UP, &CPlanScheduling::OnBnClickedBnZorderUp)
	ON_BN_CLICKED(IDC_BN_CIRCLE_SELECT, &CPlanScheduling::OnBnClickedBnCircleSelect)
	ON_BN_CLICKED(IDC_BN_ZORDER_DN, &CPlanScheduling::OnBnClickedBnZorderDn)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SP_RUNNINGTIME, &CPlanScheduling::OnDeltaposSpRunningtime)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DT_ENDTIME, &CPlanScheduling::OnDtnDatetimechangeDtEndtime)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DT_STARTTIME, &CPlanScheduling::OnDtnDatetimechangeDtStarttime)
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CPlanScheduling, CSubBroadcastPlan)
	ON_EVENT(CPlanScheduling, IDC_DP_DAILY_PLAN_TABLE, 1, CPlanScheduling::SelChangeDpDailyPlanTable, VTS_I4)
	ON_EVENT(CPlanScheduling, IDC_DP_DAILY_PLAN_TABLE, 2, CPlanScheduling::InfoChangeDpDailyPlanTable, VTS_I4 VTS_DATE VTS_DATE)
END_EVENTSINK_MAP()

void CPlanScheduling::OnBnClickedOk() {}
void CPlanScheduling::OnBnClickedCancel() {}
// CPlanScheduling 메시지 처리기입니다.

bool CPlanScheduling::IsModified()
{
	POSITION pos = GetTimePlanInfoList()->GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STimePlanInfo& info = GetTimePlanInfoList()->GetNext(pos);

		if( info.nProcType == PROC_NEW    ||
			info.nProcType == PROC_UPDATE ||
			info.nProcType == PROC_DELETE ) 
		{
			return true;
		}
	}

	return false;
}

void CPlanScheduling::UpdateInfo()
{
	for(int i=0; i<m_dpDailyPlanTable.GetItemCount() ;i++)
	{
		ST_PLAN_ITEM stInfo;
		if(m_dpDailyPlanTable.GetItem(i, (LONG*)&stInfo))
		{
			POSITION pos = (POSITION)stInfo.lParam;

			STimePlanInfo& info = GetTimePlanInfoList()->GetAt(pos);
			if( info.nProcType == PROC_TYPE::PROC_DELETE ) continue;

//			if( _ttoi(info.strZOrder) == i ) continue;

			if( info.nProcType != PROC_TYPE::PROC_NEW )
			{
				info.nProcType = PROC_TYPE::PROC_UPDATE;
			}

			info.strStartTime = stInfo.tmStartTime.Format(_T("%H:%M:00"));
			info.strEndTime   = stInfo.tmEndTime  .Format(_T("%H:%M:00"));
			info.strZOrder    = ToString(i);
		}
	}
}

bool CPlanScheduling::InputErrorCheck(bool bMsg)
{
	if(m_dpDailyPlanTable.GetItemCount() <= 0)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_PLANSCHEDULING_MSG001));
		return false;
	}

	return true;
}

void CPlanScheduling::RefreshInfo()
{
//	RefreshList();
	InitDailyTimeTable();
}

BOOL CPlanScheduling::OnInitDialog()
{
	CSubBroadcastPlan::OnInitDialog();

	m_bnRefresh     .LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnAdd         .LoadBitmap(IDB_BUTTON_NEXT2  , RGB(255,255,255));
	m_bnDel         .LoadBitmap(IDB_BUTTON_PREV2  , RGB(255,255,255));
	m_bnCircleSelect.LoadBitmap(IDB_BUTTON_CIRCLE , RGB(255,255,255));
	m_bnZorderUP    .LoadBitmap(IDB_BUTTON_FRAMEUP, RGB(255,255,255));
	m_bnZorderDown  .LoadBitmap(IDB_BUTTON_FRAMEDN, RGB(255,255,255));

	m_bnRefresh     .SetToolTipText(LoadStringById(IDS_PLANSCHEDULING_BTN001));
	m_bnAdd         .SetToolTipText(LoadStringById(IDS_PLANSCHEDULING_BTN002));
	m_bnDel         .SetToolTipText(LoadStringById(IDS_PLANSCHEDULING_BTN003));
	m_bnCircleSelect.SetToolTipText(LoadStringById(IDS_PLANSCHEDULING_BTN004));
	m_bnZorderUP    .SetToolTipText(LoadStringById(IDS_PLANSCHEDULING_BTN005));
	m_bnZorderDown  .SetToolTipText(LoadStringById(IDS_PLANSCHEDULING_BTN006));

	m_spRunningTime.SetRange32(MIN_RUNNING_TIME, 60 * 24);
	m_spRunningTime.SetPos(MIN_RUNNING_TIME);
	UDACCEL accel[3] = { {0,1}, {2,10}, {4,60} };
	m_spRunningTime.SetAccel(3, accel);

	//m_dtStart.SetFormat(_T("HH:mm:ss"));
	//m_dtEnd  .SetFormat(_T("HH:mm:ss"));
	m_dtStart.SetFormat(_T("HH:mm"));
	m_dtEnd  .SetFormat(_T("HH:mm"));

	InitList();

	RefreshInfo();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPlanScheduling::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[ePackage] = LoadStringById(IDS_PLANSCHEDULING_LIST001);
	m_szColum[eGroup  ] = LoadStringById(IDS_PLANSCHEDULING_LIST002);
	m_szColum[eDesc   ] = LoadStringById(IDS_PLANSCHEDULING_LIST003);
	m_szColum[eVolume ] = LoadStringById(IDS_PLANSCHEDULING_LIST004);

	m_lcList.InsertColumn(ePackage, m_szColum[ePackage], LVCFMT_CENTER,120);
	m_lcList.InsertColumn(eGroup  , m_szColum[eGroup  ], LVCFMT_LEFT  , 80);
	m_lcList.InsertColumn(eDesc   , m_szColum[eDesc   ], LVCFMT_LEFT  , 50);
	m_lcList.InsertColumn(eVolume , m_szColum[eVolume ], LVCFMT_RIGHT , 60);

	// 제일 마지막에 할것...
	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);

	m_lcList.SetSortEnable(true);
}

void CPlanScheduling::RefreshList()
{
	CString strWhere;
	CString strTmp;

	CString strSiteID;
	GetDlgItemText(IDC_EB_SITEID, strSiteID);

	if(strSiteID.IsEmpty())
	{
		if(	CCopModule::eSiteAdmin != GetEnvPtr()->m_Authority && 
			CCopModule::eSiteManager != GetEnvPtr()->m_Authority)
		{
			strTmp.Format(_T("siteId like '%%%s%%'"), GetEnvPtr()->m_szSite);
			strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
		}
	}
	else
	{
		strTmp.Format(_T("siteId like '%%%s%%'"), strSiteID);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	CString strPackageName;
	GetDlgItemText(IDC_EB_PACKAGE_NAME, strPackageName);

	if(!strPackageName.IsEmpty())
	{
		strTmp.Format(_T("programId like '%%%s%%'"), strPackageName);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

#ifdef _DEBUG
//	UbcMessageBox(strWhere);
#endif

	m_lsPackage.RemoveAll();
	CCopModule::GetObject()->GetPackage( (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
										, m_lsPackage
										, strWhere
										);

	m_lcList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsPackage.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SPackageInfo info = m_lsPackage.GetNext(pos);

		m_lcList.InsertItem(nRow, info.szPackage);
		m_lcList.SetItemText(nRow, eGroup, info.szSiteID  );
		m_lcList.SetItemText(nRow, eDesc , info.szDescript);
		m_lcList.SetItemText(nRow, eVolume, (info.volume < 0 ? "" : ToFileSize((ULONGLONG)info.volume, 'G')));
		m_lcList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
}

void CPlanScheduling::OnBnClickedBnRefresh()
{
	RefreshList();
}

BOOL CPlanScheduling::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EB_SITEID       )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EB_PACKAGE_NAME)->GetSafeHwnd() )
		{
			OnBnClickedBnRefresh();
			return TRUE;
		}
	}

	return CSubBroadcastPlan::PreTranslateMessage(pMsg);
}

void CPlanScheduling::OnNMDblclkLcList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
	if(!pos) return;
	SPackageInfo info = m_lsPackage.GetAt(pos);

	RunStudio(info.szPackage);
}

void CPlanScheduling::RunStudio(CString strPackage)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szLocation[MAX_PATH];
	_tsplitpath(szModule, szDrive, szLocation, NULL, NULL);

	CString szPath;
	szPath.Format(_T("\"%s%s\""), szDrive, szLocation);

	CString szParam;
	szParam.Format(_T(" +siteid=%s +userid=%s +passwd=%s +package=%s")
				  , CEnviroment::GetObject()->m_szSite
				  , CEnviroment::GetObject()->m_szLoginID
				  , CEnviroment::GetObject()->m_szPassword
				  , strPackage
				  );

	HINSTANCE nRet = ShellExecute(NULL, NULL, UBCSTUDIOEEEXE_STR, szParam, szPath, SW_SHOWNORMAL);
}

void CPlanScheduling::OnNMRclickLcList(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!IsAuth(_T("PMOD"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0) return;

	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CMenu* pPopup = menu.GetSubMenu(0);
	int nCnt = pPopup->GetMenuItemCount();
	for(int i=nCnt-1; i>=0 ; i--)
	{
		if(pPopup->GetMenuItemID(i) != ID_HOSTLIST_EDITPACKAGE)
		{
			pPopup->DeleteMenu(i, MF_BYPOSITION);
		}
	}

	CPoint pt;
	GetCursorPos(&pt);

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CPlanScheduling::OnHostlistEditpackage()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lcList.GetItemData(m_ptSelList.y);
	if(!pos) return;
	SPackageInfo info = m_lsPackage.GetAt(pos);

	RunStudio(info.szPackage);
}

void CPlanScheduling::OnBnClickedBnAdd()
{
	POSITION posSel = m_lcList.GetFirstSelectedItemPosition();
	if(!posSel)
	{
		UbcMessageBox(LoadStringById(IDS_PLANSCHEDULING_MSG002));
		return;
	}

	CTime tmStartTime = NormalizeTime(9,0);
	CTime tmEndTime   = tmStartTime + CTimeSpan(0, 2, 0, 0);

	POSITION pos = GetTimePlanInfoList()->GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STimePlanInfo& info = GetTimePlanInfoList()->GetNext(pos);

		if( info.nProcType == PROC_DELETE ) continue;

		if( ( info.strStartTime <= tmStartTime.Format(_T("%H:%M")) && info.strEndTime >  tmStartTime.Format(_T("%H:%M")) ) ||
			( info.strStartTime <  tmEndTime.Format(_T("%H:%M"))   && info.strEndTime >= tmEndTime.Format(_T("%H:%M"))   ) )
		{
			tmStartTime = NormalizeTime(_ttoi(info.strEndTime.Left(2)), _ttoi(info.strEndTime.Mid(3,2)));
			tmEndTime   = tmStartTime + CTimeSpan(0, 2, 0, 0);
		}
	}

	int nItem = m_lcList.GetNextSelectedItem(posSel);

	POSITION posItem = (POSITION)m_lcList.GetItemData(nItem);
	if(!posItem) return;
	SPackageInfo info = m_lsPackage.GetAt(posItem);

	STimePlanInfo infoNew;
	infoNew.nProcType	 = PROC_TYPE::PROC_NEW;
	infoNew.strSiteId    = GetBroadcastPlanInfo()->strSiteId;
	infoNew.strBpId      = GetBroadcastPlanInfo()->strBpId;
	infoNew.strStartTime = tmStartTime.Format(_T("%H:%M:00"));
	infoNew.strEndTime   = tmEndTime  .Format(_T("%H:%M:00"));
	infoNew.strProgramId = info.szPackage;
	infoNew.strZOrder    = _T("");

	POSITION posNew = GetTimePlanInfoList()->AddTail(infoNew);

	int nIndex = m_dpDailyPlanTable.AddItem(info.szPackage
										 , NormalizeTime(tmStartTime).GetTime()
										 , NormalizeTime(tmEndTime  ).GetTime()
										 , (LONG*)posNew
										 );

	m_dpDailyPlanTable.SetSelectedIndex(nIndex);
	InitTimeInfo(nIndex);
}

void CPlanScheduling::OnBnClickedBnDel()
{
	int nSelIndex = m_dpDailyPlanTable.GetSelectedIndex();
	
	if(nSelIndex < 0) return;

	ST_PLAN_ITEM stInfo;
	if(m_dpDailyPlanTable.GetItem(nSelIndex, (LONG*)&stInfo))
	{
		POSITION pos = (POSITION)stInfo.lParam;

		STimePlanInfo& info = GetTimePlanInfoList()->GetAt(pos);
		if( info.nProcType == PROC_TYPE::PROC_NEW )
		{
			GetTimePlanInfoList()->RemoveAt(pos);
		}
		else
		{
			info.nProcType = PROC_TYPE::PROC_DELETE;
		}
	}

	m_dpDailyPlanTable.DeleteItem(nSelIndex);
}

void CPlanScheduling::SelChangeDpDailyPlanTable(long nIndex)
{
	InitTimeInfo(nIndex);
}

void CPlanScheduling::InfoChangeDpDailyPlanTable(long nIndex, DATE tmStart, DATE tmEnd)
{
	ST_PLAN_ITEM stInfo;
	if(m_dpDailyPlanTable.GetItem(nIndex, (LONG*)&stInfo))
	{
		POSITION pos = (POSITION)stInfo.lParam;

		STimePlanInfo& info = GetTimePlanInfoList()->GetAt(pos);
		if( info.nProcType != PROC_TYPE::PROC_NEW )
		{
			info.nProcType = PROC_TYPE::PROC_UPDATE;
		}

		info.strStartTime = stInfo.tmStartTime.Format(_T("%H:%M:00"));
		info.strEndTime   = stInfo.tmEndTime  .Format(_T("%H:%M:00"));
	}

	InitTimeInfo(nIndex);
}

void CPlanScheduling::InitTimeInfo(long nIndex)
{
	m_dtStart.EnableWindow(nIndex >= 0);
	m_dtEnd  .EnableWindow(nIndex >= 0);

	m_spRunningTime.EnableWindow(nIndex >= 0);
	m_spRunningTime.SetPos(MIN_RUNNING_TIME);

	CTime tmStart = NormalizeTime(0,0);
	CTime tmEnd   = NormalizeTime(0,0);

	CString strPackage;

	if(nIndex >= 0)
	{
		ST_PLAN_ITEM stInfo;
		if(m_dpDailyPlanTable.GetItem(nIndex, (LONG*)&stInfo))
		{
			tmStart = stInfo.tmStartTime;
			tmEnd   = stInfo.tmEndTime  ;
			strPackage = stInfo.strText;
		}
	}

	m_dtStart.SetTime(&tmStart);
	m_dtEnd  .SetTime(&tmEnd  );

	InitDateRange(m_dtStart);
	InitDateRange(m_dtEnd);

	int nRunningTime = (tmEnd - tmStart).GetTotalMinutes();
	if(nRunningTime < 0) nRunningTime = (24*60)+nRunningTime;
	else if(nRunningTime < MIN_RUNNING_TIME) nRunningTime = MIN_RUNNING_TIME;

	m_spRunningTime.SetPos(nRunningTime);

	GetDlgItem(IDC_TXT_SEL_PACKAGE)->SetWindowText(strPackage);
}

void CPlanScheduling::OnBnClickedBnCircleSelect()
{
	if(m_dpDailyPlanTable.GetItemCount() <= 1) return;

	int nNewIndex = m_dpDailyPlanTable.GetSelectedIndex()-1;
	if(nNewIndex < 0) nNewIndex = m_dpDailyPlanTable.GetItemCount()-1;

	m_dpDailyPlanTable.SetSelectedIndex(nNewIndex);

	InitTimeInfo(nNewIndex);
}

void CPlanScheduling::OnBnClickedBnZorderUp()
{
	if(m_dpDailyPlanTable.GetItemCount() <= 1) return;

	int nIndex = m_dpDailyPlanTable.GetSelectedIndex();
	if(nIndex < 0) return;

	int nNewIndex = nIndex;
	if(nIndex+1 < m_dpDailyPlanTable.GetItemCount())
	{
		nNewIndex++;
	}

	if(nIndex != nNewIndex)
	{
		ST_PLAN_ITEM stInfo;
		if(m_dpDailyPlanTable.GetItem(nIndex, (LONG*)&stInfo))
		{
			POSITION pos = (POSITION)stInfo.lParam;

			STimePlanInfo& info = GetTimePlanInfoList()->GetAt(pos);
			if( info.nProcType != PROC_TYPE::PROC_NEW )
			{
				info.nProcType = PROC_TYPE::PROC_UPDATE;
			}
		}

		m_dpDailyPlanTable.SetZOrder(nIndex, nNewIndex);
		m_dpDailyPlanTable.SetSelectedIndex(nNewIndex);
	}
}

void CPlanScheduling::OnBnClickedBnZorderDn()
{
	if(m_dpDailyPlanTable.GetItemCount() <= 1) return;

	int nIndex = m_dpDailyPlanTable.GetSelectedIndex();
	if(nIndex < 0) return;

	int nNewIndex = nIndex;
	if(nIndex-1 >= 0)
	{
		nNewIndex--;
	}

	if(nIndex != nNewIndex)
	{
		ST_PLAN_ITEM stInfo;
		if(m_dpDailyPlanTable.GetItem(nIndex, (LONG*)&stInfo))
		{
			POSITION pos = (POSITION)stInfo.lParam;

			STimePlanInfo& info = GetTimePlanInfoList()->GetAt(pos);
			if( info.nProcType != PROC_TYPE::PROC_NEW )
			{
				info.nProcType = PROC_TYPE::PROC_UPDATE;
			}
		}

		m_dpDailyPlanTable.SetZOrder(nIndex, nNewIndex);
		m_dpDailyPlanTable.SetSelectedIndex(nNewIndex);
	}
}

void CPlanScheduling::OnDeltaposSpRunningtime(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	if(m_dpDailyPlanTable.GetSelectedIndex() < 0) return;

	int nRunningTime = pNMUpDown->iPos + pNMUpDown->iDelta;
	if(nRunningTime < MIN_RUNNING_TIME)
	{
		nRunningTime = MIN_RUNNING_TIME;
	}

	CTime tmStart;
	m_dtStart.GetTime(tmStart);

	CTime tmEnd = NormalizeTime(tmStart + CTimeSpan(0, 0, nRunningTime, 0));
	m_dpDailyPlanTable.SetItem(m_dpDailyPlanTable.GetSelectedIndex()
							 , FMT_END_DATE
							 , NULL
							 , 0
							 , tmEnd.GetTime()
							 , NULL);

//	m_spRunningTime.SetPos(nRunningTime);
	m_dtEnd.SetTime(&tmEnd);
}

void CPlanScheduling::OnDtnDatetimechangeDtStarttime(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	*pResult = 0;

	if(m_dpDailyPlanTable.GetSelectedIndex() < 0) return;

	CTime tmStart;
	m_dtStart.GetTime(tmStart);

	CTime tmEnd;
	m_dtEnd.GetTime(tmEnd);

	int nRunningTime = (tmEnd - tmStart).GetTotalMinutes();
	if(nRunningTime < 0) nRunningTime = (24*60)+nRunningTime;
	else if(nRunningTime < MIN_RUNNING_TIME)
	{
		nRunningTime = MIN_RUNNING_TIME;
		tmStart = NormalizeTime(tmEnd - CTimeSpan(0, 0, nRunningTime, 0));
		m_dtStart.SetTime(&tmStart);
	}

	m_dpDailyPlanTable.SetItem(m_dpDailyPlanTable.GetSelectedIndex()
							 , FMT_START_DATE
							 , NULL
							 , tmStart.GetTime()
							 , 0
							 , NULL);

	m_spRunningTime.SetPos(nRunningTime);
}

void CPlanScheduling::OnDtnDatetimechangeDtEndtime(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	*pResult = 0;

	if(m_dpDailyPlanTable.GetSelectedIndex() < 0) return;

	CTime tmStart;
	m_dtStart.GetTime(tmStart);

	CTime tmEnd;
	m_dtEnd.GetTime(tmEnd);

	int nRunningTime = (tmEnd - tmStart).GetTotalMinutes();
	if(nRunningTime < 0) nRunningTime = (24*60)+nRunningTime;
	else if(nRunningTime < MIN_RUNNING_TIME)
	{
		nRunningTime = MIN_RUNNING_TIME;
		tmEnd = NormalizeTime(tmStart + CTimeSpan(0, 0, nRunningTime, 0));
		m_dtEnd.SetTime(&tmEnd);
	}

	m_dpDailyPlanTable.SetItem(m_dpDailyPlanTable.GetSelectedIndex()
							 , FMT_END_DATE
							 , NULL
							 , 0
							 , tmEnd.GetTime()
							 , NULL);

	m_spRunningTime.SetPos(nRunningTime);
}

void CPlanScheduling::InitDailyTimeTable()
{
	m_dpDailyPlanTable.DeleteAllItem();

	POSITION pos = GetTimePlanInfoList()->GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STimePlanInfo& info = GetTimePlanInfoList()->GetNext(pos);

		if(info.nProcType == PROC_DELETE) continue;

		m_dpDailyPlanTable.AddItem(info.strProgramId
								 , NormalizeTime(_ttoi(info.strStartTime.Left(2)), _ttoi(info.strStartTime.Mid(3,2))).GetTime()
								 , NormalizeTime(_ttoi(info.strEndTime.Left(2)), _ttoi(info.strEndTime.Mid(3,2))).GetTime()
								 , (LONG*)posOld
								 );
	}

	if(GetTimePlanInfoList()->GetCount() > 0)
	{
		int nIndex = GetTimePlanInfoList()->GetCount()-1;
		m_dpDailyPlanTable.SetSelectedIndex(nIndex);
		InitTimeInfo(nIndex);
	}
	else
	{
		InitTimeInfo(-1);
	}
}