#pragma once
#include "afxcmn.h"
#include "afxdtctl.h"
#include "afxwin.h"
#include "enviroment.h"
#include "common/HoverButton.h"
#include "common/UTBListCtrl.h"

// CHollidayDlg dialog
class CHollidayDlg : public CDialog
{
	DECLARE_DYNAMIC(CHollidayDlg)

public:
	void			SetHolliday(LPCTSTR);
	LPCTSTR			GetHolliday(CString&);
	void			SetPoweron(LPCTSTR szPoweron);
	LPCTSTR			GetPoweron(CString&);


	BOOL			m_bShutdown;
	BOOL			m_bPoweron;
	CTime			m_tmShutdown;
	CTime			m_tmPoweron;

	bool			m_bMonitorOff;	// 모니터 절전 기능 관련
	std::list<std::string> m_listMonitorOffTime; // 모니터 절전 기능 관련

	CString			m_strWeekShutdownTime;

	void			ShowMonitor(bool p) { m_bShowMonitor = p;}
	void			ShowPoweron(bool p) { m_bShowPoweron = p;}

private:
	CString			m_strShutdownTime;
	CString			m_strPoweronTime;

	CButton			m_ckWeek[7];
	CUTBListCtrl	m_lscHoliday;
	CMonthCalCtrl	m_ctrCalendar;
	CHoverButton	m_bnAddHoliday;
	CHoverButton	m_bnDelHoliday;

	CDateTimeCtrl	m_dtShutdown;
	CDateTimeCtrl	m_dtPoweron;
	CButton			m_ckShutdown;
	CButton			m_ckPoweron;
	bool			m_bShowMonitor;
	bool			m_bShowPoweron;

	void			InitHolidayInfo();
	void			AddHolidayItem(CString str);

	// 모니터절전기능 관련
	CButton			m_ckUseMonitorOffTime;
	CDateTimeCtrl	m_dtMonitorOff;
	CDateTimeCtrl	m_dtMonitorOn;
	CHoverButton	m_bnAddMonitorOffTime;
	CHoverButton	m_bnDelMonitorOffTime;
	CUTBListCtrl	m_lscMonitorOffTime;
	void			InitMonitorOffTimeInfo();
	void			EnableMonitorOffInfo(BOOL bUseMonitorOff);

	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
public:
	CHollidayDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CHollidayDlg();

// Dialog Data
	enum { IDD = IDD_HOLLIDAY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedCheckShutdown();
	afx_msg void OnBnClickedCheckPoweron();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
	afx_msg void OnBnClickedKbUseMonitorOffTime();
	afx_msg void OnBnClickedBnAddMonitorOffTime();
	afx_msg void OnBnClickedBnDelMonitorOffTime();
	afx_msg void OnBnClickedBnPowerSetByDay();
	CStatic m_staticMonitorGroup;
	CButton m_btOK;
	CButton m_btCancel;
};
