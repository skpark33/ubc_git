// RemoteCliCall.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "RemoteCliCall.h"
#include "common\UbcCode.h"
#include "HostExcelSave.h"

#include <winsock2.h>
#include "common/libCLITransfer/CTSocket.h"
#include "common/libCLITransfer/CLITransfer.h"
#ifdef _DEBUG
#pragma comment(lib, "libCLITransfer_d.lib")
#else
#pragma comment(lib, "libCLITransfer.lib")
#endif

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)
#define COLOR_DOWNLOAD		RGB(0,128,255)		// ::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_FAIL			RGB(176,0,0)

// CRemoteCliCall 대화 상자입니다.

IMPLEMENT_DYNAMIC(CRemoteCliCall, CDialog)

CRemoteCliCall::CRemoteCliCall(CWnd* pParent /*=NULL*/)
	: CDialog(CRemoteCliCall::IDD, pParent)
	, m_CliTransfer(this)
	, m_bIsPlaying(false)

{

}

CRemoteCliCall::~CRemoteCliCall()
{
}

void CRemoteCliCall::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcList);
}


BEGIN_MESSAGE_MAP(CRemoteCliCall, CDialog)
	ON_BN_CLICKED(IDOK, &CRemoteCliCall::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CRemoteCliCall::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_FILESAVE, &CRemoteCliCall::OnBnClickedBnFilesave)
END_MESSAGE_MAP()


// CRemoteCliCall 메시지 처리기입니다.

BOOL CRemoteCliCall::OnInitDialog()
{
	CDialog::OnInitDialog();

	char buf[4096] = {0};
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_CB_COMMAND);
	for(int i=0; i<20 ;i++)
	{
		CString strKey;
		strKey.Format(_T("CMD%d"), i+1);
		GetPrivateProfileString("REMOTE_CLI_CALL", strKey, "", buf, 1024, szPath);
		
		CString strValue = buf;
		if(pCombo && !strValue.IsEmpty())
		{
			pCombo->AddString(strValue);
		}
	}

	if(pCombo && pCombo->GetCount() > 0)
	{
		pCombo->SetCurSel(0);
	}
	//SetDlgItemText(IDC_CB_COMMAND, buf);

	InitList();
	RefreshHostList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CRemoteCliCall::OnBnClickedOk()
{
	CString strCmd;
	GetDlgItemText(IDC_CB_COMMAND, strCmd);

	strCmd.Trim();

	if(strCmd.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_REMOTECLICALL_MSG001));
		return;
	}


	if(strCmd.GetLength() > 6 && strCmd.Mid(0,6) == "SQL://"){
		// 서버로의 query 에 해당한다.
		CString errMsg;
		InitQueryList();
		if(DoServerQuery(strCmd, errMsg)){
			RefreshQueryList();
		}else{
			UbcMessageBox(errMsg);		
		}
		return;
	}



	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_CB_COMMAND);
	BOOL bExist = FALSE;
	for(int i=0; i<pCombo->GetCount() ;i++)
	{
		CString strText;
		pCombo->GetLBText(i, strText);

		if(strCmd.CompareNoCase(strText) == 0)
		{
			bExist = TRUE;
			break;
		}
	}

	if(!bExist)
	{
		CString szPath;
		szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
		//WritePrivateProfileString("REMOTE_CLI_CALL", "CMD", strCmd, szPath);

		pCombo->InsertString(0, strCmd);
		for(int i=pCombo->GetCount(); i > 20 ; i--)
		{
			pCombo->DeleteString(i-1);
		}

		for(int i=0; i<pCombo->GetCount() ;i++)
		{
			CString strText;
			pCombo->GetLBText(i, strText);

			CString strKey;
			strKey.Format(_T("CMD%d"), i+1);
			WritePrivateProfileString("REMOTE_CLI_CALL", strKey, strText, szPath);
		}
	}

	m_CliTransfer.DeleteAllItem();
	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		CString strIp = m_lcList.GetItemText(i, eHostIP);
		if(strIp.IsEmpty())
		{
			m_lcList.SetItemText(i, eResult, LoadStringById(IDS_REMOTECLICALL_STR003));
			continue;
		}

		m_lcList.SetItemText(i, eResult, "");

		m_CliTransfer.AddItem(strIp);
	}

	if(m_CliTransfer.GetCount() > 0)
	{
		CString strCmd;
		GetDlgItemText(IDC_CB_COMMAND, strCmd);

		strCmd.Trim();

		m_CliTransfer.SetCommand(strCmd);

		this->m_bIsPlaying = true;
		if(m_CliTransfer.Run())
		{
			GetDlgItem(IDC_CB_COMMAND)->EnableWindow(FALSE);
			GetDlgItem(IDOK)->EnableWindow(FALSE);
			GetDlgItem(IDC_BN_FILESAVE)->EnableWindow(FALSE);
			GetDlgItem(IDCANCEL)->SetWindowText(LoadStringById(IDS_REMOTECLICALL_STR002));
		}
	}
}

void CRemoteCliCall::OnBnClickedCancel()
{
	if(m_CliTransfer.IsRunning())
	{
		m_CliTransfer.Stop();
	}
	else
	{
		OnCancel();
	}
}

void CRemoteCliCall::OnCancel()
{
	CDialog::OnCancel();

	if(AfxGetMainWnd() && ::IsWindow(AfxGetMainWnd()->GetSafeHwnd()))
	{
		AfxGetMainWnd()->PostMessage(WM_CLOSE_DIALOG, (WPARAM)this);
	}
}

void CRemoteCliCall::OnBnClickedBnFilesave()
{
	CHostExcelSave dlg;

	CString strExcelFile = dlg.Save(  LoadStringById(IDS_HOSTVIEW_STR001)
									, m_lcList
									);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}

void CRemoteCliCall::InitList()
{
	m_szHostColum[eHostVNCStat] = LoadStringById(IDS_HOSTVIEW_LIST033);
	m_szHostColum[eMonitorStat] = LoadStringById(IDS_HOSTVIEW_LIST041);
	m_szHostColum[eHostType] = LoadStringById(IDS_HOSTVIEW_LIST042);
	m_szHostColum[eSiteName] = LoadStringById(IDS_HOSTVIEW_LIST007);
	m_szHostColum[eHostName] = LoadStringById(IDS_HOSTVIEW_LIST009);
	m_szHostColum[eHostID] = LoadStringById(IDS_HOSTVIEW_LIST008);
	m_szHostColum[eHostIP] = LoadStringById(IDS_HOSTVIEW_LIST016);
	m_szHostColum[eResult] = LoadStringById(IDS_REMOTECLICALL_STR007);

	//m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);

	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilHostList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilHostList.Add(&bmCheck, RGB(255, 255, 255));
	m_lcList.SetImageList(&m_ilHostList, LVSIL_SMALL);

	for(int nCol = 0; nCol < eHostEnd; nCol++)
	{
		m_lcList.InsertColumn(nCol, m_szHostColum[nCol], LVCFMT_LEFT, 100);
	}

	m_lcList.SetColumnWidth(eHostVNCStat, 15);
	m_lcList.SetColumnWidth(eMonitorStat, 15);

	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CRemoteCliCall::RefreshHostList()
{
	TraceLog(("RefreshHostList begin"));

	int nRow = 0;
	LVITEM item;
	CString szBuf;
	POSITION pos = m_lsSelHost.GetHeadPosition();
	m_lcList.DeleteAllItems();

	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsSelHost.GetNext(pos);

		m_lcList.InsertItem(nRow, _T(""), -1);
	
		UpdateListRow(nRow, &info);

		m_lcList.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lcList.GetCurSortCol();
	if(-1 != nCol)
		m_lcList.SortList(nCol, m_lcList.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);
	else
		m_lcList.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);

	TraceLog(("RefreshHostList end"));
}

void CRemoteCliCall::UpdateListRow(int nRow, SHostInfo* pInfo)
{
	if(!pInfo || m_lcList.GetItemCount() < nRow)
		return;

	// 0000707: 콘텐츠 다운로드 상태 조회 기능
    COLORREF crTextBk = COLOR_WHITE;
	if(pInfo->operationalState)
	{
		crTextBk = COLOR_YELLOW;
	}
	else
	{
		crTextBk = COLOR_GRAY;
	}
	m_lcList.SetRowColor(nRow, crTextBk, m_lcList.GetTextColor());

	CString szBuf;

	m_lcList.SetItemText(nRow, eSiteName, pInfo->siteName);
	m_lcList.SetItemText(nRow, eHostID, pInfo->hostId);
	m_lcList.SetItemText(nRow, eHostName, pInfo->hostName);
	m_lcList.SetItemText(nRow, eHostType, CUbcCode::GetInstance()->GetCodeName(_T("HostType"), pInfo->hostType));
	m_lcList.SetItemText(nRow, eHostIP, pInfo->ipAddress);

	// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	//bool bMonitorStat = utvUtil::getInstance()->monitorState(pInfo->monitorOff, pInfo->monitorOffList);
	int nMonitorImgIdx = 2;
	if(pInfo->operationalState)
	{
		if(pInfo->monitorState > 0) nMonitorImgIdx = eMonitorOn;
		if(pInfo->monitorState == 0) nMonitorImgIdx = eMonitorOff;
		if(pInfo->monitorState < 0) nMonitorImgIdx = eMonitorUnknown;
	}

	LVITEM itemMonitorStat;
	itemMonitorStat.iItem = nRow;
	itemMonitorStat.iSubItem = eMonitorStat;//m_lcList.GetColPos(m_szHostColum[eMonitorStat]);
	itemMonitorStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemMonitorStat.pszText = (nMonitorImgIdx == eMonitorUnknown ? _T("Unknown") : (nMonitorImgIdx == eMonitorOn ? _T("On") : _T("Off")));
	itemMonitorStat.iImage = nMonitorImgIdx;
	m_lcList.SetItem(&itemMonitorStat);

	LVITEM itemHostVNCStat;
	itemHostVNCStat.iItem = nRow;
	itemHostVNCStat.iSubItem = eHostVNCStat;//m_lcList.GetColPos(m_szHostColum[eHostVNCStat]);
	itemHostVNCStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemHostVNCStat.pszText = (pInfo->vncState || pInfo->operationalState ? _T("On") : _T("Off"));
	itemHostVNCStat.iImage = (pInfo->vncState || pInfo->operationalState ? eVncOn : eVncOff);
	m_lcList.SetItem(&itemHostVNCStat);
}

void CRemoteCliCall::CLIResultEvent(CCLITransfer::ITEM item)
{
	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		CString strIp = m_lcList.GetItemText(i, eHostIP);
		if(strIp.IsEmpty()) continue;
		if(strIp != item.szIp) continue;

		CString strMsg;
		if(item.nResult < 0) strMsg.Format(_T("%s [%d]"), LoadStringById(IDS_REMOTECLICALL_STR004), item.nResult);
		if(item.nResult == 0) strMsg.Format(_T("%s [%d]"), LoadStringById(IDS_REMOTECLICALL_STR005), item.nResult);
		if(item.nResult > 0) strMsg.Format(_T("%s [%d]"), LoadStringById(IDS_REMOTECLICALL_STR006), item.nResult);

		m_lcList.SetItemText(i, eResult, strMsg);
		break;
	}
}

void CRemoteCliCall::CLIFinishEvent()
{
	GetDlgItem(IDC_CB_COMMAND)->EnableWindow(TRUE);
	GetDlgItem(IDOK)->EnableWindow(TRUE);
	GetDlgItem(IDC_BN_FILESAVE)->EnableWindow(TRUE);
	GetDlgItem(IDCANCEL)->SetWindowText(LoadStringById(IDS_REMOTECLICALL_STR001));

	this->m_bIsPlaying = false;

	UbcMessageBox(LoadStringById(IDS_REMOTECLICALL_MSG002));
}


bool CRemoteCliCall::DoServerQuery(CString& query, CString& errMsg)
{
	TraceLog(("DoServerQuery begin", query));
	return true;	
}
void CRemoteCliCall::RefreshQueryList()
{
	TraceLog(("RefreshQueryResult begin", NULL));
}
void CRemoteCliCall::InitQueryList()
{
	TraceLog(("InitQueryList begin", NULL));
}