#pragma once

#include "resource.h"

// CMonitorPowerDlg 대화 상자입니다.

class CMonitorPowerDlg : public CDialog
{
	DECLARE_DYNAMIC(CMonitorPowerDlg)

	int m_nMonitorState;
public:
	CMonitorPowerDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMonitorPowerDlg();

	int GetMonitorState() { return m_nMonitorState; }

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MONITOR_POWER_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonOn();
	afx_msg void OnBnClickedButtonOff();
};
