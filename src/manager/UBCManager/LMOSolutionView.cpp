// LMOSolutionView.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "LMOSolutionView.h"
#include "AnnounceDlg.h"
#include "common/UbcGUID.h"
#include "common/libProfileManager/ProfileManager.h"
#include "LMOSolutionDetailDlg.h"

// CLMOSolutionView
IMPLEMENT_DYNCREATE(CLMOSolutionView, CFormView)

CLMOSolutionView::CLMOSolutionView() : CFormView(CLMOSolutionView::IDD)
,	m_Reposition(this)
{
	m_bInit = false;
}

CLMOSolutionView::~CLMOSolutionView()
{
}

void CLMOSolutionView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LMO_SOLUTION, m_lscLmo);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_btAdd);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_btDel);
	DDX_Control(pDX, IDC_BUTTON_MOD, m_btMod);
	DDX_Control(pDX, IDC_BUTTON_REF, m_btRef);
}

BEGIN_MESSAGE_MAP(CLMOSolutionView, CFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()

	ON_NOTIFY(NM_DBLCLK, IDC_LIST_LMO_SOLUTION, OnNMDblclkList)

	ON_BN_CLICKED(IDC_BUTTON_ADD, OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_MOD, OnBnClickedButtonMod)
	ON_BN_CLICKED(IDC_BUTTON_REF, OnBnClickedButtonRef)

END_MESSAGE_MAP()

// CLMOSolutionView diagnostics
#ifdef _DEBUG
void CLMOSolutionView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CLMOSolutionView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CLMOSolutionView message handlers
int CLMOSolutionView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	GetClientRect(&m_rcClient);

	return 0;
}

void CLMOSolutionView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	if(m_bInit)	return;

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_LMO_SOLUTIONVIEW_STR001));

	m_btAdd.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btDel.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btMod.LoadBitmap(IDB_BUTTON_MODIFY, RGB(255,255,255));
	m_btRef.LoadBitmap(IDB_BUTTON_REFRESH2, RGB(255,255,255));

	m_btAdd.SetToolTipText(LoadStringById(IDS_LMO_SOLUTIONVIEW_STR002));
	m_btDel.SetToolTipText(LoadStringById(IDS_LMO_SOLUTIONVIEW_STR003));
	m_btMod.SetToolTipText(LoadStringById(IDS_LMO_SOLUTIONVIEW_STR004));
	m_btRef.SetToolTipText(LoadStringById(IDS_LMO_SOLUTIONVIEW_STR005));

	m_Reposition.SetParentRect(m_rcClient);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscLmo, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	InitList();
//	SetList();
}

void CLMOSolutionView::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CFormView::OnClose();
}

void CLMOSolutionView::OnDestroy()
{
	CFormView::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscLmo.SaveColumnState("LMO_SOLUTION-LIST", szPath);
}

void CLMOSolutionView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_Reposition.Move();
}

void CLMOSolutionView::InitList()
{
	// eCheck, eDescription, eTableName, eTimeFieldName, eLogType, eDuration, eAdminState, eEnd
	m_ColumTitle[eCheck     ] = _T("");
	m_ColumTitle[eDescription     ] =	LoadStringById(IDS_LMO_SOLUTIONVIEW_LST001);
	m_ColumTitle[eTableName ] =			LoadStringById(IDS_LMO_SOLUTIONVIEW_LST002);
	m_ColumTitle[eTimeFieldName   ] =	LoadStringById(IDS_LMO_SOLUTIONVIEW_LST003);
	m_ColumTitle[eLogType   ] =			LoadStringById(IDS_LMO_SOLUTIONVIEW_LST004);
	m_ColumTitle[eDuration  ] =			LoadStringById(IDS_LMO_SOLUTIONVIEW_LST005);
	m_ColumTitle[eAdminState] =			LoadStringById(IDS_LMO_SOLUTIONVIEW_LST006);

	m_lscLmo.SetExtendedStyle(m_lscLmo.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lscLmo.InsertColumn(eCheck,			m_ColumTitle[eCheck], LVCFMT_LEFT, 22);
	m_lscLmo.InsertColumn(eDescription,		m_ColumTitle[eDescription], LVCFMT_LEFT, 200);
	m_lscLmo.InsertColumn(eTableName,		m_ColumTitle[eTableName], LVCFMT_LEFT, 150);
	m_lscLmo.InsertColumn(eTimeFieldName,	m_ColumTitle[eTimeFieldName], LVCFMT_LEFT, 150);
	m_lscLmo.InsertColumn(eLogType,			m_ColumTitle[eLogType], LVCFMT_LEFT, 100);
	m_lscLmo.InsertColumn(eDuration,		m_ColumTitle[eDuration], LVCFMT_LEFT, 100);
	m_lscLmo.InsertColumn(eAdminState,		m_ColumTitle[eAdminState], LVCFMT_LEFT, 60);

	m_lscLmo.InitHeader(IDB_LIST_HEADER);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscLmo.LoadColumnState("LMO_SOLUTION-LIST", szPath);
}

void CLMOSolutionView::SetList()
{
	//if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority){
	if(!CCopModule::GetObject()->GetLMOSolution(m_lsLMOSolution)){
		UbcMessageBox("GetLMOSolution failed");
		return;
	}
	m_lscLmo.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsLMOSolution.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SLMOSolutionInfo info = m_lsLMOSolution.GetNext(pos);
		// eCheck, eDescription, eTableName, eTimeFieldName, eLogType, eDuration, eAdminState, eEnd
		
		m_lscLmo.InsertItem(nRow, "");
		m_lscLmo.SetItemText(nRow, eDescription, info.description);
		m_lscLmo.SetItemText(nRow, eTableName  , info.tableName);
		m_lscLmo.SetItemText(nRow, eTimeFieldName    , info.timeFieldName);
		m_lscLmo.SetItemText(nRow, eLogType  , ((info.logType==0) ? "DB":"File"));
		char buf[20] = {0,};
		sprintf(buf,"%ld", info.duration);
		m_lscLmo.SetItemText(nRow, eDuration, buf);
		m_lscLmo.SetItemText(nRow, eAdminState  , ((info.adminState == 1) ? "true":"false"));

		m_lscLmo.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
	m_lscLmo.SetRedraw(TRUE);

}


void CLMOSolutionView::OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult)
{

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscLmo.GetItemData(nRow);
	SLMOSolutionInfo info = m_lsLMOSolution.GetAt(pos);

	CLMOSolutionDetailDlg dlg;
	dlg.SetInfo(info);

	if(dlg.DoModal() != IDOK) return;

	dlg.GetInfo(info);


	if(!CCopModule::GetObject()->SetLMOSolution(info)){
		UbcMessageBox("Set LMOSolution failed");
		return;
	}

	SetList();
/*
	if(info.AnnoId.IsEmpty())
	{
		info.AnnoId = CUbcGUID::GetInstance()->ToGUID(_T(""));
	}

	// 0001375: 긴급공지 방식을 현재의 Event 방식에서 INI download 방식으로 변경한다.
	CString szLocalPath;
	szLocalPath.Format("%s%sdata\\", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	CString szAnnounceFile;
	szAnnounceFile.Format("UBCAnnounce_%s.ini", info.AnnoId);

	if(!SaveFile(szLocalPath + szAnnounceFile, info))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_LMO_SOLUTIONVIEW_MSG003), info.Title);
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!GetEnvPtr()->PutFile(szAnnounceFile, szLocalPath, "/config/", GetEnvPtr()->m_szSite))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_LMO_SOLUTIONVIEW_MSG004), info.Title);
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(CCopModule::GetObject()->SetAnnounce(info.SiteId, info.AnnoId, info))
	{
		m_lsLMOSolution.GetAt(pos) = info;
		m_lscLmo.SetItemText(nRow, eTitle, info.Title);
		
		if(!info.BgFile.IsEmpty())
		{
			CString szLocal;
			szLocal.Format("%s\\%s%s\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER);
			GetEnvPtr()->PutFile(info.BgFile, szLocal, info.BgLocation, info.SiteId);
		}
	}
*/
}

void CLMOSolutionView::OnBnClickedButtonAdd()
{
	SLMOSolutionInfo info;
	CLMOSolutionDetailDlg dlg;
	dlg.SetInfo(info);

	if(dlg.DoModal() != IDOK) return;

	dlg.GetInfo(info);

	if(info.tableName.GetLength() > 4 && info.tableName.Mid(0,4).CompareNoCase("UBC_") == 0){
		info.lmId = info.tableName.Mid(4);
	}else{
		info.lmId = info.tableName;
	}

	if(!CCopModule::GetObject()->CreateLMOSolution(info)){
		UbcMessageBox("Create LMOSolution failed");
		return;
	}

	SetList();
}

void CLMOSolutionView::OnBnClickedButtonDel()
{
	int nCnt = 0;
	for(int nRow = m_lscLmo.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscLmo.GetCheck(nRow)) continue;
		nCnt++;
	}

	if(nCnt == 0)
	{
		UbcMessageBox(LoadStringById(IDS_LMOSOLUTION_MSG002), MB_ICONINFORMATION);
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_LMOSOLUTION_MSG001), MB_YESNO|MB_ICONINFORMATION) != IDYES) return;

	for(int nRow = m_lscLmo.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscLmo.GetCheck(nRow)) continue;

		m_lscLmo.SetCheck(nRow, false);
		POSITION pos = (POSITION)m_lscLmo.GetItemData(nRow);
		SLMOSolutionInfo info = m_lsLMOSolution.GetNext(pos);;

		if(!CCopModule::GetObject()->DelLMOSolution(info)){
			UbcMessageBox("Delete LMOSolution failed");
			return;
		}

	}
	SetList();
}

void CLMOSolutionView::OnBnClickedButtonMod()
{
	int nCnt = 0;
	for(int nRow = m_lscLmo.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscLmo.GetCheck(nRow)) continue;
		nCnt++;
	}

	if(nCnt == 0)
	{
		UbcMessageBox(LoadStringById(IDS_LMOSOLUTION_MSG002), MB_ICONINFORMATION);
		return;
	}

	for(int nRow = m_lscLmo.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscLmo.GetCheck(nRow)) continue;

		m_lscLmo.SetCheck(nRow, false);
		POSITION pos = (POSITION)m_lscLmo.GetItemData(nRow);
		SLMOSolutionInfo info = m_lsLMOSolution.GetNext(pos);;

		CLMOSolutionDetailDlg dlg;
		dlg.SetInfo(info);

		if(dlg.DoModal() != IDOK) return;

		dlg.GetInfo(info);

		if(!CCopModule::GetObject()->SetLMOSolution(info)){
			UbcMessageBox("Set LMOSolution failed");
			return;
		}

	}
	SetList();

}

void CLMOSolutionView::OnBnClickedButtonRef()
{
	SetList();
	UbcMessageBox(LoadStringById(IDS_REFRASHLIST_MSG), MB_ICONINFORMATION);
}

