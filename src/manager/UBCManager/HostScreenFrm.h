#pragma once

#include "ubccopcommon\eventhandler.h"


// CHostScreenFrm frame

class CHostScreenFrm : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CHostScreenFrm)
protected:
	CHostScreenFrm();           // protected constructor used by dynamic creation
	virtual ~CHostScreenFrm();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
