// UserFrame.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "UserFrame.h"


// CUserFrame

IMPLEMENT_DYNCREATE(CUserFrame, CMDIChildWnd)

CUserFrame::CUserFrame()
{

}

CUserFrame::~CUserFrame()
{
}


BEGIN_MESSAGE_MAP(CUserFrame, CMDIChildWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CUserFrame message handlers

int CUserFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDR_USERFRAME), false);

	return 0;
}
