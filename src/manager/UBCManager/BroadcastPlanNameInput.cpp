// BroadcastPlanNameInput.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "BroadcastPlanNameInput.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "common/PreventChar.h"

// CBroadcastPlanNameInput 대화 상자입니다.

IMPLEMENT_DYNAMIC(CBroadcastPlanNameInput, CDialog)

CBroadcastPlanNameInput::CBroadcastPlanNameInput(DLG_MODE nMode, CWnd* pParent /*=NULL*/)
	: CDialog(CBroadcastPlanNameInput::IDD, pParent)
	, m_nMode(nMode)
{

}

CBroadcastPlanNameInput::~CBroadcastPlanNameInput()
{
}

void CBroadcastPlanNameInput::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BN_DUP_CHECK, m_bnDupCheck);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
	DDX_Text(pDX, IDC_EB_NAME, m_strBroadcastPlanName);
	DDV_MaxChars(pDX, m_strBroadcastPlanName, 255);
}


BEGIN_MESSAGE_MAP(CBroadcastPlanNameInput, CDialog)
	ON_BN_CLICKED(IDOK, &CBroadcastPlanNameInput::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CBroadcastPlanNameInput::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_DUP_CHECK, &CBroadcastPlanNameInput::OnBnClickedBnDupCheck)
END_MESSAGE_MAP()


// CBroadcastPlanNameInput 메시지 처리기입니다.

BOOL CBroadcastPlanNameInput::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText( (m_nMode == eModeCreate ? LoadStringById(IDS_PLANNAMEDLG_STR001) : LoadStringById(IDS_PLANNAMEDLG_STR002)) );

	m_bnDupCheck.LoadBitmap(IDB_BUTTON_DUPCHECK, RGB(255,255,255));
	m_bnOK      .LoadBitmap(IDB_BUTTON_APPLY   , RGB(255,255,255));
	m_bnCancel  .LoadBitmap(IDB_BUTTON_CANCEL  , RGB(255,255,255));

	m_bnDupCheck.SetToolTipText(LoadStringById(IDS_PLANNAMEDLG_BTN001));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CBroadcastPlanNameInput::OnBnClickedBnDupCheck()
{
	if(InputErrorCheck())
	{
		UbcMessageBox(LoadStringById(IDS_PLANNAMEDLG_MSG002));
	}
}

void CBroadcastPlanNameInput::OnBnClickedOk()
{
	if(!InputErrorCheck()) return;

	GetDlgItemText(IDC_EB_NAME, m_strBroadcastPlanName);

	OnOK();
}

bool CBroadcastPlanNameInput::InputErrorCheck()
{
	CString strBroadcastPlanName;
	GetDlgItemText(IDC_EB_NAME, strBroadcastPlanName);
	strBroadcastPlanName.Trim();
	if(strBroadcastPlanName.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_PLANNAMEDLG_MSG003));
		return FALSE;
	}

	if(strBroadcastPlanName.Find(" ") > 0)
	{
		UbcMessageBox(LoadStringById(IDS_PLANNAMEDLG_MSG004), MB_ICONWARNING);
		GetDlgItem(IDC_EB_NAME)->SetFocus();
		((CEdit*)GetDlgItem(IDC_EB_NAME))->SetSel(0, -1);
		return FALSE;
	}

	if(CPreventChar::GetInstance()->HavePreventChar(strBroadcastPlanName, CPreventChar::ePackageName))
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_PLANNAMEDLG_MSG005), CPreventChar::GetInstance()->GetPreventChar(CPreventChar::ePackageName));
		UbcMessageBox(strMsg, MB_ICONERROR);
		GetDlgItem(IDC_EB_NAME)->SetFocus();
		((CEdit*)GetDlgItem(IDC_EB_NAME))->SetSel(0, -1);
		return FALSE;
	}//if

	BOOL bIsExist = CCopModule::GetObject()->IsExistBroadcastingPlan( (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
																	, strBroadcastPlanName
																	);

	if(bIsExist)
	{
		UbcMessageBox(LoadStringById(IDS_PLANNAMEDLG_MSG001));
	}

	return !bIsExist;
}

void CBroadcastPlanNameInput::OnBnClickedCancel()
{
	OnCancel();
}
