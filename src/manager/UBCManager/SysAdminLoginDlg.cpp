// SysAdminLoginDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"

#include "SysAdminLoginDlg.h"


#define		SYS_ADMIN_LOGIN_PASSWD		("-507263a")


// CSysAdminLoginDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSysAdminLoginDlg, CDialog)

CSysAdminLoginDlg::CSysAdminLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSysAdminLoginDlg::IDD, pParent)
{

}

CSysAdminLoginDlg::~CSysAdminLoginDlg()
{
}

void CSysAdminLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_ID, m_editID);
	DDX_Control(pDX, IDC_EDIT_PASSWD, m_editPasswd);
}


BEGIN_MESSAGE_MAP(CSysAdminLoginDlg, CDialog)
END_MESSAGE_MAP()


// CSysAdminLoginDlg 메시지 처리기입니다.

void CSysAdminLoginDlg::OnOK()
{
	m_editID.GetWindowText(m_userId);

	CString str_passwd;
	m_editPasswd.GetWindowText(str_passwd);

	// userId 가 admin 이거나 비어있거나로 수정
	if(str_passwd != SYS_ADMIN_LOGIN_PASSWD)
	{
		UbcMessageBox(IDS_SYSADMINLOGIN_FAIL_TO_LOGIN, MB_ICONSTOP);
		return;
	}

	if(!m_userId.IsEmpty() && m_userId != "admin") {
		UbcMessageBox(IDS_SYSADMINLOGIN_FAIL_TO_LOGIN, MB_ICONSTOP);
		return;
	}

	CDialog::OnOK();
}
