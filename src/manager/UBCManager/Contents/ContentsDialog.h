#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "VideoContentsDialog.h"
#include "ImageContentsDialog.h"
#include "HorzTickerContentsDialog.h"
#include "FlashContentsDialog.h"
#include "WebContentsDialog.h"
#include "PPTContentsDlg.h"
//#include "SMSContentsDialog.h"
//#include "WebContentsDialog.h"
//#include "TVContentsDlg.h"
//#include "RSSContentsDlg.h"
//#include "WizardContentsDialog.h"

//#include "OverlayInterface.h"
#include "ReposControl.h"

#include "common/HoverButton.h"
#include "common/UBCTabCtrl.h"


///////////////////////////////////////////////////////////////////////////
// CContentsDialog 대화 상자입니다.

class CContentsDialog : public CDialog
{
	DECLARE_DYNAMIC(CContentsDialog)

public:
	CContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CONTENTS };
	enum {
		EICO_VIDEO,	EICO_IMAGE,	EICO_TICKER,
		EICO_FLASH,	EICO_WEB, EICO_PPT,
		/*EICO_TEXT,		
		EICON_TV, EICON_RSS, EICO_WIZARD,*/
		EICO_UNKNOWN
	};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CStringArray	m_arTabTitle;
	CReposControl	m_reposControl;
	CONTENTS_INFO	m_contentsInfo;
	bool			m_bEditMode;
	bool			m_bPreviewMode;

	//CUBCStudioDoc* m_pDocument;

public:
	//void	SetDocument(CUBCStudioDoc* pDoc) { m_pDocument=pDoc; };

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTcnSelchangeTabContentsType(NMHDR *pNMHDR, LRESULT *pResult);

	//CTabCtrl m_tabContentsType;

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	/*
	CButton m_btnOK;
	CButton m_btnCancel;
	*/
	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;
//	CXPTabCtrl		m_tabContentsType;
	CUBCTabCtrl		m_tabContentsType;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	CVideoContentsDialog		*m_pVideoContents;
	CImageContentsDialog		*m_pImageContents;
	CHorzTickerContentsDialog	*m_pHorzTickerContents;
	CFlashContentsDialog		*m_pFlashContents;
	CWebContentsDialog		*m_pWebContents;
	CPPTContentsDlg				*m_pPPTContents;
	//CSMSContentsDialog			*m_pSMSContents;
	//CWebContentsDialog			*m_pWebContents;
//	CWebCamContentsDialog		*m_pWebCamContents;
//	CPromotionContentsDialog	*m_pPromotionContents;
//	CPhoneContentsDialog		*m_pPhoneContents;
	//CTVContentsDlg				*m_pTVContents;
	//CRSSContentsDlg				*m_pRSSContents;
	//CWizardContentsDialog		*m_pWizardContents;

	void	ShowSubWindow();
	void	SetContentsInfo(CONTENTS_INFO& info) { m_contentsInfo = info; SetEditMode(); };
	void	GetContentsInfo(CONTENTS_INFO& info) { info = m_contentsInfo; };
	void	SetEditMode(bool bEditMode=true) { m_bEditMode = bEditMode; };
	// Modified by 정운형 2009-01-06 오전 11:35
	// 변경내역 :  CLI 연동 수정
	void	RunCLICmd(CONTENTS_INFO& info);		///<CLI 연동 명령어를 파일로 작성하여 CLI 명령어를 전달한다
	// Modified by 정운형 2009-01-06 오전 11:35
	// 변경내역 :  CLI 연동 수정
	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
	afx_msg void OnBnClickedOk();
};
