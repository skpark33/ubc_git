#pragma once

#include "Schedule.h"
#include "DataContainer.h"
//#include "UBCStudioDoc.h"


#define STR_ANNOFOLDER	"announce"

class CONTENTS_INFO
{
public:
	// attributes
	CString		strId                ;
	int 		nContentsType        ;
	CString		strContentsName      ;
	CString		strLocalLocation     ;	// local contents location
	CString		strServerLocation    ;	// server contents location
	CString		strFilename          ;
	ULONG 		nRunningTime         ;
	CString		strComment[10]       ;
	CString		strBgColor           ;	// background color
	CString		strFgColor           ;	// foreground color
	CString		strFont              ;	// font
	int			nFontSize            ;	// font size
	int			nPlaySpeed           ;	// tiker 등이 플레이 되는 속도
	int 		nSoundVolume         ;
	int			nDirection           ;	// 문자방향 가로 0, 세로 1
	int			nAlign               ;	// 정렬방향 1~9

	CString		strChngFileName      ;	///<콘텐츠 이름에 '[', ']' 문자가 있어서 이름이 변경되었을 경우 병경된 이름
	bool		bNameChanged         ;	///<콘텐츠의 이름이 변경 되었는지 여부
	bool		bLocalFileExist      ;	///<콘텐츠의 경로에 실제 파일이 존재하는지 여부
	bool		bServerFileExist     ;

	int			nWidth               ;
	int			nHeight              ;
	int			nCurrentComment      ;

	//							
	ULONGLONG	nFilesize            ;
	int			nContentsState       ;
	CString		strPromotionValueList;

	// 0000611: 플레쉬 콘텐츠 마법사
	CString		strWizardXML         ;  // wizard 용 XML string
	CString		strWizardFiles       ;  // wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

	CString		strParentId          ;	// 부속파일의 경우 사용된다.

	// 공용컨텐츠용 attributes
	CString		strMgrId;
	CString		strSiteId;
	CString		strProgramId;
	CString		strCategory;
	CString		strRegisterId;
	CString		strRegisterTime;	//CTime		tmRegisterTime;
	CString		strVerifier;
	CString		strDescription;
	bool		bIsPublic;

	int			nContentsCategory;
	int			nPurpose;
	int			nHostType;
	int			nVertical;
	int			nResolution;
	CString		strValidationDate;	//CTime		tmValidationDate;
	CString		strRequester;
	CString		strVerifyMembers;
	CString		strVerifyTime;		//CTime		tmVerifyTime;
	CString		strRegisterType;
	//

	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	CString		strFileMD5;
	bool		bIsFileChanged;

	int			nAlpha;

	CONTENTS_INFO()
	: nContentsType   (-1)
	, nRunningTime    (0)
	, nFontSize       (36)	// font size
	, nPlaySpeed      (0)		// tiker 등이 플레이 되는 속도
	, nSoundVolume    (0)
	, nDirection      (0)		// 문자방향 가로 0, 세로 1
	, nAlign          (5)		// 정렬방향 1~9
	, bNameChanged    (false)	///<콘텐츠의 이름이 변경 되었는지 여부
	, bLocalFileExist (false)	///<콘텐츠의 경로에 실제 파일이 존재하는지 여부
	, bServerFileExist(true)
	, nWidth          (0)
	, nHeight         (0)
	, nCurrentComment (0)
	, nFilesize       (0)
	, nContentsState  (CON_READY)
	, bIsPublic       (false)
	, nContentsCategory (99)
	, nPurpose (99)
	, nHostType (99)
	, nVertical (99)
	, nResolution (99)
	, bIsFileChanged(false)
	, nAlpha(0)
	{};

	virtual ~CONTENTS_INFO() {};

	CONTENTS_INFO& operator= (const CONTENTS_INFO& info)
	{
		strId                 = info.strId                ;
		nContentsType         = info.nContentsType        ;
		strContentsName       = info.strContentsName      ;
		strLocalLocation      = info.strLocalLocation     ;
		strServerLocation     = info.strServerLocation    ;
		strFilename           = info.strFilename          ;
		nRunningTime          = info.nRunningTime         ;
		strBgColor            = info.strBgColor           ;
		strFgColor            = info.strFgColor           ;
		strFont               = info.strFont              ;
		nFontSize             = info.nFontSize            ;
		nPlaySpeed            = info.nPlaySpeed           ;
		nSoundVolume          = info.nSoundVolume         ;
		nDirection            = info.nDirection           ;
		nAlign                = info.nAlign               ;
		strChngFileName       = info.strChngFileName      ;
		bNameChanged          = info.bNameChanged         ;
		bLocalFileExist       = info.bLocalFileExist      ;
		bServerFileExist      = info.bServerFileExist     ;
													 
		nWidth                = info.nWidth               ;
		nHeight               = info.nHeight              ;
		nCurrentComment       = info.nCurrentComment      ;
													 
		nFilesize             = info.nFilesize            ;
		nContentsState        = info.nContentsState       ;
		strPromotionValueList = info.strPromotionValueList;

		// 0000611: 플레쉬 콘텐츠 마법사
		strWizardXML          = info.strWizardXML         ;	// wizard 용 XML string
		strWizardFiles        = info.strWizardFiles       ;	// wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

		for(int i=0; i<10; i++) strComment[i] = info.strComment[i];

		strParentId           = info.strParentId          ;

		// 공용콘텐츠
		strMgrId              = info.strMgrId;
		strSiteId             = info.strSiteId;
		strProgramId          = info.strProgramId;
		strCategory           = info.strCategory;
		strRegisterId         = info.strRegisterId;
		strRegisterTime       = info.strRegisterTime;
		strVerifier           = info.strVerifier;
		strDescription        = info.strDescription;
		bIsPublic             = info.bIsPublic;

		nContentsCategory     = info.nContentsCategory;
		nPurpose              = info.nPurpose;
		nHostType             = info.nHostType;
		nVertical             = info.nVertical;
		nResolution           = info.nResolution;
		strValidationDate     = info.strValidationDate;
		strRequester          = info.strRequester;
		strVerifyMembers      = info.strVerifyMembers;
		strVerifyTime         = info.strVerifyTime;
		strRegisterType       = info.strRegisterType;
		//

		strFileMD5			  = info.strFileMD5; // file 진위여부를 체크하기 위해 MD5를 사용한다.
		bIsFileChanged			= info.bIsFileChanged;

		nAlpha				= info.nAlpha;
		return *this;
	}

	bool operator== (const CONTENTS_INFO & info)
	{
		if(strId                 != info.strId                ) return false;
		if(nContentsType         != info.nContentsType        ) return false;
		if(strContentsName       != info.strContentsName      ) return false;
		if(strFilename           != info.strFilename          ) return false;
		if(!strFilename.IsEmpty() || !info.strFilename.IsEmpty())
		{
			if(strLocalLocation  != info.strLocalLocation     ) return false;
			if(strServerLocation != info.strServerLocation    ) return false;
		}
		if(nRunningTime          != info.nRunningTime         ) return false;
		if(strBgColor            != info.strBgColor           ) return false;
		if(strFgColor            != info.strFgColor           ) return false;
		if(strFont               != info.strFont              ) return false;
		if(nFontSize             != info.nFontSize            ) return false;
		if(nPlaySpeed            != info.nPlaySpeed           ) return false;
		if(nSoundVolume          != info.nSoundVolume         ) return false;
		if(nDirection            != info.nDirection           ) return false;
		if(nAlign                != info.nAlign               ) return false;
		if(strChngFileName       != info.strChngFileName      ) return false;
		if(bNameChanged          != info.bNameChanged         ) return false;
//		if(bLocalFileExist       != info.bLocalFileExist      ) return false;	// 2010.11.10 변경유무 판단시 필요없는 항목이므로 체크하지 않음
//		if(bServerFileExist      != info.bServerFileExist     ) return false;	// ..
														 
		if(nWidth                != info.nWidth               ) return false;
		if(nHeight               != info.nHeight              ) return false;
		if(nCurrentComment       != info.nCurrentComment      ) return false;
														 
		if(nFilesize             != info.nFilesize            ) return false;
		if(nContentsState        != info.nContentsState       ) return false;
		if(strPromotionValueList != info.strPromotionValueList) return false;

		// 0000611: 플레쉬 콘텐츠 마법사
		if(strWizardXML          != info.strWizardXML         ) return false; // wizard 용 XML string
		if(strWizardFiles        != info.strWizardFiles       ) return false; // wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

		for(int i=0; i<10; i++) if(strComment[i] != info.strComment[i]) return false;

		if(strParentId           != info.strParentId          ) return false;
		//if(bIsPublic             != info.bIsPublic            ) return false;
		if(bIsFileChanged             != info.bIsFileChanged            ) return false;
		if(nAlpha             != info.nAlpha            ) return false;

		return true;
	} 

	// member function
	bool		Save(LPCTSTR lpszFullPath, LPCTSTR lpszSaveID);
	bool		Load(LPCTSTR lpszFullPath, LPCTSTR lpszLoadID);
};

typedef CMapStringToPtr CONTENTS_INFO_MAP;
typedef	CArray<CONTENTS_INFO*, CONTENTS_INFO*>	CONTENTS_INFO_LIST;


typedef struct
{
	LPTSTR			extension;
	Mng_Browser::CONTENTS_TYPE	type;
} EXT_TYPE;

extern EXT_TYPE ext_type[];

// CSubContentsDialog 대화 상자입니다.

class CSubContentsDialog : public CDialog
{
	DECLARE_DYNAMIC(CSubContentsDialog)

	//CUBCStudioDoc* m_pDocument;

public:
	CSubContentsDialog(UINT nIDTemplate, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubContentsDialog();

// 대화 상자 데이터입니다.

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	CBrush					m_brushBG;
	CArray<CWnd*, CWnd*>	m_listNoCTLWnd;
	ULONGLONG				m_ulFileSize;
	CString					m_strFileMD5;		// file 진위여부를 체크하기 위해 MD5를 사용한다.
	BOOL					m_bIsChangedFile;	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제

	static bool				IsValidType(CString szExt, Mng_Browser::CONTENTS_TYPE type=Mng_Browser::CONTENTS_NOT_DEFINE); 
	bool					IsFitContentsName(CString strContentsName, bool bErrMsg=true);
	bool					CheckTotalContentsSize(CString strFilePath);

public:
	//void					SetDocument(CUBCStudioDoc* pDoc) { m_pDocument=pDoc; };
	//CUBCStudioDoc*			GetDocument();
	BOOL					IsChangedFile() { return m_bIsChangedFile; }	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제

	virtual Mng_Browser::CONTENTS_TYPE	GetContentsType() = 0;
	virtual void			Stop() = 0;
	virtual bool			GetContentsInfo(CONTENTS_INFO& info) = 0;
	virtual bool			SetContentsInfo(CONTENTS_INFO& info) = 0;
	virtual void			SetPreviewMode(bool bPreviewMode) = 0;

	virtual bool			CopyContents(CString szSPathName, CString filename, CString ext);
	virtual bool			GetFile(LPCTSTR lpszFullPath, LPCTSTR annoId, LPCTSTR siteId);

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
