#pragma once
#include "afxwin.h"
#include "resource.h"

#include "SubContentsDialog.h"

#include "ximage.h"
#include "EditEx.h"

#include "Schedule.h"

#include "ReposControl.h"

// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업
#include "common/HoverButton.h"
// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업


// CImageContentsDialog 대화 상자입니다.

class CImageContentsDialog : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CImageContentsDialog)

public:
	CImageContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CImageContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_IMAGE_CONTENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	Mng_Browser::CPictureSchedule	m_wndImage;
	CReposControl		m_reposControl;
	bool				m_bPreviewMode;		///<미리보기 모드
	BOOL				m_bPermanent;		///<24시간 running time

	ULONGLONG			LoadContents(LPCTSTR lpszFullPath, ULONGLONG infoFileSize=0,LPCTSTR annoId="", LPCTSTR siteId="");


public:

	virtual Mng_Browser::CONTENTS_TYPE	GetContentsType() { return Mng_Browser::CONTENTS_IMAGE; };
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedButtonBrowserContentsFile();
	afx_msg void OnBnClickedButtonOriginal();
	afx_msg void OnBnClickedButtonFitToWindow();
	afx_msg void OnBnClickedButtonFullScreen();

	CEdit	m_editContentsName;
	CEdit	m_editContentsFileName;
	CEditEx	m_editContentsPlayMinute;
	CEditEx	m_editContentsPlaySecond;
	CStatic m_staticContentsWidth;
	CStatic m_staticContentsHeight;
	CStatic m_staticContentsFileSize;

	CStatic m_groupPreview;
	CStatic	m_staticContents;

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	/*
	CButton	m_btnFullScreen;
	CButton	m_btnFitToWindow;
	CButton	m_btnOriginal;
	*/
	CHoverButton	m_btnFullScreen;
	CHoverButton	m_btnFitToWindow;
	CHoverButton	m_btnOriginal;
	CHoverButton	m_btnBrowseContentsFile;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	CString			m_strLocation;

	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
	void	EnableAllControls(BOOL bEnable);			///<컨트롤의 사용가능여부 설정

	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnBnClickedPermanentCheck();
};
