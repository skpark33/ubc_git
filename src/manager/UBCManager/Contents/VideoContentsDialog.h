#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "resource.h"

#include "SubContentsDialog.h"

#include "common/HoverButton.h"
#include "EditEx.h"
#include "BitSlider.h"

#include "ReposControl.h"

#include "Frame.h"
#include "Schedule.h"

// Modified by 정운형 2008-12-23 오후 4:01
// 변경내역 :  캡션기능 추가
#include "common/FontPreviewCombo.h"
#include "ColorPickerCB.h"
// Modified by 정운형 2008-12-23 오후 4:01
// 변경내역 :  캡션기능 추가

// CVideoContentsDialog 대화 상자입니다.


class CVideoContentsDialog : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CVideoContentsDialog)

public:
	CVideoContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CVideoContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIDEO_CONTENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	bool	m_bContentsMute;
	bool	m_bPreviewMute;
	bool	m_bPreviewMode;

	Mng_Browser::CVideoSchedule			m_wndVideo;

	CReposControl			m_reposControl;

	void	EnableAllControls(BOOL bEnable);

	ULONGLONG	LoadContents(LPCTSTR lpszFullPath, ULONGLONG infoFileSize=0,LPCTSTR annoId="", LPCTSTR siteId="");

public:

	virtual Mng_Browser::CONTENTS_TYPE	GetContentsType() { return Mng_Browser::CONTENTS_VIDEO; };
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);

	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnEnChangeEditContentsVolume();

	afx_msg void OnBnClickedButtonBrowserContentsFile();
	afx_msg void OnBnClickedButtonPreviewPlay();
	afx_msg void OnBnClickedButtonPreviewStop();
	afx_msg void OnBnClickedButtonPreviewMute();

	CEdit			m_editContentsName;
	CEdit			m_editContentsFileName;
	CEditEx			m_editContentsVolume;
	CBitSlider		m_sliderContentsVolume;
	CStatic			m_staticContentsPlayMinute;
	CStatic			m_staticContentsPlaySecond;
	CStatic			m_staticContentsWidth;
	CStatic			m_staticContentsHeight;
	CStatic			m_staticContentsFileSize;
	//CEdit			m_editFelicaUrl;

	CStatic			m_groupPreview;
	CStatic			m_staticContents;
	CSliderCtrl		m_sliderPreviewPosition;
	CHoverButton	m_btnPreviewMute;
	CBitSlider		m_sliderPreviewVolume;

	HCURSOR			m_hHandCur;

	CHoverButton		m_btnBrowserContentsFile;

	CHoverButton		m_btnPreviewPlay;
	CHoverButton		m_btnPreviewStop;
	CHoverButton		m_btnMute;

	CString				m_strLocation;	

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
};
