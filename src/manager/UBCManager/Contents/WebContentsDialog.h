#pragma once

#include "afxwin.h"
#include "resource.h"
#include "SubContentsDialog.h"
#include "EditEx.h"
#include "Schedule.h"
#include "ShortcutEdit2.h"
#include "common/HoverButton.h"

class CWebContentsDialog : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CWebContentsDialog)

public:
	CWebContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CWebContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_WEB_CONTENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	Mng_Browser::CURLSchedule	m_wndWeb;
//	CxImage			m_image;

	bool			m_bVScroll;
	bool			m_bHScroll;
	bool			m_bPreviewMode;
	BOOL			m_bPermanent;

public:

	virtual Mng_Browser::CONTENTS_TYPE	GetContentsType() { return Mng_Browser::CONTENTS_WEBBROWSER; };
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);

	CEdit	m_editContentsName;
	CEditEx	m_editContentsFileName;
	CEditEx	m_editContentsPlayMinute;
	CEditEx	m_editContentsPlaySecond;
	CEditEx m_editURL;
	CStatic	m_staticContents;
	CString m_strURL;
	CString m_strFile;
	CString m_strLocation;
	CHoverButton m_btnBrowserContentsFile;
	CHoverButton m_btnDelFile;
	CEdit	m_editFelicaUrl;

	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	//CButton	m_btnPreviewPlay;
	CHoverButton	m_btnPreviewPlay;
	void			EnableAllControls(BOOL bEnable);
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업

	ULONGLONG LoadContents(LPCTSTR lpszFullPath, ULONGLONG infoFileSize=0, LPCTSTR annoId="", LPCTSTR siteId="");

	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
	void	SetPlayContents();
	
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnBnClickedFileName();
	afx_msg void OnBnClickedButtonPreviewPlay();
	afx_msg void OnBnClickedPermanentCheck();
	afx_msg void OnBnClickedWebcontentsVscroll();
	afx_msg void OnBnClickedWebcontentsHscroll();
	afx_msg void OnBnClickedDeletefile();
	CShortcutEdit2 m_editShortCutStart;
	CShortcutEdit2 m_editShortCutEnd;
};
