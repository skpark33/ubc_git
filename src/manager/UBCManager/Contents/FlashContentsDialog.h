#pragma once
#include "afxwin.h"
#include "resource.h"

#include "SubContentsDialog.h"

#include "EditEx.h"

#include "ReposControl.h"

#include "Frame.h"
#include "Schedule.h"

// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업
#include "common/HoverButton.h"
#include "common/utblistctrlex.h"
#include "afxcmn.h"
// Modified by 정운형 2009-01-21 오전 11:21
// 변경내역 :  이미지 추가 작업


// CFlashContentsDialog 대화 상자입니다.

class CFlashContentsDialog : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CFlashContentsDialog)

public:
	CFlashContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFlashContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FLASH_CONTENTS };

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CBrush	m_brushBG;
	Mng_Browser::CFlashSchedule	m_wndFlash;
	CReposControl	m_reposControl;
	bool			m_bPreviewMode;		///<미리보기 모드
	BOOL			m_bPermanent;		///<24시간 running time

	ULONGLONG			LoadContents(LPCTSTR lpszFullPath, ULONGLONG infoFileSize=0,LPCTSTR annoId="", LPCTSTR siteId="");
public:

	virtual Mng_Browser::CONTENTS_TYPE	GetContentsType() { return Mng_Browser::CONTENTS_FLASH; };
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnBnClickedButtonBrowserContentsFile();

	CEdit		m_editContentsName;
	CEdit		m_editContentsFileName;
	//CEdit		m_editFelicaUrl;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	//CButton		m_btnBrowseContentsFile;
	CHoverButton	m_btnBrowseContentsFile;
	// Modified by 정운형 2009-01-21 오전 11:21
	// 변경내역 :  이미지 추가 작업
	CEditEx		m_editContentsPlayMinute;
	CEditEx		m_editContentsPlaySecond;
	CStatic		m_staticContentsFileSize;

	CStatic		m_groupPreview;
	CStatic		m_staticContents;

	CString		m_strLocation;

	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
	void	EnableAllControls(BOOL bEnable);			///<컨트롤의 사용가능여부 설정

	// 부속파일처리 관련 수정
	//CONTENTS_INFO_MAP	m_mapSubFilesContents;
	//void			CreateSubFilesContents();
	//void			DeleteSubFilesContents();

	CString			m_strContentsId;
	CImageList		m_ilSystemImg;
	//CUTBListCtrlEx	m_lcFiles;
	//CTreeCtrl		m_treeFiles;
	//CHoverButton	m_btnBrowseAddFolder;
	//CHoverButton	m_btnBrowseDelFolder;
	//CHoverButton	m_btnDownloadFolder ;
	//CHoverButton	m_btnBrowseAddFile  ;
	//CHoverButton	m_btnBrowseDelFile  ;
	//CHoverButton	m_btnDownloadContents;
	//void			InitSubFilesCtrl();
	//void			RefreshFilesTree();
	//void			RefreshFilesList(CString strLocation);
	//void			UpdateFilesListRow(int nRow, CONTENTS_INFO* pContentsInfo);
	//void			AddSubFolder(CString strChildPath, CString szFolderPath);
	//CONTENTS_INFO*	AddSubFileContents(CString strChildPath, CString strFilepath, BOOL& bExist);
	//void			SelectTreeItem(CString strChildPath);
	//CString			GetTreeFullPath(HTREEITEM hItem);
	//BOOL			DownloadSelectedContents();

	static int CALLBACK CompareList(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnBnClickedPermanentCheck();
	//afx_msg void OnTvnSelchangedTreeFiles(NMHDR *pNMHDR, LRESULT *pResult);
	//afx_msg void OnBnClickedButtonBrowserAddFolder();
	//afx_msg void OnBnClickedButtonBrowserDelFolder();
	//afx_msg void OnBnClickedButtonBrowserAddFile();
	//afx_msg void OnBnClickedButtonBrowserDelFile();
	//afx_msg void OnBnClickedButtonDownloadFolder();
	//afx_msg void OnBnClickedButtonDownloadContents();
	//afx_msg void OnNMDblclkListFiles(NMHDR *pNMHDR, LRESULT *pResult);
	//afx_msg void OnLvnKeydownListFiles(NMHDR *pNMHDR, LRESULT *pResult);
	//afx_msg void OnTvnKeydownTreeFiles(NMHDR *pNMHDR, LRESULT *pResult);

};
