// ShortcutEdit2.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ShortcutEdit2.h"
//#include "DataContainer.h"

// CShortcutEdit2
IMPLEMENT_DYNAMIC(CShortcutEdit2, CEdit)

CShortcutEdit2::CShortcutEdit2()
{
	BYTE key[256];
	ZeroMemory(key, sizeof(key));
	GetKeyboardState(key);
	key[VK_MENU] = 0;
	key[VK_CONTROL] = 0;
	SetKeyboardState(key);

	m_nChar = 0;
	m_nFlag = 0;
}

CShortcutEdit2::~CShortcutEdit2()
{
}


BEGIN_MESSAGE_MAP(CShortcutEdit2, CEdit)
	ON_WM_SYSKEYDOWN()
	ON_WM_KEYDOWN()
	ON_WM_HELPINFO()
	ON_WM_SYSKEYUP()
	ON_WM_KEYUP()
END_MESSAGE_MAP()



// CShortcutEdit2 message handlers
void CShortcutEdit2::SetKeyValue(CString str)
{
	SetWindowText(str.GetBuffer(0));
}

void CShortcutEdit2::SetKeyValue(UINT nChar, UINT nFlag)
{
	m_nChar = nChar;
	m_nFlag = nFlag;

	if(!SetKeyText(nChar, nFlag)){
		m_nChar = 0;
		m_nFlag = 0;
	}
}

void CShortcutEdit2::GetKeyValue(UINT& nChar, UINT& nFlag)
{
	nChar = m_nChar;
	nFlag = m_nFlag;
}

void CShortcutEdit2::OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	SetKeyValue(nChar);

//	CEdit::OnSysKeyDown(nChar, nRepCnt, nFlags);
}

void CShortcutEdit2::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	SetKeyValue(nChar);

//	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CShortcutEdit2::SetKeyValue(UINT nChar)
{
	UINT nFlag = 0;

	//BYTE key[1000];
	//ZeroMemory(key, sizeof(key));
	//GetKeyboardState(key);

	//if(key[VK_MENU]){
	//	nFlag = VK_MENU;
	//}else if(key[VK_CONTROL]){
	//	nFlag = VK_CONTROL;
	//}

	if(GetKeyState(VK_MENU) & 0x8000){
		nFlag = VK_MENU;
	}
	else if(GetKeyState(VK_CONTROL) & 0x8000){
		nFlag = VK_CONTROL;
	}

//	if(GetKeyState(VK_MENU)&0x01 != 0){
//		sprintf(szTemp, "Alt + %s", szBuff);
//	}else if(GetKeyState(VK_CONTROL)&0x8000 != 0){
//		sprintf(szTemp, "Ctrl + %s", szBuff);
//	}

//	if(GetAsyncKeyState(VK_MENU)&0x01 != 0){
//		sprintf(szTemp, "Alt + %s", szBuff);
//	}else if(GetAsyncKeyState(VK_CONTROL)&0x8000 != 0){
//		sprintf(szTemp, "Ctrl + %s", szBuff);
//	}

	SetKeyValue(nChar, nFlag);
}

bool CShortcutEdit2::SetKeyText(UINT nChar, UINT nFlags)
{
	bool nRet = true;
	char szBuff[100], szTemp[100];
	ZeroMemory(szBuff, sizeof(szBuff));
	ZeroMemory(szTemp, sizeof(szTemp));

//	if(VK_CONTROL == nChar || VK_MENU == nChar){
//		sprintf(szTemp, "%s", SetKeyText(nChar, szBuff));
//	}
//	else if(VK_LCONTROL <= nChar  && nChar <= VK_RMENU){
//		sprintf(szTemp, "%s", SetKeyText(nChar, szBuff));
//	}
	if(0x30 <= nChar && nChar <= 0x39){
		sprintf(szTemp, "%s", SetKeyText(nChar, szBuff));
	}
	else if(0x41 <= nChar && nChar <= 0x5A){
		sprintf(szTemp, "%s", SetKeyText(nChar, szBuff));
	}
	else if(VK_F1<= nChar && nChar <= VK_F24){
		sprintf(szTemp, "%s", SetKeyText(nChar, szBuff));
	}
	else if(VK_DELETE == nChar || 0x8 == nChar){
		sprintf(szTemp, "");
		nRet = false;
	}
	else{
		sprintf(szTemp, "");
		nRet = false;
	}

	if(nRet){
		if(VK_MENU == nFlags){
			sprintf(szBuff, "ALT+%s", szTemp);
		}
		else if(VK_CONTROL == nFlags){
			sprintf(szBuff, "CTRL+%s", szTemp);
		}
		else{
			sprintf(szBuff, "%s", szTemp);
		}
	}else{
		sprintf(szBuff, "%s", szTemp);
	}
/* 이미 정의된 단축키는 사용 못하도록 해야 함.
{"F1",			"help"},
{"F2",			"debug dialog"},
{"F3",			"contents change"},
{"F5",			"save package to local"},
{"F6",			"reset event count"},
{"F9",			"show/hide play-time dialog"},
{"F12",			"fullscreen/window"},
{"CTRL+Q",	"toggle start/stop browser"},
{"CTRL+M",	"toggle show/hide menu bar button"},
*/
	if(!strcmp(szBuff,"F1")||!strcmp(szBuff,"F2")||!strcmp(szBuff,"F3")||!strcmp(szBuff,"F5")||!strcmp(szBuff,"F6")||
	   !strcmp(szBuff,"F9")||!strcmp(szBuff,"F12")||!strcmp(szBuff,"CTRL+Q")||!strcmp(szBuff,"CTRL+M"))
	{
		UbcMessageBox(LoadStringById(IDS_SHORTCUTEDIT_MSG001), MB_ICONWARNING);
		sprintf(szBuff,"");
		nRet = false;
		BYTE szClearBuf[256];
		ZeroMemory(szClearBuf, sizeof(szClearBuf));
		SetKeyboardState(szClearBuf);
	}
	/*
	else if(CDataContainer::getInstance()->IsUsedShortcut(szBuff)){
		CString szText;
		GetWindowText(szText);
		if(strcmp(szText,szBuff)){
			UbcMessageBox(LoadStringById(IDS_SHORTCUTEDIT_MSG002), MB_ICONWARNING);
			sprintf(szBuff,"");
			nRet = false;
			BYTE szClearBuf[256];
			ZeroMemory(szClearBuf, sizeof(szClearBuf));
			SetKeyboardState(szClearBuf);
		}
	}
	*/
	else{
		SetWindowText(szBuff);
	}

	return nRet;
}

LPCTSTR CShortcutEdit2::SetKeyText(UINT nChar, LPSTR szBuff)
{
	switch(nChar)
	{
	case VK_CONTROL:
	case VK_LCONTROL:
	case VK_RCONTROL:
		sprintf(szBuff, "CTRL");
		break;
	case VK_MENU:
	case VK_LMENU:
	case VK_RMENU:
		sprintf(szBuff, "ALT");
		break;
	case 0x30:		sprintf(szBuff, "0");	break;
	case 0x31:		sprintf(szBuff, "1");	break;
	case 0x32:		sprintf(szBuff, "2");	break;
	case 0x33:		sprintf(szBuff, "3");	break;
	case 0x34:		sprintf(szBuff, "4");	break;
	case 0x35:		sprintf(szBuff, "5");	break;
	case 0x36:		sprintf(szBuff, "6");	break;
	case 0x37:		sprintf(szBuff, "7");	break;
	case 0x38:		sprintf(szBuff, "8");	break;
	case 0x39:		sprintf(szBuff, "9");	break;
	case 0x41:		sprintf(szBuff, "A");	break;
	case 0x42:		sprintf(szBuff, "B");	break;
	case 0x43:		sprintf(szBuff, "C");	break;
	case 0x44:		sprintf(szBuff, "D");	break;
	case 0x45:		sprintf(szBuff, "E");	break;
	case 0x46:		sprintf(szBuff, "F");	break;
	case 0x47:		sprintf(szBuff, "G");	break;
	case 0x48:		sprintf(szBuff, "H");	break;
	case 0x49:		sprintf(szBuff, "I");	break;
	case 0x4A:		sprintf(szBuff, "J");	break;
	case 0x4B:		sprintf(szBuff, "K");	break;
	case 0x4C:		sprintf(szBuff, "L");	break;
	case 0x4D:		sprintf(szBuff, "M");	break;
	case 0x4E:		sprintf(szBuff, "N");	break;
	case 0x4F:		sprintf(szBuff, "O");	break;
	case 0x50:		sprintf(szBuff, "P");	break;
	case 0x51:		sprintf(szBuff, "Q");	break;
	case 0x52:		sprintf(szBuff, "R");	break;
	case 0x53:		sprintf(szBuff, "S");	break;
	case 0x54:		sprintf(szBuff, "T");	break;
	case 0x55:		sprintf(szBuff, "U");	break;
	case 0x56:		sprintf(szBuff, "V");	break;
	case 0x57:		sprintf(szBuff, "W");	break;
	case 0x58:		sprintf(szBuff, "X");	break;
	case 0x59:		sprintf(szBuff, "Y");	break;
	case 0x5A:		sprintf(szBuff, "Z");	break;
	case VK_F1:		sprintf(szBuff, "F1");	break;
	case VK_F2:		sprintf(szBuff, "F2");	break;
	case VK_F3:		sprintf(szBuff, "F3");	break;
	case VK_F4:		sprintf(szBuff, "F4");	break;
	case VK_F5:		sprintf(szBuff, "F5");	break;
	case VK_F6:		sprintf(szBuff, "F6");	break;
	case VK_F7:		sprintf(szBuff, "F7");	break;
	case VK_F8:		sprintf(szBuff, "F8");	break;
	case VK_F9:		sprintf(szBuff, "F9");	break;
	case VK_F10:	sprintf(szBuff, "F10");	break;
	case VK_F11:	sprintf(szBuff, "F11");	break;
	case VK_F12:	sprintf(szBuff, "F12");	break;
	case VK_F13:	sprintf(szBuff, "F13");	break;
	case VK_F14:	sprintf(szBuff, "F14");	break;
	case VK_F15:	sprintf(szBuff, "F15");	break;
	case VK_F16:	sprintf(szBuff, "F16");	break;
	case VK_F17:	sprintf(szBuff, "F17");	break;
	case VK_F18:	sprintf(szBuff, "F18");	break;
	case VK_F19:	sprintf(szBuff, "F19");	break;
	case VK_F20:	sprintf(szBuff, "F20");	break;
	case VK_F21:	sprintf(szBuff, "F21");	break;
	case VK_F22:	sprintf(szBuff, "F22");	break;
	case VK_F23:	sprintf(szBuff, "F23");	break;
	case VK_F24:	sprintf(szBuff, "F24");	break;
	case VK_DELETE:
	case 0x8:
		sprintf(szBuff, "");
		return szBuff;
	default:
		return NULL;
	}
	return szBuff;
}
BOOL CShortcutEdit2::PreTranslateMessage(MSG* pMsg)
{
	if(WM_KEYDOWN == pMsg->message || WM_SYSKEYDOWN == pMsg->message)
	{
		SetKeyValue(pMsg->wParam);
		return TRUE;
	}
	else if(WM_KEYUP == pMsg->message || WM_SYSKEYUP == pMsg->message){
		BYTE key[256];
		ZeroMemory(key, sizeof(key));
		GetKeyboardState(key);
		key[pMsg->wParam] = 0;
		SetKeyboardState(key);
		return TRUE;
	}

	return CEdit::PreTranslateMessage(pMsg);
}

BOOL CShortcutEdit2::OnHelpInfo(HELPINFO* pHelpInfo)
{
	// TODO: Add your message handler code here and/or call default
	return TRUE;
//	return CEdit::OnHelpInfo(pHelpInfo);
}

void CShortcutEdit2::OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	BYTE key[256];
	ZeroMemory(key, sizeof(key));
	GetKeyboardState(key);
	key[nChar] = 0;
	SetKeyboardState(key);

//	CEdit::OnSysKeyUp(nChar, nRepCnt, nFlags);
}

void CShortcutEdit2::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	BYTE key[256];
	ZeroMemory(key, sizeof(key));
	GetKeyboardState(key);
	key[nChar] = 0;
	SetKeyboardState(key);

//	CEdit::OnKeyUp(nChar, nRepCnt, nFlags);
}
