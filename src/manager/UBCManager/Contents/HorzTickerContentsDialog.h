#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "resource.h"

#include "SubContentsDialog.h"
#include "EditEx.h"
#include "ColorPickerCB.h"
#include "common/FontPreviewCombo.h"
#include "ximage.h"
#include "ReposControl.h"
#include "Frame.h"
#include "Schedule.h"
#include "common/HoverButton.h"

// CHorzTickerContentsDialog 대화 상자입니다.

class CHorzTickerContentsDialog : public CSubContentsDialog
{
	DECLARE_DYNAMIC(CHorzTickerContentsDialog)

public:
	CHorzTickerContentsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHorzTickerContentsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HORZ_TICKER_CONTENTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	Mng_Browser::CHorizontalTickerSchedule	m_wndTicker;

	CReposControl				m_reposControl;
	bool						m_bPreviewMode;		///<미리보기 모드
	BOOL						m_bPermanent;		///<24시간 running time

	void	EnableAllControls(BOOL bEnable = TRUE);
	void	SetPlayContents();
	void	MoveItem(int nItem, int nItemNewPos);

public:

	virtual Mng_Browser::CONTENTS_TYPE	GetContentsType() { return Mng_Browser::CONTENTS_SMS; };
	virtual void	Stop();
	virtual bool	GetContentsInfo(CONTENTS_INFO& info);
	virtual bool	SetContentsInfo(CONTENTS_INFO& info);

	afx_msg void OnSize(UINT nType, int cx, int cy);
//	afx_msg void OnLvnEndlabeleditListContentsComment(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnLvnKeydownListContentsComment(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedButtonBrowserContentsFile();
	afx_msg void OnBnClickedButtonContentsCommentAdd();
	afx_msg void OnBnClickedButtonContentsCommentDelete();
	afx_msg void OnBnClickedButtonContentsCommentUp();
	afx_msg void OnBnClickedButtonContentsCommentDown();
	afx_msg void OnBnClickedButtonPreviewPlay();
	afx_msg void OnBnClickedButtonPreviewStop();

	CEdit				m_editContentsName;
	CEdit				m_editContentsFileName;
	CStatic				m_staticContentsWidth;
	CStatic				m_staticContentsHeight;
	CStatic				m_staticContentsFileSize;
	CEditEx				m_editContentsPlayMinute;
	CEditEx				m_editContentsPlaySecond;
	CEditEx				m_editContentsFontSize;
	CFontPreviewCombo	m_cbxContentsFont;
	CColorPickerCB		m_cbxContentsTextColor;
	CColorPickerCB		m_cbxContentsBGColor;
	CSliderCtrl			m_sliderContentsPlaySpeed;
//	CListCtrl			m_listctrlContentsComment;

	CHoverButton		m_btnBrowserContentsFile;
	CHoverButton		m_btnContentsCommentAdd;
	CHoverButton		m_btnContentsCommentDelete;
	CHoverButton		m_btnContentsCommentUp;
	CHoverButton		m_btnContentsCommentDown;
	CHoverButton		m_btnPreviewPlay;
	CHoverButton		m_btnPreviewStop;
	CHoverButton		m_btnDelFile;

	CHoverButton		m_btnZoomIn;
	CHoverButton		m_btnZoomOut;
	CHoverButton		m_btnOriginal;

	CStatic				m_groupPreview;
	CStatic				m_staticContents;

	CString				m_strLocation;

	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void	SetPreviewMode(bool bPreviewMode=true) { m_bPreviewMode = bPreviewMode; };
	afx_msg void OnBnClickedPermanentCheck();
	afx_msg void OnEnChangeEditContentsComment();
	afx_msg void OnBnClickedDeletefile();
	afx_msg void OnBnClickedButtonZoomIn();
	afx_msg void OnBnClickedButtonZoomOut();
	afx_msg void OnBnClickedButtonOriginal();
	CEdit m_editAlpha;
	CSpinButtonCtrl m_spinAlpha;
};





