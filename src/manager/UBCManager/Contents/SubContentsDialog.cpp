// SubContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
//#include "UBCStudio.h"
#include "SubContentsDialog.h"
#include "common/PreventChar.h"
#include "Enviroment.h"

EXT_TYPE ext_type[] = {
	{".avi",	Mng_Browser::CONTENTS_VIDEO},
	{".asf",	Mng_Browser::CONTENTS_VIDEO},
	{".mkv",	Mng_Browser::CONTENTS_VIDEO},
	{".mp4",	Mng_Browser::CONTENTS_VIDEO},
	{".mpg",	Mng_Browser::CONTENTS_VIDEO},
	{".mpeg",	Mng_Browser::CONTENTS_VIDEO},
	{".wmv",	Mng_Browser::CONTENTS_VIDEO},
	{".mp3",	Mng_Browser::CONTENTS_VIDEO},
	{".wma",	Mng_Browser::CONTENTS_VIDEO},
	{".mov",	Mng_Browser::CONTENTS_VIDEO},
	{".tp",		Mng_Browser::CONTENTS_VIDEO},
	{".wav",	Mng_Browser::CONTENTS_VIDEO},
	{".flv",	Mng_Browser::CONTENTS_VIDEO},
	{".bmp",	Mng_Browser::CONTENTS_IMAGE},
	{".jpg",	Mng_Browser::CONTENTS_IMAGE},
	{".jpeg",	Mng_Browser::CONTENTS_IMAGE},
	{".gif",	Mng_Browser::CONTENTS_IMAGE},
	{".pcx",	Mng_Browser::CONTENTS_IMAGE},
	{".png",	Mng_Browser::CONTENTS_IMAGE},
	{".tif",	Mng_Browser::CONTENTS_IMAGE},
	{".tiff",	Mng_Browser::CONTENTS_IMAGE},
	{".swf",	Mng_Browser::CONTENTS_FLASH},
	{".ppt",	Mng_Browser::CONTENTS_PPT},
	{".pps",	Mng_Browser::CONTENTS_PPT},
	{".pptx",	Mng_Browser::CONTENTS_PPT},
	{".ppsx",	Mng_Browser::CONTENTS_PPT},
//	{".hwp",	Mng_Browser::CONTENTS_HWP},
//	{".nxl",	Mng_Browser::CONTENTS_EXCEL},
//	{".xls",	Mng_Browser::CONTENTS_EXCEL},
	{".txt",	Mng_Browser::CONTENTS_VIDEO},	// 텍스트파일을 단말에 내려보내기 위해 동영상콘텐츠로 간주하여 처리하도록 함.
	{".ttc",    Mng_Browser::CONTENTS_ETC},		// 2010.10.12 장광수 기타콘텐츠추가
	{".ttf",    Mng_Browser::CONTENTS_ETC},		// 2010.10.12 장광수 기타콘텐츠추가
	{".fon",    Mng_Browser::CONTENTS_ETC},		// 2010.10.12 장광수 기타콘텐츠추가
	{".tar",    Mng_Browser::CONTENTS_ETC},		// 2010.10.12 장광수 기타콘텐츠추가
	{"",		Mng_Browser::CONTENTS_NOT_DEFINE}
};

// CSubContentsDialog 대화 상자입니다.
IMPLEMENT_DYNAMIC(CSubContentsDialog, CDialog)

CSubContentsDialog::CSubContentsDialog(UINT nIDTemplate, CWnd* pParent /*=NULL*/)
	: CDialog(nIDTemplate, pParent)
	//, m_pDocument(NULL)
	, m_bIsChangedFile(FALSE)	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
{
	m_ulFileSize = 0;
	m_brushBG.CreateSolidBrush(RGB(255,255,255));
}

CSubContentsDialog::~CSubContentsDialog()
{
}

void CSubContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSubContentsDialog, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

// CSubContentsDialog 메시지 처리기입니다.
HBRUSH CSubContentsDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	int count = m_listNoCTLWnd.GetCount();
	for(int i=0; i<count; i++)
	{
		CWnd* wnd = m_listNoCTLWnd.GetAt(i);
		if(wnd->GetSafeHwnd() == pWnd->GetSafeHwnd())
			return hbr;
	}

	pDC->SetBkColor(RGB(255,255,255));
	//pDC->SetBkMode(TRANSPARENT);
	hbr = (HBRUSH)m_brushBG;

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

bool CSubContentsDialog::IsFitContentsName(CString strContentsName, bool bErrMsg/*=true*/)
{
	if(strContentsName.IsEmpty())
	{
		if(bErrMsg) UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG001), MB_ICONSTOP);
		return false;
	}
	if(CPreventChar::GetInstance()->HavePreventChar(strContentsName))
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_CONTENTSDIALOG_MSG006), CPreventChar::GetInstance()->GetPreventChar());
		if(bErrMsg) UbcMessageBox(strMsg, MB_ICONERROR);
		return false;
	}

	return true;
}

bool CSubContentsDialog::IsValidType(CString szExt, Mng_Browser::CONTENTS_TYPE conType)
{
	szExt.MakeLower();

	int nCnt = sizeof(ext_type)/sizeof(EXT_TYPE);

	for(int i = 0; i < nCnt; i++)
	{
		// 창일향 : 콘텐츠 바스켓에 Drag & Drop 으로 밀어넣을 수 있는 콘텐츠는 오직 JPG 파일 뿐이다
		if( CEnviroment::eADASSET == GetEnvPtr()->m_Customer )
		{
			if( _tcsicmp(_T(".jpg") , ext_type[i].extension) != 0 &&
				_tcsicmp(_T(".jpeg"), ext_type[i].extension) != 0 )
			{
				continue;
			}
		}

		if(szExt != ext_type[i].extension)
			continue;

		if(conType == Mng_Browser::CONTENTS_NOT_DEFINE)
		{
			return true;
		}
		else if(conType == ext_type[i].type)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

bool CSubContentsDialog::CheckTotalContentsSize(CString strFilePath)
{
	/*
	if(strFilePath.IsEmpty()) return true;

	ULONGLONG lAddSize = 0;
	ULONGLONG lFileSize = 0;
	if(GetEnvPtr()->GetFileSize(strFilePath, lFileSize))
	{
		lAddSize = lFileSize - m_ulFileSize;
	}

	// 현재 콘텐츠의 변화량이 커지는 경우에만 체크함
	if(lAddSize > 0)
	{
		// 콘텐츠의 총 용량이 지정된 용량을 넘어서는지 체크
		if(!GetDocument()->CheckTotalContentsSize(lAddSize)) return false;
	}
	*/
	return true;
}
/*
CUBCStudioDoc* CSubContentsDialog::GetDocument()
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CUBCStudioDoc)));
	return (CUBCStudioDoc*)m_pDocument;
}
*/

bool
CSubContentsDialog::CopyContents(CString szSPathName, CString filename, CString ext)
{
	TraceLog(("CopyContents(%s,%s)", szSPathName, filename));
	CString szTPathName;
	
	szTPathName.Format("%s\\%s%s\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER);
	::CreateDirectory(szTPathName, NULL);

	szTPathName += filename;
	szTPathName += ext;
	if(::CopyFile(szSPathName, szTPathName, false)){
		TraceLog(("%s --> %s copy succeed", szSPathName, szTPathName));
		return true;
	}
	TraceLog(("ERROR : %s --> %s copy Failed", szSPathName,szTPathName));
	return false;

}

bool			
CSubContentsDialog::GetFile(LPCTSTR lpszFullPath, LPCTSTR annoId, LPCTSTR siteId)
{
	TraceLog(("GetFile(%s)", lpszFullPath));

	char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_splitpath(lpszFullPath, drive, path, filename, ext);

	CString file;
	CString remote;
	CString local;

	file.Format("%s%s",filename,ext);
	remote.Format("\\Contents\\announce\\%s\\", annoId);
	local.Format("%s%s", drive, path);

	TraceLog(("GetFile(%s,%s,%s,%s)", file,remote,local,siteId));

	CString waitMsg;
	waitMsg.Format(LoadStringById(IDS_ANNOUNCE_WAIT001), file);
	CWaitMessageBox wait(waitMsg);

	if(GetEnvPtr()->GetFile(file, remote, local, siteId)){
		TraceLog(("GetFile(%s,%s,%s,%s) succeed", file,remote,local,siteId));
		return true;
	}
	CString szMsg;
	szMsg.Format("Download %s%s  fail", remote,file);
	UbcMessageBox(szMsg, MB_ICONWARNING);
	return false;
}
