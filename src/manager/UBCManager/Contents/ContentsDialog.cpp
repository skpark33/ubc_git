// ContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
//#include "UBCStudio.h"
#include "ContentsDialog.h"
#include "Enviroment.h"
#include "common/UbcGUID.h"

// Modified by 정운형 2009-01-06 오전 11:35
// 변경내역 :  CLI 연동 수정
#include "Dbghelp.h"
// Modified by 정운형 2009-01-06 오전 11:35
// 변경내역 :  CLI 연동 수정

//#include "SubContentsDialog.h"

// CContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CContentsDialog, CDialog)

CContentsDialog::CContentsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CContentsDialog::IDD, pParent)
	, m_reposControl(this)
	// Modified by 정운형 2009-01-15 오전 10:00
	// 변경내역 :  콘텐츠 추가 수정
	, m_bEditMode(false)
	, m_bPreviewMode(false)
	// Modified by 정운형 2009-01-15 오전 10:00
	// 변경내역 :  콘텐츠 추가 수정
	//, m_pDocument(NULL)
	, m_pVideoContents(NULL)
	, m_pImageContents(NULL)
	, m_pHorzTickerContents(NULL)
	, m_pFlashContents(NULL)
	, m_pPPTContents(NULL)
	//, m_pSMSContents(NULL)
	//, m_pWebContents(NULL)
	//, m_pTVContents(NULL)
	//, m_pRSSContents(NULL)
	//, m_pWizardContents(NULL)
{
}

CContentsDialog::~CContentsDialog()
{
	if(m_pVideoContents)
	{
		delete m_pVideoContents;
		m_pVideoContents = NULL;
	}
	
	if(m_pImageContents)
	{
		delete m_pImageContents;
		m_pImageContents = NULL;
	}
	
	if(m_pHorzTickerContents)
	{
		delete m_pHorzTickerContents;
		m_pHorzTickerContents = NULL;
	}

	if(m_pFlashContents)
	{
		delete m_pFlashContents;
		m_pFlashContents = NULL;
	}
	if(m_pPPTContents)
	{
		delete m_pPPTContents;
		m_pPPTContents = NULL;
	}

/*
	if(m_pSMSContents)
	{
		delete m_pSMSContents;
		m_pSMSContents = NULL;
	}

	if(m_pWebContents)
	{
		delete m_pWebContents;
		m_pWebContents = NULL;
	}

	if(m_pTVContents)
	{
		delete m_pTVContents;
		m_pTVContents = NULL;
	}

	if(m_pRSSContents)
	{
		delete m_pRSSContents;
		m_pRSSContents = NULL;
	}

	if(m_pWizardContents)
	{
		delete m_pWizardContents;
		m_pWizardContents = NULL;
	}
*/
}

void CContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_CONTENTS_TYPE, m_tabContentsType);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}

BEGIN_MESSAGE_MAP(CContentsDialog, CDialog)
	ON_WM_GETMINMAXINFO()
	ON_WM_SIZE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_CONTENTS_TYPE, &CContentsDialog::OnTcnSelchangeTabContentsType)
	ON_BN_CLICKED(IDOK, &CContentsDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// CContentsDialog 메시지 처리기입니다.

BOOL CContentsDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	CWaitMessageBox wait;

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	//HICON hIcon = (HICON)LoadImage(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_TAB_CONTENTS), IMAGE_ICON, 0, 0, LR_LOADFROMFILE);
	HICON hIcon = LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_CONTENTS));
	SetIcon(hIcon, TRUE);
	SetIcon(hIcon, FALSE);

	m_arTabTitle.RemoveAll();
	m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR002));
	m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR003));
	m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR004));
	m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR005));
	m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR007));
	//m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR006));
	m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR008));
	//m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR009));
	//m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR010));
	//m_arTabTitle.Add(LoadStringById(IDS_CONTENTSDIALOG_STR011));

	if( m_contentsInfo.bLocalFileExist               &&
		!m_contentsInfo.strFilename.IsEmpty()        &&
		m_contentsInfo.nContentsType==Mng_Browser::CONTENTS_VIDEO )
	{
		CString strExt = m_contentsInfo.strFilename.Mid(m_contentsInfo.strFilename.ReverseFind('.')+1);
		strExt.MakeLower();
		if(strExt == "txt")
		{
			::ShellExecute(GetSafeHwnd(), "open", m_contentsInfo.strLocalLocation + m_contentsInfo.strFilename, NULL, NULL, SW_SHOWNORMAL);
			EndDialog(IDCANCEL);
			return TRUE;
		}
	}

	if( m_contentsInfo.bLocalFileExist             &&
		!m_contentsInfo.strFilename.IsEmpty()      &&
		m_contentsInfo.nContentsType==Mng_Browser::CONTENTS_ETC )
	{
		::ShellExecute(GetSafeHwnd(), "open", m_contentsInfo.strLocalLocation + m_contentsInfo.strFilename, NULL, NULL, SW_SHOWNORMAL);
		EndDialog(IDCANCEL);
		return TRUE;
	}

	if(m_bPreviewMode)
	{
		m_btnCancel.ShowWindow(SW_HIDE);
		m_btnOK.SetWindowText(LoadStringById(IDS_CONTENTSDIALOG_BUT001));
		m_btnOK.LoadBitmap(IDB_BTN_OK, RGB(255, 255, 255));
		m_btnOK.SetToolTipText(LoadStringById(IDS_CONTENTSDIALOG_BUT001));

		this->SetWindowText(LoadStringById(IDS_CONTENTSDIALOG_STR001));
	}
	else
	{
		m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
		m_btnCancel.SetToolTipText(LoadStringById(IDS_CONTENTSDIALOG_BUT002));

		if(m_bEditMode)
		{
			m_btnOK.SetWindowText(LoadStringById(IDS_CONTENTSDIALOG_BUT003));
			m_btnOK.LoadBitmap(IDB_BTN_MODIFY, RGB(255, 255, 255));
			m_btnOK.SetToolTipText(LoadStringById(IDS_CONTENTSDIALOG_BUT003));
		}
		else
		{
			m_btnOK.LoadBitmap(IDB_BTN_ADD, RGB(255, 255, 255));
			m_btnOK.SetToolTipText(LoadStringById(IDS_CONTENTSDIALOG_BUT004));
		}//if
	}//if


	//m_tabContentsType.InitImageListHighColor(IDB_TAB_CONTENTS_LIST, RGB(192,192,192));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	//

	TraceLog(("OnInitDialog(%d)", m_bPreviewMode));

	int index = 0;
	if(m_bPreviewMode)
	{
		if(m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_VIDEO)
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_VIDEO), EICO_VIDEO);
		}
		if(m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_IMAGE)
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_IMAGE), EICO_IMAGE);
		}
		if(m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_FLASH)
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_FLASH), EICO_FLASH);
		}
		if(m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_SMS)
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_TICKER), EICO_TICKER);
		}
		if(m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_WEBBROWSER)
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_WEB), EICO_WEB);
		}
		if(m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_PPT)
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_PPT), EICO_PPT);
		}
		/*
		if(m_contentsInfo.nContentsType == CONTENTS_TEXT)
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_TEXT), EICO_TEXT);
		}
		//if( m_contentsInfo.nContentsType == CONTENTS_SMS  ||
		//	m_contentsInfo.nContentsType == CONTENTS_TEXT )
		//{
		//	m_contentsInfo.nContentsType = CONTENTS_IMAGE;
		//	m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_IMAGE), EICO_IMAGE);
		//}
		if( m_contentsInfo.nContentsType == CONTENTS_TV         &&
		   (CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType || GetEnvPtr()->m_bUseTV) )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICON_TV), EICON_TV);
		}
		if( m_contentsInfo.nContentsType == CONTENTS_RSS )
			//CEnviroment::eStudioEE == GetEnvPtr()->m_Edition ) // 0000760: RSS 를 PE 버전에서도 될 수 있도록
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICON_RSS), EICON_RSS);
		}
		if( m_contentsInfo.nContentsType == CONTENTS_WIZARD )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_WIZARD), EICO_WIZARD);
		}
		*/
	}
	// 창일향 : 생성할 수 있는 Contents Type 은 오직 이미지와 티커 뿐이다.  나머지 탭은 모두 invisible(또는 disable) 하다.
	else if(CEnviroment::eADASSET == GetEnvPtr()->m_Customer)
	{
		if( m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_IMAGE      )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_IMAGE), EICO_IMAGE);
		}
		if( m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_SMS        )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_TICKER), EICO_TICKER);
		}
	}
	else
	{
		if( m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_VIDEO      )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_VIDEO), EICO_VIDEO);
		}
		if( m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_IMAGE      )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_IMAGE), EICO_IMAGE);
		}
		if( m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_SMS        )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_TICKER), EICO_TICKER);
		}
		if( m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_FLASH      )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_FLASH), EICO_FLASH);
		}
		if( m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_WEBBROWSER     )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_WEB), EICO_WEB);
		}
		if( m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == Mng_Browser::CONTENTS_PPT        )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_PPT), EICO_PPT);
		}
		/*
		if( m_contentsInfo.nContentsType == CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == CONTENTS_TEXT       )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_TEXT), EICO_TEXT);
		}
		if( m_contentsInfo.nContentsType == CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == CONTENTS_WEBBROWSER )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_WEB), EICO_WEB);
		}
		if( m_contentsInfo.nContentsType == CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == CONTENTS_TV         )
		{
			if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType || GetEnvPtr()->m_bUseTV)
			{
				m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICON_TV), EICON_TV);
			}
		}
		// 0000760: RSS 를 PE 버전에서도 될 수 있도록
		// if(CEnviroment::eStudioEE == GetEnvPtr()->m_Edition)
		if( m_contentsInfo.nContentsType == CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == CONTENTS_RSS        )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICON_RSS), EICON_RSS);
		}
		if( m_contentsInfo.nContentsType == CONTENTS_NOT_DEFINE ||
			m_contentsInfo.nContentsType == CONTENTS_WIZARD     )
		{
			m_tabContentsType.InsertItem(index++, m_arTabTitle.GetAt(EICO_WIZARD), EICO_WIZARD);
		}
		*/
	}

	CRect rect;
	m_tabContentsType.GetWindowRect(rect);
	ScreenToClient(rect);
	rect.DeflateRect(5,25,5,5);

	TraceLog(("OnInitDialog(%d)", m_tabContentsType.GetItemCount()));


	for(int i=0; i<m_tabContentsType.GetItemCount(); i++)
	{
		TCHAR text[255];
		TCITEM item;
		item.mask = TCIF_TEXT;
		item.pszText = text;
		item.cchTextMax = 255;
		m_tabContentsType.GetItem(i, &item);


		TraceLog(("m_tabContentsType=%s",item.pszText));

		//
		CSubContentsDialog* pSubContentsDlg = NULL;
		if(m_arTabTitle.GetAt(EICO_VIDEO).Compare(item.pszText) == 0)
		{
			m_pVideoContents = new CVideoContentsDialog;
			m_pVideoContents->SetPreviewMode(m_bPreviewMode);
			//m_pVideoContents->SetDocument(m_pDocument);
			m_pVideoContents->Create(IDD_VIDEO_CONTENTS, this);
			pSubContentsDlg = m_pVideoContents;
			TraceLog(("IDD_VIDEO_CONTENTS created"));
		}
		else if(m_arTabTitle.GetAt(EICO_IMAGE).Compare(item.pszText) == 0)
		{
			m_pImageContents = new CImageContentsDialog;
			m_pImageContents->SetPreviewMode(m_bPreviewMode);
			//m_pImageContents->SetDocument(m_pDocument);
			m_pImageContents->Create(IDD_IMAGE_CONTENTS, this);
			pSubContentsDlg = m_pImageContents;
			TraceLog(("IDD_IMAGE_CONTENTS created"));
		}
		else if(m_arTabTitle.GetAt(EICO_TICKER).Compare(item.pszText) == 0)
		{
			m_pHorzTickerContents = new CHorzTickerContentsDialog;
			m_pHorzTickerContents->SetPreviewMode(m_bPreviewMode);
			//m_pHorzTickerContents->SetDocument(m_pDocument);
			m_pHorzTickerContents->Create(IDD_HORZ_TICKER_CONTENTS, this);
			pSubContentsDlg = m_pHorzTickerContents;
			TraceLog(("IDD_TICKER_CONTENTS created"));

		}
		else if(m_arTabTitle.GetAt(EICO_FLASH).Compare(item.pszText) == 0)
		{
			m_pFlashContents = new CFlashContentsDialog;
			m_pFlashContents->SetPreviewMode(m_bPreviewMode);
			//m_pFlashContents->SetDocument(m_pDocument);
			m_pFlashContents->Create(IDD_FLASH_CONTENTS, this);
			pSubContentsDlg = m_pFlashContents;
			TraceLog(("IDD_FLASH_CONTENTS created"));
		}
		else if(m_arTabTitle.GetAt(EICO_WEB).Compare(item.pszText) == 0)
		{
			TraceLog(("IDD_WEB_CONTENTS"));
			m_pWebContents = new CWebContentsDialog;
			m_pWebContents->SetPreviewMode(m_bPreviewMode);
			//m_pWebContents->SetDocument(m_pDocument);
			m_pWebContents->Create(IDD_WEB_CONTENTS, this);
			pSubContentsDlg = m_pWebContents;
			TraceLog(("IDD_WEB_CONTENTS created"));
		}
		else if(m_arTabTitle.GetAt(EICO_PPT).Compare(item.pszText) == 0)
		{
			m_pPPTContents = new CPPTContentsDlg;
			m_pPPTContents->SetPreviewMode(m_bPreviewMode);
			//m_pPPTContents->SetDocument(m_pDocument);
			m_pPPTContents->Create(IDD_POWERPOINT_CONTENTS, this);
			pSubContentsDlg = m_pPPTContents;
			TraceLog(("IDD_POWERPOINT_CONTENTS created"));
		}
		else{
			TraceLog(("Contents create error %s, %s", item.pszText, m_arTabTitle.GetAt(EICO_WEB)));
		
		}
			/*
		else if(m_arTabTitle.GetAt(EICO_TEXT).Compare(item.pszText) == 0)
		{
			m_pSMSContents = new CSMSContentsDialog;
			m_pSMSContents->SetPreviewMode(m_bPreviewMode);
			m_pSMSContents->SetDocument(m_pDocument);
			m_pSMSContents->Create(IDD_SMS_CONTENTS, this);
			pSubContentsDlg = m_pSMSContents;
		}
		else if(m_arTabTitle.GetAt(EICO_WEB).Compare(item.pszText) == 0)
		{
			m_pWebContents = new CWebContentsDialog;
			m_pWebContents->SetPreviewMode(m_bPreviewMode);
			m_pWebContents->SetDocument(m_pDocument);
			m_pWebContents->Create(IDD_WEB_CONTENTS, this);
			pSubContentsDlg = m_pWebContents;
		}
//		else if( strcmp(item.pszText, "Camera") == 0){
//			m_pSMSContents->Create(IDD_WEBCAM_CONTENTS, this);
//			pSubContentsDlg = m_pWebCamContents;
//		}
//		else if( strcmp(item.pszText, "Promotion") == 0){
//			m_pPromotionContents->Create(IDD_PROMOTION_CONTENTS, this);
//			pSubContentsDlg = m_pPromotionContents;
//		}
//		else if( strcmp(item.pszText, "Phone") == 0){
//			m_pPhoneContents->Create(IDD_PHONE_CONTENTS, this);
//			pSubContentsDlg = m_pPhoneContents;
//		}
		else if(m_arTabTitle.GetAt(EICON_TV).Compare(item.pszText) == 0)
		{
			m_pTVContents = new CTVContentsDlg;
			m_pTVContents->SetPreviewMode(m_bPreviewMode);
			m_pTVContents->SetDocument(m_pDocument);
			m_pTVContents->Create(IDD_TV_CONTENTS, this);
			pSubContentsDlg = m_pTVContents;
		}
		else if(m_arTabTitle.GetAt(EICON_RSS).Compare(item.pszText) == 0)
		{
			m_pRSSContents = new CRSSContentsDlg;
			m_pRSSContents->SetPreviewMode(m_bPreviewMode);
			m_pRSSContents->SetDocument(m_pDocument);
			m_pRSSContents->Create(IDD_RSS_CONTENTS, this);
			pSubContentsDlg = m_pRSSContents;
		}
		//else if(m_arTabTitle.GetAt(EICO_WIZARD).Compare(item.pszText) == 0)
		else if(m_arTabTitle.GetAt(EICO_WIZARD).Compare(item.pszText) == 0)
		{
			m_pWizardContents = new CWizardContentsDialog;
			m_pWizardContents->SetPreviewMode(m_bPreviewMode);
			m_pWizardContents->SetDocument(m_pDocument);
			m_pWizardContents->Create(IDD_WIZARD_CONTENTS, this);
			pSubContentsDlg = m_pWizardContents;
		}
		*/

		if(pSubContentsDlg != NULL)
		{
			TraceLog(("Position Control"));
			pSubContentsDlg->MoveWindow(rect);
			pSubContentsDlg->ShowWindow(SW_HIDE);
			m_reposControl.AddControl(pSubContentsDlg, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

			TCITEM item;
			item.mask = TCIF_PARAM;
			item.lParam = (LPARAM)pSubContentsDlg;
			m_tabContentsType.SetItem(i, &item);

			if(pSubContentsDlg->GetContentsType() == m_contentsInfo.nContentsType)
			{
				TraceLog(("SetContentsInfo %d", m_contentsInfo.nContentsType));
				pSubContentsDlg->SetContentsInfo(m_contentsInfo);
				m_tabContentsType.SetCurSel(i);
			}
			TraceLog(("Position Control End"));
		}
	}

	m_reposControl.AddControl(&m_tabContentsType, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnOK, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_reposControl.AddControl(&m_btnCancel, REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);

	m_reposControl.Move();

	TraceLog(("Before ShowSubWindow()"));

	ShowSubWindow();

	TraceLog(("After ShowSubWindow()"));
	

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	TCITEM item;
	item.mask = TCIF_PARAM;

	if(m_tabContentsType.GetItem(m_tabContentsType.GetCurSel(), &item))
	{
		CSubContentsDialog* pSubContentsDlg = (CSubContentsDialog*)item.lParam;

		// Modified by 정운형 2009-01-06 오전 11:33
		// 변경내역 :  CLI 연동 수정
		/*if(pSubContentsDlg->GetContentsInfo(m_contentsInfo))
			CDialog::OnOK();*/
		if(pSubContentsDlg->GetContentsInfo(m_contentsInfo))
		{
			if(m_bEditMode)
			{
				RunCLICmd(m_contentsInfo);
			}//if


			CDialog::OnOK();
		}//if
		// Modified by 정운형 2009-01-06 오전 11:33
		// 변경내역 :  CLI 연동 수정
	}
}

void CContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	CDialog::OnCancel();
}

void CContentsDialog::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMinTrackSize.x = 640;
	lpMMI->ptMinTrackSize.y = 572;
}

void CContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();
}

void CContentsDialog::OnTcnSelchangeTabContentsType(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	ShowSubWindow();
}

void CContentsDialog::ShowSubWindow()
{
	int sel = m_tabContentsType.GetCurSel();

	for(int i=0; i<m_tabContentsType.GetItemCount(); i++)
	{
		TCITEM item;
		item.mask = TCIF_PARAM;
		m_tabContentsType.GetItem(i, &item);

		CSubContentsDialog* pSubContentsDlg = (CSubContentsDialog*)item.lParam;

		//
		if(pSubContentsDlg != NULL)
		{
			if(i == sel)
				pSubContentsDlg->ShowWindow(SW_SHOW);
			else
			{
				pSubContentsDlg->ShowWindow(SW_HIDE);
				pSubContentsDlg->Stop();
			}
		}
	}
}


// Modified by 정운형 2009-01-06 오전 11:35
// 변경내역 :  CLI 연동 수정
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// CLI 연동 명령어를 파일로 작성하여 CLI 명령어를 전달한다 \n
/// @param (CONTENTS_INFO&) info : (in) 변경된 coontens 내용
/////////////////////////////////////////////////////////////////////////////////
/*
void CContentsDialog::RunCLICmd(CONTENTS_INFO& info)
{
	TCHAR szPath[MAX_PATH];
	::ZeroMemory(szPath, MAX_PATH);
	::GetModuleFileName(NULL, szPath, MAX_PATH);
	int length = _tcslen(szPath) - 1;
	while( (length > 0) && (szPath[length] != _T('\\')) )
		szPath[length--] = 0;

	CString strCliPath;
	strCliPath = szPath;
	strCliPath.Append("config\\cli\\");
	MakeSureDirectoryPathExists(strCliPath);

	CFileException ex;
	CFile clsFile;
	CString strPath = szPath;
	strPath.Append("config\\cli\\");
	strPath.Append("contents_modify.cli");
	if(!clsFile.Open(strPath, CFile::modeCreate | CFile::modeWrite | CFile::typeText | CFile::shareDenyNone, &ex))
	{
		TCHAR szError[1024] = { 0x00 };
        ex.GetErrorMessage(szError, 1024);
		TRACE(szError);
		return;
	}//if

	CString strCmd;
	strCmd.Format("set CM=0/Contents=%s attributes=[\
contentsType=%d, \
contentsName=\"%s\", \
filename=\"%s\", \
runningTime=%d, \
comment1=\"%s\", \
comment2=\"%s\", \
comment3=\"%s\", \
comment4=\"%s\", \
comment5=\"%s\", \
comment6=\"%s\", \
comment7=\"%s\", \
comment8=\"%s\", \
comment9=\"%s\", \
comment10=\"%s\", \
bgColor=\"%s\", \
fgColor=\"%s\", \
font=\"%s\", \
fontSize=%d, \
playSpeed=%d, \
soundVolume=%d, \
verifier=skpark]",
				info.id,
				info.nContentsType,
				info.strContentsName,
				info.strFilename,
				//info.strLocation,
				info.nRunningTime,
				info.strComment[0],
				info.strComment[1],
				info.strComment[2],
				info.strComment[3],
				info.strComment[4],
				info.strComment[5],
				info.strComment[6],
				info.strComment[7],
				info.strComment[8],
				info.strComment[9],
				info.strBgColor,
				info.strFgColor,
				info.strFont,
				info.nFontSize,
				info.nPlaySpeed,
				info.nSoundVolume);

	clsFile.Write(strCmd.GetBuffer(strCmd.GetLength()), strCmd.GetLength());
	clsFile.Close();

	CString strExcute;
	strExcute = szPath;
	strExcute.Append("clInvoker.exe");
	strExcute.Append(" -ORBInitRef NameService=corbaloc:iiop:211.232.57.202:14009/NameService \
-ORBInitRef InterfaceRepository=corbaloc:iiop:211.232.57.202:14010/InterfaceRepository \
+noES +noDS +socketproxy +ignore_env +noxlog +project utv1 +post +run ");
	strExcute.Append("CliCmd.cli");
	int nRet = WinExec(strExcute, SW_HIDE);
	if(nRet < 31)
	{
		TRACE("Running CLI fail\r\n");
	}//if
}
*/
void CContentsDialog::RunCLICmd(CONTENTS_INFO& info)
{
#ifndef HP_DEMO

	return;

#else

	TCHAR szPath[MAX_PATH];
	::ZeroMemory(szPath, MAX_PATH);
	::GetModuleFileName(NULL, szPath, MAX_PATH);
	int length = _tcslen(szPath) - 1;
	while( (length > 0) && (szPath[length] != _T('\\')) )
		szPath[length--] = 0;

	CString strCliPath;
	strCliPath = szPath;
	strCliPath.Append("config\\cli\\");
	MakeSureDirectoryPathExists(strCliPath);

	FILE* pFile;
	CString strPath = szPath;
	strPath.Append("config\\cli\\");
	CString strFileName;
	DWORD dwTick = GetTickCount();
	strFileName.Format("contents_modify_%s_%3d.cli", info.id, dwTick);
	strPath.Append(strFileName);
	pFile = fopen(strPath.GetBuffer(strPath.GetLength()), "w");
	if(pFile == NULL)
	{
		CString strError;
		strError.Format("CLI file create fail : %s\r\n", strPath);
		TRACE(strError);
		return;
	}//if

	CString strCmd;
	strCmd.Format("set CM=0/Contents=%s attributes=[\
contentsType=%d, \
contentsName=\"%s\", \
filename=\"%s\", \
runningTime=%d, \
comment1=\"%s\", \
comment2=\"%s\", \
comment3=\"%s\", \
comment4=\"%s\", \
comment5=\"%s\", \
comment6=\"%s\", \
comment7=\"%s\", \
comment8=\"%s\", \
comment9=\"%s\", \
comment10=\"%s\", \
bgColor=\"%s\", \
fgColor=\"%s\", \
font=\"%s\", \
fontSize=%d, \
playSpeed=%d, \
soundVolume=%d, \
verifier=skpark]",
				info.id,
				info.nContentsType,
				info.strContentsName,
				info.strFilename,
				//info.strLocation,
				info.nRunningTime,
				info.strComment[0],
				info.strComment[1],
				info.strComment[2],
				info.strComment[3],
				info.strComment[4],
				info.strComment[5],
				info.strComment[6],
				info.strComment[7],
				info.strComment[8],
				info.strComment[9],
				info.strBgColor,
				info.strFgColor,
				info.strFont,
				info.nFontSize,
				info.nPlaySpeed,
				info.nSoundVolume);

	strCmd.Append("\n");

	fwrite(strCmd.GetBuffer(strCmd.GetLength()), sizeof(char), strCmd.GetLength(), pFile); 
	fclose(pFile);
#ifndef _DEBUG
	CString strExcute;
	strExcute = szPath;
	strExcute.Append("clInvoker.exe");
	strExcute.Append(" -ORBInitRef NameService=corbaloc:iiop:211.232.57.202:14009/NameService \
-ORBInitRef InterfaceRepository=corbaloc:iiop:211.232.57.202:14010/InterfaceRepository \
+noES +noDS +socketproxy +ignore_env +noxlog +project utv1 +post +run ");
	strExcute.Append(strFileName);
	int nRet = WinExec(strExcute, SW_HIDE);
	if(nRet < 31)
	{
		TRACE("Running CLI fail\r\n");
	}//if
#endif

#endif
}
// Modified by 정운형 2009-01-06 오전 11:35
// 변경내역 :  CLI 연동 수정


void CContentsDialog::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}
