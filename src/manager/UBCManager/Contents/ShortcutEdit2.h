#pragma once


// CShortcutEdit2

class CShortcutEdit2 : public CEdit
{
	DECLARE_DYNAMIC(CShortcutEdit2)

public:
	CShortcutEdit2();
	virtual ~CShortcutEdit2();

	void SetKeyValue(CString);
	void SetKeyValue(UINT nChar, UINT nFlag);
	void GetKeyValue(UINT& nChar, UINT& nFlag);

protected:
	UINT   m_nChar;
	UINT   m_nFlag;

	void SetKeyValue(UINT nChar);
	bool SetKeyText(UINT nChar, UINT);
	LPCTSTR SetKeyText(UINT nChar, LPSTR szBuff);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnSysKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
};


