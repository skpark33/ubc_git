// FlashContentsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"

//#include "UBCStudio.h"
#include "FlashContentsDialog.h"
#include "Enviroment.h"
//#include "SubPlayContentsDialog.h"
#include "common/PreventChar.h"
#include "common/UbcGUID.h"
#include "common/MD5Util.h"
#include <IMAGEHLP.H>
#pragma comment(lib, "imagehlp.lib")

#ifdef _UBCSTUDIO_EE_
#include "ubccopcommon\ftpmultisite.h"
#endif//_UBCSTUDIO_EE_

// CFlashContentsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFlashContentsDialog, CSubContentsDialog)

CFlashContentsDialog::CFlashContentsDialog(CWnd* pParent /*=NULL*/)
	: CSubContentsDialog(CFlashContentsDialog::IDD, pParent)
	, m_wndFlash (NULL, 0, false)
	, m_reposControl (this)
	, m_strLocation("")
	, m_bPreviewMode(false)
	, m_bPermanent(FALSE)
{
	m_ulFileSize = 0;
	m_brushBG.CreateSolidBrush(RGB(255,255,255));
}

CFlashContentsDialog::~CFlashContentsDialog()
{
}

void CFlashContentsDialog::DoDataExchange(CDataExchange* pDX)
{
	CSubContentsDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_NAME, m_editContentsName);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_FILE_NAME, m_editContentsFileName);
	DDX_Control(pDX, IDC_BUTTON_BROWSER_CONTENTS_FILE, m_btnBrowseContentsFile);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_MINUTE, m_editContentsPlayMinute);
	DDX_Control(pDX, IDC_EDIT_CONTENTS_PLAY_SECOND, m_editContentsPlaySecond);
	DDX_Control(pDX, IDC_STATIC_CONTENTS_FILE_SIZE, m_staticContentsFileSize);
	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_groupPreview);
	DDX_Control(pDX, IDC_STATIC_CONTENTS, m_staticContents);
	DDX_Check(pDX, IDC_PERMANENT_CHECK, m_bPermanent);
	//DDX_Control(pDX, IDC_EDIT_FELICA_URL, m_editFelicaUrl);
	//DDX_Control(pDX, IDC_LIST_FILES, m_lcFiles);
	//DDX_Control(pDX, IDC_TREE_FILES, m_treeFiles);
	//DDX_Control(pDX, IDC_BUTTON_BROWSER_ADD_FOLDER, m_btnBrowseAddFolder);
	//DDX_Control(pDX, IDC_BUTTON_BROWSER_DEL_FOLDER, m_btnBrowseDelFolder);
	//DDX_Control(pDX, IDC_BUTTON_DOWNLOAD_FOLDER, m_btnDownloadFolder);
	//DDX_Control(pDX, IDC_BUTTON_BROWSER_ADD_FILE, m_btnBrowseAddFile);
	//DDX_Control(pDX, IDC_BUTTON_BROWSER_DEL_FILE, m_btnBrowseDelFile);
	//DDX_Control(pDX, IDC_BUTTON_DOWNLOAD_CONTENTS, m_btnDownloadContents);
}


BEGIN_MESSAGE_MAP(CFlashContentsDialog, CSubContentsDialog)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_BROWSER_CONTENTS_FILE, &CFlashContentsDialog::OnBnClickedButtonBrowserContentsFile)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_PERMANENT_CHECK, &CFlashContentsDialog::OnBnClickedPermanentCheck)
//	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_FILES, &CFlashContentsDialog::OnTvnSelchangedTreeFiles)
//	ON_BN_CLICKED(IDC_BUTTON_BROWSER_ADD_FOLDER, &CFlashContentsDialog::OnBnClickedButtonBrowserAddFolder)
//	ON_BN_CLICKED(IDC_BUTTON_BROWSER_DEL_FOLDER, &CFlashContentsDialog::OnBnClickedButtonBrowserDelFolder)
//	ON_BN_CLICKED(IDC_BUTTON_BROWSER_ADD_FILE, &CFlashContentsDialog::OnBnClickedButtonBrowserAddFile)
//	ON_BN_CLICKED(IDC_BUTTON_BROWSER_DEL_FILE, &CFlashContentsDialog::OnBnClickedButtonBrowserDelFile)
//	ON_BN_CLICKED(IDC_BUTTON_DOWNLOAD_FOLDER, &CFlashContentsDialog::OnBnClickedButtonDownloadFolder)
//	ON_BN_CLICKED(IDC_BUTTON_DOWNLOAD_CONTENTS, &CFlashContentsDialog::OnBnClickedButtonDownloadContents)
//	ON_NOTIFY(NM_DBLCLK, IDC_LIST_FILES, &CFlashContentsDialog::OnNMDblclkListFiles)
//	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_FILES, &CFlashContentsDialog::OnLvnKeydownListFiles)
//	ON_NOTIFY(TVN_KEYDOWN, IDC_TREE_FILES, &CFlashContentsDialog::OnTvnKeydownTreeFiles)
END_MESSAGE_MAP()


// CFlashContentsDialog 메시지 처리기입니다.

BOOL CFlashContentsDialog::OnInitDialog()
{
	CSubContentsDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업
	m_btnBrowseContentsFile.LoadBitmap(IDB_BTN_OPEN, RGB(236, 233, 216));
	m_btnBrowseContentsFile.SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN001));
	// Modified by 정운형 2009-01-22 오후 1:34
	// 변경내역 :  이미지 추가 작업

	//m_btnBrowseAddFolder.LoadBitmap(IDB_BTN_PLUS , RGB(255, 255, 255));
	//m_btnBrowseDelFolder.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	//m_btnBrowseAddFile  .LoadBitmap(IDB_BTN_PLUS , RGB(255, 255, 255));
	//m_btnBrowseDelFile  .LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));

	//m_btnBrowseAddFolder.SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN002));
	//m_btnBrowseDelFolder.SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN003));
	//m_btnBrowseAddFile  .SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN004));
	//m_btnBrowseDelFile  .SetToolTipText(LoadStringById(IDS_FLASHCONTENTSDIALOG_BTN005));



	m_editContentsPlayMinute.SetValue(0);
	m_editContentsPlaySecond.SetValue(0);

	//
	CRect client_rect;
	m_staticContents.GetWindowRect(client_rect);
	ScreenToClient(client_rect);
	client_rect.DeflateRect(1,1);

	m_wndFlash.Create(NULL, "", WS_CHILD, client_rect, this, 0xfeff);
	m_wndFlash.ShowWindow(SW_SHOW);

	m_reposControl.AddControl(&m_groupPreview, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_staticContents, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_reposControl.AddControl(&m_wndFlash, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_reposControl.Move();

	//
	m_listNoCTLWnd.Add((CWnd*)&m_staticContentsFileSize);

	//InitSubFilesCtrl();

	m_editContentsName.EnableWindow(false);

	if(m_bPreviewMode)
	{
	}//if
	EnableAllControls(TRUE);

	// 부속파일처리 관련 수정
	CONTENTS_INFO info;
	SetContentsInfo(info);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFlashContentsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnOK();
}

void CFlashContentsDialog::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CSubContentsDialog::OnCancel();
	GetParent()->PostMessage(WM_CLOSE);
}

void CFlashContentsDialog::Stop()
{
	//m_wndFlash.Stop(false);
}

bool CFlashContentsDialog::GetContentsInfo(CONTENTS_INFO& info)
{
	UpdateData(TRUE);

	// 부속파일처리 관련 수정
	info.strId = m_strContentsId;

	info.nContentsType = GetContentsType();
	m_editContentsName.GetWindowText(info.strContentsName);
	//m_editContentsFileName.GetWindowText(info.strLocation);
	info.strLocalLocation = m_strLocation;
	info.nRunningTime = m_editContentsPlayMinute.GetValueInt()*60 + m_editContentsPlaySecond.GetValueInt();

	//
	char dir[MAX_PATH]="", path[MAX_PATH]="", filename[MAX_PATH]="", ext[MAX_PATH]="";
	_splitpath(info.strLocalLocation, dir, path, filename, ext);

	info.strLocalLocation.Format("%s%s", dir, path); 
	info.strFilename.Format("%s%s", filename, ext);
	info.nFilesize = m_ulFileSize;
	info.strFileMD5 = m_strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.
	info.bIsFileChanged = this->m_bIsChangedFile;

	//
	if(!IsFitContentsName(info.strContentsName)) return false;
	if( info.strLocalLocation.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG002), MB_ICONSTOP);
		return false;
	}
	if( info.strFilename.GetLength() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG003), MB_ICONSTOP);
		return false;
	}
	//if(info.nRunningTime == 0)
	//{
	//	UbcMessageBox(LoadStringById(IDS_CONTENTSDIALOG_MSG004), MB_ICONSTOP);
	//	return false;
	//}


	/*
	CUBCStudioDoc *pDoc = GetDocument();
	if(!pDoc) return false;

	CONTENTS_INFO_MAP* contents_map = pDoc->GetContentsMap();
	POSITION pos = contents_map->GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		contents_map->GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		if(pContentsInfo->nContentsType != CONTENTS_FILE) continue;
		if(pContentsInfo->strParentId != m_strContentsId) continue;

		contents_map->RemoveKey(strFileKey);
		delete pContentsInfo;
	}

	pos = m_mapSubFilesContents.GetStartPosition();
	while(pos != NULL)
	{
		CString strFileKey;
		CONTENTS_INFO* pContentsInfo;
		m_mapSubFilesContents.GetNextAssoc( pos, strFileKey, (void*&)pContentsInfo );

		if(!pContentsInfo) continue;
		contents_map->SetAt(strFileKey, (void*&)pContentsInfo);

		// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제
		// 서버에 등록된 컨텐츠인경우 파일이 변경되면 ID를 변경하도록 한다.
		if( m_bIsChangedFile &&
		   !pContentsInfo->strServerLocation.IsEmpty() &&
			pContentsInfo->strServerLocation.MakeLower().CompareNoCase("\\contents\\enc\\") < 0)
		{
			pContentsInfo->strId = CUbcGUID::GetInstance()->ToGUID(_T(""));
		}
	}
	*/


	return true;
}

bool CFlashContentsDialog::SetContentsInfo(CONTENTS_INFO& info)
{
	// 부속파일처리 관련 수정
	m_strContentsId = info.strId;
	if(m_strContentsId.IsEmpty()) m_strContentsId = CUbcGUID::GetInstance()->ToGUID(_T(""));

	m_editContentsName.SetWindowText(info.strContentsName);
	//m_editContentsFileName.SetWindowText(info.strLocation + info.strFilename);
	m_editContentsFileName.SetWindowText(info.strFilename);
	m_strLocation = info.strLocalLocation + info.strFilename;
	m_ulFileSize = info.nFilesize;
	m_strFileMD5 = info.strFileMD5;	// file 진위여부를 체크하기 위해 MD5를 사용한다.

	if(info.nRunningTime >= 1440)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");
		m_bPermanent = TRUE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(TRUE);
		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText(::ToString(info.nRunningTime / 60));
		m_editContentsPlaySecond.SetWindowText(::ToString(info.nRunningTime % 60));
		m_bPermanent = FALSE;
		CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
		pBtn->SetCheck(FALSE);
	}//if

	UpdateData(FALSE);

	if(!info.strLocalLocation.IsEmpty() && !info.strFilename.IsEmpty()){
		LoadContents(info.strLocalLocation + info.strFilename, info.nFilesize, info.strId, info.strSiteId);
	}
	this->m_bIsChangedFile = false;

	return true;
}

HBRUSH CFlashContentsDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CSubContentsDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	if(	pWnd->GetSafeHwnd() != m_staticContentsFileSize.GetSafeHwnd())
	{
		pDC->SetBkColor(RGB(255,255,255));
		//pDC->SetBkMode(TRANSPARENT);

		hbr = (HBRUSH)m_brushBG;
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CFlashContentsDialog::OnSize(UINT nType, int cx, int cy)
{
	CSubContentsDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_reposControl.Move();

	if(m_wndFlash.GetSafeHwnd() && m_wndFlash.m_pFlash != NULL && m_wndFlash.m_pFlash->GetSafeHwnd() )
	{
		CRect rect;
		m_wndFlash.GetClientRect(rect);
		m_wndFlash.m_pFlash->MoveWindow(rect);
	}
}

void CFlashContentsDialog::OnBnClickedButtonBrowserContentsFile()
{
	CString filter;
	CString szFileExts = "*.swf";
	filter.Format("Flash Files (%s)|%s||",szFileExts,szFileExts);

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정
	//CFileDialog dlg(TRUE, _T(""), filename, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, filter, this);
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);
	// Modified by 정운형 2008-12-30 오전 10:57
	// 변경내역 :  PlayContents 추가부분 수정

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	if(!CSubContentsDialog::IsValidType(szExt, Mng_Browser::CONTENTS_FLASH)){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CONTENTSLISTCTRL_MSG002), dlg.GetFileName());
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!CheckTotalContentsSize(dlg.GetPathName())) return;

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	char drive[MAX_PATH], path[MAX_PATH], filename[MAX_PATH], ext[MAX_PATH];
	_splitpath(dlg.GetPathName(), drive, path, filename, ext);

	{
		CWaitMessageBox wait;

		if(!CopyContents(dlg.GetPathName(), filename, ext)){
			CString errMsg ;
			errMsg.Format("Copy %s%s%s file failed", dlg.GetPathName(), filename, ext);
			UbcMessageBox(errMsg);
			return;
		}

		ULONGLONG bufFileSize = LoadContents(dlg.GetPathName());
		if(!bufFileSize){
			return ;
		}
	}
	//m_editContentsFileName.SetWindowText(dlg.GetPathName());
	m_editContentsFileName.SetWindowText(dlg.GetFileName());
	m_strLocation = dlg.GetPathName();
	m_editContentsFileName.SetSel(0, -1);

	m_editContentsPlaySecond.SetWindowText("15");

	CString contents_name;
	m_editContentsName.GetWindowText(contents_name);
	if(contents_name.GetLength() == 0)
	{
		m_editContentsName.SetWindowText(filename);
	}

	// 0001471: 다른 이름으로 저장된 패키지에서 컨텐츠 파일 변경시, 원본 패키지의 파일까지 변경되는 문제

	// 1 MByte 이상이면 파일 크기만 비교하고, 이하이면 MD5로 파일의 진위여부를 파악한다.
	char szMd5[16*2+1] = {0};
	if(m_ulFileSize < 1000000)
	{
		CWaitMessageBox wait;
		CMD5Util::GetInstance()->MDFile((char*)(LPCTSTR)dlg.GetPathName(), szMd5);
	}
	m_strFileMD5 = CString(szMd5);

	m_bIsChangedFile = TRUE;
}

ULONGLONG CFlashContentsDialog::LoadContents(LPCTSTR lpszFullPath, ULONGLONG infoFileSize/*=0*/, LPCTSTR annoId/*=""*/, LPCTSTR siteId/*=""*/)
{
	if(strlen(lpszFullPath) == 0)
	{
		TraceLog(("NO file specified"));
		return 0;
	}

	bool fileShouldBeDownloaded = false;

	CFileStatus fs;
	if(!CEnviroment::GetFileSize(lpszFullPath, fs.m_size))
	{
		// 로컬에 파일이 아예 없다.
		if(infoFileSize==0)
		{
			CString errMsg;
			errMsg.Format("%s(%s)", LoadStringById(IDS_VIDEOCONTENTSDIALOG_MSG001), lpszFullPath);
			UbcMessageBox(errMsg, MB_ICONSTOP);
			return 0;
		}
		fileShouldBeDownloaded = true;
	}
	else 
	{
		// 파일은 있지만, 사이즈가 다르다.
		if(infoFileSize > 0 && fs.m_size != infoFileSize)
		{
			fileShouldBeDownloaded = true;
		}
	}

	if(fileShouldBeDownloaded){
		TraceLog(("%s filed should be downloaded", lpszFullPath));
		GetFile(lpszFullPath, annoId, siteId);
	}

	m_ulFileSize = fs.m_size;
	m_staticContentsFileSize.SetWindowText(::ToMoneyTypeString((ULONGLONG)(fs.m_size/1024)) + " ");

	m_wndFlash.Stop(false);
	m_wndFlash.m_strMediaFullPath = lpszFullPath;
	m_wndFlash.OpenFile(1);
	m_wndFlash.Play();

	return fs.m_size;

}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨트롤의 사용가능여부 설정 \n
/// @param (BOOL) bEnable : (in) TRUE : 사용가능, FALSE : 사용 불가
/////////////////////////////////////////////////////////////////////////////////
void CFlashContentsDialog::EnableAllControls(BOOL bEnable)
{
	m_editContentsName.EnableWindow(bEnable);
	m_editContentsFileName.EnableWindow(bEnable);
	m_editContentsPlayMinute.EnableWindow(bEnable);
	m_editContentsPlaySecond.EnableWindow(bEnable);
	m_btnBrowseContentsFile.EnableWindow(bEnable);
	CButton* pBtn = (CButton*)GetDlgItem(IDC_PERMANENT_CHECK);
	pBtn->EnableWindow(bEnable);
	//m_editFelicaUrl.EnableWindow(bEnable);

	//m_btnBrowseAddFolder.EnableWindow(bEnable);
	//m_btnBrowseDelFolder.EnableWindow(bEnable);
//	m_btnDownloadFolder .EnableWindow(bEnable);
	//m_btnBrowseAddFile  .EnableWindow(bEnable);
	//m_btnBrowseDelFile  .EnableWindow(bEnable);
//	m_btnDownloadContents.EnableWindow(bEnable);
}

void CFlashContentsDialog::OnBnClickedPermanentCheck()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//24시간을 설정
	m_bPermanent = (m_bPermanent == TRUE ? FALSE : TRUE);

	if(m_bPermanent)
	{
		m_editContentsPlayMinute.SetWindowText("1440");
		m_editContentsPlaySecond.SetWindowText("00");

		m_editContentsPlayMinute.EnableWindow(FALSE);
		m_editContentsPlaySecond.EnableWindow(FALSE);
	}
	else
	{
		m_editContentsPlayMinute.SetWindowText("");
		m_editContentsPlaySecond.SetWindowText("15");

		m_editContentsPlayMinute.EnableWindow(TRUE);
		m_editContentsPlaySecond.EnableWindow(TRUE);
	}//if
}

BOOL CFlashContentsDialog::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == m_editContentsName.GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

