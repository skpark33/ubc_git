// CacheServerListDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Enviroment.h"
#include "ubccopcommon\copmodule.h"
#include "ubccopcommon\UBCCopCommon.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "ubccopcommon\MultiSelectHost.h"
#include "CacheServerSelectDlg.h"
#include "HostListDlg.h"
#include "HostDetailDlg.h"
#include "common\TraceLog.h"


#define COLOR_LILAC			RGB(182,151,253)

// CCacheServerSelectDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCacheServerSelectDlg, CDialog)

CCacheServerSelectDlg::CCacheServerSelectDlg(LPCTSTR szCustomer, CWnd* pParent /*=NULL*/)
	: CDialog(CCacheServerSelectDlg::IDD, pParent)
	, m_strCustomer ( szCustomer )
	, m_bEditMode (false)
	, m_currentCacheServer("")
	, m_currentHostID("")
{

}

CCacheServerSelectDlg::~CCacheServerSelectDlg()
{
}

void CCacheServerSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FILTER_IP, m_editIP);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcHostList);
	DDX_Control(pDX, IDC_EDIT_CACHE_HOST, m_editSelectedHost);
	DDX_Control(pDX, IDC_BUTTON_HOST_REFRESH, m_btRefresh);
	DDX_Control(pDX, IDC_CB_OPERATION, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_HOST_ADD, m_btAdd);
	DDX_Control(pDX, IDC_BUTTON_HOST_DEL, m_btDel);
	DDX_Control(pDX, IDOK, m_btOK);
	DDX_Control(pDX, IDC_EDIT_FILTER_HOST, m_editFilterHost);
	DDX_Control(pDX, IDC_EDIT_FILTER_HOSTNAME, m_editFilterHostName);
}


BEGIN_MESSAGE_MAP(CCacheServerSelectDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCacheServerSelectDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCacheServerSelectDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_HOST_REFRESH, &CCacheServerSelectDlg::OnBnClickedButtonHostRefresh)
	ON_NOTIFY(NM_CLICK, IDC_LIST_HOST, &CCacheServerSelectDlg::OnNMClickListHost)
	ON_BN_CLICKED(IDC_BUTTON_HOST_ADD, &CCacheServerSelectDlg::OnBnClickedButtonHostAdd)
	ON_BN_CLICKED(IDC_BUTTON_HOST_DEL, &CCacheServerSelectDlg::OnBnClickedButtonHostDel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOST, &CCacheServerSelectDlg::OnNMDblclkListHost)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_HOST, &CCacheServerSelectDlg::OnNMRclickListHost)
	ON_COMMAND(ID_CACHELISTMENU_CONNECTED, &CCacheServerSelectDlg::OnCachelistmenuConnected)
END_MESSAGE_MAP()


// CCacheServerSelectDlg 메시지 처리기입니다.
BOOL CCacheServerSelectDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_btRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btAdd.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btDel.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));

	m_btRefresh.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN004));
	m_btAdd.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN001));
	m_btDel.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN002));

	m_btAdd.EnableWindow(IsAuth(_T("HDEL")));
	m_btDel.EnableWindow(IsAuth(_T("HDEL")));

	m_cbOperation.AddString("All");
	m_cbOperation.AddString(LoadStringById(IDS_HOSTVIEW_LIST004));
	m_cbOperation.AddString(LoadStringById(IDS_HOSTVIEW_LIST003));
	m_cbOperation.SetCurSel(0);

	if(m_bEditMode){
		m_btOK.EnableWindow(true);
	}else{
		m_btOK.EnableWindow(false);
	}

	if(this->m_bEditMode){
		this->SetWindowText(LoadStringById(IDS_CACHESERVER_STR001));
	}

	InitHostList();
	RefreshHostList();

	// 현재 hostId값을 select 된 채로 시작한다.
	if(!this->m_currentHostID.IsEmpty()){
		int nRow = 0;
		for(; nRow < m_lcHostList.GetItemCount(); nRow++)
		{
			POSITION pos = (POSITION)m_lcHostList.GetItemData(nRow);
			if(!pos) continue;

			SHostInfo Info = m_lsInfoList.GetAt(pos);

			if(Info.hostId == this->m_currentHostID){
				COLORREF crTextBk = COLOR_LILAC;
				m_lcHostList.SetRowColor(nRow, crTextBk, m_lcHostList.GetTextColor());

				CString hostName = Info.hostName;
				hostName += "(";
				hostName += Info.hostId;
				hostName += "), ";
				hostName += Info.ipAddress;

				m_currentCacheServer = Info.ipAddress;
				m_currentCacheServer += ":8080";

				m_editSelectedHost.SetWindowText(hostName);
				TraceLog(("m_currentHostID=%s",this->m_currentHostID));

				break;
			}
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCacheServerSelectDlg::InitHostList()
{
	m_lcHostList.SetExtendedStyle(m_lcHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eHostName] = LoadStringById(IDS_HOSTVIEW_LIST009);
	m_szColum[eHostID] = LoadStringById(IDS_HOSTVIEW_LIST008);
	m_szColum[eGroup ] = LoadStringById(IDS_HOSTVIEW_LIST007);
	m_szColum[eHostIP ] = LoadStringById(IDS_HOSTVIEW_LIST016);
	//m_szColum[eSlaveCounter ] = "# of Connected Host";

	m_lcHostList.InsertColumn(eHostName , m_szColum[eHostName ], LVCFMT_LEFT  , 200);
	m_lcHostList.InsertColumn(eHostID , m_szColum[eHostID ], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eGroup  , m_szColum[eGroup  ], LVCFMT_LEFT  ,  100);
	m_lcHostList.InsertColumn(eHostIP  , m_szColum[eHostIP  ], LVCFMT_LEFT  , 100);
	//m_lcHostList.InsertColumn(eSlaveCounter  , m_szColum[eSlaveCounter  ], LVCFMT_LEFT  , 200);

	// 제일 마지막에 할것...
	m_lcHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcHostList.SetSortEnable(false);
//	m_lcHostList.SetFixedCol(eCheck);
}

void CCacheServerSelectDlg::RefreshHostList()
{
	CWaitMessageBox wait;

	CStringArray filter_list;
	CString strTmp;

	//
	CString strIPAddress;
	m_editIP.GetWindowText(strIPAddress);
	if(!strIPAddress.IsEmpty())
	{
		strTmp.Format(_T("ipAddress like '%%%s%%'"), strIPAddress);
		filter_list.Add(strTmp);
	}

	if(m_cbOperation.GetCurSel())
	{
		strTmp.Format(_T("operationalState = %d"), (m_cbOperation.GetCurSel()==1)? 1:0);
		filter_list.Add(strTmp);
	}

	CString strHost;
	m_editFilterHost.GetWindowText(strHost);
	if(!strHost.IsEmpty())
	{
		strTmp.Format(_T("hostId like '%%%s%%'"), strHost);
		filter_list.Add(strTmp);
	}

	CString strHostName;
	m_editFilterHostName.GetWindowText(strHostName);
	if(!strHostName.IsEmpty())
	{
		strTmp.Format(_T("hostName like '%%%s%%'"), strHostName);
		filter_list.Add(strTmp);
	}

	CString strWhere = "domainName = 'localhost'";
	int count = filter_list.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str = filter_list.GetAt(i);

		if(strWhere.GetLength() > 0)
		{
			strWhere += " and ";
		}

		strWhere += "( ";
		strWhere += str;
		strWhere += ")";
	}

	m_lsInfoList.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetHostForList(&aCall
														, (CCopModule::eSiteAdmin == m_nAuthority ? _T("*") : m_strSiteId)
														, _T("*")
														, strWhere
														);

	if(bRet)
	{
		CCopModule::GetObject()->GetHostData(&aCall
											, m_lsInfoList
											, 0
											);
	}

	m_lcHostList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsInfoList.GetNext(pos);

		m_lcHostList.InsertItem(nRow, "");
		m_lcHostList.SetItemText(nRow, eHostName, info.hostName);
		m_lcHostList.SetItemText(nRow, eHostID, info.hostId);
		m_lcHostList.SetItemText(nRow, eGroup , info.siteId);
		m_lcHostList.SetItemText(nRow, eHostIP , info.ipAddress);
	
		m_lcHostList.SetItemData(nRow, (DWORD_PTR)posOld);

		TraceLog(("info.ipAddress=%s", info.ipAddress));
		nRow++;
	}

	
}


void CCacheServerSelectDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CCacheServerSelectDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CCacheServerSelectDlg::OnBnClickedButtonHostRefresh()
{
	RefreshHostList();
}

void CCacheServerSelectDlg::OnNMClickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	TraceLog(("OnNMClickListHost=%d",pNMLV->iItem));

	CString strHostName = m_lcHostList.GetItemText(pNMLV->iItem, eHostName);
	CString strHostId = m_lcHostList.GetItemText(pNMLV->iItem, eHostID);
	CString strHostIP = m_lcHostList.GetItemText(pNMLV->iItem, eHostIP);

	CString hostName = strHostName;
	hostName += "(";
	hostName += strHostId;
	hostName += "), ";
	hostName += strHostIP;

	m_currentCacheServer = strHostIP;
	m_currentCacheServer += ":8080";

	m_editSelectedHost.SetWindowText(hostName);
	TraceLog(("OnNMClickListHost=%s",hostName));
	*pResult = 0;
}

void CCacheServerSelectDlg::OnBnClickedButtonHostAdd()
{
	CMultiSelectHost dlg(GetEnvPtr()->m_strCustomer);

	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	//dlg.SetTitle(LoadStringById(IDS_PLANOVIEW_STR007));

	if(dlg.DoModal() != IDOK) return;

	CDirtyFlag aInfoDirty;
	aInfoDirty.SetDirty("domainName", true);

	int nRow=0;
	for(int i=0; i<dlg.m_astrSelHostList.GetCount() ;i++)
	{
		CString objectId = dlg.m_astrSelHostList[i];
		TraceLog(("Selected HostId = %s", objectId));

		
		SHostInfo Info;
		Info.siteId = dlg.m_astrSelSiteIdList[i];
		Info.hostId =  objectId;
		Info.domainName = "localhost";

		if(CCopModule::GetObject()->SetHost(Info,GetEnvPtr()->m_szLoginID,&aInfoDirty)){
			nRow++;
		}else{
			CString errMsg;
			errMsg.Format("%s add as CacheServer failed", objectId);  
			TraceLog((errMsg));
			UbcMessageBox(errMsg);
		}
		
	}
	RefreshHostList();
}

void CCacheServerSelectDlg::OnBnClickedButtonHostDel()
{
	bool bCheck = false;
	for(int nRow = 0; nRow < m_lcHostList.GetItemCount(); nRow++)
	{
		if(m_lcHostList.GetCheck(nRow))
		{
			bCheck = true;
			break;
		}
	}

	if(!bCheck)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG002), MB_ICONINFORMATION);
		return;
	}

	CWaitMessageBox wait;
	CDirtyFlag aInfoDirty;
	aInfoDirty.SetDirty("domainName", true);

	for(int nRow = m_lcHostList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		// 선택되어 있지 않으면 스킵
		if(!m_lcHostList.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lcHostList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsInfoList.GetAt(pos);
		Info.domainName = "";

		// 서버에 단말삭제명령 호출
		if(Info.siteId.GetLength()==0 || Info.hostId.GetLength()==0) continue;
		if(!CCopModule::GetObject()->SetHost(Info,GetEnvPtr()->m_szLoginID,&aInfoDirty)) continue;

		DisconnectHost(Info.ipAddress);

		// 선택해제
		m_lcHostList.SetCheck(nRow, FALSE);

		m_lsInfoList.RemoveAt(pos);
		m_lcHostList.DeleteItem(nRow);
	}

	UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG003), MB_ICONINFORMATION);
}

int CCacheServerSelectDlg::DisconnectHost(CString& ipAddress)
{
	// 해당하는 호스트를 CacheServer 로 물고 있던, 호스트의 domainName 을 모두 해제한다.
	HostInfoList connectedHostList;
	GetConnectedHostId(ipAddress, connectedHostList);

	CDirtyFlag aInfoDirty;
	aInfoDirty.SetDirty("domainName", true);

	int nRow = 0;
	POSITION pos = connectedHostList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo Info = connectedHostList.GetNext(pos);

		Info.domainName = "";

		// 서버에 단말삭제명령 호출
		if(Info.siteId.GetLength()==0 || Info.hostId.GetLength()==0) continue;
		if(!CCopModule::GetObject()->SetHost(Info,GetEnvPtr()->m_szLoginID,&aInfoDirty)) continue;

		nRow++;
	}
	TraceLog(("%d host disconnected", nRow));
	return nRow;
}


bool CCacheServerSelectDlg::GetConnectedHostId(CString& ipAddress, HostInfoList& outList)
{
	CString strWhere = "domainName = '";
	strWhere += ipAddress;
	strWhere += ":8080'";

	outList.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetHostForSelect(&aCall
														, _T("*")
														, _T("*")
														, strWhere
														);
	if(bRet)
	{
		CCopModule::GetObject()->GetHostData(&aCall
											, outList
											, 0
											);
		TraceLog(("%d data selected", outList.GetSize()));
	}else{
		TraceLog(("GetConnectedHostId failed"));
	}
	return bRet;
}


void CCacheServerSelectDlg::OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult)
{

	if(!IsAuth(_T("HDTL")) && !IsAuth(_T("HMOD"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lcHostList.GetItemData(nRow);
	if(!pos) return;

	SHostInfo Info = m_lsInfoList.GetAt(pos);

	CString hostName = Info.hostName;
	hostName += "(";
	hostName += Info.hostId;
	hostName += "), ";
	hostName += Info.ipAddress;
	
	CString aServer = Info.ipAddress;
	aServer += ":8080";
	CHostListDlg aDlg(hostName, aServer);

	aDlg.DoModal();
}

void CCacheServerSelectDlg::ModifyHost(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lcHostList.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lcHostList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsInfoList.GetAt(pos);
		CHostDetailDlg dlg;

		dlg.SetInfo(Info);
		if(dlg.DoModal() != IDOK)
		{
			dlg.GetInfo(Info);
			m_lsInfoList.GetAt(pos) = Info;
			continue;
		}
		dlg.GetInfo(Info);

		if(!CCopModule::GetObject()->SetHost(Info, GetEnvPtr()->m_szLoginID))
		{
			CString m_szMsg;
			m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;
		m_lsInfoList.GetAt(pos) = Info;
	}
	if(bModified)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG006), MB_ICONINFORMATION);
		RefreshHostList();
	}
}

void CCacheServerSelectDlg::OnNMRclickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	//ID_CACHELISTMENU_CONNECTED
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lcHostList.GetItemData(nRow);
	if(!pos) return;
	SHostInfo Info = m_lsInfoList.GetAt(pos);

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);
//	ClientToScreen(&pt);

	pPopup = menu.GetSubMenu(9);
	pPopup->EnableMenuItem(ID_CACHELISTMENU_CONNECTED, MF_ENABLED);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);

}

void CCacheServerSelectDlg::OnCachelistmenuConnected()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	int nRow = m_ptSelList.y;
	m_lcHostList.SetCheck(nRow, FALSE);

	if(!IsAuth(_T("HDTL")) && !IsAuth(_T("HMOD"))) return;

	CArray<int> arRow;
	arRow.Add(nRow);
	ModifyHost(arRow, true);

}
