#include "stdafx.h"
#include "resource.h"
#include "PlanoControl.h"
#include "NodeControl.h"
#include "Enviroment.h"
#include "common\TraceLog.h"
#include "ubccopcommon\CopModule.h"
#include "ubccopcommon\MultiSelectHost.h"
#include "ubccopcommon/PackageSelectDlg.h"
#include "MultiSelectLight.h"
#include "MultiSelectMonitor.h"
#include "HollidayDlg.h"
#include "IPInputDLG.h"
#include "hi/libHttp/ubcMux.h"
#include "common/libCommon/ubcMuxRegacy.h"

CPlanoControl::CPlanoControl()
{
	m_isEditMode=false;
	m_isShowInfo=false;
}

CPlanoControl::~CPlanoControl()
{
	Clear();
}

void
CPlanoControl::Clear()
{
	TraceLog(("Clear()"));

	POSITION pos = m_NodeList.GetHeadPosition();
	while(pos)
	{
		CNodeControl* aNode = m_NodeList.GetNext(pos);
		delete aNode;
	}
	m_NodeList.RemoveAll();
}

void
CPlanoControl::SelectAll(bool select)
{
	TraceLog(("SelectAll(%d)", select));

	POSITION pos = m_NodeList.GetHeadPosition();
	while(pos)
	{
		CNodeControl* aNode = m_NodeList.GetNext(pos);
		if(aNode) aNode->Selected(select,true);
		if(select) this->RedrawWindow();
	}
}

BEGIN_MESSAGE_MAP(CPlanoControl, CDownloadPictureEx)
	//{{AFX_MSG_MAP(CPlanoControl)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
	ON_WM_RBUTTONUP()
	ON_COMMAND(ID_PLANO_CREATE_LIGHT, &CPlanoControl::OnPlanoCreateLight)
	ON_COMMAND(ID_PLANO_CREATE_MONITOR, &CPlanoControl::OnPlanoCreateMonitor)
	ON_COMMAND(ID_PLANO_CREATE_PC, &CPlanoControl::OnPlanoCreatePc)
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

void CPlanoControl::OnPaint() 
{
	TraceLog(("OnPaint()"));

	CDownloadPictureEx::OnPaint(); 

	int counter=0;
	POSITION pos = m_NodeList.GetHeadPosition();
	while(pos)
	{
		++counter;
		TraceLog(("try to display %dth node", counter));
		CNodeControl* aNode = m_NodeList.GetNext(pos);
		if(this->IsEditMode()){
			aNode->ShowWindow(SW_SHOW);  //skpark 1111
		}else{
			aNode->ShowWindow(SW_HIDE);  //skpark 1111
			aNode->DrawIt();
		}
	}
}

void CPlanoControl::OnDestroy() 
{
	CDownloadPictureEx::OnDestroy();
}

bool CPlanoControl::Load(SPlanoInfo* plano, int oper, int admin) 
{
	m_planoInfo = plano;

	CString szServerLocation = "/Config/Plano/" ;

	CString szLocalLocation;
	szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

	CDownloadPictureEx::Load(m_planoInfo->backgroundFile, szLocalLocation, szServerLocation);
	if(!_LoadAllNode(oper,admin)){
		return false;
	}
	return true;
}


int
CPlanoControl::_LoadAllNode(int oper, int admin)
{
	TraceLog(("LoadAllNode()"));

	NodeViewInfoList nodeInfoList;
	int counter = CCopModule::GetObject()->GetNodeList(
						m_planoInfo->mgrId,
						m_planoInfo->siteId,
						m_planoInfo->planoId,
						CString("*"),
						oper,
						admin,
						nodeInfoList);

	Clear();

	if(counter==0){
		UbcMessageBox(LoadStringById(IDS_PLANOVIEW_STR014));
		return 0;
	}


	POSITION pos = nodeInfoList.GetHeadPosition();
	while(pos){
		SNodeViewInfo info = nodeInfoList.GetNext(pos);
		CNodeControl* ctrl = new CNodeControl(this);
		SNodeViewInfo* p_info = new SNodeViewInfo(info);
		if(!this->IsEditMode()){
			ctrl->SetDrawMode(true);
		}
		ctrl->SetNodeInfo(p_info);
		ctrl->CreateIcon(IsShowInfo()); //skpark 1111
		//ctrl->LoadIconFile();
		this->m_NodeList.AddTail(ctrl);
	}
	TraceLog(("%d node founded", m_NodeList.GetSize()));
	return counter;
}



int CPlanoControl::Save() 
{
	TraceLog(("Save()"));

	if(m_planoInfo == 0) {
		TraceLog(("No plano info"));
		return 0;
	}
	if(m_NodeList.GetSize() == 0) {
		TraceLog(("No node list"));
		return 0;
	}

	int retval = 0;
	POSITION pos = m_NodeList.GetHeadPosition();
	NodeControlList tempList;
	while(pos)
	{
		CNodeControl* aNode = m_NodeList.GetNext(pos);
		if(!aNode){
			TraceLog(("No Node"));
			continue;
		}

		if( aNode->isCreated()) {
			TraceLog(("Node created"));
			retval += aNode->CreateObject();
			tempList.AddTail(aNode);
		}else if(aNode->isDeleted()) {
			TraceLog(("Node deleted"));
			if(aNode->DeleteObject()){
				aNode->DestroyWindow();
				delete aNode;
				aNode = 0;
				retval++;
			}
		}else if(aNode->isModified()) {
			TraceLog(("Node modified"));
			retval += aNode->ModifyObject();
			tempList.AddTail(aNode);
		}else{ 
			TraceLog(("No Change"));
			tempList.AddTail(aNode);
		}
	}

	m_NodeList.RemoveAll();
	POSITION pos2 = tempList.GetHeadPosition();
	while(pos2)
	{
		CNodeControl* aNode = tempList.GetNext(pos2);
		//aNode->CreateIcon(IsShowInfo());
		m_NodeList.AddTail(aNode);
	}
	//this->RedrawWindow();

	CString msg;
	msg.Format("%d object saved", retval);
	TraceLog((msg));
	return retval;
}



void CPlanoControl::OnPlanoCreatePc()
{
	TraceLog(("OnPlanoCreatePc"));

	CMultiSelectHost dlg(GetEnvPtr()->m_strCustomer);

	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	dlg.SetTitle(LoadStringById(IDS_PLANOVIEW_STR007));

	if(dlg.DoModal() != IDOK) return;

	CString objectClass = "Host";
	int nRow=0;
	for(int i=0; i<dlg.m_astrSelHostList.GetCount() ;i++)
	{
		CString objectId = dlg.m_astrSelHostList[i];
		TraceLog(("Selected HostId = %s", objectId));

		CNodeControl* aNode = new CNodeControl(this);
		if(aNode->CreateNode(objectClass,objectId,this->m_lastRClickPoint.x, this->m_lastRClickPoint.y)){
			m_NodeList.AddTail(aNode);
			nRow++;
		}else{
			CString errMsg;
			errMsg.Format("%s node create failed", objectId);  //수정필요
			TraceLog((errMsg));
			UbcMessageBox(errMsg);
		}
	}
	TraceLog(("%d node created", nRow));
}

void CPlanoControl::OnPlanoCreateLight()
{
	TraceLog(("OnPlanoCreateLight"));

	CMultiSelectLight dlg(GetEnvPtr()->m_strCustomer);

	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	dlg.SetTitle(LoadStringById(IDS_PLANOVIEW_STR009));

	if(dlg.DoModal() != IDOK) return;

	CString objectClass = "Light";
	int nRow=0;
	for(int i=0; i<dlg.m_astrSelLightList.GetCount() ;i++)
	{
		CString objectId = dlg.m_astrSelLightList[i];
		TraceLog(("Selected LightId = %s", objectId));

		CNodeControl* aNode = new CNodeControl(this);
		if(aNode->CreateNode(objectClass,objectId,this->m_lastRClickPoint.x, this->m_lastRClickPoint.y)){
			m_NodeList.AddTail(aNode);
			nRow++;
		}else{
			CString errMsg;
			errMsg.Format("%s node create failed", objectId);  //수정필요
			TraceLog((errMsg));
			UbcMessageBox(errMsg);
		}
	}
	TraceLog(("%d node created", nRow));
}

void CPlanoControl::OnPlanoCreateMonitor()
{
	TraceLog(("OnPlanoCreateMonitor"));

	CMultiSelectMonitor dlg(GetEnvPtr()->m_strCustomer);

	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	dlg.SetTitle(LoadStringById(IDS_PLANOVIEW_STR008));

	if(dlg.DoModal() != IDOK) return;

	CString objectClass = "Monitor";
	int nRow=0;
	for(int i=0; i<dlg.m_astrSelMonitorList.GetCount() ;i++)
	{
		CString objectId = dlg.m_astrSelMonitorList[i];
		TraceLog(("Selected MonitorId = %s", objectId));

		CNodeControl* aNode = new CNodeControl(this);
		if(aNode->CreateNode(objectClass,objectId,this->m_lastRClickPoint.x, this->m_lastRClickPoint.y)){
			m_NodeList.AddTail(aNode);
			nRow++;
		}else{
			CString errMsg;
			errMsg.Format("%s node create failed", objectId);  //수정필요
			TraceLog((errMsg));
			UbcMessageBox(errMsg);
		}
	}
	TraceLog(("%d node created", nRow));
	
}
void CPlanoControl::OnRButtonUp(UINT nFlags, CPoint point)
{
	TraceLog(("OnRButtonUp()"));
	CStatic::OnRButtonUp(nFlags, point);
	//CButton::OnRButtonUp(nFlags, point);

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	if(m_isEditMode){
		pPopup = menu.GetSubMenu(5); //PLANO Menu

		long enabled = MF_ENABLED;
		long light_enabled = MF_DISABLED | MF_GRAYED;
		if(m_planoInfo->planoType == e_PLANOTYPE_LIGHT ) {
			enabled = MF_DISABLED | MF_GRAYED;
			light_enabled = MF_ENABLED;
		}else if(m_planoInfo->planoType == e_PLANOTYPE_ETC){
			light_enabled = MF_ENABLED;
		}

		pPopup->EnableMenuItem(ID_PLANO_CREATE_PC,			enabled);
		pPopup->EnableMenuItem(ID_PLANO_CREATE_MONITOR,		enabled);
		pPopup->EnableMenuItem(ID_PLANO_CREATE_LIGHT,		light_enabled);

		this->m_lastRClickPoint = point;
		ClientToScreen(&point);
		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,point.x, point.y, this);
	}else{
		// 노드를 선택한 경우는 노드 오른쪽 메뉴
		// 여백을 클릭한 경우는 모든 노드 선택을 해제한다.
		CNodeControl* aNode = ClickOnNode(point);
		if(aNode){
			ClientToScreen(&point);
			aNode->OnRButtonUp(nFlags,point);
		}else{
			SelectAll(false);
		}
	}
}

void CPlanoControl::OnLButtonUp(UINT nFlags, CPoint point)
{
	TraceLog(("OnLButtonUp()"));

	if(this->IsEditMode()) {
		POSITION pos = this->m_NodeList.GetHeadPosition();
		while(pos){
			CNodeControl* aNode = this->m_NodeList.GetNext(pos);
			if(aNode) aNode->EndMoving();
		}
	}else{
		// 노드를 선택한 경우는 노드 
		// 여백을 클릭한 경우는 모든 노드 선택을 해제한다.
		CNodeControl* aNode = ClickOnNode(point);
		if(aNode){
			ClientToScreen(&point);
			aNode->OnLButtonUp(nFlags,point);
		}else{
			SelectAll(false);
		}
	}
	CDownloadPictureEx::OnLButtonUp(nFlags, point);
}

void CPlanoControl::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(this->IsEditMode()) {
		SelectAll(false);
	}else{
		// 노드를 선택한 경우는 노드 
		// 여백을 클릭한 경우는 모든 노드 선택을 해제한다.
		CNodeControl* aNode = ClickOnNode(point);
		if(aNode){
			ClientToScreen(&point);
			aNode->OnLButtonDown(nFlags,point);
		}else{
			SelectAll(false);
		}
	}

	CDownloadPictureEx::OnLButtonDown(nFlags, point);
}

void CPlanoControl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	if(this->IsEditMode()) {
		SelectAll(false);
	}else{
		// 노드를 선택한 경우는 노드 
		// 여백을 클릭한 경우는 모든 노드 선택을 해제한다.
		CNodeControl* aNode = ClickOnNode(point);
		if(aNode){
			ClientToScreen(&point);
			aNode->OnLButtonDblClk(nFlags,point);
		}else{
			SelectAll(false);
		}
	}
	CDownloadPictureEx::OnLButtonDblClk(nFlags, point);
}

CNodeControl* CPlanoControl::ClickOnNode(CPoint& point)
{
	TraceLog(("GetNode(%d,%d)", point.x, point.y));

	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		SNodeViewInfo* info = node->GetInfo();
		if(info->x <= point.x && point.x <= info->x + node->GetWidth()) {
			if(info->y <= point.y && point.y <= info->y + node->GetHeight()) {
				TraceLog(("Click on node(%s)", info->nodeId));
				return node;
			}	
		}
	}
	return 0;
}


void CPlanoControl::Reboot()
{
	TraceLog(("Reboot()"));

	CWaitMessageBox wait;

	int counter = 0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(info && info->objectClass == "Host"){
				counter += CCopModule::GetObjectA()->Reboot(info->hostSiteId,info->hostId);
			}
		}
	}
	CString msg;
	msg.Format(LoadStringById(IDS_PLANOVIEW_STR015),counter);
	TraceLog((msg));
	UbcMessageBox(msg);
}

void CPlanoControl::Muteon()
{
	TraceLog(("Muteon()"));

	CWaitMessageBox wait;

	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(info && info->objectClass == "Host"){
				counter += CCopModule::GetObjectA()->MuteOn(info->hostSiteId,info->hostId);
			}
		}
	}
	CString msg;
	msg.Format(LoadStringById(IDS_PLANOVIEW_STR015),counter);
	TraceLog((msg));
	UbcMessageBox(msg);
}

void CPlanoControl::Muteoff()
{
	TraceLog(("Muteoff()"));

	CWaitMessageBox wait;

	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(info && info->objectClass == "Host"){
				counter += CCopModule::GetObjectA()->MuteOff(info->hostSiteId, info->hostId);
			}
		}
	}
	CString msg;
	msg.Format(LoadStringById(IDS_PLANOVIEW_STR015),counter);
	TraceLog((msg));
	UbcMessageBox(msg);
}

void CPlanoControl::Poweron()
{
	TraceLog(("Poweron()"));

	CWaitMessageBox wait;

	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(!info) continue;
			if(info->objectClass == "Host"){
				counter += CCopModule::GetObjectA()->PowerOn(info->hostSiteId, info->hostId);
			}else {
				counter += CCopModule::GetObjectA()->DevicePowerOn(info->hostSiteId, info->hostId,info->objectClass, 
																		info->objectId, info->objectType);
			}
		}
	}
	CString msg;
	msg.Format(LoadStringById(IDS_PLANOVIEW_STR015),counter);
	TraceLog((msg));
	UbcMessageBox(msg);
}

void CPlanoControl::Poweroff()
{
	TraceLog(("Poweroff()"));

	CWaitMessageBox wait;

	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(!info) continue;
			if(info->objectClass == "Host"){
				counter += CCopModule::GetObjectA()->Shutdown(info->hostSiteId, info->hostId);
			}else {
				counter += CCopModule::GetObjectA()->DevicePowerOff(info->hostSiteId,info->hostId,info->objectClass, 
																		info->objectId, info->objectType);
			}
		}
	}
	CString msg;
	msg.Format(LoadStringById(IDS_PLANOVIEW_STR015),counter);
	TraceLog((msg));
	UbcMessageBox(msg);
}

void CPlanoControl::Powerset()
{
	TraceLog(("Powerset()"));
	SNodeViewInfo* info = 0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			info = node->GetInfo();
			if(!info) continue;
			break;
		}
	}
	if(!info) return;

	CHollidayDlg dlg(this);

	ciString shutdownTimeStr = info->holiday;
	ciStringUtil::removeAllQuote(shutdownTimeStr);
	ciStringUtil::getBraceValue(shutdownTimeStr,"[","]");
	ciStringUtil::stringReplace(shutdownTimeStr,",","^");
	TraceLog(("holiday=%s", shutdownTimeStr));

	dlg.SetHolliday(shutdownTimeStr.c_str());
	dlg.SetPoweron(info->startupTime);
	dlg.ShowMonitor(false);
	dlg.ShowPoweron(true);

// strHolliday 에 꺼짐시간이 있으며, 이 값으로 초기화 하므로 값을 넘길 필요없음.
//	dlg.m_bShutdown = (!strShutdownTime.IsEmpty());
//	m_dtcPowerOff.GetTime(dlg.m_tmShutdown);

	dlg.m_bMonitorOff = false;
	dlg.m_strWeekShutdownTime = info->weekShutdownTime;

	if(dlg.DoModal() != IDOK) return;

	CString strHolliday;
	CString strShutdownTime;
	CString strWeekShutdownTime;

	dlg.GetHolliday(strHolliday);
	if(dlg.m_bShutdown)
	{
		strShutdownTime = dlg.m_tmShutdown.Format("%H:%M");
	}
	strWeekShutdownTime = dlg.m_strWeekShutdownTime;

	CWaitMessageBox wait;

	int counter=0;
	pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			info = node->GetInfo();
			if(!info) continue;
			
			info->holiday = strHolliday;
			info->shutdownTime = strShutdownTime;
			info->weekShutdownTime = strWeekShutdownTime;

			counter += CCopModule::GetObject()->SetPowerMng(info, GetEnvPtr()->m_szLoginID);

		}
	}

	CString msg;
	msg.Format(LoadStringById(IDS_PLANOVIEW_STR015),counter);
	TraceLog((msg));
	UbcMessageBox(msg);
}

void CPlanoControl::Brightset(int value)
{
	TraceLog(("Brightset()"));

	CWaitMessageBox wait;

	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(!info) continue;
			if(info->objectClass == "Host"){
				counter += CCopModule::GetObjectA()->SetVolume(info->hostSiteId, info->hostId, value);
			}else if(info->objectClass == "Light"){
				counter += CCopModule::GetObjectA()->SetBright(info->hostSiteId,info->hostId,info->objectClass, 
																		info->objectId, info->objectType, value);
			}
		}
	}
	CString msg;
	msg.Format(LoadStringById(IDS_PLANOVIEW_STR015),counter);
	TraceLog((msg));
	UbcMessageBox(msg);
}


int	 CPlanoControl::ChangeOPStat(const char* siteId, const char* hostId, bool opStat)
{
	TraceLog(("ChangeOPStat(%s,%s,%d)", siteId,hostId,opStat));
	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		SNodeViewInfo* info = node->GetInfo();
		TraceLog(("is operationalState changed on %s/%s  ?? ", info->hostSiteId, info->hostId));
		if(info->hostSiteId.Compare(siteId)==0 && info->hostId.Compare(hostId)==0){
			TraceLog(("operationalState changed !!!! %d --> %d", info->operationalState, opStat));
			if(opStat != info->operationalState){
				info->operationalState = opStat;
				node->LoadIconFile();
				node->RedrawWindow();
				//this->RedrawWindow();
				counter++;
			}
		}
	}
	return counter;
}
int	 CPlanoControl::ChangeDeviceOPStat(const char* siteId, const char* hostId, const char* objectClass, const char* objectId, bool opStat)
{
	TraceLog(("ChangeDeviceOPStat(%s,%s,%s,%s,%d)", siteId,hostId,objectClass,objectId,opStat));
	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		SNodeViewInfo* info = node->GetInfo();
		TraceLog(("is deviceOperationalState changed on %s/%s/%s  ?? ", info->hostSiteId, info->hostId, info->objectId));
		if(info->hostSiteId.Compare(siteId)==0 && info->hostId.Compare(hostId)==0 
			&& info->objectClass.Compare(objectClass)==0 && info->objectId.Compare(objectId)==0){
			TraceLog(("deviceOperationalState changed !!!! %d --> %d", info->operationalState, opStat));
			if(opStat != info->operationalState){
				info->operationalState = opStat;
				node->LoadIconFile();
				node->RedrawWindow();
				//this->RedrawWindow();
				counter++;
			}
		}
	}
	return counter;
}



BOOL CPlanoControl::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(pMsg->message == WM_MOUSEMOVE){
		if(!this->m_isEditMode && !IsShowInfo()){
			CPoint pt = pMsg->pt;
			ScreenToClient(&pt);
			TraceLog(("point = %d,%d", pt.x, pt.y));
			POSITION pos = this->m_NodeList.GetHeadPosition();
			while(pos){
				CNodeControl* node = this->m_NodeList.GetNext(pos);
				SNodeViewInfo*	nodeInfo = node->GetInfo();
				TraceLog(("Node = %d,%d,%d,%d", nodeInfo->x, nodeInfo->y, nodeInfo->x + node->GetWidth(), nodeInfo->y + node->GetHeight()));
				if( nodeInfo->x <= pt.x && pt.x <= nodeInfo->x + node->GetWidth() &&
					nodeInfo->y <= pt.y && pt.y <= nodeInfo->y + node->GetHeight() ){
					TraceLog(("Node Catched!!!"));
					node->ShowName(true);
				}else{
					node->ShowName(false);
				}
			}
		}
	}
	return CDownloadPictureEx::PreTranslateMessage(pMsg);
}


void 
CPlanoControl::Remotelogin()
{
	CWaitMessageBox wait;

	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(!info) continue;
			if(info->objectClass != "Host") continue;
			
			ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(info->siteId);
			if(!aData) return;
			TraceLog((aData->ipAddress.c_str()));

			CString szPath;
			szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
			TraceLog((szPath));

			int nPos = info->hostId.Find(_T("-"));
			CString strId = (nPos < 0 ? _T("") : info->hostId.Mid(nPos+1));

			CString szParam = " /proxy ";
			szParam += aData->ipAddress.c_str();
			szParam += ":5901 ID:";
			//szParam += ToString(Info.vncPort);
			szParam += strId;

			HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);

		}
	}

}
void 
CPlanoControl::Remotelogin2()
{
	CWaitMessageBox wait;

	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(!info) continue;
			if(info->objectClass != "Host") continue;
			if(info->vncPort==0 || info->ipAddress.IsEmpty()){
				if(!CCopModule::GetObject()->GetNodeObjectInfo(info)){
					UbcMessageBox("Get Node Object infomation failed"); // 수정필요
					return ;
				}
			}
			
			CString szPath;
			szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
			TraceLog((szPath));

			CString szParam;
			szParam.Format(_T("%s:%d"), info->ipAddress, info->vncPort);

			HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);
		}
	}
}


void 
CPlanoControl::ApplyPackage()
{
	CWaitMessageBox wait;

	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(!info) continue;
			if(info->objectClass != "Host") continue;

			SHostInfo sInfo;
			if(!node->toHostInfo(sInfo)){
				return;
			}

			//CPackageChangeDlg dlg;
			CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
			dlg.SetSingleSelection(true);

			CString szPath;
			szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
			dlg.SetEnvValue(GetEnvPtr()->m_szLoginID, szPath);

			for(int i=0; i<sInfo.displayCounter ;i++)
			{
				SHostInfo infoDisp = sInfo;
				infoDisp.displayCounter = i+1;
				dlg.m_arHostList.Add(infoDisp);
			}

			if(dlg.m_arHostList.GetCount() <= 0)
			{
				UbcMessageBox(LoadStringById(IDS_PLANOMGRVIEW_STR018), MB_ICONINFORMATION);
				return;
			}
			
			if(dlg.DoModal() == IDOK)
			{
			}
		}
	}
}

void 
CPlanoControl::CancelPackage()
{
	CWaitMessageBox wait;

	int counter=0;
	POSITION pos = this->m_NodeList.GetHeadPosition();
	while(pos){
		CNodeControl* node = this->m_NodeList.GetNext(pos);
		if(node && node->IsSelected()){
			SNodeViewInfo* info = node->GetInfo();
			if(!info) continue;
			if(info->objectClass != "Host") continue;

			if(!CCopModule::GetObject()->CancelPackage(info->siteId, info->hostId, 0, GetEnvPtr()->m_szLoginID))
			{
				CString szMsg;
				szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), info->hostId);
				UbcMessageBox(szMsg, MB_ICONERROR);
			}
		}
	}

}
