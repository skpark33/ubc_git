// GeoMapFrm.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "GeoMapFrm.h"
#include "Enviroment.h"


// CGeoMapFrm

IMPLEMENT_DYNCREATE(CGeoMapFrm, CMDIChildWnd)

CGeoMapFrm::CGeoMapFrm()
:	m_EventManager(this)
{
}

CGeoMapFrm::~CGeoMapFrm()
{
}


BEGIN_MESSAGE_MAP(CGeoMapFrm, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CGeoMapFrm message handlers

int CGeoMapFrm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDI_SCREENSHOT), false);

	// 이벤트생성시 오래걸리는 현상이 발생되어 스레드로 뺀다.
	AfxBeginThread(AddEventThread, this);

	return 0;
}

UINT CGeoMapFrm::AddEventThread(LPVOID pParam)
{
	CGeoMapFrm* pFrame = (CGeoMapFrm*)pParam;
	if(!pFrame) return 0;
	HWND hWnd = pFrame->GetSafeHwnd();

	TraceLog(("CGeoMapFrm::AddEventThread - Start"));

	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// 2010.05.13 이벤트등록을 데드락 발생으로 OnCreate에서 이쪽으로 옮김
	CString szBuf;
	szBuf.Format("PM=*/Site=%s/Host=* ", (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? "*" : GetEnvPtr()->m_szSite));

//	pFrame->m_EventManager.AddEvent(szBuf + "downloadStateChanged");
	pFrame->m_EventManager.AddEvent(szBuf + "operationalStateChanged");
	pFrame->m_EventManager.AddEvent(szBuf + "adminStateChanged");
//	pFrame->m_EventManager.AddEvent(szBuf + "vncStateChanged");
	pFrame->m_EventManager.AddEvent("* hostScheduleUpdated");

	::CoUninitialize();

	TraceLog(("CGeoMapFrm::AddEventThread - End"));

	return 0;
}

void CGeoMapFrm::OnClose()
{
	TraceLog(("CGeoMapFrm::OnClose begin"));

	// 0001414: 단말목록등 서버로부터 이벤트를 받는 화면이 닫힐때 응답없음으로 빠지는 문제.
	// 더이상 이벤트가 날아와도 처리하지 않도록 한다
	if(!CEventHandler::CloseWaitAllEvent(GetSafeHwnd())) return;

	m_EventManager.RemoveAllEvent();

	CEventHandler::SetIgnoreEvent(false);

	CMDIChildWnd::OnClose();

	TraceLog(("CGeoMapFrm::OnClose end"));
}

int CGeoMapFrm::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	TraceLog(("CGeoMapFrm::InvokeEvent begin"));

	HWND hWnd = GetSafeHwnd();
	TraceLog(("hWnd [%X]", hWnd));
	if(::IsWindow(hWnd) == FALSE) return 0;

	if(GetActiveView())
	{
		return GetActiveView()->SendMessage(WM_INVOKE_EVENT, wParam, lParam);
	}

	TraceLog(("CGeoMapFrm::InvokeEvent end"));

	return 0;
}
