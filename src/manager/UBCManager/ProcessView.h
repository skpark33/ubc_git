#pragma once

#include "afxwin.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"
#include "ubccopcommon\eventhandler.h"
#include "common\CheckComboBox.h"
#include "UBCCopCommon\CopModule.h"

struct SHostInfo;

// CProcessView 대화 상자입니다.
class CProcessView : public CFormView
{
	DECLARE_DYNCREATE(CProcessView)
public:
	enum { eCheck, // 2010.02.17 by gwangsoo
		   eHostVNCStat, eMonitorStat, eHostType,
		   eSiteName, eHostName, eTag, eHostID, eHostDisCnt,
		   eHostStarter, eHostBrowser, eHostPreDownloader, eHostFirmware,
		   eHostEnd };
	enum { eMonitorOn, eMonitorOff, eMonitorUnknown, eVncOn, eVncOff};
	enum { eFront, eBack, eAll };

private:
	HostInfoList m_lsHost;

	void InitHostList();
	void RefreshHostList();
	void InitPosition(CRect);
	int ChangeOPStat(SOpStat*);
	int ChangeAdStat(SAdStat*);
	int ChangeVncStat(cciEvent*);
	int ChangeProcessState(SProcessStateChange*);
	int ChangeMonitorStat(SMonitorStat*); // 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	BOOL RefreshHost(bool bCompMsg=true);
	void UpdateListRow(int nRow, SHostInfo* pInfo);

	CMapStringToString* m_pBackupHostList;
	void BackupHostList();
	void DeleteBackupHostList();
	bool IsJustNowHost(CString strHostId);

	void LoadFilterData();
	void SaveFilterData();
	CString m_strSiteId;
	CString m_strContentsId;
	CString m_strPackageId;

//	virtual int InvokeEvent(WPARAM, LPARAM);
	LRESULT InvokeEvent(WPARAM, LPARAM);
public:
private:
	CUTBListCtrlEx m_lscList;
	CStatic		 m_Filter;
	CString		 m_szHostColum[eHostEnd];

	CHoverButton m_btnRefHost;
	CHoverButton m_btnRestart;

	CComboBox m_cbAdmin;
	CComboBox m_cbOperation;
	//CComboBox m_cbHostType;
	CCheckComboBox m_cbHostType;

	CPoint m_ptSelList;
	CRect m_rcClient;
	CReposControl m_Reposition;

//	CList<int> m_lsEvID;
//	CEventManager m_EventManager;
//	static UINT AddEventThread(LPVOID pParam);

	CImageList m_ilHostList;

protected:
	CProcessView();           // protected constructor used by dynamic creation
	virtual ~CProcessView();

public:
	enum { IDD = IDD_PRECESSVIEW };

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnRemotelogin();
	afx_msg void OnRemotelogin2(); // 0001442: 원격접속 방법에 서버를 경유하지 않는 vnc원격접속을 만든다.
	afx_msg void OnRemotelogin3(); // 원격접속 방법에 서버를 경유하는 vnc원격접속을 만든다.
	afx_msg void OnRemoteDesktop();
	afx_msg void OnNMRclickListHost(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedButtonHostrfs();
	afx_msg void OnBnClickedButtonHostRestart();
	afx_msg void OnBnClickedButtonFilterSite();
	afx_msg void OnBnClickedButtonFilterContents();
	afx_msg void OnBnClickedButtonFilterPackage();
	afx_msg LRESULT OnFilterHostChanged(WPARAM wParam, LPARAM lParam);
	CComboBox m_cbFilterTag;
	afx_msg void OnRemoteLoginSettings();
};

