#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "Enviroment.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"

// CPackageChngLogDlg 대화 상자입니다.

class CPackageChngLogDlg : public CDialog
{
	DECLARE_DYNAMIC(CPackageChngLogDlg)

public:
	CPackageChngLogDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPackageChngLogDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PACKAGE_CHNG_LOG_DLG };
	enum { eSite, eHost, ePackage, eApplyTime, eHow, eWho, eWhy, eMaxCol };

	void SetParam(CString strHostId, CString strHostName)
		{
			m_strHostId = strHostId;
			m_strHostName = strHostName;
		}

protected:
	CRect						m_rcClient;
	bool						m_bInitialize;
	CReposControl				m_Reposition;
	CUTBListCtrlEx				m_lcList;
	CHoverButton				m_bnRefresh;
	CHoverButton				m_bnClose;
	CHoverButton				m_btnExcelSave;
	CDateTimeCtrl				m_dtFilterStart;
	CDateTimeCtrl				m_dtFilterEnd;

	CString						m_strHostName;
	CString						m_strHostId;

	CString						m_szColum[eMaxCol];
	ApplyLogInfoList			m_lsInfoList;
	void						InitList();
	void						RefreshList();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBnClickedButtonFilterHost();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnBnClickedBnToExcel();
};
