#pragma once

#include "resource.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "common\utblistctrlex.h"
#include "Enviroment.h"

// CPlanVolumeConfirmDlg 대화 상자입니다.

class CPlanVolumeConfirmDlg : public CDialog
{
	DECLARE_DYNAMIC(CPlanVolumeConfirmDlg)

public:
	CPlanVolumeConfirmDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPlanVolumeConfirmDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PLAN_VOLUME_CONFIRM_DLG };

	enum { ePCheck // 2010.02.17 by gwangsoo
		 , eSite, ePackage, eVolume
		 , eEnd };

	CStringArray m_aHostList;
	CStringArray m_aPackageList;

	PackageInfoList m_lsPackageList;
	CUTBListCtrlEx	m_lscPackage;
	void InitPackageList();
	void RefreshPackageList();

	ULONGLONG m_ulTotalPackageSize;
	void TotalPackageSize();

	enum { eHCheck, // 2010.02.17 by gwangsoo
		   eSiteName, eHostName, eHostId, eDisk1Avail, eDisk2Avail, 
		   eHostEnd };

	HostInfoList m_lsHostList;
	CUTBListCtrlEx m_lscHost;
	void InitHostList();
	void UpdateHostList();
	void RefreshHostList(ULONGLONG ulSize);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBnHostListRefresh();
	afx_msg void OnBnClickedBnHostClean();
	afx_msg void OnLvnItemchangedListPackage(NMHDR *pNMHDR, LRESULT *pResult);
};
