#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "Enviroment.h"
#include "common\HoverButton.h"
#include "common\utblistctrl.h"
#include "common\reposcontrol.h"
#include "ubccopcommon\eventhandler.h"

// CLMOSolutionView form view
class CLMOSolutionView : public CFormView
{
	DECLARE_DYNCREATE(CLMOSolutionView)
public:
	enum { eCheck, eDescription, eTableName, eTimeFieldName, eLogType, eDuration, eAdminState, eEnd };
private:
	void InitList();
	void SetList();

public:
private:
	CUTBListCtrl m_lscLmo;
	CHoverButton m_btAdd;
	CHoverButton m_btDel;
	CHoverButton m_btMod;
	CHoverButton m_btRef;
	LMOSolutionInfoList m_lsLMOSolution;

	bool m_bInit;
	CRect m_rcClient;
	CString m_ColumTitle[eEnd];
	CReposControl m_Reposition;

protected:
	CLMOSolutionView();           // protected constructor used by dynamic creation
	virtual ~CLMOSolutionView();

public:
	enum { IDD = IDD_LMO_SOLUTION_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	void OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult);
	
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
	afx_msg void OnBnClickedButtonMod();
	afx_msg void OnBnClickedButtonRef();
};


