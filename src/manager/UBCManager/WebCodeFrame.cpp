// WebCodeFrame.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "WebCodeFrame.h"


// CWebCodeFrame

IMPLEMENT_DYNCREATE(CWebCodeFrame, CMDIChildWnd)

CWebCodeFrame::CWebCodeFrame()
{

}

CWebCodeFrame::~CWebCodeFrame()
{
}


BEGIN_MESSAGE_MAP(CWebCodeFrame, CMDIChildWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CWebCodeFrame message handlers

int CWebCodeFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDR_CODEFRAME), false);

	return 0;
}
