#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"


// CCacheServerSelectDlg 대화 상자입니다.

class CCacheServerSelectDlg : public CDialog
{
	DECLARE_DYNAMIC(CCacheServerSelectDlg)

public:
	CCacheServerSelectDlg(LPCTSTR szCustomer,CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCacheServerSelectDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CACHESERVERSELECT_DLG };
	enum { eHostName, eHostID, eGroup, eHostIP, /*eSlaveCounter,*/ eMaxCol };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonHostRefresh();
	void	EditMode() { m_bEditMode = true; }

	void	ModifyHost(CArray<int>& arRow, bool bMsg);
	bool	GetConnectedHostId(CString& ipAddress, HostInfoList& outList);
	int		DisconnectHost(CString& ipAddress);

	CEdit m_editFilterHostName;
	CEdit m_editFilterHost;
	CEdit			m_editIP;
	CUTBListCtrlEx	m_lcHostList;
	CEdit			m_editSelectedHost;
	CHoverButton	m_btRefresh;
	CHoverButton m_btAdd;
	CHoverButton m_btDel;
	CComboBox		m_cbOperation;
	CButton m_btOK;
	bool		m_bEditMode;
	CString		m_currentCacheServer;
	CString		m_currentHostID;

	HostInfoList		m_lsInfoList;
	CString				m_szColum[eMaxCol];
	int					m_nAuthority;
	CString				m_strSiteId;
	CString				m_strCustomer;
	CPoint m_ptSelList;

	void				InitHostList();
	void				RefreshHostList();

	afx_msg void OnNMClickListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonHostAdd();
	afx_msg void OnBnClickedButtonHostDel();
	afx_msg void OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRclickListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCachelistmenuConnected();


};
