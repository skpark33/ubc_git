#pragma once

#include "afxwin.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"
#include "ubccopcommon\eventhandler.h"

#include "common\CheckComboBox.h"
#include "UBCCopCommon\CopModule.h"
//#include <cci/libWrapper/cciCall.h>

struct SHostInfo;

// CHostView form view
class CMainFrame;
class CHostView : public CFormView//, public IEventHandler
{
	DECLARE_DYNCREATE(CHostView)
public:
	enum { eCheck, // 2010.02.17 by gwangsoo
		   eHostVNCStat, eMonitorStat, eHostType, 
		   eSiteName, eHostName, eTag, eHostMsg, eHostCur1, eHostCur2, eHostAuto1, eHostAuto2, eHostLast1, eHostLast2, eHostNext1,eHostNext2,
		   eDisk1Avail, eDisk2Avail, 
		   eHostID, eHostDisCnt, eDomainName, /* eHostNet1,*/ /*eHostNet2,*/ 
		   eHostUpDate, eVersion, /*eHostPeriod*/eHostAddr1, /*eHostOper, eHostAdmin,*/ eHostDesc, eHostIP, eHostMacAddr,/* eHostEdition, */ eHostBootUpTime, 
		   eHostVNCPort, /*eHostVendor,*/ eMonitorType,/*eHostModel,*/ eHostOs, /*eHostSerial,*/ eDownloadTime, eGmt, eMonitorUseTime,
		   eShutdownTime, eStartupTime, eSosId,
		   eHostEnd };
	enum { eMonitorOn, eMonitorOff, eMonitorUnknown, eVncOn, eVncOff, eExclamation };
	enum { eFront, eBack, eAll };

private:
	cciCall* m_pCall;
	HostInfoList m_lsHost;

//	void InitSiteList();
//	void RefreshSiteList();
	void InitHostList();
	void RefreshHostList(POSITION posStart = NULL);
	void RefreshPaneText();
	void InitPosition(CRect);
	int ChangeOPStat(SOpStat*);
	int ChangeAdStat(SAdStat*);
	int ChangeMonitorStat(SMonitorStat*); // 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	int UpdatePackage(SUpPackage*);
	int ChangeVncStat(cciEvent*);
	int ChangeDownloadStat(SDownloadStateChange*); // 0000707: 콘텐츠 다운로드 상태 조회 기능
	int ChangeDiskAvail(SDiskAvailChanged*);
	void ModifyHost(CArray<int>& arRow, bool bMsg=false);
	BOOL RefreshHost(bool bCompMsg=true);
	void RunStudio(LPCTSTR);
	void UpdateListRow(int nRow, SHostInfo* pInfo);

	CMapStringToString* m_pBackupHostList;
	void BackupHostList();
	void DeleteBackupHostList();
	bool IsJustNowHost(CString strHostId);

	void LoadFilterData();
	void SaveFilterData();
	CString m_strSiteId;
	CString m_strContentsId;
	CString m_strPackageId;

//	virtual int InvokeEvent(WPARAM, LPARAM);
	LRESULT InvokeEvent(WPARAM, LPARAM);
public:
private:
	CMainFrame*  m_pMainFrame;
	CString		 m_szMsg;

	CUTBListCtrlEx m_lscHost;
	CStatic		 m_Filter;
	CString		 m_szHostColum[eHostEnd];

//	CHoverButton m_btnAddHost;
	CHoverButton m_btnDelHost;
	CHoverButton m_btnModHost;
	CHoverButton m_btnRefHost;
//	CHoverButton m_btnOptHost;
	CHoverButton m_btnUpdHost;
	CHoverButton m_btnShutdown;
	CHoverButton m_btnReboot;
	CHoverButton m_btnRestart;
	CHoverButton m_btnChnSite;
	CHoverButton m_btnHddThreshold;
	CHoverButton m_btnExcelSave;
	CHoverButton m_btnRetransmit;
	CHoverButton m_btnPowerMng;
	CHoverButton m_btnPackageChng;
	CHoverButton m_btnMonitorPower;
	CHoverButton m_btAddTags;
	CHoverButton m_btDelTags;

	void InitAuthCtrl();

	CComboBox m_cbAdmin;
	CComboBox m_cbOperation;
	//CComboBox m_cbHostType;
	CCheckComboBox m_cbHostType;

	CPoint m_ptSelList;
	CRect m_rcClient;
	CReposControl m_Reposition;

//	CList<int> m_lsEvID;
//	CEventManager m_EventManager;
//	static UINT AddEventThread(LPVOID pParam);

	CImageList m_ilHostList;


//	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
//	static int CALLBACK CompareHost(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

protected:
	CHostView();           // protected constructor used by dynamic creation
	virtual ~CHostView();

public:
	enum { IDD = IDD_HOSTVIEW };

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnPackageApply();
//	afx_msg void OnEditpackage();
	afx_msg void OnAutoPackage1();
	afx_msg void OnAutoPackage2();
	afx_msg void OnCurPackage1();
	afx_msg void OnCurPackage2();
	afx_msg void OnLastPackage1();
	afx_msg void OnLastPackage2();

	afx_msg void OnRemotelogin();
	afx_msg void OnRemotelogin2(); // 0001442: 원격접속 방법에 서버를 경유하지 않는 vnc원격접속을 만든다.
	afx_msg void OnRemotelogin3(); //  원격접속 방법에 지정된 서버를 경유하는 vnc원격접속을 만든다.
	afx_msg void OnRemoteDesktop();
	afx_msg void OnOpenscreenshotslide();
	afx_msg void OnUse();
	afx_msg void OnShowreservationinfo();
	afx_msg void OnProperties();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);

	afx_msg void OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRclickListHost(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedButtonHostadd();
	afx_msg void OnBnClickedButtonHostdel();
	afx_msg void OnBnClickedButtonHostmod();
	afx_msg void OnBnClickedButtonHostrfs();
	afx_msg void OnBnClickedButtonHostopt();
	afx_msg void OnBnClickedButtonHostupd();
	afx_msg void OnBnClickedButtonHostShutdown();
	afx_msg void OnBnClickedButtonHostReboot();
	afx_msg void OnBnClickedButtonHostRestart();
	afx_msg void OnBnClickButtonHostChgSite();
	afx_msg void OnHostlistCancelPackage(UINT nID);
	afx_msg void OnBnClickedButtonHostHddthreshold();
	afx_msg void OnBnClickedBnToExcel2();
	afx_msg void OnDownloadResultView();
	afx_msg void OnBnClickedBnRetransmit();
	afx_msg void OnBnClickedBnPowerMng();
	afx_msg void OnBnClickedButtonHostPackageChng();
	afx_msg void OnBnClickedButtonFilterSite();
	afx_msg void OnBnClickedButtonFilterContents();
	afx_msg void OnBnClickedButtonFilterPackage();
	afx_msg LRESULT OnFilterHostChanged(WPARAM wParam, LPARAM lParam);
	afx_msg void OnHostlistPkgChngLogView();
	afx_msg void OnBnClickedButtonHostMonitorPower();
	CComboBox m_cbFilterTag;
	CButton m_kbAdvancedFilter;
	CComboBox m_cbAdvancedFilter;
	afx_msg void OnBnClickedKbAdvancedFilter();
	afx_msg void OnBnClickedBnDeltag();
	afx_msg void OnBnClickedBnAddtag();
	afx_msg void OnLvnItemchangedListHost(NMHDR *pNMHDR, LRESULT *pResult);
	

	// TimeLine Windows [
	bool			m_showNextSchedule;
	CHoverButton	m_btnSizeToggle;

	CUTBListCtrlEx	m_lscTimeLine;
	int				m_toggle; //skpark
	int				m_time_width;

	void InvalidateItems();
	void ToggleWindow(int on_off);
	void InitTimeLineList();
	bool RefreshTimeLine(SHostInfo& hostInfo);
	afx_msg void OnBnClickedButtonResizeToggle();
	afx_msg void OnNMClickListHost(NMHDR *pNMHDR, LRESULT *pResult);

	// TimeLine Windows ]

	CComboBox m_cbFilterCache;
	CHoverButton m_btStart;
	afx_msg void OnBnClickedButtonHostStart();
	CHoverButton m_btProperty;
	afx_msg void OnBnClickedButtonHostProperty();

	// new auth dialog (update from file)
	void HostupdEx();

	afx_msg void OnRemoteLoginSettings();

	CString GetDiskAvailError(float errCode);

	CHoverButton m_btConfig;
	afx_msg void OnBnClickedButtonConfig();
	afx_msg void OnBnClickedButtonConverterReboot();
	afx_msg void OnBnClickedButtonConverterUpdate();
	CHoverButton m_btnRebootConverter;
	CHoverButton m_btnUpdateConverter;


	afx_msg void OnHostlist1Networktest();
	CHoverButton m_btnMemo;
	afx_msg void OnBnClickedButtonMemo();
};


