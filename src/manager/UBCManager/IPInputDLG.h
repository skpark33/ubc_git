#pragma once
#include "afxcmn.h"
#include "resource.h"

// CIPInputDLG 대화 상자입니다.

class CIPInputDLG : public CDialog
{
	DECLARE_DYNAMIC(CIPInputDLG)

public:
	CIPInputDLG(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CIPInputDLG();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_IPADDRESS };

	void	setIpAddress(const char* ip) { _ipAddress = ip;}
	CString	getIpAddress() { return _ipAddress; }

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	CString	_ipAddress;

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CIPAddressCtrl ipEditCtrl;
	afx_msg void OnBnClickedOk();
};
