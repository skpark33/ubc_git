#pragma once

#include "common\HoverButton.h"
#include "common\utblistctrl.h"
#include "common\reposcontrol.h"

// CSiteView form view
class CSiteView : public CFormView
{
	DECLARE_DYNCREATE(CSiteView)
public:
	enum { eCheck // 2010.02.17 by gwangsoo
		 , eSiteID, eSiteName, eSiteOpen, eSiteClose, /*eSiteHoliday,*/ eSiteEnd };
private:
	void InitSiteList();
	void RefreshSiteList();
	void InitPosition(CRect);
	void ModifySite(CArray<int>&);
public:
private:
	bool m_bInit;
	CString m_szMsg;

	CUTBListCtrl m_lscSite;
	CString m_szSiteColum[eSiteEnd];
	CHoverButton m_btnAddSite;
	CHoverButton m_btnDelSite;
	CHoverButton m_btnModSite;
	CHoverButton m_btnRefSite;

	CRect m_rcClient;
	CReposControl m_Reposition;

	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

protected:
	CSiteView();           // protected constructor used by dynamic creation
	virtual ~CSiteView();

public:
	enum { IDD = IDD_SITEVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnLvnColumnclickListSite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListSite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListSite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListSite(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedButtonSiteadd();
	afx_msg void OnBnClickedButtonSitedel();
	afx_msg void OnBnClickedButtonSitemod();
	afx_msg void OnBnClickedButtonSiterfs();
};


