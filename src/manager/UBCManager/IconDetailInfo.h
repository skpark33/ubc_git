#pragma once
#include "afxwin.h"
#include "DownloadPictureEx.h"
#include "ubccopcommon\copmodule.h"


// CIconDetailInfo 대화 상자입니다.

class CIconDetailInfo : public CDialog
{
	DECLARE_DYNAMIC(CIconDetailInfo)

public:
	CIconDetailInfo(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CIconDetailInfo();

	SNodeTypeInfo	m_IconInfo;
	CodeItemList m_listHostType;
	CodeItemList m_listLightType;
	CodeItemList m_listMonitorType;

	bool	m_IsCreateMode;
	bool	m_IconChanged;
	void	SetCreateMode() { m_IsCreateMode=true; }
	BOOL	Upload();
	BOOL	SetInfo();
	void	SetInfo(SNodeTypeInfo&);
	void	GetInfo(SNodeTypeInfo&);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ICON_DETAILINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CEdit m_editNodeTypeId;
	CComboBox m_cbObjectClass;
	CComboBox m_cbObjectType;
	CComboBox m_cbObjectState;
	CDownloadPictureEx m_picIcon;
	afx_msg void OnBnClickedButtonNew();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnCbnSelchangeComboObjectclass();
};
