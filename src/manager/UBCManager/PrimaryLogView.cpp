// PrimaryLogView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "PrimaryLogView.h"
#include "common\ubcdefine.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "ubccopcommon\SelectHostDlg.h"
#include "ubccopcommon\PackageSelectDlg.h"
#include "HostExcelSave.h"
#include "common/libCommon/ubcIni.h"

// CLogBrowser

void CLogBrowser::SetFilter(LPCSTR lpszPackageId,LPCSTR lpszHostId, CTime& tmStart, CTime& tmEnd)
{ 
	m_strHostId=lpszHostId; 
	m_strPackageId=lpszPackageId; 
	m_tmStart=tmStart; 
	m_tmEnd=tmEnd; 

	ubcConfig aIni("UBCManager");
	aIni.set("LOG-FILTER","PackageId", lpszPackageId);

}


// CPrimaryLogView

IMPLEMENT_DYNCREATE(CPrimaryLogView, CFormView)

CPrimaryLogView::CPrimaryLogView()
:	CFormView(CPrimaryLogView::IDD)
,	m_Reposition(this)
{

}

CPrimaryLogView::~CPrimaryLogView()
{
	POSITION pos = m_mapLogBrowser.GetStartPosition();
	while( pos != NULL )
	{
		CString key;
		CLogBrowser* pLogBrowser = NULL;
		m_mapLogBrowser.GetNextAssoc( pos, key, (void*&)pLogBrowser );
		if(pLogBrowser) delete pLogBrowser;
	}
}

void CPrimaryLogView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_btnRefresh);
	DDX_Control(pDX, IDC_EDIT_FILTER_HOST, m_editHostName);
	DDX_Control(pDX, IDC_DATE_FILTER_START, m_dtcFilterStart);
	DDX_Control(pDX, IDC_DATE_FILTER_END, m_dtcFilterEnd);
	DDX_Control(pDX, IDC_BN_TO_EXCEL, m_btnExcelSave);
	DDX_Control(pDX, IDC_HOSTLOG_TAB, m_tcLogTab);
	DDX_Control(pDX, IDC_LIST_APPLYLOG, m_lcLog[0]);
	DDX_Control(pDX, IDC_LIST_SCHEDULESTATELOG, m_lcLog[1]);
	DDX_Control(pDX, IDC_LIST_POWERLOG, m_lcLog[2]);
	DDX_Control(pDX, IDC_LIST_CONNECTIONLOG, m_lcLog[3]);
	DDX_Control(pDX, IDC_LIST_DOWNLOADSUMMARYLOG, m_lcLog[4]);
	DDX_Control(pDX, IDC_LIST_USERLOG, m_lcLog[5]);
	DDX_Control(pDX, IDC_LIST_BPLOG, m_lcLog[6]);
	DDX_Control(pDX, IDC_LIST_PLAYLOG, m_lcLog[7]);
	DDX_Control(pDX, IDC_LIST_FAULTLOG, m_lcLog[8]);

	DDX_Control(pDX, IDC_STATIC_PACKAGE_LABEL, m_packageLabel);
	DDX_Control(pDX, IDC_EDIT_FILTER_PACKAGE, m_filterPackage);
	DDX_Control(pDX, IDC_BUTTON_FILTER_PACKAGE, m_btFilterPackage);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOST, m_btFilterHost);
	DDX_Control(pDX, IDC_STATIC_HOSTLABEL, m_labelFilterHost);

}

BEGIN_MESSAGE_MAP(CPrimaryLogView, CFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CPrimaryLogView::OnBnClickedBtnRefresh)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_HOST, &CPrimaryLogView::OnBnClickedBtnFilterHostName)
	ON_BN_CLICKED(IDC_BN_TO_EXCEL, &CPrimaryLogView::OnBnClickedBtnToExcel)
	ON_NOTIFY(TCN_SELCHANGE, IDC_HOSTLOG_TAB, &CPrimaryLogView::OnTcnSelchangeHostlogTab)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PACKAGE, &CPrimaryLogView::OnBnClickedButtonFilterPackage)
	ON_MESSAGE(WM_CHANGE_TAB, OnChangeTab)
END_MESSAGE_MAP()


// CPrimaryLogView 진단입니다.

#ifdef _DEBUG
void CPrimaryLogView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrimaryLogView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrimaryLogView 메시지 처리기입니다.
void CPrimaryLogView::OnBnClickedButtonFilterPackage()
{
	m_strPackageId=_T("");

	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, info.szPackage);
	}

}

int CPrimaryLogView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}

void CPrimaryLogView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if(GetSafeHwnd())
	{
		m_Reposition.Move();
	}
}

void CPrimaryLogView::GetFilter()
{ 
	ubcConfig aIni("UBCManager");
	ciString buf;
	aIni.get("LOG-FILTER","PackageId", buf);
	m_strPackageId = buf.c_str();
}


void CPrimaryLogView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

    CSize scrollSize(0,0);
    SetScrollSizes(MM_TEXT, scrollSize);

	GetFilter();
	this->m_filterPackage.SetWindowText(m_strPackageId);

	m_btnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnExcelSave.LoadBitmap(IDB_BUTTON_EXCEL, RGB(255,255,255));

	m_btnRefresh.SetToolTipText(LoadStringById(IDS_PACKAGE_CHNG_LOG_BTN001));
	m_btnExcelSave.SetToolTipText(LoadStringById(IDS_PACKAGE_CHNG_LOG_BTN002));

	CTime tmCur = CTime::GetCurrentTime();
	m_dtcFilterEnd.SetTime(&tmCur);
	InitDateRange(m_dtcFilterEnd);

	CTime tmStartDate = tmCur - CTimeSpan(7, 0, 0, 0);
	m_dtcFilterStart.SetTime(&tmStartDate);
	InitDateRange(m_dtcFilterStart);



	InitTab();

	m_Reposition.SetParentRect(m_rcClient);
	m_Reposition.RemoveAll();
	m_Reposition.AddControl(&m_tcLogTab, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	for(int i=0; i<eMaxTab; i++)
		m_Reposition.AddControl(&m_lcLog[i], REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}

void CPrimaryLogView::InitTab()
{
	CLogBrowser* pLB = NULL;

	TCITEM tci = {0};
	tci.mask = TCIF_PARAM;

	// 컨텐츠 패키지 변경명령 로그
	m_tcLogTab.InsertItem(eApplyLog, LoadStringById(IDS_PRIMARYLOGVIEW_STR002), eApplyLog);
	pLB = (CLogBrowser*)new CApplyLogBrowser(&m_lcLog[0]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eApplyLog, &tci);

	// 컨텐츠 패키지 변경결과 로그
	m_tcLogTab.InsertItem(eScheduleStateLog, LoadStringById(IDS_PRIMARYLOGVIEW_STR003), eScheduleStateLog);
	pLB = (CLogBrowser*)new CScheduleStateLogBrowser(&m_lcLog[1]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eScheduleStateLog, &tci);

	// Power On/Off 로그
	m_tcLogTab.InsertItem(ePowerStateLog, LoadStringById(IDS_PRIMARYLOGVIEW_STR004), ePowerStateLog);
	pLB = (CLogBrowser*)new CPowerStateLogBrowser(&m_lcLog[2]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(ePowerStateLog, &tci);

	// Connection 로그
	m_tcLogTab.InsertItem(eConnectionStateLog, LoadStringById(IDS_PRIMARYLOGVIEW_STR005), eConnectionStateLog);
	pLB = (CLogBrowser*)new CConnectionStateLogBrowser(&m_lcLog[3]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eConnectionStateLog, &tci);

	// DownloadSummary 로그
	m_tcLogTab.InsertItem(eDownloadSummaryLog, LoadStringById(IDS_LOG_DOWNLOAD_STR001), eDownloadSummaryLog);
	pLB = (CLogBrowser*)new CSubDownloadSummaryLogBrowser(&m_lcLog[eDownloadSummaryLog]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eDownloadSummaryLog, &tci);

	// User 로그
	m_tcLogTab.InsertItem(eUserLog, LoadStringById(IDS_LOG_LOGIN_STR001), eUserLog);
	pLB = (CLogBrowser*)new CSubUserLogBrowser(&m_lcLog[eUserLog]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eUserLog, &tci);

	// BP 로그
	m_tcLogTab.InsertItem(eBPLog, LoadStringById(IDS_LOG_BP_STR001), eBPLog);
	pLB = (CLogBrowser*)new CSubBPLogBrowser(&m_lcLog[eBPLog]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eBPLog, &tci);

	// PLAY 로그
	m_tcLogTab.InsertItem(ePlayLog, LoadStringById(IDS_LOG_PLAY_STR001), ePlayLog);
	pLB = (CLogBrowser*)new CSubPlayLogBrowser(&m_lcLog[ePlayLog]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(ePlayLog, &tci);

	// FAULT 로그
	m_tcLogTab.InsertItem(eFaultLog, LoadStringById(IDS_LOG_FAULT_STR001), eFaultLog);
	pLB = (CLogBrowser*)new CSubFaultLogBrowser(&m_lcLog[eFaultLog]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eFaultLog, &tci);
}

void CPrimaryLogView::SetCurrentTab(int nTabIdx)
{
	m_tcLogTab.SetCurSel(nTabIdx);

	for(int i=0; i<eMaxTab; i++)
	{
		m_lcLog[i].ShowWindow( (nTabIdx == i) ? SW_NORMAL : SW_HIDE );

	}
	TCITEM tci;
	tci.mask = TCIF_PARAM;
	m_tcLogTab.GetItem(nTabIdx, &tci);
	CLogBrowser* pLB = (CLogBrowser*)tci.lParam;
	if(pLB)
	{
		pLB->VisibleFilter(this);
	}

}

void CPrimaryLogView::RefreshList()
{
	int tab_idx = m_tcLogTab.GetCurSel();
	if(tab_idx<0 || tab_idx>=eMaxTab) return;

	TCITEM tci;
	tci.mask = TCIF_PARAM;
	m_tcLogTab.GetItem(tab_idx, &tci);

	CLogBrowser* pLB = (CLogBrowser*)tci.lParam;
	if(pLB)
	{
		CTime tm_start, tm_end;
		m_dtcFilterStart.GetTime(tm_start);
		m_dtcFilterEnd.GetTime(tm_end);

		this->m_filterPackage.GetWindowText(m_strPackageId);

		pLB->SetFilter(m_strPackageId,m_strHostId, tm_start, tm_end);
		pLB->RefreshList();
	}
}

void CPrimaryLogView::OnBnClickedBtnRefresh()
{
	RefreshList();
}

void CPrimaryLogView::OnBnClickedBtnFilterHostName()
{
	CSelectHostDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	m_strHostId = _T("");
	m_strHostName = _T("");

	if(dlg.DoModal() == IDOK)
	{
		m_strHostId = dlg.m_strHostId;
		m_strHostName = dlg.m_strHostName;
	}

	m_editHostName.SetWindowText(m_strHostName);
}

void CPrimaryLogView::OnBnClickedBtnToExcel()
{
	CHostExcelSave dlg;

	int tab_idx = m_tcLogTab.GetCurSel();
	if(tab_idx<0 || tab_idx>=eMaxTab) return;

	char title[256] = {0};
	TCITEM tci;
	tci.pszText = title;
	tci.cchTextMax = 256;
	tci.mask = TCIF_TEXT;
	m_tcLogTab.GetItem(tab_idx, &tci);

	// 슬래시(/)문자 치환 -> 엑셀에서 사용시 에러
	CString str_title = title;
	str_title.Replace("/", ",");

	CString strExcelFile = dlg.Save(str_title, m_lcLog[tab_idx]);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}

void CPrimaryLogView::OnTcnSelchangeHostlogTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;
	SetCurrentTab(m_tcLogTab.GetCurSel());
}

LRESULT CPrimaryLogView::OnChangeTab(WPARAM wParam, LPARAM lParam)
{
	SetCurrentTab(wParam);
	return 0;
}



////////////////////////////////
// 컨텐츠 패키지 변경명령 로그
////////////////////////////////
void CApplyLogBrowser::InitList()
{
	m_pLogListCtrl->SetExtendedStyle(m_pLogListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_LogColumn[eSite     ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST001);
	m_LogColumn[eHost     ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST002);
	m_LogColumn[ePackage  ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST003);
	m_LogColumn[eApplyTime] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST004);
	m_LogColumn[eHow      ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST005);
	m_LogColumn[eWho      ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST006);
	m_LogColumn[eWhy      ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST007);

	m_pLogListCtrl->InsertColumn(eSite     , m_LogColumn[eSite     ], LVCFMT_LEFT  ,  60);
	m_pLogListCtrl->InsertColumn(eHost     , m_LogColumn[eHost     ], LVCFMT_LEFT  ,  80);
	m_pLogListCtrl->InsertColumn(ePackage  , m_LogColumn[ePackage  ], LVCFMT_LEFT  , 220);
	m_pLogListCtrl->InsertColumn(eApplyTime, m_LogColumn[eApplyTime], LVCFMT_CENTER, 120);
	m_pLogListCtrl->InsertColumn(eHow      , m_LogColumn[eHow      ], LVCFMT_LEFT  ,  80);
	m_pLogListCtrl->InsertColumn(eWho      , m_LogColumn[eWho      ], LVCFMT_LEFT  , 180);
	m_pLogListCtrl->InsertColumn(eWhy      , m_LogColumn[eWhy      ], LVCFMT_LEFT  ,  80);

	m_pLogListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CApplyLogBrowser::RefreshList()
{
	CWaitMessageBox wait;

	m_pLogListCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();

	// 작성일자
	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	strWhere.Format(_T("applyTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);
	strWhere += _T(" order by applyTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetApplyInfoList( m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	m_pLogListCtrl->SetRedraw(FALSE);

	POSITION pos = m_LogDataList.GetHeadPosition();
	for( int nRow=0; pos!=NULL; nRow++ )
	{
		POSITION posOld = pos;
		SApplyLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTime = CTime(info.applyTime);

		m_pLogListCtrl->InsertItem(nRow, "");
		m_pLogListCtrl->SetItemText(nRow, eSite     , info.siteId   );
		m_pLogListCtrl->SetItemText(nRow, eHost     , info.hostId   );
		m_pLogListCtrl->SetItemText(nRow, ePackage  , info.programId);
		m_pLogListCtrl->SetItemText(nRow, eApplyTime, (info.applyTime == 0 ? "" : tmApplyTime.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eHow      , info.how      );
		m_pLogListCtrl->SetItemText(nRow, eWho      , info.who      );
		m_pLogListCtrl->SetItemText(nRow, eWhy      , info.why      );

		m_pLogListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);
	}

	m_pLogListCtrl->SetRedraw(TRUE);
}


////////////////////////////////
// 컨텐츠 패키지 변경결과 로그
////////////////////////////////
void CScheduleStateLogBrowser::InitList()
{
	m_pLogListCtrl->SetExtendedStyle(m_pLogListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_LogColumn[eSite     ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST001);
	m_LogColumn[eHost     ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST002);
	m_LogColumn[eEventTime] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST004);
	m_LogColumn[eLastSchedule1] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST008);
	m_LogColumn[eAutoSchedule1] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST009);
	m_LogColumn[eCurrentSchedule1] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST010);

	m_pLogListCtrl->InsertColumn(eSite     , m_LogColumn[eSite     ], LVCFMT_LEFT  ,  60);
	m_pLogListCtrl->InsertColumn(eHost     , m_LogColumn[eHost     ], LVCFMT_LEFT  ,  80);
	m_pLogListCtrl->InsertColumn(eEventTime, m_LogColumn[eEventTime], LVCFMT_CENTER, 120);
	m_pLogListCtrl->InsertColumn(eLastSchedule1      , m_LogColumn[eLastSchedule1  ], LVCFMT_LEFT  ,200);
	m_pLogListCtrl->InsertColumn(eAutoSchedule1      , m_LogColumn[eAutoSchedule1  ], LVCFMT_LEFT  ,200);
	m_pLogListCtrl->InsertColumn(eCurrentSchedule1      , m_LogColumn[eCurrentSchedule1      ], LVCFMT_LEFT  ,200);

	m_pLogListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CScheduleStateLogBrowser::RefreshList()
{
	CWaitMessageBox wait;

	m_pLogListCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();


	// 작성일자
	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	strWhere.Format(_T("eventTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);
	strWhere += _T(" order by eventTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetScheduleStateInfoList( m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	m_pLogListCtrl->SetRedraw(FALSE);

	POSITION pos = m_LogDataList.GetHeadPosition();
	for( int nRow=0; pos!=NULL; nRow++ )
	{
		POSITION posOld = pos;
		SScheduleStateLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTime = CTime(info.eventTime);

		m_pLogListCtrl->InsertItem(nRow, "");
		m_pLogListCtrl->SetItemText(nRow, eSite     , info.siteId   );
		m_pLogListCtrl->SetItemText(nRow, eHost     , info.hostId   );
		m_pLogListCtrl->SetItemText(nRow, eEventTime, (info.eventTime == 0 ? "" : tmApplyTime.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eLastSchedule1  , info.lastSchedule1);
		m_pLogListCtrl->SetItemText(nRow, eAutoSchedule1      , info.autoSchedule1      );
		m_pLogListCtrl->SetItemText(nRow, eCurrentSchedule1      , info.currentSchedule1      );

		m_pLogListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);
	}

	m_pLogListCtrl->SetRedraw(TRUE);
}


////////////////////////////////
// Power 로그
////////////////////////////////
void CPowerStateLogBrowser::InitList()
{
	m_pLogListCtrl->SetExtendedStyle(m_pLogListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_LogColumn[eSite     ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST001);
	m_LogColumn[eHost     ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST002);
	m_LogColumn[eEventTime] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST004);
	m_LogColumn[eBootUpTime] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST011);
	m_LogColumn[eBootDownTime] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST012);

	m_pLogListCtrl->InsertColumn(eSite     , m_LogColumn[eSite     ], LVCFMT_LEFT  ,  60);
	m_pLogListCtrl->InsertColumn(eHost     , m_LogColumn[eHost     ], LVCFMT_LEFT  ,  80);
	m_pLogListCtrl->InsertColumn(eEventTime, m_LogColumn[eEventTime], LVCFMT_CENTER, 120);
	m_pLogListCtrl->InsertColumn(eBootUpTime      , m_LogColumn[eBootUpTime  ], LVCFMT_LEFT  ,120);
	m_pLogListCtrl->InsertColumn(eBootDownTime      , m_LogColumn[eBootDownTime  ], LVCFMT_LEFT  ,120);

	m_pLogListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CPowerStateLogBrowser::RefreshList()
{
	CWaitMessageBox wait;

	m_pLogListCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();

	// 작성일자
	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	strWhere.Format(_T("eventTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);
	strWhere += _T(" order by eventTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetPowerStateLogByHost( m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	m_pLogListCtrl->SetRedraw(FALSE);

	POSITION pos = m_LogDataList.GetHeadPosition();
	for( int nRow=0; pos!=NULL; nRow++ )
	{
		POSITION posOld = pos;
		SPowerStateLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTime = CTime(info.eventTime);
		CTime tmBootUpTime = CTime(info.bootUpTime);
		CTime tmBootDownTime = CTime(info.bootDownTime);

		m_pLogListCtrl->InsertItem(nRow, "");
		m_pLogListCtrl->SetItemText(nRow, eSite     , info.siteId   );
		m_pLogListCtrl->SetItemText(nRow, eHost     , info.hostId   );
		m_pLogListCtrl->SetItemText(nRow, eEventTime, (info.eventTime == 0 ? "" : tmApplyTime.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eBootUpTime, (info.bootUpTime == 0 ? "" : tmBootUpTime.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eBootDownTime, (info.bootDownTime == 0 ? "" : tmBootDownTime.Format("%Y/%m/%d %H:%M:%S")));

		m_pLogListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);
	}

	m_pLogListCtrl->SetRedraw(TRUE);
}


////////////////////////////////
// Connection 로그
////////////////////////////////
void CConnectionStateLogBrowser::InitList()
{
	m_pLogListCtrl->SetExtendedStyle(m_pLogListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_LogColumn[eSite     ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST001);
	m_LogColumn[eHost     ] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST002);
	m_LogColumn[eEventTime] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST004);
	m_LogColumn[eOperationalState] = LoadStringById(IDS_PRIMARYLOGVIEW_LIST013);

	m_pLogListCtrl->InsertColumn(eSite     , m_LogColumn[eSite     ], LVCFMT_LEFT  ,  60);
	m_pLogListCtrl->InsertColumn(eHost     , m_LogColumn[eHost     ], LVCFMT_LEFT  ,  80);
	m_pLogListCtrl->InsertColumn(eEventTime, m_LogColumn[eEventTime], LVCFMT_CENTER, 120);
	m_pLogListCtrl->InsertColumn(eOperationalState      , m_LogColumn[eOperationalState  ], LVCFMT_LEFT  ,200);

	m_pLogListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CConnectionStateLogBrowser::RefreshList()
{
	CWaitMessageBox wait;

	m_pLogListCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();

	// 작성일자
	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	strWhere.Format(_T("eventTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);
	strWhere += _T(" order by eventTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetConnectionStateLogByHost( m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	m_pLogListCtrl->SetRedraw(FALSE);

	CString str_conn    = LoadStringById(IDS_PRIMARYLOGVIEW_LIST014);;
	CString str_disconn = LoadStringById(IDS_PRIMARYLOGVIEW_LIST015);;

	POSITION pos = m_LogDataList.GetHeadPosition();
	for( int nRow=0; pos!=NULL; nRow++ )
	{
		POSITION posOld = pos;
		SConnectionStateLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTime = CTime(info.eventTime);

		m_pLogListCtrl->InsertItem(nRow, "");
		m_pLogListCtrl->SetItemText(nRow, eSite     , info.siteId   );
		m_pLogListCtrl->SetItemText(nRow, eHost     , info.hostId   );
		m_pLogListCtrl->SetItemText(nRow, eEventTime, (info.eventTime == 0 ? "" : tmApplyTime.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eOperationalState, (info.operationalState ? str_conn : str_disconn));

		m_pLogListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);
	}

	m_pLogListCtrl->SetRedraw(TRUE);
}

////////////////////////////////
// DownloadSummary 로그
////////////////////////////////
void CSubDownloadSummaryLogBrowser::InitList()
{
	m_pLogListCtrl->SetExtendedStyle(m_pLogListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_LogColumn[eSiteName    ] =		LoadStringById(IDS_DOWNLOADSUMMARY_BTN002);
	m_LogColumn[eHostName     ] =		LoadStringById(IDS_DOWNLOADSTATE_LIST011);
	m_LogColumn[eProgramId     ] =		LoadStringById(IDS_DOWNLOADSTATE_LIST003);

	if(GetEnvPtr()->m_strCustomer == "KPOST"){
		m_LogColumn[eBrwId     ] =			"Size";
	}else{
		m_LogColumn[eBrwId     ] =			LoadStringById(IDS_DOWNLOADSTATE_LIST002);
	}
	m_LogColumn[eProgramState] =		LoadStringById(IDS_DOWNLOADSTATE_LIST001);
	m_LogColumn[eProgramStartTime] =	LoadStringById(IDS_DOWNLOADSTATE_LIST008);
	m_LogColumn[eProgramEndTime] =		LoadStringById(IDS_DOWNLOADSTATE_LIST009);
	m_LogColumn[eProgress] =			LoadStringById(IDS_DOWNLOADSTATE_LIST007);

	m_pLogListCtrl->InsertColumn(eSiteName, m_LogColumn[eSiteName     ], LVCFMT_LEFT  ,  120);
	m_pLogListCtrl->InsertColumn(eHostName, m_LogColumn[eHostName     ], LVCFMT_LEFT  ,  120);
	m_pLogListCtrl->InsertColumn(eProgramId, m_LogColumn[eProgramId  ], LVCFMT_LEFT  ,200);
	m_pLogListCtrl->InsertColumn(eBrwId, m_LogColumn[eBrwId  ], LVCFMT_LEFT  ,40);
	m_pLogListCtrl->InsertColumn(eProgramState, m_LogColumn[eProgramState  ], LVCFMT_LEFT  ,120);
	m_pLogListCtrl->InsertColumn(eProgramStartTime, m_LogColumn[eProgramStartTime], LVCFMT_CENTER, 120);
	m_pLogListCtrl->InsertColumn(eProgramEndTime, m_LogColumn[eProgramEndTime], LVCFMT_CENTER, 120);
	m_pLogListCtrl->InsertColumn(eProgress, m_LogColumn[eProgress  ], LVCFMT_LEFT  ,80);

	// 제일 마지막에 할것...
	m_pLogListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubDownloadSummaryLogBrowser::RefreshList()
{
	CWaitMessageBox wait;

	m_pLogListCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();


	CString packageWhere;
	if(!m_strPackageId.IsEmpty()){
		packageWhere.Format(_T("programId like '%%%s%%' and"), m_strPackageId);
	}

	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	// 작성일자
	strWhere.Format(_T("%s programStartTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), packageWhere,strStartDate, strEndDate);

	strWhere += _T(" order by programStartTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetDownloadSummary((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite),
												m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	m_pLogListCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SDownloadSummaryInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTimeStart = CTime(info.programStartTime);
		CTime tmApplyTimeEnd = CTime(info.programEndTime);

		m_pLogListCtrl->InsertItem(nRow, "");
		m_pLogListCtrl->SetItemText(nRow, eSiteName, info.siteName   );
		m_pLogListCtrl->SetItemText(nRow, eHostName, info.hostId   );
		m_pLogListCtrl->SetItemText(nRow, eProgramId, info.programId   );

		if(GetEnvPtr()->m_strCustomer == "KPOST"){
			CString MbyteStr;
			MbyteStr.Format("%d MB", info.brwId);
			m_pLogListCtrl->SetItemText(nRow, eBrwId,		MbyteStr);
		}else{
			m_pLogListCtrl->SetItemText(nRow, eBrwId,		(info.brwId==0 ? "1" : "2"));
		}

		m_pLogListCtrl->SetItemText(nRow, eProgramState,	GetStateString(info.programState));
		m_pLogListCtrl->SetItemText(nRow, eProgramStartTime,	(info.programStartTime == 0 ? "" : tmApplyTimeStart.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eProgramEndTime,	(info.programEndTime == 0 ? "" : tmApplyTimeEnd.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eProgress, info.progress   );

		m_pLogListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	m_pLogListCtrl->SetRedraw(TRUE);
}

CString CSubDownloadSummaryLogBrowser::GetStateString(int nValue)
{
	switch(nValue)
	{
	case E_STATE_INIT			     : return LoadStringById(IDS_DOWNLOADSTATE_STR011); //초기상태
	case E_STATE_DOWNLOADING	     : return LoadStringById(IDS_DOWNLOADSTATE_STR012); //다운로드 상태
	case E_STATE_PARTIAL_FAIL	     : return LoadStringById(IDS_DOWNLOADSTATE_STR013); //일부파일 실패
	case E_STATE_COMPLETE_FAIL	     : return LoadStringById(IDS_DOWNLOADSTATE_STR014); //전체 실패
	case E_STATE_COMPLETE_SUCCESS    : return LoadStringById(IDS_DOWNLOADSTATE_STR015); //전체 성공
	case E_STATE_COMPLETE_LOCAL_EXIST: return LoadStringById(IDS_DOWNLOADSTATE_STR015); //전체 성공
	default: return LoadStringById(IDS_DOWNLOADSTATE_STR017); //시작안함
	}

	return _T("");
}

////////////////////////////////
// UserLog 로그
////////////////////////////////
void CSubUserLogBrowser::InitList()
{
	m_pLogListCtrl->SetExtendedStyle(m_pLogListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_LogColumn[eSiteId    ] =			LoadStringById(IDS_DOWNLOADSUMMARY_BTN002);
	m_LogColumn[eUserId     ] =			LoadStringById(IDS_USERVIEW_LST002);
	m_LogColumn[eLoginTime     ] =		LoadStringById(IDS_USERLOG_LOGINTIME);
	m_LogColumn[eVia     ] =			LoadStringById(IDS_USERLOG_VIA);
	m_LogColumn[eResult] =				LoadStringById(IDS_USERLOG_RESULT);

	m_pLogListCtrl->InsertColumn(eSiteId, m_LogColumn[eSiteId     ], LVCFMT_LEFT  ,  120);
	m_pLogListCtrl->InsertColumn(eUserId, m_LogColumn[eUserId     ], LVCFMT_LEFT  ,  120);
	m_pLogListCtrl->InsertColumn(eLoginTime, m_LogColumn[eLoginTime  ], LVCFMT_LEFT  ,120);
	m_pLogListCtrl->InsertColumn(eVia, m_LogColumn[eVia  ], LVCFMT_LEFT  ,200);
	m_pLogListCtrl->InsertColumn(eResult, m_LogColumn[eResult  ], LVCFMT_LEFT  ,100);

	// 제일 마지막에 할것...
	m_pLogListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubUserLogBrowser::RefreshList()
{
	CWaitMessageBox wait;

	m_pLogListCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();


	CString userIdWhere;
	if(!m_strUserId.IsEmpty()){
		userIdWhere.Format(_T("userId like '%%%s%%' and"), m_strUserId);
	}

	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	// 작성일자
	strWhere.Format(_T("%s loginTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), userIdWhere,strStartDate, strEndDate);

	strWhere += _T(" order by loginTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetUserLog((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite),
												 m_LogDataList, strWhere ))
	{
		return;
	}

	m_pLogListCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SUserLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmLoginTime = CTime(info.loginTime);

		m_pLogListCtrl->InsertItem(nRow, "");
		m_pLogListCtrl->SetItemText(nRow, eSiteId, info.siteId   );
		m_pLogListCtrl->SetItemText(nRow, eUserId, info.userId   );
		m_pLogListCtrl->SetItemText(nRow, eLoginTime,	(info.loginTime == 0 ? "" : tmLoginTime.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eVia, info.via   );
		m_pLogListCtrl->SetItemText(nRow, eResult, info.result   );

		m_pLogListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	m_pLogListCtrl->SetRedraw(TRUE);
}

////////////////////////////////
// BPLog 로그
////////////////////////////////
void CSubBPLogBrowser::InitList()
{
	m_pLogListCtrl->SetExtendedStyle(m_pLogListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_LogColumn[eSiteId    ] =			LoadStringById(IDS_DOWNLOADSUMMARY_BTN002);
	m_LogColumn[eBpName] =				LoadStringById(IDS_PLANVIEW_LIST002);
	m_LogColumn[eTouchTime     ] =		LoadStringById(IDS_BPLOG_TOUCHTIME);
	m_LogColumn[eWho     ] =			LoadStringById(IDS_BPLOG_WHO);
	m_LogColumn[eAction] =				LoadStringById(IDS_BPLOG_ACTION);

	m_pLogListCtrl->InsertColumn(eSiteId, m_LogColumn[eSiteId     ], LVCFMT_LEFT  ,  120);
	m_pLogListCtrl->InsertColumn(eBpName, m_LogColumn[eBpName  ], LVCFMT_LEFT  ,240);
	m_pLogListCtrl->InsertColumn(eTouchTime, m_LogColumn[eTouchTime  ], LVCFMT_LEFT  ,120);
	m_pLogListCtrl->InsertColumn(eWho, m_LogColumn[eWho  ], LVCFMT_LEFT  ,100);
	m_pLogListCtrl->InsertColumn(eAction, m_LogColumn[eAction  ], LVCFMT_LEFT  ,100);

	// 제일 마지막에 할것...
	m_pLogListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubBPLogBrowser::RefreshList()
{
	CWaitMessageBox wait;

	m_pLogListCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();


	CString siteIdWhere;
	if(!m_strSiteId.IsEmpty()){
		siteIdWhere.Format(_T("siteId like '%%%s%%' and"), m_strSiteId);
	}

	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	// 작성일자
	strWhere.Format(_T("%s touchTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), siteIdWhere,strStartDate, strEndDate);

	strWhere += _T(" order by touchTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetBPLog((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite),
												 m_LogDataList, strWhere ))
	{
		return;
	}

	m_pLogListCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SBPLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmTouchTime = CTime(info.touchTime);

		m_pLogListCtrl->InsertItem(nRow, "");
		m_pLogListCtrl->SetItemText(nRow, eSiteId, info.siteId   );
		m_pLogListCtrl->SetItemText(nRow, eBpName, info.bpName   );
		m_pLogListCtrl->SetItemText(nRow, eTouchTime,	(info.touchTime == 0 ? "" : tmTouchTime.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eWho, info.who   );
		m_pLogListCtrl->SetItemText(nRow, eAction, info.action   );

		m_pLogListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	m_pLogListCtrl->SetRedraw(TRUE);
}

////////////////////////////////
// PlayLog 로그
////////////////////////////////
void CSubPlayLogBrowser::InitList()
{
	m_pLogListCtrl->SetExtendedStyle(m_pLogListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	//eSiteId,eHostId,ePlayDate, eProgramId,eContentsName,eFilename,ePlayCount, ePlayTime, eFailCount,

	m_LogColumn[eSiteId    ] =			LoadStringById(IDS_DOWNLOADSUMMARY_BTN002);
	m_LogColumn[eHostId] =				LoadStringById(IDS_DOWNLOADSUMMARY_BTN003);//
	m_LogColumn[ePlayDate] =			LoadStringById(IDS_PLAYLOG_PLAYDATE);
	m_LogColumn[eProgramId     ] =		LoadStringById(IDS_SITEDELDLG_STR002);//
	m_LogColumn[eContentsName     ] =	LoadStringById(IDS_DOWNLOADSTATE_LIST004);
	m_LogColumn[eFilename] =			LoadStringById(IDS_DOWNLOADSTATE_LIST006);//
	m_LogColumn[ePlayCount] =			LoadStringById(IDS_PLAYLOG_PLAYCOUNT);
	m_LogColumn[ePlayTime] =			LoadStringById(IDS_PLAYLOG_PLAYTIME);
	m_LogColumn[eFailCount] =			LoadStringById(IDS_PLAYLOG_FAILCOUNT);

	m_pLogListCtrl->InsertColumn(eSiteId,		m_LogColumn[eSiteId     ],		LVCFMT_LEFT  ,80);
	m_pLogListCtrl->InsertColumn(eHostId,		m_LogColumn[eHostId  ],			LVCFMT_LEFT  ,80);
	m_pLogListCtrl->InsertColumn(ePlayDate,		m_LogColumn[ePlayDate  ],		LVCFMT_LEFT  ,80);
	m_pLogListCtrl->InsertColumn(eProgramId,	m_LogColumn[eProgramId  ],		LVCFMT_LEFT  ,200);
	m_pLogListCtrl->InsertColumn(eContentsName, m_LogColumn[eContentsName  ],	LVCFMT_LEFT  ,200);
	m_pLogListCtrl->InsertColumn(eFilename,		m_LogColumn[eFilename  ],		LVCFMT_LEFT  ,200);
	m_pLogListCtrl->InsertColumn(ePlayCount,	m_LogColumn[ePlayCount  ],		LVCFMT_LEFT  ,60);
	m_pLogListCtrl->InsertColumn(ePlayTime,		m_LogColumn[ePlayTime  ],		LVCFMT_LEFT  ,100);
	m_pLogListCtrl->InsertColumn(eFailCount,	m_LogColumn[eFailCount  ],		LVCFMT_LEFT  ,60);

	// 제일 마지막에 할것...
	m_pLogListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubPlayLogBrowser::RefreshList()
{

	if(m_strHostId.IsEmpty()){
		//playLog 는 워낙 많아서 다 보여줄수 없음.
		UbcMessageBox(LoadStringById(IDS_PLAYLOG_MSG001));
		return;
	}


	CWaitMessageBox wait;

	m_pLogListCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();


	CString siteIdWhere;
	if(!m_strSiteId.IsEmpty()){
		siteIdWhere.Format(_T("siteId like '%%%s%%' and"), m_strSiteId);
	}

	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	// 작성일자
	strWhere.Format(_T("%s playDate between CAST('%s' as datetime) and CAST('%s' as datetime)"), siteIdWhere,strStartDate, strEndDate);

	strWhere += _T(" order by playDate desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetPlayLog((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite),
												 m_strHostId,m_LogDataList, strWhere ))
	{
		UbcMessageBox("GetPlayLog failed");
		return;
	}

	m_pLogListCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	char buf[20];
	while(pos)
	{
		POSITION posOld = pos;
		SPlayLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmPlayDate = CTime(info.playDate);

	//eSiteId,eHostId,ePlayDate, eProgramId,eContentsName,eFilename,ePlayCount, ePlayTime, eFailCount,

		m_pLogListCtrl->InsertItem(nRow, "");
		m_pLogListCtrl->SetItemText(nRow, eSiteId, info.siteId   );
		m_pLogListCtrl->SetItemText(nRow, eHostId, info.hostId   );
		m_pLogListCtrl->SetItemText(nRow, ePlayDate,	(info.playDate == 0 ? "" : tmPlayDate.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eProgramId, info.programId   );
		m_pLogListCtrl->SetItemText(nRow, eContentsName, info.contentsName   );
		m_pLogListCtrl->SetItemText(nRow, eFilename, info.filename   );
		
		memset(buf,0x00,sizeof(buf));
		sprintf(buf,"%ld", info.playCount);
		m_pLogListCtrl->SetItemText(nRow, ePlayCount, buf );

		memset(buf,0x00,sizeof(buf));
		sprintf(buf,"%ld", info.playTime);
		m_pLogListCtrl->SetItemText(nRow, ePlayTime, buf   );

		memset(buf,0x00,sizeof(buf));
		sprintf(buf,"%ld", info.failCount);
		m_pLogListCtrl->SetItemText(nRow, eFailCount, buf   );

		m_pLogListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	m_pLogListCtrl->SetRedraw(TRUE);
}

////////////////////////////////
// FaultLog 로그
////////////////////////////////
void CSubFaultLogBrowser::InitList()
{
	m_pLogListCtrl->SetExtendedStyle(m_pLogListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	//eSiteId,eHostId,eEventTime,eUpdateTime,eSeverity,eProbableCause,eCounter,eAdditionalText

	m_LogColumn[eSiteId    ] =			LoadStringById(IDS_DOWNLOADSUMMARY_BTN002);
	m_LogColumn[eHostId] =				LoadStringById(IDS_DOWNLOADSUMMARY_BTN003);//
	m_LogColumn[eEventTime     ] =		LoadStringById(IDS_FAULTMNGVIEW_LIST006);//
	m_LogColumn[eUpdateTime     ] =		LoadStringById(IDS_FAULTMNGVIEW_LIST007);
	m_LogColumn[eSeverity] =			LoadStringById(IDS_FAULTMNGVIEW_LIST004);//
	m_LogColumn[eProbableCause] =		LoadStringById(IDS_FAULTMNGVIEW_LIST005);
	m_LogColumn[eCounter] =				LoadStringById(IDS_FAULTMNGVIEW_LIST003);
	m_LogColumn[eAdditionalText] =		LoadStringById(IDS_FAULTMNGVIEW_LIST008);

	m_pLogListCtrl->InsertColumn(eSiteId,			m_LogColumn[eSiteId     ],		LVCFMT_LEFT  ,80);
	m_pLogListCtrl->InsertColumn(eHostId,			m_LogColumn[eHostId  ],			LVCFMT_LEFT  ,80);
	m_pLogListCtrl->InsertColumn(eEventTime,		m_LogColumn[eEventTime  ],		LVCFMT_LEFT  ,120);
	m_pLogListCtrl->InsertColumn(eUpdateTime,		m_LogColumn[eUpdateTime  ],		LVCFMT_LEFT  ,120);
	m_pLogListCtrl->InsertColumn(eSeverity,			m_LogColumn[eSeverity  ],		LVCFMT_LEFT  ,60);
	m_pLogListCtrl->InsertColumn(eProbableCause,	m_LogColumn[eProbableCause  ],	LVCFMT_LEFT  ,200);
	m_pLogListCtrl->InsertColumn(eCounter,			m_LogColumn[eCounter  ],		LVCFMT_LEFT  ,50);
	m_pLogListCtrl->InsertColumn(eAdditionalText,	m_LogColumn[eAdditionalText  ],			LVCFMT_LEFT  ,200);

	// 제일 마지막에 할것...
	m_pLogListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubFaultLogBrowser::RefreshList()
{
	/*
	if(m_strHostId.IsEmpty()){
		//FaultLog 는 워낙 많아서 다 보여줄수 없음.
		UbcMessageBox(LoadStringById(IDS_PLAYLOG_MSG001));
		return;
	}
	*/

	CWaitMessageBox wait;

	m_pLogListCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();


	CString siteIdWhere;
	if(!m_strSiteId.IsEmpty()){
		siteIdWhere.Format(_T("siteId like '%%%s%%' and"), m_strSiteId);
	}

	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	// 작성일자
	strWhere.Format(_T("%s updateTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), siteIdWhere,strStartDate, strEndDate);

	strWhere += _T(" order by updateTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetFaultLog((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite),
												 m_strHostId,m_LogDataList, strWhere ))
	{
		UbcMessageBox("GetFaultLog failed");
		return;
	}

	m_pLogListCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	char buf[20];
	while(pos)
	{
		POSITION posOld = pos;
		SFaultInfo& info = m_LogDataList.GetNext(pos);

		CTime tmEventTime = CTime(info.eventTime);
		CTime tmUpdateTime = CTime(info.updateTime);

		//eSiteId,eHostId,eEventTime,eUpdateTime,eSource,eSeverity,eProbableCause,eCounter,eAdditionalText

		m_pLogListCtrl->InsertItem(nRow, "");
		m_pLogListCtrl->SetItemText(nRow, eSiteId, info.siteId   );
		m_pLogListCtrl->SetItemText(nRow, eHostId, info.hostId   );
		m_pLogListCtrl->SetItemText(nRow, eEventTime,	(info.eventTime == 0 ? "" : tmEventTime.Format("%Y/%m/%d %H:%M:%S")));
		m_pLogListCtrl->SetItemText(nRow, eUpdateTime,	(info.updateTime == 0 ? "" : tmUpdateTime.Format("%Y/%m/%d %H:%M:%S")));

		char* SEVERITY[6] = {_T("Cleared")
				, _T("Critical")
				, _T("Major")
				, _T("Minor")
				, _T("Warning")
				, _T("Indeter")};

		m_pLogListCtrl->SetItemText(nRow, eSeverity, 
							((info.severity < 0 || info.severity > 5) ? SEVERITY[5] : SEVERITY[info.severity]));
		
		m_pLogListCtrl->SetItemText(nRow, eProbableCause, info.probableCause   );
		
		memset(buf,0x00,sizeof(buf));
		sprintf(buf,"%ld", info.counter);
		m_pLogListCtrl->SetItemText(nRow, eCounter, buf );
		
		m_pLogListCtrl->SetItemText(nRow, eAdditionalText, info.additionalText   );

		m_pLogListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	m_pLogListCtrl->SetRedraw(TRUE);
}

