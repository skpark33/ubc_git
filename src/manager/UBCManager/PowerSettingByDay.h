#pragma once


// CPowerSettingByDay 대화 상자입니다.

class CPowerSettingByDay : public CDialog
{
	DECLARE_DYNAMIC(CPowerSettingByDay)

public:
	CPowerSettingByDay(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPowerSettingByDay();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_POWER_SET_BY_DAY };

	CString			m_strWeekShutdownTime;

protected:
	CButton			m_ckWeek[7];
	CDateTimeCtrl	m_tmShutdownWeek[7];

	void			InitShutdownWeek(int nIndex);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedCheckWeek0();
	afx_msg void OnBnClickedCheckWeek1();
	afx_msg void OnBnClickedCheckWeek2();
	afx_msg void OnBnClickedCheckWeek3();
	afx_msg void OnBnClickedCheckWeek4();
	afx_msg void OnBnClickedCheckWeek5();
	afx_msg void OnBnClickedCheckWeek6();
};
