// HostScreenView.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "HostScreenView.h"
#include "Enviroment.h"
#include "MainFrm.h"
#include "HostView.h"
#include "ProcessView.h"
#include "ubccommon\ubccommon.h"
#include "ubccopcommon\ubccopcommon.h"
#include "UBCCopCommon\CopModule.h"
#include "common\UbcCode.h"
#include "ubccopcommon/SiteSelectDlg.h"
#include "ubccopcommon/PackageSelectDlg.h"
#include "ubccopcommon/ContentsSelectDlg.h"
//#include <common/libCommon/ubcPermition.h>
#include "common/libCommon/ubcIni.h"

#define STR_ADMIN_ON		LoadStringById(IDS_HOSTSCREENVIEW_LST001)
#define STR_ADMIN_OFF		LoadStringById(IDS_HOSTSCREENVIEW_LST002)
#define STR_OPERA_ON		LoadStringById(IDS_HOSTSCREENVIEW_LST003)
#define STR_OPERA_OFF		LoadStringById(IDS_HOSTSCREENVIEW_LST004)

// CHostScreenView
IMPLEMENT_DYNCREATE(CHostScreenView, CFormView)

CHostScreenView::CHostScreenView() : CFormView(CHostScreenView::IDD)
,	m_Reposition(this)
{
}

CHostScreenView::~CHostScreenView()
{
}

void CHostScreenView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_FLASH_HOSTSCREEN, m_fcHost);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_Filter);
	DDX_Control(pDX, IDC_BUTTON_HOST_REF, m_btnRefHost);

	DDX_Control(pDX, IDC_COMBO_FILTER_ADMIN, m_cbAdmin);
	DDX_Control(pDX, IDC_COMBO_FILTER_OPR, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOSTTYPE, m_cbHostType);
	DDX_Control(pDX, IDC_COMBO_FILTER_TAG, m_cbFilterTag);
	DDX_Control(pDX, IDC_KB_ADVANCED_FILTER, m_kbAdvancedFilter);
	DDX_Control(pDX, IDC_COMBO_ADVANCED_FILTER, m_cbAdvancedFilter);
}

BEGIN_MESSAGE_MAP(CHostScreenView, CFormView)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_HOST_REF, OnBnClickedButtonHostrfs)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_SITE, OnBnClickedButtonFilterSite)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_CONTENTS, OnBnClickedButtonFilterContents)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PACKAGE, OnBnClickedButtonFilterPackage)
	ON_REGISTERED_MESSAGE(WM_FILTER_HOST_CHANGE, OnFilterHostChanged)
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
	ON_BN_CLICKED(IDC_KB_ADVANCED_FILTER, &CHostScreenView::OnBnClickedKbAdvancedFilter)
END_MESSAGE_MAP()

// CHostScreenView diagnostics
#ifdef _DEBUG
void CHostScreenView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CHostScreenView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CHostScreenView message handlers
int CHostScreenView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	GetClientRect(&m_rcClient);

	return 0;
}

void CHostScreenView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_HOSTSCREENVIEW_STR001));

	CodeItemList listHostType;
	CUbcCode::GetInstance()->GetCategoryInfo(_T("HostType"), listHostType);

	POSITION pos = listHostType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listHostType.GetNext(pos);
		m_cbHostType.AddString(info.strEnumString, info.nEnumNumber);
	}

	CRect rc;
	m_cbHostType.GetWindowRect(&rc);
	m_cbHostType.SetDroppedWidth(rc.Width() * 1.5);

	m_cbAdmin.AddString("All");
	m_cbAdmin.AddString(STR_ADMIN_ON);
	m_cbAdmin.AddString(STR_ADMIN_OFF);
	m_cbAdmin.SetCurSel(0);

	m_cbOperation.AddString("All");
	m_cbOperation.AddString(STR_OPERA_ON);
	m_cbOperation.AddString(STR_OPERA_OFF);
	m_cbOperation.SetCurSel(0);

	CStringArray aTagList;
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_btnRefHost.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnRefHost.SetToolTipText("Refresh Terminal");

	LoadFilterData();

	UpdateData(FALSE);

	InitPosition(m_rcClient);

	CString szBuf;
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR drive[MAX_PATH], path[MAX_PATH];
	_tsplitpath(szModule, drive, path, NULL, NULL);

	long lver = m_fcHost.FlashVersion();
	short maj = HIWORD(lver);
	short min = LOWORD(lver);
	TraceLog(("Flash version %d.%d",maj,min));

	//if(maj < 10){
	//	if(UbcMessageBox(LoadStringById(IDS_HOSTSCREENVIEW_MSG001), MB_ICONWARNING|MB_YESNO)==IDYES){
	//		OpenIExplorer(_T("http://www.adobe.com/go/getflash"), 860);
	//	}
	//	GetParentFrame()->PostMessage(WM_CLOSE, 0, 0);
	//	return;
	//}else{
		ubcConfig aIni("UBCVariables");
		ciLong val = 0;
		aIni.get("ROOT", "ENABLE_SHORT_SCREENSHOT_PERIOD", val);

		szBuf.Format("%s%s", drive, path);
		//szBuf += "flash\\screenshot2.swf";
		szBuf += (val==0 ? "flash\\screenshot2.swf" : "flash\\screenshot2_short.swf");

		m_fcHost.LoadMovie(0, szBuf);
		TraceLog((szBuf));
	//}
}

void CHostScreenView::OnDestroy()
{
	CFormView::OnDestroy();
}

void CHostScreenView::OnClose()
{
	CFormView::OnClose();
}

void CHostScreenView::InitPosition(CRect rc)
{
//	SetScrollSizes(MM_TEXT, CSize(1000,650));
	SetScrollSizes(MM_TEXT, CSize(1,1));

	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();
	m_Reposition.SetMinSize(1000, 650);

	m_Reposition.AddControl(&m_fcHost, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_Filter, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
}

void CHostScreenView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	POINT pt = GetDeviceScrollPosition();
	m_Reposition.Move(-pt.x, -pt.y, cx, cy);
}

BEGIN_EVENTSINK_MAP(CHostScreenView, CFormView)
	ON_EVENT(CHostScreenView, IDC_FLASH_HOSTSCREEN, 150, CHostScreenView::FSCommandFlashHostscreen, VTS_BSTR VTS_BSTR)
END_EVENTSINK_MAP()

void CHostScreenView::FSCommandFlashHostscreen(LPCTSTR command, LPCTSTR args)
{
	TraceLog(("%s / %s\n", command, args));
	if(IsSame(command, "screenshotRequest")){
		int page, count;
		if(IsSame(args, "")){
			page = 0;
			count = 0;
		}else{
			int nPos = 0;
			CString szToken;
			CString szBuf = args;
			
			szToken = szBuf.Tokenize(",", nPos);
			if(!szToken.IsEmpty()){
				page = atoi(szToken);

				szToken = szBuf.Tokenize(",", nPos);
				if(!szToken.IsEmpty()){
					count = atoi(szToken);
				}else{
					count = 0;
				}
			}else{
				page = 0;
				count = 0;
			}
		}
		QueryHost(page, count);
	}
	else if(IsSame(command, "getHost"))
	{
		int nPos = 0;
		CString szHost = args;
		// 2011.01.17 버그!! 단말ID의 사이트ID와 실제 사이트ID가 다를 수 있으므로 차후 단말ID로 사이트ID를 구해오는 로직 넣을것
		CString szSite = m_ssinfo.getSiteId(szHost);
		RunFlashView(szSite, szHost);
	}
}

LRESULT CHostScreenView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	CString szBuf = "";
	
	switch(wParam)
	{
	case IEH_OPSTAT:
		{
			SOpStat* pStat = (SOpStat*)lParam;
			szBuf += "<invoke name='operationalStateChanged'><arguments>";
			szBuf += "<string>";
			szBuf += pStat->hostId;
			szBuf += "</string>";
			szBuf += "<string>";
			szBuf += pStat->operationalState?"1":"0";
			szBuf += "</string>";
			szBuf += "</arguments></invoke>";

			TraceLog((szBuf));
			m_fcHost.CallFunction(szBuf);
		}
		return 0;
	case IEH_ADSTAT:
		{
			SAdStat* pStat = (SAdStat*)lParam;
			szBuf += "<invoke name='adminStateChanged'><arguments>";
			szBuf += "<string>";
			szBuf += pStat->hostId;
			szBuf += "</string>";
			szBuf += "<string>";
			szBuf += pStat->adminState?"1":"0";
			szBuf += "</string>";
			szBuf += "</arguments></invoke>";

			TraceLog((szBuf));
			m_fcHost.CallFunction(szBuf);
		}
		return 0;
	case IEH_UPPACKAGE:
		return 0;
	case IEH_UNKOWN:
		break;
	}
	return 0;
}

void CHostScreenView::OnBnClickedButtonHostrfs()
{
	QueryHost(0,0);

	SaveFilterData();

	// 같은 필터를 사용하는 윈도우에 통지
	((CMainFrame*)AfxGetMainWnd())->UpdateFilterData(this);
}

void CHostScreenView::QueryHost(int page, int count)
{
	CWaitMessageBox wait;

	CString szSite = (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite);

	UpdateData();

	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;
	CString strAdvancedFilter;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);
	if(m_kbAdvancedFilter.GetCheck()) m_cbAdvancedFilter.GetWindowText(strAdvancedFilter);

	// 고급필터에 select,(,) 문자사용하지 못하도록함.
	if(!strAdvancedFilter.IsEmpty())
	{
		CString strTmp = strAdvancedFilter;
		strTmp.MakeUpper();
		if(strTmp.Find("SELECT") >= 0 || strTmp.FindOneOf("()") >= 0)
		{
			UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG021));
			return;
		}
	}

	CString strWhere;

	// 단말이름
	strHostName.Trim();
	if(!strHostName.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostName like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostName
							);
	}

	// 단말ID
	strHostId.Trim();
	if(!strHostId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostId like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostId
							);
	}

	// 단말타입
	if(!strHostType.IsEmpty())
	{
		strHostType.Replace(",", "','");
		strWhere.AppendFormat(_T(" %s hostType in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostType
							);
	}

	// 소속조직
	if(!strSiteId.IsEmpty())
	{
		strSiteId.Replace(_T(","), _T("','"));
		strWhere.AppendFormat(_T(" %s siteId in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strSiteId
							);
	}

	// 포함콘텐츠
	if(!strContentsId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 in (select programId from ubc_contents where lower(contentsId) = lower('%s'))"
								      " or lastSchedule2 in (select programId from ubc_contents where lower(contentsId) = lower('%s')))")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strContentsId
							, strContentsId
							);
	}

	// 방송중인 패키지
	if(!strPackageId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 like '%%%s%%' or lastSchedule2 like '%%%s%%')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strPackageId
							, strPackageId
							);
	}

	// 연결상태
	if(_ttoi(strOperaStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s operationalState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strOperaStat)==1 ? 1:0)
							);
	}

	// 사용상태
	if(_ttoi(strAdminStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s adminState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strAdminStat)==1 ? 1:0)
							);
	}

	// 검색어
	strCategory.Trim();
	if(!strCategory.IsEmpty())
	{
		int pos = 0;
		CString strTag;
		while((strTag = strCategory.Tokenize("/", pos)) != _T(""))
		{
			//strWhere.AppendFormat(_T(" %s concat('/', category, '/') like '%%/%s/%%'")
			strWhere.AppendFormat(_T(" %s category like '%%%s%%'")
							 	 , (strWhere.IsEmpty() ? _T("") : _T("and"))
								 , strTag.MakeLower()
								 );
		}
	}

	strAdvancedFilter.Trim();
	if(!strAdvancedFilter.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (%s) ")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strAdvancedFilter
							);
	}

//	TraceLog((strWhere));

	if(page == 0)
	{
//		TraceLog(("getScreenShot:%s",strWhere));
		//ubcHost::getScreenShot(strWhere, m_ssinfo);
		CCopModule::GetObject()->GetScreenShotInfo(strWhere, m_ssinfo);
	}

	ciString sBuf;
	m_ssinfo.toString(page, count, sBuf);

	CString szBuf = "";
	szBuf += "<invoke name='getScreenShot2'><arguments>";
	szBuf += "<string>";
	szBuf += sBuf.c_str();
	szBuf += "</string>";
	szBuf += "</arguments></invoke>";

	TraceLog((szBuf));
	m_fcHost.CallFunction(szBuf);
}

void CHostScreenView::OnBnClickedButtonFilterSite()
{
	CString strSiteName;
	CString strSiteId;

	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
	if(dlg.DoModal() == IDOK)
	{
		SiteInfoList siteInfoList;
		if(dlg.GetSelectSiteInfoList(siteInfoList))
		{
			POSITION pos = siteInfoList.GetHeadPosition();
			while(pos)
			{
				SSiteInfo info = siteInfoList.GetNext(pos);
				strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
				strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
			}
		}
	}

	SetDlgItemText(IDC_EDIT_FILTER_SITENAME, strSiteName);
	m_strSiteId = strSiteId;
}

void CHostScreenView::OnBnClickedButtonFilterContents()
{
	CContentsSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, _T(""));
		m_strContentsId = _T("");
		return;
	}

	CONTENTS_INFO_EX_LIST& list = dlg.GetSelectedContentsIdList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		CONTENTS_INFO_EX& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, info.strContentsName);
		m_strContentsId = info.strContentsId;
	}
}

void CHostScreenView::OnBnClickedButtonFilterPackage()
{
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		m_strPackageId = _T("");
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, info.szPackage);
		m_strPackageId = info.szPackage;
	}
}

void CHostScreenView::SaveFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;
	CString strAdvancedFilter;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);
	if(m_kbAdvancedFilter.GetCheck()) m_cbAdvancedFilter.GetWindowText(strAdvancedFilter);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	WritePrivateProfileString("HOST-FILTER", "HostName"         , strHostName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostID"           , strHostId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostType"         , strHostType     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteName"         , strSiteName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteID"           , strSiteId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContents"  , strContentsName , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContentsID", strContentsId   , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackage"   , strPackageName  , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackageID" , strPackageId    , szPath);
	WritePrivateProfileString("HOST-FILTER", "AdminStat"        , strAdminStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "OperaStat"        , strOperaStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "Category"         , strCategory     , szPath);
	WritePrivateProfileString("HOST-FILTER", "AdvancedFilter"   , strAdvancedFilter, szPath);

	if(m_kbAdvancedFilter.GetCheck() && !strAdvancedFilter.IsEmpty() && m_cbAdvancedFilter.FindStringExact(0, strAdvancedFilter) < 0) 
	{
		m_cbAdvancedFilter.InsertString(0, strAdvancedFilter);
		for(int i=m_cbAdvancedFilter.GetCount(); i > 20 ; i--)
		{
			m_cbAdvancedFilter.DeleteString(i-1);
		}

		for(int i=0; i<m_cbAdvancedFilter.GetCount() ;i++)
		{
			CString strText;
			m_cbAdvancedFilter.GetLBText(i, strText);

			CString strKey;
			strKey.Format(_T("History%d"), i+1);
			WritePrivateProfileString("HOST-FILTER", strKey, strText, szPath);
		}
	}
}

void CHostScreenView::LoadFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;
	CString strAdvancedFilter;

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("HOST-FILTER", "HostName"         , "", buf, 1024, szPath); strHostName     = buf;
	GetPrivateProfileString("HOST-FILTER", "HostID"           , "", buf, 1024, szPath); strHostId       = buf;
	GetPrivateProfileString("HOST-FILTER", "HostType"         , "", buf, 1024, szPath); strHostType     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteName"         , "", buf, 1024, szPath); strSiteName     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteID"           , "", buf, 1024, szPath); strSiteId       = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContents"  , "", buf, 1024, szPath); strContentsName = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContentsID", "", buf, 1024, szPath); strContentsId   = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackage"   , "", buf, 1024, szPath); strPackageName  = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackageID" , "", buf, 1024, szPath); strPackageId    = buf;
	GetPrivateProfileString("HOST-FILTER", "AdminStat"        , "", buf, 1024, szPath); strAdminStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "OperaStat"        , "", buf, 1024, szPath); strOperaStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "Category"         , "", buf, 1024, szPath); strCategory     = buf;
	GetPrivateProfileString("HOST-FILTER", "AdvancedFilter"   , "", buf, 1024, szPath); strAdvancedFilter = buf;

	SetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	SetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	if(strHostType.IsEmpty())
	{
		m_cbHostType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strHostType.Tokenize(",", pos)) != _T(""))
		{
			m_cbHostType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}
	SetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	m_strSiteId = strSiteId;
	SetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	m_strContentsId = strContentsId;
	SetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageId;
	m_cbAdmin.SetCurSel(_ttoi(strAdminStat));
	m_cbOperation.SetCurSel(_ttoi(strOperaStat));
	//SetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.SetWindowText(strCategory);
	m_kbAdvancedFilter.SetCheck(!strAdvancedFilter.IsEmpty());
	m_cbAdvancedFilter.EnableWindow(m_kbAdvancedFilter.GetCheck());
	m_cbAdvancedFilter.SetWindowText(strAdvancedFilter);

	for(int i=0; i<20 ;i++)
	{
		CString strKey;
		strKey.Format(_T("History%d"), i+1);
		GetPrivateProfileString("HOST-FILTER", strKey, "", buf, 1024, szPath);

		CString strValue = buf;
		if(!strValue.IsEmpty())
		{
			m_cbAdvancedFilter.AddString(strValue);
		}
	}
}

LRESULT CHostScreenView::OnFilterHostChanged(WPARAM wParam, LPARAM lParam)
{
	LoadFilterData();
	UpdateData(FALSE);
	return 0;
}

void CHostScreenView::OnBnClickedKbAdvancedFilter()
{
	m_cbAdvancedFilter.EnableWindow(m_kbAdvancedFilter.GetCheck());
}
