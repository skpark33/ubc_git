// PackageChangeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "PackageChangeDlg.h"
#include "Enviroment.h"
#include "ubccopcommon\PackageSelectDlg.h"

// CPackageChangeDlg dialog
#define STR_SITE		LoadStringById(IDS_PACKAGECHANGEDLG_LST001)
#define STR_PACKAGE		LoadStringById(IDS_PACKAGECHANGEDLG_LST002)
#define STR_USER		LoadStringById(IDS_PACKAGECHANGEDLG_LST003)
#define STR_DATE		LoadStringById(IDS_PACKAGECHANGEDLG_LST004)
#define STR_DESC		LoadStringById(IDS_PACKAGECHANGEDLG_LST005)

IMPLEMENT_DYNAMIC(CPackageChangeDlg, CDialog)

CPackageChangeDlg::CPackageChangeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPackageChangeDlg::IDD, pParent)
{
}

CPackageChangeDlg::~CPackageChangeDlg()
{
}

void CPackageChangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HOST, m_lscHost);
}

BEGIN_MESSAGE_MAP(CPackageChangeDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CPackageChangeDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPackageChangeDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_SELECT_PACKAGE, &CPackageChangeDlg::OnBnClickedBnSelectPackage)
END_MESSAGE_MAP()

// CPackageChangeDlg message handlers
BOOL CPackageChangeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitHostList();
	InserHostList();

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPackageChangeDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscHost.SaveColumnState("PACKAGE-CHNG-LIST", szPath);
}

void CPackageChangeDlg::InitHostList()
{
	m_lscHost.SetExtendedStyle(m_lscHost.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColHostTitle[eCheck]     = _T("");
	m_szColHostTitle[eSiteID]	 = LoadStringById(IDS_PACKAGECHANGEDLG_LST001);
	m_szColHostTitle[eHostID]	 = LoadStringById(IDS_PACKAGECHANGEDLG_LST006);
	m_szColHostTitle[eDisplayNo] = LoadStringById(IDS_PACKAGECHANGEDLG_LST007);
	m_szColHostTitle[eAutoSche]	 = LoadStringById(IDS_PACKAGECHANGEDLG_LST008);
	m_szColHostTitle[eCurSche]	 = LoadStringById(IDS_PACKAGECHANGEDLG_LST009);
	m_szColHostTitle[eCreator]	 = LoadStringById(IDS_PACKAGECHANGEDLG_LST010);
	m_szColHostTitle[eLastSche]	 = LoadStringById(IDS_PACKAGECHANGEDLG_LST011);

	for(int nCol = 0; nCol < eExpEnd; nCol++)
	{
		m_lscHost.InsertColumn(nCol, m_szColHostTitle[nCol], LVCFMT_LEFT, 100);
	}

	m_lscHost.SetColumnWidth(m_lscHost.GetColPos(m_szColHostTitle[eCheck]), 22); // 2010.02.17 by gwangsoo
	m_lscHost.SetColumnWidth(eSiteID, 100);
	m_lscHost.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CPackageChangeDlg::InserHostList()
{
	m_lscHost.DeleteAllItems();

	for(int nRow = 0; nRow < m_arHostList.GetCount(); nRow++)
	{
		SHostInfo Info = m_arHostList.GetAt(nRow);

		m_lscHost.InsertItem(nRow, "");
		m_lscHost.SetItemText(nRow, eSiteID, Info.siteId);
		m_lscHost.SetItemText(nRow, eHostID, Info.hostId);

		if(Info.displayCounter==1)
		{
			m_lscHost.SetItemText(nRow, eDisplayNo, LoadStringById(IDS_PACKAGECHANGEDLG_STR001));
			m_lscHost.SetItemText(nRow, eAutoSche , Info.autoPackage1);
			m_lscHost.SetItemText(nRow, eCurSche  , Info.currentPackage1);
			m_lscHost.SetItemText(nRow, eCreator  , Info.networkUse1?LoadStringById(IDS_PACKAGECHANGEDLG_STR003):LoadStringById(IDS_PACKAGECHANGEDLG_STR004));
			m_lscHost.SetItemText(nRow, eLastSche , Info.lastPackage1);
		}
		else
		{
			m_lscHost.SetItemText(nRow, eDisplayNo, LoadStringById(IDS_PACKAGECHANGEDLG_STR002));
			m_lscHost.SetItemText(nRow, eAutoSche , Info.autoPackage2);
			m_lscHost.SetItemText(nRow, eCurSche  , Info.currentPackage2);
			m_lscHost.SetItemText(nRow, eCreator  , Info.networkUse2?LoadStringById(IDS_PACKAGECHANGEDLG_STR003):LoadStringById(IDS_PACKAGECHANGEDLG_STR004));
			m_lscHost.SetItemText(nRow, eLastSche , Info.lastPackage2);
		}

		m_lscHost.SetItemData(nRow, (int)nRow);
		m_lscHost.SetCheck(nRow);
	}

	m_lscHost.SetCheckHdrState(true);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscHost.LoadColumnState("PACKAGE-CHNG-LIST", szPath);
}

void CPackageChangeDlg::OnBnClickedOk()
{
	int nSelectHostCnt = 0;
	for(int i=0; i<m_lscHost.GetItemCount() ;i++)
	{
		if(m_lscHost.GetCheck(i)) nSelectHostCnt++;
	}

	// 단말을 선택하지 않은 경우
	if(nSelectHostCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_PACKAGECHANGEDLG_MSG001));
		return;
	}

	std::list<std::string> lsHost;
	for(int nHostRow=0; nHostRow<m_lscHost.GetItemCount() ;nHostRow++)
	{
		if(!m_lscHost.GetCheck(nHostRow)) continue;

		int nHostIdx = (int)m_lscHost.GetItemData(nHostRow);
		if(nHostIdx < 0 || nHostIdx >= m_arHostList.GetCount()) continue;

		SHostInfo HostInfo = m_arHostList.GetAt(nHostIdx);

		std::string strHost;
		strHost = HostInfo.hostId;
		strHost+= HostInfo.displayCounter==1?":A":":B";

		lsHost.push_back(strHost);
	}

	if(m_stPackageInfo.szPackage.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_PACKAGECHANGEDLG_MSG002));
		return;
	}

	CWaitMessageBox wait;

	CCopModule::GetObject()->ApplyPackage((LPCTSTR)GetEnvPtr()->m_szLoginID, (LPCTSTR)m_stPackageInfo.szSiteID, (LPCTSTR)m_stPackageInfo.szPackage, _T(""), _T(""), lsHost);

	OnOK();
}

void CPackageChangeDlg::OnBnClickedCancel()
{
	OnCancel();
}

void CPackageChangeDlg::OnBnClickedBnSelectPackage()
{
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);
	//skpark 2013.3.29
	for(int nRow = 0; nRow < m_arHostList.GetCount(); nRow++)
	{
		SHostInfo info = m_arHostList.GetAt(nRow);
		dlg.m_arHostList.Add(info);
	}	
	//skpark end

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EB_SELECT_PACKAGE, _T(""));
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();
	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		m_stPackageInfo = list.GetNext(pos);
	}

	SetDlgItemText(IDC_EB_SELECT_PACKAGE, m_stPackageInfo.szPackage);
}
