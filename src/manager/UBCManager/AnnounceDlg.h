#pragma once

#include "afxwin.h"
#include "afxdtctl.h"
#include "afxcmn.h"
#include "enviroment.h"

#include "Browser\Frame.h"
#include "Browser\Schedule.h"

#include "common\hoverbutton.h"
#include "common\utblistctrlex.h"
#include "common\colorpickercb.h"
#include "common\fontpreviewcombo.h"
#include "ximage.h"
#include "Contents\ContentsDialog.h"


// CAnnounceDlg dialog
class CAnnounceDlg : public CDialog
{
	DECLARE_DYNAMIC(CAnnounceDlg)

public:
	void SetInfo(SAnnounceInfo& info);
	void GetInfo(SAnnounceInfo& info);
private:
	void InitList();
	void SetList();
	//void InitPreview();
	void EnableAllItems(BOOL bEnable);
public:
private:
	CString m_szTitle;
	//CString m_szFile;
	//CHoverButton m_btFile;
	//CHoverButton m_btPlay;
	//CHoverButton m_btStop;
	CDateTimeCtrl m_dtcStartDate;
	CDateTimeCtrl m_dtcStartTime;
	CDateTimeCtrl m_dtcEndDate;
	CDateTimeCtrl m_dtcEndTime;
	CComboBox m_cbPosition;
	//CFontPreviewCombo m_cbFont;
	//CString m_szFontSize;
	//CSpinButtonCtrl m_spFontSize;
	//CColorPickerCB m_cbFGColor;
	//CColorPickerCB m_cbBGColor;
	//CSliderCtrl m_sldSpeed;
	//CString m_szAlpa;
	//CSpinButtonCtrl m_spAlpa;
	//CString m_szComment;
	CUTBListCtrlEx m_lscTerminal;

	//CxImage m_imBack;
	SAnnounceInfo m_AnnoInfo;
	//Mng_Browser::CHorizontalTickerSchedule	m_wndTicker;

	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
public:
	CAnnounceDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAnnounceDlg();

// Dialog Data
	enum { IDD = IDD_ANNOUNCEPROP };
	enum { eHostID, eHostName, eCOLEND };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnBnClickedOk();
	//afx_msg void OnEnChangeEditComment();
	afx_msg void OnNMKillfocusStartDate(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusStartTime(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusEndDate(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusEndTime(NMHDR *pNMHDR, LRESULT *pResult);
	//afx_msg void OnBnClickedButtonFile();
	//afx_msg void OnBnClickedButtonPlay();
	//afx_msg void OnBnClickedButtonStop();
	
	
	afx_msg void OnBnClickedButtonSelectHost();
	afx_msg void OnBnClickedBtContents();
	CComboBox m_cbContentsType;
	CComboBox m_cbAlign;
	CEdit m_editSize;
	CEdit m_editLRMargin;
	CEdit m_editUDMargin;
	CStatic m_picFrame;
	CStatic m_picDisplay;
	CEdit m_editContentsName;
	afx_msg void OnCbnSelchangeCbContentstype();
	CSpinButtonCtrl m_spSize;
	CSpinButtonCtrl m_spLRMargin;
	CSpinButtonCtrl m_spUDMargin;
	CButton m_radioHorizontal;
	CButton m_radioVertical;
	afx_msg void OnBnClickedRadioHorizontal();
	afx_msg void OnBnClickedRadioVertical();
	afx_msg void OnEnChangeEditSize();

	void RedrawExample(int x, int y, int width, int height);
	void RedrawABSExample(int x, int y, int width, int height);
	void RedrawFullScreenExample(int x, int y, int width, int height);
	void RedrawTickerExample(int x, int y, int width, int height);
	bool m_isInit;
	afx_msg void OnCbnSelchangeCbAlign();
	afx_msg void OnEnChangeEditLrMargin();
	afx_msg void OnEnChangeEditUdMargin();
	afx_msg void OnCbnSelchangeComboPosition();
	afx_msg void OnEnChangeEditHeight();

	void AnnounceToContents(CONTENTS_INFO& info);
	void ContentsToAnnounce(CONTENTS_INFO& info);

	//CButton m_checkSize;
	//afx_msg void OnBnClickedCheckSize();
	CButton m_radioSourceSize;
	afx_msg void OnBnClickedRadioSourceSize();
	afx_msg void OnBnClickedRadioAbsSize();
	afx_msg void OnBnClickedRadioFullSize2();
	CEdit m_editWidth2;
	CSpinButtonCtrl m_spWidth2;
	CEdit m_editHeight;
	CSpinButtonCtrl m_spHeight;
	CEdit m_editPosX;
	CSpinButtonCtrl m_spPosX;
	CEdit m_editPosY;
	CSpinButtonCtrl m_spPosY;
	afx_msg void OnEnChangeEditWidth2();
	afx_msg void OnEnChangeEditXPos();
	afx_msg void OnEnChangeEditYPos();
	CButton m_radioABSSize;
	CButton m_radioFullSize;

	bool	m_bReadOnly;
};
