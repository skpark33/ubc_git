// LightView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "LightView.h"
#include "Enviroment.h"
#include "MainFrm.h"
#include "LightDetailDlg.h"
#include "common\ubcdefine.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common\libcommon\utvUtil.h"
#include "common\UbcCode.h"
#include "ubccopcommon/SiteSelectDlg.h"
#include "UBCCopCommon\UbcMenuAuth.h"

#define STR_ADMIN_ON			LoadStringById(IDS_HOSTVIEW_LIST001)
#define STR_ADMIN_OFF			LoadStringById(IDS_HOSTVIEW_LIST002)
#define STR_OPERA_ON			LoadStringById(IDS_HOSTVIEW_LIST003)
#define STR_OPERA_OFF			LoadStringById(IDS_HOSTVIEW_LIST004)


#define STR_LIGHT_SITEID		LoadStringById(IDS_SITEVIEW_LST001)
#define STR_LIGHT_ID			LoadStringById(IDS_LIGHTVIEW_STR002)
#define STR_LIGHT_NAME			LoadStringById(IDS_LIGHTVIEW_STR003)
#define STR_LIGHT_DESC			LoadStringById(IDS_HOSTVIEW_LIST015)//
#define STR_LIGHT_TYPE   		LoadStringById(IDS_LIGHTVIEW_STR004)
#define STR_LIGHT_STARTUPTIME		LoadStringById(IDS_LIGHTVIEW_STR005)
#define STR_LIGHT_SHUTDOWNTIME		LoadStringById(IDS_LIGHTVIEW_STR006)
#define STR_LIGHT_BRIGHTNESS		LoadStringById(IDS_LIGHTVIEW_STR008) // 
#define STR_LIGHT_WEEKSHUTDOWNTIME	LoadStringById(IDS_LIGHTVIEW_STR007) 
#define STR_LIGHT_HOLIDAY			LoadStringById(IDS_HOLLIDAYDLG_LST001) //

#define STR_SEP				_T(",")

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)
#define COLOR_DOWNLOAD		RGB(0,128,255)		// ::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_FAIL			RGB(176,0,0)


// CLightView

IMPLEMENT_DYNCREATE(CLightView, CFormView)

CLightView::CLightView()
	: CFormView(CLightView::IDD)
,	m_Reposition(this)
{
	m_pMainFrame = (CMainFrame*)AfxGetMainWnd();

}

CLightView::~CLightView()
{
}

void CLightView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LIGHT, m_lscLight);

	DDX_Control(pDX, IDC_BUTTON_LIGHT_CREATE, m_btnAddLight);
	DDX_Control(pDX, IDC_BUTTON_LIGHT_DEL, m_btnDelLight);
	DDX_Control(pDX, IDC_BUTTON_LIGHT_MOD, m_btnModLight);
	DDX_Control(pDX, IDC_BUTTON_LIGHT_REF, m_btnRefLight);
	DDX_Control(pDX, IDC_BUTTON_LIGHT_OFF, m_btnLightOff);
	DDX_Control(pDX, IDC_BUTTON_LIGHT_ON, m_btnLightOn);

	DDX_Control(pDX, IDC_COMBO_FILTER_ADMIN, m_cbAdmin);
	DDX_Control(pDX, IDC_COMBO_FILTER_OPR, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_FILTER_LIGHTTYPE, m_cbLightType);

	DDX_Control(pDX, IDC_EDIT_FILTER_LIGHTNAME, m_editLightName);
	DDX_Control(pDX, IDC_EDIT_FILTER_LIGHTID, m_editLightId);
}

BEGIN_MESSAGE_MAP(CLightView, CFormView)
	ON_BN_CLICKED(IDC_BUTTON_LIGHT_REF, &CLightView::OnBnClickedButtonLightRef)
	ON_BN_CLICKED(IDC_BUTTON_LIGHT_CREATE, &CLightView::OnBnClickedButtonLightCreate)
	ON_BN_CLICKED(IDC_BUTTON_LIGHT_DEL, &CLightView::OnBnClickedButtonLightDel)
	ON_BN_CLICKED(IDC_BUTTON_LIGHT_MOD, &CLightView::OnBnClickedButtonLightMod)
	ON_BN_CLICKED(IDC_BUTTON_LIGHT_OFF, &CLightView::OnBnClickedButtonLightOff)
	ON_BN_CLICKED(IDC_BUTTON_LIGHT_ON, &CLightView::OnBnClickedButtonLightOn)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_SITE, &CLightView::OnBnClickedButtonFilterSite)
	ON_WM_DESTROY()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_LIGHT, &CLightView::OnNMDblclkListLight)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_LIGHT, &CLightView::OnNMRclickListLight)
	ON_COMMAND(ID_DEVICEMENU_POWERON, &CLightView::OnDevicemenuPoweron)
	ON_COMMAND(ID_DEVICEMENU_POWEROFF, &CLightView::OnDevicemenuPoweroff)
	ON_COMMAND(ID_DEVICEMENU_DETAIL, &CLightView::OnDevicemenuDetail)
	ON_COMMAND(ID_DEVICEMENU_REMOVE, &CLightView::OnDevicemenuRemove)
END_MESSAGE_MAP()


// CLightView 진단입니다.

#ifdef _DEBUG
void CLightView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CLightView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CLightView 메시지 처리기입니다.

void CLightView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_HOSTVIEW_STR001));

	CodeItemList listLightType;
	CUbcCode::GetInstance()->GetCategoryInfo(_T("LightType"), listLightType);

	POSITION pos = listLightType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listLightType.GetNext(pos);
		m_cbLightType.AddString(info.strEnumString, info.nEnumNumber);
	}

	CRect rc;
	m_cbLightType.GetWindowRect(&rc);
	m_cbLightType.SetDroppedWidth(rc.Width() * 1.5);

	m_cbAdmin.AddString("All");
	m_cbAdmin.AddString(STR_ADMIN_ON);
	m_cbAdmin.AddString(STR_ADMIN_OFF);
	m_cbAdmin.SetCurSel(0);

	m_cbOperation.AddString("All");
	m_cbOperation.AddString(STR_OPERA_ON);
	m_cbOperation.AddString(STR_OPERA_OFF);
	m_cbOperation.SetCurSel(0);

	m_btnDelLight.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btnModLight.LoadBitmap(IDB_BUTTON_MODIFY, RGB(255,255,255));
	m_btnRefLight.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnAddLight.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btnLightOff.LoadBitmap(IDB_BUTTON_SHUTDOWN, RGB(255,255,255));
	m_btnLightOn.LoadBitmap(IDB_BITMAP_POWERON, RGB(255,255,255));
/*
	m_btnAddLight.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN001));
	m_btnDelLight.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN002));
	m_btnModLight.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN003));
	m_btnRefLight.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN004));
	m_btnLightOn.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN007));
	m_btnLightOff.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN008));
*/
	InitPosition(m_rcClient);

	UpdateData(FALSE);
	InitAuthCtrl();
	InitLightList();

	SetScrollSizes(MM_TEXT, CSize(1,1));

}
void CLightView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscLight, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}


// 권한에 따라 활성화 한다.
void CLightView::InitAuthCtrl()
{

	m_btnDelLight.EnableWindow(IsAuth(_T("HDEL")));
	m_btnModLight.EnableWindow(IsAuth(_T("HMOD")));
	m_btnRefLight.EnableWindow(IsAuth(_T("HQRY")));
	m_btnAddLight.EnableWindow(IsAuth(_T("HREG")));
	m_btnLightOff.EnableWindow(IsAuth(_T("HOFF")));
	m_btnLightOn.EnableWindow(IsAuth(_T("HRBT")));
}

void CLightView::InitLightList()
{
	m_szLightColum[eCheck] = _T("");	
	m_szLightColum[eSiteId] = STR_LIGHT_SITEID;
	m_szLightColum[eLightType] = STR_LIGHT_TYPE;
	m_szLightColum[eLightName] = STR_LIGHT_NAME;
	m_szLightColum[eLightID] = STR_LIGHT_ID;
	m_szLightColum[eLightDesc] = STR_LIGHT_DESC;
	m_szLightColum[eStartupTime] = STR_LIGHT_STARTUPTIME;
	m_szLightColum[eShutdownTime] = STR_LIGHT_SHUTDOWNTIME;
	m_szLightColum[eBrightness] = STR_LIGHT_BRIGHTNESS;
	m_szLightColum[eWeekShutdownTime] = STR_LIGHT_WEEKSHUTDOWNTIME;
	m_szLightColum[eHoliday] = STR_LIGHT_HOLIDAY;


	m_lscLight.SetExtendedStyle(m_lscLight.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_INFOTIP |m_lscLight.GetExtendedStyle());

	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilLightList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilLightList.Add(&bmCheck, RGB(255, 255, 255));
	m_lscLight.SetImageList(&m_ilLightList, LVSIL_SMALL);

	m_lscLight.InsertColumn(eCheck,				m_szLightColum[eCheck], LVCFMT_LEFT, 22);
	m_lscLight.InsertColumn(eSiteId,			m_szLightColum[eSiteId], LVCFMT_LEFT, 60);
	m_lscLight.InsertColumn(eLightType,			m_szLightColum[eLightType], LVCFMT_LEFT, 60);
	m_lscLight.InsertColumn(eLightName,			m_szLightColum[eLightName], LVCFMT_LEFT, 100);
	m_lscLight.InsertColumn(eLightID,			m_szLightColum[eLightID], LVCFMT_LEFT, 80);
	m_lscLight.InsertColumn(eLightDesc,			m_szLightColum[eLightDesc], LVCFMT_LEFT, 100);
	m_lscLight.InsertColumn(eStartupTime,		m_szLightColum[eStartupTime], LVCFMT_LEFT, 80);
	m_lscLight.InsertColumn(eShutdownTime,		m_szLightColum[eShutdownTime], LVCFMT_LEFT, 80);
	m_lscLight.InsertColumn(eBrightness,		m_szLightColum[eBrightness], LVCFMT_LEFT, 40);
	m_lscLight.InsertColumn(eWeekShutdownTime,	m_szLightColum[eWeekShutdownTime], LVCFMT_LEFT, 190);
	m_lscLight.InsertColumn(eHoliday,			m_szLightColum[eHoliday], LVCFMT_LEFT, 190);

	m_lscLight.SetColumnWidth(eCheck,			22);
	m_lscLight.SetColumnWidth(eSiteId,			60);
	m_lscLight.SetColumnWidth(eLightType,		60);
	m_lscLight.SetColumnWidth(eLightName,		100);
	m_lscLight.SetColumnWidth(eLightID,			80);
	m_lscLight.SetColumnWidth(eLightDesc,		100);
	m_lscLight.SetColumnWidth(eStartupTime,		80);
	m_lscLight.SetColumnWidth(eShutdownTime,	80);
	m_lscLight.SetColumnWidth(eBrightness,		40);
	m_lscLight.SetColumnWidth(eWeekShutdownTime,190);
	m_lscLight.SetColumnWidth(eHoliday,			190);



	m_lscLight.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscLight.LoadColumnState("LIGHT-LIST", szPath);

}
BOOL CLightView::RefreshLight(bool bCompMsg)
{
	TraceLog(("RefreshLight begin"));
	DWORD dwStartTime =  ::GetTickCount() ;

	CWaitMessageBox wait;
	
	CString szSite = (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite);

	UpdateData();

	CString strLightName     ;
	CString strLightId       ;
	CString strLightType     ;
	CString strHostId	     ;
	CString strSiteId       ;
	CString strAdminStat    ;
	CString strOperaStat    ;

	m_editLightName.GetWindowText(strLightName);
	m_editLightId.GetWindowText(strLightId);
	
	strLightType = m_cbLightType.GetCheckedIDs();
	if(strLightType == _T("All")) strLightType = _T("");
	else if(strLightType.GetLength() >= 2) strLightType = strLightType.Mid(1, strLightType.GetLength()-2);
	
	strSiteId = m_strSiteId;

	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());

	CString strWhere;

	// 단말이름
	strLightName.Trim();
	if(!strLightName.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s lightName like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strLightName
							);
	}

	// 단말ID
	strLightId.Trim();
	if(!strLightId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s lightId like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strLightId
							);
	}

	// 단말타입
	if(!strLightType.IsEmpty())
	{
		strLightType.Replace(",", "','");
		strWhere.AppendFormat(_T(" %s lightType in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strLightType
							);
	}

	// 소속조직
	if(!strSiteId.IsEmpty())
	{
		strSiteId.Replace(_T(","), _T("','"));
		strWhere.AppendFormat(_T(" %s siteId in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strSiteId
							);
	}

	// 연결상태
	if(_ttoi(strOperaStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s operationalState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strOperaStat)==1 ? 1:0)
							);
	}

	// 사용상태
	if(_ttoi(strAdminStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s adminState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strAdminStat)==1 ? 1:0)
							);
	}

	TraceLog(("m_lsLight.RemoveAll() begin"));
	m_lsLight.RemoveAll();
	TraceLog(("m_lsLight.RemoveAll() end"));

	BOOL bRet = CCopModule::GetObject()->GetLightList(szSite, "*", (strWhere.IsEmpty() ? NULL : strWhere),true,false,m_lsLight);

	RefreshLightList();

	TraceLog(("RefreshLight end"));

	if(!bRet)
	{
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR010), MB_ICONINFORMATION);
		return FALSE;
	}

	if(bCompMsg)
	{
		DWORD dwElapse = ::GetTickCount() - dwStartTime;
		DWORD dwElapseSec = dwElapse / 1000;
		CString strElapse;
		strElapse.Format("\nElapse time : %ld sec", dwElapseSec);

		CString strMsg;
		strMsg.Format(LoadStringById(IDS_ICONVIEW_STR011), m_lscLight.GetItemCount());
		strMsg.Append(strElapse);

		UbcMessageBox(strMsg, MB_ICONINFORMATION);
	}

	return bRet;
}

void CLightView::RefreshLightList(POSITION posStart)
{
	TraceLog(("RefreshLightList begin"));

	m_lscLight.SetRedraw(FALSE);

	int nRow = 0;
	LVITEM item;
	CString szBuf;
	POSITION pos = m_lsLight.GetHeadPosition();
	POSITION posTail = m_lsLight.GetTailPosition();

	if(posStart)
	{
		pos = posStart;
		m_lsLight.GetNext(pos);
	}
	else
	{
		m_lscLight.DeleteAllItems();
	}

	while(pos)
	{
		POSITION posOld = pos;
		SLightInfo info = m_lsLight.GetNext(pos);

		m_lscLight.InsertItem(nRow, _T(""), -1);
	
		UpdateListRow(nRow, &info);  //OK

		m_lscLight.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lscLight.GetCurSortCol();
	if(-1 != nCol)
		m_lscLight.SortList(nCol, m_lscLight.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscLight);
	else
		m_lscLight.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscLight);

	TraceLog(("RefreshLightList end"));

	m_lscLight.SetRedraw(TRUE);

	RefreshPaneText();
}


void CLightView::UpdateListRow(int nRow, SLightInfo* pInfo)
{
	if(!pInfo || m_lscLight.GetItemCount() < nRow)
		return;

	COLORREF crTextBk = COLOR_WHITE;
	if(pInfo->operationalState)
	{
		crTextBk = COLOR_YELLOW;
	}
	else
	{
		crTextBk = COLOR_GRAY;
	}
	m_lscLight.SetRowColor(nRow, crTextBk, m_lscLight.GetTextColor());

	CString szBuf;

	m_lscLight.SetItemText(nRow, eSiteId, pInfo->siteId);
	m_lscLight.SetItemText(nRow, eLightID, pInfo->lightId);
	m_lscLight.SetItemText(nRow, eLightName, pInfo->lightName);
	m_lscLight.SetItemText(nRow, eLightType, CUbcCode::GetInstance()->GetCodeName(_T("LightType"), pInfo->lightType));
	m_lscLight.SetItemText(nRow, eLightDesc, pInfo->description);
	m_lscLight.SetItemText(nRow, eStartupTime, pInfo->startupTime);
	m_lscLight.SetItemText(nRow, eShutdownTime, pInfo->shutdownTime);
	m_lscLight.SetItemText(nRow, eWeekShutdownTime, pInfo->weekShutdownTime);
	m_lscLight.SetItemText(nRow, eHoliday, pInfo->holiday);

	// 밝기
	if(pInfo->brightness < 0)
	{
		m_lscLight.SetItemText(nRow, eBrightness, _T("Unknown"));
	}
	else
	{
		m_lscLight.SetItemText(nRow, eBrightness, ToString(pInfo->brightness));
	}

}

void CLightView::RefreshPaneText()
{
	int nActiveCnt = 0;
	POSITION pos = m_lsLight.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SLightInfo info = m_lsLight.GetNext(pos);
		if(info.operationalState)
		{
			nActiveCnt++;
		}
	}

	CString strPaneText;
	strPaneText.Format(_T("Light:%d Active:%d")
					 , m_lsLight.GetCount()
					 , nActiveCnt
					 );

	if(m_pMainFrame)
	{
		m_pMainFrame->SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_HOST, (LPARAM)(LPCTSTR)strPaneText);
	}
}

void CLightView::OnBnClickedButtonLightRef()
{
	RefreshLight();
}

void CLightView::OnBnClickedButtonLightCreate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CLightView::OnBnClickedButtonLightDel()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscLight.GetItemCount(); nRow++)
	{
		if(!m_lscLight.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	RemoveLight(arRow, true);
}

void CLightView::OnBnClickedButtonLightMod()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscLight.GetItemCount(); nRow++)
	{
		if(!m_lscLight.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	ModifyLight(arRow, true);
}


void CLightView::OnBnClickedButtonLightOff()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscLight.GetItemCount(); nRow++)
	{
		if(!m_lscLight.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	PowerOff(arRow, true);
}

void CLightView::OnBnClickedButtonLightOn()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscLight.GetItemCount(); nRow++)
	{
		if(!m_lscLight.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	PowerOn(arRow, true);
}

void CLightView::OnBnClickedButtonFilterSite()
{
	CString strSiteName;
	CString strSiteId;

	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
	if(dlg.DoModal() == IDOK)
	{
		SiteInfoList siteInfoList;
		if(dlg.GetSelectSiteInfoList(siteInfoList))
		{
			POSITION pos = siteInfoList.GetHeadPosition();
			while(pos)
			{
				SSiteInfo info = siteInfoList.GetNext(pos);
				strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
				strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
			}
		}
	}

	SetDlgItemText(IDC_EDIT_FILTER_SITENAME, strSiteName);
	m_strSiteId = strSiteId;
}

void CLightView::OnDestroy()
{
	CFormView::OnDestroy();

	if(m_pMainFrame)
	{
		m_pMainFrame->SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_HOST, (LPARAM)(LPCTSTR)_T(""));
	}

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscLight.SaveColumnState("LIGHT-LIST", szPath);
}

void CLightView::OnNMDblclkListLight(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!IsAuth(_T("HDTL")) && !IsAuth(_T("HMOD"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	CArray<int> arRow;
	arRow.Add(nRow);
	ModifyLight(arRow, true);
}


void CLightView::ModifyLight(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscLight.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscLight.GetItemData(nRow);
		if(!pos) continue;

		SLightInfo Info = m_lsLight.GetAt(pos);
		CLightDetailDlg dlg;

		dlg.SetInfo(Info);
		if(dlg.DoModal() != IDOK)
		{
			dlg.GetInfo(Info);
			m_lsLight.GetAt(pos) = Info;
			UpdateListRow(nRow, &Info); //OK
			continue;
		}
		dlg.GetInfo(Info);

		if(!CCopModule::GetObject()->SetLight(Info, GetEnvPtr()->m_szLoginID))
		{
			m_szMsg.Format(LoadStringById(IDS_LIGHTVIEW_STR010), Info.hostId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;
		m_lsLight.GetAt(pos) = Info;

		TraceLog(("holiday=%s, shutdownTime=%s", Info.holiday, Info.shutdownTime));
	}

	if(bModified)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR009), MB_ICONINFORMATION);
		RefreshLightList();
	}
}

void CLightView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
	//InvalidateItems();
}

int CLightView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}


LRESULT CLightView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	TraceLog(("CLightView::InvokeEvent begin"));

	HWND hWnd = GetSafeHwnd();
	TraceLog(("hWnd [%X]", hWnd));
	if(::IsWindow(hWnd) == FALSE) return 0;

	switch(wParam)
	{
/*
	case IEH_OPSTAT: 
		{
// cli >>> post PM=2/Site=SQI/Host=SQI-06112 operationalStateChanged siteId="SQI", hostId="SQI-06116", operationalState=true
			SOpStat* pOpStat = (SOpStat*)lParam;
			return this->m_picCtrl.ChangeOPStat((const char*)pOpStat->siteId, (const char*)pOpStat->hostId, pOpStat->operationalState);
		}
*/
	case IEH_DEVICE_OPSTAT: 
		{
			SDeviceOpStat* pOpStat = (SDeviceOpStat*)lParam;
			return this->ChangeOPStat(pOpStat);
		}
/*
	case IEH_ADSTAT:
		return ChangeAdStat((SAdStat*)lParam);

	case IEH_UPPACKAGE:
		return UpdatePackage((SUpPackage*)lParam);

	case IEH_CHNGVNC:
		return ChangeVncStat((cciEvent*)lParam);

		// 0000707: 콘텐츠 다운로드 상태 조회 기능
	case IEH_CHNGDOWN:
		return ChangeDownloadStat((SDownloadStateChange*)lParam);

		// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	case IEH_MOSTAT:
		return ChangeMonitorStat((SMonitorStat*)lParam);

	case IEH_HDDCHNG:
		return ChangeDiskAvail((SDiskAvailChanged*)lParam);
	*/
	case IEH_UNKOWN:
		//cciAttributeList* pList = (cciAttributeList*)lParam;

		//for(int i = 0; i < (int)pList->length(); i++){
		//	cciAttribute& attr = (*pList)[i];

		//	if(strcmp(attr.getName(),"siteId") == 0){
		//		attr.getValue(strSite);
		//	}
		//}
		break;
	}
	return 0;
}
int CLightView::ChangeOPStat(SDeviceOpStat* pOpStat)
{
	TraceLog(("%s=%s event arrived", pOpStat->objectClass,pOpStat->objectId));

	for(int nRow = 0; nRow < m_lscLight.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscLight.GetItemData(nRow);
		if(!pos) continue;

		SLightInfo Info = m_lsLight.GetAt(pos);

		if(	strlen(pOpStat->siteId) > 0  && pOpStat->siteId[0] != '*' && Info.siteId != pOpStat->siteId || 
			strlen(pOpStat->hostId) > 0  && pOpStat->hostId[0] != '*' && Info.hostId != pOpStat->hostId ||
			strncmp("Light",pOpStat->objectClass,strlen("Light")) != 0 ||
			Info.lightId != pOpStat->objectId)
		{
			continue;
		}

		TraceLog(("operationalState : %s", pOpStat->operationalState?"true":"false"));
//		m_lscLight.SetText(nRow, m_szLightColum[eLightOper], pOpStat->operationalState?STR_OPERA_ON:STR_OPERA_OFF);
		Info.operationalState = pOpStat->operationalState;
		m_lsLight.GetAt(pos) = Info;

//		RefreshLightList();
//		m_lscLight.Invalidate();
		UpdateListRow(nRow, &Info); //OK

		RefreshPaneText();
		return 0;
	}

	return 0;
}

void CLightView::OnNMRclickListLight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscLight.GetItemData(nRow);
	if(!pos) return;
	SLightInfo Info = m_lsLight.GetAt(pos);

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);
//	ClientToScreen(&pt);

	pPopup = menu.GetSubMenu(8);


	pPopup->EnableMenuItem(ID_DEVICEMENU_POWERON, (IsAuth(_T("HDTL")) || IsAuth(_T("HMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_DEVICEMENU_POWEROFF, (IsAuth(_T("HDTL")) || IsAuth(_T("HMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_DEVICEMENU_DETAIL, (IsAuth(_T("HDTL")) || IsAuth(_T("HMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_DEVICEMENU_REMOVE, (IsAuth(_T("HDTL")) || IsAuth(_T("HMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));


	//pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_NONOTIFY,pt.x, pt.y, this);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CLightView::OnDevicemenuPoweron()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	CArray<int> arRow;
	arRow.Add(m_ptSelList.y);
	PowerOn(arRow, true);
}

void CLightView::OnDevicemenuPoweroff()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	CArray<int> arRow;
	arRow.Add(m_ptSelList.y);
	PowerOff(arRow, true);
}

void CLightView::OnDevicemenuDetail()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	CArray<int> arRow;
	arRow.Add(m_ptSelList.y);
	ModifyLight(arRow, true);
}

void CLightView::OnDevicemenuRemove()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	CArray<int> arRow;
	arRow.Add(m_ptSelList.y);
	RemoveLight(arRow, true);
}

void CLightView::RemoveLight(CArray<int>& arRow, bool bMsg)
{
	if(UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR012), MB_YESNO|MB_ICONINFORMATION) != IDYES)
	{
		return;
	}
	TraceLog(("RemoveLight(%d)", arRow.GetCount()));


	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		//TraceLog(("nRow=%d, nCnt=%d", nRow, nCnt));		
		m_lscLight.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscLight.GetItemData(nRow);
		if(!pos) {
			//TraceLog(("Unknown error %d, %d", nRow, nCnt));		
			continue;
		}

		SLightInfo Info = m_lsLight.GetAt(pos);

		if(!CCopModule::GetObject()->DelLight(Info.siteId, Info.hostId, Info.lightId))
		{
			m_szMsg.Format(LoadStringById(IDS_LIGHTVIEW_STR013), Info.lightId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;

		m_lsLight.RemoveAt(pos);
		//m_lscLight.DeleteItem(nRow);
	}

	if(bModified)
	{
		if(bMsg) 	UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR014), MB_ICONINFORMATION);
		RefreshLightList();
	}

}

void CLightView::PowerOn(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscLight.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscLight.GetItemData(nRow);
		if(!pos) continue;

		SLightInfo Info = m_lsLight.GetAt(pos);
		

		if(!CCopModule::GetObject()->DevicePowerOn(Info.siteId, Info.hostId, "Light", Info.lightId, ::ToString(Info.lightType)))
		{
			m_szMsg.Format(LoadStringById(IDS_LIGHTVIEW_STR015), Info.lightId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;
	}

	if(bModified)
	{
		if(bMsg) 	UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR016), MB_ICONINFORMATION);
		//RefreshLightList();
	}

}

void CLightView::PowerOff(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscLight.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscLight.GetItemData(nRow);
		if(!pos) continue;

		SLightInfo Info = m_lsLight.GetAt(pos);
		

		if(!CCopModule::GetObject()->DevicePowerOff(Info.siteId, Info.hostId, "Light", Info.lightId, ::ToString(Info.lightType)))
		{
			m_szMsg.Format(LoadStringById(IDS_LIGHTVIEW_STR017), Info.lightId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;
	}

	if(bModified)
	{
		if(bMsg) 	UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR018), MB_ICONINFORMATION);
		//RefreshLightList();
	}

}

