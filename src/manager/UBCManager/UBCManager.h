// UBCManager.h : main header file for the UBCManager application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols

// CUBCManagerApp:
// See UBCManager.cpp for the implementation of this class
//

class CUBCManagerApp : public CWinApp
{
public:
	CUBCManagerApp();

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	static BOOL SetThreadLocaleEx(LCID lcLocale);

// Implementation
protected:
	HMENU m_hMDIMenu;
	HACCEL m_hMDIAccel;

public:
	afx_msg void OnAppAbout();
	afx_msg void OnFileNew();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnLincenseInfo();
};

extern CUBCManagerApp theApp;