#pragma once

#include "common\HoverButton.h"
#include "common\utblistctrl.h"
#include "common\reposcontrol.h"

// CUserView form view

class CUserView : public CFormView
{
	DECLARE_DYNCREATE(CUserView)
public:
	enum { eCheck // 2010.02.17 by gwangsoo
		 , eUserSite, eUserID, eUserPerm, eUserPW, eUserName, eUserNo, eUserTel, eUserMail
		 , eUseAlarm, eAlarmList, eSiteList, eHostList, eUserEnd };

private:
	void RefreshList();
	void InitUserList();
	void InitPosition(CRect);
	void ModifyUser(CArray<int>&);
	CString SetHide(CString);

public:
private:
	bool m_bInit;
	CString m_szMsg;
//	CFont m_font;

	CHoverButton m_btnAddUser;
	CHoverButton m_btnDelUser;
	CHoverButton m_btnModUser;
	CHoverButton m_btnRfsUser;
	CHoverButton m_btnOptUser;

	CUTBListCtrl m_lscUser;
	CString m_szUserColum[eUserEnd];

	CRect m_rcClient;
	CReposControl m_Reposition;

	static int CALLBACK CompareUser(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
protected:
	CUserView();           // protected constructor used by dynamic creation
	virtual ~CUserView();

public:
	enum { IDD = IDD_USERVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnLvnColumnclickListUser(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListUser(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListUser(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListUser(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedButtonUseradd();
	afx_msg void OnBnClickedButtonUserdel();
	afx_msg void OnBnClickedButtonUsermod();
	afx_msg void OnBnClickedButtonUserrfs();
};


