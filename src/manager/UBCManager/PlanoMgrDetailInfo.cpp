// PlanoMgrDetailInfo.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "PlanoMgrDetailInfo.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "common\UbcCode.h"
#include "UBCCopCommon\UbcMenuAuth.h"

#define STR_OPERA_ON			LoadStringById(IDS_IMAGECONTENTSDIALOG_BUT002)
#define STR_OPERA_OFF			LoadStringById(IDS_SMSCONTENTSDIALOG_BUT009)

#define STR_HOST_TYPE			LoadStringById(IDS_ICONVIEW_STR001)
#define STR_LIGHT_TYPE			LoadStringById(IDS_ICONVIEW_STR002)
#define STR_ETC_TYPE			"ETC."

// CPlanoMgrDetailInfo 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPlanoMgrDetailInfo, CDialog)

CPlanoMgrDetailInfo::CPlanoMgrDetailInfo(CWnd* pParent /*=NULL*/)
	: CDialog(CPlanoMgrDetailInfo::IDD, pParent)
{
	m_IsCreateMode = false;
	m_PlanoMgrChanged = false;
}

CPlanoMgrDetailInfo::~CPlanoMgrDetailInfo()
{
}

void CPlanoMgrDetailInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PLANOID, m_editPlanoId);
	DDX_Control(pDX, IDC_COMBO_PLANOTYPE, m_cbPlanoType);
	DDX_Control(pDX, IDC_EDIT_DESC, m_editDesc);
	DDX_Control(pDX, IDC_COMBO_EXPAND, m_cbExpand);
	DDX_Control(pDX, IDC_PIC_PLANO, m_picPlanoMgr);
}


BEGIN_MESSAGE_MAP(CPlanoMgrDetailInfo, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_NEW, &CPlanoMgrDetailInfo::OnBnClickedButtonNew)
	ON_BN_CLICKED(IDOK, &CPlanoMgrDetailInfo::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPlanoMgrDetailInfo::OnBnClickedCancel)
	ON_EN_CHANGE(IDC_EDIT_PLANOID, &CPlanoMgrDetailInfo::OnEnChangeEditPlanoid)
END_MESSAGE_MAP()


// CPlanoMgrDetailInfo 메시지 처리기입니다.

BOOL CPlanoMgrDetailInfo::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_cbPlanoType.AddString(STR_HOST_TYPE);
	m_cbPlanoType.AddString(STR_LIGHT_TYPE);
	m_cbPlanoType.AddString(STR_ETC_TYPE);
	
	m_cbExpand.AddString(STR_OPERA_OFF);
	m_cbExpand.AddString(STR_OPERA_ON);

	if(m_IsCreateMode){
		m_cbPlanoType.SetCurSel(0);
		m_cbExpand.SetCurSel(0);
	}else{
		this->m_editPlanoId.EnableWindow(false);
		this->m_editPlanoId.SetWindowText(this->m_PlanoMgrInfo.planoId);
		this->m_editDesc.SetWindowText(this->m_PlanoMgrInfo.description);
	
		m_cbPlanoType.SetCurSel(this->m_PlanoMgrInfo.planoType);	
		m_cbExpand.SetCurSel(this->m_PlanoMgrInfo.expand);	

		CString szServerLocation = "/Config/Plano/" ;
		CString szLocalLocation;
		szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

		this->m_picPlanoMgr.SetSiteId(this->m_PlanoMgrInfo.siteId, this->m_PlanoMgrInfo.expand);
		this->m_picPlanoMgr.Load(this->m_PlanoMgrInfo.backgroundFile,szLocalLocation,szServerLocation);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPlanoMgrDetailInfo::OnBnClickedButtonNew()
{
	if(!SetInfo()){
		return;
	}

	CString szFileBackgroundFiles = "*.*";
	CString filter;
	filter.Format("All Files|%s|" , szFileBackgroundFiles	 );

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szBackgroundFile[MAX_PATH], ext[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, szBackgroundFile, ext);

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	// 파일을 Plano Directory 에 copy 한다.
	CString szLocalLocation;
	szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

	//패스가 없으면 만들어야 한다.  2017.10.30
	if(::access(szLocalLocation,0)!=0){
		//CString msg;
		//msg.Format("%s folder created", szLocalLocation);
		//UbcMessageBox(msg);
		::CreateDirectory(szLocalLocation, NULL);
	}else{
		//CString msg;
		//msg.Format("%s folder already exist", szLocalLocation);
		//UbcMessageBox(msg);
	}

	CString localFullFileName;
	localFullFileName.Format("%s%s%s", szLocalLocation, szBackgroundFile, ext);

	if(!::CopyFile(dlg.GetPathName(),localFullFileName ,false)){
		CString msg;
		msg.Format(LoadStringById(IDS_ICONVIEW_STR017), dlg.GetPathName(),localFullFileName);
		UbcMessageBox(msg);
		return;
	}

	TraceLog(("CopFile : %s --> %s succeed", dlg.GetPathName(),localFullFileName));

	m_PlanoMgrChanged = true;
	m_PlanoMgrInfo.backgroundFile.Format("%s%s",szBackgroundFile,ext);
	// 파일을 Display 한다.
	this->m_picPlanoMgr.SetSiteId(this->m_PlanoMgrInfo.siteId, this->m_PlanoMgrInfo.expand);
	this->m_picPlanoMgr.LocalLoad(m_PlanoMgrInfo.backgroundFile,szLocalLocation);
	this->m_picPlanoMgr.RedrawWindow();

	return;
}


BOOL CPlanoMgrDetailInfo::Upload()
{
	if(!m_PlanoMgrChanged) return true;

	CString szLocalLocation;
	szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

	CString szServerLocation = "/Config/Plano/" ;
	
	return this->m_picPlanoMgr.Upload(this->m_PlanoMgrInfo.backgroundFile,szLocalLocation,szServerLocation);
}

BOOL CPlanoMgrDetailInfo::SetInfo()
{
	UpdateData();
	m_editDesc.GetWindowText(m_PlanoMgrInfo.description);
	m_editPlanoId.GetWindowText(m_PlanoMgrInfo.planoId);

	if(m_PlanoMgrInfo.planoId.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_PLANOMGRVIEW_STR015));
		return FALSE;
	}

	m_PlanoMgrInfo.planoType = m_cbPlanoType.GetCurSel();
	m_PlanoMgrInfo.expand = m_cbExpand.GetCurSel();

	return TRUE;
}

void CPlanoMgrDetailInfo::OnBnClickedOk()
{
	if(this->m_IsCreateMode == true && this->m_PlanoMgrChanged == false){
		UbcMessageBox(LoadStringById(IDS_PLANOMGRVIEW_STR016));
		return ;
	}

	if(SetInfo()){
		OnOK();
	}
}

void CPlanoMgrDetailInfo::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}


void CPlanoMgrDetailInfo::SetInfo(SPlanoInfo& info)
{
	PlanoInfoList lsPlanoMgr;
	CCopModule::GetObject()->GetPlanoList(info.planoId, NULL,lsPlanoMgr);
	POSITION pos = lsPlanoMgr.GetHeadPosition();
	if(pos){
		m_PlanoMgrInfo.set((SPlanoInfo*)lsPlanoMgr.GetNext(pos));
	}else{
		m_PlanoMgrInfo.set(&info);
	}
}

void CPlanoMgrDetailInfo::GetInfo(SPlanoInfo& info)
{
	info.set(&m_PlanoMgrInfo);
}
void CPlanoMgrDetailInfo::OnEnChangeEditPlanoid()
{
	CString buf, retval;
	m_editPlanoId.GetWindowText(buf);
	int llen = buf.GetLength();

	const char* special = "*,[]{}()!~`@#$%^&-+=/\\\"\':;?.| \t\n";
	int rlen = strlen(special);

	int counter=0;
	for(int i=0;i<llen;i++){
		int aCh = buf.GetAt(i);
		bool invalid = false;
		for(int j=0;j<rlen;j++){
			if(aCh == special[j]){
				TraceLog(("%s has special char %c", buf, aCh));
				invalid = true;
				counter++;
				break;
			}
		}
		if(invalid == false) {
			retval += (TCHAR)aCh;
		}
	}
	if(counter>0){
		m_editPlanoId.SetWindowText(retval);
	}
}
