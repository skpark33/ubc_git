#pragma once

#include "ubccopcommon\eventhandler.h"


// CUpdateCenterFrame frame

class CUpdateCenterFrame : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CUpdateCenterFrame)
protected:
	CUpdateCenterFrame();           // protected constructor used by dynamic creation
	virtual ~CUpdateCenterFrame();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
