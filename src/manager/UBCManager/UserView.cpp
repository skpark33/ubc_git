// UserView.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "UserView.h"
#include "Enviroment.h"
#include "UserInfoDlg.h"

#define STR_USER_SITE			LoadStringById(IDS_USERVIEW_LST001)
#define STR_USER_ID				LoadStringById(IDS_USERVIEW_LST002)
#define STR_USER_PERMISSION		LoadStringById(IDS_USERVIEW_LST003)
#define STR_USER_PASSWORD		LoadStringById(IDS_USERVIEW_LST004)
#define STR_USER_NAME			LoadStringById(IDS_USERVIEW_LST005)
#define STR_USER_NUMBER			LoadStringById(IDS_USERVIEW_LST006)
#define STR_USER_TEL			LoadStringById(IDS_USERVIEW_LST007)
#define STR_USER_EMAIL			LoadStringById(IDS_USERVIEW_LST008)
#define STR_USER_USE_ALARM		LoadStringById(IDS_USERVIEW_LST009)
#define STR_USER_ALARM_LIST		LoadStringById(IDS_USERVIEW_LST010)
#define STR_USER_SITE_LIST		LoadStringById(IDS_USERVIEW_LST011)
#define STR_USER_HOST_LIST		LoadStringById(IDS_USERVIEW_LST012)

// CUserView
IMPLEMENT_DYNCREATE(CUserView, CFormView)

CUserView::CUserView() : CFormView(CUserView::IDD)
,	m_Reposition(this)
{
	m_bInit = false;
}

CUserView::~CUserView()
{
}

void CUserView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_USER, m_lscUser);
	DDX_Control(pDX, IDC_BUTTON_USERADD, m_btnAddUser);
	DDX_Control(pDX, IDC_BUTTON_USERDEL, m_btnDelUser);
	DDX_Control(pDX, IDC_BUTTON_USERMOD, m_btnModUser);
	DDX_Control(pDX, IDC_BUTTON_USERRFS, m_btnRfsUser);
	DDX_Control(pDX, IDC_BUTTON_USEROPT, m_btnOptUser);
}

BEGIN_MESSAGE_MAP(CUserView, CFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_USER, OnLvnColumnclickListUser)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_USER, OnNMCustomdrawListUser)
	ON_NOTIFY(NM_CLICK, IDC_LIST_USER, OnNMClickListUser)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_USER, OnNMDblclkListUser)

	ON_BN_CLICKED(IDC_BUTTON_USERADD, OnBnClickedButtonUseradd)
	ON_BN_CLICKED(IDC_BUTTON_USERDEL, OnBnClickedButtonUserdel)
	ON_BN_CLICKED(IDC_BUTTON_USERMOD, OnBnClickedButtonUsermod)
	ON_BN_CLICKED(IDC_BUTTON_USERRFS, OnBnClickedButtonUserrfs)
END_MESSAGE_MAP()


// CUserView diagnostics

#ifdef _DEBUG
void CUserView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CUserView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CUserView message handlers
int CUserView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	GetClientRect(&m_rcClient);
	GetEnvPtr()->InitUser();

	return 0;
}

void CUserView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	// TODO: Add your specialized code here and/or call the base class
	if(m_bInit)	return;
	m_bInit = true;

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_USERVIEW_STR001));

	//NONCLIENTMETRICS nc;
	//ZeroMemory(&nc, sizeof(nc));
	//nc.cbSize = sizeof(nc);
	//SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, &nc, 0);
	//m_font.CreateFontIndirect(&nc.lfMenuFont);
	//SetFont(&m_font);

	m_btnOptUser.ShowWindow(SW_HIDE);

	m_btnAddUser.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btnDelUser.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btnModUser.LoadBitmap(IDB_BUTTON_MODIFY, RGB(255,255,255));
	m_btnRfsUser.LoadBitmap(IDB_BUTTON_REFRESH2, RGB(255,255,255));

	m_btnAddUser.SetToolTipText(LoadStringById(IDS_USERVIEW_STR002));
	m_btnDelUser.SetToolTipText(LoadStringById(IDS_USERVIEW_STR003));
	m_btnModUser.SetToolTipText(LoadStringById(IDS_USERVIEW_STR004));
	m_btnRfsUser.SetToolTipText(LoadStringById(IDS_USERVIEW_STR005));
	m_btnOptUser.SetToolTipText(LoadStringById(IDS_USERVIEW_STR006));

	InitPosition(m_rcClient);
	InitUserList();
	RefreshList();
	SetScrollSizes(MM_TEXT, CSize(1,1));
}

void CUserView::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CFormView::OnClose();
}

void CUserView::OnDestroy()
{
	CFormView::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscUser.SaveColumnState("USER-LIST", szPath);
}

void CUserView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_Reposition.Move();
}

void CUserView::InitUserList()
{
	// 2010.02.17 by gwangsoo
	m_szUserColum[eCheck    ]	= _T("");
	m_szUserColum[eUserSite ]	= STR_USER_SITE;
	m_szUserColum[eUserID   ]	= STR_USER_ID;
	m_szUserColum[eUserPerm ]	= STR_USER_PERMISSION;
	m_szUserColum[eUserPW   ]	= STR_USER_PASSWORD;
	m_szUserColum[eUserName ]	= STR_USER_NAME;
	m_szUserColum[eUserNo   ]	= STR_USER_NUMBER;
	m_szUserColum[eUserTel  ]	= STR_USER_TEL;
	m_szUserColum[eUserMail ]	= STR_USER_EMAIL;
	m_szUserColum[eUseAlarm ]	= STR_USER_USE_ALARM;
	m_szUserColum[eAlarmList]	= STR_USER_ALARM_LIST;
	m_szUserColum[eSiteList ]	= STR_USER_SITE_LIST;
	m_szUserColum[eHostList ]	= STR_USER_HOST_LIST;

	m_lscUser.SetExtendedStyle(m_lscUser.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	for(int nCol = 0; nCol < eUserEnd; nCol++)
	{
		m_lscUser.InsertColumn(nCol, m_szUserColum[nCol], LVCFMT_LEFT, 100);
	}

	m_lscUser.SetColumnWidth(m_lscUser.GetColPos(m_szUserColum[eCheck]), 22); // 2010.02.17 by gwangsoo

	m_lscUser.InitHeader(IDB_LIST_HEADER);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscUser.LoadColumnState("USER-LIST", szPath);
}

void CUserView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscUser, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}

void CUserView::RefreshList()
{
	int nRow = 0;
	POSITION pos = GetEnvPtr()->m_lsUser.GetHeadPosition();
	m_lscUser.DeleteAllItems();

	while(pos)
	{
		POSITION posOld = pos;
		SUserInfo info = GetEnvPtr()->m_lsUser.GetNext(pos);

		// admin은 모두 리스트업 되어야하므로 admin이 아닌경우만 체크
		if( CCopModule::eSiteAdmin != GetEnvPtr()->m_Authority )
		{
			// 권한정보가 있는 사용자만 체크하고 그렇지않은경우 보이게 함
			if(CCopModule::eSiteUnkown == info.userType)
			{
				;
			}
			else
			{
				// 로그인한 사용자보다 권한이 더 높은 사용자정보는 보여주지 않음
				if( info.userType < GetEnvPtr()->m_Authority )
				{
					continue;
				}
				// 동일한 권한인경우 자신만 보이도록함
				else if( info.userType == GetEnvPtr()->m_Authority)
				{
					// 2011.02.23 super가 아닌 권한도 여러 그룹을 관리하도록함으로 site조건을 제거함
					if( //info.siteId == GetEnvPtr()->m_szSite    &&
						info.userId == GetEnvPtr()->m_szLoginID )
					{
						;
					}
					else
					{
						continue;
					}
				}
			}
		}

		CString strUseAlarm;
		if(info.useSms) strUseAlarm = "SMS";
		if(info.useEmail) strUseAlarm += (strUseAlarm.IsEmpty() ? "": ",") + CString("Email");

		CString strAlarmList = info.probableCauseList;
		strAlarmList.Replace("[","");
		strAlarmList.Replace("]","");
		strAlarmList.Replace("\"","");

		CString strSiteList = info.siteList;
		strSiteList.Replace("[","");
		strSiteList.Replace("]","");
		strSiteList.Replace("\"","");

		CString strHostList = info.hostList;
		strHostList.Replace("[","");
		strHostList.Replace("]","");
		strHostList.Replace("\"","");

		m_lscUser.InsertItem(nRow, "");
		m_lscUser.SetItemText(nRow, eUserSite , info.siteId);
		m_lscUser.SetItemText(nRow, eUserID   , info.userId);
		m_lscUser.SetItemText(nRow, eUserPerm , CCopModule::GetObject()->UserType(info.userType));
		m_lscUser.SetItemText(nRow, eUserPW   , SetHide(info.password));
		m_lscUser.SetItemText(nRow, eUserName , info.userName);
		m_lscUser.SetItemText(nRow, eUserNo   , info.sid);
		m_lscUser.SetItemText(nRow, eUserTel  , info.phoneNo);
		m_lscUser.SetItemText(nRow, eUserMail , info.email);
		m_lscUser.SetItemText(nRow, eUseAlarm , strUseAlarm);
		m_lscUser.SetItemText(nRow, eAlarmList, strAlarmList);
		m_lscUser.SetItemText(nRow, eSiteList , strSiteList);
		m_lscUser.SetItemText(nRow, eHostList , strHostList);

		m_lscUser.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lscUser.GetCurSortCol();
	if(-1 != nCol)
		m_lscUser.SetSortHeader(nCol, m_lscUser.IsAscend());
	else
		m_lscUser.SetSortHeader(eUserSite);
}

CString CUserView::SetHide(CString szStr)
{
	if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority)
		return szStr;

	int nLen = szStr.GetLength();
	CString szTemp = _T("");

	for(int i = 0; i < nLen; i++){
		szTemp += _T("*");
	}
	return szTemp;
}

int CUserView::CompareUser(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	CUserView* pView = (CUserView*)lParam;
	CUTBListCtrl* pListCtrl = &pView->m_lscUser;
	return pListCtrl->Compare(lParam1, lParam2);
}

void CUserView::OnLvnColumnclickListUser(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

//	int nRow = pNMLV->iItem;
	int	nColumn = pNMLV->iSubItem;
	
	// 2010.02.17 by gwangsoo
	if(nColumn == m_lscUser.GetColPos(m_szUserColum[eCheck]))
	{
		bool bCheck = !m_lscUser.GetCheckHdrState();
		for(int nRow=0; nRow<m_lscUser.GetItemCount() ;nRow++)
		{
			m_lscUser.SetCheck(nRow, bCheck);
		}
		m_lscUser.SetCheckHdrState(bCheck);
	}
	else
	{
		m_lscUser.SetSortHeader(nColumn);
		m_lscUser.SortItems(CompareUser, (DWORD_PTR)this);
	}
}

void CUserView::OnNMCustomdrawListUser(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
}

void CUserView::OnNMClickListUser(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;
}

void CUserView::OnNMDblclkListUser(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
//	int	nColumn = pNMLV->iSubItem;

	CArray<int> arRow;
	arRow.Add(nRow);
	ModifyUser(arRow);
}

void CUserView::OnBnClickedButtonUseradd()
{
	if(CCopModule::eSiteUser == GetEnvPtr()->m_Authority){
		UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG009), MB_ICONWARNING);
		return;
	}

	CUserInfoDlg dlg;
	if(dlg.DoModal() != IDOK) return;

	SUserInfo info = dlg.GetInfo();

	if(!CCopModule::GetObject()->AddUser(info))
	{
		UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG001), MB_ICONERROR);
		return;
	}

	GetEnvPtr()->m_lsUser.AddTail(info);
	RefreshList();
}

void CUserView::OnBnClickedButtonUserdel()
{
	if(CCopModule::eSiteUser == GetEnvPtr()->m_Authority)
	{
		UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG004), MB_ICONINFORMATION);
		return;
	}

	CArray<int> arRow;
	for(int nRow = m_lscUser.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscUser.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lscUser.GetItemData(nRow);
		if(!pos) continue;

		SUserInfo Info = GetEnvPtr()->m_lsUser.GetAt(pos);

		if(Info.siteId == GetEnvPtr()->m_szSite && Info.userId == GetEnvPtr()->m_szLoginID)
		{
			m_lscUser.SetCheck(nRow, FALSE);
			continue;
		}

		if(Info.siteId == "Default" && Info.userId == "super")
		{
			m_lscUser.SetCheck(nRow, FALSE);
			continue;
		}

		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG002), MB_ICONINFORMATION);
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG003), MB_YESNO|MB_ICONINFORMATION) != IDYES)
	{
		return;
	}

	int nDelCnt = 0;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);

		POSITION pos = (POSITION)m_lscUser.GetItemData(nRow);
		if(!pos) continue;

		SUserInfo Info = GetEnvPtr()->m_lsUser.GetAt(pos);

		if(Info.siteId.GetLength()==0 || Info.userId.GetLength()==0) continue;
		if(!CCopModule::GetObject()->DelUser(Info.siteId, Info.userId))
			continue;

		nDelCnt++;

		GetEnvPtr()->m_lsUser.RemoveAt(pos);
		m_lscUser.DeleteItem(nRow);
	}

	if(nDelCnt == 1)
	{
		m_szMsg.Format(LoadStringById(IDS_USERVIEW_MSG005));
		UbcMessageBox(m_szMsg, MB_ICONINFORMATION);
	}
	else if(nDelCnt > 1)
	{
		m_szMsg.Format(LoadStringById(IDS_USERVIEW_MSG006), nDelCnt);
		UbcMessageBox(m_szMsg, MB_ICONINFORMATION);
	}
}

void CUserView::OnBnClickedButtonUsermod()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscUser.GetItemCount(); nRow++){
		if(!m_lscUser.GetCheck(nRow))
			continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG007), MB_ICONINFORMATION);
		return;
	}

	ModifyUser(arRow);
}

void CUserView::ModifyUser(CArray<int>& arRow)
{
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++){
		int nRow = arRow.GetAt(nCnt);
		m_lscUser.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscUser.GetItemData(nRow);
		if(!pos)	continue;
		
		SUserInfo Info = GetEnvPtr()->m_lsUser.GetAt(pos);

		if(Info.userType < GetEnvPtr()->m_Authority){
			continue;
		}
		
		CUserInfoDlg dlg;
		dlg.SetInfo(Info);
		if(dlg.DoModal() != IDOK)
			continue;
		Info = dlg.GetInfo();

		if(!CCopModule::GetObject()->SetUser(Info)){
			m_szMsg.Format(LoadStringById(IDS_USERVIEW_MSG008), Info.userId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}

		GetEnvPtr()->m_lsUser.GetAt(pos) = Info;
	}

	RefreshList();
	TraceLog(("Modify user"));
}

void CUserView::OnBnClickedButtonUserrfs()
{
	SUserInfo info;
	if(CCopModule::eSiteAdmin != GetEnvPtr()->m_Authority){
		info.siteId = GetEnvPtr()->m_szSite;
	}

	GetEnvPtr()->RefreshUser(info);
	RefreshList();
	UbcMessageBox(LoadStringById(IDS_REFRASHLIST_MSG), MB_ICONINFORMATION);
}
