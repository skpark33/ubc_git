#pragma once

#include "resource.h"
#include "afxwin.h"
#include "common\utblistctrlex.h"
#include "common/libCLITransfer/CLITransfer.h"

// CRemoteDeleteFileDlg 대화 상자입니다.

class CRemoteDeleteFileDlg : public CDialog, CCLITransfer::IResultHandler
{
	DECLARE_DYNAMIC(CRemoteDeleteFileDlg)

public:
	enum { eResult, eHostVNCStat, eMonitorStat, eHostType, 
		   eSiteName, eHostName, eHostID, eHostIP,
		   eHostEnd };
	enum { eMonitorOn, eMonitorOff, eMonitorUnknown, eVncOn, eVncOff };

	CRemoteDeleteFileDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRemoteDeleteFileDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_REMOTE_DELETE_FILE_DLG };

	// input
	CList<SHostInfo> m_lsSelHost;
	bool			m_bIsPlaying;

private:
	CCLITransfer	m_CliTransfer;
	CImageList		m_ilHostList;
	CUTBListCtrlEx	m_lcList;
	CString			m_szHostColum[eHostEnd];

	void InitList();
	BOOL RefreshHost(bool bCompMsg=true);
	void RefreshHostList();
	void UpdateListRow(int nRow, SHostInfo* pInfo);

	void CLIResultEvent(CCLITransfer::ITEM item);
	void CLIFinishEvent();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBnFilesave();
	afx_msg void OnBnClickedBnFoldername();
};
