#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "Enviroment.h"
#include "ie_bulletinboard.h"
#include "common\reposcontrol.h"


// CBulletinboardDlg 대화 상자입니다.

class CBulletinboardDlg : public CDialog
{
	DECLARE_DYNAMIC(CBulletinboardDlg)

public:
	enum OPEN_TYPE { NOTICE_POPUP, NOTICE_LIST };
	CBulletinboardDlg(OPEN_TYPE nType, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBulletinboardDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_BULLETINBOARD_DLG };

private:
	OPEN_TYPE m_nOpenType;

	CStatic m_txtVersion;
	CIe_bulletinboard m_ieBulletinBoard;

	CRect m_rcClient;
	CReposControl m_Reposition;

	void InitPosition(CRect rc);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_EVENTSINK_MAP()
	void WindowClosingIeBulletinboard(BOOL IsChildWindow, BOOL* Cancel);
};
