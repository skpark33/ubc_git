// FaultMngView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "Enviroment.h"

#include "common\ubcdefine.h"
#include "FaultMngView.h"
#include "InputBox.h"
#include "UBCCopCommon\UbcMenuAuth.h"

//#include "server\fm\libFmEvent\fmUtil.h"
#include "common\\libCommon\ubcUtil.h"

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE 		RGB(255,255,255)
#define COLOR_RED   		RGB(255,  0,  0)
#define COLOR_ORANGE		RGB(255,175, 96)
#define COLOR_YELLOW		RGB(255,255,  0)

struct tagCOLOR
{
	COLORREF color;
	TCHAR    text[20];
} SEVERITY[6] = {{COLOR_WHITE , _T("Cleared")}
				,{COLOR_RED   , _T("Critical")}
				,{COLOR_ORANGE, _T("Major")}
				,{COLOR_YELLOW, _T("Minor")}
				,{COLOR_WHITE , _T("Warning")}
				,{COLOR_WHITE , _T("Indeter")}};

// CFaultMngView

IMPLEMENT_DYNCREATE(CFaultMngView, CFormView)

CFaultMngView::CFaultMngView()
	: CFormView(CFaultMngView::IDD)
,	m_Reposition(this)
,	m_szHostID(_T(""))
,	m_szSiteID(_T(""))
{
}

CFaultMngView::~CFaultMngView()
{
}

void CFaultMngView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CUR_FAULT, m_lscList);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_Filter);
	DDX_Text(pDX, IDC_EDIT_FILTER_SITEID, m_szSiteID);
	DDX_Text(pDX, IDC_EDIT_FILTER_HOSTID, m_szHostID);
	DDX_Control(pDX, IDC_DATE_FILTER_START, m_dtStart);
	DDX_Control(pDX, IDC_DATE_FILTER_END, m_dtEnd);

	DDX_Control(pDX, IDC_BN_REFRESH, m_btnRefresh);
	DDX_Control(pDX, IDC_BN_DELETE_FAULT, m_btnDelFault);
	DDX_Control(pDX, IDC_COMBO_SEVERITY, m_cbSeverity);
	DDX_Control(pDX, IDC_COMBO_CAUSE, m_cbCause);
}

BEGIN_MESSAGE_MAP(CFaultMngView, CFormView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_REFRESH, OnBnClickedBnRefresh)
	ON_BN_CLICKED(IDC_BN_DELETE_FAULT, &CFaultMngView::OnFaultviewBnDelete)
	ON_COMMAND(ID_FAULTVIEW_DELETE, &CFaultMngView::OnFaultviewDelete)
	ON_COMMAND(ID_FAULTVIEW_ADMIN_COMMENT, &CFaultMngView::OnFaultviewAdminComment)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CUR_FAULT, &CFaultMngView::OnNMRclickListCurFault)
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
END_MESSAGE_MAP()


// CFaultMngView 진단입니다.

#ifdef _DEBUG
void CFaultMngView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CFaultMngView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CFaultMngView 메시지 처리기입니다.

// CFaultMngView message handlers
int CFaultMngView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	GetClientRect(&m_rcClient);
//	GetEnvPtr()->InitSite();

	return 0;
}

void CFaultMngView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_FAULTMNGVIEW_STR001));

	// 심각도
	//m_cbSeverity.AddColor(_T("All")     , COLOR_WHITE );	// white
	//m_cbSeverity.AddColor(_T("Critical"), COLOR_RED   );	// Red
	//m_cbSeverity.AddColor(_T("Major")   , COLOR_ORANGE);	// Orange
	//m_cbSeverity.AddColor(_T("Minor")   , COLOR_YELLOW);	// Yellow
	m_cbSeverity.AddColor(_T("All")       , SEVERITY[0].color);
	m_cbSeverity.AddColor(SEVERITY[1].text, SEVERITY[1].color);
	m_cbSeverity.AddColor(SEVERITY[2].text, SEVERITY[2].color);
	m_cbSeverity.AddColor(SEVERITY[3].text, SEVERITY[3].color);
	m_cbSeverity.SetSelectedColorName(_T("All"));

	// 장애원인
	m_cbCause.AddString(_T("All"));
	//m_cbCause.AddString(SERVER_PROCESS_DOWN		 );
	//m_cbCause.AddString(SERVER_COMMUNICATION_FAIL);
	//m_cbCause.AddString(SERVER_VNC_FAIL			 );
	//m_cbCause.AddString(SERVER_HDD_OVERLOAD		 );
	//m_cbCause.AddString(SERVER_MEM_OVERLOAD		 );
	//m_cbCause.AddString(SERVER_CPU_OVERLOAD		 );
	//m_cbCause.AddString(SERVER_NET_OVERLOAD		 );
	//m_cbCause.AddString(SERVER_DB_OVERLOAD		 );
	m_cbCause.AddString(HOST_PROCESS_DOWN		 );
	m_cbCause.AddString(HOST_COMMUNICATION_FAIL	 );
	//m_cbCause.AddString(HOST_VNC_FAIL			 );
	//m_cbCause.AddString(HOST_FTP_FAIL			 );
	//m_cbCause.AddString(HOST_UPDATE_FAIL		 );
	//m_cbCause.AddString(HOST_PLAY_CONTENTS_FAIL	 );
	//m_cbCause.AddString(HOST_CHANGE_PACKAGE_FAIL);
	//m_cbCause.AddString(HOST_SCREENSHOT_FAIL	 );
	m_cbCause.AddString(HOST_HDD_OVERLOAD		 );
	m_cbCause.AddString(HOST_MEM_OVERLOAD		 );
	m_cbCause.AddString(HOST_CPU_OVERLOAD		 );
	//m_cbCause.AddString(HOST_NET_OVERLOAD		 );
	m_cbCause.SetCurSel(0);

	m_dtStart.SetFormat(_T("yyyy/MM/dd HH:mm:ss"));
	m_dtEnd  .SetFormat(_T("yyyy/MM/dd HH:mm:ss"));

	m_dtStart.SetTime();	// 날짜를 선택하지 않은 상태로 설정
	m_dtEnd  .SetTime();	// 날짜를 선택하지 않은 상태로 설정

	m_btnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnDelFault.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));

	m_btnRefresh.SetToolTipText(LoadStringById(IDS_FAULTMNGVIEW_BTN001));
	m_btnDelFault.SetToolTipText(LoadStringById(IDS_FAULTMNGVIEW_BTN002));

	// 권한에 따라 활성화 한다.
	m_btnRefresh.EnableWindow(IsAuth(_T("FQRY")));
	m_btnDelFault.EnableWindow(IsAuth(_T("FDEL")));

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("HOST-FILTER", "SiteID", "", buf, 1024, szPath);
	m_szSiteID = buf;
	GetPrivateProfileString("HOST-FILTER", "HostID", "", buf, 1024, szPath);
	m_szHostID = buf;

	UpdateData(FALSE);

	InitPosition(m_rcClient);
	InitFaultList();

//	RefreshList();

	SetScrollSizes(MM_TEXT, CSize(1,1));
}

void CFaultMngView::OnDestroy()
{
	CFormView::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscList.SaveColumnState("FAULT-CUR-LIST", szPath);
}

void CFaultMngView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_Filter , REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_Reposition.AddControl(&m_lscList, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}

void CFaultMngView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_Reposition.Move();
}

void CFaultMngView::InitFaultList()
{
	m_szListColum[eCheck         ] = _T("");
	m_szListColum[eSiteID        ] = LoadStringById(IDS_FAULTMNGVIEW_LIST001);
	m_szListColum[eHostID        ] = LoadStringById(IDS_FAULTMNGVIEW_LIST002);
	m_szListColum[eCounter       ] = LoadStringById(IDS_FAULTMNGVIEW_LIST003);
	m_szListColum[eSeverity      ] = LoadStringById(IDS_FAULTMNGVIEW_LIST004);
	m_szListColum[eCause         ] = LoadStringById(IDS_FAULTMNGVIEW_LIST005);
	m_szListColum[eEventTime     ] = LoadStringById(IDS_FAULTMNGVIEW_LIST006);
	m_szListColum[eUpdateTime    ] = LoadStringById(IDS_FAULTMNGVIEW_LIST007);
	m_szListColum[eAdditionalText] = LoadStringById(IDS_FAULTMNGVIEW_LIST008);

	m_lscList.SetExtendedStyle(m_lscList.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_SUBITEMIMAGES);

	m_lscList.InsertColumn(eCheck         , m_szListColum[eCheck         ], LVCFMT_LEFT  ,  22);
	m_lscList.InsertColumn(eSiteID        , m_szListColum[eSiteID        ], LVCFMT_LEFT  , 120);
	m_lscList.InsertColumn(eHostID        , m_szListColum[eHostID        ], LVCFMT_LEFT  , 120);
	m_lscList.InsertColumn(eCounter       , m_szListColum[eCounter       ], LVCFMT_RIGHT ,  65);
	m_lscList.InsertColumn(eSeverity      , m_szListColum[eSeverity      ], LVCFMT_LEFT  ,  60);
	m_lscList.InsertColumn(eCause         , m_szListColum[eCause         ], LVCFMT_LEFT  , 150);
	m_lscList.InsertColumn(eEventTime     , m_szListColum[eEventTime     ], LVCFMT_CENTER, 120);
	m_lscList.InsertColumn(eUpdateTime    , m_szListColum[eUpdateTime    ], LVCFMT_CENTER, 120);
	m_lscList.InsertColumn(eAdditionalText, m_szListColum[eAdditionalText], LVCFMT_LEFT  , 150);

	CBitmap bmSeverity;
	bmSeverity.LoadBitmap(IDB_SEVERITY_COLOR_LIST);
	m_ilList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilList.Add(&bmSeverity, RGB(255, 255, 255));
	m_lscList.SetImageList(&m_ilList, LVSIL_SMALL);

	m_lscList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscList.LoadColumnState("FAULT-CUR-LIST", szPath);
}

void CFaultMngView::RefreshFaultList()
{
	int nRow = 0;
	POSITION pos = GetEnvPtr()->m_lsFault.GetHeadPosition();
	m_lscList.DeleteAllItems();

	while(pos)
	{
		POSITION posOld = pos;
		SFaultInfo info = GetEnvPtr()->m_lsFault.GetNext(pos);

		m_lscList.InsertItem(nRow, _T(""), -1);

		UpdateFaultRow(nRow, info);

		m_lscList.SetItemData(nRow++, (DWORD_PTR)posOld);
	}
}

void CFaultMngView::UpdateFaultRow(int nRow, SFaultInfo& info)
{
	m_lscList.SetText(nRow, m_szListColum[eSiteID        ], info.siteId        );
	m_lscList.SetText(nRow, m_szListColum[eHostID        ], info.hostId        );
	m_lscList.SetText(nRow, m_szListColum[eCounter       ], ::ToString(info.counter));
	m_lscList.SetText(nRow, m_szListColum[eSeverity      ], ::ToString(info.severity));
	m_lscList.SetText(nRow, m_szListColum[eCause         ], info.probableCause );
	m_lscList.SetText(nRow, m_szListColum[eEventTime     ], info.eventTime.Format(_T("%Y/%m/%d %H:%M:%S")));
	m_lscList.SetText(nRow, m_szListColum[eUpdateTime    ], info.updateTime.Format(_T("%Y/%m/%d %H:%M:%S")));
	m_lscList.SetText(nRow, m_szListColum[eAdditionalText], info.additionalText);

	LVITEM item;
	item.iItem = nRow;
	item.iSubItem = eSeverity;
	item.mask = LVIF_IMAGE|LVIF_TEXT;
	item.pszText = SEVERITY[info.severity].text;
	item.iImage = info.severity;
	m_lscList.SetItem(&item);
}

LRESULT CFaultMngView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case IEH_FLTALAM:
		return FaultAlarm((SFault*)lParam);
	case IEH_UPDALAM:
		return UpdateAlarm((SFaultUpdate*)lParam);
	case IEH_UNKOWN:
		break;
	}
	return 0;
}

int CFaultMngView::FaultAlarm(SFault* pData)
{
	if(pData)
	{
		SFaultInfo info;

		info.faultId        = pData->faultId       ;
		info.siteId         = pData->siteId        ;
		info.hostId         = pData->hostId        ;
		info.counter        = pData->counter       ;
		info.severity       = pData->severity      ;
		info.probableCause  = pData->probableCause ;
		info.eventTime      = pData->eventTime     ;
		info.updateTime     = pData->updateTime    ;
		info.additionalText = pData->additionalText;

		TraceLog(("faultId        [%s]", info.faultId       ));
		TraceLog(("siteId         [%s]", info.siteId        ));
		TraceLog(("hostId         [%s]", info.hostId        ));
		TraceLog(("counter        [%d]", info.counter       ));
		TraceLog(("severity       [%d]", info.severity      ));
		TraceLog(("probableCause  [%s]", info.probableCause ));
		TraceLog(("eventTime      [%s]", info.eventTime.Format("%Y/%m/%d %H:%M:%S")));
		TraceLog(("updateTime     [%s]", info.updateTime.Format("%Y/%m/%d %H:%M:%S")));
		TraceLog(("additionalText [%s]", info.additionalText));

		if(info.severity == FLT_CLEARED)
		{
			for(int nRow = 0; nRow < m_lscList.GetItemCount(); nRow++)
			{
				POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
				if(!pos) continue;

				SFaultInfo info = GetEnvPtr()->m_lsFault.GetAt(pos);
				if(info.faultId == pData->faultId)
				{
					GetEnvPtr()->m_lsFault.RemoveAt(pos);
					m_lscList.DeleteItem(nRow);
					break;
				}
			}
		}
		else
		{
			// 0000746: 장애관리화면에서 소팅을 "최근" 건이 위로 올라오게 바꾸어 준다.
			POSITION pos = GetEnvPtr()->m_lsFault.AddHead(info);

			m_lscList.InsertItem(0, _T(""), -1);
			UpdateFaultRow(0, info);

			m_lscList.SetItemData(0, (DWORD_PTR)pos);
		}
	}

	return 0;
}

int CFaultMngView::UpdateAlarm(SFaultUpdate* pData)
{
	for(int nRow = 0; nRow < m_lscList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
		if(!pos) continue;

		SFaultInfo info = GetEnvPtr()->m_lsFault.GetAt(pos);

		if(info.faultId != pData->faultId)
			continue;

		if(pData->bCounter       ) info.counter        = pData->counter       ;
		if(pData->bUpdateTime    ) info.updateTime     = pData->updateTime    ;
		if(pData->bAdditionalText) info.additionalText = pData->additionalText;

		GetEnvPtr()->m_lsFault.GetAt(pos) = info;

		UpdateFaultRow(nRow, info);

		m_lscList.SetItemData(nRow++, (DWORD_PTR)pos);

		return 0;
	}

	return 0;
}

void CFaultMngView::OnBnClickedBnRefresh()
{
	UpdateData();

	RefreshList();
}

void CFaultMngView::RefreshList()
{
	CString szWhere;

	if(!m_szSiteID.IsEmpty())
	{
		if(!szWhere.IsEmpty())
			szWhere += " and ";
		szWhere += "siteId like '%";
		szWhere += m_szSiteID;
		szWhere += "%'";
	}

	if(!m_szHostID.IsEmpty())
	{
		if(!szWhere.IsEmpty())
			szWhere += " and ";
		szWhere += "hostId like '%";
		szWhere += m_szHostID;
		szWhere += "%'";
	}

	if(m_cbSeverity.GetCurSel() > 0)
	{
		if(!szWhere.IsEmpty())
			szWhere += " and ";
		szWhere += "severity = ";
		szWhere += ::ToString(m_cbSeverity.GetCurSel());
		szWhere += "";
	}

	if(m_cbCause.GetCurSel() > 0)
	{
		CString strCause;
		m_cbCause.GetWindowText(strCause);

		if(!szWhere.IsEmpty())
			szWhere += " and ";
		szWhere += "probableCause = '";
		szWhere += strCause;
		szWhere += "'";
	}

	CTime tmStart;
	if(m_dtStart.GetTime(tmStart) == GDT_VALID)
	{
		if(!szWhere.IsEmpty())
			szWhere += " and ";

		szWhere.AppendFormat("updateTime >= CAST('%s' as datetime)" , tmStart.Format(_T("%Y/%m/%d %H:%M:%S")));
	}

	CTime tmEnd;
	if(m_dtEnd.GetTime(tmEnd) == GDT_VALID)
	{
		if(!szWhere.IsEmpty())
			szWhere += " and ";

		szWhere.AppendFormat("updateTime <= CAST('%s' as datetime)", tmEnd.Format(_T("%Y/%m/%d %H:%M:%S")));
	}

	// 0000746: 장애관리화면에서 소팅을 "최근" 건이 위로 올라오게 바꾸어 준다.
	szWhere += " order by updateTime DESC";
	szWhere.Trim();

	CWaitMessageBox wait;

	GetEnvPtr()->RefreshFault(szWhere);
	RefreshFaultList();
}

void CFaultMngView::OnNMRclickListCurFault(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0) return;

	POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
	if(!pos) return;
	SFaultInfo info = GetEnvPtr()->m_lsFault.GetAt(pos);

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);

	pPopup = menu.GetSubMenu(3);

	pPopup->EnableMenuItem(ID_FAULTVIEW_DELETE, (IsAuth(_T("FDEL")) ?MF_ENABLED : MF_DISABLED | MF_GRAYED));

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CFaultMngView::OnFaultviewBnDelete()
{
	CArray<SFaultInfo> arRow;
	for(int nRow = 0; nRow < m_lscList.GetItemCount(); nRow++)
	{
		if(!m_lscList.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lscList.GetItemData(nRow);
		if(!pos) continue;
		SFaultInfo info = GetEnvPtr()->m_lsFault.GetAt(pos);

		arRow.Add(info);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_FAULTMNGVIEW_MSG001), MB_ICONINFORMATION);
		return;
	}

	CWaitMessageBox wait;

	int nDeleteCnt = 0;
	for(int i=0; i<arRow.GetSize() ;i++)
	{
		SFaultInfo info = arRow.GetAt(i);

		int nResultCnt = ubcUtil::getInstance()->removeFaultCall(info.siteId
														  , info.hostId
														  , info.faultId
														  , info.severity
														  , info.probableCause
														  , info.additionalText
														  );
		if( nResultCnt > 0)
		{
			nDeleteCnt += nResultCnt;
		}
	}

	if(nDeleteCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_FAULTMNGVIEW_MSG004));
	}
	else
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_FAULTMNGVIEW_MSG002), nDeleteCnt);
		UbcMessageBox(strMsg);
	}
}

void CFaultMngView::OnFaultviewDelete()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lscList.GetItemData(m_ptSelList.y);
	if(!pos) return;
	SFaultInfo info = GetEnvPtr()->m_lsFault.GetAt(pos);

	int nDeleteCnt = ubcUtil::getInstance()->removeFaultCall(info.siteId
													  , info.hostId
													  , info.faultId
													  , info.severity
													  , info.probableCause
													  , info.additionalText
													  );
	if(nDeleteCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_FAULTMNGVIEW_MSG004));
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_FAULTMNGVIEW_MSG003));
	}
}

void CFaultMngView::OnFaultviewAdminComment()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lscList.GetItemData(m_ptSelList.y);
	if(!pos) return;
	SFaultInfo info = GetEnvPtr()->m_lsFault.GetAt(pos);

	CInputBox dlg;
	dlg.m_strTitle = LoadStringById(IDS_FAULTMNGVIEW_LIST008);
	dlg.m_strInputText = info.additionalText;

	if(dlg.DoModal() != IDOK) return;

	info.additionalText = dlg.m_strInputText;
	CCopModule::GetObject()->SetFault(info);
}
