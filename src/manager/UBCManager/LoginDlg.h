#pragma once

#include "common\HoverButton.h"

// CLoginDlg dialog
class CLoginDlg : public CDialog
{
	DECLARE_DYNAMIC(CLoginDlg)

public:
	CLoginDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLoginDlg();

// Dialog Data
	enum { IDD = IDD_LOGIN };

	static UINT WaitForLogin(LPVOID pParam);
	CWinThread* m_pLoginThread;

public:
	int m_nPort;
	CString m_szIP;
	CString m_szName;

	CString m_strSite;
	CString m_strID;
	CString m_strPW;

	bool			m_bInit;
	CStatic			m_BackImage;
	CHoverButton	m_btLogin;
	CHoverButton	m_btExit;
	CHoverButton	m_btConInfo;
	CEdit m_ebSite;
	CEdit m_ebID;
	CEdit m_ebPW;

	CBitmap m_bmpLoginBG;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonConinfo();
	afx_msg void OnBnClickedOk();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	LRESULT LoginComplete(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedCancel();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
};
