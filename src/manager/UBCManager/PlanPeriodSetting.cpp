// PlanPeriodSetting.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "PlanPeriodSetting.h"


// CPlanPeriodSetting 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPlanPeriodSetting, CDialog)

CPlanPeriodSetting::CPlanPeriodSetting(CWnd* pParent /*=NULL*/)
	: CSubBroadcastPlan(CPlanPeriodSetting::IDD, pParent)
{

}

CPlanPeriodSetting::~CPlanPeriodSetting()
{
}

void CPlanPeriodSetting::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DT_START, m_dtStart);
	DDX_Control(pDX, IDC_DT_END, m_dtEnd);
	DDX_Control(pDX, IDC_KB_WEEK0, m_kbWeek[0]);
	DDX_Control(pDX, IDC_KB_WEEK1, m_kbWeek[1]);
	DDX_Control(pDX, IDC_KB_WEEK2, m_kbWeek[2]);
	DDX_Control(pDX, IDC_KB_WEEK3, m_kbWeek[3]);
	DDX_Control(pDX, IDC_KB_WEEK4, m_kbWeek[4]);
	DDX_Control(pDX, IDC_KB_WEEK5, m_kbWeek[5]);
	DDX_Control(pDX, IDC_KB_WEEK6, m_kbWeek[6]);
	DDX_Control(pDX, IDC_CAL_EXCEPTDAY, m_calExceptDay);
	DDX_Control(pDX, IDC_BN_EXCEPTDAY_ADD, m_bnExceptDayAdd);
	DDX_Control(pDX, IDC_BN_EXCEPTDAY_DEL, m_bnExceptDayDel);
	DDX_Control(pDX, IDC_LC_EXCEPTDAY, m_lcExceptDay);
	DDX_Control(pDX, IDC_LC_VIEW, m_lcView);
	DDX_Control(pDX, IDC_DT_DOWNLOAD_TIME, m_dtDownloadTime);
	DDX_Control(pDX, IDC_KB_IMMEDIATE, m_kbImmediate);
	DDX_Control(pDX, IDC_EB_RETRY_CNT, m_ebRetryCnt);
	DDX_Control(pDX, IDC_SPIN_RETRY_CNT, m_spRetryCnt);
	DDX_Control(pDX, IDC_EB_RETRY_INTERVAL, m_ebRetryInterval);
	DDX_Control(pDX, IDC_SPIN_RETRY_INTERVAL, m_spRetryInterval);
}

BEGIN_MESSAGE_MAP(CPlanPeriodSetting, CDialog)
	ON_BN_CLICKED(IDOK, &CPlanPeriodSetting::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPlanPeriodSetting::OnBnClickedCancel)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DT_START, &CPlanPeriodSetting::OnDtnDatetimechangeDtStart)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DT_END, &CPlanPeriodSetting::OnDtnDatetimechangeDtEnd)
	ON_BN_CLICKED(IDC_BN_EXCEPTDAY_ADD, &CPlanPeriodSetting::OnBnClickedBnExceptdayAdd)
	ON_BN_CLICKED(IDC_BN_EXCEPTDAY_DEL, &CPlanPeriodSetting::OnBnClickedBnExceptdayDel)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_KB_WEEK0, IDC_KB_WEEK6, &CPlanPeriodSetting::OnBnClickedKbWeek)
	ON_NOTIFY(NM_DBLCLK, IDC_LC_EXCEPTDAY, &CPlanPeriodSetting::OnNMDblclkLcExceptday)
	ON_BN_CLICKED(IDC_KB_IMMEDIATE, &CPlanPeriodSetting::OnBnClickedKbImmediate)
END_MESSAGE_MAP()


void CPlanPeriodSetting::OnBnClickedOk() {}
void CPlanPeriodSetting::OnBnClickedCancel() {}
// CPlanPeriodSetting 메시지 처리기입니다.

bool CPlanPeriodSetting::IsModified()
{
	return !(m_oldInfo == *GetBroadcastPlanInfo());
}

void CPlanPeriodSetting::UpdateInfo()
{
	m_dtStart.GetTime(GetBroadcastPlanInfo()->tmStartDate);
	m_dtEnd  .GetTime(GetBroadcastPlanInfo()->tmEndDate  );

	GetBroadcastPlanInfo()->strWeekInfo.Empty();

	for(int i=0; i<7 ;i++)
	{
		if(m_kbWeek[i].GetCheck())
		{
			CString strWeekDay;
			strWeekDay.Format(_T("%s%d")
							, (GetBroadcastPlanInfo()->strWeekInfo.IsEmpty() ? _T("") : _T(","))
							, i
							);

			GetBroadcastPlanInfo()->strWeekInfo += strWeekDay;
		}
	}

	GetBroadcastPlanInfo()->strExceptDay.Empty();

	for(int i=0; i<m_lcExceptDay.GetItemCount() ;i++)
	{
		CString strExceptDay = m_lcExceptDay.GetItemText(i, eExceptDay);
		strExceptDay.Replace(_T("/"), _T("."));

		GetBroadcastPlanInfo()->strExceptDay += (GetBroadcastPlanInfo()->strExceptDay.IsEmpty() ? _T("") : _T(","))
											  + strExceptDay;
	}

	if(m_kbImmediate.GetCheck())
	{
		GetBroadcastPlanInfo()->tmDownloadTime = CTime::GetCurrentTime();
	}
	else
	{
		m_dtDownloadTime.GetTime(GetBroadcastPlanInfo()->tmDownloadTime);
	}

	GetBroadcastPlanInfo()->nRetryCount = m_ebRetryCnt.GetValueInt();
	GetBroadcastPlanInfo()->nRetryGap = m_ebRetryInterval.GetValueInt();
}

bool CPlanPeriodSetting::InputErrorCheck(bool bMsg)
{
	CTime tmStart, tmEnd, tmDownloadTime;
	m_dtStart.GetTime(tmStart);
	m_dtEnd  .GetTime(tmEnd  );
	m_dtDownloadTime.GetTime(tmDownloadTime);

	if(tmStart >= tmEnd)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_PLANPERIOD_MSG001));
		return false;
	}

	bool bSelWeekDay = false;
	for(int i=0; i<7 ;i++)
	{
		if(m_kbWeek[i].GetCheck())
		{
			bSelWeekDay = true;
			break;
		}
	}

	if(!bSelWeekDay)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_PLANPERIOD_MSG002));
		return false;
	}

	//if(tmDownloadTime > tmStart)
	if(!m_kbImmediate.GetCheck() && tmDownloadTime < CTime::GetCurrentTime())
	{
		if(bMsg && UbcMessageBox(LoadStringById(IDS_PLANPERIOD_MSG003), MB_YESNO) != IDYES) return false;
		m_kbImmediate.SetCheck(TRUE);
	}

	if(m_ebRetryCnt.GetValueInt() < 0)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_PLANPERIOD_MSG005));
		return false;
	}
	if(m_ebRetryInterval.GetValueInt() < 0)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_PLANPERIOD_MSG006));
		return false;
	}
	if(m_ebRetryCnt.GetValueInt() > 0 && m_ebRetryInterval.GetValueInt() <= 0)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_PLANPERIOD_MSG004));
		return false;
	}

	return true;
}

void CPlanPeriodSetting::RefreshInfo()
{
	if(GetBroadcastPlanInfo()->nProcType == PROC_TYPE::PROC_NEW)
	{
		CTime tmCur = CTime::GetCurrentTime();

		int nAddMonth = 1; // 1개월뒤로 설정
		int nTotalMonth = tmCur.GetYear() * 12 + tmCur.GetMonth() + nAddMonth;

		CTime tmEndDate  = CTime( nTotalMonth/12 - ((nTotalMonth%12) == 0 ? 1 : 0)
								, ((nTotalMonth%12) == 0 ? 12 : (nTotalMonth%12))
								, tmCur.GetDay()
								, tmCur.GetHour()
								, tmCur.GetMinute()
								, tmCur.GetSecond()
								);
		tmEndDate -= CTimeSpan(1, 0, 0, 0);

		m_dtStart.SetTime(&tmCur);
		m_dtEnd  .SetTime(&tmEndDate);
		m_dtDownloadTime.SetTime(&tmCur);
	}
	else
	{
		m_dtStart.SetTime(&GetBroadcastPlanInfo()->tmStartDate);
		m_dtEnd  .SetTime(&GetBroadcastPlanInfo()->tmEndDate  );
		m_dtDownloadTime.SetTime(&GetBroadcastPlanInfo()->tmDownloadTime);
	}

	InitDateRange(m_dtStart);
	InitDateRange(m_dtEnd);
	InitDateRange(m_dtDownloadTime);

	for(int i=0; i<7 ;i++)
	{
		CString strWeek;
		strWeek.Format(_T("%d"), i);
		m_kbWeek[i].SetCheck(GetBroadcastPlanInfo()->strWeekInfo.Find(strWeek) >= 0);
	}

	m_kbImmediate.SetCheck(FALSE);
	m_dtDownloadTime.EnableWindow(!m_kbImmediate.GetCheck());

	if(GetBroadcastPlanInfo()->nRetryCount < 0) GetBroadcastPlanInfo()->nRetryCount = 0;
	m_ebRetryCnt.SetValue(GetBroadcastPlanInfo()->nRetryCount);

	if(GetBroadcastPlanInfo()->nRetryGap < 0) GetBroadcastPlanInfo()->nRetryGap = 0;
	m_ebRetryInterval.SetValue(GetBroadcastPlanInfo()->nRetryGap);

	RefreshExceptDayList();
	RefreshViewList();
}

BOOL CPlanPeriodSetting::OnInitDialog()
{
	CSubBroadcastPlan::OnInitDialog();

	m_oldInfo = *GetBroadcastPlanInfo();

	m_dtStart.SetFormat(_T("yyyy/MM/dd HH:mm:ss"));
	m_dtEnd  .SetFormat(_T("yyyy/MM/dd HH:mm:ss"));

	m_bnExceptDayAdd.LoadBitmap(IDB_BUTTON_NEXT2, RGB(255,255,255));
	m_bnExceptDayDel.LoadBitmap(IDB_BUTTON_PREV2, RGB(255,255,255));

	// todo db컬럼이 추가되면 초기화 및 저장 로직 구현할것.
	m_dtDownloadTime.SetFormat(_T("yyyy/MM/dd HH:mm:ss"));

	m_ebRetryCnt.SetValue(3);
	m_spRetryCnt.SetRange(0, 10);

	m_ebRetryInterval.SetValue(60);
	m_spRetryInterval.SetRange(1, 60 * 24);

	InitExceptDayList();

	InitViewList();

	RefreshInfo();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPlanPeriodSetting::InitExceptDayList()
{
	m_lcExceptDay.SetExtendedStyle(m_lcExceptDay.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck    ] = _T("");
	m_szColum[eExceptDay] = LoadStringById(IDS_PLANPERIOD_LIST001);

	m_lcExceptDay.InsertColumn(eCheck    , m_szColum[eCheck    ], LVCFMT_CENTER, 22);
	m_lcExceptDay.InsertColumn(eExceptDay, m_szColum[eExceptDay], LVCFMT_CENTER, 140);

	// 제일 마지막에 할것...
	m_lcExceptDay.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	m_lcExceptDay.SetSortEnable(true);
}

void CPlanPeriodSetting::RefreshExceptDayList()
{
	m_lcExceptDay.DeleteAllItems();

	CString strExceptDayList = GetBroadcastPlanInfo()->strExceptDay;

	int nRow = 0;
	int nLoopPos=0;
	CString strData;
	while( !(strData = strExceptDayList.Tokenize(_T(","), nLoopPos)).IsEmpty() )
	{
		strData.Replace(_T("."), _T("/"));

		m_lcExceptDay.InsertItem(nRow, _T(""));
		m_lcExceptDay.SetItemText(nRow, eExceptDay, strData);

		nRow++;
	}
}

void CPlanPeriodSetting::InitViewList()
{
	CTime tmStart;
	m_dtStart.GetTime(tmStart);

	m_lcView.SetExtendedStyle(m_lcView.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lcView.InsertColumn(0, _T(""), LVCFMT_LEFT, 0);
	m_lcView.InsertColumn(1, _T(""), LVCFMT_LEFT, 500);

	// 제일 마지막에 할것...
	m_lcView.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcView.SetSortEnable(false);
	m_lcView.SetColType(1
					  , CUTBHeaderCtrl::COL_CALENDAR
					  , CTime(tmStart.GetYear(), tmStart.GetMonth(), tmStart.GetDay(), 0, 0, 0)
					  , 3
					  );

	m_lcView.InsertItem(0, _T(""));
	m_lcView.SetItemText(0,1, _T(""));
}

void CPlanPeriodSetting::RefreshViewList()
{
	UpdateInfo();

	CString strDateList;
	strDateList.Format(_T("DateList=%s~%s,|WeekInfo=%s|ExceptDay=%s|")
					 , GetBroadcastPlanInfo()->tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S"))
					 , GetBroadcastPlanInfo()->tmEndDate.Format(_T("%Y/%m/%d %H:%M:%S"))
					 , GetBroadcastPlanInfo()->strWeekInfo
					 , GetBroadcastPlanInfo()->strExceptDay
					 );

	m_lcView.SetColType(1
					  , CUTBHeaderCtrl::COL_CALENDAR
					  , CTime(GetBroadcastPlanInfo()->tmStartDate.GetYear(), GetBroadcastPlanInfo()->tmStartDate.GetMonth(), GetBroadcastPlanInfo()->tmStartDate.GetDay(), 0, 0, 0)
					  , 3
					  );

	m_lcView.SetItemText(0,1, strDateList);
}

void CPlanPeriodSetting::OnBnClickedBnExceptdayAdd()
{
	CTime tmSelDate;
	m_calExceptDay.GetCurSel(tmSelDate);

	for(int i=0; i<m_lcExceptDay.GetItemCount() ;i++)
	{
		if(m_lcExceptDay.GetItemText(i, eExceptDay) == tmSelDate.Format(_T("%m/%d")))
		{
			return;
		}
	}

	int nRow = m_lcExceptDay.GetItemCount();
	m_lcExceptDay.InsertItem(nRow, _T(""));
	m_lcExceptDay.SetItemText(nRow, eExceptDay, tmSelDate.Format(_T("%m/%d")));
	
	RefreshViewList();
}

void CPlanPeriodSetting::OnBnClickedBnExceptdayDel()
{
	for(int i=m_lcExceptDay.GetItemCount()-1; i>=0 ;i--)
	{
		if(m_lcExceptDay.GetCheck(i))
		{
			m_lcExceptDay.DeleteItem(i);
		}
	}

	RefreshViewList();
}

void CPlanPeriodSetting::OnDtnDatetimechangeDtStart(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	*pResult = 0;

	RefreshViewList();
}

void CPlanPeriodSetting::OnDtnDatetimechangeDtEnd(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	*pResult = 0;

	RefreshViewList();
}

void CPlanPeriodSetting::OnBnClickedKbWeek(UINT nID)
{
	RefreshViewList();
}

void CPlanPeriodSetting::OnNMDblclkLcExceptday(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	if(pNMLV->iItem < 0) return;
	*pResult = 0;

	m_lcExceptDay.DeleteItem(pNMLV->iItem);
}

void CPlanPeriodSetting::OnBnClickedKbImmediate()
{
	if(m_kbImmediate.GetCheck())
	{
		CTime tmCur = CTime::GetCurrentTime();
		m_dtDownloadTime.SetTime(&tmCur);
	}

	m_dtDownloadTime.EnableWindow(!m_kbImmediate.GetCheck());
}
