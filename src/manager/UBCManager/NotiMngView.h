#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "Enviroment.h"
#include "common\HoverButton.h"
//#include "common\utblistctrlex.h"
#include "common\utblistctrl.h"
#include "common\reposcontrol.h"
#include "ubccopcommon\eventhandler.h"

// CNotiMngView form view
class CNotiMngView : public CFormView
{
	DECLARE_DYNCREATE(CNotiMngView)
public:
	enum { eCheck // 2010.02.17 by gwangsoo
		 , eStartTime, eEndTime, /*eSiteId, eAnnoId,*/ eTitle, eCreator, eHostList, eComment, eCreateTime, eEnd };
private:
	void InitList();
	void SetList();
	void CreateEvent(cciAttributeList&);
	void RemoveEvent(cciAttributeList&);
	void ChangeEvent(CString szAnnoId, cciAttributeList&);
	void ExpiredEvent(CString szAnnoId, bool bStart);

//	virtual int InvokeEvent(WPARAM, LPARAM);
	LRESULT InvokeEvent(WPARAM, LPARAM);

	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
public:
private:
//	CUTBListCtrlEx m_lscNoti;
	CUTBListCtrl m_lscNoti;
	CHoverButton m_btAdd;
	CHoverButton m_btDel;
	CHoverButton m_btMod;
	CHoverButton m_btRef;
	AnnounceInfoList m_lsAnnounce;

	bool m_bInit;
	CRect m_rcClient;
	CString m_ColumTitle[eEnd];
	CReposControl m_Reposition;

//	CList<int> m_lsEvID;
//	CEventManager m_EventManager;
//	static UINT AddEventThread(LPVOID pParam);

protected:
	CNotiMngView();           // protected constructor used by dynamic creation
	virtual ~CNotiMngView();

public:
	enum { IDD = IDD_NOTIMNGVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	// 0001375: 긴급공지 방식을 현재의 Event 방식에서 INI download 방식으로 변경한다.
	BOOL	SaveFile(CString szPath, SAnnounceInfo info);

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	void OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult);
	void OnNMCustomdrawList(NMHDR *pNMHDR, LRESULT *pResult);
	void OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult);
	void OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult);
	
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
	afx_msg void OnBnClickedButtonMod();
	afx_msg void OnBnClickedButtonRef();

	bool	PutFile(SAnnounceInfo& info);
};


