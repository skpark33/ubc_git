// PrimaryLogFrame.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "PrimaryLogFrame.h"


// CPrimaryLogFrame

IMPLEMENT_DYNCREATE(CPrimaryLogFrame, CMDIChildWnd)

CPrimaryLogFrame::CPrimaryLogFrame()
{
}

CPrimaryLogFrame::~CPrimaryLogFrame()
{
}


BEGIN_MESSAGE_MAP(CPrimaryLogFrame, CMDIChildWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CPrimaryLogFrame message handlers

int CPrimaryLogFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDI_PACKAGE), false);

	return 0;
}
