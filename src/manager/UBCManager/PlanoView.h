#pragma once
#include "afxwin.h"
#include "common\hoverbutton.h"
#include "afxcmn.h"
#include "ximage.h"
#include "PlanoControl.h"
#include "UBCCopCommon\CopModule.h"
#include "ubccopcommon\eventhandler.h"
#include "common\utblistctrlex.h"


// CPlanoView ƁE���Դϴ�.

class CPlanoView : public CFormView
{
	DECLARE_DYNCREATE(CPlanoView)

protected:
	CPlanoView();           // ����E����⿡ �翁EǴ� protected �������Դϴ�.
	virtual ~CPlanoView();

public:
	enum { IDD = IDD_PLANOVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV �����Դϴ�.

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_cbOperation;
	CComboBox m_cbAdmin;
	CComboBox m_cbPlanoId;
	CHoverButton m_btnRefHost;
	CHoverButton m_btnShutdown;
	CHoverButton m_btnStartup;
	CHoverButton m_btnPowerMng;
	CHoverButton m_btnSave;
	CHoverButton m_btnCancel;
	CSliderCtrl m_sliderBright;

	CButton m_kbSelectAll;
	CButton m_kbShowName;

	CButton m_radioViewMode;
	CButton m_radioEditMode;
	CStatic m_stBright;
	CHoverButton m_btnSetBright;

	PlanoInfoList m_planoList;
	CPlanoControl m_picCtrl;

	LRESULT InvokeEvent(WPARAM wParam, LPARAM lParam);
	int ChangeOPStat(SOpStat* pOpStat);

	bool	IsShowInfoMode() { return m_kbShowName.GetCheck(); }

	afx_msg void OnBnClickedRadioViewmode();
	afx_msg void OnBnClickedRadioEditmode();

	afx_msg void OnBnClickedButtonHostRef();

	virtual void OnInitialUpdate();
	virtual void OnDraw(CDC* /*pDC*/);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void Clear();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonCancel();
	afx_msg void OnBnClickedCheckShowname();
	afx_msg void OnBnClickedCheckSelectall();
	afx_msg void OnBnClickedButtonPoweron();
	afx_msg void OnBnClickedButtonPoweroff();
	afx_msg void OnBnClickedButtonPowerset();
	afx_msg void OnBnClickedButtonSetbright();

	//CButton m_kbShowList;
	//CUTBListCtrlEx m_lscNode;
	//CImageList m_ilHostList;

	//afx_msg void OnBnClickedCheckShowlist();
};


