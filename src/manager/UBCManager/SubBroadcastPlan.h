#pragma once
#include "afxwin.h"
#include "Enviroment.h"

class CSubBroadcastPlan : public CDialog
{
	DECLARE_DYNAMIC(CSubBroadcastPlan)

public:
	CSubBroadcastPlan(UINT nIDTemplate, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSubBroadcastPlan();

	SBroadcastingPlanInfo*	GetBroadcastPlanInfo();
	void					SetBroadcastPlanInfo(SBroadcastingPlanInfo* pBroadcastingPlanInfo);

	TimePlanInfoList*		GetTimePlanInfoList();
	void					SetTimePlanInfoList(TimePlanInfoList* pTimePlanInfoList);

	TargetHostInfoList*		GetTargetHostInfoList();
	void					SetTargetHostInfoList(TargetHostInfoList* pTargetHostInfoList);

	virtual bool IsModified() = 0;
	virtual void UpdateInfo() = 0;
	virtual bool InputErrorCheck(bool bMsg) = 0;
	virtual void RefreshInfo() = 0;

private:
	SBroadcastingPlanInfo*	m_pBroadcastingPlanInfo;
	TimePlanInfoList*		m_pTimePlanInfoList;
	TargetHostInfoList*		m_pTargetHostInfoList;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
};
