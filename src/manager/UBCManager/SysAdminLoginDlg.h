#pragma once
#include "afxwin.h"


// CSysAdminLoginDlg 대화 상자입니다.

class CSysAdminLoginDlg : public CDialog
{
	DECLARE_DYNAMIC(CSysAdminLoginDlg)

public:
	CSysAdminLoginDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSysAdminLoginDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SYS_ADMIN_LOGIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	virtual void OnOK();
public:
	CEdit m_editID;
	CEdit m_editPasswd;

	CString	m_userId;
};
