#pragma once

#include "ubccopcommon\eventhandler.h"


// CNotiMngFrm frame

class CNotiMngFrm : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CNotiMngFrm)
protected:
	CNotiMngFrm();           // protected constructor used by dynamic creation
	virtual ~CNotiMngFrm();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
