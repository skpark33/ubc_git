// HostMultiModify.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HostMultiModify.h"
#include "common\UbcCode.h"
#include "ubccopcommon\ubccopcommon.h"


// CHostMultiModify 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHostMultiModify, CDialog)

CHostMultiModify::CHostMultiModify(CWnd* pParent /*=NULL*/)
	: CDialog(CHostMultiModify::IDD, pParent)
{
	m_bInit=false;
	m_nLogLimit=0; // 로그보존기간 (1~356사이의 값을 갖는다)
	m_nPeriod2=0; // 스크린샷주기 (120~3600)
	m_bAdminState=true;
	m_bMute=true;
	m_bAutoUpdate=true;
}

CHostMultiModify::~CHostMultiModify()
{
}

void CHostMultiModify::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_HOSTTYPE, m_check_hostType);
	DDX_Control(pDX, IDC_CHECK_VENDOR, m_check_vendor);
	DDX_Control(pDX, IDC_CHECK_MODEL, m_check_model);
	DDX_Control(pDX, IDC_CHECK_ADDR1, m_check_addr1);
	DDX_Control(pDX, IDC_CHECK_DESCRIPTION, m_check_description);
	DDX_Control(pDX, IDC_CHECK_MONITORTYPE, m_check_monitorType);
	DDX_Control(pDX, IDC_CHECK_VIDEORENDOR, m_check_videoRendor);
	DDX_Control(pDX, IDC_CHECK_DAY_BASE_TIME, m_check_dayCriteria);
	DDX_Control(pDX, IDC_CHECK_LOGLIMIT, m_check_logLimit);
	DDX_Control(pDX, IDC_CHECK_PERIOD2, m_check_period2);
	DDX_Control(pDX, IDC_CHECK_SOUNDVOLUME, m_check_soundVolumeCheck);
	DDX_Control(pDX, IDC_CHECK_MUTECHECK, m_check_muteCheck);
	DDX_Control(pDX, IDC_CHECK_AUTOUPDATECHECK, m_check_autoupdateCheck);
	DDX_Control(pDX, IDC_CHECK_DOWNLOADTIMECHECK, m_check_downloadTimeCheck);
	DDX_Control(pDX, IDC_CHECK_ADMINSTATE, m_check_adminState);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_cbType);
	DDX_Control(pDX, IDC_COMBO_MONITORTYPE, m_cbMonitorType);
	DDX_Control(pDX, IDC_COMBO_VIDEO_RENDERING, m_cbVideoRendering);
	DDX_Control(pDX, IDC_SPIN_LOGLIMIT, m_spLogLimit);
	DDX_Control(pDX, IDC_SPIN2, m_spPeriod2);
	DDX_Control(pDX, IDC_SLIDER_VOLUME, m_slcVolume);
	DDX_Control(pDX, IDC_BUTTON_USTATE, m_btUState);
	DDX_Control(pDX, IDC_DT_DOWNLOADTIME, m_dtcDownloadTime);
	DDX_Control(pDX, IDC_CHECK_DOWNLOADTIME, m_check_downloadTime);
	DDX_Control(pDX, IDC_EDIT_VENDOR, m_edit_vendor);
	DDX_Control(pDX, IDC_EDIT_MODEL, m_edit_model);
	DDX_Control(pDX, IDC_EDIT_LOCATION, m_edit_addr1);
	DDX_Control(pDX, IDC_EDIT_DESC, m_edit_description);
	DDX_Control(pDX, IDC_DT_DAY_BASE_TIME, m_date_dayCriteria);
	DDX_Control(pDX, IDC_EDIT_LOGLIMIT, m_edit_logLimt);
	DDX_Control(pDX, IDC_EDIT_PERIOD2, m_edit_period2);
	DDX_Control(pDX, IDC_BUTTON_MUTE, m_bt_mute);
	DDX_Control(pDX, IDC_BUTTON_AUTOUPDATE, m_bt_autoUpdate);
	DDX_Control(pDX, IDC_EDIT_CACHE_SERVER_IP, m_editCacheServer);
	DDX_Control(pDX, IDC_CHECK_CACHE_SERVER_IP, m_check_cacheServer);
}


BEGIN_MESSAGE_MAP(CHostMultiModify, CDialog)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDOK, &CHostMultiModify::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CHostMultiModify::OnBnClickedCancel)

	ON_BN_CLICKED(IDC_BUTTON_USTATE, &CHostMultiModify::OnBnClickedButtonUstate)
	ON_EN_KILLFOCUS(IDC_EDIT_PERIOD2, &CHostMultiModify::OnEnKillfocusEditPeriod2)
	ON_EN_KILLFOCUS(IDC_EDIT_LOGLIMIT, &CHostMultiModify::OnEnKillfocusEditLoglimit)
	ON_BN_CLICKED(IDC_CHECK_DOWNLOADTIME, &CHostMultiModify::OnBnClickedCheckDownloadtime)

	ON_BN_CLICKED(IDC_CHECK_HOSTTYPE, &CHostMultiModify::OnBnClickedCheckHosttype)
	ON_BN_CLICKED(IDC_CHECK_VENDOR, &CHostMultiModify::OnBnClickedCheckVendor)
	ON_BN_CLICKED(IDC_CHECK_MODEL, &CHostMultiModify::OnBnClickedCheckModel)
	ON_BN_CLICKED(IDC_CHECK_ADDR1, &CHostMultiModify::OnBnClickedCheckAddr1)
	ON_BN_CLICKED(IDC_CHECK_DESCRIPTION, &CHostMultiModify::OnBnClickedCheckDescription)
	ON_BN_CLICKED(IDC_CHECK_MONITORTYPE, &CHostMultiModify::OnBnClickedCheckMonitortype)
	ON_BN_CLICKED(IDC_CHECK_VIDEORENDOR, &CHostMultiModify::OnBnClickedCheckVideorendor)
	ON_BN_CLICKED(IDC_CHECK_DAY_BASE_TIME, &CHostMultiModify::OnBnClickedCheckDayBaseTime)
	ON_BN_CLICKED(IDC_CHECK_LOGLIMIT, &CHostMultiModify::OnBnClickedCheckLoglimit)
	ON_BN_CLICKED(IDC_CHECK_PERIOD2, &CHostMultiModify::OnBnClickedCheckPeriod2)
	ON_BN_CLICKED(IDC_CHECK_SOUNDVOLUME, &CHostMultiModify::OnBnClickedCheckSoundvolume)
	ON_BN_CLICKED(IDC_CHECK_MUTECHECK, &CHostMultiModify::OnBnClickedCheckMutecheck)
	ON_BN_CLICKED(IDC_CHECK_AUTOUPDATECHECK, &CHostMultiModify::OnBnClickedCheckAutoupdatecheck)
	ON_BN_CLICKED(IDC_CHECK_DOWNLOADTIMECHECK, &CHostMultiModify::OnBnClickedCheckDownloadtimecheck)
	ON_BN_CLICKED(IDC_CHECK_ADMINSTATE, &CHostMultiModify::OnBnClickedCheckAdminstate)
	ON_BN_CLICKED(IDC_BUTTON_MUTE, &CHostMultiModify::OnBnClickedButtonMute)
	ON_BN_CLICKED(IDC_BUTTON_AUTOUPDATE, &CHostMultiModify::OnBnClickedButtonAutoupdate)
	ON_BN_CLICKED(IDC_CHECK_CACHE_SERVER_IP, &CHostMultiModify::OnBnClickedCheckCacheServerIp)
END_MESSAGE_MAP()

void CHostMultiModify::OnDestroy()
{
	CDialog::OnDestroy();
	m_bInit = false;
}

void CHostMultiModify::OnClose()
{
	m_bInit = false;
	CDialog::OnClose();
}

BOOL CHostMultiModify::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_bInit = true;

	CUbcCode::GetInstance()->FillComboBoxCtrl(m_cbType, _T("HostType"));
	//m_cbType.SetCurSel(CUbcCode::GetInstance()->FindComboBoxCtrl(m_cbType, m_HostInfo.hostType));

	CUbcCode::GetInstance()->FillComboBoxCtrl(m_cbMonitorType, _T("MonitorType"));
	//m_cbMonitorType.SetCurSel(CUbcCode::GetInstance()->FindComboBoxCtrl(m_cbMonitorType, m_HostInfo.monitorType));

	m_cbVideoRendering.ResetContent();
	m_cbVideoRendering.AddString(_T("Overlay"));
	m_cbVideoRendering.AddString(_T("VMR Windowless"));
	m_cbVideoRendering.AddString(_T("EVR"));

	m_spLogLimit.SetRange32(1, 365);
	m_spPeriod2.SetRange32(120, 3600);

	m_slcVolume.SetRange(0, 100);
	m_btUState.LoadBitmap(IDB_BUTTON_USE, RGB(12,12,12));
	m_bt_mute.LoadBitmap(IDB_BUTTON_MUTE, RGB(12,12,12));
	m_bt_autoUpdate.LoadBitmap(IDB_BUTTON_AUTOUPDATE, RGB(12,12,12));

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}



// CHostMultiModify 메시지 처리기입니다.

void CHostMultiModify::OnBnClickedOk()
{
	CString debugStr;

	if(BOOL(this->m_check_hostType.GetCheck())) {
		m_info.hostType = (long)m_cbType.GetItemData(m_cbType.GetCurSel());
		m_infoDirty.SetDirty("hostType", true);
		char debugBuf[100];
		sprintf(debugBuf,"hostType=%d\n", m_info.hostType);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_vendor.GetCheck())) {
		this->m_edit_vendor.GetWindowText(m_info.vendor);
		m_infoDirty.SetDirty("vendor", true);
		char debugBuf[100];
		sprintf(debugBuf,"vendor=%s\n", m_info.vendor);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_model.GetCheck())) {
		this->m_edit_model.GetWindowText(m_info.model);
		m_infoDirty.SetDirty("model", true);
		char debugBuf[100];
		sprintf(debugBuf,"model=%s\n", m_info.model);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_cacheServer.GetCheck())) {
		this->m_editCacheServer.GetWindowText(m_info.domainName);
		m_infoDirty.SetDirty("domainName", true);
		char debugBuf[100];
		sprintf(debugBuf,"domainName=%s\n", m_info.domainName);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_addr1.GetCheck())) {
		this->m_edit_addr1.GetWindowText(m_info.location);
		m_infoDirty.SetDirty("addr1", true);
		char debugBuf[100];
		sprintf(debugBuf,"location=%s\n", m_info.location);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_description.GetCheck())) {
		this->m_edit_description.GetWindowText(m_info.description);
		m_infoDirty.SetDirty("description", true);
		char debugBuf[100];
		sprintf(debugBuf,"description=%s\n", m_info.description);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_monitorType.GetCheck())) {
		m_info.monitorType = (long)this->m_cbMonitorType.GetItemData(m_cbMonitorType.GetCurSel());
		m_infoDirty.SetDirty("monitorType", true);
		char debugBuf[100];
		sprintf(debugBuf,"monitorType=%d\n", m_info.monitorType);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_videoRendor.GetCheck())) {
		m_info.renderMode = (long)m_cbVideoRendering.GetCurSel()+1;
		m_infoDirty.SetDirty("renderMode", true);
		char debugBuf[100];
		sprintf(debugBuf,"renderMode=%d\n", m_info.renderMode);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_dayCriteria.GetCheck())) {
		CTime tmTemp;
		this->m_date_dayCriteria.GetTime(tmTemp);
		m_info.playLogDayCriteria = tmTemp.Format("%H:%M");		
		m_infoDirty.SetDirty("playLogDayCriteria", true);
		char debugBuf[100];
		sprintf(debugBuf,"playLogDayCriteria=%s\n", m_info.playLogDayCriteria);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_logLimit.GetCheck())) {
		CString buf;
		this->m_edit_logLimt.GetWindowText(buf);
		m_info.playLogDayLimit = (short)atoi(buf.GetBuffer());
		m_infoDirty.SetDirty("playLogDayLimit", true);
		char debugBuf[100];
		sprintf(debugBuf,"playLogDayLimit=%d\n", m_info.playLogDayLimit);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_period2.GetCheck())) {
		CString buf;
		this->m_edit_period2.GetWindowText(buf);
		m_info.screenshotPeriod = (short)atoi(buf.GetBuffer());
		m_infoDirty.SetDirty("screenshotPeriod", true);
		char debugBuf[100];
		sprintf(debugBuf,"screenshotPeriod=%d\n", m_info.screenshotPeriod);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_soundVolumeCheck.GetCheck())) {
		m_info.soundVolume = this->m_slcVolume.GetPos();
		m_infoDirty.SetDirty("soundVolume", true);
		char debugBuf[100];
		sprintf(debugBuf,"soundVolume=%d\n", m_info.soundVolume);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_muteCheck.GetCheck())) {
		m_info.mute = this->m_bMute;
		m_infoDirty.SetDirty("mute", true);
		char debugBuf[100];
		sprintf(debugBuf,"mute=%d\n", m_info.mute);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_autoupdateCheck.GetCheck())) {
		m_info.autoUpdateFlag = this->m_bAutoUpdate;
		m_infoDirty.SetDirty("autoUpdateFlag", true);
		char debugBuf[100];
		sprintf(debugBuf,"autoUpdateFlag=%d\n", m_info.autoUpdateFlag);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_downloadTimeCheck.GetCheck())) {
		if(this->m_check_downloadTime.GetCheck()) {
			CTime tmTemp;
			this->m_dtcDownloadTime.GetTime(tmTemp);
			m_info.contentsDownloadTime = tmTemp.Format("%H:%M");
		}else{
			m_info.contentsDownloadTime = "";
		}
		m_infoDirty.SetDirty("contentsDownloadTime", true);
		char debugBuf[100];
		sprintf(debugBuf,"contentsDownloadTime=%s\n", m_info.contentsDownloadTime);
		debugStr.Append(debugBuf);
	}
	if(BOOL(this->m_check_adminState.GetCheck())) {
		m_info.adminState = this->m_bAdminState;
		m_infoDirty.SetDirty("adminState", true);
		char debugBuf[100];
		sprintf(debugBuf,"adminState=%d\n", m_info.adminState);
		debugStr.Append(debugBuf);
	}

	if(!debugStr.IsEmpty()){
		//UbcMessageBox(debugStr, MB_ICONINFORMATION);
		OnOK();	
		return;
	}
	OnCancel();
}

void CHostMultiModify::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CHostMultiModify::OnBnClickedButtonUstate()
{
	if(m_bAdminState == false){
		m_btUState.LoadBitmap(IDB_BUTTON_USE, RGB(12,12,12));
		m_bAdminState = true;
	}else{
		m_btUState.LoadBitmap(IDB_BUTTON_NOTUSE, RGB(12,12,12));
		m_bAdminState = false;
	}
}
void CHostMultiModify::OnBnClickedButtonMute()
{
	if(m_bMute == false){
		m_bt_mute.LoadBitmap(IDB_BUTTON_MUTE, RGB(12,12,12));
		m_bMute = true;
	}else{
		m_bt_mute.LoadBitmap(IDB_BUTTON_NOTMUTE, RGB(12,12,12));
		m_bMute = false;
	}
}

void CHostMultiModify::OnBnClickedButtonAutoupdate()
{
	if(m_bAutoUpdate == false){
		m_bt_autoUpdate.LoadBitmap(IDB_BUTTON_AUTOUPDATE, RGB(12,12,12));
		m_bAutoUpdate = true;
	}else{
		m_bt_autoUpdate.LoadBitmap(IDB_BUTTON_NOTAUTOUPDATE, RGB(12,12,12));
		m_bAutoUpdate = false;
	}
}

void CHostMultiModify::OnEnKillfocusEditPeriod2()
{
	if(!m_bInit)	return;

	UpdateData(TRUE);
	
	CString strText;
	GetDlgItemText(IDC_EDIT_PERIOD2, strText);
	if(strText.IsEmpty())
	{
		m_nPeriod2 = 120;
	}
	else
	{
		m_nPeriod2 = GetValueRange(m_nPeriod2, 120, 3600);
	}

	UpdateData(FALSE);
}

void CHostMultiModify::OnEnKillfocusEditLoglimit()
{
	if(!m_bInit)	return;

	UpdateData(TRUE);

	CString strText;
	GetDlgItemText(IDC_EDIT_LOGLIMIT, strText);
	if(strText.IsEmpty())
	{
		m_nLogLimit = 15;
	}
	else
	{
		m_nLogLimit = GetValueRange(m_nLogLimit, 1, 365);
	}

	UpdateData(FALSE);
}

void CHostMultiModify::OnBnClickedCheckDownloadtime()
{
	this->m_dtcDownloadTime.EnableWindow(BOOL(this->m_check_downloadTime.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckHosttype()
{
	this->m_cbType.EnableWindow(BOOL(this->m_check_hostType.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckVendor()
{
	this->m_edit_vendor.EnableWindow(BOOL(this->m_check_vendor.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckModel()
{
	this->m_edit_model.EnableWindow(BOOL(this->m_check_model.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckAddr1()
{
	this->m_edit_addr1.EnableWindow(BOOL(this->m_check_addr1.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckDescription()
{
	this->m_edit_description.EnableWindow(BOOL(this->m_check_description.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckMonitortype()
{
	this->m_cbMonitorType.EnableWindow(BOOL(this->m_check_monitorType.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckVideorendor()
{
	this->m_cbVideoRendering.EnableWindow(BOOL(this->m_check_videoRendor.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckDayBaseTime()
{
	this->m_date_dayCriteria.EnableWindow(BOOL(this->m_check_dayCriteria.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckLoglimit()
{
	this->m_edit_logLimt.EnableWindow(BOOL(this->m_check_logLimit.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckPeriod2()
{
	this->m_edit_period2.EnableWindow(BOOL(this->m_check_period2.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckSoundvolume()
{
	this->m_slcVolume.EnableWindow(BOOL(this->m_check_soundVolumeCheck.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckMutecheck()
{
	this->m_bt_mute.EnableWindow(BOOL(this->m_check_muteCheck.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckAutoupdatecheck()
{
	this->m_bt_autoUpdate.EnableWindow(BOOL(this->m_check_autoupdateCheck.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckDownloadtimecheck()
{
//	this->m_dtcDownloadTime.EnableWindow(BOOL(this->m_check_hostType.GetCheck()));
	this->m_check_downloadTime.EnableWindow(BOOL(this->m_check_downloadTimeCheck.GetCheck()));
}

void CHostMultiModify::OnBnClickedCheckAdminstate()
{
	this->m_btUState.EnableWindow(BOOL(this->m_check_adminState.GetCheck()));
}



void CHostMultiModify::OnBnClickedCheckCacheServerIp()
{
	this->m_editCacheServer.EnableWindow(BOOL(this->m_check_cacheServer.GetCheck()));
}
