// HollidayDlg.cpp : implementation file
//
#include "stdafx.h"
#include "UBCManager.h"
#include "HollidayDlg.h"
#include "common\ubcdefine.h"
#include "PowerSettingByDay.h"

// CHollidayDlg dialog
IMPLEMENT_DYNAMIC(CHollidayDlg, CDialog)

CHollidayDlg::CHollidayDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHollidayDlg::IDD, pParent)
{
	m_strShutdownTime.Empty();
	m_strPoweronTime.Empty();
	m_bShowMonitor= true;
	m_bShowPoweron= false;
}

CHollidayDlg::~CHollidayDlg()
{
}

void CHollidayDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN, m_dtShutdown);
	DDX_Control(pDX, IDC_TIME_POWERON, m_dtPoweron);
	DDX_Control(pDX, IDC_CHECK_SHUTDOWN, m_ckShutdown);
	DDX_Control(pDX, IDC_CHECK_POWERON, m_ckPoweron);

	DDX_Control(pDX, IDC_CHECK_WEEK0, m_ckWeek[0]);
	DDX_Control(pDX, IDC_CHECK_WEEK1, m_ckWeek[1]);
	DDX_Control(pDX, IDC_CHECK_WEEK2, m_ckWeek[2]);
	DDX_Control(pDX, IDC_CHECK_WEEK3, m_ckWeek[3]);
	DDX_Control(pDX, IDC_CHECK_WEEK4, m_ckWeek[4]);
	DDX_Control(pDX, IDC_CHECK_WEEK5, m_ckWeek[5]);
	DDX_Control(pDX, IDC_CHECK_WEEK6, m_ckWeek[6]);
	DDX_Control(pDX, IDC_LIST_HOLIDAY, m_lscHoliday);
	DDX_Control(pDX, IDC_MONTHCALENDAR2, m_ctrCalendar);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_bnAddHoliday);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_bnDelHoliday);
	DDX_Control(pDX, IDC_KB_USE_MONITOR_OFF_TIME, m_ckUseMonitorOffTime);
	DDX_Control(pDX, IDC_TM_MONITOR_OFF, m_dtMonitorOff);
	DDX_Control(pDX, IDC_TM_MONITOR_ON, m_dtMonitorOn);
	DDX_Control(pDX, IDC_BN_ADD_MONITOR_OFF_TIME, m_bnAddMonitorOffTime);
	DDX_Control(pDX, IDC_BN_DEL_MONITOR_OFF_TIME, m_bnDelMonitorOffTime);
	DDX_Control(pDX, IDC_LIST_MONITOR_OFF_TIME, m_lscMonitorOffTime);
	DDX_Control(pDX, IDC_STATIC_MONITOR, m_staticMonitorGroup);
	DDX_Control(pDX, IDOK, m_btOK);
	DDX_Control(pDX, IDCANCEL, m_btCancel);
}

BEGIN_MESSAGE_MAP(CHollidayDlg, CDialog)
	ON_BN_CLICKED(IDC_CHECK_SHUTDOWN, OnBnClickedCheckShutdown)
	ON_BN_CLICKED(IDC_CHECK_POWERON, OnBnClickedCheckPoweron)
	ON_BN_CLICKED(IDOK, &CHollidayDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CHollidayDlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CHollidayDlg::OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_KB_USE_MONITOR_OFF_TIME, &CHollidayDlg::OnBnClickedKbUseMonitorOffTime)
	ON_BN_CLICKED(IDC_BN_ADD_MONITOR_OFF_TIME, &CHollidayDlg::OnBnClickedBnAddMonitorOffTime)
	ON_BN_CLICKED(IDC_BN_DEL_MONITOR_OFF_TIME, &CHollidayDlg::OnBnClickedBnDelMonitorOffTime)
	ON_BN_CLICKED(IDC_BN_POWER_SET_BY_DAY, &CHollidayDlg::OnBnClickedBnPowerSetByDay)
END_MESSAGE_MAP()

BOOL CHollidayDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bnAddHoliday.LoadBitmap(IDB_BUTTON_NEXT2, RGB(255,255,255));
	m_bnDelHoliday.LoadBitmap(IDB_BUTTON_PREV2, RGB(255,255,255));

	m_lscHoliday.SetExtendedStyle(m_lscHoliday.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lscHoliday.InsertColumn(0, LoadStringById(IDS_HOLLIDAYDLG_LST001), LVCFMT_CENTER, 120);
	m_lscHoliday.InitHeader(IDB_LIST_HEADER);

	m_dtShutdown.SetFormat(" HH : mm");
	m_dtPoweron.SetFormat(" HH : mm");

	if(m_bShowPoweron){
		m_ckPoweron.ShowWindow(SW_SHOW);
		m_dtPoweron.ShowWindow(SW_SHOW);
		m_ckPoweron.EnableWindow(true);
	}

	InitHolidayInfo();

	if(m_bShowMonitor) {

		// 모니터 절전 기능 관련
		m_dtMonitorOff.SetFormat(" HH : mm");
		m_dtMonitorOn.SetFormat(" HH : mm");
		m_bnAddMonitorOffTime.LoadBitmap(IDB_BUTTON_NEXT2, RGB(255,255,255));
		m_bnDelMonitorOffTime.LoadBitmap(IDB_BUTTON_PREV2, RGB(255,255,255));

		m_lscMonitorOffTime.SetExtendedStyle(m_lscMonitorOffTime.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
		m_lscMonitorOffTime.InsertColumn(0, LoadStringById(IDS_HOLLIDAYDLG_LST002), LVCFMT_CENTER, 120);
		m_lscMonitorOffTime.InitHeader(IDB_LIST_HEADER);

		InitMonitorOffTimeInfo();
	}else{

		m_ckUseMonitorOffTime.ShowWindow(SW_HIDE);
		m_dtMonitorOff.ShowWindow(SW_HIDE);
		m_dtMonitorOn.ShowWindow(SW_HIDE);
		m_bnAddMonitorOffTime.ShowWindow(SW_HIDE);
		m_bnDelMonitorOffTime.ShowWindow(SW_HIDE);
		m_lscMonitorOffTime.ShowWindow(SW_HIDE);
		m_staticMonitorGroup.ShowWindow(SW_HIDE);

		CRect parent_rc;
		this->m_pParentWnd->GetWindowRect(parent_rc);
		//this->m_pParentWnd->ClientToScreen(&parent_rc);

			
		CRect rc;
		GetWindowRect(rc);
		ClientToScreen(&rc);

		TraceLog(("parent = %d,%d, child = %d,%d", 
			parent_rc.left, parent_rc.top, rc.left, rc.top));

			//SetWindowPos(NULL,rc.left,rc.top,rc.Width(),rc.Height()-180,SWP_SHOWWINDOW);
		SetWindowPos(NULL,parent_rc.left+100,
			                           parent_rc.top+100,
									   rc.Width(),rc.Height()-180,SWP_SHOWWINDOW);
		//SetWindowPos(NULL,300,300,rc.Width(),rc.Height()-180,SWP_SHOWWINDOW);

		CRect ok_rc, cancel_rc;
		m_btOK.GetWindowRect(ok_rc);
		ScreenToClient(&ok_rc);
		//TraceLog(("dlg: %d,%d, ok:%d,%d", rc.left, rc.top, ok_rc.left, ok_rc.top));
		m_btOK.SetWindowPos(NULL,ok_rc.left,ok_rc.top-180,ok_rc.Width(),ok_rc.Height(),SWP_SHOWWINDOW);

		m_btCancel.GetWindowRect(cancel_rc);
		ScreenToClient(&cancel_rc);
		m_btCancel.SetWindowPos(NULL,cancel_rc.left,cancel_rc.top-180,cancel_rc.Width(),cancel_rc.Height(),SWP_SHOWWINDOW);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CHollidayDlg::InitHolidayInfo()
{
	TraceLog(("InitHolidayInfo()"));

	bool bShutdown = false;
	if(!m_strShutdownTime.IsEmpty()){
		TraceLog(("InitHolidayInfo(%s)", m_strShutdownTime));
		int iStart = 0;
		CString szToken = m_strShutdownTime.Tokenize(EH_SEPARATOR, iStart);
		while(!szToken.IsEmpty()){
			if(szToken.Find(":") >= 0){
				int iPos = 0;
				CString szHour = szToken.Tokenize(":", iPos);
				CString szMin = szToken.Tokenize(":", iPos);
				CTime tmShut(2000, 1, 1, atoi(szHour), atoi(szMin), 0);
				m_dtShutdown.SetTime(&tmShut);
				InitDateRange(m_dtShutdown);
				bShutdown = true;
			}
			else if(szToken.Find(".") >=0){
				AddHolidayItem(szToken);
			}
			else if(szToken.GetLength() == 1){
				int n = atoi(szToken);
				if(0 <= n && n <= 6)
					m_ckWeek[n].SetCheck(true);
			}
			szToken = m_strShutdownTime.Tokenize(EH_SEPARATOR, iStart);
		}
	}

	m_ckShutdown.SetCheck(bShutdown);
	if(!bShutdown){
		m_dtShutdown.EnableWindow(FALSE);
	}

	bool bPoweron = false;
	if(!m_strPoweronTime.IsEmpty()){
		TraceLog(("InitHolidayInfo(%s)", m_strPoweronTime));
		if(m_strPoweronTime.Find(":") >= 0){
			int iPos = 0;
			CString szHour = m_strPoweronTime.Tokenize(":", iPos);
			CString szMin = m_strPoweronTime.Tokenize(":", iPos);
			CTime tmShut(2000, 1, 1, atoi(szHour), atoi(szMin), 0);
			m_dtPoweron.SetTime(&tmShut);
			InitDateRange(m_dtPoweron);
			bPoweron = true;
		}
	}
	m_ckPoweron.SetCheck(bPoweron);
	if(!bPoweron){
		m_dtPoweron.EnableWindow(FALSE);
	}
}

void CHollidayDlg::SetPoweron(LPCTSTR szPoweron)
{
	if(szPoweron) {
		m_bPoweron = true;
		m_strPoweronTime = szPoweron;
	}else{
		m_bPoweron = false;
		m_strPoweronTime = "";
	}
	TraceLog(("SetPoweron(%s)", m_strPoweronTime));
}

LPCTSTR CHollidayDlg::GetPoweron(CString& szPoweron)
{
	return (szPoweron = m_strPoweronTime);
}

void CHollidayDlg::SetHolliday(LPCTSTR szHolliday)
{
	if(szHolliday)
	m_strShutdownTime = szHolliday?szHolliday:"";
	TraceLog(("SetHolliday(%s)", m_strShutdownTime));
}

LPCTSTR CHollidayDlg::GetHolliday(CString& szHolliday)
{
	return (szHolliday = m_strShutdownTime);
}

void CHollidayDlg::AddHolidayItem(CString str)
{
	int nRow = m_lscHoliday.GetItemCount();
	m_lscHoliday.InsertItem(nRow, str);
	m_lscHoliday.SortItems(CompareSite, (DWORD_PTR)&m_lscHoliday);
}

int CHollidayDlg::CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	LVFINDINFO lvFind;
	lvFind.flags = LVFI_PARAM;

	CListCtrl* pListCtrl = (CListCtrl*)lParam;
	if(!pListCtrl) return 0;

	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	int nCol = 0;
	CString strItem1 = pListCtrl->GetItemText(nIndex1, nCol);
	CString strItem2 = pListCtrl->GetItemText(nIndex2, nCol);

	return strItem1.Compare(strItem2);
}

void CHollidayDlg::OnBnClickedCheckShutdown()
{
	m_dtShutdown.EnableWindow(m_ckShutdown.GetCheck());
}
void CHollidayDlg::OnBnClickedCheckPoweron()
{
	m_dtPoweron.EnableWindow(m_ckPoweron.GetCheck());
}

void CHollidayDlg::OnBnClickedOk()
{
	m_bShutdown = m_ckShutdown.GetCheck();
	if(m_ckShutdown.GetCheck()){
		m_dtShutdown.GetTime(m_tmShutdown);
		m_strShutdownTime.Format("%02d:%02d", m_tmShutdown.GetHour(), m_tmShutdown.GetMinute());
	}else{
		m_strShutdownTime = "NOSET";
	}

	for(int nRow = 0; nRow < m_lscHoliday.GetItemCount(); nRow++){
		CString szBuf = m_lscHoliday.GetItemText(nRow, 0);
		m_strShutdownTime += EH_SEPARATOR;
		m_strShutdownTime += szBuf;
	}

	for(int i = 0; i < 7; i++){
		if(m_ckWeek[i].GetCheck()){
			m_strShutdownTime += EH_SEPARATOR;
			m_strShutdownTime += ToString(i);
		}
	}

	m_bPoweron = m_ckPoweron.GetCheck();
	if(m_ckPoweron.GetCheck()){
		m_dtPoweron.GetTime(m_tmPoweron);
		m_strPoweronTime.Format("%02d:%02d", m_tmPoweron.GetHour(), m_tmPoweron.GetMinute());
	}else{
		m_strPoweronTime = "NOSET";
	}



	// 모니터 절전 기능 관련
	m_bMonitorOff = m_ckUseMonitorOffTime.GetCheck();
	m_listMonitorOffTime.clear();
	for(int nRow = 0; nRow < m_lscMonitorOffTime.GetItemCount(); nRow++)
	{
		std::string strBuf = m_lscMonitorOffTime.GetItemText(nRow, 0);
		m_listMonitorOffTime.push_back(strBuf);
	}

	OnOK();
}

void CHollidayDlg::OnBnClickedButtonAdd()
{
	SYSTEMTIME st;
	m_ctrCalendar.GetCurSel(&st);

	CString str;
	str.Format("%d.%02d", st.wMonth, st.wDay);

	for(int nRow = 0; nRow < m_lscHoliday.GetItemCount(); nRow++){
		CString szBuf = m_lscHoliday.GetItemText(nRow, 0);
		if(szBuf == str)	return;
	}

	AddHolidayItem(str);
}

void CHollidayDlg::OnBnClickedButtonDel()
{
	POSITION pos = m_lscHoliday.GetFirstSelectedItemPosition();
	while(pos){
		int nRow = m_lscHoliday.GetNextSelectedItem(pos);
		m_lscHoliday.DeleteItem(nRow);
	}
}

// 모니터 절전 기능 관련
void CHollidayDlg::InitMonitorOffTimeInfo()
{
	m_lscMonitorOffTime.DeleteAllItems();

	int nRow=0;
	for(std::list<std::string>::iterator Iter = m_listMonitorOffTime.begin(); Iter != m_listMonitorOffTime.end(); Iter++)
	{
		CString strTime = (*Iter).c_str();
		if(strTime.IsEmpty()) continue;

		m_lscMonitorOffTime.InsertItem(nRow, strTime);
		nRow++;
	}

	EnableMonitorOffInfo(m_bMonitorOff);
}

// 모니터 절전 기능 관련
void CHollidayDlg::EnableMonitorOffInfo(BOOL bUseMonitorOff)
{
	m_ckUseMonitorOffTime.SetCheck(bUseMonitorOff);
	m_dtMonitorOff.EnableWindow(bUseMonitorOff);
	m_dtMonitorOn.EnableWindow(bUseMonitorOff);
	m_bnAddMonitorOffTime.EnableWindow(bUseMonitorOff);
	m_bnDelMonitorOffTime.EnableWindow(bUseMonitorOff);
	m_lscMonitorOffTime.EnableWindow(bUseMonitorOff);
}

// 모니터 절전 기능 관련
void CHollidayDlg::OnBnClickedKbUseMonitorOffTime()
{
	EnableMonitorOffInfo(m_ckUseMonitorOffTime.GetCheck());
}

// 모니터 절전 기능 관련
void CHollidayDlg::OnBnClickedBnAddMonitorOffTime()
{
	COleDateTime tmOff;
	m_dtMonitorOff.GetTime(tmOff);

	COleDateTime tmOn;
	m_dtMonitorOn.GetTime(tmOn);

	// 모니터 켜짐시간과 꺼짐시간의 시간 설정시 간격은 최소 5분이상으로 한다.
	if((tmOff-tmOn).GetTotalMinutes() < 5)
	{
		UbcMessageBox(LoadStringById(IDS_HOLLIDAYDLG_MSG002));
		return;
	}

	CString strInTime;
	strInTime.Format(_T("%s-%s"), tmOn.Format(_T("%H:%M")), tmOff.Format(_T("%H:%M")));

	for(int nRow = 0; nRow < m_lscMonitorOffTime.GetItemCount(); nRow++)
	{
		CString strTime = m_lscMonitorOffTime.GetItemText(nRow, 0);
		if(strInTime == strTime) return;

		CString strOnTime = strTime.Left(5);
		CString strOffTime = strTime.Right(5);

		// 새로 입력하는 시간이 기존 시간에 겹치는지 여부
		if( (strOnTime <= tmOn.Format(_T("%H:%M")) && tmOn.Format(_T("%H:%M")) <= strOffTime) || 
			(strOnTime <= tmOff.Format(_T("%H:%M")) && tmOff.Format(_T("%H:%M")) <= strOffTime) )
		{
			UbcMessageBox(LoadStringById(IDS_HOLLIDAYDLG_MSG001));
			return;
		}

		// 새로 입력하는 시간에 기존시간이 포함되는지 여부
		if( (tmOn.Format(_T("%H:%M")) <= strOnTime && strOnTime <= tmOff.Format(_T("%H:%M"))) || 
			(tmOn.Format(_T("%H:%M")) <= strOffTime && strOffTime <= tmOff.Format(_T("%H:%M"))) )
		{
			UbcMessageBox(LoadStringById(IDS_HOLLIDAYDLG_MSG001));
			return;
		}
	}

	int nRow = m_lscMonitorOffTime.GetItemCount();
	m_lscMonitorOffTime.InsertItem(nRow, strInTime);
	m_lscMonitorOffTime.SortItems(CompareSite, (DWORD_PTR)&m_lscMonitorOffTime);
}

// 모니터 절전 기능 관련
void CHollidayDlg::OnBnClickedBnDelMonitorOffTime()
{
	POSITION pos = m_lscMonitorOffTime.GetFirstSelectedItemPosition();
	while(pos)
	{
		int nRow = m_lscMonitorOffTime.GetNextSelectedItem(pos);
		m_lscMonitorOffTime.DeleteItem(nRow);
	}
}

void CHollidayDlg::OnBnClickedBnPowerSetByDay()
{
	CPowerSettingByDay dlg;
	dlg.m_strWeekShutdownTime = m_strWeekShutdownTime;
	if(dlg.DoModal() != IDOK) return;

	m_strWeekShutdownTime = dlg.m_strWeekShutdownTime;
}
