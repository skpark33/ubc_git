// ServerFileUploadDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"

#include "ServerFileUploadDlg.h"

#include "Enviroment.h"

#include "../Dlls/UBCCopCommon/resource.h"
#include "common/libCommon/ubcMuxRegacy.h"


#define		WM_COMPLETE_ITEM		(WM_USER + 8200)
#define		WM_TERMINATE_UPLOAD		(WM_USER + 8201)


// CServerFileUploadDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CServerFileUploadDlg, CDialog)

CServerFileUploadDlg::CServerFileUploadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CServerFileUploadDlg::IDD, pParent)
{
	m_font.CreatePointFont(8*10, "Tahoma");

	m_bLogin = FALSE;
	m_bUploading = FALSE;
	m_bUserAbort = FALSE;
	m_pThread = NULL;
	m_pCurrentUploadingFile = NULL;
}

CServerFileUploadDlg::~CServerFileUploadDlg()
{
	for(int i=0; i<m_FileList.GetCount(); i++)
	{
		FILE_ITEM* item = m_FileList.GetAt(i);
		if( item==NULL ) continue;
		delete item;
	}
	m_FileList.RemoveAll();
}

void CServerFileUploadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_SERVER_PATH, m_editServerPath);
	DDX_Control(pDX, IDC_EDIT_UPLOAD_FILE, m_editUploadFile);
	DDX_Control(pDX, IDC_TAB_CTRL, m_tc);
	DDX_Control(pDX, IDC_LIST_UPLOADING, m_lc[0]);
	DDX_Control(pDX, IDC_LIST_COMPLETED, m_lc[1]);
	DDX_Control(pDX, IDC_LIST_FAILED, m_lc[2]);
	DDX_Control(pDX, IDC_BUTTON_SELECT_FILE, m_btnSelectFile);
	DDX_Control(pDX, IDC_BUTTON_ADD_TO_UPLOAD_LIST, m_btnAddToUploadList);
	DDX_Control(pDX, IDCANCEL, m_btnClose);
}


BEGIN_MESSAGE_MAP(CServerFileUploadDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_FILE, &CServerFileUploadDlg::OnBnClickedButtonSelectFile)
	ON_BN_CLICKED(IDC_BUTTON_ADD_TO_UPLOAD_LIST, &CServerFileUploadDlg::OnBnClickedButtonAddToUploadList)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_CTRL, &CServerFileUploadDlg::OnTcnSelchangeTabCtrl)
	ON_EN_KILLFOCUS(IDC_EDIT_SERVER_PATH, &CServerFileUploadDlg::OnEnKillfocusEditServerPath)
	ON_MESSAGE(WM_COMPLETE_ITEM, OnCompleteItem)
	ON_MESSAGE(WM_TERMINATE_UPLOAD, OnTerminateUpload)
END_MESSAGE_MAP()


// CServerFileUploadDlg 메시지 처리기입니다.

BOOL CServerFileUploadDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	m_editServerPath.SetWindowText("/");

	//
	CBitmap bmp;
	bmp.LoadBitmap(IDB_TAB_IMAGE_LIST);
	m_ilTab.Create(16,16,ILC_COLORDDB|ILC_MASK, 3, 1);
	m_ilTab.Add(&bmp, RGB(255,0,255)); //바탕의 분홍색이 마스크
	m_tc.SetImageList(&m_ilTab);

	m_tc.InsertItem(eTCUploading, "uploading", 0); // temp-string
	m_tc.InsertItem(eTCCompleted, "completed", 1); // temp-string
	m_tc.InsertItem(eTCFailed,    "failed", 2); // temp-string

	UpdateTabCtrl(eTCUploading);

	//
	for(int i=0; i<eTCMAXCOUNT; i++)
	{
		m_lc[i].SetExtendedStyle(m_lc[i].GetExtendedStyle() | LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
		m_lc[i].InsertColumn(eLCFilename,   ::LoadStringById(IDS_SERVERFILEUPLOADDLG_COL001), 0, 150); //"파일 이름"
		m_lc[i].InsertColumn(eLCServerPath, ::LoadStringById(IDS_SERVERFILEUPLOADDLG_COL002), 0, 280); //"서버 경로"
		m_lc[i].InsertColumn(eLCStatus,     ::LoadStringById(IDS_SERVERFILEUPLOADDLG_COL003), 0, 100); //"상태"
		m_lc[i].InsertColumn(eLCLocalPath,  ::LoadStringById(IDS_SERVERFILEUPLOADDLG_COL004), 0, 200); //"로컬 파일 경로"
		m_lc[i].SetProgressBarCol(eLCStatus);
		//m_lc.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
		m_lc[i].SetSortEnable(false);
		m_lc[i].SetFont(&m_font);
	}

	//
	CWaitMessageBox wait(LoadStringById(IDS_FTPMULTISITE_MSG013));

	//
	ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(GetEnvPtr()->m_szSite);
	if(!aData){
		UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_UPLOAD_AUTH, MB_ICONSTOP);
		return FALSE;
	}

	//
	m_objFileSvcClient.SetHttpServerInfo(aData->getFTPAddress(), aData->ftpPort);
	m_bLogin = m_objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str());

	if( !m_bLogin )
	{
		//TraceLog(("IP:%s / PORT:%d / ID:%s /PW:%s",stPackageInfo.ip.c_str(),stPackageInfo.port,stPackageInfo.userId.c_str(),stPackageInfo.passwd.c_str()));
		UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_UPLOAD_AUTH, MB_ICONSTOP);
		return FALSE;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CServerFileUploadDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();
}

void CServerFileUploadDlg::OnCancel()
{
	if( m_lc[eTCUploading].GetItemCount() > 0 )
	{
		//int ret = UbcMessageBox("업로드 목록이 아직 남아있습니다.\r\n\r\n백그라운드에서 계속 업로드를 진행하시겠습니까 ?"/*IDS_*/, MB_ICONWARNING | MB_YESNOCANCEL);
		int ret = UbcMessageBox(::LoadStringById(IDS_SERVERFILEUPLOADDLG_MSG001), MB_ICONWARNING | MB_YESNOCANCEL);
		if( ret == IDCANCEL ) return;
		if( ret == IDNO ) StopUpload();
	}

	//CDialog::OnCancel();
	ShowWindow(SW_HIDE);
}

void CServerFileUploadDlg::UpdateTabCtrl(TAB_COLUMN eShowTab)
{
	TCITEM tci = {0};

	char buf[256] = {0};
	tci.pszText = buf;
	tci.cchTextMax = 255;
	tci.mask = TCIF_TEXT;

	CString str_fmt[eTCMAXCOUNT];
	str_fmt[eTCUploading] = ::LoadStringById(IDS_SERVERFILEUPLOADDLG_TAB001);//"업로드 목록(%d)";
	str_fmt[eTCCompleted] = ::LoadStringById(IDS_SERVERFILEUPLOADDLG_TAB002);//"완료 목록(%d)";
	str_fmt[eTCFailed] = ::LoadStringById(IDS_SERVERFILEUPLOADDLG_TAB003);//"미완료 목록(%d)";

	for(int i=0; i<eTCMAXCOUNT; i++)
	{
		sprintf(buf, str_fmt[i], m_lc[i].GetItemCount());
		m_tc.SetItem(i, &tci);

		if( eShowTab != eTCUnknown )
			m_lc[i].ShowWindow( (i == eShowTab) ? SW_SHOW : SW_HIDE );
	}
}

void CServerFileUploadDlg::OnBnClickedButtonSelectFile()
{
	CFileDialog dlg(TRUE, "", "", OFN_FILEMUSTEXIST | OFN_ALLOWMULTISELECT, "All File (*.*)|*.*||", this);
	if(dlg.DoModal() == IDOK)
	{
		m_SelectedFileList.RemoveAll();

		CString str_filelist;
		POSITION pos = dlg.GetStartPosition();
		while (pos != NULL)
		{
			CString str_fullpath = dlg.GetNextPathName(pos);
			m_SelectedFileList.Add( str_fullpath );

			if( str_filelist.GetLength() > 0 ) str_filelist+=", ";
			str_filelist += str_fullpath;
		}

		m_editUploadFile.SetWindowText(str_filelist);
	}
}

void CServerFileUploadDlg::OnBnClickedButtonAddToUploadList()
{
	for(int i=0; i<m_SelectedFileList.GetCount(); i++)
	{
		const CString& str_fullpath = m_SelectedFileList.GetAt(i);

		CFileStatus fs;
		if( !CFile::GetStatus(str_fullpath, fs) ) continue;

		FILE_ITEM* item = new FILE_ITEM;
		item->ulFilesize = fs.m_size;
		item->eErrorCode = eECNone;

		int pos = 0;
		CString str_token;
		while( (str_token=str_fullpath.Tokenize("\\",pos)) != "" )
			item->strFilename = str_token;
		m_editServerPath.GetWindowText(item->strServerPath);
		item->strLocalPath = str_fullpath;
		item->eStatus = eFSReady;

		m_FileList.Add(item);

		AddItem(eTCUploading, item);
	}

	UpdateTabCtrl();

	m_SelectedFileList.RemoveAll();
	m_editUploadFile.SetWindowText("");
}

void CServerFileUploadDlg::OnTcnSelchangeTabCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	UpdateTabCtrl((TAB_COLUMN)m_tc.GetCurSel());
}

void CServerFileUploadDlg::OnEnKillfocusEditServerPath()
{
	CString str_server_path;
	m_editServerPath.GetWindowText(str_server_path);
	str_server_path.Trim(" \t");

	if( str_server_path.GetLength() == 0 ||
		str_server_path.GetAt(0) != '/' )
	{
		str_server_path.Insert(0, "/");
		m_editServerPath.SetWindowText(str_server_path);
	}
}

void CServerFileUploadDlg::AddItem(TAB_COLUMN eTab, FILE_ITEM* pFileItem)
{
	if( pFileItem==NULL ) return;

	CAutoLock _lock(m_cs);

	int idx = m_lc[eTab].GetItemCount();
	m_lc[eTab].InsertItem(idx, "");
	m_lc[eTab].SetItemText(idx, eLCFilename, 	pFileItem->strFilename);
	m_lc[eTab].SetItemText(idx, eLCServerPath, 	pFileItem->strServerPath);
	if( pFileItem->eStatus != eFSError )
		m_lc[eTab].SetItemText(idx, eLCStatus,		GetFileStatusString(pFileItem->eStatus));
	else
	{
		CString str_err;
		str_err.Format("%s - %d", GetFileStatusString(pFileItem->eStatus), pFileItem->eErrorCode);
		m_lc[eTab].SetItemText(idx, eLCStatus, str_err);
	}
	m_lc[eTab].SetItemText(idx, eLCLocalPath, 	pFileItem->strLocalPath);

	m_lc[eTab].SetItemData(idx, (DWORD)pFileItem);
}

LPCSTR CServerFileUploadDlg::GetFileStatusString(FILE_STATUS eStatus)
{
	switch( eStatus )
	{
	case eFSReady:
		return ::LoadStringById(IDS_SERVERFILEUPLOADDLG_STATUS001);//"대기"
	case eFSUploading:
		return ::LoadStringById(IDS_SERVERFILEUPLOADDLG_STATUS002);//"업로드중"
	case eFSCompleted:
		return ::LoadStringById(IDS_SERVERFILEUPLOADDLG_STATUS003);//"완료"
	case eFSAborted:
		return ::LoadStringById(IDS_SERVERFILEUPLOADDLG_STATUS004);//"중단"
	case eFSError:
		return ::LoadStringById(IDS_SERVERFILEUPLOADDLG_STATUS005);//"오류"
	}

	return "Unknown Status";
}

void CServerFileUploadDlg::StartUpload()
{
	if( m_bUploading == FALSE )
	{
		m_bUploading = TRUE;

		//
		m_pThread = ::AfxBeginThread(UploadThreadProc, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = TRUE;
		m_pThread->ResumeThread();
	}
}

void CServerFileUploadDlg::StopUpload()
{
	// disable controls
	m_editServerPath.EnableWindow(FALSE);
	m_editUploadFile.EnableWindow(FALSE);
	m_btnSelectFile.EnableWindow(FALSE);
	m_btnAddToUploadList.EnableWindow(FALSE);
	m_btnClose.EnableWindow(FALSE);

	// change button-text
	m_btnClose.SetWindowText(::LoadStringById(IDS_SERVERFILEUPLOADDLG_MSG002)/*"닫는중"*/);

	m_bUserAbort = TRUE;
	m_bUploading = FALSE;

	// uploading ==> cancel
	m_objFileSvcClient.SetUserCancel();
}

UINT CServerFileUploadDlg::UploadThreadProc(LPVOID param)
{
//	::CoInitializeEx(NULL, COINIT_MULTITHREADED);

	CServerFileUploadDlg* pDlg = (CServerFileUploadDlg*)param;
	if(pDlg)
	{
		HWND parent_wnd = pDlg->GetSafeHwnd();

		BOOL ret = pDlg->UploadFile();

		::PostMessage(parent_wnd, WM_TERMINATE_UPLOAD, ret, NULL);
	}

//	::CoUninitialize();

	return FALSE;
}

void CServerFileUploadDlg::ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath)
{
	CAutoLock _lock(m_cs);

	if( m_pCurrentUploadingFile==NULL ) return;
	if( m_lc[eTCUploading].GetItemCount() <= 0 ) return;

	if(nFlag == CFileServiceWrap::FS_DOING && m_pCurrentUploadingFile->ulFilesize > 0)
	{
		int progress = (uSendBytes * 1000) / m_pCurrentUploadingFile->ulFilesize;

		CString str_progress;
		str_progress.Format("%d%%", progress);
		str_progress.Insert(str_progress.GetLength()-2, ".");

		m_lc[eTCUploading].SetItemText(0, eLCStatus, str_progress);
	}
}

BOOL CServerFileUploadDlg::UploadFile()
{
	while(m_bUploading && !m_bUserAbort)
	{
		m_pCurrentUploadingFile = NULL;

		if( m_lc[eTCUploading].GetItemCount() <= 0 )
		{
			::Sleep(10);
			continue;
		}

		{
			CAutoLock _lock(m_cs);
			m_pCurrentUploadingFile = (FILE_ITEM*)m_lc[eTCUploading].GetItemData(0);
		}
		FILE_ITEM* item = m_pCurrentUploadingFile;
		if( item == NULL )
		{
			//item->eStatus = eFSError;
			//item->eErrorCode = eECInternalError;
			SendMessage(WM_COMPLETE_ITEM, FALSE, (WPARAM)item);
			continue;
		}

		CFileStatus fs;
		if( !CFile::GetStatus(item->strLocalPath, fs) )
		{
			item->eStatus = eFSError;
			item->eErrorCode = eECFileNotFound;
			SendMessage(WM_COMPLETE_ITEM, FALSE, (WPARAM)item);
			continue;
		}
		item->ulFilesize = fs.m_size;

		CString str_svr_path = item->strServerPath;
		if( str_svr_path.Right(1) != "/" ) str_svr_path+="/";
		str_svr_path += item->strFilename;

		if( m_bUserAbort ) break;

		BOOL result = FALSE;
		result = m_objFileSvcClient.PutFile(item->strLocalPath, str_svr_path, 0, (IProgressHandler*)this);

		if( m_bUserAbort ) break;

		if( result )
		{
			item->eStatus = eFSCompleted;
			item->eErrorCode = eECNone;
			SendMessage(WM_COMPLETE_ITEM, TRUE, (WPARAM)item);
		}
		else
		{
			item->eStatus = eFSError;
			item->eErrorCode = eECSendError;
			SendMessage(WM_COMPLETE_ITEM, FALSE, (WPARAM)item);
		}
	}

	return TRUE;
}

LRESULT CServerFileUploadDlg::OnCompleteItem(WPARAM wParam, LPARAM lParam)
{
	if( m_lc[eTCUploading].GetItemCount() <= 0 ) return FALSE;

	BOOL result = (BOOL)wParam;
	FILE_ITEM* item = (FILE_ITEM*)lParam;

	int idx = 0;
	//LVFINDINFO lvFind;
	//lvFind.flags = LVFI_PARAM;
	//lvFind.lParam = lParam;
	//idx = m_lc[eTCUploading].FindItem(&lvFind);
	//if( idx < 0  || lParam == NULL ) idx = 0;

	{
		CAutoLock _lock(m_cs);
		m_lc[eTCUploading].DeleteItem(idx);
	}

	if( wParam )
		AddItem(eTCCompleted, item);
	else
		AddItem(eTCFailed, item);

	UpdateTabCtrl();

	return TRUE;
}

LRESULT CServerFileUploadDlg::OnTerminateUpload(WPARAM wParam, LPARAM lParam)
{
	m_bUserAbort = FALSE;

	// disable controls
	m_editServerPath.EnableWindow(TRUE);
	m_editUploadFile.EnableWindow(TRUE);
	m_btnSelectFile.EnableWindow(TRUE);
	m_btnAddToUploadList.EnableWindow(TRUE);
	m_btnClose.EnableWindow(TRUE);

	// change button-text
	m_btnClose.SetWindowText(::LoadStringById(IDS_SERVERFILEUPLOADDLG_MSG003)/*"닫기"*/);

	int cnt = m_lc[eTCUploading].GetItemCount();
	for(int i=0; i<cnt; i++)
	{
		FILE_ITEM* item = (FILE_ITEM*)m_lc[eTCUploading].GetItemData(i);
		if( item==NULL ) continue;

		item->eStatus = eFSAborted;
		AddItem(eTCFailed, item);
	}
	m_lc[eTCUploading].DeleteAllItems();

	UpdateTabCtrl();

	//
	if( m_bUploading )
	{
		StartUpload();
	}

	return FALSE;
}
