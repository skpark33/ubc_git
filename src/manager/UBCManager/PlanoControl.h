#pragma once
#include "DownloadPictureEx.h"

struct SPlanoInfo;
class CNodeControl;
typedef CList<CNodeControl*>			NodeControlList;

#define PLANO_ICON_WIDTH 24
#define PLANO_ICON_HEIGHT 24

class CPlanoControl : public CDownloadPictureEx
{
protected:
	SPlanoInfo*		m_planoInfo;
	bool			m_isEditMode;
	bool			m_isShowInfo;

	CPoint			m_lastRClickPoint;
	NodeControlList	m_NodeList;

	int _LoadAllNode(int oper, int admin);

public:
	enum { e_PLANOTYPE_HOST, e_PLANOTYPE_LIGHT, e_PLANOTYPE_ETC };

	CPlanoControl();
	virtual ~CPlanoControl();
	void	Clear();
	void	SetEditMode(bool p) {m_isEditMode=p;}
	void	SetShowInfo(bool p) {m_isShowInfo=p;}
	bool	IsEditMode() {return m_isEditMode;}
	bool	IsShowInfo() {return m_isShowInfo;}
	void	SelectAll(bool select);

	SPlanoInfo*	GetInfo() { return m_planoInfo; }

	afx_msg void OnDestroy();
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()

public:
	bool Load(SPlanoInfo* plano,int oper, int admin);
	int  Save();
	void Reboot();
	void Muteon();
	void Muteoff();
	void Poweron();
	void Poweroff();
	void Powerset();
	void Brightset(int value);

	void Remotelogin();
	void Remotelogin2();
	void ApplyPackage();
	void CancelPackage();

	int	 ChangeOPStat(const char* siteId, const char* hostId, bool opStat);
	int	 ChangeDeviceOPStat(const char* siteId, const char* hostId, const char* objectClass, const char* objectId, bool opStat);
	CNodeControl* ClickOnNode(CPoint& point);

	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnPlanoCreateLight();
	afx_msg void OnPlanoCreateMonitor();
	afx_msg void OnPlanoCreatePc();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};