// PackageChngLogDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "common\ubcdefine.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "PackageChngLogDlg.h"
#include "ubccopcommon\SelectHostDlg.h"
#include "HostExcelSave.h"


// CPackageChngLogDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPackageChngLogDlg, CDialog)

CPackageChngLogDlg::CPackageChngLogDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPackageChngLogDlg::IDD, pParent)
	, m_Reposition(this)
	, m_bInitialize(false)
{

}

CPackageChngLogDlg::~CPackageChngLogDlg()
{
}

void CPackageChngLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_DOWNLOAD, m_lcList);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_bnClose);
	DDX_Control(pDX, IDC_BN_TO_EXCEL, m_btnExcelSave);
	DDX_Control(pDX, IDC_DATE_FILTER_START, m_dtFilterStart);
	DDX_Control(pDX, IDC_DATE_FILTER_END, m_dtFilterEnd);
}


BEGIN_MESSAGE_MAP(CPackageChngLogDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BUTTON_FILTER_HOST, OnBnClickedButtonFilterHost)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, OnBnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BN_TO_EXCEL, OnBnClickedBnToExcel)
END_MESSAGE_MAP()


// CPackageChngLogDlg 메시지 처리기입니다.

BOOL CPackageChngLogDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnClose.LoadBitmap(IDB_BUTTON_CLOSE, RGB(255,255,255));
	m_btnExcelSave.LoadBitmap(IDB_BUTTON_EXCEL, RGB(255,255,255));

	m_bnRefresh.SetToolTipText(LoadStringById(IDS_PACKAGE_CHNG_LOG_BTN001));
	m_btnExcelSave.SetToolTipText(LoadStringById(IDS_PACKAGE_CHNG_LOG_BTN002));

	m_Reposition.AddControl(&m_lcList, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	CTime tmCur = CTime::GetCurrentTime();
	m_dtFilterEnd.SetTime(&tmCur);
	InitDateRange(m_dtFilterEnd);

	CTime tmStartDate = tmCur - CTimeSpan(7, 0, 0, 0);
	m_dtFilterStart.SetTime(&tmStartDate);
	InitDateRange(m_dtFilterStart);

	SetDlgItemText(IDC_EDIT_FILTER_HOST, m_strHostName);

	InitList();
	//skpark web-->client  호스트ID 가 없으면 Default 검색을 하지 않는다.
	if(!m_strHostId.IsEmpty() || !this->m_strHostName.IsEmpty()){
		RefreshList();
	}
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPackageChngLogDlg::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eSite     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST001);
	m_szColum[eHost     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST002);
	m_szColum[ePackage  ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST003);
	m_szColum[eApplyTime] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST004);
	m_szColum[eHow      ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST005);
	m_szColum[eWho      ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST006);
	m_szColum[eWhy      ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST007);

	m_lcList.InsertColumn(eSite     , m_szColum[eSite     ], LVCFMT_LEFT  ,  60);
	m_lcList.InsertColumn(eHost     , m_szColum[eHost     ], LVCFMT_LEFT  ,  80);
	m_lcList.InsertColumn(ePackage  , m_szColum[ePackage  ], LVCFMT_LEFT  , 200);
	m_lcList.InsertColumn(eApplyTime, m_szColum[eApplyTime], LVCFMT_CENTER, 120);
	m_lcList.InsertColumn(eHow      , m_szColum[eHow      ], LVCFMT_LEFT  ,  80);
	m_lcList.InsertColumn(eWho      , m_szColum[eWho      ], LVCFMT_LEFT  , 150);
	m_lcList.InsertColumn(eWhy      , m_szColum[eWhy      ], LVCFMT_LEFT  ,  80);

	// 제일 마지막에 할것...
	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CPackageChngLogDlg::RefreshList()
{
	CWaitMessageBox wait;

	m_lcList.DeleteAllItems();
	m_lsInfoList.RemoveAll();

	CString strWhere;

	CTime tmStartDate;
	m_dtFilterStart.GetTime(tmStartDate);
	
	CTime tmEndDate;
	m_dtFilterEnd.GetTime(tmEndDate);

	// 작성일자
	CString strStartDate = tmStartDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = tmEndDate.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");
	strWhere.Format(_T("applyTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);

	strWhere += _T(" order by applyTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetApplyInfoList( m_strHostId, m_lsInfoList, strWhere ))
	{
		return;
	}

	m_lcList.SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SApplyLogInfo& info = m_lsInfoList.GetNext(pos);

		CTime tmApplyTime = CTime(info.applyTime);

		m_lcList.InsertItem(nRow, info.siteId);
		m_lcList.SetItemText(nRow, eHost     , info.hostId   );
		m_lcList.SetItemText(nRow, ePackage  , info.programId);
		m_lcList.SetItemText(nRow, eApplyTime, (info.applyTime == 0 ? "" : tmApplyTime.Format("%Y/%m/%d %H:%M:%S")));
		m_lcList.SetItemText(nRow, eHow      , info.how      );
		m_lcList.SetItemText(nRow, eWho      , info.who      );
		m_lcList.SetItemText(nRow, eWhy      , info.why      );

		m_lcList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	m_lcList.SetRedraw(TRUE);
}

int CPackageChngLogDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(m_rcClient);

	m_Reposition.SetParentRect(m_rcClient);

	return 0;
}

void CPackageChngLogDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CPackageChngLogDlg::OnBnClickedButtonFilterHost()
{
	CSelectHostDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	m_strHostId = _T("");
	SetDlgItemText(IDC_EDIT_FILTER_HOST, _T(""));

	if(dlg.DoModal() == IDOK)
	{
		m_strHostId = dlg.m_strHostId;
		m_strHostName = dlg.m_strHostName;
		SetDlgItemText(IDC_EDIT_FILTER_HOST, m_strHostName);
	}
}

void CPackageChngLogDlg::OnBnClickedButtonRefresh()
{
	RefreshList();
}

void CPackageChngLogDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	CDialog::OnGetMinMaxInfo(lpMMI);

	if(m_bInitialize)
	{
		lpMMI->ptMinTrackSize.x = m_rcClient.Width() + ::GetSystemMetrics(SM_CXDLGFRAME);
		lpMMI->ptMinTrackSize.y = m_rcClient.Height() + ::GetSystemMetrics(SM_CXDLGFRAME);
	}
	else
	{
		m_bInitialize = true;
	}
}

void CPackageChngLogDlg::OnBnClickedButtonClose()
{
	OnOK();
}

void CPackageChngLogDlg::OnBnClickedBnToExcel()
{
	CHostExcelSave dlg;

	CString strExcelFile = dlg.Save(  LoadStringById(IDS_PACKAGE_CHNG_LOG_STR001)
									, m_lcList
									);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}
