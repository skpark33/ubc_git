#pragma once


// CSiteFrame frame

class CSiteFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CSiteFrame)
protected:
	CSiteFrame();           // protected constructor used by dynamic creation
	virtual ~CSiteFrame();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


