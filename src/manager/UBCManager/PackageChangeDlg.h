#pragma once

#include "common\utblistctrlex.h"
#include "ubccopcommon\CopModule.h"
#include "afxwin.h"

// CPackageChangeDlg dialog
class CPackageChangeDlg : public CDialog
{
	DECLARE_DYNAMIC(CPackageChangeDlg)
public:
	enum { eCheck, eSiteID, eHostID, eDisplayNo, eAutoSche, eCurSche, eCreator, eLastSche, /*eScheTime,*/ eExpEnd };
private:
	CString m_szColHostTitle[eExpEnd];
	CUTBListCtrlEx m_lscHost;
	void InitHostList();
	void InserHostList();

	SPackageInfo m_stPackageInfo;

public:
	CPackageChangeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPackageChangeDlg();

	CArray<SHostInfo> m_arHostList;

// Dialog Data
	enum { IDD = IDD_PACKAGE_CHANGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedBnSelectPackage();
};
