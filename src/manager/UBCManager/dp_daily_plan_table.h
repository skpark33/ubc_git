#pragma once
#include "_Font.h"

// 컴퓨터에서 Microsoft Visual C++를 사용하여 생성한 IDispatch 래퍼 클래스입니다.

// 참고: 이 파일의 내용을 수정하지 마십시오. Microsoft Visual C++에서
//  이 클래스를 다시 생성할 때 수정한 내용을 덮어씁니다.

/////////////////////////////////////////////////////////////////////////////
// CDp_daily_plan_table 래퍼 클래스입니다.

class CDp_daily_plan_table : public CWnd
{
protected:
	DECLARE_DYNCREATE(CDp_daily_plan_table)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0xB1F60086, 0xAE0E, 0x4032, { 0x97, 0xBA, 0x73, 0xF0, 0xD6, 0xF9, 0xA8, 0x33 } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle,
						const RECT& rect, CWnd* pParentWnd, UINT nID, 
						CCreateContext* pContext = NULL)
	{ 
		return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); 
	}

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, 
				UINT nID, CFile* pPersist = NULL, BOOL bStorage = FALSE,
				BSTR bstrLicKey = NULL)
	{ 
		return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); 
	}

// 특성입니다.
public:


// 작업입니다.
public:

// _DUbcDailyPlanTable

// Functions
//

	void AboutBox()
	{
		InvokeHelper(DISPID_ABOUTBOX, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Refresh()
	{
		InvokeHelper(DISPID_REFRESH, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void DeleteAllItem()
	{
		InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	BOOL DeleteItem(long nIndex)
	{
		BOOL result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, nIndex);
		return result;
	}
	long GetItemCount()
	{
		long result;
		InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	long GetSelectedIndex()
	{
		long result;
		InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	void SetSelectedIndex(long nIndex)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms, nIndex);
	}
	long AddItem(LPCTSTR strText, DATE tmStartTime, DATE tmEndTime, long * lParam)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_DATE VTS_DATE VTS_PI4 ;
		InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms, strText, tmStartTime, tmEndTime, lParam);
		return result;
	}
	long InsertItem(long nIndex, LPCTSTR strText, DATE tmStartTime, DATE tmEndTime, long * lParam)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_DATE VTS_DATE VTS_PI4 ;
		InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, parms, nIndex, strText, tmStartTime, tmEndTime, lParam);
		return result;
	}
	BOOL SetItem(long nIndex, unsigned long nFmt, LPCTSTR strText, DATE tmStartTime, DATE tmEndTime, long * lParam)
	{
		BOOL result;
		static BYTE parms[] = VTS_I4 VTS_UI4 VTS_BSTR VTS_DATE VTS_DATE VTS_PI4 ;
		InvokeHelper(0x13, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, nIndex, nFmt, strText, tmStartTime, tmEndTime, lParam);
		return result;
	}
	BOOL GetItem(long nIndex, long * lParam)
	{
		BOOL result;
		static BYTE parms[] = VTS_I4 VTS_PI4 ;
		InvokeHelper(0x14, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, nIndex, lParam);
		return result;
	}
	BOOL SetZOrder(long nCurIndex, long nNewIndex)
	{
		BOOL result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x16, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, nCurIndex, nNewIndex);
		return result;
	}
	void SetItemColor(long nIndex, BOOL bGradient, unsigned long clrColor)
	{
		static BYTE parms[] = VTS_I4 VTS_BOOL VTS_UI4 ;
		InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, parms, nIndex, bGradient, clrColor);
	}

// Properties
//

short GetBorderStyle()
{
	short result;
	GetProperty(DISPID_BORDERSTYLE, VT_I2, (void*)&result);
	return result;
}
void SetBorderStyle(short propVal)
{
	SetProperty(DISPID_BORDERSTYLE, VT_I2, propVal);
}
short Get_BorderStyle()
{
	short result;
	GetProperty(0x0, VT_I2, (void*)&result);
	return result;
}
void Set_BorderStyle(short propVal)
{
	SetProperty(0x0, VT_I2, propVal);
}
COleFont GetFont()
{
	LPDISPATCH result;
	GetProperty(DISPID_FONT, VT_DISPATCH, (void*)&result);
	return COleFont(result);
}
void SetFont(LPDISPATCH propVal)
{
	SetProperty(DISPID_FONT, VT_DISPATCH, propVal);
}
unsigned long GetBackColor()
{
	unsigned long result;
	GetProperty(DISPID_BACKCOLOR, VT_UI4, (void*)&result);
	return result;
}
void SetBackColor(unsigned long propVal)
{
	SetProperty(DISPID_BACKCOLOR, VT_UI4, propVal);
}
unsigned long GetForeColor()
{
	unsigned long result;
	GetProperty(DISPID_FORECOLOR, VT_UI4, (void*)&result);
	return result;
}
void SetForeColor(unsigned long propVal)
{
	SetProperty(DISPID_FORECOLOR, VT_UI4, propVal);
}
unsigned long GetTimeForeColor()
{
	unsigned long result;
	GetProperty(0x1, VT_UI4, (void*)&result);
	return result;
}
void SetTimeForeColor(unsigned long propVal)
{
	SetProperty(0x1, VT_UI4, propVal);
}
unsigned long GetTimeBackColor()
{
	unsigned long result;
	GetProperty(0x2, VT_UI4, (void*)&result);
	return result;
}
void SetTimeBackColor(unsigned long propVal)
{
	SetProperty(0x2, VT_UI4, propVal);
}
short GetDefaultViewTime()
{
	short result;
	GetProperty(0x3, VT_I2, (void*)&result);
	return result;
}
void SetDefaultViewTime(short propVal)
{
	SetProperty(0x3, VT_I2, propVal);
}
short GetViewRange()
{
	short result;
	GetProperty(0x4, VT_I2, (void*)&result);
	return result;
}
void SetViewRange(short propVal)
{
	SetProperty(0x4, VT_I2, propVal);
}
unsigned long GetPlanBackColor()
{
	unsigned long result;
	GetProperty(0x5, VT_UI4, (void*)&result);
	return result;
}
void SetPlanBackColor(unsigned long propVal)
{
	SetProperty(0x5, VT_UI4, propVal);
}
unsigned long GetPlanForeColor()
{
	unsigned long result;
	GetProperty(0x6, VT_UI4, (void*)&result);
	return result;
}
void SetPlanForeColor(unsigned long propVal)
{
	SetProperty(0x6, VT_UI4, propVal);
}
unsigned long GetPlanBorderColor()
{
	unsigned long result;
	GetProperty(0x7, VT_UI4, (void*)&result);
	return result;
}
void SetPlanBorderColor(unsigned long propVal)
{
	SetProperty(0x7, VT_UI4, propVal);
}
unsigned long GetSelectBorderColor()
{
	unsigned long result;
	GetProperty(0x8, VT_UI4, (void*)&result);
	return result;
}
void SetSelectBorderColor(unsigned long propVal)
{
	SetProperty(0x8, VT_UI4, propVal);
}
BOOL GetEditable()
{
	BOOL result;
	GetProperty(0x9, VT_BOOL, (void*)&result);
	return result;
}
void SetEditable(BOOL propVal)
{
	SetProperty(0x9, VT_BOOL, propVal);
}
unsigned long GetPlanLineColor()
{
	unsigned long result;
	GetProperty(0xa, VT_UI4, (void*)&result);
	return result;
}
void SetPlanLineColor(unsigned long propVal)
{
	SetProperty(0xa, VT_UI4, propVal);
}
unsigned long GetTimeLineColor()
{
	unsigned long result;
	GetProperty(0xb, VT_UI4, (void*)&result);
	return result;
}
void SetTimeLineColor(unsigned long propVal)
{
	SetProperty(0xb, VT_UI4, propVal);
}
unsigned short GetMagneticMove()
{
	unsigned short result;
	GetProperty(0x15, VT_UI2, (void*)&result);
	return result;
}
void SetMagneticMove(unsigned short propVal)
{
	SetProperty(0x15, VT_UI2, propVal);
}


};
