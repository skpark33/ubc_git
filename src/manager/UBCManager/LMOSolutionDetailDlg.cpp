// LMOSolutionDetailDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LMOSolutionDetailDlg.h"


// CLMOSolutionDetailDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLMOSolutionDetailDlg, CDialog)

CLMOSolutionDetailDlg::CLMOSolutionDetailDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLMOSolutionDetailDlg::IDD, pParent)
{
	m_isSet = false;
}

CLMOSolutionDetailDlg::~CLMOSolutionDetailDlg()
{
}

void CLMOSolutionDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_DESCRIPTION, m_edit_description);
	DDX_Control(pDX, IDC_EDIT_TABLENAME, m_edit_tablename);
	DDX_Control(pDX, IDC_EDIT_TIMEFIELDNAME, m_edit_timeFieldname);
	DDX_Control(pDX, IDC_COMBO_LOGTYPE, m_cb_logType);
	DDX_Control(pDX, IDC_EDIT_DURATION, m_edit_duration);
	DDX_Control(pDX, IDC_COMBO_ADMINSTATE, m_cb_adminState);
	DDX_Control(pDX, IDC_SPIN_DURATION, m_spin_duration);
}


BEGIN_MESSAGE_MAP(CLMOSolutionDetailDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CLMOSolutionDetailDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CLMOSolutionDetailDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CLMOSolutionDetailDlg 메시지 처리기입니다.

BOOL CLMOSolutionDetailDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_spin_duration.SetRange32(0, 365);

	m_cb_logType.AddString("DB");
	m_cb_logType.AddString("File");

	m_cb_adminState.AddString("false");
	m_cb_adminState.AddString("true");

	InitData();

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CLMOSolutionDetailDlg::InitData()
{
	if(m_isSet){
		this->m_edit_description.SetWindowText(m_lmoSolutionInfo.description);
		this->m_edit_tablename.SetWindowText(m_lmoSolutionInfo.tableName);
		this->m_edit_timeFieldname.SetWindowText(m_lmoSolutionInfo.timeFieldName);

		this->m_cb_adminState.SetCurSel(m_lmoSolutionInfo.adminState);
		this->m_cb_logType.SetCurSel(m_lmoSolutionInfo.logType);

		char buf[10] = {0,};
		sprintf(buf,"%ld", m_lmoSolutionInfo.duration);
		this->m_edit_duration.SetWindowText(buf);
	}else{
		this->m_cb_adminState.SetCurSel(true);
		this->m_cb_logType.SetCurSel(0);
		this->m_edit_duration.SetWindowText("30");
	}
}

void CLMOSolutionDetailDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_isSet){

		m_lmoSolutionInfo.adminState = this->m_cb_adminState.GetCurSel();
		m_lmoSolutionInfo.logType = this->m_cb_logType.GetCurSel();

		CString buf="0";
		this->m_edit_duration.GetWindowText(buf);
		m_lmoSolutionInfo.duration = atoi(buf);
		if(m_lmoSolutionInfo.duration == 0){
			UbcMessageBox(LoadStringById(IDS_LMOSOLUTION_MSG006), MB_ICONINFORMATION);
			return;
		}


		this->m_edit_description.GetWindowText(m_lmoSolutionInfo.description);
		this->m_edit_tablename.GetWindowText(m_lmoSolutionInfo.tableName);
		this->m_edit_timeFieldname.GetWindowText(m_lmoSolutionInfo.timeFieldName);

		if(m_lmoSolutionInfo.description.IsEmpty()){
			UbcMessageBox(LoadStringById(IDS_LMOSOLUTION_MSG003), MB_ICONINFORMATION);
			return;
		}
		if(m_lmoSolutionInfo.tableName.IsEmpty()){
			UbcMessageBox(LoadStringById(IDS_LMOSOLUTION_MSG004), MB_ICONINFORMATION);
			return;
		}
		if(m_lmoSolutionInfo.logType == 0 && m_lmoSolutionInfo.timeFieldName.IsEmpty()){
			UbcMessageBox(LoadStringById(IDS_LMOSOLUTION_MSG005), MB_ICONINFORMATION);
			return;
		}


	}

	OnOK();
}

void CLMOSolutionDetailDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
