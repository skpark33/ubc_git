// PlanoView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "PlanoView.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "common\TraceLog.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "MemDC.h"


#define STR_ADMIN_ON			LoadStringById(IDS_HOSTVIEW_LIST001)
#define STR_ADMIN_OFF			LoadStringById(IDS_HOSTVIEW_LIST002)
#define STR_OPERA_ON			LoadStringById(IDS_HOSTVIEW_LIST003)
#define STR_OPERA_OFF			LoadStringById(IDS_HOSTVIEW_LIST004)


// CPlanoView

IMPLEMENT_DYNCREATE(CPlanoView, CFormView)

CPlanoView::CPlanoView()
	: CFormView(CPlanoView::IDD)
{

}

CPlanoView::~CPlanoView()
{
	Clear();
}

void
CPlanoView::Clear()
{
	TraceLog(("Clear()"));

	POSITION pos = m_planoList.GetHeadPosition();
	while(pos)
	{
		SPlanoInfo* aPlano = m_planoList.GetNext(pos);
		delete aPlano;
	}
	m_planoList.RemoveAll();
}

void CPlanoView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_FILTER_OPR, m_cbOperation);
	DDX_Control(pDX, IDC_COMBO_FILTER_ADMIN, m_cbAdmin);
	DDX_Control(pDX, IDC_COMBO_FILTER_PLANOID, m_cbPlanoId);
	DDX_Control(pDX, IDC_BUTTON_HOST_REF, m_btnRefHost);
	DDX_Control(pDX, IDC_BUTTON_POWEROFF, m_btnShutdown);
	DDX_Control(pDX, IDC_BUTTON_POWERON, m_btnStartup);
	DDX_Control(pDX, IDC_BUTTON_POWERSET, m_btnPowerMng);
	DDX_Control(pDX, IDC_BUTTON_SAVE, m_btnSave);
	DDX_Control(pDX, IDC_BUTTON_CANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_SLIDER_BRIGHT, m_sliderBright);
	DDX_Control(pDX, IDC_CHECK_SELECTALL, m_kbSelectAll);
	DDX_Control(pDX, IDC_RADIO_VIEWMODE, m_radioViewMode);
	DDX_Control(pDX, IDC_RADIO_EDITMODE, m_radioEditMode);
	DDX_Control(pDX, IDC_BUTTON_SETBRIGHT, m_btnSetBright);
	DDX_Control(pDX, IDC_PIC_CTRL, m_picCtrl);
	DDX_Control(pDX, IDC_STATIC_BRIGHT, m_stBright);
	DDX_Control(pDX, IDC_CHECK_SHOWNAME, m_kbShowName);
	//DDX_Control(pDX, IDC_CHECK_SHOWLIST, m_kbShowList);
	//DDX_Control(pDX, IDC_LIST_NODEVIEW, m_lscNode);
}

BEGIN_MESSAGE_MAP(CPlanoView, CFormView)
	ON_BN_CLICKED(IDC_RADIO_VIEWMODE, &CPlanoView::OnBnClickedRadioViewmode)
	ON_BN_CLICKED(IDC_RADIO_EDITMODE, &CPlanoView::OnBnClickedRadioEditmode)
	ON_BN_CLICKED(IDC_BUTTON_HOST_REF, &CPlanoView::OnBnClickedButtonHostRef)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CPlanoView::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CPlanoView::OnBnClickedButtonCancel)
	ON_BN_CLICKED(IDC_CHECK_SHOWNAME, &CPlanoView::OnBnClickedCheckShowname)
	ON_BN_CLICKED(IDC_CHECK_SELECTALL, &CPlanoView::OnBnClickedCheckSelectall)
	ON_BN_CLICKED(IDC_BUTTON_POWERON, &CPlanoView::OnBnClickedButtonPoweron)
	ON_BN_CLICKED(IDC_BUTTON_POWEROFF, &CPlanoView::OnBnClickedButtonPoweroff)
	ON_BN_CLICKED(IDC_BUTTON_POWERSET, &CPlanoView::OnBnClickedButtonPowerset)
	ON_BN_CLICKED(IDC_BUTTON_SETBRIGHT, &CPlanoView::OnBnClickedButtonSetbright)
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
	//ON_BN_CLICKED(IDC_CHECK_SHOWLIST, &CPlanoView::OnBnClickedCheckShowlist)
END_MESSAGE_MAP()


// CPlanoView 진단입니다.

#ifdef _DEBUG
void CPlanoView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPlanoView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPlanoView 메시지 처리기입니다.
void CPlanoView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	this->m_radioViewMode.SetCheck(TRUE);
	this->m_radioEditMode.SetCheck(FALSE);
	this->m_radioEditMode.EnableWindow(IsAuth(_T("HREG")));

	m_btnSave.ShowWindow(SW_HIDE);
	m_btnCancel.ShowWindow(SW_HIDE);

	// 모든 플라노 객체를 가져온다 !!!
	CCopModule::GetObject()->GetPlanoList("*", NULL, m_planoList);
	POSITION pos = m_planoList.GetHeadPosition();
	while(pos)
	{
		SPlanoInfo* info = m_planoList.GetNext(pos);
		m_cbPlanoId.AddString(info->planoId);
	}

	m_cbAdmin.AddString("All");
	m_cbAdmin.AddString(STR_ADMIN_ON);
	m_cbAdmin.AddString(STR_ADMIN_OFF);
	m_cbAdmin.SetCurSel(0);

	m_cbOperation.AddString("All");
	m_cbOperation.AddString(STR_OPERA_ON);
	m_cbOperation.AddString(STR_OPERA_OFF);
	m_cbOperation.SetCurSel(0);

	m_sliderBright.SetRange(0,100);
	m_sliderBright.SetPos(50);

	m_btnRefHost.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnShutdown.LoadBitmap(IDB_BUTTON_SHUTDOWN, RGB(255,255,255));
	m_btnStartup.LoadBitmap(IDB_BITMAP_POWERON, RGB(255,255,255));  
	m_btnPowerMng.LoadBitmap(IDB_BUTTON_POWER_MNG, RGB(255,255,255));

	m_btnSetBright.LoadBitmap(IDB_BUTTON_APPLY, RGB(255,255,255));

	m_btnSave.LoadBitmap(IDB_BITMAP_SAVEIMAGE, RGB(255,255,255));   
	m_btnCancel.LoadBitmap(IDB_BITMAP_CANCELIMAGE, RGB(255,255,255));

	m_btnRefHost.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN004));
	m_btnShutdown.SetToolTipText(LoadStringById(IDS_PLANOVIEW_BTN002));
	m_btnStartup.SetToolTipText(LoadStringById(IDS_PLANOVIEW_BTN001));  
	m_btnPowerMng.SetToolTipText(LoadStringById(IDS_PLANOVIEW_BTN003));

	m_btnSave.SetToolTipText(LoadStringById(IDS_PLANOVIEW_BTN004));	
	m_btnCancel.SetToolTipText(LoadStringById(IDS_PLANOVIEW_BTN005));


	UpdateData(FALSE);
}


void CPlanoView::OnBnClickedRadioViewmode()
{
	TraceLog(("OnBnClickedRadioViewmode"));

	m_btnShutdown.ShowWindow(SW_SHOW);
	m_btnStartup.ShowWindow(SW_SHOW);
	m_btnPowerMng.ShowWindow(SW_SHOW);
	m_sliderBright.ShowWindow(SW_SHOW);
	m_kbSelectAll.ShowWindow(SW_SHOW);
	//m_kbShowName.ShowWindow(SW_SHOW);
	m_btnSetBright.ShowWindow(SW_SHOW);

	this->GetDlgItem(IDC_STATIC_BRIGHT)->ShowWindow(SW_SHOW);
	this->GetDlgItem(IDC_STATIC_BRIGHT2)->ShowWindow(SW_SHOW);
	this->GetDlgItem(IDC_STATIC_BRIGHT3)->ShowWindow(SW_SHOW);

	m_btnSave.ShowWindow(SW_HIDE);
	m_btnCancel.ShowWindow(SW_HIDE);

	m_picCtrl.SetEditMode(false);
	UpdateData(FALSE);

	CString planoId;
	m_cbPlanoId.GetWindowText(planoId);	
	if(!planoId.IsEmpty()){
		OnBnClickedButtonHostRef();
	}

}

void CPlanoView::OnBnClickedRadioEditmode()
{
	TraceLog(("OnBnClickedRadioEditmode"));

	m_btnShutdown.ShowWindow(SW_HIDE);
	m_btnStartup.ShowWindow(SW_HIDE);
	m_btnPowerMng.ShowWindow(SW_HIDE);
	m_sliderBright.ShowWindow(SW_HIDE);
	m_kbSelectAll.ShowWindow(SW_HIDE);
	//m_kbShowName.ShowWindow(SW_HIDE);
	m_btnSetBright.ShowWindow(SW_HIDE);

	this->GetDlgItem(IDC_STATIC_BRIGHT)->ShowWindow(SW_HIDE);
	this->GetDlgItem(IDC_STATIC_BRIGHT2)->ShowWindow(SW_HIDE);
	this->GetDlgItem(IDC_STATIC_BRIGHT3)->ShowWindow(SW_HIDE);

	m_btnSave.ShowWindow(SW_SHOW);
	m_btnCancel.ShowWindow(SW_SHOW);

	m_picCtrl.SetEditMode(true);

	UpdateData(FALSE);

	CString planoId;
	m_cbPlanoId.GetWindowText(planoId);	
	if(!planoId.IsEmpty()){
		OnBnClickedButtonHostRef();
	}
}

int CPlanoView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CPlanoView::OnSize(UINT nType, int cx, int cy)
{
	TraceLog(("OnSize(%d,%d)", cx,cy));
	CFormView::OnSize(nType, cx, cy);
}

void CPlanoView::OnDestroy()
{
	TraceLog(("CPlanoView::OnDestroy"));
	CFormView::OnDestroy();
}

void CPlanoView::OnBnClickedButtonHostRef()
{
	CString planoId;
	m_cbPlanoId.GetWindowText(planoId);	


	if(planoId.IsEmpty()){
		UbcMessageBox(LoadStringById(IDS_PLANOVIEW_STR003));
		return;
	}
	// 해당 plano를 다시한번 가져온다.
	CCopModule::GetObject()->GetPlanoList(planoId, NULL, m_planoList, true);

	SPlanoInfo* aPlanoInfo;
	POSITION pos = m_planoList.GetHeadPosition();
	while(pos)
	{
		aPlanoInfo = m_planoList.GetNext(pos);
		if(aPlanoInfo->planoId == planoId){
			break;
		}
	}

	if(aPlanoInfo->planoType==CPlanoControl::e_PLANOTYPE_LIGHT){
		this->m_stBright.SetWindowText(LoadStringById(IDS_PLANOVIEW_STR005));
	}else{
		this->m_stBright.SetWindowText(LoadStringById(IDS_PLANOVIEW_STR004));
	}

	int admin = m_cbAdmin.GetCurSel();
 	int oper = m_cbOperation.GetCurSel();

	if(admin==0) admin = -1;
	else if(admin == 1) admin = 1;
	else if(admin == 2) admin = 0;
	if(oper==0) oper = -1;
	else if(oper == 1) oper = 1;
	else if(oper == 2) oper = 0;



	bool bRet = m_picCtrl.Load(aPlanoInfo, oper, admin);
	this->RedrawWindow();

	return ;
}



void CPlanoView::OnDraw(CDC* pDC)
{
	TraceLog(("OnDraw"));
	this->m_picCtrl.ShowWindow(SW_SHOW);
	CFormView::OnDraw(pDC);
}


BOOL CPlanoView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CFormView::PreTranslateMessage(pMsg);
}

void CPlanoView::OnBnClickedButtonSave()
{
	if(this->m_picCtrl.Save()){
		CString msg;
		msg.Format(LoadStringById(IDS_PLANOVIEW_STR012), this->m_picCtrl.GetInfo()->planoId);
		UbcMessageBox(msg);
		return;
	}
}

void CPlanoView::OnBnClickedButtonCancel()
{
	CString planoId;
	m_cbPlanoId.GetWindowText(planoId);	
	if(!planoId.IsEmpty()){
		OnBnClickedButtonHostRef();
	}
}

void CPlanoView::OnBnClickedCheckShowname()
{
	this->m_picCtrl.SetShowInfo(this->m_kbShowName.GetCheck());
}

void CPlanoView::OnBnClickedCheckSelectall()
{
	bool select = this->m_kbSelectAll.GetCheck();

	this->m_picCtrl.SelectAll(select);
}

void CPlanoView::OnBnClickedButtonPoweron()
{
	this->m_picCtrl.Poweron();
}

void CPlanoView::OnBnClickedButtonPoweroff()
{
	this->m_picCtrl.Poweroff();
}

void CPlanoView::OnBnClickedButtonPowerset()
{
	this->m_picCtrl.Powerset();
}

void CPlanoView::OnBnClickedButtonSetbright()
{
	this->m_picCtrl.Brightset(this->m_sliderBright.GetPos());
}

LRESULT CPlanoView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	TraceLog(("CPlanoView::InvokeEvent begin"));

	HWND hWnd = GetSafeHwnd();
	TraceLog(("hWnd [%X]", hWnd));
	if(::IsWindow(hWnd) == FALSE) return 0;

	switch(wParam)
	{
	case IEH_OPSTAT: 
		{
// cli >>> post PM=2/Site=SQI/Host=SQI-06112 operationalStateChanged siteId="SQI", hostId="SQI-06116", operationalState=true
			SOpStat* pOpStat = (SOpStat*)lParam;
			int retval =  this->m_picCtrl.ChangeOPStat((const char*)pOpStat->siteId, (const char*)pOpStat->hostId, pOpStat->operationalState);
			if(retval > 0) this->RedrawWindow();
			return retval;
		}
	case IEH_DEVICE_OPSTAT: 
		{
			SDeviceOpStat* pOpStat = (SDeviceOpStat*)lParam;
			int retval =  this->m_picCtrl.ChangeDeviceOPStat((const char*)pOpStat->siteId, (const char*)pOpStat->hostId, 
				pOpStat->objectClass,pOpStat->objectId,pOpStat->operationalState);
			if(retval > 0) this->RedrawWindow();
			return retval;
		}
/*
	case IEH_ADSTAT:
		return ChangeAdStat((SAdStat*)lParam);

	case IEH_UPPACKAGE:
		return UpdatePackage((SUpPackage*)lParam);

	case IEH_CHNGVNC:
		return ChangeVncStat((cciEvent*)lParam);

		// 0000707: 콘텐츠 다운로드 상태 조회 기능
	case IEH_CHNGDOWN:
		return ChangeDownloadStat((SDownloadStateChange*)lParam);

		// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	case IEH_MOSTAT:
		return ChangeMonitorStat((SMonitorStat*)lParam);

	case IEH_HDDCHNG:
		return ChangeDiskAvail((SDiskAvailChanged*)lParam);
	*/
	case IEH_UNKOWN:
		//cciAttributeList* pList = (cciAttributeList*)lParam;

		//for(int i = 0; i < (int)pList->length(); i++){
		//	cciAttribute& attr = (*pList)[i];

		//	if(strcmp(attr.getName(),"siteId") == 0){
		//		attr.getValue(strSite);
		//	}
		//}
		break;
	}
	return 0;
}
/*
void CPlanoView::OnBnClickedCheckShowlist()
{
	if(this->m_kbShowList.GetCheck()){
		this->m_lscNode.ShowWindow(SW_SHOW);
	}else{
		this->m_lscNode.ShowWindow(SW_HIDE);
	}
}
*/

