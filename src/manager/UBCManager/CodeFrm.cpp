// CodeFrm.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "CodeFrm.h"
#include "Enviroment.h"


// CCodeFrm

IMPLEMENT_DYNCREATE(CCodeFrm, CMDIChildWnd)

CCodeFrm::CCodeFrm()
{
}

CCodeFrm::~CCodeFrm()
{
}


BEGIN_MESSAGE_MAP(CCodeFrm, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CCodeFrm message handlers

int CCodeFrm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDI_ANNOUNCE), false);

	return 0;
}

void CCodeFrm::OnClose()
{
	TraceLog(("CCodeFrm::OnClose begin"));

	CMDIChildWnd::OnClose();

	TraceLog(("CCodeFrm::OnClose end"));
}

