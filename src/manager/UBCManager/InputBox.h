#pragma once
#include "afxwin.h"


// CInputBox 대화 상자입니다.

class CInputBox : public CDialog
{
	DECLARE_DYNAMIC(CInputBox)

public:
	CInputBox(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CInputBox();

	// Input param
	CString m_strTitle;
	CString m_strInputText;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_INPUT_BOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
};
