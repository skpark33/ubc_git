#pragma once
#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"

#include "UBCCopCommon\CopModule.h"
// CLMOSolutionDetailDlg 대화 상자입니다.

class CLMOSolutionDetailDlg : public CDialog
{
	DECLARE_DYNAMIC(CLMOSolutionDetailDlg)

public:
	CLMOSolutionDetailDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLMOSolutionDetailDlg();

	void SetInfo(SLMOSolutionInfo& info) { m_lmoSolutionInfo = info; m_isSet = true;}
	void GetInfo(SLMOSolutionInfo& info) {info = m_lmoSolutionInfo;  }

	void InitData();
// 대화 상자 데이터입니다.
	enum { IDD = IDD_LMO_SOLUTION_DETAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CEdit m_edit_description;
	CEdit m_edit_tablename;
	CEdit m_edit_timeFieldname;
	CComboBox m_cb_logType;
	CEdit m_edit_duration;
	CComboBox m_cb_adminState;
	CSpinButtonCtrl m_spin_duration;

	SLMOSolutionInfo m_lmoSolutionInfo;
	bool	m_isSet;

};
