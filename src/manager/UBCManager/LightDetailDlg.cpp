// LightDetailDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LightDetailDlg.h"
#include "Enviroment.h"

#include "common\ubcdefine.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common\libcommon\utvUtil.h"
#include "common\UbcCode.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "PowerSettingByDay.h"

// CLightDetailDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLightDetailDlg, CDialog)

CLightDetailDlg::CLightDetailDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLightDetailDlg::IDD, pParent)
{
	m_bShutdown = false;
	m_strWeekShutdownTime.Empty();
	m_bInit = false;

}

CLightDetailDlg::~CLightDetailDlg()
{
}

void CLightDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_NAME, m_editLightName);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_cbLightType);
	DDX_Control(pDX, IDC_EDIT_ID, m_editLightId);
	DDX_Control(pDX, IDC_EDIT_DESC, m_editDesc);
	DDX_Control(pDX, IDC_SLIDER_VALUE, m_sliderBright);
	DDX_Control(pDX, IDC_MONTHCALENDAR, m_ctrCalendar);
	DDX_Control(pDX, IDC_LIST_HOLIDAY, m_lscHoliday);
	DDX_Control(pDX, IDC_CHECK_SHUTDOWN, m_ckShutdown);
	DDX_Control(pDX, IDC_CHECK_POWERON, m_ckStartup);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN, m_dtShutdown);
	DDX_Control(pDX, IDC_TIME_POWERON, m_dtStartup);
	DDX_Control(pDX, IDC_CHECK_WEEK0, m_ckWeek[0]);
	DDX_Control(pDX, IDC_CHECK_WEEK1, m_ckWeek[1]);
	DDX_Control(pDX, IDC_CHECK_WEEK2, m_ckWeek[2]);
	DDX_Control(pDX, IDC_CHECK_WEEK3, m_ckWeek[3]);
	DDX_Control(pDX, IDC_CHECK_WEEK4, m_ckWeek[4]);
	DDX_Control(pDX, IDC_CHECK_WEEK5, m_ckWeek[5]);
	DDX_Control(pDX, IDC_CHECK_WEEK6, m_ckWeek[6]);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_bnAddHoliday);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_bnDelHoliday);

	DDX_Control(pDX, IDC_BUTTON_USTATE, m_btUState);
	DDX_Control(pDX, IDC_STATIC_TSTATE, m_stcTState);
}


BEGIN_MESSAGE_MAP(CLightDetailDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CLightDetailDlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CLightDetailDlg::OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_BN_POWER_SET_BY_DAY, &CLightDetailDlg::OnBnClickedBnPowerSetByDay)
	ON_BN_CLICKED(IDOK, &CLightDetailDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CLightDetailDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK_SHUTDOWN, &CLightDetailDlg::OnBnClickedCheckShutdown)
	ON_BN_CLICKED(IDC_CHECK_POWERON, &CLightDetailDlg::OnBnClickedCheckPoweron)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_USTATE, &CLightDetailDlg::OnBnClickedButtonUstate)
END_MESSAGE_MAP()


// CLightDetailDlg 메시지 처리기입니다.

BOOL CLightDetailDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bInit = true;

	m_editLightName.SetWindowText(m_LightInfo.lightName);
	m_editLightId.SetWindowTextA(m_LightInfo.lightId);
	m_editDesc.SetWindowText(m_LightInfo.description);

	m_strShutdownTime = m_LightInfo.shutdownTime;
	m_strWeekShutdownTime = m_LightInfo.weekShutdownTime;

	m_sliderBright.SetRange(0,100);
	m_sliderBright.SetPos(m_LightInfo.brightness);

	CUbcCode::GetInstance()->FillComboBoxCtrl(m_cbLightType, _T("LightType"));
	m_cbLightType.SetCurSel(CUbcCode::GetInstance()->FindComboBoxCtrl(m_cbLightType, m_LightInfo.lightType));

	if(m_LightInfo.adminState)
		m_btUState.LoadBitmap(IDB_BUTTON_USE, RGB(12,12,12));
	else
		m_btUState.LoadBitmap(IDB_BUTTON_NOTUSE, RGB(12,12,12));

	m_bmTState.DeleteObject();
	if(m_LightInfo.operationalState){
		m_bmTState.LoadBitmap(IDB_ON);
		m_stcTState.SetBitmap(m_bmTState);
	}else{
		m_bmTState.LoadBitmap(IDB_OFF);
		m_stcTState.SetBitmap(m_bmTState);
	}


	m_dtStartup.SetFormat(" HH : mm");
	if(m_LightInfo.startupTime.GetLength() != 5 || m_LightInfo.startupTime == "NOSET"){
		m_ckStartup.SetCheck(false);
	}else{
		CTime tmStartup(2000, 1, 1,  _ttoi(m_LightInfo.startupTime.Left(2)),  _ttoi(m_LightInfo.startupTime.Right(2)), 0);
		m_dtStartup.SetTime(&tmStartup);
		InitDateRange(m_dtStartup);
		m_ckStartup.SetCheck(true);
	}


	m_bnAddHoliday.LoadBitmap(IDB_BUTTON_NEXT2, RGB(255,255,255));
	m_bnDelHoliday.LoadBitmap(IDB_BUTTON_PREV2, RGB(255,255,255));

	m_lscHoliday.SetExtendedStyle(m_lscHoliday.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lscHoliday.InsertColumn(0, LoadStringById(IDS_HOLLIDAYDLG_LST001), LVCFMT_CENTER, 120);
	m_lscHoliday.InitHeader(IDB_LIST_HEADER);
	this->SetHolliday(m_LightInfo.holiday);

	m_dtShutdown.SetFormat(" HH : mm");
	m_dtStartup.SetFormat(" HH : mm");

	InitHolidayInfo();

	GetDlgItem(IDOK)->EnableWindow(IsAuth(_T("HMOD")));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CLightDetailDlg::InitHolidayInfo()
{
	bool bShutdown = false;
	if(m_strShutdownTime != ""){
		int iStart = 0;
		CString szToken = m_strShutdownTime.Tokenize(EH_SEPARATOR, iStart);
		while(!szToken.IsEmpty()){
			if(szToken.Find(":") >= 0){
				int iPos = 0;
				CString szHour = szToken.Tokenize(":", iPos);
				CString szMin = szToken.Tokenize(":", iPos);
				CTime tmShut(2000, 1, 1, atoi(szHour), atoi(szMin), 0);
				m_dtShutdown.SetTime(&tmShut);
				InitDateRange(m_dtShutdown);
				bShutdown = true;
			}
			else if(szToken.Find(".") >=0){
				AddHolidayItem(szToken);
			}
			else if(szToken.GetLength() == 1){
				int n = atoi(szToken);
				if(0 <= n && n <= 6)
					m_ckWeek[n].SetCheck(true);
			}
			szToken = m_strShutdownTime.Tokenize(EH_SEPARATOR, iStart);
		}
	}

	m_ckShutdown.SetCheck(bShutdown);
	//if(!bShutdown){
	//	m_dtShutdown.EnableWindow(FALSE);
	//}
}


void CLightDetailDlg::OnBnClickedButtonAdd()
{
	SYSTEMTIME st;
	m_ctrCalendar.GetCurSel(&st);

	CString str;
	str.Format("%d.%02d", st.wMonth, st.wDay);

	for(int nRow = 0; nRow < m_lscHoliday.GetItemCount(); nRow++){
		CString szBuf = m_lscHoliday.GetItemText(nRow, 0);
		if(szBuf == str)	return;
	}

	AddHolidayItem(str);
}
void CLightDetailDlg::AddHolidayItem(CString str)
{
	int nRow = m_lscHoliday.GetItemCount();
	m_lscHoliday.InsertItem(nRow, str);
	m_lscHoliday.SortItems(CompareSite, (DWORD_PTR)&m_lscHoliday);
}

int CLightDetailDlg::CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	LVFINDINFO lvFind;
	lvFind.flags = LVFI_PARAM;

	CListCtrl* pListCtrl = (CListCtrl*)lParam;
	if(!pListCtrl) return 0;

	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	int nCol = 0;
	CString strItem1 = pListCtrl->GetItemText(nIndex1, nCol);
	CString strItem2 = pListCtrl->GetItemText(nIndex2, nCol);

	return strItem1.Compare(strItem2);
}

void CLightDetailDlg::SetHolliday(LPCTSTR szHolliday)
{
	m_strShutdownTime = szHolliday?szHolliday:"";
}

LPCTSTR CLightDetailDlg::GetHolliday(CString& szHolliday)
{
	return (szHolliday = m_strShutdownTime);
}

void CLightDetailDlg::OnBnClickedButtonDel()
{
	POSITION pos = m_lscHoliday.GetFirstSelectedItemPosition();
	while(pos){
		int nRow = m_lscHoliday.GetNextSelectedItem(pos);
		m_lscHoliday.DeleteItem(nRow);
	}
}

void CLightDetailDlg::OnBnClickedBnPowerSetByDay()
{
	CPowerSettingByDay dlg;
	dlg.m_strWeekShutdownTime = m_strWeekShutdownTime;
	if(dlg.DoModal() != IDOK) return;

	m_strWeekShutdownTime = dlg.m_strWeekShutdownTime;
}
void CLightDetailDlg::OnBnClickedCheckShutdown()
{
	m_dtShutdown.EnableWindow(m_ckShutdown.GetCheck());
}

void CLightDetailDlg::OnBnClickedCheckPoweron()
{
	m_dtStartup.EnableWindow(m_ckStartup.GetCheck());
}

void CLightDetailDlg::OnBnClickedButtonUstate()
{
	m_LightInfo.adminState = !m_LightInfo.adminState;
	if(m_LightInfo.adminState)
		m_btUState.LoadBitmap(IDB_BUTTON_USE, RGB(12,12,12));
	else
		m_btUState.LoadBitmap(IDB_BUTTON_NOTUSE, RGB(12,12,12));
}

void CLightDetailDlg::OnBnClickedOk()
{
	UpdateData();

	if(m_cbLightType.GetCount() > 0 && m_cbLightType.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR011));
		return;
	}

	m_LightInfo.lightType = (long)m_cbLightType.GetItemData(m_cbLightType.GetCurSel());

	this->m_editDesc.GetWindowText(m_LightInfo.description);
	this->m_editLightName.GetWindowText(m_LightInfo.lightName);
	m_LightInfo.brightness = this->m_sliderBright.GetPos();

	m_bStartup = m_ckStartup.GetCheck();
	if(m_ckStartup.GetCheck()){
		m_dtStartup.GetTime(m_tmStartup);
		m_strStartupTime.Format("%02d:%02d", m_tmStartup.GetHour(), m_tmStartup.GetMinute());
	}else{
		m_strStartupTime = "NOSET";
	}
	m_LightInfo.startupTime = m_strStartupTime;

	m_bShutdown = m_ckShutdown.GetCheck();
	if(m_ckShutdown.GetCheck()){
		m_dtShutdown.GetTime(m_tmShutdown);
		m_strShutdownTime.Format("%02d:%02d", m_tmShutdown.GetHour(), m_tmShutdown.GetMinute());
	}else{
		m_strShutdownTime = "NOSET";
	}

	for(int nRow = 0; nRow < m_lscHoliday.GetItemCount(); nRow++){
		CString szBuf = m_lscHoliday.GetItemText(nRow, 0);
		m_strShutdownTime += EH_SEPARATOR;
		m_strShutdownTime += szBuf;
	}

	for(int i = 0; i < 7; i++){
		if(m_ckWeek[i].GetCheck()){
			m_strShutdownTime += EH_SEPARATOR;
			m_strShutdownTime += ToString(i);
		}
	}
	m_LightInfo.shutdownTime = m_strShutdownTime;

	m_LightInfo.weekShutdownTime = m_strWeekShutdownTime;
	this->GetHolliday(m_LightInfo.holiday);


	OnOK();
}

void CLightDetailDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
void CLightDetailDlg::SetInfo(SLightInfo& info)
{
	LightInfoList lsLight;
	CCopModule::GetObject()->GetLightList(info.siteId, info.lightId, NULL, true, false,lsLight);
	POSITION pos = lsLight.GetHeadPosition();
	if(pos){
		m_LightInfo = lsLight.GetNext(pos);
	}else{
		m_LightInfo = info;
	}
}

void CLightDetailDlg::GetInfo(SLightInfo& info)
{
	info = m_LightInfo;
}


void CLightDetailDlg::OnDestroy()
{
	CDialog::OnDestroy();

	m_bInit = false;
	m_bmTState.DeleteObject();
}

void CLightDetailDlg::OnClose()
{
	m_bInit = false;
	m_bmTState.DeleteObject();

	CDialog::OnClose();
}

