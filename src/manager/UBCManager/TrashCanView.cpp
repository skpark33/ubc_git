// TrashCanView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "TrashCanView.h"
#include "common\ubcdefine.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "ubccopcommon\SelectHostDlg.h"
#include "ubccopcommon\PackageSelectDlg.h"
#include "HostExcelSave.h"
#include "UBCCopCommon\UbcMenuAuth.h"


// CTrashCanView

IMPLEMENT_DYNCREATE(CTrashCanView, CFormView)

CTrashCanView::CTrashCanView()
:	CFormView(CTrashCanView::IDD)
,	m_Reposition(this)
{

}

CTrashCanView::~CTrashCanView()
{
	POSITION pos = m_mapCanBrowser.GetStartPosition();
	while( pos != NULL )
	{
		CString key;
		CCanBrowser* pCanBrowser = NULL;
		m_mapCanBrowser.GetNextAssoc( pos, key, (void*&)pCanBrowser );
		if(pCanBrowser) delete pCanBrowser;
	}
}

void CTrashCanView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_btnRefresh);
	DDX_Control(pDX, IDC_EDIT_FILTER_HOST, m_editObjectName);
	DDX_Control(pDX, IDC_DATE_FILTER_START, m_dtcFilterStart);
	DDX_Control(pDX, IDC_DATE_FILTER_END, m_dtcFilterEnd);
	DDX_Control(pDX, IDC_BN_TO_EXCEL, m_btnExcelSave);
	DDX_Control(pDX, IDC_TRASHCAN_TAB, m_tcLogTab);
	DDX_Control(pDX, IDC_LIST_HOSTCAN, m_lcLog[0]);
	DDX_Control(pDX, IDC_LIST_SITECAN, m_lcLog[1]);
	DDX_Control(pDX, IDC_LIST_BPCAN, m_lcLog[2]);
	DDX_Control(pDX, IDC_LIST_PACKAGECAN, m_lcLog[3]);
	DDX_Control(pDX, IDC_LIST_USERCAN, m_lcLog[4]);

	DDX_Control(pDX, IDC_STATIC_HOSTLABEL, m_labelFilterObjectName);

	DDX_Control(pDX, IDC_BN_TO_RECOVERY, m_btRecovery);
	DDX_Control(pDX, IDC_BN_TO_ELIMINATE, m_btEliminate);
}

BEGIN_MESSAGE_MAP(CTrashCanView, CFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CTrashCanView::OnBnClickedBtnRefresh)
	ON_BN_CLICKED(IDC_BN_TO_EXCEL, &CTrashCanView::OnBnClickedBtnToExcel)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TRASHCAN_TAB, &CTrashCanView::OnTcnSelchangeTrashCanTab)
	ON_MESSAGE(WM_TRASHCAN_TAB, OnChangeTab)
	ON_BN_CLICKED(IDC_BN_TO_RECOVERY, &CTrashCanView::OnBnClickedBnToRecovery)
	ON_BN_CLICKED(IDC_BN_TO_ELIMINATE, &CTrashCanView::OnBnClickedBnToEliminate)
END_MESSAGE_MAP()


// CTrashCanView 진단입니다.

#ifdef _DEBUG
void CTrashCanView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CTrashCanView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


int CTrashCanView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}

void CTrashCanView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if(GetSafeHwnd())
	{
		m_Reposition.Move();
	}
}

void CTrashCanView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

    CSize scrollSize(0,0);
    SetScrollSizes(MM_TEXT, scrollSize);

	m_btnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnExcelSave.LoadBitmap(IDB_BUTTON_EXCEL, RGB(255,255,255));
	m_btRecovery.LoadBitmap(IDB_BTN_RECOVERY, RGB(255,255,255));
	m_btEliminate.LoadBitmap(IDB_BTN_ELIMINATE, RGB(255,255,255));

	m_btnRefresh.SetToolTipText(LoadStringById(IDS_PACKAGE_CHNG_LOG_BTN001));
	m_btnExcelSave.SetToolTipText(LoadStringById(IDS_PACKAGE_CHNG_LOG_BTN002));
	m_btRecovery.SetToolTipText(LoadStringById(IDS_TRASHCAN_BTN001));
	m_btEliminate.SetToolTipText(LoadStringById(IDS_TRASHCAN_BTN002));

	CTime tmCur = CTime::GetCurrentTime();
	m_dtcFilterEnd.SetTime(&tmCur);
	InitDateRange(m_dtcFilterEnd);

	CTime tmStartDate = tmCur - CTimeSpan(7, 0, 0, 0);
	m_dtcFilterStart.SetTime(&tmStartDate);
	InitDateRange(m_dtcFilterStart);

	m_btRecovery.EnableWindow(IsAuth(_T("PREG")));
	if(GetEnvPtr()->m_szLoginID == "super" || GetEnvPtr()->m_Authority != CCopModule::eSiteAdmin ) {
		m_btEliminate.EnableWindow(IsAuth(_T("PDEL")));
	}else{
		m_btEliminate.EnableWindow(false);
	}
	m_btnRefresh.EnableWindow(IsAuth(_T("PQRY")));



	InitTab();

	m_Reposition.SetParentRect(m_rcClient);
	m_Reposition.RemoveAll();
	m_Reposition.AddControl(&m_tcLogTab, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	for(int i=0; i<eMaxTab; i++)
		m_Reposition.AddControl(&m_lcLog[i], REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}

void CTrashCanView::InitTab()
{
	CCanBrowser* pLB = NULL;

	TCITEM tci = {0};
	tci.mask = TCIF_PARAM;

	// 단말 휴지통
	m_tcLogTab.InsertItem(eHostCan, LoadStringById(IDS_TRASHCAN_STR001), eHostCan);
	pLB = (CCanBrowser*)new CHostCanBrowser(&m_lcLog[0]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eHostCan, &tci);

	// 조직 휴지통
	m_tcLogTab.InsertItem(eSiteCan, LoadStringById(IDS_TRASHCAN_STR002), eSiteCan);
	pLB = (CCanBrowser*)new CSiteCanBrowser(&m_lcLog[1]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eSiteCan, &tci);

	// 방송계획 휴지통
	m_tcLogTab.InsertItem(eBPCan, LoadStringById(IDS_TRASHCAN_STR003), eBPCan);
	pLB = (CCanBrowser*)new CBPCanBrowser(&m_lcLog[2]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eBPCan, &tci);

	// 콘텐츠 패키지 휴지통
	m_tcLogTab.InsertItem(ePackageCan, LoadStringById(IDS_TRASHCAN_STR004), ePackageCan);
	pLB = (CCanBrowser*)new CPackageCanBrowser(&m_lcLog[3]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(ePackageCan, &tci);

	// 유저 휴지통
	m_tcLogTab.InsertItem(eUserCan, LoadStringById(IDS_TRASHCAN_STR005), eUserCan);
	pLB = (CCanBrowser*)new CUserCanBrowser(&m_lcLog[eUserCan]);
	pLB->InitList();
	tci.lParam = (LPARAM)pLB;
	m_tcLogTab.SetItem(eUserCan, &tci);

}

void CTrashCanView::SetCurrentTab(int nTabIdx)
{
	m_tcLogTab.SetCurSel(nTabIdx);

	for(int i=0; i<eMaxTab; i++)
	{
		m_lcLog[i].ShowWindow( (nTabIdx == i) ? SW_NORMAL : SW_HIDE );

	}
	TCITEM tci;
	tci.mask = TCIF_PARAM;
	m_tcLogTab.GetItem(nTabIdx, &tci);
	CCanBrowser* pLB = (CCanBrowser*)tci.lParam;
	if(pLB)
	{
		pLB->VisibleFilter(this);
	}

}

void CTrashCanView::RefreshList()
{
	int tab_idx = m_tcLogTab.GetCurSel();
	if(tab_idx<0 || tab_idx>=eMaxTab) return;

	TCITEM tci;
	tci.mask = TCIF_PARAM;
	m_tcLogTab.GetItem(tab_idx, &tci);

	CCanBrowser* pLB = (CCanBrowser*)tci.lParam;
	if(pLB)
	{
		CTime tm_start, tm_end;
		m_dtcFilterStart.GetTime(tm_start);
		m_dtcFilterEnd.GetTime(tm_end);

		CString filterName;
		m_editObjectName.GetWindowText(filterName);

		pLB->SetFilter(filterName, tm_start, tm_end);
		pLB->RefreshList();
	}
}

void CTrashCanView::OnBnClickedBtnRefresh()
{
	RefreshList();
}


void CTrashCanView::OnBnClickedBtnToExcel()
{
	CHostExcelSave dlg;

	int tab_idx = m_tcLogTab.GetCurSel();
	if(tab_idx<0 || tab_idx>=eMaxTab) return;

	char title[256] = {0};
	TCITEM tci;
	tci.pszText = title;
	tci.cchTextMax = 256;
	tci.mask = TCIF_TEXT;
	m_tcLogTab.GetItem(tab_idx, &tci);

	// 슬래시(/)문자 치환 -> 엑셀에서 사용시 에러
	CString str_title = title;
	str_title.Replace("/", ",");

	CString strExcelFile = dlg.Save(str_title, m_lcLog[tab_idx]);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}

void CTrashCanView::OnTcnSelchangeTrashCanTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;
	SetCurrentTab(m_tcLogTab.GetCurSel());
}

LRESULT CTrashCanView::OnChangeTab(WPARAM wParam, LPARAM lParam)
{
	SetCurrentTab(wParam);
	return 0;
}

////////////////////////////////
// CCanBrowser
////////////////////////////////
void CCanBrowser::InitList()
{
	m_pCanListCtrl->SetExtendedStyle(m_pCanListCtrl->GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);

	m_CanColumn[eCheck		] = _T("");
	m_CanColumn[eSite		]	= LoadStringById(IDS_PRIMARYLOGVIEW_LIST001);
	m_CanColumn[eObjId			]	= LoadStringById(IDS_TRASHCAN_LIST002);
	m_CanColumn[eName		]	= LoadStringById(IDS_TRASHCAN_LIST003);
	m_CanColumn[eTrashTime	]	= LoadStringById(IDS_TRASHCAN_LIST004);
	m_CanColumn[eTrashId	]	= LoadStringById(IDS_TRASHCAN_LIST005);
	m_CanColumn[eEntity		]	= _T("entity");
	m_CanColumn[eAttrList	]	= _T("attributes");
	m_CanColumn[eId	]			= _T("id");

	m_pCanListCtrl->InsertColumn(eCheck		, m_CanColumn[eCheck    ], LVCFMT_LEFT  , 22);
	m_pCanListCtrl->InsertColumn(eSite		, m_CanColumn[eSite     ], LVCFMT_LEFT  , 100);
	m_pCanListCtrl->InsertColumn(eObjId		, m_CanColumn[eObjId		], LVCFMT_LEFT  , 200);
	m_pCanListCtrl->InsertColumn(eName		, m_CanColumn[eName		], LVCFMT_LEFT  , 200);
	m_pCanListCtrl->InsertColumn(eTrashTime , m_CanColumn[eTrashTime], LVCFMT_LEFT  , 120);
	m_pCanListCtrl->InsertColumn(eTrashId	, m_CanColumn[eTrashId	], LVCFMT_LEFT  , 100);
	m_pCanListCtrl->InsertColumn(eEntity	, m_CanColumn[eEntity	], LVCFMT_LEFT  , 200);
	m_pCanListCtrl->InsertColumn(eAttrList	, m_CanColumn[eAttrList	], LVCFMT_LEFT  , 400);
	m_pCanListCtrl->InsertColumn(eId	, m_CanColumn[eId	], LVCFMT_LEFT  , 100);

	m_pCanListCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CCanBrowser::RefreshList()
{
	CWaitMessageBox wait;

	m_pCanListCtrl->DeleteAllItems();
	m_CanDataList.RemoveAll();

	// 작성일자
	CString strStartDate = m_tmStart.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = m_tmEnd.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");

	CString strWhere;
	strWhere.Format(_T("eventTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);
	//strWhere += _T(" order by eventTime desc");
	strWhere.Trim();

	CString loginSite=GetEnvPtr()->m_szSite;
	CString loginUser=GetEnvPtr()->m_szLoginID;

	m_CanDataList.RemoveAll();
	TraceLog(("GetCanInfoList(%s,%s,%s,%s,%s)", m_strObjectName,m_strFilterName, loginSite,loginUser, strWhere ));
	if(!CCopModule::GetObject()->GetCanInfoList(m_strObjectName,m_strFilterName, loginSite,loginUser,strWhere, m_CanDataList))
	{
		UbcMessageBox(IDS_ICONVIEW_STR010);
		return;
	}

	m_pCanListCtrl->SetRedraw(FALSE);

	POSITION pos = m_CanDataList.GetHeadPosition();
	for( int nRow=0; pos!=NULL; nRow++ )
	{
		POSITION posOld = pos;
		STrashCanInfo& info = m_CanDataList.GetNext(pos);

		CTime tmTrashTime = CTime(info.eventTime);

		m_pCanListCtrl->InsertItem(nRow, "");
		m_pCanListCtrl->SetItemText(nRow, eSite     , info.siteId   );
		m_pCanListCtrl->SetItemText(nRow, eObjId		, info.idValue   );
		m_pCanListCtrl->SetItemText(nRow, eName		, info.nameValue);
		m_pCanListCtrl->SetItemText(nRow, eTrashTime, (info.eventTime == 0 ? "" : tmTrashTime.Format("%Y/%m/%d %H:%M:%S")));
		m_pCanListCtrl->SetItemText(nRow, eTrashId	, info.userId      );
		m_pCanListCtrl->SetItemText(nRow, eEntity	, info.entity      );
		m_pCanListCtrl->SetItemText(nRow, eAttrList	, info.attrList      );
		m_pCanListCtrl->SetItemText(nRow, eId	, info.trashCanId      );

		m_pCanListCtrl->SetItemData(nRow, (DWORD_PTR)posOld);
	}

	m_pCanListCtrl->SetRedraw(TRUE);
}



void CTrashCanView::OnBnClickedBnToRecovery()
{
	int tab_idx = m_tcLogTab.GetCurSel();
	if(tab_idx<0 || tab_idx>=eMaxTab) return;

	TCITEM tci;
	tci.mask = TCIF_PARAM;
	m_tcLogTab.GetItem(tab_idx, &tci);

	CCanBrowser* pLB = (CCanBrowser*)tci.lParam;
	if(pLB)
	{
		pLB->Recovery();
	}
}

void CTrashCanView::OnBnClickedBnToEliminate()
{
	// 선택한 콘텐츠패키지를 삭제하시겠습니까?
	if(UbcMessageBox(LoadStringById(IDS_TRASHCAN_MSG001), MB_YESNO) != IDYES)
	{
		return;
	}

	int tab_idx = m_tcLogTab.GetCurSel();
	if(tab_idx<0 || tab_idx>=eMaxTab) return;

	TCITEM tci;
	tci.mask = TCIF_PARAM;
	m_tcLogTab.GetItem(tab_idx, &tci);

	CCanBrowser* pLB = (CCanBrowser*)tci.lParam;
	if(pLB)
	{
		pLB->Eliminate();
	}
}

int
CCanBrowser::Eliminate()
{
	TraceLog(("Eliminate()"));

	int counter=0;
	for(int nRow = m_pCanListCtrl->GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_pCanListCtrl->GetCheck(nRow)) continue;
		counter++;
	}
	if(counter==0) {
		UbcMessageBox(LoadStringById(IDS_TRASHCAN_MSG002), MB_ICONINFORMATION);
		return 0;
	}

	counter=0;
	for(int nRow = m_pCanListCtrl->GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_pCanListCtrl->GetCheck(nRow)) continue;

		CString szSite = m_pCanListCtrl->GetItemText(nRow, eSite);
		CString szTrashCanId = m_pCanListCtrl->GetItemText(nRow, eId);

		if(!CCopModule::GetObject()->CanEliminate(m_strObjectName, szSite, szTrashCanId))
		{
			UbcMessageBox(IDS_ICONVIEW_STR010);
			continue;
		}
		counter++;
	}
	CString strMsg;
	strMsg.Format(LoadStringById(IDS_TRASHCAN_MSG004), counter);
	UbcMessageBox(strMsg);
	RefreshList();
	return counter;
}

int
CCanBrowser::Recovery()
{
	TraceLog(("Recovery()"));

	int counter=0;
	for(int nRow = m_pCanListCtrl->GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_pCanListCtrl->GetCheck(nRow)) continue;
		counter++;
	}
	if(counter==0) {
		UbcMessageBox(LoadStringById(IDS_TRASHCAN_MSG002), MB_ICONINFORMATION);
		return 0;
	}

	counter=0;
	for(int nRow = m_pCanListCtrl->GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_pCanListCtrl->GetCheck(nRow)) continue;

		CString szSite = m_pCanListCtrl->GetItemText(nRow, eSite);
		CString szTrashCanId = m_pCanListCtrl->GetItemText(nRow, eId);

		if(!CCopModule::GetObject()->CanRecovery(m_strObjectName, szSite, szTrashCanId))
		{
			UbcMessageBox(IDS_ICONVIEW_STR010);
			continue;
		}
		counter++;
	}
	CString strMsg;
	strMsg.Format(LoadStringById(IDS_TRASHCAN_MSG003), counter);
	UbcMessageBox(strMsg);
	RefreshList();
	if(m_strObjectName == "Site") {
		strMsg.Format(LoadStringById(IDS_TRASHCAN_MSG005));
		UbcMessageBox(strMsg);
	}
	return counter;
}
