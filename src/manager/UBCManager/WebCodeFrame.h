#pragma once


// CWebCodeFrame frame

class CWebCodeFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CWebCodeFrame)
protected:
	CWebCodeFrame();           // protected constructor used by dynamic creation
	virtual ~CWebCodeFrame();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


