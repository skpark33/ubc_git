#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "SubBroadcastPlan.h"
#include "dp_daily_plan_table.h"
#include "Enviroment.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "afxdtctl.h"

// CPlanScheduling 대화 상자입니다.

class CPlanScheduling : public CSubBroadcastPlan
{
	DECLARE_DYNAMIC(CPlanScheduling)

public:
	CPlanScheduling(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPlanScheduling();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PLAN_SCHEDULING };
	enum { ePackage, eGroup, eDesc, eVolume, eMaxCol };

	bool IsModified();
	void UpdateInfo();
	bool InputErrorCheck(bool bMsg);
	void RefreshInfo();

	CUTBListCtrlEx			m_lcList;
	CDp_daily_plan_table	m_dpDailyPlanTable;
	CDateTimeCtrl			m_dtStart;
	CDateTimeCtrl			m_dtEnd;
	CSpinButtonCtrl			m_spRunningTime;

	CHoverButton			m_bnRefresh;
	CHoverButton			m_bnAdd;
	CHoverButton			m_bnDel;
	CHoverButton			m_bnCircleSelect;
	CHoverButton			m_bnZorderUP;
	CHoverButton			m_bnZorderDown;

	CString					m_szColum[eMaxCol];
	PackageInfoList			m_lsPackage;
	void					InitList();
	void					RefreshList();

	void					InitDailyTimeTable();

	CPoint					m_ptSelList;
	void					RunStudio(CString strPackage);
	void					InitTimeInfo(long nIndex);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBnRefresh();
	afx_msg void OnNMDblclkLcList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRclickLcList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnHostlistEditpackage();
	afx_msg void OnBnClickedBnAdd();
	afx_msg void OnBnClickedBnDel();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_EVENTSINK_MAP()
	void SelChangeDpDailyPlanTable(long nIndex);
	void InfoChangeDpDailyPlanTable(long nIndex, DATE tmStart, DATE tmEnd);
	afx_msg void OnBnClickedBnZorderUp();
	afx_msg void OnBnClickedBnCircleSelect();
	afx_msg void OnBnClickedBnZorderDn();
	afx_msg void OnDeltaposSpRunningtime(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDtnDatetimechangeDtEndtime(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDtnDatetimechangeDtStarttime(NMHDR *pNMHDR, LRESULT *pResult);
};
