// PowerSettingByDay.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "PowerSettingByDay.h"


// CPowerSettingByDay 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPowerSettingByDay, CDialog)

CPowerSettingByDay::CPowerSettingByDay(CWnd* pParent /*=NULL*/)
	: CDialog(CPowerSettingByDay::IDD, pParent)
{

}

CPowerSettingByDay::~CPowerSettingByDay()
{
}

void CPowerSettingByDay::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK0, m_tmShutdownWeek[0]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK1, m_tmShutdownWeek[1]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK2, m_tmShutdownWeek[2]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK3, m_tmShutdownWeek[3]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK4, m_tmShutdownWeek[4]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK5, m_tmShutdownWeek[5]);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN_WEEK6, m_tmShutdownWeek[6]);
	DDX_Control(pDX, IDC_CHECK_WEEK0, m_ckWeek[0]);
	DDX_Control(pDX, IDC_CHECK_WEEK1, m_ckWeek[1]);
	DDX_Control(pDX, IDC_CHECK_WEEK2, m_ckWeek[2]);
	DDX_Control(pDX, IDC_CHECK_WEEK3, m_ckWeek[3]);
	DDX_Control(pDX, IDC_CHECK_WEEK4, m_ckWeek[4]);
	DDX_Control(pDX, IDC_CHECK_WEEK5, m_ckWeek[5]);
	DDX_Control(pDX, IDC_CHECK_WEEK6, m_ckWeek[6]);
}


BEGIN_MESSAGE_MAP(CPowerSettingByDay, CDialog)
	ON_BN_CLICKED(IDOK, &CPowerSettingByDay::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPowerSettingByDay::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK_WEEK0, &CPowerSettingByDay::OnBnClickedCheckWeek0)
	ON_BN_CLICKED(IDC_CHECK_WEEK1, &CPowerSettingByDay::OnBnClickedCheckWeek1)
	ON_BN_CLICKED(IDC_CHECK_WEEK2, &CPowerSettingByDay::OnBnClickedCheckWeek2)
	ON_BN_CLICKED(IDC_CHECK_WEEK3, &CPowerSettingByDay::OnBnClickedCheckWeek3)
	ON_BN_CLICKED(IDC_CHECK_WEEK4, &CPowerSettingByDay::OnBnClickedCheckWeek4)
	ON_BN_CLICKED(IDC_CHECK_WEEK5, &CPowerSettingByDay::OnBnClickedCheckWeek5)
	ON_BN_CLICKED(IDC_CHECK_WEEK6, &CPowerSettingByDay::OnBnClickedCheckWeek6)
END_MESSAGE_MAP()


// CPowerSettingByDay 메시지 처리기입니다.

void CPowerSettingByDay::OnBnClickedOk()
{
	m_strWeekShutdownTime.Empty();

	for(int i=0; i<7 ;i++)
	{
		if(m_ckWeek[i].GetCheck())
		{
			CTime tmShut;
			m_tmShutdownWeek[i].GetTime(tmShut);
			CString strTmp;
			strTmp.Format("%d|%02d:%02d", i, tmShut.GetHour(), tmShut.GetMinute());
			m_strWeekShutdownTime += (m_strWeekShutdownTime.IsEmpty() ? _T("") : _T(",")) + strTmp;
		}
	}

	if(!m_strWeekShutdownTime.IsEmpty())
	{
		m_strWeekShutdownTime = _T("(") + m_strWeekShutdownTime + _T(")");
	}

	OnOK();
}

void CPowerSettingByDay::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

BOOL CPowerSettingByDay::OnInitDialog()
{
	CDialog::OnInitDialog();

	for(int i=0; i<7 ;i++)
	{
		m_tmShutdownWeek[i].SetFormat(" HH : mm");
	}

	m_strWeekShutdownTime.Replace(_T("("), _T(""));
	m_strWeekShutdownTime.Replace(_T(")"), _T(""));
	if(!m_strWeekShutdownTime.IsEmpty())
	{
		int iStart = 0;
		CString szToken;
		while(!(szToken = m_strWeekShutdownTime.Tokenize(_T(","), iStart)).IsEmpty())
		{
			if(szToken.Find(_T("|")) >= 0)
			{
				int nDayOfWeek = _ttoi(szToken.Left(szToken.Find(_T("|"))));

				szToken = szToken.Mid(szToken.Find(_T("|"))+1);

				if(szToken.Find(":") >= 0)
				{
					int iPos = 0;
					CTime tmShut(2000, 1, 1
							   , _ttoi(szToken.Left(szToken.Find(_T(":"))))
							   , _ttoi(szToken.Mid (szToken.Find(_T(":"))+1))
							   , 0
							   );

					m_tmShutdownWeek[nDayOfWeek].SetTime(&tmShut);
					m_ckWeek[nDayOfWeek].SetCheck(true);
				}
			}
		}
	}

	for(int i=0; i<7 ; i++)
	{
		m_tmShutdownWeek[i].EnableWindow(m_ckWeek[i].GetCheck());
		InitDateRange(m_tmShutdownWeek[i]);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPowerSettingByDay::InitShutdownWeek(int nIndex)
{
	m_tmShutdownWeek[nIndex].EnableWindow(m_ckWeek[nIndex].GetCheck());
	if(m_ckWeek[nIndex].GetCheck())
	{
//		CTime tmShut;
//		m_tmShutdown.GetTime(tmShut);
//		m_tmShutdownWeek[nIndex].SetTime(&tmShut);
	}
}

void CPowerSettingByDay::OnBnClickedCheckWeek0()
{
	InitShutdownWeek(0);
}

void CPowerSettingByDay::OnBnClickedCheckWeek1()
{
	InitShutdownWeek(1);
}

void CPowerSettingByDay::OnBnClickedCheckWeek2()
{
	InitShutdownWeek(2);
}

void CPowerSettingByDay::OnBnClickedCheckWeek3()
{
	InitShutdownWeek(3);
}

void CPowerSettingByDay::OnBnClickedCheckWeek4()
{
	InitShutdownWeek(4);
}

void CPowerSettingByDay::OnBnClickedCheckWeek5()
{
	InitShutdownWeek(5);
}

void CPowerSettingByDay::OnBnClickedCheckWeek6()
{
	InitShutdownWeek(6);
}
