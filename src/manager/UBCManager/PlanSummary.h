#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "SubBroadcastPlan.h"
#include "dp_daily_plan_table.h"
#include "common\utblistctrlex.h"

// CPlanSummary 대화 상자입니다.

class CPlanSummary : public CSubBroadcastPlan
{
	DECLARE_DYNAMIC(CPlanSummary)

public:
	CPlanSummary(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPlanSummary();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PLAN_SUMMARY };

	bool IsModified();
	void UpdateInfo();
	bool InputErrorCheck(bool bMsg);
	void RefreshInfo();

protected:
	CDp_daily_plan_table	m_dpDailyPlanTable;
	void					InitDailyTimeTable();

	CButton					m_kbWeek[7];
	CUTBListCtrlEx			m_lcExceptDay;
	CUTBListCtrlEx			m_lcView;
	void					RefreshPeriodInfo();
	void					InitExceptDayList();
	void					RefreshExceptDayList();

	void					InitViewList();
	void					RefreshViewList();

	CUTBListCtrlEx			m_lcSelHostList;
	void					InitSelHostList();
	void					RefreshSelHostList();

	CBrush					m_brush;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
