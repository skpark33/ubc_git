// PlanSummary.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "PlanSummary.h"


// CPlanSummary 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPlanSummary, CDialog)

CPlanSummary::CPlanSummary(CWnd* pParent /*=NULL*/)
	: CSubBroadcastPlan(CPlanSummary::IDD, pParent)
{

}

CPlanSummary::~CPlanSummary()
{
}

void CPlanSummary::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DP_DAILY_PLAN_TABLE, m_dpDailyPlanTable);
	DDX_Control(pDX, IDC_KB_WEEK0, m_kbWeek[0]);
	DDX_Control(pDX, IDC_KB_WEEK1, m_kbWeek[1]);
	DDX_Control(pDX, IDC_KB_WEEK2, m_kbWeek[2]);
	DDX_Control(pDX, IDC_KB_WEEK3, m_kbWeek[3]);
	DDX_Control(pDX, IDC_KB_WEEK4, m_kbWeek[4]);
	DDX_Control(pDX, IDC_KB_WEEK5, m_kbWeek[5]);
	DDX_Control(pDX, IDC_KB_WEEK6, m_kbWeek[6]);
	DDX_Control(pDX, IDC_LC_EXCEPTDAY, m_lcExceptDay);
	DDX_Control(pDX, IDC_LC_LIST, m_lcView);
	DDX_Control(pDX, IDC_LIST_SEL_HOST, m_lcSelHostList);
}


BEGIN_MESSAGE_MAP(CPlanSummary, CDialog)
	ON_BN_CLICKED(IDOK, &CPlanSummary::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPlanSummary::OnBnClickedCancel)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


void CPlanSummary::OnBnClickedOk() {}
void CPlanSummary::OnBnClickedCancel() {}
// CPlanSummary 메시지 처리기입니다.

bool CPlanSummary::IsModified()
{
	CString strDescription;
	GetDlgItemText(IDC_EB_DESCRIPTION, strDescription);

	return (GetBroadcastPlanInfo()->strDescription != strDescription);
}

void CPlanSummary::UpdateInfo()
{
	CString strDescription;
	GetDlgItemText(IDC_EB_DESCRIPTION, strDescription);

	GetBroadcastPlanInfo()->strDescription = strDescription;
}

bool CPlanSummary::InputErrorCheck(bool bMsg)
{
	return true;
}

void CPlanSummary::RefreshInfo()
{
	SetDlgItemText(IDC_EB_DESCRIPTION, GetBroadcastPlanInfo()->strDescription);

	InitDailyTimeTable();
	RefreshPeriodInfo();
	RefreshSelHostList();
}

BOOL CPlanSummary::OnInitDialog()
{
	CSubBroadcastPlan::OnInitDialog();

	m_brush.CreateSolidBrush(RGB(255,255,255));

	InitExceptDayList();
	InitViewList();
	InitSelHostList();

	RefreshInfo();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPlanSummary::InitDailyTimeTable()
{
	m_dpDailyPlanTable.SetEditable(false);

	m_dpDailyPlanTable.DeleteAllItem();

	POSITION pos = GetTimePlanInfoList()->GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STimePlanInfo& info = GetTimePlanInfoList()->GetNext(pos);

		if(info.nProcType == PROC_DELETE) continue;

		m_dpDailyPlanTable.AddItem(info.strProgramId
								 , NormalizeTime(_ttoi(info.strStartTime.Left(2)), _ttoi(info.strStartTime.Mid(3,2))).GetTime()
								 , NormalizeTime(_ttoi(info.strEndTime.Left(2)), _ttoi(info.strEndTime.Mid(3,2))).GetTime()
								 , (LONG*)posOld
								 );
	}

	m_dpDailyPlanTable.SetSelectedIndex(m_dpDailyPlanTable.GetItemCount()-1);
}

void CPlanSummary::RefreshPeriodInfo()
{
	SetDlgItemText(IDC_TXT_STARTDATE, GetBroadcastPlanInfo()->tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S")));
	SetDlgItemText(IDC_TXT_ENDDATE  , GetBroadcastPlanInfo()->tmEndDate  .Format(_T("%Y/%m/%d %H:%M:%S")));

	for(int i=0; i<7 ;i++)
	{
		CString strWeek;
		strWeek.Format(_T("%d"), i);
		m_kbWeek[i].SetCheck(GetBroadcastPlanInfo()->strWeekInfo.Find(strWeek) >= 0);
	}

	CString strDownloadTime;
	strDownloadTime.Format(LoadStringById(IDS_PLANPERIOD_STR001)//_T("%s 재배포(%d회/%d분)")
						, GetBroadcastPlanInfo()->tmDownloadTime.Format(_T("%Y/%m/%d %H:%M:%S"))
						, GetBroadcastPlanInfo()->nRetryCount
						, GetBroadcastPlanInfo()->nRetryGap
						);
	SetDlgItemText(IDC_TXT_DOWNLOAD_TIME, strDownloadTime);

	RefreshExceptDayList();
	RefreshViewList();
}

void CPlanSummary::InitExceptDayList()
{
	m_lcExceptDay.SetExtendedStyle(m_lcExceptDay.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lcExceptDay.InsertColumn(0, LoadStringById(IDS_PLANPERIOD_LIST001), LVCFMT_CENTER, 180);

	// 제일 마지막에 할것...
	m_lcExceptDay.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);

	m_lcExceptDay.SetSortEnable(true);
}

void CPlanSummary::RefreshExceptDayList()
{
	m_lcExceptDay.DeleteAllItems();

	CString strExceptDayList = GetBroadcastPlanInfo()->strExceptDay;

	int nRow = 0;
	int nLoopPos=0;
	CString strData;
	while( !(strData = strExceptDayList.Tokenize(_T(","), nLoopPos)).IsEmpty() )
	{
		strData.Replace(_T("."), _T("/"));

		m_lcExceptDay.InsertItem(nRow, strData);

		nRow++;
	}
}

void CPlanSummary::InitViewList()
{
	CTime tmStart = GetBroadcastPlanInfo()->tmStartDate;

	m_lcView.SetExtendedStyle(m_lcView.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lcView.InsertColumn(0, _T(""), LVCFMT_LEFT, 0);
	m_lcView.InsertColumn(1, _T(""), LVCFMT_LEFT, 500);

	// 제일 마지막에 할것...
	m_lcView.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcView.SetSortEnable(false);
	m_lcView.SetColType(1
					  , CUTBHeaderCtrl::COL_CALENDAR
					  , CTime(tmStart.GetYear(), tmStart.GetMonth(), tmStart.GetDay(), 0, 0, 0)
					  , 3
					  );

	m_lcView.InsertItem(0, _T(""));
	m_lcView.SetItemText(0,1, _T(""));
}

void CPlanSummary::RefreshViewList()
{
	CString strDateList;
	strDateList.Format(_T("DateList=%s~%s,|WeekInfo=%s|ExceptDay=%s|")
					 , GetBroadcastPlanInfo()->tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S"))
					 , GetBroadcastPlanInfo()->tmEndDate.Format(_T("%Y/%m/%d %H:%M:%S"))
					 , GetBroadcastPlanInfo()->strWeekInfo
					 , GetBroadcastPlanInfo()->strExceptDay
					 );

	m_lcView.SetColType(1
					  , CUTBHeaderCtrl::COL_CALENDAR
					  , CTime(GetBroadcastPlanInfo()->tmStartDate.GetYear(), GetBroadcastPlanInfo()->tmStartDate.GetMonth(), GetBroadcastPlanInfo()->tmStartDate.GetDay(), 0, 0, 0)
					  , 3
					  );

	m_lcView.SetItemText(0,1, strDateList);
}

void CPlanSummary::InitSelHostList()
{
	m_lcSelHostList.SetExtendedStyle(m_lcSelHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lcSelHostList.InsertColumn(0, LoadStringById(IDS_PLANSELECTHOST_LIST004), LVCFMT_LEFT, 140);

	// 제일 마지막에 할것...
	m_lcSelHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcSelHostList.SetSortEnable(true);
}

void CPlanSummary::RefreshSelHostList()
{
	m_lcSelHostList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = GetTargetHostInfoList()->GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		STargetHostInfo& info = GetTargetHostInfoList()->GetNext(pos);

		CString strHostId;
		int nPos = info.strTargetHost.ReverseFind('=');
		if(nPos >= 0)
		{
			strHostId = info.strTargetHost.Mid(nPos+1);
		}

		//m_lcSelHostList.InsertItem(nRow, strHostId);
		m_lcSelHostList.InsertItem(nRow, info.strHostName);
		m_lcSelHostList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
}

HBRUSH CPlanSummary::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CSubBroadcastPlan::OnCtlColor(pDC, pWnd, nCtlColor);

	if( pWnd->m_hWnd == GetDlgItem(IDC_TXT_STARTDATE)->m_hWnd ||
		pWnd->m_hWnd == GetDlgItem(IDC_TXT_ENDDATE  )->m_hWnd ||
		pWnd->m_hWnd == GetDlgItem(IDC_TXT_DOWNLOAD_TIME)->m_hWnd )
	{
		pDC->SetBkMode(TRANSPARENT);
		hbr = HBRUSH(m_brush);
	}

	return hbr;
}
