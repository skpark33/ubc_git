#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "common\utblistctrlex.h"
#include "common\hoverbutton.h"

// CHostListDlg 대화 상자입니다.

class CHostListDlg : public CDialog
{
	DECLARE_DYNAMIC(CHostListDlg)

public:
	CHostListDlg(CString& hostName,CString& cacheServer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHostListDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HOSTLIST_DLG };
	enum { /*eCheck,*/eHostName, eHostID, eGroup, eHostIP, eDescription, eCategory, eAddr1,/*eSlaveCounter,*/ eMaxCol };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();

	CUTBListCtrlEx	m_lcHostList;
	HostInfoList	m_lsInfoList;
	CString			m_szColum[eMaxCol];
	CString			m_cacheServer;
	CString			m_hostName;

	void				InitHostList();
	void				RefreshHostList();
	void				ModifyHost(CArray<int>& arRow, bool bMsg);

	CEdit m_editCacheServer;
	CEdit m_editCounter;
	CHoverButton m_btRefresh;
	CHoverButton m_btAdd;
	CHoverButton m_btDel;
	afx_msg void OnBnClickedButtonHostRefresh();
	afx_msg void OnBnClickedButtonHostAdd();
	afx_msg void OnBnClickedButtonHostDel();
	afx_msg void OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult);
};
