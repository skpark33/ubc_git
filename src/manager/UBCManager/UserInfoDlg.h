#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "common\CheckComboBox.h"
#include "common\utblistctrlex.h"
#include "UBCCopCommon\CopModule.h"
#include "afxdtctl.h"

struct SUserInfo;

// CUserInfoDlg dialog
class CUserInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CUserInfoDlg)
public:
	void SetInfo(SUserInfo);
	SUserInfo GetInfo();

private:
	RoleInfoList m_lsRole;

	SUserInfo m_UserInfo;
	CComboBox m_cbPerm;
	CComboBox m_cbRole;
	CDateTimeCtrl m_dtValidationDate;

	CCheckComboBox m_cbFault;
	CButton m_kbFaultSMS;
	CButton m_kbFaultEMAIL;
	CUTBListCtrlEx m_lcSiteList;
	CUTBListCtrlEx m_lcHostList;

	void InitCauseList();
	void InitSiteList();
	void InitHostList();

public:
	CUserInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CUserInfoDlg();

// Dialog Data
	enum { IDD = IDD_USER_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonHostList();
	afx_msg void OnBnClickedButtonSiteList();
	afx_msg void OnBnClickedButtonSiteDel();
	CDateTimeCtrl m_dtLastPWChangeTime;
	CDateTimeCtrl m_dtLastLoginTime;
};
