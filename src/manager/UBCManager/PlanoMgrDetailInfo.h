#pragma once
#include "afxwin.h"
#include "DownloadPictureEx.h"
#include "ubccopcommon\copmodule.h"


// CPlanoMgrDetailInfo 대화 상자입니다.

class CPlanoMgrDetailInfo : public CDialog
{
	DECLARE_DYNAMIC(CPlanoMgrDetailInfo)

public:
	CPlanoMgrDetailInfo(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPlanoMgrDetailInfo();

	SPlanoInfo	m_PlanoMgrInfo;

	bool	m_IsCreateMode;
	bool	m_PlanoMgrChanged;
	void	SetCreateMode() { m_IsCreateMode=true; }
	BOOL	Upload();
	BOOL	SetInfo();
	void	SetInfo(SPlanoInfo&);
	void	GetInfo(SPlanoInfo&);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PLANOMGR_DETAILINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CEdit m_editPlanoId;
	CComboBox m_cbPlanoType;
	CEdit m_editDesc;
	CComboBox m_cbExpand;
	CDownloadPictureEx m_picPlanoMgr;
	afx_msg void OnBnClickedButtonNew();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnCbnSelchangeComboObjectclass();
	afx_msg void OnEnChangeEditPlanoid();
};
