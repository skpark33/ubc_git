#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "Enviroment.h"
#include "common\HoverButton.h"
#include "common\utblistctrl.h"
#include "common\reposcontrol.h"
#include "ubccopcommon\eventhandler.h"

// CCodeView form view
class CCodeView : public CFormView
{
	DECLARE_DYNCREATE(CCodeView)
public:
	enum { eCheck, eCategory, eEnumString, eEnumNumber, eDorder, eVisible, eEnd };
private:
	void InitList();
	void SetList();

public:
private:
	CUTBListCtrl m_lscCode;
	CHoverButton m_btAdd;
	CHoverButton m_btDel;
	CHoverButton m_btMod;
	CHoverButton m_btRef;
	CodeItemList m_lsCode;

	bool m_bInit;
	CRect m_rcClient;
	CString m_ColumTitle[eEnd];
	CReposControl m_Reposition;
	CString m_current_category;

protected:
	CCodeView();           // protected constructor used by dynamic creation
	virtual ~CCodeView();

public:
	enum { IDD = IDD_CODE_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	void OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult);
	
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
	afx_msg void OnBnClickedButtonMod();
	afx_msg void OnBnClickedButtonRef();
	CComboBox m_cb_category;
};


