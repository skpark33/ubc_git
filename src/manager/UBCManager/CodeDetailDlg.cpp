// CodeDetailDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CodeDetailDlg.h"


// CCodeDetailDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCodeDetailDlg, CDialog)

CCodeDetailDlg::CCodeDetailDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCodeDetailDlg::IDD, pParent)
{
	m_isSet = false;
}

CCodeDetailDlg::~CCodeDetailDlg()
{
}

void CCodeDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CATEGORY, m_edit_category);
	DDX_Control(pDX, IDC_EDIT_ENUMSTRING, m_edit_enumString);
	DDX_Control(pDX, IDC_EDIT_ENUMNUMBER, m_edit_enumNumber);
	DDX_Control(pDX, IDC_COMBO_VISIBLE, m_cb_visible);
	DDX_Control(pDX, IDC_EDIT_DORDER, m_edit_dorder);
	DDX_Control(pDX, IDC_SPIN_DORDER, m_sp_dorder);
}


BEGIN_MESSAGE_MAP(CCodeDetailDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCodeDetailDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCodeDetailDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CCodeDetailDlg 메시지 처리기입니다.

BOOL CCodeDetailDlg::OnInitDialog()
{
	CDialog::OnInitDialog();



	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_sp_dorder.SetRange32(0, 10000);

	m_cb_visible.AddString("false");
	m_cb_visible.AddString("true");

	InitData();

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCodeDetailDlg::InitData()
{
	if(m_isSet){
		this->m_edit_category.SetWindowText(m_codeInfo.strCategoryName);
		this->m_edit_enumString.SetWindowText(m_codeInfo.strEnumString);
		char buf[20];
		sprintf(buf,"%ld", m_codeInfo.nEnumNumber);
		this->m_edit_enumNumber.SetWindowText(buf);
		
		// 이미 한번 값이 정해진 값은 바꿀수 없다.
		if(!m_codeInfo.strEnumString.IsEmpty()){
			this->m_edit_enumNumber.SetReadOnly(true);
		}

		sprintf(buf,"%d", m_codeInfo.nDisplayOrder);
		this->m_edit_dorder.SetWindowText(buf);

		this->m_cb_visible.SetCurSel(m_codeInfo.bVisible);

	}else{
		this->m_cb_visible.SetCurSel(true);
	}
}

void CCodeDetailDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_isSet){

		m_codeInfo.bVisible = this->m_cb_visible.GetCurSel();

		this->m_edit_category.GetWindowText(m_codeInfo.strCategoryName);
		this->m_edit_enumString.GetWindowText(m_codeInfo.strEnumString);
		
		CString buf;
		this->m_edit_enumNumber.GetWindowText(buf);
		m_codeInfo.nEnumNumber = atoi(buf);

		this->m_edit_dorder.GetWindowText(buf);
		m_codeInfo.nDisplayOrder = atoi(buf);


		if(m_codeInfo.strEnumString.IsEmpty()){
			UbcMessageBox(LoadStringById(IDS_CODE_MSG003), MB_ICONINFORMATION);
			return;
		}
		if(m_codeInfo.nDisplayOrder == 0 && m_codeInfo.nEnumNumber==0){
			UbcMessageBox(LoadStringById(IDS_CODE_MSG004), MB_ICONINFORMATION);
			return;
		}


	}

	OnOK();
}

void CCodeDetailDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
