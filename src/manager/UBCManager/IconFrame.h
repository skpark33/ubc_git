#pragma once

#include "ubccopcommon\eventhandler.h"

// CIconFrame frame

class CIconFrame : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CIconFrame)
protected:
	CIconFrame();           // protected constructor used by dynamic creation
	virtual ~CIconFrame();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
