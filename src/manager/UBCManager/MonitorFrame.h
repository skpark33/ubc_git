#pragma once

#include "ubccopcommon\eventhandler.h"

// CMonitorFrame frame

class CMonitorFrame : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CMonitorFrame)
protected:
	CMonitorFrame();           // protected constructor used by dynamic creation
	virtual ~CMonitorFrame();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
