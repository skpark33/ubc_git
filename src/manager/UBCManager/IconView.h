#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"
#include "common\CheckComboBox.h"
#include "UBCCopCommon\CopModule.h"
#include "common\hoverbutton.h"


// CIconView ��E?�����Դϴ�.

class CIconView : public CFormView
{
	DECLARE_DYNCREATE(CIconView)

public:
	CIconView();   // ǥ�� �������Դϴ�.
	virtual ~CIconView();

// ��E?���� �������Դϴ�.
	enum { IDD = IDD_ICONVIEW };
	enum { eCheck, 
			eIconTypeID,
			eExt,
			eObjectClass,
			eObjectType,
			eObjectState,
			eIconEnd };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV �����Դϴ�.

	DECLARE_MESSAGE_MAP()
public:

	void InitPosition(CRect rc);
	void InitAuthCtrl();
	void InitIconList();
	BOOL RefreshIcon(bool bCompMsg=true);
	void RefreshIconList(POSITION posStart=NULL);
	void UpdateListRow(int nRow, SNodeTypeInfo* pInfo);
	void ModifyIcon(CArray<int>& arRow, bool bMsg);
	void RemoveIcon(CArray<int>& arRow, bool bMsg);


	CRect m_rcClient;
	CReposControl m_Reposition;
	CString		 m_szIconColum[eIconEnd];
	CImageList m_ilIconList;
	NodeTypeInfoList	m_lsIcon;

	CodeItemList m_listHostType;
	CodeItemList m_listLightType;
	CodeItemList m_listMonitorType;


	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	CComboBox		m_cbObjectClass;
	CCheckComboBox	m_cbHostType;
	CCheckComboBox	m_cbMonitorType;
	CCheckComboBox	m_cbLightType;
	CComboBox		m_cbObjectState;

	CHoverButton m_btnRefIcon;
	CHoverButton m_btnCreateIcon;
	CHoverButton m_btnDeleteIcon;
	CHoverButton m_btnSyncIcon;

	CUTBListCtrlEx m_lscIcon;

	afx_msg void OnBnClickedButtonIconRef();
	afx_msg void OnBnClickedButtonIconCreate();
	afx_msg void OnBnClickedButtonIconDel();
	virtual void OnInitialUpdate();
	afx_msg void OnCbnSelchangeComboFilterObjectclass();
	afx_msg void OnNMDblclkListIcon(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonIconSync();
};
