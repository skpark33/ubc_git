#include "StdAfx.h"
#include "UBCManager.h"
#include "HostExcelSave.h"
#include "Enviroment.h"
#include "UBCCopCommon\CopModule.h"

#include <afxdlgs.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CHostExcelSave::CHostExcelSave(void)
{
}

CHostExcelSave::~CHostExcelSave(void)
{
}

CString CHostExcelSave::Save(CString strSheetName, CUTBListCtrlEx& lcList)
{
	CFileDialog dlg(
		FALSE,
		"xls",
		NULL,
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		"Worksheet Files (*.xls)|*.xls|All Files (*.*)|*.*||",
		NULL
		);

	if(dlg.DoModal() != IDOK) return _T("");

	CWaitMessageBox wait;

	CString strFullpath = dlg.GetPathName();

	CFileStatus fs;

	if( CFile::GetStatus(strFullpath, fs) )
	{
		try
		{
			CFile::Remove(strFullpath);
		}
		catch (CFileException* pEx)
		{
			TCHAR szErrMsg[512] = {0};
			pEx->GetErrorMessage(szErrMsg, 512);
			pEx->Delete();
			UbcMessageBox(szErrMsg, MB_OK | MB_ICONWARNING);
		}
	}

	if(m_excelApp.CreateDispatch(_T("Excel.Application")))
	{
//			m_excelApp.SetVisible(TRUE);
		
		//Get a new workbook.
		COleVariant covOptional((long)DISP_E_PARAMNOTFOUND, VT_ERROR);

		Workbooks books = m_excelApp.GetWorkbooks();
		m_book = books.Add(covOptional);

		if( SaveDataList(1, strSheetName, lcList) )
		{
			m_book.SaveCopyAs(COleVariant(strFullpath));
		}

		m_book.SetSaved(TRUE);
		m_excelApp.Quit();
	}
	else
	{
		SaveDataList(strFullpath, lcList); // csv 파일로 출력함
		//UbcMessageBox(LoadStringById(IDS_HOSTLOGVIEW_MSG002));
		//return _T("");
	}

	return strFullpath;
}

CString CHostExcelSave::GetCell(int nCol, int nRow)
{
	int nCnt = (nCol / 26);
	nCol = nCol % 26;

	CString strCell;
	strCell.Format("%s%c%d", (nCnt > 0 ? CString((TCHAR)('A'+nCnt-1)) : _T("")), 'A'+nCol, nRow);
	return strCell;
}

BOOL CHostExcelSave::SaveDataList(short nSheet, CString strSheetName, CUTBListCtrlEx& lcList)
{
	COleVariant covTrue((short)TRUE);
	COleVariant covFalse((short)FALSE);

	Range range;

	//Get the first sheet.
	Worksheets sheets = m_book.GetSheets();
	_Worksheet sheet = sheets.GetItem(COleVariant((short)nSheet));
	sheet.SetName(strSheetName);

	int nLine = 1;

	CString data;
	CHeaderCtrl* pHeader = (CHeaderCtrl*)(lcList.GetHeaderCtrl());
	ASSERT(pHeader);
	int iNumCol = pHeader->GetItemCount();
	data.Format("%d",iNumCol);

	int *pDx;
	int *pFormat;
	char **pColTitle;

	if (iNumCol <= 0) return TRUE;

	pDx = new int[iNumCol+1];
	pFormat = new int[iNumCol];
	pColTitle = new char *[iNumCol];
	ZeroMemory(pColTitle, sizeof(char *) * iNumCol);
	ZeroMemory(pDx, sizeof(int) * (iNumCol + 1));
	ZeroMemory(pFormat, sizeof(int) * iNumCol);
	char txt[128];
	CString loc;

	for (int t = 0; t < iNumCol; t++)
	{
		HDITEM hi;
		hi.cchTextMax = 128;
		hi.pszText    = txt;

		//loc.Format("%c%d",'A'+t, nLine);
		loc = GetCell(t, nLine);
		hi.mask = HDI_TEXT|HDI_WIDTH|HDI_FORMAT;
		if (pHeader->GetItem(t, &hi))
		{
			pDx[t+1] = hi.cxy;
			pFormat[t] = hi.fmt;
			if (hi.cchTextMax > 0 && hi.pszText)
			{
				pColTitle[t] = new char[hi.cchTextMax];
				ASSERT(pColTitle[t]);
				lstrcpy(pColTitle[t], hi.pszText);
				data.Format("%s",hi.pszText);			
			}
		}
		range = sheet.GetRange(COleVariant(loc),COleVariant(loc));
//		range.SetValue(COleVariant(data));
		range.SetValue2(COleVariant(data));
	}

	COleSafeArray saRow;

	DWORD numElements[2];
	numElements[0]=1;
	numElements[1]=iNumCol;

	saRow.Create(VT_VARIANT, 2, numElements);

	for(UINT i=0;i<lcList.GetItemCount();i++)
	{
		for(UINT j=0;j<iNumCol;j++)
		{
			long index[2];

			index[0] = 0;
			index[1] = j;

			CString strCellText = lcList.GetItemText(i,j);
			if(lcList.GetColType(j) == CUTBHeaderCtrl::COL_PROGRESS && strCellText.ReverseFind(_T('`')) >= 0)
			{
				strCellText = strCellText.Mid(strCellText.ReverseFind(_T('`'))+1);
			}
			COleVariant vTemp = (COleVariant)strCellText;
			saRow.PutElement(index, vTemp);
		}

//		POSITION pos = (POSITION)lcList.GetItemData(i);
//		SHostInfo Info = GetEnvPtr()->m_lsHost.GetAt(pos);

		COLORREF clrBack = RGB(255,255,255);
		COLORREF clrText = RGB(0,0,0);
		lcList.GetRowColor(i, clrBack, clrText);

		CString strRowStart, strRowEnd;
		strRowStart.Format("A%d", i+nLine+1);
		//strRowEnd.Format("%c%d",'A'+iNumCol-1, i+nLine+1);
		strRowEnd = GetCell(iNumCol-1, i+nLine+1);

		range = sheet.GetRange(COleVariant(strRowStart), COleVariant(strRowEnd));
		range.SetValue2(COleVariant(saRow));
		Interior interior = range.GetInterior();

		//interior.SetColor(COleVariant((double)(Info.operationalState ? RGB(255,255,0) : RGB(200,200,200))));
		interior.SetColor(COleVariant((double)clrBack));
	}

	CString start_num, end_num;
	start_num.Format("A%d", nLine+1);
	//end_num.Format("%c%d",'A'+iNumCol-1,lcList.GetItemCount()+nLine);
	end_num = GetCell(iNumCol-1, lcList.GetItemCount()+nLine);

	range = sheet.GetRange(COleVariant(start_num), COleVariant(/*excelEng*/end_num));

	Range cols = range.GetEntireColumn();
	cols.AutoFit();
	saRow.Detach();

	return TRUE;
}


BOOL CHostExcelSave::SaveDataList(CString& strFullpath, CUTBListCtrlEx& lcList)
{
	strFullpath.Replace(".xls",".csv");

	CHeaderCtrl* pHeader = (CHeaderCtrl*)(lcList.GetHeaderCtrl());
	ASSERT(pHeader);

	int iNumCol = pHeader->GetItemCount();
	if (iNumCol <= 0) return TRUE;

	CString header;

	char txt[128];
	for (int t = 0; t < iNumCol; t++)
	{
		HDITEM hi;
		hi.cchTextMax = 128;
		hi.pszText    = txt;
		hi.mask = HDI_TEXT|HDI_WIDTH|HDI_FORMAT;

		if (pHeader->GetItem(t, &hi))
		{
			if (hi.cchTextMax > 0 && hi.pszText)
			{
				CString buf;
				buf.Format("%s,",hi.pszText);			
				header += buf;
			}
		}
	}
	header += "\n";

	FILE* fp = fopen(strFullpath,"w");
	fwrite(header,sizeof(char), header.GetLength(), fp);

	for(UINT i=0;i<lcList.GetItemCount();i++)
	{
		CString strLine;
		for(UINT j=0;j<iNumCol;j++)
		{
			CString strCellText = lcList.GetItemText(i,j);
			if(lcList.GetColType(j) == CUTBHeaderCtrl::COL_PROGRESS && strCellText.ReverseFind(_T('`')) >= 0)
			{
				strCellText = strCellText.Mid(strCellText.ReverseFind(_T('`'))+1);
			}
			strLine += strCellText;
			strLine += ",";
		}
		strLine+= "\n";
		fwrite(strLine,sizeof(char), strLine.GetLength(), fp);
	}
	fclose(fp);
	return TRUE;
}
