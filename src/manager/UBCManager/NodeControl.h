#pragma once
#include "DownloadPictureEx.h"
#include "ubccopcommon\CopModule.h"
#include <atlimage.h> //skpark 1111

class CPlanoControl;
class CNodeControl : public CDownloadPictureEx
{
public:
	enum { ACTION_SELECT=0, ACTION_CREATED, ACTION_MODIFIED, ACTION_DELETED };

	CNodeControl(CPlanoControl* parent);
	virtual ~CNodeControl();

	void Clear();
	bool LoadIconFile();
	bool CreateIcon(bool showInfo);
	bool CreateNode(CString& pObjectClass, CString& pObjectId, int x, int y);
	bool CreateObject();
	bool DeleteObject();
	bool ModifyObject();
	bool isDeleted()	{   return ((m_action == ACTION_DELETED)	? true : false); }
	bool isModified()	{   return ((m_action == ACTION_MODIFIED)	? true : false); }
	bool isCreated()	{   return ((m_action == ACTION_CREATED)	? true : false); }
	bool isNoAction()	{   return ((m_action == ACTION_SELECT)	? true : false); }

	void SetDelete() { m_action = ACTION_DELETED; }
	void SetCreate() { m_action = ACTION_CREATED; }
	void SetModify() { m_action = ACTION_MODIFIED; }

	void SetNodeInfo(SNodeViewInfo* p);
	void EndMoving() ;
	void Selected(bool select, bool redraw=true);
	bool IsSelected() { return m_selected; }

	SNodeViewInfo*	GetInfo() { return m_NodeInfo; }

	void	DrawIt(); //skpark 1111
	bool	Load(CString& fileName, CString& localLocation, CString& serverLocation) ; //skpark 1111
	void	UnLoad(); //skpark 1111
	int		GetWidth() { if(m_DrawMode) return  m_nodeImage.GetWidth();  else return CDownloadPictureEx::GetWidth();}
	int		GetHeight() { if(m_DrawMode) return m_nodeImage.GetHeight(); else return CDownloadPictureEx::GetHeight();}
	void	SetDrawMode(bool p) { m_DrawMode=p; }
	void	ShowName(bool show);
	bool	toHostInfo(SHostInfo& outInfo);
	bool	toMonitorInfo(SMonitorInfo& outInfo);
	bool	toLightInfo(SLightInfo& outInfo);

protected:
	CImage	m_nodeImage; //skpark 1111

	CPlanoControl* m_planoCtrl;
	SNodeViewInfo* m_NodeInfo;
	int			m_action;
	bool		m_moving;
	bool		m_selected;
	bool		m_DrawMode;
	//CRect		*m_rect;

	CStatic		m_nameLabel;
	bool		m_isShowName;
	//CStatic		m_valueLabel;

	int			m_start_x;
	int			m_start_y;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnPlanoDeletenode();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//afx_msg void OnNcPaint();
	afx_msg void OnPlanoReboot();
	afx_msg void OnPlanoMuteon();
	afx_msg void OnPlanoMuteoff();
	afx_msg void OnPlanoPoweron();
	afx_msg void OnPlanoPoweroff();

	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
//	afx_msg void OnNcMouseMove(UINT nHitTest, CPoint point);
//	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnNodeRemotelogin2();
	afx_msg void OnNodeRemotelogin();
	afx_msg void OnNodemenuPackageapply();
	afx_msg void OnNodemenuCancelpackage();
};
