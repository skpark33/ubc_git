#pragma once

#include "resource.h"

// CChngPasswordDlg 대화 상자입니다.

class CChngPasswordDlg : public CDialog
{
	DECLARE_DYNAMIC(CChngPasswordDlg)

public:
	CChngPasswordDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CChngPasswordDlg();

	CString m_strPassword;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CHNG_PASSWORD_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
