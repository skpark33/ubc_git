#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "SubBroadcastPlan.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\CheckComboBox.h"

// CPlanSelectHost 대화 상자입니다.

class CPlanSelectHost : public CSubBroadcastPlan
{
	DECLARE_DYNAMIC(CPlanSelectHost)

public:
	CPlanSelectHost(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPlanSelectHost();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PLAN_SELECT_HOST };
	enum { eCheck, eHostName, eHostID, eDisplay, eGroup, eCategory, eMaxCol };

	bool IsModified();
	void UpdateInfo();
	bool InputErrorCheck(bool bMsg);
	void RefreshInfo();

protected:
	CString				m_szColum[eMaxCol];
	HostInfoList		m_lsInfoList;

	CCheckComboBox		m_cbHostType;
	CComboBox			m_cbAdmin;
	CComboBox			m_cbOperation;
	CHoverButton		m_bnRefresh;

	CUTBListCtrlEx		m_lcHostList;
	CUTBListCtrlEx		m_lcSelHostList;
	CHoverButton		m_bnAdd;
	CHoverButton		m_bnDel;

	void LoadFilterData();
	void SaveFilterData();
	CString m_strSiteId;
	CString m_strContentsId;
	CString m_strPackageId;

	void				InitHostList();
	void				RefreshHostList();
	void				RefreshHostCnt();

	void				InitSelHostList();
	void				RefreshSelHostList();
	void				RefreshSelHostCnt();

	void				AddSelHost(int nHostIndex);
	void				DelSelHost(int nSelHostIndex);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonPlanRefresh();
	afx_msg void OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListSelHost(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedBnAdd();
	afx_msg void OnBnClickedBnDel();
	afx_msg void OnBnClickedButtonFilterSite();
	afx_msg void OnBnClickedButtonFilterContents();
	afx_msg void OnBnClickedButtonFilterPackage();
	afx_msg void OnLvnColumnclickListHost(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListHost(NMHDR *pNMHDR, LRESULT *pResult);
	CComboBox m_cbFilterTag;
	afx_msg void OnBnClickedBnAddtag();
	afx_msg void OnBnClickedBnDeltag();
	CComboBox m_cbFilterCache;
};
