#include "stdafx.h"
#include "resource.h"
#include "common\TraceLog.h"
#include "DownloadPictureEx.h"
#include "Enviroment.h"


CDownloadPictureEx::CDownloadPictureEx()
{
	m_expand = false;
	m_bgColor = NODE_COLOR_WHITE;
}

CDownloadPictureEx::~CDownloadPictureEx()
{
}

BEGIN_MESSAGE_MAP(CDownloadPictureEx, CStatic)
	//{{AFX_MSG_MAP(CDownloadPictureEx)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CDownloadPictureEx::OnPaint() 
{
	TraceLog(("OnPaint()"));

	if(m_bgImage.IsValid())
	{
		CPaintDC dc(this);
		CRect rect;
		GetClientRect(rect);

		//dc.FillSolidRect(rect, RGB(255,255,255));
		dc.FillSolidRect(rect, m_bgColor);

		if(!m_expand){
			double scale;

			int image_width = m_bgImage.GetWidth();
			int image_height = m_bgImage.GetHeight();

			int scr_width = rect.Width();
			int scr_height = rect.Height();

			int width, height;
			width = scr_width;
			height = image_width * scr_height / scr_width;
			if(height < image_height)
			{
				height = scr_height;
				width = image_height * scr_width / scr_height;

				scale = ((double)scr_height) / ((double)image_height);
			}
			else
			{
				scale = ((double)scr_width) / ((double)image_width);
			}

			rect.right = rect.left + m_bgImage.GetWidth() * scale;
			rect.bottom = rect.top + m_bgImage.GetHeight() * scale;
		}
		m_bgImage.Draw2(dc.GetSafeHdc(), rect);
	}
	CStatic::OnPaint();
	//CButton::OnPaint();
}

void CDownloadPictureEx::OnDestroy() 
{
	CStatic::OnDestroy();
	//CButton::OnDestroy();
	UnLoad();
}

bool CDownloadPictureEx::Load(CString& fileName, CString& localLocation, CString& serverLocation) 
{
	TraceLog(("Load(%s,%s,%s)", fileName, localLocation, serverLocation));

	CString strMediaFullPath;
	strMediaFullPath.Format("%s%s", localLocation,fileName);

	CString strExt = FindExtension(strMediaFullPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	
	UnLoad();

	bool bRet = false;
	for(int i=0;i<2;i++) { // 2차례 시도한다.
		bRet = Download(fileName,localLocation,serverLocation);
		bRet = m_bgImage.Load(strMediaFullPath, nImgType);
		if(bRet){
			TraceLog(("%s file loaded from server", strMediaFullPath));
			return true;
		}
		TraceLog(("%s file not exist", strMediaFullPath));
	}
	TraceLog(("%s file load failed", strMediaFullPath));
	// 수정필요
	// 로드에 fail 할 때, default 를 하도록 한다.!!!
	return bRet;
}

bool CDownloadPictureEx::LocalLoad(CString& fileName, CString& localLocation) 
{
	TraceLog(("LocalLoad(%s,%s)", fileName, localLocation));

	CString strMediaFullPath;
	strMediaFullPath.Format("%s%s", localLocation,fileName);

	CString strExt = FindExtension(strMediaFullPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	
	UnLoad();

	bool bRet = m_bgImage.Load(strMediaFullPath, nImgType);
	if(bRet){
		TraceLog(("%s file loaded from local", strMediaFullPath));
		return true;
	}
	TraceLog(("%s file not exist", strMediaFullPath));
	return bRet;
}
void CDownloadPictureEx::UnLoad()
{
	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}
}

bool CDownloadPictureEx::Download(CString& fileName, CString& localLocation, CString serverLocation)
{
	TraceLog(("Download(%s,%s,%s,%s)", fileName, localLocation,serverLocation,m_siteId));
	if(!GetEnvPtr()->GetFile(fileName, serverLocation,localLocation,m_siteId)){
		TraceLog(("ERROR : Download(%s,%s,%s) failed", fileName, localLocation,serverLocation));
		return false;
	}
	TraceLog(("Download succeed(%s,%s,%s)", fileName, localLocation,serverLocation));
	return true;

}
bool CDownloadPictureEx::Upload(CString& fileName, CString& localLocation, CString serverLocation)
{
	TraceLog(("Upload(%s,%s,%s,%s)", fileName, localLocation,serverLocation,m_siteId));
	if(!GetEnvPtr()->PutFile(fileName, localLocation, serverLocation,m_siteId)){
		TraceLog(("ERROR : Upload(%s,%s,%s) failed", fileName, localLocation,serverLocation));
		return false;
	}
	TraceLog(("Upload succeed(%s,%s,%s)", fileName, localLocation,serverLocation));
	return true;

}
/*
//#define DEFAULT_BORDER_COLOR    RGB(127, 157, 185)
#define DEFAULT_BORDER_COLOR    RGB(255, 0, 0)

LRESULT CDownloadPictureEx::WindowProc(UINT message,WPARAM wParam,LPARAM lParam)    
{        
	switch (message)        
	{            
		case WM_NCPAINT:   {       
			CStatic::WindowProc(message, wParam, lParam);    
		
			// convert to client coordinates                
			CRect rect;                
			GetWindowRect(&rect);                
			rect.OffsetRect(-rect.left, -rect.top);
						
			// Draw a single line around the outside                
			CWindowDC dc(this);                
			dc.Draw3dRect(&rect, DEFAULT_BORDER_COLOR, DEFAULT_BORDER_COLOR);                
			return 0; // Handled.            
		}        
	}        
	return CStatic::WindowProc(message, wParam, lParam);
}
*/