#pragma once
#include "afxcmn.h"
#include "afxdtctl.h"
#include "afxwin.h"
#include "enviroment.h"
#include "common/HoverButton.h"
#include "common/UTBListCtrl.h"

// CDownloadTimeSetDlg dialog
class CDownloadTimeSetDlg : public CDialog
{
	DECLARE_DYNAMIC(CDownloadTimeSetDlg)

public:

	bool			m_bMonitorOff;	// 모니터 절전 기능 관련
	std::list<std::string> m_listMonitorOffTime; // 모니터 절전 기능 관련

private:
	// 모니터절전기능 관련
	CButton			m_ckUseMonitorOffTime;
	CDateTimeCtrl	m_dtMonitorOff;
	CDateTimeCtrl	m_dtMonitorOn;
	CHoverButton	m_bnAddMonitorOffTime;
	CHoverButton	m_bnDelMonitorOffTime;
	CUTBListCtrl	m_lscMonitorOffTime;

	void			InitMonitorOffTimeInfo();
	void			EnableMonitorOffInfo(BOOL bUseMonitorOff);

	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
public:
	CDownloadTimeSetDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDownloadTimeSetDlg();

// Dialog Data
	enum { IDD = IDD_DOWNLOADTIME_SET };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedKbUseMonitorOffTime();
	afx_msg void OnBnClickedBnAddMonitorOffTime();
	afx_msg void OnBnClickedBnDelMonitorOffTime();

	CStatic m_staticMonitorGroup;
	CButton m_btOK;
	CButton m_btCancel;

	void			SetContentsDownloadTime(CString& contentsDownloadTimeStr) { m_property_value = contentsDownloadTimeStr; }
	CString	getName()	{	return m_property_name;		}
	CString getValue()	{	return m_property_value;	}

	CString m_property_name;
	CString m_property_value;
	CString m_contentsDownloadTimeStr;
};
