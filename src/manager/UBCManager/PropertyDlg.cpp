// PropertyDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "PropertyDlg.h"
#include "Enviroment.h"


// CPropertyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPropertyDlg, CDialog)

CPropertyDlg::CPropertyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPropertyDlg::IDD, pParent)
{

}

CPropertyDlg::~CPropertyDlg()
{
}

void CPropertyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_DIRECTIVE, m_cbDirective);
	DDX_Control(pDX, IDC_COMBO_INIFILE, m_cbIniFile);
	DDX_Control(pDX, IDC_COMBO_ENTRYNAME, m_cbEntryName);
	DDX_Control(pDX, IDC_COMBO_VARNAME, m_cbVarName);
	DDX_Control(pDX, IDC_COMBO_VARVALUE, m_cbVarValue);
	DDX_Control(pDX, IDC_CHECK_REGET, m_check_reget);
	DDX_Control(pDX, IDC_CHECK_DELETEFOLDER, m_check_deleteFolder);
}


BEGIN_MESSAGE_MAP(CPropertyDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CPropertyDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPropertyDlg::OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_COMBO_INIFILE, &CPropertyDlg::OnCbnSelchangeComboInifile)
	ON_CBN_SELCHANGE(IDC_COMBO_ENTRYNAME, &CPropertyDlg::OnCbnSelchangeComboEntryname)
	ON_BN_CLICKED(IDC_CHECK_REGET, &CPropertyDlg::OnBnClickedCheckReget)
END_MESSAGE_MAP()


// CPropertyDlg 메시지 처리기입니다.

void CPropertyDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	bool isCheck = false;
	if(m_check_reget.GetCheck()) {
		m_strValue += "/";
		m_strValue += CEnviroment::GetObject()->m_strCustomer;
		m_strValue += "::reget";
		isCheck = true;
	}
	if(m_check_deleteFolder.GetCheck()) {
		m_strValue += "/";
		m_strValue += CEnviroment::GetObject()->m_strCustomer;
		m_strValue += "::clean";
		isCheck = true;
	}
	if(isCheck) {
		m_strName="MSG";
		OnOK();
		return;
	}


	CString buf;
	m_cbDirective.GetWindowText(buf);
	if(buf.IsEmpty()){
		return;
	}
	m_strName = buf;


	m_strName += ".";
	buf = "";
	m_cbIniFile.GetWindowText(buf);
	if(buf.IsEmpty()){
		return;
	}
	m_strName += buf;

	m_strName += ".";
	buf = "";
	m_cbEntryName.GetWindowText(buf);
	if(buf.IsEmpty()){
		return;
	}
	m_strName += buf;

	m_strName += ".";
	buf = "";
	m_cbVarName.GetWindowText(buf);
	if(buf.IsEmpty()){
		return;
	}
	m_strName += buf;

	buf = "";
	m_cbVarValue.GetWindowText(buf);
	m_strValue = buf;

	OnOK();
}

void CPropertyDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

BOOL CPropertyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_cbDirective.AddString("PLUS");
	m_cbDirective.AddString("MINUS");
	m_cbDirective.AddString("SET");
	m_cbDirective.AddString("GET");
	m_cbDirective.SetCurSel(0);

	m_cbIniFile.AddString("");
	m_cbIniFile.AddString("UBCVariables");
	m_cbIniFile.AddString("UBCStarter");
	m_cbIniFile.AddString("UBCBrowser");
	m_cbIniFile.AddString("UBCConnect");
	m_cbIniFile.SetCurSel(0);

	m_cbEntryName.ResetContent();
	m_cbVarName.ResetContent();
	m_cbVarValue.ResetContent();

	//m_check_reget.ShowWindow(SW_HIDE);
	//m_check_deleteFolder.ShowWindow(SW_HIDE);
	//m_check_reget.ShowWindow(SW_SHOW);
	//m_check_deleteFolder.ShowWindow(SW_SHOW);

	m_check_reget.EnableWindow(true);
	m_check_deleteFolder.EnableWindow(false);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	char buf[1024];
	GetPrivateProfileString("ROOT", "LAST_PROPERTY", "", buf, 1024, szPath);
	
	CString strName = buf;
	if(!strName.IsEmpty()){
		int nPos = 0;
		CString strToken = strName.Tokenize(".", nPos);
		if(strToken != "")
		{
			m_cbDirective.SetCurSel(3);
		}
		strToken = strName.Tokenize(".", nPos);
		if(strToken != "")
		{
			m_cbIniFile.SetWindowText(strToken);
		}
		strToken = strName.Tokenize(".", nPos);
		if(strToken != "")
		{
			m_cbEntryName.SetWindowText(strToken);
		}
		strToken = strName.Tokenize(".", nPos);
		if(strToken != "")
		{
			m_cbVarName.SetWindowText(strToken);
		}
	}





	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPropertyDlg::OnCbnSelchangeComboInifile()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int idx = m_cbIniFile.GetCurSel();

	CString iniName;
	m_cbIniFile.GetWindowTextA(iniName);

	m_cbEntryName.ResetContent();
	//if(iniName == "UBCVariables") {
	if(idx == 1) {
		m_cbEntryName.AddString("");
		m_cbEntryName.AddString("ROOT");
		m_cbEntryName.AddString("SHUTDOWNTIME");
		m_cbEntryName.AddString("CUSTOMER_INFO");
		m_cbEntryName.AddString("PROJECTOR");
		m_cbEntryName.AddString("XML");
		m_cbEntryName.AddString("STARTUPTIME");

	//}else if(iniName == "UBCStarter") {
	}else if(idx == 2) {

		m_cbEntryName.AddString("");
		m_cbEntryName.AddString("ROOT");
		m_cbEntryName.AddString("SERIALGW");
		m_cbEntryName.AddString("BROWSER");
		m_cbEntryName.AddString("BROWSER1");
		m_cbEntryName.AddString("CLI");
		m_cbEntryName.AddString("FirmwareView");
		m_cbEntryName.AddString("PREDOWNLOADER");
		m_cbEntryName.AddString("STARTER");
		m_cbEntryName.AddString("VNC_Proxy");
		m_cbEntryName.AddString("CoreTemp");
		m_cbEntryName.AddString("3RDPARTY_PLAYER");

	//}else if(iniName == "UBCBrowser") {
	}else if(idx == 3) {

		m_cbEntryName.AddString("");
		m_cbEntryName.AddString("ROOT");
		m_cbEntryName.AddString("UTV_brwClient2");
		m_cbEntryName.AddString("UTV_brwClient2_UBC1");
		m_cbEntryName.AddString("RELOADSCHEDULE_LOG");

	//}else if(iniName == "UBCConnect") {
	}else if(idx == 4) {

		m_cbEntryName.AddString("");
		m_cbEntryName.AddString("ORB_NAMESERVICE1");
		m_cbEntryName.AddString("ORB_NAMESERVICE");
		m_cbEntryName.AddString("AGENTMUX");
		m_cbEntryName.AddString("TIMESYNCER");
		m_cbEntryName.AddString("VNCMANAGER");
		m_cbEntryName.AddString("DEVICEWIZARD");
		m_cbEntryName.AddString("UBCCENTER");

	}
	m_cbEntryName.SetCurSel(0);
	//m_cbEntryName.ResetContent();

}

void CPropertyDlg::OnCbnSelchangeComboEntryname()
{
	CString iniName;
	m_cbIniFile.GetWindowTextA(iniName);

	int idx = m_cbEntryName.GetCurSel();


	CString entryName;
	m_cbEntryName.GetWindowTextA(entryName);

	m_cbVarName.ResetContent();
	m_cbVarName.AddString("");

	if(iniName == "UBCVariables") {
		//if(entryName == "ROOT"){
		if(idx == 1) {
			m_cbVarName.AddString("PropertyName");
			m_cbVarName.AddString("SiteId");
			m_cbVarName.AddString("AgentConnectionPeriod");
			m_cbVarName.AddString("TimeSyncPeriod");
			m_cbVarName.AddString("ScreenShotPeriod");
			m_cbVarName.AddString("AutoUpdatePeriod");
			m_cbVarName.AddString("ApplicationMonitorPeriod");
			m_cbVarName.AddString("USE_CLICK_SCHEDULE");
			m_cbVarName.AddString("AutoUpdateFlag");
			m_cbVarName.AddString("HTTP_ONLY");
			m_cbVarName.AddString("USB_AUTO_IMPORT");
			m_cbVarName.AddString("DONT_IGNORE_SCHEDULE_ENDDATE");
			m_cbVarName.AddString("HostType");
			m_cbVarName.AddString("HddThreshold");
			m_cbVarName.AddString("PlayLogDayLimit");
			m_cbVarName.AddString("PlayLogDayCriteria");
			m_cbVarName.AddString("MonitorOff");
			m_cbVarName.AddString("MonitorOffList");
			m_cbVarName.AddString("Category");
			m_cbVarName.AddString("MonitorType");
			m_cbVarName.AddString("ContentsDownloadTime");
			m_cbVarName.AddString("SoundVolume");
			m_cbVarName.AddString("Mute");
			m_cbVarName.AddString("LastScreenShotFile");
			m_cbVarName.AddString("BootUpTime");
			m_cbVarName.AddString("BootDownTime");
			m_cbVarName.AddString("ContentsCacheServer");
			m_cbVarName.AddString("TimeSyncTime");
			m_cbVarName.AddString("TimeSyncResult");
			m_cbVarName.AddString("CODEC_ROLLBACKED");
			m_cbVarName.AddString("MONITOR_COUNT");
			m_cbVarName.AddString("TIME_ANNOUNCE_CHECK");			
		//}else if(entryName =="SHUTDOWNTIME"){
		}else if(idx == 2) {
			m_cbVarName.AddString("value");
			m_cbVarName.AddString("done");
			m_cbVarName.AddString("week_value");
		//}else if(entryName =="CUSTOMER_INFO"){
		}else if(idx == 3) {
			m_cbVarName.AddString("NAME");
		//}else if(entryName =="PROJECTOR"){
		}else if(idx == 4) {
			m_cbVarName.AddString("ProjectorType");
		//}else if(entryName =="XML"){
		}else if(idx == 5) {
			m_cbVarName.AddString("WIA_WELCOMEBOARD");
			m_cbVarName.AddString("SERVER_IP");
			m_cbVarName.AddString("PORT");
		//}else if(entryName =="STARTUPTIME"){
		}else if(idx == 6) {
			m_cbVarName.AddString("value");
		}
	}else if(iniName == "UBCStarter") {

		//if(entryName =="ROOT"){
		if(idx == 1) {
			m_cbVarName.AddString("APP_MONITOR");
			m_cbVarName.AddString("APP_LIST");
			m_cbVarName.AddString("APP_LIST_");
		} else {
			m_cbVarName.AddString("ARGUMENT");
			m_cbVarName.AddString("AUTO_STARTUP");
			m_cbVarName.AddString("BINARY_NAME");
			m_cbVarName.AddString("DEBUG");
			m_cbVarName.AddString("INIT_SLEEP_SECOND");
			m_cbVarName.AddString("IS_PRIMITIVE");
			m_cbVarName.AddString("MONITORED");
			m_cbVarName.AddString("PID");
			m_cbVarName.AddString("PRERUNNING_APP");
			m_cbVarName.AddString("PROPERTIES");
			m_cbVarName.AddString("RUNNING_TYPE");
			m_cbVarName.AddString("RUNTIME_UPDATE");
			m_cbVarName.AddString("START_TIME");
			m_cbVarName.AddString("STATUS");
		}

	}else if(iniName == "UBCBrowser") {

		//if(entryName == "ROOT") {
		if(idx == 1) {
			m_cbVarName.AddString("UTV_brwClient2");
			m_cbVarName.AddString("UTV_brwClient2_UBC1");
			m_cbVarName.AddString("VIDEO_OPEN_TYPE");
			m_cbVarName.AddString("VIDEO_OPEN_TYPE_SET");
			m_cbVarName.AddString("VIDEO_RENDER");
			m_cbVarName.AddString("VIDEO_OPEN_ONCE");
		//}else if(entryName == "UTV_brwClient2") {
		}else if(idx == 2) {
			m_cbVarName.AddString("sound");
			m_cbVarName.AddString("monitor");
			m_cbVarName.AddString("UsePosition");
			m_cbVarName.AddString("posx");
			m_cbVarName.AddString("posy");
			m_cbVarName.AddString("width");
			m_cbVarName.AddString("height");
		//}else if(entryName == "UTV_brwClient2_UBC1") {
		}else if(idx == 3) {
			m_cbVarName.AddString("sound");
			m_cbVarName.AddString("monitor");
			m_cbVarName.AddString("UsePosition");
			m_cbVarName.AddString("posx");
			m_cbVarName.AddString("posy");
			m_cbVarName.AddString("width");
			m_cbVarName.AddString("height");
		//}else if(entryName == "RELOADSCHEDULE_LOG") {
		}else if(idx == 4) {
			m_cbVarName.AddString("TIME1");
			m_cbVarName.AddString("PROGRAM1");
			m_cbVarName.AddString("BY1");
			m_cbVarName.AddString("TIME");
			m_cbVarName.AddString("NAME");
			m_cbVarName.AddString("PROGRAM");
			m_cbVarName.AddString("SIDE");
			m_cbVarName.AddString("TIME2");
			m_cbVarName.AddString("PROGRAM2");
			m_cbVarName.AddString("BY2");
		}

	}else if(iniName == "UBCConnect") {

		//if(entryName == "ORB_NAMESERVICE1") {
		if(idx == 1) {
			m_cbVarName.AddString("NAME");
			m_cbVarName.AddString("IP");
			m_cbVarName.AddString("PORT");
			m_cbVarName.AddString("USE");
		//} else if(entryName == "ORB_NAMESERVICE") {
		}else if(idx == 2) {
			m_cbVarName.AddString("IP");
			m_cbVarName.AddString("NAME");
			m_cbVarName.AddString("PORT");
		//} else if(entryName == "AGENTMUX") {
		}else if(idx == 3) {
			m_cbVarName.AddString("IP");
			m_cbVarName.AddString("PORT");
			m_cbVarName.AddString("HTTP_PORT");
		//} else if(entryName == "TIMESYNCER") {
		}else if(idx == 4) {
			m_cbVarName.AddString("IP");
			m_cbVarName.AddString("PORT");
		//} else if(entryName == "VNCMANAGER") {
		}else if(idx == 5) {
			m_cbVarName.AddString("IP");
			m_cbVarName.AddString("KINGYOO");
		//} else if(entryName == "DEVICEWIZARD") {
		}else if(idx == 6) {
			m_cbVarName.AddString("TestURL");
		//} else if(entryName == "UBCCENTER") {
		}else if(idx == 7) {
			m_cbVarName.AddString("IP");
			m_cbVarName.AddString("NAME");
			m_cbVarName.AddString("PORT");
			m_cbVarName.AddString("FTPID");
			m_cbVarName.AddString("FTPPASSWD");
			m_cbVarName.AddString("FTPPORT");
			m_cbVarName.AddString("VERSION");
			m_cbVarName.AddString("FTP_IP");
		}
	}
	m_cbVarName.SetCurSel(0);

}

void CPropertyDlg::OnBnClickedCheckReget()
{
	if(this->m_check_reget.GetCheck() == 1){
		this->m_check_deleteFolder.EnableWindow(true);
	}else{
		this->m_check_deleteFolder.EnableWindow(true);
		this->m_check_deleteFolder.SetCheck(0);	
	}
}
