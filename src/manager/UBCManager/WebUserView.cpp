// WebUserView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "WebUserView.h"
#include "common\reposcontrol.h"


// CWebUserView

IMPLEMENT_DYNCREATE(CWebUserView, CFormView)

CWebUserView::CWebUserView()
	: CFormView(CWebUserView::IDD)
,	m_Reposition(this)
{

}

CWebUserView::~CWebUserView()
{
}

void CWebUserView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IE_MAIN, m_ieMain);
}

BEGIN_MESSAGE_MAP(CWebUserView, CFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CWebUserView, CDialog)
	ON_EVENT(CWebUserView, IDC_IE_MAIN, 263, CWebUserView::WindowClosingIeMain, VTS_BOOL VTS_PBOOL)
END_EVENTSINK_MAP()


// CWebUserView 진단입니다.

#ifdef _DEBUG
void CWebUserView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CWebUserView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CWebUserView 메시지 처리기입니다.

int CWebUserView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}

void CWebUserView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_USERVIEW_STR001));

	InitPosition(m_rcClient);

	//GUBUN       : UBC 로 픽스된 값 입니다.
	//SITE           : siteId 입니다.
	//ID               : 로그인 아이디 입니다.
	//PW             : 로그인 암호 입니다. 
	//PROGRAM  :  각각의 웹 프로그램의 주소입니다.
	//
	//
	//http://211.232.57.219/default.aspx?GUBUN=UBC&SITE=HYUNDAI&ID=HYUNDAImanager&PW=HYUNDAImanager&PROGRAM=community/notice_list.aspx
	//
	//
	//PROGRAM 에 들어갈 값은 다음과 같습니다.
	//community/notice_list.aspx          <- 공지사항 
	//community/program_list.aspx       <- 프로그램 다운로드
	//community/manual_list.aspx        <- 매뉴얼 다운로드
	//community/video_list.aspx           <- 동영상 매뉴얼
	//community/contents.aspx            <- 컨텐츠관리
	//
	//manager/code.aspx                     <- 공통코드관리
	//manager/user.aspx                      <- 사용자관리
	//manager/org.aspx                        <- 조직관리

	CString strEntry = _T("UNKNOWN");
	if     (CEnviroment::eKIA     == GetEnvPtr()->m_Customer) strEntry = _T("KIA");
	else if(CEnviroment::eHYUNDAI == GetEnvPtr()->m_Customer) strEntry = _T("HYUNDAI");
	else if(CEnviroment::eKMNL    == GetEnvPtr()->m_Customer) strEntry = _T("KMNL");
	else if(CEnviroment::eKMCL    == GetEnvPtr()->m_Customer) strEntry = _T("KMCL");
	else if(!GetEnvPtr()->m_strCustomer.IsEmpty()) strEntry = GetEnvPtr()->m_strCustomer;

	TCHAR szBuf[2048] = {0};
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, UBCURL_INI);
	GetPrivateProfileString(strEntry, _T("BASE"), "", szBuf, 2048, szPath); CString strBaseUrl(szBuf);
	GetPrivateProfileString(strEntry, _T("USER"), "", szBuf, 2048, szPath); CString strSubUrl(szBuf);

	CString strUrl;
	if(strBaseUrl.IsEmpty()) strUrl = _T("about:blank");
	else
	{
		strUrl.Format(_T("%s?GUBUN=%s&SITE=%s&ID=%s&PW=%s&PROGRAM=%s")
					, strBaseUrl
					, AsciiToBase64ForWeb(_T("UBC"))
					, AsciiToBase64ForWeb(GetEnvPtr()->m_szSite)
					, AsciiToBase64ForWeb(GetEnvPtr()->m_szLoginID)
					, AsciiToBase64ForWeb(GetEnvPtr()->m_szPassword)
					, AsciiToBase64ForWeb(strSubUrl)
					);
	}

	m_ieMain.Navigate(strUrl, NULL, NULL, NULL, NULL);
}

void CWebUserView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CWebUserView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_ieMain, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}

void CWebUserView::WindowClosingIeMain(BOOL IsChildWindow, BOOL* Cancel)
{
	// 브라우져의 종료확인 메세지를 안나오게 한다.
	*Cancel = TRUE;
	GetParent()->PostMessage(WM_CLOSE);
}
