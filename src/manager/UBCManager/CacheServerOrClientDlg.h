#pragma once
#include "afxwin.h"


// CCacheServerOrClientDlg 대화 상자입니다.

class CCacheServerOrClientDlg : public CDialog
{
	DECLARE_DYNAMIC(CCacheServerOrClientDlg)

public:
	CCacheServerOrClientDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCacheServerOrClientDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_CACHE_SERVER_OR_CLIENT };
	enum { NO_SET=-1, SERVER=0,  CLIENT=1};
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	CButton m_radioServer;
	CButton m_radioClient;

	int 	m_server_or_client;
};
