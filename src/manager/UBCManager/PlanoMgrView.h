#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"
#include "common\CheckComboBox.h"
#include "UBCCopCommon\CopModule.h"
#include "common\hoverbutton.h"


// CPlanoMgrView ��E� �����Դϴ�.

class CPlanoMgrView : public CFormView
{
	DECLARE_DYNCREATE(CPlanoMgrView)

public:
	CPlanoMgrView();   // ǥ�� �������Դϴ�.
	virtual ~CPlanoMgrView();

// ��E� ���� �������Դϴ�.
	enum { IDD = IDD_PLANOMGR };
	enum { eCheck, 
			ePlanoId,
			eSiteId,
			ePlanoType,
			eBackgroundFile,
			eExpand,
			eDescription,
			ePlanoMgrEnd };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV �����Դϴ�.

	DECLARE_MESSAGE_MAP()
public:

	void InitPosition(CRect rc);
	void InitAuthCtrl();
	void InitPlanoMgrList();
	BOOL RefreshPlanoMgr(bool bCompMsg=true);
	void RefreshPlanoMgrList(POSITION posStart=NULL);
	void UpdateListRow(int nRow, SPlanoInfo* pInfo);
	void ModifyPlanoMgr(CArray<int>& arRow, bool bMsg);
	void RemovePlanoMgr(CArray<int>& arRow, bool bMsg);

	CRect m_rcClient;
	CReposControl m_Reposition;
	CString		 m_szPlanoMgrColum[ePlanoMgrEnd];
	CImageList m_ilPlanoMgrList;
	PlanoInfoList	m_lsPlanoMgr;

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	CComboBox		m_cbPlanoType;

	CHoverButton m_btnRefPlanoMgr;
	CHoverButton m_btnCreatePlanoMgr;
	CHoverButton m_btnDeletePlanoMgr;

	CUTBListCtrlEx m_lscPlanoMgr;

	afx_msg void OnBnClickedButtonPlanoMgrRef();
	afx_msg void OnBnClickedButtonPlanoMgrCreate();
	afx_msg void OnBnClickedButtonPlanoMgrDel();
	virtual void OnInitialUpdate();
	afx_msg void OnNMDblclkListPlanoMgr(NMHDR *pNMHDR, LRESULT *pResult);
	CHoverButton m_btnIconSync;
	afx_msg void OnBnClickedButtonIconSync();
};
