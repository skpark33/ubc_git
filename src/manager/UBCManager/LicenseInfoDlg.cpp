// LicenseInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LicenseInfoDlg.h"
#include "common/TraceLog.h"


// CLicenseInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLicenseInfoDlg, CDialog)

CLicenseInfoDlg::CLicenseInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenseInfoDlg::IDD, pParent)
{
	TraceLog(("CLicenseInfoDlg"));

}

CLicenseInfoDlg::~CLicenseInfoDlg()
{
	TraceLog(("~CLicenseInfoDlg"));
}

void CLicenseInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_LICENSEINFO, LicenseInfoEditCtrl);
	DDX_Control(pDX, IDC_LICENSEINFOTXT, m_licenseInfoTxtCtrl);
}


BEGIN_MESSAGE_MAP(CLicenseInfoDlg, CDialog)
END_MESSAGE_MAP()


// CLicenseInfoDlg 메시지 처리기입니다.

BOOL CLicenseInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	TraceLog(("OnInitDialog"));

	CString buf;
	_getLicenseString(buf);
	
	m_licenseInfoTxtCtrl.SetWindowText(buf);
	
	//this->ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


#define	BUF_SIZE		(1024*1024)	// 파일 복사 버퍼 = 1 MByte
bool
CLicenseInfoDlg::_getLicenseString(CString& streamBuf)
{
	CString filename = "license_en.txt";
	unsigned short LangType = PRIMARYLANGID(GetSystemDefaultLangID());  //프라이머리 언어만 뽑는다. 
	if(LangType==LANG_JAPANESE) {   //일문 윈도우 
		filename = "license_jp.txt";
	}else if(LangType==LANG_KOREAN){
		filename = "license.txt";
	}

	CFile file;
	if (!file.Open(filename,CFile::modeRead | CFile::shareDenyWrite))	
	{
		TRACE(_T("Load (file): Error opening file %s\n"),filename);
		streamBuf="No License file here";
		return false;
	}

	BYTE* buf = new BYTE[BUF_SIZE];
	memset(buf,0x00,BUF_SIZE);
	int nRead;
	while((nRead = file.Read(buf, BUF_SIZE)) != 0)
	{
		buf[nRead] = 0;	
		streamBuf.Append((const char*)buf);
		memset(buf,0x00,BUF_SIZE);
	}
	file.Close();
	if(streamBuf.IsEmpty()){
		streamBuf="No License file here";
		return false;
	}
	return true;
}