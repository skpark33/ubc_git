#pragma once


// CWebPageFrame frame

class CWebPageFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CWebPageFrame)
protected:
	CWebPageFrame();           // protected constructor used by dynamic creation
	virtual ~CWebPageFrame();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


