// MainFrmBitmapBG.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMainFrmBitmapBG window

class CMainFrmBitmapBG : public CWnd
{
// Construction
public:
	CMainFrmBitmapBG();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrmBitmapBG)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrmBitmapBG();

	// Generated message map functions
protected:
	CBitmap m_bmp;
	//{{AFX_MSG(CMainFrmBitmapBG)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
