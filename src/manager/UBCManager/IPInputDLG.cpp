// IPInputDLG.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "IPInputDLG.h"


// CIPInputDLG 대화 상자입니다.

IMPLEMENT_DYNAMIC(CIPInputDLG, CDialog)

CIPInputDLG::CIPInputDLG(CWnd* pParent /*=NULL*/)
	: CDialog(CIPInputDLG::IDD, pParent)
{

}

CIPInputDLG::~CIPInputDLG()
{
}

void CIPInputDLG::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IPADDRESS1, ipEditCtrl);
}


BEGIN_MESSAGE_MAP(CIPInputDLG, CDialog)
	ON_BN_CLICKED(IDOK, &CIPInputDLG::OnBnClickedOk)
END_MESSAGE_MAP()


// CIPInputDLG 메시지 처리기입니다.

BOOL CIPInputDLG::OnInitDialog()
{
	CDialog::OnInitDialog();


	char oldIp[30];
	memset(oldIp,0x00,30);

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, NULL, NULL);

	CString szIniPath;
	szIniPath.Format(_T("%s%sdata\\%s"), szDrive, szPath, UBCVARS_INI);
	::GetPrivateProfileString("ROOT", "REPEATER_IP", _T(""),oldIp, sizeof(oldIp),szIniPath);

	if(strlen(oldIp) > 0){
		_ipAddress = oldIp;
	}

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	if(!_ipAddress.IsEmpty()){
		ipEditCtrl.SetWindowText(_ipAddress);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CIPInputDLG::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	_ipAddress = "";
	ipEditCtrl.GetWindowText(_ipAddress);
	if(!_ipAddress.IsEmpty()){
	
		TCHAR szModule[MAX_PATH];
		::ZeroMemory(szModule, MAX_PATH);
		::GetModuleFileName(NULL, szModule, MAX_PATH);

		TCHAR szDrive[MAX_PATH], szPath[MAX_PATH];
		_tsplitpath(szModule, szDrive, szPath, NULL, NULL);

		CString szIniPath;
		szIniPath.Format(_T("%s%sdata\\%s"), szDrive, szPath, UBCVARS_INI);
		::WritePrivateProfileString("ROOT", "REPEATER_IP", _ipAddress, szIniPath);
	
	}
	OnOK();
}
