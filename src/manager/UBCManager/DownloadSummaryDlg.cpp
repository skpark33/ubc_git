// DownloadSummaryDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "common\ubcdefine.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "DownloadSummaryDlg.h"
#include "ubccopcommon\SelectHostDlg.h"
#include "ubccopcommon\PackageSelectDlg.h"
#include "ubccopcommon\SelectUsers.h"
#include "ubccopcommon\SelectPlanDlg.h"
#include "DownloadStateDlg.h"
#include "HostExcelSave.h"

//다운로드 결과
enum E_DOWNLOAD_REASON
{
	E_REASON_INIT,    //초기상태
	E_REASON_CONNECTION_FAIL, //연결을 실패함
	E_REASON_FILE_NOTFOUND,  //서버에 파일이 존재하지 않음
	E_REASON_EXCEPTION_FTP,  //FTP 오류
	E_REASON_EXCEPTION_FILE, //파일액세스 오류
	E_REASON_LOCAL_EXIST,  //로컬에 존재하여 다운로드 하지 않음
	E_REASON_DOWNLOAD_SUCCESS, //다운로드 성공
	E_REASON_UNKNOWN,   //알수없는 오류(연결끊김, exception...)
	E_REASON_COPY_FAIL			//다운로드는 성공하였으나 ENC폴더 복사에 실패함.
};

//다운로드 결과
enum E_DOWNLOAD_STATE
{
	E_STATE_INIT			,  //초기상태
	E_STATE_DOWNLOADING		,  //다운로드 상태
	E_STATE_PARTIAL_FAIL	,  //일부파일 실패
	E_STATE_COMPLETE_FAIL	,  //전체 실패(접속실패등...)
	E_STATE_COMPLETE_SUCCESS,  //전체 성공
	E_STATE_COMPLETE_LOCAL_EXIST	//패키지의 모든컨텐츠 파일이 로컬에 존재하여 다운로드하지 않음
};

// 상태를 나타내는 이미지리스트 인덱스
enum E_STATE_BMP_INDEX
{
	E_STATE_IDX_OK
,	E_STATE_IDX_BLOCK
,	E_STATE_IDX_ERROR
,	E_STATE_IDX_WARNING
,	E_STATE_IDX_ING
,	E_STATE_IDX_MAX_COUNT
};

// CDownloadSummaryDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDownloadSummaryDlg, CDialog)

CDownloadSummaryDlg::CDownloadSummaryDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDownloadSummaryDlg::IDD, pParent)
	, m_Reposition(this)
	, m_bInitialize(false)
{
	BRW_NAME[0] = LoadStringById(IDS_DOWNLOADSTATE_STR001);
	BRW_NAME[1] = LoadStringById(IDS_DOWNLOADSTATE_STR002);
}

CDownloadSummaryDlg::~CDownloadSummaryDlg()
{
}

void CDownloadSummaryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_Filter);
	DDX_Control(pDX, IDC_LIST_DOWNLOAD, m_lcList);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_bnClose);
	DDX_Control(pDX, IDC_BUTTON_FILTER_BP_PACKAGE, m_cbBpPackage);
	DDX_Control(pDX, IDC_BUTTON_FILTER_BP_HOST, m_cbBpHost);
	DDX_Control(pDX, IDC_BN_TO_EXCEL, m_btnExcelSave);
}

BEGIN_MESSAGE_MAP(CDownloadSummaryDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BUTTON_FILTER_HOST, &CDownloadSummaryDlg::OnBnClickedButtonFilterHost)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_DOWNLOAD, &CDownloadSummaryDlg::OnNMDblclkListDownload)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_REGISTER, &CDownloadSummaryDlg::OnBnClickedButtonFilterRegister)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PACKAGE, &CDownloadSummaryDlg::OnBnClickedButtonFilterPackage)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PLAN_NAME, &CDownloadSummaryDlg::OnBnClickedButtonFilterPlanName)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CDownloadSummaryDlg::OnBnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CDownloadSummaryDlg::OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BN_TO_EXCEL, &CDownloadSummaryDlg::OnBnClickedBnToExcel)
END_MESSAGE_MAP()


// CDownloadSummaryDlg 메시지 처리기입니다.

BOOL CDownloadSummaryDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnClose.LoadBitmap(IDB_BUTTON_CLOSE, RGB(255,255,255));
	m_btnExcelSave.LoadBitmap(IDB_BUTTON_EXCEL, RGB(255,255,255));

	m_bnRefresh.SetToolTipText(LoadStringById(IDS_DOWNLOADSUMMARY_BTN001));

	m_Reposition.AddControl(&m_lcList, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_Filter, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_Reposition.AddControl(&m_bnClose, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_Reposition.AddControl(&m_btnExcelSave, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);

	SetDlgItemText(IDC_EDIT_FILTER_PLAN_NAME, m_stListFilter.strBpName);
	m_strBpId = m_stListFilter.strBpId;

	SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, m_stListFilter.strPackageId);

	SetDlgItemText(IDC_EDIT_FILTER_HOST, m_stListFilter.strHostName);
	m_strHostId = m_stListFilter.strHostId;

	InitList();
	InitBpPackageAndHostCombo();
//	RefreshList(); // 2012.05.07 화면열때 조회시 너무 오래 걸리므로 화면이 열린후 수동 조회

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDownloadSummaryDlg::InitBpPackageAndHostCombo()
{
	m_cbBpPackage.GetData()->RemoveAll();
	m_cbBpHost.GetData()->RemoveAll();

	m_cbBpPackage.EnableWindow(!m_strBpId.IsEmpty());
	m_cbBpHost.EnableWindow(!m_strBpId.IsEmpty());

	if(m_strBpId.IsEmpty()) return;

	// 패키지조회
	m_lsTplist.RemoveAll();
	BOOL bResult = CCopModule::GetObject()->GetTimePlan((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
														, m_strBpId
														, m_lsTplist
														);

	POSITION posTP = m_lsTplist.GetHeadPosition();
	while(posTP)
	{
		POSITION posOld = posTP;
		STimePlanInfo& infoTP = m_lsTplist.GetNext(posTP);
		m_cbBpPackage.AddString(infoTP.strProgramId, (long)posOld);
	}

	m_cbBpPackage.CheckAll(TRUE);

	// 단말조회
	m_lsThlist.RemoveAll();
	bResult = CCopModule::GetObject()->GetTargetHost((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
														, m_strBpId
														, m_lsThlist
														);

	POSITION posTH = m_lsThlist.GetHeadPosition();
	while(posTH)
	{
		POSITION posOld = posTH;
		STargetHostInfo& info = m_lsThlist.GetNext(posTH);

		CString strHostId;
		int nPos = info.strTargetHost.ReverseFind('=');
		if(nPos >= 0)
		{
			strHostId = info.strTargetHost.Mid(nPos+1);
			m_cbBpHost.AddString(strHostId, (long)posOld);
		}
	}

	m_cbBpHost.CheckAll(TRUE);
}

void CDownloadSummaryDlg::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eStatus   ] = LoadStringById(IDS_DOWNLOADSTATE_LIST001);
	m_szColum[eSite     ] = LoadStringById(IDS_DOWNLOADSUMMARY_BTN002);
	m_szColum[eHostName ] = LoadStringById(IDS_DOWNLOADSTATE_LIST011);
	m_szColum[eHostId   ] = LoadStringById(IDS_DOWNLOADSUMMARY_BTN003);	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다
	
	if(GetEnvPtr()->m_strCustomer == "KPOST")
	{
		m_szColum[eBrwId    ] = "Size";
	}
	else
	{
		m_szColum[eBrwId    ] = LoadStringById(IDS_DOWNLOADSTATE_LIST002);
	}
	m_szColum[ePackage  ] = LoadStringById(IDS_DOWNLOADSTATE_LIST003);
	m_szColum[eProgress] = LoadStringById(IDS_DOWNLOADSTATE_LIST007);
	m_szColum[eStartTime] = LoadStringById(IDS_DOWNLOADSTATE_LIST008);
	m_szColum[eEndTime  ] = LoadStringById(IDS_DOWNLOADSTATE_LIST009);

	m_lcList.InsertColumn(eStatus   , m_szColum[eStatus   ], LVCFMT_CENTER, 100);
	m_lcList.InsertColumn(eSite     , m_szColum[eSite     ], LVCFMT_CENTER,  60);
	m_lcList.InsertColumn(eHostName , m_szColum[eHostName ], LVCFMT_LEFT  ,  70);	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다
	m_lcList.InsertColumn(eHostId   , m_szColum[eHostId   ], LVCFMT_CENTER,  70);
	m_lcList.InsertColumn(eBrwId    , m_szColum[eBrwId    ], LVCFMT_CENTER,  30);
	m_lcList.InsertColumn(ePackage  , m_szColum[ePackage  ], LVCFMT_LEFT  , 200);
	m_lcList.InsertColumn(eProgress, m_szColum[eProgress], LVCFMT_CENTER, 100);
	m_lcList.InsertColumn(eStartTime, m_szColum[eStartTime], LVCFMT_CENTER, 120);
	m_lcList.InsertColumn(eEndTime  , m_szColum[eEndTime  ], LVCFMT_CENTER, 120);

	//
	CBitmap bmp;
	bmp.LoadBitmap(IDB_OK_BLOCK_ERROR_WARNING);
	if(m_ilListImage.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		m_ilListImage.Add(&bmp, RGB(255,255,255));
		m_lcList.SetImageList(&m_ilListImage, LVSIL_SMALL);
	}

	// 제일 마지막에 할것...
	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CDownloadSummaryDlg::RefreshList()
{
	CWaitMessageBox wait;

	m_lcList.DeleteAllItems();
	m_lsInfoList.RemoveAll();

	CString strWhere;
	CString strTmp;

	//skpark 2015.04.16 호스트명을 보이게 하기 위해..추가
	CMapStringToString mapHostPackage;

	// 방송계획
	if(!m_stListFilter.strBpId.IsEmpty())
	{
		CString strBpPackageList = m_stListFilter.strBpPackageList;
		CString strBpHostList = m_stListFilter.strBpHostList;

		CMapStringToString mapBpHostName;

		if(strBpPackageList.IsEmpty())
		{
			POSITION posTP = m_lsTplist.GetHeadPosition();
			while(posTP)
			{
				POSITION posOld = posTP;
				STimePlanInfo& infoTP = m_lsTplist.GetNext(posTP);
				strBpPackageList += infoTP.strProgramId + _T(",");
			}
		}


		if(strBpHostList.IsEmpty())
		{
			POSITION posTH = m_lsThlist.GetHeadPosition();
			while(posTH)
			{
				POSITION posOld = posTH;
				STargetHostInfo& info = m_lsThlist.GetNext(posTH);

				CString strHostId;
				int nPos = info.strTargetHost.ReverseFind('=');
				if(nPos >= 0)
				{
					strHostId = info.strTargetHost.Mid(nPos+1);
					//skpark 2015.04.16 호스트명을 보이게 하기 위해..추가
					//strBpHostList += strHostId + _T(",");
					mapBpHostName[strHostId]  = info.strHostName;
				}
			}
		}else{
			//skpark 2015.04.16 호스트명을 보이게 하기 위해..추가
			int nHostPos=0;
			CString strHostId;
			while( !(strHostId = strBpHostList.Tokenize(_T(","), nHostPos)).IsEmpty() )
			{
					mapBpHostName[strHostId]  = "";
			}
		}

		int nPackagePos=0;
		CString strPackage;
		while( !(strPackage = strBpPackageList.Tokenize(_T(","), nPackagePos)).IsEmpty() )
		{
			//skpark 2015.04.16 호스트명을 보이게 하기 위해..추가

			//int nHostPos=0;
			//CString strHost;
			//while( !(strHost = strBpHostList.Tokenize(_T(","), nHostPos)).IsEmpty() )
			//{
			//	mapHostPackage[strPackage + _T("/") + strHost] = _T("");
			//}
			POSITION posHost = mapBpHostName.GetStartPosition();
			while(posHost != NULL)
			{
				CString strHost;
				CString strHostName;
				mapBpHostName.GetNextAssoc( posHost, strHost, strHostName );
				mapHostPackage[strPackage + _T("/") + strHost] = strHostName;
			}
		}

		strTmp.Format(_T("programId in (select programId from ubc_tp where bpId = '%s')"), m_stListFilter.strBpId);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;

		//strTmp.Format(_T("hostId in (select RIGHT(targetHost, LEN(targetHost) - (CHARINDEX('Host=', targetHost)+4)) from ubc_th where bpId = '%s')"), m_stListFilter.strBpId);
		strTmp.Format(_T("hostId in (select dbo.Find_n_Right('Host=', targetHost) from ubc_th where bpId = '%s')"), m_stListFilter.strBpId);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;

		if(!m_stListFilter.strBpPackageList.IsEmpty())
		{
			m_stListFilter.strBpPackageList.Replace(",", "','");
			strTmp.Format(_T("programId in ('%s')"), m_stListFilter.strBpPackageList);
			strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
		}

		if(!m_stListFilter.strBpHostList.IsEmpty())
		{
			m_stListFilter.strBpHostList.Replace(",", "','");
			strTmp.Format(_T("hostId in ('%s')"), m_stListFilter.strBpHostList);
			strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
		}
	}

	// 포함패키지
	if(!m_stListFilter.strPackageId.IsEmpty())
	{
		strTmp.Format(_T("lower(programId) like lower('%%%s%%')"), m_stListFilter.strPackageId);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	// 포함단말
	if(!m_stListFilter.strHostId.IsEmpty())
	{
		strTmp.Format(_T("hostId = '%s'"), m_stListFilter.strHostId);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	strWhere += _T(" order by programStartTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetDownloadSummary( (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
													, _T("*")
													, m_lsInfoList
													, strWhere ))
	{
		return;
	}

	CMapStringToString mapHostState;
	CCopModule::GetObject()->GetOperationalStates(mapHostPackage,mapHostState);


	m_lcList.SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SDownloadSummaryInfo& info = m_lsInfoList.GetNext(pos);

		CTime tmStart = CTime(info.programStartTime);
		CTime tmEnd   = CTime(info.programEndTime  );

		COLORREF clrBackColor = RGB(255, 255, 255);

		int nStateIdx = 0;
		if( info.programState == E_STATE_PARTIAL_FAIL  ||
			info.programState == E_STATE_COMPLETE_FAIL )
		{
			nStateIdx = 2;
			clrBackColor = RGB(255,128,128);
		}
		else if(info.programState == E_STATE_DOWNLOADING)
		{
			nStateIdx = 4;
			clrBackColor = RGB(255,255,0);
		}

		m_lcList.InsertItem(nRow, GetStateString(info.programState)+mapHostState[info.hostId], nStateIdx);
		m_lcList.SetItemText(nRow, eSite     , info.siteName        );
		m_lcList.SetItemText(nRow, eHostName , info.hostName        );	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다
		m_lcList.SetItemText(nRow, eHostId   , info.hostId          );
		
		if(GetEnvPtr()->m_strCustomer == "KPOST")
		{
			CString MbyteStr;
			MbyteStr.Format("%d MB", info.brwId);
			m_lcList.SetItemText(nRow, eBrwId    , MbyteStr );
		}
		else
		{
			m_lcList.SetItemText(nRow, eBrwId    , BRW_NAME[info.brwId] );
		}
		
		m_lcList.SetItemText(nRow, ePackage  , info.programId       );
		m_lcList.SetItemText(nRow, eProgress  , info.progress       );
		m_lcList.SetItemText(nRow, eStartTime, (info.programStartTime == 0 ? "" : tmStart.Format("%Y/%m/%d %H:%M:%S")));
		m_lcList.SetItemText(nRow, eEndTime  , (info.programEndTime   == 0 ? "" : tmEnd  .Format("%Y/%m/%d %H:%M:%S")));

		m_lcList.SetRowColor(nRow, clrBackColor, RGB(0,0,0));
		m_lcList.SetItemData(nRow, (DWORD_PTR)posOld);

		mapHostPackage.RemoveKey(info.programId + _T("/") + info.hostId);

		nRow++;
	}

	// 배포결과가 없는 단말 과 패키지는 따로 표시한다.


	POSITION posFail = mapHostPackage.GetStartPosition();
	while(posFail != NULL)
	{
		CString strKey;
		CString strHostName;
		mapHostPackage.GetNextAssoc( posFail, strKey, strHostName );

		int nPos = 0;
		CString strPackage = strKey.Tokenize("/", nPos);
		CString strHostId = strKey.Tokenize("/", nPos);

		CString operationalState = mapHostState[strHostId];

		m_lcList.InsertItem(nRow, GetStateString(-1)+operationalState, 3);
		//m_lcList.SetItemText(nRow, eSite   , info.siteId          );
		m_lcList.SetItemText(nRow, eHostName , strHostName          );	// 0001411: 콘텐츠 패키지 배포 현황 보기에서 단말 ID 로만 보면 불편하므로, 단말명을 추가한다
		m_lcList.SetItemText(nRow, eHostId   , strHostId            );
		//m_lcList.SetItemText(nRow, eBrwId  , BRW_NAME[info.brwId] );
		m_lcList.SetItemText(nRow, ePackage  , strPackage       );
		//m_lcList.SetItemText(nRow, eStartTime, (info.programStartTime == 0 ? "" : tmStart.Format("%Y/%m/%d %H:%M:%S")));
		//m_lcList.SetItemText(nRow, eEndTime  , (info.programEndTime   == 0 ? "" : tmEnd  .Format("%Y/%m/%d %H:%M:%S")));

		//m_lcList.SetItemData(nRow, (DWORD_PTR)posOld);

		m_lcList.SetRowColor(nRow, RGB(200,200,200), RGB(0,0,0)); 

		nRow++;
	}

	int nCol = m_lcList.GetCurSortCol();
	if(-1 != nCol)
		m_lcList.SortList(nCol, m_lcList.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);
	else
		m_lcList.SortList(eHostName, m_lcList.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);

	m_lcList.SetRedraw(TRUE);
}

int CDownloadSummaryDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(m_rcClient);

	m_Reposition.SetParentRect(m_rcClient);

	return 0;
}

void CDownloadSummaryDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CDownloadSummaryDlg::OnBnClickedButtonFilterHost()
{
	CSelectHostDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	m_strHostId = _T("");
	SetDlgItemText(IDC_EDIT_FILTER_HOST, _T(""));

	if(dlg.DoModal() == IDOK)
	{
		m_strHostId = dlg.m_strHostId;
		SetDlgItemText(IDC_EDIT_FILTER_HOST, dlg.m_strHostName);
	}
}

void CDownloadSummaryDlg::OnBnClickedButtonRefresh()
{
	GetDlgItemText(IDC_EDIT_FILTER_PLAN_NAME, m_stListFilter.strBpName  );
	m_stListFilter.strBpId = m_strBpId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , m_stListFilter.strPackageId);
	GetDlgItemText(IDC_EDIT_FILTER_HOST     , m_stListFilter.strHostName);
	m_stListFilter.strHostId = m_strHostId;

	CString strBpPackageList = m_cbBpPackage.GetCheckedTexts();
	if(strBpPackageList == _T("All")) strBpPackageList = _T("");
	m_stListFilter.strBpPackageList = strBpPackageList;

	CString strBpHostList = m_cbBpHost.GetCheckedTexts();
	if(strBpHostList == _T("All")) strBpHostList = _T("");
	m_stListFilter.strBpHostList = strBpHostList;

	RefreshList();
}

BOOL CDownloadSummaryDlg::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_PLAN_NAME)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_TAG      )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_REGISTER )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_PACKAGE  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_DATE_FILTER_START    )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_DATE_FILTER_END      )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOST     )->GetSafeHwnd() )
		{
			OnBnClickedButtonRefresh();
			return TRUE;
		}
	}

	return __super::PreTranslateMessage(pMsg);
}

void CDownloadSummaryDlg::OnBnClickedButtonFilterRegister()
{
	CSelectUsers dlg;
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_REGISTER, _T(""));
		return;
	}

	UserInfoList& list = dlg.GetSelectedList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SUserInfo info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_REGISTER, info.userId);
	}
}

void CDownloadSummaryDlg::OnBnClickedButtonFilterPlanName()
{
	CSelectPlanDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);
	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PLAN_NAME, _T(""));
		m_strBpId = _T("");
	}

	SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
	m_strHostId = _T("");
	SetDlgItemText(IDC_EDIT_FILTER_HOST, _T(""));

	BroadcastingPlanInfoList& list = dlg.GetSelectedList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SBroadcastingPlanInfo info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PLAN_NAME, info.strBpName);
		m_strBpId = info.strBpId;
	}

	InitBpPackageAndHostCombo();
}

void CDownloadSummaryDlg::OnBnClickedButtonFilterPackage()
{
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, info.szPackage);
	}
}

CString CDownloadSummaryDlg::GetStateString(int nValue)
{
	switch(nValue)
	{
	case E_STATE_INIT			     : return LoadStringById(IDS_DOWNLOADSTATE_STR011); //초기상태
	case E_STATE_DOWNLOADING	     : return LoadStringById(IDS_DOWNLOADSTATE_STR012); //다운로드 상태
	case E_STATE_PARTIAL_FAIL	     : return LoadStringById(IDS_DOWNLOADSTATE_STR013); //일부파일 실패
	case E_STATE_COMPLETE_FAIL	     : return LoadStringById(IDS_DOWNLOADSTATE_STR014); //전체 실패
	case E_STATE_COMPLETE_SUCCESS    : return LoadStringById(IDS_DOWNLOADSTATE_STR015); //전체 성공
	case E_STATE_COMPLETE_LOCAL_EXIST: return LoadStringById(IDS_DOWNLOADSTATE_STR015); //전체 성공
	default: return LoadStringById(IDS_DOWNLOADSTATE_STR017); //시작안함
	}

	return _T("");
}

void CDownloadSummaryDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	CDialog::OnGetMinMaxInfo(lpMMI);

	if(m_bInitialize)
	{
		lpMMI->ptMinTrackSize.x = m_rcClient.Width() + ::GetSystemMetrics(SM_CXDLGFRAME);
		lpMMI->ptMinTrackSize.y = m_rcClient.Height() + ::GetSystemMetrics(SM_CXDLGFRAME);
	}
	else
	{
		m_bInitialize = true;
	}
}

void CDownloadSummaryDlg::OnBnClickedButtonClose()
{
	OnOK();
}

void CDownloadSummaryDlg::OnNMDblclkListDownload(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	if(pNMLV->iItem < 0) return;
	*pResult = 0;

	POSITION pos = (POSITION)m_lcList.GetItemData(pNMLV->iItem);
	if(!pos) return;

	SDownloadSummaryInfo info = m_lsInfoList.GetAt(pos);

	CDownloadStateDlg dlg;
	dlg.m_Info = info;
	//dlg.m_strSiteId = info.siteId;
	//dlg.m_strHostId = info.hostId;
	//dlg.m_strPackageId = info.programId;
	//dlg.m_nBrwId = info.brwId;

	dlg.DoModal();
}

void CDownloadSummaryDlg::OnBnClickedBnToExcel()
{
	CHostExcelSave dlg;

	CString strExcelFile = dlg.Save(  LoadStringById(IDS_DOWNLOADSTATE_STR016)
									, m_lcList
									);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}
