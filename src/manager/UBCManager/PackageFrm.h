#pragma once


// CPackageFrm frame

class CPackageFrm : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CPackageFrm)
protected:
	CPackageFrm();           // protected constructor used by dynamic creation
	virtual ~CPackageFrm();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


