// SiteDelDlg.cpp : implementation file
//
#include "stdafx.h"
#include "SiteDelDlg.h"
#include "Enviroment.h"

#define STR_SITE_TYPE	LoadStringById(IDS_SITEDELDLG_LST001)
#define STR_SITE_NAME	LoadStringById(IDS_SITEDELDLG_LST002)

// CSiteDelDlg dialog
IMPLEMENT_DYNAMIC(CSiteDelDlg, CDialog)

CSiteDelDlg::CSiteDelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSiteDelDlg::IDD, pParent)
{
}

CSiteDelDlg::~CSiteDelDlg()
{
}

void CSiteDelDlg::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_LIST_SITE, m_lscSite);
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSiteDelDlg, CDialog)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

// CSiteDelDlg message handlers
BOOL CSiteDelDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	InitList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CSiteDelDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CSiteDelDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnClose();
}

void CSiteDelDlg::SetList(std::list<std::string>& slHost, std::list<std::string>& slPackage)
{
	m_slHost = slHost;
	m_slPackage = slPackage;
}

void CSiteDelDlg::InitList()
{
	m_szSiteColum[eType] = STR_SITE_TYPE;
	m_szSiteColum[eName] = STR_SITE_NAME;

	m_lscSite.SetExtendedStyle(m_lscSite.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	for(int nCol = 0; nCol < eEnd; nCol++){
		m_lscSite.InsertColumn(nCol, m_szSiteColum[nCol], LVCFMT_LEFT, 100);
	}

	m_lscSite.InitHeader(IDB_LIST_HEADER);
	m_lscSite.SetColumnWidth(eName, 300);

	int nRow = -1;

	for(std::list<std::string>::iterator i = m_slHost.begin(); i != m_slHost.end(); i++){
//		TraceLog(("%s",(*i).c_str()));
		m_lscSite.InsertItem(++nRow, LoadStringById(IDS_SITEDELDLG_STR001));
		m_lscSite.SetItemText(nRow, eName, (*i).c_str());
	}

	for(std::list<std::string>::iterator i = m_slPackage.begin(); i != m_slPackage.end(); i++){
//		TraceLog(("%s",(*i).c_str()));
		m_lscSite.InsertItem(++nRow, LoadStringById(IDS_SITEDELDLG_STR002));
		m_lscSite.SetItemText(nRow, eName, (*i).c_str());
	}
}