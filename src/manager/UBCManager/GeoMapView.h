#pragma once

#include "common\libcommon\ubchost.h"
#include "common\HoverButton.h"
#include "ie_bulletinboard.h"
#include "common\reposcontrol.h"
#include "ubccopcommon\eventhandler.h"
#include "common\CheckComboBox.h"
#include "afxwin.h"

// CGeoMapView form view
class CGeoMapView : public CFormView
{
	DECLARE_DYNCREATE(CGeoMapView)

private:
	void InitPosition(CRect);
	void QueryHost(int, int);

//	virtual int InvokeEvent(WPARAM, LPARAM);
	LRESULT InvokeEvent(WPARAM, LPARAM);

public:

private:
	CIe_bulletinboard m_ieMain;

	CHoverButton m_btnRefHost;

	CStatic m_Filter;
	CComboBox m_cbAdmin;
	CComboBox m_cbOperation;
	//CComboBox m_cbHostType;
	CCheckComboBox m_cbHostType;

	CRect m_rcClient;
	CReposControl m_Reposition;
	ScreenShotInfo m_ssinfo;

//	CList<int> m_lsEvID;
//	CEventManager m_EventManager;
//	static UINT AddEventThread(LPVOID pParam);

	void LoadFilterData();
	void SaveFilterData();
	CString m_strSiteId;
	CString m_strContentsId;
	CString m_strPackageId;

protected:
	CGeoMapView();           // protected constructor used by dynamic creation
	virtual ~CGeoMapView();

public:
	enum { IDD = IDD_GEOMAPVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	DECLARE_EVENTSINK_MAP()
	void WindowClosingIeMain(BOOL IsChildWindow, BOOL* Cancel);

	virtual void OnInitialUpdate();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnBnClickedButtonHostrfs();
	afx_msg void OnBnClickedButtonFilterSite();
	afx_msg void OnBnClickedButtonFilterContents();
	afx_msg void OnBnClickedButtonFilterPackage();
	afx_msg LRESULT OnFilterHostChanged(WPARAM wParam, LPARAM lParam);
	CComboBox m_cbFilterTag;
	CButton m_kbAdvancedFilter;
	CComboBox m_cbAdvancedFilter;
	afx_msg void OnBnClickedKbAdvancedFilter();
};


