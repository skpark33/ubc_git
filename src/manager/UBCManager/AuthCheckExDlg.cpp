// AuthCheckExDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "AuthCheckExDlg.h"
#include "TraceLog.h"

#include "common/libHttpRequest/HttpRequest.h"

#include <io.h>
#include <string.h>

DWORD CAuthCheckExDlg::LICENSE_INFO::compareMask = 0;


// CAuthCheckExDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAuthCheckExDlg, CDialog)

CAuthCheckExDlg::CAuthCheckExDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAuthCheckExDlg::IDD, pParent)
{
	// KPOST인 경우 mgrid, hostname, ipAddress, authDate는 비교 생략
	LICENSE_INFO::compareMask =	LICENSE_COMP_SITEID | LICENSE_COMP_HOSTID | 
								LICENSE_COMP_MACADDRESS | LICENSE_COMP_EDITION | LICENSE_COMP_ENTKEY ;
	// 기타 사이트가 추가될경우 CEnviroment::GetObject()->m_Customer에 따라 비교(if)문 추가 필요
}

CAuthCheckExDlg::~CAuthCheckExDlg()
{
}

void CAuthCheckExDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_LICENSE_FILE, m_editLicenseFile);
	DDX_Control(pDX, IDC_LIST_LICENSE, m_lcLicense);
	DDX_Control(pDX, IDOK, m_btnOK);
}


BEGIN_MESSAGE_MAP(CAuthCheckExDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_FIND_LICENSE_FILE, &CAuthCheckExDlg::OnBnClickedButtonFindLicenseFile)
END_MESSAGE_MAP()


// CAuthCheckExDlg 메시지 처리기입니다.

BOOL CAuthCheckExDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_lcLicense.SetExtendedStyle(m_lcLicense.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );

	m_lcLicense.InsertColumn(0, "단말ID", 0, 100);
	m_lcLicense.InsertColumn(1, "MAC주소", 0, 150);
	m_lcLicense.InsertColumn(2, "처리할 작업", 0, 100);
	m_lcLicense.InsertColumn(3, "처리한 결과", 0, 100);

	m_btnOK.EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAuthCheckExDlg::OnOK()
{
	CWaitMessageBox wait;

	bool all_succ = true;
	int cnt = m_arrProcessingHost.GetCount();
	for(int i=0; i<cnt; i++)
	{
		LICENSE_INFO& info = m_arrProcessingHost.GetAt(i);

		CHttpRequest http_request;
		http_request.Init(CHttpRequest::CONNECTION_SERVER);

		BOOL ret = FALSE;
		CString str_param = "";
		switch( info.eProcType )
		{
		case eAdding:
			str_param.Format("proc=crt&mgrId=%s&siteId=%s&hostId=%s&hostName=%s&ipAddress=%s&macAddress=%s&edition=%s&entKey=%s"
								, info.mgrId
								, info.siteId
								, info.hostId
								, info.hostName
								, info.ipAddress
								, info.macAddress
								, info.edition
								, info.enterpriseKey );
			break;
		case eUpdating:
			str_param.Format("proc=upd&mgrId=%s&siteId=%s&hostId=%s&hostName=%s&ipAddress=%s&macAddress=%s&edition=%s&entKey=%s"
								, info.mgrId
								, info.siteId
								, info.hostId
								, info.hostName
								, info.ipAddress
								, info.macAddress
								, info.edition
								, info.enterpriseKey );
			break;
		case eDeleting:
			str_param.Format("proc=rmv&hostId=%s", info.hostId );
			break;
		}

		CString str_result = "";
		if( !http_request.RequestPost("/UBC_Registration/process_auth.asp", str_param, str_result) )
		{
			m_lcLicense.SetItemText(i, 3, "실패");
			continue;
		}

		if( _strnicmp(str_result, "OK", 2) != 0 )
		{
			all_succ = false;
			m_lcLicense.SetItemText(i, 3, "실패");
		}
		else
		{
			m_lcLicense.SetItemText(i, 3, "성공");
		}
	}

	if( all_succ == false )
	{
		wait.Hide();
		UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG008), MB_ICONSTOP); // 실패한 라이센스 작업이 있습니다 !!!
		return;
	}

	wait.Hide();
	UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG009), MB_ICONINFORMATION); // 모든 라이센스 작업이 완료되었습니다
	CDialog::OnOK();
}

void CAuthCheckExDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnCancel();
}

void CAuthCheckExDlg::OnBnClickedButtonFindLicenseFile()
{
	//
	m_arrProcessingHost.RemoveAll();
	m_lcLicense.DeleteAllItems();

	//
	CString filter = "UBC License File (*.dat)|*.dat||";

	CFileDialog dlg(TRUE, "dat", NULL, OFN_DONTADDTORECENT | OFN_ENABLESIZING | OFN_FILEMUSTEXIST, filter, this);
	if( dlg.DoModal() != IDOK )
	{
		m_btnOK.EnableWindow(FALSE);
		m_editLicenseFile.SetWindowText("");
		return;
	}

	CString str_dat_path = dlg.GetPathName();
	m_strDatFilename = dlg.GetFileTitle();
	m_editLicenseFile.SetWindowText(str_dat_path);

	CWaitMessageBox wait;

	if( _access(str_dat_path, 04) != 0 )
	{
		m_btnOK.EnableWindow(FALSE);
		wait.Hide();
		UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG001), MB_ICONSTOP); // 신규 라이센스 파일을 열 수 없습니다 !!!
		return;
	}

	CFile file;
	if( !file.Open(str_dat_path, CFile::modeRead | CFile::typeBinary) )
	{
		m_btnOK.EnableWindow(FALSE);
		wait.Hide();
		UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG001), MB_ICONSTOP); // 신규 라이센스 파일을 열 수 없습니다 !!!
		return;
	}

	CString str_buf;

	// read from file
	ULONGLONG filesize = file.GetLength();
	char* buf = new char[filesize+1];
	::ZeroMemory(buf, filesize+1);

	file.Read(buf, filesize);
	file.Close();
	str_buf = buf;
	delete buf;

	// decoding from base64
	CString str_decoding = Base64ToAscii(str_buf);

	// write to file
	str_dat_path.Format("%sdata\\%s.ini", GetAppPath(), dlg.GetFileTitle());
	if( !file.Open(str_dat_path, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
	{
		m_btnOK.EnableWindow(FALSE);
		wait.Hide();
		UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG002), MB_ICONSTOP); // 신규 라이센스 파일을 처리할 수 없습니다 !!!
		return;
	}

	file.Write((LPCSTR)str_decoding, str_decoding.GetLength());
	file.Close();

	//
	// get hostidlist from new
	//
	buf = new char[1024*1024];
	::ZeroMemory(buf, 1024*1024);
	::GetPrivateProfileString("Root", "HostIdList", "", buf, 1024*1024-1, str_dat_path);
	CString str_new_hostidlist = buf;
	str_new_hostidlist.Trim();
	if( str_new_hostidlist.GetLength() == 0 )
	{
		m_btnOK.EnableWindow(FALSE);
		wait.Hide();
		UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG003), MB_ICONSTOP); // 정상적인 라이센스 파일이 아닙니다 !!!
		return;
	}

	// get license-info(encoded by base64) from 운영svr
	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_SERVER);

	str_buf = "";
	if( !http_request.RequestPost("/UBC_Registration/get_auth_list.asp", "", str_buf) )
	{
		m_btnOK.EnableWindow(FALSE);
		wait.Hide();
		UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG004), MB_ICONSTOP); // 운영서버에 접속할 수 없습니다 !!!
		return;
	}

	// decoding from base64
	//TraceLog(("RequestPost(%s)", str_buf));
	str_decoding = Base64ToAscii(str_buf);
	//TraceLog(("Decoding(%s)", str_decoding));

	if( _strnicmp(str_decoding, "OK", 2) != 0 )
	{
		m_btnOK.EnableWindow(FALSE);
		wait.Hide();
		UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG005), MB_ICONSTOP); // 운영서버로부터 라이센스 정보를 가져올 수 없습니다 !!!
		return;
	}

	// write to file
	CString str_old_path;
	str_old_path.Format("%sdata\\%s.old", GetAppPath(), dlg.GetFileTitle());
	if( !file.Open(str_old_path, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
	{
		m_btnOK.EnableWindow(FALSE);
		wait.Hide();
		UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG006), MB_ICONSTOP); // 운영 라이센스 파일을 처리할 수 없습니다 !!!
		return;
	}

	file.Write((LPCSTR)str_decoding + 2, str_decoding.GetLength()-2);
	file.Close();

	//
	// get hostidlist from old
	//
	::ZeroMemory(buf, 1024*1024);
	::GetPrivateProfileString("Root", "HostIdList", "", buf, 1024*1024-1, str_old_path);
	CString str_old_hostidlist = buf;

	// get deleting host list
	CString str_delete_hostidlist = "";
	int pos = 0;
	CString str_hostid = str_old_hostidlist.Tokenize(",", pos);
	while(str_hostid != "")
	{
		str_hostid.Trim();
		if( str_hostid != "" )
		{
			::ZeroMemory(buf, 1024*1024);
			::GetPrivateProfileString(str_hostid, "hostId", "", buf, 1024*1024-1, str_dat_path);
			CString hostid = buf;
			hostid.Trim();
			if( hostid == "" )
			{
				LICENSE_INFO info;
				GetLicenseInfo(str_hostid, str_old_path, info);
				info.eProcType = eDeleting;
				m_arrProcessingHost.Add(info);

				str_delete_hostidlist += str_hostid;
				str_delete_hostidlist += ",";

				TraceLog(("DeletingHost(%s)", info.toString()));
			}
		}
		str_hostid = str_old_hostidlist.Tokenize(",", pos);
	}

	// get adding/updating host list
	CString str_add_hostidlist = "";
	pos = 0;
	str_hostid = str_new_hostidlist.Tokenize(",", pos);
	while(str_hostid != "")
	{
		str_hostid.Trim();
		if( str_hostid != "" )
		{
			::ZeroMemory(buf, 1024*1024);
			::GetPrivateProfileString(str_hostid, "hostId", "", buf, 1024*1024-1, str_old_path);
			CString hostid = buf;
			hostid.Trim();
			if( hostid == "" )
			{
				LICENSE_INFO info;
				GetLicenseInfo(str_hostid, str_dat_path, info);
				info.eProcType = eAdding;
				m_arrProcessingHost.Add(info);

				str_add_hostidlist += str_hostid;
				str_add_hostidlist += ",";

				TraceLog(("AddingHost(%s)", info.toString()));
			}
			else
			{
				LICENSE_INFO new_info;
				GetLicenseInfo(str_hostid, str_dat_path, new_info);

				LICENSE_INFO old_info;
				GetLicenseInfo(str_hostid, str_old_path, old_info);

				if( new_info != old_info )
				{
					new_info.eProcType = eUpdating;
					m_arrProcessingHost.Add(new_info);

					TraceLog(("UpdatingHost(new:%s)", new_info.toString()));
					TraceLog(("UpdatingHost(old:%s)", old_info.toString()));
				}
			}
		}
		str_hostid = str_new_hostidlist.Tokenize(",", pos);
	}

	//
	if( m_arrProcessingHost.GetCount() == 0 )
	{
		m_btnOK.EnableWindow(FALSE);
		wait.Hide();
		UbcMessageBox(LoadStringById(IDS_AUTHCHECK_MSG007), MB_ICONINFORMATION); // 라이센스 정보가 동일합니다
		CDialog::OnCancel();
		return;
	}

	m_lcLicense.SetRedraw(FALSE);

	::WritePrivateProfileString("Root", "AddHostIdList", str_add_hostidlist, str_dat_path);
	::WritePrivateProfileString("Root", "DeleteHostIdList", str_delete_hostidlist, str_dat_path);

	int idx = 0;
	for(int i=0; i<m_arrProcessingHost.GetCount(); i++,idx++)
	{
		LICENSE_INFO& info = m_arrProcessingHost.GetAt(i);
		m_lcLicense.InsertItem(idx, info.hostId);
		m_lcLicense.SetItemText(idx, 1, info.macAddress);
		m_lcLicense.SetItemText(idx, 2, GetProcTypeString(info.eProcType));
	}

	m_lcLicense.SetRedraw(TRUE);

	m_btnOK.EnableWindow(TRUE);
}

BOOL CAuthCheckExDlg::GetLicenseInfo(LPCSTR lpszHostId, LPCSTR lpszIniPath, LICENSE_INFO& info)
{
	char buf[1024];

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString(lpszHostId, "mgrId", "", buf, 1023, lpszIniPath);
	info.mgrId = buf;

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString(lpszHostId, "siteId", "", buf, 1023, lpszIniPath);
	info.siteId = buf;

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString(lpszHostId, "hostId", "", buf, 1023, lpszIniPath);
	info.hostId = buf;

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString(lpszHostId, "hostName", "", buf, 1023, lpszIniPath);
	info.hostName = buf;

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString(lpszHostId, "ipAddress", "", buf, 1023, lpszIniPath);
	info.ipAddress = buf;

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString(lpszHostId, "macAddress", "", buf, 1023, lpszIniPath);
	info.macAddress = buf;

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString(lpszHostId, "edition", "", buf, 1023, lpszIniPath);
	info.edition = buf;

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString(lpszHostId, "authDate", "", buf, 1023, lpszIniPath);
	info.authDate = buf;

	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString(lpszHostId, "enterpriseKey", "", buf, 1023, lpszIniPath);
	info.enterpriseKey = buf;

	return TRUE;
}
