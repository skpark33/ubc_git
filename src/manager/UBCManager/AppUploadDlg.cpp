// AppUploadDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "AppUploadDlg.h"

#include "Enviroment.h"
#include "SHA1.h"

#include "../Dlls/UBCCopCommon/resource.h"
#include "common/libCommon/ubcMuxRegacy.h"

#include "Dbghelp.h"

#define		WM_COMPLETE_UPLOAD		(WM_USER + 1025)


// CAppUploadDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAppUploadDlg, CDialog)

CAppUploadDlg::CAppUploadDlg(DLGTYPE eDlgType, CWnd* pParent /*=NULL*/)
	: CDialog(CAppUploadDlg::IDD, pParent)
	, m_eDlgType ( eDlgType )
	, m_bLogin ( FALSE )
	, m_bUploading ( FALSE )
	, m_pThread ( NULL )
	, m_bUserAbort ( FALSE )
	, m_nOldVersion ( 0 )
	, m_bIsAlphaVersion ( false )
	, m_ulNewFileSize ( 0L )
{

}

CAppUploadDlg::~CAppUploadDlg()
{
}

void CAppUploadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_OLD_FILE_UPLOAD_TIME, m_editOldFileUploadTime);
	DDX_Control(pDX, IDC_EDIT_OLD_FILE_SIZE, m_editOldFileSize);
	DDX_Control(pDX, IDC_EDIT_NEW_FILE_PATH, m_editNewFilePath);
	DDX_Control(pDX, IDC_EDIT_NEW_FILE_SIZE, m_editNewFileSize);
	DDX_Control(pDX, IDC_BUTTON_NEW_FILE_SELECT, m_btnNewFileSelect);
	DDX_Control(pDX, IDC_BUTTON_UPLOAD, m_btnUpload);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_EDIT_OLD_VERSION, m_editOldVersion);
	DDX_Control(pDX, IDC_EDIT_NEW_VERSION, m_editNewVersion);
	DDX_Control(pDX, IDC_PROGRESS_CTRL, m_pc);
	DDX_Control(pDX, IDC_STATIC_PROGRESS, m_stcProgress);
	DDX_Control(pDX, IDC_CHECK_ALPHA_VERSION, m_chkAlphaVersion);
	DDX_Control(pDX, IDC_ZIP_FILE_NAME, m_stZipFileName);
	DDX_Control(pDX, IDC_EDIT_CID, m_editCID);
}


BEGIN_MESSAGE_MAP(CAppUploadDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_NEW_FILE_SELECT, &CAppUploadDlg::OnBnClickedButtonNewFileSelect)
	ON_BN_CLICKED(IDC_BUTTON_UPLOAD, &CAppUploadDlg::OnBnClickedButtonUpload)
	ON_MESSAGE(WM_COMPLETE_UPLOAD, OnCompleteUpload)
	ON_BN_CLICKED(IDC_BUTTON_APPLY_CID, &CAppUploadDlg::OnBnClickedButtonApplyCid)
END_MESSAGE_MAP()


// CAppUploadDlg 메시지 처리기입니다.

BOOL CAppUploadDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	return InitData(CEnviroment::GetObject()->m_strCustomer);
	//return TRUE;
}

BOOL CAppUploadDlg::InitData(CString customer)
{

	m_editOldVersion.SetWindowText("NONE");
	m_editOldFileUploadTime.SetWindowText("NONE");
	m_editOldFileSize.SetWindowText("NONE");
	m_btnUpload.SetWindowText(LoadStringById(IDS_APPUPLOAD_UPLOAD));
	m_btnUpload.EnableWindow(FALSE);

	m_pc.SetRange32(0, 100);

	if(customer.IsEmpty())
	{
		customer = "UNKNOWN";
	}
	m_editCID.SetWindowText(customer);
	h_customer = customer;
	l_customer= customer.MakeLower();

	switch( m_eDlgType )
	{
	case eFirmware:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_FIRMWARE));

		m_strVersionServerPath = "/web/UBC_DEFAULT/UBC_Player/KPOST_FIRMWARE/fwversion.txt";
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%sKPOST_FIRMWARE\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "fwversion.txt";

		m_strAppServerPath     = "/web/UBC_DEFAULT/UBC_Player/KPOST_FIRMWARE/ks560_kpost.zip";
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_FIRMWARE\\kpost_unknown.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		break;

	case eFirmware2:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_FIRMWARE));

		m_strVersionServerPath = "/web/UBC_DEFAULT/UBC_Player/KPOST_FIRMWARE2/fwversion.txt";
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%sKPOST_FIRMWARE2\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "fwversion.txt";

		m_strAppServerPath     = "/web/UBC_DEFAULT/UBC_Player/KPOST_FIRMWARE2/updateshell.sh";
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_FIRMWARE\\kpost_unknown.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);

		m_strZipLocalPath = "";
		m_strZipServerPath     = "/web/UBC_DEFAULT/UBC_Player/KPOST_FIRMWARE2/";
		break;


	default:

	case eUBCPlayer:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_UBC_PLAYER));

		m_strVersionServerPath.Format("/web/UBC_DEFAULT/UBC_Player/%s_PLAYER/apkversion.txt", h_customer);
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%s%s_PLAYER\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, h_customer);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "apkversion.txt";

		m_strAppServerPath.Format("/web/UBC_DEFAULT/UBC_Player/%s_PLAYER/%s_ubcplayer.apk", h_customer, l_customer);
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_PLAYER\\kpost_ubcplayer.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		break;

	case eUBCLauncher:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_FIRMWARE));

		m_strVersionServerPath.Format("/web/UBC_DEFAULT/UBC_Player/%s_LAUNCHER/apkversion.txt", h_customer);
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%s%s_LAUNCHER\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, h_customer);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "apkversion.txt";

		m_strAppServerPath.Format("/web/UBC_DEFAULT/UBC_Player/%s_LAUNCHER/%s_ubclauncher.apk", h_customer, l_customer);
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_PLAYER\\kpost_ubcplayer.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		break;
	
	case eKPOSTPlayer:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_UBC_PLAYER));

		m_strVersionServerPath="/web/UBC_DEFAULT/UBC_Player/KPOST_PLAYER/apkversion.txt";
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%sKPOST_PLAYER\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "apkversion.txt";

		m_strAppServerPath.Format("/web/UBC_DEFAULT/UBC_Player/KPOST_PLAYER/kpost_ubcplayer.apk");
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_PLAYER\\kpost_ubcplayer.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		break;
	
	case eKPOSTPlayer2:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_UBC_PLAYER));

		m_strVersionServerPath = "/web/UBC_DEFAULT/UBC_Player/KPOST_PLAYER2/apkversion.txt";
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%sKPOST_PLAYER2\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "apkversion.txt";

		m_strAppServerPath     = "/web/UBC_DEFAULT/UBC_Player/KPOST_PLAYER2/kpost_ubcplayer.apk";
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_PLAYER\\kpost_ubcplayer.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		break;

	case eVaccineApp:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_VACCINE));

		m_strVersionServerPath = "/web/UBC_DEFAULT/UBC_Player/KPOST_VACCINE/apkversion.txt";
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%sKPOST_VACCINE\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "apkversion.txt";

		m_strAppServerPath     = "/web/UBC_DEFAULT/UBC_Player/KPOST_VACCINE/kpost_alyack.apk";
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_VACCINE\\kpost_alyack.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		break;

	case eVaccineDB:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_VACCINE_DB));

		m_strVersionServerPath = "/web/UBC_DEFAULT/UBC_Player/KPOST_VACCINE_DB/apkversion.txt";
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%sKPOST_VACCINE_DB\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "apkversion.txt";

		m_strAppServerPath     = "/web/UBC_DEFAULT/UBC_Player/KPOST_VACCINE_DB/data.zip";
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_VACCINE\\kpost_alyack.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		break;

	case eManagerUpdate:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_MANAGE_UPDATE));
		m_chkAlphaVersion.ShowWindow(SW_SHOW);

		m_strVersionServerPath = "/web/UBC_DEFAULT/UBC_Manager/UPDATE/apkversion.txt";
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%sUBC_Manager_UPDATE\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "apkversion.txt";

		m_strAppServerPath     = "/web/UBC_DEFAULT/UBC_Manager/UPDATE/SQISoft.tar.gz";
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_VACCINE\\kpost_alyack.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		break;

	case eServerUpdate:
		SetWindowText(LoadStringById(IDS_APPUPLOAD_TITLE_SERVER_UPDATE));
		m_chkAlphaVersion.ShowWindow(SW_SHOW);

		m_strVersionServerPath = "/web/UBC_DEFAULT/UBC_Server/UPDATE/apkversion.txt";
		m_strVersionServerOldPath = m_strVersionServerPath + ".bak";
		m_strVersionServerTmpPath = m_strVersionServerPath + ".tmp";

		m_strVersionLocalPath.Format("%s\\%sUBC_Server_UPDATE\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		::MakeSureDirectoryPathExists(m_strVersionLocalPath);
		m_strVersionLocalPath += "apkversion.txt";

		m_strAppServerPath     = "/web/UBC_DEFAULT/UBC_Server/UPDATE/ubc.tar.gz";
		m_strAppServerOldPath = m_strAppServerPath + ".bak";
		m_strAppServerTmpPath = m_strAppServerPath + ".tmp";

		m_strAppLocalPath = "";//.Format("%s\\%sKPOST_VACCINE\\kpost_alyack.apk", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH);
		break;
	}

	CWaitMessageBox wait(LoadStringById(IDS_FTPMULTISITE_MSG013));

	//
	ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(GetEnvPtr()->m_szSite);
	if(!aData){
		UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_UPLOAD_AUTH, MB_ICONSTOP);
		return FALSE;
	}

	//
	m_objFileSvcClient.SetHttpServerInfo(aData->getFTPAddress(), aData->ftpPort);
	m_bLogin = m_objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str());

	if( !m_bLogin )
	{
		//TraceLog(("IP:%s / PORT:%d / ID:%s /PW:%s",stPackageInfo.ip.c_str(),stPackageInfo.port,stPackageInfo.userId.c_str(),stPackageInfo.passwd.c_str()));
		UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_UPLOAD_AUTH, MB_ICONSTOP);
		return FALSE;
	}

	if( !::DeleteFile(m_strVersionLocalPath) )
	{
		if( ::GetLastError() != ERROR_FILE_NOT_FOUND )
		{
			UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_DELETE_OLD_VER_FILE, MB_ICONSTOP);
			return FALSE;
		}
	}

	BOOL exist_info = FALSE;
	BOOL result = TRUE;
	try {
		CFileServiceWrap::SFileInfo info;
		if( m_objFileSvcClient.FileInfo(m_strVersionServerPath, info) )
		{
			// exist server-path
			exist_info = TRUE;
			result = m_objFileSvcClient.GetFile(m_strVersionServerPath, m_strVersionLocalPath, 0, NULL);
		}
	}catch (CString& e) {
		result = FALSE;
	}

	if( !result )
	{
		//TraceLog(("PutFile (%s) failed",sRemoteFileKey.c_str()));
		UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_GET_OLD_VER_FILE, MB_ICONSTOP);
		return FALSE;
	}

	// .version 파일분석 & EDIT업데이트
	if( exist_info )
		GetDataFromVersionFile(m_strVersionLocalPath);

	m_strNewVersion.Format("%03d", m_nOldVersion+1); // 1234
	m_strNewVersion.Insert(m_strNewVersion.GetLength()-1, "."); // 123.4
	m_strNewVersion.Insert(m_strNewVersion.GetLength()-3, "."); // 12.3.4
	m_editNewVersion.SetWindowText(m_strNewVersion);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAppUploadDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();
}

void CAppUploadDlg::OnCancel()
{
	if( m_bUploading )
	{
		OnBnClickedButtonUpload();
		return;
	}

	if( m_bLogin )
	{
		m_objFileSvcClient.Logout();
	}

	CDialog::OnCancel();
}

void CAppUploadDlg::OnBnClickedButtonNewFileSelect()
{
	static const char* _filters[] = {
		"Firmware File (*.zip)|*.zip|All File (*.*)|*.*||",
		"UBC Player File (*.apk)|*.apk|All File (*.*)|*.*||",
		"Vaccine App File (*.apk)|*.apk|All File (*.*)|*.*||",
		"Vaccine DB File (*.zip)|*.zip|All File (*.*)|*.*||",
		"Manager Update File (*.gz)|*.gz|All File (*.*)|*.*||",
		"Server Update File (*.gz)|*.gz|All File (*.*)|*.*||",
		"Firmware File (*.sh)|*.sh|All File (*.*)|*.*||",
		"UBC Player File (*.apk)|*.apk|All File (*.*)|*.*||",
		"UBC Player File (*.apk)|*.apk|All File (*.*)|*.*||",
		"UBC Launcher File (*.apk)|*.apk|All File (*.*)|*.*||",
	};

	static const char* _ext[] = {
		"zip",
		"apk",
		"apk",
		"zip",
		"gz",
		"gz",
		"sh",
		"apk",
		"apk",
		"apk",
	};

	char* sz_filter = NULL;
	char* sz_ext = NULL;

	switch( m_eDlgType )
	{
	case eFirmware:
	case eFirmware2:
	case eUBCPlayer:
	case eUBCLauncher:
	case eKPOSTPlayer:
	case eKPOSTPlayer2:
	case eVaccineApp:
	case eVaccineDB:
	case eManagerUpdate:
	case eServerUpdate:
		sz_filter = (char*)_filters[m_eDlgType];
		sz_ext = (char*)_ext[m_eDlgType];
		break;
	}

	m_strZipLocalPath="";

	CFileDialog dlg(TRUE, sz_ext, "", OFN_FILEMUSTEXIST, sz_filter, this);
	if(dlg.DoModal() == IDOK)
	{
		m_strAppLocalPath = dlg.GetPathName();
		m_editNewFilePath.SetWindowText(m_strAppLocalPath);

		CFileStatus fs;
		if( !CFile::GetStatus(m_strAppLocalPath, fs) )
		{
			m_editNewFileSize.SetWindowText("ERROR");
		}
		else
		{
			m_ulNewFileSize = fs.m_size;
			m_editNewFileSize.SetWindowText( ::ToMoneyTypeString(m_ulNewFileSize) );
		
			if(m_eDlgType == eFirmware2)
			{
				m_strZipLocalPath = m_strAppLocalPath;
				if(!GetZipFileNameFromShell(m_strAppLocalPath)) 
				{
					m_strZipLocalPath = "";
				}
			}

			m_btnUpload.EnableWindow(TRUE);
		}
	}
}

void CAppUploadDlg::OnBnClickedButtonUpload()
{
	if( m_bUploading )
	{
		m_btnUpload.SetWindowText(LoadStringById(IDS_APPUPLOAD_CANCELING));
		m_btnUpload.EnableWindow(FALSE);

		m_bUserAbort = TRUE;

		// uploading ==> cancel
		m_objFileSvcClient.SetUserCancel();
	}
	else
	{
		m_editNewVersion.GetWindowText(m_strNewVersion);
		m_strNewVersion.Trim(" \t");
		if( m_strNewVersion.GetLength() == 0 )
		{
			UbcMessageBox(IDS_APPUPLOAD_INPUT_NEW_VERSION, MB_ICONWARNING);
			return;
		}

		m_bIsAlphaVersion = (m_chkAlphaVersion.GetCheck() == BST_CHECKED);
		if( m_bIsAlphaVersion )
		{
			m_strAppServerPath.Replace("SQISoft.tar.gz", "SQISoft_pre.tar.gz");
			m_strAppServerOldPath.Replace("SQISoft.tar.gz", "SQISoft_pre.tar.gz");
			m_strAppServerTmpPath.Replace("SQISoft.tar.gz", "SQISoft_pre.tar.gz");

			m_strAppServerPath.Replace("ubc.tar.gz", "ubc_pre.tar.gz");
			m_strAppServerOldPath.Replace("ubc.tar.gz", "ubc_pre.tar.gz");
			m_strAppServerTmpPath.Replace("ubc.tar.gz", "ubc_pre.tar.gz");
		}

		m_bUploading = TRUE;
		m_bUserAbort = FALSE;

		// disable button
		m_editNewVersion.EnableWindow(FALSE);
		m_chkAlphaVersion.EnableWindow(FALSE);
		m_btnNewFileSelect.EnableWindow(FALSE);
		m_btnCancel.EnableWindow(FALSE);

		// change button text
		m_btnUpload.SetWindowText(LoadStringById(IDS_APPUPLOAD_CANCEL));

		// show progress ctrl
		m_pc.ShowWindow(SW_SHOW);
		m_stcProgress.ShowWindow(SW_SHOW);
		m_stcProgress.SetWindowText("0 %");
		m_pc.SetPos(0);

		// run thread
		m_pThread = ::AfxBeginThread(UploadThreadProc, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = TRUE;
		m_pThread->ResumeThread();
	}
}

void CAppUploadDlg::ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath)
{
	TraceLog(("ProgressEvent::OnBytesReceived start"));

	//FS_START = 0,
	//FS_DOING,
	//FS_STOP,
	//FS_COMPLETE,
	//FS_ERROR

	if(nFlag == CFileServiceWrap::FS_DOING && m_ulNewFileSize > 0)
	{
		int progress = (uSendBytes * 100) / m_ulNewFileSize;

		m_pc.SetPos(progress);

		CString str_progress;
		str_progress.Format("%d %%", progress);

		m_stcProgress.SetWindowText(str_progress);

		m_editNewFileSize.SetWindowText( ::ToMoneyTypeString(m_ulNewFileSize) );
	}

	TraceLog(("ProgressEvent::OnBytesReceived end"));
}

LRESULT CAppUploadDlg::OnCompleteUpload(WPARAM wParam, LPARAM lParam)
{
	m_bUploading = FALSE;
	m_bUserAbort = FALSE;

	// enable button
	m_editNewVersion.EnableWindow(TRUE);
	m_chkAlphaVersion.EnableWindow(TRUE);
	m_btnNewFileSelect.EnableWindow(TRUE);
	m_btnCancel.EnableWindow(TRUE);
	m_btnUpload.EnableWindow(TRUE);

	// change button text
	m_btnUpload.SetWindowText(LoadStringById(IDS_APPUPLOAD_UPLOAD));

	// hide progesss ctrl
	m_pc.ShowWindow(SW_HIDE);
	m_stcProgress.ShowWindow(SW_HIDE);
	m_stcProgress.SetWindowText("0 %");

	if( wParam )
	{
		// success
		GetDataFromVersionFile(m_strVersionLocalPath);

		m_strNewVersion.Format("%03d", m_nOldVersion+1); // 1234
		m_strNewVersion.Insert(m_strNewVersion.GetLength()-1, "."); // 123.4
		m_strNewVersion.Insert(m_strNewVersion.GetLength()-3, "."); // 12.3.4
		m_editNewVersion.SetWindowText(m_strNewVersion);

		//
		m_editNewFilePath.SetWindowText("");
		m_editNewFileSize.SetWindowText("");

		m_btnUpload.EnableWindow(FALSE);

		UbcMessageBox(IDS_APPUPLOAD_SUCCESS_TO_UPLOAD, MB_ICONINFORMATION);
	}
	else
	{
		// fail or error
		// ==> do nothing
	}

	return 0;
}

UINT CAppUploadDlg::UploadThreadProc(LPVOID param)
{
	::CoInitializeEx(NULL, COINIT_MULTITHREADED);

	TraceLog(("CAppUploadDlg::UploadThreadProc"));

	CAppUploadDlg* pDlg = (CAppUploadDlg*)param;
	if(pDlg)
	{
		HWND parent_wnd = pDlg->GetSafeHwnd();

		BOOL ret = pDlg->UploadFile();

		::PostMessage(parent_wnd, WM_COMPLETE_UPLOAD, ret, NULL);
	}

	::CoUninitialize();

	return 0;
}

BOOL CAppUploadDlg::UploadFile()
{
	TraceLog(("CAppUploadDlg::UploadFile"));

	BOOL result = TRUE;
	try {
		// .version 파일 구성
		CFileStatus fs;
		if( !CFile::GetStatus(m_strAppLocalPath, fs) )
		{
			UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_FILE_OPEN, MB_ICONSTOP);
			return FALSE;
		}

		// sha1
		CSHA1 sha1;
		sha1.HashFile(m_strAppLocalPath);
		sha1.Final();

		std::string str_sha1;
		sha1.ReportHashStl(str_sha1);
		CString str_sha1_nospace = str_sha1.c_str();
		str_sha1_nospace.Replace(" ", "");

		BOOL b_zipFileCase = FALSE;

		CString player_file = l_customer;
		player_file += "_ubcplayer.apk";
		//
		CString launcher_file = l_customer;
		launcher_file += "_ubclauncher.apk";

		CString str_buf;
		switch( m_eDlgType )
		{
		case eFirmware:
			str_buf.Format("fw_name=%s\nfw_version=%s\nfw_checksum=%s\nfw_filesize=%I64u\nfw_uploadtime=%s"
					, "ks560_kpost.zip"
					, m_strNewVersion//str_version
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			break;
		case eUBCPlayer:
			str_buf.Format("apk_name=%s\napk_version=%s\napk_checksum=%s\napk_filesize=%I64u\napk_uploadtime=%s"
					, player_file
					, m_strNewVersion
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			break;
		case eUBCLauncher:
			str_buf.Format("apk_name=%s\napk_version=%s\napk_checksum=%s\napk_filesize=%I64u\napk_uploadtime=%s"
					, launcher_file
					, m_strNewVersion
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			break;
		case eKPOSTPlayer:
			str_buf.Format("apk_name=%s\napk_version=%s\napk_checksum=%s\napk_filesize=%I64u\napk_uploadtime=%s"
					, "kpost_ubcplayer.apk"
					, m_strNewVersion
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			break;
		case eFirmware2:
			str_buf.Format("fw_name=%s\nfw_version=%s\nfw_checksum=%s\nfw_filesize=%I64u\nfw_uploadtime=%s"
					, "updateshell.sh"
					, m_strNewVersion//str_version
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			if(!this->m_strZipLocalPath.IsEmpty() )
			{
				b_zipFileCase = TRUE;
			}
			break;
		case eKPOSTPlayer2:
			str_buf.Format("apk_name=%s\napk_version=%s\napk_checksum=%s\napk_filesize=%I64u\napk_uploadtime=%s"
					, "kpost_ubcplayer.apk"
					, m_strNewVersion
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			break;
		case eVaccineApp:
			str_buf.Format("apk_name=%s\napk_version=%s\napk_checksum=%s\napk_filesize=%I64u\napk_uploadtime=%s"
					, "kpost_alyack.apk"
					, m_strNewVersion
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			break;
		case eVaccineDB:
			str_buf.Format("apk_name=%s\napk_version=%s\napk_checksum=%s\napk_filesize=%I64u\napk_uploadtime=%s"
					, "data.zip"
					, m_strNewVersion
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			break;
		case eManagerUpdate:
			str_buf.Format("apk_name=%s\napk_version=%s\napk_checksum=%s\napk_filesize=%I64u\napk_uploadtime=%s"
					, ( m_bIsAlphaVersion ? "SQISoft_pre.tar.gz" : "SQISoft.tar.gz" )
					, m_strNewVersion
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			break;
		case eServerUpdate:
			str_buf.Format("apk_name=%s\napk_version=%s\napk_checksum=%s\napk_filesize=%I64u\napk_uploadtime=%s"
					, ( m_bIsAlphaVersion ? "ubc_pre.tar.gz" : "ubc.tar.gz" )
					, m_strNewVersion
					, str_sha1_nospace
					, fs.m_size
					, CTime::GetCurrentTime().Format("%Y.%m%d.%H%M%S") );
			m_ulNewFileSize = fs.m_size;
			break;
		}

		//
		CFile file;
		if( !file.Open(m_strVersionLocalPath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
		{
			UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_CREATE_NEW_VER_FILE, MB_ICONSTOP);
			return FALSE;
		}
		TRY
		{
			file.Write((LPVOID)(LPCSTR)str_buf, str_buf.GetLength());
			file.Close();
		}
		CATCH(CFileException, ex)
		{
			UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_WRITE_NEW_VER_FILE, MB_ICONSTOP);
			return FALSE;
		}
		END_CATCH

		// delete old
		CFileServiceWrap::SFileInfo info;
		BOOL exist_version_old = m_objFileSvcClient.FileInfo(m_strVersionServerOldPath, info);
		BOOL exist_app_old     = m_objFileSvcClient.FileInfo(m_strAppServerOldPath, info);
		BOOL exist_version_org = m_objFileSvcClient.FileInfo(m_strVersionServerPath, info);
		BOOL exist_app_org     = m_objFileSvcClient.FileInfo(m_strAppServerPath, info);

		BOOL exist_zip_old     = m_objFileSvcClient.FileInfo(m_strZipServerOldPath, info);
		BOOL exist_zip_org = m_objFileSvcClient.FileInfo(m_strZipServerPath, info);

		if( exist_version_old )
		{
			result = m_objFileSvcClient.DeleteFile(m_strVersionServerOldPath);
			if( !result )
			{
				UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_DELETE_BAK_VER_FILE, MB_ICONSTOP);
				return FALSE;
			}
		}
		if( exist_app_old )
		{
			result = m_objFileSvcClient.DeleteFile(m_strAppServerOldPath);
			if( !result )
			{
				UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_DELETE_BAK_FILE, MB_ICONSTOP);
				return FALSE;
			}
		}
		if( b_zipFileCase && exist_zip_old )
		{
			result = m_objFileSvcClient.DeleteFile(m_strZipServerOldPath);
			if( !result )
			{
				UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_DELETE_BAK_FILE, MB_ICONSTOP);
				return FALSE;
			}
		}

		// upload to tmp
		result = m_objFileSvcClient.PutFile(m_strVersionLocalPath, m_strVersionServerTmpPath, 0, NULL);
		if( !m_bUserAbort && !result )
		{
			UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_UPLOAD_VER_FILE, MB_ICONSTOP);
			return FALSE;
		}
		result = m_objFileSvcClient.PutFile(m_strAppLocalPath, m_strAppServerTmpPath, 0, (IProgressHandler*)this);
		if( !m_bUserAbort && !result )
		{
			UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_UPLOAD_FILE, MB_ICONSTOP);
			return FALSE;
		}
		if(b_zipFileCase )
		{
			result = m_objFileSvcClient.PutFile(m_strZipLocalPath, m_strZipServerTmpPath, 0, (IProgressHandler*)this);
			if( !m_bUserAbort && !result )
			{
				UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_UPLOAD_FILE, MB_ICONSTOP);
				return FALSE;
			}
		}

		if( m_bUserAbort ) return FALSE;

		// replace original to old
		if( exist_version_org )
		{
			result = m_objFileSvcClient.MoveFile(m_strVersionServerPath, m_strVersionServerOldPath);
			if( !result )
			{
				UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_REPLACE_VER_FILE, MB_ICONSTOP);
				return FALSE;
			}
		}
		if( exist_app_org )
		{
			result = m_objFileSvcClient.MoveFile(m_strAppServerPath, m_strAppServerOldPath);
			if( !result )
			{
				UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_REPLACE_FILE, MB_ICONSTOP);

				if( exist_version_org )
				{
					result = m_objFileSvcClient.MoveFile(m_strVersionServerOldPath, m_strVersionServerPath);
					if( !result )
					{
						UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_BAK_VER_FILE, MB_ICONSTOP);
					}
				}

				return FALSE;
			}
		}
		if( exist_zip_org && b_zipFileCase)
		{
			result = m_objFileSvcClient.MoveFile(m_strZipServerPath, m_strZipServerOldPath);
			if( !result )
			{
				UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_REPLACE_FILE, MB_ICONSTOP);

				if( exist_version_org )
				{
					result = m_objFileSvcClient.MoveFile(m_strVersionServerOldPath, m_strVersionServerPath);
					if( !result )
					{
						UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_BAK_VER_FILE, MB_ICONSTOP);
					}
				}

				if( exist_app_org )
				{
					result = m_objFileSvcClient.MoveFile(m_strAppServerOldPath, m_strAppServerPath);
					if( !result )
					{
						UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_BAK_VER_FILE, MB_ICONSTOP);
					}
				}

				return FALSE;
			}
		}

		// replace tmp to original
		result = m_objFileSvcClient.MoveFile(m_strVersionServerTmpPath, m_strVersionServerPath);
		if( !result )
		{
			UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_REPLACE_NEW_VER_FILE, MB_ICONSTOP);
			return FALSE;
		}
		result = m_objFileSvcClient.MoveFile(m_strAppServerTmpPath, m_strAppServerPath);
		if( !result )
		{
			UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_REPLACE_NEW_FILE, MB_ICONSTOP);

			result = m_objFileSvcClient.MoveFile(m_strVersionServerPath, m_strVersionServerTmpPath);
			if( !result )
			{
				UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_NEW_VER_FILE, MB_ICONSTOP);
			}

			if( exist_version_org )
			{
				result = m_objFileSvcClient.MoveFile(m_strVersionServerOldPath, m_strVersionServerPath);
				if( !result )
				{
					UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_BAK_VER_FILE, MB_ICONSTOP);
				}
			}

			if( exist_app_org )
			{
				result = m_objFileSvcClient.MoveFile(m_strAppServerOldPath, m_strAppServerPath);
				if( !result )
				{
					UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_BAK_FILE, MB_ICONSTOP);
				}
			}

			return FALSE;
		}

		if(b_zipFileCase)
		{
			result = m_objFileSvcClient.MoveFile(m_strZipServerTmpPath, m_strZipServerPath);
			if( !result )
			{
				UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_REPLACE_NEW_FILE, MB_ICONSTOP);

				result = m_objFileSvcClient.MoveFile(m_strAppServerPath, m_strAppServerTmpPath);
				if( !result )
				{
					UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_NEW_VER_FILE, MB_ICONSTOP);
				}

				result = m_objFileSvcClient.MoveFile(m_strVersionServerPath, m_strVersionServerTmpPath);
				if( !result )
				{
					UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_NEW_VER_FILE, MB_ICONSTOP);
				}

				if( exist_version_org )
				{
					result = m_objFileSvcClient.MoveFile(m_strVersionServerOldPath, m_strVersionServerPath);
					if( !result )
					{
						UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_BAK_VER_FILE, MB_ICONSTOP);
					}
				}

				if( exist_app_org )
				{
					result = m_objFileSvcClient.MoveFile(m_strAppServerOldPath, m_strAppServerPath);
					if( !result )
					{
						UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_BAK_FILE, MB_ICONSTOP);
					}
				}

				if( exist_zip_org )
				{
					result = m_objFileSvcClient.MoveFile(m_strZipServerOldPath, m_strZipServerPath);
					if( !result )
					{
						UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_RESTORE_BAK_FILE, MB_ICONSTOP);
					}
				}

				return FALSE;
			}
	
		}
		//// delete old  
		////	==>  remain old (bacause of roll-back)
		//if( exist_version_org )
		//{
		//	result = m_objFileSvcClient.DeleteFile(m_strVersionServerOldPath);
		//	if( !result )
		//	{
		//		//UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_DELETE_BAK_VER_FILE, MB_ICONSTOP);
		//		//return FALSE;
		//	}
		//}
		//if( exist_app_org )
		//{
		//	result = m_objFileSvcClient.DeleteFile(m_strAppServerOldPath);
		//	if( !result )
		//	{
		//		//UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_DELETE_BAK_FILE, MB_ICONSTOP);
		//		//return FALSE;
		//	}
		//}

	}catch (CString& e) {
		UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_UPLOAD, MB_ICONSTOP);
		return FALSE;
	}

	return TRUE;
}

BOOL CAppUploadDlg::GetDataFromVersionFile(LPCSTR lpszPath)
{
	//
	CFile file;
	if( !file.Open(lpszPath, CFile::modeRead | CFile::typeBinary) )
	{
		UbcMessageBox(IDS_APPUPLOAD_FAIL_TO_OPEN_OLD_VER_FILE, MB_ICONSTOP);
		return FALSE;
	}

	//
	CString str_buf;
	ULONGLONG size = file.GetLength();
	char* buf = new char[size+1];
	::ZeroMemory(buf, size+1);

	//
	TRY
	{
		file.Read(buf, size);
		str_buf = buf;
	}
	CATCH(CFileException, ex)
	{
		delete[]buf;

		DWORD err_code = ::GetLastError();
		LPVOID lpMsgBuf = NULL;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
					| FORMAT_MESSAGE_IGNORE_INSERTS
					| FORMAT_MESSAGE_FROM_SYSTEM
					, NULL
					, err_code
					, 0
					, (LPTSTR)&lpMsgBuf
					, 0
					, NULL );

		CString str_err_msg;
		str_err_msg.Format("%s (%u)", (LPCSTR)lpMsgBuf, err_code);

		LocalFree( lpMsgBuf );

		UbcMessageBox(str_err_msg, MB_ICONSTOP);

		return FALSE;
	}
	END_CATCH
	delete[]buf;

	//
	int pos = 0;
	CString str_line;
	while( (str_line = str_buf.Tokenize("\r\n", pos)) != "" )
	{
		str_line.Trim(" \t");
		if( str_line.Find('=') < 0 ) continue;

		int pos2 = 0;
		CString str_key = str_line.Tokenize("=", pos2);
		str_key.Trim(" \t");
		CString str_value = str_line.Tokenize("=", pos2);
		str_value.Trim(" \t");

		if( stricmp(str_key, "fw_version") == 0 || stricmp(str_key, "apk_version") == 0 )
		{
			m_editOldVersion.SetWindowText(str_value);
			str_value.Replace(".", ""); 
			m_nOldVersion = atoi(str_value);
		}
		else if( stricmp(str_key, "fw_filesize") == 0 || stricmp(str_key, "apk_filesize") == 0 )
		{
			m_editOldFileSize.SetWindowText( ::ToMoneyTypeString(str_value) );
		}
		else if( stricmp(str_key, "fw_uploadtime") == 0 || stricmp(str_key, "apk_uploadtime") == 0 )
		{
			str_value.Replace(".", ""); 
			// ex : "20141127010234"
			if( str_value.GetLength() == 14 )
			{
				str_value.Insert(12, ":"); // 201411270102:34
				str_value.Insert(10, ":"); // 2014112701:02:34
				str_value.Insert(8, " ");  // 20141127 01:02:34
				str_value.Insert(6, "-");  // 201411-27 01:02:34
				str_value.Insert(4, "-");  // 2014-11-27 01:02:34

				m_editOldFileUploadTime.SetWindowText(str_value);
			}
		}
	}

	return TRUE;
}


BOOL CAppUploadDlg::GetZipFileNameFromShell(LPCSTR lpszPath)
{
	//
	CFile file;
	if( !file.Open(lpszPath, CFile::modeRead | CFile::typeBinary) )
	{
		CString msg;
		msg.Format("%s file open failed", lpszPath);
		UbcMessageBox(msg, MB_ICONSTOP);
		return FALSE;
	}

	//
	CString str_buf;
	ULONGLONG size = file.GetLength();
	char* buf = new char[size+1];
	::ZeroMemory(buf, size+1);

	//
	TRY
	{
		file.Read(buf, size);
		str_buf = buf;
	}
	CATCH(CFileException, ex)
	{
		delete[]buf;

		DWORD err_code = ::GetLastError();
		LPVOID lpMsgBuf = NULL;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
					| FORMAT_MESSAGE_IGNORE_INSERTS
					| FORMAT_MESSAGE_FROM_SYSTEM
					, NULL
					, err_code
					, 0
					, (LPTSTR)&lpMsgBuf
					, 0
					, NULL );

		CString str_err_msg;
		str_err_msg.Format("%s (%u)", (LPCSTR)lpMsgBuf, err_code);

		LocalFree( lpMsgBuf );

		UbcMessageBox(str_err_msg, MB_ICONSTOP);

		return FALSE;
	}
	END_CATCH
	delete[]buf;

	//
	int pos = 0;
	CString str_line;
	while( (str_line = str_buf.Tokenize("\r\n", pos)) != "" )
	{
		str_line.Trim(" \t");
		if( str_line.Find('=') < 0 ) continue;

		int pos2 = 0;
		CString str_key = str_line.Tokenize("=", pos2);
		str_key.Trim(" \t");
		CString str_value = str_line.Tokenize("=", pos2);
		str_value.Trim(" \t\"");

		if( stricmp(str_key, "post_fw") == 0)
		{
			m_stZipFileName.SetWindowText(str_value);
			m_strZipLocalPath.Replace("updateshell.sh",str_value);
			m_strZipServerPath.Append(str_value);

			m_strZipServerOldPath = m_strZipServerPath + ".bak";
			m_strZipServerTmpPath = m_strZipServerPath + ".tmp";

			return TRUE;
		}
	}
	return FALSE;
}

void CAppUploadDlg::OnBnClickedButtonApplyCid()
{
	CString customer;
	this->m_editCID.GetWindowText(customer);
	InitData(customer);
}
