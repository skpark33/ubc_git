#pragma once

#include "common\HoverButton.h"
#include "common\reposcontrol.h"
#include "common\utblistctrlex.h"
#include "afxdtctl.h"
#include "afxwin.h"

using namespace std;

// CPackageView form view
class CPackageView : public CFormView
{
	DECLARE_DYNCREATE(CPackageView)
public:
	enum { eCheck // 2010.02.17 by gwangsoo
		 , eSite, ePackage, eUser, eTime, eDesc, eVolume, ePublic
		 , eCategory, ePurpose, eHostType, eVertical, eResolution, eVerify, eValidateDate
		 , eEnd };

	void InitPackageList();
	void SetPackageData();
	void Remove(int nRow);

	void InitFilterListCtrl();
	BOOL			GetBroadcastPlan(CString strPackage, CString& strBpList);

private:
	CString			m_szColName[eEnd];
	CUTBListCtrlEx	m_lscPackage;

	enum { eFilterCategory  	// 종류 (모닝,..)
		 , eFilterPurpose   	// 용도별 (교육용,..)
		 , eFilterHostType  	// 단말타입 (키오스크..)
		 , eFilterVertical  	// 가로/세로 방향 (가로,..)
		 , eFilterResolution	// 해상도 (1920x1080,..)
		 , eFilterPublic        // 공개여부
		 , eFilterVerify        // 승인여부
		 , eFilterMaxCnt    };
	CUTBListCtrlEx	m_lcFilter[eFilterMaxCnt];
	LONG			m_nFilter[eFilterMaxCnt];
	CString			m_strFilterContentsId;

	CComboBox		m_cbxIncludedContents;
	CDateTimeCtrl	m_dtStartDate;
	CDateTimeCtrl	m_dtEndDate;

	CStatic			m_Filter;
	CPoint			m_ptSelList;
	CRect			m_rcClient;
	CHoverButton	m_btnDel;
	CHoverButton	m_btnAdd;
	CHoverButton	m_btnRef;
	CHoverButton	m_btnUnused;
	CReposControl	m_Reposition;
	CHoverButton m_btnExcelSave;

	void			LoadFilterData();
	void			SaveFilterData();
	int				GetSelectItemsList(CUTBListCtrlEx& lc, CStringArray& ar);
	CString			GetSelectItemsStringFromList(CUTBListCtrlEx& lc, CString strField);
	CString			GetSelectItemsStringFromList(CStringArray& ar, CString strField);


protected:
	CPackageView();           // protected constructor used by dynamic creation
	virtual ~CPackageView();

public:
	enum { IDD = IDD_PACKAGEVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnPackageDelete();
	afx_msg void OnPackageEdit();
	afx_msg void OnPackagePlay();

	afx_msg void OnNMRclickList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
	afx_msg void OnBnClickedButtonRef();
	afx_msg void OnBnClickedBnRegister();
	afx_msg void OnCbnSelchangeCbIncludedContents();
	afx_msg void OnBnClickedButtonUnusedDel();
	afx_msg void OnNMDblclkListPackage(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBnToExcel4();
};
