// LogMainDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LogMainDlg.h"
#include "common\ubcdefine.h"
#include "UBCManager.h"
#include "Enviroment.h"
//#include "PackageChngLogDlg.h"
#include "ubccopcommon\SelectHostDlg.h"
#include "HostExcelSave.h"


// CLogMainDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLogMainDlg, CDialog)

CLogMainDlg::CLogMainDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLogMainDlg::IDD, pParent), m_defaultTab(0)
{

}

CLogMainDlg::~CLogMainDlg()
{
	for(int i=0;i<eMaxTab;i++){
		CSubLog* subLog = m_subLog[i];
		if(subLog) {
			delete subLog;
			m_subLog[i] = 0;
		}
	}
}

void CLogMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_HOSTLOG_TAB, m_hostLogTabCtrl);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_bnClose);
	DDX_Control(pDX, IDC_BN_TO_EXCEL, m_btnExcelSave);
	DDX_Control(pDX, IDC_DATE_FILTER_START, m_dtFilterStart);
	DDX_Control(pDX, IDC_DATE_FILTER_END, m_dtFilterEnd);
	DDX_Control(pDX, IDC_LIST_APPLYLOG, m_ApplyLogListCtrl);
	DDX_Control(pDX, IDC_LIST_SCHEDULESTATELOG, m_ScheduleStateLogListCtrl);
	DDX_Control(pDX, IDC_LIST_POWERLOG, m_PowerStateLogListCtrl);
	DDX_Control(pDX, IDC_LIST_CONNECTIONLOG, m_ConnectionStateLogListCtrl);
	DDX_Control(pDX, IDC_LIST_DOWNLOADSUMMARYLOG, m_ConnectionStateLogListCtrl);
	DDX_Control(pDX, IDC_STATIC_PACKAGE_LABEL, m_packageLabel);
	DDX_Control(pDX, IDC_EDIT_FILTER_PACKAGE, m_filterPackage);
	DDX_Control(pDX, IDC_BUTTON_FILTER_PACKAGE, m_btFilterPackage);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOST, m_btFilterHost);
	DDX_Control(pDX, IDC_STATIC_HOSTLABEL, m_labelFilterHost);
}


BEGIN_MESSAGE_MAP(CLogMainDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CLogMainDlg::OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_HOST, &CLogMainDlg::OnBnClickedButtonFilterHost)
	ON_BN_CLICKED(IDC_BN_TO_EXCEL, &CLogMainDlg::OnBnClickedBnToExcel)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CLogMainDlg::OnBnClickedButtonClose)
	ON_NOTIFY(TCN_SELCHANGE, IDC_HOSTLOG_TAB, &CLogMainDlg::OnTcnSelchangeHostlogTab)
	ON_NOTIFY(TCN_SELCHANGING, IDC_HOSTLOG_TAB, &CLogMainDlg::OnTcnSelchangingHostlogTab)
END_MESSAGE_MAP()


// CLogMainDlg 메시지 처리기입니다.

BOOL CLogMainDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_bnClose.LoadBitmap(IDB_BUTTON_CLOSE, RGB(255,255,255));
	m_btnExcelSave.LoadBitmap(IDB_BUTTON_EXCEL, RGB(255,255,255));

	m_bnRefresh.SetToolTipText(LoadStringById(IDS_PACKAGE_CHNG_LOG_BTN001));
	m_btnExcelSave.SetToolTipText(LoadStringById(IDS_PACKAGE_CHNG_LOG_BTN002));

	//m_Reposition.AddControl(&m_ApplyLogListCtrl, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	CTime tmCur = CTime::GetCurrentTime();
	m_dtFilterEnd.SetTime(&tmCur);
	InitDateRange(m_dtFilterEnd);

	CTime tmStartDate = tmCur - CTimeSpan(7, 0, 0, 0);
	m_dtFilterStart.SetTime(&tmStartDate);
	InitDateRange(m_dtFilterStart);

	SetDlgItemText(IDC_EDIT_FILTER_HOST, m_strHostName);

	InitTab();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CLogMainDlg::InitTab()
{
	//m_hostLogTabCtrl.InitImageListHighColor(IDB_PLAN_TABLIST, RGB(192,192,192));


	// 컨텐츠 패키지 변경명령 로그
	m_hostLogTabCtrl.InsertItem(eApplyLog, LoadStringById(IDS_PACKAGE_CHNG_LOG_STR001), eApplyLog);
	m_title[eApplyLog] = LoadStringById(IDS_PACKAGE_CHNG_LOG_STR001);
	m_subLog[eApplyLog] = (CSubLog*)new CSubApplyLog(this);
	m_listCtrl[eApplyLog] = &m_ApplyLogListCtrl;

	// 컨텐츠 패키지 변경결과 로그
	m_hostLogTabCtrl.InsertItem(eScheduleStateLog, LoadStringById(IDS_LOG_PACKAGE_STR001), eScheduleStateLog);
	m_title[eScheduleStateLog] = LoadStringById(IDS_LOG_PACKAGE_STR001);
	m_subLog[eScheduleStateLog] = (CSubLog*)new CSubScheduleStateLog(this);
	m_listCtrl[eScheduleStateLog] = &m_ScheduleStateLogListCtrl;


	// Power On/Off 로그
	m_hostLogTabCtrl.InsertItem(ePowerStateLog, LoadStringById(IDS_LOG_POWER_ONOFF_STR001), ePowerStateLog);
	m_title[ePowerStateLog] = LoadStringById(IDS_LOG_POWER_ONOFF_STR001);
	m_subLog[ePowerStateLog] = (CSubLog*)new CSubPowerStateLog(this);
	m_listCtrl[ePowerStateLog] = &m_PowerStateLogListCtrl;

	// Connection 로그
	m_hostLogTabCtrl.InsertItem(eConnectionStateLog, LoadStringById(IDS_LOG_CONNECTION_STR001), eConnectionStateLog);
	m_title[eConnectionStateLog] = LoadStringById(IDS_LOG_CONNECTION_STR001);
	m_subLog[eConnectionStateLog] = (CSubLog*)new CSubConnectionStateLog(this);
	m_listCtrl[eConnectionStateLog] = &m_ConnectionStateLogListCtrl;

	// DownloadSummary 로그
	m_hostLogTabCtrl.InsertItem(eDownloadSummaryLog, LoadStringById(IDS_LOG_DOWNLOAD_STR001), eDownloadSummaryLog);
	m_title[eDownloadSummaryLog] = LoadStringById(IDS_LOG_DOWNLOAD_STR001);
	m_subLog[eDownloadSummaryLog] = (CSubLog*)new CSubConnectionStateLog(this);
	m_listCtrl[eDownloadSummaryLog] = &m_ConnectionStateLogListCtrl;

	ToggleTab(m_defaultTab);
}

void CLogMainDlg::ToggleTab(int nIndex)
{
	m_hostLogTabCtrl.SetCurSel(nIndex);
	InitList();
}
void CLogMainDlg::InitList()
{
	int idx = m_hostLogTabCtrl.GetCurSel();
	if(idx<0 || idx>eMaxTab) return;

	CSubLog* subLog = m_subLog[idx];
	if(subLog) {
		subLog->InitList();
	}

	for(int i=0;i<eMaxTab;i++){
		if(this->m_listCtrl[i]){
			if(idx == i) {
				this->m_listCtrl[i]->ShowWindow(SW_NORMAL);
			}else{
				this->m_listCtrl[i]->ShowWindow(SW_HIDE);
			}
		}
	}
}

void CLogMainDlg::RefreshList()
{
	int idx = m_hostLogTabCtrl.GetCurSel();
	if(idx<0 || idx>eMaxTab) return;

	CSubLog* subLog = m_subLog[idx];
	if(subLog) {
		subLog->RefreshList();
	}
}


void CLogMainDlg::OnBnClickedButtonRefresh()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	RefreshList();
}

void CLogMainDlg::OnBnClickedButtonFilterHost()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CSelectHostDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	m_strHostId = _T("");
	SetDlgItemText(IDC_EDIT_FILTER_HOST, _T(""));

	if(dlg.DoModal() == IDOK)
	{
		m_strHostId = dlg.m_strHostId;
		m_strHostName = dlg.m_strHostName;
		SetDlgItemText(IDC_EDIT_FILTER_HOST, m_strHostName);
	}
}

void CLogMainDlg::OnBnClickedBnToExcel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CHostExcelSave dlg;

	int idx = m_hostLogTabCtrl.GetCurSel();
	if(idx<0 || idx>eMaxTab) return;

	CString strExcelFile = dlg.Save(m_title[idx], m_ApplyLogListCtrl);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}

void CLogMainDlg::OnBnClickedButtonClose()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CLogMainDlg::OnTcnSelchangeHostlogTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ToggleTab(m_hostLogTabCtrl.GetCurSel());
	*pResult = 0;
}

void CLogMainDlg::OnTcnSelchangingHostlogTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

// 컨텐츠 패키지 변경명령 로그

void CSubApplyLog::InitList()
{
	if(m_alreadyInited) return;
	m_alreadyInited = true;

	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_ApplyLogListCtrl);

	listCtrl->SetExtendedStyle(listCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_LogColumn[eSite     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST001);
	m_LogColumn[eHost     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST002);
	m_LogColumn[ePackage  ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST003);
	m_LogColumn[eApplyTime] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST004);
	m_LogColumn[eHow      ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST005);
	m_LogColumn[eWho      ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST006);
	m_LogColumn[eWhy      ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST007);

	listCtrl->InsertColumn(eSite     , m_LogColumn[eSite     ], LVCFMT_LEFT  ,  60);
	listCtrl->InsertColumn(eHost     , m_LogColumn[eHost     ], LVCFMT_LEFT  ,  80);
	listCtrl->InsertColumn(ePackage  , m_LogColumn[ePackage  ], LVCFMT_LEFT  , 220);
	listCtrl->InsertColumn(eApplyTime, m_LogColumn[eApplyTime], LVCFMT_CENTER, 120);
	listCtrl->InsertColumn(eHow      , m_LogColumn[eHow      ], LVCFMT_LEFT  ,  80);
	listCtrl->InsertColumn(eWho      , m_LogColumn[eWho      ], LVCFMT_LEFT  , 180);
	listCtrl->InsertColumn(eWhy      , m_LogColumn[eWhy      ], LVCFMT_LEFT  ,  80);

	// 제일 마지막에 할것...
	listCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubApplyLog::RefreshList()
{
	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_ApplyLogListCtrl);
	
	CWaitMessageBox wait;

	listCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();

	CString strWhere;

	CTime tmStartDate;
	m_parentDlg->m_dtFilterStart.GetTime(tmStartDate);
	
	CTime tmEndDate;
	m_parentDlg->m_dtFilterEnd.GetTime(tmEndDate);

	// 작성일자
	CString strStartDate = tmStartDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = tmEndDate.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");
	strWhere.Format(_T("applyTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);

	strWhere += _T(" order by applyTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetApplyInfoList( m_parentDlg->m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	listCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SApplyLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTime = CTime(info.applyTime);

		listCtrl->InsertItem(nRow, info.siteId);
		listCtrl->SetItemText(nRow, eHost     , info.hostId   );
		listCtrl->SetItemText(nRow, ePackage  , info.programId);
		listCtrl->SetItemText(nRow, eApplyTime, (info.applyTime == 0 ? "" : tmApplyTime.Format("%Y/%m/%d %H:%M:%S")));
		listCtrl->SetItemText(nRow, eHow      , info.how      );
		listCtrl->SetItemText(nRow, eWho      , info.who      );
		listCtrl->SetItemText(nRow, eWhy      , info.why      );

		listCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	listCtrl->SetRedraw(TRUE);
}

// 컨텐츠 패키지 변경결과 로그

void CSubScheduleStateLog::InitList()
{
	if(m_alreadyInited) return;
	m_alreadyInited = true;

	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_ScheduleStateLogListCtrl);

	listCtrl->SetExtendedStyle(listCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_LogColumn[eSite     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST001);
	m_LogColumn[eHost     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST002);
	m_LogColumn[eEventTime] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST004);
	m_LogColumn[eLastSchedule1] = LoadStringById(IDS_SCHEDULESTATE_LOG_LIST004);
	m_LogColumn[eAutoSchedule1] = LoadStringById(IDS_SCHEDULESTATE_LOG_LIST005);
	m_LogColumn[eCurrentSchedule1] = LoadStringById(IDS_SCHEDULESTATE_LOG_LIST006);

	listCtrl->InsertColumn(eSite     , m_LogColumn[eSite     ], LVCFMT_LEFT  ,  60);
	listCtrl->InsertColumn(eHost     , m_LogColumn[eHost     ], LVCFMT_LEFT  ,  80);
	listCtrl->InsertColumn(eEventTime, m_LogColumn[eEventTime], LVCFMT_CENTER, 120);
	listCtrl->InsertColumn(eLastSchedule1      , m_LogColumn[eLastSchedule1  ], LVCFMT_LEFT  ,200);
	listCtrl->InsertColumn(eAutoSchedule1      , m_LogColumn[eAutoSchedule1  ], LVCFMT_LEFT  ,200);
	listCtrl->InsertColumn(eCurrentSchedule1      , m_LogColumn[eCurrentSchedule1      ], LVCFMT_LEFT  ,200);

	// 제일 마지막에 할것...
	listCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubScheduleStateLog::RefreshList()
{
	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_ScheduleStateLogListCtrl);
	
	CWaitMessageBox wait;

	listCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();

	CString strWhere;

	CTime tmStartDate;
	m_parentDlg->m_dtFilterStart.GetTime(tmStartDate);
	
	CTime tmEndDate;
	m_parentDlg->m_dtFilterEnd.GetTime(tmEndDate);

	// 작성일자
	CString strStartDate = tmStartDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = tmEndDate.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");
	strWhere.Format(_T("eventTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);

	strWhere += _T(" order by eventTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetScheduleStateInfoList( m_parentDlg->m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	listCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SScheduleStateLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTime = CTime(info.eventTime);

		listCtrl->InsertItem(nRow, info.siteId);
		listCtrl->SetItemText(nRow, eHost     , info.hostId   );
		listCtrl->SetItemText(nRow, eEventTime, (info.eventTime == 0 ? "" : tmApplyTime.Format("%Y/%m/%d %H:%M:%S")));
		listCtrl->SetItemText(nRow, eLastSchedule1  , info.lastSchedule1);
		listCtrl->SetItemText(nRow, eAutoSchedule1      , info.autoSchedule1      );
		listCtrl->SetItemText(nRow, eCurrentSchedule1      , info.currentSchedule1      );

		listCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	listCtrl->SetRedraw(TRUE);
}

// Power 로그

void CSubPowerStateLog::InitList()
{
	if(m_alreadyInited) return;
	m_alreadyInited = true;

	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_PowerStateLogListCtrl);

	listCtrl->SetExtendedStyle(listCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_LogColumn[eSite     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST001);
	m_LogColumn[eHost     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST002);
	m_LogColumn[eEventTime] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST004);
	m_LogColumn[eBootUpTime] = LoadStringById(IDS_HOSTLOGVIEW_LIST007);
	m_LogColumn[eBootDownTime] = LoadStringById(IDS_HOSTLOGVIEW_LIST008);

	listCtrl->InsertColumn(eSite     , m_LogColumn[eSite     ], LVCFMT_LEFT  ,  60);
	listCtrl->InsertColumn(eHost     , m_LogColumn[eHost     ], LVCFMT_LEFT  ,  80);
	listCtrl->InsertColumn(eEventTime, m_LogColumn[eEventTime], LVCFMT_CENTER, 120);
	listCtrl->InsertColumn(eBootUpTime      , m_LogColumn[eBootUpTime  ], LVCFMT_LEFT  ,120);
	listCtrl->InsertColumn(eBootDownTime      , m_LogColumn[eBootDownTime  ], LVCFMT_LEFT  ,120);

	// 제일 마지막에 할것...
	listCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubPowerStateLog::RefreshList()
{
	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_PowerStateLogListCtrl);
	
	CWaitMessageBox wait;

	listCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();

	CString strWhere;

	CTime tmStartDate;
	m_parentDlg->m_dtFilterStart.GetTime(tmStartDate);
	
	CTime tmEndDate;
	m_parentDlg->m_dtFilterEnd.GetTime(tmEndDate);

	// 작성일자
	CString strStartDate = tmStartDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = tmEndDate.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");
	strWhere.Format(_T("eventTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);

	strWhere += _T(" order by eventTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetPowerStateLogByHost( m_parentDlg->m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	listCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SPowerStateLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTime = CTime(info.eventTime);
		CTime tmBootUpTime = CTime(info.bootUpTime);
		CTime tmBootDownTime = CTime(info.bootDownTime);

		listCtrl->InsertItem(nRow, info.siteId);
		listCtrl->SetItemText(nRow, eHost     , info.hostId   );
		listCtrl->SetItemText(nRow, eEventTime, (info.eventTime == 0 ? "" : tmApplyTime.Format("%Y/%m/%d %H:%M:%S")));
		listCtrl->SetItemText(nRow, eBootUpTime, (info.bootUpTime == 0 ? "" : tmBootUpTime.Format("%Y/%m/%d %H:%M:%S")));
		listCtrl->SetItemText(nRow, eBootDownTime, (info.bootDownTime == 0 ? "" : tmBootDownTime.Format("%Y/%m/%d %H:%M:%S")));

		listCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	listCtrl->SetRedraw(TRUE);
}

// Connection 로그

void CSubConnectionStateLog::InitList()
{
	if(m_alreadyInited) return;
	m_alreadyInited = true;

	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_ConnectionStateLogListCtrl);

	listCtrl->SetExtendedStyle(listCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_LogColumn[eSite     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST001);
	m_LogColumn[eHost     ] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST002);
	m_LogColumn[eEventTime] = LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST004);
	m_LogColumn[eOperationalState] = LoadStringById(IDS_HOSTLOGVIEW_LIST009);

	listCtrl->InsertColumn(eSite     , m_LogColumn[eSite     ], LVCFMT_LEFT  ,  60);
	listCtrl->InsertColumn(eHost     , m_LogColumn[eHost     ], LVCFMT_LEFT  ,  80);
	listCtrl->InsertColumn(eEventTime, m_LogColumn[eEventTime], LVCFMT_CENTER, 120);
	listCtrl->InsertColumn(eOperationalState      , m_LogColumn[eOperationalState  ], LVCFMT_LEFT  ,200);

	// 제일 마지막에 할것...
	listCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubConnectionStateLog::RefreshList()
{
	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_ConnectionStateLogListCtrl);
	
	CWaitMessageBox wait;

	listCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();

	CString strWhere;

	CTime tmStartDate;
	m_parentDlg->m_dtFilterStart.GetTime(tmStartDate);
	
	CTime tmEndDate;
	m_parentDlg->m_dtFilterEnd.GetTime(tmEndDate);

	// 작성일자
	CString strStartDate = tmStartDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = tmEndDate.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");
	strWhere.Format(_T("eventTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);

	strWhere += _T(" order by eventTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetConnectionStateLogByHost( m_parentDlg->m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	listCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SConnectionStateLogInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTime = CTime(info.eventTime);

		listCtrl->InsertItem(nRow, info.siteId);
		listCtrl->SetItemText(nRow, eHost     , info.hostId   );
		listCtrl->SetItemText(nRow, eEventTime, (info.eventTime == 0 ? "" : tmApplyTime.Format("%Y/%m/%d %H:%M:%S")));
		listCtrl->SetItemText(nRow, eOperationalState, (info.operationalState ? "connect" : "disconnect"));

		listCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	listCtrl->SetRedraw(TRUE);
}

// DownloadSummary 로그

void CSubDownloadSummaryLog::InitList()
{
	if(m_alreadyInited) return;
	m_alreadyInited = true;

	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_DownloadSummaryLogListCtrl);

	listCtrl->SetExtendedStyle(listCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_LogColumn[eSiteName    ] =		LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST001);
	m_LogColumn[eHostName     ] =		LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST002);
	m_LogColumn[eProgramId     ] =		LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST002);
	m_LogColumn[eBrwId     ] =			LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST002);
	m_LogColumn[eProgramState] =		LoadStringById(IDS_HOSTLOGVIEW_LIST009);
	m_LogColumn[eProgramStartTime] =	LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST004);
	m_LogColumn[eProgramEndTime] =		LoadStringById(IDS_PACKAGE_CHNG_LOG_LIST004);
	m_LogColumn[eProgress] =			LoadStringById(IDS_HOSTLOGVIEW_LIST009);

	listCtrl->InsertColumn(eSiteName, m_LogColumn[eSiteName     ], LVCFMT_LEFT  ,  60);
	listCtrl->InsertColumn(eHostName, m_LogColumn[eHostName     ], LVCFMT_LEFT  ,  80);
	listCtrl->InsertColumn(eProgramId, m_LogColumn[eProgramId  ], LVCFMT_LEFT  ,200);
	listCtrl->InsertColumn(eProgramState, m_LogColumn[eProgramState  ], LVCFMT_LEFT  ,200);
	listCtrl->InsertColumn(eBrwId, m_LogColumn[eBrwId  ], LVCFMT_LEFT  ,200);
	listCtrl->InsertColumn(eProgramStartTime, m_LogColumn[eProgramStartTime], LVCFMT_CENTER, 120);
	listCtrl->InsertColumn(eProgramEndTime, m_LogColumn[eProgramEndTime], LVCFMT_CENTER, 120);
	listCtrl->InsertColumn(eProgress, m_LogColumn[eProgress  ], LVCFMT_LEFT  ,200);

	// 제일 마지막에 할것...
	listCtrl->InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}


void CSubDownloadSummaryLog::RefreshList()
{
	CUTBListCtrlEx* listCtrl = &(m_parentDlg->m_DownloadSummaryLogListCtrl);
	
	CWaitMessageBox wait;

	listCtrl->DeleteAllItems();
	m_LogDataList.RemoveAll();

	CString strWhere;

	CTime tmStartDate;
	m_parentDlg->m_dtFilterStart.GetTime(tmStartDate);
	
	CTime tmEndDate;
	m_parentDlg->m_dtFilterEnd.GetTime(tmEndDate);

	// 작성일자
	CString strStartDate = tmStartDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	CString strEndDate = tmEndDate.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");
	strWhere.Format(_T("eventTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);

	strWhere += _T(" order by eventTime desc");
	strWhere.Trim();

	if(!CCopModule::GetObject()->GetDownloadSummary( "",m_parentDlg->m_strHostId, m_LogDataList, strWhere ))
	{
		return;
	}

	listCtrl->SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_LogDataList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SDownloadSummaryInfo& info = m_LogDataList.GetNext(pos);

		CTime tmApplyTimeStart = CTime(info.programStartTime);
		CTime tmApplyTimeEnd = CTime(info.programEndTime);

		listCtrl->InsertItem(nRow, info.siteId);
		listCtrl->SetItemText(nRow, eHostName, info.hostId   );
		listCtrl->SetItemText(nRow, eProgramId, info.programId   );
		listCtrl->SetItemText(nRow, eProgramState,		(info.programState ? "connect" : "disconnect"));
		listCtrl->SetItemText(nRow, eProgress, info.progress   );
		listCtrl->SetItemText(nRow, eProgramStartTime,	(info.programStartTime == 0 ? "" : tmApplyTimeStart.Format("%Y/%m/%d %H:%M:%S")));
		listCtrl->SetItemText(nRow, eProgramEndTime,	(info.programEndTime == 0 ? "" : tmApplyTimeEnd.Format("%Y/%m/%d %H:%M:%S")));

		listCtrl->SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	listCtrl->SetRedraw(TRUE);
}


