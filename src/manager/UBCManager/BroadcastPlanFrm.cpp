// BroadcastPlanFrm.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "BroadcastPlanFrm.h"
#include "Enviroment.h"


// CBroadcastPlanFrm

IMPLEMENT_DYNCREATE(CBroadcastPlanFrm, CMDIChildWnd)

CBroadcastPlanFrm::CBroadcastPlanFrm()
:	m_EventManager(this)
{
}

CBroadcastPlanFrm::~CBroadcastPlanFrm()
{
}


BEGIN_MESSAGE_MAP(CBroadcastPlanFrm, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CBroadcastPlanFrm 메시지 처리기입니다.

int CBroadcastPlanFrm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDI_BROADCASTPLAN), false);

	// 이벤트생성시 오래걸리는 현상이 발생되어 스레드로 뺀다.
	AfxBeginThread(AddEventThread, this);

	return 0;
}

UINT CBroadcastPlanFrm::AddEventThread(LPVOID pParam)
{
	CBroadcastPlanFrm* pFrame = (CBroadcastPlanFrm*)pParam;
	if(!pFrame) return 0;
	HWND hWnd = pFrame->GetSafeHwnd();

	TraceLog(("CBroadcastPlanFrm::AddEventThread - Start"));

	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// 2010.05.13 이벤트등록을 데드락 발생으로 OnCreate에서 이쪽으로 옮김
	CString szBuf;
	szBuf.Format("PM=*/Site=%s/BP=* ", (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? "*" : GetEnvPtr()->m_szSite));

	pFrame->m_EventManager.AddEvent(szBuf + "bpCreated");
	pFrame->m_EventManager.AddEvent(szBuf + "bpChanged");
	pFrame->m_EventManager.AddEvent(szBuf + "bpRemoved");

	CoUninitialize();

	TraceLog(("CBroadcastPlanFrm::AddEventThread - End"));

	return 0;
}

void CBroadcastPlanFrm::OnClose()
{
	TraceLog(("CBroadcastPlanFrm::OnClose begin"));

	// 0001414: 단말목록등 서버로부터 이벤트를 받는 화면이 닫힐때 응답없음으로 빠지는 문제.
	// 더이상 이벤트가 날아와도 처리하지 않도록 한다
	if(!CEventHandler::CloseWaitAllEvent(GetSafeHwnd())) return;

	m_EventManager.RemoveAllEvent();

	CEventHandler::SetIgnoreEvent(false);

	CMDIChildWnd::OnClose();

	TraceLog(("CBroadcastPlanFrm::OnClose end"));
}

int CBroadcastPlanFrm::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	TraceLog(("CBroadcastPlanFrm::InvokeEvent begin"));

	HWND hWnd = GetSafeHwnd();
	TraceLog(("hWnd [%X]", hWnd));
	if(::IsWindow(hWnd) == FALSE) return 0;

	if(GetActiveView())
	{
		return GetActiveView()->SendMessage(WM_INVOKE_EVENT, wParam, lParam);
	}

	TraceLog(("CBroadcastPlanFrm::InvokeEvent end"));

	return 0;
}
