// ProcessFrm.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "ProcessFrm.h"
#include "Enviroment.h"


// CProcessFrm

IMPLEMENT_DYNCREATE(CProcessFrm, CMDIChildWnd)

CProcessFrm::CProcessFrm()
:	m_EventManager(this)
{
}

CProcessFrm::~CProcessFrm()
{
}


BEGIN_MESSAGE_MAP(CProcessFrm, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CProcessFrm 메시지 처리기입니다.

int CProcessFrm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDI_BROADCASTPLAN), false);

	// 이벤트생성시 오래걸리는 현상이 발생되어 스레드로 뺀다.
	AfxBeginThread(AddEventThread, this);

	return 0;
}

UINT CProcessFrm::AddEventThread(LPVOID pParam)
{
	CProcessFrm* pFrame = (CProcessFrm*)pParam;
	if(!pFrame) return 0;
	HWND hWnd = pFrame->GetSafeHwnd();

	TraceLog(("CProcessFrm::AddEventThread - Start"));

	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	CString szBuf;
	szBuf.Format("PM=*/Site=%s/Host=* ", (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite));

	pFrame->m_EventManager.AddEvent(szBuf + _T("operationalStateChanged"));
	pFrame->m_EventManager.AddEvent(szBuf + _T("adminStateChanged"));
	pFrame->m_EventManager.AddEvent(szBuf + _T("vncStateChanged"));
	pFrame->m_EventManager.AddEvent(szBuf + _T("processStateChanged"));
	pFrame->m_EventManager.AddEvent(szBuf + _T("monitorStateChanged")); // 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)

	::CoUninitialize();

	TraceLog(("CProcessFrm::AddEventThread - End"));

	return 0;
}

void CProcessFrm::OnClose()
{
	TraceLog(("CProcessFrm::OnClose begin"));

	// 0001414: 단말목록등 서버로부터 이벤트를 받는 화면이 닫힐때 응답없음으로 빠지는 문제.
	// 더이상 이벤트가 날아와도 처리하지 않도록 한다
	if(!CEventHandler::CloseWaitAllEvent(GetSafeHwnd())) return;

	m_EventManager.RemoveAllEvent();

	CEventHandler::SetIgnoreEvent(false);

	CMDIChildWnd::OnClose();

	TraceLog(("CProcessFrm::OnClose end"));
}

int CProcessFrm::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	TraceLog(("CProcessFrm::InvokeEvent begin"));

	HWND hWnd = GetSafeHwnd();
	TraceLog(("hWnd [%X]", hWnd));
	if(::IsWindow(hWnd) == FALSE) return 0;

	if(GetActiveView())
	{
		return GetActiveView()->SendMessage(WM_INVOKE_EVENT, wParam, lParam);
	}

	TraceLog(("CProcessFrm::InvokeEvent end"));

	return 0;
}
