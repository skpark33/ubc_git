// UBCManager.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "LicenseInfoDlg.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "afxwin.h"
#include "enviroment.h"
#include "common\UbcCode.h"

using namespace std;
#include "common\libscratch\scratchUtil.h"
#include "common\libFlashRollback\libFlashRollback.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CUBCManagerApp
BEGIN_MESSAGE_MAP(CUBCManagerApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CUBCManagerApp::OnAppAbout)
	ON_COMMAND(ID_FILE_NEW, &CUBCManagerApp::OnFileNew)
	ON_COMMAND(ID_32893, &CUBCManagerApp::OnLincenseInfo)
END_MESSAGE_MAP()

// CUBCManagerApp construction
CUBCManagerApp::CUBCManagerApp()
: m_hMDIMenu(NULL)
, m_hMDIAccel(NULL)
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CUBCManagerApp object

CUBCManagerApp theApp;


// CUBCManagerApp initialization

BOOL CUBCManagerApp::InitInstance()
{
//TODO: call AfxInitRichEdit2() to initialize richedit2 library.
	// 프로세스 중복 실행 방지
	HANDLE hMutex = ::CreateMutex(NULL,FALSE, "UBCManager");
	if (hMutex != NULL)
	{
		if(::GetLastError() == ERROR_ALREADY_EXISTS) {
			unsigned long lPid = scratchUtil::getInstance()->getPid(UBCMANAGEREXE_STR);
			HWND hWnd = scratchUtil::getInstance()->getWHandle(lPid);

			if(hWnd){
				::ReleaseMutex(hMutex);
				UbcMessageBox(LoadStringById(IDS_UBCSTUDIOAPP_MSG001), MB_ICONERROR);
				exit(1);
				return FALSE;
			}
			// Windows Handle 이 없다면 좀비로 인식하고 죽임.
			TraceLog(("killProcess(%d)",lPid));
			scratchUtil::getInstance()->killProcess(lPid);
		}
		::ReleaseMutex(hMutex);
	}

	// 0000691: Studio/Manager 기동시, 해당 프로그램을 방화벽 예외로 설정해주어야 함.
	//Firewall exception 등록
	if(!scratchUtil::getInstance()->AddToExceptionList())
	{
		TraceLog(("Fail to add windows firewall exception"));
	}

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		UbcMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization

	HMODULE hModule = LoadLibrary(_T("UBCManagerML.dll"));
	if(hModule) AfxSetResourceHandle(hModule);

	CString strCommandLine(m_lpCmdLine);
	if( strCommandLine.Find(_T("-L:eng")) >= 0 )
		SetThreadLocaleEx(MAKELCID(MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US),SORT_DEFAULT));
	else if( strCommandLine.Find(_T("-L:jpn")) >= 0 )
		SetThreadLocaleEx(MAKELCID(MAKELANGID(LANG_JAPANESE,SUBLANG_DEFAULT),SORT_DEFAULT));
	else if( strCommandLine.Find(_T("-L:kor")) >= 0 )
		SetThreadLocaleEx(MAKELCID(MAKELANGID(LANG_KOREAN,SUBLANG_KOREAN),SORT_DEFAULT));
	else
		SetThreadLocaleEx(MAKELCID(MAKELANGID(LANG_NEUTRAL,SUBLANG_SYS_DEFAULT),SORT_DEFAULT));

	// 플래시 ocx 롤백
	CFlashRollback fbb;
	fbb.CheckFlashFile();

	SetRegistryKey(_T("SQISoft"));
	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views

	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object
	CMDIFrameWnd* pFrame = new CMainFrame;
	if (!pFrame)
		return FALSE;
	m_pMainWnd = pFrame;
	// create main MDI frame window
	if (!pFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	// try to load shared MDI menus and accelerator table
	//TODO: add additional member variables and load calls for
	//	additional menu types your application may need
	HINSTANCE hInst = AfxGetResourceHandle();
	//m_hMDIMenu  = ::LoadMenu(hInst, MAKEINTRESOURCE(IDR_UBCManagerTYPE));
	//m_hMDIAccel = ::LoadAccelerators(hInst, MAKEINTRESOURCE(IDR_UBCManagerTYPE));

	// The main window has been initialized, so show and update it
	pFrame->ShowWindow(m_nCmdShow);
	pFrame->UpdateWindow();

	return TRUE;
}


// CUBCManagerApp message handlers

int CUBCManagerApp::ExitInstance() 
{
	//TODO: handle additional resources you may have added
	if (m_hMDIMenu != NULL)
		FreeResource(m_hMDIMenu);
	if (m_hMDIAccel != NULL)
		FreeResource(m_hMDIAccel);

	CUbcCode::ClearInstance();

	return CWinApp::ExitInstance();
}

void CUBCManagerApp::OnFileNew() 
{
//	CMainFrame* pFrame = STATIC_DOWNCAST(CMainFrame, m_pMainWnd);
	// create a new MDI child window
//	pFrame->CreateNewChild(RUNTIME_CLASS(CChildFrame), IDR_UBCManagerTYPE, m_hMDIMenu, m_hMDIAccel);
}

BOOL CUBCManagerApp::SetThreadLocaleEx(LCID lcLocale)
{
	OSVERSIONINFO osVersion;
	osVersion.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if (::GetVersionEx(&osVersion) == FALSE) return false;

	BOOL bResult = FALSE;

	// >= Windows Vista
	if (osVersion.dwMajorVersion == 6)
	{   
		HINSTANCE hKernelDll = ::LoadLibrary(_T("kernel32.dll"));  
		if (hKernelDll == NULL) return false;

		unsigned (WINAPI* SetThreadUILanguage)(LANGID) = (unsigned (WINAPI* )(LANGID))::GetProcAddress(hKernelDll, "SetThreadUILanguage");
		if (SetThreadUILanguage == NULL) return false;

		LANGID resLangID = SetThreadUILanguage(static_cast<LANGID>(lcLocale));
		bResult = (resLangID == LOWORD(lcLocale));
	}
	// <= Windows XP
	else 
	{
		bResult = ::SetThreadLocale(lcLocale);
	}

	return bResult;
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	CString m_szSite;
	CString m_szUser;
	CString m_szCustomer;
protected:
	virtual void PreInitDialog();
	virtual BOOL OnInitDialog();
public:
	CString m_szServerVer;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
, m_szServerVer(_T(""))
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_STATIC_SITE, m_szSite);
	DDX_Text(pDX, IDC_STATIC_USER, m_szUser);
	DDX_Text(pDX, IDC_STATIC_CUSTOMER, m_szCustomer);
	DDX_Text(pDX, IDC_STATIC_SERVERVER, m_szServerVer);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CUBCManagerApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CUBCManagerApp message handlers
void CAboutDlg::PreInitDialog()
{
	CDialog::PreInitDialog();
}

BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	char szTemp[1024];
	CString szCopyright, szVersion;
	scratchUtil* su = scratchUtil::getInstance();

	std::string sVer;
	su->getVersion(sVer, false);

	if(CEnviroment::eSQISOFT == GetEnvPtr()->m_Customer){
		szCopyright = "Copyright (C) 2009 SQI Soft Corporation.";
		szVersion.Format("Manager Version %s", sVer.c_str());
	}else{
		szCopyright = "";
		szVersion.Format("Manager Version %s", sVer.c_str());

		CRect rc;
		GetDlgItem(IDC_ABOUT_COPYRIGHT)->GetClientRect(&rc);
		GetDlgItem(IDC_ABOUT_COPYRIGHT)->ClientToScreen(&rc);
		ScreenToClient(&rc);

		int nCY = rc.Height()/2;
		GetDlgItem(IDC_ABOUT_COPYRIGHT)->MoveWindow(rc.left, rc.top-rc.Height()+nCY, rc.Width(), rc.Height());
		GetDlgItem(IDC_ABOUT_VERSION)->MoveWindow(rc.left, rc.top+nCY, rc.Width(), rc.Height());
		GetDlgItem(IDC_STATIC_SITE)->MoveWindow(rc.left, rc.top+rc.Height()+nCY, rc.Width(), rc.Height());
		GetDlgItem(IDC_STATIC_USER)->MoveWindow(rc.left, rc.top+2*rc.Height()+nCY, rc.Width(), rc.Height());
		GetDlgItem(IDC_STATIC_CUSTOMER)->MoveWindow(rc.left, rc.top+3*rc.Height()+nCY, rc.Width(), rc.Height());
	}

	GetDlgItem(IDC_ABOUT_COPYRIGHT)->SetWindowText(szCopyright);
	GetDlgItem(IDC_ABOUT_VERSION)->SetWindowText(szVersion);

	CStatic* pLogo = (CStatic*)GetDlgItem(IDC_ABOUT_LOGO);
	if(CEnviroment::eNARSHA == GetEnvPtr()->m_Customer){
		pLogo->SetIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME_NS));
	}else{
		pLogo->SetIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME));
	}

	m_szSite = "Site : ";
	m_szSite += GetEnvPtr()->m_szSite;
	m_szUser = "Login ID : ";
	m_szUser += GetEnvPtr()->m_szLoginID;
	m_szCustomer = "Customer ID : ";
	m_szCustomer += GetEnvPtr()->m_strCustomer;
	m_szServerVer = "Server Version ";
	m_szServerVer += GetEnvPtr()->GetServerVersion();
		
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CUBCManagerApp::OnLincenseInfo()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CLicenseInfoDlg licenseInfoDlg;
	licenseInfoDlg.DoModal();

}
