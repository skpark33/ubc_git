// FaultMngFrm.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "FaultMngFrm.h"
#include "Enviroment.h"


// CFaultMngFrm

IMPLEMENT_DYNCREATE(CFaultMngFrm, CMDIChildWnd)

CFaultMngFrm::CFaultMngFrm()
:	m_EventManager(this)
{
}

CFaultMngFrm::~CFaultMngFrm()
{
}


BEGIN_MESSAGE_MAP(CFaultMngFrm, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CFaultMngFrm 메시지 처리기입니다.

int CFaultMngFrm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDR_HOSTFRAME), false);

	// 이벤트생성시 오래걸리는 현상이 발생되어 스레드로 뺀다.
	AfxBeginThread(AddEventThread, this);

	return 0;
}

UINT CFaultMngFrm::AddEventThread(LPVOID pParam)
{
	CFaultMngFrm* pFrame = (CFaultMngFrm*)pParam;
	if(!pFrame) return 0;
	HWND hWnd = pFrame->GetSafeHwnd();

	TraceLog(("CFaultMngFrm::AddEventThread - Start"));

	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	CString strEntity;
	strEntity.Format(_T("FM=*/Site=%s/Host=*"), (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? "*" : GetEnvPtr()->m_szSite));
	//strEntity.Format(_T("FM=*/Site=%s/Host=*/Fault=*"), (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? "*" : GetEnvPtr()->m_szSite));

	pFrame->m_EventManager.AddEvent(strEntity + _T(" faultAlarm"));
	pFrame->m_EventManager.AddEvent(_T("FM=* updateAlarm"));

	::CoUninitialize();

	TraceLog(("CFaultMngFrm::AddEventThread - End"));

	return 0;
}

void CFaultMngFrm::OnClose()
{
	TraceLog(("CFaultMngFrm::OnClose begin"));

	// 0001414: 단말목록등 서버로부터 이벤트를 받는 화면이 닫힐때 응답없음으로 빠지는 문제.
	// 더이상 이벤트가 날아와도 처리하지 않도록 한다
	if(!CEventHandler::CloseWaitAllEvent(GetSafeHwnd())) return;

	m_EventManager.RemoveAllEvent();

	CEventHandler::SetIgnoreEvent(false);

	CMDIChildWnd::OnClose();

	TraceLog(("CFaultMngFrm::OnClose end"));
}

int CFaultMngFrm::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	TraceLog(("CFaultMngFrm::InvokeEvent begin"));

	HWND hWnd = GetSafeHwnd();
	TraceLog(("hWnd [%X]", hWnd));
	if(::IsWindow(hWnd) == FALSE) return 0;

	if(GetActiveView())
	{
		return GetActiveView()->SendMessage(WM_INVOKE_EVENT, wParam, lParam);
	}

	TraceLog(("CFaultMngFrm::InvokeEvent end"));

	return 0;
}
