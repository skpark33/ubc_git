// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afx.h>
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#include <afxdisp.h>        // MFC Automation classes

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

//#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
//#endif

//#ifdef _ML_ENG_
//#include "UBCManager_en.h"
//#elif _ML_KOR_
//#include "UBCManager_kr.h"
//#elif _ML_JAP_
//#include "UBCManager_jp.h"
//#endif

#include "common/libCommon/utvMacro.h"
#include <afxmt.h>
#include "common\ubcdefine.h"
#include "UbcCommon/WaitMessageBox.h"

#define		WM_DSINTERFACES_GRAPHNOTIFY				(WM_USER + 100)
#define		WM_HIDE_CONTENTS						(WM_USER + 119)

const UINT WM_CLOSE_DIALOG = RegisterWindowMessage(_T("WM_CLOSE_DIALOG"));

CString ToString(int);
CString ToString(double, int nDegree=3);
CString ToString(ULONG);
CString ToString(ULONGLONG);
COLORREF GetColorFromString(LPCTSTR lpszColor);
CString GetColorFromString(COLORREF rgb);
void	GetColorRGB(COLORREF rgb, int& r, int& g, int& b);
LPCTSTR GetDataPath();
CString	FindExtension(const CString& strFileName);
CString	VariantToString(VARIANT* var);					///<VARIANT를 문자열로 변환한다.
CString ToFileSize(ULONGLONG lVal, char chUnit, bool bIncUnit = true);

LPCTSTR	GetAppPath();
CString	GetConnectServerIP(void);						///<UBCConnect.ini 파일의 Server IP 반환
bool	IsAvailableFont(CString strFontName);			///<시스템에 지정된 폰트가 있는지를 반환
LPCTSTR	GetBRWConfigPath(void);							///<UBCBrowser.ini 파일의 경로 반환
LPCTSTR	GetVARConfigPath(void);							///<UBCVariables.ini 파일의 경로 반환
int		GetVidoeRenderType(void);						///<사용할 Vidoe render 종류를 반환(1: Overlay, 2:VMR7,....)
int		GetVidoeOpenType(void);							///<사용할 Vidoe open하는 방법을 반환(1: 초기화과정에 open, 2:매번 플레이시에 open)
int		IsUseHostRegFromFile(void);						///단말등록을 파일로부터 작업할건지
CTime	NormalizeTime(CTime tmValue);
CTime	NormalizeTime(int nHour, int nMinute);

CString GetContentsTypeString(int nContentsType);
CString LoadStringById(UINT nID);

void TerminateProcess(CString processName);
BOOL IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs);
void KillProcess(DWORD dwProcessId);

void InitDateRange(CDateTimeCtrl& dtControl);

CString AsciiToBase64(CString strAscii);
CString Base64ToAscii(CString strBase64);
CString AsciiToBase64ForWeb(CString strAscii);
CString Base64ToAsciiForWeb(CString strBase64);

template<typename _Type>
CString ToCurrency(_Type var)
{
	CString szDigit;
	CString szBuf = _T("");
	_Type v = var;
	
	int i = 0;
	do{
		if(i && !(i%3)){
			szBuf = _T(",") + szBuf;
		}
		szDigit.Format("%c", (v%10) + '0');
		szBuf = szDigit + szBuf;
		v /= 10;
		i++;
	}while(v);

	return szBuf;
}

template<typename _Type>
_Type GetValueRange(_Type vValue, _Type vMin, _Type vMax)
{
	if(vValue < vMin) return vMin;
	if(vValue > vMax) return vMax;
	return vValue;
}

enum E_VIDEO_OPEN_TYPE
{
	E_OPEN_INIT = 1,		//초기화 과정에 open
	E_OPEN_PLAY,			//플레이시기에 매번 open
};

CString		ToMoneyTypeString(LPCTSTR lpszStr);
CString		ToMoneyTypeString(int value);
CString		ToMoneyTypeString(ULONG ulvalue);
CString		ToMoneyTypeString(ULONGLONG ulvalue);

int		GetEnableShortScreenshotPeriod(void);	///<짧은 스크린샷주기 여부(1:사용, 0:미사용(기본))
