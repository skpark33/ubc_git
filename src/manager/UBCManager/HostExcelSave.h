#pragma once

#include "Excel/Excel.h"
#include "common\utblistctrlex.h"

class CHostExcelSave
{
public:
	CHostExcelSave(void);
	virtual ~CHostExcelSave(void);

	CString	Save(CString strSheetName, CUTBListCtrlEx& lcList);
private:
	_Application m_excelApp;	// m_excelApp is the Excel _Application object
	_Workbook   m_book;

	BOOL	SaveDataList(short nSheet, CString strSheetName, CUTBListCtrlEx& lcList);
	BOOL	SaveDataList(CString& strFullpath, CUTBListCtrlEx& lcList);

	CString GetCell(int nCol, int nRow);
};
