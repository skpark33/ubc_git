// WebPageFrame.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "WebPageFrame.h"


// CWebPageFrame

IMPLEMENT_DYNCREATE(CWebPageFrame, CMDIChildWnd)

CWebPageFrame::CWebPageFrame()
{

}

CWebPageFrame::~CWebPageFrame()
{
}


BEGIN_MESSAGE_MAP(CWebPageFrame, CMDIChildWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CWebPageFrame message handlers

int CWebPageFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDR_CODEFRAME), false);

	return 0;
}
