#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "Enviroment.h"
#include "common\HoverButton.h"
#include "common/UBCTabCtrl.h"


// CBroadcastPlanDlg 대화 상자입니다.
class CSubBroadcastPlan;
class CBroadcastPlanDlg : public CDialog
{
	DECLARE_DYNAMIC(CBroadcastPlanDlg)

public:
	enum PLAN_MODE { eModeCreate, eModeUpdate, eModeReadonly };
	enum SUB_WIN   { eScheduling, ePeriodSetting, eSelectHost, eSummary, eMaxTab };

	CBroadcastPlanDlg(PLAN_MODE mode, SBroadcastingPlanInfo* pInfo, CString strMaxZOrder, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CBroadcastPlanDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_BROADCAST_PLAN };

	PLAN_MODE				m_nMode;
	SBroadcastingPlanInfo*	m_pBroadcastingPlanInfo;
	CString					m_strMaxZOrder;
	TimePlanInfoList		m_lsTimePlanList;
	TargetHostInfoList		m_lsTargetHostList;

	int						m_nTabOrder[eMaxTab];
	CSubBroadcastPlan*		CreateSubWindow(int nID);
	void					DeleteAllSubWindow();
	void					ToggleTab(int nIndex);

	void					InitTab();
	void					InitButton();
	void					InitButtonStat();

	bool					InitTimePlanInfoList();
	bool					InitTargetHostInfoList();

	bool					SaveTimePlanInfo(TimePlanInfoList* pInfoList, SBroadcastingPlanInfo* pPlanInfo);
	bool					SaveTargetHostInfo(TargetHostInfoList* pInfoList, SBroadcastingPlanInfo* pPlanInfo);
	bool					SaveFileAndToServer(CString strSiteId, CString strBpId);

	bool					CapacityConfirm();

protected:

	BOOL					BackupBroadcastingPlan(CString strSiteId, CString strBpId, CString strBpName);
	BOOL					RestoreBroadcastingPlan(CString strSiteId, CString strBpId, CString strBpName);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CUBCTabCtrl		m_tabMain;
	CHoverButton	m_bnClose;
	CHoverButton	m_bnPrev;
	CHoverButton	m_bnNext;
	CHoverButton	m_bnCreate;
	CHoverButton	m_bnSave;
	CHoverButton	m_bnSaveAs;
	CHoverButton	m_bnFileTrans;
	CHoverButton	m_bnSizeConfirm;

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnBnClickedButtonPrev();
	afx_msg void OnBnClickedButtonNext();
	afx_msg void OnBnClickedButtonCreate();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonSaveas();
	afx_msg void OnTcnSelchangingTabMain(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTcnSelchangeTabMain(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonToServer();
	afx_msg void OnBnClickedButtonCapacityConfirm();
};
