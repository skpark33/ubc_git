// DummyDoc.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "DummyDoc.h"


// CDummyDoc

IMPLEMENT_DYNCREATE(CDummyDoc, CDocument)

CDummyDoc::CDummyDoc()
{
}

BOOL CDummyDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

CDummyDoc::~CDummyDoc()
{
}


BEGIN_MESSAGE_MAP(CDummyDoc, CDocument)
END_MESSAGE_MAP()


// CDummyDoc 진단입니다.

#ifdef _DEBUG
void CDummyDoc::AssertValid() const
{
	CDocument::AssertValid();
}

#ifndef _WIN32_WCE
void CDummyDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif
#endif //_DEBUG

#ifndef _WIN32_WCE
// CDummyDoc serialization입니다.

void CDummyDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}
#endif


// CDummyDoc 명령입니다.
