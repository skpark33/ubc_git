#pragma once
#include "afxwin.h"


// CPropertyDlg 대화 상자입니다.

class CPropertyDlg : public CDialog
{
	DECLARE_DYNAMIC(CPropertyDlg)

public:
	CPropertyDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPropertyDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_PROPERTY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_cbDirective;
	CComboBox m_cbIniFile;
	CComboBox m_cbEntryName;
	CComboBox m_cbVarName;
	CComboBox m_cbVarValue;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();

	CString m_strName;
	CString m_strValue;

	CString& getName() { return m_strName;}
	CString& getValue() {  return m_strValue;}
	afx_msg void OnCbnSelchangeComboInifile();
	afx_msg void OnCbnSelchangeComboEntryname();
	CButton m_check_reget;
	CButton m_check_deleteFolder;
	afx_msg void OnBnClickedCheckReget();
};
